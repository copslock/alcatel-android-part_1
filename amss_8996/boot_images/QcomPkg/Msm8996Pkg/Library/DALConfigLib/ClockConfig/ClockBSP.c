/*
==============================================================================

FILE:         ClockBSP.c

DESCRIPTION:
  This file contains clock bsp data for the DAL based driver.


==============================================================================


==============================================================================
            Copyright (c) 2014-2015 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*=========================================================================
      Include Files
==========================================================================*/

#include "Drivers/ClockDxe/ClockBSP.h"

/*=========================================================================
      Data Declarations
==========================================================================*/


/*
 *  SourceFreqConfig_SLEEPCLK
 *
 *  Set of source frequency configurations.
 */
static ClockSourceFreqConfigType SourceFreqConfig_SLEEPCLK[] =
{
  {
    /* .nFreqHz    = */ 32768,
    /* .HALConfig  = */ { HAL_CLK_SOURCE_NULL },
    /* .eVRegLevel = */ CLOCK_VREG_LEVEL_OFF,
    /* .HWVersion  = */ { {0x00, 0x00}, {0x00, 0x00} },
    /* .bLastEntry = */ 0
  },
  /* last entry */
  { 0 }
};


/*
 *  SourceFreqConfig_XO
 *
 *  Set of source frequency configurations.
 */
static ClockSourceFreqConfigType SourceFreqConfig_XO[] =
{
  {
    /* .nFreqHz    = */ 19200 * 1000,
    /* .HALConfig  = */ { HAL_CLK_SOURCE_NULL },
    /* .eVRegLevel = */ CLOCK_VREG_LEVEL_OFF,
    /* .HWVersion  = */ { {0x00, 0x00}, {0x00, 0x00} },
    /* .bLastEntry = */ 0
  },
  /* last entry */
  { 0 }
};


/*
 *  SourceFreqConfig_GPLL0_DIV2
 *
 *  Set of source frequency configurations.
 */
static ClockSourceFreqConfigType SourceFreqConfig_GPLL0_DIV2[] =
{
  {
    /* .nFreqHz    = */ 300000 * 1000,
    /* .HALConfig  = */ { HAL_CLK_SOURCE_GPLL0 },
    /* .eVRegLevel = */ CLOCK_VREG_LEVEL_LOW_MINUS,
    /* .HWVersion  = */ { {0x00, 0x00}, {0x00, 0x00} },
    /* .bLastEntry = */ 0
  },
  /* last entry */
  { 0 }
};


/*
 *  SourceFreqConfig_GPLL0
 *
 *  Set of source frequency configurations.
 */
static ClockSourceFreqConfigType SourceFreqConfig_GPLL0[] =
{
  {
    /* .nFreqHz    = */ 600000 * 1000,
    /* .HALConfig  = */
    {
      /* .eSource        = */ HAL_CLK_SOURCE_XO,
      /* .eVCO           = */ HAL_CLK_PLL_VCO3,
      /* .nPreDiv        = */ 1,
      /* .nPostDiv       = */ 1,
      /* .nL             = */ 31,
      /* .nM             = */ 0,
      /* .nN             = */ 0,
      /* .nVCOMultiplier = */ 0,
      /* .nAlpha         = */ 0x00000000,
      /* .nAlphaU        = */ 0x40,
    },
    /* .eVRegLevel = */ CLOCK_VREG_LEVEL_LOW_MINUS,
    /* .HWVersion  = */ { {0x00, 0x00}, {0x00, 0x00} },
    /* .bLastEntry = */ 0
  },
  /* last entry */
  { 0 }
};


/*
 *  SourceFreqConfig_GPLL4
 *
 *  Set of source frequency configurations.
 */
static ClockSourceFreqConfigType SourceFreqConfig_GPLL4[] =
{
  {
    /* .nFreqHz    = */ 384000 * 1000,
    /* .HALConfig  = */
    {
      /* .eSource        = */ HAL_CLK_SOURCE_XO,
      /* .eVCO           = */ HAL_CLK_PLL_VCO1,
      /* .nPreDiv        = */ 1,
      /* .nPostDiv       = */ 4,
      /* .nL             = */ 80,
      /* .nM             = */ 0,
      /* .nN             = */ 0,
      /* .nVCOMultiplier = */ 0,
      /* .nAlpha         = */ 0x00000000,
      /* .nAlphaU        = */ 0x00,
    },
    /* .eVRegLevel = */ CLOCK_VREG_LEVEL_LOW,
    /* .HWVersion  = */  { {0x00, 0x00}, {0x00, 0x00} },
  },
  /* last entry */
  { 0 }
};


/*
 *  SourceFreqConfig_MMPLL0
 *
 *  Set of source frequency configurations.
 */
static ClockSourceFreqConfigType SourceFreqConfig_MMPLL0[] =
{
  {
    /* .nFreqHz    = */ 800000 * 1000,
    /* .HALConfig  = */
    {
      /* .eSource        = */ HAL_CLK_SOURCE_XO,
      /* .eVCO           = */ HAL_CLK_PLL_VCO3,
      /* .nPreDiv        = */ 1,
      /* .nPostDiv       = */ 1,
      /* .nL             = */ 41,
      /* .nM             = */ 0,
      /* .nN             = */ 0,
      /* .nVCOMultiplier = */ 0,
      /* .nAlpha         = */ 0xAAAAAAAA,
      /* .nAlphaU        = */ 0xAA,
    },
    /* .eVRegLevel = */ CLOCK_VREG_LEVEL_LOW_MINUS,
    /* .HWVersion  = */ { {0x00, 0x00}, {0x00, 0x00} },
    /* .bLastEntry = */ 0
  },
  /* last entry */
  { 0 }
};


/*
 *  SourceFreqConfig_MMPLL1
 *
 *  Set of source frequency configurations.
 */
static ClockSourceFreqConfigType SourceFreqConfig_MMPLL1[] =
{
  {
    /* .nFreqHz    = */ 740000 * 1000,
    /* .HALConfig  = */
    {
      /* .eSource        = */ HAL_CLK_SOURCE_XO,
      /* .eVCO           = */ HAL_CLK_PLL_VCO3,
      /* .nPreDiv        = */ 1,
      /* .nPostDiv       = */ 1,
      /* .nL             = */ 38,
      /* .nM             = */ 0,
      /* .nN             = */ 0,
      /* .nVCOMultiplier = */ 0,
      /* .nAlpha         = */ 0xAAAAAAAA,
      /* .nAlphaU        = */ 0x8A,
    },
    /* .eVRegLevel = */ CLOCK_VREG_LEVEL_LOW_MINUS,
    /* .HWVersion  = */  { {0x00, 0x00}, {0x02, 0x00} },
    /* .bLastEntry = */ 0
  },
  {
    /* .nFreqHz    = */ 810000 * 1000,
    /* .HALConfig  = */
    {
      /* .eSource        = */ HAL_CLK_SOURCE_XO,
      /* .eVCO           = */ HAL_CLK_PLL_VCO3,
      /* .nPreDiv        = */ 1,
      /* .nPostDiv       = */ 1,
      /* .nL             = */ 42,
      /* .nM             = */ 0,
      /* .nN             = */ 0,
      /* .nVCOMultiplier = */ 0,
      /* .nAlpha         = */ 0x00000000,
      /* .nAlphaU        = */ 0x30,
    },
    /* .eVRegLevel = */ CLOCK_VREG_LEVEL_LOW_MINUS,
    /* .HWVersion  = */ { {0x02, 0x00}, {0xFF, 0xFF} },
    /* .bLastEntry = */ 0
  },
  /* last entry */
  { 0 }
};


/*
 *  SourceFreqConfig_MMPLL2
 *
 *  Set of source frequency configurations.
 */
static ClockSourceFreqConfigType SourceFreqConfig_MMPLL2[] =
{
  {
    /* .nFreqHz    = */  125000 * 1000,
    /* .HALConfig  = */
    {
      /* .eSource        = */  HAL_CLK_SOURCE_XO,
      /* .eVCO           = */  HAL_CLK_PLL_VCO4,
      /* .nPreDiv        = */  1,
      /* .nPostDiv       = */  4,
      /* .nL             = */  26,
      /* .nM             = */  0,
      /* .nN             = */  0,
      /* .nVCOMultiplier = */  0,
      /* .nAlpha         = */  0xAAAAAAAA,
      /* .nAlphaU        = */  0x0A,
    },
    /* .eVRegLevel = */  CLOCK_VREG_LEVEL_LOW_MINUS,
    /* .HWVersion  = */  { {0x02, 0x00}, {0xFF, 0xFF} },
    /* .bLastEntry = */ 0
  },
  {
    /* .nFreqHz    = */  214000 * 1000,
    /* .HALConfig  = */
    {
      /* .eSource        = */  HAL_CLK_SOURCE_XO,
      /* .eVCO           = */  HAL_CLK_PLL_VCO3,
      /* .nPreDiv        = */  1,
      /* .nPostDiv       = */  4,
      /* .nL             = */  44,
      /* .nM             = */  0,
      /* .nN             = */  0,
      /* .nVCOMultiplier = */  0,
      /* .nAlpha         = */  0x55555555,
      /* .nAlphaU        = */  0x95,
    },
    /* .eVRegLevel = */  CLOCK_VREG_LEVEL_LOW_MINUS,
    /* .HWVersion  = */  { {0x02, 0x00}, {0xFF, 0xFF} },
    /* .bLastEntry = */ 0
  },
  {
    /* .nFreqHz    = */  315000 * 1000,
    /* .HALConfig  = */
    {
      /* .eSource        = */  HAL_CLK_SOURCE_XO,
      /* .eVCO           = */  HAL_CLK_PLL_VCO3,
      /* .nPreDiv        = */  1,
      /* .nPostDiv       = */  2,
      /* .nL             = */  32,
      /* .nM             = */  0,
      /* .nN             = */  0,
      /* .nVCOMultiplier = */  0,
      /* .nAlpha         = */  0x00000000,
      /* .nAlphaU        = */  0xD0,
    },
    /* .eVRegLevel = */  CLOCK_VREG_LEVEL_LOW_MINUS,
    /* .HWVersion  = */  { {0x02, 0x00}, {0xFF, 0xFF} },
    /* .bLastEntry = */ 0
  },
  {
    /* .nFreqHz    = */  410000 * 1000,
    /* .HALConfig  = */
    {
      /* .eSource        = */  HAL_CLK_SOURCE_XO,
      /* .eVCO           = */  HAL_CLK_PLL_VCO4,
      /* .nPreDiv        = */  1,
      /* .nPostDiv       = */  1,
      /* .nL             = */  21,
      /* .nM             = */  0,
      /* .nN             = */  0,
      /* .nVCOMultiplier = */  0,
      /* .nAlpha         = */  0xAAAAAAAA,
      /* .nAlphaU        = */  0x5A,
    },
    /* .eVRegLevel = */  CLOCK_VREG_LEVEL_LOW_MINUS,
    /* .HWVersion  = */  { {0x00, 0x00}, {0x02, 0x00} },
    /* .bLastEntry = */  0
  },
  {
    /* .nFreqHz    = */  500000 * 1000,
    /* .HALConfig  = */
    {
      /* .eSource        = */  HAL_CLK_SOURCE_XO,
      /* .eVCO           = */  HAL_CLK_PLL_VCO4,
      /* .nPreDiv        = */  1,
      /* .nPostDiv       = */  1,
      /* .nL             = */  26,
      /* .nM             = */  0,
      /* .nN             = */  0,
      /* .nVCOMultiplier = */  0,
      /* .nAlpha         = */  0xAAAAAAAA,
      /* .nAlphaU        = */  0x0A,
    },
    /* .eVRegLevel = */  CLOCK_VREG_LEVEL_LOW_MINUS,
    /* .HWVersion  = */  { {0x02, 0x00}, {0xFF, 0xFF} },
    /* .bLastEntry = */ 0
  },
  /* last entry */
  { 0 }
};


/*
 *  SourceFreqConfig_MMPLL3
 *
 *  Set of source frequency configurations.
 */
static ClockSourceFreqConfigType SourceFreqConfig_MMPLL3[] =
{
  {
    /* .nFreqHz    = */ 900000 * 1000,
    /* .HALConfig  = */
    {
      /* .eSource        = */ HAL_CLK_SOURCE_XO,
      /* .eVCO           = */ HAL_CLK_PLL_VCO3,
      /* .nPreDiv        = */ 1,
      /* .nPostDiv       = */ 1,
      /* .nL             = */ 46,
      /* .nM             = */ 0,
      /* .nN             = */ 0,
      /* .nVCOMultiplier = */ 0,
      /* .nAlpha         = */ 0x00000000,
      /* .nAlphaU        = */ 0xE0,
    },
    /* .eVRegLevel = */ CLOCK_VREG_LEVEL_LOW_MINUS,
    /* .HWVersion  = */  { {0x00, 0x00}, {0x02, 0x00} },
    /* .bLastEntry = */ 0
  },
  {
    /* .nFreqHz    = */  980000 * 1000,
    /* .HALConfig  = */
    {
      /* .eSource        = */  HAL_CLK_SOURCE_XO,
      /* .eVCO           = */  HAL_CLK_PLL_VCO3,
      /* .nPreDiv        = */  1,
      /* .nPostDiv       = */  1,
      /* .nL             = */  51,
      /* .nM             = */  0,
      /* .nN             = */  0,
      /* .nVCOMultiplier = */  0,
      /* .nAlpha         = */  0xAAAAAAAA,
      /* .nAlphaU        = */  0x0A,
    },
    /* .eVRegLevel = */  CLOCK_VREG_LEVEL_LOW_MINUS,
    /* .HWVersion  = */  { {0x02, 0x00}, {0xFF, 0xFF} },
    /* .bLastEntry = */ 0
  },
  {
    /* .nFreqHz    = */  1032000 * 1000,
    /* .HALConfig  = */
    {
      /* .eSource        = */  HAL_CLK_SOURCE_XO,
      /* .eVCO           = */  HAL_CLK_PLL_VCO2,
      /* .nPreDiv        = */  1,
      /* .nPostDiv       = */  1,
      /* .nL             = */  53,
      /* .nM             = */  0,
      /* .nN             = */  0,
      /* .nVCOMultiplier = */  0,
      /* .nAlpha         = */  0x00000000,
      /* .nAlphaU        = */  0xC0,
    },
    /* .eVRegLevel = */  CLOCK_VREG_LEVEL_LOW,
    /* .HWVersion  = */  { {0x02, 0x00}, {0xFF, 0xFF} },
    /* .bLastEntry = */ 0
  },
  /* last entry */
  { 0 }
};


/*
 *  SourceFreqConfig_MMPLL4
 *
 *  Set of source frequency configurations.
 */
static ClockSourceFreqConfigType SourceFreqConfig_MMPLL4[] =
{
  {
    /* .nFreqHz    = */ 960000 * 1000,
    /* .HALConfig  = */
    {
      /* .eSource        = */ HAL_CLK_SOURCE_XO,
      /* .eVCO           = */ HAL_CLK_PLL_VCO1,
      /* .nPreDiv        = */ 1,
      /* .nPostDiv       = */ 1,
      /* .nL             = */ 50,
      /* .nM             = */ 0,
      /* .nN             = */ 0,
      /* .nVCOMultiplier = */ 0,
      /* .nAlpha         = */ 0x00000000,
      /* .nAlphaU        = */ 0x00,
    },
    /* .eVRegLevel = */ CLOCK_VREG_LEVEL_LOW,
    /* .HWVersion  = */  { {0x00, 0x00}, {0x00, 0x00} },
    /* .bLastEntry = */ 0
  },
  /* last entry */
  { 0 }
};


/*
 *  SourceFreqConfig_MMPLL5
 *
 *  Set of source frequency configurations.
 */
static ClockSourceFreqConfigType SourceFreqConfig_MMPLL5[] =
{
  {
    /* .nFreqHz    = */ 720000 * 1000,
    /* .HALConfig  = */
    {
      /* .eSource        = */ HAL_CLK_SOURCE_XO,
      /* .eVCO           = */ HAL_CLK_PLL_VCO3,
      /* .nPreDiv        = */ 1,
      /* .nPostDiv       = */ 1,
      /* .nL             = */ 37,
      /* .nM             = */ 0,
      /* .nN             = */ 0,
      /* .nVCOMultiplier = */ 0,
      /* .nAlpha         = */ 0x00000000,
      /* .nAlphaU        = */ 0x80,
    },
    /* .eVRegLevel = */ CLOCK_VREG_LEVEL_LOW_MINUS,
    /* .HWVersion  = */  { {0x00, 0x00}, {0x02, 0x00} },
    /* .bLastEntry = */ 0
  },
  {
    /* .nFreqHz    = */  825000 * 1000,
    /* .HALConfig  = */
    {
      /* .eSource        = */  HAL_CLK_SOURCE_XO,
      /* .eVCO           = */  HAL_CLK_PLL_VCO3,
      /* .nPreDiv        = */  1,
      /* .nPostDiv       = */  1,
      /* .nL             = */  42,
      /* .nM             = */  0,
      /* .nN             = */  0,
      /* .nVCOMultiplier = */  0,
      /* .nAlpha         = */  0x00000000,
      /* .nAlphaU        = */  0xF8,
    },
    /* .eVRegLevel = */  CLOCK_VREG_LEVEL_LOW_MINUS,
    /* .HWVersion  = */  { {0x02, 0x00}, {0xFF, 0xFF} },
    /* .bLastEntry = */ 0
  },
  /* last entry */
  { 0 }
};


/*
 *  SourceFreqConfig_MMPLL8
 *
 *  Set of source frequency configurations.
 */
static ClockSourceFreqConfigType SourceFreqConfig_MMPLL8[] =
{
  {
    /* .nFreqHz    = */  125000 * 1000,
    /* .HALConfig  = */
    {
      /* .eSource        = */  HAL_CLK_SOURCE_XO,
      /* .eVCO           = */  HAL_CLK_PLL_VCO4,
      /* .nPreDiv        = */  1,
      /* .nPostDiv       = */  4,
      /* .nL             = */  26,
      /* .nM             = */  0,
      /* .nN             = */  0,
      /* .nVCOMultiplier = */  0,
      /* .nAlpha         = */  0xAAAAAAAA,
      /* .nAlphaU        = */  0x0A,
    },
    /* .eVRegLevel = */  CLOCK_VREG_LEVEL_LOW_MINUS,
    /* .HWVersion  = */  { {0x02, 0x00}, {0xFF, 0xFF} },
    /* .bLastEntry = */  0
  },
  {
    /* .nFreqHz    = */  214000 * 1000,
    /* .HALConfig  = */
    {
      /* .eSource        = */  HAL_CLK_SOURCE_XO,
      /* .eVCO           = */  HAL_CLK_PLL_VCO3,
      /* .nPreDiv        = */  1,
      /* .nPostDiv       = */  4,
      /* .nL             = */  44,
      /* .nM             = */  0,
      /* .nN             = */  0,
      /* .nVCOMultiplier = */  0,
      /* .nAlpha         = */  0x55555555,
      /* .nAlphaU        = */  0x95,
    },
    /* .eVRegLevel = */  CLOCK_VREG_LEVEL_LOW_MINUS,
    /* .HWVersion  = */  { {0x02, 0x00}, {0xFF, 0xFF} },
    /* .bLastEntry = */  0
  },
  {
    /* .nFreqHz    = */  315000 * 1000,
    /* .HALConfig  = */
    {
      /* .eSource        = */  HAL_CLK_SOURCE_XO,
      /* .eVCO           = */  HAL_CLK_PLL_VCO3,
      /* .nPreDiv        = */  1,
      /* .nPostDiv       = */  2,
      /* .nL             = */  32,
      /* .nM             = */  0,
      /* .nN             = */  0,
      /* .nVCOMultiplier = */  0,
      /* .nAlpha         = */  0x00000000,
      /* .nAlphaU        = */  0xD0,
    },
    /* .eVRegLevel = */  CLOCK_VREG_LEVEL_LOW_MINUS,
    /* .HWVersion  = */  { {0x02, 0x00}, {0xFF, 0xFF} },
    /* .bLastEntry = */  0
  },
  {
    /* .nFreqHz    = */ 360000 * 1000,
    /* .HALConfig  = */
    {
      /* .eSource        = */ HAL_CLK_SOURCE_XO,
      /* .eVCO           = */ HAL_CLK_PLL_VCO4,
      /* .nPreDiv        = */ 1,
      /* .nPostDiv       = */ 1,
      /* .nL             = */ 18,
      /* .nM             = */ 0,
      /* .nN             = */ 0,
      /* .nVCOMultiplier = */ 0,
      /* .nAlpha         = */ 0x00000000,
      /* .nAlphaU        = */ 0xC0,
    },
    /* .eVRegLevel = */ CLOCK_VREG_LEVEL_LOW_MINUS,
    /* .HWVersion  = */  { {0x00, 0x00}, {0x02, 0x00} },
    /* .bLastEntry = */  0
  },
  {
    /* .nFreqHz    = */  500000 * 1000,
    /* .HALConfig  = */
    {
      /* .eSource        = */  HAL_CLK_SOURCE_XO,
      /* .eVCO           = */  HAL_CLK_PLL_VCO4,
      /* .nPreDiv        = */  1,
      /* .nPostDiv       = */  1,
      /* .nL             = */  26,
      /* .nM             = */  0,
      /* .nN             = */  0,
      /* .nVCOMultiplier = */  0,
      /* .nAlpha         = */  0xAAAAAAAA,
      /* .nAlphaU        = */  0x0A,
    },
    /* .eVRegLevel = */  CLOCK_VREG_LEVEL_LOW_MINUS,
    /* .HWVersion  = */  { {0x02, 0x00}, {0xFF, 0xFF} },
    /* .bLastEntry = */ 0
  },
  /* last entry */
  { 0 }
};


/*
 *  SourceFreqConfig_MMPLL9
 *
 *  Set of source frequency configurations.
 */
static ClockSourceFreqConfigType SourceFreqConfig_MMPLL9[] =
{
  {
    /* .nFreqHz    = */ 480000 * 1000,
    /* .HALConfig  = */
    {
      /* .eSource        = */ HAL_CLK_SOURCE_XO,
      /* .eVCO           = */ HAL_CLK_PLL_VCO1,
      /* .nPreDiv        = */ 1,
      /* .nPostDiv       = */ 2,
      /* .nL             = */ 50,
      /* .nM             = */ 0,
      /* .nN             = */ 0,
      /* .nVCOMultiplier = */ 0,
      /* .nAlpha         = */ 0x00000000,
      /* .nAlphaU        = */ 0x00,
    },
    /* .eVRegLevel = */ CLOCK_VREG_LEVEL_LOW,
    /* .HWVersion  = */  { {0x00, 0x00}, {0x02, 0x00} },
    /* .bLastEntry = */  0
  },
  {
    /* .nFreqHz    = */  604800 * 1000,
    /* .HALConfig  = */
    {
      /* .eSource        = */  HAL_CLK_SOURCE_XO,
      /* .eVCO           = */  HAL_CLK_PLL_VCO1,
      /* .nPreDiv        = */  1,
      /* .nPostDiv       = */  2,
      /* .nL             = */  63,
      /* .nM             = */  0,
      /* .nN             = */  0,
      /* .nVCOMultiplier = */  0,
      /* .nAlpha         = */  0x00000000,
      /* .nAlphaU        = */  0x00,
    },
    /* .eVRegLevel = */  CLOCK_VREG_LEVEL_NOMINAL,
    /* .HWVersion  = */  { {0x02, 0x00}, {0xFF, 0xFF} },
    /* .bLastEntry = */ 0
  },
  /* last entry */
  { 0 }
};


/*
 *  SourceFreqConfig_APCSPRIPLL0_PLL
 *
 *  Set of source frequency configurations.
 */
static ClockSourceFreqConfigType SourceFreqConfig_APCSPRIPLL0_PLL[] =
{
  {
    /* .nFreqHz    = */ 1766400 * 1000,
    /* .HALConfig  = */
    {
      /* .eSource        = */ HAL_CLK_SOURCE_XO,
      /* .eVCO           = */ HAL_CLK_PLL_VCO1,
      /* .nPreDiv        = */ 1,
      /* .nPostDiv       = */ 1,
      /* .nL             = */ 92,
      /* .nM             = */ 0,
      /* .nN             = */ 0,
      /* .nVCOMultiplier = */ 0,
      /* .nAlpha         = */ 0x00000000,
      /* .nAlphaU        = */ 0x00,
    },
    /* .eVRegLevel = */ CLOCK_VREG_LEVEL_LOW,
    /* .HWVersion  = */ { {0x00, 0x00}, {0x00, 0x00} },
    /* .bLastEntry = */ 0
  },
  /* last entry */
  { 0 }
};


/*
 *  SourceFreqConfig_APCSSECPLL0_PLL
 *
 *  Set of source frequency configurations.
 */
static ClockSourceFreqConfigType SourceFreqConfig_APCSSECPLL0_PLL[] =
{
  {
    /* .nFreqHz    = */ 787200 * 1000,
    /* .HALConfig  = */
    {
      /* .eSource        = */ HAL_CLK_SOURCE_XO,
      /* .eVCO           = */ HAL_CLK_PLL_VCO3,
      /* .nPreDiv        = */ 1,
      /* .nPostDiv       = */ 1,
      /* .nL             = */ 41,
      /* .nM             = */ 0,
      /* .nN             = */ 0,
      /* .nVCOMultiplier = */ 0,
      /* .nAlpha         = */ 0x00000000,
      /* .nAlphaU        = */ 0x00,
    },
    /* .eVRegLevel = */ CLOCK_VREG_LEVEL_LOW,
    /* .HWVersion  = */ { {0x00, 0x00}, {0x00, 0x00} },
    /* .bLastEntry = */ 0
  },
  /* last entry */
  { 0 }
};


/*
 *  SourceFreqConfig_APCSPRIPLL1_PLL
 *
 *  Set of source frequency configurations.
 */
static ClockSourceFreqConfigType SourceFreqConfig_APCSPRIPLL1_PLL[] =
{
  {
    /* .nFreqHz    = */ 1017600 * 1000,
    /* .HALConfig  = */
    {
      /* .eSource        = */ HAL_CLK_SOURCE_XO,
      /* .eVCO           = */ HAL_CLK_PLL_VCO1,
      /* .nPreDiv        = */ 1,
      /* .nPostDiv       = */ 1,
      /* .nL             = */ 53,
      /* .nM             = */ 0,
      /* .nN             = */ 0,
      /* .nVCOMultiplier = */ 0,
      /* .nAlpha         = */ 0x00000000,
      /* .nAlphaU        = */ 0x00,
    },
    /* .eVRegLevel = */ CLOCK_VREG_LEVEL_LOW,
    /* .HWVersion  = */ { {0x00, 0x00}, {0x00, 0x00} },
    /* .bLastEntry = */ 0
  },
  /* last entry */
  { 0 }
};


/*
 *  SourceFreqConfig_APCSSECPLL1_PLL
 *
 *  Set of source frequency configurations.
 */
static ClockSourceFreqConfigType SourceFreqConfig_APCSSECPLL1_PLL[] =
{
  {
    /* .nFreqHz    = */ 921600 * 1000,
    /* .HALConfig  = */
    {
      /* .eSource        = */ HAL_CLK_SOURCE_XO,
      /* .eVCO           = */ HAL_CLK_PLL_VCO3,
      /* .nPreDiv        = */ 1,
      /* .nPostDiv       = */ 1,
      /* .nL             = */ 48,
      /* .nM             = */ 0,
      /* .nN             = */ 0,
      /* .nVCOMultiplier = */ 0,
      /* .nAlpha         = */ 0x00000000,
      /* .nAlphaU        = */ 0x00,
    },
    /* .eVRegLevel = */ CLOCK_VREG_LEVEL_LOW,
    /* .HWVersion  = */ { {0x00, 0x00}, {0x00, 0x00} },
    /* .bLastEntry = */ 0
  },
  /* last entry */
  { 0 }
};


/*
 *  SourceFreqConfig_APCSCBFPLL_PLL
 *
 *  Set of source frequency configurations.
 */
static ClockSourceFreqConfigType SourceFreqConfig_APCSCBFPLL_PLL[] =
{
  {
    /* .nFreqHz    = */ 960000 * 1000,
    /* .HALConfig  = */
    {
      /* .eSource        = */ HAL_CLK_SOURCE_XO,
      /* .eVCO           = */ HAL_CLK_PLL_VCO1,
      /* .nPreDiv        = */ 1,
      /* .nPostDiv       = */ 1,
      /* .nL             = */ 50,
      /* .nM             = */ 0,
      /* .nN             = */ 0,
      /* .nVCOMultiplier = */ 0,
      /* .nAlpha         = */ 0x00000000,
      /* .nAlphaU        = */ 0x00,
    },
    /* .eVRegLevel = */ CLOCK_VREG_LEVEL_LOW,
    /* .HWVersion  = */ { {0x00, 0x00}, {0x00, 0x00} },
    /* .bLastEntry = */ 0
  },
  /* last entry */
  { 0 }
};


/*
 * SourceConfig
 *
 * Clock source configuration data.
 */
const ClockSourceConfigType SourceConfig[] =
{
  {
    SOURCE_NAME(SLEEPCLK),

    /* .nConfigMask             = */ 0,
    /* .pSourceFreqConfig       = */ SourceFreqConfig_SLEEPCLK,
    /* .pCalibrationFreqConfig  = */ NULL,
    /* .eDisableMode            = */ HAL_CLK_SOURCE_DISABLE_MODE_NORMAL
  },
  {
    SOURCE_NAME(XO),

    /* .nConfigMask             = */ 0,
    /* .pSourceFreqConfig       = */ SourceFreqConfig_XO,
    /* .pCalibrationFreqConfig  = */ NULL,
    /* .eDisableMode            = */ HAL_CLK_SOURCE_DISABLE_MODE_NORMAL
  },
  {
    SOURCE_NAME(GPLL0_DIV2),

    /* .nConfigMask             = */ 0,
    /* .pSourceFreqConfig       = */ SourceFreqConfig_GPLL0_DIV2,
    /* .pCalibrationFreqConfig  = */ NULL,
    /* .eDisableMode            = */ HAL_CLK_SOURCE_DISABLE_MODE_NORMAL
  },
  {
    SOURCE_NAME(GPLL0),

    /* .nConfigMask             = */ CLOCK_CONFIG_PLL_FSM_MODE_ENABLE,
    /* .pSourceFreqConfig       = */ SourceFreqConfig_GPLL0,
    /* .pCalibrationFreqConfig  = */ NULL,
    /* .eDisableMode            = */ HAL_CLK_SOURCE_DISABLE_MODE_NORMAL
  },
  {
    SOURCE_NAME(GPLL4),

    /* .nConfigMask             = */ CLOCK_CONFIG_PLL_FSM_MODE_ENABLE,
    /* .pSourceFreqConfig       = */ SourceFreqConfig_GPLL4,
    /* .pCalibrationFreqConfig  = */ NULL,
    /* .eDisableMode            = */ HAL_CLK_SOURCE_DISABLE_MODE_NORMAL
  },
  {
    SOURCE_NAME(MMPLL0),

    /* .nConfigMask             = */ 0,
    /* .pSourceFreqConfig       = */ SourceFreqConfig_MMPLL0,
    /* .pCalibrationFreqConfig  = */ NULL,
    /* .eDisableMode            = */ HAL_CLK_SOURCE_DISABLE_MODE_NORMAL
  },
  {
    SOURCE_NAME(MMPLL1),

    /* .nConfigMask             = */ 0,
    /* .pSourceFreqConfig       = */ SourceFreqConfig_MMPLL1,
    /* .pCalibrationFreqConfig  = */ NULL,
    /* .eDisableMode            = */ HAL_CLK_SOURCE_DISABLE_MODE_NORMAL
  },
  {
    SOURCE_NAME(MMPLL2),

    /* .nConfigMask             = */ 0,
    /* .pSourceFreqConfig       = */ SourceFreqConfig_MMPLL2,
    /* .pCalibrationFreqConfig  = */ NULL,
    /* .eDisableMode            = */ HAL_CLK_SOURCE_DISABLE_MODE_NORMAL
  },
  {
    SOURCE_NAME(MMPLL3),

    /* .nConfigMask             = */ 0,
    /* .pSourceFreqConfig       = */ SourceFreqConfig_MMPLL3,
    /* .pCalibrationFreqConfig  = */ NULL,
    /* .eDisableMode            = */ HAL_CLK_SOURCE_DISABLE_MODE_NORMAL
  },
  {
    SOURCE_NAME(MMPLL4),

    /* .nConfigMask             = */ 0,
    /* .pSourceFreqConfig       = */ SourceFreqConfig_MMPLL4,
    /* .pCalibrationFreqConfig  = */ NULL,
    /* .eDisableMode            = */ HAL_CLK_SOURCE_DISABLE_MODE_NORMAL
  },
  {
    SOURCE_NAME(MMPLL5),

    /* .nConfigMask             = */ 0,
    /* .pSourceFreqConfig       = */ SourceFreqConfig_MMPLL5,
    /* .pCalibrationFreqConfig  = */ NULL,
    /* .eDisableMode            = */ HAL_CLK_SOURCE_DISABLE_MODE_NORMAL
  },
  {
    SOURCE_NAME(MMPLL8),

    /* .nConfigMask             = */ 0,
    /* .pSourceFreqConfig       = */ SourceFreqConfig_MMPLL8,
    /* .pCalibrationFreqConfig  = */ NULL,
    /* .eDisableMode            = */ HAL_CLK_SOURCE_DISABLE_MODE_NORMAL
  },
  {
    SOURCE_NAME(MMPLL9),

    /* .nConfigMask             = */ 0,
    /* .pSourceFreqConfig       = */ SourceFreqConfig_MMPLL9,
    /* .pCalibrationFreqConfig  = */ NULL,
    /* .eDisableMode            = */ HAL_CLK_SOURCE_DISABLE_MODE_NORMAL
  },
  {
    SOURCE_NAME(APCSPRIPLL0),

    /* .nConfigMask             = */ 0,
    /* .pSourceFreqConfig       = */ SourceFreqConfig_APCSPRIPLL0_PLL,
    /* .pCalibrationFreqConfig  = */ NULL,
    /* .eDisableMode            = */ HAL_CLK_SOURCE_DISABLE_MODE_NORMAL
  },
  {
    SOURCE_NAME(APCSSECPLL0),

    /* .nConfigMask             = */ 0,
    /* .pSourceFreqConfig       = */ SourceFreqConfig_APCSSECPLL0_PLL,
    /* .pCalibrationFreqConfig  = */ NULL,
    /* .eDisableMode            = */ HAL_CLK_SOURCE_DISABLE_MODE_NORMAL
  },
  {
    SOURCE_NAME(APCSPRIPLL1),

    /* .nConfigMask             = */ 0,
    /* .pSourceFreqConfig       = */ SourceFreqConfig_APCSPRIPLL1_PLL,
    /* .pCalibrationFreqConfig  = */ NULL,
    /* .eDisableMode            = */ HAL_CLK_SOURCE_DISABLE_MODE_NORMAL
  },
  {
    SOURCE_NAME(APCSSECPLL1),

    /* .nConfigMask             = */ 0,
    /* .pSourceFreqConfig       = */ SourceFreqConfig_APCSSECPLL1_PLL,
    /* .pCalibrationFreqConfig  = */ NULL,
    /* .eDisableMode            = */ HAL_CLK_SOURCE_DISABLE_MODE_NORMAL
  },
  {
    SOURCE_NAME(APCSCBFPLL),

    /* .nConfigMask             = */ 0,
    /* .pSourceFreqConfig       = */ SourceFreqConfig_APCSCBFPLL_PLL,
    /* .pCalibrationFreqConfig  = */ NULL,
    /* .eDisableMode            = */ HAL_CLK_SOURCE_DISABLE_MODE_NORMAL
  },
  /* last entry */
  { HAL_CLK_SOURCE_NULL }
};


/*----------------------------------------------------------------------*/
/* GCC   Clock Configurations                                           */
/*----------------------------------------------------------------------*/


/*
 * XO clock configurations
 */
const ClockMuxConfigType XOClockConfig[] =
{
  {   19200000, { HAL_CLK_SOURCE_XO,             2,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW,         },
  { 0 }
};


/*
 * BLSP1QUP1I2CAPPS clock configurations
 */
const ClockMuxConfigType BLSP1QUP1I2CAPPSClockConfig[] =
{
  {   19200000, { HAL_CLK_SOURCE_XO,             2,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  {   50000000, { HAL_CLK_SOURCE_GPLL0,          24,     0,      0,      0       }, CLOCK_VREG_LEVEL_LOW,         },
  { 0 }
};


/*
 * BLSP1QUP1SPIAPPS clock configurations
 */
const ClockMuxConfigType BLSP1QUP1SPIAPPSClockConfig[] =
{
  {     960000, { HAL_CLK_SOURCE_XO,             20,     1,      2,      2       }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  {    4800000, { HAL_CLK_SOURCE_XO,             8,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  {    9600000, { HAL_CLK_SOURCE_XO,             4,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  {   15000000, { HAL_CLK_SOURCE_GPLL0,          20,     1,      4,      4       }, CLOCK_VREG_LEVEL_LOW,         },
  {   19200000, { HAL_CLK_SOURCE_XO,             2,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  {   25000000, { HAL_CLK_SOURCE_GPLL0,          24,     1,      2,      2       }, CLOCK_VREG_LEVEL_LOW,         },
  {   50000000, { HAL_CLK_SOURCE_GPLL0,          24,     0,      0,      0       }, CLOCK_VREG_LEVEL_NOMINAL,     },
  { 0 }
};


/*
 * BLSP1UART1APPS clock configurations
 */
const ClockMuxConfigType BLSP1UART1APPSClockConfig[] =
{
  {    3686400, { HAL_CLK_SOURCE_GPLL0,          2,      96,     15625,  15625   }, CLOCK_VREG_LEVEL_LOW,         },
  {    7372800, { HAL_CLK_SOURCE_GPLL0,          2,      192,    15625,  15625   }, CLOCK_VREG_LEVEL_LOW,         },
  {   14745600, { HAL_CLK_SOURCE_GPLL0,          2,      384,    15625,  15625   }, CLOCK_VREG_LEVEL_LOW,         },
  {   16000000, { HAL_CLK_SOURCE_GPLL0,          10,     2,      15,     15      }, CLOCK_VREG_LEVEL_LOW,         },
  {   19200000, { HAL_CLK_SOURCE_XO,             2,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  {   24000000, { HAL_CLK_SOURCE_GPLL0,          10,     1,      5,      5       }, CLOCK_VREG_LEVEL_LOW,         },
  {   32000000, { HAL_CLK_SOURCE_GPLL0,          2,      4,      75,     75      }, CLOCK_VREG_LEVEL_NOMINAL,     },
  {   40000000, { HAL_CLK_SOURCE_GPLL0,          30,     0,      0,      0       }, CLOCK_VREG_LEVEL_NOMINAL,     },
  {   46400000, { HAL_CLK_SOURCE_GPLL0,          2,      29,     375,    375     }, CLOCK_VREG_LEVEL_NOMINAL,     },
  {   48000000, { HAL_CLK_SOURCE_GPLL0,          25,     0,      0,      0       }, CLOCK_VREG_LEVEL_NOMINAL,     },
  {   51200000, { HAL_CLK_SOURCE_GPLL0,          2,      32,     375,    375     }, CLOCK_VREG_LEVEL_NOMINAL,     },
  {   56000000, { HAL_CLK_SOURCE_GPLL0,          2,      7,      75,     75      }, CLOCK_VREG_LEVEL_NOMINAL,     },
  {   58982400, { HAL_CLK_SOURCE_GPLL0,          2,      1536,   15625,  15625   }, CLOCK_VREG_LEVEL_NOMINAL,     },
  {   60000000, { HAL_CLK_SOURCE_GPLL0,          20,     0,      0,      0       }, CLOCK_VREG_LEVEL_NOMINAL,     },
  {   63157895, { HAL_CLK_SOURCE_GPLL0,          19,     0,      0,      0       }, CLOCK_VREG_LEVEL_NOMINAL,     },
  { 0 }
};


/*
 * GP1 clock configurations
 */
const ClockMuxConfigType GP1ClockConfig[] =
{
  {   19200000, { HAL_CLK_SOURCE_XO,             2,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  {  100000000, { HAL_CLK_SOURCE_GPLL0,          12,     0,      0,      0       }, CLOCK_VREG_LEVEL_LOW,         },
  {  200000000, { HAL_CLK_SOURCE_GPLL0,          6,      0,      0,      0       }, CLOCK_VREG_LEVEL_NOMINAL,     },
  { 0 }
};


/*
 * HMSSAHB clock configurations
 */
const ClockMuxConfigType HMSSAHBClockConfig[] =
{
  {   19200000, { HAL_CLK_SOURCE_XO,             2,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  {   37500000, { HAL_CLK_SOURCE_GPLL0,          32,     0,      0,      0       }, CLOCK_VREG_LEVEL_LOW,         },
  {   75000000, { HAL_CLK_SOURCE_GPLL0,          16,     0,      0,      0       }, CLOCK_VREG_LEVEL_NOMINAL,     },
  { 0 }
};


/*
 * HMSSRBCPR clock configurations
 */
const ClockMuxConfigType HMSSRBCPRClockConfig[] =
{
  {   19200000, { HAL_CLK_SOURCE_XO,             2,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  { 0 }
};


/*
 * PCIEAUX clock configurations
 */
const ClockMuxConfigType PCIEAUXClockConfig[] =
{
  {    1010526, { HAL_CLK_SOURCE_XO,             2,      1,      19,     19      }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  { 0 }
};


/*
 * PDM2 clock configurations
 */
const ClockMuxConfigType PDM2ClockConfig[] =
{
  {   60000000, { HAL_CLK_SOURCE_GPLL0,          20,     0,      0,      0       }, CLOCK_VREG_LEVEL_LOW,         },
  { 0 }
};


/*
 * SDCC1APPS clock configurations
 */
const ClockMuxConfigType SDCC1APPSClockConfig[] =
{
  {     144000, { HAL_CLK_SOURCE_XO,             32,     3,      25,     25      }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  {     400000, { HAL_CLK_SOURCE_XO,             24,     1,      4,      4       }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  {   20000000, { HAL_CLK_SOURCE_GPLL0,          30,     1,      2,      2       }, CLOCK_VREG_LEVEL_LOW,         },
  {   25000000, { HAL_CLK_SOURCE_GPLL0,          24,     1,      2,      2       }, CLOCK_VREG_LEVEL_LOW,         },
  {   50000000, { HAL_CLK_SOURCE_GPLL0,          24,     0,      0,      0       }, CLOCK_VREG_LEVEL_LOW,         },
  {   96000000, { HAL_CLK_SOURCE_GPLL4,          8,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW,         { {0x00, 0x00}, {0x02, 0x00} }},
  {   96000000, { HAL_CLK_SOURCE_GPLL4,          8,      0,      0,      0       }, CLOCK_VREG_LEVEL_NOMINAL,     { {0x02, 0x00}, {0xFF, 0xFF} }},
  {  192000000, { HAL_CLK_SOURCE_GPLL4,          4,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW,         { {0x00, 0x00}, {0x02, 0x00} }},
  {  192000000, { HAL_CLK_SOURCE_GPLL4,          4,      0,      0,      0       }, CLOCK_VREG_LEVEL_NOMINAL,     { {0x02, 0x00}, {0xFF, 0xFF} }},
  {  384000000, { HAL_CLK_SOURCE_GPLL4,          2,      0,      0,      0       }, CLOCK_VREG_LEVEL_NOMINAL,     },
  { 0 }
};


/*
 * SDCC2APPS clock configurations
 */
const ClockMuxConfigType SDCC2APPSClockConfig[] =
{
  {     144000, { HAL_CLK_SOURCE_XO,             32,     3,      25,     25      }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  {     400000, { HAL_CLK_SOURCE_XO,             24,     1,      4,      4       }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  {   20000000, { HAL_CLK_SOURCE_GPLL0,          30,     1,      2,      2       }, CLOCK_VREG_LEVEL_LOW,         },
  {   25000000, { HAL_CLK_SOURCE_GPLL0,          24,     1,      2,      2       }, CLOCK_VREG_LEVEL_LOW,         },
  {   50000000, { HAL_CLK_SOURCE_GPLL0,          24,     0,      0,      0       }, CLOCK_VREG_LEVEL_LOW,         },
  {  100000000, { HAL_CLK_SOURCE_GPLL0,          12,     0,      0,      0       }, CLOCK_VREG_LEVEL_LOW,         { {0x00, 0x00}, {0x02, 0x00} }},
  {  100000000, { HAL_CLK_SOURCE_GPLL0,          12,     0,      0,      0       }, CLOCK_VREG_LEVEL_NOMINAL,     { {0x02, 0x00}, {0xFF, 0xFF} }},
  {  200000000, { HAL_CLK_SOURCE_GPLL0,          6,      0,      0,      0       }, CLOCK_VREG_LEVEL_NOMINAL,     },
  { 0 }
};


/*
 * SDCC4APPS clock configurations
 */
const ClockMuxConfigType SDCC4APPSClockConfig[] =
{
  {     144000, { HAL_CLK_SOURCE_XO,             32,     3,      25,     25      }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  {     400000, { HAL_CLK_SOURCE_XO,             24,     1,      4,      4       }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  {   20000000, { HAL_CLK_SOURCE_GPLL0,          30,     1,      2,      2       }, CLOCK_VREG_LEVEL_LOW,         },
  {   25000000, { HAL_CLK_SOURCE_GPLL0,          24,     1,      2,      2       }, CLOCK_VREG_LEVEL_LOW,         },
  {   50000000, { HAL_CLK_SOURCE_GPLL0,          24,     0,      0,      0       }, CLOCK_VREG_LEVEL_LOW,         },
  {  100000000, { HAL_CLK_SOURCE_GPLL0,          12,     0,      0,      0       }, CLOCK_VREG_LEVEL_NOMINAL,     },
  { 0 }
};


/*
 * TSIFREF clock configurations
 */
const ClockMuxConfigType TSIFREFClockConfig[] =
{
  {     105495, { HAL_CLK_SOURCE_XO,             2,      1,      182,    182     }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  {     105495, { HAL_CLK_SOURCE_EXTERNAL,       2,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  { 0 }
};


/*
 * UFSAXI clock configurations
 */
const ClockMuxConfigType UFSAXIClockConfig[] =
{
  {  100000000, { HAL_CLK_SOURCE_GPLL0,          12,     0,      0,      0       }, CLOCK_VREG_LEVEL_LOW,         },
  {  200000000, { HAL_CLK_SOURCE_GPLL0,          6,      0,      0,      0       }, CLOCK_VREG_LEVEL_NOMINAL,     },
  {  240000000, { HAL_CLK_SOURCE_GPLL0,          5,      0,      0,      0       }, CLOCK_VREG_LEVEL_HIGH,        },
  { 0 }
};


/*
 * USB20MASTER clock configurations
 */
const ClockMuxConfigType USB20MASTERClockConfig[] =
{
  {  120000000, { HAL_CLK_SOURCE_GPLL0,          10,     0,      0,      0       }, CLOCK_VREG_LEVEL_NOMINAL,     },
  { 0 }
};


/*
 * USB20MOCKUTMI clock configurations
 */
const ClockMuxConfigType USB20MOCKUTMIClockConfig[] =
{
  {   19200000, { HAL_CLK_SOURCE_XO,             2,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  { 0 }
};


/*
 * USB30MASTER clock configurations
 */
const ClockMuxConfigType USB30MASTERClockConfig[] =
{
  {   19200000, { HAL_CLK_SOURCE_XO,             2,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  {  120000000, { HAL_CLK_SOURCE_GPLL0,          10,     0,      0,      0       }, CLOCK_VREG_LEVEL_LOW,         },
  {  150000000, { HAL_CLK_SOURCE_GPLL0,          8,      0,      0,      0       }, CLOCK_VREG_LEVEL_NOMINAL,     },
  { 0 }
};


/*
 * USB3PHYAUX clock configurations
 */
const ClockMuxConfigType USB3PHYAUXClockConfig[] =
{
  {    1200000, { HAL_CLK_SOURCE_XO,             32,     0,      0,      0       }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  { 0 }
};


/*
 * USB3PHYPIPE clock configurations
 */
const ClockMuxConfigType USB3PHYPIPEClockConfig[] =
{
  {   19200000, { HAL_CLK_SOURCE_XO,             2,     0,      0,      0       }, CLOCK_VREG_LEVEL_LOW_MINUS, },
  {   62500000, { HAL_CLK_SOURCE_EXTERNAL,       2,     0,      0,      0       }, CLOCK_VREG_LEVEL_LOW_MINUS, },
  {  125000000, { HAL_CLK_SOURCE_EXTERNAL,       2,     0,      0,      0       }, CLOCK_VREG_LEVEL_LOW,       },
  { 0 }
};



/*----------------------------------------------------------------------*/
/* MMSS  Clock Configurations                                           */
/*----------------------------------------------------------------------*/


/*
 * AHB clock configurations
 */
const ClockMuxConfigType AHBClockConfig[] =
{
  {   19200000, { HAL_CLK_SOURCE_XO,             2,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  {   40000000, { HAL_CLK_SOURCE_GPLL0_DIV2,     15,     0,      0,      0       }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  {   80000000, { HAL_CLK_SOURCE_MMPLL0,         20,     0,      0,      0       }, CLOCK_VREG_LEVEL_NOMINAL,     },
  { 0 }
};


/*
 * AXI clock configurations
 */
const ClockMuxConfigType AXIClockConfig[] =
{
  {   75000000, { HAL_CLK_SOURCE_GPLL0_DIV2,     8,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  {  100000000, { HAL_CLK_SOURCE_GPLL0,         12,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW,         { {0x02, 0x00}, {0xFF, 0xFF} }},
  {  171428571, { HAL_CLK_SOURCE_GPLL0,          7,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW,         { {0x00, 0x00}, {0x02, 0x00} }},
  {  200000000, { HAL_CLK_SOURCE_GPLL0,          6,      0,      0,      0       }, CLOCK_VREG_LEVEL_NOMINAL,     },
  {  320000000, { HAL_CLK_SOURCE_MMPLL0,         5,      0,      0,      0       }, CLOCK_VREG_LEVEL_NOMINAL,     },
  {  370000000, { HAL_CLK_SOURCE_MMPLL1,         4,      0,      0,      0       }, CLOCK_VREG_LEVEL_HIGH,        { {0x00, 0x00}, {0x02, 0x00} }},
  {  400000000, { HAL_CLK_SOURCE_MMPLL0,         4,      0,      0,      0       }, CLOCK_VREG_LEVEL_HIGH,        { {0x02, 0x00}, {0xFF, 0xFF} }},
  { 0 }
};


/*
 * BYTE0 clock configurations
 */
const ClockMuxConfigType BYTE0ClockConfig[] =
{
  {   19200000, { HAL_CLK_SOURCE_XO,             2,     0,       0,      0       }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  {  131250000, { HAL_CLK_SOURCE_EXTERNAL,       2,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  {  210000000, { HAL_CLK_SOURCE_EXTERNAL,       2,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW,         },
  {  262500000, { HAL_CLK_SOURCE_EXTERNAL,       2,      0,      0,      0       }, CLOCK_VREG_LEVEL_NOMINAL,     },
  { 0 }
};


/*
 * CAMSSGP0 clock configurations
 */
const ClockMuxConfigType CAMSSGP0ClockConfig[] =
{
  {      10000, { HAL_CLK_SOURCE_XO,             32,     1,      120,    120     }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  {      24000, { HAL_CLK_SOURCE_XO,             32,     1,      50,     50      }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  {    6000000, { HAL_CLK_SOURCE_GPLL0_DIV2,     20,     1,      5,      5       }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  {   12000000, { HAL_CLK_SOURCE_GPLL0_DIV2,     2,      1,      25,     25      }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  {   13000000, { HAL_CLK_SOURCE_GPLL0_DIV2,     4,      13,     150,    150     }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  {   24000000, { HAL_CLK_SOURCE_GPLL0_DIV2,     2,      2,      25,     25      }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  { 0 }
};


/*
 * CCI clock configurations
 */
const ClockMuxConfigType CCIClockConfig[] =
{
  {   19200000, { HAL_CLK_SOURCE_XO,             2,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  {   50000000, { HAL_CLK_SOURCE_GPLL0,          24,     0,      0,      0       }, CLOCK_VREG_LEVEL_LOW,         },
  {  100000000, { HAL_CLK_SOURCE_GPLL0,          12,     0,      0,      0       }, CLOCK_VREG_LEVEL_NOMINAL,     },
  { 0 }
};


/*
 * CPP clock configurations
 */
const ClockMuxConfigType CPPClockConfig[] =
{
  {  100000000, { HAL_CLK_SOURCE_GPLL0_DIV2,     6,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  {  200000000, { HAL_CLK_SOURCE_GPLL0,          6,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW,         },
  {  320000000, { HAL_CLK_SOURCE_MMPLL0,         5,      0,      0,      0       }, CLOCK_VREG_LEVEL_NOMINAL,     },
  {  480000000, { HAL_CLK_SOURCE_MMPLL4,         4,      0,      0,      0       }, CLOCK_VREG_LEVEL_NOMINAL,     },
  {  640000000, { HAL_CLK_SOURCE_MMPLL4,         3,      0,      0,      0       }, CLOCK_VREG_LEVEL_HIGH,        },
  { 0 }
};


/*
 * CSI0 clock configurations
 */
const ClockMuxConfigType CSI0ClockConfig[] =
{
  {  100000000, { HAL_CLK_SOURCE_GPLL0_DIV2,     6,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  {  200000000, { HAL_CLK_SOURCE_GPLL0,          6,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW,         },
  {  274285714, { HAL_CLK_SOURCE_MMPLL4,         7,      0,      0,      0       }, CLOCK_VREG_LEVEL_NOMINAL,     { {0x00, 0x00}, {0x02, 0x00} }},
  {  320000000, { HAL_CLK_SOURCE_MMPLL4,         6,      0,      0,      0       }, CLOCK_VREG_LEVEL_NOMINAL,     { {0x00, 0x00}, {0x02, 0x00} }},
  {  480000000, { HAL_CLK_SOURCE_MMPLL4,         4,      0,      0,      0       }, CLOCK_VREG_LEVEL_NOMINAL,     },
  {  600000000, { HAL_CLK_SOURCE_GPLL0,          2,      0,      0,      0       }, CLOCK_VREG_LEVEL_HIGH,        },
  { 0 }
};


/*
 * CSI0PHYTIMER clock configurations
 */
const ClockMuxConfigType CSI0PHYTIMERClockConfig[] =
{
  {  100000000, { HAL_CLK_SOURCE_GPLL0_DIV2,     6,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  {  200000000, { HAL_CLK_SOURCE_MMPLL0,         8,      0,      0,      0       }, CLOCK_VREG_LEVEL_NOMINAL,     },
  {  266666667, { HAL_CLK_SOURCE_MMPLL0,         6,      0,      0,      0       }, CLOCK_VREG_LEVEL_NOMINAL,     },
  { 0 }
};


/*
 * CSIPHY03P clock configurations
 */
const ClockMuxConfigType CSIPHY03PClockConfig[] =
{
  {  100000000, { HAL_CLK_SOURCE_GPLL0_DIV2,     6,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  {  200000000, { HAL_CLK_SOURCE_GPLL0,          6,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW,         },
  {  320000000, { HAL_CLK_SOURCE_MMPLL4,         6,      0,      0,      0       }, CLOCK_VREG_LEVEL_NOMINAL,     },
  {  384000000, { HAL_CLK_SOURCE_MMPLL4,         5,      0,      0,      0       }, CLOCK_VREG_LEVEL_HIGH,        { {0x02, 0x00}, {0xFF, 0xFF} }},
  { 0 }
};


/*
 * DSACORE clock configurations
 */
const ClockMuxConfigType DSACOREClockConfig[] =
{
  {  300000000, { HAL_CLK_SOURCE_GPLL0_DIV2,     2,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  { 0 }
};


/*
 * EDPGTC clock configurations
 */
const ClockMuxConfigType EDPGTCClockConfig[] =
{
  {  300000000, { HAL_CLK_SOURCE_GPLL0_DIV2,     2,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  {  300000000, { HAL_CLK_SOURCE_EXTERNAL,       2,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW_MINUS,   { {0x00, 0x00}, {0x02, 0x00} }},
  { 0 }
};


/*
 * EDPLINK clock configurations
 */
const ClockMuxConfigType EDPLINKClockConfig[] =
{
  {  162000000, { HAL_CLK_SOURCE_EXTERNAL,       2,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  {  270000000, { HAL_CLK_SOURCE_EXTERNAL,       2,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW,         },
  {  540000000, { HAL_CLK_SOURCE_EXTERNAL,       2,      0,      0,      0       }, CLOCK_VREG_LEVEL_NOMINAL,     },
  { 0 }
};


/*
 * EDPPIXEL clock configurations
 */
const ClockMuxConfigType EDPPIXELClockConfig[] =
{
  {  168750000, { HAL_CLK_SOURCE_EXTERNAL,       2,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  {  337500000, { HAL_CLK_SOURCE_EXTERNAL,       2,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW,         },
  {  675000000, { HAL_CLK_SOURCE_EXTERNAL,       2,      0,      0,      0       }, CLOCK_VREG_LEVEL_NOMINAL,     },
  { 0 }
};


/*
 * ESC0 clock configurations
 */
const ClockMuxConfigType ESC0ClockConfig[] =
{
  {   19200000, { HAL_CLK_SOURCE_XO,             2,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  { 0 }
};


/*
 * EXTPCLK clock configurations
 */
const ClockMuxConfigType EXTPCLKClockConfig[] =
{
  {  150000000, { HAL_CLK_SOURCE_EXTERNAL,       2,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  {  300000000, { HAL_CLK_SOURCE_EXTERNAL,       2,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW,         },
  {  600000000, { HAL_CLK_SOURCE_EXTERNAL,       2,      0,      0,      0       }, CLOCK_VREG_LEVEL_NOMINAL,     },
  { 0 }
};


/*
 * FDCORE clock configurations
 */
const ClockMuxConfigType FDCOREClockConfig[] =
{
  {  100000000, { HAL_CLK_SOURCE_GPLL0_DIV2,     6,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  {  200000000, { HAL_CLK_SOURCE_GPLL0,          6,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW,         },
  {  400000000, { HAL_CLK_SOURCE_MMPLL0,         4,      0,      0,      0       }, CLOCK_VREG_LEVEL_NOMINAL,     },
  { 0 }
};


/*
 * GFX3D clock configurations
 */
const ClockMuxConfigType GFX3DClockConfig[] =
{
  {   19200000, { HAL_CLK_SOURCE_XO,             2,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW_MINUS,   { {0x00, 0x00}, {0x00, 0x00} }},
  {   60000000, { HAL_CLK_SOURCE_MMPLL8,         12,     0,      0,      0       }, CLOCK_VREG_LEVEL_LOW_MINUS,   { {0x00, 0x00}, {0x02, 0x00} }},
  {  120000000, { HAL_CLK_SOURCE_MMPLL8,         6,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW_MINUS,   { {0x00, 0x00}, {0x02, 0x00} }},
  {  125000000, { HAL_CLK_SOURCE_MMPLL2,         2,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW_MINUS,   { {0x02, 0x00}, {0xFF, 0xFF} }, &SourceFreqConfig_MMPLL2[0] /* 125.0 MHz */ },
  {  200000000, { HAL_CLK_SOURCE_GPLL0,          6,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW,         { {0x02, 0x00}, {0xFF, 0xFF} }},
  {  205000000, { HAL_CLK_SOURCE_MMPLL2,         4,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW,         { {0x00, 0x00}, {0x02, 0x00} }},
  {  214000000, { HAL_CLK_SOURCE_MMPLL2,         2,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW_MINUS,   { {0x02, 0x00}, {0xFF, 0xFF} }, &SourceFreqConfig_MMPLL2[1] /* 214.0 MHz */ },
  {  315000000, { HAL_CLK_SOURCE_MMPLL2,         2,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW,         { {0x02, 0x00}, {0xFF, 0xFF} }, &SourceFreqConfig_MMPLL2[2] /* 315.0 MHz */ },
  {  360000000, { HAL_CLK_SOURCE_MMPLL8,         2,      0,      0,      0       }, CLOCK_VREG_LEVEL_NOMINAL,     { {0x00, 0x00}, {0x02, 0x00} }},
  {  480000000, { HAL_CLK_SOURCE_MMPLL9,         2,      0,      0,      0       }, CLOCK_VREG_LEVEL_HIGH,        { {0x00, 0x00}, {0x02, 0x00} }},
  {  500000000, { HAL_CLK_SOURCE_MMPLL8,         2,      0,      0,      0       }, CLOCK_VREG_LEVEL_NOMINAL,     { {0x02, 0x00}, {0xFF, 0xFF} }, &SourceFreqConfig_MMPLL8[4] /* 500.0 MHz */ },
  {  604800000, { HAL_CLK_SOURCE_MMPLL9,         2,      0,      0,      0       }, CLOCK_VREG_LEVEL_HIGH,        { {0x02, 0x00}, {0xFF, 0xFF} }},
  { 0 }
};


/*
 * ISENSE clock configurations
 */
const ClockMuxConfigType ISENSEClockConfig[] =
{
  {   19200000, { HAL_CLK_SOURCE_XO,             2,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW_MINUS,   { {0x00, 0x00}, {0x00, 0x00} }},
  {  125000000, { HAL_CLK_SOURCE_MMPLL2,         2,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW,         { {0x02, 0x00}, {0xFF, 0xFF} }, &SourceFreqConfig_MMPLL2[0] /* 125.0 MHz */ },
  {  150000000, { HAL_CLK_SOURCE_GPLL0_DIV2,     4,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW_MINUS,   { {0x00, 0x00}, {0x02, 0x00} }},
  {  200000000, { HAL_CLK_SOURCE_GPLL0,          6,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW,         { {0x02, 0x00}, {0xFF, 0xFF} }},
  {  214000000, { HAL_CLK_SOURCE_MMPLL2,         2,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW,         { {0x02, 0x00}, {0xFF, 0xFF} }, &SourceFreqConfig_MMPLL2[1] /* 214.0 MHz */ },
  {  315000000, { HAL_CLK_SOURCE_MMPLL2,         2,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW,         { {0x02, 0x00}, {0xFF, 0xFF} }, &SourceFreqConfig_MMPLL2[2] /* 315.0 MHz */ },
  {  320000000, { HAL_CLK_SOURCE_MMPLL0,         5,      0,      0,      0       }, CLOCK_VREG_LEVEL_NOMINAL,     { {0x00, 0x00}, {0x02, 0x00} }},
  {  360000000, { HAL_CLK_SOURCE_MMPLL8,         2,      0,      0,      0       }, CLOCK_VREG_LEVEL_HIGH,        { {0x00, 0x00}, {0x02, 0x00} }},
  {  500000000, { HAL_CLK_SOURCE_MMPLL8,         2,      0,      0,      0       }, CLOCK_VREG_LEVEL_NOMINAL,     { {0x02, 0x00}, {0xFF, 0xFF} }, &SourceFreqConfig_MMPLL8[4] /* 500.0 MHz */ },
  {  604800000, { HAL_CLK_SOURCE_MMPLL9,         2,      0,      0,      0       }, CLOCK_VREG_LEVEL_HIGH,        { {0x02, 0x00}, {0xFF, 0xFF} }},
  { 0 }
};


/*
 * JPEG0 clock configurations
 */
const ClockMuxConfigType JPEG0ClockConfig[] =
{
  {   75000000, { HAL_CLK_SOURCE_GPLL0_DIV2,     8,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  {  150000000, { HAL_CLK_SOURCE_GPLL0,          8,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW,         },
  {  228571429, { HAL_CLK_SOURCE_MMPLL0,         7,      0,      0,      0       }, CLOCK_VREG_LEVEL_NOMINAL,     },
  {  266666667, { HAL_CLK_SOURCE_MMPLL0,         6,      0,      0,      0       }, CLOCK_VREG_LEVEL_NOMINAL,     },
  {  320000000, { HAL_CLK_SOURCE_MMPLL0,         5,      0,      0,      0       }, CLOCK_VREG_LEVEL_NOMINAL,     },
  {  480000000, { HAL_CLK_SOURCE_MMPLL4,         4,      0,      0,      0       }, CLOCK_VREG_LEVEL_HIGH,        },
  { 0 }
};


/*
 * JPEG2 clock configurations
 */
const ClockMuxConfigType JPEG2ClockConfig[] =
{
  {   75000000, { HAL_CLK_SOURCE_GPLL0_DIV2,     8,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  {  150000000, { HAL_CLK_SOURCE_GPLL0,          8,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW,         },
  {  228571429, { HAL_CLK_SOURCE_MMPLL0,         7,      0,      0,      0       }, CLOCK_VREG_LEVEL_NOMINAL,     },
  {  266666667, { HAL_CLK_SOURCE_MMPLL0,         6,      0,      0,      0       }, CLOCK_VREG_LEVEL_NOMINAL,     },
  {  320000000, { HAL_CLK_SOURCE_MMPLL0,         5,      0,      0,      0       }, CLOCK_VREG_LEVEL_HIGH,        },
  { 0 }
};


/*
 * MAXI clock configurations
 */
const ClockMuxConfigType MAXIClockConfig[] =
{
  {   19200000, { HAL_CLK_SOURCE_XO,             2,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  {   75000000, { HAL_CLK_SOURCE_GPLL0_DIV2,     8,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  {  100000000, { HAL_CLK_SOURCE_GPLL0,          12,     0,      0,      0       }, CLOCK_VREG_LEVEL_LOW,         },
  {  200000000, { HAL_CLK_SOURCE_GPLL0,          6,      0,      0,      0       }, CLOCK_VREG_LEVEL_NOMINAL,     },
  {  320000000, { HAL_CLK_SOURCE_MMPLL0,         5,      0,      0,      0       }, CLOCK_VREG_LEVEL_NOMINAL,     },
  {  370000000, { HAL_CLK_SOURCE_MMPLL1,         4,      0,      0,      0       }, CLOCK_VREG_LEVEL_HIGH,        { {0x00, 0x00}, {0x02, 0x00} }},
  {  400000000, { HAL_CLK_SOURCE_MMPLL0,         4,      0,      0,      0       }, CLOCK_VREG_LEVEL_HIGH,        { {0x02, 0x00}, {0xFF, 0xFF} }},
  { 0 }
};


/*
 * MCLK0 clock configurations
 */
const ClockMuxConfigType MCLK0ClockConfig[] =
{
  {    4800000, { HAL_CLK_SOURCE_XO,             8,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  {    6000000, { HAL_CLK_SOURCE_GPLL0_DIV2,     20,     1,      5,      5       }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  {    8000000, { HAL_CLK_SOURCE_GPLL0_DIV2,     2,      2,      75,     75      }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  {    9600000, { HAL_CLK_SOURCE_XO,             4,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  {   16666667, { HAL_CLK_SOURCE_GPLL0_DIV2,     4,      1,      9,      9       }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  {   19200000, { HAL_CLK_SOURCE_XO,             2,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  {   24000000, { HAL_CLK_SOURCE_GPLL0_DIV2,     2,      2,      25,     25      }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  {   33333333, { HAL_CLK_SOURCE_GPLL0_DIV2,     2,      1,      9,      9       }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  {   48000000, { HAL_CLK_SOURCE_GPLL0,          2,      2,      25,     25      }, CLOCK_VREG_LEVEL_LOW,         },
  {   66666667, { HAL_CLK_SOURCE_GPLL0,          2,      1,      9,      9       }, CLOCK_VREG_LEVEL_LOW,         },
  { 0 }
};


/*
 * MDP clock configurations
 */
const ClockMuxConfigType MDPClockConfig[] =
{
  {   85714286, { HAL_CLK_SOURCE_GPLL0,          14,     0,      0,      0       }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  {  100000000, { HAL_CLK_SOURCE_GPLL0,          12,     0,      0,      0       }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  {  150000000, { HAL_CLK_SOURCE_GPLL0,          8,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  {  171428571, { HAL_CLK_SOURCE_GPLL0,          7,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  {  200000000, { HAL_CLK_SOURCE_GPLL0,          6,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW,         { {0x02, 0x00}, {0xFF, 0xFF} }},
  {  240000000, { HAL_CLK_SOURCE_MMPLL5,         6,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW,         { {0x00, 0x00}, {0x02, 0x00} }},
  {  275000000, { HAL_CLK_SOURCE_MMPLL5,         6,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW,         { {0x02, 0x00}, {0xFF, 0xFF} }},
  {  300000000, { HAL_CLK_SOURCE_GPLL0,          4,      0,      0,      0       }, CLOCK_VREG_LEVEL_NOMINAL,     { {0x02, 0x00}, {0xFF, 0xFF} }},
  {  320000000, { HAL_CLK_SOURCE_MMPLL0,         5,      0,      0,      0       }, CLOCK_VREG_LEVEL_NOMINAL,     { {0x00, 0x00}, {0x02, 0x00} }},
  {  330000000, { HAL_CLK_SOURCE_MMPLL5,         5,      0,      0,      0       }, CLOCK_VREG_LEVEL_NOMINAL,     { {0x02, 0x00}, {0xFF, 0xFF} }},
  {  360000000, { HAL_CLK_SOURCE_MMPLL5,         4,      0,      0,      0       }, CLOCK_VREG_LEVEL_HIGH,        { {0x00, 0x00}, {0x02, 0x00} }},
  {  412500000, { HAL_CLK_SOURCE_MMPLL5,         4,      0,      0,      0       }, CLOCK_VREG_LEVEL_HIGH,        { {0x02, 0x00}, {0xFF, 0xFF} }},
  { 0 }
};


/*
 * PCLK0 clock configurations
 */
const ClockMuxConfigType PCLK0ClockConfig[] =
{
  {   19200000, { HAL_CLK_SOURCE_XO,             2,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  {  175000000, { HAL_CLK_SOURCE_EXTERNAL,       2,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  {  280000000, { HAL_CLK_SOURCE_EXTERNAL,       2,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW,         },
  {  350000000, { HAL_CLK_SOURCE_EXTERNAL,       2,      0,      0,      0       }, CLOCK_VREG_LEVEL_NOMINAL,     },
  { 0 }
};


/*
 * RBCPR clock configurations
 */
const ClockMuxConfigType RBCPRClockConfig[] =
{
  {   19200000, { HAL_CLK_SOURCE_XO,             2,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  {   50000000, { HAL_CLK_SOURCE_GPLL0,          24,     0,      0,      0       }, CLOCK_VREG_LEVEL_NOMINAL,     { {0x02, 0x00}, {0xFF, 0xFF} }},
  { 0 }
};


/*
 * VFE0 clock configurations
 */
const ClockMuxConfigType VFE0ClockConfig[] =
{
  {   75000000, { HAL_CLK_SOURCE_GPLL0_DIV2,     8,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  {  100000000, { HAL_CLK_SOURCE_GPLL0_DIV2,     6,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW,         { {0x00, 0x00}, {0x02, 0x00} }},
  {  100000000, { HAL_CLK_SOURCE_GPLL0_DIV2,     6,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW_MINUS,   { {0x02, 0x00}, {0xFF, 0xFF} }},
  {  200000000, { HAL_CLK_SOURCE_GPLL0,          6,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW,         },
  {  320000000, { HAL_CLK_SOURCE_MMPLL0,         5,      0,      0,      0       }, CLOCK_VREG_LEVEL_NOMINAL,     },
  {  400000000, { HAL_CLK_SOURCE_MMPLL0,         4,      0,      0,      0       }, CLOCK_VREG_LEVEL_NOMINAL,     { {0x00, 0x00}, {0x02, 0x00} }},
  {  480000000, { HAL_CLK_SOURCE_MMPLL4,         4,      0,      0,      0       }, CLOCK_VREG_LEVEL_NOMINAL,     },
  {  600000000, { HAL_CLK_SOURCE_GPLL0,          2,      0,      0,      0       }, CLOCK_VREG_LEVEL_HIGH,        },
  { 0 }
};


/*
 * VIDEOCORE clock configurations
 */
const ClockMuxConfigType VIDEOCOREClockConfig[] =
{
  {   75000000, { HAL_CLK_SOURCE_GPLL0_DIV2,     8,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  {  150000000, { HAL_CLK_SOURCE_GPLL0,          8,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW,         },
  {  320000000, { HAL_CLK_SOURCE_MMPLL0,         5,      0,      0,      0       }, CLOCK_VREG_LEVEL_NOMINAL,     },
  {  450000000, { HAL_CLK_SOURCE_MMPLL3,         4,      0,      0,      0       }, CLOCK_VREG_LEVEL_HIGH,        { {0x00, 0x00}, {0x02, 0x00} }},
  {  490000000, { HAL_CLK_SOURCE_MMPLL3,         4,      0,      0,      0       }, CLOCK_VREG_LEVEL_HIGH,        { {0x02, 0x00}, {0xFF, 0xFF} }, &SourceFreqConfig_MMPLL3[1] /* 980.0 MHz */ },
  {  516000000, { HAL_CLK_SOURCE_MMPLL3,         4,      0,      0,      0       }, CLOCK_VREG_LEVEL_HIGH,        { {0x02, 0x00}, {0xFF, 0xFF} }, &SourceFreqConfig_MMPLL3[2] /* 1032.0 MHz */ },
  { 0 }
};


/*
 * VIDEOSUBCORE0 clock configurations
 */
const ClockMuxConfigType VIDEOSUBCORE0ClockConfig[] =
{
  {   75000000, { HAL_CLK_SOURCE_GPLL0_DIV2,     8,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW_MINUS,   },
  {  150000000, { HAL_CLK_SOURCE_GPLL0,          8,      0,      0,      0       }, CLOCK_VREG_LEVEL_LOW,         },
  {  320000000, { HAL_CLK_SOURCE_MMPLL0,         5,      0,      0,      0       }, CLOCK_VREG_LEVEL_NOMINAL,     },
  {  450000000, { HAL_CLK_SOURCE_MMPLL3,         4,      0,      0,      0       }, CLOCK_VREG_LEVEL_HIGH,        { {0x00, 0x00}, {0x02, 0x00} }},
  {  490000000, { HAL_CLK_SOURCE_MMPLL3,         4,      0,      0,      0       }, CLOCK_VREG_LEVEL_HIGH,        { {0x02, 0x00}, {0xFF, 0xFF} }, &SourceFreqConfig_MMPLL3[1] /* 980.0 MHz */ },
  { 0 }
};

const ClockAVSCoreConfigType ClockAVSData[] =
{
  {
    /* .nPhysicalBase    = */ 0x09980000,
    /* .eCore            = */ HAL_AVS_CORE_APC0,      /* Core 0 */
    /* .ePMIC            = */ HAL_AVS_PM8996_SUPPORT,
    /* .eStepSize        = */ HAL_AVS_STEP_SIZE_25MV,
    /* .nVddRequestDelay = */ 300,
    /* .nClockDiv        = */ 1,
    /* .HWVersion        = */ { {0, 0}, {0xFF, 0xFF}, DALCHIPINFO_FAMILY_UNKNOWN, NULL },
  },
  {
    /* .nPhysicalBase    = */ 0x9990000,
    /* .eCore            = */ HAL_AVS_CORE_APC1,      /* Core 1 */
    /* .ePMIC            = */ HAL_AVS_PM8996_SUPPORT,
    /* .eStepSize        = */ HAL_AVS_STEP_SIZE_25MV,
    /* .nVddRequestDelay = */ 300,
    /* .nClockDiv        = */ 1,
    /* .HWVersion        = */ { {0, 0}, {0xFF, 0xFF}, DALCHIPINFO_FAMILY_UNKNOWN, NULL },
  },
  {
    /* .nPhysicalBase    = */ 0x099B0000,
    /* .eCore            = */ HAL_AVS_CORE_APC2,      /* Core 2 */
    /* .ePMIC            = */ HAL_AVS_PM8996_SUPPORT,
    /* .eStepSize        = */ HAL_AVS_STEP_SIZE_25MV,
    /* .nVddRequestDelay = */ 300,
    /* .nClockDiv        = */ 1,
    /* .HWVersion        = */ { {0, 0}, {0xFF, 0xFF}, DALCHIPINFO_FAMILY_UNKNOWN, NULL },
  },
  {
    /* .nPhysicalBase    = */ 0x99C0000,
    /* .eCore            = */ HAL_AVS_CORE_APC3,      /* Core 3 */
    /* .ePMIC            = */ HAL_AVS_PM8996_SUPPORT,
    /* .eStepSize        = */ HAL_AVS_STEP_SIZE_25MV,
    /* .nVddRequestDelay = */ 300,
    /* .nClockDiv        = */ 1,
    /* .HWVersion        = */ { {0, 0}, {0xFF, 0xFF}, DALCHIPINFO_FAMILY_UNKNOWN, NULL },
  },
  {
    /* .nPhysicalBase    = */ 0x9A10000,
    /* .eCore            = */ HAL_AVS_CORE_L2,        /* CBF */
    /* .ePMIC            = */ HAL_AVS_PM8996_SUPPORT,
    /* .eStepSize        = */ HAL_AVS_STEP_SIZE_25MV,
    /* .nVddRequestDelay = */ 300,
    /* .nClockDiv        = */ 1,
    /* .HWVersion        = */ { {0, 0}, {0xFF, 0xFF}, DALCHIPINFO_FAMILY_UNKNOWN, NULL },
  },

  { 0 }
};


/*
 * Clock Log Default Configuration.
 *
 * NOTE: An .nGlobalLogFlags value of 0xFF will enable all logging by default.
 */
const ClockLogType ClockLogDefaultConfig[] =
{
  {
    /* .nLogSize        = */ 4096,
    /* .nGlobalLogFlags = */ 0xFF
  }
};


/*
 * Clock Flag Init Config.
 */
const ClockFlagInitType ClockFlagInitConfig[] =
{
  {
    CLOCK_FLAG_NODE_TYPE_CLOCK_DOMAIN,
    (void *)"apcs_cluster0_clk",
    CLOCK_FLAG_SUPPRESSIBLE
  },
  {
    CLOCK_FLAG_NODE_TYPE_CLOCK_DOMAIN,
    (void *)"apcs_cluster1_clk",
    CLOCK_FLAG_SUPPRESSIBLE
  },
  {
    CLOCK_FLAG_NODE_TYPE_SOURCE,
    (void *)"SLEEPCLK",
    CLOCK_FLAG_SOURCE_NOT_CONFIGURABLE
  },
  {
    CLOCK_FLAG_NODE_TYPE_SOURCE,
    (void *)"GPLL0_DIV2",
    CLOCK_FLAG_SOURCE_NOT_CONFIGURABLE
  },
  {
    CLOCK_FLAG_NODE_TYPE_SOURCE,
    (void *)"GPLL1_DIV2",
    CLOCK_FLAG_SOURCE_NOT_CONFIGURABLE
  },
  {
    CLOCK_FLAG_NODE_TYPE_SOURCE,
    (void *)"GPLL2_EARLY",
    CLOCK_FLAG_SOURCE_NOT_CONFIGURABLE
  },
  {
    CLOCK_FLAG_NODE_TYPE_NONE,
    (void *)0,
    0
  }
};


/*
 * List of clocks allowed to be suppressible.
 */
ClockNameListType ClockSuppressibleList[] =
{
  // Serial Debugger:
  { "gcc_blsp1_uart2_apps_clk"   },
  { "gcc_blsp1_ahb_clk"          },

  // USB 2.0 Debugger:
  { "gcc_usb20_master_clk"       },
  { "gcc_usb20_mock_utmi_clk"    },
  { "gcc_usb_phy_cfg_ahb2phy_clk"},

  // USB 3.0 Debugger:
  { "gcc_usb30_master_clk"       },
  { "gcc_usb30_mock_utmi_clk"    },
  { "gcc_sys_noc_usb3_axi_clk"   },
  { "gcc_usb30_sleep_clk"        },

  { NULL }
};

/*
 * List of clocks allowed to be always on.
 */
ClockNameListType ClockAlwaysOnList[] =
{
  { "gcc_usb30_sleep_clk"        },
  { NULL }
};

/*
 * List of clocks allowed to be reference counter disabled.
 */
ClockNameListType ClockRefCountSuspendedList[] =
{
  // USB 3.0
  { "gcc_usb30_master_clk"       },
  { "gcc_usb30_mock_utmi_clk"    },
  { "gcc_sys_noc_usb3_axi_clk"   },

  { NULL }
};

/*
 * List of clocks allowed to be configured with the
 * FORCE_MEM_CORE and FORCE_MEM_PERIPH parameters.
 */
ClockNameListType ClockForceMemCoreAndPeriphList[] =
{
  { "camss_cpp_axi_clk"         },
  { "camss_cpp_clk"             },
  { "camss_csi_vfe0_clk"        },
  { "camss_csi_vfe1_clk"        },
  { "camss_jpeg0_clk"           },
  { "camss_jpeg2_clk"           },
  { "camss_jpeg_axi_clk"        },
  { "camss_jpeg_dma_clk"        },
  { "camss_micro_ahb_clk"       },
  { "camss_vfe0_clk"            },
  { "camss_vfe1_clk"            },
  { "camss_vfe_axi_clk"         },
  { "fd_core_clk"               },
  { "fd_core_uar_clk"           },
  { "gcc_blsp1_ahb_clk"         },
  { "gcc_blsp2_ahb_clk"         },
  { "gcc_ce1_clk"               },
  { "gcc_pcie_0_mstr_axi_clk"   },
  { "gcc_pcie_0_pipe_clk"       },
  { "gcc_pcie_0_slv_axi_clk"    },
  { "gcc_pcie_1_mstr_axi_clk"   },
  { "gcc_pcie_1_pipe_clk"       },
  { "gcc_pcie_1_slv_axi_clk"    },
  { "gcc_pcie_2_mstr_axi_clk"   },
  { "gcc_pcie_2_pipe_clk"       },
  { "gcc_pcie_2_slv_axi_clk"    },
  { "gcc_sdcc1_apps_clk"        },
  { "gcc_sdcc1_ice_core_clk"    },
  { "gcc_sdcc2_apps_clk"        },
  { "gcc_sdcc3_apps_clk"        },
  { "gcc_sdcc4_apps_clk"        },
  { "gcc_smmu_aggre0_axi_clk"   },
  { "gcc_tsif_ahb_clk"          },
  { "gcc_ufs_axi_clk"           },
  { "gcc_ufs_ice_core_clk"      },
  { "gcc_ufs_rx_symbol_0_clk"   },
  { "gcc_ufs_tx_symbol_0_clk"   },
  { "gcc_ufs_unipro_core_clk"   },
  { "gcc_usb20_master_clk"      },
  { "gcc_usb30_master_clk"      },
  { "gpu_gx_gfx3d_clk"          },
  { "mdss_axi_clk"              },
  { "mdss_mdp_clk"              },
  { "smmu_cpp_axi_clk"          },
  { "smmu_jpeg_axi_clk"         },
  { "smmu_mdp_axi_clk"          },
  { "smmu_rot_axi_clk"          },
  { "smmu_vfe_axi_clk"          },
  { "smmu_video_axi_clk"        },
  { "video_axi_clk"             },
  { "video_core_clk"            },
  { "video_maxi_clk"            },
  { "video_subcore0_clk"        },
  { "video_subcore1_clk"        },
  { "vmem_maxi_clk"             },

  { NULL }
};

/*
 * List of clocks anticipated to fail the BIST.
 */
ClockNameListType ClockBistExceptions[] =
{
  { "gpu_gx_gfx3d_clk"          },
  { "mmss_spdm_debug_clk"       },
  { "mmss_spdm_gfx3d_clk"       },
  { "gcc_bimc_gfx_clk"          },
  { "gcc_mmss_bimc_gfx_clk"     },

  { NULL }
};

/*
 * List of clocks excluded from the BIST.
 */
ClockNameListType ClockExcludeFromBist[] =
{
  { NULL }
};

/*
 * Clock property table for use in non-DAL environments.
 *
 * NOTE:
 * Please add clock under proper subsystem and place it in order (i.e.,
 * each sub-list is sorted based on the string clock name).
 *
 */
const ClockPropertyType Clock_aProperties[] =
{
  /*
   * Clock Sources
   */
  { "ClockSources",                         SourceConfig },

  /*
   * GCC Clock Configurations
   */

  /*
   * UART Clocks
   */
  { "gcc_blsp1_uart1_apps_clk",             BLSP1UART1APPSClockConfig },
  { "gcc_blsp1_uart2_apps_clk",             BLSP1UART1APPSClockConfig },
  { "gcc_blsp1_uart3_apps_clk",             BLSP1UART1APPSClockConfig },
  { "gcc_blsp1_uart4_apps_clk",             BLSP1UART1APPSClockConfig },
  { "gcc_blsp1_uart5_apps_clk",             BLSP1UART1APPSClockConfig },
  { "gcc_blsp1_uart6_apps_clk",             BLSP1UART1APPSClockConfig },
  { "gcc_blsp2_uart1_apps_clk",             BLSP1UART1APPSClockConfig },
  { "gcc_blsp2_uart2_apps_clk",             BLSP1UART1APPSClockConfig },
  { "gcc_blsp2_uart3_apps_clk",             BLSP1UART1APPSClockConfig },
  { "gcc_blsp2_uart4_apps_clk",             BLSP1UART1APPSClockConfig },
  { "gcc_blsp2_uart5_apps_clk",             BLSP1UART1APPSClockConfig },
  { "gcc_blsp2_uart6_apps_clk",             BLSP1UART1APPSClockConfig },

  /*
   * QUP SPI Clocks
   */
  { "gcc_blsp1_qup1_spi_apps_clk",          BLSP1QUP1SPIAPPSClockConfig },
  { "gcc_blsp1_qup2_spi_apps_clk",          BLSP1QUP1SPIAPPSClockConfig },
  { "gcc_blsp1_qup3_spi_apps_clk",          BLSP1QUP1SPIAPPSClockConfig },
  { "gcc_blsp1_qup4_spi_apps_clk",          BLSP1QUP1SPIAPPSClockConfig },
  { "gcc_blsp1_qup5_spi_apps_clk",          BLSP1QUP1SPIAPPSClockConfig },
  { "gcc_blsp1_qup6_spi_apps_clk",          BLSP1QUP1SPIAPPSClockConfig },
  { "gcc_blsp2_qup1_spi_apps_clk",          BLSP1QUP1SPIAPPSClockConfig },
  { "gcc_blsp2_qup2_spi_apps_clk",          BLSP1QUP1SPIAPPSClockConfig },
  { "gcc_blsp2_qup3_spi_apps_clk",          BLSP1QUP1SPIAPPSClockConfig },
  { "gcc_blsp2_qup4_spi_apps_clk",          BLSP1QUP1SPIAPPSClockConfig },
  { "gcc_blsp2_qup5_spi_apps_clk",          BLSP1QUP1SPIAPPSClockConfig },
  { "gcc_blsp2_qup6_spi_apps_clk",          BLSP1QUP1SPIAPPSClockConfig },

  /*
   * QUP I2C Clocks
   */
  { "gcc_blsp1_qup1_i2c_apps_clk",          BLSP1QUP1I2CAPPSClockConfig },
  { "gcc_blsp1_qup2_i2c_apps_clk",          BLSP1QUP1I2CAPPSClockConfig },
  { "gcc_blsp1_qup3_i2c_apps_clk",          BLSP1QUP1I2CAPPSClockConfig },
  { "gcc_blsp1_qup4_i2c_apps_clk",          BLSP1QUP1I2CAPPSClockConfig },
  { "gcc_blsp1_qup5_i2c_apps_clk",          BLSP1QUP1I2CAPPSClockConfig },
  { "gcc_blsp1_qup6_i2c_apps_clk",          BLSP1QUP1I2CAPPSClockConfig },
  { "gcc_blsp2_qup1_i2c_apps_clk",          BLSP1QUP1I2CAPPSClockConfig },
  { "gcc_blsp2_qup2_i2c_apps_clk",          BLSP1QUP1I2CAPPSClockConfig },
  { "gcc_blsp2_qup3_i2c_apps_clk",          BLSP1QUP1I2CAPPSClockConfig },
  { "gcc_blsp2_qup4_i2c_apps_clk",          BLSP1QUP1I2CAPPSClockConfig },
  { "gcc_blsp2_qup5_i2c_apps_clk",          BLSP1QUP1I2CAPPSClockConfig },
  { "gcc_blsp2_qup6_i2c_apps_clk",          BLSP1QUP1I2CAPPSClockConfig },


  /*
   * GCC GP Clocks
   */
  { "gcc_gp1_clk",                          GP1ClockConfig },
  { "gcc_gp2_clk",                          GP1ClockConfig },
  { "gcc_gp3_clk",                          GP1ClockConfig },

  /*
   * HMSSAHB Clock
   */
  { "gcc_hmss_ahb_clk",                     HMSSAHBClockConfig },

  /*
   * HMSSRBCPR Clock
   */
  { "gcc_hmss_rbcpr_clk",                   HMSSRBCPRClockConfig },

  /*
   * PCIEAUX Clock
   */
  { "gcc_pcie_0_aux_clk",                   PCIEAUXClockConfig },

  /*
   * PDM2 Clock
   */
  { "gcc_pdm2_clk",                         PDM2ClockConfig },

  /*
   * SDC Clocks
   */
  { "gcc_sdcc1_apps_clk",                   SDCC1APPSClockConfig },
  { "gcc_sdcc2_apps_clk",                   SDCC2APPSClockConfig },
  { "gcc_sdcc3_apps_clk",                   SDCC2APPSClockConfig },
  { "gcc_sdcc4_apps_clk",                   SDCC4APPSClockConfig },

  /*
   * TSIF Clock
   */
  { "gcc_tsif_ref_clk",                     TSIFREFClockConfig },

  /*
   * UFSAXI Clock
   */
  { "gcc_aggre2_ufs_axi_clk",               UFSAXIClockConfig },

  /*
   * USB20MASTER Clock
   */
  { "gcc_usb20_master_clk",                 USB20MASTERClockConfig },

  /*
   * USB20MOCKUTMI Clock
   */
  { "gcc_usb20_mock_utmi_clk",              USB20MOCKUTMIClockConfig },

  /*
   * USB30MASTER Clock
   */
  { "gcc_usb30_master_clk",                 USB30MASTERClockConfig },

  /*
   * USB3AXI Clock
   */
  { "gcc_aggre2_usb3_axi_clk",              USB30MASTERClockConfig },

  /*
   * USB30MASTER Clock
   */
  { "gcc_usb30_mock_utmi_clk",              USB20MOCKUTMIClockConfig },

  /*
   * USB3PHYAUX Clock
   */
  { "gcc_usb3_phy_aux_clk",                 USB3PHYAUXClockConfig },

  /*
   * USB3PHYPIPE Clock
   */
  { "gcc_usb3_phy_pipe_clk",                USB3PHYPIPEClockConfig },

  /*
   * MMSS Clock Configurations
   */

  /*
   * AHB Clock
   */
  { "camss_ahb_clk",                        AHBClockConfig },

  /*
   * AXI Clock
   */
  { "camss_cpp_axi_clk",                    AXIClockConfig },

  /*
   * CAMSSGP Clocks
   */
  { "camss_gp0_clk",                        CAMSSGP0ClockConfig },
  { "camss_gp1_clk",                        CAMSSGP0ClockConfig },

  /*
   * CCI Clock
   */
  { "camss_cci_clk",                        CCIClockConfig },

  /*
   * CPP Clock
   */
  { "camss_cpp_clk",                        CPPClockConfig },

  /*
   * CSI Clocks
   */
  { "camss_csi0_clk",                       CSI0ClockConfig },
  { "camss_csi1_clk",                       CSI0ClockConfig },
  { "camss_csi2_clk",                       CSI0ClockConfig },
  { "camss_csi3_clk",                       CSI0ClockConfig },

  /*
   * CSI PHY Timer Clocks
   */
  { "camss_csi0phytimer_clk",               CSI0PHYTIMERClockConfig },
  { "camss_csi1phytimer_clk",               CSI0PHYTIMERClockConfig },
  { "camss_csi2phytimer_clk",               CSI0PHYTIMERClockConfig },

  /*
   * CSIPHY[0-2]3P Clocks
   */
  { "camss_csiphy0_3p_clk",                 CSIPHY03PClockConfig },
  { "camss_csiphy1_3p_clk",                 CSIPHY03PClockConfig },
  { "camss_csiphy2_3p_clk",                 CSIPHY03PClockConfig },

  /*
   * DSACORE Clock
   */
  { "dsa_core_clk",                         DSACOREClockConfig },

  /*
   * EDPGTC Clock
   */
  { "mdss_edpgtc_clk",                      EDPGTCClockConfig },

  /*
   * FDCORE Clock
   */
  { "fd_core_clk",                          FDCOREClockConfig },

  /*
   * GFX3D Clock
   */
  { "gpu_gx_gfx3d_clk",                     GFX3DClockConfig },

  /*
   * ISENSE Clock
   */
  { "gpu_aon_isense_clk",                   ISENSEClockConfig },

  /*
   * JPEG Clocks
   */
  { "camss_jpeg0_clk",                      JPEG0ClockConfig },
  { "camss_jpeg2_clk",                      JPEG2ClockConfig },

  /*
   * JPEGDMA Clock
   */
  { "camss_jpeg_dma_clk",                   JPEG0ClockConfig },

  /*
   * MAXI Clock
   */
  { "mmss_mmagic_maxi_clk",                 MAXIClockConfig },

  /*
   * MCLK Clocks
   */
  { "camss_mclk0_clk",                      MCLK0ClockConfig },
  { "camss_mclk1_clk",                      MCLK0ClockConfig },
  { "camss_mclk2_clk",                      MCLK0ClockConfig },
  { "camss_mclk3_clk",                      MCLK0ClockConfig },

  /*
   * VFE Clocks
   */
  { "camss_csi_vfe0_clk",                   VFE0ClockConfig },
  { "camss_csi_vfe1_clk",                   VFE0ClockConfig },


  /*
   * MDP Clock
   */
  { "mdss_mdp_clk",                         MDPClockConfig },

  /*
   * RBCPR Clock
   */
  { "mmss_rbcpr_clk",                       RBCPRClockConfig },

  { "mdss_byte0_clk",                       BYTE0ClockConfig },
  { "mdss_byte1_clk",                       BYTE0ClockConfig },

  { "mdss_edplink_clk",                     EDPLINKClockConfig },
  { "mdss_edppixel_clk",                    EDPPIXELClockConfig },

  { "mdss_esc0_clk",                        ESC0ClockConfig },
  { "mdss_esc1_clk",                        ESC0ClockConfig },

  { "mdss_extpclk_clk",                     EXTPCLKClockConfig },

  { "mdss_pclk0_clk",                       PCLK0ClockConfig },
  { "mdss_pclk1_clk",                       PCLK0ClockConfig },

  /*
   * VIDEOCORE Clock
   */
  { "mmss_spdm_video_core_clk",             VIDEOCOREClockConfig },
  { "video_core_clk",                       VIDEOCOREClockConfig },

  { "video_subcore0_clk",                   VIDEOSUBCORE0ClockConfig },
  { "video_subcore1_clk",                   VIDEOSUBCORE0ClockConfig },

  /*
   * Adaptive Voltage Scaling
   */
  { "AVSData",                              ClockAVSData },

  /*
   * Clock Log Defaults
   */
  { "ClockLogDefaults",                     ClockLogDefaultConfig },

  /*
   * Clocks allowed to be suppressible
   */
  { "SuppressibleList",                     ClockSuppressibleList },

  /*
   * Clocks allowed to be always on
   */
  { "ClockAlwaysOnList",                    ClockAlwaysOnList },

  /*
   * Clocks allowed to be reference counter disabled
   */
  { "RefCountSuspendedList",                ClockRefCountSuspendedList },

  /*
   * Clocks allowed to be configured with the
   * FORCE_MEM_CORE and FORCE_MEM_PERIPH parameters
   */
  { "ForceMemCoreAndPeriphList",            ClockForceMemCoreAndPeriphList },

  /*
   * Clock BIST Exceptions
   */
  { "BISTExceptions",                       ClockBistExceptions },

  /*
   * List of clocks excluded from BIST
   */
  { "ExcludeFromBist",                      ClockExcludeFromBist },

  /*
   * Clock Init Flags
   */
  { "ClockInitFlags",                       ClockFlagInitConfig },

  { NULL, NULL }
};

