/*==============================================================================

FILE:      icbcfg_data.c

DESCRIPTION: This file implements the ICB Configuration driver.

PUBLIC CLASSES:  Not Applicable

INITIALIZATION AND SEQUENCING REQUIREMENTS:  N/A
 
Edit History

$Header: //components/rel/boot.xf/1.0/QcomPkg/Msm8996Pkg/Library/DALConfigLib/ICBConfig/icbcfg_data.c#26 $ 
$DateTime: 2016/02/03 11:50:09 $
$Author: pwbldsvc $
$Change: 9838719 $ 

When        Who    What, where, why
----------  ---    -----------------------------------------------------------
2016/02/02  sds    Add devcfg information for Radagast (use Istariv2+)
2016/01/18  sds    Configure MMSS master write gathering as a workaround for
                   BIMC lockup.
2015/12/18  sds    Return M4M settings to recommended.
2015/12/14  sds    Revert previous. Issues now appearing in PDT.
2015/11/05  sds    Update recommended settings for APCS_CBF_M4M_Q22SIB_LIMIT_CTRL.
2015/10/30  sds    Fix one DSAT recommendation typo (level 7 limit for LPASS)
2015/10/07  sds    Update DSAT to Oct 5 recommendations
2015/09/08  sds    Revert previous due to HW CR
2015/09/03  sds    Enable redirect for GPU/SYS for inner/outer/cacheable/shareable
2015/08/24  sds    Enable read gathering for AP->SNOC path.
2015/07/23  sds    Add initial lock bits
2015/07/09  sds    Update to DSAT spreadsheet
2015/06/16  sds    Update DDR valid region to use the new base
2015/06/11  sds    Update safe reset seg to new DDR base
2015/05/14  sds    Disable SAFE signal override
2015/05/05  sds    Update v2 configuration to latest spreadsheet
2015/04/29  sds    Danger context allocation changes for v2
2015/02/20  sds    Update for DSAT configuration
2015/02/03  sds    Updates for bringup
2014/11/20  sds    Restructured target data
2014/06/10  sds    Created
 
           Copyright (c) 2014-2016 Qualcomm Technologies, Inc.
                        All Rights Reserved.
                     QUALCOMM Proprietary/GTDR
==============================================================================*/
#include "icbcfg.h"
#include "icbcfg_hwio.h"
#include "../../../../Library/ICBLib/icbcfgi.h"
#include "../../../../Library/ICBLib/HALbimc.h"
#include "../../../../Library/ICBLib/HALbimcHwioGeneric.h"

/*---------------------------------------------------------------------------*/
/*          Macro and constant definitions                                   */
/*---------------------------------------------------------------------------*/
#define ARRAY_SIZE(arr) (sizeof(arr)/sizeof((arr)[0]))

/* DDR slave indexes */
#define SLAVE_DDR_CH0 0
#define SLAVE_DDR_CH1 1

#define BIMC_S_DDR_ARB_REG_BASE(b)  ((b)      + 0x00031000)
#define BIMC_S_DDR_ARB_MODE_ADDR(b,n)   (BIMC_S_DDR_ARB_REG_BASE(b) + (0xC000 * (n)) + 0x00000210)

#define DDR_ARB_MODE(slave,mode) \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_S_DDR_ARB_MODE_ADDR((uint8_t *)BIMC_BASE,slave), \
   BIMC_S_ARB_MODE_RMSK, \
   BIMC_S_ARB_MODE_RMSK, \
   (mode) }

#define MPORT_MODE(master,mode) \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_M_MODE_ADDR((uint8_t *)BIMC_BASE,master), \
   BIMC_M_MODE_RMSK, \
   BIMC_M_MODE_RMSK, \
   (mode) }

#define MPORT_BKE_ENABLE(master,value) \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_M_BKE_ENABLE_ADDR((uint8_t *)BIMC_BASE,master), \
   BIMC_M_BKE_ENABLE_RMSK, \
   BIMC_M_BKE_ENABLE_RMSK, \
   (value) }

#define MPORT_BKE1_ENABLE(master,value) \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_M_BKE1_ENABLE_ADDR((uint8_t *)BIMC_BASE,master), \
   BIMC_M_BKE1_ENABLE_RMSK, \
   BIMC_M_BKE1_ENABLE_RMSK, \
   (value) }

#define MPORT_BKE2_ENABLE(master,value) \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_M_BKE2_ENABLE_ADDR((uint8_t *)BIMC_BASE,master), \
   BIMC_M_BKE2_ENABLE_RMSK, \
   BIMC_M_BKE2_ENABLE_RMSK, \
   (value) }

#define MPORT_BKE3_ENABLE(master,value) \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_M_BKE3_ENABLE_ADDR((uint8_t *)BIMC_BASE,master), \
   BIMC_M_BKE3_ENABLE_RMSK, \
   BIMC_M_BKE3_ENABLE_RMSK, \
   (value) }

#define MPORT_BKE3_GRANT_PERIOD(master,value) \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_M_BKE3_GRANT_PERIOD_ADDR((uint8_t *)BIMC_BASE,master), \
   BIMC_M_BKE3_GRANT_PERIOD_RMSK, \
   BIMC_M_BKE3_GRANT_PERIOD_RMSK, \
   (value) }

#define MPORT_BKE3_GRANT_COUNT(master,value) \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_M_BKE3_GRANT_COUNT_ADDR((uint8_t *)BIMC_BASE,master), \
   BIMC_M_BKE3_GRANT_COUNT_RMSK, \
   BIMC_M_BKE3_GRANT_COUNT_RMSK, \
   (value) }

#define MPORT_BKE3_THRESHOLD_MEDIUM(master,value) \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_M_BKE3_THRESHOLD_MEDIUM_ADDR((uint8_t *)BIMC_BASE,master), \
   BIMC_M_BKE3_THRESHOLD_MEDIUM_RMSK, \
   BIMC_M_BKE3_THRESHOLD_MEDIUM_RMSK, \
   (value) }

#define MPORT_BKE3_THRESHOLD_LOW(master,value) \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_M_BKE3_THRESHOLD_LOW_ADDR((uint8_t *)BIMC_BASE,master), \
   BIMC_M_BKE3_THRESHOLD_LOW_RMSK, \
   BIMC_M_BKE3_THRESHOLD_LOW_RMSK, \
   (value) }

#define MPORT_BKE3_HEALTH_0(master,value) \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_M_BKE3_HEALTH_0_ADDR((uint8_t *)BIMC_BASE,master), \
   BIMC_M_BKE3_HEALTH_0_RMSK, \
   BIMC_M_BKE3_HEALTH_0_RMSK, \
   (value) }

/* Master indexes */
#define MASTER_APP   0
#define MASTER_GPU   1
#define MASTER_MMSS  2
#define MASTER_SYS   3
#define MASTER_PIMEM 4
#define MASTER_MDSP  5

#define DT_AGG_REQ_CFG(n, value) \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_DT_AGG_REQ_CFG_n_ADDR(BIMC_BASE, n), \
   BIMC_DT_AGG_REQ_CFG_n_RMSK, \
   BIMC_DT_AGG_REQ_CFG_n_RMSK, \
   (value) }

#define BIMC_BRIC_MSA_LOCKS_ADDR(b)      (BIMC_GLOBAL1_REG_BASE(b) + 0x0300)
#define BIMC_BRIC_PROTNS_LOCKS_ADDR(b)   (BIMC_GLOBAL1_REG_BASE(b) + 0x0310)

#define BRIC_MSA_LOCKS(value) \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_BRIC_MSA_LOCKS_ADDR((uint8_t *)BIMC_BASE), \
   (value), \
   0xffffffff, \
   (value) }

#define BRIC_PROTNS_LOCKS(value) \
  {ICBCFG_32_BIT_REG, \
   (void *)BIMC_BRIC_PROTNS_LOCKS_ADDR((uint8_t *)BIMC_BASE), \
   (value), \
   0xffffffff, \
   (value) }

/*============================================================================
                        DEVICE CONFIG PROPERTY DATA
============================================================================*/

/*---------------------------------------------------------------------------*/
/*          Properties data for device ID  = "icbcfg/boot"                   */
/*---------------------------------------------------------------------------*/

/* ICBcfg Boot Configuration Data*/

/*---------------------------------------------------------------------------*/
/* MSM8996 v1                                                                */
/*---------------------------------------------------------------------------*/
icbcfg_data_type icbcfg_boot_data_v1[] = 
{
  /* MMSS NOC AHB enable */
  ICBCFG_HWIOF_DW(GCC_MMSS_NOC_CFG_AHB_CBCR, CLK_ENABLE, 0x1),

  /* DSAT core registers */
  ICBCFG_HWIOF_DW(DSA_CORE_CBCR, CLK_ENABLE, 0x1),
  ICBCFG_HWIOF_DW(DSA_NOC_CFG_AHB_CBCR, CLK_ENABLE, 0x1),
  ICBCFG_HWIO_DW(DSA_CORE_CLK_CGC_CNTRL, 0x1),
  ICBCFG_HWIO_DW(DSA_PRE_STALL_TIMEOUT_CNT_URG_0, 0x60),
  ICBCFG_HWIO_DW(DSA_PRE_STALL_TIMEOUT_CNT_URG_1, 0x27),
  ICBCFG_HWIO_DW(DSA_PRE_STALL_TIMEOUT_CNT_URG_2, 0x27),
  ICBCFG_HWIO_DW(DSA_PRE_STALL_TIMEOUT_CNT_URG_3, 0x27),
  ICBCFG_HWIO_DW(DSA_POST_STALL_TIMEOUT_CNT_URG_0, 0x60),
  ICBCFG_HWIO_DW(DSA_POST_STALL_TIMEOUT_CNT_URG_1, 0x27),
  ICBCFG_HWIO_DW(DSA_POST_STALL_TIMEOUT_CNT_URG_2, 0x27),
  ICBCFG_HWIO_DW(DSA_POST_STALL_TIMEOUT_CNT_URG_3, 0x27),
  ICBCFG_HWIO_DW(DSA_POST_STALL_WDW_OVERLAP_CNTL, 0x1),
  ICBCFG_HWIOI_DW(DSA_CLNT_n_DANGER_AGGR_CNTRL, 8, 0x3),
  ICBCFG_HWIOI_DW(DSA_CLNT_n_DANGER_AGGR_CNTRL, 9, 0x3),
  ICBCFG_HWIO_DW(DSA_DSP_DANGER_AGGR_CNTRL, 0x3),
  ICBCFG_HWIOI_DW(DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_00, 8, 0x924688),
  ICBCFG_HWIOI_DW(DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_01, 8, 0x924),
  ICBCFG_HWIOFI_DW(DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_EN, 8, THROTTLE_LEVEL_EN, 0x1),
  ICBCFG_HWIOI_DW(DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_00, 9, 0x924688),
  ICBCFG_HWIOI_DW(DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_01, 9, 0x924),
  ICBCFG_HWIOFI_DW(DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_EN, 9, THROTTLE_LEVEL_EN, 0x1),
  ICBCFG_HWIOI_DW(DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_00, 10, 0x9248D0),
  ICBCFG_HWIOI_DW(DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_01, 10, 0x924),
  ICBCFG_HWIOFI_DW(DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_EN, 10, THROTTLE_LEVEL_EN, 0x1),
  ICBCFG_HWIO_DW(DSA_DSP_SYSTEM_DANGER_OUTPUT, 0xFFE4),
  ICBCFG_HWIO_DW(DSA_DSP_SYSTEM_DANGER_OUTPUT_EN, 0x1),
  ICBCFG_HWIO_DW(DSA_DANGER_SAFE_CNTRL,0x1),
  
  /* VENUS throttle related registers */
  ICBCFG_HWIOF_DW(MMSS_MMAGIC_VIDEO_GDSCR, SW_COLLAPSE, 0x0),
  ICBCFG_HWIOF_DW(MMSS_THROTTLE_VIDEO_CXO_CBCR, CLK_ENABLE, 0x1),
  ICBCFG_HWIOF_DW(MMSS_THROTTLE_VIDEO_AHB_CBCR, CLK_ENABLE, 0x1),
  ICBCFG_HWIOF_DW(MMSS_THROTTLE_VIDEO_AXI_CBCR, CLK_ENABLE, 0x1),

  /* VFE throttle related registers */
  ICBCFG_HWIOF_DW(MMSS_MMAGIC_CAMSS_GDSCR, SW_COLLAPSE, 0x0),
  ICBCFG_HWIOF_DW(MMSS_THROTTLE_CAMSS_CXO_CBCR, CLK_ENABLE, 0x1),
  ICBCFG_HWIOF_DW(MMSS_THROTTLE_CAMSS_AHB_CBCR, CLK_ENABLE, 0x1),
  ICBCFG_HWIOF_DW(MMSS_THROTTLE_CAMSS_AXI_CBCR, CLK_ENABLE, 0x1),

  /* MDP throttle related registers */
  ICBCFG_HWIOF_DW(MMSS_MMAGIC_MDSS_GDSCR, SW_COLLAPSE, 0x0),
  ICBCFG_HWIOF_DW(MMSS_THROTTLE_MDSS_CXO_CBCR, CLK_ENABLE, 0x1),
  ICBCFG_HWIOF_DW(MMSS_THROTTLE_MDSS_AHB_CBCR, CLK_ENABLE, 0x1),
  ICBCFG_HWIOF_DW(MMSS_THROTTLE_MDSS_AXI_CBCR, CLK_ENABLE, 0x1),

  /* LPASS throttle related registers */
  ICBCFG_HWIOF_DW(GCC_LPASS_SWAY_CBCR, CLK_ENABLE, 0x1),
  ICBCFG_HWIOF_DW(LPASS_AUDIO_WRAPPER_QOS_XO_LAT_COUNTER_CBCR, CLK_ENABLE, 0x1),
  ICBCFG_HWIOF_DW(LPASS_AUDIO_WRAPPER_QOS_AHBS_AON_CBCR, CLK_ENABLE, 0x1),
  ICBCFG_HWIOF_DW(GCC_LPASS_Q6_AXI_CBCR, CLK_ENABLE, 0x1),
  ICBCFG_HWIOF_DW(LPASS_QOS_CGC_CNTRL, THROTTLE_CGC_EN, 0x1),
  ICBCFG_HWIOF_DW(LPASS_QOS_GRANT_PERIOD, GRANT_PERIOD, 0x3e8),
  ICBCFG_HWIO_DW(LPASS_QOS_THRESHOLD_03, 0x0000FFFF),
  ICBCFG_HWIO_DW(LPASS_QOS_THRESHOLD_02, 0xFFFF0190),
  ICBCFG_HWIO_DW(LPASS_QOS_THRESHOLD_01, 0x01900190),
  ICBCFG_HWIO_DW(LPASS_QOS_THRESHOLD_00, 0x01900190),
  ICBCFG_HWIOF_DW(LPASS_QOS_PEAK_ACCUM_CREDIT, PEAK_ACCUM_CREDIT, 0x100),
  ICBCFG_HWIOF_DW(LPASS_QOS_CNTRL, THROTTLE_EN, 0x1),

  /* BIMC DT/QOS aggregator */
  DT_AGG_REQ_CFG(0, 0x500),
  DT_AGG_REQ_CFG(2, 0x500),

  /* Danger BKE configuration, AP master port */
  MPORT_BKE3_GRANT_PERIOD(MASTER_APP, 0x14),
  MPORT_BKE3_GRANT_COUNT(MASTER_APP, 0x64),
  MPORT_BKE3_THRESHOLD_MEDIUM(MASTER_APP, 0xFFCE),
  MPORT_BKE3_THRESHOLD_LOW(MASTER_APP, 0xFF9C),
  MPORT_BKE3_HEALTH_0(MASTER_APP, 0x80000000),
  MPORT_BKE_ENABLE(MASTER_APP, 0x010F0000),
  MPORT_BKE3_ENABLE(MASTER_APP, 0xFE000001),

  /* Danger BKE configuration, GPU master port */
  MPORT_BKE3_GRANT_PERIOD(MASTER_GPU, 0x14),
  MPORT_BKE3_GRANT_COUNT(MASTER_GPU, 0x64),
  MPORT_BKE3_THRESHOLD_MEDIUM(MASTER_GPU, 0xFFCE),
  MPORT_BKE3_THRESHOLD_LOW(MASTER_GPU, 0xFF9C),
  MPORT_BKE3_HEALTH_0(MASTER_GPU, 0x80000000),
  MPORT_BKE_ENABLE(MASTER_GPU, 0x010F0000),
  MPORT_BKE3_ENABLE(MASTER_GPU, 0xFE000001),

  /* DDR slaveway config */
  DDR_ARB_MODE(SLAVE_DDR_CH0, 0x10000001), 
  DDR_ARB_MODE(SLAVE_DDR_CH1, 0x10000001), 
};

icbcfg_prop_type icbcfg_boot_prop_v1 = 
{
    /* Length of the config  data array */
    ARRAY_SIZE(icbcfg_boot_data_v1),
    /* Pointer to config data array */ 
    icbcfg_boot_data_v1                                    
};

/* DDR map information. */
icbcfg_mem_region_type map_ddr_regions[1] =
{
  { 0x020000000ULL, 0x400000000ULL },
};

uint32 channel_map[2] = { SLAVE_DDR_CH0, SLAVE_DDR_CH1 };

HAL_bimc_InfoType bimc_hal_info =
{
  (uint8_t *)BIMC_BASE, /* Base address */
  19200,                 /* QoS frequency */
  {
    0,
    0,
    0,
    0,
    0,
    0,
    4, /**< Number of segments for address decode. */
  }
};

/* Make sure the config region is always prohibited when "resetting" */
HAL_bimc_SlaveSegmentType safe_reset_seg =
{
  true,
  0x00000000ULL,                 /* start of config region */
  0x20000000ULL,                 /* 512 MB */
  BIMC_SEGMENT_TYPE_SUBTRACTIVE,
  BIMC_INTERLEAVE_NONE,
};

/* L2 TCM unmapping */
icbcfg_segment_type unmap_8996_segments[] =
{
  {
    0,
    {
      false,
    }
  },
  {
    1,
    {
      false,
    }
  },
};

icbcfg_l2_unmap_info_type l2_tcm_unmap =
{
  2,
  ARRAY_SIZE(unmap_8996_segments),
  unmap_8996_segments,
};

icbcfg_device_config_type msm8996_v1 =
{
  /* Chip version information for this device data. */
  DALCHIPINFO_FAMILY_MSM8996,  /**< Chip family */
  false,                       /**< Exact match for version? */
  0x10000,                     /**< Chip version */

  /* Device information. */
  &icbcfg_boot_prop_v1,        /**< ICB_Config_Init() prop data */
  ARRAY_SIZE(channel_map),     /**< Number of DDR channels */
  channel_map,                 /**< Map of array indicies to channel numbers */
  4,                           /**< Number of BRIC segments per slave */
  ARRAY_SIZE(map_ddr_regions), /**< Number of regions in the DDR map */
  map_ddr_regions,             /**< Array of mappable DDR regions */
  &bimc_hal_info,              /**< BIMC HAL info structure */
  &safe_reset_seg,             /**< The segment config to use while reseting segments */
  false,                       /**< Have we entered best effort mode? */
  &l2_tcm_unmap,               /**< L2 TCM unmap configuration */
  NULL,                        /**< ICB_Config_PostInit() prop data */
};

/*---------------------------------------------------------------------------*/
/* MSM8996 v2                                                                */
/*---------------------------------------------------------------------------*/
icbcfg_data_type icbcfg_boot_data_v2[] = 
{
  /* MMSS NOC AHB enable */
  ICBCFG_HWIOF_DW(GCC_MMSS_NOC_CFG_AHB_CBCR, CLK_ENABLE, 0x1),

  /* DSAT core registers */
  ICBCFG_HWIOF_DW(DSA_CORE_CBCR, CLK_ENABLE, 0x1),
  ICBCFG_HWIOF_DW(DSA_NOC_CFG_AHB_CBCR, CLK_ENABLE, 0x1),
  ICBCFG_HWIO_DW(DSA_CORE_CLK_CGC_CNTRL, 0x1),
  ICBCFG_HWIO_DW(DSA_PRE_STALL_TIMEOUT_CNT_URG_0, 0x60),
  ICBCFG_HWIO_DW(DSA_PRE_STALL_TIMEOUT_CNT_URG_1, 0x27),
  ICBCFG_HWIO_DW(DSA_PRE_STALL_TIMEOUT_CNT_URG_2, 0x27),
  ICBCFG_HWIO_DW(DSA_PRE_STALL_TIMEOUT_CNT_URG_3, 0x27),
  ICBCFG_HWIO_DW(DSA_POST_STALL_TIMEOUT_CNT_URG_0, 0x60),
  ICBCFG_HWIO_DW(DSA_POST_STALL_TIMEOUT_CNT_URG_1, 0x27),
  ICBCFG_HWIO_DW(DSA_POST_STALL_TIMEOUT_CNT_URG_2, 0x27),
  ICBCFG_HWIO_DW(DSA_POST_STALL_TIMEOUT_CNT_URG_3, 0x27),
  ICBCFG_HWIO_DW(DSA_POST_STALL_WDW_OVERLAP_CNTL, 0x1),
  ICBCFG_HWIOI_DW(DSA_CLNT_n_DANGER_AGGR_CNTRL, 8, 0x3),
  ICBCFG_HWIOI_DW(DSA_CLNT_n_DANGER_AGGR_MODEM_CNTRL, 8, 0x1),
  ICBCFG_HWIOI_DW(DSA_CLNT_n_DANGER_AGGR_CNTRL, 9, 0x3),
  ICBCFG_HWIOI_DW(DSA_CLNT_n_DANGER_AGGR_MODEM_CNTRL, 9, 0x1),
  ICBCFG_HWIO_DW(DSA_DSP_DANGER_AGGR_CNTRL, 0xF3),
  ICBCFG_HWIOI_DW(DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_00, 8, 0x924688),
  ICBCFG_HWIOI_DW(DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_01, 8, 0x924),
  ICBCFG_HWIOFI_DW(DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_EN, 8, THROTTLE_LEVEL_EN, 0x1),
  ICBCFG_HWIOI_DW(DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_00, 9, 0x924688),
  ICBCFG_HWIOI_DW(DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_01, 9, 0x924),
  ICBCFG_HWIOFI_DW(DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_EN, 9, THROTTLE_LEVEL_EN, 0x1),
  ICBCFG_HWIOI_DW(DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_00, 10, 0x9248D0),
  ICBCFG_HWIOI_DW(DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_01, 10, 0x924),
  ICBCFG_HWIOFI_DW(DSA_CLNT_n_THROTTLE_LEVEL_OUTPUT_EN, 10, THROTTLE_LEVEL_EN, 0x1),
  ICBCFG_HWIO_DW(DSA_DSP_SYSTEM_DANGER_OUTPUT, 0xFFFFE4),
  ICBCFG_HWIO_DW(DSA_DSP_SYSTEM_DANGER_OUTPUT_EN, 0x1),
  ICBCFG_HWIO_DW(DSA_DSP_PRIORITY_MODE_SEL, 0x1),
  ICBCFG_HWIO_DW(DSA_THROTTLE_LEVEL_LEGACY_SELECT, 0x0),
  ICBCFG_HWIOI_DW(DSA_THROTTLE_LEVEL_QOS_n, 1, 0x7FC),
  ICBCFG_HWIOI_DW(DSA_THROTTLE_LEVEL_QOS_n, 2, 0x7FC),
  ICBCFG_HWIOI_DW(DSA_THROTTLE_LEVEL_QOS_n, 3, 0x80FC),
  ICBCFG_HWIOI_DW(DSA_THROTTLE_LEVEL_QOS_n, 4, 0x7FC),
  ICBCFG_HWIOI_DW(DSA_CLNT_n_THROTTLE_LEVEL_QOS_1, 8,  0xEE000001),
  ICBCFG_HWIOI_DW(DSA_CLNT_n_THROTTLE_LEVEL_QOS_2, 8,  0xEE000002),
  ICBCFG_HWIOI_DW(DSA_CLNT_n_THROTTLE_LEVEL_QOS_3, 8,  0xEE000003),
  ICBCFG_HWIOI_DW(DSA_CLNT_n_THROTTLE_LEVEL_QOS_1, 9,  0xEE000001),
  ICBCFG_HWIOI_DW(DSA_CLNT_n_THROTTLE_LEVEL_QOS_2, 9,  0xEE000002),
  ICBCFG_HWIOI_DW(DSA_CLNT_n_THROTTLE_LEVEL_QOS_3, 9,  0xEE000003),
  ICBCFG_HWIOI_DW(DSA_CLNT_n_THROTTLE_LEVEL_QOS_1, 10, 0x0),
  ICBCFG_HWIOI_DW(DSA_CLNT_n_THROTTLE_LEVEL_QOS_2, 10, 0xEE000081),
  ICBCFG_HWIOI_DW(DSA_CLNT_n_THROTTLE_LEVEL_QOS_3, 10, 0xEA000082),
  ICBCFG_HWIOI_DW(DSA_CLNT_n_THROTTLE_LEVEL_QOS_4, 10, 0xEE000003),
  ICBCFG_HWIOF_READ_DW(BIMC_DDR_CH0_CLK_PERIOD,PERIOD),
  ICBCFG_HWIOF_WRITE_DW(DSA_PERIOD_BUS_CFG,PERIOD_BUS_SW),
  ICBCFG_HWIOF_DW(DSA_PERIOD_BUS_CFG, PERIOD_BUS_LOAD_SW, 0x1),
  ICBCFG_HWIOF_DW(DSA_PERIOD_BUS_CFG, PERIOD_BUS_SW_OVERRIDE, 0x0),
  ICBCFG_HWIOI_DW(QOS_FREQ_BAND_BNDRY_n, 0, 0x97B),
  ICBCFG_HWIOI_DW(QOS_FREQ_BAND_BNDRY_n, 1, 0x0),
  ICBCFG_HWIOI_DW(QOS_FREQ_BAND_BNDRY_n, 2, 0x0),
  ICBCFG_HWIO_DW(DSA_AGGR_SAFE_OVERRIDE_CNTRL, 0x1),
  ICBCFG_HWIO_DW(DSA_DANGER_SAFE_CNTRL,0x1),
  
  /* VENUS throttle related registers */
  ICBCFG_HWIOF_DW(MMSS_MMAGIC_VIDEO_GDSCR, SW_COLLAPSE, 0x0),
  ICBCFG_HWIOF_DW(MMSS_THROTTLE_VIDEO_CXO_CBCR, CLK_ENABLE, 0x1),
  ICBCFG_HWIOF_DW(MMSS_THROTTLE_VIDEO_AHB_CBCR, CLK_ENABLE, 0x1),
  ICBCFG_HWIOF_DW(MMSS_THROTTLE_VIDEO_AXI_CBCR, CLK_ENABLE, 0x1),

  /* VFE throttle related registers */
  ICBCFG_HWIOF_DW(MMSS_MMAGIC_CAMSS_GDSCR, SW_COLLAPSE, 0x0),
  ICBCFG_HWIOF_DW(MMSS_THROTTLE_CAMSS_CXO_CBCR, CLK_ENABLE, 0x1),
  ICBCFG_HWIOF_DW(MMSS_THROTTLE_CAMSS_AHB_CBCR, CLK_ENABLE, 0x1),
  ICBCFG_HWIOF_DW(MMSS_THROTTLE_CAMSS_AXI_CBCR, CLK_ENABLE, 0x1),

  /* MDP throttle related registers */
  ICBCFG_HWIOF_DW(MMSS_MMAGIC_MDSS_GDSCR, SW_COLLAPSE, 0x0),
  ICBCFG_HWIOF_DW(MMSS_THROTTLE_MDSS_CXO_CBCR, CLK_ENABLE, 0x1),
  ICBCFG_HWIOF_DW(MMSS_THROTTLE_MDSS_AHB_CBCR, CLK_ENABLE, 0x1),
  ICBCFG_HWIOF_DW(MMSS_THROTTLE_MDSS_AXI_CBCR, CLK_ENABLE, 0x1),

  /* LPASS throttle related registers */
  ICBCFG_HWIOF_DW(GCC_LPASS_SWAY_CBCR, CLK_ENABLE, 0x1),
  ICBCFG_HWIOF_DW(LPASS_AUDIO_WRAPPER_QOS_XO_LAT_COUNTER_CBCR, CLK_ENABLE, 0x1),
  ICBCFG_HWIOF_DW(LPASS_AUDIO_WRAPPER_QOS_AHBS_AON_CBCR, CLK_ENABLE, 0x1),
  ICBCFG_HWIOF_DW(GCC_LPASS_Q6_AXI_CBCR, CLK_ENABLE, 0x1),
  ICBCFG_HWIOF_DW(LPASS_QOS_CGC_CNTRL, THROTTLE_CGC_EN, 0x1),
  ICBCFG_HWIOF_DW(LPASS_QOS_GRANT_PERIOD, GRANT_PERIOD, 0x3e8),
  ICBCFG_HWIO_DW(LPASS_QOS_THRESHOLD_03, 0x0000FFFF),
  ICBCFG_HWIO_DW(LPASS_QOS_THRESHOLD_02, 0xFFFF07D0),
  ICBCFG_HWIO_DW(LPASS_QOS_THRESHOLD_01, 0x01900190),
  ICBCFG_HWIO_DW(LPASS_QOS_THRESHOLD_00, 0x01900190),
  ICBCFG_HWIOF_DW(LPASS_QOS_PEAK_ACCUM_CREDIT, PEAK_ACCUM_CREDIT, 0x100),
  ICBCFG_HWIOF_DW(LPASS_QOS_CNTRL, THROTTLE_EN, 0x1),

  /* BIMC DT/QOS aggregator */
  DT_AGG_REQ_CFG(0, 0x500),
  DT_AGG_REQ_CFG(2, 0x500),

  /* Danger BKE configuration, AP master port */
  MPORT_BKE3_GRANT_PERIOD(MASTER_APP, 0x14),
  MPORT_BKE3_GRANT_COUNT(MASTER_APP, 0x64),
  MPORT_BKE3_THRESHOLD_MEDIUM(MASTER_APP, 0xFFCE),
  MPORT_BKE3_THRESHOLD_LOW(MASTER_APP, 0xFF9C),
  MPORT_BKE3_HEALTH_0(MASTER_APP, 0x80000000),
  MPORT_BKE_ENABLE(MASTER_APP, 0x010F0000),
  MPORT_BKE1_ENABLE(MASTER_APP, 0x02000000),
  MPORT_BKE2_ENABLE(MASTER_APP, 0x04000000),
  MPORT_BKE3_ENABLE(MASTER_APP, 0xF8000001),

  /* Danger BKE configuration, GPU master port */
  MPORT_BKE3_GRANT_PERIOD(MASTER_GPU, 0x14),
  MPORT_BKE3_GRANT_COUNT(MASTER_GPU, 0x64),
  MPORT_BKE3_THRESHOLD_MEDIUM(MASTER_GPU, 0xFFCE),
  MPORT_BKE3_THRESHOLD_LOW(MASTER_GPU, 0xFF9C),
  MPORT_BKE3_HEALTH_0(MASTER_GPU, 0x80000000),
  MPORT_BKE_ENABLE(MASTER_GPU, 0x030F0000),
  MPORT_BKE2_ENABLE(MASTER_GPU, 0x04000000),
  MPORT_BKE3_ENABLE(MASTER_GPU, 0xF8000001),

  /* DDR slaveway config */
  DDR_ARB_MODE(SLAVE_DDR_CH0, 0x10000001), 
  DDR_ARB_MODE(SLAVE_DDR_CH1, 0x10000001), 

  /* DDR redirect configuration */
  ICBCFG_HWIO_DW(BIMC_BRIC_REDIRECT_MSTR_EN, 0x002E002E),
  ICBCFG_HWIO_DW(BIMC_BRIC_REDIRECT_CTRL, 0xC0000100),

  /* AP->SNOC read gathering */
  ICBCFG_HWIOF_DW(BIMC_M_APP_MPORT_PIPE2_GATHERING, RD_GATHER_BEATS, 0xF),

  /* WR gather on MMSS master port for all pipes.
   * BIMC_M_MMSS_MPORT_PIPE0_GATHERING[WR_GATHER_BEATS] = 0xF
   * BIMC_M_MMSS_MPORT_PIPE1_GATHERING[WR_GATHER_BEATS] = 0xF
   * BIMC_M_MMSS_MPORT_PIPE2_GATHERING[WR_GATHER_BEATS] = 0xF
   */
  ICBCFG_RAW_DW(0x00410260, 0x000F0000), 
  ICBCFG_RAW_DW(0x00410264, 0x000F0000), 
  ICBCFG_RAW_DW(0x00410268, 0x000F0000),
};

icbcfg_prop_type icbcfg_boot_prop_v2 = 
{
    /* Length of the config  data array */
    ARRAY_SIZE(icbcfg_boot_data_v2),
    /* Pointer to config data array */ 
    icbcfg_boot_data_v2                                    
};

icbcfg_data_type icbcfg_boot_post_data_v2[] = 
{
    /* APCS_CBF_M4M_Q22SIB_LIMIT_CTRL[DEVICE_LIMIT]=0x0 (nolimit)
    * APCS_CBF_M4M_Q22SIB_LIMIT_CTRL[PCIE_LIMIT]=0x1 */
    ICBCFG_HWIO_DW(APCS_CBF_M4M_Q22SIB_LIMIT_CTRL, 0x00010808),

    BRIC_MSA_LOCKS(0x00008000),
    BRIC_PROTNS_LOCKS(0x00008000),
};

icbcfg_prop_type icbcfg_boot_post_prop_v2 = 
{
    /* Length of the config  data array */
    ARRAY_SIZE(icbcfg_boot_post_data_v2),
    /* Pointer to config data array */ 
    icbcfg_boot_post_data_v2                                    
};

icbcfg_device_config_type msm8996_v2 =
{
  /* Chip version information for this device data. */
  DALCHIPINFO_FAMILY_MSM8996,  /**< Chip family */
  false,                       /**< Exact match for version? */
  0x20000,                     /**< Chip version */

  /* Device information. */
  &icbcfg_boot_prop_v2,        /**< ICB_Config_Init() prop data */
  ARRAY_SIZE(channel_map),     /**< Number of DDR channels */
  channel_map,                 /**< Map of array indicies to channel numbers */
  4,                           /**< Number of BRIC segments per slave */
  ARRAY_SIZE(map_ddr_regions), /**< Number of regions in the DDR map */
  map_ddr_regions,             /**< Array of mappable DDR regions */
  &bimc_hal_info,              /**< BIMC HAL info structure */
  &safe_reset_seg,             /**< The segment config to use while reseting segments */
  false,                       /**< Have we entered best effort mode? */
  &l2_tcm_unmap,               /**< L2 TCM unmap configuration */
  &icbcfg_boot_post_prop_v2,   /**< ICB_Config_PostInit() prop data */
};

icbcfg_device_config_type msm8996sg =
{
  /* Chip version information for this device data. */
  DALCHIPINFO_FAMILY_MSM8996SG, /**< Chip family */
  false,                        /**< Exact match for version? */
  0x0,                          /**< Chip version */

  /* Device information. */
  &icbcfg_boot_prop_v2,        /**< ICB_Config_Init() prop data */
  ARRAY_SIZE(channel_map),     /**< Number of DDR channels */
  channel_map,                 /**< Map of array indicies to channel numbers */
  4,                           /**< Number of BRIC segments per slave */
  ARRAY_SIZE(map_ddr_regions), /**< Number of regions in the DDR map */
  map_ddr_regions,             /**< Array of mappable DDR regions */
  &bimc_hal_info,              /**< BIMC HAL info structure */
  &safe_reset_seg,             /**< The segment config to use while reseting segments */
  false,                       /**< Have we entered best effort mode? */
  &l2_tcm_unmap,               /**< L2 TCM unmap configuration */
  &icbcfg_boot_post_prop_v2,   /**< ICB_Config_PostInit() prop data */
};

/* Definitions list */
icbcfg_device_config_type *configs[] =
{
  &msm8996sg,
  &msm8996_v2,
  &msm8996_v1,
};

/* Exported target definitions */
icbcfg_info_type icbcfg_info =
{
  ARRAY_SIZE(configs),
  configs,
};
