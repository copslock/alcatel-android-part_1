/*============================================================================
  FILE:         VAdcBsp.c

  OVERVIEW:     Board support package for the VADC ADC Physical Device DAL.

  DEPENDENCIES: None

                Copyright (c) 2010-2015 Qualcomm Technologies, Inc.
                All Rights Reserved.
                Qualcomm Technologies Proprietary and Confidential.
============================================================================*/
/*============================================================================
  EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.  Please
  use ISO format for dates.

  when        who  what, where, why
  ----------  ---  -----------------------------------------------------------
  2015-04-23  jjo  Add thermistor scaling.
  2015-01-22  jjo  Ported to 8996.

============================================================================*/
/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include "DalVAdc.h"
#include "VAdcHal.h"
#include "AdcInputs.h"
#include "AdcScalingUtil.h"

/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * -------------------------------------------------------------------------*/
#define ARRAY_LENGTH(a) (sizeof(a) / sizeof((a)[0]))

/*----------------------------------------------------------------------------
 * Type Declarations
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Static Function Declarations
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Global Data Definitions
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Static Variable Definitions
 * -------------------------------------------------------------------------*/

enum
{
   VADC_CONFIG_NORMAL,
   VADC_NUM_CONFIGS
};

/*
 * USB ID Resistor Table
 *
 * The first element is Voltage V_t in mV and the second element is the
 * USB ID resistor R_id in ohms.
 *
 *             V_ref ___  1800 mV
 *                      |
 *                      >
 *                 R_p  <  100 kOhms
 *                      >
 *                      |
 *                      |
 *                      |- - - V_t
 *                      |
 *                      >
 *                R_id  <
 *                      >
 *                      |
 *                      |
 *                     Gnd
 *
 */
static const AdcMapPtInt32toInt32Type adcMap_USB_ID[] =
{
   {    0,       0 },
   {   50,    2857 },
   {  100,    5882 },
   {  150,    9091 },
   {  200,   12500 },
   {  250,   16129 },
   {  300,   20000 },
   {  350,   24138 },
   {  400,   28571 },
   {  450,   33333 },
   {  500,   38462 },
   {  550,   44000 },
   {  600,   50000 },
   {  650,   56522 },
   {  700,   63636 },
   {  750,   71429 },
   {  800,   80000 },
   {  850,   89474 },
   {  900,  100000 },
   {  950,  111765 },
   { 1000,  125000 },
   { 1050,  140000 },
   { 1100,  157143 },
   { 1150,  176923 },
   { 1200,  200000 },
   { 1250,  227273 },
   { 1300,  260000 },
   { 1350,  300000 },
   { 1400,  350000 },
   { 1450,  414286 },
   { 1500,  500000 },
   { 1550,  620000 },
   { 1600,  800000 },
   { 1650, 1100000 },
   { 1700, 1700000 },
   { 1750, 3500000 }
};

/*
 * System Thermistor Table
 *
 * The first column in the table is thermistor resistance R_T in ohms
 * and the second column is the temperature in degrees C.
 *
 *               VDD ___
 *                      |
 *                      >
 *                P_PU  <
 *                      >
 *                      |
 *                      |
 *                      |- - - V_T
 *                      |
 *                      >
 *                R_T   <   100 kOhms (NTCG104EF104FB)
 *                      >
 *                      |
 *                      |
 *                     Gnd
 *
 */
static const AdcMapPtInt32toInt32Type adcMap_NTCG104EF104FB[] =
{
   { 4251000, -40 },
   { 3004900, -35 },
   { 2148900, -30 },
   { 1553800, -25 },
   { 1135300, -20 },
   {  837800, -15 },
   {  624100, -10 },
   {  469100, -5 },
   {  355600, 0 },
   {  271800, 5 },
   {  209400, 10 },
   {  162500, 15 },
   {  127000, 20 },
   {  100000, 25 },
   {   79200, 30 },
   {   63200, 35 },
   {   50700, 40 },
   {   40900, 45 },
   {   33200, 50 },
   {   27100, 55 },
   {   22200, 60 },
   {   18300, 65 },
   {   15200, 70 },
   {   12600, 75 },
   {   10600, 80 },
   {    8890, 85 },
   {    7500, 90 },
   {    6360, 95 },
   {    5410, 100 },
   {    4620, 105 },
   {    3970, 110 },
   {    3420, 115 },
   {    2950, 120 },
   {    2560, 125 }
};

/*
 * VADC operating modes and decimation filter setup.
 */
static const VAdcConfigType vAdcConfigs[] =
{
   /* VADC_CONFIG_NORMAL. Used for standard AMUX Input Channels. */
   {
      .eDecimationRatio   = VADC_DECIMATION_RATIO_1024,
      .eClockSelect       = VADC_CLOCK_SELECT_4P8_MHZ,
      .uConversionTime_us = 426
   },
};

static const VAdcChannelConfigType vAdcCalibrationChannels[] =
{
   /* Vdd */
   {
      .pName                     = "VDD",
      .uAdcHardwareChannel       = 0x0F,
      .uConfigIdx                = VADC_CONFIG_NORMAL,
      .eSettlingDelay            = VADC_SETTLING_DELAY_0_US,
      .eFastAverageMode          = VADC_FAST_AVERAGE_NONE,
      .bUseSequencer             = FALSE,
      .uSequencerIdx             = 0,
      .scalingFactor             = {1, 1},  /* {num, den} */
      .eScalingMethod            = VADC_SCALE_TO_MILLIVOLTS,
      .pInterpolationTable       = NULL,
      .uInterpolationTableLength = 0,
      .eCalMethod                = VADC_CAL_METHOD_RATIOMETRIC,
      .eMppConfig                = VADC_CHANNEL_MPP_CONFIG_NONE,
      .eMpp                      = PM_MPP_INVALID,
      .eChSelect                 = PM_MPP__AIN__CH_INVALID
   },

   /* Gnd */
   {
      .pName                     = "GND",
      .uAdcHardwareChannel       = 0x0E,
      .uConfigIdx                = VADC_CONFIG_NORMAL,
      .eSettlingDelay            = VADC_SETTLING_DELAY_0_US,
      .eFastAverageMode          = VADC_FAST_AVERAGE_NONE,
      .bUseSequencer             = FALSE,
      .uSequencerIdx             = 0,
      .scalingFactor             = {1, 1},  /* {num, den} */
      .eScalingMethod            = VADC_SCALE_TO_MILLIVOLTS,
      .pInterpolationTable       = NULL,
      .uInterpolationTableLength = 0,
      .eCalMethod                = VADC_CAL_METHOD_RATIOMETRIC,
      .eMppConfig                = VADC_CHANNEL_MPP_CONFIG_NONE,
      .eMpp                      = PM_MPP_INVALID,
      .eChSelect                 = PM_MPP__AIN__CH_INVALID
   },

   /* Vref1 */
   {
      .pName                     = "VREF1",
      .uAdcHardwareChannel       = 0x09,
      .uConfigIdx                = VADC_CONFIG_NORMAL,
      .eSettlingDelay            = VADC_SETTLING_DELAY_0_US,
      .eFastAverageMode          = VADC_FAST_AVERAGE_NONE,
      .bUseSequencer             = FALSE,
      .uSequencerIdx             = 0,
      .scalingFactor             = {1, 1},  /* {num, den} */
      .eScalingMethod            = VADC_SCALE_TO_MILLIVOLTS,
      .pInterpolationTable       = NULL,
      .uInterpolationTableLength = 0,
      .eCalMethod                = VADC_CAL_METHOD_ABSOLUTE,
      .eMppConfig                = VADC_CHANNEL_MPP_CONFIG_NONE,
      .eMpp                      = PM_MPP_INVALID,
      .eChSelect                 = PM_MPP__AIN__CH_INVALID
   },

   /* Vref2 */
   {
      .pName                     = "VREF2",
      .uAdcHardwareChannel       = 0x0A,
      .uConfigIdx                = VADC_CONFIG_NORMAL,
      .eSettlingDelay            = VADC_SETTLING_DELAY_0_US,
      .eFastAverageMode          = VADC_FAST_AVERAGE_NONE,
      .bUseSequencer             = FALSE,
      .uSequencerIdx             = 0,
      .scalingFactor             = {1, 1},  /* {num, den} */
      .eScalingMethod            = VADC_SCALE_TO_MILLIVOLTS,
      .pInterpolationTable       = NULL,
      .uInterpolationTableLength = 0,
      .eCalMethod                = VADC_CAL_METHOD_ABSOLUTE,
      .eMppConfig                = VADC_CHANNEL_MPP_CONFIG_NONE,
      .eMpp                      = PM_MPP_INVALID,
      .eChSelect                 = PM_MPP__AIN__CH_INVALID
   }
};

/*----------------------------------------------------------------------------
 * 8996_PM8994
 * -------------------------------------------------------------------------*/

/*
 * VAdc channel configuration.
 */
static const VAdcChannelConfigType vAdcChannels_8996_PM8994[] =
{
   /* Channel 0: VCOIN */
   {
      .pName                     = ADC_INPUT_VCOIN,
      .uAdcHardwareChannel       = 0x05,
      .uConfigIdx                = VADC_CONFIG_NORMAL,
      .eSettlingDelay            = VADC_SETTLING_DELAY_0_US,
      .eFastAverageMode          = VADC_FAST_AVERAGE_NONE,
      .bUseSequencer             = FALSE,
      .uSequencerIdx             = 0,
      .scalingFactor             = {1, 3},  /* {num, den} */
      .eScalingMethod            = VADC_SCALE_TO_MILLIVOLTS,
      .pInterpolationTable       = NULL,
      .uInterpolationTableLength = 0,
      .eCalMethod                = VADC_CAL_METHOD_ABSOLUTE,
      .eMppConfig                = VADC_CHANNEL_MPP_CONFIG_NONE,
      .eMpp                      = PM_MPP_INVALID,
      .eChSelect                 = PM_MPP__AIN__CH_INVALID
   },

   /* Channel 1: VPH_PWR */
   {
      .pName                     = ADC_INPUT_VPH_PWR,
      .uAdcHardwareChannel       = 0x07,
      .uConfigIdx                = VADC_CONFIG_NORMAL,
      .eSettlingDelay            = VADC_SETTLING_DELAY_0_US,
      .eFastAverageMode          = VADC_FAST_AVERAGE_NONE,
      .bUseSequencer             = FALSE,
      .uSequencerIdx             = 0,
      .scalingFactor             = {1, 3},  /* {num, den} */
      .eScalingMethod            = VADC_SCALE_TO_MILLIVOLTS,
      .pInterpolationTable       = NULL,
      .uInterpolationTableLength = 0,
      .eCalMethod                = VADC_CAL_METHOD_ABSOLUTE,
      .eMppConfig                = VADC_CHANNEL_MPP_CONFIG_NONE,
      .eMpp                      = PM_MPP_INVALID,
      .eChSelect                 = PM_MPP__AIN__CH_INVALID
   },

   /* Channel 2: DIE_TEMP */
   {
      .pName                     = ADC_INPUT_PMIC_THERM,
      .uAdcHardwareChannel       = 0x08,
      .uConfigIdx                = VADC_CONFIG_NORMAL,
      .eSettlingDelay            = VADC_SETTLING_DELAY_0_US,
      .eFastAverageMode          = VADC_FAST_AVERAGE_NONE,
      .bUseSequencer             = FALSE,
      .uSequencerIdx             = 0,
      .scalingFactor             = {1, 1},  /* {num, den} */
      .eScalingMethod            = VADC_SCALE_PMIC_SENSOR_TO_MILLIDEGREES,
      .pInterpolationTable       = NULL,
      .uInterpolationTableLength = 0,
      .eCalMethod                = VADC_CAL_METHOD_ABSOLUTE,
      .eMppConfig                = VADC_CHANNEL_MPP_CONFIG_NONE,
      .eMpp                      = PM_MPP_INVALID,
      .eChSelect                 = PM_MPP__AIN__CH_INVALID
   },

   /* Channel 3: PMIC_HARDWARE_ID */
   {
      .pName                     = ADC_INPUT_PMIC_HARDWARE_ID,
      .uAdcHardwareChannel       = 0x76,
      .uConfigIdx                = VADC_CONFIG_NORMAL,
      .eSettlingDelay            = VADC_SETTLING_DELAY_100_US,
      .eFastAverageMode          = VADC_FAST_AVERAGE_NONE,
      .bUseSequencer             = FALSE,
      .uSequencerIdx             = 0,
      .scalingFactor             = {1, 1},  /* {num, den} */
      .eScalingMethod            = VADC_SCALE_TO_MILLIVOLTS,
      .pInterpolationTable       = NULL,
      .uInterpolationTableLength = 0,
      .eCalMethod                = VADC_CAL_METHOD_RATIOMETRIC,
      .eMppConfig                = VADC_CHANNEL_MPP_CONFIG_NONE,
      .eMpp                      = PM_MPP_INVALID,
      .eChSelect                 = PM_MPP__AIN__CH_INVALID
   },

   /* Channel 4: SYS_THERM_1 - AMUX1 */
   {
      .pName                     = ADC_INPUT_SYS_THERM1,
      .uAdcHardwareChannel       = 0x73,
      .uConfigIdx                = VADC_CONFIG_NORMAL,
      .eSettlingDelay            = VADC_SETTLING_DELAY_100_US,
      .eFastAverageMode          = VADC_FAST_AVERAGE_NONE,
      .bUseSequencer             = FALSE,
      .uSequencerIdx             = 0,
      .scalingFactor             = {1, 1},  /* {num, den} */
      .eScalingMethod            = VADC_SCALE_THERMISTOR,
      .pInterpolationTable       = adcMap_NTCG104EF104FB,
      .uInterpolationTableLength = ARRAY_LENGTH(adcMap_NTCG104EF104FB),
      .uPullUp                   = 100000,
      .eCalMethod                = VADC_CAL_METHOD_RATIOMETRIC,
      .eMppConfig                = VADC_CHANNEL_MPP_CONFIG_NONE,
      .eMpp                      = PM_MPP_INVALID,
      .eChSelect                 = PM_MPP__AIN__CH_INVALID
   },

   /* Channel 5: SYS_THERM_2 - AMUX2 */
   {
      .pName                     = ADC_INPUT_SYS_THERM2,
      .uAdcHardwareChannel       = 0x74,
      .uConfigIdx                = VADC_CONFIG_NORMAL,
      .eSettlingDelay            = VADC_SETTLING_DELAY_100_US,
      .eFastAverageMode          = VADC_FAST_AVERAGE_NONE,
      .bUseSequencer             = FALSE,
      .uSequencerIdx             = 0,
      .scalingFactor             = {1, 1},  /* {num, den} */
      .eScalingMethod            = VADC_SCALE_THERMISTOR,
      .pInterpolationTable       = adcMap_NTCG104EF104FB,
      .uInterpolationTableLength = ARRAY_LENGTH(adcMap_NTCG104EF104FB),
      .uPullUp                   = 100000,
      .eCalMethod                = VADC_CAL_METHOD_RATIOMETRIC,
      .eMppConfig                = VADC_CHANNEL_MPP_CONFIG_NONE,
      .eMpp                      = PM_MPP_INVALID,
      .eChSelect                 = PM_MPP__AIN__CH_INVALID
   },

   /* Channel 6: PA_THERM - AMUX3 */
   {
      .pName                     = ADC_INPUT_PA_THERM,
      .uAdcHardwareChannel       = 0x75,
      .uConfigIdx                = VADC_CONFIG_NORMAL,
      .eSettlingDelay            = VADC_SETTLING_DELAY_100_US,
      .eFastAverageMode          = VADC_FAST_AVERAGE_NONE,
      .bUseSequencer             = FALSE,
      .uSequencerIdx             = 0,
      .scalingFactor             = {1, 1},  /* {num, den} */
      .eScalingMethod            = VADC_SCALE_THERMISTOR,
      .pInterpolationTable       = adcMap_NTCG104EF104FB,
      .uInterpolationTableLength = ARRAY_LENGTH(adcMap_NTCG104EF104FB),
      .uPullUp                   = 100000,
      .eCalMethod                = VADC_CAL_METHOD_RATIOMETRIC,
      .eMppConfig                = VADC_CHANNEL_MPP_CONFIG_NONE,
      .eMpp                      = PM_MPP_INVALID,
      .eChSelect                 = PM_MPP__AIN__CH_INVALID
   },

   /* Channel 7: PA_THERM1 - AMUX4 */
   {
      .pName                     = ADC_INPUT_PA_THERM1,
      .uAdcHardwareChannel       = 0x77,
      .uConfigIdx                = VADC_CONFIG_NORMAL,
      .eSettlingDelay            = VADC_SETTLING_DELAY_100_US,
      .eFastAverageMode          = VADC_FAST_AVERAGE_NONE,
      .bUseSequencer             = FALSE,
      .uSequencerIdx             = 0,
      .scalingFactor             = {1, 1},  /* {num, den} */
      .eScalingMethod            = VADC_SCALE_THERMISTOR,
      .pInterpolationTable       = adcMap_NTCG104EF104FB,
      .uInterpolationTableLength = ARRAY_LENGTH(adcMap_NTCG104EF104FB),
      .uPullUp                   = 100000,
      .eCalMethod                = VADC_CAL_METHOD_RATIOMETRIC,
      .eMppConfig                = VADC_CHANNEL_MPP_CONFIG_NONE,
      .eMpp                      = PM_MPP_INVALID,
      .eChSelect                 = PM_MPP__AIN__CH_INVALID
   },

   /* Channel 8: SYS_THERM_3 - AMUX5 */
   {
      .pName                     = ADC_INPUT_SYS_THERM3,
      .uAdcHardwareChannel       = 0x78,
      .uConfigIdx                = VADC_CONFIG_NORMAL,
      .eSettlingDelay            = VADC_SETTLING_DELAY_100_US,
      .eFastAverageMode          = VADC_FAST_AVERAGE_NONE,
      .bUseSequencer             = FALSE,
      .uSequencerIdx             = 0,
      .scalingFactor             = {1, 1},  /* {num, den} */
      .eScalingMethod            = VADC_SCALE_THERMISTOR,
      .pInterpolationTable       = adcMap_NTCG104EF104FB,
      .uInterpolationTableLength = ARRAY_LENGTH(adcMap_NTCG104EF104FB),
      .uPullUp                   = 100000,
      .eCalMethod                = VADC_CAL_METHOD_RATIOMETRIC,
      .eMppConfig                = VADC_CHANNEL_MPP_CONFIG_NONE,
      .eMpp                      = PM_MPP_INVALID,
      .eChSelect                 = PM_MPP__AIN__CH_INVALID
   },

   /* Channel 9: XO_THERM_MV */
   {
      .pName                     = ADC_INPUT_XO_THERM_MV,
      .uAdcHardwareChannel       = 0x7C,
      .uConfigIdx                = VADC_CONFIG_NORMAL,
      .eSettlingDelay            = VADC_SETTLING_DELAY_500_US,
      .eFastAverageMode          = VADC_FAST_AVERAGE_NONE,
      .bUseSequencer             = FALSE,
      .uSequencerIdx             = 0,
      .scalingFactor             = {1, 1},  /* {num, den} */
      .eScalingMethod            = VADC_SCALE_TO_MILLIVOLTS,
      .pInterpolationTable       = NULL,
      .uInterpolationTableLength = 0,
      .eCalMethod                = VADC_CAL_METHOD_RATIOMETRIC,
      .eMppConfig                = VADC_CHANNEL_MPP_CONFIG_NONE,
      .eMpp                      = PM_MPP_INVALID,
      .eChSelect                 = PM_MPP__AIN__CH_INVALID
   },

   /* Channel 10: USB_DATA */
   {
      .pName                     = ADC_INPUT_USB_DATA,
      .uAdcHardwareChannel       = 0x04,
      .uConfigIdx                = VADC_CONFIG_NORMAL,
      .eSettlingDelay            = VADC_SETTLING_DELAY_0_US,
      .eFastAverageMode          = VADC_FAST_AVERAGE_NONE,
      .bUseSequencer             = FALSE,
      .uSequencerIdx             = 0,
      .scalingFactor             = {1, 3},  /* {num, den} */
      .eScalingMethod            = VADC_SCALE_TO_MILLIVOLTS,
      .pInterpolationTable       = NULL,
      .uInterpolationTableLength = 0,
      .eCalMethod                = VADC_CAL_METHOD_ABSOLUTE,
      .eMppConfig                = VADC_CHANNEL_MPP_CONFIG_NONE,
      .eMpp                      = PM_MPP_INVALID,
      .eChSelect                 = PM_MPP__AIN__CH_INVALID
   },

   /* Channel 11: USB_ID */
   {
      .pName                     = ADC_INPUT_USB_ID,
      .uAdcHardwareChannel       = 0x79,
      .uConfigIdx                = VADC_CONFIG_NORMAL,
      .eSettlingDelay            = VADC_SETTLING_DELAY_500_US,
      .eFastAverageMode          = VADC_FAST_AVERAGE_NONE,
      .bUseSequencer             = FALSE,
      .uSequencerIdx             = 0,
      .scalingFactor             = {1, 1},  /* {num, den} */
      .eScalingMethod            = VADC_SCALE_INTERPOLATE_FROM_MILLIVOLTS,
      .pInterpolationTable       = adcMap_USB_ID,
      .uInterpolationTableLength = ARRAY_LENGTH(adcMap_USB_ID),
      .eCalMethod                = VADC_CAL_METHOD_RATIOMETRIC,
      .eMppConfig                = VADC_CHANNEL_MPP_CONFIG_NONE,
      .eMpp                      = PM_MPP_INVALID,
      .eChSelect                 = PM_MPP__AIN__CH_INVALID
   },
};

const VAdcBspType VAdcBsp_8996_PM8994[] =
{
   {
      .eAccessPriority      = SPMI_BUS_ACCESS_PRIORITY_LOW,
      .uSlaveId             = 0,
      .uPmicDevice          = 0,
      .uPeripheralID        = 0x31,
      .uMasterID            = 0,
      .bUsesInterrupts      = FALSE,
      .bHasTM               = FALSE,
      .uMinDigMinor         = 0,
      .uMinDigMajor         = 0,
      .uMinAnaMinor         = 0,
      .uMinAnaMajor         = 0,
      .uPerphType           = 0x08,
      .uPerphSubType        = 0x01,
      .uVrefP_mv            = 1800,
      .uVrefN_mv            = 0,
      .uVref1_mv            = 625,
      .uVref2_mv            = 1250,
      .uReadTimeout_us      = 500000,
      .uLDOSettlingTime_us  = 200,
      .uNumSequencerConfigs = 0,
      .paSequencerParams    = NULL,
      .uNumConfigs          = ARRAY_LENGTH(vAdcConfigs),
      .paConfigs            = vAdcConfigs,
      .uNumChannels         = ARRAY_LENGTH(vAdcChannels_8996_PM8994),
      .paChannels           = vAdcChannels_8996_PM8994,
      .uNumCalChannels      = ARRAY_LENGTH(vAdcCalibrationChannels),
      .paCalChannels        = vAdcCalibrationChannels
   }
};

/*----------------------------------------------------------------------------
 * 8996_PMI8994
 * -------------------------------------------------------------------------*/

/*
 * VAdc channel configuration.
 */
static const VAdcChannelConfigType vAdcChannels_8996_PMI8994[] =
{
   /* Channel 0: USBIN */
   {
      .pName                     = ADC_INPUT_USB_IN,
      .uAdcHardwareChannel       = 0x00,
      .uConfigIdx                = VADC_CONFIG_NORMAL,
      .eSettlingDelay            = VADC_SETTLING_DELAY_0_US,
      .eFastAverageMode          = VADC_FAST_AVERAGE_NONE,
      .bUseSequencer             = FALSE,
      .uSequencerIdx             = 0,
      .scalingFactor             = {1, 20},  /* {num, den} */
      .eScalingMethod            = VADC_SCALE_TO_MILLIVOLTS,
      .pInterpolationTable       = NULL,
      .uInterpolationTableLength = 0,
      .eCalMethod                = VADC_CAL_METHOD_ABSOLUTE,
      .eMppConfig                = VADC_CHANNEL_MPP_CONFIG_NONE,
      .eMpp                      = PM_MPP_INVALID,
      .eChSelect                 = PM_MPP__AIN__CH_INVALID
   },

   /* Channel 1: DCIN */
   {
      .pName                     = ADC_INPUT_DC_IN,
      .uAdcHardwareChannel       = 0x01,
      .uConfigIdx                = VADC_CONFIG_NORMAL,
      .eSettlingDelay            = VADC_SETTLING_DELAY_0_US,
      .eFastAverageMode          = VADC_FAST_AVERAGE_NONE,
      .bUseSequencer             = FALSE,
      .uSequencerIdx             = 0,
      .scalingFactor             = {1, 20},  /* {num, den} */
      .eScalingMethod            = VADC_SCALE_TO_MILLIVOLTS,
      .pInterpolationTable       = NULL,
      .uInterpolationTableLength = 0,
      .eCalMethod                = VADC_CAL_METHOD_ABSOLUTE,
      .eMppConfig                = VADC_CHANNEL_MPP_CONFIG_NONE,
      .eMpp                      = PM_MPP_INVALID,
      .eChSelect                 = PM_MPP__AIN__CH_INVALID
   },

   /* Channel 2: USB_DP */
   {
      .pName                     = ADC_INPUT_USB_DP,
      .uAdcHardwareChannel       = 0x43,
      .uConfigIdx                = VADC_CONFIG_NORMAL,
      .eSettlingDelay            = VADC_SETTLING_DELAY_0_US,
      .eFastAverageMode          = VADC_FAST_AVERAGE_NONE,
      .bUseSequencer             = FALSE,
      .uSequencerIdx             = 0,
      .scalingFactor             = {1, 3},  /* {num, den} */
      .eScalingMethod            = VADC_SCALE_TO_MILLIVOLTS,
      .pInterpolationTable       = NULL,
      .uInterpolationTableLength = 0,
      .eCalMethod                = VADC_CAL_METHOD_ABSOLUTE,
      .eMppConfig                = VADC_CHANNEL_MPP_CONFIG_NONE,
      .eMpp                      = PM_MPP_INVALID,
      .eChSelect                 = PM_MPP__AIN__CH_INVALID
   },

   /* Channel 3: USB_DN */
   {
      .pName                     = ADC_INPUT_USB_DN,
      .uAdcHardwareChannel       = 0x44,
      .uConfigIdx                = VADC_CONFIG_NORMAL,
      .eSettlingDelay            = VADC_SETTLING_DELAY_0_US,
      .eFastAverageMode          = VADC_FAST_AVERAGE_NONE,
      .bUseSequencer             = FALSE,
      .uSequencerIdx             = 0,
      .scalingFactor             = {1, 3},  /* {num, den} */
      .eScalingMethod            = VADC_SCALE_TO_MILLIVOLTS,
      .pInterpolationTable       = NULL,
      .uInterpolationTableLength = 0,
      .eCalMethod                = VADC_CAL_METHOD_ABSOLUTE,
      .eMppConfig                = VADC_CHANNEL_MPP_CONFIG_NONE,
      .eMpp                      = PM_MPP_INVALID,
      .eChSelect                 = PM_MPP__AIN__CH_INVALID
   },
};

const VAdcBspType VAdcBsp_8996_PMI8994[] =
{
   {
      .eAccessPriority      = SPMI_BUS_ACCESS_PRIORITY_LOW,
      .uSlaveId             = 2,
      .uPmicDevice          = 1,
      .uPeripheralID        = 0x31,
      .uMasterID            = 0,
      .bUsesInterrupts      = FALSE,
      .bHasTM               = FALSE,
      .uMinDigMinor         = 0,
      .uMinDigMajor         = 0,
      .uMinAnaMinor         = 0,
      .uMinAnaMajor         = 0,
      .uPerphType           = 0x08,
      .uPerphSubType        = 0x40,
      .uVrefP_mv            = 1800,
      .uVrefN_mv            = 0,
      .uVref1_mv            = 625,
      .uVref2_mv            = 1250,
      .uReadTimeout_us      = 500000,
      .uLDOSettlingTime_us  = 200,
      .uNumSequencerConfigs = 0,
      .paSequencerParams    = NULL,
      .uNumConfigs          = ARRAY_LENGTH(vAdcConfigs),
      .paConfigs            = vAdcConfigs,
      .uNumChannels         = ARRAY_LENGTH(vAdcChannels_8996_PMI8994),
      .paChannels           = vAdcChannels_8996_PMI8994,
      .uNumCalChannels      = ARRAY_LENGTH(vAdcCalibrationChannels),
      .paCalChannels        = vAdcCalibrationChannels
   }
};

