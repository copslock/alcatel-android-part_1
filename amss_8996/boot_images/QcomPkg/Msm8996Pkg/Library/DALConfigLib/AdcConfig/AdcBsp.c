/*============================================================================
  FILE:         AdcBsp.c

  OVERVIEW:     Board support package for the ADC for 8996.

  DEPENDENCIES: None

                Copyright (c) 2014-2015 Qualcomm Technologies, Inc.
                All Rights Reserved.
                Qualcomm Technologies Confidential and Proprietary.
============================================================================*/
/*============================================================================
  EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.  Please
  use ISO format for dates.

  when        who  what, where, why
  ----------  ---  -----------------------------------------------------------
  2015-05-05  jjo  Use designated initializers.
  2014-04-02  jjo  Initial version.

============================================================================*/
/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include "AdcBsp.h"

/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * -------------------------------------------------------------------------*/
#define ARRAY_LENGTH(a) (sizeof(a) / sizeof((a)[0]))

/*----------------------------------------------------------------------------
 * Type Declarations
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Static Function Declarations
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Global Data Definitions
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Static Variable Definitions
 * -------------------------------------------------------------------------*/

/*
 * 8996_PM8994
 */
static const AdcPhysicalDeviceType adcPhysicalDevices_8996_PM8994[] =
{
   {
      .pszDevName = "/core/hwengines/adc/pmic_0/vadc",
   },
};

const AdcBspType AdcBsp_8996_PM8994[] =
{
   {
      .paAdcPhysicalDevices = adcPhysicalDevices_8996_PM8994,
      .uNumDevices          = ARRAY_LENGTH(adcPhysicalDevices_8996_PM8994)
   }
};

/*
 * 8996_PM8994_PMI8994
 */
static const AdcPhysicalDeviceType adcPhysicalDevices_8996_PM8994_PMI8994[] =
{
   /* VADC PM8994 */
   {
      .pszDevName = "/core/hwengines/adc/pmic_0/vadc",
   },
   /* VADC PMI8994 */
   {
      .pszDevName = "/core/hwengines/adc/pmic_1/vadc",
   },
};

const AdcBspType AdcBsp_8996_PM8994_PMI8994[] =
{
   {
      .paAdcPhysicalDevices = adcPhysicalDevices_8996_PM8994_PMI8994,
      .uNumDevices          = ARRAY_LENGTH(adcPhysicalDevices_8996_PM8994_PMI8994)
   }
};

