#ifndef APT_SHARED_FUNCTIONS_H
#define APT_SHARED_FUNCTIONS_H

/*===========================================================================
Copyright (c) 2014, 2015 Qualcomm Technologies, Inc.
                                APT Shared Functions
                                Header File

GENERAL DESCRIPTION
  This header file contains declarations and definitions for apt's shared 
  functions. 

 
============================================================================*/

/*===========================================================================

                           EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

when       who      what, where, why
--------   ----     ----------------------------------------------------------
10/29/15   pb       created separate type for pmic apis and added them in separate file   
06/17/15   zf       re-Version  for STI
04/23/15   kpa      Added pmic and hotplug apis
10/01/14   ck       Removed Hotplug functions
09/30/14   ck       Added boot_err_fatal
09/30/14   ck       Removing efs functions as EFS driver properly split
09/23/14   ck       Added boot_extern_crypto_interface functions
09/23/14   ck       Added boot_extern_efs_interface functions
09/23/14   ck       Added boot_extern_seccfg_interface functions
08/05/14   ck       Updated boot_clobber_check_global_whitelist_range prototype
07/14/14   ck       Initial creation
============================================================================*/

/*=============================================================================

                            INCLUDE FILES FOR MODULE

=============================================================================*/

#include "boot_comdef.h"
#include "boot_cache_mmu.h"
#include "boot_clobber_prot.h"
#include "boot_ddr_info.h"
#include "boot_dload.h"
#include "boot_error_handler.h"
#include "boot_extern_crypto_interface.h"
#include "boot_extern_hotplug_interface.h"
#include "boot_extern_pmic_interface.h"
#include "boot_extern_seccfg_interface.h"
#include "boot_flash_dev_if.h"
#include "boot_logger.h"
#include "boot_sdcc.h"
#include "boot_visual_indication.h"
#include "boothw_target.h"
#include "sbl1_hw.h"
#include "sbl1_mc.h"

/*== SUBSYSTEM TEST LEVEL HEADER ADD HERE ==*/
#include <pmic/pmic_shared_functions.h>




/*=============================================================================

            LOCAL DEFINITIONS AND DECLARATIONS FOR MODULE

This section contains local definitions for constants, macros, types,
variables and other items needed by this module.

=============================================================================*/
#define SHARED_FUNCTIONS_VERSION           1
#define SHARED_FUNCTIONS_MAGIC_COOKIE_1    0x9420C107
#define SHARED_FUNCTIONS_MAGIC_COOKIE_2    0x9B9A68DF

/* Single structure that houses all of the functions to be shared between
   XBLLoader and any other image running at the same time as XBLLoader.
   I.E. Loader_test */

 
typedef struct
{
  uint64 version;
  uint32 magic_cookie_1;
  uint32 magic_cookie_2;

   void (* boot_log_message)(char *);
  uint32(* boot_uart_tx)(char*, uint32);
  uint32(* boot_uart_rx)(char*, uint32);
  DALResult(*DALSYS_Malloc)(uint32, void **);
  DALResult(*DALSYS_Free)(void *pmem);
  void *(* DALSYS_memset)(void * , UINT8 , UINTN);
  UINTN (* DALSYS_memscpy)(VOID *, UINTN ,  VOID *, UINTN);
  void(*DALSYS_BusyWait)(uint32);

  /*== SUBSYSTEM TEST LEVEL SHARED FUNCTION TYPE ADD HERE ==*/

  /*API's for PMIC testing */
  pmtest_shared_function_type pmic_shared_apis;

  
  /*API's for Sysdrv testing */


  /*API's for <subsystem> testing */


} apt_shared_functions_type;

static apt_shared_functions_type * shared_functions_table =
 (apt_shared_functions_type *) SCL_SBL1_SHARED_FUNCTIONS_TABLE_BASE;

#endif /* APT_SHARED_FUNCTIONS */


