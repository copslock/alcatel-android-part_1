//------------------------------------------------------------------------------ 
//
// Copyright (c) 2014, Qualcomm Technologies Inc. All rights reserved.
//
//------------------------------------------------------------------------------

//=============================================================================
//                              EDIT HISTORY
//
//
// when       who     what, where, why
// --------   ---     ---------------------------------------------------------
// 07/28/15   rp      Initial revision
//
//=============================================================================
#include <Library/PcdLib.h>
#include <AutoGen.h>

  IMPORT  DDRTrngMain
  EXPORT  _ModuleEntryPoint
        
  PRESERVE8
  AREA    ModuleEntryPoint, CODE, READONLY
  

_ModuleEntryPoint

  b       DDRTrngMain

ShouldNeverGetHere
  /* sbl1_entry should never return */
  b       ShouldNeverGetHere
  END

