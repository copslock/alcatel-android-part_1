/*=============================================================================
  
                     SCATTER LOADING DESCRIPTION FILE
  
  Copyright 2014 - 2016 by Qualcomm Technologies, Inc. All Rights Reserved.
  
  GENERAL DESCRIPTION
  
  The scatter loading description file is used to define the SBL1 memory map.
=============================================================================*/
/*=============================================================================
  
                            EDIT HISTORY FOR FILE
   This section contains comments describing changes made to the module.
   Notice that changes are listed in reverse chronological order.
  
  
  when       who     what, where, why
  --------   ---     -------------------------------------------------------
  01/18/16   eo      Relocate COMMON to OCIMEM region   
  10/27/15   elt     Added regions for boot logger time markers
  10/14/15   kpa     Relocate stack to reclaim bootrom buffer, remove SBL1_STACK
  10/08/15   kpa     Relocate pmic ZI from L2 TCM to ocimem
  08/04/15   kpa     Reorganize ocimem ZI buffer objs
  06/08/15   wek     Merge main linkger script to deviceprogrammer.
  02/26/15   ck      Added missing busywait and rodata allocations
  02/20/15   kpa     Code cleanup. 
  02/12/15   kpa     Update Debug/SDI region.  
  02/12/15   kpa     Add assert to check RPM coderam buffer size.  
  02/12/15   kpa     Add XBL code  region to rpm code ram buffer.  
  01/31/15   kpa     Updates to zero initialize OCIMEM ZI buffer.  
  01/21/15   kpa     Move pre-ddr init page tables to code ram, add ocimem buffer
  11/20/14   ck      Removed references to USB as it has moved to XBLRamDump
  10/22/14   ck      Added .bss to page table regions to ensure they are ZI
  10/01/14   kpa     Include Target_cust.h instead of boot_target_scl.h
  08/18/14   ck      Cleaned up SBL regions and added region protections
  08/08/14   ck      Renamed and resized SBL1_DLOAD_ZI as contents have moved to XBLRamDump.
  06/26/14   kedara  Update sections.
  05/15/14   kedara  Inital revision.
=============================================================================*/

#include "../../Include/Target_cust.h"

ENTRY(_main)

PHDRS {
  CODE_ROM PT_LOAD;
  CODE_RAM PT_LOAD;
  CODE_RAM_ZI PT_LOAD;
  BOOT_DDR PT_LOAD;
  BOOT_SYSTEM_DEBUG PT_LOAD;
  BOOT_SYSTEM_DEBUG_DATA PT_LOAD;
  BOOT_RPM_CODE_RAM_BUFFER PT_LOAD;
  BOOT_OCIMEM_DATA PT_LOAD;
}

SECTIONS
{

  /*====================== Sections in OCIMEM ===============================*/

  /* Area of OCIMEM used for XBL code/data */
  SBL1_OCIMEM_DATA  SCL_SBL1_OCIMEM_DATA_BASE :
  {
    Image$$SBL1_OCIMEM_DATA$$Base = . ;
    
    *(.bss.BOOT_OCIMEM_PAGE_TABLE_ZONE)
    qusb_*.o* (.bss.*)

    *(.bss.LITE_CHANNEL_BUFFER_A_SECTION)
    *(.bss.LITE_CHANNEL_BUFFER_B_SECTION)
    *(.bss.TRANSMIT_BUFFER_SECTION)

    *(.bss.BOOT_INTERNAL_HEAP)

    *pm*.o*(.bss*)
    *PmicArb*.o*(.bss*)
    ASSERT(SIZEOF(SBL1_OCIMEM_DATA) <= SCL_SBL1_OCIMEM_DATA_SIZE,   "SBL1_OCIMEM_DATA is too large for DeviceProgrammerLite [ERROR]");
  } : BOOT_OCIMEM_DATA
  Image$$SBL1_OCIMEM_DATA$$ZI$$Length = SIZEOF(SBL1_OCIMEM_DATA);
  
   /* Area of RPM Coderam used to hold L2 and L3 page tables because PBL does not supply
     enough memory to expand on what it created. */
  SBL1_PAGE_TABLE  SCL_SBL1_PAGE_TABLE_BASE :
  {
    *(.bss.BOOT_PAGE_TABLE_ZONE)
    ASSERT(SIZEOF(SBL1_PAGE_TABLE) <= SCL_SBL1_PAGE_TABLE_SIZE,   "SBL1_PAGE_TABLE is too large for DeviceProgrammerLite [ERROR]");
  } : BOOT_OCIMEM_DATA


  SBL1_SHARED_FUNCTIONS_TABLE SCL_SBL1_SHARED_FUNCTIONS_TABLE_BASE :
  {
    Image$$SBL1_SHARED_FUNCTIONS_TABLE$$Base = . ;
    . = . + SCL_SBL1_SHARED_FUNCTIONS_TABLE_SIZE;
  } : BOOT_OCIMEM_DATA
  Image$$SBL1_SHARED_FUNCTIONS_TABLE$$Length = SIZEOF(SBL1_SHARED_FUNCTIONS_TABLE);
  
  SBL1_OCIMEM_DEVICEPROG_DATA SCL_DEVICEPROG_ZI_BASE: 
  {
    *fs_hotplug_parser.obj (.bss);
    *boot_sdcc_hotplug.obj (.bss);
    *fs_hotplug*.o* (.bss)
    *fs_blockdev*.o* (.bss)
    qusb_*.o* (COMMON)
    ddr_phy_config*.o* (.data.*)
    qusb_*.o* (.data.*)
    *deviceprogrammer*.o* (.stub .constdata .rodata .rodata.* .gnu.linkonce.r.* .data*)
    *deviceprogrammer_*.obj (.bss .bss.*)
    *deviceprogrammer_*.obj (COMMON)
    *ufs_bsp_boot_8996.obj (COMMON);
    *ufs_memory.obj (COMMON);
    *ufs_memory.o* (.bss)
    *sdcc_*.o* (.bss)
    *sdcc_*.o* (COMMON)
    flash_*.o* (.bss)
    flash_*.o* (COMMON)
    quadspi.o* (.bss)
    quadspi.o* (COMMON)

    ASSERT(SIZEOF(SBL1_OCIMEM_DEVICEPROG_DATA) <= SCL_DEVICEPROG_ZI_SIZE, "Invalid size of SBL1_OCIMEM_DEVICEPROG_DATA Section for DeviceProgrammerDDR [ERROR]");
  } : BOOT_OCIMEM_DATA


  /*====================== Sections in L2 TCM ===============================*/
  /*  This section contains SBL1's code and ro data */
  SBL1_ROM SCL_SBL1_CODE_BASE: 
  {
    *(SBL1_VECTOR_TABLE)
    *sbl1_Aarch64.o* (SBL1_ENTRY)
    *(BOOT_UTIL_ASM)
    *(RO)
    *(ARM_MMU)
    *(.gcc_except_table  .got .got.plt )
    /* RO DATA */
    *(.gnu.linkonce.r.*)

    *(EXCLUDE_FILE( *boot_extern_debug_interface*.o*  *ClockSDI*.o* *ddr_sdi_wrapper*.o* *ddrss_init_sdi*.o* busywait*.o* bimc*.o* *training*.o* *ddrss*.o* *dtts_load_ram*.o* *icb_sdi*.o*) .rodata* )
    *(EXCLUDE_FILE( *boot_extern_debug_interface*.o*  *ClockSDI*.o* *ddr_sdi_wrapper*.o* *ddrss_init_sdi*.o* busywait*.o* bimc*.o* *training*.o* *ddrss*.o* *dtts_load_ram*.o* *icb_sdi*.o* ) .constdata* )
    *(EXCLUDE_FILE( *boot_extern_debug_interface*.o*  *ClockSDI*.o* *ddr_sdi_wrapper*.o* *ddrss_init_sdi*.o* busywait*.o* bimc*.o* *training*.o* *ddrss*.o* *dtts_load_ram*.o* *icb_sdi*.o* PlatformInfoLoader.o* *TLMM*.o* DalVAdc.o* PmicArb*.o*) .text*)
    *(EXCLUDE_FILE( *boot_extern_debug_interface*.o*  *ClockSDI*.o* *ddr_sdi_wrapper*.o* *ddrss_init_sdi*.o* busywait*.o* bimc*.o* *training*.o* *ddrss*.o* *dtts_load_ram*.o* *icb_sdi*.o* ) .stub )

     ASSERT(SIZEOF(SBL1_ROM) <= SCL_SBL1_CODE_SIZE, "Invalid size of SBL1_ROM Section for DeviceProgrammerLite [ERROR]");
  } : CODE_ROM


  SBL1_DATA_RW SCL_SBL1_DATA_BASE :
  {
    Image$$SBL1_DATA_RW$$Base = . ;

    *(ERR_DATA_PTR ERR_DATA  .tdata)

    *(EXCLUDE_FILE( *boot_extern_debug_interface*.o* *ClockSDI*.o* *ddr_sdi_wrapper*.o* *ddrss_init_sdi*.o* busywait*.o* *icb_sdi*.o* *deviceprogrammer*.o* ddr_phy_config*.obj qusb_*.o*) .data* )
 
    ASSERT(SIZEOF(SBL1_DATA_RW) <= SCL_SBL1_DATA_SIZE, "Invalid size of SBL1_DATA_RW Section for DeviceProgrammerLite [ERROR]");
  } : CODE_RAM
  Image$$SBL1_DATA_RW$$Length = SIZEOF(SBL1_DATA_RW);

  /*====================== Sections in RPM Code RAM =========================*/

  /* XBL code to be loaded into RPM coderam. */
  SBL1_RPM_ROM SCL_SBL1_RPM_CODE_BASE :
  {
    *(DDRSS_MEM_RDWR)
    *ddrss*.o* (EXCLUDE_FILE( *ddrss_init_sdi*.o* ) .text*) 
    *training*.o* (.text*);
    bimc*.o* (.text*);    
    *dtts_load_ram*.o* (.text*);
    
    *ddrss*.o* (EXCLUDE_FILE( *ddrss_init_sdi*.o* ) .rodata*) 
    *training*.o* (.rodata*);
    bimc*.o* (.rodata*);    
    *dtts_load_ram*.o* (.rodata*);
    
    *ddrss*.o* (EXCLUDE_FILE( *ddrss_init_sdi*.o* ) .constdata*) 
    *training*.o* (.constdata*);
    bimc*.o* (.constdata*);    
    *dtts_load_ram*.o* (.constdata*);   

    *ddrss*.o* (EXCLUDE_FILE( *ddrss_init_sdi*.o* ) .stub*) 
    *training*.o* (.stub*);
    bimc*.o* (.stub*);    
    *dtts_load_ram*.o* (.stub*);     
    
    ASSERT(SIZEOF(SBL1_RPM_ROM) <= SCL_SBL1_RPM_CODE_SIZE,   "SBL1_RPM_ROM is too large for DeviceProgrammerLite [ERROR]");
  } : BOOT_RPM_CODE_RAM_BUFFER

  TBSS :{*(.tbss) }

  /*====================== Sections in DDR =========================*/

  /* This section contains all ZI that can be allocated in DDR. */  
  SBL1_DDR_ZI SCL_SBL1_DDR_ZI_BASE :
  {
    Image$$SBL1_DDR_ZI$$Base = . ;

    /* Placeholder to reserve space for the boot logger's meta info data. */
    Image$$SBL1_DDR_LOG_META_INFO_SECTION$$Base = .;
    . = . + SCL_SBL1_BOOT_LOG_META_INFO_SIZE;

    /* Placeholder to reserve buffer space for the boot logger's log messages */
    Image$$SBL1_DDR_LOG_BUF_SECTION$$Base = .;  
    . = . + SCL_SBL1_DDR_BOOT_LOG_BUF_SIZE;

    * (.bss.BOOT_DDR_ZI_DATA_ZONE)
    *fs_*.o (.bss*)
    *hfat*.o (.bss*)
    *Ce*.o (.bss*)
    *(.bss.BOOT_EXTERNAL_HEAP)
    *(.bss.BOOT_DDR_ZI_ZONE)
    ASSERT(SIZEOF(SBL1_DDR_ZI) <= SCL_SBL1_DDR_ZI_SIZE, "SBL1_DDR_ZI is too large for DeviceProgrammerLite [ERROR]");
  } : BOOT_DDR
  Image$$SBL1_DDR_ZI$$ZI$$Length = SIZEOF(SBL1_DDR_ZI);

  SBL1_DEVICEPROG_USB_ZI : ALIGN(8)
  {
    Image$$SBL1_DEVICEPROGRAMMER_USB_ZI$$Base = .;
    *(.bss.DDR_CHANNEL_BUFFER_A_SECTION)
    *(.bss.DDR_CHANNEL_BUFFER_B_SECTION)
  }
  Image$$SBL1_DEVICEPROGRAMMER_USB_ZI$$ZI$$Length = SIZEOF(SBL1_DEVICEPROG_USB_ZI);

  /* This section contains L1 and L2 page table that's allocated in DDR.
   SCL_SBL1_DDR_PAGE_TABLE_BASE must be 16k aligned */
  SBL1_DDR_PAGE_TABLE  SCL_SBL1_DDR_PAGE_TABLE_BASE :
 {
    *(.bss.BOOT_DDR_PAGE_TABLE_ZONE)
    ASSERT(SIZEOF(SBL1_DDR_PAGE_TABLE) <= SCL_SBL1_DDR_PAGE_TABLE_SIZE,   "SBL1_DDR_PAGE_TABLE is too large for DeviceProgrammerLite [ERROR]");
  } : BOOT_DDR


  /* This section contains the uncached ZI region */
  SBL1_DDR_UNCACHED_ZI  SCL_SBL1_DDR_UNCACHED_ZI_BASE :
  {
    Image$$SBL1_DDR_UNCACHED_ZI$$Base = . ;
    *(BOOT_DDR_UNCACHED_ZI)  
    *(.bss.BOOT_DDR_UNCACHED_ZI_ZONE)

    ASSERT(SIZEOF(SBL1_DDR_UNCACHED_ZI) <= SCL_SBL1_DDR_UNCACHED_ZI_SIZE, "SBL1_DDR_UNCACHED_ZI is too large for DeviceProgrammerLite [ERROR]");

    /* Ensure that all DDR regions fit in the allocated memory space */
    ASSERT((SCL_SBL1_DDR_ZI_SIZE + SCL_SBL1_DDR_PAGE_TABLE_SIZE + SCL_SBL1_DDR_UNCACHED_ZI_SIZE) <= SCL_SBL1_DDR_DATA_SIZE, "SBL1 DDR Region Overflow for DeviceProgrammerLite [ERROR]");
  } : BOOT_DDR
  Image$$SBL1_DDR_UNCACHED_ZI$$ZI$$Length = SIZEOF(SBL1_DDR_UNCACHED_ZI);
 

  /*====================== BSS Section in L2 TCM =========================*/
 
  /* This section contains non DDR ZI. */
  /* Must be placed at EOF as it contains the *(.bss.*) catch all */
  SBL1_DATA_ZI SCL_SBL1_DATA_ZI_BASE: 
  {
    Image$$SBL1_DATA_ZI$$Base = . ;

    /* Placeholder to reserve space for the boot logger's meta info data. */
     Image$$SBL1_LOG_META_INFO_SECTION$$Base = .;
    . = . + SCL_SBL1_BOOT_LOG_META_INFO_SIZE;
    
    /* Placeholder to reserve buffer space for the boot logger's log messages */
    Image$$SBL1_LOG_BUF_SECTION$$Base = .;
    . = . +  SCL_SBL1_BOOT_LOG_BUF_SIZE;

    *(.bss.BOOT_INTERNAL_HEAP)
    *(CACHE_MMU_ZI)
    /* excludes .bss for DDR_ZI and DLOAD_ZI */    
    *(EXCLUDE_FILE(*boot_extern_debug_interface*.o* *ClockSDI*.o* *ddr_sdi_wrapper*.o* *ddrss_init_sdi*.o* busywait*.o* *boot_extern_debug_interface*.o* *icb_sdi*.o* *fs_*.o *hfat*.o *Ce*.o *sahara*.o *dload*.o ) .bss )
    *(EXCLUDE_FILE(*boot_extern_debug_interface*.o* *ClockSDI*.o* *ddr_sdi_wrapper*.o* *ddrss_init_sdi*.o* busywait*.o* *boot_extern_debug_interface*.o* *icb_sdi*.o* *fs_*.o *hfat*.o *Ce*.o *sahara*.o *dload*.o ) .bss.* )
    *(.dynbss)

    *(__libc_freeres_ptrs)
    ASSERT(SIZEOF(SBL1_DATA_ZI) <= SCL_SBL1_DATA_ZI_SIZE, "Invalid size of SBL1_DATA_ZI Section for DeviceProgrammerLite [ERROR]");

    /* Ensure SBL1 regions fit inside of allocated region */
    ASSERT((SCL_SBL1_CODE_SIZE + SCL_SBL1_DATA_SIZE) <= SCL_SBL1_IMAGE_SIZE, "SBL1 Image Size Overflow for DeviceProgrammerLite [ERROR]");
  } : CODE_RAM_ZI
  Image$$SBL1_DATA_ZI$$ZI$$Length = SIZEOF(SBL1_DATA_ZI) ;


  /* This section contains the system debug image code that came from SDI.
     It is placed in RPM Code RAM memory above RPM FW. */
  SYSTEM_DEBUG_CO_RO SCL_SYSTEM_DEBUG_CO_RO_BASE:
  {
    *boot_extern_debug_interface*.o* (.text*); 
    *ddr_sdi_wrapper*.o* (.text*); 
    *ddrss_init_sdi*.o* (.text*); 
    *ClockSDI*.o* (.text*);
    *icb_sdi*.o* (.text*);
    *busywait.o* (.text*);
    PlatformInfoLoader.o* (.text*)
    *TLMM*.o* (.text*)
    DalVAdc.o* (.text*)
    PmicArb*.o* (.text*)

    *boot_extern_debug_interface*.o* (.rodata*); 
    *ddr_sdi_wrapper*.o* (.rodata*); 
    *ddrss_init_sdi*.o* (.rodata*); 
    *ClockSDI*.o* (.rodata*);
    *icb_sdi*.o* (.rodata*);
    *busywait.o* (.rodata*);

    ASSERT(SIZEOF(SYSTEM_DEBUG_CO_RO) <= SCL_SYSTEM_DEBUG_CO_RO_SIZE, "Invalid size for SYSTEM_DEBUG_CO_RO  for DeviceProgrammerLite [ERROR]");
  } : BOOT_SYSTEM_DEBUG
  
  /* section contains system debug related data and placed above system debug 
     related CO+RO in OCIMEM.
   */
  SYSTEM_DEBUG_DATA SCL_SYSTEM_DEBUG_DATA_BASE : ALIGN(8)
  {
    *boot_extern_debug_interface*.o* (.data*);
    *boot_extern_debug_interface*.o* (.bss*);
    *ddr_sdi_wrapper*.o* (.data*);
    *ddr_sdi_wrapper*.o* (.bss*);
    *ddrss_init_sdi*.o* (.data*);
    *ddrss_init_sdi*.o* (.bss*);
    *ClockSDI*.o* (.data*);
    *ClockSDI*.o* (.bss*);
    *icb_sdi*.o* (.data*);
    *icb_sdi*.o* (.bss*);
    
    ASSERT(SIZEOF(SYSTEM_DEBUG_DATA) <= SCL_SYSTEM_DEBUG_DATA_SIZE, "Invalid size for SYSTEM_DEBUG_DATA  for DeviceProgrammerLite [ERROR]");	
  } : BOOT_SYSTEM_DEBUG_DATA
  
  
  /* This must be placed at the end or adjacent execution regions overlap */
  SYSTEM_DEBUG_QSEE_INFO SCL_SYSTEM_DEBUG_QSEE_INFO_BASE :ALIGN(8)
  { 
    Image$$SYSTEM_DEBUG_QSEE_INFO$$Base = . ;
    
  } : BOOT_SYSTEM_DEBUG_DATA
  Image$$SYSTEM_DEBUG_QSEE_INFO$$ZI$$Length = SIZEOF(SYSTEM_DEBUG_QSEE_INFO);

  .debug          0 : { *(.debug) }
  .line           0 : { *(.line) }
  /* GNU DWARF 1 extensions */
  .debug_srcinfo  0 : { *(.debug_srcinfo) }
  .debug_sfnames  0 : { *(.debug_sfnames) }
  /* DWARF 1.1 and DWARF 2 */
  .debug_aranges  0 : { *(.debug_aranges) }
  .debug_pubnames 0 : { *(.debug_pubnames) }
  /* DWARF 2 */
  .debug_info     0 : { *(.debug_info .gnu.linkonce.wi.*) }
  .debug_abbrev   0 : { *(.debug_abbrev) }
  .debug_line     0 : { *(.debug_line) }
  .debug_frame    0 : { *(.debug_frame) }
  .debug_str      0 : { *(.debug_str) }
  .debug_loc      0 : { *(.debug_loc) }
  .debug_macinfo  0 : { *(.debug_macinfo) }
  /* SGI/MIPS DWARF 2 extensions */
  .debug_weaknames 0 : { *(.debug_weaknames) }
  .debug_funcnames 0 : { *(.debug_funcnames) }
  .debug_typenames 0 : { *(.debug_typenames) }
  .debug_varnames  0 : { *(.debug_varnames) }
  /* DWARF 3 */
  .debug_pubtypes 0 : { *(.debug_pubtypes) }
  .debug_ranges   0 : { *(.debug_ranges) }
  .gnu.attributes 0 : { KEEP (*(.gnu.attributes)) }
  .note.gnu.arm.ident 0 : { KEEP (*(.note.gnu.arm.ident)) }
  /DISCARD/ : { *(.ARM.exidx*) *(.note.GNU-stack) *(.gnu_debuglink) *(.gnu.lto_*) *(.init) *(.fini) }
}
