/****************************************************************************
 QUALCOMM Proprietary Design Data
 Copyright (c) 2014,2016 Qualcomm Technologies Incorporated. All rights reserved.
 ****************************************************************************/
/*==============================================================================
                                EDIT HISTORY

$Header: //components/rel/boot.xf/1.0/QcomPkg/Msm8996Pkg/Library/DSFTargetLib/target/8996/settings/target_config.c#10 $
$DateTime: 2016/08/17 04:21:06 $
$Author: pwbldsvc $
================================================================================
when       who          what, where, why
--------   ---          --------------------------------------------------------
05/11/14   arindamm     Initial creation for 8994 V1-only code.
================================================================================*/

#include "ddrss.h"
#include "target_config.h"

// Declare private function
void DDRSS_Modify_eCDT_for_Increased_DRAM_Load(EXTENDED_CDT_STRUCT *ecdt);
void DDRSS_Modify_eCDT_for_LPDDR4X_Receivers(EXTENDED_CDT_STRUCT *ecdt);
void DDRSS_Modify_eCDT_for_Micron_Load(EXTENDED_CDT_STRUCT *ecdt);


// =============================================================================
// Bit-byte remapping per the bump map. 
// =============================================================================
uint8 byte_remapping_table [NUM_CH][NUM_DQ_PCH] = {{0, 1, 2, 3}, {0, 1, 2, 3}};
uint8 bit_remapping_phy2bimc_DQ_Even[8]         = {5, 6, 7, 4, 2, 1, 0, 3};
uint8 bit_remapping_phy2bimc_DQ_Odd[8]          = {2, 1, 0, 3, 5, 6, 7, 4};
uint8 bit_remapping_bimc2phy[8]                 = {2, 1, 0, 3, 9, 5, 6, 7};

uint8 connected_bit_mapping_no_DBI_A [PINS_PER_PHY_CONNECTED_NO_DBI]     = {0, 1, 2, 3, 5, 6, 7, 9   };
uint8 connected_bit_mapping_with_DBI_A [PINS_PER_PHY_CONNECTED_WITH_DBI] = {0, 1, 2, 3, 5, 6, 7, 8, 9};
uint8 connected_bit_mapping_CA [PINS_PER_PHY_CONNECTED_CA]               = {0, 1, 2, 3, 6, 7};
uint8 connected_bit_mapping_CA_PHY [PINS_PER_PHY_CONNECTED_CA]           = {0, 1, 2, 3, 5, 9};

uint8 dq_dbi_bit    = 8;
uint8 dq_spare_bit  = 4;

uint8 dll_analog_freq_range[]= {
   15 ,  /*  <300MHz  */
   7 ,  /*  <400MHz  */   
   3 ,  /*  <500MHz  */
   1 ,  /*  <700MHz  */
   0    /*  < 1.89GHz  */
};

uint32 dll_analog_freq_range_table[]    = {300000, 400000, 500000, 700000, 1890000};
uint8  dll_analog_freq_range_table_size = sizeof(dll_analog_freq_range_table)/sizeof(uint8);

// extern struct ecdt_dram_latency_runtime_struct                  RL_WL_lpddr_struct[];
// extern struct ecdt_bimc_frequency_switch_params_runtime_struct  bimc_frequency_switch_params_struct[];

void DDRSS_Copy_Dram_Latency_Struct(struct ecdt_dram_latency_runtime_struct *source_ptr, struct ecdt_dram_latency_runtime_struct *dest_ptr)
{
    dest_ptr->RL_DBI_Off        = source_ptr->RL_DBI_Off;
    dest_ptr->RL_DBI_On         = source_ptr->RL_DBI_On;
    dest_ptr->WL                = source_ptr->WL;
    dest_ptr->MR2               = source_ptr->MR2;
    dest_ptr->rl_wl_freq_in_kHz = source_ptr->rl_wl_freq_in_kHz;
}

void DDRSS_Copy_Frequency_Switch_Params_Struct(struct ecdt_bimc_freq_switch_params_runtime_struct *source_ptr, struct ecdt_bimc_freq_switch_params_runtime_struct *dest_ptr)
{
    dest_ptr->rd_dbi                   = source_ptr->rd_dbi;                   
    dest_ptr->odt                      = source_ptr->odt;                      
    dest_ptr->fsp                      = source_ptr->fsp;                      
    dest_ptr->MR1                      = source_ptr->MR1;                      
    dest_ptr->MR3                      = source_ptr->MR3;                      
    dest_ptr->MR11                     = source_ptr->MR11;                     
    dest_ptr->freq_switch_range_in_kHz = source_ptr->freq_switch_range_in_kHz; 
}

void DDRSS_Create_Ecdt_Runtime_Structs(DDR_STRUCT *ddr, EXTENDED_CDT_STRUCT *ecdt)
{
    uint8 ecdt_index;
    uint8 runtime_index; 

    if(ecdt != NULL)
    {    
        // Create DRAM latency run-time structures.
        ddr->extended_cdt_runtime.hw_self_refresh_enable   = ecdt->hw_self_refresh_enable;
        ddr->extended_cdt_runtime.MR4_polling_enable       = ecdt->MR4_polling_enable;      
        ddr->extended_cdt_runtime.periodic_training_enable = ecdt->periodic_training_enable;
        ddr->extended_cdt_runtime.page_close_timer         = ecdt->page_close_timer;   
    }
    else
    {
        ddr->extended_cdt_runtime.hw_self_refresh_enable   = 1;
        ddr->extended_cdt_runtime.MR4_polling_enable       = 1;      
        ddr->extended_cdt_runtime.periodic_training_enable = 0;
        ddr->extended_cdt_runtime.page_close_timer         = 256;       
    }
    
    // First copy all elements of default DRAM latency structures into DDR_STRUCT.extended_cdt_runtime.
    for(runtime_index = 0; runtime_index < NUM_ECDT_DRAM_LATENCY_STRUCTS; runtime_index++) 
    {
        DDRSS_Copy_Dram_Latency_Struct(&RL_WL_lpddr_struct[runtime_index], &ddr->extended_cdt_runtime.dram_latency[runtime_index]);
    }
    
    // Then run thru the ECDT DRAM Latency input structures, and for those which have the apply_override flag set, find which band its
    // frequency falls into from in the default tables (RL_WL_lpddr_struct[]). Then copy the ECDT structure into the same band in the 
    // DDR_STRUCT.extended_cdt_runtime.dram_latency. At the end of this process, the runtime DDR latency structures in DDR_STRUCT will
    // contain structures from the ECDT if they were apply_override'd, or from the default RL_WL_lpddr_struct[] if they were not. Subsequent
    // DSF operations, both boot- and run-time will refer to the runtime structures in DDR_STRUCT, which are visible to both boot code and 
    // run-time code. 
    if(ecdt != NULL)
    {
        for(ecdt_index = 0; ecdt_index < NUM_ECDT_DRAM_LATENCY_STRUCTS; ecdt_index++) 
        {
            if(ecdt->extended_cdt_ecdt.dram_latency[ecdt_index].apply_override == 1)
            {
                runtime_index = BIMC_RL_WL_Freq_Index(ddr, ecdt->extended_cdt_ecdt.dram_latency[ecdt_index].dram_latency.rl_wl_freq_in_kHz);
                if(runtime_index >= NUM_ECDT_DRAM_LATENCY_STRUCTS)
                {
                    ddr_printf(DDR_ERROR, "Error: ECDT DRAM latency structure: Frequency out of limits: %uMHz\n\n", 
                                            ecdt->extended_cdt_ecdt.dram_latency[ecdt_index].dram_latency.rl_wl_freq_in_kHz);
                }
                else
                {
                    DDRSS_Copy_Dram_Latency_Struct(&ecdt->extended_cdt_ecdt.dram_latency[ecdt_index].dram_latency, 
                                                &ddr->extended_cdt_runtime.dram_latency[runtime_index]);
                }
            }
        }
    }

    // Create frequency switch parameter run-time structures.
    // First copy all elements of default DRAM frequency switch parameter structures into DDR_STRUCT.extended_cdt_runtime.bimc_freq_switch[].
    for(runtime_index = 0; runtime_index < NUM_ECDT_FREQ_SWITCH_STRUCTS; runtime_index++) 
    {
        DDRSS_Copy_Frequency_Switch_Params_Struct(&bimc_freq_switch_params_struct[runtime_index], &ddr->extended_cdt_runtime.bimc_freq_switch[runtime_index]);
    }

    // Then run thru the ECDT frequency switch parameter input structures, and for those with the apply_override flag set, copy them to the appropriate
    // frequency band in the run-time structures in DDR_STRUCT. 
    if(ecdt != NULL)
    {
        for(ecdt_index = 0; ecdt_index < NUM_ECDT_FREQ_SWITCH_STRUCTS; ecdt_index++) 
        {
            if(ecdt->extended_cdt_ecdt.bimc_freq_switch[ecdt_index].apply_override == 1)
            {
                runtime_index = BIMC_Freq_Switch_Params_Index(ddr, ecdt->extended_cdt_ecdt.bimc_freq_switch[ecdt_index].bimc_freq_switch_params.freq_switch_range_in_kHz);   
                if(runtime_index >= NUM_ECDT_FREQ_SWITCH_STRUCTS)
                {
                    ddr_printf(DDR_ERROR, "Error: ECDT Frequency Switch structure: Frequency out of limits: %uMHz\n\n", 
                                            ecdt->extended_cdt_ecdt.bimc_freq_switch[ecdt_index].bimc_freq_switch_params.freq_switch_range_in_kHz);
                }
                else
                {
                    DDRSS_Copy_Frequency_Switch_Params_Struct(&ecdt->extended_cdt_ecdt.bimc_freq_switch[ecdt_index].bimc_freq_switch_params, 
                                                        &ddr->extended_cdt_runtime.bimc_freq_switch[runtime_index]);
                }
            }
        }
    }
    
}

//================================================================================================//
// DDR Pre-Init function. Used to apply target- and revision-specific workaournds if any.
//================================================================================================//
uint32 DDRSS_Pre_Init(DDR_STRUCT *ddr, EXTENDED_CDT_STRUCT *ecdt)
{
    DDRSS_Create_Ecdt_Runtime_Structs(ddr, ecdt);
    return TRUE;
}

/* =============================================================================
**  Function : DDR_CC_Pre_Clock_Switch
** =============================================================================
*/
/**
*   @brief
*   DDRCC PLL programming in the pre-clock-switch routine. Platform-specific code,
*   as DDRCC PLL programming characteristics and CSRs are different across targets.
*   Targets 8992 and 8994 are the same, 8996 is different.
*
*   @param[in]  ddr          	 Pointer to ddr conifiguration struct
*   @param[in]  new_clk_index    Index into the clock plan structure corresponding to the requested frequency.
*   @param[in]  ddrcc_target_pll Parameter that selects which PLL to program.
*
*   @retval  None.
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/

//================================================================================================//
// Based on Freq_Switch_Params band, get an index for selecting in Freq_Switch_Params table
//================================================================================================//
uint8 DDRCC_dll_analog_freq_range_table_Index (DDR_STRUCT *ddr, uint32 clk_freq_khz)
{
   uint8 clk_idx = 0;

   for (clk_idx = 0; (clk_idx < dll_analog_freq_range_table_size); clk_idx++)
   {
      if (clk_freq_khz <= dll_analog_freq_range_table[clk_idx])
         break;
   }

   return clk_idx;
}

void DDR_CC_Pre_Clock_Switch(DDR_STRUCT *ddr, uint8 ch, uint8 new_clk_idx, uint8 ddrcc_target_pll)
{
    uint8  dll_params_idx;
	uint32 new_clk_khz;
	
	new_clk_khz = ddr->misc.clock_plan[new_clk_idx].clk_freq_in_khz;
	dll_params_idx  = DDRCC_dll_analog_freq_range_table_Index (ddr, new_clk_khz);
	
    if (ddrcc_target_pll) {
        //Step 1
        //Step 3
        HWIO_OUTX (REG_OFFSET_DDR_PHY_CH(ch) + DDR_CC_OFFSET, DDR_CC_DDRCC_SDPLL1_SYSCLK_EN_RESET,0x1);
        HWIO_OUTX (REG_OFFSET_DDR_PHY_CH(ch) + DDR_CC_OFFSET, DDR_CC_DDRCC_SDPLL1_SYSCLK_EN_RESET,0x0);
        HWIO_OUTX (REG_OFFSET_DDR_PHY_CH(ch) + DDR_CC_OFFSET, DDR_CC_DDRCC_SDPLL1_DEC_START,
                  ddr->misc.clock_plan[new_clk_idx].pll_dec_start);
        HWIO_OUTX (REG_OFFSET_DDR_PHY_CH(ch) + DDR_CC_OFFSET, DDR_CC_DDRCC_SDPLL1_DIV_FRAC_START3,
                   ddr->misc.clock_plan[new_clk_idx].pll_div_frac_start3);
        HWIO_OUTX (REG_OFFSET_DDR_PHY_CH(ch) + DDR_CC_OFFSET, DDR_CC_DDRCC_SDPLL1_PLLLOCK_CMP1,
                   ddr->misc.clock_plan[new_clk_idx].pll_plllock_cmp1);
        HWIO_OUTX (REG_OFFSET_DDR_PHY_CH(ch) + DDR_CC_OFFSET, DDR_CC_DDRCC_SDPLL1_PLLLOCK_CMP2,
                   ddr->misc.clock_plan[new_clk_idx].pll_plllock_cmp2);
        HWIO_OUTX (REG_OFFSET_DDR_PHY_CH(ch) + DDR_CC_OFFSET, DDR_CC_DDRCC_SDPLL1_VCO_COUNT1,
                   ddr->misc.clock_plan[new_clk_idx].pll_vco_count1);
        HWIO_OUTX (REG_OFFSET_DDR_PHY_CH(ch) + DDR_CC_OFFSET, DDR_CC_DDRCC_SDPLL1_VCO_COUNT2,
                   ddr->misc.clock_plan[new_clk_idx].pll_vco_count2);
        HWIO_OUTX (REG_OFFSET_DDR_PHY_CH(ch) + DDR_CC_OFFSET, DDR_CC_DDRCC_SDPLL1_PLL_LPF2_POSTDIV,
                   ddr->misc.clock_plan[new_clk_idx].pll_pll_lpf2_postdiv);
        HWIO_OUTX (REG_OFFSET_DDR_PHY_CH(ch) + DDR_CC_OFFSET, DDR_CC_DDRCC_SDPLL1_KVCO_CODE,
                   ddr->misc.clock_plan[new_clk_idx].pll_kvco_code);				   			   
        // HWIO_OUTXF (REG_OFFSET_DDR_PHY_CH(ch) + DDR_CC_OFFSET, DDR_CC_DDRCC_PHYDLL1_ANALOG_CFG0, FREQRANGE,
                   // dll_analog_freq_range[new_clk_idx]);				   
        HWIO_OUTXF (REG_OFFSET_DDR_PHY_CH(ch) + DDR_CC_OFFSET, DDR_CC_DDRCC_PHYDLL1_ANALOG_CFG0, FREQRANGE,
                    dll_analog_freq_range[dll_params_idx]);					   
				   //step 4
        //step 5
      // while(HWIO_INXF (REG_OFFSET_DDR_PHY_CH(ch) + DDR_CC_OFFSET, DDR_CC_DDRCC_SDPLL1_RESET_SM_READY_STATUS,CORE_READY) == 0);
        
    } else {
        //Step 1
        //step 3
        HWIO_OUTX (REG_OFFSET_DDR_PHY_CH(ch) + DDR_CC_OFFSET, DDR_CC_DDRCC_SDPLL0_SYSCLK_EN_RESET,0x1);
        HWIO_OUTX (REG_OFFSET_DDR_PHY_CH(ch) + DDR_CC_OFFSET, DDR_CC_DDRCC_SDPLL0_SYSCLK_EN_RESET,0x0);	
        HWIO_OUTX (REG_OFFSET_DDR_PHY_CH(ch) + DDR_CC_OFFSET, DDR_CC_DDRCC_SDPLL0_DEC_START,
                  ddr->misc.clock_plan[new_clk_idx].pll_dec_start);
        HWIO_OUTX (REG_OFFSET_DDR_PHY_CH(ch) + DDR_CC_OFFSET, DDR_CC_DDRCC_SDPLL0_DIV_FRAC_START3,
                   ddr->misc.clock_plan[new_clk_idx].pll_div_frac_start3);
        HWIO_OUTX (REG_OFFSET_DDR_PHY_CH(ch) + DDR_CC_OFFSET, DDR_CC_DDRCC_SDPLL0_PLLLOCK_CMP1,
                   ddr->misc.clock_plan[new_clk_idx].pll_plllock_cmp1);
        HWIO_OUTX (REG_OFFSET_DDR_PHY_CH(ch) + DDR_CC_OFFSET, DDR_CC_DDRCC_SDPLL0_PLLLOCK_CMP2,
                   ddr->misc.clock_plan[new_clk_idx].pll_plllock_cmp2);
        HWIO_OUTX (REG_OFFSET_DDR_PHY_CH(ch) + DDR_CC_OFFSET, DDR_CC_DDRCC_SDPLL0_VCO_COUNT1,
                   ddr->misc.clock_plan[new_clk_idx].pll_vco_count1);
        HWIO_OUTX (REG_OFFSET_DDR_PHY_CH(ch) + DDR_CC_OFFSET, DDR_CC_DDRCC_SDPLL0_VCO_COUNT2,
                   ddr->misc.clock_plan[new_clk_idx].pll_vco_count2);
        HWIO_OUTX (REG_OFFSET_DDR_PHY_CH(ch) + DDR_CC_OFFSET, DDR_CC_DDRCC_SDPLL0_PLL_LPF2_POSTDIV,
                   ddr->misc.clock_plan[new_clk_idx].pll_pll_lpf2_postdiv);
        HWIO_OUTX (REG_OFFSET_DDR_PHY_CH(ch) + DDR_CC_OFFSET, DDR_CC_DDRCC_SDPLL0_KVCO_CODE,
                   ddr->misc.clock_plan[new_clk_idx].pll_kvco_code);
        // HWIO_OUTXF (REG_OFFSET_DDR_PHY_CH(ch) + DDR_CC_OFFSET, DDR_CC_DDRCC_PHYDLL0_ANALOG_CFG0, FREQRANGE,
                   // dll_analog_freq_range[new_clk_idx]);
        HWIO_OUTXF (REG_OFFSET_DDR_PHY_CH(ch) + DDR_CC_OFFSET, DDR_CC_DDRCC_PHYDLL0_ANALOG_CFG0, FREQRANGE,
                   dll_analog_freq_range[dll_params_idx]);				   
        //step 4 
        //step 5
      // while(HWIO_INXF (REG_OFFSET_DDR_PHY_CH(ch) + DDR_CC_OFFSET, DDR_CC_DDRCC_SDPLL0_RESET_SM_READY_STATUS,CORE_READY) == 0);							  
    }
}


//================================================================================================//
// IO Calibration workaround for 8996 V1 and V2.
//================================================================================================//
void PHY_IO_Cal_Workaround_En_Dis(DDR_CHANNEL channel, uint8 enable)
{
    uint8 ch = 0;
    uint8 byte_lane = 0;
    uint8 ca_slave = 0;
    uint32 dq0_ddr_phy_base = 0;
    uint32 ca0_ddr_phy_base = 0;
    
    for (ch = 0; ch < NUM_CH; ch++)
    {
        if((channel >> ch) & 0x1) {
            dq0_ddr_phy_base = REG_OFFSET_DDR_PHY_CH(ch) + DQ0_DDR_PHY_OFFSET;
            ca0_ddr_phy_base = REG_OFFSET_DDR_PHY_CH(ch) + CA0_DDR_PHY_OFFSET;
            
            ca_slave = (ch == 0) ? 1 : 0; // Only program slave CA PHY
            
            if(1 == enable) {
                HWIO_OUTX ((ca0_ddr_phy_base + (ca_slave * DDR_PHY_OFFSET)), DDR_PHY_DDRPHY_IOCTLR_BYP_CFG, 0x00100000);                   // Override IO cal. PNCNT=0
                HWIO_OUTXF ((ca0_ddr_phy_base + (ca_slave * DDR_PHY_OFFSET)), DDR_PHY_DDRPHY_PAD_CNTL_3_CFG, LEFT_EXT_VREFB_EN, 1);        // Connect IO cal. Vref to floating external Vref pad
                HWIO_OUTXF ((ca0_ddr_phy_base + (ca_slave * DDR_PHY_OFFSET)), DDR_PHY_DDRPHY_PAD_CNTL_4_CFG, RIGHT_EXT_VREFB_EN, 1);       // Connect IO cal. Vref to floating external Vref pad
                HWIO_OUTXF ((ca0_ddr_phy_base + (ca_slave * DDR_PHY_OFFSET)), DDR_PHY_DDRPHY_PAD_CAL_CFG, CAL_PAD_RFU, 4);                 // Enable IO cal. comparator
                
                for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++)
                {
                    HWIO_OUTX ((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), DDR_PHY_DDRPHY_IOCTLR_BYP_CFG, 0x00110000);               // Override IO cal. PNCNT=0
                }
            }
            else {
                HWIO_OUTX ((ca0_ddr_phy_base + (ca_slave * DDR_PHY_OFFSET)), DDR_PHY_DDRPHY_IOCTLR_BYP_CFG, 0x00000000);
                HWIO_OUTXF ((ca0_ddr_phy_base + (ca_slave * DDR_PHY_OFFSET)), DDR_PHY_DDRPHY_PAD_CNTL_3_CFG, LEFT_EXT_VREFB_EN, 0);
                HWIO_OUTXF ((ca0_ddr_phy_base + (ca_slave * DDR_PHY_OFFSET)), DDR_PHY_DDRPHY_PAD_CNTL_4_CFG, RIGHT_EXT_VREFB_EN, 0);
                HWIO_OUTXF ((ca0_ddr_phy_base + (ca_slave * DDR_PHY_OFFSET)), DDR_PHY_DDRPHY_PAD_CAL_CFG, CAL_PAD_RFU, 0);
                
                for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++)
                {
                    HWIO_OUTX ((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), DDR_PHY_DDRPHY_IOCTLR_BYP_CFG, 0x00000000);
                }
            }
        }
    }
}

//================================================================================================//
// Function for detecting specific DRAMs and applying settings through eCDT
//================================================================================================//
void DDRSS_DRAM_Specific_Settings(DDR_STRUCT *ddr, EXTENDED_CDT_STRUCT *ecdt, DDR_CHANNEL channel, uint32 clk_freq_khz)
{
    uint8 ch = 0;
    uint32 mr6_value = 0, mr7_value = 0, mr8_value = 0;
    
    // Read revision ID mode registers
    mr6_value = BIMC_MR_Read(DDR_CH0, DDR_CS0, JEDEC_MR_6);
    mr7_value = BIMC_MR_Read(DDR_CH0, DDR_CS0, JEDEC_MR_7);
    
    // Read density
    mr8_value = BIMC_MR_Read(DDR_CH0, DDR_CS0, JEDEC_MR_8);
    
    // Samsung
    if(ddr->cdt_params[0].common.manufacture_name == SAMSUNG)
    {
        // 12Gb density dies (3GB single rank and 6GB dual rank x32 DRAMs)
        // 16Gb density dies (4GB single rank and 8GB dual rank x32 DRAMs)
        if((mr8_value & 0x3C) == 0x0C || (mr8_value & 0x3C) == 0x10)
        {
            DDRSS_Modify_eCDT_for_Increased_DRAM_Load (ecdt);
        }
        // C revision dies
        else if((mr6_value & 0xFF) == 0x05 && (mr7_value & 0xFF) == 0x11)
        {
            DDRSS_Modify_eCDT_for_LPDDR4X_Receivers (ecdt);
        }
    }
    // Micron
    else if(ddr->cdt_params[0].common.manufacture_name == MICRON)
    {
        // 8Gb density dies (2GB single rank and 4GB dual rank x32 DRAMs)
        if((mr8_value & 0x3C) == 0x08)
        {
            DDRSS_Modify_eCDT_for_Micron_Load (ecdt);
        }
    }
    
    // Apply eCDT settings
    DDRSS_Create_Ecdt_Runtime_Structs(ddr, ecdt);
    
#if TARGET_SILICON // Don't configure Phy & clock controller on emulation --- they are not included in the HW build
    DDR_PHY_CC_eCDT_Override(ddr, ecdt, channel);
    for (ch = 0; ch < NUM_CH; ch++)
    {
        if ((channel >> ch) & 0x1)
        {
            DDRSS_ddr_phy_sw_freq_switch(ddr, clk_freq_khz, ch);
        }
    }
#endif
}

#define OHMS_80 0x2
#define OHMS_60 0x3
#define OHMS_48 0x4
#define OHMS_40 0x5
#define OHMS_34 0x6
#define OHMS_30 0x7
#define DIV_3 0x1
#define DIV_2P5 0x2

//================================================================================================//
// Workaround for increased DRAM load (ex: windmill die layout)
//================================================================================================//
void DDRSS_Modify_eCDT_for_Increased_DRAM_Load(EXTENDED_CDT_STRUCT *ecdt)
{
    // 1017MHz DRAM CA ODT changed to 40 ohms
    // 1017MHz DRAM DQ/DQS ODT changed to 48 ohms
    ecdt->extended_cdt_ecdt.bimc_freq_switch[0].apply_override = 1;
    ecdt->extended_cdt_ecdt.bimc_freq_switch[0].bimc_freq_switch_params.rd_dbi = 0;
    ecdt->extended_cdt_ecdt.bimc_freq_switch[0].bimc_freq_switch_params.odt = 1;
    ecdt->extended_cdt_ecdt.bimc_freq_switch[0].bimc_freq_switch_params.fsp = 1;
    ecdt->extended_cdt_ecdt.bimc_freq_switch[0].bimc_freq_switch_params.MR1 = 0x6;
    ecdt->extended_cdt_ecdt.bimc_freq_switch[0].bimc_freq_switch_params.MR3 = 0xB0;
    ecdt->extended_cdt_ecdt.bimc_freq_switch[0].bimc_freq_switch_params.MR11 = 0x65;
    ecdt->extended_cdt_ecdt.bimc_freq_switch[0].bimc_freq_switch_params.freq_switch_range_in_kHz = 1295000;
    
    // 1296/1353/1555MHz DRAM CA ODT changed to 40 ohms
    // 1296/1353/1555MHz DRAM DQ/DQS ODT changed to 48 ohms
    ecdt->extended_cdt_ecdt.bimc_freq_switch[1].apply_override = 1;
    ecdt->extended_cdt_ecdt.bimc_freq_switch[1].bimc_freq_switch_params.rd_dbi = 0;
    ecdt->extended_cdt_ecdt.bimc_freq_switch[1].bimc_freq_switch_params.odt = 1;
    ecdt->extended_cdt_ecdt.bimc_freq_switch[1].bimc_freq_switch_params.fsp = 1;
    ecdt->extended_cdt_ecdt.bimc_freq_switch[1].bimc_freq_switch_params.MR1 = 0xE;
    ecdt->extended_cdt_ecdt.bimc_freq_switch[1].bimc_freq_switch_params.MR3 = 0xB0;
    ecdt->extended_cdt_ecdt.bimc_freq_switch[1].bimc_freq_switch_params.MR11 = 0x65;
    ecdt->extended_cdt_ecdt.bimc_freq_switch[1].bimc_freq_switch_params.freq_switch_range_in_kHz = 1570000;
    
    // 1804MHz DRAM CA/CK ODT changed to 40 ohms
    // 1804MHz DRAM DQ/DQS ODT changed to 48 ohms
    ecdt->extended_cdt_ecdt.bimc_freq_switch[2].apply_override = 1;
    ecdt->extended_cdt_ecdt.bimc_freq_switch[2].bimc_freq_switch_params.rd_dbi = 0;
    ecdt->extended_cdt_ecdt.bimc_freq_switch[2].bimc_freq_switch_params.odt = 1;
    ecdt->extended_cdt_ecdt.bimc_freq_switch[2].bimc_freq_switch_params.fsp = 1;
    ecdt->extended_cdt_ecdt.bimc_freq_switch[2].bimc_freq_switch_params.MR1 = 0xE;
    ecdt->extended_cdt_ecdt.bimc_freq_switch[2].bimc_freq_switch_params.MR3 = 0xB2;
    ecdt->extended_cdt_ecdt.bimc_freq_switch[2].bimc_freq_switch_params.MR11 = 0x65;
    ecdt->extended_cdt_ecdt.bimc_freq_switch[2].bimc_freq_switch_params.freq_switch_range_in_kHz = 1890000;
    
    // PRFS0 CA/CK PU/PD drive strength changed to 60 ohms
    // PRFS0 DQ/DQS PU/PD drive strength changed to 60 ohms
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[0].frequency_in_kHz = F_RANGE_0 - 1;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[0].msm_drive_strength_ca.apply_override = 1;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[0].msm_drive_strength_ca.pull_up_drive_strength = OHMS_60;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[0].msm_drive_strength_ca.pull_down_drive_strength = OHMS_60;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[0].msm_drive_strength_ca.vOH = DIV_2P5;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[0].msm_drive_strength_dq.apply_override = 1;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[0].msm_drive_strength_dq.pull_up_drive_strength = OHMS_60;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[0].msm_drive_strength_dq.pull_down_drive_strength = OHMS_60;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[0].msm_drive_strength_dq.vOH = DIV_3;
    
    // PRFS1 CA/CK PU/PD drive strength changed to 60 ohms
    // PRFS1 DQ/DQS PU/PD drive strength changed to 60 ohms
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[1].frequency_in_kHz = F_RANGE_1 - 1;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[1].msm_drive_strength_ca.apply_override = 1;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[1].msm_drive_strength_ca.pull_up_drive_strength = OHMS_60;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[1].msm_drive_strength_ca.pull_down_drive_strength = OHMS_60;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[1].msm_drive_strength_ca.vOH = DIV_2P5;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[1].msm_drive_strength_dq.apply_override = 1;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[1].msm_drive_strength_dq.pull_up_drive_strength = OHMS_60;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[1].msm_drive_strength_dq.pull_down_drive_strength = OHMS_60;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[1].msm_drive_strength_dq.vOH = DIV_3;
    
    // PRFS2 CA/CK PU/PD drive strength changed to 60 ohms
    // PRFS2 DQ/DQS PU/PD drive strength changed to 60 ohms
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[2].frequency_in_kHz = F_RANGE_2 - 1;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[2].msm_drive_strength_ca.apply_override = 1;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[2].msm_drive_strength_ca.pull_up_drive_strength = OHMS_60;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[2].msm_drive_strength_ca.pull_down_drive_strength = OHMS_60;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[2].msm_drive_strength_ca.vOH = DIV_2P5;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[2].msm_drive_strength_dq.apply_override = 1;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[2].msm_drive_strength_dq.pull_up_drive_strength = OHMS_60;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[2].msm_drive_strength_dq.pull_down_drive_strength = OHMS_60;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[2].msm_drive_strength_dq.vOH = DIV_3;
    
    // PRFS3 CA/CK PU/PD drive strength changed to 60 ohms
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[3].frequency_in_kHz = F_RANGE_3 - 1;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[3].msm_drive_strength_ca.apply_override = 1;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[3].msm_drive_strength_ca.pull_up_drive_strength = OHMS_60;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[3].msm_drive_strength_ca.pull_down_drive_strength = OHMS_60;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[3].msm_drive_strength_ca.vOH = DIV_2P5;
    
    // PRFS4 CA/CK PU drive strength changed to 40 ohms and PD drive strength changed to 30 ohms
	// PRFS4 DQ/DQS PU drive strength changed to 48 ohms and PD drive strength changed to 30 ohms
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[4].frequency_in_kHz = F_RANGE_4 - 1;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[4].msm_drive_strength_ca.apply_override = 1;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[4].msm_drive_strength_ca.pull_up_drive_strength = OHMS_40;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[4].msm_drive_strength_ca.pull_down_drive_strength = OHMS_30;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[4].msm_drive_strength_ca.vOH = DIV_2P5;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[4].msm_drive_strength_dq.apply_override = 1;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[4].msm_drive_strength_dq.pull_up_drive_strength = OHMS_48;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[4].msm_drive_strength_dq.pull_down_drive_strength = OHMS_30;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[4].msm_drive_strength_dq.vOH = DIV_3;
    
    // PRFS5 CA/CK PU drive strength changed to 40 ohms and PD drive strength changed to 30 ohms
	// PRFS5 DQ/DQS PU drive strength changed to 48 ohms and PD drive strength changed to 30 ohms
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[5].frequency_in_kHz = F_RANGE_5 - 1;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[5].msm_drive_strength_ca.apply_override = 1;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[5].msm_drive_strength_ca.pull_up_drive_strength = OHMS_40;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[5].msm_drive_strength_ca.pull_down_drive_strength = OHMS_30;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[5].msm_drive_strength_ca.vOH = DIV_2P5;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[5].msm_drive_strength_dq.apply_override = 1;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[5].msm_drive_strength_dq.pull_up_drive_strength = OHMS_48;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[5].msm_drive_strength_dq.pull_down_drive_strength = OHMS_30;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[5].msm_drive_strength_dq.vOH = DIV_3;
    
    // PRFS6 CA/CK PU drive strength changed to 40 ohms and PD drive strength changed to 30 ohms
	// PRFS6 DQ/DQS PU drive strength changed to 48 ohms and PD drive strength changed to 30 ohms
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[6].frequency_in_kHz = F_RANGE_6 - 1;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[6].msm_drive_strength_ca.apply_override = 1;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[6].msm_drive_strength_ca.pull_up_drive_strength = OHMS_40;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[6].msm_drive_strength_ca.pull_down_drive_strength = OHMS_30;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[6].msm_drive_strength_ca.vOH = DIV_2P5;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[6].msm_drive_strength_dq.apply_override = 1;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[6].msm_drive_strength_dq.pull_up_drive_strength = OHMS_48;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[6].msm_drive_strength_dq.pull_down_drive_strength = OHMS_30;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[6].msm_drive_strength_dq.vOH = DIV_3;
    
    // PRFS7 CA/CK PU drive strength changed to 40 ohms and PD drive strength changed to 30 ohms
	// PRFS7 DQ/DQS PU drive strength changed to 48 ohms and PD drive strength changed to 30 ohms
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[7].frequency_in_kHz = F_RANGE_7 - 1;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[7].msm_drive_strength_ca.apply_override = 1;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[7].msm_drive_strength_ca.pull_up_drive_strength = OHMS_40;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[7].msm_drive_strength_ca.pull_down_drive_strength = OHMS_30;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[7].msm_drive_strength_ca.vOH = DIV_2P5;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[7].msm_drive_strength_dq.apply_override = 1;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[7].msm_drive_strength_dq.pull_up_drive_strength = OHMS_48;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[7].msm_drive_strength_dq.pull_down_drive_strength = OHMS_30;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[7].msm_drive_strength_dq.vOH = DIV_3;
}

//================================================================================================//
// Workaround for DRAMs which support both LPDDR4 and LPDDR4X
//================================================================================================//
void DDRSS_Modify_eCDT_for_LPDDR4X_Receivers(EXTENDED_CDT_STRUCT *ecdt)
{
    // PRFS0 CA/CK PU/PD drive strength changed to 80 ohms
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[0].frequency_in_kHz = F_RANGE_0 - 1;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[0].msm_drive_strength_ca.apply_override = 1;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[0].msm_drive_strength_ca.pull_up_drive_strength = OHMS_80;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[0].msm_drive_strength_ca.pull_down_drive_strength = OHMS_80;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[0].msm_drive_strength_ca.vOH = DIV_2P5;
    
    // PRFS1 CA/CK PU/PD drive strength changed to 80 ohms
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[1].frequency_in_kHz = F_RANGE_1 - 1;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[1].msm_drive_strength_ca.apply_override = 1;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[1].msm_drive_strength_ca.pull_up_drive_strength = OHMS_80;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[1].msm_drive_strength_ca.pull_down_drive_strength = OHMS_80;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[1].msm_drive_strength_ca.vOH = DIV_2P5;
    
    // PRFS2 CA/CK PU/PD drive strength changed to 80 ohms
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[2].frequency_in_kHz = F_RANGE_2 - 1;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[2].msm_drive_strength_ca.apply_override = 1;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[2].msm_drive_strength_ca.pull_up_drive_strength = OHMS_80;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[2].msm_drive_strength_ca.pull_down_drive_strength = OHMS_80;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[2].msm_drive_strength_ca.vOH = DIV_2P5;
    
    // PRFS3 CA/CK PU/PD drive strength changed to 80 ohms
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[3].frequency_in_kHz = F_RANGE_3 - 1;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[3].msm_drive_strength_ca.apply_override = 1;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[3].msm_drive_strength_ca.pull_up_drive_strength = OHMS_80;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[3].msm_drive_strength_ca.pull_down_drive_strength = OHMS_60;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[3].msm_drive_strength_ca.vOH = DIV_2P5;
    
    // PRFS4 CA/CK PU drive strength changed to 60 ohms
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[4].frequency_in_kHz = F_RANGE_4 - 1;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[4].msm_drive_strength_ca.apply_override = 1;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[4].msm_drive_strength_ca.pull_up_drive_strength = OHMS_60;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[4].msm_drive_strength_ca.pull_down_drive_strength = OHMS_34;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[4].msm_drive_strength_ca.vOH = DIV_2P5;
    
    // PRFS5 CA/CK PU drive strength changed to 60 ohms
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[5].frequency_in_kHz = F_RANGE_5 - 1;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[5].msm_drive_strength_ca.apply_override = 1;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[5].msm_drive_strength_ca.pull_up_drive_strength = OHMS_60;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[5].msm_drive_strength_ca.pull_down_drive_strength = OHMS_34;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[5].msm_drive_strength_ca.vOH = DIV_2P5;
    
    // PRFS6 CA/CK PU drive strength changed to 60 ohms
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[6].frequency_in_kHz = F_RANGE_6 - 1;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[6].msm_drive_strength_ca.apply_override = 1;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[6].msm_drive_strength_ca.pull_up_drive_strength = OHMS_60;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[6].msm_drive_strength_ca.pull_down_drive_strength = OHMS_34;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[6].msm_drive_strength_ca.vOH = DIV_2P5;
    
    // PRFS7 CA/CK PU drive strength changed to 60 ohms
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[7].frequency_in_kHz = F_RANGE_7 - 1;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[7].msm_drive_strength_ca.apply_override = 1;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[7].msm_drive_strength_ca.pull_up_drive_strength = OHMS_60;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[7].msm_drive_strength_ca.pull_down_drive_strength = OHMS_34;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[7].msm_drive_strength_ca.vOH = DIV_2P5;
}

//================================================================================================//
// Workaround for Micron loading
//================================================================================================//
void DDRSS_Modify_eCDT_for_Micron_Load(EXTENDED_CDT_STRUCT *ecdt)
{
    // 1017MHz DRAM CA ODT changed to 60 ohms
    ecdt->extended_cdt_ecdt.bimc_freq_switch[0].apply_override = 1;
    ecdt->extended_cdt_ecdt.bimc_freq_switch[0].bimc_freq_switch_params.rd_dbi = 0;
    ecdt->extended_cdt_ecdt.bimc_freq_switch[0].bimc_freq_switch_params.odt = 1;
    ecdt->extended_cdt_ecdt.bimc_freq_switch[0].bimc_freq_switch_params.fsp = 1;
    ecdt->extended_cdt_ecdt.bimc_freq_switch[0].bimc_freq_switch_params.MR1 = 0x6;
    ecdt->extended_cdt_ecdt.bimc_freq_switch[0].bimc_freq_switch_params.MR3 = 0xB0;
    ecdt->extended_cdt_ecdt.bimc_freq_switch[0].bimc_freq_switch_params.MR11 = 0x43;
    ecdt->extended_cdt_ecdt.bimc_freq_switch[0].bimc_freq_switch_params.freq_switch_range_in_kHz = 1295000;
    
    // 1296/1353/1555MHz DRAM CA ODT changed to 60 ohms
    ecdt->extended_cdt_ecdt.bimc_freq_switch[1].apply_override = 1;
    ecdt->extended_cdt_ecdt.bimc_freq_switch[1].bimc_freq_switch_params.rd_dbi = 0;
    ecdt->extended_cdt_ecdt.bimc_freq_switch[1].bimc_freq_switch_params.odt = 1;
    ecdt->extended_cdt_ecdt.bimc_freq_switch[1].bimc_freq_switch_params.fsp = 1;
    ecdt->extended_cdt_ecdt.bimc_freq_switch[1].bimc_freq_switch_params.MR1 = 0xE;
    ecdt->extended_cdt_ecdt.bimc_freq_switch[1].bimc_freq_switch_params.MR3 = 0xB0;
    ecdt->extended_cdt_ecdt.bimc_freq_switch[1].bimc_freq_switch_params.MR11 = 0x43;
    ecdt->extended_cdt_ecdt.bimc_freq_switch[1].bimc_freq_switch_params.freq_switch_range_in_kHz = 1570000;
    
    // 1804MHz DRAM CA/CK ODT changed to 60 ohms
	// 1804MHz DRAM DQ ODT changed to 48 ohms
    ecdt->extended_cdt_ecdt.bimc_freq_switch[2].apply_override = 1;
    ecdt->extended_cdt_ecdt.bimc_freq_switch[2].bimc_freq_switch_params.rd_dbi = 0;
    ecdt->extended_cdt_ecdt.bimc_freq_switch[2].bimc_freq_switch_params.odt = 1;
    ecdt->extended_cdt_ecdt.bimc_freq_switch[2].bimc_freq_switch_params.fsp = 1;
    ecdt->extended_cdt_ecdt.bimc_freq_switch[2].bimc_freq_switch_params.MR1 = 0xE;
    ecdt->extended_cdt_ecdt.bimc_freq_switch[2].bimc_freq_switch_params.MR3 = 0xB2;
    ecdt->extended_cdt_ecdt.bimc_freq_switch[2].bimc_freq_switch_params.MR11 = 0x45;
    ecdt->extended_cdt_ecdt.bimc_freq_switch[2].bimc_freq_switch_params.freq_switch_range_in_kHz = 1890000;
    
    // PRFS4 CA/CK PU drive strength changed to 60 ohms 
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[4].frequency_in_kHz = F_RANGE_4 - 1;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[4].msm_drive_strength_ca.apply_override = 1;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[4].msm_drive_strength_ca.pull_up_drive_strength = OHMS_60;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[4].msm_drive_strength_ca.pull_down_drive_strength = OHMS_34;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[4].msm_drive_strength_ca.vOH = DIV_2P5;
    
    // PRFS5 CA/CK PU drive strength changed to 60 ohms
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[5].frequency_in_kHz = F_RANGE_5 - 1;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[5].msm_drive_strength_ca.apply_override = 1;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[5].msm_drive_strength_ca.pull_up_drive_strength = OHMS_60;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[5].msm_drive_strength_ca.pull_down_drive_strength = OHMS_34;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[5].msm_drive_strength_ca.vOH = DIV_2P5;

    // PRFS6 CA/CK PU drive strength changed to 60 ohms
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[6].frequency_in_kHz = F_RANGE_6 - 1;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[6].msm_drive_strength_ca.apply_override = 1;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[6].msm_drive_strength_ca.pull_up_drive_strength = OHMS_60;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[6].msm_drive_strength_ca.pull_down_drive_strength = OHMS_34;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[6].msm_drive_strength_ca.vOH = DIV_2P5;
    
    // PRFS7 CA/CK PU drive strength changed to 60 ohms
	// PRFS7 DQ/DQS PU drive strength changed to 48 ohms
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[7].frequency_in_kHz = F_RANGE_7 - 1;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[7].msm_drive_strength_ca.apply_override = 1;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[7].msm_drive_strength_ca.pull_up_drive_strength = OHMS_60;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[7].msm_drive_strength_ca.pull_down_drive_strength = OHMS_34;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[7].msm_drive_strength_ca.vOH = DIV_2P5;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[7].msm_drive_strength_dq.apply_override = 1;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[7].msm_drive_strength_dq.pull_up_drive_strength = OHMS_48;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[7].msm_drive_strength_dq.pull_down_drive_strength = OHMS_34;
    ecdt->extended_cdt_ecdt.msm_drv_str_odt_rdt2[7].msm_drive_strength_dq.vOH = DIV_3;
}

