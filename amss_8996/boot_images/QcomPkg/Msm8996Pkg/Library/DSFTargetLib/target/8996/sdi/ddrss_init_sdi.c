/****************************************************************************

 QUALCOMM Proprietary Design Data

 Copyright (c) 2014, Qualcomm Technologies Incorporated. All rights reserved.

 ****************************************************************************/

/*==============================================================================

                                EDIT HISTORY



$Header: //components/rel/boot.xf/1.0/QcomPkg/Msm8996Pkg/Library/DSFTargetLib/target/8996/sdi/ddrss_init_sdi.c#7 $

$DateTime: 2016/01/27 11:17:44 $

$Author: pwbldsvc $

================================================================================

when       who          what, where, why

--------   ---          --------------------------------------------------------

05/04/14   arindamm     First edit history header. Add new entries at top.

================================================================================*/



#include "ddrss_init_sdi.h"



//================================================================================================//

// DDR Initialization

//================================================================================================//

boolean HAL_DDR_Init_sdi (DDR_STRUCT *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select,

                      uint32 clk_freq_khz)

{

   DDR_CHANNEL ch_1hot = DDR_CH_NONE;

   uint32 reg_offset_shke;

   uint8 ch = 0;



   // Execute Pre-Init function for workarounds, if any.

   DDRSS_Pre_Init_sdi(ddr);



   // BIMC one-time settings

   BIMC_Config_sdi(ddr);



   #if TARGET_SILICON // Don't configure Phy & clock controller on emulation --- they are not included in the HW build

   // DDR PHY and CC one-time settings

   DDR_PHY_CC_Config_sdi(ddr);

   #endif // #if TARGET_SILICON



   BIMC_Pre_Init_Setup_sdi (ddr, channel, chip_select, clk_freq_khz);



   #if TARGET_SILICON // Don't configure Phy & clock controller on emulation --- they are not included in the HW build

   DDR_PHY_CC_init_sdi (ddr, channel, clk_freq_khz);  //workaround 19.2MHz SW switching hang issue.

   #endif // #if TARGET_SILICON



   // For LPDDR4, deassert reset pin before initialization

   if (ddr->cdt_params[0].common.device_type == DDR_TYPE_LPDDR4)

   {

      HWIO_OUTX (DDR_SS_BASE + SEQ_DDR_SS_DDR_REG_DDR_SS_REGS_OFFSET, DDR_SS_REGS_RESET_CMD, 1);

   }

   

   ddr_mpm_config_ebi1_freeze_io_sdi(FALSE);



   for (ch = 0; ch < 2; ch++)

   {

      ch_1hot = CH_1HOT(ch);

      reg_offset_shke = REG_OFFSET_SHKE(ch);



      if ((channel >> ch) & 0x1)

      {

         // If it is a watchdog reset we just need to do a warm-bootup . 

         // 'WDOG_SELF_RFSH' when set indicates that the memory controller is currently in self refresh due to a watchdog reset event

         // and requires a manual 'EXIT_SELF_REFRESH' trigger to exit from the forced self refresh state.

         if ( (HWIO_INXF (reg_offset_shke, SHKE_DRAM_STATUS, WDOG_SELF_RFSH) == 1) ) {

             // Exit from self-refresh on the rank

             BIMC_Exit_Self_Refresh_sdi (ddr, ch_1hot, (DDR_CHIPSELECT)ddr->cdt_params[ch].common.populated_chipselect);

         }    

         else 

         {    

             // If not a watchdog reset, we need to do a cold boot style init     

             BIMC_Memory_Device_Init_sdi (ddr, ch_1hot, (DDR_CHIPSELECT)ddr->cdt_params[ch].common.populated_chipselect, clk_freq_khz);   

         }

         

         // Set up the DDR device latency and enable flags

         BIMC_DDR_Access_Enable_sdi (ddr, ch_1hot, (DDR_CHIPSELECT)ddr->cdt_params[ch].common.populated_chipselect);

         

         // Set up the BIMC to operate with the known address range

         BIMC_Post_Init_Setup_Warm_sdi (ddr, ch_1hot, (DDR_CHIPSELECT)ddr->cdt_params[ch].common.populated_chipselect);

      }

   }

   

   // Enable 16 or 32 sequential (on-the-fly) burst length

//   BIMC_MR_Write_sdi (DDR_CH_BOTH, DDR_CS0, JEDEC_MR_1, 0x02);

	

   // Set SoC ODT value to RZQ/6

//   BIMC_MR_Write_sdi (DDR_CH_BOTH, DDR_CS0, JEDEC_MR_22, 0x06);

	

   BIMC_ZQ_Calibration_sdi (ddr, DDR_CH_BOTH, DDR_CS0);

	

   // RL and WL MR write

//   BIMC_MR_Write_sdi (DDR_CH_BOTH, DDR_CS0, JEDEC_MR_2, BIMC_RL_WL_Table_Sel_sdi(ddr, MR2_WR_VAL, 200000 /*clk_freq_khz*/));

	



   return TRUE;

}







/*==============================================================================

                                  DATA

==============================================================================*/

uint32 bimc_global0_config_sdi[][2] =

{

  {HWIO_ADDRXI((SEQ_BIMC_GLOBAL0_OFFSET), BIMC_DDR_CHN_CAPHY_CLK_EXTEND, 0), 0x00000004},

  {HWIO_ADDRXI((SEQ_BIMC_GLOBAL0_OFFSET), BIMC_DDR_CHN_CAPHY_CLK_EXTEND, 1), 0x00000004},

  {HWIO_ADDRXI((SEQ_BIMC_GLOBAL0_OFFSET), BIMC_DDR_CHN_DDRCC_CLK_EXTEND, 0), 0x00000004},

  {HWIO_ADDRXI((SEQ_BIMC_GLOBAL0_OFFSET), BIMC_DDR_CHN_DDRCC_CLK_EXTEND, 1), 0x00000004},

  {HWIO_ADDRXI((SEQ_BIMC_GLOBAL0_OFFSET), BIMC_DDR_CHN_DQPHY_CLK_EXTEND, 0), 0x00000004},

  {HWIO_ADDRXI((SEQ_BIMC_GLOBAL0_OFFSET), BIMC_DDR_CHN_DQPHY_CLK_EXTEND, 1), 0x00000004},

  {HWIO_ADDRXI((SEQ_BIMC_GLOBAL0_OFFSET), BIMC_DDR_CHN_CLK_PERIOD, 0), 0x10000000},

  {HWIO_ADDRXI((SEQ_BIMC_GLOBAL0_OFFSET), BIMC_DDR_CHN_CLK_PERIOD, 1), 0x10000000},  

  {0x0, 0x0}

};



uint32 bimc_scmo_config_sdi[][2] =

{

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_SCMO_OFFSET), SCMO_CMD_BUF_CFG), 0x00000001},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_SCMO_OFFSET), SCMO_CMD_OPT_CFG0), 0x07021110},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_SCMO_OFFSET), SCMO_CMD_OPT_CFG1), 0x010A0B1F},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_SCMO_OFFSET), SCMO_CMD_OPT_CFG2), 0x00000804},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_SCMO_OFFSET), SCMO_CMD_OPT_CFG3), 0x0004001F},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_SCMO_OFFSET), SCMO_CMD_OPT_CFG4), 0x00000011},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_SCMO_OFFSET), SCMO_FLUSH_CFG), 0x80320F08},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_SCMO_OFFSET), SCMO_GLOBAL_MON_CFG), 0x00000043},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_SCMO_OFFSET), SCMO_RCH_BKPR_CFG), 0x30307070},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_SCMO_OFFSET), SCMO_RCH_SELECT), 0x00000020},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_SCMO_OFFSET), SCMO_WCH_BUF_CFG), 0x00000001},

  {0x0, 0x0}

};



uint32 bimc_dpe_config_sdi[][2] =

{

{HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_CGC_CTRL), 0x0003FFFF},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_CONFIG_0), 0x23040010},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_CONFIG_1), 0x00000000},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_CONFIG_10), 0x00021022},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_CONFIG_11), 0x0A000030},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_CONFIG_12), 0x0000FFFA},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_CONFIG_13), 0x00000000},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_CONFIG_2), 0x82020000},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_CONFIG_3), 0x00000011},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_CONFIG_4), 0x00000000},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_CONFIG_5), 0x0FFFFFFF},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_CONFIG_6), 0x00000050},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_CONFIG_7), 0x00000000},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_CONFIG_8), 0x03070307},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_CONFIG_9), 0x00101010},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_DRAM_TIMING_0), 0x000031A4},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_DRAM_TIMING_1), 0x60B440B4},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_DRAM_TIMING_10), 0x27532753},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_DRAM_TIMING_11), 0x504B504B},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_DRAM_TIMING_12), 0x504B504B},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_DRAM_TIMING_13), 0x405E6000},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_DRAM_TIMING_14), 0x01001026},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_DRAM_TIMING_15), 0x000049CC},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_DRAM_TIMING_16), 0x0008200E},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_DRAM_TIMING_17), 0x00000000},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_DRAM_TIMING_18), 0x00000000},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_DRAM_TIMING_19), 0x00000000},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_DRAM_TIMING_2), 0x40644064},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_DRAM_TIMING_20), 0x00000000},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_DRAM_TIMING_21), 0x00000000},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_DRAM_TIMING_22), 0x00000000},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_DRAM_TIMING_23), 0x00000000},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_DRAM_TIMING_24), 0x00000020},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_DRAM_TIMING_25), 0x3300124B},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_DRAM_TIMING_26), 0x00001908},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_DRAM_TIMING_27), 0x00000008},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_DRAM_TIMING_28), 0x00000000},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_DRAM_TIMING_29), 0x00020008},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_DRAM_TIMING_3), 0x07080000},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_DRAM_TIMING_30), 0x0080012C},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_DRAM_TIMING_31), 0x000F0023},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_DRAM_TIMING_32), 0x00100000},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_DRAM_TIMING_33), 0x0010000B},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_DRAM_TIMING_34), 0x000009C4},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_DRAM_TIMING_35), 0x00000008},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_DRAM_TIMING_4), 0x0000804B},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_DRAM_TIMING_5), 0x40D240B4},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_DRAM_TIMING_6), 0x0190404B},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_DRAM_TIMING_7), 0x00000000},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_DRAM_TIMING_8), 0x00000000},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_DRAM_TIMING_9), 0x00003096},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_DRAM_TIMING_LP4_ODTLON_CNTL), 0x00000004},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_DRAM_TIMING_LP4_ODT_OFF_CNTL), 0x0023000F},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_DRAM_TIMING_LP4_ODT_ON_CNTL), 0x0023000F},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_ELEVATE_PRI_RD), 0x40404000},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_ELEVATE_PRI_THRESHOLD_SEL), 0x00000000},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_ELEVATE_PRI_WR), 0x80808000},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_MASKED_WRITE_CNTL), 0x000000F8},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_ODT_1PS_THRESHOLD_CTL), 0x0010044C},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_OE_PAD_DELAY_DEBUG_CTRL), 0x00000000},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_OPT_CTRL_0), 0x00000000},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_OPT_CTRL_1), 0x00001100},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_PAD_PASSIVATION_1PS_THRESHOLD_CTL), 0x0010044C}, //was 0x00100000, turn on passivation > 1GHz: 0x0010044C

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_PHY_RDDATA_EN_1PS_THRESHOLD_CTL), 0x001003E8},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_PWR_CTRL_0), 0x00010060},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_RANKID_EARLY_OFFSET_CNTL), 0x00260023},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_RANKID_OFFSET_CNTL), 0x14400008},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_RANK_ID_1PS_THRESHOLD_CTL), 0x00100320},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_RCW_CTRL), 0x00010100},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_RCW_CTRL_1), 0x00000000},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_RCW_RANK1_CTRL), 0x00000000},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_RCW_RANK1_TDQSCK_CTRL0), 0x00000000},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_RCW_RANK1_TDQSCK_CTRL1), 0x00000000},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_RCW_TDQSCK_CTRL0), 0x00000000},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_RCW_TDQSCK_CTRL1), 0x00000000},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_RD_POSTAMBLE_1PS_THRESHOLD_CTL), 0x00100000},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_RD_PREAMBLE_1PS_THRESHOLD_CTL), 0x00100320},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_TIMER_0), 0x03633063},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_TIMER_1), 0x00007104},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_TIMER_2), 0x00100078},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_TIMER_3), 0x00010460},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_TIMER_5), 0x03300303},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_WR_POSTAMBLE_1PS_THRESHOLD_CTL), 0x00100271},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET), DPE_WR_PREAMBLE_1PS_THRESHOLD_CTL), 0x00100000},



  {0x0, 0x0}

};



uint32 bimc_shke_config_sdi[][2] =

{

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_SHKE_OFFSET), SHKE_AUTO_REFRESH_CNTL), 0x00000049},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_SHKE_OFFSET), SHKE_AUTO_REFRESH_CNTL_1), 0x00000049},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_SHKE_OFFSET), SHKE_AUTO_REFRESH_CNTL_2), 0x00080000},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_SHKE_OFFSET), SHKE_CONFIG), 0x00148001},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_SHKE_OFFSET), SHKE_PERIODIC_ZQCAL), 0x00000800},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_SHKE_OFFSET), SHKE_SAFE_CTRL), 0x00000001},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_SHKE_OFFSET), SHKE_SELF_REFRESH_CNTL), 0x00032000},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_SHKE_OFFSET), SHKE_PERIODIC_MRR), 0x00404000},

  {0x0, 0x0}

};



uint32 bimc_shke_config_ch_diff_sdi[][2] =

{

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR0_SHKE_OFFSET), SHKE_PERIODIC_ZQCAL_1), 0x00030034},

  {HWIO_ADDRX((SEQ_BIMC_BIMC_S_DDR1_SHKE_OFFSET), SHKE_PERIODIC_ZQCAL_1), 0x00020034},

  {0x0, 0x0}

};



uint32 ddr_phy_dq_config_sdi[][2] =

{

//   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_TOP_CTRL_0_CFG), 0x0001E090},  //was: 0x0001C090},

//   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_PAD_CNTL_0_CFG), 0x00132001}, // Enable LP CMOS RX

   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_WRLVLEXT_CTL_0_CFG), 0x03420342},

   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMCDCWRLVL_TOP_CFG), 0x00200004},

//   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMCDCWR_CTL_CFG), 0x1EF4E138},

   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMCDCWR_TOP_CFG), 0x04200004},

//   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMCDCRD_CTL_CFG), 0x1EF4E138},

   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMCDCRD_TOP_CFG), 0x00200004},

   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMCDCRDT2_I0_CTL_CFG), 0x31866198},

//   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMCDCRCW_CTL_CFG), 0x31866198},

   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMCDCRCW_TOP_CFG), 0x02A00004},

//   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMHUB_TOP_1_CFG), 0x02020202},         

   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_LO_CFG), 0x8002B6DA},

   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_LUT0_CFG), 0xD57140C4},

   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_LUT1_CFG), 0xEA143877},   

   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_HI_LUT0_CFG), 0x01020307},

   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_HI_LUT1_CFG), 0x00010101},   

//   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMRX_USER_CFG), 0x09100191},   

   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_WR_0_CTL_CFG), 0x2947E1F8}, //was 0x29466198, align with phy_config.c 

   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_RD_0_CTL_CFG), 0x00001806},  // was 0x3FF66198, align with phy_config.c 

   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_RDT2_0_CTL_CFG), 0x31866198},

   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMCDCRDT2_I0_TOP_CFG), 0x00200004},   

//   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_UPDATE_INTF_CFG), 0x00000000},

//   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_FEA_WT_DEP_15_CFG), 0x0000374F},   

   {0x0, 0x0} 

};



uint32 ddr_phy_ca_config_sdi[][2] =

{

//   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_TOP_CTRL_0_CFG), 0x0001E090},  //was: 0x0001C090},

//   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_PAD_CNTL_0_CFG), 0x00100001},

   // Active CDC Registers

   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMCDCWRLVL_TOP_CFG), 0x00200004},

//   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMCDCWR_CTL_CFG), 0x1EF4E138},

   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMCDCWR_TOP_CFG), 0x00200004},

//   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMCDCRD_CTL_CFG), 0x1EF4E138},

   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMCDCRD_TOP_CFG), 0x00200004},

   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMCDCRDT2_I0_CTL_CFG), 0x31866198},

//   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMCDCRCW_CTL_CFG), 0x31866198},

   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMCDCRCW_TOP_CFG), 0x02A00004},

//   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMHUB_TOP_1_CFG), 0x02020202},

    {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_LO_CFG), 0x8002B6DA},

    {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_HI_CFG), 0x011EC09F},

//   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_LO_CFG), 0xF9E0B6D8},

//   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_HI_CFG), 0x00DEDA6D},   

   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_LUT0_CFG), 0xD57140C4},

   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_LUT1_CFG), 0xEA143877},

   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_HI_LUT0_CFG), 0x01020307},

   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_HI_LUT1_CFG), 0x00010101},   

//   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMRX_USER_CFG), 0x09100191},

   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_WR_0_CTL_CFG), 0x1EF4E138},

   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_RD_0_CTL_CFG), 0x1EF66198},

   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_RDT2_0_CTL_CFG), 0x31866198},

   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMCDCRDT2_I0_TOP_CFG), 0x00200004}, 

   {0x0, 0x0} 

};



uint32 ddr_cc_config_sdi[][2] =

{

   {HWIO_ADDRX(0, DDR_CC_DDRCC_TOP_CTRL_CFG), 0x00A01E00},

   {0x0, 0x0} 

};



uint32 ddr_phy_dq_delta_config_sdi[][2] =

{

   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_HI_CFG), 0x011EC09F}, 

   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_RD_0_CTL_CFG), 0x10801806},

   {0x0, 0x0} 

};

uint32 ddr_phy_ca_delta_config_sdi[][2] =

{

//   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_HI_CFG), 0x011EC09F},

   {0x0, 0x0} 

};

uint32 ddr_cc_delta_config_sdi[][2] =

{

   {0x0, 0x0} 

};

void Set_config_sdi (uint32 offset, uint32 config_base[][2] )

{

  uint32 reg = 0;

  uint8  ch = 0;

  uint32 ch_offset = offset;



  /* Populate base config */

  if (config_base != NULL)

  {

     if ((config_base != bimc_global0_config_sdi) && (config_base != bimc_shke_config_ch_diff_sdi))

     {

        for (ch=0; ch < 2; ch++)

        {

           ch_offset += BIMC_CH_OFFSET*ch;



           for (reg = 0; config_base[reg][0] != 0; reg++)

           {

             out_dword(config_base[reg][0] + ch_offset, config_base[reg][1]);

           }

        }

        ch_offset = offset;

     }

     else {

        for (reg = 0; config_base[reg][0] != 0; reg++)

        {

          out_dword(config_base[reg][0] + offset, config_base[reg][1]);

        }

     }

  }

}





//================================================================================================//

// BIMC One-Time Settings

//================================================================================================//

void BIMC_Config_sdi(DDR_STRUCT *ddr)

{

   Set_config_sdi(BIMC_BASE, bimc_global0_config_sdi);

   Set_config_sdi(BIMC_BASE, bimc_scmo_config_sdi);

   Set_config_sdi(BIMC_BASE, bimc_dpe_config_sdi);

   Set_config_sdi(BIMC_BASE, bimc_shke_config_sdi);

   Set_config_sdi(BIMC_BASE, bimc_shke_config_ch_diff_sdi);   

}





//================================================================================================//

// DDR PHY and CC one-time settings

//================================================================================================//

void DDR_PHY_CC_Config_sdi(DDR_STRUCT *ddr)

{

   // Enable broadcast mode for all DQ PHYs on both channels

   HWIO_OUTX((DDR_SS_BASE + SEQ_DDR_SS_DDRSS_AHB2PHY_SWMAN_OFFSET),

             AHB2PHY_SWMAN_AHB2PHY_BROADCAST_EN_CFG_LOWER, 0x1E3C);

   Set_config_sdi(BROADCAST_BASE, ddr_phy_dq_config_sdi);

   if(((ddr->misc.platform_id == PLATFORM_ID_ISTARI) && (ddr->misc.chip_version >= 0x0200)) || (ddr->misc.platform_id == PLATFORM_ID_RADAGAST))

	  Set_config_sdi(BROADCAST_BASE, ddr_phy_dq_delta_config_sdi);



   // Enable broadcast mode for all CA PHYs on both channels

   HWIO_OUTX((DDR_SS_BASE + SEQ_DDR_SS_DDRSS_AHB2PHY_SWMAN_OFFSET),

             AHB2PHY_SWMAN_AHB2PHY_BROADCAST_EN_CFG_LOWER, 0x0183);

   Set_config_sdi(BROADCAST_BASE, ddr_phy_ca_config_sdi);

   if(((ddr->misc.platform_id == PLATFORM_ID_ISTARI) && (ddr->misc.chip_version >= 0x0200)) || (ddr->misc.platform_id == PLATFORM_ID_RADAGAST))

	  Set_config_sdi(BROADCAST_BASE, ddr_phy_ca_delta_config_sdi);



   // Enable broadcast mode for all CCs on both channels

   HWIO_OUTX((DDR_SS_BASE + SEQ_DDR_SS_DDRSS_AHB2PHY_SWMAN_OFFSET),

             AHB2PHY_SWMAN_AHB2PHY_BROADCAST_EN_CFG_LOWER, 0x2040);

   Set_config_sdi(BROADCAST_BASE, ddr_cc_config_sdi);

   if(((ddr->misc.platform_id == PLATFORM_ID_ISTARI) && (ddr->misc.chip_version >= 0x0200)) || (ddr->misc.platform_id == PLATFORM_ID_RADAGAST))

	  Set_config_sdi(BROADCAST_BASE, ddr_cc_delta_config_sdi);



   // Disable broadcast mode

   HWIO_OUTX((DDR_SS_BASE + SEQ_DDR_SS_DDRSS_AHB2PHY_SWMAN_OFFSET),

             AHB2PHY_SWMAN_AHB2PHY_BROADCAST_EN_CFG_LOWER, 0x0);

}











void busywait_sdi(uint32 us_delay)

{

    uint32 i, j;

    

    for (i = 0; i < us_delay; i++)

    {

        for (j = 0; j < 10; j++)

	{

	}

    }

}







void seq_wait_sdi(uint32 time_value, SEQ_TimeUnit time_unit)

{

    if(time_unit == SEC)

    {

       busywait_sdi(1000000*time_value);

    }

    else if(time_unit == MS)

    {

       busywait_sdi(1000*time_value);

    }

    else if(time_unit == US)

    {

       busywait_sdi(time_value);

    }

    else

    {

       /* time_unit == NS */

       busywait_sdi(time_value/1000);

    }

}









//================================================================================================//

// Device Initialization

// Select LPDDR3 or LPDDR4 initialization routines for enabling CK and CKE.

// Does ZQ calibration and RL/WL programming

//================================================================================================//

void BIMC_Memory_Device_Init_sdi (DDR_STRUCT *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select,

                              uint32 clk_freq_khz)

{

   uint8 ch = 0x0;

   uint32 reg_offset_shke = 0;  



   BIMC_Memory_Device_Init_Lpddr_sdi (ddr, channel, chip_select);

   BIMC_ZQ_Calibration_sdi (ddr, channel, chip_select);



   // RL and WL MR write

   BIMC_MR_Write_sdi (channel, chip_select, JEDEC_MR_2, BIMC_RL_WL_Table_Sel_sdi(ddr, MR2_WR_VAL, clk_freq_khz));



   for (ch = 0; ch < 2; ch++)

   {

      reg_offset_shke = REG_OFFSET_SHKE(ch);



      if ((channel >> ch) & 0x1)

      {

         // Set rank init complete signal

         if (chip_select & DDR_CS0) {

            HWIO_OUTXF (reg_offset_shke, SHKE_CONFIG, RANK0_INITCOMPLETE, 1);

         }

         if (chip_select & DDR_CS1) {

            HWIO_OUTXF (reg_offset_shke, SHKE_CONFIG, RANK1_INITCOMPLETE, 1);

         }

      }

   }

}





//================================================================================================//

// LPDDR4 Device Initialization

// Does device initialization steps of enabling CK and CKE sequentially

//================================================================================================//

void BIMC_Memory_Device_Init_Lpddr_sdi (DDR_STRUCT *ddr, DDR_CHANNEL channel , DDR_CHIPSELECT chip_select)

{

   uint8 ch      = 0;

   uint8 ch_1hot = 0;

   uint32 reg_offset_shke = 0;



   for (ch = 0; ch < 2; ch++)

   {

      ch_1hot = CH_1HOT(ch);

      reg_offset_shke = REG_OFFSET_SHKE(ch);



      if ((channel >> ch) & 0x1)

      {

         // After RESET_n is deasserted, wait at least tINIT3 before activating CKE.

         // BIMC keeps CKE off at power up. Set CKE off again to trigger DPE timer to satisfy tINIT3

         // Convert tINIT3=2ms to Timer clock cycle (32KHz): 2ms/ 0.03125ms = 64 = 0x40

         BIMC_Wait_Timer_Setup_sdi ((DDR_CHANNEL)ch_1hot, WAIT_TIMER_CLOCK, 0x40);



         HWIO_OUTXF2 (reg_offset_shke, SHKE_DRAM_MANUAL_0, RANK_SEL, CKE_OFF, chip_select, 1);

         while (HWIO_INXF (reg_offset_shke, SHKE_DRAM_MANUAL_0, CKE_OFF));



         //Turn CK on and wait tINIT4=5tck

         BIMC_Wait_Timer_Setup_sdi ((DDR_CHANNEL)ch_1hot, WAIT_XO_CLOCK, 0x05);



         HWIO_OUTXF2 (reg_offset_shke, SHKE_DRAM_MANUAL_0, RANK_SEL, CK_ON, chip_select, 1);

         while (HWIO_INXF (reg_offset_shke, SHKE_DRAM_MANUAL_0, CK_ON));



         // Turn CKE on and then wait tINIT5

         // Convert tINIT5=2us to XO clock cycle (0.052us): 2us/0.052us = 39 (after roundup) = 0x27

         BIMC_Wait_Timer_Setup_sdi ((DDR_CHANNEL)ch_1hot, WAIT_XO_CLOCK, 0x27);



         HWIO_OUTXF2 (reg_offset_shke, SHKE_DRAM_MANUAL_0, RANK_SEL, CKE_ON, chip_select, 1);

         while (HWIO_INXF (reg_offset_shke, SHKE_DRAM_MANUAL_0, CKE_ON));



         // Reset manual_1 timer

         BIMC_Wait_Timer_Setup_sdi ((DDR_CHANNEL)ch_1hot, WAIT_XO_CLOCK, 0x00);

      }

   }



#if TARGET_SILICON    

    // Enable DBI-WR, DBI-RD/PDDS/PU-CAL/WR_PST are set to default

    //FSP = Settings

    BIMC_MR_Write_sdi (channel, chip_select, JEDEC_MR_3, 0xB0); //10  < F <= 400MHz, set MR3[0],PU-CAL=VDDQ/2.5 for fsp0, it was 0xB1(VDDQ/3)

    BIMC_MR_Write_sdi (channel, chip_select, JEDEC_MR_22, 0x0); //set SOC_ODT to disable for fsp0. it was SOC_ODT=40ohm for FSP=0

    

    

    // Enable 16 or 32 sequential (on-the-fly) burst length

    BIMC_MR_Write_sdi (channel, chip_select, JEDEC_MR_1, 0x06);

    

#else

   // Use these values to initialize the DDR mode regs for emulation    



   // Comment out the below lines for bringup. DBI-WR will be enabled post

   // bringup. Uncomment it then. Also needs bimc_config.c update to enable DBI-WR

   //// Enable DBI-WR, DBI-RD/PDDS/PU-CAL/WR_PST are set to default

   BIMC_MR_Write_sdi (channel, chip_select, JEDEC_MR_3, 0xB1);

   BIMC_MR_Write_sdi (channel, chip_select, JEDEC_MR_22, 0x6); //SOC_ODT=40ohm for FSP=0



   // Enable 16 or 32 sequential (on-the-fly) burst length

   BIMC_MR_Write_sdi (channel, chip_select, JEDEC_MR_1, 0x02);



#endif // TARGET_SILICON   

    

    // Reset FSP-WR=0

    BIMC_MR_Write_sdi (channel, chip_select, JEDEC_MR_13, 0x00);

}





//================================================================================================//

// Read and Write Latency Tables

// RL, WL and corresponding MR2 values

// LPDDR4 RL value below is for DBI enabled case

//================================================================================================//

uint8 RL_WL_lpddr_sdi[][4]= {

    /*RL,   RL

   DBI_OFF DBI_ON  WL, MR2  */

   { 6 ,     6 ,   4 , 0x00 },  /*   10 < F <= 266MHz  */

   { 10,     12,   6 , 0x09 },  /*  266 < F <= 533MHz  */

   { 14,     16,   8 , 0x12 },  /*  533 < F <= 800MHz  */

   { 20,     22,   10, 0x1B },  /*  800 < F <= 1066MHz */

   { 24,     28,   12, 0x24 },  /* 1066 < F <= 1333MHz */

   { 28,     32,   14, 0x2D },  /* 1333 < F <= 1600MHz */

   { 32,     36,   16, 0x36 },  /* 1600 < F <= 1866MHz */

   { 36,     40,   18, 0x3F }   /* 1866 < F <= 2133MHz */

};



uint8  RL_WL_lpddr_size_sdi                         = sizeof(RL_WL_lpddr_sdi)/4;



uint32 RL_WL_freq_range_table_sdi[]                 = {266000, 533000, 800000, 1066000, 1333000, 1600000, 1866000, 2133000};

uint8  RL_WL_freq_range_table_size_sdi              = sizeof(RL_WL_freq_range_table_sdi)/sizeof(uint32);









//================================================================================================//

// Set up init clk period.

// Update AC timing parameters from CDT, recalculate and load DPE timing actual registers

//================================================================================================//

void BIMC_Pre_Init_Setup_sdi (DDR_STRUCT *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select,

                          uint32 clk_freq_khz)

{

   uint32 tREFI_in_XO     = 0;

   uint32 period          = 0;

   uint8  ch              = 0;

   uint32 reg_offset_dpe  = 0;

   uint32 reg_offset_shke = 0;

   uint8  (*rl_wl_freq_ptr)[4] = RL_WL_lpddr_sdi;

   uint8 new_RL_WL_idx = 0;



   period = CONVERT_CYC_TO_PS / clk_freq_khz; //unit in ps



   new_RL_WL_idx  = BIMC_RL_WL_Freq_Index_sdi (ddr, clk_freq_khz);



   for (ch = 0; ch < 2; ch++)

   {

      reg_offset_dpe  = REG_OFFSET_DPE(ch);

      reg_offset_shke = REG_OFFSET_SHKE(ch);



      if ((channel >> ch) & 0x1)

      {

         // Program the new period

         HWIO_OUTXFI (REG_OFFSET_GLOBAL0, BIMC_DDR_CHN_CLK_PERIOD, ch, PERIOD, period);



         // Common AC Timing parameters between LP3 and LP4

         HWIO_OUTXF  (reg_offset_dpe, DPE_DRAM_TIMING_0, TRASMIN, ddr->cdt_params[ch].lpddr.tRAS_Min);

         HWIO_OUTXF2 (reg_offset_dpe, DPE_DRAM_TIMING_1, TWR, TRCD,

                      ddr->cdt_params[ch].lpddr.tWR, ddr->cdt_params[ch].lpddr.tRCD);

         HWIO_OUTXF2 (reg_offset_dpe, DPE_DRAM_TIMING_2, TWTR, TRRD,

                      ddr->cdt_params[ch].lpddr.tWTR, ddr->cdt_params[ch].lpddr.tRRD);

         HWIO_OUTXF (reg_offset_dpe, DPE_DRAM_TIMING_3, TRFCAB, ddr->cdt_params[ch].lpddr.tRFC);

         HWIO_OUTXF  (reg_offset_dpe, DPE_DRAM_TIMING_4, TRTP, ddr->cdt_params[ch].lpddr.tRTP);

         HWIO_OUTXF2 (reg_offset_dpe, DPE_DRAM_TIMING_5, TRPAB, TRPPB,

                      ddr->cdt_params[ch].lpddr.tRP_AB, ddr->cdt_params[ch].lpddr.tRP_PB);

         HWIO_OUTXF2 (reg_offset_dpe, DPE_DRAM_TIMING_6, TFAW, TCKE,

                      ddr->cdt_params[ch].lpddr.tFAW, ddr->cdt_params[ch].lpddr.tCKE);

         HWIO_OUTXF  (reg_offset_dpe, DPE_DRAM_TIMING_9, MIN_SR_DURATION, ddr->cdt_params[ch].lpddr.tCKESR);

         HWIO_OUTXF2 (reg_offset_dpe, DPE_DRAM_TIMING_10, TXSRD, TXSNR,

                      ddr->cdt_params[ch].lpddr.tXSR, ddr->cdt_params[ch].lpddr.tXSR);

         HWIO_OUTXF2 (reg_offset_dpe, DPE_DRAM_TIMING_11, TXPNR_ACT_PWR_DN, TXPR_ACT_PWR_DN,

                      ddr->cdt_params[ch].lpddr.tXP, ddr->cdt_params[ch].lpddr.tXP);

         HWIO_OUTXF2 (reg_offset_dpe, DPE_DRAM_TIMING_12, TXPNR_PCHG_PWR_DN, TXPR_PCHG_PWR_DN,

                      (ddr->cdt_params[ch].lpddr.tXP + ddr->cdt_params[ch].lpddr.tRCD),

                      ddr->cdt_params[ch].lpddr.tXP);

         HWIO_OUTXF2 (reg_offset_dpe, DPE_DRAM_TIMING_16, RD_LATENCY, WR_LATENCY,

                      rl_wl_freq_ptr[new_RL_WL_idx][RL_DBI_OFF], rl_wl_freq_ptr[new_RL_WL_idx][WL]);

         HWIO_OUTXF  (reg_offset_dpe, DPE_TIMER_2, TMRR, ddr->cdt_params[ch].lpddr.tMRR);



         if (15 < ddr->cdt_params[ch].lpddr.tMRW) {

            HWIO_OUTXF (reg_offset_dpe, DPE_TIMER_2, TMRW, ddr->cdt_params[ch].lpddr.tMRW);

         }

         else

         {

            HWIO_OUTXF (reg_offset_dpe, DPE_TIMER_2, TMRW, 15);

         }



         // cdt_params has resolution of 100ps, multiplied by 100 to convert to 1ps

         tREFI_in_XO = (ddr->cdt_params[ch].lpddr.tREFI * 100) / XO_PERIOD_IN_PS - 1;



         HWIO_OUTXF (reg_offset_shke, SHKE_AUTO_REFRESH_CNTL,   TREFI, tREFI_in_XO);

         HWIO_OUTXF (reg_offset_shke, SHKE_AUTO_REFRESH_CNTL_1, TREFI, tREFI_in_XO);



         // AC Timing parameters

		 BIMC_Program_Lpddr_AC_Parameters_sdi( ddr, (DDR_CHANNEL)ch);



		 // Kick off timing parameter calculation and wait until done

         HWIO_OUTXF (reg_offset_dpe, DPE_CONFIG_4, RECALC_PS_PARAMS, 0x1);

         while (HWIO_INXF (reg_offset_dpe, DPE_MEMC_STATUS_1, CYC_CALC_VALID));



         // Load all the calculated settings into DPE actual registers

         HWIO_OUTXF (reg_offset_dpe, DPE_CONFIG_4, LOAD_ALL_CONFIG, 0x1);

      }

   }

}





//================================================================================================//

// BIMC DRAM Address Setup for warm boot -- based on DDR information stored in DDR_STRUCT during Pass 0 initialization

// channel=DDR_CH_BOTH is not supported in this function

// cs=DDR_CS_BOTH is not supported in this function

//================================================================================================//



void BIMC_DDR_Addr_Setup_Warm_sdi (DDR_STRUCT *ddr, uint8 ch, uint8 cs)

{

   uint32 addr_base_9_2   = 0;

   uint32 size_in_mb      = 0;

   uint32 addr_mask       = 0;

   uint32 num_rows        = 0;

   uint32 num_cols        = 0;

   uint32 num_banks       = 0;

   uint32 reg_offset_scmo = 0;



   uint64 base_addr    = 0xFFFFFFFFFFFFFFFF;

   



   // Instead of calculating it, just pull the needed DDR info from the DDR_STRUCT

   if (ch == 0)

   {

      if (cs == 0)

      {

         base_addr = ddr->ddr_size_info.ddr0_cs0_addr;

	 size_in_mb = ddr->ddr_size_info.ddr0_cs0_mb;

	 num_banks = ddr->cdt_params[ch].common.num_banks_cs0;

	 num_rows = ddr->cdt_params[ch].common.num_rows_cs0;

	 num_cols = ddr->cdt_params[ch].common.num_cols_cs0;

      }

      else

      {

	 base_addr = ddr->ddr_size_info.ddr0_cs1_addr;

	 size_in_mb = ddr->ddr_size_info.ddr0_cs0_mb;

	 num_banks = ddr->cdt_params[ch].common.num_banks_cs1;

	 num_rows = ddr->cdt_params[ch].common.num_rows_cs1;

	 num_cols = ddr->cdt_params[ch].common.num_cols_cs1;

      }

   }

   else

   {

      if (cs == 0)

      {

         base_addr = ddr->ddr_size_info.ddr1_cs0_addr;

	 size_in_mb = ddr->ddr_size_info.ddr1_cs0_mb;

	 num_banks = ddr->cdt_params[ch].common.num_banks_cs0;

	 num_rows = ddr->cdt_params[ch].common.num_rows_cs0;

	 num_cols = ddr->cdt_params[ch].common.num_cols_cs0;

      }

      else

      {

	 base_addr = ddr->ddr_size_info.ddr1_cs1_addr;

	 size_in_mb = ddr->ddr_size_info.ddr1_cs0_mb;

	 num_banks = ddr->cdt_params[ch].common.num_banks_cs1;

	 num_rows = ddr->cdt_params[ch].common.num_rows_cs1;

	 num_cols = ddr->cdt_params[ch].common.num_cols_cs1;

      }

   }



   switch(size_in_mb){

      case 8192 : addr_mask = 0x80;

         break;

      case 4096 : addr_mask = 0xc0;

         break;

      case 2048 : addr_mask = 0xe0;

         break;

      case 1536 : addr_mask = 0xe0;

         break;  

      case 1024 : addr_mask = 0xf0;

         break;

      case 768  : addr_mask = 0xf0;

         break;

      case 512  : addr_mask = 0xf8;

         break;

      case 256  : addr_mask = 0xfc;

         break;

      case 128  : addr_mask = 0xfe;

         break;

      case 64   : addr_mask = 0xff;

         break;

      default   : addr_mask = 0x00;

         break;

   }



   reg_offset_scmo = REG_OFFSET_SCMO(ch);

   HWIO_OUTXFI (reg_offset_scmo, SCMO_ADDR_MAP_CSN,  cs, BANK_SIZE, num_banks >> 3);// 0x0:BANKS_4

                                                                                                     // 0x1:BANKS_8

   HWIO_OUTXFI (reg_offset_scmo, SCMO_ADDR_MAP_CSN,  cs, ROW_SIZE,  num_rows - 13); // 0x0:ROWS_13

                                                                                                     // 0x1:ROWS_14

                                                                                                     // 0x2:ROWS_15

                                                                                                     // 0x3:ROWS_16

   HWIO_OUTXFI (reg_offset_scmo, SCMO_ADDR_MAP_CSN,  cs, COL_SIZE,  num_cols - 8);  // 0x0:COLS_8

                                                                                                     // 0x1:COLS_9

                                                                                                     // 0x2:COLS_10

                                                                                                     // 0x3:COLS_11



   // Convert base addr to [9:2] for SCMO base CSR. Divide base address by 64MB which is the

   // minimum supported density (right shift by 26)

   addr_base_9_2 = (base_addr >> 26);

   HWIO_OUTXFI (reg_offset_scmo, SCMO_ADDR_BASE_CSN, cs, ADDR_BASE, addr_base_9_2);

   HWIO_OUTXFI (reg_offset_scmo, SCMO_ADDR_MASK_CSN, cs, ADDR_MASK, addr_mask);



   if (num_banks != 0) {

      HWIO_OUTXFI (reg_offset_scmo, SCMO_ADDR_MAP_CSN, cs, RANK_EN, 1);

   }

}







//================================================================================================//

// BIMC Post Initialization sequence  //

// Channel=DDR_CH_BOTH is not supported in this function

//================================================================================================//

void BIMC_Post_Init_Setup_Warm_sdi (DDR_STRUCT *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select)

{

   uint8 interface_width_index_cs0 = 0;

   uint8 interface_width_index_cs1 = 0;

   uint8 ch_inx                    = 0;

   uint32 reg_offset_dpe           = 0;

   uint32 reg_offset_scmo          = 0;

   uint32 reg_offset_shke          = 0;



   ch_inx          = CH_INX(channel);

   reg_offset_dpe  = REG_OFFSET_DPE(ch_inx);

   reg_offset_scmo = REG_OFFSET_SCMO(ch_inx);

   reg_offset_shke = REG_OFFSET_SHKE(ch_inx);





   if (chip_select & DDR_CS0)

   {

      if (ddr->cdt_params[ch_inx].common.interleave_en & DDR_CS0_INTERLEAVE)

      {

         HWIO_OUTXF (reg_offset_scmo, SCMO_SLV_INTERLEAVE_CFG , INTERLEAVE_CS0, 1);

      }



      switch(ddr->cdt_params[ch_inx].common.interface_width_cs0)

      {

         case 64 : interface_width_index_cs0 = 0x3;

            break;

         case 32 : interface_width_index_cs0 = 0x2;

            break;

         case 16 : interface_width_index_cs0 = 0x1;

            break;

         default : interface_width_index_cs0 = 0x0;

            break;

      }



      HWIO_OUTXF (reg_offset_dpe, DPE_CONFIG_0, DEVICE_CFG_RANK0, interface_width_index_cs0);



      BIMC_DDR_Addr_Setup_Warm_sdi (ddr, ch_inx, CS_INX(DDR_CS0));



      // Bank number 4: 0x00; Bank number 8: 0x01

      HWIO_OUTXF (reg_offset_shke, SHKE_CONFIG, NUM_BANKS, (ddr->cdt_params[ch_inx].common.num_banks_cs0) >> 3 );



   }



   if (chip_select & DDR_CS1)

   {

      if (ddr->cdt_params[ch_inx].common.interleave_en & DDR_CS1_INTERLEAVE)

      {

         HWIO_OUTXF (reg_offset_scmo, SCMO_SLV_INTERLEAVE_CFG , INTERLEAVE_CS1, 1);

      }



      switch(ddr->cdt_params[ch_inx].common.interface_width_cs1)

      {

         case 64 : interface_width_index_cs1 = 0x3;

            break;

         case 32 : interface_width_index_cs1 = 0x2;

            break;

         case 16 : interface_width_index_cs1 = 0x1;

            break;

         default : interface_width_index_cs1 = 0x0;

            break;

      }



      HWIO_OUTXF (reg_offset_dpe, DPE_CONFIG_0, DEVICE_CFG_RANK1, interface_width_index_cs1);



      BIMC_DDR_Addr_Setup_Warm_sdi (ddr, ch_inx, CS_INX(DDR_CS1));



      //Bank number 4: 0x00; Bank number 8: 0x01

      HWIO_OUTXF (reg_offset_shke, SHKE_CONFIG, NUM_BANKS, (ddr->cdt_params[ch_inx].common.num_banks_cs1) >> 3 );

   }



   // bank count for both ranks

   HWIO_OUTXF2 (reg_offset_dpe, DPE_CONFIG_1, NUM_BANKS_RANK0, NUM_BANKS_RANK1,

                ddr->cdt_params[ch_inx].common.num_banks_cs0, ddr->cdt_params[ch_inx].common.num_banks_cs1);



   // Enable activity based channel clock gating after init. This clock gating was disable in one-time

   // settings so that PHY will recieve clock during PHY init.

   HWIO_OUTXF2 (reg_offset_dpe, DPE_CONFIG_6, IOSTAGE_WR_DEBUG_MODE, IOSTAGE_CA_DEBUG_MODE, 0x0, 0x0);



   // Enable Power Down

   HWIO_OUTXF2 (reg_offset_dpe, DPE_PWR_CTRL_0, PWR_DN_EN, CLK_STOP_PWR_DN_EN, 0x1, 0x1);

   HWIO_OUTXF  (reg_offset_dpe, DPE_CONFIG_4, LOAD_ALL_CONFIG, 0x1);



   // Enable all periodic functions: auto refresh, hw self refresh, periodic ZQCAL, periodic SRR,

   BIMC_All_Periodic_Ctrl_sdi (ddr, channel, chip_select, 0x01/*0x01 for enable*/);

}







//================================================================================================//

// BIMC_Exit_Self_Refresh_sdi

// exit SW self refresh and enable HW self refresh

//================================================================================================//

void BIMC_Exit_Self_Refresh_sdi (DDR_STRUCT *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select)

{

   uint8 ch = 0;

   uint32 reg_offset_shke = 0;

   uint32 reg_offset_scmo = 0;



   for (ch = 0; ch < 2; ch++)

   {

      reg_offset_shke = REG_OFFSET_SHKE(ch);

      reg_offset_scmo = REG_OFFSET_SCMO(ch);



      if ((channel >> ch) & 0x1)

      {

         // Enable rank

         if (chip_select & DDR_CS0) {

            HWIO_OUTXFI (reg_offset_scmo, SCMO_ADDR_MAP_CSN,  0/*rank0*/, RANK_EN, 1);

         }

         if (chip_select & DDR_CS1) {

            HWIO_OUTXFI (reg_offset_scmo, SCMO_ADDR_MAP_CSN,  1/*rank1*/, RANK_EN, 1);

         }





         HWIO_OUTXF2 (reg_offset_shke, SHKE_DRAM_MANUAL_0, RANK_SEL, EXIT_SELF_REFRESH, chip_select, 1);

         while (HWIO_INXF (reg_offset_shke, SHKE_DRAM_MANUAL_0, EXIT_SELF_REFRESH));



         while (HWIO_INXF (reg_offset_shke, SHKE_DRAM_STATUS, SW_SELF_RFSH));



         // Enable HW self refresh

         if (chip_select & DDR_CS0) {

            BIMC_HW_Self_Refresh_Ctrl_sdi (ddr, ch, 0, 1);

         }

         if (chip_select & DDR_CS1) {

            BIMC_HW_Self_Refresh_Ctrl_sdi (ddr, ch, 1, 1);

         }

      }

   }

}



void BIMC_Enter_Self_Refresh_sdi (DDR_STRUCT *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select)

{

   uint8 ch = 0;

   uint32 reg_offset_shke = 0;

   uint32 reg_offset_scmo = 0;



   for (ch = 0; ch < 2; ch++)

   {

      reg_offset_shke = REG_OFFSET_SHKE(ch);

      reg_offset_scmo = REG_OFFSET_SCMO(ch);



      if ((channel >> ch) & 0x1)

      {

         // Disable rank and disable HW self refresh

         if (chip_select & DDR_CS0)  {

            HWIO_OUTXFI (reg_offset_scmo, SCMO_ADDR_MAP_CSN,  0/*rank index 0*/, RANK_EN, 0);

            BIMC_HW_Self_Refresh_Ctrl_sdi (ddr, ch, 0, 0);

         }

         if (chip_select & DDR_CS1)  {

            HWIO_OUTXFI (reg_offset_scmo, SCMO_ADDR_MAP_CSN,  1/*rank index 1*/, RANK_EN, 0);

            BIMC_HW_Self_Refresh_Ctrl_sdi (ddr, ch, 1, 0);

         }



         // Enter SW self refresh

         HWIO_OUTXF2 (reg_offset_shke, SHKE_DRAM_MANUAL_0, RANK_SEL, ENTER_SELF_REFRESH_IDLE, chip_select, 1);

         while (HWIO_INXF (reg_offset_shke, SHKE_DRAM_MANUAL_0, ENTER_SELF_REFRESH_IDLE));



         while (HWIO_INXF (reg_offset_shke, SHKE_DRAM_STATUS, SW_SELF_RFSH) == 0);

      }

   }

}





//================================================================================================//

// Enable/Disable HW activity based self refresh

//================================================================================================//

void BIMC_HW_Self_Refresh_Ctrl_sdi (DDR_STRUCT *ddr, uint8 ch, uint8 cs, uint8 enable)

{



   // Check if this feature is enabled in the DDR_STRUCT

   if (ddr->extended_cdt_runtime.hw_self_refresh_enable == 1) {

      if (cs == 0) {

         HWIO_OUTXF (REG_OFFSET_SHKE(ch), SHKE_SELF_REFRESH_CNTL, HW_SELF_RFSH_ENABLE_RANK0, enable);

      }

      if (cs == 1) {

         HWIO_OUTXF (REG_OFFSET_SHKE(ch), SHKE_SELF_REFRESH_CNTL, HW_SELF_RFSH_ENABLE_RANK1, enable);

      }

   }

}











//================================================================================================//

//ZQ Cal function that can either choose lpddr3 or lpddr4

//================================================================================================//

void BIMC_ZQ_Calibration_sdi (DDR_STRUCT  *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select)

{

   uint8 ch      = 0;

   uint8 ch_1hot = 0;



   for (ch = 0; ch < 2; ch++)

   {

      ch_1hot = CH_1HOT(ch);



      if ((channel >> ch) & 0x1)

      {

         if (chip_select & DDR_CS0) {

             BIMC_ZQ_Calibration_Lpddr_sdi (ddr, (DDR_CHANNEL)ch_1hot, DDR_CS0);

         }

         if (chip_select & DDR_CS1) {

             BIMC_ZQ_Calibration_Lpddr_sdi (ddr, (DDR_CHANNEL)ch_1hot, DDR_CS1);

         }

      }

   }

}







//================================================================================================//

//Timer function is called before triggering DRAM_MANUAL_0 command

//WAIT_TIMER_DOMAIN has 2 options: XO clock(19.2MHz) and Timer clock(32KHz), enum defined in ddr_common.h

//================================================================================================//

void BIMC_Wait_Timer_Setup_sdi (DDR_CHANNEL channel, BIMC_Wait_Timer_Domain one_xo_zero_timer_clk, uint32 timer_value)

{

   uint8  ch = 0;

   uint32 reg_offset_shke = 0;



   for (ch = 0; ch < 2; ch++)

   {

      reg_offset_shke = REG_OFFSET_SHKE(ch);

      if ((channel >> ch) & 0x1)

      {

         HWIO_OUTXF2 (reg_offset_shke, SHKE_DRAM_MANUAL_1, WAIT_TIMER_DOMAIN, WAIT_TIMER_BEFORE_HW_CLEAR, one_xo_zero_timer_clk, timer_value);

      }

   }



}

//================================================================================================//

//Mode Register write can use for both channels and both ranks

//================================================================================================//

void BIMC_MR_Write_sdi (DDR_CHANNEL channel, DDR_CHIPSELECT chip_select, uint32 MR_addr, uint32 MR_data)

{

   uint8 ch = 0;

   uint32 reg_offset_shke = 0;



   for (ch = 0; ch < 2; ch++)

   {

      reg_offset_shke = REG_OFFSET_SHKE(ch);

      if ((channel >> ch) & 0x1)

      {

         HWIO_OUTXF2 (reg_offset_shke, SHKE_MREG_ADDR_WDATA_CNTL, MREG_ADDR, MREG_WDATA, MR_addr, MR_data);

         HWIO_OUTXF2 (reg_offset_shke, SHKE_DRAM_MANUAL_0, RANK_SEL, MODE_REGISTER_WRITE, chip_select, 1);

         while (HWIO_INXF (reg_offset_shke, SHKE_DRAM_MANUAL_0, MODE_REGISTER_WRITE));

      }

   }



}//BIMC_MR_Write_sdi





//================================================================================================//

//Mode register read function only returns a single MR value, it reads on per channel per rank basis

//Both-channel option is not supported for DDR_CHANNEL

//================================================================================================//

uint32 BIMC_MR_Read_sdi (DDR_CHANNEL channel, DDR_CHIPSELECT chip_select, uint32 MR_addr)

{

   uint32  read_value = 0;

   uint32 reg_offset_shke = 0;



   reg_offset_shke = REG_OFFSET_SHKE(CH_INX(channel));



   HWIO_OUTXF (reg_offset_shke, SHKE_MREG_ADDR_WDATA_CNTL, MREG_ADDR, MR_addr);



   HWIO_OUTXF2 (reg_offset_shke, SHKE_DRAM_MANUAL_0, RANK_SEL, MODE_REGISTER_READ, chip_select, 1);

   while (HWIO_INXF (reg_offset_shke, SHKE_DRAM_MANUAL_0, MODE_REGISTER_READ));



   if(chip_select == DDR_CS0) {

      read_value = HWIO_INX (reg_offset_shke, SHKE_MREG_RDATA_RANK0_L);

   }

   if(chip_select == DDR_CS1) {

      read_value = HWIO_INX (reg_offset_shke, SHKE_MREG_RDATA_RANK1_L);

   }





   return read_value;

}//BIMC_MR_Read







//================================================================================================//

// Periodic events controlled through this function are

// 1. Auto refresh

// 2. HW self refresh (activity based)

// 3. ZQ calibration

// 4. Temperature status read

//================================================================================================//

void BIMC_All_Periodic_Ctrl_sdi (DDR_STRUCT *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select, uint8 enable)

{

   uint8  rank_ctrl = 0;

   uint8  ch        = 0;

   uint32 reg_offset_shke = 0;



   for (ch = 0; ch < 2; ch++)

   {

      reg_offset_shke = REG_OFFSET_SHKE(ch);



      if ((channel >> ch) & 0x1)

      {

        // Auto refresh and HW activity based self refresh

        if (chip_select & DDR_CS0) {

           HWIO_OUTXF (reg_offset_shke, SHKE_AUTO_REFRESH_CNTL, AUTO_RFSH_ENABLE_RANK0,    enable);

           BIMC_HW_Self_Refresh_Ctrl_sdi (ddr, ch, 0, enable);

        }

        if (chip_select & DDR_CS1) {

           HWIO_OUTXF (reg_offset_shke, SHKE_AUTO_REFRESH_CNTL, AUTO_RFSH_ENABLE_RANK1,    enable);

           BIMC_HW_Self_Refresh_Ctrl_sdi (ddr, ch, 1, enable);

        }





        /* COMMON_CODE_TODO: ZQ Calibration Mode and Interval programming

           The following code fragment is required for LPDDR4 targets. It has not yet

           been determined if it is required for LPDDR3 targets. It is currently compile-guarded

           by #ifdef such that if required for LPDDR3 targets, it can be enabled quickly.

        */ 

          // V1 workaround for ZQCAL bug.

          // Periodic ZQ calibration





        //wait for 1us

        seq_wait_sdi(1, US);

        rank_ctrl = HWIO_INXF (reg_offset_shke, SHKE_PERIODIC_ZQCAL, RANK_SEL);

        rank_ctrl = enable ? (rank_ctrl | chip_select) : (rank_ctrl & ~chip_select);

        HWIO_OUTXF  (reg_offset_shke, SHKE_PERIODIC_ZQCAL, RANK_SEL, rank_ctrl);



        // Periodic temperature status read (SRR)

        rank_ctrl = HWIO_INXF  (reg_offset_shke, SHKE_PERIODIC_MRR, MRR_RANK_SEL);

        rank_ctrl = enable ? (rank_ctrl | chip_select) : (rank_ctrl & ~chip_select);

        HWIO_OUTXF  (reg_offset_shke, SHKE_PERIODIC_MRR, MRR_RANK_SEL, rank_ctrl);



      }

   }

}









//================================================================================================//

// Based on device RL/WL/ODTLon frequency band, get an index for selecting in RL/WL/ODTLon table

//================================================================================================//

uint8 BIMC_RL_WL_Freq_Index_sdi (DDR_STRUCT *ddr, uint32 clk_freq_khz)

{

   uint8 clk_idx;



   for (clk_idx = 0; (clk_idx < RL_WL_freq_range_table_size_sdi); clk_idx++)

   {

      if (clk_freq_khz <= RL_WL_freq_range_table_sdi[clk_idx])

         break;

   }



   return clk_idx;

}





//================================================================================================//

// select RL/WL/MR2 from the table based on frequency band

// table_sel options are RL, WL, MR2_WR_VAL

//================================================================================================//

uint8 BIMC_RL_WL_Table_Sel_sdi (DDR_STRUCT *ddr, RL_WL_Table_Sel table_sel, uint32 clk_freq_khz)

{

   uint8  freq_range_index=0;

   uint8  freq_count=0;



   uint8  (*lpddr_freq_ptr)[4];

   lpddr_freq_ptr = RL_WL_lpddr_sdi;

   freq_count = RL_WL_lpddr_size_sdi;



   freq_range_index = BIMC_RL_WL_Freq_Index_sdi (ddr, clk_freq_khz);



   // Klockwork error fix: Return some determinate value even if input parameter clk_freq_khz was not found

   // in the search conducted by BIMC_Get_Freq_Range_Index_sdi().

   if(freq_range_index < freq_count)

        return lpddr_freq_ptr[freq_range_index][table_sel];

   else

       return 0xff;

}









//================================================================================================//

// void BIMC_DDR_Access_Enable_sdi(ddr, channel, chip_select)

// This function sets up the DDR read/write latency, and makes sure that missing ranks have CK and CKE disabled, allowing

//    communication to the external DDR devices.

// Channel=DDR_CH_BOTH is not supported in this function

//================================================================================================//

DDR_CHIPSELECT BIMC_DDR_Access_Enable_sdi(DDR_STRUCT *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select)

{

   uint8  cs = 0;

   uint8  ch_inx           = 0;

   DDR_CHIPSELECT  cs_1hot = DDR_CS0;

   uint32 reg_offset_shke  = 0;



   ch_inx = CH_INX(channel);

   reg_offset_shke = REG_OFFSET_SHKE(ch_inx);

		

   // Flag that initialization is complete on the populated ranks.  If the ranks are not populated, these bits will

   //    be left at 0.

   if ((((DDR_CHIPSELECT)ddr->cdt_params[ch_inx].common.populated_chipselect) & DDR_CS0) == DDR_CS0)

   {

      HWIO_OUTXF2(reg_offset_shke, SHKE_CONFIG, RANK0_INITCOMPLETE, RANK0_EN, 1, 1);

   }

   else

   {

      // Disable CKE to the unpopulated rank

      HWIO_OUTXF2 (reg_offset_shke, SHKE_DRAM_MANUAL_0, RANK_SEL, CKE_OFF, DDR_CS0, 0x1);

      while (HWIO_INXF (reg_offset_shke, SHKE_DRAM_MANUAL_0, CKE_OFF));



      // Disable CK to the unpopulated rank

      HWIO_OUTXF2 (reg_offset_shke, SHKE_DRAM_MANUAL_0, RANK_SEL, CK_OFF, DDR_CS0, 0x1);

      while (HWIO_INXF (reg_offset_shke, SHKE_DRAM_MANUAL_0, CK_OFF));

   }

   

   if ((((DDR_CHIPSELECT)ddr->cdt_params[ch_inx].common.populated_chipselect) & DDR_CS1) == DDR_CS1)

   {

      HWIO_OUTXF2(reg_offset_shke, SHKE_CONFIG, RANK1_INITCOMPLETE, RANK1_EN, 1, 1);

   }

   else

   {

      // Disable CKE to the unpopulated rank

      HWIO_OUTXF2 (reg_offset_shke, SHKE_DRAM_MANUAL_0, RANK_SEL, CKE_OFF, DDR_CS1, 0x1);

      while (HWIO_INXF (reg_offset_shke, SHKE_DRAM_MANUAL_0, CKE_OFF));



      // Disable CK to the unpopulated rank

      HWIO_OUTXF2 (reg_offset_shke, SHKE_DRAM_MANUAL_0, RANK_SEL, CK_OFF, DDR_CS1, 0x1);

      while (HWIO_INXF (reg_offset_shke, SHKE_DRAM_MANUAL_0, CK_OFF));

   }

   

   // Set up read/write latencies for mode reg reads and writes to the DDR

   for (cs = 0; cs < 2; cs++)

   {

      cs_1hot = CS_1HOT(cs);



      if ((chip_select >> cs) & 0x1)

      {



         // Set up the DDR device latencies so that we can read and write to it.

         BIMC_MR_Write_sdi (channel, cs_1hot, JEDEC_MR_2, 0);  //read write latency

         BIMC_MR_Write_sdi (channel, cs_1hot, JEDEC_MR_13, 0x80); //set FSP_OP=1, FSP_WR=0

         BIMC_MR_Write_sdi (channel, cs_1hot, JEDEC_MR_2, 0);  //read write latency		 		 		 	 		 

         BIMC_MR_Write_sdi (channel, cs_1hot, JEDEC_MR_13, 0);

		 BIMC_MR_Write_sdi (channel, cs_1hot, JEDEC_MR_2, 0);  //read write latency		 		 		 	 		 

         seq_wait_sdi(1, MS);

         BIMC_MR_Write_sdi (channel, cs_1hot, JEDEC_MR_1, 0x06); 	



         BIMC_MR_Write_sdi (channel, cs_1hot, JEDEC_MR_3, 0xB0); //10  < F <= 400MHz, set MR3[0],PU-CAL=VDDQ/2.5 for fsp0, it was 0xB1(VDDQ/3)	   	

         BIMC_MR_Write_sdi (channel, cs_1hot, JEDEC_MR_11, 0x0);

         BIMC_MR_Write_sdi (channel, cs_1hot, JEDEC_MR_22, 0x0);



         // If we can read from Mode Register 8, then we are working correctly.

	 // mr8_value = BIMC_MR_Read_sdi(channel, cs_1hot, JEDEC_MR_8);

      }

   }



   return ((DDR_CHIPSELECT)ddr->cdt_params[ch_inx].common.populated_chipselect);

}









//================================================================================================//

// Programs the AC Parameters for LPDDR4, during BIMC Pre Init

//================================================================================================//

void BIMC_Program_Lpddr_AC_Parameters_sdi(DDR_STRUCT *ddr, DDR_CHANNEL channel)

{

   uint32 reg_offset_dpe  = 0;

   reg_offset_dpe  = REG_OFFSET_DPE(channel);

 

   HWIO_OUTXF  (reg_offset_dpe, DPE_DRAM_TIMING_24, TCCDMW, ddr->cdt_params[channel].lpddr.tCCDMW);

   HWIO_OUTXF  (reg_offset_dpe, DPE_DRAM_TIMING_25, TCKEHCMD, ddr->cdt_params[channel].lpddr.tCKEHCMD);

   HWIO_OUTXF2 (reg_offset_dpe, DPE_DRAM_TIMING_29, TDQS2DQMIN, TDQS2DQMAX,

                 ddr->cdt_params[channel].lpddr.tDQS2DQMIN, ddr->cdt_params[channel].lpddr.tDQS2DQMAX);

   HWIO_OUTXF  (reg_offset_dpe, DPE_DRAM_TIMING_30, TZQL, ddr->cdt_params[channel].lpddr.tZQLAT);

   HWIO_OUTXF2 (reg_offset_dpe, DPE_DRAM_TIMING_31, TDQSCKMIN, TDQSCKMAX,

                 ddr->cdt_params[channel].lpddr.tDQSCK_min, ddr->cdt_params[channel].lpddr.tDQSCK_max);

   HWIO_OUTXF  (reg_offset_dpe, DPE_DRAM_TIMING_34, TFC, ddr->cdt_params[channel].lpddr.tFC);

   HWIO_OUTXF2 (reg_offset_dpe, DPE_DRAM_TIMING_LP4_ODT_ON_CNTL, LP4_ODT_ONMAX, LP4_ODT_ONMIN,

                 ddr->cdt_params[channel].lpddr.tODTonmax, ddr->cdt_params[channel].lpddr.tODTonmin);

   HWIO_OUTXF2 (reg_offset_dpe, DPE_DRAM_TIMING_LP4_ODT_OFF_CNTL, LP4_ODT_OFFMAX, LP4_ODT_OFFMIN,

                 ddr->cdt_params[channel].lpddr.tODToffmax, ddr->cdt_params[channel].lpddr.tODToffmin);

}



//================================================================================================//

// Manual ZQ calibration for LPDDR4

// This function supports ZQ Cal per rank and both ranks. While using for both ranks, ZQCAL_START

// is issued for one rank followed by next rank. ZQCAL_LATCH for both ranks are issued together.

//================================================================================================//

void BIMC_ZQ_Calibration_Lpddr_sdi (DDR_STRUCT  *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select)

{

   uint8  ch = 0;

   uint8  cs = 0;

   uint8  ch_1hot = 0;

   uint32 reg_offset_shke = 0;



   for (ch = 0; ch < 2; ch++)

   {

      ch_1hot = CH_1HOT(ch);

      reg_offset_shke = REG_OFFSET_SHKE(ch);



      if ((channel >> ch) & 0x1)

      {

         // tZQCAL has 100ps resolution, first multiply by 100 to convert resolution to 1ps,

         // then convert to XO_PERIOD. Rounding up using div_ceil function which rounds up

         // any decimal value to the next integer.

         BIMC_Wait_Timer_Setup_sdi ((DDR_CHANNEL)ch_1hot, WAIT_XO_CLOCK,

                                  (div_ceil((ddr->cdt_params[ch].lpddr.tZQCAL * 100), XO_PERIOD_IN_PS)));



         for (cs = 0; cs < 2; cs++)

         {

            if ((chip_select >> cs) & 0x1)

            {

            HWIO_OUTXF2 (reg_offset_shke, SHKE_DRAM_MANUAL_0, RANK_SEL, ZQCAL_START, CS_1HOT(cs), 1);

            while (HWIO_INXF (reg_offset_shke, SHKE_DRAM_MANUAL_0, ZQCAL_START));

            }

         }



         // ZQCAL_LATCH doesnot need wait timer. tZQLAT timing is already taken care while

         // executing ZQCAL_LATCH. Hence set the wait timer to 0.

         BIMC_Wait_Timer_Setup_sdi ((DDR_CHANNEL)ch_1hot, WAIT_XO_CLOCK, 0);



         HWIO_OUTXF2 (reg_offset_shke, SHKE_DRAM_MANUAL_0, RANK_SEL, ZQCAL_LATCH, chip_select, 1);

         while (HWIO_INXF (reg_offset_shke, SHKE_DRAM_MANUAL_0, ZQCAL_LATCH));

      }

   }

}







void DDRSS_ddr_phy_sw_freq_switch_sdi (DDR_STRUCT *ddr, uint32 clk_freq_khz, uint8 ch)

{



    uint32   reg_offset_ddr_phy = 0;

    uint8 sw_handshake_complete = 1;



    // Configure the DDR PHY address offset

    reg_offset_ddr_phy = REG_OFFSET_DDR_PHY_CH(ch);



    // Enable broadcast mode for 4 DQ PHYs and 2 CA PHYs

    HWIO_OUTX((DDR_SS_BASE + SEQ_DDR_SS_DDRSS_AHB2PHY_SWMAN_OFFSET), AHB2PHY_SWMAN_AHB2PHY_BROADCAST_EN_CFG_LOWER, 0x3F << (ch * 7));



    /// set FPM_INIT_START to 1 to start frequency switch

    DDR_PHY_hal_cfg_sw_handshake_start_sdi (ddr, BROADCAST_BASE, clk_freq_khz);



    /// Poll for DDRPHY_FPM_TOP_STA[FPM_SW_INIT_COMP]

    while (sw_handshake_complete == 0x1) {

      sw_handshake_complete = ((0x00000001 & ((HWIO_INX (reg_offset_ddr_phy + CA0_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_FPM_TOP_STA))>>1)) |

                               (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy + CA1_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_FPM_TOP_STA))>>1)) |

                               (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy + DQ0_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_FPM_TOP_STA))>>1)) |

                               (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy + DQ1_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_FPM_TOP_STA))>>1)) |

                               (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy + DQ2_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_FPM_TOP_STA))>>1)) |

                               (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy + DQ3_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_FPM_TOP_STA))>>1)));

    }



    /// set FPM_INIT_START to 0 to stop frequency switch

    DDR_PHY_hal_cfg_sw_handshake_stop_sdi (ddr, BROADCAST_BASE, clk_freq_khz);



    // Poll for handshake complete

    while (sw_handshake_complete == 0x0) {

      sw_handshake_complete = ((0x00000001 & ((HWIO_INX (reg_offset_ddr_phy + CA0_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_FPM_TOP_STA))>>1)) &

                               (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy + CA1_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_FPM_TOP_STA))>>1)) &

                               (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy + DQ0_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_FPM_TOP_STA))>>1)) &

                               (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy + DQ1_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_FPM_TOP_STA))>>1)) &

                               (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy + DQ2_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_FPM_TOP_STA))>>1)) &

                               (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy + DQ3_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_FPM_TOP_STA))>>1)));

    }



    /// disable software trigger handshake, and enable hardware FPM

    DDR_PHY_hal_cfg_sw_handshake_complete_sdi (BROADCAST_BASE);



    // Disable broadcast mode

    HWIO_OUTX((DDR_SS_BASE + SEQ_DDR_SS_DDRSS_AHB2PHY_SWMAN_OFFSET), AHB2PHY_SWMAN_AHB2PHY_BROADCAST_EN_CFG_LOWER, 0x0);

}











//================================================================================================//

// DDR PHY and CC Initialization

//================================================================================================//

void DDR_PHY_CC_init_sdi (DDR_STRUCT *ddr, DDR_CHANNEL channel, uint32 clk_freq_khz)

{

   uint8    ch = 0;

   uint32   reg_offset_ddr_phy;

   uint8    ca0_master;

   uint8    ca1_master;

   uint32   iocal_done = 0x0;

//   uint32   sw_handshake_complete = 0x1;



   for (ch = 0; ch < 2; ch++)

   {

      reg_offset_ddr_phy = REG_OFFSET_DDR_PHY_CH(ch);



      if ((channel >> ch) & 0x1)

      {



          //turn on LVDS terminiation for DQ0 and DQ3 PHY       

         // Initialize polling variables

         iocal_done            = 0x0;

//        sw_handshake_complete = 0x1;



         // ----------------------------------------------------------------------------------------

         // PHY one time setting for both CA and DQ.

         // DDR_PHY_hal_cfg_init (uint32 _inst_, uint32 clk_freq_khz, uint8 lpddr4)

         // master_phy: 1 = CA PHY with calibration master inside, 0 = DQ PHY or CA PHY without calibratoin master.

         // clk_freq_khz: boot clock frequency

         // ----------------------------------------------------------------------------------------

         // master PHY for all update requests 

         //Only CA0 is master for both channels	

         //ca0_master = (ch == 0) ? 1 : 0;     //In channel 0, CA0 is the master PHY marcro. 

         //ca1_master = (ch == 1) ? 1 : 0;     //In channel 1, CA1 is the master PHY marcro. 



         ca0_master = 1;

         ca1_master = 0;

         

         // Initiate IO Calibration for the DDR PHY CA Master 

         DDR_PHY_hal_cfg_sw_iocal_sdi (reg_offset_ddr_phy + CA0_DDR_PHY_OFFSET, ca0_master ); 

         DDR_PHY_hal_cfg_sw_iocal_sdi (reg_offset_ddr_phy + CA1_DDR_PHY_OFFSET, ca1_master );



         // Enable broadcast mode for 4 DQ PHYs 

         HWIO_OUTX((DDR_SS_BASE + SEQ_DDR_SS_DDRSS_AHB2PHY_SWMAN_OFFSET), AHB2PHY_SWMAN_AHB2PHY_BROADCAST_EN_CFG_LOWER, 0x3c << (ch * 7));



          // 8996 DCC init state reset sequence

          // Clean-up: 8996 platform_id check removed. 

          HWIO_OUTXF2 (BROADCAST_BASE, DDR_PHY_DDRPHY_CMSLICE0_PBIT_CTL_DQ_0_RXTX_CFG, TX0_EN, RX0_EN, 0x0 , 0x0 );

          HWIO_OUTXF2 (BROADCAST_BASE, DDR_PHY_DDRPHY_CMSLICE1_PBIT_CTL_DQ_1_RXTX_CFG, TX1_EN, RX1_EN, 0x0 , 0x0 );

          HWIO_OUTXF2 (BROADCAST_BASE, DDR_PHY_DDRPHY_CMSLICE2_PBIT_CTL_DQ_2_RXTX_CFG, TX2_EN, RX2_EN, 0x0 , 0x0 );

          HWIO_OUTXF2 (BROADCAST_BASE, DDR_PHY_DDRPHY_CMSLICE3_PBIT_CTL_DQ_3_RXTX_CFG, TX3_EN, RX3_EN, 0x0 , 0x0 );

          HWIO_OUTXF2 (BROADCAST_BASE, DDR_PHY_DDRPHY_CMSLICE4_PBIT_CTL_DQ_4_RXTX_CFG, TX4_EN, RX4_EN, 0x0 , 0x0 );

          HWIO_OUTXF2 (BROADCAST_BASE, DDR_PHY_DDRPHY_CMSLICE5_PBIT_CTL_DQ_5_RXTX_CFG, TX5_EN, RX5_EN, 0x0 , 0x0 );

          HWIO_OUTXF2 (BROADCAST_BASE, DDR_PHY_DDRPHY_CMSLICE6_PBIT_CTL_DQ_6_RXTX_CFG, TX6_EN, RX6_EN, 0x0 , 0x0 );

          HWIO_OUTXF2 (BROADCAST_BASE, DDR_PHY_DDRPHY_CMSLICE7_PBIT_CTL_DQ_7_RXTX_CFG, TX7_EN, RX7_EN, 0x0 , 0x0 );

          HWIO_OUTXF2 (BROADCAST_BASE, DDR_PHY_DDRPHY_CMSLICE8_PBIT_CTL_DQ_8_RXTX_CFG, TX8_EN, RX8_EN, 0x0 , 0x0 );

          HWIO_OUTXF2 (BROADCAST_BASE, DDR_PHY_DDRPHY_CMSLICE9_PBIT_CTL_DQ_9_RXTX_CFG, TX9_EN, RX9_EN, 0x0 , 0x0 ); 		  

          HWIO_OUTXF2 (BROADCAST_BASE, DDR_PHY_DDRPHY_CMSLICE10_PBIT_CTL_DQS_CFG, TX_EN, RX_EN, 0x0 , 0x0 );

          while ( (HWIO_INXF ((REG_OFFSET_DDR_PHY_CH(ch) + DQ0_DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMSLICE10_PBIT_CTL_DQS_CFG, TX_EN) !=0x0) ||

          (HWIO_INXF ((REG_OFFSET_DDR_PHY_CH(ch) + DQ1_DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMSLICE10_PBIT_CTL_DQS_CFG, TX_EN) !=0x0) ||

          (HWIO_INXF ((REG_OFFSET_DDR_PHY_CH(ch) + DQ2_DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMSLICE10_PBIT_CTL_DQS_CFG, TX_EN) !=0x0) ||

          (HWIO_INXF ((REG_OFFSET_DDR_PHY_CH(ch) + DQ3_DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMSLICE10_PBIT_CTL_DQS_CFG, TX_EN) !=0x0)

           );

          HWIO_OUTXF2 (BROADCAST_BASE, DDR_PHY_DDRPHY_CMSLICE0_PBIT_CTL_DQ_0_RXTX_CFG, TX0_EN, RX0_EN, 0x1 , 0x1 );

          HWIO_OUTXF2 (BROADCAST_BASE, DDR_PHY_DDRPHY_CMSLICE1_PBIT_CTL_DQ_1_RXTX_CFG, TX1_EN, RX1_EN, 0x1 , 0x1 );

          HWIO_OUTXF2 (BROADCAST_BASE, DDR_PHY_DDRPHY_CMSLICE2_PBIT_CTL_DQ_2_RXTX_CFG, TX2_EN, RX2_EN, 0x1 , 0x1 );

          HWIO_OUTXF2 (BROADCAST_BASE, DDR_PHY_DDRPHY_CMSLICE3_PBIT_CTL_DQ_3_RXTX_CFG, TX3_EN, RX3_EN, 0x1 , 0x1 );

          HWIO_OUTXF2 (BROADCAST_BASE, DDR_PHY_DDRPHY_CMSLICE4_PBIT_CTL_DQ_4_RXTX_CFG, TX4_EN, RX4_EN, 0x1 , 0x1 );

          HWIO_OUTXF2 (BROADCAST_BASE, DDR_PHY_DDRPHY_CMSLICE5_PBIT_CTL_DQ_5_RXTX_CFG, TX5_EN, RX5_EN, 0x1 , 0x1 );

          HWIO_OUTXF2 (BROADCAST_BASE, DDR_PHY_DDRPHY_CMSLICE6_PBIT_CTL_DQ_6_RXTX_CFG, TX6_EN, RX6_EN, 0x1 , 0x1 );

          HWIO_OUTXF2 (BROADCAST_BASE, DDR_PHY_DDRPHY_CMSLICE7_PBIT_CTL_DQ_7_RXTX_CFG, TX7_EN, RX7_EN, 0x1 , 0x1 );

          HWIO_OUTXF2 (BROADCAST_BASE, DDR_PHY_DDRPHY_CMSLICE8_PBIT_CTL_DQ_8_RXTX_CFG, TX8_EN, RX8_EN, 0x1 , 0x1 );

          HWIO_OUTXF2 (BROADCAST_BASE, DDR_PHY_DDRPHY_CMSLICE9_PBIT_CTL_DQ_9_RXTX_CFG, TX9_EN, RX9_EN, 0x1 , 0x1 ); 

          HWIO_OUTXF2 (BROADCAST_BASE, DDR_PHY_DDRPHY_CMSLICE10_PBIT_CTL_DQS_CFG, TX_EN, RX_EN, 0x1 , 0x1 );

          while ( (HWIO_INXF ((REG_OFFSET_DDR_PHY_CH(ch) + DQ0_DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMSLICE10_PBIT_CTL_DQS_CFG, TX_EN) !=0x1) ||

          (HWIO_INXF ((REG_OFFSET_DDR_PHY_CH(ch) + DQ1_DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMSLICE10_PBIT_CTL_DQS_CFG, TX_EN) !=0x1) ||

          (HWIO_INXF ((REG_OFFSET_DDR_PHY_CH(ch) + DQ2_DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMSLICE10_PBIT_CTL_DQS_CFG, TX_EN) !=0x1) ||

          (HWIO_INXF ((REG_OFFSET_DDR_PHY_CH(ch) + DQ3_DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMSLICE10_PBIT_CTL_DQS_CFG, TX_EN) !=0x1)

           );



         // Initiate DQ Calibration with PHY DQ broadcast

         DDR_PHY_hal_cfg_sw_iocal_sdi (BROADCAST_BASE, 0x0 );



         /// poll for IOCAL_DONE to be asserted for all PHYs

         while (iocal_done == 0x0) {

           iocal_done = ((0x00000001 & ((HWIO_INX (reg_offset_ddr_phy + CA0_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_IOCTLR_TOP_1_STA))>>12)) & 

                         (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy + CA1_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_IOCTLR_TOP_1_STA))>>12)) & 

                         (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy + DQ0_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_IOCTLR_TOP_1_STA))>>12)) & 

                         (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy + DQ1_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_IOCTLR_TOP_1_STA))>>12)) & 

                         (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy + DQ2_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_IOCTLR_TOP_1_STA))>>12)) & 

                         (0x00000001 & ((HWIO_INX (reg_offset_ddr_phy + DQ3_DDR_PHY_OFFSET, DDR_PHY_DDRPHY_IOCTLR_TOP_1_STA))>>12)));

         }



        // Load the register settings by doing a SW freq switch

        DDRSS_ddr_phy_sw_freq_switch_sdi(ddr, clk_freq_khz, ch);

      

     }

   }

}

 





void DDR_PHY_hal_cfg_sw_handshake_complete_sdi( uint32 _inst_ )

{

    uint8 FPM_BYPASS_EN_0 = 0;

    ///  FPM bypass only handshake signals generated by FPM

    uint8 FPM_SW_HDSHAKE_EN_0 = 0;

    ///  FPM SW handshake mode

    uint8 BYPASS_COMPLETE_1 = 1;

    

    uint32 tmp;

    

    ///  COMPLETE signal in BYPASS mode for debug purposes

    ///  Enable the PHY FPM HW mode

    tmp = (0x600003 & 0xFF9F00F9)    | 

          (0x0 << 8)                 | 

          (FPM_SW_HDSHAKE_EN_0 << 2) | 

          (BYPASS_COMPLETE_1 << 21)  | 

          (FPM_BYPASS_EN_0 << 1)     | 

          (FPM_BYPASS_EN_0 << 22);

    HWIO_OUTX (_inst_, DDR_PHY_DDRPHY_FPM_CNTRL_CFG, tmp);

    

    tmp = (0x600003 & 0xFF1F00F9)    | 

          (0x0 << 23)                | 

          (0x0 << 8)                 | 

          (FPM_SW_HDSHAKE_EN_0 << 2) | 

          (BYPASS_COMPLETE_1 << 21)  | 

          (FPM_BYPASS_EN_0 << 1)     | 

          (FPM_BYPASS_EN_0 << 22);

    HWIO_OUTX (_inst_, DDR_PHY_DDRPHY_FPM_CNTRL_CFG, tmp);

}





void DDR_PHY_hal_cfg_sw_handshake_stop_sdi( DDR_STRUCT *ddr, uint32 _inst_, uint32 clk_freq_khz )

{

    uint32 fpm_period  = 0;

    uint8 fpm_period_1 = 0;

    uint8 fpm_period_2 = 0;

    uint8 fpm_period_3 = 0;

    ///  FPM Period derived from clk_freq_khz

    uint8 FPM_EN_1 = 1;

    uint8 FPM_SW_HDSHAKE_EN_1 = 1;

    ///  Enable FPM SW handshake mode

    uint8 FPM_BYPASS_EN_0 = 0;

    ///  disable FPM bypass only handshake signals generated by FPM

    uint8 FPM_INIT_START_0 = 0;  

    uint32 tmp;

    

    ///  set FPM_INIT_START to 0

    ///  Calculate the fpm period

    fpm_period = (CONVERT_CYC_TO_PS / clk_freq_khz);	

    fpm_period_1 = fpm_period & 0xFF;

    fpm_period_2 = fpm_period >>8 & 0xF;

    fpm_period_3 = fpm_period >>12 & 0x1F;



    ///  de-assert init_start

    tmp = (0x080 & 0xFFFFFFEF) | 

            (FPM_EN_1 << 4);

    HWIO_OUTX (_inst_, DDR_PHY_DDRPHY_TOP_CTRL_0_CFG, tmp);

    

    tmp = (0x600003 & 0xFF3F00F1) | 

          (FPM_INIT_START_0 << 3) | 

          (0x1 << 23)             | 

          (fpm_period_1 << 8)     | 

          (fpm_period_2 << 4)     |

          (fpm_period_3 << 25)    | 

          (FPM_BYPASS_EN_0 << 1)  | 

          (FPM_BYPASS_EN_0 << 22) | 

          (FPM_SW_HDSHAKE_EN_1 << 2);



    HWIO_OUTX (_inst_, DDR_PHY_DDRPHY_FPM_CNTRL_CFG, tmp);

    ///  1 = selects period from CSR source, 0 = select from primary input

}





void DDR_PHY_hal_cfg_sw_handshake_start_sdi( DDR_STRUCT *ddr, uint32 _inst_, uint32 clk_freq_khz )

{

    uint8 FPM_EN_1 = 1;

    uint32 fpm_period  = 0;

    uint8 fpm_period_1 = 0;

    uint8 fpm_period_2 = 0;

    uint8 fpm_period_3 = 0;

    ///  Enable FPM. Set 0 to disable/clock gate FPM. If set frequency switch can be done only through SW

    uint8 FPM_SW_HDSHAKE_EN_1 = 1;

    ///  Enable FPM SW handshake mode

    uint8 FPM_INIT_START_1 = 1;

    ///  set FPM_INIT_START to 1

    uint8 FPM_BYPASS_EN_0 = 0;

    ///  disable FPM bypass only handshake signals generated by FPM      

    uint32 tmp;

    

    ///  FPM Period derived from clk_freq_khz

//    HWIO_OUTX (_inst_, DDR_PHY_DDRPHY_UPDATE_INTF_CFG, 0x3200);

//    HWIO_OUTX (_inst_, DDR_PHY_DDRPHY_UPDATE_INTF_CFG, 0x3000);

    ///  Do a SW frequency switch to load the FPM period into the PHY

    ///  Calculate the fpm period 

    fpm_period = (CONVERT_CYC_TO_PS / clk_freq_khz);

    fpm_period_1 = fpm_period & 0xFF;

    fpm_period_2 = fpm_period >>8 & 0xF;

    fpm_period_3 = fpm_period >>12 & 0x1F;	    



    ///  Assert init_start

    tmp = (0x080 & 0xFFFFFFEF) | (FPM_EN_1 << 4);

    HWIO_OUTX (_inst_, DDR_PHY_DDRPHY_TOP_CTRL_0_CFG, tmp);

    

    tmp = (0x600003 & 0xFF3F00F9) | 

          (0x1 << 23)             | 

          (fpm_period_1 << 8)     | 

          (fpm_period_2 << 4)     |

          (fpm_period_3 << 25)    | 

          (FPM_BYPASS_EN_0 << 1)  | 

          (FPM_BYPASS_EN_0 << 22) | 

          (FPM_SW_HDSHAKE_EN_1 << 2);    



    HWIO_OUTX (_inst_, DDR_PHY_DDRPHY_FPM_CNTRL_CFG, tmp);

    

    ///  1 = selects period from CSR source, 0 = select from primary input

    tmp = (0x600003 & 0xFF3F00F1) | 

          (FPM_INIT_START_1 << 3) | 

          (0x1 << 23)             | 

          (fpm_period_1 << 8)     | 

          (fpm_period_2 << 4)     |

          (fpm_period_3 << 25)    |	

          (FPM_BYPASS_EN_0 << 1)  | 

          (FPM_BYPASS_EN_0 << 22) | 

          (FPM_SW_HDSHAKE_EN_1 << 2);    



    HWIO_OUTX (_inst_, DDR_PHY_DDRPHY_FPM_CNTRL_CFG, tmp);

    ///  1 = selects period from CSR source, 0 = select from primary input

}





void DDR_PHY_hal_cfg_sw_iocal_sdi( uint32 _inst_, uint8 master_phy )

{



    ///  To minimize power consumption due to unnecessary toggling of the 'o_spdm_clk', set DDRPHY_TOP_TEST_CFG.SPDM_EN to 0x0

    HWIO_OUTX (_inst_, DDR_PHY_DDRPHY_TOP_TEST_CFG, 0xC30000);

    ///  set master phy bit

    if (master_phy) {

        HWIO_OUTX (_inst_, DDR_PHY_DDRPHY_UPDATE_INTF_CFG, 0x3000);

    }

    HWIO_OUTX (_inst_, DDR_PHY_DDRPHY_IOCTLR_CTRL_CFG, 0x4E038080);

    HWIO_OUTX (_inst_, DDR_PHY_DDRPHY_IOCTLR_CTRL_CFG, 0xCE038080);

    HWIO_OUTX (_inst_, DDR_PHY_DDRPHY_IOCTLR_CTRL_CFG, 0x4E038080);

}









//================================================================================================//

// DDR Pre-Init function. Used to apply target- and revision-specific workaournds if any.

//================================================================================================//

uint32 DDRSS_Pre_Init_sdi(DDR_STRUCT *ddr)

{

    return TRUE;

}

