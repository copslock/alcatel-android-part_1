/*==============================================================================
                      Warning: This file is auto-generated
================================================================================
                   Copyright 2014,2016 Qualcomm Technologies Incorporated
                              All Rights Reserved
                     Qualcomm Confidential and Proprietary
==============================================================================*/
/*==============================================================================
                                  INCLUDES
==============================================================================*/
#include "ddr_phy_config.h"
#include "HALhwio.h"
#include "ddr_ss_seq_hwiobase.h"
#include "ddr_ss_seq_hwioreg.h"


/*==============================================================================
                                  DATA
==============================================================================*/

uint32 ddr_phy_dq_config[][2] =
{
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_TOP_CTRL_0_CFG), 0x0001C090},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_PAD_CNTL_0_CFG), 0x00132001},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_PAD_CNTL_1_CFG), 0x00007700},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_PAD_CNTL_2_CFG), 0x00000011},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_PAD_CNTL_3_CFG), 0x006008FE},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_PAD_CNTL_4_CFG), 0x006008FE},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_PAD_VREF_CFG), 0x1F1F0008},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_IOCS_DQ_CFG), 0x80000000},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_IOCS_DQS_CFG), 0x80000000},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_IOCTLR_COMP_AUTO_OFF_CFG), 0x00000033},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_IOCTLR_TIMERS_CFG), 0x10000013},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_IOCTLR_PNCNT_INIT_CFG), 0x00001010},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_IOCTLR_CTRL_CFG), 0xCE038080},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMCDCWRLVL_TOP_CFG), 0x00200004},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMCDCWR_CTL_CFG), 0x1EF4E138},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMCDCWR_TOP_CFG), 0x04200004},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMCDCRD_CTL_CFG), 0x1EF4E138},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMCDCRD_TOP_CFG), 0x00200404},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMCDCRDT2_I0_CTL_CFG), 0x31866198},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMCDCRDT2_I0_TOP_CFG), 0x00200004},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMCDCRCW_CTL_CFG), 0x31866198},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMCDCRCW_TOP_CFG), 0x02A00004},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMCDCMSTR_CTL_CFG), 0x0000022D},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMLDO_TOP_CFG), 0x00000068},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMIO_PAD_MODE_CFG), 0x00000220},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMIO_PAD_OE_CFG), 0x00000200},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMIO_PAD_IE_CFG), 0x00000000},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMHUB_TOP_1_CFG), 0x02020202},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_FEA_WT_DEP_15_CFG), 0x000037FF},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_LO_CFG), 0x0000A492},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_HI_CFG), 0x011EC080},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_LO_CFG), 0x0000A494},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_HI_CFG), 0x013ED100},  //was 0x003ED100, bypass per-bit
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_LO_CFG), 0x0000A494},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_HI_CFG), 0x00BED100},  //turn on LVDS_RX for band2 for 421 and 547MHz
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_LO_CFG), 0x0000B6DC},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_HI_CFG), 0x00BED100},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_LO_CFG), 0xC920A5B5},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_HI_CFG), 0x00DEDE6D},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_LO_CFG), 0xC920A5B5},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_HI_CFG), 0x00DEDE6D},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_LO_CFG), 0xC920A5B5},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_HI_CFG), 0x00DEDE6D},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_LO_CFG), 0xC920B7B5},  //was 0xC920A5B5, increase PU to 60ohm
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_HI_CFG), 0x00DEDE6D},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_LUT0_CFG), 0x714294D0},  //was for 412 GCC: 0x714250D0},  //B0:250M, B1: 453M, B2: 600MHz, B3:800M
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_LUT1_CFG), 0x083850D5},  //B4:1066M, B5:1488M, B6:1603M, B7:1894M  
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_HI_LUT0_CFG), 0x02030507},  //was for 412 GCC: 0x02030407},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_HI_LUT1_CFG), 0x01010101},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMRX_USER_CFG), 0x00000191},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMRX_TEST_CFG), 0x0000000C},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_WRLVL_6_CTL_CFG), 0x0},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_WRLVL_7_CTL_CFG), 0x0},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_WR_0_CTL_CFG), 0x2947E1F8}, //was 0x29404010, for operation at VDDA=NOM or TURBO
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_WR_1_CTL_CFG), 0x2947E1F8}, //was 0x29404010, for operation at VDDA=NOM or TURBO
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_WR_2_CTL_CFG), 0x00000000},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_WR_3_CTL_CFG), 0x0000340D},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_WR_4_CTL_CFG), 0x04219465},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_WR_5_CTL_CFG), 0x02118C09},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_WR_6_CTL_CFG), 0x02108C23},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_WR_7_CTL_CFG), 0x02118862},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_RD_0_CTL_CFG), 0x00001806},  //was 0x3FF01806, based on LPCDC shmoo 
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_RD_1_CTL_CFG), 0x00001004},  //was 0x0000280A, re-adjust with per-bit bypassed //was 0x0A51300C, // 0x0A511C10},  //was for 412 GCC: 0x0A511C06},  //up to 412MHz
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_RD_2_CTL_CFG), 0x0A511C07},   
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_RD_3_CTL_CFG), 0x06319C67},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_RD_4_CTL_CFG), 0x04219403},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_RD_5_CTL_CFG), 0x02118C63},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_RD_6_CTL_CFG), 0x02108C23},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_RD_7_CTL_CFG), 0x02118862},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_RCW_0_CTL_CFG), 0x31866198},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_RCW_1_CTL_CFG), 0x31866198},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_RCW_2_CTL_CFG), 0x31866198},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_RCW_3_CTL_CFG), 0x31866198},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_RCW_4_CTL_CFG), 0x31866198},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_RCW_5_CTL_CFG), 0x31866198},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_RDT2_0_CTL_CFG), 0x31866198},  //up to 200MHz
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_RDT2_1_CTL_CFG), 0x31866198},  //up to 300. //412MHz
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_RDT2_2_CTL_CFG), 0x16B0DC32},  //up to 547Mhz
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_RDT2_3_CTL_CFG), 0x0E713C0D},  //up to 768MHz
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_RDT2_4_CTL_CFG), 0x0A512C0A},  //up to 1017MHz
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_RDT2_5_CTL_CFG), 0x06319C67},  //up to 1555MHz
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_RDT2_6_CTL_CFG), 0x06319866},  //up to 1804MHz
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_RDT2_7_CTL_CFG), 0x04219465},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_WRLVLEXT_CTL_0_CFG), 0x03420342},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_WRLVLEXT_CTL_1_CFG), 0x03420342},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_WRLVLEXT_CTL_2_CFG), 0x03420342},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_WRLVLEXT_CTL_3_CFG), 0x03420342},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_WRLVLEXT_CTL_4_CFG), 0x03020302},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_WRLVLEXT_CTL_5_CFG), 0x07460746}, //add full cycle to CLK and CA. 
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_WRLVLEXT_CTL_6_CFG), 0x07460746}, //add full cycle to CLK and CA. 
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_WRLVLEXT_CTL_7_CFG), 0x07460746}, //add full cycle to CLK and CA. 
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_RCWPREAMBEXT_TOP_3_CFG), 0x00001010},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_RCWPREAMBEXT_TOP_4_CFG), 0x00001012},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_RCWPREAMBEXT_TOP_5_CFG), 0x00003036},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_RCWPREAMBEXT_TOP_6_CFG), 0x00003036},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_RCWPREAMBEXT_TOP_7_CFG), 0x00003036}, 
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMHUB_TOP_RD_RANK_EN_CFG), 0x02060206}, //force rank_en 1 full cycle to match with command bus delay.    
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_UPDATE_INTF_CFG), 0x00000000},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_HI2_CFG), 0x00000410},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_HI2_CFG), 0x00000451},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_HI2_CFG), 0x00000451},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_HI2_CFG), 0x00000492},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_HI2_CFG), 0x000004D3},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_HI2_CFG), 0x0000049C},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_HI2_CFG), 0x0000049C},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_HI2_CFG), 0x0000049C},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_DCC_TOP_4_CFG), 0x3FFFFFC},    
   {0x0, 0x0} 
};

uint32 ddr_phy_ca_config[][2] =
{
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_TOP_CTRL_0_CFG), 0x0001C090},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_PAD_CNTL_0_CFG), 0x00100001},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_PAD_CNTL_1_CFG), 0x00007700},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_PAD_CNTL_2_CFG), 0x00000011},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_PAD_CNTL_3_CFG), 0x0020087E},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_PAD_CNTL_4_CFG), 0x0020087E},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_PAD_DQ_2_CFG), 0x00000001},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_PAD_DQ_3_CFG), 0x00000001},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_IOCS_DQ_CFG), 0x80000000},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_IOCS_DQS_CFG), 0x80000000},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_IOCTLR_COMP_AUTO_OFF_CFG), 0x00000032},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_IOCTLR_TIMERS_CFG), 0x10000013},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_IOCTLR_PNCNT_INIT_CFG), 0x00001010},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_IOCTLR_CTRL_CFG), 0xCE038080},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMCDCWRLVL_TOP_CFG), 0x00200004},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMCDCWR_CTL_CFG), 0x1EF4E138},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMCDCWR_TOP_CFG), 0x00200004},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMCDCRD_CTL_CFG), 0x1EF4E138},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMCDCRD_TOP_CFG), 0x00200004},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMCDCRDT2_I0_CTL_CFG), 0x31866198},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMCDCRDT2_I0_TOP_CFG), 0x00200004},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMCDCRCW_CTL_CFG), 0x31866198},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMCDCRCW_TOP_CFG), 0x02A00004},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMCDCMSTR_CTL_CFG), 0x0000022D},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMLDO_TOP_CFG), 0x00000068},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMIO_PAD_OE_CFG), 0x00110000},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMIO_PAD_MODE_CFG), 0x003FF800},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMHUB_TOP_1_CFG), 0x02020202},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_LO_CFG), 0x0001124A},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_HI_CFG), 0x011EC080},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_LO_CFG), 0x00011248},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_HI_CFG), 0x011ED100},  //was 0x001ED100, bypass per-bit
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_LO_CFG), 0x00011248},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_HI_CFG), 0x009ED100},  //turn on LVDS_RX for band2 for 421 and 547MHz
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_LO_CFG), 0x00011258},  //was 0x00011258, increase CK PD from 120ohm to 60ohm
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_HI_CFG), 0x009ED100},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_LO_CFG), 0x492125B0}, 
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_HI_CFG), 0x00DEDA00},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_LO_CFG), 0x492125B0},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_HI_CFG), 0x00DEDA00},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_LO_CFG), 0x492125B0},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_HI_CFG), 0x00DEDA00},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_LO_CFG), 0x492125B0},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_HI_CFG), 0x00DEDA00},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_LUT0_CFG), 0x714294D0},  //was for 412 GCC: 0x714250D0},  //B0:250M, B1: 453M, B2: 600MHz, B3:800M
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_LUT1_CFG), 0x083850D5},  //B4:1066M, B5:1488M, B6:1603M, B7:1894M
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_HI_LUT0_CFG), 0x02030507},  //was for 412 GCC: 0x02030407},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_HI_LUT1_CFG), 0x01010101},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMRX_USER_CFG), 0x00000191},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMRX_TEST_CFG), 0x0000000C},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_WRLVL_6_CTL_CFG), 0x0},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_WRLVL_7_CTL_CFG), 0x0},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_WR_0_CTL_CFG), 0x1EF4E138},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_WR_1_CTL_CFG), 0x0A512C4B},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_WR_2_CTL_CFG), 0x0A512C4B},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_WR_3_CTL_CFG), 0x06319C67},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_WR_4_CTL_CFG), 0x04219465},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_WR_5_CTL_CFG), 0x02118C63},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_WR_6_CTL_CFG), 0x02108C23},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_WR_7_CTL_CFG), 0x02118862},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_RD_0_CTL_CFG), 0x1EF66198},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_RD_1_CTL_CFG), 0x0A512C4B},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_RD_2_CTL_CFG), 0x0A512C4B},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_RD_3_CTL_CFG), 0x06319C67},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_RD_4_CTL_CFG), 0x04219465},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_RD_5_CTL_CFG), 0x02118C63},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_RD_6_CTL_CFG), 0x02108C23},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_RD_7_CTL_CFG), 0x02118862},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_RCW_0_CTL_CFG), 0x31866198},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_RCW_1_CTL_CFG), 0x31866198},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_RCW_2_CTL_CFG), 0x31866198},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_RCW_3_CTL_CFG), 0x31866198},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_RCW_4_CTL_CFG), 0x31866198},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_RCW_5_CTL_CFG), 0x31866198},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_RDT2_0_CTL_CFG), 0x31866198},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_RDT2_1_CTL_CFG), 0x16B0DC37},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_RDT2_2_CTL_CFG), 0x16B0DC37},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_RDT2_2_CTL_CFG), 0x0E713C4F},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_RDT2_3_CTL_CFG), 0x0A512C4B},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_RDT2_5_CTL_CFG), 0x06319C67},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_RDT2_6_CTL_CFG), 0x06319866},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CDCEXT_RDT2_7_CTL_CFG), 0x04219465},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_WRLVLEXT_CTL_0_CFG), 0x3020302},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_WRLVLEXT_CTL_1_CFG), 0x3020302},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_WRLVLEXT_CTL_2_CFG), 0x3020302},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_WRLVLEXT_CTL_5_CFG), 0x7060706}, //add full cycle to CLK and CA. 
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_WRLVLEXT_CTL_6_CFG), 0x7060706}, //add full cycle to CLK and CA. 
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_WRLVLEXT_CTL_7_CFG), 0x7060706}, //add full cycle to CLK and CA. 
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_RCWPREAMBEXT_TOP_3_CFG), 0x00001010},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_RCWPREAMBEXT_TOP_4_CFG), 0x00001010},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_RCWPREAMBEXT_TOP_5_CFG), 0x00001010},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_RCWPREAMBEXT_TOP_6_CFG), 0x00001010},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_RCWPREAMBEXT_TOP_7_CFG), 0x00001010},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_UPDATE_INTF_CFG), 0x00000000},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_0_PWRS_1_HI2_CFG), 0x00000410},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_1_PWRS_1_HI2_CFG), 0x00000451},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_2_PWRS_1_HI2_CFG), 0x00000451},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_3_PWRS_1_HI2_CFG), 0x00000492},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_4_PWRS_1_HI2_CFG), 0x000004D3},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_5_PWRS_1_HI2_CFG), 0x0000049C},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_6_PWRS_1_HI2_CFG), 0x0000049C},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_PRFS_7_PWRS_1_HI2_CFG), 0x0000049C},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_DCC_TOP_4_CFG), 0x3FFFFFC},    
   {0x0, 0x0} 
};

uint32 ddr_cc_config[][2] =
{
   {HWIO_ADDRX(0, DDR_CC_DDRCC_PLLCTRL_CLK_SWITCH_CTRL), 0x499E68A4},
   {HWIO_ADDRX(0, DDR_CC_DDRCC_DLLCTRL_CTRL), 0x00090900},
   {HWIO_ADDRX(0, DDR_CC_DDRCC_TXPHYCTRL_CTRL), 0x4518CC64},
   {HWIO_ADDRX(0, DDR_CC_DDRCC_TX0_USER_CTRL), 0x00000000},
   {HWIO_ADDRX(0, DDR_CC_DDRCC_TX1_USER_CTRL), 0x00000000},
   {HWIO_ADDRX(0, DDR_CC_DDRCC_SDPLL0_IE_TRIM), 0x00000004},
   {HWIO_ADDRX(0, DDR_CC_DDRCC_SDPLL0_IP_TRIM), 0x00000004},
   {HWIO_ADDRX(0, DDR_CC_DDRCC_SDPLL0_IPTAT_TRIM), 0x00000007},
   {HWIO_ADDRX(0, DDR_CC_DDRCC_SDPLL0_CLKBUFLR_EN), 0x00000001},
   {HWIO_ADDRX(0, DDR_CC_DDRCC_SDPLL0_RESETSM_CNTRL), 0x00000030},
   {HWIO_ADDRX(0, DDR_CC_DDRCC_SDPLL0_RESETSM_CNTRL2), 0x00000020},
   {HWIO_ADDRX(0, DDR_CC_DDRCC_SDPLL0_KVCO_DIV_REF1), 0x0000005E},
   {HWIO_ADDRX(0, DDR_CC_DDRCC_SDPLL0_VCO_DIV_REF1), 0x0000005E},
   {HWIO_ADDRX(0, DDR_CC_DDRCC_SDPLL0_PLLLOCK_CMP_EN), 0x00000002},
   {HWIO_ADDRX(0, DDR_CC_DDRCC_SDPLL0_PLL_TXCLK_EN), 0x00000001},
   {HWIO_ADDRX(0, DDR_CC_DDRCC_SDPLL0_PLL_CRCTRL), 0x00000012},
   {HWIO_ADDRX(0, DDR_CC_DDRCC_SDPLL0_CP_SET_CUR), 0x00000009},
   {HWIO_ADDRX(0, DDR_CC_DDRCC_SDPLL0_PLL_ICP_SET), 0x00000024},
   {HWIO_ADDRX(0, DDR_CC_DDRCC_SDPLL0_PLL_LPF1), 0x0000001B},
   {HWIO_ADDRX(0, DDR_CC_DDRCC_SDPLL0_PLL_BANDGAP), 0x00000003},
   {HWIO_ADDRX(0, DDR_CC_DDRCC_SDPLL1_IE_TRIM), 0x00000004},
   {HWIO_ADDRX(0, DDR_CC_DDRCC_SDPLL1_IP_TRIM), 0x00000004},
   {HWIO_ADDRX(0, DDR_CC_DDRCC_SDPLL1_IPTAT_TRIM), 0x00000007},
   {HWIO_ADDRX(0, DDR_CC_DDRCC_SDPLL1_CLKBUFLR_EN), 0x00000001},
   {HWIO_ADDRX(0, DDR_CC_DDRCC_SDPLL1_RESETSM_CNTRL), 0x00000030},
   {HWIO_ADDRX(0, DDR_CC_DDRCC_SDPLL1_RESETSM_CNTRL2), 0x00000020},
   {HWIO_ADDRX(0, DDR_CC_DDRCC_SDPLL1_KVCO_DIV_REF1), 0x0000005E},
   {HWIO_ADDRX(0, DDR_CC_DDRCC_SDPLL1_VCO_DIV_REF1), 0x0000005E},
   {HWIO_ADDRX(0, DDR_CC_DDRCC_SDPLL1_PLLLOCK_CMP_EN), 0x00000002},
   {HWIO_ADDRX(0, DDR_CC_DDRCC_SDPLL1_PLL_TXCLK_EN), 0x00000001},
   {HWIO_ADDRX(0, DDR_CC_DDRCC_SDPLL1_PLL_CRCTRL), 0x00000012},
   {HWIO_ADDRX(0, DDR_CC_DDRCC_SDPLL1_CP_SET_CUR), 0x00000009},
   {HWIO_ADDRX(0, DDR_CC_DDRCC_SDPLL1_PLL_ICP_SET), 0x00000024},
   {HWIO_ADDRX(0, DDR_CC_DDRCC_SDPLL1_PLL_LPF1), 0x0000001B},
   {HWIO_ADDRX(0, DDR_CC_DDRCC_SDPLL1_PLL_BANDGAP), 0x00000003},
   {0x0, 0x0} 
};

//---------------------------------------------------------------------------

uint32 ddr_phy_dq_delta[][2] =
{
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_FPM_FEA_WT_DEP_15_CFG), 0x0000374F},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMRX_USER_CFG), 0x09100191},
   {0x0, 0x0} 
};

uint32 ddr_phy_ca_delta[][2] =
{
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMRX_USER_CFG), 0x09100191},
   {0x0, 0x0} 
};

uint32 ddr_cc_delta[][2] =
{
   {HWIO_ADDRX(0, DDR_CC_DDRCC_TOP_CTRL_CFG), 0x00A02400},  //was 0x00A01E00, enable LVDS CG, extend CLK_ENABLE_TIME =9
   {0x0, 0x0} 
};

//---------------------------------------------------------------------------

uint32 ddr_phy_dq_delta_v3[][2] =
{
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMCDCWR_MODE_CFG), 0x31},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMCDCRDT2_I0_MODE_CFG), 0x71},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMCDCWRLVL_MODE_CFG), 0x1},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMSLICE0_PBIT_CTL_DQ_0_RXTX_CFG), 0x0},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMSLICE1_PBIT_CTL_DQ_1_RXTX_CFG), 0x0}, 
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMSLICE2_PBIT_CTL_DQ_2_RXTX_CFG), 0x0}, 
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMSLICE3_PBIT_CTL_DQ_3_RXTX_CFG), 0x0}, 
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMSLICE4_PBIT_CTL_DQ_4_RXTX_CFG), 0x0}, 
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMSLICE5_PBIT_CTL_DQ_5_RXTX_CFG), 0x0}, 
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMSLICE6_PBIT_CTL_DQ_6_RXTX_CFG), 0x0}, 
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMSLICE7_PBIT_CTL_DQ_7_RXTX_CFG), 0x0}, 
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMSLICE8_PBIT_CTL_DQ_8_RXTX_CFG), 0x0}, 
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMSLICE9_PBIT_CTL_DQ_9_RXTX_CFG), 0x0}, 
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMSLICE10_PBIT_CTL_DQS_CFG),      0x0}, 
   {0x0, 0x0} 
};

uint32 ddr_phy_ca_delta_v3[][2] =
{
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMCDCWR_MODE_CFG), 0x31},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMCDCWRLVL_MODE_CFG), 0x1},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMSLICE0_PBIT_CTL_DQ_0_RXTX_CFG), 0x0},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMSLICE1_PBIT_CTL_DQ_1_RXTX_CFG), 0x0}, 
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMSLICE2_PBIT_CTL_DQ_2_RXTX_CFG), 0x0}, 
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMSLICE3_PBIT_CTL_DQ_3_RXTX_CFG), 0x0}, 
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMSLICE4_PBIT_CTL_DQ_4_RXTX_CFG), 0x0}, 
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMSLICE5_PBIT_CTL_DQ_5_RXTX_CFG), 0x0}, 
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMSLICE6_PBIT_CTL_DQ_6_RXTX_CFG), 0x0}, 
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMSLICE7_PBIT_CTL_DQ_7_RXTX_CFG), 0x0}, 
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMSLICE8_PBIT_CTL_DQ_8_RXTX_CFG), 0x0}, 
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMSLICE9_PBIT_CTL_DQ_9_RXTX_CFG), 0x0}, 
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMSLICE10_PBIT_CTL_DQS_CFG),      0x0}, 
   {0x0, 0x0} 
};

uint32 ddr_cc_delta_v3[][2] =
{
   {HWIO_ADDRX(0, DDR_CC_DDRCC_PHYDLL0_ANALOG_CFG0), 0x1},
   {HWIO_ADDRX(0, DDR_CC_DDRCC_PHYDLL1_ANALOG_CFG0), 0x1},
   {0x0, 0x0} 
};

//---------------------------------------------------------------------------

uint32 ddr_phy_dq_delta_v31[][2] =
{
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_PAD_CNTL_3_CFG), 0x026008FE},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_PAD_CNTL_4_CFG), 0x026008FE},
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMCDCWR_MODE_CFG), 0x0}, // disable gearbox CGC
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMCDCRDT2_I0_MODE_CFG), 0x0}, // disable RD FIFO CGC
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_TOP_CTRL_0_CFG), 0x0001C290},  // enable SW override of DLL PHY/MC phase clock CGC
   {0x0, 0x0} 
};

uint32 ddr_phy_ca_delta_v31[][2] =
{
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_CMCDCWR_MODE_CFG), 0x0}, // disable gearbox CGC
   {HWIO_ADDRX(0, DDR_PHY_DDRPHY_TOP_CTRL_0_CFG), 0x0001C290},  // enable SW override of DLL PHY/MC phase clock CGC
   {0x0, 0x0} 
};

uint32 ddr_cc_delta_v31[][2] =
{
   {0x0, 0x0} 
};

//---------------------------------------------------------------------------

/* DDR_PHY base config */
uint32 (*ddr_phy_dq_config_base)[2]       = ddr_phy_dq_config;
uint32 (*ddr_phy_ca_config_base)[2]       = ddr_phy_ca_config;
uint32 (*ddr_cc_config_base)[2]           = ddr_cc_config;

/* DDR_PHY delta config compared to base */
uint32 (*ddr_phy_dq_config_delta)[2]      = ddr_phy_dq_delta;
uint32 (*ddr_phy_ca_config_delta)[2]      = ddr_phy_ca_delta;
uint32 (*ddr_cc_config_delta)[2]          = ddr_cc_delta;

/* DDR_PHY delta config compared to base for V3 */
uint32 (*ddr_phy_dq_config_delta_v3)[2]      = ddr_phy_dq_delta_v3;
uint32 (*ddr_phy_ca_config_delta_v3)[2]      = ddr_phy_ca_delta_v3;
uint32 (*ddr_cc_config_delta_v3)[2]          = ddr_cc_delta_v3;


/* DDR_PHY delta config compared to base for V3.1 */
uint32 (*ddr_phy_dq_config_delta_v31)[2]      = ddr_phy_dq_delta_v31;
uint32 (*ddr_phy_ca_config_delta_v31)[2]      = ddr_phy_ca_delta_v31;
uint32 (*ddr_cc_config_delta_v31)[2]          = ddr_cc_delta_v31;

/*==============================================================================
                                  FUNCTIONS
==============================================================================*/
void ddr_phy_dq_set_config ( DDR_STRUCT *ddr,
                             uint32 offset,
                             uint32 (*ddr_phy_dq_config_base)[2], 
                             uint32 (*ddr_phy_dq_config_delta)[2],
                             uint32 (*ddr_phy_dq_config_delta_v3)[2],
                             uint32 (*ddr_phy_dq_config_delta_v31)[2])
{
    uint32 reg;
    
    /* Populate base config */
    if (ddr_phy_dq_config_base != NULL)
    {
        for (reg = 0; ddr_phy_dq_config_base[reg][0] != 0; reg++)
        {
            out_dword(ddr_phy_dq_config_base[reg][0] + offset, ddr_phy_dq_config_base[reg][1]);
        }
    }
    
    if(((ddr->misc.platform_id == PLATFORM_ID_ISTARI) && (ddr->misc.chip_version >= 0x0200)) || (ddr->misc.platform_id == PLATFORM_ID_RADAGAST))
    {
        /* Populate delta config for V2 */
        if (ddr_phy_dq_config_delta != NULL)
        {
            for (reg = 0; ddr_phy_dq_config_delta[reg][0] != 0; reg++)
            {
                out_dword(ddr_phy_dq_config_delta[reg][0] + offset, ddr_phy_dq_config_delta[reg][1]);
            }
        }
    }

    if(((ddr->misc.platform_id == PLATFORM_ID_ISTARI) && (ddr->misc.chip_version >= 0x0300)) || (ddr->misc.platform_id == PLATFORM_ID_RADAGAST))
    {
        /* Populate delta config for V3 */
        if (ddr_phy_dq_config_delta_v3 != NULL)
        {
            for (reg = 0; ddr_phy_dq_config_delta_v3[reg][0] != 0; reg++)
            {
                out_dword(ddr_phy_dq_config_delta_v3[reg][0] + offset, ddr_phy_dq_config_delta_v3[reg][1]);
            }
        }
    }

    if(((ddr->misc.platform_id == PLATFORM_ID_ISTARI) && (ddr->misc.chip_version >= 0x0301)) || (ddr->misc.platform_id == PLATFORM_ID_RADAGAST))
    {
        /* Populate delta config for V3.1 */
        if (ddr_phy_dq_config_delta_v31 != NULL)
        {
            for (reg = 0; ddr_phy_dq_config_delta_v31[reg][0] != 0; reg++)
            {
                out_dword(ddr_phy_dq_config_delta_v31[reg][0] + offset, ddr_phy_dq_config_delta_v31[reg][1]);
            }
        }
    }
}

void ddr_phy_ca_set_config ( DDR_STRUCT *ddr,
                             uint32 offset,
                             uint32 (*ddr_phy_ca_config_base)[2], 
                             uint32 (*ddr_phy_ca_config_delta)[2],
                             uint32 (*ddr_phy_ca_config_delta_v3)[2],
                             uint32 (*ddr_phy_ca_config_delta_v31)[2])
{
    uint32 reg;

    /* Populate base config */
    if (ddr_phy_ca_config_base != NULL)
    {
        for (reg = 0; ddr_phy_ca_config_base[reg][0] != 0; reg++)
        {
            out_dword(ddr_phy_ca_config_base[reg][0] + offset, ddr_phy_ca_config_base[reg][1]);
        }
    }
    
    if(((ddr->misc.platform_id == PLATFORM_ID_ISTARI) && (ddr->misc.chip_version >= 0x0200)) || (ddr->misc.platform_id == PLATFORM_ID_RADAGAST))
    {
        /* Populate delta config */
        if (ddr_phy_ca_config_delta != NULL)
        {
            for (reg = 0; ddr_phy_ca_config_delta[reg][0] != 0; reg++)
            {
                out_dword(ddr_phy_ca_config_delta[reg][0] + offset, ddr_phy_ca_config_delta[reg][1]);
            }
        }
    }
    
    if(((ddr->misc.platform_id == PLATFORM_ID_ISTARI) && (ddr->misc.chip_version >= 0x0300)) || (ddr->misc.platform_id == PLATFORM_ID_RADAGAST))
    {
        /* Populate delta config for V3*/
        if (ddr_phy_ca_config_delta_v3 != NULL)
        {
            for (reg = 0; ddr_phy_ca_config_delta_v3[reg][0] != 0; reg++)
            {
                out_dword(ddr_phy_ca_config_delta_v3[reg][0] + offset, ddr_phy_ca_config_delta_v3[reg][1]);
            }
        }
    }

    if(((ddr->misc.platform_id == PLATFORM_ID_ISTARI) && (ddr->misc.chip_version >= 0x0301)) || (ddr->misc.platform_id == PLATFORM_ID_RADAGAST))
    {
        /* Populate delta config for V3.1*/
        if (ddr_phy_ca_config_delta_v31 != NULL)
        {
            for (reg = 0; ddr_phy_ca_config_delta_v31[reg][0] != 0; reg++)
            {
                out_dword(ddr_phy_ca_config_delta_v31[reg][0] + offset, ddr_phy_ca_config_delta_v31[reg][1]);
            }
        }
    }
}

void ddr_cc_set_config ( DDR_STRUCT *ddr,
                         uint32 offset,
                         uint32 (*ddr_cc_config_base)[2],  
                         uint32 (*ddr_cc_config_delta)[2],
                         uint32 (*ddr_cc_config_delta_v3)[2],
                         uint32 (*ddr_cc_config_delta_v31)[2])
{
    uint32 reg;

    /* Populate base config */
    if (ddr_cc_config_base != NULL)
    {
        for (reg = 0; ddr_cc_config_base[reg][0] != 0; reg++)
        {
            out_dword(ddr_cc_config_base[reg][0] + offset, ddr_cc_config_base[reg][1]);
        }
    }
    
    if(((ddr->misc.platform_id == PLATFORM_ID_ISTARI) && (ddr->misc.chip_version >= 0x0200)) || (ddr->misc.platform_id == PLATFORM_ID_RADAGAST))
    {
        /* Populate delta config */
        if (ddr_cc_config_delta != NULL)
        {
            for (reg = 0; ddr_cc_config_delta[reg][0] != 0; reg++)
            {
                out_dword(ddr_cc_config_delta[reg][0] + offset, ddr_cc_config_delta[reg][1]);
            }
        }
    }
    
    if(((ddr->misc.platform_id == PLATFORM_ID_ISTARI) && (ddr->misc.chip_version >= 0x0300)) || (ddr->misc.platform_id == PLATFORM_ID_RADAGAST))
    {
        /* Populate delta config for V3 */
        if (ddr_cc_config_delta_v3 != NULL)
        {
            for (reg = 0; ddr_cc_config_delta_v3[reg][0] != 0; reg++)
            {
                out_dword(ddr_cc_config_delta_v3[reg][0] + offset, ddr_cc_config_delta_v3[reg][1]);
            }
        }
    }
    
    if(((ddr->misc.platform_id == PLATFORM_ID_ISTARI) && (ddr->misc.chip_version >= 0x0301)) || (ddr->misc.platform_id == PLATFORM_ID_RADAGAST))
    {
        /* Populate delta config for V3.1 */
        if (ddr_cc_config_delta_v31 != NULL)
        {
            for (reg = 0; ddr_cc_config_delta_v31[reg][0] != 0; reg++)
            {
                out_dword(ddr_cc_config_delta_v31[reg][0] + offset, ddr_cc_config_delta_v31[reg][1]);
            }
        }
    }
}

