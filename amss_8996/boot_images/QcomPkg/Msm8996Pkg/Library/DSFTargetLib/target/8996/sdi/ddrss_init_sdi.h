/****************************************************************************
 QUALCOMM Proprietary Design Data
 Copyright (c) 2014,2016 Qualcomm Technologies Incorporated. All rights reserved.
 ****************************************************************************/
/*==============================================================================
                                EDIT HISTORY

$Header: //components/rel/boot.xf/1.0/QcomPkg/Msm8996Pkg/Library/DSFTargetLib/target/8996/sdi/ddrss_init_sdi.h#18 $
$DateTime: 2016/08/19 00:49:20 $
$Author: pwbldsvc $
================================================================================
when       who          what, where, why
--------   ---          --------------------------------------------------------
05/04/14   arindamm     First edit history header. Add new entries at top.
================================================================================*/

#include "bimc_seq_hwioreg_sdi.h"
#include "ddr_ss_seq_hwioreg_sdi.h"
#include "seq_hwio_sdi.h"

#include "HAL_SNS_DDR.h"

#define TARGET_DDR_SYSTEM_FIRMWARE_VERSION     21916

// Define silicon target or emulation target. Some portions of the DDR System Firmware will or will not
// be compiled depending on what this macro is defined as. 
// If TARGET_SILICON is defined as a 0, it implies an emulation build.
// If TARGET_SILICON is defined as a 1, it implies a real-silicon build.
#define TARGET_SILICON                         1 

// BIMC and PHY Core Architecture, Major and Minor versions.
#define TARGET_BIMC_ARCH_VERSION               2
#define TARGET_BIMC_CORE_MAJOR_VERSION         3
#define TARGET_BIMC_CORE_MINOR_VERSION         2
#define TARGET_PHY_CORE_MAJOR_VERSION          4
#define TARGET_PHY_CORE_MINOR_VERSION          2

#define PLATFORM_ID_ISTARI      0x3000
#define PLATFORM_ID_RADAGAST    0x3004

#define DDR_BASE                        0x80000000
#define DDR_SS_BASE                     0x00480000
#define BIMC_BASE                       0x00400000

#define REG_OFFSET_GLOBAL0              (BIMC_BASE + SEQ_BIMC_GLOBAL0_OFFSET)

#define REG_OFFSET_GLOBAL1              (BIMC_BASE + SEQ_BIMC_GLOBAL1_OFFSET)

#define REG_OFFSET_SCMO(uint8)          ((uint8 == 0) ? \
                                        (BIMC_BASE + SEQ_BIMC_BIMC_S_DDR0_SCMO_OFFSET) : \
                                        (BIMC_BASE + SEQ_BIMC_BIMC_S_DDR1_SCMO_OFFSET))

#define REG_OFFSET_DPE(uint8)           ((uint8 == 0) ? \
                                        (BIMC_BASE + SEQ_BIMC_BIMC_S_DDR0_DPE_OFFSET)  : \
                                        (BIMC_BASE + SEQ_BIMC_BIMC_S_DDR1_DPE_OFFSET))

#define REG_OFFSET_SHKE(uint8)          ((uint8 == 0) ? \
                                        (BIMC_BASE + SEQ_BIMC_BIMC_S_DDR0_SHKE_OFFSET) : \
                                        (BIMC_BASE + SEQ_BIMC_BIMC_S_DDR1_SHKE_OFFSET))

#define REG_OFFSET_DTTS(uint8)          ((uint8 == 0) ? \
                                        (BIMC_BASE + SEQ_BIMC_BIMC_S_DDR0_DTTS_CFG_OFFSET) : \
                                        (BIMC_BASE + SEQ_BIMC_BIMC_S_DDR1_DTTS_CFG_OFFSET))


#define CH_1HOT(uint8)                  ((uint8 == 0) ? \
                                        DDR_CH0 : \
                                        DDR_CH1)

#define CS_1HOT(uint8)                  ((uint8 == 0) ? \
                                        DDR_CS0 : \
                                        DDR_CS1)

#define CH_INX(DDR_CHANNEL)             ((DDR_CHANNEL == DDR_CH0) ? \
                                        0: \
                                        1)

#define CS_INX(DDR_CHIPSELECT)          ((DDR_CHIPSELECT == DDR_CS0) ? \
                                        0 : \
                                        1)


#define     XO_PERIOD_IN_PS 52080   //52.08ns 

#define     BIMC_CH_OFFSET  (SEQ_BIMC_BIMC_S_DDR1_OFFSET-SEQ_BIMC_BIMC_S_DDR0_OFFSET)



#define CONVERT_CYC_TO_PS        1000000000

#define DDR_PHY_OFFSET           0x800     // DDR PHY Address offset (2k Bytes)
#define CA0_DDR_PHY_OFFSET       0x0000
#define CA1_DDR_PHY_OFFSET       0x0800
#define DQ0_DDR_PHY_OFFSET       0x1000
#define DQ1_DDR_PHY_OFFSET       0x1800
#define DQ2_DDR_PHY_OFFSET       0x2000
#define DQ3_DDR_PHY_OFFSET       0x2800
#define DDR_CC_OFFSET            0x3000


#define REG_OFFSET_DDR_PHY_CH(ch) ((ch == 0)  ? \
                                  DDR_SS_BASE : \
                                  DDR_SS_BASE + 0x3800)

#define BROADCAST_BASE (DDR_SS_BASE + SEQ_DDR_SS_DDRSS_AHB2PHY_BROADCAST_SWMAN1_OFFSET)



typedef enum
{
    WAIT_TIMER_CLOCK  = 0x0 ,
    WAIT_XO_CLOCK     = 0x1
} BIMC_Wait_Timer_Domain;

typedef enum
{
    RL_DBI_OFF = 0x0 ,
    RL_DBI_ON  = 0x1 ,
    WL         = 0x2 ,
    MR2_WR_VAL = 0x3
} RL_WL_Table_Sel;



// #defines for all valid MRs
#define JEDEC_MR_0   0x0
#define JEDEC_MR_1   0x1
#define JEDEC_MR_2   0x2
#define JEDEC_MR_3   0x3
#define JEDEC_MR_4   0x4
#define JEDEC_MR_5   0x5
#define JEDEC_MR_6   0x6
#define JEDEC_MR_7   0x7
#define JEDEC_MR_8   0x8
#define JEDEC_MR_9   0x9
#define JEDEC_MR_11  0xB
#define JEDEC_MR_12  0xC
#define JEDEC_MR_13  0xD
#define JEDEC_MR_14  0xE
#define JEDEC_MR_15  0xF
#define JEDEC_MR_16  0x10
#define JEDEC_MR_17  0x11
#define JEDEC_MR_18  0x12
#define JEDEC_MR_19  0x13
#define JEDEC_MR_20  0x14
#define JEDEC_MR_22  0x16
#define JEDEC_MR_23  0x17
#define JEDEC_MR_24  0x18
#define JEDEC_MR_32  0x20
#define JEDEC_MR_40  0x28



// The addresses of the MPM low power control registers
#define REG_ADDR__MPM2_MPM_LOW_POWER_CFG 0x004A0070
#define REG_ADDR__MPM2_MPM_DDR_PHY_FREEZEIO_EBI1 0x004AB004





// SDI is based on code from the following DSF files:

// This file (ddrss_init_sdi.h) is based on data from the following DSF files:
// target_config.h
// bimc.h
// ddrss.h


// Data structures that are a specially generated subset of bimc_config.c
// bimc_global0_config[][]
// bimc_scmo_config[][]
// bimc_dpe_config[][]
// bimc_shke_config[][]

// Data structures that are a specially generated subset of ddr_phy_config.c
// ddr_phy_dq_config[][]
// ddr_phy_ca_config[][]
// ddr_cc_config[][]

// From bimc.c
void BIMC_Exit_Self_Refresh_sdi (DDR_STRUCT *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select);  
void BIMC_Enter_Self_Refresh_sdi (DDR_STRUCT *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select);  
void BIMC_ZQ_Calibration_sdi (DDR_STRUCT  *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select); 

// From bimc_common.c
void BIMC_All_Periodic_Ctrl_sdi (DDR_STRUCT *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select, uint8 enable); 
void BIMC_HW_Self_Refresh_Ctrl_sdi (DDR_STRUCT *ddr, uint8 ch, uint8 cs, uint8 enable);  
uint32 BIMC_MR_Read_sdi (DDR_CHANNEL channel, DDR_CHIPSELECT chip_select, uint32 MR_addr);  
void BIMC_MR_Write_sdi (DDR_CHANNEL channel, DDR_CHIPSELECT chip_select, uint32 MR_addr, uint32 MR_data); 
void BIMC_Wait_Timer_Setup_sdi (DDR_CHANNEL channel, BIMC_Wait_Timer_Domain one_xo_zero_timer_clk, uint32 timer_value); 

// Data structures from bimc_data_lpddr4.c
// lpddr_geometry_table_sdi[][]
// lpddr_size_table_sdi[][]
// RL_WL_lpddr_sdi[][]
// RL_WL_lpddr_size_sdi
// RL_WL_freq_range_table_sdi
// RL_WL_freq_range_table_size_sdi
// interface_width_sdi[]

// Code from bimc_data.c
uint8 BIMC_RL_WL_Freq_Index_sdi (DDR_STRUCT *ddr, uint32 clk_freq_khz);
uint8 BIMC_RL_WL_Table_Sel_sdi (DDR_STRUCT *ddr, RL_WL_Table_Sel table_sel, uint32 clk_freq_khz);

// Code from bimc_init.c
void BIMC_Config_sdi(DDR_STRUCT *ddr);
void BIMC_DDR_Addr_Setup_Warm_sdi (DDR_STRUCT *ddr, uint8 ch, uint8 cs);
DDR_CHIPSELECT BIMC_DDR_Access_Enable_sdi(DDR_STRUCT *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select);
void BIMC_Memory_Device_Init_sdi (DDR_STRUCT *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select, uint32 clk_freq_khz);
void BIMC_Post_Init_Setup_Warm_sdi (DDR_STRUCT *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select);
void BIMC_Pre_Init_Setup_sdi (DDR_STRUCT *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select, uint32 clk_freq_khz);

// Code from bimc_lpddr4.c
void BIMC_Program_Lpddr_AC_Parameters_sdi(DDR_STRUCT *ddr, DDR_CHANNEL channel);
void BIMC_ZQ_Calibration_Lpddr_sdi (DDR_STRUCT  *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select);
void BIMC_Memory_Device_Init_Lpddr_sdi (DDR_STRUCT *ddr, DDR_CHANNEL channel , DDR_CHIPSELECT chip_select);


// Code from ddrss.c
boolean HAL_DDR_Init_sdi (DDR_STRUCT *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select, uint32 clk_freq_khz);

// Code from ddrss_common.c
void DDRSS_ddr_phy_sw_freq_switch_sdi (DDR_STRUCT *ddr, uint32 clk_freq_khz, uint8 ch);

// Code from ddrss_phy_cc_init.c
void DDR_PHY_CC_Config_sdi(DDR_STRUCT *ddr);
void DDR_PHY_CC_init_sdi (DDR_STRUCT *ddr, DDR_CHANNEL channel, uint32 clk_freq_khz);


// Code from ddr_phy_hal_cfg_init.c
void DDR_PHY_hal_cfg_sw_handshake_complete_sdi( uint32 _inst_ );
void DDR_PHY_hal_cfg_sw_handshake_start_sdi( DDR_STRUCT *ddr, uint32 _inst_, uint32 clk_freq_khz );
void DDR_PHY_hal_cfg_sw_handshake_stop_sdi( DDR_STRUCT *ddr, uint32 _inst_, uint32 clk_freq_khz );
void DDR_PHY_hal_cfg_sw_iocal_sdi( uint32 _inst_, uint8 master_phy );


// Code from target_config.c
uint32 DDRSS_Pre_Init_sdi(DDR_STRUCT *ddr);


// Similar to bimc_config.c or ddr_phy_config.c
void Set_config_sdi (uint32 offset, uint32 config_base[][2] );
