/****************************************************************************
 QUALCOMM Proprietary Design Data
 Copyright (c) 2014, Qualcomm Technologies Incorporated. All rights reserved.
 ****************************************************************************/
/*==============================================================================
                                EDIT HISTORY

$Header: //components/rel/boot.xf/1.0/QcomPkg/Msm8996Pkg/Library/DSFTargetLib/ddrss/bimc/mc230/src/bimc_lpddr4.c#4 $
$DateTime: 2015/10/15 15:13:45 $
$Author: pwbldsvc $
================================================================================
when       who          what, where, why
--------   ---          --------------------------------------------------------
05/04/14   arindamm     First edit history header. Add new entries at top.
================================================================================*/

#include "bimc.h"
#include "ddr_ss_seq_hwioreg.h"

//================================================================================================//
// Programs the AC Parameters for LPDDR4, during BIMC Pre Init
//================================================================================================//
void BIMC_Program_Lpddr_AC_Parameters(DDR_STRUCT *ddr, DDR_CHANNEL channel)
{
   uint32 reg_offset_dpe  = 0;
   reg_offset_dpe  = REG_OFFSET_DPE(channel);
 
   HWIO_OUTXF  (reg_offset_dpe, DPE_DRAM_TIMING_24, TCCDMW, ddr->cdt_params[channel].lpddr.tCCDMW);
   HWIO_OUTXF  (reg_offset_dpe, DPE_DRAM_TIMING_25, TCKEHCMD, ddr->cdt_params[channel].lpddr.tCKEHCMD);
   HWIO_OUTXF2 (reg_offset_dpe, DPE_DRAM_TIMING_29, TDQS2DQMIN, TDQS2DQMAX,
                 ddr->cdt_params[channel].lpddr.tDQS2DQMIN, ddr->cdt_params[channel].lpddr.tDQS2DQMAX);
   HWIO_OUTXF  (reg_offset_dpe, DPE_DRAM_TIMING_30, TZQL, ddr->cdt_params[channel].lpddr.tZQLAT);
   HWIO_OUTXF2 (reg_offset_dpe, DPE_DRAM_TIMING_31, TDQSCKMIN, TDQSCKMAX,
                 ddr->cdt_params[channel].lpddr.tDQSCK_min, ddr->cdt_params[channel].lpddr.tDQSCK_max);
   HWIO_OUTXF  (reg_offset_dpe, DPE_DRAM_TIMING_34, TFC, ddr->cdt_params[channel].lpddr.tFC);
   HWIO_OUTXF2 (reg_offset_dpe, DPE_DRAM_TIMING_LP4_ODT_ON_CNTL, LP4_ODT_ONMAX, LP4_ODT_ONMIN,
                 ddr->cdt_params[channel].lpddr.tODTonmax, ddr->cdt_params[channel].lpddr.tODTonmin);
   HWIO_OUTXF2 (reg_offset_dpe, DPE_DRAM_TIMING_LP4_ODT_OFF_CNTL, LP4_ODT_OFFMAX, LP4_ODT_OFFMIN,
                 ddr->cdt_params[channel].lpddr.tODToffmax, ddr->cdt_params[channel].lpddr.tODToffmin);
}

//================================================================================================//
// Manual ZQ calibration for LPDDR4
// This function supports ZQ Cal per rank and both ranks. While using for both ranks, ZQCAL_START
// is issued for one rank followed by next rank. ZQCAL_LATCH for both ranks are issued together.
//================================================================================================//
void BIMC_ZQ_Calibration_Lpddr (DDR_STRUCT  *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select)
{
   uint8  ch = 0;
   uint8  cs = 0;
   uint8  ch_1hot = 0;
   uint32 reg_offset_shke = 0;

   for (ch = 0; ch < 2; ch++)
   {
      ch_1hot = CH_1HOT(ch);
      reg_offset_shke = REG_OFFSET_SHKE(ch);

      if ((channel >> ch) & 0x1)
      {
         // tZQCAL has 100ps resolution, first multiply by 100 to convert resolution to 1ps,
         // then convert to XO_PERIOD. Rounding up using div_ceil function which rounds up
         // any decimal value to the next integer.
         BIMC_Wait_Timer_Setup ((DDR_CHANNEL)ch_1hot, WAIT_XO_CLOCK,
                                  (div_ceil((ddr->cdt_params[ch].lpddr.tZQCAL * 100), XO_PERIOD_IN_PS)));

         for (cs = 0; cs < 2; cs++)
         {
            if ((chip_select >> cs) & 0x1)
            {
            HWIO_OUTXF2 (reg_offset_shke, SHKE_DRAM_MANUAL_0, RANK_SEL, ZQCAL_START, CS_1HOT(cs), 1);
            while (HWIO_INXF (reg_offset_shke, SHKE_DRAM_MANUAL_0, ZQCAL_START));
            }
         }

         // ZQCAL_LATCH doesnot need wait timer. tZQLAT timing is already taken care while
         // executing ZQCAL_LATCH. Hence set the wait timer to 0.
         BIMC_Wait_Timer_Setup ((DDR_CHANNEL)ch_1hot, WAIT_XO_CLOCK, 0);

         HWIO_OUTXF2 (reg_offset_shke, SHKE_DRAM_MANUAL_0, RANK_SEL, ZQCAL_LATCH, chip_select, 1);
         while (HWIO_INXF (reg_offset_shke, SHKE_DRAM_MANUAL_0, ZQCAL_LATCH));
      }
   }
}

//================================================================================================//
// LPDDR4 Device Initialization
// Does device initialization steps of enabling CK and CKE sequentially
//================================================================================================//
void BIMC_Memory_Device_Init_Lpddr (DDR_STRUCT *ddr, EXTENDED_CDT_STRUCT *ecdt, DDR_CHANNEL channel , DDR_CHIPSELECT chip_select)
{
   uint8  ch                     = 0;
   uint8  ch_1hot                = 0;
   uint8  soc_odt_override_value = 0;
   uint32 reg_offset_shke        = 0;

   for (ch = 0; ch < 2; ch++)
   {
      ch_1hot = CH_1HOT(ch);
      reg_offset_shke = REG_OFFSET_SHKE(ch);

      if ((channel >> ch) & 0x1)
      {
         // After RESET_n is deasserted, wait at least tINIT3 before activating CKE.
         // BIMC keeps CKE off at power up. Set CKE off again to trigger DPE timer to satisfy tINIT3
         // Convert tINIT3=2ms to Timer clock cycle (32KHz): 2ms/ 0.03125ms = 64 = 0x40
         BIMC_Wait_Timer_Setup ((DDR_CHANNEL)ch_1hot, WAIT_TIMER_CLOCK, 0x40);

         HWIO_OUTXF2 (reg_offset_shke, SHKE_DRAM_MANUAL_0, RANK_SEL, CKE_OFF, chip_select, 1);
         while (HWIO_INXF (reg_offset_shke, SHKE_DRAM_MANUAL_0, CKE_OFF));

         //Turn CK on and wait tINIT4=5tck
         BIMC_Wait_Timer_Setup ((DDR_CHANNEL)ch_1hot, WAIT_XO_CLOCK, 0x05);

         HWIO_OUTXF2 (reg_offset_shke, SHKE_DRAM_MANUAL_0, RANK_SEL, CK_ON, chip_select, 1);
         while (HWIO_INXF (reg_offset_shke, SHKE_DRAM_MANUAL_0, CK_ON));

         // Turn CKE on and then wait tINIT5
         // Convert tINIT5=2us to XO clock cycle (0.052us): 2us/0.052us = 39 (after roundup) = 0x27
         BIMC_Wait_Timer_Setup ((DDR_CHANNEL)ch_1hot, WAIT_XO_CLOCK, 0x27);

         HWIO_OUTXF2 (reg_offset_shke, SHKE_DRAM_MANUAL_0, RANK_SEL, CKE_ON, chip_select, 1);
         while (HWIO_INXF (reg_offset_shke, SHKE_DRAM_MANUAL_0, CKE_ON));

         // Reset manual_1 timer
         BIMC_Wait_Timer_Setup ((DDR_CHANNEL)ch_1hot, WAIT_XO_CLOCK, 0x00);
      }
   }

#if TARGET_SILICON    
    // Enable DBI-WR, DBI-RD/PDDS/PU-CAL/WR_PST are set to default
    //FSP = Settings
    BIMC_MR_Write (channel, chip_select, JEDEC_MR_3, 0xB0); //10  < F <= 400MHz, set MR3[0],PU-CAL=VDDQ/2.5 for fsp0, it was 0xB1(VDDQ/3)
    BIMC_MR_Write (channel, chip_select, JEDEC_MR_22, 0x0); //set SOC_ODT to disable for fsp0. it was SOC_ODT=40ohm for FSP=0    
   if(ecdt != NULL)
   {
        if(ecdt->extended_cdt_ecdt.dram_soc_odt[0].apply_override == 1)
        {
            soc_odt_override_value = ((0x0 & 0xF8) | ecdt->extended_cdt_ecdt.dram_soc_odt[0].dram_soc_odt);
            BIMC_MR_Write (channel, chip_select, JEDEC_MR_22, soc_odt_override_value);
        }
   }
    // Enable 16 or 32 sequential (on-the-fly) burst length
    BIMC_MR_Write (channel, chip_select, JEDEC_MR_1, 0x06);
    
    // Set FSP-WR=1 to write MR11, 12 and 14 values
    BIMC_MR_Write (channel, chip_select, JEDEC_MR_13, 0x40);
    // Set CA-Vref
    BIMC_MR_Write (channel, chip_select, JEDEC_MR_12, 0x18);
    // Set DQ-Vref
    BIMC_MR_Write (channel, chip_select, JEDEC_MR_14, 0x18);
    
    // Set ODT
    BIMC_MR_Write (channel, chip_select, JEDEC_MR_11, 0x33);  // was 0x34,, DQ ODT 60Ohm. Level 2: both CK and DQ ODT =80ohm.  
    BIMC_MR_Write (channel, chip_select, JEDEC_MR_22, 0x23);  //remove CK workaround, keep CA ODT disabled for V2, SOC_ODT value = 80ohm for V2.
   if(ecdt != NULL)
   {
        if(ecdt->extended_cdt_ecdt.dram_soc_odt[1].apply_override == 1)
        {
            soc_odt_override_value = ((0x23 & 0xF8) | ecdt->extended_cdt_ecdt.dram_soc_odt[1].dram_soc_odt);
            BIMC_MR_Write (channel, chip_select, JEDEC_MR_22, soc_odt_override_value);
        }    
   }
#else
   // Use these values to initialize the DDR mode regs for emulation    

   // Comment out the below lines for bringup. DBI-WR will be enabled post
   // bringup. Uncomment it then. Also needs bimc_config.c update to enable DBI-WR
   //// Enable DBI-WR, DBI-RD/PDDS/PU-CAL/WR_PST are set to default
   BIMC_MR_Write (channel, chip_select, JEDEC_MR_3, 0xB1);
   BIMC_MR_Write (channel, chip_select, JEDEC_MR_22, 0x6); //SOC_ODT=40ohm for FSP=0

   // Enable 16 or 32 sequential (on-the-fly) burst length
   BIMC_MR_Write (channel, chip_select, JEDEC_MR_1, 0x02);

   // Set FSP-WR=1 to write MR11, 12 and 14 values
   BIMC_MR_Write (channel, chip_select, JEDEC_MR_13, 0x40);
   // Set ODT
   BIMC_MR_Write (channel, chip_select, JEDEC_MR_11, 0x23);
   // Set CA-Vref
   BIMC_MR_Write (channel, chip_select, JEDEC_MR_12, 0x18);
   // Set DQ-Vref
   BIMC_MR_Write (channel, chip_select, JEDEC_MR_14, 0x18);

   // Force CS0 ODT on when rank0 is in self refresh while the other rank is active
   BIMC_MR_Write (channel, DDR_CS0, JEDEC_MR_22, 0x2E/*0x28*//*0x2B*/);  //set SoC ODT = 80ohm for FSP=1
   // set SoC ODT = 80ohm for rank1
   BIMC_MR_Write (channel, DDR_CS1, JEDEC_MR_22, 0x6 /*0x00*//*0x3*/);  //set SoC ODT = 80ohm for FSP=1   
#endif // TARGET_SILICON   
    
    // Reset FSP-WR=0
    BIMC_MR_Write (channel, chip_select, JEDEC_MR_13, 0x00);
}

//================================================================================================//
// Enter Deep Power Down
//================================================================================================//
boolean BIMC_Enter_Deep_Power_Down (DDR_STRUCT *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select)
{
    return FALSE;
}

//================================================================================================//
// Exit Deep Power Down
//================================================================================================//
boolean BIMC_Exit_Deep_Power_Down (DDR_STRUCT *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select)
{
    return FALSE;
}

//================================================================================================//
// Power collapse pre-exit sequence 
//================================================================================================//
void BIMC_Pre_Exit_Power_Collapse (DDR_STRUCT *ddr, DDR_CHANNEL channel)
{
  // Device reset register in DDRSS gets reset during power collapse. Hence set this register
   // to drive reset deasserted to the device before unfreezing the IOs.
   HWIO_OUTX (DDR_SS_BASE + SEQ_DDR_SS_DDR_REG_DDR_SS_REGS_OFFSET, DDR_SS_REGS_RESET_CMD, 1);
}

