/****************************************************************************
 QUALCOMM Proprietary Design Data
 Copyright (c) 2014, Qualcomm Technologies Incorporated. All rights reserved.
 ****************************************************************************/
/*==============================================================================
                                EDIT HISTORY

$Header: //components/rel/boot.xf/1.0/QcomPkg/Msm8996Pkg/Library/DSFTargetLib/ddrss/bimc/mc230/header/bimc_data.h#3 $
$DateTime: 2015/10/15 15:13:45 $
$Author: pwbldsvc $
================================================================================
when       who          what, where, why
--------   ---          --------------------------------------------------------
05/04/14   arindamm     First edit history header. Add new entries at top.
================================================================================*/

#ifndef __BIMC_DATA_H__
#define __BIMC_DATA_H__


// lpddrx_geometry_table[][2]:
//  Rows:       DRAM device density
//  Columns:    [Number of rows, Number of columns] for the DRAM density.
extern uint8 lpddr_geometry_table[][2];
extern uint16 lpddr_size_table[][2];
extern uint16 lpddr_timing_table[][2];

// MR23 data
extern uint8 MR23_table[];
  
// ODTLon[][2]:
//  Rows:       Frequency band such as 533 < freq <= 800MHz.
//  Columns:    [WPRE_1, WPRE_2] for the frequency band.
extern uint8 ODTLon[][2];


extern uint32 dpe_config_8[];
extern uint32 dpe_timer_3[];
extern uint32 dpe_timing_13[];

// MR13_wrdata[][2]:
//  Rows:       FSP Set
//  Columns:    [MR13_1, MR13_2] to be applied for the FSP set.
extern uint8 MR13_wrdata[][2];

extern struct ecdt_bimc_freq_switch_params_runtime_struct    bimc_freq_switch_params_struct[];
extern struct ecdt_dram_latency_runtime_struct               RL_WL_lpddr_struct[];

extern uint32 interface_width[];
#endif
