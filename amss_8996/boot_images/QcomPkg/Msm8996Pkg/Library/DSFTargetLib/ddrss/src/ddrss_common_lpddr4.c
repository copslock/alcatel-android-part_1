/****************************************************************************
 QUALCOMM Proprietary Design Data
 Copyright (c) 2014, Qualcomm Technologies Incorporated. All rights reserved.
 ****************************************************************************/
/*==============================================================================
                                EDIT HISTORY

$Header: //components/rel/boot.xf/1.0/QcomPkg/Msm8996Pkg/Library/DSFTargetLib/ddrss/src/ddrss_common_lpddr4.c#9 $
$DateTime: 2016/06/07 05:16:01 $
$Author: pwbldsvc $
================================================================================
when       who          what, where, why
--------   ---          --------------------------------------------------------
05/11/14   arindamm     First edit history header. Add new entries at top.
================================================================================*/

#include "ddrss.h"
#include "target_config.h"
#include <string.h>

extern uint8 dq_dbi_bit;

// =============================================================================
// Bit-byte remapping per the bump map. 
// =============================================================================
extern uint8 byte_remapping_table[NUM_CH][NUM_DQ_PCH];
extern uint8 bit_remapping_phy2bimc_DQ_Odd [8];
extern uint8 bit_remapping_phy2bimc_DQ_Even[8];
extern uint8 bit_remapping_bimc2phy[8];
       
extern uint8 connected_bit_mapping_no_DBI_A [PINS_PER_PHY_CONNECTED_NO_DBI];
extern uint8 connected_bit_mapping_with_DBI_A [PINS_PER_PHY_CONNECTED_WITH_DBI];
extern uint8 connected_bit_mapping_CA [PINS_PER_PHY_CONNECTED_CA];
extern uint8 connected_bit_mapping_CA_PHY [PINS_PER_PHY_CONNECTED_CA];


uint32 freq_range[6] = {F_RANGE_1, F_RANGE_2, F_RANGE_3, F_RANGE_4, F_RANGE_5, F_RANGE_6}; 

uint8  fail_flag_per_byte[4] = {0};
uint8  fail_flag_per_bit[40] = {0};

uint8  ca_training_pattern_lpddr4[CA_PATTERN_NUM][3] = {
  { 0x00, 0x3F, 0x00 },
  { 0x3F, 0x00, 0x3F },
  { 0x15, 0x2a, 0x15 },
  { 0x2a, 0x15, 0x2a }};

uint16 ddr_phy_ca_pcc_link_list[NUM_PHY_PCC_LL] =  
{// Descriptor count = 61, Memory count = 447
0x85df, // Sequence Base Address = 0x5df
0x0286, // Sequence descriptor: save = 2, skip = 6
0x0601, // Sequence Offset : repeat = 6
0x8011, // base descriptor: addr   = 0x11 save count = 1
0x0031, // addr descriptor: offset = 0x31 save count = 1
0x0133, // addr descriptor: offset = 0x33 save count = 2
0x0037, // addr descriptor: offset = 0x37 save count = 1
0x003b, // addr descriptor: offset = 0x3b save count = 1
0x003d, // addr descriptor: offset = 0x3d save count = 1
0x003f, // addr descriptor: offset = 0x3f save count = 1
0x0041, // addr descriptor: offset = 0x41 save count = 1
0x004b, // addr descriptor: offset = 0x4b save count = 1
0x004f, // addr descriptor: offset = 0x4f save count = 1
0x005b, // addr descriptor: offset = 0x5b save count = 1
0x015d, // addr descriptor: offset = 0x5d save count = 2
0x8092, // base descriptor: addr   = 0x92 save count = 1
0x0001, // addr descriptor: offset = 0x1 save count = 1
0x0108, // addr descriptor: offset = 0x8 save count = 2
0x001e, // addr descriptor: offset = 0x1e save count = 1
0x0026, // addr descriptor: offset = 0x26 save count = 1
0x0229, // addr descriptor: offset = 0x29 save count = 3
0x032e, // addr descriptor: offset = 0x2e save count = 4
0x0052, // addr descriptor: offset = 0x52 save count = 1
0x0058, // addr descriptor: offset = 0x58 save count = 1
0x0072, // addr descriptor: offset = 0x72 save count = 1
0x0178, // addr descriptor: offset = 0x78 save count = 2
0x812a, // base descriptor: addr   = 0x12a save count = 1
0x0020, // addr descriptor: offset = 0x20 save count = 1
0x0039, // addr descriptor: offset = 0x39 save count = 1
0x0242, // addr descriptor: offset = 0x42 save count = 3
0x0f4e, // addr descriptor: offset = 0x4e save count = 16
0x1d62, // addr descriptor: offset = 0x62 save count = 30
0x81aa, // base descriptor: addr   = 0x1aa save count = 1
0x2800, // addr descriptor: offset = 0x0 save count = 41
0x0046, // addr descriptor: offset = 0x46 save count = 1
0x0157, // addr descriptor: offset = 0x57 save count = 2
0x015f, // addr descriptor: offset = 0x5f save count = 2
0x824c, // base descriptor: addr   = 0x24c save count = 1
0x0201, // addr descriptor: offset = 0x1 save count = 3
0x1714, // addr descriptor: offset = 0x14 save count = 24
0x8370, // base descriptor: addr   = 0x370 save count = 1
0x0601, // addr descriptor: offset = 0x1 save count = 7
0x0b0c, // addr descriptor: offset = 0xc save count = 12
0x0720, // addr descriptor: offset = 0x20 save count = 8
0x0734, // addr descriptor: offset = 0x34 save count = 8
0x0744, // addr descriptor: offset = 0x44 save count = 8
0x0f54, // addr descriptor: offset = 0x54 save count = 16
0x0070, // addr descriptor: offset = 0x70 save count = 1
0x0074, // addr descriptor: offset = 0x74 save count = 1
0x0276, // addr descriptor: offset = 0x76 save count = 3
0x8444, // base descriptor: addr   = 0x444 save count = 1
0x0601, // addr descriptor: offset = 0x1 save count = 7
0x0714, // addr descriptor: offset = 0x14 save count = 8
0x3328, // addr descriptor: offset = 0x28 save count = 52
0x1f60, // addr descriptor: offset = 0x60 save count = 32
0x84e4, // base descriptor: addr   = 0x4e4 save count = 1
0x5e01, // addr descriptor: offset = 0x1 save count = 95
0x85d1, // base descriptor: addr   = 0x5d1 save count = 1
0x0147, // addr descriptor: offset = 0x47 save count = 2
0x074b, // addr descriptor: offset = 0x4b save count = 8
0x0063, // addr descriptor: offset = 0x63 save count = 1
0x0000
};

uint16 ddr_phy_dq_pcc_link_list[NUM_PHY_PCC_LL] =  
{// Descriptor count = 61, Memory count = 447
0x85df, // Sequence Base Address = 0x5df
0x0286, // Sequence descriptor: save = 2, skip = 6
0x0601, // Sequence Offset : repeat = 6
0x8011, // base descriptor: addr   = 0x11 save count = 1
0x0130, // addr descriptor: offset = 0x30 save count = 2
0x0133, // addr descriptor: offset = 0x33 save count = 2
0x0037, // addr descriptor: offset = 0x37 save count = 1
0x073b, // addr descriptor: offset = 0x3b save count = 8
0x005b, // addr descriptor: offset = 0x5b save count = 1
0x015d, // addr descriptor: offset = 0x5d save count = 2
0x8092, // base descriptor: addr   = 0x92 save count = 1
0x0001, // addr descriptor: offset = 0x1 save count = 1
0x0108, // addr descriptor: offset = 0x8 save count = 2
0x001e, // addr descriptor: offset = 0x1e save count = 1
0x0026, // addr descriptor: offset = 0x26 save count = 1
0x0229, // addr descriptor: offset = 0x29 save count = 3
0x032e, // addr descriptor: offset = 0x2e save count = 4
0x0052, // addr descriptor: offset = 0x52 save count = 1
0x0058, // addr descriptor: offset = 0x58 save count = 1
0x0072, // addr descriptor: offset = 0x72 save count = 1
0x0178, // addr descriptor: offset = 0x78 save count = 2
0x8129, // base descriptor: addr   = 0x129 save count = 1
0x0001, // addr descriptor: offset = 0x1 save count = 1
0x001b, // addr descriptor: offset = 0x1b save count = 1
0x0021, // addr descriptor: offset = 0x21 save count = 1
0x003a, // addr descriptor: offset = 0x3a save count = 1
0x0243, // addr descriptor: offset = 0x43 save count = 3
0x0f4f, // addr descriptor: offset = 0x4f save count = 16
0x1c63, // addr descriptor: offset = 0x63 save count = 29
0x81a9, // base descriptor: addr   = 0x1a9 save count = 1
0x2900, // addr descriptor: offset = 0x0 save count = 42
0x0047, // addr descriptor: offset = 0x47 save count = 1
0x0257, // addr descriptor: offset = 0x57 save count = 3
0x025b, // addr descriptor: offset = 0x5b save count = 3
0x025f, // addr descriptor: offset = 0x5f save count = 3
0x824c, // base descriptor: addr   = 0x24c save count = 1
0x0201, // addr descriptor: offset = 0x1 save count = 3
0x0f14, // addr descriptor: offset = 0x14 save count = 16
0x0027, // addr descriptor: offset = 0x27 save count = 1
0x8370, // base descriptor: addr   = 0x370 save count = 1
0x0601, // addr descriptor: offset = 0x1 save count = 7
0x000e, // addr descriptor: offset = 0xe save count = 1
0x0710, // addr descriptor: offset = 0x10 save count = 8
0x0720, // addr descriptor: offset = 0x20 save count = 8
0x0734, // addr descriptor: offset = 0x34 save count = 8
0x0744, // addr descriptor: offset = 0x44 save count = 8
0x0f54, // addr descriptor: offset = 0x54 save count = 16
0x0070, // addr descriptor: offset = 0x70 save count = 1
0x0074, // addr descriptor: offset = 0x74 save count = 1
0x0276, // addr descriptor: offset = 0x76 save count = 3
0x8444, // base descriptor: addr   = 0x444 save count = 1
0x0601, // addr descriptor: offset = 0x1 save count = 7
0x0714, // addr descriptor: offset = 0x14 save count = 8
0x3328, // addr descriptor: offset = 0x28 save count = 52
0x1f60, // addr descriptor: offset = 0x60 save count = 32
0x84e4, // base descriptor: addr   = 0x4e4 save count = 1
0x5e01, // addr descriptor: offset = 0x1 save count = 95
0x85d1, // base descriptor: addr   = 0x5d1 save count = 1
0x0147, // addr descriptor: offset = 0x47 save count = 2
0x074b, // addr descriptor: offset = 0x4b save count = 8
0x0063, // addr descriptor: offset = 0x63 save count = 1
0x0000
};

uint16 ddr_cc_pcc_link_list[NUM_CC_PCC_LL] =  
{// Descriptor count = 49, Memory count = 83
0x83ef, // Sequence Base Address = 0x3ef
0x0183, // Sequence descriptor: save = 1, skip = 3
0x0601, // Sequence Offset : repeat = 6
0x856f, // Sequence Base Address = 0x56f
0x0183, // Sequence descriptor: save = 1, skip = 3
0x0601, // Sequence Offset : repeat = 6
0x832b, // Sequence Base Address = 0x32b
0x0183, // Sequence descriptor: save = 1, skip = 3
0x0501, // Sequence Offset : repeat = 5
0x84ab, // Sequence Base Address = 0x4ab
0x0183, // Sequence descriptor: save = 1, skip = 3
0x0501, // Sequence Offset : repeat = 5
0x8010, // base descriptor: addr   = 0x10 save count = 1
0x0101, // addr descriptor: offset = 0x1 save count = 2
0x0314, // addr descriptor: offset = 0x14 save count = 4
0x031c, // addr descriptor: offset = 0x1c save count = 4
0x0230, // addr descriptor: offset = 0x30 save count = 3
0x0338, // addr descriptor: offset = 0x38 save count = 4
0x0040, // addr descriptor: offset = 0x40 save count = 1
0x8180, // base descriptor: addr   = 0x180 save count = 1
0x0004, // addr descriptor: offset = 0x4 save count = 1
0x0010, // addr descriptor: offset = 0x10 save count = 1
0x0014, // addr descriptor: offset = 0x14 save count = 1
0x8300, // base descriptor: addr   = 0x300 save count = 1
0x0004, // addr descriptor: offset = 0x4 save count = 1
0x0010, // addr descriptor: offset = 0x10 save count = 1
0x001c, // addr descriptor: offset = 0x1c save count = 1
0x006c, // addr descriptor: offset = 0x6c save count = 1
0x8388, // base descriptor: addr   = 0x388 save count = 1
0x0038, // addr descriptor: offset = 0x38 save count = 1
0x003c, // addr descriptor: offset = 0x3c save count = 1
0x8480, // base descriptor: addr   = 0x480 save count = 1
0x0004, // addr descriptor: offset = 0x4 save count = 1
0x0010, // addr descriptor: offset = 0x10 save count = 1
0x001c, // addr descriptor: offset = 0x1c save count = 1
0x006c, // addr descriptor: offset = 0x6c save count = 1
0x8508, // base descriptor: addr   = 0x508 save count = 1
0x0038, // addr descriptor: offset = 0x38 save count = 1
0x003c, // addr descriptor: offset = 0x3c save count = 1
0x8656, // base descriptor: addr   = 0x656 save count = 1
0x0104, // addr descriptor: offset = 0x4 save count = 2
0x000a, // addr descriptor: offset = 0xa save count = 1
0x011a, // addr descriptor: offset = 0x1a save count = 2
0x002d, // addr descriptor: offset = 0x2d save count = 1
0x001f, // addr descriptor: offset = 0x1f save count = 1
0x0050, // addr descriptor: offset = 0x50 save count = 1
0x0154, // addr descriptor: offset = 0x54 save count = 2
0x005c, // addr descriptor: offset = 0x5c save count = 1
0x016a, // addr descriptor: offset = 0x6a save count = 2
0x006f, // addr descriptor: offset = 0x6f save count = 1
0x007d, // addr descriptor: offset = 0x7d save count = 1
0x0000
};

//----------------------------------------------------------------------------------------------------
// LPDDR4
//----------------------------------------------------------------------------------------------------
uint32 global_bit_mask               =  0xFFFFFFFF;
uint64 global_beat_mask[4]           = {0xFFFFFFFFFFFFFFFF,
                                        0xFFFFFFFFFFFFFFFF,
                                        0xFFFFFFFFFFFFFFFF,
                                        0xFFFFFFFFFFFFFFFF
                                       };

uint64 training_address[2][2] = {{0}};

// Size of the array write_in_pattern[] in number of uint32s. This number is supplied to memory read/write functions used during DDR PHY training.
#define DQ_TRAINING_PATTERN_SIZE (sizeof(write_in_pattern) / sizeof(uint32))

uint32 write_in_pattern[] __attribute__((aligned(8))) = {
0xDEADBEEF, 0x5A5A5A5A, 0xA5A5A5A5, 0xFEEDFACE, 0xCAFEBABE, 0xA5A5A5A5, 0x5A5A5A5A, 0x0BADF00D, 
0xDEADBEEF, 0x5A5A5A5A, 0xA5A5A5A5, 0xFEEDFACE, 0xCAFEBABE, 0xA5A5A5A5, 0x5A5A5A5A, 0x0BADF00D,
0x22222222, 0x5a5a5a5a, 0xa5a5a5a5, 0x5a5a5a5a, 0x0f0f0f0f, 0xf0f0f0f0, 0x00000000, 0xffffffff, 
0xa5a5a5a5, 0x5a5a5a5a, 0xa5a5a5a5, 0x5a5a5a5a, 0x0f0f0f0f, 0xf0f0f0f0, 0x00000000, 0xffffffff,
0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 
0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000,
0x66666666, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 
0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF,
0xfcfcfcfc, 0x00000000, 0xfcfcfcfc, 0x00000000, 0xfcfcfcfc, 0x00000000, 0xfcfcfcfc, 0x00000000, 
0xfcfcfcfc, 0x00000000, 0xfcfcfcfc, 0x00000000, 0xfcfcfcfc, 0x00000000, 0xfcfcfcfc, 0x00000000,
0x55555555, 0xa5a5a5a5, 0x5a5a5a5a, 0xa5a5a5a5, 0xf0f0f0f0, 0x0f0f0f0f, 0xffffffff, 0x00000000, 
0x5a5a5a5a, 0xa5a5a5a5, 0x5a5a5a5a, 0xa5a5a5a5, 0xf0f0f0f0, 0x0f0f0f0f, 0xffffffff, 0x00000000,
0xf7f7f7f7, 0x08080808, 0xf7f7f7f7, 0x08080808, 0xf7f7f7f7, 0x08080808, 0xf7f7f7f7, 0x08080808, 
0xf7f7f7f7, 0x08080808, 0xf7f7f7f7, 0x08080808, 0xf7f7f7f7, 0x08080808, 0xf7f7f7f7, 0x08080808,
0x5A5A5A5A, 0x5B5B5B5B, 0x5A5A5A5A, 0x5B5B5B5B, 0x5A5A5A5A, 0x5B5B5B5B, 0x5A5A5A5A, 0x5B5B5B5B, 
0x5A5A5A5A, 0x5B5B5B5B, 0x5A5A5A5A, 0x5B5B5B5B, 0x5A5A5A5A, 0x5B5B5B5B, 0x5A5A5A5A, 0x5B5B5B5B, 
0x5B5B5B5B, 0x5A5A5A5A, 0x5B5B5B5B, 0x5A5A5A5A, 0x5B5B5B5B, 0x5A5A5A5A, 0x5B5B5B5B, 0x5A5A5A5A,
0x5B5B5B5B, 0x5A5A5A5A, 0x5B5B5B5B, 0x5A5A5A5A, 0x5B5B5B5B, 0x5A5A5A5A, 0x5B5B5B5B, 0x5A5A5A5A,

0x3789D91E, 0x19E69FC4, 0x484E3BE0, 0xCD6014C4, 0x77D1D4B1, 0xAB30DF90, 0xCB1F041D, 0xFA3D26AB, 
0x63E23084, 0x03DD0851, 0x18577023, 0x913FB1B5, 0xE81D6955, 0xCF7163D4, 0x2A452F75 ,0x01E6629F,
0x797B2280, 0x9ACE7CD7, 0xA06B1A1D, 0xBE79D5DB, 0xEBBFF686, 0x6EBB85FF, 0xBF31749A, 0x29F2E6C7, 
0x8C91AAFC, 0xAB1AAB75, 0xAC48D44E, 0x504C17D0, 0xB1C5B12A, 0xAC329613, 0xBA5D149B ,0x592054F1,
0x753B266D, 0x20944DEB, 0x3220F4C2, 0xDFDF25CD, 0x80D363CB, 0x63EEBF1B, 0x4C1A820C, 0x1E54639A, 
0x127F2554, 0xEB3C2935, 0xCDAAFAEF, 0xC84B4320, 0x2A0B9168, 0xA4CC98BF, 0xF3609880 ,0xD79CAE43,
0x713BB864, 0x72FA35F8, 0x6DED7DF2, 0x23A354E0, 0xCB34F01A, 0xB035D5A0, 0x86C06BD6, 0xEFAB7C43, 
0x74DFBE03, 0xC1E91236, 0x09CAD86F, 0x1C0D78EC, 0xDA6B8136, 0xA18EA81C, 0xEA4D7192 ,0x8B331A3B,
0x81810414, 0xA4555EFD, 0x31722982, 0xFC0E7445, 0x883A76E5, 0x4A9718FF, 0x9577C0A3, 0x49F2D41B, 
0x5D8D9AC4, 0xB48E6E84, 0x4451DBED, 0x0C03D7D7, 0x398540DD, 0xBDC56DB1, 0x29555DDF ,0x47B64FF0,
0x3DF87F32, 0x4697A951, 0x78FFBEF0, 0xA71F8CBD, 0xC449842F, 0x036EDD7C, 0x4E020585, 0xFB39FB64, 
0x4FF1CEC2, 0x7D3A52FB, 0xF8A2D60B, 0x1FD68B15, 0x092A6BD6, 0x2386376D, 0xCCFC9C10 ,0xB8257024,
0xC1A1FDED, 0x186DD915, 0xA0AC5EFE, 0x5363D067, 0x2507A9F2, 0xADD4BC69, 0xFC266ABB, 0x835F251B, 
0x9FA61759, 0x071198C9, 0x1000275A, 0xC7883CC7, 0xF3C7AB76, 0x20E052FD, 0xE3AA85F3 ,0x042B74AA
};

//================================================================================================//
// Memory write
//================================================================================================//
void DDRSS_mem_write (DDR_STRUCT *ddr, uint8 ch, uint8 cs)
{
   ddr_mem_copy (write_in_pattern, (uint32 *)(size_t)training_address[ch][cs], DQ_TRAINING_PATTERN_SIZE, 1);
}


uint16 DDRSS_dq_remapping (uint8 pattern)
{
    uint8  bit;
    uint16 return_data = 0;    
    uint8  bit_data = 0;
    
    for (bit = 0; bit < 8; bit ++)
    {
        bit_data = (pattern >> bit) & 0x1;
        return_data = return_data | (bit_data << bit_remapping_bimc2phy [bit]);        
    }
    
    return return_data;
}


//================================================================================================//
// Memory read with results per byte for RD training with Phase.
// This function will do a memory read and report pass/fail per BYTE against the expected pattern
//================================================================================================//
uint8 *DDRSS_mem_read_per_byte_phase (DDR_STRUCT *ddr, uint8 ch, uint8 cs, uint8 read_test_loop_cnt, uint8 phase)
{
   uint32 read_back_pattern[DQ_TRAINING_PATTERN_SIZE] __attribute__((aligned(8))) =  {0};

   uint32 mask_per_byte   = 0;
   uint32 beat            = 0;
   uint32 compare_pattern = 0;
   uint8  beat64          = 0;
   uint64 beat_mask       = 0;
   uint8  beat_mask_ptr   = 0;
   uint8  byte            = 0;
   uint8  byte_remap      = 0;
   uint8  loop            = 0;
   uint8  (*fail_flag_per_byte_ptr) = fail_flag_per_byte;

   // Reset fail_flag_per_byte_ptr
   for (byte = 0; byte < NUM_DQ_PCH; byte++)
   {
      fail_flag_per_byte_ptr[byte]  = 0;
   }

   for (loop = 0; loop < read_test_loop_cnt; loop++)
   {
      //ddr_mem_read 
      ddr_mem_copy ((uint32 *)(size_t)training_address[ch][cs], read_back_pattern, DQ_TRAINING_PATTERN_SIZE, 1);
      compare_pattern = 0x00000000;

      for (beat = 0; beat < DQ_TRAINING_PATTERN_SIZE; beat++)
      {
        beat64        = beat>>6;
        beat_mask     = global_beat_mask[beat64];
        beat_mask_ptr = (beat_mask>>(beat & 0x3F)) & 0x1;

        if (((phase == (beat & 0x1)) || (phase == 0x3)) & beat_mask_ptr)
        {
          // compare_pattern records the position of failed bits
          compare_pattern |= (read_back_pattern[beat] ^ write_in_pattern[beat]) & global_bit_mask;

          // If failure occurs on all bytes, jump out of the loop to save time
          if (compare_pattern == 0xFFFFFFFF)
          break;
        }
      }

      // Compare results and return pass/fail per BYTE
      mask_per_byte = 0xFF;
      for (byte = 0; byte < NUM_DQ_PCH; byte++)
      {
         byte_remap = byte_remapping_table[ch][byte];

         // Only accumulates the specific byte compare results
         fail_flag_per_byte_ptr[byte_remap] |= (((compare_pattern & mask_per_byte) == 0) ? 0 : 1);
         // Mask shift for the next byte
         mask_per_byte = mask_per_byte << 8;
      }
   }

   return fail_flag_per_byte_ptr;
}

//================================================================================================//
// Memory read with results per byte for RD training with Phase.
// This function will do a memory read and report pass/fail per BYTE against the expected pattern
//================================================================================================//
uint8 *DDRSS_mem_read_per_byte_DBI_phase (DDR_STRUCT *ddr, uint8 ch, uint8 cs, uint8 read_test_loop_cnt, uint8 phase)
{
   uint32 read_back_pattern[DQ_TRAINING_PATTERN_SIZE] __attribute__((aligned(8))) =  {0};

   uint32 mask_per_byte   = 0;
   uint32 beat            = 0;
   uint32 compare_pattern = 0;
   uint8  beat64          = 0;
   uint64 beat_mask       = 0;
   uint8  beat_mask_ptr   = 0;
   uint8  byte            = 0;
   uint8  byte_remap      = 0;
   uint8  loop            = 0;
   uint8  (*fail_flag_per_byte_ptr) = fail_flag_per_byte;
   uint32 temp_pattern    = 0;

   // Reset fail_flag_per_byte_ptr
   for (byte = 0; byte < NUM_DQ_PCH; byte++)
   {
      fail_flag_per_byte_ptr[byte]  = 0;
   }

   for (loop = 0; loop < read_test_loop_cnt; loop++)
   {
      //ddr_mem_read 
      ddr_mem_copy ((uint32 *)(size_t)training_address[ch][cs], read_back_pattern, DQ_TRAINING_PATTERN_SIZE, 1);
      compare_pattern = 0x00000000;

      for (beat = 0; beat < DQ_TRAINING_PATTERN_SIZE; beat++)
      {
        beat64        = beat>>6;
        beat_mask     = global_beat_mask[beat64];
        beat_mask_ptr = (beat_mask>>(beat & 0x3F)) & 0x1;
        
        if (((phase == (beat & 0x1)) || (phase == 0x3)) & beat_mask_ptr)
        {
          // temp_pattern records the position of failed bits
          temp_pattern = (read_back_pattern[beat] ^ write_in_pattern[beat]) & global_bit_mask;
          
          // only count failures where the whole byte is inverted
          mask_per_byte = 0xFF;
          for (byte = 0; byte < NUM_DQ_PCH; byte++)
          {
            // compare_pattern records the positions where the whole byte is inverted
            compare_pattern |= (((temp_pattern & mask_per_byte) == mask_per_byte) ? mask_per_byte : 0);
            mask_per_byte = mask_per_byte << 8;
          }

          // If failure occurs on all bytes, jump out of the loop to save time
          if (compare_pattern == 0xFFFFFFFF)
          break;
        }
      }

      mask_per_byte = 0xFF;
      for (byte = 0; byte < NUM_DQ_PCH; byte++)
      {
         byte_remap = byte_remapping_table[ch][byte];

         // Only accumulates the specific byte compare results
         fail_flag_per_byte_ptr[byte_remap] |= (((compare_pattern & mask_per_byte) == 0) ? 0 : 1);
         // Mask shift for the next byte
         mask_per_byte = mask_per_byte << 8;
      }
   }

   return fail_flag_per_byte_ptr;
}

//================================================================================================//
// Memory read with results per bit
// This function will do a memory read and report pass/fail per BIT against the expected pattern
//================================================================================================//
uint8 *DDRSS_mem_read_per_bit_phase (DDR_STRUCT *ddr, uint8 ch, uint8 cs, uint8 read_test_loop_cnt, uint8 wr_rd, uint8 byte_lane , uint8 phase)
{
   uint32 read_back_pattern[DQ_TRAINING_PATTERN_SIZE] = {0};
   uint32 mask_per_bit    = 0;
   uint32 beat            = 0;
   uint32 compare_pattern = 0;
   uint8  beat64          = 0;
   uint64 beat_mask       = 0;
   uint8  beat_mask_ptr   = 0;
   uint8  bit             = 0;
   uint8  phy_bit         = 0;
   uint8  byte            = 0;
   uint8  byte_remap      = 0;
   uint8  bit_mask_offset = 0;
   uint8  loop  = 0;
   uint8  (*fail_flag_per_bit_ptr) = fail_flag_per_bit;

   // Reset fail_flag_per_bit_ptr
   for (bit = 0; bit < 40; bit++)
   {
     fail_flag_per_bit_ptr[bit]  = 0;
   }

   for (loop = 0; loop < read_test_loop_cnt; loop++)
   {
      //ddr_mem_read 
      ddr_mem_copy ((uint32 *)(size_t)training_address[ch][cs], read_back_pattern, DQ_TRAINING_PATTERN_SIZE, 1);
      compare_pattern = 0x00000000;

      for (beat = 0; beat < DQ_TRAINING_PATTERN_SIZE; beat++)
      {
        beat64        = beat>>6;
        beat_mask     = global_beat_mask[beat64];
        beat_mask_ptr = (beat_mask>>(beat & 0x3F)) & 0x1;

        if (((phase == (beat & 0x1)) || (phase == 0x3)) & beat_mask_ptr)
        {
         // compare_pattern records the position of failed bits
         compare_pattern |= (read_back_pattern[beat] ^ write_in_pattern[beat]) & global_bit_mask;
        }

         // If failure occurs on all bits, jump out of the loop to save time 
         if (compare_pattern == 0xFFFFFFFF)
         break;
      }

      // Compare results and return pass/fail per BIT
      mask_per_bit = 0x01;
      for (byte = 0; byte < 4; byte++)
      {
         // In RD training, wr_rd = 1, all bytes are processed
         // In WR training, wr_rd = 0, only the specific byte is processed
         if (wr_rd | (byte == byte_lane))
         {
            byte_remap = byte_remapping_table[ch][byte];

            for (bit = 0; bit < 8; bit++)
            {
               phy_bit = byte*10 + connected_bit_mapping_no_DBI_A[bit];

               if ((byte&0x01) == 0) //byte 0 and byte 2
               {
                  bit_mask_offset = byte_remap*8 + bit_remapping_phy2bimc_DQ_Even[bit];
               }
               else //byte 1 and byte 3 have different mapping order compared with byte0/2
               {
                  bit_mask_offset = byte_remap*8 + bit_remapping_phy2bimc_DQ_Odd[bit];
               }
               mask_per_bit = 0x01 << bit_mask_offset;

               // Only accumulates the specific bit compare results
               fail_flag_per_bit_ptr[phy_bit] = (((compare_pattern & mask_per_bit) == 0) ? 0 : 1);
            }
         }
      } //byte loop
   }
   return fail_flag_per_bit_ptr;
}


//================================================================================================//
// MR write vref value to one die at a time.
//================================================================================================//
void DDRSS_MR_Write_per_die (DDR_STRUCT *ddr, uint8 ch, uint8 cs, uint8 MR_addr, uint8 MR_data_die1, uint8 MR_data_die2)
{
    // We write the die2 value to both dies first. Then, we mask out die2 and write the die1 value.
    BIMC_MR_Write (CH_1HOT(ch), CS_1HOT(cs), MR_addr, MR_data_die2);
    HWIO_OUTXF2 (REG_OFFSET_DPE(ch), DPE_CONFIG_13, LP4_CHB_DISABLE, LP4_CHA_DISABLE, 1, 0);
    HWIO_OUTX (REG_OFFSET_DPE(ch), DPE_CONFIG_4, 1);

    BIMC_MR_Write (CH_1HOT(ch), CS_1HOT(cs), MR_addr, MR_data_die1);
    HWIO_OUTXF2 (REG_OFFSET_DPE(ch), DPE_CONFIG_13, LP4_CHB_DISABLE, LP4_CHA_DISABLE, 0, 0);
    HWIO_OUTX (REG_OFFSET_DPE(ch), DPE_CONFIG_4, 1);
    
}


/* ============================================================================
**  Function : DDRSS_device_reset_cmd
** ============================================================================
*/
/*!
*   @brief
*   Sets the Device reset register in DDRSS gets reset during power collapse. 
*   
*   @details
*  Sets the Device reset register in DDRSS gets reset during power collapse. 
*   
*/
void DDRSS_device_reset_cmd()
{
 // Device reset register in DDRSS gets reset during power collapse. Hence set this register
   // to drive reset deasserted to the device before unfreezing the IOs.
   HWIO_OUTX (DDR_SS_BASE + SEQ_DDR_SS_DDR_REG_DDR_SS_REGS_OFFSET, DDR_SS_REGS_RESET_CMD, 1);
   while ( HWIO_INX (DDR_SS_BASE + SEQ_DDR_SS_DDR_REG_DDR_SS_REGS_OFFSET, DDR_SS_REGS_RESET_CMD) != 1); 
}


void DDRSS_Vref_CDC_Coarse_Schmoo (DDR_STRUCT *ddr,
                                   best_eye_struct *best_eye_ptr,
                                   uint8 (*coarse_fail_count_ptr)[COARSE_VREF][COARSE_CDC],
                                   uint8 ch, 
                                   uint8 cs, 
                                   training_data *training_data_ptr,
                                   training_params_t *training_params_ptr,
                                   uint32 clk_freq_khz
                                  )
{
    uint8                loop_cnt = 0;
    uint8                 phy_inx = 0;
    uint8     MR12_vref_range_bit = MR12_VREF_RANGE_BIT;
    uint8           pattern_index = 0;

    uint8             vref_value = 0;
    uint8             vref_index = 0;
    uint8              cdc_value = 0;
    uint8              cdc_index = 0;
    uint8              vref_data = 0;
    uint8             cs_pattern = 0;
    uint16        expect_pattern = 0;   

    uint32        reg_offset_dpe = 0;
    uint32   reg_offset_ddr_dq_1 = 0;
    uint32   reg_offset_ddr_dq_3 = 0;
   
    uint8  failure_cnt        [NUM_CA_PCH] = {0};
    uint16 feedback_data      [NUM_CA_PCH] = {0};
    uint32 reg_offset_ddr_phy [NUM_CA_PCH] = {0};

    reg_offset_dpe        = REG_OFFSET_DPE(ch);
    reg_offset_ddr_phy[0] = REG_OFFSET_DDR_PHY_CH (ch) + CA0_DDR_PHY_OFFSET;
    reg_offset_ddr_phy[1] = REG_OFFSET_DDR_PHY_CH (ch) + CA1_DDR_PHY_OFFSET;
    reg_offset_ddr_dq_1   = REG_OFFSET_DDR_PHY_CH (ch) + DQ1_DDR_PHY_OFFSET;
    reg_offset_ddr_dq_3   = REG_OFFSET_DDR_PHY_CH (ch) + DQ3_DDR_PHY_OFFSET;   

    for (vref_value  = training_params_ptr->ca_vref.coarse_vref_start_value; 
         vref_value <= training_params_ptr->ca_vref.coarse_vref_max_value; 
         vref_value += training_params_ptr->ca_vref.coarse_vref_step) 
    {    
        // Add MR12_vref_range_bit as the bit 6
        if (vref_value <= 50)
        {
            vref_data = (MR12_vref_range_bit << 6) | vref_value;    
        }
        else //for range 1
        {
            vref_data = ((MR12_vref_range_bit + 1) << 6) | (vref_value - CA_VREF_RANGE_THRESHOLD) ;  
        }
       // Send Vref for both CA0 and CA1
       BIMC_Send_Vref_Data (ch, cs, vref_data/*vref_data_ch_a*/, vref_data/*vref_data_ch_b*/);  
   
       vref_index = ( vref_value - training_params_ptr->ca_vref.coarse_vref_start_value ) 
                    / training_params_ptr->ca_vref.coarse_vref_step;      // Vref_step is never to be 0, truncation is expected
       
       for (cdc_value  = training_params_ptr->ca_vref.coarse_cdc_start_value; 
            cdc_value <= training_params_ptr->ca_vref.coarse_cdc_max_value; 
            cdc_value += training_params_ptr->ca_vref.coarse_cdc_step) 
       {
           // failures counters reset to 0
           failure_cnt[0] = 0;
           failure_cnt[1] = 0;
         
           cdc_index = ( cdc_value - training_params_ptr->ca_vref.coarse_cdc_start_value) 
                       / training_params_ptr->ca_vref.coarse_cdc_step;     // cdc_step can never be 0, truncation is expected
           
           // Configure CA0 and CA1 CSRs of CDC and retimer setting
           for (phy_inx = 0; phy_inx < NUM_CA_PCH; phy_inx++) 
           {
                DDR_PHY_hal_cfg_cdc_slave_wr (reg_offset_ddr_phy[phy_inx],
                                               cdc_value,
                                               1,  // 1 = coarse delay, 0 = fine delay
                                               1,  // 1 = HP mode,      0 = LP mode 
                                               cs 
                                              );

              DDRSS_CDC_Retimer (ddr,
                                 cs,
                                 cdc_value,     // Coarse CDC delay value 
                                 0,             // In coarse training, fine delay is 0
                                 0,             // coarse_wrlvl_delay,
                                 0,             // fine_wrlvl_delay,  
                                 reg_offset_ddr_phy[phy_inx],      // Base address of CA0 or CA1
                                 clk_freq_khz
                                );
           }
           
           // Different CA patterns are sent out to enchance training result
           for (pattern_index = 0; pattern_index < CA_PATTERN_NUM; pattern_index++) 
           { 
              HWIO_OUTX (reg_offset_dpe, DPE_CA_TRAIN_PRE_CS,  ca_training_pattern_lpddr4[pattern_index][0]);
              HWIO_OUTX (reg_offset_dpe, DPE_CA_TRAIN_CS,      ca_training_pattern_lpddr4[pattern_index][1]);
              HWIO_OUTX (reg_offset_dpe, DPE_CA_TRAIN_POST_CS, ca_training_pattern_lpddr4[pattern_index][2]);
             
              cs_pattern     = ca_training_pattern_lpddr4[pattern_index][1];
              expect_pattern = DDRSS_dq_remapping(cs_pattern & 0x3F);     // Only lower 6-bit is valid;

              // For each pattern, each point of histogram will be tested for 16 times,
              for (loop_cnt = training_params_ptr->ca_vref.max_loopcnt; loop_cnt > 0; loop_cnt --) 
              {
                  // The MC sends the VREF setting through DQ PHY0 to DRAM on DQ[6:0]
                  BIMC_Send_CA_Pattern (ch, cs);
                  
                  feedback_data[0] = DDR_PHY_hal_sta_ca_vref (reg_offset_ddr_dq_3); 
                  feedback_data[1] = DDR_PHY_hal_sta_ca_vref (reg_offset_ddr_dq_1); 
                  
                  if (feedback_data[0] != expect_pattern) {
                      failure_cnt[0] = failure_cnt[0] + 1; 
                  }
                  if (feedback_data[1] != expect_pattern) {
                      failure_cnt[1] = failure_cnt[1] + 1; 
                  }
                  // If one of the 2 CAs passes, ca_pattern is required to invert
                  if (feedback_data[0] == expect_pattern || feedback_data[1] == expect_pattern) {
                     cs_pattern = ~cs_pattern;   //Invert CS_Pattern if passes  
                     expect_pattern = DDRSS_dq_remapping (cs_pattern & 0x3F);   
                     HWIO_OUTX (reg_offset_dpe, DPE_CA_TRAIN_CS, cs_pattern);
                  }
              }
           }
         
           coarse_fail_count_ptr[0][vref_index][cdc_index] = failure_cnt[0];
           coarse_fail_count_ptr[1][vref_index][cdc_index] = failure_cnt[1];

       }//End of CDC loop
    }//End of Vref loop
    
    // Process the histogram of both CA0 and CA1
    for (phy_inx = 0; phy_inx < NUM_CA_PCH; phy_inx ++)     
    {
       DDRSS_Post_Histogram_Coarse_Horizon_Eye (best_eye_ptr,
                                                coarse_fail_count_ptr,
                                                training_data_ptr,
                                                training_params_ptr,
                                                0,    // Training type is CA Vref training
                                                phy_inx,
                                                clk_freq_khz
                                               );
    }
}


void DDRSS_Vref_CDC_Fine_Schmoo (DDR_STRUCT *ddr,
                                 best_eye_struct *ca_best_eye_ptr,
                                 uint8 ch, 
                                 uint8 cs, 
                                 training_data *training_data_ptr,
                                 training_params_t *training_params_ptr,
                                 uint8 (*ca_fine_fail_count_ptr)[PINS_PER_PHY][FINE_VREF][FINE_CDC],
                                 uint8 (*ca_boundary_fine_cdc_ptr)[PINS_PER_PHY][FINE_VREF],
                                 uint8 start_fine_vref_value, 
                                 uint8 max_fine_vref_value,   
                                 ca_vref_diff_struct *ca_vref_diff_ptr,
                                 uint8 *coarse_cdc_value,
                                 uint8  left_right,          // 0:Left side fine training; 1:Right side fine training
                                 uint32 clk_freq_khz
                                )
{
    uint8                   loop_cnt = 0;
    uint8                    phy_inx = 0;
    uint8                        bit = 0;
    uint8              pattern_index = 0;
    uint8                 vref_value = 0;
    uint8                 vref_index = 0;
    uint8                  cdc_value = 0;
    uint8                  cdc_index = 0;
    uint8                 cs_pattern = 0;
    uint8    temp_smaller_vref_value = 0;
    uint8     temp_larger_vref_value = 0;
    uint8         smaller_vref_value = 0;
    uint8          larger_vref_value = 0;
    
    uint16            expect_pattern = 0;
    uint16                bit_offset = 0; 
    uint32            reg_offset_dpe = 0;
    
    uint8    ca_MR12_vref_range_bit[NUM_CA_PCH] = {0};
    uint8             ca_vref_value[NUM_CA_PCH] = {0};
    uint8              ca_vref_data[NUM_CA_PCH] = {0};
    uint16         ca_feedback_data[NUM_CA_PCH] = {0};
    uint16           compare_result[NUM_CA_PCH] = {0};
    uint32          ca_ddr_phy_base[NUM_CA_PCH] = {0}; 
    uint32          dq_ddr_phy_base[NUM_CA_PCH] = {0}; 

    uint8  ca_start_fine_vref_value[NUM_CA_PCH] = {0};
    uint8    ca_max_fine_vref_value[NUM_CA_PCH] = {0};

    uint8  ca_failure_cnt[NUM_CA_PCH][PINS_PER_PHY];    
    
    reg_offset_dpe            = REG_OFFSET_DPE(ch);
    ca_ddr_phy_base[0]        = REG_OFFSET_DDR_PHY_CH(ch) + CA0_DDR_PHY_OFFSET;
    ca_ddr_phy_base[1]        = REG_OFFSET_DDR_PHY_CH(ch) + CA1_DDR_PHY_OFFSET;
    dq_ddr_phy_base[0]        = REG_OFFSET_DDR_PHY_CH(ch) + DQ3_DDR_PHY_OFFSET;
    dq_ddr_phy_base[1]        = REG_OFFSET_DDR_PHY_CH(ch) + DQ1_DDR_PHY_OFFSET;

    ca_MR12_vref_range_bit[0] = MR12_VREF_RANGE_BIT;
    ca_MR12_vref_range_bit[1] = MR12_VREF_RANGE_BIT;

    
    for (vref_value = start_fine_vref_value; vref_value <= max_fine_vref_value; vref_value += training_params_ptr->ca_vref.fine_vref_step) 
    {    
       vref_index = (vref_value - start_fine_vref_value) / training_params_ptr->ca_vref.fine_vref_step;     // vref_fine_step can never be 0, truncation is expected
       
       // Calculating the smaller vref value and larger vref value of CA0 and CA1
       // In case that the smaller coarse result is 0, the vref range can only be from 0 to coarse_vref_step, rather than to coarse_vref_step * 2
       if (ca_vref_diff_ptr->ca_coarse_vref == 0)  
       { 
          temp_smaller_vref_value = (vref_value > training_params_ptr->ca_vref.coarse_vref_step) ? 0xFF : vref_value;
          
          if (ca_vref_diff_ptr->ca_vref_diff != 0)  {
             temp_larger_vref_value  = vref_value + ca_vref_diff_ptr->ca_vref_diff - training_params_ptr->ca_vref.coarse_vref_step;
          }
          else  {
             temp_larger_vref_value = temp_smaller_vref_value;
          }
       }
       else
       {
          temp_smaller_vref_value = vref_value;
          temp_larger_vref_value  = vref_value + ca_vref_diff_ptr->ca_vref_diff;
       }
       
       // When vref value exceeds the max boundary, the value is set to 0xFF 
       smaller_vref_value = (temp_smaller_vref_value > training_params_ptr->ca_vref.coarse_vref_max_value) ? 0xFF : temp_smaller_vref_value;
       larger_vref_value  = (temp_larger_vref_value  > training_params_ptr->ca_vref.coarse_vref_max_value) ? 0xFF : temp_larger_vref_value;
       
       // Determine the CA0 and CA1 vref value based on ca_vref_flag
       ca_vref_value[0] = (ca_vref_diff_ptr->ca_vref_flag) ? larger_vref_value  : smaller_vref_value;
       ca_vref_value[1] = (ca_vref_diff_ptr->ca_vref_flag) ? smaller_vref_value : larger_vref_value;
      
       // Add MR12_vref_range_bit as bit 6 for both CA0 and CA1
    if ( ca_vref_value[0] <= 50)
    {
        ca_vref_data[0] = (ca_MR12_vref_range_bit[0] << 6) | ca_vref_value[0];
    }
    else
    {
        ca_vref_data[0] = ((ca_MR12_vref_range_bit[0]+1) << 6) | (ca_vref_value[0]-CA_VREF_RANGE_THRESHOLD);       
    }
    if ( ca_vref_value[1] <= 50)
    {
        ca_vref_data[1] = (ca_MR12_vref_range_bit[1] << 6) | ca_vref_value[1];
    }
    else
    {
        ca_vref_data[1] = ((ca_MR12_vref_range_bit[1]+1) << 6) | (ca_vref_value[1]-CA_VREF_RANGE_THRESHOLD);       
    }
       // Disable corresponding DQ sending Vref value for CA0 
       if (ca_vref_value[0] == 0xFF) 
       {
           HWIO_OUTXF ((REG_OFFSET_DDR_PHY_CH (ch) + CA0_DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMHUB_TOP_1_CFG, DQS_STRUCT_HALF_CYCLE_R0, 1);
           HWIO_OUTXF ((REG_OFFSET_DDR_PHY_CH (ch) + CA0_DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG, TRIG_WRLVL_LATCH, 0x1);
           HWIO_OUTXF ((REG_OFFSET_DDR_PHY_CH (ch) + CA0_DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG, TRIG_WRLVL_LATCH, 0x0);
       }
       // Disable corresponding DQ sending Vref value for CA1 
       if (ca_vref_value[1] == 0xFF) 
       {
           HWIO_OUTXF ((REG_OFFSET_DDR_PHY_CH (ch) + CA1_DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMHUB_TOP_1_CFG, DQS_STRUCT_HALF_CYCLE_R0, 1);
           HWIO_OUTXF ((REG_OFFSET_DDR_PHY_CH (ch) + CA1_DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG, TRIG_WRLVL_LATCH, 0x1);
           HWIO_OUTXF ((REG_OFFSET_DDR_PHY_CH (ch) + CA1_DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG, TRIG_WRLVL_LATCH, 0x0);
       }

       BIMC_Send_Vref_Data (ch, cs, ca_vref_data[0]/*vref_data_ch_a*/, ca_vref_data[1]/*vref_data_ch_b*/);  
   
       // Only in the case where not both vref of CA0 and CA1 are 0xFF, start to scan CDC
       // The case that Vref of CA0 and CA1 are 0xFF means current Vref is invalid for Both CAs, just skip the training 
       // TODO: Elessar DEBUG: Return False or error message when both vref are 0xFF
       if (ca_vref_value[0] != 0xFF || ca_vref_value[1] != 0xFF)   
       { 
          for (cdc_value = 0; cdc_value <= training_params_ptr->ca_vref.fine_cdc_max_value; cdc_value += training_params_ptr->ca_vref.fine_cdc_step) 
          {
              // failures counter = 0
              for (bit = 0; bit < PINS_PER_PHY; bit ++)
              {
                 ca_failure_cnt[0][bit] = 0;
                 ca_failure_cnt[1][bit] = 0;
              }

              cdc_index = cdc_value / training_params_ptr->ca_vref.fine_cdc_step;     // cdc_step is never to be 0, truncation is expected
              
              for (phy_inx = 0; phy_inx < NUM_CA_PCH; phy_inx ++)  
              {
                     // Setting CA0/CA1 CDC CSR
                     DDR_PHY_hal_cfg_cdc_slave_wr (ca_ddr_phy_base[phy_inx], 
                                                    cdc_value,
                                                    0,  // 1 = coarse delay, 0 = fine delay
                                                    1,  // 1 = HP mode,      0 = LP mode 
                                                    cs 
                                                   );

                     DDRSS_CDC_Retimer (ddr,
                                        cs,
                                        coarse_cdc_value[phy_inx],     // Coarse CDC delay value 
                                        cdc_value,     
                                        0, //coarse_wrlvl_delay
                                        0, //fine_wrlvl_delay  
                                        ca_ddr_phy_base[phy_inx],       // Base address of CA0 or CA1
                                        clk_freq_khz
                                       );
              }
              
              for (pattern_index = 0; pattern_index < CA_PATTERN_NUM; pattern_index++) 
              { 
                 HWIO_OUTX (reg_offset_dpe, DPE_CA_TRAIN_PRE_CS,  ca_training_pattern_lpddr4[pattern_index][0]);
                 HWIO_OUTX (reg_offset_dpe, DPE_CA_TRAIN_CS,      ca_training_pattern_lpddr4[pattern_index][1]);
                 HWIO_OUTX (reg_offset_dpe, DPE_CA_TRAIN_POST_CS, ca_training_pattern_lpddr4[pattern_index][2]);
                
                 cs_pattern     = ca_training_pattern_lpddr4[pattern_index][1];
                 expect_pattern = DDRSS_dq_remapping (cs_pattern & 0x3F);                 

                 for (loop_cnt = training_params_ptr->ca_vref.max_loopcnt; loop_cnt > 0; loop_cnt --) 
                 {
                     // The MC sends the VREF setting through DQ PHY0 to DRAM on DQ[6:0]
                     BIMC_Send_CA_Pattern (ch, cs);
                     
                     for (phy_inx = 0; phy_inx < NUM_CA_PCH; phy_inx ++)  
                     {
                             ca_feedback_data[phy_inx] = DDR_PHY_hal_sta_ca_vref (dq_ddr_phy_base[phy_inx]); 
                             

                             compare_result[phy_inx] = (ca_feedback_data[phy_inx] ^ expect_pattern);
                             for (bit = 0; bit < PINS_PER_PHY; bit ++)
                             {     
                                bit_offset = 0x1 << bit;                             
                                ca_failure_cnt[phy_inx][bit] += (((compare_result[phy_inx] & bit_offset) == 0) ? 0 : 1);
                             }
                     }                                           
                        // CA0 passes or CA1 passes 
                        if (ca_feedback_data[0] == expect_pattern || ca_feedback_data[1] == expect_pattern)  
                        {   
                           cs_pattern = ~cs_pattern;  //Invert CS_Pattern
                           expect_pattern = DDRSS_dq_remapping (cs_pattern & 0x3F);   
                           HWIO_OUTX (reg_offset_dpe, DPE_CA_TRAIN_CS, cs_pattern);
                        }

                 }
              }

              for (bit = 0; bit < PINS_PER_PHY_CONNECTED_CA; bit ++)
              {     

                 ca_fine_fail_count_ptr[0][connected_bit_mapping_CA[bit]][vref_index][cdc_index] = (ca_vref_value[0] == 0xFF) ? 
                                                                                                      (training_params_ptr->ca_vref.max_loopcnt * CA_PATTERN_NUM) + 1 
                                                                                                        : 
                                                                                                       ca_failure_cnt[0][connected_bit_mapping_CA_PHY[bit]];
                 ca_fine_fail_count_ptr[1][connected_bit_mapping_CA[bit]][vref_index][cdc_index] = (ca_vref_value[1] == 0xFF) ? 
                                                                                                      (training_params_ptr->ca_vref.max_loopcnt * CA_PATTERN_NUM) + 1 
                                                                                                        : 
                                                                                                       ca_failure_cnt[1][connected_bit_mapping_CA_PHY[bit]];
              }
          }  //End of CDC value loop
       }
    }  //End of Vref value loop
    
    // Prepare start/max fine vref value for both CA0 and CA1
    ca_start_fine_vref_value[0] = (ca_vref_diff_ptr->ca_vref_flag) ? start_fine_vref_value + ca_vref_diff_ptr->ca_vref_diff : start_fine_vref_value;
    ca_start_fine_vref_value[1] = (ca_vref_diff_ptr->ca_vref_flag) ? start_fine_vref_value : start_fine_vref_value + ca_vref_diff_ptr->ca_vref_diff;
    
    ca_max_fine_vref_value[0] = (ca_vref_diff_ptr->ca_vref_flag) ? max_fine_vref_value + ca_vref_diff_ptr->ca_vref_diff : max_fine_vref_value;
    ca_max_fine_vref_value[1] = (ca_vref_diff_ptr->ca_vref_flag) ? max_fine_vref_value : max_fine_vref_value + ca_vref_diff_ptr->ca_vref_diff;

    for (phy_inx = 0; phy_inx < NUM_CA_PCH; phy_inx ++)    // Process the histogram of CA0 and CA1 
    {
        for (bit = 0; bit < PINS_PER_PHY; bit ++)
        {        
            DDRSS_Post_Histogram_Fine_Each_Boundary (ca_fine_fail_count_ptr,
                                                     ca_boundary_fine_cdc_ptr,
                                                     ca_start_fine_vref_value[phy_inx], 
                                                     ca_max_fine_vref_value[phy_inx],   
                                                     training_data_ptr,
                                                     training_params_ptr,
                                                     left_right,  // 0:Left side fine training; 1:Right side fine training
                                                     0,    // Training type is CA Vref training
                                                     phy_inx,
                                                     bit
                                                    );
        }
    }
 }
//================================================================================================//
//  Function: DDRSS_rd_dqdqs_dcc_schmoo().
//  Brief description: This function performs dcc schmoo @1555MHz per each CS and
//                       sets the average DCC for both the CS's during read training.
//                       For a fixed vref value, the function performs cdc schmoo for
//                       all the DCC settings.
//================================================================================================//
void DDRSS_rd_dqdqs_dcc_schmoo (DDR_STRUCT *ddr, 
                                uint8 ch, 
                                uint8 cs, 
                                training_params_t *training_params_ptr, 
                                ddrss_rdwr_dqdqs_local_vars *local_vars,
                                uint32 clk_freq_khz)
{
    uint8 byte_lane                    = 0;
    uint32 dq0_ddr_phy_base            = 0;
    uint8  bit                         = 0;
    uint8  dcc_ctl                     = 0;  
    uint8  loopcnt                     = 0; 

    uint8 *compare_result;  

    best_eye_struct best_eye_dcc[NUM_DQ_PCH];
    uint8 compare_result_byte[NUM_DQ_PCH]  = {0x0};
    uint8 dq_max_dcc[NUM_DQ_PCH]           = {0x0};

    // Training data structure pointer
    training_data *training_data_ptr;
    training_data_ptr = (training_data *)(&ddr->flash_params.training_data);

#if DSF_CLK_DCC_RD_DQDQS
#else
    uint8  perbit                                      = 0;
    uint8 compare_result_acc[NUM_DQ_PCH][PINS_PER_PHY] = {{0}};
    uint8 perbit_cdc[NUM_DQ_PCH][PINS_PER_PHY]         = {{0}};
#endif    
    
    // Set DQ0 base for addressing
    dq0_ddr_phy_base    = REG_OFFSET_DDR_PHY_CH(ch) + DQ0_DDR_PHY_OFFSET;

    // Initialize the fail flag, best cdc, coarse and fine 
    for (byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
    {
        // Zero out the coarse and fine CDC values before training
        DDR_PHY_hal_cfg_cdc_slave_rd((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 0, 1, 1, cs);
        DDR_PHY_hal_cfg_cdc_slave_rd((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 0, 0, 1, cs);            
    
        best_eye_dcc[byte_lane].vref_all_fail_flag = 0; // Assume all sane values to begin with.
        best_eye_dcc[byte_lane].best_cdc_value = 0;     // Assume initial value = 0, for initial retimer() setup.    
    }
    
    // Set all local vref to (max+min)/2 and perbit to 0xF
    for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
    {        
        for(bit = 0; bit < PINS_PER_PHY; bit++)
        {
            DDR_PHY_hal_cfg_local_vref((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                                        LOCAL_VREF_MID_VALUE,
                                        cs, 
                                        bit);
            training_data_ptr->results.rd_dqdqs.perbit_vref [ch][cs][byte_lane][bit] = LOCAL_VREF_MID_VALUE;
        }
    }    

#if DSF_CLK_DCC_RD_DQDQS
#else

  // Set the perbit values to align Phase 0 to the left edge (CDC=0)
    for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
    {        
        for(perbit = 0; perbit <= PERBIT_CDC_MAX_VALUE; perbit++)
        {
            for(bit = 0; bit < PINS_PER_PHY; bit++)
            {
                // Sweep all of the perbit at once
                DDR_PHY_hal_cfg_pbit_dq_delay((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                                               bit, 
                                               0,   // 0 for RX.
                                               cs, 
                                               perbit);
            }
            
            // Clear the compare results
            for(bit = 0; bit < PINS_PER_PHY; bit++)
            {
                compare_result_acc[byte_lane][bit]  = 0x0;
            }
            compare_result_byte[byte_lane] = 0x0;

            for (loopcnt = 0;loopcnt < training_params_ptr->rd_dqdqs.max_loopcnt ; loopcnt++) 
            {
                compare_result = DDRSS_mem_read_per_bit_phase(ddr, ch, cs, 1, 1, 5, 0x0);
                for (bit = 0; bit < PINS_PER_PHY; bit++)
                {
                    // compare results are in PHY order
                    compare_result_acc [byte_lane][bit] += compare_result [(byte_lane*PINS_PER_PHY) + bit];        
                }
            }
            for (bit = 0; bit < PINS_PER_PHY; bit++)
            {
                // Find the perbit setting where all iterations fail
                if ((compare_result_acc[byte_lane][bit] == training_params_ptr->rd_dqdqs.max_loopcnt) && (perbit <= perbit_cdc[byte_lane][bit]))
                { 
                    // perbit_cdc is in PHY order
                    perbit_cdc[byte_lane][bit] = perbit;
                }
            }
        } // perbit loop
    } // byte loop

    // Populate the PHY per-bit lanes with the trained values
    for (byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
    {        
        for (bit = 0; bit < PINS_PER_PHY; bit ++)
        {
            // Write the perbit values into the PHY SLICE registers including the gaps (unused, dbi)
            DDR_PHY_hal_cfg_pbit_dq_delay((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                                        bit, 
                                        0,   // 0 for RX.
                                        cs, 
                                        perbit_cdc[byte_lane][bit]);
    
            // Store the perbit values into the data structure including the gaps (unused, dbi)
            training_data_ptr->results.rd_dqdqs.perbit_delay[ch][cs][byte_lane][bit] = perbit_cdc[byte_lane][bit];
    
          //ddr_printf(DDR_NORMAL,"  Byte %d PHY bit %d DCC Perbit = %d\n", byte_lane, bit, perbit_cdc[byte_lane][bit]);
        }
    }
#endif

    // The Phase 0 bits are aligned on the left edge (CDC=0) by the above or WR_DCC.
    // Align the left edge (CDC=0) of the falling edge of DQS.
    // Sweep the RD DCC by delaying the falling edge of DQS until 
    // the search finds the left edge of Phase 1

    //              FFFFFP          FFFFFP
    //           __ --->|        __ --->|  
    //    DQS   |  |____________|  |_______
    //           ___________________________ 
    //    DQ    X_______X_______X_______X____
    //    Phase     0       1       0       


    // Set background pattern in the dcc histogram to max_fail + 1
    memset(local_vars->coarse_schmoo.coarse_dq_dcc_info, 
           training_params_ptr->rd_dqdqs.max_loopcnt + 1, 
           (NUM_DQ_PCH * NUM_CS * RX_DCC ));
    
    // Set the RD CDC Coarse and Fine = 0
    for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
    {
      DDR_PHY_hal_cfg_cdc_slave_rd((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                                  0, 
                                  0x1,  // 1 for coarse_delay
                                  0x1, 
                                  cs);   
 
      DDR_PHY_hal_cfg_cdc_slave_rd((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)),
                                  0, 
                                  0x0, // 0 for fine_delay
                                  0x1, 
                                  cs);   
        
    }

    // Sweep the RD DCC and record the pass/fail histogram for phase 1
    for (dcc_ctl=0;dcc_ctl<RX_DCC;dcc_ctl++)
    {
      for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
      {
        // Set the RD DCC
        HWIO_OUTXF((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                    DDR_PHY_DDRPHY_CMCDCRD_TOP_CFG, 
                    DCC_CTL, 
                    dcc_ctl<<4);

        compare_result_byte[byte_lane] = 0;
      }
      
      // Read-back and compare. Fail info returned on a byte_lane basis, in an array of 4.
      for (loopcnt = 0;loopcnt < training_params_ptr->rd_dqdqs.max_loopcnt ; loopcnt++) 
      {
          compare_result = DDRSS_mem_read_per_byte_phase(ddr, ch, cs, 1, 0x1);
      
          // Accumulate the compare results per byte
          for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
          {
              compare_result_byte[byte_lane] += compare_result[byte_lane];
          }
      }
      
      // Post the error count into the histogram
      for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++)
      { 
          local_vars->coarse_schmoo.coarse_dq_dcc_info[byte_lane][cs][dcc_ctl] = 
                      compare_result_byte[byte_lane]; 
      } // byte_lane 
  
      //Print DCC Histogram
    //for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++)
    //{
    //    ddr_printf(DDR_NORMAL, "  Byte %d CS %d RD DCC %d : %d\n",
    //               byte_lane,cs,dcc_ctl,local_vars->coarse_schmoo.coarse_dq_dcc_info[byte_lane][cs][dcc_ctl]);
    //}
    } // dcc_ctl 
    
  // Find the RD DCC adjustment with the first pattern pass at CDC = 0
  // Subtract 1 from the RD DCC adjustment to derive the last failing adjustment
  for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++)
  { 
      dq_max_dcc[byte_lane] = RX_DCC;

      // Sweep the RD DCC
      for (dcc_ctl = 0; dcc_ctl < RX_DCC; dcc_ctl++) 
      {
        if ((local_vars->coarse_schmoo.coarse_dq_dcc_info[byte_lane][cs][dcc_ctl] == 0) && (dcc_ctl < dq_max_dcc[byte_lane]))
        {
          dq_max_dcc[byte_lane] = dcc_ctl;
        }
      }  

      // Write the data structure with the final value
      local_vars->coarse_schmoo.best_dcc[byte_lane][cs]         = (dq_max_dcc[byte_lane] == 0) ? 0 : (dq_max_dcc[byte_lane]-1);
      training_data_ptr->results.rd_dqdqs.rd_dcc[ch][byte_lane] = local_vars->coarse_schmoo.best_dcc[byte_lane][cs]<<4;
    
      // Write the DCC_CTL register with the final value
      HWIO_OUTXF((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                  DDR_PHY_DDRPHY_CMCDCRD_TOP_CFG, 
                  DCC_CTL, 
                  training_data_ptr->results.rd_dqdqs.rd_dcc[ch][byte_lane]);                              

      ddr_printf(DDR_NORMAL, "  Byte %d CS %d RD_DCC 0x%x\n",
                                byte_lane,cs, training_data_ptr->results.rd_dqdqs.rd_dcc[ch][byte_lane]);

  }
  
  // Average the RD DCC per rank
  if(cs == 1)
  {
      for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++)
      {
          best_eye_dcc[byte_lane].best_dcc_value = (((local_vars->coarse_schmoo.best_dcc[byte_lane][0] + 
                                                      local_vars->coarse_schmoo.best_dcc[byte_lane][1]) / 2) << 4);
  
          // Write the data structure with the final value
          training_data_ptr->results.rd_dqdqs.rd_dcc[ch][byte_lane] = best_eye_dcc[byte_lane].best_dcc_value;
  
          // Write the DCC_CTL register with the final value
          HWIO_OUTXF((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                      DDR_PHY_DDRPHY_CMCDCRD_TOP_CFG, 
                      DCC_CTL, 
                      training_data_ptr->results.rd_dqdqs.rd_dcc[ch][byte_lane]);
      }
  }   
} // DDRSS_rd_dqdqs_dcc_schmoo


//================================================================================================//
//  Function: DDRSS_rd_dqdqs_dcc_schmoo().
//  Brief description: This function performs dcc schmoo @1555MHz per each CS and
//                       sets the average DCC for both the CS's during read training.
//                       For a fixed vref value, the function performs cdc schmoo for
//                       all the DCC settings.
//================================================================================================//
void DDRSS_clk_dcc_rd_dqdqs_schmoo (DDR_STRUCT *ddr, 
                                uint8 ch, 
                                uint8 cs, 
                                training_params_t *training_params_ptr, 
                                ddrss_rdwr_dqdqs_local_vars *local_vars,
                                uint32 clk_freq_khz)
{
    uint8  dq_pin_num           = 0;
    uint8  bit                  = 0;
    uint8  byte_lane            = 0;
    uint8  all_bytes_fail_flag  = 0;
    uint32 dq0_ddr_phy_base     = 0;
    uint32 ca0_ddr_phy_base     = 0;
           
    uint8  clk_dcc_offset_idx   = 0;
    uint8  clk_dcc_offset       = 0;
    uint8  clk_dcc_offset_sign  = 0;
    uint8  phase                = 0;
    uint8  coarse_cdc_value     = 0;
    uint8  fine_cdc_value       = 0;
    uint8  loopcnt              = 0;
    uint8  index                = 0;
    uint8  perbit               = 0;
    
    uint8 *compare_result;  
    uint8 compare_result_acc[NUM_DQ_PCH][PINS_PER_PHY] = {{0}};     
    uint8 compare_result_byte[NUM_DQ_PCH] = {0};     
    
    uint8 best_clk_dcc_ca0      = 0;
    uint8 best_clk_dcc_ca1      = 0;    
    uint8 index_stop            = 0;
    uint16 min_dcd_sum_ca0      = 0xFFFF;
    uint16 min_dcd_sum_ca1      = 0xFFFF;
    uint8 determine_final_value = 0;
    uint8 perbit_cdc[NUM_DQ_PCH][PINS_PER_PHY] = {{0}};

    // Training data structure pointer
    training_data *training_data_ptr;
    training_data_ptr = (training_data *)(&ddr->flash_params.training_data);

    // Set DQ0 base for addressing
    dq0_ddr_phy_base    = REG_OFFSET_DDR_PHY_CH(ch) + DQ0_DDR_PHY_OFFSET;

    // Set DQ0 base for addressing
    dq0_ddr_phy_base    = REG_OFFSET_DDR_PHY_CH(ch) + DQ0_DDR_PHY_OFFSET;
    ca0_ddr_phy_base    = REG_OFFSET_DDR_PHY_CH(ch) + CA0_DDR_PHY_OFFSET;
    
    for(dq_pin_num = 0; dq_pin_num < PINS_PER_PHY; dq_pin_num++)
    {
        for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
        {        
            DDR_PHY_hal_cfg_local_vref((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                                         LOCAL_VREF_MID_VALUE,
                                         cs, 
                                         dq_pin_num);

            perbit_cdc [byte_lane][dq_pin_num] = 0xF;

            // Zero out the coarse and fine CDC values before training
            DDR_PHY_hal_cfg_cdc_slave_rd((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 0, 1, 1, cs);
            DDR_PHY_hal_cfg_cdc_slave_rd((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 0, 0, 1, cs);            
        }
    }    

    // Train the perbit to have at least one fail at the left edge of Phase 0
    for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
    {        
        for (perbit = 0; perbit <= PERBIT_CDC_MAX_VALUE; perbit++)
        {
            for(bit = 0; bit < PINS_PER_PHY; bit++)
            {
                // Sweep all of the perbit at once
                DDR_PHY_hal_cfg_pbit_dq_delay((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                                               bit, 
                                               0,   // 0 for RX.
                                               cs, 
                                               perbit);
            }
            
            // Clear the compare results
            for (bit = 0; bit < PINS_PER_PHY; bit++)
            {
                compare_result_acc[byte_lane][bit]  = 0x0;
            }
            compare_result_byte[byte_lane] = 0x0;

            for (loopcnt = 0;loopcnt < training_params_ptr->rd_dqdqs.max_loopcnt ; loopcnt++) 
            {
                compare_result = DDRSS_mem_read_per_bit_phase(ddr, ch, cs, 1, 1, 5, phase);
                for (bit = 0; bit < PINS_PER_PHY; bit++)
                {
                    // compare results are in PHY order
                    compare_result_acc [byte_lane][bit] += compare_result [(byte_lane*PINS_PER_PHY) + bit];        
                }
            }
            for (bit = 0; bit < PINS_PER_PHY; bit++)
            {
                if ((compare_result_acc[byte_lane][bit] == training_params_ptr->rd_dqdqs.max_loopcnt) && (perbit <= perbit_cdc[byte_lane][bit]))
                { 
                    // perbit_cdc is in PHY order
                    perbit_cdc[byte_lane][bit] = perbit;
                }
            }
        } // perbit loop
    } // byte loop

    // Populate the PHY per-bit lanes with the trained values
    for (byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
    {        
        for (bit = 0; bit < PINS_PER_PHY; bit ++)
        {
            // Write the perbit values into the PHY SLICE registers including the gaps (unused, dbi)
            DDR_PHY_hal_cfg_pbit_dq_delay((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                                           bit, 
                                           0,   // 0 for RX.
                                           cs, 
                                           perbit_cdc[byte_lane][bit]);
    
            // Store the perbit values into the data structure including the gaps (unused, dbi)
            training_data_ptr->results.rd_dqdqs.perbit_delay[ch][cs][byte_lane][bit] = perbit_cdc[byte_lane][bit];
    
            //ddr_printf(DDR_NORMAL,"  Byte %d PHY bit %d DCC Perbit = %d\n", byte_lane, bit, bitperbit_cdc[byte_lane][bit]);
        }
    }
	
    memset(local_vars->coarse_schmoo.coarse_dq_clk_dcc_info, training_params_ptr->rd_dqdqs.max_loopcnt + 1, (NUM_DQ_PCH * NUM_CS * CLK_DCC_OFFSET_RANGE * 2 * COARSE_CDC * FINE_STEPS_PER_COARSE));

    //ddr_printf(DDR_NORMAL, "  Training CLK DCC offset at %u MHz\n",clk_freq_khz/1000);

    for (clk_dcc_offset_idx = 0; clk_dcc_offset_idx < CLK_DCC_OFFSET_RANGE; clk_dcc_offset_idx++)
    {
        if (clk_dcc_offset_idx <= CLK_DCC_OFFSET_RANGE/2 )
        {
            clk_dcc_offset = clk_dcc_offset_idx;
            clk_dcc_offset_sign = 0;    
        }
        else 
        {
            clk_dcc_offset = (clk_dcc_offset_idx - (CLK_DCC_OFFSET_RANGE/2)) ;
            clk_dcc_offset_sign = 1;    
        }
        HWIO_OUTXF2(ca0_ddr_phy_base, DDR_PHY_DDRPHY_DCC_TOP_5_CFG, IO_DCC_DQS_OFFSET_MAGNITUDE,
                    IO_DCC_DQS_OFFSET_SIGN, clk_dcc_offset, clk_dcc_offset_sign);
                    
        HWIO_OUTXF2((ca0_ddr_phy_base + DDR_PHY_OFFSET), DDR_PHY_DDRPHY_DCC_TOP_5_CFG, IO_DCC_DQS_OFFSET_MAGNITUDE,
                     IO_DCC_DQS_OFFSET_SIGN, clk_dcc_offset, clk_dcc_offset_sign);    
                    
        for (phase = 0; phase < 2; phase++)
        {
            // CDC Schmoo loop
            for(coarse_cdc_value  = training_params_ptr->rd_dqdqs.coarse_cdc_start_value; 
                coarse_cdc_value <= 10 /*training_params_ptr->rd_dqdqs.coarse_cdc_max_value*/; 
                coarse_cdc_value += training_params_ptr->rd_dqdqs.coarse_cdc_step)
            {
                for(fine_cdc_value = training_params_ptr->rd_dqdqs.fine_cdc_start_value; 
                    fine_cdc_value < FINE_STEPS_PER_COARSE; 
                    fine_cdc_value += training_params_ptr->rd_dqdqs.fine_cdc_step)
                {
                    // Update RD CDC
                    for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
                    {
                        DDR_PHY_hal_cfg_cdc_slave_rd((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                                                      coarse_cdc_value, 
                                                      0x1, 
                                                      0x1, 
                                                      cs);   
    
                        DDR_PHY_hal_cfg_cdc_slave_rd((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)),
                                                      fine_cdc_value, 
                                                      0x0, 
                                                      0x1, 
                                                      cs);   // 0 for fine_delay_mode.                             
                        compare_result_byte[byte_lane] = 0;
                    }
                    
                    index = ((coarse_cdc_value * FINE_STEPS_PER_COARSE) + fine_cdc_value);
                    
                    // Read-back and compare. Fail info given on a byte_lane basis, in an array of 4.
                    for (loopcnt = 0;loopcnt < training_params_ptr->rd_dqdqs.max_loopcnt ; loopcnt++) 
                    {
                        compare_result = DDRSS_mem_read_per_byte_phase(ddr, ch, cs, 1, phase);
                    
                        // Accumulate the compare results per byte
                        for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
                        {
                            compare_result_byte[byte_lane] += compare_result[byte_lane];
                            all_bytes_fail_flag &= compare_result[byte_lane];
                        }
                    }
                    
                    for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++)
                    { 
                        // If fail, increment error_count.
                        local_vars->coarse_schmoo.coarse_dq_clk_dcc_info[byte_lane][cs][clk_dcc_offset_idx][phase][index] = 
                                    compare_result_byte[byte_lane]; 
                    } // byte_lane 
                    
                } // fine_cdc_value.
            } // CDC_LOOP
            
            index_stop = index;

        } //phase loop
  
        // Build a data structure to hold the eye widths
        for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++)
        { 
            // Evalute both Phases
            for (phase = 0; phase < 2; phase++) 
            {
                // Initialize the dcc_width to 0
                local_vars->coarse_schmoo.clk_dcc_width[byte_lane][cs][clk_dcc_offset_idx][phase] = 0;
    
                // Identify the histogram data eye
                for(index = 0; index < index_stop; index++)
                {
                    if (local_vars->coarse_schmoo.coarse_dq_clk_dcc_info[byte_lane][cs][clk_dcc_offset_idx][phase][index] == 0) 
                    {
                        // Calculate the eye width
                        local_vars->coarse_schmoo.clk_dcc_width[byte_lane][cs][clk_dcc_offset_idx][phase]++;
                    }
                }
                // ddr_printf(DDR_NORMAL, "  Channel %d  Byte %d CS %d clk_dcc_offset_idx %d Phase %d Width = %d -> %d ps\n",
                                        // ch,byte_lane,cs,clk_dcc_offset_idx,phase,
                                        // local_vars->coarse_schmoo.clk_dcc_width[byte_lane][cs][clk_dcc_offset_idx][phase],
                                        // local_vars->coarse_schmoo.clk_dcc_width[byte_lane][cs][clk_dcc_offset_idx][phase] * FINE_STEP_IN_PS);
            }
    
            // Print the DQ DCD
            if (local_vars->coarse_schmoo.clk_dcc_width[byte_lane][cs][clk_dcc_offset_idx][0] > local_vars->coarse_schmoo.clk_dcc_width[byte_lane][cs][clk_dcc_offset_idx][1])
            {
                local_vars->coarse_schmoo.clk_dq_dcd[byte_lane][cs][clk_dcc_offset_idx] = 
                    local_vars->coarse_schmoo.clk_dcc_width[byte_lane][cs][clk_dcc_offset_idx][0] - 
                            local_vars->coarse_schmoo.clk_dcc_width[byte_lane][cs][clk_dcc_offset_idx][1];
            }
            else 
            {
                local_vars->coarse_schmoo.clk_dq_dcd[byte_lane][cs][clk_dcc_offset_idx] = 
                    local_vars->coarse_schmoo.clk_dcc_width[byte_lane][cs][clk_dcc_offset_idx][1] - 
                            local_vars->coarse_schmoo.clk_dcc_width[byte_lane][cs][clk_dcc_offset_idx][0];
            }
            //ddr_printf(DDR_NORMAL, "  Channel %d  Byte %d CS %d clk dcc index - %d  DQ DCD = %d ps \n", 
            //                ch,byte_lane,cs,clk_dcc_offset_idx, local_vars->coarse_schmoo.clk_dq_dcd[byte_lane][cs][clk_dcc_offset_idx] * FINE_STEP_IN_PS);
        }
    } //CLK DCC loop
    
    // Determine the best CLK DCC with the less DCD for both the ranks.
    if((cs == 1) && (ddr->cdt_params[0].common.populated_chipselect == DDR_CS_BOTH))
    {
        determine_final_value = 1;
        for (clk_dcc_offset_idx = 0; clk_dcc_offset_idx < CLK_DCC_OFFSET_RANGE; clk_dcc_offset_idx++)
        {
            if ((local_vars->coarse_schmoo.clk_dq_dcd[0][0][clk_dcc_offset_idx] +   //byte0, CS0
                 local_vars->coarse_schmoo.clk_dq_dcd[1][0][clk_dcc_offset_idx] +   //byte1, CS0
                 local_vars->coarse_schmoo.clk_dq_dcd[0][1][clk_dcc_offset_idx] +   //byte0, CS0
                 local_vars->coarse_schmoo.clk_dq_dcd[1][1][clk_dcc_offset_idx]) < min_dcd_sum_ca0)
            {
                min_dcd_sum_ca0 = (local_vars->coarse_schmoo.clk_dq_dcd[0][0][clk_dcc_offset_idx] +
                                   local_vars->coarse_schmoo.clk_dq_dcd[1][0][clk_dcc_offset_idx] +
                                   local_vars->coarse_schmoo.clk_dq_dcd[0][1][clk_dcc_offset_idx] +                                  
                                   local_vars->coarse_schmoo.clk_dq_dcd[1][1][clk_dcc_offset_idx]);
                best_clk_dcc_ca0 = clk_dcc_offset_idx;
            }
            if ((local_vars->coarse_schmoo.clk_dq_dcd[2][0][clk_dcc_offset_idx] +   //byte0, CS0
                local_vars->coarse_schmoo.clk_dq_dcd[3][0][clk_dcc_offset_idx] +   //byte1, CS0
                local_vars->coarse_schmoo.clk_dq_dcd[2][1][clk_dcc_offset_idx] +   //byte0, CS0
                local_vars->coarse_schmoo.clk_dq_dcd[3][1][clk_dcc_offset_idx]) < min_dcd_sum_ca1)
            {
                min_dcd_sum_ca1 = (local_vars->coarse_schmoo.clk_dq_dcd[2][0][clk_dcc_offset_idx] +
                                   local_vars->coarse_schmoo.clk_dq_dcd[3][0][clk_dcc_offset_idx] +
                                   local_vars->coarse_schmoo.clk_dq_dcd[2][1][clk_dcc_offset_idx] +                                  
                                   local_vars->coarse_schmoo.clk_dq_dcd[3][1][clk_dcc_offset_idx]);
                best_clk_dcc_ca1 = clk_dcc_offset_idx;
            }
        }
    }
    else if (ddr->cdt_params[0].common.populated_chipselect == DDR_CS0)
    {
        determine_final_value = 1;
        for (clk_dcc_offset_idx = 0; clk_dcc_offset_idx < CLK_DCC_OFFSET_RANGE; clk_dcc_offset_idx++)
        {
            if ((local_vars->coarse_schmoo.clk_dq_dcd[0][0][clk_dcc_offset_idx] +   //byte0, CS0
                 local_vars->coarse_schmoo.clk_dq_dcd[1][0][clk_dcc_offset_idx]) < min_dcd_sum_ca0)
            {
                min_dcd_sum_ca0 = (local_vars->coarse_schmoo.clk_dq_dcd[0][0][clk_dcc_offset_idx] +
                                   local_vars->coarse_schmoo.clk_dq_dcd[1][0][clk_dcc_offset_idx]);
                best_clk_dcc_ca0 = clk_dcc_offset_idx;
            }
            if ((local_vars->coarse_schmoo.clk_dq_dcd[2][0][clk_dcc_offset_idx] +   //byte0, CS0
                local_vars->coarse_schmoo.clk_dq_dcd[3][0][clk_dcc_offset_idx]) < min_dcd_sum_ca1)
            {
                min_dcd_sum_ca1 = (local_vars->coarse_schmoo.clk_dq_dcd[2][0][clk_dcc_offset_idx] +
                                   local_vars->coarse_schmoo.clk_dq_dcd[3][0][clk_dcc_offset_idx]);
                best_clk_dcc_ca1 = clk_dcc_offset_idx;
            }
        }
    }
  
    if (determine_final_value == 1)
    {
        if (best_clk_dcc_ca0 <= CLK_DCC_OFFSET_RANGE/2)
        {
            clk_dcc_offset = best_clk_dcc_ca0;
            clk_dcc_offset_sign = 0;    
        }
        else 
        {
            clk_dcc_offset = (best_clk_dcc_ca0 - CLK_DCC_OFFSET_RANGE/2) ;
            clk_dcc_offset_sign = 1;    
        }            
        // Write the DCC_CTL register with the final value
        HWIO_OUTXF2(ca0_ddr_phy_base, DDR_PHY_DDRPHY_DCC_TOP_5_CFG, IO_DCC_DQS_OFFSET_MAGNITUDE,
                    IO_DCC_DQS_OFFSET_SIGN, clk_dcc_offset, clk_dcc_offset_sign);

        // Write the data structure with the final value
        training_data_ptr->results.rd_dqdqs.clk_dcc_mag[ch][0] = clk_dcc_offset;  //CA0
        training_data_ptr->results.rd_dqdqs.clk_dcc_sign[ch][0] = clk_dcc_offset_sign;  //CA0
        
        ddr_printf(DDR_NORMAL, " Channel %d, clk dcc training results CA0_offset =  %d  CA0_sign = %d \n", ch, clk_dcc_offset, clk_dcc_offset_sign);

        if (best_clk_dcc_ca1 <= CLK_DCC_OFFSET_RANGE/2)
        {
            clk_dcc_offset = best_clk_dcc_ca1;
            clk_dcc_offset_sign = 0;    
        }
        else 
        {
            clk_dcc_offset = (best_clk_dcc_ca1 - CLK_DCC_OFFSET_RANGE/2) ;
            clk_dcc_offset_sign = 1;    
        }       
        
        HWIO_OUTXF2((ca0_ddr_phy_base + DDR_PHY_OFFSET), DDR_PHY_DDRPHY_DCC_TOP_5_CFG, IO_DCC_DQS_OFFSET_MAGNITUDE,
                     IO_DCC_DQS_OFFSET_SIGN, clk_dcc_offset, clk_dcc_offset_sign);

        // Write the data structure with the final value
        training_data_ptr->results.rd_dqdqs.clk_dcc_mag[ch][1] = clk_dcc_offset;  //CA1
        training_data_ptr->results.rd_dqdqs.clk_dcc_sign[ch][1] = clk_dcc_offset_sign;  //CA1
            
        ddr_printf(DDR_NORMAL, "  Channel %d, clk dcc training results CA1_offset =  %d  CA1_sign = %d \n", ch, clk_dcc_offset, clk_dcc_offset_sign);
        
    } //Rank1 loop.
    
}


void DDRSS_RD_Vref_CDC_Coarse_Schmoo (DDR_STRUCT *ddr, 
                                      uint8 ch, 
                                      uint8 cs, 
                                      training_data *training_data_ptr,
                                      training_params_t *training_params_ptr,
                                      best_eye_struct *best_eye_ptr, 
                                      ddrss_rdwr_dqdqs_local_vars *local_vars,
                                      uint32 clk_freq_khz,
                                      uint8  current_clk_inx,
                                      uint8  max_prfs_index)
{
    uint8 coarse_vref_value = 0;
    uint8 coarse_cdc_value  = 0;
    uint8 coarse_cdc_index  = 0;
    
    uint8 byte_lane         = 0;
    uint8 loopcnt           = 0;
    uint8 *compare_result;  
    uint8 compare_result_acc[NUM_DQ_PCH]    = {0};  

    uint8 coarse_dq_error_count[NUM_DQ_PCH] = {0};
    uint32 dq0_ddr_phy_base = 0;
    
    uint8 mp_hp               = 0;
    uint8 vref_min_value      = 0;
    uint8 vref_max_value      = 0;
    uint8 all_bytes_fail_flag = 0;
    
    uint8 coarse_vref_index[NUM_DQ_PCH] = {0};
    uint8 vref_start_value[NUM_DQ_PCH]  = {0};

    uint32 decouple_bit[NUM_DQ_PCH] = {0};
    uint32 ioqual_vref = 0x20;
    
#if DSF_RD_DQDQS_EYE_MASK
    uint8  coarse_vref_mask_min_delta = 0;
    uint8  coarse_vref_mask_max_delta = 0;
    uint8  coarse_vref_mask_start     = 0;
    uint8  coarse_vref_mask_end       = 0;
#endif // #if DSF_RD_DQDQS_EYE_MASK
    
    // Set DQ0 base for addressing
    dq0_ddr_phy_base  = REG_OFFSET_DDR_PHY_CH(ch) + DQ0_DDR_PHY_OFFSET;
    
    // Determine starting VREF and minimum and maximum limits based on MPRX or HPRX
    mp_hp = (clk_freq_khz <= F_RANGE_3) ? MPRX : HPRX;
  
    // Calculate the VREF max value (if not max frequency, use the previously trained VREF)
    vref_max_value = (mp_hp == HPRX) ? 
                    ((current_clk_inx == max_prfs_index) && (cs == 0)) ? 
                         training_params_ptr->rd_dqdqs.coarse_vref_max_hprx 
                       : 0
                   :  training_params_ptr->rd_dqdqs.coarse_vref_max_mprx;

    for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
    {
      // Calculate the VREF start values (if not max frequency, use the previously trained VREF)
      vref_start_value[byte_lane] = (mp_hp == HPRX) ? 
                                ((current_clk_inx == max_prfs_index) && (cs == 0)) ? 
                                        0
                                      : training_data_ptr->results.rd_dqdqs.coarse_vref[mp_hp][ch][0][byte_lane]
                                  :  training_params_ptr->rd_dqdqs.coarse_vref_start_mprx;

        if (mp_hp == HPRX)  
        {    
            // Set decouple bit (RMW).
            decouple_bit[byte_lane] = HWIO_INX ((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), DDR_PHY_DDRPHY_PAD_DQS_1_CFG);
            decouple_bit[byte_lane] = (decouple_bit[byte_lane] | (1 << 1));
            HWIO_OUTX ((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), DDR_PHY_DDRPHY_PAD_DQS_1_CFG, decouple_bit[byte_lane]);
                
            // restore mprx vref to mprx vref reg.                  
            DDR_PHY_hal_cfg_global_vref((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)),
                            ioqual_vref,
                            F_RANGE_3,
                            0);
        }
  
        // Initialize the fail flag and best cdc
        best_eye_ptr[byte_lane].vref_all_fail_flag = 0; // Assume all sane values to begin with.
        best_eye_ptr[byte_lane].best_cdc_value = 0;     // Assume initial value = 0, for initial retimer() setup.
    
    } // byte_lane
    
    memset(local_vars->coarse_schmoo.coarse_dq_passband_info, 
           training_params_ptr->rd_dqdqs.max_loopcnt + 1, 
           NUM_DQ_PCH * COARSE_VREF * COARSE_CDC);
     
    // If HPRX and maximum frequency and Rank 0 or MPRX - Run the full 2 dimensional eye histogram
    // Otherwise the histogram is run with only the trained VREF from the maximum frequency
    for (coarse_vref_value  = 0;
         coarse_vref_value <= vref_max_value; 
         coarse_vref_value += training_params_ptr->rd_dqdqs.coarse_vref_step)  
    {
        // Set starting VREF index 
        for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
        {
          coarse_vref_index[byte_lane] = (coarse_vref_value + vref_start_value[byte_lane])/ training_params_ptr->rd_dqdqs.coarse_vref_step;

          DDR_PHY_hal_cfg_global_vref((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)),
                                       coarse_vref_index[byte_lane],
                                       clk_freq_khz,
                                       cs);
        }

        // Reset all bytes failure flag
        all_bytes_fail_flag = 1;          

        // CDC Schmoo loop
        for(coarse_cdc_value  = training_params_ptr->rd_dqdqs.coarse_cdc_start_value; 
            coarse_cdc_value <= training_params_ptr->rd_dqdqs.coarse_cdc_max_value; 
            coarse_cdc_value += training_params_ptr->rd_dqdqs.coarse_cdc_step)
        {
            coarse_cdc_index = ((coarse_cdc_value - training_params_ptr->rd_dqdqs.coarse_cdc_start_value) / 
                                 training_params_ptr->rd_dqdqs.coarse_cdc_step);
            // Update RD CDC
            for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
            {
                DDR_PHY_hal_cfg_cdc_slave_rd((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                                              coarse_cdc_value, 
                                              0x1, 
                                              0x1, 
                                              cs);   
                compare_result_acc[byte_lane] = 0;
            }

            // Read-back and compare. Fail info given on a byte_lane basis, in an array of 4.
            for (loopcnt = 0;loopcnt < training_params_ptr->rd_dqdqs.max_loopcnt; loopcnt++) 
            {
                compare_result = DDRSS_mem_read_per_byte_phase(ddr, ch, cs, 1, 0x3); //wsa for phase 0 only. 

                // Accumulate the compare results per byte
                for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
                {
                  compare_result_acc[byte_lane] += compare_result[byte_lane];
                  all_bytes_fail_flag &= compare_result[byte_lane];
                }
            }
             
            for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++)
            { 
              // If fail, increment error_count.
              coarse_dq_error_count[byte_lane] = compare_result_acc[byte_lane];
              local_vars->coarse_schmoo.coarse_dq_passband_info[byte_lane][coarse_vref_index[byte_lane]][coarse_cdc_index] = coarse_dq_error_count[byte_lane]; 
            } // byte_lane 
        } // CDC_LOOP
        // Check for condition where all bytes fail (top of eye reached)
        if((all_bytes_fail_flag) && (current_clk_inx == max_prfs_index) && (coarse_vref_value >= training_params_ptr->rd_dqdqs.coarse_vref_low_limit_hprx)) break;
    } // VREF_LOOP

    // Restore IO qualifier local VREF
    if(mp_hp == HPRX)
    {
        for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
        {
            DDR_PHY_hal_cfg_local_vref((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                                        DQS_VREF, 
                                        cs,
                                        10
                                        );
        }
    }
    
    // Construct 2D histogram for the 4 byte lanes: [DQ][VREF][CDC]. 
    for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++)
    {
        DDRSS_Post_Histogram_Coarse_Horizon_Eye (best_eye_ptr, 
                                                 local_vars->coarse_schmoo.coarse_dq_passband_info, 
                                                 training_data_ptr, 
                                                 training_params_ptr, 
                                                 2, // 2 for RD_Training
                                                 byte_lane,
                                                 clk_freq_khz);  

        if(mp_hp == HPRX) 
        {
            best_eye_ptr[byte_lane].best_vref_value = (best_eye_ptr[byte_lane].best_vref_value > training_params_ptr->rd_dqdqs.coarse_vref_low_limit_hprx) ?
                                      best_eye_ptr[byte_lane].best_vref_value : training_params_ptr->rd_dqdqs.coarse_vref_low_limit_hprx;
        }

#if DSF_RD_DQDQS_EYE_MASK
        //----------------------------------------------------------------
        // Determine whether the read eye width meets the minimum criteria
        //----------------------------------------------------------------
        
            // Build coarse_fail_count table: compressed array of coarse_dq_passband_info   
            for(coarse_vref_value = 0;
                coarse_vref_value <= training_params_ptr->rd_dqdqs.coarse_vref_max_value;
                coarse_vref_value += training_params_ptr->rd_dqdqs.coarse_vref_step)
            {
                for(coarse_cdc_value = training_params_ptr->rd_dqdqs.coarse_cdc_start_value;
                    coarse_cdc_value <= training_params_ptr->rd_dqdqs.coarse_cdc_max_value;
                    coarse_cdc_value += training_params_ptr->rd_dqdqs.coarse_cdc_step)
                {
                    if (local_vars->coarse_schmoo.coarse_dq_passband_info[byte_lane][coarse_vref_value][coarse_cdc_value] == 0)
                    {
                        local_vars->coarse_schmoo.coarse_fail_count_table[byte_lane][coarse_vref_value]++;
                    }
                }
            }
     
            coarse_vref_mask_min_delta = (mp_hp == HPRX) ?
                                        training_params_ptr->rd_dqdqs.coarse_hp_vref_mask_min_delta
                                      : training_params_ptr->rd_dqdqs.coarse_mp_vref_mask_min_delta;

            coarse_vref_mask_max_delta = (mp_hp == HPRX) ?
                                        training_params_ptr->rd_dqdqs.coarse_hp_vref_mask_max_delta
                                      : training_params_ptr->rd_dqdqs.coarse_mp_vref_mask_max_delta;

            // Check the two dimensional eye at the maximum frequency        
            if ((mp_hp == MPRX) || ((current_clk_inx == max_prfs_index) && (cs == 0)))
            {
              if ((best_eye_ptr[byte_lane].best_vref_value > coarse_vref_mask_min_delta) &&
                 ((best_eye_ptr[byte_lane].best_vref_value - coarse_vref_mask_min_delta) > vref_min_value))
              {
                  coarse_vref_mask_start = best_eye_ptr[byte_lane].best_vref_value - coarse_vref_mask_min_delta;
              }
              else
              {
                  coarse_vref_mask_start = (mp_hp == HPRX) ?  training_params_ptr->rd_dqdqs.coarse_vref_min_hprx 
                                                           :  training_params_ptr->rd_dqdqs.coarse_vref_min_mprx; 
              }
                                        
              if ((best_eye_ptr[byte_lane].best_vref_value + coarse_vref_mask_min_delta) < training_params_ptr->rd_dqdqs.coarse_vref_max_value)
              { 
                  coarse_vref_mask_end = best_eye_ptr[byte_lane].best_vref_value + coarse_vref_mask_max_delta;
              }
              else
              {
                  coarse_vref_mask_end = (mp_hp == HPRX) ?  training_params_ptr->rd_dqdqs.coarse_vref_max_hprx 
                                                         :  training_params_ptr->rd_dqdqs.coarse_vref_max_mprx;
              }
            }
            else 
            {
              // Check the one dimensional eye at all other frequencies
              coarse_vref_mask_start = training_data_ptr->results.rd_dqdqs.coarse_vref[mp_hp][ch][0][byte_lane];
              coarse_vref_mask_end   = training_data_ptr->results.rd_dqdqs.coarse_vref[mp_hp][ch][0][byte_lane];
            }
                                      
            // Check for minimum eye width
            for (coarse_vref_value  = coarse_vref_mask_start;
                 coarse_vref_value <= coarse_vref_mask_end;
                 coarse_vref_value++)
            {
                if ((best_eye_ptr[byte_lane].best_vref_value == 0xFF) ||
                    (local_vars->coarse_schmoo.coarse_fail_count_table[byte_lane][coarse_vref_value] < training_params_ptr->rd_dqdqs.coarse_cdc_min_width_value))
                {          
                    ddr_printf(DDR_ERROR,"\n");
                    ddr_printf(DDR_ERROR,"    Abort RD coarse due to minimum eye size violation\n");
                    ddr_printf(DDR_ERROR,"    Channel         = %d\n",ch);
                    ddr_printf(DDR_ERROR,"    Rank            = %d\n",cs);
                    ddr_printf(DDR_ERROR,"    Byte            = %d\n",byte_lane);
                    ddr_printf(DDR_ERROR,"    VREF Center     = %d\n",best_eye_ptr[byte_lane].best_vref_value);
                    ddr_printf(DDR_ERROR,"    VREF violation  = %d\n",coarse_vref_value);
                    ddr_printf(DDR_ERROR,"    Width violation = %d\n\n",local_vars->coarse_schmoo.coarse_fail_count_table[byte_lane][coarse_vref_value]);
                    ddr->misc.misc_cfg[0] = 1;
                    ddr_abort();
                }
              //else 
              //{
              //    ddr_printf(DDR_ERROR,"\n");
              //    ddr_printf(DDR_ERROR,"    RD Byte %d Passes Small Eye at %d MHz\n",byte_lane,clk_freq_khz/1000);
              //}
            }
#endif
        
        // Calculate the center of the coarse eye
        best_eye_ptr[byte_lane].best_cdc_value = ((best_eye_ptr[byte_lane].max_eye_left_boundary_cdc_value + 
                                                   best_eye_ptr[byte_lane].max_eye_right_boundary_cdc_value) / 2);    

        // Write the coarse_training CDC values into phy regs
        DDR_PHY_hal_cfg_cdc_slave_rd ((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)),
                                       best_eye_ptr[byte_lane].best_cdc_value, 
                                       1, // 1 for coarse_delay_mode.
                                       1, 
                                       cs);  

//                ddr_printf(DDR_NORMAL, "  Read Byte %u Coarse L<->R Edge: %u <-> %u ps : Width = %u ps\n",
//                  byte_lane,
//                  best_eye_ptr[byte_lane].max_eye_left_boundary_cdc_value * 50,
//                  best_eye_ptr[byte_lane].max_eye_right_boundary_cdc_value * 50,
//                  ((best_eye_ptr[byte_lane].max_eye_right_boundary_cdc_value -
//                   best_eye_ptr[byte_lane].max_eye_left_boundary_cdc_value) +1 )* 50);
    }
    
    if(mp_hp == HPRX)
    {
        // Restore back MPRX Vref value.
        for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++)
        {
            // Restore decouple bit.
            decouple_bit[byte_lane] = (decouple_bit[byte_lane] & 0xFFFFFFFD);
            HWIO_OUTX((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), DDR_PHY_DDRPHY_PAD_DQS_1_CFG, decouple_bit[byte_lane]);
            
            // restore mprx vref to mprx vref reg.             
            DDR_PHY_hal_cfg_global_vref((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)),
                               0x1F,
                               F_RANGE_3,
                               0);
        }
    }
 
}//DDRSS_RD_Vref_CDC_Coarse_Schmoo


void DDRSS_RD_Vref_CDC_Fine_Schmoo (DDR_STRUCT *ddr, 
                                    uint8 ch, 
                                    uint8 cs, 
                                    training_data *training_data_ptr,
                                    training_params_t *training_params_ptr,
                                    uint8 fine_start_vref_value,
                                    uint8 fine_max_vref_value,
                                    uint8 (*rd_boundary_fine_cdc_ptr)[PINS_PER_PHY][FINE_VREF],
                                    uint8 direction, 
                                    ddrss_rdwr_dqdqs_local_vars *local_vars,
                                    uint32 clk_freq_khz,
                                    uint8  current_clk_inx,
                                    uint8  max_prfs_index,
                                    dbi_struct *dbi_struct_ptr)
{
    uint8  fine_vref_value  = 0;
    uint8  fine_cdc_value   = 0;
    uint8  fine_vref_index  = 0;
    uint8  fine_cdc_index   = 0;
    uint8  byte_lane        = 0;
    uint8  bit              = 0;
    uint8  loopcnt          = 0;
    uint32 dq0_ddr_phy_base = 0;

    uint8 *compare_result;
    uint8 *compare_result_DBI = 0;
    uint8 compare_result_acc[NUM_DQ_PCH][PINS_PER_PHY] = {{0}}; 

    // Set DQ0 base for addressing
    dq0_ddr_phy_base  = REG_OFFSET_DDR_PHY_CH(ch) + DQ0_DDR_PHY_OFFSET;

    // Vref Loop
    for (fine_vref_value  = fine_start_vref_value; 
         fine_vref_value <= fine_max_vref_value; 
         fine_vref_value += training_params_ptr->rd_dqdqs.fine_vref_step)  
    {
        fine_vref_index = ((fine_vref_value - fine_start_vref_value) / training_params_ptr->rd_dqdqs.fine_vref_step);

        for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
        {
          if (current_clk_inx == max_prfs_index)
          {
            for (bit = 0; bit < PINS_PER_PHY; bit ++)
            {            
                DDR_PHY_hal_cfg_local_vref (dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET), 
                                            fine_vref_value, 
                                            cs,
                                            bit
                                            );
            }
          }
        }
  
        fine_cdc_value  = training_params_ptr->rd_dqdqs.fine_cdc_start_value; 

        // Fine CDC loop
        for(fine_cdc_value  = training_params_ptr->rd_dqdqs.fine_cdc_start_value; 
            fine_cdc_value <= training_params_ptr->rd_dqdqs.fine_cdc_max_value; 
            fine_cdc_value += training_params_ptr->rd_dqdqs.fine_cdc_step)
        {
            fine_cdc_index = (fine_cdc_value/ training_params_ptr->rd_dqdqs.fine_cdc_step);

            // Update CDC
            for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
            {
                DDR_PHY_hal_cfg_cdc_slave_rd((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)),
                                              fine_cdc_value, 
                                              0x0, 
                                              0x1, 
                                              cs);   // 0 for fine_delay_mode. 

                for (bit = 0; bit < PINS_PER_PHY; bit ++)
                {
                    compare_result_acc [byte_lane][bit] = 0;
                }
            }  
              
            // Read-back and compare. Fail info given on a byte_lane basis, in an array of 4.                 
            for (loopcnt = 0;loopcnt < training_params_ptr->rd_dqdqs.max_loopcnt; loopcnt++) 
            {
               compare_result = DDRSS_mem_read_per_bit_phase(ddr, ch, cs, 1, 1, 5, 0x3);
               
               // When DBI enabled, DBI bit test needs to compare per byte
               if (dbi_struct_ptr->dbi_flag == 1)
               {
                   compare_result_DBI = DDRSS_mem_read_per_byte_phase(ddr, ch, cs, 1, 0x3);
               }
               
               for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
               { 
                   for (bit = 0; bit < PINS_PER_PHY; bit++)  
                   {
                       if (bit == dq_dbi_bit) 
                       {
                            // Accumulate the compare results for DBI bit if DBI is enabled.
                            // To avoid null pointer reference.
                            if (compare_result_DBI != NULL)
                            {
                               compare_result_acc [byte_lane][bit] += compare_result_DBI [byte_lane];
                            }
                       }
                       else   
                       {
                           // Accumulate the compare results for every other bit
                           compare_result_acc [byte_lane][bit] += 
                             compare_result [(byte_lane*PINS_PER_PHY) + bit];
                       }
                   } 
               }
            }
            // Save the results into fine schmoo table 
            for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++)            
            { 
                for (bit = 0; bit < PINS_PER_PHY; bit ++)  
                {
                    // If fail, increment error_count.
                    if(direction == SWEEP_LEFT) 
                    {
                        local_vars->fine_schmoo.fine_rd_dq_passband_info_left[byte_lane][bit][fine_vref_index][fine_cdc_index] = 
                                    compare_result_acc[byte_lane][bit]; 
                    }
                    if(direction == SWEEP_RIGHT) 
                    {
                        local_vars->fine_schmoo.fine_rd_dq_passband_info_right[byte_lane][bit][fine_vref_index][fine_cdc_index] = 
                                    compare_result_acc[byte_lane][bit]; 
                    }
                } //bit loop
            } // byte_lane               
        } // CDC_LOOP 
    } // VREF_LOOP 
    
    // Construct 2D histogram for the all the bits: [DQ][PIN][VREF][CDC]. Indexed [5:2] in the post_histogram function.
    for (byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++)
    {
       for (bit = 0; bit < PINS_PER_PHY; bit ++)
       {  
          if(direction == SWEEP_LEFT) 
          {
              DDRSS_Post_Histogram_RD_Fine_Each_Boundary (local_vars->fine_schmoo.fine_rd_dq_passband_info_left, 
                                                          rd_boundary_fine_cdc_ptr, 
                                                          fine_start_vref_value, 
                                                          fine_max_vref_value, 
                                                          training_data_ptr, 
                                                          training_params_ptr, 
                                                          direction, 
                                                          byte_lane,
                                                          bit); 
          }

          if(direction == SWEEP_RIGHT) 
          {
              DDRSS_Post_Histogram_RD_Fine_Each_Boundary (local_vars->fine_schmoo.fine_rd_dq_passband_info_right, 
                                                          rd_boundary_fine_cdc_ptr, 
                                                          fine_start_vref_value, 
                                                          fine_max_vref_value, 
                                                          training_data_ptr, 
                                                          training_params_ptr, 
                                                          direction, 
                                                          byte_lane,
                                                          bit); 
          }
       }
    }
} // Fine Schmoo



void DDRSS_WR_Vref_CDC_Coarse_Schmoo (DDR_STRUCT *ddr, 
                                      uint8 ch,
                                      uint8 cs,
                                      training_data *training_data_ptr,
                                      training_params_t *training_params_ptr,
                                      best_eye_struct *best_eye_ptr,
                                      ddrss_rdwr_dqdqs_local_vars *local_vars,
                                      uint8 phase,
                                      uint32 clk_freq_khz,
                                      uint8 max_prfs_index)
{
    uint32   dq0_ddr_phy_base  = 0;
    uint8           byte_lane  = 0;
    uint8          loop_count  = 0;
    uint8          MR14_value  = 0;
    uint8 MR14_vref_range_bit  = 0;
    
    uint8   coarse_vref_value  = 0;
    uint8       max_verf_value = 0;
    uint8     start_verf_value = 0;
    uint8     coarse_verf_step = 0;
    uint8    coarse_cdc_value  = 0;
    uint8      fine_cdc_value  = 0;
    
    uint8   coarse_vref_index  = 0;
    uint8    coarse_cdc_index  = 0;
    uint8          vref_index  = 0;
    uint8       max_cdc_value  = 0;
    uint8     temp_vref_start  = 0;
    uint8       temp_vref_max  = 0;
    uint8      temp_vref_step  = 0;

    uint8     *compare_result; 
    uint8     coarse_dq_error_count[NUM_DQ_PCH] = {0};
    
    uint8             clk_idx = 0;
    uint8     current_clk_inx = 0;
    
    uint32    half_cycle_steps = 0;
    uint32    full_cycle_steps = 0;
    uint8 half_cycle_steps_max = 0;
    
    uint8     first_pass[NUM_DQ_PCH] = {0};
    uint8  dq_half_cycle[NUM_DQ_PCH] = {0};
    uint8  dq_full_cycle[NUM_DQ_PCH] = {0};
    uint8    cycle_count[NUM_DQ_PCH] = {0};

    uint8 coarse_wrlvl_delay[NUM_DQ_PCH] = {0};
    uint8 fine_wrlvl_delay  [NUM_DQ_PCH] = {0};
    
#if DSF_WR_DQDQS_EYE_MASK
    uint8  coarse_vref_mask_start     = 0;
    uint8  coarse_vref_mask_end       = 0;
    uint8  coarse_vref_mask_min_delta = 0;
    uint8  coarse_vref_mask_max_delta = 0;
#endif // #if DSF_WR_DQDQS_EYE_MASK

    half_cycle_steps = (((CONVERT_CYC_TO_PS / clk_freq_khz) / 2) / COARSE_STEP_IN_PS);
    full_cycle_steps = ((CONVERT_CYC_TO_PS / clk_freq_khz) / COARSE_STEP_IN_PS);
   
    
    // Set DQ0 base for addressing
    dq0_ddr_phy_base  = REG_OFFSET_DDR_PHY_CH(ch) + DQ0_DDR_PHY_OFFSET;
    // Choose Vref_Range_bit according to frequency threshold.
    MR14_vref_range_bit = (clk_freq_khz > ODT_FSP_EN_FREQ_THRESHOLD) ? 0: 1;
    
    for (clk_idx = (sizeof(freq_range)/sizeof(freq_range[0])); clk_idx > 0; clk_idx--)
    {
       if (clk_freq_khz >= freq_range[clk_idx-1])
          break;
    }
    current_clk_inx = clk_idx + 1;
    
    for (byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
    {
        best_eye_ptr[byte_lane].vref_all_fail_flag = 0;  // Assume all sane values to begin with.
        best_eye_ptr[byte_lane].best_cdc_value = 0;      // Assume initial value = 0, for initial retimer() setup.

        // Initialize the WRLVL delays
        coarse_wrlvl_delay[byte_lane] = training_data_ptr->results.wrlvl.dq_coarse_dqs_delay[current_clk_inx][ch][cs][byte_lane];
        fine_wrlvl_delay  [byte_lane] = training_data_ptr->results.wrlvl.dq_fine_dqs_delay[current_clk_inx][ch][cs][byte_lane];
    }
    
    memset(local_vars->coarse_schmoo.coarse_dq_passband_info, training_params_ptr->wr_dqdqs.max_loopcnt + 1, (NUM_DQ_PCH * COARSE_VREF * COARSE_CDC));
    memset(local_vars->coarse_schmoo.coarse_dq_half_cycle, 0, (NUM_DQ_PCH * COARSE_VREF));
    memset(local_vars->coarse_schmoo.coarse_dq_full_cycle, 0, (NUM_DQ_PCH * COARSE_VREF));
    
    if(current_clk_inx == max_prfs_index) // for 1804MHz only.
    {
        fine_cdc_value = training_params_ptr->wr_dqdqs.fine_cdc_top_freq_start_value;
        max_cdc_value = ((full_cycle_steps + 1) > COARSE_CDC_MAX_VALUE) ? COARSE_CDC_MAX_VALUE : (full_cycle_steps + 1);
        half_cycle_steps_max = (half_cycle_steps + 1);
    }
    else
    {
        fine_cdc_value = training_params_ptr->wr_dqdqs.fine_cdc_start_value;
        max_cdc_value = ((full_cycle_steps + 3) > COARSE_CDC_MAX_VALUE) ? COARSE_CDC_MAX_VALUE : (full_cycle_steps + 3);
        half_cycle_steps_max = (half_cycle_steps + 3);
    }
    //save default training_params values. 
    temp_vref_start = training_params_ptr->wr_dqdqs.coarse_vref_start_value;
    temp_vref_max = training_params_ptr->wr_dqdqs.coarse_vref_max_value;
    temp_vref_step = training_params_ptr->wr_dqdqs.coarse_vref_step; 

    //full Vref range only for top HPRX and MPRX speed.  
    if((current_clk_inx == max_prfs_index) || (clk_freq_khz <= F_RANGE_3))
    {
        start_verf_value = training_params_ptr->wr_dqdqs.coarse_vref_start_value;
        max_verf_value = training_params_ptr->wr_dqdqs.coarse_vref_max_value;
        coarse_verf_step = training_params_ptr->wr_dqdqs.coarse_vref_step;
    }
    else   //for lower frequencies, no Vref sweep is required. 
    {
        start_verf_value = 0;
        max_verf_value = 0;
        coarse_verf_step = 1;
        training_params_ptr->wr_dqdqs.coarse_vref_start_value = 0;
        training_params_ptr->wr_dqdqs.coarse_vref_max_value = 0;
        training_params_ptr->wr_dqdqs.coarse_vref_step =1;
    }
    
    // Start schmoo.
    for (coarse_vref_value  = start_verf_value; 
         coarse_vref_value <= max_verf_value; 
         coarse_vref_value += coarse_verf_step)  
    {
        if((current_clk_inx == max_prfs_index) || (clk_freq_khz <= F_RANGE_3))
        {
            MR14_value = ((MR14_vref_range_bit << 6) | (coarse_vref_value << 0));
            BIMC_MR_Write (CH_1HOT(ch), CS_1HOT(cs), JEDEC_MR_14, MR14_value);
            coarse_vref_index = ((coarse_vref_value - training_params_ptr->wr_dqdqs.coarse_vref_start_value) / training_params_ptr->wr_dqdqs.coarse_vref_step);
        }
        
        for (byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
        {
            first_pass[byte_lane]    = 0;  // Assume first_pass starts from 0 every time we have a new Vref loop
            cycle_count[byte_lane]   = 0;
            dq_half_cycle[byte_lane] = DDR_PHY_hal_sta_wrlvl_dq_half (dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET), cs); 
            dq_full_cycle[byte_lane] = DDR_PHY_hal_sta_wrlvl_dq_full (dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET), cs);
        }
        
  
        for(coarse_cdc_value  = training_params_ptr->wr_dqdqs.coarse_cdc_start_value; 
            coarse_cdc_value <= max_cdc_value; 
            coarse_cdc_value += training_params_ptr->wr_dqdqs.coarse_cdc_step)
        {                                          
            for (byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
            {             
                if((coarse_cdc_value >= half_cycle_steps_max) && (coarse_cdc_value < max_cdc_value) && (cycle_count[byte_lane] < 2) && 
                  ((coarse_dq_error_count[byte_lane] != 0) && (first_pass[byte_lane] != 1)))
                {
                    cycle_count[byte_lane] = cycle_count[byte_lane] + 1;
                    dq_half_cycle[byte_lane]  = dq_half_cycle[byte_lane] + ((cycle_count[byte_lane] >> 0) & 0x1);
                    dq_full_cycle[byte_lane]  = dq_full_cycle[byte_lane] + ((cycle_count[byte_lane] >> 1) & 0x1);
                    coarse_cdc_value = 0; 
                    
                    // Reset first_pass (the first F-to-P transition that occurs) along with CDC.
                    for (byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
                    {
                        first_pass[byte_lane] = 0;
                    }
                }
            }
            
            for (byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
            {
               
                coarse_cdc_index = (coarse_cdc_value / training_params_ptr->wr_dqdqs.coarse_cdc_step);
                
                DDR_PHY_hal_cfg_cdc_slave_wr((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                                               coarse_cdc_value, 
                                               1, 1, cs);   // 1 for coarse_delay_mode. 1 for hp_mode.                

                
                DDR_PHY_hal_cfg_wrlvl_half((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), cs, dq_half_cycle[byte_lane]);                
                DDR_PHY_hal_cfg_wrlvl_full((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), cs, dq_full_cycle[byte_lane]);              
                DDRSS_CDC_Retimer (ddr, 
                                   cs, 
                                   coarse_cdc_value, 
                                   fine_cdc_value, 
                                   coarse_wrlvl_delay[byte_lane],
                                   fine_wrlvl_delay[byte_lane],
                                   (dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                                   clk_freq_khz);  // 0 for fine_delay.                
                
                coarse_dq_error_count[byte_lane] = 0;
            }
            
            for(loop_count = training_params_ptr->wr_dqdqs.max_loopcnt; loop_count > 0 ; loop_count--)
            {                 
                // The write pattern. 
                DDRSS_mem_write(ddr, ch, cs);
                
                // Read-back and compare. Fail info given on a byte_lane basis, in an array of 4.                 
                compare_result = DDRSS_mem_read_per_byte_phase(ddr, ch, cs, 1, phase);

                for (byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
                { 
                    // Accumulate the error for all byte_lanes, based on loop_count number of times.
                    coarse_dq_error_count[byte_lane] += compare_result[byte_lane];
                    
                    // Update a pass flag to keep track to the first pass that occured.
                    if (coarse_dq_error_count[byte_lane] == 0)
                    {
                        first_pass[byte_lane] = 1;
                    }
                }
            } // LOOP_COUNT 
         
            for (byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
            {    
                // Populate the histogram every vref and cdc with the result found.
                local_vars->coarse_schmoo.coarse_dq_passband_info[byte_lane][coarse_vref_index][coarse_cdc_index] = coarse_dq_error_count[byte_lane]; 
            }
        }  //CDC loop    

        for (byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
        { 
            // Track half and hull cycle info, per vref.
            local_vars->coarse_schmoo.coarse_dq_half_cycle[byte_lane][coarse_vref_index] = dq_half_cycle[byte_lane]; 
            local_vars->coarse_schmoo.coarse_dq_full_cycle[byte_lane][coarse_vref_index] = dq_full_cycle[byte_lane];            
        }
    } // VREF_LOOP
    
    
    // Construct 2D histogram for the 4 bytelanes: [DQ][VREF][CDC]. 
    for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++)
    {
        DDRSS_Post_Histogram_Coarse_Horizon_Eye (best_eye_ptr, local_vars->coarse_schmoo.coarse_dq_passband_info, 
                                                 training_data_ptr, training_params_ptr, TRAINING_TYPE_WR_DQDQS, byte_lane, clk_freq_khz);

#if DSF_WR_DQDQS_EYE_MASK
        //----------------------------------------------------------------
        // Determine whether the write eye width meets the minimum criteria
        //----------------------------------------------------------------
        
        //if((current_clk_inx == max_prfs_index) && (phase == 3))
        if(phase == 3)
        {
            // Build coarse_fail_count table: compressed array of coarse_dq_passband_info     
            for(coarse_vref_value  = training_params_ptr->wr_dqdqs.coarse_vref_start_value; 
                coarse_vref_value <= training_params_ptr->wr_dqdqs.coarse_vref_max_value; 
                coarse_vref_value += training_params_ptr->wr_dqdqs.coarse_vref_step)
            {
                vref_index = coarse_vref_value - training_params_ptr->wr_dqdqs.coarse_vref_start_value;
                
                for(coarse_cdc_value  = training_params_ptr->wr_dqdqs.coarse_cdc_start_value; 
                    coarse_cdc_value <= max_cdc_value; 
                    coarse_cdc_value += training_params_ptr->wr_dqdqs.coarse_cdc_step)
                {            
                    if (local_vars->coarse_schmoo.coarse_dq_passband_info[byte_lane][vref_index][coarse_cdc_value] == 0)
                    {
                        local_vars->coarse_schmoo.coarse_fail_count_table[byte_lane][vref_index]++;
                    }
                }
            }
        
            coarse_vref_mask_min_delta = (MR14_vref_range_bit == 1) ? 
                                            training_params_ptr->wr_dqdqs.coarse_MR14_1_vref_mask_min_delta
                                        : training_params_ptr->wr_dqdqs.coarse_MR14_0_vref_mask_min_delta;
            coarse_vref_mask_max_delta = (MR14_vref_range_bit == 1) ? 
                                            training_params_ptr->wr_dqdqs.coarse_MR14_1_vref_mask_max_delta
                                        : training_params_ptr->wr_dqdqs.coarse_MR14_0_vref_mask_max_delta;
            
            if (( best_eye_ptr[byte_lane].best_vref_value > coarse_vref_mask_min_delta) &&
            ((best_eye_ptr[byte_lane].best_vref_value - coarse_vref_mask_min_delta) > training_params_ptr->wr_dqdqs.coarse_vref_start_value))
            {
                coarse_vref_mask_start = best_eye_ptr[byte_lane].best_vref_value - coarse_vref_mask_min_delta;
            }
            else 
            {
                coarse_vref_mask_start = training_params_ptr->wr_dqdqs.coarse_vref_start_value;
            }
            if ( (best_eye_ptr[byte_lane].best_vref_value + coarse_vref_mask_min_delta) < training_params_ptr->wr_dqdqs.coarse_vref_max_value)
            {
                coarse_vref_mask_end = best_eye_ptr[byte_lane].best_vref_value + coarse_vref_mask_max_delta;
            }
            else 
            {
                coarse_vref_mask_end = training_params_ptr->wr_dqdqs.coarse_vref_max_value;
            }
            
            // Check for minimum eye width
            for (coarse_vref_value  = coarse_vref_mask_start;
                coarse_vref_value <= coarse_vref_mask_end;
                coarse_vref_value++)
            {
                vref_index = coarse_vref_value - training_params_ptr->wr_dqdqs.coarse_vref_start_value;
                if ((best_eye_ptr[byte_lane].best_vref_value == 0xFF) ||
                    (local_vars->coarse_schmoo.coarse_fail_count_table[byte_lane][vref_index] <  
                                            training_params_ptr->wr_dqdqs.coarse_cdc_min_width_value))
                {          
                    ddr_printf(DDR_ERROR,"\n");
                    ddr_printf(DDR_ERROR,"    Abort WR coarse due to minimum eye size violation\n");
                    ddr_printf(DDR_ERROR,"    Channel         = %d\n",ch);
                    ddr_printf(DDR_ERROR,"    Rank            = %d\n",cs);
                    ddr_printf(DDR_ERROR,"    Byte            = %d\n",byte_lane);
                    ddr_printf(DDR_ERROR,"    VREF Center     = %d\n",best_eye_ptr[byte_lane].best_vref_value);
                    ddr_printf(DDR_ERROR,"    VREF violation  = %d\n",vref_index);
                    ddr_printf(DDR_ERROR,"    Width violation = %d\n\n",local_vars->coarse_schmoo.coarse_fail_count_table[byte_lane][coarse_vref_value]);
                    ddr->misc.misc_cfg[0] = 1;
                    ddr_abort();
                }
              //else 
              //{
              //    ddr_printf(DDR_ERROR,"\n");
              //    ddr_printf(DDR_ERROR,"    WR Byte %d Passes Small Eye at %d MHz\n",byte_lane,clk_freq_khz/1000);
              //}
            }
        }
#endif
               
        // Process best cdc value.
        best_eye_ptr[byte_lane].best_cdc_value = ((best_eye_ptr[byte_lane].max_eye_left_boundary_cdc_value + best_eye_ptr[byte_lane].max_eye_right_boundary_cdc_value) / 2);  
        
        if(best_eye_ptr[byte_lane].bottom_max_eye_vref_value != 0xFF)
        {
            vref_index = ((best_eye_ptr[byte_lane].bottom_max_eye_vref_value - training_params_ptr->wr_dqdqs.coarse_vref_start_value) / training_params_ptr->wr_dqdqs.coarse_vref_step);        
        }            
        
        dq_half_cycle[byte_lane] = local_vars->coarse_schmoo.coarse_dq_half_cycle[byte_lane][vref_index];
        dq_full_cycle[byte_lane] = local_vars->coarse_schmoo.coarse_dq_full_cycle[byte_lane][vref_index];
        
//        ddr_printf(DDR_NORMAL, "  Write Byte %u Coarse L<->R Edge: %u <-> %u ps : Width = %u ps\n",
//                                byte_lane,
//                                best_eye_ptr[byte_lane].max_eye_left_boundary_cdc_value * 50,
//                                best_eye_ptr[byte_lane].max_eye_right_boundary_cdc_value * 50,
//                                ((best_eye_ptr[byte_lane].max_eye_right_boundary_cdc_value -
//                                best_eye_ptr[byte_lane].max_eye_left_boundary_cdc_value) +1 )* 50);

        // Write the coarse_training values into phy regs and training_params. This is necessary in case we stop with coarse training.    
        DDR_PHY_hal_cfg_cdc_slave_wr((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), best_eye_ptr[byte_lane].best_cdc_value, 1, 1, cs);  // 1 for coarse_delay_mode. 1 for hp_mode. respectively.

        training_data_ptr->results.wr_dqdqs.dq_half_cycle[current_clk_inx][ch][cs][byte_lane] = dq_half_cycle[byte_lane];
        training_data_ptr->results.wr_dqdqs.dq_full_cycle[current_clk_inx][ch][cs][byte_lane] = dq_full_cycle[byte_lane];
        
        DDR_PHY_hal_cfg_wrlvl_half((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), cs, dq_half_cycle[byte_lane]);                
        DDR_PHY_hal_cfg_wrlvl_full((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), cs, dq_full_cycle[byte_lane]); 
        
        DDRSS_CDC_Retimer (ddr, 
                           cs, 
                           best_eye_ptr[byte_lane].best_cdc_value, 
                           fine_cdc_value, 
                           coarse_wrlvl_delay[byte_lane],
                           fine_wrlvl_delay[byte_lane],
                           (dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                           clk_freq_khz);  // 0 for fine_delay.
    }

    //restore Vref training params values 
    training_params_ptr->wr_dqdqs.coarse_vref_start_value = temp_vref_start;
    training_params_ptr->wr_dqdqs.coarse_vref_max_value = temp_vref_max;
    training_params_ptr->wr_dqdqs.coarse_vref_step = temp_vref_step;    
 
}


void DDRSS_WR_CDC_1D_Fine_Schmoo (DDR_STRUCT *ddr, 
                                  uint8 ch, 
                                  uint8 cs, 
                                  training_data *training_data_ptr,
                                  training_params_t *training_params_ptr, 
                                  ddrss_rdwr_dqdqs_local_vars *local_vars,
                                  best_eye_struct *best_eye_ptr,
                                  uint8 *coarse_cdc_value,
                                  uint32 clk_freq_khz,
                                  uint8 max_prfs_index,
                                  uint8 direction /*0 for Cl and 1 for Cr*/)
{
    uint32   dq0_ddr_phy_base = 0;
    uint8           byte_lane = 0;
    uint8          loop_count = 0;
    uint8      fine_cdc_value = 0;
    uint8  fine_cdc_max_value = 0;
    uint8      fine_cdc_index = 0;
    uint8               index = 0; 
    uint8             clk_idx = 0;
    uint8     current_clk_inx = 0;
    
    uint8     *compare_result;
    
    uint8             dq_error_count[NUM_DQ_PCH] = {0};
    uint8      left_boundary_eye_cdc_value[NUM_DQ_PCH];// = {0};
    uint8     right_boundary_eye_cdc_value[NUM_DQ_PCH];// = {0};
    uint8               coarse_wrlvl_delay[NUM_DQ_PCH] = {0};
    uint8               fine_wrlvl_delay  [NUM_DQ_PCH] = {0};

    // Set DQ0 base for addressing
    dq0_ddr_phy_base  = REG_OFFSET_DDR_PHY_CH(ch) + DQ0_DDR_PHY_OFFSET;
    
    for (clk_idx = (sizeof(freq_range)/sizeof(freq_range[0])); clk_idx > 0; clk_idx--)
    {
       if (clk_freq_khz >= freq_range[clk_idx-1])
          break;
    }
    current_clk_inx = clk_idx + 1; 
    
    for (byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
    {      
        // Initialize the WRLVL delays
        coarse_wrlvl_delay[byte_lane] = training_data_ptr->results.wrlvl.dq_coarse_dqs_delay[current_clk_inx][ch][cs][byte_lane];
        fine_wrlvl_delay  [byte_lane] = training_data_ptr->results.wrlvl.dq_fine_dqs_delay[current_clk_inx][ch][cs][byte_lane];

        if(direction == 0)
        {
            // Retain these values.
            best_eye_ptr[byte_lane].best_fine_cdc_value = 0;
            left_boundary_eye_cdc_value[byte_lane]  = 0xFF;
        }
        right_boundary_eye_cdc_value[byte_lane] = 0xFF;
    }

    memset(local_vars->fine_schmoo.fine_dq_1D_passband_info, training_params_ptr->wr_dqdqs.max_loopcnt + 1, (NUM_DQ_PCH * (2 * FINE_CDC)));

    fine_cdc_max_value = training_params_ptr->wr_dqdqs.fine_cdc_max_value;

    
    for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
    {
        // Update Coarse CDC value.
        DDR_PHY_hal_cfg_cdc_slave_wr((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                                        coarse_cdc_value[byte_lane], 
                                        1, 1, cs);   // 1 for coarse_delay_mode. 1 for hp_mode.

        // Fine CDC Schmoo loop
        for(fine_cdc_value  = training_params_ptr->wr_dqdqs.fine_cdc_start_value;
            fine_cdc_value <= fine_cdc_max_value;
            fine_cdc_value += training_params_ptr->wr_dqdqs.fine_cdc_step)
        {
            // Update Fine CDC
            DDR_PHY_hal_cfg_cdc_slave_wr((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                                           fine_cdc_value, 
                                           0, 1, cs);   // 0 for fine_delay_mode. 1 for hp_mode.  

            DDRSS_CDC_Retimer (ddr, 
                               cs, 
                               coarse_cdc_value[byte_lane],  
                               fine_cdc_value, 
                               coarse_wrlvl_delay[byte_lane],
                               fine_wrlvl_delay[byte_lane],
                               (dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                               clk_freq_khz);  // 0 for fine_delay.
            
            fine_cdc_index = (fine_cdc_value / training_params_ptr->wr_dqdqs.fine_cdc_step);
            dq_error_count[byte_lane] = 0;
            
            for(loop_count = training_params_ptr->wr_dqdqs.max_loopcnt; loop_count > 0 ; loop_count--)
            {                 
                // The write pattern. 
                DDRSS_mem_write(ddr, ch, cs);
                
                // Read-back and compare. Fail info given on a byte_lane basis, in an array of 4.                 
                compare_result = DDRSS_mem_read_per_byte_phase(ddr, ch, cs, 1, 3);
                
                // Accumulate the error for all byte_lanes, based on loop_count number of times.
                dq_error_count[byte_lane] += compare_result[byte_lane];    
            }
        
            // Populate the histogram every coarase and fine cdc with the result found.
            local_vars->fine_schmoo.fine_dq_1D_passband_info[byte_lane][fine_cdc_index] = dq_error_count[byte_lane];
        }

        //------------------Find the fine boundary.
        for(index = 0; index <= fine_cdc_max_value; index++)
        {
            // If pass.
            if(direction == 1)
            {
                // If first point is already a FAIL.
                if(local_vars->fine_schmoo.fine_dq_1D_passband_info[byte_lane][0] != 0)
                {
                    right_boundary_eye_cdc_value[byte_lane] = 0;
                }
                
                if(local_vars->fine_schmoo.fine_dq_1D_passband_info[byte_lane][index] == 0)
                {
                    right_boundary_eye_cdc_value[byte_lane] = index;
                }
        
                // Current point is FAIL and previous point is PASS.
                else if ((index >= 1) && (local_vars->fine_schmoo.fine_dq_1D_passband_info[byte_lane][index-1] == 0) &&
                                            (local_vars->fine_schmoo.fine_dq_1D_passband_info[byte_lane][index] != 0))
                {
                    right_boundary_eye_cdc_value[byte_lane] = index - 1;
                }
            }
            
            // For all frequencies except 1804, find the left fine edge. F->P transition.
            if(direction == 0)
            {
                if(local_vars->fine_schmoo.fine_dq_1D_passband_info[byte_lane][index] == 0)
                {
                    left_boundary_eye_cdc_value[byte_lane] = index;
                    break;
                }
            }            
        }

        // Calculate the center Fine value: ((Fl + Fr)/2).
        if(direction == 0)
        {
            best_eye_ptr[byte_lane].best_fine_cdc_value = left_boundary_eye_cdc_value[byte_lane];
        }
        
        if(direction == 1) // Calculate center only after right edge has also been found.
        {
            best_eye_ptr[byte_lane].best_fine_cdc_value = ((best_eye_ptr[byte_lane].best_fine_cdc_value + right_boundary_eye_cdc_value[byte_lane]) / 2);
            
             // For 1804 only, limit fine delay to range from 5-10.
            if(current_clk_inx == max_prfs_index)
            {
                if(best_eye_ptr[byte_lane].best_fine_cdc_value < WRITE_FINE_CDC_MIN) //4 and less
                {
                    best_eye_ptr[byte_lane].best_fine_cdc_value = (best_eye_ptr[byte_lane].best_fine_cdc_value + FINE_STEPS_PER_COARSE);
                    training_data_ptr->results.wr_dqdqs.coarse_cdc[current_clk_inx][ch][cs][byte_lane] = (training_data_ptr->results.wr_dqdqs.coarse_cdc[current_clk_inx][ch][cs][byte_lane] - 1);
                }
                else if(best_eye_ptr[byte_lane].best_fine_cdc_value > WRITE_FINE_CDC_MAX) //11 and more	
                {
                    best_eye_ptr[byte_lane].best_fine_cdc_value = (best_eye_ptr[byte_lane].best_fine_cdc_value - FINE_STEPS_PER_COARSE);
                    training_data_ptr->results.wr_dqdqs.coarse_cdc[current_clk_inx][ch][cs][byte_lane] = (training_data_ptr->results.wr_dqdqs.coarse_cdc [current_clk_inx][ch][cs][byte_lane] + 1);
                }
            }
        }
        
        // Write the center value found into the data structure.
        training_data_ptr->results.wr_dqdqs.fine_cdc [current_clk_inx][ch][cs][byte_lane] = best_eye_ptr[byte_lane].best_fine_cdc_value;
    }
    
}


void DDRSS_WR_CDC_1D_Schmoo (DDR_STRUCT *ddr, 
                             uint8 ch, 
                             uint8 cs, 
                             training_data *training_data_ptr,
                             training_params_t *training_params_ptr, 
                             ddrss_rdwr_dqdqs_local_vars *local_vars,
                             best_eye_struct *best_eye_ptr,
                             uint8 *coarse_cdc_left_start_value,
                             uint8 *coarse_cdc_right_start_value,
                             uint32 clk_freq_khz,
                             uint8 max_prfs_index)
{
    uint32   dq0_ddr_phy_base = 0;
    uint8           byte_lane = 0;
    uint8          loop_count = 0;
    uint8    coarse_cdc_value = 0;
    uint8    coarse_cdc_index = 0;
    uint8             clk_idx = 0;
    uint8     current_clk_inx = 0;
    uint8     coarse_odd_flag = 0;
    uint8               index = 0;
    uint8          first_pass = 0;
    
    uint8     *compare_result;

    uint8            coarse_dq_error_count[NUM_DQ_PCH] = {0};
    uint8     left_boundary_eye_cdc_value [NUM_DQ_PCH] = {0};
    uint8    right_boundary_eye_cdc_value [NUM_DQ_PCH] = {0};
    uint8    coarse_wrlvl_delay           [NUM_DQ_PCH] = {0};
    uint8      fine_wrlvl_delay           [NUM_DQ_PCH] = {0};

    // Set DQ0 base for addressing
    dq0_ddr_phy_base  = REG_OFFSET_DDR_PHY_CH(ch) + DQ0_DDR_PHY_OFFSET;
    
    for (clk_idx = (sizeof(freq_range)/sizeof(freq_range[0])); clk_idx > 0; clk_idx--)
    {
       if (clk_freq_khz >= freq_range[clk_idx-1])
          break;
    }
    current_clk_inx = clk_idx + 1; 
    
    for (byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
    {
        // Initialize the WRLVL delays
        coarse_wrlvl_delay[byte_lane] = training_data_ptr->results.wrlvl.dq_coarse_dqs_delay[current_clk_inx][ch][cs][byte_lane];
        fine_wrlvl_delay[byte_lane]   = training_data_ptr->results.wrlvl.dq_fine_dqs_delay[current_clk_inx][ch][cs][byte_lane];

        best_eye_ptr[byte_lane].best_cdc_value  = 0;
        left_boundary_eye_cdc_value[byte_lane]  = 0xFF;
        right_boundary_eye_cdc_value[byte_lane] = 0xFF;
    }
    
    memset(local_vars->coarse_schmoo.coarse_dq_1D_passband_info, training_params_ptr->wr_dqdqs.max_loopcnt + 1, NUM_DQ_PCH * COARSE_CDC);
    

    for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
    {
        // Update Fine_CDC value.
        DDR_PHY_hal_cfg_cdc_slave_wr((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                                       training_params_ptr->wr_dqdqs.fine_cdc_start_value, 
                                       0, 1, cs);   // 0 for fine_delay_mode. 1 for hp_mode.

        // CDC Schmoo loop
        for(coarse_cdc_value  = coarse_cdc_left_start_value[byte_lane]; 
            coarse_cdc_value <= coarse_cdc_right_start_value[byte_lane]; /*This limit would be enough. */
            coarse_cdc_value += training_params_ptr->wr_dqdqs.coarse_cdc_step)
        {           
            // Update coarse CDC
            DDR_PHY_hal_cfg_cdc_slave_wr((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                                           coarse_cdc_value, 
                                           1, 1, cs);   // 1 for coarse_delay_mode. 1 for hp_mode.

            DDRSS_CDC_Retimer (ddr, 
                               cs, 
                               coarse_cdc_value, 
                               training_params_ptr->wr_dqdqs.fine_cdc_start_value, 
                               coarse_wrlvl_delay[byte_lane],
                               fine_wrlvl_delay[byte_lane],
                               (dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                               clk_freq_khz);  // 0 for fine_delay.

            coarse_cdc_index = (coarse_cdc_value / training_params_ptr->wr_dqdqs.coarse_cdc_step);
            coarse_dq_error_count[byte_lane] = 0;
            first_pass = 0;
            
            for(loop_count = training_params_ptr->wr_dqdqs.max_loopcnt; loop_count > 0 ; loop_count--)
            {                 
                // The write pattern. 
                DDRSS_mem_write(ddr, ch, cs);
                
                // Read-back and compare. Fail info given on a byte_lane basis, in an array of 4.                 
                compare_result = DDRSS_mem_read_per_byte_phase(ddr, ch, cs, 1, 3);
                
                // Accumulate the error for all byte_lanes, based on loop_count number of times.
                coarse_dq_error_count[byte_lane] += compare_result[byte_lane];
    
            }
        
            // Populate the histogram every coarase and fine cdc with the result found.
            local_vars->coarse_schmoo.coarse_dq_1D_passband_info[byte_lane][coarse_cdc_index] = coarse_dq_error_count[byte_lane];
        }                       

        //------------------Find the right coarse boundary.
        for(index = 0; index <= coarse_cdc_right_start_value[byte_lane]; index++)
        {
            if (local_vars->coarse_schmoo.coarse_dq_1D_passband_info[byte_lane][index] == 0)   // When the current point is PASS
            {                
                // Record the first PASS point as left boundary   
                if(first_pass == 0)
                {
                    left_boundary_eye_cdc_value[byte_lane] = index;
                    first_pass = 1;
                }
                
                // If the max_cdc point is PASS, record it as right boundary.
                if (index == coarse_cdc_right_start_value[byte_lane])
                {
                    right_boundary_eye_cdc_value[byte_lane] = index;
                }
            }
    
            // Current point is FAIL and previous point is PASS
            else if (index >= 1 && local_vars->coarse_schmoo.coarse_dq_1D_passband_info[byte_lane][index-1] == 0)
            {
                right_boundary_eye_cdc_value[byte_lane] = index - 1;
            }
        }
        
        // Subtract the left boundary by 1 so that we always start at a passing point.
        left_boundary_eye_cdc_value[byte_lane] -= 1;
        
        // If center value is odd number, subtract right_boundary by 1 so that the center value is ALWAYS an even number.
        coarse_odd_flag = ((left_boundary_eye_cdc_value[byte_lane] + right_boundary_eye_cdc_value[byte_lane]) % 2);
        if(coarse_odd_flag == 1)
        {
            right_boundary_eye_cdc_value[byte_lane] -= 1;
        }
        
        // Calculate the center value for Coarse. ((Cl+Cr)/2).
        best_eye_ptr[byte_lane].best_cdc_value = ((left_boundary_eye_cdc_value[byte_lane] + right_boundary_eye_cdc_value[byte_lane]) / 2);
        
        training_data_ptr->results.wr_dqdqs.coarse_max_eye_left_boundary_cdc_value[current_clk_inx][ch][cs][byte_lane] = left_boundary_eye_cdc_value[byte_lane];
        training_data_ptr->results.wr_dqdqs.coarse_max_eye_right_boundary_cdc_value[current_clk_inx][ch][cs][byte_lane] = right_boundary_eye_cdc_value[byte_lane];  
        
        // Write the value found into the data structure.
        training_data_ptr->results.wr_dqdqs.coarse_cdc [current_clk_inx][ch][cs][byte_lane] = best_eye_ptr[byte_lane].best_cdc_value;
    }

    // Calculate the fine value in the vicinity of Cl.
    DDRSS_WR_CDC_1D_Fine_Schmoo (ddr, ch, cs, training_data_ptr, training_params_ptr, 
                                    local_vars, best_eye_ptr,
                                    left_boundary_eye_cdc_value,
                                    clk_freq_khz, max_prfs_index, 0);  /* 0 for direction Cl. */                                      

    // Calculate the fine value in the vicinity of Cr.
    DDRSS_WR_CDC_1D_Fine_Schmoo (ddr, ch, cs, training_data_ptr, training_params_ptr, 
                                    local_vars, best_eye_ptr,
                                    right_boundary_eye_cdc_value,
                                    clk_freq_khz, max_prfs_index, 1);  /* 1 for direction Cr. */


}


//================================================================================================//
//  Function: DDRSS_rd_dqdqs_dcc_schmoo().
//  Brief description: This function performs dcc schmoo @1555MHz per each CS and
//                       sets the average DCC for both the CS's during read training.
//                       For a fixed vref value, the function performs cdc schmoo for
//                       all the DCC settings.
//================================================================================================//
void DDRSS_wr_pbit_schmoo (DDR_STRUCT *ddr, 
                           uint8 ch, 
                           uint8 cs, 
                           training_params_t *training_params_ptr, 
                           ddrss_rdwr_dqdqs_local_vars *local_vars,
                           uint8 *coarse_cdc_left_start_value,
                           uint32 clk_freq_khz)
{
    uint8 byte_lane                    = 0;
    uint32 dq0_ddr_phy_base            = 0;
    uint8  bit                         = 0;
    uint8  loopcnt                     = 0;
    int8   perbit                      = 0;
    int8  perbit_boundary              = 0;
    uint8  clk_idx                     = 0;
    uint8  current_clk_inx             = 0;

    uint8 *compare_result;
    uint8 *compare_result_DBI = 0; 
    uint8 compare_result_acc[NUM_DQ_PCH][PINS_PER_PHY] = {{0}};     
    uint8 perbit_cdc[NUM_DQ_PCH][PINS_PER_PHY]         = {{0}};
    uint8    coarse_wrlvl_delay           [NUM_DQ_PCH] = {0};
    uint8      fine_wrlvl_delay           [NUM_DQ_PCH] = {0};

    dbi_struct dbi_struct_info;
    uint32 reg_offset_dpe =  0;

    // Training data structure pointer
    training_data *training_data_ptr;
    training_data_ptr = (training_data *)(&ddr->flash_params.training_data);
    
    // Set DBI flag to indicate Enable DBI or not
    reg_offset_dpe = REG_OFFSET_DPE (ch);    
    
    // Set DQ0 base for addressing
    dq0_ddr_phy_base    = REG_OFFSET_DDR_PHY_CH(ch) + DQ0_DDR_PHY_OFFSET;
    dbi_struct_info.dbi_flag = HWIO_INXF (reg_offset_dpe, DPE_CONFIG_9, DBI_WR);

    for (clk_idx = (sizeof(freq_range)/sizeof(freq_range[0])); clk_idx > 0; clk_idx--)
    {
       if (clk_freq_khz >= freq_range[clk_idx-1])
          break;
    }
    current_clk_inx = clk_idx + 1;
       
    // Initialize the fail flag, best cdc, coarse and fine 
    for (byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
    {   
        DDR_PHY_hal_cfg_cdc_slave_wr((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                                       coarse_cdc_left_start_value[byte_lane],
                                       1, 
                                       1, 
                                       cs);  // 1 for coarse_delay_mode. 1 for hp_mode.

        // Initialize the WRLVL delays
        coarse_wrlvl_delay[byte_lane] = training_data_ptr->results.wrlvl.dq_coarse_dqs_delay[current_clk_inx][ch][cs][byte_lane];
        fine_wrlvl_delay[byte_lane]   = training_data_ptr->results.wrlvl.dq_fine_dqs_delay[current_clk_inx][ch][cs][byte_lane];

        DDRSS_CDC_Retimer (ddr, 
                           cs, 
                           coarse_cdc_left_start_value[byte_lane],
                           training_params_ptr->wr_dqdqs.fine_cdc_top_freq_start_value,
                           coarse_wrlvl_delay[byte_lane],
                           fine_wrlvl_delay[byte_lane],
                           (dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                           clk_freq_khz);                                  
    }
    
    //Reset perbit_cdc
    for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
    {        
        perbit_boundary = 0;

        for(bit = 0; bit < PINS_PER_PHY; bit++)
        {
            perbit_cdc [byte_lane][bit] = 0x0;
        }

        for (perbit = PERBIT_CDC_MAX_VALUE; perbit >= 0; perbit--)
        {
            for(bit = 0; bit < PINS_PER_PHY; bit++)  //PHY bit
            {
                if (bit != dq_dbi_bit)  //don't sweep DBI pin yet, otherwise it will mess up with other pbit results
                {
                    // Sweep all of the perbit at once
                    DDR_PHY_hal_cfg_pbit_dq_delay((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                                                   bit, 
                                                   1,   // 1 for TX.
                                                   cs, 
                                                   perbit);
                }
                else   //set DBI per-bit CDC to max to make sure it passes. 
                {
                    DDR_PHY_hal_cfg_pbit_dq_delay((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                                                   dq_dbi_bit, 
                                                   1,   // 1 for TX.
                                                   cs, 
                                                   PERBIT_CDC_MAX_VALUE);
                }
            }
            
            // Reset the compare results
            for (bit = 0; bit < PINS_PER_PHY; bit++)
            {
                compare_result_acc[byte_lane][bit]  = 0x0;
            }
        
            for(loopcnt = training_params_ptr->wr_dqdqs.max_loopcnt; loopcnt > 0 ; loopcnt--)
            {         
                // The write pattern. 
                DDRSS_mem_write(ddr, ch, cs); 

                compare_result = DDRSS_mem_read_per_bit_phase(ddr, ch, cs, 1, 1, 5, 0x0);
        
                for (bit = 0; bit < PINS_PER_PHY; bit++)
                {
                    // fine training accumulated results for every other bit.
                    compare_result_acc [byte_lane][bit] += 
                    compare_result [(byte_lane*PINS_PER_PHY) + bit];                        
                }
            }
            
            for (bit = 0; bit < PINS_PER_PHY; bit++)
            {
                if (bit != dq_dbi_bit)
                {
                    if ((compare_result_acc[byte_lane][bit] != 0x0) && (perbit >= perbit_cdc[byte_lane][bit]))
                    { 
                        // perbit_cdc is in BIMC order
                        perbit_cdc[byte_lane][bit] = (perbit + 1);  // last passing point
                        
                        // Limit max Perbit value.
                        if (perbit_cdc[byte_lane][bit] > 0xF)
                        {
                            perbit_cdc[byte_lane][bit] = perbit;
                        }
                        // Flag set if boundary condition met.
                        if (perbit_cdc[byte_lane][bit] == 0xF)
                        {
                            perbit_boundary  = (perbit_boundary + 1);
                        }
                    }   
                }
            } // bit loop
        } // perbit loop
        
        if (perbit_boundary > 0) // perbit value for DQ is close to 0xF, need to increase coarse CDC to avoid boundary condition.
        {
            DDR_PHY_hal_cfg_cdc_slave_wr((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                                (coarse_cdc_left_start_value[byte_lane] + 1),
                                1, 
                                1, 
                                cs);  // 1 for coarse_delay_mode. 1 for hp_mode.
                                
            DDRSS_CDC_Retimer (ddr, 
                            cs, 
                            coarse_cdc_left_start_value[byte_lane] + 1,
                            training_params_ptr->wr_dqdqs.fine_cdc_top_freq_start_value,
                            coarse_wrlvl_delay[byte_lane],
                            fine_wrlvl_delay[byte_lane],
                            (dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                            clk_freq_khz);   
    
            byte_lane --;
        }
    } // byte loop

    //DBI training separately
    // Train the perbit to have at least one fail
    if (dbi_struct_info.dbi_flag == 1)
    {
        for(byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
        {
            for(bit = 0; bit < PINS_PER_PHY; bit++)  //PHY bit
            {
                if (bit != dq_dbi_bit)  //don't sweep DBI pin yet, otherwise it will mess up with other pbit results
                {
                    // reset rest of per-bit to max value to make sure they all pass. 
                    DDR_PHY_hal_cfg_pbit_dq_delay((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                                                   bit, 
                                                   1,   // 1 for TX.
                                                   cs, 
                                                   PERBIT_CDC_MAX_VALUE);
                }
            }
                
            for (perbit = PERBIT_CDC_MAX_VALUE; perbit>=0; perbit--)
            {
                // Sweep all of the perbit at once
                DDR_PHY_hal_cfg_pbit_dq_delay((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                                               dq_dbi_bit, 
                                               1,   // 1 for TX.
                                               cs, 
                                               perbit);
                // Reset the compare results
                for (bit = 0; bit < PINS_PER_PHY; bit++)
                {
                    compare_result_acc[byte_lane][dq_dbi_bit]  = 0x0;
                }
        
                for(loopcnt = training_params_ptr->wr_dqdqs.max_loopcnt; loopcnt > 0 ; loopcnt--)
                {         
                    // The write pattern. 
                    DDRSS_mem_write(ddr, ch, cs); 
                    
                    // Clear the compare results
                    compare_result_DBI = DDRSS_mem_read_per_byte_DBI_phase(ddr, ch, cs, 5, 0x0);
                    
                    if (compare_result_DBI != NULL)
                    {
                        compare_result_acc [byte_lane][dq_dbi_bit] += 
                        compare_result_DBI [byte_lane];
                    }
                }
    
                if ((compare_result_acc[byte_lane][dq_dbi_bit] != 0x0) && (perbit >= perbit_cdc[byte_lane][dq_dbi_bit]))
                { 
                    // perbit_cdc is in BIMC order
                    perbit_cdc[byte_lane][dq_dbi_bit] = (perbit + 1 + DBI_WR_PBIT_OFFSET); //last passing point with offset.
                    
                    // Limit max perbit value.
                    if (perbit_cdc[byte_lane][dq_dbi_bit] > 0xF)
                    {
                        perbit_cdc[byte_lane][dq_dbi_bit] = perbit;
                    }
                }
            } // perbit loop
        } // byte loop
    }

    // Populate the PHY per-bit lanes with the trained values
    for (byte_lane = 0; byte_lane < NUM_DQ_PCH; byte_lane++) 
    {        
        for (bit = 0; bit < PINS_PER_PHY; bit ++)
        {
            DDR_PHY_hal_cfg_pbit_dq_delay((dq0_ddr_phy_base + (byte_lane * DDR_PHY_OFFSET)), 
                                           bit, 
                                           1,   // 1 for TX.
                                           cs, 
                                           perbit_cdc[byte_lane][bit]);
    
            training_data_ptr->results.wr_dqdqs.perbit_delay[ch][cs][byte_lane][bit] = perbit_cdc[byte_lane][bit];
    
            //ddr_printf(DDR_NORMAL,"  Byte %d PHY bit %d WR DCC Perbit = %d\n",
            //                        byte_lane,bit, perbit_cdc[byte_lane][bit]);
        }
    }
    
}               


boolean DDRSS_midpoint_to_CDC_lpddr4 (
                               DDR_STRUCT *ddr,
                               uint32 middle,
                               uint32 clk_freq_in_khz,
                               uint8  ch,
                               uint8  cs,
                               training_data *training_data_ptr,
                               uint8  training_type, /* 0: CA Vref training; 1:wr_dqdqs training; 2: rd_dqdqs*/
                               uint8  phy_inx,
                               uint8 freq_inx
                              )
{
  uint32 coarse_cdc;
  uint32 fine_delay;
  uint32 fine_cdc;

  uint32 ddr_phy_chn_base;
  wrlvl_params_struct convert_cdc_info;

  uint32        period = 0;  
  
  period = CONVERT_CYC_TO_PS / clk_freq_in_khz;

  
  // if training is rd dqdqs, write the delay value to CSRs.
  if (training_type == TRAINING_TYPE_RD_DQDQS ) 
  {
     ddr_phy_chn_base = REG_OFFSET_DDR_PHY_CH(ch);

     // Start from freq band 3 (800MHz) up to band7 (1866MHz).
     if((clk_freq_in_khz > F_RANGE_2) && (clk_freq_in_khz < F_RANGE_7))
     {
        // When coarse delay exceed max value of CDC CSR, transfer the exceeding part to fine delay
        if (middle > ((COARSE_CDC_MAX_VALUE + 1) * COARSE_STEP_IN_PS))
        {
           coarse_cdc = COARSE_CDC_MAX_VALUE;
           fine_delay = (middle - (COARSE_CDC_MAX_VALUE + 1) * COARSE_STEP_IN_PS);
        }
        else
        {
           coarse_cdc = (middle / COARSE_STEP_IN_PS);
           fine_delay = (middle % COARSE_STEP_IN_PS);
        }

        fine_cdc = fine_delay / FINE_STEP_IN_PS;

        // When even fine delay exceeds max value of CDC CSR, return FALSE
        if (fine_cdc > FINE_CDC_MAX_VALUE) 
        {
            return FALSE;
        }
        else
        {
            DDR_PHY_hal_cfg_cdcext_slave_rd (ddr_phy_chn_base + DQ0_DDR_PHY_OFFSET + phy_inx * DDR_PHY_OFFSET,
                                             cs,
                                             coarse_cdc,
                                             1/*coarse*/,
                                             HP_MODE,
                                             freq_inx
                                            );

            DDR_PHY_hal_cfg_cdcext_slave_rd (ddr_phy_chn_base + DQ0_DDR_PHY_OFFSET + phy_inx * DDR_PHY_OFFSET,
                                             cs,
                                             fine_cdc,
                                             0/*fine*/,
                                             HP_MODE,
                                             freq_inx
                                            );

            training_data_ptr->results.rd_dqdqs.coarse_cdc[freq_inx][ch][cs][phy_inx] = coarse_cdc;
            training_data_ptr->results.rd_dqdqs.fine_cdc  [freq_inx][ch][cs][phy_inx] = fine_cdc;
            
            // For 547MHz, 777MHz and 1036MHz training, copy the rank0 value to rank1 to store in data structure.
            if ((clk_freq_in_khz < F_RANGE_4) && ( ddr->cdt_params[0].common.populated_chipselect == DDR_CS_BOTH))
            {
               DDR_PHY_hal_cfg_cdcext_slave_rd (ddr_phy_chn_base + DQ0_DDR_PHY_OFFSET + phy_inx * DDR_PHY_OFFSET,
                                                1,
                                                coarse_cdc,
                                                1/*coarse*/,
                                                HP_MODE,
                                                freq_inx
                                               );

               DDR_PHY_hal_cfg_cdcext_slave_rd (ddr_phy_chn_base + DQ0_DDR_PHY_OFFSET + phy_inx * DDR_PHY_OFFSET,
                                                1,
                                                fine_cdc,
                                                0/*fine*/,
                                                HP_MODE,
                                                freq_inx
                                               );

               training_data_ptr->results.rd_dqdqs.coarse_cdc[freq_inx][ch][1][phy_inx] = coarse_cdc;
               training_data_ptr->results.rd_dqdqs.fine_cdc  [freq_inx][ch][1][phy_inx] = fine_cdc;
            }
        }
     }
  }
  
  else if (training_type == TRAINING_TYPE_CA_VREF )
  {
    // This API does not handle the last three parameters for CA Vref training, they are used only by WR DQDQS training.
    DDRSS_cdc_convert (ddr, &convert_cdc_info, middle, period, 0, 0, 0);  // 0: ca_vref training_type.
    
    DDRSS_writing_ext_CSR_lpddr4 (ddr,
                                  ch,
                                  cs,
                                  &convert_cdc_info,
                                  training_data_ptr,
                                  training_type,
                                  phy_inx,
                                  freq_inx
                                  );
  }

  return TRUE;
}

void DDRSS_writing_ext_CSR_lpddr4 (DDR_STRUCT *ddr,
                                   uint8 ch,
                                   uint8 cs,
                                   wrlvl_params_struct *convert_cdc_ptr,
                                   training_data *training_data_ptr,
                                   uint8 training_type, /* 0: CA Vref training; 1:wr_dqdqs training; 2: rd_dqdqs training */
                                   uint8 phy_inx,
                                   uint8 freq_inx
                                  )
{
   uint32 ddr_phy_chn_base;
   uint8 retmr_value = 0;
    
   ddr_phy_chn_base = REG_OFFSET_DDR_PHY_CH(ch);

   // CA Vref training. freq_inx will be 6, for the highest frequency that is trained.
    if (training_type == TRAINING_TYPE_CA_VREF)  
    {
       DDR_PHY_hal_cfg_cdcext_slave_wr (ddr_phy_chn_base + CA0_DDR_PHY_OFFSET + phy_inx * DDR_PHY_OFFSET,
                                        cs,
                                        (*convert_cdc_ptr).coarse_dqs_delay,
                                        1/*coarse*/,
                                        HP_MODE,
                                        freq_inx
                                       );

       DDR_PHY_hal_cfg_cdcext_slave_wr (ddr_phy_chn_base + CA0_DDR_PHY_OFFSET + phy_inx * DDR_PHY_OFFSET,
                                        cs,
                                        (*convert_cdc_ptr).fine_dqs_delay,
                                        0/*fine*/,
                                        HP_MODE,
                                        freq_inx
                                       );

       DDR_PHY_hal_cfg_wrext_ctl_update (ddr_phy_chn_base + CA0_DDR_PHY_OFFSET + phy_inx * DDR_PHY_OFFSET,
                                         freq_inx,
                                         cs,
                                         (*convert_cdc_ptr).dqs_retmr,
                                         (*convert_cdc_ptr).dqs_half_cycle,
                                         (*convert_cdc_ptr).dqs_full_cycle
                                        );

       training_data_ptr->results.ca_vref.coarse_cdc     [0][ch][cs][phy_inx] = (*convert_cdc_ptr).coarse_dqs_delay;
       training_data_ptr->results.ca_vref.fine_cdc       [0][ch][cs][phy_inx] = (*convert_cdc_ptr).fine_dqs_delay;
       training_data_ptr->results.ca_vref.dq_retmr       [0][ch][cs][phy_inx] = (*convert_cdc_ptr).dqs_retmr;
       training_data_ptr->results.ca_vref.dq_half_cycle  [0][ch][cs][phy_inx] = (*convert_cdc_ptr).dqs_half_cycle;
       training_data_ptr->results.ca_vref.dq_full_cycle  [0][ch][cs][phy_inx] = (*convert_cdc_ptr).dqs_full_cycle;
       
       // CA training happens only for rank0 and only at the highest freq, copy the rank0 value to rank1 to store in data structure.
       if( ddr->cdt_params[0].common.populated_chipselect == DDR_CS_BOTH)
       {
          DDR_PHY_hal_cfg_cdcext_slave_wr (ddr_phy_chn_base + CA0_DDR_PHY_OFFSET + phy_inx * DDR_PHY_OFFSET,
                                           1,
                                           (*convert_cdc_ptr).coarse_dqs_delay,
                                           1/*coarse*/,
                                           HP_MODE,
                                           freq_inx
                                          );

          DDR_PHY_hal_cfg_cdcext_slave_wr (ddr_phy_chn_base + CA0_DDR_PHY_OFFSET + phy_inx * DDR_PHY_OFFSET,
                                           1,
                                           (*convert_cdc_ptr).fine_dqs_delay,
                                           0/*fine*/,
                                           HP_MODE,
                                           freq_inx
                                          );

          DDR_PHY_hal_cfg_wrext_ctl_update (ddr_phy_chn_base + CA0_DDR_PHY_OFFSET + phy_inx * DDR_PHY_OFFSET,
                                            freq_inx,
                                            1,
                                            (*convert_cdc_ptr).dqs_retmr,
                                            (*convert_cdc_ptr).dqs_half_cycle,
                                            (*convert_cdc_ptr).dqs_full_cycle
                                           );

          training_data_ptr->results.ca_vref.coarse_cdc     [0][ch][1][phy_inx] = (*convert_cdc_ptr).coarse_dqs_delay;
          training_data_ptr->results.ca_vref.fine_cdc       [0][ch][1][phy_inx] = (*convert_cdc_ptr).fine_dqs_delay;
          training_data_ptr->results.ca_vref.dq_retmr       [0][ch][1][phy_inx] = (*convert_cdc_ptr).dqs_retmr;
          training_data_ptr->results.ca_vref.dq_half_cycle  [0][ch][1][phy_inx] = (*convert_cdc_ptr).dqs_half_cycle;
          training_data_ptr->results.ca_vref.dq_full_cycle  [0][ch][1][phy_inx] = (*convert_cdc_ptr).dqs_full_cycle;
       }
    }

    // wr dqdqs training
    if (training_type == TRAINING_TYPE_WR_DQDQS)  
    {
       DDR_PHY_hal_cfg_cdcext_slave_wr (ddr_phy_chn_base + DQ0_DDR_PHY_OFFSET + phy_inx * DDR_PHY_OFFSET,
                                        cs,
                                        training_data_ptr->results.wr_dqdqs.coarse_cdc[freq_inx][ch][cs][phy_inx],
                                        1/*coarse*/,
                                        HP_MODE,
                                        freq_inx
                                       );

       DDR_PHY_hal_cfg_cdcext_slave_wr (ddr_phy_chn_base + DQ0_DDR_PHY_OFFSET + phy_inx * DDR_PHY_OFFSET,
                                        cs,
                                        training_data_ptr->results.wr_dqdqs.fine_cdc[freq_inx][ch][cs][phy_inx],
                                        0/*fine*/,
                                        HP_MODE,
                                        freq_inx
                                       );
       // Read the retimer status. No need to calculate and WR again since its already been done.
       retmr_value = DDR_PHY_hal_sta_wrlvl_dq_retmr(ddr_phy_chn_base + DQ0_DDR_PHY_OFFSET + phy_inx * DDR_PHY_OFFSET, cs);
                                
       DDR_PHY_hal_cfg_wrext_ctl_update (ddr_phy_chn_base + DQ0_DDR_PHY_OFFSET + phy_inx * DDR_PHY_OFFSET,
                                         freq_inx,
                                         cs,
                                         retmr_value, 
                                         training_data_ptr->results.wr_dqdqs.dq_half_cycle[freq_inx][ch][cs][phy_inx],
                                         training_data_ptr->results.wr_dqdqs.dq_full_cycle[freq_inx][ch][cs][phy_inx]
                                        );

       // write dq dqs training should pass the max and min out
       DDR_PHY_hal_cfg_dq_vref_dqs2dq_max (ddr_phy_chn_base + DQ0_DDR_PHY_OFFSET + phy_inx * DDR_PHY_OFFSET,
                                           cs,
                                           freq_inx,
                                           training_data_ptr->results.wr_dqdqs.coarse_max_eye_left_boundary_cdc_value [freq_inx][ch][cs][phy_inx],
                                           1/*coarse*/
                                          );
       DDR_PHY_hal_cfg_dq_vref_dqs2dq_min (ddr_phy_chn_base + DQ0_DDR_PHY_OFFSET + phy_inx * DDR_PHY_OFFSET,
                                           cs,
                                           freq_inx,
                                           training_data_ptr->results.wr_dqdqs.coarse_max_eye_right_boundary_cdc_value [freq_inx][ch][cs][phy_inx],
                                           1/*coarse*/
                                          );

       training_data_ptr->results.wr_dqdqs.coarse_cdc    [freq_inx][ch][cs][phy_inx] = training_data_ptr->results.wr_dqdqs.coarse_cdc[freq_inx][ch][cs][phy_inx];
       training_data_ptr->results.wr_dqdqs.fine_cdc      [freq_inx][ch][cs][phy_inx] = training_data_ptr->results.wr_dqdqs.fine_cdc[freq_inx][ch][cs][phy_inx];
       training_data_ptr->results.wr_dqdqs.dq_retmr      [freq_inx][ch][cs][phy_inx] = retmr_value;
       training_data_ptr->results.wr_dqdqs.dq_half_cycle [freq_inx][ch][cs][phy_inx] = training_data_ptr->results.wr_dqdqs.dq_half_cycle[freq_inx][ch][cs][phy_inx];
       training_data_ptr->results.wr_dqdqs.dq_full_cycle [freq_inx][ch][cs][phy_inx] = training_data_ptr->results.wr_dqdqs.dq_full_cycle[freq_inx][ch][cs][phy_inx];

       
       // For 547MHz; 777MHz and 1017MHz training, copy the rank0 value to rank1 to store in data structure.
       if ((freq_inx > 1) && (freq_inx < 5) && ( ddr->cdt_params[0].common.populated_chipselect == DDR_CS_BOTH))
       {
          DDR_PHY_hal_cfg_cdcext_slave_wr (ddr_phy_chn_base + DQ0_DDR_PHY_OFFSET + phy_inx * DDR_PHY_OFFSET,
                                           1,
                                           training_data_ptr->results.wr_dqdqs.coarse_cdc[freq_inx][ch][0][phy_inx],
                                           1/*coarse*/,
                                           HP_MODE,
                                           freq_inx
                                          );

          DDR_PHY_hal_cfg_cdcext_slave_wr (ddr_phy_chn_base + DQ0_DDR_PHY_OFFSET + phy_inx * DDR_PHY_OFFSET,
                                           1,
                                           training_data_ptr->results.wr_dqdqs.fine_cdc[freq_inx][ch][0][phy_inx],
                                           0/*fine*/,
                                           HP_MODE,
                                           freq_inx
                                          );
          // Read the retimer status. No need to calculate and program again since its already been done.
          retmr_value = DDR_PHY_hal_sta_wrlvl_dq_retmr(ddr_phy_chn_base + DQ0_DDR_PHY_OFFSET + phy_inx * DDR_PHY_OFFSET, 0);
          
          DDR_PHY_hal_cfg_wrext_ctl_update (ddr_phy_chn_base + DQ0_DDR_PHY_OFFSET + phy_inx * DDR_PHY_OFFSET,
                                            freq_inx,
                                            1,
                                            retmr_value,
                                            training_data_ptr->results.wr_dqdqs.dq_half_cycle[freq_inx][ch][0][phy_inx],
                                            training_data_ptr->results.wr_dqdqs.dq_full_cycle[freq_inx][ch][0][phy_inx]
                                           );

          training_data_ptr->results.wr_dqdqs.coarse_cdc    [freq_inx][ch][1][phy_inx] = training_data_ptr->results.wr_dqdqs.coarse_cdc[freq_inx][ch][0][phy_inx];
          training_data_ptr->results.wr_dqdqs.fine_cdc      [freq_inx][ch][1][phy_inx] = training_data_ptr->results.wr_dqdqs.fine_cdc[freq_inx][ch][0][phy_inx];
          training_data_ptr->results.wr_dqdqs.dq_retmr      [freq_inx][ch][1][phy_inx] = retmr_value;
          training_data_ptr->results.wr_dqdqs.dq_half_cycle [freq_inx][ch][1][phy_inx] = training_data_ptr->results.wr_dqdqs.dq_half_cycle[freq_inx][ch][0][phy_inx];
          training_data_ptr->results.wr_dqdqs.dq_full_cycle [freq_inx][ch][1][phy_inx] = training_data_ptr->results.wr_dqdqs.dq_full_cycle[freq_inx][ch][0][phy_inx];
       }
    }
     
}


void DDRSS_DIT_Read (DDR_STRUCT *ddr,
                     uint8  ch,
                     uint8  cs,
                     training_params_t *training_params_ptr,
                     uint8  dit_ave,
                     uint32 (*dit_count)[NUM_CS][NUM_DIE_PCH],
                     uint32 clk_freq_khz
                     )
{
    uint32 dit_die_count[2]     = {0};
    uint32 dit_die_count_acc[2] = {0};
    uint32 dit_die_count_max[2] = {0};
    uint32 dit_die_count_min[2] = {0xffffffff,0xffffffff};
    uint32 MR18_data            = 0;
    uint32 MR19_data            = 0;
    uint32 MR_19_18_data[2]     = {0};
    uint16 dit_runtime_count    = 0;
    uint8 loopcnt               = 0;
    uint8 die                   = 0;
    uint8 max_loopcnt           = 0;

    // Training data structure pointer
    training_data *training_data_ptr;
    training_data_ptr = (training_data *)(&ddr->flash_params.training_data);

    // Base pointer to SHKE

    // Calculate the clock period

    // Encode the channel and rank for the BIMC functions

    // set the time between subsequent interval time operation

    // Search for the optimal run time count for the first pass of the DIT Read function
//    {
     // If the runtime count is established, load from the data structure
     dit_runtime_count = training_data_ptr->results.dit.dit_runtime_count;
//    }

    // Read the max_loopcnt from the training params data structure
    max_loopcnt = training_params_ptr->dit.max_loopcnt;

    // Write the sample count to the results data structure for DTTS
    training_data_ptr->results.dit.dit_loop_count = max_loopcnt;
    //training_data_ptr->results.dit.dit_trac_ndx = 10; // TODO Tao Wang

    // Reset the loop count
    loopcnt = 0;

    // Read and average multiple DRAM Oscillator counts
    while (loopcnt < max_loopcnt)
    {

     // Read the DRAM DQS oscillator counter
      DDRSS_DIT_Capture(ch,
                        cs,
                        dit_runtime_count,
                        MR_19_18_data);

      MR18_data = MR_19_18_data[0];
      MR19_data = MR_19_18_data[1];

      // Discard illegal count results
      if(!(
          // Die 0 both MR18 and MR19 all zeros
          (((MR18_data & 0x000000FF) == 0x00)   && ((MR19_data & 0x000000FF) == 0x00))   ||

          // Die 1 both MR18 and MR19 all zeros
          ((((MR18_data>>16) & 0x00FF) == 0x00) && (((MR19_data>>16) & 0x00FF) == 0x00)) ||

          // Die 0 both MR18 and MR19 all ones
          (((MR18_data & 0xFF) == 0xFF)         && ((MR19_data & 0xFF) == 0xFF))         ||

          // Die 1 both MR18 and MR19 all ones
          ((((MR18_data>>16) & 0x00FF) == 0xFF) && (((MR19_data>>16) & 0X00FF) == 0xFF))
        ))
      {

        // Isolate the die counter results
        dit_die_count[0] = ((MR19_data & 0x000000ff)<<8) |  (MR18_data & 0x000000ff);
        dit_die_count[1] = ((MR19_data & 0x00ff0000)>>8) | ((MR18_data & 0x00ff0000)>>16);

        // Calculate the minimum and maximum counts
        for (die=0;die<=1;die++)
        {
          // Accumulate the per die counter results
          dit_die_count_acc[die] += dit_die_count[die];

          // Check for the maximum
          if (dit_die_count[die] > dit_die_count_max[die])
            dit_die_count_max[die] = dit_die_count[die];

          // Check for the minimum
          if (dit_die_count[die] < dit_die_count_min[die])
            dit_die_count_min[die] = dit_die_count[die];
        }

        loopcnt++;
      }
    } // while

    // Process the accumulated count results
    for (die=0;die<=1;die++)
    {
      // Remove the minimum and maximum counts
      dit_die_count[die] = dit_die_count_acc[die] - (dit_die_count_max[die] + dit_die_count_min[die]);

      // Average the accumulated counts (after removing the min and max)
      dit_die_count[die] = dit_die_count[die]/(max_loopcnt-2);

      // Average any previous values with the new results
      dit_count[ch][cs][die] = (dit_die_count[die] + dit_count[ch][cs][die])/(dit_ave + 1);
    }
}

void DDRSS_DIT_Slope(DDR_STRUCT *ddr,
                     uint8  ch,
                     uint8  cs,
                     uint8  die,
                     uint32 clk_freq_khz,
                     uint16 (*dit_mid_count))
{

  uint16  dit_count                         = 0;
  uint8   range                             = 0;
  uint8   index                             = 0;
  uint8   mid                               = 0;
  uint32  dit_acquisition_time              = 0;
  uint32  dit_dram_clk_period               = 0;

  uint32  dit_training_freq                 = 0;
  uint32  dit_ps_per_count                  = 0;
  uint32  dit_delta_count[NUM_DIT_COUNT+1]  = {0};

  // Training data structure pointer
  training_data *training_data_ptr;
  training_data_ptr = (training_data *)(&ddr->flash_params.training_data);

  // Fetch the boot training frequency
  dit_training_freq = training_data_ptr->results.dit.dit_training_freq;

  // Fetch the boot training DIT count result
  dit_count = training_data_ptr->results.dit.dit_count[ch][cs][die];

  // Scale the DIT count to the current clock frequency
  dit_count = (dit_count * dit_training_freq)/clk_freq_khz;

  // Calculate the clock period
  dit_dram_clk_period =  CONVERT_CYC_TO_PS/clk_freq_khz;

  // Calculate the acquisition time
  //dit_acquisition_time = (DIT_RUNTIME_FREQ * dit_dram_clk_period);//TOFIX
  dit_acquisition_time = (training_data_ptr->results.dit.dit_runtime_count * dit_dram_clk_period);

  // Populate the center of the array with the scaled DIT count
  dit_delta_count[NUM_DIT_COUNT/2] = dit_count;

  // Calculate the time (ps) per DIT count
  dit_ps_per_count = dit_acquisition_time/dit_count;

  // Calculate the 10ps to 50ps range of the array
  for (range=1;range<=NUM_DIT_COUNT/2;range++)
  {
    // Determine the index for the delta count array
    index = range + NUM_DIT_COUNT/2;

    // Calculate the Delta count when the clock tree changes in units of 10ps (10ps * two ring oscillator half cycles)
    dit_delta_count[index] = dit_acquisition_time / (dit_ps_per_count - (range * 20));
  }

  index = (NUM_DIT_COUNT/2)-1;

  // Calculate the -50ps to -10ps range of the array
  for (range=1;range<=NUM_DIT_COUNT/2;range++)
  {
    // Calculate the Delta count when the clock tree changes in units of 10ps (10ps * two ring oscillator half cycles)
    dit_delta_count[index] = dit_acquisition_time / (dit_ps_per_count + (range * 20));
    index--;
  }

  // Poplulate the return array with the midpoints between the delta counts
  for (mid=0;mid<=NUM_DIT_COUNT-1;mid++)
  {
    // Calculate the midpoints
    dit_mid_count[mid] = (dit_delta_count[mid] + dit_delta_count[mid+1])/2;
  }
}

void DDRSS_DIT_Capture(uint8 ch,
                       uint8 cs,
                       uint16 dit_runtime_count,
                       uint32 (*MR_19_18_data))
{
  uint32 MR23_value      = 0;
  DDR_CHANNEL ch_1hot    = DDR_CH_NONE;
  DDR_CHIPSELECT cs_1hot = DDR_CS_NONE;
  uint32 reg_offset_shke = 0;

  // Base pointer to SHKE
  reg_offset_shke = REG_OFFSET_SHKE(ch);

  // Convert the channel to one-hot for BIMC functions
  ch_1hot = CH_1HOT(ch);
  cs_1hot = CS_1HOT(cs);

  // Decode the runtime count for the DRAM MR23 register
  if (dit_runtime_count < 2048)
  {
    MR23_value = dit_runtime_count / 16;
  }
  else
  {
    if (dit_runtime_count == 2048)
      MR23_value = 0x40;
    else if (dit_runtime_count == 4096)
      MR23_value = 0x80;
    else if (dit_runtime_count == 8196)
      MR23_value = 0xC0;
  }

  // Set DQS interval timer runtime setting
  BIMC_MR_Write (ch_1hot, cs_1hot, JEDEC_MR_23, MR23_value);

  // Set the BIMC SHKE control register for rank
  HWIO_OUTXF2 (reg_offset_shke, SHKE_MPC_CNTL,ISSUE_TO_RANK,MPC_START,cs_1hot,1);

  // Poll for start asserted
  while(HWIO_INXF (reg_offset_shke, SHKE_MPC_CNTL,MPC_START) == 1);

  // Wait for the DRAM Oscillator to acquire the count
  seq_wait(20,US);

  // Read the DRAM Oscillator counter
  MR_19_18_data[0] = BIMC_MR_Read (ch_1hot,cs_1hot,JEDEC_MR_18);
  MR_19_18_data[1] = BIMC_MR_Read (ch_1hot,cs_1hot,JEDEC_MR_19);

}


void DDRSS_DIT_Runtime(DDR_STRUCT *ddr,
                     uint8  ch,
                     uint8  cs,
                     training_params_t *training_params_ptr,
                     uint32 (*dit_count)[NUM_CS][NUM_DIE_PCH],
                     uint32 clk_freq_khz
                     )
{
    uint32 dit_die_count[2]     = {0};
    uint32 MR18_data            = 0;
    uint32 MR19_data            = 0;
    uint32 MR_19_18_data[2]     = {0};
    uint32 dit_dram_clk_period  = 0;
    uint16 dit_runtime_table[4] = {1008, 2048, 4096, 8192};
    uint16 dit_runtime_count    = 0;
    uint32 dit_delta_count[2]   = {0};
    uint32 dit_ps_per_count[2]  = {0};
    uint32 dit_acquisition_time = 0;
    uint8 loopcnt               = 0;
    uint8 die                   = 0;

    // Training data structure pointer
    training_data *training_data_ptr;
    training_data_ptr = (training_data *)(&ddr->flash_params.training_data);

    // Base pointer to SHKE

    // Calculate the clock period
    dit_dram_clk_period =  CONVERT_CYC_TO_PS/clk_freq_khz;

    // Encode the channel and rank for the BIMC functions

    // set the time between subsequent interval time operation

    // Search for the optimal run time count for the first pass of the DIT Read function
//    {
      // Normalize the results if needed by increasing the acquisition count
      while ((dit_delta_count[0] < DIT_NORMAL_DELTA) && (dit_delta_count[1] < DIT_NORMAL_DELTA) && (loopcnt < 4))
      {
        // Calculate the MR23 acquisition count
        dit_runtime_count    = dit_runtime_table[loopcnt];

        DDRSS_DIT_Capture(ch,
                          cs,
                          dit_runtime_count,
                          MR_19_18_data);

        MR18_data = MR_19_18_data[0];
        MR19_data = MR_19_18_data[1];

        // Discard illegal count results
        if(!(
            // Die 0 both MR18 and MR19 all zeros
            (((MR18_data & 0x000000FF) == 0x00)   && ((MR19_data & 0x000000FF) == 0x00))   ||

            // Die 1 both MR18 and MR19 all zeros
            ((((MR18_data>>16) & 0x00FF) == 0x00) && (((MR19_data>>16) & 0x00FF) == 0x00)) ||

            // Die 0 both MR18 and MR19 all ones
            (((MR18_data & 0xFF) == 0xFF)         && ((MR19_data & 0xFF) == 0xFF))         ||

            // Die 1 both MR18 and MR19 all ones
            ((((MR18_data>>16) & 0x00FF) == 0xFF) && (((MR19_data>>16) & 0X00FF) == 0xFF))
            ))

        {

          // Isolate the die counter results into 16 bits
          dit_die_count[0] = ((MR19_data & 0x000000ff)<<8) |  (MR18_data & 0x000000ff);
          dit_die_count[1] = ((MR19_data & 0x00ff0000)>>8) | ((MR18_data & 0x00ff0000)>>16);

          dit_acquisition_time = dit_runtime_count * dit_dram_clk_period;

          // Calculate the dit delta count
          for (die=0;die<=1;die++)
          {
            dit_ps_per_count[die] = dit_acquisition_time / dit_die_count[die];
            dit_delta_count[die]  = dit_acquisition_time / (dit_ps_per_count[die] + 20);
            dit_delta_count[die]  = dit_die_count[die] - dit_delta_count[die];
          }

          // Update the results data structure with the revised runtime count
          if (training_data_ptr->results.dit.dit_runtime_count < dit_runtime_count)
          training_data_ptr->results.dit.dit_runtime_count = dit_runtime_count;

          loopcnt++;
        }
      }
//    }
}

void DDR_PHY_rtn_ca_pcc_link_list_load (uint32 ddr_phy_addr_base)
{
  uint32 maxLLIndex           = NUM_PHY_PCC_LL;
  uint32 curLLIndex           = 0;

  /// Assert SW Reset of the Linked List and Memory
  HWIO_OUTX(ddr_phy_addr_base,DDR_PHY_DDRPHY_PCC_LL_CFG, 0x100); //LL_IDX_RST
  HWIO_OUTX(ddr_phy_addr_base,DDR_PHY_DDRPHY_PCC_MEM_CFG,0x1000);// MEM_IDX_RST

  /// De-Assert SW Reset of the Linked List and Memory
  HWIO_OUTX(ddr_phy_addr_base,DDR_PHY_DDRPHY_PCC_LL_CFG, 0x0);//LL_IDX_RST
  HWIO_OUTX(ddr_phy_addr_base,DDR_PHY_DDRPHY_PCC_MEM_CFG,0x0);//MEM_IDX_RST

  /// Write the Link List descriptors into the Link memory
  for (curLLIndex = 0; curLLIndex < maxLLIndex; curLLIndex = curLLIndex + 2) 
  {
    HWIO_OUTX(ddr_phy_addr_base,
              DDR_PHY_DDRPHY_PCCLL_ADDR_CFG,
             ((ddr_phy_ca_pcc_link_list[curLLIndex+1]<<16) | (ddr_phy_ca_pcc_link_list[curLLIndex]))
             );
  }
}

void DDR_PHY_rtn_dq_pcc_link_list_load (uint32 ddr_phy_addr_base)
{
  uint32 maxLLIndex           = NUM_PHY_PCC_LL;
  uint32 curLLIndex           = 0;

  /// Assert SW Reset of the Linked List and Memory
  HWIO_OUTX(ddr_phy_addr_base,DDR_PHY_DDRPHY_PCC_LL_CFG, 0x100); //LL_IDX_RST
  HWIO_OUTX(ddr_phy_addr_base,DDR_PHY_DDRPHY_PCC_MEM_CFG,0x1000);// MEM_IDX_RST

  /// De-Assert SW Reset of the Linked List and Memory
  HWIO_OUTX(ddr_phy_addr_base,DDR_PHY_DDRPHY_PCC_LL_CFG, 0x0);//LL_IDX_RST
  HWIO_OUTX(ddr_phy_addr_base,DDR_PHY_DDRPHY_PCC_MEM_CFG,0x0);//MEM_IDX_RST

  /// Write the Link List descriptors into the Link memory
  for (curLLIndex = 0; curLLIndex < maxLLIndex; curLLIndex = curLLIndex + 2) 
  {
    HWIO_OUTX(ddr_phy_addr_base,
              DDR_PHY_DDRPHY_PCCLL_ADDR_CFG,
             ((ddr_phy_dq_pcc_link_list[curLLIndex+1]<<16) | (ddr_phy_dq_pcc_link_list[curLLIndex]))
             );
  }
}

void DDR_CC_rtn_pcc_link_list_load (uint32 ddr_cc_addr_base)
{
  uint32 maxLLIndex           = NUM_CC_PCC_LL;
  uint32 curLLIndex           = 0;
  
  /// Assert SW Reset of the Linked List and Memory
  HWIO_OUTX(ddr_cc_addr_base,DDR_CC_DDRCC_PCC_LL_CFG,  0x100);// LL_IDX_RST
  HWIO_OUTX(ddr_cc_addr_base,DDR_CC_DDRCC_PCC_MEM_CFG,0x1000);//MEM_IDX_RST

  /// De-Assert SW Reset of the Linked List and Memory
  HWIO_OUTX(ddr_cc_addr_base,DDR_CC_DDRCC_PCC_LL_CFG, 0x0);//LL_IDX_RST
  HWIO_OUTX(ddr_cc_addr_base,DDR_CC_DDRCC_PCC_MEM_CFG,0x0);//MEM_IDX_RST

  /// Write the Link List descriptors into the Link memory
  for (curLLIndex = 0; curLLIndex < maxLLIndex; curLLIndex = curLLIndex + 2) 
  {
    HWIO_OUTX(ddr_cc_addr_base,
              DDR_CC_DDRCC_PCCLL_ADDR_CFG,
              ((ddr_cc_pcc_link_list[curLLIndex+1]<<16) | (ddr_cc_pcc_link_list[curLLIndex]))
              );
  } 
}

// Reset the DDR PHY DCC
void DDR_PHY_DCC_Reset (uint8 ch )
{
  uint32 dq0_ddr_phy_base = 0;
  uint8  dq               = 0;

  dq0_ddr_phy_base  = REG_OFFSET_DDR_PHY_CH(ch) + DQ0_DDR_PHY_OFFSET;

  // Clear all of the DQ and DQS TX and RX DCC Enables
  for (dq=0;dq<NUM_DQ_PCH;dq++)
  {
    HWIO_OUTXF2 (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMSLICE0_PBIT_CTL_DQ_0_RXTX_CFG, TX0_EN, RX0_EN, 0x0 , 0x0 );
    HWIO_OUTXF2 (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMSLICE1_PBIT_CTL_DQ_1_RXTX_CFG, TX1_EN, RX1_EN, 0x0 , 0x0 );
    HWIO_OUTXF2 (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMSLICE2_PBIT_CTL_DQ_2_RXTX_CFG, TX2_EN, RX2_EN, 0x0 , 0x0 );
    HWIO_OUTXF2 (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMSLICE3_PBIT_CTL_DQ_3_RXTX_CFG, TX3_EN, RX3_EN, 0x0 , 0x0 );
    HWIO_OUTXF2 (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMSLICE4_PBIT_CTL_DQ_4_RXTX_CFG, TX4_EN, RX4_EN, 0x0 , 0x0 );
    HWIO_OUTXF2 (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMSLICE5_PBIT_CTL_DQ_5_RXTX_CFG, TX5_EN, RX5_EN, 0x0 , 0x0 );
    HWIO_OUTXF2 (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMSLICE6_PBIT_CTL_DQ_6_RXTX_CFG, TX6_EN, RX6_EN, 0x0 , 0x0 );
    HWIO_OUTXF2 (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMSLICE7_PBIT_CTL_DQ_7_RXTX_CFG, TX7_EN, RX7_EN, 0x0 , 0x0 );
    HWIO_OUTXF2 (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMSLICE8_PBIT_CTL_DQ_8_RXTX_CFG, TX8_EN, RX8_EN, 0x0 , 0x0 );
    HWIO_OUTXF2 (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMSLICE9_PBIT_CTL_DQ_9_RXTX_CFG, TX9_EN, RX9_EN, 0x0 , 0x0 ); 
    HWIO_OUTXF2 (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMSLICE10_PBIT_CTL_DQS_CFG,       TX_EN, RX_EN,  0x0 , 0x0 );
  }

  // Set all of the DQ and DQS TX and RX DCC Enables
  for (dq=0;dq<NUM_DQ_PCH;dq++)
  {
    HWIO_OUTXF2 (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMSLICE0_PBIT_CTL_DQ_0_RXTX_CFG, TX0_EN, RX0_EN, 0x1 , 0x1 );
    HWIO_OUTXF2 (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMSLICE1_PBIT_CTL_DQ_1_RXTX_CFG, TX1_EN, RX1_EN, 0x1 , 0x1 );
    HWIO_OUTXF2 (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMSLICE2_PBIT_CTL_DQ_2_RXTX_CFG, TX2_EN, RX2_EN, 0x1 , 0x1 );
    HWIO_OUTXF2 (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMSLICE3_PBIT_CTL_DQ_3_RXTX_CFG, TX3_EN, RX3_EN, 0x1 , 0x1 );
    HWIO_OUTXF2 (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMSLICE4_PBIT_CTL_DQ_4_RXTX_CFG, TX4_EN, RX4_EN, 0x1 , 0x1 );
    HWIO_OUTXF2 (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMSLICE5_PBIT_CTL_DQ_5_RXTX_CFG, TX5_EN, RX5_EN, 0x1 , 0x1 );
    HWIO_OUTXF2 (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMSLICE6_PBIT_CTL_DQ_6_RXTX_CFG, TX6_EN, RX6_EN, 0x1 , 0x1 );
    HWIO_OUTXF2 (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMSLICE7_PBIT_CTL_DQ_7_RXTX_CFG, TX7_EN, RX7_EN, 0x1 , 0x1 );
    HWIO_OUTXF2 (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMSLICE8_PBIT_CTL_DQ_8_RXTX_CFG, TX8_EN, RX8_EN, 0x1 , 0x1 );
    HWIO_OUTXF2 (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMSLICE9_PBIT_CTL_DQ_9_RXTX_CFG, TX9_EN, RX9_EN, 0x1 , 0x1 ); 
    HWIO_OUTXF2 (dq0_ddr_phy_base + (dq * DDR_PHY_OFFSET), DDR_PHY_DDRPHY_CMSLICE10_PBIT_CTL_DQS_CFG,       TX_EN, RX_EN,  0x1 , 0x1 );
  }

}

