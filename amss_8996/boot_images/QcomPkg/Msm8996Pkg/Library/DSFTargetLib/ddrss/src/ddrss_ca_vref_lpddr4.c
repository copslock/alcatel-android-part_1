/****************************************************************************
 QUALCOMM Proprietary Design Data
 Copyright (c) 2014, Qualcomm Technologies Incorporated. All rights reserved.
 ****************************************************************************/
/*==============================================================================
                                EDIT HISTORY

$Header: //components/rel/boot.xf/1.0/QcomPkg/Msm8996Pkg/Library/DSFTargetLib/ddrss/src/ddrss_ca_vref_lpddr4.c#4 $
$DateTime: 2015/12/04 17:21:20 $
$Author: pwbldsvc $
================================================================================
when       who          what, where, why
--------   ---          --------------------------------------------------------
05/04/14   arindamm     First edit history header. Add new entries at top.
================================================================================*/

#include "ddrss.h"
#include <string.h>

//================================================================================================//
// DDR PHY CA VREF training
//================================================================================================//
boolean DDRSS_ca_vref_lpddr4 (DDR_STRUCT    *ddr,
                       uint8         ch,  
                       uint8         cs, 
                       uint8         prfs_index,
                       training_params_t *training_params_ptr,
                       ddrss_ca_vref_local_vars *local_vars,
                       uint32 clk_freq_khz
                      )
{
    best_eye_struct ca_best_eye_coarse  [NUM_CA_PCH];
    best_eye_struct ca_best_eye_fine    [NUM_CA_PCH][PINS_PER_PHY];
           
    uint8        left_start_cdc_value[NUM_CA_PCH] = {0};
    uint8       right_start_cdc_value[NUM_CA_PCH] = {0};
    uint32         reg_offset_ddr_phy[NUM_CA_PCH] = {0};
             
    uint8    ca_start_fine_vref_value[NUM_CA_PCH] = {0};
    uint8      ca_max_fine_vref_value[NUM_CA_PCH] = {0};
    uint8              perbit_mid_min[NUM_CA_PCH] = {0};
    
    uint8  best_vref_average_per_byte[NUM_CA_PCH] = {0};
    uint16     best_vref_sum_per_byte[NUM_CA_PCH] = {0};
    uint8       final_coarse_delay   [NUM_CA_PCH] = {0};
    uint8         final_fine_delay   [NUM_CA_PCH] = {0};
    
    uint16 ca_vref_cdc_delay_in_ps = 0;
    uint8                  phy_inx = 0;
    uint8                      bit = 0;
    uint8    start_fine_vref_value = 0;
    uint8      max_fine_vref_value = 0;
    
    uint8               freq_inx   = 0;
    uint8         dqs_half_struct  = 0;
    uint8         dq_half_struct   = 2;

    ca_vref_diff_struct ca_vref_diff_info;

    // Training data structure pointer
    training_data *training_data_ptr;
    training_data_ptr = (training_data *)(&ddr->flash_params.training_data);

    reg_offset_ddr_phy[0] = REG_OFFSET_DDR_PHY_CH (ch) + CA0_DDR_PHY_OFFSET;
    reg_offset_ddr_phy[1] = REG_OFFSET_DDR_PHY_CH (ch) + CA1_DDR_PHY_OFFSET;
    
    // Add a half-cycle delay for CDC CBT training
    if (training_params_ptr->ca_vref.cdc_training_enable == 1)
    {
        for (phy_inx = 0; phy_inx < NUM_CA_PCH; phy_inx++) 
        {
            HWIO_OUTXF (reg_offset_ddr_phy[phy_inx], DDR_PHY_DDRPHY_TOP_CTRL_4_CFG,SW_ODDR_SDR_MODE_EN,0x1);
            HWIO_OUTXF (reg_offset_ddr_phy[phy_inx], DDR_PHY_DDRPHY_TOP_CTRL_4_CFG,SW_ODDR_SDR_MODE   ,0x0);
            HWIO_OUTXF2 (reg_offset_ddr_phy[phy_inx], 
                        DDR_PHY_DDRPHY_CMHUB_TOP_1_CFG, 
                        DQS_STRUCT_HALF_CYCLE_R0, 
                        DQS_STRUCT_HALF_CYCLE_R1, 
                        dqs_half_struct, 
                        dqs_half_struct); 
    
            HWIO_OUTXF2 (reg_offset_ddr_phy[phy_inx], 
                        DDR_PHY_DDRPHY_CMHUB_TOP_1_CFG, 
                        DQ_STRUCT_HALF_CYCLE_R0, 
                        DQ_STRUCT_HALF_CYCLE_R1, 
                        dq_half_struct, 
                        dq_half_struct); 
    
            HWIO_OUTXF (reg_offset_ddr_phy[phy_inx], DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG, TRIG_WRLVL_LATCH, 0x1);
            HWIO_OUTXF (reg_offset_ddr_phy[phy_inx], DDR_PHY_DDRPHY_CMHUB_TOP_0_CFG, TRIG_WRLVL_LATCH, 0x0);
        }
    }
    
    // Initialize perbit setting to 0 before CA training start.
    if (training_params_ptr->ca_vref.cdc_training_enable == 1)
    {
        for(phy_inx = 0; phy_inx < NUM_CA_PCH; phy_inx++)
        {   
            for(bit = 0; bit < PINS_PER_PHY; bit++)
            {
                DDR_PHY_hal_cfg_pbit_dq_delay(reg_offset_ddr_phy[phy_inx], 
                                              bit, 
                                              1,  // 1 for Tx
                                              cs, 
                                              training_params_ptr->ca_vref.fine_perbit_start_value);
            }
        }
    }
    
    // MC enable the CA CLK pulse mode. Pulse mode is enabled to send only 2 clock pulses to 
    // DRAM with CA patterns in order to find a clear pass/fail boundary
    BIMC_CA_Training_Pulse_Ctrl (ch, 0x1);
    
    // ---------------------------------------------------------------------------------
    // Coarse Training.    
    // ---------------------------------------------------------------------------------
    
    // Coarse delay training loop, plotting the coarse histogram
    DDRSS_Vref_CDC_Coarse_Schmoo (ddr,
                                  ca_best_eye_coarse,
                                  local_vars->coarse_fail_count_table,
                                  ch, 
                                  cs,
                                  training_data_ptr,
                                  training_params_ptr,
                                  clk_freq_khz
                                 );


    //Coarse training has valid results, 
    if (ca_best_eye_coarse[0].vref_all_fail_flag == 0 && ca_best_eye_coarse[1].vref_all_fail_flag == 0)
    {
       for (phy_inx = 0; phy_inx < NUM_CA_PCH; phy_inx++) 
       { 
         training_data_ptr->results.ca_vref.coarse_vref[prfs_index][ch][cs][phy_inx] = ca_best_eye_coarse[phy_inx].best_vref_value;
         // Copy rank0 value into rank1 in the results data structure.
         training_data_ptr->results.ca_vref.coarse_vref[prfs_index][ch][1][phy_inx] = ca_best_eye_coarse[phy_inx].best_vref_value;
       }
       // When fine training is disabled, just write the coarse results into MR12, PHY CDC setting CSR and training_results
       if ((training_params_ptr->ca_vref.fine_training_enable == 0) && (training_params_ptr->ca_vref.cdc_training_enable == 1))
       {
          for (phy_inx = 0; phy_inx < NUM_CA_PCH; phy_inx++) 
          {
             // Write coarse CDC results into CA PHY CSR 
              DDR_PHY_hal_cfg_cdc_slave_wr (reg_offset_ddr_phy[phy_inx], 
                                             ca_best_eye_coarse[phy_inx].best_cdc_value,
                                             1,  // 1 = coarse delay, 0 = fine delay
                                             1,  // 1 = HP mode,      0 = LP mode 
                                             cs 
                                            );

             DDRSS_CDC_Retimer (ddr,
                                cs,
                                ca_best_eye_coarse[phy_inx].best_cdc_value, // Coarse CDC delay value 
                                0,                             // In coarse training, fine delay is 0
                                0, // wrlvl_coarse_delay
                                0, // wrlvl_fine_delay
                                reg_offset_ddr_phy[phy_inx],    // Base address of CA0 or CA1
                                clk_freq_khz
                               );

          }
       }

    // ---------------------------------------------------------------------------------
    // Fine Training.    
    // ---------------------------------------------------------------------------------
       
       if (training_params_ptr->ca_vref.fine_training_enable == 1)   
       {
          // 0: CA0 vref value is smaller; 1: CA1 vref value is smaller
          ca_vref_diff_info.ca_vref_flag = (ca_best_eye_coarse[1].best_vref_value > ca_best_eye_coarse[0].best_vref_value) ? 0 : 1;
          
          // Calculate the difference value of CA0 and CA1 Vref
          ca_vref_diff_info.ca_vref_diff = (ca_vref_diff_info.ca_vref_flag) ? 
                                            ca_best_eye_coarse[0].best_vref_value - ca_best_eye_coarse[1].best_vref_value  
                                            : 
                                            ca_best_eye_coarse[1].best_vref_value - ca_best_eye_coarse[0].best_vref_value;  
          
          // Calculate the smaller coarse training vref result of CA0 and CA1
          ca_vref_diff_info.ca_coarse_vref = (ca_vref_diff_info.ca_vref_flag) ? 
                                              ca_best_eye_coarse[1].best_vref_value 
                                              : 
                                              ca_best_eye_coarse[0].best_vref_value;
             
          // Process Vref for both left and right boundary fine training
          if (training_params_ptr->ca_vref.fine_vref_enable)   // Fine vref training is enabled
          {
             // The smaller start_fine_vref_value of CA0 and CA1
             start_fine_vref_value = (ca_vref_diff_info.ca_coarse_vref == 0) ? 0 : 
                                       (ca_vref_diff_info.ca_coarse_vref - training_params_ptr->ca_vref.coarse_vref_step);

             // The smaller max_fine_vref_value of CA0 and CA1
             max_fine_vref_value = start_fine_vref_value + 2 * training_params_ptr->ca_vref.coarse_vref_step;
          }
          else   // In case that fine vref training is disabled
          {
             start_fine_vref_value =  ca_vref_diff_info.ca_coarse_vref;
             max_fine_vref_value   =  ca_vref_diff_info.ca_coarse_vref;
          }
        
          ca_start_fine_vref_value[0] = (ca_vref_diff_info.ca_vref_flag == 0) ? start_fine_vref_value : 
                                          (start_fine_vref_value + ca_vref_diff_info.ca_vref_diff);
          ca_start_fine_vref_value[1] = (ca_vref_diff_info.ca_vref_flag == 1) ? start_fine_vref_value : 
                                         (start_fine_vref_value + ca_vref_diff_info.ca_vref_diff);
                                         
          ca_max_fine_vref_value[0]   = (ca_vref_diff_info.ca_vref_flag == 0) ? max_fine_vref_value : 
                                         (max_fine_vref_value   + ca_vref_diff_info.ca_vref_diff);
          ca_max_fine_vref_value[1]   = (ca_vref_diff_info.ca_vref_flag == 1) ? max_fine_vref_value : 
                                         (max_fine_vref_value   + ca_vref_diff_info.ca_vref_diff);                   

          /************Left boundary fine training start******************************/

          for (phy_inx = 0; phy_inx < NUM_CA_PCH; phy_inx++) 
          {
             // Fine CDC left boundary starting value cannot be less than 0
             if (ca_best_eye_coarse[phy_inx].max_eye_left_boundary_cdc_value == 0) {
                left_start_cdc_value[phy_inx] = 0;
             }
             else {
                left_start_cdc_value[phy_inx] = ca_best_eye_coarse[phy_inx].max_eye_left_boundary_cdc_value - 
                                                training_params_ptr->ca_vref.coarse_cdc_step;
             }
             
             // Write coarse CDC values into CA PHY CSR 
             DDR_PHY_hal_cfg_cdc_slave_wr (reg_offset_ddr_phy[phy_inx], 
                                            left_start_cdc_value[phy_inx],
                                            1,  // 1 = coarse delay, 0 = fine delay
                                            1,  // 1 = HP mode,      0 = LP mode 
                                            cs 
                                           );

             DDRSS_CDC_Retimer (ddr,
                                cs,
                                left_start_cdc_value[phy_inx], // Coarse CDC delay value 
                                0,                             // In coarse training, fine delay is 0
                                0, // wrlvl_coarse_delay
                                0, // wrlvl_fine_delay
                                reg_offset_ddr_phy[phy_inx],    // Base address of CA0 or CA1
                                clk_freq_khz
                               );
          }  

          DDRSS_Vref_CDC_Fine_Schmoo (ddr,
                                      ca_best_eye_coarse,
                                      ch, 
                                      cs, 
                                      training_data_ptr,
                                      training_params_ptr,
                                      local_vars->left_fine_fail_count_table,
                                      local_vars->left_boundary_fine_cdc_value,
                                      start_fine_vref_value,
                                      max_fine_vref_value,
                                      &ca_vref_diff_info,
                                      left_start_cdc_value, 
                                      0,            // 0:Left side fine training; 1:Right side fine training
                                      clk_freq_khz
                                     );

          /************Right boundary fine training start******************************/
          
          for (phy_inx = 0; phy_inx < NUM_CA_PCH; phy_inx++) 
          {
             // Determing right boundary CDC start scanning value
             right_start_cdc_value[phy_inx] = ca_best_eye_coarse[phy_inx].max_eye_right_boundary_cdc_value;

             // Write coarse CDC values into CA0 PHY register
             DDR_PHY_hal_cfg_cdc_slave_wr (reg_offset_ddr_phy[phy_inx], 
                                            right_start_cdc_value[phy_inx],
                                            1,  // 1 = coarse delay, 0 = fine delay
                                            1,  // 1 = HP mode,      0 = LP mode 
                                            cs 
                                           );

             DDRSS_CDC_Retimer (ddr,
                                cs,
                                right_start_cdc_value[phy_inx],    // Coarse CDC delay value 
                                0,               // In coarse training, fine delay is 0
                                0, // wrlvl_coarse_delay
                                0, // wrlvl_fine_delay
                                reg_offset_ddr_phy[phy_inx],        // Base address of CA0 or CA1
                                clk_freq_khz
                               );
          }  

          DDRSS_Vref_CDC_Fine_Schmoo (ddr,
                                      ca_best_eye_coarse,
                                      ch, 
                                      cs, 
                                      training_data_ptr,
                                      training_params_ptr,
                                      local_vars->right_fine_fail_count_table,
                                      local_vars->right_boundary_fine_cdc_value,
                                      start_fine_vref_value,
                                      max_fine_vref_value,
                                      &ca_vref_diff_info,
                                      right_start_cdc_value, 
                                      1,            // 0:Left side fine training; 1:Right side fine training
                                      clk_freq_khz
                                     );
                                     
          best_vref_sum_per_byte[0] = 0;
          best_vref_sum_per_byte[1] = 0; 
          
          for (bit = 0; bit < PINS_PER_PHY; bit++)
          {   
              DDRSS_Post_Histogram_Fine_Best_Eye_Cal (ca_best_eye_fine,
                                                      local_vars->left_boundary_fine_cdc_value,
                                                      local_vars->right_boundary_fine_cdc_value,           
                                                      ca_start_fine_vref_value[0], 
                                                      ca_max_fine_vref_value[0],   
                                                      training_data_ptr,
                                                      training_params_ptr,
                                                      0,  // Training type is CA vref training 
                                                      0,  //phy_inx
                                                      bit 
                                                     );

              DDRSS_Post_Histogram_Fine_Best_Eye_Cal (ca_best_eye_fine,
                                                      local_vars->left_boundary_fine_cdc_value,
                                                      local_vars->right_boundary_fine_cdc_value,           
                                                      ca_start_fine_vref_value[1], 
                                                      ca_max_fine_vref_value[1],   
                                                      training_data_ptr,
                                                      training_params_ptr,
                                                      0,  // Training type is CA vref training 
                                                      1,  // phy_inx
                                                      bit                                                      
                                                     );
              best_vref_sum_per_byte[0] += ca_best_eye_fine [0][bit].best_vref_value;
              best_vref_sum_per_byte[1] += ca_best_eye_fine [1][bit].best_vref_value;             
          }
          
          best_vref_average_per_byte[0] = best_vref_sum_per_byte[0] / PINS_PER_PHY;
          best_vref_average_per_byte[1] = best_vref_sum_per_byte[1] / PINS_PER_PHY;

                                                 
          for (phy_inx = 0; phy_inx < NUM_CA_PCH; phy_inx++) 
          {             
             // Write final coarse and fine CDC values into CA PHY CSR
             final_coarse_delay[phy_inx] = (left_start_cdc_value[phy_inx] + right_start_cdc_value[phy_inx]) / 2;
             ca_best_eye_coarse[phy_inx].best_cdc_value  = final_coarse_delay[phy_inx];

             // Since coarse CDC value increasing 1 equal to fine CDC value increasing 4,
             // in the case of odd_flag is 1, another 2 is required to add for fine cdc delay
             final_fine_delay[phy_inx]   = perbit_mid_min[phy_inx];// + (coarse_odd_flag * 2); 

            // Write coarse CDC value
            DDR_PHY_hal_cfg_cdc_slave_wr (reg_offset_ddr_phy[phy_inx], 
                                           final_coarse_delay[phy_inx],
                                           1, // 1 = coarse delay, 0 = fine delay
                                           1,  // 1 = HP mode,      0 = LP mode 
                                           cs 
                                          );
            // Write fine CDC value
            DDR_PHY_hal_cfg_cdc_slave_wr (reg_offset_ddr_phy[phy_inx], 
                                           final_fine_delay[phy_inx],
                                           0, // 1 = coarse delay, 0 = fine delay
                                           1,  // 1 = HP mode,      0 = LP mode 
                                           cs 
                                          );

             DDRSS_CDC_Retimer (ddr,
                                cs,
                                final_coarse_delay[phy_inx],   // Coarse CDC delay value 
                                final_fine_delay[phy_inx],     // In coarse training, fine delay is 0
                                0, // wrlvl_coarse_delay
                                0, // wrlvl_fine_delay
                                reg_offset_ddr_phy[phy_inx],
                                clk_freq_khz                                
                               );
         } // for (phy_inx 
       }   // End of fine training enable
       if (training_params_ptr->ca_vref.cdc_training_enable == 1)
       {
         for (phy_inx = 0; phy_inx < NUM_CA_PCH; phy_inx++) 
         {
           ca_vref_cdc_delay_in_ps = ((final_coarse_delay[phy_inx] * COARSE_STEP_IN_PS) + 
                                     (final_fine_delay[phy_inx]   * FINE_STEP_IN_PS));
              
           freq_inx = DDRSS_Get_Freq_Index (ddr, clk_freq_khz);
           
           // We scale only from band3 to band2.
           if(clk_freq_khz < F_RANGE_3)
           {             
             // For 547MHz only.
             ca_vref_cdc_delay_in_ps = ((clk_freq_khz * ca_vref_cdc_delay_in_ps) / F_RANGE_2);
             DDRSS_midpoint_to_CDC_lpddr4 (ddr,
                                           ca_vref_cdc_delay_in_ps, 
                                           clk_freq_khz,
                                           ch, 
                                           cs, 
                                           training_data_ptr, 
                                           0, /*Training type = CA VREF*/ 
                                           phy_inx,
                                           1);
                                           
             // Put back original value for 777MHz.
             ca_vref_cdc_delay_in_ps = ((final_coarse_delay[phy_inx] * COARSE_STEP_IN_PS) + 
                                     (final_fine_delay[phy_inx]   * FINE_STEP_IN_PS));                                           
           }
           
           //TODO: This needs to be done no matter the fine_training_enable setting.
           DDRSS_midpoint_to_CDC_lpddr4 (ddr,
                                         ca_vref_cdc_delay_in_ps, 
                                         clk_freq_khz,
                                         ch, 
                                         cs, 
                                         training_data_ptr, 
                                         0, /*Training type = CA VREF*/ 
                                         phy_inx,
                                         freq_inx);
           DDR_PHY_hal_cfg_wrlvlext_ctl_dqs_dq_struct_half (reg_offset_ddr_phy[phy_inx],
                                                            prfs_index, 
                                                            dqs_half_struct, // dqs struct half cycle
                                                            dq_half_struct); // dq struct half cycle
         }
             
       }
       for (phy_inx = 0; phy_inx < NUM_CA_PCH; phy_inx++) 
       {
             // Write final coarse and fine CDC values into structure and  training_data
            ca_best_eye_coarse[phy_inx].best_cdc_value                                  = final_coarse_delay[phy_inx];    
            training_data_ptr->results.ca_vref.coarse_cdc [prfs_index][ch][cs][phy_inx] = final_coarse_delay[phy_inx];
            training_data_ptr->results.ca_vref.fine_cdc   [prfs_index][ch][cs][phy_inx] = final_fine_delay[phy_inx];
            training_data_ptr->results.ca_vref.fine_vref  [prfs_index][ch][cs][phy_inx] = best_vref_average_per_byte[phy_inx];  
    
            // copy the rank0 vref value to rank1 to store in data structure    
            training_data_ptr->results.ca_vref.fine_vref  [prfs_index][ch][1][phy_inx] = best_vref_average_per_byte[phy_inx];
       }

       // MC disable the CA CLK pulse mode, enable the continuous running clock
       BIMC_CA_Training_Pulse_Ctrl (ch, 0x0); 

       }   // End of coarse training flag
    // When one of the CA fail flag is set to 1, return FALSE
    else   {
       return FALSE;
    } 

    return TRUE;
}

