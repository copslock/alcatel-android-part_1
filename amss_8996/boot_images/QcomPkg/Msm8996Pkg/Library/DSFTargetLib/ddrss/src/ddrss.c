/****************************************************************************
 QUALCOMM Proprietary Design Data
 Copyright (c) 2014, Qualcomm Technologies Incorporated. All rights reserved.
 ****************************************************************************/
/*==============================================================================
                                EDIT HISTORY

$Header: //components/rel/boot.xf/1.0/QcomPkg/Msm8996Pkg/Library/DSFTargetLib/ddrss/src/ddrss.c#7 $
$DateTime: 2016/08/19 00:49:20 $
$Author: pwbldsvc $
================================================================================
when       who          what, where, why
--------   ---          --------------------------------------------------------
05/04/14   arindamm     First edit history header. Add new entries at top.
================================================================================*/

#include "ddrss.h"
#include "dtts_load_ram.h"
#include "target_config.h"


/* Define a neat way to trigger a compile time error based on passed
 * expression evaluating being true or false */
#define COMPILE_TIME_ASSERTION(exp) ((void)sizeof(char[1 - 2*!!((exp)-1)]))


//================================================================================================//
// DDR Initialization
//================================================================================================//
boolean HAL_DDR_Init (DDR_STRUCT *ddr,
                      DDR_CHANNEL channel, 
                      DDR_CHIPSELECT chip_select,
                      uint32 clk_freq_khz)
{
   DDR_CHANNEL ch_1hot = DDR_CH_NONE;
   uint8 ch = 0;
   uint8 in_self_refresh =0;
   uint32 msa_lock_value = 0;
   uint32 protns_lock_value = 0;   
   uint32 lock_value = 0;

   EXTENDED_CDT_STRUCT *ecdt;
       
  /* Trigger compile time check to ensure DDR_STRUCT size does not exceed
   * SW allocation of same structure.*/
  COMPILE_TIME_ASSERTION(sizeof(DDR_STRUCT) < 7000 );
  COMPILE_TIME_ASSERTION(sizeof(ddr->cdt_params) < 512 /*Total cdt and eCDT = 1024*/ /*min cdt: 489*/);
  COMPILE_TIME_ASSERTION(sizeof(ddr->extended_cdt_runtime) < 512 /*Total cdt and eCDT = 1024*/ /*min eCDT: 385*/);
  COMPILE_TIME_ASSERTION(sizeof(ddr->flash_params.training_data) < 4051 );
  
  /* Trigger compile time check to ensure local_vars size does not exceed 64KB. */
  COMPILE_TIME_ASSERTION(sizeof(ddrss_rdwr_dqdqs_local_vars) < 65536);
  COMPILE_TIME_ASSERTION(sizeof(ddrss_ca_vref_local_vars) < 65536);
  
   // Fill in the current version numbers for the DDR System Firmware, the BIMC Core and the PHY core.
   ddr->version            = TARGET_DDR_SYSTEM_FIRMWARE_VERSION;
   ddr->ctlr.version.arch  = TARGET_BIMC_ARCH_VERSION;
   ddr->ctlr.version.major = TARGET_BIMC_CORE_MAJOR_VERSION;
   ddr->ctlr.version.minor = TARGET_BIMC_CORE_MAJOR_VERSION;
   ddr->phy.version.major  = TARGET_PHY_CORE_MAJOR_VERSION;
   ddr->phy.version.minor  = TARGET_PHY_CORE_MAJOR_VERSION;

   ecdt = (EXTENDED_CDT_STRUCT *)((size_t)(ddr->ecdt_input));

   // Execute Pre-Init function for workarounds, if any.
   DDRSS_Pre_Init(ddr, ecdt);

   // BIMC one-time settings
   BIMC_Config(ddr);

   #if TARGET_SILICON // Don't configure Phy & clock controller on emulation --- they are not included in the HW build
   // DDR PHY and CC one-time settings
   DDR_PHY_CC_Config(ddr);
   if(ecdt != NULL)
   {
      DDR_PHY_CC_eCDT_Override(ddr, ecdt, channel);
   }
   #endif // #if TARGET_SILICON 
   
   BIMC_Pre_Init_Setup (ddr, channel, chip_select, clk_freq_khz);

   #if TARGET_SILICON // Don't configure Phy & clock controller on emulation --- they are not included in the HW build
   DDR_PHY_CC_init (ddr, channel, clk_freq_khz);  //workaround 19.2MHz SW switching hang issue.
   #endif // #if TARGET_SILICON

   // IO Calibration workaround for 8996 V1 and V2.
//   PHY_IO_Cal_Workaround_En_Dis(channel, 1);  // 1 for enable.
   
   //detects if we are in self-refresh
   in_self_refresh = (((HWIO_INXF(REG_OFFSET_SHKE(0), SHKE_DRAM_STATUS, SW_SELF_RFSH)) &
                       (HWIO_INXF(REG_OFFSET_SHKE(1), SHKE_DRAM_STATUS, SW_SELF_RFSH))) |
                      ((HWIO_INXF(REG_OFFSET_SHKE(0), SHKE_DRAM_STATUS, WDOG_SELF_RFSH)) &
                       (HWIO_INXF(REG_OFFSET_SHKE(1), SHKE_DRAM_STATUS, WDOG_SELF_RFSH))));   

   // If it is a warm reset we just need to do a warm-bootup . 
   // 'WDOG_SELF_RFSH' when set indicates that the memory controller is currently in self refresh due to a watchdog reset event
   // and requires a manual 'EXIT_SELF_REFRESH' trigger to exit from the forced self refresh state.
   if ( in_self_refresh == 1) 
   {
      // Disable clamps.
      ddr_mpm_config_ebi1_freeze_io(0);
        
       BIMC_Exit_Self_Refresh (ddr, channel, (DDR_CHIPSELECT)ddr->cdt_params[ch].common.populated_chipselect);
       BIMC_Memory_Device_Init (ddr, ecdt, channel, (DDR_CHIPSELECT)ddr->cdt_params[ch].common.populated_chipselect, clk_freq_khz, in_self_refresh);   		   
   } 
   else 
   {   // If BIMC is not SW_SELF_REFRSH, we need to do a cold boot style init
       //assert and deassert RESET_n pin to reset DRAM device in case lock-up happens inside DRAM after Wdog reset   
       HWIO_OUTX (DDR_SS_BASE + SEQ_DDR_SS_DDR_REG_DDR_SS_REGS_OFFSET, DDR_SS_REGS_RESET_CMD, 1);
       BIMC_Memory_Device_Init (ddr, ecdt, channel, chip_select, clk_freq_khz, in_self_refresh);  

       //Topology detection only during cold boot: 
       for (ch = 0; ch < 2; ch++)
       {
           ch_1hot = CH_1HOT(ch);
           
           if ((channel >> ch) & 0x1)
           {
               ddr->cdt_params[ch].common.populated_chipselect = BIMC_DDR_Topology_Detection (ddr, ch_1hot, chip_select);
           }
       }
   }

   for (ch = 0; ch < 2; ch++)
   {
      ch_1hot = CH_1HOT(ch);

      if ((channel >> ch) & 0x1)
      {
         BIMC_Post_Init_Setup (ddr, ch_1hot, (DDR_CHIPSELECT)ddr->cdt_params[ch].common.populated_chipselect);
      }
   }
   
   // During training and restore boots (cold boot), apply DRAM specific settings
   if((in_self_refresh != 1) && (ecdt != NULL))
   {
      DDRSS_DRAM_Specific_Settings(ddr, ecdt, channel, clk_freq_khz);
   }
   
   // CSR locking   
   for (ch = 0; ch < 2; ch++)
   {
      if ((channel >> ch) & 0x1)
      {
         lock_value = (ch ==0) ? 0x00010000 : 0x00020000;
         msa_lock_value = (HWIO_INXF (REG_OFFSET_GLOBAL1, BRIC_GLOBAL1_BRIC_MSA_LOCKS, LOCK )) | lock_value;
         protns_lock_value = (HWIO_INXF (REG_OFFSET_GLOBAL1, BRIC_GLOBAL1_BRIC_PROTNS_LOCKS, LOCK )) | lock_value ; 		 
         HWIO_OUTXF (REG_OFFSET_GLOBAL1, BRIC_GLOBAL1_BRIC_MSA_LOCKS, LOCK, msa_lock_value);
         HWIO_OUTXF (REG_OFFSET_GLOBAL1, BRIC_GLOBAL1_BRIC_PROTNS_LOCKS, LOCK, protns_lock_value); 
      }
   }
   
   return TRUE;
}


//================================================================================================//
// DDR Software Self Refresh Enter
//================================================================================================//
boolean HAL_DDR_Enter_Self_Refresh (DDR_STRUCT *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select)
{
   BIMC_Enter_Self_Refresh (ddr, channel, chip_select);

   return TRUE;
}

//================================================================================================//
// DDR Software Self Refresh Exit
//================================================================================================//
boolean HAL_DDR_Exit_Self_Refresh (DDR_STRUCT *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select)
{
   BIMC_Exit_Self_Refresh (ddr, channel, chip_select);

   return TRUE;
}

//================================================================================================//
// DDR Device Mode Register Read
//================================================================================================//
uint32 HAL_DDR_Read_Mode_Register (DDR_STRUCT *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select,
                                   uint32 MR_addr)
{
   return BIMC_MR_Read (channel, chip_select, MR_addr);
}

//================================================================================================//
// DDR Device Mode Register Write
//================================================================================================//
boolean HAL_DDR_Write_Mode_Register (DDR_STRUCT *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select,
                                     uint32 MR_addr, uint32 MR_data)
{
   BIMC_MR_Write (channel, chip_select, MR_addr, MR_data);

   return TRUE;
}

//================================================================================================//
// ZQ Calibration
//================================================================================================//
boolean HAL_DDR_ZQ_Calibration (DDR_STRUCT *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select)
{
   BIMC_ZQ_Calibration (ddr, channel, chip_select);

   return TRUE;
}

//================================================================================================//
// Enter Power Collapse
//================================================================================================//
boolean HAL_DDR_Enter_Power_Collapse(DDR_STRUCT *ddr, DDR_CHANNEL channel, DDR_POWER_CLPS_MODE collapse_mode, 
                                     uint32 clk_freq_khz)
{
    if(collapse_mode == DDR_POWER_CLPS_MODE_BIMC_ONLY)
    {
        // Put DRAM into self-refresh
        BIMC_Enter_Power_Collapse (ddr, channel);
        
        // Enable clamps.
        ddr_mpm_config_ebi1_freeze_io(1);
        
        // Turn on cfg clock before accessing DDRSS CSRs
        ddr_external_set_ddrss_cfg_clk (TRUE);
        
        // Modify band0 performance settings to utilize LPCDC
        DDR_PHY_hal_hpcdc_enable (0);

        // Turn off cfg clock after accessing DDRSS CSRs
        ddr_external_set_ddrss_cfg_clk (FALSE);
    }
    
    if((collapse_mode == DDR_POWER_CLPS_MODE_PHY_ONLY) && (((ddr->misc.platform_id == PLATFORM_ID_ISTARI) && (ddr->misc.chip_version >= 0x0200)) || (ddr->misc.platform_id == PLATFORM_ID_RADAGAST)))
    { 
        // Turn on cfg clock before accessing DDRSS CSRs
        ddr_external_set_ddrss_cfg_clk (TRUE);

        // Call the DDR PHY Pre collapse routine
        DDR_PHY_rtn_pcc_pre_collapse (ddr, PHY_POWER_CLPS_RESTORE, clk_freq_khz);      

        // Turn off cfg clock after accessing DDRSS CSRs
        ddr_external_set_ddrss_cfg_clk (FALSE);
        }
 
    return TRUE;
}

//================================================================================================//
// Exit Power Collapse
//================================================================================================//
boolean HAL_DDR_Exit_Power_Collapse(DDR_STRUCT *ddr, DDR_CHANNEL channel, DDR_POWER_CLPS_MODE collapse_mode, 
                                    uint32 clk_freq_khz)
{
#if DSF_PERIODIC_TRAINING_EN
    uint32 chan;
#endif
    
    if(collapse_mode == DDR_POWER_CLPS_MODE_BIMC_ONLY)
    {
        // Turn on cfg clock before accessing DDRSS CSRs
        ddr_external_set_ddrss_cfg_clk (TRUE);
        
        //enable HPCDC master clock
        DDR_PHY_hal_hpcdc_enable (1);     
        
        DDRSS_device_reset_cmd();   // LPDDR4 :Device reset register in DDRSS gets reset during power collapse. 
        
        // Turn off cfg clock after accessing DDRSS CSRs
        ddr_external_set_ddrss_cfg_clk (FALSE);
        
        // Disable clamps.
        ddr_mpm_config_ebi1_freeze_io(0);
        
#if DSF_PERIODIC_TRAINING_EN
        // Restore the DTTS PXI control settings that do not get retained during power collapse.
        for (chan = 0; chan < NUM_CH; chan++)
        {
            // Idle pattern when DTTS has the bus.
            HWIO_OUTXI(REG_OFFSET_DTTS(chan), DTTS_CFG_PXI_CA_CMD_TABLEN, 16, 
                               HWIO_INXI(REG_OFFSET_DTTS_SRAM(chan), DTTS_SRAM_DTTS_DSRAM_WORDN, 8));
            HWIO_OUTXI(REG_OFFSET_DTTS(chan), DTTS_CFG_PXI_CA_CMD_TABLEN, 17, 
                               HWIO_INXI(REG_OFFSET_DTTS_SRAM(chan), DTTS_SRAM_DTTS_DSRAM_WORDN, 9));
            HWIO_OUTXI(REG_OFFSET_DTTS(chan), DTTS_CFG_PXI_CA_CMD_TABLEN, 18, 
                               HWIO_INXI(REG_OFFSET_DTTS_SRAM(chan), DTTS_SRAM_DTTS_DSRAM_WORDN, 10));
            HWIO_OUTXI(REG_OFFSET_DTTS(chan), DTTS_CFG_PXI_CA_CMD_TABLEN, 19, 
                               HWIO_INXI(REG_OFFSET_DTTS_SRAM(chan), DTTS_SRAM_DTTS_DSRAM_WORDN, 11));
                               
            HWIO_OUTXI(REG_OFFSET_DTTS(chan), DTTS_CFG_PXI_PHY_CTRL_PAYLOAD_TABLEN, 0, 
                               HWIO_INXI(REG_OFFSET_DTTS_SRAM(chan), DTTS_SRAM_DTTS_DSRAM_WORDN, 12));
            HWIO_OUTXI(REG_OFFSET_DTTS(chan), DTTS_CFG_PXI_PHY_CTRL_PAYLOAD_TABLEN, 1, 
                               HWIO_INXI(REG_OFFSET_DTTS_SRAM(chan), DTTS_SRAM_DTTS_DSRAM_WORDN, 13));
                               
            HWIO_OUTXI(REG_OFFSET_DTTS(chan), DTTS_CFG_PXI_PHY_CTRL_CTL_PATTERN_TABLE2_N, 31, 
                               HWIO_INXI(REG_OFFSET_DTTS_SRAM(chan), DTTS_SRAM_DTTS_DSRAM_WORDN, 14));
                               
            // Flag to ensure DTTS Imem and Dmem is loaded correctly.
            HWIO_OUTXI(REG_OFFSET_DTTS(chan), DTTS_CFG_PXI_PHY_CTRL_PAYLOAD_TABLEN, 15, 
                               HWIO_INXI(REG_OFFSET_DTTS_SRAM(chan), DTTS_SRAM_DTTS_DSRAM_WORDN, 15));
        
        }
#endif
        
        BIMC_Exit_Power_Collapse (ddr, channel);
    }
    if((collapse_mode == DDR_POWER_CLPS_MODE_PHY_ONLY) && (((ddr->misc.platform_id == PLATFORM_ID_ISTARI) && (ddr->misc.chip_version >= 0x0200)) || (ddr->misc.platform_id == PLATFORM_ID_RADAGAST)))
    {
        // Turn on continuous GCC clock per channel 
        HWIO_OUTXF2 (REG_OFFSET_DPE(0), DPE_CONFIG_6, IOSTAGE_WR_DEBUG_MODE, IOSTAGE_CA_DEBUG_MODE, 0x1, 0x1);
        HWIO_OUTXF (REG_OFFSET_DPE(0), DPE_CONFIG_4, LOAD_ALL_CONFIG, 0x1);

        // Turn on continuous GCC clock per channel 
        HWIO_OUTXF2 (REG_OFFSET_DPE(1), DPE_CONFIG_6, IOSTAGE_WR_DEBUG_MODE, IOSTAGE_CA_DEBUG_MODE, 0x1, 0x1);
        HWIO_OUTXF (REG_OFFSET_DPE(1), DPE_CONFIG_4, LOAD_ALL_CONFIG, 0x1);

        // Turn on cfg clock before accessing DDRSS CSRs
        ddr_external_set_ddrss_cfg_clk (TRUE);

        DDR_PHY_rtn_pcc_post_collapse (ddr, PHY_POWER_CLPS_RESTORE, clk_freq_khz);  
        
        // Turn off cfg clock after accessing DDRSS CSRs
        ddr_external_set_ddrss_cfg_clk (FALSE);

        // Turn off continuous GCC clock per channel 
        HWIO_OUTXF2 (REG_OFFSET_DPE(0), DPE_CONFIG_6, IOSTAGE_WR_DEBUG_MODE, IOSTAGE_CA_DEBUG_MODE, 0x0, 0x0);
        HWIO_OUTXF (REG_OFFSET_DPE(0), DPE_CONFIG_4, LOAD_ALL_CONFIG, 0x1);

        // Turn off continuous GCC clock per channel 
        HWIO_OUTXF2 (REG_OFFSET_DPE(1), DPE_CONFIG_6, IOSTAGE_WR_DEBUG_MODE, IOSTAGE_CA_DEBUG_MODE, 0x0, 0x0);
        HWIO_OUTXF (REG_OFFSET_DPE(1), DPE_CONFIG_4, LOAD_ALL_CONFIG, 0x1);

    }
 
   return TRUE;
}

//============================================================================
// HAL_DDR_Enter_Deep_Power_Down
//============================================================================
boolean HAL_DDR_Enter_Deep_Power_Down(DDR_STRUCT *ddr, DDR_CHANNEL channel,
  DDR_CHIPSELECT chip_select)
{
    return(BIMC_Enter_Deep_Power_Down(ddr, channel, chip_select));
}

//============================================================================
// HAL_DDR_Exit_Deep_Power_Down
//============================================================================
boolean HAL_DDR_Exit_Deep_Power_Down(DDR_STRUCT *ddr, DDR_CHANNEL channel,
  DDR_CHIPSELECT chip_select, uint32 clkspeed)
{
    return(BIMC_Exit_Deep_Power_Down(ddr, channel, chip_select));
}

//============================================================================
// HAL_DDR_IOCTL
//============================================================================
boolean HAL_DDR_IOCTL (DDR_STRUCT *ddr, IOCTL_CMD ioctl_cmd, IOCTL_ARG *ioctl_arg)
{
    boolean return_value;

    switch (ioctl_cmd)
    {
        case IOCTL_CMD_GET_CORE_REGISTERS_FOR_CRASH_DEBUG:
            ioctl_arg->result_code = IOCTL_RESULT_COMMAND_NOT_IMPLEMENTED;
            return_value = FALSE;
            break;

        /* For the IOCTL_CMD_GET_HIGHEST_BANK_BITcommand, ioctl_arg->results
           is where this API will write the highest DRAM bank bit. */
        case IOCTL_CMD_GET_HIGHEST_BANK_BIT:
            // Calculate highest DRAM bank bit in the 32-bit system address here.
            // Starting with LSB traveling towards MSB in the system address, we have
            //
            // Byte Address: 1b for x16 device, 2b for x32 device.
            // Remove SA[10] for interleave, assign next set of LSBs for column address bits, which is 10 for LPDDR4 parts supported.
            // Next set of LSBs is bank address, 3 bits.
            // Next set of LSBs is row address.
            //
            // This code assumes total symmetry of geometry across all dies, across all channels.
            // Given that assumption, this API simply resolves to:
            //
            // If interleave is enabled,  SA[15:13] are DRAM BA[2:0] i.e. highest bank address bit is 15. (Channel interleave bit is SA[10]).
            // If interleave is disabled, SA[14:12] are DRAM BA[2:0] i.e. highest bank address bit is 14. (Channel interleave bit is SA[30]).

            ioctl_arg->results = (ddr->cdt_params[0].common.interleave_en) ? 15 : 14;

            // No need to update ioctl_arg->result_code, since we are returning return_value as TRUE, caller software
            // should ignore result_code.
            // ioctl_arg->result_code = IOCTL_RESULT_COMMAND_NOT_IMPLEMENTED;
            return_value = TRUE;
            break;

        case IOCTL_CMD_TEMP_CTRL_MIN_FREQ:
            ioctl_arg->results = BIMC_Temp_Ctrl_Min_Freq (ddr, DDR_CH_BOTH);

            return_value = TRUE;
            break;

        /* Bad command, return appropriate result code. */
        default:
            ioctl_arg->result_code = IOCTL_RESULT_ILLEGAL_COMMAND;
            return_value = FALSE;
    }

    return return_value;
}


//================================================================================================//
// DDR Post Boot Training Setup.
//================================================================================================//
boolean DDRSS_Post_Boot_Training(DDR_STRUCT *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select)
{
    DDR_CHIPSELECT qualified_cs = (DDR_CHIPSELECT)(chip_select & ddr->cdt_params[0].common.populated_chipselect);
    
    // Refresh rate control through TUF bit override
    BIMC_Refresh_Rate_Ctrl (channel, qualified_cs);
    // Enables DownTime requests
    BIMC_Downtime_Request_Ctrl ( channel, 1);
    
    // Add Power Collapse Save mode after training is done.
    if(((ddr->misc.platform_id == PLATFORM_ID_ISTARI) && (ddr->misc.chip_version >= 0x0200)) || (ddr->misc.platform_id == PLATFORM_ID_RADAGAST))
    {
        ddr_printf(DDR_NORMAL, "DDR PHY Power Collapse SAVE\n\n");
        
        // 200MHz is ok to use, since this parameter does not matter for SAVE mode.
        ddr_external_set_clk_speed (200000);
        DDR_PHY_rtn_pcc_pre_collapse (ddr, PHY_POWER_CLPS_SAVE, 200000);
        DDR_PHY_rtn_pcc_post_collapse(ddr, PHY_POWER_CLPS_SAVE, 200000);
    }
    
   return TRUE;
}



