/** @file DDRDebugImageLibNull.c
  
  DDR DebugImage Functions

  Copyright (c) 2015, Qualcomm Technologies, Inc. All rights reserved.
**/

/*=============================================================================
                              EDIT HISTORY


when       who       what, where, why
--------   ---       -----------------------------------------------------------
24/24/15   sng  Initial revision.

=============================================================================*/

#include "boot_sbl_if.h"

/*!
 * Stub for boot_hand_control_to_DDR_Debug_Image_main()
 *
 * @param bl_shared_data
 *    The shared bootloader information.
 *
*****************************************************************************/
void
boot_hand_control_to_DDR_Debug_Image_main (bl_shared_data_type *bl_shared_data)
{
  (void)bl_shared_data;
}

void sendDatatoDDI(void * buffer,uint32 size, void* targs, void *training_params_ptr)
{
        
  
}

void changeDRAMSettingsforDDI(uint32 clk_in_khz)
{
    
}
