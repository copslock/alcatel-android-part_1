#include "HALhwio.h"
#include "sec_img_auth.h"
#include "secboot.h"
#include "msmhwiobase.h"
#include "secboot_hwio.h"
#include "secboot_hw.h"


/* REquired only for 8996.*/
void sec_img_auth_re_enable_debug(secboot_verified_info_type v_info)
{
  switch (v_info.enable_debug)
  {
    case SECBOOT_DEBUG_DISABLE:
    {
     HWIO_OVERRIDE_1_OUT(0x0);
     break;
    }
    case SECBOOT_DEBUG_ENABLE:
    {
     HWIO_OVERRIDE_1_OUT(HWIO_OVERRIDE_1_RMSK);
     break;
    }
    case SECBOOT_DEBUG_NOP:
    {
     break;
    }
    default:
     HWIO_OVERRIDE_1_OUT(0x0);
     break;
  }
}
