/*=============================================================================

                       Apt Shared Functions Producer

GENERAL DESCRIPTION
  This file contains definitions of functions for boot shared functions.

Copyright 2014, 2015 by QUALCOMM Technologies Inc.  All Rights Reserved.
=============================================================================*/


/*=============================================================================

                            EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

when       who     what, where, why
--------   ---     ------------------------------------------------------------
10/29/15   pbarot  common and pmic functions register in seperate function  
06/17/15   zf      re-Version for STI
07/14/14   ck      Initial creation
=============================================================================*/


/*=============================================================================

                            INCLUDE FILES FOR MODULE

=============================================================================*/

#include "apt_shared_functions.h"
#include "boot_uart.h"
#include "DALFramework.h"

static void pmic_shared_functions_register(void)
{

  shared_functions_table->pmic_shared_apis.pm_gpio_status_get = &pm_gpio_status_get;
#if 0
  shared_functions_table->pmic_shared_apis.pm_gpio_config_bias_voltage = &pm_gpio_config_bias_voltage;
  shared_functions_table->pmic_shared_apis.pm_gpio_config_digital_input = &pm_gpio_config_digital_input;
  shared_functions_table->pmic_shared_apis.pm_gpio_config_digital_output = &pm_gpio_config_digital_output;
  shared_functions_table->pmic_shared_apis.pm_gpio_config_digital_input_output = &pm_gpio_config_digital_input_output;
  shared_functions_table->pmic_shared_apis.pm_gpio_set_volt_source = &pm_gpio_set_volt_source;
  // shared_functions_table->pmic_shared_apis.pm_gpio_config_mode_selection = &pm_gpio_config_mode_selection;
  shared_functions_table->pmic_shared_apis.pm_gpio_set_output_buffer_configuration = &pm_gpio_set_output_buffer_configuration;
  shared_functions_table->pmic_shared_apis.pm_gpio_set_inversion_configuration = &pm_gpio_set_inversion_configuration;
  shared_functions_table->pmic_shared_apis.pm_gpio_set_current_source_pulls = &pm_gpio_set_current_source_pulls;
  shared_functions_table->pmic_shared_apis.pm_gpio_set_ext_pin_config = &pm_gpio_set_ext_pin_config;
  shared_functions_table->pmic_shared_apis.pm_gpio_set_output_buffer_drive_strength = &pm_gpio_set_output_buffer_drive_strength;
  shared_functions_table->pmic_shared_apis.pm_gpio_set_source_configuration = &pm_gpio_set_source_configuration;
  shared_functions_table->pmic_shared_apis.pm_gpio_set_mux_ctrl = &pm_gpio_set_mux_ctrl;
  shared_functions_table->pmic_shared_apis.pm_gpio_irq_enable = &pm_gpio_irq_enable;
  shared_functions_table->pmic_shared_apis.pm_gpio_irq_clear = &pm_gpio_irq_clear;
  shared_functions_table->pmic_shared_apis.pm_gpio_irq_set_trigger = &pm_gpio_irq_set_trigger;
  shared_functions_table->pmic_shared_apis.pm_gpio_irq_status = &pm_gpio_irq_status;
  shared_functions_table->pmic_shared_apis.pm_gpio_set_dig_in_ctl = &pm_gpio_set_dig_in_ctl;
 // boot_uart_tx("[Shared Functions] PMIC_GPIO API\n\r", strlen("[Shared Functions] PMIC_GPIO API\n\r"));
#endif
#if 0
  shared_functions_table->pmic_shared_apis.pm_mpp_status_get = &pm_mpp_status_get;
  shared_functions_table->pmic_shared_apis.pm_mpp_config_digital_input = &pm_mpp_config_digital_input;
  shared_functions_table->pmic_shared_apis.pm_mpp_config_digital_output = &pm_mpp_config_digital_output;
  shared_functions_table->pmic_shared_apis.pm_mpp_config_digital_inout = &pm_mpp_config_digital_inout;
  shared_functions_table->pmic_shared_apis.pm_mpp_config_dtest_output = &pm_mpp_config_dtest_output;
  shared_functions_table->pmic_shared_apis.pm_mpp_config_analog_input = &pm_mpp_config_analog_input;
  shared_functions_table->pmic_shared_apis.pm_mpp_config_analog_output = &pm_mpp_config_analog_output;
  shared_functions_table->pmic_shared_apis.pm_mpp_config_i_sink = &pm_mpp_config_i_sink;
  shared_functions_table->pmic_shared_apis.pm_mpp_config_atest = &pm_mpp_config_atest;
  shared_functions_table->pmic_shared_apis.pm_mpp_enable = &pm_mpp_enable;
  shared_functions_table->pmic_shared_apis.pm_mpp_irq_enable = &pm_mpp_irq_enable;
  shared_functions_table->pmic_shared_apis.pm_mpp_irq_clear = &pm_mpp_irq_clear;
  shared_functions_table->pmic_shared_apis.pm_mpp_irq_set_trigger = &pm_mpp_irq_set_trigger;
  shared_functions_table->pmic_shared_apis.pm_mpp_irq_status = &pm_mpp_irq_status;
  shared_functions_table->pmic_shared_apis.pm_mpp_set_list_mpp_with_shunt_cap = &pm_mpp_set_list_mpp_with_shunt_cap;
  //boot_uart_tx("[Shared Functions] PMIC_MPP API\n\r",strlen("[Shared Functions] PMIC_MPP API\n\r"));
  // shared_functions_table->pmic_shared_apis.pm_dev_get_mpp_with_shunt_cap_list_status_for_device = &pm_dev_get_mpp_with_shunt_cap_list_status_for_device;
#endif
#if 0
  shared_functions_table->pmic_shared_apis.pm_ibb_lcd_amoled_sel = &pm_ibb_lcd_amoled_sel;
  shared_functions_table->pmic_shared_apis.pm_ibb_ibb_module_rdy = &pm_ibb_ibb_module_rdy;
  shared_functions_table->pmic_shared_apis.pm_ibb_config_ibb_ctrl = &pm_ibb_config_ibb_ctrl;
  shared_functions_table->pmic_shared_apis.pm_ibb_set_soft_strt_chgr_resistor = &pm_ibb_set_soft_strt_chgr_resistor;
  shared_functions_table->pmic_shared_apis.pm_ibb_set_swire_output_pulse = &pm_ibb_set_swire_output_pulse;
  shared_functions_table->pmic_shared_apis.pm_ibb_config_output_volt = &pm_ibb_config_output_volt;
  // shared_functions_table->pmic_shared_apis.pm_ibb_config_pwrup_pwrdn_dly = &pm_ibb_config_pwrup_pwrdn_dly;
 // boot_uart_tx("[Shared Functions] LOADED PMIC_IBB API\n\r",strlen("[Shared Functions] LOADED PMIC_IBB API\n\r"));
  shared_functions_table->pmic_shared_apis.pm_ibb_get_ibb_status = &pm_ibb_get_ibb_status;
  shared_functions_table->pmic_shared_apis.pm_lab_lcd_amoled_sel = &pm_lab_lcd_amoled_sel;
  shared_functions_table->pmic_shared_apis.pm_lab_lab_module_rdy = &pm_lab_lab_module_rdy;
  shared_functions_table->pmic_shared_apis.pm_lab_en_lab_module = &pm_lab_en_lab_module;
  shared_functions_table->pmic_shared_apis.pm_lab_ibb_rdy_en = &pm_lab_ibb_rdy_en;
  shared_functions_table->pmic_shared_apis.pm_lab_config_precharge_ctrl = &pm_lab_config_precharge_ctrl;
  shared_functions_table->pmic_shared_apis.pm_lab_get_lab_status = &pm_lab_get_lab_status;
  shared_functions_table->pmic_shared_apis.pm_lab_config_output_volt = &pm_lab_config_output_volt;
  //boot_uart_tx("[Shared Functions] PMIC_LAB API\n\r",strlen("[Shared Functions] PMIC_GPIO API\n\r"));
  shared_functions_table->pmic_shared_apis.pm_lpg_pwm_enable = &pm_lpg_pwm_enable;
  shared_functions_table->pmic_shared_apis.pm_lpg_pwm_output_enable = &pm_lpg_pwm_output_enable;
  shared_functions_table->pmic_shared_apis.pm_lpg_pwm_set_pwm_value = &pm_lpg_pwm_set_pwm_value;
  shared_functions_table->pmic_shared_apis.pm_lpg_pwm_set_pre_divide = &pm_lpg_pwm_set_pre_divide;
  shared_functions_table->pmic_shared_apis.pm_lpg_pwm_clock_sel = &pm_lpg_pwm_clock_sel;
  shared_functions_table->pmic_shared_apis.pm_lpg_set_pwm_bit_size = &pm_lpg_set_pwm_bit_size;
  // shared_functions_table->pmic_shared_apis.pm_lpg_bank_enable = &pm_lpg_bank_enable;
  // shared_functions_table->pmic_shared_apis.pm_lpg_bank_select = &pm_lpg_bank_select;
  // shared_functions_table->pmic_shared_apis.pm_lpg_pwm_value_bypass_enable = &pm_lpg_pwm_value_bypass_enable;
  shared_functions_table->pmic_shared_apis.pm_lpg_pwm_src_select  = &pm_lpg_pwm_src_select ;
  shared_functions_table->pmic_shared_apis.pm_lpg_config_pwm_type = &pm_lpg_config_pwm_type;
  shared_functions_table->pmic_shared_apis.pm_lpg_pattern_config = &pm_lpg_pattern_config;
  shared_functions_table->pmic_shared_apis.pm_lpg_lut_config_set = &pm_lpg_lut_config_set;
  shared_functions_table->pmic_shared_apis.pm_lpg_lut_config_get = &pm_lpg_lut_config_get;
  shared_functions_table->pmic_shared_apis.pm_lpg_lut_config_set_array = &pm_lpg_lut_config_set_array;
  shared_functions_table->pmic_shared_apis.pm_lpg_lut_config_get_array = &pm_lpg_lut_config_get_array;
  shared_functions_table->pmic_shared_apis.pm_lpg_pwm_ramp_generator_start = &pm_lpg_pwm_ramp_generator_start;
  shared_functions_table->pmic_shared_apis.pm_lpg_pwm_lut_index_set = &pm_lpg_pwm_lut_index_set;
  shared_functions_table->pmic_shared_apis.pm_lpg_config_pause_time = &pm_lpg_config_pause_time;
  shared_functions_table->pmic_shared_apis.pm_lpg_pwm_ramp_generator_enable = &pm_lpg_pwm_ramp_generator_enable;
  shared_functions_table->pmic_shared_apis.pm_lpg_get_status = &pm_lpg_get_status;
  shared_functions_table->pmic_shared_apis.pm_lpg_set_lpg_dtest = &pm_lpg_set_lpg_dtest;
  //boot_uart_tx("[Shared Functions] PMIC_LPG API\n\r",strlen("[Shared Functions] PMIC_LPG API\n\r"));
  shared_functions_table->pmic_shared_apis.pm_vib_enable = &pm_vib_enable;
  shared_functions_table->pmic_shared_apis.pm_vib_set_volt = &pm_vib_set_volt;
 // boot_uart_tx("[Shared Functions] LOADED PMIC_VIB API\n\r",strlen("[Shared Functions] LOADED PMIC_VIB API\n\r"));
#endif

}


static void common_shared_functions_register(void)
{
   /*API's used by Loader_test */
  shared_functions_table->boot_uart_tx = &boot_uart_tx;
  shared_functions_table->boot_uart_rx = &boot_uart_rx; 
  shared_functions_table->DALSYS_Malloc = &DALSYS_Malloc;
  shared_functions_table->DALSYS_Free = &DALSYS_Free;
  shared_functions_table->DALSYS_memset = &DALSYS_memset;
  shared_functions_table->DALSYS_memscpy =&DALSYS_memscpy;
  shared_functions_table->DALSYS_BusyWait =&DALSYS_BusyWait;

}

static void apt_shared_functions_register(void)
{
  
  /* Fill in version number and magic cookie values so consumer knows table has
     been populated. */
  shared_functions_table->version = SHARED_FUNCTIONS_VERSION;
  shared_functions_table->magic_cookie_1 = SHARED_FUNCTIONS_MAGIC_COOKIE_1;
  shared_functions_table->magic_cookie_2 = SHARED_FUNCTIONS_MAGIC_COOKIE_2;
  
  /* Assign each function pointer here.  When adding new functions simply
     add to this list. */

   /* register common API's used by Loader test */
   common_shared_functions_register();

   /* register shared pmic apis */
   pmic_shared_functions_register();

   /* register shared sysdrv apis */
  
   /* register shared <subsystem> apis */
}

void apt_sti_init(uintnt entry_point )
{
  apt_shared_functions_register();
  ((void (*)())(uintnt)(entry_point))(shared_functions_table); 
}
