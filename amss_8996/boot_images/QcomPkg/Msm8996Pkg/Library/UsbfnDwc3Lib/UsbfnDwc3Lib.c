//============================================================================
/**
 * @file        UsbfnDwc3Lib.c
 * @author      kameya
 * @date        15-Feb-2013
 *
 * @brief       Platform library for the Synopsys Usbfn driver (UsbfnDwc3)
 *
 * @details     An instance of this library provides platform specific functionallity
 *              for the UsbfnDwc3 driver in order for it to remain platform independed.
 *              This is currently only used for charger detection.
 *
 * @ref         "DWC_usb3_databook 2.5(a)" and "HwProgrammingGuide - usb30 and SS PHY".
 *
 *              Copyright (c) 2013-2014 Qualcomm Technologies, Inc.
 *              All Rights Reserved.
 *              Qualcomm Confidential and Proprietary
 *
 */
//============================================================================

// ===========================================================================
//
//                            EDIT HISTORY FOR FILE
//   This section contains comments describing changes made to the module.
//   Notice that changes are listed in reverse chronological order.
//
//
// when          who     what, where, why
// ----------   -----    ----------------------------------------------------------
// 06/05/14      amitg       MSM 8994 Bring Up Updates in Vbus Override
// 04/30/2014   ar           Cleanup for NDEBUG build  
// 04/25/2014   amitg      Charger and Cable Detection Updates for MSM 8994 (Elessar)
// 03/14/2014   amitg      Updates for 8994
// 12/05/2013   ck           port to 8994.
// 08/19/2013   kameya   Proprietary charger detection and misc cleanup.
// 05/20/2013   kameya   Clean up support for ACA detection.
// 02/15/2013   kameya   Initial Revision based on UsbfnLib.c
// ===========================================================================

#include <Library/UsbfnDwc3Lib.h>
#include "UsbfnDwc3Private.h"
#include "HalusbHWIO.h"
#include <Library/UefiBootServicesTableLib.h>
#include <Protocol/EFIAdc.h>

//----------------------------------------------------------------------------
// Preprocessor Definitions and Constants
//----------------------------------------------------------------------------

#define READ_BIT(addr,reg,bit)    (HWIO_##reg##_INM(addr,HWIO_##reg##_##bit##_BMSK) >> HWIO_##reg##_##bit##_SHFT)

#define CLEAR_BIT_VERIFY(addr,reg,bit) \
    HWIO_##reg##_OUTM(addr,HWIO_##reg##_##bit##_BMSK,0x0); \
    if(READ_BIT(addr,reg,bit) != 0) { \
        DEBUG((EFI_D_ERROR,"Clear bit failed @0x%X @pos=%d\n", (HWIO_##reg##_ADDR(addr)),HWIO_##reg##_##bit##_SHFT)); \
        return 0; \
    }

#define SET_BIT_VERIFY(addr,reg,bit) \
    HWIO_##reg##_OUTM(addr,HWIO_##reg##_##bit##_BMSK,HWIO_##reg##_##bit##_BMSK); \
    if(READ_BIT(addr,reg,bit) != 1) { \
        DEBUG((EFI_D_ERROR,"Set bit failed @0x%X @pos=%d\n", (HWIO_##reg##_ADDR(addr)),HWIO_##reg##_##bit##_SHFT)); \
        return 0; \
    }

#define CLEAR_BIT(addr,reg,bit)    HWIO_##reg##_OUTM(addr,HWIO_##reg##_##bit##_BMSK,0x0)
#define SET_BIT(addr,reg,bit)      HWIO_##reg##_OUTM(addr,HWIO_##reg##_##bit##_BMSK,HWIO_##reg##_##bit##_BMSK)

//----------------------------------------------------------------------------
// Type Declarations
//----------------------------------------------------------------------------

#ifdef PHY_CHG_DETECTION

//----------------------------------------------------------------------------
// Global Data Definitions
//----------------------------------------------------------------------------

static UINTN coreAddr[1] = {
    USB30_QSCRATCH_REG_BASE
};

//----------------------------------------------------------------------------
// Static Variable Definitions
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// Externalized Function Definitions
//----------------------------------------------------------------------------

CHAR8* dci_dwc_print_charger_type(dci_dwc_chg_port_type charger_type) {

    switch(charger_type) {
        case DCI_DWC_CHG_PORT_SDP:
            return "Standard Downstream Port (SDP)";
        case DCI_DWC_CHG_PORT_CDP:
            return "Charging Downstream Port (CDP)";
        case DCI_DWC_CHG_PORT_DCP:
            return "Dedicated Charging Port (DCP)";
        case DCI_DWC_CHG_PORT_INVALID:
            return "Invalid charger";
        case DCI_DWC_CHG_PORT_UNKNOWN:
        default:
            return "Unknown charger";
    }

}

//----------------------------------------------------------------------------
// Static Function Declarations and Definitions
//----------------------------------------------------------------------------

#ifndef MDEPKG_NDEBUG
static CHAR8* print_charger_state(dci_dwc_chg_state_type charger_state) {

    switch(charger_state) {
        case DCI_DWC_CHG_ST_START:
            return "ST_START";
        case DCI_DWC_CHG_ST_BSESS_VALID:
            return "ST_BSESS_VALID";
        case DCI_DWC_CHG_ST_WAIT_FOR_DCD:
            return "ST_WAIT_FOR_DCD";
        case DCI_DWC_CHG_ST_CHARGING_PORT_DETECTION:
            return "ST_CHARGING_PORT_DETECTION";
        case DCI_DWC_CHG_ST_CHARGING_PORT_DETECTED:
            return "ST_CHARGING_PORT_DETECTED";
        case DCI_DWC_CHG_ST_SDP:
            return "ST_SDP";
        case DCI_DWC_CHG_ST_CDP:
            return "ST_CDP";
        case DCI_DWC_CHG_ST_DCP:
            return "ST_DCP";
        case DCI_DWC_CHG_ST_INVALID_CHARGER:
            return "ST_INVALID_CHARGER";
        case DCI_DWC_CHG_ST_DONE:
            return "ST_DONE";
        case DCI_DWC_CHG_ST_END:
            return "ST_END";
        default:
            return "Unknown charger state";
    }

}
#endif // MDEPKG_NDEBUG


static void dci_dwc_delay_ms(uint32 msecs) {
    gBS->Stall(1000*msecs);
}
#endif


//============================================================================
/**
 * @function  dci_enable_external_vbus_config
 *
 * Enables external VBUS configuration. This in turn manually drives D+ pull-up.
 *
 *
 *
 * @param Core Qscratch Base Address
 *
 *
 *
 * @return none
 */
//=============================================================================
static void dci_enable_external_vbus_config(UINTN QscratchBase)
{
 /*
    *Reference USB 3.0 HPG (Oct, 2014)  
    */
    //1.    Software updates HS_PHY_CTRL.UTMI_OTG_VBUS_VALID. 
    HWIO_HS_PHY_CTRL_OUTM(QscratchBase, HWIO_HS_PHY_CTRL_UTMI_OTG_VBUS_VALID_BMSK, (0x1 << HWIO_HS_PHY_CTRL_UTMI_OTG_VBUS_VALID_SHFT));
    
    //2.    Software updates HS_PHY_CTRL.SW_SESSVLD_SEL to 1 indicating that the value is driven by UTMI_OTG_VBUS_VALID.
    HWIO_HS_PHY_CTRL_OUTM(QscratchBase, HWIO_HS_PHY_CTRL_SW_SESSVLD_SEL_BMSK, (0x1 << HWIO_HS_PHY_CTRL_SW_SESSVLD_SEL_SHFT));
    
    //3.    Software set lane0_power_present
    HWIO_SS_PHY_CTRL_OUTM(QscratchBase, HWIO_SS_PHY_CTRL_LANE0_PWR_PRESENT_BMSK, (0x1 << HWIO_SS_PHY_CTRL_LANE0_PWR_PRESENT_SHFT)); 
}


//============================================================================
/**
 * @function  dci_enable_external_vbus_config
 *
 * Disables external VBUS configuration. This in turn manually drives D+ pull-down.
 *
 *
 *
 * @param Core Qscratch Base Address
 *
 *
 *
 * @return none
 */
//=============================================================================
static void dci_disable_external_vbus_config(UINTN QscratchBase)
{
 /*
    *Reference USB 3.0 HPG (Oct, 2014)  
    */

    //1.    Software updates HS_PHY_CTRL.UTMI_OTG_VBUS_VALID. 
    HWIO_HS_PHY_CTRL_OUTM(QscratchBase, HWIO_HS_PHY_CTRL_UTMI_OTG_VBUS_VALID_BMSK, (0x0 << HWIO_HS_PHY_CTRL_UTMI_OTG_VBUS_VALID_SHFT));
    
    //2.    Software updates HS_PHY_CTRL.SW_SESSVLD_SEL to 1 indicating that the value is driven by UTMI_OTG_VBUS_VALID.
    HWIO_HS_PHY_CTRL_OUTM(QscratchBase, HWIO_HS_PHY_CTRL_SW_SESSVLD_SEL_BMSK, (0x0 << HWIO_HS_PHY_CTRL_SW_SESSVLD_SEL_SHFT));
    
    //3.    Software clear lane0_power_present
    HWIO_SS_PHY_CTRL_OUTM(QscratchBase, HWIO_SS_PHY_CTRL_LANE0_PWR_PRESENT_BMSK, (0x0 << HWIO_SS_PHY_CTRL_LANE0_PWR_PRESENT_SHFT)); 
}


//============================================================================
/**
 * @function  dci_connect
 *
 * @brief Connect the device to the USB bus.
 *
 * @Note :  Connect/Disconnect is done via PMIC
 *          It can be done via the USB core when using PIPE3 PHY.
 *
 * @param Core Qscratch Base Address
 *
 * @return none
 *
 * @ref USB 3.0 Controller HPG:
 *      Chapter 4.4.2.8 - ID and VBUS override 
 *
 */
//============================================================================
void dci_connect(UINTN QscratchBase)
{
    dci_enable_external_vbus_config(QscratchBase);
}


//============================================================================
/**
 * @function  dci_disconnect
 *
 * @brief Disconnect the device from the USB bus.
 *
 * @Note :  Connect/Disconnect is done via PMIC
 *          It can be done via the USB core when using PIPE3 PHY.
 *
 * @param Core Base Address
 *
 * @return none
 *
 * @ref USB 3.0 Controller HPG:
 *      Chapter 4.4.2.8 - ID and VBUS override 

 *
 */
//============================================================================
void dci_disconnect(UINTN QscratchBase)
{
    dci_disable_external_vbus_config(QscratchBase);
}

#ifdef PHY_CHG_DETECTION
/**
 * @function  is_dcdintlch_set
 *
 * @brief check if DCDINTLCH bit of HS_PHY_IRQ_STAT is set.
 *
 * @param Addr
 *
 * @return uint32 ( TRUE / FALSE )
 *
 *
 */
static uint32 is_dcdintlch_set(UINTN Addr) {
    return (HWIO_HS_PHY_IRQ_STAT_INM(Addr,HWIO_HS_PHY_IRQ_STAT_DCDINTLCH_BMSK) >> HWIO_HS_PHY_IRQ_STAT_DCDINTLCH_SHFT);
}

/**
 * @function  enable_dcd_bc
 *
 * Enables data contact detection
 *
 *
 *
 * @param Addr
 *
 * @return uint32 ( TRUE / FALSE )
 *
 * @return none.
 */
static uint32 enable_dcd_bc(UINTN Addr) {
    DEBUG((EFI_D_INFO,"Enable data contact detection for battery charging.\n"));
    // (AG) Left the functions as Stub for future Charger Detection Code for 8994
    return 1;
}

/**
 * @function  disable_dcd_bc
 *
 * Disable data contact detection
 *
 *
 *
 * @param Addr
 *
 *
 *
 * @return uint32 ( TRUE / FALSE )
 */
static uint32 disable_dcd_bc(UINTN Addr) {

    DEBUG((EFI_D_INFO,"Disable data contact detection for battery charging. \n"));
    // (AG) Left the functions as Stub for future Charger Detection Code for 8994
    return 1;
}

/**
 * @function  enable_primary_detection
 *
 * Enables primary charger detection
 *
 *
 *
 * @param Addr
 *
 *
 *
 * @return uint32 ( TRUE / FALSE )
 */
static uint32 enable_primary_detection(UINTN Addr) {
    DEBUG((EFI_D_INFO,"Enable primary detection. \n"));
    // (AG) Left the functions as Stub for future Charger Detection Code for 8994
    return 1;
}

/**
 * @function  disable_primary_detection
 *
 * Disables primary charger detection
 *
 *
 *
 * @param Addr
 *
 *
 *
 * @return uint32 ( TRUE / FALSE )
 */
static uint32 disable_primary_detection(UINTN Addr) {
    DEBUG((EFI_D_INFO,"Disable primary detection. \n"));
    // (AG) Left the functions as Stub for future Charger Detection Code for 8994
    return 1;
}

/**
 * @function  enable_secondary_detection
 *
 * Enables Secondary charger detection
 *
 *
 *
 * @param Addr
 *
 *
 *
 * @return uint32 ( TRUE / FALSE )
 */
static uint32 enable_secondary_detection(UINTN Addr) {
    DEBUG((EFI_D_INFO,"Enable secondary detection.\n"));
    // (AG) Left the functions as Stub for future Charger Detection Code for 8994
    return 1;
}

/**
 * @function  disable_detection
 *
 * Disables charger detection
 *
 *
 *
 * @param Addr
 *
 *
 *
 * @return uint32 ( TRUE / FALSE )
 */
static uint32 disable_detection(UINTN Addr) {
    DEBUG((EFI_D_INFO,"Disable charger detection. \n"));
    // (AG) Left the functions as Stub for future Charger Detection Code for 8994
    return 1;
}

/**
 * @function  read_chgdet_output
 *
 * Returns the value read from CHGDET bit from the CHARGING_DET_OUTPUT register
 *
 *
 *
 * @param Addr
 *
 *
 *
 * @return value of CHGDET bit.
 */
static uint32 read_chgdet_output(UINTN Addr) {
    DEBUG((EFI_D_INFO, "Read CHGDET output. \n"));
    // (AG) Left the functions as Stub for future Charger Detection Code for 8994
    return 1 ;
}

/**
 * @function  read_linestate_output
 *
 * Returns the value read from LINESTATE bits from the CHARGING_DET_OUTPUT register
 *
 *
 *
 * @param Addr
 *
 *
 *
 * @return value of LINESTATE bits.
 */
static uint32 read_linestate_output(UINTN Addr) {
    DEBUG((EFI_D_INFO,"Read LINESTATE output. \n"));
    // (AG) Left the functions as Stub for future Charger Detection Code for 8994
    return 1;
}

/**
 * @function  dci_dwc_detect_charger_type
 *
 * Returns the dci_dwc_chg_port_type based charger detection algorithm
 *
 *
 *
 * @param Addr
 *
 *
 *
 * @return dci_dwc_chg_port_type - Charger port type.
 */
dci_dwc_chg_port_type dci_dwc_detect_charger_type(uint32 core_id)
{
    // For 8994, charger detection will be done from a new HW in PMIC
    // The API from PMIC is not available at this time. Assume it is connected to SDP.

    uint32 loop_count = 0;
    uint32 linestate = 0;
    //hard coded
    UINTN QscratchBaseAddr = coreAddr[0];

    dci_dwc_chg_state_type chg_sm_state = DCI_DWC_CHG_ST_SDP;
    dci_dwc_chg_port_type chg_port_type = DCI_DWC_CHG_PORT_UNKNOWN;

    // Charger detection state machine
    while((chg_sm_state != DCI_DWC_CHG_ST_END) && (loop_count++ < CHG_DETECTION_LOOP_COUNT))
    {
        DEBUG ((EFI_D_INFO, "Charger state machine: %a\n", print_charger_state(chg_sm_state)));
        switch(chg_sm_state)
        {
            case DCI_DWC_CHG_ST_START :
            {

                // If link is in device mode Run/Stop should 0
                if (READ_BIT(BaseAddr, DCTL, RUN_STOP) != 0) {
                  chg_sm_state = DCI_DWC_CHG_ST_END;
                  break;
                }
                // SW if there is sess_vld
                chg_sm_state = DCI_DWC_CHG_ST_BSESS_VALID;
            }
            break;

            case DCI_DWC_CHG_ST_BSESS_VALID :
            {

                //Eanble data contact detection for battery charging
                if(!enable_dcd_bc(BaseAddr)) {
                    DEBUG((EFI_D_ERROR, "Enable data contact detection failed.\n"));
                    chg_sm_state = DCI_DWC_CHG_ST_DONE;
                    chg_port_type = DCI_DWC_CHG_PORT_UNKNOWN;
                    break;
                }
                chg_sm_state = DCI_DWC_CHG_ST_WAIT_FOR_DCD;
            }
            break;

            case DCI_DWC_CHG_ST_WAIT_FOR_DCD :
            {
                int pollCount=10;

                // On timeout go to charging port detection
                chg_sm_state = DCI_DWC_CHG_ST_CHARGING_PORT_DETECTION;

                // Wait for 500ms DCD timeout
                while(pollCount--) {

                    // Poll for DCD interrupt
                    if(is_dcdintlch_set(BaseAddr)) {
                        chg_sm_state = DCI_DWC_CHG_ST_CHARGING_PORT_DETECTION;
                        DEBUG ((EFI_D_INFO, "DCDINT latch bit is set.\n"));
                        break;
                    }

                    dci_dwc_delay_ms(50);
                }

                // Disable DCD
                if(!disable_dcd_bc(BaseAddr)) {
                    DEBUG((EFI_D_INFO, "Disable data cotact detection failed.\n"));
                    chg_sm_state = DCI_DWC_CHG_ST_DONE;
                    chg_port_type = DCI_DWC_CHG_PORT_UNKNOWN;
                    break;
                }
            }
            break;

            case DCI_DWC_CHG_ST_CHARGING_PORT_DETECTION:
            {

                // Read linestate bits from CHARGING_DET_CTRL <9:8> i.e. <Dm:Dp>
                linestate = read_linestate_output(BaseAddr);

                // if Vd- > Vlgc, this is a proprietary charger
                if(linestate & 0x2){
                    DEBUG(( EFI_D_ERROR, "Proprietary charger detected: linestate 0x%x \r\n", linestate));
                    chg_sm_state = DCI_DWC_CHG_ST_INVALID_CHARGER;
                    chg_port_type = DCI_DWC_CHG_PORT_INVALID;
                    break;
                }

                if(!enable_primary_detection(BaseAddr)) {
                    DEBUG((EFI_D_ERROR, "Enable primary detection failed."));
                    chg_sm_state = DCI_DWC_CHG_ST_DONE;
                    chg_port_type = DCI_DWC_CHG_PORT_UNKNOWN;
                    break;
                }

                // If CHGDET is set go to CHARGING_PORT_DETECTED else it is a SDP
                if(read_chgdet_output(BaseAddr)){
                    chg_sm_state = DCI_DWC_CHG_ST_CHARGING_PORT_DETECTED;
                }
                else {
                    chg_sm_state = DCI_DWC_CHG_ST_SDP;
                }

                // Disable primary detection
                if(!disable_primary_detection(BaseAddr)) {
                    DEBUG((EFI_D_ERROR, "Disable primary detection failed.\n"));
                    chg_sm_state = DCI_DWC_CHG_ST_DONE;
                    chg_port_type = DCI_DWC_CHG_PORT_UNKNOWN;
                    break;
                }
            }
            break;

            case DCI_DWC_CHG_ST_CHARGING_PORT_DETECTED :
            {
                // Perform secondary detection to identify if it is a DCP or CDP
                if(!enable_secondary_detection(BaseAddr)) {
                    DEBUG((EFI_D_ERROR, "Enable secondary detection failed."));
                    chg_sm_state = DCI_DWC_CHG_ST_DONE;
                    chg_port_type = DCI_DWC_CHG_PORT_UNKNOWN;
                    break;
                }

                // If CHGDET is set it is a DCP else CDP
                if(read_chgdet_output(BaseAddr)) {
                    chg_sm_state = DCI_DWC_CHG_ST_DCP;
                }
                else {
                    chg_sm_state = DCI_DWC_CHG_ST_CDP;
                }
            }
            break;

            case DCI_DWC_CHG_ST_SDP :
            {
                chg_port_type = DCI_DWC_CHG_PORT_SDP;
                chg_sm_state = DCI_DWC_CHG_ST_DONE;
            }
            break;

            case DCI_DWC_CHG_ST_CDP :
            {
                chg_port_type = DCI_DWC_CHG_PORT_CDP;
                chg_sm_state = DCI_DWC_CHG_ST_DONE;
            }
            break;

            case DCI_DWC_CHG_ST_DCP :
            {
                chg_port_type = DCI_DWC_CHG_PORT_DCP;
                chg_sm_state = DCI_DWC_CHG_ST_DONE;
            }
            break;

            case DCI_DWC_CHG_ST_INVALID_CHARGER :
            {
                chg_port_type = DCI_DWC_CHG_PORT_INVALID;
                chg_sm_state = DCI_DWC_CHG_ST_DONE;

            }
            break;

            case DCI_DWC_CHG_ST_DONE :
            {
                disable_detection(BaseAddr);
                if ( chg_port_type == DCI_DWC_CHG_PORT_SDP || chg_port_type == DCI_DWC_CHG_PORT_CDP)
                {
                    // code to configure D+ pull up manually
                    dci_connect(QscratchBaseAddr);
                }

                chg_sm_state = DCI_DWC_CHG_ST_END;
            }
            break;

            default :
            {
                chg_port_type = DCI_DWC_CHG_PORT_UNKNOWN;
                chg_sm_state = DCI_DWC_CHG_ST_DONE;
                DEBUG ((EFI_D_ERROR, "Unknown charger detected.\n"));
            }
            break;
        }

    }

    if(loop_count >= QHSUSB_CHG_DETECTION_LOOP_COUNT)
    {
        chg_port_type = DCI_DWC_CHG_PORT_UNKNOWN;
    }

    DEBUG ((EFI_D_ERROR, "Charger type: %a\n", dci_dwc_print_charger_type(chg_port_type)));
    return chg_port_type;
}
#endif
