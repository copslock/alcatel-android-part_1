#ifndef __PCIECONFIGDATA_H__
#define __PCIECONFIGDATA_H__
/*
===========================================================================
*/
/**
  @file PcieConfigData.h
*/
/*
  ===========================================================================

  Copyright (c) 2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

  ===========================================================================

  $Header: //components/rel/boot.xf/1.0/QcomPkg/Msm8996Pkg/Library/PcieConfigLib/PcieConfigData.h#1 $
  $DateTime: 2015/06/22 18:04:20 $
  $Author: pwbldsvc $

                              EDIT HISTORY

 when       who      what, where, why
 --------   ---      ----------------------------------------------------------
 06/15/15  tselvam  Removed unwanted mactos and unified data structs and also
                    updated PHY sequence for V2 and V3 hardware.
                    Renamed the file to PcieConfigData.h from PcieHwioRegDef.h
 05/15/15  tselvam  First checkin as part of Unified code for multi platforms
 
  ===========================================================================
*/

#include "PcieHwioRegs.h"
#include <HALhwio.h>
#include <Protocol/EFIPmicGpio.h>
#include <Protocol/EFITlmm.h>
#include <Library/DebugLib.h>

/*
 * Chipset specific macro definition
 * MSM8996
 */
#define MAX_RP                                           (3)
#define PCIE_INVALID_REG_BASE                            (0xFFFFFFFFUL)
#define PHY_REG_DELAY_VAL                                (0xDEDEDEDE)
#define PHY_REG_NOP                                      (0xFFFFFFFF)

/*
 * Register base address for multiple instances
 */

CONST UINTN pcie20_wrapper_ahb_bases[] = {
  PCIE_0_PCIE20_WRAPPER_AHB_BASE,
  PCIE_1_PCIE20_WRAPPER_AHB_BASE,
  PCIE_2_PCIE20_WRAPPER_AHB_BASE
};
#define PCIE20_WRAPPER_AHB_BASE(x)                       (pcie20_wrapper_ahb_bases[(x)])

CONST UINTN pcie20_wrapper_axi_bases[] = {
  PCIE_0_PCIE20_WRAPPER_AXI_BASE,
  PCIE_1_PCIE20_WRAPPER_AXI_BASE,
  PCIE_2_PCIE20_WRAPPER_AXI_BASE
};
#define PCIE20_WRAPPER_AXI_BASE(x)                       (pcie20_wrapper_axi_bases[(x)])

CONST UINTN axi_addr_space_sizes[] = {
  PCIE_0_PCIE20_WRAPPER_AXI_BASE_SIZE,
  PCIE_1_PCIE20_WRAPPER_AXI_BASE_SIZE,
  PCIE_2_PCIE20_WRAPPER_AXI_BASE_SIZE
};
#define AXI_ADDR_SPACE_SIZE(x)                           (axi_addr_space_sizes[(x)])

CONST UINTN port_reg_bases[] = {
  PCIE_A_PCIE_PORT_REG_BASE,
  PCIE_B_PCIE_PORT_REG_BASE,
  PCIE_C_PCIE_PORT_REG_BASE
};
#define PORT_REG_BASE(x)                                 (port_reg_bases[(x)])

CONST UINTN rx_reg_bases[] = {
  QSERDES_RX_A_PCIE_QMP_RX_REG_BASE,
  QSERDES_RX_B_PCIE_QMP_RX_REG_BASE,
  QSERDES_RX_B_PCIE_QMP_RX_REG_BASE
};
#define RX_REG_BASE(x)                                   (rx_reg_bases[(x)])

CONST UINTN tx_reg_bases[] = {
  QSERDES_TX_A_PCIE_QMP_TX_REG_BASE,
  QSERDES_TX_B_PCIE_QMP_TX_REG_BASE,
  QSERDES_TX_C_PCIE_QMP_TX_REG_BASE
};
#define TX_REG_BASE(x)                                   (tx_reg_bases[(x)])

CONST UINTN com_phy_reg_base[] = {
  PCIE20_AHB2PHY_BASE
};

CONST UINTN pcie_invalid_reg_base[] = {
  PCIE_INVALID_REG_BASE
};

#define PCIE20_DBI_REG_BASE(x)                           (PCIE20_WRAPPER_AXI_BASE(x)      + 0x00000000)
#define PCIE20_ELBI_REG_BASE(x)                          (PCIE20_WRAPPER_AXI_BASE(x)      + 0x00000f20)
#define PCIE20_MHI_REG_BASE(x)                           (PCIE20_WRAPPER_AHB_BASE(x)      + 0x00007000)
#define PCIE20_PARF_REG_BASE(x)                          (PCIE20_WRAPPER_AHB_BASE(x)      + 0x00000000)

typedef enum pin_config_type
{
  NONE = 0,
  MSM_GPIO,
  PMIC_GPIO,
  PMIC_MPP,
  DELAY
} pin_config_type;

typedef enum {
  invalid_reg_base = 0,
  dbi_reg_base,
  parf_reg_base,
  port_reg_base,
  rx_reg_base,
  tx_reg_base,
  phy_reg_base
}reg_base_index;

typedef struct
{
  /* Base addresses for the registers to be configured */
  reg_base_index base;
  /* Offset for the registers to be configured */
  UINT32         reg_offset;
  /* Values for the register listed above */
  UINT32         reg_val;
} pcie_phy_reg_addrs_vals;

typedef struct
{
  UINT32 GpioNum;
  UINT32 Func;
  UINT32 Direction;
  UINT32 Pull;
  UINT32 DriveStrength;
  TLMM_ValueType value;
}tlmmGpioParam_t;

typedef struct
{
  UINT32 PmicIndex;
  EFI_PM_GPIO_WHICH_TYPE GpioNum;
  EFI_PM_GPIO_OUT_BUFFER_CONFIG_TYPE OutBuffConfig;
  EFI_PM_GPIO_VOLTAGE_SOURCE_TYPE VSrc;
  EFI_PM_GPIO_SOURCE_CONFIG_TYPE Source;
  EFI_PM_GPIO_OUT_BUFFER_DRIVE_STRENGTH_TYPE BufferStrength;
  BOOLEAN inversion;
}pmicGpioParam_t;

typedef union
{
  tlmmGpioParam_t TlmmGpioParam;
  pmicGpioParam_t PmicGpioParam;
  UINTN DelayValue;
}gpioConfigParam_t;

typedef struct
{
  pin_config_type type;
  gpioConfigParam_t GpioConfigParam;
}gpioPinConfig_t;

CONST UINTN * pcie_base_addr[] = {
  pcie_invalid_reg_base,
  pcie20_wrapper_axi_bases,
  pcie20_wrapper_ahb_bases,
  port_reg_bases,
  rx_reg_bases,
  tx_reg_bases,
  com_phy_reg_base
};

/*
 * CLK and voltage domains to be enabled
 */

enum pcie_phy_clk_name_index
{
  gcc_pcie_phy_cfg_ahb_clk = 0,
  gcc_pcie_phy_aux_clk,
  gcc_max_pcie_phy_clk
};

/*
 * Index in this pcie_phy_clk array is governed by enum
 * pcie_phy_clk_name_index. The order in which the clock
 * names initialized in this array should match the
 * index in the enum.
 *
 * gcc_max_pcie_phy_clk will be used in for loop for
 * exit condition.
 */
CONST CHAR8 pcie_phy_clk[gcc_max_pcie_phy_clk][PCIE_MAX_CLK_NAME] =
{
  "gcc_pcie_phy_cfg_ahb_clk",
  "gcc_pcie_phy_aux_clk"
};

enum pcie_clk_name_index
{
  gcc_pcie_phy_clk = 0,
  gcc_pcie_aux_clk,
  gcc_pcie_slv_axi_clk,
  gcc_pcie_mstr_axi_clk,
  gcc_pcie_cfg_ahb_clk,
  gcc_max_pcie_clk
};

/*
 * Index in this pcie_clk array is governed by enum
 * pcie_clk_name_index. The order in which the clock
 * names initialized in this array should match the
 * index in the enum.
 *
 * gcc_max_pcie_clk will be used in for loop for
 * exit condition.
 */
CONST CHAR8 pcie_clk[MAX_RP][gcc_max_pcie_clk][PCIE_MAX_CLK_NAME] =
{
  {
    "gcc_pcie_0_pipe_clk",
    "gcc_pcie_0_aux_clk",
    "gcc_pcie_0_slv_axi_clk",
    "gcc_pcie_0_mstr_axi_clk",
    "gcc_pcie_0_cfg_ahb_clk"
  },
  {
    "gcc_pcie_1_pipe_clk",
    "gcc_pcie_1_aux_clk",
    "gcc_pcie_1_slv_axi_clk",
    "gcc_pcie_1_mstr_axi_clk",
    "gcc_pcie_1_cfg_ahb_clk"
  },
  {
    "gcc_pcie_2_pipe_clk",
    "gcc_pcie_2_aux_clk",
    "gcc_pcie_2_slv_axi_clk",
    "gcc_pcie_2_mstr_axi_clk",
    "gcc_pcie_2_cfg_ahb_clk"
  }
};

CONST CHAR8 pcie_clk_pwr_dmn_name[MAX_RP][PCIE_MAX_CLK_NAME] =
{
  "VDD_PCIE_0",
  "VDD_PCIE_1",
  "VDD_PCIE_2"
};

/*
 * Chipset specific PHY init sequence
 * MSM8996
 */
CONST pcie_phy_reg_addrs_vals phy_reg_addr[]=
{
  /*Register Base,    Offset                                               Value */
  { phy_reg_base,     HWIO_OFFS(PCIE_COM_POWER_DOWN_CONTROL),              0x01 },
  { phy_reg_base,     HWIO_OFFS(QSERDES_COM_BIAS_EN_CLKBUFLR_EN),          0x1C },
  { phy_reg_base,     HWIO_OFFS(QSERDES_COM_CLK_ENABLE1),                  0x10 },
  { phy_reg_base,     HWIO_OFFS(QSERDES_COM_CLK_SELECT),                   0x33 },
  { phy_reg_base,     HWIO_OFFS(QSERDES_COM_CMN_CONFIG),                   0x06 },
  { phy_reg_base,     HWIO_OFFS(QSERDES_COM_LOCK_CMP_EN),                  0x42 },
  { phy_reg_base,     HWIO_OFFS(QSERDES_COM_VCO_TUNE_MAP),                 0x00 },
  { phy_reg_base,     HWIO_OFFS(QSERDES_COM_VCO_TUNE2_MODE0),              0x01 },
  { phy_reg_base,     HWIO_OFFS(QSERDES_COM_VCO_TUNE1_MODE0),              0x3F },
  { phy_reg_base,     HWIO_OFFS(QSERDES_COM_VCO_TUNE_TIMER1),              0xFF },
  { phy_reg_base,     HWIO_OFFS(QSERDES_COM_VCO_TUNE_TIMER2),              0x1F },
  { phy_reg_base,     HWIO_OFFS(QSERDES_COM_HSCLK_SEL),                    0x01 },
  { phy_reg_base,     HWIO_OFFS(QSERDES_COM_SVS_MODE_CLK_SEL),             0x01 },
  { phy_reg_base,     HWIO_OFFS(QSERDES_COM_CORE_CLK_EN),                  0x00 },
  { phy_reg_base,     HWIO_OFFS(QSERDES_COM_CORECLK_DIV),                  0x0A },
  { phy_reg_base,     HWIO_OFFS(QSERDES_COM_BG_TIMER),                     0x09 },
  { phy_reg_base,     HWIO_OFFS(QSERDES_COM_DEC_START_MODE0),              0x82 },
  { phy_reg_base,     HWIO_OFFS(QSERDES_COM_DIV_FRAC_START3_MODE0),        0x03 },
  { phy_reg_base,     HWIO_OFFS(QSERDES_COM_DIV_FRAC_START2_MODE0),        0x55 },
  { phy_reg_base,     HWIO_OFFS(QSERDES_COM_DIV_FRAC_START1_MODE0),        0x55 },
  { phy_reg_base,     HWIO_OFFS(QSERDES_COM_LOCK_CMP3_MODE0),              0x00 },
  { phy_reg_base,     HWIO_OFFS(QSERDES_COM_LOCK_CMP2_MODE0),              0x1A },
  { phy_reg_base,     HWIO_OFFS(QSERDES_COM_LOCK_CMP1_MODE0),              0x0A },
  { phy_reg_base,     HWIO_OFFS(QSERDES_COM_CLK_SELECT),                   0x33 },
  { phy_reg_base,     HWIO_OFFS(QSERDES_COM_SYS_CLK_CTRL),                 0x02 },
  { phy_reg_base,     HWIO_OFFS(QSERDES_COM_SYSCLK_BUF_ENABLE),            0x1F },
  { phy_reg_base,     HWIO_OFFS(QSERDES_COM_SYSCLK_EN_SEL),                0x04 },
  { phy_reg_base,     HWIO_OFFS(QSERDES_COM_CP_CTRL_MODE0),                0x0B },
  { phy_reg_base,     HWIO_OFFS(QSERDES_COM_PLL_RCTRL_MODE0),              0x16 },
  { phy_reg_base,     HWIO_OFFS(QSERDES_COM_PLL_CCTRL_MODE0),              0x28 },
  { phy_reg_base,     HWIO_OFFS(QSERDES_COM_INTEGLOOP_GAIN1_MODE0),        0x00 },
  { phy_reg_base,     HWIO_OFFS(QSERDES_COM_INTEGLOOP_GAIN0_MODE0),        0x80 },
  { phy_reg_base,     HWIO_OFFS(QSERDES_COM_SSC_EN_CENTER),                0x01 },
  { phy_reg_base,     HWIO_OFFS(QSERDES_COM_SSC_PER1),                     0x31 },
  { phy_reg_base,     HWIO_OFFS(QSERDES_COM_SSC_PER2),                     0x01 },
  { phy_reg_base,     HWIO_OFFS(QSERDES_COM_SSC_ADJ_PER1),                 0x02 },
  { phy_reg_base,     HWIO_OFFS(QSERDES_COM_SSC_ADJ_PER2),                 0x00 },
  { phy_reg_base,     HWIO_OFFS(QSERDES_COM_SSC_STEP_SIZE1),               0x2F },
  { phy_reg_base,     HWIO_OFFS(QSERDES_COM_SSC_STEP_SIZE2),               0x19 },
  { phy_reg_base,     HWIO_OFFS(QSERDES_COM_RESCODE_DIV_NUM),              0x15 },
  { phy_reg_base,     HWIO_OFFS(QSERDES_COM_BG_TRIM),                      0x0F },
  { phy_reg_base,     HWIO_OFFS(QSERDES_COM_PLL_IVCO),                     0x0F },
  { phy_reg_base,     HWIO_OFFS(QSERDES_COM_CLK_EP_DIV),                   0x19 },
  { phy_reg_base,     HWIO_OFFS(QSERDES_COM_CLK_ENABLE1),                  0x10 },
  /* PHY START */
  { phy_reg_base,     HWIO_OFFS(PCIE_COM_SW_RESET),                        0x00 },
  { phy_reg_base,     HWIO_OFFS(PCIE_COM_START_CONTROL),                   0x03 },
  /* NOP indicates end of config and poll status start*/
  { invalid_reg_base, PHY_REG_NOP,                                         0xFF },
  /* Poll Status */
  { phy_reg_base,     HWIO_OFFS(PCIE_COM_PCS_READY_STATUS),                HWIO_PCIE_COM_PCS_READY_STATUS_PCS_READY_BMSK }
};

CONST pcie_phy_reg_addrs_vals phy_port_reg_addr[] =
{
  /* Register Base,   Offset,                                                 Value */
  /* Device type as RC */
  {parf_reg_base,     HWIO_PCIE20_PARF_DEVICE_TYPE_OFFS,                      0x4},
  /* Port PHY init sequence */
  {tx_reg_base,       HWIO_QSERDES_TX_HIGHZ_TRANSCEIVEREN_BIAS_DRVR_EN_OFFS,  0x45},
  {tx_reg_base,       HWIO_QSERDES_TX_LANE_MODE_OFFS,                         0x06},
  {rx_reg_base,       HWIO_QSERDES_RX_SIGDET_ENABLES_OFFS,                    0x1C},
  {rx_reg_base,       HWIO_QSERDES_RX_SIGDET_LVL_OFFS,                        0x17},
  {rx_reg_base,       HWIO_QSERDES_RX_RX_EQU_ADAPTOR_CNTRL2_OFFS,             0x01},
  {rx_reg_base,       HWIO_QSERDES_RX_RX_EQU_ADAPTOR_CNTRL3_OFFS,             0x00},
  {rx_reg_base,       HWIO_QSERDES_RX_RX_EQU_ADAPTOR_CNTRL4_OFFS,             0xDB},
  {rx_reg_base,       HWIO_QSERDES_RX_UCDR_SO_GAIN_OFFS,                      0x04},
  {rx_reg_base,       HWIO_QSERDES_RX_UCDR_SO_GAIN_HALF_OFFS,                 0x04},
  {port_reg_base,     HWIO_PCIE_RX_IDLE_DTCT_CNTRL_OFFS,                      0x4C},
  {port_reg_base,     HWIO_PCIE_PWRUP_RESET_DLY_TIME_AUXCLK_OFFS,             0x00},
  {port_reg_base,     HWIO_PCIE_LP_WAKEUP_DLY_TIME_AUXCLK_OFFS,               0x00},
  {port_reg_base,     HWIO_PCIE_PLL_LOCK_CHK_DLY_TIME_OFFS,                   0x05},
  {rx_reg_base,       HWIO_QSERDES_RX_UCDR_SO_SATURATION_AND_ENABLE_OFFS,     0x4b},
  {rx_reg_base,       HWIO_QSERDES_RX_SIGDET_DEGLITCH_CNTRL_OFFS,             0x14},
  {port_reg_base,     HWIO_PCIE_ENDPOINT_REFCLK_DRIVE_OFFS,                   0x05},
  {port_reg_base,     HWIO_PCIE_POWER_DOWN_CONTROL_OFFS,                      0x02},
  {port_reg_base,     HWIO_PCIE_POWER_STATE_CONFIG4_OFFS,                     0x00},
  {port_reg_base,     HWIO_PCIE_POWER_STATE_CONFIG1_OFFS,                     0xA3},
  {port_reg_base,     HWIO_PCIE_POWER_DOWN_CONTROL_OFFS,                      0x03},
  {port_reg_base,     HWIO_PCIE_SW_RESET_OFFS,                                0x00},
  {port_reg_base,     HWIO_PCIE_START_CONTROL_OFFS,                           0x0A},
  /* NOP indicates end of config and poll status start*/
  {invalid_reg_base,  PHY_REG_NOP,                                            0xFF },
  /* Poll Status */
  {port_reg_base,     HWIO_PCIE_PCS_STATUS_OFFS,                              HWIO_PCIE_PCS_STATUS_PHYSTATUS_BMSK},
};

/*
 * CLKREQ GPIO settings for all the RP instances
 */
CONST gpioPinConfig_t ClkReqGpio[] =
{
  {
   .type = MSM_GPIO,
   {
    .TlmmGpioParam.GpioNum = 36,
    .TlmmGpioParam.Func = 2,
    .TlmmGpioParam.Direction = GPIO_INPUT,
    .TlmmGpioParam.Pull = GPIO_NO_PULL,
    .TlmmGpioParam.DriveStrength = GPIO_2MA,
    .TlmmGpioParam.value = GPIO_LOW_VALUE
   }
  }
  ,
  {
   .type = MSM_GPIO,
   {
    .TlmmGpioParam.GpioNum = 131,
    .TlmmGpioParam.Func = 2,
    .TlmmGpioParam.Direction = GPIO_INPUT,
    .TlmmGpioParam.Pull = GPIO_NO_PULL,
    .TlmmGpioParam.DriveStrength = GPIO_2MA,
    .TlmmGpioParam.value = GPIO_LOW_VALUE
   }
  }
  ,
  {
   .type = NONE,
   {
    .DelayValue = NONE
   }
  }
};

/*
 * ENDP GPIO configuration
 */
CONST gpioPinConfig_t GpioPinConfig[] =
{
  {
   .type = MSM_GPIO,
   {
    .TlmmGpioParam.GpioNum = 35,
    .TlmmGpioParam.Func = 0,
    .TlmmGpioParam.Direction = GPIO_OUTPUT,
    .TlmmGpioParam.Pull = GPIO_NO_PULL,
    .TlmmGpioParam.DriveStrength = GPIO_2MA,
    .TlmmGpioParam.value = GPIO_LOW_VALUE
   }
  }
  ,
  {
   .type = MSM_GPIO,
   {
    .TlmmGpioParam.GpioNum = 1,
    .TlmmGpioParam.Func = 0,
    .TlmmGpioParam.Direction = GPIO_OUTPUT,
    .TlmmGpioParam.Pull = GPIO_NO_PULL,
    .TlmmGpioParam.DriveStrength = GPIO_2MA,
    .TlmmGpioParam.value = GPIO_HIGH_VALUE
   }
  }
  ,
  {
   .type = MSM_GPIO,
   {
    .TlmmGpioParam.GpioNum = 46,
    .TlmmGpioParam.Func = 0,
    .TlmmGpioParam.Direction = GPIO_OUTPUT,
    .TlmmGpioParam.Pull = GPIO_NO_PULL,
    .TlmmGpioParam.DriveStrength = GPIO_2MA,
    .TlmmGpioParam.value = GPIO_HIGH_VALUE
   }
  }
  ,
  {
   .type = PMIC_GPIO,
   {
    .PmicGpioParam.PmicIndex = 0,
    .PmicGpioParam.GpioNum = EFI_PM_GPIO_9,
    .PmicGpioParam.OutBuffConfig = EFI_PM_GPIO_OUT_BUFFER_CONFIG_CMOS,
    .PmicGpioParam.VSrc = EFI_PM_GPIO_VIN2,
    .PmicGpioParam.Source = EFI_PM_GPIO_SOURCE_GND,
    .PmicGpioParam.BufferStrength = EFI_PM_GPIO_OUT_BUFFER_HIGH,
    .PmicGpioParam.inversion = 1
   }
  }
  ,
  {
   .type = PMIC_GPIO,
   {
    .PmicGpioParam.PmicIndex = 0,
    .PmicGpioParam.GpioNum = EFI_PM_GPIO_19,
    .PmicGpioParam.OutBuffConfig = EFI_PM_GPIO_OUT_BUFFER_CONFIG_CMOS,
    .PmicGpioParam.VSrc = EFI_PM_GPIO_VIN2,
    .PmicGpioParam.Source = EFI_PM_GPIO_SOURCE_GND,
    .PmicGpioParam.BufferStrength = EFI_PM_GPIO_OUT_BUFFER_HIGH,
    .PmicGpioParam.inversion = 0
   }
  }
  ,
  {
   .type = PMIC_GPIO,
   {
    .PmicGpioParam.PmicIndex = 0,
    .PmicGpioParam.GpioNum = EFI_PM_GPIO_8,
    .PmicGpioParam.OutBuffConfig = EFI_PM_GPIO_OUT_BUFFER_CONFIG_CMOS,
    .PmicGpioParam.VSrc = EFI_PM_GPIO_VIN2,
    .PmicGpioParam.Source = EFI_PM_GPIO_SOURCE_GND,
    .PmicGpioParam.BufferStrength = EFI_PM_GPIO_OUT_BUFFER_OFF,
    .PmicGpioParam.inversion = 0
   }
  }
  ,
  {
   .type = MSM_GPIO,
   {
    .TlmmGpioParam.GpioNum = 50,
    .TlmmGpioParam.Func = 0,
    .TlmmGpioParam.Direction = GPIO_OUTPUT,
    .TlmmGpioParam.Pull = GPIO_NO_PULL,
    .TlmmGpioParam.DriveStrength = GPIO_2MA,
    .TlmmGpioParam.value = GPIO_LOW_VALUE
   }
  }
  ,
  {
   .type = PMIC_GPIO,
   {
    .PmicGpioParam.PmicIndex = 0,
    .PmicGpioParam.GpioNum = EFI_PM_GPIO_8,
    .PmicGpioParam.OutBuffConfig = EFI_PM_GPIO_OUT_BUFFER_CONFIG_CMOS,
    .PmicGpioParam.VSrc = EFI_PM_GPIO_VIN2,
    .PmicGpioParam.Source = EFI_PM_GPIO_SOURCE_GND,
    .PmicGpioParam.BufferStrength = EFI_PM_GPIO_OUT_BUFFER_HIGH,
    .PmicGpioParam.inversion = 1
   }
  }
  ,
  {
   .type = DELAY,
   {
    .DelayValue = 1000
   }
  }
  ,
  {
   .type = MSM_GPIO,
   {
    .TlmmGpioParam.GpioNum = 35,
    .TlmmGpioParam.Func = 0,
    .TlmmGpioParam.Direction = GPIO_OUTPUT,
    .TlmmGpioParam.Pull = GPIO_NO_PULL,
    .TlmmGpioParam.DriveStrength = GPIO_2MA,
    .TlmmGpioParam.value = GPIO_HIGH_VALUE
   }
  }
  ,
  {
   .type = NONE,
   {
    .DelayValue = NONE
   }
  }
};
#endif /* __PCIECONFIGDATA_H__ */
