#/** @file Msm8996PkgCommon.fdf.inc
# Common images in layout file for Msm8996Pkg.
#
# Copyright (c) 2015 Qualcomm Technologies, Inc. All rights reserved.
# Portions Copyright (c) 2009, Apple Inc. All rights reserved.
# This program and the accompanying materials
# are licensed and made available under the terms and conditions of the BSD License
# which accompanies this distribution.  The full text of the license may be found at
# http://opensource.org/licenses/bsd-license.php
#
# THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
# WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
#
#**/
#
#==============================================================================
#                              EDIT HISTORY
#
#
# when       who     what, where, why
# --------   ---     ----------------------------------------------------------
# 06/24/15   bh      Initial revision
#
#==============================================================================

  INF MdeModulePkg/Core/Dxe/DxeMain.inf

  #
  # PI DXE Drivers producing Architectural Protocols (EFI Services)
  #
  INF ArmPkg/Drivers/CpuDxe/CpuDxe.inf

  INF MdeModulePkg/Core/RuntimeDxe/RuntimeDxe.inf

  #
  # Security Dxe
  #
  INF QcomPkg/Drivers/SecurityDxe/SecurityDxe.inf


  #
  # PIL Loader
  #
  INF QcomPkg/Drivers/TzDxe/TzDxe.inf
  INF MdeModulePkg/Universal/WatchdogTimerDxe/WatchdogTimer.inf
  INF QcomPkg/Drivers/CapsuleRuntimeDxe/CapsuleRuntimeDxe.inf

  #
  # Variable Dxe
  #
  #INF QcomPkg/Drivers/VariableDxe/VariableDxe.inf
  INF QcomPkg/Drivers/EmbeddedMonotonicCounter/EmbeddedMonotonicCounter.inf
  INF QcomPkg/Drivers/SimpleTextInOutSerialDxe/SimpleTextInOutSerial.inf

  INF QcomPkg/Drivers/ResetRuntimeDxe/ResetRuntimeDxe.inf

  INF EmbeddedPkg/RealTimeClockRuntimeDxe/RealTimeClockRuntimeDxe.inf
  INF EmbeddedPkg/MetronomeDxe/MetronomeDxe.inf
  INF MdeModulePkg/Universal/PrintDxe/PrintDxe.inf
  INF MdeModulePkg/Universal/DevicePathDxe/DevicePathDxe.inf

  INF MdeModulePkg/Universal/Console/ConPlatformDxe/ConPlatformDxe.inf
  INF MdeModulePkg/Universal/Console/ConSplitterDxe/ConSplitterDxe.inf
  INF MdeModulePkg/Universal/Console/GraphicsConsoleDxe/GraphicsConsoleDxe.inf
  INF MdeModulePkg/Universal/HiiDatabaseDxe/HiiDatabaseDxe.inf


  INF QcomPkg/Drivers/EnvDxe/EnvDxe.inf



  #
  # SoC Drivers
  #
  INF ArmPkg/Drivers/ArmGic/ArmGicDxe.inf
  INF ArmPkg/Drivers/TimerDxe/TimerDxe.inf
  INF QcomPkg/Drivers/ChipInfoDxe/ChipInfoDxe.inf

  #
  # GLink Driver 
  #
  INF QcomPkg/Drivers/GLinkDxe/GLinkDxe.inf

  #
  # SMEM Driver
  #
  INF QcomPkg/Drivers/SmemDxe/SmemDxe.inf

  #
  # ULog Driver 
  #
  INF QcomPkg/Drivers/ULogDxe/ULogDxe.inf

  #
  # NPA Driver
  #
  INF QcomPkg/Drivers/NpaDxe/NpaDxe.inf

  #
  # FAT filesystem + GPT/MBR partitioning
  #
  INF QcomPkg/Drivers/DiskIoDxe/DiskIoDxe.inf
  INF QcomPkg/Drivers/PartitionDxe/PartitionDxe.inf
  INF FatPkg/EnhancedFatDxe/Fat.inf
  INF MdeModulePkg/Universal/Disk/UnicodeCollation/EnglishDxe/EnglishDxe.inf

  #
  # PIL Image Loader
  #

  #
  # DAL Drivers
  #
  INF QcomPkg/Drivers/DALSYSDxe/DALSYSDxe.inf

  #
  # Clock DXE driver
  #
  INF QcomPkg/Drivers/ClockDxe/ClockDxe.inf
  
  #
  # HWIO DXE driver
  #
  INF QcomPkg/Drivers/HWIODxe/HWIODxe.inf

  #
  # I2C Driver
  #
  INF QcomPkg/Drivers/I2CDxe/I2CDxe.inf



  #
  # SPMI Driver
  #
  INF QcomPkg/Drivers/SPMIDxe/SPMIDxe.inf


  #
  # MMC/SD
  #
  INF QcomPkg/Drivers/SdccDxe/SdccDxe.inf

  #
  # UFS 
  #
  INF QcomPkg/Drivers/UFSDxe/UFSDxe.inf

  #
  # TLMM
  #
  INF QcomPkg/Drivers/TLMMDxe/TLMMDxe.inf


  #
  # Display/MDP DXE driver
  #
  INF QcomPkg/Drivers/DisplayDxe/DisplayDxe.inf

  #
  # PlatformInfo Driver
  #

  INF QcomPkg/Drivers/PlatformInfoDxe/PlatformInfoDxe.inf

  #
  # Button Driver
  #
  INF QcomPkg/Drivers/ButtonsDxe/ButtonsDxe.inf

  #
  # Touch Screen Firmware upgrade
  #

  #
  # PMIC Driver
  #
  INF QcomPkg/Drivers/PmicDxe/PmicDxe.inf

  #
  # Charger Driver
  #

  #
  # ADC Driver
  #
  INF QcomPkg/Drivers/AdcDxe/AdcDxe.inf

  #
  # TSENS Driver
  #
  INF QcomPkg/Drivers/TsensDxe/TsensDxe.inf




  #
  # USB Support
  #
  INF QcomPkg/Drivers/UsbfnDwc3Dxe/UsbfnDwc3Dxe.inf
  INF QcomPkg/Drivers/UsbBusDxe/UsbBusDxe.inf
  INF QcomPkg/Drivers/UsbMsdDxe/UsbMsdDxe.inf
  INF QcomPkg/Drivers/UsbDeviceDxe/UsbDeviceDxe.inf
  INF QcomPkg/Drivers/UsbConfigDxe/UsbConfigDxe.inf

  #
  # PCI Support
  #
  INF QcomPkg/Drivers/PcieDxe/PciBusDxe.inf

  #
  # Hash Driver
  #
  INF QcomPkg/Drivers/HashDxe/HashDxe.inf

  #
  # RNG Driver
  #
  INF QcomPkg/Drivers/RNGDxe/RngDxe.inf

  
  #
  # BDS
  #
  INF QcomPkg/Drivers/QcomBds/QcomBds.inf

  # Memory Map Driver


  
  #
  # Application - Utilities
  #
  INF QcomPkg/Application/Pgm/Pgm.inf
  INF QcomPkg/Application/CmdApp/CmdApp.inf
  INF QcomPkg/Application/MenuApp/MenuApp.inf
  INF QcomPkg/Application/RPMBErase/RPMBErase.inf
  INF QcomPkg/Application/RPMBProvision/RPMBProvision.inf  
  INF QcomPkg/Application/UsbfnMsdApp/UsbfnMsdApp.inf
  INF QcomPkg/Application/FastbootApp/FastbootApp.inf
  
  #INF ShellPkg/Application/Shell/Shell.inf

 
  # Security Toggle application to enable and disable Windows debug policy
  # and Platform Key UEFI secure boot variable. This application is
  # expected to be released to OEMs to use but not present in shipping
  # builds.
  #INF QcomPkg/Application/SecurityToggleApp/SecurityToggleApp.inf


  FILE FREEFORM = a91d838e-a5fa-4138-825d-455e2303079e {
    SECTION UI = "BDS_Menu.cfg"
    SECTION RAW = QcomPkg/Library/PlatformBdsLib/bds_menu.cfg
  }

  FILE FREEFORM = a91d838e-a5fa-4138-825d-455e23030790 {
    SECTION UI = "USB_Menu.cfg"
    SECTION RAW = QcomPkg/Application/MenuApp/Usb_Menu.cfg
  }

  FILE FREEFORM = a91d838e-a5fa-4138-825d-455e23030791 {
    SECTION UI = "Uefi_Menu.cfg"
    SECTION RAW = QcomPkg/Application/MenuApp/Uefi_Menu.cfg
  }

  FILE FREEFORM = a91d838e-a5fa-4138-825d-455e23030792 {
    SECTION UI = "Pmic_Menu.cfg"
    SECTION RAW = QcomPkg/Application/MenuApp/Pmic_Menu.cfg
  }

  FILE FREEFORM = a91d838e-a5fa-4138-825d-455e23030793 {
    SECTION UI = "Config_Menu.cfg"
    SECTION RAW = QcomPkg/Application/MenuApp/Config_Menu.cfg
  }
     
  #
  # UEFI Shell
  #
  INF  EmbeddedPkg/Ebl/Ebl.inf

  #
  # CFG files needed in PRODMODE builds
  #
  FILE FREEFORM = A1E235DE-E825-4591-9623-C43175811826 {
    SECTION UI = "SecParti.cfg"
    SECTION RAW = QcomPkg/Msm8996Pkg/SecParti.cfg
  }

  FILE FREEFORM = 45FE4B7C-150C-45da-A021-4BEB2048EC6F {
    SECTION UI = "PmicChargerApp.cfg"
    SECTION RAW = QcomPkg/Drivers/PmicDxe/Fg/PmicChargerApp_8996.cfg
  }

  FILE FREEFORM = 3E5584ED-05D4-4267-9048-0D47F76F4248 {
    SECTION UI = "battery_symbol_Soc10.bmp"
    SECTION RAW = QcomPkg/Drivers/ChargerDxe/battery_symbol_Soc10.bmp
  }

  FILE FREEFORM = 21D7756C-ABCE-4ca9-88B6-8515EAB40595 {
    SECTION UI = "battery_symbol_Soc25.bmp"
    SECTION RAW = QcomPkg/Drivers/ChargerDxe/battery_symbol_Soc25.bmp
  }

  FILE FREEFORM = 203962DD-A7F3-462b-A006-8956ECBAAB57 {
      SECTION UI = "battery_symbol_Soc50.bmp"
      SECTION RAW = QcomPkg/Drivers/ChargerDxe/battery_symbol_Soc50.bmp
    }

  FILE FREEFORM = 47B43C60-ECED-4874-B8BC-CDCD154E73B6 {
      SECTION UI = "battery_symbol_Soc75.bmp"
      SECTION RAW = QcomPkg/Drivers/ChargerDxe/battery_symbol_Soc75.bmp
    }

  FILE FREEFORM = C5D3FB9A-1FE9-4e2b-868E-ED553EBB9897 {
      SECTION UI = "battery_symbol_Soc100.bmp"
      SECTION RAW = QcomPkg/Drivers/ChargerDxe/battery_symbol_Soc100.bmp
    }

  FILE FREEFORM = 7207EA78-5000-4d8e-9071-FE8C86783367 {
      SECTION UI = "battery_symbol_LowBattery.bmp"
      SECTION RAW = QcomPkg/Drivers/ChargerDxe/battery_symbol_LowBattery.bmp
    }

  FILE FREEFORM = 4753E815-DDD8-402d-BF69-9B8C4EB7573E {
      SECTION UI = "battery_symbol_NoBattery.bmp"
      SECTION RAW = QcomPkg/Drivers/ChargerDxe/battery_symbol_NoBattery.bmp
    }

  FILE FREEFORM = 03DED53E-BECD-428f-9F79-5ABA64C58445 {
      SECTION UI = "battery_symbol_Nocharger.bmp"
      SECTION RAW = QcomPkg/Drivers/ChargerDxe/battery_symbol_Nocharger.bmp
    }

  FILE FREEFORM = 8b86cd38-c772-4fcf-85aa-345b2b3c1ab4 {
      SECTION UI = "battery_symbol_LowBatteryCharging.bmp"
      SECTION RAW = QcomPkg/Drivers/ChargerDxe/battery_symbol_LowBatteryCharging.bmp
    }

