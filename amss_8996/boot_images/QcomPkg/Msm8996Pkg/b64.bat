@ECHO OFF
REM /** @file b64.bat
REM Batch file to initiate build
REM */
REM Copyright (c) 2015, Qualcomm Technologies, Inc. All rights reserved.
REM Portions copyright (c) 2008 - 2010, Apple Inc. All rights reserved.
REM This program and the accompanying materials
REM are licensed and made available under the terms and conditions of the BSD License
REM which accompanies this distribution.  The full text of the license may be found at
REM http://opensource.org/licenses/bsd-license.php
REM 
REM THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
REM WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
echo **************************

REM===========================================================================
REM when       who     what, where, why
REM --------   ---     -------------------------------------------------------
REM 06/24/15   bh      Initial revision
REM
REM===========================================================================

@ECHO OFF 

REM Example usage of this script. Default is a DEBUG build

REM Default build command. If *_pre.dsc file is present, it builds the pre-release binaries 
REM and continues to make the full build. Else, it makes the full build and assumes the 
REM pre-release binaries are already compiled and available in their driver folders.
REM
REM b                                

REM This will build only the pre-release binaries and exit the build once they are done.
REM
REM b PRERELEASE 

REM The remaining options are to clean the build, update version information, config file to be 
REM used, output file name to be used and architecture to be used.
REM
REM b clean 
REM b -v <version number> -cfg <config file name> -o <output file name> -arch <architecture used> -compiler 

REM Set target related variables
SET TARGETMSM=Msm8996
SET VARIANT=LA
SET TARGETPKG=%TARGETMSM%Pkg
SET TARGETROOT=QcomPkg\%TARGETPKG%
SET TARGETMSM_EFI=MSM8996_EFI.fd
SET TARGETMSM_ELF=MSM8996_EFI.elf
SET TARGETID=8996
SET TARGET_ADDR=0x80200000
SET SECTOOLS_POLICY=USES_SEC_POLICY_MULTIPLE_DEFAULT_SIGN
SET USE_SECTOOLS=1
SET ARCH=AARCH64

cd ..
CALL b_full.bat -arch AARCH64 -cfg uefiplatLA.cfg -compiler LLVMWIN %*
cd %TARGETPKG%

EXIT /B
