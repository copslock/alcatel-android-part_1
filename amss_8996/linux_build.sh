#!/bin/bash
#===============================================================================
#
# Copyright (C) 2015 TCL Communication Technology Holdings Limited.
#
# build shell script file.
#
#     Author: Fan Yi
#     E-mail: yi.fan@tcl.com
#     Date  : 2015/07/21
#
#-------------------------------------------------------------------------------
#
#                      EDIT HISTORY FOR FILE
#
# This section contains comments describing changes made to the module.
# Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     -----------------------------------------------------------
# 11/17/2015 Fan Yi  Create modem build script
#===============================================================================

usage() {
echo "
usage : linux_build [-h] [-b] [-m] [-r] [-t] [-d] [-s] [-a] [--clean] <project name> <operator name> <build_id>


 parameters :
 <project name>  : name of the project to build.
 <operator name> : name of the operator to build.
 <build_id>      : configure build id if need.

optional arguments:
  -h, --help        print this help and exits
  -b, --boot        build boot only
  -m, --modem       build modem only
  -r, --rpm         build rpm only
  -t, --trustzone   build trustzone only
  -d, --adsp        build adsp only
  -s, --slpi        build slpi only
  -a, --all         build boot modem rpm
  --clean           clean selected build modules
"
    base="vendor/tct"
    if [ ! -d $base ]; then
        return
    fi
    echo -e "\033[31mYou can try below instead:\033[0m"
    for entry in `ls $base`
    do
        files=`ls $base/$entry`
            for file in $files
            do
                #just filter out the generated header,excluding "build" dir
                if (echo $file | grep -q ".h") ; then
                    a=`echo $file |cut -d'.' -f1| cut -d'_' -f2`
                    echo -e "\033[33m$0 $entry $a \033[0m"
                fi
            done
    done
}

getargs() {
    index=0
    boot=0
    modem=1
    rpm=0
    tz=0
    slpi=0
    adsp=0
    non_version=K3C10000BU00
    simlock=true
    nonhlos=false
    export MP_MACRO=false
    for parameter in $* ;do
        start=$(expr match "${parameter}" '-\|--')
        option=${parameter:$start}
        if [[ $start -gt 0 ]];then
            if [[ "$option" == "h" || "$option" == "help" ]];then
                usage && exit 0
            elif [[ "$option" == "b" || "$option" == "boot" ]];then
                boot=1
                modem=0
                rpm=0
                tz=0
                adsp=0
                slpi=0
            elif [[ "$option" == "m" || "$option" == "modem" ]];then
                boot=0
                modem=1
                rpm=0
                tz=0
                adsp=1
                slpi=0
                nonhlos=true
            elif [[ "$option" == "r" || "$option" == "rpm" ]];then
                boot=0
                modem=0
                rpm=1
                tz=0
                adsp=0
                slpi=0
            elif [[ "$option" == "t" || "$option" == "trustzone" ]];then
                boot=0
                modem=0
                rpm=0
                tz=1
                adsp=0
                slpi=0
            elif [[ "$option" == "d" || "$option" == "adsp" ]];then
                boot=0
                modem=1
                rpm=0
                tz=0
                adsp=1
                slpi=0
            elif [[ "$option" == "s" || "$option" == "slpi" ]];then
                boot=0
                modem=0
                rpm=0
                tz=0
                adsp=0
                slpi=1
            elif [[ "$option" == "a" || "$option" == "all" ]];then
                boot=1
                modem=1
                rpm=1
                tz=1
                adsp=1
                slpi=1
                nonhlos=true
            else
                echo "unvalid option $parameter. try --help"
            fi
        elif [[ ${parameter:0:1} != '-' ]];then
            if [[ $index -eq 0 ]];then project=$parameter;fi
            if [[ $index -eq 1 ]];then operator=$parameter;fi
            if [[ $index -eq 2 ]];then non_version=$parameter;fi
            if [[ $index -eq 3 ]];then simlock=$parameter;fi
            ((index++))
        else
            echo "!!unvalid parameter '$parameter' !!\n"
        fi
    done

    if [[ $index == 0 ]];then
        usage && exit 0
    fi
}

#===============================================================================
# fail msg
#===============================================================================
fail ()
{
    if [ ! -z "$@" ]
    then
        echo -e "\033[31mERROR: $@\033[0m" >&2
    fi
    echo -e "\033[31mERROR: failed.\033[0m" >&2
    usage
    exit 1
}

#===============================================================================
# set up Hexagon environment
#===============================================================================
export_hexagon64()
{
    version=6.4.06
    if [ -d "$project_dir/vendor/tct/buildtools/HEXAGON_Tools" ]; then
        export HEXAGON_ROOT=$project_dir/vendor/tct/buildtools/HEXAGON_Tools
        export HEXAGON_RTOS_RELEASE=${version}
        export HEXAGON_Q6VERSION=v55
        export HEXAGON_IMAGE_ENTRY=0x88800000
    elif [ -d "$project_dir/buildtools/HEXAGON_Tools" ]; then
        export HEXAGON_ROOT=$project_dir/buildtools/HEXAGON_Tools
        export HEXAGON_RTOS_RELEASE=${version}
        export HEXAGON_Q6VERSION=v55
        export HEXAGON_IMAGE_ENTRY=0x88800000
    else
        echo -e "\033[31mPlease clone vendor/tct/buildtools for HEXAGON_Tools ${version}\033[0m"
        exit
    fi
}

export_hexagon72()
{
    version=7.2.11
    if [ -d "$project_dir/vendor/tct/buildtools/HEXAGON_Tools" ]; then
        export HEXAGON_ROOT=$project_dir/vendor/tct/buildtools/HEXAGON_Tools
        export HEXAGON_RTOS_RELEASE=${version}
        export HEXAGON_Q6VERSION=v60
        export HEXAGON_IMAGE_ENTRY=0x88800000
    elif [ -d "$project_dir/buildtools/HEXAGON_Tools" ]; then
        export HEXAGON_ROOT=$project_dir/buildtools/HEXAGON_Tools
        export HEXAGON_RTOS_RELEASE=${version}
        export HEXAGON_Q6VERSION=v60
        export HEXAGON_IMAGE_ENTRY=0x88800000
    else
        echo -e "\033[31mPlease clone vendor/tct/buildtools for HEXAGON_Tools ${version}\033[0m"
        exit
    fi
}
#===============================================================================
# set python environment
#===============================================================================
export_python()
{
    python_ver=2.7.6
    git_server=$(dirname `git remote -v | awk '{print $2}' | head -n 1 `)
    git_branch=$(git branch | cut -d ' ' -f2)
    if [ -d "$project_dir/vendor/tct/buildtools/python-${python_ver}/bin" ]; then
        export MAKE_PATH=$project_dir/vendor/tct/buildtools/python-${python_ver}/bin
    elif [ -d "$project_dir/buildtools/python-${python_ver}/bin" ]; then
        export MAKE_PATH=$project_dir/buildtools/python-${python_ver}/bin
    else
        echo -e "\033[31mPlease clone vendor/tct/buildtools for python ${python_ver}\033[0m"
        echo -e "With below commands:"
        echo -e "\033[33mcd ..   #(put buildtools and amss codes under the same folder)\ngit clone $git_server/vendor/tct-source/buildtools -b ${git_branch}\033[0m"
        exit
    fi
    export PATH=$MAKE_PATH:$PATH
}

#===============================================================================
# set up LLVM environment
#===============================================================================
export_llvm35210()
{
    export LLVMROOT=$project_dir/vendor/tct/buildtools/llvm/3.5.2.1
    export LLVMBIN=$LLVMROOT/bin
    export LLVMLIB=$LLVMROOT/lib/clang/3.5.2/lib/linux
    export MUSLPATH=$LLVMROOT/tools/lib64
    export MUSL32PATH=$LLVMROOT/tools/lib32
    export LLVMINC=$MUSLPATH/include
    export LLVM32INC=$MUSL32PATH/include
    export LLVMTOOLPATH=$LLVMROOT/tools/bin
    export LLVMLINUX_TOOLS_PATH=$project_dir/vendor/tct/buildtools/llvm/3.5.2.1/bin
}

export_llvm35250()
{
    export LLVMROOT=$project_dir/vendor/tct/buildtools/llvm/3.5.2.5
    export LLVMBIN=$LLVMROOT/bin
    export LLVMLIB=$LLVMROOT/lib/clang/3.5.2/lib/linux
    export MUSLPATH=$LLVMROOT/tools/lib64
    export MUSL32PATH=$LLVMROOT/tools/lib32
    export LLVMINC=$MUSLPATH/include
    export LLVM32INC=$MUSL32PATH/include
    export LLVMTOOLPATH=$LLVMROOT/tools/bin
    export LLVMLINUX_TOOLS_PATH=$project_dir/vendor/tct/buildtools/llvm/3.5.2.5/bin
}

#===============================================================================
# set up LINARO environment
#===============================================================================
export_linaro()
{
    export LINAROGCCLINUX_TOOLS_PATH=$project_dir/vendor/tct/buildtools/linaro-toolchain/aarch64-none-elf/4.9-2014.07/bin
    export GNUROOT=$project_dir/vendor/tct/buildtools/linaro-toolchain/aarch64-none-elf/4.9-2014.07
    export GNUARM7=$project_dir/vendor/tct/buildtools/linaro-toolchain/gcc-linaro-arm-linux-gnueabihf/4.8-2014.02
}

#===============================================================================
# set up ARM600 environment
#===============================================================================
export_ARM600()
{
    export COMPILER=6.01bld48
    export COMPILER_VER=6.01
    export MAKE_PATH=/opt/bin:/usr/bin
    if [ -d "/opt/TCTNBTools/${COMPILER}/bin" ]; then
        export ARM_COMPILER_PATH=/opt/TCTNBTools/${COMPILER}/bin
        export ARMROOT=/opt/TCTNBTools/${COMPILER}
    elif [ -d "$project_dir/vendor/tct/buildtools/${COMPILER}" ]; then
        export ARM_COMPILER_PATH=$project_dir/vendor/tct/buildtools/${COMPILER}/bin
        export ARMROOT=$project_dir/vendor/tct/buildtools/${COMPILER}
    elif [ -d "$project_dir/buildtools/${COMPILER}" ]; then
        export ARM_COMPILER_PATH=$project_dir/buildtools/${COMPILER}/bin
        export ARMROOT=$project_dir/buildtools/${COMPILER}
    else
        echo -e "\033[31mPlease install ARM compiler tool ${COMPILER}\033[0m"
        exit 1
    fi
    export ARMTOOLS=ARMCT6
    export ARMLIB=$ARMROOT/lib
    export ARMINCLUDE=$ARMROOT/include
    export ARMINC=$ARMINCLUDE
    export ARMBIN=$ARMROOT/bin
    export ARMHOME=$ARMROOT
    export LLVMROOT=$ARMROOT
    export LLVMBIN=$ARMROOT/bin
    export PATH=$ARMBIN:$MAKE_PATH:$PATH
    export ARMLMD_LICENSE_FILE=8225@armls2
    export_python
    export_hexagon64
    export_llvm35250
    export_linaro
}

#===============================================================================
# set up ARM501 environment
#===============================================================================
export_ARM501()
{
    export MAKE_PATH=/opt/bin:/usr/bin
    export COMPILER=5.01bld94
    if [ -d "/opt/TCTNBTools/5.01bld94" ]; then
        export ARM_COMPILER_PATH=/opt/TCTNBTools/5.01bld94/bin64
        export ARMROOT=/opt/TCTNBTools/5.01bld94
    elif [ -d "$project_dir/vendor/tct/buildtools/${COMPILER}" ]; then
        export ARM_COMPILER_PATH=$project_dir/vendor/tct/buildtools/${COMPILER}/bin64
        export ARMROOT=$project_dir/vendor/tct/buildtools/${COMPILER}
    elif [ -d "$project_dir/buildtools/${COMPILER}" ]; then
        export ARM_COMPILER_PATH=$project_dir/buildtools/${COMPILER}/bin64
        export ARMROOT=$project_dir/buildtools/${COMPILER}
    else
        echo -e "\033[31mPlease install ARM compiler tool ${COMPILER}\033[0m"
        exit 1
    fi
    export ARMTOOLS=ARMCT5.01
    export ARMLIB=$ARMROOT/lib
    export ARMINCLUDE=$ARMROOT/include
    export ARMINC=$ARMINCLUDE
    export ARMBIN=$ARMROOT/bin64
    export ARMHOME=$ARMROOT
    export COMPILER=
    export COMPILER_VER=
    export PATH=$ARMBIN:$MAKE_PATH:$PATH
    export ARMLMD_LICENSE_FILE=8225@armls2
    export_python
    export_hexagon64
    export_llvm35210
    export_linaro
}


#===============================================================================
# build boot image
#===============================================================================
build_boot()
{
    export_ARM501
    export TARGET=RELEASE
    bash boot_images/QcomPkg/Msm${CPU_CHIP_TYPE}Pkg/b64.sh
    local result=$?
    if [ $result -ne 0 ]; then
        exit $result
    fi
    efuse_sign bootimage
    echo "" >> $1/build-log.txt
    cd $1
}

#===============================================================================
# build rpm image
#===============================================================================
build_rpm()
{
    export_ARM501
    pushd $1 > /dev/null
    #huaidi.feng added BEGIN to mismatch the rpm elf file to the DUMP, on the QCAP net.
    #DEFECT 1488324
    bash rpm_proc/build/build_${CPU_CHIP_TYPE}.sh OEM_BUILD_VER=TCT
    #huaidi.feng added end
    local result=$?
    if [ $result -ne 0 ]; then
        exit $result
    fi
    efuse_sign rpm
    echo "" >> $1/build-log.txt
    popd > /dev/null
}

#===============================================================================
# build trustzone image
#===============================================================================
build_tz()
{

    if [ -d "TEMP_HDCP/trustzone_images" ]; then
        echo "check hdcp temp directory exist.\n"
    else
        echo "check hdcp temp directory doesn't exist.\n"
        mkdir TEMP_HDCP
        cp trustzone_images TEMP_HDCP/ -fr
    fi


    export_ARM600
    cd $1/trustzone_images/build/ms
    bash build.sh CHIPSET=msm${CPU_CHIP_TYPE} MAPREPORT=1 devcfg synafp64 sampleapp tclapp
    local result=$?
    if [ $result -ne 0 ]; then
        cd $1
        rm -rf TEMP_HDCP
        exit $result
    fi

    cd $1/DxHDCP/TZBuildScripts/postbuild
    python postbuild.py -s ../../../TEMP_HDCP -c msm8996  -o out_dxhdcp2 -v -l ../../DxHDCP_Core/Release/tz_prod/relocatable/dxhdcp2_release_Prod_TZ4_0_relocatable.lib -b ../../DxHDCP_Core/Release/tz_prod/relocatable/dxhdcp2_build_spec.json
    python $1/common/sectools/sectools.py secimage -i out_dxhdcp2/dxhdcp2.mbn -c $1/common/sectools/config/8996/8996_secimage.xml -o out_dxhdcp2/ -s
    cp out_dxhdcp2/8996/dxhdcp2/dxhdcp2.mbn ../../../trustzone_images/build/ms/bin/IADAANAA/

    efuse_sign tz
    cd $1

    rm -rf TEMP_HDCP
}

#===============================================================================
# build adsp
#===============================================================================
build_adsp()
{
    export_hexagon72
    export_python
    cd $1/adsp_proc/adsp_proc/build
    python ./build.py -k -c msm$CPU_CHIP_TYPE -o all
    local result=$?
    if [ $result -ne 0 ]; then
        exit $result
    fi
    efuse_sign adsp
    cd $1
}

#===============================================================================
# build slpi image
#===============================================================================
build_slpi()
{
    export_ARM501
    export_hexagon72
    cd $1/slpi_proc/build
    python build.py -c msm${CPU_CHIP_TYPE} -f slpi_v2 -o all
    local result=$?
    if [ $result -ne 0 ]; then
        exit $result
    fi
    efuse_sign slpi
    cd $1
}

#===============================================================================
# build modem image
#===============================================================================
build_modem()
{
    export_ARM501
    cd $1/modem_proc/build/ms
    bash build.sh ${CPU_CHIP_TYPE}.gen.prod bparams=-k OEM_BUILD_VER=$non_version
    local result=$?
    if [ "$result" == "0" ]; then
        efuse_sign venus
        efuse_sign modem
        pushd $1/common/build > /dev/null
        python build.py --nonhlos $non_version
        popd > /dev/null
    else
        exit $result
    fi
    echo "" >> $1/build-log.txt
    cd $1
}

#===============================================================================
# update non-hlos
#===============================================================================

update_nonhlos()
{
    pushd $1/common/build > /dev/null
    python build.py --nonhlos $non_version
    popd > /dev/null
}

#===============================================================================
# efuse sign
#===============================================================================
efuse_sign()
{
    if [ -d "$project_dir/sign_tool" ]; then
        pushd $project_dir/sign_tool > /dev/null
        ./st.sh $project_dir ${project} $1
        if test $? -ne 0 ; then
            echo "ERROR: efuse sign amss error"
            echo "ERROR: device cannot poweron without modem efuse sign"
            exit 1
        fi
        popd > /dev/null
    else
        echo "ERROR: cannot find efuse sign tool"
        echo "ERROR: device cannot poweron without modem efuse sign"
        exit 1
    fi
}

#===============================================================================
# remove tct.h
#===============================================================================
rm_tct_h()
{
    if [[ -e "$1/adsp_proc/adsp_proc/build/cust/tct.h" ]];then
        echo "remove old tct.h under adsp_proc/adsp_proc/build/cust"
        rm $1/adsp_proc/adsp_proc/build/cust/tct.h
    fi

    if [[ -e "$1/boot_images/QcomPkg/Msm8996Pkg/Include/tct.h" ]];then
        echo "remove old tct.h under boot_images/QcomPkg/Msm8996Pkg/Include"
        rm $1/boot_images/QcomPkg/Msm8996Pkg/Include/tct.h
    fi

    if [[ -e "$1/rpm_proc/build/ms/tct.h" ]];then
        echo "remove old tct.h under rpm_proc/build/ms"
        rm $1/rpm_proc/build/ms/tct.h
    fi

    if [[ -e "$1/trustzone_images/build/cust/tct.h" ]];then
        echo "remove old tct.h under trustzone_images/build/cust/cust"
        rm $1/trustzone_images/build/cust/tct.h
    fi

    if [[ -e "$1/modem_proc/build/ms/tct.h" ]];then
        echo "remove old tct.h under modem_proc/build/ms"
        rm $1/modem_proc/build/ms/tct.h
    fi
}

#===============================================================================
# link tct.h
#===============================================================================
link_tct_h()
{
    if [[ -e "$1/vendor/tct/$2/${2}_${3}.h" ]];then
        ln -s $1/vendor/tct/$2/${2}_${3}.h $1/adsp_proc/adsp_proc/build/cust/tct.h
        echo "vendor/tct/$2/${2}_${3}.h to adsp_proc/adsp_proc/build/cust/tct.h"
        ln -s $1/vendor/tct/$2/${2}_${3}.h $1/boot_images/QcomPkg/Msm8996Pkg/Include/tct.h
        echo "vendor/tct/$2/${2}_${3}.h to boot_images/QcomPkg/Msm8996Pkg/Include/tct.h"

        ln -s $1/vendor/tct/$2/${2}_${3}.h $1/rpm_proc/build/ms/tct.h
        echo "vendor/tct/$2/${2}_${3}.h to rpm_proc/build/ms/tct.h"

        ln -s $1/vendor/tct/$2/${2}_${3}.h $1/trustzone_images/build/cust/tct.h
        echo "vendor/tct/$2/${2}_${3}.h to trustzone_images/build/cust/tct.h"

        ln -s $1/vendor/tct/$2/${2}_${3}.h $1/modem_proc/build/ms/tct.h
        echo "vendor/tct/$2/${2}_${3}.h to modem_proc/build/ms/tct.h"
    else
        echo "# Unknow project name: $2 or operator name: $3 " >> build-log.txt
        fail "Unknow project name: \033[32m$2\033[31m or operator name: \033[32m$3\033[0m "
    fi
}

#===============================================================================
# set project partition
#===============================================================================
set_partition()
{
    if [[ -e "$1/vendor/tct/$2/build/partition_load_pt/partition_$3.xml" ]]; then
        echo "#----------------------------------------------------------------------"
        echo "# copy $1/vendor/tct/$2/build/partition_load_pt/partition_$3.xml to $1/common/config/emmc/partition.xml"
        cp -r $1/vendor/tct/$2/build/partition_load_pt/partition_$3.xml $1/common/config/emmc/partition.xml
    else
        if [[ -e "$1/vendor/tct/$2/build/partition_load_pt/partition.xml" ]];then
            echo "#----------------------------------------------------------------------"
            echo "# copy $1/vendor/tct/$2/build/partition_load_pt/partition.xml to $1/common/config/emmc/partition.xml"
            cp -r $1/vendor/tct/$2/build/partition_load_pt/partition.xml $1/common/config/emmc/partition.xml
        fi
    fi
}



#===============================================================================
# main
#===============================================================================
getargs $*


setenv=`export`
starttime="$(date +%s)"
starttimefmt=`date --date='@'$starttime`

#===============================================================================
# Setup Paths
#===============================================================================
build_dir=$(dirname $0)
build_dir=$(readlink -e $build_dir)
project_dir=$(dirname $build_dir)

export BOOT_ROOT=$build_dir/modem_proc/build/ms
VENDOR_ROOT=$build_dir/vendor

#===============================================================================
# Set target enviroment
#===============================================================================

echo "Start Time = $starttimefmt" > build-log.txt
echo "#-------------------------------------------------------------------------------" >> build-log.txt
echo "# ENVIRONMENT BEGIN" >> build-log.txt
echo "#-------------------------------------------------------------------------------" >> build-log.txt
export >> build-log.txt
echo "#-------------------------------------------------------------------------------" >> build-log.txt
echo "# ENVIRONMENT END" >> build-log.txt
echo "#-------------------------------------------------------------------------------" >> build-log.txt

echo "#-------------------------------------------------------------------------------" >> build-log.txt
echo "# H HEADER FILE GENERATE" >> build-log.txt
echo "#-------------------------------------------------------------------------------" >> build-log.txt
chmod +x $VENDOR_ROOT/tools/TCTHeaderGen.py
$VENDOR_ROOT/tools/TCTHeaderGen.py $VENDOR_ROOT/Macro_Desc.csv $VENDOR_ROOT/tct 2>&1 | tee -a build-log.txt
echo "" >> build-log.txt

#simlock
if [ "$simlock" == "false" ];then
  echo "disable __JRD_PERSO_SML__ when simlock is false" >> build-log.txt
  sed -i "s/#define __JRD_PERSO_SML__/\/\/#define __JRD_PERSO_SML__/g" $VENDOR_ROOT/tct/$project/$project"_"$operator.h
  echo "disable FEATURE_TCTNB_RPMB_FOR_SIMLOCK when simlock is false" >> build-log.txt
  sed -i "s/#define FEATURE_TCTNB_RPMB_FOR_SIMLOCK/\/\/#define FEATURE_TCTNB_RPMB_FOR_SIMLOCK/g" $VENDOR_ROOT/tct/$project/$project"_"$operator.h
elif [ "$simlock" == "true" ];then
  echo "# enable __JRD_PERSO_SML__ when simlock is true"
  sed -i "s/\/\/#define __JRD_PERSO_SML__/#define __JRD_PERSO_SML__/g" $VENDOR_ROOT/tct/$project/$project"_"$operator.h
  echo "# enable FEATURE_TCTNB_RPMB_FOR_SIMLOCK when simlock is true"
  sed -i "s/\/\/#define FEATURE_TCTNB_RPMB_FOR_SIMLOCK/#define FEATURE_TCTNB_RPMB_FOR_SIMLOCK/g" $VENDOR_ROOT/tct/$project/$project"_"$operator.h
fi

#clean link files
rm_tct_h $build_dir 2>&1 | tee -a build-log.txt
#here link project_operator.h to tct.h
echo "# Now linking $project"_"$operator.h to tct.h" >> build-log.txt
#link_tct_h $build_dir $project $operator 2>&1 | tee -a build-log.txt
link_tct_h $build_dir $project $operator

#check cpu chip type
CPU_CHIP_TYPE=$(cat "$build_dir/vendor/tct/$project/$project"_"$operator.h" | grep PROJECT_TCTNB_CPU_CHIP_TYPE | awk '{print $3}')
export PROJECT_TCT_RF_CARD=$(cat "$build_dir/vendor/tct/$project/$project"_"$operator.h" | grep PROJECT_TCT_RF_CARD | awk '{print $3}')
export TCT_AVS_TYPE=$(cat "$build_dir/vendor/tct/$project/$project"_"$operator.h" | grep PROJECT_TCT_AVS | awk '{print $3}')

#set partition
set_partition $build_dir $project $operator

echo "#-------------------------------------------------------------------------------" >> build-log.txt
echo "# BUILD BEGIN" >> build-log.txt
echo "#-------------------------------------------------------------------------------" >> build-log.txt
if [[ $boot -eq 1 ]];then
    echo "# Build boot image" >> build-log.txt
    echo "#-------------------------------------------------------------------------------" >> build-log.txt
    build_boot $build_dir 2>&1 | tee -a build-log.txt
    result=$(grep "** Build errors..." -rn build-log.txt)
    if [ "$result" != "" ]; then
        echo "Build boot error! exit!"
        exit 1
    fi
else
    echo "###IGNORE build boot image" >> build-log.txt
fi

if [[ $rpm -eq 1 ]];then
    echo "#-------------------------------------------------------------------------------" >> build-log.txt
    echo "# Build rpm image" >> build-log.txt
    build_rpm $build_dir 2>&1 | tee -a build-log.txt
    result=$(grep "** Build errors..." -rn build-log.txt)
    if [ "$result" != "" ]; then
        echo "Build rpm error! exit!"
        exit 1
    fi
else
    echo "###IGNORE build rpm image" >> build-log.txt
fi

if [[ $tz -eq 1 ]];then
    echo "#-------------------------------------------------------------------------------" >> build-log.txt
    echo "# Build tz image" >> build-log.txt
    build_tz $build_dir 2>&1 | tee -a build-log.txt
    result=$(grep "** Build errors..." -rn build-log.txt)
    if [ "$result" != "" ]; then
        echo "Build tz error! exit!"
        exit 1
    fi
else
    echo "###IGNORE build trustzone image" >> build-log.txt
fi

if [[ $adsp -eq 1 ]];then
    echo "#-------------------------------------------------------------------------------" >> build-log.txt
    echo "# Build adsp image" >> build-log.txt
    build_adsp $build_dir 2>&1 | tee -a build-log.txt
    result=$(grep "** Build errors..." -rn build-log.txt)
    if [ "$result" != "" ]; then
        echo "Build adsp error! exit!"
        exit 1
    fi
else
    echo "###IGNORE build adsp image" >> build-log.txt
fi

if [[ $slpi -eq 1 ]]; then
    echo "#-------------------------------------------------------------------------------" >> build-log.txt
    echo "# Build slpi image" >> build-log.txt
    build_slpi $build_dir 2>&1 | tee -a build-log.txt
    result=$(grep "** Build errors..." -rn build-log.txt)
    if [ "$result" != "" ]; then
        echo "Build slpi error! exit!"
        exit 1
    fi
else
    echo "###IGNORE build slpi image" >> build-log.txt
fi

if [[ $modem -eq 1 ]];then
    echo "#-------------------------------------------------------------------------------" >> build-log.txt
    echo "# Build modem image" >> build-log.txt
    build_modem $build_dir 2>&1 | tee -a build-log.txt
    result=$(grep "** Build errors..." -rn build-log.txt)
    if [ "$result" != "" ]; then
        echo "Build modem error! exit!"
        exit 1
    fi
else
    echo "###IGNORE build modem image" >> build-log.txt
fi

echo "#-------------------------------------------------------------------------------" >> build-log.txt
echo "# BUILD END" >> build-log.txt
echo "#-------------------------------------------------------------------------------" >> build-log.txt
endtime="$(date +%s)"
endtimefmt=`date --date='@'$endtime`
elapsedtime=$(expr $endtime - $starttime)
echo
echo "Start Time = $starttimefmt - End Time = $endtimefmt" >> build-log.txt
echo "Elapsed Time = $elapsedtime seconds" >> build-log.txt

echo "Start Time = $starttimefmt - End Time = $endtimefmt"
echo "Elapsed Time = $elapsedtime seconds"

# Copy image files to common/build
$build_dir/vendor/script/modembin_copy.sh $CPU_CHIP_TYPE
