//============================================================================
//  Name:                                                                     
//    startup.cmm 
//
//  Description:                                                              
//    Common startup script that calls the processor specific init scripts 
//                                                                            
// Copyright (c) 2012 - 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.        
//
//
//
//
//                      EDIT HISTORY FOR FILE
//  This section contains comments describing changes made to the module.
//  Notice that changes are listed in reverse chronological order.
//
// when       who           what, where, why
// --------   ---           ---------------------------------------------------------
// 03/10/2016 JBILLING      Updated temp path for host OS
// 10/26/2015 JBILLING      Changed location of gen_buildflavor.cmm
// 12/03/2013 AJCheriyan    Added MPQ support
// 05/02/2013 AJCheriyan    Added support for History
// 04/18/2013 AJCheriyan    Added default access specifier option
// 04/04/2013 AJCheriyan    Added option for SWD connectivity
// 07/10/2012 AJCheriyan    Created for B-family
//

// Supports three input arguments 
// ARG0 - T32 connection property
// ARG1 - Chipset Name 
// ARG2 - Sub-system Name
// ARG3 - Core Number
// ARG4 - Connectivity option
// ARG5 - Fusion platform option

ENTRY &ARG0 &ARG1 &ARG2 &ARG3 &ARG4 &ARG5

// Global variables for the args passed in along with product flavor information
GLOBAL &T32PROP &CHIPSET &SUBSYS &CORENUM &PRODUCT_FLAVOR &CONNECT &ACCESS &CHIPTYPE

// Save the arguments 
//&T32PROP=STR.LWR("&ARG0")
&T32PROP="&ARG0"
&CHIPSET=STR.LWR("&ARG1")
&SUBSYS="&ARG2"
// &SUBSYS=STR.LWR("&ARG2")
&CORENUM=STR.LWR("&ARG3")
&CONNECT=STR.LWR("&ARG4")
&ACCESS="AXI"

GLOBAL &FUSION_PLATFORM
IF STR.LWR("&ARG5")=="fusion"
(
    &FUSION_PLATFORM="TRUE"
)
ELSE
(
    &FUSION_PLATFORM="FALSE"
)


// Set the default access specifier based on the chipset if different

// Assign the type of the chip for fusion setups
//IF (("&CHIPSET"=="mdm9x45")||("&CHIPSET"=="mdm9645"))
LOCAL &CHIPSET_UPR
&CHIPSET_UPR=STR.UPR("&CHIPSET")
IF STRING.SCAN("&CHIPSET_UPR","MDM",0)!=-1
(
	&CHIPTYPE="MDM"
)
ELSE
(
	&CHIPTYPE="MSM"
)


// Locals used
LOCAL &SUBSYSCRIPT &SESSIONSCRIPT

// Now call the subsystem level script
&SUBSYSCRIPT="&T32PROP"+"_"+"&SUBSYS"
PRINT "Calling script: &SUBSYSCRIPT with args: &CHIPSET &CORENUM &CONNECT"
do config/&SUBSYSCRIPT &CHIPSET &CORENUM &CONNECT

// At this point we have the location of the scripts for 
// this session. Call the script to populate the build information
PRINT "Calling script: gen_buildinfo"

//Get host os type
GLOBAL &TEMPDIR &HOSTOS &TEMP
IF (VERSION.BUILD()<65625.)
(
    &HOSTOS="Windows"
)
ELSE
(
    &HOSTOS=OS.NAME()
)
IF ("&HOSTOS"=="Windows")
(
    &TEMPDIR=OS.ENV(TEMP)
)
ELSE
(
    &TEMPDIR="~~~"
)

IF FILE.EXIST("&TEMPDIR/gen_buildinfo.cmm")
(
    GLOBAL &BUILDINFOHASRUN
    &BUILDINFOHASRUN="TRUE"
    do &TEMPDIR/gen_buildinfo.cmm
)
ELSE
(
    PRINT "Warning - Could not access gen_buildinfo.cmm at &TEMPDIR"
)
&TEMP="&TEMPDIR"
//FIXME - here until variable name is changed
IF ((STRING.SCAN("&APPS_BUILDROOT","APPS_BUILDROOT",0)!=-1)&&(STRING.SCAN("&APSS_BUILDROOT","APSS_BUILDROOT",0)==-1))
(
    GLOBAL &APPS_BUILDROOT
    &APPS_BUILDROOT="&APSS_BUILDROOT"
)
// Now setup the scripts information first
PRINT "Calling script: std_toolsconfig with args: &CHIPSET &SUBSYS"
do std_toolsconfig &CHIPSET &SUBSYS
// Load up the product flavor information for the meta build
// PRODUCT_FLAVOR information is obtained from 
IF ("&PRODUCT_FLAVORS"=="")
(
    PRINT %ERROR "Product Flavor invalid. Invalid meta build configuration"
    GOTO FATALEXIT
)

GLOBAL &BUILD_FLAVOR_SCRIPT
IF FILE.EXIST(&METASCRIPTSDIR/../../../build/app/gen_buildflavor.cmm)
(
    &BUILD_FLAVOR_SCRIPT="&METASCRIPTSDIR/../../../build/app/gen_buildflavor.cmm"
)
ELSE IF (FILE.EXIST(&METASCRIPTSDIR/gen/gen_buildflavor.cmm))
(
    &BUILD_FLAVOR_SCRIPT="&METASCRIPTSDIR/gen/gen_buildflavor.cmm"
)
ELSE
(

            WINPOS 0. 0. 50. 10.
            area.create gen_buildflavor
            AREA.SELECT gen_buildflavor
            area.view gen_buildflavor
            PRINT %ERROR "   Warning! gen_buildflavor.cmm not found!" 
            PRINT " "
            PRINT %ERROR "   Metabuild binary paths, "
            PRINT %ERROR "   ELF file paths and build "
            PRINT %ERROR "   flavors will be missing."
            PRINT " "
            PRINT " "
            PRINT " "
            area.select A000
            wait.3s
    
    &BUILD_FLAVOR_SCRIPT="NULL"
)

IF "&BUILD_FLAVOR_SCRIPT"!="NULL"
(
    IF (STR.SCAN("&PRODUCT_FLAVORS",",",0)<0.)
    (
        // This means there is just one product flavor
        do &BUILD_FLAVOR_SCRIPT &PRODUCT_FLAVORS
        &PRODUCT_FLAVOR="&PRODUCT_FLAVORS"
    )
    ELSE
    (
        // Populate the data using the first one and make it the default
        &DEFAULT_FLAVOR=STR.MID("&PRODUCT_FLAVORS",0,STR.SCAN("&PRODUCT_FLAVORS",",",0))
        do &BUILD_FLAVOR_SCRIPT &DEFAULT_FLAVOR
        &PRODUCT_FLAVOR="&DEFAULT_FLAVOR"
    )
)
// Perform the intercom initialization
do std_intercom_init NEWSESSION

// Now perform the debug session specific setup
// Pass the sub-system name along as well
PRINT "Calling script: std_sessioncfg with args: &SUBSYS"
&SESSIONSCRIPT="std_sessioncfg_"+STR.LWR("&SUBSYS")
do &SESSIONSCRIPT INIT &CHIPSET &SUBSYS

// Other various varaibles
GLOBAL &STDRESETPMIC
&STDRESETPMIC="TRUE"

// Now save the history as well
AUTOSTORE , HISTORY

EXIT:
    ENDDO

FATALEXIT:
    END

