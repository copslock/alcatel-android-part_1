#!/usr/bin/python
# *************************************************
# Copyright (c) 2016  ARM Ltd (or its subsidiaries)
# *************************************************

import argparse
import base64
import copy
import hashlib
import json
import logging
import marshal
import os
import re
import shutil
import subprocess
import sys

import config_provider
import json_helper
import qc_src_tree_helper
from argparse_action_helper import AppendValidFile
from argparse_action_helper import StoreInputDir
from argparse_action_helper import StoreOutputDir
from argparse_action_helper import StoreValidFile

__author__ = 'alexander.zilberkant@arm.com'
__version__ = '2.0.0'

SCRIPT_ROOT = os.path.dirname(os.path.abspath(__file__))
QC_BACKUP_DIR = os.path.join(SCRIPT_ROOT, 'qc_backup')
logger = logging.getLogger('ARM-TA')


class ArmTaSpec(object):
    def __init__(self, ta_name, stack_size, heap_size, lib_md5s):
        self.ta_name = ta_name
        self.lib_md5s = lib_md5s
        self.stack_size = stack_size
        self.heap_size = heap_size

    @classmethod
    def from_file(cls, file_name):
        build_spec = json_helper.from_file(file_name)
        return cls(**build_spec)


class QcBuildConfig(object):
    def __init__(self, sources, includes, defines, arm_ta_libs, qc_libs, metadata, arm_ta_spec):

        qc_tree_sources = []  # source as they seen by SCons during compilation
        for src in sources:
            if src.endswith('.c') or src.endswith('.cpp'):
                qc_tree_sources.append(os.path.join('${BUILD_ROOT}', 'arm-ta', os.path.basename(src)))

        qc_tree_libs = qc_libs  # libraries as they seen by SCons during compilation
        for lib in arm_ta_libs:
            qc_tree_libs.append(os.path.join('${BUILD_ROOT}', 'arm-ta', os.path.basename(lib)))

        local_metadata = copy.deepcopy(metadata)
        local_metadata['appName'] = arm_ta_spec.ta_name
        self.ta_config = {
            'includes': [os.path.join('${BUILD_ROOT}', 'arm-ta')] + includes,
            'sources': qc_tree_sources,
            'metadata': local_metadata,
            'defines': defines,
            'libs': qc_tree_libs,
            'heap_size': arm_ta_spec.heap_size,
            'stack_size': arm_ta_spec.stack_size,
        }
        logger.debug('ta_cfg=%s', str(self.__dict__))

    def serialize(self):
        return base64.b64encode(marshal.dumps(self.ta_config))


class QcBuilder(object):
    def __init__(self, pkg_root, chipset, target_name, build_config):
        self.serialized_cfg = build_config.serialize()
        self.chipset = chipset
        self.target_name = target_name
        self.pkg_root = pkg_root
        logger.debug('ARM_TA_CFG=%s', self.serialized_cfg)

    def __invoke(self, is_clean_build):

        my_env = os.environ.copy()
        my_env['ARM_TA_CFG'] = self.serialized_cfg
        build_sh_arg = [] if not is_clean_build else ['-c']
        f_null = open(os.devnull, 'w')
        stream_kwargs = {}

        if logger.isEnabledFor(logging.DEBUG):
            build_sh_arg.append('--verbose=1')
        elif not logger.isEnabledFor(logging.INFO):
            stream_kwargs = {'stdout': f_null, 'stderr': subprocess.STDOUT}

        subprocess.check_call(
            ['bash', '-e', './build.sh', 'CHIPSET=' + self.chipset, self.target_name] + build_sh_arg,
            cwd=os.path.join(self.pkg_root, 'build', 'ms'),
            env=my_env,
            **stream_kwargs
        )

    def clean(self):
        self.__invoke(True)

    def build(self):
        self.__invoke(False)


def get_parser():
    parser = argparse.ArgumentParser(
        description='Postbuild for ARM TA library'
    )

    qc_cfg_group = parser.add_argument_group('Qualcomm Source Configuration')

    qc_cfg_group.add_argument(
        '-s',
        '--source-dir',
        action=StoreInputDir,
        metavar='DIR',
        required=True,
        help="specify directory containing Qualcomm's source tree"
    )

    qc_cfg_group.add_argument(
        '-c',
        '--chipset',
        help='chipset. i.e. msm8996 or msm8937',
        required=True
    )

    qc_cfg_group.add_argument(
        '-e',
        '--env-setup',
        action=StoreValidFile,
        help='path to custom Qualcomm environment setup script (setenv.sh). '
             'In case provided - it will replace <QCT_ROOT>/trustzone_images/build/ms/setenv.sh. '
    )

    arm_ta_cfg_group = parser.add_argument_group('ARM TA Configuration')

    arm_ta_cfg_group.add_argument(
        '-l',
        '--lib',
        dest='libs',
        action=AppendValidFile,
        required=True,
        metavar='LIB_PATH',
        help='path to ARM-TA static library (XXX.lib). Can be applied more than once for '
             'specifying multiple libs'
    )

    arm_ta_cfg_group.add_argument(
        '-b',
        '--build-spec',
        action=StoreValidFile,
        required=True,
        help='path to ARM-TA build spec provided with the release, for example: *_build_spec.json'
    )

    arm_ta_cfg_group.add_argument(
        '-o',
        '--out',
        required=True,
        action=StoreOutputDir,
        help='specify output directory for target files (*.elf, *.mbn, *.map, TZ split files)'
    )

    build_ctrl = parser.add_argument_group('Build Control')
    build_ctrl.add_argument(
        '--inplace',
        action='store_true',
        help='do not copy QC sources before build - modify sources inplace'
    )
    verbosity_ctrl_mutual = build_ctrl.add_mutually_exclusive_group()
    verbosity_ctrl_mutual.add_argument(
        '-v',
        '--verbose',
        action='store_true',
        dest='verbosity',
        help='set verbosity mode'
    )
    verbosity_ctrl_mutual.add_argument(
        '-q',
        '--quiet',
        action='store_false',
        dest='verbosity',
        help='execute in quiet mode'
    )

    build_ctrl.add_argument(
        '--clean',
        action='store_true',
        help='perform Qualcomm cleanup'
    )

    build_ctrl.add_argument(
        '--restore',
        action='store_true',
        help='restore Qualcomm tree sources - remove ARM-TA sources and libraries copied to the tree'
    )

    return parser


def get_qc_build_id(pkg_root, chipset):
    """
    parser build.sh script and retrieve BUILD_ID for current chipset
    """
    with open(os.path.join(pkg_root, 'build/ms/build.sh'), 'rt') as fh:
        pattern = re.compile(
            r'"CHIPSET\s*={}"\s*\]\s*;\s*then.*?export BUILD_ID\s*=\s*(\S+)'.format(chipset),
            re.DOTALL
        )
        match = pattern.search(fh.read())
        if not match:
            raise Exception('Failed to find BUILD_ID for {} in build/ms/build.sh'.format(chipset))
        return match.group(1)


def do_postbuild(arm_ta_spec, args):
    if not args.inplace:
        new_root = QC_BACKUP_DIR
        if os.path.isdir(new_root):
            logger.debug('remove existing backup dir: %s', new_root)
            shutil.rmtree(new_root)
        logger.debug('copy sources to backup dir: %s -> %s', args.source_dir, new_root)
        shutil.copytree(args.source_dir, new_root)
        logger.debug('copy sources to backup dir: - done')
        pkg_root = qc_src_tree_helper.find_tz_root(new_root)
    else:
        pkg_root = qc_src_tree_helper.find_tz_root(args.source_dir)

    qc_build_id = get_qc_build_id(pkg_root, args.chipset)

    cfg = config_provider.Config(
        build_id=qc_build_id,
        qc_manifest=config_provider.QcManifest.form_package(pkg_root),
        config_file=os.path.join(SCRIPT_ROOT, 'arm_ta_cfg.json'))

    qc_builder = QcBuilder(
        pkg_root=pkg_root,
        target_name=cfg.target_name,
        chipset=args.chipset,
        build_config=QcBuildConfig(
            sources=cfg.extra_sources,
            includes=cfg.include_dirs,
            defines=cfg.defines,
            arm_ta_libs=args.libs,
            qc_libs=cfg.extra_qc_libs,
            metadata=cfg.metadata,
            arm_ta_spec=arm_ta_spec
        )
    )

    if args.env_setup:
        qc_src_tree_helper.replace_envsetup(os.path.join(pkg_root, 'build', 'ms', 'setenv.sh'), args.env_setup)

    integrated = qc_src_tree_helper.is_arm_ta_integrated(pkg_root)
    if args.clean or args.restore:
        qc_builder.clean()
        if args.restore:
            qc_src_tree_helper.restore(pkg_root=pkg_root, patch_file=cfg.patch_file, integrated=integrated)
        return

    if not integrated:
        qc_src_tree_helper.apply_arm_ta_patch(pkg_root, cfg.patch_file)

    qc_src_tree_helper.copy_files_to_tree(os.path.join(pkg_root, 'arm-ta'), cfg.extra_sources + args.libs)

    qc_builder.build()

    qc_src_tree_helper.collect_artifacts(
        pkg_root=pkg_root,
        out_dir=args.out,
        build_id=qc_build_id,
        sample_app_target_name=cfg.sample_app_target_name,
        ta_name=arm_ta_spec.ta_name
    )


def main(argv):
    parser = get_parser()
    args = parser.parse_args(argv)

    debug_level = logging.INFO
    if args.verbosity is not None:
        if args.verbosity:
            debug_level = logging.DEBUG
        else:
            debug_level = logging.WARNING

    logging.basicConfig(
        level=debug_level,
        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    )
    logger.info('starting ARM-TA Postbuild tool v:' + __version__)
    logger.debug('command line arguments: \n' + json.dumps(args.__dict__, indent=2))
    arm_ta_spec = ArmTaSpec.from_file(args.build_spec)
    for lib in args.libs:
        with open(lib, 'rb') as fh:
            md5_hash = hashlib.md5(fh.read()).hexdigest()
        if md5_hash not in arm_ta_spec.lib_md5s:
            raise Exception(
                'unknown lib:{} - make sure postbuild script invoked belongs to the same release'.format(lib)
            )

    do_postbuild(arm_ta_spec, args)


if __name__ == '__main__':
    main(sys.argv[1:])
