# *************************************************
# Copyright (c) 2016  ARM Ltd (or its subsidiaries)
# *************************************************

import json


def from_file(file_name):
    """
    helper function that loads json file with all string values and keys
    represented as string objects

    by default json loads treats all string values as unicode
    perhaps will not be needed on Python 3
    """
    def utf8_json_decoder_json_hook(pairs):
        """
        callback function to be called by json.load for handling object
        converts keys and string values from unicode to string objects
        """
        new_pairs = []
        for key, value in pairs.items():
            if isinstance(value, unicode):
                value = value.encode('utf-8')
            if isinstance(key, unicode):
                key = key.encode('utf-8')
            new_pairs.append((key, value))
        return dict(new_pairs)

    with open(file_name, 'rt') as fh:
        return json.load(fh, object_hook=utf8_json_decoder_json_hook)