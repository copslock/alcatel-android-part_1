#ifndef DX_QSEE_ENV_H
#define DX_QSEE_ENV_H

#include <stdint.h>
#include "object.h"

// Request a service from the system.
//
// On success, *objOut will hold the resulting object reference and
// Object_OK will be returned.  In this case, the caller will hold a
// reference to the object and is responsible for releasing it at some
// point.
//
// Otherwise, a non-zero value will be returned and *objOut will be
// Object_NULL.
//
int32_t dx_qsee_open(uint32_t uid, Object *objOut);

#endif // DX_QSEE_ENV_H