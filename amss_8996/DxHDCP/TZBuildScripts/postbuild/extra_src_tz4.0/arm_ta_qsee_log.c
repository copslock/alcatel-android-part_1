
#include <stddef.h>
#include <stdint.h>
#include "comdef.h"

#include "qsee_log.h"
#include "arm_ta_qsee_log.h"


void dx_qsee_log(uint8_t pri, const char* fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);

	qsee_log(pri, fmt, ap);

	va_end(ap);
}

