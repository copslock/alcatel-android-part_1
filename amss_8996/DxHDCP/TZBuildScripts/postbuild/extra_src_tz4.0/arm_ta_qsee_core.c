#include <stddef.h>
#include <stdint.h>
#include "comdef.h"

#include "qsee_core.h"
#include "qsee_log.h"
#include "../../../../accesscontrol/api/ACCommon.h"
#include "arm_ta_qsee_core.h"


bool dx_qsee_is_ns_range(const void* start, uint32_t len)
{
    return qsee_is_ns_range(start, len);
}


static int Map_DX_ACVirtualMachineId(DX_ACVirtualMachineId param_in, ACVirtualMachineId *param_out)
{
    int res = 0;

    switch (param_in)
    {
    case DX_AC_VM_HLOS:
        *param_out = AC_VM_HLOS;
        break;

    case DX_AC_VM_CP_BITSTREAM:
        *param_out = AC_VM_CP_BITSTREAM;
        break;

    default:
        res = -1;
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "%s failed : invalid AC Virtual Machine Id %" PRIu32, __FUNCTION__, (uint32_t)param_in);
        break;
    }
    return res;
}


bool dx_qsee_is_s_tag_area
( 
  uint32_t                    vmid,
  uint64_t                    start,
  uint64_t                    end
)
{

	ACVirtualMachineId vmid_converted = AC_VM_MAX;

	if (0 != Map_DX_ACVirtualMachineId(vmid, &vmid_converted))
	{
		QSEE_LOG(QSEE_LOG_MSG_ERROR, "%s failed : Map_DX_ACVirtualMachineId failed!", __FUNCTION__);
		return -1;
	}

    return qsee_is_s_tag_area(
    		vmid_converted,
    		start,
    		end);
}

int dx_qsee_hdmi_status_read(uint32_t* hdmi_enable, uint32_t* hdmi_sense, uint32_t* hdcp_auth) {
    return qsee_hdmi_status_read(hdmi_enable, hdmi_sense, hdcp_auth);
}
