#include <stddef.h>
#include <stdint.h>
#include "comdef.h"

#include "qsee_sfs.h"
#include "arm_ta_qsee_sfs.h"

int dx_qsee_sfs_getSize(int fd, uint32_t* size)
{
	return qsee_sfs_getSize(fd, size);
}

int dx_qsee_sfs_open(const char* path, int flags)
{
	return qsee_sfs_open(path, flags);

}

int dx_qsee_sfs_close(int fd)
{
	return qsee_sfs_close(fd);
}

int dx_qsee_sfs_read(int fd, char *buf, int nbytes)
{
	return qsee_sfs_read(fd, buf, nbytes);
}

int dx_qsee_sfs_write(int fd, const char *buf, int nbytes)
{
	return qsee_sfs_write(fd, buf, nbytes);
}


int dx_qsee_sfs_rm(const char *path)
{
	return qsee_sfs_rm(path);
}
