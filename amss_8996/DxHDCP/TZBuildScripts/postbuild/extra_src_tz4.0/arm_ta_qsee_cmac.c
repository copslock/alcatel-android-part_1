#include <stddef.h>
#include <stdint.h>
#include "comdef.h"

#include "qsee_log.h"
#include "qsee_cmac.h"
#include "arm_ta_qsee_cmac.h"


static int Map_DX_QSEE_CMAC_ALGO_ET(DX_QSEE_CMAC_ALGO_ET param_in, QSEE_CMAC_ALGO_ET *param_out)
{
    int res = 0;

    switch (param_in)
    {
    case DX_QSEE_CMAC_ALGO_AES_128:
        *param_out = QSEE_CMAC_ALGO_AES_128;
        break;

    case DX_QSEE_CMAC_ALGO_AES_256:
        *param_out = QSEE_CMAC_ALGO_AES_256;
        break;
        
    default:
        res = -1;
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "%s failed : invalid CMAC_ALGO_ET %" PRIu32, __FUNCTION__, (uint32_t)param_in);
        break;
    }
    return res;
}


        

int dx_qsee_cmac(DX_QSEE_CMAC_ALGO_ET alg, const uint8_t *msg, uint32_t msg_len,
              const uint8_t *key, uint32_t key_len, uint8_t *cmac_digest,
              uint32_t cmac_len)
{
	QSEE_CMAC_ALGO_ET alg_converted = QSEE_CMAC_ALGO_AES_128;

    if (0 != Map_DX_QSEE_CMAC_ALGO_ET(alg, &alg_converted))
    {
    	QSEE_LOG(QSEE_LOG_MSG_ERROR, "%s failed : Map_DX_QSEE_CMAC_ALGO_ET failed!", __FUNCTION__);
        return -1;
    }

    return qsee_cmac(alg_converted,msg, msg_len,
            key, key_len, cmac_digest,
            cmac_len);

}
