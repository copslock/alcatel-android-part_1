#ifndef ARM_TA_QSEE_CLK_H
#define ARM_TA_QSEE_CLK_H

#include <stdint.h>

/*
* Level of clock frequencies
*/

typedef enum
{
    DX_QSEE_CLK_LVL_INACTIVE = 0x0,
    DX_QSEE_CLK_LVL_LOW = 0x1,
    DX_QSEE_CLK_LVL_MEDIUM = 0x2,
    DX_QSEE_CLK_LVL_HIGH = 0x3,
    DX_QSEE_CLK_LVL_COUNT_MAX = 0x4,
    DX_QSEE_CLK_LVL_MAX_ENTRY = 0x7FFFFFFF
}DX_QSEE_CLKLEV;

/**
Sets clock frequency

@param[in]  tzapp_name     tzapp name.
@param[in]  tzapp_name_len Length of tzapp name.
@param[in]  clock_level    Level to set the clock frequency (to unset previous set give DX_QSEE_CLK_LVL_INACTIVE).

@return
E_SUCCESS/E_FAILURE

*/
uint32_t dx_qsee_set_bandwidth(const char *tzapp_name, uint32_t tzapp_name_len, DX_QSEE_CLKLEV clock_level);

#endif /* ARM_TA_QSEE_CLK_H */
