
#include "qsee_clk.h"
#include "arm_ta_qsee_clk.h"

static int map_dx_qsee_clock_level(DX_QSEE_CLKLEV param_in, QSEE_CLKLEV *param_out)
{
    int res = 0;
    switch (param_in)
    {
    case DX_QSEE_CLK_LVL_INACTIVE:
        *param_out = INACTIVE;
        break;

    case DX_QSEE_CLK_LVL_LOW:
        *param_out = LOW;
        break;

    case DX_QSEE_CLK_LVL_MEDIUM:
        *param_out = MEDIUM;
        break;

    case DX_QSEE_CLK_LVL_HIGH:
        *param_out = HIGH;
        break;

    case DX_QSEE_CLK_LVL_COUNT_MAX:
        *param_out = COUNT_MAX;
        break;

    default:
        res = -1;
        break;
    }
    return res;
}

uint32_t dx_qsee_set_bandwidth(const char *tzapp_name, uint32_t tzapp_name_len, DX_QSEE_CLKLEV clock_level)
{
    QSEE_CLKLEV clock_level_converted = COUNT_MAX;

    if (0 != map_dx_qsee_clock_level(clock_level, &clock_level_converted))
    {
        return 1;
    }

    return qsee_set_bandwidth((void *)tzapp_name, tzapp_name_len, (uint32_t)CE1, (uint32_t)clock_level_converted, 0);

}

