#ifndef ARM_TA_QSEE_SFS_H_
#define ARM_TA_QSEE_SFS_H_

/**
@file qsee_sfs.h
@brief Secure File System with Encryption and Integrity Protection.
This file contains the definitions of the constants, data 
structures, and interfaces that provide methods to 
encrypt/integrity-protect and decrypt/verify content to the EFS.
*/



#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>


#define DX_O_RDONLY 00000000

#define DX_O_RDWR   00000002

#define DX_O_CREAT  00000100

#define DX_O_TRUNC  00001000


/**
   @addtogroupqsee_sfs_apis 
   @{
*/

/**
Retrieves the size of an open SFS file.

retrieves the file size of an open sfs file 

@param[in] fd    File descriptor.
@param[out] size Open file size.
  
@return
E_SUCCESS - File size is stored in the size parameter. \n 
E_FAILURE - Error occurred while getting the file size. 

*/
int dx_qsee_sfs_getSize(int fd, uint32_t* size);


/**
Opens an SFS file.\ The options are specified by the File mode in the flag 
parameter.

In the SFS, opening a file does not actually do anything in the file 
system. However, if the file (along with the associated file segments) already 
exists and the file is created with the DX_O_TRUNC mode, the associated 
subfiles are deleted. The first segment is created only when new bytes to be 
written begin arriving.
Note:The base directory must exist; otherwise, a NULL is returned.

@param[in] path      Fully qualified path of the filename to be
                     opened.
@param[in] flags     Bitmask field that is used to specify file 
                     modes.
                     - DX_O_RDONLY - Open for read-only access.
                     - O_READWRITE - Open for read-write access.
                     - DX_O_CREAT - Create the file if it does not
                       exist.
                     - DX_O_TRUNC - Truncate the file to zero size
                       after opening.
                     - O_APPEND - Write operations occur at
                       the end of the file.

@return
Non Zero - Opened the file successfully. 
Zero - Error occurred while opening the file.

*/
int dx_qsee_sfs_open(const char* path, int flags);

/**
Closes an open SFS file.\
All the resources used by the file are released.

@param[in] fd File descriptor.

@return
E_SUCCESS - Closed the file successfully. \n 
E_FAILURE - Error occurred while closing the file. 

*/
int dx_qsee_sfs_close(int fd);

/**
Reads bytes from an encrypted SFS file that was previously opened via 
a call to IxFileMgr::OpenFile. 

The bytes are read from the current file position, and the file position 
advances by the number of read bytes. The SFS performs the necessary cipher and 
verification operations as bytes are being read from the file.  

@param[in] fd     File descriptor.
@param[in] buf    Pointer to the buffer to hold the bytes read.
@param[in] nbytes Number of bytes to read from the file.

@return 
Returns the number of bytes read from an SFS file. 
Returns -1 for the error case

*/
int dx_qsee_sfs_read(int fd, char *buf, int nbytes);

/**
Writes bytes to an encrypted SFS file that was previously opened 
via a call to dx_qsee_sfs_open. 

The bytes are written to the current file position. If the APPEND flag was set 
in dx_qsee_sfs_open(), the bytes are written at the end of the file unless 
dx_qsee_sfs_seek() was issued before dx_qsee_sfs_write(). In this case, the data is written to 
the file position that dx_qsee_sfs_seek() set. The file position advances by the number 
of bytes written. 

@param[in] fd     File descriptor.
@param[in] buf    Pointer to the buffer to be written.
@param[in] nbytes Number of bytes to write to the file.

@return
Returns the number of bytes written to an SFS file. 
Returns -1 for the error case

*/
int dx_qsee_sfs_write(int fd, const char *buf, int nbytes);

/**
Removes an SFS file that was previously created through dx_qsee_sfs_open().

@param[in] path Fully qualified path of the file to be deleted.

@return
E_SUCCESS - File removal is successful. \n 
E_FAILURE - Error occurred while removing the file. 

*/
int dx_qsee_sfs_rm(const char *path);


/** @} */   /* end_addtogroup qsee_sfs_apis */

#ifdef __cplusplus
}
#endif

#endif // ARM_TA_QSEE_SFS_H_
