#ifndef ARM_TA_QSEE_HMAC_H
#define ARM_TA_QSEE_HMAC_H

/**
@file qsee_hmac.h
@brief Provide hmac API wrappers
*/


/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include <stdint.h>

/*----------------------------------------------------------------------------
 * Type Declarations
 * -------------------------------------------------------------------------*/

/** Supported HMAC algorithms   */
typedef enum
{
	DX_QSEE_HMAC_SHA1          = 1,
	DX_QSEE_HMAC_SHA256        = 2,
	DX_QSEE_HMAC_INVALID       = 0x7FFFFFFF,

}  	DX_QSEE_HMAC_ALGO_ET;

/*----------------------------------------------------------------------------
 * Function Declarations and Documentation
 * -------------------------------------------------------------------------*/

/**
 * @brief Create a hash MAC per FIPS pub 198 using the specified hash algorithm.
 *
 * @param[in] msg             - Pointer to message to be authenticated
 * @param[in] msg_len         - Length of message in bytes
 * @param[in] key             - Pointer to input key to HMAC algorithm
 * @param[in] key_len         - Length of input key in bytes
 * @param[out] msg_digest     - Pointer to message digest (memory provided by caller)
 *
 * @return 0 on success, negative on failure
*/
int dx_qsee_hmac(DX_QSEE_HMAC_ALGO_ET alg, const uint8_t *msg, uint32_t msg_len,
              const uint8_t *key, uint16_t key_len, uint8_t *msg_digest);

#endif /*ARM_TA_QSEE_HMAC_H*/

