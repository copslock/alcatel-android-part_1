#ifndef DX_QSEE_CORE_H
#define DX_QSEE_CORE_H

/**
@file qsee_core.h
@brief Provide Core functionality
*/

/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/

#include <stdint.h>
#include <stdbool.h>
#include "comdef.h"



/*----------------------------------------------------------------------------
 * Function Declarations and Documentation
 * -------------------------------------------------------------------------*/


/**
 * Tests whether all of the range [\c start, \c start + \c len] is in
 * non-secure memory. This is a convenience function to access \c
 * tzbsp_is_ns_area. \c NULL is a valid value for \c start, because
 * physical addressing is used.
 *
 * @param [in] start Start of the memory range, physical address,
 * included in the range.
 *
 * @param [in] len Length of the memory range in bytes.
 *
 * @return \c TRUE if the entire area is in non-secure memory. \c
 * FALSE if the area contains secure memory.
 */
bool dx_qsee_is_ns_range(const void* start, uint32_t len);



/* Tag that pages of shared memory tagged with.
 * Value from this enum should be argument for the 'vmid' parameter of dx_qsee_is_s_tag_area */
typedef enum
{
  DX_AC_VM_NONE,
  DX_AC_VM_TZ,
  DX_AC_VM_RPM,  /* Single stage */
  DX_AC_VM_HLOS,
  DX_AC_VM_HYP,
  DX_AC_VM_SSC_Q6_ELF, /* Single */  /* IPA = PA */
  DX_AC_VM_ADSP_Q6_ELF, /* Single */
  DX_AC_VM_SSC_HLOS,   /* ??, may be we combine this with other SSC one */
  DX_AC_VM_CP_TOUCH,
  DX_AC_VM_CP_BITSTREAM,
  DX_AC_VM_CP_PIXEL,
  DX_AC_VM_CP_NON_PIXEL,
  DX_AC_VM_VIDEO_FW,
  DX_AC_VM_CP_CAMERA,
  DX_AC_VM_HLOS_UNMAPPED,  /* Memory owned by HLOS but not currently mapped in HLOS VM */
  DX_AC_VM_MSS_MSA, /* This is MSA=1 view */
  DX_AC_VM_MSS_NONMSA, /* IPA not equal PA */
  DX_AC_VM_UNMAPPED,
  DX_AC_VM_LAST,
  DX_AC_VM_MAX = 0x7FFFFFFF,
}DX_ACVirtualMachineId;



/**
 * Tests whether all of the range [\c start, \c end] is tagged
 * for the particular VMID, specific to CPZ use case
 *  
 * @param [in] vmid  Virtual machine ID defined in access
 *        control layer (enum ACVirtualMachineId)
 *        (see enum DX_ACVirtualMachineId for TzInfra code implementation)
 *  
 * @param [in] start Start of the memory range, physical address,
 * included in the range.
 *
 * @param [in] end End of the memory range, physical address,
 * included  in the range.
 *
 * @return \c TRUE if the entire area is tagged for VMID. \c
 *  FALSE if not
 */
bool dx_qsee_is_s_tag_area(uint32_t vmid, uint64_t start, uint64_t end);

/**
* Reads the status of the HDMI link and hardware HDCP
* @param [out] hdmi_enable  HDMI output enabled
* @param [out] hdmi_sense HDMI sense
* @param [out] hdcp_auth HDCP authentication success.
* @return 0 on success
*/
int dx_qsee_hdmi_status_read(uint32_t* hdmi_enable, uint32_t* hdmi_sense, uint32_t* hdcp_auth);

#endif /* DX_QSEE_CORE_H */

