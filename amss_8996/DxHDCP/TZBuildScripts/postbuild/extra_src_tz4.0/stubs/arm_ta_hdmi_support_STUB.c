#include <comdef.h>

int qsee_hdmi_status_read(uint32_t *hdmi_enable, uint32_t *hdmi_sense, uint32_t *hdcp_auth)
{
    if (hdmi_enable)
        *hdmi_enable = 0;

    if (hdmi_sense)
        *hdmi_sense = 0;

    if (hdcp_auth)
        *hdcp_auth = 0;

    return 0;
}
