
#include <stddef.h>
#include <stdint.h>

#include "qsee_cipher.h"
#include "qsee_log.h"
#include "arm_ta_qsee_cipher.h"


static int Map_DX_QSEE_CIPHER_ALGO_ET(DX_QSEE_CIPHER_ALGO_ET param_in, QSEE_CIPHER_ALGO_ET *param_out)
{
    int res = 0;

    switch (param_in)
    {
        case DX_QSEE_CIPHER_ALGO_AES_128:
            *param_out = QSEE_CIPHER_ALGO_AES_128;
            break;
        case DX_QSEE_CIPHER_ALGO_AES_256:
            *param_out = QSEE_CIPHER_ALGO_AES_256;
            break;
        default:
            res=-1;
        	QSEE_LOG(QSEE_LOG_MSG_ERROR, "%s failed : invalid CIPHER_ALGO_ET %" PRIu32, __FUNCTION__, (uint32_t)param_in);
            break;
    }
    return res;
}



static int Map_DX_QSEE_CIPHER_PARAM_ET(DX_QSEE_CIPHER_PARAM_ET param_in, QSEE_CIPHER_PARAM_ET *param_out)
{
    int res = 0;

    switch (param_in)
    {
    case DX_QSEE_CIPHER_PARAM_KEY:
        *param_out = QSEE_CIPHER_PARAM_KEY;
        break;

    case DX_QSEE_CIPHER_PARAM_IV:
        *param_out = QSEE_CIPHER_PARAM_IV;
        break;

    case DX_QSEE_CIPHER_PARAM_MODE:
        *param_out = QSEE_CIPHER_PARAM_MODE;
        break;

    case DX_QSEE_CIPHER_PARAM_PAD:
        *param_out = QSEE_CIPHER_PARAM_PAD;
        break;

    default:
        res = -1;
    	QSEE_LOG(QSEE_LOG_MSG_ERROR, "%s failed : invalid PARAM_ET %" PRIu32, __FUNCTION__, (uint32_t)param_in);
        break;
    }
    return res;
}


static int Map_DX_QSEE_CIPHER_MODE_ET(DX_QSEE_CIPHER_MODE_ET param_in, QSEE_CIPHER_MODE_ET *param_out)
{
    int res = 0;

    switch (param_in)
    {
    case DX_QSEE_CIPHER_MODE_ECB:
        *param_out = QSEE_CIPHER_MODE_ECB;
        break;

    case DX_QSEE_CIPHER_MODE_CBC:
        *param_out = QSEE_CIPHER_MODE_CBC;
        break;

    case DX_QSEE_CIPHER_MODE_CTR:
        *param_out = QSEE_CIPHER_MODE_CTR;
        break;

    default:
        res = -1;
    	QSEE_LOG(QSEE_LOG_MSG_ERROR, "%s failed : invalid MODE_ET %" PRIu32, __FUNCTION__, (uint32_t)param_in);
        break;
    }
    return res;
}


static int Map_DX_QSEE_CIPHER_PAD_ET(DX_QSEE_CIPHER_PAD_ET param_in, QSEE_CIPHER_PAD_ET *param_out)
{
    int res = 0;
    switch (param_in)
    {
    case DX_QSEE_CIPHER_PAD_ISO10126:
        *param_out = QSEE_CIPHER_PAD_ISO10126;
        break;

    case DX_QSEE_CIPHER_PAD_PKCS7:
        *param_out = QSEE_CIPHER_PAD_PKCS7;
        break;

    case DX_QSEE_CIPHER_PAD_NO_PAD:
        *param_out = QSEE_CIPHER_PAD_NO_PAD;
        break;

    default:
    	res = -1;
    	QSEE_LOG(QSEE_LOG_MSG_ERROR, "%s failed : invalid PAD_ET %" PRIu32, __FUNCTION__, (uint32_t)param_in);
    	break;
    }
    return res;
}




static int Map_QSEE_CIPHER_MODE_ET(QSEE_CIPHER_MODE_ET param_in, DX_QSEE_CIPHER_MODE_ET *param_out)
{
    int res = 0;
    switch (param_in)
    {
    case QSEE_CIPHER_MODE_ECB:
        *param_out = DX_QSEE_CIPHER_MODE_ECB;
        break;

    case QSEE_CIPHER_MODE_CBC:
        *param_out = DX_QSEE_CIPHER_MODE_CBC;
        break;

    case QSEE_CIPHER_MODE_CTR:
        *param_out = DX_QSEE_CIPHER_MODE_CTR;
        break;

    default:
        res = -1;
    	QSEE_LOG(QSEE_LOG_MSG_ERROR, "%s failed : invalid MODE_ET %" PRIu32, __FUNCTION__, (uint32_t)param_in);
    	break;
    }
    return res;
}


static int Map_QSEE_CIPHER_PAD_ET(QSEE_CIPHER_PAD_ET param_in, DX_QSEE_CIPHER_PAD_ET *param_out)
{
    int res = 0;
    switch (param_in)
    {
    case QSEE_CIPHER_PAD_ISO10126:
        *param_out = DX_QSEE_CIPHER_PAD_ISO10126;
        break;

    case QSEE_CIPHER_PAD_PKCS7:
        *param_out = DX_QSEE_CIPHER_PAD_PKCS7;
        break;

    case QSEE_CIPHER_PAD_NO_PAD:
        *param_out = DX_QSEE_CIPHER_PAD_NO_PAD;
        break;

    default:
        res = -1;
    	QSEE_LOG(QSEE_LOG_MSG_ERROR, "%s failed : invalid PAD_ET %" PRIu32, __FUNCTION__, (uint32_t)param_in);
        break;
    }

    return res;
}


int dx_qsee_cipher_init(DX_QSEE_CIPHER_ALGO_ET alg,
                     dx_qsee_cipher_ctx  **cipher_ctx)
{
    QSEE_CIPHER_ALGO_ET alg_converted = QSEE_CIPHER_ALGO_AES_128;

    if (0 != Map_DX_QSEE_CIPHER_ALGO_ET(alg, &alg_converted))
    {
    	QSEE_LOG(QSEE_LOG_MSG_ERROR, "%s failed : Map_DX_QSEE_CIPHER_ALGO_ET failed!", __FUNCTION__);
        return -1;
    }

	return qsee_cipher_init(alg_converted, (qsee_cipher_ctx**)cipher_ctx);
}


int dx_qsee_cipher_free_ctx(dx_qsee_cipher_ctx *cipher_ctx)
{
	return qsee_cipher_free_ctx((dx_qsee_cipher_ctx*)cipher_ctx);
}


int dx_qsee_cipher_set_param(dx_qsee_cipher_ctx *cipher_ctx,
                          DX_QSEE_CIPHER_PARAM_ET param_id,
                          const void *param,
                          uint32_t param_len)
{
    QSEE_CIPHER_PARAM_ET param_id_converted = QSEE_CIPHER_PARAM_KEY;
    QSEE_CIPHER_MODE_ET mode_converted = QSEE_CIPHER_MODE_ECB;
    QSEE_CIPHER_PAD_ET pad_converted = QSEE_CIPHER_PAD_NO_PAD;

    if(!param)
    {
    	QSEE_LOG(QSEE_LOG_MSG_ERROR, "%s failed : bad args", __FUNCTION__);
    	return -1;
    }

    if (0 != Map_DX_QSEE_CIPHER_PARAM_ET(param_id, &param_id_converted))
    {
    	QSEE_LOG(QSEE_LOG_MSG_ERROR, "%s failed : Map_DX_QSEE_CIPHER_PARAM_ET failed!", __FUNCTION__);
        return -1;
    }

    if (DX_QSEE_CIPHER_PARAM_MODE == param_id)
    {
    	DX_QSEE_CIPHER_MODE_ET mode_in = DX_QSEE_CIPHER_MODE_ECB;
    	if(param_len != sizeof(DX_QSEE_CIPHER_MODE_ET))
    	{
        	QSEE_LOG(QSEE_LOG_MSG_ERROR,
        			"%s failed : param_len=%" PRIu32 " != sizeof(DX_QSEE_CIPHER_MODE_ET)=%zu",
        			__FUNCTION__,
					param_len,
					sizeof(DX_QSEE_CIPHER_MODE_ET));
    		return -1;
    	}

    	mode_in = *((DX_QSEE_CIPHER_MODE_ET*)param);

        if (0 != Map_DX_QSEE_CIPHER_MODE_ET(mode_in, &mode_converted))
        {
        	QSEE_LOG(QSEE_LOG_MSG_ERROR, "%s failed : Map_DX_QSEE_CIPHER_MODE_ET failed!", __FUNCTION__);
            return -1;
        }

    	param = &mode_converted;
		param_len = sizeof(mode_converted);
    }
    else if (DX_QSEE_CIPHER_PARAM_PAD == param_id)
    {
    	DX_QSEE_CIPHER_PAD_ET pad_in  = DX_QSEE_CIPHER_PAD_NO_PAD;
		if(param_len != sizeof(DX_QSEE_CIPHER_PAD_ET))
		{
        	QSEE_LOG(QSEE_LOG_MSG_ERROR,
        			"%s failed : param_len=%" PRIu32 " != sizeof(DX_QSEE_CIPHER_PAD_ET)=%zu",
        			__FUNCTION__,
					param_len,
					sizeof(DX_QSEE_CIPHER_PAD_ET));
			return -1;
		}

		pad_in = *((DX_QSEE_CIPHER_PAD_ET*)param);

		if (0 != Map_DX_QSEE_CIPHER_PAD_ET(pad_in, &pad_converted))
		{
        	QSEE_LOG(QSEE_LOG_MSG_ERROR, "%s failed : Map_DX_QSEE_CIPHER_PAD_ET failed!", __FUNCTION__);
			return -1;
		}

    	param = &pad_converted;
		param_len = sizeof(pad_converted);
    }

	return qsee_cipher_set_param((qsee_cipher_ctx *)cipher_ctx,
	                          param_id_converted,
	                          param,
	                          param_len);
}

int dx_qsee_cipher_get_param(const dx_qsee_cipher_ctx *cipher_ctx,
                          DX_QSEE_CIPHER_PARAM_ET param_id,
                          void *param,
                          uint32_t *param_len)
{
	int res;

    QSEE_CIPHER_PARAM_ET param_id_converted = QSEE_CIPHER_PARAM_KEY;

    if(!param || !param_len)
    {
    	QSEE_LOG(QSEE_LOG_MSG_ERROR, "%s failed : bad args", __FUNCTION__);
    	return -1;
    }

    if (0 != Map_DX_QSEE_CIPHER_PARAM_ET(param_id, &param_id_converted))
    {
    	QSEE_LOG(QSEE_LOG_MSG_ERROR, "%s failed : Map_DX_QSEE_CIPHER_PARAM_ET failed!", __FUNCTION__);
        return -1;
    }



    if (DX_QSEE_CIPHER_PARAM_MODE == param_id)
    {
        DX_QSEE_CIPHER_MODE_ET mode_converted = DX_QSEE_CIPHER_MODE_ECB;
    	QSEE_CIPHER_MODE_ET mode_in = QSEE_CIPHER_MODE_ECB;
    	uint32_t len_out = sizeof(QSEE_CIPHER_MODE_ET);

    	if(*param_len < sizeof(DX_QSEE_CIPHER_MODE_ET))
    	{
    		QSEE_LOG(QSEE_LOG_MSG_ERROR,
      			"%s failed : *param_len=%" PRIu32 " < sizeof(DX_QSEE_CIPHER_MODE_ET)=%zu",
      			__FUNCTION__,
				*param_len,
    			sizeof(DX_QSEE_CIPHER_MODE_ET));
    		return -1;
    	}

    	res = qsee_cipher_get_param((qsee_cipher_ctx*)cipher_ctx,
    	            param_id_converted,
    	            &mode_in,
    	            &len_out);
        if (0 != res)
        {
        	QSEE_LOG(QSEE_LOG_MSG_ERROR, "%s failed : qsee_cipher_get_param (MODE) failed!", __FUNCTION__);
            return res;
        }

        if (0 != Map_QSEE_CIPHER_MODE_ET(mode_in, &mode_converted))
        {
        	QSEE_LOG(QSEE_LOG_MSG_ERROR, "%s failed : Map_QSEE_CIPHER_MODE_ET failed!", __FUNCTION__);
            return -1;
        }


    	*((DX_QSEE_CIPHER_MODE_ET*)param) = mode_converted;
		*param_len = sizeof(DX_QSEE_CIPHER_MODE_ET);
    }
    else if (DX_QSEE_CIPHER_PARAM_PAD == param_id)
    {
    	QSEE_CIPHER_PAD_ET pad_in  = QSEE_CIPHER_PAD_NO_PAD;
    	DX_QSEE_CIPHER_PAD_ET pad_converted = DX_QSEE_CIPHER_PAD_NO_PAD;
    	uint32_t len_out = sizeof(QSEE_CIPHER_PAD_ET);

    	if(*param_len < sizeof(DX_QSEE_CIPHER_PAD_ET))
    	{
    		QSEE_LOG(QSEE_LOG_MSG_ERROR,
      			"%s failed : *param_len=%" PRIu32 " < sizeof(DX_QSEE_CIPHER_PAD_ET)=%zu",
      			__FUNCTION__,
				*param_len,
    			sizeof(DX_QSEE_CIPHER_PAD_ET));
    		return -1;
    	}

    	res = qsee_cipher_get_param((qsee_cipher_ctx*)cipher_ctx,
    	            param_id_converted,
    	            &pad_in,
    	            &len_out);
        if (0 != res)
        {
        	QSEE_LOG(QSEE_LOG_MSG_ERROR, "%s failed : qsee_cipher_get_param (PAD) failed!", __FUNCTION__);
            return res;
        }

        if (0 != Map_QSEE_CIPHER_PAD_ET(pad_in, &pad_converted))
        {
        	QSEE_LOG(QSEE_LOG_MSG_ERROR, "%s failed : Map_QSEE_CIPHER_PAD_ET failed!", __FUNCTION__);
            return -1;
        }


    	*((DX_QSEE_CIPHER_PAD_ET*)param) = pad_converted;
		*param_len = sizeof(DX_QSEE_CIPHER_PAD_ET);
    }
    else
    {
    	// getting buffer
    	res = qsee_cipher_get_param((qsee_cipher_ctx*)cipher_ctx,
	            param_id_converted,
	            param,
	            param_len);
    }

	return res;
}


int dx_qsee_cipher_encrypt(const dx_qsee_cipher_ctx *cipher_ctx,
                        const uint8_t *pt,
                        uint32_t pt_len,
                        uint8_t *ct,
                        uint32_t *ct_len)
{
	return qsee_cipher_encrypt((qsee_cipher_ctx *)cipher_ctx,
	                        pt,
	                        pt_len,
	                        ct,
	                        ct_len);

}

int dx_qsee_cipher_decrypt(const dx_qsee_cipher_ctx *cipher_ctx,
                        const uint8_t *ct,
                        uint32_t ct_len,
                        uint8_t* pt,
                        uint32_t *pt_len)
{
	return qsee_cipher_decrypt((qsee_cipher_ctx *)cipher_ctx,
            ct,
            ct_len,
            pt,
            pt_len);
}
