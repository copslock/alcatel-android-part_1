
#include <stddef.h>
#include <stdint.h>

#include "qsee_prng.h"
#include "arm_ta_qsee_prng.h"

uint32_t dx_qsee_prng_getdata(uint8_t *out, uint32_t out_len)
{
    return qsee_prng_getdata(out, out_len);
}
