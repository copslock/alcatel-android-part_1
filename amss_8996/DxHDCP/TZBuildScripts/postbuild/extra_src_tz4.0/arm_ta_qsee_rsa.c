#include <stddef.h>
#include <stdint.h>
#include "comdef.h"

#include "qsee_log.h"
#include "qsee_rsa.h"
#include "arm_ta_qsee_rsa.h"



static int Map_DX_QSEE_RSA_KEY_TYPE(DX_QSEE_RSA_KEY_TYPE param_in, QSEE_RSA_KEY_TYPE *param_out)
{
    int res = 0;

    if (param_out == 0)
        return -1;

    switch (param_in)
    {
    case DX_QSEE_RSA_KEY_PUBLIC:
        *param_out = QSEE_RSA_KEY_PUBLIC;
        break;

    case DX_QSEE_RSA_KEY_PRIVATE:
        *param_out = QSEE_RSA_KEY_PRIVATE;
        break;

    case DX_QSEE_RSA_KEY_PRIVATE_CRT:
        *param_out = QSEE_RSA_KEY_PRIVATE_CRT;
        break;

    case DX_QSEE_RSA_KEY_PRIVATE_PUBLIC:
        *param_out = QSEE_RSA_KEY_PRIVATE_PUBLIC;
        break;

    default:
        res = -1;
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "%s failed : invalid KEY_TYPE %" PRIu32, __FUNCTION__, (uint32_t)param_in);
        break;
    }
    return res;
}

static int Map_DX_QSEE_RSA_PADDING_TYPE(DX_QSEE_RSA_PADDING_TYPE param_in, QSEE_RSA_PADDING_TYPE *param_out)
{
    int res = 0;

    if (param_out == 0)
        return -1;

    switch (param_in)
    {
    case DX_QSEE_RSA_PAD_PKCS1_V1_5_SIG:
        *param_out = QSEE_RSA_PAD_PKCS1_V1_5_SIG;
        break;

    case DX_QSEE_RSA_PAD_PKCS1_V1_5_ENC:
        *param_out = QSEE_RSA_PAD_PKCS1_V1_5_ENC;
        break;

    case DX_QSEE_RSA_PAD_PKCS1_OAEP:
        *param_out = QSEE_RSA_PAD_PKCS1_OAEP;
        break;

    case DX_QSEE_RSA_PAD_PKCS1_PSS:
        *param_out = QSEE_RSA_PAD_PKCS1_PSS;
        break;

    default:
        res = -1;
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "%s failed : invalid PADDING_TYPE %" PRIu32, __FUNCTION__, (uint32_t)param_in);
        break;
    }
    return res;
}

static int Map_DX_QSEE_HASH_IDX(DX_QSEE_HASH_IDX param_in, QSEE_HASH_IDX *param_out)
{
    int res = 0;

    if (param_out == 0)
        return -1;

    switch (param_in)
    {
    case DX_QSEE_HASH_IDX_NULL:
        *param_out = QSEE_HASH_IDX_NULL;
        break;

    case DX_QSEE_HASH_IDX_SHA1:
        *param_out = QSEE_HASH_IDX_SHA1;
        break;

    case DX_QSEE_HASH_IDX_SHA256:
        *param_out = QSEE_HASH_IDX_SHA256;
        break;

    case DX_QSEE_HASH_IDX_SHA384:
        *param_out = QSEE_HASH_IDX_SHA384;
        break;

    case DX_QSEE_HASH_IDX_SHA512:
        *param_out = QSEE_HASH_IDX_SHA512;
        break;

    case DX_QSEE_HASH_IDX_MAX:
        *param_out = QSEE_HASH_IDX_MAX;
        break;
        
    default:
        res = -1;
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "%s failed : invalid HASH_IDX %" PRIu32, __FUNCTION__, (uint32_t)param_in);
        break;
    }
    return res;
}


static int Fill_RSA_key(DX_QSEE_RSA_KEY* param_in, QSEE_RSA_KEY* param_out)
{
	QSEE_RSA_KEY_TYPE tmp_key_type = QSEE_RSA_KEY_PUBLIC;


	if (0 != Map_DX_QSEE_RSA_KEY_TYPE(param_in->type, &tmp_key_type))
	{
		QSEE_LOG(QSEE_LOG_MSG_ERROR, "%s failed : Map_DX_QSEE_RSA_KEY_TYPE failed!", __FUNCTION__);
	    return -1;
	}

	param_out->type = tmp_key_type;
	param_out->e = (QSEE_S_BIGINT*)param_in->e;
	param_out->d = (QSEE_S_BIGINT*)param_in->d;
	param_out->N = (QSEE_S_BIGINT*)param_in->N;
	param_out->p = (QSEE_S_BIGINT*)param_in->p;
	param_out->q = (QSEE_S_BIGINT*)param_in->q;
	param_out->qP = (QSEE_S_BIGINT*)param_in->qP;
	param_out->dP = (QSEE_S_BIGINT*)param_in->dP;
	param_out->dQ = (QSEE_S_BIGINT*)param_in->dQ;

	return 0;
}


int dx_qsee_rsa_verify_signature
(
   DX_QSEE_RSA_KEY              *key, 
   DX_QSEE_RSA_PADDING_TYPE     padding_type, 
   void                      *padding_info,
   DX_QSEE_HASH_IDX             hashidx,
   unsigned char             *hash, 
   int                       hashlen,
   unsigned char             *sig, 
   int                       siglen
)
{
    QSEE_RSA_PADDING_TYPE padding_type_converted = QSEE_RSA_NO_PAD;
    QSEE_HASH_IDX hashidx_converted = QSEE_HASH_IDX_NULL;
    QSEE_RSA_KEY tmp_key = {0};

    if (0 != Fill_RSA_key(key, &tmp_key))
    {
    	QSEE_LOG(QSEE_LOG_MSG_ERROR, "%s failed : Fill_RSA_key failed!", __FUNCTION__);
        return -1;
    }

    if (0 != Map_DX_QSEE_RSA_PADDING_TYPE(padding_type, &padding_type_converted))
    {
    	QSEE_LOG(QSEE_LOG_MSG_ERROR, "%s failed : Map_DX_QSEE_RSA_PADDING_TYPE failed!", __FUNCTION__);
        return -1;
    }

    if (0 != Map_DX_QSEE_HASH_IDX(hashidx, &hashidx_converted))
    {
    	QSEE_LOG(QSEE_LOG_MSG_ERROR, "%s failed : Map_DX_QSEE_HASH_IDX failed!", __FUNCTION__);
    	return -1;
    }

    return qsee_rsa_verify_signature
    (
       &tmp_key,
       padding_type_converted,
       padding_info,
       hashidx_converted,
       hash,
       hashlen,
       sig,
       siglen
    );
}


int dx_qsee_rsa_encrypt
(
   DX_QSEE_RSA_KEY            *key, 
   DX_QSEE_RSA_PADDING_TYPE   padding_type, 
   void                    *padding_info,
   const unsigned char     *msg, 
   int                     msglen,
   unsigned char           *cipher,
   int                     *cipherlen
)
{
    QSEE_RSA_PADDING_TYPE padding_type_converted = QSEE_RSA_NO_PAD;
    QSEE_RSA_KEY tmp_key = {0};

    if (0 != Fill_RSA_key(key, &tmp_key))
    {
    	QSEE_LOG(QSEE_LOG_MSG_ERROR, "%s failed : Fill_RSA_key failed!", __FUNCTION__);
        return -1;
    }

    if (0 != Map_DX_QSEE_RSA_PADDING_TYPE(padding_type, &padding_type_converted))
    {
    	QSEE_LOG(QSEE_LOG_MSG_ERROR, "%s failed : Map_DX_QSEE_RSA_PADDING_TYPE failed!", __FUNCTION__);
        return -1;
    }

        
    return qsee_rsa_encrypt
    		(
    		   &tmp_key,
    		   padding_type_converted,
    		   padding_info,
    		   msg,
    		   msglen,
    		   cipher,
    		   cipherlen
    		   );
}


int dx_qsee_rsa_decrypt
(
   DX_QSEE_RSA_KEY              *key,
   DX_QSEE_RSA_PADDING_TYPE     padding_type,
   void                    *padding_info,
   unsigned char           *cipher,
   int                     cipherlen,
   unsigned char           *msg,
   int                     *msglen
)
{
    QSEE_RSA_PADDING_TYPE padding_type_converted = QSEE_RSA_NO_PAD;
    QSEE_RSA_KEY tmp_key = {0};

    if (0 != Fill_RSA_key(key, &tmp_key))
    {
    	QSEE_LOG(QSEE_LOG_MSG_ERROR, "%s failed : Fill_RSA_key failed!", __FUNCTION__);
        return -1;
    }


    if (0 != Map_DX_QSEE_RSA_PADDING_TYPE(padding_type, &padding_type_converted))
    {
    	QSEE_LOG(QSEE_LOG_MSG_ERROR, "%s failed : Map_DX_QSEE_RSA_PADDING_TYPE failed!", __FUNCTION__);
        return -1;
    }

    return qsee_rsa_decrypt
    		(
    		   &tmp_key,
    		   padding_type_converted,
    		   padding_info,
    		   cipher,
    		   cipherlen,
    		   msg,
    		   msglen
    		);
}


int dx_qsee_BIGINT_read_unsigned_bin
(
   DX_QSEE_BigInt * a, 
   const uint8_t * buf,
   uint32_t len
)
{
    return qsee_BIGINT_read_unsigned_bin(
    		   (QSEE_BigInt*) a,
    		   buf,
    		   len
    		);
}




