
#include <stddef.h>
#include <stdint.h>

#include "qsee_log.h"
#include "qsee_hash.h"
#include "arm_ta_qsee_hash.h"



static int Map_DX_QSEE_HASH_ALGO_ET(DX_QSEE_HASH_ALGO_ET param_in, QSEE_HASH_ALGO_ET *param_out)
{
    int res = 0;

    switch (param_in)
    {
    case DX_QSEE_HASH_NULL:
        *param_out = QSEE_HASH_NULL;
        break;

    case DX_QSEE_HASH_SHA1:
        *param_out = QSEE_HASH_SHA1;
        break;

    case DX_QSEE_HASH_SHA256:
        *param_out = QSEE_HASH_SHA256;
        break;

    default:
        res = -1;
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "%s failed : invalid HASH_ALGO_ET %" PRIu32, __FUNCTION__, (uint32_t)param_in);
        break;
    }
    return res;
}


int dx_qsee_hash(DX_QSEE_HASH_ALGO_ET alg,
              const uint8_t *msg,
              uint32_t msg_len,
              uint8_t *digest,
              uint32_t digest_len)
{
    QSEE_HASH_ALGO_ET alg_converted = QSEE_HASH_NULL;

    if (0 != Map_DX_QSEE_HASH_ALGO_ET(alg, &alg_converted))
    {
    	QSEE_LOG(QSEE_LOG_MSG_ERROR, "%s failed : Map_DX_QSEE_HASH_ALGO_ET failed!", __FUNCTION__);
        return -1;
    }

    return qsee_hash(
    		  alg_converted,
              msg,
              msg_len,
              digest,
              digest_len);
}


int dx_qsee_hash_init(DX_QSEE_HASH_ALGO_ET alg,
                   dx_qsee_hash_ctx **hash_ctx)
{
    QSEE_HASH_ALGO_ET alg_converted = QSEE_HASH_NULL;

    if (0 != Map_DX_QSEE_HASH_ALGO_ET(alg, &alg_converted))
    {
    	QSEE_LOG(QSEE_LOG_MSG_ERROR, "%s failed : Map_DX_QSEE_HASH_ALGO_ET failed!", __FUNCTION__);
        return -1;
    }

    return qsee_hash_init(alg_converted,
                   (qsee_hash_ctx**)hash_ctx);
}


int dx_qsee_hash_update(const dx_qsee_hash_ctx  *hash_ctx,
                     const uint8_t           *msg,
                     uint32_t                msg_len)
{
    return qsee_hash_update((qsee_hash_ctx*)hash_ctx,
                     msg,
                     msg_len);
}

int dx_qsee_hash_final(const dx_qsee_hash_ctx  *hash_ctx,
                    uint8_t                 *digest,
                    uint32_t                digest_len)
{
    return qsee_hash_final((qsee_hash_ctx*)hash_ctx,
                    digest,
                    digest_len);
}

int dx_qsee_hash_free_ctx(dx_qsee_hash_ctx *hash_ctx)
{
    return qsee_hash_free_ctx((qsee_hash_ctx*) hash_ctx);
}

