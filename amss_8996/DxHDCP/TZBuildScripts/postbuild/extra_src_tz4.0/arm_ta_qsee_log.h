#ifndef ARM_TA_QSEE_LOG_H
#define ARM_TA_QSEE_LOG_H

#include <stdint.h>
#include <stdarg.h>
#include <string.h>


/* Priority of debug log messages */


/* Flags to control logging.  Enable the flag which will allow
 * the logs with corresponding log level.  For e.g., if we set 
 * ENABLE_QSEE_LOG_MSG_ERROR to 1 then we will get only error logging */
#define DX_ENABLE_QSEE_LOG_MSG_LOW      0
#define DX_ENABLE_QSEE_LOG_MSG_MED      0
#define DX_ENABLE_QSEE_LOG_MSG_HIGH     0
#define DX_ENABLE_QSEE_LOG_MSG_ERROR    1
#define DX_ENABLE_QSEE_LOG_MSG_FATAL    1
#define DX_ENABLE_QSEE_LOG_MSG_DEBUG    1


/* Priority of debug log messages */
#define DX_QSEE_LOG_MSG_LOW      0
#define DX_QSEE_LOG_MSG_MED      1
#define DX_QSEE_LOG_MSG_HIGH     2
#define DX_QSEE_LOG_MSG_ERROR    3
#define DX_QSEE_LOG_MSG_FATAL    4
#define DX_QSEE_LOG_MSG_DEBUG    5


void dx_qsee_log(uint8_t pri, const char* fmt, ...);

#endif /*ARM_TA_QSEE_LOG_H*/

