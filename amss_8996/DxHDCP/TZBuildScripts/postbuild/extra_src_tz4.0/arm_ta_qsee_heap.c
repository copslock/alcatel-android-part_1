
#include <stddef.h>
#include <stdint.h>

#include "qsee_heap.h"
#include "arm_ta_qsee_heap.h"


void* dx_qsee_malloc(size_t size)
{
	return qsee_malloc(size);

}

void dx_qsee_free(void *ptr)
{
	qsee_free(ptr);
}

