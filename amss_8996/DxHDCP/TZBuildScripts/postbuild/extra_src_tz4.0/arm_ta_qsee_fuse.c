
#include <stddef.h>
#include <stdint.h>

#include "qsee_log.h"
#include "qsee_fuse.h"
#include "arm_ta_qsee_fuse.h"



static int Map_dx_qsee_sw_fuse(dx_qsee_sw_fuse_t param_in, qsee_sw_fuse_t *param_out)
{
    int res = 0;

    if (0 == param_out)
        return -1;

    switch (param_in)
    {
    case DX_QSEE_HLOS_IMG_TAMPER_FUSE:
        *param_out = QSEE_HLOS_IMG_TAMPER_FUSE;
        break;

    case DX_QSEE_WINSECAPP_LOADED_FUSE:
        *param_out = QSEE_WINSECAPP_LOADED_FUSE;
        break;

    case DX_QSEE_UEFISECAPP_LOADED_FUSE:
        *param_out = QSEE_UEFISECAPP_LOADED_FUSE;
        break;

    default:
        res = -1;
        QSEE_LOG(QSEE_LOG_MSG_ERROR, "%s failed : invalid sw_fuse_t %" PRIu32, __FUNCTION__, (uint32_t)param_in);
        break;
    }
    return res;
}

int dx_qsee_is_sw_fuse_blown(
      dx_qsee_sw_fuse_t    fuse_num,
      bool*          is_blown,
      uint32_t            is_blown_sz
    )
{
	qsee_sw_fuse_t fuse_num_converted = QSEE_NUM_SW_FUSES;

	if (0 != Map_dx_qsee_sw_fuse(fuse_num, &fuse_num_converted))
	{
		QSEE_LOG(QSEE_LOG_MSG_ERROR, "%s failed : Map_dx_qsee_sw_fuse failed!", __FUNCTION__);
	    return -1;
	}

    return qsee_is_sw_fuse_blown(
    	fuse_num_converted,
    	is_blown,
    	is_blown_sz
    );
}
