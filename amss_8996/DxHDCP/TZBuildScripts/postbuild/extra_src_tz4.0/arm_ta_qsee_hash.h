#ifndef ARM_TA_QSEE_HASH_H
#define ARM_TA_QSEE_HASH_H

/**
@file dx_qsee_hash.h
@brief Provide hash API wrappers
*/


/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include <stdint.h>

/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * -------------------------------------------------------------------------*/
#define DX_QSEE_SHA1_HASH_SZ    20
#define DX_QSEE_SHA256_HASH_SZ  32

/*----------------------------------------------------------------------------
 * Type Declarations
 * -------------------------------------------------------------------------*/
typedef void dx_qsee_hash_ctx;

/** Support for SHA1 and SHA256 for hash */
typedef enum
{
  DX_QSEE_HASH_NULL         = 1, ///< Do not perform any hashing
  DX_QSEE_HASH_SHA1         = 2,
  DX_QSEE_HASH_SHA256       = 3,
  DX_QSEE_HASH_INVALID      = 0x7FFFFFFF

} DX_QSEE_HASH_ALGO_ET;

/*----------------------------------------------------------------------------
 * Function Declarations and Documentation
 * -------------------------------------------------------------------------*/

/**
 * @brief  This function will create a message digest hash using the
 *         algorithm specified.
 *
 * @param[in] hash         The hash algorithm
 * @param[in] msg          The message to hash
 * @param[in] msg_len      The length of the message
 * @param[in,out] digest   The digest to store
 * @param[in] digest_len   Length of the output message digest hash
 *                         buffer in bytes. Must be 20 bytes for SHA1 and
 *                         32 bytes for SHA256
 *
 * @return 0 on success, negative on failure
 */
int dx_qsee_hash(DX_QSEE_HASH_ALGO_ET alg,
              const uint8_t *msg,
              uint32_t msg_len,
              uint8_t *digest,
              uint32_t digest_len);

/**
 * @brief Intialize a hash context for update and final functions.
 *
 * @param[in] alg        The algorithm standard to use
 * @param[out] hash_ctx  The hash context
 *
 * @return 0 on success, negative on failure
 *
 */
int dx_qsee_hash_init(DX_QSEE_HASH_ALGO_ET alg,
                   dx_qsee_hash_ctx **hash_ctx);

/**
 * @brief  This function will hash some data into the hash context
 *         structure, which Must have been initialized by
 *         qsee_hash_init().
 *
 * @param[in] hash_ctx    The hash context
 * @param[in] msg         Pointer to the msg to hash
 * @param[in] msg_len     Length of the msg to hash
 *
 * @return 0 on success, negative on failure
 *
 * @see qsee_hash_init
 */
int dx_qsee_hash_update(const dx_qsee_hash_ctx  *hash_ctx,
                     const uint8_t           *msg,
                     uint32_t                msg_len);

/**
 * @brief  Compute the digest hash value
 *
 * @param[in] hash_ctx     The hash context
 * @param[in] digest       Pointer to output message digest hash
 * @param[in] digest_len   Length of the output message digest hash
 *                         buffer in bytes. Must be 20 bytes for SHA1 and
 *                         32 bytes for SHA256
 *
 * @return 0 on success, negative on failure
 *
 * @see qsee_hash_init
 */
int dx_qsee_hash_final(const dx_qsee_hash_ctx  *hash_ctx,
                    uint8_t                 *digest,
                    uint32_t                digest_len);

/**
 * @brief Release all resources with a given hash context.
 *
 * @param[in] hash_ctx Hash context to be deleted
 *
 * @return 0 on success, negative on failure
 *
 */
int dx_qsee_hash_free_ctx(dx_qsee_hash_ctx *hash_ctx);

#endif /*ARM_TA_QSEE_HASH_H*/

