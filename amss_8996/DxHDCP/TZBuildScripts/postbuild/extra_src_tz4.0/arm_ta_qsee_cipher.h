#ifndef ARM_TA_QSEE_CIPHER_H
#define ARM_TA_QSEE_CIPHER_H

/**
@file qsee_cipher.h
@brief Provide cipher API wrappers
*/


/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include <stdint.h>

/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * -------------------------------------------------------------------------*/
#define DX_QSEE_AES128_IV_SIZE         16
#define DX_QSEE_AES128_KEY_SIZE        16
#define DX_QSEE_AES256_IV_SIZE         16
#define DX_QSEE_AES256_KEY_SIZE        32

/*----------------------------------------------------------------------------
 * Type Declarations
 * -------------------------------------------------------------------------*/
typedef void dx_qsee_cipher_ctx;

/** Cipher supported algorithms */
typedef enum
{
   DX_QSEE_CIPHER_ALGO_AES_128    = 0,
   DX_QSEE_CIPHER_ALGO_AES_256,
   DX_QSEE_CIPHER_ALGO_AES_INVALID = 0x7FFFFFFF,
} DX_QSEE_CIPHER_ALGO_ET;

typedef enum
{
  DX_QSEE_CIPHER_PARAM_KEY        = 0,
  DX_QSEE_CIPHER_PARAM_IV,
  DX_QSEE_CIPHER_PARAM_MODE,
  DX_QSEE_CIPHER_PARAM_PAD,
  DX_QSEE_CIPHER_PARAM_INVALID = 0x7FFFFFFF,
} DX_QSEE_CIPHER_PARAM_ET;

/** Supported modes of operation */
typedef enum
{
   DX_QSEE_CIPHER_MODE_ECB        = 0,
   DX_QSEE_CIPHER_MODE_CBC,
   DX_QSEE_CIPHER_MODE_CTR,
   DX_QSEE_CIPHER_MODE_INVALID = 0x7FFFFFFF
} DX_QSEE_CIPHER_MODE_ET;

typedef enum
{
  DX_QSEE_CIPHER_PAD_ISO10126,
  DX_QSEE_CIPHER_PAD_PKCS7,
  DX_QSEE_CIPHER_PAD_NO_PAD,
  DX_QSEE_CIPHER_PAD_INVALID = 0x7FFFFFFF

} DX_QSEE_CIPHER_PAD_ET;

#define DX_AES_BLOCK_SZ (0x10)

/*----------------------------------------------------------------------------
 * Function Declarations and Documentation
 * -------------------------------------------------------------------------*/

 /**
 * @brief Intialize a cipher context for encrypt/decrypt operation
 *
 * @param[in] alg  The algorithm standard to use
 * @param[out] cipher_ctx The cipher ctx
 *
 * @return 0 on success, negative on failure
 *
 */
int dx_qsee_cipher_init(DX_QSEE_CIPHER_ALGO_ET alg,
                     dx_qsee_cipher_ctx  **cipher_ctx);

/**
 * @brief Release all resources with a given cipher context.
 *
 * @param[in] cipher_ctx Cipher context to be deleted
 *
 * @return 0 on success, negative on failure
 *
 */
int dx_qsee_cipher_free_ctx(dx_qsee_cipher_ctx *cipher_ctx);

/**
 * @brief Modify the parameters for a given cipher operation.
 *
 * @param[in] cipher_ctx  Cipher context
 * @param[in] param_id    The parameter to modify
 * @param[in] param       The parameter value to set.
 * @param[in] param_len   The length of the param (in bytes).
 *
 * @return 0 on success,
 * negative on failure,
 * -E_NOT_SUPPORTED if an alogirthm or parameter is not currently supported.
 *
 */
int dx_qsee_cipher_set_param(dx_qsee_cipher_ctx *cipher_ctx,
                          DX_QSEE_CIPHER_PARAM_ET param_id,
                          const void *param,
                          uint32_t param_len);
/**
 * @brief Retrieve the parameters for a given cipher context.
 *
 * @param[in] cipher_ctx  Cipher context
 * @param[in] param_id    The parameter to retrieve
 * @param[in] param       The memory location to store the parameter.
 * @param[in] param_len   The length of the param (in bytes).
 *
 * @return 0 on success, negative on failure
 *
 */
int dx_qsee_cipher_get_param(const dx_qsee_cipher_ctx *cipher_ctx,
                          DX_QSEE_CIPHER_PARAM_ET param_id,
                          void *param,
                          uint32_t *param_len);

/**
 * @brief This function encrypts the passed plaintext message using
 *        the specified algorithm. The memory allocated for the
 *        ciphertext must be large enough to hold the plaintext
 *        equivalent. If the output buffer is not large enough to
 *        hold the encrypted results, an error is returned.
 *
 * @param[in] cipher_ctx         The cipher context to create
 * @param[in] pt                 The input plaintext buffer
 * @param[in] pt_len             The input plaintext buffer length (in bytes)
 * @param[in,out] ct             The output ciphertext buffer
 * @param[in,out] ct_len         The output ciphertext buffer length. This
 *                               is modified to the actual ct bytes written.
 *
 * @return E_SUCCESS if successful
 *         E_INVALID_ARG if not multiple of block length
 *         E_FAILURE otherwise
 */
int dx_qsee_cipher_encrypt(const dx_qsee_cipher_ctx *cipher_ctx,
                        const uint8_t *pt,
                        uint32_t pt_len,
                        uint8_t *ct,
                        uint32_t *ct_len);

/**
 * @brief This function decrypts the passed ciphertext message using
 *        the specified algorithm. The memory allocated for the
 *        plaintext must be large enough to hold the ciphertext
 *        equivalent. If the output buffer is not large enough to
 *        hold the decrypted results, an error is returned.
 *
 * @param[in] cipher_ctx         The cipher context to create
 * @param[in] ct                 The input ciphertext buffer
 * @param[in] ct_len             The input ciphertext buffer length (in bytes)
 * @param[in,out] pt             The output plaintext buffer
 * @param[in,out] pt_len         The output plaintext buffer length. This
 *                               is modified to the actual pt bytes written.
 *
 * @return E_SUCCESS if successful
 *         E_INVALID_ARG if not multiple of block length
 *         E_FAILURE otherwise
 */
int dx_qsee_cipher_decrypt(const dx_qsee_cipher_ctx *cipher_ctx,
                        const uint8_t *ct,
                        uint32_t ct_len,
                        uint8_t* pt,
                        uint32_t *pt_len);

#endif /*ARM_TA_QSEE_CIPHER_H*/

