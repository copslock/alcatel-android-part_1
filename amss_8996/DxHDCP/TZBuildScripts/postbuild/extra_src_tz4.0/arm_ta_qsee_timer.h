#ifndef ARM_TA_QSEE_TIMER_H
#define ARM_TA_QSEE_TIMER_H

/**
@file qsee_timer.h
@brief Provide API wrappers for timer functions
*/


/**
 * @brief Get up time from bootup in ms.
 *
 * @param[in] None
 *
 * @return the uptime in ms from system bootup.
 *
 */
unsigned long long dx_qsee_get_uptime(void);

#endif /*ARM_TA_QSEE_TIMER_H*/

