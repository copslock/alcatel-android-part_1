#ifndef ARM_TA_QSEE_CMAC_H
#define ARM_TA_QSEE_CMAC_H


/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include <stdint.h>

/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * -------------------------------------------------------------------------*/

#define DX_QSEE_CMAC_DIGEST_SIZE      16

/*----------------------------------------------------------------------------
 * Type Declarations
 * -------------------------------------------------------------------------*/

/** Supported CMAC algorithms   */
typedef enum
{
   DX_QSEE_CMAC_ALGO_AES_128          = 1,
   DX_QSEE_CMAC_ALGO_AES_256          = 2,
   DX_QSEE_CMAC_ALGO_AES_INVALID      = 0x7FFFFFFF
}  DX_QSEE_CMAC_ALGO_ET;

/*----------------------------------------------------------------------------
 * Function Declarations and Documentation
 * -------------------------------------------------------------------------*/

/**
 * @brief Create a cipher MAC per FIPS pub 198 using the specified hash algorithm.
 *
 * @param[in] msg             - Pointer to message to be authenticated
 * @param[in] msg_len         - Length of message in bytes
 * @param[in] key             - Pointer to input key to CMAC algorithm
 * @param[in] key_len         - Length of input key in bytes
 * @param[out] cmac_digest    - Pointer to cmac digest (memory provided by caller)
 * @param[in] cmac_len        - Length of CMAC in bytes. Allowed len 8 - 16 bytes
 *
 * @return 0 on success, negative on failure
*/
int dx_qsee_cmac(DX_QSEE_CMAC_ALGO_ET alg, const uint8_t *msg, uint32_t msg_len,
              const uint8_t *key, uint32_t key_len, uint8_t *cmac_digest,
              uint32_t cmac_len);

#endif /*ARM_TA_QSEE_CMAC_H*/

