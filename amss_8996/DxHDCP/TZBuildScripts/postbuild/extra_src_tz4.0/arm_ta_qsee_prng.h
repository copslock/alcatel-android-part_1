#ifndef ARM_TA_QSEE_PRNG_H
#define ARM_TA_QSEE_PRNG_H

/**
@file qsee_prng.h
@brief Provide prng API wrappers
*/


/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include <stdint.h>

/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * -------------------------------------------------------------------------*/
#define DX_QSEE_MAX_PRNG    512  /* Maximum number of PRNG bytes read */

/**
 * @brief Release all resources with a given prng context.
 *
 * @param[in] out     The output data buffer
 * @param[in] out_len The output data length. The out_len
 *                    must be at most DX_QSEE_MAX_PRNG bytes.
 *
 * @return number of bytes read
 *
 */
uint32_t dx_qsee_prng_getdata(uint8_t *out, uint32_t out_len);

#endif /*ARM_TA_QSEE_PRNG_H*/

