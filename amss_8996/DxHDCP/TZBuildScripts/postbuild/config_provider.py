# *************************************************
# Copyright (c) 2016  ARM Ltd (or its subsidiaries)
# *************************************************
import glob
import json
import logging
import os
import string
import xml.etree.cElementTree as eTree
from distutils.version import LooseVersion

import json_helper

SCRIPT_ROOT = os.path.dirname(os.path.abspath(__file__))
logger = logging.getLogger('ARM-TA-ConfigProvider')


class QcManifest(object):
    def __init__(self, name, revision):
        self.name = name
        self.revision = revision

    @classmethod
    def form_package(cls, pkg_root):
        with open(os.path.join(pkg_root, 'build/manifest.xml'), 'rt') as f:
            tree = eTree.parse(f)
            name = tree.find('image_tree/name').text
            revision = tree.find('image_tree/revision').text.upper()
        if not name or not revision:
            raise Exception('Malformed QC manifest - ' + os.path.join(pkg_root, 'build/manifest.xml'))
        return cls(name, revision)

    def __str__(self):
        return str(self.__dict__)


class Config(object):
    def __init__(self, build_id, config_file, qc_manifest):
        try:
            config_data = json_helper.from_file(config_file)
        except ValueError as config_parse_exception:
            raise Exception(
                'Malformed {} - {}'.format(os.path.join(SCRIPT_ROOT, 'arm_ta_cfg.json'), config_parse_exception)
            )

        resolved_config = Config.resolve_configuration(qc_manifest, config_data)
        self.target_name = resolved_config['sample_app_target_name']
        self.extra_sources = resolved_config.get('extra_sources', [])
        self.include_dirs = resolved_config.get('include_dirs', [])
        self.defines = resolved_config.get('defines', [])
        self.metadata = resolved_config.get('metadata', {})
        self.patch_file = resolved_config['patch_file']
        self.sample_app_target_name = resolved_config['sample_app_target_name']

        temp_qc_libs = resolved_config.get('extra_qc_libs', [])  # get libs from config

        # substitute build ID place holder
        resolved_temp_qc_libs = [
            string.Template(p).safe_substitute(build_id=build_id) for p in temp_qc_libs
            ]

        # make full paths by appending ${BUILD_ROOT} env variable available at build time
        self.extra_qc_libs = [os.path.join('${BUILD_ROOT}', p) for p in resolved_temp_qc_libs]

    @staticmethod
    def __handle_input_source_list(input_source_files):
        sources = []
        # handle relative paths by resolving them relative to script dir
        for src_file in input_source_files:
            src_file = src_file.encode('utf8')
            if '*' not in src_file:
                absolute_src_file = src_file if os.path.isabs(src_file) else os.path.join(SCRIPT_ROOT, src_file)
                if not os.path.isfile(absolute_src_file):
                    raise IOError('Extra Source file not found' + absolute_src_file)
                sources.append(absolute_src_file)
            else:  # we have a pattern - need to glob
                if os.path.isabs(os.path.dirname(src_file)):
                    absolute_pattern = src_file
                else:
                    absolute_pattern = os.path.join(SCRIPT_ROOT, src_file)

                found_files = glob.glob(absolute_pattern)
                if not found_files:
                    raise IOError('No Extra Source file found for pattern' + src_file)
                sources.extend(found_files)
        return sources

    @staticmethod
    def resolve_configuration(qc_manifest, config_provider):
        if qc_manifest.name not in config_provider:
            raise KeyError(
                'postbuild does not support {} TrustZone version - no configuration found'.format(qc_manifest.name)
            )
        resolved_cfg = config_provider[qc_manifest.name]
        revisions_provider = config_provider[qc_manifest.name].get('update_effective_from_revisions', {})
        revision_config_delta = revisions_provider.get(qc_manifest.revision, None)
        if revision_config_delta:
            resolved_cfg.update(revision_config_delta)
            logger.info('image revision config for %s - found', str(qc_manifest))
        else:
            manifest_revision = LooseVersion(qc_manifest.revision)
            compatible_revision = None
            for revision in sorted(revisions_provider.keys(), key=lambda rev: LooseVersion(rev.upper())):
                if manifest_revision > LooseVersion(revision.upper()):
                    revision_config_delta = revisions_provider[revision]
                    compatible_revision = revision
            if revision_config_delta:
                resolved_cfg.update(revision_config_delta)
                logger.info(
                    'image revision config for %s - using config for %s',
                    str(qc_manifest),
                    compatible_revision
                )
            else:
                logger.info('image revision config for %s - not found using DEFAULTS', str(qc_manifest))

        if not os.path.isabs(resolved_cfg['patch_file']):
            resolved_cfg['patch_file'] = os.path.join(SCRIPT_ROOT, resolved_cfg['patch_file'])
        if not os.path.isfile(resolved_cfg['patch_file']):
            raise IOError('Patch file not found' + resolved_cfg['patch_file'])

        if resolved_cfg.get('extra_sources', []):
            resolved_cfg['extra_sources'] = Config.__handle_input_source_list(resolved_cfg['extra_sources'])

        logger.debug('resolved config:' + json.dumps(resolved_cfg, indent=2))
        return resolved_cfg
