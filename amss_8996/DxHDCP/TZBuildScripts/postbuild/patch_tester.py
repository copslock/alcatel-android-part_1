#!/usr/bin/python
# *************************************************
# Copyright (c) 2016  ARM Ltd (or its subsidiaries)
# *************************************************
import argparse
import json
import logging
import os
import subprocess
import sys

from argparse_action_helper import StoreInputDir
from config_provider import QcManifest
from qc_src_tree_helper import find_tz_root, get_patch_file_strip_number

author__ = 'alexander.zilberkant@arm.com'
__version__ = '1.0.0'

SCRIPT_ROOT = os.path.dirname(os.path.abspath(__file__))
logger = logging.getLogger('ARM-TA')


def get_parser():
    parser = argparse.ArgumentParser(
        description='Postbuild for ARM TA library',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )

    qc_cfg_group = parser.add_argument_group('Qualcomm Source Configuration')

    qc_cfg_group.add_argument(
        'dir',
        action=StoreInputDir,
        metavar='DIR',
        help="specify directory containing Qualcomm's source tree"
    )
    return parser


def dry_run(pkg_root, patch_file):
    strip_num = get_patch_file_strip_number(patch_file)
    f_null = open(os.devnull, 'w')
    subprocess.check_call(
        [
            'patch',
            '--ignore-whitespace',
            '--dry-run',
            '--strip=' + str(strip_num),
            '--input=' + patch_file,
            '--quiet'

        ],
        stdout=f_null,
        stderr=subprocess.STDOUT,
        stdin=None,
        cwd=pkg_root
    )


def test_all_patch_files(source_dir):
    pkg_root = find_tz_root(source_dir)
    fitting_patches = []
    for patch_file in os.listdir(os.path.join(SCRIPT_ROOT, 'patches')):
        try:
            dry_run(pkg_root, os.path.join(SCRIPT_ROOT, 'patches', patch_file))
            logging.info('%s - PATCH OK', patch_file)
            fitting_patches.append(patch_file)
        except subprocess.CalledProcessError:
            logging.info('%s - PATCH FAIL', patch_file)
    manifest = QcManifest.form_package(pkg_root)
    return manifest.name, fitting_patches


def main(argv):
    parser = get_parser()
    args = parser.parse_args(argv)

    logging.basicConfig(
        level=logging.INFO,
        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    )
    logger.info('starting ARM-TA Postbuild tool v:%s', __version__)
    logger.debug('command line arguments: \n%s', json.dumps(args.__dict__, indent=2))

    test_all_patch_files(args.dir)


if __name__ == '__main__':
    main(sys.argv[1:])
