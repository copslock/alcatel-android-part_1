/***************************************************************************
*   Copyright 2015 (C) Discretix Technologies Ltd.
*
*   This software is protected by copyright, international
*   treaties and various patents. Any copy, reproduction or otherwise use of
*   this software must be authorized by Discretix in a license agreement and
*   include this Copyright Notice and any other notices specified
*   in the license agreement. Any redistribution in binary form must be
*   authorized in the license agreement and include this Copyright Notice
*   and any other notices specified in the license agreement and/or in
*   materials provided with the binary distribution.
*
*   Some software modules which may be used or linked, may be subject to
*   respective "open source" license agreement(s), and may be copyrighted by
*   their respective author(s), as detailed in such respective license
*   agreement(s); please refer to those license agreement(s), and to any
*   Copyright file(s) or Copyright notices included in them, for further
*   information with regard to copyright of third parties and other license
*   provisions.
****************************************************************************/

#ifndef _DX_HDCP_REPEATER_H
#define _DX_HDCP_REPEATER_H

/*! \file DX_Hdcp_Repeater.h
This module provides HDCP repeater services.
*/

#include "DX_Hdcp_Types.h"

#ifdef __cplusplus
extern "C"
{
#endif

/*! Inits repeater & default parameters values
\parameters:
    notifyCallbackFunction -	Callback function reference for events notification
\return listen status
*/
EXTERNAL_API uint32_t DX_HDCP_Rpt_Init(EventCbFunction notifyCallbackFunction);

/*! Opens repeater's transmission connection. 
\parameters:
    remoteIpAddress -	Remote IP address (4 bytes)
    ctrlPort -			Control port (for HDCP authentication messages)
    clientID -			Connection ID (value returned by function)
    timeout -			Connection timeout (in milli-seconds)
\return connection status
*/
EXTERNAL_API uint32_t DX_HDCP_Rpt_Connect(uint8_t *remoteIpAddress, uint32_t ctrlPort, DxClientID_t *clientID, uint32_t timeout);

/*! Registers repeater's transmission HDCP1.X connection. 
\parameters:
    ksvList              - List of downstream device KSV's
    numKsvs              - Number of entries in ksvList
    depth                - Downstream device depth
                           In more details: Number of connection levels below the HDCP1 connection, not including the HDCP1 connection itself.
                           For example, if our Converter has one HDCP1 Repeater connected to it, which is connected to one Receiver, this API should be called with depth parameter equals to 1.
                           In another case, if our Converter has only one HDCP1 Receiver connected to it, this API should be called with depth parameter equals to 0.
    max_cascade_exceeded - Downstream device max_cascade_exceeded indication
    max_devs_exceeded    - Downstream device max_devs_exceeded indication
\return Status
*/
EXTERNAL_API uint32_t DX_HDCP_Rpt_Update_HDCP1_Topology(DxHdcpReceiverId_t *ksvList, uint32_t numKsvs, uint32_t depth, bool max_cascade_exceeded, bool max_devs_exceeded);

/*! Opens repeater's connection (listens to transmitter's authentication request). 
\parameters:
    localIpAddr -		Local IP address (4 bytes)
    ctrlPort -			Control port (for HDCP authentication messages)
\return listen status
*/
EXTERNAL_API uint32_t DX_HDCP_Rpt_Listen(uint8_t *localIpAddr, uint32_t ctrlPort);

/*! Returns the repeater's upstream authentication status. 
\parameters:
    isConnected -		set to true if upstream is connected
    isAuthenticated -	set to true if upstream is authenticated
    isRepeater -		set to true if last authentication sent a REPEATER flag
\return GetStatus status
*/
EXTERNAL_API uint32_t DX_HDCP_Rpt_Get_Status(bool *isConnected, bool *isAuthenticated, bool *isRepeater);

/*! Returns the list of connected streams sent by the transmitter. 
\parameters:
    k -					input: size of array passed in streams argument
                        output: number of streams actually recorded
    streams -			per-stream data including the stream's ID & type
\return GetStatus status
*/
EXTERNAL_API uint32_t DX_HDCP_Rpt_Get_Streams(uint16_t *k, DxHdcpStreamInfo_t *streams);

/*! re-encrypts data to be transferred. 
\parameters:
    pesPrivateData -	PES private data. If pesPrivateData = NULL input encrypted data will be copied to the output decrypted data as-is
    msgIn -				Input encrypted data
    msgOut -			Output re-encrypted data (value returned by function)
    msgLen -			Input/Output data length
\return re-encryption status
*/
EXTERNAL_API uint32_t DX_HDCP_Rpt_ReEncrypt(const DxPesData_t pesPrivateData, uint64_t msgIn, uint64_t msgOut, uint32_t msgLen);

/*! re-encrypts data to be transferred.
\parameters:
pesPrivateData -	PES private data. If pesPrivateData = NULL input encrypted data will be copied to the output decrypted data as-is
msgIn -				Input encrypted data
msgOut -			Output decrypted data (value returned by function)
msgLen -			Input/Output data length
inputBufferType -	Input buffer type (virtual memory address or shared memory handle)
outputBufferType -	Output buffer type (virtual memory address or shared memory handle)
\return decryption status
*/
EXTERNAL_API uint32_t DX_HDCP_Rpt_ReEncrypt2(const DxPesData_t    pesPrivateData,
                                             uint64_t             msgIn,
                                             uint64_t             msgOut,
                                             uint32_t             msgLen,
                                             EDxHdcpBufferType    inputBufferType,
                                             EDxHdcpBufferType    outputBufferType);

/*! decrypts data to be transferred. 
\parameters:
    pesPrivateData -	PES private data. If pesPrivateData = NULL input encrypted data will be copied to the output decrypted data as-is
    msgIn -				Input encrypted data
    msgOut -			Output re-encrypted data (value returned by function)
    msgLen -			Input/Output data length
\return decryption status
*/
EXTERNAL_API uint32_t DX_HDCP_Rpt_Decrypt(const DxPesData_t pesPrivateData, uint64_t msgIn, uint64_t msgOut, uint32_t msgLen, EDxHdcpStreamType streamType);

/*! decrypts data to be transferred.
\parameters:
    pesPrivateData -	PES private data. If pesPrivateData = NULL input encrypted data will be copied to the output decrypted data as-is
    msgIn -				Input encrypted data
    msgOut -			Output decrypted data (value returned by function)
    msgLen -			Input/Output data length
    msgOutOffset -	    Offset related to the beginning of output data, in bytes
    EDxHdcpStreamType - Stream type (video/audio)
    inputBufferType -	Input buffer type (virtual memory address or shared memory handle)
    outputBufferType -	Output buffer type (virtual memory address or shared memory handle)
\return decryption status
*/
EXTERNAL_API uint32_t DX_HDCP_Rpt_Decrypt2( const DxPesData_t    pesPrivateData,
                                            uint64_t             msgIn,
                                            uint64_t             msgOut,
                                            uint32_t             msgLen,
                                            uint32_t             msgOutOffset,
                                            EDxHdcpStreamType    streamType,
                                            EDxHdcpBufferType    inputBufferType,
                                            EDxHdcpBufferType    outputBufferType);

/*! Closes repeater's downstream connection. 
\parameters:
    clientID -			Connection ID
\return connection status
*/
EXTERNAL_API uint32_t DX_HDCP_Rpt_Disconnect(DxClientID_t clientID);

/*! Closes repeater's session.
\return close session status
*/
EXTERNAL_API uint32_t DX_HDCP_Rpt_Close_Session();

/*! Closes repeater.
\return close status
*/
EXTERNAL_API uint32_t DX_HDCP_Rpt_Close();

/*! Set system parameter. This API is deprecated and should not be used.
\parameters:
paramID -			Parameter key
paramData -			Parameter value (returned by function)
\return set-parameter status
*/
EXTERNAL_API uint32_t DX_HDCP_Rpt_Set_Parameter(EDxHdcpConfigParam paramID, void *paramData);

/*! Get version
\return repeater's version
*/
EXTERNAL_API char *DX_HDCP_Rpt_Get_Version();

#ifdef  __cplusplus
}
#endif

#endif
