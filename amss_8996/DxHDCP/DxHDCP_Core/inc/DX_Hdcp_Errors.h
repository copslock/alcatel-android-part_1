/***************************************************************************
*   Copyright 2015 (C) Discretix Technologies Ltd.
*
*   This software is protected by copyright, international
*   treaties and various patents. Any copy, reproduction or otherwise use of
*   this software must be authorized by Discretix in a license agreement and
*   include this Copyright Notice and any other notices specified
*   in the license agreement. Any redistribution in binary form must be
*   authorized in the license agreement and include this Copyright Notice
*   and any other notices specified in the license agreement and/or in
*   materials provided with the binary distribution.
*
*   Some software modules which may be used or linked, may be subject to
*   respective "open source" license agreement(s), and may be copyrighted by
*   their respective author(s), as detailed in such respective license
*   agreement(s); please refer to those license agreement(s), and to any
*   Copyright file(s) or Copyright notices included in them, for further
*   information with regard to copyright of third parties and other license
*   provisions.
****************************************************************************/

#ifndef _DX_HDCP_ERRORS_H
#define _DX_HDCP_ERRORS_H

/*! \file DX_Hdcp_Errors.h
This module includes all HDCP error codes
*/
#define ERROR_CODE_BITS 24
#define	DX_SUBSYSTEM_HDCP_DEF 0x0C	// to sync with DX_VOS_Errors.h definition

typedef enum {
    DX_HDCP_UNEXPECTED_MESSAGE_ERROR = (DX_SUBSYSTEM_HDCP_DEF << ERROR_CODE_BITS),  // 0x0C000000
    DX_HDCP_RECEIVERS_NUM_OVERFLOW,                                                 // 0x0C000001
    DX_HDCP_SESSIONS_NUM_OVERFLOW,                                                  // 0x0C000002
    DX_HDCP_STREAMS_NUM_OVERFLOW,                                                   // 0x0C000003
    DX_HDCP_STREAM_COUNTER_OVERFLOW,                                                // 0x0C000004
    DX_HDCP_ILLEGAL_CONFIG_PARAM,                                                   // 0x0C000005
    DX_HDCP_ELEMENT_TYPE_MISMATCH,                                                  // 0x0C000006
    DX_HDCP_SET_CONFIG_PARAM_FORBIDDEN_BEFORE_INIT,                                 // 0x0C000007
    DX_HDCP_SESSIONS_NUMBER_MISMATCH,                                               // 0x0C000008
    DX_HDCP_SESSION_ALREADY_OPEN,                                                   // 0x0C000009
    DX_HDCP_SESSION_ALREADY_CLOSE,                                                  // 0x0C00000A
    DX_HDCP_CONNECTIONS_NUMBER_MISMATCH,                                            // 0x0C00000B
    DX_HDCP_CONNECTION_ALREADY_OPEN,                                                // 0x0C00000C
    DX_HDCP_CONNECTION_ALREADY_CLOSE,                                               // 0x0C00000D
    DX_HDCP_ONLY_SINGLE_TSMT_PER_RCV,                                               // 0x0C00000E
    DX_HDCP_CERT_SIGNATURE_VERIFICATION_FAILED,                                     // 0x0C00000F
    DX_HDCP_SRM_SIGNATURE_VERIFICATION_FAILED,                                      // 0x0C000010
    DX_HDCP_H_PRIM_TIMEOUT,                                                         // 0x0C000011
    DX_HDCP_PAIRING_INFO_TIMEOUT,                                                   // 0x0C000012
    DX_HDCP_WRONG_MESSAGE_LENGTH,                                                   // 0x0C000013
    DX_HDCP_CORRUPTED_RECEIVER_DATA,                                                // 0x0C000014
    DX_HDCP_CONNECTION_TIMEOUT,                                                     // 0x0C000015
    DX_HDCP_H_VERIFICATION_FAILED,                                                  // 0x0C000016
    DX_HDCP_LOCALITY_CHECK_FAILED,                                                  // 0x0C000017
    DX_HDCP_SRM_FILE_INEXIST,                                                       // 0x0C000018
    DX_HDCP_SRM_FILE_TOO_OLD,                                                       // 0x0C000019
    DX_HDCP_SRM_INVALID_FORMAT,                                                     // 0x0C00001A
    DX_HDCP_NON_ACTIVE_SESSION,                                                     // 0x0C00001B
    DX_HDCP_NON_AUTHENTICATED_CONNECTION,                                           // 0x0C00001C
    DX_HDCP_NON_AUTHORIZED_CONNECTION,                                              // 0x0C00001D
    DX_HDCP_SESSION_KEY_ENCRYPTION_FAILED,                                          // 0x0C00001E
    DX_HDCP_SESSION_KEY_DECRYPTION_FAILED,                                          // 0x0C00001F
    DX_HDCP_SESSION_UNDEFINED_SECURED_SERVICE,                                      // 0x0C000020
    DX_HDCP_INVALID_PES_PRIVATE_DATA_STRUCT,                                        // 0x0C000021
    DX_HDCP_SRM_REVOCATION_LIST_CONTAINS_THE_RECEIVER_ID,                           // 0x0C000022
    DX_HDCP_STREAM_ID_MISMATCH,                                                     // 0x0C000023
    DX_HDCP_INPUT_CTR_MISMATCH,                                                     // 0x0C000024
    DX_HDCP_UPSTREAM_PROPAGATION_FAILED,                                            // 0x0C000025
    DX_HDCP_UPSTREAM_PROPAGATION_SEQNUMV_ROLLOVER,                                  // 0x0C000026   - seq_num_V roll-over or not incremented by 1
    DX_HDCP_UPSTREAM_PROPAGATION_TYPE1_HDCP_CONFLICT,                               // 0x0C000027
    DX_HDCP_UPSTREAM_PROPAGATION_TOPOLOGY_EXCEEDED,                                 // 0x0C000028
    DX_HDCP_DOWNSTREAM_PROPAGATION_FAILED,                                          // 0x0C000029
    DX_HDCP_UPSTREAM_PROPAGATION_SEQNUMM_ROLLOVER,                                  // 0x0C00002A
    DX_HDCP_AUTHENTICATION_FAILURE,                                                 // 0x0C00002B
    DX_HDCP_PROPAGATION_FAILURE,                                                    // 0x0C00002C
    DX_HDCP_NO_ACTIVE_CONNECTION,                                                   // 0x0C00002D
    DX_HDCP_INVALID_MESSAGE_ID,                                                     // 0x0C00002E
    DX_HDCP_REVOKED_DEVICE,                                                         // 0x0C00002F
    DX_HDCP_ILLEGAL_VERSION,                                                        // 0x0C000030
    DX_HDCP_SHARED_MEMORY_ERROR,                                                    // 0x0C000031
    DX_HDCP_DEPRECATED_API,                                                         // 0x0C000032
    DX_HDCP_SESSION_ALREADY_INITIALIZED,                                            // 0x0C000033
    DX_HDCP_TIMER_ALREADY_SET,                                                      // 0x0C000034
    DX_HDCP_LC_TIMEOUT_FAILED,                                                      // 0x0C000035
    DX_HDCP_NO_REPEATER_SUPPORT,                                                    // 0x0C000036
    DX_HDCP_HLOS_TEE_MISMATCH,                                                      // 0x0C000037
    DX_HDCP_CONNECTION_ABORTED,                                                     // 0x0C000038
    DX_HDCP_CERTIFICATE_NOT_RECEIVED_WITHIN_ALLOCATED_TIME,                         // 0x0C000039
    DX_HDCP_2_2_DEVICE_NOT_SENDING_CERTIFICATE,                                     // 0x0C00003A
    DX_HDCP_MISSING_CONFIG_PARAM,                                                   // 0x0C00003B
    DX_HDCP_UPSTREAM_PROPAGATION_TOPOLOGY_DEV_EXCEEDED,                             // 0x0C00003C
    DX_HDCP_UPSTREAM_PROPAGATION_TOPOLOGY_CASCADE_EXCEEDED,                         // 0x0C00003D
    DX_HDCP_PARAMETERS_CONFLICT,                                                    // 0x0C00003E
    DX_HDCP_DECRYPTION_FOR_UNCONNECTED_REPEATER,                                    // 0x0C00003F
    DX_HDCP_SECURED_SERVICE_INIT_FAILED = DX_HDCP_UNEXPECTED_MESSAGE_ERROR + 0x100, // 0x0C000100
    DX_HDCP_SECURED_SERVICE_DEVICE_ROOTED,                                          // 0x0C000101
    DX_HDCP_SECURED_SERVICE_COMPUTE_KH_FAILED,                                      // 0x0C000102
    DX_HDCP_SECURED_SERVICE_COMPUTE_KP_FAILED,                                      // 0x0C000103
    DX_HDCP_SECURED_SERVICE_COMPUTE_KD_FAILED,                                      // 0x0C000104
    DX_HDCP_SECURED_SERVICE_COMPUTE_H_FAILED,                                       // 0x0C000105
    DX_HDCP_SECURED_SERVICE_DECRYPT_KM_WITH_KPRIV_FAILED,                           // 0x0C000106
    DX_HDCP_SECURED_SERVICE_DECRYPT_KM_WITH_KH_FAILED,                              // 0x0C000107
    DX_HDCP_SECURED_SERVICE_VERIFY_DATA_SIGNATURE_FAILED,                           // 0x0C000108
    DX_HDCP_SECURED_SERVICE_GET_ENCRYPTED_NEW_KM_WITH_KPUB_FAILED,                  // 0x0C000109
    DX_HDCP_SECURED_SERVICE_GET_ENCRYPTED_KM_WITH_KH_FAILED,                        // 0x0C00010A
    DX_HDCP_SECURED_SERVICE_COMPUTE_L_FAILED,                                       // 0x0C00010B
    DX_HDCP_SECURED_SERVICE_CIPHER_DATA_FAILED,                                     // 0x0C00010C
    DX_HDCP_SECURED_SERVICE_CIPHER_AUTHENTICATION_FAILED,                           // 0x0C00010D
    DX_HDCP_SECURED_SERVICE_COMPUTE_DKEY2_FAILED,                                   // 0x0C00010E
    DX_HDCP_SECURED_SERVICE_START_TIME_COUNT_FAILED,                                // 0x0C00010F
    DX_HDCP_SECURED_SERVICE_GET_STORED_KM_DATA_FAILED,                              // 0x0C000110
    DX_HDCP_SECURED_SERVICE_GET_EKH_FAILED = DX_HDCP_SECURED_SERVICE_GET_STORED_KM_DATA_FAILED, // 0x0C000110 - deprecated, use DX_HDCP_SECURED_SERVICE_GET_STORED_KM_DATA_FAILED instead
    DX_HDCP_SECURED_SERVICE_STORE_EKH_FAILED,                                       // 0x0C000111
    DX_HDCP_SECURED_SERVICE_GET_H_PRIM_STORED_KM_FAILED,                            // 0x0C000112
    DX_HDCP_SECURED_SERVICE_GET_H_PRIM_NO_STORED_KM_FAILED,                         // 0x0C000113
    DX_HDCP_SECURED_SERVICE_VERIFY_L_FAILED,                                        // 0x0C000114
    DX_HDCP_SECURED_SERVICE_COMPUTE_V_FAILED,                                       // 0x0C000115
    DX_HDCP_SECURED_SERVICE_VERIFY_V_FAILED,                                        // 0x0C000116
    DX_HDCP_SECURED_SERVICE_COMPUTE_M_FAILED,                                       // 0x0C000117
    DX_HDCP_SECURED_SERVICE_VERIFY_M_FAILED,                                        // 0x0C000118
    DX_HDCP_SECURED_SERVICE_CERTIFICATE_RETRIEVAL_FAILED,                           // 0x0C000119
    DX_HDCP_SECURED_SERVICE_PROVISIONING_FAILED,                                    // 0x0C00011A
    DX_HDCP_SECURED_SERVICE_PROVISIONING_VALIDATION_FAILED,                         // 0x0C00011B
    DX_HDCP_SECURED_SERVICE_PROVISIONING_STORE_FAILED,                              // 0x0C00011C
    DX_HDCP_SECURED_SERVICE_PROVISIONING_REMOVAL_FAILED,                            // 0x0C00011D
    DX_HDCP_SECURED_SERVICE_PROVISIONING_CEK_STORE_FAILED,                          // 0x0C00011E
    DX_HDCP_SECURED_SERVICE_PROVISIONING_CEK_REMOVAL_FAILED,                        // 0x0C00011F
    DX_HDCP_SECURED_SERVICE_PROVISIONING_DATA_STORE_FAILED,                         // 0x0C000120
    DX_HDCP_SECURED_SERVICE_PROVISIONING_DATA_REMOVAL_FAILED,                       // 0x0C000121
    DX_HDCP_SECURED_SERVICE_PROVISIONING_LC128_MISSING_FAILED,                      // 0x0C000122
    DX_HDCP_SECURED_SERVICE_PROVISIONING_CERTIFICATE_MISSING_FAILED,                // 0x0C000123
    DX_HDCP_SECURED_SERVICE_PROVISIONING_PRIVATE_KEY_MISSING_FAILED,                // 0x0C000124
    DX_HDCP_SECURED_SERVICE_DATA_SAVING_FAILED,                                     // 0x0C000125
    DX_HDCP_SECURED_SERVICE_SET_PARAMETER_FAILED,                                   // 0x0C000126
    DX_HDCP_SECURED_SERVICE_STATE_MACHINE_FAILED,                                   // 0x0C000127
    DX_HDCP_SECURED_SERVICE_FAILED,                                                 // 0x0C000128
    DX_HDCP_SECURED_SERVICE_CIPHER_UNSECURED_DATA_PATH,                             // 0x0C000129
    DX_HDCP_SECURED_SERVICE_CIPHER_SECURED_DATA_PATH,                               // 0x0C00012A
    DX_HDCP_SECURED_SERVICE_CHECK_TIME_COUNT_FAILED,                                // 0x0C00012B
    DX_HDCP_SECURED_SERVICE_HDMI_CONNECTION_NOT_PERMITTED,                          // 0x0C00012C
    DX_HDCP_SECURED_SERVICE_HDMI_CONNECTION_FAILURE,                                // 0x0C00012D
    DX_HDCP_SECURED_SERVICE_COPYING_SECURED_DATA,                                   // 0x0C00012E
    DX_HDCP_CIPHER_ILLEGAL_BUFFER_TYPE,                                             // 0x0C00012F
    DX_HDCP_REQUIRED_TO_REAUTHENTICATE_AS_VERSION_2_1,                              // 0x0C000130    //this error code is not used anymore, kept to prevent hard coded mistakes
    DX_HDCP_NOT_IMPELEMENTED,                                                       // 0x0C000131
    DX_HDCP_REENCRYPTION_NOT_SUPPORTED,	                                            // 0x0C000132
    DX_HDCP_STREAM_READ_CONTENT_TYPE_ERROR,                                         // 0x0C000133 
    DX_HDCP_STREAM_CONTENT_TYPE_CHANGED_ERROR,                                      // 0x0C000134
    DX_HDCP_BAD_PARAMETERS,                                                         // 0x0C000135
    DX_HDCP_UNAUTHORIZED_EXEC_TZ_SECTION,                                           // 0x0C000136
    DX_HDCP_LOCALITY_RETRY_REQUIRED,                                                // 0x0C000137
    DX_HDCP_CTR_OVERFLOW,                                                           // 0x0C000138
    DX_HDCP_INIT_RTX_FAILED,                                                        // 0x0C000139
    DX_HDCP_INIT_RIV_FAILED,                                                        // 0x0C00013A
    DX_HDCP_UNAUTHORIZED_TZ_SESSION_DEVICE_TYPE,                                    // 0x0C00013B
    DX_HDCP_CRITICAL_SECTION_ERROR,                                                 // 0x0C00013C
    DX_HDCP_SECURED_SERVICE_INVALID_PERSISTENT_AUTH_DATA_ERROR,                     // 0x0C00013D
    DX_HDCP_SECURED_SERVICE_DECRYPT_STREAM_CONTENT_TYPE_NOT_ALLOWED,                // 0x0C00013E
    DX_HDCP_SOCKET_ERROR,                                                           // 0x0C00013F
    DX_HDCP_PRODUCTION_KEYS_INVALID_HDCP_VERSION,                                   // 0x0C000140
    DX_HDCP_HDCP_VERSION_MISMATCH,                                                  // 0x0C000141
    DX_HDCP_CPPF_CALL_ERROR,                                                        // 0x0C000142
    DX_HDCP_CPPF_CIPHER_NOT_ALLOWED,                                                // 0x0C000143
    DX_HDCP_MULTIPLE_SESSIONS_NOT_ALLOWED,                                          // 0x0C000144
    DX_HDCP_SECURED_SERVICE_ERROR_LAST//Add new Discretix errors before this error!!!
} EDxHdcpErrors;


#define  IS_VALID_HDCP_ERROR_CODE(errCode) \
    ( (errCode == DX_SUCCESS) || \
      ( (errCode >= DX_HDCP_UNEXPECTED_MESSAGE_ERROR) && \
        (errCode < DX_HDCP_SECURED_SERVICE_ERROR_LAST) \
      ) \
    )

#endif
