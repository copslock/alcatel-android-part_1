/***************************************************************************
*   Copyright 2015 (C) Discretix Technologies Ltd.
*
*   This software is protected by copyright, international
*   treaties and various patents. Any copy, reproduction or otherwise use of
*   this software must be authorized by Discretix in a license agreement and
*   include this Copyright Notice and any other notices specified
*   in the license agreement. Any redistribution in binary form must be
*   authorized in the license agreement and include this Copyright Notice
*   and any other notices specified in the license agreement and/or in
*   materials provided with the binary distribution.
*
*   Some software modules which may be used or linked, may be subject to
*   respective "open source" license agreement(s), and may be copyrighted by
*   their respective author(s), as detailed in such respective license
*   agreement(s); please refer to those license agreement(s), and to any
*   Copyright file(s) or Copyright notices included in them, for further
*   information with regard to copyright of third parties and other license
*   provisions.
****************************************************************************/

#ifndef _DX_HDCP_TYPES_H
#define _DX_HDCP_TYPES_H


/*! \file DX_Hdcp_Types.h
This module includes all HDCP API definitions
*/

#include <stdint.h>
#include <stdbool.h>

#define DX_HDCP_RECEIVER_ID_LENGTH				5


#ifndef IGNORE_VISIBILITY

#if (defined _WIN32) || (defined __CYGWIN__)
  #ifdef BUILDING_DLL
    #ifdef __GNUC__
      #define EXTERNAL_API __attribute__ ((dllexport))
    #else
      #define EXTERNAL_API __declspec(dllexport)
    #endif
  #else
    #ifdef __GNUC__
      #define EXTERNAL_API __attribute__ ((dllimport))
    #else
      #define EXTERNAL_API __declspec(dllimport)
    #endif
  #endif
#else
  #if __GNUC__ >= 4
    #define EXTERNAL_API __attribute__ ((visibility ("default")))
  #else
    #define EXTERNAL_API 
  #endif
#endif
#else //IGNORE_VISIBILITY
#define EXTERNAL_API 
#endif

typedef enum {
    DX_HDCP_LC_RETRIES,
    DX_HDCP_LC_L_PRIME_TIMEOUT,
    DX_HDCP_VERSION,
    DX_HDCP_OVERALL_TIMEOUT,
    DX_HDCP_SFS_DIR,
    DX_HDCP_HLOS_DATA_DIR,
    DX_HDCP_LOGS_PATH,
    DX_HDCP_MULTI_RECEIVERS
} EDxHdcpConfigParam;

typedef enum {
    DX_HDCP_VERSION_UNKNOWN,
    DX_HDCP_VERSION_1_X,
    DX_HDCP_VERSION_2_0,
    DX_HDCP_VERSION_2_1,
    DX_HDCP_VERSION_2_2,
    DX_HDCP_VERSION_TYPE_INVALID = 0x7FFFFFFF
} EDxHdcpVersion;

typedef enum {
    DX_HDCP_TYPE_UNKNOWN,
    DX_HDCP_TYPE_RECEIVER,
    DX_HDCP_TYPE_REPEATER
} EDxHdcpElementType;

typedef enum {
    DX_HDCP_EVENT_UPSTREAM_OPEN,
    DX_HDCP_EVENT_UPSTREAM_CLOSE,
    DX_HDCP_EVENT_DOWNSTREAM_CLOSE,                         // Param1: connectionID (DxClientID_t)
    DX_HDCP_EVENT_UNAUTHENTICATED_CONNECTION,               // Param1: connectionID (DxClientID_t)
    DX_HDCP_EVENT_UNAUTHORIZED_CONNECTION,                  // Param1: connectionID (DxClientID_t)      Param2: streamID (DxStreamID_t)
    DX_HDCP_EVENT_REVOKED_CONNECTION,                       // Param1: connectionID (DxClientID_t)
    DX_HDCP_EVENT_TOPOLOGY_EXCEED,                          // Param1: connectionID (DxClientID_t)
    DX_HDCP_EVENT_REAUTHENTICATE_REQUEST,                   // Param1: connectionID (DxClientID_t)
    DX_HDCP_EVENT_PROPAGATION_FAILURE,                      // Param1: connectionID (DxClientID_t)
    DX_HDCP_EVENT_PROPAGATION_TIMEOUT,                      // Param1: connectionID (DxClientID_t)
    DX_HDCP_EVENT_PROPAGATION_FINISHED,                     // Param1: connectionID (DxClientID_t)
    DX_HDCP_EVENT_CIPHER_ENABLED,                           // Param1: connectionID (DxClientID_t)
    DX_HDCP_EVENT_CIPHER_DISABLED,                          // Param1: connectionID (DxClientID_t)
    DX_HDCP_EVENT_INTERNAL_ERROR,                           // Param1: error result (uint32_t)
    DX_HDCP_EVENT_LISTEN_READY,                             // Param1: connectionID (uint8_t)
    DX_HDCP_EVENT_DOWNSTREAM_PROPAGATION_FINISHED,          // Param1: connectionID (uint8_t)
    DX_HDCP_EVENT_UPSTREAM_PROPAGATION_SEQ_NUM_V_ROLLOVER,  // Param1: connectionID (uint8_t) - seq_num_V roll-over or not incremented by 1
    DX_HDCP_EVENT_LAST
} EDxHdcpEventType;

typedef enum {
    DX_HDCP_STREAM_TYPE_UNKNOWN,
    DX_HDCP_STREAM_TYPE_VIDEO,
    DX_HDCP_STREAM_TYPE_AUDIO,
    DX_HDCP_STREAM_TYPE_INVALID = 0x7FFFFFFF
} EDxHdcpStreamType;

typedef void (*EventCbFunction)(EDxHdcpEventType, void*, void*);

typedef struct {
    uint8_t bytes[DX_HDCP_RECEIVER_ID_LENGTH];
} DxHdcpReceiverId_t;

typedef struct {
    DxHdcpReceiverId_t rcvId;
    EDxHdcpElementType type;
    EDxHdcpVersion version;
} DxHdcpTopologyElementData_t;

typedef struct
{
    uint8_t                     devicesNum;     /* DEVICE_COUNT protocol value  */
    uint8_t                     depth;          /* DEPTH protocol value */
    DxHdcpTopologyElementData_t devices[32];

} DxHdcpTopologyData_t;

typedef struct {
    uint16_t contentStreamId;
    uint8_t contentType;
} DxHdcpStreamInfo_t;

typedef void* DxSessionID_t;
typedef void* DxClientID_t;
typedef void* DxStreamID_t;
typedef uint8_t DxPesData_t[16];

typedef enum {
    DX_HDCP_BUFFER_TYPE_VIRTUAL_POINTER = 1,
    DX_HDCP_BUFFER_TYPE_SHARED_MEMORY_HANDLE = 2,
    DX_HDCP_BUFFER_TYPE_INVALID = 0x7FFFFFFF
} EDxHdcpBufferType;

#endif

