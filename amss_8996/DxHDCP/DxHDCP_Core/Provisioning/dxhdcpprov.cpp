/***************************************************************************
*   Copyright 2015 (C) Discretix Technologies Ltd.
*
*   This software is protected by copyright, international
*   treaties and various patents. Any copy, reproduction or otherwise use of
*   this software must be authorized by Discretix in a license agreement and
*   include this Copyright Notice and any other notices specified
*   in the license agreement. Any redistribution in binary form must be
*   authorized in the license agreement and include this Copyright Notice
*   and any other notices specified in the license agreement and/or in
*   materials provided with the binary distribution.
*
*   Some software modules which may be used or linked, may be subject to
*   respective "open source" license agreement(s), and may be copyrighted by
*   their respective author(s), as detailed in such respective license
*   agreement(s); please refer to those license agreement(s), and to any
*   Copyright file(s) or Copyright notices included in them, for further
*   information with regard to copyright of third parties and other license
*   provisions.
****************************************************************************/

// main.cpp : Defines the entry point for the console application.
//
#include <string.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include "CE2_HASH.h"
#include <windows.h>
#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <iomanip>      // std::setw
#include "ProvisioningTool.h"

#include "VOS_API/DX_VOS_Mem.h"
#include "VOS_API/DX_VOS_String.h"

#ifndef WIN32
#include <unistd.h>
#define _tmain main
#define _TCHAR char
#define _T(A) A
#define _tfopen fopen
#define _tcslen strlen
#define _stprintf sprintf
#define _tcscpy strcpy
#define _tcstcat strcat
#define _tsystem system
#define _tstoi atoi
#define _tprintf printf
#define _tcscmp strcmp
#define _trename rename
#define _tunlink unlink
#else
#include <tchar.h>
#endif

#define GET_FROM_COMMAND_LINE_FAILURE 0
#define GET_FROM_COMMAND_LINE_SUCCESS 1
#define GET_FROM_COMMAND_LINE_HELP 2

typedef enum {
	DX_PROV_UNKNOWN_DEV_TYPE = 0,
	DX_PROV_TRANSMITTER_DEV_TYPE = 1,
	DX_PROV_RECEIVER_DEV_TYPE = 2
} DxProvDevType;

// Hdcp2 provisioning data file names
#define LC128_FNAME			"lc128.dat"
#define CERT_FNAME			"certificate.dat"
#define PK_D_MOD_P_1_FNAME	"private_key_d_mod_p-1.dat"
#define PK_D_MOD_Q_1_FNAME	"private_key_d_mod_q-1.dat"
#define PK_P_FNAME			"private_key_p.dat"
#define PK_Q_FNAME			"private_key_q.dat"
#define PK_Q_1_MOD_P_FNAME	"private_key_q-1_mod_p.dat"
#define ROOT_PUB_KEY_FNAME	"root_public_key.dat"

// Delivered file definitions
#define TSMT_HEADER_VALUE					0x00000001UL
#define TSMT_HEADER_SIZE					4
#define LC128_SIZE							16
#define LC128_SHA1_SIZE						20
#define ROOT_PUB_MODULUS_SIZE				384
#define ROOT_PUB_EXP_SIZE					1
#define ROOT_PUB_KEY_SHA1_SIZE				20
#define TSMT_MAX_DATAIN_SIZE				ROOT_PUB_MODULUS_SIZE+ROOT_PUB_EXP_SIZE
#define TSMT_MAX_SHA1_SIZE					LC128_SHA1_SIZE
#define TRS_DCP_FILE_SIZE					445
#define RCV_DCP_FILE_KEYS_BLOCK_SIZE		862
#define TSMT_KEY_SET_SIZE					441
#define LICENSED_CONSTANT_BLOCK				36
#define RCV_HEADER_SIZE						4
#define RCV_HEADER_VALUE					0x00000002UL
#define RCV_CERT_SIZE						522
#define RCV_DEVICE_PRIVATE_CRT_ELEMENT_SIZE	64
#define RCV_DEVICE_PRIVATE_CRT_KEY_SIZE		5*RCV_DEVICE_PRIVATE_CRT_ELEMENT_SIZE
#define RCV_PRIV_KEY_SHA1_SIZE				20

#define RCV_KEY_SET_SIZE					862
#define RCV_DATAIN_SIZE						RCV_CERT_SIZE+RCV_DEVICE_PRIVATE_CRT_KEY_SIZE

#define RCV_ID_SIZE							5

#define DX_BITS_IN_BYTE						8
#define MAX_NUM_OF_ITEMS_IN_DATA_ITEM		10

#define RCV_INVALID_MAX_ITEMS				0xFFFFFFFF

struct Options {
	bool verbose;
	DxProvDevType devType;

	_TCHAR* outputFileName;
	_TCHAR* hdcp2DevFileName;
	_TCHAR* oemIdFileName;
	_TCHAR* cekFileName;
	DxUint32 beginIndex;
	DxUint32 maxItems;

	Options() {
		verbose = false;
		devType = DX_PROV_UNKNOWN_DEV_TYPE;
		hdcp2DevFileName = NULL;
		oemIdFileName = NULL;
		cekFileName = NULL;
		outputFileName = new _TCHAR[_tcslen(_T("PM.out")) + 1];
		_tcscpy(outputFileName, _T("PM.out"));
		beginIndex = 0;
		maxItems = RCV_INVALID_MAX_ITEMS;
	}

	~Options(){
		delete [] outputFileName;
		delete [] cekFileName;
		delete [] oemIdFileName;
		delete [] hdcp2DevFileName;
	}

	int GetFromCommandLine(int argc, _TCHAR* argv[]) {
		for (int i = 1; i < argc; i++) {
			bool found = false;
			if (!_tcscmp(argv[i], _T("-help"))) {
				Help();
				found = true;
				return GET_FROM_COMMAND_LINE_HELP;
			}
			if (!_tcscmp(argv[i], _T("-v"))) {
				verbose = true;
				found = true;

			} else if (!_tcscmp(argv[i], _T("-cek")) && argc > i) {
				cekFileName = new _TCHAR[_tcslen(argv[i + 1]) + 1];
				_tcscpy(cekFileName, argv[i + 1]);
				found = true;
				i++;
			} else if (!_tcscmp(argv[i], _T("-oemid")) && argc > i) {
				oemIdFileName = new _TCHAR[_tcslen(argv[i + 1]) + 1];
				_tcscpy(oemIdFileName, argv[i + 1]);
				found = true;
				i++;
			} else if (!_tcscmp(argv[i], _T("-o")) && argc > i) {
				delete[] outputFileName;
				outputFileName = new _TCHAR[_tcslen(argv[i + 1]) + 1];
				_tcscpy(outputFileName, argv[i + 1]);
				found = true;
				i++;
			}else if (!_tcscmp(argv[i], _T("-hdcp2_data_file")) && argc > i){
				hdcp2DevFileName = new _TCHAR[_tcslen(argv[i + 1]) + 1];
				_tcscpy(hdcp2DevFileName, argv[i + 1]);
				found = true;
				i++;
			}else if (!_tcscmp(argv[i], _T("-tx")) && argc > i){
				devType = DX_PROV_TRANSMITTER_DEV_TYPE;
				found = true;
				//i++;
			}else if (!_tcscmp(argv[i], _T("-rx")) && argc > i){
				devType = DX_PROV_RECEIVER_DEV_TYPE;
				found = true;
				//i++;
			}else if (!_tcscmp(argv[i], _T("-begin_index")) && argc > i){
				beginIndex = _tcstoul(argv[i + 1], NULL, 10);
				found = true;
				i++;
			}else if (!_tcscmp(argv[i], _T("-max_items")) && argc > i){
				maxItems = _tcstoul(argv[i + 1], NULL, 10);
				found = true;
				i++;
			}
			
			if (found == false) {
				_tprintf(_T("\nError: unknown option %s\n"), argv[i]);
				return GET_FROM_COMMAND_LINE_FAILURE;
			}
		}
		return GET_FROM_COMMAND_LINE_SUCCESS;
	}

	bool Check() {
		bool status = true;
		if (hdcp2DevFileName == NULL) {
			printf("\nError: hdcp2_data_file not specified\n");
			status = false;
		}
		if (cekFileName == NULL) {
			printf("\nError: -cek file not specified\n");
			status = false;
		}
		if (oemIdFileName == NULL) {
			printf("\nError: oemid file not specified\n");
			status = false;
		}
		if (devType == DX_PROV_UNKNOWN_DEV_TYPE) {
			printf("\nError: device type (tx/rx) not specified\n");
			status = false;
		}
		// forbidding usage of beginIndex and/or maxItems flags with Transmitter
		if ( ( DX_PROV_TRANSMITTER_DEV_TYPE == devType ) &&
			 ( (beginIndex != 0) || (RCV_INVALID_MAX_ITEMS != maxItems) ) ) {
			printf("\nError: 'beginIndex' and 'maxItems' parameters are only valid for Receiver device type\n");
			status = false;
		}
		if (status == false)
			Help();
		return status;
	}
	void Help() {
		_tprintf(_T("Usage: dxhdcpprov <-v> <-help>\n")
		_T("\t<-hdcp2_data_file  hdcp2_data_file>\n")
		_T("\t[-begin_index      <receiver data file item index to begin processing from>]\n")
		_T("\t[-max_items        <maximum number of items to process from the receiver data file>]\n")
		_T("\t[-tx | -rx]        device type (Transmitter | Receiver) \n")
		_T("\t<-cek              cek_file_name>\n")
		_T("\t<-oemid            oemid_file_name>\n")
		_T("\t<-o                base_out_name>\n")
		_T("\n")
		);
	}
};

class PackagingTool{
	const Options& m_options;
	std::vector<dataTuple**> m_dataItemsVector; //vector of arrays, each array is a group tuples. each tuple repesents single provisioning file (lc128, cerfiticate, private key, etc.)
	DxUint8		m_cek[16];
	DxUint8		m_oem[16];
	DxUint32	m_numOfDataItems;

public:
	PackagingTool(const Options& options):m_options(options),m_numOfDataItems(0){

	}
	bool Init(){
		bool status = true;
		size_t readBytes;
		
		if (m_options.hdcp2DevFileName)\
		{
			status = parseHdcp2DevFile();
			if(!status)
				goto end;
		}

		FILE* oemIdFile = _tfopen(m_options.oemIdFileName,_T("rb"));
		if(oemIdFile == NULL)
		{
			status = false;
			perror("Failed opening oemId file");
			goto end;
		}
		readBytes = fread(m_oem,1,sizeof(m_oem),oemIdFile);
		if (readBytes != sizeof(m_oem) )
		{
			status = false;
			perror("Failed reading oemId file");
			goto end;
		}
		fclose(oemIdFile);
		FILE* cekKeyFile = _tfopen(m_options.cekFileName,_T("rb"));
		if(cekKeyFile == NULL)
		{
			status = false;
			perror("Failed opening cek file");
			goto end;
		}
		readBytes = fread(m_cek,1,sizeof(m_cek),cekKeyFile);
		if (readBytes != sizeof(m_cek) )
		{
			status = false;
			perror("Failed reading cek file");
			goto end;
		}
		fclose(cekKeyFile);

end:	
		return status;
	}
	~PackagingTool()
	{
		CleanVector();
	}

	bool RunCommand(){
		bool status;
		DxStatus  ret = DX_SUCCESS;
		const DxUint32 dataItemsVectorSize = m_dataItemsVector.size(); //numOfKeySets
		DxChar*	CharOutputFileName = new DxChar[_tcslen(m_options.outputFileName)+1];
		wcstombs_s(NULL,CharOutputFileName,_tcslen(m_options.outputFileName)+1,m_options.outputFileName,_TRUNCATE);
		std::string StringOutputFileName = CharOutputFileName; //store the Dxchar name as a string
		std::stringstream StreamForFullStringName; //for convert Index to string easier
		DxUint32 CalcDigit = dataItemsVectorSize; //copy the size to the calculate because the calculate variable is changing.
		DxUint32 NumOfDigits = 0; //number of digits in the dataItemsVectorSize, to do zero padding to the output filename

		while (CalcDigit != 0) { CalcDigit /= 10; NumOfDigits++; } //Calculate how many digits in the dataItemsVectorSize

		for (DxUint32 Index=0; Index < dataItemsVectorSize; ++Index )
		{
			// Basically (dataItemsVectorSize > 1) means this is a Receiver
			if ( dataItemsVectorSize > 1)
			{
				DxByte recieverID[5] = {0};
				DxBool foundReceiverID = DX_FALSE;

				// Getting the ReceiverID
				for (DxUint32 tupleIdx = 0 ; tupleIdx < m_numOfDataItems ; tupleIdx++)
				{
					const dataTuple *tuple = ((dataTuple**)m_dataItemsVector[Index])[tupleIdx];
					// Locate the CERT tuple
					if (0 == DX_VOS_MemCmp(CERT_FNAME, tuple->fileName, std::strlen(CERT_FNAME)) && RCV_CERT_SIZE == tuple->size)
					{
						DX_VOS_FastMemCpy(recieverID, tuple->data, RCV_ID_SIZE);
						foundReceiverID = DX_TRUE;
						break;
					}
				}

				StreamForFullStringName.str(""); //Clean the stream
				StringOutputFileName.clear(); //Clean the String
				//Index + 1 so the first number will be 1 and not 0
				//StreamForFullStringName << padding length << fill value << (Index+1) << "_" << CharOutputFileName;
				StreamForFullStringName << std::setw( NumOfDigits ) << std::setfill( '0' ) << (Index+1) << "_" ;
				if (foundReceiverID)
				{
					char sRecieverID[(2 * RCV_ID_SIZE) + 1] = {0};
					std::sprintf(sRecieverID, "%02x%02x%02x%02x%02x", 
						recieverID[0], recieverID[1], recieverID[2], recieverID[3], recieverID[4]);
					StreamForFullStringName << sRecieverID << "_" ;
				}
				StreamForFullStringName << CharOutputFileName;
				StringOutputFileName.insert(0,StreamForFullStringName.str());
			}

			ret = DxPackageData("dxhdcp2",m_oem,m_cek,(const dataTuple**)m_dataItemsVector[Index],m_numOfDataItems,(char*)StringOutputFileName.c_str());
			
			if (ret != DX_SUCCESS)
			{
				break;
			}
			else
			{
				printf("Output file %s Created\n",StringOutputFileName.c_str());
			}
		}
		
		status = (ret == DX_SUCCESS);
		delete[] CharOutputFileName;

		return status;
	}
private:
	bool parseHdcp2DevFile(void);
	bool parseHdcp2TsmtData(FILE *fin);
	bool parseHdcp2RcvData(FILE *fin);
	bool seekToHeader(FILE *fin, DxUint32 value);
	bool addLC128(FILE *fin, DxByte *dataBuffer, DxByte *hashBuffer, dataTuple*& dataTupleLC128);
	void CleanVector();
	void CleanDataItemsArr( dataTuple** DataItemsArr, DxUint32 NumOfItems );

static DxUint32 convertUint32ToNetworkValue(DxUint32 value);

};

// ------------- parse --------------

bool PackagingTool::parseHdcp2DevFile(void)
{
	FILE *fin;
	bool status;

	if(m_options.verbose)
		_tprintf(_T("Parsing: %s\n"), m_options.hdcp2DevFileName);
	fin = _tfopen(m_options.hdcp2DevFileName,_T("rb"));
	if(fin == NULL)
	{
		perror("Failed opening hdcp2Dev file");
		return false;
	}

	switch(m_options.devType)
	{
	case DX_PROV_RECEIVER_DEV_TYPE:
		status = parseHdcp2RcvData(fin);
		break;
	case DX_PROV_TRANSMITTER_DEV_TYPE:
		status = parseHdcp2TsmtData(fin);
		break;
	default:
		status = false;
	}

	fclose(fin);
	return status;
}

// ---------------- parse Tsmt ------------

bool PackagingTool::parseHdcp2TsmtData(FILE *fin)
{
	bool		status = false;
	dataTuple*	LC128tuple =(dataTuple*)DX_VOS_MemMalloc(sizeof(dataTuple));
	DxUint32	NumOfDataItems = 0;
	DxUint32	FileSize = 0;
	dataTuple** dataItems;
	DX_VOS_MemSet(LC128tuple, 0, sizeof(dataTuple));
	LC128tuple->data = (DxUint8*)DX_VOS_MemMalloc(LC128_SIZE);
	if(m_options.verbose)
		_tprintf(_T("Parsing Transmitter data...\n"));

	fseek(fin, 0, SEEK_END);
	FileSize = ftell(fin);
	rewind(fin);

	if(seekToHeader(fin, TSMT_HEADER_VALUE))
	{
		CE2_HASH_Result_t sha1Buffer;
		CE2Error_t ce2Error;
		size_t cnt;
		DxByte dataBuffer[TSMT_MAX_DATAIN_SIZE];
		DxByte hashBuffer[TSMT_MAX_SHA1_SIZE];
		dataItems = (dataTuple**)DX_VOS_MemMalloc(sizeof(dataTuple*)*MAX_NUM_OF_ITEMS_IN_DATA_ITEM);
		//printf("parseTsmt: header found\n");

		// process LC128 data
		if(!addLC128(fin, dataBuffer, hashBuffer, LC128tuple))
		{	
			status = false;
			goto end;
		}
		
		//insert lc128 into dataItems
		dataItems[NumOfDataItems] = (dataTuple*)DX_VOS_MemMalloc(sizeof(dataTuple));
		dataItems[NumOfDataItems]->data = (DxUint8*)DX_VOS_MemMalloc(LC128_SIZE);
		dataItems[NumOfDataItems]->size = LC128_SIZE;
		DX_VOS_MemSet(dataItems[NumOfDataItems]->fileName, 0, DX_FILENAME_LENGTH);
		dataTuple::CopyTuple(dataItems[NumOfDataItems], LC128tuple );
		NumOfDataItems++;
		
		// process Root Public Key data
		// Read Root Public Modulus and Exponent 
		cnt = fread(dataBuffer, 1, ROOT_PUB_MODULUS_SIZE+ROOT_PUB_EXP_SIZE, fin);
		if(cnt != ROOT_PUB_MODULUS_SIZE+ROOT_PUB_EXP_SIZE)
		{
			_tprintf(_T("Error: Failed reading Root Public Key data"));
			status = false;
			goto end;
		}
		// read sha1 data
		cnt = fread(hashBuffer, 1, ROOT_PUB_KEY_SHA1_SIZE, fin);
		if(cnt != ROOT_PUB_KEY_SHA1_SIZE)
		{
			_tprintf(_T("Error: Failed reading Root Public Key SHA1 data"));
			
			status = false;
			goto end;
		}

		// create  hash with the crypto engine api
		ce2Error = CE2_HASH(CE2_HASH_SHA1_mode, dataBuffer, ROOT_PUB_MODULUS_SIZE+ROOT_PUB_EXP_SIZE, sha1Buffer);
		if(ce2Error != CE2_OK)
		{
			_tprintf(_T("Error: Failed LLF_HASH lc128"));
			status = false;
			goto end;
		}

		// Compare the hash result with the read buffer
		if(DX_VOS_MemCmp(hashBuffer, sha1Buffer, ROOT_PUB_KEY_SHA1_SIZE) != 0)
		{
			_tprintf(_T("Error: Failed Root Public Key SHA1 validation"));
			status = false;
			goto end;
		}

		// create and fill the tuple
		dataItems[NumOfDataItems] = (dataTuple*)DX_VOS_MemMalloc(sizeof(dataTuple));
		dataItems[NumOfDataItems]->data = (DxUint8*)DX_VOS_MemMalloc(ROOT_PUB_MODULUS_SIZE+ROOT_PUB_EXP_SIZE);
		DX_VOS_MemSet(dataItems[NumOfDataItems]->fileName, 0, DX_FILENAME_LENGTH);
		DX_VOS_StrNCopy(dataItems[NumOfDataItems]->fileName, DX_FILENAME_LENGTH, ROOT_PUB_KEY_FNAME);
		DX_VOS_FastMemCpy(dataItems[NumOfDataItems]->data, dataBuffer, ROOT_PUB_MODULUS_SIZE+ROOT_PUB_EXP_SIZE);
		dataItems[NumOfDataItems]->size = ROOT_PUB_MODULUS_SIZE+ROOT_PUB_EXP_SIZE;

		m_dataItemsVector.push_back(dataItems);
		//m_numOfDataItems is sent to DxPackageData() as the number of items, 
		//currently the number of index is less one (array started from 0).
		m_numOfDataItems = NumOfDataItems + 1; 

		status = true;
		//printf("Root Public Key tuple added successfully\n");///
	}

	//Check if filesize is correct
	if ( FileSize != TRS_DCP_FILE_SIZE )
	{
		status = false;
	}

end:
	if(m_options.verbose)
	{
		if(status)
			_tprintf(_T("Parsing Transmitter data succeeded\n"));
		else
			_tprintf(_T("Parsing Transmitter data failed\n"));
	}
	
	if(status == false )
	{
		CleanDataItemsArr( dataItems, NumOfDataItems);
	}

	if ( LC128tuple != NULL )
	{
		DX_VOS_MemFree(LC128tuple->data);
		DX_VOS_MemFree(LC128tuple);
		LC128tuple = NULL;
	}
		
	return status;
}

// ---------------- parse Rcv ------------

bool PackagingTool::parseHdcp2RcvData(FILE *fin)
{
	bool		status = false;
	dataTuple*	LC128tuple = (dataTuple*)DX_VOS_MemMalloc(sizeof(dataTuple));
	DxUint32	NumOfDataItems = 0;
	DxUint32	FileSize = 0;
	dataTuple** dataItems;
	LC128tuple->data = (DxUint8*)DX_VOS_MemMalloc(LC128_SIZE);
	if(m_options.verbose)
		_tprintf(_T("Parsing Receiver data...\n"));

	fseek(fin, 0, SEEK_END);
	FileSize = ftell(fin);
	rewind(fin);

	if(seekToHeader(fin, RCV_HEADER_VALUE))
	{
		CE2_HASH_Result_t sha1Buffer;
		CE2Error_t ce2Error;
		size_t cnt;
		DxByte dataBuffer[RCV_DATAIN_SIZE];
		DxByte hashBuffer[RCV_PRIV_KEY_SHA1_SIZE];
		DxByte *p;
		size_t itemIndex = 0;
		size_t numItemsProcessed = 0;

		//printf("parseRcv: header found\n");///

		// process LC128 data
		if(!addLC128(fin, dataBuffer, hashBuffer, LC128tuple))
		{	
			status = false; 
			goto end;
		}

		while(!feof(fin)) //each iteration each receiver/converter key set
		{

			// validate and process the rest of data
			// read Rcv data
			cnt = fread(dataBuffer, 1, RCV_DATAIN_SIZE, fin);

			if(cnt == 0 && feof(fin))
			{
				// end of file reached
				//printf("End of file reached\n");
				break;
			}

			if(cnt != RCV_DATAIN_SIZE)
			{
				perror("Failed reading Rcv data");
				status = false;
				break;
			}

			// read hashed Rcv data
			cnt = fread(hashBuffer, 1, RCV_PRIV_KEY_SHA1_SIZE, fin);
			if(cnt != RCV_PRIV_KEY_SHA1_SIZE)
			{
				_tprintf(_T("Failed reading hashed Rcv data, index: %u"), itemIndex);
				status = false;
				break;
			}

			// Getting to the item index we need to process
			// Processing as many items as specified
			// Keeping the itemIndex counter correctness as it is used for final size verification
			if ((itemIndex < m_options.beginIndex) || (numItemsProcessed >= m_options.maxItems))
			{
				itemIndex++;
				continue;
			}
			
			dataItems = (dataTuple**)DX_VOS_MemMalloc(sizeof(dataTuple*)*MAX_NUM_OF_ITEMS_IN_DATA_ITEM);
			NumOfDataItems = 0;

			//Add lc128 to tuple 
			dataItems[NumOfDataItems] = (dataTuple*)DX_VOS_MemMalloc(sizeof(dataTuple));
			dataItems[NumOfDataItems]->data = (DxUint8*)DX_VOS_MemMalloc(LC128_SIZE);
			dataItems[NumOfDataItems]->size = LC128_SIZE;
			DX_VOS_MemSet(dataItems[NumOfDataItems]->fileName, 0, DX_FILENAME_LENGTH);
			dataTuple::CopyTuple(dataItems[NumOfDataItems], LC128tuple );
			NumOfDataItems++;

			// create  hash with the crypto engine api
			ce2Error = CE2_HASH(CE2_HASH_SHA1_mode, dataBuffer, RCV_DATAIN_SIZE, sha1Buffer);
			if(ce2Error != CE2_OK)
			{
				_tprintf(_T("Error: Failed LLF_HASH lc128"));
				status = false;
				break;
			}

			// Compare the hash result with the read buffer
			if(DX_VOS_MemCmp(hashBuffer, sha1Buffer, RCV_PRIV_KEY_SHA1_SIZE) != 0)
			{
				_tprintf(_T("Error: Failed Rcv Private Key SHA1 validation"));
				status = false;
				break;
			}

			// create and fill the tuple for certificate
			p = dataBuffer;

			// Add certificate
			dataItems[NumOfDataItems] = (dataTuple*)DX_VOS_MemMalloc(sizeof(dataTuple));
			dataItems[NumOfDataItems]->data = (DxUint8*)DX_VOS_MemMalloc(RCV_CERT_SIZE);
			DX_VOS_MemSet(dataItems[NumOfDataItems]->fileName, 0, DX_FILENAME_LENGTH);
			DX_VOS_StrNCopy(dataItems[NumOfDataItems]->fileName, DX_FILENAME_LENGTH, CERT_FNAME);
			DX_VOS_FastMemCpy(dataItems[NumOfDataItems]->data, p, RCV_CERT_SIZE);
			dataItems[NumOfDataItems]->size = RCV_CERT_SIZE;
			NumOfDataItems ++;

			p += RCV_CERT_SIZE;

			// Add Device Private CRT P
			dataItems[NumOfDataItems] = (dataTuple*)DX_VOS_MemMalloc(sizeof(dataTuple));
			dataItems[NumOfDataItems]->data =(DxUint8*) DX_VOS_MemMalloc(RCV_DEVICE_PRIVATE_CRT_ELEMENT_SIZE);
			DX_VOS_MemSet(dataItems[NumOfDataItems]->fileName, 0, DX_FILENAME_LENGTH);
			DX_VOS_StrNCopy(dataItems[NumOfDataItems]->fileName, DX_FILENAME_LENGTH, PK_P_FNAME);
			DX_VOS_FastMemCpy(dataItems[NumOfDataItems]->data, p, RCV_DEVICE_PRIVATE_CRT_ELEMENT_SIZE);
			dataItems[NumOfDataItems]->size = RCV_DEVICE_PRIVATE_CRT_ELEMENT_SIZE;
			NumOfDataItems ++;

			p += RCV_DEVICE_PRIVATE_CRT_ELEMENT_SIZE;

			// Add Device Private CRT Q
			dataItems[NumOfDataItems] = (dataTuple*)DX_VOS_MemMalloc(sizeof(dataTuple));
			dataItems[NumOfDataItems]->data = (DxUint8*)DX_VOS_MemMalloc(RCV_DEVICE_PRIVATE_CRT_ELEMENT_SIZE);
			DX_VOS_MemSet(dataItems[NumOfDataItems]->fileName, 0, DX_FILENAME_LENGTH);
			DX_VOS_StrNCopy(dataItems[NumOfDataItems]->fileName, DX_FILENAME_LENGTH, PK_Q_FNAME);
			DX_VOS_FastMemCpy(dataItems[NumOfDataItems]->data, p, RCV_DEVICE_PRIVATE_CRT_ELEMENT_SIZE);
			dataItems[NumOfDataItems]->size = RCV_DEVICE_PRIVATE_CRT_ELEMENT_SIZE;
			NumOfDataItems ++;

			p += RCV_DEVICE_PRIVATE_CRT_ELEMENT_SIZE;

			// Add Device Private CRT D mod (P-1)
			dataItems[NumOfDataItems] =(dataTuple*) DX_VOS_MemMalloc(sizeof(dataTuple));
			dataItems[NumOfDataItems]->data =(DxUint8*) DX_VOS_MemMalloc(RCV_DEVICE_PRIVATE_CRT_ELEMENT_SIZE);	
			DX_VOS_MemSet(dataItems[NumOfDataItems]->fileName, 0, DX_FILENAME_LENGTH);
			DX_VOS_StrNCopy(dataItems[NumOfDataItems]->fileName, DX_FILENAME_LENGTH, PK_D_MOD_P_1_FNAME);
			DX_VOS_FastMemCpy(dataItems[NumOfDataItems]->data, p, RCV_DEVICE_PRIVATE_CRT_ELEMENT_SIZE);
			dataItems[NumOfDataItems]->size = RCV_DEVICE_PRIVATE_CRT_ELEMENT_SIZE;
			NumOfDataItems ++;

			p += RCV_DEVICE_PRIVATE_CRT_ELEMENT_SIZE;

			// Add Device Private CRT D mod (Q-1)
			dataItems[NumOfDataItems] = (dataTuple*)DX_VOS_MemMalloc(sizeof(dataTuple));
			dataItems[NumOfDataItems]->data = (DxUint8*)DX_VOS_MemMalloc(RCV_DEVICE_PRIVATE_CRT_ELEMENT_SIZE);
			DX_VOS_MemSet(dataItems[NumOfDataItems]->fileName, 0, DX_FILENAME_LENGTH);
			DX_VOS_StrNCopy(dataItems[NumOfDataItems]->fileName, DX_FILENAME_LENGTH, PK_D_MOD_Q_1_FNAME);
			DX_VOS_FastMemCpy(dataItems[NumOfDataItems]->data, p, RCV_DEVICE_PRIVATE_CRT_ELEMENT_SIZE);
			dataItems[NumOfDataItems]->size = RCV_DEVICE_PRIVATE_CRT_ELEMENT_SIZE;
			NumOfDataItems ++;

			p += RCV_DEVICE_PRIVATE_CRT_ELEMENT_SIZE;

			// Add Device Private CRT a^-1 mod p
			dataItems[NumOfDataItems] =(dataTuple*)DX_VOS_MemMalloc(sizeof(dataTuple));
			dataItems[NumOfDataItems]->data = (DxUint8*)DX_VOS_MemMalloc(RCV_DEVICE_PRIVATE_CRT_ELEMENT_SIZE);
			DX_VOS_MemSet(dataItems[NumOfDataItems]->fileName, 0, DX_FILENAME_LENGTH);
			DX_VOS_StrNCopy(dataItems[NumOfDataItems]->fileName, DX_FILENAME_LENGTH, PK_Q_1_MOD_P_FNAME);
			DX_VOS_FastMemCpy(dataItems[NumOfDataItems]->data, p, RCV_DEVICE_PRIVATE_CRT_ELEMENT_SIZE);
			dataItems[NumOfDataItems]->size = RCV_DEVICE_PRIVATE_CRT_ELEMENT_SIZE;

			m_dataItemsVector.push_back(dataItems);
			//m_numOfDataItems is sent to DxPackageData() as the number of items, 
			//currently the number of index is less one (array started from 0).
			m_numOfDataItems = NumOfDataItems + 1;

			status = true;
			itemIndex++;
			numItemsProcessed++;
			//printf("Rcv Private Key set added successfully\n");///
		}
	}

	// filesize sanity check
	if ( ( FileSize < ( RCV_HEADER_SIZE + LICENSED_CONSTANT_BLOCK + RCV_DCP_FILE_KEYS_BLOCK_SIZE) ) ||      // If doesn't contain even one receiver keyset
		 ( 0 != ((FileSize - RCV_HEADER_SIZE - LICENSED_CONSTANT_BLOCK) % RCV_DCP_FILE_KEYS_BLOCK_SIZE) ) ) // If doesn't contain a multplication of receiver keysets
	{
		status = false;
	}

end:
	if(m_options.verbose)
	{
		if(status)
			_tprintf(_T("Parsing Receiver data succeeded\n"));
		else
			_tprintf(_T("Parsing Receiver data failed\n"));
	}

	if(status == false )
	{
		CleanDataItemsArr( dataItems, NumOfDataItems);
	}

	if ( LC128tuple != NULL )
	{
		DX_VOS_MemFree(LC128tuple->data);
		DX_VOS_MemFree(LC128tuple);
		LC128tuple = NULL;
	}

	return status;
}

// ---------------- seek to header ------------

bool PackagingTool::seekToHeader(FILE *fin, DxUint32 value)
// seek to the specified value in the file.
// if it's found, return true, otherwise, return false.
{
	DxUint32 header;
	size_t cnt;
	while(!feof(fin))
	{
		cnt = fread(&header, 1, sizeof(DxUint32), fin);
		if(cnt != sizeof(header))
			return false;
		header = convertUint32ToNetworkValue(header);
		//printf("header=%d\n",header);///
		if(header == value)
			return true;
	}
	return false;
}

// --------------------- add LC 128 ---------------------

bool PackagingTool::addLC128(FILE *fin, DxByte *dataBuffer, DxByte *hashBuffer, dataTuple*& dataTupleLC128)
// Check validity of LC128 contained in the file.
// If it's valid, add new data tuple and return true.
// otherwise, return false;
{
	CE2_HASH_Result_t sha1Buffer;
	CE2Error_t ce2Error;
	size_t cnt;
	bool status = false;

	// Read lc128 
	cnt = fread(dataBuffer, 1, LC128_SIZE, fin);
	if(cnt != LC128_SIZE)
	{
		perror("Failed reading lc128 data");
		goto end;
	}
	// read sha1 data
	cnt = fread(hashBuffer, 1, LC128_SHA1_SIZE, fin);
	if(cnt != LC128_SHA1_SIZE)
	{
		perror("Failed reading lc128 SHA1 data");
		goto end;
	}

	// create  hash with the crypto engine api
	ce2Error = CE2_HASH(CE2_HASH_SHA1_mode, dataBuffer, LC128_SIZE, sha1Buffer);
	if(ce2Error != CE2_OK)
	{
		_tprintf(_T("Error: Failed LLF_HASH lc128"));
		goto end;
	}

	// Compare the hash result with the read buffer
	if(DX_VOS_MemCmp(hashBuffer, sha1Buffer, LC128_SHA1_SIZE) != 0)
	{
		_tprintf(_T("Error: Failed lc128 SHA1 validation"));
		goto end;
	}

	// create and fill the tuple
	DX_VOS_FastMemCpy( dataTupleLC128->fileName, LC128_FNAME, sizeof(LC128_FNAME) );
	DX_VOS_FastMemCpy( dataTupleLC128->data, dataBuffer, LC128_SIZE );
	dataTupleLC128->size = LC128_SIZE;

	//printf("lc128 tuple added successfully\n");///
	status = true;
end:
	return status;
}

// --------------------- Clean Vector ---------------------

void PackagingTool::CleanVector()
{
	DxUint32 VectorSize;

	VectorSize = m_dataItemsVector.size();

	for(DxUint32 j=0; j < VectorSize; ++j)
	{
		CleanDataItemsArr( m_dataItemsVector[j], m_numOfDataItems );
	}
	
	m_dataItemsVector.clear();
}

// --------------------- Clean DataItems Arr ---------------------

void PackagingTool::CleanDataItemsArr( dataTuple** DataItemsArr, DxUint32 NumOfItems )
{
	
	if ( DataItemsArr != NULL )
	{
		for( DxUint32 i=0; (i < NumOfItems) && (DataItemsArr[i] != NULL); ++i)
		{
			DX_VOS_MemFree(DataItemsArr[i]->data);
			DX_VOS_MemFree(DataItemsArr[i]);
			DataItemsArr[i] = NULL;
		}
		DX_VOS_MemFree(DataItemsArr);
		DataItemsArr = NULL;
	}
}

// --------------------- convert to network value ---------------------

DxUint32 PackagingTool::convertUint32ToNetworkValue(DxUint32 value)
{
	DxUint32 newValue = 0;
	DxUint i = 0;
	for (i = 0; i < sizeof(DxUint32); ++i)
	{
		newValue = (newValue << DX_BITS_IN_BYTE) + (value & 0xFF);
		value >>= DX_BITS_IN_BYTE;
	}
	return newValue;
}

//###################################################################################
//###################################################################################
//###################################################################################
int _tmain(int argc, _TCHAR* argv[]) {

	bool success = true;
	int commandLineRes;

	Options options;
	PackagingTool packagingTool(options);

	commandLineRes = options.GetFromCommandLine(argc, argv);
	if(commandLineRes == GET_FROM_COMMAND_LINE_FAILURE)
	{
		_tprintf(_T("Conversion of provisioning data failed. Can't create output file: %s due to incorrect arguments\n"), options.outputFileName);
		return -1;
	}
	
	if(commandLineRes == GET_FROM_COMMAND_LINE_HELP)
	{
		return 0;
	}

	success = options.Check();
	if(!success)
	{
		_tprintf(_T("Conversion of provisioning data failed. Can't create output file: %s due to missing arguments\n"), options.outputFileName);
		return -1;
	}

	success = packagingTool.Init();
	if(!success)
	{
		_tprintf(_T("Conversion of provisioning data failed. Can't create output file: %s due to incorrect input data\n"), options.outputFileName);
		return -1;
	}

	success = packagingTool.RunCommand();
	if(success)
		_tprintf(_T("Conversion of provisioning data succeeded. All Output file/s created\n"), options.outputFileName);
	else
		_tprintf(_T("Conversion of provisioning data failed. Can't create output file: %s due to incorrect input data\n"), options.outputFileName);


	return success ? 0 : -1;
}

