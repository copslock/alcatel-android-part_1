/***************************************************************************
*   Copyright 2015 (C) Discretix Technologies Ltd.
*
*   This software is protected by copyright, international
*   treaties and various patents. Any copy, reproduction or otherwise use of
*   this software must be authorized by Discretix in a license agreement and
*   include this Copyright Notice and any other notices specified
*   in the license agreement. Any redistribution in binary form must be
*   authorized in the license agreement and include this Copyright Notice
*   and any other notices specified in the license agreement and/or in
*   materials provided with the binary distribution.
*
*   Some software modules which may be used or linked, may be subject to
*   respective "open source" license agreement(s), and may be copyrighted by
*   their respective author(s), as detailed in such respective license
*   agreement(s); please refer to those license agreement(s), and to any
*   Copyright file(s) or Copyright notices included in them, for further
*   information with regard to copyright of third parties and other license
*   provisions.
****************************************************************************/


#include "Dx_Hdcp_Provisioning.h"
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>


int main()
{
//	const char*	cekFile = "/data/tmp/CEK.txt";
//	const char*	dataFile = "/data/tmp/PM.out";
//	const char*	cekFile = "C:\\HDCP\\HDCP2_Cek.dat";
	//const char*	dataFile = "C:\\HDCP\\HDCP2_Tsmt_Tst.dat";
//	const char*	dataFile = "C:\\HDCP\\HDCP2_Rcv.dat";
	const char*	cekFile = "HDCP2_Cek.dat";
	const char*	dataFile = "HDCP2_Tsmt_Tst.dat";

	FILE*	tmpFile = NULL;
	char	cekDat[16];
	char*	provDat = NULL;
	off_t	dataFileSize = 0;
	unsigned  ret = 0;
	int bytesRead = 0;
	struct stat statBuf;
	ret = stat(dataFile,&statBuf);
	if ( ret != 0 )
		goto end;
		
	if ( ret != 0 )
		goto end;
	dataFileSize = statBuf.st_size;
	provDat = malloc(dataFileSize);
	if ( provDat == 0 )
		goto end;
	tmpFile = fopen(dataFile,"rb");
	if ( tmpFile == 0 )
		goto end;
	
	bytesRead = fread(provDat,1,dataFileSize,tmpFile);
	fclose(tmpFile);
	if ( bytesRead != dataFileSize)
		goto end;
	
	tmpFile = fopen(cekFile,"rb");
	if ( tmpFile == 0 )
		goto end;		
	
	bytesRead = fread(cekDat,1,sizeof(cekDat),tmpFile);
	fclose(tmpFile);
	if ( bytesRead != sizeof(cekDat))
		goto end;	
	ret = DxHdcp_Provisioning_Init();
	if (ret != 0)
	{
		printf("DxHdcp_Provisioning_Init failed\n");
		goto end;
	}
	
	ret = DxHDCP_ProvisionWithCEK(provDat,dataFileSize,cekDat);	
	if (ret != 0)
	{
		printf("DxHdcp_ProvisionWithCEK failed\n");
		goto end;
	}

	ret = DxHDCP_StoreCEK(cekDat);	
	if (ret != 0)
	{
		printf("DxHdcp_ProvisionWithCEK failed\n");
		goto end;
	}

	ret = DxHDCP_ProvisionValidate(provDat,dataFileSize,cekDat);
	if (ret != 0)
	{
		printf("DxHdcp_ProvisionValidate failed\n");
		goto end;
	}


end:
	DxHdcp_Provisioining_Terminate();
	if (provDat)
		free (provDat);
	printf("provisioning test finished with return code: 0x%x\n",ret);
	return ret;
}
