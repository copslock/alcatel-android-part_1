
/*
WARNING: This file is auto-generated.

Generated at:    Tue Jul 28 14:26:27 2015
Generated using: C:\Dropbox\FBRxAG\fbrx_autogen.pl v1.0.0
Generated from:  v1.0.8 of Thor_FBRxLM_Register_Settings.xlsx
*/

/*=============================================================================

          F B R X    A U T O G E N    F I L E

GENERAL DESCRIPTION
  This file is auto-generated and it captures the uK parameter settings for 
  different modes of FBRx operation provided by rflm_fbrx_settings

Copyright (c) 2009, 2010, 2011, 2012, 2013, 2014, 2015 by Qualcomm Technologies, Inc.  All Rights Reserved.

$DateTime: 2016/03/28 23:06:38 $
$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/rflm/api/fw/rflm_fbrx_typedef_ag.h#1 $

=============================================================================*/

/*=============================================================================
                           REVISION HISTORY
Version    Author   Date   
         Comments   IR Number   Other POC (Comma Delimited)   Release Validation Notes   Dependencies   AG File Location   
1.0.8   Robert Wilson   7/27/2015   
         Reduce WCDMA capture lengths, NUM_TX_IN_SAMPLES_C0 = 480, NUM_TX_IN_SAMPLES_C1 = 480, TX_DATA_LEN = 433, NUM_RX_IN_SAMPLES = 400, NUM_RX_SAMPLES = 433       rwilson@qti.qualcomm.com, christos@qti.qualcomm.com   Recommended and validated by FBRx team    None   https://sharepoint.qualcomm.com/qct/Modem-Tech/Projects/Thor/Systems/Forms/Systems.aspx?RootFolder=0.000000qct0.000000Modem 0Tech0.000000Projects0.000000Thor0.000000Systems0.000000Common0.000000FBRx0.000000FBRxLM%20Spreadsheet   
1.0.7   Christos Komminakis, Ankit Agarwal   7/10/2015   
         1. Removed reserved fields       christos@qti.qualcomm.com,  ankitaga@qti.qualcomm.com   Recommended and validated by FBRx team    None   https://sharepoint.qualcomm.com/qct/Modem-Tech/Projects/Thor/Systems/Forms/Systems.aspx?RootFolder=0.000000qct0.000000Modem 0Tech0.000000Projects0.000000Thor0.000000Systems0.000000Common0.000000FBRx0.000000FBRxLM%20Spreadsheet   
1.0.7   Christos Komminakis, Ankit Agarwal   7/10/2015   
         1. Add VSRC_TO_OUTPUT_RATIO to static params 2. Add int32 reserved fields in static and dynamic parameters for future use to remove CFW interface change dependencies.       christos@qti.qualcomm.com,  ankitaga@qti.qualcomm.com   Recommended and validated by FBRx team    None   https://sharepoint.qualcomm.com/qct/Modem-Tech/Projects/Thor/Systems/Forms/Systems.aspx?RootFolder=0.000000qct0.000000Modem 0Tech0.000000Projects0.000000Thor0.000000Systems0.000000Common0.000000FBRx0.000000FBRxLM%20Spreadsheet   
1.0.6   Christos Komminakis, Ankit Agarwal   7/9/2015   
         1. Add VSWR_RATIO_MODELA (boolean) to static params       christos@qti.qualcomm.com,  ankitaga@qti.qualcomm.com   Recommended and validated by FBRx team    None   https://sharepoint.qualcomm.com/qct/Modem-Tech/Projects/Thor/Systems/Forms/Systems.aspx?RootFolder=0.000000qct0.000000Modem 0Tech0.000000Projects0.000000Thor0.000000Systems0.000000Common0.000000FBRx0.000000FBRxLM%20Spreadsheet   
1.0.5   Christos Komminakis, Ankit Agarwal   7/7/2015   
         1. Added STAGE3_EN (boolean) to power dependent params, so that we can do Stage-1 for very high carrier imbalance       christos@qti.qualcomm.com,  ankitaga@qti.qualcomm.com   Recommended and validated by FBRx team    None   https://sharepoint.qualcomm.com/qct/Modem-Tech/Projects/Thor/Systems/Forms/Systems.aspx?RootFolder=0.000000qct0.000000Modem 0Tech0.000000Projects0.000000Thor0.000000Systems0.000000Common0.000000FBRx0.000000FBRxLM%20Spreadsheet   
1.0.4   Christos Komminakis, Ankit Agarwal   6/26/2015   
         1. Added meas_req_type enum 2. Moved estimate_tau_en and txfe_update to powe dependent params 3.  Added input_capture_id and meas_type to power dependent params       christos@qti.qualcomm.com,  ankitaga@qti.qualcomm.com   Recommended and validated by FBRx team    None   https://sharepoint.qualcomm.com/qct/Modem-Tech/Projects/Thor/Systems/Forms/Systems.aspx?RootFolder=0.000000qct0.000000Modem 0Tech0.000000Projects0.000000Thor0.000000Systems0.000000Common0.000000FBRx0.000000FBRxLM%20Spreadsheet   
1.0.0   Christos Komminakis, Ankit Agarwal   2/18/2015   
         1. Initial version; added default values; Items highlighted in blue will be over-written based on NV data; Items highlighted in orange are obselete. Only static params will be used from SS. The dynamic and power dependent params will be updated by SW.       christos@qti.qualcomm.com,  ankitaga@qti.qualcomm.com   Recommended and validated by FBRx team    None   https://sharepoint.qualcomm.com/qct/Modem-Tech/Projects/Thor/Systems/Forms/Systems.aspx?RootFolder=0.000000qct0.000000Modem 0Tech0.000000Projects0.000000Thor0.000000Systems0.000000Common0.000000FBRx0.000000FBRxLM%20Spreadsheet   

=============================================================================*/

/*=============================================================================
                           INCLUDE FILES
=============================================================================*/


#ifndef RFLM_FBRX_TYPEDEF_AG_H
#define RFLM_FBRX_TYPEDEF_AG_H

#ifdef __cplusplus
extern "C" {
#endif

#include "comdef.h"


/*==============================================================================

            EXTERNAL DEFINITIONS AND TYPES : MACROS

==============================================================================*/ 

#define RFLM_FBRX_MATRIX_PREDIST_AMAM_NUM 4
#define RFLM_FBRX_RX_FILTER_TAPS_NUM 24
#define RFLM_FBRX_TX_UP_FILTER_TAPS_NUM 5
#define RFLM_FBRX_DPD_PCOEFF_0_NUM 4
#define RFLM_FBRX_DPD_PCOEFF_1_NUM 4
#define RFLM_FBRX_DPD_K_NUM 3


/*==============================================================================

            EXTERNAL DEFINITIONS AND TYPES : ENUMS

==============================================================================*/ 

typedef enum
{
  RFLM_FBRX_1X0_V1 = 0, 
  RFLM_FBRX_1X0_V2 = 1, 
  RFLM_FBRX_1X1 = 2, 
  RFLM_FBRX_2X2 = 3, 
  RFLM_FBRX_3X3 = 4, 
}rflm_fbrx_algorithm_t;

typedef enum
{
  RFLM_FBRX_RSB_BEFORE_LOFT = 0, 
  RFLM_FBRX_LOFT_BEFORE_RSB = 1, 
}rflm_fbrx_algorithm_order_t;

typedef enum
{
  RFLM_FBRX_1_STAGE = 0, 
  RFLM_FBRX_2_STAGE = 1, 
}rflm_fbrx_upsample_stage_t;

typedef enum
{
  RFLM_FBRX_FFT_256 = 0, 
  RFLM_FBRX_FFT_512 = 1, 
  RFLM_FBRX_FFT_1024 = 2, 
}rflm_fbrx_fft_size_t;

typedef enum
{
  RFLM_FBRX_ENV_SCALE = 0, 
  RFLM_FBRX_IQ_SCALE = 1, 
}rflm_fbrx_es_iq_t;

typedef enum
{
  RFLM_FBRX_REAL_FILTER = 0, 
  RFLM_FBRX_COMPLEX_FILTER = 1, 
}rflm_fbrx_rx_filter_mode_t;

typedef enum
{
  RFLM_FBRX_RUN_POWER_MEAS_TYPE = 1, 
  RFLM_FBRX_RUN_VSWR_MEAS_TYPE = 2, 
  RFLM_FBRX_RUN_VSWR_PHASE_MEAS_TYPE = 3, 
  RFLM_FBRX_RUN_SWPT_MEAS_TYPE = 4, 
  RFLM_FBRX_RUN_NO_MEAS_TYPE = 255, 
}rflm_fbrx_meas_req_type_e;


/*==============================================================================

            EXTERNAL DEFINITIONS AND TYPES : STRUCTURES
     ANY CHNAGES TO THIS SECTION REQUIRES FBRX CMN FW INTERFACE CHNAGES 
==============================================================================*/ 

typedef struct
{
  uint16 matrix_predist_amam[RFLM_FBRX_MATRIX_PREDIST_AMAM_NUM]; /* vector of 64 AMAM coefs (16 unsigned bits each) */ 
  uint16 threshold_xcorr_1; /* Threshold for TA |Xcor| uniformity (Test 1: small RB case) */ 
  uint16 threshold_xcorr_2; /* Threshold for  detecting tau=1/2 (Test 2: T.O. ~= 1/2) */ 
  boolean use_previous_corr_en; /* 0: Recalculate correlation values 1 :Use the previous Correlation values */ 
  boolean use_previous_est_en; /* 0: Recalculate estimation values 1 :Use the previous estimation values */ 
  uint32 rx_filter_taps[RFLM_FBRX_RX_FILTER_TAPS_NUM]; /* Sc16t time domain taps for RX filter */ 
  int16 tx_up_filter_taps[RFLM_FBRX_TX_UP_FILTER_TAPS_NUM]; /* only for uplink CA */ 
  uint8 txfe_index; /* TxFE index:  0 = TxFE-0, 1 = TxFE-1 */ 
  boolean ca_enable; /* Enable Carrier Aggregation , 0: Disable, 1 : Enable */ 
  rflm_fbrx_upsample_stage_t ca0_num_upsample_stages; /* only for uplink CA0; LTE 10+10 = RFLM_FBRX_2_STAGE, for LTE 20+20 = RFLM_FBRX_1_STAGE */ 
  rflm_fbrx_upsample_stage_t ca1_num_upsample_stages; /* only for uplink CA1; LTE 10+10 = RFLM_FBRX_2_STAGE, for LTE 20+20 = RFLM_FBRX_1_STAGE */ 
  uint8 num_corr_lags; /* Number of Time Alignment Cross Correlations . Can be only 4,8,16,32 */ 
  uint8 num_tx_extension_samples; /* One-sided sample padding applied to Tx Data , Max 16 */ 
  uint8 lag_spacing; /* 0 or 1 sample */ 
  uint8 time_align_mode; /* 0: liner interp, 1: frequency domain interpolation */ 
  uint16 num_tx_in_samples_c0; /* Number of samples to be read from DTR-IB for ca0; LTE 10+10 = 195, for LTE 20+20 = 375 */ 
  uint16 num_tx_in_samples_c1; /* Number of samples to be read from DTR-IB for ca1; LTE 10+10 = 195, for LTE 20+20 = 375 */ 
  uint16 num_rx_in_samples; /* Number of sampled to be read from RxFE output, equals num_rx_samples + num_rx_taps */ 
  uint16 num_rx_samples; /* number of rx samples */ 
  uint8 num_rx_extension_samples; /* Padding to Apply to FBRx data for FBRx filter transient response */ 
  uint8 rx_filter_group_delay; /* Group delay of FBRx filter */ 
  boolean estimate_tau_en; /* 0: use predetermined Tau , 1 : Estimate new Tau.  Toggle based on num of RB's in LTE. */ 
  boolean apply_tau_en; /* 0: Do not apply Tau,  1: Apply Tau */ 
  int16 scaling_corr; /* Value to scale previous correlation settings. Only used if use_previous_en = TRUE */ 
  int16 scaling_est; /* Value to scale previous estimate. Only used if use_previous_en = TRUE */ 
  rflm_fbrx_algorithm_t processing_type; /* Select processing type : 3x3 , 2x2 , 1x1 , 1x0 */ 
  rflm_fbrx_algorithm_order_t subtype_3x3; /* 3x3 subtype: 1=LOFT before RSB, 0 = RSB before LOFT */ 
  uint16 threshold_gain; /* Threshold for GAIN Calculation */ 
  uint16 threshold_rsb; /* Threshold for RSB Calculation */ 
  uint16 threshold_loft; /* Threshold for LOFT Calculation */ 
  rflm_fbrx_fft_size_t fft_size_sel; /* fft size sel */ 
  uint16 num_rx_taps; /* num_rx_taps */ 
  uint16 tx_data_len; /* tx_data_len */ 
  uint16 predet_tau; /* predet_tau */ 
  boolean txfe_update; /* E.g., VSWR and Switchpoint dual captures don't do updates. */ 
  boolean scale_rx_en; /* scale_rx_en */ 
  uint16 scale_rx; /* Rx scale factor */ 
  boolean scale_tx_en; /* scale_tx_en */ 
  uint16 scale_tx; /* scale_tx 0x7FFF */ 
  int16 dtr_ib_offset_0; /* dtr_ib_offset */ 
  int16 dtr_ib_offset_1; /* dtr_ib_offset */ 
  rflm_fbrx_rx_filter_mode_t rx_filter_mode; /* rx_filter_mode */ 
  boolean use_uk; /* use_uk flag */ 
  uint16 predet_index; /* shift in Tx samples to be applied if time-alignment is disabled */ 
  boolean use_current_params; /* use current results for next result */ 
  int8 dpd_pcoeff_0[RFLM_FBRX_DPD_PCOEFF_0_NUM]; /* DPD vector param */ 
  int8 dpd_pcoeff_1[RFLM_FBRX_DPD_PCOEFF_1_NUM]; /* DPD vector param */ 
  int8 dpd_k[RFLM_FBRX_DPD_K_NUM]; /* DPD vector param */ 
  int16 dpd_src2_value; /* DPD scalar param */ 
  int8 dpd_bias; /* DPD scalar param */ 
  uint16 dpd_uconst; /* DPD scalar param */ 
  uint8 dpd_pwlin_mode; /* DPD scalar param */ 
  int8 dpd_scale; /* DPD scalar param */ 
  int16 scale_corr_alpha; /* correlation IIR scale params */ 
  int16 scale_corr_beta; /* correlation IIR scale params */ 
  int16 est_corr_alpha; /* estimation IIR scale params */ 
  int16 est_corr_beta; /* estimation IIR scale params */ 
  boolean vswr_ratio_modela; /* 0:  stay with standard ratio of complex gains for VSWR in common FED,  1: do ratio of modelA (incident/reflected) for LTE intra-band UL CA VSWR.  All other VSWR uses this as FALSE. */ 
  uint8 vsrc_to_output_ratio; /* VSRC to output ratio based on the dac settings for each fbrx mode */ 
}rflm_fbrx_static_param_t;

typedef struct
{
  int16 gain_comp; /* Parameter used for equaling (1tau, tau) TA interpolator gain droop */ 
  uint32 previous_gain; /* Previous Gain compensation. Needed if use_previous_en = TRUE */ 
  uint32 previous_loft; /* Previous LOFT compensation. Needed if use_previous_en = TRUE */ 
  uint32 previous_rsb; /* Previous Residual Side Band estimate. Needed if use_previous_en = TRUE */ 
  uint32 previous_gain_imbalance; /* Previous gain imbalance estimate. Needed if use_previous_en = TRUE */ 
  uint32 previous_phase_imbalance; /* Previous phase imbalance estimate. Needed if use_previous_en = TRUE */ 
  uint32 gain_droop_center; /* obsolete, will be removed once removed from fw */ 
  uint32 gain_droop_bw; /* obsolete, will be removed once removed from fw */ 
}rflm_fbrx_dynamic_param_t;

typedef struct
{
  uint32 gain_fbrx; /* fbrx expected gain based on external cal */ 
  uint16 threshold_ls; /* Threshold for Total LS Error */ 
  uint32 gain_txfe; /* TXFE gain calibration factor */ 
  boolean dpd_enabled; /* 0: Disabled,  1: Enabled. Depends on PA state */ 
  uint32 current_gain; /* Current Gain compensation */ 
  uint32 current_loft; /* Current LOFT compensation */ 
  uint32 current_rsb; /* Current RSB compensation */ 
  uint32 current_gain_imbalance; /* Current gain imbalance estimate */ 
  uint32 current_phase_imbalance; /* Current phase imbalance estimate */ 
  uint8 input_bias; /* Input bias to be applied to format conversion from FBRx buffer */ 
  uint32 wbdc_q; /* DC compensation for Q channel */ 
  uint32 wbdc_i; /* DC compensation for I channel */ 
  rflm_fbrx_es_iq_t es_iq_sel; /* Select envelope scaling or IQ scaling */ 
  uint16 scale_rx; /* Rx scale factor */ 
  uint16 predet_idx; /* shift in Tx samples to be applied if time-alignment is disabled */ 
  uint16 thresh_gain_high; /* threshold gain high */ 
  uint16 thresh_gain_low; /* threshold gain low */ 
  uint16 scale_tx; /* scale_tx 0x7FFF */ 
  uint32 gain_ca0; /* scale factor for CA0 */ 
  uint32 gain_ca1; /* scale factor for CA1 */ 
  rflm_fbrx_meas_req_type_e meas_type; /* 1: power measurement, 2: VSWR Magnitude, 3: VSWR Mag/Phase, 4: Switchpoint dual capture, 0xFF: no type */ 
  boolean txfe_update; /* E.g., VSWR and Switchpoint dual captures don't do updates. */ 
  uint32 input_capture_id; /*  */ 
  boolean estimate_tau_en; /* 0: use predetermined Tau , 1 : Estimate new Tau.  Toggle based on num of RB's in LTE. */ 
  boolean stage3_en; /* 0: stay with Stage-1,  1: Do Stage-3 algorithm for LTE intraband UL CA (but Stage-1 in Cal).  Dynamic in order to do Stage-1 in cases of very high imbalance. */ 
  uint32 reserved; /* reserved fields for future use */ 
}rflm_fbrx_pwr_dependent_param_t;

typedef struct
{
  rflm_fbrx_static_param_t fbrx_static_param;
  rflm_fbrx_dynamic_param_t fbrx_dynamic_param;
  rflm_fbrx_pwr_dependent_param_t fbrx_pwr_dependent_param;
}rflm_fbrx_struct_type_ag;

extern rflm_fbrx_struct_type_ag rflm_fbrx_txfe_dac_in_rate_17p2mhz_275p2mhz; 
extern rflm_fbrx_struct_type_ag rflm_fbrx_txfe_dac_in_rate_34p4mhz_275p2mhz; 
extern rflm_fbrx_struct_type_ag rflm_fbrx_txfe_dac_in_rate_68p8mhz_275p2mhz; 
extern rflm_fbrx_struct_type_ag rflm_fbrx_txfe_dac_in_rate_17p2mhz_137p6mhz; 
extern rflm_fbrx_struct_type_ag rflm_fbrx_txfe_dac_in_rate_14p4mhz_115p2mhz; 

#ifdef __cplusplus
}
#endif



#endif


