
#ifndef QFE3345_V32_ET_PA_CONFIG_AG_H
#define QFE3345_V32_ET_PA_CONFIG_AG_H/*
WARNING: This QFE3345_V32_ET driver is auto-generated.

Generated using: qpa_autogen.pl 
Generated from-  

	File: QFE3345_RFFE_Settings.xlsx 
	Released: 1/22/2015
	Author: rspring
	Revision: 6.52
	Change Note: Deleted the Vth REG on the PA config because its now within the sequence.  Added comment on the sequence to indicate the default value.
	Tab: qfe3345_v32_pa_settings_et

*/

/*=============================================================================

          RF DEVICE  A U T O G E N    F I L E

GENERAL DESCRIPTION
  This file is auto-generated and it captures the configuration of the QFE3345_V32_ET PA.

  Copyright (c) 2013, 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies, Inc.

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/rfdevice_qfe3345/api/qfe3345_v32_et_pa_config_ag.h#1 $
$Author: mplcsds1 $
$DateTime: 2016/03/28 23:04:26 $ 

=============================================================================*/

/*=============================================================================
                           INCLUDE FILES
=============================================================================*/


#include "comdef.h"
#include "rfc_common.h"
#include "rfdevice_qpa_typedef.h"
#ifdef __cplusplus
extern "C" {
#endif  

boolean rfdevice_qpa_qfe3345_v32_et_construct_driver_ag
(
  rfdevice_qpa_settings_type* qpa_settings,
  rfdevice_qpa_func_tbl_type* pa_fn_ptrs
);

extern void qfe3345_v32_et_convert_port_mapping(rfcom_band_type_u band, rfdevice_pa_bw_enum_type bw, rfm_mode_enum_type mode, uint8 *port_index);



#ifdef __cplusplus
}
#endif
#endif