#ifndef __MCPM_STATE_H__
#define __MCPM_STATE_H__

/*=========================================================================


           M O D E M   C L O C K   A N D   P O W E R   M A N A G E R 
                  
                 S T A T E   M A N G E M E N T   F I L E



GENERAL DESCRIPTION

  This file contains the MCPM state definitions.

PUBLIC EXTERNALIZED FUNCTIONS


INITIALIZATION AND SEQUENCING REQUIREMENTS
  Invoke the MCPM_Init function to initialize the Modem Clock and Power Manager.


    Copyright (c) 2013 by QUALCOMM Technologies, Inc.  All Rights Reserved.

==========================================================================*/


/*==========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/mpower_cmn/api/mcpm_state.h#1 $

when       who      what, where, why
--------   ---      --------------------------------------------------------
12/09/14   hg       Added LTE INIT state support.
02/13/14   ls       LTE VoLTE and CDRX support.
2/12/13    vs       Ported from Dime release branch. 
 

==========================================================================*/

/*==========================================================================

                     INCLUDE FILES FOR MODULE

==========================================================================*/

/*
 * Includes outside of the local directory.
 */
#include <mcpm_api.h>


/*
 * mcpm_state_type 
 *  
 * mcpm state type - the following states are maintained for each technology.
 */

typedef enum
{
  /* The technology is not active */
  MCPM_POWER_DOWN_STATE,

  /* The technology is in initial/suspend state */
  MCPM_INIT_STATE,

  /* The technology is in acquisiton mode */
  MCPM_ACQ_STATE,

  /* The technology is in receive mode */
  MCPM_IDLE_RX_STATE,

  /* The technology is in Page decode mode */
  MCPM_PAGE_INDICATOR_DECODE_STATE,

  /* The technology is in sleep mode */
  MCPM_SLEEP_STATE,

  /* The technology is in light sleep mode */
  MCPM_LIGHT_SLEEP_STATE,

  /* The technology is in sleep mode */
  MCPM_PSEUDO_SLEEP_STATE,

  /* The technology is in Voice call */
  MCPM_VOICE_STATE,

  /* The technology is in Data call */
  MCPM_DATA_STATE,

  /* The technology is in Voice and Data call */
  MCPM_VOICE_DATA_STATE,

  /* GPS Tech in Non DPO */
  MCPM_NON_DPO_STATE,

  /* GPS Tech in DPO */
  MCPM_DPO_STATE,

  /* max state used for boundary check */
  MCPM_MAX_STATE
} mcpm_state_type;


/*==========================================================================

  FUNCTION      MCPM_Is_State_Update_Needed

  DESCRIPTION   This function handles the MCPM requests in 
                MCPM_POWER_DOWN_STATE and computes the new MCPM state.


  PARAMETERS    req      - Modem SW mode change request.

  DEPENDENCIES  MCPM must be initialized.

  RETURN VALUE  
                TRUE - MCPM State needs to be updated
                FALSE- MCPM State update not needed

  SIDE EFFECTS  None.

==========================================================================*/

boolean MCPM_Is_State_Update_Needed
(
  mcpm_request_type req
);


/*==========================================================================

  FUNCTION      MCPM_Get_New_State

  DESCRIPTION   This function returns a new state based on the current state
                and input state transition request.

  PARAMETERS    curr_state - Tech's current state.
                req        - Modem SW mode/state change request.

  DEPENDENCIES  MCPM must be initialized.

  RETURN VALUE  mcpm_state_type - return new state.
                
  SIDE EFFECTS  None.

==========================================================================*/

mcpm_state_type MCPM_Get_New_State
(
  mcpm_state_type   curr_state,
  mcpm_request_type req
);


#endif /* __MCPM_STATE_H__ */
