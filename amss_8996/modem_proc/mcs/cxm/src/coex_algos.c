/*!
  @file
  coex_algos.c

  @brief
  This file contains all the message handlers as well CoEx algos (if any).


  @ingroup per_implementation
*/

/*=============================================================================

  Copyright (c) 2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/mcs/cxm/src/coex_algos.c#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
10/27/15   btl     Fix wci2 t6 aggregation for multiple carriers
08/22/15   cdh     Spreadsheet to victim table autogen and corresponding sys bhvr
06/19/15   btl     Move to new LTE RF power limit i/f supporting UL CA
05/15/15   btl     Add LTE UL CA support
05/15/15   blt     Use new CA conflict processing algorithm
03/17/15   btl     LTE DL CA support
02/24/15   ckk     Add support for WCDMA [instance 2]
01/20/15   ckk     Update type7[rrc_c] using RAT layer 3 states
01/14/15   ckk     Add support for type7[ant_tx]
12/02/14   ckk     Implement wci2 tx pwr limiting using type6[src]&[pl_req]
08/07/14   ckk     Handle wci2 type7 only if policy is enabled
07/28/14   tak     TDS/GSM Coex over SMEM
07/17/14   btl     Use updated WCI2 client interface
07/14/14   ckk     Added WCI2 type4 & type7[pwr_bit] support
07/10/14   btl     Update default GRFCs to BO/TH hw
05/28/14   btl     Featurize WCI2 subsystem when UART not present
05/16/14   tak     Basic WCI-2 protocol
05/05/14   ckk     Handling GSM2 & GSM3 RAT state updates over QMI
05/02/14   btl     Replace wakeup timer with a SLPC callback
04/25/14   btl     Type 6 filtering for WCI2 control + t6 reorg
03/25/14   btl     Type 7 filtering to eliminate short glitches
03/18/14   btl     Add LTE SINR support for QMB
02/26/14   btl     Add sending type 7 (WWAN TX ON/OFF)
02/19/14   ckk     Filter out too frequent LTE frame offset updates 
02/12/14   btl     Use SLPC to determine when WWAN techs do sleep/wakeup
01/20/14   btl     Filter out CXM WWAN State Info msgs with special link types
10/23/13   tak     Added TX advanced GRFC control, off state, center frequency
                   only conflict handling
07/18/13   tak     Removed dependency on NV file for DI 2.1 PL
07/15/13   tak     Added LTE subframe marker port and npa vote for bler/sinr
06/11/13   btl     Use cxm_task signal for timers instead of APC
06/10/13   btl     Throttle QMI succ/fail INDs to 1x per second each
05/28/13   tak     Support for 3-wire on 8974, power limiting via NV
05/22/13   ckk     React to LTE RRC state to update current power limiting state
03/01/13   tak     Remove dependency on LL1 messages and structures
02/13/13   tak     Added WWAN update and sleep indication support for all techs
02/04/13   btl     Add WCN<->WWAN page scan sync (featurized)
02/04/13   tak     Added control of GPIOs
01/10/13   tak     Added CA and TDS support for WWAN update and sleep indication 
12/19/12   btl     Port power limiting, band filtering, set APT table, WCN Prio
                   cond fail and consecutive subframe params, and trace logging
10/02/12   btl     Add metrics
09/06/12   cab     Removed hardcoded boot params, back to reading from nv
08/14/12   btl     Picked up missed coex msgr handlers
07/23/12   btl     Prevent sending duplicate WWAN state IND
07/19/12   cab     Updated for new LTE MSGR interface
07/18/12   cab     Updated NV versioning and protocols
07/13/12   cab     Update FW protocol versioning
07/13/12   cab     Fixes for messaging, loop index warnings, loop conditional
07/10/12   cab     Fix head pointer name in conn update
07/10/12   cab     Updated messaging, fix deletion from head of linked list
07/05/12   ckk     Correcly identify TDD frame structure
07/05/12   ejv     Add support for mqcsi layer.
04/23/12   ckk     Add support for WWAN state info REQ/RESP message
04/17/12   ckk     Move to new coex IDL
04/08/12   ckk     Initial Revision

=============================================================================*/

/*=============================================================================

                           INCLUDE FILES

=============================================================================*/

#include "coex_algos.h"
#include "mqcsi_conn_mgr.h"
#include <qmi_csi.h>
#include "cxm_msgr.h"
#include "cxm_utils.h"
#include <coexistence_service_v01.h>
#include "cxm_trace.h"
#include <lte_l1_types.h> /* to access LTE_L1_FRAME_STRUCTURE_FS2 */
#include <wcn_coex_mgr.h>
#include "cxm_wcn_tx.h"
#include "float.h"
#include "custmcs.h"
#include <coexistence_service_v02.h>
#include <slpc.h> /* to access slpc wakeup cb api */
#include "cxm_smem.h"
#include "cxm_fw_msg.h"
#include "cxm_intf_types.h"
#include <wci2_uart.h>
#include <wci2_core.h>
#include "coex_algos_v2.h"
#ifdef FEATURE_COEX_USE_NV
#include "coex_victim_table.h"
#endif

#ifdef FEATURE_COEX_USE_NV
#include "coex_nv.h"
#include "coex_confl.h"
#endif

#ifdef FEATURE_LTE
#include "rflte_msg.h"
#endif /*FEATURE_LTE*/

#include <DDITimetick.h>
#include <DDIChipInfo.h>

#include "modem_fw_memmap.h" /*FW SHMEM*/
#include "fws.h"
#include "modem_fw_ipc.h"
#include "stringl.h"

/*=============================================================================

                         INTERNAL VARIABLES

=============================================================================*/
/* high prio state values from QMI messaging */
#define COEX_WLAN_HIGH_PRIO_STOP  0x00
#define COEX_WLAN_HIGH_PRIO_START 0x01

/* connection state values from QMI messaging */
#define COEX_WLAN_CONN_DISABLED 0x00
#define COEX_WLAN_CONN_SETUP    0x01
#define COEX_WLAN_CONN_STEADY   0x02

/* connection mode values from QMI messaging */
#define COEX_WLAN_CONN_MODE_NONE             0x00
#define COEX_WLAN_CONN_MODE_STATION          0x01
#define COEX_WLAN_CONN_MODE_SOFTAP           0x02
#define COEX_WLAN_CONN_MODE_P2P_GROUP_OWNER  0x03
#define COEX_WLAN_CONN_MODE_P2P_CLIENT       0x04
#define COEX_WLAN_CONN_MODE_AMP              0x05

/* Max LTE Tx subframes */
#define COEX_MAX_LTE_TX_SUBF     10

/* recurring timer duration when sending boot params req to lte */
#define COEX_BOOT_REQ_RETRY_TIME_MS  1000

/* Values used for QMI LTE SINR */
/* 1.0 in Q8 format = 1 << 8 */
#define COEX_LTE_METRICS_MAX_ALPHA 0x100
/* float neg infinity */
#define COEX_INVALID_SINR_VALUE    0xff800000
/* special V01 carrier value meaning all carriers */
#define COEX_CARRIER_ALL_V01_INTERNAL -1

#define COEX_UART_JITTER_115200 87
#define COEX_UART_JITTER_2000000 5
#define COEX_UART_JITTER_3000000 4

#define MHZ_HZ_CONVERSION 1000000
#define MHZ_KHZ_CONVERSION 1000
#define KHZ_HZ_CONVERSION 1000

#define COEX_TIMETICKS_PER_MSEC 19200

/* policy max, min, default values */
#define COEX_POLICY_POWER_THRESH_MIN                        -70
#define COEX_POLICY_POWER_THRESH_MAX                         30
#define COEX_POLICY_POWER_THRESH_DEF                        -128
#define COEX_POLICY_RB_THRESH_MAX                            100
#define COEX_POLICY_CONT_SF_DENIALS_THRESH_MIN               2
#define COEX_POLICY_CONT_SF_DENIALS_THRESH_DEF               5
#define COEX_POLICY_CONT_SF_DENIALS_THRESH_MAX               8
#define COEX_POLICY_MAX_SF_DENIALS_MAX                       30
#define COEX_POLICY_MAX_SF_DENIALS_DEF                       10
#define COEX_POLICY_SF_DENIAL_WINDOW_MAX                     2000
#define COEX_POLICY_SF_DENIAL_WINDOW_DEF                     10
#define COEX_POLICY_TX_POWER_LIMIT_MIN                       16
#define COEX_POLICY_TX_POWER_LIMIT_MAX                       30
#define COEX_DEFAULT_LTE_POWER_LIMIT                         127
#define COEX_POLICY_LINK_PATH_LOSS_THRESH_MIN                0
#define COEX_POLICY_LINK_PATH_LOSS_THRESH_DEF                194
#define COEX_POLICY_LINK_PATH_LOSS_THRESH_MAX                194
#define COEX_POLICY_RB_FILTER_ALPHA_MIN                      0.0
#define COEX_POLICY_RB_FILTER_ALPHA_MAX                      1.0
#define COEX_POLICY_FILTERED_RB_THRESH_MIN                   0
#define COEX_POLICY_FILTERED_RB_THRESH_MAX                   100
#define COEX_POLICY_CONTR_PWRLMT_TIMEOUT_DEF                 150
#define COEX_POLICY_WCI2_PWRLMT_TIMEOUT_DEF                  150
#define COEX_POLICY_POWER_THRESH_FOR_TX_ADV_NTC_MIN_DB10    -700
#define COEX_POLICY_POWER_THRESH_FOR_TX_ADV_NTC_MAX_DB10     300
#define COEX_POLICY_POWER_THRESH_FOR_TX_ADV_NTC_DEF_DB10    -1280
#define COEX_POLICY_RB_THRESH_FOR_TX_ADV_NTC_MIN             0
#define COEX_POLICY_RB_THRESH_FOR_TX_ADV_NTC_MAX             100
#define COEX_POLICY_HOLDOFF_TIMER_RAT_CONN_STATE_MIN         0
#define COEX_POLICY_HOLDOFF_TIMER_RAT_CONN_STATE_DEF         90000
#define COEX_POLICY_HOLDOFF_TIMER_RAT_CONN_STATE_MAX         180000
#define COEX_POLICY_FILTER_ALPHA_RAT_PWR_STATE_MIN           0.0
#define COEX_POLICY_FILTER_ALPHA_RAT_PWR_STATE_MAX           1.0
#define COEX_POLICY_TX_PWR_THRESH_RAT_PWR_STATE_MIN          0
#define COEX_POLICY_TX_PWR_THRESH_RAT_PWR_STATE_DEF          1
#define COEX_POLICY_TX_PWR_THRESH_RAT_PWR_STATE_MAX          500
#define COEX_POLICY_HOLDOFF_TIMER_RAT_PWR_STATE_MIN          0
#define COEX_POLICY_HOLDOFF_TIMER_RAT_PWR_STATE_DEF          90000
#define COEX_POLICY_HOLDOFF_TIMER_RAT_PWR_STATE_MAX          180000

/* Defined to handle CUST_1's request for handle 90 sec delay in reporting
    not_connected and no_tx_pwr for WCI2 Type7 message byte */
#define COEX_SPL_PLCY_HLDFF_TMR_RAT_CONN_ST_DEF              90000
#define COEX_SPL_PLCY_HLDFF_TMR_RAT_PWR_ST_DEF               90000


#define COEX_WIRE_POLICIES ( CXM_POLICY_TOGGLE_FRAME_SYNC | \
                             CXM_POLICY_TOGGLE_TX_ACTIVE | \
                             CXM_POLICY_TOGGLE_RX_PRIORITY | \
                             CXM_POLICY_REACT_TO_WCN_PRIORITY )

#define COEX_WCI2_TYPE7_POLICIES ( CXM_POLICY_WCI2_SEND_TYPE7_MDM_CONN_STATE | \
                                   CXM_POLICY_WCI2_SEND_TYPE7_MDM_PWR_STATE )

/* defines for page scan wakeup sync between WCN and WWAN */
#define COEX_MAX_WWAN_WAKE_INTERVALS     4
#define COEX_MAX_WCN_WAKE_INTERVALS      4
#define COEX_WCN_WAKE_MIN_TIME_THRESH    20
#define COEX_WCN_WAKE_MAX_TIME_THRESH    60000

/*parameters for asserting tx active in advance*/
#define COEX_GRFC_ASSERT_PERIOD_USTMR    19200  /*1 ms*/
#define COEX_MAX_ADVANCED_TX_NOTIFI      40320  /*2.1 ms*/
#define COEX_MIN_ADVANCED_TX_NOTIFI      23040  /*1.2 ms*/
#define COEX_ADV_TIMING_TOLERANCE        1920   /*100 us*/

/* defines for WWAN TECH LTE */
#define COEX_TECH_LTE_FRAME_OFFSET_DIFF_THRESH  2 /* delta threshold frame offset
                                                       from last sent to trigger update */

#define COEX_BASIC_WCI2_MASK ~( COEX_PCM_SEND_WCI2_TYPE3_INACT_DURN_V01 | \
                                COEX_PCM_SEND_WCI2_TYPE6_TX_ADV_NOTICE_V01 | \
                                COEX_PCM_REACT_TO_WCI2_TYPE6_TX_POWER_LIMIT_V01 | \
                                COEX_PCM_SEND_WCI2_TYPE7_MDM_CONN_STATE_V01 )

#define COEX_TECH_GLOBAL_POLICY_MASK ( COEX_PCM_TOGGLE_FRAME_SYNC_V01              | \
                                       COEX_PCM_SEND_WCI2_TYPE3_INACT_DURN_V01     | \
                                       COEX_PCM_SEND_WCI2_TYPE4_SCAN_FREQ_V01      | \
                                       COEX_PCM_SEND_WCI2_TYPE7_MDM_PWR_STATE_V01  | \
                                       COEX_PCM_SEND_WCI2_TYPE7_MDM_TX_ANT_SEL_V01 | \
                                       COEX_PCM_SEND_WCI2_TYPE7_MDM_CONN_STATE_V01 )

#define COEX_WCI2_T6_PL_MASK             0x03
#define COEX_WCI2_T6_PL_REQ_OFF          0x00
#define COEX_WCI2_T6_PL_REQ_ON           0x01
#define COEX_WCI2_T6_PL_SRC_BT           0x00
#define COEX_WCI2_T6_PL_SRC_WIFI         0x01
#define COEX_WCI2_T6_PL_REQ_BIT_SHIFT    0x01
#define COEX_WCI2_T6_PL_REQ_OFF_SRC_BT   ( (COEX_WCI2_T6_PL_REQ_OFF << \
                                            COEX_WCI2_T6_PL_REQ_BIT_SHIFT) | \
                                            COEX_WCI2_T6_PL_SRC_BT )
#define COEX_WCI2_T6_PL_REQ_ON_SRC_BT    ( (COEX_WCI2_T6_PL_REQ_ON << \
                                            COEX_WCI2_T6_PL_REQ_BIT_SHIFT) | \
                                            COEX_WCI2_T6_PL_SRC_BT )
#define COEX_WCI2_T6_PL_REQ_OFF_SRC_WIFI ( (COEX_WCI2_T6_PL_REQ_OFF << \
                                            COEX_WCI2_T6_PL_REQ_BIT_SHIFT) | \
                                            COEX_WCI2_T6_PL_SRC_WIFI )
#define COEX_WCI2_T6_PL_REQ_ON_SRC_WIFI  ( (COEX_WCI2_T6_PL_REQ_ON << \
                                            COEX_WCI2_T6_PL_REQ_BIT_SHIFT) | \
                                            COEX_WCI2_T6_PL_SRC_WIFI )

/* Link types that WLAN coex does not about */
#define CXM_WLAN_IGNORED_LINK_TYPES (CXM_LNK_TYPE_POWER_MONITOR | \
                                     CXM_LNK_TYPE_DIVERSITY     | \
                                     CXM_LNK_TYPE_IRAT )
#define COEX_GET_LINK_CC(wwan_link_type) (((wwan_link_type) >> 8) & 0x03)

/* macros to translate CC mask to/from CC enum */
#define COEX_CC_MASK_ALL          0xFFFFFFFF
#define COEX_CC_TO_MASK(cc)       (1<<(cc))
#define COEX_CC_IN_MASK(cc, mask) (((mask)&(1<<(cc))) != 0)

typedef struct
{
  uint8         bytes[8];
  atomic_word_t status;
} coex_wci2_rx_s;

STATIC coex_wci2_rx_s coex_wci2_rx;

/* assign src to dest within bounds min to max */
#define COEX_BOUND_AND_SET( valid, dest, src, min, max ) \
  if ( valid ) \
  { \
    if ( src < min ) \
    { \
      dest = min; \
      CXM_MSG_2( ERROR, "Value coerced: %d to %d", (int)src, (int)min ); \
    } \
    else if ( src > max ) \
    { \
      dest = max; \
      CXM_MSG_2( ERROR, "Value coerced: %d to %d", (int)src, (int)max ); \
    } \
    else \
    { \
      dest = src; \
    } \
  }

/* macro to test if tech policy is active */
#define COEX_IS_ACTIVE( tech ) \
  (coex_wwan_state_info[(tech)].carrier[CXM_CARRIER_PCC].policy != CXM_POLICY_NONE)
#define COEX_CC_IS_ACTIVE( tech, cc) \
  (coex_wwan_state_info[(tech)].carrier[(cc)].policy != CXM_POLICY_NONE)

/* WLAN max number of SSIDs that can be active at once */
#define CXM_MAX_NUM_SSIDS 8

/*=============================================================================
 * WWAN STATE DATA TYPES & GLOBALS
 *=============================================================================*/
/*-table containing state and sleep information for all techs
  -the tech_id enum is used to index into this data structure at 
   runtime*/
coex_wwan_tech_info coex_wwan_state_info[CXM_TECH_MAX];

/* policy parameters */
coex_plcy_parms_s coex_plcy_parms;

/* flag indicating that coex bler metrics have been started */
boolean coex_metrics_lte_bler_started;

STATIC coex_pwr_lmt_s coex_pwr_lmt_info[CXM_CARRIER_MAX];
STATIC uint8 coex_wci2_pwr_lmt_src_req_status = 0x00;
/* timer when wci2 power limiting wdog timer expires */
STATIC timer_type coex_pwr_lmt_wdog_timer;

/* state used for tracking when uart can be powered off */
STATIC coex_state_type coex_state = 
       {
         0,                    // active_tech_mask
	   		 0,                    // conn_tech_mask
	   		 0,                    // tx_pwr_tech_mask
	   		 FALSE,                // uart_en_state
	   		 0,                    // uart_handle
	   		 CXM_TX_ANT_SEL_UNUSED // tx_ant_sel
	   	};

/* storage for band filtering info */
STATIC coex_band_filter_s coex_band_filter_info;
STATIC coex_scan_freq_band_filter_s coex_scan_freq_filter_info;

/*port mapping for wci-2*/
STATIC cxm_port_purpose_mapping_table_v01_s coex_wci2_port_mapping;

/* port mapping for SMEM data */
STATIC cxm_port_purpose_mapping_table_v01_s coex_smem_port_mapping;

/* port mapping for SMEM_V2 data */
STATIC cxm_port_purpose_mapping_table_v01_s coex_smem_v2_port_mapping;

STATIC coex_throttle_condition_ind_s coex_throttle_ind_info;

/* positions in these two index arrays are used as indices into the
 * common interval lookup table */
STATIC const uint16 coex_wwan_wake_intervals[COEX_MAX_WWAN_WAKE_INTERVALS] = {
  320, 640, 1280, 2560
};

STATIC const uint16 coex_wcn_wake_intervals[COEX_MAX_WCN_WAKE_INTERVALS] = {
  640,  1280, 1920, 2560
};

/* WWAN and WCN page intervals and the corresponding common interval
 * to adhere to (in msec) */
STATIC const uint16 
  coex_wcn_wake_interval_LUT[COEX_MAX_WWAN_WAKE_INTERVALS][COEX_MAX_WCN_WAKE_INTERVALS] = 
{
  /* WCN intvls 640,  1280, 1920, 2560 */
  /* ------------------------------------| WWAN intvls */
              { 640,  1280, 1920, 2560 }, /* 320  */
              { 640,  1280, 1920, 2560 }, /* 640  */
              { 1280, 1280, 3840, 2560 }, /* 1280 */
              { 2560, 2560, 7680, 2560 }  /* 2560 */
};

STATIC coex_wcn_wake_sync_s coex_wcn_wake_sync_info;

STATIC DalDeviceHandle *coex_DAL_handle = NULL;

/* structures used exclusively for logging */
STATIC cxm_lte_tx_adv_s_pack         cxm_tx_adv_log;
STATIC cxm_tech_metrics_read_s_pack  cxm_tech_metrics_log;
STATIC cxm_lte_bler_read_s_pack      cxm_lte_bler_log;
STATIC cxm_high_prio_s_pack          cxm_high_prio_log;
STATIC uint8                         cxm_recved_wci2_data_log;

STATIC wci2_type7_type_s coex_wci2_t7_state;

STATIC coex_state_info_type coex_state_info = 
{
  &coex_state,
  &coex_pwr_lmt_info[0],
  &coex_metrics_lte_bler_started,
  &coex_band_filter_info,
  FALSE,
  &cxm_tx_adv_log,
  &cxm_tech_metrics_log,
  &cxm_lte_bler_log,
  &cxm_high_prio_log,
  &coex_wci2_t7_state,
  &cxm_recved_wci2_data_log
};

/*scheduled deassert time in ustmr for cxm feature to enable tx in advance*/
STATIC boolean coex_deassert_scheduled = FALSE;
STATIC uint32 coex_tx_deassert_time;

/*advnaced notification period in ustmr for cxm feature to enable tx in advance*/
STATIC uint32 coex_adv_notifi_period;

STATIC boolean coex_state_txadv_late = FALSE;

/* store the old_indication to know if message has changed */
STATIC coex_wwan_state_ind_msg_v01 coex_wwan_state_ind;

/*=============================================================================
 * WLAN STATE DATA TYPES & GLOBALS
 *=============================================================================*/
/* linked list for active wlan connections */
STATIC coex_wlan_conn_node_type *coex_wlan_conn_head = NULL;

/* linked list for active wlan high prio */
STATIC coex_wlan_hp_node_type *coex_wlan_hp_head = NULL;

/* used to carry out mapping from SSID to a connection handle */
typedef struct
{
  uint8                  ssid_len;                          
  char                   ssid[CXM_MAX_SSID_LEN];  
} cxm_wlan_ssids_s;

STATIC cxm_wlan_ssids_s    cxm_ssid_list[CXM_MAX_NUM_SSIDS];

/*=============================================================================
 * CONFLICT DATA STRUCTURES
 *=============================================================================*/
/* storage related to NV coex_config_data */
coex_config_params_v10  coex_params;

/*=============================================================================
 * PROTOCOL DEFINES/DATA STRUCTURES
 *=============================================================================*/
/* COEX PROTOCOL -> COEX SYS BEHAVIOR MASK translation */
#define COEX_PROTOCOL_TRANSL_1 0x00 /* channel avoidance only */
#define COEX_PROTOCOL_TRANSL_3 ( CXM_SYS_BHVR_WCI2_DATA            | \
                                 CXM_SYS_BHVR_QMI_POLICY_CONTROL   | \
                                 CXM_SYS_BHVR_CXM_SENDS_POLICY     | \
                                 CXM_SYS_BHVR_CXM_SENDS_TYPE6      | \
                                 CXM_SYS_BHVR_QMI_METRICS)
#define COEX_PROTOCOL_TRANSL_4 ( CXM_SYS_BHVR_WCI2_DATA            | \
                                 CXM_SYS_BHVR_VICTIM_TABLE         | \
                                 CXM_SYS_BHVR_DISREGARD_RRC_PROC   | \
                                 CXM_SYS_BHVR_CXM_SENDS_POLICY     | \
                                 CXM_SYS_BHVR_CXM_ASSERTS_TX_ADV   | \
                                 CXM_SYS_BHVR_OFF_STATE_ENABLED    | \
                                 CXM_SYS_BHVR_TX_ADV_ALL_SUBFRAMES | \
                                 CXM_SYS_BHVR_CNTR_FRQ_CONFLICT)
/* QMB Control Plane over WCI2 */
#define COEX_PROTOCOL_TRANSL_5 ( CXM_SYS_BHVR_WCI2_DATA            | \
                                 CXM_SYS_BHVR_VICTIM_TABLE         | \
                                 CXM_SYS_BHVR_CXM_SENDS_POLICY     | \
                                 CXM_SYS_BHVR_WCI2_CONTROL         | \
                                 CXM_SYS_BHVR_TX_ADV_ALL_SUBFRAMES | \
                                 CXM_SYS_BHVR_CXM_SENDS_TYPE6)
/* QMB Control Plane over WCI2 + Antenna Sharing */
#define COEX_PROTOCOL_TRANSL_6 ( CXM_SYS_BHVR_WCI2_DATA            | \
                                 CXM_SYS_BHVR_VICTIM_TABLE         | \
                                 CXM_SYS_BHVR_CXM_SENDS_POLICY     | \
                                 CXM_SYS_BHVR_WCI2_CONTROL         | \
                                 CXM_SYS_BHVR_CXM_SENDS_TYPE6      | \
                                 CXM_SYS_BHVR_TX_ADV_ALL_SUBFRAMES | \
                                 CXM_SYS_BHVR_WLAN_ANT_SHARE)
/* Data plane over SMEM */
#define COEX_PROTOCOL_TRANSL_8 ( CXM_SYS_BHVR_SMEM_DATA            | \
                                 CXM_SYS_BHVR_VICTIM_TABLE         | \
                                 CXM_SYS_BHVR_CXM_SENDS_POLICY     | \
                                 CXM_SYS_BHVR_TX_ADV_ALL_SUBFRAMES | \
                                 CXM_SYS_BHVR_QMI_METRICS)
/* MCS asserts TX Adv + victim table, no type 6 */
#define COEX_PROTOCOL_TRANSL_9 ( CXM_SYS_BHVR_WCI2_DATA            | \
                                 CXM_SYS_BHVR_VICTIM_TABLE         | \
                                 CXM_SYS_BHVR_CXM_SENDS_POLICY     | \
                                 CXM_SYS_BHVR_DISREGARD_RRC_PROC   | \
                                 CXM_SYS_BHVR_CXM_ASSERTS_TX_ADV)
#define COEX_PROTOCOL_TRANSL_10 ( CXM_SYS_BHVR_WCI2_DATA           | \
                                  CXM_SYS_BHVR_QMI_POLICY_CONTROL  | \
                                  CXM_SYS_BHVR_CXM_SENDS_POLICY    | \
                                  CXM_SYS_BHVR_BASIC_WCI2)
/* Data plane over SMEM  with SAWless RF card */
#define COEX_PROTOCOL_TRANSL_12 ( CXM_SYS_BHVR_SMEM_DATA            | \
                                  CXM_SYS_BHVR_VICTIM_TABLE         | \
                                  CXM_SYS_BHVR_CXM_SENDS_POLICY     | \
                                  CXM_SYS_BHVR_TX_ADV_ALL_SUBFRAMES | \
                                  CXM_SYS_BHVR_QMI_METRICS          | \
                                  CXM_SYS_BHVR_NV_ONLY_VICT_TBL     | \
                                  CXM_SYS_BHVR_GSM_TDS_HIGH_PRIO_MSG)
/* Data plane over SMEM  with Suboptimal Filter RF card */
#define COEX_PROTOCOL_TRANSL_13 ( CXM_SYS_BHVR_SMEM_DATA            | \
                                  CXM_SYS_BHVR_VICTIM_TABLE         | \
                                  CXM_SYS_BHVR_CXM_SENDS_POLICY     | \
                                  CXM_SYS_BHVR_TX_ADV_ALL_SUBFRAMES | \
                                  CXM_SYS_BHVR_QMI_METRICS          | \
                                  CXM_SYS_BHVR_LTE_HIGH_PRIO_MSG)
/* Data plane over SMEM */
#define COEX_PROTOCOL_TRANSL_14 ( CXM_SYS_BHVR_SMEM_DATA                 | \
                                  CXM_SYS_BHVR_VICTIM_TABLE              | \
                                  CXM_SYS_BHVR_CXM_SENDS_POLICY          | \
                                  CXM_SYS_BHVR_TX_ADV_ALL_SUBFRAMES      | \
                                  CXM_SYS_BHVR_QMI_METRICS               | \
                                  CXM_SYS_BHVR_LTE_HIGH_PRIO_MSG         | \
                                  CXM_SYS_BHVR_GSM_TDS_HIGH_PRIO_MSG)
/* LTE-U with WLAN state info from DS */
#define COEX_PROTOCOL_TRANSL_15 ( CXM_SYS_BHVR_WLAN_INFO_FROM_DS         | \
                                  CXM_SYS_BHVR_VICTIM_TABLE              | \
                                  CXM_SYS_BHVR_NV_ONLY_VICT_TBL          | \
                                  CXM_SYS_BHVR_CXM_SENDS_POLICY)
/* Data plane over SMEM with SAWless and Suboptimal Filter RF card */
#define COEX_PROTOCOL_TRANSL_16 ( CXM_SYS_BHVR_SMEM_DATA                 | \
                                  CXM_SYS_BHVR_VICTIM_TABLE              | \
                                  CXM_SYS_BHVR_CXM_SENDS_POLICY          | \
                                  CXM_SYS_BHVR_TX_ADV_ALL_SUBFRAMES      | \
                                  CXM_SYS_BHVR_QMI_METRICS               | \
                                  CXM_SYS_BHVR_PROCESS_SAWLESS_CONFLICTS | \
                                  CXM_SYS_BHVR_LTE_HIGH_PRIO_MSG         | \
                                  CXM_SYS_BHVR_GSM_TDS_HIGH_PRIO_MSG)
/* QMB Control Plane over WCI2, channel avoidance only */
#define COEX_PROTOCOL_TRANSL_17 ( COEX_PROTOCOL_TRANSL_5                 | \
                                  CXM_SYS_BHVR_CHAN_AVOID_ONLY)

/*behavior of the system*/
cxm_sys_bhvr_t coex_sys_behavior_mask;

/*Shared memory with FW. Used for FW counters and information on TX advance grfc activity*/
cxm_fw_smem_s *cxm_fw_smem;

/*=============================================================================
 * CALLBACK PROTOTYPES
 *=============================================================================*/
/* callbacks for sleep/wake */
void coex_slpc_notify_cb( slpc_id_type id, uint64 wakeup_tick, 
                          boolean update, boolean extension );
void coex_slpc_notify_wakeup_cb( slpc_id_type id, uint64 wakeup_tick);

/* UART WCI2 receive */
void coex_algos_wci2_dir_read_cb( uint8* rx_types, uint8 status );

/* notification of new WLAN/WWAN conflict */
void coex_conflict_cb( coex_confl_retval *retval );

/* for each wifi band, call the work function */
void coex_wifi_band_iterator( cxm_wcn_tech_type tech, coex_wcn_mode_e mode,
                              coex_confl_wcn_work_cb work );

/*=============================================================================

                         INTERNAL FUNCTIONS

=============================================================================*/

/*=============================================================================

  FUNCTION:  coex_get_state_info_ptr

=============================================================================*/
/*!
    @brief
    Returns a pointer to the global coex_state_info struct for diag logging.

    @return
    coex_state_info_type *
*/
/*===========================================================================*/
coex_state_info_type* coex_get_state_info_ptr( void )
{
  return &coex_state_info;
}

/*=============================================================================

  FUNCTION:  coex_get_wwan_state_info_ptr

=============================================================================*/
/*!
    @brief
    Returns a pointer to the global coex_wwan_state_info struct for diag
    logging.

    @return
    coex_wwan_tech_info *
*/
/*===========================================================================*/
coex_wwan_tech_info* coex_get_wwan_state_info_ptr( void )
{
  return coex_wwan_state_info;
}

/*=============================================================================

  FUNCTION:  coex_get_params_ptr

=============================================================================*/
/*!
    @brief
    Returns a pointer to the global coex_params struct for diag processing.

    @return
    coex_config_params_v10 *
*/
/*===========================================================================*/
coex_config_params_v10* coex_get_params_ptr( void )
{
  return &coex_params;
}

/*=============================================================================

  FUNCTION:  coex_carrier_idl_to_cxm_transl

=============================================================================*/
/*!
    @brief
    Convert IDL carrier enum to CXM enum values. If not a valid translation,
    return CXM_CARRIER_MAX

    @return
    void
*/
/*===========================================================================*/
cxm_carrier_e coex_carrier_idl_to_cxm_transl( coex_carrier_v01 idl_id )
{
  /* There is a 1:1 mapping from one to the other. Perhaps this function
   * is not necessary */
  switch( idl_id )
  {
    case COEX_CARRIER_PRIMARY_V01:
      return CXM_CARRIER_PCC;
    case COEX_CARRIER_SECONDARY_0_V01:
      return CXM_CARRIER_SCC_0;
    case COEX_CARRIER_SECONDARY_1_V01:
      return CXM_CARRIER_SCC_1;
    default:
      return CXM_CARRIER_MAX;
  }
}

/*=============================================================================

  FUNCTION:  coex_slpc_to_cxm_id_transl

=============================================================================*/
/*!
    @brief
    convert slpc_id to cxm_tech_id

    @return
    cxm_tech_type
*/
/*===========================================================================*/
inline cxm_tech_type coex_slpc_to_cxm_id_transl( slpc_id_type slpc_id )
{
  cxm_tech_type cxm_id;
  /*-----------------------------------------------------------------------*/
  switch( slpc_id )
  {
    case SLPC_GSM:
      cxm_id = CXM_TECH_GSM1;
      break;

    case SLPC_1X:
      cxm_id = CXM_TECH_ONEX;
      break;

    case SLPC_HDR:
      cxm_id = CXM_TECH_HDR;
      break;

    case SLPC_WCDMA:
      cxm_id = CXM_TECH_WCDMA;
      break;

    case SLPC_LTE:
      cxm_id = CXM_TECH_LTE;
      break;

    case SLPC_TDSCDMA:
      cxm_id = CXM_TECH_TDSCDMA;
      break;

    case SLPC_GSM2:
      cxm_id = CXM_TECH_GSM2;
      break;

    case SLPC_GSM3:
      cxm_id = CXM_TECH_GSM3;
      break;

    case SLPC_WCDMA2:
      cxm_id = CXM_TECH_WCDMA2;
      break;

    default:
      cxm_id = CXM_TECH_DFLT_INVLD;
      break;
  }

  return cxm_id;
}

/*=============================================================================

  FUNCTION:  coex_fatal_err_cb

=============================================================================*/
/*!
    @brief
    Gets called when err fatal occurs, to log fatal in cxm trace

    @detail

    @return
    none
*/
/*===========================================================================*/
void coex_fatal_err_cb( void )
{
  /*-----------------------------------------------------------------------*/
  cxm_trace_event( CXM_TRC_ERR_FATAL, 0, 0, 0, CXM_TRC_EN );

  return;
}

/*=============================================================================

  FUNCTION:  set_coex_params

=============================================================================*/
/*!
    @brief
    Sets up paramters for coex.

    @detail
    Sets up the protocol used to determine algorithm behaviour and configures
    data plane ports

    @return
    none
*/
/*===========================================================================*/
void set_coex_params (
  void
)
{
  /*-----------------------------------------------------------------------*/
  memset( &coex_params, 0, sizeof( coex_config_params_v10 ) );

  coex_params.version = CXM_CONFIG_VERSION;
  coex_params.baud = WCI2_UART_BAUD_3000000;
#ifdef FEATURE_COEX_USE_NV
  coex_params.cxm_coex_protocol = CXM_COEX_PROTOCOL_1; 
#else
  coex_params.cxm_coex_protocol = CXM_COEX_PROTOCOL_3;
#endif

  coex_params.port_table.num_ports = 5;
  /* Frame Sync */
  coex_params.port_table.port_info[0].id = CXM_PORT_A;
  coex_params.port_table.port_info[0].type = CXM_PORT_TYPE_GRFC_BASED;
  coex_params.port_table.port_info[0].addr = 21;
  coex_params.port_table.port_info[0].assert_offset_us = -63;
  coex_params.port_table.port_info[0].deassert_offset_us = +7;
  /* RX Active */
  coex_params.port_table.port_info[1].id = CXM_PORT_B;
  coex_params.port_table.port_info[1].type = CXM_PORT_TYPE_GRFC_BASED;
  coex_params.port_table.port_info[1].addr = 22;
  coex_params.port_table.port_info[1].assert_offset_us = -43;
  coex_params.port_table.port_info[1].deassert_offset_us = +37;
  /* TX Active */
  coex_params.port_table.port_info[2].id = CXM_PORT_C;
  coex_params.port_table.port_info[2].type = CXM_PORT_TYPE_GRFC_BASED;
  coex_params.port_table.port_info[2].addr = 17;
  coex_params.port_table.port_info[2].assert_offset_us = -53;
  coex_params.port_table.port_info[2].deassert_offset_us = -53;
  /* WCN Prio */
  coex_params.port_table.port_info[3].id = CXM_PORT_D;
  coex_params.port_table.port_info[3].type = CXM_PORT_TYPE_L2VIC_BASED;
#ifdef FEATURE_MCS_THOR
  coex_params.port_table.port_info[3].addr = 143;
#else
  coex_params.port_table.port_info[3].addr = 240;
#endif
  coex_params.port_table.port_info[3].assert_offset_us = 0;
  coex_params.port_table.port_info[3].deassert_offset_us = 0;
  /* LTE subframe marker */
  coex_params.port_table.port_info[4].id = CXM_PORT_E;
  coex_params.port_table.port_info[4].type = CXM_PORT_TYPE_GRFC_BASED;
  coex_params.port_table.port_info[4].addr = 20;
  coex_params.port_table.port_info[4].assert_offset_us = 0;
  coex_params.port_table.port_info[4].deassert_offset_us = 0;

  return;
}

/*=============================================================================

  FUNCTION:  coex_algos_init

=============================================================================*/
/*!
    @brief
    Initializes coex algorithm structures/states
	
    @detail
    Initializes coex algorithm and structures including ports,
    policy information and boot parameters
 
    @return
    none
*/
/*===========================================================================*/
void coex_algos_init (
  void
)
{
  DALResult               DAL_retval = DAL_SUCCESS;
  wci2_client_open_s      uart_params;
  wci2_error_e            uart_retval;
  /* boot message received by CFW and tech L1s*/
  cxm_config_params_ind_s cxm_config_ind;
  uint8                   i = 0;
#ifdef FEATURE_COEX_USE_NV
  int32                   temp_Q8_conv;
  uint8                   j = 0;
#endif
  /*-----------------------------------------------------------------------*/

#ifdef FEATURE_COEX_USE_NV
  /* initialize and/or retrieve coex NV data */
  coex_nv_init( &coex_params );
#else
  set_coex_params();
#endif

  /*Protocol to system behavior translation. This determines what 
  functionality is enabled in coex. */
  switch(coex_params.cxm_coex_protocol)
  {
    case CXM_COEX_PROTOCOL_3:
      coex_sys_behavior_mask = COEX_PROTOCOL_TRANSL_3;
      break;
    case CXM_COEX_PROTOCOL_4:
      coex_sys_behavior_mask = COEX_PROTOCOL_TRANSL_4;
      break;
    case CXM_COEX_PROTOCOL_5:
      coex_sys_behavior_mask = COEX_PROTOCOL_TRANSL_5;
      break;
    case CXM_COEX_PROTOCOL_6:
      coex_sys_behavior_mask = COEX_PROTOCOL_TRANSL_6;
      break;
    case CXM_COEX_PROTOCOL_8:
      coex_sys_behavior_mask = COEX_PROTOCOL_TRANSL_8;
      break;
    case CXM_COEX_PROTOCOL_9:
      coex_sys_behavior_mask = COEX_PROTOCOL_TRANSL_9;
      break;
    case CXM_COEX_PROTOCOL_10:
      coex_sys_behavior_mask = COEX_PROTOCOL_TRANSL_10;
      break;
    case CXM_COEX_PROTOCOL_12:
      coex_sys_behavior_mask = COEX_PROTOCOL_TRANSL_12;
      break;
    case CXM_COEX_PROTOCOL_13:
      coex_sys_behavior_mask = COEX_PROTOCOL_TRANSL_13;
      break;
    case CXM_COEX_PROTOCOL_14:
      coex_sys_behavior_mask = COEX_PROTOCOL_TRANSL_14;
      break;
    case CXM_COEX_PROTOCOL_15:
      coex_sys_behavior_mask = COEX_PROTOCOL_TRANSL_15;
      break;
    case CXM_COEX_PROTOCOL_16:
      coex_sys_behavior_mask = COEX_PROTOCOL_TRANSL_16;
      break;
    case CXM_COEX_PROTOCOL_17:
      coex_sys_behavior_mask = COEX_PROTOCOL_TRANSL_17;
      break;
    case CXM_COEX_PROTOCOL_1:
    default:
      coex_sys_behavior_mask = COEX_PROTOCOL_TRANSL_1;
      break;
  }

  CXM_MSG_1( MED, "Protocol received: ", coex_params.cxm_coex_protocol );

#ifdef FEATURE_COEX_USE_NV
  /* now that we know protocol, initialize the victim table */
  coex_init_victim_table( &coex_params, &(coex_victim_tbl[0]) );
#endif

  /*Initialize shared memory between CXM and FW*/
  cxm_fw_smem = (cxm_fw_smem_s*) FW_SMEM_COEX_ADDR; 
  if( 0 != FW_SMEM_COEX_ADDR )
  {
    memset( cxm_fw_smem, 0, sizeof( cxm_fw_smem_s ) );
  }
  if ( COEX_SYS_ENABLED(CXM_SYS_BHVR_CXM_ASSERTS_TX_ADV) )
  {
    /*for WCI-2, CXM_PORT_C is for TX Active*/
    cxm_fw_smem->grfc_task.grfc_num = coex_params.port_table.port_info[2].addr;
    /*read the advanced notice for asserting TX active in 100microseconds*/
    coex_adv_notifi_period = coex_params.cxm_tx_active_adv_notice * COEX_CONVERT_100MICROSECS_USTMR;
  }

  /*Initialize wwan state info*/
  memset( &coex_wwan_state_info, 0, sizeof( coex_wwan_tech_info ) * CXM_TECH_MAX );

  /*read LTE sleep notification threshold from NV*/
  coex_wwan_state_info[CXM_TECH_LTE].sleep_thresh = coex_params.cxm_lte_sleep_notifi_thres;

  /*set up the port mappings*/
  coex_wci2_port_mapping.num_ports = 5;
  coex_wci2_port_mapping.port_purpose[0].id = CXM_PORT_A;
  coex_wci2_port_mapping.port_purpose[0].purpose = CXM_PORT_PURPOSE_FRAME_SYNC;
  coex_wci2_port_mapping.port_purpose[1].id = CXM_PORT_B;
  coex_wci2_port_mapping.port_purpose[1].purpose = CXM_PORT_PURPOSE_RX_ACTIVE;
  coex_wci2_port_mapping.port_purpose[2].id = CXM_PORT_C;
  coex_wci2_port_mapping.port_purpose[2].purpose = CXM_PORT_PURPOSE_TX_ACTIVE;
  coex_wci2_port_mapping.port_purpose[3].id = CXM_PORT_D;
  coex_wci2_port_mapping.port_purpose[3].purpose = CXM_PORT_PURPOSE_WCN_PRIORITY;
  coex_wci2_port_mapping.port_purpose[4].id = CXM_PORT_E;
  coex_wci2_port_mapping.port_purpose[4].purpose = CXM_PORT_PURPOSE_LTE_SUBFR_MARKER;

  coex_smem_port_mapping.num_ports = 3;
  coex_smem_port_mapping.port_purpose[0].id = CXM_PORT_A;
  coex_smem_port_mapping.port_purpose[0].purpose = CXM_PORT_PURPOSE_TX_ACTIVE;
  coex_smem_port_mapping.port_purpose[1].id = CXM_PORT_B;
  coex_smem_port_mapping.port_purpose[1].purpose = CXM_PORT_PURPOSE_RX_ACTIVE;
  coex_smem_port_mapping.port_purpose[2].id = CXM_PORT_C;
  coex_smem_port_mapping.port_purpose[2].purpose = CXM_PORT_PURPOSE_WCN_PRIORITY;

  coex_smem_v2_port_mapping.num_ports = 3;
  coex_smem_v2_port_mapping.port_purpose[0].id = CXM_PORT_D;
  coex_smem_v2_port_mapping.port_purpose[0].purpose = CXM_PORT_PURPOSE_TX_ACTIVE;
  coex_smem_v2_port_mapping.port_purpose[1].id = CXM_PORT_D;
  coex_smem_v2_port_mapping.port_purpose[1].purpose = CXM_PORT_PURPOSE_RX_ACTIVE;
  coex_smem_v2_port_mapping.port_purpose[2].id = CXM_PORT_C;
  coex_smem_v2_port_mapping.port_purpose[2].purpose = CXM_PORT_PURPOSE_WCN_PRIORITY;

  /* initialize COEX policy info for LTE */
  coex_wwan_state_info[CXM_TECH_LTE].plcy_parms.params.lte.controller_tx_power_limit =
    COEX_DEFAULT_LTE_POWER_LIMIT;
  coex_wwan_state_info[CXM_TECH_LTE].plcy_parms.params.lte.wci2_power_limit = 
    COEX_DEFAULT_LTE_POWER_LIMIT;
  coex_wwan_state_info[CXM_TECH_LTE].plcy_parms.params.lte.apt_table = 
    COEX_APT_TABLE_DEFAULT_V01;
  coex_wwan_state_info[CXM_TECH_LTE].plcy_parms.tx_continuous_subframe_denials_threshold = 
    COEX_POLICY_CONT_SF_DENIALS_THRESH_DEF;
  coex_wwan_state_info[CXM_TECH_LTE].plcy_parms.tx_subrame_denial_params.max_allowed_frame_denials = 
    COEX_POLICY_MAX_SF_DENIALS_DEF;
  coex_wwan_state_info[CXM_TECH_LTE].plcy_parms.tx_subrame_denial_params.frame_denial_window = 
    COEX_POLICY_SF_DENIAL_WINDOW_DEF;
  coex_wwan_state_info[CXM_TECH_LTE].plcy_parms.params.lte.wci2_tx_pwrlmt_timeout = 
    COEX_POLICY_WCI2_PWRLMT_TIMEOUT_DEF;
  coex_wwan_state_info[CXM_TECH_LTE].plcy_parms.params.lte.controller_tx_pwrlmt_timeout = 
    COEX_POLICY_CONTR_PWRLMT_TIMEOUT_DEF;
  coex_wwan_state_info[CXM_TECH_LTE].plcy_parms.params.lte.tx_power_threshold_for_adv_tx_notice = 
    COEX_POLICY_POWER_THRESH_FOR_TX_ADV_NTC_DEF_DB10;
  coex_wwan_state_info[CXM_TECH_LTE].plcy_parms.params.lte.rb_threshold_for_adv_tx_notice = 
    COEX_POLICY_RB_THRESH_FOR_TX_ADV_NTC_MIN;
  for( i = 0; i < CXM_CARRIER_MAX; i++ )
  {
    coex_wwan_state_info[CXM_TECH_LTE].carrier[i].plcy_parms.power_threshold = 
      COEX_POLICY_POWER_THRESH_DEF;
  }

  coex_plcy_parms.link_path_loss_threshold = COEX_POLICY_LINK_PATH_LOSS_THRESH_DEF;
  coex_plcy_parms.t7_con_holdoff = COEX_POLICY_HOLDOFF_TIMER_RAT_CONN_STATE_DEF;
  coex_plcy_parms.t7_pwr_alpha = COEX_POLICY_FILTER_ALPHA_RAT_PWR_STATE_MIN;
  coex_plcy_parms.t7_pwr_thresh = COEX_POLICY_TX_PWR_THRESH_RAT_PWR_STATE_DEF;
  coex_plcy_parms.t7_pwr_holdoff = COEX_POLICY_HOLDOFF_TIMER_RAT_PWR_STATE_DEF;

  /* initialize band filter info */
  coex_band_filter_info.ul_len = 0;
  coex_band_filter_info.dl_len = 0;
  coex_band_filter_info.filter_on = FALSE;

  /* send boot params to LTE, if it makes it..great..otherwise wait for req */
  cxm_config_ind.boot_config.static_sys_bhvr = coex_sys_behavior_mask;
  cxm_config_ind.boot_config.port_tbl = coex_params.port_table;

  cxm_msgr_send_msg( &cxm_config_ind.msg_hdr, 
                     MCS_CXM_COEX_CONFIG_PARAMS_IND, 
                     sizeof(cxm_config_params_ind_s) ); 

  if ( COEX_SYS_ENABLED(CXM_SYS_BHVR_WCI2_DATA) ||
       COEX_SYS_ENABLED(CXM_SYS_BHVR_WCI2_CONTROL) )
  {
    /* Initialize the UART */
    uart_params.baud = coex_params.baud;
    uart_params.mode = WCI2_CLIENT_MODE_WCI2;
    uart_params.type_mask = WCI2_REG_TYPE_1 | WCI2_REG_TYPE_6;
    uart_params.frame_mode = WCI2_FRAME_MODE_NONE;
    uart_params.basic_rx_cb = NULL;
    uart_params.dir_rx_cb = coex_algos_wci2_dir_read_cb;
    uart_params.t2_rx_cb = NULL;
    uart_retval = wci2_client_register( &uart_params, &coex_state.uart_handle );
    CXM_ASSERT( WCI2_E_SUCCESS == uart_retval );

    /*initialize manager for WLAN sticky bit*/
    cxm_wcn_tx_init();

    /* register for SLPC callbacks, used to tell when all techs are in/out 
     * of sleep. Used for powering UART on/off */
    slpc_set_notify_callback( &coex_slpc_notify_cb );
    slpc_set_notify_wakeup_callback( &coex_slpc_notify_wakeup_cb );

    coex_wci2_t7_state.wwan_tx_active = FALSE;
    coex_wci2_t7_state.wwan_tx_pwr_active = FALSE;
    coex_wci2_t7_state.tx_ant_sel = FALSE;
  }

  /* initialize info for power limiting */
  memset( coex_pwr_lmt_info, 0, sizeof(coex_pwr_lmt_s)*CXM_CARRIER_MAX );
  for( i = 0; i < CXM_CARRIER_MAX; i++ )
  {
    coex_pwr_lmt_info[i].cur_pwr_lmt = COEX_DEFAULT_LTE_POWER_LIMIT;
  }
  /* define timer needed for wdog in wci-2-based LTE power limiting */
  timer_def( &coex_pwr_lmt_wdog_timer, &cxm_nondeferrable_timer_group, 
             &cxm_tcb, CXM_COEX_PWR_LMT_WDOG_SIG, NULL, 0 );

  /* info to limit frequency of condition success/fail indications */
  memset( &coex_throttle_ind_info, 0, sizeof(coex_throttle_condition_ind_s) );

  coex_state_info.coex_initialized = TRUE;
  coex_metrics_lte_bler_started = FALSE;

  /* open a handle to DAL for use later getting the Qtimer 
   * timestamp for WCN<->WWAN page scan synchronization */
  DAL_retval = DalTimetick_Attach("SystemTimer", &coex_DAL_handle);
  CXM_ASSERT( (DAL_SUCCESS == DAL_retval) && (NULL != coex_DAL_handle) );
  coex_wcn_wake_sync_info.last_msg_ts = 0;

#ifdef FEATURE_COEX_USE_NV
  if ( COEX_SYS_ENABLED(CXM_SYS_BHVR_VICTIM_TABLE) )
  {
    /* initialize conflict processing */
    coex_confl_init( &coex_params, &(coex_victim_tbl[0]) );
    coex_confl_set_cb( coex_conflict_cb, NULL );
    if( COEX_SYS_ENABLED(CXM_SYS_BHVR_WCI2_CONTROL) )
    {
      coex_algos_init_v2( &coex_params, &(coex_victim_tbl[0]) );
    }
    else
    {
      coex_confl_set_wcn_band_iterator( coex_wifi_band_iterator );
    }

    /* initialize wwan state info to indicate no conflicts are active */
    for( i = 0; i < CXM_TECH_MAX; i++ )
    {
      for( j = 0; j < CXM_CARRIER_MAX; j++ )
      {
        coex_wwan_state_info[i].carrier[j].active_conf_index = 
          COEX_CONFL_INVALID_INDEX;
        coex_wwan_state_info[i].carrier[j].active_conf_row_num = 0;
      }
    }

    /* coerce victim table entries as needed*. This only happens once since
       victim table does not change */
    /* check all power limiting entries and coerce as needed */
    for ( i = 0; i < coex_params.num_conflicts; i++ )
    {
      if ( coex_victim_tbl[i].mdm_policy & CXM_POLICY_ENFORCE_CTRLR_TX_PWR_LMT )
      {
        /*check upper and lower limits and coerce value as needed*/
        if( coex_victim_tbl[i].mdm_params.pwr_lmt.power > COEX_POLICY_TX_POWER_LIMIT_MAX )
        {
          coex_victim_tbl[i].mdm_params.pwr_lmt.power = COEX_POLICY_TX_POWER_LIMIT_MAX;
        }
        else if( coex_victim_tbl[i].mdm_params.pwr_lmt.power < COEX_POLICY_TX_POWER_LIMIT_MIN )
        {
          coex_victim_tbl[i].mdm_params.pwr_lmt.power = COEX_POLICY_TX_POWER_LIMIT_MIN;
        }
      }

      if ( coex_victim_tbl[i].mdm_policy & CXM_POLICY_REACT_TO_WCN_PRIORITY )
      {  
        /*convert tx power threshold from db10 to signed Q8*/
        temp_Q8_conv = coex_victim_tbl[i].mdm_params.wcn_prio.tx_thlds.tx_pwr_thld;
        temp_Q8_conv = ( temp_Q8_conv << 8 ) / 10;
        coex_victim_tbl[i].mdm_params.wcn_prio.tx_thlds.tx_pwr_thld = (int16)temp_Q8_conv;
      }
    }

    /* check bounds on link path loss threshold */
    if(coex_params.link_path_loss_threshold < COEX_POLICY_LINK_PATH_LOSS_THRESH_MIN )
    {
      coex_params.link_path_loss_threshold = COEX_POLICY_LINK_PATH_LOSS_THRESH_MIN;
    }

    /* check bounds and coerce: [0 - 100] */
    if( coex_params.filtered_rb_threshold > COEX_POLICY_FILTERED_RB_THRESH_MAX)
    {
      coex_params.filtered_rb_threshold = COEX_POLICY_FILTERED_RB_THRESH_MAX;
    }
    else if( coex_params.filtered_rb_threshold < COEX_POLICY_FILTERED_RB_THRESH_MIN )
    {
      coex_params.filtered_rb_threshold = COEX_POLICY_FILTERED_RB_THRESH_MIN;
    }
  }
#endif
  /* register cb to receive err fatal indication in trace buffer */
  if ( err_crash_cb_register( coex_fatal_err_cb ) == FALSE )
  {
    CXM_MSG_0( ERROR, "failed to register err fatal callback" );
  }

  coex_state.active_tech_mask = 0;
  coex_state.conn_tech_mask = 0;
  coex_state.tx_pwr_tech_mask = 0;
#ifdef FEATURE_COEX_USE_NV
  coex_state.tx_ant_sel = CXM_TX_ANT_SEL_UNUSED;
#else
  coex_state.tx_ant_sel = CXM_TX_ANT_SEL_X;
#endif

  /* reset memory used to store SSIDs when WLAN state comes from Data Services*/
  memset( cxm_ssid_list, 0, sizeof(cxm_wlan_ssids_s) * CXM_MAX_NUM_SSIDS );

  CXM_MSG_0( HIGH, "coex_algos initialized" );
  
  return;
}

/*=============================================================================

  FUNCTION:  cxm_slpc_notify_cb

=============================================================================*/
/*!
    @brief
    Callback called by slpc when a tech goes in to sleep.

    @return
    void
*/
/*===========================================================================*/
void coex_slpc_notify_cb( 
  slpc_id_type id, 
  uint64 wakeup_tick, 
  boolean update, 
  boolean extension 
)
{
  cxm_coex_tech_sleep_wakeup_duration_ind_s msg;
  DalTimetickTime64Type timestamp, delta, delta_ms;
  DALResult             DAL_retval = DAL_SUCCESS;
  cxm_tech_type         cxm_tech_id;
  errno_enum_type       retval;
  /*-----------------------------------------------------------------------*/
  /* translate slpc_id to cxm_id type */
  cxm_tech_id = coex_slpc_to_cxm_id_transl( id );
  if( cxm_tech_id == CXM_TECH_DFLT_INVLD )
  {
    CXM_MSG_1( ERROR, "SLPC cb: unsupported tech %d", id );
  }
  else if( (cxm_tech_id != CXM_TECH_LTE) &&
           (coex_wwan_state_info[cxm_tech_id].conn_state != 
            CXM_TECH_STATE_INACTIVE) )
  {
    /* LTE sends us their own sleep/wake msgs, so this is not needed 
     * for LTE */

    /* determine if this is a valid wakeup. If so, send ourselves a msg; 
     * if not, process now. */
    coex_wwan_state_info[cxm_tech_id].wakeup_tick = wakeup_tick;
    DAL_retval = DalTimetick_GetTimetick64( coex_DAL_handle, &timestamp );
    CXM_MSG_4( MED, "SLPC cb: id=%d, wake_tick=%u, ts=%u, update=%d", 
               id, wakeup_tick, timestamp, update );

    msg.tech_id = cxm_tech_id;

    if( DAL_SUCCESS != DAL_retval || timestamp > wakeup_tick )
    {
      /* wakeup in the past! Process now */
      msg.is_going_to_sleep = FALSE;
      msg.duration = 0;
    }
    else
    {
      /* Whether start of new wakeup or update to existing, send 
       * CXM Sleep notification */
      delta = wakeup_tick - timestamp;
      DalTimetick_CvtFromTimetick64( coex_DAL_handle, delta, T_MSEC, &delta_ms );
      CXM_ASSERT( DAL_SUCCESS == DAL_retval );
      msg.is_going_to_sleep = TRUE;
      msg.duration = delta_ms;
    } /* valid wakeup */

    retval = cxm_msgr_send_msg( &msg.msg_hdr, 
               MCS_CXM_COEX_TECH_SLEEP_WAKEUP_IND, 
               sizeof(cxm_coex_tech_sleep_wakeup_duration_ind_s) );
    CXM_ASSERT( E_SUCCESS == retval );

  } /* valid cxm_tech_id */

  return;
} /* coex_slpc_notify_cb */

/*=============================================================================

  FUNCTION:  coex_slpc_notify_wakeup_cb

=============================================================================*/
/*!
    @brief
    Callback triggered by slpc when tech is waking up. 

    @return
    void
*/
/*===========================================================================*/
void coex_slpc_notify_wakeup_cb( slpc_id_type id, uint64 wakeup_tick )
{
  cxm_coex_tech_sleep_wakeup_duration_ind_s msg;
  errno_enum_type                           retval;
  cxm_tech_type                             cxm_tech_id;
  /*-----------------------------------------------------------------------*/
  CXM_MSG_1( LOW, "SLPC wakeup event cb: id=%d", id );

  /* translate slpc_id to cxm_id type */
  cxm_tech_id = coex_slpc_to_cxm_id_transl( id );

  if( (cxm_tech_id != CXM_TECH_DFLT_INVLD) &&
      (cxm_tech_id != CXM_TECH_LTE) &&
      (coex_wwan_state_info[cxm_tech_id].conn_state != 
       CXM_TECH_STATE_INACTIVE) )
  {
    /* fill out wakeup msg to send to ourselves, to process appropriately
     * in CXM context. */
    msg.tech_id = cxm_tech_id;
    msg.is_going_to_sleep = FALSE;
    msg.duration = 0;
    retval = cxm_msgr_send_msg( &msg.msg_hdr, 
               MCS_CXM_COEX_TECH_SLEEP_WAKEUP_IND, 
               sizeof(cxm_coex_tech_sleep_wakeup_duration_ind_s) );
    CXM_ASSERT( E_SUCCESS == retval );
  }

  return; 
} /* coex_slpc_notify_wakeup_cb */

/*=============================================================================

  FUNCTION:  coex_wifi_band_iterator

=============================================================================*/
/*!
    @brief
    Iterate through each of the wifi bands and call the "work" function
    for each one.

    @return
    void
*/
/*===========================================================================*/
void coex_wifi_band_iterator( 
  cxm_wcn_tech_type           tech,
  coex_wcn_mode_e             mode,
  coex_confl_wcn_work_cb      work
)
{
  uint32                    i;
  coex_band_type_v01       *band;
  coex_wlan_hp_node_type   *hp_node = coex_wlan_hp_head;
  coex_wlan_conn_node_type *conn_node = coex_wlan_conn_head;
  /*-----------------------------------------------------------------------*/
  CXM_ASSERT( tech == CXM_TECH_WIFI );

  if( mode == COEX_WCN_MODE_HP )
  {
    while( hp_node != NULL )
    {
      band = &hp_node->high_prio.band;
      work( tech, mode, band );
      hp_node = hp_node->next;
    }
  }
  else
  {
    /* mode == CONN */
    while( conn_node != NULL )
    {
      for ( i = 0; i < conn_node->conn.band_len; i++ )
      {
        band = &conn_node->conn.band[i];
        work( tech, mode, band );
      }
      conn_node = conn_node->next;
    }
  }

  return;
}

/*=============================================================================

  FUNCTION:  coex_trigger_grfc

=============================================================================*/
/*!
    @brief
    Triggers TX Active grfc on or off at a specified ustmr time
 
    @return
    none
*/
/*===========================================================================*/
void coex_trigger_grfc (
  boolean level,
  uint32 ustmr_time
)
{
  /*-----------------------------------------------------------------------*/
  CXM_MSG_2( MED, "set grfc level: %d, at time: %d ", level, ustmr_time);
  cxm_fw_smem->grfc_task.assert = level;
  cxm_fw_smem->grfc_task.trigger_time = ustmr_time;
  fws_ipc_send(FWS_IPC_SW_FW__GRFC_CMD);

  return;
}

/*=============================================================================

  FUNCTION:  coex_process_late_tx_adv

=============================================================================*/
/*!
    @brief
    Handles late TX advanced notice according to NV specification. 
 
    @return
    none
*/
/*===========================================================================*/
void coex_process_late_tx_adv (
  void
)
{
  /*-----------------------------------------------------------------------*/
  cxm_counter_event(CXM_CNT_LATE_LTE_TX_MSG, 0);
  CXM_MSG_0( MED, "Processing late TX advanced, count");
  if (!coex_state_txadv_late)
  {
    coex_state_txadv_late = TRUE;
    switch (coex_params.tx_adv_late_toggle)
    {
      case COEX_STATE_OFF:
        coex_trigger_grfc(FALSE, COEX_ADD_USTMR(COEX_READ_USTMR_TIME(), COEX_ADV_TIMING_TOLERANCE) );
        coex_deassert_scheduled = FALSE;
        break;
      case COEX_STATE_ON:
        coex_trigger_grfc(TRUE, COEX_ADD_USTMR(COEX_READ_USTMR_TIME(), COEX_ADV_TIMING_TOLERANCE) );
        coex_deassert_scheduled = TRUE;
        break;
      case COEX_STATE_LAST:
      default:
        break;
    }
  }

  return;
}

/*=============================================================================

  FUNCTION:  coex_manage_uart_npa_vote

=============================================================================*/
/*!
    @brief
    Check policy and lte state to determine if uart should be powered
    down or up.

    @return none
*/
/*===========================================================================*/
static void coex_manage_uart_npa_vote ( void )
{
  boolean new_uart_en_state;
  boolean type7_active = FALSE;
  boolean lte_metrics_vote = FALSE;
  uint8   i, sinr_cc_mask = 0, policy_cc_mask = 0;
  /*-----------------------------------------------------------------------*/
  /*if CXM is controlling the TX grfc, check if it needs to be turned OFF*/
  if ( COEX_SYS_ENABLED(CXM_SYS_BHVR_CXM_ASSERTS_TX_ADV) &&
       coex_deassert_scheduled && 
       ( coex_state.active_tech_mask == 0 ||
       !(coex_wwan_state_info[CXM_TECH_LTE].carrier[CXM_CARRIER_PCC].policy & 
         CXM_POLICY_TOGGLE_TX_ACTIVE) ) )
  {
    coex_deassert_scheduled = FALSE;
    coex_trigger_grfc(FALSE, COEX_ADD_USTMR(COEX_READ_USTMR_TIME(), COEX_ADV_TIMING_TOLERANCE) );
  }

  /* check if UART needs to be on for the type 7 policy 
   * (tech both on and connected) */
  if( ( COEX_PCM_SEND_WCI2_TYPE7_MDM_CONN_STATE_V01 & 
        coex_wwan_state_info[CXM_TECH_LTE].plcy_parms.tech_policy ||
        COEX_PCM_SEND_WCI2_TYPE7_MDM_PWR_STATE_V01 & 
        coex_wwan_state_info[CXM_TECH_LTE].plcy_parms.tech_policy ) && 
      ( (coex_state.conn_tech_mask & coex_state.active_tech_mask) != 0 ) )
  {
    type7_active = TRUE;
  }

  /* check if UART needs to be on for SINR or policy */
  for( i = 0; i < CXM_CARRIER_MAX; i++ )
  {
    policy_cc_mask |= COEX_CC_IS_ACTIVE(CXM_TECH_LTE, i) << i;
  }

  /* don't want to force the UART on if doing control plane over WCI-2 */
  sinr_cc_mask = coex_wwan_state_info[CXM_TECH_LTE].metrics_started;
  if( COEX_SYS_ENABLED(CXM_SYS_BHVR_QMI_METRICS) && 
      (coex_metrics_lte_bler_started || sinr_cc_mask != 0) )
  {
    lte_metrics_vote = TRUE;
  }

  /* now that we've evaluated all parts, determine if the UART should be on */
  if ( ((coex_state.active_tech_mask & (1<<CXM_TECH_LTE)) || type7_active ) &&
       (policy_cc_mask != 0 || lte_metrics_vote) )
  {
    new_uart_en_state = TRUE;
  }
  else
  {
    new_uart_en_state = FALSE;
  }

  /* power on or off the UART */
  if( coex_state.uart_en_state != new_uart_en_state )
  {
    coex_state.uart_en_state = new_uart_en_state;
    wci2_client_enable_uart( coex_state.uart_handle, coex_state.uart_en_state );
  }

  CXM_MSG_6( MED, "coex npa - vote %d, policies 0x%x, bler %d, sinrs 0x%x, "
                  "techs_active 0x%x, techs_conn 0x%x",
             coex_state.uart_en_state, 
             policy_cc_mask,
             coex_metrics_lte_bler_started,
             sinr_cc_mask,
             coex_state.active_tech_mask,
             coex_state.conn_tech_mask );
  CXM_TRACE(0, 0, 0, 0, CXM_LOG_PKT_EN, CXM_LOG_STATE);

  return;
}

/*=============================================================================

  FUNCTION:  coex_update_wlan_conn_state_info

=============================================================================*/
/*!
    @brief
    This function updates current connection list with new connection,
    or deletes an old connection, per incoming wlan message
	
    @detail
    Using the conn linked list, the function attempts to locate the
    incoming conn data in the existing list.  If it is not found, a new
    node is created with the new conn data.  Then the function determines
    if the incoming conn data is changing to DISABLED state, indicating
    that the conn is no longer active.  If this is the case, the conn
    data is removed from the conn linked list.
 
    @return
    none
*/
/*===========================================================================*/
static void coex_update_wlan_conn_state_info (
  coex_set_wlan_state_req_msg_v01 *request
)
{
  coex_wlan_conn_node_type *node = NULL;
  coex_wlan_conn_node_type *prev = NULL;
  uint8                    i;
  boolean                  found_wlan_5g = FALSE;
  /*-----------------------------------------------------------------------*/
  CXM_TRACE( CXM_TRC_QMI_UPDATE_WLAN_CONN,
                   request->conn_info.handle, request->conn_info.state,
                   request->conn_info.mode, CXM_TRC_EN );

  if ( coex_wlan_conn_head == NULL )
  {
    CXM_MSG_1( MED, "creating new wlan conn node, handle %d",
               request->conn_info.handle );

    /* list is empty, create single node list */
    node = CXM_MEM_CALLOC( 1, (size_t) sizeof(coex_wlan_conn_node_type) );
    CXM_ASSERT( NULL != node );
    coex_wlan_conn_head = node;
  }
  else
  {
    /* list is not empty, try to find an existing node */
    prev = coex_wlan_conn_head;
    node = coex_wlan_conn_head;
    while ( node != NULL )
    {
      /* if handles match, this is a known connection */
      if ( node->conn.handle == request->conn_info.handle )
      {
        break;
      }
      prev = node;
      node = node->next;
    }

    if ( node == NULL )
    {
      CXM_MSG_1( MED, "creating new wlan conn node, handle %d",
                 request->conn_info.handle );

      /* could not find existing node, create new one at tail */
      node = CXM_MEM_CALLOC( 1, (size_t) sizeof(coex_wlan_conn_node_type) );
      CXM_ASSERT( NULL != node );
      prev->next = node;
    }
  }

  /* at this point node points to the correct node for this data */

  if ( request->conn_info.state == COEX_WLAN_CONN_DISABLED )
  {
    CXM_MSG_1( MED, "deleting wlan conn node, handle %d",
               request->conn_info.handle );

    /* this conn is done, delete it */
    if ( node == coex_wlan_conn_head )
    {
      /* special case to delete first node */
      coex_wlan_conn_head = node->next;
    }
    else if ( prev != NULL )
    {
      /* multiple nodes, delete and rejoin the remaining list */
      prev->next = node->next;
    }

    CXM_MEM_FREE( node );
  }
  else
  {
    /* this connection is active, copy in the data */
    node->conn = request->conn_info;
  }

  return;
}

/*=============================================================================

  FUNCTION:  coex_update_wlan_hp_info

=============================================================================*/
/*!
    @brief
    This function updates current high prio list with new high prio,
    or deletes an old high prio, per incoming wlan message
	
    @detail
    Using the high prio event linked list, the function attempts to locate the
    incoming high prio data in the existing list. If it is not found, a new
    node is created with the new high prio data. Then the function determines
    if the incoming high prio data is changing to STOP state, indicating
    that the high prio event is no longer active. If this is the case, the high prio
    event data is removed from the linked list.
 
    @return
    none
*/
/*===========================================================================*/
static void coex_update_wlan_hp_state_info (
  coex_set_wlan_state_req_msg_v01 *request
)
{
  coex_wlan_hp_node_type *node = NULL;
  coex_wlan_hp_node_type *prev = NULL;
  /*-----------------------------------------------------------------------*/
  CXM_TRACE( CXM_TRC_QMI_UPDATE_WLAN_HP,
                   request->high_prio_info.id, request->high_prio_info.state, 
                   request->high_prio_info.band.freq, CXM_TRC_EN );

  if ( coex_wlan_hp_head == NULL )
  {
    CXM_MSG_1( MED, "creating first wlan high prio node, id %d",
               request->high_prio_info.id );

    /* list is empty, create single node list */
    node = CXM_MEM_CALLOC( 1, (size_t) sizeof(coex_wlan_hp_node_type) );
    CXM_ASSERT( NULL != node );
    coex_wlan_hp_head = node;
  }
  else
  {
    /* list is not empty, try to find an existing node */
    prev = coex_wlan_hp_head;
    node = coex_wlan_hp_head;
    while ( node != NULL )
    {
      /* if id's match, this is a known connection */
      if ( node->high_prio.id == request->high_prio_info.id )
      {
        break;
      }
      prev = node;
      node = node->next;
    }

    if ( node == NULL )
    {
      CXM_MSG_1( MED, "creating new wlan high prio node, id %d",
                 request->high_prio_info.id );

      /* could not find existing node, create new one at tail */
      node = CXM_MEM_CALLOC( 1, (size_t) sizeof(coex_wlan_hp_node_type) );
      CXM_ASSERT( NULL != node );
      prev->next = node;
    }
  }

  if ( request->high_prio_info.state == COEX_WLAN_HIGH_PRIO_STOP )
  {
    CXM_MSG_1( MED, "deleting wlan high prio node, id %d",
               request->high_prio_info.id );

    /* this high prio is done, delete it */
    if ( node == coex_wlan_hp_head )
    {
      /* special case to delete first node */
      coex_wlan_hp_head = node->next;
    }
    else if ( prev != NULL )
    {
      /* multiple nodes, delete and rejoin the remaining list */
      prev->next = node->next;
    }

    CXM_MEM_FREE( node );
  }
  else
  {
    /* this high prio event is active, copy in the data */
    node->high_prio = request->high_prio_info;
  }

  return;
}

/*=============================================================================

  FUNCTION:  coex_handle_boot_params_req

=============================================================================*/
/*!
    @brief
    Handles the boot parameters confirmation message
 
    @detail
    This function handles the parameters request message, sent by
    lte when it desires the params information.  This function
    verifies that the result returned in the message is successful.  If
    this is a successful request, this function sends the params
    indication to lte.
 
    @return
    err code from lte indicating success/failure in receiving params
*/
/*===========================================================================*/
errno_enum_type coex_handle_params_req (
  void *rcv_msgr_msg_ptr
)
{
  /* boot message received by CFW and tech L1s*/
  cxm_config_params_ind_s        cxm_config_ind;
  errno_enum_type ret_val;
  /*-----------------------------------------------------------------------*/
  CXM_ASSERT( coex_state_info.coex_initialized == TRUE );

  /* send config params */
  cxm_config_ind.boot_config.static_sys_bhvr = coex_sys_behavior_mask;
  cxm_config_ind.boot_config.port_tbl = coex_params.port_table;

  ret_val = cxm_msgr_send_msg( &cxm_config_ind.msg_hdr, 
                     MCS_CXM_COEX_CONFIG_PARAMS_IND, 
                     sizeof(cxm_config_params_ind_s) ); 

  return ret_val;
}

/*===========================================================================

  FUNCTION:  cxm_req_boot_params

===========================================================================*/
/*!
  @brief
    Resend the boot params message. CFW will use this to ask for the boot
    params message. A function call is used here since CFW does not yet
    have a msgr message send queue.

  @return
    none
*/
/*=========================================================================*/
void cxm_req_boot_params (
  void
)
{
  cxm_config_params_ind_s        cxm_config_ind;
  /*-----------------------------------------------------------------------*/
  /* if algos init is not yet done, we will send the boot params when it is done*/
  if ( coex_state_info.coex_initialized )
  {
    /* send config params */
    cxm_config_ind.boot_config.static_sys_bhvr = coex_sys_behavior_mask;
    cxm_config_ind.boot_config.port_tbl = coex_params.port_table;

    cxm_msgr_send_msg( &cxm_config_ind.msg_hdr, 
                       MCS_CXM_COEX_CONFIG_PARAMS_IND, 
                       sizeof(cxm_config_params_ind_s) );
  }

  return;
}

/*===========================================================================

  FUNCTION:  cxm_set_tx_ant_sel

===========================================================================*/
/*!
  @brief
    Used to set transmit (Tx) antenna selected
	  
	@return
	  errno_enum_type
*/
/*=========================================================================*/
errno_enum_type cxm_set_tx_ant_sel (
  cxm_tx_ant_sel_e tx_ant_sel
)
{
  errno_enum_type retval = E_SUCCESS;
  cxm_coex_tx_ant_sel_ind_s msgr_msg;
  /*-----------------------------------------------------------------------*/
  if ( COEX_SYS_ENABLED(CXM_SYS_BHVR_WCI2_DATA) )
  {
    msgr_msg.tx_ant_sel = tx_ant_sel;
    retval = cxm_msgr_send_msg( &msgr_msg.msg_hdr,
                                MCS_CXM_COEX_TX_ANT_SEL_IND,
                                sizeof(cxm_coex_tx_ant_sel_ind_s) );
  }
  else
  {
    retval = E_NOT_SUPPORTED;
  }

  CXM_MSG_2( HIGH, "Request to set Tx Ant selection[%d]/retval[%d]",
             tx_ant_sel, retval );

  return retval;
}

/*=============================================================================

  FUNCTION:  coex_check_and_set_power_limit

=============================================================================*/
/*!
    @brief
    Helper function to clear or set appropriately the LTE power limit

    @return void
*/
/*===========================================================================*/
void coex_check_and_set_power_limit (
  uint32 cc_mask
)
{
#ifdef FEATURE_LTE
  float                          new_power_limit;
  int16                          int_power_limit;
  boolean                        send_message = FALSE;
  cxm_carrier_e                  cc;
  errno_enum_type                retval = E_SUCCESS;
  coex_wwan_tech_info           *lte_info;
  rfa_rf_lte_set_tx_plimit_ind_s msgr_msg;
  /*-----------------------------------------------------------------------*/
  /* Reset the memory available for the RF set Tx power limit message */
  memset( &msgr_msg, 0, sizeof( rfa_rf_lte_set_tx_plimit_ind_s ) );
  lte_info = &coex_wwan_state_info[CXM_TECH_LTE];

  /* can only reset limit if LTE in system... */
  if( lte_info->state_data.num_link_info_sets != 0 )
  {
    for( cc = CXM_CARRIER_PCC; cc < CXM_CARRIER_MAX && 
                               cc < RFA_RF_LTE_MAX_TX_CELLS_SUPPORTED; cc++ )
    {
      if( !COEX_CC_IN_MASK(cc, cc_mask) )
      {
        continue;
      }

      /* set based on state of both controller and wci-2 power limit */
      if( (coex_pwr_lmt_info[cc].controller_state == COEX_PWR_LMT_ACTIVE) &&
          (coex_pwr_lmt_info[cc].wci2_state       == COEX_PWR_LMT_ACTIVE) )
      {
        /* resolve - use lower of the two */
        if( lte_info->plcy_parms.params.lte.controller_tx_power_limit < 
            lte_info->plcy_parms.params.lte.wci2_power_limit )
        {
          new_power_limit = lte_info->plcy_parms.params.lte.controller_tx_power_limit;
        }
        else
        {
          new_power_limit = lte_info->plcy_parms.params.lte.wci2_power_limit;
        }
      }
      else if( coex_pwr_lmt_info[cc].controller_state == COEX_PWR_LMT_ACTIVE )
      {
        new_power_limit = lte_info->plcy_parms.params.lte.controller_tx_power_limit;
      }
      else if( coex_pwr_lmt_info[cc].wci2_state == COEX_PWR_LMT_ACTIVE )
      {
        new_power_limit = lte_info->plcy_parms.params.lte.wci2_power_limit;
      }
      else
      {
        /* neither policy active - clear limit */
        new_power_limit = COEX_DEFAULT_LTE_POWER_LIMIT;
      }

      if( new_power_limit != coex_pwr_lmt_info[cc].cur_pwr_lmt )
      {
        /* if no limits changed for any of the CCs, nothing for RF to do */
        send_message = TRUE;

        /* cast is safe - range is checked in SET POLICY REQ */
        int_power_limit = (int16) (new_power_limit * 10);
        CXM_MSG_2( HIGH, "Setting LTE power limit to %d(db10) (cc %d)",
                   int_power_limit, cc );
        msgr_msg.sg_txpl_ulca_params[cc].plimit_type_mask = 
          RFA_RF_LTE_WCN_COEX_PLIMIT_MASK;
        msgr_msg.sg_txpl_ulca_params[cc].wcn_coex_plimit = int_power_limit;
        CXM_TRACE( CXM_TRC_SET_LTE_TX_PWR_LMT, (uint32)new_power_limit, 
                   (uint32)coex_pwr_lmt_info[cc].cur_pwr_lmt, cc, 
                   CXM_TRC_AND_PKT_EN, CXM_LOG_WCI2_PWR_LMT_STATE,
                   CXM_LOG_CONTROLLER_PWR_LMT_STATE, CXM_LOG_LTE_POLICY_INFO );
        coex_pwr_lmt_info[cc].cur_pwr_lmt = new_power_limit;
      }
    } /* LTE CC loop */
  }

  if( send_message )
  {
    retval = cxm_msgr_send_msg( &msgr_msg.hdr,
                                RFA_RF_LTE_SET_TX_PLIMIT_IND,
                                sizeof(rfa_rf_lte_set_tx_plimit_ind_s) );
    CXM_ASSERT( E_SUCCESS == retval );
  }
#endif /*FEATURE_LTE*/

  return;
}

/*=============================================================================

  FUNCTION:  coex_check_power_limiting_conditions

=============================================================================*/
/*!
    @brief
    Helper function to check with ML1 to see if conditions allow LTE
    power limiting

    @return void
*/
/*===========================================================================*/
void coex_check_power_limiting_conditions (
  uint32  cc_mask
)
{
  uint32                               num = 0;
  cxm_carrier_e                        cc;
  errno_enum_type                      retval = E_SUCCESS;
  cxm_coex_tx_pwr_lmt_lte_cndtns_req_s msgr_msg;
  /*-----------------------------------------------------------------------*/
  /* only call if LTE in system. if LTE not in system, state remains
     pending, and check will repeat when it comes back */
  if( coex_wwan_state_info[CXM_TECH_LTE].state_data.num_link_info_sets == 0 )
  {
    return;
  }

  /* ask LTE to check the carriers from the mask. Skip any we already asked */
  for( cc = CXM_CARRIER_PCC; cc < CXM_CARRIER_MAX; cc++ )
  {
    if( COEX_CC_IN_MASK(cc, cc_mask) && 
        !coex_pwr_lmt_info[cc].check_in_progress )
    {
      coex_pwr_lmt_info[cc].check_in_progress = TRUE;
      msgr_msg.cc_ids[num] = cc;
      num++;
    }
  }

  if( num > 0 )
  {
    msgr_msg.num_ccs = num;
    retval = cxm_msgr_send_msg( &msgr_msg.msg_hdr,
                                MCS_CXM_COEX_TX_PWR_LMT_LTE_CNDTNS_REQ,
                                sizeof(cxm_coex_tx_pwr_lmt_lte_cndtns_req_s) );
    CXM_ASSERT( E_SUCCESS == retval );
  }

  CXM_TRACE( 0, 0, 0, 0, CXM_LOG_PKT_EN, 
             CXM_LOG_WCI2_PWR_LMT_STATE, CXM_LOG_CONTROLLER_PWR_LMT_STATE );

  return;
}

/*=============================================================================

  FUNCTION:  coex_prep_and_send_wci2_type7

=============================================================================*/
/*!
    @brief
    Send a type 7 (WWAN TX ACTIVE) message out over WCI2, if the corresponding
    policy bit is enabled.

    @return
    void
*/
/*===========================================================================*/
void coex_prep_and_send_wci2_type7(
  uint32 event_mask,
  cxm_tech_type tech
)
{
  wci2_msg_type_s t7_msg;
  uint64          policy;
  /*-----------------------------------------------------------------------*/
  CXM_MSG_1( HIGH, "Request to prep & send wci2 type7 for event mask %d",
             event_mask );
  policy = coex_wwan_state_info[tech].plcy_parms.tech_policy;
  if( COEX_SYS_ENABLED(CXM_SYS_BHVR_WCI2_DATA) &&
      (COEX_PCM_SEND_WCI2_TYPE7_MDM_CONN_STATE_V01 & policy ||
       COEX_PCM_SEND_WCI2_TYPE7_MDM_PWR_STATE_V01  & policy ||
       COEX_PCM_SEND_WCI2_TYPE7_MDM_TX_ANT_SEL_V01 & policy) )
  {
    t7_msg.type = WCI2_TYPE7;

    /* copy over previously sent state */
    t7_msg.data.type7_wwan_state.wwan_tx_active     = coex_wci2_t7_state.wwan_tx_active;
    t7_msg.data.type7_wwan_state.wwan_tx_pwr_active = coex_wci2_t7_state.wwan_tx_pwr_active;
    t7_msg.data.type7_wwan_state.tx_ant_sel         = coex_wci2_t7_state.tx_ant_sel;

    /* handle wci2 t7[rrc_c] state */
    if( (event_mask & COEX_WCI2_T7_CONN_ST_EVNT_MSK) != 0 )
    {
      t7_msg.data.type7_wwan_state.wwan_tx_active = 
          (coex_state.conn_tech_mask != 0) ? TRUE : FALSE;
      coex_wci2_t7_state.wwan_tx_active = t7_msg.data.type7_wwan_state.wwan_tx_active;
    }

    /* handle wci2 t7[p_q] state */
    if ( (event_mask & COEX_WCI2_T7_PWR_ST_EVNT_MSK) != 0 )
    {
      t7_msg.data.type7_wwan_state.wwan_tx_pwr_active = 
        (coex_state.tx_pwr_tech_mask != 0) ? TRUE : FALSE;
      coex_wci2_t7_state.wwan_tx_pwr_active = t7_msg.data.type7_wwan_state.wwan_tx_pwr_active;
    }

    /* handle wci2 t7[ant_tx] state */
    if ( (event_mask & COEX_WCI2_T7_TX_ANT_SEL_EVNT_MSK) != 0 )
    {
      t7_msg.data.type7_wwan_state.tx_ant_sel =
        (coex_state.tx_ant_sel == CXM_TX_ANT_SEL_Y) ? TRUE : FALSE;
      coex_wci2_t7_state.tx_ant_sel = t7_msg.data.type7_wwan_state.tx_ant_sel;
    }

    /* since various state bits of type7 transitioning to off may be delayed 
     * by holdoff timer for filtering, we may have to power back on the UART
     * explicitly. This is because the coex npa voting code relies on the modem 
     * connection state, and a delayed type7 does not reflect the true 
     * Modem CONN state or Modem Tx power state. */
    if( coex_state.uart_en_state == FALSE )
    {
      CXM_MSG_0( MED, "Powering on/off UART for type 7" );
      wci2_client_enable_uart( coex_state.uart_handle, TRUE );
      wci2_send_msg( &t7_msg );
      wci2_client_enable_uart( coex_state.uart_handle, FALSE );
    }
    else
    {
      wci2_send_msg( &t7_msg );
    }
    CXM_MSG_3( HIGH, "Sent wci2 type7 data: wwan active %d, wwan tx active %d, tx ant %d",
               t7_msg.data.type7_wwan_state.wwan_tx_active,
               t7_msg.data.type7_wwan_state.wwan_tx_pwr_active,
               t7_msg.data.type7_wwan_state.tx_ant_sel );
    CXM_TRACE( 0, 0, 0, 0, CXM_LOG_PKT_EN, CXM_LOG_MDM_STATE );
  }

  return;
}

/*=============================================================================

  FUNCTION:  coex_check_wci2_type7_state

=============================================================================*/
/*!
    @brief
    Helper function to check current type7 state & decide to send it

    @return void
*/
/*===========================================================================*/
void coex_check_wci2_type7_state (
  uint32           old_tech_tx_active_mask,
  uint32           old_tech_tx_power_mask,
  cxm_tx_ant_sel_e old_tx_ant_sel,
  uint32           event_mask
)
{
  uint32 wci2_t7_msk = 0x00;
  /*-----------------------------------------------------------------------*/
  /* if TX On, see if we need to power on UART to send type 7 */
  coex_manage_uart_npa_vote();

  CXM_MSG_7( HIGH, "Check type7-[event %d][conn old:%d new:%d][tx_pwr old:%d new:%d][tx_ant old:%d new:%d]",
             event_mask,
             old_tech_tx_active_mask,
             coex_state.conn_tech_mask,
             old_tech_tx_power_mask,
             coex_state.tx_pwr_tech_mask,
             old_tx_ant_sel,
             coex_state.tx_ant_sel );

  /* handle wci2 type7[rrc_c] processing */
  if( (COEX_WCI2_T7_CONN_ST_EVNT_MSK & event_mask) &&
      (COEX_PCM_SEND_WCI2_TYPE7_MDM_CONN_STATE_V01 &
       coex_wwan_state_info[CXM_TECH_LTE].plcy_parms.tech_policy) )
  {
    if( (old_tech_tx_active_mask == 0 && coex_state.conn_tech_mask != 0) ||
        (old_tech_tx_active_mask != 0 && coex_state.conn_tech_mask == 0) )
    {
      wci2_t7_msk |= COEX_WCI2_T7_CONN_ST_EVNT_MSK;
    }
  }

  /* handle wci2 type7[p_q] processing */
  if( (COEX_WCI2_T7_PWR_ST_EVNT_MSK & event_mask) &&
      (COEX_PCM_SEND_WCI2_TYPE7_MDM_PWR_STATE_V01 &
       coex_wwan_state_info[CXM_TECH_LTE].plcy_parms.tech_policy) )
  {
    if( (old_tech_tx_power_mask == 0 && coex_state.tx_pwr_tech_mask != 0) ||
        (old_tech_tx_power_mask != 0 && coex_state.tx_pwr_tech_mask == 0) )
    {
      wci2_t7_msk |= COEX_WCI2_T7_PWR_ST_EVNT_MSK;
    }
  }

  /* handle wci2 type7[ant_tx] processing */
  if( (COEX_WCI2_T7_TX_ANT_SEL_EVNT_MSK & event_mask) &&
    (COEX_PCM_SEND_WCI2_TYPE7_MDM_TX_ANT_SEL_V01 &
     coex_wwan_state_info[CXM_TECH_LTE].plcy_parms.tech_policy) )
  {
    /* React only to transitions
       - Unused --> X or Y
       - X --> Y
       - Y --> X */
    if( (old_tx_ant_sel == CXM_TX_ANT_SEL_UNUSED && 
         coex_state.tx_ant_sel != CXM_TX_ANT_SEL_UNUSED) ||
        (old_tx_ant_sel == CXM_TX_ANT_SEL_X && 
         coex_state.tx_ant_sel == CXM_TX_ANT_SEL_Y) ||
        (old_tx_ant_sel == CXM_TX_ANT_SEL_Y && 
         coex_state.tx_ant_sel == CXM_TX_ANT_SEL_X) )
    {
      wci2_t7_msk |= COEX_WCI2_T7_TX_ANT_SEL_EVNT_MSK;
    }
  }

  if( wci2_t7_msk )
  {
    coex_prep_and_send_wci2_type7( wci2_t7_msk, CXM_TECH_LTE );
  }
  /* TX now off, power off UART if needed after sending the type 7 */
  coex_manage_uart_npa_vote();

  return;
}

/*=============================================================================

  FUNCTION:  coex_prep_and_send_lte_policy_msg

=============================================================================*/
/*!
    @brief
    Function to prepare & send the coex policies currently in use for LTE
    for all carriers

    @detail
    This message is LTE-specific because of LTE carrier aggregation, 
    the active_config_v02 used by LTE is not compatible with the 
    active_config_v01 used by other techs.

    @return
    errno_enum_type
*/
/*===========================================================================*/
errno_enum_type coex_prep_and_send_lte_policy_msg( void )
{
  errno_enum_type                   retval = E_SUCCESS;
  cxm_coex_active_policy_lte_ind_s  lte_msg;
  int32                             tx_thresh;
  uint32                            rb_alpha;
  uint32                            tx_pwr_alpha;
  cxm_active_config_v02_s           *cfg;
  coex_conflict_mdm_params_type_v01 *confl_params;
  coex_wwan_tech_info               *lte_info;
  int32                             cc;
  /*-----------------------------------------------------------------------*/
  memset( &lte_msg, 0, sizeof( cxm_coex_active_policy_lte_ind_s ) );
  cfg = &lte_msg.active_cfg;
  lte_info = &coex_wwan_state_info[CXM_TECH_LTE];

  /* first, copy carrier-specific information */
  for( cc = 0; cc < CXM_CARRIER_MAX; cc++ )
  {
    /* copy info from policy info to msgr msg */
    cfg->carrier_cfg[cc].active_cxm_policy = 
      (cxm_policy_t)lte_info->carrier[cc].policy;

    /* tech-global policy info should go only in the Primary CC field */
    if( cc == CXM_CARRIER_PCC )
    {
      cfg->carrier_cfg[cc].active_cxm_policy |= lte_info->plcy_parms.tech_policy;
    }
    else
    {
      cfg->carrier_cfg[cc].active_cxm_policy &= ~COEX_TECH_GLOBAL_POLICY_MASK;
    }

    /* if CXM function to assert TX active in advance is enabled, do not send 
       TX active policy to FW and send type 6 policy */
    if ( COEX_SYS_ENABLED(CXM_SYS_BHVR_CXM_ASSERTS_TX_ADV) )
    {
      cfg->carrier_cfg[cc].active_cxm_policy &= 
        ~CXM_POLICY_WCI2_OUTGNG_TYPE6_TX_ADV_NOTICE;
      if ( cfg->carrier_cfg[cc].active_cxm_policy & CXM_POLICY_TOGGLE_TX_ACTIVE )
      {
        cfg->carrier_cfg[cc].active_cxm_policy &= 
          ~CXM_POLICY_TOGGLE_TX_ACTIVE;
      }
    }
  }

  /* port mapping is not per-carrier */
  if ( COEX_SYS_ENABLED(CXM_SYS_BHVR_SMEM_DATA) )
  {
    cfg->active_port_mapping = coex_smem_port_mapping;
  }
  else
  {
    cfg->active_port_mapping = coex_wci2_port_mapping;
  }

  if ( COEX_SYS_ENABLED(CXM_SYS_BHVR_QMI_POLICY_CONTROL) )
  {
    /* until IDL is updated, QMI Policy only supports primary carrier */
    cxm_float_to_signed_Q8( 
      lte_info->carrier[CXM_CARRIER_PCC].plcy_parms.power_threshold, &tx_thresh);
    cxm_float_to_unsigned_Q8( coex_plcy_parms.rb_filter_alpha, &rb_alpha );
    cxm_float_to_unsigned_Q16( coex_plcy_parms.t7_pwr_alpha, &tx_pwr_alpha );
    cfg->carrier_cfg[CXM_CARRIER_PCC].tech_tx_thlds.tx_pwr_thld = (int16) tx_thresh;
    cfg->carrier_cfg[CXM_CARRIER_PCC].tech_tx_thlds.tx_bw_thld = 
      lte_info->carrier[CXM_CARRIER_PCC].plcy_parms.rb_threshold;
    cfg->wcn_txfrmdnl_params.max_continuous_allowed_frame_denials = 
      (uint32) lte_info->plcy_parms.tx_continuous_subframe_denials_threshold;
    cfg->wcn_txfrmdnl_params.frame_denial_params.max_allowed_frame_denials = 
      (uint32) lte_info->plcy_parms.tx_subrame_denial_params.max_allowed_frame_denials;
    cfg->wcn_txfrmdnl_params.frame_denial_params.frame_denial_window =
      (uint32) lte_info->plcy_parms.tx_subrame_denial_params.frame_denial_window;
    lte_msg.tx_bw_filter_alpha = rb_alpha;
    lte_msg.tx_pwr_filter_params.filter_alpha = tx_pwr_alpha;
    lte_msg.tx_pwr_filter_params.threshold = coex_plcy_parms.t7_pwr_thresh;
  }
  if( COEX_SYS_ENABLED(CXM_SYS_BHVR_VICTIM_TABLE) )
  {
    /* fill out the per-carrier info */
    for( cc = CXM_CARRIER_PCC; cc < CXM_CARRIER_MAX; cc++ )
    {
      if( lte_info->carrier[cc].active_confl != NULL )
      {
        confl_params = &lte_info->carrier[cc].active_confl->mdm_params;
        cfg->carrier_cfg[cc].tech_tx_thlds.tx_pwr_thld = 
          confl_params->wcn_prio.tx_thlds.tx_pwr_thld;
        cfg->carrier_cfg[cc].tech_tx_thlds.tx_bw_thld = 
          confl_params->wcn_prio.tx_thlds.tx_bw_thld;
      }
    }

    /* WCN Prio params are not per carrier -- use primary as tech-global */
    /* TODO: consider modifying interface to support WCN Prio params per CC */
    /* for now, pick the first one with valid params */
    for( cc = CXM_CARRIER_PCC; cc < CXM_CARRIER_MAX; cc++ )
    {
      if( lte_info->carrier[cc].active_confl != NULL )
      {
        confl_params = &lte_info->carrier[cc].active_confl->mdm_params;
        cfg->wcn_txfrmdnl_params.max_continuous_allowed_frame_denials = 
          confl_params->wcn_prio.frame_denials.max_cont_denials;
        cfg->wcn_txfrmdnl_params.frame_denial_params.frame_denial_window = 
          confl_params->wcn_prio.frame_denials.duty_cycle_params.frame_denial_window;
        cfg->wcn_txfrmdnl_params.frame_denial_params.max_allowed_frame_denials = 
          confl_params->wcn_prio.frame_denials.duty_cycle_params.max_allowed_frame_denials;
        break;
      }
    }

    /* RB filter alpha (NV-global) */
    cxm_float_to_unsigned_Q8(coex_params.rb_filter_alpha, &rb_alpha );
    lte_msg.tx_bw_filter_alpha = rb_alpha;
    /* TODO: add tx pwr filtering parameters to NV based solution */
  }

  /* if sinr or bler are active we need lte subframe marker. */
  if ( (coex_metrics_lte_bler_started || lte_info->metrics_started) &&
        COEX_SYS_ENABLED(CXM_SYS_BHVR_WCI2_DATA) )
  {
    lte_msg.active_cfg.dynamic_sys_bhvr |= CXM_SYS_BHVR_TRIGGER_LTE_SUBFR_MARKER;
  }

  retval = cxm_msgr_send_msg( &lte_msg.msg_hdr, 
                              MCS_CXM_COEX_ACTIVE_POLICY_LTE_IND, 
                              sizeof(cxm_coex_active_policy_lte_ind_s) );

  CXM_MSG_3( HIGH, "Active (policy) msg sent, tech=LTE, masks: PCC=%x SCC0=%x SCC1=%x", 
             cfg->carrier_cfg[CXM_CARRIER_PCC].active_cxm_policy, 
             cfg->carrier_cfg[CXM_CARRIER_SCC_0].active_cxm_policy, 
             cfg->carrier_cfg[CXM_CARRIER_SCC_1].active_cxm_policy );
  CXM_TRACE(0, 0, 0, 0, CXM_LOG_PKT_EN, CXM_LOG_LTE_POLICY_INFO);

  return retval;
}

/*=============================================================================

  FUNCTION:  coex_prep_and_send_policy_msg

=============================================================================*/
/*!
    @brief
    Function to prepare & send the coex policy currently in use

    @detail
    This function currently is can be called from either within the actual
    set policy REQ message or when sending the WWAN state IND msg

    @return
    qmi_csi_error
*/
/*===========================================================================*/
errno_enum_type coex_prep_and_send_policy_msg (
  cxm_tech_type tech
)
{
  errno_enum_type                   retval = E_SUCCESS;
  cxm_coex_active_policy_ind_s      tech_msg;
  uint32                            log_code = 0;
  msgr_umid_type                    tech_umid = 0;
  cxm_active_config_v01_s           *cfg;
  coex_conflict_mdm_params_type_v01 *confl_params;
  /*-----------------------------------------------------------------------*/
  if ( COEX_SYS_ENABLED(CXM_SYS_BHVR_CXM_SENDS_POLICY) )
  {
    if ( tech == CXM_TECH_LTE )
    {
      retval = coex_prep_and_send_lte_policy_msg();
      return retval;
    }
    else
    {
      memset( &tech_msg, 0, sizeof( cxm_coex_active_policy_ind_s ) );
      cfg = &tech_msg.active_config;
    }

    /* copy info from policy info to msgr msg */
    cfg->active_cxm_policy = 
      (cxm_policy_t)coex_wwan_state_info[tech].carrier[CXM_CARRIER_PCC].policy;

    /* if CXM function to assert TX active in advance is enabled, do not send 
       TX active policy to FW and send type 6 policy */
    if ( COEX_SYS_ENABLED(CXM_SYS_BHVR_CXM_ASSERTS_TX_ADV) )
    {
      if ( cfg->active_cxm_policy & CXM_POLICY_TOGGLE_TX_ACTIVE )
      {
        cfg->active_cxm_policy &= ~CXM_POLICY_TOGGLE_TX_ACTIVE;
        cfg->active_cxm_policy |= CXM_POLICY_WCI2_OUTGNG_TYPE6_TX_ADV_NOTICE;
      }
      else
      {
        cfg->active_cxm_policy &= ~CXM_POLICY_WCI2_OUTGNG_TYPE6_TX_ADV_NOTICE;
      }
    }

    if ( COEX_SYS_ENABLED(CXM_SYS_BHVR_SMEM_DATA) )
    {
      if ( tech == CXM_TECH_LTE )
      {
        cfg->active_port_mapping = coex_smem_port_mapping;
      }
      else
      {
        cfg->active_port_mapping = coex_smem_v2_port_mapping;
      }
    }
    else
    {
      cfg->active_port_mapping = coex_wci2_port_mapping;
    }
    if ( COEX_SYS_ENABLED(CXM_SYS_BHVR_VICTIM_TABLE) && 
         coex_wwan_state_info[tech].carrier[CXM_CARRIER_PCC].active_confl != NULL )
    {
      confl_params = 
        &coex_wwan_state_info[tech].carrier[CXM_CARRIER_PCC].active_confl->mdm_params;
      cfg->tech_tx_thlds.tx_pwr_thld = confl_params->wcn_prio.tx_thlds.tx_pwr_thld;
      cfg->tech_tx_thlds.tx_bw_thld = confl_params->wcn_prio.tx_thlds.tx_bw_thld;
      cfg->wcn_txfrmdnl_params.max_continuous_allowed_frame_denials = 
        confl_params->wcn_prio.frame_denials.max_cont_denials;
      cfg->wcn_txfrmdnl_params.frame_denial_params.frame_denial_window = 
        confl_params->wcn_prio.frame_denials.duty_cycle_params.frame_denial_window;
      cfg->wcn_txfrmdnl_params.frame_denial_params.max_allowed_frame_denials = 
        confl_params->wcn_prio.frame_denials.duty_cycle_params.max_allowed_frame_denials;
    }

    if ( tech == CXM_TECH_TDSCDMA || tech == CXM_TECH_GSM1 || 
         tech == CXM_TECH_GSM2    || tech == CXM_TECH_GSM3 )
    {
      switch ( tech )
      {
        case CXM_TECH_TDSCDMA:
          tech_umid = MCS_CXM_COEX_ACTIVE_POLICY_TDSCDMA_IND;
          log_code = CXM_LOG_TDS_POLICY_INFO;
          break;
        case CXM_TECH_GSM1:
          tech_umid = MCS_CXM_COEX_ACTIVE_POLICY_GSM1_IND;
          log_code = CXM_LOG_GSM1_POLICY_INFO;
          break;
        case CXM_TECH_GSM2:
          tech_umid = MCS_CXM_COEX_ACTIVE_POLICY_GSM2_IND;
          log_code = CXM_LOG_GSM2_POLICY_INFO;
          break;
        case CXM_TECH_GSM3:
          tech_umid = MCS_CXM_COEX_ACTIVE_POLICY_GSM3_IND;
          log_code = CXM_LOG_GSM3_POLICY_INFO;
          break;
        default:
          break;
      }

      retval = cxm_msgr_send_msg( &tech_msg.msg_hdr, 
                                  tech_umid, 
                                  sizeof(cxm_coex_active_policy_ind_s) 
                                );
    }

    CXM_MSG_2( HIGH, "Active (policy) msg sent, tech=%d, mask=%x", tech, 
               cfg->active_cxm_policy );
    CXM_TRACE(0, 0, 0, 0, CXM_LOG_PKT_EN, log_code );
  }

  return retval;
}

#ifdef FEATURE_COEX_USE_NV
/*=============================================================================

  FUNCTION:  coex_conflict_cb

=============================================================================*/
/*!
    @brief
    Notification of new list of active conflict indices

    @return
    void
*/
/*===========================================================================*/
void coex_conflict_cb( coex_confl_retval *confls )
{
  static boolean         avoid_lte_u = FALSE;
  int16                  index;
  uint16                 xl_row_num;
  cxm_carrier_e          cc;
  coex_wwan_tech_info   *info;
  uint32                 cc_pl_check_mask  = 0;
  uint32                 cc_pl_clear_mask  = 0;
  boolean                send_policy = FALSE;
  cxm_policy_t           policy;
  /*-----------------------------------------------------------------------*/
  info = &coex_wwan_state_info[confls->wwan_tech];

  /* reset tech-global policy, fill out again below */
  info->plcy_parms.tech_policy = 0;

  for( cc = CXM_CARRIER_PCC; cc < CXM_CARRIER_MAX; cc++ )
  {
    index = confls->active_index[cc];
    xl_row_num = confls->active_row_num[cc];

    if( index != COEX_CONFL_INVALID_INDEX )
    {
      CXM_ASSERT( index <= COEX_CONFL_INDEX_MAX && 
                  index >= COEX_CONFL_INDEX_MIN );
      policy = (cxm_policy_t)coex_victim_tbl[index].mdm_policy;
      info->plcy_parms.tech_policy |= policy & COEX_TECH_GLOBAL_POLICY_MASK;

      /* conflict is active for this carrier, see if it's the same.
       * If it is the same, don't need to do anything */
      if( index != info->carrier[cc].active_conf_index )
      {
        /* active conflict has changed, update info */
        CXM_MSG_5( HIGH, "NEW CONFLICT! Tech %d, cc %d: index=%d, old index=%d, xl row=%d", 
                   confls->wwan_tech, cc, index, 
                   info->carrier[cc].active_conf_index, xl_row_num );
        CXM_TRACE( CXM_TRC_COEX_CONFLICT_START, cc, confls->wwan_tech, index, 
                   CXM_TRC_EN );
        send_policy = TRUE;
        info->carrier[cc].policy = policy;
        /* keep track of the active conflict because we need to check RBs 
         * and Power thresholds before asserting TX active */
        info->carrier[cc].active_conf_index = index;
        info->carrier[cc].active_conf_row_num = xl_row_num;
        info->carrier[cc].active_confl = &coex_victim_tbl[index];

        /* power limiting only applies to LTE PCC for now */
        if( confls->wwan_tech == CXM_TECH_LTE )
        {
          /* Process all CCs that possibly changed power limits states 
           * in one shot at the end */

          /* If power limiting policy is ON, set the TX power limit */
          if( (policy & CXM_POLICY_ENFORCE_CTRLR_TX_PWR_LMT) )
          {
            info->plcy_parms.params.lte.controller_tx_power_limit = 
              coex_victim_tbl[index].mdm_params.pwr_lmt.power;

            /* If power limiting has been OFF, check power limiting conditions */
            if( coex_pwr_lmt_info[cc].controller_state == COEX_PWR_LMT_OFF ) 
            {
              /* controller power limiting now enforced. set power limiting conditions */
              cc_pl_check_mask |= COEX_CC_TO_MASK( cc );
              coex_plcy_parms.link_path_loss_threshold = coex_params.link_path_loss_threshold;
              coex_plcy_parms.filtered_rb_threshold = coex_params.filtered_rb_threshold;
              coex_pwr_lmt_info[cc].controller_state = COEX_PWR_LMT_PENDING;
              cxm_counter_event( CXM_CNT_CONTR_PWR_LMT_REQ, 
                                 info->plcy_parms.params.lte.controller_tx_power_limit );
            }
          }
          else
          {
            /* If power limiting policy is OFF, set the state */
            coex_pwr_lmt_info[cc].controller_state = COEX_PWR_LMT_OFF;
            cc_pl_clear_mask |= COEX_CC_TO_MASK( cc );
          }
        }
      }
    }

    /* if conflict was active but no longer is, need to clear policy */
    if( COEX_CONFL_INVALID_INDEX != info->carrier[cc].active_conf_index &&
        COEX_CONFL_INVALID_INDEX == index )
    {
      send_policy = TRUE;

      CXM_MSG_4( HIGH, "CONFLICT END! tech=%d, cc=%d, index=%d, row num=%d", 
                 confls->wwan_tech, cc, info->carrier[cc].active_conf_index,
                 info->carrier[cc].active_conf_row_num );
      CXM_TRACE( CXM_TRC_COEX_CONFLICT_END, cc, confls->wwan_tech, 
                 info->carrier[cc].active_conf_index, CXM_TRC_EN );

      /* Turn power limiting OFF if it is not already OFF */
      if( (info->carrier[cc].policy & CXM_POLICY_ENFORCE_CTRLR_TX_PWR_LMT) && 
          confls->wwan_tech == CXM_TECH_LTE )
      {
        coex_pwr_lmt_info[cc].controller_state = COEX_PWR_LMT_OFF;
        cc_pl_clear_mask |= COEX_CC_TO_MASK( cc );
      }

      /* Set tech carrier policy info */
      info->carrier[cc].policy = CXM_POLICY_NONE;
      info->carrier[cc].active_conf_index = index;
      info->carrier[cc].active_conf_row_num = xl_row_num;
      info->carrier[cc].active_confl = NULL;
    }
  }

  /* check if WLAN is active on 5GHz, requiring LTE-U avoidance */
  if( confls->wwan_tech == CXM_TECH_LTE )
  {
    if( avoid_lte_u != confls->tech_spec.lte.lteu_active )
    {
      send_policy = TRUE;
      avoid_lte_u = confls->tech_spec.lte.lteu_active;
      CXM_MSG_1( HIGH, "LTE-U conflict changed state: %d", avoid_lte_u );
    }

    /* enable LTE-U policy if LTE-U conflict is on */
    if( avoid_lte_u )
    {
      info->plcy_parms.tech_policy |= CXM_POLICY_LTE_U_CHANNEL_AVOIDANCE;
      CXM_MSG_0( LOW, "LTE-U channel avoidance policy ON" );
    }
  }

  if( send_policy )
  {
    /* Power limit states may have changed. Check if power limit needs to be set. 
     * condition check only needed for new power limits. Not needed for 
     * conflicts that used to have a limit but no longer do */
    coex_check_and_set_power_limit( cc_pl_clear_mask );
    coex_check_power_limiting_conditions( cc_pl_check_mask );

    if( COEX_SYS_ENABLED(CXM_SYS_BHVR_WCI2_DATA) ||
        COEX_SYS_ENABLED(CXM_SYS_BHVR_WCI2_CONTROL) ) 
    {
      coex_manage_uart_npa_vote();
    }
    coex_prep_and_send_policy_msg( confls->wwan_tech );
  }

  return;
}
#endif

/*=============================================================================

  FUNCTION:  coex_send_diag_policy

=============================================================================*/
/*!
    @brief
    Function to take policy from diag interface and send to LTE to
    simulate coex conditions.  Only policy is taken from diag...all supporting
    parameters stay at existing or default values.

    @detail

    @return
    none
*/
/*===========================================================================*/
void coex_send_diag_policy (
  uint64        policy,
  cxm_carrier_e carrier
)
{
  coex_wwan_tech_info *lte_info = &coex_wwan_state_info[CXM_TECH_LTE];
  /*-----------------------------------------------------------------------*/
  if( carrier >= CXM_CARRIER_MAX )
  {
    carrier = CXM_CARRIER_PCC;
  }
  lte_info->carrier[carrier].policy = (coex_policy_config_mask_v01) policy;
  lte_info->plcy_parms.tech_policy  = policy & COEX_TECH_GLOBAL_POLICY_MASK;
  coex_prep_and_send_policy_msg( CXM_TECH_LTE );

  return;
}  /* coex_send_diag_policy */

/*=============================================================================

  FUNCTION:  coex_prep_and_send_wwan_state_ind

=============================================================================*/
/*!
    @brief
    Function to prepare & send the WWAN STATE info INDICATION message

    @detail
    This function currently is can be called from either within the actual
    send out IND message or as a follow on to the enable REQ for the WWAN
    STATE info IND by any client
 
    @return
    qmi_csi_error
*/
/*===========================================================================*/
static qmi_csi_error coex_prep_and_send_wwan_state_ind (
   boolean always_send
)
{
  qmi_csi_error               retval = QMI_CSI_NO_ERR;
  errno_enum_type             temp_retval = E_SUCCESS;
  /* static variables initialized to first time */
  /*used to determine if lte has just become active*/
  static boolean              prev_lte_state_inactive = TRUE;
  coex_wwan_state_ind_msg_v01 indication;
  uint8                       i, band_index;
  coex_wwan_tech_info        *tech_info;
  cxm_carrier_e               cc;
  uint32                      cc_pl_mask = 0;
  /*-----------------------------------------------------------------------*/
  /* Reset the memory available for the IND message */
  memset( &indication, 0, sizeof( coex_wwan_state_ind_msg_v01 ) );

  /*-----------------------------------------------------------------------*/
  /* LTE STATE INFO */
  /*-----------------------------------------------------------------------*/
  /* if band info is sent for this tech, is valid to add to the QMI msg */
  tech_info = &coex_wwan_state_info[CXM_TECH_LTE];
  if (tech_info->state_data.num_link_info_sets != 0) 
  {
    /* fill in the old lte_band_info message to support backwards compatibility */
    for ( i = 0; i < tech_info->state_data.num_link_info_sets; i++ ) 
    {
      /* if an entry in our state array matches UL and we havent already 
         encountered one that does, fill in the ul lte info */
      if ( indication.lte_band_info.ul_band.freq == 0 &&
           tech_info->state_data.link_list[i].direction == CXM_LNK_DRCTN_UL && 
           (!coex_band_filter_info.filter_on || tech_info->tech_band_filter[i] == TRUE) ) 
      {
        /* old bandwidth is in Mhz */
        indication.lte_band_info.ul_band.bandwidth = 
          tech_info->state_data.link_list[i].bandwidth / MHZ_HZ_CONVERSION;
        indication.lte_band_info.ul_band.freq = 
          tech_info->state_data.link_list[i].frequency / MHZ_KHZ_CONVERSION;
      }
      /* if an entry in our state array matches DL and we havent already 
         encountered one that does, fill in the dl lte info */
      if ( indication.lte_band_info.dl_band.freq == 0 &&
           tech_info->state_data.link_list[i].direction == CXM_LNK_DRCTN_DL &&
           (!coex_band_filter_info.filter_on || tech_info->tech_band_filter[i] == TRUE)) 
      {
        indication.lte_band_info.dl_band.bandwidth = 
          tech_info->state_data.link_list[i].bandwidth / MHZ_HZ_CONVERSION;
        indication.lte_band_info.dl_band.freq = 
          tech_info->state_data.link_list[i].frequency / MHZ_KHZ_CONVERSION;
      }
    }

    /* Fill in the LTE band array */
    band_index = 0;
    /* Fill in the LTE band array */
    for ( i = 0; i < tech_info->state_data.num_link_info_sets && 
                 band_index < COEX_WWAN_MAX_BANDS_PER_TECH_V01; i++) 
    {
      if (!coex_band_filter_info.filter_on || tech_info->tech_band_filter[i] == TRUE)
      {
        indication.lte_band_info_set[band_index].bandwidth = 
          tech_info->state_data.link_list[i].bandwidth;
        indication.lte_band_info_set[band_index].freq = 
          tech_info->state_data.link_list[i].frequency;
        indication.lte_band_info_set[band_index].direction = 
          tech_info->state_data.link_list[i].direction;

        /* Fill out LTE carrier-specific info. Carrier array info should match
         * indexes with LTE band info set above */
        if( (tech_info->state_data.link_list[i].type & CXM_LNK_TYPE_CA1) != 0 )
        {
          indication.lte_carrier_info_set[band_index].id = COEX_CARRIER_SECONDARY_0_V01;
          cc = CXM_CARRIER_SCC_0;
        }
        else if( (tech_info->state_data.link_list[i].type & CXM_LNK_TYPE_CA2) != 0 )
        {
          indication.lte_carrier_info_set[band_index].id = COEX_CARRIER_SECONDARY_1_V01;
          cc = CXM_CARRIER_SCC_1;
        }
        else
        {
          indication.lte_carrier_info_set[band_index].id = COEX_CARRIER_PRIMARY_V01;
          cc = CXM_CARRIER_PCC;
        }

        /* if this carrier is LTE TDD, fill out TDD info */
        if( tech_info->state_data.cc_params[cc].lte_params.frame_structure == 
            LTE_L1_FRAME_STRUCTURE_FS2 )
        {
          /* save LTE frame structure (FDD/TDD) */
          indication.lte_carrier_info_set[band_index].operating_dim = 
            COEX_TECH_OPERATING_DIMENSION_TDD_V01;

          /* Update LTE TDD frame offset info (if provided) */
          indication.lte_carrier_info_set[band_index].tdd_info.frame_offset = 
            tech_info->state_data.cc_params[cc].lte_params.frame_offset;
          /* instead of reporting LTE ML1's invalid frame offset value of -1 (0xFFFFFFFF)
               report that of nominal TA of 0, therefore frame offset of 2000 uSecs */
          if( indication.lte_carrier_info_set[band_index].tdd_info.frame_offset == 0xFFFFFFFF )
          {
            indication.lte_carrier_info_set[band_index].tdd_info.frame_offset = 2000;
          }

          /* Update LTE TDD config (if provided) */
          indication.lte_carrier_info_set[band_index].tdd_info.tdd_config = 
            (coex_lte_tdd_config_enum_v01)
            tech_info->state_data.cc_params[cc].lte_params.tdd_config;

          /* Update LTE TDD SSP (if provided) */
          indication.lte_carrier_info_set[band_index].tdd_info.subframe_config = 
            (coex_lte_tdd_subframe_config_enum_v01)
            tech_info->state_data.cc_params[cc].lte_params.ssp;

          /* Update LTE TDD DL CP config (if provided) */
          indication.lte_carrier_info_set[band_index].tdd_info.dl_config = 
            (coex_lte_tdd_link_config_enum_v01)
            tech_info->state_data.cc_params[cc].lte_params.dl_cp;

          /* Update LTE TDD UL CP config (if provided) */
          indication.lte_carrier_info_set[band_index].tdd_info.ul_config = 
            (coex_lte_tdd_link_config_enum_v01)
            tech_info->state_data.cc_params[cc].lte_params.ul_cp;
        }
        else
        {
          indication.lte_carrier_info_set[band_index].operating_dim = 
            COEX_TECH_OPERATING_DIMENSION_FDD_V01;
        }
        indication.lte_carrier_info_set[band_index].frame_boundary_offset = 
          tech_info->state_data.cc_params[cc].lte_params.cc_frame_offset;

        band_index++;
      }
    }

    if (band_index > 0) 
    {
      indication.lte_band_info_valid = TRUE;
      indication.lte_band_info_set_valid = TRUE;
      indication.lte_band_info_set_len = band_index;
      indication.lte_carrier_info_set_valid = TRUE;
      indication.lte_carrier_info_set_len = band_index;
    }

    /* Check if we are in LTE TDD and fill in the appropriate fields. 
     * If all band information has been filtered out (i.e. length is 0), 
     * dont send out other LTE params */
    for( i = 0; i < indication.lte_carrier_info_set_len; i++ )
    {
      if( indication.lte_carrier_info_set[i].operating_dim == 
          COEX_TECH_OPERATING_DIMENSION_TDD_V01 && 
          indication.lte_carrier_info_set[i].id == 
          COEX_CARRIER_PRIMARY_V01 )
      {
        indication.lte_tdd_info_valid = TRUE;
        indication.lte_tdd_info = indication.lte_carrier_info_set[i].tdd_info;
        /* instead of reporting LTE ML1's invalid frame offset value of -1 (0xFFFFFFFF)
             report that of nominal TA of 0, therefore frame offset of 2000 uSecs */
        if( indication.lte_tdd_info.frame_offset == 0xFFFFFFFF )
        {
          indication.lte_tdd_info.frame_offset = 2000;
        }
      }
    }

    /* if lte comes up for first time: 
      - resend policy msg 
      - check if power limit changed */
    if( prev_lte_state_inactive )
    {
      prev_lte_state_inactive = FALSE;
      temp_retval = coex_prep_and_send_policy_msg( CXM_TECH_LTE );
      CXM_ASSERT( temp_retval == E_SUCCESS );

      /* ensure power limits are up to date */
      coex_check_and_set_power_limit( COEX_CC_MASK_ALL );
      for( cc = CXM_CARRIER_PCC; cc < CXM_CARRIER_MAX; cc++ )
      {
        if( coex_pwr_lmt_info[cc].controller_state == COEX_PWR_LMT_PENDING ||
            coex_pwr_lmt_info[cc].wci2_state       == COEX_PWR_LMT_PENDING )
        {
          cc_pl_mask |= COEX_CC_TO_MASK(cc);
        }
      } /* carrier loop for power limiting */
      coex_check_power_limiting_conditions( cc_pl_mask );
    }
  } /*end of 'if lte is valid'*/
  else
  {
    prev_lte_state_inactive = TRUE;
  }

  /*-----------------------------------------------------------------------*/
  /* TDSCDMA STATE INFO */
  /*-----------------------------------------------------------------------*/
  tech_info = &coex_wwan_state_info[CXM_TECH_TDSCDMA];
  if (tech_info->state_data.num_link_info_sets != 0) 
  {
    band_index = 0;
    /* Fill in the TDS band array */
    for (i = 0; i < tech_info->state_data.num_link_info_sets && 
                band_index < COEX_WWAN_MAX_BANDS_PER_TECH_V01; i++) 
    {
      if (!coex_band_filter_info.filter_on || tech_info->tech_band_filter[i] == TRUE)
      {
        indication.tdscdma_band_info_set[band_index].bandwidth = 
          tech_info->state_data.link_list[i].bandwidth;
        indication.tdscdma_band_info_set[band_index].freq = 
          tech_info->state_data.link_list[i].frequency;
        indication.tdscdma_band_info_set[band_index].direction = 
          tech_info->state_data.link_list[i].direction;
        band_index++;
      }
    }

    if (band_index > 0) 
    {
      indication.tdscdma_band_info_set_valid = TRUE;
      indication.tdscdma_band_info_set_len = band_index; 
    }

  } /*end of if TDS is valid*/

  /*-----------------------------------------------------------------------*/
  /* GSM [1, 2, 3] STATE INFO */
  /*-----------------------------------------------------------------------*/
  tech_info = &coex_wwan_state_info[CXM_TECH_GSM1];
  if( tech_info->state_data.num_link_info_sets != 0 )
  {
    band_index = 0;
    /* Fill in the GSM1 band array */
    for ( i = 0; i < tech_info->state_data.num_link_info_sets && 
                 band_index < COEX_WWAN_MAX_BANDS_PER_TECH_V01; i++ )
    {
      if (!coex_band_filter_info.filter_on || tech_info->tech_band_filter[i] == TRUE)
      {
        indication.gsm_band_info_set[band_index].bandwidth = 
          tech_info->state_data.link_list[i].bandwidth;
        indication.gsm_band_info_set[band_index].freq = 
          tech_info->state_data.link_list[i].frequency;
        indication.gsm_band_info_set[band_index].direction = 
          tech_info->state_data.link_list[i].direction;
        band_index++;
      }
    }

    if (band_index > 0) 
    {
      indication.gsm_band_info_set_valid = TRUE;
      indication.gsm_band_info_set_len = band_index; 
    }
  } /* end of if GSM1 is valid */

  tech_info = &coex_wwan_state_info[CXM_TECH_GSM2];
  if( tech_info->state_data.num_link_info_sets != 0 )
  {  
    band_index = 0;
    /* Fill in the GSM2 band array */
    for ( i = 0; i < tech_info->state_data.num_link_info_sets && 
                 band_index < COEX_WWAN_MAX_BANDS_PER_TECH_V01; i++ ) 
    {
      if (!coex_band_filter_info.filter_on || tech_info->tech_band_filter[i] == TRUE)
      {
        indication.gsm2_band_info_set[band_index].bandwidth = 
          tech_info->state_data.link_list[i].bandwidth;
        indication.gsm2_band_info_set[band_index].freq = 
          tech_info->state_data.link_list[i].frequency;
        indication.gsm2_band_info_set[band_index].direction = 
          tech_info->state_data.link_list[i].direction;
        band_index++;
      }
    }

    if (band_index > 0) 
    {
      indication.gsm2_band_info_set_valid = TRUE;
      indication.gsm2_band_info_set_len = band_index; 
    }
  } /* end of if GSM2 is valid */

  tech_info = &coex_wwan_state_info[CXM_TECH_GSM3];
  if( tech_info->state_data.num_link_info_sets != 0 )
  {
    band_index = 0;
    /* Fill in the GSM3 band array */
    for ( i = 0; i < tech_info->state_data.num_link_info_sets && 
                 band_index < COEX_WWAN_MAX_BANDS_PER_TECH_V01; i++ )
    {
      if (!coex_band_filter_info.filter_on || tech_info->tech_band_filter[i] == TRUE)
      {
        indication.gsm3_band_info_set[band_index].bandwidth = 
          tech_info->state_data.link_list[i].bandwidth;
        indication.gsm3_band_info_set[band_index].freq = 
          tech_info->state_data.link_list[i].frequency;
        indication.gsm3_band_info_set[band_index].direction = 
          tech_info->state_data.link_list[i].direction;
        band_index++;
      }
    }

    if (band_index > 0) 
    {
      indication.gsm3_band_info_set_valid = TRUE;
      indication.gsm3_band_info_set_len = band_index; 
    }
  } /* end of if GSM3 is valid */

  /*-----------------------------------------------------------------------*/
  /* ONEX STATE INFO */
  /*-----------------------------------------------------------------------*/
  tech_info = &coex_wwan_state_info[CXM_TECH_ONEX];
  if (tech_info->state_data.num_link_info_sets != 0) 
  {
    band_index = 0;
    /* Fill in the ONEX band array */
    for ( i = 0; i < tech_info->state_data.num_link_info_sets && 
                 band_index < COEX_WWAN_MAX_BANDS_PER_TECH_V01; i++ )
    {
      if (!coex_band_filter_info.filter_on || tech_info->tech_band_filter[i] == TRUE)
      {
        indication.onex_band_info_set[band_index].bandwidth = 
          tech_info->state_data.link_list[i].bandwidth;
        indication.onex_band_info_set[band_index].freq = 
          tech_info->state_data.link_list[i].frequency;
        indication.onex_band_info_set[band_index].direction = 
          tech_info->state_data.link_list[i].direction;
        band_index++;
      }
    }

    if (band_index > 0) 
    {
      indication.onex_band_info_set_valid = TRUE;
      indication.onex_band_info_set_len = band_index; 
    }

  } /* end of if ONEX is valid */

  /*-----------------------------------------------------------------------*/
  /* HDR STATE INFO */
  /*-----------------------------------------------------------------------*/
  tech_info = &coex_wwan_state_info[CXM_TECH_HDR];
  if (tech_info->state_data.num_link_info_sets != 0) 
  {
    band_index = 0;
    /* Fill in the HDR band array */
    for ( i = 0; i < tech_info->state_data.num_link_info_sets && 
                 band_index < COEX_WWAN_MAX_BANDS_PER_TECH_V01; i++ )
    {
      if (!coex_band_filter_info.filter_on || tech_info->tech_band_filter[i] == TRUE)
      {
        indication.hdr_band_info_set[band_index].bandwidth = 
          tech_info->state_data.link_list[i].bandwidth;
        indication.hdr_band_info_set[band_index].freq = 
          tech_info->state_data.link_list[i].frequency;
        indication.hdr_band_info_set[band_index].direction = 
          tech_info->state_data.link_list[i].direction;
        band_index++;
      }
    }

    if (band_index > 0) 
    {
      indication.hdr_band_info_set_valid = TRUE;
      indication.hdr_band_info_set_len = band_index; 
    }

  } /* end of if HDR is valid */

  /*-----------------------------------------------------------------------*/
  /* WCDMA [1, 2] STATE INFO */
  /*-----------------------------------------------------------------------*/
  tech_info = &coex_wwan_state_info[CXM_TECH_WCDMA];
  if (tech_info->state_data.num_link_info_sets != 0) 
  {
    band_index = 0;
    /* Fill in the WCDMA band array */
    for ( i = 0; i < tech_info->state_data.num_link_info_sets && 
                 band_index < COEX_WWAN_MAX_BANDS_PER_TECH_V01; i++ )
    {
      if (!coex_band_filter_info.filter_on || tech_info->tech_band_filter[i] == TRUE)
      {
        indication.wcdma_band_info_set[band_index].bandwidth = 
          tech_info->state_data.link_list[i].bandwidth;
        indication.wcdma_band_info_set[band_index].freq = 
          tech_info->state_data.link_list[i].frequency;
        indication.wcdma_band_info_set[band_index].direction = 
          tech_info->state_data.link_list[i].direction;
        band_index++;
      }
    }

    if (band_index > 0)
    {
      indication.wcdma_band_info_set_valid = TRUE;
      indication.wcdma_band_info_set_len = band_index;
    }

  } /*end of if WCDMA is valid*/

  tech_info = &coex_wwan_state_info[CXM_TECH_WCDMA2];
  if (tech_info->state_data.num_link_info_sets != 0) 
  {
    band_index = 0;
    /* Fill in the WCDMA2 band array */
    for ( i = 0; i < tech_info->state_data.num_link_info_sets && 
                 band_index < COEX_WWAN_MAX_BANDS_PER_TECH_V01; i++ )
    {
      if (!coex_band_filter_info.filter_on || tech_info->tech_band_filter[i] == TRUE)
      {
        indication.wcdma2_band_info_set[band_index].bandwidth = 
          tech_info->state_data.link_list[i].bandwidth;
        indication.wcdma2_band_info_set[band_index].freq = 
          tech_info->state_data.link_list[i].frequency;
        indication.wcdma2_band_info_set[band_index].direction = 
          tech_info->state_data.link_list[i].direction;
        band_index++;
      }
    }

    if (band_index > 0)
    {
      indication.wcdma2_band_info_set_valid = TRUE;
      indication.wcdma2_band_info_set_len = band_index;
    }

  } /*end of if WCDMA2 is valid*/

  /*-----------------------------------------------------------------------*/

  /* check if the indication is different from the last one */
  if( always_send || memcmp( &indication, &coex_wwan_state_ind, sizeof(indication) ) )
  {
    coex_wwan_state_ind = indication;
    /* Send the message via QMI */
    retval = mqcsi_conn_mgr_send_ind (
               MQCSI_COEX_SERVICE_ID,
               QMI_COEX_WWAN_STATE_IND_V01,
               &indication,
               sizeof ( coex_wwan_state_ind_msg_v01 )
             );

    CXM_TRACE( CXM_TRC_QMI_SND_WWAN_STATE_IND, retval, 0, 0,
               CXM_TRC_EN );

    CXM_MSG_1( HIGH, "Send WWAN state IND return %d ", retval );
  }

  return retval;
}

/*=============================================================================

  FUNCTION:  coex_throttle_condition_ind_masks

=============================================================================*/
/*!
    @brief
    Helper function to check if a QMI condition mask (for condition fail 
    or condition success indications) needs to be throttled or should be sent.

    @detail
    Only send the IND if it hasn't been sent within 1 second (throttled to 
    prevent memory depletion from sending too often

    @return throttle-adjusted mask, drop_count
*/
/*===========================================================================*/
uint64 coex_throttle_condition_ind_masks (
  coex_throttle_count_s *conditions, /* array of conditions for this mask */
  int                    num_conditions,
  uint64                 mask,
  uint32                *drop_count /* output - total drops in last second */
)
{
  timetick_type curr_time, time_elapsed;
  uint64        mask_to_send = 0;
  int           i;
  /*-----------------------------------------------------------------------*/
  /* check if we have to throttle (if timestamp is older than throttling 
   * period */
  CXM_ASSERT( NULL != drop_count && NULL != conditions );
  *drop_count = 0;

  curr_time = timetick_get();

  /* check each bit in the mask in turn */
  for( i = 0; i < num_conditions; i++ )
  {
    /* check if this condition bit is set in the mask */
    if( 0x01 & (mask >> i) )
    {
      /* bit set. check timestamp for last time this condition was
       * sent out to see if we need to throttle */
      time_elapsed = timetick_diff( conditions[i].last_sent_ts,
                                    curr_time, T_MSEC );
      if( time_elapsed > COEX_THROTTLE_QMI_CONDITION_IND_PERIOD_MS )
      {
        /* don't throttle. Send this condition and reset info */
        conditions[i].last_sent_ts = curr_time;
        conditions[i].drop_count   = 0;
        mask_to_send |= (0x01 << i);
      }
      else
      {
        /* this condition has been sent out before in this throttling
         * period, so don't send this one */
        conditions[i].drop_count++;
        *drop_count += conditions[i].drop_count;
      }

      CXM_MSG_2( LOW, "Condition IND mask bit %d: drop_cnt %d", 
                 i, conditions[i].drop_count );
    }
  }

  return mask_to_send;
}

/*=============================================================================

  FUNCTION:  coex_prep_and_send_condition_fail_ind

=============================================================================*/
/*!
    @brief
    Check if appropriate to send a QMI condition fail IND, and send it if so.

    @detail
    Only send the IND if it hasn't been sent within 1 second (throttled to 
    prevent memory depletion from sending too often

    @return void
*/
/*===========================================================================*/
void coex_prep_and_send_condition_fail_ind (
  coex_txfrmdnl_condition_failure_mask_v01 tx_subframe_denials_cond,
  coex_pwrlmt_condition_fail_mask_v01      controller_tx_pwrlmt_fail_cond,
  coex_pwrlmt_condition_fail_mask_v01      wci2_tx_pwrlmt_fail_cond
)
{
  coex_condition_fail_ind_msg_v01 fail_indication;
  qmi_csi_error                   qmi_retval = QMI_CSI_NO_ERR;
  boolean                         send_ind = FALSE;
  uint32                          drop_count;
  uint64                          mask_to_send = 0;
  /*-----------------------------------------------------------------------*/
  /* Reset the memory available for the IND message */
  memset( &fail_indication, 0, sizeof(coex_condition_fail_ind_msg_v01) );

  /* for each of three masks: if mask is empty, we don't need to worry 
   * about filling out or sending it in this indication */
  if( tx_subframe_denials_cond )
  {
    /* check if this is a unique failure in this throttling period */
    mask_to_send = coex_throttle_condition_ind_masks(
                      coex_throttle_ind_info.wcn_prio_fail,
                      COEX_WCN_PRIO_COND_FAIL_MASK_LEN,
                      tx_subframe_denials_cond,
                      &drop_count );

    if( mask_to_send )
    {
      /* this failure bit has not been set yet in this throttling period;
       * fill out the field in the indication to send */
      send_ind = TRUE;
      fail_indication.tx_subframe_denials_status_valid = TRUE;
      fail_indication.tx_subframe_denials_status = mask_to_send;
    }

    CXM_MSG_3( HIGH, "WCN Prio condition fail: this_mask=%x sent_mask=%x drop_cnt=%d",
               tx_subframe_denials_cond, mask_to_send, drop_count );

    CXM_TRACE( CXM_TRC_QMI_TX_SUBFRAME_DENIAL,
               tx_subframe_denials_cond,
               fail_indication.tx_subframe_denials_status,
               drop_count, CXM_TRC_EN );
  }

  if( controller_tx_pwrlmt_fail_cond )
  {
    /* check if this is a unique failure in this throttling period */
    mask_to_send = coex_throttle_condition_ind_masks(
                      coex_throttle_ind_info.contr_pwrlmt_fail,
                      COEX_CONTR_PWRLMT_COND_FAIL_MASK_LEN,
                      controller_tx_pwrlmt_fail_cond,
                      &drop_count );

   if( mask_to_send )
    {
      /* this failure bit has not been set yet in this throttling period;
       * fill out the field in the indication to send */
      send_ind = TRUE;
      fail_indication.controller_tx_pwrlmt_fail_cond_valid = TRUE;
      fail_indication.controller_tx_pwrlmt_fail_cond = mask_to_send;
    }

    CXM_TRACE( CXM_TRC_QMI_LTE_TX_PWR_LMT_FAIL,
               controller_tx_pwrlmt_fail_cond, 0, drop_count,
               CXM_TRC_EN );

    cxm_counter_event( CXM_CNT_CONTR_PWR_LMT_COND_FAIL, 
                       (uint32) controller_tx_pwrlmt_fail_cond );

    CXM_MSG_3( HIGH, "Contr Pwrlmt condition fail: this_mask=%x sent_mask=%x drop_cnt=%d", 
               controller_tx_pwrlmt_fail_cond, mask_to_send, drop_count );
  }

  if( wci2_tx_pwrlmt_fail_cond )
  {
    /* check if this is a unique failure in this throttling period */
    mask_to_send = coex_throttle_condition_ind_masks(
                      coex_throttle_ind_info.wci2_pwrlmt_fail,
                      COEX_WCI2_PWRLMT_COND_FAIL_MASK_LEN,
                      wci2_tx_pwrlmt_fail_cond,
                      &drop_count );

    if( mask_to_send )
    {
      /* this failure bit has not been set yet in this throttling period;
       * fill out the field in the indication to send */
      send_ind = TRUE;
      fail_indication.wci2_tx_pwrlmt_fail_cond_valid = TRUE;
      fail_indication.wci2_tx_pwrlmt_fail_cond = mask_to_send;
    }

    CXM_TRACE( CXM_TRC_QMI_LTE_TX_PWR_LMT_FAIL,
               0, wci2_tx_pwrlmt_fail_cond, drop_count, CXM_TRC_EN );

    cxm_counter_event( CXM_CNT_WCI2_PWR_LMT_COND_FAIL, 
                       (uint32) wci2_tx_pwrlmt_fail_cond );

    CXM_MSG_3( HIGH, "Wci2 Pwrlmt condition fail: this_mask=%x sent_mask=%x drop_cnt=%d", 
               wci2_tx_pwrlmt_fail_cond, mask_to_send, drop_count );
  }

  /* if indication changed from last time or it's been more than 1 second since
   * the last time the indication was sent, send this one */
  if( send_ind )
  {
    qmi_retval = mqcsi_conn_mgr_send_ind (
                   MQCSI_COEX_SERVICE_ID,
                   QMI_COEX_CONDITION_FAIL_IND_V01,
                   &fail_indication,
                   sizeof ( coex_condition_fail_ind_msg_v01 )
                 );
    CXM_ASSERT( QMI_CSI_NO_ERR == qmi_retval );
  }

  return;
}

/*=============================================================================

  FUNCTION:  coex_prep_and_send_condition_success_ind

=============================================================================*/
/*!
    @brief
    Check if appropriate to send a QMI condition succ IND, and send it if so.

    @detail
    Only send the IND if it hasn't been sent within 1 second (throttled to 
    prevent memory depletion from sending too often

    @return void
*/
/*===========================================================================*/
void coex_prep_and_send_condition_success_ind (
  coex_pwrlmt_condition_success_mask_v01 tx_pwrlmt_success_case
)
{
  coex_condition_success_ind_msg_v01 succ_indication;
  qmi_csi_error                      qmi_retval = QMI_CSI_NO_ERR;
  uint32                             drop_count;
  uint64                             mask_to_send;
  /*-----------------------------------------------------------------------*/
  if( tx_pwrlmt_success_case )
  {
    /* filter mask of all 'throttled' bits */
    mask_to_send = coex_throttle_condition_ind_masks(
                      coex_throttle_ind_info.pwrlmt_success,
                      COEX_PWRLMT_COND_SUCCESS_MASK_LEN,
                      tx_pwrlmt_success_case,
                      &drop_count );

    if( mask_to_send )
    {
      /* message not restricted from throttling, send it out */
      succ_indication.tx_pwrlmt_success_case_valid = TRUE;
      succ_indication.tx_pwrlmt_success_case = mask_to_send;

      qmi_retval = mqcsi_conn_mgr_send_ind (
                     MQCSI_COEX_SERVICE_ID,
                     QMI_COEX_CONDITION_SUCCESS_IND_V01,
                     &succ_indication,
                     sizeof ( coex_condition_success_ind_msg_v01 )
                   );
      CXM_ASSERT( QMI_CSI_NO_ERR == qmi_retval );
    }

    CXM_TRACE( CXM_TRC_QMI_LTE_TX_PWR_LMT_SUCC,
               tx_pwrlmt_success_case,
               mask_to_send,
               drop_count,
               CXM_TRC_EN );

    CXM_MSG_3( HIGH, "Power limit cond succ: real_mask=%x sent_mask=%x drop_cnt=%d", 
               tx_pwrlmt_success_case, mask_to_send, drop_count );
  }

  return;
}

/*=============================================================================

  FUNCTION:  coex_apply_tech_band_filter

=============================================================================*/
/*!
    @brief
    Function to apply band filtering for a specified tech

    @detail
    This function is called when a new tech update comes in or band filtering
    has just been set.
 
    @return
    void
*/
/*===========================================================================*/
void coex_apply_tech_band_filter (
   cxm_tech_type tech_id
)
{
  uint8 i,j;
  /*-----------------------------------------------------------------------*/
  /*if band filtering is ON, run the filter*/
  if (coex_band_filter_info.filter_on) 
  {
    for (i=0; i < coex_wwan_state_info[tech_id].state_data.num_link_info_sets; i++) 
    {

      /*if it is an UL band perform filtering with the UL filter*/
      if (coex_wwan_state_info[tech_id].state_data.link_list[i].direction & CXM_LNK_DRCTN_UL )
      {
        /*if there is no band filter in this direction, set filter to true*/
        if ( coex_band_filter_info.ul_len == 0 ) 
        {
          coex_wwan_state_info[tech_id].tech_band_filter[i] = FALSE;
        }

        for (j=0; j<coex_band_filter_info.ul_len; j++) 
        {
          if (coex_wwan_state_info[tech_id].state_data.link_list[i].frequency >= coex_band_filter_info.ul_filter[j].freq_start
              && coex_wwan_state_info[tech_id].state_data.link_list[i].frequency <= coex_band_filter_info.ul_filter[j].freq_stop) 
          {
            coex_wwan_state_info[tech_id].tech_band_filter[i] = TRUE;
            break;
          }
          else
          {
            coex_wwan_state_info[tech_id].tech_band_filter[i] = FALSE;
          }
        }
      } /* end UL filtering */

      /*if it is a DL band perform filtering with the DL filter*/
      if ( coex_wwan_state_info[tech_id].state_data.link_list[i].direction & CXM_LNK_DRCTN_DL )
      {
        /*if there is no band filter in this direction, set filter to true*/
        if ( coex_band_filter_info.dl_len == 0 ) 
        {
          coex_wwan_state_info[tech_id].tech_band_filter[i] = FALSE;
        }

        for (j=0; j<coex_band_filter_info.dl_len; j++) 
        {
          if (coex_wwan_state_info[tech_id].state_data.link_list[i].frequency >= coex_band_filter_info.dl_filter[j].freq_start
              && coex_wwan_state_info[tech_id].state_data.link_list[i].frequency <= coex_band_filter_info.dl_filter[j].freq_stop) 
          {
            coex_wwan_state_info[tech_id].tech_band_filter[i] = TRUE;
            break;
          }
          else
          {
            coex_wwan_state_info[tech_id].tech_band_filter[i] = FALSE;
          }
        }
      } /* end DL filtering */

    }
  }

  return;
} /* coex_apply_tech_band_filter */

/*=============================================================================

  FUNCTION:  coex_prep_and_send_wcn_wake_sync_ind

=============================================================================*/
/*!
    @brief
    Function to prepare & send the WCN wake sync INDICATION message to 
    synchronize the WWAN and WCN page wakeups

    @detail
    This function first finds the common interval between the two page
    intervals, checks timing, and sends out the indication if the
    conditions are appropriate.
 
    @return
    qmi_csi_error
*/
/*===========================================================================*/
static qmi_csi_error coex_prep_and_send_wcn_wake_sync_ind (
  uint32 wwan_page_cycle
)
{
  qmi_csi_error                  retval = QMI_CSI_NO_ERR;
  DALResult                      DAL_retval = DAL_SUCCESS;
  coex_wcn_wake_sync_ind_msg_v01 indication;
  int                            WCN_i, WWAN_i;
  int32                          time_elapsed, interval_error;
  uint64                         timestamp;
  /*-----------------------------------------------------------------------*/
  /* find the common interval at which to send the indication from
   * the lookup table */

  /* find WCN index into table */
  for( WCN_i = 0; WCN_i < COEX_MAX_WCN_WAKE_INTERVALS; WCN_i++ )
  {
    if( coex_wcn_wake_intervals[WCN_i] == coex_wcn_wake_sync_info.scan_interval )
    {
      break;
    }
  }
  /* find WWAN index into table */
  for( WWAN_i = 0; WWAN_i < COEX_MAX_WWAN_WAKE_INTERVALS; WWAN_i++ )
  {
    if( coex_wwan_wake_intervals[WWAN_i] == wwan_page_cycle )
    {
      break;
    }
  }
  /* use both indices to find the common interval; else use 0 */
  if( WCN_i  < COEX_MAX_WCN_WAKE_INTERVALS &&
      WWAN_i < COEX_MAX_WWAN_WAKE_INTERVALS )
  {
    coex_wcn_wake_sync_info.common_interval = coex_wcn_wake_interval_LUT[WWAN_i][WCN_i];
  }
  else
  {
    coex_wcn_wake_sync_info.common_interval = 0;
  }

  CXM_MSG_3( MED, "wake_sync, intervals: lte=%d bt=%d common=%d", 
             wwan_page_cycle, coex_wcn_wake_sync_info.scan_interval,
             coex_wcn_wake_sync_info.common_interval );

  /* based on the common interval and the last time we sent an indication,
   * decide whether or not to send one now. We will send one if one of the
   * following cases occurs:
   *   1) It has been ~ <common_interval> since the last time we sent an
   *      indication (+/- a small interval error threshold)
   *   2) It has been more than MAX_TIME_THRESHOLD since we last sent one */

  /* query DAL to get the Qtimer timestamp */
  DAL_retval = DalTimetick_GetTimetick64( coex_DAL_handle, 
                                          (DalTimetickTime64Type *) &timestamp );
  CXM_ASSERT( DAL_SUCCESS == DAL_retval );

  /* calculate time_elapsed in Qclock timeticks (19.2MHz ticks) */
  time_elapsed = timestamp - coex_wcn_wake_sync_info.last_msg_ts;
  /* convert time_elapsed from ticks to mSec */
  time_elapsed = time_elapsed / COEX_TIMETICKS_PER_MSEC;
  interval_error = time_elapsed - coex_wcn_wake_sync_info.common_interval;
  interval_error = (interval_error < 0) ? -interval_error : interval_error;
  CXM_MSG_3( MED, "wake_sync, interval_error=%d timestamp=%d, elapsed_ms=%d",
             interval_error, timestamp, time_elapsed );
  if( ( interval_error < COEX_WCN_WAKE_MIN_TIME_THRESH ) ||
      ( time_elapsed > COEX_WCN_WAKE_MAX_TIME_THRESH ) )
  {
    /* fill in and send out indication to WCN */
    memset( &indication, 0, sizeof( coex_wcn_wake_sync_ind_msg_v01 ) );
    indication.page_interval = wwan_page_cycle;
    indication.timestamp_valid = TRUE;
    indication.timestamp = timestamp;

    retval = mqcsi_conn_mgr_send_ind (
                MQCSI_COEX_SERVICE_ID,
                QMI_COEX_WCN_WAKE_SYNC_IND_V01,
                &indication,
                sizeof ( coex_wcn_wake_sync_ind_msg_v01 )
             );

    /* reset timestamp for next cycle */
    coex_wcn_wake_sync_info.last_msg_ts = timestamp;
  }

  return retval;
}

/*=============================================================================

  FUNCTION:  coex_pwr_lmt_wdog_timer_handler

=============================================================================*/
/*!
    @brief
    Timer handler, called when wci2-based power limiting wdog timer expires.
    Called from cxm_task when timer signals task.

    @return void
*/
/*===========================================================================*/
void coex_pwr_lmt_wdog_timer_handler (
  void
)
{
  uint32        cc_pl_mask = 0;
  cxm_carrier_e cc;
  /*-----------------------------------------------------------------------*/
  /* stop power limiting for all carriers */
  for( cc = CXM_CARRIER_PCC; cc < CXM_CARRIER_MAX; cc++ )
  {
    if( coex_pwr_lmt_info[cc].wci2_state != COEX_PWR_LMT_OFF )
    {
      coex_pwr_lmt_info[cc].wci2_state = COEX_PWR_LMT_OFF;
      cc_pl_mask |= COEX_CC_TO_MASK( cc );
    }
  }

  coex_check_and_set_power_limit( cc_pl_mask );

  /* send condition fail to AP */
  CXM_MSG_1( MED, "Condition fail for power limiting - wci2 timer expired "
                  "ccs=0x%x", cc_pl_mask );
  coex_prep_and_send_condition_fail_ind( 0, 0, COEX_PLCFM_WCI2_TX_PWRLMT_TIMED_OUT_V01 );

  return;
}

/*=============================================================================

  FUNCTION:  coex_wci2_type7_timer_handler

=============================================================================*/
/*!
    @brief
    Handle timer expiration for WCI-2 type 7 filtering. 

    @detail
    Trying to filter
    out extraneous fast transitions, so if timer expires, enough time has
    elapsed to say that this is not an "extraneous fast transition". Thus,
    send the type 7.

    @return
    void
*/
/*===========================================================================*/
void coex_wci2_type7_timer_handler(
  uint32 event_mask
)
{
  /*-----------------------------------------------------------------------*/
  CXM_MSG_1( HIGH, "WCI2 type7 timer handler for event mask %d",
             event_mask );
  /* send the type 7 */
  coex_prep_and_send_wci2_type7( event_mask, CXM_TECH_LTE );

  return;
}

/*=============================================================================

  FUNCTION:  coex_assert_lte_tx_adv

=============================================================================*/
/*!
    @brief
    CXM_SYS_BHVR_CXM_ASSERTS_TX_ADV

    @return
    void
*/
/*===========================================================================*/
void coex_assert_lte_tx_adv(
  cxm_lte_tx_adv_ntc_info_s *tx_info
)
{
  uint32                             i, tx_trigger_time;
  boolean                            rb_check_passed = FALSE;
  cxm_carrier_e                      cc;
  coex_conflict_mdm_params_type_v01 *confl_params;
  /*-----------------------------------------------------------------------*/
  /* TODO: support multiple CCs, tx_info->cc_id; */
  cc = CXM_CARRIER_PCC;
  confl_params = &coex_wwan_state_info[CXM_TECH_LTE].carrier[cc].active_confl->mdm_params;

  CXM_MSG_5( LOW, "SFN received at time: %d, for subframe_ustmr: %d, "
                  "sfn: %d, power: %d, rbs: %d", COEX_READ_USTMR_TIME(), 
             tx_info->ustmr_time, tx_info->sfn, tx_info->tx_power, tx_info->rbs);

  /*if this is an assert subframe, perform condition checks*/
  if (tx_info->transmit)
  {
    /*find out the assert time for the tx assert*/
    tx_trigger_time = COEX_SUB_USTMR(tx_info->ustmr_time, coex_adv_notifi_period);
    /*check if this notification is too late because we are too close to its assert time*/
    CXM_MSG_3( LOW, "assert trigger time for sf: %d space till transmit "
                    "time: %d, required space: %d ", tx_trigger_time,
               COEX_SUB_USTMR( tx_info->ustmr_time, COEX_READ_USTMR_TIME() ), 
               (COEX_ADV_TIMING_TOLERANCE + coex_adv_notifi_period) );
    if( COEX_SUB_USTMR( tx_info->ustmr_time, COEX_READ_USTMR_TIME() ) >= 
        (COEX_ADV_TIMING_TOLERANCE + coex_adv_notifi_period) )
    {
      coex_state_txadv_late = FALSE;
      /*perform RB checks */
      for ( i = 0; i < confl_params->tx_adv.rb_thresh_len; i++ )
      {
        if ( tx_info->rbs >= confl_params->tx_adv.rb_thresh[i].start
              && tx_info->rbs <= confl_params->tx_adv.rb_thresh[i].end )
        {
          rb_check_passed = TRUE;
          break;
        }
      }
      /*if both RB and power checks pass, assert the grfc and schedule the deassert*/
      CXM_MSG_3( LOW, "rb passed: %d, lte_tx_power: %d, conflict_tx_thres: %d ", 
                 rb_check_passed, tx_info->tx_power, 
                 confl_params->tx_adv.tx_power_thresh );
      if ( rb_check_passed && ( tx_info->tx_power >= 
           confl_params->tx_adv.tx_power_thresh ) )
      {
        /*send assert command to common FW if the line is not already high*/
        if (!coex_deassert_scheduled)
        {
          coex_trigger_grfc(TRUE, tx_trigger_time);
        }
        /*schedule the off time*/
        coex_deassert_scheduled = TRUE;
        coex_tx_deassert_time = 
          COEX_ADD_USTMR(tx_info->ustmr_time, COEX_GRFC_ASSERT_PERIOD_USTMR);
        cxm_counter_event(CXM_CNT_TX_ADV_TRANSMIT, 0);

      }
      else
      {
        coex_deassert_scheduled = FALSE;
        /*send deassert to FW*/
        coex_trigger_grfc(FALSE, tx_trigger_time);
      }
    }
    else
    {
      /*if late state is going to be assert, we make sure we know when the deassert should be*/
      coex_tx_deassert_time = 
        COEX_ADD_USTMR(tx_info->ustmr_time, COEX_GRFC_ASSERT_PERIOD_USTMR);

      /*handle late message*/
      coex_process_late_tx_adv();
    }
  }
  else if (!tx_info->transmit && coex_deassert_scheduled)
  {
    /*check if you are too late to deassert on time*/
    tx_trigger_time = COEX_SUB_USTMR(coex_tx_deassert_time, coex_adv_notifi_period);
    CXM_MSG_1( LOW, "deassert trigger time check for sf: %d ", tx_trigger_time );
    if( COEX_SUB_USTMR( coex_tx_deassert_time, COEX_READ_USTMR_TIME() ) >= 
        (COEX_ADV_TIMING_TOLERANCE + coex_adv_notifi_period) )
    {
      coex_state_txadv_late = FALSE;
      /*send deassert to FW if grfc is currently high*/
      if (coex_deassert_scheduled)
      {
        coex_trigger_grfc(FALSE, tx_trigger_time);
        coex_deassert_scheduled = FALSE;
      }
    }
    else
    {
      /*move out the scheduled deassert to the next subframe*/
      coex_tx_deassert_time+=COEX_GRFC_ASSERT_PERIOD_USTMR;

      /*handle late message*/
      coex_process_late_tx_adv();
    }
  }

  return;
}

/*=============================================================================

  FUNCTION:  coex_algos_wci2_dir_read_cb

=============================================================================*/
/*!
    @brief
    Called when direct read of type 0-7 is ready from UART

    @return
    void
*/
/*===========================================================================*/
void coex_algos_wci2_dir_read_cb( uint8* rx_types, uint8 status )
{
  uint8 i;
  /*-----------------------------------------------------------------------*/

  for( i = 0; i < 8; i++ )
  {
    coex_wci2_rx.bytes[i] = rx_types[i];
  }
  /* add new status bits to existing status bit mask */
  atomic_or( &coex_wci2_rx.status, (atomic_plain_word_t) status );
  CXM_TRACE( CXM_TRC_WCI2_RX_CB, status, 0, 0, CXM_TRC_EN );
  rex_set_sigs( &cxm_tcb, CXM_UART_RX_DIRECT_SIG );

  return;
}

/*=============================================================================

  FUNCTION:  coex_handle_recv_wci2_type1

=============================================================================*/
/*!
    @brief
    handle an incoming type 1 message. Type 0 is automatically sent by
    hardware, but we need to send a type 7 out by software.

    @return
    void
*/
/*===========================================================================*/
void coex_handle_recv_wci2_type1( 
  uint8 data
)
{
  /*-----------------------------------------------------------------------*/
  CXM_MSG_0( HIGH, "Handling incoming wci2 type1 message" );
  coex_prep_and_send_wci2_type7( COEX_WCI2_T7_NO_EVNT_MSK, CXM_TECH_LTE );
}

/*=============================================================================

  FUNCTION:  coex_handle_recv_wci2_type6

=============================================================================*/
/*!
    @brief
    Function to handle wci-2 type 6 messages received from 3rd party. Input
    data has already been stripped of type.
    Apply or cancel power limit, as indicated by the T6, to every carrier
    with the policy enabled.

    @return void
*/
/*===========================================================================*/
void coex_handle_recv_wci2_type6 (
  uint8 data
)
{
  uint8         ored_pl_req_update = 0x00;
  uint32        cc_pl_clear_mask   = 0x00;
  uint32        cc_pl_set_mask     = 0x00;
  cxm_carrier_e cc;
  /*-----------------------------------------------------------------------*/
  CXM_MSG_2( HIGH, "Handling wci2 type 6 msg: [incoming_data:%x][curr_req_status:%x]",
             data, coex_wci2_pwr_lmt_src_req_status );

  /* determine action to take and who's asking, based off WCI2 T6 payload */
  data &= COEX_WCI2_T6_PL_MASK;
  switch (data)
  {
    case COEX_WCI2_T6_PL_REQ_OFF_SRC_BT:
      ored_pl_req_update |= (COEX_WCI2_T6_PL_REQ_OFF << COEX_WCI2_T6_PL_SRC_BT);
      break;
    case COEX_WCI2_T6_PL_REQ_ON_SRC_BT:
      ored_pl_req_update |= (COEX_WCI2_T6_PL_REQ_ON << COEX_WCI2_T6_PL_SRC_BT);
      break;
    case COEX_WCI2_T6_PL_REQ_OFF_SRC_WIFI:
      ored_pl_req_update |= (COEX_WCI2_T6_PL_REQ_OFF << COEX_WCI2_T6_PL_SRC_WIFI);
      break;
    case COEX_WCI2_T6_PL_REQ_ON_SRC_WIFI:
      ored_pl_req_update |= (COEX_WCI2_T6_PL_REQ_ON << COEX_WCI2_T6_PL_SRC_WIFI);
      break;
    default:
      ored_pl_req_update = coex_wci2_pwr_lmt_src_req_status;
      CXM_MSG_0( ERROR, "Incorrect data for type 6 msg" );
      break;
  }

  for( cc = CXM_CARRIER_PCC; cc < CXM_CARRIER_MAX; cc++ )
  {
    if( coex_wwan_state_info[CXM_TECH_LTE].carrier[cc].policy & 
        COEX_PCM_REACT_TO_WCI2_TYPE6_TX_POWER_LIMIT_V01 )
    {
      CXM_MSG_3( HIGH, "Processing WCI2 power limiting: [cc:%d][curr_state:%x][new_req:%x]",
                 cc, coex_pwr_lmt_info[cc].wci2_state, ored_pl_req_update );
      if( coex_pwr_lmt_info[cc].wci2_state == COEX_PWR_LMT_OFF &&
          ored_pl_req_update != 0 )
      {
        /* wci2 power limiting now enforced */
        cc_pl_set_mask |= COEX_CC_TO_MASK( cc );
        coex_pwr_lmt_info[cc].wci2_state = COEX_PWR_LMT_PENDING;
      }
      else if( ( coex_pwr_lmt_info[cc].wci2_state == COEX_PWR_LMT_ACTIVE ||
                 coex_pwr_lmt_info[cc].wci2_state == COEX_PWR_LMT_RRC_PROC_ONGOING ||
                 coex_pwr_lmt_info[cc].wci2_state == COEX_PWR_LMT_PENDING ) &&
                 ored_pl_req_update == 0 )
      {
        /* clear the power limit */
        cc_pl_clear_mask |= COEX_CC_TO_MASK( cc );
        coex_pwr_lmt_info[cc].wci2_state = COEX_PWR_LMT_OFF;
      }
      else
      {
        CXM_MSG_2( HIGH, "Prev WCI2 power limiting state continues %x [cc:%d]", 
                   coex_pwr_lmt_info[cc].wci2_state, cc );
      }
    }
  }

  if( cc_pl_set_mask != 0 )
  {
    /* need to set limits -- call to ML1 to check power limiting conditions */
    coex_check_power_limiting_conditions( cc_pl_set_mask );
    cxm_counter_event( CXM_CNT_WCI2_PWR_LMT_REQ, 
                       (uint32) coex_wwan_state_info[CXM_TECH_LTE].plcy_parms.params.lte.wci2_power_limit );
    CXM_MSG_1( HIGH, "Requesting to enforce WCI-2 power limiting [cc_mask:0x%x]", 
               cc_pl_set_mask );

  }
  else if( cc_pl_clear_mask != 0 )
  {
    /* clear the limit for the CCs with WCI2 limits */
    timer_clr( &coex_pwr_lmt_wdog_timer, T_MSEC );
    coex_check_and_set_power_limit( cc_pl_clear_mask );
    CXM_MSG_1( HIGH, "Stopping WCI-2 power limiting [cc_mask:0x%x]",
               cc_pl_clear_mask );
  }

  coex_wci2_pwr_lmt_src_req_status = ored_pl_req_update;

  return;
}

/*=============================================================================

  FUNCTION:  coex_algos_process_wci2_rx

=============================================================================*/
/*!
    @brief
    Process recieved WCI2 bytes

    @return
    void
*/
/*===========================================================================*/
void coex_algos_process_wci2_rx( void )
{
  uint8 i;
  uint8 data;
  uint8 status;
  /*-----------------------------------------------------------------------*/
  /* read the status mask, then clear it and process new data */
  status = (uint8) atomic_read( &coex_wci2_rx.status );
  for( i = 0; i < 8; i++ )
  {
    /* if this type mask is set in the status, this byte is new */
    if( status & (1 << i) )
    {
      data = WCI2_GET_PAYLOAD(coex_wci2_rx.bytes[i]);
      /* log received data */
      cxm_recved_wci2_data_log = data;
      CXM_TRACE( CXM_TRC_WCI2_PROC_RX, coex_wci2_rx.bytes[i], i, 0, 
                 CXM_TRC_AND_PKT_EN, CXM_LOG_WCI2_DATA_RECV );

      atomic_clear_bit( &coex_wci2_rx.status, i );
      switch( (wci2_data_type_e) i )
      {
        case WCI2_TYPE1: /* type 1 message - handled in hardware */
          coex_handle_recv_wci2_type1( data );
          break;

        case WCI2_TYPE6: /* type 6 message */
          coex_handle_recv_wci2_type6( data );
          break;

        default:
          CXM_MSG_2( ERROR, "dropped type %d data = 0x%x", i, data);
          break;
      }
    }
  }

  return;
}

/*=============================================================================

                                FUNCTIONS

=============================================================================*/

/*=============================================================================

  FUNCTION:  coex_handle_indication_register_req

=============================================================================*/
/*!
    @brief
    Function to handle & respond to CXM_INDICATION_REGISTER_REQ message
 
    @return
    qmi_csi_cb_error
*/
/*===========================================================================*/
qmi_csi_cb_error coex_handle_indication_register_req (
  void          *connection_handle,
  qmi_req_handle req_handle,
  unsigned int   msg_id,
  void          *req_c_struct,
  unsigned int   req_c_struct_len,
  void          *service_cookie
)
{
  qmi_csi_cb_error                      req_cb_retval = QMI_CSI_CB_NO_ERR;
  errno_enum_type                       estatus       = E_SUCCESS;
  qmi_csi_error                         ind_retval    = QMI_CSI_NO_ERR;
  coex_indication_register_req_msg_v01 *request;
  coex_indication_register_resp_msg_v01 response;
  /*-----------------------------------------------------------------------*/
  CXM_ASSERT( req_c_struct != NULL );

  request = ( coex_indication_register_req_msg_v01* ) req_c_struct;

  /* Init response data */
  memset( &response, 0, sizeof( coex_indication_register_resp_msg_v01 ) );

  /* Validate REQ message */
  if( sizeof( coex_indication_register_req_msg_v01 ) != req_c_struct_len )
  {
    response.resp.result = QMI_RESULT_FAILURE_V01;
    response.resp.error  = QMI_ERR_MALFORMED_MSG_V01;
    req_cb_retval        = QMI_CSI_CB_INTERNAL_ERR;
  }
  else
  {
    /* All looks okay, update the indication map for this client and
         send out the response */
    response.resp.result = QMI_RESULT_SUCCESS_V01;
    response.resp.error  = QMI_ERR_NONE_V01;

    if( TRUE == request->report_coex_wwan_state_valid )
    {
      /* call CXM CONN MGR layer to update the client's indication map */
      estatus = mqcsi_conn_mgr_update_client_ind_map(
                  MQCSI_COEX_SERVICE_ID,
                  connection_handle,
                  QMI_COEX_WWAN_STATE_IND_V01,
                  request->report_coex_wwan_state
                );
      if( E_SUCCESS != estatus )
      {
        req_cb_retval = QMI_CSI_CB_INTERNAL_ERR;
      }
      else
      {
        /* Send out the current WWAN state info IND */
        ind_retval = coex_prep_and_send_wwan_state_ind( TRUE );
        if( QMI_CSI_NO_ERR != ind_retval )
        {
          req_cb_retval = QMI_CSI_CB_INTERNAL_ERR;
        }
      }
      CXM_TRACE( CXM_TRC_QMI_HNDL_REG_IND_REQ,
                 req_cb_retval, QMI_COEX_WWAN_STATE_IND_V01,
                 (uint32)connection_handle, /* client ptr */
                 CXM_TRC_EN );
    }

    if( TRUE == request->report_coex_metrics_lte_bler_valid )
    {
      /* call CXM CONN MGR layer to update the client's indication map */
      estatus = mqcsi_conn_mgr_update_client_ind_map(
                  MQCSI_COEX_SERVICE_ID,
                  connection_handle,
                  QMI_COEX_METRICS_LTE_BLER_IND_V01,
                  request->report_coex_metrics_lte_bler
                );
      if( E_SUCCESS != estatus )
      {
        req_cb_retval = QMI_CSI_CB_INTERNAL_ERR;
      }
      CXM_TRACE( CXM_TRC_QMI_HNDL_REG_IND_REQ,
                 req_cb_retval, QMI_COEX_METRICS_LTE_BLER_IND_V01,
                 (uint32)connection_handle, /* client ptr */
                 CXM_TRC_EN );
    }

    if( TRUE == request->report_coex_fail_condition_valid )
    {
      /* call CXM CONN MGR layer to update the client's indication map */
      estatus = mqcsi_conn_mgr_update_client_ind_map(
                  MQCSI_COEX_SERVICE_ID,
                  connection_handle,
                  QMI_COEX_CONDITION_FAIL_IND_V01,
                  request->report_coex_fail_condition
                );
      if( E_SUCCESS != estatus )
      {
        req_cb_retval = QMI_CSI_CB_INTERNAL_ERR;
      }
      CXM_TRACE( CXM_TRC_QMI_HNDL_REG_IND_REQ,
                 req_cb_retval, QMI_COEX_CONDITION_FAIL_IND_V01,
                 (uint32)connection_handle, /* client ptr */
                 CXM_TRC_EN );
    }

    if( TRUE == request->report_coex_success_condition_valid )
    {
      /* call CXM CONN MGR layer to update the client's indication map */
      estatus = mqcsi_conn_mgr_update_client_ind_map(
                  MQCSI_COEX_SERVICE_ID,
                  connection_handle,
                  QMI_COEX_CONDITION_SUCCESS_IND_V01,
                  request->report_coex_success_condition
                );
      if( E_SUCCESS != estatus )
      {
        req_cb_retval = QMI_CSI_CB_INTERNAL_ERR;
      }
      CXM_TRACE( CXM_TRC_QMI_HNDL_REG_IND_REQ,
                 req_cb_retval, QMI_COEX_CONDITION_SUCCESS_IND_V01,
                 (uint32)connection_handle, /* client ptr */
                 CXM_TRC_EN );
    }

    if( TRUE == request->report_coex_sleep_valid )
    {
      /* call CXM CONN MGR layer to update the client's indication map */
      estatus = mqcsi_conn_mgr_update_client_ind_map(
                  MQCSI_COEX_SERVICE_ID,
                  connection_handle,
                  QMI_COEX_SLEEP_IND_V01,
                  request->report_coex_sleep
                );
      if( E_SUCCESS != estatus )
      {
        req_cb_retval = QMI_CSI_CB_INTERNAL_ERR;
      }
      CXM_TRACE( CXM_TRC_QMI_HNDL_REG_IND_REQ,
                 req_cb_retval, QMI_COEX_SLEEP_IND_V01,
                 (uint32)connection_handle, /* client ptr */
                 CXM_TRC_EN );
    }

    if( TRUE == request->report_coex_wakeup_valid )
    {
      /* call CXM CONN MGR layer to update the client's indication map */
      estatus = mqcsi_conn_mgr_update_client_ind_map(
                  MQCSI_COEX_SERVICE_ID,
                  connection_handle,
                  QMI_COEX_WAKEUP_IND_V01,
                  request->report_coex_wakeup
                );
      if( E_SUCCESS != estatus )
      {
        req_cb_retval = QMI_CSI_CB_INTERNAL_ERR;
      }
      CXM_TRACE( CXM_TRC_QMI_HNDL_REG_IND_REQ,
                 req_cb_retval, QMI_COEX_WAKEUP_IND_V01,
                 (uint32)connection_handle, /* client ptr */
                 CXM_TRC_EN );
    }

    if( TRUE == request->report_coex_page_sync_valid )
    {
      /* call CXM CONN MGR layer to update the client's indication map */
      estatus = mqcsi_conn_mgr_update_client_ind_map(
                  MQCSI_COEX_SERVICE_ID,
                  connection_handle,
                  QMI_COEX_WCN_WAKE_SYNC_IND_V01,
                  request->report_coex_page_sync
                );
      if( E_SUCCESS != estatus )
      {
        req_cb_retval = QMI_CSI_CB_INTERNAL_ERR;
      }
      CXM_TRACE( CXM_TRC_QMI_HNDL_REG_IND_REQ,
                 req_cb_retval, QMI_COEX_WCN_WAKE_SYNC_IND_V01,
                 (uint32)connection_handle, /* client ptr */
                 CXM_TRC_EN );
    }
  }

  CXM_TRACE( CXM_TRC_QMI_HNDL_REG_IND_REQ,
             req_cb_retval, !0x00, (uint32)connection_handle,
             CXM_TRC_EN );

  if( QMI_CSI_CB_NO_ERR != req_cb_retval )
  {
    /* Something went wrong while updating one of the indication map(s) for this client and
         send out the response */
    response.resp.result = QMI_RESULT_FAILURE_V01;
    response.resp.error  = QMI_ERR_INTERNAL_V01;
  }

  /* Send the response */
  req_cb_retval = mqcsi_conn_mgr_send_resp_from_cb (
                    connection_handle,
                    req_handle,
                    msg_id,
                    &response,
                    sizeof( coex_indication_register_resp_msg_v01 )
                  );
  CXM_ASSERT( QMI_CSI_CB_NO_ERR == req_cb_retval );

  return req_cb_retval;
}


/*=============================================================================

  FUNCTION:  coex_handle_get_wwan_state_req

=============================================================================*/
/*!
    @brief
    Function to handle & respond to COEX_GET_WWAN_STATE_REQ message
 
    @return
    qmi_csi_cb_error
*/
/*===========================================================================*/
qmi_csi_cb_error coex_handle_get_wwan_state_req (
  void          *connection_handle,
  qmi_req_handle req_handle,
  unsigned int   msg_id,
  void          *req_c_struct,
  unsigned int   req_c_struct_len,
  void          *service_cookie
)
{
  qmi_csi_cb_error                 req_cb_retval = QMI_CSI_CB_NO_ERR;
  coex_get_wwan_state_resp_msg_v01 response;
  uint8                            i;
  /*-----------------------------------------------------------------------*/
  /* Reset the memory available for the RESP message */
  memset( &response, 0, sizeof( coex_get_wwan_state_resp_msg_v01 ) );

  /* fill out the info by copying the COEX WWAN State Indication, which
   * is up to date since it's filled out every time new state info arrives */
  response.lte_band_info_valid = coex_wwan_state_ind.lte_band_info_valid;
  response.lte_band_info = coex_wwan_state_ind.lte_band_info;
  response.lte_tdd_info_valid = coex_wwan_state_ind.lte_tdd_info_valid;
  response.lte_tdd_info = coex_wwan_state_ind.lte_tdd_info;
  /* instead of reporting LTE ML1's invalid frame offset value of -1 (0xFFFFFFFF)
       report that of nominal TA of 0, therefore frame offset of 2000 uSecs */
  if( response.lte_tdd_info.frame_offset == 0xFFFFFFFF )
  {
    response.lte_tdd_info.frame_offset = 2000;
  }

  if( coex_wwan_state_ind.lte_band_info_set_valid )
  {
    response.lte_band_info_set_valid = TRUE;
    response.lte_band_info_set_len = coex_wwan_state_ind.lte_band_info_set_len;
    for( i = 0; i < response.lte_band_info_set_len; i++ )
    {
      response.lte_band_info_set[i] = coex_wwan_state_ind.lte_band_info_set[i];
    }
  }

  if( coex_wwan_state_ind.lte_carrier_info_set_valid )
  {
    response.lte_carrier_info_set_valid = TRUE;
    response.lte_carrier_info_set_len = coex_wwan_state_ind.lte_carrier_info_set_len;
    for( i = 0; i < response.lte_carrier_info_set_len; i++ )
    {
      response.lte_carrier_info_set[i] = coex_wwan_state_ind.lte_carrier_info_set[i];
      /* instead of reporting LTE ML1's invalid frame offset value of -1 (0xFFFFFFFF)
           report that of nominal TA of 0, therefore frame offset of 2000 uSecs */
      if( response.lte_carrier_info_set[i].tdd_info.frame_offset == 0xFFFFFFFF )
      {
        response.lte_carrier_info_set[i].tdd_info.frame_offset = 2000;
      }
    }
  }

  if( coex_wwan_state_ind.tdscdma_band_info_set_valid )
  {
    response.tdscdma_band_info_set_valid = TRUE;
    response.tdscdma_band_info_set_len = coex_wwan_state_ind.tdscdma_band_info_set_len;
    for( i = 0; i < response.tdscdma_band_info_set_len; i++ )
    {
      response.tdscdma_band_info_set[i] = coex_wwan_state_ind.tdscdma_band_info_set[i];
    }
  }

  if( coex_wwan_state_ind.gsm_band_info_set_valid )
  {
    response.gsm_band_info_set_valid = TRUE;
    response.gsm_band_info_set_len = coex_wwan_state_ind.gsm_band_info_set_len;
    for( i = 0; i < response.gsm_band_info_set_len; i++ )
    {
      response.gsm_band_info_set[i] = coex_wwan_state_ind.gsm_band_info_set[i];
    }
  }

  if( coex_wwan_state_ind.gsm2_band_info_set_valid )
  {
    response.gsm2_band_info_set_valid = TRUE;
    response.gsm2_band_info_set_len = coex_wwan_state_ind.gsm2_band_info_set_len;
    for( i = 0; i < response.gsm2_band_info_set_len; i++ )
    {
      response.gsm2_band_info_set[i] = coex_wwan_state_ind.gsm2_band_info_set[i];
    }
  }

  if( coex_wwan_state_ind.gsm3_band_info_set_valid )
  {
    response.gsm3_band_info_set_valid = TRUE;
    response.gsm3_band_info_set_len = coex_wwan_state_ind.gsm3_band_info_set_len;
    for( i = 0; i < response.gsm3_band_info_set_len; i++ )
    {
      response.gsm3_band_info_set[i] = coex_wwan_state_ind.gsm3_band_info_set[i];
    }
  }

  if( coex_wwan_state_ind.onex_band_info_set_valid )
  {
    response.onex_band_info_set_valid = TRUE;
    response.onex_band_info_set_len = coex_wwan_state_ind.onex_band_info_set_len;
    for( i = 0; i < response.onex_band_info_set_len; i++ )
    {
      response.onex_band_info_set[i] = coex_wwan_state_ind.onex_band_info_set[i];
    }
  }

  if( coex_wwan_state_ind.hdr_band_info_set_valid )
  {
    response.hdr_band_info_set_valid = TRUE;
    response.hdr_band_info_set_len = coex_wwan_state_ind.hdr_band_info_set_len;
    for( i = 0; i < response.hdr_band_info_set_len; i++ )
    {
      response.hdr_band_info_set[i] = coex_wwan_state_ind.hdr_band_info_set[i];
    }
  }

  if( coex_wwan_state_ind.wcdma_band_info_set_valid )
  {
    response.wcdma_band_info_set_valid = TRUE;
    response.wcdma_band_info_set_len = coex_wwan_state_ind.wcdma_band_info_set_len;
    for( i = 0; i < response.wcdma_band_info_set_len; i++ )
    {
      response.wcdma_band_info_set[i] = coex_wwan_state_ind.wcdma_band_info_set[i];
    }
  }

  /* Send the response */
  req_cb_retval = mqcsi_conn_mgr_send_resp_from_cb (
                    connection_handle,
                    req_handle,
                    msg_id,
                    &response,
                    sizeof( coex_get_wwan_state_resp_msg_v01 )
                  );
  CXM_ASSERT( QMI_CSI_CB_NO_ERR == req_cb_retval );

  return req_cb_retval;
}


/*=============================================================================

  FUNCTION:  coex_handle_set_wlan_state_req

=============================================================================*/
/*!
    @brief
    This function handles the set wlan state req message from QMI.  If
    the message indicates valid info, the data is passed to a
    function to update the local coex wlan data with this new information.

    @return
    qmi_csi_cb_error
*/
/*===========================================================================*/
qmi_csi_cb_error coex_handle_set_wlan_state_req (
  void          *connection_handle,
  qmi_req_handle req_handle,
  unsigned int   msg_id,
  void          *req_c_struct,
  unsigned int   req_c_struct_len,
  void          *service_cookie
)
{
  uint32                           mode_mask = 0;
  qmi_csi_cb_error req_cb_retval = QMI_CSI_CB_NO_ERR;
  coex_set_wlan_state_req_msg_v01 *request;
  coex_set_wlan_state_resp_msg_v01 response;
  /*-----------------------------------------------------------------------*/
  CXM_ASSERT( req_c_struct != NULL );

  CXM_MSG_0( HIGH, "handling QMI_COEX_SET_WLAN_STATE_REQ_V01" );

  request = (coex_set_wlan_state_req_msg_v01 *) req_c_struct;

  /* Init response data */
  memset( &response, 0, sizeof(coex_set_wlan_state_resp_msg_v01) );
  response.resp.result = QMI_RESULT_SUCCESS_V01;
  response.resp.error  = QMI_ERR_NONE_V01;

  if ( request->scan_info_valid )
  {
    response.resp.result = QMI_RESULT_FAILURE_V01;
    response.resp.error  = QMI_ERR_REQUESTED_NUM_UNSUPPORTED_V01;
  }
  else
  {
    if ( request->conn_info_valid )
    {
      /* if the connection info is valid, need to compare to our conn list */
      coex_update_wlan_conn_state_info( request );
      mode_mask |= CXM_WLAN_CONN_TYPE;
    }

    if ( request->high_prio_info_valid )
    {
      /* if the high prio info is valid, need to compare to our high prio list */
      coex_update_wlan_hp_state_info( request );
      mode_mask |= CXM_WLAN_HIGH_PRIO_TYPE;
    }
  }

  /* Send the response */
  req_cb_retval = mqcsi_conn_mgr_send_resp_from_cb (
                    connection_handle,
                    req_handle,
                    msg_id,
                    &response,
                    sizeof( coex_set_wlan_state_resp_msg_v01 )
                  );
  CXM_ASSERT( QMI_CSI_CB_NO_ERR == req_cb_retval );
#ifdef FEATURE_COEX_USE_NV
  if( COEX_SYS_ENABLED(CXM_SYS_BHVR_VICTIM_TABLE) )
  {
    coex_confl_update_wcn_tech( CXM_TECH_WIFI, mode_mask );
  }
#endif

  return req_cb_retval;
}

/*=============================================================================

  FUNCTION:  coex_handle_get_wlan_hp_state_req

=============================================================================*/
/*!
    @brief
    Given a high prio id, the function locates the id in the coex wlan data (if
    it exists) and returns the data in that high prio entry.
 
    @return
    qmi_csi_cb_error
*/
/*===========================================================================*/
qmi_csi_cb_error coex_handle_get_wlan_hp_state_req (
  void          *connection_handle,
  qmi_req_handle req_handle,
  unsigned int   msg_id,
  void          *req_c_struct,
  unsigned int   req_c_struct_len,
  void          *service_cookie
)
{
  qmi_csi_cb_error req_cb_retval = QMI_CSI_CB_NO_ERR;
  coex_get_wlan_high_prio_state_req_msg_v01 *request;
  coex_get_wlan_high_prio_state_resp_msg_v01 response;
  coex_wlan_hp_node_type *node = coex_wlan_hp_head;
  /*-----------------------------------------------------------------------*/
  CXM_ASSERT( req_c_struct != NULL );

  request = (coex_get_wlan_high_prio_state_req_msg_v01 *) req_c_struct;

  /* intialize to default, in case no match is found */
  response.resp.result = QMI_RESULT_FAILURE_V01;
  response.resp.error = QMI_ERR_INVALID_HANDLE_V01;
  response.high_prio_info.id = 0;
  response.high_prio_info.band.freq = 0;
  response.high_prio_info.band.bandwidth = 0;
  response.high_prio_info.state = 0;
  response.high_prio_info_valid = FALSE;

  while ( node != NULL )
  {
    /* id is unique and indicates a match */
    if ( node->high_prio.id == request->id )
    {
      response.resp.result = QMI_RESULT_SUCCESS_V01;
      response.resp.error = QMI_ERR_NONE_V01;
      response.high_prio_info.id = node->high_prio.id;
      response.high_prio_info.band.freq = node->high_prio.band.freq;
      response.high_prio_info.band.bandwidth = node->high_prio.band.bandwidth;
      response.high_prio_info.state = node->high_prio.state;
      response.high_prio_info_valid = TRUE;
      break;
    }
    node = node->next;
  }

  /* send the qmi response */
  req_cb_retval = mqcsi_conn_mgr_send_resp_from_cb (
                    connection_handle,  
                    req_handle,
                    msg_id,
                    &response,
                    sizeof( coex_get_wlan_high_prio_state_resp_msg_v01 )
                  );
  CXM_ASSERT( QMI_CSI_CB_NO_ERR == req_cb_retval );

  return req_cb_retval;
}

/*=============================================================================

  FUNCTION:  coex_handle_get_wlan_conn_state_req

=============================================================================*/
/*!
    @brief
    Given a conn handle, the function locates the handle in the coex wlan data
    (if it exists) and returns the data in that conn entry.
 
    @return
    qmi_csi_cb_error
*/
/*===========================================================================*/
qmi_csi_cb_error coex_handle_get_wlan_conn_state_req (
  void          *connection_handle,
  qmi_req_handle req_handle,
  unsigned int   msg_id,
  void          *req_c_struct,
  unsigned int   req_c_struct_len,
  void          *service_cookie
)
{
  qmi_csi_cb_error req_cb_retval = QMI_CSI_CB_NO_ERR;
  coex_get_wlan_conn_state_req_msg_v01 *request;
  coex_get_wlan_conn_state_resp_msg_v01 response;
  coex_wlan_conn_node_type *node = coex_wlan_conn_head;
  uint16 i;
  /*-----------------------------------------------------------------------*/
  CXM_ASSERT( req_c_struct != NULL );

  request = (coex_get_wlan_conn_state_req_msg_v01 *) req_c_struct;

  /* intialize to default, in case no match is found */
  response.resp.result = QMI_RESULT_FAILURE_V01;
  response.resp.error = QMI_ERR_INVALID_HANDLE_V01;
  response.conn_info_valid = FALSE;
  response.conn_info.handle = 0;
  response.conn_info.band_len = 0;
  response.conn_info.band[0].bandwidth = 0;
  response.conn_info.band[0].freq = 0;
  response.conn_info.state = COEX_WLAN_CONN_DISABLED;
  response.conn_info.mode = COEX_WLAN_CONN_MODE_NONE;

  while ( node != NULL )
  {
    /* handle is unique and indicates a match */
    if ( node->conn.handle == request->conn_handle )
    {
      response.resp.result = QMI_RESULT_SUCCESS_V01;
      response.resp.error = QMI_ERR_NONE_V01;
      response.conn_info_valid = TRUE;
      response.conn_info.handle = node->conn.handle;
      response.conn_info.band_len = node->conn.band_len;
      for ( i = 0; i < response.conn_info.band_len; i++ )
      {
        response.conn_info.band[i].bandwidth = node->conn.band[i].bandwidth;
        response.conn_info.band[i].freq = node->conn.band[i].freq;
      }
      response.conn_info.state = node->conn.state;
      response.conn_info.mode = node->conn.mode;

      /* handle has been found, no need to keep searching */
      break;
    }
    node = node->next;
  }

  /* send the qmi response */
  req_cb_retval = mqcsi_conn_mgr_send_resp_from_cb (
                    connection_handle,
                    req_handle,
                    msg_id,
                    &response,
                    sizeof( coex_get_wlan_conn_state_resp_msg_v01 )
                  );
  CXM_ASSERT( QMI_CSI_CB_NO_ERR == req_cb_retval );

  return req_cb_retval;
}

/*=============================================================================

  FUNCTION:  coex_handle_set_policy_req

=============================================================================*/
/*!
    @brief
    Function to handle client's request to set the current COEX policy to be
    followed. It processes the message and appropriately forwards the same to
    underlying layers.
    TODO: this only handles primary carrier for now

    @return
    qmi_csi_cb_error
*/
/*===========================================================================*/
qmi_csi_cb_error coex_handle_set_policy_req (
  void          *connection_handle,
  qmi_req_handle req_handle,
  unsigned int   msg_id,
  void          *req_c_struct,
  unsigned int   req_c_struct_len,
  void          *service_cookie
)
{
  qmi_csi_cb_error             req_cb_retval = QMI_CSI_CB_NO_ERR;
  qmi_response_type_v01        response;
  coex_set_policy_req_msg_v01 *req_ptr;
  errno_enum_type              retval = E_SUCCESS;
  uint32                       wci2_t7_msk = 0x00;
  coex_policy_config_mask_v01  prev_policy_mask = 0x00;
  coex_wwan_tech_info         *lte_info;
  coex_carrier_info           *cc_info;
  cxm_carrier_e                cc;
  /*-----------------------------------------------------------------------*/
  CXM_ASSERT( req_c_struct != NULL );

  /* Init the memory available for the REQ message */
  memset( &response, 0, sizeof( qmi_response_type_v01 ) );

  /* De-mystify req_c_struct */
  req_ptr = (coex_set_policy_req_msg_v01 *) req_c_struct;
  lte_info = &coex_wwan_state_info[CXM_TECH_LTE];
  cc = CXM_CARRIER_PCC;
  cc_info = &coex_wwan_state_info[CXM_TECH_LTE].carrier[cc];

  if ( COEX_SYS_ENABLED(CXM_SYS_BHVR_QMI_POLICY_CONTROL) )
  {
    CXM_MSG_2( HIGH, "Handling QMI_COEX_SET_POLICY_REQ policy_valid=%d mask=%d",
               req_ptr->policy_valid, req_ptr->policy );
    response.result = QMI_RESULT_SUCCESS_V01;
    response.error  = QMI_ERR_NONE_V01;

    /* Update global copy of policy info. Use prev value when a field is invalid */
    if(req_ptr->policy_valid)
    {
      prev_policy_mask = cc_info->policy;
      cc_info->policy = req_ptr->policy;
      /* copy any global policy bits (not per-carrier policies) */
      lte_info->plcy_parms.tech_policy = cc_info->policy & COEX_TECH_GLOBAL_POLICY_MASK;

      if( COEX_SYS_ENABLED(CXM_SYS_BHVR_BASIC_WCI2) )
      {
        cc_info->policy &= COEX_BASIC_WCI2_MASK;
      }

      /* manage controller power limit */
      if( ( coex_pwr_lmt_info[cc].controller_state == COEX_PWR_LMT_OFF ) &&
          ( cc_info->policy & COEX_PCM_ENFORCE_CONTROLLER_TX_POWER_LIMIT_V01 ))
      {
        /* controller power limiting now enforced */
        coex_pwr_lmt_info[cc].controller_state = COEX_PWR_LMT_PENDING;
        coex_check_power_limiting_conditions( COEX_CC_TO_MASK(cc) );
        cxm_counter_event( CXM_CNT_CONTR_PWR_LMT_REQ, 
                           lte_info->plcy_parms.params.lte.controller_tx_power_limit );
      }
      else if( (cc_info->policy & COEX_PCM_ENFORCE_CONTROLLER_TX_POWER_LIMIT_V01) == 0 )
      {
        coex_pwr_lmt_info[cc].controller_state = COEX_PWR_LMT_OFF;
      }

      /* manage wci-2 power limit */
      if( ((cc_info->policy & COEX_PCM_REACT_TO_WCI2_TYPE6_TX_POWER_LIMIT_V01) == 0 ) &&
          (coex_pwr_lmt_info[cc].wci2_state != COEX_PWR_LMT_OFF) )
      {
        coex_pwr_lmt_info[cc].wci2_state = COEX_PWR_LMT_OFF;
        /* stop wdog timer */
        timer_clr( &coex_pwr_lmt_wdog_timer, T_MSEC );
      }

      if ( req_ptr->policy != 0 )
      {
        /* policy is now enforced, check if uart should be powered on */
        if ( COEX_SYS_ENABLED(CXM_SYS_BHVR_WCI2_DATA) )
        {
          coex_manage_uart_npa_vote();

          /* Check for type7[connected] policy transition */
          if( !(COEX_PCM_SEND_WCI2_TYPE7_MDM_CONN_STATE_V01 & prev_policy_mask) &&
              COEX_PCM_SEND_WCI2_TYPE7_MDM_CONN_STATE_V01 & cc_info->policy )
          {
            wci2_t7_msk |= COEX_WCI2_T7_CONN_ST_EVNT_MSK;
          }
          if( !(COEX_PCM_SEND_WCI2_TYPE7_MDM_CONN_STATE_V01 & cc_info->policy) )
          {
            coex_wci2_t7_state.wwan_tx_active = FALSE;
          }

          /* Check for type7[tx_pwr] policy transition */
          if( !(COEX_PCM_SEND_WCI2_TYPE7_MDM_PWR_STATE_V01 & prev_policy_mask) &&
              COEX_PCM_SEND_WCI2_TYPE7_MDM_PWR_STATE_V01 & cc_info->policy )
          {
            wci2_t7_msk |= COEX_WCI2_T7_PWR_ST_EVNT_MSK;
          }
          if( !(COEX_PCM_SEND_WCI2_TYPE7_MDM_PWR_STATE_V01 & cc_info->policy) )
          {
            coex_wci2_t7_state.wwan_tx_pwr_active = FALSE;
          }

          /* Check for type7[tx_ant] policy transition */
          if( !(COEX_PCM_SEND_WCI2_TYPE7_MDM_TX_ANT_SEL_V01 & prev_policy_mask) &&
              COEX_PCM_SEND_WCI2_TYPE7_MDM_TX_ANT_SEL_V01 & cc_info->policy )
          {
	   		    wci2_t7_msk |= COEX_WCI2_T7_TX_ANT_SEL_EVNT_MSK;
          }
          if( !(COEX_PCM_SEND_WCI2_TYPE7_MDM_TX_ANT_SEL_V01 & cc_info->policy) )
          {
            coex_wci2_t7_state.tx_ant_sel = FALSE;
          }

          /* Trigger WCI2 type7 message only if a valid event identified */
          if( COEX_PCM_SEND_WCI2_TYPE7_MDM_CONN_STATE_V01 & cc_info->policy ||
              COEX_PCM_SEND_WCI2_TYPE7_MDM_PWR_STATE_V01  & cc_info->policy ||
              COEX_PCM_SEND_WCI2_TYPE7_MDM_TX_ANT_SEL_V01 & cc_info->policy )
          {
            coex_prep_and_send_wci2_type7( wci2_t7_msk, CXM_TECH_LTE );
          }
        }

        if( COEX_PCM_RESERVED_FOR_CONTROLLER_V01 & cc_info->policy )
        {
          CXM_MSG_0( HIGH, "Setting policy RESERVED FOR CONTROLLER");
          cxm_counter_event( CXM_CNT_PLCY_RSRVD_FOR_CTRLR, 0 );
        }
      }
      else
      {
        /* policy is now not enforced, check if uart should be powered off */
        if ( COEX_SYS_ENABLED(CXM_SYS_BHVR_WCI2_DATA) ) 
        {
          coex_manage_uart_npa_vote();
          coex_wci2_t7_state.wwan_tx_active = FALSE;
          coex_wci2_t7_state.wwan_tx_pwr_active = FALSE;
          coex_wci2_t7_state.tx_ant_sel = FALSE;
        }

        /* if the policy was set to 0 (reset) count it */
        cxm_counter_event( CXM_CNT_POLICY_RESET, 0 );
      }
    }

    COEX_BOUND_AND_SET( req_ptr->power_threshold_valid,
                        cc_info->plcy_parms.power_threshold, 
                        req_ptr->power_threshold, COEX_POLICY_POWER_THRESH_MIN, 
                        COEX_POLICY_POWER_THRESH_MAX );

    COEX_BOUND_AND_SET( req_ptr->rb_threshold_valid,
                        cc_info->plcy_parms.rb_threshold, 
                        req_ptr->rb_threshold, 0, COEX_POLICY_RB_THRESH_MAX );

    COEX_BOUND_AND_SET( req_ptr->lte_tx_continuous_subframe_denials_threshold_valid,
                        lte_info->plcy_parms.tx_continuous_subframe_denials_threshold, 
                        req_ptr->lte_tx_continuous_subframe_denials_threshold, 
                        COEX_POLICY_CONT_SF_DENIALS_THRESH_MIN, COEX_POLICY_CONT_SF_DENIALS_THRESH_MAX );

    COEX_BOUND_AND_SET( req_ptr->lte_tx_subrame_denial_params_valid,
                        lte_info->plcy_parms.tx_continuous_subframe_denials_threshold, 
                        req_ptr->lte_tx_continuous_subframe_denials_threshold, 
                        COEX_POLICY_CONT_SF_DENIALS_THRESH_MIN, COEX_POLICY_CONT_SF_DENIALS_THRESH_MAX );

    if(req_ptr->lte_tx_subrame_denial_params_valid)
    {
      lte_info->plcy_parms.tx_subrame_denial_params = 
        req_ptr->lte_tx_subrame_denial_params;

      lte_info->plcy_parms.tx_subrame_denial_params.max_allowed_frame_denials =
        ( req_ptr->lte_tx_subrame_denial_params.max_allowed_frame_denials > COEX_POLICY_MAX_SF_DENIALS_MAX ?
          COEX_POLICY_MAX_SF_DENIALS_MAX : req_ptr->lte_tx_subrame_denial_params.max_allowed_frame_denials );

      lte_info->plcy_parms.tx_subrame_denial_params.frame_denial_window =
        ( req_ptr->lte_tx_subrame_denial_params.frame_denial_window > COEX_POLICY_SF_DENIAL_WINDOW_MAX ?
          COEX_POLICY_SF_DENIAL_WINDOW_MAX : req_ptr->lte_tx_subrame_denial_params.frame_denial_window );
    }

    if(req_ptr->apt_table_valid)
    {
      /* this value no longer supported but still copy it in */
      lte_info->plcy_parms.params.lte.apt_table = req_ptr->apt_table;
    }

    COEX_BOUND_AND_SET( req_ptr->controller_tx_power_limit_valid,
                        lte_info->plcy_parms.params.lte.controller_tx_power_limit, 
                        req_ptr->controller_tx_power_limit, 
                        COEX_POLICY_TX_POWER_LIMIT_MIN, COEX_POLICY_TX_POWER_LIMIT_MAX );

    COEX_BOUND_AND_SET( req_ptr->wci2_power_limit_valid,
                        lte_info->plcy_parms.params.lte.wci2_power_limit, 
                        req_ptr->wci2_power_limit, 
                        COEX_POLICY_TX_POWER_LIMIT_MIN, COEX_POLICY_TX_POWER_LIMIT_MAX );

    COEX_BOUND_AND_SET( req_ptr->link_path_loss_threshold_valid,
                        coex_plcy_parms.link_path_loss_threshold, 
                        req_ptr->link_path_loss_threshold, 
                        COEX_POLICY_LINK_PATH_LOSS_THRESH_MIN, COEX_POLICY_LINK_PATH_LOSS_THRESH_MAX );

    COEX_BOUND_AND_SET( req_ptr->rb_filter_alpha_valid,
                        coex_plcy_parms.rb_filter_alpha, 
                        req_ptr->rb_filter_alpha, 
                        COEX_POLICY_RB_FILTER_ALPHA_MIN, COEX_POLICY_RB_FILTER_ALPHA_MAX );

    COEX_BOUND_AND_SET( req_ptr->filtered_rb_threshold_valid,
                        coex_plcy_parms.filtered_rb_threshold, 
                        req_ptr->filtered_rb_threshold, 
                        COEX_POLICY_FILTERED_RB_THRESH_MIN, COEX_POLICY_FILTERED_RB_THRESH_MAX );

    if(req_ptr->wci2_tx_pwrlmt_timeout_valid)
    {
      lte_info->plcy_parms.params.lte.wci2_tx_pwrlmt_timeout = 
        req_ptr->wci2_tx_pwrlmt_timeout;
    }

    if(req_ptr->controller_tx_pwrlmt_timeout_valid)
    {
      lte_info->plcy_parms.params.lte.controller_tx_pwrlmt_timeout = 
        req_ptr->controller_tx_pwrlmt_timeout;
    }

    COEX_BOUND_AND_SET( req_ptr->tx_power_threshold_for_adv_tx_notice_valid,
                        lte_info->plcy_parms.params.lte.tx_power_threshold_for_adv_tx_notice, 
                        req_ptr->tx_power_threshold_for_adv_tx_notice, 
                        COEX_POLICY_POWER_THRESH_FOR_TX_ADV_NTC_MIN_DB10, 
                        COEX_POLICY_POWER_THRESH_FOR_TX_ADV_NTC_MAX_DB10 );

    COEX_BOUND_AND_SET( req_ptr->rb_threshold_for_adv_tx_notice_valid,
                        lte_info->plcy_parms.params.lte.rb_threshold_for_adv_tx_notice, 
                        req_ptr->rb_threshold_for_adv_tx_notice, 
                        0, COEX_POLICY_RB_THRESH_FOR_TX_ADV_NTC_MAX );

    if(req_ptr->holdoff_timer_for_rat_conn_state_valid)
    {
      /* to uint32, incoming value uint16, min=0 and max=MAX_INT(uint16) enforced by type */
      coex_plcy_parms.t7_con_holdoff = req_ptr->holdoff_timer_for_rat_conn_state;
    }

    COEX_BOUND_AND_SET( req_ptr->filter_alpha_for_rat_power_state_valid,
                        coex_plcy_parms.t7_pwr_alpha, 
                        req_ptr->filter_alpha_for_rat_power_state, 
                        COEX_POLICY_FILTER_ALPHA_RAT_PWR_STATE_MIN, 
                        COEX_POLICY_FILTER_ALPHA_RAT_PWR_STATE_MAX );

    COEX_BOUND_AND_SET( req_ptr->tx_power_threshold_for_rat_power_state_valid,
                        coex_plcy_parms.t7_pwr_thresh, 
                        req_ptr->tx_power_threshold_for_rat_power_state, 
                        COEX_POLICY_TX_PWR_THRESH_RAT_PWR_STATE_MIN, 
                        COEX_POLICY_TX_PWR_THRESH_RAT_PWR_STATE_MAX );

    if(req_ptr->holdoff_timer_for_rat_power_state_valid)
    {
      /* to uint32, incoming value uint16, min=0 and max=MAX_INT(uint16) enforced by type */
      coex_plcy_parms.t7_pwr_holdoff = req_ptr->holdoff_timer_for_rat_power_state;
    }

    /* Power limit states may have changed. Check if power limit needs to be set */
    coex_check_and_set_power_limit( COEX_CC_TO_MASK(cc) );

    retval = coex_prep_and_send_policy_msg( CXM_TECH_LTE );

    cxm_counter_event( CXM_CNT_POLICY_UPDATES, cc_info->policy );
    CXM_TRACE( CXM_TRC_QMI_SET_POLICY_REQ, req_ptr->policy, 0, 0,
                 CXM_TRC_AND_PKT_EN, CXM_LOG_GLOBAL_POLICY_PARAMS,
                 CXM_LOG_WCI2_PWR_LMT_STATE, CXM_LOG_CONTROLLER_PWR_LMT_STATE );
  } 
  else if ( COEX_SYS_ENABLED(CXM_SYS_BHVR_OFF_STATE_ENABLED) && 
            req_ptr->policy_valid && req_ptr->policy == CXM_POLICY_NONE)
  {
    /* send policy 0 then terminate CXM task after sending the QMI response */
    cc_info->policy = CXM_POLICY_NONE;
    retval = coex_prep_and_send_policy_msg( CXM_TECH_LTE );

    response.result = QMI_RESULT_SUCCESS_V01;
    response.error  = QMI_ERR_NONE_V01;
  }
  else
  {
    /* currently not supported, send out the response */
    response.result = QMI_RESULT_FAILURE_V01;
    response.error  = QMI_ERR_REQUESTED_NUM_UNSUPPORTED_V01;
  } /*CXM_SYS_BHVR_QMI_CONTROL, CXM_SYS_BHVR_OFF_STATE_ENABLED*/

  /* send QMI response */
  req_cb_retval = mqcsi_conn_mgr_send_resp_from_cb (
                    connection_handle,
                    req_handle,
                    msg_id,
                    &response,
                    sizeof( qmi_response_type_v01 )
                  );
  CXM_ASSERT( QMI_CSI_CB_NO_ERR == req_cb_retval );

  if(retval != E_SUCCESS)
  {
    req_cb_retval = QMI_CSI_CB_INTERNAL_ERR;
  }

  /* terminate CXM task if OFF state is enabled and policy 0 is sent */
  if ( COEX_SYS_ENABLED(CXM_SYS_BHVR_OFF_STATE_ENABLED) && 
       req_ptr->policy_valid && req_ptr->policy == CXM_POLICY_NONE )
  {
    cxm_terminate_task();
  }

  return req_cb_retval;
}

/*=============================================================================

  FUNCTION:  coex_handle_get_policy_req

=============================================================================*/
/*!
    @brief
    Function to handle client's query request of the current understanding
    of COEX policy followed by the service.

    @return
    qmi_csi_cb_error
*/
/*===========================================================================*/
qmi_csi_cb_error coex_handle_get_policy_req (
  void          *connection_handle,
  qmi_req_handle req_handle,
  unsigned int   msg_id,
  void          *req_c_struct,
  unsigned int   req_c_struct_len,
  void          *service_cookie
)
{
  qmi_csi_cb_error              req_cb_retval = QMI_CSI_CB_NO_ERR;
  coex_get_policy_resp_msg_v01  response;
  coex_tech_policy_params      *plcy_params;
  coex_carrier_info            *cc;
  /* TODO: this is only primary carrier for now */
  /*-----------------------------------------------------------------------*/
  /* Init the memory available for the REQ message */
  memset( &response, 0, sizeof( coex_get_policy_resp_msg_v01 ) );
  plcy_params = &coex_wwan_state_info[CXM_TECH_LTE].plcy_parms;
  cc = &coex_wwan_state_info[CXM_TECH_LTE].carrier[CXM_CARRIER_PCC];

  response.resp.result = QMI_RESULT_SUCCESS_V01;
  response.resp.error  = QMI_ERR_NONE_V01;

  /* copy fields from the stored info */
  response.policy_valid = TRUE;
  response.policy = cc->policy;
  response.power_threshold_valid = TRUE;
  response.power_threshold = cc->plcy_parms.power_threshold;
  response.rb_threshold_valid = TRUE;
  response.rb_threshold = cc->plcy_parms.rb_threshold;
  response.lte_tx_continuous_subframe_denials_threshold_valid = TRUE;
  response.lte_tx_continuous_subframe_denials_threshold = 
    plcy_params->tx_continuous_subframe_denials_threshold;
  response.lte_tx_subrame_denial_params_valid = TRUE;
  response.lte_tx_subrame_denial_params = plcy_params->tx_subrame_denial_params;
  response.apt_table_valid = TRUE;
  response.apt_table = plcy_params->params.lte.apt_table;
  response.controller_tx_power_limit_valid = TRUE;
  response.controller_tx_power_limit = 
    plcy_params->params.lte.controller_tx_power_limit;
  response.wci2_power_limit_valid = TRUE;
  response.wci2_power_limit = plcy_params->params.lte.wci2_power_limit;
  response.link_path_loss_threshold_valid = TRUE;
  response.link_path_loss_threshold = 
    coex_plcy_parms.link_path_loss_threshold;
  response.rb_filter_alpha_valid = TRUE;
  response.rb_filter_alpha = coex_plcy_parms.rb_filter_alpha;
  response.filtered_rb_threshold_valid = TRUE;
  response.filtered_rb_threshold = coex_plcy_parms.filtered_rb_threshold;
  response.wci2_tx_pwrlmt_timeout_valid = TRUE;
  response.wci2_tx_pwrlmt_timeout = plcy_params->params.lte.wci2_tx_pwrlmt_timeout;
  response.controller_tx_pwrlmt_timeout_valid = TRUE;
  response.controller_tx_pwrlmt_timeout = 
    plcy_params->params.lte.controller_tx_pwrlmt_timeout;
  response.tx_power_threshold_for_adv_tx_notice_valid = TRUE;
  response.tx_power_threshold_for_adv_tx_notice = 
    plcy_params->params.lte.tx_power_threshold_for_adv_tx_notice;
  response.rb_threshold_for_adv_tx_notice_valid = TRUE;
  response.rb_threshold_for_adv_tx_notice = 
    plcy_params->params.lte.rb_threshold_for_adv_tx_notice;
  response.holdoff_timer_for_rat_conn_state_valid = TRUE;
  response.holdoff_timer_for_rat_conn_state = 
    (uint16_t) coex_plcy_parms.t7_con_holdoff;
  response.filter_alpha_for_rat_power_state_valid = TRUE;
  response.filter_alpha_for_rat_power_state =
    coex_plcy_parms.t7_pwr_alpha;
  response.tx_power_threshold_for_rat_power_state_valid = TRUE;
  response.tx_power_threshold_for_rat_power_state =
    coex_plcy_parms.t7_pwr_thresh;
  response.holdoff_timer_for_rat_power_state_valid = TRUE;
  response.holdoff_timer_for_rat_power_state =
    (uint16_t) coex_plcy_parms.t7_pwr_holdoff;

  /* send QMI response */
  req_cb_retval = mqcsi_conn_mgr_send_resp_from_cb (
                    connection_handle,
                    req_handle,
                    msg_id,
                    &response,
                    sizeof( coex_get_policy_resp_msg_v01 )
                  );
  CXM_ASSERT( QMI_CSI_CB_NO_ERR == req_cb_retval );

  return req_cb_retval;
}

/*=============================================================================

  FUNCTION:  coex_handle_lte_metric_bler_start_req

=============================================================================*/
/*!
    @brief
    Function to handle the client's query request to start collecting/
    collating LTE BLER metrics

    @return
    qmi_csi_cb_error
*/
/*===========================================================================*/
qmi_csi_cb_error coex_handle_lte_metric_bler_start_req (
  void          *connection_handle,
  qmi_req_handle req_handle,
  unsigned int   msg_id,
  void          *req_c_struct,
  unsigned int   req_c_struct_len,
  void          *service_cookie
)
{
  qmi_csi_cb_error                           req_cb_retval = QMI_CSI_CB_NO_ERR;
  coex_metrics_lte_bler_start_resp_msg_v01   response;
  errno_enum_type                            retval = E_SUCCESS;
  coex_metrics_lte_bler_start_req_msg_v01    *request;
  cxm_coex_metrics_lte_bler_req_s            bler_start_req;
  /*-----------------------------------------------------------------------*/
  CXM_ASSERT( req_c_struct != NULL );

  /* Init the memory available for the REQ message */
  memset( &response, 0, sizeof( coex_metrics_lte_bler_start_resp_msg_v01 ) );
  request = (coex_metrics_lte_bler_start_req_msg_v01 *) req_c_struct;
  CXM_MSG_2( HIGH,
             "handling COEX_METRICS_BLER_START, tb_cnt=%d, err_thresh=%d",
             request->tb_cnt, request->threshold_err_tb_cnt );

  /* Implicity register for indications */
  retval = mqcsi_conn_mgr_update_client_ind_map(
              MQCSI_COEX_SERVICE_ID,
              connection_handle,
              QMI_COEX_METRICS_LTE_BLER_IND_V01,
              TRUE
           );

  if( E_SUCCESS != retval )
  {
    /* failed to register for indication */
    response.resp.result = QMI_RESULT_FAILURE_V01;
    response.resp.error  = QMI_ERR_INTERNAL_V01;
  }
  else if (coex_wwan_state_info[CXM_TECH_LTE].state_data.num_link_info_sets == 0)
  {
    /* LTE not currently active */
    response.resp.result = QMI_RESULT_FAILURE_V01;
    response.resp.error  = QMI_ERR_NO_RADIO_V01;
  }
  else
  {
    /* check bounds on request */
    if( request->tb_cnt == 0 || request->threshold_err_tb_cnt == 0 ||
        request->threshold_err_tb_cnt > request->tb_cnt )
    {
      response.resp.result = QMI_RESULT_FAILURE_V01;
      response.resp.error  = QMI_ERR_INVALID_ARG_V01;
    }
    else
    {
      /* check if bler already running */
      if( !coex_metrics_lte_bler_started )
      {
        coex_metrics_lte_bler_started = TRUE;
        CXM_TRACE( CXM_TRC_QMI_STRT_BLER_REQ,
                         request->tb_cnt, request->threshold_err_tb_cnt, 0,
                         CXM_TRC_AND_PKT_EN, CXM_LOG_METRICS_LTE_BLER_STARTED );

        /* Turn on the uart and send policy for lte subframe marker */
        if ( COEX_SYS_ENABLED(CXM_SYS_BHVR_WCI2_DATA) )
        {
           coex_manage_uart_npa_vote();
        }
        coex_prep_and_send_policy_msg( CXM_TECH_LTE );

        /* send start msgr msg to LTE */
        bler_start_req.action = CXM_ACTION_START;
        bler_start_req.payload.start_params.tb_count = request->tb_cnt;
        bler_start_req.payload.start_params.tb_err_count_thresh = request->threshold_err_tb_cnt;
        retval = cxm_msgr_send_msg( &bler_start_req.msg_hdr,
                                     MCS_CXM_COEX_METRICS_LTE_BLER_REQ,
                                     sizeof(cxm_coex_metrics_lte_bler_req_s) );
        CXM_ASSERT( E_SUCCESS == retval );
        response.resp.result = QMI_RESULT_SUCCESS_V01;
        response.resp.error  = QMI_ERR_NONE_V01;

        CXM_MSG_0( HIGH, "BLER started" );
      }
      else
      {
        /* bler already started - can't start again */
        response.resp.result = QMI_RESULT_FAILURE_V01;
        response.resp.error  = QMI_ERR_INTERNAL_V01;
      }
    }
  }
  
  /* send QMI response */
  req_cb_retval = mqcsi_conn_mgr_send_resp_from_cb (
                    connection_handle,
                    req_handle,
                    msg_id,
                    &response,
                    sizeof( coex_metrics_lte_bler_start_resp_msg_v01 )
                  );
  CXM_ASSERT( QMI_CSI_CB_NO_ERR == req_cb_retval );

  return req_cb_retval;
}

/*=============================================================================

  FUNCTION:  coex_handle_lte_metric_bler_stop_req

=============================================================================*/
/*!
    @brief
    Function to handle the client's query request to stop collecting LTE
    BLER metrics

    @return
    qmi_csi_cb_error
*/
/*===========================================================================*/
qmi_csi_cb_error coex_handle_lte_metric_bler_stop_req (
  void          *connection_handle,
  qmi_req_handle req_handle,
  unsigned int   msg_id,
  void          *req_c_struct,
  unsigned int   req_c_struct_len,
  void          *service_cookie
)
{
  qmi_csi_cb_error             req_cb_retval = QMI_CSI_CB_NO_ERR;
  coex_metrics_lte_bler_stop_resp_msg_v01 response;
  errno_enum_type              retval = E_SUCCESS;
  cxm_coex_metrics_lte_bler_req_s            msgr_stop_bler_req;
  /*-----------------------------------------------------------------------*/
  /* Init the memory available for the REQ message */
  memset( &response, 0, sizeof( coex_metrics_lte_bler_stop_resp_msg_v01 ) );
  
  /* check if bler has been started first */
  if( coex_metrics_lte_bler_started )
  {
    /* forward stop message to LTE */
    msgr_stop_bler_req.action = CXM_ACTION_STOP;
    retval = cxm_msgr_send_msg( &msgr_stop_bler_req.msg_hdr,
                                MCS_CXM_COEX_METRICS_LTE_BLER_REQ,
                                sizeof(cxm_coex_metrics_lte_bler_req_s) );
    CXM_ASSERT( E_SUCCESS == retval );

    coex_metrics_lte_bler_started = FALSE;

    /*Turn off the uart and send policy for lte subframe marker*/
    if ( COEX_SYS_ENABLED(CXM_SYS_BHVR_WCI2_DATA) )
    {
       coex_manage_uart_npa_vote();
    }
    coex_prep_and_send_policy_msg( CXM_TECH_LTE );

    response.resp.result = QMI_RESULT_SUCCESS_V01;
    response.resp.error  = QMI_ERR_NONE_V01;

    CXM_MSG_0( HIGH, "BLER stopped" );
  }
  else
  {
    /* bler hasn't been started yet */
    response.resp.result = QMI_RESULT_FAILURE_V01;
    response.resp.error  = QMI_ERR_INVALID_OPERATION_V01;
  }

  /* send QMI response */
  req_cb_retval = mqcsi_conn_mgr_send_resp_from_cb (
                    connection_handle,
                    req_handle,
                    msg_id,
                    &response,
                    sizeof( coex_metrics_lte_bler_stop_resp_msg_v01 )
                  );
  CXM_ASSERT( QMI_CSI_CB_NO_ERR == req_cb_retval );

  return req_cb_retval;
}

/*=============================================================================

  FUNCTION:  coex_handle_start_metrics_req_over_qmi

=============================================================================*/
/*!
    @brief
    Function to handle the client's query request to start collecting/
    collating SINR metrics

    @return
    none
*/
/*===========================================================================*/
void coex_handle_start_metrics_req_over_qmi (
  coex_wwan_tech_v01     tech,
  qmi_response_type_v01 *resp,
  float                  alpha,
  coex_carrier_v01       carrier
)
{
  uint32          alpha_q8;
  errno_enum_type retval = E_SUCCESS;
  cxm_carrier_e   cc;
  /*-----------------------------------------------------------------------*/ 
  if( COEX_SYS_ENABLED(CXM_SYS_BHVR_QMI_METRICS) )
  {
    /* convert and check bounds on alpha */
    retval = cxm_float_to_unsigned_Q8( alpha, &alpha_q8 );

    /* if LTE, validate component carrier id */
    cc = (tech == COEX_LTE_TECH_V01) ? 
            coex_carrier_idl_to_cxm_transl(carrier) : CXM_CARRIER_PCC;

    if (coex_wwan_state_info[tech].state_data.num_link_info_sets == 0)
    {
      /* tech not currently active */
      resp->result = QMI_RESULT_FAILURE_V01;
      resp->error  = QMI_ERR_NO_RADIO_V01;
    }
    /* validate alpha. Only supported for LTE, TDS, GSM. */
    else if( retval == E_SUCCESS && alpha_q8 <= COEX_LTE_METRICS_MAX_ALPHA 
             && (tech == COEX_LTE_TECH_V01 || tech == COEX_TDSCDMA_TECH_V01 ||
                 tech == COEX_GSM_TECH_V01 || tech == COEX_GSM2_TECH_V01 ||
                 tech == COEX_GSM3_TECH_V01 ) 
             && (cc < CXM_CARRIER_MAX || 
                 carrier == COEX_CARRIER_ALL_V01_INTERNAL) )
    {
      /* check if metrics already running */
      if( cc == CXM_CARRIER_MAX )
      {
        /* special value -- Start metrics for ALL carriers that are active */
        for( cc = CXM_CARRIER_PCC; cc < CXM_CARRIER_MAX; cc++ )
        {
          if( !COEX_METRICS_STARTED(tech, cc) && 
              coex_wwan_state_info[tech].carrier[cc].conn_state !=
              CXM_TECH_STATE_INACTIVE )
          {
            coex_tech_metrics_initiate_req( tech, CXM_ACTION_START, alpha_q8, 0, cc );
          }
        }
        resp->result = QMI_RESULT_SUCCESS_V01;
        resp->error  = QMI_ERR_NONE_V01;
      }
      else if( !COEX_METRICS_STARTED(tech, cc) )
      {
        /* Tell tech to start metrics (just one carrier) */
        coex_tech_metrics_initiate_req( tech, CXM_ACTION_START, alpha_q8, 0, cc );

        resp->result = QMI_RESULT_SUCCESS_V01;
        resp->error  = QMI_ERR_NONE_V01;
      }
      else
      {
        /* metrics already started - can't start again */
        resp->result = QMI_RESULT_FAILURE_V01;
        resp->error  = QMI_ERR_INVALID_OPERATION_V01;
      }
    }
    else
    {
      /* metrics or tech from request not valid */
      resp->result = QMI_RESULT_FAILURE_V01;
      resp->error  = QMI_ERR_INVALID_ARG_V01;
    }
  }
  else
  {
    /* metrics not supported over QMI interface */
    resp->result = QMI_RESULT_FAILURE_V01;
    resp->error  = QMI_ERR_REQUESTED_NUM_UNSUPPORTED_V01;
  }
}

/*=============================================================================

  FUNCTION:  coex_handle_read_metrics_req_over_qmi

=============================================================================*/
/*!
    @brief
    Function to handle the client's query request to start collecting/
    collating SINR metrics. Returns true if the reponse is deferred.

    @return
    boolean
*/
/*===========================================================================*/
boolean coex_handle_read_metrics_req_over_qmi (
  coex_wwan_tech_v01     tech,
  qmi_response_type_v01 *resp,
  void                  *connection_handle,
  qmi_req_handle         req_handle,
  unsigned int           msg_id,
  coex_carrier_v01       carrier
)
{
  boolean             msg_deferred = FALSE;
  qmi_csi_error       push_req_error;
  uint32              deferred_req_id;
  cxm_carrier_e       cc;
  /*-----------------------------------------------------------------------*/
  if( COEX_SYS_ENABLED(CXM_SYS_BHVR_QMI_METRICS) )
  {
    /* if LTE, validate component carrier id */
    cc = (tech == COEX_LTE_TECH_V01) ? 
            coex_carrier_idl_to_cxm_transl(carrier) : CXM_CARRIER_PCC;

    if ( cc >= CXM_CARRIER_MAX ||
         (tech != COEX_LTE_TECH_V01 && tech != COEX_TDSCDMA_TECH_V01 &&
          tech != COEX_GSM_TECH_V01 && tech != COEX_GSM2_TECH_V01 &&
          tech != COEX_GSM3_TECH_V01) )
    {
      /* tech from request not valid */
      resp->result = QMI_RESULT_FAILURE_V01;
      resp->error  = QMI_ERR_INVALID_ARG_V01;
    }

    /* check if currently doing SINR; if not, read_req is not appropriate */
    else if( COEX_METRICS_STARTED(tech, cc) )
    {
      /* push qmi req info onto delayed response stack to recover when
         read cnf response from LTE received */
      push_req_error = mqcsi_conn_mgr_push_deferred_req( MQCSI_COEX_SERVICE_ID,
                         connection_handle,
                         req_handle,
                         msg_id,
                         &deferred_req_id );
      CXM_ASSERT( QMI_CSI_NO_ERR == push_req_error );

      /* ask tech to read the info for us */
      coex_tech_metrics_initiate_req( tech, CXM_ACTION_READ, 0, 
                                      deferred_req_id, cc );
      msg_deferred = TRUE;
    }
    else
    {
      /* SINR not started - send error response */
      resp->result = QMI_RESULT_FAILURE_V01;
      resp->error  = QMI_ERR_INVALID_OPERATION_V01;
    }
  }
  else
  {
    /* LTE metrics not supported over QMI interface */
    resp->result = QMI_RESULT_FAILURE_V01;
    resp->error  = QMI_ERR_REQUESTED_NUM_UNSUPPORTED_V01;
  }

  return msg_deferred;
}

/*=============================================================================

  FUNCTION:  coex_handle_stop_metrics_req_over_qmi

=============================================================================*/
/*!
    @brief
    Function to handle the client's query request to stop collecting/
    collating SINR metrics

    @return
    none
*/
/*===========================================================================*/
void coex_handle_stop_metrics_req_over_qmi (
  coex_wwan_tech_v01     tech,
  qmi_response_type_v01 *resp,
  coex_carrier_v01       carrier
)
{
  cxm_carrier_e       cc;
  /*-----------------------------------------------------------------------*/
  if( COEX_SYS_ENABLED(CXM_SYS_BHVR_QMI_METRICS) )
  {
    /* if LTE, validate component carrier id */
    cc = (tech == COEX_LTE_TECH_V01) ? 
            coex_carrier_idl_to_cxm_transl(carrier) : CXM_CARRIER_PCC;

    /* handle special 'ALL CARRIERS' value */
    if( (cc >= CXM_CARRIER_MAX && carrier != COEX_CARRIER_ALL_V01_INTERNAL) ||
        (tech != COEX_LTE_TECH_V01 && tech != COEX_TDSCDMA_TECH_V01 &&
         tech != COEX_GSM_TECH_V01 && tech != COEX_GSM2_TECH_V01 &&
         tech != COEX_GSM3_TECH_V01) )
    {
      /* tech from request not valid */
      resp->result = QMI_RESULT_FAILURE_V01;
      resp->error  = QMI_ERR_INVALID_ARG_V01;
    }
    else if( cc == CXM_CARRIER_MAX )
    {
      /* special value -- stop metrics for ALL carriers that are active */
      for( cc = CXM_CARRIER_PCC; cc < CXM_CARRIER_MAX; cc++ )
      {
        if( COEX_METRICS_STARTED(tech, cc) )
        {
          coex_tech_metrics_initiate_req( tech, CXM_ACTION_STOP, 0, 0, cc );
        }
      }
      resp->result = QMI_RESULT_SUCCESS_V01;
      resp->error  = QMI_ERR_NONE_V01;
    }
    /* single-CC REQ only appropriate if SINR has been started */
    else if( COEX_METRICS_STARTED(tech, cc) )
    {
      /* tell LTE to stop SINR */
      coex_tech_metrics_initiate_req( tech, CXM_ACTION_STOP, 0, 0, cc );

      resp->result = QMI_RESULT_SUCCESS_V01;
      resp->error  = QMI_ERR_NONE_V01;
    }
    else
    {
      /* SINR not started yet */
      resp->result = QMI_RESULT_FAILURE_V01;
      resp->error  = QMI_ERR_INVALID_OPERATION_V01;
    }
  }
  else
  {
    resp->result = QMI_RESULT_FAILURE_V01;
    resp->error  = QMI_ERR_REQUESTED_NUM_UNSUPPORTED_V01;
  }
}

/*=============================================================================

  FUNCTION:  coex_handle_lte_metric_sinr_start_req

=============================================================================*/
/*!
    @brief
    Function to handle the client's query request to start collecting/
    collating LTE SINR metrics

    @return
    qmi_csi_cb_error
*/
/*===========================================================================*/
qmi_csi_cb_error coex_handle_lte_metric_sinr_start_req (
  void          *connection_handle,
  qmi_req_handle req_handle,
  unsigned int   msg_id,
  void          *req_c_struct,
  unsigned int   req_c_struct_len,
  void          *service_cookie
)
{
  qmi_csi_cb_error                         req_cb_retval = QMI_CSI_CB_NO_ERR;
  coex_metrics_lte_sinr_start_resp_msg_v01 response;
  coex_metrics_lte_sinr_start_req_msg_v01 *req;
  coex_carrier_v01                         cc;
  /*-----------------------------------------------------------------------*/
  CXM_ASSERT( req_c_struct != NULL );

  /* Init the memory available for the REQ message */
  memset( &response, 0, sizeof( coex_metrics_lte_sinr_start_resp_msg_v01 ) );

  /* forward the request to LTE */
  req = (coex_metrics_lte_sinr_start_req_msg_v01 *) req_c_struct;
  cc = (req->carrier_valid) ? req->carrier: COEX_CARRIER_PRIMARY_V01;
  coex_handle_start_metrics_req_over_qmi( COEX_LTE_TECH_V01, 
                                                &response.resp, 
                                                req->alpha, cc );

  /* send QMI response */
  response.carrier_valid = TRUE;
  response.carrier = cc;
  req_cb_retval = mqcsi_conn_mgr_send_resp_from_cb (
                    connection_handle,
                    req_handle,
                    msg_id,
                    &response,
                    sizeof( coex_metrics_lte_sinr_start_resp_msg_v01 )
                  );
  CXM_ASSERT( QMI_CSI_CB_NO_ERR == req_cb_retval );

  return req_cb_retval;
}

/*=============================================================================

  FUNCTION:  coex_handle_lte_metric_sinr_read_req

=============================================================================*/
/*!
    @brief
    Function to handle the client's query request to read current filter
    output for LTE SINR metrics

    @return
    qmi_csi_cb_error
*/
/*===========================================================================*/
qmi_csi_cb_error coex_handle_lte_metric_sinr_read_req (
  void          *connection_handle,
  qmi_req_handle req_handle,
  unsigned int   msg_id,
  void          *req_c_struct,
  unsigned int   req_c_struct_len,
  void          *service_cookie
)
{
  qmi_csi_cb_error                        req_cb_retval = QMI_CSI_CB_NO_ERR;
  coex_metrics_lte_sinr_read_resp_msg_v01 response;
  coex_metrics_lte_sinr_read_req_msg_v01 *req;
  boolean                                 msg_deferred;
  coex_carrier_v01                        cc;
  /*-----------------------------------------------------------------------*/
  memset( &response, 0, sizeof( coex_metrics_lte_sinr_read_resp_msg_v01 ) );

  req = (coex_metrics_lte_sinr_read_req_msg_v01*) req_c_struct;
  cc = (req->carrier_valid) ? req->carrier : COEX_CARRIER_PRIMARY_V01;
  msg_deferred = coex_handle_read_metrics_req_over_qmi ( 
                   COEX_LTE_TECH_V01, &response.resp, connection_handle, 
                   req_handle, msg_id, cc );
  if ( !msg_deferred  )
  {
    response.carrier_valid = TRUE;
    response.carrier = cc;
    req_cb_retval = mqcsi_conn_mgr_send_resp_from_cb (
                      connection_handle,
                      req_handle,
                      msg_id,
                      &response,
                      sizeof( coex_metrics_lte_sinr_read_resp_msg_v01 )
                    );
    CXM_ASSERT( QMI_CSI_CB_NO_ERR == req_cb_retval );
  }

  return req_cb_retval;
}

/*=============================================================================

  FUNCTION:  coex_handle_lte_metric_sinr_stop_req

=============================================================================*/
/*!
    @brief
    Function to handle the client's query request to stop collecting/collating
    LTE SINR metrics

    @return
    qmi_csi_cb_error
*/
/*===========================================================================*/
qmi_csi_cb_error coex_handle_lte_metric_sinr_stop_req (
  void          *connection_handle,
  qmi_req_handle req_handle,
  unsigned int   msg_id,
  void          *req_c_struct,
  unsigned int   req_c_struct_len,
  void          *service_cookie
)
{
  qmi_csi_cb_error                        req_cb_retval = QMI_CSI_CB_NO_ERR;
  coex_metrics_lte_sinr_stop_resp_msg_v01 response;
  coex_metrics_lte_sinr_stop_req_msg_v01 *req;
  coex_carrier_v01                        cc;
  /*-----------------------------------------------------------------------*/
    /* Init the memory available for the REQ message */
  memset( &response, 0, sizeof( coex_metrics_lte_sinr_stop_resp_msg_v01 ) );

  req = (coex_metrics_lte_sinr_stop_req_msg_v01*) req_c_struct;
  cc = (req->carrier_valid) ? req->carrier : COEX_CARRIER_PRIMARY_V01;
  coex_handle_stop_metrics_req_over_qmi ( COEX_LTE_TECH_V01, &response.resp, 
                                          cc );

  /* send QMI response */
  response.carrier_valid = TRUE;
  response.carrier = cc;
  req_cb_retval = mqcsi_conn_mgr_send_resp_from_cb (
                    connection_handle,
                    req_handle,
                    msg_id,
                    &response,
                    sizeof( coex_metrics_lte_sinr_stop_resp_msg_v01 )
                  );
  CXM_ASSERT( QMI_CSI_CB_NO_ERR == req_cb_retval );

  return req_cb_retval;
}

/*=============================================================================

  FUNCTION:  coex_handle_tech_metric_start_req

=============================================================================*/
/*!
    @brief
    Function to handle the client's query request to start collecting/
    collating metrics for a tech

    @return
    qmi_csi_cb_error
*/
/*===========================================================================*/
qmi_csi_cb_error coex_handle_tech_metric_start_req (
  void          *connection_handle,
  qmi_req_handle req_handle,
  unsigned int   msg_id,
  void          *req_c_struct,
  unsigned int   req_c_struct_len,
  void          *service_cookie
)
{
  qmi_csi_cb_error                req_cb_retval = QMI_CSI_CB_NO_ERR;
  coex_metrics_start_resp_msg_v01 response;
  coex_metrics_start_req_msg_v01 *req;
  coex_carrier_v01                cc;
  /*-----------------------------------------------------------------------*/
  CXM_ASSERT( req_c_struct != NULL );

  /* Init the memory available for the REQ message */
  memset( &response, 0, sizeof( coex_metrics_start_resp_msg_v01 ) );

  req = (coex_metrics_start_req_msg_v01 *) req_c_struct;
  cc = (req->carrier_valid) ? req->carrier : COEX_CARRIER_PRIMARY_V01;
  coex_handle_start_metrics_req_over_qmi( req->tech, &response.resp, 
                                          req->alpha, cc );

  /* set the tech the start request is for */
  response.tech_valid = TRUE;
  response.tech = req->tech;
  if( response.tech == COEX_LTE_TECH_V01 )
  {
    response.carrier_valid = TRUE;
    response.carrier = cc;
  }

  /* send QMI response */
  req_cb_retval = mqcsi_conn_mgr_send_resp_from_cb (
                    connection_handle,
                    req_handle,
                    msg_id,
                    &response,
                    sizeof( coex_metrics_start_resp_msg_v01 )
                  );
  CXM_ASSERT( QMI_CSI_CB_NO_ERR == req_cb_retval );

  return req_cb_retval;
}

/*=============================================================================

  FUNCTION:  coex_handle_tech_metric_read_req

=============================================================================*/
/*!
    @brief
    Function to handle the client's query request to read current filter
    output for metrics

    @return
    qmi_csi_cb_error
*/
/*===========================================================================*/
qmi_csi_cb_error coex_handle_tech_metric_read_req (
  void          *connection_handle,
  qmi_req_handle req_handle,
  unsigned int   msg_id,
  void          *req_c_struct,
  unsigned int   req_c_struct_len,
  void          *service_cookie
)
{
  qmi_csi_cb_error               req_cb_retval = QMI_CSI_CB_NO_ERR;
  coex_metrics_read_resp_msg_v01 response;
  coex_metrics_read_req_msg_v01 *request;
  boolean                        msg_deferred;
  coex_carrier_v01               cc;
  /*-----------------------------------------------------------------------*/
  CXM_ASSERT( req_c_struct != NULL );

  memset( &response, 0, sizeof( coex_metrics_read_resp_msg_v01 ) );
  request = (coex_metrics_read_req_msg_v01 *) req_c_struct;

  cc= (request->carrier_valid) ? request->carrier : COEX_CARRIER_PRIMARY_V01;
  msg_deferred = coex_handle_read_metrics_req_over_qmi ( 
                   request->tech, &response.resp, connection_handle, 
                   req_handle, msg_id, cc );
  if ( !msg_deferred  )
  {
    response.tech_valid = TRUE;
    response.tech = request->tech;
    if( response.tech == COEX_LTE_TECH_V01 )
    {
      response.carrier_valid = TRUE;
      response.carrier = cc;
    }
    req_cb_retval = mqcsi_conn_mgr_send_resp_from_cb (
                      connection_handle,
                      req_handle,
                      msg_id,
                      &response,
                      sizeof( coex_metrics_read_resp_msg_v01 )
                    );
    CXM_ASSERT( QMI_CSI_CB_NO_ERR == req_cb_retval );
  }

  return req_cb_retval;
}

/*=============================================================================

  FUNCTION:  coex_handle_tech_metric_stop_req

=============================================================================*/
/*!
    @brief
    Function to handle the client's query request to stop collecting/collating
    metrics

    @return
    qmi_csi_cb_error
*/
/*===========================================================================*/
qmi_csi_cb_error coex_handle_tech_metric_stop_req (
  void          *connection_handle,
  qmi_req_handle req_handle,
  unsigned int   msg_id,
  void          *req_c_struct,
  unsigned int   req_c_struct_len,
  void          *service_cookie
)
{
  qmi_csi_cb_error               req_cb_retval = QMI_CSI_CB_NO_ERR;
  coex_metrics_stop_resp_msg_v01 response;
  coex_metrics_stop_req_msg_v01 *req;
  coex_carrier_v01               cc;
  /*-----------------------------------------------------------------------*/
  CXM_ASSERT( req_c_struct != NULL );

  /* Init the memory available for the REQ message */
  memset( &response, 0, sizeof( coex_metrics_stop_resp_msg_v01 ) );
  req= (coex_metrics_stop_req_msg_v01 *) req_c_struct;

  cc = (req->carrier_valid) ? req->carrier : COEX_CARRIER_PRIMARY_V01;
  coex_handle_stop_metrics_req_over_qmi ( req->tech, &response.resp, cc );

  /* send QMI response */
  response.tech_valid = TRUE;
  response.tech = req->tech;
  if( response.tech == COEX_LTE_TECH_V01 )
  {
    response.carrier_valid = TRUE;
    response.carrier = cc;
  }
  req_cb_retval = mqcsi_conn_mgr_send_resp_from_cb (
                    connection_handle,
                    req_handle,
                    msg_id,
                    &response,
                    sizeof( coex_metrics_stop_resp_msg_v01 )
                  );
  CXM_ASSERT( QMI_CSI_CB_NO_ERR == req_cb_retval );

  return req_cb_retval;
}

/*=============================================================================

  FUNCTION:  coex_handle_set_band_filter_info_req

=============================================================================*/
/*!
    @brief
    Function to handle QMI req to set band filter info

    @return
    qmi_csi_cb_error
*/
/*===========================================================================*/
qmi_csi_cb_error coex_handle_set_band_filter_info_req (
  void          *connection_handle,
  qmi_req_handle req_handle,
  unsigned int   msg_id,
  void          *req_c_struct,
  unsigned int   req_c_struct_len,
  void          *service_cookie
)
{
  qmi_csi_cb_error                       req_cb_retval = QMI_CSI_CB_NO_ERR;
  qmi_csi_error                          qmi_retval = QMI_CSI_NO_ERR;
  coex_set_band_filter_info_resp_msg_v01 response;
  coex_set_band_filter_info_req_msg_v01 *request;
  uint8 i;
  uint8 ul_count = 0;
  uint8 dl_count = 0;
  /*-----------------------------------------------------------------------*/
  CXM_ASSERT( req_c_struct != NULL );

  /* De-mystify the received message pointer to the appropriate type */
  request = (coex_set_band_filter_info_req_msg_v01 *) req_c_struct;

  /* Init the memory available for the REQ and LTE ML1 message */
  memset( &response, 0, sizeof( coex_set_band_filter_info_resp_msg_v01 ) );

  /*clear existing band filter info*/  
  memset( &coex_band_filter_info, 0, sizeof( coex_band_filter_s ) );

  response.resp.result = QMI_RESULT_SUCCESS_V01;
  response.resp.error  = QMI_ERR_NONE_V01;

  /* forward band filter info to LTE ML1 */
  if(request->bands_valid)
  {
    if( request->bands_len > COEX_WWAN_MAX_BANDS_TO_MONITOR_V01 )
    {
      response.resp.result = QMI_RESULT_FAILURE_V01;
      response.resp.error  = QMI_ERR_INVALID_ARG_V01;
    }
    else
    {
      for( i = 0; i < request->bands_len; i++ )
      {
        if ( (request->bands[i].band_mask & COEX_ENABLE_UL_BAND_INFO_V01) &&  request->bands[i].band_info.ul_band.freq != 0 ) 
        {
          coex_band_filter_info.ul_filter[ul_count].freq_stop = 
            ( request->bands[i].band_info.ul_band.freq + (request->bands[i].band_info.ul_band.bandwidth/2) ) * MHZ_KHZ_CONVERSION;

          coex_band_filter_info.ul_filter[ul_count].freq_start = 
            ( request->bands[i].band_info.ul_band.freq - (request->bands[i].band_info.ul_band.bandwidth/2) ) * MHZ_KHZ_CONVERSION;

          ul_count++;

        }
        if ( (request->bands[i].band_mask & COEX_ENABLE_DL_BAND_INFO_V01) &&  request->bands[i].band_info.dl_band.freq != 0 ) 
        {
          coex_band_filter_info.dl_filter[dl_count].freq_stop = 
            ( request->bands[i].band_info.dl_band.freq + (request->bands[i].band_info.dl_band.bandwidth/2) ) * MHZ_KHZ_CONVERSION;

          coex_band_filter_info.dl_filter[dl_count].freq_start = 
            ( request->bands[i].band_info.dl_band.freq - (request->bands[i].band_info.dl_band.bandwidth/2) ) * MHZ_KHZ_CONVERSION;

          dl_count++;

        }
      }

      coex_band_filter_info.ul_len = ul_count;
      coex_band_filter_info.dl_len = dl_count;
      if (request->bands_len > 0) 
      {
        coex_band_filter_info.filter_on = TRUE;
      }
      else
      {
        coex_band_filter_info.filter_on = FALSE;
      }
    }
  }
  /*set the band filter for all techs*/
  for (i=0; i < CXM_TECH_MAX; i++)
  {
    coex_apply_tech_band_filter(i);
  }

  /* send QMI response */
  req_cb_retval = mqcsi_conn_mgr_send_resp_from_cb (
                    connection_handle,
                    req_handle,
                    msg_id,
                    &response,
                    sizeof( coex_set_band_filter_info_resp_msg_v01 )
                  );
  CXM_ASSERT( QMI_CSI_CB_NO_ERR == req_cb_retval );

  /* Prepare & send out the IND message*/
  qmi_retval = coex_prep_and_send_wwan_state_ind( TRUE );

  /* although we lose info here, it is recorded in an f3 inside
   * the above function */
  if( QMI_CSI_NO_ERR != qmi_retval )
  {
    req_cb_retval = QMI_CSI_CB_INTERNAL_ERR;
  }

  CXM_TRACE(0, 0, 0, 0, CXM_LOG_PKT_EN, CXM_LOG_BAND_FILTER_INFO);

  return req_cb_retval;
}

/*=============================================================================

  FUNCTION:  coex_handle_get_band_filter_info_req

=============================================================================*/
/*!
    @brief
    Function to handle QMI req to get band filter info

    @return
    qmi_csi_cb_error
*/
/*===========================================================================*/
qmi_csi_cb_error coex_handle_get_band_filter_info_req (
  void          *connection_handle,
  qmi_req_handle req_handle,
  unsigned int   msg_id,
  void          *req_c_struct,
  unsigned int   req_c_struct_len,
  void          *service_cookie
)
{
  qmi_csi_cb_error                       req_cb_retval = QMI_CSI_CB_NO_ERR;
  coex_get_band_filter_info_resp_msg_v01 response;
  uint8 i;
  /*-----------------------------------------------------------------------*/
  /* Init the memory available for the REQ message */
  memset( &response, 0, sizeof( coex_get_band_filter_info_resp_msg_v01 ) );

  response.resp.result = QMI_RESULT_SUCCESS_V01;
  response.resp.error  = QMI_ERR_NONE_V01;

  response.bands_valid = TRUE;

  for (i=0; i < coex_band_filter_info.ul_len; i++) 
  {
    response.bands[i].band_info.ul_band.freq      = 
      ( (coex_band_filter_info.ul_filter[i].freq_stop + coex_band_filter_info.ul_filter[i].freq_start)/2 ) / MHZ_KHZ_CONVERSION;
    response.bands[i].band_info.ul_band.bandwidth = 
      ( coex_band_filter_info.ul_filter[i].freq_stop - coex_band_filter_info.ul_filter[i].freq_start ) / MHZ_KHZ_CONVERSION;
    response.bands[i].band_mask |= COEX_ENABLE_UL_BAND_INFO_V01;
  }

  for (i=0; i < coex_band_filter_info.dl_len; i++) 
  {
    response.bands[i].band_info.dl_band.freq      = 
      ( (coex_band_filter_info.dl_filter[i].freq_stop + coex_band_filter_info.dl_filter[i].freq_start)/2 ) / MHZ_KHZ_CONVERSION;
    response.bands[i].band_info.dl_band.bandwidth = 
      ( coex_band_filter_info.dl_filter[i].freq_stop - coex_band_filter_info.dl_filter[i].freq_start ) / MHZ_KHZ_CONVERSION;
    response.bands[i].band_mask |= COEX_ENABLE_DL_BAND_INFO_V01;
  }

  response.bands_len = coex_band_filter_info.ul_len > coex_band_filter_info.dl_len ? 
                          coex_band_filter_info.ul_len : coex_band_filter_info.dl_len;
  
  /* send QMI response */
  req_cb_retval = mqcsi_conn_mgr_send_resp_from_cb (
                    connection_handle,
                    req_handle,
                    msg_id,
                    &response,
                    sizeof( coex_get_band_filter_info_resp_msg_v01 )
                  );
  CXM_ASSERT( QMI_CSI_CB_NO_ERR == req_cb_retval );

  return req_cb_retval;
}

/*=============================================================================

  FUNCTION:  coex_handle_get_wci2_mws_params_req

=============================================================================*/
/*!
    @brief
    Function to handle QMI req to query the type0 offset/jitter parameters

    @return
    qmi_csi_cb_error
*/
/*===========================================================================*/
qmi_csi_cb_error coex_handle_get_wci2_mws_params_req (
  void          *connection_handle,
  qmi_req_handle req_handle,
  unsigned int   msg_id,
  void          *req_c_struct,
  unsigned int   req_c_struct_len,
  void          *service_cookie
)
{
  qmi_csi_cb_error                      req_cb_retval = QMI_CSI_CB_NO_ERR;
  coex_get_wci2_mws_params_resp_msg_v01 response;
  uint8                                 char_jitter;
  wci2_uart_baud_type_e                 baud_rate;
  /*-----------------------------------------------------------------------*/
  /* Init the memory available for the REQ message */
  memset( &response, 0, sizeof( coex_get_wci2_mws_params_resp_msg_v01 ) );

  response.resp.result = QMI_RESULT_SUCCESS_V01;
  response.resp.error  = QMI_ERR_NONE_V01;

  baud_rate = wci2_uart_get_baud();

  if(baud_rate == WCI2_UART_BAUD_115200)
  {
    char_jitter = COEX_UART_JITTER_115200;
  }
  else if(baud_rate == WCI2_UART_BAUD_2000000)
  {
    char_jitter = COEX_UART_JITTER_2000000;
  }
  else
  {
    /* 3Mbps */
    char_jitter = COEX_UART_JITTER_3000000;
  }

  /* fill in params. Note: all values in uSec */
  response.mws_frame_sync_assert_offset_valid = TRUE;
  response.mws_frame_sync_assert_offset.min = -60;
  response.mws_frame_sync_assert_offset.max = -60;
  response.mws_frame_sync_assert_jitter_valid = TRUE;
  response.mws_frame_sync_assert_jitter.min = -3;
  response.mws_frame_sync_assert_jitter.max =  3;
  response.mws_rx_assert_offset_valid = TRUE;
  response.mws_rx_assert_offset.min   = -40;
  response.mws_rx_assert_offset.max   = -40;
  response.mws_rx_assert_jitter_valid = TRUE;
  response.mws_rx_assert_jitter.min   = -char_jitter;
  response.mws_rx_assert_jitter.max   =  char_jitter;
  response.mws_rx_deassert_offset_valid = TRUE;
  response.mws_rx_deassert_offset.min = 40;
  response.mws_rx_deassert_offset.max = 40;
  response.mws_rx_deassert_jitter_valid = TRUE;
  response.mws_rx_deassert_jitter.min = -char_jitter;
  response.mws_rx_deassert_jitter.max =  char_jitter;
  response.mws_tx_assert_offset_valid = TRUE;
  response.mws_tx_assert_offset.min   = -50;
  response.mws_tx_assert_offset.max   = -50;
  response.mws_tx_assert_jitter_valid = TRUE;
  response.mws_tx_assert_jitter.min   = -char_jitter;
  response.mws_tx_assert_jitter.max   =  char_jitter;
  response.mws_tx_deassert_offset_valid = TRUE;
  response.mws_tx_deassert_offset.min = -50;
  response.mws_tx_deassert_offset.max = -50;
  response.mws_tx_deassert_jitter_valid = TRUE;
  response.mws_tx_deassert_jitter.min = -char_jitter;
  response.mws_tx_deassert_jitter.max =  char_jitter;

  /* send QMI response */
  req_cb_retval = mqcsi_conn_mgr_send_resp_from_cb (
                    connection_handle,
                    req_handle,
                    msg_id,
                    &response,
                    sizeof( coex_get_wci2_mws_params_resp_msg_v01 )
                  );
  CXM_ASSERT( QMI_CSI_CB_NO_ERR == req_cb_retval );

  return req_cb_retval;
}


/*=============================================================================

  FUNCTION:  coex_handle_set_sleep_notification_thresh_req

=============================================================================*/
/*!
    @brief
    Function to handle QMI req to set sleep notification threshold for a tech

    @return
    qmi_csi_cb_error
*/
/*===========================================================================*/
qmi_csi_cb_error coex_handle_set_sleep_notification_thresh_req (
  void          *connection_handle,
  qmi_req_handle req_handle,
  unsigned int   msg_id,
  void          *req_c_struct,
  unsigned int   req_c_struct_len,
  void          *service_cookie
)
{
  qmi_csi_cb_error                           req_cb_retval = QMI_CSI_CB_NO_ERR;
  coex_set_sleep_notification_resp_msg_v01   response;
  coex_set_sleep_notification_req_msg_v01   *request;
  /*-----------------------------------------------------------------------*/
  CXM_ASSERT( req_c_struct != NULL );  

  /* De-mystify the received message pointer to the appropriate type */
  request = (coex_set_sleep_notification_req_msg_v01 *) req_c_struct;  

  /* Init the memory available for the resp message */
  memset( &response, 0, sizeof( coex_set_sleep_notification_resp_msg_v01 ) );
  
  if( request->tech > (coex_wwan_tech_v01)CXM_TECH_DFLT_INVLD && 
              request->tech < (coex_wwan_tech_v01)CXM_TECH_MAX )
  {  
    /*check the technolgy entry and store in the appropriate field*/
    coex_wwan_state_info[request->tech].sleep_thresh = request->off_period_threshold;

    response.resp.result = QMI_RESULT_SUCCESS_V01;
    response.resp.error  = QMI_ERR_NONE_V01;
  }
  else
  {
    response.resp.result = QMI_RESULT_FAILURE_V01;
    response.resp.error  = QMI_ERR_INVALID_ARG_V01;
  }
  /* send QMI response */
  req_cb_retval = mqcsi_conn_mgr_send_resp_from_cb (
                    connection_handle,
                    req_handle,
                    msg_id,
                    &response,
                    sizeof( coex_set_sleep_notification_resp_msg_v01 )
                  );
  CXM_ASSERT( QMI_CSI_CB_NO_ERR == req_cb_retval );

  CXM_TRACE( CXM_TRC_QMI_SET_SLP_NOTFY_THRESH, request->tech, 
                   request->off_period_threshold, 0, CXM_TRC_EN );

  return req_cb_retval;
}

/*=============================================================================

  FUNCTION:  coex_handle_get_sleep_notification_thresh_req

=============================================================================*/
/*!
    @brief
    Function to handle QMI req to get sleep notification threshold for a tech

    @return
    qmi_csi_cb_error
*/
/*===========================================================================*/
qmi_csi_cb_error coex_handle_get_sleep_notification_thresh_req (
  void          *connection_handle,
  qmi_req_handle req_handle,
  unsigned int   msg_id,
  void          *req_c_struct,
  unsigned int   req_c_struct_len,
  void          *service_cookie
)
{
  qmi_csi_cb_error                           req_cb_retval = QMI_CSI_CB_NO_ERR;
  coex_get_sleep_notification_resp_msg_v01   response;
  coex_get_sleep_notification_req_msg_v01   *request;
  /*-----------------------------------------------------------------------*/
  CXM_ASSERT( req_c_struct != NULL );

  /* De-mystify the received message pointer to the appropriate type */
  request = (coex_get_sleep_notification_req_msg_v01 *) req_c_struct;  

  /* Init the memory available for the resp message */
  memset( &response, 0, sizeof( coex_get_sleep_notification_resp_msg_v01 ) );

  /*explicitly set optional fields to FALSE*/
  response.off_period_threshold_valid = FALSE;
  response.tech_valid = FALSE;
  
  if( request->tech > (coex_wwan_tech_v01)CXM_TECH_DFLT_INVLD && 
              request->tech < (coex_wwan_tech_v01)CXM_TECH_MAX )
  {   
    /*check the technolgy entry and store in the appropriate field*/
    response.off_period_threshold = coex_wwan_state_info[request->tech].sleep_thresh;
    response.off_period_threshold_valid = TRUE;
    response.tech =  request->tech;
    response.tech_valid = TRUE;

    response.resp.result = QMI_RESULT_SUCCESS_V01;
    response.resp.error  = QMI_ERR_NONE_V01;
  }
  else
  {
    response.resp.result = QMI_RESULT_FAILURE_V01;
    response.resp.error  = QMI_ERR_INVALID_ARG_V01;
  }

  /* send QMI response */
  req_cb_retval = mqcsi_conn_mgr_send_resp_from_cb (
                    connection_handle,
                    req_handle,
                    msg_id,
                    &response,
                    sizeof( coex_get_sleep_notification_resp_msg_v01 )
                  );
  CXM_ASSERT( QMI_CSI_CB_NO_ERR == req_cb_retval );

  return req_cb_retval;
}

/*=============================================================================

  FUNCTION:  coex_handle_wcn_wake_sync_req

=============================================================================*/
/*!
    @brief
    Function to handle QMI req to sync BT and LTE page scans

    @return
    qmi_csi_cb_error
*/
/*===========================================================================*/
qmi_csi_cb_error coex_handle_wcn_wake_sync_req (
  void          *connection_handle,
  qmi_req_handle req_handle,
  unsigned int   msg_id,
  void          *req_c_struct,
  unsigned int   req_c_struct_len,
  void          *service_cookie
)
{
  qmi_csi_cb_error                 req_cb_retval = QMI_CSI_CB_NO_ERR;
#ifdef T_WINNT
  #error code not present
#else
  coex_wcn_wake_sync_resp_msg_v01  response = {
    .resp.result = QMI_RESULT_SUCCESS_V01,
    .resp.error  = QMI_ERR_NONE_V01
  };
#endif
  coex_wcn_wake_sync_req_msg_v01 * request;
  /*-----------------------------------------------------------------------*/
  CXM_ASSERT( req_c_struct != NULL );

  /* de-mystify pointer */
  request = (coex_wcn_wake_sync_req_msg_v01 *) req_c_struct;

#ifdef T_WINNT
  #error code not present
#endif

  /* save the WCN info from the request */
  coex_wcn_wake_sync_info.scan_enabled = request->scan_enabled;

  /* only save the page scan interval if scan_enabled == TRUE */
  if (request->scan_interval_valid && request->scan_enabled)
  {
    coex_wcn_wake_sync_info.scan_interval = request->scan_interval;
    /* reset last timestamp, so first IND will be sent at next
     * WWAN wakeup */
    coex_wcn_wake_sync_info.last_msg_ts = 0;
  }

  CXM_MSG_2( HIGH, "Wake Sync REQ, enable=%d interval=%d", 
             request->scan_enabled, request->scan_interval );

  /* send QMI response */
  req_cb_retval = mqcsi_conn_mgr_send_resp_from_cb (
                    connection_handle,
                    req_handle,
                    msg_id,
                    &response,
                    sizeof( coex_wcn_wake_sync_resp_msg_v01 )
                  );
  CXM_ASSERT( QMI_CSI_CB_NO_ERR == req_cb_retval );

  return req_cb_retval;
}

/*=============================================================================

  FUNCTION:  coex_handle_get_conflict_params_req

=============================================================================*/
/*!
    @brief
    Function to handle QMI req to get the conflict table

    @return
    qmi_csi_cb_error
*/
/*===========================================================================*/
qmi_csi_cb_error coex_handle_get_conflict_params_req (
  void          *connection_handle,
  qmi_req_handle req_handle,
  unsigned int   msg_id,
  void          *req_c_struct,
  unsigned int   req_c_struct_len,
  void          *service_cookie
)
{
  qmi_csi_cb_error                        req_cb_retval = QMI_CSI_CB_NO_ERR;
  coex_get_conflict_params_resp_msg_v01   response;
  uint8 i, j;
  coex_get_conflict_params_req_msg_v01    *request;
  uint32 num_remaining_entries;
  uint32 ending_index = 0;
  /*-----------------------------------------------------------------------*/
  CXM_ASSERT( req_c_struct != NULL );

  /* De-mystify the received message pointer to the appropriate type */
  request = (coex_get_conflict_params_req_msg_v01 *) req_c_struct;

  /* Init the memory available for the resp message */
  memset( &response, 0, sizeof( coex_get_conflict_params_resp_msg_v01 ) );

#ifdef FEATURE_COEX_USE_NV
  response.wcn_behavior_valid = TRUE;
  response.resp.result = QMI_RESULT_SUCCESS_V01;
  response.resp.error  = QMI_ERR_NONE_V01;

  if ( COEX_SYS_ENABLED(CXM_SYS_BHVR_SMEM_DATA) )
  {
    response.wcn_behavior |= COEX_WCN_BHVR_SMEM_DATA_V01;
    if ( request->victim_tbl_offset >= coex_params.num_conflicts )
    {
      response.resp.result = QMI_RESULT_FAILURE_V01;
      response.resp.error  = QMI_ERR_INVALID_ARG_V01;
    }
    else
    {
      num_remaining_entries = coex_params.num_conflicts - request->victim_tbl_offset;
      response.victim_tbl_valid = TRUE;
      response.victim_tbl_len = COEX_MAX_VICTIM_TBL_ENTRIES_V01 > num_remaining_entries?
                                num_remaining_entries : COEX_MAX_VICTIM_TBL_ENTRIES_V01;
      response.victim_tbl_complete_size_valid = TRUE;
      response.victim_tbl_complete_size = coex_params.num_conflicts;
      response.victim_tbl_offset_valid = TRUE;
      response.victim_tbl_offset = request->victim_tbl_offset;

      /* also include group numbers corresponding to each entry */
      response.victim_tbl_groups_valid = TRUE;
      response.victim_tbl_groups_len = response.victim_tbl_len;

      ending_index = request->victim_tbl_offset + response.victim_tbl_len - 1;
      for ( i = request->victim_tbl_offset, j = 0; i <= ending_index; i++, j++ )
      {
        response.victim_tbl[j].wcn = coex_victim_tbl[i].wcn;
        response.victim_tbl[j].wwan = coex_victim_tbl[i].wwan;
        response.victim_tbl[j].wcn_params = coex_victim_tbl[i].wcn_params;
        response.victim_tbl[j].mdm_params = coex_victim_tbl[i].mdm_params;
        response.victim_tbl[j].mdm_policy = coex_victim_tbl[i].mdm_policy;
        response.victim_tbl[j].wcn_policy = coex_victim_tbl[i].wcn_policy;
        response.victim_tbl_groups[j] = coex_victim_tbl[i].group;
      }
    }
  }
  else
  {
    response.wcn_behavior |= COEX_WCN_BHVR_NONE_V01;
  }

  /* send QMI response */
  req_cb_retval = mqcsi_conn_mgr_send_resp_from_cb (
                    connection_handle,
                    req_handle,
                    msg_id,
                    &response,
                    sizeof( coex_get_conflict_params_resp_msg_v01 )
                  );
  CXM_MSG_2( HIGH, "Sent config to WCN, behavior=%d return_val=%d", 
             response.wcn_behavior, req_cb_retval );
  CXM_ASSERT( QMI_CSI_CB_NO_ERR == req_cb_retval );
  /*if we are in SMEM system behavior and all conflict data has been transferred, init SMEM*/
  if ( COEX_SYS_ENABLED(CXM_SYS_BHVR_SMEM_DATA) && ( ( ending_index + 1 ) == coex_params.num_conflicts ) )
  {
    cxm_smem_init();
  }
#else
  /* no victim table supported */
  response.resp.result = QMI_RESULT_FAILURE_V01;
  response.resp.error  = QMI_ERR_REQUESTED_NUM_UNSUPPORTED_V01;
#endif /* FEATURE_COEX_USE_NV */

  return req_cb_retval;
}

/*=============================================================================

  FUNCTION:  coex_handle_sleep_wakeup_ind

=============================================================================*/
/*!
    @brief
    Function to send out the sleep or wakeup indication message and
    send through QMI
 
    @return
    qmi_csi_cb_error
*/
/*===========================================================================*/
errno_enum_type coex_handle_sleep_wakeup_ind (
  void *rcv_msgr_msg_ptr
)
{
  cxm_coex_tech_sleep_wakeup_duration_ind_s *msg_ptr;
  errno_enum_type         retval = E_SUCCESS;
  qmi_csi_error           qmi_retval = QMI_CSI_NO_ERR;
  coex_sleep_ind_msg_v01  sleep_msg;
  coex_wakeup_ind_msg_v01 wakeup_msg;
  wci2_msg_type_s         wci2_msg;
  /*-----------------------------------------------------------------------*/
  /* De-mystify the received message pointer to the appropriate type */
  msg_ptr = (cxm_coex_tech_sleep_wakeup_duration_ind_s *) rcv_msgr_msg_ptr;

  CXM_MSG_3( HIGH, "Sleep duration rcvd, tech: %d is_going_to_sleep: %d, duration: %d", 
             msg_ptr->tech_id, msg_ptr->is_going_to_sleep, msg_ptr->duration );

  cxm_counter_event( CXM_CNT_LTE_ML1_SLEEP_DURN_RECVD, msg_ptr->duration );

  /* update sleep/wake state for this tech and handle UART power on/off */
  if( msg_ptr->is_going_to_sleep )
  {
    coex_state.active_tech_mask &= ~(1<<msg_ptr->tech_id);
  }
  else
  {
    coex_state.active_tech_mask |= 1<<msg_ptr->tech_id;
  }

  if ( ( msg_ptr->is_going_to_sleep && (msg_ptr->duration >= coex_wwan_state_info[msg_ptr->tech_id].sleep_thresh) ) 
         || !(msg_ptr->is_going_to_sleep) )
  {
    if ( COEX_SYS_ENABLED(CXM_SYS_BHVR_WCI2_DATA) ||
       COEX_SYS_ENABLED(CXM_SYS_BHVR_WCI2_CONTROL) )
    {
      /* if possibly waking from sleep, make sure to power up before sending */
      if ( !msg_ptr->is_going_to_sleep )
      {
        coex_manage_uart_npa_vote();
      }

      if ( (coex_wwan_state_info[msg_ptr->tech_id].plcy_parms.tech_policy & 
            COEX_PCM_SEND_WCI2_TYPE3_INACT_DURN_V01) &&
           CXM_TECH_LTE == msg_ptr->tech_id )
      {
        /* if type3 policy set, then send the data out uart (LTE only) */
        wci2_msg.type = WCI2_TYPE3;
        wci2_msg.data.type3_inact_durn = (uint16) msg_ptr->duration;
        wci2_send_msg( &wci2_msg );
      }

      if( COEX_SYS_ENABLED(CXM_SYS_BHVR_WCI2_CONTROL) )
      {
        /* send sync msg to WLAN on both sleep and wake */
        coex_process_wwan_sleep_wake_v2( msg_ptr );
      }

      /* if possibly entering sleep, make sure to power down after sending */
      if ( msg_ptr->is_going_to_sleep )
      {
        coex_manage_uart_npa_vote();
      }
    }
    else if ( COEX_SYS_ENABLED(CXM_SYS_BHVR_SMEM_DATA) && 
              coex_wwan_state_info[msg_ptr->tech_id].carrier[CXM_CARRIER_PCC].policy & COEX_PCM_SEND_WCI2_TYPE3_INACT_DURN_V01)
    {
      cxm_smem_handle_sleep_wakeup( msg_ptr );
    }
  }

  /*if it is a sleep duration, check the time against the threshold val*/
  if (msg_ptr->is_going_to_sleep) 
  {
    if (msg_ptr->duration >= coex_wwan_state_info[msg_ptr->tech_id].sleep_thresh ) 
    {
      /* send the sleep message to QMI */
      CXM_MSG_1( LOW, "sending QMI_COEX_SLEEP_IND_V01, duration: %d",
                 msg_ptr->duration );

      /* Reset the memory available for the IND message */
      memset( &sleep_msg, 0, sizeof( coex_sleep_ind_msg_v01 ) );

      /* populate the sleep msg */
      sleep_msg.off_period = msg_ptr->duration;
      sleep_msg.tech = (coex_wwan_tech_v01) msg_ptr->tech_id;

      /* send the QMI message */
      qmi_retval = mqcsi_conn_mgr_send_ind (
                  MQCSI_COEX_SERVICE_ID,
                  QMI_COEX_SLEEP_IND_V01,
                  &sleep_msg,
                  sizeof ( coex_sleep_ind_msg_v01 )
              );
      CXM_ASSERT( QMI_CSI_NO_ERR == qmi_retval );
    }
  }

  /*else if it is a wakeup msg*/
  else if (!msg_ptr->is_going_to_sleep)
  {
    /* If an LTE wakeup, want to sync WCN and WWAN wakeups to save power.
     * We only do this if both LTE and BT are doing paging (LTE in idle DRX)
     * (FR 2288) */
    if ( CXM_TECH_LTE == msg_ptr->tech_id && msg_ptr->params.lte.idle_flag &&
         coex_wcn_wake_sync_info.scan_enabled )
    {
      qmi_retval = coex_prep_and_send_wcn_wake_sync_ind( msg_ptr->params.lte.page_cycle );
      CXM_ASSERT( QMI_CSI_NO_ERR == qmi_retval );
    }

     /*send the wakeup message to QMI*/
     CXM_MSG_1( LOW , "sending QMI_COEX_WAKEUP_IND_V01, duration: %d",
             msg_ptr->duration );

     /* Reset the memory available for the IND message */
     memset( &wakeup_msg, 0, sizeof( coex_wakeup_ind_msg_v01 ) );

     /*populate the wakeup msg*/
     wakeup_msg.time_to_wakeup = msg_ptr->duration;
     wakeup_msg.tech = (coex_wwan_tech_v01) msg_ptr->tech_id;

     /* send the QMI message */
     qmi_retval = mqcsi_conn_mgr_send_ind (
               MQCSI_COEX_SERVICE_ID,
               QMI_COEX_WAKEUP_IND_V01,
               &wakeup_msg,
               sizeof ( coex_wakeup_ind_msg_v01 )
             );
     CXM_ASSERT( QMI_CSI_NO_ERR == qmi_retval );
  }

  CXM_TRACE( CXM_TRC_MSGR_WWAN_TECH_SLEEP_WAKEUP, msg_ptr->tech_id, 
             msg_ptr->is_going_to_sleep, msg_ptr->duration,
             CXM_TRC_AND_PKT_EN, CXM_LOG_STATE );

  return retval;
}

/*=============================================================================

  FUNCTION:  coex_update_coex_wwan_state_info_ind

=============================================================================*/
/*!
    @brief
    Function to update the WWAN state info & if needed send out the 
    COEX_WWAN_STATE info IND message
 
    @return
    qmi_csi_cb_error
*/
/*===========================================================================*/
errno_enum_type coex_update_wwan_state_info_ind (
  void *rcv_msgr_msg_ptr
)
{
  errno_enum_type            retval = E_SUCCESS;
  qmi_csi_error              qmi_retval = QMI_CSI_NO_ERR;
  boolean                    state_changed = FALSE;
  cxm_tech_data_s            filtered_tech_data;
  cxm_tech_data_s           *state_data;
  int32                      lte_frame_offset_delta = 0;
  unsigned int               i, j, cc;
  cxm_wwan_tech_state_ind_s *msg_ptr;
  cxm_coex_tech_sleep_wakeup_duration_ind_s sleep_wake_ind;
  cxm_tech_conn_state_e      new_conn_state[CXM_CARRIER_MAX];
  uint32                     trc_code = 0;
  /*-----------------------------------------------------------------------*/
  /* De-mystify the received message pointer to the appropriate type */
  msg_ptr = (cxm_wwan_tech_state_ind_s *) rcv_msgr_msg_ptr;
  CXM_ASSERT( msg_ptr->tech_id > CXM_TECH_DFLT_INVLD && 
              msg_ptr->tech_id < CXM_TECH_MAX &&
              msg_ptr->tech_data.num_link_info_sets <= CXM_MAX_SUPPORTED_LINK_SETS );
  state_data = &coex_wwan_state_info[msg_ptr->tech_id].state_data;

  /* copy relevant tech data into temporary local data structure, filtering
   * out unused info. Use this filtered info to determine if anything we care
   * about has changed */
  memset( &filtered_tech_data, 0, sizeof(cxm_tech_data_s) );
  for( i = 0, j = 0; i < msg_ptr->tech_data.num_link_info_sets; i++ )
  {
    /* some link types are for WWAN coex only; drop those */
    if( (CXM_WLAN_IGNORED_LINK_TYPES & msg_ptr->tech_data.link_list[i].type) == 0)
    {
      filtered_tech_data.link_list[j++] = msg_ptr->tech_data.link_list[i];
    }
  }
  filtered_tech_data.num_link_info_sets = j;
  if( filtered_tech_data.num_link_info_sets > 0 )
  {
    /* do not copy params if we don't have any link info, to avoid 
     * triggering unneccessary wwan state indications while not attached */
    for( cc = 0; cc < CXM_MAX_CA_SUPPORTED; cc++ )
    {
      filtered_tech_data.cc_params[cc] = msg_ptr->tech_data.cc_params[cc];
    }
  }

  CXM_MSG_9( HIGH, "Received WWAN state update, tech: %d, len: %d, filter_len: %d, "
                   "Entry 1: freq: %d, bandwidth: %d, dir: %d "
                   "Entry 2: freq: %d, bandwidth: %d, dir: %d",
                   msg_ptr->tech_id,
                   msg_ptr->tech_data.num_link_info_sets,
                   filtered_tech_data.num_link_info_sets,
                   filtered_tech_data.link_list[0].frequency,
                   filtered_tech_data.link_list[0].bandwidth,
                   filtered_tech_data.link_list[0].direction,
                   filtered_tech_data.link_list[1].frequency,
                   filtered_tech_data.link_list[1].bandwidth,
                   filtered_tech_data.link_list[1].direction);

  /* For WWAN tech LTE check if change in frame_offset is greater than 2 units 
     This helps filtering minor updates or rounding off updates to other side */
  if( msg_ptr->tech_id == CXM_TECH_LTE )
  {
    for( cc = CXM_CARRIER_PCC; cc < CXM_CARRIER_MAX; cc++ )
    {
      lte_frame_offset_delta = 
          filtered_tech_data.cc_params[cc].lte_params.frame_offset -
          state_data->cc_params[cc].lte_params.frame_offset;
      lte_frame_offset_delta = ( (lte_frame_offset_delta >= 0) ? 
        lte_frame_offset_delta : (-(lte_frame_offset_delta)) );
      if( lte_frame_offset_delta <= COEX_TECH_LTE_FRAME_OFFSET_DIFF_THRESH ) 
      {
        CXM_MSG_4( HIGH, "Received LTE frame offset %d, previously sent %d & "
                         "delta %d within threshold - no change reported (cc %d)",
                   filtered_tech_data.cc_params[cc].lte_params.frame_offset,
                   state_data->cc_params[cc].lte_params.frame_offset,
                   lte_frame_offset_delta, cc );
        /* Update incoming "frame offset" with previously sent value */
        filtered_tech_data.cc_params[cc].lte_params.frame_offset = 
          state_data->cc_params[cc].lte_params.frame_offset;
      }
    }
  }

  /* -Determine the tech and copy in current state info as it is in the correct 
      local state info object if it has changed.
     -Note that we need to know if state has changed to determine whether to run 
      the process_conflicts algorithm */
  if( memcmp( &filtered_tech_data, state_data, sizeof(msg_ptr->tech_data) ) )
  {
    *state_data = filtered_tech_data;

    /* parse link list for UL/DL to determine tech state (conn/idle/inactive).
     * If doing CA, check this for each CC. */
    for( cc = CXM_CARRIER_PCC; cc < CXM_CARRIER_MAX; cc++ )
    {
      new_conn_state[cc] = CXM_TECH_STATE_INACTIVE;
    }
    for( i = 0; i < filtered_tech_data.num_link_info_sets; i++ )
    {
      cc = (msg_ptr->tech_id == CXM_TECH_LTE) ? 
        COEX_GET_LINK_CC(filtered_tech_data.link_list[i].type) : CXM_CARRIER_PCC;
      CXM_ASSERT( cc < CXM_CARRIER_MAX );
      if( filtered_tech_data.link_list[i].direction == CXM_LNK_DRCTN_UL || 
          filtered_tech_data.link_list[i].direction == CXM_LNK_DRCTN_UL_AND_DL )
      {
        new_conn_state[cc] = CXM_TECH_STATE_CONN;
      }
      else if( filtered_tech_data.link_list[i].direction == CXM_LNK_DRCTN_DL &&
          new_conn_state[cc] == CXM_TECH_STATE_INACTIVE )
      {
        new_conn_state[cc] = CXM_TECH_STATE_IDLE;
      }
    }

    /* we don't get slpc sleep notifications when tech leaves the system or wake 
     * notifications when tech enters the system for the first time, so trigger ourselves */
    /* LTE sends us their own SLEEP/WAKE notifications, so this doesn't apply for LTE */
    if( msg_ptr->tech_id != CXM_TECH_LTE )
    {
      cc = CXM_CARRIER_PCC;
      if( coex_wwan_state_info[msg_ptr->tech_id].conn_state == CXM_TECH_STATE_INACTIVE && 
          new_conn_state[cc] != CXM_TECH_STATE_INACTIVE )
      {
        /* when entering the system, need to process wake BEFORE we finish processing this
         * state update, so call the wake handler directly */
        sleep_wake_ind.tech_id = msg_ptr->tech_id;
        sleep_wake_ind.is_going_to_sleep = FALSE;
        sleep_wake_ind.duration = 0;
        retval = coex_handle_sleep_wakeup_ind( &sleep_wake_ind );
      }
      else if( coex_wwan_state_info[msg_ptr->tech_id].conn_state != CXM_TECH_STATE_INACTIVE &&
               new_conn_state[cc] == CXM_TECH_STATE_INACTIVE )
      {
        /* process sleep after we finish processing the current state */
        sleep_wake_ind.tech_id = msg_ptr->tech_id;
        sleep_wake_ind.is_going_to_sleep = TRUE;
        sleep_wake_ind.duration = 0xffffffff; /* infinite duraton */
        retval = cxm_msgr_send_msg( &sleep_wake_ind.msg_hdr, 
                   MCS_CXM_COEX_TECH_SLEEP_WAKEUP_IND, 
                   sizeof(cxm_coex_tech_sleep_wakeup_duration_ind_s) );
      }
    }
    for( cc = CXM_CARRIER_PCC; cc < CXM_CARRIER_MAX; cc++ )
    {
      coex_wwan_state_info[msg_ptr->tech_id].carrier[cc].conn_state =
        new_conn_state[cc];
    }
    coex_wwan_state_info[msg_ptr->tech_id].conn_state =
      new_conn_state[CXM_CARRIER_PCC];

    /* if we're running control plane over WCI-2, process this state change
     * for that interface as well */
    if( COEX_SYS_ENABLED(CXM_SYS_BHVR_WCI2_CONTROL) )
    {
      coex_process_wwan_state_info_msg_v2( msg_ptr->tech_id, &filtered_tech_data );
    }

#ifdef FEATURE_COEX_USE_NV
    /* check for conflicts and mitigate if necessary. Do this for 3-wire mode only */
    if( COEX_SYS_ENABLED(CXM_SYS_BHVR_VICTIM_TABLE) )
    {
      coex_confl_update_wwan_tech(
        msg_ptr->tech_id, 
        &coex_wwan_state_info[msg_ptr->tech_id].state_data );
    }
#endif

    /* apply band filter and send QMI indication based on new tech state */
    coex_apply_tech_band_filter(msg_ptr->tech_id);
    qmi_retval = coex_prep_and_send_wwan_state_ind( FALSE );
    retval = ( QMI_CSI_NO_ERR != qmi_retval ? E_FAILURE : retval );

    state_changed = TRUE;

    /* log the new WWAN state info*/
    switch( msg_ptr->tech_id )
    {
      case CXM_TECH_LTE:
        trc_code = CXM_LOG_LTE_WWAN_STATE_INFO;
        break;
      case CXM_TECH_TDSCDMA:
        trc_code = CXM_LOG_TDS_WWAN_STATE_INFO;
        break;
      case CXM_TECH_GSM1:
        trc_code = CXM_LOG_GSM1_WWAN_STATE_INFO;
        break;
      case CXM_TECH_GSM2:
        trc_code = CXM_LOG_GSM2_WWAN_STATE_INFO;
        break;
      case CXM_TECH_GSM3:
        trc_code = CXM_LOG_GSM3_WWAN_STATE_INFO;
        break;
      default:
        trc_code = CXM_LOG_LTE_WWAN_STATE_INFO;
        break;
    }
    CXM_TRACE(0, 0, 0, 0, CXM_LOG_PKT_EN,
             CXM_LOG_STATE, trc_code );
  }

  CXM_TRACE(CXM_TRC_MSGR_WWAN_TECH_STATE_INFO, msg_ptr->tech_id, state_changed,
             filtered_tech_data.num_link_info_sets, CXM_TRC_EN);

  return retval;
}

/*=============================================================================

  FUNCTION:  coex_handle_sfn_ind

=============================================================================*/
/*!
    @brief
    Function to send out wci2 type 6 msg when SFN IND received

    @return qmi_csi_cb_error
*/
/*===========================================================================*/
errno_enum_type coex_handle_tx_adv_ntc_ind (
  void *rcv_msgr_msg_ptr
)
{
  uint8                           i, j;
  boolean                         cc_thresh_check_passed;
  boolean                         total_thresh_check_passed;
  cxm_carrier_e                   cc;
  errno_enum_type                 retval = E_SUCCESS;
  wci2_msg_type_s                 wci2_msg;
  coex_conflict_type             *confl_ptr;
  cxm_lte_tx_adv_ntc_info_s      *tx_info = NULL, *pcc_tx_info = NULL;
  cxm_coex_tech_tx_adv_ntc_ind_s *msg_ptr;
  /*-----------------------------------------------------------------------*/
  /* De-mystify the received message pointer to the appropriate type */
  msg_ptr = (cxm_coex_tech_tx_adv_ntc_ind_s *) rcv_msgr_msg_ptr;
  CXM_ASSERT( msg_ptr->num_cc_params <= CXM_CARRIER_MAX );
  cxm_counter_event( CXM_CNT_LTE_ML1_ADVANCE_NOTICE_RECVD, msg_ptr->cc_params[0].lte.sfn );

  /* locate the Primary CC: some TX Adv algorithms don't support UL CA yet */
  for( i = 0; i < msg_ptr->num_cc_params; i++ )
  {
    cc = msg_ptr->cc_params[i].lte.cc_id;

    CXM_MSG_4( HIGH, "Advance Tx notification for %d sfn, %d rb & %d tx_power [cc=%d]",
               msg_ptr->cc_params[i].lte.sfn, msg_ptr->cc_params[i].lte.rbs, 
               msg_ptr->cc_params[i].lte.tx_power, cc );

    if( cc == CXM_CARRIER_PCC )
    {
      pcc_tx_info = &msg_ptr->cc_params[i].lte;
      /* log tx adv */
      cxm_tx_adv_log.sfn = msg_ptr->cc_params[i].lte.sfn;
      cxm_tx_adv_log.transmit = msg_ptr->cc_params[i].lte.transmit;
      cxm_tx_adv_log.ustmr_time = msg_ptr->cc_params[i].lte.ustmr_time;
      cxm_tx_adv_log.rbs = msg_ptr->cc_params[i].lte.rbs;
      cxm_tx_adv_log.tx_power = msg_ptr->cc_params[i].lte.tx_power;
      CXM_TRACE( CXM_TRC_MSGR_LTE_SFN, msg_ptr->cc_params[i].lte.sfn, 0, 0,
                 CXM_TRC_AND_PKT_EN, CXM_LOG_LTE_TX_ADV );
    }
  }

  if( COEX_SYS_ENABLED(CXM_SYS_BHVR_CXM_ASSERTS_TX_ADV) && pcc_tx_info != NULL )
  {
    /* TODO: support for multiple carriers when MCS asserts TX Type0?
             for now only primary since logic is complex and don't know if
             required */
    coex_assert_lte_tx_adv( pcc_tx_info );
  }
  else if( COEX_SYS_ENABLED(CXM_SYS_BHVR_TX_ADV_ALL_SUBFRAMES) )
  {
    total_thresh_check_passed = FALSE;
    for( j = 0; j < msg_ptr->num_cc_params; j++ )
    {
      cc = msg_ptr->cc_params[j].lte.cc_id;
      tx_info = &msg_ptr->cc_params[j].lte;
      /* reset the thresh check from the previous iteration */
      cc_thresh_check_passed = FALSE;
      /* send message for both TX and non-TX subframes in this mode. Use 5th
       * bit to specify which is the case */

      /* Only mark as a TX if all of the following are true:
       *  - Is a TX subframe
       *  - #RBs within range in victim table
       *  - LTE TX power >= threshold in NV */
      confl_ptr = coex_wwan_state_info[CXM_TECH_LTE].carrier[cc].active_confl;
      if( confl_ptr != NULL && 
          (coex_wwan_state_info[CXM_TECH_LTE].carrier[cc].policy & 
           CXM_POLICY_WCI2_OUTGNG_TYPE6_TX_ADV_NOTICE) != 0 )
      {
        if( tx_info->transmit && (tx_info->tx_power >= 
            confl_ptr->mdm_params.tx_adv.tx_power_thresh) )
        {
          for( i = 0; i < confl_ptr->mdm_params.tx_adv.rb_thresh_len; i++ )
          {
            if ( tx_info->rbs >= confl_ptr->mdm_params.tx_adv.rb_thresh[i].start && 
                 tx_info->rbs <= confl_ptr->mdm_params.tx_adv.rb_thresh[i].end )
            {
              /* all checks passed! */
              total_thresh_check_passed = TRUE;
              cc_thresh_check_passed = TRUE;
              break;
            } /* rb check */
          } /* rb loop */
        } /* transmit + power limit check */

        /* send the info for this cc to SMEM */
        if( COEX_SYS_ENABLED(CXM_SYS_BHVR_SMEM_DATA) )
        {
          cxm_smem_handle_lte_tx_adv( tx_info, cc_thresh_check_passed );
        }
        CXM_MSG_7( MED, "Type 6 TX adv[cc=%d]: sfn=%d, tx=%d, tx_pwr=%d, "
                        "tx_pwr_thresh=%d, rbs=%d, t6_tx=%d",
                   cc, tx_info->sfn, tx_info->transmit, tx_info->tx_power, 
                   confl_ptr->mdm_params.tx_adv.tx_power_thresh,
                   tx_info->rbs, cc_thresh_check_passed );
      }
    }

    if( COEX_SYS_ENABLED(CXM_SYS_BHVR_CXM_SENDS_TYPE6) && tx_info != NULL )
    {
      /* send message for both TX and non-TX subframes in this mode. Use 5th
       * bit to specify which is the case */
      wci2_msg.type = WCI2_TYPE6;
      wci2_msg.data.type6_adv_tx_sfn.sfn = tx_info->sfn;
      wci2_msg.data.type6_adv_tx_sfn.tx = total_thresh_check_passed;

      /* send the completed type 6 message */
      wci2_send_msg( &wci2_msg );
    }
    
  }
  else if( COEX_SYS_ENABLED(CXM_SYS_BHVR_CXM_SENDS_TYPE6) && pcc_tx_info != NULL )
  {
    if ( pcc_tx_info->rbs >= 
         coex_wwan_state_info[CXM_TECH_LTE].plcy_parms.params.lte.rb_threshold_for_adv_tx_notice &&
         pcc_tx_info->tx_power >= 
         coex_wwan_state_info[CXM_TECH_LTE].plcy_parms.params.lte.tx_power_threshold_for_adv_tx_notice )
    {
      /* normal ML1 protocol mode - only send TX subframes. TX is 
       * implied, so do not fill in extra TX boolean */
      wci2_msg.type = WCI2_TYPE6;
      wci2_msg.data.type6_adv_tx_sfn.sfn = pcc_tx_info->sfn;
      wci2_msg.data.type6_adv_tx_sfn.tx = FALSE;
      wci2_send_msg( &wci2_msg );

      CXM_MSG_1( HIGH, "Advance Tx notification sent out for %d LTE sfn", 
                 pcc_tx_info->sfn );
    }
    else
    {
      CXM_MSG_1( HIGH, "Advance Tx notification filtered out for %d LTE sfn", 
                 pcc_tx_info->sfn );
    }
  }

  return retval;
}

/*=============================================================================

  FUNCTION:  coex_handle_activity_timeline_ind

=============================================================================*/
/*!
    @brief
    Handle activity timeline message 

    @return
    errno_enum_type
*/
/*===========================================================================*/
errno_enum_type coex_handle_activity_timeline_ind (
  void *rcv_msgr_msg_ptr
)
{
  errno_enum_type     retval = E_SUCCESS;
  cxm_timing_info_s   *msg_ptr;
  /*-----------------------------------------------------------------------*/
  /* De-mystify the received message pointer to the appropriate type */
  msg_ptr = (cxm_timing_info_s *) rcv_msgr_msg_ptr;

  /* make sure policy is enabled because activity timeline message can come in when 
     policy is not enabled */
  if ( COEX_SYS_ENABLED(CXM_SYS_BHVR_SMEM_DATA) 
       && (coex_wwan_state_info[msg_ptr->tech_id].carrier[CXM_CARRIER_PCC].policy & 
           CXM_POLICY_WCI2_OUTGNG_TYPE6_TX_ADV_NOTICE) )
  {
    cxm_smem_handle_activity_timeline( msg_ptr );
  }

  return retval;
}

/*=============================================================================

  FUNCTION:  coex_handle_bler_ind

=============================================================================*/
/*!
    @brief
    Forward BLER indication from LTE to AP over QMI as part of metrics

    @return qmi_csi_cb_error
*/
/*===========================================================================*/
errno_enum_type coex_handle_bler_ind (
  void *rcv_msgr_msg_ptr
)
{
  errno_enum_type                         retval = E_SUCCESS;
  cxm_coex_metrics_lte_bler_ind_s         *msg_ptr;
  qmi_csi_error                           qmi_retval = QMI_CSI_NO_ERR;
  coex_metrics_lte_bler_ind_msg_v01 indication;
  /*-----------------------------------------------------------------------*/
  /* De-mystify the received message pointer to the appropriate type */
  msg_ptr = (cxm_coex_metrics_lte_bler_ind_s *) rcv_msgr_msg_ptr;

  CXM_TRACE( CXM_TRC_MSGR_LTE_BLER, 0, msg_ptr->tb_err_count, msg_ptr->tb_count,
             CXM_TRC_EN );

  /* Reset the memory available for the IND message */
  memset( &indication, 0, sizeof( coex_metrics_lte_bler_ind_msg_v01 ) );

  indication.tb_cnt_valid         = TRUE;
  indication.tb_cnt               = msg_ptr->tb_count;
  indication.errored_tb_cnt_valid = TRUE;
  indication.errored_tb_cnt       = msg_ptr->tb_err_count;

  if (msg_ptr->tb_count_bt)
  {
     indication.tb_cnt_bt_only_valid = TRUE;
     indication.tb_cnt_bt_only       = msg_ptr->tb_count_bt;
     indication.errored_tb_cnt_bt_only_valid = TRUE;
     indication.errored_tb_cnt_bt_only = msg_ptr->tb_err_count_bt;
  }
  if (msg_ptr->tb_count_wifi)
  {
     indication.tb_cnt_wifi_only_valid = TRUE;
     indication.tb_cnt_wifi_only       = msg_ptr->tb_count_wifi;
     indication.errored_tb_cnt_wifi_only_valid = TRUE;
     indication.errored_tb_cnt_wifi_only = msg_ptr->tb_err_count_wifi;
  }
  if (msg_ptr->tb_count_bt_wifi)
  {
     indication.tb_cnt_bt_wifi_valid = TRUE;
     indication.tb_cnt_bt_wifi      = msg_ptr->tb_count_bt_wifi;
     indication.errored_tb_cnt_bt_wifi_valid = TRUE;
     indication.errored_tb_cnt_bt_wifi = msg_ptr->tb_err_count_bt_wifi;
  }
  if (msg_ptr->tb_count_lte_only)
  {
     indication.tb_cnt_lte_only_valid = TRUE;
     indication.tb_cnt_lte_only      = msg_ptr->tb_count_lte_only;
     indication.errored_tb_cnt_lte_only_valid = TRUE;
     indication.errored_tb_cnt_lte_only = msg_ptr->tb_err_count_lte_only;
  }

  qmi_retval = mqcsi_conn_mgr_send_ind (
                 MQCSI_COEX_SERVICE_ID,
                 QMI_COEX_METRICS_LTE_BLER_IND_V01,
                 &indication,
                 sizeof ( coex_metrics_lte_bler_ind_msg_v01 )
               );
  CXM_ASSERT( QMI_CSI_NO_ERR == qmi_retval );

  if(qmi_retval != QMI_CSI_NO_ERR)
  {
    retval = E_FAILURE;
  }

  CXM_MSG_3( HIGH, "Send Metrics BLER stats; tb_cnt=%d, err_cnt=%d IND return %d", 
             msg_ptr->tb_count, msg_ptr->tb_err_count, qmi_retval );

  /* log BLER */
  cxm_lte_bler_log.tb_count = msg_ptr->tb_count;
  cxm_lte_bler_log.tb_err_count = msg_ptr->tb_err_count;
  cxm_lte_bler_log.tb_count_bt = msg_ptr->tb_count_bt;
  cxm_lte_bler_log.tb_err_count_bt = msg_ptr->tb_err_count_bt;
  cxm_lte_bler_log.tb_count_wifi = msg_ptr->tb_count_wifi;
  cxm_lte_bler_log.tb_err_count_wifi = msg_ptr->tb_err_count_wifi;
  cxm_lte_bler_log.tb_count_bt_wifi = msg_ptr->tb_count_bt_wifi;
  cxm_lte_bler_log.tb_err_count_bt_wifi = msg_ptr->tb_err_count_bt_wifi;
  cxm_lte_bler_log.tb_count_lte_only = msg_ptr->tb_count_lte_only;
  cxm_lte_bler_log.tb_err_count_lte_only = msg_ptr->tb_err_count_lte_only;
  CXM_TRACE( 0, 0, 0, 0, CXM_LOG_PKT_EN, CXM_LOG_LTE_BLER_READ );

  return retval;
}

/*=============================================================================

  FUNCTION:  coex_handle_lte_sinr_read_cnf

=============================================================================*/
/*!
    @brief
    Function to send QMI message with SINR stats from LTE

    @return qmi_csi_cb_error
*/
/*===========================================================================*/
errno_enum_type coex_handle_lte_sinr_read_cnf (
  void *rcv_msgr_msg_ptr
)
{
  errno_enum_type                             retval = E_SUCCESS;
  cxm_coex_metrics_lte_sinr_rsp_s            *msg_ptr;

  qmi_csi_error                               qmi_retval = QMI_CSI_NO_ERR;
  coex_metrics_lte_sinr_read_resp_msg_v01     qmi_response;
  void                                       *connection_handle;
  qmi_req_handle                              req_handle;
  /*-----------------------------------------------------------------------*/
  /* De-mystify the received message pointer to the appropriate type */
  msg_ptr = (cxm_coex_metrics_lte_sinr_rsp_s *) rcv_msgr_msg_ptr;

  if( COEX_SYS_ENABLED(CXM_SYS_BHVR_QMI_METRICS) )
  {
    memset( &qmi_response, 0, sizeof( coex_metrics_lte_sinr_read_resp_msg_v01 ) );

    if(msg_ptr->status == E_SUCCESS)
    {
      /* all okay; send the qmi_response */
      qmi_response.sinr_valid          = TRUE;
      qmi_response.sinr                = msg_ptr->filtered_sinr_db;
      if (msg_ptr->sinr_count_bt)
      {
        qmi_response.sinr_bt_only_valid  = TRUE;
        qmi_response.sinr_bt_only        = msg_ptr->filtered_sinr_bt_db;
      }
      if (msg_ptr->sinr_count_wifi)
      {
        qmi_response.sinr_wifi_only_valid  = TRUE;
        qmi_response.sinr_wifi_only        = msg_ptr->filtered_sinr_wifi_db;
      }
      if (msg_ptr->sinr_count_bt_wifi)
      {
        qmi_response.sinr_bt_and_wifi_valid  = TRUE;
        qmi_response.sinr_bt_and_wifi        = msg_ptr->filtered_sinr_bt_wifi_db;
      }
      if (msg_ptr->sinr_count_lte_only)
      {
        qmi_response.sinr_lte_only_valid  = TRUE;
        qmi_response.sinr_lte_only        = msg_ptr->filtered_sinr_lte_only_db;
      }

      qmi_response.carrier_valid = TRUE;
      qmi_response.carrier = (coex_carrier_v01) msg_ptr->carrier;
      qmi_response.resp.result = QMI_RESULT_SUCCESS_V01;
      qmi_response.resp.error  = QMI_ERR_NONE_V01;
    }
    else if(msg_ptr->status == E_NOT_AVAILABLE)
    {
      /* likely LTE not in system */
      qmi_response.sinr_valid  = TRUE;
      qmi_response.sinr        = COEX_INVALID_SINR_VALUE;
      qmi_response.resp.result = QMI_RESULT_SUCCESS_V01;
      qmi_response.resp.error  = QMI_ERR_NONE_V01;
    }
    else
    {
      qmi_response.sinr_valid  = FALSE;
      qmi_response.resp.result = QMI_RESULT_FAILURE_V01;
      qmi_response.resp.error  = QMI_ERR_NONE_V01;
    }

    /* look up the QMI delayed qmi_response to the read req */
    qmi_retval = mqcsi_conn_mgr_pop_deferred_req(
                    MQCSI_COEX_SERVICE_ID,
                    msg_ptr->msg_id,
                    QMI_COEX_METRICS_LTE_SINR_READ_REQ_V01,
                    &connection_handle,
                    &req_handle);
    CXM_ASSERT( QMI_CSI_NO_ERR == qmi_retval );

    /* send the qmi delayed qmi_response */
    qmi_retval = mqcsi_conn_mgr_send_resp (
                    connection_handle,  
                    req_handle,
                    QMI_COEX_METRICS_LTE_SINR_READ_RESP_V01,
                    &qmi_response,
                    sizeof( coex_metrics_lte_sinr_read_resp_msg_v01 )
                  );
    CXM_ASSERT( QMI_CSI_NO_ERR == qmi_retval );

    if(QMI_CSI_NO_ERR != qmi_retval )
    {
      retval = E_FAILURE;
    }

    CXM_TRACE( CXM_TRC_MSGR_LTE_SINR,
             msg_ptr->status,
             qmi_response.sinr_valid,
             qmi_response.sinr,
             CXM_TRC_EN );
  }
  else
  {
    retval = coex_handle_lte_sinr_read_cnf_v2( rcv_msgr_msg_ptr );
  }

  CXM_MSG_1( HIGH, "Send Metrics SINR read resp, filtered_sinr_db=%d",
             msg_ptr->filtered_sinr_db );

  /* log metrics */
  cxm_tech_metrics_log.status = msg_ptr->status;
  cxm_tech_metrics_log.tech_id = CXM_TECH_LTE;
  cxm_tech_metrics_log.metric_cnt = msg_ptr->sinr_count;
  cxm_tech_metrics_log.metric_cnt_bt = msg_ptr->sinr_count_bt;
  cxm_tech_metrics_log.metric_cnt_wifi = msg_ptr->sinr_count_wifi;
  cxm_tech_metrics_log.metric_cnt_bt_wifi = msg_ptr->sinr_count_bt_wifi;
  cxm_tech_metrics_log.metric_cnt_mdm_only = msg_ptr->sinr_count_lte_only;
  cxm_tech_metrics_log.filt_metric = msg_ptr->filtered_sinr_db;
  cxm_tech_metrics_log.filt_metric_bt = msg_ptr->filtered_sinr_bt_db;
  cxm_tech_metrics_log.filt_metric_wifi = msg_ptr->filtered_sinr_wifi_db;
  cxm_tech_metrics_log.filt_metric_bt_wifi = msg_ptr->filtered_sinr_bt_wifi_db;
  cxm_tech_metrics_log.filt_metric_mdm_only = msg_ptr->filtered_sinr_lte_only_db;
  cxm_tech_metrics_log.cc = msg_ptr->carrier;
  CXM_TRACE( 0, 0, 0, 0, CXM_LOG_PKT_EN, CXM_LOG_TECH_METRICS_READ );

  return retval;
}

/*=============================================================================

  FUNCTION:  coex_handle_metrics_read_rsp

=============================================================================*/
/*!
    @brief
    Function to send QMI message with metrics stats

    @return qmi_csi_cb_error
*/
/*===========================================================================*/
errno_enum_type coex_handle_metrics_read_rsp (
  void *rcv_msgr_msg_ptr
)
{
  errno_enum_type                    retval = E_SUCCESS;
  cxm_coex_metrics_rsp_s             *msg_ptr;

  qmi_csi_error                      qmi_retval = QMI_CSI_NO_ERR;
  coex_metrics_read_resp_msg_v01     qmi_response;
  void                               *connection_handle;
  qmi_req_handle                     req_handle;

  /*-----------------------------------------------------------------------*/

  /* De-mystify the received message pointer to the appropriate type */
  msg_ptr = (cxm_coex_metrics_rsp_s *) rcv_msgr_msg_ptr;

  if( COEX_SYS_ENABLED(CXM_SYS_BHVR_QMI_METRICS) )
  {
    memset( &qmi_response, 0, sizeof( coex_metrics_read_resp_msg_v01 ) );

    if(msg_ptr->status == E_SUCCESS)
    {
      /* all okay; send the qmi_response */
      if ( msg_ptr->tech_id == CXM_TECH_GSM1 || msg_ptr->tech_id == CXM_TECH_GSM2 
           || msg_ptr->tech_id == CXM_TECH_GSM3 )
      {
        qmi_response.sinr_valid          = TRUE;
        qmi_response.sinr                = msg_ptr->filt_metric;
      }
      else if ( msg_ptr->tech_id == CXM_TECH_TDSCDMA )
      {
        qmi_response.nb_noise_valid      = TRUE;
        qmi_response.nb_noise            = msg_ptr->filt_metric;
      }

      qmi_response.tech_valid = TRUE;
      qmi_response.tech = (coex_wwan_tech_v01)msg_ptr->tech_id;

      qmi_response.resp.result = QMI_RESULT_SUCCESS_V01;
      qmi_response.resp.error  = QMI_ERR_NONE_V01;
    }
    else if(msg_ptr->status == E_NOT_AVAILABLE)
    {
      /* likely tech not in system */
      if ( msg_ptr->tech_id == CXM_TECH_GSM1 || msg_ptr->tech_id == CXM_TECH_GSM2 
           || msg_ptr->tech_id == CXM_TECH_GSM3 )
      {
        qmi_response.sinr_valid          = TRUE;
        qmi_response.sinr                = COEX_INVALID_SINR_VALUE;
      }
      else if ( msg_ptr->tech_id == CXM_TECH_TDSCDMA )
      {
        qmi_response.nb_noise_valid      = TRUE;
        qmi_response.nb_noise            = COEX_INVALID_SINR_VALUE;
      }
      qmi_response.resp.result = QMI_RESULT_SUCCESS_V01;
      qmi_response.resp.error  = QMI_ERR_NONE_V01;
    }
    else
    {
      qmi_response.resp.result = QMI_RESULT_FAILURE_V01;
      qmi_response.resp.error  = QMI_ERR_NONE_V01;
    }

    /* look up the QMI delayed qmi_response to the read req */
    qmi_retval = mqcsi_conn_mgr_pop_deferred_req(
                    MQCSI_COEX_SERVICE_ID,
                    msg_ptr->msg_id,
                    QMI_COEX_METRICS_READ_REQ_V01,
                    &connection_handle,
                    &req_handle);
    CXM_ASSERT( QMI_CSI_NO_ERR == qmi_retval );

    /* send the qmi delayed qmi_response */
    qmi_retval = mqcsi_conn_mgr_send_resp (
                    connection_handle,  
                    req_handle,
                    QMI_COEX_METRICS_READ_RESP_V01,
                    &qmi_response,
                    sizeof( coex_metrics_read_resp_msg_v01 )
                  );
    CXM_ASSERT( QMI_CSI_NO_ERR == qmi_retval );

    if(QMI_CSI_NO_ERR != qmi_retval )
    {
      retval = E_FAILURE;
    }

    /* log metrics info */
    cxm_tech_metrics_log.status = msg_ptr->status;
    cxm_tech_metrics_log.tech_id = msg_ptr->tech_id;
    cxm_tech_metrics_log.metric_cnt = msg_ptr->metric_cnt;
    cxm_tech_metrics_log.filt_metric = msg_ptr->filt_metric;
    cxm_tech_metrics_log.cc = CXM_CARRIER_PCC;

    CXM_TRACE( CXM_TRC_MSGR_LTE_SINR,
             msg_ptr->status,
             qmi_response.sinr_valid,
             qmi_response.sinr, CXM_TRC_AND_PKT_EN,
             CXM_LOG_TECH_METRICS_READ );
  }

  CXM_MSG_2( HIGH, "Send Metrics read resp, tech=%d, filtered_metric=%d",
             msg_ptr->tech_id, msg_ptr->filt_metric );

  return retval;

}

/*=============================================================================

  FUNCTION:  coex_handle_lte_tx_power_limit_cnf

=============================================================================*/
/*!
    @brief
    Function to handle the response to the request to LTE ML1 for power
    limiting conditions. Evaluates conditions and starts power limiting if
    appropriate

    @return
    errno_enum_type
*/
/*===========================================================================*/
errno_enum_type coex_handle_lte_tx_power_limit_rsp (
  void *rcv_msgr_msg_ptr
)
{
  uint32                                 i, cc_success_mask = 0;
  cxm_carrier_e                          cc;
  errno_enum_type                        retval = E_SUCCESS;
  cxm_coex_tx_pwr_lmt_lte_cndtns_rsp_s   *msg_ptr;
  coex_pwrlmt_condition_success_mask_v01 succ_ind_mask = 0;
  coex_pwrlmt_condition_fail_mask_v01    controller_tx_pwrlmt_fail_cond = 0;
  coex_pwrlmt_condition_fail_mask_v01    wci2_tx_pwrlmt_fail_cond = 0;
  float                                  path_loss, filtered_rbs;
  /*-----------------------------------------------------------------------*/
  /* De-mystify the received message pointer to the appropriate type */
  msg_ptr = (cxm_coex_tx_pwr_lmt_lte_cndtns_rsp_s *) rcv_msgr_msg_ptr;
  CXM_ASSERT( msg_ptr->num_ccs <= CXM_CARRIER_MAX );

  /* TODO: need to add CC to condition fail/success masks */

  for( i = 0; i < msg_ptr->num_ccs; i++ )
  {
    cc = msg_ptr->cndtns[i].cc_id;
    coex_pwr_lmt_info[cc].check_in_progress = FALSE;
    if( (coex_pwr_lmt_info[cc].controller_state != COEX_PWR_LMT_PENDING) &&
        (coex_pwr_lmt_info[cc].wci2_state       != COEX_PWR_LMT_PENDING) )
    {
      /* if policy has been disabled before power limit enacted do not
         need to do this check */
      return retval;
    }

    /* check conditions, set power limit if appropriate */
    path_loss = (float) msg_ptr->cndtns[i].dl_pathloss;
    retval = cxm_unsigned_Q8_to_float( msg_ptr->cndtns[i].rb_filtered, 
                                       &filtered_rbs);
    CXM_ASSERT( retval == E_SUCCESS );

    if( ( path_loss    < coex_plcy_parms.link_path_loss_threshold ) &&
        ( filtered_rbs > coex_plcy_parms.filtered_rb_threshold )    &&
        ( COEX_SYS_ENABLED(CXM_SYS_BHVR_DISREGARD_RRC_PROC) || 
          msg_ptr->cndtns[i].rrc_state == FALSE ) )
    {
      /* conditions passed: set power limit if LTE in system. If LTE not in
         system, state remains pending and the check will happen again when
         it re-enters */
      if( coex_wwan_state_info[CXM_TECH_LTE].state_data.num_link_info_sets != 0 )
      {
        if( coex_pwr_lmt_info[cc].controller_state == COEX_PWR_LMT_PENDING )
        {
          coex_pwr_lmt_info[cc].controller_state = COEX_PWR_LMT_ACTIVE;
          succ_ind_mask |= COEX_PLCSM_CONTROLLER_TX_PWR_LMT_ENFORCED_V01;
          cxm_counter_event( CXM_CNT_CONTR_PWR_LMT_HONORED, 
                             (uint32) coex_wwan_state_info[CXM_TECH_LTE].plcy_parms.params.lte.controller_tx_power_limit );
          CXM_MSG_1( HIGH, "Starting controller power limiting (cc=%d)", cc );
        }

        if( coex_pwr_lmt_info[cc].wci2_state == COEX_PWR_LMT_PENDING )
        {
          coex_pwr_lmt_info[cc].wci2_state = COEX_PWR_LMT_ACTIVE;
          succ_ind_mask |= COEX_PLCSM_WCI2_TX_PWR_LMT_ENFORCED_V01;

          /* start watchdog timer */
          timer_set( &coex_pwr_lmt_wdog_timer,
                     (timetick_type) coex_wwan_state_info[CXM_TECH_LTE].plcy_parms.params.lte.wci2_tx_pwrlmt_timeout,
                     0, T_MSEC );
          cxm_counter_event( CXM_CNT_WCI2_PWR_LMT_HONORED, 
                             (uint32) coex_wwan_state_info[CXM_TECH_LTE].plcy_parms.params.lte.wci2_power_limit );
          CXM_MSG_1( HIGH, "Starting WCI-2 power limiting (cc=%d)", cc );
        }

        /* keep track of successful CC limits and set all at once at the end */
        cc_success_mask |= COEX_CC_TO_MASK( cc );

        /* send out the indication if necessary */
        coex_prep_and_send_condition_success_ind( succ_ind_mask );
      }
    }
    else
    {
      /* condition failed; send IND */
      if( coex_pwr_lmt_info[cc].controller_state == COEX_PWR_LMT_PENDING )
      {
        /* controller power limiting was not enforced. */
        coex_pwr_lmt_info[cc].controller_state = COEX_PWR_LMT_OFF;

        /* One or more conditions failed for controller, fill in appropriately */
        if( path_loss >= coex_plcy_parms.link_path_loss_threshold )
        {
          controller_tx_pwrlmt_fail_cond |= COEX_PLCFM_LINK_PATH_LOSS_THLD_CROSSED_V01;
        }
        if( filtered_rbs <= coex_plcy_parms.filtered_rb_threshold )
        {
          controller_tx_pwrlmt_fail_cond |= COEX_PLCFM_FILTERED_RB_THLD_CROSSED_V01;
        }
        /*We will not report fail on RRC procdure if we are disregarding it*/
        if( msg_ptr->cndtns[i].rrc_state && 
            !COEX_SYS_ENABLED(CXM_SYS_BHVR_DISREGARD_RRC_PROC) )
        {
          controller_tx_pwrlmt_fail_cond |= COEX_PLCFM_RRC_PROCEDURE_ACTIVE_V01;
        }
      }
      if( coex_pwr_lmt_info[cc].wci2_state == COEX_PWR_LMT_PENDING )
      {
        coex_pwr_lmt_info[cc].wci2_state = COEX_PWR_LMT_OFF;

        /* One or more conditions failed for wci2, fill in appropriately */
        if( path_loss >= coex_plcy_parms.link_path_loss_threshold )
        {
          wci2_tx_pwrlmt_fail_cond |= COEX_PLCFM_LINK_PATH_LOSS_THLD_CROSSED_V01;
        }
        if( filtered_rbs <= coex_plcy_parms.filtered_rb_threshold )
        {
          wci2_tx_pwrlmt_fail_cond |= COEX_PLCFM_FILTERED_RB_THLD_CROSSED_V01;
        }
        /*We will not report fail on RRC procdure if we are disregarding it*/
        if( msg_ptr->cndtns[i].rrc_state && 
            !COEX_SYS_ENABLED(CXM_SYS_BHVR_DISREGARD_RRC_PROC) )
        {
          wci2_tx_pwrlmt_fail_cond |= COEX_PLCFM_RRC_PROCEDURE_ACTIVE_V01;
        }
      }

      /* send the indication over QMI if necessary */
      coex_prep_and_send_condition_fail_ind( 0, controller_tx_pwrlmt_fail_cond, 
                                             wci2_tx_pwrlmt_fail_cond );
    }

    CXM_TRACE( CXM_TRC_MSGR_LTE_TX_PWR_LMT, msg_ptr->cndtns[i].dl_pathloss,
               msg_ptr->cndtns[i].rb_filtered, msg_ptr->cndtns[i].rrc_state,
               CXM_TRC_EN );
  }

  if( cc_success_mask != 0 )
  {
    /* one or more states may have changed, set power limit appropriately */
    coex_check_and_set_power_limit( cc_success_mask );
  }

  CXM_TRACE( CXM_TRC_MSGR_LTE_TX_PWR_LMT, 0, 0, 0, CXM_TRC_AND_PKT_EN, 
             CXM_LOG_WCI2_PWR_LMT_STATE, CXM_LOG_CONTROLLER_PWR_LMT_STATE );

  return retval;
}

/*=============================================================================

  FUNCTION:  coex_handle_lte_tx_power_limit_ind

=============================================================================*/
/*!
    @brief
    Function to handle indication that RRC procedure is in progress. If we
    are power limiting, stop and send condition fail

    @return
    errno_enum_type
*/
/*===========================================================================*/
errno_enum_type coex_handle_lte_tx_power_limit_ind (
  void *rcv_msgr_msg_ptr
)
{
  cxm_carrier_e                         cc;
  errno_enum_type                       retval = E_SUCCESS;
  cxm_coex_tx_pwr_lmt_lte_cndtns_ind_s  *msg_ptr;
  coex_pwrlmt_condition_fail_mask_v01   controller_tx_pwrlmt_fail_cond = 0;
  coex_pwrlmt_condition_fail_mask_v01   wci2_tx_pwrlmt_fail_cond = 0;
  boolean coex_reenforce_power_limit_reqd = FALSE;
  /*-----------------------------------------------------------------------*/
  if ( COEX_SYS_ENABLED(CXM_SYS_BHVR_DISREGARD_RRC_PROC) )
  {
    return retval;
  }

  /* De-mystify the received message pointer to the appropriate type */
  msg_ptr = (cxm_coex_tx_pwr_lmt_lte_cndtns_ind_s *) rcv_msgr_msg_ptr;
  cc = msg_ptr->cc_id;
  CXM_ASSERT( cc < CXM_CARRIER_MAX );

  /* check if RRC Procedure in progress AND currently power limiting */
  if( msg_ptr->rrc_state )
  {
    /* fill in indication and disable power limiting */
    if( coex_pwr_lmt_info[cc].controller_state == COEX_PWR_LMT_ACTIVE )
    {
      controller_tx_pwrlmt_fail_cond |= COEX_PLCFM_RRC_PROCEDURE_ACTIVE_V01;
      coex_pwr_lmt_info[cc].controller_state = COEX_PWR_LMT_RRC_PROC_ONGOING;
    }

    if( coex_pwr_lmt_info[cc].wci2_state == COEX_PWR_LMT_ACTIVE )
    {
      wci2_tx_pwrlmt_fail_cond |= COEX_PLCFM_RRC_PROCEDURE_ACTIVE_V01;
      coex_pwr_lmt_info[cc].wci2_state = COEX_PWR_LMT_RRC_PROC_ONGOING;
    }

    /* Power limit states have changed. Power limit needs to be cleared */
    coex_check_and_set_power_limit( COEX_CC_TO_MASK(cc) );

    coex_prep_and_send_condition_fail_ind( 0, 
      controller_tx_pwrlmt_fail_cond, 
      wci2_tx_pwrlmt_fail_cond 
    );
  }
  else
  {
    /* manage controller power limit */
    if( ( coex_pwr_lmt_info[cc].controller_state == COEX_PWR_LMT_OFF ||
          coex_pwr_lmt_info[cc].controller_state == COEX_PWR_LMT_RRC_PROC_ONGOING ) &&
        ( coex_wwan_state_info[CXM_TECH_LTE].carrier[cc].policy & 
          COEX_PCM_ENFORCE_CONTROLLER_TX_POWER_LIMIT_V01 ))
    {
      /* controller power limiting now enforced */
      coex_pwr_lmt_info[cc].controller_state = COEX_PWR_LMT_PENDING;
      coex_reenforce_power_limit_reqd = TRUE;
    }

    /* manage wci2 power limit */
    if( ( coex_pwr_lmt_info[cc].wci2_state == COEX_PWR_LMT_RRC_PROC_ONGOING ) &&
      ( coex_wwan_state_info[CXM_TECH_LTE].carrier[cc].policy & 
        COEX_PCM_REACT_TO_WCI2_TYPE6_TX_POWER_LIMIT_V01 ) )
    {
      /* wci2 power limiting now enforced */
      coex_pwr_lmt_info[cc].wci2_state = COEX_PWR_LMT_PENDING;
      coex_reenforce_power_limit_reqd = TRUE;
    }

    if( coex_reenforce_power_limit_reqd )
    {
      /* call to ML1 to check power limiting conditions */
      coex_check_power_limiting_conditions( COEX_CC_TO_MASK(cc) );
    }
  }

  CXM_MSG_2( MED, "recvd RRC Proc status[cc=%d]: %x", cc, msg_ptr->rrc_state );
  CXM_TRACE( CXM_TRC_MSGR_LTE_RRC_STATE, msg_ptr->rrc_state, 
             coex_pwr_lmt_info[cc].controller_state, 
             coex_pwr_lmt_info[cc].wci2_state , CXM_TRC_EN );

  return retval;
}

/*=============================================================================

  FUNCTION:  coex_handle_lte_wcn_report_ind

=============================================================================*/
/*!
    @brief
    Handle message from ML1 to CXM reporting FW WCN threshold exceeded

    @return
    errno_enum_type
*/
/*===========================================================================*/
errno_enum_type coex_handle_wcn_txfrndnl_report_ind (
  void *rcv_msgr_msg_ptr
)
{
  errno_enum_type                          retval = E_SUCCESS;
  cxm_coex_wcn_txfrndnl_report_ind_s      *msg_ptr;
  coex_txfrmdnl_condition_failure_mask_v01 tx_subframe_denials_status = 0;
  /*-----------------------------------------------------------------------*/
  /* De-mystify the received message pointer to the appropriate type */
  msg_ptr = (cxm_coex_wcn_txfrndnl_report_ind_s *) rcv_msgr_msg_ptr;

  CXM_TRACE( CXM_TRC_MSGR_WCN_RPT, msg_ptr->stats.result, 
                   msg_ptr->stats.num_wcn_reqs, 0,
                   CXM_TRC_EN );

  /*error number from msgr should line up with QMI IDL error number*/
  tx_subframe_denials_status = msg_ptr->stats.result;

  CXM_MSG_2( MED, "WCN Prio condition fail: %d blanking requests recvd, mask=%x",
             msg_ptr->stats.num_wcn_reqs,
             tx_subframe_denials_status );

  coex_prep_and_send_condition_fail_ind( tx_subframe_denials_status, 0, 0 );

  return retval;
}

/*=============================================================================

  FUNCTION:  coex_handle_tech_wcn_txfrndnl_report_ind

=============================================================================*/
/*!
    @brief
    Handle message from tech L1 to CXM reporting FW WCN threshold exceeded

    @return
    errno_enum_type
*/
/*===========================================================================*/
errno_enum_type coex_handle_tech_wcn_txfrndnl_report_ind (
  void *rcv_msgr_msg_ptr
)
{
  errno_enum_type                          retval = E_SUCCESS;
  cxm_coex_tech_tx_frm_dnl_report_ind_s    *msg_ptr;
  /*-----------------------------------------------------------------------*/
  /* De-mystify the received message pointer to the appropriate type */
  msg_ptr = (cxm_coex_tech_tx_frm_dnl_report_ind_s *) rcv_msgr_msg_ptr;

  CXM_TRACE( CXM_TRC_MSGR_WCN_RPT, msg_ptr->stats.result, 
                   msg_ptr->stats.num_wcn_reqs, msg_ptr->tech_id, CXM_TRC_EN );

  CXM_MSG_3( MED, "WCN Prio condition fail: %d blanking requests recvd, result=%x, tech=%d",
             msg_ptr->stats.num_wcn_reqs,
             msg_ptr->stats.result, 
             msg_ptr->tech_id );

  return retval;
}

/*=============================================================================

  FUNCTION:  coex_handle_activity_adv_ntc

=============================================================================*/
/*!
    @brief
    Handle message from L1 to CXM reporting activity in advance

    @return
    errno_enum_type
*/
/*===========================================================================*/
errno_enum_type coex_handle_activity_adv_ntc (
  void *rcv_msgr_msg_ptr
)
{
  errno_enum_type                          retval = E_SUCCESS;
  cxm_coex_tech_activity_adv_ntc_ind_s     *msg_ptr;
  /*-----------------------------------------------------------------------*/
  /* De-mystify the received message pointer to the appropriate type */
  msg_ptr = (cxm_coex_tech_activity_adv_ntc_ind_s *) rcv_msgr_msg_ptr;

  if ( COEX_SYS_ENABLED(CXM_SYS_BHVR_SMEM_DATA) )
  {
    cxm_smem_handle_activity_adv_ntc(msg_ptr);
  }

  return retval;
}

/*=============================================================================

  FUNCTION:  coex_handle_tx_ant_sel_ind

=============================================================================*/
/*!
  @brief
    Handle set transmit (Tx) antenna selection message
 
  @return
    errno_enum_type
*/
/*===========================================================================*/
errno_enum_type coex_handle_tx_ant_sel_ind (
  void *rcv_msgr_msg_ptr
)
{
  errno_enum_type  retval = E_SUCCESS;
  uint32           old_tech_tx_active_mask = coex_state.conn_tech_mask;
  uint32           old_tech_tx_power_mask  = coex_state.tx_pwr_tech_mask;
  cxm_tx_ant_sel_e old_tx_ant_sel          = coex_state.tx_ant_sel;
  cxm_coex_tx_ant_sel_ind_s *msg_ptr;
  /*-----------------------------------------------------------------------*/
  /* De-mystify the received message pointer to the appropriate type */
  msg_ptr = (cxm_coex_tx_ant_sel_ind_s *) rcv_msgr_msg_ptr;
  coex_state.tx_ant_sel = msg_ptr->tx_ant_sel;
  CXM_MSG_3( HIGH, "Handling update for type7[ant_tx] plcy_actv[%d] - old:%d new:%d",
             (COEX_PCM_SEND_WCI2_TYPE7_MDM_TX_ANT_SEL_V01 &
             coex_wwan_state_info[CXM_TECH_LTE].plcy_parms.tech_policy),
             old_tx_ant_sel,
             coex_state.tx_ant_sel );

  if( COEX_SYS_ENABLED(CXM_SYS_BHVR_WCI2_DATA) &&
      (COEX_PCM_SEND_WCI2_TYPE7_MDM_TX_ANT_SEL_V01 &
      coex_wwan_state_info[CXM_TECH_LTE].plcy_parms.tech_policy) )
  {
    coex_check_wci2_type7_state( old_tech_tx_active_mask,
                                 old_tech_tx_power_mask,
                                 old_tx_ant_sel,
                                 COEX_WCI2_T7_TX_ANT_SEL_EVNT_MSK );
  }
  return retval;
}

/*=============================================================================

  FUNCTION:  coex_set_wci2_type7_filter_time

=============================================================================*/
/*!
    @brief
    Set timeout for type 7 filter timer

    @return
    void
*/
/*===========================================================================*/
void coex_set_wci2_type7_filter_time( 
  uint8  event,
  uint32 time_ms 
)
{
  /*-----------------------------------------------------------------------*/
  switch( (coex_wci2_t7_event_e)event )
  {
    case COEX_WCI2_T7_CONN_ST_EVNT:
      coex_plcy_parms.t7_con_holdoff = time_ms;
      break;

    case COEX_WCI2_T7_PWR_ST_EVNT:
      coex_plcy_parms.t7_pwr_holdoff = time_ms;
      break;

    case COEX_WCI2_T7_TX_ANT_SEL_EVNT:
    default:
      CXM_MSG_1( HIGH, "Unsupported/Incorrect update for WCI2 type7 event %d", event);
      break;
  }
  return;
}

/*=============================================================================

  FUNCTION:  coex_tech_metrics_initiate_req

=============================================================================*/
/*!
    @brief
    Start/stop/read SINR. Fill out params based on requested action.

    @return
    void
*/
/*===========================================================================*/
errno_enum_type coex_tech_metrics_initiate_req(
  coex_wwan_tech_v01 tech,     /* LTE, GSM1/2/3, TDS */
  cxm_action_e       action,   /* START/STOP/READ */
  uint32             alpha_q8, /* only for START */
  uint32             msg_id,   /* only for READ */
  cxm_carrier_e      carrier   /* carrier id, only LTE for now */
)
{
  errno_enum_type                 retval = E_SUCCESS;
  cxm_coex_metrics_lte_sinr_req_s lte_msg;
  cxm_coex_metrics_req_s          msgr_tech_msg;
  uint32                          trc_code = 0;
  /*-----------------------------------------------------------------------*/
  switch( action )
  {
    case CXM_ACTION_START:
      CXM_TRACE(CXM_TRC_QMI_STRT_SINR_REQ, alpha_q8, tech, carrier, CXM_TRC_EN );
      CXM_MSG_2( HIGH, "SINR Started, tech=%d, cc=%d", tech, carrier );
      /* make sure alpha within range [0 - 1] */
      if( alpha_q8 > COEX_LTE_METRICS_MAX_ALPHA )
      {
        alpha_q8 = COEX_LTE_METRICS_MAX_ALPHA;
      }

      if ( tech == CXM_TECH_LTE )
      {
        lte_msg.action = CXM_ACTION_START;
        lte_msg.payload.start_params.alpha = alpha_q8;
        COEX_METRICS_STATE_START(tech, carrier);
      }
      else
      {
        msgr_tech_msg.action = CXM_ACTION_START;
        msgr_tech_msg.payload.start_params.alpha = alpha_q8;
        coex_wwan_state_info[tech].metrics_started = TRUE;
      }

      /* Turn on the uart and send policy for lte subframe marker */
      if( COEX_SYS_ENABLED(CXM_SYS_BHVR_WCI2_DATA) )
      {
        coex_manage_uart_npa_vote();
      }
      coex_prep_and_send_policy_msg( (cxm_tech_type)tech );
      break;

    case CXM_ACTION_STOP:
      CXM_MSG_2( HIGH, "SINR Stopped, tech=%d, cc=%d", tech, carrier );
      if ( tech == CXM_TECH_LTE )
      {
        lte_msg.action = CXM_ACTION_STOP;
        COEX_METRICS_STATE_STOP(tech, carrier);
      }
      else
      {
        msgr_tech_msg.action = CXM_ACTION_STOP;
        coex_wwan_state_info[tech].metrics_started = FALSE;
      }

      /* Turn off the uart and send policy for lte subframe marker */
      if( COEX_SYS_ENABLED(CXM_SYS_BHVR_WCI2_DATA) )
      {
         coex_manage_uart_npa_vote();
      }
      coex_prep_and_send_policy_msg( (cxm_tech_type)tech );
      break;

    case CXM_ACTION_READ:
      if ( tech == CXM_TECH_LTE )
      {
        lte_msg.action = CXM_ACTION_READ;
        lte_msg.payload.read_params.msg_id = msg_id;
      }
      else
      {
        msgr_tech_msg.action = CXM_ACTION_READ;
        msgr_tech_msg.payload.read_params.msg_id = msg_id;
      }
      break;

    default:
      return retval;
  }

  switch ( tech )
  {
    case CXM_TECH_LTE:
      /* send request to LTE ML1 */
      lte_msg.carrier = carrier;
      retval = cxm_msgr_send_msg( &lte_msg.msg_hdr,
                                  MCS_CXM_COEX_METRICS_LTE_SINR_REQ,
                                  sizeof(cxm_coex_metrics_lte_sinr_req_s) );
      trc_code = CXM_LOG_METRICS_LTE_SINR_STARTED;
      break;
    case CXM_TECH_TDSCDMA:
      /* send request to TL1 */
      retval = cxm_msgr_send_msg( &msgr_tech_msg.msg_hdr,
                                  MCS_CXM_COEX_METRICS_TDSCDMA_REQ,
                                  sizeof(cxm_coex_metrics_req_s) );
      trc_code = CXM_LOG_METRICS_TDS_NRRWBND_NOISE_STARTED;
      break;
    /* send request to GL1 */
    case CXM_TECH_GSM1:
      retval = cxm_msgr_send_msg( &msgr_tech_msg.msg_hdr,
                                  MCS_CXM_COEX_METRICS_GSM1_REQ,
                                  sizeof(cxm_coex_metrics_req_s) );
      trc_code = CXM_LOG_METRICS_GSM1_SINR_STARTED;
      break;
    case CXM_TECH_GSM2:
      retval = cxm_msgr_send_msg( &msgr_tech_msg.msg_hdr,
                                  MCS_CXM_COEX_METRICS_GSM2_REQ,
                                  sizeof(cxm_coex_metrics_req_s) );
      trc_code = CXM_LOG_METRICS_GSM2_SINR_STARTED;
      break;
    case CXM_TECH_GSM3:
      retval = cxm_msgr_send_msg( &msgr_tech_msg.msg_hdr,
                                  MCS_CXM_COEX_METRICS_GSM3_REQ,
                                  sizeof(cxm_coex_metrics_req_s) );
      trc_code = CXM_LOG_METRICS_GSM3_SINR_STARTED;
      break;
    default:
      break;
  }

  CXM_ASSERT( E_SUCCESS == retval );
  CXM_MSG_3( HIGH, "Initiated metrics req, tech=%d, cc=%d, action=%x", 
             tech, carrier, action );
  if ( action == CXM_ACTION_START || action == CXM_ACTION_STOP )
  {
    CXM_TRACE(0, 0, 0, 0, CXM_LOG_PKT_EN, trc_code);
  }

  return retval;
}

/*=============================================================================

  FUNCTION:  coex_handle_tech_tx_pwr_report_ind

=============================================================================*/
/*!
    @brief
    Message from WWAN techs informing of the Tx power report.

    @return
    errno_enum_type
*/
/*===========================================================================*/
errno_enum_type coex_handle_tech_tx_pwr_report_ind(
  void *rcv_msgr_msg_ptr
)
{
  errno_enum_type            retval = E_SUCCESS;
  cxm_coex_tech_tx_pwr_rpt_ind_s *msg_ptr;
  uint32 old_tech_tx_active_mask = coex_state.conn_tech_mask;
  uint32 old_tech_tx_power_mask  = coex_state.tx_pwr_tech_mask;
  cxm_tx_ant_sel_e old_tx_ant_sel = coex_state.tx_ant_sel;
  /*-----------------------------------------------------------------------*/
  /* De-mystify the received message pointer to the appropriate type */
  msg_ptr = (cxm_coex_tech_tx_pwr_rpt_ind_s *) rcv_msgr_msg_ptr;

  CXM_ASSERT( CXM_TECH_LTE == msg_ptr->tech_id );
  CXM_MSG_3( MED, "Tech %d Tx pwr state %d & Tx pwr (mW) %d",
             msg_ptr->tech_id, msg_ptr->report.lte.wci2_t7_tx_pwr_state,
             msg_ptr->report.lte.filtered_tx_pwr_mW );
  if( msg_ptr->report.lte.wci2_t7_tx_pwr_state )
  {
    coex_state.tx_pwr_tech_mask |= (1<<msg_ptr->tech_id);
  }
  else
  {
    coex_state.tx_pwr_tech_mask &= ~(1<<msg_ptr->tech_id);
  }

  if( COEX_SYS_ENABLED(CXM_SYS_BHVR_WCI2_DATA) && 
      (COEX_PCM_SEND_WCI2_TYPE7_MDM_PWR_STATE_V01 & 
      coex_wwan_state_info[CXM_TECH_LTE].carrier[CXM_CARRIER_PCC].policy) )
  {
    coex_check_wci2_type7_state( old_tech_tx_active_mask,
                                 old_tech_tx_power_mask,
                                 old_tx_ant_sel,
                                 COEX_WCI2_T7_PWR_ST_EVNT_MSK );
  }

  CXM_TRACE(0, 0, 0, 0, CXM_LOG_PKT_EN, CXM_LOG_STATE);

  return retval;
}

/*=============================================================================

  FUNCTION:  coex_apply_scan_freq_filter

=============================================================================*/
/*!
    @brief
    Helper API to apply the scan freq filter to WWAN tech's high prio event

    @return
    boolean
*/
/*===========================================================================*/
boolean coex_apply_scan_freq_filter(
  uint32 freq,
  uint8 *index
)
{
  boolean found = FALSE;
  uint8 i;
  /*-----------------------------------------------------------------------*/
  *index = 0; /* no match found to begin with */
  for( i=0 ; i < coex_scan_freq_filter_info.bands_len+1 ; i++ )
  {
    if( freq >= coex_scan_freq_filter_info.processed_filter[i].freq_start &&
        freq <= coex_scan_freq_filter_info.processed_filter[i].freq_stop )
    {
      found = TRUE;
      *index = i;
      break;
    }
  }

  return found;
}

/*=============================================================================

  FUNCTION:  coex_prep_and_send_wci2_type4

=============================================================================*/
/*!
    @brief
    Helper API to send out WCI2 type4 scan freq message

    @return
    void
*/
/*===========================================================================*/
void coex_prep_and_send_wci2_type4(
  cxm_tech_type tech_id,
  uint8 index
)
{
  wci2_msg_type_s t4_msg;
  /*-----------------------------------------------------------------------*/
  if( COEX_SYS_ENABLED(CXM_SYS_BHVR_WCI2_DATA) )
  {
    if( COEX_PCM_SEND_WCI2_TYPE4_SCAN_FREQ_V01 & 
        coex_wwan_state_info[tech_id].plcy_parms.tech_policy )  
    {
      coex_scan_freq_filter_info.reported_filter_index = index;
      coex_scan_freq_filter_info.reported_scan_freq =
        coex_scan_freq_filter_info.bands[index].freq * MHZ_KHZ_CONVERSION;
      coex_scan_freq_filter_info.reported_tech_id = tech_id;

      t4_msg.type = WCI2_TYPE4;
      /* payload/index can only be a number between [0-31]
       * 0 - reserved to indicate stop event or no scanning
       * and 1 to 31 represent indices 0 to 30 in the SCAN filter set using QMI message
       */
      t4_msg.data.type4_freq_index = index; 
      wci2_send_msg( &t4_msg );
      CXM_MSG_3( MED, "Sending wci2 type4: payload/index %d, tech %d, freq %d",
                 index, tech_id, coex_scan_freq_filter_info.reported_scan_freq );
    }
  }

  return;
}

/*=============================================================================

  FUNCTION:  coex_handle_high_priority_ind

=============================================================================*/
/*!
    @brief
    Message from WWAN techs informing of high priority events.

    @return
    errno_enum_type
*/
/*===========================================================================*/
errno_enum_type coex_handle_high_priority_ind(
  void *rcv_msgr_msg_ptr
)
{
  errno_enum_type           retval = E_SUCCESS;
  cxm_coex_high_prio_ind_s *msg_ptr;
  uint8 i;
  uint8 filter_index = 0;
  boolean match = FALSE;
  static uint8 active_event_cnt = 0;
  static uint8 service_started = FALSE;
  /*-----------------------------------------------------------------------*/
  /* De-mystify the received message pointer to the appropriate type */
  msg_ptr = (cxm_coex_high_prio_ind_s *) rcv_msgr_msg_ptr;
  CXM_ASSERT( msg_ptr->num_freqs <= COEX_MAX_HIGH_PRIO_FREQ );

  filter_index = 0;
  CXM_MSG_6( MED, "WWAN tech %d high priority event %d; start %x end %x; #freq %d; e_cnt %d",
             msg_ptr->tech_id, msg_ptr->op_id, msg_ptr->start, msg_ptr->end,
             msg_ptr->num_freqs, active_event_cnt );

  /* send a log packet with high prio event */
  cxm_high_prio_log.tech_id = msg_ptr->tech_id;
  cxm_high_prio_log.op_id = msg_ptr->op_id;
  cxm_high_prio_log.start = msg_ptr->start;
  cxm_high_prio_log.end= msg_ptr->end;
  cxm_high_prio_log.num_freqs = msg_ptr->num_freqs;
  for ( i=0; i<msg_ptr->num_freqs; i++ )
  {
    cxm_high_prio_log.freq[i] = msg_ptr->freq[i];
  }
  CXM_TRACE(0, 0, 0, 0, CXM_LOG_PKT_EN, CXM_LOG_HIGH_PRIO_INFO);

  if( COEX_SYS_ENABLED(CXM_SYS_BHVR_WCI2_DATA) && msg_ptr->tech_id == CXM_TECH_LTE )
  {
    switch( msg_ptr->op_id )
    {
      case CXM_HIGH_PRIO_START_SVC:
        /* start listening to high prio events */
        service_started = TRUE;
        active_event_cnt = 0;
        break;
      case CXM_HIGH_PRIO_STOP_SVC:
        /* stop listening to high prio events, clean up any ongoing events */
        service_started = FALSE;
        if( active_event_cnt != 0 )
        {
          CXM_MSG_0( ERROR, "SCAN filter stop(s) missed, resetting" );
          coex_prep_and_send_wci2_type4( msg_ptr->tech_id, 0x00 );
          active_event_cnt = 0;
        }
        break;
      case CXM_HIGH_PRIO_MEAS:
      case CXM_HIGH_PRIO_PAGE:
      case CXM_HIGH_PRIO_ACQ_SRCH:
        if( coex_scan_freq_filter_info.filter_on && service_started )
        {
          for( i = 0; i < msg_ptr->num_freqs; i++ )
          {
            match = coex_apply_scan_freq_filter( msg_ptr->freq[i], &filter_index );
            CXM_MSG_7( HIGH, "SCAN filter:[match/tech/op/freq/indx/st/en][%d/%d/%d/%d/%d/%d/%d]",
                       match, msg_ptr->tech_id, msg_ptr->op_id, msg_ptr->freq[i],
                       filter_index, msg_ptr->start, msg_ptr->end );
            if( match )
            {
              /* Ensure that payload for type4 is between [1-31]
               * 0 is reserved to indicate scan stop 
               * for all other indices from the filter table add 1 to report
               */
              /* check whether 'start' or 'end' event */
              if( msg_ptr->start == 0xFFFFFFFF &&
                  msg_ptr->end == 0xFFFFFFFF )
              {
                /* 'start' of SCAN freq event
                    - send WCI2 type4 message with (filter_index) as payload */
                if( active_event_cnt == 0 )
                {
                  coex_prep_and_send_wci2_type4( msg_ptr->tech_id, filter_index );
                }
                active_event_cnt++;
              }
              else if( msg_ptr->start == 0x000000000 &&
                       msg_ptr->end == 0x000000000 &&
                       active_event_cnt > 0)
              {
                active_event_cnt--;
                /* 'end' of SCAN freq event
                    - send WCI2 type4 message with 0x00 payload */
                if( active_event_cnt == 0 )
                {
                  coex_prep_and_send_wci2_type4( msg_ptr->tech_id, 0x00 );
                }
              }
            }
          }
        }
        else
        {
          CXM_MSG_2( ERROR, "high priority event ignored: [filter_on/in_service][%d/%d]",
                     coex_scan_freq_filter_info.filter_on, service_started );
        }
        break;
      default:
        CXM_MSG_1( MED, "Unsupported COEX high priority event with id %d", msg_ptr->op_id );
    }
  }

  if ( COEX_SYS_ENABLED(CXM_SYS_BHVR_SMEM_DATA) )
  {
    if ( msg_ptr->tech_id == CXM_TECH_LTE && COEX_SYS_ENABLED(CXM_SYS_BHVR_LTE_HIGH_PRIO_MSG) )
    {
      cxm_smem_handle_high_prio(msg_ptr);
    }   
    else if ( (msg_ptr->tech_id == CXM_TECH_TDSCDMA || msg_ptr->tech_id == CXM_TECH_GSM1 || 
              msg_ptr->tech_id == CXM_TECH_GSM2 || msg_ptr->tech_id == CXM_TECH_GSM3) &&  
              COEX_SYS_ENABLED(CXM_SYS_BHVR_GSM_TDS_HIGH_PRIO_MSG) )
    {
      cxm_smem_handle_high_prio(msg_ptr);
    }
  }   

  return retval;
}

/*=============================================================================

  FUNCTION:  coex_handle_set_scan_freq_band_filter_info_req

=============================================================================*/
/*!
    @brief
    Function to handle QMI req to set SCAN freq band filter info

    @return
    qmi_csi_cb_error
*/
/*===========================================================================*/
qmi_csi_cb_error coex_handle_set_scan_freq_band_filter_info_req (
  void          *connection_handle,
  qmi_req_handle req_handle,
  unsigned int   msg_id,
  void          *req_c_struct,
  unsigned int   req_c_struct_len,
  void          *service_cookie
)
{
  qmi_csi_cb_error req_cb_retval = QMI_CSI_CB_NO_ERR;
  coex_set_scan_freq_band_filter_resp_msg_v01 response;
  coex_set_scan_freq_band_filter_req_msg_v01 *request;
  uint32 i;
  /*-----------------------------------------------------------------------*/
  CXM_ASSERT( req_c_struct != NULL );

  /* De-mystify the received message pointer to the appropriate type */
  request = (coex_set_scan_freq_band_filter_req_msg_v01 *) req_c_struct;

  CXM_MSG_1 ( HIGH, "Set SCAN freq band filter request",
             request->bands_len );
  /* Init the memory available for the REQ and LTE ML1 message */
  memset( &response, 0, sizeof( coex_set_scan_freq_band_filter_resp_msg_v01 ) );

  /*clear existing band filter info*/  
  memset( &coex_scan_freq_filter_info, 0, sizeof( coex_scan_freq_band_filter_s ) );
  coex_scan_freq_filter_info.bands_len = request->bands_len;

  response.resp.result = QMI_RESULT_SUCCESS_V01;
  response.resp.error  = QMI_ERR_NONE_V01;

  /* forward band filter info to LTE ML1 */
  if(request->bands_valid)
  {
    /* SCAN freq filter can support only 31 bands (even though the struct support 32)
     * Limitation is due to the way we report type 4 message
     * - 0th index is reserved to report SCAN stop
     * - 1 to 31 indices are reported as valid payloads */
    if( request->bands_len >= COEX_WWAN_MAX_BANDS_TO_MONITOR_V01 )
    {
      response.resp.result = QMI_RESULT_FAILURE_V01;
      response.resp.error  = QMI_ERR_INVALID_ARG_V01;
    }
    else
    {
      for( i = 0; i < request->bands_len; i++ )
      {
        /* NOTE: 0 to 30 indices of incoming scan filter are stored in 1 to 31 indices in the table */
        coex_scan_freq_filter_info.bands[i+1] = request->bands[i];
        coex_scan_freq_filter_info.processed_filter[i+1].freq_start = 
          ( request->bands[i].freq - (request->bands[i].bandwidth/2) ) * MHZ_KHZ_CONVERSION;

        coex_scan_freq_filter_info.processed_filter[i+1].freq_stop = 
          ( request->bands[i].freq + (request->bands[i].bandwidth/2) ) * MHZ_KHZ_CONVERSION;
      }

      if (request->bands_len > 0) 
      {
        coex_scan_freq_filter_info.filter_on = TRUE;
      }
      else
      {
        coex_scan_freq_filter_info.filter_on = FALSE;
      }
    }
  }

  /* send QMI response */
  req_cb_retval = mqcsi_conn_mgr_send_resp_from_cb (
                    connection_handle,
                    req_handle,
                    msg_id,
                    &response,
                    sizeof( coex_set_scan_freq_band_filter_resp_msg_v01 )
                  );
  CXM_ASSERT( QMI_CSI_CB_NO_ERR == req_cb_retval );

  return req_cb_retval;
}

/*=============================================================================

  FUNCTION:  coex_handle_get_scan_freq_band_filter_info_req

=============================================================================*/
/*!
    @brief
    Function to handle QMI req to get SCAN freq band filter info

    @return
    qmi_csi_cb_error
*/
/*===========================================================================*/
qmi_csi_cb_error coex_handle_get_scan_freq_band_filter_info_req (
  void          *connection_handle,
  qmi_req_handle req_handle,
  unsigned int   msg_id,
  void          *req_c_struct,
  unsigned int   req_c_struct_len,
  void          *service_cookie
)
{
  qmi_csi_cb_error req_cb_retval = QMI_CSI_CB_NO_ERR;
  coex_get_scan_freq_band_filter_resp_msg_v01 response;
  uint32 i;
  /*-----------------------------------------------------------------------*/
  /* Init the memory available for the REQ message */
  memset( &response, 0, sizeof( coex_get_scan_freq_band_filter_resp_msg_v01 ) );

  response.resp.result = QMI_RESULT_SUCCESS_V01;
  response.resp.error  = QMI_ERR_NONE_V01;

  /* Copy over current filter info */
  response.bands_valid = TRUE;
  response.bands_len   = coex_scan_freq_filter_info.bands_len;
  /* NOTE: 0th element is reserved and empty */
  for( i = 0; i < coex_scan_freq_filter_info.bands_len; i++ )
  {
    response.bands[i] = coex_scan_freq_filter_info.bands[i+1];
  }

  /* send QMI response */
  req_cb_retval = mqcsi_conn_mgr_send_resp_from_cb (
                    connection_handle,
                    req_handle,
                    msg_id,
                    &response,
                    sizeof( coex_get_scan_freq_band_filter_resp_msg_v01 )
                  );
  CXM_ASSERT( QMI_CSI_CB_NO_ERR == req_cb_retval );

  return req_cb_retval;
}

/*=============================================================================

  FUNCTION:  coex_algos_deinit

=============================================================================*/
/*!
    @brief
    Deinitializes all COEX states as needed
 
    @return
    none
*/
/*===========================================================================*/
void coex_algos_deinit (
  void
)
{
  cxm_coex_metrics_lte_bler_req_s  msgr_lte_bler_msg;
  coex_wlan_conn_node_type        *c_prev, *c_node = coex_wlan_conn_head;
  coex_wlan_hp_node_type          *h_prev, *h_node = coex_wlan_hp_head;
  uint8                            i, j;
  /*-----------------------------------------------------------------------*/
  /* Disable COEX algorithms for the lower layers */

  /* 1. clear all timers (if any pending) */
  (void) timer_clr( &coex_pwr_lmt_wdog_timer, T_MSEC );
  CXM_MSG_0( HIGH, "Task deinit: Disabling coex timers" );

  /* 2. disable coex policy (if any) */
  for ( i = 0; i < CXM_TECH_MAX; i++ )
  {
    if( COEX_CC_IS_ACTIVE(i, CXM_CARRIER_PCC)   ||
        COEX_CC_IS_ACTIVE(i, CXM_CARRIER_SCC_0) ||
        COEX_CC_IS_ACTIVE(i, CXM_CARRIER_SCC_1) )
    {
      for( j = 0; j < CXM_CARRIER_MAX; j++ )
      {
        coex_wwan_state_info[i].carrier[j].policy = CXM_POLICY_NONE;
      }
      coex_prep_and_send_policy_msg( i );
      CXM_MSG_1( HIGH, "Task deinit: Inactive (policy) msg sent, tech=%d", i);
    }
  }

  /* 3. stop/disable all metrics (if any) collection/reporting */
  for ( i = 0; i < CXM_TECH_MAX; i++ )
  {
    for( j = 0; j < CXM_CARRIER_MAX; j++ )
    {
      if( COEX_METRICS_STARTED(i, j) )
      {
        coex_tech_metrics_initiate_req( i, CXM_ACTION_STOP, 0, 0, j);
        CXM_MSG_1( HIGH, "Task deinit: Stopping metrics, tech=%d", i );
      }
    }
  }

  if( coex_metrics_lte_bler_started )
  {
    msgr_lte_bler_msg.action = CXM_ACTION_STOP;
    (void) cxm_msgr_send_msg( &msgr_lte_bler_msg.msg_hdr,
                                MCS_CXM_COEX_METRICS_LTE_BLER_REQ,
                                sizeof(cxm_coex_metrics_lte_bler_req_s) );
    coex_metrics_lte_bler_started = FALSE;
    CXM_MSG_0( HIGH, "Task deinit: Stopping LTE BLER metrics" );
  }

  /* 4. if enabled disable QMB solution */
  if( COEX_SYS_ENABLED(CXM_SYS_BHVR_WCI2_CONTROL) )
  {
    coex_algos_deinit_v2();
    CXM_MSG_0( HIGH, "Task deinit: Disabling COEX QMB control plane" );
  }

  /* 5. disable uart interface for WCI2 solution (if enabled) */
  if ( COEX_SYS_ENABLED(CXM_SYS_BHVR_WCI2_DATA) ||
       COEX_SYS_ENABLED(CXM_SYS_BHVR_WCI2_CONTROL) )
  {
    /* deinitialize the UART */
    CXM_MSG_0( HIGH, "Task deinit: Powering down UART interface" );
    (void) wci2_client_deregister( coex_state.uart_handle );

    /* unregister from all slpc callbacks */
    slpc_deregister_notify_callback( &coex_slpc_notify_cb );
    slpc_deregister_notify_wakeup_callback( &coex_slpc_notify_wakeup_cb );
    CXM_MSG_0( HIGH, "Task deinit: Disabling all slpc callbacks" );
  }

#ifdef FEATURE_COEX_USE_NV
  if( COEX_SYS_ENABLED(CXM_SYS_BHVR_VICTIM_TABLE) )
  {
    coex_confl_deinit();
  }
#endif /* FEATURE_COEX_USE_NV */

  /* 6. Free memory allocated from QMI coex set wlan state req */
  while( c_node != NULL )
  {
    c_prev = c_node;
    c_node = c_node->next;
    CXM_MEM_FREE( c_prev );
  }
  while( h_node != NULL )
  {
    h_prev = h_node;
    h_node = h_node->next;
    CXM_MEM_FREE( h_prev );
  }

  return;
}

/*===========================================================================

  FUNCTION:  cxm_set_tech_l3_state

===========================================================================*/
/*!
  @brief
    To set WWAN tech's layer3 state
 
  @return
    errno_enum_type
*/
/*=========================================================================*/
errno_enum_type cxm_set_tech_l3_state (
  cxm_tech_type        tech,
  cxm_tech_l3_state_s *l3
)
{
  cxm_coex_tech_l3_state_ind_s msg;
  errno_enum_type retval = E_SUCCESS;
  /*-----------------------------------------------------------------------*/
  CXM_MSG_3( HIGH, "Tech[%d] called API to set L3 mode=%d & state=%d",
             tech, l3->mode, l3->state );
  msg.tech     = tech;
  msg.l3.mode  = l3->mode;
  msg.l3.state = l3->state;

  retval = cxm_msgr_send_msg( &msg.msg_hdr, 
                              MCS_CXM_COEX_TECH_L3_STATE_IND, 
                              sizeof(cxm_coex_tech_l3_state_ind_s) ); 
  return retval;
}

/*=============================================================================

  FUNCTION:  coex_handle_tech_l3_state_ind

=============================================================================*/
/*!
    @brief
    Message from WWAN techs informing of their layer 3 (RRC/CP/RR) state.

    @return
    errno_enum_type
*/
/*===========================================================================*/
errno_enum_type coex_handle_tech_l3_state_ind(
  void *rcv_msgr_msg_ptr
)
{
  errno_enum_type               retval = E_SUCCESS;
  cxm_coex_tech_l3_state_ind_s *msg_ptr;
  uint32                        old_tech_tx_active_mask = coex_state.conn_tech_mask;
  uint32                        old_tech_tx_power_mask  = coex_state.tx_pwr_tech_mask;
  cxm_tx_ant_sel_e              old_tx_ant_sel          = coex_state.tx_ant_sel;
  /*-----------------------------------------------------------------------*/
  /* De-mystify the received message pointer to the appropriate type */
  msg_ptr = (cxm_coex_tech_l3_state_ind_s *) rcv_msgr_msg_ptr;

  CXM_MSG_3( HIGH, "Processing L3 state message for tech[%d]:mode[%d],state[%d]",
             msg_ptr->tech, msg_ptr->l3.mode, msg_ptr->l3.state);

  if( msg_ptr->tech > CXM_TECH_DFLT_INVLD && msg_ptr->tech < CXM_TECH_MAX )
  {
    /* storing current RAT's layer 3 state */
    coex_wwan_state_info[msg_ptr->tech].l3 = msg_ptr->l3;

    switch ( msg_ptr->l3.mode )
    {
      case CXM_TECH_INACTIVE:
        // RAT is either going out of service or is the source RAT for an IRAT scenario
        // Update info for 'tech' reported & ignore any 'l3.state' reported
        coex_state.conn_tech_mask &= ~(1 << msg_ptr->tech);
        coex_state.tx_pwr_tech_mask &= ~(1 << msg_ptr->tech);
        break;

      case CXM_TECH_ACTIVE:
        // RAT is actively updating it's layer 3 (RRC/CP/RR) state
        // Update info for 'tech' & accordingly report it as connected or
        //   not connected over WCI2 type7 data message
        switch ( msg_ptr->l3.state )
        {
          // Capture all not-connected (no uplink activity) states here
          case CXM_TECH_L3_INACTIVE:
          case CXM_TECH_L3_DISCONNECT:
          case CXM_TECH_L3_ACQUISITION:
          case CXM_TECH_L3_IDLE:
          case CXM_TECH_L3_CSFB_IDLE:
          case CXM_TECH_L3_IDLE_NOT_CAMPED:
          case CXM_TECH_L3_IDLE_CAMPED:
          case CXM_TECH_L3_CELL_PCH:
          case CXM_TECH_L3_URA_PCH:
            coex_state.conn_tech_mask &= ~(1 << msg_ptr->tech);
            if( msg_ptr->tech != CXM_TECH_LTE )
            {
              coex_state.tx_pwr_tech_mask &= ~(1 << msg_ptr->tech);
            }
            break;

          // Capture all connected (uplink activity) states here
          case CXM_TECH_L3_CONNECTING:
          case CXM_TECH_L3_CONNECTION_ESTB:
          case CXM_TECH_L3_ACCESS:
          case CXM_TECH_L3_CSFB_ACCESS:
          case CXM_TECH_L3_CONNECTED:
          case CXM_TECH_L3_CELL_DCH:
          case CXM_TECH_L3_CELL_FACH:
          case CXM_TECH_L3_TRAFFIC:
          case CXM_TECH_L3_RACH:
          case CXM_TECH_L3_CONNECTED_CS:
          case CXM_TECH_L3_CONNECTED_PS:
            coex_state.conn_tech_mask |= (1 << msg_ptr->tech);
            if( msg_ptr->tech != CXM_TECH_LTE )
            {
              coex_state.tx_pwr_tech_mask |= (1 << msg_ptr->tech);
            }
            break;

          case CXM_TECH_L3_SUSPEND:
          default:
            CXM_MSG_2( HIGH, "Tech[%d] L3 state[%d] not supported",
                       msg_ptr->tech, msg_ptr->l3.state );
        }
        break; 

      default:
        CXM_MSG_2( HIGH, "Tech[%d] L3 mode[%d] not supported",
                   msg_ptr->tech, msg_ptr->l3.mode );
    }

    /* update tech's conn & pwr state needed for wci2 type7 data */
    if( COEX_SYS_ENABLED(CXM_SYS_BHVR_WCI2_DATA) && 
        ((COEX_PCM_SEND_WCI2_TYPE7_MDM_CONN_STATE_V01 &
         coex_wwan_state_info[CXM_TECH_LTE].plcy_parms.tech_policy) ||
         (COEX_PCM_SEND_WCI2_TYPE7_MDM_PWR_STATE_V01 &
          coex_wwan_state_info[CXM_TECH_LTE].plcy_parms.tech_policy)) )
    {
      coex_check_wci2_type7_state( old_tech_tx_active_mask,
                                   old_tech_tx_power_mask,
                                   old_tx_ant_sel,
                                   COEX_WCI2_T7_ALL_EVNT_MSK );
    }
  }
  else
  {
    CXM_MSG_1( ERROR, "Invalid tech[%d] reporting L3 state", msg_ptr->tech );
  }
  return retval; 
}

/*=============================================================================

  FUNCTION:  coex_handle_wlan_conn_from_ds

=============================================================================*/
/*!
    @brief
    HAndles WLAN connection state when it comes in from data services via msgr

    @return
    errno_enum_type
*/
/*===========================================================================*/
errno_enum_type coex_handle_wlan_conn_from_ds(
  void *rcv_msgr_msg_ptr
)
{
  errno_enum_type                 retval = E_SUCCESS;
  cxm_coex_wlan_conn_ind_s        *msg_ptr;
  uint8                           i;
  boolean                         ssid_flag = FALSE;
  coex_set_wlan_state_req_msg_v01 wlan_conn;
  
  /*-----------------------------------------------------------------------*/
  /* De-mystify the received message pointer to the appropriate type */
  msg_ptr = (cxm_coex_wlan_conn_ind_s *) rcv_msgr_msg_ptr;
  CXM_ASSERT(msg_ptr->ssid_len <= CXM_MAX_SSID_LEN);

  CXM_MSG_5( HIGH, "received WLAN state from DS, ssid_len: %d, freq: %d, bw: %d, state: %d, mode: %d", 
             msg_ptr->ssid_len, msg_ptr->band[0].freq, msg_ptr->band[0].bw,
             msg_ptr->state, msg_ptr->mode );

  /* determine the connection handle to use so that the general handler for WLAN
     state can be called */
  for ( i = 0; i < CXM_MAX_NUM_SSIDS; i++ )
  {
    if ( cxm_ssid_list[i].ssid_len == msg_ptr->ssid_len && 
         !memcmp( cxm_ssid_list[i].ssid, msg_ptr->ssid, (sizeof(char) * cxm_ssid_list[i].ssid_len) ) )
    {
      ssid_flag = TRUE;
      break;
    }
  }

  /* remove SSID from the list if it is disabled */
  if ( ssid_flag && msg_ptr->state == CXM_WLAN_DISABLED )
  {
    cxm_ssid_list[i].ssid_len = 0;
  }
  /* if it was not found, add it in the first available slot. Also
     make sure that the SSID is getting enabled before storing it*/
  else if ( !ssid_flag && msg_ptr->state != CXM_WLAN_DISABLED )
  {
    for ( i = 0; i < CXM_MAX_NUM_SSIDS; i++ )
    {
      if ( cxm_ssid_list[i].ssid_len == 0 )
      {
        cxm_ssid_list[i].ssid_len = msg_ptr->ssid_len;
        memscpy( (void *)(cxm_ssid_list[i].ssid), (sizeof(char) * cxm_ssid_list[i].ssid_len), 
                 (void *)(msg_ptr->ssid), (sizeof(char) * msg_ptr->ssid_len));
        ssid_flag = TRUE;
        break;
      }
    }

    /* assert if we run out of SSIDs*/
    CXM_ASSERT(ssid_flag);
  }

  /* fill out WLAN connection info*/
  wlan_conn.conn_info_valid = TRUE;
  wlan_conn.conn_info.handle = i;
  wlan_conn.conn_info.band_len = msg_ptr->band_len;
  
  for ( i = 0; i < wlan_conn.conn_info.band_len; i++ )
  {
    wlan_conn.conn_info.band[i].freq = msg_ptr->band[i].freq; 
    wlan_conn.conn_info.band[i].bandwidth = msg_ptr->band[i].bw;
  }

  wlan_conn.conn_info.state = (coex_wlan_conn_state_enum_v01)msg_ptr->state;
  wlan_conn.conn_info.mode = (coex_wlan_conn_mode_enum_v01)msg_ptr->mode;

  /* call common handler for conn state */
  coex_update_wlan_conn_state_info(&wlan_conn);
#ifdef FEATURE_COEX_USE_NV
  if( COEX_SYS_ENABLED(CXM_SYS_BHVR_VICTIM_TABLE) )
  {
    coex_confl_update_wcn_tech( CXM_TECH_WIFI, CXM_WLAN_CONN_TYPE );
  }
#endif

  return retval;
}

