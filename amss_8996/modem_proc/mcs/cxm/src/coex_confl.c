/*!
  @file
  coex_confl.c

  @brief
  This file contains the process of finding and choosing an active WCN/WWAN
  conflict.

  @ingroup per_implementation
*/
/*=============================================================================

  Copyright (c) 2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/mcs/cxm/src/coex_confl.c#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
06/15/15   tak     Support WWAN overlap list
05/15/15   btl     Support multiple conflict callbacks
05/15/15   btl     Initial Revision, moved from coex_algos.c

=============================================================================*/

/*=============================================================================

                           INCLUDE FILES

=============================================================================*/
#include <stringl.h> /* memscpy */
#include <cxm.h>
#include "cxm_utils.h"
#include "coex_algos.h"
#include "coex_confl.h"

/*=============================================================================

                         DEFINES AND MACROS

=============================================================================*/
/* max number of conflicts per group */
#define COEX_GROUP_CONFLS_MAX CXM_CARRIER_MAX

/* max number of groups */
#define COEX_GROUPS_MAX 256

/* max number of conflict callbacks */
#define COEX_CONFL_CB_MAX 2

#define KHZ_HZ_CONVERSION 1000

/* max of two values */
#define COEX_MAX( a, b ) ( a > b ? a : b )
/* min of two values */
#define COEX_MIN( a, b ) ( a > b ? b : a )

/* [ 1 bit | 5 bits   | 2 bits  | 24 bits ]
 * [ UL/DL | reserved | match_e | overlap ]
 * assign each one a priority based on the following criteria:
 *  xx. Group with most conflicts
 *   1. Direction
 *   2. Center frequency in conflict range
 *   3. Greatest overlap (kHz), range 0:16,777,215 */
#define COEX_WWAN_CONFL_PRIO(ul, match, overlap) \
  ( ((ul)<<31) | ((match)<<24) | ((overlap)&0xFFFFFF) )

#define COEX_GET_MATCH_TYPE_FROM_PRIO(prio) \
  (((prio) >> 24) & 0x03)

/*=============================================================================

                         TYPEDEFS

=============================================================================*/
/* modem-side info for triggering a conflict */
typedef struct
{
  /* start freq of conflict in kHz */
  uint32                            freq_start;

  /* stop freq of conflict in kHz */
  uint32                            freq_stop;

  /* FDD/TDD/Both */
  coex_wwan_operating_dimension_v01 op_dim;

  /* UL/DL/Both */
  cxm_tech_link_direction           dir;

  /* priority is a combination of factors:
   * [ 8 bits          | 2 bits  | 22 bits ]
   * [ confls in group | match_e | overlap ] */
  uint32                            prio[CXM_CARRIER_MAX];

  /* if this conflict is active (from wwan perspective), this
   * points to the next active wwan conflict */
  struct _coex_conflict_s          *next_active[CXM_CARRIER_MAX];

} coex_wwan_conflict_s;

/* wcn-side info for detecting a conflict */
typedef struct
{
  /* wcn tech for which this conflict is defined */
  cxm_wcn_tech_type             tech;

  /* start freq of conflict in MHz */
  uint32                        freq_start;

  /* stop freq of conflict in MHz */
  uint32                        freq_stop;

  /* are all the conflicts active, and if so what match type? */
  coex_conflict_match_e         match[COEX_WCN_MODE_MAX];

} coex_wcn_conflict_s;

typedef struct _coex_conflict_s
{
  /* victim table index from NV */
  uint8                index;

  /* row number from excel spreadsheet */
  uint16               xl_row_num;

  /* is this conflict valid from a group perspective? */
  boolean              group_valid;

  /* is there overlap with all the WWAN ranges in the group?
     this is important for sending WLAN the indexes with channels 
     to avoid even when WLAN is not active. */
  boolean              wwan_group_valid;

  /* wwan half of conflict definition */
  coex_wwan_conflict_s wwan;

  /* wcn half of conflict definition */
  coex_wcn_conflict_s  wcn;

} coex_conflict_s;

typedef struct
{
  /* group number */
  uint16                 group;

  /* num of conflicts in this group */
  uint8                  num_confls;

  /* array of conflicts in this group */
  coex_conflict_s       *confls[COEX_GROUP_CONFLS_MAX];

} coex_conflict_group_s;

/* array of pointers to conflict definitions */
/* each tech will have a run-time allocated array of conflict defs */
typedef struct
{
  uint8             num_confls;
  coex_conflict_s **confls;
  coex_conflict_s  *active_head[CXM_CARRIER_MAX];
  int16             best[CXM_CARRIER_MAX];
  uint16            best_row_num[CXM_CARRIER_MAX];
} coex_wwan_victim_tbl;

typedef struct
{
  uint8             num_confls;
  coex_conflict_s **confls;
} coex_wcn_victim_tbl;

/* used for LTE-U: hard-coded conflict not from victim table */
typedef struct
{
  boolean wwan_match;
  boolean wcn_match;
} coex_simple_confl_s;

typedef struct
{
  uint32                      total_confls;
  coex_conflict_s            *confls;
  coex_wwan_victim_tbl        wwan[CXM_TECH_MAX];
  coex_wcn_victim_tbl         wcn[CXM_WCN_TECH_MAX][COEX_WCN_MODE_MAX];
  uint32                      num_groups;
  coex_conflict_group_s      *groups;
  coex_confl_active_cb        cb[COEX_CONFL_CB_MAX];
  coex_confl_total_cb         total_cb[COEX_CONFL_CB_MAX];
  coex_confl_wcn_band_itr_cb  wcn_iterator;
  coex_simple_confl_s         lteu;
} coex_conflicts_s;

/*=============================================================================

                      GLOBAL VARIABLES

=============================================================================*/
/* dynamically built tables at boot */
STATIC coex_conflicts_s coex_conflicts;

/*=============================================================================

                      INTERNAL FUNCTION DEFINITIONS

=============================================================================*/
/*=============================================================================

  FUNCTION:  coex_confl_init

=============================================================================*/
/*!
    @brief
    Construct the data structures used in finding (a) conflict(s) in 
    victim table-based coex operation.

    @return
    void
*/
/*===========================================================================*/
void coex_confl_init(
  coex_config_params_v10  *params,
  coex_conflict_type      *victim_tbl
)
{
  size_t                size;
  uint32                i, j;
  cxm_tech_type         wwan_tech;
  cxm_wcn_tech_type     wcn_tech;
  coex_wcn_mode_e       wcn_mode;
  coex_conflict_s      *confls;
  coex_conflict_group_s groups[COEX_GROUPS_MAX];
  /*-----------------------------------------------------------------------*/
  memset( &coex_conflicts, 0, sizeof(coex_conflicts) );
  memset( &groups[0], 0, sizeof(groups) );

  /* determine the size of the tech-specific victim tables */
  coex_conflicts.total_confls = params->num_conflicts;
  for( i = 0; i < params->num_conflicts; i++ )
  {
    wwan_tech = coex_tech_idl_to_cxm_transl( victim_tbl[i].wwan.tech );
    wcn_tech = coex_tech_wcn_idl_to_cxm_transl( victim_tbl[i].wcn.tech );
    if( CXM_WCN_TECH_DFLT_INVLD == wcn_tech || 
        CXM_TECH_DFLT_INVLD == wwan_tech )
    {
      /* ignore any unrecognized techs */
      continue;
    }
    else
    {
      if( (victim_tbl[i].wcn.mode & COEX_WLAN_CONN_MODE_V01) != 0 )
      {
        coex_conflicts.wcn[wcn_tech][COEX_WCN_MODE_CONN].num_confls++;
      }
      if( (victim_tbl[i].wcn.mode & COEX_WLAN_HIGH_PRIO_MODE_V01) != 0 )
      {
        coex_conflicts.wcn[wcn_tech][COEX_WCN_MODE_HP].num_confls++;
      }
      coex_conflicts.wwan[wwan_tech].num_confls++;
    }
  }

  /* allocate memory to build tech-specific victim tables */
  size = coex_conflicts.total_confls;
  if( size > 0 )
  {
    confls = CXM_MEM_CALLOC( size, sizeof(coex_conflict_s) );
    CXM_ASSERT( confls != NULL );
    coex_conflicts.confls = confls;
  }
  for( wwan_tech = CXM_TECH_LTE; wwan_tech < CXM_TECH_MAX; wwan_tech++ )
  {
    size = coex_conflicts.wwan[wwan_tech].num_confls;
    if( size > 0 )
    {
      coex_conflicts.wwan[wwan_tech].confls = 
        CXM_MEM_CALLOC( size, sizeof(coex_conflict_s*) );
      CXM_ASSERT( coex_conflicts.wwan[wwan_tech].confls != NULL );
    }
  }
  for( wcn_tech = CXM_TECH_WIFI; wcn_tech < CXM_WCN_TECH_MAX; wcn_tech++ )
  {
    for( wcn_mode = COEX_WCN_MODE_HP; wcn_mode < COEX_WCN_MODE_MAX; wcn_mode++ )
    {
      size = coex_conflicts.wcn[wcn_tech][wcn_mode].num_confls;
      if( size > 0 )
      {
        coex_conflicts.wcn[wcn_tech][wcn_mode].confls = 
          CXM_MEM_CALLOC( size, sizeof(coex_conflict_s*) );
        CXM_ASSERT( coex_conflicts.wcn[wcn_tech][wcn_mode].confls != NULL );
      }
    }
  }

  /* fill out the tables */
  for( i = 0; i < params->num_conflicts; i++ )
  {
    wwan_tech = coex_tech_idl_to_cxm_transl( victim_tbl[i].wwan.tech );
    wcn_tech = coex_tech_wcn_idl_to_cxm_transl( victim_tbl[i].wcn.tech );
    if( CXM_WCN_TECH_DFLT_INVLD == wcn_tech || CXM_TECH_DFLT_INVLD == wwan_tech )
    {
      /* skip any invalid/unrecognized tech conflict defs */
      continue;
    }
    else
    {
      confls[i].index = i;
      confls[i].xl_row_num = victim_tbl[i].xl_row_num;
      confls[i].wwan.freq_start = victim_tbl[i].wwan.freq_start;
      confls[i].wwan.freq_stop = victim_tbl[i].wwan.freq_stop;
      confls[i].wwan.op_dim = victim_tbl[i].wwan.operating_dim;
      confls[i].wwan.dir = victim_tbl[i].wwan.direction;
      confls[i].wcn.tech = wcn_tech;
      /* convert from kHz to MHz */
      confls[i].wcn.freq_start = victim_tbl[i].wcn.freq_start / 1000;
      confls[i].wcn.freq_stop = victim_tbl[i].wcn.freq_stop / 1000;

      /* place this conflict into the correct WWAN tech-specific
       * victim table (pointer) */
      j = 0;
      while( coex_conflicts.wwan[wwan_tech].confls[j] != NULL && 
             j < coex_conflicts.wwan[wwan_tech].num_confls )
      {
        j++;
      }
      CXM_ASSERT( j < coex_conflicts.wwan[wwan_tech].num_confls );
      coex_conflicts.wwan[wwan_tech].confls[j] = &confls[i];

      /* place this conflict into the correct WCN tech/mode-specific
       * victim table (pointer) */
      if( (victim_tbl[i].wcn.mode & COEX_WLAN_CONN_MODE_V01) != 0 )
      {
        j = 0;
        while( coex_conflicts.wcn[wcn_tech][COEX_WCN_MODE_CONN].confls[j] != NULL && 
               j < coex_conflicts.wcn[wcn_tech][COEX_WCN_MODE_CONN].num_confls )
        {
          j++;
        }
        CXM_ASSERT( j < coex_conflicts.wcn[wcn_tech][COEX_WCN_MODE_CONN].num_confls );
        coex_conflicts.wcn[wcn_tech][COEX_WCN_MODE_CONN].confls[j] = &confls[i];
      }
      if( (victim_tbl[i].wcn.mode & COEX_WLAN_HIGH_PRIO_MODE_V01) != 0 )
      {
        j = 0;
        while( coex_conflicts.wcn[wcn_tech][COEX_WCN_MODE_HP].confls[j] != NULL && 
               j < coex_conflicts.wcn[wcn_tech][COEX_WCN_MODE_HP].num_confls )
        {
          j++;
        }
        CXM_ASSERT( j < coex_conflicts.wcn[wcn_tech][COEX_WCN_MODE_HP].num_confls );
        coex_conflicts.wcn[wcn_tech][COEX_WCN_MODE_HP].confls[j] = &confls[i];
      }

      /* if this conflict is part of a group, save group info */
      if( victim_tbl[i].group != 0 )
      {
        for( j = 0; j < COEX_GROUPS_MAX; j++ )
        {
          if( groups[j].group == 0 )
          {
            /* found an empty group spot */
            coex_conflicts.num_groups++;
            groups[j].group = victim_tbl[i].group;
            break;
          }
          else if( groups[j].group == victim_tbl[i].group )
          {
            /* already encountered this group */
            break;
          }
        }

        /* save this conflict info in the group spot we found */
        CXM_ASSERT( COEX_GROUPS_MAX != j );
        CXM_ASSERT( groups[j].num_confls < COEX_GROUP_CONFLS_MAX );
        groups[j].confls[groups[j].num_confls] = &confls[i];
        groups[j].num_confls++;
      }
      else
      {
        /* not part of a group, so don't need group validation */
        coex_conflicts.confls[i].group_valid = TRUE;
        coex_conflicts.confls[i].wwan_group_valid = TRUE;
      }
    }
  }

  /* allocate and fill out group info */
  if( coex_conflicts.num_groups > 0 )
  {
    size = coex_conflicts.num_groups * sizeof(coex_conflict_group_s);
    coex_conflicts.groups = CXM_MEM_ALLOC( size );
    CXM_ASSERT( coex_conflicts.groups != NULL );
    memscpy( coex_conflicts.groups, size, &groups[0], sizeof(groups) );
  }
}

/*=============================================================================

  FUNCTION:  coex_confl_deinit

=============================================================================*/
/*!
    @brief
    Free any memory used by coex conflict processing

    @return
    void
*/
/*===========================================================================*/
void coex_confl_deinit( void )
{
  uint32 i, j;
  /*-----------------------------------------------------------------------*/
  if( coex_conflicts.groups != NULL )
  {
    CXM_MEM_FREE( coex_conflicts.groups );
  }
  if( coex_conflicts.confls != NULL )
  {
    CXM_MEM_FREE( coex_conflicts.confls );
  }
  for( i = 0; i < CXM_TECH_MAX; i++ )
  {
    if( coex_conflicts.wwan[i].confls != NULL )
    {
      CXM_MEM_FREE( coex_conflicts.wwan[i].confls );
    }
  }
  for( i = 0; i < CXM_WCN_TECH_MAX; i++ )
  {
    for( j = 0; j < COEX_WCN_MODE_MAX; j++ )
    {
      if( coex_conflicts.wcn[i][j].confls != NULL )
      {
        CXM_MEM_FREE( coex_conflicts.wcn[i][j].confls );
      }
    }
  }

  memset( &coex_conflicts, 0, sizeof(coex_conflicts) );

  return;
}

/*=============================================================================

  FUNCTION:  coex_confl_set_cb

=============================================================================*/
/*!
    @brief
    Set a callback that the conflict processing algorithm will call with
    the result. Multiple callbacks are allowed.

    @return
    void
*/
/*===========================================================================*/
void coex_confl_set_cb(
  coex_confl_active_cb tech_cb,
  coex_confl_total_cb  total_cb
)
{
  uint32 i;
  /*-----------------------------------------------------------------------*/
  for( i = 0; i < COEX_CONFL_CB_MAX; i++ )
  {
    if( coex_conflicts.cb[i] == NULL )
    {
      coex_conflicts.cb[i] = tech_cb;
      break;
    }
  }
  CXM_ASSERT( i != COEX_CONFL_CB_MAX );
  for( i = 0; i < COEX_CONFL_CB_MAX; i++ )
  {
    if( coex_conflicts.total_cb[i] == NULL )
    {
      coex_conflicts.total_cb[i] = total_cb;
      break;
    }
  }
  CXM_ASSERT( i != COEX_CONFL_CB_MAX );
}

/*=============================================================================

  FUNCTION:  coex_confl_set_wcn_band_iterator

=============================================================================*/
/*!
    @brief
    Callback that the conflict processing algorithm will call to iterate
    over all the WCN bands, passing each WCN band in turn for processing.
    This enables us to support multiple WCN band formats and separate the data

    @return
    void
*/
/*===========================================================================*/
void coex_confl_set_wcn_band_iterator(
  coex_confl_wcn_band_itr_cb  wcn_iterator
)
{
  coex_conflicts.wcn_iterator = wcn_iterator;
  return;
}

/*=============================================================================

  FUNCTION:  coex_confl_lteu_process

=============================================================================*/
/*!
    @brief
    Check if LTE-U conflicts with WLAN 5GHz

    @return
    boolean -- TRUE if LTE-U conflict is active
*/
/*===========================================================================*/
inline boolean coex_confl_lteu_process( void )
{
  boolean match = FALSE;
  /*-----------------------------------------------------------------------*/
  if( coex_conflicts.lteu.wwan_match && coex_conflicts.lteu.wcn_match )
  {
    match = TRUE;
  }

  return match;
}

/*=============================================================================

  FUNCTION:  coex_confl_validate_groups

=============================================================================*/
/*!
    @brief
    Validate conflict groups. Groups are only active if all conflicts
    within the group are active.

    @return
    void
*/
/*===========================================================================*/
void coex_confl_validate_groups( void )
{
  uint32           i, j;
  boolean          active;
  coex_conflict_s *confl;
  /*-----------------------------------------------------------------------*/
  for( i = 0; i < coex_conflicts.num_groups; i++ )
  {
    for( j = 0; j < coex_conflicts.groups[i].num_confls; j++ )
    {
      /* check if conflict is active (any combination of CC/modes) */
      /* NOTE:already validated WAN groups in coex_confl_validate_wwan_groups */
      confl = coex_conflicts.groups[i].confls[j];
      active = (( confl->wwan_group_valid ) &&
                ((confl->wcn.match[COEX_WCN_MODE_HP]   != COEX_MATCH_NONE) ||
                 (confl->wcn.match[COEX_WCN_MODE_CONN] != COEX_MATCH_NONE)) );

      if( active )
      {
        confl->group_valid = TRUE;
      }
      else
      {
        /* validation failed, mark all conflicts in the group as invalid */
        for( j = 0; j < coex_conflicts.groups[i].num_confls; j++ )
        {
          coex_conflicts.groups[i].confls[j]->group_valid = FALSE;
        }
        break;
      }
    }

    if( active )
    {
      CXM_MSG_1( MED, "Confl Group[%d] valid!", coex_conflicts.groups[i].group );
    }
  }
}

/*=============================================================================

  FUNCTION:  coex_confl_validate_wwan_groups

=============================================================================*/
/*!
    @brief
    Validate conflict groups for just WWAN. WWAN Group is active if there is overlap
    all WWAN ranges. This determines what groups qualify for channel avoidance reporting.

    @return
    void
*/
/*===========================================================================*/
void coex_confl_validate_wwan_groups( void )
{
  uint32           i, j;
  boolean          wwan_active;
  coex_conflict_s *confl;
  /*-----------------------------------------------------------------------*/
  for( i = 0; i < coex_conflicts.num_groups; i++ )
  {
    for( j = 0; j < coex_conflicts.groups[i].num_confls; j++ )
    {
      /* check if conflict is active (any combination of CC/modes) */
      confl = coex_conflicts.groups[i].confls[j];
      wwan_active = ( (confl->wwan.prio[CXM_CARRIER_PCC]   != 0) ||
                      (confl->wwan.prio[CXM_CARRIER_SCC_0] != 0) ||
                      (confl->wwan.prio[CXM_CARRIER_SCC_1] != 0) );

      /* update the wwan active state */
      if( wwan_active )
      {
        confl->wwan_group_valid = TRUE;
      }
      else
      {
        /* validation failed, mark all conflicts in the group as invalid */
        for( j = 0; j < coex_conflicts.groups[i].num_confls; j++ )
        {
          coex_conflicts.groups[i].confls[j]->wwan_group_valid = FALSE;
        }
        break;
      }
    }

    if( wwan_active )
    {
      CXM_MSG_1( MED, "All WWAN ranges in Group[%d] overlap", coex_conflicts.groups[i].group );
    }
  }
}


/*=============================================================================

  FUNCTION:  coex_confl_choose

=============================================================================*/
/*!
    @brief
    Given a WWAN tech and WCN tech, determines which conflict best fits
    and returns that victim table index

    @detail
    For the given WWAN band and WCN band, the victim table is checked to
    see if they are in conflict. If a conflict is found, the appropriate
    action is carried out. There is a conflict if there is an intersection
    between both the WWAN and WCN bands with a pair in the victim table.

    Several conflicts may match the WWAN and WCN bands simultaneously, but
    only one can be chosen at a time per tech/carrier.

    @return
    new active victim table index
*/
/*===========================================================================*/
inline int16 coex_confl_choose(
  cxm_tech_type     wwan_tech,
  cxm_carrier_e     wwan_cc,
  cxm_wcn_tech_type wcn_tech, 
  coex_wcn_mode_e   wcn_mode,
  uint16           *best_row_num
)
{
  int32                 best_index = COEX_CONFL_INVALID_INDEX;
  uint32                best_prio = 0;
  coex_conflict_s      *confl;
  /*--------------------------------------------------------------------------*/
  CXM_ASSERT( best_row_num != NULL );
  *best_row_num = 0;

  /* don't have to check all conflicts, only active ones of these techs */
  confl = coex_conflicts.wwan[wwan_tech].active_head[wwan_cc];
  while( confl != NULL )
  {
    if( confl->wwan.prio[wwan_cc]  != 0               &&
        confl->wcn.match[wcn_mode] != COEX_MATCH_NONE &&
        confl->group_valid                            && 
        confl->wcn.tech == wcn_tech )
    {
      /* This conflict is active!
       * Check if it's better than any previously-found conflicts */
      if( confl->wwan.prio[wwan_cc] > best_prio )
      {
        best_prio  = confl->wwan.prio[wwan_cc];
        best_index = confl->index;
        *best_row_num = confl->xl_row_num;
      }
    }

    /* go to next active conflict in linked list */
    confl = confl->wwan.next_active[wwan_cc];
  }

  /* store the best index to be aggregated with other techs/CCs later */
  coex_conflicts.wwan[wwan_tech].best[wwan_cc] = best_index;
  coex_conflicts.wwan[wwan_tech].best_row_num[wwan_cc] = *best_row_num;
  return best_index;
}

/*=============================================================================

  FUNCTION:  coex_confl_update_tot_confl

=============================================================================*/
/*!
    @brief
    Collects a complete list of all the acive indices for all techs. Needed for
    DSDS/DSDA situations for SMEM since WLAN is expecting to see the collection
    of conflicts for all techs.

    @return
    None
*/
/*===========================================================================*/
void coex_confl_update_tot_confl(
  boolean wan_updated,
  boolean wcn_updated
)
{
  uint8                   tech, cc, i;
  coex_confl_total_retval retval;
  cxm_tot_cofl_s          *tot_confl = &retval.tot_confls;
  coex_conflict_s         *confl;
  /*--------------------------------------------------------------------------*/
  memset( tot_confl, 0, sizeof( cxm_tot_cofl_s ) );
  retval.wan_updated = wan_updated;
  retval.wcn_updated = wcn_updated;
  for( tech = 0; tech < CXM_TECH_MAX; tech++ )
  {
    for( cc = 0; cc < CXM_CARRIER_MAX; cc++ )
    {
      /*check for the best conflict*/
      if( coex_conflicts.wwan[tech].active_head[cc] != NULL && 
          coex_conflicts.wwan[tech].best[cc] != COEX_CONFL_INVALID_INDEX )
      {
        tot_confl->best_indx[tot_confl->num_best].index 
          = coex_conflicts.wwan[tech].best[cc];
        tot_confl->best_indx[tot_confl->num_best].xl_row_num
          = coex_conflicts.wwan[tech].best_row_num[cc];
        tot_confl->best_indx[tot_confl->num_best].carrier = cc;
        tot_confl->best_indx[tot_confl->num_best].tech = tech;
        tot_confl->num_best++;
      }

      /* check for all overlapping WAN conflicts -- only changes with WAN */
      confl = coex_conflicts.wwan[tech].active_head[cc];
      while( confl != NULL )
      {
        if( confl->wwan.prio[cc] && confl->wwan_group_valid )
        {
          /* add to list of overlap conflicts for this tech. Possible future update: make
             this list arranged per cc if required by WLAN*/
          tot_confl->ovlp_indx[tot_confl->num_ovlp].index = confl->index;
          tot_confl->ovlp_indx[tot_confl->num_ovlp].xl_row_num = confl->xl_row_num;
          tot_confl->ovlp_indx[tot_confl->num_ovlp].tech = tech;
          tot_confl->ovlp_indx[tot_confl->num_ovlp].carrier = cc;
          tot_confl->ovlp_indx[tot_confl->num_ovlp].match = 
            COEX_GET_MATCH_TYPE_FROM_PRIO( confl->wwan.prio[cc] );
          tot_confl->num_ovlp++;
        }

        /* go to next active conflict in linked list */
        confl = confl->wwan.next_active[cc];
      }
    }
  }

  /* notify registered callbacks of aggregated conflict info */
  for( i = 0; i < COEX_CONFL_CB_MAX; i++ )
  {
    if( coex_conflicts.total_cb[i] != NULL )
    {
      coex_conflicts.total_cb[i]( &retval );
    }
  }
}

/*=============================================================================

  FUNCTION:  coex_confl_process

=============================================================================*/
/*!
    @brief
    Examine all updated data and decide if there is a new conflict or not,
    and assign one conflict, if applicable, per wwan tech carrier

    @return
    void
*/
/*===========================================================================*/
void coex_confl_process( 
  cxm_tech_type      wwan_tech,
  cxm_wcn_tech_type  wcn_tech
)
{
  int16             index;
  uint16            xl_row_num;
  uint32            i;
  cxm_carrier_e     cc;
  coex_confl_retval retval;
  /*-----------------------------------------------------------------------*/
  CXM_MSG_2( MED, "Checking conflicts: wwan_tech=%d wcn_tech=%d", 
             wwan_tech, wcn_tech );

  for( cc = CXM_CARRIER_PCC; cc < CXM_CARRIER_MAX; cc++ )
  {
    /* choose the active conflict */
    index = coex_confl_choose( wwan_tech, cc, wcn_tech, COEX_WCN_MODE_HP, &xl_row_num );
    /* only consider connection conflicts if no High Priority ones found */
    if( index == COEX_CONFL_INVALID_INDEX )
    {
      index = coex_confl_choose( wwan_tech, cc, wcn_tech, COEX_WCN_MODE_CONN, &xl_row_num );
    }
    retval.active_index[cc] = index;
    retval.active_row_num[cc] = xl_row_num;
  }

  /* fill out tech-specific conflict checks */
  if( wwan_tech == CXM_TECH_LTE )
  {
    /* Check LTE-U conflict */
    retval.tech_spec.lte.lteu_active = coex_confl_lteu_process();
  }

  /* pass new indeces to coex algos */
  retval.wwan_tech = wwan_tech;
  retval.wcn_tech = wcn_tech;
  for( i = 0; i < COEX_CONFL_CB_MAX; i++ )
  {
    if( coex_conflicts.cb[i] != NULL )
    {
      coex_conflicts.cb[i]( &retval );
    }
  }

  return;
}

/*=============================================================================

  FUNCTION:  coex_confl_update_wwan_tech

=============================================================================*/
/*!
    @brief
    Update the list of possible conflicts that apply to the given tech.
    This will in turn call coex_find_conflict to pick the best conflict,
    if any, out of the list of active conflicts in each tech

    @detail
    For the given WWAN band and WCN band, the victim table is checked to
    see if they are in conflict. If a conflict is found, the appropriate
    action is carried out. There is a conflict if there is an intersection
    between both the WWAN and WCN bands with a pair in the victim table

    @return
    none
*/
/*===========================================================================*/
void coex_confl_update_wwan_tech(
  cxm_tech_type      tech,
  cxm_tech_data_s   *band_info
)
{
  int64                  overlap;
  uint32                 i, j;
  uint32                 half_bw, op_dim, prio;
  uint32                 active_start, active_stop; /* KHz */
  boolean                ul;
  cxm_carrier_e          cc;
  cxm_tech_link_info_s  *band;
  coex_conflict_match_e  match;
  coex_conflict_s       *confl;
  /*--------------------------------------------------------------------------*/
  CXM_MSG_1( HIGH, "Updating wwan [tech=%d] conflicts", tech );
  /* initialize linked list of active wwan conflict pointers */
  for( cc = CXM_CARRIER_PCC; cc < CXM_CARRIER_MAX; cc++ )
  {
    coex_conflicts.wwan[tech].active_head[cc] = NULL;
    coex_conflicts.wwan[tech].best[cc] = COEX_CONFL_INVALID_INDEX;
    coex_conflicts.wwan[tech].best_row_num[cc] = 0;
  }

  /* initialize priorities for victim table (clear matches) */
  for( i = 0; i < coex_conflicts.wwan[tech].num_confls; i++ )
  {
    for( cc = CXM_CARRIER_PCC; cc < CXM_CARRIER_MAX; cc++ )
    {
      coex_conflicts.wwan[tech].confls[i]->wwan.prio[cc] = 0;
    }
  }

  /* loop through all of the tech's active bands */
  for( i = 0; i < band_info->num_link_info_sets; i++ )
  {
    band = &band_info->link_list[i];
    cc = COEX_GET_LINK_CC( band->type );
    if( tech != CXM_TECH_LTE )
    {
      CXM_ASSERT( cc == CXM_CARRIER_PCC );
    }
    else
    {
      CXM_ASSERT( cc < CXM_CARRIER_MAX );
    }

    CXM_MSG_5( LOW, "Updating conflicts for wwan tech %d, cc=%d CF=%u BW=%u FrType=%d", 
               tech, cc, band->frequency, band->bandwidth, band->frame_type );

    op_dim = (band->frame_type == CXM_FRAME_TYPE_TDD) ? 
      COEX_TECH_OPERATING_DIMENSION_TDD_V01 : COEX_TECH_OPERATING_DIMENSION_FDD_V01;

    /* evaluate the current active tech start/stop freq ranges */
    /* TODO: do we still need this? */
    if ( COEX_SYS_ENABLED(CXM_SYS_BHVR_CNTR_FRQ_CONFLICT) )
    {
      /* if a conflict is defined by only the center frequency of the current wwan 
       * and wcn, set start = stop = center_frequency. This preserves the range based 
       * algorithm although there is a loss in efficiency since the conflict check 
       * could be simpler */
      active_start = active_stop = band->frequency;
    }
    else
    {
      /* get the ranges from the wwan (in KHz) and wlan (in MHz) state infos */
      half_bw      = band->bandwidth / (2 * KHZ_HZ_CONVERSION);
      active_start = band->frequency - half_bw;
      active_stop   = band->frequency + half_bw;
    }

    /* loop through victim table group entries for each tech and indicate 
     * whether match or not. Match means CF or PARTIAL_OVERLAP */
    for( j = 0; j < coex_conflicts.wwan[tech].num_confls; j++ )
    {
      /* for each conflict in group, check if it matches the active tech freqs. 
         op_dim check is only needed for LTE. */
      confl = coex_conflicts.wwan[tech].confls[j];
      if( (band->direction & confl->wwan.dir)     != 0 && 
          ( (op_dim          & confl->wwan.op_dim)  != 0 || tech != CXM_TECH_LTE ) )
      {
        /* okay so far, now check freq ranges */
        overlap = (int64)COEX_MIN( confl->wwan.freq_stop,  active_stop ) - 
                  (int64)COEX_MAX( confl->wwan.freq_start, active_start );

        /* have a match, check if is also center freq match */
        if( overlap > 0 )
        {
          if( confl->wwan.freq_start <= band->frequency &&
              band->frequency        <= confl->wwan.freq_stop )
          {
            match = COEX_MATCH_CF;
          }
          else
          {
            match = COEX_MATCH_PART_OVERLAP;
          }

          /* assign each one a priority based on the 4 criteria */
          ul = ( (confl->wwan.dir & CXM_LNK_DRCTN_UL) != 0 );
          prio = COEX_WWAN_CONFL_PRIO( ul, match, overlap );
          CXM_MSG_7( MED, "WWAN potential: confl[%d](%u,%u) active(%u,%u) prio=0x%x xl_row_num=%d",
                     confl->index, confl->wwan.freq_start, confl->wwan.freq_stop, 
                     active_start, active_stop, prio, confl->xl_row_num );

          /* add this conflict to the linked list of active conflicts */
          if( confl->wwan.prio[cc] == 0 )
          {
            confl->wwan.next_active[cc] = coex_conflicts.wwan[tech].active_head[cc];
            coex_conflicts.wwan[tech].active_head[cc] = confl;
          }
          if( prio > confl->wwan.prio[cc] )
          {
            confl->wwan.prio[cc] = prio;
          }
        }
      }
    } /* loop over wwan tech conflicts */
  } /* loop over active wwan tech bands */

  /* 'hardcoded' victim table entry: 
   * if WLAN is in 5GHz range & LTE is active, LTE needs to avoid LTE-U. */
  if( tech == CXM_TECH_LTE )
  {
    coex_conflicts.lteu.wwan_match = 
      (band_info->num_link_info_sets > 0) ? TRUE : FALSE;
  }

  /* now that info is updated, assign active conflicts and notify */
  coex_confl_validate_wwan_groups();
  coex_confl_validate_groups();
  coex_confl_process( tech, CXM_TECH_WIFI );
  coex_confl_update_tot_confl( TRUE, FALSE );

  return;
}

/*=============================================================================

  FUNCTION:  coex_confl_update_wcn_mode

=============================================================================*/
/*!
    @brief
    Update the list of possible conflicts that apply to the given tech.
    This will in turn call coex_find_conflict to pick the best conflict,
    if any, out of the list of active conflicts in each tech

    @detail
    For the given WWAN band and WCN band, the victim table is checked to
    see if they are in conflict. If a conflict is found, the appropriate
    action is carried out. There is a conflict if there is an intersection
    between both the WWAN and WCN bands with a pair in the victim table

    @return
    none
*/
/*===========================================================================*/
void coex_confl_update_wcn_mode(
  cxm_wcn_tech_type   tech,
  coex_wcn_mode_e     mode,
  coex_band_type_v01 *band
)
{
  uint32           i;
  int32            overlap;
  uint32           half_bw;
  uint32           active_start, active_stop;   /* MHz */
  coex_conflict_s *confl;
  /*--------------------------------------------------------------------------*/
  /* for each tech victim table entry, loop through the active conflicts */
  CXM_MSG_4( MED, "Updating conflicts for wcn tech %d, mode %d, CF=%u BW=%u", 
             tech, mode, band->freq, band->bandwidth );

  /* evaluate the current active tech start/stop freq ranges */
  /* get the ranges from wlan (in MHz) state info */
  half_bw = band->bandwidth / 2;
  active_start = band->freq - half_bw;
  active_stop  = band->freq + half_bw;

  /* loop through victim table group entries for each tech and indicate 
   * whether match or not. */
  for( i = 0; i < coex_conflicts.wcn[tech][mode].num_confls; i++ )
  {
    confl = coex_conflicts.wcn[tech][mode].confls[i];

    /* okay so far, now check freq ranges */
    overlap = (int32)COEX_MIN( confl->wcn.freq_stop, active_stop ) - 
              (int32)COEX_MAX( confl->wcn.freq_start, active_start );

    /* WLAN conflict only considered a match if the active operating
     * frequency COMPLETELY overlaps with the conflict freq range */
    confl->wcn.match[mode] = (overlap >= (int32)band->bandwidth) ? 
      COEX_MATCH_COMPLETE_OVERLAP : COEX_MATCH_NONE;
    if( confl->wcn.match[mode] )
    {
      CXM_MSG_4( MED, "WCN potential: confl[%d]: start=%d, stop=%d, xl_row_num=%d", 
                 confl->index, confl->wcn.freq_start, confl->wcn.freq_stop, 
                 confl->xl_row_num );
    }
  }

  /* 'hardcoded' victim table entry: 
   * if WLAN is in 5GHz range, LTE needs to avoid LTE-U. */
  if( tech == CXM_TECH_WIFI && mode == COEX_WCN_MODE_CONN &&
      band->freq >= CXM_5000MHZ )
  {
    coex_conflicts.lteu.wcn_match = TRUE;
  }
}

/*=============================================================================

  FUNCTION:  coex_confl_update_wcn_tech

=============================================================================*/
/*!
    @brief
    Update the list of possible conflicts that apply to the given tech.
    This will in turn call coex_find_conflict to pick the best conflict,
    if any, out of the list of active conflicts in each tech

    @detail
    For the given WWAN band and WCN band, the victim table is checked to
    see if they are in conflict. If a conflict is found, the appropriate
    action is carried out. There is a conflict if there is an intersection
    between both the WWAN and WCN bands with a pair in the victim table

    @return
    none
*/
/*===========================================================================*/
void coex_confl_update_wcn_tech(
  cxm_wcn_tech_type tech,
  uint32            mode_mask
)
{
  coex_wcn_mode_e mode;
  uint32          i;
  /*--------------------------------------------------------------------------*/
  CXM_MSG_2( HIGH, "Updating wcn [tech=%d modes=%d] conflicts", tech, mode_mask );
  CXM_ASSERT( coex_conflicts.wcn_iterator != NULL );
  if( tech == CXM_TECH_WIFI )
  {
    /* search through active wifi high priority bands */
    if( (CXM_WLAN_HIGH_PRIO_TYPE & mode_mask) != 0 )
    {
      mode = COEX_WCN_MODE_HP;
      /* initialize: clear all conflicts */
      for( i = 0; i < coex_conflicts.wcn[tech][mode].num_confls; i++ )
      {
        coex_conflicts.wcn[tech][mode].confls[i]->wcn.match[mode] = COEX_MATCH_NONE;
      }

      /* call wifi iterator, which will in turn call to update the victim table
       * for each wifi band */
      coex_conflicts.wcn_iterator( tech, mode, coex_confl_update_wcn_mode );
    } /* HIGH PRIO */

    /* search through active wifi connection bands */
    if( (CXM_WLAN_CONN_TYPE & mode_mask) != 0 )
    {
      mode = COEX_WCN_MODE_CONN;
      /* initialize LTE-U conflict search */
      coex_conflicts.lteu.wcn_match = FALSE;
      /* initialize (clear) all victim table-based conflicts */
      for( i = 0; i < coex_conflicts.wcn[tech][mode].num_confls; i++ )
      {
        coex_conflicts.wcn[tech][mode].confls[i]->wcn.match[mode] = COEX_MATCH_NONE;
      }

      /* call wifi iterator, which will in turn call to update the victim table
       * for each wifi band */
      coex_conflicts.wcn_iterator( tech, mode, coex_confl_update_wcn_mode );
    } /* CONNECTION */
  } /* WIFI */
  else
  {
    CXM_MSG_1( ERROR, "Unsupported WCN conflict tech: %d", tech );
    return;
  }

  /* now that info is updated, assign active conflicts */
  coex_confl_validate_groups();
  coex_confl_process( CXM_TECH_LTE,     tech );
  coex_confl_process( CXM_TECH_TDSCDMA, tech );
  coex_confl_process( CXM_TECH_GSM1,    tech );
  coex_confl_process( CXM_TECH_GSM2,    tech );
  coex_confl_process( CXM_TECH_GSM3,    tech );
  coex_confl_update_tot_confl( FALSE, TRUE );

  return;
}

