/*!
  @file
  hwio_cap.c

  @brief

*/

/*===========================================================================

  Copyright (c) 2016 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================
$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/mcs/hwio/src/hwio_cap.c#1 $
===========================================================================*/

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/
#include "mcs_variation.h"
#include <customer.h>
#include "msmhwioreg.h"
#include <DDIChipInfo.h>
#include "hwio_cap.h"
#include <msg_diag_service.h>

#ifdef TEST_FRAMEWORK
  #error code not present
#else
  #if defined(FEATURE_MCS_ATLAS) || defined (FEATURE_MCS_THOR)
    #define MCSHWIO_READ_CAP() HWIO_IN(MSS_SW_FEATURE_FUSES)
  #else
    #if defined(FEATURE_MCS_JOLOKIA) || defined(FEATURE_MCS_TABASCO)
      #define MCSHWIO_READ_CAP() HWIO_IN(MODEM_FEATURE_EN)
    #else
      #define MCSHWIO_READ_CAP() HWIO_IN(MODEM_OPTION_EN)
    #endif
  #endif
#endif /* TEST_FRAMEWORK */

 
  
/*===========================================================================

                   INTERNAL DEFINITIONS AND TYPES

===========================================================================*/

/*----------------------------------------------------------------------------
  Define special values to denote capability supported and unsupported. These 
  allow us to define capability (as needed), when either the corresponding bit
  is missing in hardware or the respective feature is not applicable for a 
  particular modem.
----------------------------------------------------------------------------*/
#define CAP_SUPPORTED    0xFE
#define CAP_UNSUPPORTED  0xFF

typedef enum
{
  TESLA_1X = 0,
  TESLA_DO = 2,
  TESLA_WCDMA = 4,
  TESLA_HSDPA = 6,
  TESLA_HSDPA_MIMO = 8,
  TESLA_HSDPA_DC = 10,
  TESLA_LTE = 12,
  TESLA_LTE_ABOVE_CAT2 = 14,
  TESLA_LTE_ABOVE_CAT1 = 16,
  TESLA_TDSCDMA = 18,
  TESLA_GERAN = 20,
  TESLA_MODEM = 27,
  TESLA_SUPPORTED = CAP_SUPPORTED,
  TESLA_UNSUPPORTED = CAP_UNSUPPORTED
}modem_cap_tesla;

typedef enum
{
  CHEEL_GERAN = 0,
  CHEEL_1X = 1,
  CHEEL_DO = 2,
  CHEEL_WCDMA = 3,
  CHEEL_HSDPA = 4,
  CHEEL_LTE = 5,
  CHEEL_MODEM = 6,
  CHEEL_TDSCDMA = 7,
  CHEEL_HSDPA_MIMO = 9,
  CHEEL_HSDPA_DC = 10,
  CHEEL_LTE_ABOVE_CAT1 = 11,
  CHEEL_LTE_ABOVE_CAT2 = 12,
  CHEEL_DSDA = 13,
  CHEEL_SUPPORTED = CAP_SUPPORTED,
  CHEEL_UNSUPPORTED = CAP_UNSUPPORTED
}modem_cap_cheel;

typedef enum
{
  SAHI_1X = 0,
  SAHI_DO = 1,
  SAHI_WCDMA = 2,
  SAHI_HSDPA = 3,
  SAHI_LTE = 4,
  SAHI_MODEM = 5,
  SAHI_TDSCDMA = 6,
  SAHI_NAV = 7,
  SAHI_HSUPA_DC = 8,
  SAHI_LTE_ABOVE_CAT1 = 9,
  SAHI_LTE_ABOVE_CAT4 = 10,
  SAHI_DSDA = 11,
  SAHI_LTE_UL_CA = 12,
  SAHI_LTE_ABOVE_CAT4_SW = 13,
  SAHI_SUPPORTED = CAP_SUPPORTED,
  SAHI_GERAN = SAHI_SUPPORTED, /* NOTE: GERAN is supported by default as MODEM_FEATURE_EN doesn't have a bit for GERAN */
  SAHI_UNSUPPORTED = CAP_UNSUPPORTED
}modem_cap_sahi;

/*===========================================================================

                         INTERNAL VARIABLES

===========================================================================*/
modem_cap_tesla tesla_bitpositions[MCS_MODEM_NUM_CAPABILITY]=
{
  /* MCS_MODEM_CAPABILITY_FEATURE_GSM */                  TESLA_GERAN,
  /* MCS_MODEM_CAPABILITY_FEATURE_1X */                   TESLA_1X,
  /* MCS_MODEM_CAPABILITY_FEATURE_DO */                   TESLA_DO,
  /* MCS_MODEM_CAPABILITY_FEATURE_WCDMA*/                 TESLA_WCDMA,
  /* MCS_MODEM_CAPABILITY_FEATURE_HSPA */                 TESLA_HSDPA,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE */                  TESLA_LTE,
  /* MCS_MODEM_CAPABILITY_FEATURE_TDSCDMA */              TESLA_TDSCDMA,
  /* MCS_MODEM_CAPABILITY_FEATURE_MODEM */                TESLA_MODEM,
  /* MCS_MODEM_CAPABILITY_FEATURE_HSPA_ABOVE_CAT14 */     TESLA_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_NAV */                  TESLA_SUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_HSPA_MIMO */            TESLA_HSDPA_MIMO,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE_ABOVE_CAT1 */       TESLA_LTE_ABOVE_CAT1,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE_ABOVE_CAT2 */       TESLA_LTE_ABOVE_CAT2,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE_BBRX2 */            TESLA_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE_BBRX3 */            TESLA_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_UMTS_DL_ABOVE_CAT8 */   TESLA_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_UMTS_DL_ABOVE_CAT10 */  TESLA_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_MCDO */                 TESLA_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_UMTS_UL_ABOVE_CAT6 */   TESLA_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_RXD */                  TESLA_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_DSDA */                 TESLA_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_HSDPA_DC */             TESLA_HSDPA_DC,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE_40MHZ */            TESLA_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE_60MHZ */            TESLA_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE_UL_CA */            TESLA_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_ADC2 */                 TESLA_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_ADC3 */                 TESLA_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_HSUPA_DC */             TESLA_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE_ABOVE_CAT4 */       TESLA_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE_ABOVE_CAT4_SW */    TESLA_UNSUPPORTED
};

modem_cap_cheel cheel_bitpositions[MCS_MODEM_NUM_CAPABILITY]=
{
  /* MCS_MODEM_CAPABILITY_FEATURE_GSM */                  CHEEL_GERAN,
  /* MCS_MODEM_CAPABILITY_FEATURE_1X */                   CHEEL_1X,
  /* MCS_MODEM_CAPABILITY_FEATURE_DO */                   CHEEL_DO,
  /* MCS_MODEM_CAPABILITY_FEATURE_WCDMA*/                 CHEEL_WCDMA,
  /* MCS_MODEM_CAPABILITY_FEATURE_HSPA */                 CHEEL_HSDPA,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE */                  CHEEL_LTE,
  /* MCS_MODEM_CAPABILITY_FEATURE_TDSCDMA */              CHEEL_TDSCDMA,
  /* MCS_MODEM_CAPABILITY_FEATURE_MODEM */                CHEEL_MODEM,
  /* MCS_MODEM_CAPABILITY_FEATURE_HSPA_ABOVE_CAT14 */     CHEEL_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_NAV */                  CHEEL_SUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_HSPA_MIMO */            CHEEL_HSDPA_MIMO,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE_ABOVE_CAT1 */       CHEEL_LTE_ABOVE_CAT1,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE_ABOVE_CAT2 */       CHEEL_LTE_ABOVE_CAT2,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE_BBRX2 */            CHEEL_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE_BBRX3 */            CHEEL_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_UMTS_DL_ABOVE_CAT8 */   CHEEL_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_UMTS_DL_ABOVE_CAT10 */  CHEEL_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_MCDO */                 CHEEL_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_UMTS_UL_ABOVE_CAT6 */   CHEEL_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_RXD */                  CHEEL_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_DSDA */                 CHEEL_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_HSDPA_DC */             CHEEL_HSDPA_DC,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE_40MHZ */            CHEEL_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE_60MHZ */            CHEEL_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE_UL_CA */            CHEEL_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_ADC2 */                 CHEEL_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_ADC3 */                 CHEEL_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_HSUPA_DC */             CHEEL_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE_ABOVE_CAT4 */       CHEEL_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE_ABOVE_CAT4_SW */    CHEEL_UNSUPPORTED
};

modem_cap_sahi sahi_bitpositions[MCS_MODEM_NUM_CAPABILITY]=
{
  /* MCS_MODEM_CAPABILITY_FEATURE_GSM */                  SAHI_GERAN,
  /* MCS_MODEM_CAPABILITY_FEATURE_1X */                   SAHI_1X,
  /* MCS_MODEM_CAPABILITY_FEATURE_DO */                   SAHI_DO,
  /* MCS_MODEM_CAPABILITY_FEATURE_WCDMA*/                 SAHI_WCDMA,
  /* MCS_MODEM_CAPABILITY_FEATURE_HSPA */                 SAHI_HSDPA,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE */                  SAHI_LTE,
  /* MCS_MODEM_CAPABILITY_FEATURE_TDSCDMA */              SAHI_TDSCDMA,
  /* MCS_MODEM_CAPABILITY_FEATURE_MODEM */                SAHI_MODEM,
  /* MCS_MODEM_CAPABILITY_FEATURE_HSPA_ABOVE_CAT14 */     SAHI_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_NAV */                  SAHI_NAV,
  /* MCS_MODEM_CAPABILITY_FEATURE_HSPA_MIMO */            SAHI_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE_ABOVE_CAT1 */       SAHI_LTE_ABOVE_CAT1,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE_ABOVE_CAT2 */       SAHI_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE_BBRX2 */            SAHI_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE_BBRX3 */            SAHI_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_UMTS_DL_ABOVE_CAT8 */   SAHI_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_UMTS_DL_ABOVE_CAT10 */  SAHI_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_MCDO */                 SAHI_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_UMTS_UL_ABOVE_CAT6 */   SAHI_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_RXD */                  SAHI_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_DSDA */                 SAHI_DSDA,
  /* MCS_MODEM_CAPABILITY_FEATURE_HSDPA_DC */             SAHI_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE_40MHZ */            SAHI_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE_60MHZ */            SAHI_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE_UL_CA */            SAHI_LTE_UL_CA,
  /* MCS_MODEM_CAPABILITY_FEATURE_ADC2 */                 SAHI_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_ADC3 */                 SAHI_UNSUPPORTED,
  /* MCS_MODEM_CAPABILITY_FEATURE_HSUPA_DC */             SAHI_HSUPA_DC,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE_ABOVE_CAT4 */       SAHI_LTE_ABOVE_CAT4,
  /* MCS_MODEM_CAPABILITY_FEATURE_LTE_ABOVE_CAT4_SW */    SAHI_LTE_ABOVE_CAT4_SW
};

typedef struct
{
  uint32 data;
  DalChipInfoFamilyType chipType;
}modem_capability_type;

/*! Global state for modem capability */
static modem_capability_type modem_cap;

/*===========================================================================

                    INTERNAL FUNCTION PROTOTYPES

===========================================================================*/


/*===========================================================================

                                FUNCTIONS

===========================================================================*/
/*============================================================================

FUNCTION HWIO_CAP_INIT

DESCRIPTION
  Reads the modem capability information and caches it.
  
DEPENDENCIES
  Must be called by RC INIT before any other functions in this
  file are called.

RETURN VALUE
  mcs_modem_cap_return_enum

SIDE EFFECTS
  None

============================================================================*/
void hwio_cap_init(void)
{

  /* Get chip type */
  modem_cap.chipType = DalChipInfo_ChipFamily();

  #ifdef FEATURE_GNSS_SA
  /* Not applicable for GPS-only builds */
  modem_cap.data = 0;
  #else
  /* Get all hw capabilities */
  modem_cap.data = MCSHWIO_READ_CAP();
  #endif
}


/*============================================================================

FUNCTION MCS_MODEM_HAS_CAPABILITY

DESCRIPTION
  Returns if modem is capable of supporting queried modem technology.
  
DEPENDENCIES
  None

RETURN VALUE
  mcs_modem_cap_return_enum

SIDE EFFECTS
  None

============================================================================*/
mcs_modem_cap_return_enum mcs_modem_has_capability
(
  mcs_modem_capability_enum capability /* capability to check */
)
{
  mcs_modem_cap_return_enum return_value = MCS_MODEM_CAP_UNKNOWN;
  uint8 bitposition;

  /* Get bit position based on the chip type */
  switch (modem_cap.chipType)
  {
    case DALCHIPINFO_FAMILY_MSM8909:
      {
        bitposition = (uint8)cheel_bitpositions[(uint8)capability];
      }
      break;
    case DALCHIPINFO_FAMILY_MSM8952:
    case DALCHIPINFO_FAMILY_MSM8976:
      {
        bitposition = (uint8)sahi_bitpositions[(uint8)capability];
      }
      break;
    case DALCHIPINFO_FAMILY_MDM9x45:
    case DALCHIPINFO_FAMILY_MSM8996:
    case DALCHIPINFO_FAMILY_MSM8996SG:
      {
        bitposition = (uint8)tesla_bitpositions[(uint8)capability];
      }
      break;
    default:
      bitposition = CAP_UNSUPPORTED;
      break;
  }

  /* Not all features exist in every chip. If capability is not supported, return unavailable. */
  if (bitposition == CAP_UNSUPPORTED)
  {
    MSG_3(MSG_SSID_DFLT, MSG_LEGACY_ERROR,
          "Returning Unsupported capability %d for requested bit %d on chip %d",
          (uint8)MCS_MODEM_CAP_UNKNOWN, 
          (uint8)capability,
          modem_cap.chipType);
    return_value = MCS_MODEM_CAP_UNKNOWN;
  }
  /* Check if capability is supported by default (usually due to a missing hardware bit or fuse).
     If yes, return available. */
  else if (bitposition == CAP_SUPPORTED)
  {
    return_value = MCS_MODEM_CAP_AVAILABLE;
  }
  else
  {
    /* Check if the required bit is set */
    if (modem_cap.data & (1<<bitposition))
    {
      return_value = MCS_MODEM_CAP_AVAILABLE;
    }
    else
    {
      return_value = MCS_MODEM_CAP_UNAVAILABLE;
      MSG_3(MSG_SSID_DFLT, MSG_LEGACY_HIGH,
            "Modem Option Register 0x%x; Capability Requested %d, Chip %d", 
            modem_cap.data, capability, modem_cap.chipType);
    }
  }

  return return_value;
}
