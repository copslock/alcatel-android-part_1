/*!
  @file
  vbatt_core.c

  @brief
  This file implements VBATT-based limits
*/

/*=============================================================================

  Copyright (c) 2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document
  are confidential and proprietary information of Qualcomm Technologies
  Incorporated and all rights therein are expressly reserved.  By accepting
  this material the recipient agrees that this material and the information
  contained therein are held in confidence and in trust and will not be used,
  copied, reproduced in whole or in part, nor its contents revealed in any
  manner to others without the express written permission of Qualcomm
  Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/mcs/limitsmgr/vbatt/src/vbatt_core.c#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
08/13/15   tl      Release ADC handle when shutting down
06/01/15   rj      Added TA feature flag
05/29/15   tl      Initial revision

=============================================================================*/

/*=============================================================================
                                INCLUDE FILES
=============================================================================*/

#include "vbatt_i.h"
#include "vbatt_efs.h"
#include "lmtsmgr_diag.h"
#include "lmtsmgr_task.h"
#include "lmtsmgr_translation.h"
#include "mcsprofile.h"

#include "diagcmd.h"
#include "diagpkt.h"

/*=============================================================================
                              MACRO DEFINITIONS
=============================================================================*/

#ifdef TEST_FRAMEWORK
#error code not present
#endif // TEST_FRAMEWORK

/*=============================================================================
                         INTERNAL FUNCTION PROTOTYPES
=============================================================================*/

#if !defined( FEATURE_MCS_TABASCO )

/*=============================================================================

  FUNCTION:  vbatt_adc_set_threshold

=============================================================================*/
/*!
    @brief
    Calls the ADC API to set the specified threshold.

    @param[in]  type    The threshold type to set, ADC_TM_THRESHOLD_LOWER or
                        ADC_TM_THRESHOLD_HIGHER
    @param[in]  limit   The threshold to set, in mV

    @return
    None
*/
/*===========================================================================*/
static void vbatt_adc_set_threshold(AdcTMThresholdType type, int32 limit);

/*=============================================================================

  FUNCTION:  vbatt_adc_clear_threshold

=============================================================================*/
/*!
    @brief
    Calls the ADC API to clear the specified threshold.

    @param[in]  type    The threshold type to clear, ADC_TM_THRESHOLD_LOWER or
                        ADC_TM_THRESHOLD_HIGHER

    @return
    None
*/
/*===========================================================================*/
static void vbatt_adc_clear_threshold(AdcTMThresholdType type);

/*=============================================================================

  FUNCTION:  vbatt_adc_threshold_callback

=============================================================================*/
/*!
    @brief
    Callback called by ADC when a threshold set by DalAdc_TMSetThreshold() is
    hit.

    Updates the vbatt value and signals the Limits Manager task to check the
    current vbatt level against the current limits.

    @param[in]  object          Not used
    @param[in]  param           Not used
    @param[in]  vpayload        Pointer to the payload object, which is
                                expected to be a structure of type
                                AdcTMCallbackPayloadType
    @param[in]  payload_size    Size of the payload object, which is expected
                                to be sizeof(AdcTMCallbackPayloadType)

    @return
    NULL
*/
/*===========================================================================*/
static void * vbatt_adc_threshold_callback
(
  void * object,
  uint32 param,
  void * vpayload,
  uint32 payload_size
);

/*=============================================================================

  FUNCTION:  vbatt_adc_read_callback

=============================================================================*/
/*!
    @brief
    Callback called by ADC when DalAdc_RequestConversion() completes.

    Updates the vbatt value and signals the Limits Manager task to check the
    current vbatt level against the current limits.

    @param[in]  object          Not used
    @param[in]  param           Not used
    @param[in]  vpayload        Pointer to the payload object, which is
                                expected to be a structure of type
                                AdcResultType
    @param[in]  payload_size    Size of the payload object, which is expected
                                to be sizeof(AdcResultType)

    @return
    NULL
*/
/*===========================================================================*/
static void * vbatt_adc_read_callback
(
  void * object,
  uint32 param,
  void * vpayload,
  uint32 payload_size
);

/*=============================================================================

  FUNCTION:  vbatt_set_test_override

=============================================================================*/
/*!
    @brief
    Diag command handler to set the vbatt override value, to test the vbatt
    core algorithm in cases where the actual vbatt level can't be easily
    manipulated

    @param[in]  req_pkt The input packet, representing the command request
                        from QXDM, which is expected to be of type
                        DIAG_SUBSYS_LIMITSMGR_VBATT_SET_req_type
    @param[in]  pkt_len The length of the input packet, which is expected to
                        be sizeof(DIAG_SUBSYS_LIMITSMGR_VBATT_SET_req_type)

    @return
    Diag command response packet
*/
/*===========================================================================*/
static PACK(void *) vbatt_set_test_override
(
  PACK(void*) req_pkt,
  uint16 pkt_len
);

#endif /* !FEATURE_MCS_TABASCO */

/*=============================================================================
                              INTERNAL VARIABLES
=============================================================================*/

#if !defined( FEATURE_MCS_TABASCO )

vbatt_type vbatt;

/* Table containing diag commands and their callback function */
static const diagpkt_user_table_entry_type vbatt_diag_tbl[] =
{
  /* start id, end id, callback function */
  { (word)VBATT_SET, (word)VBATT_SET, vbatt_set_test_override },
};

#endif /* !FEATURE_MCS_TABASCO */

/*=============================================================================
                               PUBLIC FUNCTIONS
=============================================================================*/

/*=============================================================================

  FUNCTION:  vbatt_init

=============================================================================*/
/*!
    @brief
    Initializes vbatt limits and other resources

    @return
    None
*/
/*===========================================================================*/
void vbatt_init(void)
{
#if !defined( FEATURE_MCS_TABASCO )
  DALResult result;

  memset(&vbatt, 0, sizeof(vbatt));

  rex_init_crit_sect(&vbatt.vbatt_crit_sect);

  vbatt.active_band = SYS_BAND_CLASS_NONE;
  vbatt.active_tech = CXM_TECH_DFLT_INVLD;
  vbatt.record = NULL;
  vbatt.current_stage = VBATT_STAGE_0;
  vbatt.current_vbatt = INVALID_VBATT_VALUE;
  vbatt.vbatt_test_override = INVALID_VBATT_VALUE;
  vbatt.hysteresis_timer_expires = 0;
  vbatt.hysteresis_stage = VBATT_STAGE_INVALID;
  vbatt.vbatt_threshold_low = -1;
  vbatt.vbatt_threshold_high = -1;

  /* Create a timer used to honor the time hysteresis. When the timer expires
   * it will deliver a signal to the limits manager task, which will call
   * lmtsmgr_process_vbatt_event(). */
  timer_def(&vbatt.hysteresis_timer, NULL, rex_self(), LMTSMGR_SAR_VBATT_SIG,
      NULL, 0);

  /* Connect to ADC */
  DALSYS_InitMod(NULL);

  result = DAL_AdcDeviceAttach(DALDEVICEID_ADC, &vbatt.adc_handle);
  if(result != DAL_SUCCESS)
  {
    LMTSMGR_MSG_1(ERROR,
        "ADC device attach failed with result %d",
        result);
    return;
  }

  /* Get input properties for the VBATT channel */
  result = DalAdc_GetAdcInputProperties(vbatt.adc_handle, ADC_INPUT_VPH_PWR,
      strlen(ADC_INPUT_VPH_PWR), &vbatt.adc_properties);
  if(result != DAL_SUCCESS)
  {
    LMTSMGR_MSG_1(ERROR,
        "ADC get input properties for VBATT failed with result %d",
        result);
  }

  result = DalAdc_TMGetInputProperties(vbatt.adc_handle, ADC_INPUT_VPH_PWR,
      strlen(ADC_INPUT_VPH_PWR), &vbatt.adc_tm_properties);
  if(result != DAL_SUCCESS)
  {
    LMTSMGR_MSG_1(ERROR,
        "ADC get TM input properties for VBATT failed with result %d",
        result);
  }

  result = DALSYS_EventCreate(
              DALSYS_EVENT_ATTR_CLIENT_DEFAULT |
              DALSYS_EVENT_ATTR_CALLBACK_EVENT,
              &vbatt.adc_read_event,
              NULL);
  if(result != DAL_SUCCESS)
  {
    LMTSMGR_MSG_1(ERROR,
        "DAL create read event failed with result %d",
        result);
  }

  result = DALSYS_SetupCallbackEvent(vbatt.adc_read_event,
      vbatt_adc_read_callback, NULL);
  if(result != DAL_SUCCESS)
  {
    LMTSMGR_MSG_1(ERROR,
        "DAL create read event failed with result %d",
        result);
  }

  result = DALSYS_EventCreate(
              DALSYS_EVENT_ATTR_CLIENT_DEFAULT |
              DALSYS_EVENT_ATTR_CALLBACK_EVENT,
              &vbatt.adc_threshold_event,
              NULL);
  if(result != DAL_SUCCESS)
  {
    LMTSMGR_MSG_1(ERROR,
        "DAL create threshold event failed with result %d",
        result);
  }

  result = DALSYS_SetupCallbackEvent(vbatt.adc_threshold_event,
      vbatt_adc_threshold_callback, NULL);
  if(result != DAL_SUCCESS)
  {
    LMTSMGR_MSG_1(ERROR,
        "DAL create threshold event failed with result %d",
        result);
  }

  /* register the vbatt diag command table to the diag command service */
  DIAGPKT_DISPATCH_TABLE_REGISTER(DIAG_SUBSYS_LIMITSMGR, vbatt_diag_tbl);
#endif /* !FEATURE_MCS_TABASCO */
}

/*=============================================================================

  FUNCTION:  vbatt_deinit

=============================================================================*/
/*!
    @brief
    Frees all memory and other resources allocated by vbatt_init().

    @return
    None
*/
/*===========================================================================*/
void vbatt_deinit(void)
{
#if !defined( FEATURE_MCS_TABASCO )
  DALResult result;

  timer_clr(&vbatt.hysteresis_timer, T_NONE);
  timer_undef(&vbatt.hysteresis_timer);

  if(vbatt.adc_handle)
  {
    result = DAL_DeviceDetach(vbatt.adc_handle);

    if(result != DAL_SUCCESS)
    {
      LMTSMGR_MSG_1(ERROR,
          "ADC DAL_DeviceDetach() failed: %d",
          result);
    }
    vbatt.adc_handle = NULL;
  }
#endif /* !FEATURE_MCS_TABASCO */
}

/*=============================================================================

  FUNCTION:  lmtsmgr_process_vbatt_freq_input

=============================================================================*/
/*!
    @brief
    Updates the current active tech used for vbatt limiting. Calls
    lmtsmgr_process_vbatt_event() to run the complete vbatt limiting
    algorithm.

    @param[in]  new_tech        The tech that provided the frequency update

    @return
    None
*/
/*===========================================================================*/
void lmtsmgr_process_vbatt_freq_input(cxm_tech_type new_tech)
{
#if !defined( FEATURE_MCS_TABASCO )
  int i;
  sys_band_class_e_type new_band = SYS_BAND_CLASS_NONE;
  const lmtsmgr_tech_list_type *currList;

  ASSERT((new_tech > CXM_TECH_DFLT_INVLD) && (new_tech < CXM_TECH_MAX));

  MCSMARKER(VBATT_UPDATE_TECHS_I);

  /* Look through the uplink frequency information to determine what the
   * active transmit band is. If the band has changed, update everything. */
  currList = &lmtsmgr.tech_state[new_tech].currList;
  for(i = 0; i < currList->num_ul_entries; i++)
  {
    int j = currList->ul_freqList[i];

    if(new_band == SYS_BAND_CLASS_NONE)
    {
      new_band = currList->links[j].freqInfo.link_info.band;
    }
    else if(new_band != currList->links[j].freqInfo.link_info.band)
    {
      LMTSMGR_MSG_2(HIGH,
          "Multiple bands found for tech %d; using only band %d",
          new_tech, new_band);
    }
  }

  if(new_band == SYS_BAND_CLASS_NONE && new_tech != vbatt.active_tech)
  {
    /* This tech is rx-only, and another tech has active rx and tx. This looks
     * like a DR-DSDS scenario. Ignore this tech. */
    LMTSMGR_MSG_2(MED,
        "Skipping rx-only tech %d in favor of active tech %d",
        new_tech, vbatt.active_tech);
  }
  else if(new_band != vbatt.active_band)
  {
    vbatt.active_band = new_band;
    vbatt.active_tech = new_tech;
    vbatt.record = vbatt_lookup(new_band);
    vbatt.current_stage = VBATT_STAGE_INVALID;
    vbatt.hysteresis_timer_expires = 0;
    vbatt.hysteresis_stage = VBATT_STAGE_INVALID;

    timer_clr(&vbatt.hysteresis_timer, T_NONE);

    if(vbatt.record == NULL)
    {
      LMTSMGR_MSG_2(MED, "Record not found for new band %d for tech %d",
          new_band, new_tech);
    }
    else
    {
      LMTSMGR_MSG_2(MED, "Found record for new band %d for tech %d",
          new_band, new_tech);
    }

    lmtsmgr_process_vbatt_event();
  }

  MCSMARKER(VBATT_UPDATE_TECHS_O);
#endif /* !FEATURE_MCS_TABASCO */
}

/*=============================================================================

  FUNCTION:  lmtsmgr_process_vbatt_event

=============================================================================*/
/*!
    @brief
    Runs the vbatt limiting algorithm on the current active tech. Sets ADC
    thresholds and sends power limits to tech RF.

    @return
    None
*/
/*===========================================================================*/
void lmtsmgr_process_vbatt_event(void)
{
#if !defined( FEATURE_MCS_TABASCO )
  int i;
  vbatt_stage_type new_stage;
  /* New tx power limit to be applied */
  int16 vbatt_plimit;

  if(vbatt.adc_handle == NULL)
  {
    LMTSMGR_MSG_0(ERROR, "ADC device not connected");
    return;
  }

  MCSMARKER(VBATT_UPDATE_I);

  rex_enter_crit_sect(&vbatt.vbatt_crit_sect);

  if(vbatt.hysteresis_timer_expires > 0)
  {
    uint32 now = timetick_get_ms();
    if(now < vbatt.hysteresis_timer_expires)
    {
      LMTSMGR_MSG_2(MED,
          "Hysteresis timer not yet expired "
          "(expires at %u, current time is %u)",
          vbatt.hysteresis_timer_expires, now);
    }
    else
    {
      /* Hysteresis timer expired. */
      LMTSMGR_MSG_0(MED, "Hysteresis timer expired");
      vbatt.hysteresis_timer_expires = 0;
      vbatt.hysteresis_stage = VBATT_STAGE_INVALID;
      vbatt.current_vbatt = INVALID_VBATT_VALUE;
    }
  }

  /* If the current band does not have a vbatt record, there is no work to do.
   * Clear the state and return immediately. */
  if(vbatt.record == NULL)
  {
    /* No record for this band. Set stage to -1, indicating no limit. */
    vbatt.current_stage = VBATT_STAGE_0;

    /* Clear the ADC limits */
    vbatt_adc_clear_threshold(ADC_TM_THRESHOLD_LOWER);
    vbatt_adc_clear_threshold(ADC_TM_THRESHOLD_HIGHER);

    /* We are no longer tracking vbatt */
    vbatt.current_vbatt = INVALID_VBATT_VALUE;

    vbatt_log_state();

    rex_leave_crit_sect(&vbatt.vbatt_crit_sect);

    if(vbatt.active_tech != CXM_TECH_DFLT_INVLD)
    {
      lmtsmgr.tech_state[vbatt.active_tech].pwr_info[0].new_limits.vbatt_plimit
        = DEFAULT_MAX_PLIMIT;
      lmtsmgr_notify_tech_plimit(vbatt.active_tech);
    }

    MCSMARKER(VBATT_UPDATE_O);
    return;
  }

  if(vbatt.vbatt_test_override != INVALID_VBATT_VALUE)
  {
    /* If an override value has been specified using the Diag command from
     * QXDM, use the override value here instead of the actual physical value
     * from ADC. */
    vbatt.current_vbatt = vbatt.vbatt_test_override;
  }

  /* Check whether the vbatt value is valid. If stale, request a new vbatt
   * read. */
  if(vbatt.current_vbatt == INVALID_VBATT_VALUE)
  {
    DALResult result;
    AdcRequestParametersType adc_params;

    adc_params.hEvent = vbatt.adc_read_event;
    adc_params.nDeviceIdx = vbatt.adc_properties.nDeviceIdx;
    adc_params.nChannelIdx = vbatt.adc_properties.nChannelIdx;

    MCSMARKER(VBATT_ADC_READ_REQ);

    result = DalAdc_RequestConversion(vbatt.adc_handle, &adc_params, NULL);
    if(result == DAL_SUCCESS)
    {
      LMTSMGR_MSG_0(LOW, "Requested ADC conversion for VBATT");
    }
    else
    {
      LMTSMGR_MSG_1(ERROR, "ADC read request failed: %d", result);
    }

    /* Wait for the ADC read to complete. When complete, the callback will
     * signal the Limits Manager task to continue the algorithm. */

    rex_leave_crit_sect(&vbatt.vbatt_crit_sect);
    MCSMARKER(VBATT_UPDATE_O);
    return;
  }

  /*
   * Determine any updates to the current stage
   */

  if((vbatt.current_stage <= VBATT_STAGE_INVALID) ||
      (vbatt.current_stage >= VBATT_STAGES_MAX))
  {
    vbatt.current_stage = VBATT_STAGE_INVALID;
    new_stage = VBATT_STAGE_0;
  }
  else
  {
    new_stage = vbatt.current_stage;
  }

  for(i = 0; i <= vbatt.current_stage; i++)
  {
    /* Compare the current vbatt against each lower stage's voltage_up
     * threshold. If the threshold is met, transition to the prior stage. */
    if(vbatt.current_vbatt >= vbatt.record->stages[i].voltage_up)
    {
      if(i - 1 != vbatt.hysteresis_stage)
      {
        new_stage = i - 1;
        LMTSMGR_MSG_4(HIGH,
            "Transition from stage %d to stage %d: vbatt %d > voltage_up %d",
            vbatt.current_stage,
            new_stage,
            vbatt.current_vbatt,
            vbatt.record->stages[i].voltage_up);
        break;
      }
      else
      {
        LMTSMGR_MSG_1(MED,
            "Hysteresis timer not yet expired; cannot go back to stage %d",
            vbatt.hysteresis_stage);
      }
    }
  }

  for(i = VBATT_STAGES_MAX - 1; i > vbatt.current_stage && i >= 0; i--)
  {
    /* Compare the current vbatt against each higher stage's voltage_down
     * threshold. If the threshold is met, transition to that stage. */
    if(vbatt.current_vbatt <= vbatt.record->stages[i].voltage_down)
    {
      if(i != vbatt.hysteresis_stage)
      {
        new_stage = i;
        LMTSMGR_MSG_4(HIGH,
            "Transition from stage %d to stage %d: "
            "vbatt %d < voltage_down %d",
            vbatt.current_stage,
            new_stage,
            vbatt.current_vbatt,
            vbatt.record->stages[i].voltage_down);
        break;
      }
      else
      {
        LMTSMGR_MSG_1(MED,
            "Hysteresis timer not yet expired; cannot go back to stage %d",
            vbatt.hysteresis_stage);
      }
    }
  }

  if(new_stage != vbatt.current_stage)
  {
    if(vbatt.record->time_hysteresis > 0)
    {
      /* If this is a new stage, and the hysteresis timer is non-zero, set a
       * timer to wait before applying the new limits */
      vbatt.hysteresis_timer_expires =
        timetick_get_ms() + vbatt.record->time_hysteresis;

      vbatt.hysteresis_stage = vbatt.current_stage;

      LMTSMGR_MSG_4(HIGH,
          "Band %d stage %d Setting hysteresis timer for %u expiring at %u",
          vbatt.active_band, new_stage,
          vbatt.record->time_hysteresis,
          vbatt.hysteresis_timer_expires);

      timer_set(&vbatt.hysteresis_timer, vbatt.record->time_hysteresis,
          0, T_MSEC);
    }
  }

  vbatt.current_stage = new_stage;

  /*
   * Determine power limits for the current stage
   */
  if(vbatt.current_stage == VBATT_STAGE_0)
  {
    vbatt_plimit = DEFAULT_MAX_PLIMIT;
  }
  else if((vbatt.current_stage >= VBATT_STAGE_1) &&
      (vbatt.current_stage < VBATT_STAGES_MAX))
  {
    vbatt_plimit = vbatt.record->stages[vbatt.current_stage].tx_power_limit;
  }
  else
  {
    ERR_FATAL("Invalid current stage %d", vbatt.current_stage, 0, 0);
  }

  LMTSMGR_MSG_3(HIGH,
      "Band %d stage %d vbatt limit %d",
      vbatt.active_band, vbatt.current_stage, vbatt_plimit);

  ASSERT((vbatt.active_tech > CXM_TECH_DFLT_INVLD) &&
      (vbatt.active_tech < CXM_TECH_MAX));
  lmtsmgr.tech_state[vbatt.active_tech].pwr_info[0].new_limits.vbatt_plimit =
    vbatt_plimit;

  /*
   * Set ADC limits for the limits where the hysteresis timer does not apply
   */
  if((vbatt.current_stage < VBATT_STAGES_MAX - 1) &&
      (vbatt.current_stage + 1 != vbatt.hysteresis_stage) &&
      (vbatt.vbatt_test_override == INVALID_VBATT_VALUE))
  {
    vbatt_adc_set_threshold(ADC_TM_THRESHOLD_LOWER,
        vbatt.record->stages[vbatt.current_stage + 1].voltage_down);
  }
  else
  {
    vbatt_adc_clear_threshold(ADC_TM_THRESHOLD_LOWER);
  }

  if((vbatt.current_stage >= 0) &&
      (vbatt.current_stage - 1 != vbatt.hysteresis_stage) &&
      (vbatt.vbatt_test_override == INVALID_VBATT_VALUE))
  {
    vbatt_adc_set_threshold(ADC_TM_THRESHOLD_HIGHER,
        vbatt.record->stages[vbatt.current_stage].voltage_up);
  }
  else
  {
    vbatt_adc_clear_threshold(ADC_TM_THRESHOLD_HIGHER);
  }

  vbatt_log_state();

  rex_leave_crit_sect(&vbatt.vbatt_crit_sect);

  /* Notify the active tech if the vbatt limit has changed */
  lmtsmgr_notify_tech_plimit(vbatt.active_tech);

  MCSMARKER(VBATT_UPDATE_O);
#endif /* !FEATURE_MCS_TABASCO */
}

/*=============================================================================
                              INTERNAL FUNCTIONS
=============================================================================*/

#if !defined( FEATURE_MCS_TABASCO )

/*=============================================================================

  FUNCTION:  vbatt_adc_set_threshold

=============================================================================*/
/*!
    @brief
    Calls the ADC API to set the specified threshold.

    @param[in]  type    The threshold type to set, ADC_TM_THRESHOLD_LOWER or
                        ADC_TM_THRESHOLD_HIGHER
    @param[in]  limit   The threshold to set, in mV

    @return
    None
*/
/*===========================================================================*/
static void vbatt_adc_set_threshold(AdcTMThresholdType type, int32 limit)
{
  DALResult result;
  int32 limit_set;
  AdcTMRequestParametersType adc_params;

  ASSERT(vbatt.adc_handle);

  adc_params.adcTMInputProps = vbatt.adc_tm_properties;
  adc_params.hEvent = vbatt.adc_threshold_event;

  result = DalAdc_TMSetThreshold(vbatt.adc_handle, &adc_params, type, &limit,
      &limit_set);

  if(result == DAL_SUCCESS)
  {
    LMTSMGR_MSG_3(MED, "Set threshold %d to %d (%d)", type, limit, limit_set);
  }
  else
  {
    LMTSMGR_MSG_3(ERROR, "Unable to set threshold %d to %d: %d",
        type, limit, result);
  }

  /* Save the limit for logging */
  switch(type)
  {
    case ADC_TM_THRESHOLD_LOWER:
      vbatt.vbatt_threshold_low = limit;
      break;

    case ADC_TM_THRESHOLD_HIGHER:
      vbatt.vbatt_threshold_high = limit;
      break;

    default:
      break;
  }
}

/*=============================================================================

  FUNCTION:  vbatt_adc_clear_threshold

=============================================================================*/
/*!
    @brief
    Calls the ADC API to clear the specified threshold.

    @param[in]  type    The threshold type to clear, ADC_TM_THRESHOLD_LOWER or
                        ADC_TM_THRESHOLD_HIGHER

    @return
    None
*/
/*===========================================================================*/
static void vbatt_adc_clear_threshold(AdcTMThresholdType type)
{
  DALResult result;
  AdcTMRequestParametersType adc_params;

  ASSERT(vbatt.adc_handle);

  adc_params.adcTMInputProps = vbatt.adc_tm_properties;
  adc_params.hEvent = vbatt.adc_threshold_event;

  result = DalAdc_TMSetThreshold(vbatt.adc_handle, &adc_params, type, NULL,
      NULL);

  if(result == DAL_SUCCESS)
  {
    LMTSMGR_MSG_1(MED, "Cleared threshold %d", type);
  }
  else
  {
    LMTSMGR_MSG_2(ERROR, "Unable to clear threshold %d: %d", type, result);
  }

  /* Save the limit for logging */
  switch(type)
  {
    case ADC_TM_THRESHOLD_LOWER:
      vbatt.vbatt_threshold_low = -1;
      break;

    case ADC_TM_THRESHOLD_HIGHER:
      vbatt.vbatt_threshold_high = -1;
      break;

    default:
      break;
  }
}

/*=============================================================================

  FUNCTION:  vbatt_adc_threshold_callback

=============================================================================*/
/*!
    @brief
    Callback called by ADC when a threshold set by DalAdc_TMSetThreshold() is
    hit.

    Updates the vbatt value and signals the Limits Manager task to check the
    current vbatt level against the current limits.

    @param[in]  object          Not used
    @param[in]  param           Not used
    @param[in]  vpayload        Pointer to the payload object, which is
                                expected to be a structure of type
                                AdcTMCallbackPayloadType
    @param[in]  payload_size    Size of the payload object, which is expected
                                to be sizeof(AdcTMCallbackPayloadType)

    @return
    NULL
*/
/*===========================================================================*/
static void * vbatt_adc_threshold_callback
(
  void * object,
  uint32 param,
  void * vpayload,
  uint32 payload_size
)
{
  const AdcTMCallbackPayloadType *payload =
    (const AdcTMCallbackPayloadType *)vpayload;

  MCSMARKER(VBATT_ADC_THRESHOLD);

#if DALADC_INTERFACE_VERSION >= DALINTERFACE_VERSION(4,2)
  LMTSMGR_MSG_4(MED,
      "Threshold %d triggered on device %d channel %d: new level is %d",
      payload->eThresholdTriggered,
      payload->adcTMInputProps.uDeviceIdx,
      payload->adcTMInputProps.uChannelIdx,
      payload->nPhysicalTriggered);
#else
  LMTSMGR_MSG_3(MED, "Threshold %d triggered on channel %d: new level is %d",
      payload->eThresholdTriggered,
      payload->uTMChannelIdx,
      payload->nPhysicalTriggered);
#endif

  /* Update the new vbatt level */
  rex_enter_crit_sect(&vbatt.vbatt_crit_sect);
  vbatt.current_vbatt = payload->nPhysicalTriggered;
  rex_leave_crit_sect(&vbatt.vbatt_crit_sect);

  /* Signal the Limits Manager task to handle the updated level */
  lmtsmgr_set_sigs(LMTSMGR_SAR_VBATT_SIG);

  return NULL;
}

/*=============================================================================

  FUNCTION:  vbatt_adc_read_callback

=============================================================================*/
/*!
    @brief
    Callback called by ADC when DalAdc_RequestConversion() completes.

    Updates the vbatt value and signals the Limits Manager task to check the
    current vbatt level against the current limits.

    @param[in]  object          Not used
    @param[in]  param           Not used
    @param[in]  vpayload        Pointer to the payload object, which is
                                expected to be a structure of type
                                AdcResultType
    @param[in]  payload_size    Size of the payload object, which is expected
                                to be sizeof(AdcResultType)

    @return
    NULL
*/
/*===========================================================================*/
static void * vbatt_adc_read_callback
(
  void * object,
  uint32 param,
  void * vpayload,
  uint32 payload_size
)
{
  const AdcResultType *payload =
    (const AdcResultType *)vpayload;

  MCSMARKER(VBATT_ADC_READ_RESP);

  LMTSMGR_MSG_3(MED,
      "ADC read completed on channel %d: result %d new level is %d",
      payload->nChannelIdx,
      payload->eStatus,
      payload->nPhysical);

  /* Update the new vbatt level */
  rex_enter_crit_sect(&vbatt.vbatt_crit_sect);
  if(payload->eStatus == ADC_RESULT_VALID)
  {
    vbatt.current_vbatt = payload->nPhysical;
  }
  else
  {
    /* Result was not successful. Set the current vbatt value to invalid and
     * signal the task. lmtsmgr_process_vbatt_event() will reissue the read
     * request. */
    vbatt.current_vbatt = INVALID_VBATT_VALUE;
  }
  rex_leave_crit_sect(&vbatt.vbatt_crit_sect);

  /* Signal the Limits Manager task to handle the updated level */
  lmtsmgr_set_sigs(LMTSMGR_SAR_VBATT_SIG);

  return NULL;
}

/*=============================================================================

  FUNCTION:  vbatt_set_test_override

=============================================================================*/
/*!
    @brief
    Diag command handler to set the vbatt override value, to test the vbatt
    core algorithm in cases where the actual vbatt level can't be easily
    manipulated

    @param[in]  req_pkt The input packet, representing the command request
                        from QXDM, which is expected to be of type
                        DIAG_SUBSYS_LIMITSMGR_VBATT_SET_req_type
    @param[in]  pkt_len The length of the input packet, which is expected to
                        be sizeof(DIAG_SUBSYS_LIMITSMGR_VBATT_SET_req_type)

    @return
    Diag command response packet
*/
/*===========================================================================*/
static PACK(void *) vbatt_set_test_override
(
  PACK(void*) req_pkt,
  uint16 pkt_len
)
{
  DIAG_SUBSYS_LIMITSMGR_VBATT_SET_req_type       *req_ptr;
  DIAG_SUBSYS_LIMITSMGR_VBATT_SET_rsp_type       *rsp_ptr;

  /* Check if the request is valid */
  if(req_pkt == NULL)
  {
    LMTSMGR_MSG_0(ERROR, "Invalid diag request to set DSI value");
    return NULL;
  }
  if(pkt_len != sizeof(DIAG_SUBSYS_LIMITSMGR_VBATT_SET_req_type))
  {
    LMTSMGR_MSG_2(ERROR,
        "Invalid diag request to set DSI value (size %d, expected %d)",
        pkt_len, sizeof(DIAG_SUBSYS_LIMITSMGR_VBATT_SET_req_type));
    return NULL;
  }

  req_ptr = (DIAG_SUBSYS_LIMITSMGR_VBATT_SET_req_type *)req_pkt;

  rex_enter_crit_sect(&vbatt.vbatt_crit_sect);
  if(req_ptr->vbatt_value == 0x7fff)
  {
    LMTSMGR_MSG_0(MED, "Clearing VBATT level override");
    vbatt.vbatt_test_override = INVALID_VBATT_VALUE;
    vbatt.current_vbatt = INVALID_VBATT_VALUE;
  }
  else
  {
    LMTSMGR_MSG_1(MED, "Setting VBATT level override to %d",
        req_ptr->vbatt_value);
    vbatt.vbatt_test_override = req_ptr->vbatt_value;
  }
  rex_leave_crit_sect(&vbatt.vbatt_crit_sect);

  /* Signal the Limits Manager task to handle the updated level */
  lmtsmgr_set_sigs(LMTSMGR_SAR_VBATT_SIG);

  /* Allocate the memory for the response */
  rsp_ptr = (DIAG_SUBSYS_LIMITSMGR_VBATT_SET_rsp_type *)
            diagpkt_subsys_alloc((diagpkt_subsys_id_type)DIAG_SUBSYS_LIMITSMGR,
            (diagpkt_subsys_cmd_code_type)VBATT_SET,
            sizeof(DIAG_SUBSYS_LIMITSMGR_VBATT_SET_rsp_type));

  return rsp_ptr;
}

#endif /* !FEATURE_MCS_TABASCO */
