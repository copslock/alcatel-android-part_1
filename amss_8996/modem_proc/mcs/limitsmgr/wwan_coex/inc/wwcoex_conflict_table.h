#ifndef __WWCOEX_CONFLICT_TABLE_H__
#define __WWCOEX_CONFLICT_TABLE_H__
/*!
  @file wwcoex_conflict_table.h

  @brief
   Interface to the conflict table for the WWAN COEX module
 
*/

/*=============================================================================

  Copyright (c) 2013-2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

=============================================================================*/

/*==============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/mcs/limitsmgr/wwan_coex/inc/wwcoex_conflict_table.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
10/07/15   jm      Immediately flush out FreqIDs during FW conflict check logging
08/04/15   sg      Fix for getting the right freq ID for QCTA conflicts
06/16/15   jm      Add history buffer for conflict checks
06/16/15   jm      Ignore dummy RF devices in channel conflict table
04/15/15   sg      QXTA support
04/03/15   jm      Allow coex disable for all MSIM modes
09/22/14   sg      Support for spur mitigation
07/03/14   jm      Support for desense calculation in DR-DSDS
01/29/14   jm      Increase max number of concurrent active techs
01/14/14   jm      Ensure freqID is 16-bit value
05/05/13   ag      Moved init function
03/24/13   ag      Initial Revision

=============================================================================*/
/*=============================================================================

                           INCLUDE FILES

=============================================================================*/

#include "comdef.h"
#include "wwcoex_action_iface.h"

#ifdef __cplusplus
extern "C" {
#endif
/*=============================================================================

                       CONSTANTS AND DEFINES

=============================================================================*/
/*! Maximum number of concurrent active tech scenarios */
#define COEX_MAX_CONCURRENT_TECHS        4
#define COEX_MAX_CONCURRENT_SCENARIOS   (COEX_MAX_CONCURRENT_TECHS*(COEX_MAX_CONCURRENT_TECHS-1)/2) 
#define WWCOEX_TBL_INVALID  (int8)       -1
#define WWCOEX_MAX_TABLES_PER_SCENARIO   2

/* Spur related macros */
#define WWCOEX_MAX_SPUR_TABLE_SIZE       256
#define WWCOEX_SPUR_HANDLE_NONE          0
#define WWCOEX_SPUR_HANDLE_UNKNOWN       0xFFF0

/*! These are duplicate macros to the ones existing in wwan_coex_mgr.h. 
    Compile time assert in place to ensure these are always in sync */
#define WWCOEX_MAX_SUPPORTED_LINKS       24
#define WWCOEX_UNKNOWN_FREQID            0x0000FFF0

/* MAX RF devices supported */
#define WWCOEX_MAX_RF_DEV                8 // 3 bits max
#define WWCOEX_MAX_CHNL_CFLT_TBLS        2

/* MAX history size for channel conflict query from FW */
#define WWCOEX_MAX_CHNL_QUERY_HIST_SIZE  20

/* MAX history size for CRAT mode query from FW */
#define WWCOEX_MAX_CRAT_QUERY_HIST_SIZE  20

/* MAX history size for CRAT merged buffer that has updates
   from TRM, MMCP and FW */
#define WWCOEX_MAX_CRAT_MERGE_HIST_SIZE 40

/* Number of freqids packed */
#define WWCOEX_MAX_NUM_PACKED_FREQIDS      4

/* channels per channel grouping */
#define WWCOEX_MAX_CHNL_PER_GRP            2

/* defines the incrament we are copying to logs */
#define CHNL_HIST_NEW_ENTRY_PER_LOG        5


/*=============================================================================

                             TYPEDEFS

=============================================================================*/

/* Enum used to indicate the type of channel conflict
   between the given channel IDs 
   Enum is sorted in the order of severity */
typedef enum
{
  WWCOEX_CHNL_CFLT_NONE,
  WWCOEX_CHNL_CFLT_DGLNA,
  WWCOEX_CHNL_CFLT_FILTER,
  WWCOEX_CHNL_CFLT_LEGACY,
  WWCOEX_CHNL_CFLT_CARRIER,
  WWCOEX_CHNL_CFLT_MAX
}wwcoex_chnl_cflt_type;

/* Enum used to indicate the type of channel conflict
   between the given channel IDs 
   Enum is sorted in the order of severity */
typedef enum
{
  WWCOEX_CFLT_NONE,
  WWCOEX_CFLT_DESENSE,
  WWCOEX_CFLT_SPUR,
  WWCOEX_CFLT_FILTER,
  WWCOEX_CFLT_CHANNEL,
  WWCOEX_CFLT_MAX
}wwcoex_cflt_type;

/* Enum used to indicate the entiry that updated
   CRAT merged history buffer */
typedef enum
{
  WWCOEX_CRAT_FW,
  WWCOEX_CRAT_MMCP,
  WWCOEX_CRAT_TRM,
  WWCOEX_CRAT_L1,
  WWCOEX_CRAT_RF,
  WWCOEX_CRAT_MAX
}wwcoex_crat_update_type;

/* Structure used for storing result 
   for desense and spurs */
typedef struct 
{
  cxm_action_type   action;
  wwan_coex_desense_type  desense;
  uint16                  spur_handle_1;
  uint16                  spur_handle_2;
}coex_result_type;

/* Sturcture used for storing for storing desense
   and spur tables. This table is indexed during a FW 
   desense query */
typedef struct 
{
  /* Table for tech 1, tech 2 conflicts */
  coex_result_type arr[WWCOEX_MAX_SUPPORTED_LINKS][WWCOEX_MAX_SUPPORTED_LINKS];

  /* Freq ID offsets of each tech used for this table */
  uint32 tech1_fid_offset;
  uint32 tech2_fid_offset;

  uint32 tech1_num_entries;
  uint32 tech2_num_entries;

  boolean is_valid;
}wwcoex_tbl_type;

/* Structure to store all the conflict tables */
typedef struct
{
  /* Tables for tech 1, tech 2 conflicts */
  wwcoex_tbl_type tables[WWCOEX_MAX_TABLES_PER_SCENARIO];

  uint8   tech1;
  uint8   tech2;

  int8           current_tbl;
}wwcoex_conflict_tbls_type;


/* Structure used for storing channel conflict information 
   along with freq IDs for which there is channel conflict */
typedef struct
{
  wwcoex_chnl_cflt_type  cflt_type;
  uint16                 freq_id;
}wwcoex_chnl_cflt_result_type;

/* Stucture used for storing channel conflict tables */
typedef struct
{
  /* stores channel conflict result */
  wwcoex_chnl_cflt_result_type arr[WWCOEX_MAX_RF_DEV][WWCOEX_MAX_RF_DEV];

  /* tech IDs for which the table is created */
  uint8 tech1;
  uint8 tech2;

  /* indicates if the current table is valid */
  boolean is_valid;
}wwcoex_chnl_cflt_table_type;

/* Structure to store the parameters of FW query
   and CXM result for desense, channel and filter conflicts */
typedef struct
{

  /* Parameters passed by FW-CXM to MCS during conflict check */
  cxm_conflict_check_s  input_params;

  /* Requesting Freq IDs */
  uint64                req_ids_packed;

  /* Conflicting Freq IDs */
  uint64                conf_ids_packed;
  
  /* Type of conflict FW-CXM requested */
  wwcoex_cflt_type      cflt_type;
  
}wwcoex_query_cflt_hist_type;

/* Structure to store CRAT mode updates from
   TRM, MMCP and CXM */
typedef struct
{
  cxm_fw_crat_mode_t       crat_mode;
  wwcoex_crat_update_type  client;
}wwcoex_crat_merge_hist_type;


/*=============================================================================

                        FUNCTION DECLARATIONS

=============================================================================*/


/*=============================================================================

  FUNCTION:  wwcoex_get_table_node

=============================================================================*/
/*!
    @brief
    Get the conflict table node corresponding to the two tech pair. If there
    is no table for the pair, then return an unused node.
 
    @return
    conflict table node

*/
/*===========================================================================*/
wwcoex_conflict_tbls_type* wwcoex_get_table_node
(
  uint8   tech1,
  uint8   tech2
);

/*=============================================================================

  FUNCTION:  wwcoex_invalidate_tables

=============================================================================*/
/*!
    @brief
    Invalidates the conflict tables containing the specified tech
 
    @return
    NONE

*/
/*===========================================================================*/
void wwcoex_invalidate_tables
(
  uint8  tech
);

/*=============================================================================

  FUNCTION:  wwcoex_get_oldest_tbl_index

=============================================================================*/
/*!
    @brief
    Returns index to the oldest table
 
    @return
    NONE

*/
/*===========================================================================*/
int8 wwcoex_get_oldest_tbl_index
(
  wwcoex_conflict_tbls_type*  conflict_tbl
);

/*===========================================================================
FUNCTION wwcoex_set_sub_state

DESCRIPTION
  This API will update the subscription state

DEPENDENCIES 
  None

RETURN VALUE  
  None

SIDE EFFECTS
  None
  
===========================================================================*/
void wwcoex_set_sub_state
(
  uint32 num_standby_stacks,
  uint32 num_active_stacks
);

/*===========================================================================
FUNCTION wwcoex_get_sub_state_standby

DESCRIPTION
  This API will retrieve the standby stack value

DEPENDENCIES 
  None

RETURN VALUE  
  uint32

SIDE EFFECTS
  None
  
===========================================================================*/
uint32 wwcoex_get_sub_state_standby();

/*===========================================================================
FUNCTION wwcoex_set_rf_mode

DESCRIPTION
  This API will update the RF mode

DEPENDENCIES 
  None

RETURN VALUE  
  None

SIDE EFFECTS
  None
  
===========================================================================*/
void wwcoex_set_rf_mode
(
  boolean is_drdsds_enabled
);

/*===========================================================================
FUNCTION wwcoex_set_spur_mitigation_mask

DESCRIPTION
  This API will update the RF mode

DEPENDENCIES 
  None

RETURN VALUE  
  None

SIDE EFFECTS
  None
  
===========================================================================*/
void wwcoex_set_spur_mitigation_mask
(
  boolean is_spur_mitigation_enabled
);

/*=============================================================================*/
/*!
    @brief
    Search for a freq id pair in the channel conflict tables and 
    return the action
 
    @return
    cxm_action_type

*/
/*===========================================================================*/
boolean wwcoex_is_chnl_cflt_table_valid
(
  uint32 tech1,
  uint32 tech2,
  uint32 *index
);


#ifdef __cplusplus
}
#endif

#endif /* __WWCOEX_CONFLICT_TABLE_H__ */
