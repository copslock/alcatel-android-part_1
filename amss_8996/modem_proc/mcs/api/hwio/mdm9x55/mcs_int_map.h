#ifndef __MCS_INT_MAP_H__
#define __MCS_INT_MAP_H__
/*
===========================================================================
*/
/**
  @file mcs_int_map.h
  @brief MCS interrupt include file.

*/
/*
  ===========================================================================

  Copyright (c) 2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

  ===========================================================================

  $Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/mcs/api/hwio/mdm9x55/mcs_int_map.h#1 $
  $DateTime: 2016/03/28 23:03:58 $
  $Author: mplcsds1 $

  ===========================================================================
*/

/*----------------------------------------------------------------------------
 * MODULE: STMR events IRQ's
 *--------------------------------------------------------------------------*/

#ifndef HW_IRQ_STMR_EVT0

  #define HW_IRQ_STMR_EVT0           32
  #define HW_IRQ_STMR_EVT1           33
  #define HW_IRQ_STMR_EVT2           34
  #define HW_IRQ_STMR_EVT3           35
  #define HW_IRQ_STMR_EVT4           36
  #define HW_IRQ_STMR_EVT5           37
  #define HW_IRQ_STMR_EVT6           38
  #define HW_IRQ_STMR_EVT7           39
  #define HW_IRQ_STMR_EVT8           40
  #define HW_IRQ_STMR_EVT9           41
  #define HW_IRQ_STMR_EVT10          42
  #define HW_IRQ_STMR_EVT11          43
  #define HW_IRQ_STMR_EVT12          44
  #define HW_IRQ_STMR_EVT13          45
  #define HW_IRQ_STMR_EVT14          46
  #define HW_IRQ_STMR_EVT15          47
  #define HW_IRQ_STMR_EVT16          48
  #define HW_IRQ_STMR_EVT17          49
  #define HW_IRQ_STMR_EVT18          50
  #define HW_IRQ_STMR_EVT19          51
  #define HW_IRQ_STMR_EVT20          52
  #define HW_IRQ_STMR_EVT21          53
  #define HW_IRQ_STMR_EVT22          54
  #define HW_IRQ_STMR_EVT23          55
  #define HW_IRQ_STMR_EVT24          56
  #define HW_IRQ_STMR_EVT25          57
  #define HW_IRQ_STMR_EVT26          58
  #define HW_IRQ_STMR_EVT27          59
  #define HW_IRQ_STMR_EVT28          60
  #define HW_IRQ_STMR_EVT29          61
  #define HW_IRQ_STMR_EVT30          62
  #define HW_IRQ_STMR_EVT31          63

#endif

#endif /* __MCS_INT_MAP_H__ */
