#ifndef TRM_DOT_H
#define TRM_DOT_H

/*===========================================================================

                   T R M    S T R U C T   H E A D E R    F I L E

DESCRIPTION
   This file contains the declaration of TRM data structure.

  Copyright (c) 2014-2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.
===========================================================================*/


/*===========================================================================

                      EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/mcs/trm/src/trm_dot_init.h#1 $

when         who     what, where, why
--------     ---     ----------------------------------------------------------
08/31/2015   mn      Changes for LTE CA device hopping (CR: 894455).
08/28/2015   rj      Add support for 2UL CA to make 1UL CA entry in DOT
08/19/2015   ag      Disable autogen on RUMI builds.
04/24/2015   ag      Added support for new table format and TDD UL CA card 
02/26/2015   mn      Initial version.
===========================================================================*/

/*============================================================================

                           INCLUDE FILES FOR MODULE

============================================================================*/

#include "customer.h"
#include "trm_config_handler.h"
#include "list.h"
#include "sys.h"
#include "trm.h"
#include "rflte_ext_mc.h"

/*============================================================================

                   DEFINITIONS AND DECLARATIONS FOR MODULE

This section contains local definitions for constants, macros, typesdefs,
and other items needed by this module.

============================================================================*/

#if defined(FEATURE_MCS_ATLAS) || defined(T_RUMI_EMULATION)
 /* AT has link issues so disable AG for now... */ 
 #undef FEATURE_MCS_DOT_AG_ENABLED
#else 
 #define FEATURE_MCS_DOT_AG_ENABLED
#endif

#ifdef FEATURE_MCS_ATLAS
  /* AT does not have this api */
  #define FEATURE_RF_NEW_CA_API_ONLY_SUPPORT_ULCA
#endif

//#ifdef FEATURE_CUST_1
  /* DGLNA is supported only if this feature is defined */
 // #define FEATURE_MCS_DGLNA_SUPPORTED
//#endif

/* Band range for each RAT */
#define TRM_DOT_1X_BAND_START  SYS_BAND_BC0
#define TRM_DOT_1X_BAND_END  SYS_BAND_BC19

#define TRM_DOT_HDR_BAND_START  SYS_BAND_BC0
#define TRM_DOT_HDR_BAND_END  SYS_BAND_BC19

#define TRM_DOT_TDSCDMA_BAND_START  SYS_BAND_TDS_BANDA
#define TRM_DOT_TDSCDMA_BAND_END  SYS_BAND_TDS_BANDF

#define TRM_DOT_LTE_TDD_BAND_START SYS_BAND_LTE_EUTRAN_BAND33
#define TRM_DOT_LTE_TDD_BAND_END SYS_BAND_LTE_EUTRAN_BAND255

#define TRM_DOT_LTE_FDD_BAND_START SYS_BAND_LTE_EUTRAN_BAND1
#define TRM_DOT_LTE_FDD_BAND_END SYS_BAND_LTE_EUTRAN_BAND32

#define TRM_DOT_UMTS_BAND_START SYS_BAND_WCDMA_I_IMT_2000
#define TRM_DOT_UMTS_BAND_END SYS_BAND_WCDMA_XIX_850

#define TRM_DOT_GSM_BAND_START SYS_BAND_GSM_450
#define TRM_DOT_GSM_BAND_END SYS_BAND_GSM_PCS_1900

#define TRM_DOT_INVALID_ANTNUM (uint32)0xFFFFFFFF
#define TRM_DOT_NO_DEVICE_SET_NUM (uint32)0x7FFFFFFF

#define TRM_DOT_NO_DEVICE_SET_IDX (uint8)0xFF

/* MAX number of Bands for LTE tech */
#define TRM_DOT_LTE_MAX_NUM_OF_BANDS (TRM_DOT_LTE_TDD_BAND_END - TRM_DOT_LTE_FDD_BAND_START + 1)


#define TRM_DOT_GET_BAND_IDX(rat,dds,dr, band) ((uint32)(band) - trm_dot.tech_list[(rat)][(dds)][(dr)].start_band)
#define TRM_DOT_IS_BAND_IDX_PRESENT(rat,dds,dr,band_idx) (trm_dot.tech_list[(rat)][(dds)][(dr)].valid_band_mask & (1 << (band_idx)))
#define TRM_DOT_SET_BAND_IDX(rat,dds,dr,band_idx) (trm_dot.tech_list[(rat)][(dds)][(dr)].valid_band_mask |= (1 << (band_idx)))

typedef enum 
{
  TRM_DOT_RAT_1X,
  TRM_DOT_RAT_HDR,
  TRM_DOT_RAT_UMTS,
  TRM_DOT_RAT_GSM,
  TRM_DOT_RAT_TDSCDMA,
  TRM_DOT_RAT_LTE_FDD,
  TRM_DOT_RAT_LTE_TDD,
  TRM_DOT_RAT_MAX
}trm_dot_rat_enum_type;

typedef enum
{
  TRM_DOT_NON_DDS,
  TRM_DOT_DDS,  
  TRM_DOT_DDS_MAX
}trm_dot_dds_enum_type;

typedef enum
{
  TRM_DOT_DR_NOT_ALLOWED,
  TRM_DOT_DR_ALLOWED,
  TRM_DOT_DR_ALLOWED_MAX
}trm_dot_dr_allowed_enum_type;

/* Node for device list */
typedef struct trm_dot_dev_list_node_s
{
  /* Link */
  list_link_type        link;

  /* current device */
  rfm_device_enum_type  device;

  /* Ant num */
  uint32                antnum;

  /* may need pointer to associted device */

}trm_dot_dev_list_node_type;

/* Node for band list */
typedef struct
{
  trm_band_t band;

  rfm_device_enum_type  best_rfc_rx_dev;
  rfm_device_enum_type  best_rfc_tx_dev;

  rfm_device_enum_type  best_tx_dev;
  rfm_device_enum_type  best_rx_dev;
  list_type any_tx_dev_list;
  list_type any_rx_dev_list;
  list_type rfc_rx_dev_list;
  
  uint32 set_num;
  uint32 best_rx_set_num;
  uint8 band_group;
  uint8 set_idx;
  uint8 best_rx_set_idx;
}trm_dot_band_list_node_type;

/* Node for band list */
typedef struct
{
  trm_band_t band1;
  trm_band_t band2;

  list_type b1_any_rx_dev_list;
  list_type b2_any_rx_dev_list;

  list_type b1_best_rx_dev_list;

  /* dev list when dglna is applicable */
  list_type b2_dglna_ap_any_rx_dev_list;
   
  uint32 b1_any_set_idx;
  uint32 b1_best_set_idx;

  uint32  b2_any_set_num;
  uint32 b2_dglna_any_set_num;

  uint8 b2_any_set_idx;
  uint8 b2_dglna_any_set_idx;
}trm_dot_conc_band_list_node_type;

typedef struct
{
  uint64       valid_band_mask;
  trm_dot_band_list_node_type** band_list_ptr;
  trm_band_t   start_band;
  trm_band_t   end_band; 
  uint8        num_valid_bands;
}trm_dot_tech_info;

typedef struct
{
  trm_dot_conc_band_list_node_type** conc_band_list_ptr;
}trm_dot_conc_tech_info;

typedef struct
{
  /* LTE SCell Bands Info for 2DL case */
  sys_lte_band_mask_e_type scell_bands;

  /* LTE SCell Intra Band CA mask for 2DL case */
  uint8                    intra_band_ca_mask;
}trm_dot_scell_ca_band_info;

typedef struct
{
  rfm_devices_configuration_type  *rf_config;

#ifdef FEATURE_MCS_DOT_AG_ENABLED
  rfm_rfcard_dynamic_properties_type rfc_dyn_props;
#endif

  /* Mask for Devices supporting DGLNA */
  uint32  dglna_device_mask;

  /* List of all techs supported... */
  trm_dot_tech_info     tech_list[TRM_DOT_RAT_MAX][TRM_DOT_DDS_MAX][TRM_DOT_DR_ALLOWED_MAX];

  trm_dot_conc_tech_info  conc_tech_list[TRM_DOT_RAT_MAX][TRM_DOT_RAT_MAX][TRM_DOT_DDS_MAX];

  /* List of all devices supporting Rx */
  list_type  all_rx_list;

  /* List of all devices supporting Tx */
  list_type  all_tx_list;

  /* LTE PCell Bands Info for 2DL case */
  sys_lte_band_mask_e_type pcell_bands;
  
  /* LTE PCell Bands Info for 2DL case */
  trm_dot_scell_ca_band_info spcell_info[SYS_SBAND_LTE_EUTRAN_BAND_MAX];
  
#ifdef FEATURE_RF_NEW_CA_API_ONLY_SUPPORT_ULCA
  /* LTE Bands active Info for 3DL case */
  rflte_mc_rrc_ca_band_combos_s band_combo_3dl;
#else
  /* LTE Bands active Info for 3DL/2DL+2UL CA case */
  rflte_mc_bw_class_combos_s ca_list;
#endif /* FEATURE_RF_NEW_CA_API_ONLY_SUPPORT_ULCA */
}trm_dot_type;

boolean trm_dot_run();

uint32 trm_dot_get_ant_num
(
  trm_band_t band,
  rfm_device_enum_type rf_dev
);

void trm_dot_destroy_band_node
(
   trm_dot_tech_info *tech_info,
   uint32 band_idx
);

void trm_dot_destroy_device_list
(
  list_type *dev_list
);

#endif /* TRM_DOT_H */
