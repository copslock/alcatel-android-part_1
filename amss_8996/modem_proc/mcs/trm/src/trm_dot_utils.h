#ifndef TRM_DOT_UTILS_H
#define TRM_DOT_UTILS_H

/*===========================================================================

            T R M   D O T   C O N C U R R E N C Y   H E A D E R    F I L E

DESCRIPTION
   This file contains the declaration of TRM data structure.

  Copyright (c) 2014-2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.
===========================================================================*/


/*===========================================================================

                      EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/mcs/trm/src/trm_dot_utils.h#1 $

when         who     what, where, why
--------     ---     ----------------------------------------------------------
04/24/2015   ag      Added support for new table format and TDD UL CA card 
02/26/2015   mn      Initial version.
===========================================================================*/

/*============================================================================

                           INCLUDE FILES FOR MODULE

============================================================================*/

#include "customer.h"
#include "trm_dot_init.h"
#include "list.h"
#include "rfm_device_types.h"

/*============================================================================

                   DEFINITIONS AND DECLARATIONS FOR MODULE

This section contains local definitions for constants, macros, typesdefs,
and other items needed by this module.

============================================================================*/

trm_dot_rat_enum_type trm_get_rat_enum_from_band
(
  trm_band_t  band
);

void trm_dot_put_device
(
   list_type *dev_q, 
   trm_dot_dev_list_node_type *dev_node
);

int trm_dot_dev_q_compare_func
(
  void* dev_node, 
  void* compare_val
);

void trm_dot_srch_dev_and_move_to_top
(
  list_type *dev_list,
  rfm_device_enum_type dev
);

void trm_dot_move_dev_to_top
(
  list_type *dev_list,
  trm_dot_dev_list_node_type *dev_link
);

rfm_device_enum_type trm_dot_get_first_dev_from_list
(
  list_type  *dev_list
);

void trm_dot_copy_dev_lists
(
  list_type *src_q,
  list_type *dest_q
);

void trm_dot_swap_nodes
(
  list_type *dev_list,
  trm_dot_dev_list_node_type *node1,
  trm_dot_dev_list_node_type *node2
);

void trm_dot_move_dev_to_bottom
(
  list_type *dev_list,
  list_link_type *dev_node
);

uint32 trm_dot_get_dev_set_num(list_type *dev_list);

void trm_dot_get_rat_group_tdd_flag
(
  trm_dot_rat_enum_type tech,
  trm_rat_group_type  *rat_group,
  boolean *tdd_flag
);

trm_dot_dev_list_node_type* trm_dot_srch_dev_in_list
(
  list_type *dev_list,
  rfm_device_enum_type dev
);

void trm_dot_copy_list_values
(
  list_type *list1,
  list_type *list2
);

void trm_dot_retain_one_device
(
  list_type *dev_list
);

boolean trm_dot_check_if_same_wtr
(
  rfm_device_enum_type device1,
  rfm_device_enum_type device2
);

void trm_dot_remove_drx_devices 
(
  list_type *dev_list 
);

void trm_dot_apply_prxond_rule 
(
  list_type *dev_list
);

#endif /* TRM_DOT_UTILS_H */
