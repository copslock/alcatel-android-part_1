#ifndef TRM_DOT_CONCURRENCY_H
#define TRM_DOT_CONCURRENCY_H

/*===========================================================================

            T R M   D O T   C O N C U R R E N C Y   H E A D E R    F I L E

DESCRIPTION
   This file contains the declaration of TRM data structure.

  Copyright (c) 2014-2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.
===========================================================================*/


/*===========================================================================

                      EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/mcs/trm/src/trm_dot_concurrency.h#1 $

when         who     what, where, why
--------     ---     ----------------------------------------------------------
04/24/2015   ag      Added support for new table format and TDD UL CA card 
02/26/2015   mn      Initial version.
===========================================================================*/

/*============================================================================

                           INCLUDE FILES FOR MODULE

============================================================================*/

#include "customer.h"

#define TRM_DOT_GET_CONC_BAND_PTR(band_list_ptr, b1_idx, b2_idx, num_valid_bands2) ((band_list_ptr)+((b1_idx)*(num_valid_bands2))+(b2_idx))
/*============================================================================

                   DEFINITIONS AND DECLARATIONS FOR MODULE

This section contains local definitions for constants, macros, typesdefs,
and other items needed by this module.

============================================================================*/

void trm_dot_update_concurrency_tables(void);

void trm_dot_free_conc_tables(void);


#endif /* TRM_DOT_CONCURRENCY_H */
