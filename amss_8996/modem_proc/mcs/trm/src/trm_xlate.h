#ifndef TRM_XLATE_H
#define TRM_XLATE_H
/*===========================================================================

                   T R M    S T R U C T   H E A D E R    F I L E

DESCRIPTION
   This file contains the declaration of TRM data structure.

  Copyright (c) 2014 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.
===========================================================================*/


/*===========================================================================

                      EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/mcs/trm/src/trm_xlate.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
08/21/15   mn      Changes for LTE CA device hopping (CR: 894455).
07/07/15   mn      Inform rx_operation and as_id to RF (CR: 780579).
08/12/14   mn      Support for a unified IRAT mechanism which works across all 
                    TRM modes (CR: 705286).
04/11/14   mn      Refactoring TRM

===========================================================================*/

#include "customer.h"
#include "trmi.h"
#ifndef FEATURE_DISABLE_SRX_RF_VERIFICATION
#include "rflte_single_radio.h"
#endif /* FEATURE_DISABLE_SRX_RF_VERIFICATION */

/*===========================================================================

                              HEADER CONSTANTS

  Numeric values for the identifiers in this section should not be changed.

===========================================================================*/

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/*===========================================================================

                              FUNCTIONS

===========================================================================*/

/*============================================================================
FUNCTION TRM_GET_GRANT_EVENT_FROM_RF_DEVICE

DESCRIPTION
  Converts the rfm_device_enum_type to trm_grant_event_enum_t

DEPENDENCIES
  None

RETURN VALUE
  Grant event corresponding to the input chain.
  TRM_DENIED when conversion is not possible.

SIDE EFFECTS
  None
============================================================================*/
trm_grant_event_enum_t trm_get_grant_event_from_rf_device( rfm_device_enum_type chain );

/*============================================================================

FUNCTION TRM_GET_RF_DEVICE_FROM_GROUP

DESCRIPTION
  This is an internal API that can be used to convert a group to an RF device 

DEPENDENCIES
  None

RETURN VALUE
  RF device. RFM_INVALID device when group is INVALID.

SIDE EFFECTS
  None

============================================================================*/
rfm_device_enum_type trm_get_rf_device_from_group(trm_group c_id);

/*============================================================================

FUNCTION TRM_GET_GROUP_FROM_DEVICE

DESCRIPTION
  Returns the trm Group for corresponding rf device.
  
DEPENDENCIES
  None

RETURN VALUE
  trm_group

SIDE EFFECTS
  None

============================================================================*/
trm_group trm_get_group_from_device
(
  rfm_device_enum_type device
);

/*============================================================================

FUNCTION TRM_GET_RF_SUB_ID_FROM_AS_ID

DESCRIPTION
  Returns the rfm_device_sub_id_type for corresponding sys_modem_as_id_e_type.
  
DEPENDENCIES
  None

RETURN VALUE
  trm_group

SIDE EFFECTS
  None

============================================================================*/
rfm_device_sub_id_type trm_get_rf_sub_id_from_as_id
(
  trm_asid_mapping_return_data as_id
);

#ifndef FEATURE_DISABLE_SRX_RF_VERIFICATION
/*============================================================================

FUNCTION trm_get_rf_path_type_from_client

DESCRIPTION
  Returns the rflte_path_type for corresponding trm_client_enum_t.
  
DEPENDENCIES
  None

RETURN VALUE
  trm_group

SIDE EFFECTS
  None

============================================================================*/
rflte_path_type trm_get_rf_path_type_from_client( trm_client_enum_t client_id );

/*============================================================================

FUNCTION trm_get_mc_cell_idx_from_client_enum

DESCRIPTION
  Returns the rflte_mc_cell_idx_type for corresponding trm_client_enum_t.
  
DEPENDENCIES
  None

RETURN VALUE
  trm_group

SIDE EFFECTS
  None

============================================================================*/
rflte_mc_cell_idx_type trm_get_mc_cell_idx_from_client_enum( trm_client_enum_t client_id );
#endif /* FEATURE_DISABLE_SRX_RF_VERIFICATION */

/*============================================================================

FUNCTION trm_get_carrier_idx_from_client_enum

DESCRIPTION
  Returns the trm_carrier_idx_enum_type for corresponding trm_client_enum_t.
  
DEPENDENCIES
  None

RETURN VALUE
  trm_group

SIDE EFFECTS
  None

============================================================================*/
trm_carrier_idx_enum_type trm_get_carrier_idx_from_client_enum( trm_client_enum_t client_id );

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* TRM_XLATE_H */
