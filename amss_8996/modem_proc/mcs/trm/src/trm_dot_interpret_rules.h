#ifndef TRM_DOT_INTERPRET_RULES_H
#define TRM_DOT_INTERPRET_RULES_H

/*===========================================================================

            T R M   D O T   C O N C U R R E N C Y   H E A D E R    F I L E

DESCRIPTION
   This file contains the declaration of TRM data structure.

  Copyright (c) 2014-2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.
===========================================================================*/


/*===========================================================================

                      EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/mcs/trm/src/trm_dot_interpret_rules.h#1 $

when         who     what, where, why
--------     ---     ----------------------------------------------------------
04/24/2015   ag      Added support for new table format and TDD UL CA card 
02/26/2015   mn      Initial version.
===========================================================================*/

/*============================================================================

                           INCLUDE FILES FOR MODULE

============================================================================*/

#include "customer.h"
#include "queue.h"
#include "rfm_device_types.h"
#include "trm.h"

/*============================================================================

                   DEFINITIONS AND DECLARATIONS FOR MODULE

This section contains local definitions for constants, macros, typesdefs,
and other items needed by this module.

============================================================================*/
void trm_dot_apply_rule_preferred_rf_dev
(
  list_type   *dev_list,
  rfm_device_enum_type pref_dev
);

void trm_dot_apply_rule_same_antenna
(
  list_type *dev_list,
  rfm_device_enum_type tx_dev,
  trm_band_t band
);

void trm_dot_apply_rule_same_wtr
(
  list_type *dev_list,
  rfm_device_enum_type tx_dev
);

boolean trm_dot_chk_single_rat_restr_for_devices
(
  rfm_device_enum_type dev1,
  rfm_device_enum_type dev2
);

void trm_dot_apply_rule_use_dglna
( 
  list_type *dev_list
);

#endif /* TRM_DOT_INTERPRET_RULES_H */
