#ifndef TRM_DOT_CA_H
#define TRM_DOT_CA_H

/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*==

           T R A N S C E I V E R   R E S O U R C E   M A N A G E R

              Transceiver Resource Manager Internal Header File

GENERAL DESCRIPTION

  This file provides some common definitions for trm_dot_ca.cpp


EXTERNALIZED FUNCTIONS

  None


REGIONAL FUNCTIONS

  None


INITIALIZATION AND SEQUENCING REQUIREMENTS

  None


  Copyright (c) 2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

=*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*==



===============================================================================

                           EDIT HISTORY FOR MODULE

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/mcs/trm/src/trm_dot_ca.h#1 $

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

when         who     what, where, why
----------   ---     ----------------------------------------------------------
08/21/2015   mn      Changes for LTE CA device hopping (CR: 894455).
06/23/2015   rj      Add support for W CA Device Ordering
06/02/2015   rj      Intial Version

=============================================================================*/

/*=============================================================================

                           INCLUDE FILES FOR MODULE

=============================================================================*/

#include "customer.h"
#include "modem_mcs_defs.h"
#include "trm_dot_init.h"
#include "list.h"
#include "trm.h"
#include "trm_device_ordering.h"


/*=============================================================================

                   DEFINITIONS AND DECLARATIONS FOR MODULE

This section contains local definitions for constants, macros, typesdefs,
and other items needed by this module.

=============================================================================*/


/*----------------------------------------------------------------------------
  TRM DOT Intra-Band Contiguous/non-Contiguous CA Type
----------------------------------------------------------------------------*/
typedef enum
{
  TRM_DOT_INTRA_BAND_CA_TYPE_INVALID = -1,

  /* Bit 0 set if Intra Band Non-Contiguous CA is supported */
  TRM_DOT_INTRA_BAND_NON_CONTIGUOUS_CA = 0,

  /* Bit 1 set if Intra Band 20MHz Contiguous CA supported */
  TRM_DOT_INTRA_BAND_20MHz_CONTIGUOUS_CA,

  /* Bit 2 set if Intra Band 40MHz Contiguous CA supported */
  TRM_DOT_INTRA_BAND_40MHz_CONTIGUOUS_CA,

  /* Bit 3 set if Intra Band 60MHz Contiguous CA supported */
  TRM_DOT_INTRA_BAND_60MHz_CONTIGUOUS_CA,

  TRM_DOT_INTRA_BAND_CA_TYPE_MAX
}trm_dot_intra_band_ca_enum_type;


/*----------------------------------------------------------------------------
  TRM Device Ordering for CA
----------------------------------------------------------------------------*/
typedef struct
{
  /* Pointer to next node in this linked list */
  list_link_type               list_ptr;

  boolean                      is_2Dl_node;
  trm_band_t                   pcell_band;
  trm_band_t                   scell1_band;
  trm_band_t                   scell2_band;
  trm_dot_ca_assoc_enum_type   asso_enum;
  trm_dot_dr_allowed_enum_type dr_mode;
  boolean                      is_2Ul_node; /* Whether SCell1 Tx present */

  rfm_device_enum_type         pCellTxDev;
  rfm_device_enum_type         pCellRxDev;
  rfm_device_enum_type         sCell1TxDev;
  rfm_device_enum_type         sCell1RxDev;
  rfm_device_enum_type         sCell2RxDev;


  list_type                  pcell_tx_list;
  list_type                  pcell_rx_list;
  list_type                  scell1_tx_list;
  list_type                  scell1_rx_list;
  list_type                  scell2_rx_list;
} trm_dot_ca_type;

/*----------------------------------------------------------------------------
  TRM CRAT Concurrency Priority Info
----------------------------------------------------------------------------*/

typedef struct TrmDotCaList
{
  /* Pointer to next node in this linked list */
  list_type    list;

  /* Linked list for WCDMA CA combinations */
  list_type    list_umts_ca;

  /* Array of rfm_device_enum_type & Max LTE Bands */
  uint32 dotCaMaxBwKhz[TRM_MAX_VALID_CHAINS][TRM_DOT_LTE_MAX_NUM_OF_BANDS];

  /* This is for Buffer list to save LTE/UMTS nodes */
  uint32 maxLteNodes;
  trm_ca_dot_type ca_tbl_ptr[50];
  uint32 maxUmtsNodes;
  trm_ca_dot_type umts_ca_tbl_ptr[5];
}trm_dot_ca_list;

/*============================================================================

FUNCTION TRM_DOT_CA_INIT

DESCRIPTION
  This API is to initialize Device Ordering for CA clients
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/
void trm_dot_ca_init( void );

/*============================================================================

FUNCTION TRM_DOT_CA_GET_CA_TBL

DESCRIPTION
  This function is for getting CA Table 
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/
uint32 trm_dot_get_ca_tbl
(
  trm_ca_dot_type **ca_tbl_ptr
);

#endif /* TRM_DOT_CA_H */
