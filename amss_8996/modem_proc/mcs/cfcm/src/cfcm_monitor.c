/*!
  @file
  cfcm_monitor.c

  @brief
  CFCM monitor related implementation.

  @detail
  OPTIONAL detailed description of this C module.
  - DELETE this section if unused.

  @author
  rohitj

*/

/*==============================================================================

  Copyright (c) 2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

==============================================================================*/

/*==============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/mcs/cfcm/src/cfcm_monitor.c#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------- 
10/22/15   jm      Updated monitor support in DSM Large/Dup Pool for ML1 CPU client
08/13/15   rj      Avoid repeated registration with NPA node for same monitor
07/01/15   rj      Don't register with DSM twice for same level
06/24/15   rj      Fix for not ignoring reg by two clients for same monitor
06/17/15   rj      Adding support for PDCP COMPRESSION feature for LTE
05/19/15   rj      Support stepTimer value for CMD_OFF
05/04/15   rj      CFCM re-Design changes
04/07/15   rj      Added support in Diag cmd for MDMTemp/Vdd monitor
02/18/15   sg      Added support for new QSH framework
12/15/14   sg      QSH related modifications
12/11/14   rj      New Monitors in DSM Large and Dup Pool
11/21/14   rj      Adding MDM_TEMP and VDD_PEAK_CURR monitors
11/20/14   rj      Log Packet support added in CFCM
10/29/14   rj      Adding Bootup crash fix
10/27/14   rj      Adding QTF support for CFCM
10/17/14   rj      Added supoort for Thermal RunAway Monitor
10/09/14   rj      CFCM changes to Process monitor's input in its task context
09/15/14   rj      Include only those monitors in mask which have triggered CFCM cmd
09/08/14   rj      Added support for DSM monitor for IPA client
08/20/14   mm      Fixing call to qsh_invoke_cb()
09/04/14   rj      Ignore FREEZE Command if last Command sent is OFF.
08/11/14   rj      Adding support for QSH in CFCM
08/07/14   rj      Update monitor state for all clients, even when its not reg.
07/28/14   rj      Updating F3s to debugging
07/15/14   rj      Added support for QTF compilation issue
07/07/14   rj      Added support for dsm_reg_mem_event_optimized_level()
07/03/14   rj      BUS BW changes
04/07/14   rj      initial version
==============================================================================*/

/*==============================================================================

                           INCLUDE FILES

==============================================================================*/

#include <customer.h>
#include <comdef.h>
#include <msg.h>
#include <dsm_pool.h>
#include <dsm_init.h>
#include "cfcm_cfg.h"
#include "cfcm_msg.h"
#include "cfcmi.h"
#include "cfcm_monitor.h"
#include "cfcm_client.h"
#include "cfcm_dbg.h"
#include "cfcm_cpu_monitor.h"
#include "cfcm_msgr.h"
#include "cfcm_qsh.h"
#include "cfcm_log.h"
#include <npa.h>

/*==============================================================================

                   INTERNAL DEFINITIONS AND TYPES

==============================================================================*/

/*! @brief whether the monitor is turned on in the mask
*/
#define CFCM_MONITOR_IN_MASK(mask, monitor)    (((mask) & (1 << (monitor))) != 0)

/*! @brief history buffer size
*/
#define CFCM_MONITOR_HIST_BUF_SZ                                         (1 << 4)

/*! @brief history buffer index mask
*/
#define CFCM_MONITOR_HIST_BUF_IDX_MASK              (CFCM_MONITOR_HIST_BUF_SZ - 1)

/*! @brief BUS BW Default Level setting 
*/
#define CFCM_MONITOR_DEFAULT_BUS_BW_LEVEL              (-1)

/*! @brief CFCM Maximum number of states for Thermal PA/Runaway/MDM Temp/VDD Peak Curr monitor
*/
#define CFCM_MONITOR_MAX_NPA_NODE_STATE                (4)

/*! @brief Whether New command should be ignored based on the last command sent to clients 
*/
#define CFCM_MONITOR_IS_IGNORE_NEW_CMD(last_cmd, new_cmd) (((last_cmd) == CFCM_CMD_FC_OFF) && \
                                                          (((new_cmd) == CFCM_CMD_UP)||((new_cmd) == CFCM_CMD_FREEZE)))

#define CFCM_MONITOR_IS_STEP_TIMER_VALID_CMDS(cmd) (((cmd) == CFCM_CMD_FC_OFF) || ((cmd) == CFCM_CMD_UP)||((cmd) == CFCM_CMD_DOWN))


/* Mapping of Runaway thermal state to CFCM commands interpretation */
static const cfcm_cmd_e cfcm_npa_node_state_map[CFCM_MONITOR_MAX_NPA_NODE_STATE]=
{

  /* State 0 ,  <MONITOR>         */ CFCM_CMD_FC_OFF,

  /* State 1 ,  <MONITOR>         */ CFCM_CMD_DOWN,

  /* State 2 ,  <MONITOR>         */ CFCM_CMD_DOWN,

  /* State 3 ,  <MONITOR>         */ CFCM_CMD_DOWN,
};

/* Mapping of PA thermal state to CFCM commands interpretation */
static const cfcm_cmd_e cfcm_shim_thermal_pa_state_map[4][3] =
{

  {
    /* State 0 ,  MONITOR_THERMAL_PA         */ CFCM_CMD_UP,
    /* State 0 ,  MONITOR_THERMAL_PA_EX   */ CFCM_CMD_UP,
    /* State 0 ,  MONITOR_THERMAL_PA_EM  */ CFCM_CMD_FC_OFF,
  },

  {
    /* State 1 ,  MONITOR_THERMAL_PA         */ CFCM_CMD_DOWN,
    /* State 1 ,  MONITOR_THERMAL_PA_EX   */ CFCM_CMD_UP,
    /* State 1 ,  MONITOR_THERMAL_PA_EM  */ CFCM_CMD_FREEZE,
  },

  {
    /* State 2 ,  MONITOR_THERMAL_PA         */ CFCM_CMD_DOWN,
    /* State 2 ,  MONITOR_THERMAL_PA_EX   */ CFCM_CMD_DOWN,
    /* State 2 ,  MONITOR_THERMAL_PA_EM  */ CFCM_CMD_FREEZE,
  },

  {
    /* State 3 ,  MONITOR_THERMAL_PA         */ CFCM_CMD_SHUT_DOWN,
    /* State 3 ,  MONITOR_THERMAL_PA_EX   */ CFCM_CMD_DOWN,
    /* State 3 ,  MONITOR_THERMAL_PA_EM  */ CFCM_CMD_SHUT_DOWN,
  }
};

/* Mapping of Runaway thermal state to CFCM commands interpretation */
static const cfcm_cmd_e cfcm_shim_npa_node_state_map[CFCM_MONITOR_MAX_NPA_NODE_STATE][2] =
{

  {
    /* State 0 ,  <MONITOR>         */ CFCM_CMD_UP,
    /* State 0 ,  <MONITOR>_EX   */ CFCM_CMD_UP,
  },

  {
    /* State 1 ,  <MONITOR>         */ CFCM_CMD_DOWN,
    /* State 1 ,  <MONITOR>_EX   */ CFCM_CMD_UP,
  },

  {
    /* State 2 ,  <MONITOR>         */ CFCM_CMD_DOWN,
    /* State 2 ,  <MONITOR>_EX   */ CFCM_CMD_DOWN,
  },

  {
    /* State 3 ,  <MONITOR>         */ CFCM_CMD_SHUT_DOWN,
    /* State 3 ,  <MONITOR>_EX   */ CFCM_CMD_SHUT_DOWN,
  }
};

/*! @brief String representation of monitor enum */
const char * cfcm_monitor_str[CFCM_MONITOR_MAX] =
{
  "THERMAL_PA",
  "CPU",
  "DSM_LARGE_POOL",
  "DSM_DUP_POOL",
  "DSM_SMALL_POOL",
  "BW_THROTTLING",
  "THERMAL_CX",
  "MDM_TEMP",
  "VDD_PEAK_CURR",
  "DSM_LARGE_POOL_LEVEL1"
};

/*! @brief String representation of monitor enum */
const char * cfcm_cmd_str[CFCM_CMD_MAX] =
{
  "FC_OFF",
  "FC_UP",
  "FC_FREEZE",
  "FC_DOWN",
  "FC_SET_MIN",
  "FC_SHUT_DOWN",
  "FC_SET_VALUE",
  "UNKNOWN"
};

/*! @brief REQUIRED one-sentence brief description of this structure typedef
*/
typedef struct
{
  uint32  num_state_change;              /*!< total number of state change */
  uint32          num_down;              /*!< total number of down cmds */
  uint32          num_set_min;           /*!< total number of set min cmds */
  uint32          num_shut_down;         /*!< total number of shut down cmds */
  cfcm_timetick_ms_t  time_down;         /*!< time last down cmd sent */
  cfcm_timetick_ms_t  time_set_min;      /*!< time last set min cmd sent */
  cfcm_timetick_ms_t  time_shut_down;    /*!< time last shut down cmd sent */
} cfcm_monitor_stats_s;

/*! @brief: QMI register data info
*/
typedef struct
{
  qmi_csi_service_handle     *service_handle;
  qmi_idl_service_object_type service_object;
  qmi_cfcm_bus_bw_throttle_level_enum_type_v01 qmi_bw_lvl;
} cfcm_monitor_qmi_data_s;

/*! @brief flow control command that is sent by the CFCM
*/
typedef struct
{
  boolean              cmd_updated; /*!< Is this command updated due to monitor input */

  cfcm_cmd_e           cmd;        /*!< flow control command */

  /* Union of monitor data sent */
  cfcm_monitor_data     monitor_data;

} cfcm_monitor_last_cmd_type_s;

/*! @brief cfcm monitor information structure
*/
typedef struct
{
  cfcm_monitor_e monitor_id;  /*!< for ease of debugging */
  uint32        num_client; /*!< number of clients  */
  cfcm_client_e  client[CFCM_CLIENT_MAX];/*!<clients to respond to the monitor */
  cfcm_monitor_last_cmd_type_s  last_cmd[CFCM_CLIENT_MAX]; /*!<  last cmd from this monitor per client */
  boolean       registered[CFCM_CLIENT_MAX]; /*!< registered with the source monitor */
  cfcm_monitor_stats_s stats; /*!< statistics for the monitor */
  uint32        latest_cmd_hist_idx;/*!< latest command history buffer index */ 
  cfcm_cmd_e     cmd_hist[CFCM_MONITOR_HIST_BUF_SZ];  /*!< command history buffer */
  cfcm_client_e     client_hist[CFCM_MONITOR_HIST_BUF_SZ];  /*!< client's command history buffer */
} cfcm_monitor_info_s;

/*! @brief top level structure for cfcm_monitor
*/
typedef struct
{
  cfcm_monitor_info_s  monitors[CFCM_MONITOR_MAX];/*!<info for all known monitor*/
  uint32              num_errors;               /*!< total number of errors */
  cfcm_monitor_qmi_data_s      qmi_data;    /*!< Monitor's specific data */
} cfcm_monitor_s;

/*==============================================================================

                         LOCAL VARIABLES

==============================================================================*/

STATIC cfcm_monitor_s cfcm_monitor;
STATIC cfcm_monitor_s* const cfcm_monitor_ptr = &cfcm_monitor;
/*==============================================================================

                    INTERNAL FUNCTION PROTOTYPES

==============================================================================*/
/*==============================================================================

  FUNCTION:  cfcm_monitor_proc_npa_change

==============================================================================*/
/*!
    @brief
    handles NPA state change for thermal.

    @return
    None
*/
/*============================================================================*/
static void cfcm_monitor_proc_npa_change
(
  npa_resource_state state   /*!< new NPA state  */
)
{
  cfcm_monitor_ind_msg_s  msg;
  errno_enum_type         send_status;
  /*--------------------------------------------------------------------------*/

  /*--------------------------------------------------------------------------*/

  CFCM_MSG_1(HIGH, "CFCM thermal monitor cfcm_monitor_proc_npa_change state %d", state);

  msg.monitor_id = CFCM_MONITOR_THERMAL_PA;
  msg.monitor_data.npa_node.state = (uint32)state;

  send_status = cfcm_msgr_send_msg(&msg.hdr,
                             MCS_CFCM_MONITOR_IND,
                             sizeof(cfcm_monitor_ind_msg_s));
  CFCM_ASSERT(send_status == E_SUCCESS);

} /* cfcm_monitor_proc_npa_change() */


/*==============================================================================

  FUNCTION:  cfcm_monitor_npa_event_cb

==============================================================================*/
/*!
    @brief
    NPA event change handler.

    @return
    None
*/
/*============================================================================*/
static void cfcm_monitor_npa_event_cb
(
  void         *context,    /*!< context for the event */
  unsigned int  event_type, /*!< event type triggered the callback */
  void         *data,       /*!< pointer to data */
  unsigned int  data_size   /*!< size of data */
)
{
  /*--------------------------------------------------------------------------*/

  /*--------------------------------------------------------------------------*/
  npa_event_data *event_data_ptr = (npa_event_data*)data;

  cfcm_monitor_proc_npa_change(event_data_ptr->state);

} /* cfcm_monitor_npa_event_cb() */



/*==============================================================================

  FUNCTION:  cfcm_monitor_npa_available_cb

==============================================================================*/
/*!
    @brief
    callback function when npa is avaiable.

    @return
    None

*/
/*============================================================================*/
void cfcm_monitor_npa_available_cb 
(
  void         *context,    /*!< context for the event */
  unsigned int  event_type, /*!< event type triggered the callback */
  void         *data,       /*!< pointer to data */
  unsigned int  data_size   /*!< size of data */
)
{
  npa_event_handle event_client;
  npa_query_type query_result;
  npa_query_status query_status;
  /*--------------------------------------------------------------------------*/

  /*--------------------------------------------------------------------------*/

  event_client = npa_create_change_event_cb(CFCM_MONITOR_THERMAL_PA_NPA_NODE,
                                           "PA Thermal CFCM",
                                           cfcm_monitor_npa_event_cb,
                                           NULL);
  CFCM_ASSERT(event_client != NULL);

  CFCM_MSG_0(HIGH, "CFCM thermal monitor npa_create_change_event_cb done");

  query_status = npa_query_by_event(event_client,
                                    NPA_QUERY_CURRENT_STATE,
                                    &query_result);

  CFCM_ASSERT(query_status == NPA_QUERY_SUCCESS);

  cfcm_monitor_proc_npa_change(query_result.data.state);

} /* cfcm_monitor_npa_available_cb() */


/*==============================================================================

  FUNCTION:  cfcm_monitor_proc_thermal_ra_change

==============================================================================*/
/*!
    @brief
    handles NPA state change for thermal Runaway monitor.

    @return
    None
*/
/*============================================================================*/
static void cfcm_monitor_proc_thermal_ra_change
(
  npa_resource_state state   /*!< new NPA state  */
)
{
  cfcm_monitor_ind_msg_s  msg;
  errno_enum_type         send_status;
  /*--------------------------------------------------------------------------*/

  /*--------------------------------------------------------------------------*/

  CFCM_MSG_1(HIGH, "CFCM thermal monitor cfcm_monitor_proc_thermal_ra_change state %d", state);

  msg.monitor_id = CFCM_MONITOR_THERMAL_CX;
  msg.monitor_data.npa_node.state = (uint32)state;

  send_status = cfcm_msgr_send_msg(&msg.hdr,
                             MCS_CFCM_MONITOR_IND,
                             sizeof(cfcm_monitor_ind_msg_s));
  CFCM_ASSERT(send_status == E_SUCCESS);

} /* cfcm_monitor_proc_thermal_ra_change() */



/*==============================================================================

  FUNCTION:  cfcm_monitor_thermal_ra_event_cb

==============================================================================*/
/*!
    @brief
    NPA event change handler for Thermal Runaway monitor.

    @return
    None
*/
/*============================================================================*/
static void cfcm_monitor_thermal_ra_event_cb
(
  void         *context,    /*!< context for the event */
  unsigned int  event_type, /*!< event type triggered the callback */
  void         *data,       /*!< pointer to data */
  unsigned int  data_size   /*!< size of data */
)
{
  /*--------------------------------------------------------------------------*/

  /*--------------------------------------------------------------------------*/
  npa_event_data *event_data_ptr = (npa_event_data*)data;

  cfcm_monitor_proc_thermal_ra_change(event_data_ptr->state);

} /* cfcm_monitor_thermal_ra_event_cb() */



/*==============================================================================

  FUNCTION:  cfcm_monitor_thermal_ra_available_cb

==============================================================================*/
/*!
    @brief
    callback function when npa is avaiable.

    @return
    None

*/
/*============================================================================*/
void cfcm_monitor_thermal_ra_available_cb 
(
  void         *context,    /*!< context for the event */
  unsigned int  event_type, /*!< event type triggered the callback */
  void         *data,       /*!< pointer to data */
  unsigned int  data_size   /*!< size of data */
)
{
  npa_event_handle event_client;
  npa_query_type query_result;
  npa_query_status query_status;
  /*--------------------------------------------------------------------------*/

  /*--------------------------------------------------------------------------*/

  event_client = npa_create_change_event_cb(CFCM_MONITOR_THERMAL_RA_NPA_NODE,
                                           "PA Thermal CFCM",
                                           cfcm_monitor_thermal_ra_event_cb,
                                           NULL);
  CFCM_ASSERT(event_client != NULL);

  CFCM_MSG_0(HIGH, "CFCM thermal Runaway monitor npa_create_change_event_cb done");

  query_status = npa_query_by_event(event_client,
                                    NPA_QUERY_CURRENT_STATE,
                                    &query_result);

  CFCM_ASSERT(query_status == NPA_QUERY_SUCCESS);

  cfcm_monitor_proc_thermal_ra_change(query_result.data.state);

} /* cfcm_monitor_thermal_ra_available_cb() */

/*==============================================================================

  FUNCTION:  cfcm_monitor_proc_mdm_temp_change

==============================================================================*/
/*!
    @brief
    handles NPA state change for MDM Temperature monitor.

    @return
    None
*/
/*============================================================================*/
static void cfcm_monitor_proc_mdm_temp_change
(
  npa_resource_state state   /*!< new NPA state  */
)
{
  cfcm_monitor_ind_msg_s  msg;
  errno_enum_type         send_status;
  /*--------------------------------------------------------------------------*/

  /*--------------------------------------------------------------------------*/

  CFCM_MSG_1(HIGH, "CFCM thermal monitor cfcm_monitor_proc_mdm_temp_change state %d", state);

  msg.monitor_id = CFCM_MONITOR_MDM_TEMP;
  msg.monitor_data.npa_node.state = (uint32)state;

  send_status = cfcm_msgr_send_msg(&msg.hdr,
                             MCS_CFCM_MONITOR_IND,
                             sizeof(cfcm_monitor_ind_msg_s));
  CFCM_ASSERT(send_status == E_SUCCESS);

} /* cfcm_monitor_proc_mdm_temp_change() */



/*==============================================================================

  FUNCTION:  cfcm_monitor_mdm_temp_event_cb

==============================================================================*/
/*!
    @brief
    NPA event change handler for MDM Temp monitor.

    @return
    None
*/
/*============================================================================*/
static void cfcm_monitor_mdm_temp_event_cb
(
  void         *context,    /*!< context for the event */
  unsigned int  event_type, /*!< event type triggered the callback */
  void         *data,       /*!< pointer to data */
  unsigned int  data_size   /*!< size of data */
)
{
  /*--------------------------------------------------------------------------*/

  /*--------------------------------------------------------------------------*/
  npa_event_data *event_data_ptr = (npa_event_data*)data;

  cfcm_monitor_proc_mdm_temp_change(event_data_ptr->state);

} /* cfcm_monitor_mdm_temp_event_cb() */



/*==============================================================================

  FUNCTION:  cfcm_monitor_mdm_temp_available_cb

==============================================================================*/
/*!
    @brief
    callback function when npa is avaiable.

    @return
    None

*/
/*============================================================================*/
void cfcm_monitor_mdm_temp_available_cb 
(
  void         *context,    /*!< context for the event */
  unsigned int  event_type, /*!< event type triggered the callback */
  void         *data,       /*!< pointer to data */
  unsigned int  data_size   /*!< size of data */
)
{
  npa_event_handle event_client;
  npa_query_type query_result;
  npa_query_status query_status;
  /*--------------------------------------------------------------------------*/

  /*--------------------------------------------------------------------------*/

  event_client = npa_create_change_event_cb(CFCM_MONITOR_MDM_TEMP_NPA_NODE,
                                           "MDM Temp CFCM",
                                           cfcm_monitor_mdm_temp_event_cb,
                                           NULL);
  CFCM_ASSERT(event_client != NULL);

  CFCM_MSG_0(HIGH, "CFCM MDM Temp monitor npa_create_change_event_cb done");

  query_status = npa_query_by_event(event_client,
                                    NPA_QUERY_CURRENT_STATE,
                                    &query_result);

  CFCM_ASSERT(query_status == NPA_QUERY_SUCCESS);

  cfcm_monitor_proc_mdm_temp_change(query_result.data.state);

} /* cfcm_monitor_mdm_temp_available_cb() */

/*==============================================================================

  FUNCTION:  cfcm_monitor_proc_vdd_peak_curr_change

==============================================================================*/
/*!
    @brief
    handles NPA state change for VDD Peak Current monitor.

    @return
    None
*/
/*============================================================================*/
static void cfcm_monitor_proc_vdd_peak_curr_change
(
  npa_resource_state state   /*!< new NPA state  */
)
{
  cfcm_monitor_ind_msg_s  msg;
  errno_enum_type         send_status;
  /*--------------------------------------------------------------------------*/

  /*--------------------------------------------------------------------------*/

  CFCM_MSG_1(HIGH, "CFCM thermal monitor cfcm_monitor_proc_vdd_peak_curr_change state %d", state);

  msg.monitor_id = CFCM_MONITOR_VDD_PEAK_CURR_EST;
  msg.monitor_data.npa_node.state = (uint32)state;

  send_status = cfcm_msgr_send_msg(&msg.hdr,
                             MCS_CFCM_MONITOR_IND,
                             sizeof(cfcm_monitor_ind_msg_s));
  CFCM_ASSERT(send_status == E_SUCCESS);

} /* cfcm_monitor_proc_vdd_peak_curr_change() */



/*==============================================================================

  FUNCTION:  cfcm_monitor_vdd_peak_curr_event_cb

==============================================================================*/
/*!
    @brief
    NPA event change handler for VDD Peak Current monitor.

    @return
    None
*/
/*============================================================================*/
static void cfcm_monitor_vdd_peak_curr_event_cb
(
  void         *context,    /*!< context for the event */
  unsigned int  event_type, /*!< event type triggered the callback */
  void         *data,       /*!< pointer to data */
  unsigned int  data_size   /*!< size of data */
)
{
  /*--------------------------------------------------------------------------*/

  /*--------------------------------------------------------------------------*/
  npa_event_data *event_data_ptr = (npa_event_data*)data;

  cfcm_monitor_proc_vdd_peak_curr_change(event_data_ptr->state);

} /* cfcm_monitor_vdd_peak_curr_event_cb() */



/*==============================================================================

  FUNCTION:  cfcm_monitor_vdd_peak_curr_available_cb

==============================================================================*/
/*!
    @brief
    callback function when npa is avaiable.

    @return
    None

*/
/*============================================================================*/
void cfcm_monitor_vdd_peak_curr_available_cb 
(
  void         *context,    /*!< context for the event */
  unsigned int  event_type, /*!< event type triggered the callback */
  void         *data,       /*!< pointer to data */
  unsigned int  data_size   /*!< size of data */
)
{
  npa_event_handle event_client;
  npa_query_type query_result;
  npa_query_status query_status;
  /*--------------------------------------------------------------------------*/

  /*--------------------------------------------------------------------------*/

  event_client = npa_create_change_event_cb(CFCM_MONITOR_VDD_PEAK_CURR_NPA_NODE,
                                           "VDD Peak Curr CFCM",
                                           cfcm_monitor_vdd_peak_curr_event_cb,
                                           NULL);
  CFCM_ASSERT(event_client != NULL);

  CFCM_MSG_0(HIGH, "CFCM VDD Peak Current monitor npa_create_change_event_cb done");

  query_status = npa_query_by_event(event_client,
                                    NPA_QUERY_CURRENT_STATE,
                                    &query_result);

  CFCM_ASSERT(query_status == NPA_QUERY_SUCCESS);

  cfcm_monitor_proc_vdd_peak_curr_change(query_result.data.state);

} /* cfcm_monitor_vdd_peak_curr_available_cb() */



/*==============================================================================

  FUNCTION:  cfcm_monitor_get_last_cmd_updated

==============================================================================*/
/*!
    @brief
    Return whether Last command for a client (monitor) is updated

    @return
    TRUE/FALSE
*/
/*============================================================================*/
boolean cfcm_monitor_get_last_cmd_updated
( 
  cfcm_client_e           client_id,
  cfcm_monitor_e          monitor_id  /*!< monitor id */
)
{
  boolean cmd_updated = FALSE;

  /*--------------------------------------------------------------------------*/

  /*--------------------------------------------------------------------------*/
  cmd_updated = cfcm_monitor_ptr->monitors[monitor_id].last_cmd[client_id].cmd_updated;
  CFCM_MSG_3(HIGH, "cfcm_monitor_get_last_cmd_updated %d, client %d, monitor %d", cmd_updated, client_id, monitor_id);
  return cmd_updated;

} /* cfcm_monitor_reg_with_vdd_peak_curr() */

/*==============================================================================

  FUNCTION:  cfcm_process_bus_bw_input

==============================================================================*/
/*!
    @brief
    Handles Bus BW Throttling level input.

    @return
    None
*/
/*============================================================================*/
static void cfcm_process_bus_bw_input( void )
{
  qmi_cfcm_bus_bw_throttle_level_enum_type_v01 bw_lvl;
  cfcm_monitor_ind_msg_s  msg;
  errno_enum_type         send_status;
  /*--------------------------------------------------------------------------*/

  /*--------------------------------------------------------------------------*/

  bw_lvl = cfcm_monitor_ptr->qmi_data.qmi_bw_lvl;
  
  if ((bw_lvl < QMI_CFCM_BUS_BW_NO_THROTTLE_V01) || (bw_lvl > QMI_CFCM_BUS_BW_CRITICAL_THROTTLE_V01))
  {
    CFCM_MSG_1(ERROR, "::CFCM:: cfcm_process_bus_bw_input invalid Input %d", bw_lvl);
    return;
  }

  msg.monitor_id = CFCM_MONITOR_BW_THROTTLING;
  msg.monitor_data.bw.level = (uint8)bw_lvl;

  send_status = cfcm_msgr_send_msg(&msg.hdr,
                             MCS_CFCM_MONITOR_IND,
                             sizeof(cfcm_monitor_ind_msg_s));
  CFCM_ASSERT(send_status == E_SUCCESS);

} /* cfcm_process_bus_bw_input() */
/*=============================================================================

  FUNCTION:  cfcm_qmi_process_req

=============================================================================*/
/*!
    @brief
        Callback function called by QCSI infrastructure when a REQ message to
        CFCM is received
 
    @note
        QCSI infrastructure decodes the data before forwarding it to this layer
 
    @return
     qmi_csi_cb_error
*/
/*===========================================================================*/

qmi_csi_cb_error cfcm_qmi_process_req (
  void           *connection_handle,
  qmi_req_handle  req_handle,
  unsigned int    msg_id,
  void           *req_c_struct,
  unsigned int    req_c_struct_len,
  void           *service_cookie
)
{
  qmi_csi_cb_error      req_cb_retval    = QMI_CSI_CB_NO_ERR;
  qmi_response_type_v01 response;
  qmi_cfcm_bus_bw_throttle_level_enum_type_v01 rcvd_qmi_bw_lvl, old_bw_lvl;
  cfcm_get_bus_bw_throttling_level_resp_msg_v01 get_resp;
  /*-----------------------------------------------------------------------*/

  /* Process message appropriately based on QMI msg_id */
  switch( msg_id )
  {
    /* This request provides the updated Throttling level */
    case QMI_CFCM_BUS_BW_THROTTLING_LEVEL_REQ_MSG_V01:
      {
        /* Store current BW Level locally */
        old_bw_lvl = cfcm_monitor_ptr->qmi_data.qmi_bw_lvl;

        /* Store the new BW Level locally */
        rcvd_qmi_bw_lvl = 
          ((cfcm_set_bus_bw_throttling_level_req_msg_v01 *)req_c_struct)->bus_bw_throttling_level;

         /* Initialize structure holding response */
         memset( &response, 0, sizeof( cfcm_set_bus_bw_throttling_level_resp_msg_v01 ) );

        /* If the BW Level is not within limits, respond with failure... */
        if (!CFCM_IS_BUS_BW_LEVEL_VALID(rcvd_qmi_bw_lvl))
        {
          response.result = QMI_RESULT_FAILURE_V01;
          response.error = QMI_ERR_INVALID_ARG_V01;
        }
        else
        {
          /* Update Bus BW Level */
          cfcm_monitor_ptr->qmi_data.qmi_bw_lvl = rcvd_qmi_bw_lvl;

          /* Send a success response */
          response.result = QMI_RESULT_SUCCESS_V01;
          response.error = QMI_ERR_NONE_V01;
        }

        CFCM_MSG_3(HIGH, "Received new Bus_BW Level %d, current %d, applied %d",
                       rcvd_qmi_bw_lvl, old_bw_lvl, cfcm_monitor_ptr->qmi_data.qmi_bw_lvl);

        /* send response prior to initiating further algorithm  */
        req_cb_retval   = mqcsi_conn_mgr_send_resp_from_cb( 
           connection_handle,
           req_handle,
           QMI_CFCM_BUS_BW_THROTTLING_LEVEL_RESP_MSG_V01,
           &response,
           sizeof( cfcm_set_bus_bw_throttling_level_resp_msg_v01 )
           );
        if( req_cb_retval == QMI_CSI_CB_NO_ERR )
        {
          CFCM_MSG_3(MED, "Sent resp msg %d res %d err %d for BUS_BW_THROTTLING_LEVEL req msg",
                        msg_id, response.result, response.error);
        }
        else
        {
          CFCM_MSG_3(ERROR, "Could not send BUS_BW_THROTTLING_LEVEL resp msg %d res %d err %d",
                        msg_id, response.result, response.error);
        }
        
        /* Send messages to interested parties only if the BW Throttling Level has changed */
        if( cfcm_monitor_ptr->qmi_data.qmi_bw_lvl != old_bw_lvl )
        {
          /* Process the change in BW */
          cfcm_process_bus_bw_input();
        }
      }
      break;

     /* This request is used to query Bus BW Throttling */
     case QMI_CFCM_GET_BW_THROTTLING_LEVEL_REQ_MSG_V01:
       {
         /* Initialize structure holding response */
         memset( &get_resp, 0, sizeof( cfcm_get_bus_bw_throttling_level_resp_msg_v01 ) );

         if (cfcm_monitor_ptr->qmi_data.qmi_bw_lvl != CFCM_MONITOR_DEFAULT_BUS_BW_LEVEL)
         {
           get_resp.bus_bw_throttling_level_valid = TRUE;
         }
         else
         {
           get_resp.bus_bw_throttling_level_valid = FALSE;
         }
         get_resp.bus_bw_throttling_level = cfcm_monitor_ptr->qmi_data.qmi_bw_lvl;

         /* send out response */
         req_cb_retval   = mqcsi_conn_mgr_send_resp_from_cb( 
                              connection_handle,
                              req_handle,
                              QMI_CFCM_GET_BW_THROTTLING_LEVEL_RESP_MSG_V01,
                              &get_resp,
                              sizeof( cfcm_get_bus_bw_throttling_level_resp_msg_v01 )
                          );

        if( req_cb_retval == QMI_CSI_CB_NO_ERR )
        {
          CFCM_MSG_3(MED, "Sent Get BUS_BW_THROTTLING_LEVEL %d resp msg %d handle %d", 
                    get_resp.bus_bw_throttling_level, msg_id, req_handle);
        }
        else
        {
          CFCM_MSG_2(ERROR, "Could not send Get BUS_BW_THROTTLING_LEVEL resp msg %d handle %d",
                    msg_id, req_handle);
        }
      }
      break;

    /* error case */
    default:
      /* Initialize structure holding response */
      memset( &response, 0, sizeof( qmi_response_type_v01 ) );

      response.result = QMI_RESULT_FAILURE_V01;
      response.error  = QMI_ERR_INVALID_MESSAGE_ID_V01;

      req_cb_retval   = mqcsi_conn_mgr_send_resp_from_cb( 
                          connection_handle,
                          req_handle,
                          msg_id,
                          &response,
                          sizeof( qmi_response_type_v01 )
                        );
      break;
  } /* switch ( msg_id ) */

  /*-----------------------------------------------------------------------*/
  return req_cb_retval;
}

/*=============================================================================

  FUNCTION:  cfcm_qmi_get_ind_offset

=============================================================================*/
/*!
    @brief
    This method maintains the internal mapping between the indication method
    id and offset of the message in the message map.

    @return
    int32 offset
*/
/*===========================================================================*/
int32 cfcm_qmi_get_ind_offset 
(
  const uint32   ind_msg_id  /*!< Indication whose internal offset is needed */
)
{
  /*-----------------------------------------------------------------------*/
  CFCM_UNUSED(ind_msg_id);
  return -1;
}

/*==============================================================================

  FUNCTION:  cfcm_monitor_bus_bw_init

==============================================================================*/
/*!
    @brief
    Initialized Bus Bandwidth Monitor.

    @return
    None
*/
/*============================================================================*/
static void cfcm_monitor_bus_bw_init( void )
{
  /*--------------------------------------------------------------------------*/

  /*--------------------------------------------------------------------------*/

  /* 1. Setup service object */
  cfcm_monitor_ptr->qmi_data.service_object = cfcm_get_service_object_v01();

  /* 2. Register object with QMI */
  (void) qmi_si_register_object ( cfcm_monitor_ptr->qmi_data.service_object,
                                  0, /* Service Instance */
                                  cfcm_get_service_impl_v01() );

  /* 3. Register/Open/Initiate service with Connection Manager infrastructure */
  cfcm_monitor_ptr->qmi_data.service_handle = mqcsi_conn_mgr_service_open (
                                               MQCSI_CFCM_SERVICE_ID,
                                               "CFCM_SERVICE",
                                               &cfcm_tcb,
                                               CFCM_TASK_QMI_SIG,
                                               cfcm_monitor_ptr->qmi_data.service_object, 
                                               cfcm_qmi_process_req,
                                               cfcm_qmi_get_ind_offset,
                                               NULL,
                                               CFCM_V01_IDL_MAJOR_VERS,
                                               CFCM_V01_IDL_MINOR_VERS );
  CFCM_ASSERT( NULL != cfcm_monitor_ptr->qmi_data.service_handle );

  cfcm_monitor_ptr->qmi_data.qmi_bw_lvl = CFCM_MONITOR_DEFAULT_BUS_BW_LEVEL;
} /* cfcm_monitor_bus_bw_init() */

/*==============================================================================

  FUNCTION:  cfcm_monitor_get_clients_step_timer_value

==============================================================================*/
/*!
    @brief
    Get step timer value for client's monitor 

    @return
    None
*/
/*============================================================================*/
static uint32 cfcm_monitor_get_clients_step_timer_value
(
  cfcm_client_e client,
  cfcm_monitor_e monitor   /*!< monitor */
)
{
  uint32               step_timer = 0;
  
  if (CFCM_MONITOR_IS_STEP_TIMER_VALID_CMDS(cfcm_monitor_ptr->monitors[monitor].last_cmd[client].cmd))
  {
    switch(monitor)
    {
      case CFCM_MONITOR_DSM_LARGE_POOL:
      case CFCM_MONITOR_DSM_DUP_POOL:
      case CFCM_MONITOR_DSM_SMALL_POOL:
      case CFCM_MONITOR_DSM_LARGE_POOL_LEVEL1:
      {      
        step_timer  = cfcm_cfg_get_dsm_step_timer(client);
        break;
      }
      case CFCM_MONITOR_THERMAL_PA:    
      case CFCM_MONITOR_THERMAL_CX:
      case CFCM_MONITOR_MDM_TEMP:
      case CFCM_MONITOR_VDD_PEAK_CURR_EST:
      {
        step_timer  = cfcm_cfg_get_thermal_step_timer(client);
        break;
      }
      case CFCM_MONITOR_CPU:
      {
        step_timer = cfcm_cfg_get_cpu_step_timer(client);
        break;
      }
      case CFCM_MONITOR_BW_THROTTLING:
      {
        step_timer  = cfcm_cfg_get_bw_step_timer(client);
        break;
      }
      default:
      {
        step_timer  = CFCM_MONITOR_DEFAULT_STEP_TIMER;
        CFCM_ERR_FATAL("cfcm_monitor_init_monitor monitor %d invalid", monitor, 0, 0);
        break;
      }
    }
  }
  else
  {
    step_timer  = CFCM_MONITOR_DEFAULT_STEP_TIMER;
  }
  
  return step_timer;
}

/*==============================================================================

  FUNCTION:  cfcm_monitor_init_monitor

==============================================================================*/
/*!
    @brief
    initialize a single monitor.

    @return
    None
*/
/*============================================================================*/
void cfcm_monitor_init_monitor
(
  cfcm_monitor_e monitor   /*!< monitor to be initialized */
)
{
  cfcm_monitor_info_s*   monitor_ptr;
  uint32 j;
  /*--------------------------------------------------------------------------*/

  CFCM_ASSERT(monitor < CFCM_MONITOR_MAX);

  /*--------------------------------------------------------------------------*/
  monitor_ptr = &cfcm_monitor_ptr->monitors[(uint32)monitor];

  monitor_ptr->monitor_id           = monitor;
  monitor_ptr->num_client           = 0;
  memset(&monitor_ptr->stats, 0, sizeof(cfcm_monitor_stats_s));

  for(j = 0; j < (uint32)CFCM_CLIENT_MAX; j++)
  {
    monitor_ptr->last_cmd[j].cmd = CFCM_CMD_FC_OFF;
    monitor_ptr->last_cmd[j].cmd_updated = FALSE;
    monitor_ptr->client[j] = CFCM_CLIENT_MAX;
    monitor_ptr->registered[j]   = FALSE;
  }

  for(j = 0; j < CFCM_MONITOR_HIST_BUF_SZ; j++)
  {
    monitor_ptr->cmd_hist[j] = CFCM_CMD_FC_OFF;
    monitor_ptr->client_hist[j] = CFCM_CLIENT_MAX;
  }
  monitor_ptr->latest_cmd_hist_idx = CFCM_MONITOR_HIST_BUF_IDX_MASK;
} /* cfcm_monitor_init_monitor() */

/*==============================================================================

  FUNCTION:  cfcm_monitor_large_pool_event_cb

==============================================================================*/
/*!
    @brief
    callback function registered with large dsm_pool indicating when memory is 
    low.

    @return
    None
*/
/*============================================================================*/
EXTERN void cfcm_monitor_large_pool_event_cb
(
  dsm_mempool_id_enum_type pool_id, /*!< pool triggered the event */
  dsm_mem_level_enum_type  event,   /*!< level reached */
  dsm_mem_op_enum_type     op       /*!< operation that triggered the event */
)
{
  cfcm_monitor_ind_msg_s  msg;
  errno_enum_type        send_status;
  cfcm_cmd_e             cmd = CFCM_CMD_INVALID;
  uint32                 client_mask = 0;
  cfcm_dsm_mem_level_e   level = CFCM_DSM_MEM_LEVEL_INVALID;

  /*--------------------------------------------------------------------------*/
  CFCM_ASSERT(pool_id == DSM_DS_LARGE_ITEM_POOL);
  /*--------------------------------------------------------------------------*/

  CFCM_MSG_1(HIGH, "CFCM DSM monitor cfcm_monitor_large_pool_event_cb event %d", event);

  switch(event)
  {
    case DSM_MEM_LEVEL_LTE_DL_FEW:
    case DSM_MEM_LEVEL_LTE_DL_MANY:
    case DSM_MEM_LEVEL_LTE_DNE:
    {
      if(event == DSM_MEM_LEVEL_LTE_DL_FEW)
      {
        CFCM_ASSERT(op == DSM_MEM_OP_NEW);
        cmd = CFCM_CMD_DOWN;
        level = CFCM_DSM_MEM_LEVEL_FEW;
      }
      else if(event == DSM_MEM_LEVEL_LTE_DL_MANY)
      {
        CFCM_ASSERT(op == DSM_MEM_OP_FREE);
        cmd = CFCM_CMD_FC_OFF;
        level = CFCM_DSM_MEM_LEVEL_MANY;
      }
      else // DSM_MEM_LEVEL_LTE_DNE
      {
        CFCM_ASSERT(op == DSM_MEM_OP_NEW);
        cmd = CFCM_CMD_DOWN;
        level = CFCM_DSM_MEM_LEVEL_DNE;
      }
      /* Update commands for LTE clients for DSM levels */
      client_mask = (CFCM_CONV_ENUM_TO_BIT_MASK(CFCM_CLIENT_LTE_RLC_DL) | 
                     CFCM_CONV_ENUM_TO_BIT_MASK(CFCM_CLIENT_LTE_PDCP_DL));
    }
    break;

    case DSM_MEM_LEVEL_RLC_FEW:
    case DSM_MEM_LEVEL_RLC_MANY:
    case DSM_MEM_LEVEL_LINK_LAYER_DNE:
    {
      if(event == DSM_MEM_LEVEL_RLC_FEW)
      {
        CFCM_ASSERT(op == DSM_MEM_OP_NEW);
        cmd = CFCM_CMD_DOWN;
        level = CFCM_DSM_MEM_LEVEL_FEW;
      }
      else if(event == DSM_MEM_LEVEL_RLC_MANY)
      {
        CFCM_ASSERT(op == DSM_MEM_OP_FREE);
        cmd = CFCM_CMD_FC_OFF;
        level = CFCM_DSM_MEM_LEVEL_MANY;
      }
      else // DSM_MEM_LEVEL_LINK_LAYER_DNE
      {
        CFCM_ASSERT(op == DSM_MEM_OP_NEW);
        cmd = CFCM_CMD_DOWN;
        level = CFCM_DSM_MEM_LEVEL_DNE;
      }
      /* Update commands for RLC clients for DSM levels */
      client_mask = CFCM_CONV_ENUM_TO_BIT_MASK(CFCM_CLIENT_WCDMA_RLC_DL);
    }
    break;

    case DSM_MEM_LEVEL_A2_UL_PER_FEW:
    case DSM_MEM_LEVEL_A2_UL_PER_MANY:
    {
      if(event == DSM_MEM_LEVEL_A2_UL_PER_FEW)
      {
        CFCM_ASSERT(op == DSM_MEM_OP_NEW);
        cmd = CFCM_CMD_DOWN;
        level = CFCM_DSM_MEM_LEVEL_FEW;
      }
      else // DSM_MEM_LEVEL_A2_UL_PER_MANY
      {
        CFCM_ASSERT(op == DSM_MEM_OP_FREE);
        cmd = CFCM_CMD_FC_OFF;
        level = CFCM_DSM_MEM_LEVEL_MANY;
      }
      /* Update commands for LTE clients for DSM levels */
      client_mask = CFCM_CONV_ENUM_TO_BIT_MASK(CFCM_CLIENT_A2_UL_PER);
    }
    break;

    case DSM_MEM_LEVEL_LTE_DL_LEVEL1_FEW:
    case DSM_MEM_LEVEL_LTE_DL_LEVEL1_MANY:
    {
      if(event == DSM_MEM_LEVEL_LTE_DL_LEVEL1_FEW)
      {
        CFCM_ASSERT(op == DSM_MEM_OP_NEW);
        cmd = CFCM_CMD_DOWN;
        level = CFCM_DSM_MEM_LEVEL_FEW;
      }
      else // DSM_MEM_LEVEL_LTE_DL_LEVEL1_MANY
      {
        CFCM_ASSERT(op == DSM_MEM_OP_FREE);
        cmd = CFCM_CMD_FC_OFF;
        level = CFCM_DSM_MEM_LEVEL_MANY;
      }
      /* Update commands for LTE clients for DSM levels */
      client_mask = CFCM_CONV_ENUM_TO_BIT_MASK(CFCM_CLIENT_LTE_RLC_LEVEL1_DL);
    }
    break;

    case DSM_MEM_LEVEL_LTE_PDCP_COMP_FEW:
    case DSM_MEM_LEVEL_LTE_PDCP_COMP_MANY:
    case DSM_MEM_LEVEL_LTE_PDCP_COMP_DNE:
    {
      if(event == DSM_MEM_LEVEL_LTE_PDCP_COMP_FEW)
      {
        CFCM_ASSERT(op == DSM_MEM_OP_NEW);
        cmd = CFCM_CMD_DOWN;
        level = CFCM_DSM_MEM_LEVEL_FEW;
      }
      else if(event == DSM_MEM_LEVEL_LTE_PDCP_COMP_MANY)
      {
        CFCM_ASSERT(op == DSM_MEM_OP_FREE);
        cmd = CFCM_CMD_FC_OFF;
        level = CFCM_DSM_MEM_LEVEL_MANY;
      }
      else // DSM_MEM_LEVEL_LTE_PDCP_COMP_DNE
      {
        CFCM_ASSERT(op == DSM_MEM_OP_NEW);
        cmd = CFCM_CMD_DOWN;
        level = CFCM_DSM_MEM_LEVEL_DNE;
      }
      /* Update commands for LTE clients for DSM levels */
      client_mask = CFCM_CONV_ENUM_TO_BIT_MASK(CFCM_CLIENT_LTE_PDCP_COMP);
    }
    break;

    case DSM_MEM_LEVEL_LTE_ML1_CPU_DNE:
    case DSM_MEM_LEVEL_LTE_ML1_CPU_FEW:
    case DSM_MEM_LEVEL_LTE_ML1_CPU_MANY:
    {
      if(event == DSM_MEM_LEVEL_LTE_ML1_CPU_FEW)
      {
        CFCM_ASSERT(op == DSM_MEM_OP_NEW);
        cmd = CFCM_CMD_DOWN;
        level = CFCM_DSM_MEM_LEVEL_FEW;
      }
      else if(event == DSM_MEM_LEVEL_LTE_ML1_CPU_MANY)
      {
        CFCM_ASSERT(op == DSM_MEM_OP_FREE);
        cmd = CFCM_CMD_FC_OFF;
        level = CFCM_DSM_MEM_LEVEL_MANY;
      }
      else // DSM_MEM_LEVEL_LTE_ML1_CPU_DNE
      {
        CFCM_ASSERT(op == DSM_MEM_OP_NEW);
        cmd = CFCM_CMD_DOWN;
        level = CFCM_DSM_MEM_LEVEL_DNE;
      }
      /* Update commands for LTE ML1 CPU client for DSM levels */
      client_mask = CFCM_CONV_ENUM_TO_BIT_MASK(CFCM_CLIENT_LTE_ML1_CPU);
    }
    break;

    default:
    {
      CFCM_MSG_1(ERROR, "cfcm_monitor_large_pool_event_cb invalid event %d", event);
    }
    break;
  }

  msg.monitor_id = CFCM_MONITOR_DSM_LARGE_POOL;
  msg.monitor_data.dsm.client_mask = client_mask;
  msg.monitor_data.dsm.cmd = cmd;
  msg.monitor_data.dsm.level = level;
  msg.monitor_data.dsm.pool_id= pool_id;
  msg.monitor_data.dsm.event= event;
  msg.monitor_data.dsm.op= op;

  /* send monitor state change indication to the CFCM */
  send_status = cfcm_msgr_send_msg(&msg.hdr,
                             MCS_CFCM_MONITOR_IND,
                             sizeof(cfcm_monitor_ind_msg_s));
  CFCM_ASSERT(send_status == E_SUCCESS);

} /* cfcm_monitor_large_pool_dl_event_cb() */

/*==============================================================================

  FUNCTION:  cfcm_monitor_dup_pool_event_cb

==============================================================================*/
/*!
    @brief
    callback function registered with dup dsm_pool indicating when memory is 
    low.

    @return
    None
*/
/*============================================================================*/
EXTERN void cfcm_monitor_dup_pool_event_cb
(
  dsm_mempool_id_enum_type pool_id, /*!< pool triggered the event */
  dsm_mem_level_enum_type  event,   /*!< level reached */
  dsm_mem_op_enum_type     op       /*!< operation that triggered the event */
)
{
  cfcm_monitor_ind_msg_s  msg;
  errno_enum_type        send_status;
  cfcm_cmd_e             cmd = CFCM_CMD_INVALID;
  uint32                 client_mask = 0;
  cfcm_dsm_mem_level_e   level = CFCM_DSM_MEM_LEVEL_INVALID;

  /*--------------------------------------------------------------------------*/
  CFCM_ASSERT(pool_id == DSM_DUP_ITEM_POOL);
  /*--------------------------------------------------------------------------*/

  CFCM_MSG_1(HIGH, "CFCM DSM monitor cfcm_monitor_dup_pool_event_cb event %d", event);

  switch(event)
  {
    case DSM_MEM_LEVEL_LTE_DL_FEW:
    case DSM_MEM_LEVEL_LTE_DL_MANY:
    case DSM_MEM_LEVEL_LTE_DNE:
    {
      /* send monitor state change indication to the CFCM */
      if(event == DSM_MEM_LEVEL_LTE_DL_FEW)
      {
        CFCM_ASSERT(op == DSM_MEM_OP_NEW);
        cmd = CFCM_CMD_DOWN;
        level = CFCM_DSM_MEM_LEVEL_FEW;
      }
      else if(event == DSM_MEM_LEVEL_LTE_DL_MANY)
      {
        CFCM_ASSERT(op == DSM_MEM_OP_FREE);
        cmd = CFCM_CMD_FC_OFF;
        level = CFCM_DSM_MEM_LEVEL_MANY;
      }
      else // DSM_MEM_LEVEL_LTE_DNE
      {
        CFCM_ASSERT(op == DSM_MEM_OP_NEW);
        cmd = CFCM_CMD_DOWN;
        level = CFCM_DSM_MEM_LEVEL_DNE;
      }
      /* Update commands for LTE clients for DSM levels */
      client_mask = (CFCM_CONV_ENUM_TO_BIT_MASK(CFCM_CLIENT_LTE_RLC_DL) | 
                     CFCM_CONV_ENUM_TO_BIT_MASK(CFCM_CLIENT_LTE_PDCP_DL));
    }
    break;

    case DSM_MEM_LEVEL_RLC_FEW:
    case DSM_MEM_LEVEL_RLC_MANY:
    {
      /* send monitor state change indication to the CFCM */
      if(event == DSM_MEM_LEVEL_RLC_FEW)
      {
        CFCM_ASSERT(op == DSM_MEM_OP_NEW);
        cmd = CFCM_CMD_DOWN;
        level = CFCM_DSM_MEM_LEVEL_FEW;
      }
      else // DSM_MEM_LEVEL_RLC_MANY
      {
        CFCM_ASSERT(op == DSM_MEM_OP_FREE);
        cmd = CFCM_CMD_FC_OFF;
        level = CFCM_DSM_MEM_LEVEL_MANY;
      }
      /* Update commands for RLC clients for DSM levels */
      client_mask = CFCM_CONV_ENUM_TO_BIT_MASK(CFCM_CLIENT_WCDMA_RLC_DL);
    }
    break;

    case DSM_MEM_LEVEL_A2_UL_PER_FEW:
    case DSM_MEM_LEVEL_A2_UL_PER_MANY:
    {
      if(event == DSM_MEM_LEVEL_A2_UL_PER_FEW)
      {
        CFCM_ASSERT(op == DSM_MEM_OP_NEW);
        cmd = CFCM_CMD_DOWN;
        level = CFCM_DSM_MEM_LEVEL_FEW;
      }
      else // DSM_MEM_LEVEL_A2_UL_PER_MANY
      {
        CFCM_ASSERT(op == DSM_MEM_OP_FREE);
        cmd = CFCM_CMD_FC_OFF;
        level = CFCM_DSM_MEM_LEVEL_MANY;
      }
      /* Update commands for LTE clients for DSM levels */
      client_mask = CFCM_CONV_ENUM_TO_BIT_MASK(CFCM_CLIENT_A2_UL_PER);
    }
    break;

    case DSM_MEM_LEVEL_LTE_DL_LEVEL1_FEW:
    case DSM_MEM_LEVEL_LTE_DL_LEVEL1_MANY:
    {
      if(event == DSM_MEM_LEVEL_LTE_DL_LEVEL1_FEW)
      {
        CFCM_ASSERT(op == DSM_MEM_OP_NEW);
        cmd = CFCM_CMD_DOWN;
        level = CFCM_DSM_MEM_LEVEL_FEW;
      }
      else // DSM_MEM_LEVEL_LTE_DL_LEVEL1_MANY
      {
        CFCM_ASSERT(op == DSM_MEM_OP_FREE);
        cmd = CFCM_CMD_FC_OFF;
        level = CFCM_DSM_MEM_LEVEL_MANY;
      }
      /* Update commands for LTE clients for DSM levels */
      client_mask = CFCM_CONV_ENUM_TO_BIT_MASK(CFCM_CLIENT_LTE_RLC_LEVEL1_DL);
    }
    break;

    case DSM_MEM_LEVEL_LTE_PDCP_COMP_FEW:
    case DSM_MEM_LEVEL_LTE_PDCP_COMP_MANY:
    case DSM_MEM_LEVEL_LTE_PDCP_COMP_DNE:
    {
      /* send monitor state change indication to the CFCM */
      if(event == DSM_MEM_LEVEL_LTE_PDCP_COMP_FEW)
      {
        CFCM_ASSERT(op == DSM_MEM_OP_NEW);
        cmd = CFCM_CMD_DOWN;
        level = CFCM_DSM_MEM_LEVEL_FEW;
      }
      else if(event == DSM_MEM_LEVEL_LTE_PDCP_COMP_MANY)
      {
        CFCM_ASSERT(op == DSM_MEM_OP_FREE);
        cmd = CFCM_CMD_FC_OFF;
        level = CFCM_DSM_MEM_LEVEL_MANY;
      }
      else // DSM_MEM_LEVEL_LTE_PDCP_COMP_DNE
      {
        CFCM_ASSERT(op == DSM_MEM_OP_NEW);
        cmd = CFCM_CMD_DOWN;
        level = CFCM_DSM_MEM_LEVEL_DNE;
      }
      /* Update commands for LTE clients for DSM levels */
      client_mask = CFCM_CONV_ENUM_TO_BIT_MASK(CFCM_CLIENT_LTE_PDCP_COMP);
    }
    break;

    case DSM_MEM_LEVEL_LTE_ML1_CPU_DNE:
    case DSM_MEM_LEVEL_LTE_ML1_CPU_FEW:
    case DSM_MEM_LEVEL_LTE_ML1_CPU_MANY:
    {
      if(event == DSM_MEM_LEVEL_LTE_ML1_CPU_FEW)
      {
        CFCM_ASSERT(op == DSM_MEM_OP_NEW);
        cmd = CFCM_CMD_DOWN;
        level = CFCM_DSM_MEM_LEVEL_FEW;
      }
      else if(event == DSM_MEM_LEVEL_LTE_ML1_CPU_MANY)
      {
        CFCM_ASSERT(op == DSM_MEM_OP_FREE);
        cmd = CFCM_CMD_FC_OFF;
        level = CFCM_DSM_MEM_LEVEL_MANY;
      }
      else // DSM_MEM_LEVEL_LTE_ML1_CPU_DNE
      {
        CFCM_ASSERT(op == DSM_MEM_OP_NEW);
        cmd = CFCM_CMD_DOWN;
        level = CFCM_DSM_MEM_LEVEL_DNE;
      }
      /* Update commands for LTE ML1 CPU client for DSM levels */
      client_mask = CFCM_CONV_ENUM_TO_BIT_MASK(CFCM_CLIENT_LTE_ML1_CPU);
    }
    break;

    default:
    {
      CFCM_ERR_FATAL("cfcm_monitor_dup_pool_event_cb invalid event %d", event, 0, 0);
    }
    break;
  }


  msg.monitor_id = CFCM_MONITOR_DSM_DUP_POOL;
  msg.monitor_data.dsm.client_mask = client_mask;
  msg.monitor_data.dsm.cmd = cmd;
  msg.monitor_data.dsm.level = level;
  msg.monitor_data.dsm.pool_id= pool_id;
  msg.monitor_data.dsm.event= event;
  msg.monitor_data.dsm.op= op;

  send_status = cfcm_msgr_send_msg(&msg.hdr,
                             MCS_CFCM_MONITOR_IND,
                             sizeof(cfcm_monitor_ind_msg_s));
  CFCM_ASSERT(send_status == E_SUCCESS);

} /* cfcm_monitor_dup_pool_event_cb() */

/*==============================================================================

  FUNCTION:  cfcm_monitor_small_pool_event_cb

==============================================================================*/
/*!
    @brief
    callback function registered with small dsm_pool indicating when memory is 
    low.

    @return
    None
*/
/*============================================================================*/
EXTERN void cfcm_monitor_small_pool_event_cb
(
  dsm_mempool_id_enum_type pool_id, /*!< pool triggered the event */
  dsm_mem_level_enum_type  event,   /*!< level reached */
  dsm_mem_op_enum_type     op       /*!< operation that triggered the event */
)
{
  cfcm_monitor_ind_msg_s  msg;
  errno_enum_type        send_status;
  cfcm_cmd_e             cmd = CFCM_CMD_INVALID;
  uint32                 client_mask = 0;
  cfcm_dsm_mem_level_e   level = CFCM_DSM_MEM_LEVEL_INVALID;
  /*--------------------------------------------------------------------------*/
  CFCM_ASSERT(pool_id == DSM_DS_SMALL_ITEM_POOL);
  /*--------------------------------------------------------------------------*/

  CFCM_MSG_1(HIGH, "CFCM DSM monitor cfcm_monitor_small_pool_event_cb event %d", event);

  switch(event)
  {
    case DSM_MEM_LEVEL_RLC_FEW:
    case DSM_MEM_LEVEL_RLC_MANY:
    case DSM_MEM_LEVEL_LINK_LAYER_DNE:
    {
      if(event == DSM_MEM_LEVEL_RLC_FEW)
      {
        CFCM_ASSERT(op == DSM_MEM_OP_NEW);
        cmd = CFCM_CMD_DOWN;
        level = CFCM_DSM_MEM_LEVEL_FEW;
      }
      else if(event == DSM_MEM_LEVEL_RLC_MANY)
      {
        CFCM_ASSERT(op == DSM_MEM_OP_FREE);
        cmd = CFCM_CMD_FC_OFF;
        level = CFCM_DSM_MEM_LEVEL_MANY;
      }
      else // DSM_MEM_LEVEL_LINK_LAYER_DNE
      {
        CFCM_ASSERT(op == DSM_MEM_OP_NEW);
        cmd = CFCM_CMD_DOWN;
        level = CFCM_DSM_MEM_LEVEL_DNE;
      }
      /* Update commands for RLC clients for DSM levels */
      client_mask = CFCM_CONV_ENUM_TO_BIT_MASK(CFCM_CLIENT_WCDMA_RLC_DL);
    }
    break;

    default:
    {
      CFCM_ERR_FATAL("cfcm_monitor_small_pool_event_cb invalid event %d", event, 0, 0);
    }
    break;
  }

  msg.monitor_id = CFCM_MONITOR_DSM_SMALL_POOL;
  msg.monitor_data.dsm.client_mask = client_mask;
  msg.monitor_data.dsm.cmd = cmd;
  msg.monitor_data.dsm.level = level;
  msg.monitor_data.dsm.pool_id= pool_id;
  msg.monitor_data.dsm.event= event;
  msg.monitor_data.dsm.op= op;

  /* send monitor state change indication to the CFCM */
  send_status = cfcm_msgr_send_msg(&msg.hdr,
                             MCS_CFCM_MONITOR_IND,
                             sizeof(cfcm_monitor_ind_msg_s));
  CFCM_ASSERT(send_status == E_SUCCESS);

} /* cfcm_monitor_small_pool_dl_event_cb() */


/*==============================================================================

  FUNCTION:  cfcm_monitor_large_pool_level1_event_cb

==============================================================================*/
/*!
    @brief
    callback function registered with large dsm_pool indicating when memory is 
    low.

    @return
    None
*/
/*============================================================================*/
EXTERN void cfcm_monitor_large_pool_level1_event_cb
(
  dsm_mempool_id_enum_type pool_id, /*!< pool triggered the event */
  dsm_mem_level_enum_type  event,   /*!< level reached */
  dsm_mem_op_enum_type     op       /*!< operation that triggered the event */
)
{
  cfcm_monitor_ind_msg_s  msg;
  errno_enum_type        send_status;
  cfcm_cmd_e             cmd = CFCM_CMD_INVALID;
  uint32                 client_mask = 0;
  cfcm_dsm_mem_level_e   level = CFCM_DSM_MEM_LEVEL_INVALID;

  /*--------------------------------------------------------------------------*/
  CFCM_ASSERT(pool_id == DSM_DS_LARGE_ITEM_POOL);
  /*--------------------------------------------------------------------------*/

  CFCM_MSG_1(HIGH, "CFCM DSM monitor cfcm_monitor_large_pool_level1_event_cb event %d", event);

  switch(event)
  {
    case DSM_MEM_LEVEL_IPA_DL_FEW:
    case DSM_MEM_LEVEL_IPA_DL_MANY:
    {
      if(event == DSM_MEM_LEVEL_IPA_DL_FEW)
      {
        CFCM_ASSERT(op == DSM_MEM_OP_NEW);
        cmd = CFCM_CMD_DOWN;
        level = CFCM_DSM_MEM_LEVEL_FEW;
      }
      else // DSM_MEM_LEVEL_IPA_DL_MANY
      {
        CFCM_ASSERT(op == DSM_MEM_OP_FREE);
        cmd = CFCM_CMD_FC_OFF;
        level = CFCM_DSM_MEM_LEVEL_MANY;
      }
      /* Update commands for LTE clients for DSM levels */
      client_mask = CFCM_CONV_ENUM_TO_BIT_MASK(CFCM_CLIENT_UL_PER);
    }
    break;

    default:
    {
      CFCM_MSG_1(ERROR, "cfcm_monitor_large_pool_level1_event_cb invalid event %d", event);
    }
    break;
  }

  msg.monitor_id = CFCM_MONITOR_DSM_LARGE_POOL_LEVEL1;
  msg.monitor_data.dsm.client_mask = client_mask;
  msg.monitor_data.dsm.cmd = cmd;
  msg.monitor_data.dsm.level = level;
  msg.monitor_data.dsm.pool_id= pool_id;
  msg.monitor_data.dsm.event= event;
  msg.monitor_data.dsm.op= op;

  /* send monitor state change indication to the CFCM */
  send_status = cfcm_msgr_send_msg(&msg.hdr,
                             MCS_CFCM_MONITOR_IND,
                             sizeof(cfcm_monitor_ind_msg_s));
  CFCM_ASSERT(send_status == E_SUCCESS);

} /* cfcm_monitor_large_pool_level1_event_cb() */
/*==============================================================================

  FUNCTION:  cfcm_monitor_reg_with_dsm_large_pool

==============================================================================*/
/*!
    @brief
    Register clients with DSM LARGE pool.

    @return
    None
*/
/*============================================================================*/
static void cfcm_monitor_reg_with_dsm_large_pool
(
  cfcm_client_e client   /*!<  client that needs to register */
)
{
  cfcm_dbg_s* cfcm_dbg_ptr;
  /*--------------------------------------------------------------------------*/

  /*--------------------------------------------------------------------------*/
  cfcm_dbg_ptr = cfcm_dbg_get_data();

  switch(client)
  {
    case CFCM_CLIENT_WCDMA_RLC_DL:
    {
      /* WCDMA_RLC_DL */
      dsm_reg_mem_event_level(DSM_DS_LARGE_ITEM_POOL,
                              DSM_MEM_LEVEL_LINK_LAYER_DNE,
                              cfcm_dbg_ptr->large_pool_rlc_dne);

      dsm_reg_mem_event_level(DSM_DS_LARGE_ITEM_POOL,
                              DSM_MEM_LEVEL_RLC_FEW,
                              cfcm_dbg_ptr->large_pool_rlc_dl_few);

      dsm_reg_mem_event_level(DSM_DS_LARGE_ITEM_POOL,
                              DSM_MEM_LEVEL_RLC_MANY,
                              cfcm_dbg_ptr->large_pool_rlc_dl_many);

      dsm_reg_mem_event_optimized_level(DSM_DS_LARGE_ITEM_POOL,
                                        DSM_MEM_LEVEL_RLC_FEW,
                                        DSM_MEM_LEVEL_RLC_MANY);

      dsm_reg_mem_event_cb(DSM_DS_LARGE_ITEM_POOL,
                           DSM_MEM_LEVEL_LINK_LAYER_DNE,
                           DSM_MEM_OP_NEW,
                           cfcm_monitor_large_pool_event_cb );

      dsm_reg_mem_event_cb(DSM_DS_LARGE_ITEM_POOL,
                           DSM_MEM_LEVEL_RLC_FEW,
                           DSM_MEM_OP_NEW,
                           cfcm_monitor_large_pool_event_cb );

      dsm_reg_mem_event_cb(DSM_DS_LARGE_ITEM_POOL,
                           DSM_MEM_LEVEL_RLC_MANY,
                           DSM_MEM_OP_FREE,
                           cfcm_monitor_large_pool_event_cb );
    }
    break;

    case CFCM_CLIENT_A2_UL_PER:
    {
      /* A2_UL_PER */
      dsm_reg_mem_event_level(DSM_DS_LARGE_ITEM_POOL,
                              DSM_MEM_LEVEL_A2_UL_PER_FEW,
                              cfcm_dbg_ptr->large_pool_a2_ul_per_few);

      dsm_reg_mem_event_level(DSM_DS_LARGE_ITEM_POOL,
                              DSM_MEM_LEVEL_A2_UL_PER_MANY,
                              cfcm_dbg_ptr->large_pool_a2_ul_per_many);

      dsm_reg_mem_event_optimized_level(DSM_DS_LARGE_ITEM_POOL,
                                        DSM_MEM_LEVEL_A2_UL_PER_FEW,
                                        DSM_MEM_LEVEL_A2_UL_PER_MANY);

      dsm_reg_mem_event_cb(DSM_DS_LARGE_ITEM_POOL,
                           DSM_MEM_LEVEL_A2_UL_PER_FEW,
                           DSM_MEM_OP_NEW,
                           cfcm_monitor_large_pool_event_cb );

      dsm_reg_mem_event_cb(DSM_DS_LARGE_ITEM_POOL,
                           DSM_MEM_LEVEL_A2_UL_PER_MANY,
                           DSM_MEM_OP_FREE,
                           cfcm_monitor_large_pool_event_cb );
    }
    break;

    case CFCM_CLIENT_LTE_RLC_DL:
    case CFCM_CLIENT_LTE_PDCP_DL:
    {

      if((cfcm_monitor_ptr->monitors[CFCM_MONITOR_DSM_LARGE_POOL].registered[CFCM_CLIENT_LTE_RLC_DL] == TRUE) || 
         (cfcm_monitor_ptr->monitors[CFCM_MONITOR_DSM_LARGE_POOL].registered[CFCM_CLIENT_LTE_PDCP_DL] == TRUE))
      {
        CFCM_MSG_1(ERROR, "CFCM: dsm_large_pool Ignore: Already register with DSM Levels %d ", client);
      }
      else
      {
        /* LTE RLC/PDCP DL */
        dsm_reg_mem_event_level(DSM_DS_LARGE_ITEM_POOL,
                                DSM_MEM_LEVEL_LTE_DNE,
                                cfcm_dbg_ptr->large_pool_lte_dne);
  
        dsm_reg_mem_event_level(DSM_DS_LARGE_ITEM_POOL,
                                DSM_MEM_LEVEL_LTE_DL_FEW,
                                cfcm_dbg_ptr->large_pool_lte_dl_few);
  
        dsm_reg_mem_event_level(DSM_DS_LARGE_ITEM_POOL,
                                DSM_MEM_LEVEL_LTE_DL_MANY,
                                cfcm_dbg_ptr->large_pool_lte_dl_many);
  
        dsm_reg_mem_event_optimized_level(DSM_DS_LARGE_ITEM_POOL,
                                          DSM_MEM_LEVEL_LTE_DL_FEW,
                                          DSM_MEM_LEVEL_LTE_DL_MANY);
  
        dsm_reg_mem_event_cb(DSM_DS_LARGE_ITEM_POOL,
                             DSM_MEM_LEVEL_LTE_DNE,
                             DSM_MEM_OP_NEW,
                             cfcm_monitor_large_pool_event_cb );
  
        dsm_reg_mem_event_cb(DSM_DS_LARGE_ITEM_POOL,
                             DSM_MEM_LEVEL_LTE_DL_FEW,
                             DSM_MEM_OP_NEW,
                             cfcm_monitor_large_pool_event_cb );
  
        dsm_reg_mem_event_cb(DSM_DS_LARGE_ITEM_POOL,
                             DSM_MEM_LEVEL_LTE_DL_MANY,
                             DSM_MEM_OP_FREE,
                             cfcm_monitor_large_pool_event_cb );
      }
    }
    break;

    case CFCM_CLIENT_LTE_ML1_CPU:
    {
      /* LTE ML1 CPU */
      dsm_reg_mem_event_level(DSM_DS_LARGE_ITEM_POOL,
                              DSM_MEM_LEVEL_LTE_ML1_CPU_DNE,
                              cfcm_dbg_ptr->large_pool_lte_ml1_cpu_dne);
  
      dsm_reg_mem_event_level(DSM_DS_LARGE_ITEM_POOL,
                              DSM_MEM_LEVEL_LTE_ML1_CPU_FEW,
                              cfcm_dbg_ptr->large_pool_lte_ml1_cpu_few);
  
      dsm_reg_mem_event_level(DSM_DS_LARGE_ITEM_POOL,
                              DSM_MEM_LEVEL_LTE_ML1_CPU_MANY,
                              cfcm_dbg_ptr->large_pool_lte_ml1_cpu_many);
  
      dsm_reg_mem_event_optimized_level(DSM_DS_LARGE_ITEM_POOL,
                                        DSM_MEM_LEVEL_LTE_ML1_CPU_FEW,
                                        DSM_MEM_LEVEL_LTE_ML1_CPU_MANY);
  
      dsm_reg_mem_event_cb(DSM_DS_LARGE_ITEM_POOL,
                           DSM_MEM_LEVEL_LTE_ML1_CPU_DNE,
                           DSM_MEM_OP_NEW,
                           cfcm_monitor_large_pool_event_cb );
  
      dsm_reg_mem_event_cb(DSM_DS_LARGE_ITEM_POOL,
                           DSM_MEM_LEVEL_LTE_ML1_CPU_FEW,
                           DSM_MEM_OP_NEW,
                           cfcm_monitor_large_pool_event_cb );
  
      dsm_reg_mem_event_cb(DSM_DS_LARGE_ITEM_POOL,
                           DSM_MEM_LEVEL_LTE_ML1_CPU_MANY,
                           DSM_MEM_OP_FREE,
                           cfcm_monitor_large_pool_event_cb );
    }
    break;

    case CFCM_CLIENT_LTE_RLC_LEVEL1_DL:
    {
      /* LTE_RLC_LEVEL1_DL */
      dsm_reg_mem_event_level(DSM_DS_LARGE_ITEM_POOL,
                              DSM_MEM_LEVEL_LTE_DL_LEVEL1_FEW,
                              cfcm_dbg_ptr->large_pool_lte_dl_level1_few);

      dsm_reg_mem_event_level(DSM_DS_LARGE_ITEM_POOL,
                              DSM_MEM_LEVEL_LTE_DL_LEVEL1_MANY,
                              cfcm_dbg_ptr->large_pool_lte_dl_level1_many);

      dsm_reg_mem_event_optimized_level(DSM_DS_LARGE_ITEM_POOL,
                                        DSM_MEM_LEVEL_LTE_DL_LEVEL1_FEW,
                                        DSM_MEM_LEVEL_LTE_DL_LEVEL1_MANY);

      dsm_reg_mem_event_cb(DSM_DS_LARGE_ITEM_POOL,
                           DSM_MEM_LEVEL_LTE_DL_LEVEL1_FEW,
                           DSM_MEM_OP_NEW,
                           cfcm_monitor_large_pool_event_cb );

      dsm_reg_mem_event_cb(DSM_DS_LARGE_ITEM_POOL,
                           DSM_MEM_LEVEL_LTE_DL_LEVEL1_MANY,
                           DSM_MEM_OP_FREE,
                           cfcm_monitor_large_pool_event_cb );
    }
    break;

    case CFCM_CLIENT_LTE_PDCP_COMP:
    {
      /* LTE_PDCP_COMP */
      dsm_reg_mem_event_level(DSM_DS_LARGE_ITEM_POOL,
                              DSM_MEM_LEVEL_LTE_PDCP_COMP_DNE,
                              cfcm_dbg_ptr->large_pool_lte_pdcp_comp_dne);      

      dsm_reg_mem_event_level(DSM_DS_LARGE_ITEM_POOL,
                              DSM_MEM_LEVEL_LTE_PDCP_COMP_FEW,
                              cfcm_dbg_ptr->large_pool_lte_pdcp_comp_few);

      dsm_reg_mem_event_level(DSM_DS_LARGE_ITEM_POOL,
                              DSM_MEM_LEVEL_LTE_PDCP_COMP_MANY,
                              cfcm_dbg_ptr->large_pool_lte_pdcp_comp_many);

      dsm_reg_mem_event_optimized_level(DSM_DS_LARGE_ITEM_POOL,
                                        DSM_MEM_LEVEL_LTE_PDCP_COMP_FEW,
                                        DSM_MEM_LEVEL_LTE_PDCP_COMP_MANY);

      dsm_reg_mem_event_cb(DSM_DS_LARGE_ITEM_POOL,
                           DSM_MEM_LEVEL_LTE_PDCP_COMP_DNE,
                           DSM_MEM_OP_NEW,
                           cfcm_monitor_large_pool_event_cb );

      dsm_reg_mem_event_cb(DSM_DS_LARGE_ITEM_POOL,
                           DSM_MEM_LEVEL_LTE_PDCP_COMP_FEW,
                           DSM_MEM_OP_NEW,
                           cfcm_monitor_large_pool_event_cb );

      dsm_reg_mem_event_cb(DSM_DS_LARGE_ITEM_POOL,
                           DSM_MEM_LEVEL_LTE_PDCP_COMP_MANY,
                           DSM_MEM_OP_FREE,
                           cfcm_monitor_large_pool_event_cb );
    }
    break;

    default:
    {
      CFCM_MSG_1(ERROR, "cfcm_monitor_reg_with_dsm_large_pool invalid client %d", client);
      break;
    }
  }
} /* cfcm_monitor_reg_with_dsm_large_pool() */

/*==============================================================================

  FUNCTION:  cfcm_monitor_reg_with_dsm_dup_pool

==============================================================================*/
/*!
    @brief
    Register clients with DSM DUP pool.

    @return
    None
*/
/*============================================================================*/
static void cfcm_monitor_reg_with_dsm_dup_pool
(
  cfcm_client_e client   /*!<  client that needs to register */
)
{
  cfcm_dbg_s* cfcm_dbg_ptr;
  /*--------------------------------------------------------------------------*/

  /*--------------------------------------------------------------------------*/
  cfcm_dbg_ptr = cfcm_dbg_get_data();

  switch(client)
  {
    case CFCM_CLIENT_WCDMA_RLC_DL:
    {
      /* WCDMA_RLC_DL */
      dsm_reg_mem_event_level(DSM_DUP_ITEM_POOL,
                              DSM_MEM_LEVEL_RLC_FEW,
                              cfcm_dbg_ptr->dup_pool_rlc_dl_few);

      dsm_reg_mem_event_level(DSM_DUP_ITEM_POOL,
                              DSM_MEM_LEVEL_RLC_MANY,
                              cfcm_dbg_ptr->dup_pool_rlc_dl_many);

      dsm_reg_mem_event_optimized_level(DSM_DUP_ITEM_POOL,
                                        DSM_MEM_LEVEL_RLC_FEW,
                                        DSM_MEM_LEVEL_RLC_MANY);

      dsm_reg_mem_event_cb(DSM_DUP_ITEM_POOL,
                           DSM_MEM_LEVEL_RLC_FEW,
                           DSM_MEM_OP_NEW,
                           cfcm_monitor_dup_pool_event_cb );

      dsm_reg_mem_event_cb(DSM_DUP_ITEM_POOL,
                           DSM_MEM_LEVEL_RLC_MANY,
                           DSM_MEM_OP_FREE,
                           cfcm_monitor_dup_pool_event_cb );
    }
    break;

    case CFCM_CLIENT_A2_UL_PER:
    {
      /* A2_UL_PER */
      dsm_reg_mem_event_level(DSM_DUP_ITEM_POOL,
                              DSM_MEM_LEVEL_A2_UL_PER_FEW,
                              cfcm_dbg_ptr->dup_pool_a2_ul_per_few);

      dsm_reg_mem_event_level(DSM_DUP_ITEM_POOL,
                              DSM_MEM_LEVEL_A2_UL_PER_MANY,
                              cfcm_dbg_ptr->dup_pool_a2_ul_per_many);

      dsm_reg_mem_event_optimized_level(DSM_DUP_ITEM_POOL,
                                        DSM_MEM_LEVEL_A2_UL_PER_FEW,
                                        DSM_MEM_LEVEL_A2_UL_PER_MANY);

      dsm_reg_mem_event_cb(DSM_DUP_ITEM_POOL,
                           DSM_MEM_LEVEL_A2_UL_PER_FEW,
                           DSM_MEM_OP_NEW,
                           cfcm_monitor_dup_pool_event_cb );

      dsm_reg_mem_event_cb(DSM_DUP_ITEM_POOL,
                           DSM_MEM_LEVEL_A2_UL_PER_MANY,
                           DSM_MEM_OP_FREE,
                           cfcm_monitor_dup_pool_event_cb );
    }
    break;

    case CFCM_CLIENT_LTE_RLC_DL:
    case CFCM_CLIENT_LTE_PDCP_DL:
    {
      if((cfcm_monitor_ptr->monitors[CFCM_MONITOR_DSM_DUP_POOL].registered[CFCM_CLIENT_LTE_RLC_DL] == TRUE) || 
         (cfcm_monitor_ptr->monitors[CFCM_MONITOR_DSM_DUP_POOL].registered[CFCM_CLIENT_LTE_PDCP_DL] == TRUE))
      {
        CFCM_MSG_1(ERROR, "CFCM: dsm_dup_pool Ignore: Already register with DSM Levels %d ", client);
      }
      else
      {
        /* LTE RLC/PDCP DL */
        dsm_reg_mem_event_level(DSM_DUP_ITEM_POOL,
                                DSM_MEM_LEVEL_LTE_DNE,
                                cfcm_dbg_ptr->dup_pool_lte_dne);      
  
        dsm_reg_mem_event_level(DSM_DUP_ITEM_POOL,
                                DSM_MEM_LEVEL_LTE_DL_FEW,
                                cfcm_dbg_ptr->dup_pool_lte_dl_few);
  
        dsm_reg_mem_event_level(DSM_DUP_ITEM_POOL,
                                DSM_MEM_LEVEL_LTE_DL_MANY,
                                cfcm_dbg_ptr->dup_pool_lte_dl_many);
  
        dsm_reg_mem_event_optimized_level(DSM_DUP_ITEM_POOL,
                                          DSM_MEM_LEVEL_LTE_DL_FEW,
                                          DSM_MEM_LEVEL_LTE_DL_MANY);
  
        dsm_reg_mem_event_cb(DSM_DUP_ITEM_POOL,
                             DSM_MEM_LEVEL_LTE_DNE,
                             DSM_MEM_OP_NEW,
                             cfcm_monitor_dup_pool_event_cb );
  
        dsm_reg_mem_event_cb(DSM_DUP_ITEM_POOL,
                             DSM_MEM_LEVEL_LTE_DL_FEW,
                             DSM_MEM_OP_NEW,
                             cfcm_monitor_dup_pool_event_cb );
  
        dsm_reg_mem_event_cb(DSM_DUP_ITEM_POOL,
                             DSM_MEM_LEVEL_LTE_DL_MANY,
                             DSM_MEM_OP_FREE,
                             cfcm_monitor_dup_pool_event_cb );
      }
    }
    break;

    case CFCM_CLIENT_LTE_ML1_CPU:
    {
      /* LTE ML1 CPU */
      dsm_reg_mem_event_level(DSM_DUP_ITEM_POOL,
                              DSM_MEM_LEVEL_LTE_ML1_CPU_DNE,
                              cfcm_dbg_ptr->dup_pool_lte_ml1_cpu_dne);      

      dsm_reg_mem_event_level(DSM_DUP_ITEM_POOL,
                              DSM_MEM_LEVEL_LTE_ML1_CPU_FEW,
                              cfcm_dbg_ptr->dup_pool_lte_ml1_cpu_few);

      dsm_reg_mem_event_level(DSM_DUP_ITEM_POOL,
                              DSM_MEM_LEVEL_LTE_ML1_CPU_MANY,
                              cfcm_dbg_ptr->dup_pool_lte_ml1_cpu_many);

      dsm_reg_mem_event_optimized_level(DSM_DUP_ITEM_POOL,
                                        DSM_MEM_LEVEL_LTE_ML1_CPU_FEW,
                                        DSM_MEM_LEVEL_LTE_ML1_CPU_MANY);

      dsm_reg_mem_event_cb(DSM_DUP_ITEM_POOL,
                           DSM_MEM_LEVEL_LTE_ML1_CPU_DNE,
                           DSM_MEM_OP_NEW,
                           cfcm_monitor_dup_pool_event_cb );

      dsm_reg_mem_event_cb(DSM_DUP_ITEM_POOL,
                           DSM_MEM_LEVEL_LTE_ML1_CPU_FEW,
                           DSM_MEM_OP_NEW,
                           cfcm_monitor_dup_pool_event_cb );

      dsm_reg_mem_event_cb(DSM_DUP_ITEM_POOL,
                           DSM_MEM_LEVEL_LTE_ML1_CPU_MANY,
                           DSM_MEM_OP_FREE,
                           cfcm_monitor_dup_pool_event_cb );
    }
    break;

    case CFCM_CLIENT_LTE_RLC_LEVEL1_DL:
    {
      /* LTE_RLC_LEVEL1_DL */
      dsm_reg_mem_event_level(DSM_DUP_ITEM_POOL,
                              DSM_MEM_LEVEL_LTE_DL_LEVEL1_FEW,
                              cfcm_dbg_ptr->dup_pool_lte_dl_level1_few);

      dsm_reg_mem_event_level(DSM_DUP_ITEM_POOL,
                              DSM_MEM_LEVEL_LTE_DL_LEVEL1_MANY,
                              cfcm_dbg_ptr->dup_pool_lte_dl_level1_many);

      dsm_reg_mem_event_optimized_level(DSM_DUP_ITEM_POOL,
                                        DSM_MEM_LEVEL_LTE_DL_LEVEL1_FEW,
                                        DSM_MEM_LEVEL_LTE_DL_LEVEL1_MANY);

      dsm_reg_mem_event_cb(DSM_DUP_ITEM_POOL,
                           DSM_MEM_LEVEL_LTE_DL_LEVEL1_FEW,
                           DSM_MEM_OP_NEW,
                           cfcm_monitor_dup_pool_event_cb );

      dsm_reg_mem_event_cb(DSM_DUP_ITEM_POOL,
                           DSM_MEM_LEVEL_LTE_DL_LEVEL1_MANY,
                           DSM_MEM_OP_FREE,
                           cfcm_monitor_dup_pool_event_cb );
    }
    break;

    case CFCM_CLIENT_LTE_PDCP_COMP:
    {
      /* LTE_PDCP_COMP */
      dsm_reg_mem_event_level(DSM_DUP_ITEM_POOL,
                              DSM_MEM_LEVEL_LTE_PDCP_COMP_DNE,
                              cfcm_dbg_ptr->dup_pool_lte_pdcp_comp_dne);      

      dsm_reg_mem_event_level(DSM_DUP_ITEM_POOL,
                              DSM_MEM_LEVEL_LTE_PDCP_COMP_FEW,
                              cfcm_dbg_ptr->dup_pool_lte_pdcp_comp_few);

      dsm_reg_mem_event_level(DSM_DUP_ITEM_POOL,
                              DSM_MEM_LEVEL_LTE_PDCP_COMP_MANY,
                              cfcm_dbg_ptr->dup_pool_lte_pdcp_comp_many);

      dsm_reg_mem_event_optimized_level(DSM_DUP_ITEM_POOL,
                                        DSM_MEM_LEVEL_LTE_PDCP_COMP_FEW,
                                        DSM_MEM_LEVEL_LTE_PDCP_COMP_MANY);

      dsm_reg_mem_event_cb(DSM_DUP_ITEM_POOL,
                           DSM_MEM_LEVEL_LTE_PDCP_COMP_DNE,
                           DSM_MEM_OP_NEW,
                           cfcm_monitor_dup_pool_event_cb );

      dsm_reg_mem_event_cb(DSM_DUP_ITEM_POOL,
                           DSM_MEM_LEVEL_LTE_PDCP_COMP_FEW,
                           DSM_MEM_OP_NEW,
                           cfcm_monitor_dup_pool_event_cb );

      dsm_reg_mem_event_cb(DSM_DUP_ITEM_POOL,
                           DSM_MEM_LEVEL_LTE_PDCP_COMP_MANY,
                           DSM_MEM_OP_FREE,
                           cfcm_monitor_dup_pool_event_cb );
    }
    break;

    default:
    {
      CFCM_MSG_1(ERROR, "cfcm_monitor_reg_with_dsm_dup_pool invalid client %d", client);
      break;
    }
  }
} /* cfcm_monitor_reg_with_dsm_dup_pool() */

/*==============================================================================

  FUNCTION:  cfcm_monitor_reg_with_dsm_small_pool

==============================================================================*/
/*!
    @brief
    Register clients with DSM SMALL pool.

    @return
    None
*/
/*============================================================================*/
static void cfcm_monitor_reg_with_dsm_small_pool
(
  cfcm_client_e client   /*!<  client that needs to register */
)
{
  cfcm_dbg_s* cfcm_dbg_ptr;
  /*--------------------------------------------------------------------------*/

  /*--------------------------------------------------------------------------*/
  cfcm_dbg_ptr = cfcm_dbg_get_data();

  switch(client)
  {
    case CFCM_CLIENT_WCDMA_RLC_DL:
    {
      /* WCDMA_RLC_DL */
      dsm_reg_mem_event_level(DSM_DS_SMALL_ITEM_POOL,
                              DSM_MEM_LEVEL_LINK_LAYER_DNE,
                              cfcm_dbg_ptr->small_pool_rlc_dne);      

      dsm_reg_mem_event_level(DSM_DS_SMALL_ITEM_POOL,
                              DSM_MEM_LEVEL_RLC_FEW,
                              cfcm_dbg_ptr->small_pool_rlc_dl_few);

      dsm_reg_mem_event_level(DSM_DS_SMALL_ITEM_POOL,
                              DSM_MEM_LEVEL_RLC_MANY,
                              cfcm_dbg_ptr->small_pool_rlc_dl_many);

      dsm_reg_mem_event_optimized_level(DSM_DS_SMALL_ITEM_POOL,
                                        DSM_MEM_LEVEL_RLC_FEW,
                                        DSM_MEM_LEVEL_RLC_MANY);

      dsm_reg_mem_event_cb(DSM_DS_SMALL_ITEM_POOL,
                           DSM_MEM_LEVEL_LINK_LAYER_DNE,
                           DSM_MEM_OP_NEW,
                           cfcm_monitor_small_pool_event_cb );

      dsm_reg_mem_event_cb(DSM_DS_SMALL_ITEM_POOL,
                           DSM_MEM_LEVEL_RLC_FEW,
                           DSM_MEM_OP_NEW,
                           cfcm_monitor_small_pool_event_cb );

      dsm_reg_mem_event_cb(DSM_DS_SMALL_ITEM_POOL,
                           DSM_MEM_LEVEL_RLC_MANY,
                           DSM_MEM_OP_FREE,
                           cfcm_monitor_small_pool_event_cb );
    }
    break;

    default:
    {
      CFCM_MSG_1(ERROR, "cfcm_monitor_reg_with_dsm_small_pool invalid client %d", client);
      break;
    }
  }
} /* cfcm_monitor_reg_with_dsm_small_pool() */

/*==============================================================================

  FUNCTION:  cfcm_monitor_reg_with_dsm_large_pool_level1

==============================================================================*/
/*!
    @brief
    Register clients with DSM LARGE pool Level1

    @return
    None
*/
/*============================================================================*/
static void cfcm_monitor_reg_with_dsm_large_pool_level1
(
  cfcm_client_e client   /*!<  client that needs to register */
)
{
  cfcm_dbg_s* cfcm_dbg_ptr;
  /*--------------------------------------------------------------------------*/

  /*--------------------------------------------------------------------------*/
  cfcm_dbg_ptr = cfcm_dbg_get_data();

  switch(client)
  {
    case CFCM_CLIENT_UL_PER :
    {
      /* large pool */
      dsm_reg_mem_event_level(DSM_DS_LARGE_ITEM_POOL,
                              DSM_MEM_LEVEL_IPA_DL_FEW,
                              cfcm_dbg_ptr->large_pool_ipa_dl_few);

      dsm_reg_mem_event_level(DSM_DS_LARGE_ITEM_POOL,
                              DSM_MEM_LEVEL_IPA_DL_MANY,
                              cfcm_dbg_ptr->large_pool_ipa_dl_many);

      dsm_reg_mem_event_optimized_level(DSM_DS_LARGE_ITEM_POOL,
                                        DSM_MEM_LEVEL_IPA_DL_FEW,
                                        DSM_MEM_LEVEL_IPA_DL_MANY);

      dsm_reg_mem_event_cb(DSM_DS_LARGE_ITEM_POOL,
                           DSM_MEM_LEVEL_IPA_DL_FEW,
                           DSM_MEM_OP_NEW,
                           cfcm_monitor_large_pool_level1_event_cb );

      dsm_reg_mem_event_cb(DSM_DS_LARGE_ITEM_POOL,
                           DSM_MEM_LEVEL_IPA_DL_MANY,
                           DSM_MEM_OP_FREE,
                           cfcm_monitor_large_pool_level1_event_cb );
    }
    break;

    default:
    {
      CFCM_MSG_1(ERROR, "cfcm_monitor_reg_with_dsm_large_pool_level1 invalid client %d", client);
      break;
    }
  }
} /* cfcm_monitor_reg_with_dsm_large_pool_level1() */


/*==============================================================================

  FUNCTION:  cfcm_monitor_reg_with_dsm_pool

==============================================================================*/
/*!
    @brief
    Register with DSM pools.

    @return
    None
*/
/*============================================================================*/
void cfcm_monitor_reg_with_dsm_pool
(
  cfcm_monitor_e   monitor,   /*!< client that triggered this registration */
  cfcm_client_e client   /*!< the client id */
)
{
  /*--------------------------------------------------------------------------*/

  /*--------------------------------------------------------------------------*/

  switch(monitor)
  {
    /* Register for LARGE POOL */
    case CFCM_MONITOR_DSM_LARGE_POOL:
    {
      cfcm_monitor_reg_with_dsm_large_pool(client);
    }
    break;

    /* Register for DUP POOL */
    case CFCM_MONITOR_DSM_DUP_POOL:
    {
      cfcm_monitor_reg_with_dsm_dup_pool(client);
    }
    break;

    /* Register for SMALL POOL */
    case CFCM_MONITOR_DSM_SMALL_POOL:
    {
      cfcm_monitor_reg_with_dsm_small_pool(client);
    }
    break;

    /* Register for LARGE POOL */
    case CFCM_MONITOR_DSM_LARGE_POOL_LEVEL1:
    {
      cfcm_monitor_reg_with_dsm_large_pool_level1(client);
    }
    break;

    /* Should not come here */
    default:
    {
      CFCM_ERR_FATAL("cfcm_monitor_reg_with_dsm_pool invalid monitor %d client %d", monitor, client, 0);
    }
    break;
  }

} /* cfcm_monitor_reg_with_dsm_pool() */

/*==============================================================================

  FUNCTION:  cfcm_monitor_check_monitor_reg_once

==============================================================================*/
/*!
    @brief
    Return TRUE in case a monitor is registered for a client

    @return
    TRUE/FALSE
*/
/*============================================================================*/
static boolean cfcm_monitor_check_monitor_reg_once
(
  cfcm_monitor_e  monitor /*!< monitor needs to be registered with the source */
)
{

  uint32 i;
  /*--------------------------------------------------------------------------*/

  /*--------------------------------------------------------------------------*/
  for(i = 0; i < (uint32)CFCM_CLIENT_MAX; i++)
  {
    if(cfcm_monitor_ptr->monitors[monitor].registered[i])
    {
      return TRUE;
    }
  }
  return FALSE;
} /* cfcm_monitor_reg_with_thermal() */

/*==============================================================================

  FUNCTION:  cfcm_monitor_reg_with_thermal

==============================================================================*/
/*!
    @brief
    Register with thermal power amplifer (PA) monitor.

    @return
    None
*/
/*============================================================================*/
void cfcm_monitor_reg_with_thermal( void )
{

  /*--------------------------------------------------------------------------*/

  /*--------------------------------------------------------------------------*/

  if (cfcm_monitor_check_monitor_reg_once(CFCM_MONITOR_THERMAL_PA))
  {
    return;
  }
  npa_resource_available_cb(CFCM_MONITOR_THERMAL_PA_NPA_NODE,
                            cfcm_monitor_npa_available_cb,
                            NULL);
  CFCM_MSG_0(HIGH, "CFCM thermal monitor registers with NPA");
} /* cfcm_monitor_reg_with_thermal() */

/*==============================================================================

  FUNCTION:  cfcm_monitor_reg_with_thermal_ra

==============================================================================*/
/*!
    @brief
    Register with thermal Runaway (RA) monitor.

    @return
    None
*/
/*============================================================================*/
void cfcm_monitor_reg_with_thermal_ra( void )
{

  /*--------------------------------------------------------------------------*/

  /*--------------------------------------------------------------------------*/
  if (cfcm_monitor_check_monitor_reg_once(CFCM_MONITOR_THERMAL_CX))
  {
    return;
  }

  npa_resource_available_cb(CFCM_MONITOR_THERMAL_RA_NPA_NODE,
                            cfcm_monitor_thermal_ra_available_cb,
                            NULL);
  CFCM_MSG_0(HIGH, "CFCM thermal Runaway monitor registers with NPA");
} /* cfcm_monitor_reg_with_thermal_ra() */

/*==============================================================================

  FUNCTION:  cfcm_monitor_reg_with_mdm_temp

==============================================================================*/
/*!
    @brief
    Register with thermal MDM Temp monitor.

    @return
    None
*/
/*============================================================================*/
void cfcm_monitor_reg_with_mdm_temp( void )
{

  /*--------------------------------------------------------------------------*/

  /*--------------------------------------------------------------------------*/
  if (cfcm_monitor_check_monitor_reg_once(CFCM_MONITOR_MDM_TEMP))
  {
    return;
  }

  npa_resource_available_cb(CFCM_MONITOR_MDM_TEMP_NPA_NODE,
                            cfcm_monitor_mdm_temp_available_cb,
                            NULL);
  CFCM_MSG_0(HIGH, "CFCM thermal MDM Temp monitor registers with NPA");
} /* cfcm_monitor_reg_with_mdm_temp() */

/*==============================================================================

  FUNCTION:  cfcm_monitor_reg_with_vdd_peak_curr

==============================================================================*/
/*!
    @brief
    Register with VDD Peak Current monitor.

    @return
    None
*/
/*============================================================================*/
void cfcm_monitor_reg_with_vdd_peak_curr( void )
{

  /*--------------------------------------------------------------------------*/

  /*--------------------------------------------------------------------------*/
  if (cfcm_monitor_check_monitor_reg_once(CFCM_MONITOR_VDD_PEAK_CURR_EST))
  {
    return;
  }

  npa_resource_available_cb(CFCM_MONITOR_VDD_PEAK_CURR_NPA_NODE,
                            cfcm_monitor_vdd_peak_curr_available_cb,
                            NULL);
  CFCM_MSG_0(HIGH, "CFCM VDD Peak Current monitor registers with NPA");
} /* cfcm_monitor_reg_with_vdd_peak_curr() */

/*==============================================================================

  FUNCTION:  cfcm_monitor_register_with_src

==============================================================================*/
/*!
    @brief
    Register with the source monitor typcially via a callback function.

    @return
    None
*/
/*============================================================================*/
void cfcm_monitor_register_with_src
(
  cfcm_monitor_e  monitor, /*!< monitor needs to be registered with the source */
  cfcm_client_e client   /*!< the client id */
)
{
  /*--------------------------------------------------------------------------*/

  /*--------------------------------------------------------------------------*/
  if(cfcm_cfg_monitor_enabled(monitor))
  {
    switch(monitor)
    {
      case CFCM_MONITOR_DSM_LARGE_POOL:
      case CFCM_MONITOR_DSM_DUP_POOL:
      case CFCM_MONITOR_DSM_SMALL_POOL:
      case CFCM_MONITOR_DSM_LARGE_POOL_LEVEL1:
      {
        cfcm_monitor_reg_with_dsm_pool(monitor, client);
        break;
      }
      case CFCM_MONITOR_THERMAL_PA:
      {
        cfcm_monitor_reg_with_thermal();
        break;
      }
      case CFCM_MONITOR_THERMAL_CX:
      {
        cfcm_monitor_reg_with_thermal_ra();
        break;
      }
      case CFCM_MONITOR_CPU:
      {
        break;
      }
      case CFCM_MONITOR_BW_THROTTLING:
      {
        break;
      }
      case CFCM_MONITOR_MDM_TEMP:
      {
        cfcm_monitor_reg_with_mdm_temp();
        break;
      }
      case CFCM_MONITOR_VDD_PEAK_CURR_EST:
      {
        cfcm_monitor_reg_with_vdd_peak_curr();
        break;
      }
      default:
      {
        CFCM_ERR_FATAL("cfcm_monitor_register_with_src invalid monitor %d", monitor, 0, 0);
        break;
      }
    }
    cfcm_monitor_ptr->monitors[(uint32)monitor].registered[(uint32)client] = TRUE;
  }
  else
  {
    CFCM_MSG_2(HIGH, "CFCM monintor =%d client = %d not enabled, skip registration",
              (uint32)monitor, (uint32)client);
  }
} /* cfcm_monitor_register_with_src() */

/*==============================================================================

                                FUNCTIONS

==============================================================================*/

/*==============================================================================

  FUNCTION:  cfcm_monitor_init

==============================================================================*/
/*!
    @brief
    initialized internal data structure at start-up.

    @return
    None
*/
/*============================================================================*/
EXTERN void cfcm_monitor_init( void )
{
  uint32 i;
  /*--------------------------------------------------------------------------*/

  /*--------------------------------------------------------------------------*/
  for(i = 0; i < (uint32)CFCM_MONITOR_MAX; i++)
  {
    cfcm_monitor_init_monitor((cfcm_monitor_e)i);
  }

  cfcm_monitor_bus_bw_init();

  cfcm_monitor_ptr->num_errors = 0;

} /* cfcm_monitor_init() */

/*=============================================================================

  FUNCTION:  cfcm_monitor_deinit

=============================================================================*/
/*!
    @brief
        De-Initializes CFCM QMI layer's functionalities
 
    @return
        None
*/
/*===========================================================================*/
void cfcm_monitor_deinit 
(
  void
)
{
  /* Turn off the service */
  mqcsi_conn_mgr_service_close(MQCSI_CFCM_SERVICE_ID);
}

/*==============================================================================

  FUNCTION:  cfcm_monitor_proc_reg

==============================================================================*/
/*!
    @brief
    update monitor internals info in response to client registration.

    @return
    None
*/
/*============================================================================*/
EXTERN void cfcm_monitor_proc_reg
(
  const cfcm_reg_req_type_s*     msg_ptr/*!< registration message */
)
{
  uint32 i;
  /*--------------------------------------------------------------------------*/

  /*--------------------------------------------------------------------------*/
  for(i = 0; i < (uint32)CFCM_MONITOR_MAX; i++)
  {
    if(CFCM_MONITOR_IN_MASK(msg_ptr->monitor_mask, i))
    {
      /* first time monitor is set up */
      cfcm_monitor_ptr->monitors[i].num_client++;
      cfcm_monitor_ptr->monitors[i].client[(uint32)msg_ptr->client_id] = 
      msg_ptr->client_id;

      if(!cfcm_monitor_ptr->monitors[i].registered[(uint32)msg_ptr->client_id])
      {
        cfcm_monitor_register_with_src(
          cfcm_monitor_ptr->monitors[i].monitor_id, msg_ptr->client_id);
      }
    }
  }
} /* cfcm_monitor_proc_reg() */

/*==============================================================================

  FUNCTION:  cfcm_monitor_proc_dereg

==============================================================================*/
/*!
    @brief
    update the internal info in response to client deregistration message.

    @return
    None
*/
/*============================================================================*/
EXTERN void cfcm_monitor_proc_dereg
(
  const cfcm_dereg_req_type_s*     msg_ptr,/*!< deregistration message */
  uint32                         monitor_mask /*!< monitor mask */
)
{
  uint32 i;
  /*--------------------------------------------------------------------------*/

  /*--------------------------------------------------------------------------*/
  for(i = 0; i < (uint32)CFCM_MONITOR_MAX; i++)
  {
    if(CFCM_MONITOR_IN_MASK(monitor_mask, i))
    {
      if(cfcm_monitor_ptr->monitors[i].client[(uint32)msg_ptr->client_id] == 
         msg_ptr->client_id)
      {
        CFCM_ASSERT(cfcm_monitor_ptr->monitors[i].num_client > 0);
        cfcm_monitor_ptr->monitors[i].num_client--;
        cfcm_monitor_ptr->monitors[i].client[(uint32)msg_ptr->client_id] = 
          CFCM_CLIENT_MAX;
      }
      else
      {
        CFCM_MSG_1(ERROR, "Deregistration for a client=%d already deregisted: Ignored",
                  (uint32)msg_ptr->client_id);
        cfcm_monitor_ptr->num_errors++;
      }
    }
  }
} /* cfcm_monitor_proc_dereg() */


/*==============================================================================

  FUNCTION:  cfcm_shim_get_merged_cmd

==============================================================================*/
/*!
    @brief
    compute the flow control command based on the state of monitors specified.

    @detail
    for a given set of monitors a client is designed to respond to, 
    calculate the correct flow control command, and for DOWN command, also
    calculate the step timer.

    @return
    None
*/
/*============================================================================*/
static void cfcm_shim_get_merged_cmd
(
  cfcm_cmd_type_s* fc_cmd_ptr, /*!<pointer to the flow control cmd result output */
  uint32*          cmd,              /*!< cmd */
  uint32*          monitor_active,   /*!< the monitor Active */
  cfcm_monitor_e   monitor_id,       /*!< the set of monitors to be considered */
  cfcm_cmd_e       tmp_cmd_old,      /*!< the client id */
  uint32           step_timer
)
{
  /*--------------------------------------------------------------------------*/

  /*--------------------------------------------------------------------------*/

  /* Findout Merged command for a client */
  if (*cmd == (uint32)tmp_cmd_old)
  {
    *monitor_active |= CFCM_CONV_ENUM_TO_BIT_MASK(monitor_id);
  }
  else if (*cmd < (uint32)tmp_cmd_old)
  {
    /* find the common denominator, so to speak */
    *cmd |= (uint32)tmp_cmd_old;
    *monitor_active = CFCM_CONV_ENUM_TO_BIT_MASK(monitor_id);
  }

  if((*cmd == CFCM_CMD_DOWN) && (tmp_cmd_old == CFCM_CMD_DOWN))
  {
    /* take the minimum of step timer for DOWN command */
    if(fc_cmd_ptr->step_timer > step_timer)
    {
      fc_cmd_ptr->step_timer = step_timer;
    }
  }
  return;
} /* cfcm_shim_get_merged_cmd() */

/*==============================================================================

  FUNCTION:  cfcm_monitor_compute_fc_cmd

==============================================================================*/
/*!
    @brief
    compute the flow control command based on the state of monitors specified.

    @detail
    for a given set of monitors a client is designed to respond to, 
    calculate the correct flow control command, and for DOWN command, also
    calculate the step timer.

    @return
    None
*/
/*============================================================================*/
static void cfcm_shim_get_backward_compatible_cmd
(
  cfcm_cmd_type_s* fc_cmd_ptr,       /*!<pointer to the flow control cmd result output */
  uint32*          cmd,              /*!< cmd */
  uint32*          monitor_active,   /*!< the monitor Active */
  cfcm_monitor_e   new_monitor_id,   /*!< the set of monitors to be considered */
  cfcm_client_e    client,           /*!< the client id */
  uint32           step_timer
)
{
  cfcm_cmd_e                     tmp_cmd;
  cfcm_monitor_last_cmd_type_s*  last_cmd; /*!<  last cmd from this monitor per client */
  uint32                 old_monitor_id = 0xFF;
  uint32 shim_monitor_mask = cfcm_shim_get_client_shim_monitor_mask(client);

  /*--------------------------------------------------------------------------*/
  CFCM_ASSERT(cmd != NULL);
  CFCM_ASSERT(monitor_active != NULL);
  if(!CFCM_IS_CLIENT_VALID(client))
    return;

  /*--------------------------------------------------------------------------*/
  tmp_cmd = CFCM_CMD_FC_OFF;

    last_cmd = &cfcm_monitor_ptr->monitors[new_monitor_id].last_cmd[client];
    if(last_cmd->cmd_updated)
    {
      switch (new_monitor_id)
      {
        case CFCM_MONITOR_CPU:
        {
          if (CFCM_BIT_PRESENT_IN_MASK(shim_monitor_mask, CFCM_MONITOR_CPU))
          {
            tmp_cmd = last_cmd->cmd;
            cfcm_shim_get_merged_cmd(fc_cmd_ptr, cmd, monitor_active, 
                           CFCM_MONITOR_CPU, tmp_cmd, step_timer);
          }
        }
        break;

        case CFCM_MONITOR_THERMAL_PA:
        {
          if (CFCM_BIT_PRESENT_IN_MASK(shim_monitor_mask, CFCM_MONITOR_THERMAL_PA))
          {
            tmp_cmd = cfcm_shim_thermal_pa_state_map[last_cmd->monitor_data.npa_node.state][0];
            cfcm_shim_get_merged_cmd(fc_cmd_ptr, cmd, monitor_active, 
                           CFCM_MONITOR_THERMAL_PA, tmp_cmd, step_timer);
          }

          tmp_cmd = CFCM_CMD_FC_OFF;
          if (CFCM_BIT_PRESENT_IN_MASK(shim_monitor_mask, CFCM_MONITOR_THERMAL_PA_EX))
          {
            tmp_cmd = cfcm_shim_thermal_pa_state_map[last_cmd->monitor_data.npa_node.state][1];
            cfcm_shim_get_merged_cmd(fc_cmd_ptr, cmd, monitor_active, 
                           CFCM_MONITOR_THERMAL_PA_EX, tmp_cmd, step_timer);
          }

          tmp_cmd = CFCM_CMD_FREEZE;
          if (CFCM_BIT_PRESENT_IN_MASK(shim_monitor_mask, CFCM_MONITOR_THERMAL_PA_EM))
          {
            tmp_cmd = cfcm_shim_thermal_pa_state_map[last_cmd->monitor_data.npa_node.state][2];
            if (tmp_cmd != CFCM_CMD_FREEZE)
            {
              cfcm_shim_get_merged_cmd(fc_cmd_ptr, cmd, monitor_active, 
                           CFCM_MONITOR_THERMAL_PA_EM, tmp_cmd, step_timer);
            }
          }
        }
        break;

        case CFCM_MONITOR_THERMAL_CX:
        {
          if (CFCM_BIT_PRESENT_IN_MASK(shim_monitor_mask, CFCM_MONITOR_THERMAL_RUNAWAY))
          {
            tmp_cmd = cfcm_shim_npa_node_state_map[last_cmd->monitor_data.npa_node.state][0];
            cfcm_shim_get_merged_cmd(fc_cmd_ptr, cmd, monitor_active, 
                           CFCM_MONITOR_THERMAL_RUNAWAY, tmp_cmd, step_timer);
          }

          tmp_cmd = CFCM_CMD_FC_OFF;
          if (CFCM_BIT_PRESENT_IN_MASK(shim_monitor_mask, CFCM_MONITOR_THERMAL_RUNAWAY_EX))
          {
            tmp_cmd = cfcm_shim_npa_node_state_map[last_cmd->monitor_data.npa_node.state][1];
            cfcm_shim_get_merged_cmd(fc_cmd_ptr, cmd, monitor_active, 
                           CFCM_MONITOR_THERMAL_RUNAWAY_EX, tmp_cmd, step_timer);
          }
        }
        break;

        case CFCM_MONITOR_MDM_TEMP:
        {
          if (CFCM_BIT_PRESENT_IN_MASK(shim_monitor_mask, CFCM_MONITOR_MDM_TEMP))
          {
            tmp_cmd = cfcm_shim_npa_node_state_map[last_cmd->monitor_data.npa_node.state][0];
            cfcm_shim_get_merged_cmd(fc_cmd_ptr, cmd, monitor_active, 
                           CFCM_MONITOR_MDM_TEMP, tmp_cmd, step_timer);
          }

          tmp_cmd = CFCM_CMD_FC_OFF;
          if (CFCM_BIT_PRESENT_IN_MASK(shim_monitor_mask, CFCM_MONITOR_MDM_TEMP_EX))
          {
            tmp_cmd = cfcm_shim_npa_node_state_map[last_cmd->monitor_data.npa_node.state][1];
            cfcm_shim_get_merged_cmd(fc_cmd_ptr, cmd, monitor_active, 
                           CFCM_MONITOR_MDM_TEMP_EX, tmp_cmd, step_timer);
          }
        }
        break;

        case CFCM_MONITOR_VDD_PEAK_CURR_EST:
        {
          if (CFCM_BIT_PRESENT_IN_MASK(shim_monitor_mask, CFCM_MONITOR_VDD_PEAK_CURR_EST))
          {
            tmp_cmd = cfcm_shim_npa_node_state_map[last_cmd->monitor_data.npa_node.state][0];
            cfcm_shim_get_merged_cmd(fc_cmd_ptr, cmd, monitor_active, 
                           CFCM_MONITOR_VDD_PEAK_CURR_EST, tmp_cmd, step_timer);
          }

          tmp_cmd = CFCM_CMD_FC_OFF;
          if (CFCM_BIT_PRESENT_IN_MASK(shim_monitor_mask, CFCM_MONITOR_VDD_PEAK_CURR_EST_EX))
          {
            tmp_cmd = cfcm_shim_npa_node_state_map[last_cmd->monitor_data.npa_node.state][1];
            cfcm_shim_get_merged_cmd(fc_cmd_ptr, cmd, monitor_active, 
                           CFCM_MONITOR_VDD_PEAK_CURR_EST_EX, tmp_cmd, step_timer);
          }
        }
        break;

        case CFCM_MONITOR_DSM_LARGE_POOL:
        {
          if ( last_cmd->monitor_data.dsm.level== CFCM_DSM_MEM_LEVEL_MANY)
          {
            tmp_cmd = CFCM_CMD_FC_OFF;
          }
          else if ( last_cmd->monitor_data.dsm.level== CFCM_DSM_MEM_LEVEL_FEW)
          {
            tmp_cmd = CFCM_CMD_DOWN;
          }
          else if ( last_cmd->monitor_data.dsm.level== CFCM_DSM_MEM_LEVEL_DNE)
          {
            tmp_cmd = CFCM_CMD_SHUT_DOWN;
          }

          if (CFCM_BIT_PRESENT_IN_MASK(shim_monitor_mask, CFCM_MONITOR_DSM_LARGE_POOL_UL))
          {
            old_monitor_id = (uint32)CFCM_MONITOR_DSM_LARGE_POOL_UL;
          }
          else if (CFCM_BIT_PRESENT_IN_MASK(shim_monitor_mask, CFCM_MONITOR_DSM_LARGE_POOL_DL))
          {
            old_monitor_id = (uint32)CFCM_MONITOR_DSM_LARGE_POOL_DL;
          }
          else if (CFCM_BIT_PRESENT_IN_MASK(shim_monitor_mask, CFCM_MONITOR_DSM_LARGE_POOL_LEVEL1_DL))
          {
            old_monitor_id = (uint32)CFCM_MONITOR_DSM_LARGE_POOL_LEVEL1_DL;
          }

          if (old_monitor_id != (0xFF))
          {
            cfcm_shim_get_merged_cmd(fc_cmd_ptr, cmd, monitor_active, 
                             (cfcm_monitor_e)old_monitor_id, tmp_cmd, step_timer);
          }

        }
        break;

        case CFCM_MONITOR_DSM_LARGE_POOL_LEVEL1:
        {
          if ( last_cmd->monitor_data.dsm.level== CFCM_DSM_MEM_LEVEL_MANY)
          {
            tmp_cmd = CFCM_CMD_FC_OFF;
          }
          else if ( last_cmd->monitor_data.dsm.level== CFCM_DSM_MEM_LEVEL_FEW)
          {
            tmp_cmd = CFCM_CMD_DOWN;
          }
          else if ( last_cmd->monitor_data.dsm.level== CFCM_DSM_MEM_LEVEL_DNE)
          {
            tmp_cmd = CFCM_CMD_SHUT_DOWN;
          }

          if (CFCM_BIT_PRESENT_IN_MASK(shim_monitor_mask, CFCM_MONITOR_DSM_LARGE_POOL_IPA_DL))
          {
            old_monitor_id = (uint32)CFCM_MONITOR_DSM_LARGE_POOL_IPA_DL;
          }

          if (old_monitor_id != (0xFF))
          {
            cfcm_shim_get_merged_cmd(fc_cmd_ptr, cmd, monitor_active, 
                             (cfcm_monitor_e)old_monitor_id, tmp_cmd, step_timer);
          }

        }
        break;

        case CFCM_MONITOR_DSM_DUP_POOL:
        {
          if ( last_cmd->monitor_data.dsm.level== CFCM_DSM_MEM_LEVEL_MANY)
          {
            tmp_cmd = CFCM_CMD_FC_OFF;
          }
          else if ( last_cmd->monitor_data.dsm.level== CFCM_DSM_MEM_LEVEL_FEW)
          {
            tmp_cmd = CFCM_CMD_DOWN;
          }
          else if ( last_cmd->monitor_data.dsm.level== CFCM_DSM_MEM_LEVEL_DNE)
          {
            tmp_cmd = CFCM_CMD_SHUT_DOWN;
          }

          if (CFCM_BIT_PRESENT_IN_MASK(shim_monitor_mask, CFCM_MONITOR_DSM_DUP_POOL_UL))
          {
            old_monitor_id = (uint32)CFCM_MONITOR_DSM_DUP_POOL_UL;
          }
          else if (CFCM_BIT_PRESENT_IN_MASK(shim_monitor_mask, CFCM_MONITOR_DSM_DUP_POOL_DL))
          {
            old_monitor_id = (uint32)CFCM_MONITOR_DSM_DUP_POOL_DL;
          }
          else if (CFCM_BIT_PRESENT_IN_MASK(shim_monitor_mask, CFCM_MONITOR_DSM_DUP_POOL_LEVEL1_DL))
          {
            old_monitor_id = (uint32)CFCM_MONITOR_DSM_DUP_POOL_LEVEL1_DL;
          }

          if (old_monitor_id != (0xFF))
          {
            cfcm_shim_get_merged_cmd(fc_cmd_ptr, cmd, monitor_active, 
                             (cfcm_monitor_e)old_monitor_id, tmp_cmd, step_timer);
          }

        }
        break;

        case CFCM_MONITOR_DSM_SMALL_POOL:
        {
          if ( last_cmd->monitor_data.dsm.level== CFCM_DSM_MEM_LEVEL_MANY)
          {
            tmp_cmd = CFCM_CMD_FC_OFF;
          }
          else if ( last_cmd->monitor_data.dsm.level== CFCM_DSM_MEM_LEVEL_FEW)
          {
            tmp_cmd = CFCM_CMD_DOWN;
          }
          else if ( last_cmd->monitor_data.dsm.level== CFCM_DSM_MEM_LEVEL_DNE)
          {
            tmp_cmd = CFCM_CMD_SHUT_DOWN;
          }

          if (CFCM_BIT_PRESENT_IN_MASK(shim_monitor_mask, CFCM_MONITOR_DSM_SMALL_POOL_DL))
          {
            old_monitor_id = (uint32)CFCM_MONITOR_DSM_SMALL_POOL_DL;
          }

          if (old_monitor_id != (0xFF))
          {
            cfcm_shim_get_merged_cmd(fc_cmd_ptr, cmd, monitor_active, 
                             (cfcm_monitor_e)old_monitor_id, tmp_cmd, step_timer);
          }

        }
        break;

        case CFCM_MONITOR_BW_THROTTLING:
        {
          if (CFCM_BIT_PRESENT_IN_MASK(shim_monitor_mask, CFCM_MONITOR_BW_THROTTLING))
          {
            tmp_cmd = cfcm_shim_npa_node_state_map[last_cmd->monitor_data.npa_node.state][0];
            cfcm_shim_get_merged_cmd(fc_cmd_ptr, cmd, monitor_active, 
                           CFCM_MONITOR_BW_THROTTLING, tmp_cmd, step_timer);
          }
        }
        break;

        default:
        {
          CFCM_MSG_1(ERROR, "cfcm_monitor_get_backward_compatible_cmd Invalid Monitor ID %d ", new_monitor_id);
          break;
        }
      }
    }

  return;
} /* cfcm_monitor_compute_fc_cmd() */

/*==============================================================================

  FUNCTION:  cfcm_monitor_compute_fc_cmd

==============================================================================*/
/*!
    @brief
    compute the flow control command based on the state of monitors specified.

    @detail
    for a given set of monitors a client is designed to respond to, 
    calculate the correct flow control command, and for DOWN command, also
    calculate the step timer.

    @return
    None
*/
/*============================================================================*/
EXTERN void cfcm_monitor_compute_fc_cmd
(
  cfcm_cmd_type_s* fc_cmd_ptr, /*!<pointer to the flow control cmd result output */
  uint32        monitor_mask,   /*!< the set of monitors to be considered */
  cfcm_client_e client   /*!< the client id */
)
{
  uint32 i;
  cfcm_npa_node_data_type_s*     npa_ptr = NULL;
  cfcm_dsm_data_type_s*          dsm = NULL;
  uint32                         tmp_cmd, tmp_cmd_old;
  uint32                         tmp_monitor_active = 0;
  uint32                         tmp_step_timer; 
  cfcm_monitor_last_cmd_type_s*  last_cmd; /*!<  last cmd from this monitor per client */
  /*--------------------------------------------------------------------------*/

  CFCM_ASSERT(fc_cmd_ptr != NULL);

  /*--------------------------------------------------------------------------*/
  fc_cmd_ptr->monitors_mask = 0;
  memset(&fc_cmd_ptr->monitor_data, 0, sizeof(cfcm_monitor_data_s));
  tmp_cmd = (uint32)CFCM_CMD_FC_OFF;
  tmp_cmd_old = (uint32)CFCM_CMD_FC_OFF;
  fc_cmd_ptr->step_timer = CFCM_MONITOR_DEFAULT_STEP_TIMER;

  for(i = 0; i < (uint32)CFCM_MONITOR_MAX; i++)
  {
    if(CFCM_MONITOR_IN_MASK(monitor_mask, i) && cfcm_monitor_ptr->monitors[i].last_cmd[client].cmd_updated)
    {
      last_cmd = &cfcm_monitor_ptr->monitors[i].last_cmd[client];
      tmp_step_timer = cfcm_monitor_get_clients_step_timer_value(client, (cfcm_monitor_e)i);
      switch ((cfcm_monitor_e)i)
      {
        case CFCM_MONITOR_CPU:
        {
          fc_cmd_ptr->monitors_mask |= CFCM_CONV_ENUM_TO_BIT_MASK(CFCM_MONITOR_CPU);
          fc_cmd_ptr->monitor_data.cpu.cmd = cfcm_monitor_ptr->monitors[i].last_cmd[client].cmd;
          fc_cmd_ptr->monitor_data.cpu.load = cfcm_monitor_ptr->monitors[i].last_cmd[client].monitor_data.cpu_info.cpu_load;
          fc_cmd_ptr->monitor_data.cpu.step_timer = tmp_step_timer;
        }
        break;

        case CFCM_MONITOR_THERMAL_PA:
        case CFCM_MONITOR_THERMAL_CX:
        case CFCM_MONITOR_MDM_TEMP:
        case CFCM_MONITOR_VDD_PEAK_CURR_EST:
        {
          fc_cmd_ptr->monitors_mask |= CFCM_CONV_ENUM_TO_BIT_MASK(i);
          if ( i == (uint32)CFCM_MONITOR_THERMAL_PA)
          {
            npa_ptr = &fc_cmd_ptr->monitor_data.pa;
          }
          else if ( i == (uint32)CFCM_MONITOR_THERMAL_CX)
          {
            npa_ptr = &fc_cmd_ptr->monitor_data.cx;
          }
          else if ( i == (uint32)CFCM_MONITOR_MDM_TEMP)
          {
            npa_ptr = &fc_cmd_ptr->monitor_data.mdm_temp;
          }
          else if ( i == (uint32)CFCM_MONITOR_VDD_PEAK_CURR_EST)
          {
            npa_ptr = &fc_cmd_ptr->monitor_data.vdd_peak_curr;
          }

          /* Update Thermal Monitor data */
          npa_ptr->cmd = cfcm_monitor_ptr->monitors[i].last_cmd[client].cmd;
          npa_ptr->state = cfcm_monitor_ptr->monitors[i].last_cmd[client].monitor_data.npa_node.state;
          npa_ptr->step_timer = tmp_step_timer;
        }
        break;

        case CFCM_MONITOR_DSM_LARGE_POOL:
        case CFCM_MONITOR_DSM_DUP_POOL:
        case CFCM_MONITOR_DSM_SMALL_POOL:
        case CFCM_MONITOR_DSM_LARGE_POOL_LEVEL1:
        {
          fc_cmd_ptr->monitors_mask |= CFCM_CONV_ENUM_TO_BIT_MASK(i);
          if ( i == (uint32)CFCM_MONITOR_DSM_LARGE_POOL)
          {
            dsm = &fc_cmd_ptr->monitor_data.large_pool;
          }
          else if ( i == (uint32)CFCM_MONITOR_DSM_DUP_POOL)
          {
            dsm = &fc_cmd_ptr->monitor_data.dup_pool;
          }
          else if ( i == (uint32)CFCM_MONITOR_DSM_SMALL_POOL)
          {
            dsm = &fc_cmd_ptr->monitor_data.small_pool;
          }
          else if ( i == (uint32)CFCM_MONITOR_DSM_LARGE_POOL_LEVEL1)
          {
            dsm = &fc_cmd_ptr->monitor_data.large_pool_level1;
          }

          /* Update DSM Monitor data */
          dsm->cmd = cfcm_monitor_ptr->monitors[i].last_cmd[client].cmd;
          dsm->level = cfcm_monitor_ptr->monitors[i].last_cmd[client].monitor_data.dsm.level;
          dsm->step_timer = tmp_step_timer;
        }
        break;

        case CFCM_MONITOR_BW_THROTTLING:
        {
          fc_cmd_ptr->monitors_mask |= CFCM_CONV_ENUM_TO_BIT_MASK(i);
          fc_cmd_ptr->monitor_data.bw.cmd = cfcm_monitor_ptr->monitors[i].last_cmd[client].cmd;
          fc_cmd_ptr->monitor_data.bw.level = cfcm_monitor_ptr->monitors[i].last_cmd[client].monitor_data.bw.level;
          fc_cmd_ptr->monitor_data.bw.step_timer = tmp_step_timer;
        }
        break;

        default:
        {
          CFCM_MSG_1(ERROR, "cfcm_monitor_proc_update Invalid invalid Monitor ID %d ", i);
          return;
        }
      }

      if (CFCM_IS_CLIENT_VALID(client) && CFCM_CLIENT_REQUIRED_MERGED_CMD(client))
      {
        cfcm_shim_get_backward_compatible_cmd(fc_cmd_ptr, &tmp_cmd, &tmp_monitor_active, 
                           (cfcm_monitor_e)i, client, tmp_step_timer);
      }
    }
  }

  /* Update Merged command for a client */
  fc_cmd_ptr->cmd = (cfcm_cmd_e)tmp_cmd;
  fc_cmd_ptr->monitors_active = tmp_monitor_active;

} /* cfcm_monitor_compute_fc_cmd() */


/*==============================================================================

  FUNCTION:  cfcm_monitor_update_cfcm_cmd

==============================================================================*/
/*!
    @brief
    The function calculates final command for the client if it has register for it and 
    any change in the command due to the triggered monitor.

    @detail
    The function Updates Last Command details for each client for CPU monitor.

    @return
    None
*/
/*============================================================================*/
static void cfcm_monitor_update_cfcm_cmd
(
  cfcm_client_e           client_id,
  cfcm_monitor_e          monitor_id,  /*!< monitor id */
  boolean*                qsh_invoked
)
{
  cfcm_monitor_info_s* monitor_ptr;
  cfcm_client_dbg_s*   client_dbg_ptr;
  
 uint32 curr_time, last_fc_time, curr_fc_period, max_fc_period;
  
  /*--------------------------------------------------------------------------*/
  CFCM_ASSERT(CFCM_IS_CLIENT_VALID(client_id));
  CFCM_ASSERT(qsh_invoked != NULL);
  /*--------------------------------------------------------------------------*/

  monitor_ptr = &cfcm_monitor_ptr->monitors[monitor_id];
  client_dbg_ptr  = &cfcm_client.clients[client_id].stats.monitor[monitor_id];

  /* Calculate Client's final command if client is registered for this monitor and 
        command is updated for the client due to this monitor */
  if((monitor_ptr->client[client_id] != CFCM_CLIENT_MAX) &&
    (monitor_ptr->last_cmd[client_id].cmd_updated))
  {
    monitor_ptr->latest_cmd_hist_idx++;
    monitor_ptr->latest_cmd_hist_idx &= CFCM_MONITOR_HIST_BUF_IDX_MASK;
    monitor_ptr->cmd_hist[monitor_ptr->latest_cmd_hist_idx] = monitor_ptr->last_cmd[client_id].cmd;
    monitor_ptr->client_hist[monitor_ptr->latest_cmd_hist_idx] = client_id;
    
    monitor_ptr->stats.num_state_change++;
    if (monitor_ptr->last_cmd[client_id].cmd == CFCM_CMD_DOWN)
    {
      monitor_ptr->stats.num_down ++;
      monitor_ptr->stats.time_down = cfcm_timetick_get_ms();
      if(client_dbg_ptr->fc_started == FALSE)
      {
        client_dbg_ptr->last_fc_start_time = cfcm_timetick_get_ms();
        client_dbg_ptr->fc_started = TRUE;
      }
    }
    else if (monitor_ptr->last_cmd[client_id].cmd == CFCM_CMD_SET_MIN)
    {
      monitor_ptr->stats.num_set_min ++;
      monitor_ptr->stats.time_set_min = cfcm_timetick_get_ms();
      if(client_dbg_ptr->fc_started == FALSE)
      {
        client_dbg_ptr->last_fc_start_time = cfcm_timetick_get_ms();
        client_dbg_ptr->fc_started = TRUE;
      }
    }
    else if (monitor_ptr->last_cmd[client_id].cmd == CFCM_CMD_SHUT_DOWN)
    {
      monitor_ptr->stats.num_shut_down ++;
      monitor_ptr->stats.time_shut_down = cfcm_timetick_get_ms();
      if(client_dbg_ptr->fc_started == FALSE)
      {
        client_dbg_ptr->last_fc_start_time = cfcm_timetick_get_ms();
        client_dbg_ptr->fc_started = TRUE;
      }
    }
    else if (monitor_ptr->last_cmd[client_id].cmd == CFCM_CMD_FREEZE)
    {
      if(client_dbg_ptr->fc_started == FALSE)
      {
        client_dbg_ptr->last_fc_start_time = cfcm_timetick_get_ms();
        client_dbg_ptr->fc_started = TRUE;
      };
    }
    else if ((monitor_ptr->last_cmd[client_id].cmd == CFCM_CMD_UP) ||
             (monitor_ptr->last_cmd[client_id].cmd == CFCM_CMD_FC_OFF))
    {
      /* If the flow control is set to up then calculate how long fc was 
         enabled for and the total fc time per client per monitor */
      curr_time = cfcm_timetick_get_ms();
      last_fc_time = client_dbg_ptr->last_fc_start_time;
      
      /* check if the time got reset to avoid overflow 
         and that UP command is being sent inreponse to previous flow control */
      if((curr_time > last_fc_time) &&
         (client_dbg_ptr->fc_started == TRUE))
      {
        curr_fc_period = curr_time - last_fc_time;
        max_fc_period = client_dbg_ptr->max_fc_period;
        client_dbg_ptr->max_fc_period = (uint32)MAX(curr_fc_period,max_fc_period);
        client_dbg_ptr->total_fc_period += curr_fc_period;
      }
      else
      {
        /* If the time got reset then reset the last fc time
           and do not count this interval towards the stats */
        client_dbg_ptr->last_fc_start_time = 0;
      }
      
      client_dbg_ptr->fc_started = FALSE;
    }
  
    if(cfcm_cfg_monitor_enabled(monitor_id))
    {
      if ((*qsh_invoked == FALSE) && (monitor_ptr->last_cmd[client_id].cmd == CFCM_CMD_SHUT_DOWN) && 
        CFCM_IS_DSM_MONITOR(monitor_id))
      {

        #ifdef FEATURE_QSH_DUMP
        qsh_invoke_s params;
        #else
        qsh_invoke_params_s params;
        #endif
        
        if (CFCM_IS_DSM_LARGE_POOL_MONITOR(monitor_id))
        {
          QSH_LOG(
            QSH_CLT_CFCM,
            QSH_CAT_DSM,
            QSH_MSG_TYPE_HIGH,
            "Invoking QSH analysis: DSM large pool running low, free count <= lte (%d) rlc (%d), client %d",
            CFCM_MONITOR_DSM_LARGE_ITEM_LTE_DL_DNE_MARK, 
            CFCM_MONITOR_DSM_LARGE_ITEM_RLC_DL_DNE_MARK, client_id);
        }
        else if (CFCM_IS_DSM_DUP_POOL_MONITOR(monitor_id))
        {
          QSH_LOG(
            QSH_CLT_CFCM,
            QSH_CAT_DSM,
            QSH_MSG_TYPE_HIGH,
            "Invoking QSH analysis: DSM dup pool running low, free count <= lte (%d) rlc (%d), client %d",
            CFCM_MONITOR_DSM_DUP_ITEM_LTE_DL_DNE_MARK, 
            CFCM_MONITOR_DSM_SMALL_ITEM_RLC_DL_DNE_MARK, client_id);
        }
        else if (CFCM_IS_DSM_SMALL_POOL_MONITOR(monitor_id))
        {
          QSH_LOG(
            QSH_CLT_CFCM,
            QSH_CAT_DSM,
            QSH_MSG_TYPE_HIGH,
            "Invoking QSH analysis: DSM Small pool running low, free count <= rlc (%d), client %d",
            CFCM_MONITOR_DSM_SMALL_ITEM_RLC_DL_DNE_MARK, client_id);
        }

        #ifdef FEATURE_QSH_DUMP
        /* if monitor is DSM and cmd is shutdown, invoke QSH analysis for DSM */
        /* init before sending invoke */
        qsh_invoke_init(&params);

        /* set action values */
        params.action.action = QSH_ACTION_ANALYSIS;
        params.action.done_params_ptr = NULL;
        params.action.params.analysis.category_mask = QSH_CAT_DSM|QSH_CAT_TPUT|QSH_CAT_CFG|QSH_CAT_PERF ;

        /* set config values */
        params.config.action_mode = QSH_ACTION_MODE_SYNC_OPTIONAL;
        params.config.client = QSH_CLT_ALL;
        params.config.invoke_done_cb = NULL;

        /* Invoke QSH API for call backs */
        (void)qsh_invoke(&params);
        #else
        /* if monitor is DSM and cmd is shutdown, invoke QSH analysis for DSM */
        params.client = QSH_CLT_ALL;
        params.action = QSH_ACTION_ANALYSIS;
        params.category_mask = QSH_CAT_DSM | QSH_CAT_TPUT;
        params.async_allowed = TRUE;
        qsh_invoke_cb(&params);
        #endif
        
        *qsh_invoked = TRUE;
      }
  
      cfcm_client_update_fc_cmd(monitor_ptr->client[client_id], monitor_id);
    }
    else
    {
      CFCM_MSG_3(ERROR,
              "CFCM unproc monitor state change monitor=%d, cmd=%d, cmd_updated=%d",
              (uint32)monitor_id,
              (uint32)monitor_ptr->last_cmd[client_id].cmd,
              monitor_ptr->last_cmd[client_id].cmd_updated);
    }
  }
} /* cfcm_monitor_update_cfcm_cmd() */

/*==============================================================================

  FUNCTION:  cfcm_monitor_update_last_cmd_per_client

==============================================================================*/
/*!
    @brief
    The function Updates Last Command details for a client within requested monitor.

    @detail
    The function Updates Last Command details for a client within requested monitor.

    @return
    None
*/
/*============================================================================*/
static void cfcm_monitor_update_last_cmd_per_client
(
  cfcm_client_e           client_id,
  cfcm_monitor_e          monitor_id,  /*!< monitor id */
  cfcm_cmd_e              cmd, /*!<command corresponding to the new monitor state */
  cfcm_monitor_data       monitor_data
)
{
  cfcm_monitor_info_s* monitor_ptr;
  /*--------------------------------------------------------------------------*/
  CFCM_ASSERT(CFCM_IS_CLIENT_VALID(client_id));
  CFCM_ASSERT(CFCM_IS_MONITOR_ID_VALID(monitor_id));
  /*--------------------------------------------------------------------------*/
    
  monitor_ptr = &cfcm_monitor_ptr->monitors[monitor_id];
  monitor_ptr->last_cmd[client_id].cmd_updated = FALSE;

  /* Update the monitor's Last cmd details */
  if((cmd == CFCM_CMD_INVALID) || ((monitor_ptr->last_cmd[client_id].cmd == cmd) && // Command is same and also Data same then return
      (0 == memcmp(&monitor_ptr->last_cmd[client_id].monitor_data, &monitor_data, sizeof(cfcm_monitor_data)))) ||
     CFCM_MONITOR_IS_IGNORE_NEW_CMD(monitor_ptr->last_cmd[client_id].cmd, cmd))
  {
    return;
  }
  
  /* Update the monitor's Last cmd details */
  monitor_ptr->last_cmd[client_id].cmd_updated = TRUE;
  monitor_ptr->last_cmd[client_id].cmd = cmd;
  monitor_ptr->last_cmd[client_id].monitor_data = monitor_data;
} /* cfcm_monitor_update_last_cmd_per_client() */

/*==============================================================================

  FUNCTION:  cfcm_monitor_process_cpu_load_input

==============================================================================*/
/*!
    @brief
    The function Updates Last Command details for each client for CPU monitor.

    @detail
    The function Updates Last Command details for each client for CPU monitor.

    @return
    None
*/
/*============================================================================*/
static void cfcm_monitor_process_cpu_load_input
(
  cfcm_monitor_data     monitor_data
)
{
  cfcm_monitor_info_s* monitor_ptr;
  uint32              i;
  cfcm_cmd_e           cmd;
  boolean             qsh_invoked = FALSE;
  cfcm_cpu_load_type_s           cpu_info;  /*!< cpu_load */
  /*--------------------------------------------------------------------------*/


  /*--------------------------------------------------------------------------*/

  monitor_ptr = &cfcm_monitor_ptr->monitors[CFCM_MONITOR_CPU];
  cpu_info = monitor_data.cpu_info;

  /* Call cpu monitor API to update to store load */
  cfcm_cpu_monitor_process_cpu_info(cpu_info);
  
  /* Update the monitor's Last cmd details */
  for(i = 0; i < (uint32)CFCM_CLIENT_MAX; i++)
  {
    /* Start with Last command for client */
    cmd = monitor_ptr->last_cmd[i].cmd;
    monitor_ptr->last_cmd[i].cmd_updated = FALSE;
    if (cfcm_cpu_monitor_process_load_percentage((cfcm_client_e)i, cpu_info.cpu_load, &cmd))
    {
      cfcm_monitor_update_last_cmd_per_client((cfcm_client_e)i, CFCM_MONITOR_CPU, cmd, monitor_data);
    }
  
    /* Run the loop for the client due to this monitor change */
    cfcm_monitor_update_cfcm_cmd((cfcm_client_e)i, CFCM_MONITOR_CPU, &qsh_invoked);
  }
} /* cfcm_monitor_process_cpu_load_input() */

/*==============================================================================

  FUNCTION:  cfcm_monitor_process_thermal_input

==============================================================================*/
/*!
    @brief
    handles NPA state change for thermal monitors, which gets input using NPA Node.

    @return
    None
*/
/*============================================================================*/
static void cfcm_monitor_process_thermal_input
(
  cfcm_monitor_data     monitor_data,
  cfcm_monitor_e        monitor_id  /*!< monitor id */
)
{
  uint8           index = 0;
  cfcm_cmd_e      cmd;
  boolean         qsh_invoked = FALSE;
  uint32          state;   /*!< new NPA state  */

  state = monitor_data.npa_node.state;

  /*--------------------------------------------------------------------------*/
  CFCM_ASSERT(CFCM_IS_MONITOR_ID_VALID(monitor_id));

  if (state >= CFCM_MONITOR_MAX_NPA_NODE_STATE)
  {
    CFCM_MSG_1(ERROR, "CFCM thermal PA monitor state %d INVALID ", state);
    return;
  }
  /*--------------------------------------------------------------------------*/

  cmd = cfcm_npa_node_state_map[state];

  for(index = 0; index < CFCM_CLIENT_MAX; index++)
  {
    cfcm_monitor_update_last_cmd_per_client((cfcm_client_e)index, monitor_id, cmd, monitor_data);
  
    /* Run the loop for the client due to this monitor change */
    cfcm_monitor_update_cfcm_cmd((cfcm_client_e)index, monitor_id, &qsh_invoked);
  }
} /* cfcm_monitor_process_thermal_pa() */

/*==============================================================================

  FUNCTION:  cfcm_monitor_process_dsm_input

==============================================================================*/
/*!
    @brief
    handles DSM state change for DSM monitors.

    @return
    None
*/
/*============================================================================*/
static void cfcm_monitor_process_dsm_input
(
  cfcm_monitor_data     monitor_data,
  cfcm_monitor_e        monitor_id  /*!< monitor id */
)
{
  uint8           index = 0;
  boolean         qsh_invoked = FALSE;
  cfcm_monitor_info_s* monitor_ptr;

  /*--------------------------------------------------------------------------*/
  CFCM_ASSERT(CFCM_IS_MONITOR_ID_VALID(monitor_id));

  /*--------------------------------------------------------------------------*/

  monitor_ptr = &cfcm_monitor_ptr->monitors[monitor_id];

  for(index = 0; index < CFCM_CLIENT_MAX; index++)
  {
    monitor_ptr->last_cmd[index].cmd_updated = FALSE;
    if (CFCM_BIT_PRESENT_IN_MASK(monitor_data.dsm.client_mask, index))
    {
      cfcm_monitor_update_last_cmd_per_client((cfcm_client_e)index, monitor_id, monitor_data.dsm.cmd, monitor_data);
    
      /* Run the loop for the client due to this monitor change */
      cfcm_monitor_update_cfcm_cmd((cfcm_client_e)index, monitor_id, &qsh_invoked);
    }

  }
} /* cfcm_monitor_process_thermal_pa() */

/*==============================================================================

  FUNCTION:  cfcm_monitor_proc_update

==============================================================================*/
/*!
    @brief
    The function processes the monitor state change indication.

    @detail
    It iterates over all clients that are designed to respond to the monitor and
    triggers flow control command recalculation for them.

    @return
    None
*/
/*============================================================================*/
EXTERN void cfcm_monitor_proc_update
(
  cfcm_monitor_ind_msg_s*  msg_ptr/*!< monitor state change indication message */
)
{
  uint32        hist_idx[CFCM_CLIENT_MAX];
  cfcm_log_monitor_type log_monitor;
  cfcm_monitor_e           monitor_id;

  /*--------------------------------------------------------------------------*/

  CFCM_ASSERT(msg_ptr != NULL);
  CFCM_ASSERT(CFCM_IS_MONITOR_ID_VALID(msg_ptr->monitor_id));

  /*--------------------------------------------------------------------------*/

  cfcm_client_get_current_history_idxs(hist_idx);
  
  switch (msg_ptr->monitor_id)
  {
    case CFCM_MONITOR_CPU:
    {
      /* For CPU command is not updated 
                so calculate command and later update monitor last cmd data */
      cfcm_monitor_process_cpu_load_input(msg_ptr->monitor_data);
      monitor_id = CFCM_MONITOR_CPU;
      log_monitor.cpu.load = msg_ptr->monitor_data.cpu_info.cpu_load;
      log_monitor.cpu.curr_cpu_freq = msg_ptr->monitor_data.cpu_info.curr_cpu_freq;
      log_monitor.cpu.max_cpu_freq = msg_ptr->monitor_data.cpu_info.max_cpu_freq;
    }
    break;

    case CFCM_MONITOR_THERMAL_PA:
    case CFCM_MONITOR_THERMAL_CX:
    case CFCM_MONITOR_MDM_TEMP:
    case CFCM_MONITOR_VDD_PEAK_CURR_EST:
    {
      /* For Thermal PA/Runaway/MDM Temp/VDD Peak Curr, command is not updated 
                so calculate command and later update monitor last cmd data */
      cfcm_monitor_process_thermal_input(msg_ptr->monitor_data, msg_ptr->monitor_id);
      monitor_id = msg_ptr->monitor_id;
      log_monitor.npa_node.state = msg_ptr->monitor_data.npa_node.state;
    }
    break;

    case CFCM_MONITOR_DSM_LARGE_POOL:
    case CFCM_MONITOR_DSM_DUP_POOL:
    case CFCM_MONITOR_DSM_SMALL_POOL:
    case CFCM_MONITOR_DSM_LARGE_POOL_LEVEL1:
    {
      /* For DSM Large Pool, command is not updated 
                so calculate command and later update monitor last cmd data */
      cfcm_monitor_process_dsm_input(msg_ptr->monitor_data, msg_ptr->monitor_id);
      monitor_id = msg_ptr->monitor_id;

      log_monitor.dsm.pool_id = msg_ptr->monitor_data.dsm.pool_id;
      log_monitor.dsm.event = msg_ptr->monitor_data.dsm.event;
      log_monitor.dsm.op = msg_ptr->monitor_data.dsm.op;
    }
    break;

    case CFCM_MONITOR_BW_THROTTLING:
    {
      /* For Bus Bandwidth command is not updated 
                so calculate command and later update monitor last cmd data */
      cfcm_monitor_process_thermal_input(msg_ptr->monitor_data, CFCM_MONITOR_BW_THROTTLING);
      monitor_id = CFCM_MONITOR_BW_THROTTLING;
      log_monitor.bw.level = msg_ptr->monitor_data.bw.level;
    }
    break;

    default:
    {
      CFCM_MSG_1(ERROR, "cfcm_monitor_proc_update Invalid data info %d ",
                  msg_ptr->monitor_id);
      return;
    }
  }

  cfcm_log_monitor_input(monitor_id, &log_monitor);
  cfcm_log_command_output(hist_idx, monitor_id, &log_monitor);
} /* cfcm_monitor_proc_update() */


/*==============================================================================

  FUNCTION:  cfcm_monitor_registered

==============================================================================*/
/*!
    @brief
    returns whether or not the specified monitor is registered with the source.

    @return
    TRUE if the monitor is registered. FALSE, otherwise.
*/
/*============================================================================*/
EXTERN boolean cfcm_monitor_registered
(
  cfcm_monitor_e monitor,  /*!< REQUIRED short parameter description */
  cfcm_client_e client   /*!< the client id */
)
{
  /*--------------------------------------------------------------------------*/

  /*--------------------------------------------------------------------------*/
  return cfcm_monitor_ptr->monitors[(uint32)monitor].registered[(uint32)client];
} /* cfcm_monitor_registered() */

/*==============================================================================

  FUNCTION:  cfcm_monitor_get_cmd_string

==============================================================================*/
/*!
    @brief
    Returns the string associated with a flow control command.
*/
/*============================================================================*/
EXTERN const char * cfcm_monitor_get_cmd_string
(
  cfcm_cmd_e cmd 
)
{
  const char * cmd_str = cfcm_cmd_str[CFCM_CMD_MAX-1];
  uint32 cmd_id = (uint32)cmd;
  uint32 idx = 0;

  while(cmd_id & 0x1)
  {
    idx += 1;
    cmd_id = (cmd_id >> 1);
  }

  if(idx < CFCM_CMD_MAX)
  {
    cmd_str = cfcm_cmd_str[idx];
  }

  return cmd_str;
  
}

/*==============================================================================

  FUNCTION:  cfcm_monitor_qsh_analysis

==============================================================================*/
/*!
    @brief
    Performs QSH analysis on this module.
*/
/*============================================================================*/
EXTERN void cfcm_monitor_qsh_analysis
(
  void
)
{
  unsigned int i;
  uint32 idx;
  const char * cmd_str;

  /* iterate through monitors */
  for (i = 0; i < CFCM_MONITOR_MAX; i ++)
  {
    idx = cfcm_monitor_ptr->monitors[i].latest_cmd_hist_idx;
    cmd_str = cfcm_monitor_get_cmd_string(cfcm_monitor_ptr->monitors[i].cmd_hist[idx]);
    /* If shutdown was triggered then use HIGH message.
       Otherwise use medium message.
       High messages are used by QSherlock to detect problem areas 
    */
    if(cfcm_monitor_ptr->monitors[i].stats.num_shut_down > 0)
    {
      QSH_CHECK_LOG(
        cfcm_monitor_ptr->monitors[i].stats.num_down != 0 ||
        cfcm_monitor_ptr->monitors[i].stats.num_set_min != 0 ||
        cfcm_monitor_ptr->monitors[i].stats.num_shut_down != 0,
        QSH_CLT_CFCM,
        QSH_CAT_TPUT,
        QSH_MSG_TYPE_HIGH,
        "%s: Down=%d; SetMin=%d; Shutdown=%d; CurrState: %s",
        cfcm_monitor_str[i],
        cfcm_monitor_ptr->monitors[i].stats.num_down,
        cfcm_monitor_ptr->monitors[i].stats.num_set_min,
        cfcm_monitor_ptr->monitors[i].stats.num_shut_down,
        cmd_str);
    }
    else
    {
      QSH_CHECK_LOG(
        cfcm_monitor_ptr->monitors[i].stats.num_down != 0 ||
        cfcm_monitor_ptr->monitors[i].stats.num_set_min != 0 ||
        cfcm_monitor_ptr->monitors[i].stats.num_shut_down != 0,
        QSH_CLT_CFCM,
        QSH_CAT_TPUT,
        QSH_MSG_TYPE_MEDIUM,
        "%s: Down=%d; SetMin=%d; Shutdown=%d; CurrState: %s",
        cfcm_monitor_str[i],
        cfcm_monitor_ptr->monitors[i].stats.num_down,
        cfcm_monitor_ptr->monitors[i].stats.num_set_min,
        cfcm_monitor_ptr->monitors[i].stats.num_shut_down,
        cmd_str);
    }
  }
}

/*==============================================================================

  FUNCTION:  cfcm_monitor_qsh_reset

==============================================================================*/
/*!
    @brief
    Clears the statistics.
*/
/*============================================================================*/
EXTERN void cfcm_monitor_qsh_reset
(
  void
)
{
  unsigned int i;
  
  for (i = 0; i < CFCM_MONITOR_MAX; i ++)
  {
    memset(
      &cfcm_monitor_ptr->monitors[i].stats, 
      0, 
      sizeof(cfcm_monitor_stats_s));
  }
}

/*==============================================================================

                                UNIT TEST

==============================================================================*/
#ifdef FEATURE_MODEM_CFCM_DIAG_TEST
#error code not present
#endif /* FEATURE_MODEM_CFCM_DIAG_TEST */


static void cfcm_test_dsm_rlc_monitor_via_diag
(
  uint8 input 
)
{
#ifdef FEATURE_MODEM_CFCM_DIAG_TEST
  #error code not present
#else
  CFCM_UNUSED(input);
#endif /* FEATURE_MODEM_CFCM_DIAG_TEST */

  return;
} /* cfcm_test_dsm_rlc_monitor_via_diag() */

static void cfcm_test_dsm_lte_monitor_via_diag
(
  uint8 input 
)
{
#ifdef FEATURE_MODEM_CFCM_DIAG_TEST
  #error code not present
#else
  CFCM_UNUSED(input);
#endif /* FEATURE_MODEM_CFCM_DIAG_TEST */
  return;
} /* cfcm_test_dsm_lte_monitor_via_diag() */


void cfcm_test_thermal_monitor_via_diag
(
  uint8 input 
)
{
#ifdef FEATURE_MODEM_CFCM_DIAG_TEST
  #error code not present
#else
  CFCM_UNUSED(input);
#endif /* FEATURE_MODEM_CFCM_DIAG_TEST */

  return;
} /* cfcm_test_thermal_monitor_via_diag() */


void cfcm_test_dsm_monitor_via_diag
(
  uint8 input 
)
{
#ifdef FEATURE_MODEM_CFCM_DIAG_TEST
  #error code not present
#else
  cfcm_test_dsm_rlc_monitor_via_diag(input);
  cfcm_test_dsm_lte_monitor_via_diag(input);
#endif /* FEATURE_MODEM_CFCM_DIAG_TEST */
  return;
} /* cfcm_test_dsm_monitor_via_diag() */

