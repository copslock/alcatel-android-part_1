#ifndef DS707_RMSM_PROXY_H
#define DS707_RMSM_PROXY_H

/*===========================================================================

                          D S 7 0 7 _ R M S M _ P R O X Y. H

DESCRIPTION

  The Data Services RmSm Proxy'S main operations are to handle 1X Dial String
  Callbacks, set up the port bridge between the MDM and MSM, handle modem 
  control signals such as Data Terminal Ready(DTR), Carrier Detect(CD)
  signals and handle abort commands.

Copyright (c) 2001 - 2010 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/


/*===========================================================================

                            EDIT HISTORY FOR FILE

  $PVCSPath: L:/src/asw/MM_DATA/vcs/ds707_rmsm.h_v   1.6   23 Jan 2003 16:34:28   randrew  $
  $Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/datamodem/3gpp2/ds707/inc/ds707_rmsm_proxy.h#1 $ 
  $DateTime: 2016/03/28 23:02:50 $ $Author: mplcsds1 $

when        who    what, where, why
--------    ---    ----------------------------------------------------------
05/21/10    vs     Initial File
01/07/11    vs     Changes to make dstask free-floating.

===========================================================================*/


/*===========================================================================

                          INCLUDE FILES FOR MODULE

===========================================================================*/

#include "datamodem_variation.h"
#include "comdef.h"
#include "customer.h"

							 
#endif /* DS707_RMSM_H */
