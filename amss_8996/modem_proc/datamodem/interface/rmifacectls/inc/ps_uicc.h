#ifndef _PS_UICC_H
#define _PS_UICC_H
/*===========================================================================

                              P S _ U I C C . H
                   
DESCRIPTION
  The header file for UICC external/common declarations.

EXTERNALIZED FUNCTIONS
  
  ps_uicc_powerup_init()
    This function performs power-up initialization for the UICC sub-system

  ps_uicc_sio_cleanup()
    This function is used to close the SIO stream

Copyright (c) 2008-2009 Qualcomm Technologies Incorporated. 
All Rights Reserved.
Qualcomm Confidential and Proprietary
===========================================================================*/
/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/datamodem/interface/rmifacectls/inc/ps_uicc.h#1 $
  $Author: mplcsds1 $ $DateTime: 2016/03/28 23:02:50 $
===========================================================================*/

#include "datamodem_variation.h"
#include "comdef.h"
#include "customer.h"
#endif /* _PS_UICC_H */
