/*===========================================================================
  FILE: PS_Sock_RemoteSocket_IManager.cpp

  OVERVIEW: This file is implementation for the interface RemoteSocketManager

  DEPENDENCIES: None

  Copyright (c) 2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary
===========================================================================*/

/*===========================================================================
  EDIT HISTORY FOR MODULE

  Please notice that the changes are listed in reverse chronological order.

  $Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/datamodem/interface/pssock/src/PS_Sock_RemoteSocket_IManager.cpp#1 $
  $DateTime: 2016/03/28 23:02:50 $ $Author: mplcsds1 $

  when       who what, where, why
  ---------- --- ------------------------------------------------------------

===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "comdef.h"
#include "customer.h"

#include "PS_Sock_RemoteSocket_Manager.h"

using namespace PS::Sock::RemoteSocket;


/*===========================================================================

                         PUBLIC MEMBER FUNCTIONS

===========================================================================*/
IManager * IManager::GetInstance
(
  void
)
{
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  return Manager::GetInstance();

} /* IManager::GetInstance() */

IManager::~IManager()
{

}/* IManager::~IManager() */