/******************************************************************************
  @file    ds_profile_qmi.c
  @brief   DS PROFILE - QMI related code, which is not technology specific

  DESCRIPTION
  DS PROFILE - QMI related code, which is not technology specific

  INITIALIZATION AND SEQUENCING REQUIREMENTS
  None

  $Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/datamodem/interface/dsprofile/src/ds_profile_qmi.c#1 $ $DateTime: 2016/03/28 23:02:50 $ $Author: mplcsds1 $

  ---------------------------------------------------------------------------
  Copyright (c) 2012 Qualcomm Technologies Incorporated. 
  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.
  ---------------------------------------------------------------------------
******************************************************************************/
