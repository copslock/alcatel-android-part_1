/*==============================================================================

                            ds_http_cookie_info.cpp

GENERAL DESCRIPTION
  Cookie manager for DS HTTP

  Copyright (c) 2015 by Qualcomm Technologies Incorporated. All Rights Reserved.
==============================================================================*/

/*==============================================================================
                           EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

when        who    what, where, why
--------    ---    ----------------------------------------------------------
09/30/15    ml     Fix parse issue on HTTP date conversion.
06/22/15    ml     Created file/Initial version.
==============================================================================*/
#include "ds_http_cookie_info.h"

#include "ds_ASStringStream.h"
#include "ds_appsrv_mem.h"
#include "data_msg.h"
extern "C"
{
  #include "time_svc.h"
}
#include <cstdlib>


static const char month_list[12][4] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun",
                                       "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
static const char dow_list[7][4] = {"Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"};


ds_http_cookie_pair::ds_http_cookie_pair()
{ }

ds_http_cookie_pair::ds_http_cookie_pair(const ASString& attribute_name, const ASString& attribute_value)
: name(attribute_name), value(attribute_value)
{ }



ds_http_cookie_info::ds_http_cookie_info(const ASString& cookie_header_value, bool& error_flag)
: expiration_set(false), http_only(false), security(false)
{
  ASStringStream iss(cookie_header_value.c_str(), true);
  ASString       getter;
  error_flag = false;

  // Name/value
  iss.get_next(getter, '=');
  name.name = getter;
  iss.get_next(getter, ';');
  name.value = getter;

  // Attributes
  while(iss.good())
  {
    iss.get_next(getter, ';');
    DATA_APPSRV_MSG_SPRINTF_1(MSG_LEGACY_ERROR, "ds_http_cookie_info - attr %s", getter.c_str());

    if(getter.empty())
    {
      DATA_APPSRV_MSG0(MSG_LEGACY_LOW, "ds_http_cookie_info - Finished parsing cookie attributes");
      break;
    }

    ASStringStream hss(getter, true);
    ASString attribute_name;
    hss.get_next(attribute_name, "=");
    if(hss.eof())
    {
      if("httponly" == attribute_name)
      {
        http_only = true;
      }
      else if("secure" == attribute_name)
      {
        security = true;
      }
      else
      {
        DATA_APPSRV_MSG0(MSG_LEGACY_ERROR, "ds_http_cookie_info - Unknown cookie flag");
      }
      continue;
    }

    // else
    ASString attribute_value;
    hss.get_next(attribute_value, '\0');
    DATA_APPSRV_MSG_SPRINTF_1(MSG_LEGACY_HIGH, "ds_http_cookie_info - attr %s", attribute_value.c_str());

    if("Domain" == attribute_name)
    {
      domain = attribute_value;
    }
    else if("Path" == attribute_name)
    {
      path = attribute_value;
    }
    else if("Expires" == attribute_name)
    {
      // Fixed format : Wdy, DD Mon YYYY HH:MM:SS GMT
      error_flag    = true;
      uint32 offset = 0;

      if(29 != attribute_value.length())
      {
        DATA_APPSRV_MSG_SPRINTF_2(
                                  MSG_LEGACY_ERROR,
                                  "ds_http_cookie_info - attr len %d value %s",
                                  attribute_value.length(),
                                  attribute_value.c_str()
                                  );
        break;
      }

      // 'Wdy, '
      expiration.day_of_week = parse_day_of_week(attribute_value, offset);
      if(!parse_expires_delim_validator(attribute_value, offset, ", ")) break;
      // 'DD '
      expiration.day = parse_expires_value(attribute_value, offset, 2);
      if(!parse_expires_date_delim_validator(attribute_value, offset)) break;
      // 'Mon '
      expiration.month = parse_month(attribute_value, offset);
      if(!parse_expires_date_delim_validator(attribute_value, offset)) break;
      // 'YYYY '
      expiration.year = parse_expires_value(attribute_value, offset, 4);
      if(!parse_expires_delim_validator(attribute_value, offset, " ")) break;
      // 'HH:'
      expiration.hour = parse_expires_value(attribute_value, offset, 2);
      if(!parse_expires_delim_validator(attribute_value, offset, ":")) break;
      // 'MM:'
      expiration.minute = parse_expires_value(attribute_value, offset, 2);
      if(!parse_expires_delim_validator(attribute_value, offset, ":")) break;
      // 'ss '
      expiration.second = parse_expires_value(attribute_value, offset, 2);
      if(!parse_expires_delim_validator(attribute_value, offset, " ")) break;

      if(!parse_expires_delim_validator(attribute_value, offset, "GMT")) break;

      // Validate Julian time values
      if(
         6    <  expiration.day_of_week ||
         0    == expiration.day         ||
         31   <  expiration.day         ||
         0    == expiration.month       ||
         12   <  expiration.month       ||
         1980 >  expiration.year        ||
         2100 <  expiration.year        ||
         24   <= expiration.hour        ||
         60   <= expiration.minute      ||
         60   <= expiration.second
         )
      {
        DATA_APPSRV_MSG0(MSG_LEGACY_ERROR, "ds_http_cookie_info - Invalid Julian time value");
        break;
      }
      expiration_set = true;
      error_flag     = false;
    }
    else if("Max-Age" == attribute_name)
    {
      // Get current time. Add expire seconds.
      uint32 expire_sec = strtoul(attribute_value.c_str(), NULL, 10);
      uint32 current_sec = time_get_secs();
      time_jul_from_secs(current_sec + expire_sec, &expiration);
      expiration_set = true;
    }
    else
    {
      DATA_APPSRV_MSG_SPRINTF_1(MSG_LEGACY_ERROR, "ds_http_cookie_info - %s is not a cookie attribute", attribute_name.c_str());
      error_flag = true;
      break;
    }
  }
}


uint16 ds_http_cookie_info::parse_expires_value(const ASString& expires, uint32& offset, uint32 len) const
{
  ASString value_str(expires.c_str()+offset, len);
  offset += len;
  return strtoul(value_str.c_str(), 0, 10);
}


bool ds_http_cookie_info::parse_expires_delim_validator(const ASString& expires, uint32& offset, const char* expected) const
{
  uint32   len = strlen(expected);
  ASString delim_str(expires.c_str()+offset, len);
  offset += len;
  return (delim_str == expected);
}


bool ds_http_cookie_info::parse_expires_date_delim_validator(const ASString& expires, uint32& offset) const
{
  char delim_chr = *(expires.c_str()+offset);
  offset += 1;
  return ('-' == delim_chr || ' ' == delim_chr);
}


void ds_http_cookie_info::get_cookie_info(ds_http_cookie_info_s_type& cookie_info) const
{
  // Copy cookie name and value
  memscpy(
          cookie_info.name_value.name,
          DS_HTTP_COOKIE_NAME_MAX_LEN,
          name.name.c_str(),
          name.name.length()
          );
  memscpy(
          cookie_info.name_value.value,
          DS_HTTP_COOKIE_VALUE_MAX_LEN,
          name.value.c_str(),
          name.value.length()
          );

  // populate attribute fields
  cookie_info.domain         = domain.c_str();
  cookie_info.path           = path.c_str();
  cookie_info.expiration     = expiration; // Value should be ignored if expiration_set is false.
  cookie_info.expiration_set = (expiration_set) ? TRUE : FALSE;
  cookie_info.http_only      = (http_only) ? TRUE : FALSE;
  cookie_info.security       = (security) ? TRUE : FALSE;
}


uint16 ds_http_cookie_info::parse_month(const ASString& expires, uint32& offset)
{
  ASString mon_str(expires.c_str()+offset, 3);
  offset += 3;

  for (uint32 i = 0; i < 12; ++i)
  {
    if(month_list[i] == mon_str)
    {
      return i+1;
    }
  }

  return 13;
}


uint16 ds_http_cookie_info::parse_day_of_week(const ASString& expires, uint32& offset)
{
  ASString dow_str(expires.c_str()+offset, 3);
  offset += 3;

  for (uint32 i = 0; i < 7; ++i)
  {
    if(dow_list[i] == dow_str)
    {
      return i;
    }
  }

  return 7;
}

