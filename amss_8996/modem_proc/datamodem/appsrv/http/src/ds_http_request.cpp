/*==============================================================================

                              ds_http_request.cpp

GENERAL DESCRIPTION
  HTTP request information object

  Copyright (c) 2014 by Qualcomm Technologies Incorporated. All Rights Reserved.
==============================================================================*/

/*==============================================================================
                           EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

when        who    what, where, why
--------    ---    ----------------------------------------------------------
09/29/15    ml     IPv6 URI support
06/11/15    ml     Cookie support
04/16/15    ml     Response authentication support.
01/06/15    ml     HTTP Digest auth-int support.
12/02/14    ml     Added features for more detailed authentication requests.
07/21/14    ml     Created file/Initial version.
==============================================================================*/
#include "ds_http_request.h"

#include "ds_http_data_mgr.h" // ds_http_notify_session_cb
#include "ds_http_utility.h"

#include "ds_ASStringStream.h"

#include <cctype>
#include <cstdlib>
#include <stringl/stringl.h>

#include "data_msg.h"
extern "C"
{
  #include "md5.h"
}

/*==============================================================================
                              Static variables
==============================================================================*/

static const uint32 HTTP_DEFAULT_PORT            = 80;
static const uint32 HTTPS_DEFAULT_PORT           = 443;
static const uint8  DS_HTTP_HEADER_DELIMITER[]   = "\r\n\r\n";
static const uint32 DS_HTTP_HEADER_DELIMITER_LEN = 4;
static const char   URI_PORT_DELIM               = ':';
static const char   URI_PATH_DELIM               = '/';
static const uint32 MAX_REDIRECT_COUNT           = 5;



/*==============================================================================
                              ds_http_request
==============================================================================*/
ds_http_request::ds_http_request(
                                 const uint32                 sid,
                                 const uint32                 rid,
                                 const char*                  uri_str,
                                 const ds_http_header_s_type* header,
                                 uint16                       rsv_src_port,
                                 sint15*                      request_errno
                                 )
: session_id(sid),
  request_id(rid),
  reserved_src_port(rsv_src_port),
  send_status(REQUEST_SEND_REQUEST),
  redirect_counter(0),
  bytes_written(0)
{
  if(!decode_uri(uri_str))
  {
    *request_errno = DS_HTTP_ERROR_INVALID_URI;
    return;
  }
  memset(&credential, 0, sizeof(ds_http_credential_s_type));

  create_header_str(header);
}



ds_http_request::~ds_http_request()
{ }



/*==============================================================================
                      ds_http_request - Utility
==============================================================================*/
const char* ds_http_request::get_hostname() const
{
  return uri_info.hostname;
}



sint15 ds_http_request::get_port() const
{
  return uri_info.port;
}



bool ds_http_request::is_ssl_protocol() const
{
  return (DS_HTTP_PROTOCOL_HTTPS == uri_info.protocol);
}


void ds_http_request::get_auth_info(ds_http_block_auth_info_type* auth_data) const
{
  auth_data->realm = auth_generator.realm.c_str();
  auth_data->nonce = auth_generator.nonce.c_str();
}


const ds_http_credential_s_type* ds_http_request::get_credential() const
{
  return &credential;
}


const ds_http_auth_generator* ds_http_request::get_request_auth_info() const
{
  return &auth_generator;
}


const char* ds_http_request::get_uri_path() const
{
  return uri_info.path;
}


bool ds_http_request::decode_uri(const char* uri_str)
{
  const char* uri_ref = NULL;
  const char* tmp     = NULL;
  uint32 counter      = 0;
  uint32 path_len     = 0;

  if(NULL == uri_str || 0 == strlen(uri_str))
    return false;

  memset(&uri_info, 0, sizeof(ds_http_request_uri));

  if(strncmp(uri_str, "http://", 7) == 0)
  {
    uri_info.protocol = DS_HTTP_PROTOCOL_HTTP;
    uri_info.port     = HTTP_DEFAULT_PORT;
    uri_str += 7;
  }
  else if(strncmp(uri_str, "https://", 8) == 0)
  {
    uri_info.protocol = DS_HTTP_PROTOCOL_HTTPS;
    uri_info.port     = HTTPS_DEFAULT_PORT;
    uri_str += 8;
  }
  else
  {
    DATA_APPSRV_MSG0(MSG_LEGACY_ERROR, "ds_http_decode_uri - Invalid protocol type");
    return false;
  }

  uri_ref = uri_str;

  if('[' == *uri_ref)
  {
    do
    {
      if('\0' == *uri_ref)
      {
        DATA_APPSRV_MSG0(MSG_LEGACY_ERROR, "ds_http_decode_uri - IPv6 address end delimiter not found");
        return false;
      }

      uri_ref++;
      counter++;
    } while(']' != *uri_ref);

    // Add ']' to hostname
    uri_ref++;
    counter++;
  }
  else
  {
    while('\0' != *uri_ref && URI_PATH_DELIM != *uri_ref && URI_PORT_DELIM != *uri_ref)
    {
      uri_ref++;
      counter++;
    }
  }


  // Get domain name (hostname)
  if(0 == counter || DS_HTTP_HOSTNAME_MAX_LEN < counter) // Empty hostname
  {
    DATA_APPSRV_MSG1(MSG_LEGACY_ERROR, "ds_http_decode_uri - Invalid hostname len %d", counter);
    return false;
  }
  memscpy(uri_info.hostname, DS_HTTP_HOSTNAME_MAX_LEN, uri_str, counter);


  // Get port if specified
  if(URI_PORT_DELIM == *uri_ref)
  {
    // get port
    uri_ref++;
    tmp           = uri_ref;
    uri_info.port = 0;

    while('\0' != *uri_ref)
    {
      if(URI_PATH_DELIM == *uri_ref)
        break;

      if(0 == isdigit(*uri_ref))
      {
        DATA_APPSRV_MSG0(MSG_LEGACY_ERROR, "ds_http_decode_uri - Nondigit port number");
        return false;
      }

      uri_info.port = (uri_info.port*10) + (*uri_ref - '0');
      uri_ref++;
    }

    if(tmp == uri_ref)
    {
      DATA_APPSRV_MSG0(MSG_LEGACY_ERROR, "ds_http_decode_uri - Port delimiter with no port number");
      return false;
    }
  }


  // remaining chars is the request path
  path_len = strlen(uri_ref);
  if(0 == path_len)
  {
    uri_info.path[0] = '/';
  }
  else
  {
    memscpy(uri_info.path, DS_HTTP_PATH_MAX_LEN, uri_ref, path_len);
  }

  return true;
}



void ds_http_request::create_request_str(const char* request_method)
{
  request_str.clear();
  request_str.append(request_method);
  request_str.append(uri_info.path);
  request_str.append(" HTTP/1.1\r\nHost: "); // host not in header_str since it could change by redirect
  request_str.append(uri_info.hostname);
}



void ds_http_request::create_header_str(const ds_http_header_s_type* header_info)
{
  append_header("Connection", "Keep-Alive");
  if(NULL == header_info)
    return;

  append_header("Accept", header_info->accept);
  append_header("Accept-Charset", header_info->accept_charset);
  append_header("Accept-Encoding", header_info->accept_encoding);
  append_header("Accept-Language", header_info->accept_language);
  append_header("User-Agent", header_info->user_agent);
  append_header("UAProf", header_info->ua_profile);
  append_header("Referer", header_info->referer);
  append_header("Content-Type", header_info->content_type);

  for(uint32 i = 0; i < (uint32)header_info->num_cust_headers; ++i)
  {
    append_header(header_info->cust_header_list[i].name, header_info->cust_header_list[i].value);
  }

  uint32 auth_len = strlen(header_info->authorization);
  if(0 != auth_len)
  {
    auth_header_str.append("\r\nAuthorization: ");
    auth_header_str.append(header_info->authorization);
  }

  create_cookie_header_str(header_info->cookie, header_info->num_cookies);
}


void ds_http_request::create_cookie_header_str(const ds_http_cookie_name_s_type cookie[], uint16 num_cookies)
{
  if(0 == num_cookies)
    return;
  else if(num_cookies >= DS_HTTP_COOKIE_MAX)
    num_cookies = DS_HTTP_COOKIE_MAX;

  cookie_header_str.clear();
  cookie_header_str.append("\r\nCookie: ");

  cookie_header_str.append(cookie[0].name);
  cookie_header_str.append("=");
  cookie_header_str.append(cookie[0].value);

  for(uint16 i = 1; i < num_cookies; ++i)
  {
    cookie_header_str.append("; ");
    cookie_header_str.append(cookie[i].name);
    cookie_header_str.append("=");
    cookie_header_str.append(cookie[i].value);
  }
}


void ds_http_request::append_header(const char* header_name, const char* header_value)
{
  if(NULL == header_name || NULL == header_value)
    return;

  uint32 nsize = strlen(header_name);
  uint32 vsize = strlen(header_value);

  if(
     0 < nsize &&
     0 < vsize &&
     !has_carriage_return(header_name, nsize) &&
     !has_carriage_return(header_value, vsize)
     )
  {
    header_str.append("\r\n");
    header_str.append(header_name, nsize);
    header_str.append(": ");
    header_str.append(header_value, vsize);
  }
}



void ds_http_request::append_content(const uint8* content_data, const uint32 content_size)
{
  char buffer[11] = {0};
  append_header("Content-Length", uitoa(content_size, buffer));
  content.append(content_data, content_size);
}



void ds_http_request::notify_error(const sint15 http_error) const
{
  ds_http_notify_session_cb(session_id, request_id, http_error, NULL);
}


bool ds_http_request::check_buffer_error() const
{
  return (header_str.error()      ||
          content.error()         ||
          request_str.error()     ||
          auth_header_str.error() ||
          cookie_header_str.error());
}



/*==============================================================================
                ds_http_request - buffer & status management
==============================================================================*/
const uint8* ds_http_request::get_send_buffer_content() const
{
  switch(send_status)
  {
    case REQUEST_SEND_REQUEST:
      return request_str.content() + bytes_written;

    case REQUEST_SEND_HEADER:
      return header_str.content() + bytes_written;

    case REQUEST_SEND_COOKIE_HEADER:
      return cookie_header_str.content() + bytes_written;

    case REQUEST_SEND_AUTH_HEADER:
      return auth_header_str.content() + bytes_written;

    case REQUEST_SEND_HEADER_DELIM:
      return DS_HTTP_HEADER_DELIMITER + bytes_written;

    case REQUEST_SEND_CONTENT:
      return content.content() + bytes_written;

    case REQUEST_SEND_COMPLETE: // fallthrough
    default:
      return NULL;
  }
}



uint32 ds_http_request::get_send_buffer_size() const
{
  switch(send_status)
  {
    case REQUEST_SEND_REQUEST:
      return request_str.size() - bytes_written;

    case REQUEST_SEND_HEADER:
      return header_str.size() - bytes_written;

    case REQUEST_SEND_COOKIE_HEADER:
      return cookie_header_str.size() - bytes_written;

    case REQUEST_SEND_AUTH_HEADER:
      return auth_header_str.size() - bytes_written;

    case REQUEST_SEND_HEADER_DELIM:
      return DS_HTTP_HEADER_DELIMITER_LEN - bytes_written;

    case REQUEST_SEND_CONTENT:
      return content.size() - bytes_written;

    case REQUEST_SEND_COMPLETE: // fallthrough
    default:
      return 0;
  }
}


// Returns true if no error
void ds_http_request::reset_send_status()
{
  send_status   = REQUEST_SEND_REQUEST;
  bytes_written = 0;
}



void ds_http_request::update_send_status(const uint32 written)
{
  bytes_written += written;
  DATA_APPSRV_MSG2(MSG_LEGACY_MED, "update_status - status:%d written:%d", send_status, bytes_written);

  switch(send_status)
  {
    case REQUEST_SEND_REQUEST:
    {
      if(request_str.size() <= bytes_written)
      {
        DATA_APPSRV_MSG0(MSG_LEGACY_MED, "update_send_status - request_str send complete");
        send_status   = REQUEST_SEND_HEADER;
        bytes_written = 0;
      }
      else
        break;
    } // fallthrough

    case REQUEST_SEND_HEADER:
    {
      if(header_str.size() <= bytes_written)
      {
        DATA_APPSRV_MSG0(MSG_LEGACY_MED, "update_send_status - header_str send complete");
        send_status   = REQUEST_SEND_COOKIE_HEADER;
        bytes_written = 0;
      }
      else
        break;
    } // fallthrough

    case REQUEST_SEND_COOKIE_HEADER:
    {
      if(cookie_header_str.size() <= bytes_written)
      {
        DATA_APPSRV_MSG0(MSG_LEGACY_MED, "update_send_status - cookie_header_str send complete");
        send_status   = REQUEST_SEND_AUTH_HEADER;
        bytes_written = 0;
      }
      else
        break;
    } // fallthrough

    case REQUEST_SEND_AUTH_HEADER:
    {
      if(auth_header_str.size() <= bytes_written)
      {
        DATA_APPSRV_MSG0(MSG_LEGACY_MED, "update_send_status - auth_header_str send complete");
        send_status   = REQUEST_SEND_HEADER_DELIM;
        bytes_written = 0;
      }
      else
        break;
    } // fallthrough

    case REQUEST_SEND_HEADER_DELIM:
    {
      if(DS_HTTP_HEADER_DELIMITER_LEN <= bytes_written)
      {
        DATA_APPSRV_MSG0(MSG_LEGACY_MED, "update_send_status - header delimiter send complete");
        send_status   = REQUEST_SEND_CONTENT;
        bytes_written = 0;
      }
      else
        break;
    } // fallthrough

    case REQUEST_SEND_CONTENT:
    {
      if(content.size() <= bytes_written)
      {
        DATA_APPSRV_MSG1(MSG_LEGACY_MED, "update_send_status - content send complete %d", content.size());
        send_status = REQUEST_SEND_COMPLETE;
      }
    }
    break;

    case REQUEST_SEND_COMPLETE: // fallthrough
    default:
      break;
  }
}



bool ds_http_request::send_complete() const
{
  return (REQUEST_SEND_COMPLETE == send_status);
}



bool ds_http_request::update_redirect(const char* redirect_uri, sint15* err)
{
  redirect_counter++;
  auth_header_str.clear(); // clear authentication info (no change if none)

  if(NULL == redirect_uri || MAX_REDIRECT_COUNT <= redirect_counter)
  {
    DATA_APPSRV_MSG0(MSG_LEGACY_ERROR, "status_redirect - Redirect loop detected");
    *err = DS_HTTP_ERROR_HTTP_STATUS;
    return false;
  }

  if(!decode_uri(redirect_uri))
  {
    DATA_APPSRV_MSG0(MSG_LEGACY_ERROR, "status_redirect - Redirect URI decode failed");
    *err = DS_HTTP_ERROR_HTTP_HEADER;
    return false;
  }

  create_request_str();

  if(header_str.error())
    return false;

  return true;
}



/*==============================================================================
                    ds_http_request - HTTP Authentication
==============================================================================*/
bool ds_http_request::create_auth_header_str(const ds_http_auth_info_s_type* client_auth_info)
{
  if(NULL == client_auth_info || 0 == client_auth_info->credential.username_len)
    return false;

  memset(&credential, 0, sizeof(ds_http_credential_s_type));
  memscpy(
          &credential,
          sizeof(ds_http_credential_s_type),
          &client_auth_info->credential,
          sizeof(ds_http_credential_s_type)
          );

  if(!auth_generator.generate_response(
                                       &(client_auth_info->credential),
                                       get_request_method_str(),
                                       uri_info.path,
                                       content.non_const_content(),
                                       content.size(),
                                       auth_header_str
                                       ))
  {
    DATA_APPSRV_MSG0(MSG_LEGACY_ERROR, "create_auth_header_str - Failed to generate auth header string");
    return false;
  }

  if(!add_cust_auth_param(client_auth_info))
  {
    DATA_APPSRV_MSG0(MSG_LEGACY_ERROR, "create_auth_header_str - Failed to append custom params");
    return false;
  }

  if(auth_header_str.error())
  {
    DATA_APPSRV_MSG0(MSG_LEGACY_ERROR, "create_auth_header_str - auth_header buffer error");
    return false;
  }

  create_cookie_header_str(client_auth_info->cookie, client_auth_info->num_cookies);
  if(cookie_header_str.error())
  {
    DATA_APPSRV_MSG0(MSG_LEGACY_ERROR, "create_auth_header_str - cookie_header buffer error");
    return false;
  }

  return true;
}


bool ds_http_request::decode_auth_header(const char* auth_str, uint32 http_status)
{
  return auth_generator.parse_auth_header(auth_str, http_status);
}



/* Note: Assumes caller checked that client_auth_info is not NULL */
bool ds_http_request::add_cust_auth_param(const ds_http_auth_info_s_type* client_auth_info)
{
  if(0 == client_auth_info->num_cust_auth_param)
    return true;

  if(DS_HTTP_CUST_PARAM_MAX_SIZE <= client_auth_info->num_cust_auth_param)
  {
    DATA_APPSRV_MSG0(MSG_LEGACY_ERROR, "add_cust_auth_param - Invalid additional auth param size");
    return false;
  }

  for(uint32 i = 0; i < client_auth_info->num_cust_auth_param; ++i)
  {
    if(0 == strlen(client_auth_info->cust_auth_param[i].name) || 0 == strlen(client_auth_info->cust_auth_param[i].value))
    {
      DATA_APPSRV_MSG0(MSG_LEGACY_ERROR, "add_cust_auth_param - Invalid auth param value");
      return false;
    }

    auth_header_str.append(",");
    auth_header_str.append(client_auth_info->cust_auth_param[i].name);
    auth_header_str.append("=");
    auth_header_str.append(client_auth_info->cust_auth_param[i].value);
  }
  return true;
}

