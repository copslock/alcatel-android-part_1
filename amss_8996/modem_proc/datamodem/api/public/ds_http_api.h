/*==============================================================================

                              ds_http_api.h

GENERAL DESCRIPTION
  API header for Data Service HTTP request

EXTERNALIZED FUNCTIONS

INITIALIZATION AND SEQUENCING REQUIREMENTS
  ds_http_init()
    - required initialization
    - ps_sys and ds_ssl must be initialized before

  Copyright (c) 2014 by Qualcomm Technologies Incorporated. All Rights Reserved.
==============================================================================*/

/*==============================================================================
                           EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

when        who    what, where, why
--------    ---    ----------------------------------------------------------
06/11/15    ml     Cookie support
04/20/15    ml     Session deregistration
03/10/15    ml     IWLAN / WLAN LB Support
12/02/14    ml     Added features for more detailed authentication requests.
07/21/14    ml     Created file/Initial version.
==============================================================================*/
#ifndef DS_HTTP_API_H
#define DS_HTTP_API_H

#include "ds_http_types.h"

#ifdef __cplusplus
extern "C"
{
#endif

/*==============================================================================
FUNCTION ds_http_open_session

DESCRIPTION
  Creates a new session for a http request.

PARAMETERS
  [In] http_cb_fcn  - Callback function to register
  [In] block_cb_fcn - Callback function to register
  [In] iface_info   - Iface information

RETURN VALUE
  On success, returns a session ID.
  On fail, returns DS_HTTP_ERROR.

DEPENDENCIES
  None
==============================================================================*/
uint32 ds_http_open_session(
                            ds_http_cb_fcn             http_cb_fcn,
                            ds_http_block_event_cb_fcn block_cb_fcn,
                            ds_http_iface_info_s_type* iface_info
                            );

uint32 ds_http_open_session_ex(
                               ds_http_cb_fcn_ex          http_cb_fcn,
                               ds_http_block_event_cb_fcn block_cb_fcn,
                               ds_http_iface_info_s_type* iface_info
                               );

/*==============================================================================
FUNCTION ds_http_close_session

DESCRIPTION
  Closes the session

PARAMETERS
  [In] session_id - The session to close

RETURN VALUE
  None

DEPENDENCIES
  None
==============================================================================*/
void ds_http_close_session(const uint32 session_id);


/*==============================================================================
FUNCTION ds_http_create_get_request

DESCRIPTION
  Creates a HTTP GET request.

PARAMETERS
  [In]  session_id     - The session ID obtained from ds_http_open_session
  [In]  uri            - Request uri string
  [In]  request_header - Header information for the request. NULL ok
  [Out] request_errno  - Error code

RETURN VALUE
  On success, a request identifier.
  On fail, returns DS_HTTP_ERROR and request_errno will be set.

  request_errno values
  --------------------
  DS_HTTP_ERROR_NO_URI_INFO
  DS_HTTP_ERROR_UNSUPPORTED_METHOD
  DS_HTTP_ERROR_INVALID_SESSION
  DS_HTTP_ERROR_NETDOWN
  DS_HTTP_ERROR_MEMALLOC


DEPENDENCIES
  ds_http_open_session must be called to create a session.
==============================================================================*/
uint32 ds_http_create_get_request(
                                  const uint32           session_id,
                                  const char*            uri,
                                  ds_http_header_s_type* request_header,
                                  uint16                 reserved_src_port,
                                  sint15*                request_errno
                                  );




/*==============================================================================
FUNCTION ds_http_create_post_request

DESCRIPTION
  Creates a HTTP POST request.

PARAMETERS
  [In]  session_id     - The session ID obtained from ds_http_open_session
  [In]  uri            - Request uri string
  [In]  request_header - Header information for the request. Can't be NULL and
                         content_type must not be an empty string
  [In]  content        - Content to post. Can't be NULL.
  [Out] request_errno  - Error code

RETURN VALUE
  On success, a request identifier.
  On fail, returns DS_HTTP_ERROR and request_errno will be set.

  request_errno values
  --------------------
  DS_HTTP_ERROR_NO_URI_INFO
  DS_HTTP_ERROR_UNSUPPORTED_METHOD
  DS_HTTP_ERROR_INVALID_SESSION
  DS_HTTP_ERROR_INVALID_PARAM
  DS_HTTP_ERROR_NETDOWN
  DS_HTTP_ERROR_MEMALLOC


DEPENDENCIES
  ds_http_open_session must be called to create a session.
==============================================================================*/
uint32 ds_http_create_post_request(
                                   const uint32           session_id,
                                   const char*            uri,
                                   ds_http_header_s_type* request_header,
                                   const uint8*           content_data,
                                   const uint32           content_size,
                                   uint16                 reserved_src_port,
                                   sint15*                request_errno
                                   );



/*==============================================================================
FUNCTION ds_http_resolve_auth_block

DESCRIPTION
  Provide authentication information to the request with 401/407 error.
  Providing NULL or empty credentials will fail the request and notify a 401/407
  error in the http callback function.

  This function will have no effect if
    - Session/request ID is invalid
    - Authentication block event didn't occur for the request
    - Socket times out before client provides the credentials

PARAMETERS
  [In] session_id - Session ID given by ds_http_open_session
  [In] request_id - Request ID given by ds_http_create_xxx_request
  [In] auth_info  - Information needed to resolve authentication status error,
                    such as username and password to authenticate.
                    Giving a NULL pointer will fail the request and will notify
                    the failure through the http callback fcn

RETURN VALUE
  Success
    DS_HTTP_ERROR_NONE
  Fail
    DS_HTTP_ERROR_INVALID_SESSION

DEPENDENCIES
  None
==============================================================================*/
void ds_http_resolve_auth_block(
                                uint32                    session_id,
                                uint32                    request_id,
                                ds_http_auth_info_s_type* auth_info
                                );



/*==============================================================================
FUNCTION ds_http_resolve_ssl_cert_block

DESCRIPTION
  Updates the request to handle SSL block event.

  This function will have no effect if
    - Session/request ID is invalid
    - SSL block event didn't occur for the request
    - Socket times out before client provides the credentials

PARAMETERS
  [In] session_id - Session ID given by ds_http_open_session
  [In] request_id - Request ID given by ds_http_create_xxx_request
  [In] cont       - DS_HTTP_SSL_CONTINUE to ignore SSL alert and continue.
                    DS_HTTP_SSL_ABORT to fail the request. There will be a
                    request failure notification in the callback.

RETURN VALUE
  Success - DS_HTTP_ERROR_NONE
  Fail    - DS_HTTP_ERROR_INVALID_SESSION

DEPENDENCIES
  None
==============================================================================*/
void ds_http_resolve_ssl_cert_block(
                                    uint32  session_id,
                                    uint32  request_id,
                                    boolean cont
                                    );


/*==============================================================================
FUNCTION ds_http_is_nw_srv_avail

DESCRIPTION
  Query for network service status.

PARAMETERS
  [In] iface - The iface to query network status for

RETURN VALUE
  TRUE if network service is available, else FALSE

DEPENDENCIES
  None
==============================================================================*/
boolean ds_http_is_nw_srv_avail(ds_http_iface_e_type iface);
boolean ds_http_is_nw_srv_avail_for_session_type(
                                                 ds_http_iface_e_type        iface,
                                                 ds_http_session_type_e_type session_type
                                                 );



#ifdef __cplusplus
}
#endif

#endif /* DS_HTTP_API_H */

