/*!
  @file
  ds_3gpp_loopback_hdlr.h

  @brief
  REQUIRED brief one-sentence description of this C module.

  @detail
  OPTIONAL detailed description of this C module.
  - DELETE this section if unused.

*/

/*===========================================================================

  Copyright (c) 2009-2014 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

  $Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/datamodem/3gpp/ps/inc/ds_3gpp_loopback_hdlr.h#1 $ $DateTime: 2016/03/28 23:02:50 $ $Author: mplcsds1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
09/30/14   sb     Loopback modehandler using 3gppmodehandler wm

===========================================================================*/



#include "cm.h"
#include "cm_gw.h"
#include "ds_3gpp_pdn_context.h"
#include "ds_3gpp_bearer_context.h"
#include "rrcdata.h"



typedef enum lb_client_event_reg_e {
  LB_CLIENT_EVENT_REG,
    /**< Event registration. */
  LB_CLIENT_EVENT_DEREG,
    /**< Event deregistration. */
  LB_CLIENT_SS_EVENT_REG,
  LB_CLIENT_EVENT_MAX   /* FOR INTERNAL CM USE ONLY! */
/** @endcond
*/

} lb_client_event_reg_e_type;


typedef enum
{
  LOOPBACK_TOKEN_DEFAULT = 0,
  LOOPBACK_TOKEN_LOOPBACK_ENABLED,
  LOOPBACK_TOKEN_SYS_MODE,
  LOOPBACK_TOKEN_DL_MULF,
  LOOPBACK_TOKEN_DL_DUPF,
  LOOPBACK_TOKEN_UL_ONLY
}ds_3gpp_loopback_hdlr_token;

typedef struct
{
  uint8 current_eps_id;
  uint8 current_call_id;
  uint8 lb_dl_mulf;
  uint8 lb_dl_dupf;
  uint8 lb_ul_only;
  boolean ds_3gpp_loopback_mode;
  ps_sig_enum_type  lb_rx_sig;
  sys_sys_mode_e_type  loopback_sys_mode;
  sys_modem_as_id_e_type loopback_subs_id;
}ds_3gpp_loopback_context_info_type;


typedef enum lb_client_status_e {
  LB_CLIENT_ERR_NONE=-1,   /* FOR INTERNAL CM USE ONLY! */
  LB_CLIENT_OK,
  LB_CLIENT_ERR_EVENT_REG,
  LB_CLIENT_MAX
}lb_client_status_e_type;



typedef struct
{
  cm_call_event_e_type cm_event_type;
  cm_mm_call_event_f_type  *call_event_func;
}lb_call_info;

typedef struct
{
  cm_ss_event_e_type             cm_ss_event_type;
  cm_mm_msim_ss_event_f_type      *ss_event_func;
}lb_call_ss_info;



typedef struct{
  uint8                is_active;
  uint8                loop_back_call_id;
  uint8                loop_back_eps_id;
  uint8                loop_back_rb_id;
  boolean              v4_call;
  boolean              v6_call;
  boolean              ra_received;
  dsm_watermark_type*  tx_wm_ptr;
  dsm_watermark_type*  rx_wm_ptr;
}loopback_wm_info_type;

typedef struct
{
  uint8 eps_id;
  uint8 inst_id;
}loopback_wm_reg_type;



lb_client_status_e_type ds_3gpp_loopback_client_call_reg(
    cm_mm_call_event_f_type         *call_event_func,
        /* Pointer to a callback function to notify the client of call
        ** events */
    lb_client_event_reg_e_type  event_reg_type,
    cm_call_event_e_type        call_event
);

lb_client_status_e_type ds_3gpp_loopback_client_ss_call_reg(
    cm_mm_msim_ss_event_f_type      *ss_event_func,
        /* Pointer to a callback function to notify the client of call
        ** events */
    lb_client_event_reg_e_type  event_reg_type,
    cm_ss_event_e_type        call_event
);

/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_PDN_CONNECTIVITY REQUEST

DESCRIPTION
  This function is responsible for handling pdn connectivty sent as part of
  Lte call bring up in case of Loopback configuration. This function is responsible
  for setting up the apn, ip address based on the pdp type and send an
  activate bearer indication This function is also responsible for setting up the
  loopback wm info table with the call id, eps id  and initialize the singal for
  further data processing.
 
PARAMETERS
  cm_call_cmd_cb_f_type  cmd_cb_func,
  void                   *data_block_ptr,
  const cm_pdn_connectivity_s_type   *pdn_connectivity_ptr
    pointer to the request specific data
 
 
DEPENDENCIES
  

RETURN VALUE
..
SIDE EFFECTS
  None.

===========================================================================*/

boolean ds_3gpp_loopback_pdn_connectivity_req(

  cm_call_cmd_cb_f_type  cmd_cb_func,
    /**< client callback function */

  void                   *data_block_ptr,
    /**< pointer to client callback data block */


  const cm_pdn_connectivity_s_type   *pdn_connectivity_ptr
    /**< pointer to the request specific data */
);


void ds_3gpp_loopback_hdlr_attach();


void ds_3gpp_loopback_send_pdnconnection_ind();

/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_PDN_CONNECTIVITY REQUEST

DESCRIPTION
  This function is responsible for handling activate bearer response as part of
  Lte call bring up in case of Loopback configuration.This function is responsible
  for sending cm call connected event followed by attach complte incase of INITIAL
  EPS Bearer. This functions is also responsible for sending the Rab Reestabilishment
  Indication which will inturn set up the data path.
 
PARAMETERS
  call_id - call id passed during bearer activation
  act_bearer_rsp_ptr - activate bearer response pointer

DEPENDENCIES
  

RETURN VALUE
..
SIDE EFFECTS
  None.

===========================================================================*/

boolean ds_3gpp_loopback_act_bearer_rsp
(
   cm_call_id_type                  call_id,
   const cm_act_bearer_rsp_s_type   *act_bearer_rsp_ptr
);

/*===========================================================================
FUNCTION  DS_3GPP_LOOPBACK_LTE_PDCPDL_RAB_REGISTER_REQ

DESCRIPTION
  This function is responsible for handling lte pdcp ul rab registration.
  This function stores the uplink wm pointer in the loopback_wm_table 
 
PARAMETERS
  eps_bearer_id      eps bearer id
  tx_wm_item         tx wm pointer
  uint8              inst_id

DEPENDENCIES
  

RETURN VALUE
..
SIDE EFFECTS
  None.

===========================================================================*/

boolean ds_3gpp_loopback_lte_pdcpul_rab_register_req
(
   uint8 eps_bearer_id,
   dsm_watermark_type* tx_wm_item,
   uint8                  inst_id
);

/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_LTE_PDCPDL_RAB_REGISTER_REQ

DESCRIPTION
  This function is responsible for handling lte pdcp dl rab registration.
  This function stores the downlink wm pointer in the loopback_wm_table.
  This function is also reponsible for setting up the signal handler and
  register for nonempty callback function.
 
PARAMETERS
  eps_bearer_id      eps bearer id
  rx_wm_item         rx wm pointer
  inst_id

DEPENDENCIES
  

RETURN VALUE
..
SIDE EFFECTS
  None.

===========================================================================*/

boolean ds_3gpp_loopback_lte_pdcpdl_rab_register_req
(
   uint8 eps_bearer_id,
   dsm_watermark_type* rx_wm_item,
   uint8                  inst_id
);


/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_UL_RAB_REGISTER_CNF

DESCRIPTION
  This function is responsible for sending lte pdcp ul rab registration
  confirmation
 
PARAMETERS
  eps_id      eps bearer id
  inst_id     subscription id

DEPENDENCIES
  

RETURN VALUE
..
SIDE EFFECTS
  None.

===========================================================================*/

void ds_3gpp_loopback_ul_rab_register_cnf
(
   uint8 eps_id,
   uint8 inst_id
);

/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_DL_RAB_REGISTER_CNF

DESCRIPTION
  This function is responsible for sending lte pdcp dl rab registration
  confirmation
 
PARAMETERS
  eps_id      eps bearer id
  inst_id     subscription id

DEPENDENCIES
  

RETURN VALUE
..
SIDE EFFECTS
  None.

===========================================================================*/

void ds_3gpp_loopback_dl_rab_register_cnf
(
   uint8 eps_id,
   uint8 inst_id
);
/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_HDLR_PROCESS_CMDS

DESCRIPTION
  This function is responsible for processing the UL confirmation as well
  as DL confirmation.
 
PARAMETERS
  cmd_ptr

DEPENDENCIES
  

RETURN VALUE
..
SIDE EFFECTS
  None.

===========================================================================*/

void ds_3gpp_loopback_hdlr_process_cmds
(
  const ds_cmd_type      *cmd_ptr
);

void ds_3gpp_loopback_hdlr_tx_data
(
  dsm_watermark_type *wm_p,
  void               *call_back_data
);

unsigned char ds_3gpp_loopback_hdlr_process_data_pkt
( 
  ps_sig_enum_type sig, 
  void *user_data_p
);  

/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_HDLR_PDN_DISCONNECT_REQ

DESCRIPTION
  This function is responsible for handling pdn disconnect request in case of
  LTE loopback mode.This function inturn will invoke CM_CALL_EVENT_END 
 
PARAMETERS
  eps_id  -eps bearer id
  pdn_disconnect_ptr - pdn disconnect ptr

DEPENDENCIES
  

RETURN VALUE
..TRUE/FALSE - indicating whether operation is succcesful 
 
SIDE EFFECTS
  None.

===========================================================================*/

boolean ds_3gpp_loopback_hdlr_pdn_disconnect_req
(
  uint8 eps_id,
  const cm_pdn_disconnect_s_type   *pdn_disconnect_ptr
);

/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_SEND_FULL_SERVICE_IND

DESCRIPTION
  This function is responsible for sending full service indication This function
  is used to send  CM_SS_EVENT_SRV_CHANGED for the sys mode read as part of
  loopback efs configuration
 
PARAMETERS
  None

DEPENDENCIES
  None

RETURN VALUE
  None 
SIDE EFFECTS
  None.

===========================================================================*/

void ds_3gpp_loopback_hdlr_send_fullservice_ind();

/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_RES_ALLOC_REQ

DESCRIPTION
  This function is responsible for handling resource allocation request from the
  3GPP Modehandler in case of Loopback configuration. This inturn will send  activate
  bearer indication and intiate a network initiated bearer.
 
PARAMETERS
  bearer_alloc_params_ptr - bearer alloc paramaters pointer

DEPENDENCIES
  None

RETURN VALUE
  ..TRUE/FALSE - indicating whether operation is succcesful
 
SIDE EFFECTS
  None.

===========================================================================*/
boolean ds_3gpp_loopback_res_alloc_req
(
  cm_res_alloc_s_type *bearer_alloc_params_ptr
);


/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_HDLR_CALL_CMD_ORIG_CC_PER_SUBS

DESCRIPTION
  This function is responsible for handling call cmd origination for all non
  lte mode of operation in case of Loopback config
 
PARAMETERS
 gw_ps_orig_params_ptr    - gw ps origination paramaters 
 asubs_id,                - subs_id
	*return_call_id_ptr      - return call_id_ptr

DEPENDENCIES
  None

RETURN VALUE
  ..TRUE/FALSE - indicating whether operation is succcesful
 
SIDE EFFECTS
  None.

===========================================================================*/

boolean ds_3gpp_loopback_hdlr_call_cmd_orig_cc_per_subs
(
  cm_gw_ps_orig_params_s_type *gw_ps_orig_params_ptr,
	sys_modem_as_id_e_type      asubs_id,
  cm_call_id_type             *return_call_id_ptr
);


/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_HDLR_RRC_RETURN_LC_INFO_FOR_RAB

DESCRIPTION
  This function is responsible for filling the lc information pointer based
  on the rab id passed by the client.This function is invoked as part of WCDMA/TDS
  mode of operation
 
PARAMETERS
  lc_info_ptr  - Logical channel information pointer
 
DEPENDENCIES
  None

RETURN VALUE
  ..RRC_RAB_FOUND/RRC_RAB_NOT_FOUND - indicating whether operation is succcesful
 
SIDE EFFECTS
  None.

===========================================================================*/
rrc_rab_search_e_type ds_3gpp_loopback_hdlr_rrc_return_lc_info_for_rab
(
  rrc_user_plane_lc_info_type *lc_info_ptr
);

/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_L2_REG_UL_WM

DESCRIPTION
  This function is responsible for registering the uplink wm with the coresponding
  rb id passed .This function is invoked as part of all non-LTE mode of operation.

 
PARAMETERS
  rlc_ul_id - rlc id/nsapi in case of Wcdma/TDS/gsm
  tx_wm_ptr - tx wm pointer
 
DEPENDENCIES
  None

RETURN VALUE
  
 
SIDE EFFECTS
  None.

===========================================================================*/

void ds_3gpp_loopback_l2_reg_ul_wm
(
  uint8 rlc_ul_id,
  dsm_watermark_type* tx_wm_ptr
);

/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_L2_REG_DL_WM

DESCRIPTION
  This function is responsible for registering the downlink wm with the coresponding
  rb id passed .This function is invoked as part of all non-LTE mode of operation.

 
PARAMETERS
  rlc_ul_id - rlc id/nsapi in case of Wcdma/TDS/gsm
  rx_wm_ptr - tx wm pointer
 
DEPENDENCIES
  None

RETURN VALUE
  
 
SIDE EFFECTS
  None.

===========================================================================*/

void ds_3gpp_loopback_l2_reg_dl_wm
(
  uint8 rlc_dl_id,
  dsm_watermark_type* rx_wm_ptr
);

/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_HDLR_CALL_CMD_END

DESCRIPTION
  This function is responsible for handling call cmd end in case of Umts mode
  operation when Loopback is configured. 

 
PARAMETERS
  call_end_params - call end paramters pointer
 
DEPENDENCIES
  None

RETURN VALUE
  
 
SIDE EFFECTS
  None.

===========================================================================*/
boolean ds_3gpp_loopback_hdlr_call_cmd_end
(
  cm_end_params_s_type  *call_end_params
);

/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_HDLR_SYS_MODE

DESCRIPTION
  This function is used to return the current loopback handler sys mode.
  This is read as part of efs configuration information
 
PARAMETERS
  None

DEPENDENCIES
  None.

RETURN VALUE
..loopback_sys_mode -current loopback sys mode
 
SIDE EFFECTS
  None.

===========================================================================*/

sys_sys_mode_e_type ds_3gpp_loopback_hdlr_sys_mode();


/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_IS_ENABLED

DESCRIPTION
  This function is used to check whether Loopback is enabled in the current
  mode of operation.
 
PARAMETERS

DEPENDENCIES
  

RETURN VALUE
..boolean - TRUE/FALSE (indicating whether Loopback is enabled)

SIDE EFFECTS
  None.

===========================================================================*/

boolean ds_3gpp_loopback_is_enabled();

/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_HDLR_READ_EFS

DESCRIPTION
  This function is used to read the efs during dstask initialization.This function
  will be used to read the following paramters:
  LOOPBACK_ENABLED:1
  SYS MODE:9
  DL_MULF:3
  DL_DUPF:0
  UL_ONLY:0
 
PARAMETERS

DEPENDENCIES
  

RETURN VALUE
..boolean - TRUE/FALSE (indicating whether Loopback is enabled)

SIDE EFFECTS
  None.

===========================================================================*/

void ds_3gpp_loopback_hdlr_read_efs();


/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_HDLR_CHECK_TOKENID

DESCRIPTION
  This function is used to check the paramter read from the efs file against
  the preconfigured loopback parameters and approprioate token id is returned
 
PARAMETERS
  from-
  to-

 
 
DEPENDENCIES
  

RETURN VALUE
..ds_3gpp_loopback_hdlr_token - Token read from the efs

SIDE EFFECTS
  None.

===========================================================================*/

ds_3gpp_loopback_hdlr_token ds_3gpp_loopback_hdlr_check_token_id
(
  char *from,
  char *to
);

/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_HDLR_SNDCP_PDP_REQ

DESCRIPTION
  This function is responsible for sending sndcp pdcp reg confirmation
  in case of gsm mode of operation. This callback invokation is needed to
  configure the wm and registration

 
PARAMETERS
  nsapi,
  pdp_ul_suspend_fnc_ptr,
  pdp_ul_resume_fnc_ptr,
 ( *pdp_dl_fnc_ptr )( void* ds_context, dsm_item_type **npdu ),
 *ds_context,
 cipher,
( *pdp_reg_cnf_fnc_ptr )( void* ds_context, boolean success )
 
DEPENDENCIES
  None

RETURN VALUE
  
 
SIDE EFFECTS
  None.

===========================================================================*/

void  ds_3gpp_loopback_hdlr_sndcp_pdp_reg
(
  uint8        nsapi,
  wm_cb_type   pdp_ul_suspend_fnc_ptr,
  wm_cb_type   pdp_ul_resume_fnc_ptr,
  void         ( *pdp_dl_fnc_ptr )( void* ds_context, dsm_item_type **npdu ),
  void         *ds_context,
  boolean      cipher,
  void         ( *pdp_reg_cnf_fnc_ptr )( void* ds_context, boolean success )
);

/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_HDLR_SEND_CM_CALL_BACK_EVENT

DESCRIPTION
  This function is responsible for calling the appropriate cm call event function
  based on the event and call info pointer passed
 
PARAMETERS
..cm_event_type - CM Call Event Type 
  call_info_ptr - CM Call Info Pointer
 
DEPENDENCIES
  

RETURN VALUE
..
SIDE EFFECTS
  None.

===========================================================================*/
void ds_3gpp_loopback_hdlr_send_cm_call_back_event
(
   cm_call_event_e_type cm_event_type,
   cm_mm_call_info_s_type *call_info_ptr
);

/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_PDP_DATA_CTRL_SETUP_CB

DESCRIPTION
  This function sets up the path to enqueue data between two bearers
 
PARAMETERS
  *phys_link_ptr - ptr to the phys link
  **item_ptr - ptr to items to be enqueued
  *meta_info_ptr - ptr to ps meta info
  *tx_info_ptr - ptr to bearer context
 
DEPENDENCIES
  None.

RETURN VALUE
  None.
 
SIDE EFFECTS
  None.

===========================================================================*/
void ds_3gpp_loopback_pdp_data_ctrl_setup_cb
(
  ps_phys_link_type *phys_link_ptr,
  dsm_item_type     **item_ptr,
  ps_meta_info_type *meta_info_ptr,
  void              *tx_info_ptr
);

/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_PDP_IP_TX_UM_DATA_CB

DESCRIPTION
  This function directly enqueues the data into the other bearer's rx wm
 
PARAMETERS
  *phys_link_ptr - ptr to the phys link
  **item_ptr - ptr to items to be enqueued
  *meta_info_ptr - ptr to ps meta info
  *tx_info_ptr - ptr to bearer context
 
DEPENDENCIES
  None.

RETURN VALUE
  None.
 
SIDE EFFECTS
  None.

===========================================================================*/
void ds_3gpp_loopback_pdp_ip_tx_um_data_cb
(
  ps_phys_link_type *phys_link_ptr,
  dsm_item_type     **item_ptr,
  ps_meta_info_type *meta_info_ptr,
  void              *tx_info_ptr
);

/*===========================================================================
FUNCTION DS_3GPP_LOOPBACK_GET_IPV4_ADDR

DESCRIPTION
  This function sets the ra_received flag for a bearer to indicate that ipv6
  is complete
 
PARAMETERS
  bearer_id - bearer to set the flag
 
DEPENDENCIES
  None.

RETURN VALUE
  None.
 
SIDE EFFECTS
  None.

===========================================================================*/
void ds_3gpp_loopback_set_bearer_ra_received
(
  uint8 bearer_id
);

