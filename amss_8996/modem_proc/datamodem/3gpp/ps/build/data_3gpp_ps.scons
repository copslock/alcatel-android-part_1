#===============================================================================
#
# GENERAL DESCRIPTION
#    build script
#
# Copyright (c) 2009-2014 by Qualcomm Technologies, Incorporated.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#
#-------------------------------------------------------------------------------
#
#  $Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/datamodem/3gpp/ps/build/data_3gpp_ps.scons#1 $
#  $DateTime: 2016/03/28 23:02:50 $
#
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
# 11/12/10   ack     Initial revision
#===============================================================================
Import('env')

from glob import glob
from os.path import join, basename

#if ('USES_WPLT' in env or 'USES_UMTS' not in env) and 'USES_GSM' not in env:
#    Return()

#turn off debug if requested  
if ARGUMENTS.get('DEBUG_OFF','no') == 'yes':
    env.Replace(ARM_DBG = "")
    env.Replace(HEXAGON_DBG = "")
    env.Replace(GCC_DBG = "")


#turn on debug if requested
if ARGUMENTS.get('DEBUG_ON','no') == 'yes':
    env.Replace(ARM_DBG = "-g --dwarf2") 
    env.Replace(HEXAGON_DBG = "-g")  
    env.Replace(GCC_DBG = "-g")
	
env.Replace(MSVC_WARN = '/W0')	

#-------------------------------------------------------------------------------
# Necessary Core Public API's
#-------------------------------------------------------------------------------
CORE_PUBLIC_APIS = [
    'DEBUGTOOLS',
    'SERVICES',
    'SYSTEMDRIVERS',
    'WIREDCONNECTIVITY',
    'STORAGE',
    'SECUREMSM',
    'BUSES',
    'DAL',
	'POWER',
	'MPROC',

    # needs to be last also contains wrong comdef.h
    'KERNEL',
    ]

env.RequirePublicApi(CORE_PUBLIC_APIS, area='core')

#-------------------------------------------------------------------------------
# Necessary Modem Public API's
#-------------------------------------------------------------------------------
MODEM_PUBLIC_APIS = [
    'MMODE',
    'DATACOMMON',
    'DATAMODEM',
    'UIM',
    'MCS',
    'ONEX',
    'DATA',
    'HDR',
    'WMS',
    'PBM',
    'NAS',
    'WCDMA',
    'UTILS',
    'GERAN',
    'CNE',
	'ECALL',
    ]

env.RequirePublicApi(MODEM_PUBLIC_APIS)

segment_load_public_api_list = [
       ('MCFG',                'MCFG'),
       ('GPS',                 'GPS'),
       ('RFA',                 'CDMA'),
       ('RFA',                 'MEAS'),
       ('FW',                  'GERAN'),
       ('RFA',                 'LM'),
       ('RFA',                 'GSM'),
       ('RFA',                 'GNSS'),
       ('RFA',                 'LTE'),
       ('FW',                  'RF'),
       ('FW',                  'COMMON'),
       ]

for api_area,api_name in segment_load_public_api_list:
    env.RequirePublicApi([api_name], area=api_area)

#-------------------------------------------------------------------------------
# Necessary Modem Restricted API's
#-------------------------------------------------------------------------------
MODEM_RESTRICTED_APIS = [
    'DATA',
    'MODEM_DATA',
    'MODEM_DATACOMMON',
    'DATACOMMON',
    'DATAMODEM',
    'MCS',
    'ONEX',
    'NAS',
    'HDR',
    'MMODE',
    'RFA',
    'GERAN',
    'UIM',
    'WCDMA',
    'MDSP',
    'UTILS',
    'LTE',
    'CNE',
    ]

env.RequireRestrictedApi(MODEM_RESTRICTED_APIS)

#-------------------------------------------------------------------------------
#  OffTarget/QTF
#-------------------------------------------------------------------------------
if 'USES_MOB' in env:
  env.RequireRestrictedApi(['DATAMODEM_QTF_OFT'])

#-------------------------------------------------------------------------------
# Setup source PATH
#-------------------------------------------------------------------------------
SRCPATH = ".."

env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0)

#-------------------------------------------------------------------------------
# Setup UMIDs
#-------------------------------------------------------------------------------

env.AddUMID('${BUILDPATH}/data_3gpp_ps.umid', ['../inc/ds_3gpp_msg.h'])

#-------------------------------------------------------------------------------
# Generate the library and add to an image
#-------------------------------------------------------------------------------
#code shipped as source

#PS_FILES = glob(SRCPATH + '/src/ds*.c')
PS_SOURCES = []

#for filename in PS_FILES:
   #PS_SOURCES.append('${BUILDPATH}/' + filename.replace(SRCPATH,''))

PS_SOURCE_FILES = [
  '${BUILDPATH}/src/ds_3gpp_a2.c',
  '${BUILDPATH}/src/ds_3gpp_api.c',
  '${BUILDPATH}/src/ds_3gpp_apn_switch_mgr.c',
  '${BUILDPATH}/src/ds_3gpp_apn_table.c',
  '${BUILDPATH}/src/ds_3gpp_auth_mgr.c',
  '${BUILDPATH}/src/ds_3gpp_bearer_context.c',
  '${BUILDPATH}/src/ds_3gpp_bearer_flow_manager.c',
  '${BUILDPATH}/src/ds_3gpp_cc_sp_throt_config.c',
  '${BUILDPATH}/src/ds_3gpp_device_ev_hdlr.c',
  '${BUILDPATH}/src/ds_3gpp_dyn_mem_hdlr.c',
  '${BUILDPATH}/src/ds_3gpp_epc_if.c',
  '${BUILDPATH}/src/ds_3gpp_flow_context.c',
  '${BUILDPATH}/src/ds_3gpp_flow_ev_cb_hdlr.c',
  '${BUILDPATH}/src/ds_3gpp_flow_manager.c',
  '${BUILDPATH}/src/ds_3gpp_hdlr.c',
  '${BUILDPATH}/src/ds_3gpp_iface_ev_cb_hdlr.c',
  '${BUILDPATH}/src/ds_3gpp_loopback_hdlr.c',
  '${BUILDPATH}/src/ds_3gpp_network_override_hdlr.c',
  '${BUILDPATH}/src/ds_3gpp_nv_manager.c',
  '${BUILDPATH}/src/ds_3gpp_op_pco_hdlr.c',
  '${BUILDPATH}/src/ds_3gpp_pdn_context.c',
  '${BUILDPATH}/src/ds_3gpp_pdn_limit_hdlr.c',
  '${BUILDPATH}/src/ds_3gpp_pdn_redial_hdlr.c',
  '${BUILDPATH}/src/ds_3gpp_pdn_throt_config.c',
  '${BUILDPATH}/src/ds_3gpp_pdn_throttle_sm.c',
  '${BUILDPATH}/src/ds_3gpp_phys_link_ev_cb_hdlr.c',
  '${BUILDPATH}/src/ds_3gpp_plmn_hdlr.c',
  '${BUILDPATH}/src/ds_3gpp_roaming_hdlr.c',
  '${BUILDPATH}/src/ds_3gpp_rpm.c',
  '${BUILDPATH}/src/ds_3gpp_rt_acl.c',
  '${BUILDPATH}/src/ds_3gpp_srv_req_throt_config.c',
  '${BUILDPATH}/src/ds_3gpp_stats.c',
  '${BUILDPATH}/src/ds_3gpp_tft.c',
  '${BUILDPATH}/src/ds_3gpp_throttle_sm.c',
  '${BUILDPATH}/src/ds_3gppi_utils.c',
  '${BUILDPATH}/src/ds_um_lo.c',
  '${BUILDPATH}/src/dsumtspsmthdlr.c',
  '${BUILDPATH}/src/dsumtspspco.c',
  '${BUILDPATH}/src/dsumtspsqos.c',
  '${BUILDPATH}/src/dsumtspstft.c',
]

PS_SOURCES = []

PS_SOURCES += PS_SOURCE_FILES

if 'USES_UMTS' in env or ('USES_WCDMA' in env and 'USES_PDCP' in env) or 'USES_GSM' in env:
   PS_SOURCES.extend([ '${BUILDPATH}/src/comptask.c', ])

env.AddLibrary(['MODEM_MODEM', 'MOB_DATAMODEM' ], '${BUILDPATH}/ps',[PS_SOURCES, ])

#code shipped as binary library
LIB_PS_SOURCE = [
   '${BUILDPATH}/src/ds_3gpp_diag_hdlr.c'
]

env.AddBinaryLibrary(['MODEM_MODEM', 'MOB_DATAMODEM',],
                      '${BUILDPATH}/lib_ps',
                      [LIB_PS_SOURCE, ])

# Build image for which this task belongs
RCINIT_COMP  = ['MODEM_MODEM', 'MOB_DATAMODEM']

# RC Init Function Dictionary
RCINIT_INIT_COMP = {
	    'sequence_group'      : env.subst('$MODEM_UPPERLAYER'),
	    'init_name'           : 'comp_init',
	    'init_function'       : 'comp_task_init',
	    # 'dependencies'      : ['cfm']
    }

# RC Init Task Dictionary
RCINIT_TASK_COMP = {
	    'thread_name'         : 'COMP',
	    'sequence_group'      : env.subst('$MODEM_UPPERLAYER'),
	    'stack_size_bytes'    : env.subst('$COMP_STKSZ'),
	    'priority_amss_order' : 'COMP_PRI_ORDER',
	    'stack_name'          : 'comp_stack',
	    'thread_entry'        : 'comp_task',
	    'tcb_name'            : 'comp_tcb',
            'cpu_affinity'	  : env.subst('$MODEM_CPU_AFFINITY')
    }
    
# Add init function to RCInit
if 'USES_MODEM_RCINIT' in env:
	    env.AddRCInitTask(RCINIT_COMP, RCINIT_TASK_COMP)
