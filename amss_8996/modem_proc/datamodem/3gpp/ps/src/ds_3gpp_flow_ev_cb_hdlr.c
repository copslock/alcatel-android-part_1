/*!
  @file
  ds_3gpp_flow_ev_cb_hdlr.c

  @brief
  REQUIRED brief one-sentence description of this C module.

  @detail
  OPTIONAL detailed description of this C module.
  - DELETE this section if unused.

*/

/*===========================================================================

  Copyright (c) 2014 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.
  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

  $Header: //components/dev/data.mpss/3.3/vbhanupr.data.mpss.3.3.pw33_11_24/3gpp/ps/src/ds_3gpp_flow_ev_cb_hdlr.c#1
  $ $DateTime: 2016/03/28 23:02:50 $ $Author: mplcsds1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
11/25/14   vb     flow ev callback handler

===========================================================================*/

#include "ds_3gpp_flow_ev_cb_hdlr.h"
#include "ps_flowi_event.h"

/*-------------------------------------------------------------------------- 
  Structure definitions
  --------------------------------------------------------------------------*/
ds_3gpp_flow_ev_cb_queue_type ds_3gpp_flow_ev_queue
                                            [DS_3GPP_MAX_FLOW_EV_CNT];

/*-------------------------------------------------------------------------- 
  Buffer for flow registrations
  --------------------------------------------------------------------------*/
static void *ds_3gpp_flow_ev_buf_ptr = NULL;

/*------------------------------------------------------------------------ 
  Structures to hold flow reg/dereg events and their count.
  This will always be freed dynamically after registration/deregistration
  ------------------------------------------------------------------------*/
typedef struct
{
  uint8                    num_flow_ev;
  ps_iface_event_enum_type ds_3gpp_flow_ev_list_p[DS_3GPP_MAX_FLOW_EV_CNT];
}ds_3gpp_flow_reg_dereg_list_type;

/*===========================================================================
FUNCTION  DS_3GPP_FLOW_EV_CBACK

DESCRIPTION
  This function gets called when any flow event is posted from PS framework
  
PARAMETERS 
  this_flow_ptr:   Iface on which up event was posted
  event:            Event that caused the cback to be invoked
  event_info:       Event info posted by PS
  user_data_ptr:    Data to be passed to the cback function

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/

void ds_3gpp_flow_ev_cback
(
  ps_flow_type                            *this_flow_ptr,
  ps_iface_event_enum_type                 ps_event,
  ps_iface_event_info_u_type               event_info,
  void                                    *user_data_ptr
);

/*===========================================================================
FUNCTION  DS_3GPP_MAP_FLOW_EV_TO_DS

DESCRIPTION
  This function converts PS flow event to the registered PS flow events
  
PARAMETERS 
 
DEPENDENCIES
  None

RETURN VALUE
  ds_3gpp_flow_ev_list 

SIDE EFFECTS
  None

===========================================================================*/
static ds_3gpp_flow_ev_enum_type ds_3gpp_map_flow_ev_to_ds
(
  ps_iface_event_enum_type   ps_event
);

/*===========================================================================
FUNCTION DS_3GPP_FLOW_ALLOC_EV_CB_DATA

DESCRIPTION 
    This function allocates the ev callback for the specified flow event

 
PARAMETERS
    ds_3gpp_flow_ev_list   - flow event where memory needs to be allocated
 
DEPENDENCIES
    None.

RETURN VALUE 
    int       -1       if unsuccessful
                       allocated_index if successful
  
   
SIDE EFFECTS 
  None.
===========================================================================*/

static int ds_3gpp_flow_alloc_ev_cb_data
(
   ds_3gpp_flow_ev_enum_type flow_ev
)
{
  int     ret_val              = -1;
  uint8   cb_cnt               = 0;
  ds_3gpp_flow_ev_cb_data_type   *flow_ev_cb_p = NULL;
 /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  if(flow_ev >= DS_3GPP_MAX_FLOW_EV_CNT)
  {
    DS_3GPP_MSG1_ERROR("flow_ev:%d passed is INVALID",flow_ev);
    return ret_val;
  }

  if(ds_3gpp_flow_ev_buf_ptr == NULL)
  {
    DS_3GPP_MSG0_HIGH("Allocating flow reg buf");
    ds_3gpp_flow_ev_buf_ptr =  ps_flow_alloc_event_handle(NULL,
                                                          ds_3gpp_flow_ev_cback,
                                                          NULL);
    if(ds_3gpp_flow_ev_buf_ptr == NULL)
    {
      DS_3GPP_MSG0_ERROR("flow ev buf ptr allocation failed");
      ASSERT(0);
      return ret_val;
    }
  }

  for(cb_cnt = 0; cb_cnt < DS_3GPP_FLOW_MAX_CB_CNT;cb_cnt++)
  {
    flow_ev_cb_p = ds_3gpp_flow_ev_queue[flow_ev].flow_ev_cb_data_ptr[cb_cnt];

    if(flow_ev_cb_p != NULL)
    {
      continue;
    }

    flow_ev_cb_p = (ds_3gpp_flow_ev_cb_data_type *)modem_mem_alloc
                    (sizeof(ds_3gpp_flow_ev_cb_data_type),
                    MODEM_MEM_CLIENT_DATA);

    if(flow_ev_cb_p == NULL)
    {
      DS_3GPP_MSG0_ERROR("Heap allocation error");
      return ret_val;
    }



    ds_3gpp_flow_ev_queue[flow_ev].flow_ev_cb_data_ptr[cb_cnt] = 
                                                                flow_ev_cb_p;

    DS_3GPP_MSG1_MED("Iface Ev cb data allocated at %d",cb_cnt);
    ret_val = cb_cnt;
    break;
  }

  if(cb_cnt >= DS_3GPP_FLOW_MAX_CB_CNT)
  {
    DS_3GPP_MSG1_ERROR("All entries are allocated:%d",cb_cnt);
  }

  return ret_val;

}

/*===========================================================================
FUNCTION DS_3GPP_FLOW_IS_EVENT_REG_DEREG

DESCRIPTION 
    This function will determine if the flow_ev_cb_hdlr will need to
    subscribe events with the FRAMEWORK. If an flow event has already been
    subscribed then there is no need to subscribe again

 
PARAMETERS
    ps_iface_event_enum_type    - flow event on which registration needs to happen
    *cb_fptr;                  - callback function
    ds_3gpp_flow_ev_func      - subscribe or unsubscribe
 
DEPENDENCIES
    None.

RETURN VALUE 
    boolean        FALSE       if unsuccessful
                               otherwise
  
   
SIDE EFFECTS 
  None.
===========================================================================*/
static boolean ds_3gpp_flow_is_event_reg_dereg
(
  ps_iface_event_enum_type          *flow_ev_p,
  uint8                             flow_ev_cnt,
  ds_3gpp_flow_ev_func              flow_ev_func
)
{
  boolean ret_val = FALSE;
  uint8   flow_tmp_ev_cnt = 0;
  ds_3gpp_flow_ev_enum_type ds_flow_ev = DS_3GPP_FLOW_MAX_EV;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  for(flow_tmp_ev_cnt = 0;flow_tmp_ev_cnt < flow_ev_cnt; flow_tmp_ev_cnt++)
  {
    if(flow_ev_p == NULL)
    {
      DS_3GPP_MSG0_HIGH("FLOW_EV_P is NULL");
      return ret_val;
    }

    ds_flow_ev = ds_3gpp_map_flow_ev_to_ds(*flow_ev_p);

    if(ds_flow_ev >= DS_3GPP_MAX_FLOW_EV_CNT)
    {
      DS_3GPP_MSG1_ERROR("flow_ev:%d passed is INVALID",ds_flow_ev);
      return ret_val;
    }

    /*--------------------------------------------------------------------- 
      For subscription -- Only if the active cb count is 1 does DS need to
      register with the framework else there is no need
      To unsubscribe - Only if the active cb count is 0 does DS need to
      deregister with the Framework
      ---------------------------------------------------------------------*/

    if(((ds_3gpp_flow_ev_queue[ds_flow_ev].active_cb_cnt == 1) && 
        (flow_ev_func == DS_3GPP_FLOW_EV_SUBSCRIBE)) ||
        ((ds_3gpp_flow_ev_queue[ds_flow_ev].active_cb_cnt == 0) && 
        (flow_ev_func == DS_3GPP_FLOW_EV_UNSUBSCRIBE)))
    {
      DS_3GPP_MSG1_HIGH("Iface Event subscription/unsubscription needed:%d",
                        flow_ev_func);

      ret_val = TRUE;
      break;
    }

    flow_ev_p++;
  }

  return ret_val;
}

/*===========================================================================
FUNCTION DS_3GPP_FLOW_BUILD_EV_REG_DEREG_LIST

DESCRIPTION 
    This function will build the flow events that need to be subscribed
    with PS framework

 
PARAMETERS
    ps_iface_event_enum_type       - flow event on which registration needs to happen
    *cb_fptr;                      - callback function
    ds_3gpp_flow_ev_func           - SUBScribe or UNSUBSCRIBE
    ds_3gpp_flow_reg_dereg_list_type   flow_reg_dereg_list
 
DEPENDENCIES
    None.

RETURN VALUE 
    boolean        -1       if unsuccessful
    number of events added  otherwise
  
   
SIDE EFFECTS 
  None.
===========================================================================*/
static void ds_3gpp_flow_build_ev_reg_dereg_list
(
  ps_iface_event_enum_type          *flow_ev_p,
  uint8                              flow_ev_cnt,
  ds_3gpp_flow_ev_func               flow_ev_func,
  ds_3gpp_flow_reg_dereg_list_type  *flow_reg_dereg_list

)
{
  int     num_events  = 0;
  uint8   flow_tmp_ev_cnt = 0;
  ds_3gpp_flow_ev_enum_type ds_flow_ev = DS_3GPP_FLOW_MAX_EV;
 /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  if( flow_ev_cnt > DS_3GPP_FLOW_MAX_EV)
  {
    DS_3GPP_MSG1_ERROR("Num flow events is invalid:%d",flow_ev_cnt);
    return;
  }


  for(flow_tmp_ev_cnt = 0;flow_tmp_ev_cnt < flow_ev_cnt; flow_tmp_ev_cnt++)
  {
    if(flow_ev_p == NULL)
    {
      DS_3GPP_MSG0_HIGH("FLOW_EV_P is NULL");
      return;
    }

    ds_flow_ev = ds_3gpp_map_flow_ev_to_ds(*flow_ev_p);

    if(ds_flow_ev >= DS_3GPP_MAX_FLOW_EV_CNT)
    {
      DS_3GPP_MSG1_ERROR("flow_ev:%d passed is INVALID",ds_flow_ev);
      return;
    }

    if(((ds_3gpp_flow_ev_queue[ds_flow_ev].active_cb_cnt == 1) &&
        (flow_ev_func == DS_3GPP_FLOW_EV_SUBSCRIBE)) ||
        ((ds_3gpp_flow_ev_queue[ds_flow_ev].active_cb_cnt == 0) && 
        (flow_ev_func == DS_3GPP_FLOW_EV_UNSUBSCRIBE)))
    {
	    flow_reg_dereg_list->ds_3gpp_flow_ev_list_p[num_events] = *flow_ev_p;
        num_events++;
  	  }
	flow_ev_p++;
  }

  DS_3GPP_MSG1_HIGH("Number of events returned is %d",num_events);
  flow_reg_dereg_list->num_flow_ev = num_events;
  return;

}

/*===========================================================================
FUNCTION  DS_3GPP_FLOW_DISPATCH_EVENTS

DESCRIPTION
  This function gets called when any flow event is registered
  Check if there is a need to dispatch flow events.
  
PARAMETERS 
  ps_flow_event_enum_type            *flow_ev_p,
  uint8                               flow_ev_cnt,
  ps_flow_type                       *flow_ptr

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
void ds_3gpp_flow_dispatch_events
(
  ps_iface_event_enum_type            *flow_ev_p,
  uint8                                flow_ev_cnt,
  ps_flow_type                        *flow_ptr
)
{
  uint8 temp_ev_cnt = 0;
  ps_iface_event_info_u_type event_info;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

  /*----------------------------------------------------------------------- 
    For each of the phys_link subscriptions received check if CB's
    have to be dispatched. Since MH registers with PS framework as NULL,
    If a phys_linkis already in that state we will need to dispatch the event
    since Framework will not do this for NULL subscriptions.
    -----------------------------------------------------------------------*/

  if(flow_ptr == NULL)
  {
    DS_3GPP_MSG0_ERROR("flow ptr is NULL");
    return;
  }

  memset(&event_info,0,sizeof(ps_iface_event_info_u_type));

  for(temp_ev_cnt = 0; temp_ev_cnt < flow_ev_cnt; temp_ev_cnt++)
  {
    if(flow_ev_p == NULL)
    {
      DS_3GPP_MSG0_ERROR("flow ptr is NULL");
      return;
    }

    switch(*flow_ev_p)
    {

      case FLOW_NULL_EV:
        if (PS_FLOWI_GET_STATE(flow_ptr) == FLOW_NULL)
        {
            ds_3gpp_flow_ev_cback(flow_ptr,*flow_ev_p,
                                     event_info,NULL);
        }
        break;


      case FLOW_ACTIVATED_EV:
        if (PS_FLOWI_GET_STATE(flow_ptr) == FLOW_ACTIVATED)
        {
           ds_3gpp_flow_ev_cback(flow_ptr,*flow_ev_p,
                                     event_info,NULL);
        }
        break;

      default:
        DS_3GPP_MSG0_ERROR("Invalid flow");
        break;
    }
    flow_ev_p++;
  }
}



/*===========================================================================
FUNCTION DS_3GPP_FLOW_REG_EV

DESCRIPTION 
    This function is to be called when MH modules need to subscribe for a
    3gpp flow event

 
PARAMETERS
    ps_iface_event_enum_type    - flow event on which registration needs to happen
    *cb_fptr;                - callback function
    *flow_ptr;              - flow_ptr
    *user_data_ptr;          - user_data_ptr
 
DEPENDENCIES
    None.

RETURN VALUE 
    boolean        FALSE       if unsuccessful
                               otherwise
  
   
SIDE EFFECTS 
  None.
===========================================================================*/

boolean ds_3gpp_flow_reg_ev
(
  ps_iface_event_enum_type         *flow_ev_p,
  uint8                             flow_ev_cnt,
  ps_flow_event_cback_type          cb_fptr,
  ps_flow_type                     *flow_ptr,
  void                             *user_data_ptr
)
{
  boolean                          ret_val = FALSE;
  int8                             cb_cnt  = -1;
  ds_3gpp_flow_ev_enum_type        ds_flow_ev = DS_3GPP_MAX_FLOW_EV_CNT;
  uint8                            flow_tmp_ev_cnt = 0;
  ps_iface_event_enum_type         *flow_tmp_ev_p  = flow_ev_p;
  ds_3gpp_flow_reg_dereg_list_type flow_reg_dereg_list;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

 /*------------------------------------------------------------------------ 
   Check for cb_ptr here, If the cb_ptr is NULL return
  ------------------------------------------------------------------------*/
  if(cb_fptr == NULL)
  {
    DS_3GPP_MSG0_ERROR("CB function ptr is NULL");
    return ret_val;
  }

  /*----------------------------------------------------------------------- 
    For every event passed, allocate cb_data and store the corresponding
    flow_ptr, user_data and cb_ptr
  ------------------------------------------------------------------------*/

  for(flow_tmp_ev_cnt = 0;flow_tmp_ev_cnt < flow_ev_cnt; flow_tmp_ev_cnt++)
  {

    if(flow_tmp_ev_p == NULL)
    {
      DS_3GPP_MSG0_ERROR("Iface_ev ptr is INVALID");
      return ret_val;
    }

    ds_flow_ev = ds_3gpp_map_flow_ev_to_ds(*flow_tmp_ev_p);

    if(ds_flow_ev >= DS_3GPP_MAX_FLOW_EV_CNT)
    {
      DS_3GPP_MSG1_ERROR("flow_ev:%d passed is INVALID",ds_flow_ev);
      return ret_val;
    }

    /*------------------------------------------------------------------------ 
      Check if the ev is already registered with the same cb_fn and
      user_data_ptr. If it is then there is no need to register again
    ------------------------------------------------------------------------*/
  	for(cb_cnt = 0;cb_cnt < DS_3GPP_FLOW_MAX_CB_CNT;cb_cnt++)
	  {
      if(( ds_3gpp_flow_ev_queue[ds_flow_ev].
           flow_ev_cb_data_ptr[cb_cnt] != NULL)
           && (ds_3gpp_flow_ev_queue[ds_flow_ev].
           flow_ev_cb_data_ptr[cb_cnt]->cb_fptr == cb_fptr) && 
          (ds_3gpp_flow_ev_queue[ds_flow_ev].flow_ev_cb_data_ptr
          [cb_cnt]->user_data_ptr == user_data_ptr) &&
          (ds_3gpp_flow_ev_queue[ds_flow_ev].flow_ev_cb_data_ptr
          [cb_cnt]->flow_ptr == flow_ptr) &&
          flow_ptr != NULL)
      {
        DS_3GPP_MSG0_HIGH("Reg ev with the same userdata ptr already present:");
        return ret_val;
      }
    } 

    /*------------------------------------------------------------------------- 
      Allocate the cb data for the flow event received
    -------------------------------------------------------------------------*/
    cb_cnt = ds_3gpp_flow_alloc_ev_cb_data(ds_flow_ev);

    if(cb_cnt < 0)
    {
      DS_3GPP_MSG0_ERROR("Failure to allocate callback memory");
      return ret_val;
    }

    /*-------------------------------------------------------------------------- 
      CB data has been allocated at cb_cnt index
      Now save the contents received and increment the cb_data count
    --------------------------------------------------------------------------*/
    ds_3gpp_flow_ev_queue[ds_flow_ev].flow_ev_cb_data_ptr[cb_cnt]->cb_fptr
                                                                       = cb_fptr;

    ds_3gpp_flow_ev_queue[ds_flow_ev].flow_ev_cb_data_ptr[cb_cnt]->flow_ptr 
                                                                     = flow_ptr;

    ds_3gpp_flow_ev_queue[ds_flow_ev].flow_ev_cb_data_ptr[cb_cnt]->user_data_ptr
                                                                  = user_data_ptr;

    ds_3gpp_flow_ev_queue[ds_flow_ev].active_cb_cnt++;

    flow_tmp_ev_p++;

  }

  /*---------------------------------------------------------------------------- 
    Only if flow_tmp_ev_cnt is the same as flow_ev_cnt the for loop has been
    successful, If registration is successful then check if DS needs to subscribe
    to flow events with PS framework
    ----------------------------------------------------------------------------*/

  DS_3GPP_MSG0_HIGH("Registration of all events successful");
  ret_val = TRUE;

  if(ds_3gpp_flow_is_event_reg_dereg(flow_ev_p,
                                     flow_ev_cnt,
                                     DS_3GPP_FLOW_EV_SUBSCRIBE))
  {
    ds_3gpp_flow_build_ev_reg_dereg_list(flow_ev_p,
                                         flow_ev_cnt,
                                         DS_3GPP_FLOW_EV_SUBSCRIBE,
                                         &flow_reg_dereg_list);
   /*---------------------------------------------------------------------------- 
     Deallocate the flow_ev_list always after registration/deregistration with the
     PS
    -----------------------------------------------------------------------------*/
    if((flow_reg_dereg_list.num_flow_ev > 0) && 
	   (flow_reg_dereg_list.num_flow_ev <= DS_3GPP_MAX_FLOW_EV_CNT) && 
	   (ds_3gpp_flow_ev_buf_ptr != NULL))
    {
       if(ps_flow_subscribe_event_list(ds_3gpp_flow_ev_buf_ptr,
                                       flow_reg_dereg_list.ds_3gpp_flow_ev_list_p,
                                       flow_reg_dereg_list.num_flow_ev) == -1)
       {
         DS_3GPP_MSG0_HIGH("Flow deReg event unsuccessful");
		 ret_val = FALSE;
       }
    }
	else
	{
	  ret_val = FALSE;
	}
   }

   if(ret_val)
   {

      ds_3gpp_flow_dispatch_events(flow_ev_p,
                                   flow_ev_cnt,
                                   flow_ptr);
   }
  
   return ret_val;

}

/*===========================================================================
FUNCTION DS_3GPP_FLOW_DEREG_EV

DESCRIPTION 
    This function is to be called when MH modules need to unsubscribe for a
    3gpp flow event

 
PARAMETERS
    ds_3gpp_flow_ev_list  - flow event on which registration needs to happen
    uint8                   flow_ev_cnt
    cb_fptr;                callback function
    ps_flow_type            *flow_ptr,
 
DEPENDENCIES
    None.

RETURN VALUE 
    boolean        FALSE       if unsuccessful
                               otherwise
  
   
SIDE EFFECTS 
  None.
===========================================================================*/

boolean ds_3gpp_flow_dereg_ev
(
  ps_iface_event_enum_type        *flow_ev_p,
  uint8                            flow_ev_cnt,
  ps_flow_event_cback_type         cb_fptr,
  ps_flow_type                     *flow_ptr
)
{
  boolean                            ret_val = FALSE;
  uint8                              cb_cnt  = 0;
  ps_flow_event_cback_type           cb_tmp_fptr = NULL;
  uint8                              flow_tmp_ev_cnt = 0; 
  ps_iface_event_enum_type          *flow_tmp_ev_p = flow_ev_p;
  ds_3gpp_flow_ev_enum_type          ds_flow_ev = DS_3GPP_FLOW_MAX_EV;
  ds_3gpp_flow_reg_dereg_list_type   flow_reg_dereg_list;
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /*------------------------------------------------------------------------- 
    Loop through to deregister all the required events
   -------------------------------------------------------------------------*/
  for(flow_tmp_ev_cnt = 0;flow_tmp_ev_cnt < flow_ev_cnt;flow_tmp_ev_cnt++)
  {

    if(flow_tmp_ev_p == NULL)
    {
      DS_3GPP_MSG0_ERROR("Iface ev ptr is NULL");
      return ret_val;
    }

    ds_flow_ev = ds_3gpp_map_flow_ev_to_ds(*flow_tmp_ev_p);

    if(ds_flow_ev >= DS_3GPP_MAX_FLOW_EV_CNT)
    {
      DS_3GPP_MSG1_ERROR("flow_ev:%d passed is INVALID",ds_flow_ev);
      return ret_val;
    }

    for(cb_cnt = 0;cb_cnt < DS_3GPP_FLOW_MAX_CB_CNT; cb_cnt++)
    {
      if(ds_3gpp_flow_ev_queue[ds_flow_ev].flow_ev_cb_data_ptr[cb_cnt] == NULL)
      {
        continue;
      }

      cb_tmp_fptr = ds_3gpp_flow_ev_queue[ds_flow_ev].flow_ev_cb_data_ptr[cb_cnt]->cb_fptr;

      if((cb_tmp_fptr == NULL) || (cb_tmp_fptr != cb_fptr)) 
      {
        continue;
      }

      /*--------------------------------------------------------------------- 
        Now we have a match by function ptr.
        If flow_ptr is not NULL, then perform flow comparision too to find
        the match
        If flow_ptr is NULL then go ahead and delete the entry.
      --------------------------------------------------------------------*/
      if((flow_ptr != NULL) && 
         (flow_ptr != ds_3gpp_flow_ev_queue[ds_flow_ev].
          flow_ev_cb_data_ptr[cb_cnt]->flow_ptr))
      {
        continue;
      }

      /*---------------------------------------------------------------------- 
        Match found, Now deallocate the cb_data and reduce the cb_cnt
       ----------------------------------------------------------------------*/
      modem_mem_free((void *)ds_3gpp_flow_ev_queue[ds_flow_ev].
                     flow_ev_cb_data_ptr[cb_cnt],
                     MODEM_MEM_CLIENT_DATA);

      ds_3gpp_flow_ev_queue[ds_flow_ev].flow_ev_cb_data_ptr[cb_cnt] = NULL;

      ds_3gpp_flow_ev_queue[ds_flow_ev].active_cb_cnt--;

      DS_3GPP_MSG2_HIGH("Active callback count for flow_ev:%d is %d",ds_flow_ev,
                        ds_3gpp_flow_ev_queue[ds_flow_ev].active_cb_cnt);

      break;

    }

    if(cb_cnt >= DS_3GPP_FLOW_MAX_CB_CNT)
    {
      DS_3GPP_MSG0_ERROR("Deregistration failed");
    }
    else
    {
      ret_val = TRUE;
    }

    flow_tmp_ev_p++;

  }

  if(ret_val != TRUE)
  {
    DS_3GPP_MSG0_ERROR("Deregistration failure");
    return ret_val;
  }

  if(ds_3gpp_flow_is_event_reg_dereg(flow_ev_p,
                                     flow_ev_cnt,
                                     DS_3GPP_FLOW_EV_UNSUBSCRIBE))
  {
    ds_3gpp_flow_build_ev_reg_dereg_list(flow_ev_p,
                                         flow_ev_cnt,
                                         DS_3GPP_FLOW_EV_UNSUBSCRIBE,
                                         &flow_reg_dereg_list);
  /*---------------------------------------------------------------------------- 
    Only if flow_tmp_ev_cnt is the same as flow_ev_cnt the for loop has been
    successful, If deregistration is successful then check if DS needs to unsubscribe
    to flow events with PS framework
    ----------------------------------------------------------------------------*/
    if((flow_reg_dereg_list.num_flow_ev > 0) && 
	   (flow_reg_dereg_list.num_flow_ev <= DS_3GPP_MAX_FLOW_EV_CNT) && 
	   (ds_3gpp_flow_ev_buf_ptr != NULL))
    {
      if(ps_flow_unsubscribe_event_list(ds_3gpp_flow_ev_buf_ptr,
                                        flow_reg_dereg_list.ds_3gpp_flow_ev_list_p,
                                        flow_reg_dereg_list.num_flow_ev) == -1)
      {
        DS_3GPP_MSG0_HIGH("Flow deReg event unsuccessful");
        ret_val = FALSE;
      }
      else
      {
        DS_3GPP_MSG0_HIGH("Flow deReg event successful");
        ret_val = TRUE;
      }
    }
  }

  return ret_val;

}

/*===========================================================================
FUNCTION  DS_3GPP_MAP_FLOW_EV_TO_DS

DESCRIPTION
  This function converts PS flow event to the registered PS flow events
  
PARAMETERS 
 
DEPENDENCIES
  None

RETURN VALUE
  ds_3gpp_flow_ev_list 

SIDE EFFECTS
  None

===========================================================================*/
static ds_3gpp_flow_ev_enum_type ds_3gpp_map_flow_ev_to_ds
(
  ps_iface_event_enum_type   ps_event
)
{
  ds_3gpp_flow_ev_enum_type ds_3gpp_flow_ev = DS_3GPP_FLOW_MAX_EV;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  switch(ps_event)
  {
    case FLOW_ACTIVATED_EV:
      ds_3gpp_flow_ev = DS_3GPP_FLOW_ACTIVATED_EV;
      break;

    case FLOW_MODIFY_ACCEPTED_EV:
      ds_3gpp_flow_ev = DS_3GPP_FLOW_MODIFY_ACCEPTED_EV;
      break;

    case FLOW_MODIFY_REJECTED_EV:
      ds_3gpp_flow_ev = DS_3GPP_FLOW_MODIFY_REJECTED_EV;
      break;

    case FLOW_NULL_EV:
      ds_3gpp_flow_ev = DS_3GPP_FLOW_NULL_EV;
      break;

    default:
      DS_3GPP_MSG0_HIGH("Invalid flow ev");
  }

  return ds_3gpp_flow_ev;

}

/*===========================================================================
FUNCTION  DS_3GPP_FLOW_EV_CBACK

DESCRIPTION
  This function gets called when any flow event is posted from PS framework
  
PARAMETERS 
  this_flow_ptr:   Iface on which up event was posted
  event:            Event that caused the cback to be invoked
  event_info:       Event info posted by PS
  user_data_ptr:    Data to be passed to the cback function

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/

void ds_3gpp_flow_ev_cback
(
  ps_flow_type                            *this_flow_ptr,
  ps_iface_event_enum_type                 ps_event,
  ps_iface_event_info_u_type               event_info,
  void                                    *user_data_ptr
)
{
  uint8                           cb_cnt = 0;
  ds_3gpp_flow_ev_enum_type       ds_3gpp_flow_ev = DS_3GPP_FLOW_MAX_EV;
  uint8                           active_cb_cnt    = 0;
  ds_3gpp_flow_ev_cb_data_type   *flow_ev_cb_data_p = NULL;
  uint8                           serviced_cb_cnt = 0;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  if(this_flow_ptr == NULL)
  {
    DS_3GPP_MSG0_ERROR("Event received on a NULL Ptr");
    return;
  }

  ds_3gpp_flow_ev = ds_3gpp_map_flow_ev_to_ds(ps_event);

  if(ds_3gpp_flow_ev == DS_3GPP_FLOW_MAX_EV)
  {
    DS_3GPP_MSG0_ERROR("Invalid 3gpp Event received");
    return;
  }

  active_cb_cnt = ds_3gpp_flow_ev_queue[ds_3gpp_flow_ev].active_cb_cnt;
  /*------------------------------------------------------------------------- 
    For all the call backs registered on the particular flow_ev, Check if
    the flow match occurs. If the flow on which the flow_ev is received
    is the same as the registered flow 
    The flow on which the event is received is copared with the
    flow cb ev tbl, If a match occurs (Actual match of NULL flow match),
    respective cb function ptrs are dispatched
    ------------------------------------------------------------------------*/
  for(cb_cnt = 0; cb_cnt < DS_3GPP_MAX_FLOW_EV_CNT; cb_cnt++)
  {
    flow_ev_cb_data_p = 
      ds_3gpp_flow_ev_queue[ds_3gpp_flow_ev].flow_ev_cb_data_ptr[cb_cnt];

    if((flow_ev_cb_data_p != NULL) && 
       ((flow_ev_cb_data_p->flow_ptr == this_flow_ptr) || 
       (flow_ev_cb_data_p->flow_ptr == NULL))&&
       (flow_ev_cb_data_p->cb_fptr != NULL))
    {
      DS_3GPP_MSG2_MED("Match occured on flow_p:0x%x for event:%d",
                       this_flow_ptr,ps_event);

      flow_ev_cb_data_p->cb_fptr(this_flow_ptr,ps_event,event_info,
                                  flow_ev_cb_data_p->user_data_ptr);
								  
	  serviced_cb_cnt++;
	  
	  if(serviced_cb_cnt >= active_cb_cnt)
	  {
   	    DS_3GPP_MSG1_HIGH("Done serviceing all callbacks:%d",serviced_cb_cnt);
	    break;
	  }

    }
	
  }
  return;
}
