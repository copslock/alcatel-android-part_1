#ifndef MSMHWIO_H
#define MSMHWIO_H

#define BAM_MEMORY_MGR 1

/* ----------------------------------------------------------------------- 
**                            COMMON DEFINES
** ----------------------------------------------------------------------- */

#define __STR2__(x) #x
#define __STR1__(x) __STR2__(x)
#define __LOC__ __FILE__ "("__STR1__(__LINE__)") : Reminder: "

#define WARN(msg)	(__LOC__ __FUNCTION__ ": " msg) 

/*
 * HWIO_BASE_PTR
 *
 * This macro maps a base name to the pointer to access the base.  I.e.
 * HWIO_BASE_PTR(MYCODE) -> MYCODE_BASE_PTR
 * This is generally just used internally.
 *
 */
#define HWIO_BASE_PTR(base) base##_BASE_PTR


/*
 * DECLARE_HWIO_BASE_PTR
 *
 * This macro will declare a HWIO base pointer data structure.  The pointer
 * will always be declared as a weak symbol so multiple declarations will
 * resolve correctly to the same data at link-time.
 * DECLARE_HWIO_BASE_PTR(MYCODE) -> uint8 *MYCODE_BASE_PTR
 */
#ifdef __ARMCC_VERSION
  #define DECLARE_HWIO_BASE_PTR(base) __weak uint8 *HWIO_BASE_PTR(base)
#else
  #define DECLARE_HWIO_BASE_PTR(base) uint8 *HWIO_BASE_PTR(base)
#endif


/*
 * HWIO macros for input and output.
 */
#define HWIO_IN(hwiosym)                                 __msmhwio_in(hwiosym)
#define HWIO_INI(hwiosym, index)                         __msmhwio_ini(hwiosym, index)
#define HWIO_INI2(hwiosym, index1, index2)               __msmhwio_ini2(hwiosym, index1, index2)
#define HWIO_INM(hwiosym, mask)                          __msmhwio_inm(hwiosym, mask)
#define HWIO_INMI(hwiosym, index, mask)                  __msmhwio_inmi(hwiosym, index, mask)
#define HWIO_INMI2(hwiosym, index1, index2, mask)        __msmhwio_inmi2(hwiosym, index1, index2, mask)
#define HWIO_OUT(hwiosym, val)                           __msmhwio_out(hwiosym, val)
#define HWIO_OUTI(hwiosym, index, val)                   __msmhwio_outi(hwiosym, index, val)
#define HWIO_OUTI2(hwiosym, index1, index2, val)         __msmhwio_outi2(hwiosym, index1, index2, val)
#define HWIO_OUTM(hwiosym, mask, val)                    __msmhwio_outm(hwiosym, mask, val)
#define HWIO_OUTMI(hwiosym, index, mask, val)            __msmhwio_outmi(hwiosym, index, mask, val)
#define HWIO_OUTMI2(hwiosym, index1, index2, mask, val)  __msmhwio_outmi2(hwiosym, index1, index2, mask, val)

/*
 * HWIO macros for masks and shifts.
 */
#define HWIO_RMSK(hwiosym)                               __msmhwio_rmsk(hwiosym)
#define HWIO_RMSKI(hwiosym, index)                       __msmhwio_rmski(hwiosym, index)
#define HWIO_SHFT(hwio_regsym, hwio_fldsym)              __msmhwio_shft(hwio_regsym, hwio_fldsym)
#define HWIO_FMSK(hwio_regsym, hwio_fldsym)              __msmhwio_fmsk(hwio_regsym, hwio_fldsym)

/*
 * HWIO macros for addresses.
 */
#define HWIO_ADDR(hwiosym)                               __msmhwio_addr(hwiosym)
#define HWIO_ADDRI(hwiosym, index)                       __msmhwio_addri(hwiosym, index)
#define HWIO_ADDRI2(hwiosym, index1, index2)             __msmhwio_addri2(hwiosym, index1, index2)
#define HWIO_PHYS(hwiosym)                               __msmhwio_phys(hwiosym)
#define HWIO_PHYSI(hwiosym, index)                       __msmhwio_physi(hwiosym, index)
#define HWIO_PHYSI2(hwiosym, index1, index2)             __msmhwio_physi2(hwiosym, index1, index2)

/*
 * HWIO shadow register access
 */
#define HWIO_SHDW(hwiosym)                               __msmhwio_shdw(hwiosym)
#define HWIO_SHDWI(hwiosym, index)                       __msmhwio_shdwi(hwiosym, index)

/*
 * Combination helper macros.
 */
#define HWIO_INF(io, field)                         (HWIO_INM(io, HWIO_FMSK(io, field)) >> HWIO_SHFT(io, field))
#define HWIO_INFI(io, index, field)                 (HWIO_INMI(io, index, HWIO_FMSK(io, field)) >> HWIO_SHFT(io, field))
#define HWIO_INFI2(io, index1, index2, field)       (HWIO_INMI2(io, index1, index2, HWIO_FMSK(io, field)) >> HWIO_SHFT(io, field))
#define HWIO_OUTF(io, field, val)                   HWIO_OUTM(io, HWIO_FMSK(io, field), (uint32)(val) << HWIO_SHFT(io, field))
#define HWIO_OUTFI(io, index, field, val)           HWIO_OUTMI(io, index, HWIO_FMSK(io, field), (uint32)(val) << HWIO_SHFT(io, field))
#define HWIO_OUTFI2(io, index1, index2, field, val) HWIO_OUTMI2(io, index1, index2, HWIO_FMSK(io, field), (uint32)(val) << HWIO_SHFT(io, field))
#define HWIO_FVAL(io, field, val)                   (((uint32)(val) << HWIO_SHFT(io, field)) & HWIO_FMSK(io, field))


/*
 * Map to final symbols.  This remapping is done to allow register 
 * redefinitions.  If we just define HWIO_IN(xreg) as HWIO_##xreg##_IN
 * then remappings like "#define xreg xregnew" do not work as expected.
 */
#define __msmhwio_in(hwiosym)                            HWIO_##hwiosym##_IN
#define __msmhwio_ini(hwiosym, index)                    HWIO_##hwiosym##_INI(index)
#define __msmhwio_ini2(hwiosym, index1, index2)          HWIO_##hwiosym##_INI2(index1, index2)
#define __msmhwio_inm(hwiosym, mask)                     HWIO_##hwiosym##_INM(mask)
#define __msmhwio_inmi(hwiosym, index, mask)             HWIO_##hwiosym##_INMI(index, mask)
#define __msmhwio_inmi2(hwiosym, index1, index2, mask)   HWIO_##hwiosym##_INMI2(index1, index2, mask)
#define __msmhwio_out(hwiosym, val)                      HWIO_##hwiosym##_OUT(val)
#define __msmhwio_outi(hwiosym, index, val)              HWIO_##hwiosym##_OUTI(index,val)
#define __msmhwio_outi2(hwiosym, index1, index2, val)    HWIO_##hwiosym##_OUTI2(index1, index2, val)
#define __msmhwio_outm(hwiosym, mask, val)               HWIO_##hwiosym##_OUTM(mask, val)
#define __msmhwio_outmi(hwiosym, index, mask, val)       HWIO_##hwiosym##_OUTMI(index, mask, val)
#define __msmhwio_outmi2(hwiosym, idx1, idx2, mask, val) HWIO_##hwiosym##_OUTMI2(idx1, idx2, mask, val)
#define __msmhwio_addr(hwiosym)                          HWIO_##hwiosym##_ADDR
#define __msmhwio_addri(hwiosym, index)                  HWIO_##hwiosym##_ADDR(index)
#define __msmhwio_addri2(hwiosym, idx1, idx2)            HWIO_##hwiosym##_ADDR(idx1, idx2)
#define __msmhwio_phys(hwiosym)                          HWIO_##hwiosym##_PHYS
#define __msmhwio_physi(hwiosym, index)                  HWIO_##hwiosym##_PHYS(index)
#define __msmhwio_physi2(hwiosym, idx1, idx2)            HWIO_##hwiosym##_PHYS(idx1, idx2)
#define __msmhwio_rmsk(hwiosym)                          HWIO_##hwiosym##_RMSK
#define __msmhwio_rmski(hwiosym, index)                  HWIO_##hwiosym##_RMSK(index)
#define __msmhwio_fmsk(hwiosym, hwiofldsym)              HWIO_##hwiosym##_##hwiofldsym##_BMSK
#define __msmhwio_rshft(hwiosym)                         HWIO_##hwiosym##_SHFT
#define __msmhwio_shft(hwiosym, hwiofldsym)              HWIO_##hwiosym##_##hwiofldsym##_SHFT
#define __msmhwio_shdw(hwiosym)                          HWIO_##hwiosym##_shadow
#define __msmhwio_shdwi(hwiosym, index)                  HWIO_##hwiosym##_SHDW(index)


/*
 * HWIO_INTLOCK
 *
 * Macro used by autogenerated code for mutual exclusion around
 * read-mask-write operations.  This is not supported in HAL
 * code but can be overridden by non-HAL code.
 */
#define HWIO_INTLOCK()
#define HWIO_INTFREE()


/*
 * Input/output port macros for memory mapped IO.
 */
#define __inp(port)         (*((volatile uint8 *) (port)))
#define __inpw(port)        (*((volatile uint16 *) (port)))
#define __inpdw(port)       (*((volatile uint32 *) (port)))
#define __outp(port, val)   (*((volatile uint8 *) (port)) = ((uint8) (val)))
#define __outpw(port, val)  (*((volatile uint16 *) (port)) = ((uint16) (val)))
#define __outpdw(port, val) (*((volatile uint32 *) (port)) = ((uint32) (val)))


#ifdef HAL_HWIO_EXTERNAL

/*
 * Replace macros with externally supplied functions.
 */
#undef  __inp
#undef  __inpw
#undef  __inpdw
#undef  __outp
#undef  __outpw
#undef  __outpdw

#define  __inp(port)          __inp_extern(port)         
#define  __inpw(port)         __inpw_extern(port)
#define  __inpdw(port)        __inpdw_extern(port)
#define  __outp(port, val)    __outp_extern(port, val)
#define  __outpw(port, val)   __outpw_extern(port, val)
#define  __outpdw(port, val)  __outpdw_extern(port, val)

extern uint8   __inp_extern      ( uint32 nAddr );
extern uint16  __inpw_extern     ( uint32 nAddr );
extern uint32  __inpdw_extern    ( uint32 nAddr );
extern void    __outp_extern     ( uint32 nAddr, uint8  nData );
extern void    __outpw_extern    ( uint32 nAddr, uint16 nData );
extern void    __outpdw_extern   ( uint32 nAddr, uint32 nData );

#endif /* HAL_HWIO_EXTERNAL */


/*
 * Base 8-bit byte accessing macros.
 */
#define in_byte(addr)               (__inp(addr))
#define in_byte_masked(addr, mask)  (__inp(addr) & (mask)) 
#define out_byte(addr, val)         __outp(addr,val)
#define out_byte_masked(io, mask, val, shadow)  \
  HWIO_INTLOCK();    \
  out_byte( io, shadow); \
  shadow = (shadow & (uint16)(~(mask))) | ((uint16)((val) & (mask))); \
  HWIO_INTFREE()
#define out_byte_masked_ns(io, mask, val, current_reg_content)  \
  out_byte( io, ((current_reg_content & (uint16)(~(mask))) | \
                ((uint16)((val) & (mask)))) )


/*
 * Base 16-bit word accessing macros.
 */
#define in_word(addr)              (__inpw(addr))
#define in_word_masked(addr, mask) (__inpw(addr) & (mask))
#define out_word(addr, val)        __outpw(addr,val)
#define out_word_masked(io, mask, val, shadow)  \
  HWIO_INTLOCK( ); \
  shadow = (shadow & (uint16)(~(mask))) |  ((uint16)((val) & (mask))); \
  out_word( io, shadow); \
  HWIO_INTFREE( )
#define out_word_masked_ns(io, mask, val, current_reg_content)  \
  out_word( io, ((current_reg_content & (uint16)(~(mask))) | \
                ((uint16)((val) & (mask)))) )


/*
 * Base 32-bit double-word accessing macros.
 */
#define in_dword(addr)              (__inpdw(addr))
#define in_dword_masked(addr, mask) (__inpdw(addr) & (mask))
#define out_dword(addr, val)        __outpdw(addr,val)
#define out_dword_masked(io, mask, val, shadow)  \
   HWIO_INTLOCK(); \
   shadow = (shadow & (uint32)(~(mask))) | ((uint32)((val) & (mask))); \
   out_dword( io, shadow); \
   HWIO_INTFREE()
#define out_dword_masked_ns(io, mask, val, current_reg_content) \
  out_dword( io, ((current_reg_content & (uint32)(~(mask))) | \
                 ((uint32)((val) & (mask)))) )

#endif /* MSMHWIO_H */



