/*!
  @file
  ipa_uc_mem_layout.h

  @brief
  This file contains memory layout for the IPA uC

*/
/*===========================================================================

  Copyright (c) 2014 QUALCOMM Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regctlated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  QUALCOMM Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of QUALCOMM Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the modctle.
Notice that changes are listed in reverse chronological order.

$Header:

when       who     what, where, why
--------   ---     ----------------------------------------------------------
05/19/14   ack      Initial version
===========================================================================*/

#ifndef IPA_UC_MEM_LAYOUT_H_
#define IPA_UC_MEM_LAYOUT_H_

#define IRAM_START_ADDR   0x0           
#define IRAM_SIZE         0x4000	/* IRAM Size: 16K */

#define DRAM_START_ADDR   0x4000
#define DRAM_SIZE         0x3E00	/* DRAM Size: 16K - (128 mailbox reg * 4 bytes) */

#define STACK_START_ADDR  0x7E00	/* Stack Top = At the top of DRAM. Aligned to 4 bytes*/
#define STACK_SIZE       -0x200		/* Stack size = 512 Bytes (grows downwards) */

#define IPA_UC_IRAM_OFFSET 0x50000
#endif /* IPA_UC_MEM_LAYOUT_H_ */
