/*
 * inline_asm.h
 *
 *  Created on: May 19, 2013
 *      Author: tbenchen
 */

#ifndef INLINE_ASM_H_
#define INLINE_ASM_H_

//This function attribute is a GNU compiler extension that is supported by the ARM compiler
#define PRAGMA_PLACE_CODE_IN_DRAM      __attribute__ ((__section__(".slow_code")))
#ifdef UC_FEATURE_POWER_COLLAPSE
#define PRAGMA_PLACE_DATA_IN_PCDATA     __attribute__ ((__section__(".slow_code")))
#endif

//Instruction intrinsics
#define ASM_WAIT_FOR_INTTERUPT         __wfi() 
#define ROR                            __ror
#define STOP_EXECUTION                 __breakpoint(0xAA) //Magic Number: 0xAA (argument must be within 0-255 for thumb)

#if 0
inline uint32_t ror(uint32_t z, uint32_t y) {
	__asm__ __volatile__
		("rors    %[result], %[result], %[rot]"
			: [result]"+r" (z)  // Rotation input/result
			: [rot]"r"     (y)  // Rotation
			: // No clobbers
		);
	return z;
}
#endif

#endif /* INLINE_ASM_H_ */
