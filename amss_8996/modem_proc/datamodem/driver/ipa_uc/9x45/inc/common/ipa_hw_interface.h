/*=========================================================================*//**
    @file  ipa_hw_interface.h

    @brief Define the IPA HW to IPA driver common interface

    @note Struct packing is not in use. These structs are aligned along 4-byte 
    boundaries, other archs (ex: 8-byte aligned) must make necessary changes 
    for compatibility.
*//****************************************************************************/
/*------------------------------------------------------------------------------
    Copyright (c) 2014 Qualcomm Technologies Incorporated.
    All Rights Reserved.
    Qualcomm Confidential and Proprietary
 
    $Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/datamodem/driver/ipa_uc/9x45/inc/common/ipa_hw_interface.h#1 $
------------------------------------------------------------------------------*/

#ifndef IPA_HW_INTERFACE_H_
#define IPA_HW_INTERFACE_H_

#define IPA_HW_INTERFACE_VERSION         0x0113            /**< A7 to HW interface version*/
#define IPA_HW_FIRST_ERROR_ADDRESS_RESET_VALUE  0x7EEDBEAF /**< Reset value for firstErrorAddress field in shared memory  */

#define IPA_HW_NUM_FEATURES                     0x8        /**< Maximum number of features that HW can support */  

#define IPA_HW_SRAM_CPU_COMM_SIZE               128         /**< 128B shared memory located in offset zero of SW Partition in IPA SRAM.*/
#define IPA_HW_SRAM_DEBUG_EVENT_ADDR            (HWIO_ADDRI(IPA_SRAM_DIRECT_ACCESS_n, 0) + IPA_HW_SRAM_CPU_COMM_SIZE) /**< Location of the structure where the debug info will be advertised */

/**
 * @brief   Values that represent the features supported in IPA HW
*/
typedef enum
{
   IPA_HW_FEATURE_COMMON                    = 0x0,    /**< Feature related to common operation of IPA HW */
   IPA_HW_FEATURE_MHI                       = 0x1,    /**< Feature related to MHI operation in IPA HW */
   IPA_HW_FEATURE_POWER_COLLAPSE            = 0x2,    /**< Feature related to IPA Power collapse */
   IPA_HW_FEATURE_WDI                       = 0x3,    /**< Feature related to WDI operation in IPA HW */
   IPA_HW_FEATURE_ZIP                       = 0x4,    /**< Feature related to CMP/DCMP operation in IPA HW */
   IPA_HW_FEATURE_MAX                       = IPA_HW_NUM_FEATURES     
}IPA_HW_FEATURES;

/**
 * @brief   Bitmask representation for the features supported in
 *          IPA HW
*/
typedef enum
{
   IPA_HW_FEATURE_BMASK_COMMON              = 1 << IPA_HW_FEATURE_COMMON,            /**< Feature related to common operation of IPA HW  */
   IPA_HW_FEATURE_BMASK_MHI                 = 1 << IPA_HW_FEATURE_MHI,               /**< Feature related to MHI operation in IPA HW  */
   IPA_HW_FEATURE_BMASK_POWER_COLLAPSE      = 1 << IPA_HW_FEATURE_POWER_COLLAPSE,    /**< Feature related to IPA Power Collapse  */
   IPA_HW_FEATURE_BMASK_WDI                 = 1 << IPA_HW_FEATURE_WDI,               /**< Feature related to WDI  */
   IPA_HW_FEATURE_BMASK_ZIP                 = 1 << IPA_HW_FEATURE_ZIP,               /**< Feature related to ZIP/DCMP operation in IPA HW  */  
}IPA_HW_FEATURE_MASK;

/**
 *  @brief   Enum value determined based on the feature it
 *           corresponds to
 *  +----------------+----------------+
 *  |    3 bits      |     5 bits     |
 *  +----------------+----------------+
 *  |   HW_FEATURE   |     OPCODE     |
 *  +----------------+----------------+
 *  
 */
#define FEATURE_ENUM_VAL(feature,opcode) ((feature << 5) | opcode)

/**
 * @brief   Values that represent the common commands from CPU 
 *          to IPA HW.
*/
typedef enum
{
   IPA_CPU_2_HW_CMD_NO_OP                     = FEATURE_ENUM_VAL(IPA_HW_FEATURE_COMMON, 0),  	 /**< No operation is required.  */
   IPA_CPU_2_HW_CMD_UPDATE_FLAGS              = FEATURE_ENUM_VAL(IPA_HW_FEATURE_COMMON, 1),    /**< Update SW flags which defines the behavior of HW . Once operation was completed HW shall respond with IPA_HW_2_CPU_RESPONSE_CMD_COMPLETED */
   IPA_CPU_2_HW_CMD_DEBUG_RUN_TEST            = FEATURE_ENUM_VAL(IPA_HW_FEATURE_COMMON, 2),    /**< Launch predefined test over HW  */
   IPA_CPU_2_HW_CMD_DEBUG_GET_INFO            = FEATURE_ENUM_VAL(IPA_HW_FEATURE_COMMON, 3),    /**< Read HW internal debug information  */
   IPA_CPU_2_HW_CMD_ERR_FATAL                 = FEATURE_ENUM_VAL(IPA_HW_FEATURE_COMMON, 4),    /**< CPU instructs HW to perform error fatal handling  */
   IPA_CPU_2_HW_CMD_CLK_GATE                  = FEATURE_ENUM_VAL(IPA_HW_FEATURE_COMMON, 5),    /**< CPU instructs HW to goto Clock Gated state  */ 
   IPA_CPU_2_HW_CMD_CLK_UNGATE                = FEATURE_ENUM_VAL(IPA_HW_FEATURE_COMMON, 6),    /**< CPU instructs HW to goto Clock Ungated state  */ 
   IPA_CPU_2_HW_CMD_MEMCPY                    = FEATURE_ENUM_VAL(IPA_HW_FEATURE_COMMON, 7),     /**< CPU instructs HW to do memcopy using QMB  */    
   IPA_CPU_2_HW_CMD_RESET_PIPE                = FEATURE_ENUM_VAL(IPA_HW_FEATURE_COMMON, 8),    /**< Command to reset a pipe - SW WA for a HW bug */
}IPA_CPU_2_HW_COMMANDS;

/**
 * @brief   Values that represent common HW responses to CPU 
 *          commands.
*/
typedef enum
{
   IPA_HW_2_CPU_RESPONSE_NO_OP                    = FEATURE_ENUM_VAL(IPA_HW_FEATURE_COMMON, 0),
   IPA_HW_2_CPU_RESPONSE_INIT_COMPLETED           = FEATURE_ENUM_VAL(IPA_HW_FEATURE_COMMON, 1),   /**< HW shall send this command once boot sequence is completed and HW is ready to serve commands from CPU */
   IPA_HW_2_CPU_RESPONSE_CMD_COMPLETED            = FEATURE_ENUM_VAL(IPA_HW_FEATURE_COMMON, 2),   /**< Response to CPU commands */
   IPA_HW_2_CPU_RESPONSE_DEBUG_GET_INFO           = FEATURE_ENUM_VAL(IPA_HW_FEATURE_COMMON, 3),  /**< Response to IPA_CPU_2_HW_CMD_DEBUG_GET_INFO command */
}IPA_HW_2_CPU_RESPONSES;

/**
 * @brief   Values that represent common HW events to be sent to
 *          CPU.
*/
typedef enum
{
   IPA_HW_2_CPU_EVENT_NO_OP     = FEATURE_ENUM_VAL(IPA_HW_FEATURE_COMMON, 0),   /**< No event present  */
   IPA_HW_2_CPU_EVENT_ERROR     = FEATURE_ENUM_VAL(IPA_HW_FEATURE_COMMON, 1),   /**< Event specify a system error is detected by the device */
   IPA_HW_2_CPU_EVENT_LOG_INFO  = FEATURE_ENUM_VAL(IPA_HW_FEATURE_COMMON, 2),   /**< Event providing logging specific information */
}IPA_HW_2_CPU_EVENTS;

/**
 * @brief   SW flags define the behavior of HW.
*/
typedef enum
{
   IPA_HW_FLAG_HALT_SYSTEM_ON_ASSERT_FAILURE          = 0x01,   /**< Halt system in case of assert failure */
   IPA_HW_FLAG_NO_REPORT_MHI_CHANNEL_ERORR            = 0x02,	/**< Channel error would be reported in the event ring only. No event to CPU */
   IPA_HW_FLAG_NO_REPORT_MHI_CHANNEL_WAKE_UP          = 0x04, 	/**< No need to report event IPA_HW_2_CPU_EVENT_MHI_WAKE_UP_REQUEST.  */
   IPA_HW_FLAG_WORK_OVER_DDR			      = 0x08, 	/**< Perform all transaction to external addresses by QMB (avoid memcpy)  */
   IPA_HW_FLAG_NO_REPORT_OOB                          = 0x10,  /**< If set do not report that the device is OOB in IN Channel */
   IPA_HW_FLAG_NO_REPORT_DB_MODE                      = 0x20,   /**< If set, do not report that the device is entering a mode where it expects
                                                                    a doorbell to be rung for OUT Channel */
   IPA_HW_FLAG_NO_START_OOB_TIMER                     = 0x40
}IPA_HW_FLAGS;

/**
 * @brief   Common error types.
*/
typedef enum
{
   IPA_HW_ERROR_NONE              = FEATURE_ENUM_VAL(IPA_HW_FEATURE_COMMON, 0),  /**< No error persists  */
   IPA_HW_INVALID_DOORBELL_ERROR  = FEATURE_ENUM_VAL(IPA_HW_FEATURE_COMMON, 1),	 /**< Invalid data read from doorbell */
   IPA_HW_DMA_ERROR 	          = FEATURE_ENUM_VAL(IPA_HW_FEATURE_COMMON, 2),  /**< Unexpected DMA error */
   IPA_HW_FATAL_SYSTEM_ERROR 	  = FEATURE_ENUM_VAL(IPA_HW_FEATURE_COMMON, 3),  /**< HW has crashed and requires reset. */
   IPA_HW_INVALID_OPCODE 	      = FEATURE_ENUM_VAL(IPA_HW_FEATURE_COMMON, 4)   /**< Invalid opcode sent */
}IPA_HW_ERRORS;

/**
 * @brief   HW warning types
*/
typedef enum
{
   IPA_HW_WARN_NONE, 	                   /**< No warning persists  */
   IPA_HW_INVALID_CPU_REQUEST_WARN,        /**< Invalid cpu command or wrong timing for the command*/
   IPA_HW_INVALID_CH_TRANSITION_WARN,	   /**< Unexpected transition in Channel state machine */
   IPA_HW_INVALID_EV_TRANSITION_WARN,	   /**< Unexpected transition in Event state machine */
   IPA_HW_INVALID_QMB_TRANSITION_WARN,	   /**< Unexpected transition in QMB state machine */
   IPA_HW_INVALID_QUEUE_OVERFLOW_WARN, 	   /**< Unexpected overflow in internal queue lead to discarding an element */
   
}IPA_HW_WARNINGS;

/**
 * @brief   Clk State change types
*/
typedef enum
{
   IPA_HW_CLK_STATE_INVAID,
   IPA_HW_CLK_GATE,           /**< CPU requests to gate further processing  */
   IPA_HW_CLK_UNGATE          /**< CPU requests to ungate and resume processing */
}IPA_HW_CLK_STATE;


/**
 * @brief   Strucuture referring to the common section in 128B shared memory located in 
 *          offset zero of SW Partition in IPA SRAM. 
 * @note    The shared memory is used for communication between IPA HW and CPU. 
 *                                                                                        
*/
typedef struct IpaHwSharedMemCommonMapping_t
{
   /*word 0 */
   uint8_t  cmdOp;          /**< CPU->HW command opcode. See IPA_CPU_2_HW_COMMANDS */
   uint8_t  reserved_01;
   uint16_t reserved_03_02;
   /*word 1 */
   uint32_t cmdParams;        /**< CPU->HW command parameter. The parameter filed can hold 32 bits of parameters (immediate parameters) and point on structure in system memory (in such case the address must be accessible for HW) */
   /*word 2 */
   uint8_t  responseOp;       /**< HW->CPU response opcode. See IPA_HW_2_CPU_RESPONSES */
   uint8_t  reserved_09;
   uint16_t reserved_0B_0A;
   /*word 3 */
   uint32_t responseParams;   /**< HW->CPU response parameter. The parameter filed can hold 32 bits of parameters (immediate parameters) and point on structure in system memory*/
   /*word 4 */
   uint8_t  eventOp;          /**< HW->CPU event opcode. See IPA_HW_2_CPU_EVENTS */
   uint8_t  reserved_11;
   uint16_t reserved_13_12;
   /*word 5 */
   uint32_t eventParams;      /**< HW->CPU event parameter. The parameter filed can hold 32 bits of parameters (immediate parameters) and point on structure in system memory*/
   /*word 6 */
   uint32_t reserved_1B_18;
   /*word 7 */
   uint32_t firstErrorAddress;  /**< Contains the address of first error-source on SNOC */
   /*word 8 */
   uint8_t  hwState;            /**< State of HW. The state carries information regarding the error type. See IPA_HW_MHI_ERRORS */
   uint8_t  warningCounter;     /**< The warnings counter. The counter carries information regarding non fatal errors in HW */
   uint16_t reserved_23_22;
   /*word 9 */
   uint16_t interfaceVersionCommon; /**< The Common interface version as reported by HW*/
   uint16_t reserved_27_26;
}IpaHwSharedMemCommonMapping_t;

/*CPU->HW Common commands*/

/**
 * @brief   Structure holding the parameters for IPA_CPU_2_HW_CMD_UPDATE_FLAGS command.
 *          Parameters are sent as 32b immediate parameters.
*/
typedef union IpaHwUpdateFlagsCmdData_t
{
   struct IpaHwUpdateFlagsCmdParams_t
   {
      uint32_t     newFlags;      /**< SW flags defined the behavior of HW. This field is expected to be used as bitmask for IPA_HW_FLAGS*/
   }params;
   uint32_t raw32b;
}IpaHwUpdateFlagsCmdData_t;


/**
 * @brief   Structure holding the parameters for 
 *          IPA_CPU_2_HW_CMD_RESET_PIPE command. Parameters are
 *          sent as 32b immediate parameters.
*/
typedef union IpaHwResetPipeCmdData_t
{
   struct IpaHwResetPipeCmdParams_t
   {
      uint8_t     pipeNum;          /**< Pipe number to be reset */
      uint8_t     direction;        /**< Pipe direction          */
      uint16_t    reserved_02_03;   /**< Reserved                */
   }params;
   uint32_t raw32b;
}IpaHwResetPipeCmdData_t;

/*HW->CPU Common responses*/

/**
 * @brief   Structure holding the parameters for IPA_HW_2_CPU_RESPONSE_CMD_COMPLETED response.
 *          Parameters are sent as 32b immediate parameters.
*/
typedef union IpaHwCpuCmdCompletedResponseData_t
{
   struct IpaHwCpuCmdCompletedResponseParams_t
   {
      uint32_t     originalCmdOp     :8;  /**< The original command opcode */
      uint32_t     status            :8;  /**< 0 for success indication, otherwise failure */
      uint32_t     reserved          :16;
   }params;
   uint32_t raw32b;
}IpaHwCpuCmdCompletedResponseData_t;

/*HW->CPU Common Events */

typedef union IpaHwErrorEventData_t
{
   struct IpaHwErrorEventParams_t
   {
      uint32_t     errorType :8;        /**< Entered when a system error is detected by the HW. Type of error is specified by IPA_HW_ERRORS*/
      uint32_t     reserved  :24;
   }params;
   uint32_t raw32b;
}IpaHwErrorEventData_t;

/******************************************************************************
               Data Types related to IPA Memcopy 
******************************************************************************/
#define IPA_HWP_DMA_IOC_FLAG            0x1  /**< Interrupt on DMA request Completion */

//#define IPA_HWP_DMA_IOC_FLAG 0x1 /**< Interrupt on DMA request Completion */

#define IPA_HWP_ASYNC_DMA_REQ_MBOX_n     14  /**< CPU writes the Write Index for the Async DMA Req fifo */

#define IPA_HWP_ASYNC_DMA_RSP_MBOX_n     15  /**< HWP writes the Read Index for the Async DMA Req fifo */

typedef struct 
{
   uint32_t destination_addr;
   uint32_t source_addr;
   uint32_t source_buffer_size;   
   uint8_t  flags;                /**< Flags indicating specific config for this DMA request */ 
   uint8_t  reserved_0; 
   uint16_t reserved_1;
} IpaHwMemCopyData_t;

/******************************************************************************
               Data Types related to debug logging 
******************************************************************************/
typedef union IpaHwFeatureInfoData_t
{
   struct IpaHwFeatureInfoParams_t
   {
      uint32_t     offset  :16;     /**< Location of a feature within the EventInfoData */
      uint32_t     size    :16;     /**< Size of the feature */
   }params;
   uint32_t raw32b;
}IpaHwFeatureInfoData_t;

/**
 * @brief   Structure holding the parameters for 
 *          statistics and config info
 * @note    Information about each feature in the featureInfo[]
 *          array is populated at predefined indices per the
 *          IPA_HW_FEATURES enum definition
*/
typedef struct IpaHwEventInfoData_t
{
   uint32_t                 baseAddrOffset;                      /**< Base Address Offset of the statistics or config structure from IPA_WRAPPER_BASE */
   IpaHwFeatureInfoData_t   featureInfo[IPA_HW_NUM_FEATURES];    /**< Location and size of each feature within the statistics or config structure */
}IpaHwEventInfoData_t;


/**
 * @brief   Structure holding the parameters for 
 *          IPA_HW_2_CPU_EVENT_LOG_INFO Event
 * @note    The offset location of this structure from 
 *          IPA_WRAPPER_BASE will be provided as Event Params
 *          for the IPA_HW_2_CPU_EVENT_LOG_INFO Event
*/
typedef struct IpaHwEventLogInfoData_t
{
   uint32_t                 featureMask;             /**< Mask indicating the features enabled in HW. Refer IPA_HW_FEATURE_MASK */
   uint32_t                 circBuffBaseAddrOffset;  /**< Base Address Offset of the Circular Event Log Buffer structure */
   IpaHwEventInfoData_t     statsInfo;               /**< Statistics related information */
   IpaHwEventInfoData_t     configInfo;              /**< Configuration related information */

}IpaHwEventLogInfoData_t;

/******************************************************************************
               Data Types related to Statistics Collection 
******************************************************************************/
/**
 * @brief   Structure holding the BAM statistics
*/
typedef struct IpaHwBamStats_t
{
   uint32_t  bamFifoFull;          /**< Number of times Bam Fifo got full - For In Ch: Good, For Out Ch: Bad */
   uint32_t  bamFifoEmpty;         /**< Number of times Bam Fifo got empty - For In Ch: Bad, For Out Ch: Good */
   uint32_t  bamFifoUsageHigh;     /**< Number of times Bam fifo usage went above 75% - For In Ch: Good, For Out Ch: Bad */
   uint32_t  bamFifoUsageLow;      /**< Number of times Bam fifo usage went below 25% - For In Ch: Bad, For Out Ch: Good */
   uint32_t  bamUtilCount;         /**< Number of times we sample the BAM Fifo utilization  */
} IpaHwBamStats_t;

/**
 * @brief   Structure holding the Ring statistics
*/
typedef struct IpaHwRingStats_t
{
   uint32_t  ringFull;             /**< Number of times Transfer Ring got full - For In Ch: Good, For Out Ch: Bad */
   uint32_t  ringEmpty;            /**< Number of times Transfer Ring got empty - For In Ch: Bad, For Out Ch: Good */
   uint32_t  ringUsageHigh;        /**< Number of times Transfer Ring usage went above 75% - For In Ch: Good, For Out Ch: Bad */
   uint32_t  ringUsageLow;         /**< Number of times Transfer Ring usage went below 25% - For In Ch: Bad, For Out Ch: Good */
   uint32_t  RingUtilCount;        /**< Number of times we sample the Transfer Ring utilization  */
} IpaHwRingStats_t;
/**
 * @brief   Structure holding the common statistics info of hw
*/
typedef struct IpaHwStatsCommonInfoData_t
{
   uint64_t hw_usage_time;    /**< hw usage time counted in IPA clock ticks */
   uint32_t NumQmbReadIrq;    /**< Number of QMB Read Interrupts */
   uint32_t NumQmbWriteIrq;   /**< Number of QMB Write Interrupts */
   uint32_t NumClkGateReq;    /**< Number of Clock Gate request from CPU */
   uint32_t NumClkUngateReq;  /**< Number of Clock Ungate request from CPU */

   uint64_t TotTimeTakenDMAReq;     /**< Total accumulative time taken for all DMA requests */
   uint64_t TotPayloadSize;         /**< Total Payload Size */
   uint32_t NumProgAsyncDMAReq;        /**< Number of Programmed Async DMA Req */
   uint32_t NumCompAsyncDMAReq;        /**< Number of Completed Async DMA Req */
   uint32_t NumRejectedCpyToBuff;      /**< Number of Rejected Attempts to copy into bounce buffer */
   uint32_t NumBuffCpyCompleted;       /**< Number of completed reads and writes into bounce buffer */
   uint32_t NumDMAReqDoorbells;        /**< Number of Doorbells received for DMA Request */
   uint32_t NumUnderUsedBounceBuff;    /**< Number of under utilized bounce buffers because of 1K QMB alignment only and not "tail end" bytes less than 120B */

   uint32_t TimeStartDMAReq;        /**< The time when the DMA Req was started */
   uint32_t TimeStopDMAReq;         /**< The time when the DMA Req has completed */
   uint32_t MaxTimeTakenDMAReq;     /**< Maximum time take for a single DMA reqest */
   
   uint32_t MaxPayloadSize;         /**< Maximum Payload Size */
   uint32_t MinPayloadSize;         /**< Minimum Payload Size */
   
   uint32_t NumQMBCompReads;        /**< When QMB Read irq fires, read IPA_UC_QMB_STATUS and count the completed reads */
   uint32_t NumQMBCompWrites;       /**< When QMB Write irq fires, read IPA_UC_QMB_STATUS and count the completed writes */
}IpaHwStatsCommonInfoData_t;


#endif /* IPA_HW_INTERFACE_H_ */
