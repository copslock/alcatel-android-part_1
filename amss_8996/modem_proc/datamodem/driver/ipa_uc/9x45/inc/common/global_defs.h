/*=========================================================================*//**
    @file  global_defs.h

    @brief Global definitions and includes
*//****************************************************************************/
/*------------------------------------------------------------------------------
    Copyright (c) 2013 Qualcomm Technologies Incorporated.
    All Rights Reserved.
    Qualcomm Confidential and Proprietary
------------------------------------------------------------------------------*/

#ifndef GLOBAL_DEFS_H_
#define GLOBAL_DEFS_H_

/**
 * @brief   Optimize configuration for debugging the firmware with JTAG connection.
*/
//#define UC_DEBUG_WITH_JTAG 

/**
 * @brief   The uC firmware version. 
*/
#define UC_FIRMWARE_VERSION 0x0139

/**
 * @brief   The maximum value for uint16_t. 
*/
#define UC_UINT16_MAX  0xFFFF

/**
 * @brief   Enable IPA EOT Coalescing 
*/
#define UC_ENABLE_EOT_COAL

/**
 * @brief   Enable UC SW WA for the AHB Arbiter issue. When 
 *          uC and other EEs access the IPA Register/SRAM space
 *          at the same time, register read from Q6/A7 returns 0
 *          even when the register has non-zero contents
*/
#define UC_SW_WA_AHB_ARBITER

/**
 * @brief   When a write is done to AXI address just as the link
 *          is going to enter L1ss, the write will get buffered
 *          and never sent out. WA is to read the PCIe Vendor ID
 *          register so the write is sent out
*/
#define UC_SW_WA_PCIE_L1SS

/**
 * @brief   Enable IPA EOT Coalescing Debug Capability
 *  
 * @note Enabling the feature adds 176 Bytes in IRAM, 4 Bytes in 
 *       Slow code and 1512 Bytes in BSS
*/
//#define UC_EOT_COAL_DEBUG

/**
 * @brief   Disable the feature to profile CPU (turning on 
 *          reduces code size)
*/
//#define UC_FEATURE_CPU_PROFILING_DISABLED

/**
 * @brief   Disable the feature to profile CPU usage in IRQ handlers that are invoked while we are in wfi (turning on 
 *          reduces code size)
*/
#define UC_FEATURE_CPU_PROFILING_IRQ_HDLR_DISABLED

/**
 * @brief   Enable PC Profiling
*/
#ifdef UC_FEATURE_CPU_PROFILING_IRQ_HDLR_DISABLED
#define UC_FEATURE_PC_PROFILING
#endif

/**
 * @brief   Enable debug logging so logging info is sent to CPU
*/
#define UC_ENABLE_DEBUG_LOGGING

/**
 * @brief   Collect timestamps about various events
*/
#define UC_FEATURE_COLLECT_TIMESTAMPS

/**
 * @brief   Enable feature to implement SW WA to reset producer 
 *          pipes
*/
#define UC_FEATURE_PIPE_RESET_WA

//header can can be includes only after defining the global definitions
#include "target_defs.h"
#include "stdint.h"
#include "comdef.h"
#include "debug.h"
#include "inline_asm.h"
#include "services.h"
#include "hal.h"
#include "msmhwioreg.h"
#include "msmhwio.h"

#endif /* GLOBAL_DEFS_H_ */
