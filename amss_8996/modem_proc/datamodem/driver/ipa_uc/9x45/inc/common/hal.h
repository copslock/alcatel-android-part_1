/*=========================================================================*//**
    @file  hal.h

    @brief IPA SWI defintions
*//****************************************************************************/
/*------------------------------------------------------------------------------
    Copyright (c) 2013 Qualcomm Technologies Incorporated.
    All Rights Reserved.
    Qualcomm Confidential and Proprietary
------------------------------------------------------------------------------*/

#ifndef HAL_H_
#define HAL_H_

#define HAL_BAM_PIPE_MODE_SYSTEM     0x20   /* BAM - System Memory mode */
#define HAL_BAM_PIPE_ENABLE          0x2    /* Enable pipe register value*/

/**
 * @brief   The number of BAM pipes in ipa
*/
#define HAL_BAM_IPA_NUM_OF_PIPES     20

#ifdef MDM9x45
#define HAL_UC_MEM_IN_SRAM_OFFSET	    0xA880 /* ToDo: Need to change this to IPA_RAM_UC_MEM_ADDR_FIRST */
#define HAL_UC_MEM_IN_SRAM_OFFSET_LAST	 0xB4FF
#else
#define HAL_UC_MEM_IN_SRAM_OFFSET	 0x3918 /* 0x723*8 -currently it does not appear in SWI*/
#endif

typedef enum
{
   HAL_QMB_DIRECTION_READ  = 0, 	/**< Read from System NOC to local memory.  */
   HAL_QMB_DIRECTION_WRITE = 1 	/**< Write to System NOC from local memory.  */
}HAL_QMB_DIRECTIONS;

/**
 * @brief   Values that represent HAL_QMB_LOCAL_ADDRESS_TYPE. 
*/
typedef enum
{
   HAL_QMB_LOCAL_ADDRESS_UC  = 0,	   /**< uC local memory.  */
   HAL_QMB_LOCAL_ADDRESS_IPA = 1 	   /**< IPA shared memory  */
}HAL_QMB_LOCAL_ADDRESS_TYPE;

/**
 * @brief   QMB posing modes 
*/
typedef enum
{
   HAL_QMB_POSTING_DATA_POSTED     = 0, //Async QMB Write comes back as soon as transacation is placed on bus (NA for READ)
   HAL_QMB_POSTING_RESP_POSTED      = 1, //Async QMB Read/Write comes back only after Slave asserts the response signal
   HAL_QMB_POSTING_DATA_COMPLETE   = 2, //Sync QMB Write comes back as soon as transacation is placed on bus (NA for READ)
   HAL_QMB_POSTING_RESP_COMPLETE    = 3 //Sync QMB Read/Write comes back only after Slave asserts the response signal
}HAL_QMB_POSTING;

/**
 * @brief   Values that represent the various execution environments.
*/
typedef enum
{
   HAL_EE_A5 = 0,
   HAL_EE_Q6 = 1,
   HAL_EE_TZ = 2, /**< Trust Zone */
   HAL_EE_UC = 3 /**< micro controller */
} HAL_EE_TYPE;

#endif /* HAL_H_ */
