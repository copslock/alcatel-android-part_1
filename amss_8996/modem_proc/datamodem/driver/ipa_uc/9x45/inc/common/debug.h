/*=========================================================================*//**
    @file  debug.h

    @brief Declares of debug infrastructures
*//****************************************************************************/
/*------------------------------------------------------------------------------
    Copyright (c) 2013 Qualcomm Technologies Incorporated.
    All Rights Reserved.
    Qualcomm Confidential and Proprietary
------------------------------------------------------------------------------*/

#ifndef DEBUG_H_
#define DEBUG_H_

#include <assert.h>
#include "nvic_drv.h"
#include "inline_asm.h"

#ifdef UC_EOT_COAL_DEBUG
typedef struct BamInfo_t
{
   uint8_t  swOfstIdx;
   uint8_t  PI;
   uint8_t  eventSMstate;
}BamInfo_t;
#endif //UC_EOT_COAL_DEBUG

//For now ignore gUserFlags and always spin in an infinite loop
//Trigger a break point and stop execution
#define DEBUG_SW_BREAK()          \
        assert_ex(FALSE);
//   if(IPA_UC_FLAG_HALT_SYSTEM_ON_ASSERT_FAILURE & gUserFlags) { while(1) {} }

        //STOP_EXECUTION;           \

void assert_ex(uint8_t expr);

#ifdef _DEBUG
#include <stdio.h>
#define DBG_PRINT printf
void flushToUart(uint32_t param);

#define DEBUG_SCHM_GOTO(errLabel) \
      goto errLabel;

#define DEBUG_SCHM_RETURN(retVal) \
      return retVal;

#define DEBUG_SCHM_GOTO_IF(cond, errLabel) \
    if ( cond ) { \
      DEBUG_SCHM_GOTO( errLabel); \
    }

#define DEBUG_SCHM_RETURN_IF(cond, retVal) \
    if ( cond ) { \
      DEBUG_SCHM_RETURN(retVal); \
    }

       

#else

#define DBG_PRINT(...)
#define DEBUG_SCHM_GOTO(errLabel)
#define DEBUG_SCHM_RETURN(retVal) 
#define DEBUG_SCHM_GOTO_IF(cond, errLabel) 
#define DEBUG_SCHM_RETURN_IF(cond, retVal)
#endif

#endif /* DEBUG_H_ */
