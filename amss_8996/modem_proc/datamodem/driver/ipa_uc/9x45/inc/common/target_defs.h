/*=========================================================================*//**
    @file  target_defs.h

    @brief Defines the target specific features
*//****************************************************************************/
/*------------------------------------------------------------------------------
    Copyright (c) 2013 Qualcomm Technologies Incorporated.
    All Rights Reserved.
    Qualcomm Confidential and Proprietary
------------------------------------------------------------------------------*/

#ifndef TARGET_DEFS_H_
#define TARGET_DEFS_H_

/**
 * @brief Features enabled based on the target
*/
#ifdef MDM9x35
   #define UC_FEATURE_MHI 

#elif MSM8994
   #define UC_FEATURE_WDI
   #define UC_FEATURE_POWER_COLLAPSE

#elif MDM9x45
   #define UC_FEATURE_XLAT
   #define UC_FEATURE_WDI
   #define UC_FEATURE_MHI
   #define UC_ENABLE_MHI_DL_UL_SYNC //Enable MHI DL UL Synchronization
   #define FEATURE_SENDOOB //Enable Send Out of Buffer Feature

   #ifndef UC_ENABLE_MHI_DL_UL_SYNC
     #define UC_FEATURE_INT_MODERATION //Enable Int Mod only if ULDL Sync is disabled
   #endif

#endif


#endif /* TARGET_DEFS_H_ */
