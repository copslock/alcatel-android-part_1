
#ifndef RFLM_DTR_TX_TYPEDEF_AG_H
#define RFLM_DTR_TX_TYPEDEF_AG_H


#ifdef __cplusplus
extern "C" {
#endif

/*
WARNING: This file is auto-generated.

Generated at:    Thu Aug 20 09:14:12 2015
Generated using: lm_autogen.exe v4.0.50
Generated from:  v4.9.3 of Bolt_TXLM_register_settings.xlsx
*/

/*=============================================================================

           T X    A U T O G E N    F I L E

GENERAL DESCRIPTION
  This file is auto-generated and it captures the modem register settings 
  configured by FW, provided by the rflm_dtr_tx.

Copyright (c) 2009, 2010, 2011, 2012, 2013, 2014, 2015 by Qualcomm Technologies, Inc.  All Rights Reserved.

$DateTime: 2016/03/28 23:07:43 $
$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/rfmodem_thor/lm/inc/rflm_dtr_tx_typedef_ag.h#1 $

=============================================================================*/

/*=============================================================================
                           REVISION HISTORY
Version    Author   Date   
         Comments   IR Number   Other POC (Comma Delimited)   Release Validation Notes   Dependencies   AG File Location   Changes for   
4.9.3   nichunw   8/19/2015   
         1. Updated TX_DIGITAL_DELAY_MICROSECS_Q16 for all states. Requested by NLIC team.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            LTE   
4.9.2   nichunw   8/18/2015   
         1. Added DTR_TXFE_SP_REFLOG_CHAINc to Intra_CA_SW_Dyamic tab and make SRIF group SW dynamic. Self test capture requires ULCA to use post CA combiner logging point.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            LTE   
4.9.1   nichunw   4/16/2015   
         1. Enabled chip/foundry-dependent settings feature. In the    MASK"" column of the SW dynamic tab    the second number is the SW passed in value    where the format of the passed in value is {chip_id[9:7]    foundry_id[6:4]    avg_error_chain1[3]    avg_error_chain0[2]   
4.9.0   nichunw   12/19/2014   
         Adding a field in AG that communicates the DAC reactivation with FW. (no spreadsheet change.)      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            FW interface   
4.8.9   nichunw   12/10/2014   
         1. Make TXFE_EVT group SW dynamic. (for UL CA)      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            LTE   
4.8.8   nichunw   12/8/2014   
         1. Added       nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            LTE   
4.8.7   nichunw   11/17/2014   
         1. Added LTE_DAC_230p4MHz_CHAINc_BW5MHz_ULCA and LTE_DAC_275p2MHz_CHAINc_BW5MHz_ULCA states for LTE intra-CA, when combining with 15/20MHz. 2. Updated intra-CA PEQ/SORU coefficients on DP_PEQ_SW_Dynamic tab. 3. Remove LTE_BW10MHz_ULCA in DP_PEQ_SW_Dynamic tab since there will be no such use case in inter-band CA where the txfe input rate is doubled for LTE10MHz.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            LTE   
4.8.6   nichunw   10/20/2014   
         1. Updated modem dependent txdac config.  2. Updated the UMTS state names on DP_PEQ_SW_Dynamic tab to UMTS_SC_DAC_230p4MHz_115p2MHz_SAWLESS and UMTS_DC_DAC_230p4MHz_115p2MHz_SAWLESS.  Both 230.4 and 115.2MHz DAC rates should use the same filter settings.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            DAC, UMTS   
4.8.5   nichunw   10/1/2014   
         1. Added AvgErr_OBit_SW_Dynamic tab. This change will prevent those two bits being overwritten to 0 during txlm configuration. The second value in    MASK"" column is the bit mask and it will AND with the pass in 4-bit value [avg_error_chain1(3)    avg_error_chain0(2)    overflow_bit_chain1(1)    overflow_bit_chain0(0)]. If the result matches with the first value in the ""FUSE   MASK"" column    that row will be selected."   
4.8.4   nichunw   9/29/2014   
         1. Added 115.2MHz states for UMTS, 1x, DO, TDS.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            UMTS/1x/DO/TDS   
4.8.3   nichunw   9/26/2014   
         1. Added LTE10_ULCA states (275.2MHz and 230.4MHz). Also added a new state config parameter 'ULCA_mode.' 2. Updated DP_PEQ_SW_Dynamic tab for all intra-band ULCA cases and LTE10_ULCA.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            LTE   
4.8.2   nichunw   9/11/2014   
         1. Added IQ_GAIN_SW_Dynamic tab for DO.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            DO   
4.8.1   nichunw   8/22/2014   
         1. Added DAC_CALIB_CFG_c (for fields, TXDAC_CALIB, ET_CAL_MODE, TX_CAL_MODE), to resume the register value after modem sleep.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            All techs   
4.8.0   nichunw   7/18/2014   
         1. Updated foundry ID register name for Thor (Tesla) on IREF_LUT tab. 2. Added a new txlm variable TXFE_input_freq_hz.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            All techs   
4.7.0   nichunw   6/10/2014   
         1. Added PEQ filters for UMTS DC (same as LTE20) on DP_PEQ_SW_Dynamic tab.       nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            UMTS   
4.6.15   nichunw   5/29/2014   
         1. Updated QFPROM register name for reading foundry ID in Bolt+      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            DAC   
4.6.14   nichunw   5/9/2014   
         1. Keep CLK_XO_GATE_CFG always on to rule out the unclock DAC register issue.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            DAC   
4.6.13   nichunw   4/7/2014   
         1. Updated the PEQ/SORU coefficients for TDS and CDMA.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            TDS/CDMA   
4.6.12   nichunw   3/12/2014   
         1. Changed PEQ/SORU settings for TDS/CDMA to the same settings as in UMTS. The previous settings in TDS/CDMA causes the RF cal failure and RF system is surveying on that.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            TDS/CDMA   
4.6.11   nichunw   2/24/2014   
         1. Added ET_split_capture_FTM_SW_Dynamic tab. 2. Removed DAC_RT_FUSE_CODE_c from the main tab since the value will only be latched during boot-up.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            All techs   
4.6.10   nichunw   2/14/2014   
         1. Remove fft points from the state name LTE_DAC_230p4MHz_CHAINc_BW10MHz. 2. Updated the PEQ/SORU settings for all techs and also DP_PEQ_SW_Dynamic tab.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            All techs   
4.6.9   nichunw   2/7/2014   
         1. Change DAC temp comp slope to Q20 format. 2. Added etdac_retrieve_rpoly_code_sw_offset to the txlm sequence. This is the constant value that should be added to rpoly cal code.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            DAC temp comp   
4.6.8   nichunw   2/5/2014   
         1. Adding temp sensor indices and modem version into the DAC temp comp table. 2. Adding UMTS_SC_DAC_275p2MHz_CHAINc_CW state on the main tab. (RFSW dependent).      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            UMTS and DAC   
4.6.7   nichunw   1/31/2014   
         1. Adding max and min temp for DAC temp comp. 2. Update ref temp to 85.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            DAC temp comp   
4.6.6   nichunw   1/30/2014   
         1. Updated the DP_PEQ_SW_dynamic tab. Move the numbers under LTE10/20 SAW states to SAWLESS states.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            LTE   
4.6.5   nichunw   1/28/2014   
         1. Added DAC temp compensation info on IREF_LUT tab. 2. On DP_PEQ_SW_Dynamic tab: (a) Corrected the SAWEQ field name errors (b) The group and subgroup names are updated to �TXFE_DRIF�. (c) The register names are changed to match msm_register settings : replace �_CHAIN0� by �CHAINc� 3. Updated txdac_cfg on DAC_cal_LM_settings tab. (only change for MSB cal in txdac)      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            All techs   
4.6.4   nichunw   1/21/2014   
         1. Enabled the generation of DP_PEQ dynamic tab. In addition to LTE saw/sawless states, DAC rate is also added into the LTE state name. 2. Requested by RF system, PEQ2/SORU are enabled by default for all tech except GSM. 3. Default PEQ2 and SORU setting are filled in for LTE10/20 and UMTS 275.2MHz (SAWless settings).      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com             UMTS/LTE/TDS/1X/DO   
4.6.3   nichunw   1/7/2014   
         1. Change DM1_SEL_MODE to 1 for UMTS/LTE/TDS/1X/DO, for the correct delay matching results in ET mode.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com             UMTS/LTE/TDS/1X/DO   
4.6.2   nichunw   12/20/2013   
         1. Move FFT_SIZE out of state params. 2. Rename LTE_DAC_275p2MHz_CHAINc_BW10MHz to remove fft size in the name. 3. Updated PowerMode_SW_Dynamic to have APT mode and adding mask write.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            All techs   
4.6.1   nichunw   12/19/2013   
         1. Updated IREF LUT tab. 2. Updated ETDAC_config according to the latest DAC team recommendation.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            All techs   
4.6.0   nichunw   12/13/2013   
         1. DP_IQ_GAIN=1139 for UMTS, the same value as in Dime. 2. Bolt+ tabs are removed. They are no longer needed. Bolt and Bolt+ will share the same msm_reg_settings tab. 3. DAC_CAL_LUT section is removed since it is only programmed once by SW during boot-up.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            UMTS   
4.5.6   nichunw   12/12/2013   
         1. Changed MODE_REF to 1 for all tech for the convenience of RFSW xPT team. 2. Updated the modem version label to contain Bolt+ labels.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            All techs   
4.5.5   nichunw   12/9/2013   
         1. Fix the DM3_rate_Q25 values for all techs. 2. Update the guideline tab for DAC power up sequence (set txdac_calib to 1 after ETDAC is powered up. It is required for iref update.)      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            All techs   
4.5.4   nichunw   12/6/2013   
         1. Set Rotator_bypass to 0 for 1x and DO and change the default IQ_gain value accordingly to compensate the rotator gain. The rotator is on by default for all techs so all states can share the same FED cluster mask without any rotator issue. 2. Changed the etdac_cfg1 value for all techs. (clean up typos in the previous release and also change bit 24 to 0 to reduce the reactivation time)      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            CDMA and ETDAC   
4.5.3   nichunw   12/3/2013   
         Change default DP_ENV_SCALE_RAMP_MODE to 1 for all non-GSM states. For non-GSM, this field should stay at 1 all time.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            All non-GSM techs   
4.5.2   nichunw   11/27/2013   
         Change the IQ_GAIN value (for APT/EPT/ET/QPT) of 1x and DO at both DAC rates, in order to have higher output power level. Requested by c2k team.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            1x and DO   
4.5.1   nichunw   11/20/2013   
         1. Assign DM1_SEL_MODE to SW dynamic so SW can change the value according to lab characterization results. 2. Make TXFE_DELAYMATCH subgroup runtime dynamic.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            All techs   
4.5.0   sunilk/vikramr   11/15/2013   
         Adding FW interfaces to provide FW with the ability to commit just the Delay Match portion of the TxLM buffer.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com            All techs   
4.4.3   nichunw   11/6/2013   
         1. Change UMTS default settings to have ROTATOR always on. 2. Change TXDAC_CONFIG/ETDAC_CONFIG values.3. Updated IREF LUT. 4. Adding TXLM sequences for DAC factory cal.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com               
4.4.2   nichunw   11/5/2013   
         Change TDS default settings to have ROTATOR always on.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com               
4.4.1   nichunw   10/30/2013   
         1. Correct the typo in LTE_DAC_275p2MHz_CHAINc_BW10MHz_CW state. (phase inc value) 2. Modify the DAC power up sequence to include enable_iref_update/disable_iref_update 3. Disable IQ_GAIN2_ENABLE for GSM76p8 and UMTS275p2 state.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com               
4.4.0   nichunw   10/23/2013   
         1. Set default IQ_GAIN value to all xPT mode. Default IQ_GAIN_ENABLE=1. 2. Add GSM DM values. 3. Changed txlm_var DM3_rate_Q26 to DM3_rate_Q25. 4. Change default env_scale value for all techs. 5. SW dynamic for all txlm_var.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com               
4.3.2   nichunw   10/23/2013   
         1. Made DAC_CLK_CFG SW dynamic.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com               
4.3.1   nichunw   10/22/2013   
         1. Added DTR_DACc_IREF_UPDATE_START/DTR_DACc_IREF_UPDATE_STOP.  2. Changed LTE default settings according to LTE team.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com               
4.3.0   nichunw   10/18/2013   
         1. Change ppdsm to 2-bit mode for GSM 57.6MHz. 2. Add TXROT_WAV_CLK_HZ and TXROT_WAV_SCALE_Q15 to txlm vars for C2K.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com               
4.2.0   nichunw   10/17/2013   
         1. Add independent write column and mark DAC_CLK_CFG as independent write.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com               
4.1.2   nichunw   10/17/2013   
         1. Add values for DAC_freq_khz row. 2. 10MHz -> 10MHz_cw for the CW column. 3. Add mask write for DAC_CLK_CFG      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com               
4.1.1   nichunw   10/16/2013   
         1. Updated TXLM_SEQEUNCES for dac power up and dac clk bringup. 2. Added a guideline tap to show the flow of dac power up and dac clk bringup 3. Add CW tone column 4. Add ADV_RET_STEP_Q29 and ADV_RET_STEP_Q31 into txlm vars. (for UMTS) 5. Add DAC_freq_khz in txlm var. 6. Modify DAC_CLK_CFG section to include bit mask shift.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com               
4.1.0   djoffe   10/9/2013   
         1. Updated DTR_CLK_DIV_CFG for chain based field offset and masking 2. Updated PowerMode tab for SW dynamic table generation 3. Disabled generation of DP_PEQ dynamic tab      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com               
4.0.16   nichunw   9/27/2013   
         1. Added DAC_CONFIG_IREF_UPDATES section for IREF update  2. Added DAC_RT_FUSE_CODE register for IREF cal code writing.  3. Updated IREF_LUT tab for mask and shift values. 4. Updated txfe and NBTX group delays in TXLM variables 5. Added TXLM sequences tab.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com               
4.0.15   nichunw   9/24/2013   
         1. Updated the txdac/etdac cfg values      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com               
4.0.14   nichunw   9/23/2013   
         1. Updated the registers in PowerMode_SW_Dynamic tab. 2. Added TX_DIGITAL_DELAY_MICROSECS_Q16 (txfe delay) and NBTX_DELAY_MICROSEC_Q16 (nbtx delay) to TXLM_Vars.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com               
4.0.13   sunilk   9/23/2013   
         1. adding Iref/PowerMode tabs  2. removing chain state_cfg and moving DAC_freq state_cfg       nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com               
4.0.12   sunilk   8/22/2013   
         1. adding a new GSM_DAC_38p4MHz_Anritsu_CHAINc for GSM/Anritsu operation      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com               
4.0.11   sunilk   8/2/2013   
         1. removed tab LTE_PRACH_SW_Dynamic 2. added new txlm variables for PRACH_PDA, only LTE will need these      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com               
4.0.10   sunilk   7/31/2013   
         1. added new txlm variables for TXR/PDA  2. added new tab LTE_PRACH_SW_Dynamic for PRACH for 15/20MHz      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com               
4.0.9   nichunw   7/22/2013   
         1. DAC_CLK_CFG subgroup is added.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com               
4.0.8   nichunw   7/18/2013   
         1. Two dummy IREF fields are added to DRIF for the continuity of DRIF address map. 2. QPT, ET, EPT tabs are updated. 3. UDM1 select is set to 3 for GSM.      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com               
4.0.7   sunilk   7/18/2013   
         1. removing MDM_CLK registers, as they need to programmed as per state/flow control 2. removing some EVT registers 3. enabling/disabling CLK_XO_GATE_CFG before writing DAC_CFG registers      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com               
4.0.6   sunilk   6/13/2013   
         1. fixed typo name for DM3      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com               
4.0.5   sunilk   6/13/2013   
         1. fixed typos for SAWEQ fields      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com               
4.0.4   sunilk   6/12/2013   
         1. fixed CONFIG2 field name typo      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com               
4.0.3   sunilk   6/12/2013   
         1. updating register name from DM3_FINE to DM3_FRAC      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com               
4.0.2   sunilk   6/7/2013   
         1. removing some deprecated registers 2. removing some hidden states 3. adding autosim tags for Bolt_txlmscenario2xml.pl script purposes      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com               
4.0.1   sunilk   6/5/2013   
         1. adding group names/other format changes      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com               
1.0.2   sunilk   6/5/2013   
         1. removed the Dime clk registers  2. deleted some Bolt start/stop time registers, as they will be set in FW 3. moved DAC_CLK_CFG registers to the MDM_CLK section      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com               
1.0.1   sunilk   5/1/2013   
         1. adding DAC_CONFIG and DTR_EVT sections 2. adding new states for DAC rate=275.2MHz 3. adding address values for DRIF/SRIF 4. adding cluster group IDs for DRIF registers      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com               
1.0.0   sunilk   4/24/2013   
         1. initial version      nichunw@qti.qualcomm.com,sunilk@qti.qualcomm.com               
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              

=============================================================================*/
/*=============================================================================
                           INCLUDE FILES
=============================================================================*/

#include "comdef.h" 
#include "txlm_intf.h" 





typedef enum
{
  RFLM_DTR_TX_STATE_GSM_DAC_57P6MHZ_CHAINC,
  RFLM_DTR_TX_STATE_UMTS_SC_DAC_115P2MHZ_CHAINC,
  RFLM_DTR_TX_STATE_UMTS_SC_DAC_230P4MHZ_CHAINC,
  RFLM_DTR_TX_STATE_LTE_DAC_230P4MHZ_CHAINC_BW10MHZ,
  RFLM_DTR_TX_STATE_LTE_DAC_230P4MHZ_CHAINC_BW10MHZ_ULCA,
  RFLM_DTR_TX_STATE_TDS_DAC_115P2MHZ_CHAINC,
  RFLM_DTR_TX_STATE_TDS_DAC_230P4MHZ_CHAINC,
  RFLM_DTR_TX_STATE_1X_DAC_115P2MHZ_CHAINC,
  RFLM_DTR_TX_STATE_1X_DAC_230P4MHZ_CHAINC,
  RFLM_DTR_TX_STATE_HDR_DAC_115P2MHZ_CHAINC,
  RFLM_DTR_TX_STATE_HDR_DAC_230P4MHZ_CHAINC,
  RFLM_DTR_TX_STATE_LTE_DAC_230P4MHZ_CHAINC_BW20MHZ,
  RFLM_DTR_TX_STATE_LTE_DAC_230P4MHZ_CHAINC_BW15MHZ,
  RFLM_DTR_TX_STATE_LTE_DAC_230P4MHZ_CHAINC_BW5MHZ,
  RFLM_DTR_TX_STATE_LTE_DAC_230P4MHZ_CHAINC_BW5MHZ_ULCA,
  RFLM_DTR_TX_STATE_LTE_DAC_230P4MHZ_CHAINC_BW3MHZ,
  RFLM_DTR_TX_STATE_LTE_DAC_230P4MHZ_CHAINC_BW1P4MHZ,
  RFLM_DTR_TX_STATE_GSM_DAC_76P8MHZ_CHAINC,
  RFLM_DTR_TX_STATE_GSM_DAC_38P4MHZ_ANRITSU_CHAINC,
  RFLM_DTR_TX_STATE_UMTS_SC_DAC_275P2MHZ_CHAINC,
  RFLM_DTR_TX_STATE_UMTS_SC_DAC_275P2MHZ_CHAINC_CW,
  RFLM_DTR_TX_STATE_LTE_DAC_275P2MHZ_CHAINC_BW10MHZ,
  RFLM_DTR_TX_STATE_LTE_DAC_275P2MHZ_CHAINC_BW10MHZ_ULCA,
  RFLM_DTR_TX_STATE_LTE_DAC_275P2MHZ_CHAINC_BW10MHZ_CW,
  RFLM_DTR_TX_STATE_TDS_DAC_275P2MHZ_CHAINC,
  RFLM_DTR_TX_STATE_1X_DAC_275P2MHZ_CHAINC,
  RFLM_DTR_TX_STATE_HDR_DAC_275P2MHZ_CHAINC,
  RFLM_DTR_TX_STATE_LTE_DAC_275P2MHZ_CHAINC_BW20MHZ,
  RFLM_DTR_TX_STATE_LTE_DAC_275P2MHZ_CHAINC_BW15MHZ,
  RFLM_DTR_TX_STATE_LTE_DAC_275P2MHZ_CHAINC_BW5MHZ,
  RFLM_DTR_TX_STATE_LTE_DAC_275P2MHZ_CHAINC_BW5MHZ_ULCA,
  RFLM_DTR_TX_STATE_LTE_DAC_275P2MHZ_CHAINC_BW3MHZ,
  RFLM_DTR_TX_STATE_LTE_DAC_275P2MHZ_CHAINC_BW1P4MHZ,
  RFLM_DTR_TX_STATE_NUM,
  RFLM_DTR_TX_STATE_INVALID
}rflm_dtr_tx_state_enum_type;



typedef enum
{
  RFLM_DTR_TX_BW_10MHZ, 
  RFLM_DTR_TX_BW_20MHZ, 
  RFLM_DTR_TX_BW_15MHZ, 
  RFLM_DTR_TX_BW_5MHZ, 
  RFLM_DTR_TX_BW_3MHZ, 
  RFLM_DTR_TX_BW_1P4MHZ, 
  RFLM_DTR_TX_BW_CW, 
  RFLM_DTR_TX_BW_NUM,
  RFLM_DTR_TX_BW_INVALID
}rflm_dtr_tx_enum_bw;



typedef enum
{
  RFLM_DTR_TX_DAC_FREQ_57P6, 
  RFLM_DTR_TX_DAC_FREQ_115P2, 
  RFLM_DTR_TX_DAC_FREQ_230P4, 
  RFLM_DTR_TX_DAC_FREQ_76P8, 
  RFLM_DTR_TX_DAC_FREQ_38P4, 
  RFLM_DTR_TX_DAC_FREQ_275P2, 
  RFLM_DTR_TX_DAC_FREQ_NUM,
  RFLM_DTR_TX_DAC_FREQ_INVALID
}rflm_dtr_tx_enum_dac_freq;



typedef enum
{
  RFLM_DTR_TX_INTRA_ULCA_MODE_DEFAULT, 
  RFLM_DTR_TX_INTRA_ULCA_MODE_10_X_20, 
  RFLM_DTR_TX_INTRA_ULCA_MODE_10_X_15, 
  RFLM_DTR_TX_INTRA_ULCA_MODE_5_X_20, 
  RFLM_DTR_TX_INTRA_ULCA_MODE_5_X_15, 
  RFLM_DTR_TX_INTRA_ULCA_MODE_NUM,
  RFLM_DTR_TX_INTRA_ULCA_MODE_INVALID
}rflm_dtr_tx_enum_intra_ulca_mode;



typedef struct
{
  rflm_dtr_tx_enum_bw bw;
  boolean carrier_cfg;
  rflm_dtr_tx_enum_dac_freq dac_freq;
  rflm_dtr_tx_enum_intra_ulca_mode intra_ulca_mode;
}txlm_state_cfg_params;



typedef enum
{
  RFLM_DTR_TX_HEADER,	/* Use Struct: rflm_dtr_tx_header_struct */
  TXLM_GROUP_TXLM_VARS,	/* Use Struct: rflm_dtr_tx_txlm_vars_group_struct */
  TXLM_GROUP_DAC_CLK_CFG,	/* Use Struct: rflm_dtr_tx_dac_clk_cfg_group_struct */
  TXLM_GROUP_TX_DAC_CFG,	/* Use Struct: rflm_dtr_tx_tx_dac_cfg_group_struct */
  TXLM_GROUP_TXFE_XO_CLK_ENABLE_IREF,	/* Use Struct: rflm_dtr_tx_txfe_xo_clk_enable_group_struct */
  TXLM_GROUP_TX_DAC_CFG_IREF,	/* Use Struct: rflm_dtr_tx_tx_dac_cfg_iref_group_struct */
  TXLM_GROUP_TXFE_XO_CLK_DISABLE_IREF,	/* Use Struct: rflm_dtr_tx_txfe_xo_clk_enable_group_struct */
  TXLM_GROUP_TXFE_SRIF,	/* Use Struct: rflm_dtr_tx_txfe_srif_group_struct */
  TXLM_GROUP_TXFE_SRIF_UDM1,	/* Use Struct: rflm_dtr_tx_txfe_srif_udm1_group_struct */
  TXLM_GROUP_TXFE_DELAYMATCH,	/* Use Struct: rflm_dtr_tx_txfe_delaymatch_group_struct */
  TXLM_GROUP_TXFE_DRIF,	/* Use Struct: rflm_dtr_tx_txfe_drif_group_struct */
  TXLM_GROUP_TXFE_EVT,	/* Use Struct: rflm_dtr_tx_txfe_evt_group_struct */
  RFLM_DTR_TX_BLOCK_ENABLE_FLAGS,	/* Use Struct: rflm_dtr_tx_block_valid_flags_struct */
  RFLM_DTR_TX_INDICES,	/* Use Struct: rflm_dtr_tx_indices_struct */
  TXLM_GROUP_NUM,
  TXLM_GROUP_INVALID
}txlm_dyn_group_type;

typedef enum
{
  RFLM_DTR_TX_COMMON_EPT,
  RFLM_DTR_TX_COMMON_ET,
  RFLM_DTR_TX_COMMON_QPT,
  RFLM_DTR_TX_COMMON_APT,
  RFLM_DTR_TX_COMMON_APT_LTE,
  RFLM_DTR_TX_POWERMODE_SW_DYNAMIC_SETTINGS_NUM,
  RFLM_DTR_TX_POWERMODE_SW_DYNAMIC_SETTINGS_INVALID
}rflm_dtr_tx_powermode_sw_dynamic_settings_enum_type;

typedef enum
{
  RFLM_DTR_TX_COMMON_CHAIN0,
  RFLM_DTR_TX_COMMON_CHAIN1,
  RFLM_DTR_TX_AVGERR_OBIT_SW_DYNAMIC_SETTINGS_NUM,
  RFLM_DTR_TX_AVGERR_OBIT_SW_DYNAMIC_SETTINGS_INVALID
}rflm_dtr_tx_avgerr_obit_sw_dynamic_settings_enum_type;

typedef enum
{
  RFLM_DTR_TX_COMMON_INTRA_CA,
  RFLM_DTR_TX_INTRA_CA_SW_DYNAMIC_SETTINGS_NUM,
  RFLM_DTR_TX_INTRA_CA_SW_DYNAMIC_SETTINGS_INVALID
}rflm_dtr_tx_intra_ca_sw_dynamic_settings_enum_type;

typedef enum
{
  RFLM_DTR_TX_COMMON_SCDO,
  RFLM_DTR_TX_COMMON_MCDO,
  RFLM_DTR_TX_IQ_GAIN_SW_DYNAMIC_SETTINGS_NUM,
  RFLM_DTR_TX_IQ_GAIN_SW_DYNAMIC_SETTINGS_INVALID
}rflm_dtr_tx_iq_gain_sw_dynamic_settings_enum_type;

typedef enum
{
  RFLM_DTR_TX_LTE_LTE_BW10MHZ_WTR_275P2MHZ_SAW,
  RFLM_DTR_TX_LTE_LTE_BW20MHZ_WTR_275P2MHZ_SAW,
  RFLM_DTR_TX_LTE_LTE_BW15MHZ_WTR_275P2MHZ_SAW,
  RFLM_DTR_TX_LTE_LTE_BW5MHZ_WTR_275P2MHZ_SAW,
  RFLM_DTR_TX_LTE_LTE_BW3MHZ_WTR_275P2MHZ_SAW,
  RFLM_DTR_TX_LTE_LTE_BW1p4MHZ_WTR_275P2MHZ_SAW,
  RFLM_DTR_TX_LTE_LTE_INTRABAND_ULCA_WTR_275P2MHZ_SAW,
  RFLM_DTR_TX_LTE_LTE_BW10MHZ_WTR_230P4MHZ_SAW,
  RFLM_DTR_TX_LTE_LTE_BW20MHZ_WTR_230P4MHZ_SAW,
  RFLM_DTR_TX_LTE_LTE_BW15MHZ_WTR_230P4MHZ_SAW,
  RFLM_DTR_TX_LTE_LTE_BW5MHZ_WTR_230P4MHZ_SAW,
  RFLM_DTR_TX_LTE_LTE_BW3MHZ_WTR_230P4MHZ_SAW,
  RFLM_DTR_TX_LTE_LTE_BW1p4MHZ_WTR_230P4MHZ_SAW,
  RFLM_DTR_TX_LTE_LTE_INTRABAND_ULCA_WTR_230P4MHZ_SAW,
  RFLM_DTR_TX_LTE_LTE_BW10MHZ_275P2MHZ_SAWLESS,
  RFLM_DTR_TX_LTE_LTE_BW20MHZ_275P2MHZ_SAWLESS,
  RFLM_DTR_TX_LTE_LTE_BW15MHZ_275P2MHZ_SAWLESS,
  RFLM_DTR_TX_LTE_LTE_BW5MHZ_275P2MHZ_SAWLESS,
  RFLM_DTR_TX_LTE_LTE_BW3MHZ_S275P2MHZ_AWLESS,
  RFLM_DTR_TX_LTE_LTE_BW1p4MHZ_275P2MHZ_SAWLESS,
  RFLM_DTR_TX_LTE_LTE_BW10MHZ_RTR_SAW,
  RFLM_DTR_TX_LTE_LTE_BW20MHZ_RTR_SAW,
  RFLM_DTR_TX_LTE_LTE_BW15MHZ_RTR_SAW,
  RFLM_DTR_TX_LTE_LTE_BW5MHZ_RTR_SAW,
  RFLM_DTR_TX_LTE_LTE_BW3MHZ_RTR_SAW,
  RFLM_DTR_TX_LTE_LTE_BW1p4MHZ_RTR_SAW,
  RFLM_DTR_TX_LTE_LTE_INTRABAND_ULCA_275P2MHZ_SAWLESS,
  RFLM_DTR_TX_LTE_LTE_BW10MHZ_230P4MHZ_SAWLESS,
  RFLM_DTR_TX_LTE_LTE_BW20MHZ_230P4MHZ_SAWLESS,
  RFLM_DTR_TX_LTE_LTE_BW15MHZ_230P4MHZ_SAWLESS,
  RFLM_DTR_TX_LTE_LTE_BW5MHZ_230P4MHZ_SAWLESS,
  RFLM_DTR_TX_LTE_LTE_BW3MHZ_230P4MHZ_SAWLESS,
  RFLM_DTR_TX_LTE_LTE_BW1p4MHZ_230P4MHZ_SAWLESS,
  RFLM_DTR_TX_LTE_LTE_INTRABAND_ULCA_230P4MHZ_SAWLESS,
  RFLM_DTR_TX_LTE_UMTS_SC_DAC_275P2MHZ_SAWLESS,
  RFLM_DTR_TX_LTE_UMTS_DC_DAC_275P2MHZ_SAWLESS,
  RFLM_DTR_TX_LTE_UMTS_SC_DAC_230P4MHZ_115P2MHZ_SAWLESS,
  RFLM_DTR_TX_LTE_UMTS_DC_DAC_230P4MHZ_115P2MHZ_SAWLESS,
  RFLM_DTR_TX_DP_PEQ_SW_DYNAMIC_SETTINGS_NUM,
  RFLM_DTR_TX_DP_PEQ_SW_DYNAMIC_SETTINGS_INVALID
}rflm_dtr_tx_dp_peq_sw_dynamic_settings_enum_type;

typedef enum
{
  RFLM_DTR_TX_COMMON_CONST_ENV,
  RFLM_DTR_TX_COMMON_CONST_IQ,
  RFLM_DTR_TX_COMMON_DEFAULT,
  RFLM_DTR_TX_ET_SPLIT_CAPTURE_FTM_SW_DYNAMIC_SETTINGS_NUM,
  RFLM_DTR_TX_ET_SPLIT_CAPTURE_FTM_SW_DYNAMIC_SETTINGS_INVALID
}rflm_dtr_tx_et_split_capture_ftm_sw_dynamic_settings_enum_type;

typedef enum
{
  RFLM_DTR_TX_POWERMODE,
  RFLM_DTR_TX_INTRA_CA,
  RFLM_DTR_TX_DP_PEQ,
  RFLM_DTR_TX_IQ_GAIN,
  RFLM_DTR_TX_AVGERR_OBIT,
  RFLM_DTR_TX_ET_SPLIT_CAPTURE_FTM,
  RFLM_DTR_TX_DYN_FUNCTIONALITY_NUM,
  RFLM_DTR_TX_DYN_FUNCTIONALITY_INVALID
}rflm_dtr_tx_dyn_functionality_type;

typedef struct
{
	uint32 empty_struct;
}txlm_dac_cal_config_type;

typedef enum
{
   /* Write enums */
   TXLM_DAC_CAL_CMD_WRITE_DAC_CALIB_CFG_C_ETDAC_CALIB, /*  */
   TXLM_DAC_CAL_CMD_WRITE_DAC_CALIB_CFG_C_ET_CAL_MODE, /*  */
   TXLM_DAC_CAL_CMD_WRITE_DAC_CALIB_CFG_C_FORCE_ETDAC_DATA, /*  */
   TXLM_DAC_CAL_CMD_WRITE_DAC_CALIB_CFG_C_FORCE_TXDAC_DATA, /*  */
   TXLM_DAC_CAL_CMD_WRITE_DAC_CALIB_CFG_C_TXDAC_CALIB, /*  */
   TXLM_DAC_CAL_CMD_WRITE_DAC_CALIB_CFG_C_TX_CAL_MODE, /*  */

   TXLM_DAC_CAL_CMD_NUM,
   TXLM_DAC_CAL_CMD_INVALID
}txlm_dac_cal_cmd_enum_type;


#ifdef __cplusplus
}
#endif



#endif


