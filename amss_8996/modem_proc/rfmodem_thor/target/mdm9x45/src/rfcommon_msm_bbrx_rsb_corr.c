/*==============================================================================

  Copyright (c) 2014 - 2015 Qualcomm Technologies, Inc. All Rights Reserved

  Qualcomm Technologies Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies, Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies, Incorporated.

==============================================================================*/

/*==============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/rfmodem_thor/target/mdm9x45/src/rfcommon_msm_bbrx_rsb_corr.c#1 $

when        who    what, where, why
--------    ---    ----------------------------------------------------------
05/29/15    dej    Add handling of V2 registers
05/14/14    cvd    Initial Revision 
==============================================================================*/ 

#include "comdef.h"
#include "rxlm_intf.h"
#include "rflm_dtr_rx_bbrx_rsb_corr.h"
#include "rflm_features.h"


#if (RFLM_FEATURE_BUILD_MODE!=RFLM_FEATURE_BUILD_FW_LIB)

#include "rflm_hwintf.h"

uint32 bbrx_fuse_to_gain_value_table[BBRX_RSB_FUSE_COMBINATIONS] =  \
{65536, 64488, 63457, 62442, 65536, 66600, 67683, 68782};

bbrx_rsb_fuse_reg_data bbrx_gain_reg_data[RXLM_CHAIN_MAX] = /* Array fuse_gain_reg_data */
{
  /* Gain Mismatch for BBRX#0 */ /*fuse_gain_reg_data[0] */
  {
    /* bits_location as struct */
    {
      /* bits_location[0] */
      { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_LSB), 0 } ,
      /* bits_location[1] */
      { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_LSB), 1 } ,
      /* bits_location[2] */
      { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_LSB), 2 } 
    }
  }, 

  /* Gain Mismatch for BBRX#1 */
  {
    {
      { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_LSB), 21 } ,
      { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_LSB), 22 } ,
      { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_LSB), 23 } 
    }
  }, 

  /* Gain Mismatch for BBRX#2 */
  {
    {
      { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_MSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_MSB), 10 } ,
      { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_MSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_MSB), 11 } ,
      { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_MSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_MSB), 12 } 
    }
  }, 

  /* Gain Mismatch for BBRX#3 */
  {
    {
      { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_LSB), 7 } ,
      { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_LSB), 8 } ,
      { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_LSB), 9 } 
    }
  }, 

  /* Gain Mismatch for BBRX#4 */
  {
    {
      { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_LSB), 28 } ,
      { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_LSB), 29 } ,
      { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_LSB), 30 } 
    }
  }, 

  /* Gain Mismatch for BBRX#5 */
  {
    {
      { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_MSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_MSB), 17 } ,
      { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_MSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_MSB), 18 } ,
      { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_MSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_MSB), 19 } 
    }
  }, 

  /* Gain Mismatch for BBRX#6 */
  {
    {
      { HWIO_ADDR(QFPROM_RAW_CALIB_ROW6_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW6_LSB), 14 } ,
      { HWIO_ADDR(QFPROM_RAW_CALIB_ROW6_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW6_LSB), 15 } ,
      { HWIO_ADDR(QFPROM_RAW_CALIB_ROW6_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW6_LSB), 16 } 
    }
  }
};

bbrx_rsb_fuse_reg_data bbrx_gain_reg_data_v2[RXLM_CHAIN_MAX] = /* Array fuse_gain_reg_data */
{
  /* Gain Mismatch for BBRX#0 */ /*fuse_gain_reg_data[0] */
  {
    /* bits_location as struct */
    {
      /* bits_location[0] */
      { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_LSB), 0 } ,
      /* bits_location[1] */
      { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_LSB), 1 } ,
      /* bits_location[2] */
      { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_LSB), 2 } 
    }
  }, 

  /* Gain Mismatch for BBRX#1 */
  {
    {
      { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_LSB), 21 } ,
      { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_LSB), 22 } ,
      { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_LSB), 23 } 
    }
  }, 

  /* Gain Mismatch for BBRX#2 */
  {
    {
      { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_MSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_MSB), 10 } ,
      { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_MSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_MSB), 11 } ,
      { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_MSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_MSB), 12 } 
    }
  }, 

  /* Gain Mismatch for BBRX#3 */
  {
    {
      { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_LSB), 7 } ,
      { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_LSB), 8 } ,
      { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_LSB), 9 } 
    }
  }, 

  /* Gain Mismatch for BBRX#4 */
  {
    {
      { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_LSB), 28 } ,
      { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_LSB), 29 } ,
      { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_LSB), 30 } 
    }
  }, 

  /* Gain Mismatch for BBRX#5 */
  {
    {
      { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_MSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_MSB), 17 } ,
      { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_MSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_MSB), 18 } ,
      { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_MSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_MSB), 19 } 
    }
  }, 

  /* Gain Mismatch for BBRX#6 */
  {
    {
      { HWIO_ADDR(QFPROM_RAW_CALIB_ROW6_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW6_LSB), 14 } ,
      { HWIO_ADDR(QFPROM_RAW_CALIB_ROW6_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW6_LSB), 15 } ,
      { HWIO_ADDR(QFPROM_RAW_CALIB_ROW6_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW6_LSB), 16 } 
    }
  }
};

/* Stored with (int32)(pow(10, (gain_delta / 20)) * 65536); done */
bbrx_fuse_to_gain_delta_value_type bbrx_fuse_to_gain_delta_value_table =  \
{
  /* Mode_0 */
  {65536, 65384, 65244, 65114, 65536, 65688, 65828, 65959},
  /* Mode_1 */
  {65536, 65321, 65122, 64939, 65536, 65751, 65951, 66138},
  /* Mode_2 */
  {65536, 65286, 65055, 64842, 65536, 65786, 66019, 66237},
  /* Mode_3 */
  {65536, 65165, 64823, 64506, 65536, 65908, 66256, 66581}
};

bbrx_fuse_to_gain_delta_value_type bbrx_fuse_to_gain_delta_value_table_fbrx =  \
{  /* Mode_0 */
  {65536, 65536, 65536, 65536, 65536, 65536, 65536, 65536},
  /* Mode_1 */
  {65536, 65422, 65302, 65189, 65536, 65649, 65770, 65883},
  /* Mode_2 */
  {65536, 65536, 65536, 65536, 65536, 65536, 65536, 65536},
  /* Mode_3 */
  {65536, 65325, 65107, 64897, 65536, 65747, 65967, 66180}
};

bbrx_rsb_fuse_reg_data bbrx_phase_reg_data[RXLM_CHAIN_MAX][BBRX_MODE_MAX] = /* Array fuse_gain_reg_data */
{
  /* Phase Mismatch for Chain_0 */ /* bbrx_phase_reg_data[0] */
  {
    /* Chain_0, Mode_0 */ /* bbrx_phase_reg_data[0][0] */
    {
      /* bits_location as struct */
      {
        /* bits_location[0] */
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_LSB), 5 } ,
        /* bits_location[1] */
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_LSB), 6 } ,
        /* bits_location[2] */
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_LSB), 7 } 
      }
    }, 

    /* Gain Mismatch for  Chain_0, Mode_1 */
    {
      {
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_LSB), 8 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_LSB), 9 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_LSB), 10 } 
      }
    }, 

    /* Gain Mismatch for Chain_0, Mode_2 */
    {
      {
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_LSB), 11 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_LSB), 12 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_LSB), 13 } 
      }
    }, 

    /* Gain Mismatch for Chain_0, Mode_3 */
    {
      {
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_LSB), 14 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_LSB), 15 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_LSB), 16 } 
      }
    }
  },

  /* Phase Mismatch for Chain_1 */ /* bbrx_phase_reg_data[1] */
  {
    /* Chain_1, Mode_0 */ /* bbrx_phase_reg_data[1][0] */
    {
      /* bits_location as struct */
      {
        /* bits_location[0] */
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_LSB), 26 } ,
        /* bits_location[1] */
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_LSB), 27 } ,
        /* bits_location[2] */
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_LSB), 28 } 
      }
    }, 

    /* Gain Mismatch for  Chain_1, Mode_1 */
    {
      {
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_LSB), 29 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_LSB), 30 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_LSB), 31 } 
      }
    }, 

    /* Gain Mismatch for Chain_1, Mode_2 */
    {
      {
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_LSB), 0 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_LSB), 1 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_MSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_MSB), 2 } 
      }
    }, 

    /* Gain Mismatch for Chain_1, Mode_3 */
    {
      {
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_MSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_MSB), 3 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_MSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_MSB), 4 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_MSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_MSB), 5 } 
      }
    }
  },

  /* Phase Mismatch for Chain_2 */ /* bbrx_phase_reg_data[2] */
  {
    /* Chain_2, Mode_0 */ /* bbrx_phase_reg_data[2][0] */
    {
      /* bits_location as struct */
      {
        /* bits_location[0] */
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_MSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_MSB), 15 } ,
        /* bits_location[1] */
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_MSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_MSB), 16 } ,
        /* bits_location[2] */
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_MSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_MSB), 17 } 
      }
    }, 

    /* Gain Mismatch for Chain_2, Mode_1 */
    {
      {
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_MSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_MSB), 18 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_MSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_MSB), 19 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_MSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_MSB), 20 } 
      }
    }, 

    /* Gain Mismatch for Chain_2, Mode_2 */
    {
      {
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_MSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_MSB), 21 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_MSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_MSB), 22 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_MSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_MSB), 23 } 
      }
    }, 

    /* Gain Mismatch for Chain_2, Mode_3 */
    {
      {
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_MSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_MSB), 24 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_MSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_MSB), 25 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_LSB), 26 } 
      }
    }
  },

  /* Phase Mismatch for Chain_3 */ /* bbrx_phase_reg_data[3] */
  {
    /* Chain_3, Mode_0 */ /* bbrx_phase_reg_data[3][0] */
    {
      /* bits_location as struct */
      {
        /* bits_location[0] */
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_LSB), 5 } ,
        /* bits_location[1] */
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_LSB), 6 } ,
        /* bits_location[2] */
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_LSB), 7 } 
      }
    }, 

    /* Gain Mismatch for Chain_3, Mode_1 */
    {
      {
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_LSB), 8 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_LSB), 9 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_LSB), 10 } 
      }
    }, 

    /* Gain Mismatch for Chain_3, Mode_2 */
    {
      {
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_LSB), 11 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_LSB), 12 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_LSB), 13 } 
      }
    }, 

    /* Gain Mismatch for Chain_3, Mode_3 */
    {
      {
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_LSB), 14 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_LSB), 15 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_LSB), 16 } 
      }
    }
  },

  /* Phase Mismatch for Chain_FBRX */ /* bbrx_phase_reg_data[4] */
  {
    /* Chain_FBRX, Mode_0 */ /* bbrx_phase_reg_data[4][0] */
    {
      /* bits_location as struct */
      {
        /* bits_location[0] */
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_LSB), 26 } ,
        /* bits_location[1] */
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_MSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_MSB), 27 } ,
        /* bits_location[2] */
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_MSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_MSB), 28 } 
      }
    }, 

    /* Gain Mismatch for Chain_FBRX, Mode_1 */
    {
      {
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_MSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_MSB), 29 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_MSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_MSB), 30 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_MSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_MSB), 31 } 
      }
    }, 

    /* Gain Mismatch for Chain_FBRX, Mode_2 */
    {
      {
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_MSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_MSB), 0 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_MSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_MSB), 1 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_MSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_MSB), 2 } 
      }
    }, 

    /* Gain Mismatch for Chain_FBRX, Mode_3 */
    {
      {
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_MSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_MSB), 3 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_MSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_MSB), 4 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_MSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_MSB), 5 } 
      }
    }
  },

  /* Phase Mismatch for Chain_5 */ /* bbrx_phase_reg_data[5] */
  {
    /* Chain_5, Mode_0 */ /* bbrx_phase_reg_data[3][0] */
    {
      /* bits_location as struct */
      {
        /* bits_location[0] */
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_MSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_MSB), 15 } ,
        /* bits_location[1] */
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_MSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_MSB), 16 } ,
        /* bits_location[2] */
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_MSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_MSB), 17 } 
      }
    }, 

    /* Gain Mismatch for Chain_5, Mode_1 */
    {
      {
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_MSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_MSB), 18 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW6_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW6_LSB), 19 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW6_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW6_LSB), 20 } 
      }
    }, 

    /* Gain Mismatch for Chain_5, Mode_2 */
    {
      {
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW6_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW6_LSB), 21 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW6_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW6_LSB), 22 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW6_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW6_LSB), 23 } 
      }
    }, 

    /* Gain Mismatch for Chain_5, Mode_3 */
    {
      {
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW6_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW6_LSB), 24 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW6_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW6_LSB), 25 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW6_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW6_LSB), 26 } 
      }
    }
  },

  /* Phase Mismatch for Chain_6 */ /* bbrx_phase_reg_data[6] */
  {
    /* Chain_6, Mode_0 */ /* bbrx_phase_reg_data[3][0] */
    {
      /* bits_location as struct */
      {
        /* bits_location[0] */
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW6_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW6_LSB), 5 } ,
        /* bits_location[1] */
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW6_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW6_LSB), 6 } ,
        /* bits_location[2] */
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW6_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW6_LSB), 7 } 
      }
    }, 

    /* Gain Mismatch for Chain_6, Mode_1 */
    {
      {
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW6_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW6_LSB), 8 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW6_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW6_LSB), 9 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW6_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW6_LSB), 10 } 
      }
    }, 

    /* Gain Mismatch for Chain_6, Mode_2 */
    {
      {
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW6_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW6_LSB), 11 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW6_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW6_LSB), 12 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW6_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW6_LSB), 13 } 
      }
    }, 

    /* Gain Mismatch for Chain_6, Mode_3 */
    {
      {
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW6_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW6_LSB), 14 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW6_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW6_LSB), 15 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW6_LSB), HWIO_RMSK(QFPROM_RAW_CALIB_ROW6_LSB), 16 } 
      }
    }
  }
};

bbrx_rsb_fuse_reg_data bbrx_phase_reg_data_v2[RXLM_CHAIN_MAX][BBRX_MODE_MAX] = /* Array fuse_gain_reg_data */
{
  /* Phase Mismatch for Chain_0 */ /* bbrx_phase_reg_data[0] */
  {
    /* Chain_0, Mode_0 */ /* bbrx_phase_reg_data[0][0] */
    {
      /* bits_location as struct */
      {
        /* bits_location[0] */
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_LSB_V2), 5 } ,
        /* bits_location[1] */
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_LSB_V2), 6 } ,
        /* bits_location[2] */
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_LSB_V2), 7 } 
      }
    }, 

    /* Gain Mismatch for  Chain_0, Mode_1 */
    {
      {
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_LSB_V2), 8 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_LSB_V2), 9 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_LSB_V2), 10 } 
      }
    }, 

    /* Gain Mismatch for Chain_0, Mode_2 */
    {
      {
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_LSB_V2), 11 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_LSB_V2), 12 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_LSB_V2), 13 } 
      }
    }, 

    /* Gain Mismatch for Chain_0, Mode_3 */
    {
      {
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_LSB_V2), 14 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_LSB_V2), 15 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_LSB_V2), 16 } 
      }
    }
  },

  /* Phase Mismatch for Chain_1 */ /* bbrx_phase_reg_data[1] */
  {
    /* Chain_1, Mode_0 */ /* bbrx_phase_reg_data[1][0] */
    {
      /* bits_location as struct */
      {
        /* bits_location[0] */
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_LSB_V2), 26 } ,
        /* bits_location[1] */
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_LSB_V2), 27 } ,
        /* bits_location[2] */
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_LSB_V2), 28 } 
      }
    }, 

    /* Gain Mismatch for  Chain_1, Mode_1 */
    {
      {
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_LSB_V2), 29 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_LSB_V2), 30 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_LSB_V2), 31 } 
      }
    }, 

    /* Gain Mismatch for Chain_1, Mode_2 */
    {
      {
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_LSB_V2), 0 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_LSB_V2), 1 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_MSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_MSB_V2), 2 } 
      }
    }, 

    /* Gain Mismatch for Chain_1, Mode_3 */
    {
      {
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_MSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_MSB_V2), 3 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_MSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_MSB_V2), 4 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_MSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_MSB_V2), 5 } 
      }
    }
  },

  /* Phase Mismatch for Chain_2 */ /* bbrx_phase_reg_data[2] */
  {
    /* Chain_2, Mode_0 */ /* bbrx_phase_reg_data[2][0] */
    {
      /* bits_location as struct */
      {
        /* bits_location[0] */
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_MSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_MSB_V2), 15 } ,
        /* bits_location[1] */
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_MSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_MSB_V2), 16 } ,
        /* bits_location[2] */
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_MSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_MSB_V2), 17 } 
      }
    }, 

    /* Gain Mismatch for Chain_2, Mode_1 */
    {
      {
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_MSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_MSB_V2), 18 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_MSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_MSB_V2), 19 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_MSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_MSB_V2), 20 } 
      }
    }, 

    /* Gain Mismatch for Chain_2, Mode_2 */
    {
      {
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_MSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_MSB_V2), 21 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_MSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_MSB_V2), 22 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_MSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_MSB_V2), 23 } 
      }
    }, 

    /* Gain Mismatch for Chain_2, Mode_3 */
    {
      {
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_MSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_MSB_V2), 24 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW4_MSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW4_MSB_V2), 25 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_LSB_V2), 26 } 
      }
    }
  },

  /* Phase Mismatch for Chain_3 */ /* bbrx_phase_reg_data[3] */
  {
    /* Chain_3, Mode_0 */ /* bbrx_phase_reg_data[3][0] */
    {
      /* bits_location as struct */
      {
        /* bits_location[0] */
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_LSB_V2), 5 } ,
        /* bits_location[1] */
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_LSB_V2), 6 } ,
        /* bits_location[2] */
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_LSB_V2), 7 } 
      }
    }, 

    /* Gain Mismatch for Chain_3, Mode_1 */
    {
      {
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_LSB_V2), 8 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_LSB_V2), 9 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_LSB_V2), 10 } 
      }
    }, 

    /* Gain Mismatch for Chain_3, Mode_2 */
    {
      {
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_LSB_V2), 11 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_LSB_V2), 12 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_LSB_V2), 13 } 
      }
    }, 

    /* Gain Mismatch for Chain_3, Mode_3 */
    {
      {
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_LSB_V2), 14 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_LSB_V2), 15 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_LSB_V2), 16 } 
      }
    }
  },

  /* Phase Mismatch for Chain_FBRX */ /* bbrx_phase_reg_data[4] */
  {
    /* Chain_FBRX, Mode_0 */ /* bbrx_phase_reg_data[4][0] */
    {
      /* bits_location as struct */
      {
        /* bits_location[0] */
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_LSB_V2), 26 } ,
        /* bits_location[1] */
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_MSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_MSB_V2), 27 } ,
        /* bits_location[2] */
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_MSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_MSB_V2), 28 } 
      }
    }, 

    /* Gain Mismatch for Chain_FBRX, Mode_1 */
    {
      {
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_MSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_MSB_V2), 29 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_MSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_MSB_V2), 30 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_MSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_MSB_V2), 31 } 
      }
    }, 

    /* Gain Mismatch for Chain_FBRX, Mode_2 */
    {
      {
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_MSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_MSB_V2), 0 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_MSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_MSB_V2), 1 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_MSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_MSB_V2), 2 } 
      }
    }, 

    /* Gain Mismatch for Chain_FBRX, Mode_3 */
    {
      {
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_MSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_MSB_V2), 3 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_MSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_MSB_V2), 4 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_MSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_MSB_V2), 5 } 
      }
    }
  },

  /* Phase Mismatch for Chain_5 */ /* bbrx_phase_reg_data[5] */
  {
    /* Chain_5, Mode_0 */ /* bbrx_phase_reg_data[3][0] */
    {
      /* bits_location as struct */
      {
        /* bits_location[0] */
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_MSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_MSB_V2), 15 } ,
        /* bits_location[1] */
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_MSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_MSB_V2), 16 } ,
        /* bits_location[2] */
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_MSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_MSB_V2), 17 } 
      }
    }, 

    /* Gain Mismatch for Chain_5, Mode_1 */
    {
      {
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW5_MSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW5_MSB_V2), 18 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW6_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW6_LSB_V2), 19 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW6_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW6_LSB_V2), 20 } 
      }
    }, 

    /* Gain Mismatch for Chain_5, Mode_2 */
    {
      {
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW6_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW6_LSB_V2), 21 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW6_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW6_LSB_V2), 22 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW6_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW6_LSB_V2), 23 } 
      }
    }, 

    /* Gain Mismatch for Chain_5, Mode_3 */
    {
      {
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW6_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW6_LSB_V2), 24 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW6_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW6_LSB_V2), 25 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW6_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW6_LSB_V2), 26 } 
      }
    }
  },

  /* Phase Mismatch for Chain_6 */ /* bbrx_phase_reg_data[6] */
  {
    /* Chain_6, Mode_0 */ /* bbrx_phase_reg_data[3][0] */
    {
      /* bits_location as struct */
      {
        /* bits_location[0] */
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW6_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW6_LSB_V2), 5 } ,
        /* bits_location[1] */
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW6_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW6_LSB_V2), 6 } ,
        /* bits_location[2] */
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW6_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW6_LSB_V2), 7 } 
      }
    }, 

    /* Gain Mismatch for Chain_6, Mode_1 */
    {
      {
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW6_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW6_LSB_V2), 8 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW6_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW6_LSB_V2), 9 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW6_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW6_LSB_V2), 10 } 
      }
    }, 

    /* Gain Mismatch for Chain_6, Mode_2 */
    {
      {
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW6_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW6_LSB_V2), 11 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW6_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW6_LSB_V2), 12 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW6_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW6_LSB_V2), 13 } 
      }
    }, 

    /* Gain Mismatch for Chain_6, Mode_3 */
    {
      {
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW6_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW6_LSB_V2), 14 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW6_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW6_LSB_V2), 15 } ,
        { HWIO_ADDR(QFPROM_RAW_CALIB_ROW6_LSB_V2), HWIO_RMSK(QFPROM_RAW_CALIB_ROW6_LSB_V2), 16 } 
      }
    }
  }
};


/*---------------------------------------------------------------------------*/

uint32* rfcommon_msm_get_bbrx_fuse_to_gain_value_table(void)
{
  return bbrx_fuse_to_gain_value_table;
}

bbrx_fuse_to_gain_delta_value_type *rfcommon_msm_get_bbrx_fuse_to_gain_delta_value_table(void)
{
  return &bbrx_fuse_to_gain_delta_value_table;
}

bbrx_fuse_to_gain_delta_value_type *rfcommon_msm_get_bbrx_fuse_to_gain_delta_value_table_fbrx(void)
{
  return &bbrx_fuse_to_gain_delta_value_table_fbrx;
}

#endif
