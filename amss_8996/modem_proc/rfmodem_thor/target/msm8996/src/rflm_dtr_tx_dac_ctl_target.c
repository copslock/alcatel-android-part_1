/*! 
  @file
  rflm_dtr_tx_dac_ctl.c
 
  @brief
  Implements the RFLM DTR tx Interface Functions to FW
 
*/

/*==============================================================================

  Copyright (c) 2015 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

==============================================================================*/

/*==============================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

  $Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/rfmodem_thor/target/msm8996/src/rflm_dtr_tx_dac_ctl_target.c#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------- 
06/21/15   lm       Enabled qfuse read for chain 1
04/17/15   dej      Initial version.
==============================================================================*/

/*=============================================================================
                           INCLUDE FILES
=============================================================================*/
#include "rflm.h"
#include "txlm_intf.h"
#include "rflm_dtr_tx_dac_ctl.h"
#include "rflm_dtr_tx_struct_ag.h"
#include "rflm_dtr_tx_typedef_ag.h"
#include "rfmodem_target_common.h"

#include "rflm_hwintf.h"
#include "rflm_features.h"

#include "rflm_time.h"
#if (RFLM_FEATURE_BUILD_MODE!=RFLM_FEATURE_BUILD_FW_LIB)
#include "DALSys.h"

rflm_dtr_tx_dac_therm_info_t rflm_dtr_tx_dac_therm_info[] =
{
  {
  	0,
	0,
  	{
  	  -346,
	  20,
	  85,
	  130,
	  -50,
	  5
  	}, /* Thor - TSMC */
  },
  {
  	0,
	1,
  	{
  	  159,
	  20,
	  85,
	  130,
	  -50,
	  5
  	}, /* Thor - GF */
  }
};
#endif /* SKIP FOR FW STANDALONE BUILD */

const txdac_iref_t txdac_iref_lut[TXDAC_IREF_LUT_LENGTH] RFLM_SECTION_TCM_DATA = {
	{2500,0xF0000000,0xF0000000,0x0000000F,0xC},
	{2228,0xF0000000,0x40000000,0x0000000F,0xC},
	{1986,0xF0000000,0xA0000000,0x0000000F,0xB},
	{1770,0xF0000000,0x70000000,0x0000000F,0xA},
	{1577,0xF0000000,0x30000000,0x0000000F,0xA},
	{1406,0xF0000000,0xC0000000,0x0000000F,0x9},
	{1253,0xF0000000,0xF0000000,0x0000000F,0x8},
	{1117,0xF0000000,0x40000000,0x0000000F,0x8},
	{995,0xF0000000,0xB0000000,0x0000000F,0x7},
	{887,0xF0000000,0x70000000,0x0000000F,0x6},
	{791,0xF0000000,0x40000000,0x0000000F,0x6},
	{705,0xF0000000,0xC0000000,0x0000000F,0x5},
	{628,0xF0000000,0xF0000000,0x0000000F,0x4},
	{560,0xF0000000,0x50000000,0x0000000F,0x4},
	{499,0xF0000000,0xB0000000,0x0000000F,0x3},
	{445,0xF0000000,0xF0000000,0x0000000F,0x2},
	{396,0xF0000000,0x40000000,0x0000000F,0x2},
	{353,0xF0000000,0xB0000000,0x0000000F,0x1},
	{315,0xF0000000,0xF0000000,0x0000000F,0x0},
	{281,0xF0000000,0x40000000,0x0000000F,0x0}	
};

void rflm_dtr_tx_read_qfuse(txlm_chain_type chain, uint32* qfuse_rd)
{
  if (TXLM_CHAIN_0 == chain)
  {
#ifdef HWIO_QFPROM_CORR_CALIB_ROW12_LSB_TXDAC0_CAL_BMSK
    *qfuse_rd = HWIO_INF(QFPROM_CORR_CALIB_ROW12_LSB, TXDAC0_CAL);
#endif
  }
  else if (TXLM_CHAIN_1 == chain) 
  {
#ifdef HWIO_QFPROM_CORR_CALIB_ROW12_LSB_TXDAC1_CAL_BMSK
    *qfuse_rd = HWIO_INF(QFPROM_CORR_CALIB_ROW12_LSB, TXDAC1_CAL);
#endif
  }
  else
  {
    /* Invalid Chain Information: Returning default value */
    *qfuse_rd = 128;
  }
}

void rflm_dtr_tx_read_0_1_fuseflag_bit(uint32* fuseflag_bit)
{
#ifdef HWIO_QFPROM_CORR_CALIB_ROW12_LSB_TXDAC_0_1_FLAG_BMSK
  *fuseflag_bit =  HWIO_INF(QFPROM_CORR_CALIB_ROW12_LSB, TXDAC_0_1_FLAG);
#endif
}

void rflm_dtr_tx_read_avg_error(txlm_chain_type chain, uint32* avg_error)
{
  if (TXLM_CHAIN_0 == chain)
  {
#ifdef HWIO_QFPROM_CORR_CALIB_ROW12_LSB_TXDAC0_CAL_AVG_ERR_BMSK
    *avg_error = HWIO_INF(QFPROM_CORR_CALIB_ROW12_LSB, TXDAC0_CAL_AVG_ERR );
#endif
  }
  else if (TXLM_CHAIN_1 == chain)  
  {
#ifdef HWIO_QFPROM_CORR_CALIB_ROW12_MSB_TXDAC1_CAL_AVG_ERR_BMSK
    *avg_error = HWIO_INF(QFPROM_CORR_CALIB_ROW12_MSB, TXDAC1_CAL_AVG_ERR );
#endif
  }
}

void rflm_dtr_tx_read_overflow_bit(txlm_chain_type chain, uint32* overflow_bit)
{
  if (TXLM_CHAIN_0 == chain)
  {
#ifdef HWIO_QFPROM_CORR_CALIB_ROW12_LSB_TXDAC0_CAL_OVFLOW_BMSK
    *overflow_bit = HWIO_INF(QFPROM_CORR_CALIB_ROW12_LSB, TXDAC0_CAL_OVFLOW);
#endif
  }
  else if (TXLM_CHAIN_1 == chain) 
  {
#ifdef HWIO_QFPROM_CORR_CALIB_ROW12_LSB_TXDAC1_CAL_OVFLOW_BMSK
    *overflow_bit = HWIO_INF(QFPROM_CORR_CALIB_ROW12_LSB, TXDAC1_CAL_OVFLOW);
#endif
  }
  else
  {
    /* Invalid Chain Information: Returning default value */
    *overflow_bit = 0;
  }
}

uint32 txlm_read_foundry_register()
{
    uint32 foundry_id = 0;
 #ifdef HWIO_QFPROM_CORR_PTE2_IN
    foundry_id = (HWIO_IN(QFPROM_CORR_PTE2) & 0x700) >> 8;
 #endif
    return foundry_id;
}

uint32 rfmodem_target_get_chipset_id(void)
{
   return (uint32)RFMODEM_TARGET_MSM8996;
}
