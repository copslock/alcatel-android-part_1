/*! 
  @file
  rfcommon_msm_bbrx_adc_dc_cal_tbl.c
 
  @brief
  captures the BBRx ADC DC cal table 
 
*/

/*==============================================================================

  Copyright (c) 2014 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

==============================================================================*/

/*==============================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

  $Header: //source/qcom/qct/modem/rfmodem/bolt/main/latest/target/mdm9x45/src/rfcommon_msm_bbrx_adc_dc_cal_tbl.c

when       who     what, where, why
--------   ---     ------------------------------------------------------------- 
10/27/15   sar     Added VCM Cal support for Istari MSM8996. 
07/21/15   sar     Ported VCM Cal Support for Tesla V2.
06/02/15   sar     Removed Diag MSG to fix Standalone FW error.
05/11/15   sar     Updated get_rfcommon_msm_adc_vcm_cal_tbl to read modem ver only once.
12/16/14   vv      Initial version.
==============================================================================*/

/*=============================================================================
                           INCLUDE FILES
=============================================================================*/

#include "rflm_adc_dc_cal.h"
#include "rxlm_rxf_hal.h"

static const rfcommon_msm_adc_vcm_cal_tbl_type rfcommon_msm_adc_vcm_cal_tbl_v1[MAX_ADC_DC_CAL_V1_ENTRIES] =
{

     /*-999mv <= DC <= -260mv*/
   {
          -999,              /*start range*/                    
          -260,              /*stop_range*/                     
         0x00002400,        /*config2 reg DC value from modem*/
          0x00000000         /*config3 reg DC value from modem*/

   },

     /*-260mv <= DC <= -230mv*/
   {
          -260,              /*start range*/                    
          -230,              /*stop_range*/                     
         0x00002800,        /*config2 reg DC value from modem*/
          0x00000000         /*config3 reg DC value from modem*/

   },

     /*-230mv <= DC <= -215mv*/
   {
          -230,              /*start range*/                    
          -215,              /*stop_range*/                     
         0x00002C00,        /*config2 reg DC value from modem*/
          0x00000000         /*config3 reg DC value from modem*/ 

   },


     /*-215mv <= DC <= -200mv*/
   {
          -215,              /*start range*/                    
          -200,              /*stop_range*/                     
          0x0003000,         /*config2 reg DC value from modem*/ 
          0x00000000         /*config3 reg DC value from modem*/ 

   },

     /*-200mv <= DC <= -180mv*/
   {
          -200,              /*start range*/                    
        -180,               /*stop_range*/
          0x00003400,        /*config2 reg DC value from modem*/ 
          0x00000000         /*config3 reg DC value from modem*/ 
   },

     /*-180mv <= DC <= -160mv*/
   {
        -180,               /*start range*/
          -160,              /*stop_range*/                     
          0x00003C00,        /*config2 reg DC value from modem*/ 
          0x00000000         /*config3 reg DC value from modem*/ 

   },

     /*-160mv <= DC <= -140mv*/
   {
          -160,              /*start range*/                    
          -140,              /*stop_range*/                     
          0x00006400,        /*config2 reg DC value from modem*/ 
          0x00000000         /*config3 reg DC value from modem*/ 

   },

     /*-140mv <= DC <= -125mv*/
   {
          -140,              /*start range*/                    
          -125,              /*stop_range*/                     
          0x00006800,        /*config2 reg DC value from modem*/ 
          0x00000000         /*config3 reg DC value from modem*/ 

   },                                                          

     /*-125mv <= DC <= -110mv*/
   {                                                           
          -125,              /*start range*/                    
          -110,              /*stop_range*/                     
          0x00006000,        /*config2 reg DC value from modem*/ 
          0x00000000         /*config3 reg DC value from modem*/ 

   },                                                           

     /*-110mv <= DC <= -90mv*/
   {                                                           
          -110,              /*start range*/ 
          -90,               /*stop_range*/  
          0x00006C00,        /*config2 reg DC value from modem*/
          0x00000000         /*config3 reg DC value from modem*/

   },                                                           

     /*-90mv <= DC <= -75mv*/
   {                                                           
          -90,               /*start range*/                    
          -75,               /*stop_range*/                     
          0x00007000,        /*config2 reg DC value from modem*/
          0x00000000         /*config3 reg DC value from modem*/ 

   },                                                           

     /*-75mv <= DC <= -55mv*/
   {                                                           
          -75,               /*start range*/                    
          -55,               /*stop_range*/                     
          0x00007400,        /*config2 reg DC value from modem*/
          0x00000000         /*config3 reg DC value from modem*/ 

   },                                                           

     /*-55mv <= DC <= -40mv*/
   {                                                           
          -55,               /*start range*/                    
          -40,               /*stop_range*/                     
          0x00007800,        /*config2 reg DC value from modem*/ 
          0x00000000         /*config3 reg DC value from modem*/ 

   },                                                           

     /*-40mv <= DC <= -25mv*/
   {                                                           
        -40,                /*start range*/                    
          -25,               /*stop_range*/                     
         0x00000400,        /*config2 reg DC value from modem*/
          0x00000000         /*config3 reg DC value from modem*/ 

   },                                                           

     /*-25mv <= DC <= -10mv*/
   {                                                           
          -25,               /*start range*/                    
          -10,               /*stop_range*/                     
         0x00000800,        /*config2 reg DC value from modem*/
          0x00000000         /*config3 reg DC value from modem*/ 

   },                                                           

     /*-10mv <= DC <= 10mv*/
   {                                                           
          -10,               /*start range*/                    
           10,               /*stop_range*/                     
         0x00000000,        /*config2 reg DC value from modem*/
          0x00000000         /*config3 reg DC value from modem*/ 

   },                                                           

     /*10mv <= DC <= 30mv*/
   {                                                           
          10,                /*start range*/                    
          30,                /*stop_range*/                     
         0x00000C00,        /*config2 reg DC value from modem*/
          0x00000000         /*config3 reg DC value from modem*/ 

   },                                                           

   /*30mv <= DC <= 50mv*/
   {                                                           
          30,                /*start range*/                    
          50,                /*stop_range*/                     
         0x00001000,        /*config2 reg DC value from modem*/
          0x00000000         /*config3 reg DC value from modem*/ 

   },                                                           

   /*50mv <= DC <= 70mv*/  
   {                                                        
          50,                /*start range*/                    
          70,                /*stop_range*/                     
         0x00001400,        /*config2 reg DC value from modem*/
          0x00000000         /*config3 reg DC value from modem*/ 

   },                                                        

     /*70mv <= DC <= 90mv*/
   {                                                        
          70,                /*start range*/                    
          90,                /*stop_range*/                     
         0x00001800,        /*config2 reg DC value from modem*/
          0x00000000         /*config3 reg DC value from modem*/ 

   },                                                        

     /*90mv <= DC <= 110mv*/
   {                                                          
          90,                /*start range*/                    
          110,               /*stop_range*/                     
          0x00004800,        /*config2 reg DC value from modem*/ 
          0x00000000         /*config3 reg DC value from modem*/ 

   },                                                          

     /*110mv <= DC <= 130mv*/
   {                                                          
          110,               /*start range*/                    
          130,               /*stop_range*/                     
         0x00004000,        /*config2 reg DC value from modem*/
          0x00000000         /*config3 reg DC value from modem*/ 

   },                                                          

     /*130mv <= DC <= 150mv*/
   {                                                          
          130,               /*start range*/                    
          150,               /*stop_range*/                     
        0x00004C00,        /*config2 reg DC value from modem*/
          0x00000000         /*config3 reg DC value from modem*/ 

   },                                                          

     /*150mv <= DC <= 170mv*/
   {                                                          
          150,               /*start range*/                    
          170,               /*stop_range*/                     
         0x00005000,        /*config2 reg DC value from modem*/
          0x00000000         /*config3 reg DC value from modem*/ 

   },                                                          

     /*170mv <= DC <= 190mv*/
   {                                                          
          170,               /*start range*/                    
          190,               /*stop_range*/                     
         0x00005400,        /*config2 reg DC value from modem*/
          0x00000000         /*config3 reg DC value from modem*/ 

   },                                                          

     /*190mv <= DC <= 210mv*/
   {                                                          
          190,               /*start range*/                    
          210,               /*stop_range*/                     
         0x00005800,        /*config2 reg DC value from modem*/
          0x00000000         /*config3 reg DC value from modem*/ 

   },                                                           

     /*210mv <= DC <= 230mv*/
   {                                                           
          210,               /*start range*/                    
          230,               /*stop_range*/                     
         0x00005C00,        /*config2 reg DC value from modem*/
          0x00000000         /*config3 reg DC value from modem*/ 

   }

};

static const rfcommon_msm_adc_vcm_cal_tbl_type rfcommon_msm_adc_vcm_cal_tbl_v2[MAX_ADC_DC_CAL_V2_ENTRIES] =
   {                                                         

     /*-999mv <= DC <= -260mv*/
   {                                                         
          -999,              /*start range*/                    
          -260,              /*stop_range*/                     
          0x00002400,        /*config2 reg DC value from modem*/
          0x00000000         /*config3 reg DC value from modem*/

   },                                                         

     /*-260mv <= DC <= -230mv*/
   {                                                         
          -260,              /*start range*/                    
          -230,              /*stop_range*/                     
          0x00002800,        /*config2 reg DC value from modem*/
          0x00000000         /*config3 reg DC value from modem*/

   },

     /*-230mv <= DC <= -215mv*/
   {
          -230,              /*start range*/                    
          -215,              /*stop_range*/                     
          0x00002C00,        /*config2 reg DC value from modem*/ 
          0x00000000         /*config3 reg DC value from modem*/ 

   },


     /*-215mv <= DC <= -200mv*/
   {
          -215,              /*start range*/                    
          -200,              /*stop_range*/                     
          0x0003000,         /*config2 reg DC value from modem*/ 
          0x00000000         /*config3 reg DC value from modem*/ 

   },

     /*-200mv <= DC <= -180mv*/
   {
          -200,              /*start range*/                    
          -180,              /*stop_range*/                     
          0x00003400,        /*config2 reg DC value from modem*/ 
          0x00000000         /*config3 reg DC value from modem*/ 
   },

     /*-180mv <= DC <= -160mv*/
   {
          -180,              /*start range*/                    
          -160,              /*stop_range*/                     
          0x00003C00,        /*config2 reg DC value from modem*/ 
          0x00000000         /*config3 reg DC value from modem*/ 

   },

     /*-160mv <= DC <= -140mv*/
   {
          -160,              /*start range*/                    
          -140,              /*stop_range*/                     
          0x00006400,        /*config2 reg DC value from modem*/ 
          0x00000000         /*config3 reg DC value from modem*/ 

   },

     /*-140mv <= DC <= -125mv*/
   {
          -140,              /*start range*/                    
          -125,              /*stop_range*/                     
          0x00006800,        /*config2 reg DC value from modem*/ 
          0x00000000         /*config3 reg DC value from modem*/ 

   },

     /*-125mv <= DC <= -110mv*/
   {
          -125,              /*start range*/                    
          -110,              /*stop_range*/                     
          0x00006000,        /*config2 reg DC value from modem*/ 
          0x00000000         /*config3 reg DC value from modem*/ 

   },

     /*-110mv <= DC <= -90mv*/
   {
          -110,              /*start range*/ 
          -90,               /*stop_range*/  
          0x00006C00,        /*config2 reg DC value from modem*/
          0x00000000         /*config3 reg DC value from modem*/

   },

     /*-90mv <= DC <= -75mv*/
   {
          -90,               /*start range*/                    
          -75,               /*stop_range*/                     
          0x00007000,        /*config2 reg DC value from modem*/
          0x00000000         /*config3 reg DC value from modem*/ 

   },

     /*-75mv <= DC <= -55mv*/
   {
          -75,               /*start range*/                    
          -55,               /*stop_range*/                     
          0x00007400,        /*config2 reg DC value from modem*/
          0x00000000         /*config3 reg DC value from modem*/ 

   },

     /*-55mv <= DC <= -40mv*/
   {
          -55,               /*start range*/                    
          -40,               /*stop_range*/                     
          0x00007800,        /*config2 reg DC value from modem*/ 
          0x00000000         /*config3 reg DC value from modem*/ 

   },

     /*-40mv <= DC <= -25mv*/
   {
          -40,               /*start range*/                    
          -25,               /*stop_range*/                     
          0x00000400,        /*config2 reg DC value from modem*/
          0x00000000         /*config3 reg DC value from modem*/ 

   },

     /*-25mv <= DC <= -10mv*/
   {
          -25,               /*start range*/                    
          -10,               /*stop_range*/                     
          0x00000800,        /*config2 reg DC value from modem*/
          0x00000000         /*config3 reg DC value from modem*/ 

   },

   /*-10mv   <= DC <=  10mv*/
   {                                                           
        -10,            /*start range*/                      
         10,            /*stop_range*/                      
        0x00000000,     /*config2 reg DC value from modem*/ 
          0x00000000         /*config3 reg DC value from modem*/ 

   },                                                           

   /*10mv   <= DC <=  30mv*/
   {                                                           
        10,             /*start range*/                     
        30,             /*stop_range*/                      
          0x00000C00,        /*config2 reg DC value from modem*/ 
          0x00000000         /*config3 reg DC value from modem*/ 

   },                                                           

   /*30mv   <= DC <=  50mv*/
   {                                                           
        30,             /*start range*/                     
        50,             /*stop_range*/                      
          0x00001000,        /*config2 reg DC value from modem*/ 
          0x00000000         /*config3 reg DC value from modem*/ 

   },                                                           

   /*50mv   <= DC <=  70mv*/
   {                                                           
        50,            /*start range*/                      
        70,            /*stop_range*/                      
          0x00001400,        /*config2 reg DC value from modem*/ 
          0x00000000         /*config3 reg DC value from modem*/ 

   },                                                           

   /*70mv   <= DC <=  90mv*/
   {                                                        
        70,            /*start range*/                      
        90,            /*stop_range*/                      
          0x00001800,        /*config2 reg DC value from modem*/ 
          0x00000000         /*config3 reg DC value from modem*/ 

   },                                                        

   /*90mv   <= DC <=  110mv*/
   {                                                        
        90,            /*start range*/                      
        110,           /*stop_range*/                      
          0x00004800,        /*config2 reg DC value from modem*/ 
          0x00000000         /*config3 reg DC value from modem*/ 

   },                                                        

     /*110mv <= DC <= 130mv*/
   {                                                        
        110,          /*start range*/                        
          130,               /*stop_range*/                     
        0x00004000,   /*config2 reg DC value from modem*/ 
          0x00000000         /*config3 reg DC value from modem*/ 

   },                                                          

     /*130mv <= DC <= 150mv*/
   {                                                          
          130,               /*start range*/                    
          150,               /*stop_range*/                     
        0x00004C00,        /*config2 reg DC value from modem*/
          0x00000000         /*config3 reg DC value from modem*/ 

   },                                                          

     /*150mv <= DC <= 170mv*/
   {                                                          
          150,               /*start range*/                    
          170,               /*stop_range*/                     
         0x00005000,        /*config2 reg DC value from modem*/
          0x00000000         /*config3 reg DC value from modem*/ 

   },                                                          

     /*170mv <= DC <= 190mv*/
   {                                                          
          170,               /*start range*/                    
          190,               /*stop_range*/                     
         0x00005400,        /*config2 reg DC value from modem*/
          0x00000000         /*config3 reg DC value from modem*/ 

   },                                                          

     /*190mv <= DC <= 210mv*/
   {                                                          
          190,               /*start range*/                    
          210,               /*stop_range*/                     
         0x00005800,        /*config2 reg DC value from modem*/
          0x00000000         /*config3 reg DC value from modem*/ 

   },                                                           

     /*210mv <= DC <= 230mv*/
   {                                                           
          210,               /*start range*/                    
          230,               /*stop_range*/                     
         0x00005C00,        /*config2 reg DC value from modem*/
          0x00000000         /*config3 reg DC value from modem*/ 

   }

};

boolean get_rfcommon_msm_adc_vcm_cal_tbl(const rfcommon_msm_adc_vcm_cal_tbl_type **rfcommon_msm_adc_vcm_cal_tbl_p)
{
    static uint32 modem_version = 0;
    static boolean modem_version_read = FALSE;

    if (FALSE == modem_version_read )
    { 
    modem_version = rxlm_hal_rxf_get_modem_version();
       modem_version_read = TRUE;
    }

    switch (modem_version & 0xffffff00) 
    {
    case 0x30000100: /* MDM9x45 (Tesla) v1*/
       *rfcommon_msm_adc_vcm_cal_tbl_p = (const rfcommon_msm_adc_vcm_cal_tbl_type*)rfcommon_msm_adc_vcm_cal_tbl_v1;
       break;

    case 0x30000200: /* MDM9x45 (Tesla) v2 */
       *rfcommon_msm_adc_vcm_cal_tbl_p = (const rfcommon_msm_adc_vcm_cal_tbl_type*)rfcommon_msm_adc_vcm_cal_tbl_v2;
       break;

    default:
       *rfcommon_msm_adc_vcm_cal_tbl_p =  NULL;
       return FALSE;
    }

   return TRUE;

}

uint32 get_rfcommon_msm_adc_cal_tbl_size(void)
{
    uint32 cal_tbl_size = 0;
    static uint32 modem_version = 0;
    static boolean modem_version_read = FALSE;

    if (FALSE == modem_version_read )
    { 
       modem_version = rxlm_hal_rxf_get_modem_version();
       modem_version_read = TRUE;
    }

    switch (modem_version & 0xffffff00) 
    {
    case 0x30000100: /* M8996 (Istari) v1.0*/
       cal_tbl_size = MAX_ADC_DC_CAL_V1_ENTRIES ;
       break;

    case 0x30000200: /* M8996 (Istari)  v2.X */
       cal_tbl_size = MAX_ADC_DC_CAL_V2_ENTRIES ;
       break;

    default:
       break;
    }

   return cal_tbl_size;

}
