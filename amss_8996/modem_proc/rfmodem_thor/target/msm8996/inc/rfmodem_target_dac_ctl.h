#ifndef RFMODEM_THOR_TARGET_DAC_CTL_H
#define RFMODEM_THOR_TARGET_DAC_CTL_H

/*!
   @file
   rfmodem_target_dac_ctl.h

   @brief
   This file implements the Target Specific DAC specifications. These are platform
   dependent.

*/

/*===========================================================================

Copyright (c) 2010 - 2015 by Qualcomm Technologies, Inc.  All Rights Reserved.

                           EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$DateTime: 2016/03/28 23:07:43 $ $Author: mplcsds1 $
$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/rfmodem_thor/target/msm8996/inc/rfmodem_target_dac_ctl.h#1 $

when       who     what, where, why
------------------------------------------------------------------------------- 
07/22/15   dej     Set ETDAC_RTUNE_CODE_SW_OFFSET to 0 for Istari
04/14/15   dej     Initial version.
============================================================================*/

/*===========================================================================
                           INCLUDE FILES
===========================================================================*/

#ifdef _cplusplus
extern "C" {
#endif

#include "txlm_intf.h"

#define DAC_PWR_ON_CLK_DIV_PGM		2
#define DAC_PWR_ON_CLK_PLL_SEL		0
#define DAC_PWR_ON_CLK_DAC1_CLK_SRC	0

#define DAC_PWRUP_TXDAC_CFG0   0x02A2211B
#define DAC_PWRUP_TXDAC_CFG1   0x8004D140
#define DAC_PWRUP_TXDAC_CFG2   0xF1E405AE
#define DAC_PWRUP_TXDAC_CFG3   0x0C

#define DAC_PWRUP_ETDAC_CFG0   0x02260224
#define DAC_PWRUP_ETDAC_CFG1   0x8000D148
#define DAC_PWRUP_ETDAC_CFG2   0xF00605AC
#define DAC_PWRUP_ETDAC_CFG3   0x00

#define DAC_MSBCAL_TXDAC_CFG0  0x02A2211B
#define DAC_MSBCAL_TXDAC_CFG1  0x8004D144
#define DAC_MSBCAL_TXDAC_CFG2  0xF1E405AE
#define DAC_MSBCAL_TXDAC_CFG3  0x0C

#define DAC_DCCAL_TXDAC_CFG0   0x02A2011B
#define DAC_DCCAL_TXDAC_CFG1   0x8004D044
#define DAC_DCCAL_TXDAC_CFG2   0xF16C04AC
#define DAC_DCCAL_TXDAC_CFG3   0x0C

#define DAC_MSBCAL_ETDAC_CFG0  0x02240224
#define DAC_MSBCAL_ETDAC_CFG1  0x8000D108
#define DAC_MSBCAL_ETDAC_CFG2  0xF00205AC
#define DAC_MSBCAL_ETDAC_CFG3  0x00

#define DAC_MSBCAL_REGARRAY_MASK   0x3F
#define DAC_MSBCAL_REGARRAY_MASK_0 0x3F
#define DAC_MSBCAL_REGARRAY_MASK_1 0xFC0
#define DAC_MSBCAL_REGARRAY_MASK_2 0x3F00
#define DAC_MSBCAL_REGARRAY_MASK_3 0xFC000
#define DAC_MSBCAL_REGARRAY_SHFT_0 0
#define DAC_MSBCAL_REGARRAY_SHFT_1 6
#define DAC_MSBCAL_REGARRAY_SHFT_2 12
#define DAC_MSBCAL_REGARRAY_SHFT_3 18

#define DAC_DCCAL_REGARRAY_MASK    0x7F
#define DAC_DCCAL_REGARRAY_MASK_0  0x7F
#define DAC_DCCAL_REGARRAY_MASK_1  0x3F80
#define DAC_DCCAL_REGARRAY_SHFT_0  0
#define DAC_DCCAL_REGARRAY_SHFT_1  7

#define TXDAC_MSBCAL_SIZE 142
#define TXDAC_DC_CAL_SIZE 0
#define TXDAC_CAL_DATA_SIZE (TXDAC_MSBCAL_SIZE+TXDAC_DC_CAL_SIZE)
#define ETDAC_MSBCAL_SIZE 71
#define ETDAC_CAL_DATA_SIZE (ETDAC_MSBCAL_SIZE)

#define DAC_FCAL_CLK_10_CYCLES							1
#define DAC_PWR_ON_TX_ALL_DONE_WAIT_US					50  /* Actual 11us - Safe Margin added */
#define TXDAC_PWR_ON_MSBCAL_MEM_WR_DONE_WAIT_US			50	/* actual 568 XO Cycles - Safe Margin added */
#define ETDAC_PWR_ON_MSBCAL_MEM_WR_DONE_WAIT_US			50  /* Actual 284 XO Cycles - Safe Margin added */
#define TXDAC_PWR_ON_DCCAL_MEM_WR_DONE_WAIT_US			50	/* Actual 8 XO Cycles - Safe Margin added */

#define ETDAC_RTUNE_CODE_SW_OFFSET						0 	/* DAC chip recommended value */




#ifdef _cplusplus
}
#endif

#endif /* RFMODEM_THOR_TARGET_DAC_CTL_H */

