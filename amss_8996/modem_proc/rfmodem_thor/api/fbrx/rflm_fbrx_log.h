
/*!
  @file
  rflm_fbrx_log.h

  @brief
  RF LOG API definitions for FBRX to interact with SW and LM
  
  @detail
  This file will contain all definitios and declarations to be
  shared with SW and LM for FBRX LM Logging
 
*/

/*==============================================================================

  Copyright (c) 2014 Qualcomm Technologies, Inc. (QTI). All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

==============================================================================*/

/*==============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/rfmodem_thor/api/fbrx/rflm_fbrx_log.h#1 $

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
09/16/15   aa     Changes for FBRX 0x184A V2 Logging support 
07/16/15   aa     Changes for FBRX 0x1849 V6 Logging support 
06/12/15   sub    Added support for new fbrx log packet 0x18F7 
08/20/14   aa     Changes for FBRX 0x1849 V4 Logging support 
07/24/14   aa     [1] Added FBRX results log packet 0x1849 v3 revision changes
                  [2] Added support for new sample capture log packet 0x184A
06/06/14   aa      Added new log version 2 
04/14/14   aa      Update log structure for version control
04/03/14   ka      Update log structure
03/18/14   aa      Update exp gain data results into buffer
03/12/14   aa      Latest FBRx changes for ILPC correction
03/04/14   aa      Initial Version - Support for FBRX Logging


==============================================================================*/

#ifndef RFLM_FBRX_LOG_H
#define RFLM_FBRX_LOG_H


/*==============================================================================

                           INCLUDE FILES

==============================================================================*/

#include "rflm.h"
#include "rflm_api_fbrx.h"
#include "rflm_fbrx.h"
#include "rflm_diag_log.h"
#include "log.h"

/*==============================================================================

                EXTERNAL DEFINITIONS AND TYPES : MACROS

==============================================================================*/

/************************************************************/
/*                LOG PACKET IDENTIFICATION                 */
/************************************************************/

/*----------------------------------------------------------------------------*/
/*! @brief RFLM FBRx log packet ID */
#define RFLM_LOG_PACKET_FBRX_RESULTS_ID                 0x1849 
#define RFLM_LOG_PACKET_FBRX_CAPTURE_ID                 0x184A 
#define RFLM_LOG_PACKET_FBRX_BASIC_RESULTS_ID           0x18F7 

/*----------------------------------------------------------------------------*/
/*! @brief RFLM FBRx user ID */
#define RFLM_LOG_PACKET_RAT_FBRX_RESULTS                0
#define RFLM_LOG_PACKET_RAT_FBRX_CAPTURE                1
#define RFLM_LOG_PACKET_RAT_FBRX_BASIC_RESULTS          2

/*----------------------------------------------------------------------------*/
/*! @brief RFLM FBRx tag size */
#define RFLM_LOG_PACKET_FBRX_RESULTS_TAG_SIZE_WORDS     4 /* Should be multiple of 4 */
#define RFLM_LOG_PACKET_FBRX_CAPTURE_TAG_SIZE_WORDS     4 /* Should be multiple of 4 */
#define RFLM_LOG_PACKET_FBRX_BASIC_RESULTS_TAG_SIZE_WORDS   4 /* Should be multiple of 4 */

/*----------------------------------------------------------------------------*/

#define RFLM_FBRX_MAX_TX_SAMPLE_SIZE 391
#define RFLM_FBRX_MAX_RX_SAMPLE_SIZE 391
#define RFLM_FBRX_MAX_SAMPLE_SIZE 896
#define RFLM_FBRX_LIN_SIZE 64

#define RFLM_FBRX_VALID_TIME_MASK 0x3FFFFF

/************************************************************/
/*                   LOG PACKET VERSIONS                    */
/************************************************************/

/*----------------------------------------------------------------------------*/
/*!@brief Enumeration indicating the FBRx Log Packet version */
typedef enum
{
  RFLM_FBRX_LOG_VER_1 = 1, /*!< FBRx Log Packet Version 1 */
  RFLM_FBRX_LOG_VER_2 = 2, /*!< FBRx Log Packet Version 2 */
  RFLM_FBRX_LOG_VER_3 = 3, /*!< FBRx Log Packet Version 3: Unused */
  RFLM_FBRX_LOG_VER_4 = 4, /*!< FBRx Log Packet Version 4 */
  RFLM_FBRX_LOG_VER_5 = 5, /*!< FBRx Log Packet Version 5: Unused */
  RFLM_FBRX_LOG_VER_6 = 6, /*!< FBRx Log Packet Version 6 */
} rflm_fbrx_log_version_type;

/*----------------------------------------------------------------------------*/
/*!@brief Enumeration indicating the FBRx Sample Capture type */
typedef enum
{
  RFLM_FBRX_LOG_TX = 0, /*!< TX Sample Capture */
  RFLM_FBRX_LOG_RX = 1, /*!< RX Sample Capture */
} rflm_fbrx_log_sample_capture_type;

/*----------------------------------------------------------------------------*/
/*==============================================================================

              EXTERNAL DEFINITIONS AND TYPES : ENUMS

==============================================================================*/


/*==============================================================================

            EXTERNAL DEFINITIONS AND TYPES : STRUCTURES

==============================================================================*/

/************************************************************/
/*              LOG PACKET PAYLOAD DEFINITION               */
/************************************************************/

/*----------------------------------------------------------------------------*/
/*! @brief  FBRX Log packet structure Version 1 */

typedef struct
{
  uint8  rflm_tech;         /*!< rflm tech id ( 0=1X, 1=HDR, 2=LTE, 3=WCDMA, 4=TDSCDMA, 5=GSM ) */
  uint8  bandwidth;         /*!< rfcommon_fbrx_tx_cfg_type tech bandwidth */
  uint8  fbrx_mode;         /*!< flag indicating the fbrx mode (0,1,2 or 3) */
  uint8  gain_state;        /*!< flag indicating the fbrx gain state (0,1,2 or 3) */
  uint8  slot_idx;          /*!< flag indicating the slot index (0,1, or 2) per SF for current fbrx run */ 
  uint8  pa_idx;            /*!< flag indicating the PA State */ 
  uint8  rgi_idx;           /*!< flag indicating the RGI row index */ 
  int16  temp_comp;         /*!< Temp Comp value for current tech/band/chan/mode/gs */

  int16  exp_cal_gain;      /*!< FBRx Expected Cal Gain value */
  uint8  ubias_new;         /*!< Updated ubias value for current tech/band/chan/mode/gs */
  uint16 rxscale_new;       /*!< Updated Rx Scale value for current tech/band/chan/mode/gs */
  int16  gain_est;          /*!< Current Gain estimate result */
  int16  next_gain;         /*!< Next Gain estimate result */

  int16  last_txagc;        /*!< Current TxAGC value */
  int16  delta_prx;         /*!< Updated Tx power in dB10 for current tech/band/chan/mode/gs */
  int16  gain_error_db10;   /*!< The FBRx Gain Error in dB10 uint for current txagc */
  int16  filt_error_db10;   /*!< The FBRx Filtered Gain Error in dB10 uint for current txagc */

  uint32 cgain_est;         /*!< complex gain estimate result */
  uint32 ls_err;            /*!< complex least squared error estimate result */
  int32  model_a;           /*!< model A param estimate result */
  int32  model_b;           /*!< model B param estimate result */
  int32  model_c;           /*!< model C param estimate result */

  int32  loft;              /*!< Current LOFT compensation */
  int32  next_loft;         /*!< Next LOFT compensation estimate */
  int32  rsb;               /*!< Current Residual Side Band estimate */
  int32  next_rsb;          /*!< Next Residual Side Band estimate */
  int32  gain_imbalance;    /*!< Current Gain imbalance estimate */
  int32  phase_imbalance;   /*!< Current Phase imbalance estimate */
  uint16 xcorr_log[RFLM_FBRX_XCORR_LOG_SIZE];  /*!< Cross correlation results */

}rflm_fbrx_log_packet_v1;

/* Sort the data into 32bits field */
typedef struct
{
  uint8  rflm_tech;         /*!< rflm tech id ( 0=1X, 1=HDR, 2=LTE, 3=WCDMA, 4=TDSCDMA, 5=GSM ) */
  uint8  bandwidth;         /*!< rfcommon_fbrx_tx_cfg_type tech bandwidth */
  uint8  ftm_cal_mode;      /*!< flag indicating the RF Cal mode (0 =FALSE, 1=TRUE) */ 
  uint8  fbrx_mode;         /*!< flag indicating the fbrx mode (0,1,2 or 3) */

  uint8  gain_state;        /*!< flag indicating the fbrx gain state (0,1,2 or 3) */
  uint8  slot_idx;          /*!< flag indicating the slot index (0,1, or 2) per SF for current fbrx run */ 
  uint8  pa_idx;            /*!< flag indicating the PA State */ 
  uint8  rgi_idx;           /*!< flag indicating the RGI row index */ 

  int16  temp_comp;         /*!< Temp Comp value for current tech/band/chan/mode/gs */
  int16  exp_cal_gain;      /*!< FBRx Expected Cal Gain value */

  uint16 ubias_new;         /*!< Updated ubias value for current tech/band/chan/mode/gs */
  uint16 rxscale_new;       /*!< Updated Rx Scale value for current tech/band/chan/mode/gs */

  int16  gain_est;          /*!< Current Gain estimate result */
  int16  next_gain;         /*!< Next Gain estimate result */

  int16  txagc;             /*!< Current TxAGC value */
  int16  delta_prx;         /*!< Updated Tx power in dB10 for current tech/band/chan/mode/gs */

  int16  gain_error_db10;   /*!< The FBRx Gain Error in dB10 uint for current txagc */
  int16  filt_error_db10;   /*!< The FBRx Filtered Gain Error in dB10 uint for current txagc */

  uint32 cgain_est;         /*!< complex gain estimate result */

  uint32 ls_err;            /*!< complex least squared error estimate result */

  int32  model_a;           /*!< model A param estimate result */

  int32  model_b;           /*!< model B param estimate result */

  int32  model_c;           /*!< model C param estimate result */

  int32 loft;               /*!< Current LOFT compensation */

  int32 next_loft;          /*!< Next LOFT compensation estimate */

  int32 rsb;                /*!< Current Residual Side Band estimate */

  int32 next_rsb;           /*!< Next Residual Side Band estimate */

  int32 gain_imbalance;     /*!< Current Gain imbalance estimate */

  int32 phase_imbalance;    /*!< Current Phase imbalance estimate */

  uint16 xcorr_log[RFLM_FBRX_XCORR_LOG_SIZE];  /*!< Cross correlation results */

}rflm_fbrx_log_packet_v2;


/* Sort the data into 32bits field */
typedef struct
{
  int16  txagc;         /*!< Current TxAGC value in dB10 */
  int16  gn_est;        /*!< Current Gain estimate result */

  int16  gn_err;        /*!< The FBRx Gain Error in dB10 uint for current txagc */
  int16  ft_err;        /*!< The FBRx Filtered Gain Error in dB10 uint for current txagc */

  uint32 ls_err;        /*!< complex least squared error estimate result */

  uint16 nxt_gn;        /*!< BB IQ gain or Env Scale from uK */
  uint16 cur_gn;        /*!< BB IQ gain or Env Scale from tech TXAGC */

  uint16 t_gn_h;        /*!< Nominal BB IQ gain or Envelope scale value */
  uint16 t_gn_l;        /*!< BB IQ gain or Envelope scale low threshold value */

  int16  cal_pwr;       /*!< FBRx Cal Power for current gain state */
  int16  cal_gn;        /*!< FBRx Expected Cal Gain value */

  int16  d_prx;         /*!< The delta in Tx power in dB10 units for the current TxAGC from the FBRX Cal point or power for current gain state */  
  int16  l_all_r;       /*!< Flag indicating if single RGI update or ALL RGIs were udpated */  

  uint16 l_pa;          /*!< PA state for which lin is updated */  
  uint16 l_rgi;         /*!< rgi row index for which lin is updated */  

  int16  l_old;         /*!< txagc value prior to lin update */  
  int16  l_new;         /*!< new txagc value after lin update */  

  int16  temp;          /*!< Temp Comp value for current tech/band/chan/mode/gs */
  uint16 ubias;         /*!< Updated ubias value for current tech/band/chan/mode/gs */

  uint16 rxscale;       /*!< Updated Rx Scale value for current tech/band/chan/mode/gs */
  uint16 txscale;       /*!< Updated Tx Scale value for current tech/band/chan/mode/gs */

  uint8  tech;          /*!< rflm tech id ( 0=1X, 1=HDR, 2=LTE, 3=WCDMA, 4=TDSCDMA, 5=GSM ) */
  uint8  bw;            /*!< rfcommon_fbrx_tx_cfg_type tech bandwidth */
  uint8  fb;            /*!< flag indicating the fbrx mode (0,1,2 or 3) */
  uint8  gs;            /*!< flag indicating the fbrx gain state (0,1,2 or 3) */

  uint8  slot;          /*!< flag indicating the slot index (0,1, or 2) per SF for current fbrx run */ 
  uint8  pa;            /*!< flag indicating the PA State */ 
  uint8  rgi;           /*!< flag indicating the RGI row index */ 
  uint8  iq;            /*!< flag indicating if IQ gain or Envelope scale */ 

  uint8  cal;           /*!< flag indicating the RF Cal mode (0 =FALSE, 1=TRUE) */ 
  uint8  bnd;           /*!< flag indicating the current Band of type (rfcom_band_type_u) */ 
  uint8  nv;            /*!< flag indicating the FBRX SYS common NV control value for current tech */ 
  uint8  fw;            /*!< flag indicating if FW aborted FBRX due to timeline limitation */

  uint16 fw_cnt;        /*!< Number of valid FBRX skips from bootup */
  uint16 bsc_i;         /*!< flag indicating the bad sample capture index */ 

  uint32 effect_t;      /*!< Time at which the FBRx LDO was last turned on */

  uint32 start_t;       /*!< Time at which CMN FW starts the FBRx */

  uint32 end_t;         /*!< Time at which CMN FW stops FBRX */

  uint32 cgain_est;     /*!< complex gain estimate result */

  int32  model_a;       /*!< model A param estimate result */

  int32  model_b;       /*!< model B param estimate result */

  int32  model_c;       /*!< model C param estimate result */

  int32  loft;          /*!< Current LOFT compensation */

  int32  next_loft;     /*!< Next LOFT compensation estimate */

  int32  rsb;           /*!< Current Residual Side Band estimate */

  int32  next_rsb;      /*!< Next Residual Side Band estimate */

  int32  gi;            /*!< Current Gain imbalance estimate */

  int32  pi;            /*!< Current Phase imbalance estimate */

  uint16 xcorr_log[RFLM_FBRX_XCORR_LOG_SIZE];  /*!< Cross correlation results */

  int16  lin_data[RFLM_FBRX_LIN_SIZE]; /*!< Linearizer data during cur fbrx run for cur PA state */

}rflm_fbrx_log_packet_v4;


/* Sort the data into 32bits field */
typedef struct
{
  int16  txagc;         /*!< Current TxAGC value in dB10 */
  int16  gn_est;        /*!< Current Gain estimate result */

  int16  gn_err;        /*!< The FBRx Gain Error in dB10 uint for current txagc */
  int16  ft_err;        /*!< The FBRx Filtered Gain Error in dB10 uint for current txagc */

  uint32 ls_err;        /*!< complex least squared error estimate result */

  uint16 nxt_gn;        /*!< BB IQ gain or Env Scale from uK */
  uint16 cur_gn;        /*!< BB IQ gain or Env Scale from tech TXAGC */

  uint16 t_gn_h;        /*!< Nominal BB IQ gain or Envelope scale value */
  uint16 t_gn_l;        /*!< BB IQ gain or Envelope scale low threshold value */

  int16  cal_pwr;       /*!< FBRx Cal Power for current gain state */
  int16  cal_gn;        /*!< FBRx Expected Cal Gain value */

  int16  d_prx;         /*!< The delta in Tx power in dB10 units for the current TxAGC from the FBRX Cal point or power for current gain state */  
  int16  l_all_r;       /*!< Flag indicating if single RGI update or ALL RGIs were udpated */  

  uint16 l_pa;          /*!< PA state for which lin is updated */  
  uint16 l_rgi;         /*!< rgi row index for which lin is updated */  

  int16  l_old;         /*!< txagc value prior to lin update */  
  int16  l_new;         /*!< new txagc value after lin update */  

  int16  temp;          /*!< Temp Comp value for current tech/band/chan/mode/gs */
  uint16 ubias;         /*!< Updated ubias value for current tech/band/chan/mode/gs */

  uint16 rxscale;       /*!< Updated Rx Scale value for current tech/band/chan/mode/gs */
  uint16 txscale;       /*!< Updated Tx Scale value for current tech/band/chan/mode/gs */

  uint8  tech;          /*!< rflm tech id ( 0=1X, 1=HDR, 2=LTE, 3=WCDMA, 4=TDSCDMA, 5=GSM ) */
  uint8  bw;            /*!< rfcommon_fbrx_tx_cfg_type tech bandwidth */
  uint8  fb;            /*!< flag indicating the fbrx mode (0,1,2 or 3) */
  uint8  gs;            /*!< flag indicating the fbrx gain state (0,1,2 or 3) */

  uint8  slot;          /*!< flag indicating the slot index (0,1, or 2) per SF for current fbrx run */ 
  uint8  pa;            /*!< flag indicating the PA State */ 
  uint8  rgi;           /*!< flag indicating the RGI row index */ 
  uint8  iq;            /*!< flag indicating if IQ gain or Envelope scale */ 

  uint8  cal;           /*!< flag indicating the RF Cal mode (0 =FALSE, 1=TRUE) */ 
  uint8  bnd;           /*!< flag indicating the current Band of type (rfcom_band_type_u) */ 
  uint8  nv;            /*!< flag indicating the FBRX SYS common NV control value for current tech */ 
  uint8  fw;            /*!< flag indicating if FW aborted FBRX due to timeline limitation */

  uint16 fw_cnt;        /*!< Number of valid FBRX skips from bootup */
  uint16 bsc_i;         /*!< flag indicating the bad sample capture index */ 

  uint32 effect_t;      /*!< Time at which the FBRx LDO was last turned on */

  uint32 start_t;       /*!< Time at which CMN FW starts the FBRx */

  uint32 end_t;         /*!< Time at which CMN FW stops FBRX */

  uint32 cgain_est;     /*!< complex gain estimate result */

  int32  model_a;       /*!< model A param estimate result */

  int32  model_b;       /*!< model B param estimate result */

  int32  model_c;       /*!< model C param estimate result */

  int32  loft;          /*!< Current LOFT compensation */

  int32  next_loft;     /*!< Next LOFT compensation estimate */

  int32  rsb;           /*!< Current Residual Side Band estimate */

  int32  next_rsb;      /*!< Next Residual Side Band estimate */

  int32  gi;            /*!< Current Gain imbalance estimate */

  int32  pi;            /*!< Current Phase imbalance estimate */

  uint32 tx_h;          /*!< TX handle info to distinguish between two runs for dual TX chain concurrency scenarios for FBRx */

  uint32 trig_r;        /*!< FBRx trigger rate in us */

  uint32 req_id;        /*!< FBRX Capture Request ID returned by SW for current FBRx trigger */

  uint32 res_id;        /*!< FBRX Capture Response ID returned by CFW for current results  */

  uint8  meas_t;        /*!< Specifies the FBRX Measurement type of rflm_fbrx_meas_req_type_e */
  uint8  a_pin;         /*!< Auto Pin processing measurement type indicator */
  uint8  max_i;         /*!< Specifies the maximum Xcorr index value */
  uint8  p_ramp;        /*!< Flag to specify if Power ramp is currently active */

  int16  err_h;         /*!< Error high from switch point based capture for current WTR */
  int16  err_l;         /*!< Error high from switch point based capture for current WTR */

  int16  err_r;         /*!< Error high from switch point based capture for current WTR */
  uint16 res_a;         /*!< Reserved field for future/debug purpose */

  uint32 tx_acor;       /*!< Acor: R_sxs* */

  uint32 rx_acor;       /*!< FBRx Autocor: R_yxy* */

  uint32 rx_sum;        /*!< Xcor: Sum of Received Data: R_yx1*/

  int32  gain_cc0;      /*!< Digitial gain for ca0  */

  int32  gain_cc1;      /*!< Digitial gain for ca1  */

  int32  freq_cc0;      /*!< Freq for ca0  */

  int32  freq_cc1;      /*!< Freq for ca1  */

  int32  phase_cc1;     /*!< Phase offset for ca1 wrt ca0  */

  int32  res_b;         /*!< Reserved field for future/debug purpose */

  uint16 xcorr_log[RFLM_FBRX_XCORR_LOG_SIZE];  /*!< Cross correlation results */

  int16  lin_data[RFLM_FBRX_LIN_SIZE]; /*!< Linearizer data during cur fbrx run for cur PA state */

}rflm_fbrx_log_packet_v6;

/*----------------------------------------------------------------------------*/

/*!  @brief FBRx log packet structure */
/// @log_id 0x1849
typedef struct
{
  uint8  log_version;           /*!< Log Packet Version */
  uint8  num_sub_packets;       /*!< Number of Sub-Packets in each log packet */   
  int16  reserved;              /*!< Reserved 16 bit field */

  /// @descriminator log_version
  union  rflm_fbrx_log_packet_versions
  {
    /// @condition 1
    rflm_fbrx_log_packet_v1 v1;   /*!< Log Packet Version 1 */
    /// @condition 2
    rflm_fbrx_log_packet_v2 v2;   /*!< Log Packet Version 2 */
    /// @condition 4
    rflm_fbrx_log_packet_v4 v4;   /*!< Log Packet Version 4 */
    /// @condition 6
    rflm_fbrx_log_packet_v6 v6;   /*!< Log Packet Version 4 */
  } versions;

}rflm_fbrx_log_packet_t;

/*----------------------------------------------------------------------------*/

/************************************************************/
/*              SAMPLE CAPTURE PACKET PAYLOAD DEFINITION               */
/************************************************************/

/*----------------------------------------------------------------------------*/
/*! @brief  FBRX sample_capture_packet packet structure Version 1 */

typedef struct
{
  uint32 bad_capture_idx;    /*!< flag indicating the bad sample capture index */ 
  uint32 tx_samples[RFLM_FBRX_MAX_TX_SAMPLE_SIZE];  /*!< TX Samples */
  uint32 rx_samples[RFLM_FBRX_MAX_RX_SAMPLE_SIZE];  /*!< RX samples */

}rflm_fbrx_sample_capture_packet_v1;

/*! @brief  FBRX sample_capture_packet packet structure Version 2 */

typedef struct
{
  uint32 bsc_idx;       /*!< flag indicating the bad sample capture index */ 
  uint32 fbrx_mode;     /*!< FBRx Mode */
  uint32 sc_type;       /*!< 0 =TX samples, 1=RX samples */
  uint32 sc_len;        /*!< TX/RX valid sample capture length */
  uint32 samples[RFLM_FBRX_MAX_SAMPLE_SIZE];  /*!< TX/RX Samples */
}rflm_fbrx_sample_capture_packet_v2;


/*!  @brief FBRx Sample Captures structure */
/// @log_id 0x184A
typedef struct
{
  uint8  log_version;           /*!< Log Packet Version */
  uint8  num_sub_packets;       /*!< Number of Sub-Packets in each log packet */   
  int16  reserved;              /*!< Reserved 16 bit field */

  /// @descriminator log_version
  union  rflm_fbrx_sample_captures_versions
  {
    /// @condition 1
    rflm_fbrx_sample_capture_packet_v1 v1;   /*!< FBRX Sample Capture Packet Version 1 */
    /// @condition 2
    rflm_fbrx_sample_capture_packet_v2 v2;   /*!< FBRX Sample Capture Packet Version 2 */
  } versions;

}rflm_fbrx_sample_captures_t;

/*----------------------------------------------------------------------------*/
/************************************************************/
/*              BASIC FBRX RESULTS PACKET PAYLOAD DEFINITION               */
/************************************************************/
/*----------------------------------------------------------------------------*/
/*! @brief  FBRX Basic Log packet structure Version 1 */

typedef struct
{
  int16  txagc;        /*!< Current TxAGC value */
  int16  gn_err;       /*!< The FBRx Gain Error in dB10 uint for current txagc */
  uint32 ls_err;       /*!< complex least squared error estimate result */
}rflm_fbrx_log_results_basic_packet_v1;


/*!  @brief FBRx Basic Results structure */
/// @log_id 0x18F7
typedef struct
{
  uint8   log_version;           /*!< Log Packet Version */
  uint8   reserved;              /*!< Reserved 8 bit field */
  uint16  num_sub_packets;       /*!< Number of Sub-Packets in each log packet */   

  /// @descriminator log_version
  union  rflm_fbrx_basic_log_packet_versions
  {
    /// @condition 1
    rflm_fbrx_log_results_basic_packet_v1 v1;   /*!< FBRX Basic Results Packet Version 1 */

  } versions;

}rflm_fbrx_basic_log_packet_t;

/*==============================================================================

                    EXTERNAL FUNCTION PROTOTYPES

==============================================================================*/

#ifdef __cplusplus
extern "C" {
#endif

/*==============================================================================
              API input/output function definitions -- FBRX 
==============================================================================*/

/************************************************************/
/*           DIAG LOG SUBPACKET API DEFINITIONS             */
/************************************************************/

/*----------------------------------------------------------------------------*/

void
rflm_fbrx_results_diag_log_subpacket
(
  rflm_fbrx_dm_template_t* fbrx_data_ptr, 
  uint8 run_idx 
);

void
rflm_fbrx_captures_diag_log_subpacket
(
  rflm_fbrx_dm_template_t* fbrx_data_ptr, 
  uint8 run_idx,
  rflm_fbrx_log_sample_capture_type sample_type
);

void
rflm_fbrx_basic_results_diag_log_subpacket
(
  rflm_fbrx_dm_template_t* fbrx_data_ptr, 
  uint8 run_idx 
);

/*----------------------------------------------------------------------------*/

#ifdef __cplusplus
} // extern "C"
#endif

#endif /* RFLM_FBRX_LOG_H */
