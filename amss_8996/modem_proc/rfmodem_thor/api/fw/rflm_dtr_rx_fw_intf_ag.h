
#ifndef RFLM_DTR_RX_FW_INTF_AG_H
#define RFLM_DTR_RX_FW_INTF_AG_H


#ifdef __cplusplus
extern "C" {
#endif

/*
WARNING: This file is auto-generated.

Generated at:    Tue Dec  1 12:40:26 2015
Generated using: lm_autogen.exe v4.0.51
Generated from:  v4.9.XX of Bolt_RXFE_register_settings.xlsx
*/

/*=============================================================================

           R X    A U T O G E N    F I L E

GENERAL DESCRIPTION
  This file is auto-generated and it captures the modem register settings 
  configured by FW, provided by the rflm_dtr_rx.

Copyright (c) 2009, 2010, 2011, 2012, 2013, 2014, 2015 by Qualcomm Technologies, Inc.  All Rights Reserved.

$DateTime: 2016/03/28 23:07:43 $
$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/rfmodem_thor/api/fw/rflm_dtr_rx_fw_intf_ag.h#1 $

=============================================================================*/


/*=============================================================================
                           INCLUDE FILES
=============================================================================*/

#include "comdef.h" 



/*==============================================================================

Interface Revision Information

==============================================================================*/

#define RFLM_DTR_RX_MAJOR_REV 4
#define RFLM_DTR_RX_FW_INTF_REV 9

/*==============================================================================

Options for CONFIG_MASK

==============================================================================*/

#define RFLM_DTR_RX_STATIC_MASK 0x1 	/* Used to configure the set of registers that will be configured by SW just once; Rest may be updated dynamically from SW and needs to be written as independent blocks*/ 
#define RFLM_DTR_RX_ADC_CFG_MASK 0x2 /* This Control Word triggers programming of the Bolt registers to program ADC sampling clock*/ 
#define RFLM_DTR_RX_COMMON_NOTCH_01_MASK 0x4 /* This Control Word triggers programming of the URxFE registers to program the common notches 0/1.*/ 
#define RFLM_DTR_RX_COMMON_NOTCH_23_MASK 0x8 /* This Control Word triggers programming of the URxFE registers to program the common notches 2/3.*/ 
#define RFLM_DTR_RX_IQMISMATCH_MASK 0x10 /* This Control Word triggers programming of IQMismatch compensation regsiters*/ 
#define RFLM_DTR_RX_NOTCH_MASK 0x20 /* This Control Word triggers programming of all notch related registers in a WB chain*/ 
#define RFLM_DTR_RX_URXFE_NB0_MASK 0x40 /* This Control Word triggers programming of the URxFE registers to program the first NB chain in a NB group.*/ 
#define RFLM_DTR_RX_CSR_FREQ_OFFSET_NB0_MASK 0x80 /* This Control Word triggers programming of the URxFE registers to program the second NB chain's cordic rotator frequency offset in a NB group.*/ 
#define RFLM_DTR_RX_DVGA_NB0_MASK 0x100  
#define RFLM_DTR_RX_ICIFIR_NB0_MASK 0x200 /* This Control Word triggers programming of ICIFIR filter coeffs for NB0*/ 
#define RFLM_DTR_RX_URXFE_NB1_MASK 0x400 /* This Control Word triggers programming of the URxFE registers to program the second NB chain*/ 
#define RFLM_DTR_RX_CSR_FREQ_OFFSET_NB1_MASK 0x800 /* This Control Word triggers programming of the URxFE registers to program the second NB chain's cordic rotator frequency offset in a NB group.*/ 
#define RFLM_DTR_RX_DVGA_NB1_MASK 0x1000  
#define RFLM_DTR_RX_ICIFIR_NB1_MASK 0x2000 /* This Control Word triggers programming of ICIFIR filter coeffs for NB1*/ 
#define RFLM_DTR_RX_URXFE_NB2_MASK 0x4000 /* This Control Word triggers programming of the URxFE registers to program the third NB chain in a NB group.*/ 
#define RFLM_DTR_RX_CSR_FREQ_OFFSET_NB2_MASK 0x8000 /* This Control Word triggers programming of the URxFE registers to program the third NB chain's cordic rotator frequency offset in a NB group.*/ 
#define RFLM_DTR_RX_DVGA_NB2_MASK 0x10000  
#define RFLM_DTR_RX_ICIFIR_NB2_MASK 0x20000 /* This Control Word triggers programming of ICIFIR filter coeffs for NB2*/ 
#define RFLM_DTR_RX_URXFE_NB3_MASK 0x40000 /* This Control Word triggers programming of the URxFE registers to program the fourth NB chain in a NB group.*/ 
#define RFLM_DTR_RX_CSR_FREQ_OFFSET_NB3_MASK 0x80000 /* This Control Word triggers programming of the URxFE registers to program the fourth NB chain's cordic rotator frequency offset in a NB group.*/ 
#define RFLM_DTR_RX_DVGA_NB3_MASK 0x100000  
#define RFLM_DTR_RX_ICIFIR_NB3_MASK 0x200000 /* This Control Word triggers programming of ICIFIR filter coeffs for NB3*/ 
#define RFLM_DTR_RX_RXFE_TOP_MASK 0x400000 /* This Control Word triggers programming of the ADC RXFE DBG mode registers and ACI filters*/ 

#define RFLM_DTR_RX_FULL_CFG_MASK ( RFLM_DTR_RX_STATIC_MASK | RFLM_DTR_RX_ADC_CFG_MASK \
   | RFLM_DTR_RX_IQMISMATCH_MASK \
   | RFLM_DTR_RX_NOTCH_MASK \
   | RFLM_DTR_RX_URXFE_NB0_MASK \
   | RFLM_DTR_RX_CSR_FREQ_OFFSET_NB0_MASK \
   | RFLM_DTR_RX_DVGA_NB0_MASK \
   | RFLM_DTR_RX_ICIFIR_NB0_MASK \
   | RFLM_DTR_RX_URXFE_NB1_MASK \
   | RFLM_DTR_RX_CSR_FREQ_OFFSET_NB1_MASK \
   | RFLM_DTR_RX_DVGA_NB1_MASK \
   | RFLM_DTR_RX_ICIFIR_NB1_MASK \
   | RFLM_DTR_RX_URXFE_NB2_MASK \
   | RFLM_DTR_RX_CSR_FREQ_OFFSET_NB2_MASK \
   | RFLM_DTR_RX_DVGA_NB2_MASK \
   | RFLM_DTR_RX_ICIFIR_NB2_MASK \
   | RFLM_DTR_RX_URXFE_NB3_MASK \
   | RFLM_DTR_RX_CSR_FREQ_OFFSET_NB3_MASK \
   | RFLM_DTR_RX_DVGA_NB3_MASK \
   | RFLM_DTR_RX_ICIFIR_NB3_MASK \
   | RFLM_DTR_RX_RXFE_TOP_MASK \
   )
  	/* This control word triggers programming all static and dynamic regsiters in the buffer */
#define RFLM_DTR_RX_NB0_CFG_MASK ( RFLM_DTR_RX_URXFE_NB0_MASK | RFLM_DTR_RX_CSR_FREQ_OFFSET_NB0_MASK | RFLM_DTR_RX_DVGA_NB0_MASK | RFLM_DTR_RX_ICIFIR_NB0_MASK ) /* This control word triggers programming all dynamic NB0 register blocks */

#define RFLM_DTR_RX_NB2_CFG_MASK ( RFLM_DTR_RX_URXFE_NB2_MASK | RFLM_DTR_RX_CSR_FREQ_OFFSET_NB2_MASK | RFLM_DTR_RX_DVGA_NB2_MASK | RFLM_DTR_RX_ICIFIR_NB2_MASK ) /* This control word triggers programming all dynamic NB2 register blocks */

#define RFLM_DTR_RX_NB1_CFG_MASK ( RFLM_DTR_RX_URXFE_NB1_MASK | RFLM_DTR_RX_CSR_FREQ_OFFSET_NB1_MASK | RFLM_DTR_RX_DVGA_NB1_MASK | RFLM_DTR_RX_ICIFIR_NB1_MASK ) /* This control word triggers programming all dynamic NB1 register blocks */

#define RFLM_DTR_RX_NB3_CFG_MASK ( RFLM_DTR_RX_URXFE_NB3_MASK | RFLM_DTR_RX_CSR_FREQ_OFFSET_NB3_MASK | RFLM_DTR_RX_DVGA_NB3_MASK | RFLM_DTR_RX_ICIFIR_NB3_MASK ) /* This control word triggers programming all dynamic NB3 register blocks */

#define RFLM_DTR_RX_GSM_MASK RFLM_DTR_RX_STATIC_MASK	/* This control word triggers programming the GSM set of RxFE registers */

#define RFLM_DTR_RX_NUM_CMNNOTCH_BLOCKS 2 /* Macro defining the number of blocks of common notches available for allocation */




#define RFLM_DTR_RX_UNUSED_INDEX 0xF

/* This struct captures the autogenerated header, for Revision information, etc. */
typedef struct
{
  uint16 major_rev;  /* Indicates Product Line */ 
  uint16 fw_intf_rev;  /* Indicates RX Buffer structure Rev */ 
  uint16 sw_settings_rev;  /* Indicates SW Settings Rev for the given FW Intf */ 
}rflm_dtr_rx_header_struct;



/* This struct captures the different indices used to index into the HWIO registers. */
typedef struct ALIGN(4)
{
  uint8 rxchn_idx_0;
  uint8 adca_idx_0;
  uint8 wbw_idx_0;
  uint8 nbn_idx_0;
  uint8 nbn_idx_1;
  uint8 nbn_idx_2;
  uint8 nbn_idx_3;
}rflm_dtr_rx_indices_struct;



 /* Group CSR_XO_VARS: */ 
/* This block contains RxLM variables used by FW for programming XO / frequency error related RxFE registers*/
typedef struct
{
  uint32 csr_clk_hz_c0; /* Hz */
  uint32 csr_clk_hz_c1; /* Hz */
  uint32 csr_clk_hz_c2; /* Hz */
  uint32 csr_clk_hz_c3; /* Hz */
  uint32 csr_ideal_val_c0; /* 29U28 */
  uint32 csr_ideal_val_c1; /* 29U28 */
  uint32 csr_ideal_val_c2; /* 29U28 */
  uint32 csr_ideal_val_c3; /* 29U28 */
  uint32 f_lo_inv_24u19;
}rflm_dtr_rx_csr_xo_vars_group_struct;



 /* Group XO_VARS: */ 
/* This block contains RxLM variables used by FW for programming XO / frequency error related RxFE registers*/
typedef struct
{
  uint32 csr_res_inv_c0;
  uint32 csr_res_inv_c1;
  uint32 csr_res_inv_c2;
  uint32 csr_res_inv_c3;
}rflm_dtr_rx_xo_vars_group_struct;



 /* Group VSRC_XO_VARS: */ 
/* This block contains RxLM variables used by FW for programming XO / frequency error related RxFE registers*/
typedef struct
{
  uint32 vsrc_t1byt2_ideal_val_q7; /* Q7 */
  uint32 vsrc_t1byt2_scale;
  uint32 vsrc_t2byt1m1_ideal_val_q32; /* Q32 */
  uint32 vsrc_t2byt1_ideal_scale;
}rflm_dtr_rx_vsrc_xo_vars_group_struct;



 /* Group STMR_XO_VARS: */ 
/* This block contains RxLM variables used by FW for programming XO / frequency error related RxFE registers*/
typedef struct
{
  uint32 stmr_fcw_ideal_val;
  uint32 stmr_fcw_scale;
}rflm_dtr_rx_stmr_xo_vars_group_struct;



 /* Group DELAY_VARS: */ 
/* This block contains RxLM variables used by FW for programming XO / frequency error related RxFE registers*/
typedef struct
{
  uint32 rxfe_delay_nb0_in_sctr; /* Q16 */
  uint32 rxfe_delay_nb1_in_sctr; /* Q16 */
  uint32 rxfe_delay_nb2_in_sctr; /* Q16 */
  uint32 rxfe_delay_nb3_in_sctr; /* Q16 */
  uint32 adc_vsrc_delay_in_off_clk;
}rflm_dtr_rx_delay_vars_group_struct;



 /* Group DVGA_VARS: */ 
/* This block contains RxLM variables used by FW for programming XO / frequency error related RxFE registers*/
typedef struct
{
  uint32 dvga_offset_c0;
  uint32 dvga_offset_c1;
  uint32 dvga_offset_c2;
  uint32 dvga_offset_c3;
}rflm_dtr_rx_dvga_vars_group_struct;



 /* Group VSRC_VARS: */ 
/* This block contains RxLM variables used by FW for programming XO / frequency error related RxFE registers*/
typedef struct
{
  uint32 vsrc_output_rate; /* Hz */
}rflm_dtr_rx_vsrc_vars_group_struct;



 /* Group ADC_VARS: */ 
/* This block contains RxLM variables used by FW for programming XO / frequency error related RxFE registers*/
typedef struct
{
  int32 adc_insertion_loss_delta; /* Q24 */
  uint32 ideal_adc_rate; /* Q22 */
}rflm_dtr_rx_adc_vars_group_struct;



 /* Group CSR_PHASE_COMP_VARS: */ 
/* This block contains RxLM variables used by FW for programming XO / frequency error related RxFE registers*/
typedef struct
{
  uint32 csr_phase_offset[6];
}rflm_dtr_rx_csr_phase_comp_vars_group_struct;



 /* Group SCALE_VARS: */ 
/* This block contains RxLM variables used by FW for programming XO / frequency error related RxFE registers*/
typedef struct
{
  int32 wbdc_scale; /* Q24 */
  int32 wbdc_offset; /* Q31 */
  int32 wbpwr_scale; /* Q24 */
  int32 nbpwr0_scale; /* Q24 */
  int32 nbpwr1_scale; /* Q24 */
  int32 nbpwr2_scale; /* Q24 */
  int32 nbpwr3_scale; /* Q24 */
  int32 lte_rfcal_offset; /* Q24 */
  uint32 zero_db_digital_gain;
}rflm_dtr_rx_scale_vars_group_struct;



 /* Group NOTCH_VARS: */ 
/* This block contains RxLM variables used by FW for programming XO / frequency error related RxFE registers*/
typedef struct
{
  uint32 notch_sampling_rate; /* Q22 */
  int32 wb_notch_filt01_threshold[2];
  int32 wb_notch_filt23_threshold[2];
  int32 cmn_notch_filt01_threshold[2];
  int32 cmn_notch_filt23_threshold[2];
}rflm_dtr_rx_notch_vars_group_struct;



 /* Group VSRC_WA_COUNT: */ 
/* This block consist of the URxFE registers to program a WB chain.*/
typedef struct
{
  uint32 rxfe_wb_vsrc_sctr_wa_wbw;
}rflm_dtr_rx_vsrc_wa_count_group_struct;





typedef enum
{
  RFLM_DTR_RX_HEADER_FW_GROUP,	/* Use Struct: rflm_dtr_rx_header_struct */
  RFLM_DTR_RX_FW_GROUP_CSR_XO_VARS,	/* Use Struct: rflm_dtr_rx_csr_xo_vars_group_struct */
  RFLM_DTR_RX_FW_GROUP_XO_VARS,	/* Use Struct: rflm_dtr_rx_xo_vars_group_struct */
  RFLM_DTR_RX_FW_GROUP_VSRC_XO_VARS,	/* Use Struct: rflm_dtr_rx_vsrc_xo_vars_group_struct */
  RFLM_DTR_RX_FW_GROUP_STMR_XO_VARS,	/* Use Struct: rflm_dtr_rx_stmr_xo_vars_group_struct */
  RFLM_DTR_RX_FW_GROUP_DELAY_VARS,	/* Use Struct: rflm_dtr_rx_delay_vars_group_struct */
  RFLM_DTR_RX_FW_GROUP_DVGA_VARS,	/* Use Struct: rflm_dtr_rx_dvga_vars_group_struct */
  RFLM_DTR_RX_FW_GROUP_VSRC_VARS,	/* Use Struct: rflm_dtr_rx_vsrc_vars_group_struct */
  RFLM_DTR_RX_FW_GROUP_ADC_VARS,	/* Use Struct: rflm_dtr_rx_adc_vars_group_struct */
  RFLM_DTR_RX_FW_GROUP_CSR_PHASE_COMP_VARS,	/* Use Struct: rflm_dtr_rx_csr_phase_comp_vars_group_struct */
  RFLM_DTR_RX_FW_GROUP_SCALE_VARS,	/* Use Struct: rflm_dtr_rx_scale_vars_group_struct */
  RFLM_DTR_RX_FW_GROUP_NOTCH_VARS,	/* Use Struct: rflm_dtr_rx_notch_vars_group_struct */
  RFLM_DTR_RX_FW_GROUP_VSRC_WA_COUNT,	/* Use Struct: rflm_dtr_rx_vsrc_wa_count_group_struct */
  RFLM_DTR_RX_INDICES_FW_GROUP,	/* Use Struct: rflm_dtr_rx_indices_struct */
  RFLM_DTR_RX_FW_GROUP_NUM,
  RFLM_DTR_RX_FW_GROUP_INVALID
}rflm_dtr_rx_fw_export_group_id;


#ifdef __cplusplus
}
#endif



#endif


