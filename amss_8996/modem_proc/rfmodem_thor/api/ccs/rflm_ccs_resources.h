#ifndef RFLM_CCS_RESOURCES_H
#define RFLM_CCS_RESOURCES_H


/*
   @file
   rflm_resources.h

   @brief
   RFLM CCS Driver's interface for resources request from other clients.

   @details

*/

/*===========================================================================
Copyright (c) 2013 - 2015 by Qualcomm Technologies, Incorporated.  All Rights Reserved.

                           EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/rfmodem_thor/api/ccs/rflm_ccs_resources.h#1 $
$DateTime: 2016/03/28 23:07:43 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
02/23/15   ra      Mainline CCS memory retention voting for Thor
08/10/14   ra      Support CCS memory retention voting for Thor
04/21/14   ra      Support M3 bit flip with no image download via MCPM
10/01/13   ra      Added support for CCS memory collapse 

==========================================================================*/

typedef enum{
  RFC_RETAIN_MEMORY,
  RFC_DONT_RETAIN_MEMORY,
  RFC_MCPM_MAX
} rfm_sleep_cfg_e;

rfm_sleep_cfg_e  rfm_get_mcpm_rfi_sleep_control( void );



void rfm_ccs_sleep(void);
void rfm_ccs_wakeup(void);
#endif  /* RFLM_CCS_RESOURCES.H */
