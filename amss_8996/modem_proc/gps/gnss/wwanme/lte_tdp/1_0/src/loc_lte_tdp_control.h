#ifndef LOC_LTE_TDP_CONTROL_H
#define LOC_LTE_TDP_CONTROL_H
/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                Location LTE OTDOA Control Module Header File

GENERAL DESCRIPTION
This file contains API definitions between Location LTE OTDOA Control module
files.

===========================================================================

  Copyright (c) 2014-2015 Qualcomm Atheros, Inc.
  All Rights Reserved. 
  Qualcomm Atheros Confidential and Proprietary.		

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================

Version Control

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/gps/gnss/wwanme/lte_tdp/1_0/src/loc_lte_tdp_control.h#1 $
$DateTime: 2016/03/28 23:03:26 $
$Author: mplcsds1 $

*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/


/*--------------------------------------------------------------------------
 * Include Files
 *-----------------------------------------------------------------------*/
#include "loc_lte_tdp_api.h"
#include "loc_lte_tdp_gnss_api.h"
#include "loc_lte_otdoa_common_defs.h"
#include "loc_lte_otdoa_control.h"


/*--------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 *-----------------------------------------------------------------------*/

/* OpCrs meas from ML1 DM log version */
#define LLOC_TDP_MEAS_LOG_VERSION      0

/* Global Cell Params DM log version */
#define LLOC_TDP_GLOBAL_CELL_PARAMS_VERSION 0

/* Time stamped meas DM log version */
#define LLOC_TDP_TIMESTAMPED_MEAS_LOG_VERSION      0

/* Tdp Database DM log version */
#define LLOC_TDP_DATABASE_LOG_VERSION              1

/* TDP Single Database Meas DM Log version */
#define LLOC_TDP_DB_SINGLE_MEAS_LOG_VERSION        2

/* OPCRS measurement bit mask */
#define LLOC_TDP_MEAS_TYPE_SERVING          0x00000001  
#define LLOC_TDP_MEAS_TYPE_NEIGHBOR         0x00000002  
#define LLOC_TDP_MEAS_TYPE_OPCRS            0x00000004
#define LLOC_TDP_MEAS_TYPE_UKCRS            0x00000008
#define LLOC_TDP_MEAS_TYPE_PRS              0x00000010
#define LLOC_TDP_MEAS_EAP_RCVD              0x00000020
#define LLOC_TDP_MEAS_EAP_VALID             0x00000040
#define LLOC_TDP_MEAS_SUBFRAME_GPSTIME_SET  0x00000080
#define LLOC_TDP_MEAS_ACCEPTED              0x00000100

/* Time related paramaters */
#define LLOC_TDP_GNSSRTC_MS_MASK        0x7fff
#define LLOC_TDP_GNSSRTC_MS_ROLLOVER    0x8000
#define LLOC_TDP_GNSSRTC_MS_SHFT         (17) 

/* GPS CHIPS related definitions */
#define LLOC_TDP_GCX1_PER_MSEC           1023
#define LLOC_TDP_GCX32_PER_MSEC          (32*LLOC_TDP_GCX1_PER_MSEC)
#define LLOC_TDP_GCX80_PER_MSEC          (80*LLOC_TDP_GCX1_PER_MSEC)

/*Define the bits and masks for GNSSRTC (Gen8)*/
#define LLOC_TDP_GNSSRTC_CNT_CX80_MASK        (0x1FFFF)

/* 16:7  Chip count */
#define LLOC_TDP_GNSSRTC_CHIP_CNT_MASK        (0x1FF80) 
#define LLOC_TDP_GNSSRTC_CHIP_CNT_SHIFT       (7) 

/* 6:0  subChip x 80 sample count */
#define LLOC_TDP_GNSSRTC_SAMPLE_CNT_MASK      (0x7F) 

#define LLOC_TDP_GNSSPHASE_PER_CX80           4294967296

/* 64 bit Dal Time - Lower 24 bits are Ustmr, so higher 40 are MSB */
#define LLOC_TDP_USTMR_MSB_SHIFT               (24)   

/* USTMR Constants for Bolt */
#define LLOC_TDP_USTMR_MASK                   (0xFFFFFF)

/* USTMR Clock speed = 19.2 MHz */
#define LLOC_TDP_USTMR_CLOCK_FREQ_HZ          19200000 

/* Ms to USTMR conversion */
#define LLOC_TDP_USTMR_CNT_PER_MS             19200 

/* USTMR Rolls over every 0x8000000 counts */
#define LLOC_TDP_USTMR_ROLLOVER_COUNT         16777216 

/* USTMR Rollover midpoint */
#define LLOC_TDP_USTMR_HALF_ROLLOVER_COUNT    8388608 

/* USTMR Rolls over every 873 ms (16777216/19200)  */
#define LLOC_TDP_USTMR_ROLLOVER_MS            873.813 

/* Number of CDMA chips in 1 week.
 * 1.2288 Mcps * 3600 * 24 * 7 */
#define LLOC_TDP_NUM_OF_SECS_PER_WEEK_CX1   743178240000

/* No of ms in a week 7*24*60*60*1000 */
#define LLOC_TDP_NUM_OF_MSEC_IN_ONE_WEEK  604800000

/* OSTMR Phase Conversion factor */
#define LLOC_TDP_OSTMR_CLOCK_SPEED_KHZ 30720.0

/* OSTMR Phase Conversion factor */
#define LLOC_TDP_OSTMR_PHASE_FACTOR  4294967296.0

/* OSTMR Phase Conversion factor */
#define LLOC_TDP_VSRC_ROLLOVER  1258291200

#define LLOC_TDP_1MS_IN_LTE_TS   LLOC_TDP_OSTMR_CLOCK_SPEED_KHZ
#define LLOC_TDP_11MS_IN_LTE_TS   337920 /* 11 * 30720 */
#define LLOC_TDP_LTE_OSTMR_ROLLOVER  1073741824

#define LLOC_TDP_OSTMR_TO_USTMR         (0.625f)

/* XO freq to use when determining the Meas Clk unc */
#define LLOC_TDP_XO_DRIFT_PPB                 (50)

/* OstmrVsrcDiff unc when it is available e.g. connected mode when RxFE has not resetted */
#define LLOC_TDP_OSTMR_VSRC_DIFF_VALID_UNC_MSEC      (26.0f * NSEC_TO_MSEC)

/* OstmrVsrcDiff unc when it is un available e.g. in idle mode when RxFE resets */
#define LLOC_TDP_OSTMR_VSRC_DIFF_INVALID_UNC_MSEC    (1.0f * NSEC_TO_MSEC)

/* Time latch uncertainty */
#define LLOC_TDP_GPS_TIME_TRANSFER_LATCH_UNC         (6.0f * NSEC_TO_MSEC)

/* TODO_CONNECTED: Scale factor for clk unc in connected mode */
#define LLOC_TDP_CLK_UNC_CONNECTED_MODE_SCALE_FACTOR  (3)

#ifndef ABS_TDP
#define ABS_TDP(a)          (((a) < 0) ? (-(a)) : (a))
#endif

/*--------------------------------------------------------------------------
 * Type Declarations
 *-----------------------------------------------------------------------*/

typedef loc_lte_otdoa_DiagCmdType lloc_tdp_DiagCmdType;

typedef enum
{
  LLOC_TDP_NO_ERR = 1,
  LLOC_TDP_NO_ERR_NO_PEAK,
  LLOC_TDP_ERR_PEAK_INDEX_OUT_OF_BOUNDS,
  LLOC_TDP_ERR_INVALID_CELLID,
  LLOC_TDP_ERR_NULL_CER_MEMORY,
  LLOC_TDP_ERR_NO_MEAS,
  LLOC_TDP_ERR_INVALID_INPUT_PARAM,
  LLOC_TDP_ERR_MAX
} lloc_tdp_MeasProcErrorEnumTypeVal;

typedef uint8 lloc_tdp_MeasProcErrorEnumType;

typedef uint8 loc_lte_tdp_BandwidthType;

typedef enum
{
  LOC_LTE_TDP_STATE_NO_SERVICE = 0, /*!< No service state */
  LOC_LTE_TDP_STATE_ACQ,        /*!< LTE is in ACQ*/
  LOC_LTE_TDP_STATE_IDLE,       /*!< Idle state */
  LOC_LTE_TDP_STATE_CONNECTED,  /*!< Connected state */
  LOC_LTE_TDP_STATE_GAP_LTE,    /*!< LTE is in LTE gap */
  LOC_LTE_TDP_STATE_GAP_IRAT,   /*!< LTE is in IRAT gap */
  LOC_LTE_TDP_STATE_MAX,        /*!< Max states */
} loc_lte_tdp_LteStateTypeVal;

/* Processed results from processing CER vector of a hypothesis */
typedef struct
{
  lloc_tdp_MeasProcErrorEnumType e_MeasStatus;

  uint32 q_SeqNum;  /* sequence number */

  uint16  w_PhysicalId; /* Range: 0...503 */

  uint32 q_Earfcn;  /* Radio freq. channel no. */

  /* EAP sample CER value */
  uint16 w_EapCer;

  /* EAP sample index value  */
  uint16 w_EapPos;

  /* interpolated EAP CER value */
  FLT f_EapCerFine;

  /* interpolated EAP index value in Ts Units */
  FLT f_EapPosFineTs;

  /* Max sample CER value */
  uint16 w_MaxPeakCer;

  /* Max sample index value  */
  uint16 w_MaxPeakPos;

  /* interpolated EAP CER value */
  FLT f_MaxPeakCerFine;

  /* interpolated EAP index value in Ts Units */
  FLT f_MaxPeakPosFineTs;

  uint8 u_MeasUncK;  /* OpCRS measurement 
                               uncertainty*/

  /* Noise floor associated with the meas. */
  uint16 w_NoiseFloor;  

  /* System Frame Dl time Adj to be used for Ngbr cell.
  Will be zero for serv meas */
  int32 x_ServNgbrAdjTs;

  /* Selected Rx chain */
  uint8 u_RxNumSel;

}lloc_tdp_EapOutputStructType;

typedef struct
{
  boolean b_Valid;
  uint16 w_Mcc; /* Layer 1 region ID */
  uint16 w_Mnc; /* Layer 2 region ID */
  uint16 w_Tac; /* Layer 3 region ID */
  uint32 q_CellId; /* Layer 4 region ID */
} lloc_tdp_GlobalCellIdStructType;

typedef struct
{
  boolean b_Valid;   /* GPS Time validity flag TRUE: valid, FALSE: invalid */
  uint32  q_FixUstmr;   /* USTMR Time @ GPS Fix time that is propagated to Ustmr latch */
  uint64  t_FixUstmr64; /* USTMR Time based on Time Tick */ 
  uint32  q_GpsMsec; /* GPS Ms estimate */ 
  uint16  w_GpsWeek; /* GPS Week estimate */ 
  FLT     f_ClkTimeUnc1SigmaMs; /* Time Uncertainity Estimate at the GPS Time */ 
  FLT     f_ClkTimeBias;  /* GPS Sub Ms or Bias at the GPS Time */ 

} loc_lte_tdp_GpsTimeInfoType;  

typedef struct
{
  /* Raw Gps Time Injection from ME */
  uint32  q_Fcount;
  uint16  w_GpsWeek; /* GPS Week estimate */ 
  uint32  q_GpsMsec; /* GPS Ms estimate */ 
  FLT     f_ClkTimeBias;  /* GPS Sub Ms or Bias at the GPS Time */ 
  FLT     f_ClkTimeUnc1SigmaMs; /* Time Uncertainity Estimate at the GPS Time */ 

  /* GPS Latch */
  uint32  q_UstmrLatch;   
  uint32  q_GnssRtcLatch;
  uint64  t_Ustmr64Latch;
  loc_lte_tdp_LatchedDataType z_LatchedData[8];  
} loc_lte_tdp_RawGpsFixTimeInfoType;  

#ifdef T_WINNT
#error code not present
#endif /* T_WINNT */

/* Tdp Database DM log */
typedef PACKED struct PACKED_POST
{
  uint8 b_CellIdValid;	
  uint16 w_PhyId;  /* Physical Id 0-503 */
  uint32 q_Earfcn; /* Downlink center frequency - Integer (0..262143) */
  uint16 w_Mcc; /* Layer 1 region ID */
  uint16 w_Mnc; /* Layer 2 region ID */
  uint16 w_Tac; /* Layer 3 region ID */
  uint32 q_CellId; /* Layer 4 region ID */

} lloc_tdp_LteCellIdStructDmLog;


typedef PACKED struct PACKED_POST
{
  /* Meas Description related fields */
  uint32 q_LatestMeasFlags;
  uint32 q_SeqNum;  /* sequence number */
  uint8 u_SignalPower;  /* Range (-140 dBm to -44 dBm) with 1 dBm 
                        resolution. Converted to value per Table 2-36 in
                        ICD Upload doc*/

  uint8 u_SignalQuality; /* Reference signal received quality.
                         The signal Quality will 
                         be RSRQ as defined in LPP. Range (-19.5 dB 
                         to -3dB with 0.5 dB resolution. Converted per 
                         Table 2-37 in ICD Upload doc */

  uint8 u_LteState;   /* Lte state  */

  /* EAP sample index value  */
  uint16 w_EapPos;
  /* Max sample index value  */
  uint16 w_MaxPeakPos;
  /* Eap Pos Fine Ts */
  uint32 f_EapPosFineTs;
  /* Com Adj */
  uint32 f_ScaledComAdjTs; /* Com adjustment in Ts units */
  /* EAP SNR */
  uint32 f_EapSnr;
  /* Max Peak Snr */
  uint32 f_MaxPeakSnr;
  /* EAP DL Time */
  uint64 d_TOA;

  /* GNSS Time injection USTMRs */
  uint64 t_PrevGpsFixUstmr64;
  uint64 t_CurrGpsFixUstmr64;

  /* Meas Timestamp */
  uint32  q_GpsMsec; /* GPS Ms estimate */ 
  uint16  w_GpsWeek; /* GPS Week estimate */ 
  uint32  f_ClkTimeUnc1SigmaMs; /* Time Uncertainity Estimate at the GPS Time */ 
  uint32  f_ClkTimeBias;  /* GPS Sub Ms or Bias at the GPS Time */
  uint32  q_Ustmr;         /* Measurement Ustmr */

  /* Misc data from FW */
  uint64 t_DlSfnRefTime; /* Dl Sfn ref time from FW */
  uint32 q_DlSfnSrAddr; /* Dl Sfn Sr addr from FW */
  uint16 w_UeProcMode;  /* LTE_LL1_UE_PROC_MODE_ON_LINE,            on-line: Process 1 symbol at a time
                                                                    as they are stored in sample buffer
                        LTE_LL1_UE_PROC_MODE_ON_LINE_INIT_ACQ_PBCH, indicates UE is in init-acq state
                        LTE_LL1_UE_PROC_MODE_ON_LINE_FSCAN,         indicates UE is in freq scan state
                        LTE_LL1_UE_PROC_MODE_ON_LINE_TRAFFIC,       indicates UE is in connected state
                        LTE_LL1_UE_PROC_MODE_ON_LINE_IRAT,          indicates UE is in IRAT state                                                                                                                
                        LTE_LL1_UE_PROC_MODE_OFF_LINE,              off-line: Store ~1m of samples, then
                                                                    process without collecting more samples */
  uint16 w_UeResMode; /* UE RES MODE : enum value 0=NO LTE, 1=IDLE, 2=IRAT, 3=SRCH ONLY, 4=FULL */

  int32  l_TtlAdjTs; /* Com adjustment in Ts units */

  int64  x_MstmrAdjTs; /* Mstmr adjustment in Ts units */

} lloc_tdp_DatabaseMeasDmLog;


typedef PACKED struct PACKED_POST
{
  lloc_tdp_LteCellIdStructDmLog z_CellIdDmLog;

  lloc_tdp_DatabaseMeasDmLog z_LatestMeasDmLog;

  lloc_tdp_DatabaseMeasDmLog z_ReportableMeasDmLog;

} lloc_tdp_DbRecordDmLog;

typedef PACKED struct PACKED_POST
{
  uint32  q_GnssRtc; /* GNSS RTC value latched at the same time as USTMR */
  uint32  q_GnssPhase; /* GNSS RTC Phase count at Latch */
  uint32  q_SampleCount;   /* GNSS RTC sample and chip counts @ at Latch */
  uint32  q_Ustmr;   /* USTMR latch in 19.2MHz untis) */
} loc_lte_tdp_LatchedPkedDataType;


typedef PACKED struct PACKED_POST
{
  boolean b_PrevPrevTimeValid;   /* GPS Time validity flag TRUE: valid, FALSE: invalid */
  uint32  q_PrevPrevTimeUstmr;   /* USTMR Time @ GPS Fix time */
  uint64  t_PrevPrevTimeUstmr64; /* USTMR Time based on Time Tick */ 
  uint32  q_PrevPrevTimeGpsMsec; /* GPS Ms estimate */ 
  uint16  w_PrevPrevTimeGpsWeek; /* GPS Week estimate */ 
  uint32  f_PrevPrevTimeClkTimeUncMs; /* Time Uncertainity Estimate at the GPS Time */ 
  uint32  f_PrevPrevTimeClkTimeBias;  /* GPS Sub Ms or Bias at the GPS Time */ 
  uint32  q_PrevPrevTimeFcount;
  /* GPS Latch */
  uint32  q_PrevPrevTimeUstmrLatch;   
  uint32  q_PrevPrevTimeGnssRtcLatch;
  uint64  t_PrevPrevTimeUstmr64Latch;
  /* Gps Time at Latch */
  uint32  q_PrevPrevLatchGpsMsec; /* GPS Ms estimate */ 
  uint16  w_PrevPrevLatchGpsWeek; /* GPS Week estimate */ 
  uint32  f_PrevPrevLatchClkTimeBias;  /* GPS Sub Ms or Bias at the GPS Time */ 

  boolean b_PrevTimeValid;   /* GPS Time validity flag TRUE: valid, FALSE: invalid */
  uint32  q_PrevTimeUstmr;   /* USTMR Time @ GPS Fix time */
  uint64  t_PrevTimeUstmr64; /* USTMR Time based on Time Tick */ 
  uint32  q_PrevTimeGpsMsec; /* GPS Ms estimate */ 
  uint16  w_PrevTimeGpsWeek; /* GPS Week estimate */ 
  uint32  f_PrevTimeClkTimeUncMs; /* Time Uncertainity Estimate at the GPS Time */ 
  uint32  f_PrevTimeClkTimeBias;  /* GPS Sub Ms or Bias at the GPS Time */ 
  uint32  q_PrevTimeFcount;
  /* GPS Latch */
  uint32  q_PrevTimeUstmrLatch;   
  uint32  q_PrevTimeGnssRtcLatch;
  uint64  t_PrevTimeUstmr64Latch;
  /* Gps Time at Latch */
  uint32  q_PrevLatchGpsMsec; /* GPS Ms estimate */ 
  uint16  w_PrevLatchGpsWeek; /* GPS Week estimate */ 
  uint32  f_PrevLatchClkTimeBias;  /* GPS Sub Ms or Bias at the GPS Time */ 


  boolean b_CurrTimeValid;   /* GPS Time validity flag TRUE: valid, FALSE: invalid */
  uint32  q_CurrTimeUstmr;   /* USTMR Time @ GPS Fix time */
  uint64  t_CurrTimeUstmr64; /* USTMR Time based on Time Tick */ 
  uint32  q_CurrTimeGpsMsec; /* GPS Ms estimate */ 
  uint16  w_CurrTimeGpsWeek; /* GPS Week estimate */ 
  uint32  f_CurrTimeClkTimeUncMs; /* Time Uncertainity Estimate at the GPS Time */ 
  uint32  f_CurrTimeClkTimeBias;  /* GPS Sub Ms or Bias at the GPS Time */ 
  uint32  q_CurrTimeFcount;

  /* GPS Latch */
  uint32  q_CurrTimeUstmrLatch;   
  uint32  q_CurrTimeGnssRtcLatch;
  uint64  t_CurrTimeUstmr64Latch;

  /* Gps Time at Latch */
  uint32  q_CurrLatchGpsMsec; /* GPS Ms estimate */ 
  uint16  w_CurrLatchGpsWeek; /* GPS Week estimate */ 
  uint32  f_CurrLatchClkTimeBias;  /* GPS Sub Ms or Bias at the GPS Time */ 

  loc_lte_tdp_LatchedPkedDataType z_PrevLatch[8];
  loc_lte_tdp_LatchedPkedDataType z_CurrLatch[8];

} lloc_tdp_GPSFixTimeDmLog;

typedef PACKED struct PACKED_POST
{
  log_hdr_type z_Hdr;  /* Standard DM header */

  uint8   u_Version;  /* Version number for the log */

  uint32  q_NumTdpMeas; /* Number of TDP measurements */

  lloc_tdp_DbRecordDmLog  z_DbRecord;

} lloc_tdp_CompleteDbDmLog;


/* DM log structure for Global cell params obtained using the RRC API */
typedef PACKED struct PACKED_POST
{
  log_hdr_type z_Hdr;  /* Standard DM header */

  uint8  u_Version;  /* Version number for the log */

  uint16 w_Mcc; /* Layer 1 region ID */

  uint16 w_Mnc; /* Layer 2 region ID */

  uint16 w_Tac; /* Layer 3 region ID */

  uint32 q_CellId; /* Layer 4 region ID */

  uint16 w_PhyId;  /* Physical Id 0-503 */

  uint32 q_Earfcn; /* Downlink center frequency - Integer (0..262143) */

} lloc_tdp_ServCellParamsDmLog;

/* DM log structure for the measurement received from ML1 */

/* LTE Time Transfer sets of O_STMR+GNSS RTCs latched at the same time structure */
typedef PACKED struct PACKED_POST
{
  /* O_STMR count value (in CDMA Chipx8) */
  uint32  q_OStmr;

  /* O_STMR phase value (in CDMA Chipx2^35) */
  uint32  q_OStmrPhase;

  /* GNSS RTC value (in GPS Chipx80) */
  uint32  q_GnssRtc;

  /* GNSS Phase value (in sub-GPS Chipx80) */
  uint32  q_GnssPhase;

  /* GNSS Sample Count status */
  uint32  q_SampleCount; 

} cgps_LteTTLatchedDataDmLog;

/* Reference Time structure for Timetags */
typedef PACKED struct PACKED_POST
{
  /* Ref Time */
  uint8                  b_RefTimeValid;
  /* Ref Time Validity */
  uint64                   t_RefTime;
} cgps_RefInfoDmLog;


typedef PACKED struct PACKED_POST
{
  uint8   b_Valid;
  uint16  w_Sfn;
  uint64  t_VsrcFromFW[2]; 
  uint32  q_OstmrFromFW[2];
  uint32  q_OstmrPhFromFW[2];
  uint64  t_RefDlTime; 
} lloc_tdp_OpcrsVsrcInfoDmLog;

typedef PACKED struct PACKED_POST
{
  uint8   b_Sib8Valid;   /* SIB8 "cdma-EUTRA-Synchronization" 
                            info. 
                            TRUE: LTE NW is synchronized.
                            FALSE: LTE NW is NOT synchronized */

  uint64  t_Sib8CdmaSystemTime; /* The CDMA2000 system time corresponding to 
                                   the SFN boundary at or after the ending 
                                   boundary of the SI-Window in which this Time 
                                   Transfer was taken. The size is 52 bits and 
                                   the unit is [CDMA2000 chips] */

  uint8   b_Sib16Valid; /* SIB16 System Time info*/

  uint64  t_Sib16GpsMsecs; /* The Full gps time at the SFN boundary in units of 
                              msecs */

  uint16  w_LeapSeconds; /* Number of leap seconds between UTC time and GPS 
                            time */


  uint16 w_Sfn;            /* System Frame Number of this OpCRS measurement */

  uint8  u_SubFn;          /* Subframe number of this OpCRS measurement */            

  uint32  q_OStmrSfBoundary;  /* O-STMR value at the boundary of the subframe 
                                 in which CRS measurement was made */

  uint32  q_UstmrLatched; /* Universal STMR value latched at the same time as 
                             O_STMR (in 19.2MHz units) */

  uint32  q_OstmrLatched; /* OSTMR value latched at the same time as USTMR */

  uint32  q_OstmrPhaseLatched; /* OSTMR Phase value latched at the same time as USTMR */

  uint32  q_UstmrFwDLSubFnNoAdj;     /* Universal STMR value at DL SF according to FW 
                                (in 19.2MHz units) */

  uint32  q_OstmrFwDLSubFnNoAdj; /* OSTMR value at DL Sub Fn as per FW */

  uint32  q_UstmrFwDLSubFN;     /* Universal STMR value at DL SF according to FW 
                                (in 19.2MHz units) */

  uint32  q_OstmrFwDLSubFN; /* OSTMR value at DL Sub Fn using VSTMR API on USTMR from FW */

  uint32  q_OstmrPhFwDLSubFN; /* OSTMR Phase at DL SubFn using VSTMR API on USTMR from FW */

  uint8   b_OstmrVsrcValid; /* Mark if the next field is valid or not */
  lloc_tdp_OpcrsVsrcInfoDmLog z_RefVsrcInfo[3];
  lloc_tdp_OpcrsVsrcInfoDmLog z_SFNVsrcInfo[3];
  cgps_RefInfoDmLog  z_RefTimeInfo;

  /* For Unit Testing purpose */
  uint8   b_LatchTTr;

  cgps_LteTTLatchedDataDmLog z_LatchedData[8];

  uint32 q_UstmrLatchTTr[8];  

} lloc_tdp_OpcrsTimeInfoDmLog;

/* Cer structure that has the Cer vector and it's size*/
typedef PACKED struct PACKED_POST
{
  uint16 w_NgbrCerArray[LOC_LTE_TDP_OPCRS_NGBR_CER_MAX_SIZE];
} lloc_tdp_NgbrCerStructType;

/* CerVector blk that has Cer vector for all the Rx Antennas */
typedef PACKED struct PACKED_POST
{
  lloc_tdp_NgbrCerStructType z_NgbrCer[LOC_LTE_TDP_MAX_UE_RX_ANT];
} lloc_tdp_NgbrCerBlkStructType;

typedef PACKED struct PACKED_POST
{
  log_hdr_type z_Hdr;  /* Standard DM header */

  uint8  u_Version;  /* Version number for the log */

  uint8 u_LogType; /* 0: Serv meas 1: Ngbr meas */

  uint32 q_SeqNum;    /* sequence number */

  uint32 q_MeasFlags; /* See OPCRS measurement bit mask above */
                     
  uint16 w_PhysicalId;   /* Range: 0...503 */

  uint32 q_Earfcn;   /* Radio freq. channel no. */

  uint8 b_CyclicPrefix; /* FALSE: normal
                             TRUE: extended */
  
  uint8 b_AntennaPortCfg;  /* FALSE: 1 or 2 transmit antennas 
                                TRUE: 4 transmit antennas */

  uint8 u_CarrierIdx;  /* 0 = PCC, 1 = SCC */

  uint8 e_Bandwidth; /* Bandwidth of type loc_lte_tdp_OpCrsBandwidthType */

  uint16 w_SysSubFn;  /* System frame number of measurement */

  uint8 u_SignalPower;  /* Range (-140 dBm to -44 dBm) with 1 dBm 
                          resolution. */

  uint8 u_SignalQuality; /* Reference signal received quality.
                           The signalQuality will 
                           be RSRQ as defined in LPP. Range (-19.5 dB 
                           to -3dB with 0.5 dB resolution. */
 
  int32  l_TtlAdj; /* Time tracking loop adjusment */

  int32  l_ComAdj; /* Com adjustment */

  int64  t_MstmrAdj; /* Mstmr adjustment */
 
  uint16 w_RxTxTimeDiff; /* rxtx timediff */

  uint8 u_EnergyType;  /* Encoded as:
                            0x0: signal energy
                            0x1: signal + noise energy */

  int64 l_ServDlSubframeRefTimeNoAdj; /* Dl time of Serv Cell */

  int64 l_ServDlSubframeRefTimeAdj; /* Adjusted Dl time of Serv cell */

  uint16 w_CerSize;   /* Range: 1..512 */

  uint8 u_NumRxAntennas; /* Num Rx antenna diversity */

  lloc_tdp_NgbrCerBlkStructType z_NgbrCerBlk;

  uint32 t_SysFrameRefTimeTs[LOC_LTE_TDP_MAX_UE_RX_ANT];

  lloc_tdp_OpcrsTimeInfoDmLog z_TimeInfo; /*LTE related timing parameters*/

  uint16 w_UeProcMode; /* LTE_LL1_UE_PROC_MODE_ON_LINE,               on-line: Process 1 symbol at a time
                                                                      as they are stored in sample buffer
                          LTE_LL1_UE_PROC_MODE_ON_LINE_INIT_ACQ_PBCH, indicates UE is in init-acq state
                          LTE_LL1_UE_PROC_MODE_ON_LINE_FSCAN,         indicates UE is in freq scan state
                          LTE_LL1_UE_PROC_MODE_ON_LINE_TRAFFIC,       indicates UE is in connected state
                          LTE_LL1_UE_PROC_MODE_ON_LINE_IRAT,          indicates UE is in IRAT state                                                                                                                
                          LTE_LL1_UE_PROC_MODE_OFF_LINE,              off-line: Store ~1m of samples, then
                                                                      process without collecting more samples */

  uint16 w_UeResMode; /* UE RES MODE : enum value 0=NO LTE, 1=IDLE, 2=IRAT, 3=SRCH ONLY, 4=FULL */

  uint8 u_LteState;  /* Lte state */

} lloc_tdp_Ml1OpCrsNgbrMeasDmLog;


/* Cer structure that has the Cer vector and it's size*/
typedef PACKED struct PACKED_POST
{
  uint16 w_ServCerArray[LOC_LTE_TDP_OPCRS_SERV_CER_MAX_SIZE];
} lloc_tdp_ServCerStructType;

/* CerVector blk that has Cer vector for all the Rx Antennas */
typedef PACKED struct PACKED_POST
{
  lloc_tdp_ServCerStructType z_ServCer[LOC_LTE_TDP_MAX_UE_RX_ANT];
} lloc_tdp_ServCerBlkStructType;

typedef PACKED struct PACKED_POST
{
  log_hdr_type z_Hdr;  /* Standard DM header */

  uint8  u_Version;  /* Version number for the log */

  uint8 u_LogType; /* 0: Serv meas 1: Ngbr meas */

  uint32 q_SeqNum;    /* sequence number */

  uint32 q_MeasFlags; /* See OPCRS measurement bit mask above */
                     
  uint16 w_PhysicalId;   /* Range: 0...503 */

  uint32 q_Earfcn;   /* Radio freq. channel no. */

  uint8 b_CyclicPrefix; /* FALSE: normal
                             TRUE: extended */
  
  uint8 b_AntennaPortCfg;  /* FALSE: 1 or 2 transmit antennas 
                                TRUE: 4 transmit antennas */

  uint8 u_CarrierIdx;  /* 0 = PCC, 1 = SCC */

  uint8 e_Bandwidth; /* Bandwidth of type loc_lte_tdp_OpCrsBandwidthType */

  uint16 w_SysSubFn;  /* System frame number of measurement */

  uint8 u_SignalPower;  /* Range (-140 dBm to -44 dBm) with 1 dBm 
                          resolution. */

  uint8 u_SignalQuality; /* Reference signal received quality.
                           The signalQuality will 
                           be RSRQ as defined in LPP. Range (-19.5 dB 
                           to -3dB with 0.5 dB resolution. */
 
  int32  l_TtlAdj; /* Time tracking loop adjusment */

  int32  l_ComAdj; /* Com adjustment */

  int64  t_MstmrAdj; /* Mstmr adjustment */
 
  uint16 w_RxTxTimeDiff; /* rxtx timediff */

  uint8 u_EnergyType;  /* Encoded as:
                            0x0: signal energy
                            0x1: signal + noise energy */

  int64 l_ServDlSubframeRefTimeNoAdj; /* Dl time of Serv Cell */

  int64 l_ServDlSubframeRefTimeAdj; /* Adjusted Dl time of Serv cell */

  uint16 w_CerSize;   /* Range: 1..512 */

  uint8 u_NumRxAntennas; /* Num Rx antenna diversity */

  lloc_tdp_ServCerBlkStructType z_ServCerBlk;

  lloc_tdp_OpcrsTimeInfoDmLog z_TimeInfo; /*LTE related timing parameters*/

  uint16 w_UeProcMode; /* LTE_LL1_UE_PROC_MODE_ON_LINE,               on-line: Process 1 symbol at a time
                                                                      as they are stored in sample buffer
                          LTE_LL1_UE_PROC_MODE_ON_LINE_INIT_ACQ_PBCH, indicates UE is in init-acq state
                          LTE_LL1_UE_PROC_MODE_ON_LINE_FSCAN,         indicates UE is in freq scan state
                          LTE_LL1_UE_PROC_MODE_ON_LINE_TRAFFIC,       indicates UE is in connected state
                          LTE_LL1_UE_PROC_MODE_ON_LINE_IRAT,          indicates UE is in IRAT state                                                                                                                
                          LTE_LL1_UE_PROC_MODE_OFF_LINE,              off-line: Store ~1m of samples, then
                                                                      process without collecting more samples */

  uint16 w_UeResMode; /* UE RES MODE : enum value 0=NO LTE, 1=IDLE, 2=IRAT, 3=SRCH ONLY, 4=FULL */

  uint8 u_LteState;  /* Lte state */

} lloc_tdp_Ml1OpCrsServMeasDmLog;

/* DM log for timestamped measurement */
/* Complete GPS Time information at either GPS Fix or CRS Meas */
typedef PACKED struct PACKED_POST
{
  uint64  t_FixUstmr64; /* USTMR Latch time to which the gps fix time has been propagated */ 
  uint32  q_GpsMsec; /* GPS Ms estimate */ 
  uint16  w_GpsWeek; /* GPS Week estimate */ 
  uint32  f_ClkTimeUnc1SigmaMs; /* Time Uncertainity Estimate at the GPS Time */ 
  uint32  f_ClkTimeBias;  /* GPS Sub Ms or Bias at the GPS Time */ 
} lloc_tdp_GpsTimeInfoDmLog;  


typedef PACKED struct PACKED_POST
{
  log_hdr_type z_Hdr;  /* Standard DM header */

  uint8  u_Version;  /* Version number for the log */

  /* Meas identifier */
  uint32 q_MeasFlags;

  /* Sequence number */ 
  uint32 q_SeqNum;  

  /* Cell description */
  uint16 w_PhyId;  /* Physical Id 0-503 */
  uint32 q_Earfcn; /* Downlink center frequency - Integer (0..262143) */
  uint16 w_CrsSubFn;  /* CRS Sub FN (SysFN + SubFn) */

  /* CRS Meas Time calculated by Interpolation */
  lloc_tdp_GpsTimeInfoDmLog z_MeasTime;  

  /* GPS Fix Time used for Interpolation */
  lloc_tdp_GpsTimeInfoDmLog z_PrevTime; 
  lloc_tdp_GpsTimeInfoDmLog z_CurrTime;  

  /* Time stamping related */
  lloc_tdp_OpcrsTimeInfoDmLog z_LteTimeInfo;  

} lloc_tdp_TimeStampDmLog;

typedef PACKED struct PACKED_POST
{
  log_hdr_type z_Hdr;  /* Standard DM header */

  uint8   u_Version;  /* Version number for the log */

  lloc_tdp_DbRecordDmLog  z_DbRecord;

  boolean b_TSAfterEAP;

  lloc_tdp_GPSFixTimeDmLog z_GPSFixTime;

  lloc_tdp_OpcrsTimeInfoDmLog z_CrsRawTime;

} lloc_tdp_MeasDmLog;

#ifdef T_WINNT
#error code not present
#endif /* T_WINNT */

/* Cell id structure */
typedef struct
{
  uint16 w_PhyId;
  uint32 q_Earfcn;
} lloc_tdp_CellIdStructType;


/* Tdp Upload session related parameters */
typedef struct
{
  boolean b_IsEnabled;
  boolean b_IsOtdoaSessOngoing;
  lloc_tdp_CellIdStructType z_LatestCellId;
  FLT f_MeasTunc1SigmaThresholdMeters; /* Tunc threshold to be applied to meas beyond which
                           measurement will not be reported to TLE */
  boolean b_AcceptMeasInConnectedMode; /* If set to true, measurements in connected mode will also
                                   be time stamped */
  boolean b_SetConnectedDrxVsrcThreshold; /* If set to true, then only connected mode measurements with Vsrc
                                          offset < 0.5Ts will be time stamped */
} lloc_tdp_ParamsStructType;

/*--------------------------------------------------------------------------
 * Function Definitions
 *-----------------------------------------------------------------------*/


/*
 ******************************************************************************
 * Function: lloc_tdp_ProcDiagPkt
 *
 * Description:
 *  Handle TDP diag cmd
 *
 * Parameters: pz_DiagCmd - Pointer to lloc_tdp_DiagCmdType
 *  
 * Dependencies:
 *  None
 *
 * Return value:
 *  None
 *
 ******************************************************************************
 */
void lloc_tdp_ProcDiagPkt(lloc_tdp_DiagCmdType *pz_DiagCmd);


/*
 ******************************************************************************
 * Function: lloc_tdp_EapComplete
 *
 * Description: API called by OMP task to send Eap date to OC task 
 *   
 *
 * Parameters: pz_TdpMeasProcResp - Pointer to the OMP task response structure
 *  
 * Dependencies:
 *  None
 *
 * Return value:
 *  None
 *
 ******************************************************************************
 */
void lloc_tdp_EapComplete(lloc_tdp_EapOutputStructType *pz_EapOutput);

/*
 ******************************************************************************
 * Function: loc_lte_tdp_control_ProcApiMsg
 *
 * Description:
 *  Message processor
 *
 * Parameters:
 *  None
 *
 * Dependencies:
 *  None
 *
 * Return value:
 *  None
 *
 ******************************************************************************
 */
void loc_lte_tdp_control_ProcApiMsg ( const os_IpcMsgType *const p_Msg );

/*
 ******************************************************************************
 * Function: lloc_tdp_HandleClockUpdateMsg
 *
 * Description:
 *  This function is used by LOC LTE Control Task to handle the Clock update
 *  IPC message from ME
 *
 * Parameters:
 *  None
 *
 * Dependencies:
 *  None
 *
 * Return value:
 *  None
 *
 ******************************************************************************
 */
void lloc_tdp_HandleClockUpdateMsg (loc_lte_tdp_GpsFixTimeInfoType *pz_ClockUpdate);

/*
 ******************************************************************************
 * Function: loc_lte_tdp_GpsFixTimeUpdate
 *
 * Description:
 *  This API is called by ME to send Time Update to OC task
 *
 * Parameters: p_GpsFixTime - Pointer to GPS Fix Time
 *
 * Dependencies:
 *  None
 *
 * Return value:
 *  None.
 *
 ******************************************************************************
 */
void loc_lte_tdp_GpsFixTimeUpdate (loc_lte_tdp_GpsFixTimeInfoType *p_GpsFixTime);


/*
 ******************************************************************************
 * Function: lloc_tdp_SetServCellGlobalParams
 *
 * Description:
 *  Set the MCC, MNC, TAC, GlobalID into the Tdp Db
 *
 * Parameters: pz_CellIdOut - Db cell structure where the cell params will be stored
 *             pz_CellIdIn - Input structure from which the cell params will be 
 *                           taken
 *
 * Dependencies:
 *  None
 *
 * Return value:
 *  None.
 *
 ******************************************************************************
 */
void lloc_tdp_SetServCellGlobalParams(const uint16 w_InputPhyId, 
                                      const uint32 q_Earfcn, 
                                      const lloc_tdp_GlobalCellIdStructType *pz_CellIdIn);

/*
 ******************************************************************************
 * Function: lloc_tdp_LogServCellParams
 *
 * Description: Log Serv Cell Global Cell Params
 *
 * Parameters: w_PhyId - Serv Cell physical Id
 *             q_Earfcn - Serv Cell Earfcn
 *             pz_GlobalCellId - Pointer to Serv Cell global params
 *
 * Dependencies:
 *  None
 *
 * Return value:
 *  None.
 *
 ******************************************************************************
 */
void lloc_tdp_LogServCellParams(const uint16 w_PhyId, 
                              const uint32 q_Earfcn, 
                              const lloc_tdp_GlobalCellIdStructType *pz_GlobalCellId);

/*
 ******************************************************************************
 * Function: lloc_tdp_HandleOtdoaSessionStateChange
 *
 * Description: Function to let WWAN ME TDP s/w know that E911 has started
 *
 * Parameters: b_IsSessStart - bool to indicate if E911 session is ongoing
 *  
 *
 * Dependencies:
 *  None
 *
 * Return value:
 *  None.
 *
 ******************************************************************************
 */
void lloc_tdp_HandleOtdoaSessionStateChange(boolean b_IsSessStart);

#endif /* #ifndef LOC_LTE_TDP_CONTROL_H */
