/*============================================================================
  @file aon_db.cpp

  @brief
    This file contains the implementation for Alwayson service data base.
 
               Copyright (c) 2014 QUALCOMM Atheros, Inc.
               All Rights Reserved.
               QUALCOMM Proprietary and Confidential.
			   Copyright (c) 2015 Qualcomm Technologies, Inc. All Rights Reserved.
               Confidential and Proprietary - Qualcomm Technologies, Inc.

============================================================================*/

/*============================================================================

                           EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/gps/gnss/loc_mw/aon/src/aon_db.cpp#1 $ 
  $DateTime: 2016/03/28 23:03:26 $ 
  $Author: mplcsds1 $ 



when        who  what, where, why
----------  ---  -----------------------------------------------------------
04/06/15    sj    LB 2.0 integration
07/31/14    sj    Initial creation of file.


=============================================================================*/
/*****************************************************************************
 * Include Files
 * *************************************************************************/

#include "aon_db.h"
#include "aon_transac.h"
#include "aon_gmproxy.h"

AonDatabase *AonDatabase::m_pInstance = 0;

AonDatabase :: AonDatabase()
{
  for(int i = 0; i < AON_DB_MAX_GF_TRANSACTIONS ; i++)
  {
	m_TransacInfoMap[i] = NULL;
  }
 
}
 
AonDatabase * AonDatabase::Instance()
{
  if (!m_pInstance)   
  {
	m_pInstance = new  AonDatabase;

  } 
   
  return m_pInstance; 
}

boolean AonDatabase::addAonTransac(AonTransac* aonTransac)
{
  if(NULL == aonTransac)
  {
	AON_MSG_ERROR("AonDatabase:: addAonTransac NULL transac input", 0,0,0);
	return FALSE;
  }
  
  //store the AON transaction instance in the transaction list of the client
  uint32 ctr = 0;
  while( 0 != m_TransacInfoMap[ctr])
  {
	ctr++;
	if(AON_DB_MAX_GF_TRANSACTIONS <= ctr)
	{
	  AON_MSG_ERROR("AonDatabase::addAonTransac Client's transaction Buffer is full",
		0,0,0);
	 return FALSE;
	}
  }
  
  m_TransacInfoMap[ctr] = aonTransac;
  return (setCommonConfig());
}

AonTransac * AonDatabase::getAonTransacByReqId(uint32 reqId)
{
  AonTransac *p_transac = NULL;
    uint32 ctr = 0;
  while(ctr < AON_DB_MAX_GF_TRANSACTIONS)
  {
	p_transac = m_TransacInfoMap[ctr];
	if( (NULL != p_transac) && (reqId == p_transac->getReqId()))
	{
	  return p_transac;
	}
	ctr ++;
  }
  
  AON_MSG_ERROR("AonDatabase:: getAonTransacByReqId did not find transaction", 
	  0,0,0);
  return NULL;
}

AonTransac * AonDatabase::getAonTransacByGfId(uint32 gfId)
{
  AonTransac *p_transac = NULL;
  uint32 ctr = 0;
  while(ctr < AON_DB_MAX_GF_TRANSACTIONS)
  {
	p_transac = m_TransacInfoMap[ctr];
	if( (NULL != p_transac) && (gfId == p_transac->getGfId())
	  && (TRUE == p_transac->validGfId()) )
	{
	  return p_transac;
	}
	ctr ++;
  }
  
  AON_MSG_ERROR("AonDatabase:: getAonTransacByGfId did not find transaction", 
	  0,0,0);
  return NULL;
}

boolean AonDatabase::delAonTransac(AonTransac* aonTransac)
{
  uint32 ctr = 0;
  while(ctr < AON_DB_MAX_GF_TRANSACTIONS)
  {
	if( aonTransac == m_TransacInfoMap[ctr])
	{
	  delete (aonTransac);
	  m_TransacInfoMap[ctr] = NULL;
	  return (setCommonConfig());
	}
	ctr ++;
  }
  
  AON_MSG_ERROR("AonDatabase:: delAonTransac did not find transaction", 
	  0,0,0);
  return FALSE;
}

boolean AonDatabase::setCommonConfig()
{
  uint32 ctr = 0;
  AonGmClientConfig commonGmConfig = AonGmClientConfig();
  boolean retVal = FALSE;
  
  AonGmProxy *gmProxy = AonGmProxy::Instance();
  if(NULL == gmProxy)
  {
	AON_MSG_ERROR("AonDatabase::setCommonConfig cannot get GMProxy instance",0,0,0);
	return retVal;
  }
  
  while(ctr < AON_DB_MAX_GF_TRANSACTIONS)
  {
	AonGmClientConfig transacGmConfig;
	AonTransac* aonTransac = m_TransacInfoMap[ctr];
	if(aonTransac && (aonTransac->getEqGmClientConfig(transacGmConfig)))
	{
	  //atleast one transaction active 
	  retVal = TRUE;
	  //calculate minimum among max Bo times		 
	  if(commonGmConfig.mGmBackOffMax > transacGmConfig.mGmBackOffMax)
	  {
		commonGmConfig.mGmBackOffMax = transacGmConfig.mGmBackOffMax;
	  }

	  //calculate min amoung min Bo Times
	  if(commonGmConfig.mGmBackOffMin > transacGmConfig.mGmBackOffMin)
	  {
		commonGmConfig.mGmBackOffMin = transacGmConfig.mGmBackOffMin;
	  }

	  //calculate max session timeout. It is a requirement from GM that
      //its clients calculate the maximum session tiemout amongs it requests
      // and inject that information into GM so that it knows how long it
      //can attempt a GNSS position fix
	  if(commonGmConfig.mSessionTimeout < transacGmConfig.mSessionTimeout)
	  {
		commonGmConfig.mSessionTimeout = transacGmConfig.mSessionTimeout;
	  }

	  //If any of the transacs want agressive exit then the common config will have it
	  commonGmConfig.mGnssUnavailAggresiveExit |= transacGmConfig.mGnssUnavailAggresiveExit;

	  /* Caclulate the minimum CPI req rate, if no clients sets this rate then GF defaults shall be used */
	  if( transacGmConfig.mCpiReqRate < commonGmConfig.mCpiReqRate)
	  {
		commonGmConfig.mCpiReqRate = transacGmConfig.mCpiReqRate;
	  }
	  
	}	
	ctr ++;
  }

  if(TRUE == retVal)
  {
	retVal = gmProxy->setGfEngineParam(commonGmConfig);
  }
  else
  {
	/* Since the client request queue is empty we need to reset the "Unable to track" 
	 * (UAT) flag . THis is because we dont want aon_transac to send an UAT
	 * indication even before GM actually tries GNSS & CPI for a session if UAT
	 * was sent in the previous session
	 */
    gmProxy->setUnableToTrackReported(FALSE);
  }
  return retVal;
}

boolean AonDatabase::broadcastStatusInfo(aonSessionStatusType status)
{
  uint32 ctr = 0;
  boolean retVal = FALSE;
  while(ctr < AON_DB_MAX_GF_TRANSACTIONS)
  {
	 AonTransac* aonTransac = m_TransacInfoMap[ctr];
	 if(NULL != aonTransac )
	 {
		 aonTransac->statusCallbackHandler(aonTransac->getReqId(), status);
		 retVal =  TRUE; //We had atleast one valid transaction in the list
	 }	
	ctr ++;
  }
  
  AON_MSG_MED("AonDatabase:: status %d ctr %d retVal %d", 
	  status, ctr, retVal);
  return retVal;
}







