
/*
WARNING: This file is auto-generated.

Generated using: rfc_autogen.exe
Generated from:  V5.20.780 of RFC_HWSWCD.xlsm
*/

/*=============================================================================

          R F C     A U T O G E N    F I L E

GENERAL DESCRIPTION
  This file is auto-generated and it captures the configuration of the RF Card.

Copyright (c) 2015 Qualcomm Technologies Incorporated.  All Rights Reserved.

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/rfc_thor/rf_card/rfc_wtr3925_3dlca_naeukr_1100/cdma/src/rfc_wtr3925_3dlca_naeukr_1100_cdma_config_ag.cpp#1 $ 


=============================================================================*/

/*=============================================================================
                           INCLUDE FILES
=============================================================================*/
#include "comdef.h"

#include "rfc_wtr3925_3dlca_naeukr_1100_cdma_config_ag.h" 
#include "rfc_wtr3925_3dlca_naeukr_1100_cmn_ag.h" 
#include "rfc_common.h" 
#include "rfm_cdma_band_types.h" 



extern "C" 
{
  extern rfc_device_info_type rf_card_wtr3925_3dlca_naeukr_1100_init_cdma_rx_device_info;
  extern rfc_sig_info_type rf_card_wtr3925_3dlca_naeukr_1100_init_cdma_rx_sig_cfg;
  extern rfc_device_info_type rf_card_wtr3925_3dlca_naeukr_1100_init_cdma_tx_device_info;
  extern rfc_sig_info_type rf_card_wtr3925_3dlca_naeukr_1100_init_cdma_tx_sig_cfg;
  extern rfc_device_info_type rf_card_wtr3925_3dlca_naeukr_1100_rx_on_rfm_device_4_cdma_bc0_device_info;
  extern rfc_sig_info_type rf_card_wtr3925_3dlca_naeukr_1100_rx_on_rfm_device_4_cdma_bc0_sig_cfg;
  extern rfc_device_info_type rf_card_wtr3925_3dlca_naeukr_1100_rx_on_rfm_device_5_cdma_bc0_device_info;
  extern rfc_sig_info_type rf_card_wtr3925_3dlca_naeukr_1100_rx_on_rfm_device_5_cdma_bc0_sig_cfg;
  extern rfc_device_info_type rf_card_wtr3925_3dlca_naeukr_1100_tx_on_rfm_device_7_cdma_bc0_device_info;
  extern rfc_sig_info_type rf_card_wtr3925_3dlca_naeukr_1100_tx_on_rfm_device_7_cdma_bc0_sig_cfg;
  extern rfc_device_info_type rf_card_wtr3925_3dlca_naeukr_1100_rx_on_rfm_device_0_cdma_bc1_device_info;
  extern rfc_sig_info_type rf_card_wtr3925_3dlca_naeukr_1100_rx_on_rfm_device_0_cdma_bc1_sig_cfg;
  extern rfc_device_info_type rf_card_wtr3925_3dlca_naeukr_1100_rx_on_rfm_device_1_cdma_bc1_device_info;
  extern rfc_sig_info_type rf_card_wtr3925_3dlca_naeukr_1100_rx_on_rfm_device_1_cdma_bc1_sig_cfg;
  extern rfc_device_info_type rf_card_wtr3925_3dlca_naeukr_1100_tx_on_rfm_device_6_cdma_bc1_device_info;
  extern rfc_sig_info_type rf_card_wtr3925_3dlca_naeukr_1100_tx_on_rfm_device_6_cdma_bc1_sig_cfg;
  extern rfc_device_info_type rf_card_wtr3925_3dlca_naeukr_1100_rx_on_rfm_device_4_cdma_bc10_device_info;
  extern rfc_sig_info_type rf_card_wtr3925_3dlca_naeukr_1100_rx_on_rfm_device_4_cdma_bc10_sig_cfg;
  extern rfc_device_info_type rf_card_wtr3925_3dlca_naeukr_1100_rx_on_rfm_device_5_cdma_bc10_device_info;
  extern rfc_sig_info_type rf_card_wtr3925_3dlca_naeukr_1100_rx_on_rfm_device_5_cdma_bc10_sig_cfg;
  extern rfc_device_info_type rf_card_wtr3925_3dlca_naeukr_1100_tx_on_rfm_device_7_cdma_bc10_device_info;
  extern rfc_sig_info_type rf_card_wtr3925_3dlca_naeukr_1100_tx_on_rfm_device_7_cdma_bc10_sig_cfg;
  extern rfc_device_info_type rf_card_wtr3925_3dlca_naeukr_1100_rx_on_rfm_device_0_cdma_bc14_device_info;
  extern rfc_sig_info_type rf_card_wtr3925_3dlca_naeukr_1100_rx_on_rfm_device_0_cdma_bc14_sig_cfg;
  extern rfc_device_info_type rf_card_wtr3925_3dlca_naeukr_1100_rx_on_rfm_device_1_cdma_bc14_device_info;
  extern rfc_sig_info_type rf_card_wtr3925_3dlca_naeukr_1100_rx_on_rfm_device_1_cdma_bc14_sig_cfg;
  extern rfc_device_info_type rf_card_wtr3925_3dlca_naeukr_1100_tx_on_rfm_device_6_cdma_bc14_device_info;
  extern rfc_sig_info_type rf_card_wtr3925_3dlca_naeukr_1100_tx_on_rfm_device_6_cdma_bc14_sig_cfg;
  extern rfc_device_info_type rf_card_wtr3925_3dlca_naeukr_1100_rx_on_rfm_device_0_cdma_bc15_device_info;
  extern rfc_sig_info_type rf_card_wtr3925_3dlca_naeukr_1100_rx_on_rfm_device_0_cdma_bc15_sig_cfg;
  extern rfc_device_info_type rf_card_wtr3925_3dlca_naeukr_1100_rx_on_rfm_device_1_cdma_bc15_device_info;
  extern rfc_sig_info_type rf_card_wtr3925_3dlca_naeukr_1100_rx_on_rfm_device_1_cdma_bc15_sig_cfg;
  extern rfc_device_info_type rf_card_wtr3925_3dlca_naeukr_1100_tx_on_rfm_device_6_cdma_bc15_device_info;
  extern rfc_sig_info_type rf_card_wtr3925_3dlca_naeukr_1100_tx_on_rfm_device_6_cdma_bc15_sig_cfg;
  extern rfc_device_info_type rf_card_wtr3925_3dlca_naeukr_1100_cdma_en_et_cal0_enable_fbrx_enable_xpt_capture_device_info;
  extern rfc_sig_info_type rf_card_wtr3925_3dlca_naeukr_1100_cdma_en_et_cal0_enable_fbrx_enable_xpt_capture_sig_cfg;
  extern rfc_device_info_type rf_card_wtr3925_3dlca_naeukr_1100_cdma_dis_et_cal0_disable_fbrx_disable_xpt_capture_device_info;
  extern rfc_sig_info_type rf_card_wtr3925_3dlca_naeukr_1100_cdma_dis_et_cal0_disable_fbrx_disable_xpt_capture_sig_cfg;
  extern rfc_device_info_type rf_card_wtr3925_3dlca_naeukr_1100_cdma_en_et_cal2_fbrx_path_dedicated_iq_enable_fbrx_enable_xpt_capture_device_info;
  extern rfc_sig_info_type rf_card_wtr3925_3dlca_naeukr_1100_cdma_en_et_cal2_fbrx_path_dedicated_iq_enable_fbrx_enable_xpt_capture_sig_cfg;
  extern rfc_device_info_type rf_card_wtr3925_3dlca_naeukr_1100_cdma_dis_et_cal2_fbrx_path_dedicated_iq_disable_fbrx_disable_xpt_capture_device_info;
  extern rfc_sig_info_type rf_card_wtr3925_3dlca_naeukr_1100_cdma_dis_et_cal2_fbrx_path_dedicated_iq_disable_fbrx_disable_xpt_capture_sig_cfg;
} /* extern "C" */


rfc_cdma_data * rfc_wtr3925_3dlca_naeukr_1100_cdma_ag::get_instance()
{
  if (rfc_cdma_data_ptr == NULL)
  {
    rfc_cdma_data_ptr = (rfc_cdma_data *)new rfc_wtr3925_3dlca_naeukr_1100_cdma_ag();
  }
  return( (rfc_cdma_data *)rfc_cdma_data_ptr);
}

//constructor
rfc_wtr3925_3dlca_naeukr_1100_cdma_ag::rfc_wtr3925_3dlca_naeukr_1100_cdma_ag()
  :rfc_cdma_data()
{
}

boolean rfc_wtr3925_3dlca_naeukr_1100_cdma_ag::sig_cfg_data_get( rfc_cfg_params_type *cfg, rfc_sig_cfg_type **ptr )
{

  boolean ret_val = FALSE;

  if (NULL==ptr)
  {
    return FALSE;
  }

  if (NULL==cfg)
  {
    *ptr = NULL;
    return FALSE;
  }

  *ptr = NULL;

  if ( ( cfg->rx_tx == RFC_CONFIG_RX ) && ( cfg->alternate_path == 0 /*Warning: not specified*/ ) && ( cfg->req == RFC_REQ_INIT ) )
  { *ptr = &(rf_card_wtr3925_3dlca_naeukr_1100_init_cdma_rx_sig_cfg.cfg_sig_list[0]);  ret_val = TRUE; }

  if ( ( cfg->rx_tx == RFC_CONFIG_TX ) && ( cfg->alternate_path == 0 /*Warning: not specified*/ ) && ( cfg->req == RFC_REQ_INIT ) && !ret_val )
  { *ptr = &(rf_card_wtr3925_3dlca_naeukr_1100_init_cdma_tx_sig_cfg.cfg_sig_list[0]);  ret_val = TRUE; }

  if ( ( cfg->rx_tx == RFC_CONFIG_RX ) && ( cfg->logical_device == RFM_DEVICE_4 ) && ( cfg->alternate_path == 0 /*Warning: not specified*/ ) && ( cfg->band == (int)RFM_CDMA_BC0 ) && ( cfg->req == RFC_REQ_DEFAULT_GET_DATA ) && !ret_val )
  { *ptr = &(rf_card_wtr3925_3dlca_naeukr_1100_rx_on_rfm_device_4_cdma_bc0_sig_cfg.cfg_sig_list[0]);  ret_val = TRUE; }

  if ( ( cfg->rx_tx == RFC_CONFIG_RX ) && ( cfg->logical_device == RFM_DEVICE_5 ) && ( cfg->alternate_path == 0 /*Warning: not specified*/ ) && ( cfg->band == (int)RFM_CDMA_BC0 ) && ( cfg->req == RFC_REQ_DEFAULT_GET_DATA ) && !ret_val )
  { *ptr = &(rf_card_wtr3925_3dlca_naeukr_1100_rx_on_rfm_device_5_cdma_bc0_sig_cfg.cfg_sig_list[0]);  ret_val = TRUE; }

  if ( ( cfg->rx_tx == RFC_CONFIG_TX ) && ( cfg->logical_device == RFM_DEVICE_7 ) && ( cfg->alternate_path == 0 /*Warning: not specified*/ ) && ( cfg->band == (int)RFM_CDMA_BC0 ) && ( cfg->req == RFC_REQ_DEFAULT_GET_DATA ) && !ret_val )
  { *ptr = &(rf_card_wtr3925_3dlca_naeukr_1100_tx_on_rfm_device_7_cdma_bc0_sig_cfg.cfg_sig_list[0]);  ret_val = TRUE; }

  if ( ( cfg->rx_tx == RFC_CONFIG_RX ) && ( cfg->logical_device == RFM_DEVICE_0 ) && ( cfg->alternate_path == 0 /*Warning: not specified*/ ) && ( cfg->band == (int)RFM_CDMA_BC1 ) && ( cfg->req == RFC_REQ_DEFAULT_GET_DATA ) && !ret_val )
  { *ptr = &(rf_card_wtr3925_3dlca_naeukr_1100_rx_on_rfm_device_0_cdma_bc1_sig_cfg.cfg_sig_list[0]);  ret_val = TRUE; }

  if ( ( cfg->rx_tx == RFC_CONFIG_RX ) && ( cfg->logical_device == RFM_DEVICE_1 ) && ( cfg->alternate_path == 0 /*Warning: not specified*/ ) && ( cfg->band == (int)RFM_CDMA_BC1 ) && ( cfg->req == RFC_REQ_DEFAULT_GET_DATA ) && !ret_val )
  { *ptr = &(rf_card_wtr3925_3dlca_naeukr_1100_rx_on_rfm_device_1_cdma_bc1_sig_cfg.cfg_sig_list[0]);  ret_val = TRUE; }

  if ( ( cfg->rx_tx == RFC_CONFIG_TX ) && ( cfg->logical_device == RFM_DEVICE_6 ) && ( cfg->alternate_path == 0 /*Warning: not specified*/ ) && ( cfg->band == (int)RFM_CDMA_BC1 ) && ( cfg->req == RFC_REQ_DEFAULT_GET_DATA ) && !ret_val )
  { *ptr = &(rf_card_wtr3925_3dlca_naeukr_1100_tx_on_rfm_device_6_cdma_bc1_sig_cfg.cfg_sig_list[0]);  ret_val = TRUE; }

  if ( ( cfg->rx_tx == RFC_CONFIG_RX ) && ( cfg->logical_device == RFM_DEVICE_4 ) && ( cfg->alternate_path == 0 /*Warning: not specified*/ ) && ( cfg->band == (int)RFM_CDMA_BC10 ) && ( cfg->req == RFC_REQ_DEFAULT_GET_DATA ) && !ret_val )
  { *ptr = &(rf_card_wtr3925_3dlca_naeukr_1100_rx_on_rfm_device_4_cdma_bc10_sig_cfg.cfg_sig_list[0]);  ret_val = TRUE; }

  if ( ( cfg->rx_tx == RFC_CONFIG_RX ) && ( cfg->logical_device == RFM_DEVICE_5 ) && ( cfg->alternate_path == 0 /*Warning: not specified*/ ) && ( cfg->band == (int)RFM_CDMA_BC10 ) && ( cfg->req == RFC_REQ_DEFAULT_GET_DATA ) && !ret_val )
  { *ptr = &(rf_card_wtr3925_3dlca_naeukr_1100_rx_on_rfm_device_5_cdma_bc10_sig_cfg.cfg_sig_list[0]);  ret_val = TRUE; }

  if ( ( cfg->rx_tx == RFC_CONFIG_TX ) && ( cfg->logical_device == RFM_DEVICE_7 ) && ( cfg->alternate_path == 0 /*Warning: not specified*/ ) && ( cfg->band == (int)RFM_CDMA_BC10 ) && ( cfg->req == RFC_REQ_DEFAULT_GET_DATA ) && !ret_val )
  { *ptr = &(rf_card_wtr3925_3dlca_naeukr_1100_tx_on_rfm_device_7_cdma_bc10_sig_cfg.cfg_sig_list[0]);  ret_val = TRUE; }

  if ( ( cfg->rx_tx == RFC_CONFIG_RX ) && ( cfg->logical_device == RFM_DEVICE_0 ) && ( cfg->alternate_path == 0 /*Warning: not specified*/ ) && ( cfg->band == (int)RFM_CDMA_BC14 ) && ( cfg->req == RFC_REQ_DEFAULT_GET_DATA ) && !ret_val )
  { *ptr = &(rf_card_wtr3925_3dlca_naeukr_1100_rx_on_rfm_device_0_cdma_bc14_sig_cfg.cfg_sig_list[0]);  ret_val = TRUE; }

  if ( ( cfg->rx_tx == RFC_CONFIG_RX ) && ( cfg->logical_device == RFM_DEVICE_1 ) && ( cfg->alternate_path == 0 /*Warning: not specified*/ ) && ( cfg->band == (int)RFM_CDMA_BC14 ) && ( cfg->req == RFC_REQ_DEFAULT_GET_DATA ) && !ret_val )
  { *ptr = &(rf_card_wtr3925_3dlca_naeukr_1100_rx_on_rfm_device_1_cdma_bc14_sig_cfg.cfg_sig_list[0]);  ret_val = TRUE; }

  if ( ( cfg->rx_tx == RFC_CONFIG_TX ) && ( cfg->logical_device == RFM_DEVICE_6 ) && ( cfg->alternate_path == 0 /*Warning: not specified*/ ) && ( cfg->band == (int)RFM_CDMA_BC14 ) && ( cfg->req == RFC_REQ_DEFAULT_GET_DATA ) && !ret_val )
  { *ptr = &(rf_card_wtr3925_3dlca_naeukr_1100_tx_on_rfm_device_6_cdma_bc14_sig_cfg.cfg_sig_list[0]);  ret_val = TRUE; }

  if ( ( cfg->rx_tx == RFC_CONFIG_RX ) && ( cfg->logical_device == RFM_DEVICE_0 ) && ( cfg->alternate_path == 0 /*Warning: not specified*/ ) && ( cfg->band == (int)RFM_CDMA_BC15 ) && ( cfg->req == RFC_REQ_DEFAULT_GET_DATA ) && !ret_val )
  { *ptr = &(rf_card_wtr3925_3dlca_naeukr_1100_rx_on_rfm_device_0_cdma_bc15_sig_cfg.cfg_sig_list[0]);  ret_val = TRUE; }

  if ( ( cfg->rx_tx == RFC_CONFIG_RX ) && ( cfg->logical_device == RFM_DEVICE_1 ) && ( cfg->alternate_path == 0 /*Warning: not specified*/ ) && ( cfg->band == (int)RFM_CDMA_BC15 ) && ( cfg->req == RFC_REQ_DEFAULT_GET_DATA ) && !ret_val )
  { *ptr = &(rf_card_wtr3925_3dlca_naeukr_1100_rx_on_rfm_device_1_cdma_bc15_sig_cfg.cfg_sig_list[0]);  ret_val = TRUE; }

  if ( ( cfg->rx_tx == RFC_CONFIG_TX ) && ( cfg->logical_device == RFM_DEVICE_6 ) && ( cfg->alternate_path == 0 /*Warning: not specified*/ ) && ( cfg->band == (int)RFM_CDMA_BC15 ) && ( cfg->req == RFC_REQ_DEFAULT_GET_DATA ) && !ret_val )
  { *ptr = &(rf_card_wtr3925_3dlca_naeukr_1100_tx_on_rfm_device_6_cdma_bc15_sig_cfg.cfg_sig_list[0]);  ret_val = TRUE; }

  if ( ( cfg->rx_tx == RFC_CONFIG_TX ) && ( cfg->logical_device == RFM_DEVICE_6 ) && ( cfg->alternate_path == 0 /*Warning: not specified*/ ) && ( ( cfg->band == (int)RFM_CDMA_BC1 )||  ( cfg->band == (int)RFM_CDMA_BC14 )||  ( cfg->band == (int)RFM_CDMA_BC15 )) && ( ( cfg->req == RFC_REQ_ENABLE_FBRX )||  ( cfg->req == RFC_REQ_ENABLE_XPT_CAPTURE )) && !ret_val )
  { *ptr = &(rf_card_wtr3925_3dlca_naeukr_1100_cdma_en_et_cal0_enable_fbrx_enable_xpt_capture_sig_cfg.cfg_sig_list[0]);  ret_val = TRUE; }

  if ( ( cfg->rx_tx == RFC_CONFIG_TX ) && ( cfg->logical_device == RFM_DEVICE_6 ) && ( cfg->alternate_path == 0 /*Warning: not specified*/ ) && ( ( cfg->band == (int)RFM_CDMA_BC1 )||  ( cfg->band == (int)RFM_CDMA_BC14 )||  ( cfg->band == (int)RFM_CDMA_BC15 )) && ( ( cfg->req == RFC_REQ_DISABLE_FBRX )||  ( cfg->req == RFC_REQ_DISABLE_XPT_CAPTURE )) && !ret_val )
  { *ptr = &(rf_card_wtr3925_3dlca_naeukr_1100_cdma_dis_et_cal0_disable_fbrx_disable_xpt_capture_sig_cfg.cfg_sig_list[0]);  ret_val = TRUE; }

  if ( ( cfg->rx_tx == RFC_CONFIG_TX ) && ( cfg->logical_device == RFM_DEVICE_7 ) && ( cfg->alternate_path == 0 /*Warning: not specified*/ ) && ( ( cfg->band == (int)RFM_CDMA_BC0 )||  ( cfg->band == (int)RFM_CDMA_BC10 )) && ( ( cfg->req == RFC_REQ_ENABLE_FBRX )||  ( cfg->req == RFC_REQ_ENABLE_XPT_CAPTURE )) && !ret_val )
  { *ptr = &(rf_card_wtr3925_3dlca_naeukr_1100_cdma_en_et_cal2_fbrx_path_dedicated_iq_enable_fbrx_enable_xpt_capture_sig_cfg.cfg_sig_list[0]);  ret_val = TRUE; }

  if ( ( cfg->rx_tx == RFC_CONFIG_TX ) && ( cfg->logical_device == RFM_DEVICE_7 ) && ( cfg->alternate_path == 0 /*Warning: not specified*/ ) && ( ( cfg->band == (int)RFM_CDMA_BC0 )||  ( cfg->band == (int)RFM_CDMA_BC10 )) && ( ( cfg->req == RFC_REQ_DISABLE_FBRX )||  ( cfg->req == RFC_REQ_DISABLE_XPT_CAPTURE )) && !ret_val )
  { *ptr = &(rf_card_wtr3925_3dlca_naeukr_1100_cdma_dis_et_cal2_fbrx_path_dedicated_iq_disable_fbrx_disable_xpt_capture_sig_cfg.cfg_sig_list[0]);  ret_val = TRUE; }

  if (ret_val == FALSE)
  {
    RF_MSG_5( RF_ERROR, "RFC detected error. Unsupported params combo provided by calling tech: device %d band %d altp %d rxtx %d reqtype %d",
              cfg->logical_device, cfg->band, cfg->alternate_path, cfg->rx_tx, cfg->req );
  }

  return ret_val;
}

boolean rfc_wtr3925_3dlca_naeukr_1100_cdma_ag::devices_cfg_data_get( rfc_cfg_params_type *cfg, rfc_device_info_type **ptr )
{

  boolean ret_val = FALSE;

  if (NULL==ptr)
  {
    return FALSE;
  }

  if (NULL==cfg)
  {
    *ptr = NULL;
    return FALSE;
  }

  *ptr = NULL;

  if ( ( cfg->rx_tx == RFC_CONFIG_RX ) && ( cfg->alternate_path == 0 /*Warning: not specified*/ ) && ( cfg->req == RFC_REQ_INIT ) )
  { *ptr = &(rf_card_wtr3925_3dlca_naeukr_1100_init_cdma_rx_device_info);  ret_val = TRUE; }

  if ( ( cfg->rx_tx == RFC_CONFIG_TX ) && ( cfg->alternate_path == 0 /*Warning: not specified*/ ) && ( cfg->req == RFC_REQ_INIT ) && !ret_val )
  { *ptr = &(rf_card_wtr3925_3dlca_naeukr_1100_init_cdma_tx_device_info);  ret_val = TRUE; }

  if ( ( cfg->rx_tx == RFC_CONFIG_RX ) && ( cfg->logical_device == RFM_DEVICE_4 ) && ( cfg->alternate_path == 0 /*Warning: not specified*/ ) && ( cfg->band == (int)RFM_CDMA_BC0 ) && ( cfg->req == RFC_REQ_DEFAULT_GET_DATA ) && !ret_val )
  { *ptr = &(rf_card_wtr3925_3dlca_naeukr_1100_rx_on_rfm_device_4_cdma_bc0_device_info);  ret_val = TRUE; }

  if ( ( cfg->rx_tx == RFC_CONFIG_RX ) && ( cfg->logical_device == RFM_DEVICE_5 ) && ( cfg->alternate_path == 0 /*Warning: not specified*/ ) && ( cfg->band == (int)RFM_CDMA_BC0 ) && ( cfg->req == RFC_REQ_DEFAULT_GET_DATA ) && !ret_val )
  { *ptr = &(rf_card_wtr3925_3dlca_naeukr_1100_rx_on_rfm_device_5_cdma_bc0_device_info);  ret_val = TRUE; }

  if ( ( cfg->rx_tx == RFC_CONFIG_TX ) && ( cfg->logical_device == RFM_DEVICE_7 ) && ( cfg->alternate_path == 0 /*Warning: not specified*/ ) && ( cfg->band == (int)RFM_CDMA_BC0 ) && ( cfg->req == RFC_REQ_DEFAULT_GET_DATA ) && !ret_val )
  { *ptr = &(rf_card_wtr3925_3dlca_naeukr_1100_tx_on_rfm_device_7_cdma_bc0_device_info);  ret_val = TRUE; }

  if ( ( cfg->rx_tx == RFC_CONFIG_RX ) && ( cfg->logical_device == RFM_DEVICE_0 ) && ( cfg->alternate_path == 0 /*Warning: not specified*/ ) && ( cfg->band == (int)RFM_CDMA_BC1 ) && ( cfg->req == RFC_REQ_DEFAULT_GET_DATA ) && !ret_val )
  { *ptr = &(rf_card_wtr3925_3dlca_naeukr_1100_rx_on_rfm_device_0_cdma_bc1_device_info);  ret_val = TRUE; }

  if ( ( cfg->rx_tx == RFC_CONFIG_RX ) && ( cfg->logical_device == RFM_DEVICE_1 ) && ( cfg->alternate_path == 0 /*Warning: not specified*/ ) && ( cfg->band == (int)RFM_CDMA_BC1 ) && ( cfg->req == RFC_REQ_DEFAULT_GET_DATA ) && !ret_val )
  { *ptr = &(rf_card_wtr3925_3dlca_naeukr_1100_rx_on_rfm_device_1_cdma_bc1_device_info);  ret_val = TRUE; }

  if ( ( cfg->rx_tx == RFC_CONFIG_TX ) && ( cfg->logical_device == RFM_DEVICE_6 ) && ( cfg->alternate_path == 0 /*Warning: not specified*/ ) && ( cfg->band == (int)RFM_CDMA_BC1 ) && ( cfg->req == RFC_REQ_DEFAULT_GET_DATA ) && !ret_val )
  { *ptr = &(rf_card_wtr3925_3dlca_naeukr_1100_tx_on_rfm_device_6_cdma_bc1_device_info);  ret_val = TRUE; }

  if ( ( cfg->rx_tx == RFC_CONFIG_RX ) && ( cfg->logical_device == RFM_DEVICE_4 ) && ( cfg->alternate_path == 0 /*Warning: not specified*/ ) && ( cfg->band == (int)RFM_CDMA_BC10 ) && ( cfg->req == RFC_REQ_DEFAULT_GET_DATA ) && !ret_val )
  { *ptr = &(rf_card_wtr3925_3dlca_naeukr_1100_rx_on_rfm_device_4_cdma_bc10_device_info);  ret_val = TRUE; }

  if ( ( cfg->rx_tx == RFC_CONFIG_RX ) && ( cfg->logical_device == RFM_DEVICE_5 ) && ( cfg->alternate_path == 0 /*Warning: not specified*/ ) && ( cfg->band == (int)RFM_CDMA_BC10 ) && ( cfg->req == RFC_REQ_DEFAULT_GET_DATA ) && !ret_val )
  { *ptr = &(rf_card_wtr3925_3dlca_naeukr_1100_rx_on_rfm_device_5_cdma_bc10_device_info);  ret_val = TRUE; }

  if ( ( cfg->rx_tx == RFC_CONFIG_TX ) && ( cfg->logical_device == RFM_DEVICE_7 ) && ( cfg->alternate_path == 0 /*Warning: not specified*/ ) && ( cfg->band == (int)RFM_CDMA_BC10 ) && ( cfg->req == RFC_REQ_DEFAULT_GET_DATA ) && !ret_val )
  { *ptr = &(rf_card_wtr3925_3dlca_naeukr_1100_tx_on_rfm_device_7_cdma_bc10_device_info);  ret_val = TRUE; }

  if ( ( cfg->rx_tx == RFC_CONFIG_RX ) && ( cfg->logical_device == RFM_DEVICE_0 ) && ( cfg->alternate_path == 0 /*Warning: not specified*/ ) && ( cfg->band == (int)RFM_CDMA_BC14 ) && ( cfg->req == RFC_REQ_DEFAULT_GET_DATA ) && !ret_val )
  { *ptr = &(rf_card_wtr3925_3dlca_naeukr_1100_rx_on_rfm_device_0_cdma_bc14_device_info);  ret_val = TRUE; }

  if ( ( cfg->rx_tx == RFC_CONFIG_RX ) && ( cfg->logical_device == RFM_DEVICE_1 ) && ( cfg->alternate_path == 0 /*Warning: not specified*/ ) && ( cfg->band == (int)RFM_CDMA_BC14 ) && ( cfg->req == RFC_REQ_DEFAULT_GET_DATA ) && !ret_val )
  { *ptr = &(rf_card_wtr3925_3dlca_naeukr_1100_rx_on_rfm_device_1_cdma_bc14_device_info);  ret_val = TRUE; }

  if ( ( cfg->rx_tx == RFC_CONFIG_TX ) && ( cfg->logical_device == RFM_DEVICE_6 ) && ( cfg->alternate_path == 0 /*Warning: not specified*/ ) && ( cfg->band == (int)RFM_CDMA_BC14 ) && ( cfg->req == RFC_REQ_DEFAULT_GET_DATA ) && !ret_val )
  { *ptr = &(rf_card_wtr3925_3dlca_naeukr_1100_tx_on_rfm_device_6_cdma_bc14_device_info);  ret_val = TRUE; }

  if ( ( cfg->rx_tx == RFC_CONFIG_RX ) && ( cfg->logical_device == RFM_DEVICE_0 ) && ( cfg->alternate_path == 0 /*Warning: not specified*/ ) && ( cfg->band == (int)RFM_CDMA_BC15 ) && ( cfg->req == RFC_REQ_DEFAULT_GET_DATA ) && !ret_val )
  { *ptr = &(rf_card_wtr3925_3dlca_naeukr_1100_rx_on_rfm_device_0_cdma_bc15_device_info);  ret_val = TRUE; }

  if ( ( cfg->rx_tx == RFC_CONFIG_RX ) && ( cfg->logical_device == RFM_DEVICE_1 ) && ( cfg->alternate_path == 0 /*Warning: not specified*/ ) && ( cfg->band == (int)RFM_CDMA_BC15 ) && ( cfg->req == RFC_REQ_DEFAULT_GET_DATA ) && !ret_val )
  { *ptr = &(rf_card_wtr3925_3dlca_naeukr_1100_rx_on_rfm_device_1_cdma_bc15_device_info);  ret_val = TRUE; }

  if ( ( cfg->rx_tx == RFC_CONFIG_TX ) && ( cfg->logical_device == RFM_DEVICE_6 ) && ( cfg->alternate_path == 0 /*Warning: not specified*/ ) && ( cfg->band == (int)RFM_CDMA_BC15 ) && ( cfg->req == RFC_REQ_DEFAULT_GET_DATA ) && !ret_val )
  { *ptr = &(rf_card_wtr3925_3dlca_naeukr_1100_tx_on_rfm_device_6_cdma_bc15_device_info);  ret_val = TRUE; }

  if ( ( cfg->rx_tx == RFC_CONFIG_TX ) && ( cfg->logical_device == RFM_DEVICE_6 ) && ( cfg->alternate_path == 0 /*Warning: not specified*/ ) && ( ( cfg->band == (int)RFM_CDMA_BC1 )||  ( cfg->band == (int)RFM_CDMA_BC14 )||  ( cfg->band == (int)RFM_CDMA_BC15 )) && ( ( cfg->req == RFC_REQ_ENABLE_FBRX )||  ( cfg->req == RFC_REQ_ENABLE_XPT_CAPTURE )) && !ret_val )
  { *ptr = &(rf_card_wtr3925_3dlca_naeukr_1100_cdma_en_et_cal0_enable_fbrx_enable_xpt_capture_device_info);  ret_val = TRUE; }

  if ( ( cfg->rx_tx == RFC_CONFIG_TX ) && ( cfg->logical_device == RFM_DEVICE_6 ) && ( cfg->alternate_path == 0 /*Warning: not specified*/ ) && ( ( cfg->band == (int)RFM_CDMA_BC1 )||  ( cfg->band == (int)RFM_CDMA_BC14 )||  ( cfg->band == (int)RFM_CDMA_BC15 )) && ( ( cfg->req == RFC_REQ_DISABLE_FBRX )||  ( cfg->req == RFC_REQ_DISABLE_XPT_CAPTURE )) && !ret_val )
  { *ptr = &(rf_card_wtr3925_3dlca_naeukr_1100_cdma_dis_et_cal0_disable_fbrx_disable_xpt_capture_device_info);  ret_val = TRUE; }

  if ( ( cfg->rx_tx == RFC_CONFIG_TX ) && ( cfg->logical_device == RFM_DEVICE_7 ) && ( cfg->alternate_path == 0 /*Warning: not specified*/ ) && ( ( cfg->band == (int)RFM_CDMA_BC0 )||  ( cfg->band == (int)RFM_CDMA_BC10 )) && ( ( cfg->req == RFC_REQ_ENABLE_FBRX )||  ( cfg->req == RFC_REQ_ENABLE_XPT_CAPTURE )) && !ret_val )
  { *ptr = &(rf_card_wtr3925_3dlca_naeukr_1100_cdma_en_et_cal2_fbrx_path_dedicated_iq_enable_fbrx_enable_xpt_capture_device_info);  ret_val = TRUE; }

  if ( ( cfg->rx_tx == RFC_CONFIG_TX ) && ( cfg->logical_device == RFM_DEVICE_7 ) && ( cfg->alternate_path == 0 /*Warning: not specified*/ ) && ( ( cfg->band == (int)RFM_CDMA_BC0 )||  ( cfg->band == (int)RFM_CDMA_BC10 )) && ( ( cfg->req == RFC_REQ_DISABLE_FBRX )||  ( cfg->req == RFC_REQ_DISABLE_XPT_CAPTURE )) && !ret_val )
  { *ptr = &(rf_card_wtr3925_3dlca_naeukr_1100_cdma_dis_et_cal2_fbrx_path_dedicated_iq_disable_fbrx_disable_xpt_capture_device_info);  ret_val = TRUE; }

  if (ret_val == FALSE)
  {
    RF_MSG_5( RF_ERROR, "RFC detected error. Unsupported params combo provided by calling tech: device %d band %d altp %d rxtx %d reqtype %d",
              cfg->logical_device, cfg->band, cfg->alternate_path, cfg->rx_tx, cfg->req );
  }

  return ret_val;
}

boolean rfc_wtr3925_3dlca_naeukr_1100_cdma_ag::band_split_cfg_data_get( rfc_cfg_params_type *cfg, rfc_band_split_info_type **ptr )
{
  boolean ret_val = FALSE;

  if (NULL==ptr)
  {
    return FALSE;
  }

  if (NULL==cfg)
  {
    *ptr = NULL;
    return FALSE;
  }

  *ptr = NULL;

  return ret_val;
}

