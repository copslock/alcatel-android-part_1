/* Copyright (C) 2016 Tcl Corporation Limited */

#ifndef RFC_WTR3925_SSKU_3100_WCDMA_CONFIG_AG
#define RFC_WTR3925_SSKU_3100_WCDMA_CONFIG_AG


#ifdef __cplusplus
extern "C" {
#endif

/*
WARNING: This file is auto-generated.

Generated using: D:\Builds\MDM9645\TH.2.1-00597_2015_10_06_12_22_47\modem_proc\rfc_thor\common\etc\rfc_autogen.exe
Generated from:  v5.17.703 of C:\Temp\Downloads\RFC_HWSWCD_v5.17.703.xlsm
*/

/*=============================================================================

          R F C     A U T O G E N    F I L E

GENERAL DESCRIPTION
  This file is auto-generated and it captures the configuration of the RF Card.

Copyright (c) 2015 Qualcomm Technologies Incorporated.  All Rights Reserved.

$Header: //components/rel/rfc_thor.mpss/2.2.1/rf_card/rfc_na_wtr3925_ssku_3100_idol4_pro/wcdma/inc/rfc_na_wtr3925_ssku_3100_wcdma_config_ag.h#7 $


=============================================================================*/

/*=============================================================================
                           INCLUDE FILES
=============================================================================*/
#include "comdef.h"

#include "rfc_msm_typedef.h"
#include "rfc_common.h"
#include "rfc_wcdma_data.h"



class rfc_na_wtr3925_ssku_3100_wcdma_ag:public rfc_wcdma_data
{
public:
  static rfc_wcdma_data * get_instance();
    boolean sig_cfg_data_get( rfc_cfg_params_type *cfg, rfc_sig_cfg_type **ptr );
    boolean devices_cfg_data_get( rfc_cfg_params_type *cfg, rfc_device_info_type **ptr );
    boolean band_split_cfg_data_get( rfc_cfg_params_type *cfg, rfc_band_split_info_type **ptr );
    boolean get_wcdma_properties(rfc_wcdma_properties_type **ptr);

protected:
  rfc_na_wtr3925_ssku_3100_wcdma_ag(void);  /*  Constructor  */
};


#ifdef __cplusplus
}
#endif



#endif


