/* Copyright (C) 2016 Tcl Corporation Limited */

#ifndef RFC_NA_WTR3925_SSKU_3100_CMN_AG
#define RFC_NA_WTR3925_SSKU_3100_CMN_AG


#ifdef __cplusplus
extern "C" {
#endif

/*
WARNING: This file is auto-generated.

Generated using: D:\Builds\MDM9645\TH.2.1-00597_2015_10_06_12_22_47\modem_proc\rfc_thor\common\etc\rfc_autogen.exe
Generated from:  v5.17.703 of C:\Temp\Downloads\RFC_HWSWCD_v5.17.703.xlsm
*/

/*=============================================================================

          R F C     A U T O G E N    F I L E

GENERAL DESCRIPTION
  This file is auto-generated and it captures the configuration of the RF Card.

Copyright (c) 2015 Qualcomm Technologies Incorporated.  All Rights Reserved.

$Header: //components/rel/rfc_thor.mpss/2.2.1/rf_card/rfc_na_wtr3925_ssku_3100_idol4_pro/common/inc/rfc_na_wtr3925_ssku_3100_cmn_ag.h#7 $


=============================================================================*/

/*=============================================================================
                           INCLUDE FILES
=============================================================================*/
#include "comdef.h"

#include "rfc_common.h"



typedef enum
{
  RFC_NA_WTR3925_SSKU_3100_TIMING_PA_CTL,
  RFC_NA_WTR3925_SSKU_3100_TIMING_PA_RANGE,
  RFC_NA_WTR3925_SSKU_3100_TIMING_ASM_CTL,
  RFC_NA_WTR3925_SSKU_3100_TIMING_TUNER_CTL,
  RFC_NA_WTR3925_SSKU_3100_TIMING_PAPM_CTL,
  RFC_NA_WTR3925_SSKU_3100_TIMING_TX_TX_RF_ON0,
  RFC_NA_WTR3925_SSKU_3100_TIMING_TX_RX_RF_ON0,
  /*MODIFIED-BEGIN by qiuwei, 2016-04-08,BUG-1917853*/
  //RFC_NA_WTR3925_SSKU_3100_RF_PATH_SEL_00,
  //RFC_NA_WTR3925_SSKU_3100_RF_PATH_SEL_02,
  /*MODIFIED-END by qiuwei,BUG-1917853*/
  RFC_NA_WTR3925_SSKU_3100_RF_PATH_SEL_14,
  RFC_NA_WTR3925_SSKU_3100_RF_PATH_SEL_15,
  RFC_NA_WTR3925_SSKU_3100_RF_PATH_SEL_16,
  RFC_NA_WTR3925_SSKU_3100_RF_PATH_SEL_17,
  RFC_NA_WTR3925_SSKU_3100_RF_PATH_SEL_18,
  RFC_NA_WTR3925_SSKU_3100_RF_PATH_SEL_20,
  RFC_NA_WTR3925_SSKU_3100_RF_PATH_SEL_21,
  RFC_NA_WTR3925_SSKU_3100_RF_PATH_SEL_23,
  RFC_NA_WTR3925_SSKU_3100_RF_PATH_SEL_24,
  RFC_NA_WTR3925_SSKU_3100_RFFE1_CLK,
  RFC_NA_WTR3925_SSKU_3100_RFFE1_DATA,
  RFC_NA_WTR3925_SSKU_3100_RFFE2_CLK,
  RFC_NA_WTR3925_SSKU_3100_RFFE2_DATA,
  RFC_NA_WTR3925_SSKU_3100_RFFE3_CLK,
  RFC_NA_WTR3925_SSKU_3100_RFFE3_DATA,
  RFC_NA_WTR3925_SSKU_3100_RFFE4_CLK,
  RFC_NA_WTR3925_SSKU_3100_RFFE4_DATA,
  RFC_NA_WTR3925_SSKU_3100_RFFE5_CLK,
  RFC_NA_WTR3925_SSKU_3100_RFFE5_DATA,
  RFC_NA_WTR3925_SSKU_3100_GPDATA0_1,
  RFC_NA_WTR3925_SSKU_3100_GPDATA0_0,
  RFC_NA_WTR3925_SSKU_3100_INTERNAL_GNSS_BLANK,
  RFC_NA_WTR3925_SSKU_3100_TX_GTR_TH,
  RFC_NA_WTR3925_SSKU_3100_PA_IND,
  RFC_NA_WTR3925_SSKU_3100_SIG_NUM,
  RFC_NA_WTR3925_SSKU_3100_SIG_INVALID,
}wtr3925_ssku_3100_na_sig_type;


#ifdef __cplusplus

#include "rfc_common_data.h"

class rfc_na_wtr3925_ssku_3100_cmn_ag:public rfc_common_data
{
  public:
    uint32 sig_info_table_get(rfc_signal_info_type **rfc_info_table);
    rfc_phy_device_info_type* get_phy_device_cfg( void );
    rfc_logical_device_info_type* get_logical_device_cfg( void );
    boolean get_logical_path_config(rfm_devices_configuration_type* dev_cfg);
    const rfm_devices_configuration_type* get_logical_device_properties( void );
    boolean get_cmn_properties(rfc_cmn_properties_type **ptr);
    static rfc_common_data * get_instance(rf_hw_type rf_hw);
    boolean rfc_get_remapped_device_info
     (
       rfc_cal_device_remap_info_type *source_device_info,
       rfc_cal_device_remap_info_type *remapped_device_info
     );


  protected:
    rfc_na_wtr3925_ssku_3100_cmn_ag(rf_hw_type rf_hw);
};

#endif   /*  __cplusplus  */


#ifdef __cplusplus
}
#endif



#endif


