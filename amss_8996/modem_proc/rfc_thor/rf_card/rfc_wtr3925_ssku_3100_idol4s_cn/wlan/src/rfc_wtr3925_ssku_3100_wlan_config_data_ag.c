
/*
WARNING: This file is auto-generated.

Generated using: rfc_autogen.exe
Generated from:  V5.20.780.2 of RFC_HWSWCD.xlsm
*/

/*=============================================================================

          R F C     A U T O G E N    F I L E

GENERAL DESCRIPTION
  This file is auto-generated and it captures the configuration of the RF Card.

Copyright (c) 2015 Qualcomm Technologies Incorporated.  All Rights Reserved.

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/rfc_thor/rf_card/rfc_wtr3925_ssku_3100/wlan/src/rfc_wtr3925_ssku_3100_wlan_config_data_ag.c#1 $ 


=============================================================================*/

/*=============================================================================
                           INCLUDE FILES
=============================================================================*/
#include "comdef.h"

#include "rfc_wtr3925_ssku_3100_cmn_ag.h" 
#include "rfc_common.h" 
#include "rfcom.h" 
#include "wtr3925_typedef_ag.h" 
#include "qfe4335_port_typedef_ag.h" 
#include "qfe4345_port_typedef_ag.h" 
#include "rfdevice_coupler.h" 



rfc_device_info_type rf_card_wtr3925_ssku_3100_wlan_device_info = 
{
  RFC_ENCODED_REVISION, 
  0 /* Warning: Not Specified */,   /* Modem Chain */
  0 /* Warning: Not Specified */,   /* NV Container */
  RFC_INVALID_PARAM /* Warning: Not Specified */,   /* Antenna */
  0,  /* NUM_DEVICES_TO_CONFIGURE */
  {
    /* The following is dummy FILLER information, to keep from having an empty array.
     An empty array causes compilation issues on certain compilers. */
    {
      RFDEVICE_COUPLER,
      GEN_COUPLER /*  */,  /* NAME */
      0 /*Warning: Not specified*/,  /* DEVICE_MODULE_TYPE_INSTANCE */
      0 /*Warning: Not specified*/,  /* PHY_PATH_NUM */
      {
        0  /* Orig setting:  */,  /* INTF_REV */
        (int)RFDEVICE_COUPLER_ATTENUATION_INVALID,  /* ATTEN_FWD */
        (int)RFDEVICE_COUPLER_ATTENUATION_INVALID,  /* ATTEN_REV */
        (int)RFDEVICE_COUPLER_DIRECTION_INVALID,  /* POSITION */
        0,  /* Array Filler */
        0,  /* Array Filler */
      },
    },
  },
};


rfc_sig_info_type rf_card_wtr3925_ssku_3100_wlan_sig_cfg = 
{
  RFC_ENCODED_REVISION, 
  {
    { (int)RFC_WTR3925_SSKU_3100_RF_PATH_SEL_02,   { RFC_HIGH, 0 }, {RFC_LOW, 0 }  },
    { (int)RFC_SIG_LIST_END,   { RFC_LOW, 0 }, {RFC_LOW, 0 } }
  },
};


