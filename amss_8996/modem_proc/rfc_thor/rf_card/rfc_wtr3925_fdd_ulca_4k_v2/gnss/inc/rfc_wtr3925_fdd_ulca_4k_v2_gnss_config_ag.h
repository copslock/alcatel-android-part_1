
#ifndef RFC_WTR3925_FDD_ULCA_4K_V2_GNSS_CONFIG_AG
#define RFC_WTR3925_FDD_ULCA_4K_V2_GNSS_CONFIG_AG


#ifdef __cplusplus
extern "C" {
#endif

/*
WARNING: This file is auto-generated.

Generated using: rfc_autogen.exe
Generated from:  V5.20.780 of RFC_HWSWCD.xlsm
*/

/*=============================================================================

          R F C     A U T O G E N    F I L E

GENERAL DESCRIPTION
  This file is auto-generated and it captures the configuration of the RF Card.

Copyright (c) 2015 Qualcomm Technologies Incorporated.  All Rights Reserved.

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/rfc_thor/rf_card/rfc_wtr3925_fdd_ulca_4k_v2/gnss/inc/rfc_wtr3925_fdd_ulca_4k_v2_gnss_config_ag.h#1 $ 


=============================================================================*/

/*=============================================================================
                           INCLUDE FILES
=============================================================================*/
#include "comdef.h"

#include "rfc_msm_typedef.h" 
#include "rfc_common.h" 
#include "rfc_gnss_data.h" 



class rfc_wtr3925_fdd_ulca_4k_v2_gnss_ag:public rfc_gnss_data
{
public:
  static rfc_gnss_data * get_instance();
    boolean sig_cfg_data_get( rfc_cfg_params_type *cfg, rfc_sig_cfg_type **ptr );
    boolean devices_cfg_data_get( rfc_cfg_params_type *cfg, rfc_device_info_type **ptr );
    boolean band_split_cfg_data_get( rfc_cfg_params_type *cfg, rfc_band_split_info_type **ptr );

protected:
  rfc_wtr3925_fdd_ulca_4k_v2_gnss_ag(void);  /*  Constructor  */
};


#ifdef __cplusplus
}
#endif



#endif


