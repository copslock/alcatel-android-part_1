#ifndef RFC_HWID_H
#define RFC_HWID_H

/*===========================================================================


      R F  D r i v e r  C o m m o n  H a r d w a r e  I d e n t i f i e r

                            H e a d e r  F i l e

DESCRIPTION
  This file contains the Hardware IDs for RF Cards used across multiple
  layers of the RF driver.

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None

Copyright (c) 2013 - 2015 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
============================================================================*/

/*===========================================================================

                           EDIT HISTORY FOR FILE

  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

  $Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/rfc_thor/api/rfc_hwid.h#1 $

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
07/17/15   fhuo    Added card association
07/02/15   jfc     Added RF_HW_WTR3925_TP160_3S
04/27/15   Saul    CRAT Updates
03/31/15   fhuo    Added RF_HW_WTR3925_3DLCA_APAC11_3100v2
                         RF_HW_WTR3925_3DLCA_APAC21_3100v2
                         RF_HW_WTR3925_3DLCA_NAEUKR_3100v2
                         RF_HW_WTR3925_FDD_ULCA_4K_V2
11/10/14   kg      Added RF_HW_WTR3925_WTR4905_SXLTE_DSDA1 
10/10/14   kg      Added RF_HW_WTR3925_3DLCA_NAEUKR 
07/14/14   pl      added RF_HW_WTR3925_SSKU_QFES_DR_DSDS 
06/20/14   cc      Added HWID for TP130_3_CA_73_37/71_17/13_74 
05/12/14   Saul    Added RF_HW_WTR3925_3DLCA_QFE_B11 
04/12/14   Saul    Added RF_HW_WTR3925_WTR4905_SXLTE_DSDA
03/19/14   Saul    Added RF_HW_WTR3925_SXLTE_DSDA_DEV
01/13/14   cc      Added HWID for TP130_CA
09/11/13   jfc     Added HWID for TP100_4B
09/09/13   sd      Added HWID for WTR1625_SGLTE_DSDA
09/03/13   sd      Added HWID for WTR1625_GLOBAL_CA_TP100_4, WTR1605_SGLTE_DSDA,
                   WTR1605_VZ_SV_APT_ASDIV and WTR1605_SGLTE_ASDIV cards
08/26/13   sd      Added HWID for WTR1625_SGLTE_QFE
08/08/13   sd      Added HWID for WTR1625_SGLTE
07/31/13   sd      Removed WTR1605_VZ_SV_APT_1X_MRD. Added WTR1625_APAC_CA_B20
07/22/13   sd      Added VZ_SV_ET and TP100_3 cards
07/12/13   sd      Removed obsolete cards. Added CMCC_QFE and TP100_2 cards
07/09/13   sd      Added HWID for WTR1605_SGLTE
06/05/13   sd      Remove obsoleted cards
05/23/13   sd      Added HWID for WTR1605_RP3
05/10/13   sd      Removed TP100 card. Added HWID for WTR1605_SGLTE_RP1,
                   WTR1605_RP2 and WTR1625_NAEU_CA_LIQUID cards
05/01/13   sd      HW IDs for WTR1625_QFE2720_CA_NA, WTR1625_QFE2720_CA_EU
                   and WTR1605_VZ_SV_APT_1X_MRD cards
04/25/13   dyc     Added HW IDs for TP100_1 and TP100_LATEST
04/17/13   sd      Defining RF_HW_DEFAULT for Off Target builds
04/16/13   sd      Initial revision for Dime RFC HW IDs.

============================================================================*/

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/
#include "comdef.h"

#ifdef __cplusplus
extern "C" {
#endif

/*===========================================================================

                      PUBLIC DATA DECLARATIONS

===========================================================================*/

/* -------------------------------------------------------
** The RF Card Id used in the target
** Note: The Id needs to be sequential
** ------------------------------------------------------- */
typedef enum {
  RF_HW_UNDEFINED                             = (uint8)0,
  RF_TARGET_NONE                              = RF_HW_UNDEFINED,
  RF_HW_WTR3925_GLBL_2DLCA_B11                = (uint8)1,  
  RF_HW_WTR3925_QFEPAD_GLBL_2DLCA             = (uint8)2,
  RF_HW_WTR3925_SSKU_QFES_ET                  = (uint8)5,
  RF_HW_WTR3925_QFE_RGNL_NAEUKR               = (uint8)6, 
  RF_HW_WTR3925_GLBL_2DLCA_4345               = (uint8)7,
  RF_HW_WTR3925_SSKU_QFES_DR_DSDS             = (uint8)10,
  RF_HW_WTR3925_SSKU_QFES_1TX_DSDA            = (uint8)11,
  RF_HW_WTR1605_NA2_APT                       = (uint8)12,
  RF_HW_WTR1605_EU1_APT                       = (uint8)13,
  RF_HW_WTR1605_ATT_CA_APT                    = (uint8)14,
  RF_HW_WTR1605_KR_CA_APT                     = (uint8)15,
  RF_HW_WTR1605_CMCC_APT                      = (uint8)16,
  RF_HW_WTR3925_SSKU_DR_DSDS_ELNA             = (uint8)17,
  RF_HW_WTR1605_APAC11_APT                    = (uint8)18,
  RF_HW_WTR1625_NAEU_CA_LIQUID                = (uint8)19,
  RF_HW_WTR1625_SGLTE_QFE                     = (uint8)20,
  RF_HW_WTR1625_QFE2720_CA_NA                 = (uint8)21,
  RF_HW_WTR1625_QFE2720_CA_EU                 = (uint8)22,
  RF_HW_WTR1625_APAC_CA_B20                   = (uint8)23,
  RF_HW_WTR3925_QFE_GLBL_2DLCA_FLD            = (uint8)24,
  RF_HW_WTR3925_QFE_GLBL_2DLCA_LQD            = (uint8)25,
  RF_HW_WTR3925_QFE_RGNL_CHNA                 = (uint8)26,
  RF_HW_WTR3925_QFE_RGNL_JPN                  = (uint8)28,
  RF_HW_WTR1605_RP2                           = (uint8)29,
  RF_HW_WTR1605_NA1_ET121                     = (uint8)30,
  RF_HW_WTR1625_GLOBAL_CA_QFE                 = (uint8)31,
  RF_HW_WTR1625_RP1                           = (uint8)32,
  RF_HW_WTR3925_WTR4905_SXLTE_DSDA            = (uint8)34,
  RF_HW_WTR3925_HORXD_B5                      = (uint8)35,
  RF_HW_WTR1605_NA1_APT_N5331                 = (uint8)39,
  RF_HW_WTR1605_NA2_APT_N5331                 = (uint8)40,
  RF_HW_WTR1605_EU1_APT_N5331                 = (uint8)41,
  RF_HW_WTR1605_ATT_CA_ET                     = (uint8)42,
  RF_HW_WTR1605_EU_ET                         = (uint8)43,
  RF_HW_WTR1605_NA2_ET                        = (uint8)44,
  RF_HW_WTR1625_APAC                          = (uint8)46,
  RF_HW_WTR1625_GLOBAL_CA_TP100_1             = (uint8)47,
  RF_HW_WTR1605_SGLTE                         = (uint8)51,
  RF_HW_WTR1625_GLOBAL_CA_TP100_LATEST        = (uint8)52,
  RF_HW_WTR1625_VZ_SV1                        = (uint8)53,
  RF_HW_WTR1625_VZ_SV2                        = (uint8)54,
  RF_HW_WTR1625_GLOBAL_CA_TP100_2             = (uint8)55,
  RF_HW_WTR1625_GLOBAL_CA_TP100_3             = (uint8)56,
  RF_HW_WTR1625_GLOBAL_CA_TP100_4             = (uint8)57,
  RF_HW_WTR1625_GLOBAL_CA_TP100_4B            = (uint8)58,
  RF_HW_WTR3925_3DLCA_NAEU3100V2_PW           = (uint8)63,
  RF_HW_WTR3925_3DLCA_APAC11_3100v2_MEM_SHARE = (uint8)64,
  RF_HW_WTR3925_3DLCA_APAC113100_PW           = RF_HW_WTR3925_3DLCA_APAC11_3100v2_MEM_SHARE,
  RF_HW_WTR3925_TP160_0                       = (uint8)100,
  RF_HW_WTR3925_TP160_01                      = (uint8)102,
  RF_HW_WTR3925_TP160_1                       = (uint8)103,
  RF_HW_WTR3925_TP160_2                       = (uint8)104,
  RF_HW_WTR3925_TP160_02                      = (uint8)106,
  RF_HW_WTR3925_TP160_3                       = (uint8)107,
  RF_HW_WTR3925_TP160_3_MEM_SHARE             = (uint8)108,
  RF_HW_WTR3925_TP160_3S                      = RF_HW_WTR3925_TP160_3_MEM_SHARE,  
  RF_HW_WTR3925_QFE_3DLCA_B11                 = (uint8)109,  
  RF_HW_WTR3925_WTR3950_3DLCA_LTEU            = (uint8)110,
  RF_HW_WTR3925_TP160_4                       = (uint8)113,
  RF_HW_WTR3925_TP165_0                       = (uint8)114,
  RF_HW_WTR1625_SGLTE_DSDA                    = (uint8)124,
  RF_HW_WTR3925_TP130_3                       = (uint8)133,
  RF_HW_WTR3925_TP130_3_CA                    = (uint8)134,  
  RF_HW_WTR3925_SGLTE_CA_CHINA                = (uint8)141,
  RF_HW_WTR3925_SG_DSDA_SINGLETX_CA           = (uint8)142,
  RF_HW_WTR1605_SGLTE_DSDA                    = (uint8)151,
  RF_HW_WTR3925_3DLCA_QFE1100                 = (uint8)170,
  RF_HW_WTR3925_3DLCA_QFE1100_FLD             = (uint8)171,
  RF_HW_WTR3925_3DLCA_QFE1100_HORXD           = (uint8)172,
  RF_HW_WTR3925_3DLCA_QFE1100_V2              = (uint8)173,
  RF_HW_WTR3925_3DLCA_NAEUKR_1100             = (uint8)174,
  RF_HW_WTR3925_3DLCA_NAEUKR                  = (uint8)176,
  RF_HW_WTR3925_3DLCA_APAC11                  = (uint8)177,
  RF_HW_WTR3925_3DLCA_APAC21                  = (uint8)178,
  RF_HW_WTR3925_TDD_ULCA_DSDA                 = (uint8)179,
  RF_HW_WTR3925_3DLCA_FLD                     = (uint8)181,
  RF_HW_WTR3925_TDD_ULCA_DSDA_QFE4X           = (uint8)182,
  RF_HW_WTR3925_FDD_ULCA_4K                   = (uint8)183,
  RF_HW_WTR3925_3DLCA_APAC11_1100             = (uint8)184,
  RF_HW_WTR3925_3DLCA_ET_TDS                  = (uint8)185,
  RF_HW_WTR3925_FDD_ULCA_4K_V2                = (uint8)188,
  RF_HW_WTR3925_TDD_ULCA_V2                   = (uint8)189,
  RF_HW_EFS_CARD                              = (uint8)191, /* EFS Card. RESERVED DO NOT CHANGE! */
  RF_HW_WTR3925_TDD_ULCA_3P                   = (uint8)197,
  RF_HW_WTR3925_TDD_ULCA_V4_2_MEM_SHARE       = (uint8)199,
  RF_HW_WTR3925_TDD_ULCA_V4                   = RF_HW_WTR3925_TDD_ULCA_V4_2_MEM_SHARE,
  RF_HW_WTR3925_TDD_ULCA_V4_2                 = (uint8)201,
  RF_HW_WTR1605_VZ_SV_QFE2320                 = (uint8)203,
  RF_HW_WTR1625_NAEU                          = (uint8)207,
  RF_HW_WTR3925_QFE_3DLCA_ET_HB_B11           = (uint8)209,
  RF_HW_WTR3925_3DLCA_NAEUKR_3100v2           = (uint8)210,
  RF_HW_WTR3925_3DLCA_APAC11_3100v2           = (uint8)211,
  RF_HW_WTR3925_3DLCA_APAC21_3100v2           = (uint8)212,
  RF_HW_WTR3925_3DLCA_APAC11_ANTS             = (uint8)217,
  RF_HW_WTR3925_3DLCA_NA_VZ                   = (uint8)219,
  RF_HW_WTR3925_SSKU_3100                     = (uint8)220,
  RF_HW_WTR3925_SSKU_3100_IDOL4S_CN           = (uint8)240,
  RF_HW_WTR3925_SSKU_3100_IDOL4_PRO           = (uint8)243, //254-243 MODIFIED by zhangxianzhu, 2016-05-25,BUG-2162840
  RF_HW_WTR3925_3DLCA_NAEUKR_4K               = (uint8)221,
  RF_HW_WTR3925_CHN_ULCA_3P                   = (uint8)222,
  RF_HW_WTR3925_SSKU_PAMID                    = (uint8)224,
  RF_HW_WTR3925_WTR4905_SXLTE_DSDA1           = (uint8)234,
  RF_HW_WTR1625_NAEU_CA                       = (uint8)246,
  RF_HW_WTR1625_APAC_CA                       = (uint8)247,
  RF_HW_WTR1605_VZ_SV_APT                     = (uint8)249,
  RF_HW_WTR3925_SXLTE_DSDA_DEV                = (uint8)250,
  RF_HW_WTR1605_SGLTE_ASDIV                   = (uint8)251,
  RF_HW_WTR3925_SSKU_QFES                     = (uint8)252,
  RF_HW_WTR3925_2DLCA                         = (uint8)253,

  /* Add any new HW ID before this line */
  RF_HW_MAX,
  RF_HW_DEFAULT = RF_HW_MAX, /* Default card for Off-Target Builds */
} rf_hw_type;

#ifdef __cplusplus
}
#endif

#endif  /* RFC_HWID_H */
