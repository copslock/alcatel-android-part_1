/*!
  @file
  fw_mvc_intf.h

  @brief
  Modem Voltage Control external interface

*/

/*===========================================================================

  Copyright (c) 2015 Qualcomm Technologies Incorporated. 
  All rights reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

#ifndef FW_MVC_INTF_H
#define FW_MVC_INTF_H

/*===========================================================================

                               CONSTANTS

===========================================================================*/

/* Number of possible PMIC commands, limited by SAW interface. Only using two
   for now */
#define FW_MVC_MAX_PMIC_CMDS  4

/* PMIC action to set the VDD_MSS rail to a new voltage */
#define FW_MVC_PMIC_CMD_IDX_VOLTAGE 0
/* PMIC action to trigger the PBS sequence for VDD_MSS */
#define FW_MVC_PMIC_CMD_IDX_PBS     1

/* Special return code from MCPM, indicates we are executing in the MCPM UT
   environment */
#define FW_MVC_MCPM_UNIT_TEST_ENV (0xFF)


/*===========================================================================

                                TYPES

===========================================================================*/

/* List of clients who can make MVC requests */
typedef enum
{
  FW_MVC_CLIENT_LTE_UL,
  FW_MVC_CLIENT_LTE_NLIC = FW_MVC_CLIENT_LTE_UL,
  FW_MVC_CLIENT_LTE_DL,
  FW_MVC_CLIENT_LTE_QICE = FW_MVC_CLIENT_LTE_DL,
  FW_MVC_CLIENT_LAST
} fw_mvc_client_e;

/* Voltage corners used for making requests */
typedef enum
{
  FW_MVC_CORNER_SVS2,
  FW_MVC_CORNER_SVS,
  FW_MVC_CORNER_NOM,
  FW_MVC_CORNER_TURBO,
  FW_MVC_CORNER_LAST
} fw_mvc_corner_e;

/* Return code from MCPM, telling FW whether it is ok to change the rail */
typedef enum
{
  FW_MVC_MPMU_APPROVED,
  FW_MVC_MPMU_DENIED
} fw_mvc_return_code_e;

/* PMIC data for a single command */
typedef struct
{
  uint8 valid;
  uint8 data;
} fw_mvc_pmic_cmd_t;

/* All PMIC data for a single corner */
typedef struct
{
  uint32 uvolt;
  fw_mvc_pmic_cmd_t pmic_cmds[FW_MVC_MAX_PMIC_CMDS];
} fw_mvc_voltage_lut_entry_t;

/* Input parameters for FW->SW request interface */
typedef struct
{
  fw_mvc_corner_e req;
  fw_mvc_client_e client;
} fw_mvc_fw_sw_input_t;

/* Output parameters for FW->SW request interface */
typedef struct
{
  fw_mvc_return_code_e ret;
  fw_mvc_corner_e volt_lut_index;
} fw_mvc_fw_sw_output_t;

/* MCPM command types */
typedef enum
{
  FW_MVC_MCPM_ENTER_MVC,
  FW_MVC_MCPM_EXIT_MVC,
  FW_MVC_MCPM_PUSH_REQ,
  FW_MVC_MCPM_UNIT_TEST_CMD
} fw_mvc_mcpm_cmd_e;

/* Input parameters for SW->FW push interface */
typedef struct
{
  fw_mvc_mcpm_cmd_e cmd_type;
  uint32 cmd_data;
} fw_mvc_sw_fw_input_t;

/* Output parameters for SW->FW push interface */
typedef struct
{
  boolean success;
} fw_mvc_sw_fw_output_t;

/* Top-level FW<->MCPM shared memory interface */
typedef struct fw_mvc_smem_t
{
  /* FW->SW interface data */
  fw_mvc_fw_sw_input_t fw_input;
  fw_mvc_fw_sw_output_t sw_output;

  /* SW->FW interface data */
  fw_mvc_sw_fw_input_t sw_input;
  fw_mvc_sw_fw_output_t fw_output;

  /* Mutex to track MPMU activity during mode switches */
  qurt_sem_t mpmu_lock;

  /* Table of voltage and PMIC data per corner */
  fw_mvc_voltage_lut_entry_t volt_lut[FW_MVC_CORNER_LAST];
} fw_mvc_smem_t;


#endif /* FW_MVC_INTF_H */

