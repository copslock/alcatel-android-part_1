/*!
  @file
  cmnfw_qsh_ext.h

  @brief
  QSH dump structure for common FW registers and states

*/

/*===========================================================================

  Copyright (c) 2015 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

#ifndef CMNFW_QSH_EXT_H
#define CMNFW_QSH_EXT_H

/*! @brief Common FW QSH dump Major version */
#define CMNFW_QSH_MAJOR_VER 2

/*! @brief Common FW QSH dump Minor version */
#define CMNFW_QSH_MINOR_VER 0

/*! @brief Number of VUSS's */
#define CMNFW_QSH_DUMP_NUM_VUSS              (9)

/*! @brief Number of RXFE WB chains */
#define CMNFW_QSH_DUMP_NUM_RXFE_WB_CHAINS    (7)

/*! @brief Number of RXFE NB chains */
#define CMNFW_QSH_DUMP_NUM_RXFE_NB_CHAINS    (11)

/*! @brief Number of MCDMA HW channels */
#define CMNFW_QSH_DUMP_NUM_MCDMA_HW_CHANNELS (4)

/*! @brief Number of MCDMA SW channels */
#define CMNFW_QSH_DUMP_NUM_MCDMA_SW_CHANNELS (10)

/*! @brief Number of MTC DBG ports */
#define CMNFW_QSH_DUMP_NUM_MTC_DBG_PORTS     (16)

/*! @brief Number of TDEC */
#define CMNFW_QSH_DUMP_NUM_TDEC              (2)

/*! @brief Number of DEMBACK */
#define CMNFW_QSH_DUMP_NUM_DEMBACK           (2)

/*! @brief Number of TX chains */
#define CMNFW_QSH_DUMP_NUM_TX_CHAINS         (2)

/***************************************************************************/
/*                   VPE QSH dump                                         */
/***************************************************************************/

/*! @brief VPE registers for state save per VPE */
typedef struct 
{
  uint32 VUSS_ENABLE;
  uint32 VUSS_CLK_CTL;
  uint32 VUSS_IRQ_PEND_RDATA;
  uint32 VUSS_CLK_EN;
  uint32 VUSS_MODE_CTL;
  uint32 VUSS_PWR_STATUS;

} cmnfw_qsh_dump_vpe_reg_t;

/*! @brief Contents of VPE PC trace */
typedef struct
{
  uint32 PCTRACE_BUF_STAT;
  uint32 PCTRACE_BUF_CURRENT[4];
  uint32 PCTRACE_BUF_TARGET[4];

} cmnfw_qsh_dump_vpe_pctrace_t;

/*! @brief VPE QSH dump structure */
typedef struct
{
  cmnfw_qsh_dump_vpe_reg_t reg;                  /*!< Per-VUSS registers */
  uint32 pre_nmi_MSS_STMR;                       /*!< STMR capture before issuing NMI */
  cmnfw_qsh_dump_vpe_pctrace_t pctrace_pre_nmi;  /*!< PC trace captured before issuing NMI */
  uint32 post_nmi_MSS_STMR;                      /*!< STMR capture after issuing NMI */

} cmnfw_qsh_dump_vpe_t;

/***************************************************************************/
/*                   RXFE QSH dump                                        */
/***************************************************************************/

/*! @brief RXFE QSH dump structure */
typedef struct
{
  uint32 MSS_MODEM_DTR_PLL1_STATUS;
  uint32 MSS_MODEM_DTR_PLL2_STATUS;
  uint32 RXFE_WBw_ADCMUX_CFG[CMNFW_QSH_DUMP_NUM_RXFE_WB_CHAINS];
  uint32 RXFE_NBn_WBMUX_CFG[CMNFW_QSH_DUMP_NUM_RXFE_NB_CHAINS];
  uint32 RXFE_TOP_STATUS;
  uint32 DTR_RXFE_XO_TRIGGER_RXCHn[CMNFW_QSH_DUMP_NUM_RXFE_WB_CHAINS];
  uint32 DTR_RXFE_EN;
  uint32 DTR_BRDG_RXFE_WRTR_XFER_CTL;
  uint32 DTR_BRDG_RXFE_WRTR_STATUS;
  uint32 DTR_BRDG_FBRX_WRTR_XFER_CTL;
  uint32 DTR_BRDG_FBRX_WRTR_STATUS;
  uint32 CLK_PROC_ODIV;
  
} cmnfw_qsh_dump_rxfe_t;

/***************************************************************************/
/*                   MCDMA QSH dump                                       */
/***************************************************************************/

/*! @brief MCDMA QSH dump structure */
typedef struct
{
  uint32 MCDMA_ENABLE;
  uint32 MCDMA_LOG_CH_BUSY;
  uint32 MCDMA_PHY_CH_CTL;
  uint32 MCDMA_LOG_HW_CHn_DBG[CMNFW_QSH_DUMP_NUM_MCDMA_HW_CHANNELS];
  uint32 MCDMA_LOG_SW_CHn_DBG[CMNFW_QSH_DUMP_NUM_MCDMA_SW_CHANNELS];
  uint32 MCDMA_TS_STATUS_WORD0;

} cmnfw_qsh_dump_mcdma_t;


/***************************************************************************/
/*                   MTC DBG QSH dump                                     */
/***************************************************************************/

/*! @brief MTC DBG QSH dump structure */
typedef struct
{
  uint32 DBG_ENABLE;
  uint32 DBG_LOG_STATUS;
  uint32 DBG_SW_LOG_STATUS;
  uint32 DBG_ERR_IRQ_STATUS0;

} cmnfw_qsh_dump_mtc_dbg_t;


/***************************************************************************/
/*                   CCS QSH dump                                         */
/***************************************************************************/

/*! @brief CCS QSH dump structure */
typedef struct
{
  uint32 CCS_MSTS;
  uint32 CCS_MCTL;
  uint32 CCS_DEMBACK_PWRUP;
  uint32 CCS_DEMBACK_PWRON;

} cmnfw_qsh_dump_ccs_t;

/***************************************************************************/
/*                   DEMBACK TDEC QSH dump                                */
/***************************************************************************/

/*! @brief TDEC registers */
typedef struct
{
  uint32 TDEC_CLK_CTL;
  uint32 TDEC_SW_CLK_EN;
  uint32 TDEC_MODE_CTL;
  uint32 TDEC_HW_IDLE;

} cmnfw_qsh_dump_tdec_t;

/*! @brief DEMBACK registers */
typedef struct
{
  uint32 DEMBACK_EN;
  uint32 DEMBACK_MODE_SEL;
  uint32 DEMBACK_BRDG_CTL;
  uint32 DEMBACK_BRDG_STATUS;

} cmnfw_qsh_dump_demback_t;

/*! @brief Demback crash dump structure */
typedef struct
{
  cmnfw_qsh_dump_tdec_t    tdec[CMNFW_QSH_DUMP_NUM_TDEC];        /*!< per-TDEC registers */
  cmnfw_qsh_dump_demback_t demback[CMNFW_QSH_DUMP_NUM_DEMBACK];  /*!< per-DEMBACK registers */

} cmnfw_qsh_dump_demback_tdec_t;

/***************************************************************************/
/*                   TXFE QSH dump                                        */
/***************************************************************************/

/*! @brief DTR TX register dump at crash */
typedef struct
{
  uint32 DTR_TXFEc_EN;
  uint32 DTR_TXFEc_SAMPLE_COUNT_LOAD_NSAMPLE;
  uint32 DTR_TXFEc_SAMPLE_COUNT_LOAD_SAMPLE_COUNT_UPDATE_STATUS;
  uint32 DTR_TXFE_DPRDB_ROTATOR_CFG_CHAINc;

} cmnfw_qsh_dump_txfe_dtr_tx_reg_t;

/*! @brief DTR BRDG register dump at crash */
typedef struct
{
  uint32 DTR_BRDG_TXCONFIG_CH_STATUS;

} cmnfw_qsh_dump_txfe_dtr_brdg_reg_t;

/*! @brief Misc register dump at crash */
typedef struct
{
  uint32 DAC_CLK_CFG;

} cmnfw_qsh_dump_txfe_misc_reg_t;

/*! @brief fatal error register dump structure */
typedef struct
{
  /*! @brief DTR TXFE register dump */
  cmnfw_qsh_dump_txfe_dtr_tx_reg_t dtr_tx_dump[CMNFW_QSH_DUMP_NUM_TX_CHAINS];

  /*! @brief DTR BRDG register dump */
  cmnfw_qsh_dump_txfe_dtr_brdg_reg_t dtr_brdg_dump;

  /*! @brief Misc register dump */
  cmnfw_qsh_dump_txfe_misc_reg_t misc_dump;

} cmnfw_qsh_dump_txfe_t;

/***************************************************************************/
/*                   Overall QSH dump structure                            */
/***************************************************************************/

/*! @brief QSH dump structure */
typedef struct
{
  cmnfw_qsh_dump_vpe_t          vuss_dump[CMNFW_QSH_DUMP_NUM_VUSS]; /*!< VUSS state dump */
  cmnfw_qsh_dump_rxfe_t         rxfe_dump;                          /*!< RXFE register dump */
  cmnfw_qsh_dump_mcdma_t        mcdma_dump;                         /*!< MCDMA register dump */
  cmnfw_qsh_dump_mtc_dbg_t      mtc_dbg_dump;                       /*!< MTC DBG register dump */
  cmnfw_qsh_dump_ccs_t          ccs_dump;                           /*!< CCS register dump */
  cmnfw_qsh_dump_demback_tdec_t demback_tdec_dump;                  /*!< DEMBACK TDEC register dump */
  cmnfw_qsh_dump_txfe_t         txfe_dump;                          /*!< TXFE register dump */

} cmnfw_qsh_dump_int_s;

/*! @brief QSH dump structure definition */
typedef struct
{
  char                 version[64]; /*!< Common FW version */
  boolean              valid;       /*!< If the dump structure is valid */
  cmnfw_qsh_dump_int_s dumps;       /*!< QSH dumps */

} cmnfw_qsh_dump_s;

/***************************************************************************/
/*                   TLV structure                                         */
/***************************************************************************/

/*! @brief Common FW QSH dump enum */
typedef enum
{
  CMNFW_QSH_DUMP_TAG_0 = 0

} cmnfw_qsh_dump_tag_e;

/*! @brief Define the header locally. This should match QSH def "qsh_dump_tag_hdr_s" */
typedef struct
{
  /*! set using dump_tag enum exported by client */
  uint16  dump_tag;
  
  /*! size of dump struct in bytes */
  uint16  dump_struct_size_bytes;

} cmnfw_qsh_dump_tag_hdr_s;

/*! @brief Common FW QSH dump TLV structure */
typedef struct
{
  cmnfw_qsh_dump_tag_hdr_s hdr;             /*!< QSH dump Header */
  cmnfw_qsh_dump_s         cmnfw_qsh_dump;  /*!< QSH dump */

} cmnfw_qsh_dump_tag_0_s;

#endif // CMNFW_QSH_EXT_H
