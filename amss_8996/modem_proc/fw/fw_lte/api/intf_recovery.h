/*!
  @file
  intf_recovery.h

  @brief
  This file contains data structures for recovery and wakeup requests
  and confirmations.
  
 
*/

/*===========================================================================

  Copyright (c) 2009 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //source/qcom/qct/modem/fw/cpl/thor/MPSS.TH.2.0.C1.9/image/fw_lte/api/intf_recovery.h#34 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
===========================================================================*/
#ifndef INTF_RECOVERY_H
#define INTF_RECOVERY_H

#include "intf_common.h"
#include "intf_config_app.h"

#include "lte_l1_types.h"
#include "lte_LL1_ue_types.h"

/*===========================================================================

      Constants

===========================================================================*/


/*===========================================================================

      Macros

===========================================================================*/

   

/*===========================================================================

      Typedefs

===========================================================================*/

/*! @brief
 *  indicate to ML a task stall condition has been detected
 *  and LL is in recovery mode
 */
typedef enum
{
  LTE_LL1_SYS_RECOV_IND_CAUSE_NONE = 0, 
  LTE_LL1_SYS_RECOV_IND_CAUSE_RFD_TQ_STALL =1,
  LTE_LL1_SYS_RECOV_IND_CAUSE_DLCCH = 2,
  LTE_LL1_SYS_RECOV_IND_CAUSE_DF_SCHD_STALL = 3,
  LTE_LL1_SYS_RECOV_IND_CAUSE_TX_STALL = 4,
  LTE_LL1_SYS_RECOV_IND_CAUSE_MIPS_OVERUN = 5, 
  LTE_LL1_SYS_RECOV_IND_CAUSE_VPE_ERR_IRQ = 6, 
  LTE_LL1_SYS_RECOV_IND_CAUSE_VPE_CMD_VALID = 7,
  LTE_LL1_SYS_RECOV_IND_CAUSE_VPE_WAKEUP_MARGIN_MISS = 8,
  LTE_LL1_SYS_RECOV_IND_CAUSE_VPE_SYNCP_MISS = 9,
  LTE_LL1_SYS_RECOV_IND_CAUSE_SYNCP_REPROGRAM_TIMELINE_OVERRUN = 10,
  LTE_LL1_SYS_RECOV_IND_CAUSE_PDSCH_DEMAP_DELAY = 11, 
  LTE_LL1_SYS_RECOV_IND_CAUSE_TX_VU_ERR_IRQ = 12,
  LTE_LL1_SYS_RECOV_IND_CAUSE_RX_ON_TOGGLE = 13,
  LTE_LL1_SYS_RECOV_IND_CAUSE_PDCCH_CMD_RSP = 14,  
  LTE_LL1_SYS_RECOV_IND_CAUSE_SRCH_MEAS_LATE = 15,
  LTE_LL1_SYS_RECOV_IND_CAUSE_SRCH_MEAS_CMD_VALID = 16,
  LTE_LL1_SYS_RECOV_IND_CAUSE_SRCH_MEAS_DONE_PING_PONG_MISMATCH = 17,
  LTE_LL1_SYS_RECOV_IND_CAUSE_OVERRUN_VSRC_DUMP = 18,
  LTE_LL1_SYS_RECOV_IND_CAUSE_CCS_PDSCH_STALL = 19,  
  LTE_LL1_SYS_RECOV_IND_CAUSE_NLIC_VU_ERR_IRQ = 20,
  LTE_LL1_SYS_RECOV_IND_CAUSE_TX_TIMELINE_OVERRUN = 21,
  LTE_LL1_SYS_RECOV_IND_CAUSE_TX_VU_CMD_VALID = 22,
  LTE_LL1_SYS_RECOV_IND_CAUSE_NLIC_VU_CMD_VALID = 23,
  LTE_LL1_SYS_RECOV_IND_CAUSE_FBRX_VU_CMD_VALID = 24,
  LTE_LL1_SYS_RECOV_IND_CAUSE_FBRX_VU_ERR_IRQ = 25,
  LTE_LL1_SYS_RECOV_IND_CAUSE_MAX_CNT = 26
} lte_LL1_sys_recovery_ind_cause_e;


// --------------------------------------------------------------------------
//
// Recovery/Stall indication
//
// --------------------------------------------------------------------------

/*! @brief
 *  indicate to ML a task stall condition has been detected
 *  and LL is in recovery mode
 */
typedef struct {
  uint16 frame;
  uint8 subframe;
  uint8 recovery_state;  //XXX- rename to "recovery_cause" once ML intf updated
} lte_LL1_sys_recovery_ind_struct;

typedef struct {
  msgr_hdr_struct_type              msg_hdr;      ///< message router header
  lte_LL1_sys_recovery_ind_struct   msg_payload;
} lte_LL1_sys_recovery_ind_msg_struct;

#endif

