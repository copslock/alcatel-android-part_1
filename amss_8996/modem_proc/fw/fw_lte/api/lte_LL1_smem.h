/*!
  @file
  lte_LL1_smem.h

  @brief
  Shared memory interface for LTE LL1.

  @detail
  Constants and structures for LTE LL1 FW-SW shared memory.

*/

/*===========================================================================

  Copyright (c) 2009 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //source/qcom/qct/modem/fw/cpl/thor/MPSS.TH.2.0.C1.9/image/fw_lte/api/lte_LL1_smem.h#34 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
===========================================================================*/

#ifndef LTE_LL1_SMEM_H
#define LTE_LL1_SMEM_H

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/
#include "modem_fw_memmap.h"
#include "intf_prs_sm.h"
#include "qsh_clt_lfw.h"
#include "intf_prs_fd_logging_types.h"
#include "lte_LL1_mvc.h"


/*===========================================================================

                   EXTERNAL DEFINITIONS AND TYPES

===========================================================================*/

// XXX TODO: These should be removed, or defined in terms of FW SMEM header.

/* uart utility driver on ARM9 */
//#define Q6_SMEM_UART_PHYS_START_ADDR 0x03a30000

/* PLT server buffer address */
#define Q6_SMEM_PLT_PHYS_START_ADDR  FW_SMEM_LTE_PLT_ADDR
#define Q6_SMEM_LTE_START_ADDR  FW_SMEM_LTE_ADDR

/* Enum for VPE clock setting for NLIC modes */
typedef enum
{
  LTE_LL1_SMEM_NLIC_CLK_SETTING_SVS2 = 0,
  LTE_LL1_SMEM_NLIC_CLK_SETTING_SVS,
  LTE_LL1_SMEM_NLIC_CLK_SETTING_NOMINAL,
  LTE_LL1_SMEM_NLIC_CLK_SETTING_TURBO,
  LTE_LL1_SMEM_NLIC_CLK_SETTING_MAX,
  LTE_LL1_SMEM_NLIC_CLK_SETTING_INVALID,
  LTE_LL1_SMEM_NLIC_CLK_SETTING_DEFAULT,
  _LTE_LL1_SMEM_NLIC_CLK_SETTING_FORCE_32_BIT = 0x7FFFFFFF
}lte_LL1_smem_nlic_clk_setting_e;

typedef struct
{
/* Whether MVC is enabled or disabled */
  boolean lte_LL1_mvc_disabled;

  /* PRS smem region */
  lte_LL1_prs_occasion_meas_s prs_smem;

  /* VPE clock settings for NLIC mode */
  lte_LL1_smem_nlic_clk_setting_e nlic_clock_setting; 
  
  /* VPE clock table for MVC */
  lte_LL1_mvc_clock_table_s mvc_clock_table;

  /* PRS FD Logging */
  lte_LL1_prs_fd_logging_db_s prs_fd_log_db;

  /* TDP smem region */
  lte_LL1_opcrs_meas_db_s opcrs_smem;
  
#ifdef TEST_MINIDUMP
  /* Mini Dump structure*/
  lte_LL1_miniset_main_db_t lte_mini_dump_smem;
#endif

  uint32 qsh_dump_ptr;
  
}lte_LL1_smem_t;

#endif /* LTE_LL1_SMEM_H */
