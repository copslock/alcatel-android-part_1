#ifndef RFCOMMON_AUTOPIN_H
#define RFCOMMON_AUTOPIN_H

/*!
  @file
  rfcommon_autopin.h

  @brief
  This module contains internal prototypes and definitions used by common AutoPin.
*/

/*==============================================================================

  Copyright (c) 2015 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

==============================================================================*/

/*==============================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

  $Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/rfa/rf/common/rf/autopin/inc/rfcommon_autopin.h#1 $

when       who     what, where, why
--------   ---     -------------------------------------------------------------
10/29/15   whz     Remove timers completely
08/03/15   kg      Add header "rfcommon_core_txlin_types.h" 
06/04/15   whz     Initial version

==============================================================================*/

#include "rfcommon_autopin_api.h"
#include "rfcommon_core_txlin_types.h"


void rfcommon_autopin_get_nv_info(rfm_mode_enum_type rfm_tech);


rflm_tech_id_t rfcommon_autopin_mc_convert_rfm_mode_to_rflm_tech(rfm_mode_enum_type rfm_tech);


#endif /* RFCOMMON_AUTOPIN_H */
