#ifndef __GSMHWIO_H__
#define __GSMHWIO_H__
/*===========================================================================

  Copyright (c) 2012-2015 Qualcomm Technologies, Inc.
  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.


$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/geran/gcommon/inc/gsmhwio.h#1 $ $DateTime: 2016/03/28 23:02:53 $ $Author: mplcsds1 $

when       who       what, where, why
--------   ---       --------------------------------------------------------
14/03/12   pg        Inital version

===========================================================================*/ 

#include "geran_variation.h"
#include "msm.h"
#endif
