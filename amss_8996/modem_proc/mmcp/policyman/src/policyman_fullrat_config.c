/**
  @file policyman_fullrat_config.c

  @brief
*/

/*
    Copyright (c) 2015 QUALCOMM Technologies Incorporated.
    All Rights Reserved.
    Qualcomm Technologies Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this
  document are confidential and proprietary information of
  QUALCOMM Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of QUALCOMM Technologies Incorporated.

  $Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/mmcp/policyman/src/policyman_fullrat_config.c#2 $
  $DateTime: 2016/07/06 23:33:19 $
  $Author: abanand $
*/

#include "mre_efs.h"
#include "mre_xml.h"
#include "mre_rules.h"
#include "mre_set.h"

#include "policyman_dbg.h"
#include "policyman_device_config.h"
#include "policyman_fullrat_config.h"
#include "policyman_phone_events.h"
#include "policyman_policy.h"
#include "policyman_rules.h"
#include "policyman_serving_system.h"
#include "policyman_set.h"
#include "policyman_timer.h"

#include <stringl/stringl.h>

#define FULLRAT_TIMER_ID 1000
#define FULLRAT_TIMER_DEFAULT 120
#define ACQFAIL_CNT_DEFAULT 1

struct fullrat_config_t
{
  policyman_timer_t      *pTimer;
  uint8                   scan_cnt;
  sys_modem_as_id_e_type  asubs_id;
  policyman_set_t        *pActionSet;
};

STATIC fullrat_config_t g_FullRatConfigs[POLICYMAN_NUM_SUBS];

/*-------- policyman_fullrat_config_find --------*/
STATIC fullrat_config_t *policyman_fullrat_config_find(
  sys_modem_as_id_e_type asubs_id
)
{
  return &g_FullRatConfigs[asubs_id];
}

/*-------- policyman_fullrat_config_init_per_subs --------*/
void policyman_fullrat_config_init_per_subs(
  sys_modem_as_id_e_type asubs_id
)
{
  fullrat_config_t *pFullRatCfg;

  pFullRatCfg = policyman_fullrat_config_find(asubs_id);

  pFullRatCfg->pTimer     = NULL;
  pFullRatCfg->scan_cnt   = ACQFAIL_CNT_DEFAULT;
  pFullRatCfg->asubs_id   = asubs_id;
  pFullRatCfg->pActionSet = NULL;
}


/*-------- policyman_fullrat_config_init --------*/
void policyman_fullrat_config_init(
  void
)
{
  size_t            subsIndex;

  for (subsIndex = 0; subsIndex < POLICYMAN_NUM_SUBS; subsIndex++)
  {
    policyman_fullrat_config_init_per_subs(subsIndex);
  }  
}

/*-------- policyman_fullrat_config_deinit_per_subs --------*/
void policyman_fullrat_config_deinit_per_subs(
  sys_modem_as_id_e_type asubs_id
)
{
  fullrat_config_t *pFullRatCfg;

  pFullRatCfg = policyman_fullrat_config_find(asubs_id);

  policyman_timer_stop(pFullRatCfg->pTimer);
  REF_CNT_OBJ_RELEASE_IF(pFullRatCfg->pTimer);
  REF_CNT_OBJ_RELEASE_IF(pFullRatCfg->pActionSet);
  
}

/*-------- policyman_fullrat_config_deinit --------*/
void policyman_fullrat_config_deinit(
  void
)
{
  size_t            subsIndex;

  for (subsIndex = 0; subsIndex < POLICYMAN_NUM_SUBS; subsIndex++)
  {
    policyman_fullrat_config_deinit_per_subs(subsIndex);
  }  
}

/*-------- policyman_scancnt_exceeded_config --------*/
STATIC boolean policyman_scancnt_exceeded_config(
  policyman_state_t *pState,
  fullrat_config_t  *pFullRatCfg
)
{
  boolean limitWasReached = FALSE;
  
  if (pFullRatCfg->scan_cnt > 0)
  {
    limitWasReached = (policyman_ss_get_min_acq_fail_count(pState, pFullRatCfg->asubs_id) >= pFullRatCfg->scan_cnt);   
  }
  
  return limitWasReached;
}

/*-------- policyman_fullrat_conditions_are_met --------*/
STATIC boolean policyman_fullrat_conditions_are_met(
  policyman_state_t      *pState,
  sys_modem_as_id_e_type  asubs_id
)
{
  fullrat_config_t *pFullRatCfg;
  boolean           have_location;
  boolean           timer_expired;
  boolean           oos_scancnt_exceeded;
  boolean           hysteresis_expired;

  if (!policyman_ue_is_online(pState))
  {
    
    POLICYMAN_MSG_MED_0("FullRatMode check : UE is not ONLINE");
    return FALSE;
  }
  
  pFullRatCfg          = policyman_fullrat_config_find(asubs_id);
  have_location        = policyman_find_subs_with_location_info(pState, &asubs_id);
  timer_expired        = policyman_timer_is_expired(pFullRatCfg->pTimer);
  oos_scancnt_exceeded = policyman_scancnt_exceeded_config(pState, pFullRatCfg);
  hysteresis_expired   = timer_expired || oos_scancnt_exceeded;
  
  POLICYMAN_MSG_HIGH_3( "FullRatMode check : have_location: %d, timer_expired: %d, oos_scancnt_exceeded: %d",
                        have_location,
                        timer_expired,
                        oos_scancnt_exceeded);
  
  /* Eligible to enter fullrat mode if no service and timer expired/oos scan count exceeded threshold*/
  return (!have_location && hysteresis_expired);
}

/*-------- policyman_fullrat_execute_actions --------*/
STATIC void policyman_fullrat_execute_actions(
  policy_execute_ctx_t *pCtx
)
{
  fullrat_config_t *pFullRatCfg;

  pFullRatCfg = policyman_fullrat_config_find(pCtx->asubs_id);
  POLICYMAN_MSG_HIGH_0("policyman_fullrat_execute_actions : Entering Full Rat mode");
  
  mre_actionset_execute(pFullRatCfg->pActionSet, pCtx);
}

/*-------- policyman_get_fullrat_timer --------*/
policyman_timer_t * policyman_get_fullrat_timer(
  sys_modem_as_id_e_type  asubs_id
)
{
  fullrat_config_t *pFullRatCfg;

  pFullRatCfg = policyman_fullrat_config_find(asubs_id);

  return pFullRatCfg->pTimer;  
}

/*-------- policyman_fullrat_enter_check --------*/
STATIC void policyman_fullrat_enter_check(
  policy_execute_ctx_t  *pCtx
)
{
  if (policyman_fullrat_conditions_are_met(pCtx->pState, pCtx->asubs_id))
  {
    policyman_fullrat_execute_actions(pCtx);
  }
}

/*-------- policyman_fullrat_config_evaluate --------*/
void policyman_fullrat_config_evaluate(
  policy_execute_ctx_t   *pCtx
)
{
  size_t                 numSim          = 0;
  sys_modem_as_id_e_type subs;

  (void)policyman_get_current_num_sim(&numSim);

  for (subs = SYS_MODEM_AS_ID_1; subs < numSim; subs++)
  {
    POLICYMAN_MSG_HIGH_1("policyman_fullrat_config_evaluate for subs %d", subs);
    pCtx->asubs_id = subs;
    policyman_fullrat_enter_check(pCtx);
  }
}

/*-------- policyman_fullrat_config_notify_service --------*/
void policyman_fullrat_config_notify_service(
  policyman_state_t      *pState,
  boolean                 haveService,
  sys_modem_as_id_e_type  subsId
)
{
  policyman_timer_t      *pTimer;
  policyman_timer_t      *pOtherSubsTimer;
  sys_modem_as_id_e_type  otherSubsId;

  otherSubsId = (subsId == SYS_MODEM_AS_ID_1) ?
                SYS_MODEM_AS_ID_2 :
                SYS_MODEM_AS_ID_1;
  
  pTimer = policyman_get_fullrat_timer(subsId);
  pOtherSubsTimer = policyman_get_fullrat_timer(otherSubsId);
  
  if (!haveService &&
      policyman_ue_is_online(pState))
  {
    policyman_timer_start(pTimer);
  }
  else
  {
    /* stop fullrat timers on both subs if UE have service*/
    policyman_timer_stop(pTimer);
    policyman_timer_stop(pOtherSubsTimer);    
  }
}

/*-------- policyman_fullrat_timer_expired --------*/
STATIC void policyman_fullrat_timer_expired(
  policyman_timer_t *pTimer
)
{
  policy_execute_ctx_t    ctx;
  sys_modem_as_id_e_type  asubs_id;

  asubs_id = policyman_timer_get_subs(pTimer);
    
  ctx.pState    = policyman_state_get_state();
  ctx.pItemSet  = policyman_itemset_new();
  ctx.asubs_id  = asubs_id;

  POLICYMAN_MSG_HIGH_1("policyman_fullrat_timer_expired for asubs_id %d", asubs_id);
  
  policyman_fullrat_enter_check(&ctx);
  
  policyman_cfgitem_update_items(ctx.pItemSet, NULL);
  ref_cnt_obj_release(ctx.pItemSet);
    
  policyman_timer_set_handled(pTimer);
}
/*=============================================================================
  <define_fullrat_config>
=============================================================================*/
typedef struct
{
  POLICYMAN_ACTION_BASE;
 
  timetick_type      timer_secs;
  uint8              scan_cnt;
  policyman_set_t   *pActionSet;
} fullrat_config_action_t;
  
/*-------- policyman_named_fullrat_action_dtor --------*/
STATIC void policyman_fullrat_action_dtor(
  void  *pObj
)
{
  fullrat_config_action_t *pAction = (fullrat_config_action_t *) pObj;
  
  REF_CNT_OBJ_RELEASE_IF(pAction->pActionSet);
  policyman_action_dtor(pAction);
}

/*-------- policyman_fullrat_config_execute --------*/
STATIC boolean policyman_fullrat_config_execute(
  mre_action_t const  *pAction,
  void                *pCtx
)
{
  fullrat_config_action_t  *pFRAction = (fullrat_config_action_t *) pAction;
  policyman_policy_t       *pPolicy   = (policyman_policy_t *)pFRAction->pPolicy;
  fullrat_config_t         *pFullRatCfg;
  policyman_timer_t        *pTimer;

  pFullRatCfg = policyman_fullrat_config_find(pPolicy->subs);

  /* release Timer and actionset in full rat config if already exists. 
        we are now going to update with recently set configuration */
  policyman_fullrat_config_deinit_per_subs(pPolicy->subs);
  
  pTimer = policyman_timer_create_fixed_timer( NULL,
                                               pPolicy->subs,
                                               FULLRAT_TIMER_ID,
                                               pFRAction->timer_secs,
                                               policyman_fullrat_timer_expired);
  
  pFullRatCfg->pTimer     = pTimer;
  pFullRatCfg->scan_cnt   = pFRAction->scan_cnt;
  pFullRatCfg->pActionSet = pFRAction->pActionSet;
  ref_cnt_obj_add_ref(pFullRatCfg->pActionSet);
  
  return TRUE;

}

/*-------- policyman_fullrat_get_actions --------*/
STATIC void policyman_fullrat_get_actions(
  mre_policy_t                   *pPolicy,
  policyman_xml_element_t const  *pElem,
  mre_set_t                     **ppActionSet
)
{
  mre_xml_element_t const *pChild;

  // Expecting only one child in full rat config
  pChild = mre_xml_get_child(pElem, 0);

  // The child should be actions 
  if (mre_xml_tag_is(pChild, "actions"))
  {
    mre_rule_get_actions(pPolicy, ppActionSet, pChild);
  }      
}

/*-------- policyman_fullrat_config_new --------*/
mre_status_t policyman_fullrat_config_new(
  policyman_xml_element_t const  *pElem,
  mre_policy_t                   *pPolicy,
  mre_action_t                  **ppAction
)
{
  fullrat_config_action_t    *pAction = NULL;
  char const                 *pStr;
 
  pAction = (fullrat_config_action_t *) policyman_mem_alloc(sizeof(fullrat_config_action_t));
  ref_cnt_obj_init(pAction, policyman_fullrat_action_dtor);
  
  pStr = mre_xml_get_attribute(pElem, "timer_secs");
  
  if (NULL != pStr)
  {
    pAction->timer_secs = atoi(pStr);
  }
  if (0 == pAction->timer_secs)
  {  
    pAction->timer_secs = FULLRAT_TIMER_DEFAULT;
  }

  pStr = mre_xml_get_attribute(pElem, "scan_fail_cnt");
  if (NULL != pStr)
  {
    pAction->scan_cnt = atoi(pStr);
  }
  else 
  {  
    pAction->scan_cnt = ACQFAIL_CNT_DEFAULT;
  }

  
  policyman_fullrat_get_actions((mre_policy_t *)pPolicy, pElem, &pAction->pActionSet);

  pAction->pmSubsId = SUBS_THIS;

  pAction->execute = policyman_fullrat_config_execute;

  *ppAction = (mre_action_t *) pAction;

  return MRE_STATUS_SUCCESS;
}

/*=============================================================================
  <enter_fullrat>
=============================================================================*/
/*-------- policyman_fullrat_enter_execute --------*/
STATIC boolean policyman_fullrat_enter_execute(
  mre_action_t const  *pAction,
  void                *pCtx
)
{
  policyman_fullrat_execute_actions(pCtx);
  
  return TRUE;
}


/*-------- policyman_fullrat_enter_new --------*/
mre_status_t policyman_fullrat_enter_new(
  policyman_xml_element_t const *pElem,
  mre_policy_t                  *pPolicy,
  mre_action_t                 **ppAction
)
{
  mre_action_t  *pAction;
  
  pAction = (mre_action_t *) policyman_mem_alloc(sizeof(mre_action_t));
  ref_cnt_obj_init(pAction, policyman_action_dtor);
 
  pAction->execute = policyman_fullrat_enter_execute;
  
  *ppAction = pAction;
  return MRE_STATUS_SUCCESS;
  
}
