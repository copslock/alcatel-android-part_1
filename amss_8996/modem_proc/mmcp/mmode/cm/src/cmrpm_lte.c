/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

   RPM   M O D U L E  - LTE

GENERAL DESCRIPTION
  This module contains the Call Manager RPM functionality for maintain a list of permanent EMM failure events.

  The  RPM modile  is responsible for:
  1. Maintaining the RPM related parameters 
  2. Receiving the info related to EMM failure from NAS and processing
     the info as per the RPM ploicy.


INITIALIZATION AND SEQUENCING REQUIREMENTS
  cmrpm_lte_init() must be called to initialize this module before any other
  function declared in this module is being called.


Copyright (c) 2014 - 2015 by Qualcomm Technologies INCORPORATED. All Rights Reserved.

Export of this technology or software is regulated by the U.S. Government.
Diversion contrary to U.S. law prohibited.

*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/


/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.


when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
01/14/15   sk     Integrated RPM feature for EMM reject cause 
01/14/15   sk     Add interface for storing/retrieving the values into efs files

===========================================================================*/


/****************************************************************************

 Organization of the file:

    The file is divided into multiple sections.
    You can jump from one sections to the other by searching for / followed
    by 2 *'s. The order of objects defined is as follows:

        includes
        forward declarations
        #defines
        enums
        macros

        functions
        - Internal [Common, 1x only, GW only]
        - External [Common, 1x only, GW only]

****************************************************************************/

/**--------------------------------------------------------------------------
** Includes
** --------------------------------------------------------------------------
*/

#include "policyman.h"

#include "cmrpm_lte.h"
#include "cmmmgsdi.h"
#include "cmtaski.h"
#include "cmdbg.h"     /* Interface to CM debug services */
#include "mm.h"
#include "cm_msgr_msg.h"
#include "cmnv.h"


#ifdef CM_DEBUG
#error code not present
#endif

/**--------------------------------------------------------------------------
** Forward declarations
** --------------------------------------------------------------------------
*/

#define CM_RPM_LTE_MAX_EVENT_COUNTER    3 /* Max number of HLOS initiated resets per Tmax duration */
#define CM_RPM_LTE_MAX_LOGTIME          120 /* Timer value in min to be started following permanent EMM reject */

#define IS_VALID_TAC(X)  ( (X) != 0xFFFE && (X) != 0 )
#define INVALID_TAC      0xFFFE
#define INVALID_PLMN     0xFFFFFFFF



/**--------------------------------------------------------------------------
** Functions - external
** --------------------------------------------------------------------------
*/  
       
/*===========================================================================

FUNCTION cmrpm_lte_ptr

DESCRIPTION
  Return a pointer to the one and only RPM object.


DEPENDENCIES
  none

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
cmrpm_lte_s_type *cmrpm_lte_ptr( void )
{
  static cmrpm_lte_s_type    cmrpm_lte_local;
  /* The one and only lte rpm object */

  return &cmrpm_lte_local;

} /* cmrpm_lte_ptr() */

/*===========================================================================

FUNCTION cmrpm_lte_is_rpm_enabled

DESCRIPTION
  Returns whether rpm is enabled, based on the rpm pointer.
  Meant for use by external functions that don't have much to do with RPM
  but need to know if it is enabled.

DEPENDENCIES
  cmrpm_lte_init should be called before this function.

RETURN VALUE
  TRUE if the rpm pointer indicates that rpm is enabled
  FALSE otherwise

SIDE EFFECTS
  none

===========================================================================*/
boolean cmrpm_lte_is_rpm_enabled( void )
{
  return (cmrpm_lte_ptr()->is_rpm_enabled);
  
} /* cmrpm_lte_is_rpm_enabled() */


/*==========================================================================

FUNCTION cmrpm_lte_read_rpm_enabled_flag

DESCRIPTION
  Function that reads RPM Enabled Flag from PM

RETURN VALUE
  boolean  flag indicating whether RPM Enabled Flag read
  from the PM successfully (TRUE) or unsuccessfully(FALSE).

DEPENDENCIES
  None
===========================================================================*/
static boolean cmrpm_lte_get_pm_rpm_enabled_flag(boolean  *enabled_flag_ptr)
{
  boolean            status = FALSE;
   
  /*
  ** Read the RPM Enabled Flag
  */
  status = policyman_get_boolean_value("cm:rpm_enabled", enabled_flag_ptr);

  CM_MSG_HIGH_1("read_rpm_enabled_flag: PM RPM Enabled Flag is %d", *enabled_flag_ptr);

  return status;

} /* cmrpm_lte_read_rpm_enabled_flag() */


/*===========================================================================

FUNCTION cmrpm_lte_read_efs_sys_time

DESCRIPTION
 This function reads system time from EFS, that was written at last power-down 
 And updates local variable sys_time_efs with the time read from EFS.
 If time read from EFS is greater than uptime returned by API  
 cm_util_get_curr_systime(), we can consider coin cell battery as absent and
 use EFS itself for maintaining time across resets.

DEPENDENCIES
  RPM object must have already been initialized with cmrpm_lte_init().

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
boolean cmrpm_lte_read_efs_sys_time(boolean is_update)
{
  cmrpm_lte_s_type              *rpm_ptr        = cmrpm_lte_ptr();
  uint32                        sys_time_pwr_up ;
  int32                         size_of_timer   = sizeof(uint32);
  boolean                       status = TRUE;
 

  /* Read RPM SYS TIME from NV-EFS */
  cmnv_efs_read(CMNV_EFS_ID_RPM_SYS_TIME,
                (byte *)&sys_time_pwr_up,
                 &size_of_timer);

  CM_MSG_HIGH_3("read_efs: efs system time: %d, curr_systime: %d is_update: %d",
                         sys_time_pwr_up, cm_util_get_curr_systime(), is_update);
  
  /* If time read from EFS(time stored at last power-down) is greater than the 
  ** uptime(time since recent power-up), we can consider time is not persistent
  ** across resets and coin cell battery support is not there. 
  ** In this case, update time to local variable and set is_coin_cell_support 
  ** to FALSE
  */
  if((is_update == TRUE) && size_of_timer > 0 && sys_time_pwr_up > cm_util_get_curr_systime())
  {
    rpm_ptr->is_coin_cell_support = FALSE;
    rpm_ptr->sys_time_efs = (uint32)sys_time_pwr_up;
  }
  else if(size_of_timer < 0)
  {
    status = FALSE;
  }
  CM_MSG_HIGH_2("read_efs: rpm_ptr->sys_time_efs: %d, coin_cell_support: %d",
                                           rpm_ptr->sys_time_efs,
                                           rpm_ptr->is_coin_cell_support);
  return status;
}/* cmrpm_lte_read_efs_sys_time() */

/*===========================================================================

FUNCTION cmrpm_lte_write_efs_sys_time

DESCRIPTION
  This function writes system time at power-down in EFS

DEPENDENCIES
  RPM object must have already been initialized with cmrpm_lte_init().

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
boolean cmrpm_lte_write_efs_sys_time(
  sys_oprt_mode_e_type  prev_oprt_mode,

  sys_oprt_mode_e_type  curr_oprt_mode
)
{
  boolean                  status = TRUE;
  
  cmrpm_lte_s_type         *rpm_ptr        = cmrpm_lte_ptr();
  uint32                    sys_time_pwr_down;
 
  CM_MSG_HIGH_3("write_efs: oprt_mode_chg prev: %d curr: %d rpm_enable: %d",
      prev_oprt_mode, curr_oprt_mode, cmrpm_lte_is_rpm_enabled());
  
  CM_MSG_HIGH_3("write_efs: sys_time_efs: %d coin_cell_support: %d curr_systime: %d",
  	               rpm_ptr->sys_time_efs, rpm_ptr->is_coin_cell_support, cm_util_get_curr_systime());
  
  /* Check if oprt mode switch is valid and CMRPM is enabled. If so and 
  ** no coin cell battery support, write current system time to EFS
  */
  if((TRUE == cmrpm_lte_is_rpm_enabled()) &&
       prev_oprt_mode == SYS_OPRT_MODE_ONLINE && 
      ((curr_oprt_mode == SYS_OPRT_MODE_OFFLINE)||
       (curr_oprt_mode == SYS_OPRT_MODE_OFFLINE_CDMA)||
       (curr_oprt_mode == SYS_OPRT_MODE_LPM)||
       (curr_oprt_mode == SYS_OPRT_MODE_FTM)))
  {
    if(!cmrpm_lte_read_efs_sys_time(FALSE))
    {
      CM_ERR_0("write_efs: Can't read RPM EFS INFO");
    }
    else if(!(rpm_ptr->is_coin_cell_support == TRUE &&
              (rpm_ptr->sys_time_efs == cm_util_get_curr_systime())))
    {
      /* Current system time = time from EFS at power-up + time since power-up
           */
      sys_time_pwr_down = (rpm_ptr->sys_time_efs) + cm_util_get_curr_systime();
      CM_MSG_HIGH_1("write_efs: write system time: %d to EFS", sys_time_pwr_down);
	 
      cmnv_efs_write(CMNV_EFS_ID_RPM_SYS_TIME,
                     (byte *)&sys_time_pwr_down,
                      sizeof(uint32));
      status = TRUE;
     } // else if
  }

  return status;

}/* cmrpm_lte_write_efs_sys_time() */


/*===========================================================================

FUNCTION cmrpm_lte_read_efs_rpm_config

DESCRIPTION
  This function reads RPM parameter from EFS

DEPENDENCIES
  RPM object must have already been initialized with cmrpm_lte_init().

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
static boolean cmrpm_lte_read_efs_rpm_info(cmrpm_lte_efs_rpm_info_s_type *rpm_info_ptr )
{
  boolean                  status = TRUE;
  int32                    size_of_efs_info = sizeof(cmrpm_lte_efs_rpm_info_s_type);

  /* Read RPM INFO from NV-EFS */
  cmnv_efs_read(CMNV_EFS_ID_LTE_RPM_INFO, (byte *)rpm_info_ptr, &size_of_efs_info);

  if(size_of_efs_info <= 0)
  {
    status = FALSE;
    CM_ERR_0("read_efs_rpm_info: read CMNV_EFS_ID_LTE_RPM_INFO failed");
  }  

  return status;
}

/*===========================================================================

FUNCTION cmrpm_lte_write_efs_rpm_config

DESCRIPTION
  This function writes RPM parameter into EFS

DEPENDENCIES
  Phone object must have already been initialized with cmph_init().

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
static boolean cmrpm_lte_write_efs_rpm_info(const cmrpm_lte_s_type   *rpm_ptr )
{
  boolean                            status = FALSE;
  cmrpm_lte_efs_rpm_info_s_type       rpm_efs_info;
  uint8                               i=0;

  if(!cmrpm_lte_read_efs_rpm_info(&rpm_efs_info))
  {
    CM_ERR_0("write_efs_rpm_info: Can't read LTE RPM EFS INFO");
  }
  else
  {
    /* Update writable RPM config parameter 
      ** update only fixed parameter such as below
      ** is_rpm_enabled, max_num_reset, max_reset_timer, etc.
      */
    rpm_efs_info.is_rpm_enabled      = rpm_ptr->is_rpm_enabled;
    rpm_efs_info.app_max_num_reset   = rpm_ptr->max_num_reset;
    rpm_efs_info.app_max_reset_timer = rpm_ptr->max_reset_timer;
    rpm_efs_info.app_reset_counter   = rpm_ptr->app_reset.app_reset_counter;
    rpm_efs_info.timer               = rpm_ptr->app_reset.timer;
    rpm_efs_info.tac_counter         = rpm_ptr->tac_counter;

    CM_MSG_HIGH_2("write_efs_rpm_info: app_max_num_reset: %d, app_max_reset_timer: %d min",
      rpm_efs_info.app_max_num_reset, rpm_efs_info.app_max_reset_timer);

    CM_MSG_HIGH_3("write_efs_rpm_info: app_reset_counter: %d, tac_counter: %d, reset_timer: %d",
      rpm_efs_info.app_reset_counter, rpm_efs_info.tac_counter, rpm_efs_info.timer);

    for(i=0; i< MAX_NUM_OF_TAC; i++)
    {
      rpm_efs_info.lte_tai[i].tac = rpm_ptr->lte_tai[i].tac;
      rpm_efs_info.lte_tai[i].plmn = rpm_ptr->lte_tai[i].plmn;
    }

    cmnv_efs_write(CMNV_EFS_ID_LTE_RPM_INFO,
                  (byte *)&rpm_efs_info,
                      sizeof(cmrpm_lte_efs_rpm_info_s_type));
    status = TRUE;

  }
  return status;

}


/*===========================================================================

FUNCTION cmrpm_lte_check_sim_changed

DESCRIPTION
  Check if sim is changed. If SIM changed then reset all RPM timers and counters.
  
DEPENDENCIES
  RPM object must have already been initialized with cmrpm_lte_init().

RETURN VALUE
  boolean 

SIDE EFFECTS
  none

===========================================================================*/
static boolean cmrpm_lte_check_sim_changed( void )
{ 
  cmrpm_lte_s_type             *rpm_ptr        = cmrpm_lte_ptr();
  boolean                       is_sim_changed = FALSE;
  uint8   i = 0;

  if ((mmgsdi_rpm_has_iccid_changed(MMGSDI_SLOT_1, &is_sim_changed) 
        == MMGSDI_SUCCESS) && is_sim_changed)
  {
    CM_MSG_HIGH_0("check_sim_changed: SIM changed, reset RPM counter/timer");

    rpm_ptr->app_reset.timer = 0;
    rpm_ptr->app_reset.app_reset_counter = 0;
    rpm_ptr->tac_counter = 0;

    for(i=0; i< MAX_NUM_OF_TAC; i++)
    {
      rpm_ptr->lte_tai[i].tac = INVALID_TAC;
      sys_plmn_undefine_plmn_id(&(rpm_ptr->lte_tai[i].plmn));
    }
    
    if(cmrpm_lte_write_efs_rpm_info(rpm_ptr) == FALSE)
    {
      CM_ERR_0("check_sim_changed: Can't write RPM EFS INFO");
    }

    return TRUE;
    
  }  
  else
  {
    return FALSE;
  }
}


/*===========================================================================

FUNCTION cmrpm_lte_check_timer_expired

DESCRIPTION
  Check if RPM timers are expired. 
  
DEPENDENCIES
  RPM object must have already been initialized with cmrpm_lte_init().

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
static void cmrpm_lte_check_timer_expired( void )
{
  cmrpm_lte_s_type              *rpm_ptr        = cmrpm_lte_ptr();
  uint32                     curr_system_time   = cm_util_get_curr_systime();
  uint8                      i = 0;

  CM_MSG_MED_3("check_timer: sys_time_efs: %d coin_cell_support: %d curr_systime: %d",
             rpm_ptr->sys_time_efs, rpm_ptr->is_coin_cell_support, curr_system_time);
  
  /* If no coin cell battery, add EFS time to current uptime to get total
  ** system time since power-up
  */
  if(rpm_ptr->is_coin_cell_support == FALSE )
  {
    curr_system_time = (rpm_ptr->sys_time_efs) + curr_system_time;
  }

  /* 
  ** Check APP_RESET_TIMER if it's already expired or not 
  */
  if((rpm_ptr->app_reset.timer != 0) && 
     (rpm_ptr->app_reset.timer <= curr_system_time))
  {
    CM_MSG_HIGH_2("check_timer: App Reset Timer expired - curr_systime:%d, timer:%d",
      curr_system_time, rpm_ptr->app_reset.timer);
    
    /* Update APP_RESET_TIMER to 0 */
    rpm_ptr->app_reset.timer = 0;
    rpm_ptr->app_reset.app_reset_counter = 0;

    /* clear the TAC list */
    rpm_ptr->tac_counter = 0;
    for(i=0; i< MAX_NUM_OF_TAC; i++)
    {
      rpm_ptr->lte_tai[i].tac = INVALID_TAC;
      sys_plmn_undefine_plmn_id(&(rpm_ptr->lte_tai[i].plmn));
    }
    
    if(cmrpm_lte_write_efs_rpm_info(rpm_ptr) == FALSE)
    {
      CM_ERR_0("check_timer: fail to write APP_RESET_TIMER ");
    }    
  } 
}/* cmrpm_lte_check_timer_expired */


/*===========================================================================

FUNCTION cmrpm_lte_init

DESCRIPTION
  Initializes the RPM object.

  This function must be called before the before RPM object
  is being used, in any way, place, or form.

DEPENDENCIES
  none

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
boolean cmrpm_lte_init( void )
{
  boolean                  is_pm_rpm_enabled = FALSE;
  
  boolean                  efs_read_status = FALSE;
  uint8                    i=0;
  cmrpm_lte_s_type         *rpm_ptr       = cmrpm_lte_ptr();

  cmrpm_lte_efs_rpm_info_s_type read_rpm_efs_info;

  /* Initialize all RPM parameters */
  memset(rpm_ptr, 0 , sizeof(cmrpm_lte_s_type));
  memset(&read_rpm_efs_info, 0 , sizeof(cmrpm_lte_efs_rpm_info_s_type));
  rpm_ptr->is_coin_cell_support = TRUE;

  efs_read_status = cmrpm_lte_read_efs_rpm_info(&read_rpm_efs_info);

  if(efs_read_status == TRUE)
  {
    rpm_ptr->is_rpm_enabled = read_rpm_efs_info.is_rpm_enabled;
    rpm_ptr->max_num_reset = read_rpm_efs_info.app_max_num_reset;
    rpm_ptr->max_reset_timer = read_rpm_efs_info.app_max_reset_timer;
  
    CM_MSG_HIGH_3("cmrpm_lte_init: efs is_rpm_enabled: %d efs app_max_num_reset: %d, app_max_reset_timer: %d min", 
      rpm_ptr->is_rpm_enabled, rpm_ptr->max_num_reset, rpm_ptr->max_reset_timer);
  
    rpm_ptr->app_reset.app_reset_counter = read_rpm_efs_info.app_reset_counter;
    rpm_ptr->app_reset.timer             = read_rpm_efs_info.timer;
  
    CM_MSG_HIGH_2("cmrpm_lte_init: efs app_reset_counter: %d, app_reset timer: %d", 
      rpm_ptr->app_reset.app_reset_counter, rpm_ptr->app_reset.timer);
  
    rpm_ptr->tac_counter         = read_rpm_efs_info.tac_counter;
    CM_MSG_HIGH_1("cmrpm_lte_init: efs tac_counter: %d",rpm_ptr->tac_counter);
  }


  /* RPM functionality is only enabled when GCF flag is OFF/FALSE */
      /* RPM functionality is only enabled when RPM SIM exist */
  if((FALSE == (*(cmph_get_gprs_anite_gcf_ptr()))) && \
      cmrpm_lte_get_pm_rpm_enabled_flag(&is_pm_rpm_enabled) && \
      is_pm_rpm_enabled)
  {
      CM_MSG_MED_0("cmrpm_lte_init: RPM Initializing...");
      
      /* Read default system time from EFS at init*/
      cmrpm_lte_read_efs_sys_time(TRUE);

      if(efs_read_status == TRUE)
      {
        rpm_ptr->is_rpm_enabled = (is_pm_rpm_enabled & read_rpm_efs_info.is_rpm_enabled);
        
        /* Check if sim is changed then reset all RPM timers and counters */
        if(rpm_ptr->is_rpm_enabled)
        {
          cmrpm_lte_check_sim_changed();
        }
        
        if(rpm_ptr->tac_counter != 0)
        {
          for(i=0; i< MAX_NUM_OF_TAC; i++)
          {
            rpm_ptr->lte_tai[i].tac = read_rpm_efs_info.lte_tai[i].tac;
            rpm_ptr->lte_tai[i].plmn = read_rpm_efs_info.lte_tai[i].plmn;

            CM_MSG_HIGH_2("cmrpm_lte_init: read efs tac[%d] = %d",i, rpm_ptr->lte_tai[i].tac);
          }
        }
        else
        {
          for(i=0; i< MAX_NUM_OF_TAC; i++)
          {
            rpm_ptr->lte_tai[i].tac = INVALID_TAC;
            sys_plmn_undefine_plmn_id(&(rpm_ptr->lte_tai[i].plmn));
          
            CM_MSG_HIGH_2("cmrpm_lte_init: reset tac[%d] = %d",i, rpm_ptr->lte_tai[i].tac);
          }
        }
        
      }
      /* efs read failed, use default RPM config value */  
      else
      {
        CM_ERR_0("cmrpm_lte_init: Can't read RPM rpm_efs_info");
        rpm_ptr->max_num_reset = CM_RPM_LTE_MAX_EVENT_COUNTER;
        rpm_ptr->max_reset_timer = CM_RPM_LTE_MAX_LOGTIME;

        /* first time read, reset  and write default value to efs file */
        memset(&read_rpm_efs_info, 0 , sizeof(cmrpm_lte_efs_rpm_info_s_type));


        read_rpm_efs_info.app_max_num_reset   = rpm_ptr->max_num_reset;
        read_rpm_efs_info.app_max_reset_timer = rpm_ptr->max_reset_timer;

        for(i=0; i< MAX_NUM_OF_TAC; i++)
        {
          read_rpm_efs_info.lte_tai[i].tac = INVALID_TAC;
          sys_plmn_undefine_plmn_id(&(read_rpm_efs_info.lte_tai[i].plmn));
          
          CM_MSG_HIGH_2("cmrpm_lte_init: write efs tac[%d] = %d",i, 
                              read_rpm_efs_info.lte_tai[i].tac);
        }

        cmnv_efs_write(CMNV_EFS_ID_LTE_RPM_INFO,
                      (byte *)&read_rpm_efs_info,
                           sizeof(cmrpm_lte_efs_rpm_info_s_type));
         CM_MSG_HIGH_0("cmrpm_lte_init: write default CMNV_EFS_ID_LTE_RPM_INFO ");
        
      }
     } 
  else
  {
    CM_MSG_HIGH_1("cmrpm_lte_init: PM RPM Enabled Flag, is_pm_rpm_enabled %d",
                           is_pm_rpm_enabled);

      /* reset the timer value if PM RPM not enabled */
      if(efs_read_status == TRUE  && rpm_ptr->app_reset.timer != 0)
      {
        /* Update APP_RESET_TIMER to 0 */
        rpm_ptr->app_reset.timer = 0;
        rpm_ptr->app_reset.app_reset_counter = 0;
        
        CM_MSG_HIGH_1("cmrpm_lte_init: Reset App Reset Timer timer:%d",
                             rpm_ptr->app_reset.timer);

        /* clear the TAC list */
        rpm_ptr->tac_counter = 0;
        for(i=0; i< MAX_NUM_OF_TAC; i++)
        {
          rpm_ptr->lte_tai[i].tac = INVALID_TAC;
          sys_plmn_undefine_plmn_id(&(rpm_ptr->lte_tai[i].plmn));
        }
        
        if(cmrpm_lte_write_efs_rpm_info(rpm_ptr) == FALSE)
        {
          CM_ERR_0("check_timer: fail to write APP_RESET_TIMER ");
        }
      }

      rpm_ptr->is_rpm_enabled = FALSE;

    }

  /* If RPM is enabled, read all other permanent parameters from EFS */
  if(rpm_ptr->is_rpm_enabled)
  {
    CM_MSG_HIGH_0("cmrpm_lte_init: LTE RPM Parameters loaded");

    /* Check if there is any expired timer, then do processing accordingly */
    cmrpm_lte_check_timer_expired();
  }

  return rpm_ptr->is_rpm_enabled;

} /* cmrpm_lte_init */


/*===========================================================================

FUNCTION cmrpm_lte_set_rpm_parameters_req

DESCRIPTION
  Updates value of  CM RPM parameters and updates the EFS as well.
  If there is any RPM timer running currently it will incorporate new value.

DEPENDENCIES

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
void cmrpm_lte_set_rpm_parameters_req( cm_mm_lte_rpm_parameters_s_type set_cm_rpm_parameters_req)
{
  
  cmrpm_lte_s_type *lte_rpm_ptr = cmrpm_lte_ptr();
  cmrpm_lte_s_type  old_rpm_info;

  cm_mm_set_rpm_parameters_rsp_s_type *cm_rpm_set_params_rsp;

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    
  cm_rpm_set_params_rsp = (cm_mm_set_rpm_parameters_rsp_s_type *)cm_mem_malloc(
	sizeof(cm_mm_set_rpm_parameters_rsp_s_type));

  if(cm_rpm_set_params_rsp == NULL)
  { 
    CM_ERR_0("set_rpm: cm_rpm_set_params_rsp heap exhausted, cm_mem_malloc() returned NULL");
  }

  if(!lte_rpm_ptr->is_rpm_enabled)
  {
    cm_rpm_set_params_rsp->rpm_rsp_ret_val = CM_RPM_FAILED_RPM_DISABLED;
    
    (void) cm_msgr_send( MM_CM_SET_RPM_PARAMETERS_RSP, MSGR_MM_CM,
        (msgr_hdr_s *)cm_rpm_set_params_rsp, sizeof(cm_mm_set_rpm_parameters_rsp_s_type) );
    
    cm_mem_free(cm_rpm_set_params_rsp);

    CM_MSG_HIGH_1("set_rpm: send SET RPM_PARAMETERS_RSP with rsp = %d", \
                     cm_rpm_set_params_rsp->rpm_rsp_ret_val);

    return;
  }

  /* if new_rpm_config is  set to > 16 and > 120, QMI  itself would
         reject the command so CM does not expect this input at all */
  
  if(set_cm_rpm_parameters_req.max_num_app_resets > 16 || \
     set_cm_rpm_parameters_req.avg_mmreject_reset_time > 360 || \
     set_cm_rpm_parameters_req.avg_mmreject_reset_time == 0)
  {
    CM_ERR_2("set_rpm: rpm_parameters out of range max_reset: %d, reset_time: %d",
                   set_cm_rpm_parameters_req.max_num_app_resets,
                   set_cm_rpm_parameters_req.avg_mmreject_reset_time);
    cm_mem_free(cm_rpm_set_params_rsp);
    return;
  }
  
  /* compute new app reset timer if  timer is running */
  if(lte_rpm_ptr->app_reset.timer != 0)
  {
    lte_rpm_ptr->app_reset.timer += (set_cm_rpm_parameters_req.avg_mmreject_reset_time - \
                                     lte_rpm_ptr->max_reset_timer)* CMRPM_SEC_PER_MIN;
  }
  
  /* fill the structure to be written in EFS from provide new config */
  {
    old_rpm_info.max_num_reset = lte_rpm_ptr->max_num_reset;
    old_rpm_info.max_reset_timer = lte_rpm_ptr->max_reset_timer;
    
    lte_rpm_ptr->max_num_reset = set_cm_rpm_parameters_req.max_num_app_resets;
    lte_rpm_ptr->max_reset_timer = set_cm_rpm_parameters_req.avg_mmreject_reset_time;
  }

  if (!cmrpm_lte_write_efs_rpm_info(lte_rpm_ptr))
  {
    CM_ERR_0( "set_rpm: efs write for set_rpm_parameters fail");
    cm_rpm_set_params_rsp->rpm_rsp_ret_val = CM_RPM_FAILED_RPM_DISABLED;
    
    /* write failed, restore old rpm config */
    lte_rpm_ptr->max_num_reset = old_rpm_info.max_num_reset;
    lte_rpm_ptr->max_reset_timer = old_rpm_info.max_reset_timer;
  }
  else
  {
    cm_rpm_set_params_rsp->rpm_rsp_ret_val = CM_RPM_SUCCESS_RPM_ENABLED;
      
    CM_MSG_HIGH_2("set_rpm: set_rpm_parameters updated with max reset = %d, reset timer = %d", \
                  lte_rpm_ptr->max_num_reset, lte_rpm_ptr->max_reset_timer);
  }

  (void) cm_msgr_send( MM_CM_SET_RPM_PARAMETERS_RSP, MSGR_MM_CM,
	                   (msgr_hdr_s *)cm_rpm_set_params_rsp, sizeof(cm_mm_set_rpm_parameters_rsp_s_type) );

  cm_mem_free(cm_rpm_set_params_rsp);

  CM_MSG_HIGH_1("set_rpm: send SET RPM_PARAMETERS_RSP with rsp = %d", \
                   cm_rpm_set_params_rsp->rpm_rsp_ret_val);

  /* Check if there is any expired timer, then do processing accordingly */
  cmrpm_lte_check_timer_expired();

  return;
  
}/* cmrpm_lte_set_rpm_parameters_req */

/*===========================================================================

FUNCTION cmrpm_lte_send_get_rpm_parameters_rsp

DESCRIPTION
  Sends the current value of the CM RPM parameters  to 
  CM clients via MSGR interface MM_CM_GET_RPM_PARAMETERS_RSP.

DEPENDENCIES

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
void cmrpm_lte_send_get_rpm_parameters_rsp( void )
{
  cm_mm_get_rpm_parameters_rsp_s_type *cm_rpm_get_params_rsp;
  
  
  cm_rpm_get_params_rsp = (cm_mm_get_rpm_parameters_rsp_s_type *)cm_mem_malloc(
	sizeof(cm_mm_get_rpm_parameters_rsp_s_type));

  if(cm_rpm_get_params_rsp == NULL)
  { 
    CM_ERR_0("get_rpm: cm_rpm_get_params_rsp heap exhausted, cm_mem_malloc() returned NULL");
  }

  if(cmrpm_lte_ptr()->is_rpm_enabled)
  {
    cm_rpm_get_params_rsp->rpm_rsp_ret_val = CM_RPM_SUCCESS_RPM_ENABLED;
    cm_rpm_get_params_rsp->lte_rpm_params.max_num_app_resets = cmrpm_lte_ptr()->max_num_reset;
    cm_rpm_get_params_rsp->lte_rpm_params.avg_mmreject_reset_time = cmrpm_lte_ptr()->max_reset_timer;
  }
  else
  {
    cm_rpm_get_params_rsp->rpm_rsp_ret_val = CM_RPM_FAILED_RPM_DISABLED;
  }
  (void) cm_msgr_send( MM_CM_GET_RPM_PARAMETERS_RSP, MSGR_MM_CM,
	                   (msgr_hdr_s *)cm_rpm_get_params_rsp, sizeof(cm_mm_get_rpm_parameters_rsp_s_type) );

  cm_mem_free(cm_rpm_get_params_rsp);

  CM_MSG_HIGH_3("get_rpm: send GET RPM_PARAMETERS_RSP with rsp = %d, max_reset = %d, max_timer =%d", \
   cm_rpm_get_params_rsp->rpm_rsp_ret_val,cm_rpm_get_params_rsp->lte_rpm_params.max_num_app_resets, \
      cm_rpm_get_params_rsp->lte_rpm_params.avg_mmreject_reset_time);

  return;
  
} /* cmrpm_lte_send_get_rpm_parameters_rsp */


/*===========================================================================

FUNCTION cmrpm_lte_set_rpm_config_req

DESCRIPTION
  Updates value of  CM RPM config and updates the EFS as well.
  If there is any RPM timer running currently it will be stopped on 
  disabling rpm config.

DEPENDENCIES

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
void cmrpm_lte_set_rpm_config_req( boolean set_rpm_config_req)
{
  
  cmrpm_lte_s_type  *rpm_ptr = cmrpm_lte_ptr();
  uint8              i = 0;

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  if(rpm_ptr->is_rpm_enabled != set_rpm_config_req)
  {
    rpm_ptr->is_rpm_enabled = set_rpm_config_req;

    /* 
    ** Check APP_RESET_TIMER if it's already running, reset it 
    */
    if((rpm_ptr->app_reset.timer != 0) && 
       (set_rpm_config_req == FALSE))
    {
      CM_MSG_HIGH_1("set_rpm_config: new rpm conf set to FALSE, reset timer: %d",
                     rpm_ptr->app_reset.timer);
    
      /* Update APP_RESET_TIMER to 0 */
      rpm_ptr->app_reset.timer = 0;
      rpm_ptr->app_reset.app_reset_counter = 0;

      /* clear the TAC list */
      rpm_ptr->tac_counter = 0;
      for(i=0; i< MAX_NUM_OF_TAC; i++)
      {
        rpm_ptr->lte_tai[i].tac = INVALID_TAC;
        sys_plmn_undefine_plmn_id(&(rpm_ptr->lte_tai[i].plmn));
      }
    } 

    CM_MSG_HIGH_1("set_rpm_config: write new rpm conf: %d", rpm_ptr->is_rpm_enabled);
    
    if(cmrpm_lte_write_efs_rpm_info(rpm_ptr) == FALSE)
    {
      CM_ERR_0("set_rpm_config: fail to write APP_RESET_TIMER ");
    }    

  }
  else
  {
    CM_MSG_HIGH_2("set_rpm_config: no change in rpm config old: %d, new config: %d",
                  rpm_ptr->is_rpm_enabled, set_rpm_config_req);
  }

  return;
  
}/* cmrpm_lte_set_rpm_config_req */

/*===========================================================================

FUNCTION cmrpm_lte_process_app_reset

DESCRIPTION
  Process oprt mode change 

DEPENDENCIES
  RPM object must have already been initialized with cmrpm_lte_init().

RETURN VALUE
  TRUE if reset counter is incremented

SIDE EFFECTS
  None
===========================================================================*/
  boolean  cmrpm_lte_process_app_reset(
  cm_ph_cmd_s_type           *ph_cmd_ptr
)
{
  cm_ph_cmd_info_s_type     *cmd_info_ptr = NULL;
  cmrpm_lte_s_type          *rpm_ptr      = cmrpm_lte_ptr();
  int i;

  static const struct {
    
          sys_oprt_mode_e_type    from_oprt_mode;
              /* from operating mode */
    
          sys_oprt_mode_e_type    to_oprt_mode;
              /* to operating mode */
    
      } cmrpm_oprt_mode_tbl[] = {
    
            /* from oprt mode */         /* to oprt mode */
            {SYS_OPRT_MODE_ONLINE,        SYS_OPRT_MODE_OFFLINE},
            {SYS_OPRT_MODE_ONLINE,        SYS_OPRT_MODE_OFFLINE_CDMA},
            {SYS_OPRT_MODE_ONLINE,        SYS_OPRT_MODE_LPM},
            {SYS_OPRT_MODE_ONLINE,        SYS_OPRT_MODE_FTM}
           
                };
  
  /* App inititated resets for oprt mode OFFLINE/LPM/OFFLINE_CDMA are 
   ** counted for rpm resets
   */
  cmd_info_ptr = CMD_INFO_PTR( ph_cmd_ptr );

  if(rpm_ptr->is_rpm_enabled &&
      (ph_cmd_ptr->client_id != CM_CLIENT_ID_ANONYMOUS) &&
      ((cmd_info_ptr->oprt_mode == SYS_OPRT_MODE_OFFLINE) ||
       (cmd_info_ptr->oprt_mode == SYS_OPRT_MODE_LPM) ||
       (cmd_info_ptr->oprt_mode == SYS_OPRT_MODE_OFFLINE_CDMA) || 
       (cmd_info_ptr->oprt_mode == SYS_OPRT_MODE_FTM)))
    {
      boolean valid_oprt_change = FALSE;
      for( i=0; i < ARR_SIZE(cmrpm_oprt_mode_tbl); i++ )
      {
        if( cmrpm_oprt_mode_tbl[i].from_oprt_mode == cmph_ptr()->oprt_mode &&
            cmrpm_oprt_mode_tbl[i].to_oprt_mode   == cmd_info_ptr->oprt_mode )
        {
          valid_oprt_change =  TRUE;
        }
      }
      
        /* If RPM APP RESET TIMER is not running, we don't need to track the 
               ** app reset counter nor checking reset allow or not 
              */
        if((rpm_ptr->app_reset.timer != 0) && (valid_oprt_change == TRUE))
        {
          if(rpm_ptr->app_reset.app_reset_counter < rpm_ptr->max_num_reset)
          {
            /* app reset counter has not been reached the max reset allowed 
                     **  increament app_reset_counter and allow reset 
                     */
            rpm_ptr->app_reset.app_reset_counter += 1;
            CM_MSG_HIGH_1("process_app_reset: RESET Allowed, new app_reset_counter: %d", 
                rpm_ptr->app_reset.app_reset_counter);

            if(cmrpm_lte_write_efs_rpm_info(rpm_ptr) ==FALSE )
            {
              CM_ERR_0("process_app_reset: Can't write APP_RESET_COUNTER");
            }
            return TRUE;
          }
            
          
        }
        
    }
  
    return FALSE;

}/* cmrpm_lte_process_app_reset */

/*===========================================================================

FUNCTION cmrpm_lte_app_reset_rejected_proc

DESCRIPTION
  Processes PH cmd from CM when app reset was rejected 

DEPENDENCIES
  RPM object must have already been initialized with cmrpm_lte_init().

RETURN VALUE
None

SIDE EFFECTS
  None
===========================================================================*/

void cmrpm_lte_app_reset_rejected_proc( void )
{

  cmrpm_lte_s_type         *rpm_ptr        = cmrpm_lte_ptr();
  uint32                     curr_sys_time = cm_util_get_curr_systime();

  CM_MSG_MED_3("reset_reject: rpm_ptr->sys_time_efs:%d coin_cell_support: %d curr_systime:%d",
        rpm_ptr->sys_time_efs, rpm_ptr->is_coin_cell_support, curr_sys_time );
  
  /* If no coin cell battery, add EFS time to current uptime to get total
  ** system time since power-up
  */	      
  if(rpm_ptr->is_coin_cell_support == FALSE )
  {
    curr_sys_time = (rpm_ptr->sys_time_efs) + curr_sys_time;
  }

 if(rpm_ptr->is_rpm_enabled )
  {
    
    /* If RPM APP RESET TIMER is not running, we don't need to track the 
    ** app reset counter nor checking reset allow or not 
    */
    if(rpm_ptr->app_reset.timer != 0)
    {
      if(rpm_ptr->app_reset.app_reset_counter >= rpm_ptr->max_num_reset)
      {
         CM_MSG_HIGH_2("reset_reject: max_num_reset: %d reset timer: %d",
          rpm_ptr->max_num_reset,
           rpm_ptr->app_reset.timer);
      }
       
     }
   }

} /* cmrpm_lte_app_reset_rejected_proc */


/*===========================================================================

FUNCTION cmrpm_lte_check_reset_allowed

DESCRIPTION
  Check whether reset is allowed or not when RPM is on

DEPENDENCIES
  RPM object must have already been initialized with cmrpm_lte_init().

RETURN VALUE
TRUE if reset is allowed

SIDE EFFECTS
  None
===========================================================================*/
boolean  cmrpm_lte_check_reset_allowed(
  void
)
 {
  boolean              ret_val = TRUE;
  boolean    is_pm_rpm_enabled = FALSE;
  cmrpm_lte_s_type    *rpm_ptr = cmrpm_lte_ptr();
  

  /* Internal CM reset(CLIENT_ID_ANONYMOUS) should be always allowed 
  ** All other reset should be checked before proceeing 
        */

  if(rpm_ptr->is_rpm_enabled )
  {
      /* If RPM APP RESET TIMER is not running, we don't need to track the 
      ** app reset counter nor checking reset allow or not 
          */
      if((rpm_ptr->app_reset.timer!= 0) && \
         cmrpm_lte_get_pm_rpm_enabled_flag(&is_pm_rpm_enabled) && \
         is_pm_rpm_enabled)
      {
        if(rpm_ptr->app_reset.app_reset_counter >= rpm_ptr->max_num_reset)
        {
          CM_MSG_HIGH_2("check_reset: app_reset Not Allowed - max_num_reset: %d timer: %d",
            rpm_ptr->max_num_reset,
            rpm_ptr->app_reset.timer);
  
           ret_val = FALSE; 
          
        }
        else
        {
          CM_MSG_HIGH_0("check_reset: app_reset Allowed");
        }
      }
      else
      {
        CM_MSG_HIGH_1("check_reset: app_reset Allowed, is_pm_rpm_enabled %d", is_pm_rpm_enabled);
      }
      
    }

  return ret_val;
} /* cmrpm_lte_check_reset_allowed */


/*===========================================================================

FUNCTION cmrpm_lte_timer_proc

DESCRIPTION
  Process timer for RPM.

DEPENDENCIES
  RPM object must have already been initialized with cmrpm_lte_init().

RETURN VALUE
  none

SIDE EFFECTS
  none

===========================================================================*/
void cmrpm_lte_timer_proc(
  cm_timer_event_type    timer_event   /* Indicate specific timer event */
)
{
  cmrpm_lte_s_type             *rpm_ptr        = cmrpm_lte_ptr();
  uint32                        curr_sys_time  = cm_util_get_curr_systime();
  uint8                         i = 0;

  CM_MSG_LOW_3("timer_proc: rpm_ptr->sys_time_efs: %d coin_cell: %d curr_systime: %d",
                 rpm_ptr->sys_time_efs, rpm_ptr->is_coin_cell_support, curr_sys_time);
  
  /* If no coin cell battery, add EFS time to current uptime to get total
  ** system time since power-up
  */
  if(rpm_ptr->is_coin_cell_support == FALSE )
  {
    curr_sys_time = (rpm_ptr->sys_time_efs) + curr_sys_time;
  }

  SYS_ARG_NOT_USED( timer_event );

  /* Check if APP reset timer is expired */
  if((rpm_ptr->app_reset.timer !=0) &&
     (curr_sys_time >= rpm_ptr->app_reset.timer))
  {
    CM_MSG_HIGH_2("timer_proc: app_reset Timer expired - curr_systime: %d, reset timer: %d",
      curr_sys_time, rpm_ptr->app_reset.timer);

    
    /* reset the counter */
    rpm_ptr->app_reset.app_reset_counter = 0;
    
    /* restart the timer */
    rpm_ptr->app_reset.timer = 0;

    /* clear the TAC list */
    rpm_ptr->tac_counter = 0;
    for(i=0; i< MAX_NUM_OF_TAC; i++)
    {
      rpm_ptr->lte_tai[i].tac = INVALID_TAC;
      sys_plmn_undefine_plmn_id(&(rpm_ptr->lte_tai[i].plmn));
    }

    if(cmrpm_lte_write_efs_rpm_info(rpm_ptr)  == FALSE)
    {
      CM_ERR_0("timer_proc: Can't write APP_RESET_TIMER");
      
    }    
  }  
  
} /* cmrpm_lte_timer_proc */


/*===========================================================================

FUNCTION cmrpm_lte_emm_tai_list_ind_proc

DESCRIPTION
  Update the TAI info based on NAS_EMM_TAI_LIST_IND.

DEPENDENCIES

None

RETURNS
  None

SIDE EFFECTS

None

===========================================================================*/
void cmrpm_lte_emm_tai_list_ind_proc (emm_tai_list_ind_type  *emm_tai_list_ind)
{
  uint8 i;

  lte_nas_emm_tai_lst_info_type    *tai_lst_ptr = NULL;

  lte_nas_tai_lst1_type cmrpm_tai;
  
  cmrpm_lte_s_type      *rpm_ptr = cmrpm_lte_ptr();
  
  tai_lst_ptr = &(emm_tai_list_ind->tai_lst_info);

  CM_ASSERT( tai_lst_ptr != NULL );

  if(rpm_ptr->is_rpm_enabled)
  {
    CM_MSG_HIGH_2("tai_list_ind: tai_lst_info length %d, tac_counter = %d",
                        tai_lst_ptr->tai_lst_length, rpm_ptr->tac_counter);

    /* search for each tai from the  cmrpm tai list */
    for (i=0; (i < MAX_NUM_OF_TAC) && (rpm_ptr->tac_counter !=0); i++)
    {
      if(IS_VALID_TAC(rpm_ptr->lte_tai[i].tac))
      {
        cmrpm_tai.plmn = rpm_ptr->lte_tai[i].plmn;
        cmrpm_tai.tac  = rpm_ptr->lte_tai[i].tac;

        CM_MSG_HIGH_2("tai_list_ind: emm_search_tai_list() tac[%d] = %d", i, cmrpm_tai.tac);
        CM_MSG_HIGH_3("tai_list_ind: emm_search_tai_list() plmn %d %d %d",
                       cmrpm_tai.plmn.identity[0], 
                       cmrpm_tai.plmn.identity[1], 
                       cmrpm_tai.plmn.identity[2]);

        /* check if reporting tai match with the tai from cmrpm list, remove it */
        if(emm_search_tai_list(&cmrpm_tai, tai_lst_ptr))
        {
          CM_MSG_HIGH_3("tai_list_ind: tac found, tac[%d] = %d, tac_counter = %d",\
               i, rpm_ptr->lte_tai[i].tac, rpm_ptr->tac_counter);

          rpm_ptr->lte_tai[i].tac = INVALID_TAC;
          sys_plmn_undefine_plmn_id(&(rpm_ptr->lte_tai[i].plmn));
          rpm_ptr->tac_counter--;
        }
        else
        {
          CM_MSG_HIGH_3("tai_list_ind: tac NOT found, tac[%d] = %d, tac_counter = %d",\
               i, rpm_ptr->lte_tai[i].tac, rpm_ptr->tac_counter);
        }
      }
    } /* for () */



    /* Check if no TAI is pending, then reset the timer */
    if(rpm_ptr->tac_counter == 0)
    {
      CM_MSG_HIGH_0("tai_list_ind: no TAI is pending, reset the timer ");

      /* reset the counter */
      rpm_ptr->app_reset.app_reset_counter = 0;
    
      /* restart the timer */
      rpm_ptr->app_reset.timer = 0;

      for(i=0; i< MAX_NUM_OF_TAC; i++)
      {
        rpm_ptr->lte_tai[i].tac = INVALID_TAC;
        sys_plmn_undefine_plmn_id(&(rpm_ptr->lte_tai[i].plmn));
      }

      if(cmrpm_lte_write_efs_rpm_info(rpm_ptr)  == FALSE)
      {
        CM_ERR_0("tai_list_ind: Can't write APP_RESET_TIMER");
      }
    }/* if() */
    
  } /* if(rpm_enabled)*/


} /* cmrpm_lte_emm_tai_list_ind_proc */

/*===========================================================================

  FUNCTION cmrpm_lte_sd_rpt_proc

  DESCRIPTION
    Process reports from System Determination.

  DEPENDENCIES
    RPM object must have already been initialized with cmrpm_lte_init().

  RETURN VALUE
    none

  SIDE EFFECTS
    none

===========================================================================*/
void cmrpm_lte_sd_rpt_proc(

  const cm_hdr_type   *rpt_ptr
    /* Pointer to SD report */
)
{
  const cm_sd_rpt_u_type         *sd_rpt_ptr = (cm_sd_rpt_u_type *) rpt_ptr;
    /* Pointer to SD reports */

  cmrpm_lte_s_type                *rpm_ptr   = cmrpm_lte_ptr();
    /* Point at rpm  object */

  uint8      i = 0;
  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  CM_ASSERT( rpm_ptr != NULL );
  CM_ASSERT( sd_rpt_ptr != NULL );


  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  CM_MSG_LOW_1("sd_rpt_proc: START cmrpm_lte_s_type(), cmd=%d", sd_rpt_ptr->hdr.cmd);

  /* This is serving system change notifiction
  ** 1. Updating appropriate domain selection object fields.
  */
  switch( sd_rpt_ptr->hdr.cmd )
  {
    /* Service Indicators information is changed.
    */
    case CM_SRV_IND_INFO_F:
      if(rpm_ptr->is_rpm_enabled &&
         (rpm_ptr->app_reset.timer != 0) &&
         (rpm_ptr->tac_counter == 0))
      {
        /* Reset timer 1hr timer when UE is registered in both CS and PS domain */
        if((sd_rpt_ptr->srv_ind_info.si_info.srv_status == SYS_SRV_STATUS_SRV) && 
           (sd_rpt_ptr->srv_ind_info.si_info.srv_domain == SYS_SRV_DOMAIN_CS_PS))
        {
          /* Reset the app reset counter */
          rpm_ptr->app_reset.app_reset_counter = 0;
  
          /* Stop the app reset timer */
          rpm_ptr->app_reset.timer = 0;

          /* clear the TAC list */
          rpm_ptr->tac_counter = 0;
          for (i=0; i< MAX_NUM_OF_TAC; i++)
          {
            rpm_ptr->lte_tai[i].tac = INVALID_TAC;
          }
          
  
          /* Update EFS param */
          if(cmrpm_lte_write_efs_rpm_info(rpm_ptr) == FALSE)
          {
            CM_ERR_0("sd_rpt_proc: Can't write EFS_INFO");
          }    
          
          CM_MSG_HIGH_2("CS/PS registered, Stop APP reset timer. app_reset_counter: %d, app_reset timer: %d", 
          rpm_ptr->app_reset.app_reset_counter, rpm_ptr->app_reset.timer);
          
        }
      }
      break;
         
          
    default:
      break;
  } /* switch( sd_rpt_ptr->hdr.cmd ) */

  return;
} /* cmrpm_lte_sd_rpt_proc() */

/*===========================================================================

FUNCTION cmrpm_lte_rpt_proc

DESCRIPTION

  Process LL reports related to RPM


DEPENDENCIES
  RPM object must have already been initialized with cmrpm_lte_init().

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
void cmrpm_lte_rpt_proc(

  const cm_hdr_type   *rpt_ptr
    /* Pointer to a LL report */
)
{
  cm_rpt_type             *cm_rpt_ptr = (cm_rpt_type *) rpt_ptr;
     /* Pointer to a LL report */
     
  cmrpm_lte_s_type        *rpm_ptr        = cmrpm_lte_ptr();     
     /* Point at RPM object */
     
  uint32           curr_system_time   = cm_util_get_curr_systime();
  uint8            i = 0;
  boolean          is_tac_already_present = FALSE;
    

  /* If no coin cell battery, add EFS time to current uptime to get total
  ** system time since power-up
  */
  if(rpm_ptr->is_coin_cell_support == FALSE )
  {
    curr_system_time = (rpm_ptr->sys_time_efs) + curr_system_time;
  }

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  CM_ASSERT(cm_rpt_ptr != NULL);
  CM_ASSERT(rpm_ptr != NULL);


  CM_MSG_LOW_1( "START cmrpm_lte_rpt_proc(), cmd=%d",  cm_rpt_ptr->hdr.cmd);

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
  /* Process the report
  */
  switch( cm_rpt_ptr->hdr.cmd )
  {
    case CM_REG_REJECT_IND:
      if(rpm_ptr->is_rpm_enabled)
      {
        cm_reg_reject_ind_s_type *reg_reject_ind_ptr = 
          &cm_rpt_ptr->cmd.reg_reject_ind;
        
        CM_MSG_MED_3("reg_rej_ind: sys_time_efs: %d, coin_cell: %d, rpm_systime: %d",
            rpm_ptr->sys_time_efs, rpm_ptr->is_coin_cell_support, curr_system_time);
          
        if(reg_reject_ind_ptr->reject_domain == SYS_SRV_DOMAIN_CS_ONLY || 
          reg_reject_ind_ptr->reject_domain == SYS_SRV_DOMAIN_PS_ONLY || 
          reg_reject_ind_ptr->reject_domain == SYS_SRV_DOMAIN_CS_PS)
        {

         CM_MSG_HIGH_2("reg_rej_ind: EMM Reject,  domain: %d, cause: %d", 
              reg_reject_ind_ptr->reject_domain,
              reg_reject_ind_ptr->reject_cause );
         
         /* Start RPM timer upon any one of the 3,6,7 or 8  EMM reject cause */
        
         if(reg_reject_ind_ptr->reject_cause == IMSI_UNKNOWN_IN_HLR || 
            reg_reject_ind_ptr->reject_cause == ILLEGAL_MS ||
            reg_reject_ind_ptr->reject_cause == ILLEGAL_ME || 
            reg_reject_ind_ptr->reject_cause == GPRS_SERVICES_NOT_ALLOWED || 
            reg_reject_ind_ptr->reject_cause == GPRS_SERVICES_AND_NON_GPRS_SERVICES_NOT_ALLOWED)
         {
            /* And the app_reset_timer is not already running, 
                      ** then start the app_reset_timer
                      */
          if(rpm_ptr->app_reset.timer == 0)
          {
            rpm_ptr->app_reset.timer = CMRPM_SEC_PER_MIN * rpm_ptr->max_reset_timer \
                                       + curr_system_time;
    
            CM_MSG_HIGH_1("reg_rej_ind: started app_reset timer: %d",
              rpm_ptr->app_reset.timer);
                      
            if(cmrpm_lte_write_efs_rpm_info(rpm_ptr) == FALSE)
            {
              CM_ERR_0("reg_rej_ind: Can't write APP_RESET_TIMER");
            }              
          }
        }
          /* If the EMM reject cause of below received, rpm timer should start */
         else if(reg_reject_ind_ptr->reject_cause == NO_SUITABLE_CELLS_IN_LA )
          {
            CM_MSG_HIGH_1("reg_rej_ind: reg_reject_ind_ptr->tac = %d", 
                              reg_reject_ind_ptr->tac);

            CM_MSG_HIGH_3("reg_rej_ind: reg_reject_ind_ptr->plmn %d %d %d",
                           reg_reject_ind_ptr->plmn.identity[0],
                           reg_reject_ind_ptr->plmn.identity[1],
                           reg_reject_ind_ptr->plmn.identity[2]);

            /* when the reset_timer is not already running, 
                     ** then start the mmreject reset_timer 
                     */
            if(rpm_ptr->app_reset.timer == 0)
            {
              rpm_ptr->app_reset.timer = CMRPM_SEC_PER_MIN * rpm_ptr->max_reset_timer \
                                       + curr_system_time;

              CM_MSG_HIGH_2("reg_rej_ind: started reset timer: %d, curr_systime: %d",
                rpm_ptr->app_reset.timer, curr_system_time);
            }
            else
            {
              CM_MSG_HIGH_2("reg_rej_ind: timer is already running: %d, curr_systime: %d",
                rpm_ptr->app_reset.timer, curr_system_time);
            }

            
             /* there can be EMM Reject from different TA  than  received earlier 
             * run a loop  and chek if new tac is different than previous one.
             */

            CM_ASSERT(IS_VALID_TAC(reg_reject_ind_ptr->tac));

            /* Check for duplicates before adding the tac */
            for ( i = 0;i < MAX_NUM_OF_TAC; i++)
            {
              if ((IS_VALID_TAC(rpm_ptr->lte_tai[i].tac)) && \
                   (rpm_ptr->lte_tai[i].tac == reg_reject_ind_ptr->tac) && \
                    sys_plmn_match(rpm_ptr->lte_tai[i].plmn, reg_reject_ind_ptr->plmn))
              {
              /* tac is already in the list, no need to add again */
                is_tac_already_present = TRUE;
                CM_MSG_HIGH_2("reg_rej_ind: TAC already present: tac = %d, i = %d",
                reg_reject_ind_ptr->tac, i);

              break;
              }
            } /* for () */

            /* tac is not  in the list, add to the list */
            if((is_tac_already_present == FALSE) && (rpm_ptr->tac_counter < MAX_NUM_OF_TAC) )
            {
              /* Check for available slot for adding the tac */
              for ( i = 0; i < MAX_NUM_OF_TAC; i++)
              {
                if(!IS_VALID_TAC(rpm_ptr->lte_tai[i].tac))
                {
                  rpm_ptr->lte_tai[i].tac = reg_reject_ind_ptr->tac;
                  rpm_ptr->lte_tai[i].plmn = reg_reject_ind_ptr->plmn;
                rpm_ptr->tac_counter++;
                
                  CM_MSG_HIGH_3("reg_rej_ind: Added tac[%d] = %d, new tac_counter = %d",
                    i, rpm_ptr->lte_tai[i].tac, rpm_ptr->tac_counter);
                
                  CM_MSG_HIGH_3("reg_rej_ind: Added plmn %d %d %d",
                                    rpm_ptr->lte_tai[i].plmn.identity[0],
                                    rpm_ptr->lte_tai[i].plmn.identity[1],
                                    rpm_ptr->lte_tai[i].plmn.identity[2]);

                  if(cmrpm_lte_write_efs_rpm_info(rpm_ptr) == FALSE)
                  {
                    CM_ERR_0("reg_rej_ind: Can't write APP_RESET_TIMER");
                  }

                  break;                  
                }
                else
                {
                  CM_MSG_HIGH_3("reg_rej_ind: existing tac[%d] = %d, tac_counter = %d",
                    i, rpm_ptr->lte_tai[i].tac, rpm_ptr->tac_counter);
                }
              } // for
            }
            else
            {
              CM_MSG_HIGH_2("reg_rej_ind: TAC %d not added, tac_counter = %d", 
                          reg_reject_ind_ptr->tac, rpm_ptr->tac_counter);
            }
            }
        else
        {
          CM_MSG_HIGH_2("reg_rej_ind: Unknown Reject Cause, timer: %d, curr_systime: %d",
            rpm_ptr->app_reset.timer, curr_system_time);
          }
        }
      }
      break;  // CM_REG_REJECT_IND

    default:
      break;
  }
} /* cmrpm_lte_rpt_proc */

