/*==============================================================================
  FILE:         uim_logging.c

  OVERVIEW:     File contains the functions for APDU and EFS logging.

  DEPENDENCIES: N/A

                Copyright (c) 2014-2015 QUALCOMM Technologies, Inc.
                All Rights Reserved.
                QUALCOMM Technologies Confidential and Proprietary
==============================================================================*/

/*=============================================================================
  EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.  Please
  use ISO format for dates.

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/uim/uimdrv/src/uim_logging.c#1 $
$DateTime: 2016/03/28 23:02:52 $
$Author: mplcsds1 $

when        who        what, where, why
------      ----       ---------------------------------------------------------
08/07/15    lm         Fix compilation issues on linux mob
08/03/15    lm         Replace strcpy_s with strncpy
07/02/15    sam        Offartget change- correcting the destination buffer size
                       in strcpy_s
05/11/15    hyo        In efs logging, clear the buffer size when ptr is freed
04/08/15    sam        FR24498: Powerup logging changes
04/06/15    ll         UIMDRV migrates to UIM COMMON EFS APIs for all EFS items
02/27/15    xj         Add EFS apdu logging ut support
02/23/15    ks         Fix with APDU logging in EFS during efs clean-up
01/07/14    sam        Modification of msg macros for FR24498-UIM Powerup Logging
12/10/14    na         Making APDU Logging file instance based
11/10/14    akv        Support for user to customize LDO settings
10/01/14    ll         KW errors
09/18/14    akv        UIMDRV UT enhancements
06/16/14    ll         Switch to new string based TLMM APIs
03/03/14    nmb        Directory maintenance I&T bug fixes
02/20/14    ak         Fix compile errors due to strict compiler on 9x35
01/29/14    akv        RX ISR refactoring changes
01/23/14    yk         The first revision
==============================================================================*/
#include "intconv.h"
#include "rex.h"
#include "fs_public.h"
#include "comdef.h"
#include "time_svc.h"
#include "log.h"
#include "uimdrv_msg.h"
#include "fs_lib.h"
#include "fs_stdlib.h"
#include "fs_diag_access.h"
#include "mcfg_fs.h"

#include "uim_p.h"
#include "uimi.h"
#include "uimglobals.h"
#include "uim_logging.h"
#include "uimdrv_gpio.h"


#define UIM_EFSLOG_FILE_SIZE        1500
#define UIM_EFSLOG_MAX_BUFFER_SIZE  1000
#define UIM_EFSLOG_ASCII_CR         0x0D
#define UIM_EFSLOG_ASCII_LF         0x0A


/**
 * DECLARATIONS OF INTERNAL FUNCTIONS
 */
static boolean uim_log_put_buffer( uim_instance_global_type *uim_ptr );
static void uim_log_put_char( uim_instance_global_type *uim_ptr );
static void uim_log_put_tstamp( uim_instance_global_type *uim_ptr );
static boolean uim_efslog_purge_check( uim_instance_global_type *uim_ptr );
static boolean uim_efs_write(
  int                       fhandle,
  const char               *databuffer_p,
  int                       datasize,
  uim_instance_global_type *uim_ptr
);
static void uim_close_log_timeout_info(
  uim_instance_global_type  *uim_ptr,
  char                      *data_to_be_freed,
  int                       fhandle );


/**
 * DEFINITIONS OF EXTERNAL FUNTIONS
 *
 */

/**
 * Put a byte to APDU log buf
 *
 * @param attr Attribute of logging data
 * @param ch Data to be logged.
 * @param uim_ptr Pointer to the global data common.
 */
void uim_log_put_byte(uim_log_attrib_type attr,
                      char ch,
                      uim_instance_global_type *uim_ptr)
{
  uim_ptr->debug.log_char.attrib = attr;
  uim_ptr->debug.log_char.the_char = ch;
  uim_ptr->debug.log_char.slot_id = uim_ptr->id;

  uim_log_put_char(uim_ptr);
  if( (uim_nv_is_feature_enabled(UIMDRV_FEATURE_LOG_APDU_TO_EFS,
                               uim_ptr) == TRUE) &&
      (TRUE == uim_ptr->debug.efslog_logging_in_progress)
    )
  {
    uim_efslog_apdu_to_buffer(uim_ptr);
  }
}


/**
 * Put a time stamp then logging all data in APDU log bug.
 *
 * @param uim_ptr Pointer to the global data common.
 */
void uim_log_put_last_byte(uim_instance_global_type *uim_ptr)
{
  uim_ptr->debug.log_char.slot_id = uim_ptr->id;

  if (uim_ptr->debug.log_data.length != 0)
  {
    uim_log_put_tstamp(uim_ptr);
    uim_log_put_buffer(uim_ptr);
  }
}


/**
 * This procedure would be invoked when we see a timeout signal
 * after the PPS procedure has been completed.
 *
 * @param external_cmd_ptr Pointer to the external command.
 * @param static_cmd_ptr Pointer to the static command.
 * @param i_mask Signal mask of UIM task.
 * @param uart_status Current UART status.
 * @param uim_ptr Pointer to the global data common.
 *
 * @return boolean Success or Fail.
 */
boolean uim_log_timeout_info(
  uim_cmd_type             *external_cmd_ptr,
  uim_cmd_type             *static_cmd_ptr,
  rex_sigs_type             i_mask,
  dword                     uart_status,
  uim_instance_global_type *uim_ptr)
{
  /* -1 is used for invalid file handle */
  int           fhandle         = -1;
  uim_cmd_type  *cmd_ptr_in_use = NULL;
  int           num_bytes       = 0;
  unsigned int  unum_bytes      = 0;
  unsigned int  i               = 0;
  char          *data_to_write  = NULL;
  char          temp_buff[10];
  static char* uim_reset_ef_file[UIM_MAX_INSTANCES] =
               {
                 "Uim1Reset.Txt",
                 "Uim2Reset.Txt"
               };

  if (uim_ptr->id >= UIM_MAX_INSTANCES)
  {
    UIM_MSG_ERR_1("Invalid input param instance id 0x%x", uim_ptr->id);
    return FALSE;
  }

  /* Initialize temp_buff */
  memset(temp_buff,'\0',sizeof(temp_buff));

  if (uim_ptr->flag.static_buffer_used)
  {
    cmd_ptr_in_use = static_cmd_ptr;
  }
  else
  {
    cmd_ptr_in_use = external_cmd_ptr;
  }
  /* Let us allocate enough memory to contain the strings
   * that we are going to create for writing to the Log
   */
  data_to_write = (char *) uim_malloc(UIM_EFSLOG_FILE_SIZE);

  if (data_to_write == NULL)
  {
    /* There is nothing much we can do if we do not have
     * the buffer space to write to */
    UIMDRV_MSG_ERR_0(uim_ptr->id,"Could not allocate 1500 bytes for logging buffer");
    return FALSE;
  }

  /* --------------------------------------------------------------------------
     Creating file in EFS
     ------------------------------------------------------------------------*/
  fhandle = mcfg_fopen(uim_reset_ef_file[uim_ptr->id], MCFG_FS_O_WRONLY|MCFG_FS_O_CREAT|MCFG_FS_O_TRUNC,
                       MCFG_FS_ALLPERMS, MCFG_FS_TYPE_EFS, MCFG_FS_SUBID_NONE);
  /* invalid file handle is denote by -1 */
  if (fhandle < 0)
  {
    UIMDRV_MSG_ERR_1(uim_ptr->id,"Failed to open UimReset File, efs_errno: 0x%x", efs_errno);
    /* Let us return the memory allocated */
    UIM_FREE(data_to_write);
    return FALSE;
  }

  UIMDRV_PUP_MSG_HIGH_0(UIMLOG_MSG_151,uim_ptr->id, "Starting to log the timeout Information to UimReset.Txt");

  /* Indicate that this timeout is after a PPS procedure */
  if (uim_ptr->atr.atr_pps_done)
  {
    num_bytes = snprintf((char *)data_to_write,UIM_EFSLOG_FILE_SIZE,"%s\r\n",
                         "Timeout occured after PPS Procedure");
    if ((num_bytes >= 0) && (TRUE != uim_efs_write(fhandle,data_to_write,
                                                   MIN(num_bytes,UIM_EFSLOG_FILE_SIZE),
                                                   uim_ptr)))

    {
      uim_close_log_timeout_info(uim_ptr, data_to_write, fhandle);
      return FALSE;
    }
  }
  else
  {
    num_bytes = snprintf((char *)data_to_write,UIM_EFSLOG_FILE_SIZE,"%s\r\n",
                         "Timeout occured before PPS Procedure");
    if ((num_bytes >= 0) && (TRUE != uim_efs_write(fhandle,data_to_write,
                                       MIN(num_bytes,UIM_EFSLOG_FILE_SIZE),
                                       uim_ptr)))
    {
      uim_close_log_timeout_info(uim_ptr, data_to_write, fhandle);
      return FALSE;
    }
  }

  /* Print the voltage class information */
  num_bytes = snprintf((char *)data_to_write,UIM_EFSLOG_FILE_SIZE,"%s %s\r\n",
                       "Operating Voltage = ",
                       (uim_ptr->state.current_voltage_class==
                        UIM_VOLTAGE_CLASS_C)?"1.8":"3.0");
  if ((num_bytes >= 0) && (TRUE != uim_efs_write(fhandle,data_to_write,
                                     MIN(num_bytes,UIM_EFSLOG_FILE_SIZE),
                                     uim_ptr)))
  {
    uim_close_log_timeout_info(uim_ptr, data_to_write, fhandle);
    return FALSE;
  }

  //Print the protocol being used
  if (cmd_ptr_in_use != NULL)
  {
    num_bytes = snprintf((char *)data_to_write,UIM_EFSLOG_FILE_SIZE,"%s 0x%x\r\n",
                         "Command Protocol = ",
                         cmd_ptr_in_use->hdr.protocol);
    if ((num_bytes >= 0) && (TRUE != uim_efs_write(
                                       fhandle,data_to_write,
                                       MIN(num_bytes,UIM_EFSLOG_FILE_SIZE),
                                       uim_ptr)))
    {
      uim_close_log_timeout_info(uim_ptr, data_to_write, fhandle);
      return FALSE;
    }
  }
  else
  {
    /* Indicate that this command is an internal command */
    num_bytes = snprintf((char *)data_to_write,UIM_EFSLOG_FILE_SIZE,
                         "%s\r\n","The command Pointer is NULL");
    if ((num_bytes >= 0) && (TRUE != uim_efs_write(fhandle,data_to_write,
                                      MIN(num_bytes,UIM_EFSLOG_FILE_SIZE),
                                      uim_ptr)))
    {
      uim_close_log_timeout_info(uim_ptr, data_to_write, fhandle);
      return FALSE;
    }
  }

  /* Check to see if the timeout is because of an error */
  if (uim_ptr->flag.max_parity_error ||
      uim_ptr->flag.max_rx_break_error ||
      uim_ptr->flag.max_overrun_error)
  {
    /* Log the error because of which we have a timeout signal */
    (void)snprintf((char *)data_to_write,UIM_EFSLOG_FILE_SIZE,"%s","Timeout occured with the following error: ");
    if (uim_ptr->flag.max_parity_error)
    {
      (void)strlcat((char *)data_to_write," Max Parity Error\r\n", UIM_EFSLOG_FILE_SIZE);
    }
    if (uim_ptr->flag.max_rx_break_error)
    {
      (void)strlcat((char *)data_to_write," Max RX_Break Error\r\n",
                        UIM_EFSLOG_FILE_SIZE);
    }
    if (uim_ptr->flag.max_overrun_error)
    {
      (void)strlcat((char *)data_to_write," Max Overrun Error\r\n",
                        UIM_EFSLOG_FILE_SIZE);
    }
    if (uim_ptr->flag.overrun_error)
    {
      (void)strlcat((char *)data_to_write,"Processing Overrun Error\r\n",
                        UIM_EFSLOG_FILE_SIZE);
    }


    unum_bytes = strlen(data_to_write);
    if (TRUE != uim_efs_write(fhandle,data_to_write,(int)unum_bytes,
                              uim_ptr))
    {
      uim_close_log_timeout_info(uim_ptr, data_to_write, fhandle);
      return FALSE;
    }
  }
  else
  {
    /* Not an error this is a genuine timeout */
    num_bytes = snprintf((char *)data_to_write,UIM_EFSLOG_FILE_SIZE,"%s\r\n",
            "Timeout occured waiting for response from the card");
    if ((num_bytes >= 0) && (TRUE != uim_efs_write(fhandle,data_to_write,
                                       MIN(num_bytes,UIM_EFSLOG_FILE_SIZE),
                                       uim_ptr)))
    {
      uim_close_log_timeout_info(uim_ptr, data_to_write, fhandle);
      return FALSE;
    }
  }

  /* Check to see if a command is in progress */
  if (uim_ptr->flag.command_in_progress)
  {
    /* Indicate that a command is in progress */
    num_bytes = snprintf((char *)data_to_write,UIM_EFSLOG_FILE_SIZE,"%s\r\n",
                        "A command is in progress");
    if ((num_bytes >= 0) && (TRUE != uim_efs_write(fhandle,data_to_write,
                                       MIN(num_bytes,UIM_EFSLOG_FILE_SIZE),
                                       uim_ptr)))
    {
      uim_close_log_timeout_info(uim_ptr, data_to_write, fhandle);
      return FALSE;
    }

    /* Check to see if this is an internal command or an external command */
    if (uim_ptr->flag.static_buffer_used)
    {
      /* Indicate that this command is an Internal Command */
      num_bytes = snprintf((char *)data_to_write,UIM_EFSLOG_FILE_SIZE,"%s\r\n",
                           "The command is an Internal command");
      if ((num_bytes >= 0) && (TRUE != uim_efs_write(fhandle,data_to_write,
                                         MIN(num_bytes,UIM_EFSLOG_FILE_SIZE),
                                         uim_ptr)))
      {
        uim_close_log_timeout_info(uim_ptr, data_to_write, fhandle);
        return FALSE;
      }
    }
    else if (cmd_ptr_in_use != NULL)
    {
      /* Indicate that this command is an external command and also
       * log the response call back pointer we can later locate
       * who has initiated this command and other details
       */
      num_bytes = snprintf((char *)data_to_write,UIM_EFSLOG_FILE_SIZE,"%s %s 0x%lX\r\n",
                          "The command is an External command ",
                          "With the call back function pointing to ",
                          (uint32)cmd_ptr_in_use->hdr.rpt_function);
      if ((num_bytes >= 0) && (TRUE != uim_efs_write(fhandle,data_to_write,
                                         MIN(num_bytes,UIM_EFSLOG_FILE_SIZE),
                                         uim_ptr)))
      {
        uim_close_log_timeout_info(uim_ptr, data_to_write, fhandle);
        return FALSE;
      }
    }
    else
    {
      /* Indicate that this command is an Internal Command */
      num_bytes = snprintf((char *)data_to_write,UIM_EFSLOG_FILE_SIZE,
                           "%s\r\n","The command Pointer is NULL");
      if ((num_bytes >= 0) && (TRUE != uim_efs_write(
                                fhandle,data_to_write,
                                MIN(num_bytes,UIM_EFSLOG_FILE_SIZE),
                                uim_ptr)))
      {
        uim_close_log_timeout_info(uim_ptr, data_to_write, fhandle);
        return FALSE;
      }
    }

    if(cmd_ptr_in_use != NULL)
    {
      /* Indicate the command count done on this command*/
      num_bytes = snprintf((char *)data_to_write,UIM_EFSLOG_FILE_SIZE,"%s 0x%x\r\n",
                          "cmd_ptr->hdr.count : ",
                          cmd_ptr_in_use->hdr.cmd_count);
      if ((num_bytes >= 0) && (TRUE != uim_efs_write(fhandle,data_to_write,
                                         MIN(num_bytes,UIM_EFSLOG_FILE_SIZE),
                                         uim_ptr)))
      {
        uim_close_log_timeout_info(uim_ptr, data_to_write, fhandle);
        return FALSE;
      }

      /* Indicate the command itself, SELECT, Fetch etc */
      num_bytes = snprintf((char *)data_to_write,UIM_EFSLOG_FILE_SIZE,
                           "%s 0x%X\r\n","The command is : ",
                          cmd_ptr_in_use->hdr.command);
      if ((num_bytes >= 0) && (TRUE != uim_efs_write(fhandle,data_to_write,
                                         MIN(num_bytes,UIM_EFSLOG_FILE_SIZE),
                                         uim_ptr)))
      {
        uim_close_log_timeout_info(uim_ptr, data_to_write, fhandle);
        return FALSE;
      }


      /* Indicate the protocol GSM/USIM etc */
      num_bytes = snprintf((char *)data_to_write,UIM_EFSLOG_FILE_SIZE,"%s 0x%X\r\n",
                          "The command protocol is : ",
                          uim_ptr->command.req_buf.protocol);
      if ((num_bytes >= 0) && (TRUE != uim_efs_write(fhandle,data_to_write,
                                         MIN(num_bytes,UIM_EFSLOG_FILE_SIZE),
                                         uim_ptr)))
      {
        uim_close_log_timeout_info(uim_ptr, data_to_write, fhandle);
        return FALSE;
      }
    }

    /* Log the last selected path */
    num_bytes = snprintf((char *)data_to_write,UIM_EFSLOG_FILE_SIZE,"%s 0x%x 0x%x 0x%x\r\n",
                          "ADF was Last selected DF2,EF",
                          uim_ptr->state.current_path[uim_ptr->card_cmd.cur_channel].path.path[0],
                          uim_ptr->state.current_path[uim_ptr->card_cmd.cur_channel].path.path[1],
                          uim_ptr->state.current_path[uim_ptr->card_cmd.cur_channel].path.path[2]);

    /* Log the signals that are pending on the TCB */
    num_bytes = snprintf((char *)data_to_write,UIM_EFSLOG_FILE_SIZE,"%s 0x%lX\r\n",
                        "Signals pending in uim TCB are : ",
                        (unsigned long)rex_get_sigs(uim_ptr->tcb_ptr));
    if ((num_bytes >= 0) && (TRUE != uim_efs_write(fhandle,data_to_write,
                                       MIN(num_bytes,UIM_EFSLOG_FILE_SIZE),
                                       uim_ptr)))
    {
      uim_close_log_timeout_info(uim_ptr, data_to_write, fhandle);
      return FALSE;
    }

    /* Log the signals that UIM task is waiting / will look for */
    num_bytes = snprintf((char *)data_to_write,UIM_EFSLOG_FILE_SIZE,"%s 0x%lX\r\n",
                        "UIM Task currently is interested in signals : ",
                        i_mask);
    if ((num_bytes >= 0) && (TRUE != uim_efs_write(fhandle,data_to_write,
                                       MIN(num_bytes,UIM_EFSLOG_FILE_SIZE),
                                       uim_ptr)))
    {
      uim_close_log_timeout_info(uim_ptr, data_to_write, fhandle);
      return FALSE;
    }

    /* Log the information that we have about the atr_bytes */
    if ( uim_ptr->atr.atr_buf.num_bytes > UIM_MAX_ATR_CHARS )
    {
      /* Indicate the fact that we are showing only 33 bytes */
      num_bytes = snprintf((char *)data_to_write,UIM_EFSLOG_FILE_SIZE,
                           "Num atr bytes is 0x%x %s\r\n",
                          uim_ptr->atr.atr_buf.num_bytes,
                          "truncating to 33");
      if ((num_bytes >= 0) && (TRUE != uim_efs_write(fhandle,data_to_write,
                                         MIN(num_bytes,UIM_EFSLOG_FILE_SIZE),
                                                   uim_ptr)))
      {
       uim_close_log_timeout_info(uim_ptr, data_to_write, fhandle);
        return FALSE;
      }
      uim_ptr->atr.atr_buf.num_bytes = UIM_MAX_ATR_CHARS;
    }
    else
    {
      /* Indicate the number of ATR bytes */
      num_bytes = snprintf((char *)data_to_write,UIM_EFSLOG_FILE_SIZE,
                           "Num atr bytes is 0x%x\r\n",
                           uim_ptr->atr.atr_buf.num_bytes);
      if ((num_bytes >= 0) && (TRUE != uim_efs_write(fhandle,data_to_write,
                                         MIN(num_bytes,UIM_EFSLOG_FILE_SIZE),
                                         uim_ptr)))
      {
        uim_close_log_timeout_info(uim_ptr, data_to_write, fhandle);
        return FALSE;
      }
    }

    num_bytes = snprintf((char *)data_to_write,UIM_EFSLOG_FILE_SIZE,"%s",
                        "ATR : ");
    if(uim_ptr->atr.atr_buf.num_bytes < UIM_MAX_ATR_CHARS)
    {
      for(i=0; (int)i<uim_ptr->atr.atr_buf.num_bytes; i++)
      {
        (void)snprintf(temp_buff,sizeof(temp_buff),"0x%x ",uim_ptr->atr.atr_buf.data[i]);
        (void)strlcat(data_to_write,temp_buff,UIM_EFSLOG_FILE_SIZE);
      }
    }
    (void)strlcat(data_to_write,"\r\n",UIM_EFSLOG_FILE_SIZE);
    unum_bytes = strlen(data_to_write);
    if (TRUE != uim_efs_write(fhandle,data_to_write,(int)unum_bytes,
                                                   uim_ptr))
    {
      uim_close_log_timeout_info(uim_ptr, data_to_write, fhandle);
      return FALSE;
    }

    if((uim_ptr->state.op_params_buf.FI) < UIM_CRCF_SIZE &&
       (uim_ptr->state.op_params_buf.DI < UIM_BRAF_SIZE))
    {
      /* Log the information about the FI and DI values being used */
      num_bytes = snprintf((char *)data_to_write,UIM_EFSLOG_FILE_SIZE,
                           "Running with FI = 0x%x, DI = 0x%x \r\n",
                           (int)crcf_values[uim_ptr->state.op_params_buf.FI],
                           (int)braf_values[uim_ptr->state.op_params_buf.DI]);
    }
    if ((num_bytes >= 0) && (TRUE != uim_efs_write(fhandle,data_to_write,
                                       MIN(num_bytes,UIM_EFSLOG_FILE_SIZE),
                                       uim_ptr)))
    {
      uim_close_log_timeout_info(uim_ptr, data_to_write, fhandle);
      return FALSE;
    }


    /* Log the value of timeout value calculated */
    num_bytes = snprintf((char *)data_to_write,UIM_EFSLOG_FILE_SIZE,
                         "Calculated timeout value is %ld \r\n",
                        uim_ptr->card_cmd.work_waiting_time);
    if ((num_bytes >= 0) && (TRUE != uim_efs_write(fhandle,data_to_write,
                                       MIN(num_bytes,UIM_EFSLOG_FILE_SIZE),
                                       uim_ptr)))
    {
      uim_close_log_timeout_info(uim_ptr, data_to_write, fhandle);
      return FALSE;
    }

    /* Log the driver UIM_RX_STATE indicates what exactly the
     * driver is expecting
     */
    num_bytes = snprintf((char *)data_to_write,UIM_EFSLOG_FILE_SIZE,"%s 0x%x\r\n",
                        "uim_rx_state is : ",
                        uim_ptr->rxtx_state_machine.rx_state);
    if ((num_bytes >= 0) && (TRUE != uim_efs_write(fhandle,data_to_write,
                                       MIN(num_bytes,UIM_EFSLOG_FILE_SIZE),
                                       uim_ptr)))
    {
      uim_close_log_timeout_info(uim_ptr, data_to_write, fhandle);
      return FALSE;
    }

    /* Log the driver UIM_TX_STATE indicates what exactly the
     * driver is transmiting
     */
    num_bytes = snprintf((char *)data_to_write,UIM_EFSLOG_FILE_SIZE,"%s 0x%x\r\n",
                        "uim_tx_state is : ",
                        uim_ptr->rxtx_state_machine.tx_state);
    if ((num_bytes >= 0) && (TRUE != uim_efs_write(fhandle,data_to_write,
                                       MIN(num_bytes,UIM_EFSLOG_FILE_SIZE),
                                       uim_ptr)))
    {
      uim_close_log_timeout_info(uim_ptr, data_to_write, fhandle);
      return FALSE;
    }

    /* Check to see if this command has data to be sent*/
    if ( uim_ptr->command.req_buf.instrn_case != UIM_INSTRN_CASE_2)
    {
      /* Check to see if this is the last byte */
      if (uim_ptr->card_cmd.num_bytes_to_send == 1)
      {
        /* Check to see if we are in transmitting data Mode */
        if ( uim_ptr->rxtx_state_machine.tx_state == UIM_TX_SND_CMD_DATA )
        {
          /* Log the info that we are done sending the APDU */
          num_bytes = snprintf((char *)data_to_write,UIM_EFSLOG_FILE_SIZE,"%s\r\n",
                              "Both the command and data has been sent");
          if ((num_bytes >= 0) && (TRUE != uim_efs_write(
                                    fhandle,data_to_write,
                                    MIN(num_bytes,UIM_EFSLOG_FILE_SIZE),
                                    uim_ptr)))
          {
            uim_close_log_timeout_info(uim_ptr, data_to_write, fhandle);
            return FALSE;
          }
        }
        else
        {
          /* Log the info that we are done sending the command */
          num_bytes = snprintf((char *)data_to_write,UIM_EFSLOG_FILE_SIZE,"%s\r\n",
                              "Command is sent but not the data");
          if ((num_bytes >= 0) && (TRUE != uim_efs_write(fhandle,data_to_write,
                                           MIN(num_bytes,UIM_EFSLOG_FILE_SIZE),
                                           uim_ptr)))
          {
            uim_close_log_timeout_info(uim_ptr, data_to_write, fhandle);
            return FALSE;
          }
        }
      }
      else
      {
        /* We are in the middle of sending bytes either command or data */
        num_bytes = snprintf((char *)data_to_write,UIM_EFSLOG_FILE_SIZE,"%s 0x%x\r\n",
                            "Bytes remaining to send is : ",
                            uim_ptr->card_cmd.num_bytes_to_send);
        if ((num_bytes >= 0) && (TRUE != uim_efs_write(fhandle,data_to_write,
                                    MIN(num_bytes,UIM_EFSLOG_FILE_SIZE),
                                                   uim_ptr)))
        {
          uim_close_log_timeout_info(uim_ptr, data_to_write, fhandle);
          return FALSE;
        }
      }
    }
    else
    {
      /* This command does not have data to be sent but expects
       * data from the card.  Indicate the number of bytes expected
       */
      num_bytes = snprintf((char *)data_to_write,UIM_EFSLOG_FILE_SIZE,"%s 0x%x Bytes\r\n",
                          "This command expects data - Expecting ",
                          uim_ptr->card_cmd.num_resp_data_bytes_expected);
      if ((num_bytes >= 0) && (TRUE != uim_efs_write(fhandle,data_to_write,
                                         MIN(num_bytes,UIM_EFSLOG_FILE_SIZE),
                                         uim_ptr)))
      {
        uim_close_log_timeout_info(uim_ptr, data_to_write, fhandle);
        return FALSE;
      }

      /* Indicate the number of bytes that we have received*/
      num_bytes = snprintf((char *)data_to_write,UIM_EFSLOG_FILE_SIZE,
                           "%s 0x%x Bytes\r\n",
                          "We have received",
                          uim_ptr->card_cmd.num_resp_bytes_rcvd);
      if ((num_bytes >= 0) && (TRUE != uim_efs_write(fhandle,data_to_write,
                                         MIN(num_bytes,UIM_EFSLOG_FILE_SIZE),
                                         uim_ptr)))
      {
        uim_close_log_timeout_info(uim_ptr, data_to_write, fhandle);
        return FALSE;
      }


      /* List the bytes that we have already received  if in
       * meaning full range*/

      if ( uim_ptr->card_cmd.num_resp_bytes_rcvd > 256 )
      {
        uim_ptr->card_cmd.num_resp_bytes_rcvd = 256;
        /* Indicate the fact that we are showing only 256 bytes */
        num_bytes = snprintf((char *)data_to_write,UIM_EFSLOG_FILE_SIZE,"%s\r\n",
                            "num_resp_bytes_rcvd out of range truncating to 256");
        if ((num_bytes >= 0) && (TRUE != uim_efs_write(fhandle,data_to_write,
                                           MIN(num_bytes,UIM_EFSLOG_FILE_SIZE),
                                           uim_ptr)))
        {
          uim_close_log_timeout_info(uim_ptr, data_to_write, fhandle);
          return FALSE;
        }
      }

      num_bytes = snprintf((char *)data_to_write,UIM_EFSLOG_FILE_SIZE,"%s",
                          "Bytes received :");
      for(i=0; i<uim_ptr->card_cmd.num_resp_bytes_rcvd; i++)
      {
        (void)snprintf(temp_buff,sizeof(temp_buff),"0x%x ",*((unsigned char *)(&uim_ptr->command.req_buf.rsp_ptr->rsp) + i));
        (void)strlcat(data_to_write,temp_buff,UIM_EFSLOG_FILE_SIZE);
      }
      (void)strlcat(data_to_write,"\r\n",UIM_EFSLOG_FILE_SIZE);
      unum_bytes = strlen(data_to_write);
      if (TRUE != uim_efs_write(fhandle,data_to_write,(int)unum_bytes,
                                                   uim_ptr))
      {
        uim_close_log_timeout_info(uim_ptr, data_to_write, fhandle);
        return FALSE;
      }
    }

    num_bytes = snprintf((char *)data_to_write,UIM_EFSLOG_FILE_SIZE,"%s",
                        "APDU being sent :");

    for(i=0;i<(int32touint32(NUM_BYTES_APDU_HDR +
               ((uim_ptr->command.req_buf.instrn_case == UIM_INSTRN_CASE_2)?0:
               uim_ptr->command.req_buf.apdu_hdr.p3)));i++)
    {
      (void)snprintf(temp_buff,sizeof(temp_buff),"0x%x ",*((unsigned char *)(&uim_ptr->command.req_buf.apdu_hdr) + i));
      (void)strlcat(data_to_write,temp_buff,UIM_EFSLOG_FILE_SIZE);
    }
    (void)strlcat(data_to_write,"\r\n",UIM_EFSLOG_FILE_SIZE);

    unum_bytes = strlen(data_to_write);
    if (TRUE != uim_efs_write(fhandle,data_to_write,(int)unum_bytes,
                                                   uim_ptr))
    {
      uim_close_log_timeout_info(uim_ptr, data_to_write, fhandle);
      return FALSE;
    }

    if((uim_ptr->card_cmd.request_buffer_ptr) != NULL)
    {
       num_bytes = snprintf((char *)data_to_write,UIM_EFSLOG_FILE_SIZE,"%s 0x%x value 0x%x\r\n",
                          "Current position in the buffer is : ",
                          ((unsigned int) uim_ptr->card_cmd.request_buffer_ptr -
                           (unsigned int) (&uim_ptr->command.req_buf.apdu_hdr))+1,
                          *(uim_ptr->card_cmd.request_buffer_ptr));
    }
    else
    {
      UIMDRV_MSG_HIGH_0(uim_ptr->id,"Request Buffer Ptr is NULL, did not write current position in buffer");
    }

    if ((num_bytes >= 0) && (TRUE != uim_efs_write(fhandle,data_to_write,
                                       MIN(num_bytes,UIM_EFSLOG_FILE_SIZE),
                                       uim_ptr)))
    {
      uim_close_log_timeout_info(uim_ptr, data_to_write, fhandle);
      return FALSE;
    }


    num_bytes = snprintf((char *)data_to_write,UIM_EFSLOG_FILE_SIZE,"%s 0x%lX\r\n",
                        "UART STATUS is : ", uart_status);
    if ((num_bytes >= 0) && (TRUE != uim_efs_write(fhandle,data_to_write,
                                       MIN(num_bytes,UIM_EFSLOG_FILE_SIZE),
                                       uim_ptr)))
    {
      uim_close_log_timeout_info(uim_ptr, data_to_write, fhandle);
      return FALSE;
    }

  }
  else
  {
    num_bytes = snprintf((char *)data_to_write,UIM_EFSLOG_FILE_SIZE,
                         "%s\r\n","No command in progress\r\n");
    if ((num_bytes >= 0) && (TRUE != uim_efs_write(fhandle,data_to_write,
                                       MIN(num_bytes,UIM_EFSLOG_FILE_SIZE),
                                       uim_ptr)))
    {
      uim_close_log_timeout_info(uim_ptr, data_to_write, fhandle);
      return FALSE;
    }
  }

  /* ------------------------------------------------------------------------
     Need to close the file_handle used in the open.
     If the close fails, we'll assume it is still usable for now.
     ----------------------------------------------------------------------*/
  fhandle = mcfg_fclose(fhandle, MCFG_FS_TYPE_EFS);
  if (0 > fhandle)
  {
    UIMDRV_MSG_ERR_0(uim_ptr->id,"Failed to close written UimReset.txt file");
  }

  /* Let us return the memory allocated */
  UIM_FREE(data_to_write);

  UIMDRV_MSG_HIGH_0(uim_ptr->id,"End logging informationto UimReset.Txt");

  return TRUE;
} /* uim_log_timeout_info */


/**
 * Save GPIO register state to globals upon command response
 * timeout(Saves the 10 most recent occurances of command
 * response timeout on an internal or external command).
 *
 * @param uim_ptr Pointer to the global data common.
 * @param is_internal_command Internal or External.
 */
void uim_save_gpio_info(
  uim_instance_global_type *uim_ptr,
  boolean                  is_internal_command
)
{
  uint32 i;
  static uint32 num_times_called = 0;
  boolean uim_populate_hw_gpio_info_status = FALSE;

  if( NULL == uim_ptr )
  {
    UIMDRV_MSG_ERR_0(0, "uim_save_gpio_info: uim_ptr is NULL");
    return;
  }
  RETURN_IF_INSTANCE_INVALID(uim_ptr->id);

  UIMDRV_MSG_HIGH_0(uim_ptr->id, "uim_save_gpio_info in globals");
  /* index for circular buffer */
  i = num_times_called % UIM_NUM_TIMEOUT_INFO;
  if(i >= UIM_NUM_TIMEOUT_INFO)
  {
    UIMDRV_MSG_ERR_1(uim_ptr->id, "uim_save_gpio_info index out of range %d",
                     i);
    i = 0;
  }

  uim_ptr->debug.uim_timeout_gpio_info[i].isInternalCmd  = is_internal_command;
  uim_ptr->debug.uim_timeout_gpio_info[i].num_timeouts   = num_times_called;

  uim_populate_hw_gpio_info_status = uim_populate_hw_gpio_info(uim_ptr, &uim_ptr->debug.uim_timeout_gpio_info[i]);

  if (uim_populate_hw_gpio_info_status)
  {
    UIMDRV_MSG_ERR_3( uim_ptr->id, "Timeout Print UIM GPIO input RST Value:0x%x CLK Value:0x%x DATA Value:0x%x",
                      uim_ptr->debug.uim_timeout_gpio_info[i].gpio_input.reset,
                      uim_ptr->debug.uim_timeout_gpio_info[i].gpio_input.clk,
                      uim_ptr->debug.uim_timeout_gpio_info[i].gpio_input.data);
    UIMDRV_MSG_ERR_1( uim_ptr->id, "Detect Value: 0x%x",
                      uim_ptr->debug.uim_timeout_gpio_info[i].gpio_input.detect);
    UIMDRV_MSG_ERR_3( uim_ptr->id, "Data_CFG:Dir: 0x%x Pull: 0x%x Driv: 0x%x",
                      uim_ptr->debug.uim_timeout_gpio_info[i].data_config.eDirection,
                      uim_ptr->debug.uim_timeout_gpio_info[i].data_config.ePull,
                      uim_ptr->debug.uim_timeout_gpio_info[i].data_config.eDriveStrength);
    UIMDRV_MSG_ERR_3( uim_ptr->id, "CLK_CFG:Dir: 0x%x Pull: 0x%x Driv: 0x%x",
                      uim_ptr->debug.uim_timeout_gpio_info[i].clk_config.eDirection,
                      uim_ptr->debug.uim_timeout_gpio_info[i].clk_config.ePull,
                      uim_ptr->debug.uim_timeout_gpio_info[i].clk_config.eDriveStrength);
    UIMDRV_MSG_ERR_3( uim_ptr->id, "Reset_CFG:Dir: 0x%x Pull: 0x%x Driv: 0x%x",
                      uim_ptr->debug.uim_timeout_gpio_info[i].reset_config.eDirection,
                      uim_ptr->debug.uim_timeout_gpio_info[i].reset_config.ePull,
                      uim_ptr->debug.uim_timeout_gpio_info[i].reset_config.eDriveStrength);

    if(uim_ptr->flag.runtime_disable_recovery)
    {
      UIMDRV_MSG_ERR_3(uim_ptr->id, "Set the UIM lines to pull-up input GPIOs values:RST:0x%x CLK:0x%x DATA:0x%x",
                       uim_ptr->debug.uim_timeout_gpio_info[i].gpio_input_pull_up.reset,
                       uim_ptr->debug.uim_timeout_gpio_info[i].gpio_input_pull_up.clk,
                       uim_ptr->debug.uim_timeout_gpio_info[i].gpio_input_pull_up.data);
      UIMDRV_MSG_ERR_1(uim_ptr->id, "Detect value: 0x%x",
                       uim_ptr->debug.uim_timeout_gpio_info[i].gpio_input_pull_up.detect);
    }
  }
  num_times_called++;
} /* uim_save_gpio_info */


/**
 * This file establishes the file in EFS to which EFSLOG will
 * write. The local EFSLOG buffer cannot be purged until this
 * function has completed successfully. This function will also
 * establish the local EFS buffer
 *
 * @param uim_ptr Pointer to the global data common.
 *
 * @return boolean Success or Fail.
 */
boolean uim_efslog_init( uim_instance_global_type *uim_ptr )
{
  byte   apdu_log_file_name[20];
  if(uim_nv_is_feature_enabled(UIMDRV_FEATURE_LOG_APDU_TO_EFS,
                               uim_ptr) == FALSE)
  {
    uim_ptr->debug.efslog_logging_in_progress = FALSE;
    return TRUE;
  }
  (void)memset((void *)apdu_log_file_name,
               (int)0x00,
               (size_t)sizeof(apdu_log_file_name));

  if(snprintf((char *)apdu_log_file_name,20,"Uim%dEfsAPDULog.Txt",(uim_ptr->id + 1)) < 0)
  {
    uim_ptr->debug.efslog_logging_in_progress = FALSE;
    return FALSE;
  }
  /* Before creating the EFS File for APDU logging, erase the old one if
  there is one */
  if(MCFG_FS_STATUS_EFS_ERR == mcfg_fs_delete((char *)apdu_log_file_name, MCFG_FS_TYPE_EFS, MCFG_FS_SUBID_NONE))
  {
    UIMDRV_MSG_ERR_0(uim_ptr->id,"Error removing file for EFSLog, or no old file to remove");
  }

  // Create file using MCFG file open API
  uim_ptr->debug.efslog_apdu_file_handle = mcfg_fopen((char *)apdu_log_file_name, MCFG_FS_O_WRONLY|MCFG_FS_O_CREAT|MCFG_FS_O_TRUNC,
                                                       MCFG_FS_ALLPERMS, MCFG_FS_TYPE_EFS, MCFG_FS_SUBID_NONE);

  if (uim_ptr->debug.efslog_apdu_file_handle < 0)
  {
    uim_ptr->debug.efslog_logging_in_progress = FALSE;
    return FALSE;
  }

  /* Allocate space for main buffer */
  uim_ptr->debug.efslog_apdu_buffer.data = uim_malloc(UIM_EFSLOG_MAX_BUFFER_SIZE);

  /* If allocation failed, return without turning on logging */
  if(uim_ptr->debug.efslog_apdu_buffer.data == NULL)
  {
    /* Close the EFS file  */
    (void)mcfg_fclose(uim_ptr->debug.efslog_apdu_file_handle, MCFG_FS_TYPE_EFS);
    uim_ptr->debug.efslog_logging_in_progress = FALSE;
    return FALSE;
  }

  /* Allocate space for secondary outgoing data buffer */
  uim_ptr->debug.efslog_apdu_outgoing_data_buffer.data = uim_malloc(UIM_EFSLOG_MAX_BUFFER_SIZE);

  /* If allocation failed, return without turning on logging */
  if(uim_ptr->debug.efslog_apdu_outgoing_data_buffer.data == NULL)
  {
    /* Close the EFS file and free up the local buffer memory */
    (void)mcfg_fclose(uim_ptr->debug.efslog_apdu_file_handle, MCFG_FS_TYPE_EFS);
    UIM_FREE(uim_ptr->debug.efslog_apdu_buffer.data);
    uim_ptr->debug.efslog_apdu_buffer.buffer_size = 0;
    uim_ptr->debug.efslog_logging_in_progress = FALSE;
    return FALSE;
  }
  uim_ptr->debug.efslog_apdu_buffer.buffer_size = 0;
  uim_ptr->debug.efslog_apdu_outgoing_data_buffer.buffer_size = 0;

  uim_ptr->debug.efslog_logging_in_progress = TRUE;
  return TRUE;
} /* uim_efslog_init */


/**
 * This function takes a character to be stored to the buffer
 * and an attribute,only if EFS Logging is currently in
 * progress. It converts the character to ascii, and checks the
 * attribute to see if it differs from the previous character's
 * attribute. If it does not, then it simply places the ASCII
 * value of the character in the next space in the buffer (One
 * character will become two ASCII bytes).  If the attribute is
 * different, it will place a new line character '\n' followed
 * by the attribute tag, a colon, and then the character (in
 * ASCII format).
 *
 * @param attribute Attribute of logging data
 * @param character Data to be logged.
 * @param uim_ptr Pointer to the global data common.
 */
void uim_efslog_apdu_to_buffer(
  uim_instance_global_type *uim_ptr
)
{
  /* for converting from HEX To ASCII */
  char ascii[17] = "0123456789ABCDEF";

  /* Create a local ptr to uim_efslog_apdu_buffer to improve
     code readability */
  uim_efslog_buffer_type *log_buf_ptr = &uim_ptr->debug.efslog_apdu_buffer;

  /* If the buffer is half full, set the EFSLOG signal */
  if(uim_efslog_purge_check(uim_ptr))
  {
    (void) rex_set_sigs( uim_ptr->tcb_ptr, UIM_EFSLOG_PURGE_SIG );
  }

  /* If EFS Logging not currently in progress, return without doing anyting */
  if(!uim_ptr->debug.efslog_logging_in_progress || log_buf_ptr->data == NULL)
  {
    return;
  }
  if (log_buf_ptr->buffer_size >= (UIM_EFSLOG_MAX_BUFFER_SIZE - 8))
  {
    /* There must be at least 8 empty spaces for single slot feature and 17
     * empty spaces for dual slot feature left in the buffer to log another
     * character in case a new attribute tag is encountered
     */
    return;
  }

  /* If the attibute has changed, start a new line */
  if(uim_ptr->debug.log_char.attrib != uim_ptr->debug.efslog_previous_char_attribute)
  {
    /* Insert a carriage return and line feed */
    log_buf_ptr->data[log_buf_ptr->buffer_size++] = UIM_EFSLOG_ASCII_CR;
    log_buf_ptr->data[log_buf_ptr->buffer_size++] = UIM_EFSLOG_ASCII_LF;
    /* Store Upper Nibble of Attribute tag in ASCII */
    switch((uim_log_attrib_type)uim_ptr->debug.log_char.attrib)
    {
      case UIM_LOG_TX_DATA:
        log_buf_ptr->data[log_buf_ptr->buffer_size++] = 'T';
        log_buf_ptr->data[log_buf_ptr->buffer_size++] = 'X';
        break;
      case UIM_LOG_RX_DATA:
        log_buf_ptr->data[log_buf_ptr->buffer_size++] = 'R';
        log_buf_ptr->data[log_buf_ptr->buffer_size++] = 'X';
        break;
      /* Required because of compiler warnings */
      case UIM_LOG_TSTAMP:
      default:
        break;
    }

    /* Insert a Colon and a space */
    log_buf_ptr->data[log_buf_ptr->buffer_size++] = ':';
  }

  /* Store Upper Nibble of byte in ASCII */
  log_buf_ptr->data[log_buf_ptr->buffer_size++] = ascii[uim_ptr->debug.log_char.the_char >> 4];

  /* Store Lower Nibble of byte in ASCII */
  log_buf_ptr->data[log_buf_ptr->buffer_size++] = ascii[uim_ptr->debug.log_char.the_char & 0x0F];

  /* Update Previous Char Attribute tag */
  uim_ptr->debug.efslog_previous_char_attribute = uim_ptr->debug.log_char.attrib;

} /* uim_efslog_apdu_to_buffer */


/**
 * This function gets called when a the local logging buffer
 * reaches a certain threashold, and we want to purge this data
 * to the EFS file.  This makes a blocking call to efs_write.
 * First, we copy into the secondary buffer and then write the
 * contents of the secondary buffer to EFS.
 *
 * @param uim_ptr Pointer to the global data common.
 */
void uim_efslog_save_buffer_to_efs( uim_instance_global_type *uim_ptr )
{
  int return_status = 0;

  if(uim_nv_is_feature_enabled(UIMDRV_FEATURE_LOG_APDU_TO_EFS,
                               uim_ptr) == FALSE)
  {
    /* Increment the EFS Write counter in case we have to stop logging
       before we ever get a GSDI Event */
    uim_ptr->debug.efslog_save_count++;
    return;
  }
  (void) rex_clr_sigs( uim_ptr->tcb_ptr, UIM_EFSLOG_PURGE_SIG );

  if( (uim_ptr->debug.efslog_apdu_buffer.buffer_size > 0) &&
      (TRUE == uim_ptr->debug.efslog_logging_in_progress)
    )
  {
    uim_ptr->debug.efslog_apdu_outgoing_data_buffer.buffer_size =
                      uim_ptr->debug.efslog_apdu_buffer.buffer_size;
    uim_memscpy(uim_ptr->debug.efslog_apdu_outgoing_data_buffer.data,
           int32touint32(uim_ptr->debug.efslog_apdu_outgoing_data_buffer.buffer_size),
           uim_ptr->debug.efslog_apdu_buffer.data,
           int32touint32(uim_ptr->debug.efslog_apdu_outgoing_data_buffer.buffer_size));
    /* Clear the buffer and set size to 0 */
    memset(uim_ptr->debug.efslog_apdu_buffer.data,0,UIM_EFSLOG_MAX_BUFFER_SIZE);
    uim_ptr->debug.efslog_apdu_buffer.buffer_size = 0;
    return_status = mcfg_fwrite( uim_ptr->debug.efslog_apdu_file_handle,
                                (void *)(uim_ptr->debug.efslog_apdu_outgoing_data_buffer.data),
                                 int32touint32(uim_ptr->debug.efslog_apdu_outgoing_data_buffer.buffer_size),
                                 MCFG_FS_TYPE_EFS );

    /* If the write failed, we want to clean-up and stop logging.  However,
       if the write failed because the buffer size was zero, we don't count
       this as a failure.  Also, if logging is already off, it means that this
       is the final write called from uim_efslog_clean_up.  Clean-up will
       free the buffers, so don't do it here */

     if((return_status <= 0) &&
        (!(uim_ptr->debug.efslog_apdu_outgoing_data_buffer.buffer_size == 0)) &&
         uim_ptr->debug.efslog_logging_in_progress)
     {
      /* There was an error saving to EFS, or it's full.  Clean up without
      trying to save again */
      uim_ptr->debug.efslog_logging_in_progress = FALSE;
      (void) rex_clr_sigs( uim_ptr->tcb_ptr, UIM_EFSLOG_PURGE_SIG );
    (void)mcfg_fclose(uim_ptr->debug.efslog_apdu_file_handle, MCFG_FS_TYPE_EFS);
      UIM_FREE(uim_ptr->debug.efslog_apdu_buffer.data);
      uim_ptr->debug.efslog_apdu_buffer.buffer_size = 0;
      UIM_FREE(uim_ptr->debug.efslog_apdu_outgoing_data_buffer.data);
      uim_ptr->debug.efslog_apdu_outgoing_data_buffer.buffer_size = 0;
      return;
    }

    /* Increment the EFS Write counter in case we have to stop logging
       before we ever get a GSDI Event */
    uim_ptr->debug.efslog_save_count++;
  }
} /* uim_efslog_save_buffer_to_efs */


/**
 * Cleans up EFSLOG when complete
 *
 * @param uim_ptr
 */
void uim_efslog_clean_up( uim_instance_global_type *uim_ptr )
{
  /* In case there's any data left in the buffer, send it to
     EFS now by enabling the flag.
     Clear the flag immediately after the buffer is written to efs */
  uim_ptr->debug.efslog_logging_in_progress = TRUE;
  uim_efslog_save_buffer_to_efs(uim_ptr);
  uim_ptr->debug.efslog_logging_in_progress = FALSE;

  if(uim_nv_is_feature_enabled(UIMDRV_FEATURE_LOG_APDU_TO_EFS,
                               uim_ptr) == FALSE)
  {
    return;
  }

  /* Close the EFS file and free up the local buffer memory */
  (void) rex_clr_sigs( uim_ptr->tcb_ptr, UIM_EFSLOG_PURGE_SIG );
  /* Close the EFS file and free up the local buffer memory */
  (void)mcfg_fclose(uim_ptr->debug.efslog_apdu_file_handle, MCFG_FS_TYPE_EFS);
  if(uim_ptr->debug.efslog_apdu_buffer.data != NULL)
  {
    UIM_FREE(uim_ptr->debug.efslog_apdu_buffer.data);
    uim_ptr->debug.efslog_apdu_buffer.buffer_size = 0;
  }
  if(uim_ptr->debug.efslog_apdu_outgoing_data_buffer.data != NULL)
  {
    UIM_FREE(uim_ptr->debug.efslog_apdu_outgoing_data_buffer.data);
    uim_ptr->debug.efslog_apdu_outgoing_data_buffer.buffer_size = 0;
  }
  return;
} /* uim_efslog_clean_up */


/**
 * DEFINITIONS OF INTERNAL FUNTIONS
 *
 */

/**
 * This function allocate memory and copy buffered data to the
 * memory for diag. It initializes the buffer parameters when
 * copying is done.
 *
 * @param uim_ptr Pointer to the global data common.
 *
 * @return boolean Success or Fail.
 */
static boolean uim_log_put_buffer
(
  uim_instance_global_type *uim_ptr
)
{
  uint8             log_len_local = 0;
  uim_log_data_type *log_ptr  = NULL;
  boolean           status = FALSE;
#ifdef FEATURE_UIM_TEST_FRAMEWORK
  #error code not present
#endif /* FEATURE_UIM_TEST_FRAMEWORK */

  if (uim_ptr->debug.log_data.length == 0)
    return TRUE; /* Nothing to log */

  /* Use local variable to store length of buffer, so that it
     does not get updated while switching to isr context */
  log_len_local = uim_ptr->debug.log_data.length;

  if (MULTI_SLOT_LOGGING_MASK_14CE == uim_ptr->debug.apdu_log_mask.log_mask)
  {
    log_ptr = (uim_log_data_type *) log_alloc (LOG_UIM_DS_DATA_C,
              FPOS(uim_log_data_type, data) + log_len_local);
  }
  else if (SINGLE_SLOT_LOGGING_MASK_1098 == uim_ptr->debug.apdu_log_mask.log_mask)
  {
    log_ptr = (uim_log_data_type *) log_alloc (LOG_UIM_DATA_C,
                FPOS(uim_log_data_type, data) + log_len_local);
  }

  if (log_ptr != NULL)
  {
    log_ptr->length = log_len_local;

    if (log_ptr->length <= (LOG_UIM_MAX_CHARS + LOG_UIM_TSTAMP_SIZE))
    {
      uim_memscpy( (void *) (log_ptr->data),
               sizeof(log_ptr->data),
              (void *) uim_ptr->debug.log_data.data,
              log_ptr->length );
    }

    /* Do not commit data if uim_log_data got updated after log_alloc
       as we may loose data because of this */
    if(log_len_local != uim_ptr->debug.log_data.length)
    {
      log_free((uim_log_data_type  *) log_ptr);
      return FALSE;
    }
    log_commit((uim_log_data_type  *) log_ptr);
    status = TRUE;

#ifdef FEATURE_UIM_TEST_FRAMEWORK
      #error code not present
#endif /* FEATURE_UIM_TEST_FRAMEWORK */
  }

  /* Reset the parameters */
  uim_ptr->debug.log_data.length = 0;
  uim_ptr->debug.log_char.attrib = UIM_LOG_ATTRIB_INIT;
  return status;
} /* uim_log_put_buffer */


/**
 * This funciton put a byte along with the attrib byte to the
 * buffer.
 *
 * @param uim_ptr Pointer to the global data common.
 */
static void uim_log_put_char( uim_instance_global_type *uim_ptr )
{
  /* LOG_UIM_MAX_CHARS is 247 while uim_log_data_ptr->data[] size is
     255 bytes. Last 8 bytes are reserved for time stamp.
     Need to add boundary checks so that for always keep last 8 bytes
     free for time stamp.

     We add minimum three log bytes. We need to adjust boundary check
     appropriately.
  */
  if (MULTI_SLOT_LOGGING_MASK_14CE == uim_ptr->debug.apdu_log_mask.log_mask &&
        uim_ptr->debug.log_data.length  <= (LOG_UIM_MAX_CHARS - 3))
  {
    /* Store the attribute byte */
    uim_ptr->debug.log_data.data[uim_ptr->debug.log_data.length++] = uim_ptr->debug.log_char.attrib;
    /* Store the slot byte */
    uim_ptr->debug.log_data.data[uim_ptr->debug.log_data.length++] = uim_ptr->debug.log_char.slot_id;
    /* Store the byte */
    uim_ptr->debug.log_data.data[uim_ptr->debug.log_data.length++] = uim_ptr->debug.log_char.the_char;
  }
  else if (SINGLE_SLOT_LOGGING_MASK_1098 == uim_ptr->debug.apdu_log_mask.log_mask &&
             uim_ptr->debug.log_data.length  <= (LOG_UIM_MAX_CHARS - 2))
  {
    /* Store the attribute byte */
    uim_ptr->debug.log_data.data[uim_ptr->debug.log_data.length++] = uim_ptr->debug.log_char.attrib;
    /* Store the byte */
    uim_ptr->debug.log_data.data[uim_ptr->debug.log_data.length++] = uim_ptr->debug.log_char.the_char;
  }

  if(uim_ptr->debug.log_data.length  >= LOG_UIM_MAX_CHARS)
  {
    if(!uim_log_put_buffer(uim_ptr))
    {
      /* As buffer could not be committed, reset the length of buffer
         to prevent buffer overflow */
      uim_ptr->debug.log_data.length = 0;
      uim_ptr->debug.log_char.attrib = UIM_LOG_ATTRIB_INIT;
    }
  }
} /* uim_log_put_char */


/**
 * This function put time stamp along with attrib byte to the
 * buffer
 *
 * @param uim_ptr Pointer to the global data common.
 */
static void uim_log_put_tstamp
(
  uim_instance_global_type *uim_ptr
)
{
  qword                t_stamp;
  uim_log_data_type   *uim_log_data_ptr = NULL;

  if (uim_ptr->id  >= UIM_MAX_INSTANCES)
  {
    UIMDRV_MSG_ERR_0(uim_ptr->id,"uim_log_put_tstamp invalid instance");
    return;
  }

  uim_log_data_ptr = &uim_ptr->debug.log_data;

  /* LOG_UIM_MAX_CHARS is 247 while uim_log_data_ptr->data[] size is
     255 bytes. Last 8 bytes are reserved for time stamp.
     Need to add boundary checks so that for always keep last 8 bytes
     free for time stamp.

     We add minimum three log bytes log bytes before time stamp. We need to
     adjust boundary check appropriately.
  */
  if (uim_log_data_ptr->length < (LOG_UIM_MAX_CHARS - 1))
  {
    uim_ptr->debug.log_char.attrib = UIM_LOG_TSTAMP;
    if (MULTI_SLOT_LOGGING_MASK_14CE == uim_ptr->debug.apdu_log_mask.log_mask)
    {
      /* Store the attribute byte */
      uim_log_data_ptr->data[uim_log_data_ptr->length++] = uim_ptr->debug.log_char.attrib;
      /* Store the slot byte */
      uim_log_data_ptr->data[uim_log_data_ptr->length++] = uim_ptr->debug.log_char.slot_id;
    }
    else if (SINGLE_SLOT_LOGGING_MASK_1098 == uim_ptr->debug.apdu_log_mask.log_mask)
    {
      /* Store the attribute byte */
      uim_log_data_ptr->data[uim_log_data_ptr->length++] = uim_ptr->debug.log_char.attrib;
    }
    /* Get time stamp */
    /*lint -e{792) supress void cast of void */
    (void)time_get(t_stamp);
    /* Copy the time stamp to the buffer */
    uim_memscpy( (void *) &uim_log_data_ptr->data[uim_log_data_ptr->length],
            (sizeof(uim_log_data_ptr->data) - uim_log_data_ptr->length),
            (void *) t_stamp,
            LOG_UIM_TSTAMP_SIZE);
    /* Update the buffer length */
    uim_log_data_ptr->length   += LOG_UIM_TSTAMP_SIZE;
  }

  if(uim_log_data_ptr->length  >= LOG_UIM_MAX_CHARS)
  {
    if(!uim_log_put_buffer(uim_ptr))
    {
      /* As buffer could not be committed, reset the length of buffer
         to prevent buffer overflow */
      uim_log_data_ptr->length = 0;
      uim_ptr->debug.log_char.attrib = UIM_LOG_ATTRIB_INIT;
    }
  }
} /* uim_log_put_tstamp */


/**
 * This file checks if a purge is necessary for any EFS Buffers.
 *
 * @param uim_ptr Pointer to the global data common.
 *
 * @return boolean True if EFS logging feature is enabled.
 */
static boolean uim_efslog_purge_check(
  uim_instance_global_type *uim_ptr
)
{
  if(uim_nv_is_feature_enabled(UIMDRV_FEATURE_LOG_APDU_TO_EFS,
                               uim_ptr) == TRUE &&
     uim_ptr->debug.efslog_apdu_buffer.buffer_size >= (UIM_EFSLOG_MAX_BUFFER_SIZE/2))
  {
    return TRUE;
  }
  return FALSE;
} /* uim_efslog_purge_check */


/**
 * This procedure write given databuffer to given file handler.
 *
 * @param fhandle File handle
 * @param databuffer_p Pointer to the data buffer.
 * @param datasize Data size.
 * @param uim_ptr Pointer to the global data common.
 *
 * @return boolean Success or Fail.
 */
static boolean uim_efs_write(
  int                       fhandle,
  const char               *databuffer_p,
  int                       datasize,
  uim_instance_global_type *uim_ptr
)
{
  int32 bytewritten = 0;

  /* Check file handler, invalid file handler is denote by -1*/
  if ((0 > fhandle) || (NULL == databuffer_p))
  {
    UIMDRV_MSG_ERR_1(uim_ptr->id,"Invalid File Handle 0x%x or No data to write",
                     fhandle);
    if(NULL == databuffer_p)
    {
      return TRUE;
    }
    return FALSE;
  }

  /* Writing entire data */
  while (datasize > 0)
  {
    bytewritten = mcfg_fwrite(fhandle, (void *)(databuffer_p), int32touint32(datasize), MCFG_FS_TYPE_EFS );
    /* efs_write return -1 on error case */
    if( bytewritten < 0)
    {
      UIM_MSG_ERR_1("Write to UimReset efs failed - efs_errno: 0x%x", efs_errno);
      return FALSE;
    }
    datasize -= bytewritten;
    databuffer_p += bytewritten;
  } /* end of while */

  return TRUE;
}/* uim_efs_write */


/**
 * Close timeout info log, return allocated data memory to heap
 * and close file handle.
 *
 * @param uim_ptr Pointer to the global data common.
 * @param data_to_be_freed Pointer to the data to be freed.
 * @param fhandle File handle.
 */
static void uim_close_log_timeout_info(
  uim_instance_global_type  *uim_ptr,
  char                      *data_to_be_freed,
  int                       fhandle )
{
  UIMDRV_MSG_ERR_1((uim_ptr->id), "Failed to write on UimReset File : 0x%x",
                    fhandle);

  UIM_FREE(data_to_be_freed);
  (void)mcfg_fclose(fhandle, MCFG_FS_TYPE_EFS);
} /* uim_close_log_timeout_info */
