#ifndef _KR_VSIM_CONTROL_PRIVATE_H_
#define _KR_VSIM_CONTROL_PRIVATE_H_

#include <string.h>


/*  
	define VSIM_DEBUGGING

	This define controls the following: 
	  - Baseband / SIM communication logging via Qualcomm's logging engine in the following locations:
	    - uim_rxtx.c     (1 instance)
	    - uim_rxstatex.c (3 instances)
	  - Private function logging via writing to an EFS file (free space on EFS is 1MB, so if the phone crashes randomly, its because of this)
	    - krvsim_control_public.c (1 instance)

	This can be set or unset without recompiling on KnowRoaming's side (since its only used in TCL code)
   
*/
//[BUGFIX]-Add-BEGIN by TCTNB.Yongzhen.wang,10/26/2016,defect3232029,
//[telecom][uim][VSIM] Too much debugging logs in UIM Tx/Rx state machine
//#define VSIM_DEBUGGING
//[BUGFIX]-Add-END by TCTNB.Yongzhen.wang

// Used to service the logging wrappers
void vsim_private_handle_log (const char* tag, const char* data, const char* filename, int lineNumber, int arg, int isArgDefined);


// Gets the filename from the full file path
#define __FILENAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)


// Private logging wrappers
#define PRINT_0(data)             vsim_private_handle_log("INFO ", data, __FILENAME__, __LINE__, 0,   0)
#define PRINT_1(data, arg)        vsim_private_handle_log("INFO ", data, __FILENAME__, __LINE__, arg, 1)
#define PRINT_DEBUG_0(data)       vsim_private_handle_log("DEBUG", data, __FILENAME__, __LINE__, 0,   0)
#define PRINT_DEBUG_1(data, arg)  vsim_private_handle_log("DEBUG", data, __FILENAME__, __LINE__, arg, 1)
#define PRINT_ERROR_0(data)       vsim_private_handle_log("ERROR", data, __FILENAME__, __LINE__, 0,   0)
#define PRINT_ERROR_1(data, arg)  vsim_private_handle_log("ERROR", data, __FILENAME__, __LINE__, arg, 1)



////////////////////////////////////////////////////////////////////////////////////////////
//																																											  //
// These functions are to be exposed to TCL source code. DO NOT ADD ANYTHING ELSE IN HERE //
//																																											  //
////////////////////////////////////////////////////////////////////////////////////////////

void vsim_private_handle_data (unsigned short ef_file, unsigned char* data, int data_length, int fileOffset);
void vsim_private_setup_vsim (int sim_slot);
void vsim_private_set_clock_started();
void vsim_private_set_clock_stopped();
void vsim_private_set_gstk_shutdown_complete();
void vsim_private_set_powerdown_complete();
void vsim_private_set_powerup_complete();
void vsim_private_set_ongoing_reset();
void vsim_private_set_power_off_state();
void vsim_private_clear_ongoing_reset();
void vsim_private_clear_power_on_state();
void vsim_private_clear_gstk_shutdown();
void vsim_private_clear_clock_run_once();
int  vsim_private_get_powerdown_state();
int  vsim_private_get_power_on_state();
int  vsim_private_get_power_off_state();
int  vsim_private_get_clock_run_once();
int  vsim_private_is_ongoing_reset();
int  vsim_private_is_clock_running();
int  vsim_private_is_gstk_shutdown();
int  vsim_private_is_enabled (int sim_slot);
int  vsim_private_is_reset_needed (int sim_slot);

#endif