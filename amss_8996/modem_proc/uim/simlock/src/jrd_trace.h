/* Copyright (C) 2016 Tcl Corporation Limited */
/*
    Jrd trace head
*/
#ifndef JRD_TRACE_H
#define JRD_TRACE_H
#ifdef __JRD_TRACE_LOG__
void jrd_trace(const char * fmt,...);
void jrd_trace_begin(void);
void jrd_trace_end(void);


#define JRD_TRACE(a) jrd_trace a
#define JRD_TRACE_ENTRY(a) jrd_trace a; jrd_trace_begin()
#define JRD_TRACE_EXIT(a) jrd_trace_end(); jrd_trace a
#define JRD_TRACE_BUF_HEXDUMP(a) jrd_buffer_hexdump a
#define JRD_TRACE_FILE_HEXDUMP(a) jrd_file_hexdump a
#else
#define JRD_TRACE(a)
#define JRD_TRACE_ENTRY(a)
#define JRD_TRACE_EXIT(a)
#define JRD_TRACE_BUF_HEXDUMP(a)
#define JRD_TRACE_FILE_HEXDUMP(a)

#endif/*__JRD_TRACE_LOG__*/
#endif /*JRD_TRACE_H*/
