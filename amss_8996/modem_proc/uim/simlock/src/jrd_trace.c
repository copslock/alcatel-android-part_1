/* Copyright (C) 2016 Tcl Corporation Limited */
//#ifdef __JRD_TRACE_LOG__
#include <stdarg.h>
#include <stdio.h>
#ifdef __MUTEX__
#include "swipp_interface.h"
#endif /*__MUTEX__*/

#include "mmgsdi_efs.h"


//Begin QCT struct data and API type define
#define uint32 unsigned long



//End QCT struct data and API type define

//#define JRD_TRACE_FILE_DEF       "mmgsdi/perso/jrd_trace.log"
#define JRD_TRACE_FILE_DEF       "jrd_trace.log"
#define JRD_TRACE_FILE_MAX_SIZE   (10*1024*1024)
#define JRD_TRACE_FILE_MIN_SIZE   (1*1024)
#define JRD_TRACE_SIZE   JRD_TRACE_FILE_MIN_SIZE


#ifdef __MUTEX__
static kal_mutexid g_jrd_trace_mutex = NULL;
#endif /*__MUTEX__*/
static char  gp_trace_file_name[128] = {0};
static uint32  g_jrd_trace_file_space = JRD_TRACE_FILE_MAX_SIZE;
static char g_jrd_trace[JRD_TRACE_SIZE] = {0};
static int g_space_index = 0;
static uint32 g_file_index = 0;

//#define DBG_TEST
#ifdef DBG_TEST /*debug test*/
#define DBG_FILE_NAME "mmgsdi/perso/dbg_trace.log"
static char *dbg_name = "mmgsdi/perso/dbg_trace.log";
int g_fh = -1;
#endif

void jrd_trace_flush(char* pStr)
{
    int fh = -1;

    //ASSERT(pStr);

#ifdef DBG_TEST
    {
        uint32 uWrited;
        uWrited = gsdi_efs_write_file(DBG_FILE_NAME, strlen("Enter jrd_trace_flush\n")+1,
                                      "Enter jrd_trace_flush\n");
    }
#endif /*DBG_TEST*/

    /*Check file size*/
    if(g_jrd_trace_file_space > JRD_TRACE_FILE_MIN_SIZE)
    {
        uint32 uSize = strlen(pStr), uWrited, ret = 0;

        fh = efs_open(gp_trace_file_name, O_CREAT | O_WRONLY); /*O_TRUNC?*/
        //ASSERT (fh > -1);

        ret = efs_lseek(fh, g_file_index, SEEK_SET);
        //ASSERT(ret == g_file_index);

#ifdef DBG_TEST
    {
        uint32 uWrited;
        uWrited = gsdi_efs_write_file(DBG_FILE_NAME, strlen(pStr), pStr);
    }
#endif /*DBG_TEST*/

        uWrited = efs_write(fh, pStr, uSize);
        //ASSERT(uWrited == uSize);
        /*if(FS_DISK_FULL == ret)
        {
            uint32 index = 0;
            ret = FS_GetFilePosition (fh, &index);
            ASSERT(FS_NO_ERROR == ret);
            uWrited = index - g_file_index;
        }
        else
        {
            if(FS_NO_ERROR != ret)
                goto END;
        }*/

        g_jrd_trace_file_space -= uWrited;
        g_file_index += uWrited;

    }

//END:

    if(fh >= 0)
    {
        efs_close(fh);
    }

#ifdef DBG_TEST
    {
        uint32 uWrited;
        uWrited = gsdi_efs_write_file(DBG_FILE_NAME, strlen("Exit jrd_trace_flush\n")+1, "Exit jrd_trace_flush\n");
    }
#endif /*DBG_TEST*/

}

void jrd_trace_begin(void)
{
    g_space_index++;
}

void jrd_trace_end(void)
{
    g_space_index--;
}

void jrd_trace(const char* fmt, ...)
{
    char* pTrace = g_jrd_trace;
    va_list alist; /*va_list*//*__VALIST*/
    uint32 /*uTicks = 0, */uSecs = 0, i = 0;
    static int log_num = 0;
    char endLine[2] = {'\n','\0'};
#ifdef __MTK_MUTEX__
    if(!g_jrd_trace_mutex)
    {
         g_jrd_trace_mutex = kal_create_mutex("JrdTrace");
        ASSERT(g_jrd_trace_mutex);
    }

    kal_take_mutex(g_jrd_trace_mutex);
#endif /*__MTK_MUTEX__*/

#ifdef DBG_TEST
    //g_fh = FS_Open(dbg_name, FS_CREATE_ALWAYS | FS_READ_WRITE);
#endif /*DBG_TEST*/

    /*Create file*/
    if(0 == strlen(gp_trace_file_name))
    {
        int fh = -1;
        sprintf(gp_trace_file_name, JRD_TRACE_FILE_DEF);
        fh = efs_open(gp_trace_file_name, O_WRONLY);//| O_WRONLY /*Continue log*/
        //ASSERT(fh > FS_NO_ERROR);
        if(fh > -1)
        {//Exist
            struct fs_stat file_stat = {0};

            efs_close(fh);
            if(efs_stat(gp_trace_file_name,&file_stat) == 0)
            {//Add at the end of file
              g_file_index = file_stat.st_size;
              g_jrd_trace_file_space = JRD_TRACE_FILE_MAX_SIZE - g_file_index;
            }
            else
            {
              g_jrd_trace_file_space = JRD_TRACE_FILE_MAX_SIZE;
            }
        }
        else
        {//Create new file
          fh = efs_open(gp_trace_file_name, O_CREAT);
          if(fh > -1)
          {
            efs_close(fh);
            g_jrd_trace_file_space = JRD_TRACE_FILE_MAX_SIZE;
          }
        }
    }

    /*Add time*/
    uSecs = timetick_get_ms();
    //uSecs = timetick_cvt_from_sclk(uTicks, T_MSEC);
    sprintf (pTrace, "%5d | %10.3f | ", log_num, 0.001*uSecs);
    log_num++;

    for(i = 0; i < g_space_index && i < 20; i++)
    {
        strcat(pTrace, "  ");
    }

#ifdef DBG_TEST
    {
        uint32 uWrited;
        uWrited = gsdi_efs_write_file(DBG_FILE_NAME, strlen( "Get Ticks\n")+1, "Get Ticks\n");
        uWrited = gsdi_efs_write_file(DBG_FILE_NAME, strlen(pTrace), pTrace);
        uWrited = gsdi_efs_write_file(DBG_FILE_NAME, strlen("\n"), "\n");
    }
#endif /*DBG_TEST*/

    /*Print va list*/
   va_start(alist, fmt);
   vsnprintf(pTrace+strlen(pTrace), JRD_TRACE_SIZE-strlen(pTrace)-2, fmt, alist);
   pTrace[JRD_TRACE_SIZE-2] = endLine[0];
   pTrace[JRD_TRACE_SIZE-1] = endLine[1];
   va_end(alist);

#ifdef DBG_TEST
    {
        uint32 uWrited;
        uWrited = gsdi_efs_write_file(DBG_FILE_NAME, strlen("Get va_list\n")+1, "Get va_list\n");
        uWrited = gsdi_efs_write_file(DBG_FILE_NAME, strlen(pTrace), pTrace);
    }
#endif /*DBG_TEST*/
    /*Flush trace to file*/
    jrd_trace_flush(pTrace);
    jrd_trace_flush(endLine);

#ifdef __MTK_MUTEX__
    kal_give_mutex(g_jrd_trace_mutex);
#endif /*__MTK_MUTEX__*/
}

#if 0
static void jrd_write_local_file(const char *pFileName, char *pBuf, int uBufLen)
{
    FS_HANDLE fh = FS_NO_ERROR;
    int iFileLen;
    kal_wchar pwFileName[128]={0};
    kal_uint32 ret = 0, uWrited = 0;

    kal_wsprintf(pwFileName, pFileName);
    fh = FS_Open (pwFileName, FS_READ_WRITE | FS_CREATE);

    if(FS_NO_ERROR < fh)
    {
        //ret = FS_Seek(fh, 0, FS_FILE_END);
        FS_GetFileSize(fh, &iFileLen);
        ret = FS_Seek(fh, iFileLen, FS_FILE_CURRENT);
        ret = FS_Write (fh, pBuf, uBufLen, &uWrited);
        FS_Close(fh);
    }

}

static void DBGOUT_EXT (const char *pFileName, const char *fmt, ... )
{
    va_list     ap;
    char buffer[128]={0};

    va_start(ap,fmt);
    vsprintf(buffer,fmt,ap);
    va_end(ap);

    jrd_write_local_file(pFileName, buffer, strlen(buffer));
}

static void DBGOUT ( const char *fmt, ... )
{
    va_list     ap;
    char buffer[512] = {0}; //static

    va_start(ap,fmt);
    vsprintf(buffer,fmt,ap);
    va_end(ap);

    fprintf(stdout,"%s",buffer);
}
static void jrd_hexdump (const char *pFileName, const unsigned char *pData, int size )
{
    char     szHexLine[80] = {0}, szHex[12] = {0};
    unsigned char   x, h, l;
    int             i,j;

    if (!size || !pData)
        return;

    while (size>0)
    {
        memset(szHexLine,0x20,sizeof(szHexLine));
        szHexLine[77] = 0x0A;
        szHexLine[78] = 0x00;
        if (size>8)
            szHexLine[34] = '-';

        sprintf(szHex,"%08lX",(unsigned long)pData);
        memcpy(szHexLine,szHex,8);

        i=0;j=0;
        while (size>0)
        {
            x = *(pData++);
            size--;
            h = (x>>4)+0x30;
            l = (x&15)+0x30;

            if (h>0x39) h+=7;

            if (l>0x39) l+=7;

            szHexLine[i*3+10+j] = (char)h;
            szHexLine[i*3+11+j] = (char)l;

            if ((x<32) || (x>=127)) x = '.';

            szHexLine[i+61] = (char)x;

            i++;

            if (i==8)
                j = 2;

            if (i==16)
                break;
        }

        if(pFileName)
        {
            DBGOUT_EXT(pFileName, "%s",szHexLine);
        }
        else
        {
            jrd_trace( "%s",szHexLine );
        }
    }
}

void jrd_buffer_hexdump(unsigned char *pData, int size)
{
    jrd_hexdump(NULL, pData, size);
}

void jrd_file_hexdump(char *pFileName, unsigned char *pData, int size)
{
    jrd_hexdump((const char *)pFileName, (const unsigned char *)pData, size);
}
#endif
//#endif/*__JRD_TRACE_LOG__*/
