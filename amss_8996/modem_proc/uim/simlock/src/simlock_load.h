/* Copyright (C) 2016 Tcl Corporation Limited */
#ifndef SIMLOCK_LOAD_H
#define SIMLOCK_LOAD_H
#include <com_dtypes.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "tct.h"

#ifdef __JRD_PERSO_SML__
#define __DUMP__
#endif

#ifdef FEATURE_TCTNB_EMMC_IMEI
#define IMEI_RAM_MAGIC1           0x2012181B
#define FLASH_IMEI_SIZE            (32)
#define FLASH_MEID_SIZE           (14)
#define IMEI_SIZE                        (17)

/*Macros for Random NCK generation*/
#define NUM_OF_CAT                  (5)
#define NCK_IMEI_SIZE              (0x200)
#define NCK_SALT_SIZE              (0x20)
#define NCK_HASH_SIZE              (0x20)
#define NCK_SHA_SIZE                (NCK_SALT_SIZE+NUM_OF_CAT*NCK_HASH_SIZE)
#define ONE_BUFF_LEN                NCK_IMEI_SIZE

#ifdef    FEATURE_TCTNB_RPMB_FOR_SIMLOCK
#define SECURE_RO_BUF_SIZE     (1024*4)
#define CU_NAME_SIZE                 (20)
#define SECURE_RO_BUF_LEN_SIZE       (4)
#endif

typedef struct
{
    uint8 Salt[NCK_SALT_SIZE];
    uint8 Hash1[NCK_HASH_SIZE];
    uint8 Hash2[NCK_HASH_SIZE];
    uint8 Hash3[NCK_HASH_SIZE];
    uint8 Hash4[NCK_HASH_SIZE];
    uint8 Hash5[NCK_HASH_SIZE];
}
secure_Sha;

typedef struct
{
    uint32 magic1;
    /*IMEI*/
    uint8 imei_1[FLASH_IMEI_SIZE];
    uint8 imei_2[FLASH_IMEI_SIZE];
    uint8 imei_3[FLASH_IMEI_SIZE];
    uint8 imei_4[FLASH_IMEI_SIZE];

    /*Remove actually useless elements */
/*
    uint8 imei_1_backup[FLASH_IMEI_SIZE];
    uint8 imei_2_backup[FLASH_IMEI_SIZE];
    uint8 imei_3_backup[FLASH_IMEI_SIZE];
    uint8 imei_4_backup[FLASH_IMEI_SIZE];
    uint8 reserved[NCK_IMEI_SIZE-8*FLASH_IMEI_SIZE];
*/

/*Salt and Hash,ONLY 2 sims is needed*/
    secure_Sha sha1;
    secure_Sha sha2;
/*
   secure_Sha sha3;
    secure_Sha sha4;
    secure_Sha sha5;
    secure_Sha sha6;
    secure_Sha sha7;
    secure_Sha sha8;
*/
#ifdef FEATURE_TCTNB_MEID_FOR_CTCC
    uint8 meid[FLASH_MEID_SIZE/2+1];
#else
    uint8 meid[FLASH_MEID_SIZE];
#endif

#ifdef  FEATURE_TCTNB_RPMB_FOR_SIMLOCK
    uint16 CU_len;
    uint8 CU_buf[CU_NAME_SIZE];
    uint32 sec_len;
    uint8 sec_buf[SECURE_RO_BUF_SIZE];
#endif /*FEATURE_TCTNB_RPMB_FOR_SIMLOCK*/
}
secure_imei;

uint8 jrd_NCK_load(uint8 *buffer_p, uint32 len);
uint8 jrd_read_otp_imei(uint8* imei_buffer, uint16 size, uint8 source);

uint8 jrd_read_CU(uint8 *CU_buffer, int size);
uint8 jrd_secro_get_length_and_errorCode(uint32 *len, uint8 *errorCode);
uint8 jrd_secro_load(uint8 *buffer_p, uint32 len);

/*!!!!!!ONLY for local test with x file in efs!!!!!*/
uint8 simlock_perso_load_Xfile_from_efs(uint8 *outPersoBuffer,uint32 len);

#ifdef __DUMP__
void simlock_load_test();
#endif

#endif /*FEATURE_TCTNB_EMMC_IMEI*/
#endif /*SIMLOCK_LOAD_H*/
