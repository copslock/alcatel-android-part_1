#ifndef __APR_MISC_H__
#define __APR_MISC_H__

/*
  Copyright (C) 2009 QUALCOMM Technologies, Inc.
  All rights reserved.
  Confidential and Proprietary - QUALCOMM Technologies, Inc.

  $Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/apr/osal/inc/apr_misc.h#1 $
  $Author: mplcsds1 $
*/

#include "apr_comdef.h"

APR_INTERNAL int32_t apr_misc_sleep ( uint64_t time_ns );

#endif /* __APR_MISC_H__ */

