/*!
  @file
  mem_share_qmi_client.c

  @brief
  Implementation of the QMI client used to request and free memory from the server on HLOS.

*/

/*===========================================================================

  Copyright (c) 2009-2014 QUALCOMM Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  QUALCOMM Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of QUALCOMM Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/core.mpss/3.4.c3.11/services/utils/src/mem_share_qmi_client.c#1 $

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
05/29/14   abh     Replaced qmi_client_init() with qmi_client_init_instance().
11/30/12   ps      Initial Version

===========================================================================*/

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/

#include "stdio.h"
#include "stdlib.h"
#include "qmi_client.h"
#include "qmi_cci_target_ext.h"
#include "dynamic_heap_memory_sharing_v01.h"
#include "mem_share_client.h"
#include "msg.h"


/*===========================================================================

                   INTERNAL DEFINITIONS AND TYPES

===========================================================================*/

#define QMI_CLNT_WAIT_SIG      (0x1 << 5)
#define QMI_CLNT_TIMER_SIG     (0x1 << 6)

#define MEM_SHARE_QMI_TIMEOUT_MS  5000  /* 5 secs */

qmi_idl_service_object_type   mem_share_service_object;
qmi_cci_os_signal_type        os_params;
qmi_client_type               mem_client_handle;


/*===========================================================================

  FUNCTION:  mem_client_ind_cb

===========================================================================*/
/*!
    @brief
    Helper function for the client indication callback

    @detail
    Does nothing as of now.

    @return
    None

*/
/*=========================================================================*/
void mem_client_ind_cb
(
 qmi_client_type                mem_client_handle,
 unsigned int                   msg_id,
 void                           *ind_buf,
 unsigned int                   ind_buf_len,
 void                           *ind_cb_data
)
{
  return;
}

/*===========================================================================

  FUNCTION:  mem_share_client_init

===========================================================================*/
/*!
    @brief
    Initialize the QMI client for dynamic memory allocation requests

    @detail
    This function will initialize the mem_share client to talk to the service on HLOS that will 
   allocate the memory on HLOS and pass the handle back to the client.

    @return
    MEM_SHARE_SUCCESS : Initialization was succesful
    MEM_SHARE_FAILURE: Initialization failed

*/
/*=========================================================================*/
mem_share_status_t mem_share_client_init(void)
{
   qmi_client_error_type ret_val;
   static boolean mem_share_client_initialized = FALSE;
   
   os_params.tcb = rex_self();
   os_params.sig = QMI_CLNT_WAIT_SIG;
   os_params.timer_sig = QMI_CLNT_TIMER_SIG;

   if (mem_share_client_initialized == TRUE)
    {
	  return MEM_SHARE_SUCCESS;
	}   
   
   mem_share_service_object = dhms_get_service_object_v01();
   if (!mem_share_service_object)
   {
      MSG_ERROR("mem_share_client_init: mem_share_service_object error \n",0,0,0);
      return MEM_SHARE_FAILURE;
   }

	ret_val = qmi_client_init_instance(	mem_share_service_object,
										QMI_CLIENT_INSTANCE_ANY,
										mem_client_ind_cb,
										NULL,
										&os_params,
										MEM_SHARE_QMI_TIMEOUT_MS,
										&mem_client_handle );

	if(ret_val != QMI_NO_ERR)
	  {
		MSG_ERROR("mem_share_client_init: qmi_client_init_instance returned failure \n",0,0,0);
		return MEM_SHARE_FAILURE;
	  }										
   
   mem_share_client_initialized = TRUE;
   
   return MEM_SHARE_SUCCESS;
}


/*===========================================================================

  FUNCTION:  mem_share_request_pool

===========================================================================*/
/*!
    @brief
    Send a request to the server to allocate a memory block to be used on modem

    @detail
    This function is called when modem requires additional memory to be allocated in the HLOS.
    This function requires the size and the block alignment required for the requested memory.
    The handle and number of bytes actually allocated are returned in the pointer *handle and *num_bytes_ret

    @return
    MEM_SHARE_SUCCESS : request for memory was succesful
    MEM_SHARE_FAILURE: request for memory failed

*/
/*=========================================================================*/
mem_share_status_t mem_share_request_pool(
                                             uint32_t num_bytes, 
                                             uint32_t block_alignment,
                                             uint32_t  *handle,
                                             uint32_t  *num_bytes_ret)
{
   int ret = 0;
   dhms_mem_alloc_req_msg_v01 mem_alloc_req;
   dhms_mem_alloc_resp_msg_v01 mem_alloc_resp;
   mem_share_status_t  return_stat = MEM_SHARE_SUCCESS;

   mem_alloc_req.num_bytes = num_bytes;
   mem_alloc_req.block_alignment = block_alignment;
   mem_alloc_req.block_alignment_valid = 0;

   MSG_HIGH("requesting for memory from server\n",0,0,0);

   /* QMI call to APPS to get memmory */
   ret = qmi_client_send_msg_sync(mem_client_handle,QMI_DHMS_MEM_ALLOC_REQ_V01,
                                            (void*)&mem_alloc_req,sizeof(mem_alloc_req),
                                            (void*)&mem_alloc_resp,sizeof(mem_alloc_resp),0 );
   if(QMI_NO_ERR != ret)
   {
      MSG_ERROR("requesting for memory from server failed, ret = %d\n",ret,0,0);
      return_stat = MEM_SHARE_IPC_FAILURE;
      *num_bytes_ret= 0;
      return return_stat;
   }

   if(mem_alloc_resp.resp != QMI_NO_ERR)
   {
      MSG_ERROR("requesting for memory from server failed, result = %d\n",mem_alloc_resp.resp,0,0);
      return_stat = MEM_SHARE_IPC_FAILURE;
      *num_bytes_ret= 0;
      return return_stat;
   }

   MSG_HIGH("requesting for memory from server success",0,0,0);
   MSG_HIGH("mem_alloc_resp. handle [%d] num_bytes: %d\n",\
               mem_alloc_resp.handle, mem_alloc_resp.num_bytes,0);

   /* Return size and handle to the memory */
   *num_bytes_ret = mem_alloc_resp.num_bytes;
   *handle = (uint32)mem_alloc_resp.handle;

  return return_stat;
}


/*===========================================================================

  FUNCTION:  mem_share_release_pool

===========================================================================*/
/*!
    @brief
    Send a request to the server to free a memory block that was allocated earlier by HLOS

    @detail
    This function is called when modem is done using the memory allocated earlier by HLOS.
    This function requires the handle to the memory block that was passed by HLOS.

    @return
    MEM_SHARE_SUCCESS : request for memory was succesful
    MEM_SHARE_FAILURE: request for memory failed

*/
/*=========================================================================*/
mem_share_status_t mem_share_release_pool(uint32 handle,
                                              uint32_t num_bytes)
{
  dhms_mem_free_req_msg_v01 mem_free_req;
  dhms_mem_free_resp_msg_v01 mem_free_resp;

  if(0 == handle || 0 == num_bytes)
  {
     MSG_ERROR("mem_share_release_pool,invalid arguments handle=%d, num_bytes=%d",handle,num_bytes,0);
     return MEM_SHARE_FAILURE;
  }
  mem_free_req.handle = (uint64)handle;

  MSG_HIGH("releasing memory to server ",0,0,0);
  if(QMI_NO_ERR != qmi_client_send_msg_sync(mem_client_handle,QMI_DHMS_MEM_FREE_REQ_V01,
                                            (void*)&mem_free_req,sizeof(mem_free_req),
                                            (void*)&mem_free_resp,sizeof(mem_free_resp),0 ))
  {
    MSG_ERROR("qmi_client_send_msg_sync returned failure",0,0,0);  
    return MEM_SHARE_FAILURE;
  }
  MSG_HIGH("releasing memory to server success",0,0,0);
  return MEM_SHARE_SUCCESS;
}

