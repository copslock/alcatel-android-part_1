/******************************************************************************
 * mem_share_client.h
 *
 * memory sharing interface functions
 * 
 *
 * Copyright (c) 2013
 * Qualcomm Technologies Incorporated.
 * All Rights Reserved.
 * Qualcomm Confidential and Proprietary
 *
 *****************************************************************************/

#ifndef __MEM_SHARE_CLIENT_H__
#define __MEM_SHARE_CLIENT_H__

/* To avoid errors in new users due to type issues */
#include <stdint.h>
/**
  @file mem_share_client.h
  @brief Memshare interface functions used to share the memory with HLOS. This module keeps an internal
  pool data structure for the registered usecase.
*/


#ifdef __cplusplus
extern "C" {
#endif


/** Status of the memshare operations */
typedef enum _mem_share_status_t
{
   MEM_SHARE_SUCCESS      =  0,
   MEM_SHARE_FAILURE      = -1,
   MEM_SHARE_IPC_FAILURE  = -2,
   MEM_SHARE_INVALID_POOL = -3,
}mem_share_status_t;


/**
  This function establish a client connection with HLOS service layer, This function should be called 
  by the subsystem before any of the client functions getting called.
  
  @param[in] usr  user name
  @return
  mem_share_status_t, status of the client intialization

  @dependencies
  None.
*/
mem_share_status_t mem_share_client_init(void);



/**
  This function is to allocate the memory from the HLOS, once the blocks are added to the pool user should request for
  blocks by mem_share_block_alloc() function

  @param[in] pusr_handle      block pool handle
  @param[in] num_blocks       number of blocks to be allocated from HLOS
  @param[in] block_size       block size of allocated blocks, each block will be physically continous  
  @param[in] block_alignment  Each block alignment
  @param[in] num_blocks_ret   number of blocks returned from the HLOS
  @return
  mem_share_status_t, status of the allocation from the HLOS

  @dependencies
  None.
*/

mem_share_status_t mem_share_request_pool(
                                             uint32_t num_bytes, 
                                             uint32_t block_alignment,
                                             uint32_t  *handle,
                                             uint32_t  *num_bytes_ret);


/**
  This is to trim the size of the pool, once the user released the usage of the block
  this pool should be trimed to return the memory to HLOS

  @param[in] pusr_handle      block pool handle
  @param[in] num_blocks       number of blocks to be freed to HLOS
  @param[in] block_size       block size of blocks to be freed
  @param[in] block_alignment  Each block alignment
  @param[in] num_trim_blocks  number of blocks freed.
  @return
  mem_share_status_t, status of the allocation from the HLOS

  @dependencies
  None.
*/
mem_share_status_t mem_share_release_pool(uint32 handle,
                                                 uint32_t num_bytes);


#ifdef __cplusplus
}
#endif

#endif /* __MEM_SHARE_CLIENT_H__ */
