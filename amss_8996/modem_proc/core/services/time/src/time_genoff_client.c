/*=============================================================================

                Time_genoff_Client.c

GENERAL DESCRIPTION
      Time Genoff Client Process Code

EXTERNAL FUNCTIONS
  time_genoff_opr
   Does time set/get operation on the basis of Inputs
   
INITIALIZATION AND SEQUENCING REQUIREMENTS
   None.

      Copyright (c) 2009 - 2014
      by QUALCOMM Technologies Incorporated.  All Rights Reserved.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

 This section contains comments describing changes made to the module.
 Notice that changes are listed in reverse chronological order.


$Header: //components/rel/core.mpss/3.4.c3.11/services/time/src/time_genoff_client.c#1 $ 
$DateTime: 2016/03/28 23:02:17 $ $Author: mplcsds1 $

when         who     what, where, why
--------   ---     ------------------------------------------------------------
05/01/13   abh     Created the file
=============================================================================*/
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stringl/stringl.h>
#include "time_svc.h"
#include "time_jul.h"
#include "time_conv.h"
#include "timetick_sclk64.h"
#include "time_genoff_client_v.h"
#include "osal.h"
#include "time_genoff_qdi_v.h"
#include "ats_v.h"
#include "err.h"
#include "rcinit.h"


/*****************************************************************************/
/*                          DATA DECLARATIONS & DEFINES                            */
/*****************************************************************************/

/* Qdi Time Genoff Client Handle to communicate with Guest os layer */
int time_genoff_client_qdi_handle = -1;

/* Flag to indicate if time genoff client is initialized */
boolean ptime_genoff_client_initialized = FALSE;

#define TIME_GENOFF_PRI 0x50						/* Priority for the worker thread */
#define TIME_GENOFF_STACK_SIZE 4096					/* Stack size for the worker thread */
qurt_thread_t      time_genoff_worker_thread_id;	/* Thread ID of worker thread */

/*-----------------------------------------------------------------------------
  Time Bases static information
-----------------------------------------------------------------------------*/

time_genoff_client_struct_type              ats_client_bases[ATS_MAX];

/* List of external time genoff pointers */



/*==============================================================================

                            A P I   D E F I N I T I O N S

=============================================================================*/



/*=============================================================================

FUNCTION TIME_GENOFF_GET_CLIENT_POINTER                                 

DESCRIPTION
  Returns the Pointer to different time bases 

DEPENDENCIES
  None

RETURN VALUE
  The pointer to time_genoff_struct_type

SIDE EFFECTS
  None

=============================================================================*/

time_genoff_client_ptr time_genoff_get_client_pointer
( 
  /* Time bases whose pointer needs to be returned */
  time_bases_type               base
)
{
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

  ASSERT( base < ATS_MAX );

  return &(ats_client_bases[base]) ;
     
} /* time_genoff_get_client_pointer */



/*=============================================================================

FUNCTION TIME_GENOFF_GET_CLIENT_OFFSET

DESCRIPTION
  Returns generic offset value of a given time base

DEPENDENCIES
  None

RETURN VALUE
  int64 value

SIDE EFFECTS
  None

=============================================================================*/
int64 time_genoff_get_client_offset
( 
    time_bases_type  base  
)
{
  /* Pointer to 'base' in consideration */
  time_genoff_client_ptr    ptime_genoff;
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  
  if ( base >= ATS_MAX ) 
    return 0;
 
  /* Get pointer to base in consideration */
  ptime_genoff = time_genoff_get_client_pointer(base);
  return ptime_genoff->generic_offset;
} /*  time_genoff_get_client_offset */



/*=============================================================================

FUNCTION TIME_GENOFF_CLIENT_GET_OPTIMIZED_TS

DESCRIPTION
  Gets Genoff Time for the time base in consideration in 64 bit Timestamp format, only if time_genoff is 
  initialized 

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

=============================================================================*/


void time_genoff_client_get_optimized_ts
(
  /* Pointer to time_genoff_args_type with genoff_ptr in consideration */
  time_genoff_args_ptr         pargs
)
{
  /* Pointer to genoff in consideration */
  time_genoff_client_ptr       ptime_genoff;

  /* Local vars for storing the genoff structure values */
  int64                        local_sum_generic_offset = 0;
  boolean                      local_sum_generic_offset_ts_is_negative;
  time_type                    local_sum_generic_offset_ts ;

  /*to store the generic offset in a temp variable*/
  uint32     old_generic_offset=0;  

  /*temporary timestamp value*/
  time_type                    ts_val_temp;

  /* This variable is used to erase TIME_GENOFF_TIMETICK_VALID from getting onto stack */
  volatile uint32 timetick_valid_flag = TIME_GENOFF_TIMETICK_VALID;  

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

  /* If ptime_genoff is not initilazed */
  if ( ptime_genoff_client_initialized == FALSE )
  {
    qw_set( pargs->ts_val, 0, 0 );
    return;
  }  
  
  qw_set( ts_val_temp, 0, 0 );
  
  /* Get pointer to genoff */
  if( pargs->operation & TIME_GENOFF_OPR_EXTERNAL_MASK )
  {
    /* Copy the values from external genoff pointer */
	local_sum_generic_offset = pargs->ext_genoff_ptr->sum_generic_offset;
	local_sum_generic_offset_ts_is_negative = 
		pargs->ext_genoff_ptr->sum_generic_offset_ts_is_negative;
	qw_equ( local_sum_generic_offset_ts ,
		   (pargs->ext_genoff_ptr->sum_generic_offset_ts) );
	
  }
  else
  {
    /* Get the values from the client genoff pointer */
    ptime_genoff = time_genoff_get_client_pointer( pargs->base );
	local_sum_generic_offset = ptime_genoff->sum_generic_offset;
	local_sum_generic_offset_ts_is_negative = 
		ptime_genoff->sum_generic_offset_ts_is_negative;
	qw_equ( local_sum_generic_offset_ts ,
		   (ptime_genoff->sum_generic_offset_ts) );	
  }

  do
  {    

	/*record the generic offset before we begin*/
	old_generic_offset = local_sum_generic_offset>>32;
    
    /* If offset is not negative, directly use time_sclk_get()
       for addition of uptime by copying offset to ts_val_temp */
    if(local_sum_generic_offset_ts_is_negative == FALSE)
       qw_equ(ts_val_temp, local_sum_generic_offset_ts);

	/* Get time from sclk */
	if ( pargs->dal_timetick_val_valid != timetick_valid_flag )
	{
	  /* Erase TIME_GENOFF_TIMETICK_VALID from getting onto stack.
         This will make sure there will not be any issues if clients
		 doesnt explicitly set dal_timetick_val_valid to 0 if they dont use it */
	  timetick_valid_flag = 0;
	  /* Get time from sclk */
	  time_sclk_get(ts_val_temp, (uint64)-1);
	}
	else
	{
      timetick_valid_flag = 0;	
	  time_sclk_get(ts_val_temp, pargs->dal_timetick_val);
	}


	/* add / sub sum_generic_offset_ts from uptime based
	    on the state of sum_generic_offset_ts_is_negative */
    if(local_sum_generic_offset_ts_is_negative == TRUE)
    {
       time_conv_ts_sub(ts_val_temp, local_sum_generic_offset_ts);
    }

  }while( FALSE == osal_atomic_compare_and_set(
                                  (osal_atomic_word_t *)&old_generic_offset,
                                  (local_sum_generic_offset)>>32,
                                  (old_generic_offset)) );

     qw_equ(pargs->ts_val, ts_val_temp);
     
} /* time_genoff_get_optimized_ts */


/*=============================================================================

FUNCTION TIME_GENOFF_OPR

DESCRIPTION
  Does time set/get operation on the basis of Inputs.

DEPENDENCIES
  Time to be set should never be less than uptime of the UE.

RETURN VALUE
  None

SIDE EFFECTS
  None

=============================================================================*/

void time_genoff_opr
(
  /* Pointer to time_genoff_args_type with genoff_ptr in consideration */
  time_genoff_args_ptr         pargs
)
{
  /* Arguments to be passed to base_genoff */
  time_genoff_args_struct_type temp_genoff_args;

  /* Genoff time, in secs */
  time_type                    ts_val_secs;

  /* Genoff time in secs temp variable */
  uint32                       lsecs;

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

  /* GET operation */
  if ( pargs->operation & TIME_GENOFF_OPR_GET_MASK )
  {

    /* unit of time */
    switch( pargs->unit )
    {
      case TIME_STAMP:
      {
        /* Get time in timestamp format */
        time_genoff_client_get_optimized_ts( pargs );
      }
	  break;
      case TIME_MSEC:
      {
        /* Get time in timestamp format */
        time_genoff_client_get_optimized_ts( pargs );
		/* Convert time into msecs */
		time_conv_to_ms( pargs->ts_val );
      }
	  break;
      case TIME_SECS:
      {
        /* Get time in timestamp format */
        time_genoff_client_get_optimized_ts( pargs );

        /* Convert time into secs */
        lsecs = time_conv_to_sec( pargs->ts_val );

        /* Set the lower 32 bits as the value in seconds */
        qw_set(pargs->ts_val, 0, lsecs);
      }
	  break;
      case TIME_JULIAN:
      {
        /* Get time in secs */
        temp_genoff_args.base                   = pargs->base;
        temp_genoff_args.base_source            = pargs->base_source;
        temp_genoff_args.ts_val                 = &ts_val_secs;
        temp_genoff_args.unit                   = TIME_SECS;
        if( TIME_GENOFF_OPR_EXTERNAL_MASK & pargs->operation )
        {
          temp_genoff_args.operation   = T_GET_EXT;
          temp_genoff_args.ext_genoff_ptr = pargs->ext_genoff_ptr;
        }
        else
        {
          temp_genoff_args.operation   = T_GET;
        }
        time_genoff_opr( &temp_genoff_args );

        lsecs = ts_val_secs[0];

        /* Convert time into julian */
        time_jul_from_secs( lsecs, pargs->ts_val);
      }
	  break;
      case TIME_20MS_FRAME:
      {
        /* Get time in timestamp format */
		time_genoff_client_get_optimized_ts( pargs );

		/* Convert time into 20ms frame time */
        time_conv_to_20ms_frame_time( pargs->ts_val );
      }
	  break;
      default:
        ATS_ERR_1("Invalid time unit %d", pargs->unit);
        break;
    }
	
  }
  else if ( pargs->operation & TIME_GENOFF_OPR_SET_MASK ) /* SET operation */
  {

    /* Invoke QDI call to perform the set operation */
    time_genoff_invoke_opr(time_genoff_client_qdi_handle, pargs);
	
  }
  else if ( pargs->operation & TIME_GENOFF_OPR_INIT_MASK ) /* External genoff init */
  {
  
    /* memset the external genoff structure before initialization */
    memset( pargs->ext_genoff_ptr, 0, sizeof(time_genoff_struct_type));
    time_genoff_invoke_opr(time_genoff_client_qdi_handle, pargs);
	
  }/* End init operation */

} /* time_genoff_opr */


/*=============================================================================

FUNCTION TIME_GENOFF_SYNC_WITH_TIME_TOD

DESCRIPTION
  This function syncs is called from time_tod_set. The purpose of the 
  function is to sync the time maintained by time_tod module and the genoff
  module.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

=============================================================================*/
void time_genoff_sync_with_tod
(
  time_type ts_val
)
{
	//STUBBED
}


/*=============================================================================

FUNCTION TIME_GENOFF_SET_REMOTE_MODEM_UPDATE

DESCRIPTION
  This function sets the static variable which is used to indicate whether
  the remote modem should be updated or not

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

=============================================================================*/
void time_genoff_set_remote_modem_update
(
  boolean update_remote_modem
)
{
  /* To supress compiler warning */
  update_remote_modem = TRUE;

 
  /* Return, since for remote PD's this is not supported */
  return;
}

/*=============================================================================

FUNCTION TIME_GENOFF_GET_REMOTE_MODEM_UPDATE

DESCRIPTION
  This function gets the static variable which is used to indicate whether
  the remote modem should be updated or not

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

=============================================================================*/
boolean time_genoff_get_remote_modem_update
(
  void
)
{
  /* STUBBED */
  return FALSE;
}


#ifdef FEATURE_QMI_TIME_REMOTE_CLNT
/*=============================================================================

FUNCTION TIME_GENOFF_GET_SYS_TIME_IN_SEC_WITH_LP_SEC

DESCRIPTION
  This function sets gets the system time alongwith leap second info

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

=============================================================================*/
uint32 time_genoff_get_sys_time_in_sec_with_lp_sec
(
  void
)
{
   /* STUBBED */
   return (uint32)0;
}
#endif /*FEATURE_QMI_TIME_REMOTE_CLNT*/

/*=============================================================================

FUNCTION TIME_GENOFF_REPORT_OUT_OF_COVERAGE_TIME

DESCRIPTION
  This function records when a radio technology goes out of coverage

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

=============================================================================*/
void time_genoff_report_out_of_coverage
(
  time_bases_type base
)
{
  if(base < ATS_MAX) 
  {
    time_genoff_invoke_report_out_of_coverage(time_genoff_client_qdi_handle, base);
  }  
}

/*==============================================================================

                     CLIENT WORKER THREAD & INITIALIZATION ROUTINES

=============================================================================*/


/*===========================================================================
FUNCTION    TIME_GENOFF_CLIENT_WORKER_FUNCTION

DESCRIPTION
  Function executed after thread is created in the client PD.
  Sets the offsets for clients of time-services.
  
DEPENDENCIES
  None.

RETURN VALUE
  None.
  
SIDE EFFECTS
  None.
===========================================================================*/

void time_genoff_client_worker_function(void *parameter)
{
   int result;
   time_genoff_qdi_update_container      update_params;
   time_genoff_client_ptr                ptime_genoff_client;

   /* Indicate to Guest OS that we are ready to receive time updates */
   time_genoff_invoke_get_init_updates(time_genoff_client_qdi_handle);
   
   while(1)
   {
     /* memset the structure */
	 memset(&update_params, 0, sizeof(time_genoff_qdi_update_container));

     /* Make qdi call to get the genoff update data from pipe */
     result = time_genoff_invoke_get_update_info(time_genoff_client_qdi_handle, &update_params);

     /* If there was a cancel signal or copy fail, try again */
     if( result == TIME_ERR_CANCEL_WAIT ||
         result == TIME_ERR_QDI_CPY_FAIL )	 
     {
       //MSG_ERROR("time_genoff qdi server returned %d result", result, 0, 0);
       continue;
     }

     /* Update the client side time genoff details */
	 if ( update_params.base < ATS_MAX )
	 	{
	 	  ptime_genoff_client = &ats_client_bases[update_params.base];
		  memscpy(ptime_genoff_client,
   	         sizeof(time_genoff_client_struct_type),
   	         &update_params.time_genoff_client,
   	         sizeof(time_genoff_client_struct_type));
	 	}
   }
} /* time_genoff_client_worker_function */



/*===========================================================================
FUNCTION    TIMEGENOFFCREATETHREAD

DESCRIPTION
  Creates worker thread that waits inside Guest OS for time setting details.
  
DEPENDENCIES
  None.

RETURN VALUE
  Error value from the thread creation function.
  
SIDE EFFECTS
  None.
===========================================================================*/

static int TimeGenoffCreateThread(unsigned int process_idx)
{
   int ret_value = 0;
   qurt_thread_attr_t tattr;
   unsigned int stackbase;
   char thread_name[20];
   RCINIT_INFO info_handle = NULL;
   RCINIT_PRIO prio = 0;
   unsigned long stksz = 0;

   /* Init the thread name based on the process index */  
   snprintf(thread_name, sizeof(thread_name), "TIME_CLIENT_%u", process_idx);

   /* Look up thread info: 
      rcinit_task_prio.csv must be updated as new clients are added */
   info_handle = rcinit_lookup(thread_name);
	   
   if (!info_handle) 
   {
      ERR_FATAL("TIME_CLIENT_%u task info not found",process_idx , 0, 0);
   }
   else
   {
     prio = rcinit_lookup_prio_info(info_handle);
     stksz = rcinit_lookup_stksz_info(info_handle);
     if ((prio > 255) || (stksz == 0)) 
     {
       ERR_FATAL("Invalid Priority:%d or Stack Size: %d",prio, stksz, 0);
     }
   
   /* Alocate memory for stack */
     stackbase = (unsigned int)qurt_malloc(stksz);

   /* Set the thread attributes */
   qurt_thread_attr_init (&tattr);
   qurt_thread_attr_set_name(&tattr, thread_name);   
   qurt_thread_attr_set_priority (&tattr, 255-prio);
   qurt_thread_attr_set_stack_size (&tattr, (stksz - 8));
   qurt_thread_attr_set_stack_addr (&tattr, (void*)((stackbase + 7) & (~7)) );

   /* Create the thread */
   ret_value =  qurt_thread_create(&time_genoff_worker_thread_id,
                                   &tattr, 
                                   time_genoff_client_worker_function, 
                                   NULL);
   }
  
   return ret_value;
}  /* TimeGenoffCreateThread() */



/*===========================================================================
FUNCTION    TIME_GENOFF_CLIENT_INIT

DESCRIPTION
  Initialize the Time Genoff Client service.

DEPENDENCIES
  None.

RETURN VALUE
  None.
  
SIDE EFFECTS
  None.
===========================================================================*/

void time_genoff_client_init(void) 
{
   /* pid of the client process */
   unsigned int time_genoff_client_pid;
   time_genoff_client_pid = qurt_getpid();

   /* Invoke QDI call to get the handle for the time driver */
   time_genoff_client_qdi_handle = qurt_qdi_open(TIME_GENOFF_DRIVER_NAME);

   if(time_genoff_client_qdi_handle < 0)
   {
      printf("time_genoff_client_init: qdi_open failed");
      ERR_FATAL("time_genoff_client_init :qdi_open failed\n", 0, 0, 0);
      return;
   }

   /* Create the worker thread, unique to each client connecting with the time driver */
   if(TimeGenoffCreateThread(time_genoff_client_pid) != 0)
    {
       printf("time_genoff_client_init : TimeGenoffCreateThread failed\n");
       ERR_FATAL("time_genoff_client_init : TimeGenoffCreateThread failed\n", 0, 0, 0);
       return;
    }

   /* Initialize for timetick_sclk64 */
   timetick_sclk64_init();

   /* Mark the client as initialized */
   ptime_genoff_client_initialized = TRUE;

   return;
}  /* time_genoff_client_init */


