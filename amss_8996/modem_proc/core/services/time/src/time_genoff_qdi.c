/*=============================================================================

                 time_genoff_qdi.c

GENERAL DESCRIPTION
   Implements QDI layer for Time services
   
INITIALIZATION AND SEQUENCING REQUIREMENTS
   None.

      Copyright (c) 2010 - 2014
   by QUALCOMM Technologies Incorporated.  All Rights Reserved.

=============================================================================*/


/*=============================================================================

                        EDIT HISTORY FOR MODULE

 This section contains comments describing changes made to the module.
 Notice that changes are listed in reverse chronological order.
 
$Header: //components/rel/core.mpss/3.4.c3.11/services/time/src/time_genoff_qdi.c#1 $ 
$DateTime: 2016/03/28 23:02:17 $ $Author: mplcsds1 $

when         who     what, where, why
--------   ---     ------------------------------------------------------------
05/01/14   abh     File Created

=============================================================================*/


/*****************************************************************************/
/*                           INCLUDE FILES                                   */
/*****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <stringl/stringl.h>
#include "qurt.h"
#include "qurt_qdi.h"
#include "qurt_qdi_driver.h"
#include "qurt_pimutex.h"
#include "time_genoff_qdi_v.h"
#include "msg.h"
#include "err.h"

/*===========================================================================
Data Declarations
===========================================================================*/

/* Num of elements that the pipe will be able to queue */
#define TIME_GENOFF_CLIENT_PIPE_SIZE     (2 * ATS_MAX)

/* Num of max user PD clients to time_genoff */
#define TIME_GENOFF_MAX_CLIENTS          4

/* Pointers to the qdi_structure for user PD client */
static time_genoff_qdi_opener *time_genoff_clients[TIME_GENOFF_MAX_CLIENTS];

/* Counter for the number of user PD clients */
uint32 time_genoff_clients_cntr = 0;

time_osal_mutex_t   time_genoff_qdi_mutex;

/*===========================================================================
Declarations
===========================================================================*/
static int time_genoff_qdi_invoke(int client_handle,
                          qurt_qdi_obj_t *pobj,
                          int method,
                          qurt_qdi_arg_t a1,
                          qurt_qdi_arg_t a2,
                          qurt_qdi_arg_t a3,
                          qurt_qdi_arg_t a4,
                          qurt_qdi_arg_t a5,
                          qurt_qdi_arg_t a6,
                          qurt_qdi_arg_t a7,
                          qurt_qdi_arg_t a8,
                          qurt_qdi_arg_t a9);
                          

/*===========================================================================
Pipe Apis
===========================================================================*/
int time_genoff_qdi_create_pipe(time_genoff_qdi_opener *clntobj)
{
   qurt_pipe_attr_t pipe_attr;
   int ret;
   
   //Create Pipe for the pid
   qurt_pipe_attr_init(&pipe_attr);
   qurt_pipe_attr_set_elements(&pipe_attr, TIME_GENOFF_CLIENT_PIPE_SIZE);
   ret = qurt_pipe_create(&clntobj->pipe, &pipe_attr);

   return ret;
} /* time_genoff_qdi_create_pipe */

/*===========================================================================
Function Definitions
===========================================================================*/


/*=============================================================================

FUNCTION TIME_GENOFF_UPDATE_TO_PIPE

DESCRIPTION
 Updates the passed structure pointer to the pipe for respective client PD object.

DEPENDENCIES
  None.

RETURN VALUE
 None.

SIDE EFFECTS
 None.

=============================================================================*/
void time_genoff_update_to_pipe(time_genoff_ptr ptime_genoff, int client_index)
{
   int index = 0;

   qurt_pipe_data_t data = (qurt_pipe_data_t)0;

   data = (uint32)ptime_genoff;

   if (client_index == -1)
    {
     for (index = 0 ; index<time_genoff_clients_cntr ; index++)
      {
        qurt_pipe_send(time_genoff_clients[index]->pipe,data);
      }
    }
   else
   	{
      qurt_pipe_send(time_genoff_clients[client_index]->pipe,data);
   	}
   return;
} /* time_genoff_qdi_report_out_of_coverage */


/*=============================================================================

FUNCTION TIME_GENOFF_QDI_REPORT_OUT_OF_COVERAGE

DESCRIPTION
 For reporting out of coverage.

DEPENDENCIES
  None

RETURN VALUE
 None

SIDE EFFECTS
 None

=============================================================================*/
static inline int time_genoff_qdi_report_out_of_coverage(time_bases_type base)
{
   time_genoff_report_out_of_coverage(base);
   return 0;
} /* time_genoff_qdi_report_out_of_coverage */


/*=============================================================================

FUNCTION TIME_GENOFF_QDI_GET_UPDATE_INFO

DESCRIPTION
  Function invoked by client PD requesting the updates to time offsets.
 
 
DEPENDENCIES
  None

RETURN VALUE
  Error value chosen as per the operation.

SIDE EFFECTS
  None

=============================================================================*/
static inline int time_genoff_qdi_get_update_info( int client_handle, 
                                            time_genoff_qdi_opener * client_obj,
                                            time_genoff_qdi_update_container * remote_pd_ptr )
{
  /* return value from copy_to_user calls */
  int                            result;

  /* Container for the pipe_receive */
  qurt_pipe_data_t               data;
  uint32                         data_val;

  /* Pointer to the genoff base for which the update needs to be passed */
  time_genoff_ptr                ptime_genoff;

  /* Temp container for the client genoff update values */
  time_genoff_qdi_update_container temp_update_container = { 0 };
      
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  
   /* Receive the pointer for time base updated information from the GUEST PD */
   if( qurt_pipe_receive_cancellable(client_obj->pipe, &data) == QURT_ECANCEL )
   {
      return TIME_ERR_CANCEL_WAIT;
   }   

   /* Pointer to the genoff from which update needs to be copied */
   data_val = (uint32)data;    // Suppressing compiler warning
   ptime_genoff = (time_genoff_ptr)(data_val);

   /* Pipe data check */
   if (ptime_genoff == NULL)
   	  return -1;
  
   /* Populate the temp update container structure */
   temp_update_container.base =    ptime_genoff->bases_type;   
   temp_update_container.time_genoff_client.generic_offset = 
                                   ptime_genoff->generic_offset;
   temp_update_container.time_genoff_client.sum_generic_offset = 
   	                               ptime_genoff->sum_generic_offset;
   temp_update_container.time_genoff_client.sum_generic_offset_ts_is_negative = 
   	                               ptime_genoff->sum_generic_offset_ts_is_negative;
   qw_equ(temp_update_container.time_genoff_client.sum_generic_offset_ts,
   	                               ptime_genoff->sum_generic_offset_ts);

   /* Copy values to the remote PD */
   result = qurt_qdi_copy_to_user(client_handle, 
                                  remote_pd_ptr, 
                                  &temp_update_container, 
                                  sizeof(time_genoff_qdi_update_container));

   if(result >= 0)
   {
      return TIME_ERR_NONE;
   }
   else
   {
      return TIME_ERR_QDI_CPY_FAIL;
   }								  

} /* time_genoff_qdi_get_update_info */

/*=============================================================================

                   TIME GENOFF API QDI Functions

=============================================================================*/
static inline int time_genoff_qdi_opr(int                  client_handle,
	                                     time_genoff_args_ptr  pargs)
{
   int result;
   time_genoff_args_struct_type temp_args;
   time_genoff_ptr              local_ext_ptime_genoff = NULL;
   time_genoff_ptr              client_ext_genoff_ptr = NULL;

   /* Copy args structure locally to pass to time_genoff_opr */  
   result = qurt_qdi_copy_from_user(client_handle, 
   	                                &temp_args,
   	                                pargs,
   	                                sizeof(time_genoff_args_struct_type));
   if(result)
     return result;

   /* Client PD address for the external time genoff pointer */
   client_ext_genoff_ptr = temp_args.ext_genoff_ptr;

   if (temp_args.operation == TIME_GENOFF_OPR_EXTERNAL_MASK || 
   	   temp_args.operation == TIME_GENOFF_OPR_INIT_MASK )
   	{
      /* Malloc the structure, later to be copied to remote PD & then freed */
      local_ext_ptime_genoff = malloc(sizeof(time_genoff_struct_type));

      /* Check malloc failure */
      if (local_ext_ptime_genoff == NULL)
      	{
      	  MSG_ERROR("time_genoff_qdi_opr_ext_init: malloc call failed",0,0,0);
    	  return -1;
     	}
	  
	  /* Copy the values from UserPD based external time genoff structure to local buffer */
	  result = qurt_qdi_copy_from_user(client_handle, 
									   &local_ext_ptime_genoff,
									   client_ext_genoff_ptr,
									   sizeof(time_genoff_struct_type));
	  if(result)
	  	{
	  	  free(local_ext_ptime_genoff);
	  	  return result;
	  	}

      temp_args.ext_genoff_ptr = local_ext_ptime_genoff;
   	}

  time_genoff_opr(&temp_args);


  if (temp_args.operation == TIME_GENOFF_OPR_EXTERNAL_MASK || 
	  temp_args.operation == TIME_GENOFF_OPR_INIT_MASK )

  {
    /* Copy back the updated ext genoff structure in UserPD */
    result = qurt_qdi_copy_to_user(client_handle, 
                                   client_ext_genoff_ptr, 
                                   local_ext_ptime_genoff, 
                                   sizeof(time_genoff_struct_type));

    free(local_ext_ptime_genoff);
  }
 	   
   return result;
} /* time_genoff_qdi_opr */


/*=============================================================================

FUNCTION TIME_GENOFF_QDI_GET_INIT_UPDATES

DESCRIPTION
  This will be called by the client PD when it's initialization finishes. 
  Once called, this function will pass the update information from Guest OS's ats_bases to the inited client PD. 
 
DEPENDENCIES
  None

RETURN VALUE
  Zero.

SIDE EFFECTS
  None

=============================================================================*/

static inline int time_genoff_qdi_get_init_updates( int client_index )
{
   time_genoff_get_init_updates(client_index);
   return 0;
}

/*=============================================================================

FUNCTION TIME_GENOFF_QDI_RELEASE

DESCRIPTION
 Releases the Qdi object that corresponding to a process that got destroyed

DEPENDENCIES
  None

RETURN VALUE
 None

SIDE EFFECTS
  None

=============================================================================*/
void time_genoff_qdi_release(qurt_qdi_obj_t *obj)
{
   time_genoff_qdi_opener *clntobj = (time_genoff_qdi_opener *)obj;
   uint32 process_idx = clntobj->process_idx;
   
   /* Note: Remove INTLOCK and use LOCK */
   TIME_OSAL_MUTEX_LOCK(&time_genoff_qdi_mutex);
   if(clntobj->pipe)
   {
      qurt_pipe_delete(clntobj->pipe);
   }

   /* Free the object related to this process */
   free(time_genoff_clients[process_idx]);
   time_genoff_clients[process_idx] = NULL;

   time_genoff_clients_cntr--;
   
   TIME_OSAL_MUTEX_UNLOCK(&time_genoff_qdi_mutex);
   return;
} /* time_genoff_qdi_release */


/*=============================================================================

FUNCTION TIME_GENOFF_QDI_OPEN

DESCRIPTION
 Creates time_genoff driver object for the client and initializes required details

DEPENDENCIES
  None

RETURN VALUE
 QURT_EOK if the time_genoff is defined and set correctly.

SIDE EFFECTS
  None

=============================================================================*/
int time_genoff_qdi_open(int client_handle, time_genoff_qdi_opener *me)
{
   time_genoff_qdi_opener *clntobj;
   int index;
   int status;
   
   TIME_OSAL_MUTEX_LOCK(&time_genoff_qdi_mutex);

   /* Get a free pd slot */
   for(index=0; index<TIME_GENOFF_MAX_CLIENTS && time_genoff_clients[index] != NULL; index++);
   
   if(index == TIME_GENOFF_MAX_CLIENTS)
   {
      TIME_OSAL_MUTEX_UNLOCK(&time_genoff_qdi_mutex);
      ERR_FATAL("Max Processes %d reached", time_genoff_clients_cntr, 0, 0);
      return -1;
   }
   clntobj = malloc(sizeof(time_genoff_qdi_opener));
   
   if(NULL == clntobj)
   {
      TIME_OSAL_MUTEX_UNLOCK(&time_genoff_qdi_mutex);
      printf("malloc err for %d bytes", sizeof(time_genoff_qdi_opener));
      return -1;
   }
   
   clntobj->qdiobj.invoke = time_genoff_qdi_invoke;
   clntobj->qdiobj.refcnt = QDI_REFCNT_INIT;
   clntobj->qdiobj.release = time_genoff_qdi_release;
   
   /* Create Pipe */
   status = time_genoff_qdi_create_pipe(clntobj);
   if(status == QURT_EFAILED)
     {
	   TIME_OSAL_MUTEX_UNLOCK(&time_genoff_qdi_mutex);
       free(clntobj);
       return -1;
     }
   
   time_genoff_clients[index] = clntobj;
   clntobj->process_idx = index;
   time_genoff_clients_cntr++;
   TIME_OSAL_MUTEX_UNLOCK(&time_genoff_qdi_mutex);

   return qurt_qdi_handle_create_from_obj_t(client_handle, &clntobj->qdiobj);
} /* time_genoff_qdi_open */


/*=============================================================================

FUNCTION TIME_GENOFF_QDI_INVOKE

DESCRIPTION
 This function contains the switch which maps to all methods

DEPENDENCIES
  None

RETURN VALUE
  Error codes from the respective functions.

SIDE EFFECTS
  None

=============================================================================*/
static int time_genoff_qdi_invoke(int client_handle,
                          qurt_qdi_obj_t *pobj,
                          int method,
                          qurt_qdi_arg_t a1,
                          qurt_qdi_arg_t a2,
                          qurt_qdi_arg_t a3,
                          qurt_qdi_arg_t a4,
                          qurt_qdi_arg_t a5,
                          qurt_qdi_arg_t a6,
                          qurt_qdi_arg_t a7,
                          qurt_qdi_arg_t a8,
                          qurt_qdi_arg_t a9)
{
   time_genoff_qdi_opener *clntobj = (time_genoff_qdi_opener *)pobj;
   unsigned int      process_idx = clntobj->process_idx;
   
   switch(method)
   {
      case QDI_OPEN: 
         return time_genoff_qdi_open(client_handle, clntobj);
      
      case TIME_GENOFF_QDI_OPR: 
         return time_genoff_qdi_opr(client_handle, a1.ptr);

      case TIME_GENOFF_QDI_GET_UPDATE_INFO: 
         return time_genoff_qdi_get_update_info(client_handle, clntobj, a1.ptr);

      case TIME_GENOFF_QDI_REPORT_OUT_OF_COVERAGE:
         return time_genoff_qdi_report_out_of_coverage(a1.num);	   

	  case TIME_GENOFF_QDI_GET_INIT_UPDATES:
         return time_genoff_qdi_get_init_updates(process_idx);	  	

      default:
         return qurt_qdi_method_default(client_handle, pobj, method,
                                     a1, a2, a3, a4, a5, a6, a7, a8, a9);
   }
} /* time_genoff_qdi_invoke */


/*=============================================================================

FUNCTION TIME_GENOFF_QDI_INIT

DESCRIPTION
 Registers with QDI Framework for Time
 
DEPENDENCIES
  None

RETURN VALUE
 None

SIDE EFFECTS
  None

=============================================================================*/
void time_genoff_qdi_init 
(
  void
) 
{
  time_genoff_qdi_opener *p_opener;
  int i;

  /* initialize the mutex here */
  TIME_OSAL_MUTEX_INIT(&time_genoff_qdi_mutex);

  /* Initialize the time genoff processes */
  for(i=0; i<TIME_GENOFF_MAX_CLIENTS; i++)
     time_genoff_clients[i] = NULL;  

   p_opener = (time_genoff_qdi_opener *)malloc(sizeof(time_genoff_qdi_opener));
   if(NULL == p_opener)
   {
      printf("malloc err for %d bytes", sizeof(time_genoff_qdi_opener));
      return;
   }
   
   p_opener->qdiobj.invoke = time_genoff_qdi_invoke;
   p_opener->qdiobj.refcnt = QDI_REFCNT_INIT;
   p_opener->qdiobj.release = time_genoff_qdi_release;
   qurt_qdi_register_devname(TIME_GENOFF_DRIVER_NAME, p_opener);

   /* Register the pipe update function */
   time_genoff_register_client_update_func(time_genoff_update_to_pipe);

} /* time_genoff_qdi_init */
