#ifndef SMLDIAG_H
#define SMLDIAG_H

#include "customer.h"
#include "comdef.h"

#include "diagcmd.h"
#include "diagpkt.h"
#include "diagi.h"
#include "log_codes.h"
#include "nv.h"

#define DIAG_SIMLOCK_EXT_SIZE                   1024
#define DIAG_SIMLOCK_FILE_NAME_SIZE             120
#define SML_MAX_FILENAME_SIZE                   50
#define SML_FILELIST_SIZE                       44

#define DIAG_SUBSYS_SIMLOCK_EXT_SFS_BACKUP_F      0
#define DIAG_SUBSYS_SIMLOCK_EXT_SFS_RESTORE_F     1
#define DIAG_SUBSYS_SIMLOCK_EXT                 103

#define DEFAULT_FILE_PATH "/safe/sfs/uim/simlock/config"

typedef enum {
  SIMLOCK_SUCCESS,
  SIMLOCK_GENERIC_ERROR,
  SIMLOCK_INCORRECT_KEY,
  SIMLOCK_INCORRECT_PARAMS,
  SIMLOCK_MEMORY_ERROR_HEAP_EXHAUSTED,
  SIMLOCK_MAX_RETRIES_REACHED,
  SIMLOCK_ISIMLOCKALID_STATE,
  SIMLOCK_UNSUPPORTED,
  SIMLOCK_ISIMLOCKALID_LOCK_DATA,
  SIMLOCK_CONFLICTING_LOCK_DATA
} smldiag_result_enum_type;

typedef struct file
{
	char name[SML_MAX_FILENAME_SIZE];
	uint8_t  item_data[128];
	uint16_t item_size;
}File;

typedef struct nv_files
{
	uint16_t fileNum;
	File files[SML_FILELIST_SIZE];
}NV_FILES;

typedef struct
{
	diagpkt_subsys_header_type header;           /* Sub System header */
	uint16_t item;                             /* NV item */
	uint8_t  file[DIAG_SIMLOCK_FILE_NAME_SIZE];/*NV file name*/
	NV_FILES Files;
}DIAG_SIMLOCK_type;

typedef DIAG_SIMLOCK_type  DIAG_SIMLOCK_EXT_SFS_BACKUP_F_req_type;
typedef DIAG_SIMLOCK_type  DIAG_SIMLOCK_EXT_SFS_BACKUP_F_rsp_type;

#endif
