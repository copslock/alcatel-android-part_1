/*==============================================================================

FILE:      qdi_client_log.h

DESCRIPTION: ADSPPM MPD client logging utils API

This is the internal logging utils API for ADSPPM QDI client. This is not distributed to
any clients that uses ADSPPM. 

INITIALIZATION AND SEQUENCING REQUIREMENTS:  N/A

* Copyright (c) 2014 Qualcomm Technologies, Inc.
* All Rights Reserved.
* Qualcomm Technologies, Inc. Confidential and Proprietary.

==============================================================================*/

#ifndef QDI_CLIENT_LOG_H
#define QDI_CLIENT_LOG_H

#include "msg.h"

#define ADSPPM_QDI_CLIENT_LOG 1

//Turn on/off DIAG messages
#if (ADSPPM_QDI_CLIENT_LOG==1)
#define ADSPPM_QDI_DBG_MSG_0(prio, message)  MSG(MSG_SSID_QDSP6, prio, message )
#define ADSPPM_QDI_DBG_MSG_1(prio, message, arg_1)  MSG_1(MSG_SSID_QDSP6, prio , message, arg_1)
#define ADSPPM_QDI_DBG_MSG_2(prio, message, arg_1, arg_2)  MSG_2(MSG_SSID_QDSP6, prio, message, arg_1, arg_2)
#define ADSPPM_QDI_DBG_MSG_3(prio, message, arg_1, arg_2,arg_3)  MSG_3(MSG_SSID_QDSP6, prio , message, arg_1 ,arg_2, arg_3)
#define ADSPPM_QDI_DBG_MSG_4(prio, message, arg_1, arg_2,arg_3,arg_4)  MSG_4(MSG_SSID_QDSP6, prio , message, arg_1 ,arg_2, arg_3,arg_4)
#define ADSPPM_QDI_DBG_MSG_5(prio, message, arg_1, arg_2,arg_3,arg_4,arg_5)  MSG_5(MSG_SSID_QDSP6, prio , message, arg_1 ,arg_2, arg_3,arg_4, arg_5)
#else
#define ADSPPM_QDI_DBG_MSG(prio, message)
#define ADSPPM_QDI_DBG_MSG_1(prio, message, arg_1)
#define ADSPPM_QDI_DBG_MSG_2(prio, message, arg_1, arg_2)
#define ADSPPM_QDI_DBG_MSG_3(prio, message, arg_1, arg_2,arg_3)
#define ADSPPM_QDI_DBG_MSG_4(prio, message, arg_1, arg_2,arg_3,arg_4)
#define ADSPPM_QDI_DBG_MSG_5(prio, message, arg_1, arg_2,arg_3,arg_4,arg_5)
#endif

#endif


