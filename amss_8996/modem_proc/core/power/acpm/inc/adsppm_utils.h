/*==============================================================================

FILE:      adsppm_utils.h

DESCRIPTION: ADSPPM internal utils API

This is the internal utils API for ADSPPM. This is not distributed to any clients that uses
ADSPPM. 

INITIALIZATION AND SEQUENCING REQUIREMENTS:  N/A


* Copyright (c) 2013 Qualcomm Technologies, Inc.
* All Rights Reserved.
* Qualcomm Technologies, Inc. Confidential and Proprietary.

==============================================================================

                           EDIT HISTORY FOR MODULE

$Header: //components/rel/core.mpss/3.4.c3.11/power/acpm/inc/adsppm_utils.h#1 $

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

when       who     what, where, why
--------   ---     ---------------------------------------------------------
07/20/11   CK      Initial version
==============================================================================*/

#ifndef ADSPPM_UTILS_H
#define ADSPPM_UTILS_H

#include "DALSys.h"
#include "adsppm.h"
#include "core_internal.h"

extern __inline void adsppm_lock( DALSYSSyncHandle lock );
extern __inline void adsppm_unlock( DALSYSSyncHandle lock );

#define ADSPPM_ARRAY_SIZE( array ) (sizeof(array)/sizeof(array[0]))
#define ADSPPM_ARRAY( array ) ADSPPM_ARRAY_SIZE(array), &array[0]

#define DIVIDE_ROUND_UP(val, base) ((val+base-1)/base)

#if (ADSPPM_NO_MUTEX==1)
#define ADSPPM_LOCK(hLock)
#define ADSPPM_UNLOCK(hLock)
#else
#define ADSPPM_LOCK(hLock) adsppm_lock(hLock)
#define ADSPPM_UNLOCK(hLock) adsppm_unlock(hLock)
#endif

#define ADSPPM_LOCK_RESOURCE(resourceId) adsppm_lock(GetGlobalContext()->RMCtxLock[resourceId-1])
#define ADSPPM_UNLOCK_RESOURCE(resourceId) adsppm_unlock(GetGlobalContext()->RMCtxLock[resourceId-1])

#endif

