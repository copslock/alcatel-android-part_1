#ifndef _ACPM_TRACER_EVENT_IDS_H
#define _ACPM_TRACER_EVENT_IDS_H

// File autogenerated by swe_builder.

#ifndef EVENT_TABLE_TAG
#define EVENT_TABLE_TAG "QDSS_TAG_0000D57175A8"
#define EVENT_PROCESSOR "qdsp6_sw"
#define EVENT_SUBSYSTEM MODEM
#endif

enum tracer_event_id_enum {
	TRACER_EVENT_RESERVE_0=0,
	ADSPPM_CLK_SET,
	ADSPPM_CLK_ENABLE,
	ADSPPM_CLK_DISABLE,
	ADSPPM_PWR_REQ,
	ADSPPM_PWR_REL,
	ADSPPM_BW_REQ,
	ADSPPM_BW_REL,
	ADSPPM_DCG_ENABLE,
	ADSPPM_DCG_DISABLE,
	ADSPPM_LPASS_CORE_PWR,
	ADSPPM_XPU_PROG,
	ADSPPM_MEM_PWR,
	ADSPPM_API_PWR_REQ,
	ADSPPM_API_PWR_REL,
	ADSPPM_API_MIPS_REQ,
	ADSPPM_API_MIPS_REL,
	ADSPPM_API_REG_REQ,
	ADSPPM_API_REG_REL,
	ADSPPM_API_BW_REQ,
	ADSPPM_API_BW_REL,
	ADSPPM_API_LATENCY_REQ,
	ADSPPM_API_LATENCY_REL,
	ADSPPM_API_MEM_PWR_REQ,
	ADSPPM_API_MEM_PWR_REL,
	CLOCK_EVENT_INIT,
	CLOCK_EVENT_CLOCK_STATUS,
	CLOCK_EVENT_CLOCK_FREQ,
	CLOCK_EVENT_SOURCE_STATUS,
	CLOCK_EVENT_SOURCE_FREQ,
	CLOCK_EVENT_CX_VOLTAGE,
	CLOCK_EVENT_MSS_VOLTAGE,
	CLOCK_EVENT_XO,
	CLOCK_EVENT_LDO,
	CLOCK_EVENT_LDO_VOLTAGE,
	DCVS_CPU_FREQUENCY_UPDATE,
	DCVS_CPU_UTILIZATION_UPDATE,
	PMIC_EVENT_FIRST,
	PMIC_MPSS_EVENT_INIT,
	PMIC_MPSS_PAM_INIT,
	PMIC_RESOURCE_VDD_UVOL_INIT,
	PMIC_LDO_AGG1,
	PMIC_LDO_AGG2,
	PMIC_LDO_AGG3,
	PMIC_LDO_AGG4,
	PMIC_LDO_NO_CHANGE,
	PMIC_SET_VDD_MEM_UVOL,
	PMIC_CLK_BUF_AGG1,
	PMIC_CLK_BUF_AGG2,
	PMIC_PROCESS_RESOURCE,
	PMIC_EVENT_LAST,
	RCECB_SWE_CREATE,
	RCECB_SWE_REGISTER,
	RCECB_SWE_SIGNAL,
	RCECB_SWE_UNREGISTER,
	RCEVT_SWE_CREATE,
	RCEVT_SWE_REGISTER,
	RCEVT_SWE_SIGNAL,
	RCEVT_SWE_UNREGISTER,
	RCINIT_SWE_INIT_TIMEO,
	RCINIT_SWE_INIT_BAIL,
	RCINIT_SWE_TERM_TIMEO,
	RCINIT_SWE_TERM_BAIL,
	RCINIT_SWE_INIT_GROUP_0,
	RCINIT_SWE_INIT_GROUP_1,
	RCINIT_SWE_INIT_GROUP_2,
	RCINIT_SWE_INIT_GROUP_3,
	RCINIT_SWE_INIT_GROUP_4,
	RCINIT_SWE_INIT_GROUP_5,
	RCINIT_SWE_INIT_GROUP_6,
	RCINIT_SWE_INIT_GROUP_7,
	RCINIT_SWE_INITGROUPS,
	RCINIT_SWE_TERMGROUPS,
	RCINIT_SWE_TERM_GROUP_7,
	RCINIT_SWE_TERM_GROUP_6,
	RCINIT_SWE_TERM_GROUP_5,
	RCINIT_SWE_TERM_GROUP_4,
	RCINIT_SWE_TERM_GROUP_3,
	RCINIT_SWE_TERM_GROUP_2,
	RCINIT_SWE_TERM_GROUP_1,
	RCINIT_SWE_TERM_GROUP_0,
	RCINIT_SWE_INIT_FUNC_R,
	RCINIT_SWE_INIT_FUNC_U,
	RCINIT_SWE_INIT_FUNC_RN,
	RCINIT_SWE_INIT_FUNC_XT,
	RCINIT_SWE_TERM_FUNC_R,
	RCINIT_SWE_TERM_FUNC_U,
	RCINIT_SWE_TERM_FUNC_RN,
	RCINIT_SWE_TERM_FUNC_XT,
	RCINIT_SWE_INIT_TASK_R,
	RCINIT_SWE_INIT_TASK_RI,
	RCINIT_SWE_INIT_TASK_U,
	RCINIT_SWE_INIT_TASK_UO,
	RCINIT_SWE_INIT_TASK,
	RCINIT_SWE_INIT_TASK_RN,
	RCINIT_SWE_INIT_TASK_RS,
	RCINIT_SWE_INIT_TASK_HS,
	RCINIT_SWE_INIT_TASK_HI,
	RCINIT_SWE_INIT_TASK_E,
	RCINIT_SWE_INIT_TASK_XT,
	RCINIT_SWE_TERM_TASK_R,
	RCINIT_SWE_TERM_TASK_RI,
	RCINIT_SWE_TERM_TASK_U,
	RCINIT_SWE_TERM_TASK_UO,
	RCINIT_SWE_TERM_TASK,
	RCINIT_SWE_TERM_TASK_RN,
	RCINIT_SWE_TERM_TASK_RS,
	RCINIT_SWE_TERM_TASK_HS,
	RCINIT_SWE_TERM_TASK_HI,
	RCINIT_SWE_TERM_TASK_E,
	RCINIT_SWE_TERM_TASK_XT,
	RCXH_SWE_TRY,
	RCXH_SWE_CATCH,
	RCXH_SWE_CAUGHT,
	RCXH_SWE_FINALLY,
	RCXH_SWE_ENDTRY,
	RCXH_SWE_THROW,
	RCXH_SWE_UNCAUGHT,
	SLEEP_ENTER_IDLE,
	SLEEP_EARLY_EXIT_STM,
	SLEEP_EXIT_IDLE,
	SLEEP_EXIT_STM,
	SLEEP_BKOFF_STATS,
	SLEEP_WAKEUP,
	SLEEP_ENTER_SOLVER,
	SLEEP_EXIT_SOLVER,
	SLEEP_MODE_ENABLED,
	SLEEP_MODE_DISABLED,
	SLEEP_ENTER_MODE,
	SLEEP_EXIT_MODE,
	SLEEP_SET_SEND,
	SYS_M_SWE_EVENT,
	TRACER_EVENT_INIT_COMPLETE,
	TRCEVT_TEST1,
	TRCEVT_TEST2_3ARGS,
	TRACER_EVENT_ID_MAX,
	TRACER_EVENT_RESERVE_LAST=0x00FFFFFF,
	TRACER_EVENT_ALL=0xFFFFFFFF,
};

#ifndef TRACER_EVENT_PREENABLE_SCONS
#define TRACER_EVENT_PREENABLE_SCONS {\
	0x0,\
	0xf8000000L,\
	0xffffffffL,\
	0x3fffff,\
	0x10\
}
#endif

#endif // _ACPM_TRACER_EVENT_IDS_H

