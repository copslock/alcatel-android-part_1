/*===========================================================================

  Copyright (c) 2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ===========================================================================*/

#include "cpr_enablement.h"
#include "cpr_device_hw_version.h"
#include "cpr_voltage_ranges.h"
#include "cpr_qfprom.h"
#include "cpr_logging.h"
#include "HALhwio.h"
#include "CoreVerify.h"


/* CPR floor voltages will be no lower than the design floor voltage of .520V 
   This is the minimum design voltage with margin added, as agree in the 7/15/15
   characterization discussions. */
#define CPR_DESIGN_FLOOR_VOLTAGE_8996   520000



#define CPR_CONFIG_EFUSE_MULTIPLIER 10000


/**
 * <!-- cpr_config_open_loop_voltage_8996 -->
 * 
 * Simmilar to normal open loop calculation but with CPR_OPENLOOP_VOLTAGE_ADJUSTMENT added.  
 * Also this function does not do floor/ceiling clipping or PMIC rounding. 
 */
static uint32 cpr_config_open_loop_voltage_8996(cpr_rail_id_t rail_id, cpr_voltage_mode_t voltage_mode)
{
    if (voltage_mode == CPR_VOLTAGE_MODE_OFF)
    {
        return 0;
    }

    //CPR_LOGGING(2, "cpr_config_open_loop_voltage_8996 rail %s voltage mode: %s", cpr_logging_get_ascii_rail_id_name(rail_id), cpr_logging_get_ascii_corner_name(voltage_mode));

    const cpr_config_versioned_voltage_ranges_t* voltage_ranges = cpr_config_find_versioned_voltage_ranges(rail_id);
    CORE_VERIFY_PTR(voltage_ranges);

    const cpr_config_voltage_level_t* voltage_level = cpr_config_find_voltage_level(voltage_ranges, voltage_mode);
    CORE_VERIFY_PTR(voltage_level);

    const int32 voltage_offset_steps = cpr_fuses_get_fuse_value(rail_id, voltage_level->fuse_type);
    const int32 voltage_offset = CPR_CONFIG_EFUSE_MULTIPLIER * voltage_offset_steps;

    uint32 open_loop_voltage = voltage_offset + voltage_level->voltage_fuse_ref + voltage_level->fuse_correction_factor;

    //clip to the ceiling if needed.
    if (open_loop_voltage >= voltage_level->voltage_ceiling){
        open_loop_voltage = voltage_level->voltage_ceiling;
    }

    //clip to the floor if needed.
    if (open_loop_voltage <= voltage_level->voltage_floor){
        open_loop_voltage = voltage_level->voltage_floor;
    }


    //CPR_LOGGING(4, "Calculated open loop voltage (rail: %s) (mode: %s) (offset_steps: %d) (voltage: %d)", cpr_logging_get_ascii_rail_id_name(rail_id), cpr_logging_get_ascii_corner_name(voltage_mode), voltage_offset_steps, open_loop_voltage);

    return open_loop_voltage;
}

/**
 *  <!-- custom_8996_voltage_fn -->
 *
 * 8996 introduces new open loop and floor voltage calculations.
 *
 * �Adjusted PMIC CPR Open Loop� = round up to the PMIC voltage( "CPR Open Loop Efuse Reference Voltage" + PMIC Fused Value (0 to -15)*10mV + "CPR Open Loop Voltage Adjustment" )
 * �CPR Fused Floor Voltage� = �Adjusted PMIC CPR Open Loop� � �Max Floor To Ceiling Range� 
 * �CPR Floor Voltage� = max(Design Floor, �CPR Fused Floor Voltage�)
 * 
 * �Max Floor To Ceiling Range�
 *    For V2, "Max Floor to Ceiling" will be a fixed value
 *	  For V3, "Max Floor to Ceiling" will be a variable based off efuse.
 *
 */
uint32 custom_8996_voltage_fn(cpr_enablement_custom_voltage_calculation_type_t voltage_calculation_type, cpr_voltage_mode_t voltage_mode){

    //CPR_LOGGING(0, "custom_8996_voltage_fn");
    // the open loop calculation is the same as earlier chips with the new open loop voltage adjustment added in.  
    // �Adjusted PMIC CPR Open Loop� = round up to the PMIC voltage("CPR Open Loop Efuse Reference Voltage" + PMIC Fused Value(0 to - 15) * 10mV + "CPR Open Loop Voltage Adjustment")
    if (voltage_calculation_type == CPR_OPEN_LOOP_VOLTAGE){
        uint32 open_loop_voltage;
        open_loop_voltage = cpr_config_open_loop_voltage_8996(CPR_RAIL_MSS, voltage_mode);

        //make sure the voltage is lower than the ceiling, and higher than the floor.
        open_loop_voltage = cpr_config_clip_voltage_to_floor_ceiling(CPR_RAIL_MSS, voltage_mode, open_loop_voltage);

        //Now round up to PMIC step boundary.
        open_loop_voltage = cpr_config_round_up_to_pmic_step(CPR_RAIL_MSS, open_loop_voltage);

        return open_loop_voltage;
    }

    //The new closed loop calculation 
    if (voltage_calculation_type == CPR_CLOSED_LOOP_FLOOR_VOLTAGE){
        uint32 open_loop_voltage;
        uint32 closed_loop_floor_voltage;
        
        //start with the open loop voltage
        open_loop_voltage = cpr_config_open_loop_voltage_8996(CPR_RAIL_MSS, voltage_mode); 

        closed_loop_floor_voltage = open_loop_voltage - cpr_config_max_floor_to_ceiling(CPR_RAIL_MSS, voltage_mode);

        //make sure the voltage is lower than the ceiling, and higher than the floor.
        closed_loop_floor_voltage = cpr_config_clip_voltage_to_floor_ceiling(CPR_RAIL_MSS, voltage_mode, closed_loop_floor_voltage);

        //Now round up to PMIC step boundary.
        closed_loop_floor_voltage = cpr_config_round_up_to_pmic_step(CPR_RAIL_MSS, closed_loop_floor_voltage);

        CPR_LOGGING(2, "Calculated fused floor (mode: %s) (voltage: %d)", cpr_logging_get_ascii_corner_name(voltage_mode), closed_loop_floor_voltage);
        return closed_loop_floor_voltage;
    }

    return 0;
}


/**
 *  <!-- mss_8996_static_margin_adjustment -->
 *
 *   A fuse setting will potentialy raise or lower the static margin on some parts.  New feature Sept 2015
 */
static int32 mss_8996_static_margin_adjustment(cpr_rail_id_t rail_id, cpr_voltage_mode_t voltage_mode, int32 static_margin_mv)
{
  // If the fuse indicates an adjustment reduction, reduce the static margin by 30mV
  int32 ret = static_margin_mv;
  int32 offset_margin_change = 0;  //how much is the fuse changing the static margin

  //CPR_FUSE_FUSED_MARGIN_REDUCTION is also called the CPRx_TARGET_OFFSET_FUSE in the HPG
  //increase the margin if the fuse is a positive number, lower it if it's negative
  if ((voltage_mode == CPR_VOLTAGE_MODE_SVS) || (voltage_mode == CPR_VOLTAGE_MODE_SVS2)) {
      //changes are in 10mV increments
      offset_margin_change = cpr_fuses_get_fuse_value(CPR_RAIL_MSS, CPR_FUSE_SVS_OFFSET) * 10;
      ret += offset_margin_change;
  }
  if (voltage_mode == CPR_VOLTAGE_MODE_TURBO) {
      //changes are in 10mV increments
      offset_margin_change = cpr_fuses_get_fuse_value(CPR_RAIL_MSS, CPR_FUSE_TURBO_OFFSET) * 10;
      ret += offset_margin_change;
  }  
  
  CPR_LOGGING(3, "mss_8996_static_margin_adjustment (mode: %s) (change: %d) (margin: %d)", cpr_logging_get_ascii_corner_name(voltage_mode), offset_margin_change, ret);

  return ret;
}



////////////////////////////////////////////////
// Cx config
////////////////////////////////////////////////

static const cpr_enablement_versioned_rail_config_t cx_8996_versioned_cpr_enablement =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {     //foundary        min version (inclusive)      max version (exclusive)       min CPR Fuse rev,  max CPR Fuse rev
            { CPR_FOUNDRY_SS,   CPR_CHIPINFO_VERSION(0, 0), CPR_CHIPINFO_VERSION(0xFF, 0xFF),   0,   0xff },
            { CPR_FOUNDRY_TSMC, CPR_CHIPINFO_VERSION(0, 0), CPR_CHIPINFO_VERSION(0xFF, 0xFF),   0,   0xff },
        },
        .foundry_range_count = 2,
    },
    .enablement_init_params = &CPR_ENABLE_OPEN_LOOP,  //read the fuses, but we won't actually set any voltages.
};

static const cpr_enablement_rail_config_t cx_8996_cpr_enablement =
{
    .rail_id = CPR_RAIL_CX,
    .versioned_rail_config = (const cpr_enablement_versioned_rail_config_t*[])
    {
        &cx_8996_versioned_cpr_enablement,
    },
    .versioned_rail_config_count = 1,
};

////////////////////////////////////////////////
// MSS config
////////////////////////////////////////////////
// 8996 V1,V2
static const cpr_enablement_versioned_rail_config_t mss_8996_versioned_cpr_enablement_v0_2_rall =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {     //foundary        min version (inclusive)      max version (exclusive)       min CPR Fuse rev,  max CPR Fuse rev
            { CPR_FOUNDRY_SS, CPR_CHIPINFO_VERSION(0, 0), CPR_CHIPINFO_VERSION(0x3, 0x0), 0, 0xff },
            { CPR_FOUNDRY_TSMC, CPR_CHIPINFO_VERSION(0, 0), CPR_CHIPINFO_VERSION(0x3, 0x0), 0, 0xff },
        },
        .foundry_range_count = 2,
    },
    .enablement_init_params = &CPR_ENABLE_CLOSED_LOOP,
    //.enablement_init_params = &CPR_ENABLE_OPEN_LOOP,
    //.enablement_init_params = &CPR_ENABLE_GLOBAL_CEILING_VOLTAGE,
    .supported_level = (const cpr_enablement_supported_level_t[])
    {                          
        //Mode                Static-Margin (mV) Custom static margin function    aging_scaling_factor 
        { CPR_VOLTAGE_MODE_SVS_MIN,          0,                  NULL,                  1},
        { CPR_VOLTAGE_MODE_SVS2,             0,                  NULL,                  1},  
        { CPR_VOLTAGE_MODE_SVS,              0,                  NULL,                  1},
        { CPR_VOLTAGE_MODE_NOMINAL,        -30,                  NULL,                  1},
        { CPR_VOLTAGE_MODE_TURBO,          -75,                  NULL,                  1},
    },
    .supported_level_count = 5,
	.margin_adjustment_enablement = (const cpr_margin_adjustment_enablement_t[])
    {
        //adjustment type            //enablement
        {CPR_MARGIN_ADJUSTMENT_STATIC, TRUE},
    },
    .margin_adjustment_enablement_count = 1,
 
    .max_floor_to_ceilings = (const cpr_config_max_floor_to_ceiling_level_t[])
    {
            //CPR dynamic range for the corner is the open loop ceiling - (max_floor_to_ceiling)
            //Mode,                   max_floor_to_ceiling
            { CPR_VOLTAGE_MODE_SVS_MIN,               0  },  //SVS Min is pinned at the OL value
            { CPR_VOLTAGE_MODE_SVS2,              75000  },
            { CPR_VOLTAGE_MODE_SVS,               80000  },
            { CPR_VOLTAGE_MODE_NOMINAL,          140000  },
            { CPR_VOLTAGE_MODE_TURBO,            222500  },
    },
    .max_floor_to_ceiling_level_count = 5,

    //7/21/15 8996 CPR voltages use new calculation methods
    .custom_voltage_fn = custom_8996_voltage_fn,  // NULL ==  Use ordinary floor and ceiling calculation functions.

    .mode_to_settle_count = 0,  // 0 == No init time CPR measurements
};


//====================================================================================
// 8996 V3 MSS CPR Fuse Rev 0,1
//====================================================================================
static const cpr_enablement_versioned_rail_config_t mss_8996_versioned_cpr_enablement_v3_r01 =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {      //foundary        min version (inclusive)      max version (exclusive)       min CPR Fuse rev,  max CPR Fuse rev
            { CPR_FOUNDRY_SS,   CPR_CHIPINFO_VERSION(0x3, 0x0), CPR_CHIPINFO_VERSION(0x4, 0),   0,   2 },
            { CPR_FOUNDRY_TSMC, CPR_CHIPINFO_VERSION(0x3, 0x0), CPR_CHIPINFO_VERSION(0x4, 0),   0,   2 },
        },
        .foundry_range_count = 2,
    },
    .enablement_init_params = &CPR_ENABLE_CLOSED_LOOP,
    //.enablement_init_params = &CPR_ENABLE_OPEN_LOOP,
    //.enablement_init_params = &CPR_ENABLE_GLOBAL_CEILING_VOLTAGE,
    .supported_level = (const cpr_enablement_supported_level_t[])
    {                          
        //Mode                Static-Margin (mV) Custom static margin function    aging_scaling_factor 
        { CPR_VOLTAGE_MODE_SVS_MIN,          100,                  NULL,                 1},
        { CPR_VOLTAGE_MODE_SVS2,             48,                  mss_8996_static_margin_adjustment,                  1},  
        { CPR_VOLTAGE_MODE_SVS,              53,                  mss_8996_static_margin_adjustment,                  1},
        { CPR_VOLTAGE_MODE_NOMINAL,          48,                  NULL,                  1},
        { CPR_VOLTAGE_MODE_TURBO,             0,                  mss_8996_static_margin_adjustment,                  1},
    },
    .supported_level_count = 5,
	.margin_adjustment_enablement = (const cpr_margin_adjustment_enablement_t[])
    {
        //adjustment type            //enablement
        {CPR_MARGIN_ADJUSTMENT_STATIC, TRUE},
        { CPR_MARGIN_ADJUSTMENT_AGING, TRUE },
    },
    .margin_adjustment_enablement_count = 2,	 
    .max_floor_to_ceilings = (const cpr_config_max_floor_to_ceiling_level_t[])
    {
            //CPR dynamic range for the corner is the open loop ceiling - (max_floor_to_ceiling)
            //Mode,                   max_floor_to_ceiling
            { CPR_VOLTAGE_MODE_SVS_MIN,               0 },  //SVS Min is pinned at the OL value
            { CPR_VOLTAGE_MODE_SVS2,              75000 },
            { CPR_VOLTAGE_MODE_SVS,               80000 },
            { CPR_VOLTAGE_MODE_NOMINAL,           90000 },
            { CPR_VOLTAGE_MODE_TURBO,            100000 },
    },
    .max_floor_to_ceiling_level_count = 5,

    //7/21/15 8996 CPR voltages use new calculation methods
    .custom_voltage_fn = custom_8996_voltage_fn,  // NULL ==  Use ordinary floor and ceiling calculation functions.

    .mode_to_settle_count = 0,  // 0 == No init time CPR measurements
};



//====================================================================================
// 8996 V3 MSS CPR Fuse Rev 2
//====================================================================================
static const cpr_enablement_versioned_rail_config_t mss_8996_versioned_cpr_enablement_v3_r2 =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {      //foundary        min version (inclusive)      max version (exclusive)       min CPR Fuse rev,  max CPR Fuse rev
            { CPR_FOUNDRY_SS,   CPR_CHIPINFO_VERSION(0x3, 0x0), CPR_CHIPINFO_VERSION(0x4, 0),   2,   3 },
            { CPR_FOUNDRY_TSMC, CPR_CHIPINFO_VERSION(0x3, 0x0), CPR_CHIPINFO_VERSION(0x4, 0),   2,   3 },
        },
        .foundry_range_count = 2,
    },
    .enablement_init_params = &CPR_ENABLE_CLOSED_LOOP,
    //.enablement_init_params = &CPR_ENABLE_OPEN_LOOP,
    //.enablement_init_params = &CPR_ENABLE_GLOBAL_CEILING_VOLTAGE,
    .supported_level = (const cpr_enablement_supported_level_t[])
    {                          
        //Mode                Static-Margin (mV) Custom static margin function    aging_scaling_factor 
        { CPR_VOLTAGE_MODE_SVS_MIN,          100,                  NULL,                  1},
        { CPR_VOLTAGE_MODE_SVS2,             30,                   mss_8996_static_margin_adjustment,                  1},  
        { CPR_VOLTAGE_MODE_SVS,              65,                   mss_8996_static_margin_adjustment,                  1},
        { CPR_VOLTAGE_MODE_NOMINAL,          45,                   NULL,                  1},
        { CPR_VOLTAGE_MODE_TURBO,            15,                   mss_8996_static_margin_adjustment,                  1},
    },
    .supported_level_count = 5,
	.margin_adjustment_enablement = (const cpr_margin_adjustment_enablement_t[])
    {
        //adjustment type            //enablement
        {CPR_MARGIN_ADJUSTMENT_STATIC, TRUE},
        { CPR_MARGIN_ADJUSTMENT_AGING, TRUE },
    },
    .margin_adjustment_enablement_count = 2,	 
    .max_floor_to_ceilings = (const cpr_config_max_floor_to_ceiling_level_t[])
    {
            //CPR dynamic range for the corner is the open loop ceiling - (max_floor_to_ceiling)
            //Mode,                   max_floor_to_ceiling
            { CPR_VOLTAGE_MODE_SVS_MIN,               0 },
            { CPR_VOLTAGE_MODE_SVS2,              75000 },
            { CPR_VOLTAGE_MODE_SVS,               80000 },
            { CPR_VOLTAGE_MODE_NOMINAL,           90000 },
            { CPR_VOLTAGE_MODE_TURBO,            100000 },
    },
    .max_floor_to_ceiling_level_count = 5,

    //7/21/15 8996 CPR voltages use new calculation methods
    .custom_voltage_fn = custom_8996_voltage_fn,  // NULL ==  Use ordinary floor and ceiling calculation functions.

    .mode_to_settle_count = 0,  // 0 == No init time CPR measurements
};


//====================================================================================
// 8996 V3 MSS CPR Fuse Rev 3 and beyond
//====================================================================================
static const cpr_enablement_versioned_rail_config_t mss_8996_versioned_cpr_enablement_v3_r34all =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {      //foundary        min version (inclusive)      max version (exclusive)       min CPR Fuse rev,  max CPR Fuse rev
            { CPR_FOUNDRY_SS,   CPR_CHIPINFO_VERSION(0x3, 0x0), CPR_CHIPINFO_VERSION(0x4, 0),   3,   0xff },
            { CPR_FOUNDRY_TSMC, CPR_CHIPINFO_VERSION(0x3, 0x0), CPR_CHIPINFO_VERSION(0x4, 0),   3,   0xff },
        },
        .foundry_range_count = 2,
    },
    .enablement_init_params = &CPR_ENABLE_CLOSED_LOOP,
    //.enablement_init_params = &CPR_ENABLE_OPEN_LOOP,
    //.enablement_init_params = &CPR_ENABLE_GLOBAL_CEILING_VOLTAGE,
    .supported_level = (const cpr_enablement_supported_level_t[])
    {                          
        //Mode                Static-Margin (mV) Custom static margin function    aging_scaling_factor 
        { CPR_VOLTAGE_MODE_SVS_MIN,          100,                  NULL,                  1},
        { CPR_VOLTAGE_MODE_SVS2,              0,                   mss_8996_static_margin_adjustment,                  1},  
        { CPR_VOLTAGE_MODE_SVS,              35,                   mss_8996_static_margin_adjustment,                  1},
        { CPR_VOLTAGE_MODE_NOMINAL,          45,                   NULL,                  1},
        { CPR_VOLTAGE_MODE_TURBO,            5,                    mss_8996_static_margin_adjustment,                  1},
    },
    .supported_level_count = 5,
	.margin_adjustment_enablement = (const cpr_margin_adjustment_enablement_t[])
    {
        //adjustment type            //enablement
        {CPR_MARGIN_ADJUSTMENT_STATIC, TRUE},
       { CPR_MARGIN_ADJUSTMENT_AGING, TRUE },
    },
    .margin_adjustment_enablement_count = 2,	 
    .max_floor_to_ceilings = (const cpr_config_max_floor_to_ceiling_level_t[])
    {
            //CPR dynamic range for the corner is the open loop ceiling - (max_floor_to_ceiling)
            //Mode,                   max_floor_to_ceiling
            { CPR_VOLTAGE_MODE_SVS_MIN,               0 },
            { CPR_VOLTAGE_MODE_SVS2,              75000 },
            { CPR_VOLTAGE_MODE_SVS,               80000 },
            { CPR_VOLTAGE_MODE_NOMINAL,           90000 },
            { CPR_VOLTAGE_MODE_TURBO,            100000 },
    },
    .max_floor_to_ceiling_level_count = 5,

    //7/21/15 8996 CPR voltages use new calculation methods
    .custom_voltage_fn = custom_8996_voltage_fn,  // NULL ==  Use ordinary floor and ceiling calculation functions.

    .mode_to_settle_count = 0,  // 0 == No init time CPR measurements
};


//====================================================================================
// 8996 V4 MSS and beyond   (A VERY SAFE CONFIGURATION TO USE IF THIS DRIVER HAS NOT
//                           BEEN UPDATED FOR FUTURE PARTS)
//====================================================================================
static const cpr_enablement_versioned_rail_config_t mss_8996_versioned_cpr_enablement_v4_rall =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {      //foundary        min version (inclusive)      max version (exclusive)       min CPR Fuse rev,  max CPR Fuse rev
            { CPR_FOUNDRY_SS,   CPR_CHIPINFO_VERSION(0x4, 0x0), CPR_CHIPINFO_VERSION(0xF, 0xF),   0,   0xff },
            { CPR_FOUNDRY_TSMC, CPR_CHIPINFO_VERSION(0x4, 0x0), CPR_CHIPINFO_VERSION(0xF, 0xF),   0,   0xff },
        },
        .foundry_range_count = 2,
    },
    .enablement_init_params = &CPR_ENABLE_CLOSED_LOOP,
    //.enablement_init_params = &CPR_ENABLE_OPEN_LOOP,
    //.enablement_init_params = &CPR_ENABLE_GLOBAL_CEILING_VOLTAGE,
    .supported_level = (const cpr_enablement_supported_level_t[])
    {                          
        //Mode                Static-Margin (mV) Custom static margin function    aging_scaling_factor 
        { CPR_VOLTAGE_MODE_SVS_MIN,          100,                  NULL,                  1},
        { CPR_VOLTAGE_MODE_SVS2,             100,                  mss_8996_static_margin_adjustment,                  1},  
        { CPR_VOLTAGE_MODE_SVS,              100,                  mss_8996_static_margin_adjustment,                  1},
        { CPR_VOLTAGE_MODE_NOMINAL,          100,                  NULL,                  1},
        { CPR_VOLTAGE_MODE_TURBO,            100,                  mss_8996_static_margin_adjustment,                  1},
    },
    .supported_level_count = 5,
	.margin_adjustment_enablement = (const cpr_margin_adjustment_enablement_t[])
    {
        //adjustment type            //enablement
        {CPR_MARGIN_ADJUSTMENT_STATIC, TRUE },
       //{ CPR_MARGIN_ADJUSTMENT_AGING, TRUE },
    },
    .margin_adjustment_enablement_count = 1,	 
    .max_floor_to_ceilings = (const cpr_config_max_floor_to_ceiling_level_t[])
    {
            //CPR dynamic range for the corner is the open loop ceiling - (max_floor_to_ceiling)
            //Mode,                   max_floor_to_ceiling
            { CPR_VOLTAGE_MODE_SVS_MIN,               0 },
            { CPR_VOLTAGE_MODE_SVS2,              75000 },
            { CPR_VOLTAGE_MODE_SVS,               80000 },
            { CPR_VOLTAGE_MODE_NOMINAL,           90000 },
            { CPR_VOLTAGE_MODE_TURBO,            100000 },
    },
    .max_floor_to_ceiling_level_count = 5,

    //7/21/15 8996 CPR voltages use new calculation methods
    .custom_voltage_fn = custom_8996_voltage_fn,  // NULL ==  Use ordinary floor and ceiling calculation functions.

    .mode_to_settle_count = 0,  // 0 == No init time CPR measurements
};



static const cpr_enablement_rail_config_t mss_8996_cpr_enablement =
{
    .rail_id = CPR_RAIL_MSS,
    .versioned_rail_config = (const cpr_enablement_versioned_rail_config_t*[])
    {
        &mss_8996_versioned_cpr_enablement_v0_2_rall,
        &mss_8996_versioned_cpr_enablement_v3_r01,
        &mss_8996_versioned_cpr_enablement_v3_r2,
        &mss_8996_versioned_cpr_enablement_v3_r34all,
        &mss_8996_versioned_cpr_enablement_v4_rall,
    },
    .versioned_rail_config_count = 5,
};



const cpr_enablement_config_t cpr_bsp_enablement_config =
{
    .rail_enablement_config = (const cpr_enablement_rail_config_t*[])
    {
        &mss_8996_cpr_enablement,
        &cx_8996_cpr_enablement,
    },
    .rail_enablement_config_count = 2,
};

