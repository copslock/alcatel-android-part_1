/*===========================================================================

  Copyright (c) 2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ===========================================================================*/
#include "cpr_voltage_ranges.h"
#include "cpr_device_hw_version.h"


// Cx Voltage Ranges

static const cpr_config_versioned_voltage_ranges_t vALL_8996_cx_voltage_ranges =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            {CPR_FOUNDRY_SS,   CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF),   0,   0xff},
            {CPR_FOUNDRY_TSMC, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF),   0,   0xff},  //modem foundry detection not fully implemented, it currently returns TSMC
        },
        .foundry_range_count = 2,
    },
    .voltage_level = (const cpr_config_voltage_level_t[])
    {
        //Mode,                              Ceiling,  Floor, Fuse-Ref, Corr-factor,         Fuse-Type
        {CPR_VOLTAGE_MODE_SVS2,               670000, 545000,   670000,           0,     CPR_FUSE_SVS2},
        {CPR_VOLTAGE_MODE_SVS,                745000, 625000,   745000,           0,      CPR_FUSE_SVS},
        {CPR_VOLTAGE_MODE_NOMINAL,            905000, 755000,   905000,           0,  CPR_FUSE_NOMINAL},
        {CPR_VOLTAGE_MODE_SUPER_TURBO,       1015000, 855000,  1015000,           0,    CPR_FUSE_TURBO},
    },
    .voltage_level_count = 4,
};

static const cpr_config_rail_voltage_ranges_t cx_8996_voltage_ranges =
{
    .rail_id = CPR_RAIL_CX,
    .versioned_voltage_range = (const cpr_config_versioned_voltage_ranges_t*[])
    {
        &vALL_8996_cx_voltage_ranges,
    },
    .versioned_voltage_range_count = 1,
};

//====================================================================================
// MSS Voltage Ranges V2.x
// 2.x devices have no open loop adjustments
//====================================================================================
static const cpr_config_versioned_voltage_ranges_t MSS_8996_voltage_ranges_V2 =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {     //foundary        min version (inclusive)      max version (exclusive)       min CPR Fuse rev,  max CPR Fuse rev
            { CPR_FOUNDRY_SS,   CPR_CHIPINFO_VERSION(0, 0), CPR_CHIPINFO_VERSION(0x3, 0),   0,   0xF },
            { CPR_FOUNDRY_TSMC, CPR_CHIPINFO_VERSION(0, 0), CPR_CHIPINFO_VERSION(0x3, 0),   0,   0xF },  //modem foundry detection not fully implemented, it currently returns TSMC
        },
        .foundry_range_count = 2,
    },
    .voltage_level = (const cpr_config_voltage_level_t[])
    {
               //Mode,               Ceiling, Floor, Fuse-Ref,    Corr-factor,       Fuse-Type
        { CPR_VOLTAGE_MODE_SVS_MIN,  600000, 520000,   600000,         0,    CPR_FUSE_NO_FUSE },
        { CPR_VOLTAGE_MODE_SVS2,     675000, 520000,   675000,         0,       CPR_FUSE_SVS2 },
        { CPR_VOLTAGE_MODE_SVS,      750000, 520000,   750000,         0,        CPR_FUSE_SVS },
        { CPR_VOLTAGE_MODE_NOMINAL,  900000, 520000,   900000,         0,    CPR_FUSE_NOMINAL },
        { CPR_VOLTAGE_MODE_TURBO,   1012500, 520000,  1012500,         0,      CPR_FUSE_TURBO },
    },
    .voltage_level_count = 5,
};

//====================================================================================
// MSS Voltage Ranges V3.x  CPR Fuse Ver 0
// 3.x Fuse Ver 0 devices have some unique open loop adjustments
//====================================================================================
static const cpr_config_versioned_voltage_ranges_t MSS_8996_voltage_ranges_V3_CPRFuseRev0=
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {     //foundary        min version (inclusive)      max version (exclusive)       min CPR Fuse rev,  max CPR Fuse rev
            { CPR_FOUNDRY_SS,   CPR_CHIPINFO_VERSION(0x3, 0), CPR_CHIPINFO_VERSION(0xff, 0xff),  0,   0x1 },
            { CPR_FOUNDRY_TSMC, CPR_CHIPINFO_VERSION(0x3, 0), CPR_CHIPINFO_VERSION(0xff, 0xff),  0,   0x1 },  //modem foundry detection not fully implemented, it currently returns TSMC
        },
        .foundry_range_count = 2,
    },
    .voltage_level = (const cpr_config_voltage_level_t[])
    {
               //Mode,               Ceiling, Floor, Fuse-Ref,    Corr-factor,       Fuse-Type
        { CPR_VOLTAGE_MODE_SVS_MIN,  600000, 520000,   600000,         0,    CPR_FUSE_NO_FUSE },
        { CPR_VOLTAGE_MODE_SVS2,     675000, 520000,   675000,         0,       CPR_FUSE_SVS2 },
        { CPR_VOLTAGE_MODE_SVS,      750000, 520000,   750000,         0,        CPR_FUSE_SVS },
        { CPR_VOLTAGE_MODE_NOMINAL,  900000, 520000,   900000,     30000,    CPR_FUSE_NOMINAL },
        { CPR_VOLTAGE_MODE_TURBO,   1012500, 520000,  1012500,    -10000,      CPR_FUSE_TURBO },
    },
    .voltage_level_count = 5,
};



//====================================================================================
// MSS Voltage Ranges V3.x  CPR Fuse Ver 1 and Ver 2
// 3.x Fuse Ver 1 and Ver 2 share the same open loop adjustments
//====================================================================================
static const cpr_config_versioned_voltage_ranges_t MSS_8996_voltage_ranges_V3_CPRFuseRev1v2 =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {     //foundary        min version (inclusive)      max version (exclusive)       min CPR Fuse rev,  max CPR Fuse rev
            { CPR_FOUNDRY_SS,   CPR_CHIPINFO_VERSION(0x3, 0), CPR_CHIPINFO_VERSION(0xff, 0xff),  1,   3 },
            { CPR_FOUNDRY_TSMC, CPR_CHIPINFO_VERSION(0x3, 0), CPR_CHIPINFO_VERSION(0xff, 0xff),  1,   3 },  //modem foundry detection not fully implemented, it currently returns TSMC
        },
        .foundry_range_count = 2,
    },
    .voltage_level = (const cpr_config_voltage_level_t[])
    {
               //Mode,               Ceiling, Floor, Fuse-Ref,    Corr-factor,       Fuse-Type
        { CPR_VOLTAGE_MODE_SVS_MIN,  600000, 520000,   600000,         0,    CPR_FUSE_NO_FUSE },
        { CPR_VOLTAGE_MODE_SVS2,     675000, 520000,   675000,    -30000,       CPR_FUSE_SVS2 },
        { CPR_VOLTAGE_MODE_SVS,      750000, 520000,   750000,    -30000,        CPR_FUSE_SVS },
        { CPR_VOLTAGE_MODE_NOMINAL,  900000, 520000,   900000,         0,    CPR_FUSE_NOMINAL },
        { CPR_VOLTAGE_MODE_TURBO,   1012500, 520000,  1012500,    -10000,      CPR_FUSE_TURBO },
    },
    .voltage_level_count = 5,
};


//====================================================================================
// MSS Voltage Ranges V3.x  CPR Fuse Ver 1 and Ver 2
// 3.x Fuse Ver 3 and beyond currently have 0 open loop adjustments
//====================================================================================
static const cpr_config_versioned_voltage_ranges_t MSS_8996_voltage_ranges_V3_CPRFuseRev3 =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {     //foundary        min version (inclusive)      max version (exclusive)       min CPR Fuse rev,  max CPR Fuse rev
            { CPR_FOUNDRY_SS,   CPR_CHIPINFO_VERSION(0x3, 0), CPR_CHIPINFO_VERSION(0xff, 0xff),  3,    0xf },
            { CPR_FOUNDRY_TSMC, CPR_CHIPINFO_VERSION(0x3, 0), CPR_CHIPINFO_VERSION(0xff, 0xff),  3,    0xf },  //modem foundry detection not fully implemented, it currently returns TSMC
        },
        .foundry_range_count = 2,
    },
    .voltage_level = (const cpr_config_voltage_level_t[])
    {
               //Mode,               Ceiling, Floor, Fuse-Ref,    Corr-factor,       Fuse-Type
        { CPR_VOLTAGE_MODE_SVS_MIN,  600000, 520000,   600000,         0,    CPR_FUSE_NO_FUSE },
        { CPR_VOLTAGE_MODE_SVS2,     675000, 520000,   675000,         0,       CPR_FUSE_SVS2 },
        { CPR_VOLTAGE_MODE_SVS,      750000, 520000,   750000,         0,        CPR_FUSE_SVS },
        { CPR_VOLTAGE_MODE_NOMINAL,  900000, 520000,   900000,         0,    CPR_FUSE_NOMINAL },
        { CPR_VOLTAGE_MODE_TURBO,   1012500, 520000,  1012500,         0,      CPR_FUSE_TURBO },
    },
    .voltage_level_count = 5,
};


static const cpr_config_rail_voltage_ranges_t mss_8996_voltage_ranges =
{
    .rail_id = CPR_RAIL_MSS,
    .versioned_voltage_range = (const cpr_config_versioned_voltage_ranges_t*[])
    {
        &MSS_8996_voltage_ranges_V2,
        &MSS_8996_voltage_ranges_V3_CPRFuseRev0,
        &MSS_8996_voltage_ranges_V3_CPRFuseRev1v2,
        &MSS_8996_voltage_ranges_V3_CPRFuseRev3,
    },
    .versioned_voltage_range_count = 4,
};




const cpr_config_global_voltage_ranges_t cpr_bsp_voltage_ranges_config =
{
    .rail_voltage_ranges = (const cpr_config_rail_voltage_ranges_t*[])
    {
        &mss_8996_voltage_ranges,
        &cx_8996_voltage_ranges,
    },
    .rail_voltage_ranges_count = 2,
};

// Rail PMIC step-sizes

const cpr_config_misc_info_t cpr_bsp_misc_info_config =
{
    .rail_info = (const cpr_config_misc_rail_info_t[])
    {
        //Rail,             PMIC Step-Size
        {CPR_RAIL_MX,                 5000},
        {CPR_RAIL_CX,                 5000},
        {CPR_RAIL_MSS,               12500},
        {CPR_RAIL_VDDA_EBI,          12500},
        {CPR_RAIL_SSC_MX,            12500},
        {CPR_RAIL_SSC_CX,            12500},
    },
    .rail_info_count = 6,
};
