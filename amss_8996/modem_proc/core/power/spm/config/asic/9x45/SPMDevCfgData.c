/*===========================================================================

FILE:         SPMDevCfgData.c

DESCRIPTION:  This is the BSP data for the SPM driver. This data is for the 
              9x45 MSS processor.

              Copyright (c) 2014 QUALCOMM Technologies Inc. (QTI)
              All Rights Reserved.
              Qualcomm Confidential and Proprietary

$Header: //components/rel/core.mpss/3.4.c3.11/power/spm/config/asic/9x45/SPMDevCfgData.c#1 $

============================================================================*/


/*==========================================================================
 *                            INCLUDES
 *==========================================================================*/
#include "BSPspm.h"

/*==========================================================================
 *                         INTERNAL TYPES
 *=========================================================================*/

/**
 * @brief Enumeration containing SPM control signal mapping for this target.
 *
 * The bits are toggled using the bit index (not bitmask)
 */
typedef enum 
{
  QDSP6_RESET,               /* 0  */
  QDSP6_CLAMP,               /* 1  */
  QDSP6_CLOCK,               /* 2  */
  QDSP6_MEM_SLEEP,           /* 3  */
  QDSP6_BHS,                 /* 4  */
  QDSP6_LDO_BYPASS,          /* 5  */
  QDSP6_LDO,                 /* 6  */
  QDSP6_PLL_RESET,           /* 7  */
  QDSP6_PLL_BYPASS,          /* 8  */
  QDSP6_PLL_OUTPUT,          /* 9  */
  QDSP6_XO_CLOCK       = 14, /* 14 */
  QDSP6_LDO_RET_VREF,        /* 15 */
  QDSP6_CLAMP_WL,            /* 16 */
  QDSP6_QMC_MEM_CLAMP,       /* 17 */
  QDSP6_PLL_FREEZE,          /* 18 */
  QDSP6_MEM_SRC_BIAS,        /* 19 */
  QDSP6_RETAIN_SAVE,         /* 20 */
  QDSP6_RETAIN_RESTORE,      /* 21 */
  QDSP6_PMI,                 /* 22 */
  QDSP6_MEM_RET        = 30, /* 30 */
} SPM_SEQ_PWR_CTL_BITS;

/**
 * @brief Set of macros to create SPM command to toggle power control bits
 *        defined in @SPM_SEQ_PWR_CTL_BITS
 */
#define SPM_CMD_BIT7_4(bit)   (((bit) & 0xF) << 4)
#define SPM_CMD_BIT3(bit)     (((bit) & 0x10) >> 1)
#define SPM_CMD_BIT2_1(delay) ((delay) << 1)
#define SPM_SEQ_PWR_CTL(bit,delay) ( SPM_CMD_BIT7_4(bit)   | \
                                     SPM_CMD_BIT3(bit)     | \
                                     SPM_CMD_BIT2_1(delay) ) 

/**
 * @brief Enumeration to indicate bit for different PMIC data in 
 *        SPM command.
 */
typedef enum
{
  DATA_0,
  DATA_1,
  DATA_2,
} SPM_SEQ_PMIC_DATA_BITS;

/**
 * @brief Macro to create SPM command to send PMIC data based on 
 *        @SPM_SEQ_PMIC_DATA_BITS.
 */
#define SPM_SEQ_PMIC_CTL(bit,delay) (bit<<4)|(0x1)

/**
 * @brief Enumeration to indicate bit settings for RPM interaction
 *        during Sleep in SPM command.
 */
typedef enum
{
  NO_RPM_HS,
  RPM_HS
} SPM_SEQ_SLEEP_BITS;

/**
 * @brief Macro to set up SPM command for RPM interaction while waiting for
 *        wake up event.
 */
#define SPM_SEQ_SLP_CTL(bit,delay) (bit<<2)|(0x3)

/**
 * @brief Event bits for SPM command.
 */
typedef enum
{
  EVENT_0,
  EVENT_1,
  EVENT_2,
  EVENT_3
} SPM_SEQ_EVENT_BITS;

/**
 * @brief Macro to create SPM command to wait for events defined in 
 *        @SPM_SEQ_EVENT_BITS.
 */
#define SPM_SEQ_EVENT_CTL(bit,delay) (bit<<4)|(0xB)

/**
 * @brief SPM end of sequence command.
 */
#define SPM_SEQ_END_CTL(bit,delay)   (0xF)

/**
 * @brief Macro to wait for PMIC to restore modem power rail
 */
 #define SPM_SEQ_ADD_XO_CYCLE_DELAY(bit,delay)  (0x1F)
 
/*==========================================================================
 *                                   NOTE
 *=========================================================================*/

/**
 * Even though different low power modes share some commands, for some 
 * of them actual impact at the HW level will be determined by special control
 * registers values (e.g. SPMCTL) which are programmed as part of enter 
 * functions of low power modes. Below is the list of such known 
 * commands/toggles.
 *
 * QDSP6_RESET
 * -----------
 * Depending on how SPMCTL_EN_ARES register is programed, this command may 
 * reset core differently (partially or full).
 * 
 * QDSP6_RETAIN_SAVE
 * -----------------
 * If SAVE field in SPMCTL_EN_STATERET is programed zero, this command/toggle 
 * may not take impact. (For example in Power collapse - but we removed this 
 * toggle itself from sequence).
 *
 * QDSP6_RETAIN_RESTORE 
 * --------------------
 * Same as QDSP6_RETAIN_SAVE but with RESTORE field.
 *
 * QDSP6_PMI
 * ---------
 * Same as QDSP6_RETAIN_SAVE but with WAKEUP_IRQ field.
 */
    
/*==========================================================================
 *                                   DATA
 *=========================================================================*/

/**
 * @brief Target specific SPM Hardware configuration.
 */
BSP_spm_ConfigDataType devcfgSpmBspData[] =
{
  {
    /* Saw2SecureCfg
     * ------------
     * sawCtl                  pwrCtl                   vltCtl */
    {  BSP_SPM_SAW_NONSECURE,  BSP_SPM_SAW_NONSECURE,   BSP_SPM_SAW_NONSECURE },
 
    /* Saw2Cfg
     * ------
     * frcRefClkOn  clkDiv   */     
    {  0x1,         0x1F },

    /* Saw2SpmCtlCfg
     * ------------
     * inhibitStartAddrReset wakeupConfig */
    {  0x1,                  BSP_SPM_SAW_WAKEUP_CONFIG_0 },

    /* Saw2SpmDlyType
     * -------------
     * each field is 10 bits: max value of 1024
     * delay time = (delay_n * clkDiv)/19.2e6
     * delay_1, delay_2, delay_3 */
    {  0      , 0      , 0      },

    /* Saw2SpmPmicData[]
     * ----------
     *  pmicSel                      , pmicData, pmicDly, pmicDataSize, adrIdx */
    {
      { BSP_SPM_SAW_PMIC_DATA_NOMINAL, 0x80,     0x0,      0,           3 }, /* 1 */
      { BSP_SPM_SAW_PMIC_DATA_PC     , 0x00,     0x0,      0,           3 }, /* 0 */
      { BSP_SPM_SAW_PMIC_DATA_VDD_MIN, 0x00,     0x0,      0,           0 }, /* 2 */
    }
  }
}; /* SPM_BSP_DATA */

/**
 * @brief SPM command sequence for MSS APCR with PLL on 
 *  
 *        Corresponds to APCR short sequence from HPG. Table 4-5.
 */
static uint8 spmCmdSeqAPCRPLLOn[] =
{ 
  /* index 0 */
  SPM_SEQ_PWR_CTL( QDSP6_CLOCK,         0 ), /* QDSP6 Clock Off */
  SPM_SEQ_PWR_CTL( QDSP6_CLAMP,         0 ), /* QDSP6 Assert Clamp */
  SPM_SEQ_PWR_CTL( QDSP6_RETAIN_SAVE,   0 ), /* Assert QDSP6 retain save*/
  SPM_SEQ_PWR_CTL( QDSP6_RETAIN_SAVE,   0 ), /* Deassert QDSP6 retain save */

  /* index 4 */
  SPM_SEQ_PWR_CTL( QDSP6_CLAMP_WL,      0 ), /* QDSP6 Assert Clamp WL */
  SPM_SEQ_PWR_CTL( QDSP6_MEM_RET      , 0 ), /* QDSP6 Assert Mem Ret Sleep */
  SPM_SEQ_ADD_XO_CYCLE_DELAY( 0,        0 ), /* Add one XO cycle delay */
  SPM_SEQ_PWR_CTL( QDSP6_QMC_MEM_CLAMP, 0 ), /* QDSP6 QMC Mem Clamp */

  /* index 8 */
  SPM_SEQ_PWR_CTL( QDSP6_LDO_BYPASS,    0 ), /* Force LDO Bypass Off */
  SPM_SEQ_PWR_CTL( QDSP6_BHS,           0 ), /* Force BHS Off */
  SPM_SEQ_PWR_CTL( QDSP6_LDO,           0 ), /* Force LDO Off */
  SPM_SEQ_SLP_CTL( NO_RPM_HS,           0 ), /* Init Sleep to NO RPM HS */

  /* index 12 */
  SPM_SEQ_PWR_CTL( QDSP6_RESET,         0 ), /* Assert Reset */  
  SPM_SEQ_PWR_CTL( QDSP6_LDO,           0 ), /* Deassert LDO Off */
  SPM_SEQ_PWR_CTL( QDSP6_BHS,           1 ), /* Deassert BHS Off */
  SPM_SEQ_PWR_CTL( QDSP6_LDO_BYPASS,    0 ), /* Deassert LDO Bypass Off */

  /* index 16 */
  SPM_SEQ_PWR_CTL( QDSP6_QMC_MEM_CLAMP, 0 ), /* Deassert QMC Mem Clamp */ 
  SPM_SEQ_PWR_CTL( QDSP6_MEM_RET,       1 ), /* QDSP6 Deassert Mem Ret Sleep */
  SPM_SEQ_PWR_CTL( QDSP6_CLAMP_WL,      0 ), /* Deassert QDSP6 Clamp WL */
  SPM_SEQ_PWR_CTL( QDSP6_RETAIN_RESTORE,0 ), /* Assert QDSP6 retain restore */

  /* index 20 */
  SPM_SEQ_PWR_CTL( QDSP6_RETAIN_RESTORE,0 ), /* Deassert QDSP6 retain restore */
  SPM_SEQ_PWR_CTL( QDSP6_CLAMP,         0 ), /* Deassert QDSP6 Clamp */
  SPM_SEQ_PWR_CTL( QDSP6_RESET,         0 ), /* Deassert Reset */
  SPM_SEQ_PWR_CTL( QDSP6_PMI,           0 ), /* Assert Power Manager Interrupt (PMI) */

  /* index 24 */
  SPM_SEQ_PWR_CTL( QDSP6_PMI,           0 ), /* Deassert PMI */
  SPM_SEQ_PWR_CTL( QDSP6_CLOCK,         0 ), /* Deassert QDSP6 Clock Off */
  SPM_SEQ_END_CTL( 0,                   0 ), /* End of Program */
};

/**
 * @brief SPM command sequence for MSS Power Collapse.
 *
 *        From Table 4-6 of QDSP6SSv56 HPG. APCR Long Sequence
 */
static uint8 spmCmdSeqFullPcOrApcr[] =
{
  /* index 0x0 */
  SPM_SEQ_PWR_CTL( QDSP6_CLOCK,         1 ), /* QDSP6 Clock Off */
  SPM_SEQ_EVENT_CTL( EVENT_2,           0 ), /* Wait for clock off event */
  SPM_SEQ_PWR_CTL( QDSP6_CLAMP,         0 ), /* Assert clamp */
  SPM_SEQ_PWR_CTL( QDSP6_RETAIN_SAVE,   0 ), /* Assert QDSP6 retain save*/

  /* index 4 */
  SPM_SEQ_PWR_CTL( QDSP6_RETAIN_SAVE,   0 ), /* Deassert QDSP6 retain save */
  SPM_SEQ_PWR_CTL( QDSP6_CLAMP_WL,      0 ), /* Assert QDSP6 Clamp WL */
  SPM_SEQ_PWR_CTL( QDSP6_MEM_SRC_BIAS,  0 ), /* Assert memory biasing */
  SPM_SEQ_PWR_CTL( QDSP6_MEM_RET,       0 ), /* Assert memory retention */

  /* index 8 */
  SPM_SEQ_PWR_CTL( QDSP6_MEM_SLEEP,     1 ), /* Assert memory non-retention sleep */
  SPM_SEQ_PWR_CTL( QDSP6_QMC_MEM_CLAMP, 0 ), /* Assert Q6 memory clamp */
  SPM_SEQ_PWR_CTL( QDSP6_LDO_BYPASS,    0 ), /* Assert LDO bypass */
  SPM_SEQ_PWR_CTL( QDSP6_BHS,           0 ), /* Assert BHS off */

  /* index 12 */
  SPM_SEQ_PWR_CTL( QDSP6_LDO,           0 ), /* Assert LDO off */
  SPM_SEQ_PWR_CTL( QDSP6_XO_CLOCK,      0 ), /* MSS switch clock to XO */
  SPM_SEQ_PWR_CTL( QDSP6_PLL_OUTPUT,    0 ), /* Disable Q6 PLL outputs */
  SPM_SEQ_PWR_CTL( QDSP6_PLL_RESET,     0 ), /* Reset Q6 PLL */

  /* index 16 */
  SPM_SEQ_PWR_CTL( QDSP6_PLL_BYPASS,    0 ), /* Bypass Q6 PLL */
  SPM_SEQ_PWR_CTL( QDSP6_PLL_FREEZE,    0 ), /* Freeze Q6 PLL */
  SPM_SEQ_SLP_CTL( NO_RPM_HS,           0 ), /* Sleep w/o RPM handshake */
  SPM_SEQ_PWR_CTL( QDSP6_RESET,         0 ), /* Assert QDSP6 reset */

  /* index 20 */
  SPM_SEQ_PWR_CTL( QDSP6_PLL_FREEZE,    0 ), /* Unfreeze Q6 PLL */
  SPM_SEQ_PWR_CTL( QDSP6_PLL_BYPASS,    2 ), /* Disable Q6 PLL bypass */
  SPM_SEQ_PWR_CTL( QDSP6_PLL_RESET,     0 ), /* Release Q6 PLL from reset */
  SPM_SEQ_PWR_CTL( QDSP6_PLL_OUTPUT,    0 ), /* Enable Q6 PLL outputs */
  
  /* index 24 */
  SPM_SEQ_PWR_CTL( QDSP6_XO_CLOCK,      0 ), /* MSS switch back clock from XO */
  SPM_SEQ_PWR_CTL( QDSP6_LDO,           0 ), /* Deassert LDO force off */
  SPM_SEQ_PWR_CTL( QDSP6_BHS,           1 ), /* Deassert BHS off, 1 us min delay is req'd */
  SPM_SEQ_PWR_CTL( QDSP6_LDO_BYPASS,    0 ), /* Deassert LDO bypass force off */

  /* index 28 */
  SPM_SEQ_PWR_CTL( QDSP6_QMC_MEM_CLAMP, 0 ), /* Deassert Q6 QMC memory clamp */
  SPM_SEQ_PWR_CTL( QDSP6_MEM_SLEEP,     1 ), /* Deassert Q6 memory sleep + pipeline delay to assert L2 */
  SPM_SEQ_PWR_CTL( QDSP6_MEM_RET, 1 ), /* Deassert Q6 memory retention sleep + delay */
  SPM_SEQ_PWR_CTL( QDSP6_MEM_SRC_BIAS,  0 ), /* Disable Q6 memory source biasing */

  /* index 32 */
  SPM_SEQ_PWR_CTL( QDSP6_CLAMP_WL,      0 ), /* Assert Q6 clamp_wl */  
  SPM_SEQ_PWR_CTL( QDSP6_RETAIN_RESTORE,0 ), /* Assert Q6 restore */
  SPM_SEQ_PWR_CTL( QDSP6_RETAIN_RESTORE,0 ), /* De-assert Q6 restore */
  SPM_SEQ_PWR_CTL( QDSP6_CLAMP,         0 ), /* Deassert Q6 clamp */

  /* index 36 */
  SPM_SEQ_PWR_CTL( QDSP6_RESET,         0 ), /* Deassert Q6 reset */
  SPM_SEQ_PWR_CTL( QDSP6_PMI,           0 ), /* Assert SPM wakeup IRQ */
  SPM_SEQ_PWR_CTL( QDSP6_PMI,           0 ), /* De-assert SPM wakeup IRQ */
  SPM_SEQ_EVENT_CTL( EVENT_0,           0 ), /* Wait for Q6 PLL lock */

  /* index 40 */
  SPM_SEQ_PWR_CTL( QDSP6_CLOCK,         0 ), /* Q6 Clock on */
  SPM_SEQ_END_CTL( 0,                   0 )  /* End of command sequence */
};

/**
 * @brief Array containing SPM command sequences for supported low power
 *        modes and associated information.
 */
BSP_spm_CmdSequenceType devcfgSpmCmdSeqArray[][SPM_NUM_LOW_POWER_MODES] =
{
  /* MSS has only one core */
  {
    { 
      SPM_MODE_APCR_PLL_ON,
      spmCmdSeqAPCRPLLOn,
      sizeof(spmCmdSeqAPCRPLLOn), 
      11, /* Index of the RPM handshake cmd in the command sequence array */
      -1
    },
    {
      SPM_MODE_PWRC_BHS,
      spmCmdSeqFullPcOrApcr,
      sizeof(spmCmdSeqFullPcOrApcr),
      18, /* Index of the RPM handshake cmd in the command sequence array */
      -1
    }
  }
};
