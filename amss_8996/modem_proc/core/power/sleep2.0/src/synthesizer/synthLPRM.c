/*==============================================================================
  FILE:         synthLPRM.c
  
  OVERVIEW:     Provides the synth lprm functions
 
  DEPENDENCIES: None

                Copyright (c) 2013-2015 Qualcomm Technologies, Inc. (QTI).
                All Rights Reserved.
                Qualcomm Technologies Confidential and Proprietary
================================================================================
$Header: //components/rel/core.mpss/3.4.c3.11/power/sleep2.0/src/synthesizer/synthLPRM.c#1 $
$DateTime: 2016/03/28 23:02:17 $
==============================================================================*/
#include <stdlib.h>
#include "sleepi.h"
#include "sleep_statsi.h"
#include "sleep_lpri.h"
#include "synthLPRM.h"
#include "synthLPR.h"
#include "CoreAtomicOps.h"
#include "lookup_table_types.h"
#include "sleep_qr.h"

/*==============================================================================
                           FUNCTION DEFINITIONS
 =============================================================================*/
/*
 * synthLPRM_setupComponentModeParents
 */
void synthLPRM_setupComponentModeParents( sleep_synth_lprm *synthLPRM )
{
  uint32      i;
  uint32      newParentIdx;
  sleep_lprm  *sleepLPRM;
  void        *tmpPtr;
  
  CORE_VERIFY_PTR(synthLPRM);

  /* Iterating through each component mode */
  for( i = 0; i < synthLPRM->num_component_modes; i++ )
  {
    sleepLPRM     = synthLPRM->component_modes[i];
    newParentIdx  = sleepLPRM->synth_LPRM_parent_count++;

    CORE_VERIFY_PTR(tmpPtr = realloc(sleepLPRM->synth_LPRM_parent_array,
                                     sizeof(void *) * sleepLPRM->synth_LPRM_parent_count));

    sleepLPRM->synth_LPRM_parent_array = tmpPtr;

    /* Set the component mode's parent to this synthmode */
    sleepLPRM->synth_LPRM_parent_array[newParentIdx] = synthLPRM;
  }

  return;
}

/*
 * synthLPRM_initSynth
 */
void synthLPRM_initSynth(sleep_synth_lprm *synthLPRM)
{
  CORE_LOG_VERIFY_PTR( synthLPRM, sleepLog_printf(SLEEP_LOG_LEVEL_ERROR, 0,
                       "ERROR (message: \"NULL Synthesized LPRM\")" ) );

  /* Default is synthmode disabled */
  synthLPRM->mode_status = SLEEP_DISABLED;
  synthLPRM_setupComponentModeParents(synthLPRM);

  return;
}

/*
 * synthLPRM_getLPRMLatency
 */
uint32 synthLPRM_getLPRMLatency(sleep_synth_lprm  *synthLPRM,
                                uint32            mLUTidx)
{
  CORE_VERIFY_PTR(synthLPRM);
  CORE_VERIFY(mLUTidx < SLEEP_NUM_STATIC_FREQ);

  /* Since enter & exit latencies are not dynamically adjusted,
   * always return the appropriate seed value */
  return synthLPRM->seed_latency->enter_exit[mLUTidx];
}

/*
 * synthLPRM_getBackOffTime
 */
const uint32* synthLPRM_getBackOffTime(sleep_synth_lprm  *synthLPRM,
                                       sleep_fLUT_node   *fLUT)
{
  const uint32* backoffPtr = NULL;

#ifdef SLEEP_ENABLE_AUTO_SYNTH_BACKOFF_ADJUSTMENT
  uint32  dynIndex;

  CORE_VERIFY_PTR(synthLPRM);

  dynIndex = synthLPRM->dynamic_num;

  /* If the backoff adjustment feature is enabled, then get the backoff value
   * from the frequency dependant list */
  backoffPtr = &(fLUT->dynamic_data.synth.LPRM[dynIndex].backoff);

#else

  CORE_VERIFY_PTR(synthLPRM);
  /* If the backoff feature is disabled, then get the value from the initial
   * seed value(s) given */
  backoffPtr = &(synthLPRM->seed_latency->backoff[fLUT->mLUT_idx]);

#endif

  return backoffPtr;
}

/*
 * synthLPRM_isCacheable
 */
boolean synthLPRM_isCacheable( sleep_synth_lprm  *synthLPRM )
{
  return ( ( synthLPRM->attr & SYNTH_MODE_ATTR_LPR_CACHEABLE ) ==
           SYNTH_MODE_ATTR_LPR_CACHEABLE );
}

/*
 * synthLPRM_cachedModeEnter
 */
void synthLPRM_cachedModeEnter(sleep_synth_lprm  *synthLPRM)
{
  sleep_lprm  *component;
  uint32      cached;
  int         nMode;
  
  CORE_VERIFY_PTR(synthLPRM);
  CORE_VERIFY(TRUE != sleepOS_isProcessorInSTM());

  /* In cached mode, wakeup_tick is meaningless, set to max duration */
  synthLPRM->wakeup_tick = UINT64_MAX;

  /* Iterating through each component mode */
  for(nMode = 0; nMode < synthLPRM->num_component_modes; nMode++)
  {
    /* Calling the actual enter functions of the component modes */
    component = synthLPRM->component_modes[nMode];
    cached    = component->attributes & SLEEP_MODE_ATTR_LPR_CACHEABLE;

    /* Call the enter function */
    if((NULL != component->enter_func) && (0 != cached))
    {
      sleepLog_QDSSPrintf(SLEEP_LOG_LEVEL_PROFILING, 
                          SLEEP_ATS_ENTER_MODE_NUM_ARGS,
                          SLEEP_ATS_ENTER_MODE_STR, 
                          SLEEP_ATS_ENTER_MODE, 
                          component->parent_LPR->resource_name,
                          component->mode_name);
      
      component->enter_func(UINT64_MAX);
    }
  }

  return;
}

/*
 * synthLPRM_enter
 */
void synthLPRM_enter(sleep_synth_lprm  *synthLPRM, 
                     uint64            wakeupTick, 
                     sleep_fLUT_node   *fLUT)
{
  sleep_lprm  *component;
  uint64      currentTime;
  int         i;
  uint64      LPRMTime   = CoreTimetick_Get64();
  
  CORE_VERIFY_PTR(synthLPRM);

  /* setup initial value */
  synthLPRM->wakeup_tick = wakeupTick;

  /* Iterating through each component mode */
  for(i = 0; i < synthLPRM->num_component_modes; i++)
  {
    /* Calling the actual enter functions of the component modes */
    component   = synthLPRM->component_modes[i];
    currentTime = 0;

    /* Sleep statistics */
    component->parent_LPR->run_count++;                   /* Update LPR cycles*/
    component->mode_statistics.mode_run_cycles++;         /* Update LPRM cycles */
    component->mode_statistics.last_mode_time = LPRMTime; /* Recored LPRM enter time */

    /* Logging mode entry message */
    sleepLog_QDSSPrintf( SLEEP_LOG_LEVEL_PROFILING, 
                         SLEEP_ENTER_MODE_NUM_ARGS,
                         SLEEP_ENTER_MODE_STR, SLEEP_ENTER_MODE, 
                         component->parent_LPR->resource_name,
                         component->mode_name,
                         ULOG64_DATA(LPRMTime));

    /* Call the enter function with the latest sleep duration value */
    if(NULL != component->enter_func)
    {
      component->enter_func(synthLPRM->wakeup_tick);
    }

    /* Get the end of the current LPRM exit, and also use this as the
     * beginning of the next LPRM exit so that multiple calls to timetick
     * are avoided. */
    currentTime = CoreTimetick_Get64();

#ifdef SLEEP_ENABLE_AUTO_LPR_PROFILING
    /* Record LPR time for profiling feature */
    if((component->attributes & SLEEP_MODE_ATTR_NO_AUTO_LATENCY_TRACK) &&
       (NULL != component->enter_func))
    {
      LPRMTime = sleepStats_getLastLprLatency(SLEEP_STATS_ENTER_LATENCY_TYPE);
    }
    else
    {
      LPRMTime = currentTime - LPRMTime;
    }

    /* Update statistics for enter function */
    sleepStats_updateValue(
        &(fLUT->dynamic_data.sleep_LPRM[component->dynamic_num].enter), LPRMTime);
#endif

    /* To avoid multiple calls to get the current time use the point after
     * the exit function returns, which is close enough to the start of the
     * next LRPM exit for statistical purposes. */
    if(0 != currentTime)
    {
      LPRMTime = currentTime;
    }
    else
    {
      LPRMTime = CoreTimetick_Get64();
    }
  }

  return;
}

/*
 * synthLPRM_cachedModeExit
 */
void synthLPRM_cachedModeExit(sleep_synth_lprm *synthLPRM)
{
  int         nMode;
  uint32      cached;
  sleep_lprm  *component;
  
  CORE_VERIFY_PTR(synthLPRM);
  CORE_VERIFY(TRUE != sleepOS_isProcessorInSTM());

  for(nMode = synthLPRM->num_component_modes - 1; nMode >=0; nMode--)
  {
    /* Calling the exit functions of the component modes */
    component = synthLPRM->component_modes[nMode];
    cached    = component->attributes & SLEEP_MODE_ATTR_LPR_CACHEABLE;
    
    /* Call LRPM exit function */
    if((NULL != component->exit_func) && (0 != cached))
    {
      sleepLog_QDSSPrintf(SLEEP_LOG_LEVEL_PROFILING, 
                          SLEEP_ATS_EXIT_MODE_NUM_ARGS,
                          SLEEP_ATS_EXIT_MODE_STR, 
                          SLEEP_ATS_EXIT_MODE, 
                          component->parent_LPR->resource_name,
                          component->mode_name);

      component->exit_func();
    }
  }

  return;
}

/*
 * synthLPRM_exit
 */
void synthLPRM_exit( sleep_synth_lprm *synthLPRM,
                     sleep_fLUT_node  *fLUT)
{
  int         index;
  sleep_lprm  *component;
  uint32      noAutoLat;
  uint64      currentTime;
  uint64      LPRMTime = CoreTimetick_Get64();
  
  CORE_VERIFY_PTR(synthLPRM);

  for(index = synthLPRM->num_component_modes - 1; index >=0; index--)
  {
    /* Calling the exit functions of the component modes */
    component   = synthLPRM->component_modes[index];
    noAutoLat   = component->attributes & SLEEP_MODE_ATTR_NO_AUTO_LATENCY_TRACK;
    currentTime = 0;

    sleepLog_QDSSPrintf( SLEEP_LOG_LEVEL_PROFILING, 
                         SLEEP_EXIT_MODE_NUM_ARGS,
                         SLEEP_EXIT_MODE_STR, SLEEP_EXIT_MODE, 
                         component->parent_LPR->resource_name,
                         component->mode_name,
                         ULOG64_DATA(LPRMTime));

    /* Call LRPM exit function */
    if(NULL != component->exit_func)
    {
      component->exit_func();
    }

    /* Get the end of the current LPRM exit, and also use this as the
     * beginning of the next LPRM exit so that multiple calls to timetick
     * are avoided. */
    currentTime = CoreTimetick_Get64();

    /* Do statistic calculations */
    if((0 != noAutoLat) && (NULL != component->exit_func))
    {
      LPRMTime = sleepStats_getLastLprLatency(SLEEP_STATS_BACKOFF_LATENCY_TYPE);
    }
    else
    {
      LPRMTime = currentTime - LPRMTime;
    }

    /* Record the backoff time for QR handling */
    sleepQR_setComponentCycleBackoff(component, LPRMTime, &fLUT->dynamic_data);

#ifdef SLEEP_ENABLE_AUTO_LPR_PROFILING
    /* Add backoff statistics */
    sleepStats_updateValue(
        &(fLUT->dynamic_data.sleep_LPRM[component->dynamic_num].backoff), LPRMTime);
#endif

    /* calculate last time in mode stats */
    component->mode_statistics.last_mode_time = currentTime - 
      component->mode_statistics.last_mode_time;

    /* Keep running total of mode time */
    component->mode_statistics.in_mode_time += 
      component->mode_statistics.last_mode_time;

    /* To avoid multiple calls to get the current time use the point after
     * the exit function returns, which is close enough to the start of the
     * next LRPM exit for statistical purposes. */
    if(0 != currentTime)
    {
      LPRMTime = currentTime;
    }
    else
    {
      LPRMTime = CoreTimetick_Get64();
    }
  }

  return;
}

/*
 * synthLPRM_update
 */
void synthLPRM_update( sleep_synth_lprm *synthLPRM,
                       sleep_lprm       *changedSleepLPRM )
{
  sleep_status oldStatus;
  
  CORE_VERIFY_PTR(synthLPRM);
  CORE_VERIFY_PTR(changedSleepLPRM);

  oldStatus = synthLPRM->mode_status;

  /* Update the number of enabled modes */
  if( TRUE == changedSleepLPRM->mode_enabled )
    synthLPRM->num_enabled_component_modes++;
  else
    synthLPRM->num_enabled_component_modes--;

  /* Make sure the number of enabled component modes is not out of range. */
  CORE_VERIFY( synthLPRM->num_enabled_component_modes >= 0 &&
               (synthLPRM->num_enabled_component_modes <= 
                synthLPRM->num_component_modes) );
  
  if(synthLPRM->num_enabled_component_modes == synthLPRM->num_component_modes)
  {
    synthLPRM->mode_status = SLEEP_ENABLED;
  }
  else
  {
    synthLPRM->mode_status = SLEEP_DISABLED;
  }

  /* If we changed state, check for a parent and update it if we have one */
  if(synthLPRM->mode_status != oldStatus)
  {
    synthLPR_modeUpdated(synthLPRM);
  }

  return;
}

