/*============================================================================
  FILE:         msa_wa_lpr.c

  OVERVIEW:     This file provides the LPR definition for the MSA hardware
                workaround

  DEPENDENCIES: None

                Copyright (c) 2016 Qualcomm Technologies, Inc. (QTI).
                All Rights Reserved.
                Qualcomm Technologies Confidential and Proprietary
================================================================================
$Header: //components/rel/core.mpss/3.4.c3.11/power/sleep2.0/src/lpr/msa_wa_lpr.c#1 $
$DateTime: 2016/03/28 23:02:17 $
==============================================================================*/
#include "comdef.h"
#include "HALhwio.h"
#include "HALsleep_hwio.h"

/*==============================================================================
                               GLOBAL FUNCTIONS
 =============================================================================*/
/**
 * msaWALpr_enter
 *
 * @brief Provides a hardware workaround when entering full power collapse
 *        to lock the MSA configuration
 */
void msaWALpr_enter(void)
{
  HWIO_OUTF(MSS_MSA, CONFIG_LOCK, 0x01);
  HWIO_OUTF(MSS_MSA, MBA_OK,      0x01);
  return;
}

/**
 * msaWALpr_exit
 *
 * @brief Releases the MSA lock when exiting full power collapse
 */
void msaWALpr_exit(void)
{
  HWIO_OUTF(MSS_MSA, CONFIG_LOCK, 0x00);
  return;
}
