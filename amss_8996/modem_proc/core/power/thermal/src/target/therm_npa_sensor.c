/*============================================================================
  @file therm_npa_sensor.c

  Define NPA nodes representing sensors.  Provide polling functionality where
  needed.

  Copyright (c) 2011-2015 QUALCOMM Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary
============================================================================*/
/*=======================================================================
$Header: //components/rel/core.mpss/3.4.c3.11/power/thermal/src/target/therm_npa_sensor.c#1 $
$DateTime: 2016/03/28 23:02:17 $
$Author: mplcsds1 $
========================================================================*/
/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "comdef.h"
#include "npa_resource.h"
#include "AdcInputs.h"
#include "DDIAdc.h"
#include "DDITsens.h"
#include "timer.h"
#include "CoreVerify.h"
#include "therm_diag.h"
#include "therm_log.h"
#include "stdarg.h"
#include <stringl/stringl.h>
#include "log.h"
#include "rex.h"
#include "therm_npa_sensor.h"
#include "therm_v.h"

/*=======================================================================

                  STATIC MEMBER / FUNCTION DECLARATIONS / DEFINITIONS

========================================================================*/

/* Use 300 K as default temp */
#define THERM_NPA_DEFAULT_TEMP 300
#define THERM_NPA_MAX_RES_TEMP 528

#define THERM_NPA_SENSOR_MAX 8
#define THERM_NPA_STR_MAX    64

#define THERM_K_TO_C(x) ((int32)(((int32)(x)) - 273))
#define THERM_C_TO_K(x) ((int32)(((int32)(x)) + 273))

#define THERM_NPA_BCL_WARNING             "therm_bcl_warning"
#define THERM_NPA_BCL_DEFAULT_WARNING     0
#define THERM_NPA_MSS_LM_WARNING_STR      "therm_mss_warning"
#define THERM_NPA_MSS_LM_DEFAULT_WARNING  (npa_resource_state)0
#define THERM_NPA_MSS_LM_WARNING          (npa_resource_state)1
#define THERM_TSENS                       "TSENS"
#define THERM_DEGREES_K_STR               "Degrees K"
#define THERM_WARNING_LVL_STR             "Warning Level"

#define NPA_RESOURCE_AT_INDEX( node_ptr, index ) (&((node_ptr)->resources[(index)]))

/* Struct for allowing sync reads using ADC DAL interface. */
typedef struct
{
  AdcResultType *pAdcResult;
  DALSYSEventHandle hCallbackEvent;
  DALSYS_EVENT_OBJECT(callbackEventObject);
  DALSYSEventHandle hSignalEvent;
  DALSYS_EVENT_OBJECT(signalEventObject);
  DALSYSSyncHandle hSync;
  DALSYS_SYNC_OBJECT(syncObject);
} therm_npa_adc_sync_read_obj_type;

/* Internal struct for managing ADC DAL sensor init of read object */
typedef struct
{
  AdcResultType                    readResult;
  therm_npa_adc_sync_read_obj_type readObject;
  AdcInputPropertiesType           properties;
  boolean                          itemInit;
  boolean                          sensorPresent;
} therm_npa_adc_sync_read_type;

/* ADC DAL device handle */
static DalDeviceHandle *phADCDalDevHandle = NULL;

/* Internal struct for managing TSENS DAL sensor init of read object */
typedef struct
{
  boolean                          itemInit;
  boolean                          sensorPresent;
} therm_npa_tsens_sync_read_type;

/* TSENS DAL devince handle */
static DalDeviceHandle *phTSENSDALDevHandle = NULL;
static uint32 numTSensors;

typedef enum
{
  THERM_NPA_HKADC_SENSOR = 0,
  THERM_NPA_TSENS_SENSOR,
  THERM_NPA_LOGICAL_SENSOR,
  THERM_NPA_MAX_SENSOR = THERM_NPA_LOGICAL_SENSOR
} therm_npa_sensor_type;

typedef struct
{
  /* Diag log packet subsystem id */
  unsigned int subsystem_id;
  /* Diag log packet sensor id */
  unsigned int sensor_id;
  /* Units of measurement Eg. Degrees C, .001 Degrees C */
  const char  *units_of_measure;
  /* Nuber of units per whole integer value */
  unsigned int unit_per_whole;
  /* Sensor input name */
  const char  *sensor_input_str;
  /* NPA node name */
  const char  *npa_node_str;
  /* Therm sensor types */
  therm_npa_sensor_type sensor_type;
  /* Sensor type specific data */
  void *sensor_data;
} therm_sensor_info;

static therm_npa_adc_sync_read_type pa0_adc_read_data;

static therm_sensor_info pa0_info =
{
  .subsystem_id = PA_THERM_SS,
  .sensor_id = 0,
  .units_of_measure = THERM_DEGREES_K_STR,
  .unit_per_whole = 1,
  .sensor_input_str = ADC_INPUT_PA_THERM,
  .npa_node_str = "pa",
  .sensor_type = THERM_NPA_HKADC_SENSOR,
  .sensor_data = (void*)&pa0_adc_read_data,
};

static therm_npa_adc_sync_read_type pa1_adc_read_data;

static therm_sensor_info pa1_info =
{
  .subsystem_id = PA_THERM_SS,
  .sensor_id = 1,
  .units_of_measure = THERM_DEGREES_K_STR,
  .unit_per_whole = 1,
  .sensor_input_str = ADC_INPUT_PA_THERM1,
  .npa_node_str = "pa_1",
  .sensor_type = THERM_NPA_HKADC_SENSOR,
  .sensor_data = (void*)&pa1_adc_read_data,
};

static therm_sensor_info modem_bcl_warn_info =
{
  .subsystem_id = LOGICAL_THERM_SS,
  .sensor_id = 0,
  .units_of_measure = THERM_WARNING_LVL_STR,
  .unit_per_whole = 1,
  .sensor_input_str = THERM_NPA_BCL_WARNING,
  .npa_node_str = "modem_bcl_warning",
  .sensor_type = THERM_NPA_LOGICAL_SENSOR,
};

#ifdef THERMAL_9x45
static therm_sensor_info mss_rail_lm_warn_info =
{
  .subsystem_id = LOGICAL_THERM_SS,
  .sensor_id = 1,
  .units_of_measure = THERM_WARNING_LVL_STR,
  .unit_per_whole = 1,
  .sensor_input_str = THERM_NPA_MSS_LM_WARNING_STR,
  .npa_node_str = "mss_rail_lm_warning",
  .sensor_type = THERM_NPA_LOGICAL_SENSOR,
};

static therm_npa_tsens_sync_read_type tsens_sync_read_data;
static therm_sensor_info modem_tsens_info =
{
  .subsystem_id = MSM_THERM_SS,
  .sensor_id = 2,
  .units_of_measure = THERM_DEGREES_K_STR,
  .unit_per_whole = 1,
  .sensor_input_str = THERM_TSENS,
  .npa_node_str = "modem_tsens",
  .sensor_type = THERM_NPA_TSENS_SENSOR,
  .sensor_data = (void*)&tsens_sync_read_data,
};
#endif

/* Structure to maintain the internal state of the DCVS NPA members */
typedef struct
{
  npa_resource_state sampling_time;
} therm_npa_type;

static npa_resource_state therm_sensor_update_fcn( npa_resource *resource,
                                                   npa_client_handle client );
#ifdef THERMAL_9x45
static npa_resource_state therm_mss_lm_update_fcn( npa_resource *resource,
                                                   npa_client_handle client );
#endif

static npa_resource_state therm_logical_max_update_fcn( npa_resource *resource,
                                                        npa_client_handle client );

static npa_resource_state therm_node_driver_fcn( npa_resource *resource,
                                                 npa_client *client,
                                                 npa_resource_state state );

static npa_resource_state therm_sampling_node_driver_fcn( npa_resource *resource,
                                                          npa_client *client,
                                                          npa_resource_state state );


/*=======================================================================

                  GLOBAL DEFINITIONS

========================================================================*/

#ifdef THERMAL_9x45
extern mss_rail_cfg_type mss_rail_cfg;
#endif

/* Supplies sensor list for QMI request for thermal sensor device list.
   Must edit if additional thermal sensors are to be added to QMI sensor
   list response. */
const char *therm_sensor_list[] =
{
  "pa",
  "pa_1",
  "modem_bcl_warning",
#ifdef THERMAL_9x45
  "mss_rail_lm_warning",
#endif
};

const unsigned int therm_sensor_list_count = ARR_SIZE(therm_sensor_list);

/* "/therm/sampling" node and resource */
static npa_resource_definition therm_sampling_resource[] =
{
  {
    "/therm/sampling",                  /* name of resource */
    "Milliseconds",                     /* Units of the resource */
    0xFFFFFFFF,                         /* Maximum value of resource */
    &npa_min_plugin,                    /* Plugin - only handles impulses */
    NPA_RESOURCE_DEFAULT,
    NULL,
    NULL
  }
};

/* Define the dependencies of our sampling resource.  Must edit if additional
   sensors are added */
static npa_node_dependency therm_sampling_deps[] =
{
  { "/therm/sensor/pa",                NPA_NO_CLIENT },
  { "/therm/sensor/pa_1",              NPA_NO_CLIENT },
  { "/therm/sensor/modem_bcl_warning", NPA_NO_CLIENT },
#ifdef THERMAL_9x45
  { "/therm/sensor/mss_rail_lm_warning", NPA_NO_CLIENT },
  { "/therm/sensor/modem_tsens",         NPA_NO_CLIENT },
#endif
};


static npa_node_definition therm_sampling_node =
{
  "/node/therm/sampling",            /* Node name - info only */
  therm_sampling_node_driver_fcn,    /* Driver function for temperature */
  NPA_NODE_DEFAULT,                  /* No attributes */
  NULL,                              /* No User Data */
  NPA_ARRAY(therm_sampling_deps),    /* No Dependencies */
  NPA_ARRAY(therm_sampling_resource) /* Resources */
};

static npa_resource_state initial_sampling_state[] = {10000};

/* The plugin definition for /therm/<sensor> resource */
const npa_resource_plugin therm_sensor_plugin =
{
  therm_sensor_update_fcn,
  NPA_CLIENT_IMPULSE,                            /* Supported client types */
  NULL,                                          /* Create client function */
  NULL                                           /* Destroy client function */
};

/* The plugin definition for logical resource - similar to npa max
 * plugin with added logging */
const npa_resource_plugin therm_sensor_max_plugin =
{
  therm_logical_max_update_fcn,
  NPA_CLIENT_REQUIRED | NPA_CLIENT_SUPPRESSIBLE |
  NPA_CLIENT_LIMIT_MAX | NPA_CLIENT_IMPULSE,     /* Supported Client types */
  NULL,                                          /* Create client function */
  NULL                                           /* Destroy client function */
};

#ifdef THERMAL_9x45
/* The plugin definition for logical resource mss_rail_lm_warning -
   using properties of similar to npa max
 * plugin with added logging */
const npa_resource_plugin therm_mss_lm_warning_plugin =
{
  therm_mss_lm_update_fcn,
  NPA_CLIENT_REQUIRED | NPA_CLIENT_SUPPRESSIBLE |
  NPA_CLIENT_LIMIT_MAX | NPA_CLIENT_IMPULSE,     /* Supported Client types */
  NULL,                                          /* Create client function */
  NULL                                           /* Destroy client function */
};
#endif

/* "therm" node and resource
   Must edit if additional sensors are added.

   Place all actual physical resources together at the beginning and
   logical/virtual resources at the end.
 */
static npa_resource_definition therm_sensor_resource[] =
{
  /* Physical Resources */
  {
    "/therm/sensor/pa",                        /* name of resource */
    "Degrees K",                               /* Units of the resource */
    THERM_NPA_MAX_RES_TEMP,                    /* Maximum value of resource */
    &therm_sensor_plugin,
    NPA_RESOURCE_DEFAULT,
    (npa_user_data)&pa0_info,
    NULL
  },
  {
    "/therm/sensor/pa_1",                      /* name of resource */
    "Degrees K",                               /* Units of the resource */
    THERM_NPA_MAX_RES_TEMP,                    /* Maximum value of resource */
    &therm_sensor_plugin,
    NPA_RESOURCE_DEFAULT,
    (npa_user_data)&pa1_info,
    NULL
  },
  /* Logical Resources */
  {
    "/therm/sensor/modem_bcl_warning",         /* name of resource */
    "Warning Level",                           /* Unit of the resource */
    100,                                       /* Maximum value of resource */
    &therm_sensor_max_plugin,                  /* Plugin - Max Aggregation */
    NPA_RESOURCE_DEFAULT,                      /* Resource attribute */
    (npa_user_data)&modem_bcl_warn_info,       /* Resource specific data */
    NULL                                       /* Default query function */
  },
#ifdef THERMAL_9x45
  {
    "/therm/sensor/mss_rail_lm_warning",       /* name of resource */
    "Warning Level",                           /* Unit of the resource */
    THERM_NPA_MAX_RES_TEMP,                    /* Maximum value of resource */
    &therm_mss_lm_warning_plugin,              /* Plugin - Max Aggregation */
    NPA_RESOURCE_DEFAULT,                      /* Resource attribute */
    (npa_user_data)&mss_rail_lm_warn_info,     /* Resource specific data */
    NULL                                       /* Default query function */
  },
  /* Tsens Resources */
  {
    "/therm/sensor/modem_tsens",               /* name of resource */
    "Degrees K",                               /* Units of the resource */
    THERM_NPA_MAX_RES_TEMP,                    /* Maximum value of resource */
    &therm_sensor_plugin,
    NPA_RESOURCE_DEFAULT,
    (npa_user_data)&modem_tsens_info,
    NULL
  },
#endif
};

/* Must edit if additional sensors are added */
static npa_resource_state initial_sensor_state[] =
{
  /* Initial state for ADC resources */
  THERM_NPA_DEFAULT_TEMP,
  THERM_NPA_DEFAULT_TEMP,

  /* Initial state for logical resources */
  THERM_NPA_BCL_DEFAULT_WARNING,
#ifdef THERMAL_9x45
  THERM_C_TO_K(THERM_NPA_BCL_DEFAULT_WARNING),

  /* Initial state for TSENS resources */
  THERM_NPA_DEFAULT_TEMP,
#endif
};


npa_node_definition therm_sensor_node =
{
  "/node/therm/sensor",          /* Node name - info only */
  therm_node_driver_fcn,         /* Driver function for temperature */
  NPA_NODE_DEFAULT,              /* No attributes */
  NULL,                          /* No User Data */
  NPA_EMPTY_ARRAY,               /* No Dependencies */
  NPA_ARRAY(therm_sensor_resource) /* Resources */
};

static npa_client_handle sensor_client_handle[ARR_SIZE(therm_sensor_resource)];

static timer_type  sampling_timer;

static npa_resource_state sampling_time;

extern rex_tcb_type thermal_tcb;

/*=======================================================================

                  LOCAL FUNCTION DEFINITIONS

========================================================================*/
/**
  @brief therm_npa_log_reading

  Log sensor reading to QXDM.

  @param  :  res_index: resource index.
             state: Thermal resource state to report over QXDM.

  @return : None
*/
static void therm_npa_log_reading( unsigned int res_index,
                                   int          state )
{
  PACK(void *)            log_ptr = NULL;
  diag_therm_packet       temp_data;
  const char              *debug_log_str = NULL;
  const char              *stats_log_str = NULL;
  npa_resource_definition *resource = NPA_RESOURCE_AT_INDEX( &therm_sensor_node, res_index );
  therm_sensor_info       *sensor_info = (therm_sensor_info *)resource->data;

  memset(&temp_data, '\0', sizeof(temp_data));

  temp_data.version = 0x1;
  temp_data.num_samples = 1;

  temp_data.samples[0].subsystem_id = sensor_info->subsystem_id;
  temp_data.samples[0].sensor_id    = sensor_info->sensor_id;
  temp_data.samples[0].temp         = state;

  /* Setting log string as per resource - Make sure to provide same number of
   * format specifiers (%d, %s, ..) */
  if( THERM_NPA_LOGICAL_SENSOR == sensor_info->sensor_type )
  {
    debug_log_str = "THERM_NPA read ss_id %d %s, sensor_id %d, State %d";
    stats_log_str = "State ( %s:%d )";
  }
  /* Add checks for other type of resources as required */
  else
  {
    debug_log_str = "THERM_NPA read ss_id %d %s, sensor_id %d, TEMP %d C";
    stats_log_str = "Temperature ( %s:%d )";
  }

  therm_log_printf(THERM_LOG_LEVEL_DEBUG, 4,
                   debug_log_str,
                   temp_data.samples[0].subsystem_id,
                   sensor_info->npa_node_str,
                   temp_data.samples[0].sensor_id,
                   temp_data.samples[0].temp);

  therm_log_printf(THERM_LOG_LEVEL_STATS, 2,
                   stats_log_str,
                   sensor_info->npa_node_str,
                   temp_data.samples[0].temp);

  /* Only send out a log packet if we have samples to report on. */
  log_ptr = log_alloc(LOG_TEMPERATURE_MONITOR_C, sizeof(temp_data) + sizeof(log_hdr_type));

  if (log_ptr)
  {
    memscpy((uint8 *)log_ptr + sizeof(log_hdr_type), sizeof(temp_data), 
            &temp_data, sizeof(temp_data));
    log_commit(log_ptr);
  }
}

/**
  @brief therm_npa_adc_sync_read_complete_cb

  Read complete CB func of type DALSYSCallbackFunc.

  @param  :  pObj: Internal therm NPA adc read object data.
             dwParam: Unused
             pPayload: AdcResultType data.
             nPayloadSize: Unused

  @return : None
*/
static void therm_npa_adc_sync_read_complete_cb(
                                         therm_npa_adc_sync_read_obj_type *pObj,
                                         uint32 dwParam,
                                         void *pPayload,
                                         uint32 nPayloadSize)
{
  AdcResultType *pAdcResult = pPayload;

  DALSYS_memcpy(pObj->pAdcResult, pAdcResult, sizeof(AdcResultType));
  DALSYS_EventCtrl(pObj->hSignalEvent, DALSYS_EVENT_CTRL_TRIGGER);
}

/**
  @brief therm_npa_adc_sync_read_perform

  Peforms sync/blocking read of sensor.

  @param  :  pObj: Internal therm NPA adc read object data.
             pAdcInputProps: Input properties struct of sensor to read.
             pAdcResult: Pointer to address to place read result.

  @return : DALResult
*/
static DALResult therm_npa_adc_sync_read_perform(
                                        therm_npa_adc_sync_read_obj_type *pObj,
                                        AdcInputPropertiesType *pAdcInputProps,
                                        AdcResultType *pAdcResult)
{
  AdcRequestParametersType adcParams;
  DALResult result = DAL_ERROR;

  DALSYS_SyncEnter(pObj->hSync);
  pObj->pAdcResult = pAdcResult;
  adcParams.hEvent = pObj->hCallbackEvent;
  adcParams.nDeviceIdx = pAdcInputProps->nDeviceIdx;
  adcParams.nChannelIdx = pAdcInputProps->nChannelIdx;
  DALSYS_EventCtrl(pObj->hSignalEvent, DALSYS_EVENT_CTRL_RESET);
  result = DalAdc_RequestConversion(phADCDalDevHandle, &adcParams, NULL);
  (void)DALSYS_EventWait(pObj->hSignalEvent);
  DALSYS_SyncLeave(pObj->hSync);

  return result;
}

/**
  @brief therm_npa_adc_sync_read_obj_init

  Init DAL ADC read obj.

  @param  : phDev: DAL handle for ADC device driver.
            pObj: Internal therm NPA adc read object data.

  @return : DALResult
*/
static DALResult therm_npa_adc_sync_read_obj_init(DalDeviceHandle *phDev,
                                          therm_npa_adc_sync_read_obj_type *pObj)
{
  DALResult result;

  DALSYS_memset(pObj, 0, sizeof(therm_npa_adc_sync_read_obj_type));

  result = DALSYS_EventCreate(DALSYS_EVENT_ATTR_CALLBACK_EVENT,
                              &pObj->hCallbackEvent,
                              &pObj->callbackEventObject);

  if(result != DAL_SUCCESS)
  {
     return result;
  }

  result = DALSYS_SetupCallbackEvent(
     pObj->hCallbackEvent,
     (DALSYSCallbackFunc)therm_npa_adc_sync_read_complete_cb,
     pObj);

  if(result != DAL_SUCCESS)
  {
     return result;
  }

  result = DALSYS_EventCreate(DALSYS_EVENT_ATTR_CLIENT_DEFAULT,
     &pObj->hSignalEvent,
     &pObj->signalEventObject);

  if(result != DAL_SUCCESS)
  {
     return result;
  }

  result = DALSYS_SyncCreate(DALSYS_SYNC_ATTR_RESOURCE,
                             &pObj->hSync,
                             &pObj->syncObject);
  if(result != DAL_SUCCESS)
  {
     return result;
  }

  return DAL_SUCCESS;
}

/**
  @brief therm_npa_adc_sync_read_init

  Init ADC DAL read objects.

  @param  : res_index: resource index.

  @return : TRUE or FALSE
*/
static boolean therm_npa_adc_sync_read_init(unsigned int res_index)
{
  DALResult result;
  DALBOOL ret_val    = FALSE;
  char   *input_name = NULL;
  uint32  input_name_size = 0;
  npa_resource_definition *resource = NPA_RESOURCE_AT_INDEX( &therm_sensor_node, res_index );
  therm_sensor_info       *sensor_info = (therm_sensor_info *)resource->data;
  therm_npa_adc_sync_read_type *adc_sync_read_data = (therm_npa_adc_sync_read_type *)sensor_info->sensor_data;

  do
  {
    input_name = (char*)sensor_info->sensor_input_str;
    input_name_size = strlen(sensor_info->sensor_input_str);

    result = DalAdc_GetAdcInputProperties(phADCDalDevHandle, input_name,
                                          input_name_size,
                                          &adc_sync_read_data->properties);

    if(result != DAL_SUCCESS)
    {
      therm_log_printf(THERM_LOG_LEVEL_ERROR, 1,
                       "DalAdc_GetAdcInputProperties FAIL %d", result);
      break;
    }

    result = therm_npa_adc_sync_read_obj_init(phADCDalDevHandle,
                                              &adc_sync_read_data->readObject);
    if(result != DAL_SUCCESS)
    {
      therm_log_printf(THERM_LOG_LEVEL_ERROR, 1, "Init read obj FAIL %d", result);
      break;
    }
    ret_val = TRUE;
  } while (0);

  return ret_val;
}

/**
  @brief therm_npa_adc_sync_read

  This function is used to perform sensor reads.

  @param  : res_index : resource index.

  @return : Sensor reading.
*/
static npa_resource_state therm_npa_adc_sync_read(unsigned int res_index)
{
  static DALBOOL adc_init = FALSE;
  npa_resource_state therm_state = THERM_NPA_DEFAULT_TEMP;
  DALResult result;
  int32 nPhysical = 0;
  npa_resource_definition *resource = NPA_RESOURCE_AT_INDEX( &therm_sensor_node, res_index );
  therm_sensor_info       *sensor_info = (therm_sensor_info *)resource->data;
  therm_npa_adc_sync_read_type *adc_sync_read_data = (therm_npa_adc_sync_read_type *)sensor_info->sensor_data;

  if (adc_init == FALSE)
  {
    /* One time init. */
    DALResult result;
    unsigned int idx;

    DALSYS_InitMod(NULL);

    result = DAL_AdcDeviceAttach((DALDEVICEID)DALDEVICEID_ADC,
                                 &phADCDalDevHandle);
    if (result != DAL_SUCCESS)
    {
      therm_log_printf(THERM_LOG_LEVEL_ERROR, 1, "DAL_AdcDeviceAttach FAIL %d",
                       result);

      /* Mark all sensors as init'd and not present. */
      for (idx = 0;
           (idx < ARR_SIZE(therm_sampling_resource)) && (idx < THERM_NPA_SENSOR_MAX);
           idx++)
      {
        npa_resource_definition *res = NPA_RESOURCE_AT_INDEX( &therm_sensor_node, idx );
        therm_sensor_info       *sensor = (therm_sensor_info *)res->data;

        if (THERM_NPA_HKADC_SENSOR == sensor->sensor_type)
        {
          therm_npa_adc_sync_read_type *adc_data =
            (therm_npa_adc_sync_read_type *)sensor->sensor_data;
          adc_data->itemInit = TRUE;
          adc_data->sensorPresent = FALSE;;
        }
      }
    }

    adc_init = TRUE;
  }

  if (adc_sync_read_data->itemInit == FALSE)
  {
    /* Init ADC sync read object data for each signal. */
    adc_sync_read_data->sensorPresent =
      therm_npa_adc_sync_read_init(res_index);
    adc_sync_read_data->itemInit = TRUE;
  }

  do
  {
    if(adc_sync_read_data->sensorPresent == FALSE)
    {
      break;
    }

    result = therm_npa_adc_sync_read_perform(&adc_sync_read_data->readObject,
                                             &adc_sync_read_data->properties,
                                             &adc_sync_read_data->readResult);

    if(result != DAL_SUCCESS)
    {
      break;
    }

    /* Convert to whole units */
    nPhysical = adc_sync_read_data->readResult.nPhysical /
                ((int)sensor_info->unit_per_whole);

    /* Convert units to K. */
    therm_state =  (npa_resource_state)THERM_C_TO_K(nPhysical);

  } while (0);
  return therm_state;
}

/**
  @brief therm_npa_tsens_sync_read

  This function is used to perform sensor reads.

  @param  : res_index : resource index.

  @return : Sensor reading.
*/
static npa_resource_state therm_npa_tsens_sync_read(unsigned int res_index)
{
  static boolean init = FALSE;
  npa_resource_definition *resource = NPA_RESOURCE_AT_INDEX( &therm_sensor_node, res_index );
  therm_sensor_info       *sensor_info = (therm_sensor_info *)resource->data;
  therm_npa_tsens_sync_read_type *tsens_sync_read_data =
    (therm_npa_tsens_sync_read_type *)sensor_info->sensor_data;
  TsensTempType tsensTemp = {0};
  npa_resource_state therm_state = THERM_NPA_DEFAULT_TEMP;

  if (!init)
  {
    DALSYS_InitMod(NULL);

    if (DAL_SUCCESS !=
          DAL_TsensDeviceAttach(DALDEVICEID_TSENS, &phTSENSDALDevHandle))
    {
      uint32 idx;
      therm_log_printf(THERM_LOG_LEVEL_ERROR, 0, "DAL_TsensDeviceAttach FAIL");

      /* Mark all TSENS sensors as init'd and not present. */
      for (idx = 0;
           (idx < ARR_SIZE(therm_sampling_resource)) && (idx < THERM_NPA_SENSOR_MAX);
           idx++)
      {
        npa_resource_definition *res = NPA_RESOURCE_AT_INDEX( &therm_sensor_node, idx );
        therm_sensor_info       *sensor = (therm_sensor_info *)res->data;

        if (THERM_NPA_TSENS_SENSOR == sensor->sensor_type)
        {
          therm_npa_tsens_sync_read_type *tsens_data =
            (therm_npa_tsens_sync_read_type *)sensor->sensor_data;
          tsens_data->itemInit = TRUE;
          tsens_data->sensorPresent = FALSE;;
        }
      }
    }
    else
    {
      if (DAL_SUCCESS != DalTsens_GetNumSensors(phTSENSDALDevHandle,
                                                &numTSensors))
      {
        therm_log_printf(THERM_LOG_LEVEL_ERROR, 0,
                         "FAILED to get number of TSENS sensors");
      }
    }
    init = TRUE;
  }

  if (tsens_sync_read_data->itemInit == FALSE)
  {
    /* Init TSENS */
    if (sensor_info->sensor_id < numTSensors)
    {
      tsens_sync_read_data->sensorPresent = TRUE;
      tsens_sync_read_data->itemInit = TRUE;
    }
  }

  do
  {
    if (tsens_sync_read_data->sensorPresent == FALSE || phTSENSDALDevHandle == NULL)
    {
      break;
    }

    if (DalTsens_GetTemp(phTSENSDALDevHandle, sensor_info->sensor_id, &tsensTemp) !=
        DAL_SUCCESS)
    {
      therm_log_printf(THERM_LOG_LEVEL_ERROR, 1, "%s: Get Temp failed",
                       __func__);
      break;
    }

    /* Convert units to K. */
    therm_state =  (npa_resource_state)THERM_C_TO_K(tsensTemp.nDegC);

  } while (0);
  return therm_state;

}

/**
  @brief therm_sensor_update_fcn

  This function is invoked by the /therm/sensor/<sensor>
  resources to service requests.

  @param  : resource: A dynamic system element that work requests can be made against.
            client: The handle to the clients registered to the resource.

  @return : None.
*/
static npa_resource_state therm_sensor_update_fcn( npa_resource *resource,
                                                   npa_client_handle client )
{
  npa_resource_state temperature = THERM_NPA_DEFAULT_TEMP;
  npa_resource_definition *definition = resource->definition;
  therm_sensor_info       *sensor_info = (therm_sensor_info *)definition->data;

  /* Ensure that the client is not NULL */
  CORE_VERIFY(client);

  switch (client->type)
  {
    case NPA_CLIENT_IMPULSE:
      if (THERM_NPA_HKADC_SENSOR == sensor_info->sensor_type)
      {
        therm_npa_adc_sync_read_type *adc_sync_read_data =
          (therm_npa_adc_sync_read_type *)sensor_info->sensor_data;

        temperature = therm_npa_adc_sync_read(resource->index);
        if (adc_sync_read_data->sensorPresent)
        {
          therm_npa_log_reading(resource->index, THERM_K_TO_C(temperature));
        }
      }
      else if (THERM_NPA_TSENS_SENSOR == sensor_info->sensor_type)
      {
        therm_npa_tsens_sync_read_type *tsens_sync_read_data =
          (therm_npa_tsens_sync_read_type *)sensor_info->sensor_data;

        temperature = therm_npa_tsens_sync_read(resource->index);
        if (tsens_sync_read_data->sensorPresent)
        {
          therm_npa_log_reading(resource->index, THERM_K_TO_C(temperature));
        }
      }
      break;

    default:
      break;
  } /* End of switch */

  return temperature;
}

#ifdef THERMAL_9x45
/**
  @brief therm_mss_lm_update_fcn

  This function is invoked by the
  /therm/sensor/mss_rail_lm_warning resources to service
  requests.

  @param  : resource: A dynamic system element that work requests can be made against.
            client: The handle to the clients registered to the resource.

  @return : None.
*/
static npa_resource_state therm_mss_lm_update_fcn( npa_resource *resource,
                                                   npa_client_handle client )
{
  npa_resource_state state = THERM_C_TO_K(THERM_NPA_MSS_LM_DEFAULT_WARNING);
  npa_resource_definition *definition = resource->definition;
  therm_sensor_info       *sensor_info = (therm_sensor_info *)definition->data;

  /* Ensure that the client is not NULL */
  CORE_VERIFY(client);

  switch (client->type)
  {
  case NPA_CLIENT_IMPULSE:
      if (mss_rail_cfg.disable ||
          (mss_rail_cfg.mss_iddq < mss_rail_cfg.mss_iddq_thresh))
      {
        /* Feature explicitly disabled or iddq falls below threshold for this
           warning to be relevant. */
        break;
      }

      /*Set state to active state */
      state = resource->active_state;

      therm_log_printf(THERM_LOG_LEVEL_DEBUG, 3, "mss_lm in: %d, %d %d",
                       THERM_K_TO_C(state), mss_rail_cfg.vdd_state,
                       mss_rail_cfg.tsens_state);
      if ((((uint32)mss_rail_cfg.vdd_state) >= mss_rail_cfg.vdd_thresh) &&
          ((uint32)mss_rail_cfg.tsens_state >= mss_rail_cfg.therm_thresh))
      {
        state = THERM_C_TO_K(THERM_NPA_MSS_LM_WARNING);
      }
      else if ((state == THERM_C_TO_K(THERM_NPA_MSS_LM_WARNING)) &&
               ((uint32)mss_rail_cfg.tsens_state <= mss_rail_cfg.therm_thresh_clr))
      {
        state = THERM_C_TO_K(THERM_NPA_MSS_LM_DEFAULT_WARNING);
      }
      break;

    default:
      break;
  } /* End of switch */

  therm_npa_log_reading(resource->index, THERM_K_TO_C(state));
  return state;
}
#endif

/**
  @brief therm_logical_max_update_fcn

  This function will be invoked when an NPA request is issued to
  /therm/sensor/modem_bcl_warning resource.

  @param : resource: NPA resource against which request was isssued.
           client: The handle to the client which issued this request.

  @return : State of the resource after request is applied.
 */
static npa_resource_state therm_logical_max_update_fcn( npa_resource *resource,
                                                        npa_client_handle client )
{
  npa_resource_state new_state = npa_max_plugin.update_fcn(resource, client);

  /* Log the state of the resource */
  therm_npa_log_reading( resource->index, new_state );

  return new_state;
}

/**
  @brief therm_node_driver_fcn

  Sensor node driver function.  Nothing to update.

  @param  : resource: A dynamic system element that work requests can be made against.
            client: The handle to the clients registered to the
            resource.
            state: Update function state.

  @return : Resource state.

*/
static npa_resource_state therm_node_driver_fcn ( npa_resource *resource,
                                                  npa_client *client,
                                                  npa_resource_state state )
{
  if(client->type == NPA_CLIENT_INITIALIZE)
  {
    // The driver function will be called with this client *before* it is
    // made publicly available, so you can place any initialization you
    // need here.

    // The value of state passed here is provided by the
    // npa_define_node function.
    therm_log_printf(THERM_LOG_LEVEL_REF, 2, "%s:%&d",
                   resource->definition->name,
                   &resource->active_state);
  }
  else
  {
    // Set the resource based on the value state.
  }

  return state;
}

/**
  @brief therm_sampling_node_driver_fcn

  Sampling node driver function.  Perform init, but no actual
  driver update.

  @param  : resource: A dynamic system element that work requests can be made against.
            client: The handle to the clients registered to the
            resource.
            state: State passed in from update function.

  @return : Resource state.

*/
static npa_resource_state therm_sampling_node_driver_fcn( npa_resource *resource,
                                                          npa_client *client,
                                                          npa_resource_state state )
{
  sampling_time = state;

  if(client->type == NPA_CLIENT_INITIALIZE)
  {
    // The driver function will be called with this client *before* it is
    // made publicly available, so you can place any initialization you
    // need here.

    // The value of state passed here is provided by the
    // npa_define_node function.
    unsigned int i;

    for ( i = 0; i < ARR_SIZE(sensor_client_handle); i++)
    {
      /* Create impulse client for sampling node to trigger a sensor read */
      sensor_client_handle[i] =  npa_create_sync_client( therm_sensor_resource[i].name,
                                                         "Sampling Sensor Handle",
                                                         NPA_CLIENT_IMPULSE);
      CORE_VERIFY(sensor_client_handle[i]);
    }

    timer_def( &sampling_timer, NULL, &thermal_tcb,
               THERM_SAMPLING_LOOP_TIMER_SIG, NULL, 0 );

    /* Start the sensor sampling polling timer. */
    timer_set( &sampling_timer, sampling_time, 0, T_MSEC);

  }
  else
  {
    // Set the resource based on the value state.
  }

  return state;
}

/*=======================================================================

                 PUBLIC FUNCTION DEFINITIONS

========================================================================*/
/**
  @brief therm_sampling_timer_cb

  Used to trigger sensor read update.

  @param  : timerIndex: Unused.

  @return : None.
*/
void therm_sampling_timer_cb(void)
{
  unsigned int i;

  for ( i = 0; i < ARR_SIZE(sensor_client_handle); i++)
  {
    npa_issue_impulse_request(sensor_client_handle[i]);
  }

  /* Restart sampling timer. */
  timer_set( &sampling_timer, sampling_time, 0, T_MSEC);
}

/**
  @brief therm_npa_sensor_init

  Thermal NPA sensor init.

 */
void therm_npa_sensor_init(void)
{
  /* Define nodes */
  npa_define_node( &therm_sensor_node, initial_sensor_state, NULL );

  npa_define_node( &therm_sampling_node, initial_sampling_state, NULL );

  /* Create logical resources. For simplicty sampling will be performed a the rate of
     the smallest sampling resource request from clients.  */
  npa_alias_resource_cb( "/therm/sampling", "/therm/sensor/pa/sampling", NULL, NULL );
  npa_alias_resource_cb( "/therm/sampling", "/therm/sensor/pa_1/sampling", NULL, NULL );
  npa_alias_resource_cb( "/therm/sampling", "/therm/sensor/modem_bcl_warning/sampling", NULL, NULL );
#ifdef THERMAL_9x45
  npa_alias_resource_cb( "/therm/sampling", "/therm/sensor/mss_rail_lm_warning/sampling", NULL, NULL );
  npa_alias_resource_cb( "/therm/sampling", "/therm/sensor/modem_tsens/sampling", NULL, NULL );
#endif
}
