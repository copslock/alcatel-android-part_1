/*============================================================================
  @file therm_target.c

  Target processor specific thermal code.

  Copyright (c) 2012-2015 QUALCOMM Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary
============================================================================*/
/*=======================================================================
$Header: //components/rel/core.mpss/3.4.c3.11/power/thermal/src/target/therm_target.c#1 $
$DateTime: 2016/03/28 23:02:17 $
$Author: mplcsds1 $
========================================================================*/
/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "comdef.h"
#include "therm_v.h"
#include "therm_npa_sensor.h"
#include "therm_npa_mitigate.h"
#include "therm_log.h"
#include "DALSys.h"
#include "npa.h"
#include "VCSDefs.h"
#ifdef THERMAL_9x45
#include "HALhwio.h"
#include "limits_hwio.h"
#include "limits_sec_cntrl_v2_hwio.h"
#include "DDIChipInfo.h"
#include "CoreIni.h"
#endif

/*=======================================================================

                  STATIC MEMBER / FUNCTION DECLARATIONS / DEFINITIONS

========================================================================*/

/*=======================================================================

                  GLOBAL DEFINITIONS

========================================================================*/
/* List of all resources to be created for this target processor.
   Must defined for each target processor. */
const char *therm_monitor_res_avail[] =
{
  "/therm/sensor/pa",
  "/therm/sensor/pa_1",
  "/therm/sensor/modem_bcl_warning",
  "/therm/sensor/pa/sampling",
  "/therm/sensor/pa_1/sampling",
  "/therm/sensor/modem_bcl_warning/sampling",
  "/therm/mitigate/pa",
  "/therm/mitigate/modem",
  "/therm/mitigate/cpuv_restriction_cold",
  "/therm/mitigate/modem_current",
#ifdef THERMAL_9x45
  "/therm/sensor/mss_rail_lm_warning",
  "/therm/sensor/mss_rail_lm_warning/sampling",
  "/therm/mitigate/cpr_cold",
#endif
};

static const char *vdd_restriction_res_avail[] =
{
  "/therm/mitigate/cpuv_restriction_cold",
  VCS_NPA_RESOURCE_VDD_MSS_ACTIVE_FLOOR,
};

#ifdef THERMAL_9x45
static const char *mss_lm_res_avail[] =
{
  "/therm/sensor/mss_rail_lm_warning",
  "/therm/sensor/modem_tsens",
  VCS_NPA_RESOURCE_VDD_MSS_UV,
};
#endif

/* Array size of therm_monitor_res_avail[] to be used by common thermal
   SW. Must defined for each target processor. */
const unsigned int therm_monitor_res_count = ARR_SIZE(therm_monitor_res_avail);

/* Thermal task info struct */
extern rex_tcb_type thermal_tcb;

/* Allocate a event handle for your client. */
static npa_event_handle  vRestrictEventClient;
static npa_client_handle mssClientHandle;

/*=======================================================================

                  LOCAL FUNCTION DEFINITIONS

========================================================================*/

/* Set voltage restriction requests. */
static void handle_v_restrict_state(unsigned int state)
{
  switch (state)
  {
    case 0:
      /*Handle Normal*/
      therm_log_printf(THERM_LOG_LEVEL_INFO, 0, "No MSS voltage restriction.");
      npa_cancel_request(mssClientHandle);
      break;
    default:
    case 1:
      /* Disable Restrict to Nominal */
      therm_log_printf(THERM_LOG_LEVEL_INFO, 0, "Restrict MSS to nominal.");
      npa_issue_scalar_request(mssClientHandle,
			       VCS_CORNER_NOMINAL);
      break;
  }
}

/* Handle "/therm/mitigate/cpuv_restriction_cold" change events. */
void change_event_callback_handler(void         *context,
                                   unsigned int  event_type,
                                   void         *data,
                                   unsigned int  data_size )
{
  npa_event_data *event_data_ptr = (npa_event_data*)data;

  do
  {
    if (data == NULL)
    {
      therm_log_printf(THERM_LOG_LEVEL_ERROR, 0,
                       "Invalid data for cpuv_restriction_cold cb.");
      break;
    }

    event_data_ptr = (npa_event_data*)data;

    handle_v_restrict_state((unsigned int)event_data_ptr->state);
  } while (0);
}

/* Resource is available, so proceed to register for change events. */
static void init_npa_event_callback(void         *context,
                                    unsigned int  event_type,
                                    void         *data,
                                    unsigned int  data_size )
{
  npa_query_type query_result;

  mssClientHandle = npa_create_sync_client(VCS_NPA_RESOURCE_VDD_MSS_ACTIVE_FLOOR,
                                           "MSS_V Restrict",
                                           NPA_CLIENT_REQUIRED);
  if (mssClientHandle == NULL)
  {
    therm_log_printf(THERM_LOG_LEVEL_ERROR, 0,
                     "Cannot create MSS client.");
    return;
  }

  vRestrictEventClient = npa_create_change_event_cb(
                                       "/therm/mitigate/cpuv_restriction_cold",
                                       "Event Name",
                                       change_event_callback_handler, NULL);
  if (vRestrictEventClient == NULL)
  {
    therm_log_printf(THERM_LOG_LEVEL_ERROR, 0,
                     "Cannot create change event cb cpuv_restriction_cold.");
    return;
  }

  memset(&query_result, 0x0, sizeof(npa_query_type));
  if (npa_query_by_event(vRestrictEventClient, NPA_QUERY_CURRENT_STATE,
                         &query_result) == NPA_QUERY_SUCCESS)
  {
    /* Get current state and apply it. */
    handle_v_restrict_state((unsigned int)query_result.data.state);
  }
}

#ifdef THERMAL_9x45
#define THERM_K_TO_C(x) ((int32)(((int32)x) - 273))
#define THERM_C_TO_K(x) ((int32)(((int32)x) + 273))

/* Global define */
mss_rail_cfg_type mss_rail_cfg = {
  .disable = FALSE,
  .mss_iddq = 26,
  .mss_iddq_thresh = 78,
  .therm_thresh = 95,
  .therm_thresh_clr = 65,
  .vdd_thresh = 1000,
  .vdd_thresh_clr =998,
};

typedef struct {
  const char *keyword;
  uint32     *value;
} cfg_kvp;

static const char *section_mss = "mss_lm";

static cfg_kvp kvp[] = {
  {
    .keyword = "disable",
    .value = &mss_rail_cfg.disable,
  },
  {
    .keyword = "iddq_override",
    .value = &mss_rail_cfg.mss_iddq,
  },
  {
    .keyword = "iddq_thresh",
    .value = &mss_rail_cfg.mss_iddq_thresh,
  },
  {
    .keyword = "therm_thresh",
    .value = &mss_rail_cfg.therm_thresh,
  },
  {
    .keyword = "therm_thresh_clr",
    .value = &mss_rail_cfg.therm_thresh_clr,
  },
  {
    .keyword = "vdd_thresh",
    .value = &mss_rail_cfg.vdd_thresh,
  },
  {
    .keyword = "vdd_thresh_clr",
    .value = &mss_rail_cfg.vdd_thresh_clr,
  },
};

static const char *mss_lm_ini_file = "/nv/item_files/therm_monitor/mss_lm.ini";

/* Example Config
  [mss_lm]
  disable=0
  iddq_override=79
  iddq_thresh=78
  therm_thresh=95
  therm_thresh_clr=65
  vdd_thresh=1000
  vdd_thresh_clr=998
*/

static void mss_rail_init(void)
{
  CoreConfigHandle handle;
  uint8 idx;

  if ((DalChipInfo_ChipFamily() == DALCHIPINFO_FAMILY_MDM9x45) &&
      (DalChipInfo_ChipVersion() >= DALCHIPINFO_VERSION(2,0)))
  {
    mss_rail_cfg.mss_iddq = HWIO_INF(QFPROM_CORR_PTE2_V2, IDDQ_MSS_ON) *
                           (0x1 << HWIO_INF(QFPROM_CORR_PTE1_V2, IDDQ_MULTIPLIER));
  }
  else
  {
    mss_rail_cfg.mss_iddq = HWIO_INF(QFPROM_CORR_PTE2, IDDQ_MSS_ON) *
                           (0x1 << HWIO_INF(QFPROM_CORR_PTE1, IDDQ_MULTIPLIER));
  }

  /* Create therm log ref entries for debug */
  therm_log_printf(THERM_LOG_LEVEL_REF, 3, "Rail cfg: %&d %&d %&d",
                   &mss_rail_cfg.disable,
                   &mss_rail_cfg.mss_iddq,
                   &mss_rail_cfg.mss_iddq_thresh);
  therm_log_printf(THERM_LOG_LEVEL_REF, 4,
                   "Rail cfg: therm %&d %&d, vdd %&d %&d",
                   &mss_rail_cfg.therm_thresh,
                   &mss_rail_cfg.therm_thresh_clr,
                   &mss_rail_cfg.vdd_thresh,
                   &mss_rail_cfg.vdd_thresh_clr);
  therm_log_printf(THERM_LOG_LEVEL_REF, 4,
                   "Rail cfg state: therm %&d %&d, vdd %&d %&d",
                   &mss_rail_cfg.tsens_state,
                   &mss_rail_cfg.tsens_max,
                   &mss_rail_cfg.vdd_state,
                   &mss_rail_cfg.vdd_max);

  handle = CoreIni_ConfigCreate(mss_lm_ini_file);
  if (NULL == handle)
  {
    return;
  }

  for (idx = 0; idx < ARR_SIZE(kvp); idx++)
  {
    unsigned int value;
    if (CORE_CONFIG_SUCCESS == CoreConfig_ReadUint32(handle, (char*)section_mss,
                                                     (char*)kvp[idx].keyword,
                                                     &value))
    {
      *(kvp[idx].value) = (uint32)value;
      therm_log_printf(THERM_LOG_LEVEL_DEBUG, 2, "mss_cfg found: %s, %d",
                       kvp[idx].keyword, *(kvp[idx].value));
    }
  }
  CoreIni_ConfigDestroy(handle);
}

/* Handle LM MSS VDD thresh events. */
void lm_mss_vdd_event_cb(void         *context,
                         unsigned int  event_type,
                         void         *data,
                         unsigned int  data_size )
{
  static boolean init;
  npa_event_data *event_data_ptr = (npa_event_data*)data;

  do
  {
    if (data == NULL)
    {
      therm_log_printf(THERM_LOG_LEVEL_ERROR, 0,
                       "Invalid data for mss vdd event");
      break;
    }
    /* Use only event states for making decisions to avoid race conditions */
    mss_rail_cfg.vdd_state = THERM_uV_TO_mV(event_data_ptr->state);

    if (init && (event_type == NPA_EVENT_THRESHOLD_LO ||
                 event_type == NPA_EVENT_THRESHOLD_HI))
    {
      npa_issue_impulse_request(mss_rail_cfg.lmClientHandle);
    }

    if ((!init) ||
        (event_type == NPA_EVENT_THRESHOLD_LO ||
         event_type == NPA_EVENT_THRESHOLD_HI))
    {
      /* Only set thresholds on hi/lo event or if !init */
      if (mss_rail_cfg.vdd_state  >= mss_rail_cfg.vdd_thresh)
      {
        /* Config for thresh clr then */
        npa_set_event_thresholds(mss_rail_cfg.mssVddEventClient,
                                 THERM_mV_TO_uV(mss_rail_cfg.vdd_thresh_clr + 1),
                                 mss_rail_cfg.vdd_max);
      }
      else
      {
        /* Config for thresh then */
        npa_set_event_thresholds(mss_rail_cfg.mssVddEventClient, 0,
                                 THERM_mV_TO_uV(mss_rail_cfg.vdd_thresh - 1));
      }
      init = TRUE;
    }
  } while (0);
}

/* Handle LM MSS TSENS thresh events. */
void lm_mss_tsens_event_cb(void         *context,
                           unsigned int  event_type,
                           void         *data,
                           unsigned int  data_size )
{
  static boolean init;
  npa_event_data *event_data_ptr = (npa_event_data*)data;

  do
  {
    if (data == NULL)
    {
      therm_log_printf(THERM_LOG_LEVEL_ERROR, 0,
                       "Invalid data for mss tsens event");
      break;
    }
    /* Use only event states for making decisions to avoid race conditions */
    mss_rail_cfg.tsens_state = THERM_K_TO_C(event_data_ptr->state);

    if (init && (event_type == NPA_EVENT_THRESHOLD_LO ||
                 event_type == NPA_EVENT_THRESHOLD_HI))
    {
      npa_issue_impulse_request(mss_rail_cfg.lmClientHandle);
    }

    if ((!init) ||
        (event_type == NPA_EVENT_THRESHOLD_LO ||
         event_type == NPA_EVENT_THRESHOLD_HI))
    {
      /* Only set thresholds on hi/lo event or if !init */
      if (mss_rail_cfg.tsens_state >= mss_rail_cfg.therm_thresh)
      {
        /* Config for thresh clr then */
        npa_set_event_thresholds(mss_rail_cfg.tsensEventClient,
                                 THERM_C_TO_K(mss_rail_cfg.therm_thresh_clr + 1),
                                 mss_rail_cfg.tsens_max);
      }
      else
      {
        /* Config for thresh then */
        npa_set_event_thresholds(mss_rail_cfg.tsensEventClient, 0,
                                 THERM_C_TO_K(mss_rail_cfg.therm_thresh - 1));
      }
      init = TRUE;
    }
  } while (0);
}


/* Resource is available, so proceed to register for change events. */
static void init_lm_res_avail_cb(void         *context,
                                 unsigned int  event_type,
                                 void         *data,
                                 unsigned int  data_size )
{
  npa_query_type query_result;
  npa_client_handle modem_tsens_handle;

  /* Create impulse client to force mss lm warning update. */
  mss_rail_cfg.lmClientHandle = npa_create_sync_client(
                                          "/therm/sensor/mss_rail_lm_warning",
                                          "LM MSS Impulse",
                                          NPA_CLIENT_IMPULSE);
  if (mss_rail_cfg.lmClientHandle == NULL)
  {
    therm_log_printf(THERM_LOG_LEVEL_ERROR, 0,
                     "Cannot create lm mss client.");
    return;
  }

  /* Create temporary handle to force a sensor update on init. */
  modem_tsens_handle = npa_create_sync_client("/therm/sensor/modem_tsens",
                                              "tsens temp handle",
                                              NPA_CLIENT_IMPULSE);
  if (modem_tsens_handle == NULL)
  {
    therm_log_printf(THERM_LOG_LEVEL_ERROR, 0,
                     "Cannot create tsens temp handle");
    return;
  }
  npa_issue_impulse_request(modem_tsens_handle);
  npa_destroy_client(modem_tsens_handle);

  /* Record mss tsens max state to assist with setting threholds */
  memset(&query_result, 0x0, sizeof(npa_query_type));
  if (npa_query_by_name("/therm/sensor/modem_tsens",
                        NPA_QUERY_RESOURCE_MAX,
                        &query_result) != NPA_QUERY_SUCCESS)
  {
    therm_log_printf(THERM_LOG_LEVEL_ERROR, 0,
                     "Cannot query tsens max");
  }
  else
  {
    mss_rail_cfg.tsens_max = query_result.data.state;
  }

  /* Create event client to request modem tsens thresholds. */
  mss_rail_cfg.tsensEventClient = npa_create_event_cb(
                                                  "/therm/sensor/modem_tsens",
                                                  "LM MSS tsens",
                                                  NPA_TRIGGER_THRESHOLD_EVENT,
                                                  lm_mss_tsens_event_cb,
                                                  NULL);
  if (mss_rail_cfg.tsensEventClient == NULL)
  {
    therm_log_printf(THERM_LOG_LEVEL_ERROR, 0,
                     "Cannot create lm mss tsens client.");
    return;
  }

  /* Record latest temp reading */
  memset(&query_result, 0x0, sizeof(npa_query_type));
  if (npa_query_by_event(mss_rail_cfg.tsensEventClient,
                         NPA_QUERY_CURRENT_STATE,
                         &query_result) != NPA_QUERY_SUCCESS)
  {
    therm_log_printf(THERM_LOG_LEVEL_ERROR, 0,
                     "Cannot query tsens state");
  }
  else
  {
    mss_rail_cfg.tsens_state = THERM_K_TO_C(query_result.data.state);
  }

  /* Record mss uV max state to assist with setting threholds */
  memset(&query_result, 0x0, sizeof(npa_query_type));
  if (npa_query_by_name(VCS_NPA_RESOURCE_VDD_MSS_UV,
                        NPA_QUERY_RESOURCE_MAX,
                        &query_result) != NPA_QUERY_SUCCESS)
  {
    therm_log_printf(THERM_LOG_LEVEL_ERROR, 0,
                     "Cannot query vdd max");
  }
  else
  {
    mss_rail_cfg.vdd_max = query_result.data.state;
  }

  /* Create event client to request mss uV thresholds. */
  mss_rail_cfg.mssVddEventClient = npa_create_event_cb(
                                                  VCS_NPA_RESOURCE_VDD_MSS_UV,
                                                  "LM MSS uV",
                                                  NPA_TRIGGER_THRESHOLD_EVENT,
                                                  lm_mss_vdd_event_cb,
                                                  NULL);
  if (mss_rail_cfg.mssVddEventClient == NULL)
  {
    therm_log_printf(THERM_LOG_LEVEL_ERROR, 0,
                     "Cannot create MSS uV client.");
    return;
  }

  /* Record mss uV max current state */
  memset(&query_result, 0x0, sizeof(npa_query_type));
  if (npa_query_by_event(mss_rail_cfg.mssVddEventClient,
                         NPA_QUERY_CURRENT_STATE,
                         &query_result) != NPA_QUERY_SUCCESS)
  {
    therm_log_printf(THERM_LOG_LEVEL_ERROR, 0,
                     "Cannot query vdd max");
  }
  else
  {
    mss_rail_cfg.vdd_state = THERM_uV_TO_mV(query_result.data.state);
  }

  /* Force update of mss lm warning sensor */
  npa_issue_impulse_request(mss_rail_cfg.lmClientHandle);

  /* Set initial thresholds for mss tsens and vdd uv to stimulate call from
     event cb handler */
  npa_set_event_thresholds(mss_rail_cfg.mssVddEventClient, 0, mss_rail_cfg.vdd_max);
  npa_set_event_thresholds(mss_rail_cfg.tsensEventClient, 0, mss_rail_cfg.tsens_max);
}
#endif

/*=======================================================================

                 PUBLIC FUNCTION DEFINITIONS

========================================================================*/
/**
  @brief therm_target_init

  Initializes target specific thermal SW.

*/
void therm_target_init(void)
{

  DALSYS_InitMod(NULL);

  therm_log_init();

  therm_log_set_level(THERM_LOG_LEVEL_MAX);

#ifdef THERMAL_9x45
  mss_rail_init();

  npa_resources_available_cb(ARR_SIZE(mss_lm_res_avail),
                             mss_lm_res_avail,
                             init_lm_res_avail_cb,
                             NULL);
#endif

  therm_npa_sensor_init();

  therm_npa_mitigate_init();

  npa_resources_available_cb(ARR_SIZE(vdd_restriction_res_avail),
                             vdd_restriction_res_avail,
                             init_npa_event_callback,
                             NULL);
}

/**
  @brief therm_target_after_task_start_init

  Initialize target specific thermal SW after thermal
  task has started.
*/
void therm_target_after_task_start_init(void)
{
  /* Intentionally left empty */
}

/**
  @brief therm_target_process_sigs

  Process rex signals for target specific SW.

  @param rcvdSigs
*/
void therm_target_process_sigs(rex_sigs_type rcvdSigs)
{
  if (rcvdSigs & THERM_SAMPLING_LOOP_TIMER_SIG)
  {
    rex_clr_sigs(&thermal_tcb, THERM_SAMPLING_LOOP_TIMER_SIG);
    therm_sampling_timer_cb();
  }
}
