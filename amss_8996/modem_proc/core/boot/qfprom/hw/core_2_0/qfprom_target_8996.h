#ifndef QFPROM_TARGET_8996_H
#define QFPROM_TARGET_8996_H

/*===========================================================================

                        QFPROM  Driver Header  File

DESCRIPTION
 Contains target specific defintions and APIs to be used to read and write
 qfprom values for sec ctrl 3.0 (used by MSM8996).

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None

Copyright 2015 by Qualcomm Technologies Incorporated.  All Rights Reserved.
============================================================================*/

/*===========================================================================

                           EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.


when       who     what, where, why
--------   ---     ----------------------------------------------------------
10/28/15   kpa     Update blow timer clock freq to that supported by clk driver
05/14/15   ck      Updated for 8996 v2
04/02/15   ck      Initial version for 8996

============================================================================*/

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/
#include "comdef.h"
#include "qfprom.h"

/*===========================================================================

                      PUBLIC DATA DECLARATIONS

===========================================================================*/

/* Blow timer clock frequency in Mhz*/
#define QFPROM_BLOW_TIMER_CLK_FREQ_MHZ 4.8

/* Amount of time required to hold charge to blow fuse in micro-seconds */
#define QFPROM_FUSE_BLOW_TIME_IN_US    1

/*
* Configure QFPROM_ACCEL[QFPROM_TRIPPT_SEL] SW field to a value of 0x6 (110) to blow at a high resistance setting of 7 Kohms.
* Configure QFPROM_ACCEL[QFPROM_ACCEL] SW field to a value of 0x90.
* Leave the value of QFPROM_ACCEL[QFPROM_GATELAST] to a default value of 0x1 for 14nm technology.
*/
#define QFPROM_ACCEL_VALUE             0xE90

/* QFPROM_ACCEL register default value */
#define QFPROM_ACCEL_RESET_VALUE       0x800

/*
** Raw Address Start and End configuration to use for read/write operations.
** The start address is designed to be at the first supported region LSB address in the 
** memory map. The end address is designed to be the last supported region MSB address
** in the memory map.
*/
#define QFPROM_RAW_LOWEST_ADDRESS_OFFSET           (0x00000130 )
#define QFPROM_RAW_HIGHEST_ADDRESS_OFFSET          (0x000007FC )

/* Macro for Read/write permission of corrected address */
#define QFPROM_READ_PERM_CORRECTED_ADDR		HWIO_QFPROM_CORR_RD_PERM_LSB_ADDR
#define QFPROM_WRITE_PERM_CORRECTED_ADDR	HWIO_QFPROM_CORR_WR_PERM_LSB_ADDR


#endif /* QFPROM_TARGET_8996_H */

