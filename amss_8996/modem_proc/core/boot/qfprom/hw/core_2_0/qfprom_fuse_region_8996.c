/*===========================================================================

                       QFPROM  Driver Source  Code

DESCRIPTION
 Contains target specific defintions and APIs to be used to read and write
 qfprom values for sec ctrl 3.0 (used by MSM8996).

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None

Copyright 2015 by Qualcomm Technologies Incorporated.  All Rights Reserved.
============================================================================*/

/*===========================================================================

                           EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/core.mpss/3.4.c3.11/boot/qfprom/hw/core_2_0/qfprom_fuse_region_8996.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
05/14/15   ck      Updated for MSM8996 v2
04/02/15   ck      Initial revision for MSM8996

============================================================================*/

/*=============================================================================

                            INCLUDE FILES FOR MODULE

=============================================================================*/
#include QFPROM_HWIO_REG_INCLUDE_H
#include QFPROM_TARGET_INCLUDE_H
#include "qfprom_target_common.h"

/*=============================================================================

            LOCAL DEFINITIONS AND DECLARATIONS FOR MODULE

This section contains local definitions for constants, macros, types,
variables and other items needed by this module.

=============================================================================*/
/*
**  Array containing QFPROM data items that can be read and associated
**  registers, mask and shift values for the same.
*/
/*---------------------------------------------------------------------------
  QFPROM REGIONS 
---------------------------------------------------------------------------*/
typedef enum
{
  QFPROM_CRI_CM_PRIVATE_REGION = 0,
  QFPROM_ACC_PRIVATE_FEAT_PROV_REGION,
  QFPROM_PTE_REGION,
  QFPROM_READ_PERMISSION_REGION,
  QFPROM_WRITE_PERMISSION_REGION,
  QFPROM_FEC_EN_REGION,
  QFPROM_ANTI_ROLLBACK_1_REGION,
  QFPROM_ANTI_ROLLBACK_2_REGION,
  QFPROM_ANTI_ROLLBACK_3_REGION,
  QFPROM_ANTI_ROLLBACK_4_REGION,
  QFPROM_OEM_CONFIG_REGION,
  QFPROM_FEAT_CONFIG_REGION,
  QFPROM_PK_HASH0_REGION,
  QFPROM_CALIB_REGION,
  QFPROM_MEM_CONFIG_REGION,
  QFPROM_QC_SPARE_REG15_REGION,
  QFPROM_QC_SPARE_REG16_REGION,
  QFPROM_QC_SPARE_REG17_REGION,
  QFPROM_QC_SPARE_REG18_REGION,
  QFPROM_QC_SPARE_REG19_REGION,
  QFPROM_QC_SPARE_REG20_REGION,
  QFPROM_QC_SPARE_REG21_REGION,
  QFPROM_QC_IMAGE_ENCR_KEY_REGION,
  QFPROM_OEM_IMAGE_ENCR_KEY_REGION,
  QFPROM_OEM_SEC_BOOT_REGION,
  QFPROM_PRI_KEY_DERIVATION_KEY_REGION,
  QFPROM_SEC_KEY_DERIVATION_KEY_REGION,
  QFPROM_BOOT_ROM_PATCH_REGION,
  QFPROM_HDCP_KEY_REGION,
  QFPROM_IMAGE_ENCR_KEY1_REGION,
  QFPROM_PK_HASH1_REGION,
  QFPROM_OEM_SPARE_REG31_REGION,
  QFPROM_OEM_SPARE_REG32_REGION,
  QFPROM_OEM_SPARE_REG33_REGION,
  QFPROM_OEM_SPARE_REG34_REGION,
  QFPROM_OEM_SPARE_REG35_REGION,
  QFPROM_OEM_SPARE_REG36_REGION,
  QFPROM_OEM_SPARE_REG37_REGION,
  QFPROM_OEM_SPARE_REG38_REGION,
  QFPROM_OEM_SPARE_REG39_REGION,
  QFPROM_OEM_SPARE_REG40_REGION,
  QFPROM_OEM_SPARE_REG41_REGION,
  QFPROM_OEM_SPARE_REG42_REGION,
  QFPROM_OEM_SPARE_REG43_REGION,
  QFPROM_OEM_SPARE_REG44_REGION,
  
  /* Add above this */
  QFPROM_LAST_REGION_DUMMY,
  QFPROM_MAX_REGION_ENUM                = 0x7FFF /* To ensure it's 16 bits wide */
} QFPROM_REGION_NAME;


const QFPROM_REGION_INFO qfprom_region_data[] =
{
  {
    QFPROM_ANTI_ROLLBACK_4_REGION,
    1,
    QFPROM_FEC_NONE,
    HWIO_QFPROM_RAW_ANTI_ROLLBACK_4_LSB_ADDR ,
    HWIO_QFPROM_CORR_ANTI_ROLLBACK_4_LSB_ADDR ,
    HWIO_QFPROM_CORR_RD_PERM_LSB_ANTI_ROLLBACK_4_BMSK,
    HWIO_QFPROM_CORR_WR_PERM_LSB_ANTI_ROLLBACK_4_BMSK,
    QFPROM_ROW_LSB,
    TRUE,
    7
  },

  {
    QFPROM_OEM_SPARE_REG43_REGION, 
    2,
    QFPROM_FEC_NONE,
    HWIO_QFPROM_RAW_OEM_SPARE_REGn_ROW0_LSB_ADDR(43),
    HWIO_QFPROM_CORR_OEM_SPARE_REGn_ROW0_LSB_ADDR(43),
    HWIO_QFPROM_CORR_RD_PERM_MSB_OEM_SPARE_REG43_BMSK,
    HWIO_QFPROM_CORR_WR_PERM_MSB_OEM_SPARE_REG43_BMSK,
    QFPROM_ROW_MSB,
    TRUE,
    39
  },
  
  /* Add above this entry */
  {
    QFPROM_LAST_REGION_DUMMY,
    0,
    QFPROM_FEC_NONE,
    0,
    0,
    0,
    0,
    QFPROM_ROW_LSB
  }
};

uint32 qfprom_region_data_size = sizeof(qfprom_region_data)/sizeof(QFPROM_REGION_INFO);


/*===========================================================================
**  Function :    : qfprom_update_fusemap

** ==========================================================================
*/
/*!
*
* @brief :  This function updates the qfprom_region_data table pointer and other variables to support 9x45 V2 hw.
*
* @param  :
*   qfprom_fusemap_data *fusemap_data_ptr  - pointer to access the fusemap related variables

* @par Dependencies:
*
* @retval: None
*
*/
void qfprom_update_fusemap(qfprom_fusemap_data *fusemap_data_ptr){}
