#ifndef GLINK_SIO_PROC_H
#define GLINK_SIO_PROC_H

/*---------------------------------------------------------------------------
            GLINK SIO Processor-Specific Definitions
---------------------------------------------------------------------------*/
/*!
  @file
    glink_proc.h    
*/

/*--------------------------------------------------------------------------
     Copyright  2014 Qualcomm Technologies Incorporated. 
     All rights reserved.
---------------------------------------------------------------------------*/
/*===========================================================================

                           EDIT HISTORY FOR FILE

when       who     what, where, why
--------   ---     ----------------------------------------------------------
01/31/14   db      Initial version. 
===========================================================================*/

/*===========================================================================
                        INCLUDE FILES
===========================================================================*/
#include "smd_v.h"
#include "DDIIPCInt.h"

#ifdef __cplusplus
extern "C" {
#endif

/*===========================================================================
                        MACRO DEFINITIONS
===========================================================================*/
/** \name GLINK Host
 *
 * This macro defines the local processor in which GLINK runs.
 *
 * @{
 */
#define SMD_THIS_HOST   ( SMEM_MODEM )
#define SMD_MODEM_PROC
/*@}*/


#ifdef __cplusplus
}
#endif

#endif  /* GLINK_SIO_PROC_H */
