#ifndef GLINK_SIO_TASK_H
#define GLINK_SIO_TASK_H

/*===========================================================================

                    GLINK TASK Header File

 Copyright (c) 2005-20014 by Qualcomm Technologies, Incorporated.  All Rights
 Reserved.
===========================================================================*/

/*===========================================================================

                           EDIT HISTORY FOR FILE



when       who     what, where, why
--------   ---     ----------------------------------------------------------
08/29/14   db     initial version 

===========================================================================*/

/*===========================================================================

                        INCLUDE FILES

===========================================================================*/
#include "queue.h"
#include "smem_type.h"
#include "glink_sio_proc.h"
#include "glink_os_utils.h"
#include "glink.h"

#define GLINK_SIO_CMD_BUF_CNT 	100
#define GLINK_SIO_NO_OF_EVT		  1
#define GLINK_SIO_CMD_EVT_IDX	  0

/*===========================================================================

                        TYPEDEFS

===========================================================================*/

/* function pointer for thread callback function  */
/* this function will be passed in glink_command 
 * and called back from glink thread              */
typedef int32 ( *glink_sio_thread_cb_fn )( void * );

/* enum for different glink commands */
typedef enum
{
  GLINK_CMD_PROCESS_PORT
} glink_sio_cmd_enum_type;

/* glink command structure */
typedef struct
{
  glink_sio_cmd_enum_type cmd;                    /* glink command enum */
  glink_sio_thread_cb_fn glink_sio_thread_cb;     /* thread callback function */
  void *glink_sio_thread_priv_data;               /* private data for thread callback function */
} glink_sio_command_type;

/* glink context - stores queue of commands       */
/* add context specific parameters in this struct 
 * in future                                      */
typedef struct
{
  /* command queue head pointer */
  q_type *cmd_queue;  

  /* mutex protecting queue operations on cmd_queue */
  void *cmd_queue_cs;
} glink_sio_context_type;

/* structure to create queue of commands */
typedef struct
{
  q_link_type             link;
  glink_sio_command_type  cmd;
} glinki_sio_command_type;

/*===========================================================================

                        FUNCTION DECLARATION

===========================================================================*/
/* function definition to be exposed */
void glink_sio_notify_write
(
  glink_sio_thread_cb_fn glink_sio_thread_cb,
  void *priv
);

/*===========================================================================

                        GLOBAL AND STATIC VARIABLES

===========================================================================*/        
/* command queue header */
extern q_type glink_sio_cmd_queue;

/* command queue buffer header */
extern q_type glink_sio_cmd_free_queue;

/* command queue buffer */
extern glinki_sio_command_type glink_sio_cmd_buf[GLINK_SIO_CMD_BUF_CNT];

/* thread context per processor */
extern glink_sio_context_type glink_sio_thread_context[SMEM_NUM_HOSTS];

/* event for processing glink commands */
extern os_event_type glink_sio_evt[GLINK_SIO_NO_OF_EVT];

#endif /* GLINK_SIO_TASK_H */

