/*===========================================================================

               I P C    R O U T E R   X A L  S M D L  R E X

        This file provides Rex OS specific functionality of the smdl
                          XAL for the IPC Router

  ---------------------------------------------------------------------------
  Copyright (c) 2011-2013 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.
  ---------------------------------------------------------------------------
===========================================================================*/

/*===========================================================================

                           EDIT HISTORY FOR FILE

$Header: //components/rel/core.mpss/3.4.c3.11/mproc/ipc_router/src/ipc_router_xal_smdl_rex.c#1 $
$DateTime: 2016/03/28 23:02:17 $
$Author: mplcsds1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
09/01/11   aep     Initial creation
==========================================================================*/

/*===========================================================================
                          INCLUDE FILES
===========================================================================*/

#include "ipc_router_types.h"
#include "ipc_router_xal_smdl_common.h"
#include "rex.h"

/*===========================================================================
                  CONSTANT / MACRO DECLARATIONS
===========================================================================*/

/** The worker thread's stack size */
#define IPC_ROUTER_XAL_SMDL_STACK_SIZE (2048 * 4)

/** The signal the worker thread waits on */
#define IPC_ROUTER_XAL_WORKER_SIGNAL_READ_WRITE    (0x00010000)
#define IPC_ROUTER_XAL_WORKER_SIGNAL_REMOTE_CLOSED (0x00020000)
#define IPC_ROUTER_XAL_WORKER_SIGNAL_REMOTE_RESUME (0x00040000)
#define IPC_ROUTER_XAL_WORKER_SIGNAL_KILL          (0x00080000)
#define IPC_ROUTER_XAL_WORKER_SIGNAL (IPC_ROUTER_XAL_WORKER_SIGNAL_READ_WRITE | \
                                      IPC_ROUTER_XAL_WORKER_SIGNAL_REMOTE_CLOSED | \
                                      IPC_ROUTER_XAL_WORKER_SIGNAL_REMOTE_RESUME | \
                                      IPC_ROUTER_XAL_WORKER_SIGNAL_KILL)

/*===========================================================================
                        PRIVATE TYPE DEFINITIONS 
===========================================================================*/

/** The OS specific private structure of the smdl port */
typedef struct {
  rex_tcb_type            tcb;        /* TCB of the worker thread */
  rex_stack_pointer_type  rex_stack;  /* Stack of the worker thread */
} ipc_router_xal_smdl_os_port_type;

/*===========================================================================
                        LOCAL FUNCTIONS
===========================================================================*/

/*===========================================================================
FUNCTION      ipc_router_xal_smdl_worker_thread

DESCRIPTION   The Worker thread dedicated to handle RX and TX events
              on this SMDL port.

              priv - Private parameter (port)

RETURN VALUE  None

SIDE EFFECTS  This function never returns.
===========================================================================*/
static void ipc_router_xal_smdl_worker_thread(rex_task_func_param_type priv)
{
  ipc_router_xal_smdl_port_type *port = (ipc_router_xal_smdl_port_type *)priv;
  rex_sigs_type got_sigs = 0;

  while((got_sigs & IPC_ROUTER_XAL_WORKER_SIGNAL_KILL) == 0)
  {
    got_sigs = rex_wait(IPC_ROUTER_XAL_WORKER_SIGNAL);
    rex_clr_sigs(rex_self(), got_sigs);
    if(got_sigs & IPC_ROUTER_XAL_WORKER_SIGNAL_KILL)
    {
      break;
    }
    if(got_sigs & IPC_ROUTER_XAL_WORKER_SIGNAL_REMOTE_CLOSED)
    {
      ipc_router_xal_smdl_handle_remote_close(port);
    }
    if(got_sigs & IPC_ROUTER_XAL_WORKER_SIGNAL_REMOTE_RESUME)
    {
      ipc_router_xal_smdl_handle_remote_resume(port);
    }
    if(got_sigs & IPC_ROUTER_XAL_WORKER_SIGNAL_READ_WRITE)
    {
      ipc_router_xal_smdl_handle_rx(port);
      ipc_router_xal_smdl_handle_tx(port);
    }
  }
}

/*===========================================================================
                        EXPORTED FUNCTIONS
===========================================================================*/

/*===========================================================================
FUNCTION      ipc_router_xal_smdl_os_init

DESCRIPTION   Initializes the OS Private structure and spawns a worker 
              thread to handle this port.

              port - The port structure for this SMDL interface.

RETURN VALUE  IPC_ROUTER_STATUS_SUCCESS on success,
              error code on failure.
===========================================================================*/
int ipc_router_xal_smdl_os_init(ipc_router_xal_smdl_port_type *port)
{
  ipc_router_xal_smdl_os_port_type *os_priv;

  port->task_handle = os_priv = ipc_router_os_malloc(sizeof(ipc_router_xal_smdl_os_port_type));
  if(!os_priv)
  {
    return IPC_ROUTER_STATUS_NO_MEM;
  }

  ipc_router_os_mem_set(os_priv, 0, sizeof(ipc_router_xal_smdl_os_port_type));

  os_priv->rex_stack = (rex_stack_pointer_type)ipc_router_os_malloc(IPC_ROUTER_XAL_SMDL_STACK_SIZE);
  if(!os_priv->rex_stack)
  {
    port->task_handle = NULL;
    ipc_router_os_free(os_priv);
    return IPC_ROUTER_STATUS_NO_MEM;
  }

  rex_def_task_ext(&os_priv->tcb, 
		  os_priv->rex_stack,
		  IPC_ROUTER_XAL_SMDL_STACK_SIZE,
		  150,
		  ipc_router_xal_smdl_worker_thread,
		  (rex_task_func_param_type)port,
		  "IPCSMDL_W",
		  0);

  return IPC_ROUTER_STATUS_SUCCESS;
}


/*===========================================================================
FUNCTION      ipc_router_xal_smdl_os_deinit

DESCRIPTION   De-initialize the OS specific elements associated with this port.

              port - The port structure for this SMDL interface.

RETURN VALUE  None

SIDE EFFECTS  The worker thread associated with this port might be killed.
===========================================================================*/
void ipc_router_xal_smdl_os_deinit(ipc_router_xal_smdl_port_type *port)
{
  ipc_router_xal_smdl_os_port_type *os_port;
  if(!port)
  {
    return;
  }
  os_port = (ipc_router_xal_smdl_os_port_type *)port->task_handle;
  if(!os_port)
  {
    return;
  }

  rex_set_sigs(&os_port->tcb, IPC_ROUTER_XAL_WORKER_SIGNAL_KILL);
  /* Assuming rex_kill_task() is like pthread_join() */
  rex_kill_task(&os_port->tcb);
  ipc_router_os_free((void *)os_port->rex_stack);
  ipc_router_os_free(os_port);
  port->task_handle = NULL;
}

/*===========================================================================
FUNCTION      ipc_router_xal_smdl_os_signal_worker

DESCRIPTION   Signal the worker to handle an event

              port - The port structure for this SMDL interface.

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
void ipc_router_xal_smdl_os_signal_worker(ipc_router_xal_smdl_port_type *port, ipc_router_xal_smdl_event_type event)
{
  ipc_router_xal_smdl_os_port_type *os = (ipc_router_xal_smdl_os_port_type *)port->task_handle;
  switch(event)
  {
    case IPC_ROUTER_XAL_SMDL_READ_WRITE:
      rex_set_sigs(&os->tcb, IPC_ROUTER_XAL_WORKER_SIGNAL_READ_WRITE);
      break;
    case IPC_ROUTER_XAL_SMDL_REMOTE_CLOSED:
      rex_set_sigs(&os->tcb, IPC_ROUTER_XAL_WORKER_SIGNAL_REMOTE_CLOSED);
      break;
    case IPC_ROUTER_XAL_SMDL_REMOTE_RESUME:
      rex_set_sigs(&os->tcb, IPC_ROUTER_XAL_WORKER_SIGNAL_REMOTE_RESUME);
      break;
  }
}

