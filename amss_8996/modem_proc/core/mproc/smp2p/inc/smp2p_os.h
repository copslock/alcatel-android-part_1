/*
 * @file smp2p_os.h
 *
 * OS specific routines and definitions for SMP2P.
 */

/*==============================================================================
     Copyright (c) 2012 Qualcomm Technologies Incorporated.
     All rights reserved.
     Qualcomm Confidential and Proprietary
==============================================================================*/

/*===========================================================================

                           EDIT HISTORY FOR FILE

$Header: //components/rel/core.mpss/3.4.c3.11/mproc/smp2p/inc/smp2p_os.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
10/10/12   pa      Initial revision.
===========================================================================*/
#ifndef SMP2P_OS_H_
#define SMP2P_OS_H_

#include "DALSys.h"
#include "DALFramework.h"
 
/** SMP2P OS parameters
 * Contains any OS-specific variables needed by the SMP2P API.
 * Currently only defined for DAL, though other OS's may be added.
 */
typedef struct
{
  DALSYSEventHandle hEvent;
} smp2p_os_params_type;

/* -------------------------------------------------------------
   Memory barrier for REX
------------------------------------------------------------- */
#define SMP2P_MEMORY_BARRIER() memory_barrier()

#endif /* SMP2P_OS_H_ */