#ifndef __CLOCKMSSHWIO_H__
#define __CLOCKMSSHWIO_H__
/*
===========================================================================
*/
/**
  @file ClockMSSHWIO.h
  @brief Auto-generated HWIO interface include file.

  This file contains HWIO register definitions for the following modules:
    MSS_QDSP6SS_PUB

  'Include' filters applied: MSS_QDSP6SS_STRAP_ACC 
  'Exclude' filters applied: RESERVED DUMMY 
*/
/*
  ===========================================================================

  Copyright (c) 2014 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

  ===========================================================================

  $Header: //components/rel/core.mpss/3.4.c3.11/systemdrivers/clock/hw/msm8996/mss/inc/ClockMSSHWIO.h#1 $
  $DateTime: 2016/03/28 23:02:17 $
  $Author: mplcsds1 $

  ===========================================================================
*/

#include <HALhwio.h>

/*
 * HWIO base definitions
 */
extern uint32                      HAL_clk_nHWIOBaseMSS;
#define MODEM_TOP_BASE             HAL_clk_nHWIOBaseMSS



/*----------------------------------------------------------------------------
 * MODULE: MSS_QDSP6SS_PUB
 *--------------------------------------------------------------------------*/

#define MSS_QDSP6SS_PUB_REG_BASE                                                (MODEM_TOP_BASE      + 0x00080000)
#define MSS_QDSP6SS_PUB_REG_BASE_OFFS                                           0x00080000

#define HWIO_MSS_QDSP6SS_STRAP_ACC_ADDR                                         (MSS_QDSP6SS_PUB_REG_BASE      + 0x00000110)
#define HWIO_MSS_QDSP6SS_STRAP_ACC_OFFS                                         (MSS_QDSP6SS_PUB_REG_BASE_OFFS + 0x00000110)
#define HWIO_MSS_QDSP6SS_STRAP_ACC_RMSK                                         0xffffffff
#define HWIO_MSS_QDSP6SS_STRAP_ACC_IN          \
        in_dword_masked(HWIO_MSS_QDSP6SS_STRAP_ACC_ADDR, HWIO_MSS_QDSP6SS_STRAP_ACC_RMSK)
#define HWIO_MSS_QDSP6SS_STRAP_ACC_INM(m)      \
        in_dword_masked(HWIO_MSS_QDSP6SS_STRAP_ACC_ADDR, m)
#define HWIO_MSS_QDSP6SS_STRAP_ACC_OUT(v)      \
        out_dword(HWIO_MSS_QDSP6SS_STRAP_ACC_ADDR,v)
#define HWIO_MSS_QDSP6SS_STRAP_ACC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_QDSP6SS_STRAP_ACC_ADDR,m,v,HWIO_MSS_QDSP6SS_STRAP_ACC_IN)
#define HWIO_MSS_QDSP6SS_STRAP_ACC_DATA_BMSK                                    0xffffffff
#define HWIO_MSS_QDSP6SS_STRAP_ACC_DATA_SHFT                                           0x0


#endif /* __CLOCKMSSHWIO_H__ */
