/*
==============================================================================

FILE:         HALclkCOXM.c

DESCRIPTION:
   This auto-generated file contains the clock HAL code for the 
   COXM clocks.

   List of clock domains:
     - HAL_clk_mMSSBITCOXMMNDClkDomain


   List of power domains:



==============================================================================

$Header: //components/rel/core.mpss/3.4.c3.11/systemdrivers/hal/clk/hw/msm8996/src/mss/HALclkCOXM.c#1 $

==============================================================================
            Copyright (c) 2014 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include <HALhwio.h>

#include "HALclkInternal.h"
#include "HALclkTest.h"
#include "HALclkGeneric.h"
#include "HALclkHWIO.h"


/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/


/* ============================================================================
**    Prototypes
** ==========================================================================*/


/* ============================================================================
**    Externs
** ==========================================================================*/

extern HAL_clk_ClockDomainControlType  HAL_clk_mMSSClockDomainControl;
extern HAL_clk_ClockDomainControlType  HAL_clk_mMSSClockDomainControlRO;


/* ============================================================================
**    Data
** ==========================================================================*/


/*                           
 *  HAL_clk_mBITCOXMMNDClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mBITCOXMMNDClkDomainClks[] =
{
  {
    /* .szClockName      = */ "clk_bit_coxm",
    /* .mRegisters       = */ { HWIO_OFFS(MSS_BIT_COXM_CBCR), 0, {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MSS_TEST_CLK_BIT_COXM
  },
};


/*
 * HAL_clk_mMSSBITCOXMMNDClkDomain
 *
 * BITCOXMMND clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mMSSBITCOXMMNDClkDomain =
{
  /* .nCGRAddr             = */ HWIO_OFFS(MSS_BIT_COXM_MND_CMD_RCGR),
  /* .pmClocks             = */ HAL_clk_mBITCOXMMNDClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mBITCOXMMNDClkDomainClks)/sizeof(HAL_clk_mBITCOXMMNDClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mMSSClockDomainControl,
  /* .pmNextClockDomain    = */ NULL
};

