/*
==============================================================================

FILE:         HALclkSNOC.c

DESCRIPTION:
   This auto-generated file contains the clock HAL code for the 
   XO clocks.

   List of clock domains:
     - HAL_clk_mMSSCLKSNOCClkDomain


   List of power domains:



==============================================================================

                             Edit History

$Header: //components/rel/core.mpss/3.4.c3.11/systemdrivers/hal/clk/hw/msm8996/src/mss/HALclkSNOC.c#1 $

when         who     what, where, why
----------   ---     ----------------------------------------------------------- 
07/09/2014           Auto-generated.


==============================================================================
            Copyright (c) 2014 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include <HALhwio.h>

#include "HALclkInternal.h"
#include "HALclkTest.h"
#include "HALclkGeneric.h"
#include "HALclkHWIO.h"


/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/


/* ============================================================================
**    Prototypes
** ==========================================================================*/


/* ============================================================================
**    Externs
** ==========================================================================*/

extern HAL_clk_ClockDomainControlType  HAL_clk_mMSSClockDomainControl;
extern HAL_clk_ClockDomainControlType  HAL_clk_mMSSClockDomainControlRO;


/* ============================================================================
**    Data
** ==========================================================================*/


/*                           
 *  HAL_clk_mCLKSNOCClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mCLKSNOCClkDomainClks[] =
{
  {
    /* .szClockName      = */ "clk_axi_dma",
    /* .mRegisters       = */ { HWIO_OFFS(MSS_AXI_DMA_CBCR), 0, {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ 0
  },
};


/*
 * HAL_clk_mMSSCLKSNOCClkDomain
 *
 * CLKSNOC clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mMSSCLKSNOCClkDomain =
{
  /* .nCGRAddr             = */ 0, /* this domain does not have a cmd rcgr */
  /* .pmClocks             = */ HAL_clk_mCLKSNOCClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mCLKSNOCClkDomainClks)/sizeof(HAL_clk_mCLKSNOCClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mMSSClockDomainControl,
  /* .pmNextClockDomain    = */ NULL
};

