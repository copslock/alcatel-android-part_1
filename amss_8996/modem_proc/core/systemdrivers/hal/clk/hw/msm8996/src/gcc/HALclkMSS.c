/*
==============================================================================

FILE:         HALclkMSS.c

DESCRIPTION:
   This auto-generated file contains the clock HAL code for the 
   MSS clocks.

   List of clock domains:
     - HAL_clk_mGCCMSSQ6BIMCAXIClkDomain


   List of power domains:



==============================================================================

$Header: //components/rel/core.mpss/3.4.c3.11/systemdrivers/hal/clk/hw/msm8996/src/gcc/HALclkMSS.c#1 $

==============================================================================
            Copyright (c) 2014 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include <HALhwio.h>

#include "HALclkInternal.h"
#include "HALclkTest.h"
#include "HALclkGeneric.h"
#include "HALclkHWIO.h"


/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/


/* ============================================================================
**    Prototypes
** ==========================================================================*/


/* ============================================================================
**    Externs
** ==========================================================================*/

extern HAL_clk_ClockDomainControlType  HAL_clk_mGCCClockDomainControl;
extern HAL_clk_ClockDomainControlType  HAL_clk_mGCCClockDomainControlRO;


/* ============================================================================
**    Data
** ==========================================================================*/


/*                           
 *  HAL_clk_mMSSQ6BIMCAXIClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mMSSQ6BIMCAXIClkDomainClks[] =
{
  {
    /* .szClockName      = */ "gcc_mss_q6_bimc_axi_clk",
    /* .mRegisters       = */ { HWIO_OFFS(GCC_MSS_Q6_BIMC_AXI_CBCR), 0, {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_GCC_TEST_GCC_MSS_Q6_BIMC_AXI_CLK
  },
  {
    /* .szClockName      = */ "gcc_mss_q6_msmpu_client_axi_clk",
    /* .mRegisters       = */ { HWIO_OFFS(GCC_MSS_Q6_MSMPU_CLIENT_AXI_CBCR), HWIO_OFFS(GCC_MSMPU_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_GCC_TEST_GCC_MSS_Q6_MSMPU_CLIENT_AXI_CLK
  },
  {
    /* .szClockName      = */ "gcc_mss_q6_smmu_clk",
    /* .mRegisters       = */ { HWIO_OFFS(GCC_MSS_Q6_SMMU_CBCR), HWIO_OFFS(GCC_MSS_Q6_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_GCC_TEST_GCC_MSS_Q6_SMMU_CLK
  },
};


/*
 * HAL_clk_mGCCMSSQ6BIMCAXIClkDomain
 *
 * MSSQ6BIMCAXI clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mGCCMSSQ6BIMCAXIClkDomain =
{
  /* .nCGRAddr             = */ HWIO_OFFS(GCC_MSS_Q6_BIMC_AXI_CMD_RCGR),
  /* .pmClocks             = */ HAL_clk_mMSSQ6BIMCAXIClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mMSSQ6BIMCAXIClkDomainClks)/sizeof(HAL_clk_mMSSQ6BIMCAXIClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mGCCClockDomainControl,
  /* .pmNextClockDomain    = */ NULL
};

