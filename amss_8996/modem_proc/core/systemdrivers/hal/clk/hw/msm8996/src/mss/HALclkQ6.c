/*
==============================================================================

FILE:         HALclkQ6.c

DESCRIPTION:
   This auto-generated file contains the clock HAL code for the 
   Q6 clocks.

   List of clock domains:


   List of power domains:



==============================================================================

$Header: //components/rel/core.mpss/3.4.c3.11/systemdrivers/hal/clk/hw/msm8996/src/mss/HALclkQ6.c#1 $

==============================================================================
            Copyright (c) 2014 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include <HALhwio.h>

#include "HALclkInternal.h"
#include "HALclkTest.h"
#include "HALclkGeneric.h"
#include "HALclkHWIO.h"


/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/


/* ============================================================================
**    Prototypes
** ==========================================================================*/

static boolean HAL_clk_Q6CoreClkIsEnabled    ( HAL_clk_ClockDescType  *pmClockDesc );
static boolean HAL_clk_Q6CoreClkIsOn         ( HAL_clk_ClockDescType  *pmClockDesc );
static void    HAL_clk_Q6CoreClkConfig       ( HAL_clk_ClockDescType  *pmClockDesc, HAL_clk_ClockConfigType eConfig );
static void    HAL_clk_Q6CoreConfigDivider   ( HAL_clk_ClockDescType  *pmClockDesc, uint32 nDiv2x );
static void    HAL_clk_Q6ConfigMux           ( HAL_clk_ClockDomainDescType *pmClockDomainDesc, const HAL_clk_ClockMuxConfigType *pmConfig);

/* ============================================================================
**    Externs
** ==========================================================================*/

extern HAL_clk_ClockDomainControlType  HAL_clk_mMSSClockDomainControl;
extern HAL_clk_ClockDomainControlType  HAL_clk_mMSSClockDomainControlRO;


/* ============================================================================
**    Data
** ==========================================================================*/


/*
 * aCLKQ6SourceMap
 *
 * CLKQ6 HW source mapping
 * 
 * NOTES:
 * - HAL_clk_SourceMapType is an array of mapped sources
 *   - see HALclkInternal.h.
 *
 * - If source index is reserved/not used in a clock diagram, please tie that
 *   to HAL_CLK_SOURCE_GROUND.
 *
 * - {HAL_CLK_SOURCE_NULL, HAL_CLK_SOURCE_INDEX_INVALID} is used to indicate
 *   the end of the mapping array. If we reach this element during our lookup,
 *   we'll know we could not find the matching source enum for the register
 *   value, or vice versa.
 * 
 */
static HAL_clk_SourceMapType aQ6SourceMap[] =
{
  {HAL_CLK_SOURCE_XO,                    0},
  {HAL_CLK_SOURCE_MPLL3,                 1},
  {HAL_CLK_SOURCE_PLLTEST,               7},
  {HAL_CLK_SOURCE_NULL,                  HAL_CLK_SOURCE_INDEX_INVALID}
};


/*
 * HAL_clk_mQ6ClockDomainControl
 *
 * Functions for controlling Q6 clock domains
 */
HAL_clk_ClockDomainControlType HAL_clk_mQ6ClockDomainControl =
{
   /* .ConfigMux          = */ HAL_clk_Q6ConfigMux,
   /* .DetectMuxConfig    = */ HAL_clk_GenericDetectMuxConfig,
   /* .pmSourceMap        = */ aQ6SourceMap
};


/*
 * HAL_clk_mQ6ClockControl
 *
 * Functions for controlling Q6 clock functions.
 */
HAL_clk_ClockControlType HAL_clk_mQ6ClockControl =
{
  /* .Enable           = */ NULL,
  /* .Disable          = */ NULL,
  /* .IsEnabled        = */ HAL_clk_Q6CoreClkIsEnabled,
  /* .IsOn             = */ HAL_clk_Q6CoreClkIsOn,
  /* .Reset            = */ NULL,
  /* .IsReset          = */ NULL,
  /* .Config           = */ HAL_clk_Q6CoreClkConfig,
  /* .DetectConfig     = */ NULL,
  /* .ConfigDivider    = */ HAL_clk_Q6CoreConfigDivider,
  /* .DetectDivider    = */ NULL,
  /* .ConfigFootswitch = */ NULL,
};


/*                           
 *  HAL_clk_mCLKQ6IMAGEClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mQ6CoreClkDomainClks[] =
{
  {
    /* .szClockName      = */ "clk_q6",
    /* .mRegisters       = */ { 0, 0, {0, 0} },
    /* .pmControl        = */ &HAL_clk_mQ6ClockControl,
    /* .nTestClock       = */ HAL_CLK_MSS_TEST_CLK_Q6
  },
};


/*
 * HAL_clk_mMSSQ6ClkDomain
 *
 * Q6 clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mMSSQ6ClkDomain =
{
  /* .nCGRAddr             = */ HWIO_OFFS(MSS_QDSP6SS_CORE_CMD_RCGR),
  /* .pmClocks             = */ HAL_clk_mQ6CoreClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mQ6CoreClkDomainClks)/sizeof(HAL_clk_mQ6CoreClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mQ6ClockDomainControl,
  /* .pmNextClockDomain    = */ NULL
};


/*============================================================================

               FUNCTION DEFINITIONS FOR MODULE

============================================================================*/


/* ===========================================================================
**  HAL_clk_Q6CoreClkIsEnabled
**
** ======================================================================== */

boolean HAL_clk_Q6CoreClkIsEnabled
(
  HAL_clk_ClockDescType  *pmClockDesc
)
{
  /*
   * If this function is being called then Q6 core clock has to be enabled.
   * NOTE: We can't check ROOT_EN bit since enable is auto set by the HW.
   */
  return TRUE;

}  /* END HAL_clk_Q6CoreClkIsEnabled */


/* ===========================================================================
**  HAL_clk_Q6CoreClkIsOn
**
** ======================================================================== */

boolean HAL_clk_Q6CoreClkIsOn
(
  HAL_clk_ClockDescType  *pmClockDesc
)
{
  if(HWIO_INF(MSS_QDSP6SS_CORE_CMD_RCGR, ROOT_OFF))
  {
    return FALSE;
  }
  else
  {
    return TRUE;
  }
}  /* END HAL_clk_Q6CoreClkIsOn */


/* ===========================================================================
**  HAL_clk_Q6CoreClkConfig
**
** ======================================================================== */

void HAL_clk_Q6CoreClkConfig
(
  HAL_clk_ClockDescType   *pmClockDesc,
  HAL_clk_ClockConfigType eConfig
)
{
  uint32 nSrcSel;

  switch (eConfig)
  {
    case HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX:
      nSrcSel = 0x0;
      break;

    case HAL_CLK_CONFIG_Q6SS_CORE_PLL_MAIN:
      nSrcSel = 0x1;
      break;


    default:
      return;
  }

  HWIO_OUTF(MSS_QDSP6SS_GFMUX_CTL, CLK_SRC_SEL, nSrcSel);

}  /* END HAL_clk_Q6CoreClkConfig */


/* ===========================================================================
**  HAL_clk_Q6CoreConfigDivider
**
** ======================================================================== */

void HAL_clk_Q6CoreConfigDivider
(
  HAL_clk_ClockDescType *pmClockDesc,
  uint32                 nDiv2x
)
{
  /*
   * Update the divider.
   */
 if(nDiv2x <= 1)
 {
   nDiv2x = 0;
 }
 else
 {
   nDiv2x--;
 }

  HWIO_OUTF(MSS_QDSP6SS_CORE_CFG_RCGR, SRC_DIV, nDiv2x);

  /*
   * Trigger the update
   */
  HWIO_OUTF(MSS_QDSP6SS_CORE_CMD_RCGR, UPDATE, 1);

  /*
   * Wait until update finishes
   */
  while(HWIO_INF(MSS_QDSP6SS_CORE_CMD_RCGR, UPDATE));

} /* END HAL_clk_Q6CoreConfigDivider */


/* ===========================================================================
**  HAL_clk_Q6ConfigMux
**
** ======================================================================== */

static void HAL_clk_Q6ConfigMux
(
  HAL_clk_ClockDomainDescType      *pmClockDomainDesc,
  const HAL_clk_ClockMuxConfigType *pmConfig
)
{
  /*
   * The Q6 clock can be temporarily disabled during the AXIM2 bus isolation
   * sequence, which will feed back and disable the RCG. If this happens
   * at the same time the UPDATE bit is set, then the RCG will lock up and
   * the UPDATE bit will never clear. To prevent this, manually force the
   * RCG root on during configuration.
   */
  HWIO_OUTF(MSS_QDSP6SS_CORE_CMD_RCGR, ROOT_EN, 1);
  HAL_clk_GenericConfigMux(pmClockDomainDesc, pmConfig);
  HWIO_OUTF(MSS_QDSP6SS_CORE_CMD_RCGR, ROOT_EN, 0);
}

