/*
==============================================================================

FILE:         HALclkNAVSS.c

DESCRIPTION:
   This auto-generated file contains the clock HAL code for the 
   NAVSS clocks.

   List of clock domains:


   List of power domains:



==============================================================================

$Header: //components/rel/core.mpss/3.4.c3.11/systemdrivers/hal/clk/hw/msm8996/src/mss/HALclkNAVSS.c#1 $

==============================================================================
            Copyright (c) 2014 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include <HALhwio.h>

#include "HALclkInternal.h"
#include "HALclkTest.h"
#include "HALclkGeneric.h"
#include "HALclkHWIO.h"


/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/


/* ============================================================================
**    Prototypes
** ==========================================================================*/


/* ============================================================================
**    Externs
** ==========================================================================*/

extern HAL_clk_ClockDomainControlType  HAL_clk_mMSSClockDomainControl;
extern HAL_clk_ClockDomainControlType  HAL_clk_mMSSClockDomainControlRO;


/* ============================================================================
**    Data
** ==========================================================================*/


/*                           
 *  HAL_clk_mCLKNAVClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mCLKNAVClkDomainClks[] =
{
  {
    /* .szClockName      = */ "clk_axi_nav",
    /* .mRegisters       = */ { HWIO_OFFS(MSS_AXI_NAV_CBCR), 0, {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_MSS_TEST_CLK_AXI_NAV
  },
};


/*
 * HAL_clk_mMSSCLKNAVClkDomain
 *
 * CLKNAV clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mMSSCLKNAVClkDomain =
{
  /* .nCGRAddr             = */ 0, /* this domain does not have a cmd rcgr */
  /* .pmClocks             = */ HAL_clk_mCLKNAVClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mCLKNAVClkDomainClks)/sizeof(HAL_clk_mCLKNAVClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mMSSClockDomainControl,
  /* .pmNextClockDomain    = */ NULL
};

