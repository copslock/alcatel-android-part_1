#===============================================================================
#
# HAL LDO CHIPSET LIBRARY
#
# GENERAL DESCRIPTION
#    build script
#
# Copyright (c) 2014 Qualcomm Technologies Incorporated.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#
#-------------------------------------------------------------------------------
#
#  $Header: //components/rel/core.mpss/3.4.c3.11/systemdrivers/hal/ldo/hw/msm8996/build/halldo_chipset.scons#1 $
#  $DateTime: 2016/03/28 23:02:17 $
#  $Author: mplcsds1 $
#  $Change: 10156097 $
#
#===============================================================================

import os
Import('env')
env = env.Clone()

#-------------------------------------------------------------------------------
# Source PATH
#-------------------------------------------------------------------------------

SRCPATH = "../"
env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0) 

#-------------------------------------------------------------------------------
# Internal depends within CoreBSP
#-------------------------------------------------------------------------------

CBSP_API = [
   'HAL',
   'DAL',
   'DEBUGTOOLS',

   # needs to be last also contains wrong comdef.h
   'KERNEL',
]

env.RequirePublicApi(CBSP_API)
env.RequireRestrictedApi(CBSP_API)

HAL_LDO_CHIPSET_BUILD_ROOT = os.getcwd();

env.PublishPrivateApi("SYSTEMDRIVERS_HAL_LDO_CHIPSET", [
  HAL_LDO_CHIPSET_BUILD_ROOT + "/../inc",
])

#-------------------------------------------------------------------------------
# Sources: HALLDO Chipset Sources
#-------------------------------------------------------------------------------

HALLDO_CHIPSET_SOURCES = [
    '${BUILDPATH}/src/mss/HALldoQ6.c',
    '${BUILDPATH}/src/HALldoMain.c',
]

#-------------------------------------------------------------------------------
# Sources: HALLDOChipset Library Creation
#-------------------------------------------------------------------------------

env.AddLibrary(
   ['CORE_MODEM'],
   '${BUILDPATH}/HALldo_chipset',
   HALLDO_CHIPSET_SOURCES)

#-------------------------------------------------------------------------------
# HWIO
#-------------------------------------------------------------------------------

if env.has_key('HWIO_IMAGE'):

  env.AddHWIOFile('HWIO', [
    {
      'filename': '${INC_ROOT}/core/systemdrivers/hal/ldo/hw/${CHIPSET}/inc/HALldoHWIO.h',
      'modules': ['MPSS_PERPH', 'MSS_QDSP6SS_PUB'],
      'filter-include': [
        'MSS_QDSP6SS_PWR_CTL',
        'MSS_QDSP6SS_LDO_CFG0',
        'MSS_QDSP6SS_LDO_CFG1',
        'MSS_QDSP6SS_LDO_CFG2',
        'MSS_QDSP6SS_LDO_VREF_TEST_CFG',
        'MSS_QDSP6SS_LDO_VREF_CMD',
        'MSS_QDSP6SS_LDO_VREF_SET',
        'MSS_QDSP6SS_QMC_SVS_CTL'],
      'filter-exclude': ['RESERVED', 'RESERVE', 'DUMMY'],
      'output-offsets': True,
      'header':
        '/*\n'
        ' * HWIO base definitions\n'
        ' */\n'
        'extern uint32                      HAL_ldo_nHWIOBaseMSS;\n' +
        '#define MODEM_TOP_BASE             HAL_ldo_nHWIOBaseMSS\n' +
        '#define MODEM_TOP_BASE_PHYS        0x02000000\n' +
        '#define MODEM_TOP_BASE_SIZE        0x01000000\n\n' +
        'extern uint32                      HAL_ldo_nHWIOBaseSecurity;\n' +
        '#define SECURITY_CONTROL_BASE      HAL_ldo_nHWIOBaseSecurity\n' +
        '#define SECURITY_CONTROL_BASE_PHYS 0x00070000\n' +
        '#define SECURITY_CONTROL_BASE_SIZE 0x00010000',
    }
  ])

