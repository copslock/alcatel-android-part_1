/*
==============================================================================

FILE:         HALldoQ6.c

DESCRIPTION:
   This file contains the LDO HAL code for the QDSP6 core clock.


==============================================================================

                             Edit History

$Header: //components/rel/core.mpss/3.4.c3.11/systemdrivers/hal/ldo/hw/msm8996/src/mss/HALldoQ6.c#1 $

when          who     what, where, why
--------      ---     ----------------------------------------------------------- 


==============================================================================
            Copyright (c) 2014 Qualcomm Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include <HALhwio.h>
#include "HALldoInternal.h"
#include "HALldoHWIO.h"


/* ============================================================================
**    Prototypes
** ==========================================================================*/

static void HAL_ldo_Q6LDOConfig              ( void );
static void HAL_ldo_Q6LDOEnable              ( void );
static void HAL_ldo_Q6LDODisable             ( void );
static void HAL_ldo_Q6LDOSetVoltage          ( uint32 nLDOVoltageUV );
static void HAL_ldo_Q6LDOSetRetentionVoltage ( uint32 nLDOVoltageUV );
static void HAL_ldo_Q6LDOSetAutoBypass       ( boolean bEnable);

/*=========================================================================
      Global Variables
==========================================================================*/


/*=========================================================================
      Macros
==========================================================================*/


/*
 * Value for LDO_CFG0[LDO_CTL1] per the QDSP6SS HPG.
 */
#define HAL_LDO_CTL1_CONFIG                  0x86

/*
 * Value for LDO_CFG1[LDO_VREF_TRIM_SW] per the QDSP6SS HPG.
 */
#define HAL_LDO_LDO_VREF_TRIM_SW_CONFIG      0x10

/*
 * Value for LDO_CFG1[DIG_CTL] per the QDSP6SS HPG.
 */
#define HAL_LDO_DIG_CTL_CONFIG               0x01

/*
 * Value for LDO_CFG2[REF_CTL] per the QDSP6SS HPG.
 */
#define HAL_LDO_REF_CTL_CONFIG               0xC0

/*
 * Value for LDO_CFG2[IDAC_CTL] per the QDSP6SS HPG.
 */
#define HAL_LDO_IDAC_CTL_CONFIG              0x08

/*
 * Value for LDO_VREG_TEST_CFG[OSC_CTL] per the QDSP6SS HPG.
 */
#define HAL_LDO_OSC_CTL_CONFIG               0x01

/*
 * Value for when setting Q6 compiler memory to SVS mode.
 */
#define Q6_COMPILER_MEM_SVS_MODE    0x3

/*
 * Min/Max voltage values for the LDO operation range.
 * These voltages need to be converted to LDO reg format when programmed.
 */
#define HAL_LDO_VREF_MIN_UV  345 * 1000
#define HAL_LDO_VREF_MAX_UV  980 * 1000

/*
 * LDO voltage delta.
 */
#define HAL_LDO_DELTA(val1, val2)                                            \
  ( (val1) > (val2) ? (val1) - (val2) : (val2) - (val1) )

/*
 * LDO range checker.
 */
#define HAL_LDO_IN_RANGE(x)                                                  \
  ( ((x) >= HAL_LDO_VREF_MIN_UV) && ((x) <= HAL_LDO_VREF_MAX_UV) )

/*
 * LDO uV to LDO_REF register format.
 */
#define HAL_LDO_UV_MAP_TO_HW(x)                                              \
  ( ( ( (x) - HAL_LDO_VREF_MIN_UV ) / 1000 ) / 5 ) + 1

/*
 * Set wait time to the equivalent of 5 us per 10 mV.
 * This value is expected to change when data becomes available.
 */
#define HAL_LDO_SETTLE_DELAY(start, end)                                     \
  HAL_ldo_BusyWait(5 * ( ( ( HAL_LDO_DELTA(start, end) + (10000 - 1) ) / 10000) ) )


/*
 * Macros for accessing the eLDO autobypass bit
 * These macros are manually created here b/c they are not individually defined
 * in the current FLAT file used to generate the HWIOs
 */
#define HWIO_MSS_QDSP6SS_LDO_VREF_TEST_CFG_AUTOBYP_BMSK    0x00100000
#define HWIO_MSS_QDSP6SS_LDO_VREF_TEST_CFG_AUTOBYP_SHFT    0x14


/* ============================================================================
**    Data
** ==========================================================================*/


/*
 * HAL_ldo_mQ6LDOControl
 *
 * Functions for controlling the Q6 LDO
 */
HAL_ldo_LDOControlType HAL_ldo_mQ6LDOControl =
{
  /* .Config              = */ HAL_ldo_Q6LDOConfig,
  /* .Enable              = */ HAL_ldo_Q6LDOEnable,
  /* .Disable             = */ HAL_ldo_Q6LDODisable,
  /* .SetVoltage          = */ HAL_ldo_Q6LDOSetVoltage,
  /* .SetRetentionVoltage = */ HAL_ldo_Q6LDOSetRetentionVoltage,
  /* .SetAutoBypass       = */ HAL_ldo_Q6LDOSetAutoBypass
};


/*============================================================================

               FUNCTION DEFINITIONS FOR MODULE

============================================================================*/


/* =========================================================================
**  HAL_ldo_Q6LDOConfig
** =========================================================================*/
/**
  Program the Q6 LDO config registers.

  @return
  None.

  @dependencies
  None.
*/
static void HAL_ldo_Q6LDOConfig
(
  void
)
{
  uint32 nRegVal;

  /*-----------------------------------------------------------------------*/
  /* Clear the LDO_VREF_SEL_RST once at config time (POR value is 1).      */
  /*-----------------------------------------------------------------------*/

  HWIO_OUTF(MSS_QDSP6SS_LDO_VREF_SET, LDO_VREF_SEL_RST, 0);

  /*-----------------------------------------------------------------------*/
  /* Program LDO_CFG0 based on the Q6SS HPG recommended values.            */
  /*-----------------------------------------------------------------------*/

  nRegVal = HWIO_IN(MSS_QDSP6SS_LDO_CFG0);

  nRegVal |= HWIO_FMSK(MSS_QDSP6SS_LDO_CFG0, IDAC_EN);

  nRegVal &= ~HWIO_FMSK(MSS_QDSP6SS_LDO_CFG0, LDO_CTL1);
  nRegVal |= (HAL_LDO_CTL1_CONFIG << HWIO_SHFT(MSS_QDSP6SS_LDO_CFG0, LDO_CTL1))
               & HWIO_FMSK(MSS_QDSP6SS_LDO_CFG0, LDO_CTL1);

  HWIO_OUT(MSS_QDSP6SS_LDO_CFG0, nRegVal);

  /*-----------------------------------------------------------------------*/
  /* Program LDO_CFG1 based on the Q6SS HPG recommended values.            */
  /*-----------------------------------------------------------------------*/

  HWIO_OUTF(MSS_QDSP6SS_LDO_CFG1, LDO_VREF_TRIM_SEL, 1);
  HWIO_OUTF(MSS_QDSP6SS_LDO_CFG1, LDO_VREF_TRIM_SW, HAL_LDO_LDO_VREF_TRIM_SW_CONFIG);
  HWIO_OUTF(MSS_QDSP6SS_LDO_CFG1, DIG_CTL, HAL_LDO_DIG_CTL_CONFIG);

  /*-----------------------------------------------------------------------*/
  /* Program LDO_CFG2 based on the Q6SS HPG recommended values.            */
  /*-----------------------------------------------------------------------*/

  nRegVal = HWIO_IN(MSS_QDSP6SS_LDO_CFG2);

  nRegVal &= ~HWIO_FMSK(MSS_QDSP6SS_LDO_CFG2, REF_CTL);
  nRegVal |=
    (HAL_LDO_REF_CTL_CONFIG << HWIO_SHFT(MSS_QDSP6SS_LDO_CFG2, REF_CTL))
      & HWIO_FMSK(MSS_QDSP6SS_LDO_CFG2, REF_CTL);

  nRegVal &= ~HWIO_FMSK(MSS_QDSP6SS_LDO_CFG2, IDAC_CTL);
  nRegVal |=
    (HAL_LDO_IDAC_CTL_CONFIG << HWIO_SHFT(MSS_QDSP6SS_LDO_CFG2, IDAC_CTL))
      & HWIO_FMSK(MSS_QDSP6SS_LDO_CFG2, IDAC_CTL);

  HWIO_OUT(MSS_QDSP6SS_LDO_CFG2, nRegVal);

  /*-----------------------------------------------------------------------*/
  /* Program LDO_VREG_TEST_CFG based on the Q6SS HPG recommended values.   */
  /*-----------------------------------------------------------------------*/

  nRegVal = HWIO_IN(MSS_QDSP6SS_LDO_VREF_TEST_CFG);

  nRegVal &= ~HWIO_FMSK(MSS_QDSP6SS_LDO_VREF_TEST_CFG, OSC_CTL);
  nRegVal |=
    (HAL_LDO_OSC_CTL_CONFIG << HWIO_SHFT(MSS_QDSP6SS_LDO_VREF_TEST_CFG, OSC_CTL))
      & HWIO_FMSK(MSS_QDSP6SS_LDO_VREF_TEST_CFG, OSC_CTL);

  HWIO_OUT(MSS_QDSP6SS_LDO_VREF_TEST_CFG, nRegVal);

  /*-----------------------------------------------------------------------*/
  /* Configure VREF_SET to select the LDO (as opposed to retention).       */
  /*-----------------------------------------------------------------------*/

  HWIO_OUTF(MSS_QDSP6SS_LDO_VREF_SET, LDO_VREF_SEL, 0x0);

} /* END HAL_ldo_Q6LDOConfig */


/* ===========================================================================
**  HAL_ldo_Q6LDOEnable
**
** ======================================================================== */
/**
  Enable the MSS Q6 LDO.

  This function is used to switch to LDO mode.  If already in LDO mode,
  it will simply return.

  @return
  None.

  @dependencies
  None.
*/

static void HAL_ldo_Q6LDOEnable
(
  void
)
{
  uint32 nLDOCfg0RegVal, nPwrCtlRegVal;

  /*-----------------------------------------------------------------------*/
  /* Only switch to LDO if in BHS mode.                                    */
  /*-----------------------------------------------------------------------*/

  nPwrCtlRegVal = HWIO_IN(MSS_QDSP6SS_PWR_CTL);
  if(nPwrCtlRegVal & HWIO_FMSK(MSS_QDSP6SS_PWR_CTL, BHS_ON))
  {
    /*
     * Switch compiler memory to low power mode.
     */
    HWIO_OUT(MSS_QDSP6SS_QMC_SVS_CTL, Q6_COMPILER_MEM_SVS_MODE);

    /*
     * The mode bit inside LDO_CTL1 needs to be set during the switch.
     */
    nLDOCfg0RegVal = HWIO_IN(MSS_QDSP6SS_LDO_CFG0);
    nLDOCfg0RegVal &= ~HWIO_FMSK(MSS_QDSP6SS_LDO_CFG0, LDO_CTL1);
    nLDOCfg0RegVal |=
      (HAL_LDO_CTL1_CONFIG << HWIO_SHFT(MSS_QDSP6SS_LDO_CFG0, LDO_CTL1))
        & HWIO_FMSK(MSS_QDSP6SS_LDO_CFG0, LDO_CTL1);
    HWIO_OUT(MSS_QDSP6SS_LDO_CFG0, nLDOCfg0RegVal);

    /*
     * Turn on LDO by setting LDO_PWR_UP.
     */
    nPwrCtlRegVal |= HWIO_FMSK(MSS_QDSP6SS_PWR_CTL, LDO_PWR_UP);
    HWIO_OUT(MSS_QDSP6SS_PWR_CTL, nPwrCtlRegVal);

    /*
     * Wait for 10 us.
     */
    HAL_ldo_BusyWait(10);

    /*
     * Turn off BHS and LDO Bypass.
     */
    nPwrCtlRegVal &= ~HWIO_FMSK(MSS_QDSP6SS_PWR_CTL, BHS_ON);
    nPwrCtlRegVal &= ~HWIO_FMSK(MSS_QDSP6SS_PWR_CTL, LDO_BYP);
    HWIO_OUT(MSS_QDSP6SS_PWR_CTL, nPwrCtlRegVal);

    /*
     * Clear the mode bit inside LDO_CTL1 after switching to LDO mode from BHS.
     */
    nLDOCfg0RegVal &= ~HWIO_FMSK(MSS_QDSP6SS_LDO_CFG0, LDO_CTL1);
    HWIO_OUT(MSS_QDSP6SS_LDO_CFG0, nLDOCfg0RegVal);
  }

} /* END HAL_ldo_Q6LDOEnable */


/* =========================================================================
**  HAL_ldo_Q6LDODisable
** =========================================================================*/
/**
  Turn off the MSS Q6 LDO.

  This function is used to switch to BHS mode.  If already in BHS mode,
  it will simply return.

  @return
  None.

  @dependencies
  None.
*/
static void HAL_ldo_Q6LDODisable
(
  void
)
{
  uint32 nPwrCtlRegVal;

  /*-----------------------------------------------------------------------*/
  /* Only switch to BHS if we aren't already in BHS mode.                  */
  /*-----------------------------------------------------------------------*/

  nPwrCtlRegVal = HWIO_IN(MSS_QDSP6SS_PWR_CTL);
  if ((nPwrCtlRegVal & HWIO_FMSK(MSS_QDSP6SS_PWR_CTL, BHS_ON)) == 0)
  {
    /*
     * Turn on BHS from LDO mode.
     */
    nPwrCtlRegVal |= HWIO_FMSK(MSS_QDSP6SS_PWR_CTL, BHS_ON);
    HWIO_OUT(MSS_QDSP6SS_PWR_CTL, nPwrCtlRegVal);

    /*
     * Wait 1 uS for the BHS to turn on.
     */
    HAL_ldo_BusyWait(1);

    /*
     * Put LDO in bypass.
     */
    nPwrCtlRegVal |= HWIO_FMSK(MSS_QDSP6SS_PWR_CTL, LDO_BYP);
    HWIO_OUT(MSS_QDSP6SS_PWR_CTL, nPwrCtlRegVal);

    /*
     * Turn off LDO.
     */
    nPwrCtlRegVal &= ~HWIO_FMSK(MSS_QDSP6SS_PWR_CTL, LDO_PWR_UP);
    HWIO_OUT(MSS_QDSP6SS_PWR_CTL, nPwrCtlRegVal);

    /*
     * Switch compiler memory away from low power mode.
     */
    HWIO_OUT(MSS_QDSP6SS_QMC_SVS_CTL, 0x0);
  }

} /* HAL_ldo_LDODisable */


/* =========================================================================
**  HAL_ldo_Q6LDODSetVoltage
** =========================================================================*/
/**
  Set the MSS Q6 LDO to required voltage.

  This function does not enable or disable the LDO. It simply updates the
  voltage.

  @return
  None.

  @dependencies
  None.
*/
static void HAL_ldo_Q6LDOSetVoltage
(
  uint32 nLDOVoltageUV
)
{
  uint32 nLDOVoltageData;

  /*
   * Program the new operating voltage data to the LDO VREF register.
   */
  nLDOVoltageData = HAL_LDO_UV_MAP_TO_HW(nLDOVoltageUV);
  HWIO_OUTF(MSS_QDSP6SS_LDO_VREF_SET, VREF_LDO, nLDOVoltageData);
  HWIO_OUT(MSS_QDSP6SS_LDO_VREF_CMD, 1);

} /* END HAL_ldo_Q6LDOSetVoltage */


/* =========================================================================
**  HAL_ldo_Q6LDOSetRetentionVoltage
** =========================================================================*/
/**
  Set the MSS Q6 LDO retention voltage.

  @return
  None.

  @dependencies
  None.
*/
static void HAL_ldo_Q6LDOSetRetentionVoltage
(
  uint32 nLDOVoltageUV
)
{
  uint32 nLDOVoltageData;

  /*
   * Program the new operating voltage data to the LDO VREF register.
   */
  nLDOVoltageData = HAL_LDO_UV_MAP_TO_HW(nLDOVoltageUV);
  HWIO_OUTF(MSS_QDSP6SS_LDO_VREF_SET, VREF_RET, nLDOVoltageData);

} /* END HAL_ldo_Q6LDOSetRetentionVoltage */


/* ===========================================================================
**  HAL_ldo_Q6LDOSetAutoBypass
**
** ======================================================================== */
/**
  Enable the MSS Q6 LDO autobypass

  This function is used to enable/disable the LDO autobypass ctrl bit.


  @return
  None.

  @dependencies
  None.
*/

static void HAL_ldo_Q6LDOSetAutoBypass
(
  boolean bEnable
)
{
  HWIO_OUTF(MSS_QDSP6SS_LDO_VREF_TEST_CFG, AUTOBYP, bEnable);

} /* END HAL_ldo_Q6LDOSetAutoBypass */


/* ===========================================================================
**  HAL_ldo_PlatformInitQ6
**
** ======================================================================== */

void HAL_ldo_PlatformInitQ6
(
  void
)
{

  /*
   * Install all MSS LDOs.
   */
  HAL_ldo_InstallLDO(HAL_LDO_MSS_Q6, &HAL_ldo_mQ6LDOControl);

} /* END HAL_ldo_PlatformInitMSSMain */


