#ifndef __HALLDOHWIO_H__
#define __HALLDOHWIO_H__
/*
===========================================================================
*/
/**
  @file HALldoHWIO.h
  @brief Auto-generated HWIO interface include file.

  This file contains HWIO register definitions for the following modules:
    MPSS_PERPH
    MSS_QDSP6SS_PUB

  'Include' filters applied: MSS_QDSP6SS_PWR_CTL MSS_QDSP6SS_LDO_CFG0 MSS_QDSP6SS_LDO_CFG1 MSS_QDSP6SS_LDO_CFG2 MSS_QDSP6SS_LDO_VREF_TEST_CFG MSS_QDSP6SS_LDO_VREF_CMD MSS_QDSP6SS_LDO_VREF_SET 
  'Exclude' filters applied: RESERVED RESERVE DUMMY 
*/
/*
  ===========================================================================

  Copyright (c) 2014 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

  ===========================================================================

  $Header: //components/rel/core.mpss/3.4.c3.11/systemdrivers/hal/ldo/hw/msm8996/inc/HALldoHWIO.h#1 $
  $DateTime: 2016/03/28 23:02:17 $
  $Author: mplcsds1 $

  ===========================================================================
*/

/*
 * HWIO base definitions
 */
extern uint32                      HAL_ldo_nHWIOBaseMSS;
#define MODEM_TOP_BASE             HAL_ldo_nHWIOBaseMSS
#define MODEM_TOP_BASE_PHYS        0x02000000
#define MODEM_TOP_BASE_SIZE        0x01000000

extern uint32                      HAL_ldo_nHWIOBaseSecurity;
#define SECURITY_CONTROL_BASE      HAL_ldo_nHWIOBaseSecurity
#define SECURITY_CONTROL_BASE_PHYS 0x00070000
#define SECURITY_CONTROL_BASE_SIZE 0x00010000

/*----------------------------------------------------------------------------
 * MODULE: MPSS_PERPH
 *--------------------------------------------------------------------------*/

#define MPSS_PERPH_REG_BASE                                                   (MODEM_TOP_BASE      + 0x001a8000)
#define MPSS_PERPH_REG_BASE_OFFS                                              0x001a8000

/*----------------------------------------------------------------------------
 * MODULE: MSS_QDSP6SS_PUB
 *--------------------------------------------------------------------------*/

#define MSS_QDSP6SS_PUB_REG_BASE                                                (MODEM_TOP_BASE      + 0x00080000)
#define MSS_QDSP6SS_PUB_REG_BASE_OFFS                                           0x00080000

#define HWIO_MSS_QDSP6SS_PWR_CTL_ADDR                                           (MSS_QDSP6SS_PUB_REG_BASE      + 0x00000030)
#define HWIO_MSS_QDSP6SS_PWR_CTL_OFFS                                           (MSS_QDSP6SS_PUB_REG_BASE_OFFS + 0x00000030)
#define HWIO_MSS_QDSP6SS_PWR_CTL_RMSK                                            0x77c0000
#define HWIO_MSS_QDSP6SS_PWR_CTL_IN          \
        in_dword_masked(HWIO_MSS_QDSP6SS_PWR_CTL_ADDR, HWIO_MSS_QDSP6SS_PWR_CTL_RMSK)
#define HWIO_MSS_QDSP6SS_PWR_CTL_INM(m)      \
        in_dword_masked(HWIO_MSS_QDSP6SS_PWR_CTL_ADDR, m)
#define HWIO_MSS_QDSP6SS_PWR_CTL_OUT(v)      \
        out_dword(HWIO_MSS_QDSP6SS_PWR_CTL_ADDR,v)
#define HWIO_MSS_QDSP6SS_PWR_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_QDSP6SS_PWR_CTL_ADDR,m,v,HWIO_MSS_QDSP6SS_PWR_CTL_IN)
#define HWIO_MSS_QDSP6SS_PWR_CTL_LDO_PWR_UP_BMSK                                 0x4000000
#define HWIO_MSS_QDSP6SS_PWR_CTL_LDO_PWR_UP_SHFT                                      0x1a
#define HWIO_MSS_QDSP6SS_PWR_CTL_LDO_BYP_BMSK                                    0x2000000
#define HWIO_MSS_QDSP6SS_PWR_CTL_LDO_BYP_SHFT                                         0x19
#define HWIO_MSS_QDSP6SS_PWR_CTL_BHS_ON_BMSK                                     0x1000000
#define HWIO_MSS_QDSP6SS_PWR_CTL_BHS_ON_SHFT                                          0x18
#define HWIO_MSS_QDSP6SS_PWR_CTL_CLAMP_QMC_MEM_BMSK                               0x400000
#define HWIO_MSS_QDSP6SS_PWR_CTL_CLAMP_QMC_MEM_SHFT                                   0x16
#define HWIO_MSS_QDSP6SS_PWR_CTL_CLAMP_WL_BMSK                                    0x200000
#define HWIO_MSS_QDSP6SS_PWR_CTL_CLAMP_WL_SHFT                                        0x15
#define HWIO_MSS_QDSP6SS_PWR_CTL_CLAMP_IO_BMSK                                    0x100000
#define HWIO_MSS_QDSP6SS_PWR_CTL_CLAMP_IO_SHFT                                        0x14
#define HWIO_MSS_QDSP6SS_PWR_CTL_SLP_RET_N_BMSK                                    0x80000
#define HWIO_MSS_QDSP6SS_PWR_CTL_SLP_RET_N_SHFT                                       0x13
#define HWIO_MSS_QDSP6SS_PWR_CTL_L2DATA_STBY_N_BMSK                                0x40000
#define HWIO_MSS_QDSP6SS_PWR_CTL_L2DATA_STBY_N_SHFT                                   0x12

#define HWIO_MSS_QDSP6SS_LDO_CFG0_ADDR                                          (MSS_QDSP6SS_PUB_REG_BASE      + 0x00000050)
#define HWIO_MSS_QDSP6SS_LDO_CFG0_OFFS                                          (MSS_QDSP6SS_PUB_REG_BASE_OFFS + 0x00000050)
#define HWIO_MSS_QDSP6SS_LDO_CFG0_RMSK                                           0x500ff7f
#define HWIO_MSS_QDSP6SS_LDO_CFG0_IN          \
        in_dword_masked(HWIO_MSS_QDSP6SS_LDO_CFG0_ADDR, HWIO_MSS_QDSP6SS_LDO_CFG0_RMSK)
#define HWIO_MSS_QDSP6SS_LDO_CFG0_INM(m)      \
        in_dword_masked(HWIO_MSS_QDSP6SS_LDO_CFG0_ADDR, m)
#define HWIO_MSS_QDSP6SS_LDO_CFG0_OUT(v)      \
        out_dword(HWIO_MSS_QDSP6SS_LDO_CFG0_ADDR,v)
#define HWIO_MSS_QDSP6SS_LDO_CFG0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_QDSP6SS_LDO_CFG0_ADDR,m,v,HWIO_MSS_QDSP6SS_LDO_CFG0_IN)
#define HWIO_MSS_QDSP6SS_LDO_CFG0_LDO_TEST_BMSK                                  0x4000000
#define HWIO_MSS_QDSP6SS_LDO_CFG0_LDO_TEST_SHFT                                       0x1a
#define HWIO_MSS_QDSP6SS_LDO_CFG0_IDAC_EN_BMSK                                   0x1000000
#define HWIO_MSS_QDSP6SS_LDO_CFG0_IDAC_EN_SHFT                                        0x18
#define HWIO_MSS_QDSP6SS_LDO_CFG0_LDO_CTL1_BMSK                                     0xff00
#define HWIO_MSS_QDSP6SS_LDO_CFG0_LDO_CTL1_SHFT                                        0x8
#define HWIO_MSS_QDSP6SS_LDO_CFG0_SYS_CTL_BMSK                                        0x7e
#define HWIO_MSS_QDSP6SS_LDO_CFG0_SYS_CTL_SHFT                                         0x1
#define HWIO_MSS_QDSP6SS_LDO_CFG0_SYSTEM_LOAD_EN_BMSK                                  0x1
#define HWIO_MSS_QDSP6SS_LDO_CFG0_SYSTEM_LOAD_EN_SHFT                                  0x0

#define HWIO_MSS_QDSP6SS_LDO_CFG1_ADDR                                          (MSS_QDSP6SS_PUB_REG_BASE      + 0x00000054)
#define HWIO_MSS_QDSP6SS_LDO_CFG1_OFFS                                          (MSS_QDSP6SS_PUB_REG_BASE_OFFS + 0x00000054)
#define HWIO_MSS_QDSP6SS_LDO_CFG1_RMSK                                              0x3fff
#define HWIO_MSS_QDSP6SS_LDO_CFG1_IN          \
        in_dword_masked(HWIO_MSS_QDSP6SS_LDO_CFG1_ADDR, HWIO_MSS_QDSP6SS_LDO_CFG1_RMSK)
#define HWIO_MSS_QDSP6SS_LDO_CFG1_INM(m)      \
        in_dword_masked(HWIO_MSS_QDSP6SS_LDO_CFG1_ADDR, m)
#define HWIO_MSS_QDSP6SS_LDO_CFG1_OUT(v)      \
        out_dword(HWIO_MSS_QDSP6SS_LDO_CFG1_ADDR,v)
#define HWIO_MSS_QDSP6SS_LDO_CFG1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_QDSP6SS_LDO_CFG1_ADDR,m,v,HWIO_MSS_QDSP6SS_LDO_CFG1_IN)
#define HWIO_MSS_QDSP6SS_LDO_CFG1_LDO_VREF_TRIM_SEL_BMSK                            0x2000
#define HWIO_MSS_QDSP6SS_LDO_CFG1_LDO_VREF_TRIM_SEL_SHFT                               0xd
#define HWIO_MSS_QDSP6SS_LDO_CFG1_LDO_VREF_TRIM_SW_BMSK                             0x1f00
#define HWIO_MSS_QDSP6SS_LDO_CFG1_LDO_VREF_TRIM_SW_SHFT                                0x8
#define HWIO_MSS_QDSP6SS_LDO_CFG1_DIG_CTL_BMSK                                        0xff
#define HWIO_MSS_QDSP6SS_LDO_CFG1_DIG_CTL_SHFT                                         0x0

#define HWIO_MSS_QDSP6SS_LDO_CFG2_ADDR                                          (MSS_QDSP6SS_PUB_REG_BASE      + 0x00000058)
#define HWIO_MSS_QDSP6SS_LDO_CFG2_OFFS                                          (MSS_QDSP6SS_PUB_REG_BASE_OFFS + 0x00000058)
#define HWIO_MSS_QDSP6SS_LDO_CFG2_RMSK                                              0xffff
#define HWIO_MSS_QDSP6SS_LDO_CFG2_IN          \
        in_dword_masked(HWIO_MSS_QDSP6SS_LDO_CFG2_ADDR, HWIO_MSS_QDSP6SS_LDO_CFG2_RMSK)
#define HWIO_MSS_QDSP6SS_LDO_CFG2_INM(m)      \
        in_dword_masked(HWIO_MSS_QDSP6SS_LDO_CFG2_ADDR, m)
#define HWIO_MSS_QDSP6SS_LDO_CFG2_OUT(v)      \
        out_dword(HWIO_MSS_QDSP6SS_LDO_CFG2_ADDR,v)
#define HWIO_MSS_QDSP6SS_LDO_CFG2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_QDSP6SS_LDO_CFG2_ADDR,m,v,HWIO_MSS_QDSP6SS_LDO_CFG2_IN)
#define HWIO_MSS_QDSP6SS_LDO_CFG2_REF_CTL_BMSK                                      0xff00
#define HWIO_MSS_QDSP6SS_LDO_CFG2_REF_CTL_SHFT                                         0x8
#define HWIO_MSS_QDSP6SS_LDO_CFG2_IDAC_CTL_BMSK                                       0xff
#define HWIO_MSS_QDSP6SS_LDO_CFG2_IDAC_CTL_SHFT                                        0x0

#define HWIO_MSS_QDSP6SS_LDO_VREF_SET_ADDR                                      (MSS_QDSP6SS_PUB_REG_BASE      + 0x0000005c)
#define HWIO_MSS_QDSP6SS_LDO_VREF_SET_OFFS                                      (MSS_QDSP6SS_PUB_REG_BASE_OFFS + 0x0000005c)
#define HWIO_MSS_QDSP6SS_LDO_VREF_SET_RMSK                                         0x37f7f
#define HWIO_MSS_QDSP6SS_LDO_VREF_SET_IN          \
        in_dword_masked(HWIO_MSS_QDSP6SS_LDO_VREF_SET_ADDR, HWIO_MSS_QDSP6SS_LDO_VREF_SET_RMSK)
#define HWIO_MSS_QDSP6SS_LDO_VREF_SET_INM(m)      \
        in_dword_masked(HWIO_MSS_QDSP6SS_LDO_VREF_SET_ADDR, m)
#define HWIO_MSS_QDSP6SS_LDO_VREF_SET_OUT(v)      \
        out_dword(HWIO_MSS_QDSP6SS_LDO_VREF_SET_ADDR,v)
#define HWIO_MSS_QDSP6SS_LDO_VREF_SET_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_QDSP6SS_LDO_VREF_SET_ADDR,m,v,HWIO_MSS_QDSP6SS_LDO_VREF_SET_IN)
#define HWIO_MSS_QDSP6SS_LDO_VREF_SET_LDO_VREF_SEL_BMSK                            0x20000
#define HWIO_MSS_QDSP6SS_LDO_VREF_SET_LDO_VREF_SEL_SHFT                               0x11
#define HWIO_MSS_QDSP6SS_LDO_VREF_SET_LDO_VREF_SEL_RST_BMSK                        0x10000
#define HWIO_MSS_QDSP6SS_LDO_VREF_SET_LDO_VREF_SEL_RST_SHFT                           0x10
#define HWIO_MSS_QDSP6SS_LDO_VREF_SET_VREF_RET_BMSK                                 0x7f00
#define HWIO_MSS_QDSP6SS_LDO_VREF_SET_VREF_RET_SHFT                                    0x8
#define HWIO_MSS_QDSP6SS_LDO_VREF_SET_VREF_LDO_BMSK                                   0x7f
#define HWIO_MSS_QDSP6SS_LDO_VREF_SET_VREF_LDO_SHFT                                    0x0

#define HWIO_MSS_QDSP6SS_LDO_VREF_TEST_CFG_ADDR                                 (MSS_QDSP6SS_PUB_REG_BASE      + 0x00000060)
#define HWIO_MSS_QDSP6SS_LDO_VREF_TEST_CFG_OFFS                                 (MSS_QDSP6SS_PUB_REG_BASE_OFFS + 0x00000060)
#define HWIO_MSS_QDSP6SS_LDO_VREF_TEST_CFG_RMSK                                   0xffffff
#define HWIO_MSS_QDSP6SS_LDO_VREF_TEST_CFG_IN          \
        in_dword_masked(HWIO_MSS_QDSP6SS_LDO_VREF_TEST_CFG_ADDR, HWIO_MSS_QDSP6SS_LDO_VREF_TEST_CFG_RMSK)
#define HWIO_MSS_QDSP6SS_LDO_VREF_TEST_CFG_INM(m)      \
        in_dword_masked(HWIO_MSS_QDSP6SS_LDO_VREF_TEST_CFG_ADDR, m)
#define HWIO_MSS_QDSP6SS_LDO_VREF_TEST_CFG_OUT(v)      \
        out_dword(HWIO_MSS_QDSP6SS_LDO_VREF_TEST_CFG_ADDR,v)
#define HWIO_MSS_QDSP6SS_LDO_VREF_TEST_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_QDSP6SS_LDO_VREF_TEST_CFG_ADDR,m,v,HWIO_MSS_QDSP6SS_LDO_VREF_TEST_CFG_IN)
#define HWIO_MSS_QDSP6SS_LDO_VREF_TEST_CFG_LD_TEST_BMSK                           0xff0000
#define HWIO_MSS_QDSP6SS_LDO_VREF_TEST_CFG_LD_TEST_SHFT                               0x10
#define HWIO_MSS_QDSP6SS_LDO_VREF_TEST_CFG_DTEST_CTL_BMSK                           0xff00
#define HWIO_MSS_QDSP6SS_LDO_VREF_TEST_CFG_DTEST_CTL_SHFT                              0x8
#define HWIO_MSS_QDSP6SS_LDO_VREF_TEST_CFG_OSC_CTL_BMSK                               0xff
#define HWIO_MSS_QDSP6SS_LDO_VREF_TEST_CFG_OSC_CTL_SHFT                                0x0

#define HWIO_MSS_QDSP6SS_LDO_VREF_CMD_ADDR                                      (MSS_QDSP6SS_PUB_REG_BASE      + 0x00000064)
#define HWIO_MSS_QDSP6SS_LDO_VREF_CMD_OFFS                                      (MSS_QDSP6SS_PUB_REG_BASE_OFFS + 0x00000064)
#define HWIO_MSS_QDSP6SS_LDO_VREF_CMD_RMSK                                             0x1
#define HWIO_MSS_QDSP6SS_LDO_VREF_CMD_OUT(v)      \
        out_dword(HWIO_MSS_QDSP6SS_LDO_VREF_CMD_ADDR,v)
#define HWIO_MSS_QDSP6SS_LDO_VREF_CMD_LDO_VREF_SEL_UPDATE_BMSK                         0x1
#define HWIO_MSS_QDSP6SS_LDO_VREF_CMD_LDO_VREF_SEL_UPDATE_SHFT                         0x0

#define HWIO_MSS_QDSP6SS_QMC_SVS_CTL_ADDR                                       (MSS_QDSP6SS_PUB_REG_BASE      + 0x0000007c)
#define HWIO_MSS_QDSP6SS_QMC_SVS_CTL_OFFS                                       (MSS_QDSP6SS_PUB_REG_BASE_OFFS + 0x0000007c)
#define HWIO_MSS_QDSP6SS_QMC_SVS_CTL_RMSK                                              0x3
#define HWIO_MSS_QDSP6SS_QMC_SVS_CTL_IN          \
        in_dword_masked(HWIO_MSS_QDSP6SS_QMC_SVS_CTL_ADDR, HWIO_MSS_QDSP6SS_QMC_SVS_CTL_RMSK)
#define HWIO_MSS_QDSP6SS_QMC_SVS_CTL_INM(m)      \
        in_dword_masked(HWIO_MSS_QDSP6SS_QMC_SVS_CTL_ADDR, m)
#define HWIO_MSS_QDSP6SS_QMC_SVS_CTL_OUT(v)      \
        out_dword(HWIO_MSS_QDSP6SS_QMC_SVS_CTL_ADDR,v)
#define HWIO_MSS_QDSP6SS_QMC_SVS_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_QDSP6SS_QMC_SVS_CTL_ADDR,m,v,HWIO_MSS_QDSP6SS_QMC_SVS_CTL_IN)
#define HWIO_MSS_QDSP6SS_QMC_SVS_CTL_QMC_MEM_SVS_SEL_BMSK                              0x2
#define HWIO_MSS_QDSP6SS_QMC_SVS_CTL_QMC_MEM_SVS_SEL_SHFT                              0x1
#define HWIO_MSS_QDSP6SS_QMC_SVS_CTL_QMC_MEM_SVS_BMSK                                  0x1
#define HWIO_MSS_QDSP6SS_QMC_SVS_CTL_QMC_MEM_SVS_SHFT                                  0x0


#endif /* __HALLDOHWIO_H__ */
