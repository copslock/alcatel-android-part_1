#===========================================================================
#
#  @file hwio_msm8996.py
#  @brief HWIO config file for the HWIO generation scripts for MSM8996.
#
#  This file can be invoked by calling:
#
#    HWIOGen.py --cfg=hwio_msm8996.py --flat=..\..\..\api\systemdrivers\hwio\msm8996\ARM_ADDRESS_FILE.FLAT
#
#  ===========================================================================
#
#  Copyright (c) 2011-2014 Qualcomm Technologies Incorporated.
#  All Rights Reserved.
#  QUALCOMM Proprietary and Confidential.
#
#  ===========================================================================
#
#  $Header: //components/rel/core.mpss/3.4.c3.11/systemdrivers/hwio/build/hwio_msm8996.py#1 $
#  $DateTime: 2016/03/28 23:02:17 $
#  $Author: mplcsds1 $
#
#  ===========================================================================

CHIPSET = 'msm8996'

# ============================================================================
# HWIO_BASE_FILES
# ============================================================================

bases = [
  'A2_NOC_AGGRE2_NOC',
  'BIMC',
  'BOOT_ROM',
  'CLK_CTL',
  'CONFIG_NOC',
  'CORE_TOP_CSR',
  'CRYPTO0_CRYPTO_TOP',
  'IPA_0_IPA_WRAPPER',
  'MODEM',
  'MODEM_TOP',
  'MPM2_MPM',
  'PERIPH_SS',
  'PMIC_ARB',
  'PRNG_CFG_PRNG_TOP',
  'QDSS_QDSS_ISTARI',
  'RPM',
  'RPM_SS_MSG_RAM_START_ADDRESS',
  'SECURITY_CONTROL',
  'SPDM_WRAPPER_TOP',
  'SYSTEM_NOC',
  'TLMM',
  'XPU_CFG_ANOC2_CFG_MPU1032_4_M16L12_AHB',
]

base_resize = {
  'RPM_SS_MSG_RAM_START_ADDRESS':         0x6000,
}

# Define the base files.  This is complicated by the fact that the MODEM and
# MODEM_TOP regions are manually mapped in the cust_config.xml file due to more
# complex virtual memory requirements.  We still need to allow getting the base
# addresses via the HWIO DAL and msmhwiobase.h, so we use fixed virtual
# addresses in msmhwiobase.h and remove them from the msmhwioplat.xml file.
HWIO_BASE_FILES = [
  {
    'filename': '../../../api/systemdrivers/hwio/' + CHIPSET + '/msmhwiobase.h',
    'bases': bases,
    'map-type': 'qurt',
    'virtual-address-start': 0xE0000000,
    'virtual-address-end': 0xF0000000,
    'resize': base_resize,
    'qurt-memsection-filename': '../../../api/systemdrivers/hwio/' + CHIPSET + '/msmhwioplat.xml',
    'qurt-memsection-attributes': { 'PERIPH_SS': 'tlb_lock="boot" glob="1"',
                                    'CLK_CTL':   'tlb_lock="boot" glob="1"' },
    'default-cache-policy': 'device',
    'devcfg-filename': '../config/' + CHIPSET + '/HWIOBaseMap.c',
    'check-sizes': True,
    'check-for-overlaps': True,

    'fixed-virtual-address': { 'MODEM_TOP': 0xEC000000,
                               'MODEM':     0xED000000 },
    'skip-memsection': ['MODEM_TOP', 'MODEM'],
  }
]


# ============================================================================
# HWIO_REGISTER_FILES
# ============================================================================

HWIO_REGISTER_FILES = [
  {
    'filename': '../hw/' + CHIPSET + '/msmhwioreg.h.ref',
    'bases': bases,
    'filter-exclude': ['RESERVED', 'DUMMY'],
    'header': '''
#error This file is for reference only and cannot be included.  See "go/hwio" or mail corebsp.sysdrv.hwio for help.
'''
  }
]


# ============================================================================
# HWIO_T32VIEW_FILES
# ============================================================================

HWIO_T32VIEW_FILES = [
  {
    'symbol-filename': '../scripts/' + CHIPSET + '/hwio.cmm',
    'limit-array-size': [ 10, 4 ],
    'per-filename': '../scripts/' + CHIPSET + '/hwioreg',
    'filter-exclude': ['RESERVED', 'DUMMY']
  },
]


# ============================================================================
# Main
#
# Entry point when invoking this directly.
# ============================================================================

if __name__ == "__main__":
  from subprocess import Popen
  hwiogen = Popen(["\\\\ben\\corebsp_labdata_0001\\sysdrv\\hwio\\HWIOGen.py", "--cfg=hwio_" + CHIPSET + ".py", "--flat=../../../api/systemdrivers/hwio/" + CHIPSET + "/ARM_ADDRESS_FILE.FLAT"], shell=True)
  hwiogen.wait()

