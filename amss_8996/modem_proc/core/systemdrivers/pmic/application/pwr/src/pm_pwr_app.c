/*! \file pm_pwr_app.c
*  \n
*  \brief Pwr Application APIs implementation.
*  \n  
*  \n &copy; Copyright 2013 QUALCOMM Technologies Incorporated, All Rights Reserved
*/

/* =======================================================================
                            Edit History
This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/core.mpss/3.4.c3.11/systemdrivers/pmic/application/pwr/src/pm_pwr_app.c#1 $

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
10/09/14   vk      vdd_mss API change to include setting time and support ON/OFF feature
10/07/13   rh      File created
========================================================================== */
/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/
#include "hw_module_type.h"
#include "pm_pbs_client.h"
#include "pmapp_pwr.h"
#include "pm_target_information.h"
#include "pm_resources_and_types.h"
#include "pm_version.h"
#include "pm_npa.h"
#include "DDITimetick.h"
#include "pm_ulog.h"
#include "pm_ldo_driver.h"
#include "pm_smps_driver.h"
#include "pm_malloc.h"

static pm_pwr_resource_info_type* mss_rail = NULL;
static boolean  configure_once = FALSE;

#define PM_STATUS_REG_POLL_INTERVAL             25     /* us time slice to poll the VREG status flag after */ 
#define PM_SMPS_ENABLE_MIN_SETTLE_TIME_US      120    /* SMPS min settling time to wait after turning ON the rail */
#define PM_SMPS_VOLT_MIN_SETTLE_TIME_US         25    /* SMPS min settling time to wait after voltage change on this rail */
#define SYS_CLK_FREQ_KHZ                        19200    /* System Clock Freq of 19.2 Mhz(Could change in future targets)*/
#define STEPPER_VS_CTL_CLOCK_BASE               20    /* Min No. Clock Cycles in STEPPER_VS_CTL Register used to calculate stepper rate */              


/*
 * Structure to hold the Power rails Diag runtime variables
 */
static struct
{
  DalTimetickTime64Type     start_settling_time;                              /**< start settling time */
  DalTimetickTime64Type     end_settling_time;                                /**< end settling time */
  uint64                    actual_settling_time_usec;                        /**< actual settling time in usec */
  uint32                    mss_time_taken_to_settle;                         /**< mss time taken to settle in usec */                          
  uint32                    calculated_settling_time_usec;                    /**< calculated settling time based on formula in usec */
  uint32                    mss_settling_time_us;                             /**< settling time if changed in different conditions in usec */
  uint32                    settling_loop_count;                              /**< vreg_ok polling loop count */
  uint32                    vreg_ok_err_count;                                 /**< vreg_ok error count */
  pm_err_flag_type          err_flag_vreg_ok;                                 /**< vreg_ok error flag */
  pm_on_off_type            prev_rail_status;                                 /**< prev_ON/OFF rail_status */
  pm_on_off_type            current_rail_status;                              /**< current_ON/OFF rail_status */
  uint32                    prev_voltage;                                     /**< previous voltage */
  uint32                    current_voltage;                                  /**< current voltage request*/
  pm_err_flag_type          returned_err_flag;                                /**< current voltage request*/
  pm_pbs_client_type*       mss_pbs_level_trigger_sequence;
  pm_pbs_client_type*       mss_pbs_ocp_trigger_sequence;
  DalDeviceHandle           *timetick_handle;                                 /**< handle for getting system time stamp */  
  uint8                     model;      
  pm_pwr_data_type          *pwr_data;
  pm_comm_info_type         *comm_ptr;
  pm_smps_data_type         *smps_ptr;
  pm_ldo_data_type          *ldo_ptr;
  uint32                    mss_stepper_rate;
  boolean                   external_manage_flag;
} mss_rail_status;


/*===========================================================================

                        FUNCTION DEFINITIONS

===========================================================================*/

static pm_err_flag_type pmapp_pwr_mss_get_stepper_rate(pm_pwr_data_type *pwr_data, pm_comm_info_type *comm_ptr, uint8 peripheral_index, uint32 *mss_stepper_rate_uV_per_uS);
static pm_err_flag_type pmapp_pwr_mss_configure_once(void);
static void pmapp_pwr_clear_mss_status(void);



pm_err_flag_type pmapp_pwr_set_vdd_mss(pm_on_off_type on_off, uint32 voltage)
{
/*Initialize variables*/
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
    pm_err_flag_type err_flag_vreg_ok = PM_ERR_FLAG__SUCCESS;
    pm_on_off_type vreg_ok_status = PM_OFF;

/*Check if external manage mode is set */
    if (mss_rail_status.external_manage_flag==TRUE)
    {
        err_flag=PM_ERR_FLAG__EXTERNAL_MANAGE_MODE;
        ULOG_RT_PRINTF_0(pm_ulog_vars.pm_log, "MSS RAIL MANAGED EXTERNALLY");
        return err_flag;
    }

    
/*Update Rail Status and Rail Voltage variables*/
    mss_rail_status.prev_rail_status = mss_rail_status.current_rail_status;                                     
    mss_rail_status.prev_voltage = mss_rail_status.current_voltage;
    mss_rail_status.current_voltage = voltage; 
    mss_rail_status.current_rail_status=PM_INVALID;
            
/*Clear mss_rail_status variables*/
    pmapp_pwr_clear_mss_status();
    
/*Configure MSS Rail Once*/
    if(configure_once == FALSE)
    {
        err_flag |= pmapp_pwr_mss_configure_once();
    } //run_once
        
/*Get the VREG enable status if previously Rail status was off as RPM Could have turned it ON*/
    if((mss_rail_status.prev_rail_status==PM_OFF) && (on_off==PM_ON))
    {
        err_flag |= pm_pwr_sw_enable_status_alg(mss_rail_status.pwr_data, mss_rail_status.comm_ptr, mss_rail->resource_index - 1, &mss_rail_status.prev_rail_status);
        if (err_flag != PM_ERR_FLAG__SUCCESS)
        {
            mss_rail_status.returned_err_flag = err_flag;
            DALSYS_LogEvent(0, DALSYS_LOGEVENT_FATAL_ERROR, "DALLOG PMIC: Failed to read mss vreg_enable status");
        }
    }
/*Conditions to Enable, Disable and Set Voltage*/
    /*Condition 1: For turning MSS rail OFF*/
    if((voltage==0) || (on_off==PM_OFF))
    {
        /* Disable MSS */
        if(mss_rail_status.prev_rail_status==PM_ON)
        {
        err_flag |= pm_pwr_sw_enable_alg(mss_rail_status.pwr_data, mss_rail_status.comm_ptr, mss_rail->resource_index - 1, PM_OFF);
        CORE_VERIFY(err_flag == PM_ERR_FLAG__SUCCESS);  
        }
        
        if((voltage>0) && (mss_rail_status.prev_voltage != mss_rail_status.current_voltage))
        {
        err_flag |= pm_pwr_volt_level_alg(mss_rail_status.pwr_data, mss_rail_status.comm_ptr, mss_rail->resource_index - 1, voltage);
        mss_rail_status.returned_err_flag = err_flag;
        CORE_VERIFY(err_flag == PM_ERR_FLAG__SUCCESS);      
        }
    }
        
    /*Condition 2: For changing Voltage level if MSS Rail is already ON*/
    else if((mss_rail_status.prev_rail_status == PM_ON) && (on_off==PM_ON))
    {       
        //If current and previous voltages are same, then dont set voltage level register to save timeline hit 
        if(mss_rail_status.prev_voltage != mss_rail_status.current_voltage)
        {
            err_flag |= pm_pwr_volt_level_alg(mss_rail_status.pwr_data, mss_rail_status.comm_ptr, mss_rail->resource_index - 1, voltage);
            mss_rail_status.returned_err_flag = err_flag;
            CORE_VERIFY(err_flag == PM_ERR_FLAG__SUCCESS);
        }   
    }
    
    /*Condition 3: For Enabling and Setting Voltage if MSS is OFF*/
    /*Default condition (mss_rail_status.prev_rail_status=0 and (voltage != 0 or on_off==ON))*/
    else{
        //set new voltage level
        err_flag |= pm_pwr_volt_level_alg(mss_rail_status.pwr_data, mss_rail_status.comm_ptr, mss_rail->resource_index - 1, voltage);
        mss_rail_status.returned_err_flag = err_flag;
        CORE_VERIFY(err_flag == PM_ERR_FLAG__SUCCESS);
        //mss ocp workaround : disable ocp, save mode, mode=pwm, Enable mss, enable ocp, restore mode
        err_flag |= pm_pbs_client_sw_trigger(0, *mss_rail_status.mss_pbs_ocp_trigger_sequence);
    }
        
/*Settling Conditions*/
    if((on_off == PM_ON) && (voltage != 0))
    {
        /* Settling start time in ticks. */
        DalTimetick_GetTimetick64(mss_rail_status.timetick_handle, &mss_rail_status.start_settling_time);
        /*Calculate setteling time*/
        if(voltage>=mss_rail_status.prev_voltage)
            mss_rail_status.calculated_settling_time_usec  = (voltage - mss_rail_status.prev_voltage)/mss_rail_status.mss_stepper_rate;
        else
            mss_rail_status.calculated_settling_time_usec  = (mss_rail_status.prev_voltage - voltage)/mss_rail_status.mss_stepper_rate;
        
        mss_rail_status.mss_settling_time_us= (mss_rail_status.calculated_settling_time_usec < PM_SMPS_VOLT_MIN_SETTLE_TIME_US) ? PM_SMPS_VOLT_MIN_SETTLE_TIME_US : mss_rail_status.calculated_settling_time_usec;
        //If mss rail was OFF minimum settling time needed is 125uS
        if((mss_rail_status.prev_rail_status == PM_OFF) && (mss_rail_status.mss_settling_time_us < PM_SMPS_ENABLE_MIN_SETTLE_TIME_US))
        {  
            mss_rail_status.mss_settling_time_us =  PM_SMPS_ENABLE_MIN_SETTLE_TIME_US;   
        }
        /*end of calculate settling time */
        
        /* Wait for the calculated settling time */
        /*if rail was previously off wait for 125uS(min enable wait time)*/
        if((mss_rail_status.prev_rail_status==PM_OFF) || (mss_rail_status.prev_voltage != mss_rail_status.current_voltage)) 
        {
            DALSYS_BusyWait(mss_rail_status.mss_settling_time_us);
            mss_rail_status.mss_time_taken_to_settle = mss_rail_status.mss_settling_time_us;
        }
        //if prev_status is PM_ON and PrevVoltage=CurrentVoltage then no settling time needed
        else 
            mss_rail_status.mss_time_taken_to_settle =0;
                
        while(mss_rail_status.mss_time_taken_to_settle <= (mss_rail_status.mss_settling_time_us*10))
        {//end while if time taken to settle exceeds 10times of settling time
            /* Get the VREG_OK status for MSS */
            err_flag_vreg_ok=pm_pwr_is_vreg_ready_alg(mss_rail_status.pwr_data, mss_rail_status.comm_ptr, mss_rail->resource_index - 1, &vreg_ok_status);   
            if(vreg_ok_status == PM_ON)
                break;
            else 
            {
                mss_rail_status.settling_loop_count++;
                DALSYS_BusyWait(PM_STATUS_REG_POLL_INTERVAL);
                mss_rail_status.mss_time_taken_to_settle += PM_STATUS_REG_POLL_INTERVAL;
                if(PM_ERR_FLAG__SUCCESS != err_flag_vreg_ok)
                {
                    mss_rail_status.vreg_ok_err_count++;
                    mss_rail_status.err_flag_vreg_ok = err_flag;
                }
            }
        }       
        mss_rail_status.current_rail_status=vreg_ok_status;
        
        if ((vreg_ok_status != PM_ON) && (mss_rail_status.mss_time_taken_to_settle > (mss_rail_status.mss_settling_time_us*10)))
        {
           /* Settling end time in ticks. */
           DalTimetick_GetTimetick64(mss_rail_status.timetick_handle, &mss_rail_status.end_settling_time);
           DalTimetick_CvtFromTimetick64(mss_rail_status.timetick_handle,
                              (mss_rail_status.end_settling_time - mss_rail_status.start_settling_time),  
                                T_USEC, &mss_rail_status.actual_settling_time_usec);
           mss_rail_status.current_rail_status = PM_OFF;
           CORE_VERIFY(0);  
        }   
    }

    /*No Settling time if rail is turning off*/ 
    if((on_off == PM_OFF) || (voltage == 0))
    {   
        mss_rail_status.current_rail_status = PM_OFF;
    }

/*PMD9635 needs PBS client from config to be triggered after setting MSS rail*/
    /*Not required if rail is turned off*/
    /*if voltage is != prev voltage, do this*/ 
    if(err_flag == PM_ERR_FLAG__SUCCESS && mss_rail_status.model == PMIC_IS_PMD9635 && (mss_rail_status.prev_voltage != mss_rail_status.current_voltage))
    {       
        err_flag |= pm_pbs_client_sw_trigger(0, *mss_rail_status.mss_pbs_level_trigger_sequence);
        if (err_flag != PM_ERR_FLAG__SUCCESS)
        {
            mss_rail_status.returned_err_flag = err_flag;
            DALSYS_LogEvent(0, DALSYS_LOGEVENT_FATAL_ERROR, "DALLOG PMIC: pm_pbs_client_sw_trigger returned error ");
        } 
        
    }
    
    ULOG_RT_PRINTF_5(
        pm_ulog_vars.pm_log,
        "prev_status=%d, current_status=%d, prev_voltage=%d, current_voltage=%d, Settling time=%d",
        mss_rail_status.prev_rail_status, mss_rail_status.current_rail_status, mss_rail_status.prev_voltage, mss_rail_status.current_voltage, mss_rail_status.mss_time_taken_to_settle);
        
    mss_rail_status.returned_err_flag = err_flag;       
    return err_flag;

}


/******************************************************************************************************************************************/
/*  Get VDD MSS Status API                                                                                                                */
/******************************************************************************************************************************************/


pm_err_flag_type pmapp_pwr_get_vdd_mss_status(uint32 *voltage, pm_on_off_type *on_off, pmapp_rail_status_type *get_status)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
    
    /*prepare first run of MSS Rail*/
    if(configure_once == FALSE)
    {
        err_flag=pmapp_pwr_mss_configure_once();
    } //run_once
    err_flag |= pm_pwr_volt_level_status_alg(mss_rail_status.pwr_data, mss_rail_status.comm_ptr, mss_rail->resource_index - 1,  voltage);
    err_flag |= pm_pwr_sw_enable_status_alg(mss_rail_status.pwr_data, mss_rail_status.comm_ptr, mss_rail->resource_index - 1, on_off);
    get_status->prev_rail_status=mss_rail_status.prev_rail_status;
    get_status->current_rail_status=mss_rail_status.current_rail_status;
    get_status->current_voltage=mss_rail_status.current_voltage;
    get_status->prev_voltage=mss_rail_status.prev_voltage;
    get_status->settling_time=mss_rail_status.mss_time_taken_to_settle;
    return err_flag;
}

/******************************************************************************************************************************************/
/*  VDD MSS EXTERNAL MANAGE                                                                                                       */
/******************************************************************************************************************************************/
pm_err_flag_type pmapp_pwr_vdd_mss_external_manage (boolean flag){
    mss_rail_status.external_manage_flag = flag;
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
    pmapp_rail_status_type mss_status;
    if (flag==FALSE){
        //read mss rail voltage and status and populate mss_rail_status accordingly
        err_flag=pmapp_pwr_get_vdd_mss_status(&(mss_rail_status.current_voltage), &(mss_rail_status.current_rail_status), &mss_status); 
        ULOG_RT_PRINTF_0(pm_ulog_vars.pm_log, "Exiting External Manage Mode");
    }
    if (flag==TRUE){
        //clear params in mss_rail_status structure  
        mss_rail_status.prev_rail_status=0;
        mss_rail_status.prev_voltage=0;
        mss_rail_status.current_rail_status=0;
        mss_rail_status.current_voltage=0;
        ULOG_RT_PRINTF_0(pm_ulog_vars.pm_log, "Entering External Manage Mode");
    }
    return err_flag;
}


/*Helper Functions*/

/******************************************************************************************************************************************/
/*Get Stepper Rate: Calculates MSS stepper rate based on STEPPER_VS_CTL Register                                                          */    
/******************************************************************************************************************************************/
static pm_err_flag_type pmapp_pwr_mss_get_stepper_rate(pm_pwr_data_type *pwr_data, pm_comm_info_type *comm_ptr, uint8 peripheral_index, uint32 *mss_stepper_rate_uV_per_uS)
{

    pm_register_address_type reg = 0; 
    pm_register_data_type data =0;
    uint32 no_of_clock_cycles=0;
    uint8 LSB=0;
    uint32 sys_freq_khz=SYS_CLK_FREQ_KHZ;
    uint32 step_size_uV = *((uint32*)mss_rail->data3);
    uint32 configured_step_size_uV=0;
    uint32 uS_to_configured_step_size=0;
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
    
    if(pwr_data->pwr_reg_table == NULL){return PM_ERR_FLAG__INVALID_POINTER;}
    
    //get register address
    reg = (pm_register_address_type)(pwr_data->pwr_specific_info[peripheral_index].periph_base_address + pwr_data->pwr_reg_table->STEPPER_VS_CTL);

    //read register 
    err_flag = pm_comm_read_byte(comm_ptr->slave_id, reg, &data, 0);

    //check error flag
    if(err_flag != PM_ERR_FLAG__SUCCESS) 
    {
        return err_flag;
    }
    
    //Calculate no. of clock cycles, multiply by 1000 for Accuracy 
    no_of_clock_cycles=STEPPER_VS_CTL_CLOCK_BASE*(1<<(data & 0x7));
    no_of_clock_cycles*=1000; //multiply by 1000 to maintain accuracy
    
    //Get LSB
    LSB=(data & 0x18)>>3; //LSB Part in STEPPER_VS_CTL_Register
    //calculate configured step size based on LSB 
    configured_step_size_uV=step_size_uV*(1<<LSB);  
    
    //uS taken to reach step size configured using LSB in VS_CTL Register
    uS_to_configured_step_size=no_of_clock_cycles/sys_freq_khz; 
    
    //uV increase per uS
    *mss_stepper_rate_uV_per_uS=configured_step_size_uV/uS_to_configured_step_size;
    
    return err_flag;
}

/******************************************************************************************************************************************/
/*pmapp_pwr_mss_confgure_once: Initialization steps for pmapp_pwr_set_vdd_mss API                                                         */
/******************************************************************************************************************************************/
static pm_err_flag_type pmapp_pwr_mss_configure_once(void)
{
    static uint8 pmic_index = 0;
    const char* timetick_dev_str = "SystemTimer";
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

    /* Get MSS rail data from DAL CONFIG */
    mss_rail = (pm_pwr_resource_info_type*)pm_target_information_get_specific_info(PM_PROP_MSS);
    CORE_VERIFY_PTR(mss_rail);
    
    /*Update mss_rail_status struct variables*/
    mss_rail_status.model = pm_get_pmic_model(0);
    mss_rail_status.timetick_handle = NULL;
    mss_rail_status.mss_pbs_level_trigger_sequence = (pm_pbs_client_type*)mss_rail->data1;
    mss_rail_status.mss_pbs_ocp_trigger_sequence = (pm_pbs_client_type*)mss_rail->data2;
    
    /*Verify pbs_level_trigger and pbs_ocp_trigger pointers */
    if(mss_rail_status.model==PMIC_IS_PMD9635) 
    {       
        CORE_VERIFY_PTR(mss_rail_status.mss_pbs_level_trigger_sequence);
    }         
    CORE_VERIFY_PTR(mss_rail_status.mss_pbs_ocp_trigger_sequence);  

    if ( ( mss_rail->resource_type == RPM_SMPS_B_REQ) || (mss_rail->resource_type == RPM_LDO_B_REQ) )
    {
        pmic_index = 1;
    }

    // Apply supplied voltage to MSS rail. Note that in target config where we get the MSS rail info
    // from the regulator indices are 1-indexed while driver code expects 0-indexed resource ids.
    // Subtract resource_index by 1 to account for this.

    if((mss_rail->resource_type==RPM_SMPS_A_REQ) || (mss_rail->resource_type==RPM_SMPS_B_REQ))
    {
        mss_rail_status.smps_ptr = pm_smps_get_data(pmic_index);
        if(mss_rail_status.smps_ptr != NULL)
        {
            mss_rail_status.pwr_data = &((mss_rail_status.smps_ptr)->pm_pwr_data); 
            mss_rail_status.comm_ptr = ((mss_rail_status.smps_ptr)->comm_ptr);
        }
    }
    if((mss_rail->resource_type==RPM_LDO_A_REQ) || (mss_rail->resource_type==RPM_LDO_B_REQ))
    {
        mss_rail_status.ldo_ptr = pm_ldo_get_data(pmic_index);
        if(mss_rail_status.ldo_ptr != NULL)
        {
            mss_rail_status.pwr_data = &((mss_rail_status.ldo_ptr)->pm_pwr_data); 
            mss_rail_status.comm_ptr = ((mss_rail_status.ldo_ptr)->comm_ptr);
        }
    }
    CORE_VERIFY_PTR(mss_rail_status.pwr_data);
    CORE_VERIFY_PTR(mss_rail_status.comm_ptr);

    /*get mss_stepper_rate*/    
    err_flag=pmapp_pwr_mss_get_stepper_rate(mss_rail_status.pwr_data, mss_rail_status.comm_ptr, mss_rail->resource_index - 1, &(mss_rail_status.mss_stepper_rate));

    if (DAL_SUCCESS != DalTimetick_Attach(timetick_dev_str, &mss_rail_status.timetick_handle))
    {
        /* Update state to indicate DAL ERROR */
        CORE_VERIFY(0);
    }
    
    /*Get initial MSS Rail Status*/
    err_flag |= pm_pwr_sw_enable_status_alg(mss_rail_status.pwr_data, mss_rail_status.comm_ptr, mss_rail->resource_index - 1, &mss_rail_status.prev_rail_status);
    if (err_flag != PM_ERR_FLAG__SUCCESS)
    {
         mss_rail_status.returned_err_flag = err_flag;
         DALSYS_LogEvent(0, DALSYS_LOGEVENT_FATAL_ERROR, "DALLOG PMIC: Failed to read mss vreg_enable status");
    }
    /*Get initial MSS Rail Voltage */
    err_flag |= pm_pwr_volt_level_status_alg(mss_rail_status.pwr_data, mss_rail_status.comm_ptr, mss_rail->resource_index - 1,  &mss_rail_status.prev_voltage);
    if (err_flag != PM_ERR_FLAG__SUCCESS)
    {
        mss_rail_status.returned_err_flag = err_flag;
        DALSYS_LogEvent(0, DALSYS_LOGEVENT_FATAL_ERROR, "DALLOG PMIC: Failed to read mss volt level status");
    }
    configure_once = TRUE;
   return err_flag; 
}//end pmapp_pwr_mss_configure_once

/******************************************************************************************************************************************/
/*pmapp_pwr_clear_mss_status: Clears some variables of mss_rail_status struct                                                             */
/******************************************************************************************************************************************/
static void pmapp_pwr_clear_mss_status(void)
{
    mss_rail_status.start_settling_time = 0;
    mss_rail_status.end_settling_time = 0;
    mss_rail_status.actual_settling_time_usec = 0;
    mss_rail_status.calculated_settling_time_usec = 0;                       
    mss_rail_status.mss_time_taken_to_settle = 0;
    mss_rail_status.mss_settling_time_us = 0;
    mss_rail_status.settling_loop_count = 0;
    mss_rail_status.vreg_ok_err_count = 0;    
    mss_rail_status.returned_err_flag = PM_ERR_FLAG__SUCCESS;                             
    mss_rail_status.err_flag_vreg_ok = PM_ERR_FLAG__SUCCESS; 
}

/******************************************************************************************************************************************/
/*pmapp_pwr_mss_volt_level_info: Passes volt level to be set in volt_ctl2 register for mss rail and the pm_mss_config_info for MVC        */
/******************************************************************************************************************************************/

pm_err_flag_type pmapp_pwr_mss_volt_level_info(uint32 voltage, pm_rail_configs_type *pm_mss_rail_configs){
    pm_err_flag_type err_flag=PM_ERR_FLAG__SUCCESS;
    static pm_rail_cfg_info_type *pm_mss_rail_info_temp;
	static pm_rail_cfg_info_type *pm_mss_rail_info_alloc_mem;
    uint32 vset = 0; //register values for the voltage to be set
	static boolean pm_external_data_init = FALSE;
	uint8 i =0;
    
    if(pm_mss_rail_configs == NULL){
        return PM_ERR_FLAG__INVALID_POINTER;
    }
    
	if (pm_external_data_init==FALSE){		
		pm_mss_rail_info_temp = (pm_rail_cfg_info_type*)pm_target_information_get_specific_info(PM_PROP_MSS_CFG_INFO);
		CORE_VERIFY_PTR(pm_mss_rail_info_temp);
		pm_malloc((PM_MSS_CFG_COUNT * sizeof(pm_rail_cfg_info_type)), ((void**)&(pm_mss_rail_info_alloc_mem))); 
		CORE_VERIFY_PTR(pm_mss_rail_info_alloc_mem);
		pm_external_data_init=TRUE;
	}
	
	if(configure_once==FALSE)
    {
        pmapp_pwr_mss_configure_once();
    }
	
	for(i=0; i<PM_MSS_CFG_COUNT;i++){
		pm_mss_rail_info_alloc_mem[i].valid = pm_mss_rail_info_temp[i].valid;
		pm_mss_rail_info_alloc_mem[i].data = pm_mss_rail_info_temp[i].data;
	}
	
	
	err_flag = pm_pwr_volt_calculate_vset_alg(mss_rail_status.pwr_data, mss_rail_status.comm_ptr, mss_rail->resource_index - 1, voltage, &vset);
    
    pm_mss_rail_configs->nNumConfigs = PM_MSS_CFG_COUNT;
    
    pm_mss_rail_info_alloc_mem[PM_MSS_VOLT_LEVEL].data = vset;
    
    pm_mss_rail_configs->pm_rail_cfg_info  	=	pm_mss_rail_info_alloc_mem;
    
    ULOG_RT_PRINTF_2(
        pm_ulog_vars.pm_log,
        "pmapp_pwr_mss_volt_level_info: Voltage=%d, RegValue=%d",
        voltage, pm_mss_rail_info_alloc_mem[PM_MSS_VOLT_LEVEL].data);    
    
    return err_flag;
}


