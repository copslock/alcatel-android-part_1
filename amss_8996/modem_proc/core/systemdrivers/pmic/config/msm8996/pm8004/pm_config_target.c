/*! \file
 *  
 *  \brief  rpm_settings.c ----This file contains customizable target specific driver settings & PMIC registers.
 *  \details This file contains customizable target specific 
 * driver settings & PMIC registers. This file is generated from database functional
 * configuration information that is maintained for each of the targets.
 *  
 *    PMIC code generation Version: 1.0.0.0
 *    PMIC code generation Resource Setting Information Version: VU.Please Provide Valid Label - Not Approved
 *    PMIC code generation Software Register Information Version: VU.Please Provide Valid Label - Not Approved
 *    PMIC code generation Processor Allocation Information Version: VU.Please Provide Valid Label - Not Approved
 *    This file contains code for Target specific settings and modes.
 *  
 *  &copy; Copyright 2010 Qualcomm Technologies Incorporated, All Rights Reserved
 */

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This document is created by a code generator, therefore this section will
  not contain comments describing changes made to the module.

$Header: //components/rel/core.mpss/3.4.c3.11/systemdrivers/pmic/config/msm8996/pm8004/pm_config_target.c#1 $ 

===========================================================================*/

/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/

#include "pm_target_information.h"
#include "pm_npa.h"
#include "pmapp_pwr.h"
#include "pm_mpp_driver.h"
#include "pm_xo_driver.h"
#include "pm_pbs_client.h"
#include "pm_uicc_app.h"

//PM8994,PMi8994,PM8004
uint32   pm8004_num_of_ldo[]  =       {32, 1, 1, 2};
uint32   pm8004_num_of_smps[] =       {12, 3, 5, 0};
uint32   pm8004_num_of_gpio[] =       {22,10, 0, 4};
uint32   pm8004_num_of_mpp[]  =       { 8, 4, 4, 0};
uint32   pm8004_num_of_rtc[] =        { 1, 0, 0, 1};
uint32   pm8004_num_of_talm[] =       { 1, 0, 0, 0};
uint32   pm8004_num_of_megaxo[] =     { 1, 0, 0, 1};
uint32   pm8004_num_of_boost[]  =     { 0, 1, 0, 0}; 
uint32   pm8004_num_of_vs[] =         { 2, 0, 0, 0};
uint32	 pm8004_num_of_pbs_client[] = {16, 12, 8, 5};

pm_xo_core_specific_info_type pm8004_xocore_specific[1] =
{
    {0x7FF, 0x3F}
};
pm_mpp_specific_info_type pm8004_mpp_specific[1] = 
{
    {0x9E0, 4}
};

pm_uicc_specific_info_type pm8004_uicc_specific[1] =
{
    {PM_GPIO_21, PM_PBS_CLIENT_6, PM_LDO_9, PM_LDO_10, PM_LDO_INVALID, PM_LDO_INVALID}
};

//=======================================================================================

//Mode	            Logic Voltage	Memory Voltage
//Super Turbo	    1.05V	        1.05V
//Turbo	            0.9875V	        1.05V
//Normal	        0.9V	        0.95V
//SVS SOC	        0.8125V	        0.95V
//SVS Krait	        0.725V	        0.95V
//Retention	        0.5V	        0.675V


uint8 pm8004_num_of_cx_corners = 6;
uint32 pm8004_pm_cx_corners[6] = 
{ 500000, 725000, 812500, 900000, 987500, 1050000};
char pm8004_PM_MODEM_CX_VREG[50] = "/pmic/device/smps/A/smps1/vec";

uint8 pm8004_num_of_mx_corners = 6;
uint32 pm8004_pm_mx_corners[6] = 
{675000, 950000, 950000, 950000, 1050000, 1050000};
char pm8004_PM_MODEM_MX_VREG[50] = "/pmic/device/smps/A/smps2/vec";

pm_pwr_resource_info_type pm8004_mx_rail[1] = 
{
    {RPM_SMPS_A_REQ, 2, &pm8004_num_of_mx_corners, pm8004_pm_mx_corners, pm8004_PM_MODEM_MX_VREG }// data 1 is the size of the LUT, data 2 is the MX LUT, data3 is the RPM resource string 
};
pm_pwr_resource_info_type pm8004_cx_rail[1] = 
{
    {RPM_SMPS_A_REQ, 1, &pm8004_num_of_cx_corners, pm8004_pm_cx_corners, pm8004_PM_MODEM_CX_VREG }// data 1 is the size of the LUT, data 2 is the CX LUT, data3 is the RPM resource string.
};

pm_pbs_client_type pm8004_mss_enable_ocp_pbs_client = PM_PBS_CLIENT_11;
uint32  pm8004_mss_min_step_size_uV = 12000;

pm_pwr_resource_info_type pm8004_mss_rail[1] = 
{
    {RPM_SMPS_A_REQ, 7, NULL, &pm8004_mss_enable_ocp_pbs_client, &pm8004_mss_min_step_size_uV } //data 1 is the PBS sequence associated with MSS (if necessary)
    //data2 :  mss ocp workaround : disable ocp, save mode, mode=pwm, Enable mss, enable ocp, restore mode
    //data3 :  mss min step size uV: Used to calculate mss_stepper_rate
};

//use as initial values only, values may be changed runtime and are not static. 
pm_rail_cfg_info_type pm8004_mss_config_info[4] =
{
	[PM_MSS_VOLT_LEVEL] = {TRUE, 0}, 
	[PM_MSS_PBS_SEQ]	= {FALSE, 0},
	[PM_MSS_RES_1]		= {FALSE, 0},
	[PM_MSS_RES_2]		= {FALSE, 0},
};





