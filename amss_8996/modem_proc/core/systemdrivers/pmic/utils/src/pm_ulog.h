#ifndef __PMICULOG_H__
#define __PMICULOG_H__
/*
===========================================================================
*/
/**
  @file pm_ulog.h

  This header provides with initializaiton of PMIC ULOG.

  This file provides an abstraction layer for PMIC ULOG.

*/
/*
  ====================================================================

  Copyright (c) 2011-13 Qualcomm Technologies Incorporated.  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ====================================================================
  $Header: 
  $DateTime: 2016/03/28 23:02:17 $
  $Author: mplcsds1 $

  when       who     what, where, why
  --------   ---     -------------------------------------------------
  02/08/13   rl     Created.

  ====================================================================
*/


/*=========================================================================
      Include Files
==========================================================================*/

#include "ULogFront.h"


/*=========================================================================
      Structures
==========================================================================*/

struct pm_ulog_vars
{
	ULogHandle pm_log;  
	uint32 log_size;
}pm_ulog_vars;


void pm_ulog_init(void);


#endif // __PMICULOG_H__

