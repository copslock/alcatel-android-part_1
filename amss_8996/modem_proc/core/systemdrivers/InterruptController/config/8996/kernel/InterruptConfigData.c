/*==============================================================================

    D A L I N T E R R U P T   C O N T R O L L E R    



  DESCRIPTION
   This file is autogenerated for interrupt controller config for this platform.

          Copyright (c) 2014 QUALCOMM Technologies Incorporated.
                 All Rights Reserved.
              QUALCOMM Proprietary/GTDR

  ===========================================================================*/


#include "DALReg.h"
#include "DALDeviceId.h"
#include "DDIInterruptController.h"
#include "DALInterruptControllerConfig.h"

static InterruptConfigType InterruptConfigs[] = 
{
  {1, 4, DAL_IST_STACK_SIZE, "qdsp6_isdb_irq"},
  {2, 4, DAL_IST_STACK_SIZE, "q6_qtimer_irq[0]"},
  {3, 4, DAL_IST_STACK_SIZE, "q6_qtimer_irq[1]"},
  {10, 4, DAL_IST_STACK_SIZE, "dtr_rxfe_wbpwr0_evt"},
  {11, 4, DAL_IST_STACK_SIZE, "dtr_rxfe_wbpwr1_evt"},
  {12, 4, DAL_IST_STACK_SIZE, "dtr_rxfe_wbpwr2_evt"},
  {13, 4, DAL_IST_STACK_SIZE, "dtr_rxfe_wbpwr3_evt"},
  {14, 4, DAL_IST_STACK_SIZE, "dtr_rxfe_wbpwr4_evt"},
  {15, 4, DAL_IST_STACK_SIZE, "dtr_rxfe_wbpwr5_evt (3CA)"},
  {16, 4, DAL_IST_STACK_SIZE, "stmr_evt0"},
  {17, 4, DAL_IST_STACK_SIZE, "stmr_evt1"},
  {18, 4, DAL_IST_STACK_SIZE, "stmr_evt2"},
  {19, 4, DAL_IST_STACK_SIZE, "stmr_evt3"},
  {20, 4, DAL_IST_STACK_SIZE, "stmr_evt4"},
  {21, 4, DAL_IST_STACK_SIZE, "stmr_evt5"},
  {22, 4, DAL_IST_STACK_SIZE, "irq_hp_out[0]"},
  {23, 4, DAL_IST_STACK_SIZE, "irq_hp_out[1]"},
  {24, 4, DAL_IST_STACK_SIZE, "irq_hp_out[2]"},
  {25, 4, DAL_IST_STACK_SIZE, "irq_hp_out[3]"},
  {26, 4, DAL_IST_STACK_SIZE, "irq_hp_out[4]"},
  {27, 4, DAL_IST_STACK_SIZE, "irq_hp_out[5]"},
  {28, 4, DAL_IST_STACK_SIZE, "irq_hp_out[6]"},
  {29, 4, DAL_IST_STACK_SIZE, "irq_hp_out[7]"},
  {30, 4, DAL_IST_STACK_SIZE, "q6ss_idle_timer_irq"},
  {31, 4, DAL_IST_STACK_SIZE, "q6ss_cti_irq"},
  {32, 4, DAL_IST_STACK_SIZE, "rpm_ipc[12]"},
  {33, 4, DAL_IST_STACK_SIZE, "rpm_ipc[13]"},
  {34, 4, DAL_IST_STACK_SIZE, "rpm_ipc[14]"},
  {35, 4, DAL_IST_STACK_SIZE, "rpm_ipc[15]"},
  {36, 4, DAL_IST_STACK_SIZE, "APCS_mssHlosIPCInterrupt[0]"},
  {37, 4, DAL_IST_STACK_SIZE, "APCS_mssHlosIPCInterrupt[1]"},
  {38, 4, DAL_IST_STACK_SIZE, "APCS_mssHlosIPCInterrupt[2]"},
  {39, 4, DAL_IST_STACK_SIZE, "APCS_mssHlosIPCInterrupt[3]"},
  {40, 4, DAL_IST_STACK_SIZE, "q6ss_irq_out(12)"},
  {41, 4, DAL_IST_STACK_SIZE, "q6ss_irq_out(13)"},
  {42, 4, DAL_IST_STACK_SIZE, "q6ss_irq_out(14)"},
  {43, 4, DAL_IST_STACK_SIZE, "q6ss_irq_out(15)"},
  {56, 4, DAL_IST_STACK_SIZE, "q6ss_irq_out(12)"},
  {57, 4, DAL_IST_STACK_SIZE, "q6ss_irq_out(13)"},
  {58, 4, DAL_IST_STACK_SIZE, "q6ss_irq_out(14)"},
  {59, 4, DAL_IST_STACK_SIZE, "q6ss_irq_out(15)"},
  {60, 4, DAL_IST_STACK_SIZE, "APCS_mssHypIPCInterrupt[0]"},
  {61, 4, DAL_IST_STACK_SIZE, "APCS_mssHypIPCInterrupt[1]"},
  {62, 4, DAL_IST_STACK_SIZE, "APCS_mssHypIPCInterrupt[2]"},
  {63, 4, DAL_IST_STACK_SIZE, "APCS_mssHypIPCInterrupt[3]"},
  {64, 4, DAL_IST_STACK_SIZE, "audio_out1_irq"},
  {65, 4, DAL_IST_STACK_SIZE, "lpass_aud_slimbus_bam_ee2_irq"},
  {66, 4, DAL_IST_STACK_SIZE, "lpass_aud_slimbus_core_ee2_irq"},
  {67, 4, DAL_IST_STACK_SIZE, "lpass_qca_slimbus_bam_ee2_irq"},
  {68, 4, DAL_IST_STACK_SIZE, "lpass_qca_slimbus_core_ee2_irq"},
  {73, 4, DAL_IST_STACK_SIZE, "dir_conn_irq_lpa_dsp_0"},
  {74, 4, DAL_IST_STACK_SIZE, "dir_conn_irq_lpa_dsp_1"},
  {75, 4, DAL_IST_STACK_SIZE, "dir_conn_irq_lpa_dsp_2"},
  {76, 4, DAL_IST_STACK_SIZE, "dir_conn_irq_lpa_dsp_3"},
  {77, 4, DAL_IST_STACK_SIZE, "dir_conn_irq_lpa_dsp_4"},
  {78, 4, DAL_IST_STACK_SIZE, "dir_conn_irq_lpa_dsp_5"},
  {79, 4, DAL_IST_STACK_SIZE, "summary_irq_lpa_dsp"},
  {88, 4, DAL_IST_STACK_SIZE, "bam_irq[2]"},
  {90, 4, DAL_IST_STACK_SIZE, "ssc_qup_int"},
  {91, 4, DAL_IST_STACK_SIZE, "ssc_uart_int"},
  {92, 4, DAL_IST_STACK_SIZE, "bam_irq[0]"},
  {93, 4, DAL_IST_STACK_SIZE, "phss_uart_mss_int[0]"},
  {94, 4, DAL_IST_STACK_SIZE, "bam_irq[2]"},
  {95, 4, DAL_IST_STACK_SIZE, "phss_qup_mss_int[0]"},
  {96, 4, DAL_IST_STACK_SIZE, "qup_irq"},
  {97, 4, DAL_IST_STACK_SIZE, "qup_irq"},
  {98, 4, DAL_IST_STACK_SIZE, "qup_irq"},
  {99, 4, DAL_IST_STACK_SIZE, "qup_irq"},
  {100, 4, DAL_IST_STACK_SIZE, "qup_irq"},
  {101, 4, DAL_IST_STACK_SIZE, "qup_irq"},
  {102, 4, DAL_IST_STACK_SIZE, "ipa_bam_irq[1]"},
  {103, 4, DAL_IST_STACK_SIZE, "ipa_irq[1]"},
  {104, 4, DAL_IST_STACK_SIZE, "spmi_periph_irq[2]"},
  {105, 4, DAL_IST_STACK_SIZE, "pmic_arb_trans_done_irq[2]"},
  {106, 4, DAL_IST_STACK_SIZE, "dir_conn_irq_gss_0"},
  {107, 4, DAL_IST_STACK_SIZE, "dir_conn_irq_gss_1"},
  {108, 4, DAL_IST_STACK_SIZE, "summary_irq_gss"},
  {109, 4, DAL_IST_STACK_SIZE, "tsync_irq"},
  {110, 4, DAL_IST_STACK_SIZE, "dir_conn_irq_sensors_0"},
  {111, 4, DAL_IST_STACK_SIZE, "dir_conn_irq_sensors_1"},
  {112, 4, DAL_IST_STACK_SIZE, "summary_irq_sensors"},
  {114, 4, DAL_IST_STACK_SIZE, "vsense_q6_alarm_irq"},
  {116, 4, DAL_IST_STACK_SIZE, "o_tcsr_xpu2_msa_summary_intr"},
  {120, 4, DAL_IST_STACK_SIZE, "bypass_error_irpt"},
  {121, 4, DAL_IST_STACK_SIZE, "bypass_error_irpt"},
  {122, 4, DAL_IST_STACK_SIZE, "bypass_error_irpt"},
  {124, 4, DAL_IST_STACK_SIZE, "spdm_offline_irq"},
  {125, 4, DAL_IST_STACK_SIZE, "spdm_realtime_irq"},
  {126, 4, DAL_IST_STACK_SIZE, "qdss_etrbytecnt_irq"},
  {128, 4, DAL_IST_STACK_SIZE, "o_timeout_slave_mss_summary_intr"},
  {129, 4, DAL_IST_STACK_SIZE, "q6ss_wdog_irq"},
  {129, 4, DAL_IST_STACK_SIZE, "q6ss_wdog_irq"},
  {129, 4, DAL_IST_STACK_SIZE, "q6ss_wdog_irq"},
  {130, 4, DAL_IST_STACK_SIZE, "q6_qtimer_irq[2]"},
  {131, 4, DAL_IST_STACK_SIZE, "q6ss_avs_sw_done_irq"},
  {132, 4, DAL_IST_STACK_SIZE, "nav_irq"},
  {133, 4, DAL_IST_STACK_SIZE, "nav_dm_irq"},
  {134, 4, DAL_IST_STACK_SIZE, "mgpi_irq"},
  {135, 4, DAL_IST_STACK_SIZE, "crypto_bam_irq"},
  {136, 4, DAL_IST_STACK_SIZE, "crypto_irq"},
  {137, 4, DAL_IST_STACK_SIZE, "mss_ahb_access_err_irq"},
  {138, 4, DAL_IST_STACK_SIZE, "coxm_dir_uart_trig_intr"},
  {139, 4, DAL_IST_STACK_SIZE, "coxm_rx_wci2_type2_misalign_intr"},
  {140, 4, DAL_IST_STACK_SIZE, "coxm_rx_wci2_msg_intr"},
  {141, 4, DAL_IST_STACK_SIZE, "coxm_typ0_b4_tout_intr"},
  {142, 4, DAL_IST_STACK_SIZE, "coxm_uart_intr"},
  {143, 4, DAL_IST_STACK_SIZE, "coxm_wcn_priority_muxed_intr"},
  {144, 4, DAL_IST_STACK_SIZE, "rbcpr_ctrl_0_irq"},
  {147, 4, DAL_IST_STACK_SIZE, "uart0_dm_uim_intr"},
  {148, 4, DAL_IST_STACK_SIZE, "uart1_dm_uim_intr"},
  {149, 4, DAL_IST_STACK_SIZE, "uart2_dm_uim_intr"},
  {150, 4, DAL_IST_STACK_SIZE, "uart3_dm_uim_intr"},
  {151, 4, DAL_IST_STACK_SIZE, "uart0_dm_intr"},
  {152, 4, DAL_IST_STACK_SIZE, "uart1_dm_intr"},
  {153, 4, DAL_IST_STACK_SIZE, "uart2_dm_intr"},
  {154, 4, DAL_IST_STACK_SIZE, "uart3_dm_intr"},
  {159, 4, DAL_IST_STACK_SIZE, "mss_dgr_irq"},
  {160, 4, DAL_IST_STACK_SIZE, "rfc_ipc0_evt"},
  {161, 4, DAL_IST_STACK_SIZE, "rfc_ipc1_evt"},
  {162, 4, DAL_IST_STACK_SIZE, "rfc_ipc2_evt"},
  {163, 4, DAL_IST_STACK_SIZE, "rfc_ipc3_evt"},
  {164, 4, DAL_IST_STACK_SIZE, "stmr_evt6"},
  {165, 4, DAL_IST_STACK_SIZE, "stmr_evt7"},
  {166, 4, DAL_IST_STACK_SIZE, "stmr_evt8"},
  {167, 4, DAL_IST_STACK_SIZE, "stmr_evt9"},
  {168, 4, DAL_IST_STACK_SIZE, "stmr_evt10"},
  {169, 4, DAL_IST_STACK_SIZE, "stmr_evt11"},
  {170, 4, DAL_IST_STACK_SIZE, "stmr_evt12"},
  {171, 4, DAL_IST_STACK_SIZE, "stmr_evt13"},
  {172, 4, DAL_IST_STACK_SIZE, "stmr_evt14"},
  {173, 4, DAL_IST_STACK_SIZE, "stmr_evt15"},
  {174, 4, DAL_IST_STACK_SIZE, "stmr_evt16"},
  {175, 4, DAL_IST_STACK_SIZE, "stmr_evt17"},
  {176, 4, DAL_IST_STACK_SIZE, "stmr_evt18"},
  {177, 4, DAL_IST_STACK_SIZE, "stmr_evt19"},
  {178, 4, DAL_IST_STACK_SIZE, "stmr_evt20"},
  {179, 4, DAL_IST_STACK_SIZE, "stmr_evt21"},
  {180, 4, DAL_IST_STACK_SIZE, "stmr_evt22"},
  {181, 4, DAL_IST_STACK_SIZE, "stmr_evt23"},
  {182, 4, DAL_IST_STACK_SIZE, "stmr_evt24"},
  {183, 4, DAL_IST_STACK_SIZE, "stmr_evt25"},
  {184, 4, DAL_IST_STACK_SIZE, "stmr_evt26"},
  {185, 4, DAL_IST_STACK_SIZE, "stmr_evt27"},
  {186, 4, DAL_IST_STACK_SIZE, "stmr_evt28"},
  {187, 4, DAL_IST_STACK_SIZE, "stmr_evt29"},
  {188, 4, DAL_IST_STACK_SIZE, "stmr_evt30"},
  {189, 4, DAL_IST_STACK_SIZE, "stmr_evt31"},
  {190, 4, DAL_IST_STACK_SIZE, "dtr_rxfe_wbpwr6_evt (3CA)"},
  {191, 4, DAL_IST_STACK_SIZE, "dtr_rxfe_nbpwr00_evt"},
  {192, 4, DAL_IST_STACK_SIZE, "dtr_rxfe_nbpwr01_evt"},
  {193, 4, DAL_IST_STACK_SIZE, "dtr_rxfe_nbpwr02_evt"},
  {194, 4, DAL_IST_STACK_SIZE, "dtr_rxfe_nbpwr03_evt"},
  {195, 4, DAL_IST_STACK_SIZE, "dtr_rxfe_nbpwr04_evt"},
  {196, 4, DAL_IST_STACK_SIZE, "dtr_rxfe_nbpwr05_evt"},
  {197, 4, DAL_IST_STACK_SIZE, "dtr_rxfe_nbpwr06_evt"},
  {198, 4, DAL_IST_STACK_SIZE, "dtr_rxfe_nbpwr07_evt"},
  {199, 4, DAL_IST_STACK_SIZE, "dtr_rxfe_nbpwr08_evt"},
  {200, 4, DAL_IST_STACK_SIZE, "dtr_rxfe_nbpwr09_evt (3CA)"},
  {201, 4, DAL_IST_STACK_SIZE, "dtr_rxfe_nbpwr10_evt (3CA)"},
  {202, 4, DAL_IST_STACK_SIZE, "dtr_fbrx_capture_done_evt"},
  {203, 4, DAL_IST_STACK_SIZE, "dtr_reflog_capture_done_evt"},
  {204, 4, DAL_IST_STACK_SIZE, "dtr_update_ch0_done_evt"},
  {205, 4, DAL_IST_STACK_SIZE, "dtr_update_ch1_done_evt"},
  {206, 4, DAL_IST_STACK_SIZE, "dtr_update_ch2_done_evt"},
  {207, 4, DAL_IST_STACK_SIZE, "dtr_update_ch3_done_evt"},
  {208, 4, DAL_IST_STACK_SIZE, "dtr_update_ch4_done_evt"},
  {209, 4, DAL_IST_STACK_SIZE, "dtr_update_ch5_done_evt"},
  {210, 4, DAL_IST_STACK_SIZE, "dtr_update_ch6_done_evt"},
  {211, 4, DAL_IST_STACK_SIZE, "dtr_update_ch7_done_evt"},
  {214, 4, DAL_IST_STACK_SIZE, "dtr_txfe0_block_done"},
  {215, 4, DAL_IST_STACK_SIZE, "dtr_txfe1_block_done"},
  {216, 4, DAL_IST_STACK_SIZE, "offline_irq_out[0]"},
  {217, 4, DAL_IST_STACK_SIZE, "offline_irq_out[1]"},
  {218, 4, DAL_IST_STACK_SIZE, "offline_irq_out[2]"},
  {219, 4, DAL_IST_STACK_SIZE, "offline_irq_out[3]"},
  {220, 4, DAL_IST_STACK_SIZE, "offline_irq_out[4]"},
  {221, 4, DAL_IST_STACK_SIZE, "offline_irq_out[5]"},
  {222, 4, DAL_IST_STACK_SIZE, "offline_irq_out[6]"},
  {223, 4, DAL_IST_STACK_SIZE, "offline_irq_out[7]"},
  {224, 4, DAL_IST_STACK_SIZE, "offline_irq_out[8]"},
  {225, 4, DAL_IST_STACK_SIZE, "offline_irq_out[9]"},
  {226, 4, DAL_IST_STACK_SIZE, "offline_irq_out[10]"},
  {227, 4, DAL_IST_STACK_SIZE, "offline_irq_out[11]"},
  {228, 4, DAL_IST_STACK_SIZE, "offline_irq_out[12]"},
  {229, 4, DAL_IST_STACK_SIZE, "offline_irq_out[13]"},
  {230, 4, DAL_IST_STACK_SIZE, "offline_irq_out[14]"},
  {231, 4, DAL_IST_STACK_SIZE, "offline_irq_out[15]"},
  {232, 4, DAL_IST_STACK_SIZE, "offline_irq_out[16]"},
  {233, 4, DAL_IST_STACK_SIZE, "offline_irq_out[17]"},
  {234, 4, DAL_IST_STACK_SIZE, "offline_irq_out[18]"},
  {235, 4, DAL_IST_STACK_SIZE, "offline_irq_out[19]"},
  {236, 4, DAL_IST_STACK_SIZE, "offline_irq_out[20]"},
  {237, 4, DAL_IST_STACK_SIZE, "offline_irq_out[21]"},
  {238, 4, DAL_IST_STACK_SIZE, "offline_irq_out[22]"},
  {239, 4, DAL_IST_STACK_SIZE, "offline_irq_out[23]"},
  {240, 4, DAL_IST_STACK_SIZE, "offline_irq_out[24]"},
  {241, 4, DAL_IST_STACK_SIZE, "offline_irq_out[25]"},
  {242, 4, DAL_IST_STACK_SIZE, "offline_irq_out[26]"},
  {243, 4, DAL_IST_STACK_SIZE, "offline_irq_out[27]"},
  {244, 4, DAL_IST_STACK_SIZE, "offline_irq_out[28]"},
  {245, 4, DAL_IST_STACK_SIZE, "offline_irq_out[29]"},
  {246, 4, DAL_IST_STACK_SIZE, "offline_irq_out[30]"},
  {247, 4, DAL_IST_STACK_SIZE, "offline_irq_out[31]"},
  {248, 4, DAL_IST_STACK_SIZE, "offline_irq_out[32]"},
  {249, 4, DAL_IST_STACK_SIZE, "offline_irq_out[33]"},
  {250, 4, DAL_IST_STACK_SIZE, "offline_irq_out[34]"},
  {251, 4, DAL_IST_STACK_SIZE, "offline_irq_out[35]"},
  {252, 4, DAL_IST_STACK_SIZE, "offline_irq_out[36]"},
  {253, 4, DAL_IST_STACK_SIZE, "offline_irq_out[37]"},
  {254, 4, DAL_IST_STACK_SIZE, "offline_irq_out[38]"},
  {255, 4, DAL_IST_STACK_SIZE, "offline_irq_out[39]"},
  {256, 4, DAL_IST_STACK_SIZE, "offline_irq_out[40]"},
  {257, 4, DAL_IST_STACK_SIZE, "offline_irq_out[41]"},
  {258, 4, DAL_IST_STACK_SIZE, "offline_irq_out[42]"},
  {259, 4, DAL_IST_STACK_SIZE, "offline_irq_out[43]"},
  {260, 4, DAL_IST_STACK_SIZE, "offline_irq_out[44]"},
  {261, 4, DAL_IST_STACK_SIZE, "offline_irq_out[45]"},
  {262, 4, DAL_IST_STACK_SIZE, "offline_irq_out[46]"},
  {263, 4, DAL_IST_STACK_SIZE, "offline_irq_out[47]"},
  {264, 4, DAL_IST_STACK_SIZE, "offline_irq_out[48]"},
  {265, 4, DAL_IST_STACK_SIZE, "offline_irq_out[49]"},
  {266, 4, DAL_IST_STACK_SIZE, "offline_irq_out[50]"},
  {267, 4, DAL_IST_STACK_SIZE, "offline_irq_out[51]"},
  {268, 4, DAL_IST_STACK_SIZE, "offline_irq_out[52]"},
  {269, 4, DAL_IST_STACK_SIZE, "offline_irq_out[53]"},
  {270, 4, DAL_IST_STACK_SIZE, "offline_irq_out[54]"},
  {271, 4, DAL_IST_STACK_SIZE, "offline_irq_out[55]"},
  {272, 4, DAL_IST_STACK_SIZE, "offline_irq_out[56]"},
  {273, 4, DAL_IST_STACK_SIZE, "offline_irq_out[57]"},
  {274, 4, DAL_IST_STACK_SIZE, "offline_irq_out[58]"},
  {275, 4, DAL_IST_STACK_SIZE, "offline_irq_out[59]"},
  {276, 4, DAL_IST_STACK_SIZE, "offline_irq_out[60]"},
  {277, 4, DAL_IST_STACK_SIZE, "offline_irq_out[61]"},
  {278, 4, DAL_IST_STACK_SIZE, "offline_irq_out[62]"},
  {279, 4, DAL_IST_STACK_SIZE, "offline_irq_out[63]"},
  {280, 4, DAL_IST_STACK_SIZE, "offline_irq_out[64]"},
  {281, 4, DAL_IST_STACK_SIZE, "offline_irq_out[65]"},
  {282, 4, DAL_IST_STACK_SIZE, "offline_irq_out[66]"},
  {283, 4, DAL_IST_STACK_SIZE, "offline_irq_out[67]"},
  {284, 4, DAL_IST_STACK_SIZE, "offline_irq_out[68]"},
  {285, 4, DAL_IST_STACK_SIZE, "offline_irq_out[69]"},
  {286, 4, DAL_IST_STACK_SIZE, "offline_irq_out[70]"},
  {287, 4, DAL_IST_STACK_SIZE, "offline_irq_out[71]"},
  {288, 4, DAL_IST_STACK_SIZE, "offline_irq_out[72]"},
  {289, 4, DAL_IST_STACK_SIZE, "offline_irq_out[73]"},
  {290, 4, DAL_IST_STACK_SIZE, "offline_irq_out[74]"},
  {291, 4, DAL_IST_STACK_SIZE, "offline_irq_out[75]"},
  {292, 4, DAL_IST_STACK_SIZE, "offline_irq_out[76]"},
  {293, 4, DAL_IST_STACK_SIZE, "offline_irq_out[77]"},
  {294, 4, DAL_IST_STACK_SIZE, "offline_irq_out[78]"},
  {295, 4, DAL_IST_STACK_SIZE, "offline_irq_out[79]"},
  {296, 4, DAL_IST_STACK_SIZE, "offline_irq_out[80]"},
  {297, 4, DAL_IST_STACK_SIZE, "offline_irq_out[81]"},
  {298, 4, DAL_IST_STACK_SIZE, "offline_irq_out[82]"},
  {299, 4, DAL_IST_STACK_SIZE, "offline_irq_out[83]"},
  {300, 4, DAL_IST_STACK_SIZE, "offline_irq_out[84]"},
  {301, 4, DAL_IST_STACK_SIZE, "offline_irq_out[85]"},
  {302, 4, DAL_IST_STACK_SIZE, "offline_irq_out[86]"},
  {303, 4, DAL_IST_STACK_SIZE, "offline_irq_out[87]"},
  {304, 4, DAL_IST_STACK_SIZE, "offline_irq_out[88]"},
  {305, 4, DAL_IST_STACK_SIZE, "offline_irq_out[89]"},
  {306, 4, DAL_IST_STACK_SIZE, "offline_irq_out[90]"},
  {307, 4, DAL_IST_STACK_SIZE, "offline_irq_out[91]"},
  {308, 4, DAL_IST_STACK_SIZE, "offline_irq_out[92]"},
  {309, 4, DAL_IST_STACK_SIZE, "offline_irq_out[93]"},
  {310, 4, DAL_IST_STACK_SIZE, "offline_irq_out[94]"},
  {311, 4, DAL_IST_STACK_SIZE, "offline_irq_out[95]"},
  {312, 4, DAL_IST_STACK_SIZE, "offline_irq_out[96]"},
  {313, 4, DAL_IST_STACK_SIZE, "offline_irq_out[97]"},
  {314, 4, DAL_IST_STACK_SIZE, "offline_irq_out[98]"},
  {315, 4, DAL_IST_STACK_SIZE, "offline_irq_out[99]"},
  {316, 4, DAL_IST_STACK_SIZE, "offline_irq_out[100]"},
  {317, 4, DAL_IST_STACK_SIZE, "offline_irq_out[101]"},
  {318, 4, DAL_IST_STACK_SIZE, "offline_irq_out[102]"},
  {319, 4, DAL_IST_STACK_SIZE, "offline_irq_out[103]"},
  {320, 4, DAL_IST_STACK_SIZE, "offline_irq_out[104]"},
  {321, 4, DAL_IST_STACK_SIZE, "offline_irq_out[105]"},
  {322, 4, DAL_IST_STACK_SIZE, "offline_irq_out[106]"},
  {323, 4, DAL_IST_STACK_SIZE, "offline_irq_out[107]"},
  {324, 4, DAL_IST_STACK_SIZE, "offline_irq_out[108]"},
  {325, 4, DAL_IST_STACK_SIZE, "offline_irq_out[109]"},
  {326, 4, DAL_IST_STACK_SIZE, "offline_irq_out[110]"},
  {327, 4, DAL_IST_STACK_SIZE, "offline_irq_out[111]"},
  {328, 4, DAL_IST_STACK_SIZE, "offline_irq_out[112]"},
  {329, 4, DAL_IST_STACK_SIZE, "offline_irq_out[113]"},
  {330, 4, DAL_IST_STACK_SIZE, "offline_irq_out[114]"},
  {331, 4, DAL_IST_STACK_SIZE, "offline_irq_out[115]"},
  {332, 4, DAL_IST_STACK_SIZE, "offline_irq_out[116]"},
  {333, 4, DAL_IST_STACK_SIZE, "offline_irq_out[117]"},
  {334, 4, DAL_IST_STACK_SIZE, "offline_irq_out[118]"},
  {335, 4, DAL_IST_STACK_SIZE, "offline_irq_out[119]"},
  {336, 4, DAL_IST_STACK_SIZE, "offline_irq_out[120]"},
  {337, 4, DAL_IST_STACK_SIZE, "offline_irq_out[121]"},
  {338, 4, DAL_IST_STACK_SIZE, "offline_irq_out[122]"},
  {339, 4, DAL_IST_STACK_SIZE, "offline_irq_out[123]"},
  {340, 4, DAL_IST_STACK_SIZE, "offline_irq_out[124]"},
  {341, 4, DAL_IST_STACK_SIZE, "offline_irq_out[125]"},
  {342, 4, DAL_IST_STACK_SIZE, "offline_irq_out[126]"},
  {343, 4, DAL_IST_STACK_SIZE, "offline_irq_out[127]"},
  {344, 4, DAL_IST_STACK_SIZE, "offline_irq_out[128]"},
  {345, 4, DAL_IST_STACK_SIZE, "offline_irq_out[129]"},
  {346, 4, DAL_IST_STACK_SIZE, "offline_irq_out[130]"},
  {347, 4, DAL_IST_STACK_SIZE, "offline_irq_out[131]"},
  {348, 4, DAL_IST_STACK_SIZE, "offline_irq_out[132]"},
  {349, 4, DAL_IST_STACK_SIZE, "offline_irq_out[133]"},
  {350, 4, DAL_IST_STACK_SIZE, "offline_irq_out[134]"},
  {351, 4, DAL_IST_STACK_SIZE, "offline_irq_out[135]"},
  {352, 4, DAL_IST_STACK_SIZE, "offline_irq_out[136]"},
  {353, 4, DAL_IST_STACK_SIZE, "offline_irq_out[137]"},
  {354, 4, DAL_IST_STACK_SIZE, "offline_irq_out[138]"},
  {355, 4, DAL_IST_STACK_SIZE, "offline_irq_out[139]"},
  {356, 4, DAL_IST_STACK_SIZE, "offline_irq_out[140]"},
  {357, 4, DAL_IST_STACK_SIZE, "offline_irq_out[141]"},
  {358, 4, DAL_IST_STACK_SIZE, "offline_irq_out[142]"},
  {359, 4, DAL_IST_STACK_SIZE, "offline_irq_out[143]"},
  {360, 4, DAL_IST_STACK_SIZE, "offline_irq_out[144]"},
  {361, 4, DAL_IST_STACK_SIZE, "offline_irq_out[145]"},
  {362, 4, DAL_IST_STACK_SIZE, "offline_irq_out[146]"},
  {363, 4, DAL_IST_STACK_SIZE, "offline_irq_out[147]"},
  {364, 4, DAL_IST_STACK_SIZE, "offline_irq_out[148]"},
  {365, 4, DAL_IST_STACK_SIZE, "offline_irq_out[149]"},
  {INVALID_INTERRUPT, 0, DAL_IST_STACK_SIZE, ""}
};


InterruptPlatformDataType  pInterruptControllerConfigData[] =
{
  {
    InterruptConfigs,
    366,
  }
};
