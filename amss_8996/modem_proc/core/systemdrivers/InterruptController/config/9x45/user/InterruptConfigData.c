/*==============================================================================

  D A L I N T E R R U P T   C O N T R O L L E R    

DESCRIPTION
 This file Contains the Interrupt configuration data for all the interrupts for 
 this platform.

REFERENCES

        Copyright � 2011 Qualcomm Technologies Incorporated.
               All Rights Reserved.
            QUALCOMM Proprietary/GTDR
===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/core.mpss/3.4.c3.11/systemdrivers/InterruptController/config/9x45/user/InterruptConfigData.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
1/1/2010   aratin  First draft created. 
===========================================================================*/

#include "DALReg.h"
#include "DALDeviceId.h"
#include "DDIInterruptController.h"
#include "DALInterruptControllerUserConfig.h"



/*=========================================================================

                           DATA DEFINITIONS

===========================================================================*/

/*
 * Interrupt_Configs
 * BSP data for this target's interrupts.
 * The data layout is the following :
 * {  l2VIC interrupt vector number , Interrupt Service thread stack size , interrupt name}
 *
 */
static InterruptConfigType InterruptConfigs[] = 
{
  {0, "qdsp6_etm_irq"},
  {1, "qdsp6_isdb_irq"},
  {2, "qtmr_phy_irq[0]"},
  {3, "qtmr_phy_irq[1]"},
  {6, "irq_hp_out[8]"},
  {7, "irq_hp_out[9]"},
  {8, "irq_hp_out[10]"},
  {9, "irq_hp_out[11]"},
  {10, "dtr_rxfe_wbpwr0_evt"},
  {11, "dtr_rxfe_wbpwr1_evt"},
  {12, "dtr_rxfe_wbpwr2_evt"},
  {13, "dtr_rxfe_wbpwr3_evt"},
  {14, "dtr_rxfe_wbpwr4_evt"},
  {15, "dtr_rxfe_wbpwr5_evt"},
  {16, "stmr_evt0"},
  {17, "stmr_evt1"},
  {18, "stmr_evt2"},
  {19, "stmr_evt3"},
  {20, "stmr_evt4"},
  {21, "stmr_evt5"},
  {22, "irq_hp_out[0]"},
  {23, "irq_hp_out[1]"},
  {24, "irq_hp_out[2]"},
  {25, "irq_hp_out[3]"},
  {26, "irq_hp_out[4]"},
  {27, "irq_hp_out[5]"},
  {28, "irq_hp_out[6]"},
  {29, "irq_hp_out[7]"},
  {30, "q6ss_idle_timer_irq"},
  {31, "q6ss_cti_irq"},
  {32, "rpm_ipc[12]"},
  {33, "rpm_ipc[13]"},
  {34, "rpm_ipc[14]"},
  {35, "rpm_ipc[15]"},
  {36, "APCS_sysIPCInterrupt[12]"},
  {37, "APCS_sysIPCInterrupt[13]"},
  {38, "APCS_sysIPCInterrupt[14]"},
  {39, "APCS_sysIPCInterrupt[15]"},
  {46, "audio_out1_irq"},
  {64, "q6ss_avs_cpu_up_irq"},
  {65, "lpass_sb_bam_ee0_irq"},
  {65, "q6ss_avs_cpu_dn_irq"},
  {66, "lpass_sb_core_ee0_irq"},
  {67, "lpass_sb_bam_ee1_irq"},
  {67, "q6ss_avs_irq"},
  {68, "lpass_sb_core_ee1_irq"},
  {69, "vfr_irq_mux_out[0]"},
  {70, "vfr_irq_mux_out[1]"},
  {71, "bus_irq"},
  {72, "avtimer_int_0"},
  {73, "dir_conn_irq_lpa_dsp[0]"},
  {74, "dir_conn_irq_lpa_dsp[1]"},
  {75, "dir_conn_irq_lpa_dsp[2]"},
  {88, "qpic_lcdc_irq"},
  {89, "qpic_bam_irq[1]"},
  {90, "qpic_nandc_irq"},
  {91, "qpic_nandc_op_done_irq"},
  {92, "qpic_nandc_wr_er_done_irq"},
  {93, "phss_uart_mss_int[0]"},
  {94, "bam_irq[2]"},
  {95, "phss_qup_mss_int[0]"},
  {96, "peripheral_irq[4]"},
  {97, "peripheral_irq[5]"},
  {98, "peripheral_irq[6]"},
  {99, "peripheral_irq[7]"},
  {102, "ipa_bam_irq[1]"},
  {103, "Ipa_irq[1]"},
  {104, "ee2_mss_spmi_periph_irq"},
  {105, "channel2_mss_trans_done_irq"},
  {106, "dir_conn_irq_mss[0]"},
  {107, "dir_conn_irq_mss[1]"},
  {108, "summary_irq_mss"},
  {109, "tsync_irq"},
  {110, "dir_conn_irq_sensors[1]"},
  {111, "dir_conn_irq_sensors[0]"},
  {112, "summary_irq_sensors"},
  {113, "pcie20_intb _mhi_q6"},
  {116, "o_tcsr_xpu2_msa_summary_intr"},
  {117, "modem_va_pa_mismatch_irq_qdsp"},
  {118, "modem_va_pa_mismatch_irq_ipa"},
  {119, "modem_va_pa_mismatch_irq_ce"},
  {120, "mcdma_ch0_done"},
  {121, "mcdma_ch1_done"},
  {122, "mcdma_ch2_done"},
  {123, "mcdma_ch3_done"},
  {124, "mcdma_vbuf_done"},
  {124, "spdm_offline_irq"},
  {125, "spdm_realtime_irq[3]"},
  {126, "qdss_etrbytecnt_irq"},
  {128, "o_timeout_slave_mss_summary_intr"},
  {129, "q6_wdog_irq"},
  {130, "qtmr_phy_irq[2]"},
  {131, "q6ss_avs_sw_done_irq"},
  {132, "nav_irq"},
  {133, "nav_dm_irq"},
  {134, "mgpi_irq"},
  {135, "bam_irq[0]"},
  {136, "crypto_core_irq[0]"},
  {137, "mss_ahb_access_err_irq"},
  {138, "coxm_dir_uart_trig_intr"},
  {139, "coxm_rx_wci2_type2_misalign_intr"},
  {140, "coxm_rx_wci2_msg_intr"},
  {141, "coxm_typ0_b4_tout_intr"},
  {142, "coxm_uart_intr"},
  {143, "coxm_wcn_priority_muxed_irq"},
  {144, "rbif_irq[0]"},
  {145, "rbif_irq[1]"},
  {146, "rbif_irq[2]"},
  {147, "uart_dm_intr"},
  {148, "uart_dm_intr"},
  {149, "uart_dm_intr"},
  {150, "uart_dm_intr"},
  {151, "uart_dm_uim_intr"},
  {152, "uart_dm_uim_intr"},
  {153, "uart_dm_uim_intr"},
  {154, "uart_dm_uim_intr"},
  {160, "rfc_ipc0_evt"},
  {161, "rfc_ipc1_evt"},
  {162, "rfc_ipc2_evt"},
  {163, "rfc_ipc3_evt"},
  {164, "stmr_evt6"},
  {165, "stmr_evt7"},
  {166, "stmr_evt8"},
  {167, "stmr_evt9"},
  {168, "stmr_evt10"},
  {169, "stmr_evt11"},
  {170, "stmr_evt12"},
  {171, "stmr_evt13"},
  {172, "stmr_evt14"},
  {173, "stmr_evt15"},
  {174, "stmr_evt16"},
  {175, "stmr_evt17"},
  {176, "stmr_evt18"},
  {177, "stmr_evt19"},
  {178, "stmr_evt20"},
  {179, "stmr_evt21"},
  {180, "stmr_evt22"},
  {181, "stmr_evt23"},
  {182, "stmr_evt24"},
  {183, "stmr_evt25"},
  {184, "stmr_evt26"},
  {185, "stmr_evt27"},
  {186, "stmr_evt28"},
  {187, "stmr_evt29"},
  {188, "stmr_evt30"},
  {189, "stmr_evt31"},
  {190, "dtr_rxfe_wbpwr6_evt"},
  {191, "dtr_rxfe_nbpwr00_evt"},
  {192, "dtr_rxfe_nbpwr01_evt"},
  {193, "dtr_rxfe_nbpwr02_evt"},
  {194, "dtr_rxfe_nbpwr03_evt"},
  {195, "dtr_rxfe_nbpwr04_evt"},
  {196, "dtr_rxfe_nbpwr05_evt"},
  {197, "dtr_rxfe_nbpwr06_evt"},
  {198, "dtr_rxfe_nbpwr07_evt"},
  {199, "dtr_rxfe_nbpwr08_evt"},
  {200, "dtr_rxfe_nbpwr08_evt"},
  {201, "dtr_rxfe_nbpwr10_evt"},
  {202, "dtr_fbrx_capture_done_evt"},
  {203, "dtr_reflog_capture_done_evt"},
  {204, "dtr_update_ch0_done_evt"},
  {205, "dtr_update_ch1_done_evt"},
  {206, "dtr_update_ch2_done_evt"},
  {207, "dtr_update_ch3_done_evt"},
  {208, "dtr_update_ch4_done_evt"},
  {209, "dtr_update_ch5_done_evt"},
  {210, "dtr_update_ch6_done_evt"},
  {211, "dtr_update_ch7_done_evt"},
  {214, "dtr_txfe0_block_done"},
  {215, "dtr_txfe1_block_done"},
  {255, "smmu_bus_intr[25]"},
  {262, "smmu_bus_intr[26]"},
  {INVALID_INTERRUPT, ""}
};

/*
 * InterruptPlatformDataType 
 * This is used by the Interrupt controller platform to query platform specific data. 
 */
InterruptPlatformDataType  pInterruptControllerConfigData[] =
{
  {
    InterruptConfigs,           /* pIRQConfigs */
    264,
  }
};
