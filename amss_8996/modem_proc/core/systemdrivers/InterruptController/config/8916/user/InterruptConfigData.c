/*==============================================================================

  D A L I N T E R R U P T   C O N T R O L L E R    

DESCRIPTION
 This file Contains the Interrupt configuration data for all the interrupts for 
 this platform.

REFERENCES

        Copyright � 2013 Qualcomm Technologies Incorporated.
               All Rights Reserved.
            QUALCOMM Proprietary/GTDR
===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/core.mpss/3.4.c3.11/systemdrivers/InterruptController/config/8916/user/InterruptConfigData.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
1/1/2010   aratin  First draft created. 
===========================================================================*/

#include "DALReg.h"
#include "DALDeviceId.h"
#include "DDIInterruptController.h"
#include "DALInterruptControllerUserConfig.h"



/*=========================================================================

                           DATA DEFINITIONS

===========================================================================*/

/*
 * Interrupt_Configs
 * BSP data for this target's interrupts.
 * The data layout is the following :
 * {  l2VIC interrupt vector number, Interrupt Service thread stack size, interrupt name}
 *
 */
static InterruptConfigType InterruptConfigs[] = 
{
  {0,"qdsp6_etm_irq"},
  {1,"qdsp6_isdb_irq"},
  {2,"qtmr_phy_irq[0]"},
  {3,"qtmr_phy_irq[1]"},
  {10,"demss_cdma_mdsp_finger_irq"},
  {11,"demss_cdma_mdsp_combiner_irq"},
  {12,"1x_rtc_tx_64chip_irq"},
  {13,"rxfe_wbpwr0_irq"},
  {14,"rxfe_wbpwr1_irq"},
  {15,"rxfe_wbpwr2_irq"},
  {16,"rxfe_wbpwr3_irq"},
  {17,"umts_stmr_snp0_irq"},
  {18,"umts_stmr_snp1_irq"},
  {19,"umts_stmr_tx_chipx256_irq"},
  {20,"hdr_rtc_tx_irq"},
  {21,"o_stmr0_irq"},
  {22,"g_stmr_fw_irq"},
  {23,"tds_stmr_wall_irq"},
  {24,"vpe_gp0_irq"},
  {25,"vpe_gp1_irq"},
  {26,"vpe_gp2_irq"},
  {27,"ccs_swi0_irq"},
  {28,"ccs_swi1_irq"},
  {29,"ccs_swi2_irq"},
  {30,"q6ss_idle_timer_irq"},
  {31,"q6ss_cti_irq"},
  {32,"rpm_ipc[12]"},
  {33,"rpm_ipc[13]"},
  {34,"rpm_ipc[14]"},
  {35,"rpm_ipc[15]"},
  {36,"APCS_mssIPCInterrupt[0]"},
  {37,"APCS_mssIPCInterrupt[1]"},
  {38, "APCS_mssIPCInterrupt[2]"},
  {39,"APCS_mssIPCInterrupt[3]"},
  {40,"o_wcss_lpass_mbox_intr"},
  {41,"dir_conn_irq_sensors[0]"},
  {42,"dir_conn_irq_sensors[1]"},
  {43,"summary_irq_sensors"},
  {44,"o_wcss_lpass_bt_tx_intr"},
  {45,"avtimer_int0_irq"},
  {46,"audio_out1_irq"},
  {47,"o_wcss_lpass_bt_rx_intr"},
  {48,"o_wcss_mss_sw_intr_0"},
  {49,"o_wcss_mss_sw_intr_1"},
  {50,"o_wcss_mss_sw_intr_2"},
  {51,"o_wcss_mss_sw_intr_3"},
  {56,"o_wcss_lpass_fm_intr"},
  {59,"vfr_irq_mux_out[0]"},
  {60,"vfr_irq_mux_out[1]"},
  {61,"dir_conn_irq_lpa_dsp[0]"},
  {62,"dir_conn_irq_lpa_dsp[1]"},
  {63,"dir_conn_irq_lpa_dsp[2]"},
  {64,"q6ss_avs_cpu_up_irq"},
  {65,"q6ss_avs_cpu_dn_irq"},
  {66,"q6ss_avs_sw_done_irq"},
  {67,"q6ss_avs_irq"},
  {68,"q6_wdog_irq"},
  {69,"o_tcsr_xpu2_msa_summary_intr"},
  {70,"xpu2_msa_intr"},
  {71,"dir_conn_irq_mss[0]"},
  {72,"dir_conn_irq_mss[1]"},
  {73,"crypto_core_irq[0]"},
  {74,"bam_irq[0]"},
  {75,"ee2_mss_spmi_periph_irq"},
  {76,"channel2_mss_trans_done_irq"},
  {81,"spdm_realtime_irq[3]"},
  {82,"spdm_offline_irq"},
  {83,"mss_ahb_access_err_irq"},
  {84,"nav_irq"},
  {85,"nav_dm_irq"},
  {86,"tsync_irq"},
  {87,"uart_dm_intr"},
  {88,"uart_dm_intr"},
  {89,"uart_dm_uim_intr"},
  {90,"uart_dm_uim_intr"},
  {96,"sys_online0_irq"},
  {97,"sys_online1_irq"},
  {98,"umts_stmr_pri0_irq"},
  {99,"umts_stmr_pri1_irq"},
  {100,"umts_stmr_pri2_irq"},
  {101,"umts_stmr_st_event_irq"},
  {102,"umts_stmr_tx_10ms_irq"},
  {103,"hdr_rtc_rx_irq"},
  {104,"hdr_rtc_pnroll_irq"},
  {105,"hdr_rtc_slot_irq"},
  {106,"hdr_rtc_fr_irq"},
  {107,"hdr_rtc_even_sec_irq"},
  {108,"sdo_rtc_fr_irq"},
  {109,"sdo_rtc_slot_irq"},
  {110,"1x_rtc_gp2_irq"},
  {111,"1x_rtc_gp1_irq"},
  {112,"1x_rtc_pnroll_irq"},
  {113,"1x_rtc_tx_slot_irq"},
  {114,"1x_rtc_slot_irq"},
  {115,"1x_rtc_tx_fr_irq"},
  {116,"o_stmr1_irq"},
  {117,"tds_stmr_rx_irq"},
  {118,"tds_stmr_st_event_irq"},
  {119,"g_stmr_sw_irq"},
  {120,"mcdma_ch0_done"},
  {121,"mcdma_ch1_done"},
  {122,"mcdma_ch2_done"},
  {123,"mcdma_ch3_done"},
  {124,"mcdma_vbuf_done"},
  {125,"error_irq"},
  {138, "a2_ulper_irq"},
  {139, "rxfe_wrt_trig0_irq"},
  {140, "rxfe_wrt_trig1_irq"},
  {141, "rxfe_wrt_trig2_irq"},
  {142, "rxfe_wrt_trig3_irq"},
  {143, "rxfe_wrt_trig4_irq"},
  {144, "rxfe_wrt_trig5_irq"},
  {145, "rxfe_wrt_trig6_irq"},
  {146, "rxfe_nbpwr0_irq"},
  {147, "rxfe_nbpwr1_irq"},
  {148, "rxfe_nbpwr2_irq"},
  {149, "rxfe_nbpwr3_irq"},
  {150, "rxfe_nbpwr4_irq"},
  {151, "rxfe_nbpwr5_irq"},
  {152, "rxfe_nbpwr6_irq"},
  {153, "a2_frag0_irq"},
  {154, "a2_frag1_irq"},
  {155, "tx_mp_rd_irq"},
  {156, "tx_mp_wr_irq"},
  {157, "tx_enc_irq"},
  {159, "demss_cdma_gp_cmp_irq"},
  {160, "demss_cdma_26m_irq"},
  {161, "demss_cdma_time2_irq"},
  {162, "demss_cdma_time1_irq"},
  {163, "demss_cdma_quick_paging_irq"},
  {164, "demss_cdma_offl_dn_irq"},
  {165, "demss_cdma_pnroll_irq"},
  {166, "demss_cdma_srchq_done_irq"},
  {167, "demss_cdma_dem1x_ret_irq"},
  {168, "demss_cdma_dem1x_adv_irq"},
  {169, "demss_cdma_qlic_irq"},
  {170, "demss_cdma_4_eeq_irq"},
  {171, "demss_cdma_4_a5_irq"},
  {172, "demss_cdma_4_gea_irq"},
  {173, "demss_umts_dp_done_irq"},
  {174, "a2_tmr0_irq"},
  {175, "a2_tmr1_irq"},
  {176, "a2_tmr2_irq"},
  {177, "a2_tmr3_irq"},
  {178, "a2_decob_irq"},
  {179, "demback_sch_deint_done_irq"},
  {180, "demback_hsscch_p2_irq"},
  {181, "demback_hsscch_p1_irq"},
  {182, "demback_eagch_dn_irq"},
  {183, "demback_eagch_cl_dn_irq"},
  {184, "demback_decib_wr_done_irq"},
  {185, "demback_mdsp_td_done_irq"},
  {186, "demback_mdsp_fch_done_irq"},
  {187, "demback_dec_done_irq"},
  {188, "tdec_c_cp_ping_irq"},
  {189, "tdec_c_dn_ping_irq"},
  {190, "tdec_evt_irq"},
  {192, "vpe_gp4_irq"},
  {193, "vpe_gp5_irq"},
  {194, "vpe_gp6_irq"},
  {195, "vpe_gp7_irq"},
  {196, "vpe_gp8_irq"},
  {197, "vpe_gp9_irq"},
  {198, "a2_dma0_irq"},
  {199, "a2_dma1_irq"},
  {200, "a2_dma2_irq"},
  {201, "a2_dma3_irq"},
  {202, "a2_dma4_irq"},
  {203, "vpe_gp3_irq"},
  {204, "vpe_brdg0_irq"},
  {205, "vpe_brdg1_irq"},
  {206, "vpe_brdg2_irq"},
  {207, "vpe_brdg3_irq"},
  {208, "ccs_swi3_irq"},
  {209, "ccs_dma0_irq"},
  {210, "ccs_dma1_irq"},
  {211, "ccs_dma2_irq"},
  {212, "ccs_dma3_irq"},
  {213, "ccs_dma4_irq"},
  {214, "ccs_dma5_irq"},
  {215, "ccs_dma6_irq"},
  {216, "ccs_dma7_irq"},
  {217, "g_stmr_sw_irq_g1"},
  {218, "g_stmr_fw_irq_g1"},
  {219, "demss_cdma_4_a5_irq_g1"},
  {220, "demss_cdma_4_gea_irq_g1"},
  {221, "g_stmr_sw_irq_g2"},
  {222, "g_stmr_fw_irq_g2"},
  {223, "sys_online2_irq"},
  {238, "o_timeout_slave_mss_summary_intr"},
  {239, "summary_irq_mss"},
  {242, "qtmr_phy_irq[2]"},
  {243, "phss_uart_mss_int[0]"},
  {244, "bam_irq[2]"},
  {245, "phss_qup_mss_int[0]"},
  {249, "qdss_etrbytecnt_irq"},
  {253, "uart_dm_intr"},
  {254, "uart_dm_uim_intr"},
  {255, "smmu_bus_intr[25]"},
  {259, "bus_irq"},
  {262, "smmu_bus_intr[26]"},
  {263, "smmu_bus_intr[27]"},
  {INVALID_INTERRUPT, ""}
};

/*
 * InterruptPlatformDataType 
 * This is used by the Interrupt controller platform to query platform specific data. 
 */
InterruptPlatformDataType  pInterruptControllerConfigData[] =
{
  {
    InterruptConfigs,           /* pIRQConfigs */
    264,
  }
};
