#ifndef __VCSMSSHWIO_H__
#define __VCSMSSHWIO_H__
/*
===========================================================================
*/
/**
  @file VCSMSSHWIO.h
  @brief Auto-generated HWIO interface include file.

  This file contains HWIO register definitions for the following modules:
    MSS_MVC
    SECURITY_CONTROL_CORE

  'Include' filters applied: MVC_MEM_SVS QFPROM_CORR_CALIB_ROW0_LSB QFPROM_CORR_CALIB_ROW0_MSB QFPROM_CORR_CALIB_ROW1_LSB QFPROM_CORR_CALIB_ROW1_MSB QFPROM_CORR_CALIB_ROW2_LSB QFPROM_CORR_CALIB_ROW2_MSB QFPROM_RAW_PTE_ROW1_MSB 
  'Exclude' filters applied: RESERVED DUMMY 
*/
/*
  ===========================================================================

  Copyright (c) 2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

  ===========================================================================

  $Header: //components/rel/core.mpss/3.4.c3.11/systemdrivers/vcs/hw/msm8996/mss/inc/VCSMSSHWIO.h#1 $
  $DateTime: 2016/03/28 23:02:17 $
  $Author: mplcsds1 $

  ===========================================================================
*/

#include <HALhwio.h>

/*
 * HWIO base definitions
 */
extern uint32                      HAL_ldo_nHWIOBaseMSS;
#define MODEM_TOP_BASE             HAL_ldo_nHWIOBaseMSS

extern uint32                      HAL_ldo_nHWIOBaseSecurity;
#define SECURITY_CONTROL_BASE      HAL_ldo_nHWIOBaseSecurity


/*----------------------------------------------------------------------------
 * MODULE: SECURITY_CONTROL_CORE
 *--------------------------------------------------------------------------*/

#define SECURITY_CONTROL_CORE_REG_BASE                                                        (SECURITY_CONTROL_BASE      + 0x00000000)
#define SECURITY_CONTROL_CORE_REG_BASE_OFFS                                                   0x00000000

#define HWIO_QFPROM_RAW_PTE_ROW1_MSB_ADDR                                                     (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000013c)
#define HWIO_QFPROM_RAW_PTE_ROW1_MSB_OFFS                                                     (SECURITY_CONTROL_CORE_REG_BASE_OFFS + 0x0000013c)
#define HWIO_QFPROM_RAW_PTE_ROW1_MSB_RMSK                                                     0xffffffff
#define HWIO_QFPROM_RAW_PTE_ROW1_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_PTE_ROW1_MSB_ADDR, HWIO_QFPROM_RAW_PTE_ROW1_MSB_RMSK)
#define HWIO_QFPROM_RAW_PTE_ROW1_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_PTE_ROW1_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_PTE_ROW1_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_PTE_ROW1_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_PTE_ROW1_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_PTE_ROW1_MSB_ADDR,m,v,HWIO_QFPROM_RAW_PTE_ROW1_MSB_IN)
#define HWIO_QFPROM_RAW_PTE_ROW1_MSB_PTE_DATA1_BMSK                                           0xffff0000
#define HWIO_QFPROM_RAW_PTE_ROW1_MSB_PTE_DATA1_SHFT                                                 0x10
#define HWIO_QFPROM_RAW_PTE_ROW1_MSB_CHIP_ID_BMSK                                                 0xffff
#define HWIO_QFPROM_RAW_PTE_ROW1_MSB_CHIP_ID_SHFT                                                    0x0

#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x000041e8)
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_OFFS                                                  (SECURITY_CONTROL_CORE_REG_BASE_OFFS + 0x000041e8)
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW0_LSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW0_LSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW0_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_APPS_APC0_LDO_VREF_FUNC_TRIM_BMSK                     0xf8000000
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_APPS_APC0_LDO_VREF_FUNC_TRIM_SHFT                           0x1b
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_APPS_APC0_LDO_VREF_RET_TRIM_BMSK                       0x7c00000
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_APPS_APC0_LDO_VREF_RET_TRIM_SHFT                            0x16
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_GP_HW_CALIB_BMSK                                        0x3c0000
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_GP_HW_CALIB_SHFT                                            0x12
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_Q6SS1_LDO_VREF_TRIM_BMSK                                 0x3fc00
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_Q6SS1_LDO_VREF_TRIM_SHFT                                     0xa
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_Q6SS1_LDO_SVS_ENABLE_BMSK                                  0x200
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_Q6SS1_LDO_SVS_ENABLE_SHFT                                    0x9
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_Q6SS0_LDO_VREF_TRIM_BMSK                                   0x1fe
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_Q6SS0_LDO_VREF_TRIM_SHFT                                     0x1
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_Q6SS0_LDO_SVS_ENABLE_BMSK                                    0x1
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_Q6SS0_LDO_SVS_ENABLE_SHFT                                    0x0

#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x000041ec)
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_OFFS                                                  (SECURITY_CONTROL_CORE_REG_BASE_OFFS + 0x000041ec)
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW0_MSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW0_MSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW0_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_SPARE0_BMSK                                           0xc0000000
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_SPARE0_SHFT                                                 0x1e
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_APPS_APC0_CPU1_IMEAS_BHS_MODE_ACC_TRIM_BMSK           0x3e000000
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_APPS_APC0_CPU1_IMEAS_BHS_MODE_ACC_TRIM_SHFT                 0x19
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_APPS_APC0_CPU0_IMEAS_BHS_MODE_ACC_TRIM_BMSK            0x1f00000
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_APPS_APC0_CPU0_IMEAS_BHS_MODE_ACC_TRIM_SHFT                 0x14
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_APPS_APC1_L2_IMEAS_BHS_MODE_ACC_TRIM_BMSK                0xf8000
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_APPS_APC1_L2_IMEAS_BHS_MODE_ACC_TRIM_SHFT                    0xf
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_APPS_APC0_L2_IMEAS_BHS_MODE_ACC_TRIM_BMSK                 0x7c00
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_APPS_APC0_L2_IMEAS_BHS_MODE_ACC_TRIM_SHFT                    0xa
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_APPS_APC1_LDO_VREF_FUNC_TRIM_BMSK                          0x3e0
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_APPS_APC1_LDO_VREF_FUNC_TRIM_SHFT                            0x5
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_APPS_APC1_LDO_VREF_RET_TRIM_BMSK                            0x1f
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_APPS_APC1_LDO_VREF_RET_TRIM_SHFT                             0x0

#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x000041f0)
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_OFFS                                                  (SECURITY_CONTROL_CORE_REG_BASE_OFFS + 0x000041f0)
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW1_LSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW1_LSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW1_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_SPARE0_BMSK                                           0xc0000000
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_SPARE0_SHFT                                                 0x1e
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_APPS_APC0_CPU1_IMEAS_LDO_MODE_ACC_TRIM_BMSK           0x3e000000
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_APPS_APC0_CPU1_IMEAS_LDO_MODE_ACC_TRIM_SHFT                 0x19
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_APPS_APC0_CPU0_IMEAS_LDO_MODE_ACC_TRIM_BMSK            0x1f00000
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_APPS_APC0_CPU0_IMEAS_LDO_MODE_ACC_TRIM_SHFT                 0x14
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_APPS_APC1_ODCM_DAC_VREF_BMSK                             0xc0000
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_APPS_APC1_ODCM_DAC_VREF_SHFT                                0x12
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_APPS_APC1_MEMCELL_VMIN_VREF_TRIM_BMSK                    0x38000
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_APPS_APC1_MEMCELL_VMIN_VREF_TRIM_SHFT                        0xf
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_APPS_APC0_ODCM_DAC_VREF_BMSK                              0x6000
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_APPS_APC0_ODCM_DAC_VREF_SHFT                                 0xd
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_APPS_APC0_MEMCELL_VMIN_VREF_TRIM_BMSK                     0x1c00
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_APPS_APC0_MEMCELL_VMIN_VREF_TRIM_SHFT                        0xa
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_APPS_APC1_CPU1_IMEAS_BHS_MODE_ACC_TRIM_BMSK                0x3e0
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_APPS_APC1_CPU1_IMEAS_BHS_MODE_ACC_TRIM_SHFT                  0x5
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_APPS_APC1_CPU0_IMEAS_BHS_MODE_ACC_TRIM_BMSK                 0x1f
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_APPS_APC1_CPU0_IMEAS_BHS_MODE_ACC_TRIM_SHFT                  0x0

#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x000041f4)
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_OFFS                                                  (SECURITY_CONTROL_CORE_REG_BASE_OFFS + 0x000041f4)
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW1_MSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW1_MSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW1_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_SPARE0_BMSK                                           0x80000000
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_SPARE0_SHFT                                                 0x1f
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_Q6SS1_LDO_MIN_SVS_ENABLE_BMSK                         0x40000000
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_Q6SS1_LDO_MIN_SVS_ENABLE_SHFT                               0x1e
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_Q6SS1_LDO_LOW_SVS_ENABLE_BMSK                         0x20000000
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_Q6SS1_LDO_LOW_SVS_ENABLE_SHFT                               0x1d
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_Q6SS0_LDO_MIN_SVS_ENABLE_BMSK                         0x10000000
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_Q6SS0_LDO_MIN_SVS_ENABLE_SHFT                               0x1c
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_Q6SS0_LDO_LOW_SVS_ENABLE_BMSK                          0x8000000
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_Q6SS0_LDO_LOW_SVS_ENABLE_SHFT                               0x1b
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_BANDGAP_TRIM_BMSK                                      0x7f00000
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_BANDGAP_TRIM_SHFT                                           0x14
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_APPS_APC1_L2_IMEAS_LDO_MODE_ACC_TRIM_BMSK                0xf8000
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_APPS_APC1_L2_IMEAS_LDO_MODE_ACC_TRIM_SHFT                    0xf
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_APPS_APC0_L2_IMEAS_LDO_MODE_ACC_TRIM_BMSK                 0x7c00
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_APPS_APC0_L2_IMEAS_LDO_MODE_ACC_TRIM_SHFT                    0xa
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_APPS_APC1_CPU1_IMEAS_LDO_MODE_ACC_TRIM_BMSK                0x3e0
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_APPS_APC1_CPU1_IMEAS_LDO_MODE_ACC_TRIM_SHFT                  0x5
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_APPS_APC1_CPU0_IMEAS_LDO_MODE_ACC_TRIM_BMSK                 0x1f
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_APPS_APC1_CPU0_IMEAS_LDO_MODE_ACC_TRIM_SHFT                  0x0

#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x000041f8)
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_OFFS                                                  (SECURITY_CONTROL_CORE_REG_BASE_OFFS + 0x000041f8)
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW2_LSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW2_LSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW2_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_CPR1_TARG_VOLT_SVS_1_0_BMSK                           0xc0000000
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_CPR1_TARG_VOLT_SVS_1_0_SHFT                                 0x1e
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_CPR1_TARG_VOLT_NOM_BMSK                               0x3e000000
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_CPR1_TARG_VOLT_NOM_SHFT                                     0x19
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_CPR1_TARG_VOLT_TUR_BMSK                                0x1f00000
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_CPR1_TARG_VOLT_TUR_SHFT                                     0x14
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_CPR0_TARG_VOLT_SVS2_BMSK                                 0xf8000
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_CPR0_TARG_VOLT_SVS2_SHFT                                     0xf
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_CPR0_TARG_VOLT_SVS_BMSK                                   0x7c00
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_CPR0_TARG_VOLT_SVS_SHFT                                      0xa
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_CPR0_TARG_VOLT_NOM_BMSK                                    0x3e0
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_CPR0_TARG_VOLT_NOM_SHFT                                      0x5
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_CPR0_TARG_VOLT_TUR_BMSK                                     0x1f
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_CPR0_TARG_VOLT_TUR_SHFT                                      0x0

#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x000041fc)
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_OFFS                                                  (SECURITY_CONTROL_CORE_REG_BASE_OFFS + 0x000041fc)
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW2_MSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW2_MSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW2_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_CPR3_TARG_VOLT_TUR_3_0_BMSK                           0xf0000000
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_CPR3_TARG_VOLT_TUR_3_0_SHFT                                 0x1c
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_CPR2_TARG_VOLT_SVS2_BMSK                               0xf800000
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_CPR2_TARG_VOLT_SVS2_SHFT                                    0x17
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_CPR2_TARG_VOLT_SVS_BMSK                                 0x7c0000
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_CPR2_TARG_VOLT_SVS_SHFT                                     0x12
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_CPR2_TARG_VOLT_NOM_BMSK                                  0x3e000
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_CPR2_TARG_VOLT_NOM_SHFT                                      0xd
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_CPR2_TARG_VOLT_TUR_BMSK                                   0x1f00
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_CPR2_TARG_VOLT_TUR_SHFT                                      0x8
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_CPR1_TARG_VOLT_SVS2_BMSK                                    0xf8
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_CPR1_TARG_VOLT_SVS2_SHFT                                     0x3
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_CPR1_TARG_VOLT_SVS_4_2_BMSK                                  0x7
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_CPR1_TARG_VOLT_SVS_4_2_SHFT                                  0x0

/*----------------------------------------------------------------------------
 * MODULE: MSS_MVC
 *--------------------------------------------------------------------------*/

#define MSS_MVC_REG_BASE                                          (MODEM_TOP_BASE      + 0x001b3000)
#define MSS_MVC_REG_BASE_OFFS                                     0x001b3000

#define HWIO_MVC_MEM_SVS_ADDR                                     (MSS_MVC_REG_BASE      + 0x0000001c)
#define HWIO_MVC_MEM_SVS_OFFS                                     (MSS_MVC_REG_BASE_OFFS + 0x0000001c)
#define HWIO_MVC_MEM_SVS_RMSK                                            0x1
#define HWIO_MVC_MEM_SVS_IN          \
        in_dword_masked(HWIO_MVC_MEM_SVS_ADDR, HWIO_MVC_MEM_SVS_RMSK)
#define HWIO_MVC_MEM_SVS_INM(m)      \
        in_dword_masked(HWIO_MVC_MEM_SVS_ADDR, m)
#define HWIO_MVC_MEM_SVS_OUT(v)      \
        out_dword(HWIO_MVC_MEM_SVS_ADDR,v)
#define HWIO_MVC_MEM_SVS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MVC_MEM_SVS_ADDR,m,v,HWIO_MVC_MEM_SVS_IN)
#define HWIO_MVC_MEM_SVS_MEM_SVS_BMSK                                    0x1
#define HWIO_MVC_MEM_SVS_MEM_SVS_SHFT                                    0x0


#endif /* __VCSMSSHWIO_H__ */
