#ifndef VCSMSS_H
#define VCSMSS_H
/*
===========================================================================
*/
/**
  @file VCSMSS.h 
  
  Internal header file for the VCS device driver on the MSS image.
*/
/*  
  ====================================================================

  Copyright (c) 2011-14 QUALCOMM Technologies Incorporated.  All Rights Reserved.  
  QUALCOMM Proprietary and Confidential. 

  ==================================================================== 
  $Header: //components/rel/core.mpss/3.4.c3.11/systemdrivers/vcs/hw/msm8996/mss/src/VCSMSS.h#1 $
  $DateTime: 2016/03/28 23:02:17 $
  $Author: mplcsds1 $

  when       who     what, where, why
  --------   ---     -------------------------------------------------
  01/22/14   lil     Created.

  ====================================================================
*/ 


/*=========================================================================
      Include Files
==========================================================================*/


#include "DDIVCS.h"


/*=========================================================================
      Macro Definitions
==========================================================================*/


/*
 * NPA client names
 */
#define VCS_NPA_CLIENT_NAME_VDD_MSS_DISABLE_SCALING "/vdd/mss/disable_scaling"


/*=========================================================================
      Type Definitions
==========================================================================*/


/**
 * VCS driver image context.
 *
 *  hClientPMIC - NPA client to send VDD_MSS NAS request to RPM.
 *  bNASCleanup - Dirty flag indicating that a NAS request occurred for which we need to clean up.
 */
typedef struct
{
  npa_client_handle      hClientPMIC;
  boolean                bNASCleanup;
} VCSImageCtxtType;


/*=========================================================================
      Functions
==========================================================================*/


#endif /* !VCSMSS_H */

