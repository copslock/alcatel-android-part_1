/*
===========================================================================
*/
/**
  @file VCSMSS.c 
  
  Main entry point for the MSS VCS driver.
*/
/*  
  ====================================================================

  Copyright (c) 2014 QUALCOMM Technologies Incorporated. All Rights Reserved.  
  QUALCOMM Proprietary and Confidential. 

  ==================================================================== 
  $Header: //components/rel/core.mpss/3.4.c3.11/systemdrivers/vcs/hw/msm8996/mss/src/VCSMSS.c#1 $
  $DateTime: 2016/03/28 23:02:17 $
  $Author: mplcsds1 $
 
  when       who     what, where, why
  --------   ---     -------------------------------------------------
  01/22/14   lil     Created.
 
  ====================================================================
*/ 


/*=========================================================================
      Include Files
==========================================================================*/

#include "DALDeviceId.h"
#include "VCSDriver.h"
#include "VCSMSS.h"
#include "VCSSWEVT.h"
#include "VCSMSSHWIO.h"
#include "pmapp_pwr.h"
#include "pmapp_npa.h"
#include <CoreIni.h>
#include "cpr.h"
#include "DDIPlatformInfo.h"
#include <npa_resource.h>

/*=========================================================================
      Macros
==========================================================================*/


/*
 * VCS configuration names in EFS .ini file
 * Shared with ClockDriver
 */
#define VCS_EFS_INI_FILENAME                   "/nv/item_files/clock/settings.ini"

/*
 * EFS Sections for MPSS VCS.
 */
#define VCS_EFS_MPSS_RAIL_CX_CONFIG_SECTION    "MPSS_VDDCX"
#define VCS_EFS_MPSS_RAIL_MSS_CONFIG_SECTION   "MPSS_VDDMSS"

/*
 * EFS Keys for MPSS VCS.
 */
#define VCS_EFS_RAIL_DVS_FLAG                  "EnableDVS"
#define VCS_EFS_RAIL_CPR_FLAG                  "EnableCPR"
#define VCS_EFS_RAIL_MIN_CORNER                "MinCorner"
#define VCS_EFS_RAIL_MAX_CORNER                "MaxCorner"

#define VCS_EFS_RAIL_VOLTAGE_RETENTION_MIN     "RetentionMin"
#define VCS_EFS_RAIL_VOLTAGE_RETENTION_MAX     "RetentionMax"

#define VCS_EFS_RAIL_VOLTAGE_LOW_MINUS_MIN     "LowMinusMin"
#define VCS_EFS_RAIL_VOLTAGE_LOW_MINUS_MAX     "LowMinusMax"

#define VCS_EFS_RAIL_VOLTAGE_LOW_MIN           "LowMin"
#define VCS_EFS_RAIL_VOLTAGE_LOW_MAX           "LowMax"

#define VCS_EFS_RAIL_VOLTAGE_LOW_PLUS_MIN      "LowPlusMin"
#define VCS_EFS_RAIL_VOLTAGE_LOW_PLUS_MAX      "LowPlusMax"

#define VCS_EFS_RAIL_VOLTAGE_NOMINAL_MIN       "NominalMin"
#define VCS_EFS_RAIL_VOLTAGE_NOMINAL_MAX       "NominalMax"

#define VCS_EFS_RAIL_VOLTAGE_NOMINAL_PLUS_MIN  "NominalPlusMin"
#define VCS_EFS_RAIL_VOLTAGE_NOMINAL_PLUS_MAX  "NominalPlusMax"

#define VCS_EFS_RAIL_VOLTAGE_TURBO_MIN         "TurboMin"
#define VCS_EFS_RAIL_VOLTAGE_TURBO_MAX         "TurboMax"

/*
 * Value for indicating that the corner should be disabled.
 * A corner is only disabled if both the min and max are equal to this field.
 */
#define VCS_EFS_CORNER_DISABLED                (-1)

/*
 * Efuse bit for enable/disable LDO at the SVS corner voltage.
 */
#define VCS_EFUSE_LDO_SVS_BMSK                 (1 << 21)

/*
 * Efuse bit for enable/disable LDO at the SVS2 corner voltage.
 */
#define VCS_EFUSE_LDO_SVS2_BMSK                (1 << 1)


/*=========================================================================
      Type Definitions
==========================================================================*/


/*=========================================================================
      Extern Definitions
==========================================================================*/


/*=========================================================================
      Function prototypes
==========================================================================*/


static npa_resource_state VCS_NPAMSSUVDriverFunc(npa_resource *, npa_client_handle, npa_resource_state);


/*=========================================================================
      Data
==========================================================================*/


static VCSImageCtxtType VCS_ImageCtxt;

/*
 * The MSS Rail voltage broadcast node and resource. Broadcasts voltage in
 * microvolt on changes.
 */

typedef struct
{
  npa_resource_definition resource;
  npa_node_definition     node;
} VCS_NPAMSSUVResourceType;


static VCS_NPAMSSUVResourceType VCS_NPAMSSUVResource =
{
  /* Resource */
  {
    VCS_NPA_RESOURCE_VDD_MSS_UV,          /* Name          */
    "uV",                                 /* Units         */
    0,                                    /* Max Value TBD */
    &npa_no_client_plugin,                /* Plugin        */
    NPA_RESOURCE_DEFAULT,                 /* Attributes    */
    NULL,                                 /* User data     */
    NULL,                                 /* Query fn      */
    NULL                                  /* Query lnk fn  */
  },

  /* Node */
  {
    "/node" VCS_NPA_RESOURCE_VDD_MSS_UV,  /* Name           */
    &VCS_NPAMSSUVDriverFunc,              /* Driver fn      */
    NPA_NODE_DEFAULT,                     /* Attributes     */
    NULL,                                 /* User data      */
    0,                                    /* Num Dependency */
    NULL,                                 /* Dependency     */
    1,                                    /* Num resources  */
    &VCS_NPAMSSUVResource.resource        /* Resource       */
  }
};


/* =========================================================================
      Prototypes
==========================================================================*/


/* =========================================================================
      Functions
==========================================================================*/


/* =========================================================================
**  Function : VCS_NPAMSSUVDriverFunc
** =========================================================================*/
/**
  MSS Rail uV resource driver function

  This is a dummy function to satisfy NPA node requirement of a
  driver function for every resource.

  @return
  state.

  @dependencies
  None.
*/
npa_resource_state VCS_NPAMSSUVDriverFunc
(
  npa_resource       *pResource,
  npa_client_handle   hClient,
  npa_resource_state  nState
)
{
  if(hClient->type == NPA_CLIENT_INITIALIZE)
  {
    return nState;
  }
  else
  {
    DALSYS_LogEvent(
      0,
      DALSYS_LOGEVENT_FATAL_ERROR,
      "DALLOG Device VCS: MSS Rail uV broadcast resource operation not supported.");

    return (npa_resource_state)0;
  }
}


/* =========================================================================
**  Function : VCS_DetectLDOEFuse
** =========================================================================*/
/**
  Read LDO Trimmed Info

  This function will read an efuse field to detect whether the LDO has
  been trimmed and is safe to enable the LDO output.

  @return
  None.

  @dependencies
  None.
*/
#if 0
static void VCS_DetectLDOEFuse
(
  VCSLDONodeType *pLDO
)
{

} /* END VCS_DetectLDOEFuse */
#endif


/* =========================================================================
**  Function : VCS_MapCornerToAccMemSel
** =========================================================================*/
/**
  Maps a MSS voltage rail corner to an ACC memory select setting.

  This function maps VDD_MSS corners to ACC settings.

  @param eCorner [in] -- /vdd/mss corner to map.

  @return
  ACC memory select setting.

  @dependencies
  None.
*/
static uint32 VCS_MapCornerToAccMemSel
(
  VCSCornerType eCorner
)
{
  uint32 nACCVal;

  if (eCorner >= VCS_CORNER_NOMINAL)
  {
    nACCVal = 0;
  }
  else
  {
    nACCVal = 1;
  }

  return nACCVal;

} /* END VCS_MapCornerToAccMemSel */


/* =========================================================================
**  Function : VCS_SetRailMode
** =========================================================================*/
/*
  See DDIVCS.h
*/

DALResult VCS_SetRailMode
(
  VCSDrvCtxt      *pDrvCtxt,
  VCSRailType      eRail,
  VCSRailModeType  eMode,
  const void      *pModeData
)
{
  VCSRailNodeType *pRail;
  VCSCornerType    eCorner;
  DALResult        eResult;
  pm_err_flag_type pm_err;

  /*-----------------------------------------------------------------------*/
  /* We only support setting VDD_MSS rail mode.                            */
  /*-----------------------------------------------------------------------*/

  if (eRail != VCS_RAIL_MSS ||
      eMode >= VCS_RAIL_MODE_NUM_OF_MODES)
  {
    return DAL_ERROR_NOT_SUPPORTED;
  }

  /*-----------------------------------------------------------------------*/
  /* Get a pointer to the VDD_MSS rail data node.                          */
  /*-----------------------------------------------------------------------*/

  pRail = pDrvCtxt->apRailMap[VCS_RAIL_MSS];
  if (pRail == NULL)
  {
    return DAL_ERROR_INTERNAL;
  }

  /*-----------------------------------------------------------------------*/
  /* Short-circuit if already at requested mode.                           */
  /*-----------------------------------------------------------------------*/

  if (eMode == pRail->eMode)
  {
    return DAL_SUCCESS;
  }

  /*-----------------------------------------------------------------------*/
  /* Log the change in mode.                                               */
  /*-----------------------------------------------------------------------*/

  VCS_SWEvent(
    VCS_EVENT_RAIL_MODE_CHANGE,
    3,
    pRail->eRail,
    eMode,
    (uint32)pModeData);

  ULOG_RT_PRINTF_3(
    pDrvCtxt->hVCSLog,
    "Rail[%s] changing to mode[%lu] with data[%lu].",
    pRail->pBSPConfig->szName,
    eMode,
    (uint32)pModeData);

  /*-----------------------------------------------------------------------*/
  /* Check for MVC mode.                                                   */
  /*-----------------------------------------------------------------------*/

  if (eMode == VCS_RAIL_MODE_MVC)
  {
    /*
     * Notify CPR that external agent is taking over ownership of rail.
     * NOTE: It's important to notify CPR before PMIC so that CPR can disable
     * interrupts from triggering voltage change requests to PMIC.
     */
    eResult = CPR_RelinquishedControl_Enter(VCS_RAIL_MSS);
    if (eResult != DAL_SUCCESS)
    {
      DALSYS_LogEvent(
        0,
        DALSYS_LOGEVENT_FATAL_ERROR,
        "DALLOG Device VCS: CPR error when setting to externally managed mode .");

      return DAL_ERROR_INTERNAL;
    }

    /*
     * Notify PMIC that external agent is taking over ownership of rail.
     */
    pm_err = pmapp_pwr_vdd_mss_external_manage(TRUE);
    if (pm_err != PM_ERR_FLAG__SUCCESS)
    {
      DALSYS_LogEvent(
        0,
        DALSYS_LOGEVENT_FATAL_ERROR,
        "DALLOG Device VCS: PMIC error when setting to externally managed mode .");

      return DAL_ERROR_INTERNAL;
    }

    pRail->eMode = eMode;
  }

  /*-----------------------------------------------------------------------*/
  /* Check for default mode.                                               */
  /*-----------------------------------------------------------------------*/

  else if (eMode == VCS_RAIL_MODE_DEFAULT)
  {
    /*
     * Verify pModeData contains current rail corner data.
     */
    if (pModeData == NULL)
    {
      DALSYS_LogEvent(
        0,
        DALSYS_LOGEVENT_FATAL_ERROR,
        "DALLOG Device VCS: Missing required current corner argument when switching to default mode.");

      return DAL_ERROR_INVALID_PARAMETER;
    }

    /*
     * Verify corner is supported.
     */
    eCorner = (VCSCornerType)pModeData;
    if (eCorner > pRail->eCornerMax)
    {
      DALSYS_LogEvent(
        0,
        DALSYS_LOGEVENT_FATAL_ERROR,
        "DALLOG Device VCS: Current corner argument above max supported when switching to default mode.");

      return DAL_ERROR_INVALID_PARAMETER;
    }

    /*
     * Notify PMIC that external agent is giving back ownership of rail.
     */
    pm_err = pmapp_pwr_vdd_mss_external_manage(FALSE);
    if (pm_err != PM_ERR_FLAG__SUCCESS)
    {
      DALSYS_LogEvent(
        0,
        DALSYS_LOGEVENT_FATAL_ERROR,
        "DALLOG Device VCS: PMIC error when setting to internally managed mode .");

      return DAL_ERROR_INTERNAL;
    }

    /*
     * Notify CPR that external agent is giving back ownership of rail.
     * NOTE: It's important to notify CPR after PMIC so that PMIC is ready to
     * process voltage change requests that could result immediately after
     * notifying CPR.
     */
    eResult = CPR_RelinquishedControl_Exit(VCS_RAIL_MSS);
    if (eResult != DAL_SUCCESS)
    {
      DALSYS_LogEvent(
        0,
        DALSYS_LOGEVENT_FATAL_ERROR,
        "DALLOG Device VCS: CPR error when setting to internally managed mode .");

      return DAL_ERROR_INTERNAL;
    }

    /*
     * Update mode before triggering impulse on the rail.
     */
    pRail->eMode = eMode;

    /*
     * Update current corner and trigger impulse to sync up w/CPR + PMIC.
     */
    npa_assign_resource_state(
      pRail->resources[VCS_NPA_RAIL_RES_IDX_RAIL].handle,
      eCorner);

    npa_issue_impulse_request(pRail->hClientImpulse);
  }

  /*-----------------------------------------------------------------------*/
  /* Check for unexpected mode.                                            */
  /*-----------------------------------------------------------------------*/

  else
  {
    return DAL_ERROR_INVALID_PARAMETER;
  }

  return DAL_SUCCESS;

} /* END VCS_SetRailMode */


/* =========================================================================
**  Function : VCS_SetRailVoltage
** =========================================================================*/
/*
  See DDIVCS.h
*/

DALResult VCS_SetRailVoltage
(
  VCSDrvCtxt  *pDrvCtxt,
  VCSRailType  eRail,
  uint32       nVoltageUV
)
{
  int                        nLockResult;
  pm_err_flag_type           pm_err;
  VCSRailNodeType           *pRail;
  VCSCornerVoltageRangeType *pVoltageRange;
  npa_resource              *pRailResource;
  VCSNPARailEventDataType    RailEventData;

  /*-----------------------------------------------------------------------*/
  /* We only support setting VDD_MSS rail voltage.                         */
  /*-----------------------------------------------------------------------*/

  if (eRail != VCS_RAIL_MSS)
  {
    return DAL_ERROR_NOT_SUPPORTED;
  }

  /*-----------------------------------------------------------------------*/
  /* Get rail node.                                                        */
  /*-----------------------------------------------------------------------*/

  pRail = pDrvCtxt->apRailMap[eRail];
  if (pRail == NULL || pRail->pVoltageTable == NULL)
  {
    return DAL_ERROR_INTERNAL;
  }

  /*-----------------------------------------------------------------------*/
  /* Protect operation on rail with a critical section.                    */
  /* If the lock is already taken (outstanding voltage corner switch) then */
  /* return immediately.                                                   */
  /*-----------------------------------------------------------------------*/

  pRailResource = pRail->resources[VCS_NPA_RAIL_RES_IDX_RAIL].handle;
  nLockResult = npa_resource_trylock(pRailResource);
  if (nLockResult == -1)
  {
    return DAL_ERROR_BUSY_RESOURCE;
  }

  /*-----------------------------------------------------------------------*/
  /* Get voltage range for the current corner.                             */
  /*-----------------------------------------------------------------------*/

  pVoltageRange = pRail->pVoltageTable->apCornerMap[pRail->eCorner];
  if (pVoltageRange == NULL)
  {
    npa_resource_unlock(pRailResource);
    return DAL_ERROR_INTERNAL;
  }

  /*-----------------------------------------------------------------------*/
  /* Validate the requested voltage.                                       */
  /*-----------------------------------------------------------------------*/

  if (nVoltageUV < pVoltageRange->nMinUV || nVoltageUV > pVoltageRange->nMaxUV)
  {
    npa_resource_unlock(pRailResource);
    return DAL_ERROR_INVALID_PARAMETER;
  }

  /*-----------------------------------------------------------------------*/
  /* Populate voltage change event on uV resource.                         */
  /*-----------------------------------------------------------------------*/

  RailEventData.bIsNAS = FALSE;
  RailEventData.PreChange.eCorner = pRail->eCorner;
  RailEventData.PreChange.nVoltageUV = pRail->nVoltageUV;
  RailEventData.PostChange.eCorner = pRail->eCorner;
  RailEventData.PostChange.nVoltageUV = nVoltageUV;

  /*-----------------------------------------------------------------------*/
  /* Send out pre-switch event on uV resource.                             */
  /*-----------------------------------------------------------------------*/

  npa_dispatch_custom_events(
    VCS_NPAMSSUVResource.resource.handle,
    (npa_event_type)VCS_NPA_RAIL_EVENT_PRE_CHANGE,
    &RailEventData);

  /*-----------------------------------------------------------------------*/
  /* Adjust the rail voltage.                                              */
  /*-----------------------------------------------------------------------*/

  pm_err = pmapp_pwr_set_vdd_mss(PM_ON, nVoltageUV);
  if (pm_err != PM_ERR_FLAG__SUCCESS)
  {
    npa_resource_unlock(pRailResource);

    DALSYS_LogEvent(
      0,
      DALSYS_LOGEVENT_FATAL_ERROR,
      "DALLOG Device VCS: PMIC failed to update VDD_MSS.");

    return DAL_ERROR_INTERNAL;
  }

  pRail->nVoltageUV = nVoltageUV;

  /*-----------------------------------------------------------------------*/
  /* Update the VCS SW Event for MSS Rail voltage and corner               */
  /*-----------------------------------------------------------------------*/

  VCS_SWEvent(
    VCS_EVENT_RAIL_VOLTAGE_SET,
    4,
    pRail->eRail,
    pRail->eCorner,
    pRail->nVoltageUV,
    0);

  /*-----------------------------------------------------------------------*/
  /* Update the MSS broadcast rail voltage resource.                       */
  /*-----------------------------------------------------------------------*/

  npa_assign_resource_state(
    VCS_NPAMSSUVResource.resource.handle,
    pRail->nVoltageUV);

  /*-----------------------------------------------------------------------*/
  /* Send out post-switch event on uV resource.                            */
  /*-----------------------------------------------------------------------*/

  npa_dispatch_custom_events(
    VCS_NPAMSSUVResource.resource.handle,
    (npa_event_type)VCS_NPA_RAIL_EVENT_POST_CHANGE,
    &RailEventData);

  /*-----------------------------------------------------------------------*/
  /* Release lock.                                                         */
  /*-----------------------------------------------------------------------*/

  npa_resource_unlock(pRailResource);

  return DAL_SUCCESS;

} /* END VCS_SetRailVoltage */


/* =========================================================================
**  Function : VCS_SetVDDMSSCornerCPR
** =========================================================================*/
/**
  Set VDD_MSS rail to a requested corner in CPR mode.

  @param pRail [in]   -- Pointer to the rail node.
  @param eCorner [in] -- Requested corner.
  @param bIsNASRequest[in] -- Whether the request is for the next active set.

  @return
  DAL_SUCCESS -- Sucessfully configured VDD_MSS to requested corner.
  DAL_ERROR -- Failed to configure VDD_MSS to requested corner.

  @dependencies
  None.
*/

static DALResult VCS_SetVDDMSSCornerCPR
(
  VCSRailNodeType *pRail,
  VCSCornerType    eCorner,
  boolean          bIsNASRequest
)
{
  DALResult                  eResult;
  pm_err_flag_type           pm_err;
  uint32                     nVoltageUV;
  VCSCornerVoltageRangeType *pVoltageRange;
  pm_on_off_type             pm_on_off;
  VCSDrvCtxt                *pDrvCtxt;
  VCSImageCtxtType          *pImageCtxt;
  VCSNPARailEventDataType    RailEventData;

  /*-----------------------------------------------------------------------*/
  /* Sanity.                                                               */
  /*-----------------------------------------------------------------------*/

  if (pRail == NULL || eCorner >= VCS_CORNER_NUM_OF_CORNERS)
  {
    return DAL_ERROR_INVALID_PARAMETER;
  }

  /*-----------------------------------------------------------------------*/
  /* Get the driver context.                                               */
  /*-----------------------------------------------------------------------*/

  pDrvCtxt = VCS_GetDrvCtxt();
  pImageCtxt = (VCSImageCtxtType *)pDrvCtxt->pImageCtxt;

  /*-----------------------------------------------------------------------*/
  /* Disable CPR before changing the voltage.                              */
  /*-----------------------------------------------------------------------*/

  if (!pRail->nDisableCPR)
  {
    /*
     * Log the CPR disable event.
     */
    ULOG_RT_PRINTF_1(
      pDrvCtxt->hVCSLog,
      "Rail[%s] Disabling CPR.",
      pRail->pBSPConfig->szName);

    VCS_SWEvent(VCS_EVENT_CPR_STATE_REQUEST, 2, pRail->eRail, 0);

    eResult = CPR_Disable(pRail->eRail);
    if (eResult != DAL_SUCCESS)
    {
      DALSYS_LogEvent(
        0,
        DALSYS_LOGEVENT_FATAL_ERROR,
        "DALLOG Device VCS: CPR returned error[%lu] when called to disable.",
        eResult);

      return DAL_ERROR_INTERNAL;
    }
  }

  /*-----------------------------------------------------------------------*/
  /* Gate voltage for the OFF corner case.                                 */
  /*-----------------------------------------------------------------------*/

  if (eCorner == VCS_CORNER_OFF)
  {
    /*
     * Flag indicating whether we want the rail off.
     * For NAS requests we leave the rail on for RPM to disable.
     */
    pm_on_off = bIsNASRequest ? PM_ON : PM_OFF;

    /*
     * PMIC gates the voltage output for the off case.
     * VCS maintains the outstanding voltage setting.
     */
    nVoltageUV = pRail->nVoltageUV;
  }

  /*-----------------------------------------------------------------------*/
  /* Get the voltage recommendation from CPR if not turning off the rail.  */
  /*-----------------------------------------------------------------------*/

  else
  {
    /*
     * Flag indicating whether we want the rail on.
     * For NAS requests we leave the rail off for RPM to enable.
     */
    pm_on_off = bIsNASRequest ? PM_OFF : PM_ON;

    /*
     * Init the voltage to the max voltage at the specified corner.
     */
    pVoltageRange = pRail->pVoltageTable->apCornerMap[eCorner];
    nVoltageUV = pVoltageRange->nMaxUV;

    if(!pRail->nDisableCPR)
    {
      eResult =
        CPR_GetRailVoltageRecommendation(
          pRail->eRail,
          eCorner,
          &nVoltageUV);
      if (eResult != DAL_SUCCESS)
      {
        DALSYS_LogEvent(
          0,
          DALSYS_LOGEVENT_FATAL_ERROR,
          "DALLOG Device VCS: CPR returned error[%lu] when called for voltage recommendation.",
          eResult);

        return DAL_ERROR_INTERNAL;
      }
    }

    /*
     * Bound the CPR requested voltage if necessary.
     */
    if (nVoltageUV < pVoltageRange->nMinUV)
    {
      nVoltageUV = pVoltageRange->nMinUV;
    }
    else if (nVoltageUV > pVoltageRange->nMaxUV)
    {
      nVoltageUV = pVoltageRange->nMaxUV;
    }
  }

  /*-----------------------------------------------------------------------*/
  /* Populate voltage change event on uV resource.                         */
  /*-----------------------------------------------------------------------*/

  RailEventData.bIsNAS = bIsNASRequest;
  RailEventData.PreChange.eCorner = pRail->eCorner;
  RailEventData.PreChange.nVoltageUV = pRail->nVoltageUV;
  RailEventData.PostChange.eCorner = eCorner;
  RailEventData.PostChange.nVoltageUV = nVoltageUV;

  /*-----------------------------------------------------------------------*/
  /* Send out pre-switch event on uV resource.                             */
  /*-----------------------------------------------------------------------*/

  npa_dispatch_custom_events(
    VCS_NPAMSSUVResource.resource.handle,
    (npa_event_type)VCS_NPA_RAIL_EVENT_PRE_CHANGE,
    &RailEventData);

  /*-----------------------------------------------------------------------*/
  /* Adjust the rail voltage.                                              */
  /*-----------------------------------------------------------------------*/

  ULOG_RT_PRINTF_3(
    pDrvCtxt->hVCSLog,
    "Rail[%s] PMIC[%s] Voltage[%lu].",
    pRail->pBSPConfig->szName,
    pm_on_off == PM_ON ? "ON" : "OFF",
    nVoltageUV);

  if (DalPlatformInfo_Platform() != DALPLATFORMINFO_TYPE_RUMI)
  {
    pm_err = pmapp_pwr_set_vdd_mss(pm_on_off, nVoltageUV);
    if (pm_err != PM_ERR_FLAG__SUCCESS)
    {
      DALSYS_LogEvent(
        0,
        DALSYS_LOGEVENT_FATAL_ERROR,
        "DALLOG Device VCS: PMIC API returned error[%lu] for voltage[%lu]",
        pm_err,
        nVoltageUV);

      return DAL_ERROR_INTERNAL;
    }

    /*
     * Log the rail voltage SW Event
     */
    VCS_SWEvent(
      VCS_EVENT_RAIL_VOLTAGE_SET,
      4,
      pRail->eRail,
      pRail->eCorner,
      nVoltageUV,
      bIsNASRequest);
  }

  pRail->nVoltageUV = nVoltageUV;

  /*-----------------------------------------------------------------------*/
  /* Update the MSS broadcast rail voltage resource                        */
  /*-----------------------------------------------------------------------*/

  npa_assign_resource_state(
    VCS_NPAMSSUVResource.resource.handle,
    pRail->nVoltageUV);

  /*-----------------------------------------------------------------------*/
  /* Send out post-switch event on uV resource.                            */
  /* Only if not a NAS request.                                            */
  /*-----------------------------------------------------------------------*/

  if (bIsNASRequest != TRUE)
  {
    npa_dispatch_custom_events(
      VCS_NPAMSSUVResource.resource.handle,
      (npa_event_type)VCS_NPA_RAIL_EVENT_POST_CHANGE,
      &RailEventData);
  }

  /*-----------------------------------------------------------------------*/
  /* Enable CPR in the absence of a current or NAS disable request.        */
  /*-----------------------------------------------------------------------*/

  if (!pRail->nDisableCPR &&
      eCorner != VCS_CORNER_OFF &&
      !bIsNASRequest)
  {
    /*
     * Log the CPR enable event.
     */
    ULOG_RT_PRINTF_1(
      pDrvCtxt->hVCSLog,
      "Rail[%s] Enabling CPR.",
      pRail->pBSPConfig->szName);

    VCS_SWEvent(VCS_EVENT_CPR_STATE_REQUEST, 2, pRail->eRail, 1);

    eResult = CPR_Enable(pRail->eRail);
    if (eResult != DAL_SUCCESS)
    {
      DALSYS_LogEvent(
        0,
        DALSYS_LOGEVENT_FATAL_ERROR,
        "DALLOG Device VCS: CPR returned error[%lu] when called to enable.",
        eResult);

      return DAL_ERROR_INTERNAL;
    }
  }

  /*-----------------------------------------------------------------------*/
  /* Schedule the VDD_MSS NAS request with RPM.                            */
  /*-----------------------------------------------------------------------*/

  if (bIsNASRequest)
  {
    npa_set_request_attribute(pImageCtxt->hClientPMIC, NPA_REQUEST_NEXT_AWAKE);
    npa_issue_scalar_request(pImageCtxt->hClientPMIC, 1);

    /*
     * Set the NAS dirty flag to indicate that we need to clean it up later.
     */
    pImageCtxt->bNASCleanup = TRUE;
  }
  else
  {
    /*
     * If this is the first post-NAS request for OFF then we need to remove
     * the vote to PMIC RPM so that we are back in sync.
     */
    if (eCorner == VCS_CORNER_OFF &&
        pImageCtxt->bNASCleanup)
    {
      npa_set_request_attribute(pImageCtxt->hClientPMIC, NPA_REQUEST_NEXT_AWAKE);
      npa_issue_scalar_request(pImageCtxt->hClientPMIC, 0);
      pImageCtxt->bNASCleanup = FALSE;
    }
  }

  return DAL_SUCCESS;

} /* END VCS_SetVDDMSSCornerCPR */


/* =========================================================================
**  Function : VCS_SetVDDMSSCorner
** =========================================================================*/
/**
  Set VDD_MSS rail to a requested corner.

  @param pRail [in] -- Pointer to the rail node.
  @param eCorner [in] -- Requested corner.
  @param bIsNASRequest[in] -- Whether the request is for the next active set.

  @return
  DAL_SUCCESS -- Sucessfully configured VDD_MSS to requested corner.
  DAL_ERROR -- Failed to configure VDD_MSS to requested corner.

  @dependencies
  None.
*/

static DALResult VCS_SetVDDMSSCorner
(
  VCSRailNodeType *pRail,
  VCSCornerType    eCorner,
  boolean          bIsNASRequest
)
{
  DALResult eResult = DAL_ERROR;

  /*-----------------------------------------------------------------------*/
  /* Sanity.                                                               */
  /*-----------------------------------------------------------------------*/

  if (pRail == NULL || eCorner >= VCS_CORNER_NUM_OF_CORNERS)
  {
    return DAL_ERROR_INVALID_PARAMETER;
  }

  /*-----------------------------------------------------------------------*/
  /* Pre-voltage update.                                                   */
  /*-----------------------------------------------------------------------*/

  if (eCorner < pRail->eCorner)
  {
    /*
     * ACC settings from high to low.
     */
    HWIO_OUTF(MVC_MEM_SVS, MEM_SVS, VCS_MapCornerToAccMemSel(eCorner));
  }

  /*-----------------------------------------------------------------------*/
  /* Update the voltage corner.                                            */
  /*-----------------------------------------------------------------------*/

  eResult = VCS_SetVDDMSSCornerCPR(pRail, eCorner, bIsNASRequest);
  if (eResult != DAL_SUCCESS)
  {
    DALSYS_LogEvent(
      0,
      DALSYS_LOGEVENT_FATAL_ERROR,
      "DALLOG Device VCS: Failed to set rail to requested voltage.");

    return eResult;
  }

  /*-----------------------------------------------------------------------*/
  /* Post-voltage update.                                                  */
  /*-----------------------------------------------------------------------*/

  if (eCorner > pRail->eCorner)
  {
    /*
     * ACC settings from high to low.
     */
    HWIO_OUTF(MVC_MEM_SVS, MEM_SVS, VCS_MapCornerToAccMemSel(eCorner));
  }

  return DAL_SUCCESS;

} /* END VCS_SetVDDMSSCorner */


/* =========================================================================
**  Function : VCS_LoadNV_CX
** =========================================================================*/
/**
  Load EFS data for CX rail.

  @param pDrvCtxt [in] -- Pointer to the VCS driver context.
  @param hConfig [in] -- Handle to core config.

  @return
  DAL_SUCCESS -- Successfully parsed EFS data for CX.
  DAL_ERROR -- Failed to parse EFS data for CX.

  @dependencies
  None.
*/

static DALResult VCS_LoadNV_CX
(
  VCSDrvCtxt       *pDrvCtxt,
  CoreConfigHandle  hConfig
)
{
  VCSRailNodeType         *pRail;
  uint32                   nReadResult, nData;
  VCSNPARailEventDataType  RailEventData;
  npa_resource            *pRailResource;

  /*-----------------------------------------------------------------------*/
  /* Get rail nodes.                                                       */
  /*-----------------------------------------------------------------------*/

  pRail = pDrvCtxt->apRailMap[VCS_RAIL_CX];
  if (pRail == NULL)
  {
    DALSYS_LogEvent(
      0,
      DALSYS_LOGEVENT_FATAL_ERROR,
      "DALLOG Device VCS: Invalid CX rail structure");

    return DAL_ERROR_INTERNAL;
  }

  /*-----------------------------------------------------------------------*/
  /* BEGIN CRITICAL SECTION: EFS data update.                              */
  /*-----------------------------------------------------------------------*/

  pRailResource = pRail->resources[VCS_NPA_RAIL_RES_IDX_RAIL].handle;
  npa_resource_lock(pRailResource);

  /*-----------------------------------------------------------------------*/
  /* Update max corner.                                                    */
  /*-----------------------------------------------------------------------*/

  nReadResult =
    CoreConfig_ReadUint32(
      hConfig,
      VCS_EFS_MPSS_RAIL_CX_CONFIG_SECTION,
      VCS_EFS_RAIL_MAX_CORNER,
      (unsigned int *)&nData);
  if (nReadResult == CORE_CONFIG_SUCCESS)
  {
    pRail->eCornerMax = MIN(nData, VCS_CORNER_MAX);
  }

  /*-----------------------------------------------------------------------*/
  /* Update min corner.                                                    */
  /*-----------------------------------------------------------------------*/

  nReadResult =
    CoreConfig_ReadUint32(
      hConfig,
      VCS_EFS_MPSS_RAIL_CX_CONFIG_SECTION,
      VCS_EFS_RAIL_MIN_CORNER,
      (unsigned int *)&nData);
  if (nReadResult == CORE_CONFIG_SUCCESS)
  {
    pRail->eCornerMin = MIN(nData, pRail->eCornerMax);
  }

  /*-----------------------------------------------------------------------*/
  /* Update DVS.                                                           */
  /*-----------------------------------------------------------------------*/

  nReadResult =
    CoreConfig_ReadUint32(
      hConfig,
      VCS_EFS_MPSS_RAIL_CX_CONFIG_SECTION,
      VCS_EFS_RAIL_DVS_FLAG,
      (unsigned int *)&nData);
  if (nReadResult == CORE_CONFIG_SUCCESS)
  {
    if (nData == FALSE)
    {
      pRail->nDisableDVS |= VCS_FLAG_DISABLED_BY_EFS;
    }
    else
    {
      pRail->nDisableDVS = 0;
    }
  }

  /*-----------------------------------------------------------------------*/
  /* END CRITICAL SECTION: EFS data update.                                */
  /*-----------------------------------------------------------------------*/

  npa_resource_unlock(pRailResource);

  /*-----------------------------------------------------------------------*/
  /* Notify clients of the new max limit for this rail.                    */
  /*-----------------------------------------------------------------------*/

  RailEventData.PreChange.eCorner  = VCS_CORNER_OFF;
  RailEventData.PostChange.eCorner = pRail->eCornerMax;

  npa_dispatch_custom_events(
    pRailResource,
    (npa_event_type)VCS_NPA_RAIL_EVENT_LIMIT_MAX,
    &RailEventData);

  /*-----------------------------------------------------------------------*/
  /* BEGIN CRITICAL SECTION: limit max update.                             */
  /*-----------------------------------------------------------------------*/

  npa_resource_lock(pRailResource);

  /*-----------------------------------------------------------------------*/
  /* Verify votes reduced within supported range.                          */
  /*-----------------------------------------------------------------------*/

  if (pRailResource->request_state > pRail->eCornerMax)
  {
    DALSYS_LogEvent(
      0,
      DALSYS_LOGEVENT_FATAL_ERROR,
      "DALLOG Device VCS: request[%l] on rail[%s] above max[%l]",
      pRailResource->request_state,
      pRail->pBSPConfig->szName,
      pRail->eCornerMax);
  }

  /*-----------------------------------------------------------------------*/
  /* Trigger impulse to update the rail active state.                      */
  /* NOTE: We update the resoure state to 0 in order for the impulse       */
  /*       request to make it's way into the driver function.              */
  /*       This workaround will be replaced by a new NPA API.              */
  /*-----------------------------------------------------------------------*/

  npa_assign_resource_state(pRailResource, 0);
  npa_issue_impulse_request(pRail->hClientImpulse);

  /*-----------------------------------------------------------------------*/
  /* END CRITICAL SECTION: limit max update.                               */
  /*-----------------------------------------------------------------------*/

  npa_resource_unlock(pRailResource);

  return DAL_SUCCESS;

} /* END of VCS_LoadNV_CX */


/* =========================================================================
**  Function : VCS_LoadNV_MSS
** =========================================================================*/
/**
  Load EFS data for MSS rail.

  @param pDrvCtxt [in] -- Pointer to the VCS driver context.
  @param hConfig [in] -- Handle to core config.

  @return
  DAL_SUCCESS -- Successfully parsed EFS data for MSS.
  DAL_ERROR -- Failed to parse EFS data for MSS.

  @dependencies
  None.
*/

static DALResult VCS_LoadNV_MSS
(
  VCSDrvCtxt       *pDrvCtxt,
  CoreConfigHandle  hConfig
)
{
  VCSRailNodeType           *pRail;
  VCSCornerVoltageRangeType *pCornerRange;
  uint32                     nReadResult, nData;
  VCSNPARailEventDataType    RailEventData;
  npa_resource              *pRailResource;

  /*-----------------------------------------------------------------------*/
  /* Get rail nodes.                                                       */
  /*-----------------------------------------------------------------------*/

  pRail = pDrvCtxt->apRailMap[VCS_RAIL_MSS];
  if (pRail == NULL || pRail->pVoltageTable == NULL)
  {
    DALSYS_LogEvent(
      0,
      DALSYS_LOGEVENT_FATAL_ERROR,
      "DALLOG Device VCS: Invalid MSS rail structures");

    return DAL_ERROR_INTERNAL;
  }

  /*-----------------------------------------------------------------------*/
  /* BEGIN CRITICAL SECTION: EFS data update.                              */
  /*-----------------------------------------------------------------------*/

  pRailResource = pRail->resources[VCS_NPA_RAIL_RES_IDX_RAIL].handle;
  npa_resource_lock(pRailResource);

  /*-----------------------------------------------------------------------*/
  /* Update CPR.                                                           */
  /*-----------------------------------------------------------------------*/

  nReadResult =
    CoreConfig_ReadUint32(
      hConfig,
      VCS_EFS_MPSS_RAIL_MSS_CONFIG_SECTION,
      VCS_EFS_RAIL_CPR_FLAG,
      (unsigned int *)&nData);
  if (nReadResult == CORE_CONFIG_SUCCESS)
  {
    if (nData == FALSE)
    {
      pRail->nDisableCPR |= VCS_FLAG_DISABLED_BY_EFS;
    }
    else
    {
      pRail->nDisableCPR = 0;
    }
  }

  /*-----------------------------------------------------------------------*/
  /* Update max corner.                                                    */
  /*-----------------------------------------------------------------------*/

  nReadResult =
    CoreConfig_ReadUint32(
      hConfig,
      VCS_EFS_MPSS_RAIL_MSS_CONFIG_SECTION,
      VCS_EFS_RAIL_MAX_CORNER,
      (unsigned int *)&nData);
  if (nReadResult == CORE_CONFIG_SUCCESS)
  {
    pRail->eCornerMax = MIN(nData, VCS_CORNER_MAX);
  }

  /*-----------------------------------------------------------------------*/
  /* Update min corner.                                                    */
  /*-----------------------------------------------------------------------*/

  nReadResult =
    CoreConfig_ReadUint32(
      hConfig,
      VCS_EFS_MPSS_RAIL_MSS_CONFIG_SECTION,
      VCS_EFS_RAIL_MIN_CORNER,
      (unsigned int *)&nData);
  if (nReadResult == CORE_CONFIG_SUCCESS)
  {
    pRail->eCornerMin = MIN(nData, pRail->eCornerMax);
  }

  /*-----------------------------------------------------------------------*/
  /* Update DVS.                                                           */
  /*-----------------------------------------------------------------------*/

  nReadResult =
    CoreConfig_ReadUint32(
      hConfig,
      VCS_EFS_MPSS_RAIL_MSS_CONFIG_SECTION,
      VCS_EFS_RAIL_DVS_FLAG,
      (unsigned int *)&nData);
  if (nReadResult == CORE_CONFIG_SUCCESS)
  {
    if (nData == FALSE)
    {
      pRail->nDisableDVS |= VCS_FLAG_DISABLED_BY_EFS;
    }
    else
    {
      pRail->nDisableDVS = 0;
    }
  }

  /*-----------------------------------------------------------------------*/
  /* Update corner: LOW_MINUS                                              */
  /*-----------------------------------------------------------------------*/

  pCornerRange = pRail->pVoltageTable->apCornerMap[VCS_CORNER_LOW_MINUS];
  if (pCornerRange != NULL)
  {
    nReadResult =
      CoreConfig_ReadUint32(
        hConfig,
        VCS_EFS_MPSS_RAIL_MSS_CONFIG_SECTION,
        VCS_EFS_RAIL_VOLTAGE_LOW_MINUS_MIN,
        (unsigned int *)&nData);
    if (nReadResult == CORE_CONFIG_SUCCESS)
    {
      pCornerRange->nMinUV = nData;
    }

    nReadResult =
      CoreConfig_ReadUint32(
        hConfig,
        VCS_EFS_MPSS_RAIL_MSS_CONFIG_SECTION,
        VCS_EFS_RAIL_VOLTAGE_LOW_MINUS_MAX,
        (unsigned int *)&nData);
    if (nReadResult == CORE_CONFIG_SUCCESS)
    {
      pCornerRange->nMaxUV = nData;
    }

    /*
     * Disable corner if min/max both are both set to disabled.
     */
    if (pCornerRange->nMinUV == VCS_EFS_CORNER_DISABLED &&
        pCornerRange->nMaxUV == VCS_EFS_CORNER_DISABLED)
    {
      pRail->pVoltageTable->apCornerMap[VCS_CORNER_LOW_MINUS] = NULL;
    }
  }

  /*-----------------------------------------------------------------------*/
  /* Update corner: LOW                                                    */
  /*-----------------------------------------------------------------------*/

  pCornerRange = pRail->pVoltageTable->apCornerMap[VCS_CORNER_LOW];
  if (pCornerRange != NULL)
  {
    nReadResult =
      CoreConfig_ReadUint32(
        hConfig,
        VCS_EFS_MPSS_RAIL_MSS_CONFIG_SECTION,
        VCS_EFS_RAIL_VOLTAGE_LOW_MIN,
        (unsigned int *)&nData);
    if (nReadResult == CORE_CONFIG_SUCCESS)
    {
      pCornerRange->nMinUV = nData;
    }

    nReadResult =
      CoreConfig_ReadUint32(
        hConfig,
        VCS_EFS_MPSS_RAIL_MSS_CONFIG_SECTION,
        VCS_EFS_RAIL_VOLTAGE_LOW_MAX,
        (unsigned int *)&nData);
    if (nReadResult == CORE_CONFIG_SUCCESS)
    {
      pCornerRange->nMaxUV = nData;
    }

    /*
     * Disable corner if min/max both are both set to disabled.
     */
    if (pCornerRange->nMinUV == VCS_EFS_CORNER_DISABLED &&
        pCornerRange->nMaxUV == VCS_EFS_CORNER_DISABLED)
    {
      pRail->pVoltageTable->apCornerMap[VCS_CORNER_LOW] = NULL;
    }
  }

  /*-----------------------------------------------------------------------*/
  /* Update corner: LOW_PLUS                                               */
  /*-----------------------------------------------------------------------*/

  pCornerRange = pRail->pVoltageTable->apCornerMap[VCS_CORNER_LOW_PLUS];
  if (pCornerRange != NULL)
  {
    nReadResult =
      CoreConfig_ReadUint32(
        hConfig,
        VCS_EFS_MPSS_RAIL_MSS_CONFIG_SECTION,
        VCS_EFS_RAIL_VOLTAGE_LOW_PLUS_MIN,
        (unsigned int *)&nData);
    if (nReadResult == CORE_CONFIG_SUCCESS)
    {
      pCornerRange->nMinUV = nData;
    }

  nReadResult =
    CoreConfig_ReadUint32(
      hConfig,
      VCS_EFS_MPSS_RAIL_MSS_CONFIG_SECTION,
        VCS_EFS_RAIL_VOLTAGE_LOW_PLUS_MAX,
        (unsigned int *)&nData);
    if (nReadResult == CORE_CONFIG_SUCCESS)
    {
      pCornerRange->nMaxUV = nData;
    }

    /*
     * Disable corner if min/max both are both set to disabled.
     */
    if (pCornerRange->nMinUV == VCS_EFS_CORNER_DISABLED &&
        pCornerRange->nMaxUV == VCS_EFS_CORNER_DISABLED)
    {
      pRail->pVoltageTable->apCornerMap[VCS_CORNER_LOW_PLUS] = NULL;
    }
  }

  /*-----------------------------------------------------------------------*/
  /* Update corner: NOMINAL                                                */
  /*-----------------------------------------------------------------------*/

  pCornerRange = pRail->pVoltageTable->apCornerMap[VCS_CORNER_NOMINAL];
  if (pCornerRange != NULL)
  {
    nReadResult =
      CoreConfig_ReadUint32(
        hConfig,
        VCS_EFS_MPSS_RAIL_MSS_CONFIG_SECTION,
        VCS_EFS_RAIL_VOLTAGE_NOMINAL_MIN,
        (unsigned int *)&nData);
    if (nReadResult == CORE_CONFIG_SUCCESS)
    {
      pCornerRange->nMinUV = nData;
    }

    nReadResult =
      CoreConfig_ReadUint32(
        hConfig,
        VCS_EFS_MPSS_RAIL_MSS_CONFIG_SECTION,
        VCS_EFS_RAIL_VOLTAGE_NOMINAL_MAX,
        (unsigned int *)&nData);
    if (nReadResult == CORE_CONFIG_SUCCESS)
    {
      pCornerRange->nMaxUV = nData;
    }

    /*
     * Disable corner if min/max both are both set to disabled.
     */
    if (pCornerRange->nMinUV == VCS_EFS_CORNER_DISABLED &&
        pCornerRange->nMaxUV == VCS_EFS_CORNER_DISABLED)
    {
      pRail->pVoltageTable->apCornerMap[VCS_CORNER_NOMINAL] = NULL;
    }
  }

  /*-----------------------------------------------------------------------*/
  /* Update corner: NOMINAL_PLUS                                           */
  /*-----------------------------------------------------------------------*/

  pCornerRange = pRail->pVoltageTable->apCornerMap[VCS_CORNER_NOMINAL_PLUS];
  if (pCornerRange != NULL)
  {
    nReadResult =
      CoreConfig_ReadUint32(
        hConfig,
        VCS_EFS_MPSS_RAIL_MSS_CONFIG_SECTION,
        VCS_EFS_RAIL_VOLTAGE_NOMINAL_PLUS_MIN,
        (unsigned int *)&nData);
    if (nReadResult == CORE_CONFIG_SUCCESS)
    {
      pCornerRange->nMinUV = nData;
    }

  nReadResult =
    CoreConfig_ReadUint32(
      hConfig,
      VCS_EFS_MPSS_RAIL_MSS_CONFIG_SECTION,
        VCS_EFS_RAIL_VOLTAGE_NOMINAL_PLUS_MAX,
        (unsigned int *)&nData);
    if (nReadResult == CORE_CONFIG_SUCCESS)
    {
      pCornerRange->nMaxUV = nData;
    }

    /*
     * Disable corner if min/max both are both set to disabled.
     */
    if (pCornerRange->nMinUV == VCS_EFS_CORNER_DISABLED &&
        pCornerRange->nMaxUV == VCS_EFS_CORNER_DISABLED)
    {
      pRail->pVoltageTable->apCornerMap[VCS_CORNER_NOMINAL_PLUS] = NULL;
    }
  }

  /*-----------------------------------------------------------------------*/
  /* Update corner: TURBO                                                  */
  /*-----------------------------------------------------------------------*/

  pCornerRange = pRail->pVoltageTable->apCornerMap[VCS_CORNER_TURBO];
  if (pCornerRange != NULL)
  {
    nReadResult =
      CoreConfig_ReadUint32(
        hConfig,
        VCS_EFS_MPSS_RAIL_MSS_CONFIG_SECTION,
        VCS_EFS_RAIL_VOLTAGE_TURBO_MIN,
        (unsigned int *)&nData);
    if (nReadResult == CORE_CONFIG_SUCCESS)
    {
      pCornerRange->nMinUV = nData;
    }

    nReadResult =
      CoreConfig_ReadUint32(
        hConfig,
        VCS_EFS_MPSS_RAIL_MSS_CONFIG_SECTION,
        VCS_EFS_RAIL_VOLTAGE_TURBO_MAX,
        (unsigned int *)&nData);
    if (nReadResult == CORE_CONFIG_SUCCESS)
    {
      pCornerRange->nMaxUV = nData;
    }

    /*
     * Disable corner if min/max both are both set to disabled.
     */
    if (pCornerRange->nMinUV == VCS_EFS_CORNER_DISABLED &&
        pCornerRange->nMaxUV == VCS_EFS_CORNER_DISABLED)
    {
      pRail->pVoltageTable->apCornerMap[VCS_CORNER_TURBO] = NULL;
    }
  }

  /*-----------------------------------------------------------------------*/
  /* END CRITICAL SECTION: EFS data update.                                */
  /*-----------------------------------------------------------------------*/

  npa_resource_unlock(pRailResource);

  /*-----------------------------------------------------------------------*/
  /* Notify clients of the new max limit for this rail.                    */
  /*-----------------------------------------------------------------------*/

  RailEventData.PreChange.eCorner  = VCS_CORNER_OFF;
  RailEventData.PostChange.eCorner = pRail->eCornerMax;

  npa_dispatch_custom_events(
    pRailResource,
    (npa_event_type)VCS_NPA_RAIL_EVENT_LIMIT_MAX,
    &RailEventData);

  /*-----------------------------------------------------------------------*/
  /* BEGIN CRITICAL SECTION: limit max update.                             */
  /*-----------------------------------------------------------------------*/

  npa_resource_lock(pRailResource);

  /*-----------------------------------------------------------------------*/
  /* Verify votes reduced within supported range.                          */
  /*-----------------------------------------------------------------------*/

  if (pRailResource->request_state > pRail->eCornerMax)
  {
    DALSYS_LogEvent(
      0,
      DALSYS_LOGEVENT_FATAL_ERROR,
      "DALLOG Device VCS: request[%l] on rail[%s] above max[%l]",
      pRailResource->request_state,
      pRail->pBSPConfig->szName,
      pRail->eCornerMax);

    return DAL_ERROR_NOT_ALLOWED;
  }

  /*-----------------------------------------------------------------------*/
  /* Trigger impulse to update the rail active state.                      */
  /* NOTE: We update the resoure state to 0 in order for the impulse       */
  /*       request to make it's way into the driver function.              */
  /*       This workaround will be replaced by a new NPA API.              */
  /*-----------------------------------------------------------------------*/

  npa_assign_resource_state(pRailResource, 0);
  npa_issue_impulse_request(pRail->hClientImpulse);

  /*-----------------------------------------------------------------------*/
  /* END CRITICAL SECTION: limit max update.                               */
  /*-----------------------------------------------------------------------*/

  npa_resource_unlock(pRailResource);

  return DAL_SUCCESS;

} /* END of VCS_LoadNV_MSS */


/* =========================================================================
**  Function : VCS_LoadNV
** =========================================================================*/
/*
  See VCSMSS.h
*/

void VCS_LoadNV
(
  void
)
{
  VCSDrvCtxt       *pDrvCtxt;
  CoreConfigHandle  hConfig;
  DALResult         eResult;

  pDrvCtxt = VCS_GetDrvCtxt();

  /*-----------------------------------------------------------------------*/
  /* Read clock configuration file.                                        */
  /*-----------------------------------------------------------------------*/

  hConfig = CoreIni_ConfigCreate(VCS_EFS_INI_FILENAME);
  if (hConfig == NULL)
  {
    DALSYS_LogEvent(
      0,
      DALSYS_LOGEVENT_INFO,
      "DALLOG Device VCS: Unable to read EFS file: %s",
      VCS_EFS_INI_FILENAME);

    return;
  }

  /*-----------------------------------------------------------------------*/
  /* Load the EFS data for CX.                                             */
  /*-----------------------------------------------------------------------*/

  eResult = VCS_LoadNV_CX(pDrvCtxt, hConfig);
  if (eResult != DAL_SUCCESS)
  {
    DALSYS_LogEvent(
      0,
      DALSYS_LOGEVENT_FATAL_ERROR,
      "DALLOG Device VCS: unable to load EFS data for rail CX");

    return;
  }

  /*-----------------------------------------------------------------------*/
  /* Load the EFS data for MSS.                                            */
  /*-----------------------------------------------------------------------*/

  eResult = VCS_LoadNV_MSS(pDrvCtxt, hConfig);
  if (eResult != DAL_SUCCESS)
  {
    DALSYS_LogEvent(
      0,
      DALSYS_LOGEVENT_FATAL_ERROR,
      "DALLOG Device VCS: unable to load EFS data for rail CX");

    return;
  }

  /*-----------------------------------------------------------------------*/
  /* Destroy the handle.                                                   */
  /*-----------------------------------------------------------------------*/

  CoreIni_ConfigDestroy(hConfig);

} /* END VCS_LoadNV */


/* =========================================================================
**  Function : VCS_InitVDDMSSRailVoltageTable
** =========================================================================*/
/**
  Initializes the voltage table for this HW version.

  @param pRail [in] -- Pointer rail node.
  @return
  DAL_ERROR if a voltage table not initialized, other DAL_SUCCESS.

  @dependencies
  None.
*/

static DALResult VCS_InitVDDMSSRailVoltageTable
(
  VCSRailNodeType *pRail
)
{
  DALResult                  eResult;
  uint32                     i, nFloorUV, nCeilingUV;
  VCSCornerVoltageRangeType *pRailVoltageRange;
  VCSRailVoltageTableType   *pVoltageTable;
  VCSDrvCtxt                *pDrvCtxt;

  /*-----------------------------------------------------------------------*/
  /* Sanity.                                                               */
  /*-----------------------------------------------------------------------*/

  if (pRail == NULL)
  {
    return DAL_ERROR_INVALID_PARAMETER;
  }

  pDrvCtxt = VCS_GetDrvCtxt();

  /*-----------------------------------------------------------------------*/
  /* Query CPR for open loop voltages.                                     */
  /*-----------------------------------------------------------------------*/

  ULOG_RT_PRINTF_0(pDrvCtxt->hVCSLog, "[Open-Loop Voltages]");

  pVoltageTable = pRail->pVoltageTable;
  for (i = 0; i < pVoltageTable->nNumVoltageRanges; i++)
  {
    // Copy corner voltage table from BSP
    pRailVoltageRange = &pVoltageTable->pVoltageRange[i];

    // Query CPR for open-loop voltages.
    eResult =
      CprGetFloorAndCeiling(
        pRail->eRail,
        pRailVoltageRange->eCorner,
        &nFloorUV,
        &nCeilingUV);
    if (eResult != DAL_SUCCESS)
    {
      DALSYS_LogEvent(
        0,
        DALSYS_LOGEVENT_FATAL_ERROR,
        "DALLOG Device VCS: CprGetFloorandCeiling returned error[%lu].",
        eResult);

      return DAL_ERROR_INTERNAL;
    }

    /*-----------------------------------------------------------------------*/
    /* Log the voltages.                                                     */
    /*-----------------------------------------------------------------------*/

    ULOG_RT_PRINTF_6(
      pDrvCtxt->hVCSLog,
      "Rail[%s] Corner[%s] BSP-Min[%lu] BSP-Max[%lu] CPR-Min[%lu] CPR-Max[%lu]",
      pRail->pBSPConfig->szName,
      pDrvCtxt->aszCornerNameMap[pRailVoltageRange->eCorner],
      pRailVoltageRange->nMinUV,
      pRailVoltageRange->nMaxUV,
      nFloorUV,
      nCeilingUV);

    // Bounds check open-loop ceiling voltage.
    if ((nCeilingUV < pRailVoltageRange->nMaxUV) &&
        (nCeilingUV >= pRailVoltageRange->nMinUV))
    {
      pRailVoltageRange->nMaxUV = nCeilingUV;
    }

    // Bounds check open-loop floor voltage.
    if (nFloorUV > pRailVoltageRange->nMinUV)
    {
      if (nFloorUV < pRailVoltageRange->nMaxUV)
      {
        pRailVoltageRange->nMinUV = nFloorUV;
      }
      else
      {
        pRailVoltageRange->nMinUV = pRailVoltageRange->nMaxUV;
      }
    }

    /*-----------------------------------------------------------------------*/
    /* Log the final selected values.                                        */
    /*-----------------------------------------------------------------------*/

    ULOG_RT_PRINTF_4(
      pDrvCtxt->hVCSLog,
      "Rail[%s] Corner[%s] Selected Min[%lu] Max[%lu]",
      pRail->pBSPConfig->szName,
      pDrvCtxt->aszCornerNameMap[pRailVoltageRange->eCorner],
      pRailVoltageRange->nMinUV,
      pRailVoltageRange->nMaxUV);
  }

  return DAL_SUCCESS;

} /* END VCS_InitVDDMSSRailVoltageTable */


/* =========================================================================
**  Function : VCS_NPAVDDMSSRailResourceQuery
** =========================================================================*/
/**
  NPA rail resource query function.

  This function is called to get the following rail information:
    -- MVC safe voltages.

  @param *pResource  [in]  -- Pointer to the resource in question
  @param  nID        [in]  -- ID of the query.
  @param *pResult    [out] -- Pointer to the data to be filled by this function.

  @return
  npa_query_status - NPA_QUERY_SUCCESS, if query supported.
                   - NPA_QUERY_UNSUPPORTED_QUERY_ID, if query not supported.

  @dependencies
  None.
*/

static npa_query_status VCS_NPAVDDMSSRailResourceQuery
(
  npa_resource   *pResource,
  unsigned int    nID,
  npa_query_type *pResult
)
{
  npa_query_status           nStatus = NPA_QUERY_SUCCESS;
  uint32                     nNumCorners, nCorner;
  DALResult                  eResult;
  pm_err_flag_type           pm_err;
  VCSNPAMVCQueryDataType    *pMVCQueryData;
  VCSCornerType              eCorner;
  VCSRailNodeType           *pRail = (VCSRailNodeType *)pResource->node->data;

  /*-----------------------------------------------------------------------*/
  /* Validate parameters.                                                  */
  /*-----------------------------------------------------------------------*/

  if (pResource == NULL || pResult == NULL || pRail == NULL)
  {
    return NPA_QUERY_NULL_POINTER;
  }

  nNumCorners = pRail->pVoltageTable->nNumVoltageRanges;
  nCorner = nID - VCS_NPA_QUERY_VOLTAGE_LEVEL_MVC_SAFE;
  if (nCorner >= nNumCorners)
  {
    nStatus = NPA_QUERY_UNSUPPORTED_QUERY_ID;
  }
  else
  {
    eCorner = pRail->pVoltageTable->pVoltageRange[nCorner].eCorner;

    /*
     * Query result should be a client provided pointer.
     */
    pResult->type = NPA_QUERY_TYPE_REFERENCE;
    pMVCQueryData = (VCSNPAMVCQueryDataType *)pResult->data.reference;

    /*
     * Query CPR for voltage at the specified corner.
     */
    eResult =
      CPR_GetSafeVoltage(
        pRail->eRail,
        eCorner,
        &pMVCQueryData->nVoltageUV);
    if (eResult != DAL_SUCCESS)
    {
      DALSYS_LogEvent(
        0,
        DALSYS_LOGEVENT_FATAL_ERROR,
        "DALLOG Device VCS: Query corner[%lu] on rail[%s] failed voltage lookup in CPR.",
        eCorner,
        pRail->pBSPConfig->szName);

      return NPA_QUERY_NO_VALUE;
    }

    /*
     * Query PMIC for rail programming data.
     */
    pm_err =
      pmapp_pwr_mss_volt_level_info(
        pMVCQueryData->nVoltageUV,
        &pMVCQueryData->pm_rail_configs);
    if (pm_err != PM_ERR_FLAG__SUCCESS)
    {
      DALSYS_LogEvent(
        0,
        DALSYS_LOGEVENT_FATAL_ERROR,
        "DALLOG Device VCS: PMIC API returned error[%lu] for MVC safe voltage[%lu]",
        pm_err,
        pMVCQueryData->nVoltageUV);

      return NPA_QUERY_NO_VALUE;
    }
  }

  return nStatus;

} /* END VCS_NPAVDDMSSRailResourceQuery */


/* =========================================================================
**  Function : VCS_InitVDDMSS
** =========================================================================*/
/**
  Initialize the VDD_MSS rail.

  This function initializes the VDD_MSS NPA resource.

  @param *pDrvCtxt [in] -- Pointer to driver context.

  @return
  DAL_SUCCESS -- Initialization was successful.
  DAL_ERROR -- Initialization failed.

  @dependencies
  None.
*/

static DALResult VCS_InitVDDMSS
(
  VCSDrvCtxt *pDrvCtxt
)
{
  VCSRailNodeType         *pRail;
  VCSImageCtxtType        *pImageCtxt;
  npa_resource_state       nInitialState;
  uint32                   nMaxVoltageRange;
  DALResult                eResult;
  VCSRailCornerConfigType *pSupportedCornerConfig;

  pImageCtxt = (VCSImageCtxtType *)pDrvCtxt->pImageCtxt;

  /*-----------------------------------------------------------------------*/
  /* Get a pointer to the VDD_MSS rail data node.                          */
  /*-----------------------------------------------------------------------*/

  pRail = pDrvCtxt->apRailMap[VCS_RAIL_MSS];
  if (pRail == NULL)
  {
    return DAL_ERROR_INTERNAL;
  }

  /*-----------------------------------------------------------------------*/
  /* Hook up the rail's "set corner" function pointer.                     */
  /*-----------------------------------------------------------------------*/

  pRail->fpSetRailCorner = &VCS_SetVDDMSSCorner;

  /*-----------------------------------------------------------------------*/
  /* Hook up the rail's "query" function pointer.                          */
  /*-----------------------------------------------------------------------*/

  pRail->fpRailQuery = &VCS_NPAVDDMSSRailResourceQuery;

  /*-----------------------------------------------------------------------*/
  /* Initialize the voltage table for VDD_MSS.                             */
  /*-----------------------------------------------------------------------*/

  eResult = VCS_InitVDDMSSRailVoltageTable(pRail);
  if (eResult != DAL_SUCCESS)
  {
    DALSYS_LogEvent(
      0,
      DALSYS_LOGEVENT_FATAL_ERROR,
      "DALLOG Device VCS: Failed to initialize CPUs / LDOs.");

    return DAL_ERROR_INTERNAL;
  }

  /*-----------------------------------------------------------------------*/
  /* Define the initial state and create the MSS rail broadcast node       */
  /* Choose the maximum voltage from the highest voltage corner supported  */
  /* by this target from the BSP data.                                     */
  /*-----------------------------------------------------------------------*/

  /*-----------------------------------------------------------------------*/
  /* Find the corner configuration supported for this hardware.            */
  /*-----------------------------------------------------------------------*/

  eResult = VCS_DetectRailBSPVersion(pRail, &pSupportedCornerConfig);
  if (eResult != DAL_SUCCESS)
  {
    DALSYS_LogEvent(
      0,
      DALSYS_LOGEVENT_FATAL_ERROR,
      "DALLOG Device VCS: Failed to detect a valid BSP version.");

    return DAL_ERROR_INTERNAL;
  }

  /*-----------------------------------------------------------------------*/
  /* The last corner has the highest voltage range.                        */
  /*-----------------------------------------------------------------------*/

  nMaxVoltageRange = pSupportedCornerConfig->nNumVoltageRanges - 1;

  /*-----------------------------------------------------------------------*/
  /* Assign the max value and create the node.                             */
  /*-----------------------------------------------------------------------*/

  VCS_NPAMSSUVResource.resource.max =
    pSupportedCornerConfig->pVoltageRange[nMaxVoltageRange].nMaxUV;

  nInitialState = 0;
  npa_define_node(&VCS_NPAMSSUVResource.node, &nInitialState, NULL);

  /*-----------------------------------------------------------------------*/
  /* Create NPA client handle to remote PMIC resource for NAS requests.    */
  /*-----------------------------------------------------------------------*/

  pImageCtxt->hClientPMIC =
    npa_create_sync_client(
      PMIC_NPA_GROUP_ID_RAIL_MSS,
      pRail->pBSPConfig->szName,
      NPA_CLIENT_SUPPRESSIBLE);
  if (!pImageCtxt->hClientPMIC)
  {
    DALSYS_LogEvent(
      0,
      DALSYS_LOGEVENT_FATAL_ERROR,
      "DALLOG Device VCS: Failed to create an NPA client handle for PMIC VDD_MSS resource.");

    return DAL_ERROR_INTERNAL;
  }

  return DAL_SUCCESS;

} /* END VCS_InitVDDMSS */


/* =========================================================================
**  Function : VCS_InitQ6LDOVoltageTable
** =========================================================================*/
/**
  Initializes the voltage table for this HW version.

  @param pCPU [in] -- Pointer to CPU node.
  @return
  DAL_ERROR if a voltage table not initialized, other DAL_SUCCESS.

  @dependencies
  None.
*/

static DALResult VCS_InitQ6LDOVoltageTable
(
  VCSCPUNodeType *pCPU
)
{
  uint32                  i;
  boolean                 bIsFuseEnabled;
  VCSLDONodeType         *pLDO;
  VCSCornerVoltageType   *pLDOVoltage;
  VCSLDOVoltageTableType *pVoltageTable;
  VCSDrvCtxt             *pDrvCtxt;
  DALResult               eResult;

  /*-----------------------------------------------------------------------*/
  /* Sanity.                                                               */
  /*-----------------------------------------------------------------------*/

  if (pCPU == NULL || pCPU->pLDO == NULL)
  {
    return DAL_ERROR_INVALID_PARAMETER;
  }

  pLDO = pCPU->pLDO;
  pDrvCtxt = VCS_GetDrvCtxt();

  /*-----------------------------------------------------------------------*/
  /* Update corner voltage table based on dynamic target inspection.       */
  /*-----------------------------------------------------------------------*/

  pVoltageTable = pLDO->pVoltageTable;
  for (i = 0; i < pVoltageTable->nNumCornerVoltages; i++)
  {
    pLDOVoltage = &pVoltageTable->pCornerVoltage[i];

    /*
     * Query CPR for eLDO voltage at this corner.
     */
    eResult =
      CPR_GetEldoVoltageRecommendation(
        VCS_RAIL_CX,
        pLDOVoltage->eCorner,
        &pLDOVoltage->nVoltageUV);
    if (eResult != DAL_SUCCESS)
    {
      DALSYS_LogEvent(
        0,
        DALSYS_LOGEVENT_FATAL_ERROR,
        "DALLOG Device VCS: CPR returned error[%lu] for voltage query on LDO[%s] corner[%s].",
        eResult,
        pCPU->pBSPConfig->szName,
        pDrvCtxt->aszCornerNameMap[pLDOVoltage->eCorner]);

      return DAL_ERROR_INTERNAL;
    }

    /*
     * The LDO is enabled by default prior to efuse checks.
     */
    bIsFuseEnabled = TRUE;

    /*
     * Read LDO disablement fuse for the MIN SVS corner.
     */
    if (pLDOVoltage->eCorner == VCS_CORNER_LOW_MIN)
    {
      if (!(HWIO_INF(QFPROM_CORR_CALIB_ROW1_MSB, Q6SS0_LDO_MIN_SVS_ENABLE)))
      {
        bIsFuseEnabled = FALSE;
      }
    }

    /*
     * Read LDO disablement fuse for the SVS2 corner.
     */
    else if (pLDOVoltage->eCorner == VCS_CORNER_LOW_MINUS)
    {
      if (!(HWIO_INF(QFPROM_CORR_CALIB_ROW1_MSB, Q6SS0_LDO_LOW_SVS_ENABLE)))
      {
        bIsFuseEnabled = FALSE;
      }
    }

    /*
     * Read LDO disablement fuse for the SVS corner.
     */
    else if (pLDOVoltage->eCorner == VCS_CORNER_LOW)
    {
      if (!(HWIO_INF(QFPROM_CORR_CALIB_ROW0_LSB, Q6SS0_LDO_SVS_ENABLE )))
      {
        bIsFuseEnabled = FALSE;
      }
    }

    /*
     * We only expect MinSVS, SVS2 and SVS corners to be supported by the LDO.
     */
    else
    {
      DALSYS_LogEvent(
        0,
        DALSYS_LOGEVENT_FATAL_ERROR,
        "DALLOG Device VCS: Unexpected corner[%s] on LDO[%s] in BSP.",
        pDrvCtxt->aszCornerNameMap[pLDOVoltage->eCorner],
        pCPU->pBSPConfig->szName);

      return DAL_ERROR_INTERNAL;
    }

    if (!bIsFuseEnabled)
    {
      pVoltageTable->apCornerMap[pLDOVoltage->eCorner] = NULL;
    }

    /*
     * Log the corner voltages and status.
     */
    ULOG_RT_PRINTF_4(
      pDrvCtxt->hVCSLog,
      "LDO[%s] corner[%s] voltage[%lu] permitted[%s]",
      pCPU->pBSPConfig->szName,
      pDrvCtxt->aszCornerNameMap[pLDOVoltage->eCorner],
      pLDOVoltage->nVoltageUV,
      bIsFuseEnabled ? "YES" : "NO");
  }

  return DAL_SUCCESS;

} /* END VCS_InitQ6LDOVoltageTable */


/* =========================================================================
**  Function : VCS_InitQ6LDO
** =========================================================================*/
/**
  Initialize the Q6 LDO

  @param *pDrvCtxt [in] -- Pointer to driver context.

  @return
  DAL_SUCCESS -- Initialization was successful.
  DAL_ERROR_INTERNAL -- Q6 CPU node was not found.

  @dependencies
  None.
*/

static DALResult VCS_InitQ6LDO
(
  VCSDrvCtxt *pDrvCtxt
)
{
  VCSCPUNodeType *pCPU;
  DALResult       eResult;

  /*-----------------------------------------------------------------------*/
  /* Get the Q6 CPU node.                                                  */
  /*-----------------------------------------------------------------------*/

  pCPU = pDrvCtxt->apCPUMap[CLOCK_CPU_MSS_Q6];
  if (pCPU == NULL)
  {
    DALSYS_LogEvent(
      0,
      DALSYS_LOGEVENT_FATAL_ERROR,
      "DALLOG Device VCS: Unable to get CPU node.");

    return DAL_ERROR_INTERNAL;
  }

  /*-----------------------------------------------------------------------*/
  /* Get the LDO node.                                                     */
  /*-----------------------------------------------------------------------*/

  if (pCPU->pLDO == NULL)
  {
    DALSYS_LogEvent(
      0,
      DALSYS_LOGEVENT_FATAL_ERROR,
      "DALLOG Device VCS: Unable to get LDO node.");

    return DAL_ERROR_INTERNAL;
  }

  /*-----------------------------------------------------------------------*/
  /* Initialize the LDO voltage table.                                     */
  /*-----------------------------------------------------------------------*/

  eResult = VCS_InitQ6LDOVoltageTable(pCPU);
  if (eResult != DAL_SUCCESS)
  {
    return eResult;
  }

  /*-----------------------------------------------------------------------*/
  /* Program the LDO_CFG registers per recommendation from HW.             */
  /*-----------------------------------------------------------------------*/

  HAL_ldo_ConfigLDO(pCPU->pLDO->eLDO);

  return DAL_SUCCESS;

} /* END VCS_InitQ6LDO */


/* =========================================================================
**  Function : VCS_InitImage
** =========================================================================*/
/*
  See VCSDriver.h
*/

DALResult VCS_InitImage
(
  VCSDrvCtxt *pDrvCtxt
)
{
  DALResult eResult;

  /*-----------------------------------------------------------------------*/
  /* Assign the image context.                                             */
  /*-----------------------------------------------------------------------*/

  pDrvCtxt->pImageCtxt = &VCS_ImageCtxt;

  /*-----------------------------------------------------------------------*/
  /* Initialize VDD_MSS.                                                   */
  /*-----------------------------------------------------------------------*/

  eResult = VCS_InitVDDMSS(pDrvCtxt);
  if (eResult != DAL_SUCCESS)
  {
    DALSYS_LogEvent(
      0,
      DALSYS_LOGEVENT_FATAL_ERROR,
      "DALLOG Device VCS: Unable to init VDD_MSS.");

    return eResult;
  }

  /*-----------------------------------------------------------------------*/
  /* Initialize the Q6 LDO.                                                */
  /*-----------------------------------------------------------------------*/

  eResult = VCS_InitQ6LDO(pDrvCtxt);
  if (eResult != DAL_SUCCESS)
  {
    DALSYS_LogEvent(0,
      DALSYS_LOGEVENT_FATAL_ERROR,
      "DALLOG Device VCS: Unable to init Q6 LDO.");

    return eResult;
  }

  /*-----------------------------------------------------------------------*/
  /* Done.                                                                 */
  /*-----------------------------------------------------------------------*/

  return DAL_SUCCESS;

} /* END VCS_InitImage */


/* =========================================================================
**  Function : VCSStub_InitImage
** =========================================================================*/
/*
  See VCSDriver.h
*/

DALResult VCSStub_InitImage
(
  VCSDrvCtxt *pDrvCtxt
)
{
  /*-----------------------------------------------------------------------*/
  /* Good to go.                                                           */
  /*-----------------------------------------------------------------------*/

  return DAL_SUCCESS;

} /* END VCSStub_InitImage */

