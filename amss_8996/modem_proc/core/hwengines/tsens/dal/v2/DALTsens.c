/*============================================================================
  FILE:         TsensDevice.c

  OVERVIEW:     Implementation of the TSENS device library

  DEPENDENCIES: None

                Copyright (c) 2012-2015 Qualcomm Technologies, Inc.
                All Rights Reserved.
                Qualcomm Technologies Proprietary and Confidential.
============================================================================*/
/*============================================================================
  EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.  Please
  use ISO format for dates.

  $Header: //components/rel/core.mpss/3.4.c3.11/hwengines/tsens/dal/v2/DALTsens.c#1 $$DateTime: 2016/03/28 23:02:17 $$Author: mplcsds1 $

  when        who  what, where, why
  ----------  ---  -----------------------------------------------------------
  2015-02-04  jjo  Add calibration API.
  2014-12-10  jjo  Modify for hardware temperature conversion.

============================================================================*/
/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include "DALTsens.h"
#include "HALtsens.h"
#include "TsensBsp.h"
#include "msg.h"

/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Type Declarations
 * -------------------------------------------------------------------------*/
typedef enum
{
   TSENS_DEVICE_STATE_INIT = 0,
   TSENS_DEVICE_STATE_ERROR,
   TSENS_DEVICE_STATE_READY
} TsensDeviceStateType;

typedef struct
{
   uint8 *pucTsensSROTAddr;
   uint8 *pucTsensTMAddr;
   uint32 uChannelEnableMask;
} TsensControllerType;

typedef struct
{
   const TsensBspType *pBsp;
   TsensControllerType *paControllers;
   DalDeviceHandle *phHWIO;
   TsensDeviceStateType eDeviceState;
} TsensDevCtxtType;

/*----------------------------------------------------------------------------
 * Static Function Declarations
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Global Data Definitions
 * -------------------------------------------------------------------------*/
TsensDevCtxtType gTsensDevCtxt = {0};

/*----------------------------------------------------------------------------
 * Static Variable Definitions
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Static Function Declarations and Definitions
 * -------------------------------------------------------------------------*/
static uint8* Tsens_MapHWIORegion(const char *pszBase)
{
   DALResult status;
   uint8 *puVirtAddr;

   status = DalHWIO_MapRegion(gTsensDevCtxt.phHWIO,
                              pszBase,
                              &puVirtAddr);

   if (status != DAL_SUCCESS)
   {
      return NULL;
   }

   return puVirtAddr;
}

static void Tsens_GetTempInternal(uint32 uController, uint32 uChannel, int32 *pnDeciDegC)
{
   int32 nDeciDegC = 0;
   int32 nDeciDegCTry1;
   int32 nDeciDegCTry2;
   int32 nDeciDegCTry3;
   uint8 *pucTsensTMAddr;
   boolean bValid;

   pucTsensTMAddr = gTsensDevCtxt.paControllers[uController].pucTsensTMAddr;

   bValid = HAL_tsens_tm_GetChannelPrevTemp(pucTsensTMAddr, uChannel, &nDeciDegCTry1);
   if (bValid == TRUE)
   {
      nDeciDegC = nDeciDegCTry1;
   }
   else
   {
      bValid = HAL_tsens_tm_GetChannelPrevTemp(pucTsensTMAddr, uChannel, &nDeciDegCTry2);
      if (bValid == TRUE)
      {
         nDeciDegC = nDeciDegCTry2;
      }
      else
      {
         bValid = HAL_tsens_tm_GetChannelPrevTemp(pucTsensTMAddr, uChannel, &nDeciDegCTry3);
         if (bValid == TRUE)
         {
            nDeciDegC = nDeciDegCTry3;
         }
         else if (nDeciDegCTry1 == nDeciDegCTry2)
         {
            nDeciDegC = nDeciDegCTry1;
         }
         else if (nDeciDegCTry2 == nDeciDegCTry3)
         {
            nDeciDegC = nDeciDegCTry2;
         }
         else
         {
            nDeciDegC = nDeciDegCTry1;
         }
      }
   }

   *pnDeciDegC = (int32)nDeciDegC;

   return;
}

/*----------------------------------------------------------------------------
 * Externalized Function Definitions
 * -------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------
 * Functions specific to the DalTsens interface
 * ----------------------------------------------------------------------*/
DALResult Tsens_SetThreshold(TsensClientCtxt *pCtxt, uint32 uSensor, TsensThresholdType eThreshold, int32 nDeciDegC, DALSYSEventHandle hEvent)
{
   return TSENS_ERROR_UNSUPPORTED;
}

DALResult Tsens_GetTemp(TsensClientCtxt *pCtxt, uint32 uSensor, TsensTempType *pTemp)
{
   const TsensBspType *pBsp = gTsensDevCtxt.pBsp;
   uint32 uController;
   uint32 uChannel;
   int32 nDeciDegC;

   if (uSensor >= pBsp->uNumSensors)
   {
      return TSENS_ERROR_INVALID_PARAMETER;
   }

   if (pTemp == NULL)
   {
      return TSENS_ERROR_INVALID_PARAMETER;
   }

   uController = pBsp->paSensorCfgs[uSensor].uController;
   uChannel = pBsp->paSensorCfgs[uSensor].uChannel;

   if (((1 << uChannel) & gTsensDevCtxt.paControllers[uController].uChannelEnableMask) == 0)
   {
      return TSENS_ERROR_DEAD_SENSOR;
   }

   Tsens_GetTempInternal(uController, uChannel, &nDeciDegC);

   pTemp->nDeciDegC = nDeciDegC;
   pTemp->nDegC = nDeciDegC / 10;

   MSG_2(MSG_SSID_ADC,
         MSG_LEGACY_HIGH,
         "TSENS: Sensor = %u, DeciDegC = %i",
         uSensor,
         nDeciDegC);

   return DAL_SUCCESS;
}

DALResult Tsens_GetTempRange(TsensClientCtxt *pCtxt, uint32 uSensor, TsensTempRangeType *pTempRange)
{
   return TSENS_ERROR_UNSUPPORTED;
}

DALResult Tsens_DeviceInit(TsensClientCtxt *pCtxt)
{
   const char *pszTsensBspPropName = "TSENS_BSP";
   const TsensControllerCfgType *pControllerCfg;
   const TsensBspType *pBsp;
   TsensDevCtxt *pDevCtxt = pCtxt->pTsensDevCtxt;
   DALSYSPropertyVar propertyVar;
   TsensControllerType *pController;
   uint32 uController;
   DALResult status;

   gTsensDevCtxt.eDeviceState = TSENS_DEVICE_STATE_INIT;

   /* Get the BSP */
   status = DALSYS_GetPropertyValue(pDevCtxt->hProp,
                                    pszTsensBspPropName,
                                    0,
                                    &propertyVar);
   if (status != DAL_SUCCESS)
   {
      DALSYS_LogEvent(pDevCtxt->DevId, DALSYS_LOGEVENT_FATAL_ERROR,
                      "Tsens_DeviceInit : Device failed to get tsens_bsp property");
      goto error;
   }

   pBsp = (TsensBspType *)propertyVar.Val.pStruct;
   gTsensDevCtxt.pBsp = pBsp;

   /* Allocate memory for controller context */
   status = DALSYS_Malloc((pBsp->uNumControllers * sizeof(TsensControllerType)),
                          (void **)&gTsensDevCtxt.paControllers);
   if (status != DAL_SUCCESS || gTsensDevCtxt.paControllers == NULL)
   {
      status = DAL_ERROR;
      DALSYS_LogEvent(pDevCtxt->DevId, DALSYS_LOGEVENT_FATAL_ERROR,
                      "Tsens_DeviceInit : Failed to allocate memory");
      goto error;
   }

   /* Attach to the HWIO DAL */
   status = DAL_DeviceAttach(DALDEVICEID_HWIO, &gTsensDevCtxt.phHWIO);
   if (status != DAL_SUCCESS)
   {
      DALSYS_LogEvent(pDevCtxt->DevId, DALSYS_LOGEVENT_FATAL_ERROR,
                      "Tsens_DeviceInit : Failed to attach to HWIO with status=0x%08x\n", (unsigned int)status);
      goto error;
   }

   /* Map physical to virtual addresses */
   for (uController = 0; uController < pBsp->uNumControllers; uController++)
   {
      pControllerCfg = &pBsp->paControllerCfgs[uController];
      pController = &gTsensDevCtxt.paControllers[uController];

      pController->pucTsensSROTAddr = Tsens_MapHWIORegion(pControllerCfg->pszSROTModule);
      if (pController->pucTsensSROTAddr == NULL)
      {
         status = DAL_ERROR;
         DALSYS_LogEvent(pDevCtxt->DevId, DALSYS_LOGEVENT_FATAL_ERROR,
                         "Tsens_DeviceInit : Failed to map SROT region");
         goto error;
      }

      pController->pucTsensTMAddr = Tsens_MapHWIORegion(pControllerCfg->pszTMModule);
      if (pController->pucTsensSROTAddr == NULL)
      {
         status = DAL_ERROR;
         DALSYS_LogEvent(pDevCtxt->DevId, DALSYS_LOGEVENT_FATAL_ERROR,
                         "Tsens_DeviceInit : Failed to map TM region");
         goto error;
      }
   }

   /* Check which sensors are actually enabled */
   for (uController = 0; uController < pBsp->uNumControllers; uController++)
   {
      pController = &gTsensDevCtxt.paControllers[uController];

      pController->uChannelEnableMask = HAL_tsens_srot_GetEnabledChannels(pController->pucTsensSROTAddr);
   }

   gTsensDevCtxt.eDeviceState = TSENS_DEVICE_STATE_READY;

   return DAL_SUCCESS;

error:
   gTsensDevCtxt.eDeviceState = TSENS_DEVICE_STATE_ERROR;
   return status;
}

DALResult Tsens_DeviceDeInit(TsensClientCtxt *pCtxt)
{
   gTsensDevCtxt.eDeviceState = TSENS_DEVICE_STATE_INIT;

   return DAL_SUCCESS;
}

DALResult Tsens_GetNumSensors(TsensClientCtxt *pCtxt, uint32 *puNumSensors)
{
   const TsensBspType *pBsp = gTsensDevCtxt.pBsp;

   if (puNumSensors == NULL)
   {
      return TSENS_ERROR_INVALID_PARAMETER;
   }

   *puNumSensors = pBsp->uNumSensors;

   return DAL_SUCCESS;
}

DALResult Tsens_SetEnableThresholds(TsensClientCtxt *pCtxt, DALBOOL bEnableThresholds)
{
   return TSENS_ERROR_UNSUPPORTED;
}

DALResult Tsens_CheckCalibration(TsensClientCtxt *pCtxt, uint32 uSensor)
{
   const TsensBspType *pBsp = gTsensDevCtxt.pBsp;
   uint32 uController;
   uint32 uChannel;

   if (uSensor >= pBsp->uNumSensors)
   {
      return TSENS_ERROR_INVALID_PARAMETER;
   }

   uController = pBsp->paSensorCfgs[uSensor].uController;
   uChannel = pBsp->paSensorCfgs[uSensor].uChannel;

   if (((1 << uChannel) & gTsensDevCtxt.paControllers[uController].uChannelEnableMask) == 0)
   {
      return TSENS_ERROR_DEAD_SENSOR;
   }

   return DAL_SUCCESS;
}

/*-------------------------------------------------------------------------
 * Functions specific to the DAL interface
 * ----------------------------------------------------------------------*/
DALResult
Tsens_DriverInit(TsensDrvCtxt *pCtxt)
{
   return DAL_SUCCESS;
}

DALResult
Tsens_DriverDeInit(TsensDrvCtxt *pCtxt)
{
   return DAL_SUCCESS;
}

DALResult
Tsens_RegisterClient(TsensClientCtxt *pCtxt)
{
   return DAL_SUCCESS;
}

DALResult
Tsens_DisableClient(uint32 uClient)
{
   return TSENS_ERROR_UNSUPPORTED;
}

DALResult
Tsens_DeregisterClient(TsensClientCtxt *pCtxt)
{
   return DAL_SUCCESS;
}

