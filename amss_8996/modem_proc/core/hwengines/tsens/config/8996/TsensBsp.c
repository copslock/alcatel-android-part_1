/*============================================================================
  FILE:         TsensBsp.c

  OVERVIEW:     Tsens bsp file

  DEPENDENCIES: None

                Copyright (c) 2014-2015 Qualcomm Technologies, Inc.
                All Rights Reserved.
                Qualcomm Technologies Proprietary and Confidential.
============================================================================*/
/*============================================================================
  EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.  Please
  use ISO format for dates.

  $Header: //components/rel/core.mpss/3.4.c3.11/hwengines/tsens/config/8996/TsensBsp.c#1 $$DateTime: 2016/03/28 23:02:17 $$Author: mplcsds1 $

  when        who  what, where, why
  ----------  ---  -----------------------------------------------------------
  2015-01-29  jjo  Use designated initializers.
  2014-12-10  jjo  Use array of sensors.
  2014-07-08  jjo  Initial version.

============================================================================*/
/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include "DALFramework.h"
#include "TsensBsp.h"

/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * -------------------------------------------------------------------------*/
#define ARRAY_LENGTH(a) (sizeof(a) / sizeof((a)[0]))

/*----------------------------------------------------------------------------
 * Type Declarations
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Static Function Declarations
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Global Data Definitions
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Static Variable Definitions
 * -------------------------------------------------------------------------*/
const TsensControllerCfgType gaTsensControllerCfgs[] =
{
   /* Controller 0 */
   {
      .pszSROTModule = "+MPM2_TSENS0",
      .pszTMModule   = "+MPM2_TSENS0_TSENS0_TM"
   },

   /* Controller 1 */
   {
      .pszSROTModule = "+MPM2_TSENS1",
      .pszTMModule   = "+MPM2_TSENS1_TSENS1_TM"
   },
};

const TsensSensorCfgType gaTsensSensorCfgs[] =
{
   /* Sensor 0 */
   {
      .uController = 0,
      .uChannel    = 0
   },

   /* Sensor 1 */
   {
      .uController = 0,
      .uChannel    = 1
   },

   /* Sensor 2 */
   {
      .uController = 0,
      .uChannel    = 2
   },

   /* Sensor 3 */
   {
      .uController = 0,
      .uChannel    = 3
   },

   /* Sensor 4 */
   {
      .uController = 0,
      .uChannel    = 4
   },

   /* Sensor 5 */
   {
      .uController = 0,
      .uChannel    = 5
   },

   /* Sensor 6 */
   {
      .uController = 0,
      .uChannel    = 6
   },

   /* Sensor 7 */
   {
      .uController = 0,
      .uChannel    = 7
   },

   /* Sensor 8 */
   {
      .uController = 0,
      .uChannel    = 8
   },

   /* Sensor 9 */
   {
      .uController = 0,
      .uChannel    = 9
   },

   /* Sensor 10 */
   {
      .uController = 0,
      .uChannel    = 10
   },

   /* Sensor 11 */
   {
      .uController = 0,
      .uChannel    = 11
   },

   /* Sensor 12 */
   {
      .uController = 0,
      .uChannel    = 12
   },

   /* Sensor 13 */
   {
      .uController = 1,
      .uChannel    = 1
   },

   /* Sensor 14 */
   {
      .uController = 1,
      .uChannel    = 6
   },

   /* Sensor 15 */
   {
      .uController = 1,
      .uChannel    = 7
   },

   /* Sensor 16 */
   {
      .uController = 1,
      .uChannel    = 0
   },

   /* Sensor 17 */
   {
      .uController = 1,
      .uChannel    = 2
   },

   /* Sensor 18 */
   {
      .uController = 1,
      .uChannel    = 3
   },

   /* Sensor 19 */
   {
      .uController = 1,
      .uChannel    = 4
   },

   /* Sensor 20 */
   {
      .uController = 1,
      .uChannel    = 5
   },
};

const TsensBspType TsensBsp[] =
{
   {
      .paControllerCfgs = gaTsensControllerCfgs,
      .uNumControllers  = ARRAY_LENGTH(gaTsensControllerCfgs),
      .paSensorCfgs     = gaTsensSensorCfgs,
      .uNumSensors      = ARRAY_LENGTH(gaTsensSensorCfgs)
   }
};

/*----------------------------------------------------------------------------
 * Static Function Declarations and Definitions
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Externalized Function Definitions
 * -------------------------------------------------------------------------*/

