/*============================================================================
  FILE:         VAdcTMBsp.c

  OVERVIEW:     Board support package for the VADC TM.

  DEPENDENCIES: None

                Copyright (c) 2015 Qualcomm Technologies, Inc.
                All Rights Reserved.
                Qualcomm Technologies Proprietary and Confidential.
============================================================================*/
/*============================================================================
  EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.  Please
  use ISO format for dates.

  $Header: //components/rel/core.mpss/3.4.c3.11/hwengines/adc/config/9x45/VAdcTMBsp.c#1 $

  when        who  what, where, why
  ----------  ---  -----------------------------------------------------------
  2015-02-18  jjo  Switch to VBAT_MIN peripheral.
  2015-01-08  jjo  Initial version.

============================================================================*/
/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include "VAdcTMDevice.h"
#include "AdcInputs.h"

/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * -------------------------------------------------------------------------*/
#define ARRAY_LENGTH(a) (sizeof(a) / sizeof((a)[0]))

/*----------------------------------------------------------------------------
 * Type Declarations
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Static Function Declarations
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Global Data Definitions
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Static Variable Definitions
 * -------------------------------------------------------------------------*/
static const VAdcTMMeasConfigType vAdcTMMeasurements[] =
{
   /* Meas 0: */
   {
      .pName                     = ADC_INPUT_VPH_PWR,
      .eMeasIntervalTimeSelect   = VADCTM_MEAS_INTERVAL_TIME1,  // Reserved
   },
};

const VAdcTMBspType VAdcTMBsp[] =
{
   {
      .paMeasConfig         = vAdcTMMeasurements,
      .uNumMeas             = ARRAY_LENGTH(vAdcTMMeasurements),
      .eAccessPriority      = SPMI_BUS_ACCESS_PRIORITY_LOW,
      .uSlaveId             = 0,
      .uPmicDevice          = 0,
      .uPeripheralID        = 0x35,
      .uMasterID            = 0,
      .uMinDigMinor         = 0,
      .uMinDigMajor         = 0,
      .uMinAnaMinor         = 0,
      .uMinAnaMajor         = 0,
      .uPerphType           = 0x08,
      .uPerphSubType        = 0x04,
      .eDecimationRatio     = VADCTM_DECIMATION_RATIO_1024,
      .eClockSelect         = VADCTM_CLOCK_SELECT_4P8_MHZ,
      .uConversionTime_us   = 426,
      .uVAdcConfigIdx       = 0,
      .eSettlingDelay       = VADCTM_SETTLING_DELAY_0_US,
      .eFastAverageMode     = VADCTM_FAST_AVERAGE_4_SAMPLES,
      .eMeasIntervalTime1   = VADCTM_MEAS_INTERVAL_TIME1_3P9_MS,
      .eMeasIntervalTime2   = VADCTM_MEAS_INTERVAL_TIME2_100_MS,  // Reserved
      .eMeasIntervalTime3   = VADCTM_MEAS_INTERVAL_TIME3_5_S,     // Reserved
      .nThresholdMin_mV     = 0,
      .nThresholdMax_mV     = 1800
   }
};

