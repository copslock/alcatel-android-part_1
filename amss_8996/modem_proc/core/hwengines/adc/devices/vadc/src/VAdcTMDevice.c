/*============================================================================
  FILE:         VAdcTMTMDevice.c

  OVERVIEW:     Implementation of a device for the VADCTM TM peripheral.

  DEPENDENCIES: None

                Copyright (c) 2013, 2015 Qualcomm Technologies, Inc.
                All Rights Reserved.
                Qualcomm Technologies Proprietary and Confidential.
============================================================================*/
/*============================================================================
  EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.  Please
  use ISO format for dates.

  $Header: //components/rel/core.mpss/3.4.c3.11/hwengines/adc/devices/vadc/src/VAdcTMDevice.c#1 $$DateTime: 2016/03/28 23:02:17 $$Author: mplcsds1 $

  when        who  what, where, why
  ----------  ---  -----------------------------------------------------------
  2015-05-27  jjo  Clear thresholds on unregister.
  2015-03-27  jjo  Added tolerance API.
  2013-11-21  jjo  Enable NPA latency request.
  2013-10-10  jjo  Updated disable sequence.
  2013-07-15  jjo  Vote for minimum latency during delays.
  2013-03-06  jjo  Updated threshold returned by payload.
  2013-02-22  jjo  Initial version.

============================================================================*/
/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include "VAdcTMDevice.h"
#include "DDIAdc.h"         /* For payload type */
#include "DDIChipInfo.h"
#include "DalVAdc.h"
#include "msg.h"

/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * -------------------------------------------------------------------------*/
#define VADCTM_DISABLE_NUM_RETRIES 1000
#define VADCTM_DEFAULT_STACK_SIZE 0x2000
#define VADCTM_MAX_NUM_DEVICES 1

/*----------------------------------------------------------------------------
 * Type Declarations
 * -------------------------------------------------------------------------*/
typedef enum
{
   VADCTM_CLIENT_STATE_UNCLAIMED = 0,
   VADCTM_CLIENT_STATE_CLAIMED
} VAdcTMClientStateType;

typedef enum
{
   VADCTM_THRESHOLD_STATE_DISABLED = 0,
   VADCTM_THRESHOLD_STATE_THRESHOLD,
   VADCTM_THRESHOLD_STATE_TOLERANCE,
   VADCTM_THRESHOLD_STATE_TRIGGERED
} VAdcTMThresholdStateType;

typedef struct
{
   DALSYSEventHandle hEvent;
   VAdcTMThresholdStateType eThresholdState;
   int32 nPhysicalDesired;
   uint32 uCode;
   int32 nPhysicalMonitored;
   int32 nTolerance;
   int32 nCurrentValue;
} VAdcTMThresholdRequestType;

typedef struct
{
   VAdcTMThresholdRequestType aClientThresholds[VADCTM_MAX_NUM_CLIENTS][ADC_DEVICE_TM_NUM_THRESHOLDS];
   uint32 uVAdcChannelIdx;
   int32 nPhysical_min;
   int32 nPhysical_max;
   DALBOOL bPhysicalInverseToCode;
} VAdcTMMeasurementType;

typedef struct
{
   VAdcTMClientStateType eState;
   uint32 uClientId;
   DALBOOL bEnabled;
} VAdcTMClientType;

typedef struct
{
   VAdcTMDevCtxt *pDevCtxt;
} VAdcTMHalInterfaceCtxtType;

struct VAdcTMDevCtxt
{
   VAdcTMClientType aClients[VADCTM_MAX_NUM_CLIENTS];       /* Client array */
   VAdcTMMeasurementType aThresholds[VADCTM_MAX_NUM_MEAS];  /* Threshold array */
   VAdcTMHalInterfaceCtxtType vAdcTMHalInterfaceCtxt;       /* VADCTM HAL interface context */
   VAdcTMHalInterfaceType iVAdcTMHalInterface;              /* VADCTM HAL interface */
   VAdcTMRevisionInfoType revisionInfo;                     /* VADCTM revision info */
   VAdcCalibDataType vAdcCalibData;                         /* Calibration data */
   pm_device_info_type pmicDeviceInfo;                      /* PMIC device info */
   const VAdcTMBspType *pBsp;                               /* pointer to the BSP */
   const char *pszLastErrorMsg;                             /* last error message - for debugging help */
   DalDeviceHandle *phVAdcDev;                              /* handle to the ADC device */
   DALSYSEventHandle ahDelayEvents[_VADCTM_NUM_EVENTS];     /* events used to cause a delay */
   DALSYSSyncHandle hSync;                                  /* synchronization object */
   DALSYS_SYNC_OBJECT(syncObject);                          /* synchronization object */
   DALSYSWorkLoopHandle hThresholdWorkLoop;                 /* work loop handle */
   DALSYSEventHandle hWorkLoopEvent;                        /* event to trigger work loop */
   DALSYSEventHandle hSignalEvent;                          /* signal set by the interrupt */
   DALSYS_EVENT_OBJECT(signalEventObject);                  /* signal set by the interrupt */
   npa_client_handle hNPACpuLatency;                        /* npa handle for min latency vote */
   npa_client_handle hNPAPmicHkadc;                         /* npa handle for VREG */
   DalDeviceHandle *phSpmiDev;                              /* handle to the SPMI DAL device */
   DALBOOL bProcessingThresholdEvent;                       /* whether or not the driver is currently processing a threshold event */
   DALDEVICEID devId;                                       /* DAL device ID */
   DALBOOL bHardwareSupported;                              /* flag to indicate if the hardware is supported */
   DALBOOL bLdoVotedOn;                                     /* whether or not the LDO is voted on by software using the PMIC NPA client */
};

/*----------------------------------------------------------------------------
 * Static Function Declarations
 * -------------------------------------------------------------------------*/
static uint32 gVAdcTMWorkloopPriority = 0;
static uint32 guNumDevices = 0;

/*----------------------------------------------------------------------------
 * Global Data Definitions
 * -------------------------------------------------------------------------*/
VAdcTMDevCtxt gaVAdcTMDevCtxt[VADCTM_MAX_NUM_DEVICES];

/*----------------------------------------------------------------------------
 * Static Variable Definitions
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Static Function Declarations and Definitions
 * -------------------------------------------------------------------------*/
/*======================================================================

  FUNCTION        VAdcTM_LogError

  DESCRIPTION     This function logs an error

  DEPENDENCIES    None

  PARAMETERS      pDevCtxt    [in]
                  pszErrorMsg [in]
                  bFatalError [in]

  RETURN VALUE    None

  SIDE EFFECTS    None

======================================================================*/
static void
VAdcTM_LogError(
   VAdcTMDevCtxt *pDevCtxt,
   const char *pszErrorMsg,
   DALBOOL bFatalError
   )
{
   pDevCtxt->pszLastErrorMsg = pszErrorMsg;

   if (bFatalError == TRUE)
   {
      MSG_SPRINTF_1(MSG_SSID_ADC, MSG_LEGACY_FATAL, "%s", pszErrorMsg);

      DALSYS_LogEvent(pDevCtxt->devId,
                      DALSYS_LOGEVENT_FATAL_ERROR,
                      pszErrorMsg);
   }
   else
   {
      MSG_SPRINTF_1(MSG_SSID_ADC, MSG_LEGACY_ERROR, "%s", pszErrorMsg);

      DALSYS_LogEvent(pDevCtxt->devId,
                      DALSYS_LOGEVENT_ERROR,
                      pszErrorMsg);
   }
}

/*======================================================================

  FUNCTION        VAdcTM_ReadBytes

  DESCRIPTION     This function reads from the VAdcTM peripheral

  DEPENDENCIES    None

  PARAMETERS
      pCtxt            [in]  pointer to the HAL interface context
      uOffset          [in]  the starting address
      pucData          [out] the bytes read
      uDataLen         [in]  the number of bytes to read
      puTotalBytesRead [out] the number of bytes read

  RETURN VALUE    VADCTM_HAL_SUCCESS or an error code

  SIDE EFFECTS    None

======================================================================*/
static VAdcTMHalResultType
VAdcTM_ReadBytes(
   void *pCtxt,
   uint32 uOffset,
   unsigned char *pucData,
   uint32 uDataLen,
   uint32 *puTotalBytesRead
   )
{
   VAdcTMHalInterfaceCtxtType *pVAdcTMHalInterfaceCtxt;
   VAdcTMDevCtxt *pDevCtxt;
   uint32 uRegisterAddress;
   DALResult result;

   if (pCtxt == NULL)
   {
      return VADCTM_HAL_ERROR;
   }

   pVAdcTMHalInterfaceCtxt = (VAdcTMHalInterfaceCtxtType *)pCtxt;
   pDevCtxt = pVAdcTMHalInterfaceCtxt->pDevCtxt;

   uRegisterAddress = uOffset & 0xFF;
   uRegisterAddress |= (pDevCtxt->pBsp->uPeripheralID << 8) & 0xFF00;

   result = DalSpmi_ReadLong(pDevCtxt->phSpmiDev,
                             pDevCtxt->pBsp->uSlaveId,
                             pDevCtxt->pBsp->eAccessPriority,
                             uRegisterAddress,
                             pucData,
                             uDataLen,
                             puTotalBytesRead);

   if (result != DAL_SUCCESS)
   {
      VAdcTM_LogError(pDevCtxt, "VAdcTM - SPMI read failed", TRUE);
      return VADCTM_HAL_ERROR;
   }

   return VADCTM_HAL_SUCCESS;
}

/*======================================================================

  FUNCTION        VAdcTM_WriteBytes

  DESCRIPTION     This function writes to the VAdcTM peripheral

  DEPENDENCIES    None

  PARAMETERS
      pCtxt            [in] pointer to the HAL interface context
      uOffset          [in] the starting address
      pucData          [in] the bytes to write
      uDataLen         [in] the number of bytes to write

  RETURN VALUE    VADCTM_HAL_SUCCESS or an error code

  SIDE EFFECTS    None

======================================================================*/
static VAdcTMHalResultType
VAdcTM_WriteBytes(
   void *pCtxt,
   uint32 uOffset,
   unsigned char* pucData,
   uint32 uDataLen
   )
{
   VAdcTMHalInterfaceCtxtType *pVAdcTMHalInterfaceCtxt;
   VAdcTMDevCtxt *pDevCtxt;
   uint32 uRegisterAddress;
   DALResult result;

   if (pCtxt == NULL)
   {
      return VADCTM_HAL_ERROR;
   }

   pVAdcTMHalInterfaceCtxt = (VAdcTMHalInterfaceCtxtType *)pCtxt;
   pDevCtxt = pVAdcTMHalInterfaceCtxt->pDevCtxt;

   uRegisterAddress = uOffset & 0xFF;
   uRegisterAddress |= (pDevCtxt->pBsp->uPeripheralID << 8) & 0xFF00;

   result = DalSpmi_WriteLong(pDevCtxt->phSpmiDev,
                              pDevCtxt->pBsp->uSlaveId,
                              pDevCtxt->pBsp->eAccessPriority,
                              uRegisterAddress,
                              pucData,
                              uDataLen);

   if (result != DAL_SUCCESS)
   {
      VAdcTM_LogError(pDevCtxt, "VAdcTM - SPMI write failed", TRUE);
      return VADCTM_HAL_ERROR;
   }

   return VADCTM_HAL_SUCCESS;
}

/*======================================================================

  FUNCTION        VAdcTM_Sleep

  DESCRIPTION     This function sleeps for the specified time.

  DEPENDENCIES    None

  PARAMETERS      pDevCtxt  [in]
                  uDelay_us [in]

  RETURN VALUE    None

  SIDE EFFECTS    None

======================================================================*/
static void
VAdcTM_Sleep(
   VAdcTMDevCtxt *pDevCtxt,
   uint32 uDelay_us
   )
{
   DALResult result;
   uint32 uEventIdx;

   /* Reset the events */
   (void)DALSYS_EventCtrl(pDevCtxt->ahDelayEvents[VADCTM_EVENT_DEFAULT],
                          DALSYS_EVENT_CTRL_RESET);
   (void)DALSYS_EventCtrl(pDevCtxt->ahDelayEvents[VADCTM_EVENT_TIMEOUT],
                          DALSYS_EVENT_CTRL_RESET);

   /* Block sleep while waiting */
   if (pDevCtxt->hNPACpuLatency == NULL)
   {
      pDevCtxt->hNPACpuLatency = npa_create_sync_client("/core/cpu/latency/usec",
                                                        "VADCTM",
                                                         NPA_CLIENT_REQUIRED);
   }

   if (pDevCtxt->hNPACpuLatency != NULL)
   {
      npa_issue_required_request(pDevCtxt->hNPACpuLatency, 1);
   }

   /* Wait for the timeout to occur */
   result = DALSYS_EventMultipleWait(pDevCtxt->ahDelayEvents,
                                     _VADCTM_NUM_EVENTS,
                                     uDelay_us,
                                     &uEventIdx);

   if (pDevCtxt->hNPACpuLatency != NULL)
   {
      npa_complete_request(pDevCtxt->hNPACpuLatency);
   }

   if (result != DAL_ERROR_TIMEOUT)
   {
      VAdcTM_LogError(pDevCtxt, "VAdcTM - Timeout not working", FALSE);
   }

   return;
}

/*======================================================================

  FUNCTION        VAdcTM_Disable

  DESCRIPTION     This function disables the VADC TM and waits with
                  a timeout for the req bit to fall low in case
                  any conversions are currently taking place.

  DEPENDENCIES    None

  PARAMETERS      pDevCtxt [in]

  RETURN VALUE    None

  SIDE EFFECTS    None

======================================================================*/
static void
VAdcTM_Disable(
   VAdcTMDevCtxt *pDevCtxt
   )
{
   VAdcTMStatusType convStatus;
   VAdcTMHalResultType result;
   uint32 uDelay_us;
   uint32 uRetry;

   /* Set op mode to normal (needed for req_sts to go low)
    * Do NOT write 0 to MULTI_MEAS_EN here - keep the current channel selection
    * because this register acts as a peripheral disable. */
   result = VAdcTMHalSetOpMode(&pDevCtxt->iVAdcTMHalInterface,
                               VADCTM_OP_MODE_NORMAL);
   if (result != VADCTM_HAL_SUCCESS)
   {
      VAdcTM_LogError(pDevCtxt, "VAdcTM - Failed setting op mode", FALSE);
      return;
   }

   /* Disable the VADCTM */
   result = VAdcTMHalSetEnable(&pDevCtxt->iVAdcTMHalInterface,
                               VADCTM_DISABLE);
   if (result != VADCTM_HAL_SUCCESS)
   {
      VAdcTM_LogError(pDevCtxt, "VAdcTM - Disable failed", FALSE);
      return;
   }

   /* Re-enable the VADCTM or clock gating will prevent req_sts from going low */
   result = VAdcTMHalSetEnable(&pDevCtxt->iVAdcTMHalInterface,
                               VADCTM_ENABLE);
   if (result != VADCTM_HAL_SUCCESS)
   {
      VAdcTM_LogError(pDevCtxt, "VAdcTM - Enable failed", FALSE);
      return;
   }

   /* Make sure no ADC conversions are currently occuring - if one is then wait for it
    * to finnish */

   /* Get the conversion status */
   result = VAdcTMHalGetStatus(&pDevCtxt->iVAdcTMHalInterface,
                               &convStatus);
   if (result != VADCTM_HAL_SUCCESS)
   {
      VAdcTM_LogError(pDevCtxt, "VAdcTM - Get conv status failed", FALSE);
      return;
   }

   if (convStatus.eConversionStatus != VADCTM_CONVERSION_STATUS_COMPLETE)
   {
      uDelay_us = pDevCtxt->pBsp->uConversionTime_us * pDevCtxt->pBsp->uNumMeas;

      for (uRetry = 0; uRetry < VADCTM_DISABLE_NUM_RETRIES; uRetry++)
      {
         /* Wait for conversions to finish and recheck */
         VAdcTM_Sleep(pDevCtxt, uDelay_us);

         result = VAdcTMHalGetStatus(&pDevCtxt->iVAdcTMHalInterface,
                                     &convStatus);
         if (result != VADCTM_HAL_SUCCESS)
         {
            VAdcTM_LogError(pDevCtxt, "VAdcTM - Get conv status failed", FALSE);
            return;
         }

         if (convStatus.eConversionStatus == VADCTM_CONVERSION_STATUS_COMPLETE)
         {
            /* Switch back to interval mode */
            result = VAdcTMHalSetOpMode(&pDevCtxt->iVAdcTMHalInterface,
                                        VADCTM_OP_MODE_INTERVAL);
            if (result != VADCTM_HAL_SUCCESS)
            {
               VAdcTM_LogError(pDevCtxt, "VAdcTM - Failed setting op mode", FALSE);
               return;
            }

            /* Disable the VADCTM */
            result = VAdcTMHalSetEnable(&pDevCtxt->iVAdcTMHalInterface,
                                        VADCTM_DISABLE);
            if (result != VADCTM_HAL_SUCCESS)
            {
               VAdcTM_LogError(pDevCtxt, "VAdcTM - Disable failed", FALSE);
               return;
            }

            return;
         }
      }

      /* Conv req bit did not yet fall low */
      VAdcTM_LogError(pDevCtxt, "VAdcTM - req bit never fell low", FALSE);
   }

   /* Switch back to interval mode */
   result = VAdcTMHalSetOpMode(&pDevCtxt->iVAdcTMHalInterface,
                               VADCTM_OP_MODE_INTERVAL);
   if (result != VADCTM_HAL_SUCCESS)
   {
      VAdcTM_LogError(pDevCtxt, "VAdcTM - Failed setting op mode", FALSE);
      return;
   }

   /* Disable the VADCTM */
   result = VAdcTMHalSetEnable(&pDevCtxt->iVAdcTMHalInterface,
                               VADCTM_DISABLE);
   if (result != VADCTM_HAL_SUCCESS)
   {
      VAdcTM_LogError(pDevCtxt, "VAdcTM - Disable failed", FALSE);
      return;
   }

   /* Remove the vote for the LDO if applicable */
   if (pDevCtxt->hNPAPmicHkadc != NULL && pDevCtxt->bLdoVotedOn == TRUE)
   {
      npa_issue_required_request(pDevCtxt->hNPAPmicHkadc,
                                 PMIC_NPA_MODE_ID_HKADC_OFF);
      pDevCtxt->bLdoVotedOn = FALSE;
   }

   return;
}

/*======================================================================

  FUNCTION        VAdcTM_GetClientIdx

  DESCRIPTION     Gets the client index from the client ID. If the
                  client is not registered then this function will
                  attempt to register the client and assign an index.

  DEPENDENCIES    Callers need to have the device lock acquired.

  PARAMETERS
     pDevCtxt    [in]  device context
     uClientId   [in]  client ID
     puClientIdx [out] client index

  RETURN VALUE
     DAL_SUCCESS: success
     ADC_DEVICE_ERROR_OUT_OF_TM_CLIENTS: out of client handles

  SIDE EFFECTS    None

======================================================================*/
static DALResult
VAdcTM_GetClientIdx(
   VAdcTMDevCtxt *pDevCtxt,
   uint32 uClientId,
   uint32 *puClientIdx)
{
   VAdcTMMeasurementType *pMeas;
   uint32 uClientIdx;
   uint32 uMeas;
   uint32 uThresh;

   /* First check if the client is registered */
   for (uClientIdx = 0; uClientIdx < VADCTM_MAX_NUM_CLIENTS; uClientIdx++)
   {
      if (pDevCtxt->aClients[uClientIdx].eState == VADCTM_CLIENT_STATE_CLAIMED &&
          pDevCtxt->aClients[uClientIdx].uClientId == uClientId)
      {
         /* The client has been found */
         *puClientIdx = uClientIdx;

         return DAL_SUCCESS;
      }
   }

   /* Client needs to be registered */
   for (uClientIdx = 0; uClientIdx < VADCTM_MAX_NUM_CLIENTS; uClientIdx++)
   {
      if (pDevCtxt->aClients[uClientIdx].eState == VADCTM_CLIENT_STATE_UNCLAIMED)
      {
         /* Register the client here */
         pDevCtxt->aClients[uClientIdx].eState = VADCTM_CLIENT_STATE_CLAIMED;
         pDevCtxt->aClients[uClientIdx].uClientId = uClientId;
         pDevCtxt->aClients[uClientIdx].bEnabled = TRUE;

         for (uMeas = 0; uMeas < pDevCtxt->pBsp->uNumMeas; uMeas++)
         {
            pMeas = &pDevCtxt->aThresholds[uMeas];

            for (uThresh = 0; uThresh < ADC_DEVICE_TM_NUM_THRESHOLDS; uThresh++)
            {
               pMeas->aClientThresholds[uClientIdx][uThresh].eThresholdState = VADCTM_THRESHOLD_STATE_DISABLED;
            }
         }

         *puClientIdx = uClientIdx;

         return DAL_SUCCESS;
      }
   }

   return ADC_DEVICE_ERROR_OUT_OF_TM_CLIENTS;
}

/*======================================================================

  FUNCTION        VAdcTM_GetAdcCode

  DESCRIPTION     This function gets the raw ADC code.

  DEPENDENCIES    VADC

  PARAMETERS
     pDevCtxt        [in]
     uMeasIdx        [in]
     eThreshold      [in]
     nPhysical       [in]
     puCode           [out]
     pnCodeToPhysical [out]

  RETURN VALUE    DAL_SUCCESS or an error code

  SIDE EFFECTS    None

======================================================================*/
static DALResult
VAdcTM_GetAdcCode(
   VAdcTMDevCtxt *pDevCtxt,
   uint32 uMeasIdx,
   AdcDeviceTMThresholdType eThreshold,
   int32 nPhysical,
   uint32 *puCode,
   int32 *pnCodeToPhysical
   )
{
   VAdcClientCtxt *pVAdcClientCtxt = pDevCtxt->phVAdcDev->pClientCtxt;
   DALBOOL bPhysicalInverseToCode;
   AdcDeviceResultType vAdcResult;
   uint32 uIteration;
   uint32 uCodeIncrement;
   uint32 uCode;

   /*
    * The ADC code needs to result in a *pnCodeToPhysical that is:
    * - bPhysicalInverseToCode == FALSE
    *   -- ADC_DEVICE_TM_THRESHOLD_LOWER: CodeToPhysical(*puCode) <= nPhysical
    *   -- ADC_DEVICE_TM_THRESHOLD_HIGHER: CodeToPhysical(*puCode) >= nPhysical
    * - bPhysicalInverseToCode == TRUE
    *   -- ADC_DEVICE_TM_THRESHOLD_LOWER: CodeToPhysical(*puCode) >= nPhysical
    *   -- ADC_DEVICE_TM_THRESHOLD_HIGHER: CodeToPhysical(*puCode) <= nPhysical
    *
    * First, calculate a code that is close. Next, increment / decrement the code
    * until the physical value meets the above criteria.
    */
   bPhysicalInverseToCode = pDevCtxt->aThresholds[uMeasIdx].bPhysicalInverseToCode;

   /* Scale the physical value to code */
   VAdc_ProcessConversionResultInverse(pVAdcClientCtxt->pVAdcDevCtxt,
                                       pDevCtxt->aThresholds[uMeasIdx].uVAdcChannelIdx,
                                       &pDevCtxt->vAdcCalibData,
                                       nPhysical,
                                       &vAdcResult);

   if (vAdcResult.eStatus != ADC_DEVICE_RESULT_VALID)
   {
      /* Result cannot be scaled */
      VAdcTM_LogError(pDevCtxt, "VAdcTM - Failed calculating inverse", FALSE);

      return DAL_ERROR;
   }

   uCode = vAdcResult.uCode;
   uCodeIncrement = 0;
   *puCode = uCode;

   for (uIteration = 0; uIteration < 15; uIteration++)
   {
      /* Scale the code back to physcial to get the physical value being monitored */
      VAdc_ProcessConversionResult(pVAdcClientCtxt->pVAdcDevCtxt,
                                   pDevCtxt->aThresholds[uMeasIdx].uVAdcChannelIdx,
                                   &pDevCtxt->vAdcCalibData,
                                   *puCode,
                                   &vAdcResult);

      if (vAdcResult.eStatus != ADC_DEVICE_RESULT_VALID)
      {
         /* Result cannot be scaled */
         VAdcTM_LogError(pDevCtxt, "VAdcTM - Failed calculating physical from code", FALSE);

         return DAL_ERROR;
      }

      *pnCodeToPhysical = vAdcResult.nPhysical;

      if ((bPhysicalInverseToCode == FALSE && eThreshold == ADC_DEVICE_TM_THRESHOLD_LOWER  && *pnCodeToPhysical <= nPhysical) ||
          (bPhysicalInverseToCode == FALSE && eThreshold == ADC_DEVICE_TM_THRESHOLD_HIGHER && *pnCodeToPhysical >= nPhysical) ||
          (bPhysicalInverseToCode == TRUE  && eThreshold == ADC_DEVICE_TM_THRESHOLD_LOWER  && *pnCodeToPhysical >= nPhysical) ||
          (bPhysicalInverseToCode == TRUE  && eThreshold == ADC_DEVICE_TM_THRESHOLD_HIGHER && *pnCodeToPhysical <= nPhysical))
      {
         /* Physical value is OK and does not need adjusted */
         return DAL_SUCCESS;
      }
      else
      {
         /* Physical value needs adjusted - use 2^N adjustment because 1 code is only 97 uV
            and thresholds won't converge without a lot of iterations */
         if (uCodeIncrement == 0)
         {
            uCodeIncrement = 1;
         }
         else
         {
            uCodeIncrement *= 2;
         }

         if (eThreshold == ADC_DEVICE_TM_THRESHOLD_LOWER)
         {
            /* Decrement code */
            *puCode = uCode - uCodeIncrement;
         }
         else
         {
            /* Increment code */
            *puCode = uCode + uCodeIncrement;
         }
      }
   }

   if ((bPhysicalInverseToCode == FALSE && eThreshold == ADC_DEVICE_TM_THRESHOLD_LOWER  && *pnCodeToPhysical <= nPhysical) ||
       (bPhysicalInverseToCode == FALSE && eThreshold == ADC_DEVICE_TM_THRESHOLD_HIGHER && *pnCodeToPhysical >= nPhysical) ||
       (bPhysicalInverseToCode == TRUE  && eThreshold == ADC_DEVICE_TM_THRESHOLD_LOWER  && *pnCodeToPhysical >= nPhysical) ||
       (bPhysicalInverseToCode == TRUE  && eThreshold == ADC_DEVICE_TM_THRESHOLD_HIGHER && *pnCodeToPhysical <= nPhysical))
   {
      /* Physical value is OK */
      return DAL_SUCCESS;
   }

   /* Acceptable physical value not found */
   VAdcTM_LogError(pDevCtxt, "VAdcTM - Failed calculating physical", FALSE);

   return DAL_ERROR;
}

/*======================================================================

  FUNCTION        VAdcTM_UpdateThresholds

  DESCRIPTION     This function is to update the thresholds.

  DEPENDENCIES    VADCTM is disabled & the threshold list lock is acquired

  PARAMETERS
     pDevCtxt [in] device context
     bReArm   [in] Rearm VADC TM if TRUE

  RETURN VALUE    DAL_SUCCESS or an error code

  SIDE EFFECTS    None

======================================================================*/
static DALResult
VAdcTM_UpdateThresholds(
   VAdcTMDevCtxt *pDevCtxt,
   DALBOOL bReArm
   )
{
   VAdcTMThresholdMaskType uMeasEnableMask = 0;
   VAdcTMThresholdMaskType uLowerEnableMask = 0;
   VAdcTMThresholdMaskType uHigherEnableMask = 0;
   VAdcTMHalResultType result;
   uint32 uMeas, uThresh, uClient;
   uint32 uNumThresholdsSet = 0;
   DALBOOL bThresholdEnabled;
   VAdcTMThresholdRequestType *pThresholdRequest;
   uint32 uCode = 0;

   /* Note: TM is already disabled & we already have the lock */

   /* If processing a threshold event do not update */
   if (pDevCtxt->bProcessingThresholdEvent == TRUE)
   {
      return DAL_SUCCESS;
   }

   /* For each threshold, set the max of the lowers & min of the highers */
   for (uMeas = 0; uMeas < pDevCtxt->pBsp->uNumMeas; uMeas++)
   {
      for (uThresh = 0; uThresh < ADC_DEVICE_TM_NUM_THRESHOLDS; uThresh++)
      {
         bThresholdEnabled = FALSE;

         for (uClient = 0; uClient < VADCTM_MAX_NUM_CLIENTS; uClient++)
         {
            pThresholdRequest = &pDevCtxt->aThresholds[uMeas].aClientThresholds[uClient][uThresh];

            if (pDevCtxt->aClients[uClient].eState == VADCTM_CLIENT_STATE_CLAIMED &&
                pDevCtxt->aClients[uClient].bEnabled == TRUE &&
                (pThresholdRequest->eThresholdState == VADCTM_THRESHOLD_STATE_THRESHOLD ||
                 pThresholdRequest->eThresholdState == VADCTM_THRESHOLD_STATE_TOLERANCE))
            {
               /* Special case for the first client to register a threshold */
               if (bThresholdEnabled == FALSE)
               {
                  uCode = pThresholdRequest->uCode;

                  bThresholdEnabled = TRUE;
               }
               else
               {
                  switch ((AdcDeviceTMThresholdType)uThresh)
                  {
                     case ADC_DEVICE_TM_THRESHOLD_LOWER:
                        /* Need the max */
                        if (pThresholdRequest->uCode > uCode)
                        {
                           uCode = pThresholdRequest->uCode;
                        }
                        break;
                     case ADC_DEVICE_TM_THRESHOLD_HIGHER:
                        /* Need the min */
                        if (pThresholdRequest->uCode < uCode)
                        {
                           uCode = pThresholdRequest->uCode;
                        }
                        break;
                     default:
                        return DAL_ERROR;
                  }
               }
            }
         }

         if (bThresholdEnabled == TRUE)
         {
            uNumThresholdsSet++;

            /* Write code & enable threshold */
            uMeasEnableMask |= 1 << uMeas;

            switch ((AdcDeviceTMThresholdType)uThresh)
            {
               case ADC_DEVICE_TM_THRESHOLD_LOWER:
                  uLowerEnableMask |= 1 << uMeas;

                  result = VAdcTMHalSetLowThresholdCode(&pDevCtxt->iVAdcTMHalInterface,
                                                        uMeas,
                                                        uCode);
                  if (result != VADCTM_HAL_SUCCESS)
                  {
                     VAdcTM_LogError(pDevCtxt, "VAdcTM - Set low thresh code failed", FALSE);
                     return DAL_ERROR;
                  }

                  break;
               case ADC_DEVICE_TM_THRESHOLD_HIGHER:
                  uHigherEnableMask |= 1 << uMeas;

                  result = VAdcTMHalSetHighThresholdCode(&pDevCtxt->iVAdcTMHalInterface,
                                                         uMeas,
                                                         uCode);
                  if (result != VADCTM_HAL_SUCCESS)
                  {
                     VAdcTM_LogError(pDevCtxt, "VAdcTM - Set high thresh code failed", FALSE);
                     return DAL_ERROR;
                  }

                  break;
               default:
                  return DAL_ERROR;
            }
         }
      }
   }

   /* Enable the sensors and thresholds */
   result = VAdcTMHalSetMeasEnable(&pDevCtxt->iVAdcTMHalInterface,
                                   uMeasEnableMask);
   if (result != VADCTM_HAL_SUCCESS)
   {
      VAdcTM_LogError(pDevCtxt, "VAdcTM - Set meas enable failed", FALSE);
      return DAL_ERROR;
   }

   result = VAdcTMHalSetLowThrIntEnable(&pDevCtxt->iVAdcTMHalInterface,
                                        uLowerEnableMask);
   if (result != VADCTM_HAL_SUCCESS)
   {
      VAdcTM_LogError(pDevCtxt, "VAdcTM - Set low int failed", FALSE);
      return DAL_ERROR;
   }

   result = VAdcTMHalSetHighThrIntEnable(&pDevCtxt->iVAdcTMHalInterface,
                                         uHigherEnableMask);
   if (result != VADCTM_HAL_SUCCESS)
   {
      VAdcTM_LogError(pDevCtxt, "VAdcTM - Set high int failed", FALSE);
      return DAL_ERROR;
   }

   /* Rearm the device */
   if (bReArm == TRUE && uNumThresholdsSet > 0)
   {
      /* Vote for the LDO if applicable */
      if (pDevCtxt->hNPAPmicHkadc != NULL && pDevCtxt->bLdoVotedOn == FALSE)
      {
         npa_issue_required_request(pDevCtxt->hNPAPmicHkadc,
                                    PMIC_NPA_MODE_ID_HKADC_ACTIVE);
         pDevCtxt->bLdoVotedOn = TRUE;
      }

      /* Enable the VADC TM */
      result = VAdcTMHalSetEnable(&pDevCtxt->iVAdcTMHalInterface,
                                  VADCTM_ENABLE);
      if (result != VADCTM_HAL_SUCCESS)
      {
         VAdcTM_LogError(pDevCtxt, "VAdcTM - Enable failed", FALSE);
         return DAL_ERROR;
      }

      /* Request conversion strobe */
      result = VAdcTMHalRequestConversion(&pDevCtxt->iVAdcTMHalInterface);
      if (result != VADCTM_HAL_SUCCESS)
      {
         VAdcTM_LogError(pDevCtxt, "VAdcTM - Req conv failed", FALSE);
         return DAL_ERROR;
      }
   }

   return DAL_SUCCESS;
}

static DALResult
VAdcTM_ConfigTolerance(
   VAdcTMDevCtxt *pDevCtxt,
   uint32 uClientIdx,
   uint32 uMeasIdx,
   AdcDeviceTMThresholdType eThreshold,
   const DALSYSEventHandle hEvent,
   const int32 *pnTolerance,
   const int32 *pnCurrentValue
   )
{
   VAdcTMMeasurementType *pMeasurement;
   VAdcTMThresholdRequestType *pThresholdRequest;
   int32 nThresholdSet;
   int32 nThreshold = 0;
   DALResult result;
   uint32 uCode;

   pMeasurement = &pDevCtxt->aThresholds[uMeasIdx];
   pThresholdRequest = &pMeasurement->aClientThresholds[uClientIdx][eThreshold];

   if (pnTolerance != NULL && pnCurrentValue != NULL)
   {
      pThresholdRequest->nCurrentValue = *pnCurrentValue;

      if ((eThreshold == ADC_DEVICE_TM_THRESHOLD_LOWER && pMeasurement->bPhysicalInverseToCode == FALSE) ||
          (eThreshold == ADC_DEVICE_TM_THRESHOLD_HIGHER && pMeasurement->bPhysicalInverseToCode == TRUE))
      {
         nThreshold = *pnCurrentValue - *pnTolerance;
         if (nThreshold < pMeasurement->nPhysical_min)
         {
            nThreshold = pMeasurement->nPhysical_min;
         }
      }
      else
      {
         nThreshold = *pnCurrentValue + *pnTolerance;
         if (nThreshold > pMeasurement->nPhysical_max)
         {
            nThreshold = pMeasurement->nPhysical_max;
         }
      }
   }
   else
   {
      pThresholdRequest->eThresholdState = VADCTM_THRESHOLD_STATE_DISABLED;
      return DAL_SUCCESS;
   }

   result = VAdcTM_GetAdcCode(pDevCtxt,
                              uMeasIdx,
                              eThreshold,
                              nThreshold,
                              &uCode,
                              &nThresholdSet);
   if (result != DAL_SUCCESS)
   {
      VAdcTM_LogError(pDevCtxt, "VAdcTM - failed getting new code on config tolerance", FALSE);
      pThresholdRequest->eThresholdState = VADCTM_THRESHOLD_STATE_DISABLED;
      return result;
   }

   /* Set the new threshold */
   pThresholdRequest->eThresholdState = VADCTM_THRESHOLD_STATE_TOLERANCE;
   pThresholdRequest->hEvent = hEvent;
   pThresholdRequest->nPhysicalDesired = nThreshold;
   pThresholdRequest->nPhysicalMonitored = nThresholdSet;
   pThresholdRequest->uCode = uCode;
   pThresholdRequest->nTolerance = *pnTolerance;

   return DAL_SUCCESS;
}

static DALResult
VAdcTM_SlideToleranceThresholds(
   VAdcTMDevCtxt *pDevCtxt,
   uint32 uClientIdx,
   uint32 uMeasIdx,
   int32 nCurrentValue
   )
{
   VAdcTMThresholdRequestType *pThresholdRequest;
   AdcDeviceTMThresholdType eThreshold;
   VAdcTMMeasurementType *pMeasurement;
   DALResult result;

   for (eThreshold = (AdcDeviceTMThresholdType)0; eThreshold < ADC_DEVICE_TM_NUM_THRESHOLDS; eThreshold++)
   {
      pMeasurement = &pDevCtxt->aThresholds[uMeasIdx];
      pThresholdRequest = &pMeasurement->aClientThresholds[uClientIdx][eThreshold];

      if (pThresholdRequest->eThresholdState == VADCTM_THRESHOLD_STATE_TOLERANCE)
      {
         result = VAdcTM_ConfigTolerance(pDevCtxt,
                                         uClientIdx,
                                         uMeasIdx,
                                         eThreshold,
                                         pThresholdRequest->hEvent,
                                         &pThresholdRequest->nTolerance,
                                         &nCurrentValue);
         if (result != DAL_SUCCESS)
         {
            return result;
         }
      }
   }

   return DAL_SUCCESS;
}

/*======================================================================

  FUNCTION        VAdcTM_ServiceThresholdEventsInWorkLoop

  DESCRIPTION
      This function services threshold events in a workloop.

  DEPENDENCIES    None

  PARAMETERS
      hEvent     [in]
      pInDevCtxt [in]

  RETURN VALUE    Infinite loop

  SIDE EFFECTS    Completes the client notification

======================================================================*/
static DALResult
VAdcTM_ServiceThresholdEventsInWorkLoop(
   DALSYSEventHandle hEvent,
   void *pInDevCtxt
   )
{
   VAdcTMDevCtxt *pDevCtxt = pInDevCtxt;
   VAdcClientCtxt *pVAdcClientCtxt = pDevCtxt->phVAdcDev->pClientCtxt;
   VAdcTMHalResultType status;
   VAdcTMThresholdRequestType *pThresholdRequest;
   AdcDeviceResultType vAdcResult;
   uint32 uMeas, uThresh, uClient;
   VAdcTMStatusType convStatus;
   VAdcTMConversionCodeType uCode;
   VAdcTMThresholdMaskType thresholdMask;
   DALBOOL bTriggered;
   AdcTMCallbackPayloadType adcTMCallbackPayload;  /* From DDIAdc.h */
   DALResult result;

   DALSYS_EventCtrl(pDevCtxt->hSignalEvent, DALSYS_EVENT_CTRL_ACCQUIRE_OWNERSHIP);

   for ( ; ; )
   {
      /* Wait on the interrupt event */
      (void)DALSYS_EventWait(pDevCtxt->hSignalEvent);

      /* Acquire lock */
      DALSYS_SyncEnter(pDevCtxt->hSync);

      pDevCtxt->bProcessingThresholdEvent = TRUE;

      /* Disable TM */
      VAdcTM_Disable(pDevCtxt);

      /* Reset signal */
      DALSYS_EventCtrl(pDevCtxt->hSignalEvent, DALSYS_EVENT_CTRL_RESET);

      /* Clear the interrupt */
      status = VAdcTMHalClearInterrupts(&pDevCtxt->iVAdcTMHalInterface,
                                        (VADCTM_INTERRUPT_MASK_BIT_HIGH_THRESHOLD |
                                         VADCTM_INTERRUPT_MASK_BIT_LOW_THRESHOLD));
      if (status != VADCTM_HAL_SUCCESS)
      {
         VAdcTM_LogError(pDevCtxt, "VAdcTM - failed to clear interrupt", FALSE);
      }

      /* Get the conversion result */
      status = VAdcTMHalGetStatus(&pDevCtxt->iVAdcTMHalInterface, &convStatus);
      if (status != VADCTM_HAL_SUCCESS)
      {
         /* This should never happen */
         VAdcTM_LogError(pDevCtxt, "VAdcTM - Failed to get conv result", TRUE);
         goto updateThreshReleaseLockCont;
      }

      /* Determine which thresholds are crossed */
      for (uMeas = 0; uMeas < pDevCtxt->pBsp->uNumMeas; uMeas++)
      {
         if ((convStatus.lowThresholdStatus | convStatus.highThresholdStatus) & (1 << uMeas))
         {
            /* Get code */
            status = VAdcTMHalGetConversionCode(&pDevCtxt->iVAdcTMHalInterface,
                                                uMeas,
                                                &uCode);
            if (status != VADCTM_HAL_SUCCESS)
            {
               /* This should never happen */
               VAdcTM_LogError(pDevCtxt, "VAdcTM - Failed to get conv code", TRUE);
               goto updateThreshReleaseLockCont;
            }

            /* Scale to physical */
            VAdc_ProcessConversionResult(pVAdcClientCtxt->pVAdcDevCtxt,
                                         pDevCtxt->aThresholds[uMeas].uVAdcChannelIdx,
                                         &pDevCtxt->vAdcCalibData,
                                         uCode,
                                         &vAdcResult);
         }
         else
         {
            continue;
         }

         for (uThresh = 0; uThresh < ADC_DEVICE_TM_NUM_THRESHOLDS; uThresh++)
         {
            switch ((AdcDeviceTMThresholdType)uThresh)
            {
               case ADC_DEVICE_TM_THRESHOLD_LOWER:
                  thresholdMask = convStatus.lowThresholdStatus;
                  break;
               case ADC_DEVICE_TM_THRESHOLD_HIGHER:
                  thresholdMask = convStatus.highThresholdStatus;
                  break;
               default:
                  thresholdMask = 0;
            }

            if (thresholdMask & (1 << uMeas))
            {
               /* Check each client - if they triggered complete the request */
               for (uClient = 0; uClient < VADCTM_MAX_NUM_CLIENTS; uClient++)
               {
                  if (pDevCtxt->aClients[uClient].eState != VADCTM_CLIENT_STATE_CLAIMED ||
                      pDevCtxt->aClients[uClient].bEnabled != TRUE)
                  {
                     continue;
                  }

                  pThresholdRequest = &pDevCtxt->aThresholds[uMeas].aClientThresholds[uClient][uThresh];

                  if (pThresholdRequest->eThresholdState == VADCTM_THRESHOLD_STATE_THRESHOLD ||
                      pThresholdRequest->eThresholdState == VADCTM_THRESHOLD_STATE_TOLERANCE)
                  {
                     bTriggered = FALSE;

                     switch ((AdcDeviceTMThresholdType)uThresh)
                     {
                        case ADC_DEVICE_TM_THRESHOLD_LOWER:

                           if (vAdcResult.uCode <= pThresholdRequest->uCode)
                           {
                              if (pDevCtxt->aThresholds[uMeas].bPhysicalInverseToCode == FALSE)
                              {
                                 adcTMCallbackPayload.eThresholdTriggered = ADC_TM_THRESHOLD_LOWER;
                              }
                              else
                              {
                                 adcTMCallbackPayload.eThresholdTriggered = ADC_TM_THRESHOLD_HIGHER;
                              }

                              bTriggered = TRUE;
                           }

                           break;

                        case ADC_DEVICE_TM_THRESHOLD_HIGHER:

                           if (vAdcResult.uCode >= pThresholdRequest->uCode)
                           {
                              if (pDevCtxt->aThresholds[uMeas].bPhysicalInverseToCode == FALSE)
                              {
                                 adcTMCallbackPayload.eThresholdTriggered = ADC_TM_THRESHOLD_HIGHER;
                              }
                              else
                              {
                                 adcTMCallbackPayload.eThresholdTriggered = ADC_TM_THRESHOLD_LOWER;
                              }

                              bTriggered = TRUE;
                           }

                           break;

                        default:
                           bTriggered = FALSE;
                     }

                     if (bTriggered == TRUE)
                     {
                        if (pThresholdRequest->eThresholdState == VADCTM_THRESHOLD_STATE_THRESHOLD)
                        {
                           /* Mark as triggered */
                           pThresholdRequest->eThresholdState = VADCTM_THRESHOLD_STATE_TRIGGERED;
                        }
                        else
                        {
                           /* Update tolerances */
                           result = VAdcTM_SlideToleranceThresholds(pDevCtxt,
                                                                    uClient,
                                                                    uMeas,
                                                                    vAdcResult.nPhysical);
                           if (result != DAL_SUCCESS)
                           {
                              VAdcTM_LogError(pDevCtxt, "VAdcTM - failed to slide threshold tolerances", FALSE);
                           }
                        }

                        /* Release lock */
                        DALSYS_SyncLeave(pDevCtxt->hSync);

                        /* Set the payload */
                        adcTMCallbackPayload.uTMChannelIdx = uMeas;
                        adcTMCallbackPayload.nPhysicalTriggered = vAdcResult.nPhysical;

                        /* Trigger the event */
                        DALSYS_EventCtrlEx(pThresholdRequest->hEvent,
                                           DALSYS_EVENT_CTRL_TRIGGER,
                                           NULL,
                                           (void *)&adcTMCallbackPayload,
                                           sizeof(adcTMCallbackPayload));

                        /* Acquire lock */
                        DALSYS_SyncEnter(pDevCtxt->hSync);
                     }
                  }
               }
            }
         }
      }

updateThreshReleaseLockCont:
      pDevCtxt->bProcessingThresholdEvent = FALSE;

      /* Update thresholds and arm */
      (void)VAdcTM_UpdateThresholds(pDevCtxt, TRUE);

      /* Release lock */
      DALSYS_SyncLeave(pDevCtxt->hSync);
   }
}

/*======================================================================

  FUNCTION        VAdcTM_InterruptCb

  DESCRIPTION
      This function is the callback to process the threshold interrupt.

  DEPENDENCIES    None

  PARAMETERS
      pArg            [in]
      uIntrStatusMask [in]

  RETURN VALUE    None

  SIDE EFFECTS    None

======================================================================*/
static void*
VAdcTM_InterruptCb(
   void *pArg,
   uint32 uIntrStatusMask
   )
{
   VAdcTMDevCtxt *pDevCtxt = pArg;

   DALSYS_EventCtrl(pDevCtxt->hSignalEvent, DALSYS_EVENT_CTRL_TRIGGER);

   return NULL;
}

/*----------------------------------------------------------------------------
 * Externalized Function Definitions
 * -------------------------------------------------------------------------*/
DALResult
VAdcTM_Init(
   VAdcTMDevCtxt **ppDevCtxt,
   DalDeviceHandle *phVAdcDev
   )
{
   VAdcTMDevCtxt *pDevCtxt;
   VAdcClientCtxt *pVAdcClientCtxt = phVAdcDev->pClientCtxt;
   VAdcTMMeasurementType *pMeasurement;
   DALResult ret;
   DALSYSPropertyVar propVar;
   VAdcTMHalResultType result;
   VAdcTMInterruptsConfigType interruptConfig;
   VAdcTMConversionParametersType convParams;
   VAdcTMMeasIntervalCtlType measIntervalCtl;
   const VAdcTMMeasConfigType *pMeasConfig;
   VAdcTMAmuxChannelSelectType uHwChannel;
   pm_err_flag_type pmResult;
   uint32 uMeasIdx;
   uint32 uClientIdx;
   AdcDeviceResultType adcResult;
   int32 nPhysical_min;
   int32 nPhysical_max;
   uint16 uPerph;

   if (guNumDevices < VADCTM_MAX_NUM_DEVICES)
   {
      *ppDevCtxt = &gaVAdcTMDevCtxt[guNumDevices];
      pDevCtxt = *ppDevCtxt;
      guNumDevices++;
   }
   else
   {
      return DAL_ERROR;
   }

   /* Initialize VADC TM context */
   DALSYS_memset(pDevCtxt, 0, sizeof(*pDevCtxt));
   pDevCtxt->bHardwareSupported = TRUE;
   pDevCtxt->devId = pVAdcClientCtxt->pVAdcDevCtxt->DevId;
   pDevCtxt->bLdoVotedOn = FALSE;
   pDevCtxt->phVAdcDev = phVAdcDev;

   // Set all thresholds to disabled
   for (uMeasIdx = 0; uMeasIdx < VADCTM_MAX_NUM_MEAS; uMeasIdx++)
   {
      pMeasurement = &pDevCtxt->aThresholds[uMeasIdx];
      for (uClientIdx = 0; uClientIdx < VADCTM_MAX_NUM_CLIENTS; uClientIdx++)
      {
         pMeasurement->aClientThresholds[uClientIdx][ADC_DEVICE_TM_THRESHOLD_LOWER].eThresholdState =
            VADCTM_THRESHOLD_STATE_DISABLED;
         pMeasurement->aClientThresholds[uClientIdx][ADC_DEVICE_TM_THRESHOLD_HIGHER].eThresholdState =
            VADCTM_THRESHOLD_STATE_DISABLED;
      }
   }

   // Set all clients to unclaimed
   for (uClientIdx = 0; uClientIdx < VADCTM_MAX_NUM_CLIENTS; uClientIdx++)
   {
      pDevCtxt->aClients[uClientIdx].eState = VADCTM_CLIENT_STATE_UNCLAIMED;
   }

   /* Read the DAL properties */
   ret = DALSYS_GetPropertyValue(pVAdcClientCtxt->pVAdcDevCtxt->hProp,
                                 "VADCTM_BSP",
                                 0,
                                 &propVar);
   if (ret != DAL_SUCCESS)
   {
      VAdcTM_LogError(pDevCtxt, "VAdcTM - failed to get BSP", TRUE);
      return ret;
   }

   pDevCtxt->pBsp = (VAdcTMBspType *)propVar.Val.pStruct;

   /* Get the PMIC device info */
   pmResult = pm_get_pmic_info((uint8)pDevCtxt->pBsp->uPmicDevice,
                               &pDevCtxt->pmicDeviceInfo);
   if (pmResult != PM_ERR_FLAG__SUCCESS)
   {
      VAdcTM_LogError(pDevCtxt, "VAdcTM - failed to get PMIC device info", TRUE);
      return DAL_ERROR;
   }

   /* Create NPA clients */
   pDevCtxt->hNPACpuLatency = npa_create_sync_client("/core/cpu/latency/usec",
                                                     "VADCTM",
                                                     NPA_CLIENT_REQUIRED);

   if (DALCHIPINFO_FAMILY_MDM9x45 == DalChipInfo_ChipFamily())
   {
      pDevCtxt->hNPAPmicHkadc = npa_create_sync_client(PMIC_NPA_GROUP_ID_HKADC,
                                                       "VADCTM",
                                                       NPA_CLIENT_REQUIRED);
   }
   else
   {
      pDevCtxt->hNPAPmicHkadc = NULL;
   }

   /* Attach to to the SPMI driver */
   ret = DAL_SpmiDeviceAttach("DALDEVICEID_SPMI_DEVICE", &pDevCtxt->phSpmiDev);
   if (ret != DAL_SUCCESS)
   {
      VAdcTM_LogError(pDevCtxt, "VAdcTM - failed to attach to SPMI", TRUE);
      return ret;
   }

   /* Open the SPMI driver */
   ret = DalDevice_Open(pDevCtxt->phSpmiDev,
                        DAL_OPEN_SHARED | DAL_OPEN_READ | DAL_OPEN_WRITE);
   if (ret != DAL_SUCCESS)
   {
      VAdcTM_LogError(pDevCtxt, "VAdcTM - failed to open SPMI", TRUE);
      return ret;
   }

   /* Initialize the HAL interface */
   pDevCtxt->vAdcTMHalInterfaceCtxt.pDevCtxt = pDevCtxt;
   pDevCtxt->iVAdcTMHalInterface.pCtxt = (void *)&pDevCtxt->vAdcTMHalInterfaceCtxt;
   pDevCtxt->iVAdcTMHalInterface.pfnWriteBytes = VAdcTM_WriteBytes;
   pDevCtxt->iVAdcTMHalInterface.pfnReadBytes = VAdcTM_ReadBytes;

   /* Log the revision & peripheral type */
   result = VAdcTMHalGetRevisionInfo(&pDevCtxt->iVAdcTMHalInterface,
                                     &pDevCtxt->revisionInfo);
   if (result != VADCTM_HAL_SUCCESS)
   {
      return DAL_ERROR;
   }

   // Sanity check the peripheral type & subtype
   if (pDevCtxt->pBsp->uPerphType != pDevCtxt->revisionInfo.uType)
   {
      VAdcTM_LogError(pDevCtxt, "VAdcTM - Invalid VADC peripheral type", TRUE);
      pDevCtxt->bHardwareSupported = FALSE;
   }

   // Check the digital version information
   if (pDevCtxt->revisionInfo.uDigitalMajor < pDevCtxt->pBsp->uMinDigMajor)
   {
      VAdcTM_LogError(pDevCtxt, "VAdcTM - Invalid HW - dig major", TRUE);
      pDevCtxt->bHardwareSupported = FALSE;
   }
   else if (pDevCtxt->revisionInfo.uDigitalMajor == pDevCtxt->pBsp->uMinDigMajor)
   {
      if (pDevCtxt->revisionInfo.uDigitalMinor < pDevCtxt->pBsp->uMinDigMinor)
      {
         VAdcTM_LogError(pDevCtxt, "VAdcTM - Invalid HW - dig minor", TRUE);
         pDevCtxt->bHardwareSupported = FALSE;
      }
   }

   // Check the analog version information
   if (pDevCtxt->revisionInfo.uAnalogMajor < pDevCtxt->pBsp->uMinAnaMajor)
   {
      VAdcTM_LogError(pDevCtxt, "VAdcTM - Invalid HW - ana major", TRUE);
      pDevCtxt->bHardwareSupported = FALSE;
   }
   else if (pDevCtxt->revisionInfo.uAnalogMajor == pDevCtxt->pBsp->uMinAnaMajor)
   {
      if (pDevCtxt->revisionInfo.uAnalogMinor < pDevCtxt->pBsp->uMinAnaMinor)
      {
         VAdcTM_LogError(pDevCtxt, "VAdcTM - Invalid HW - ana minor", TRUE);
         pDevCtxt->bHardwareSupported = FALSE;
      }
   }

   if (pDevCtxt->bHardwareSupported == FALSE)
   {
      return DAL_SUCCESS;
   }

   // Get the calibration data
   VAdc_GetCalibration(pVAdcClientCtxt->pVAdcDevCtxt,
                       pDevCtxt->pBsp->uVAdcConfigIdx,
                       &pDevCtxt->vAdcCalibData);

   // Get the VAdc channel for each measurement and determine the range
   for (uMeasIdx = 0; uMeasIdx < pDevCtxt->pBsp->uNumMeas; uMeasIdx++)
   {
      pMeasConfig = &pDevCtxt->pBsp->paMeasConfig[uMeasIdx];

      // Get the channel
      ret = DalAdcDevice_GetInputProperties(pDevCtxt->phVAdcDev,
                                            pMeasConfig->pName,
                                            &pDevCtxt->aThresholds[uMeasIdx].uVAdcChannelIdx);

      if (ret != DAL_SUCCESS)
      {
         VAdcTM_LogError(pDevCtxt, "VAdcTM - Could not get channel from VAdc", TRUE);
         return ret;
      }


      // Get the range
      VAdc_ProcessConversionResultFromMilliVolts(pVAdcClientCtxt->pVAdcDevCtxt,
                                                 pDevCtxt->aThresholds[uMeasIdx].uVAdcChannelIdx,
                                                 &pDevCtxt->vAdcCalibData,
                                                 pDevCtxt->pBsp->nThresholdMin_mV,
                                                 &adcResult);

      if (adcResult.eStatus != ADC_DEVICE_RESULT_VALID)
      {
         VAdcTM_LogError(pDevCtxt, "VAdcTM - Could not get TM range", TRUE);
         return DAL_ERROR;
      }

      nPhysical_min = adcResult.nPhysical;

      VAdc_ProcessConversionResultFromMilliVolts(pVAdcClientCtxt->pVAdcDevCtxt,
                                                 pDevCtxt->aThresholds[uMeasIdx].uVAdcChannelIdx,
                                                 &pDevCtxt->vAdcCalibData,
                                                 pDevCtxt->pBsp->nThresholdMax_mV,
                                                 &adcResult);

      if (adcResult.eStatus != ADC_DEVICE_RESULT_VALID)
      {
         VAdcTM_LogError(pDevCtxt, "VAdcTM - Could not get TM range", TRUE);
         return DAL_ERROR;
      }

      nPhysical_max = adcResult.nPhysical;

      if (nPhysical_max > nPhysical_min)
      {
         pDevCtxt->aThresholds[uMeasIdx].nPhysical_min = nPhysical_min;
         pDevCtxt->aThresholds[uMeasIdx].nPhysical_max = nPhysical_max;
         pDevCtxt->aThresholds[uMeasIdx].bPhysicalInverseToCode = FALSE;
      }
      else
      {
         pDevCtxt->aThresholds[uMeasIdx].nPhysical_min = nPhysical_max;
         pDevCtxt->aThresholds[uMeasIdx].nPhysical_max = nPhysical_min;
         pDevCtxt->aThresholds[uMeasIdx].bPhysicalInverseToCode = TRUE;
      }
   }

   // Initialize synchronization object
   ret = DALSYS_SyncCreate(DALSYS_SYNC_ATTR_RESOURCE,
                           &pDevCtxt->hSync,
                           &pDevCtxt->syncObject);
   if (ret != DAL_SUCCESS)
   {
      VAdcTM_LogError(pDevCtxt, "VAdcTM - failed to create the sync object", TRUE);
      return ret;
   }

   /* Create events to be used to create a delay */
   ret = DALSYS_EventCreate(DALSYS_EVENT_ATTR_CLIENT_DEFAULT,
                            &pDevCtxt->ahDelayEvents[VADCTM_EVENT_DEFAULT],
                            NULL);
   if (ret != DAL_SUCCESS)
   {
       VAdcTM_LogError(pDevCtxt, "VAdcTM - failed to create delay event", TRUE);
      return ret;
   }

   ret = DALSYS_EventCreate(DALSYS_EVENT_ATTR_TIMEOUT_EVENT,
                            &pDevCtxt->ahDelayEvents[VADCTM_EVENT_TIMEOUT],
                            NULL);
   if (ret != DAL_SUCCESS)
   {
      VAdcTM_LogError(pDevCtxt, "VAdcTM - failed to create delay timeout event", TRUE);

      return ret;
   }

   /* Create a work loop to wait on threshold events */
   ret = DALSYS_RegisterWorkLoopEx("VADCTM_WORK_LOOP",
                                   VADCTM_DEFAULT_STACK_SIZE,
                                   gVAdcTMWorkloopPriority,
                                   10,
                                   &pDevCtxt->hThresholdWorkLoop,
                                   NULL);
   if (ret != DAL_SUCCESS)
   {
      VAdcTM_LogError(pDevCtxt, "VAdcTM - failed to create workloop", TRUE);
      return ret;
   }

   /* Create a workloop event */
   ret = DALSYS_EventCreate(DALSYS_EVENT_ATTR_WORKLOOP_EVENT,
                            &pDevCtxt->hWorkLoopEvent,
                            NULL);
   if (ret != DAL_SUCCESS)
   {
      VAdcTM_LogError(pDevCtxt, "VAdcTM - failed to create workloop event", TRUE);
      return ret;
   }

   /* Add the event to the workloop */
   ret = DALSYS_AddEventToWorkLoop(pDevCtxt->hThresholdWorkLoop,
                                   VAdcTM_ServiceThresholdEventsInWorkLoop,
                                   pDevCtxt,
                                   pDevCtxt->hWorkLoopEvent,
                                   NULL);
   if (ret != DAL_SUCCESS)
   {
      VAdcTM_LogError(pDevCtxt, "VAdcTM - failed to add event to workloop", TRUE);
      return ret;
   }

   // Register interrupts
   uPerph = ((pDevCtxt->pBsp->uSlaveId << 8) & 0xF00) | (pDevCtxt->pBsp->uPeripheralID & 0xFF);

   ret = DalSpmi_RegisterIsr(pDevCtxt->phSpmiDev,
                             uPerph,
                             (uint32)(VADCTM_INTERRUPT_MASK_BIT_HIGH_THRESHOLD |
                                      VADCTM_INTERRUPT_MASK_BIT_LOW_THRESHOLD),
                             VAdcTM_InterruptCb,
                             pDevCtxt);
   if (ret != DAL_SUCCESS)
   {
      VAdcTM_LogError(pDevCtxt, "VAdcTM - Failed to register ISR", TRUE);
      return ret;
   }

   // Configure peripheral interrupts
   interruptConfig.eHighThresholdInterruptConfig = VADCTM_INTERRUPT_CONFIG_RISING_EDGE;
   interruptConfig.eLowThresholdInterruptConfig = VADCTM_INTERRUPT_CONFIG_RISING_EDGE;
   interruptConfig.eEocInterruptConfig = VADCTM_INTERRUPT_CONFIG_NONE;
   interruptConfig.eFifoNotEmptyInterruptConfig = VADCTM_INTERRUPT_CONFIG_NONE;
   interruptConfig.eSequencerTimeoutInterruptConfig = VADCTM_INTERRUPT_CONFIG_NONE;

   result = VAdcTMHalConfigInterrupts(&pDevCtxt->iVAdcTMHalInterface, &interruptConfig);
   if (result != VADCTM_HAL_SUCCESS)
   {
      VAdcTM_LogError(pDevCtxt, "VAdcTM - Failed to configure interrupts", TRUE);
      return DAL_ERROR;
   }

   // Set master ID
   result = VAdcTMHalSetInterruptMid(&pDevCtxt->iVAdcTMHalInterface,
                                     (VAdcTMInterruptMid)pDevCtxt->pBsp->uMasterID);
   if (result != VADCTM_HAL_SUCCESS)
   {
      VAdcTM_LogError(pDevCtxt, "VAdcTM - Failed setting interrupt MID", TRUE);
      return DAL_ERROR;
   }

   // Clear all pending interrupts
   result = VAdcTMHalClearInterrupts(&pDevCtxt->iVAdcTMHalInterface,
                                     (VADCTM_INTERRUPT_MASK_BIT_HIGH_THRESHOLD |
                                      VADCTM_INTERRUPT_MASK_BIT_LOW_THRESHOLD));
   if (result != VADCTM_HAL_SUCCESS)
   {
      VAdcTM_LogError(pDevCtxt, "VAdcTM - Failed clearing interrupts", TRUE);
      return DAL_ERROR;
   }

   // Enable interrupts
   result = VAdcTMHalEnableInterrupts(&pDevCtxt->iVAdcTMHalInterface,
                                      (VADCTM_INTERRUPT_MASK_BIT_HIGH_THRESHOLD |
                                       VADCTM_INTERRUPT_MASK_BIT_LOW_THRESHOLD));
   if (result != VADCTM_HAL_SUCCESS)
   {
      VAdcTM_LogError(pDevCtxt, "VAdcTM - Failed enabling interrupts", TRUE);
      return DAL_ERROR;
   }

   // Set operating mode
   result = VAdcTMHalSetOpMode(&pDevCtxt->iVAdcTMHalInterface, VADCTM_OP_MODE_INTERVAL);
   if (result != VADCTM_HAL_SUCCESS)
   {
      VAdcTM_LogError(pDevCtxt, "VAdcTM - Failed setting op mode", TRUE);
      return DAL_ERROR;
   }

   // Set conversion parameters
   convParams.eClockSelect = pDevCtxt->pBsp->eClockSelect;
   convParams.eDecimationRatio = pDevCtxt->pBsp->eDecimationRatio;
   convParams.eFastAverageMode = pDevCtxt->pBsp->eFastAverageMode;
   convParams.eIntervalMode = VADCTM_INTERVAL_MODE_CONTINUOUS;
   convParams.eSettlingDelay = pDevCtxt->pBsp->eSettlingDelay;

   result = VAdcTMHalSetConversionParameters(&pDevCtxt->iVAdcTMHalInterface, &convParams);
   if (result != VADCTM_HAL_SUCCESS)
   {
      VAdcTM_LogError(pDevCtxt, "VAdcTM - Failed setting conv params", TRUE);
      return DAL_ERROR;
   }

   // Set measurement intervals
   measIntervalCtl.eMeasIntervalTime1 = pDevCtxt->pBsp->eMeasIntervalTime1;
   measIntervalCtl.eMeasIntervalTime2 = pDevCtxt->pBsp->eMeasIntervalTime2;
   measIntervalCtl.eMeasIntervalTime3 = pDevCtxt->pBsp->eMeasIntervalTime3;

   result = VAdcTMHalSetMeasIntervalCtl(&pDevCtxt->iVAdcTMHalInterface, &measIntervalCtl);
   if (result != VADCTM_HAL_SUCCESS)
   {
      VAdcTM_LogError(pDevCtxt, "VAdcTM - Failed setting meas ctrl", TRUE);
      return DAL_ERROR;
   }

   // Configure each of the measurements
   for (uMeasIdx = 0; uMeasIdx < pDevCtxt->pBsp->uNumMeas; uMeasIdx++)
   {
      pMeasConfig = &pDevCtxt->pBsp->paMeasConfig[uMeasIdx];

      uHwChannel = VAdc_GetHardwareChannel(pVAdcClientCtxt->pVAdcDevCtxt,
                                           pDevCtxt->aThresholds[uMeasIdx].uVAdcChannelIdx);

      result = VAdcTMHalSetChannel(&pDevCtxt->iVAdcTMHalInterface,
                                   uMeasIdx,
                                   uHwChannel);
      if (result != VADCTM_HAL_SUCCESS)
      {
         VAdcTM_LogError(pDevCtxt, "VAdcTM - Failed to set channel", TRUE);
         return DAL_ERROR;
      }

      result = VAdcTMHalSetMeasInterval(&pDevCtxt->iVAdcTMHalInterface,
                                        uMeasIdx,
                                        pMeasConfig->eMeasIntervalTimeSelect);
      if (result != VADCTM_HAL_SUCCESS)
      {
         VAdcTM_LogError(pDevCtxt, "VAdcTM - Failed to set measurement interval", TRUE);
         return DAL_ERROR;
      }
   }

   // Disable all thresholds and measurements by default
   result = VAdcTMHalSetMeasEnable(&pDevCtxt->iVAdcTMHalInterface, 0);
   if (result != VADCTM_HAL_SUCCESS)
   {
      VAdcTM_LogError(pDevCtxt, "VAdcTM - Failed disabing measurements", TRUE);
      return DAL_ERROR;
   }

   result = VAdcTMHalSetLowThrIntEnable(&pDevCtxt->iVAdcTMHalInterface, 0);
   if (result != VADCTM_HAL_SUCCESS)
   {
      VAdcTM_LogError(pDevCtxt, "VAdcTM - Failed disabling low threshold", TRUE);
      return DAL_ERROR;
   }

   result = VAdcTMHalSetHighThrIntEnable(&pDevCtxt->iVAdcTMHalInterface, 0);
   if (result != VADCTM_HAL_SUCCESS)
   {
      VAdcTM_LogError(pDevCtxt, "VAdcTM - Failed disabling high threshold", TRUE);
      return DAL_ERROR;
   }

   // Create the event use to signal the threshold work loop
   ret = DALSYS_EventCreate(DALSYS_EVENT_ATTR_CLIENT_DEFAULT,
                            &pDevCtxt->hSignalEvent,
                            &pDevCtxt->signalEventObject);
   if (ret != DAL_SUCCESS)
   {
      VAdcTM_LogError(pDevCtxt, "VAdcTM - Failed to create signal", TRUE);
      return ret;
   }
   DALSYS_EventCtrl(pDevCtxt->hSignalEvent, DALSYS_EVENT_CTRL_RESET);

   // Start the work loop that processes threshold events
   ret = DALSYS_EventCtrlEx(pDevCtxt->hWorkLoopEvent,
                            DALSYS_EVENT_CTRL_TRIGGER,
                            0,
                            NULL,
                            0);
   if (ret != DAL_SUCCESS)
   {
      VAdcTM_LogError(pDevCtxt, "VAdcTM - failed to trigger workloop", TRUE);
      return ret;
   }

   return DAL_SUCCESS;
}

DALResult
VAdcTM_DeInit(
   VAdcTMDevCtxt *pDevCtxt
   )
{
   DALResult retResult = DAL_SUCCESS;
   DALResult result;
   uint16 uPerph;

   if (pDevCtxt->phSpmiDev != NULL)
   {
      result = DalDevice_Close(pDevCtxt->phSpmiDev);
      if (result != DAL_SUCCESS)
      {
         retResult = DAL_ERROR;
      }

      uPerph = ((pDevCtxt->pBsp->uSlaveId << 8) & 0xF00) | (pDevCtxt->pBsp->uPeripheralID & 0xFF);

      result = DalSpmi_UnregisterIsr(pDevCtxt->phSpmiDev,
                                     uPerph,
                                     (uint32)(VADCTM_INTERRUPT_MASK_BIT_HIGH_THRESHOLD |
                                              VADCTM_INTERRUPT_MASK_BIT_LOW_THRESHOLD));
      if (result != DAL_SUCCESS)
      {
         retResult = DAL_ERROR;
      }

      result = DAL_DeviceDetach(pDevCtxt->phSpmiDev);
      if (result != DAL_SUCCESS)
      {
         retResult = DAL_ERROR;
      }
      pDevCtxt->phSpmiDev = NULL;
   }

   if (pDevCtxt->ahDelayEvents[VADCTM_EVENT_DEFAULT] != NULL)
   {
      result = DALSYS_DestroyObject(pDevCtxt->ahDelayEvents[VADCTM_EVENT_DEFAULT]);
      if (result != DAL_SUCCESS)
      {
         retResult = DAL_ERROR;
      }
      pDevCtxt->ahDelayEvents[VADCTM_EVENT_DEFAULT] = NULL;
   }

   if (pDevCtxt->ahDelayEvents[VADCTM_EVENT_TIMEOUT] != NULL)
   {
      result = DALSYS_DestroyObject(pDevCtxt->ahDelayEvents[VADCTM_EVENT_TIMEOUT]);
      if (result != DAL_SUCCESS)
      {
         retResult = DAL_ERROR;
      }
      pDevCtxt->ahDelayEvents[VADCTM_EVENT_TIMEOUT] = NULL;
   }

   if (pDevCtxt->hSignalEvent != NULL)
   {
      result = DALSYS_DestroyObject(pDevCtxt->hSignalEvent);
      if (result != DAL_SUCCESS)
      {
         retResult = DAL_ERROR;
      }
      pDevCtxt->hSignalEvent = NULL;
   }

   if (pDevCtxt->hWorkLoopEvent != NULL)
   {
      if (pDevCtxt->hThresholdWorkLoop != NULL)
      {
         result = DALSYS_DeleteEventFromWorkLoop(pDevCtxt->hThresholdWorkLoop,
                                                 pDevCtxt->hWorkLoopEvent);
         if (result != DAL_SUCCESS)
         {
            retResult = DAL_ERROR;
         }
      }

      result = DALSYS_DestroyObject(pDevCtxt->hWorkLoopEvent);
      if (result != DAL_SUCCESS)
      {
         retResult = DAL_ERROR;
      }
      pDevCtxt->hWorkLoopEvent = NULL;
   }

   if (pDevCtxt->hSync != NULL)
   {
      result = DALSYS_DestroyObject(pDevCtxt->hSync);
      if (result != DAL_SUCCESS)
      {
         retResult = DAL_ERROR;
      }
      pDevCtxt->hSync = NULL;
   }

   return retResult;
}

DALResult
VAdcTM_GetInputProperties(
   VAdcTMDevCtxt *pDevCtxt,
   const char * pInputName,
   uint32 *puMeasIdx
   )
{
   uint32 uNumMeas;
   uint32 uMeas;
   const char *pszCurrentMeasName;

   if (pDevCtxt->bHardwareSupported == FALSE)
   {
      return DAL_ERROR;
   }

   uNumMeas = pDevCtxt->pBsp->uNumMeas;

   for (uMeas = 0; uMeas < uNumMeas; uMeas++)
   {
      pszCurrentMeasName = pDevCtxt->pBsp->paMeasConfig[uMeas].pName;

      if (strcmp(pInputName, pszCurrentMeasName) == 0)
      {
         *puMeasIdx = uMeas;
         return DAL_SUCCESS;
      }
   }

   return DAL_ERROR;
}

DALResult
VAdcTM_GetRange(
   VAdcTMDevCtxt *pDevCtxt,
   uint32 uMeasIdx,
   AdcDeviceTMRangeType *pAdcDeviceTMRange
   )
{
   if (uMeasIdx >= pDevCtxt->pBsp->uNumMeas)
   {
      return ADC_DEVICE_ERROR_TM_INVALID_MEAS_IDX;
   }

   pAdcDeviceTMRange->nPhysicalMin = pDevCtxt->aThresholds[uMeasIdx].nPhysical_min;
   pAdcDeviceTMRange->nPhysicalMax = pDevCtxt->aThresholds[uMeasIdx].nPhysical_max;

   return DAL_SUCCESS;
}

DALResult
VAdcTM_SetThreshold(
   VAdcTMDevCtxt *pDevCtxt,
   uint32 uClientId,
   uint32 uMeasIdx,
   const DALSYSEventHandle hEvent,
   AdcDeviceTMThresholdType eThreshold,
   const int32 *pnThresholdDesired,
   int32 *pnThresholdSet
   )
{
   VAdcTMThresholdRequestType *pLowerThresholdRequest;
   VAdcTMThresholdRequestType *pHigherThresholdRequest;
   VAdcTMThresholdRequestType *pThresholdRequest;
   VAdcTMMeasurementType *pMeasurement;
   DALResult result = DAL_SUCCESS;
   uint32 uClientIdx;
   uint32 uCode = 0;

   DALSYS_SyncEnter(pDevCtxt->hSync);

   /* Validate parameters */
   if (uMeasIdx >= pDevCtxt->pBsp->uNumMeas)
   {
      VAdcTM_LogError(pDevCtxt, "VAdcTM - invalid meas index", FALSE);
      result = ADC_DEVICE_ERROR_TM_INVALID_MEAS_IDX;
      goto relLockReturn;
   }

   /* Get the client index */
   result = VAdcTM_GetClientIdx(pDevCtxt, uClientId, &uClientIdx);
   if (result != DAL_SUCCESS)
   {
      VAdcTM_LogError(pDevCtxt, "VAdcTM - could not get client index", FALSE);
      goto relLockReturn;
   }

   pMeasurement = &pDevCtxt->aThresholds[uMeasIdx];
   pLowerThresholdRequest = &pMeasurement->aClientThresholds[uClientIdx][ADC_DEVICE_TM_THRESHOLD_LOWER];
   pHigherThresholdRequest = &pMeasurement->aClientThresholds[uClientIdx][ADC_DEVICE_TM_THRESHOLD_HIGHER];

   if (pLowerThresholdRequest->eThresholdState == VADCTM_THRESHOLD_STATE_TOLERANCE ||
       pHigherThresholdRequest->eThresholdState == VADCTM_THRESHOLD_STATE_TOLERANCE)
   {
      VAdcTM_LogError(pDevCtxt, "VAdcTM - cannot set threshold - tolerance already set", FALSE);
      result = ADC_DEVICE_ERROR_TM_BUSY;
      goto relLockReturn;
   }

   /* Adjust for channels whose physical values vary inversely with code */
   if (pMeasurement->bPhysicalInverseToCode == TRUE)
   {
      /* Flip the threshold type */
      if (eThreshold == ADC_DEVICE_TM_THRESHOLD_LOWER)
      {
         eThreshold = ADC_DEVICE_TM_THRESHOLD_HIGHER;
      }
      else
      {
         eThreshold = ADC_DEVICE_TM_THRESHOLD_LOWER;
      }
   }

   if (pnThresholdDesired != NULL)
   {
      if (*pnThresholdDesired < pMeasurement->nPhysical_min ||
          *pnThresholdDesired > pMeasurement->nPhysical_max)
      {
         result = ADC_DEVICE_ERROR_TM_THRESHOLD_OUT_OF_RANGE;
         goto relLockReturn;
      }

      /* Get the ADC code */
      result = VAdcTM_GetAdcCode(pDevCtxt,
                                 uMeasIdx,
                                 eThreshold,
                                 *pnThresholdDesired,
                                 &uCode,
                                 pnThresholdSet);
      if (result != DAL_SUCCESS)
      {
         goto relLockReturn;
      }
   }

   pThresholdRequest = &pMeasurement->aClientThresholds[uClientIdx][eThreshold];

   /* Set the threshold */
   if (pnThresholdDesired == NULL)
   {
      /* Cancel the threshold */
      pThresholdRequest->eThresholdState = VADCTM_THRESHOLD_STATE_DISABLED;
   }
   else
   {
      /* Set the new threshold */
      pThresholdRequest->eThresholdState = VADCTM_THRESHOLD_STATE_THRESHOLD;
      pThresholdRequest->hEvent = hEvent;
      pThresholdRequest->nPhysicalDesired = *pnThresholdDesired;
      pThresholdRequest->nPhysicalMonitored = *pnThresholdSet;
      pThresholdRequest->uCode = uCode;
   }

   if (pDevCtxt->aClients[uClientIdx].bEnabled == TRUE)
   {
      /* Disable the VADCTM */
      VAdcTM_Disable(pDevCtxt);

      /* Update thresholds and arm */
      result = VAdcTM_UpdateThresholds(pDevCtxt, TRUE);
      if (result != DAL_SUCCESS)
      {
         VAdcTM_LogError(pDevCtxt, "VAdcTM - failed to update and arm", FALSE);
         pThresholdRequest->eThresholdState = VADCTM_THRESHOLD_STATE_DISABLED;
         goto relLockReturn;
      }
   }

relLockReturn:
   DALSYS_SyncLeave(pDevCtxt->hSync);

   return result;
}

DALResult
VAdcTM_SetEnableThresholds(
   VAdcTMDevCtxt *pDevCtxt,
   uint32 uClientId,
   DALBOOL bEnable
   )
{
   DALResult result;
   uint32 uClientIdx;

   DALSYS_SyncEnter(pDevCtxt->hSync);

   /* Get the client index */
   result = VAdcTM_GetClientIdx(pDevCtxt, uClientId, &uClientIdx);
   if (result != DAL_SUCCESS)
   {
      DALSYS_SyncLeave(pDevCtxt->hSync);
      return result;
   }

   /* Disable VADCTM */
   VAdcTM_Disable(pDevCtxt);

   /* Update the client */
   pDevCtxt->aClients[uClientIdx].bEnabled = bEnable;

   /* Update thresholds and arm */
   result = VAdcTM_UpdateThresholds(pDevCtxt, TRUE);
   if (result != DAL_SUCCESS)
   {
      DALSYS_SyncLeave(pDevCtxt->hSync);
      return result;
   }

   DALSYS_SyncLeave(pDevCtxt->hSync);

   return DAL_SUCCESS;
}

DALResult
VAdcTM_UpdateCalibration(
   VAdcTMDevCtxt *pDevCtxt
   )
{
   VAdcClientCtxt *pVAdcClientCtxt = pDevCtxt->phVAdcDev->pClientCtxt;
   VAdcTMThresholdRequestType *pThresholdRequest;
   VAdcTMMeasurementType *pMeasurement;
   int32 nThresholdMonitored;
   int32 nCurrentValue = 0;
   DALBOOL bRearmTolerance;
   DALResult result;
   uint32 uThresh;
   uint32 uClient;
   uint32 uMeas;
   uint32 uCode;

   DALSYS_SyncEnter(pDevCtxt->hSync);

   VAdc_GetCalibration(pVAdcClientCtxt->pVAdcDevCtxt,
                       pDevCtxt->pBsp->uVAdcConfigIdx,
                       &pDevCtxt->vAdcCalibData);

   /* Update the threshold codes */
   for (uMeas = 0; uMeas < pDevCtxt->pBsp->uNumMeas; uMeas++)
   {
      pMeasurement = &pDevCtxt->aThresholds[uMeas];

      for (uClient = 0; uClient < VADCTM_MAX_NUM_CLIENTS; uClient++)
      {
         for (uThresh = 0; uThresh < ADC_DEVICE_TM_NUM_THRESHOLDS; uThresh++)
         {
            pThresholdRequest = &pMeasurement->aClientThresholds[uClient][uThresh];

            if (pThresholdRequest->eThresholdState == VADCTM_THRESHOLD_STATE_THRESHOLD)
            {
               result = VAdcTM_GetAdcCode(pDevCtxt,
                                          uMeas,
                                          (AdcDeviceTMThresholdType)uThresh,
                                          pThresholdRequest->nPhysicalDesired,
                                          &uCode,
                                          &nThresholdMonitored);
               if (result != DAL_SUCCESS)
               {
                  VAdcTM_LogError(pDevCtxt, "VAdcTM - failed getting new code on recal", FALSE);
               }
               else
               {
                  pThresholdRequest->uCode = uCode;
                  pThresholdRequest->nPhysicalMonitored = nThresholdMonitored;
               }
            }
         }

         bRearmTolerance = FALSE;
         pThresholdRequest = &pMeasurement->aClientThresholds[uClient][ADC_DEVICE_TM_THRESHOLD_LOWER];
         if (pThresholdRequest->eThresholdState == VADCTM_THRESHOLD_STATE_TOLERANCE)
         {
            nCurrentValue = pThresholdRequest->nCurrentValue;
            bRearmTolerance = TRUE;
         }
         else
         {
            pThresholdRequest = &pMeasurement->aClientThresholds[uClient][ADC_DEVICE_TM_THRESHOLD_HIGHER];
            if (pThresholdRequest->eThresholdState == VADCTM_THRESHOLD_STATE_TOLERANCE)
            {
               nCurrentValue = pThresholdRequest->nCurrentValue;
               bRearmTolerance = TRUE;
            }
         }

         if (bRearmTolerance == TRUE)
         {
            result = VAdcTM_SlideToleranceThresholds(pDevCtxt,
                                                     uClient,
                                                     uMeas,
                                                     nCurrentValue);
            if (result != DAL_SUCCESS)
            {
               VAdcTM_LogError(pDevCtxt, "VAdcTM - failed to slide threshold tolerances on recal", FALSE);
            }
         }
      }
   }

   VAdcTM_Disable(pDevCtxt);

   result = VAdcTM_UpdateThresholds(pDevCtxt, TRUE);
   if (result != DAL_SUCCESS)
   {
      DALSYS_SyncLeave(pDevCtxt->hSync);
      return result;
   }

   DALSYS_SyncLeave(pDevCtxt->hSync);

   return DAL_SUCCESS;
}

DALResult
VAdcTM_UnregisterClient(
   VAdcTMDevCtxt *pDevCtxt,
   uint32 uClientId)
{
   uint32 uClientIdx;
   DALResult result;

   DALSYS_SyncEnter(pDevCtxt->hSync);

   for (uClientIdx = 0; uClientIdx < VADCTM_MAX_NUM_CLIENTS; uClientIdx++)
   {
      if (pDevCtxt->aClients[uClientIdx].eState == VADCTM_CLIENT_STATE_CLAIMED &&
          pDevCtxt->aClients[uClientIdx].uClientId == uClientId)
      {
         /* Disable VADCTM */
         VAdcTM_Disable(pDevCtxt);

         /* The client has been found */
         pDevCtxt->aClients[uClientIdx].eState = VADCTM_CLIENT_STATE_UNCLAIMED;
         pDevCtxt->aClients[uClientIdx].bEnabled = FALSE;

         /* Update thresholds and arm */
         result = VAdcTM_UpdateThresholds(pDevCtxt, TRUE);
         if (result != DAL_SUCCESS)
         {
            DALSYS_SyncLeave(pDevCtxt->hSync);
            return result;
         }
      }
   }

   DALSYS_SyncLeave(pDevCtxt->hSync);

   return DAL_SUCCESS;
}

DALResult
VAdcTM_SetTolerance(
   VAdcTMDevCtxt *pDevCtxt,
   uint32 uClientId,
   uint32 uMeasIdx,
   const DALSYSEventHandle hEvent,
   const int32 *pnLowerTolerance,
   const int32 *pnHigherTolerance
   )
{
   VAdcTMThresholdRequestType *pLowerThresholdRequest;
   VAdcTMThresholdRequestType *pHigherThresholdRequest;
   VAdcTMMeasurementType *pMeasurement;
   AdcDeviceTMThresholdType eThreshold;
   DALResult result = DAL_SUCCESS;
   int32 nCurrentValue;
   uint32 uClientIdx;
   int32 *pnCurrentValue;

   DALSYS_SyncEnter(pDevCtxt->hSync);

   if (pnLowerTolerance != NULL)
   {
      if (*pnLowerTolerance <= 0)
      {
         VAdcTM_LogError(pDevCtxt, "VAdcTM - lower tolerance", FALSE);
         result = ADC_DEVICE_ERROR_TM_THRESHOLD_OUT_OF_RANGE;
         goto relLockReturn;
      }
   }

   if (pnHigherTolerance != NULL)
   {
      if (*pnHigherTolerance <= 0)
      {
         VAdcTM_LogError(pDevCtxt, "VAdcTM - higher tolerance", FALSE);
         result = ADC_DEVICE_ERROR_TM_THRESHOLD_OUT_OF_RANGE;
         goto relLockReturn;
      }
   }

   if (uMeasIdx >= pDevCtxt->pBsp->uNumMeas)
   {
      VAdcTM_LogError(pDevCtxt, "VAdcTM - invalid meas index", FALSE);
      result = ADC_DEVICE_ERROR_TM_INVALID_MEAS_IDX;
      goto relLockReturn;
   }

   result = VAdcTM_GetClientIdx(pDevCtxt, uClientId, &uClientIdx);
   if (result != DAL_SUCCESS)
   {
      VAdcTM_LogError(pDevCtxt, "VAdcTM - could not get client index", FALSE);
      goto relLockReturn;
   }

   pMeasurement = &pDevCtxt->aThresholds[uMeasIdx];
   pLowerThresholdRequest = &pMeasurement->aClientThresholds[uClientIdx][ADC_DEVICE_TM_THRESHOLD_LOWER];
   pHigherThresholdRequest = &pMeasurement->aClientThresholds[uClientIdx][ADC_DEVICE_TM_THRESHOLD_HIGHER];

   if (pLowerThresholdRequest->eThresholdState == VADCTM_THRESHOLD_STATE_THRESHOLD ||
       pHigherThresholdRequest->eThresholdState == VADCTM_THRESHOLD_STATE_THRESHOLD)
   {
      VAdcTM_LogError(pDevCtxt, "VAdcTM - cannot set tolerance - thresholds already set", FALSE);
      result = ADC_DEVICE_ERROR_TM_BUSY;
      goto relLockReturn;
   }

   pLowerThresholdRequest->eThresholdState = VADCTM_THRESHOLD_STATE_DISABLED;
   pHigherThresholdRequest->eThresholdState = VADCTM_THRESHOLD_STATE_DISABLED;

   if (pnLowerTolerance == NULL && pnHigherTolerance == NULL)
   {
      pnCurrentValue = NULL;
   }
   else if (pnLowerTolerance != NULL && pnHigherTolerance == NULL)
   {
      pnCurrentValue = &pMeasurement->nPhysical_max;
   }
   else if (pnLowerTolerance == NULL && pnHigherTolerance != NULL)
   {
      pnCurrentValue = &pMeasurement->nPhysical_min;
   }
   else
   {
      /* TODO: determine the current value by reading the channel. For now, assume the
       * current value is the midpoint of the range. */
      nCurrentValue = (pMeasurement->nPhysical_max + pMeasurement->nPhysical_min) / 2;
      pnCurrentValue = &nCurrentValue;
   }

   if (pMeasurement->bPhysicalInverseToCode == FALSE)
   {
      eThreshold = ADC_DEVICE_TM_THRESHOLD_LOWER;
   }
   else
   {
      eThreshold = ADC_DEVICE_TM_THRESHOLD_HIGHER;
   }
   result = VAdcTM_ConfigTolerance(pDevCtxt,
                                   uClientIdx,
                                   uMeasIdx,
                                   eThreshold,
                                   hEvent,
                                   pnLowerTolerance,
                                   pnCurrentValue);
   if (result != DAL_SUCCESS)
   {
      goto relLockReturn;
   }

   if (pMeasurement->bPhysicalInverseToCode == FALSE)
   {
      eThreshold = ADC_DEVICE_TM_THRESHOLD_HIGHER;
   }
   else
   {
      eThreshold = ADC_DEVICE_TM_THRESHOLD_LOWER;
   }
   result = VAdcTM_ConfigTolerance(pDevCtxt,
                                   uClientIdx,
                                   uMeasIdx,
                                   eThreshold,
                                   hEvent,
                                   pnHigherTolerance,
                                   pnCurrentValue);
   if (result != DAL_SUCCESS)
   {
      goto relLockReturn;
   }

   if (pDevCtxt->aClients[uClientIdx].bEnabled == TRUE)
   {
      VAdcTM_Disable(pDevCtxt);

      result = VAdcTM_UpdateThresholds(pDevCtxt, TRUE);
      if (result != DAL_SUCCESS)
      {
         VAdcTM_LogError(pDevCtxt, "VAdcTM - failed to update and arm", FALSE);
         goto relLockReturn;
      }
   }

relLockReturn:
   DALSYS_SyncLeave(pDevCtxt->hSync);

   return result;
}

