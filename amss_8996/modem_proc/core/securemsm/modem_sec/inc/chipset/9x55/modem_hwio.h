#ifndef __MODEM_HWIO_H__
#define __MODEM_HWIO_H__
/*
===========================================================================
*/
/**
  @file modem_hwio.h
  @brief Auto-generated HWIO interface include file.

  Reference chip release:
    MDM9x45 (Tesla) [tesla_v1.0_p3q3r19.4_MTO]
 
  This file contains HWIO register definitions for the following modules:
    MPSS_RMB
    MPSS_PERPH
    SECURITY_CONTROL_CORE
    TCSR_TCSR_REGS
    TCSR_TCSR_MUTEX

  'Include' filters applied: 
  'Exclude' filters applied: RESERVED DUMMY 
*/
/*
  ===========================================================================

  Copyright (c) 2014 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

  ===========================================================================

  $Header: //components/rel/core.mpss/3.4.c3.11/securemsm/modem_sec/inc/chipset/9x55/modem_hwio.h#1 $
  $DateTime: 2016/03/28 23:02:17 $
  $Author: mplcsds1 $

  ===========================================================================
*/

/*----------------------------------------------------------------------------
 * MODULE: MPSS_RMB
 *--------------------------------------------------------------------------*/

#define MPSS_RMB_REG_BASE                                                  (MODEM_TOP_BASE      + 0x00180000)
#define MPSS_RMB_REG_BASE_PHYS                                             (MODEM_TOP_BASE_PHYS + 0x00180000)

#define HWIO_MSS_RELAY_MSG_BUFFER_00_ADDR                                  (MPSS_RMB_REG_BASE      + 0x00000000)
#define HWIO_MSS_RELAY_MSG_BUFFER_00_PHYS                                  (MPSS_RMB_REG_BASE_PHYS + 0x00000000)
#define HWIO_MSS_RELAY_MSG_BUFFER_00_RMSK                                  0xffffffff
#define HWIO_MSS_RELAY_MSG_BUFFER_00_IN          \
        in_dword_masked(HWIO_MSS_RELAY_MSG_BUFFER_00_ADDR, HWIO_MSS_RELAY_MSG_BUFFER_00_RMSK)
#define HWIO_MSS_RELAY_MSG_BUFFER_00_INM(m)      \
        in_dword_masked(HWIO_MSS_RELAY_MSG_BUFFER_00_ADDR, m)
#define HWIO_MSS_RELAY_MSG_BUFFER_00_OUT(v)      \
        out_dword(HWIO_MSS_RELAY_MSG_BUFFER_00_ADDR,v)
#define HWIO_MSS_RELAY_MSG_BUFFER_00_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_RELAY_MSG_BUFFER_00_ADDR,m,v,HWIO_MSS_RELAY_MSG_BUFFER_00_IN)
#define HWIO_MSS_RELAY_MSG_BUFFER_00_MSS_RELAY_MSG_BUFFER_00_BMSK          0xffffffff
#define HWIO_MSS_RELAY_MSG_BUFFER_00_MSS_RELAY_MSG_BUFFER_00_SHFT                 0x0

#define HWIO_MSS_RELAY_MSG_BUFFER_01_ADDR                                  (MPSS_RMB_REG_BASE      + 0x00000004)
#define HWIO_MSS_RELAY_MSG_BUFFER_01_PHYS                                  (MPSS_RMB_REG_BASE_PHYS + 0x00000004)
#define HWIO_MSS_RELAY_MSG_BUFFER_01_RMSK                                  0xffffffff
#define HWIO_MSS_RELAY_MSG_BUFFER_01_IN          \
        in_dword_masked(HWIO_MSS_RELAY_MSG_BUFFER_01_ADDR, HWIO_MSS_RELAY_MSG_BUFFER_01_RMSK)
#define HWIO_MSS_RELAY_MSG_BUFFER_01_INM(m)      \
        in_dword_masked(HWIO_MSS_RELAY_MSG_BUFFER_01_ADDR, m)
#define HWIO_MSS_RELAY_MSG_BUFFER_01_OUT(v)      \
        out_dword(HWIO_MSS_RELAY_MSG_BUFFER_01_ADDR,v)
#define HWIO_MSS_RELAY_MSG_BUFFER_01_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_RELAY_MSG_BUFFER_01_ADDR,m,v,HWIO_MSS_RELAY_MSG_BUFFER_01_IN)
#define HWIO_MSS_RELAY_MSG_BUFFER_01_MSS_RELAY_MSG_BUFFER_01_BMSK          0xffffffff
#define HWIO_MSS_RELAY_MSG_BUFFER_01_MSS_RELAY_MSG_BUFFER_01_SHFT                 0x0

#define HWIO_MSS_RELAY_MSG_BUFFER_02_ADDR                                  (MPSS_RMB_REG_BASE      + 0x00000008)
#define HWIO_MSS_RELAY_MSG_BUFFER_02_PHYS                                  (MPSS_RMB_REG_BASE_PHYS + 0x00000008)
#define HWIO_MSS_RELAY_MSG_BUFFER_02_RMSK                                  0xffffffff
#define HWIO_MSS_RELAY_MSG_BUFFER_02_IN          \
        in_dword_masked(HWIO_MSS_RELAY_MSG_BUFFER_02_ADDR, HWIO_MSS_RELAY_MSG_BUFFER_02_RMSK)
#define HWIO_MSS_RELAY_MSG_BUFFER_02_INM(m)      \
        in_dword_masked(HWIO_MSS_RELAY_MSG_BUFFER_02_ADDR, m)
#define HWIO_MSS_RELAY_MSG_BUFFER_02_OUT(v)      \
        out_dword(HWIO_MSS_RELAY_MSG_BUFFER_02_ADDR,v)
#define HWIO_MSS_RELAY_MSG_BUFFER_02_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_RELAY_MSG_BUFFER_02_ADDR,m,v,HWIO_MSS_RELAY_MSG_BUFFER_02_IN)
#define HWIO_MSS_RELAY_MSG_BUFFER_02_MSS_RELAY_MSG_BUFFER_02_BMSK          0xffffffff
#define HWIO_MSS_RELAY_MSG_BUFFER_02_MSS_RELAY_MSG_BUFFER_02_SHFT                 0x0

#define HWIO_MSS_RELAY_MSG_BUFFER_03_ADDR                                  (MPSS_RMB_REG_BASE      + 0x0000000c)
#define HWIO_MSS_RELAY_MSG_BUFFER_03_PHYS                                  (MPSS_RMB_REG_BASE_PHYS + 0x0000000c)
#define HWIO_MSS_RELAY_MSG_BUFFER_03_RMSK                                  0xffffffff
#define HWIO_MSS_RELAY_MSG_BUFFER_03_IN          \
        in_dword_masked(HWIO_MSS_RELAY_MSG_BUFFER_03_ADDR, HWIO_MSS_RELAY_MSG_BUFFER_03_RMSK)
#define HWIO_MSS_RELAY_MSG_BUFFER_03_INM(m)      \
        in_dword_masked(HWIO_MSS_RELAY_MSG_BUFFER_03_ADDR, m)
#define HWIO_MSS_RELAY_MSG_BUFFER_03_OUT(v)      \
        out_dword(HWIO_MSS_RELAY_MSG_BUFFER_03_ADDR,v)
#define HWIO_MSS_RELAY_MSG_BUFFER_03_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_RELAY_MSG_BUFFER_03_ADDR,m,v,HWIO_MSS_RELAY_MSG_BUFFER_03_IN)
#define HWIO_MSS_RELAY_MSG_BUFFER_03_MSS_RELAY_MSG_BUFFER_03_BMSK          0xffffffff
#define HWIO_MSS_RELAY_MSG_BUFFER_03_MSS_RELAY_MSG_BUFFER_03_SHFT                 0x0

#define HWIO_MSS_RELAY_MSG_BUFFER_04_ADDR                                  (MPSS_RMB_REG_BASE      + 0x00000010)
#define HWIO_MSS_RELAY_MSG_BUFFER_04_PHYS                                  (MPSS_RMB_REG_BASE_PHYS + 0x00000010)
#define HWIO_MSS_RELAY_MSG_BUFFER_04_RMSK                                  0xffffffff
#define HWIO_MSS_RELAY_MSG_BUFFER_04_IN          \
        in_dword_masked(HWIO_MSS_RELAY_MSG_BUFFER_04_ADDR, HWIO_MSS_RELAY_MSG_BUFFER_04_RMSK)
#define HWIO_MSS_RELAY_MSG_BUFFER_04_INM(m)      \
        in_dword_masked(HWIO_MSS_RELAY_MSG_BUFFER_04_ADDR, m)
#define HWIO_MSS_RELAY_MSG_BUFFER_04_OUT(v)      \
        out_dword(HWIO_MSS_RELAY_MSG_BUFFER_04_ADDR,v)
#define HWIO_MSS_RELAY_MSG_BUFFER_04_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_RELAY_MSG_BUFFER_04_ADDR,m,v,HWIO_MSS_RELAY_MSG_BUFFER_04_IN)
#define HWIO_MSS_RELAY_MSG_BUFFER_04_MSS_RELAY_MSG_BUFFER_04_BMSK          0xffffffff
#define HWIO_MSS_RELAY_MSG_BUFFER_04_MSS_RELAY_MSG_BUFFER_04_SHFT                 0x0

#define HWIO_MSS_RELAY_MSG_BUFFER_05_ADDR                                  (MPSS_RMB_REG_BASE      + 0x00000014)
#define HWIO_MSS_RELAY_MSG_BUFFER_05_PHYS                                  (MPSS_RMB_REG_BASE_PHYS + 0x00000014)
#define HWIO_MSS_RELAY_MSG_BUFFER_05_RMSK                                  0xffffffff
#define HWIO_MSS_RELAY_MSG_BUFFER_05_IN          \
        in_dword_masked(HWIO_MSS_RELAY_MSG_BUFFER_05_ADDR, HWIO_MSS_RELAY_MSG_BUFFER_05_RMSK)
#define HWIO_MSS_RELAY_MSG_BUFFER_05_INM(m)      \
        in_dword_masked(HWIO_MSS_RELAY_MSG_BUFFER_05_ADDR, m)
#define HWIO_MSS_RELAY_MSG_BUFFER_05_OUT(v)      \
        out_dword(HWIO_MSS_RELAY_MSG_BUFFER_05_ADDR,v)
#define HWIO_MSS_RELAY_MSG_BUFFER_05_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_RELAY_MSG_BUFFER_05_ADDR,m,v,HWIO_MSS_RELAY_MSG_BUFFER_05_IN)
#define HWIO_MSS_RELAY_MSG_BUFFER_05_MSS_RELAY_MSG_BUFFER_05_BMSK          0xffffffff
#define HWIO_MSS_RELAY_MSG_BUFFER_05_MSS_RELAY_MSG_BUFFER_05_SHFT                 0x0

#define HWIO_MSS_RELAY_MSG_BUFFER_06_ADDR                                  (MPSS_RMB_REG_BASE      + 0x00000018)
#define HWIO_MSS_RELAY_MSG_BUFFER_06_PHYS                                  (MPSS_RMB_REG_BASE_PHYS + 0x00000018)
#define HWIO_MSS_RELAY_MSG_BUFFER_06_RMSK                                  0xffffffff
#define HWIO_MSS_RELAY_MSG_BUFFER_06_IN          \
        in_dword_masked(HWIO_MSS_RELAY_MSG_BUFFER_06_ADDR, HWIO_MSS_RELAY_MSG_BUFFER_06_RMSK)
#define HWIO_MSS_RELAY_MSG_BUFFER_06_INM(m)      \
        in_dword_masked(HWIO_MSS_RELAY_MSG_BUFFER_06_ADDR, m)
#define HWIO_MSS_RELAY_MSG_BUFFER_06_OUT(v)      \
        out_dword(HWIO_MSS_RELAY_MSG_BUFFER_06_ADDR,v)
#define HWIO_MSS_RELAY_MSG_BUFFER_06_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_RELAY_MSG_BUFFER_06_ADDR,m,v,HWIO_MSS_RELAY_MSG_BUFFER_06_IN)
#define HWIO_MSS_RELAY_MSG_BUFFER_06_MSS_RELAY_MSG_BUFFER_06_BMSK          0xffffffff
#define HWIO_MSS_RELAY_MSG_BUFFER_06_MSS_RELAY_MSG_BUFFER_06_SHFT                 0x0

#define HWIO_MSS_RELAY_MSG_BUFFER_07_ADDR                                  (MPSS_RMB_REG_BASE      + 0x0000001c)
#define HWIO_MSS_RELAY_MSG_BUFFER_07_PHYS                                  (MPSS_RMB_REG_BASE_PHYS + 0x0000001c)
#define HWIO_MSS_RELAY_MSG_BUFFER_07_RMSK                                  0xffffffff
#define HWIO_MSS_RELAY_MSG_BUFFER_07_IN          \
        in_dword_masked(HWIO_MSS_RELAY_MSG_BUFFER_07_ADDR, HWIO_MSS_RELAY_MSG_BUFFER_07_RMSK)
#define HWIO_MSS_RELAY_MSG_BUFFER_07_INM(m)      \
        in_dword_masked(HWIO_MSS_RELAY_MSG_BUFFER_07_ADDR, m)
#define HWIO_MSS_RELAY_MSG_BUFFER_07_OUT(v)      \
        out_dword(HWIO_MSS_RELAY_MSG_BUFFER_07_ADDR,v)
#define HWIO_MSS_RELAY_MSG_BUFFER_07_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_RELAY_MSG_BUFFER_07_ADDR,m,v,HWIO_MSS_RELAY_MSG_BUFFER_07_IN)
#define HWIO_MSS_RELAY_MSG_BUFFER_07_MSS_RELAY_MSG_BUFFER_07_BMSK          0xffffffff
#define HWIO_MSS_RELAY_MSG_BUFFER_07_MSS_RELAY_MSG_BUFFER_07_SHFT                 0x0

#define HWIO_MSS_RELAY_MSG_BUFFER_08_ADDR                                  (MPSS_RMB_REG_BASE      + 0x00000020)
#define HWIO_MSS_RELAY_MSG_BUFFER_08_PHYS                                  (MPSS_RMB_REG_BASE_PHYS + 0x00000020)
#define HWIO_MSS_RELAY_MSG_BUFFER_08_RMSK                                  0xffffffff
#define HWIO_MSS_RELAY_MSG_BUFFER_08_IN          \
        in_dword_masked(HWIO_MSS_RELAY_MSG_BUFFER_08_ADDR, HWIO_MSS_RELAY_MSG_BUFFER_08_RMSK)
#define HWIO_MSS_RELAY_MSG_BUFFER_08_INM(m)      \
        in_dword_masked(HWIO_MSS_RELAY_MSG_BUFFER_08_ADDR, m)
#define HWIO_MSS_RELAY_MSG_BUFFER_08_OUT(v)      \
        out_dword(HWIO_MSS_RELAY_MSG_BUFFER_08_ADDR,v)
#define HWIO_MSS_RELAY_MSG_BUFFER_08_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_RELAY_MSG_BUFFER_08_ADDR,m,v,HWIO_MSS_RELAY_MSG_BUFFER_08_IN)
#define HWIO_MSS_RELAY_MSG_BUFFER_08_MSS_RELAY_MSG_BUFFER_08_BMSK          0xffffffff
#define HWIO_MSS_RELAY_MSG_BUFFER_08_MSS_RELAY_MSG_BUFFER_08_SHFT                 0x0

#define HWIO_MSS_RELAY_MSG_BUFFER_09_ADDR                                  (MPSS_RMB_REG_BASE      + 0x00000024)
#define HWIO_MSS_RELAY_MSG_BUFFER_09_PHYS                                  (MPSS_RMB_REG_BASE_PHYS + 0x00000024)
#define HWIO_MSS_RELAY_MSG_BUFFER_09_RMSK                                  0xffffffff
#define HWIO_MSS_RELAY_MSG_BUFFER_09_IN          \
        in_dword_masked(HWIO_MSS_RELAY_MSG_BUFFER_09_ADDR, HWIO_MSS_RELAY_MSG_BUFFER_09_RMSK)
#define HWIO_MSS_RELAY_MSG_BUFFER_09_INM(m)      \
        in_dword_masked(HWIO_MSS_RELAY_MSG_BUFFER_09_ADDR, m)
#define HWIO_MSS_RELAY_MSG_BUFFER_09_OUT(v)      \
        out_dword(HWIO_MSS_RELAY_MSG_BUFFER_09_ADDR,v)
#define HWIO_MSS_RELAY_MSG_BUFFER_09_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_RELAY_MSG_BUFFER_09_ADDR,m,v,HWIO_MSS_RELAY_MSG_BUFFER_09_IN)
#define HWIO_MSS_RELAY_MSG_BUFFER_09_MSS_RELAY_MSG_BUFFER_09_BMSK          0xffffffff
#define HWIO_MSS_RELAY_MSG_BUFFER_09_MSS_RELAY_MSG_BUFFER_09_SHFT                 0x0

#define HWIO_MSS_RELAY_MSG_BUFFER_10_ADDR                                  (MPSS_RMB_REG_BASE      + 0x00000028)
#define HWIO_MSS_RELAY_MSG_BUFFER_10_PHYS                                  (MPSS_RMB_REG_BASE_PHYS + 0x00000028)
#define HWIO_MSS_RELAY_MSG_BUFFER_10_RMSK                                  0xffffffff
#define HWIO_MSS_RELAY_MSG_BUFFER_10_IN          \
        in_dword_masked(HWIO_MSS_RELAY_MSG_BUFFER_10_ADDR, HWIO_MSS_RELAY_MSG_BUFFER_10_RMSK)
#define HWIO_MSS_RELAY_MSG_BUFFER_10_INM(m)      \
        in_dword_masked(HWIO_MSS_RELAY_MSG_BUFFER_10_ADDR, m)
#define HWIO_MSS_RELAY_MSG_BUFFER_10_OUT(v)      \
        out_dword(HWIO_MSS_RELAY_MSG_BUFFER_10_ADDR,v)
#define HWIO_MSS_RELAY_MSG_BUFFER_10_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_RELAY_MSG_BUFFER_10_ADDR,m,v,HWIO_MSS_RELAY_MSG_BUFFER_10_IN)
#define HWIO_MSS_RELAY_MSG_BUFFER_10_MSS_RELAY_MSG_BUFFER_10_BMSK          0xffffffff
#define HWIO_MSS_RELAY_MSG_BUFFER_10_MSS_RELAY_MSG_BUFFER_10_SHFT                 0x0

#define HWIO_MSS_RELAY_MSG_BUFFER_11_ADDR                                  (MPSS_RMB_REG_BASE      + 0x0000002c)
#define HWIO_MSS_RELAY_MSG_BUFFER_11_PHYS                                  (MPSS_RMB_REG_BASE_PHYS + 0x0000002c)
#define HWIO_MSS_RELAY_MSG_BUFFER_11_RMSK                                  0xffffffff
#define HWIO_MSS_RELAY_MSG_BUFFER_11_IN          \
        in_dword_masked(HWIO_MSS_RELAY_MSG_BUFFER_11_ADDR, HWIO_MSS_RELAY_MSG_BUFFER_11_RMSK)
#define HWIO_MSS_RELAY_MSG_BUFFER_11_INM(m)      \
        in_dword_masked(HWIO_MSS_RELAY_MSG_BUFFER_11_ADDR, m)
#define HWIO_MSS_RELAY_MSG_BUFFER_11_OUT(v)      \
        out_dword(HWIO_MSS_RELAY_MSG_BUFFER_11_ADDR,v)
#define HWIO_MSS_RELAY_MSG_BUFFER_11_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_RELAY_MSG_BUFFER_11_ADDR,m,v,HWIO_MSS_RELAY_MSG_BUFFER_11_IN)
#define HWIO_MSS_RELAY_MSG_BUFFER_11_MSS_RELAY_MSG_BUFFER_11_BMSK          0xffffffff
#define HWIO_MSS_RELAY_MSG_BUFFER_11_MSS_RELAY_MSG_BUFFER_11_SHFT                 0x0

#define HWIO_MSS_RELAY_MSG_BUFFER_12_ADDR                                  (MPSS_RMB_REG_BASE      + 0x00000030)
#define HWIO_MSS_RELAY_MSG_BUFFER_12_PHYS                                  (MPSS_RMB_REG_BASE_PHYS + 0x00000030)
#define HWIO_MSS_RELAY_MSG_BUFFER_12_RMSK                                  0xffffffff
#define HWIO_MSS_RELAY_MSG_BUFFER_12_IN          \
        in_dword_masked(HWIO_MSS_RELAY_MSG_BUFFER_12_ADDR, HWIO_MSS_RELAY_MSG_BUFFER_12_RMSK)
#define HWIO_MSS_RELAY_MSG_BUFFER_12_INM(m)      \
        in_dword_masked(HWIO_MSS_RELAY_MSG_BUFFER_12_ADDR, m)
#define HWIO_MSS_RELAY_MSG_BUFFER_12_OUT(v)      \
        out_dword(HWIO_MSS_RELAY_MSG_BUFFER_12_ADDR,v)
#define HWIO_MSS_RELAY_MSG_BUFFER_12_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_RELAY_MSG_BUFFER_12_ADDR,m,v,HWIO_MSS_RELAY_MSG_BUFFER_12_IN)
#define HWIO_MSS_RELAY_MSG_BUFFER_12_MSS_RELAY_MSG_BUFFER_12_BMSK          0xffffffff
#define HWIO_MSS_RELAY_MSG_BUFFER_12_MSS_RELAY_MSG_BUFFER_12_SHFT                 0x0

#define HWIO_MSS_RELAY_MSG_BUFFER_13_ADDR                                  (MPSS_RMB_REG_BASE      + 0x00000034)
#define HWIO_MSS_RELAY_MSG_BUFFER_13_PHYS                                  (MPSS_RMB_REG_BASE_PHYS + 0x00000034)
#define HWIO_MSS_RELAY_MSG_BUFFER_13_RMSK                                  0xffffffff
#define HWIO_MSS_RELAY_MSG_BUFFER_13_IN          \
        in_dword_masked(HWIO_MSS_RELAY_MSG_BUFFER_13_ADDR, HWIO_MSS_RELAY_MSG_BUFFER_13_RMSK)
#define HWIO_MSS_RELAY_MSG_BUFFER_13_INM(m)      \
        in_dword_masked(HWIO_MSS_RELAY_MSG_BUFFER_13_ADDR, m)
#define HWIO_MSS_RELAY_MSG_BUFFER_13_OUT(v)      \
        out_dword(HWIO_MSS_RELAY_MSG_BUFFER_13_ADDR,v)
#define HWIO_MSS_RELAY_MSG_BUFFER_13_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_RELAY_MSG_BUFFER_13_ADDR,m,v,HWIO_MSS_RELAY_MSG_BUFFER_13_IN)
#define HWIO_MSS_RELAY_MSG_BUFFER_13_MSS_RELAY_MSG_BUFFER_13_BMSK          0xffffffff
#define HWIO_MSS_RELAY_MSG_BUFFER_13_MSS_RELAY_MSG_BUFFER_13_SHFT                 0x0

#define HWIO_MSS_RELAY_MSG_BUFFER_14_ADDR                                  (MPSS_RMB_REG_BASE      + 0x00000038)
#define HWIO_MSS_RELAY_MSG_BUFFER_14_PHYS                                  (MPSS_RMB_REG_BASE_PHYS + 0x00000038)
#define HWIO_MSS_RELAY_MSG_BUFFER_14_RMSK                                  0xffffffff
#define HWIO_MSS_RELAY_MSG_BUFFER_14_IN          \
        in_dword_masked(HWIO_MSS_RELAY_MSG_BUFFER_14_ADDR, HWIO_MSS_RELAY_MSG_BUFFER_14_RMSK)
#define HWIO_MSS_RELAY_MSG_BUFFER_14_INM(m)      \
        in_dword_masked(HWIO_MSS_RELAY_MSG_BUFFER_14_ADDR, m)
#define HWIO_MSS_RELAY_MSG_BUFFER_14_OUT(v)      \
        out_dword(HWIO_MSS_RELAY_MSG_BUFFER_14_ADDR,v)
#define HWIO_MSS_RELAY_MSG_BUFFER_14_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_RELAY_MSG_BUFFER_14_ADDR,m,v,HWIO_MSS_RELAY_MSG_BUFFER_14_IN)
#define HWIO_MSS_RELAY_MSG_BUFFER_14_MSS_RELAY_MSG_BUFFER_14_BMSK          0xffffffff
#define HWIO_MSS_RELAY_MSG_BUFFER_14_MSS_RELAY_MSG_BUFFER_14_SHFT                 0x0

#define HWIO_MSS_RELAY_MSG_BUFFER_15_ADDR                                  (MPSS_RMB_REG_BASE      + 0x0000003c)
#define HWIO_MSS_RELAY_MSG_BUFFER_15_PHYS                                  (MPSS_RMB_REG_BASE_PHYS + 0x0000003c)
#define HWIO_MSS_RELAY_MSG_BUFFER_15_RMSK                                  0xffffffff
#define HWIO_MSS_RELAY_MSG_BUFFER_15_IN          \
        in_dword_masked(HWIO_MSS_RELAY_MSG_BUFFER_15_ADDR, HWIO_MSS_RELAY_MSG_BUFFER_15_RMSK)
#define HWIO_MSS_RELAY_MSG_BUFFER_15_INM(m)      \
        in_dword_masked(HWIO_MSS_RELAY_MSG_BUFFER_15_ADDR, m)
#define HWIO_MSS_RELAY_MSG_BUFFER_15_OUT(v)      \
        out_dword(HWIO_MSS_RELAY_MSG_BUFFER_15_ADDR,v)
#define HWIO_MSS_RELAY_MSG_BUFFER_15_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_RELAY_MSG_BUFFER_15_ADDR,m,v,HWIO_MSS_RELAY_MSG_BUFFER_15_IN)
#define HWIO_MSS_RELAY_MSG_BUFFER_15_MSS_RELAY_MSG_BUFFER_15_BMSK          0xffffffff
#define HWIO_MSS_RELAY_MSG_BUFFER_15_MSS_RELAY_MSG_BUFFER_15_SHFT                 0x0

/*----------------------------------------------------------------------------
 * MODULE: MPSS_PERPH
 *--------------------------------------------------------------------------*/

#define MPSS_PERPH_REG_BASE                                                   (MODEM_TOP_BASE      + 0x001a8000)
#define MPSS_PERPH_REG_BASE_PHYS                                              (MODEM_TOP_BASE_PHYS + 0x001a8000)

#define HWIO_MSS_AHB_ACCESS_ERR_IRQ_EN_ADDR                                   (MPSS_PERPH_REG_BASE      + 0x00000000)
#define HWIO_MSS_AHB_ACCESS_ERR_IRQ_EN_PHYS                                   (MPSS_PERPH_REG_BASE_PHYS + 0x00000000)
#define HWIO_MSS_AHB_ACCESS_ERR_IRQ_EN_RMSK                                          0x1
#define HWIO_MSS_AHB_ACCESS_ERR_IRQ_EN_IN          \
        in_dword_masked(HWIO_MSS_AHB_ACCESS_ERR_IRQ_EN_ADDR, HWIO_MSS_AHB_ACCESS_ERR_IRQ_EN_RMSK)
#define HWIO_MSS_AHB_ACCESS_ERR_IRQ_EN_INM(m)      \
        in_dword_masked(HWIO_MSS_AHB_ACCESS_ERR_IRQ_EN_ADDR, m)
#define HWIO_MSS_AHB_ACCESS_ERR_IRQ_EN_OUT(v)      \
        out_dword(HWIO_MSS_AHB_ACCESS_ERR_IRQ_EN_ADDR,v)
#define HWIO_MSS_AHB_ACCESS_ERR_IRQ_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_AHB_ACCESS_ERR_IRQ_EN_ADDR,m,v,HWIO_MSS_AHB_ACCESS_ERR_IRQ_EN_IN)
#define HWIO_MSS_AHB_ACCESS_ERR_IRQ_EN_EN_BMSK                                       0x1
#define HWIO_MSS_AHB_ACCESS_ERR_IRQ_EN_EN_SHFT                                       0x0

#define HWIO_MSS_AHB_ACCESS_ERR_IRQ_STATUS_ADDR                               (MPSS_PERPH_REG_BASE      + 0x00000004)
#define HWIO_MSS_AHB_ACCESS_ERR_IRQ_STATUS_PHYS                               (MPSS_PERPH_REG_BASE_PHYS + 0x00000004)
#define HWIO_MSS_AHB_ACCESS_ERR_IRQ_STATUS_RMSK                                      0x1
#define HWIO_MSS_AHB_ACCESS_ERR_IRQ_STATUS_IN          \
        in_dword_masked(HWIO_MSS_AHB_ACCESS_ERR_IRQ_STATUS_ADDR, HWIO_MSS_AHB_ACCESS_ERR_IRQ_STATUS_RMSK)
#define HWIO_MSS_AHB_ACCESS_ERR_IRQ_STATUS_INM(m)      \
        in_dword_masked(HWIO_MSS_AHB_ACCESS_ERR_IRQ_STATUS_ADDR, m)
#define HWIO_MSS_AHB_ACCESS_ERR_IRQ_STATUS_STATUS_BMSK                               0x1
#define HWIO_MSS_AHB_ACCESS_ERR_IRQ_STATUS_STATUS_SHFT                               0x0

#define HWIO_MSS_AHB_ACCESS_ERR_IRQ_CLR_ADDR                                  (MPSS_PERPH_REG_BASE      + 0x00000008)
#define HWIO_MSS_AHB_ACCESS_ERR_IRQ_CLR_PHYS                                  (MPSS_PERPH_REG_BASE_PHYS + 0x00000008)
#define HWIO_MSS_AHB_ACCESS_ERR_IRQ_CLR_RMSK                                         0x1
#define HWIO_MSS_AHB_ACCESS_ERR_IRQ_CLR_OUT(v)      \
        out_dword(HWIO_MSS_AHB_ACCESS_ERR_IRQ_CLR_ADDR,v)
#define HWIO_MSS_AHB_ACCESS_ERR_IRQ_CLR_CMD_BMSK                                     0x1
#define HWIO_MSS_AHB_ACCESS_ERR_IRQ_CLR_CMD_SHFT                                     0x0

#define HWIO_MSS_MSA_CRYPTO_ADDR                                              (MPSS_PERPH_REG_BASE      + 0x0000000c)
#define HWIO_MSS_MSA_CRYPTO_PHYS                                              (MPSS_PERPH_REG_BASE_PHYS + 0x0000000c)
#define HWIO_MSS_MSA_CRYPTO_RMSK                                                     0xf
#define HWIO_MSS_MSA_CRYPTO_IN          \
        in_dword_masked(HWIO_MSS_MSA_CRYPTO_ADDR, HWIO_MSS_MSA_CRYPTO_RMSK)
#define HWIO_MSS_MSA_CRYPTO_INM(m)      \
        in_dword_masked(HWIO_MSS_MSA_CRYPTO_ADDR, m)
#define HWIO_MSS_MSA_CRYPTO_OUT(v)      \
        out_dword(HWIO_MSS_MSA_CRYPTO_ADDR,v)
#define HWIO_MSS_MSA_CRYPTO_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_MSA_CRYPTO_ADDR,m,v,HWIO_MSS_MSA_CRYPTO_IN)
#define HWIO_MSS_MSA_CRYPTO_MSA_CRYPTO_BMSK                                          0xf
#define HWIO_MSS_MSA_CRYPTO_MSA_CRYPTO_SHFT                                          0x0

#define HWIO_MSS_MSA_ADDR                                                     (MPSS_PERPH_REG_BASE      + 0x00000010)
#define HWIO_MSS_MSA_PHYS                                                     (MPSS_PERPH_REG_BASE_PHYS + 0x00000010)
#define HWIO_MSS_MSA_RMSK                                                            0xf
#define HWIO_MSS_MSA_IN          \
        in_dword_masked(HWIO_MSS_MSA_ADDR, HWIO_MSS_MSA_RMSK)
#define HWIO_MSS_MSA_INM(m)      \
        in_dword_masked(HWIO_MSS_MSA_ADDR, m)
#define HWIO_MSS_MSA_OUT(v)      \
        out_dword(HWIO_MSS_MSA_ADDR,v)
#define HWIO_MSS_MSA_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_MSA_ADDR,m,v,HWIO_MSS_MSA_IN)
#define HWIO_MSS_MSA_CONFIG_UNLOCK2_BMSK                                             0x8
#define HWIO_MSS_MSA_CONFIG_UNLOCK2_SHFT                                             0x3
#define HWIO_MSS_MSA_FORCE_Q6_MSA_BMSK                                               0x4
#define HWIO_MSS_MSA_FORCE_Q6_MSA_SHFT                                               0x2
#define HWIO_MSS_MSA_MBA_OK_BMSK                                                     0x2
#define HWIO_MSS_MSA_MBA_OK_SHFT                                                     0x1
#define HWIO_MSS_MSA_CONFIG_LOCK_BMSK                                                0x1
#define HWIO_MSS_MSA_CONFIG_LOCK_SHFT                                                0x0

#define HWIO_MSS_CLOCK_SPDM_MON_ADDR                                          (MPSS_PERPH_REG_BASE      + 0x00000014)
#define HWIO_MSS_CLOCK_SPDM_MON_PHYS                                          (MPSS_PERPH_REG_BASE_PHYS + 0x00000014)
#define HWIO_MSS_CLOCK_SPDM_MON_RMSK                                                 0x1
#define HWIO_MSS_CLOCK_SPDM_MON_IN          \
        in_dword_masked(HWIO_MSS_CLOCK_SPDM_MON_ADDR, HWIO_MSS_CLOCK_SPDM_MON_RMSK)
#define HWIO_MSS_CLOCK_SPDM_MON_INM(m)      \
        in_dword_masked(HWIO_MSS_CLOCK_SPDM_MON_ADDR, m)
#define HWIO_MSS_CLOCK_SPDM_MON_OUT(v)      \
        out_dword(HWIO_MSS_CLOCK_SPDM_MON_ADDR,v)
#define HWIO_MSS_CLOCK_SPDM_MON_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_CLOCK_SPDM_MON_ADDR,m,v,HWIO_MSS_CLOCK_SPDM_MON_IN)
#define HWIO_MSS_CLOCK_SPDM_MON_BUS_MON_CLKEN_BMSK                                   0x1
#define HWIO_MSS_CLOCK_SPDM_MON_BUS_MON_CLKEN_SHFT                                   0x0

#define HWIO_MSS_DEBUG_CLOCK_CTL_ADDR                                         (MPSS_PERPH_REG_BASE      + 0x00000018)
#define HWIO_MSS_DEBUG_CLOCK_CTL_PHYS                                         (MPSS_PERPH_REG_BASE_PHYS + 0x00000018)
#define HWIO_MSS_DEBUG_CLOCK_CTL_RMSK                                         0xff8003ff
#define HWIO_MSS_DEBUG_CLOCK_CTL_IN          \
        in_dword_masked(HWIO_MSS_DEBUG_CLOCK_CTL_ADDR, HWIO_MSS_DEBUG_CLOCK_CTL_RMSK)
#define HWIO_MSS_DEBUG_CLOCK_CTL_INM(m)      \
        in_dword_masked(HWIO_MSS_DEBUG_CLOCK_CTL_ADDR, m)
#define HWIO_MSS_DEBUG_CLOCK_CTL_OUT(v)      \
        out_dword(HWIO_MSS_DEBUG_CLOCK_CTL_ADDR,v)
#define HWIO_MSS_DEBUG_CLOCK_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_DEBUG_CLOCK_CTL_ADDR,m,v,HWIO_MSS_DEBUG_CLOCK_CTL_IN)
#define HWIO_MSS_DEBUG_CLOCK_CTL_PLL_RESET_SEL_BMSK                           0xe0000000
#define HWIO_MSS_DEBUG_CLOCK_CTL_PLL_RESET_SEL_SHFT                                 0x1d
#define HWIO_MSS_DEBUG_CLOCK_CTL_PLL_BYPASS_SEL_BMSK                          0x1c000000
#define HWIO_MSS_DEBUG_CLOCK_CTL_PLL_BYPASS_SEL_SHFT                                0x1a
#define HWIO_MSS_DEBUG_CLOCK_CTL_PLLOUT_LV_TEST_SEL_BMSK                       0x3800000
#define HWIO_MSS_DEBUG_CLOCK_CTL_PLLOUT_LV_TEST_SEL_SHFT                            0x17
#define HWIO_MSS_DEBUG_CLOCK_CTL_MODEM_MUX_SEL_BMSK                                0x3c0
#define HWIO_MSS_DEBUG_CLOCK_CTL_MODEM_MUX_SEL_SHFT                                  0x6
#define HWIO_MSS_DEBUG_CLOCK_CTL_DBG_LEVEL6_MUX_SEL_BMSK                            0x20
#define HWIO_MSS_DEBUG_CLOCK_CTL_DBG_LEVEL6_MUX_SEL_SHFT                             0x5
#define HWIO_MSS_DEBUG_CLOCK_CTL_DBG_LEVEL5_MUX_SEL_BMSK                            0x10
#define HWIO_MSS_DEBUG_CLOCK_CTL_DBG_LEVEL5_MUX_SEL_SHFT                             0x4
#define HWIO_MSS_DEBUG_CLOCK_CTL_DBG_LEVEL4_MUX_SEL_BMSK                             0x8
#define HWIO_MSS_DEBUG_CLOCK_CTL_DBG_LEVEL4_MUX_SEL_SHFT                             0x3
#define HWIO_MSS_DEBUG_CLOCK_CTL_DBG_LEVEL3_MUX_SEL_BMSK                             0x4
#define HWIO_MSS_DEBUG_CLOCK_CTL_DBG_LEVEL3_MUX_SEL_SHFT                             0x2
#define HWIO_MSS_DEBUG_CLOCK_CTL_DBG_LEVEL2_MUX_SEL_BMSK                             0x2
#define HWIO_MSS_DEBUG_CLOCK_CTL_DBG_LEVEL2_MUX_SEL_SHFT                             0x1
#define HWIO_MSS_DEBUG_CLOCK_CTL_DBG_LEVEL1_MUX_SEL_BMSK                             0x1
#define HWIO_MSS_DEBUG_CLOCK_CTL_DBG_LEVEL1_MUX_SEL_SHFT                             0x0

#define HWIO_MSS_DEBUG_CTL_ADDR                                               (MPSS_PERPH_REG_BASE      + 0x0000001c)
#define HWIO_MSS_DEBUG_CTL_PHYS                                               (MPSS_PERPH_REG_BASE_PHYS + 0x0000001c)
#define HWIO_MSS_DEBUG_CTL_RMSK                                                      0x7
#define HWIO_MSS_DEBUG_CTL_IN          \
        in_dword_masked(HWIO_MSS_DEBUG_CTL_ADDR, HWIO_MSS_DEBUG_CTL_RMSK)
#define HWIO_MSS_DEBUG_CTL_INM(m)      \
        in_dword_masked(HWIO_MSS_DEBUG_CTL_ADDR, m)
#define HWIO_MSS_DEBUG_CTL_OUT(v)      \
        out_dword(HWIO_MSS_DEBUG_CTL_ADDR,v)
#define HWIO_MSS_DEBUG_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_DEBUG_CTL_ADDR,m,v,HWIO_MSS_DEBUG_CTL_IN)
#define HWIO_MSS_DEBUG_CTL_DAC_DISABLE_ON_Q6_DBG_BMSK                                0x4
#define HWIO_MSS_DEBUG_CTL_DAC_DISABLE_ON_Q6_DBG_SHFT                                0x2
#define HWIO_MSS_DEBUG_CTL_GRFC_DISABLE_Q6_DBG_BMSK                                  0x2
#define HWIO_MSS_DEBUG_CTL_GRFC_DISABLE_Q6_DBG_SHFT                                  0x1
#define HWIO_MSS_DEBUG_CTL_GRFC_DISABLE_Q6_WDOG_BMSK                                 0x1
#define HWIO_MSS_DEBUG_CTL_GRFC_DISABLE_Q6_WDOG_SHFT                                 0x0

#define HWIO_MSS_DBG_BUS_CTL_ADDR                                             (MPSS_PERPH_REG_BASE      + 0x00000020)
#define HWIO_MSS_DBG_BUS_CTL_PHYS                                             (MPSS_PERPH_REG_BASE_PHYS + 0x00000020)
#define HWIO_MSS_DBG_BUS_CTL_RMSK                                                0x7ffff
#define HWIO_MSS_DBG_BUS_CTL_IN          \
        in_dword_masked(HWIO_MSS_DBG_BUS_CTL_ADDR, HWIO_MSS_DBG_BUS_CTL_RMSK)
#define HWIO_MSS_DBG_BUS_CTL_INM(m)      \
        in_dword_masked(HWIO_MSS_DBG_BUS_CTL_ADDR, m)
#define HWIO_MSS_DBG_BUS_CTL_OUT(v)      \
        out_dword(HWIO_MSS_DBG_BUS_CTL_ADDR,v)
#define HWIO_MSS_DBG_BUS_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_DBG_BUS_CTL_ADDR,m,v,HWIO_MSS_DBG_BUS_CTL_IN)
#define HWIO_MSS_DBG_BUS_CTL_FABRIC_BRIDGES_SEL_BMSK                             0x70000
#define HWIO_MSS_DBG_BUS_CTL_FABRIC_BRIDGES_SEL_SHFT                                0x10
#define HWIO_MSS_DBG_BUS_CTL_AXI2AXI_DMA_SEL_BMSK                                 0xe000
#define HWIO_MSS_DBG_BUS_CTL_AXI2AXI_DMA_SEL_SHFT                                    0xd
#define HWIO_MSS_DBG_BUS_CTL_AHB2AXI_NAV_SEL_BMSK                                 0x1c00
#define HWIO_MSS_DBG_BUS_CTL_AHB2AXI_NAV_SEL_SHFT                                    0xa
#define HWIO_MSS_DBG_BUS_CTL_AHB2AHB_1X1_CNOC_SEL_BMSK                             0x300
#define HWIO_MSS_DBG_BUS_CTL_AHB2AHB_1X1_CNOC_SEL_SHFT                               0x8
#define HWIO_MSS_DBG_BUS_CTL_MODEM_DBG_BUS_SEL_BMSK                                 0xf0
#define HWIO_MSS_DBG_BUS_CTL_MODEM_DBG_BUS_SEL_SHFT                                  0x4
#define HWIO_MSS_DBG_BUS_CTL_MPSS_DBG_BUS_SEL_BMSK                                   0xf
#define HWIO_MSS_DBG_BUS_CTL_MPSS_DBG_BUS_SEL_SHFT                                   0x0

#define HWIO_MSS_NAV_AXI_HALTREQ_ADDR                                         (MPSS_PERPH_REG_BASE      + 0x00000024)
#define HWIO_MSS_NAV_AXI_HALTREQ_PHYS                                         (MPSS_PERPH_REG_BASE_PHYS + 0x00000024)
#define HWIO_MSS_NAV_AXI_HALTREQ_RMSK                                                0x7
#define HWIO_MSS_NAV_AXI_HALTREQ_IN          \
        in_dword_masked(HWIO_MSS_NAV_AXI_HALTREQ_ADDR, HWIO_MSS_NAV_AXI_HALTREQ_RMSK)
#define HWIO_MSS_NAV_AXI_HALTREQ_INM(m)      \
        in_dword_masked(HWIO_MSS_NAV_AXI_HALTREQ_ADDR, m)
#define HWIO_MSS_NAV_AXI_HALTREQ_OUT(v)      \
        out_dword(HWIO_MSS_NAV_AXI_HALTREQ_ADDR,v)
#define HWIO_MSS_NAV_AXI_HALTREQ_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_NAV_AXI_HALTREQ_ADDR,m,v,HWIO_MSS_NAV_AXI_HALTREQ_IN)
#define HWIO_MSS_NAV_AXI_HALTREQ_IDLE_BMSK                                           0x4
#define HWIO_MSS_NAV_AXI_HALTREQ_IDLE_SHFT                                           0x2
#define HWIO_MSS_NAV_AXI_HALTREQ_HALTACK_BMSK                                        0x2
#define HWIO_MSS_NAV_AXI_HALTREQ_HALTACK_SHFT                                        0x1
#define HWIO_MSS_NAV_AXI_HALTREQ_HALTREQ_BMSK                                        0x1
#define HWIO_MSS_NAV_AXI_HALTREQ_HALTREQ_SHFT                                        0x0

#define HWIO_MSS_BIMC_DANGER_ADDR                                             (MPSS_PERPH_REG_BASE      + 0x00000028)
#define HWIO_MSS_BIMC_DANGER_PHYS                                             (MPSS_PERPH_REG_BASE_PHYS + 0x00000028)
#define HWIO_MSS_BIMC_DANGER_RMSK                                                    0x7
#define HWIO_MSS_BIMC_DANGER_IN          \
        in_dword_masked(HWIO_MSS_BIMC_DANGER_ADDR, HWIO_MSS_BIMC_DANGER_RMSK)
#define HWIO_MSS_BIMC_DANGER_INM(m)      \
        in_dword_masked(HWIO_MSS_BIMC_DANGER_ADDR, m)
#define HWIO_MSS_BIMC_DANGER_OUT(v)      \
        out_dword(HWIO_MSS_BIMC_DANGER_ADDR,v)
#define HWIO_MSS_BIMC_DANGER_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BIMC_DANGER_ADDR,m,v,HWIO_MSS_BIMC_DANGER_IN)
#define HWIO_MSS_BIMC_DANGER_MSS_SAVE_BMSK                                           0x4
#define HWIO_MSS_BIMC_DANGER_MSS_SAVE_SHFT                                           0x2
#define HWIO_MSS_BIMC_DANGER_MSS_DANGER_BMSK                                         0x3
#define HWIO_MSS_BIMC_DANGER_MSS_DANGER_SHFT                                         0x0

#define HWIO_MSS_HW_VERSION_ADDR                                              (MPSS_PERPH_REG_BASE      + 0x0000002c)
#define HWIO_MSS_HW_VERSION_PHYS                                              (MPSS_PERPH_REG_BASE_PHYS + 0x0000002c)
#define HWIO_MSS_HW_VERSION_RMSK                                              0xffffffff
#define HWIO_MSS_HW_VERSION_IN          \
        in_dword_masked(HWIO_MSS_HW_VERSION_ADDR, HWIO_MSS_HW_VERSION_RMSK)
#define HWIO_MSS_HW_VERSION_INM(m)      \
        in_dword_masked(HWIO_MSS_HW_VERSION_ADDR, m)
#define HWIO_MSS_HW_VERSION_MAJOR_BMSK                                        0xf0000000
#define HWIO_MSS_HW_VERSION_MAJOR_SHFT                                              0x1c
#define HWIO_MSS_HW_VERSION_MINOR_BMSK                                         0xfff0000
#define HWIO_MSS_HW_VERSION_MINOR_SHFT                                              0x10
#define HWIO_MSS_HW_VERSION_STEP_BMSK                                             0xffff
#define HWIO_MSS_HW_VERSION_STEP_SHFT                                                0x0

#define HWIO_MSS_RELAY_MSG_SHADOW0_ADDR                                       (MPSS_PERPH_REG_BASE      + 0x00000400)
#define HWIO_MSS_RELAY_MSG_SHADOW0_PHYS                                       (MPSS_PERPH_REG_BASE_PHYS + 0x00000400)
#define HWIO_MSS_RELAY_MSG_SHADOW0_RMSK                                       0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW0_IN          \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW0_ADDR, HWIO_MSS_RELAY_MSG_SHADOW0_RMSK)
#define HWIO_MSS_RELAY_MSG_SHADOW0_INM(m)      \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW0_ADDR, m)
#define HWIO_MSS_RELAY_MSG_SHADOW0_OUT(v)      \
        out_dword(HWIO_MSS_RELAY_MSG_SHADOW0_ADDR,v)
#define HWIO_MSS_RELAY_MSG_SHADOW0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_RELAY_MSG_SHADOW0_ADDR,m,v,HWIO_MSS_RELAY_MSG_SHADOW0_IN)
#define HWIO_MSS_RELAY_MSG_SHADOW0_RELAY_MSG_SHADOW0_BMSK                     0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW0_RELAY_MSG_SHADOW0_SHFT                            0x0

#define HWIO_MSS_RELAY_MSG_SHADOW1_ADDR                                       (MPSS_PERPH_REG_BASE      + 0x00000404)
#define HWIO_MSS_RELAY_MSG_SHADOW1_PHYS                                       (MPSS_PERPH_REG_BASE_PHYS + 0x00000404)
#define HWIO_MSS_RELAY_MSG_SHADOW1_RMSK                                       0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW1_IN          \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW1_ADDR, HWIO_MSS_RELAY_MSG_SHADOW1_RMSK)
#define HWIO_MSS_RELAY_MSG_SHADOW1_INM(m)      \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW1_ADDR, m)
#define HWIO_MSS_RELAY_MSG_SHADOW1_OUT(v)      \
        out_dword(HWIO_MSS_RELAY_MSG_SHADOW1_ADDR,v)
#define HWIO_MSS_RELAY_MSG_SHADOW1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_RELAY_MSG_SHADOW1_ADDR,m,v,HWIO_MSS_RELAY_MSG_SHADOW1_IN)
#define HWIO_MSS_RELAY_MSG_SHADOW1_RELAY_MSG_SHADOW1_BMSK                     0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW1_RELAY_MSG_SHADOW1_SHFT                            0x0

#define HWIO_MSS_RELAY_MSG_SHADOW2_ADDR                                       (MPSS_PERPH_REG_BASE      + 0x00000408)
#define HWIO_MSS_RELAY_MSG_SHADOW2_PHYS                                       (MPSS_PERPH_REG_BASE_PHYS + 0x00000408)
#define HWIO_MSS_RELAY_MSG_SHADOW2_RMSK                                       0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW2_IN          \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW2_ADDR, HWIO_MSS_RELAY_MSG_SHADOW2_RMSK)
#define HWIO_MSS_RELAY_MSG_SHADOW2_INM(m)      \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW2_ADDR, m)
#define HWIO_MSS_RELAY_MSG_SHADOW2_OUT(v)      \
        out_dword(HWIO_MSS_RELAY_MSG_SHADOW2_ADDR,v)
#define HWIO_MSS_RELAY_MSG_SHADOW2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_RELAY_MSG_SHADOW2_ADDR,m,v,HWIO_MSS_RELAY_MSG_SHADOW2_IN)
#define HWIO_MSS_RELAY_MSG_SHADOW2_RELAY_MSG_SHADOW2_BMSK                     0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW2_RELAY_MSG_SHADOW2_SHFT                            0x0

#define HWIO_MSS_RELAY_MSG_SHADOW3_ADDR                                       (MPSS_PERPH_REG_BASE      + 0x0000040c)
#define HWIO_MSS_RELAY_MSG_SHADOW3_PHYS                                       (MPSS_PERPH_REG_BASE_PHYS + 0x0000040c)
#define HWIO_MSS_RELAY_MSG_SHADOW3_RMSK                                       0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW3_IN          \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW3_ADDR, HWIO_MSS_RELAY_MSG_SHADOW3_RMSK)
#define HWIO_MSS_RELAY_MSG_SHADOW3_INM(m)      \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW3_ADDR, m)
#define HWIO_MSS_RELAY_MSG_SHADOW3_OUT(v)      \
        out_dword(HWIO_MSS_RELAY_MSG_SHADOW3_ADDR,v)
#define HWIO_MSS_RELAY_MSG_SHADOW3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_RELAY_MSG_SHADOW3_ADDR,m,v,HWIO_MSS_RELAY_MSG_SHADOW3_IN)
#define HWIO_MSS_RELAY_MSG_SHADOW3_RELAY_MSG_SHADOW3_BMSK                     0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW3_RELAY_MSG_SHADOW3_SHFT                            0x0

#define HWIO_MSS_RELAY_MSG_SHADOW4_ADDR                                       (MPSS_PERPH_REG_BASE      + 0x00000410)
#define HWIO_MSS_RELAY_MSG_SHADOW4_PHYS                                       (MPSS_PERPH_REG_BASE_PHYS + 0x00000410)
#define HWIO_MSS_RELAY_MSG_SHADOW4_RMSK                                       0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW4_IN          \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW4_ADDR, HWIO_MSS_RELAY_MSG_SHADOW4_RMSK)
#define HWIO_MSS_RELAY_MSG_SHADOW4_INM(m)      \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW4_ADDR, m)
#define HWIO_MSS_RELAY_MSG_SHADOW4_OUT(v)      \
        out_dword(HWIO_MSS_RELAY_MSG_SHADOW4_ADDR,v)
#define HWIO_MSS_RELAY_MSG_SHADOW4_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_RELAY_MSG_SHADOW4_ADDR,m,v,HWIO_MSS_RELAY_MSG_SHADOW4_IN)
#define HWIO_MSS_RELAY_MSG_SHADOW4_RELAY_MSG_SHADOW4_BMSK                     0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW4_RELAY_MSG_SHADOW4_SHFT                            0x0

#define HWIO_MSS_RELAY_MSG_SHADOW5_ADDR                                       (MPSS_PERPH_REG_BASE      + 0x00000414)
#define HWIO_MSS_RELAY_MSG_SHADOW5_PHYS                                       (MPSS_PERPH_REG_BASE_PHYS + 0x00000414)
#define HWIO_MSS_RELAY_MSG_SHADOW5_RMSK                                       0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW5_IN          \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW5_ADDR, HWIO_MSS_RELAY_MSG_SHADOW5_RMSK)
#define HWIO_MSS_RELAY_MSG_SHADOW5_INM(m)      \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW5_ADDR, m)
#define HWIO_MSS_RELAY_MSG_SHADOW5_OUT(v)      \
        out_dword(HWIO_MSS_RELAY_MSG_SHADOW5_ADDR,v)
#define HWIO_MSS_RELAY_MSG_SHADOW5_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_RELAY_MSG_SHADOW5_ADDR,m,v,HWIO_MSS_RELAY_MSG_SHADOW5_IN)
#define HWIO_MSS_RELAY_MSG_SHADOW5_RELAY_MSG_SHADOW5_BMSK                     0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW5_RELAY_MSG_SHADOW5_SHFT                            0x0

#define HWIO_MSS_RELAY_MSG_SHADOW6_ADDR                                       (MPSS_PERPH_REG_BASE      + 0x00000418)
#define HWIO_MSS_RELAY_MSG_SHADOW6_PHYS                                       (MPSS_PERPH_REG_BASE_PHYS + 0x00000418)
#define HWIO_MSS_RELAY_MSG_SHADOW6_RMSK                                       0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW6_IN          \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW6_ADDR, HWIO_MSS_RELAY_MSG_SHADOW6_RMSK)
#define HWIO_MSS_RELAY_MSG_SHADOW6_INM(m)      \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW6_ADDR, m)
#define HWIO_MSS_RELAY_MSG_SHADOW6_OUT(v)      \
        out_dword(HWIO_MSS_RELAY_MSG_SHADOW6_ADDR,v)
#define HWIO_MSS_RELAY_MSG_SHADOW6_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_RELAY_MSG_SHADOW6_ADDR,m,v,HWIO_MSS_RELAY_MSG_SHADOW6_IN)
#define HWIO_MSS_RELAY_MSG_SHADOW6_RELAY_MSG_SHADOW6_BMSK                     0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW6_RELAY_MSG_SHADOW6_SHFT                            0x0

#define HWIO_MSS_RELAY_MSG_SHADOW7_ADDR                                       (MPSS_PERPH_REG_BASE      + 0x0000041c)
#define HWIO_MSS_RELAY_MSG_SHADOW7_PHYS                                       (MPSS_PERPH_REG_BASE_PHYS + 0x0000041c)
#define HWIO_MSS_RELAY_MSG_SHADOW7_RMSK                                       0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW7_IN          \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW7_ADDR, HWIO_MSS_RELAY_MSG_SHADOW7_RMSK)
#define HWIO_MSS_RELAY_MSG_SHADOW7_INM(m)      \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW7_ADDR, m)
#define HWIO_MSS_RELAY_MSG_SHADOW7_OUT(v)      \
        out_dword(HWIO_MSS_RELAY_MSG_SHADOW7_ADDR,v)
#define HWIO_MSS_RELAY_MSG_SHADOW7_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_RELAY_MSG_SHADOW7_ADDR,m,v,HWIO_MSS_RELAY_MSG_SHADOW7_IN)
#define HWIO_MSS_RELAY_MSG_SHADOW7_RELAY_MSG_SHADOW7_BMSK                     0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW7_RELAY_MSG_SHADOW7_SHFT                            0x0

#define HWIO_MSS_RELAY_MSG_SHADOW8_ADDR                                       (MPSS_PERPH_REG_BASE      + 0x00000420)
#define HWIO_MSS_RELAY_MSG_SHADOW8_PHYS                                       (MPSS_PERPH_REG_BASE_PHYS + 0x00000420)
#define HWIO_MSS_RELAY_MSG_SHADOW8_RMSK                                       0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW8_IN          \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW8_ADDR, HWIO_MSS_RELAY_MSG_SHADOW8_RMSK)
#define HWIO_MSS_RELAY_MSG_SHADOW8_INM(m)      \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW8_ADDR, m)
#define HWIO_MSS_RELAY_MSG_SHADOW8_OUT(v)      \
        out_dword(HWIO_MSS_RELAY_MSG_SHADOW8_ADDR,v)
#define HWIO_MSS_RELAY_MSG_SHADOW8_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_RELAY_MSG_SHADOW8_ADDR,m,v,HWIO_MSS_RELAY_MSG_SHADOW8_IN)
#define HWIO_MSS_RELAY_MSG_SHADOW8_RELAY_MSG_SHADOW8_BMSK                     0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW8_RELAY_MSG_SHADOW8_SHFT                            0x0

#define HWIO_MSS_RELAY_MSG_SHADOW9_ADDR                                       (MPSS_PERPH_REG_BASE      + 0x00000424)
#define HWIO_MSS_RELAY_MSG_SHADOW9_PHYS                                       (MPSS_PERPH_REG_BASE_PHYS + 0x00000424)
#define HWIO_MSS_RELAY_MSG_SHADOW9_RMSK                                       0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW9_IN          \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW9_ADDR, HWIO_MSS_RELAY_MSG_SHADOW9_RMSK)
#define HWIO_MSS_RELAY_MSG_SHADOW9_INM(m)      \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW9_ADDR, m)
#define HWIO_MSS_RELAY_MSG_SHADOW9_OUT(v)      \
        out_dword(HWIO_MSS_RELAY_MSG_SHADOW9_ADDR,v)
#define HWIO_MSS_RELAY_MSG_SHADOW9_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_RELAY_MSG_SHADOW9_ADDR,m,v,HWIO_MSS_RELAY_MSG_SHADOW9_IN)
#define HWIO_MSS_RELAY_MSG_SHADOW9_RELAY_MSG_SHADOW9_BMSK                     0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW9_RELAY_MSG_SHADOW9_SHFT                            0x0

#define HWIO_MSS_RELAY_MSG_SHADOW10_ADDR                                      (MPSS_PERPH_REG_BASE      + 0x00000428)
#define HWIO_MSS_RELAY_MSG_SHADOW10_PHYS                                      (MPSS_PERPH_REG_BASE_PHYS + 0x00000428)
#define HWIO_MSS_RELAY_MSG_SHADOW10_RMSK                                      0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW10_IN          \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW10_ADDR, HWIO_MSS_RELAY_MSG_SHADOW10_RMSK)
#define HWIO_MSS_RELAY_MSG_SHADOW10_INM(m)      \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW10_ADDR, m)
#define HWIO_MSS_RELAY_MSG_SHADOW10_OUT(v)      \
        out_dword(HWIO_MSS_RELAY_MSG_SHADOW10_ADDR,v)
#define HWIO_MSS_RELAY_MSG_SHADOW10_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_RELAY_MSG_SHADOW10_ADDR,m,v,HWIO_MSS_RELAY_MSG_SHADOW10_IN)
#define HWIO_MSS_RELAY_MSG_SHADOW10_RELAY_MSG_SHADOW10_BMSK                   0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW10_RELAY_MSG_SHADOW10_SHFT                          0x0

#define HWIO_MSS_RELAY_MSG_SHADOW11_ADDR                                      (MPSS_PERPH_REG_BASE      + 0x0000042c)
#define HWIO_MSS_RELAY_MSG_SHADOW11_PHYS                                      (MPSS_PERPH_REG_BASE_PHYS + 0x0000042c)
#define HWIO_MSS_RELAY_MSG_SHADOW11_RMSK                                      0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW11_IN          \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW11_ADDR, HWIO_MSS_RELAY_MSG_SHADOW11_RMSK)
#define HWIO_MSS_RELAY_MSG_SHADOW11_INM(m)      \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW11_ADDR, m)
#define HWIO_MSS_RELAY_MSG_SHADOW11_OUT(v)      \
        out_dword(HWIO_MSS_RELAY_MSG_SHADOW11_ADDR,v)
#define HWIO_MSS_RELAY_MSG_SHADOW11_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_RELAY_MSG_SHADOW11_ADDR,m,v,HWIO_MSS_RELAY_MSG_SHADOW11_IN)
#define HWIO_MSS_RELAY_MSG_SHADOW11_RELAY_MSG_SHADOW11_BMSK                   0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW11_RELAY_MSG_SHADOW11_SHFT                          0x0

#define HWIO_MSS_RELAY_MSG_SHADOW12_ADDR                                      (MPSS_PERPH_REG_BASE      + 0x00000430)
#define HWIO_MSS_RELAY_MSG_SHADOW12_PHYS                                      (MPSS_PERPH_REG_BASE_PHYS + 0x00000430)
#define HWIO_MSS_RELAY_MSG_SHADOW12_RMSK                                      0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW12_IN          \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW12_ADDR, HWIO_MSS_RELAY_MSG_SHADOW12_RMSK)
#define HWIO_MSS_RELAY_MSG_SHADOW12_INM(m)      \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW12_ADDR, m)
#define HWIO_MSS_RELAY_MSG_SHADOW12_OUT(v)      \
        out_dword(HWIO_MSS_RELAY_MSG_SHADOW12_ADDR,v)
#define HWIO_MSS_RELAY_MSG_SHADOW12_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_RELAY_MSG_SHADOW12_ADDR,m,v,HWIO_MSS_RELAY_MSG_SHADOW12_IN)
#define HWIO_MSS_RELAY_MSG_SHADOW12_RELAY_MSG_SHADOW12_BMSK                   0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW12_RELAY_MSG_SHADOW12_SHFT                          0x0

#define HWIO_MSS_RELAY_MSG_SHADOW13_ADDR                                      (MPSS_PERPH_REG_BASE      + 0x00000434)
#define HWIO_MSS_RELAY_MSG_SHADOW13_PHYS                                      (MPSS_PERPH_REG_BASE_PHYS + 0x00000434)
#define HWIO_MSS_RELAY_MSG_SHADOW13_RMSK                                      0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW13_IN          \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW13_ADDR, HWIO_MSS_RELAY_MSG_SHADOW13_RMSK)
#define HWIO_MSS_RELAY_MSG_SHADOW13_INM(m)      \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW13_ADDR, m)
#define HWIO_MSS_RELAY_MSG_SHADOW13_OUT(v)      \
        out_dword(HWIO_MSS_RELAY_MSG_SHADOW13_ADDR,v)
#define HWIO_MSS_RELAY_MSG_SHADOW13_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_RELAY_MSG_SHADOW13_ADDR,m,v,HWIO_MSS_RELAY_MSG_SHADOW13_IN)
#define HWIO_MSS_RELAY_MSG_SHADOW13_RELAY_MSG_SHADOW13_BMSK                   0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW13_RELAY_MSG_SHADOW13_SHFT                          0x0

#define HWIO_MSS_RELAY_MSG_SHADOW14_ADDR                                      (MPSS_PERPH_REG_BASE      + 0x00000438)
#define HWIO_MSS_RELAY_MSG_SHADOW14_PHYS                                      (MPSS_PERPH_REG_BASE_PHYS + 0x00000438)
#define HWIO_MSS_RELAY_MSG_SHADOW14_RMSK                                      0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW14_IN          \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW14_ADDR, HWIO_MSS_RELAY_MSG_SHADOW14_RMSK)
#define HWIO_MSS_RELAY_MSG_SHADOW14_INM(m)      \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW14_ADDR, m)
#define HWIO_MSS_RELAY_MSG_SHADOW14_OUT(v)      \
        out_dword(HWIO_MSS_RELAY_MSG_SHADOW14_ADDR,v)
#define HWIO_MSS_RELAY_MSG_SHADOW14_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_RELAY_MSG_SHADOW14_ADDR,m,v,HWIO_MSS_RELAY_MSG_SHADOW14_IN)
#define HWIO_MSS_RELAY_MSG_SHADOW14_RELAY_MSG_SHADOW14_BMSK                   0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW14_RELAY_MSG_SHADOW14_SHFT                          0x0

#define HWIO_MSS_RELAY_MSG_SHADOW15_ADDR                                      (MPSS_PERPH_REG_BASE      + 0x0000043c)
#define HWIO_MSS_RELAY_MSG_SHADOW15_PHYS                                      (MPSS_PERPH_REG_BASE_PHYS + 0x0000043c)
#define HWIO_MSS_RELAY_MSG_SHADOW15_RMSK                                      0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW15_IN          \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW15_ADDR, HWIO_MSS_RELAY_MSG_SHADOW15_RMSK)
#define HWIO_MSS_RELAY_MSG_SHADOW15_INM(m)      \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW15_ADDR, m)
#define HWIO_MSS_RELAY_MSG_SHADOW15_OUT(v)      \
        out_dword(HWIO_MSS_RELAY_MSG_SHADOW15_ADDR,v)
#define HWIO_MSS_RELAY_MSG_SHADOW15_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_RELAY_MSG_SHADOW15_ADDR,m,v,HWIO_MSS_RELAY_MSG_SHADOW15_IN)
#define HWIO_MSS_RELAY_MSG_SHADOW15_RELAY_MSG_SHADOW15_BMSK                   0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW15_RELAY_MSG_SHADOW15_SHFT                          0x0

#define HWIO_MSS_OFFLINE_PLL_MODE_ADDR                                        (MPSS_PERPH_REG_BASE      + 0x00001000)
#define HWIO_MSS_OFFLINE_PLL_MODE_PHYS                                        (MPSS_PERPH_REG_BASE_PHYS + 0x00001000)
#define HWIO_MSS_OFFLINE_PLL_MODE_RMSK                                        0xffffffff
#define HWIO_MSS_OFFLINE_PLL_MODE_IN          \
        in_dword_masked(HWIO_MSS_OFFLINE_PLL_MODE_ADDR, HWIO_MSS_OFFLINE_PLL_MODE_RMSK)
#define HWIO_MSS_OFFLINE_PLL_MODE_INM(m)      \
        in_dword_masked(HWIO_MSS_OFFLINE_PLL_MODE_ADDR, m)
#define HWIO_MSS_OFFLINE_PLL_MODE_OUT(v)      \
        out_dword(HWIO_MSS_OFFLINE_PLL_MODE_ADDR,v)
#define HWIO_MSS_OFFLINE_PLL_MODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_OFFLINE_PLL_MODE_ADDR,m,v,HWIO_MSS_OFFLINE_PLL_MODE_IN)
#define HWIO_MSS_OFFLINE_PLL_MODE_PLL_LOCK_DET_BMSK                           0x80000000
#define HWIO_MSS_OFFLINE_PLL_MODE_PLL_LOCK_DET_SHFT                                 0x1f
#define HWIO_MSS_OFFLINE_PLL_MODE_RESERVE_BIT30_BMSK                          0x40000000
#define HWIO_MSS_OFFLINE_PLL_MODE_RESERVE_BIT30_SHFT                                0x1e
#define HWIO_MSS_OFFLINE_PLL_MODE_PLL_ACK_LATCH_BMSK                          0x20000000
#define HWIO_MSS_OFFLINE_PLL_MODE_PLL_ACK_LATCH_SHFT                                0x1d
#define HWIO_MSS_OFFLINE_PLL_MODE_RESERVE_BIT28_24_BMSK                       0x1f000000
#define HWIO_MSS_OFFLINE_PLL_MODE_RESERVE_BIT28_24_SHFT                             0x18
#define HWIO_MSS_OFFLINE_PLL_MODE_PLL_HW_UPDATE_LOGIC_BYPASS_BMSK               0x800000
#define HWIO_MSS_OFFLINE_PLL_MODE_PLL_HW_UPDATE_LOGIC_BYPASS_SHFT                   0x17
#define HWIO_MSS_OFFLINE_PLL_MODE_PLL_UPDATE_BMSK                               0x400000
#define HWIO_MSS_OFFLINE_PLL_MODE_PLL_UPDATE_SHFT                                   0x16
#define HWIO_MSS_OFFLINE_PLL_MODE_RESERVE_BIT21_4_BMSK                          0x3ffff0
#define HWIO_MSS_OFFLINE_PLL_MODE_RESERVE_BIT21_4_SHFT                               0x4
#define HWIO_MSS_OFFLINE_PLL_MODE_PLL_PLLTEST_BMSK                                   0x8
#define HWIO_MSS_OFFLINE_PLL_MODE_PLL_PLLTEST_SHFT                                   0x3
#define HWIO_MSS_OFFLINE_PLL_MODE_PLL_RESET_N_BMSK                                   0x4
#define HWIO_MSS_OFFLINE_PLL_MODE_PLL_RESET_N_SHFT                                   0x2
#define HWIO_MSS_OFFLINE_PLL_MODE_PLL_BYPASSNL_BMSK                                  0x2
#define HWIO_MSS_OFFLINE_PLL_MODE_PLL_BYPASSNL_SHFT                                  0x1
#define HWIO_MSS_OFFLINE_PLL_MODE_PLL_OUTCTRL_BMSK                                   0x1
#define HWIO_MSS_OFFLINE_PLL_MODE_PLL_OUTCTRL_SHFT                                   0x0

#define HWIO_MSS_OFFLINE_PLL_L_VAL_ADDR                                       (MPSS_PERPH_REG_BASE      + 0x00001004)
#define HWIO_MSS_OFFLINE_PLL_L_VAL_PHYS                                       (MPSS_PERPH_REG_BASE_PHYS + 0x00001004)
#define HWIO_MSS_OFFLINE_PLL_L_VAL_RMSK                                           0xffff
#define HWIO_MSS_OFFLINE_PLL_L_VAL_IN          \
        in_dword_masked(HWIO_MSS_OFFLINE_PLL_L_VAL_ADDR, HWIO_MSS_OFFLINE_PLL_L_VAL_RMSK)
#define HWIO_MSS_OFFLINE_PLL_L_VAL_INM(m)      \
        in_dword_masked(HWIO_MSS_OFFLINE_PLL_L_VAL_ADDR, m)
#define HWIO_MSS_OFFLINE_PLL_L_VAL_OUT(v)      \
        out_dword(HWIO_MSS_OFFLINE_PLL_L_VAL_ADDR,v)
#define HWIO_MSS_OFFLINE_PLL_L_VAL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_OFFLINE_PLL_L_VAL_ADDR,m,v,HWIO_MSS_OFFLINE_PLL_L_VAL_IN)
#define HWIO_MSS_OFFLINE_PLL_L_VAL_PLL_L_BMSK                                     0xffff
#define HWIO_MSS_OFFLINE_PLL_L_VAL_PLL_L_SHFT                                        0x0

#define HWIO_MSS_OFFLINE_PLL_ALPHA_VAL_ADDR                                   (MPSS_PERPH_REG_BASE      + 0x00001008)
#define HWIO_MSS_OFFLINE_PLL_ALPHA_VAL_PHYS                                   (MPSS_PERPH_REG_BASE_PHYS + 0x00001008)
#define HWIO_MSS_OFFLINE_PLL_ALPHA_VAL_RMSK                                   0xffffffff
#define HWIO_MSS_OFFLINE_PLL_ALPHA_VAL_IN          \
        in_dword_masked(HWIO_MSS_OFFLINE_PLL_ALPHA_VAL_ADDR, HWIO_MSS_OFFLINE_PLL_ALPHA_VAL_RMSK)
#define HWIO_MSS_OFFLINE_PLL_ALPHA_VAL_INM(m)      \
        in_dword_masked(HWIO_MSS_OFFLINE_PLL_ALPHA_VAL_ADDR, m)
#define HWIO_MSS_OFFLINE_PLL_ALPHA_VAL_OUT(v)      \
        out_dword(HWIO_MSS_OFFLINE_PLL_ALPHA_VAL_ADDR,v)
#define HWIO_MSS_OFFLINE_PLL_ALPHA_VAL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_OFFLINE_PLL_ALPHA_VAL_ADDR,m,v,HWIO_MSS_OFFLINE_PLL_ALPHA_VAL_IN)
#define HWIO_MSS_OFFLINE_PLL_ALPHA_VAL_PLL_ALPHA_31_0_BMSK                    0xffffffff
#define HWIO_MSS_OFFLINE_PLL_ALPHA_VAL_PLL_ALPHA_31_0_SHFT                           0x0

#define HWIO_MSS_OFFLINE_PLL_ALPHA_VAL_U_ADDR                                 (MPSS_PERPH_REG_BASE      + 0x0000100c)
#define HWIO_MSS_OFFLINE_PLL_ALPHA_VAL_U_PHYS                                 (MPSS_PERPH_REG_BASE_PHYS + 0x0000100c)
#define HWIO_MSS_OFFLINE_PLL_ALPHA_VAL_U_RMSK                                       0xff
#define HWIO_MSS_OFFLINE_PLL_ALPHA_VAL_U_IN          \
        in_dword_masked(HWIO_MSS_OFFLINE_PLL_ALPHA_VAL_U_ADDR, HWIO_MSS_OFFLINE_PLL_ALPHA_VAL_U_RMSK)
#define HWIO_MSS_OFFLINE_PLL_ALPHA_VAL_U_INM(m)      \
        in_dword_masked(HWIO_MSS_OFFLINE_PLL_ALPHA_VAL_U_ADDR, m)
#define HWIO_MSS_OFFLINE_PLL_ALPHA_VAL_U_OUT(v)      \
        out_dword(HWIO_MSS_OFFLINE_PLL_ALPHA_VAL_U_ADDR,v)
#define HWIO_MSS_OFFLINE_PLL_ALPHA_VAL_U_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_OFFLINE_PLL_ALPHA_VAL_U_ADDR,m,v,HWIO_MSS_OFFLINE_PLL_ALPHA_VAL_U_IN)
#define HWIO_MSS_OFFLINE_PLL_ALPHA_VAL_U_PLL_ALPHA_39_32_BMSK                       0xff
#define HWIO_MSS_OFFLINE_PLL_ALPHA_VAL_U_PLL_ALPHA_39_32_SHFT                        0x0

#define HWIO_MSS_OFFLINE_PLL_USER_CTL_ADDR                                    (MPSS_PERPH_REG_BASE      + 0x00001010)
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_PHYS                                    (MPSS_PERPH_REG_BASE_PHYS + 0x00001010)
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_RMSK                                    0xffffffff
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_IN          \
        in_dword_masked(HWIO_MSS_OFFLINE_PLL_USER_CTL_ADDR, HWIO_MSS_OFFLINE_PLL_USER_CTL_RMSK)
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_INM(m)      \
        in_dword_masked(HWIO_MSS_OFFLINE_PLL_USER_CTL_ADDR, m)
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_OUT(v)      \
        out_dword(HWIO_MSS_OFFLINE_PLL_USER_CTL_ADDR,v)
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_OFFLINE_PLL_USER_CTL_ADDR,m,v,HWIO_MSS_OFFLINE_PLL_USER_CTL_IN)
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_RESERVE_BIT31_28_BMSK                   0xf0000000
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_RESERVE_BIT31_28_SHFT                         0x1c
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_SSC_MODE_CONTROL_BMSK                    0x8000000
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_SSC_MODE_CONTROL_SHFT                         0x1b
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_RESERVE_BITS26_25_BMSK                   0x6000000
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_RESERVE_BITS26_25_SHFT                        0x19
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_ALPHA_EN_BMSK                            0x1000000
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_ALPHA_EN_SHFT                                 0x18
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_RESERVE_BITS23_22_BMSK                    0xc00000
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_RESERVE_BITS23_22_SHFT                        0x16
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_VCO_SEL_BMSK                              0x300000
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_VCO_SEL_SHFT                                  0x14
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_RESERVE_BITS19_15_BMSK                     0xf8000
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_RESERVE_BITS19_15_SHFT                         0xf
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_PRE_DIV_RATIO_BMSK                          0x7000
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_PRE_DIV_RATIO_SHFT                             0xc
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_POST_DIV_RATIO_BMSK                          0xf00
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_POST_DIV_RATIO_SHFT                            0x8
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_OUTPUT_INV_BMSK                               0x80
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_OUTPUT_INV_SHFT                                0x7
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_RESERVE_BITS6_5_BMSK                          0x60
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_RESERVE_BITS6_5_SHFT                           0x5
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_PLLOUT_LV_TEST_BMSK                           0x10
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_PLLOUT_LV_TEST_SHFT                            0x4
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_PLLOUT_LV_EARLY_BMSK                           0x8
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_PLLOUT_LV_EARLY_SHFT                           0x3
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_PLLOUT_LV_AUX2_BMSK                            0x4
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_PLLOUT_LV_AUX2_SHFT                            0x2
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_PLLOUT_LV_AUX_BMSK                             0x2
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_PLLOUT_LV_AUX_SHFT                             0x1
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_PLLOUT_LV_MAIN_BMSK                            0x1
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_PLLOUT_LV_MAIN_SHFT                            0x0

#define HWIO_MSS_OFFLINE_PLL_USER_CTL_U_ADDR                                  (MPSS_PERPH_REG_BASE      + 0x00001014)
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_U_PHYS                                  (MPSS_PERPH_REG_BASE_PHYS + 0x00001014)
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_U_RMSK                                  0xffffffff
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_U_IN          \
        in_dword_masked(HWIO_MSS_OFFLINE_PLL_USER_CTL_U_ADDR, HWIO_MSS_OFFLINE_PLL_USER_CTL_U_RMSK)
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_U_INM(m)      \
        in_dword_masked(HWIO_MSS_OFFLINE_PLL_USER_CTL_U_ADDR, m)
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_U_OUT(v)      \
        out_dword(HWIO_MSS_OFFLINE_PLL_USER_CTL_U_ADDR,v)
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_U_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_OFFLINE_PLL_USER_CTL_U_ADDR,m,v,HWIO_MSS_OFFLINE_PLL_USER_CTL_U_IN)
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_U_RESERVE_BITS31_12_BMSK                0xfffff000
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_U_RESERVE_BITS31_12_SHFT                       0xc
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_U_LATCH_INTERFACE_BMSK                       0x800
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_U_LATCH_INTERFACE_SHFT                         0xb
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_U_STATUS_REGISTER_BMSK                       0x700
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_U_STATUS_REGISTER_SHFT                         0x8
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_U_DSM_BMSK                                    0x80
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_U_DSM_SHFT                                     0x7
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_U_WRITE_STATE_BMSK                            0x40
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_U_WRITE_STATE_SHFT                             0x6
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_U_TARGET_CTL_BMSK                             0x38
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_U_TARGET_CTL_SHFT                              0x3
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_U_LOCKDET_BMSK                                 0x4
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_U_LOCKDET_SHFT                                 0x2
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_U_FREEZE_PLL_BMSK                              0x2
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_U_FREEZE_PLL_SHFT                              0x1
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_U_TOGGLE_DET_BMSK                              0x1
#define HWIO_MSS_OFFLINE_PLL_USER_CTL_U_TOGGLE_DET_SHFT                              0x0

#define HWIO_MSS_OFFLINE_PLL_CONFIG_CTL_ADDR                                  (MPSS_PERPH_REG_BASE      + 0x00001018)
#define HWIO_MSS_OFFLINE_PLL_CONFIG_CTL_PHYS                                  (MPSS_PERPH_REG_BASE_PHYS + 0x00001018)
#define HWIO_MSS_OFFLINE_PLL_CONFIG_CTL_RMSK                                  0xffffffff
#define HWIO_MSS_OFFLINE_PLL_CONFIG_CTL_IN          \
        in_dword_masked(HWIO_MSS_OFFLINE_PLL_CONFIG_CTL_ADDR, HWIO_MSS_OFFLINE_PLL_CONFIG_CTL_RMSK)
#define HWIO_MSS_OFFLINE_PLL_CONFIG_CTL_INM(m)      \
        in_dword_masked(HWIO_MSS_OFFLINE_PLL_CONFIG_CTL_ADDR, m)
#define HWIO_MSS_OFFLINE_PLL_CONFIG_CTL_OUT(v)      \
        out_dword(HWIO_MSS_OFFLINE_PLL_CONFIG_CTL_ADDR,v)
#define HWIO_MSS_OFFLINE_PLL_CONFIG_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_OFFLINE_PLL_CONFIG_CTL_ADDR,m,v,HWIO_MSS_OFFLINE_PLL_CONFIG_CTL_IN)
#define HWIO_MSS_OFFLINE_PLL_CONFIG_CTL_SINGLE_DMET_MODE_ENABLE_BMSK          0x80000000
#define HWIO_MSS_OFFLINE_PLL_CONFIG_CTL_SINGLE_DMET_MODE_ENABLE_SHFT                0x1f
#define HWIO_MSS_OFFLINE_PLL_CONFIG_CTL_DMET_WINDOW_ENABLE_BMSK               0x40000000
#define HWIO_MSS_OFFLINE_PLL_CONFIG_CTL_DMET_WINDOW_ENABLE_SHFT                     0x1e
#define HWIO_MSS_OFFLINE_PLL_CONFIG_CTL_TOGGLE_DET_SAMPLE_INTER_BMSK          0x3c000000
#define HWIO_MSS_OFFLINE_PLL_CONFIG_CTL_TOGGLE_DET_SAMPLE_INTER_SHFT                0x1a
#define HWIO_MSS_OFFLINE_PLL_CONFIG_CTL_TOGGLE_DET_THRESHOLD_BMSK              0x3800000
#define HWIO_MSS_OFFLINE_PLL_CONFIG_CTL_TOGGLE_DET_THRESHOLD_SHFT                   0x17
#define HWIO_MSS_OFFLINE_PLL_CONFIG_CTL_TOGGLE_DET_SAMPLE_BMSK                  0x700000
#define HWIO_MSS_OFFLINE_PLL_CONFIG_CTL_TOGGLE_DET_SAMPLE_SHFT                      0x14
#define HWIO_MSS_OFFLINE_PLL_CONFIG_CTL_LOCK_DET_THRESHOLD_BMSK                  0xff000
#define HWIO_MSS_OFFLINE_PLL_CONFIG_CTL_LOCK_DET_THRESHOLD_SHFT                      0xc
#define HWIO_MSS_OFFLINE_PLL_CONFIG_CTL_LOCK_DET_SAMPLE_SIZE_BMSK                  0xf00
#define HWIO_MSS_OFFLINE_PLL_CONFIG_CTL_LOCK_DET_SAMPLE_SIZE_SHFT                    0x8
#define HWIO_MSS_OFFLINE_PLL_CONFIG_CTL_RESERVE_BITS7_6_BMSK                        0xc0
#define HWIO_MSS_OFFLINE_PLL_CONFIG_CTL_RESERVE_BITS7_6_SHFT                         0x6
#define HWIO_MSS_OFFLINE_PLL_CONFIG_CTL_REF_CYCLE_BMSK                              0x30
#define HWIO_MSS_OFFLINE_PLL_CONFIG_CTL_REF_CYCLE_SHFT                               0x4
#define HWIO_MSS_OFFLINE_PLL_CONFIG_CTL_KFN_BMSK                                     0xf
#define HWIO_MSS_OFFLINE_PLL_CONFIG_CTL_KFN_SHFT                                     0x0

#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_ADDR                                    (MPSS_PERPH_REG_BASE      + 0x0000101c)
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_PHYS                                    (MPSS_PERPH_REG_BASE_PHYS + 0x0000101c)
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_RMSK                                    0xffffffff
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_IN          \
        in_dword_masked(HWIO_MSS_OFFLINE_PLL_TEST_CTL_ADDR, HWIO_MSS_OFFLINE_PLL_TEST_CTL_RMSK)
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_INM(m)      \
        in_dword_masked(HWIO_MSS_OFFLINE_PLL_TEST_CTL_ADDR, m)
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_OUT(v)      \
        out_dword(HWIO_MSS_OFFLINE_PLL_TEST_CTL_ADDR,v)
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_OFFLINE_PLL_TEST_CTL_ADDR,m,v,HWIO_MSS_OFFLINE_PLL_TEST_CTL_IN)
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_BIAS_GEN_TRIM_BMSK                      0xe0000000
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_BIAS_GEN_TRIM_SHFT                            0x1d
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_PROCESS_CALB_BMSK                       0x1c000000
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_PROCESS_CALB_SHFT                             0x1a
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_OVERRIDE_PROCESS_CALB_BMSK               0x2000000
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_OVERRIDE_PROCESS_CALB_SHFT                    0x19
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_FINE_FCW_BMSK                            0x1f00000
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_FINE_FCW_SHFT                                 0x14
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_OVERRIDE_FINE_FCW_BMSK                     0x80000
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_OVERRIDE_FINE_FCW_SHFT                        0x13
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_COARSE_FCW_BMSK                            0x7e000
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_COARSE_FCW_SHFT                                0xd
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_OVERRIDE_COARSE_BMSK                        0x1000
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_OVERRIDE_COARSE_SHFT                           0xc
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_DISABLE_LFSR_BMSK                            0x800
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_DISABLE_LFSR_SHFT                              0xb
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_DTEST_SEL_BMSK                               0x700
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_DTEST_SEL_SHFT                                 0x8
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_DTEST_EN_BMSK                                 0x80
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_DTEST_EN_SHFT                                  0x7
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_BYP_TESTAMP_BMSK                              0x40
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_BYP_TESTAMP_SHFT                               0x6
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_ATEST1_SEL_BMSK                               0x30
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_ATEST1_SEL_SHFT                                0x4
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_ATEST0_SEL_BMSK                                0xc
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_ATEST0_SEL_SHFT                                0x2
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_ATEST1_EN_BMSK                                 0x2
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_ATEST1_EN_SHFT                                 0x1
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_ATEST0_EN_BMSK                                 0x1
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_ATEST0_EN_SHFT                                 0x0

#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_U_ADDR                                  (MPSS_PERPH_REG_BASE      + 0x00001020)
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_U_PHYS                                  (MPSS_PERPH_REG_BASE_PHYS + 0x00001020)
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_U_RMSK                                  0xfffffeff
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_U_IN          \
        in_dword_masked(HWIO_MSS_OFFLINE_PLL_TEST_CTL_U_ADDR, HWIO_MSS_OFFLINE_PLL_TEST_CTL_U_RMSK)
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_U_INM(m)      \
        in_dword_masked(HWIO_MSS_OFFLINE_PLL_TEST_CTL_U_ADDR, m)
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_U_OUT(v)      \
        out_dword(HWIO_MSS_OFFLINE_PLL_TEST_CTL_U_ADDR,v)
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_U_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_OFFLINE_PLL_TEST_CTL_U_ADDR,m,v,HWIO_MSS_OFFLINE_PLL_TEST_CTL_U_IN)
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_U_RESERVE_BIT31_22_BMSK                 0xffc00000
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_U_RESERVE_BIT31_22_SHFT                       0x16
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_U_GLITCH_DET_CNT_BMSK                     0x300000
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_U_GLITCH_DET_CNT_SHFT                         0x14
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_U_DISABLE_GLITCH_DET_BMSK                  0x80000
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_U_DISABLE_GLITCH_DET_SHFT                     0x13
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_U_DTEST_MODE_SEL_EXT_BMSK                  0x60000
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_U_DTEST_MODE_SEL_EXT_SHFT                     0x11
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_U_DITHER_SEL_BMSK                          0x18000
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_U_DITHER_SEL_SHFT                              0xf
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_U_PROCESS_CALB_SEL_BMSK                     0x4000
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_U_PROCESS_CALB_SEL_SHFT                        0xe
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_U_OVERRIDE_FINE_FCW_MSB_BMSK                0x2000
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_U_OVERRIDE_FINE_FCW_MSB_SHFT                   0xd
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_U_DTEST_MODE_SEL_BMSK                       0x1800
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_U_DTEST_MODE_SEL_SHFT                          0xb
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_U_NMO_OSC_SEL_BMSK                           0x600
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_U_NMO_OSC_SEL_SHFT                             0x9
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_U_NOISE_MAG_BMSK                              0xe0
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_U_NOISE_MAG_SHFT                               0x5
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_U_NOISE_GEN_BMSK                              0x10
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_U_NOISE_GEN_SHFT                               0x4
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_U_OSC_BIAS_GND_BMSK                            0x8
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_U_OSC_BIAS_GND_SHFT                            0x3
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_U_PLL_TEST_OUT_SEL_BMSK                        0x6
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_U_PLL_TEST_OUT_SEL_SHFT                        0x1
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_U_CAL_CODE_UPDATE_BMSK                         0x1
#define HWIO_MSS_OFFLINE_PLL_TEST_CTL_U_CAL_CODE_UPDATE_SHFT                         0x0

#define HWIO_MSS_OFFLINE_PLL_STATUS_ADDR                                      (MPSS_PERPH_REG_BASE      + 0x00001024)
#define HWIO_MSS_OFFLINE_PLL_STATUS_PHYS                                      (MPSS_PERPH_REG_BASE_PHYS + 0x00001024)
#define HWIO_MSS_OFFLINE_PLL_STATUS_RMSK                                      0xffffffff
#define HWIO_MSS_OFFLINE_PLL_STATUS_IN          \
        in_dword_masked(HWIO_MSS_OFFLINE_PLL_STATUS_ADDR, HWIO_MSS_OFFLINE_PLL_STATUS_RMSK)
#define HWIO_MSS_OFFLINE_PLL_STATUS_INM(m)      \
        in_dword_masked(HWIO_MSS_OFFLINE_PLL_STATUS_ADDR, m)
#define HWIO_MSS_OFFLINE_PLL_STATUS_STATUS_31_0_BMSK                          0xffffffff
#define HWIO_MSS_OFFLINE_PLL_STATUS_STATUS_31_0_SHFT                                 0x0

#define HWIO_MSS_OFFLINE_PLL_FREQ_CTL_ADDR                                    (MPSS_PERPH_REG_BASE      + 0x00001028)
#define HWIO_MSS_OFFLINE_PLL_FREQ_CTL_PHYS                                    (MPSS_PERPH_REG_BASE_PHYS + 0x00001028)
#define HWIO_MSS_OFFLINE_PLL_FREQ_CTL_RMSK                                    0xffffffff
#define HWIO_MSS_OFFLINE_PLL_FREQ_CTL_IN          \
        in_dword_masked(HWIO_MSS_OFFLINE_PLL_FREQ_CTL_ADDR, HWIO_MSS_OFFLINE_PLL_FREQ_CTL_RMSK)
#define HWIO_MSS_OFFLINE_PLL_FREQ_CTL_INM(m)      \
        in_dword_masked(HWIO_MSS_OFFLINE_PLL_FREQ_CTL_ADDR, m)
#define HWIO_MSS_OFFLINE_PLL_FREQ_CTL_OUT(v)      \
        out_dword(HWIO_MSS_OFFLINE_PLL_FREQ_CTL_ADDR,v)
#define HWIO_MSS_OFFLINE_PLL_FREQ_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_OFFLINE_PLL_FREQ_CTL_ADDR,m,v,HWIO_MSS_OFFLINE_PLL_FREQ_CTL_IN)
#define HWIO_MSS_OFFLINE_PLL_FREQ_CTL_PLL_FREQ_CONTROL_WORD_BMSK              0xffffffff
#define HWIO_MSS_OFFLINE_PLL_FREQ_CTL_PLL_FREQ_CONTROL_WORD_SHFT                     0x0

#define HWIO_MSS_CLAMP_MEM_ADDR                                               (MPSS_PERPH_REG_BASE      + 0x0000102c)
#define HWIO_MSS_CLAMP_MEM_PHYS                                               (MPSS_PERPH_REG_BASE_PHYS + 0x0000102c)
#define HWIO_MSS_CLAMP_MEM_RMSK                                                      0xf
#define HWIO_MSS_CLAMP_MEM_IN          \
        in_dword_masked(HWIO_MSS_CLAMP_MEM_ADDR, HWIO_MSS_CLAMP_MEM_RMSK)
#define HWIO_MSS_CLAMP_MEM_INM(m)      \
        in_dword_masked(HWIO_MSS_CLAMP_MEM_ADDR, m)
#define HWIO_MSS_CLAMP_MEM_OUT(v)      \
        out_dword(HWIO_MSS_CLAMP_MEM_ADDR,v)
#define HWIO_MSS_CLAMP_MEM_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_CLAMP_MEM_ADDR,m,v,HWIO_MSS_CLAMP_MEM_IN)
#define HWIO_MSS_CLAMP_MEM_UNCLAMP_ALL_BMSK                                          0x8
#define HWIO_MSS_CLAMP_MEM_UNCLAMP_ALL_SHFT                                          0x3
#define HWIO_MSS_CLAMP_MEM_SPARE_2_TO_1_BMSK                                         0x6
#define HWIO_MSS_CLAMP_MEM_SPARE_2_TO_1_SHFT                                         0x1
#define HWIO_MSS_CLAMP_MEM_CLAMP_MEM_BMSK                                            0x1
#define HWIO_MSS_CLAMP_MEM_CLAMP_MEM_SHFT                                            0x0

#define HWIO_MSS_CLAMP_IO_ADDR                                                (MPSS_PERPH_REG_BASE      + 0x00001030)
#define HWIO_MSS_CLAMP_IO_PHYS                                                (MPSS_PERPH_REG_BASE_PHYS + 0x00001030)
#define HWIO_MSS_CLAMP_IO_RMSK                                                       0xf
#define HWIO_MSS_CLAMP_IO_IN          \
        in_dword_masked(HWIO_MSS_CLAMP_IO_ADDR, HWIO_MSS_CLAMP_IO_RMSK)
#define HWIO_MSS_CLAMP_IO_INM(m)      \
        in_dword_masked(HWIO_MSS_CLAMP_IO_ADDR, m)
#define HWIO_MSS_CLAMP_IO_OUT(v)      \
        out_dword(HWIO_MSS_CLAMP_IO_ADDR,v)
#define HWIO_MSS_CLAMP_IO_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_CLAMP_IO_ADDR,m,v,HWIO_MSS_CLAMP_IO_IN)
#define HWIO_MSS_CLAMP_IO_UNCLAMP_ALL_BMSK                                           0x8
#define HWIO_MSS_CLAMP_IO_UNCLAMP_ALL_SHFT                                           0x3
#define HWIO_MSS_CLAMP_IO_SPARE_2_TO_1_BMSK                                          0x6
#define HWIO_MSS_CLAMP_IO_SPARE_2_TO_1_SHFT                                          0x1
#define HWIO_MSS_CLAMP_IO_MODEM_BMSK                                                 0x1
#define HWIO_MSS_CLAMP_IO_MODEM_SHFT                                                 0x0

#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL_ADDR                                    (MPSS_PERPH_REG_BASE      + 0x00001034)
#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL_PHYS                                    (MPSS_PERPH_REG_BASE_PHYS + 0x00001034)
#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL_RMSK                                    0xffffffff
#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL_IN          \
        in_dword_masked(HWIO_MSS_OFFLINE_MEM_SLP_CNTL_ADDR, HWIO_MSS_OFFLINE_MEM_SLP_CNTL_RMSK)
#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL_INM(m)      \
        in_dword_masked(HWIO_MSS_OFFLINE_MEM_SLP_CNTL_ADDR, m)
#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL_OUT(v)      \
        out_dword(HWIO_MSS_OFFLINE_MEM_SLP_CNTL_ADDR,v)
#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_OFFLINE_MEM_SLP_CNTL_ADDR,m,v,HWIO_MSS_OFFLINE_MEM_SLP_CNTL_IN)
#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL_CLUSTER_D_SLP_NRET_N_BMSK               0xc0000000
#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL_CLUSTER_D_SLP_NRET_N_SHFT                     0x1e
#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL_CLUSTER_D_SLP_RET_N_BMSK                0x20000000
#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL_CLUSTER_D_SLP_RET_N_SHFT                      0x1d
#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL_CLUSTER_C_SLP_NRET_N_BMSK               0x18000000
#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL_CLUSTER_C_SLP_NRET_N_SHFT                     0x1b
#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL_CLUSTER_C_SLP_RET_N_BMSK                 0x4000000
#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL_CLUSTER_C_SLP_RET_N_SHFT                      0x1a
#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL_CLUSTER_B0_SLP_NRET_N_BMSK               0x3000000
#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL_CLUSTER_B0_SLP_NRET_N_SHFT                    0x18
#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL_CLUSTER_B0_SLP_RET_N_BMSK                 0x800000
#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL_CLUSTER_B0_SLP_RET_N_SHFT                     0x17
#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL_CLUSTER_A_SLP_NRET_N_BMSK                 0x600000
#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL_CLUSTER_A_SLP_NRET_N_SHFT                     0x15
#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL_CLUSTER_A_SLP_RET_N_BMSK                  0x100000
#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL_CLUSTER_A_SLP_RET_N_SHFT                      0x14
#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL_SLICE_D_DMEM_NRET_N_BMSK                   0xc0000
#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL_SLICE_D_DMEM_NRET_N_SHFT                      0x12
#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL_SLICE_D_DMEM_RET_N_BMSK                    0x30000
#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL_SLICE_D_DMEM_RET_N_SHFT                       0x10
#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL_SLICE_C_DMEM_NRET_N_BMSK                    0xc000
#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL_SLICE_C_DMEM_NRET_N_SHFT                       0xe
#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL_SLICE_C_DMEM_RET_N_BMSK                     0x3000
#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL_SLICE_C_DMEM_RET_N_SHFT                        0xc
#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL_SLICE_B0_DMEM_NRET_N_BMSK                    0xc00
#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL_SLICE_B0_DMEM_NRET_N_SHFT                      0xa
#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL_SLICE_B0_DMEM_RET_N_BMSK                     0x300
#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL_SLICE_B0_DMEM_RET_N_SHFT                       0x8
#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL_SLICE_A_DMEM_NRET_N_BMSK                      0xe0
#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL_SLICE_A_DMEM_NRET_N_SHFT                       0x5
#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL_SLICE_A_DMEM_RET_N_BMSK                       0x1c
#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL_SLICE_A_DMEM_RET_N_SHFT                        0x2
#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL_DB_CTL_PDMEM_SLP_NRET_N_BMSK                   0x2
#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL_DB_CTL_PDMEM_SLP_NRET_N_SHFT                   0x1
#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL_DB_CTL_PDMEM_SLP_RET_N_BMSK                    0x1
#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL_DB_CTL_PDMEM_SLP_RET_N_SHFT                    0x0

#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL2_ADDR                                   (MPSS_PERPH_REG_BASE      + 0x00001038)
#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL2_PHYS                                   (MPSS_PERPH_REG_BASE_PHYS + 0x00001038)
#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL2_RMSK                                   0xffffffff
#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL2_IN          \
        in_dword_masked(HWIO_MSS_OFFLINE_MEM_SLP_CNTL2_ADDR, HWIO_MSS_OFFLINE_MEM_SLP_CNTL2_RMSK)
#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL2_INM(m)      \
        in_dword_masked(HWIO_MSS_OFFLINE_MEM_SLP_CNTL2_ADDR, m)
#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL2_OUT(v)      \
        out_dword(HWIO_MSS_OFFLINE_MEM_SLP_CNTL2_ADDR,v)
#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_OFFLINE_MEM_SLP_CNTL2_ADDR,m,v,HWIO_MSS_OFFLINE_MEM_SLP_CNTL2_IN)
#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL2_SPARE_31_12_BMSK                       0xfffff000
#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL2_SPARE_31_12_SHFT                              0xc
#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL2_PMEM_SLP_NRET_N_BMSK                        0xf00
#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL2_PMEM_SLP_NRET_N_SHFT                          0x8
#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL2_PMEM_SLP_RET_N_BMSK                          0xf0
#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL2_PMEM_SLP_RET_N_SHFT                           0x4
#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL2_COMMON_NRET_N_BMSK                            0x8
#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL2_COMMON_NRET_N_SHFT                            0x3
#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL2_COMMON_RET_N_BMSK                             0x4
#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL2_COMMON_RET_N_SHFT                             0x2
#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL2_SPARE_1_0_BMSK                                0x3
#define HWIO_MSS_OFFLINE_MEM_SLP_CNTL2_SPARE_1_0_SHFT                                0x0

#define HWIO_MSS_MODEM_BCR_ADDR                                               (MPSS_PERPH_REG_BASE      + 0x0000103c)
#define HWIO_MSS_MODEM_BCR_PHYS                                               (MPSS_PERPH_REG_BASE_PHYS + 0x0000103c)
#define HWIO_MSS_MODEM_BCR_RMSK                                               0x80000001
#define HWIO_MSS_MODEM_BCR_IN          \
        in_dword_masked(HWIO_MSS_MODEM_BCR_ADDR, HWIO_MSS_MODEM_BCR_RMSK)
#define HWIO_MSS_MODEM_BCR_INM(m)      \
        in_dword_masked(HWIO_MSS_MODEM_BCR_ADDR, m)
#define HWIO_MSS_MODEM_BCR_OUT(v)      \
        out_dword(HWIO_MSS_MODEM_BCR_ADDR,v)
#define HWIO_MSS_MODEM_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_MODEM_BCR_ADDR,m,v,HWIO_MSS_MODEM_BCR_IN)
#define HWIO_MSS_MODEM_BCR_STATUS_BMSK                                        0x80000000
#define HWIO_MSS_MODEM_BCR_STATUS_SHFT                                              0x1f
#define HWIO_MSS_MODEM_BCR_BLK_ARES_BMSK                                             0x1
#define HWIO_MSS_MODEM_BCR_BLK_ARES_SHFT                                             0x0

#define HWIO_MSS_AXI_MNOC_CBCR_ADDR                                           (MPSS_PERPH_REG_BASE      + 0x00001040)
#define HWIO_MSS_AXI_MNOC_CBCR_PHYS                                           (MPSS_PERPH_REG_BASE_PHYS + 0x00001040)
#define HWIO_MSS_AXI_MNOC_CBCR_RMSK                                           0x80000001
#define HWIO_MSS_AXI_MNOC_CBCR_IN          \
        in_dword_masked(HWIO_MSS_AXI_MNOC_CBCR_ADDR, HWIO_MSS_AXI_MNOC_CBCR_RMSK)
#define HWIO_MSS_AXI_MNOC_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_AXI_MNOC_CBCR_ADDR, m)
#define HWIO_MSS_AXI_MNOC_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_AXI_MNOC_CBCR_ADDR,v)
#define HWIO_MSS_AXI_MNOC_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_AXI_MNOC_CBCR_ADDR,m,v,HWIO_MSS_AXI_MNOC_CBCR_IN)
#define HWIO_MSS_AXI_MNOC_CBCR_CLKOFF_BMSK                                    0x80000000
#define HWIO_MSS_AXI_MNOC_CBCR_CLKOFF_SHFT                                          0x1f
#define HWIO_MSS_AXI_MNOC_CBCR_CLKEN_BMSK                                            0x1
#define HWIO_MSS_AXI_MNOC_CBCR_CLKEN_SHFT                                            0x0

#define HWIO_MSS_XO_MDM_CBCR_ADDR                                             (MPSS_PERPH_REG_BASE      + 0x00001044)
#define HWIO_MSS_XO_MDM_CBCR_PHYS                                             (MPSS_PERPH_REG_BASE_PHYS + 0x00001044)
#define HWIO_MSS_XO_MDM_CBCR_RMSK                                             0x80000001
#define HWIO_MSS_XO_MDM_CBCR_IN          \
        in_dword_masked(HWIO_MSS_XO_MDM_CBCR_ADDR, HWIO_MSS_XO_MDM_CBCR_RMSK)
#define HWIO_MSS_XO_MDM_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_XO_MDM_CBCR_ADDR, m)
#define HWIO_MSS_XO_MDM_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_XO_MDM_CBCR_ADDR,v)
#define HWIO_MSS_XO_MDM_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_XO_MDM_CBCR_ADDR,m,v,HWIO_MSS_XO_MDM_CBCR_IN)
#define HWIO_MSS_XO_MDM_CBCR_CLKOFF_BMSK                                      0x80000000
#define HWIO_MSS_XO_MDM_CBCR_CLKOFF_SHFT                                            0x1f
#define HWIO_MSS_XO_MDM_CBCR_CLKEN_BMSK                                              0x1
#define HWIO_MSS_XO_MDM_CBCR_CLKEN_SHFT                                              0x0

#define HWIO_MSS_BUS_CFG_XO_ADDR                                              (MPSS_PERPH_REG_BASE      + 0x00001048)
#define HWIO_MSS_BUS_CFG_XO_PHYS                                              (MPSS_PERPH_REG_BASE_PHYS + 0x00001048)
#define HWIO_MSS_BUS_CFG_XO_RMSK                                                     0x1
#define HWIO_MSS_BUS_CFG_XO_IN          \
        in_dword_masked(HWIO_MSS_BUS_CFG_XO_ADDR, HWIO_MSS_BUS_CFG_XO_RMSK)
#define HWIO_MSS_BUS_CFG_XO_INM(m)      \
        in_dword_masked(HWIO_MSS_BUS_CFG_XO_ADDR, m)
#define HWIO_MSS_BUS_CFG_XO_OUT(v)      \
        out_dword(HWIO_MSS_BUS_CFG_XO_ADDR,v)
#define HWIO_MSS_BUS_CFG_XO_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BUS_CFG_XO_ADDR,m,v,HWIO_MSS_BUS_CFG_XO_IN)
#define HWIO_MSS_BUS_CFG_XO_DISABLE_SPM_XO_EN_BMSK                                   0x1
#define HWIO_MSS_BUS_CFG_XO_DISABLE_SPM_XO_EN_SHFT                                   0x0

#define HWIO_MSS_FABRIC_AXI2AHB_IDLE_ADDR                                     (MPSS_PERPH_REG_BASE      + 0x0000104c)
#define HWIO_MSS_FABRIC_AXI2AHB_IDLE_PHYS                                     (MPSS_PERPH_REG_BASE_PHYS + 0x0000104c)
#define HWIO_MSS_FABRIC_AXI2AHB_IDLE_RMSK                                            0x3
#define HWIO_MSS_FABRIC_AXI2AHB_IDLE_IN          \
        in_dword_masked(HWIO_MSS_FABRIC_AXI2AHB_IDLE_ADDR, HWIO_MSS_FABRIC_AXI2AHB_IDLE_RMSK)
#define HWIO_MSS_FABRIC_AXI2AHB_IDLE_INM(m)      \
        in_dword_masked(HWIO_MSS_FABRIC_AXI2AHB_IDLE_ADDR, m)
#define HWIO_MSS_FABRIC_AXI2AHB_IDLE_AHB_S_IDLE_BMSK                                 0x2
#define HWIO_MSS_FABRIC_AXI2AHB_IDLE_AHB_S_IDLE_SHFT                                 0x1
#define HWIO_MSS_FABRIC_AXI2AHB_IDLE_AXI_M_IDLE_BMSK                                 0x1
#define HWIO_MSS_FABRIC_AXI2AHB_IDLE_AXI_M_IDLE_SHFT                                 0x0

#define HWIO_MSS_OFFLINE_PLL_ODIV_MBIST_CTL_ADDR                              (MPSS_PERPH_REG_BASE      + 0x00001050)
#define HWIO_MSS_OFFLINE_PLL_ODIV_MBIST_CTL_PHYS                              (MPSS_PERPH_REG_BASE_PHYS + 0x00001050)
#define HWIO_MSS_OFFLINE_PLL_ODIV_MBIST_CTL_RMSK                              0xffffffff
#define HWIO_MSS_OFFLINE_PLL_ODIV_MBIST_CTL_IN          \
        in_dword_masked(HWIO_MSS_OFFLINE_PLL_ODIV_MBIST_CTL_ADDR, HWIO_MSS_OFFLINE_PLL_ODIV_MBIST_CTL_RMSK)
#define HWIO_MSS_OFFLINE_PLL_ODIV_MBIST_CTL_INM(m)      \
        in_dword_masked(HWIO_MSS_OFFLINE_PLL_ODIV_MBIST_CTL_ADDR, m)
#define HWIO_MSS_OFFLINE_PLL_ODIV_MBIST_CTL_OUT(v)      \
        out_dword(HWIO_MSS_OFFLINE_PLL_ODIV_MBIST_CTL_ADDR,v)
#define HWIO_MSS_OFFLINE_PLL_ODIV_MBIST_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_OFFLINE_PLL_ODIV_MBIST_CTL_ADDR,m,v,HWIO_MSS_OFFLINE_PLL_ODIV_MBIST_CTL_IN)
#define HWIO_MSS_OFFLINE_PLL_ODIV_MBIST_CTL_SPARE_31_11_BMSK                  0xfffff800
#define HWIO_MSS_OFFLINE_PLL_ODIV_MBIST_CTL_SPARE_31_11_SHFT                         0xb
#define HWIO_MSS_OFFLINE_PLL_ODIV_MBIST_CTL_DIVIDER_SEL_BMSK                       0x400
#define HWIO_MSS_OFFLINE_PLL_ODIV_MBIST_CTL_DIVIDER_SEL_SHFT                         0xa
#define HWIO_MSS_OFFLINE_PLL_ODIV_MBIST_CTL_TDEC_ODIV_MBIST_BMSK                   0x3e0
#define HWIO_MSS_OFFLINE_PLL_ODIV_MBIST_CTL_TDEC_ODIV_MBIST_SHFT                     0x5
#define HWIO_MSS_OFFLINE_PLL_ODIV_MBIST_CTL_PROC_ODIV_MBIST_BMSK                    0x1f
#define HWIO_MSS_OFFLINE_PLL_ODIV_MBIST_CTL_PROC_ODIV_MBIST_SHFT                     0x0

#define HWIO_MSS_OFFLINE_CLK_CTL_ADDR                                         (MPSS_PERPH_REG_BASE      + 0x00001054)
#define HWIO_MSS_OFFLINE_CLK_CTL_PHYS                                         (MPSS_PERPH_REG_BASE_PHYS + 0x00001054)
#define HWIO_MSS_OFFLINE_CLK_CTL_RMSK                                                0x3
#define HWIO_MSS_OFFLINE_CLK_CTL_IN          \
        in_dword_masked(HWIO_MSS_OFFLINE_CLK_CTL_ADDR, HWIO_MSS_OFFLINE_CLK_CTL_RMSK)
#define HWIO_MSS_OFFLINE_CLK_CTL_INM(m)      \
        in_dword_masked(HWIO_MSS_OFFLINE_CLK_CTL_ADDR, m)
#define HWIO_MSS_OFFLINE_CLK_CTL_OUT(v)      \
        out_dword(HWIO_MSS_OFFLINE_CLK_CTL_ADDR,v)
#define HWIO_MSS_OFFLINE_CLK_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_OFFLINE_CLK_CTL_ADDR,m,v,HWIO_MSS_OFFLINE_CLK_CTL_IN)
#define HWIO_MSS_OFFLINE_CLK_CTL_CLK_TDEC_ENABLE_BMSK                                0x2
#define HWIO_MSS_OFFLINE_CLK_CTL_CLK_TDEC_ENABLE_SHFT                                0x1
#define HWIO_MSS_OFFLINE_CLK_CTL_CLK_PROC_MDM_ENABLE_BMSK                            0x1
#define HWIO_MSS_OFFLINE_CLK_CTL_CLK_PROC_MDM_ENABLE_SHFT                            0x0

#define HWIO_MSS_BUS_CMD_RCGR_ADDR                                            (MPSS_PERPH_REG_BASE      + 0x00002000)
#define HWIO_MSS_BUS_CMD_RCGR_PHYS                                            (MPSS_PERPH_REG_BASE_PHYS + 0x00002000)
#define HWIO_MSS_BUS_CMD_RCGR_RMSK                                            0x80000001
#define HWIO_MSS_BUS_CMD_RCGR_IN          \
        in_dword_masked(HWIO_MSS_BUS_CMD_RCGR_ADDR, HWIO_MSS_BUS_CMD_RCGR_RMSK)
#define HWIO_MSS_BUS_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_MSS_BUS_CMD_RCGR_ADDR, m)
#define HWIO_MSS_BUS_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_MSS_BUS_CMD_RCGR_ADDR,v)
#define HWIO_MSS_BUS_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BUS_CMD_RCGR_ADDR,m,v,HWIO_MSS_BUS_CMD_RCGR_IN)
#define HWIO_MSS_BUS_CMD_RCGR_ROOT_OFF_BMSK                                   0x80000000
#define HWIO_MSS_BUS_CMD_RCGR_ROOT_OFF_SHFT                                         0x1f
#define HWIO_MSS_BUS_CMD_RCGR_UPDATE_BMSK                                            0x1
#define HWIO_MSS_BUS_CMD_RCGR_UPDATE_SHFT                                            0x0

#define HWIO_MSS_BUS_CFG_RCGR_ADDR                                            (MPSS_PERPH_REG_BASE      + 0x00002004)
#define HWIO_MSS_BUS_CFG_RCGR_PHYS                                            (MPSS_PERPH_REG_BASE_PHYS + 0x00002004)
#define HWIO_MSS_BUS_CFG_RCGR_RMSK                                                 0x71f
#define HWIO_MSS_BUS_CFG_RCGR_IN          \
        in_dword_masked(HWIO_MSS_BUS_CFG_RCGR_ADDR, HWIO_MSS_BUS_CFG_RCGR_RMSK)
#define HWIO_MSS_BUS_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_MSS_BUS_CFG_RCGR_ADDR, m)
#define HWIO_MSS_BUS_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_MSS_BUS_CFG_RCGR_ADDR,v)
#define HWIO_MSS_BUS_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BUS_CFG_RCGR_ADDR,m,v,HWIO_MSS_BUS_CFG_RCGR_IN)
#define HWIO_MSS_BUS_CFG_RCGR_SRC_SEL_BMSK                                         0x700
#define HWIO_MSS_BUS_CFG_RCGR_SRC_SEL_SHFT                                           0x8
#define HWIO_MSS_BUS_CFG_RCGR_SRC_DIV_BMSK                                          0x1f
#define HWIO_MSS_BUS_CFG_RCGR_SRC_DIV_SHFT                                           0x0

#define HWIO_MSS_UIM_CMD_RCGR_ADDR                                            (MPSS_PERPH_REG_BASE      + 0x00002008)
#define HWIO_MSS_UIM_CMD_RCGR_PHYS                                            (MPSS_PERPH_REG_BASE_PHYS + 0x00002008)
#define HWIO_MSS_UIM_CMD_RCGR_RMSK                                            0x80000003
#define HWIO_MSS_UIM_CMD_RCGR_IN          \
        in_dword_masked(HWIO_MSS_UIM_CMD_RCGR_ADDR, HWIO_MSS_UIM_CMD_RCGR_RMSK)
#define HWIO_MSS_UIM_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_MSS_UIM_CMD_RCGR_ADDR, m)
#define HWIO_MSS_UIM_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_MSS_UIM_CMD_RCGR_ADDR,v)
#define HWIO_MSS_UIM_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_UIM_CMD_RCGR_ADDR,m,v,HWIO_MSS_UIM_CMD_RCGR_IN)
#define HWIO_MSS_UIM_CMD_RCGR_ROOT_OFF_BMSK                                   0x80000000
#define HWIO_MSS_UIM_CMD_RCGR_ROOT_OFF_SHFT                                         0x1f
#define HWIO_MSS_UIM_CMD_RCGR_ROOT_EN_BMSK                                           0x2
#define HWIO_MSS_UIM_CMD_RCGR_ROOT_EN_SHFT                                           0x1
#define HWIO_MSS_UIM_CMD_RCGR_UPDATE_BMSK                                            0x1
#define HWIO_MSS_UIM_CMD_RCGR_UPDATE_SHFT                                            0x0

#define HWIO_MSS_UIM_CFG_RCGR_ADDR                                            (MPSS_PERPH_REG_BASE      + 0x0000200c)
#define HWIO_MSS_UIM_CFG_RCGR_PHYS                                            (MPSS_PERPH_REG_BASE_PHYS + 0x0000200c)
#define HWIO_MSS_UIM_CFG_RCGR_RMSK                                                  0x1f
#define HWIO_MSS_UIM_CFG_RCGR_IN          \
        in_dword_masked(HWIO_MSS_UIM_CFG_RCGR_ADDR, HWIO_MSS_UIM_CFG_RCGR_RMSK)
#define HWIO_MSS_UIM_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_MSS_UIM_CFG_RCGR_ADDR, m)
#define HWIO_MSS_UIM_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_MSS_UIM_CFG_RCGR_ADDR,v)
#define HWIO_MSS_UIM_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_UIM_CFG_RCGR_ADDR,m,v,HWIO_MSS_UIM_CFG_RCGR_IN)
#define HWIO_MSS_UIM_CFG_RCGR_SRC_DIV_BMSK                                          0x1f
#define HWIO_MSS_UIM_CFG_RCGR_SRC_DIV_SHFT                                           0x0

#define HWIO_MSS_UIM0_MND_CMD_RCGR_ADDR                                       (MPSS_PERPH_REG_BASE      + 0x00002010)
#define HWIO_MSS_UIM0_MND_CMD_RCGR_PHYS                                       (MPSS_PERPH_REG_BASE_PHYS + 0x00002010)
#define HWIO_MSS_UIM0_MND_CMD_RCGR_RMSK                                       0x80000003
#define HWIO_MSS_UIM0_MND_CMD_RCGR_IN          \
        in_dword_masked(HWIO_MSS_UIM0_MND_CMD_RCGR_ADDR, HWIO_MSS_UIM0_MND_CMD_RCGR_RMSK)
#define HWIO_MSS_UIM0_MND_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_MSS_UIM0_MND_CMD_RCGR_ADDR, m)
#define HWIO_MSS_UIM0_MND_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_MSS_UIM0_MND_CMD_RCGR_ADDR,v)
#define HWIO_MSS_UIM0_MND_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_UIM0_MND_CMD_RCGR_ADDR,m,v,HWIO_MSS_UIM0_MND_CMD_RCGR_IN)
#define HWIO_MSS_UIM0_MND_CMD_RCGR_ROOT_OFF_BMSK                              0x80000000
#define HWIO_MSS_UIM0_MND_CMD_RCGR_ROOT_OFF_SHFT                                    0x1f
#define HWIO_MSS_UIM0_MND_CMD_RCGR_ROOT_EN_BMSK                                      0x2
#define HWIO_MSS_UIM0_MND_CMD_RCGR_ROOT_EN_SHFT                                      0x1
#define HWIO_MSS_UIM0_MND_CMD_RCGR_UPDATE_BMSK                                       0x1
#define HWIO_MSS_UIM0_MND_CMD_RCGR_UPDATE_SHFT                                       0x0

#define HWIO_MSS_UIM0_MND_CFG_RCGR_ADDR                                       (MPSS_PERPH_REG_BASE      + 0x00002014)
#define HWIO_MSS_UIM0_MND_CFG_RCGR_PHYS                                       (MPSS_PERPH_REG_BASE_PHYS + 0x00002014)
#define HWIO_MSS_UIM0_MND_CFG_RCGR_RMSK                                           0x3000
#define HWIO_MSS_UIM0_MND_CFG_RCGR_IN          \
        in_dword_masked(HWIO_MSS_UIM0_MND_CFG_RCGR_ADDR, HWIO_MSS_UIM0_MND_CFG_RCGR_RMSK)
#define HWIO_MSS_UIM0_MND_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_MSS_UIM0_MND_CFG_RCGR_ADDR, m)
#define HWIO_MSS_UIM0_MND_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_MSS_UIM0_MND_CFG_RCGR_ADDR,v)
#define HWIO_MSS_UIM0_MND_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_UIM0_MND_CFG_RCGR_ADDR,m,v,HWIO_MSS_UIM0_MND_CFG_RCGR_IN)
#define HWIO_MSS_UIM0_MND_CFG_RCGR_MODE_BMSK                                      0x3000
#define HWIO_MSS_UIM0_MND_CFG_RCGR_MODE_SHFT                                         0xc

#define HWIO_MSS_UIM0_MND_M_ADDR                                              (MPSS_PERPH_REG_BASE      + 0x00002018)
#define HWIO_MSS_UIM0_MND_M_PHYS                                              (MPSS_PERPH_REG_BASE_PHYS + 0x00002018)
#define HWIO_MSS_UIM0_MND_M_RMSK                                                  0xffff
#define HWIO_MSS_UIM0_MND_M_IN          \
        in_dword_masked(HWIO_MSS_UIM0_MND_M_ADDR, HWIO_MSS_UIM0_MND_M_RMSK)
#define HWIO_MSS_UIM0_MND_M_INM(m)      \
        in_dword_masked(HWIO_MSS_UIM0_MND_M_ADDR, m)
#define HWIO_MSS_UIM0_MND_M_OUT(v)      \
        out_dword(HWIO_MSS_UIM0_MND_M_ADDR,v)
#define HWIO_MSS_UIM0_MND_M_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_UIM0_MND_M_ADDR,m,v,HWIO_MSS_UIM0_MND_M_IN)
#define HWIO_MSS_UIM0_MND_M_M_BMSK                                                0xffff
#define HWIO_MSS_UIM0_MND_M_M_SHFT                                                   0x0

#define HWIO_MSS_UIM0_MND_N_ADDR                                              (MPSS_PERPH_REG_BASE      + 0x0000201c)
#define HWIO_MSS_UIM0_MND_N_PHYS                                              (MPSS_PERPH_REG_BASE_PHYS + 0x0000201c)
#define HWIO_MSS_UIM0_MND_N_RMSK                                                  0xffff
#define HWIO_MSS_UIM0_MND_N_IN          \
        in_dword_masked(HWIO_MSS_UIM0_MND_N_ADDR, HWIO_MSS_UIM0_MND_N_RMSK)
#define HWIO_MSS_UIM0_MND_N_INM(m)      \
        in_dword_masked(HWIO_MSS_UIM0_MND_N_ADDR, m)
#define HWIO_MSS_UIM0_MND_N_OUT(v)      \
        out_dword(HWIO_MSS_UIM0_MND_N_ADDR,v)
#define HWIO_MSS_UIM0_MND_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_UIM0_MND_N_ADDR,m,v,HWIO_MSS_UIM0_MND_N_IN)
#define HWIO_MSS_UIM0_MND_N_NOT_N_MINUS_M_BMSK                                    0xffff
#define HWIO_MSS_UIM0_MND_N_NOT_N_MINUS_M_SHFT                                       0x0

#define HWIO_MSS_UIM0_MND_D_ADDR                                              (MPSS_PERPH_REG_BASE      + 0x00002020)
#define HWIO_MSS_UIM0_MND_D_PHYS                                              (MPSS_PERPH_REG_BASE_PHYS + 0x00002020)
#define HWIO_MSS_UIM0_MND_D_RMSK                                                  0xffff
#define HWIO_MSS_UIM0_MND_D_IN          \
        in_dword_masked(HWIO_MSS_UIM0_MND_D_ADDR, HWIO_MSS_UIM0_MND_D_RMSK)
#define HWIO_MSS_UIM0_MND_D_INM(m)      \
        in_dword_masked(HWIO_MSS_UIM0_MND_D_ADDR, m)
#define HWIO_MSS_UIM0_MND_D_OUT(v)      \
        out_dword(HWIO_MSS_UIM0_MND_D_ADDR,v)
#define HWIO_MSS_UIM0_MND_D_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_UIM0_MND_D_ADDR,m,v,HWIO_MSS_UIM0_MND_D_IN)
#define HWIO_MSS_UIM0_MND_D_NOT_2D_BMSK                                           0xffff
#define HWIO_MSS_UIM0_MND_D_NOT_2D_SHFT                                              0x0

#define HWIO_MSS_CARD_SRC_UIM0_CBCR_ADDR                                      (MPSS_PERPH_REG_BASE      + 0x00002024)
#define HWIO_MSS_CARD_SRC_UIM0_CBCR_PHYS                                      (MPSS_PERPH_REG_BASE_PHYS + 0x00002024)
#define HWIO_MSS_CARD_SRC_UIM0_CBCR_RMSK                                      0x80000001
#define HWIO_MSS_CARD_SRC_UIM0_CBCR_IN          \
        in_dword_masked(HWIO_MSS_CARD_SRC_UIM0_CBCR_ADDR, HWIO_MSS_CARD_SRC_UIM0_CBCR_RMSK)
#define HWIO_MSS_CARD_SRC_UIM0_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_CARD_SRC_UIM0_CBCR_ADDR, m)
#define HWIO_MSS_CARD_SRC_UIM0_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_CARD_SRC_UIM0_CBCR_ADDR,v)
#define HWIO_MSS_CARD_SRC_UIM0_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_CARD_SRC_UIM0_CBCR_ADDR,m,v,HWIO_MSS_CARD_SRC_UIM0_CBCR_IN)
#define HWIO_MSS_CARD_SRC_UIM0_CBCR_CLKOFF_BMSK                               0x80000000
#define HWIO_MSS_CARD_SRC_UIM0_CBCR_CLKOFF_SHFT                                     0x1f
#define HWIO_MSS_CARD_SRC_UIM0_CBCR_CLKEN_BMSK                                       0x1
#define HWIO_MSS_CARD_SRC_UIM0_CBCR_CLKEN_SHFT                                       0x0

#define HWIO_MSS_UART_BIT_UIM0_CBCR_ADDR                                      (MPSS_PERPH_REG_BASE      + 0x00002028)
#define HWIO_MSS_UART_BIT_UIM0_CBCR_PHYS                                      (MPSS_PERPH_REG_BASE_PHYS + 0x00002028)
#define HWIO_MSS_UART_BIT_UIM0_CBCR_RMSK                                      0x80000001
#define HWIO_MSS_UART_BIT_UIM0_CBCR_IN          \
        in_dword_masked(HWIO_MSS_UART_BIT_UIM0_CBCR_ADDR, HWIO_MSS_UART_BIT_UIM0_CBCR_RMSK)
#define HWIO_MSS_UART_BIT_UIM0_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_UART_BIT_UIM0_CBCR_ADDR, m)
#define HWIO_MSS_UART_BIT_UIM0_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_UART_BIT_UIM0_CBCR_ADDR,v)
#define HWIO_MSS_UART_BIT_UIM0_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_UART_BIT_UIM0_CBCR_ADDR,m,v,HWIO_MSS_UART_BIT_UIM0_CBCR_IN)
#define HWIO_MSS_UART_BIT_UIM0_CBCR_CLKOFF_BMSK                               0x80000000
#define HWIO_MSS_UART_BIT_UIM0_CBCR_CLKOFF_SHFT                                     0x1f
#define HWIO_MSS_UART_BIT_UIM0_CBCR_CLKEN_BMSK                                       0x1
#define HWIO_MSS_UART_BIT_UIM0_CBCR_CLKEN_SHFT                                       0x0

#define HWIO_MSS_BUS_UIM0_CBCR_ADDR                                           (MPSS_PERPH_REG_BASE      + 0x0000202c)
#define HWIO_MSS_BUS_UIM0_CBCR_PHYS                                           (MPSS_PERPH_REG_BASE_PHYS + 0x0000202c)
#define HWIO_MSS_BUS_UIM0_CBCR_RMSK                                           0x80007ff1
#define HWIO_MSS_BUS_UIM0_CBCR_IN          \
        in_dword_masked(HWIO_MSS_BUS_UIM0_CBCR_ADDR, HWIO_MSS_BUS_UIM0_CBCR_RMSK)
#define HWIO_MSS_BUS_UIM0_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_BUS_UIM0_CBCR_ADDR, m)
#define HWIO_MSS_BUS_UIM0_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_BUS_UIM0_CBCR_ADDR,v)
#define HWIO_MSS_BUS_UIM0_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BUS_UIM0_CBCR_ADDR,m,v,HWIO_MSS_BUS_UIM0_CBCR_IN)
#define HWIO_MSS_BUS_UIM0_CBCR_CLKOFF_BMSK                                    0x80000000
#define HWIO_MSS_BUS_UIM0_CBCR_CLKOFF_SHFT                                          0x1f
#define HWIO_MSS_BUS_UIM0_CBCR_FORCE_MEM_CORE_ON_BMSK                             0x4000
#define HWIO_MSS_BUS_UIM0_CBCR_FORCE_MEM_CORE_ON_SHFT                                0xe
#define HWIO_MSS_BUS_UIM0_CBCR_FORCE_MEM_PERIPH_ON_BMSK                           0x2000
#define HWIO_MSS_BUS_UIM0_CBCR_FORCE_MEM_PERIPH_ON_SHFT                              0xd
#define HWIO_MSS_BUS_UIM0_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                          0x1000
#define HWIO_MSS_BUS_UIM0_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                             0xc
#define HWIO_MSS_BUS_UIM0_CBCR_WAKEUP_BMSK                                         0xf00
#define HWIO_MSS_BUS_UIM0_CBCR_WAKEUP_SHFT                                           0x8
#define HWIO_MSS_BUS_UIM0_CBCR_SLEEP_BMSK                                           0xf0
#define HWIO_MSS_BUS_UIM0_CBCR_SLEEP_SHFT                                            0x4
#define HWIO_MSS_BUS_UIM0_CBCR_CLKEN_BMSK                                            0x1
#define HWIO_MSS_BUS_UIM0_CBCR_CLKEN_SHFT                                            0x0

#define HWIO_MSS_UIM1_MND_CMD_RCGR_ADDR                                       (MPSS_PERPH_REG_BASE      + 0x00002030)
#define HWIO_MSS_UIM1_MND_CMD_RCGR_PHYS                                       (MPSS_PERPH_REG_BASE_PHYS + 0x00002030)
#define HWIO_MSS_UIM1_MND_CMD_RCGR_RMSK                                       0x80000003
#define HWIO_MSS_UIM1_MND_CMD_RCGR_IN          \
        in_dword_masked(HWIO_MSS_UIM1_MND_CMD_RCGR_ADDR, HWIO_MSS_UIM1_MND_CMD_RCGR_RMSK)
#define HWIO_MSS_UIM1_MND_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_MSS_UIM1_MND_CMD_RCGR_ADDR, m)
#define HWIO_MSS_UIM1_MND_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_MSS_UIM1_MND_CMD_RCGR_ADDR,v)
#define HWIO_MSS_UIM1_MND_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_UIM1_MND_CMD_RCGR_ADDR,m,v,HWIO_MSS_UIM1_MND_CMD_RCGR_IN)
#define HWIO_MSS_UIM1_MND_CMD_RCGR_ROOT_OFF_BMSK                              0x80000000
#define HWIO_MSS_UIM1_MND_CMD_RCGR_ROOT_OFF_SHFT                                    0x1f
#define HWIO_MSS_UIM1_MND_CMD_RCGR_ROOT_EN_BMSK                                      0x2
#define HWIO_MSS_UIM1_MND_CMD_RCGR_ROOT_EN_SHFT                                      0x1
#define HWIO_MSS_UIM1_MND_CMD_RCGR_UPDATE_BMSK                                       0x1
#define HWIO_MSS_UIM1_MND_CMD_RCGR_UPDATE_SHFT                                       0x0

#define HWIO_MSS_UIM1_MND_CFG_RCGR_ADDR                                       (MPSS_PERPH_REG_BASE      + 0x00002034)
#define HWIO_MSS_UIM1_MND_CFG_RCGR_PHYS                                       (MPSS_PERPH_REG_BASE_PHYS + 0x00002034)
#define HWIO_MSS_UIM1_MND_CFG_RCGR_RMSK                                           0x3000
#define HWIO_MSS_UIM1_MND_CFG_RCGR_IN          \
        in_dword_masked(HWIO_MSS_UIM1_MND_CFG_RCGR_ADDR, HWIO_MSS_UIM1_MND_CFG_RCGR_RMSK)
#define HWIO_MSS_UIM1_MND_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_MSS_UIM1_MND_CFG_RCGR_ADDR, m)
#define HWIO_MSS_UIM1_MND_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_MSS_UIM1_MND_CFG_RCGR_ADDR,v)
#define HWIO_MSS_UIM1_MND_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_UIM1_MND_CFG_RCGR_ADDR,m,v,HWIO_MSS_UIM1_MND_CFG_RCGR_IN)
#define HWIO_MSS_UIM1_MND_CFG_RCGR_MODE_BMSK                                      0x3000
#define HWIO_MSS_UIM1_MND_CFG_RCGR_MODE_SHFT                                         0xc

#define HWIO_MSS_UIM1_MND_M_ADDR                                              (MPSS_PERPH_REG_BASE      + 0x00002038)
#define HWIO_MSS_UIM1_MND_M_PHYS                                              (MPSS_PERPH_REG_BASE_PHYS + 0x00002038)
#define HWIO_MSS_UIM1_MND_M_RMSK                                                  0xffff
#define HWIO_MSS_UIM1_MND_M_IN          \
        in_dword_masked(HWIO_MSS_UIM1_MND_M_ADDR, HWIO_MSS_UIM1_MND_M_RMSK)
#define HWIO_MSS_UIM1_MND_M_INM(m)      \
        in_dword_masked(HWIO_MSS_UIM1_MND_M_ADDR, m)
#define HWIO_MSS_UIM1_MND_M_OUT(v)      \
        out_dword(HWIO_MSS_UIM1_MND_M_ADDR,v)
#define HWIO_MSS_UIM1_MND_M_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_UIM1_MND_M_ADDR,m,v,HWIO_MSS_UIM1_MND_M_IN)
#define HWIO_MSS_UIM1_MND_M_M_BMSK                                                0xffff
#define HWIO_MSS_UIM1_MND_M_M_SHFT                                                   0x0

#define HWIO_MSS_UIM1_MND_N_ADDR                                              (MPSS_PERPH_REG_BASE      + 0x0000203c)
#define HWIO_MSS_UIM1_MND_N_PHYS                                              (MPSS_PERPH_REG_BASE_PHYS + 0x0000203c)
#define HWIO_MSS_UIM1_MND_N_RMSK                                                  0xffff
#define HWIO_MSS_UIM1_MND_N_IN          \
        in_dword_masked(HWIO_MSS_UIM1_MND_N_ADDR, HWIO_MSS_UIM1_MND_N_RMSK)
#define HWIO_MSS_UIM1_MND_N_INM(m)      \
        in_dword_masked(HWIO_MSS_UIM1_MND_N_ADDR, m)
#define HWIO_MSS_UIM1_MND_N_OUT(v)      \
        out_dword(HWIO_MSS_UIM1_MND_N_ADDR,v)
#define HWIO_MSS_UIM1_MND_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_UIM1_MND_N_ADDR,m,v,HWIO_MSS_UIM1_MND_N_IN)
#define HWIO_MSS_UIM1_MND_N_NOT_N_MINUS_M_BMSK                                    0xffff
#define HWIO_MSS_UIM1_MND_N_NOT_N_MINUS_M_SHFT                                       0x0

#define HWIO_MSS_UIM1_MND_D_ADDR                                              (MPSS_PERPH_REG_BASE      + 0x00002040)
#define HWIO_MSS_UIM1_MND_D_PHYS                                              (MPSS_PERPH_REG_BASE_PHYS + 0x00002040)
#define HWIO_MSS_UIM1_MND_D_RMSK                                                  0xffff
#define HWIO_MSS_UIM1_MND_D_IN          \
        in_dword_masked(HWIO_MSS_UIM1_MND_D_ADDR, HWIO_MSS_UIM1_MND_D_RMSK)
#define HWIO_MSS_UIM1_MND_D_INM(m)      \
        in_dword_masked(HWIO_MSS_UIM1_MND_D_ADDR, m)
#define HWIO_MSS_UIM1_MND_D_OUT(v)      \
        out_dword(HWIO_MSS_UIM1_MND_D_ADDR,v)
#define HWIO_MSS_UIM1_MND_D_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_UIM1_MND_D_ADDR,m,v,HWIO_MSS_UIM1_MND_D_IN)
#define HWIO_MSS_UIM1_MND_D_NOT_2D_BMSK                                           0xffff
#define HWIO_MSS_UIM1_MND_D_NOT_2D_SHFT                                              0x0

#define HWIO_MSS_CARD_SRC_UIM1_CBCR_ADDR                                      (MPSS_PERPH_REG_BASE      + 0x00002044)
#define HWIO_MSS_CARD_SRC_UIM1_CBCR_PHYS                                      (MPSS_PERPH_REG_BASE_PHYS + 0x00002044)
#define HWIO_MSS_CARD_SRC_UIM1_CBCR_RMSK                                      0x80000001
#define HWIO_MSS_CARD_SRC_UIM1_CBCR_IN          \
        in_dword_masked(HWIO_MSS_CARD_SRC_UIM1_CBCR_ADDR, HWIO_MSS_CARD_SRC_UIM1_CBCR_RMSK)
#define HWIO_MSS_CARD_SRC_UIM1_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_CARD_SRC_UIM1_CBCR_ADDR, m)
#define HWIO_MSS_CARD_SRC_UIM1_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_CARD_SRC_UIM1_CBCR_ADDR,v)
#define HWIO_MSS_CARD_SRC_UIM1_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_CARD_SRC_UIM1_CBCR_ADDR,m,v,HWIO_MSS_CARD_SRC_UIM1_CBCR_IN)
#define HWIO_MSS_CARD_SRC_UIM1_CBCR_CLKOFF_BMSK                               0x80000000
#define HWIO_MSS_CARD_SRC_UIM1_CBCR_CLKOFF_SHFT                                     0x1f
#define HWIO_MSS_CARD_SRC_UIM1_CBCR_CLKEN_BMSK                                       0x1
#define HWIO_MSS_CARD_SRC_UIM1_CBCR_CLKEN_SHFT                                       0x0

#define HWIO_MSS_UART_BIT_UIM1_CBCR_ADDR                                      (MPSS_PERPH_REG_BASE      + 0x00002048)
#define HWIO_MSS_UART_BIT_UIM1_CBCR_PHYS                                      (MPSS_PERPH_REG_BASE_PHYS + 0x00002048)
#define HWIO_MSS_UART_BIT_UIM1_CBCR_RMSK                                      0x80000001
#define HWIO_MSS_UART_BIT_UIM1_CBCR_IN          \
        in_dword_masked(HWIO_MSS_UART_BIT_UIM1_CBCR_ADDR, HWIO_MSS_UART_BIT_UIM1_CBCR_RMSK)
#define HWIO_MSS_UART_BIT_UIM1_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_UART_BIT_UIM1_CBCR_ADDR, m)
#define HWIO_MSS_UART_BIT_UIM1_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_UART_BIT_UIM1_CBCR_ADDR,v)
#define HWIO_MSS_UART_BIT_UIM1_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_UART_BIT_UIM1_CBCR_ADDR,m,v,HWIO_MSS_UART_BIT_UIM1_CBCR_IN)
#define HWIO_MSS_UART_BIT_UIM1_CBCR_CLKOFF_BMSK                               0x80000000
#define HWIO_MSS_UART_BIT_UIM1_CBCR_CLKOFF_SHFT                                     0x1f
#define HWIO_MSS_UART_BIT_UIM1_CBCR_CLKEN_BMSK                                       0x1
#define HWIO_MSS_UART_BIT_UIM1_CBCR_CLKEN_SHFT                                       0x0

#define HWIO_MSS_BUS_UIM1_CBCR_ADDR                                           (MPSS_PERPH_REG_BASE      + 0x0000204c)
#define HWIO_MSS_BUS_UIM1_CBCR_PHYS                                           (MPSS_PERPH_REG_BASE_PHYS + 0x0000204c)
#define HWIO_MSS_BUS_UIM1_CBCR_RMSK                                           0x80007ff1
#define HWIO_MSS_BUS_UIM1_CBCR_IN          \
        in_dword_masked(HWIO_MSS_BUS_UIM1_CBCR_ADDR, HWIO_MSS_BUS_UIM1_CBCR_RMSK)
#define HWIO_MSS_BUS_UIM1_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_BUS_UIM1_CBCR_ADDR, m)
#define HWIO_MSS_BUS_UIM1_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_BUS_UIM1_CBCR_ADDR,v)
#define HWIO_MSS_BUS_UIM1_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BUS_UIM1_CBCR_ADDR,m,v,HWIO_MSS_BUS_UIM1_CBCR_IN)
#define HWIO_MSS_BUS_UIM1_CBCR_CLKOFF_BMSK                                    0x80000000
#define HWIO_MSS_BUS_UIM1_CBCR_CLKOFF_SHFT                                          0x1f
#define HWIO_MSS_BUS_UIM1_CBCR_FORCE_MEM_CORE_ON_BMSK                             0x4000
#define HWIO_MSS_BUS_UIM1_CBCR_FORCE_MEM_CORE_ON_SHFT                                0xe
#define HWIO_MSS_BUS_UIM1_CBCR_FORCE_MEM_PERIPH_ON_BMSK                           0x2000
#define HWIO_MSS_BUS_UIM1_CBCR_FORCE_MEM_PERIPH_ON_SHFT                              0xd
#define HWIO_MSS_BUS_UIM1_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                          0x1000
#define HWIO_MSS_BUS_UIM1_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                             0xc
#define HWIO_MSS_BUS_UIM1_CBCR_WAKEUP_BMSK                                         0xf00
#define HWIO_MSS_BUS_UIM1_CBCR_WAKEUP_SHFT                                           0x8
#define HWIO_MSS_BUS_UIM1_CBCR_SLEEP_BMSK                                           0xf0
#define HWIO_MSS_BUS_UIM1_CBCR_SLEEP_SHFT                                            0x4
#define HWIO_MSS_BUS_UIM1_CBCR_CLKEN_BMSK                                            0x1
#define HWIO_MSS_BUS_UIM1_CBCR_CLKEN_SHFT                                            0x0

#define HWIO_MSS_UIM2_MND_CMD_RCGR_ADDR                                       (MPSS_PERPH_REG_BASE      + 0x00002050)
#define HWIO_MSS_UIM2_MND_CMD_RCGR_PHYS                                       (MPSS_PERPH_REG_BASE_PHYS + 0x00002050)
#define HWIO_MSS_UIM2_MND_CMD_RCGR_RMSK                                       0x80000003
#define HWIO_MSS_UIM2_MND_CMD_RCGR_IN          \
        in_dword_masked(HWIO_MSS_UIM2_MND_CMD_RCGR_ADDR, HWIO_MSS_UIM2_MND_CMD_RCGR_RMSK)
#define HWIO_MSS_UIM2_MND_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_MSS_UIM2_MND_CMD_RCGR_ADDR, m)
#define HWIO_MSS_UIM2_MND_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_MSS_UIM2_MND_CMD_RCGR_ADDR,v)
#define HWIO_MSS_UIM2_MND_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_UIM2_MND_CMD_RCGR_ADDR,m,v,HWIO_MSS_UIM2_MND_CMD_RCGR_IN)
#define HWIO_MSS_UIM2_MND_CMD_RCGR_ROOT_OFF_BMSK                              0x80000000
#define HWIO_MSS_UIM2_MND_CMD_RCGR_ROOT_OFF_SHFT                                    0x1f
#define HWIO_MSS_UIM2_MND_CMD_RCGR_ROOT_EN_BMSK                                      0x2
#define HWIO_MSS_UIM2_MND_CMD_RCGR_ROOT_EN_SHFT                                      0x1
#define HWIO_MSS_UIM2_MND_CMD_RCGR_UPDATE_BMSK                                       0x1
#define HWIO_MSS_UIM2_MND_CMD_RCGR_UPDATE_SHFT                                       0x0

#define HWIO_MSS_UIM2_MND_CFG_RCGR_ADDR                                       (MPSS_PERPH_REG_BASE      + 0x00002054)
#define HWIO_MSS_UIM2_MND_CFG_RCGR_PHYS                                       (MPSS_PERPH_REG_BASE_PHYS + 0x00002054)
#define HWIO_MSS_UIM2_MND_CFG_RCGR_RMSK                                           0x3000
#define HWIO_MSS_UIM2_MND_CFG_RCGR_IN          \
        in_dword_masked(HWIO_MSS_UIM2_MND_CFG_RCGR_ADDR, HWIO_MSS_UIM2_MND_CFG_RCGR_RMSK)
#define HWIO_MSS_UIM2_MND_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_MSS_UIM2_MND_CFG_RCGR_ADDR, m)
#define HWIO_MSS_UIM2_MND_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_MSS_UIM2_MND_CFG_RCGR_ADDR,v)
#define HWIO_MSS_UIM2_MND_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_UIM2_MND_CFG_RCGR_ADDR,m,v,HWIO_MSS_UIM2_MND_CFG_RCGR_IN)
#define HWIO_MSS_UIM2_MND_CFG_RCGR_MODE_BMSK                                      0x3000
#define HWIO_MSS_UIM2_MND_CFG_RCGR_MODE_SHFT                                         0xc

#define HWIO_MSS_UIM2_MND_M_ADDR                                              (MPSS_PERPH_REG_BASE      + 0x00002058)
#define HWIO_MSS_UIM2_MND_M_PHYS                                              (MPSS_PERPH_REG_BASE_PHYS + 0x00002058)
#define HWIO_MSS_UIM2_MND_M_RMSK                                                  0xffff
#define HWIO_MSS_UIM2_MND_M_IN          \
        in_dword_masked(HWIO_MSS_UIM2_MND_M_ADDR, HWIO_MSS_UIM2_MND_M_RMSK)
#define HWIO_MSS_UIM2_MND_M_INM(m)      \
        in_dword_masked(HWIO_MSS_UIM2_MND_M_ADDR, m)
#define HWIO_MSS_UIM2_MND_M_OUT(v)      \
        out_dword(HWIO_MSS_UIM2_MND_M_ADDR,v)
#define HWIO_MSS_UIM2_MND_M_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_UIM2_MND_M_ADDR,m,v,HWIO_MSS_UIM2_MND_M_IN)
#define HWIO_MSS_UIM2_MND_M_M_BMSK                                                0xffff
#define HWIO_MSS_UIM2_MND_M_M_SHFT                                                   0x0

#define HWIO_MSS_UIM2_MND_N_ADDR                                              (MPSS_PERPH_REG_BASE      + 0x0000205c)
#define HWIO_MSS_UIM2_MND_N_PHYS                                              (MPSS_PERPH_REG_BASE_PHYS + 0x0000205c)
#define HWIO_MSS_UIM2_MND_N_RMSK                                                  0xffff
#define HWIO_MSS_UIM2_MND_N_IN          \
        in_dword_masked(HWIO_MSS_UIM2_MND_N_ADDR, HWIO_MSS_UIM2_MND_N_RMSK)
#define HWIO_MSS_UIM2_MND_N_INM(m)      \
        in_dword_masked(HWIO_MSS_UIM2_MND_N_ADDR, m)
#define HWIO_MSS_UIM2_MND_N_OUT(v)      \
        out_dword(HWIO_MSS_UIM2_MND_N_ADDR,v)
#define HWIO_MSS_UIM2_MND_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_UIM2_MND_N_ADDR,m,v,HWIO_MSS_UIM2_MND_N_IN)
#define HWIO_MSS_UIM2_MND_N_NOT_N_MINUS_M_BMSK                                    0xffff
#define HWIO_MSS_UIM2_MND_N_NOT_N_MINUS_M_SHFT                                       0x0

#define HWIO_MSS_UIM2_MND_D_ADDR                                              (MPSS_PERPH_REG_BASE      + 0x00002060)
#define HWIO_MSS_UIM2_MND_D_PHYS                                              (MPSS_PERPH_REG_BASE_PHYS + 0x00002060)
#define HWIO_MSS_UIM2_MND_D_RMSK                                                  0xffff
#define HWIO_MSS_UIM2_MND_D_IN          \
        in_dword_masked(HWIO_MSS_UIM2_MND_D_ADDR, HWIO_MSS_UIM2_MND_D_RMSK)
#define HWIO_MSS_UIM2_MND_D_INM(m)      \
        in_dword_masked(HWIO_MSS_UIM2_MND_D_ADDR, m)
#define HWIO_MSS_UIM2_MND_D_OUT(v)      \
        out_dword(HWIO_MSS_UIM2_MND_D_ADDR,v)
#define HWIO_MSS_UIM2_MND_D_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_UIM2_MND_D_ADDR,m,v,HWIO_MSS_UIM2_MND_D_IN)
#define HWIO_MSS_UIM2_MND_D_NOT_2D_BMSK                                           0xffff
#define HWIO_MSS_UIM2_MND_D_NOT_2D_SHFT                                              0x0

#define HWIO_MSS_CARD_SRC_UIM2_CBCR_ADDR                                      (MPSS_PERPH_REG_BASE      + 0x00002064)
#define HWIO_MSS_CARD_SRC_UIM2_CBCR_PHYS                                      (MPSS_PERPH_REG_BASE_PHYS + 0x00002064)
#define HWIO_MSS_CARD_SRC_UIM2_CBCR_RMSK                                      0x80000001
#define HWIO_MSS_CARD_SRC_UIM2_CBCR_IN          \
        in_dword_masked(HWIO_MSS_CARD_SRC_UIM2_CBCR_ADDR, HWIO_MSS_CARD_SRC_UIM2_CBCR_RMSK)
#define HWIO_MSS_CARD_SRC_UIM2_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_CARD_SRC_UIM2_CBCR_ADDR, m)
#define HWIO_MSS_CARD_SRC_UIM2_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_CARD_SRC_UIM2_CBCR_ADDR,v)
#define HWIO_MSS_CARD_SRC_UIM2_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_CARD_SRC_UIM2_CBCR_ADDR,m,v,HWIO_MSS_CARD_SRC_UIM2_CBCR_IN)
#define HWIO_MSS_CARD_SRC_UIM2_CBCR_CLKOFF_BMSK                               0x80000000
#define HWIO_MSS_CARD_SRC_UIM2_CBCR_CLKOFF_SHFT                                     0x1f
#define HWIO_MSS_CARD_SRC_UIM2_CBCR_CLKEN_BMSK                                       0x1
#define HWIO_MSS_CARD_SRC_UIM2_CBCR_CLKEN_SHFT                                       0x0

#define HWIO_MSS_UART_BIT_UIM2_CBCR_ADDR                                      (MPSS_PERPH_REG_BASE      + 0x00002068)
#define HWIO_MSS_UART_BIT_UIM2_CBCR_PHYS                                      (MPSS_PERPH_REG_BASE_PHYS + 0x00002068)
#define HWIO_MSS_UART_BIT_UIM2_CBCR_RMSK                                      0x80000001
#define HWIO_MSS_UART_BIT_UIM2_CBCR_IN          \
        in_dword_masked(HWIO_MSS_UART_BIT_UIM2_CBCR_ADDR, HWIO_MSS_UART_BIT_UIM2_CBCR_RMSK)
#define HWIO_MSS_UART_BIT_UIM2_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_UART_BIT_UIM2_CBCR_ADDR, m)
#define HWIO_MSS_UART_BIT_UIM2_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_UART_BIT_UIM2_CBCR_ADDR,v)
#define HWIO_MSS_UART_BIT_UIM2_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_UART_BIT_UIM2_CBCR_ADDR,m,v,HWIO_MSS_UART_BIT_UIM2_CBCR_IN)
#define HWIO_MSS_UART_BIT_UIM2_CBCR_CLKOFF_BMSK                               0x80000000
#define HWIO_MSS_UART_BIT_UIM2_CBCR_CLKOFF_SHFT                                     0x1f
#define HWIO_MSS_UART_BIT_UIM2_CBCR_CLKEN_BMSK                                       0x1
#define HWIO_MSS_UART_BIT_UIM2_CBCR_CLKEN_SHFT                                       0x0

#define HWIO_MSS_BUS_UIM2_CBCR_ADDR                                           (MPSS_PERPH_REG_BASE      + 0x0000206c)
#define HWIO_MSS_BUS_UIM2_CBCR_PHYS                                           (MPSS_PERPH_REG_BASE_PHYS + 0x0000206c)
#define HWIO_MSS_BUS_UIM2_CBCR_RMSK                                           0x80007ff1
#define HWIO_MSS_BUS_UIM2_CBCR_IN          \
        in_dword_masked(HWIO_MSS_BUS_UIM2_CBCR_ADDR, HWIO_MSS_BUS_UIM2_CBCR_RMSK)
#define HWIO_MSS_BUS_UIM2_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_BUS_UIM2_CBCR_ADDR, m)
#define HWIO_MSS_BUS_UIM2_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_BUS_UIM2_CBCR_ADDR,v)
#define HWIO_MSS_BUS_UIM2_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BUS_UIM2_CBCR_ADDR,m,v,HWIO_MSS_BUS_UIM2_CBCR_IN)
#define HWIO_MSS_BUS_UIM2_CBCR_CLKOFF_BMSK                                    0x80000000
#define HWIO_MSS_BUS_UIM2_CBCR_CLKOFF_SHFT                                          0x1f
#define HWIO_MSS_BUS_UIM2_CBCR_FORCE_MEM_CORE_ON_BMSK                             0x4000
#define HWIO_MSS_BUS_UIM2_CBCR_FORCE_MEM_CORE_ON_SHFT                                0xe
#define HWIO_MSS_BUS_UIM2_CBCR_FORCE_MEM_PERIPH_ON_BMSK                           0x2000
#define HWIO_MSS_BUS_UIM2_CBCR_FORCE_MEM_PERIPH_ON_SHFT                              0xd
#define HWIO_MSS_BUS_UIM2_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                          0x1000
#define HWIO_MSS_BUS_UIM2_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                             0xc
#define HWIO_MSS_BUS_UIM2_CBCR_WAKEUP_BMSK                                         0xf00
#define HWIO_MSS_BUS_UIM2_CBCR_WAKEUP_SHFT                                           0x8
#define HWIO_MSS_BUS_UIM2_CBCR_SLEEP_BMSK                                           0xf0
#define HWIO_MSS_BUS_UIM2_CBCR_SLEEP_SHFT                                            0x4
#define HWIO_MSS_BUS_UIM2_CBCR_CLKEN_BMSK                                            0x1
#define HWIO_MSS_BUS_UIM2_CBCR_CLKEN_SHFT                                            0x0

#define HWIO_MSS_UIM3_MND_CMD_RCGR_ADDR                                       (MPSS_PERPH_REG_BASE      + 0x00002070)
#define HWIO_MSS_UIM3_MND_CMD_RCGR_PHYS                                       (MPSS_PERPH_REG_BASE_PHYS + 0x00002070)
#define HWIO_MSS_UIM3_MND_CMD_RCGR_RMSK                                       0x80000003
#define HWIO_MSS_UIM3_MND_CMD_RCGR_IN          \
        in_dword_masked(HWIO_MSS_UIM3_MND_CMD_RCGR_ADDR, HWIO_MSS_UIM3_MND_CMD_RCGR_RMSK)
#define HWIO_MSS_UIM3_MND_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_MSS_UIM3_MND_CMD_RCGR_ADDR, m)
#define HWIO_MSS_UIM3_MND_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_MSS_UIM3_MND_CMD_RCGR_ADDR,v)
#define HWIO_MSS_UIM3_MND_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_UIM3_MND_CMD_RCGR_ADDR,m,v,HWIO_MSS_UIM3_MND_CMD_RCGR_IN)
#define HWIO_MSS_UIM3_MND_CMD_RCGR_ROOT_OFF_BMSK                              0x80000000
#define HWIO_MSS_UIM3_MND_CMD_RCGR_ROOT_OFF_SHFT                                    0x1f
#define HWIO_MSS_UIM3_MND_CMD_RCGR_ROOT_EN_BMSK                                      0x2
#define HWIO_MSS_UIM3_MND_CMD_RCGR_ROOT_EN_SHFT                                      0x1
#define HWIO_MSS_UIM3_MND_CMD_RCGR_UPDATE_BMSK                                       0x1
#define HWIO_MSS_UIM3_MND_CMD_RCGR_UPDATE_SHFT                                       0x0

#define HWIO_MSS_UIM3_MND_CFG_RCGR_ADDR                                       (MPSS_PERPH_REG_BASE      + 0x00002074)
#define HWIO_MSS_UIM3_MND_CFG_RCGR_PHYS                                       (MPSS_PERPH_REG_BASE_PHYS + 0x00002074)
#define HWIO_MSS_UIM3_MND_CFG_RCGR_RMSK                                           0x3000
#define HWIO_MSS_UIM3_MND_CFG_RCGR_IN          \
        in_dword_masked(HWIO_MSS_UIM3_MND_CFG_RCGR_ADDR, HWIO_MSS_UIM3_MND_CFG_RCGR_RMSK)
#define HWIO_MSS_UIM3_MND_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_MSS_UIM3_MND_CFG_RCGR_ADDR, m)
#define HWIO_MSS_UIM3_MND_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_MSS_UIM3_MND_CFG_RCGR_ADDR,v)
#define HWIO_MSS_UIM3_MND_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_UIM3_MND_CFG_RCGR_ADDR,m,v,HWIO_MSS_UIM3_MND_CFG_RCGR_IN)
#define HWIO_MSS_UIM3_MND_CFG_RCGR_MODE_BMSK                                      0x3000
#define HWIO_MSS_UIM3_MND_CFG_RCGR_MODE_SHFT                                         0xc

#define HWIO_MSS_UIM3_MND_M_ADDR                                              (MPSS_PERPH_REG_BASE      + 0x00002078)
#define HWIO_MSS_UIM3_MND_M_PHYS                                              (MPSS_PERPH_REG_BASE_PHYS + 0x00002078)
#define HWIO_MSS_UIM3_MND_M_RMSK                                                  0xffff
#define HWIO_MSS_UIM3_MND_M_IN          \
        in_dword_masked(HWIO_MSS_UIM3_MND_M_ADDR, HWIO_MSS_UIM3_MND_M_RMSK)
#define HWIO_MSS_UIM3_MND_M_INM(m)      \
        in_dword_masked(HWIO_MSS_UIM3_MND_M_ADDR, m)
#define HWIO_MSS_UIM3_MND_M_OUT(v)      \
        out_dword(HWIO_MSS_UIM3_MND_M_ADDR,v)
#define HWIO_MSS_UIM3_MND_M_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_UIM3_MND_M_ADDR,m,v,HWIO_MSS_UIM3_MND_M_IN)
#define HWIO_MSS_UIM3_MND_M_M_BMSK                                                0xffff
#define HWIO_MSS_UIM3_MND_M_M_SHFT                                                   0x0

#define HWIO_MSS_UIM3_MND_N_ADDR                                              (MPSS_PERPH_REG_BASE      + 0x0000207c)
#define HWIO_MSS_UIM3_MND_N_PHYS                                              (MPSS_PERPH_REG_BASE_PHYS + 0x0000207c)
#define HWIO_MSS_UIM3_MND_N_RMSK                                                  0xffff
#define HWIO_MSS_UIM3_MND_N_IN          \
        in_dword_masked(HWIO_MSS_UIM3_MND_N_ADDR, HWIO_MSS_UIM3_MND_N_RMSK)
#define HWIO_MSS_UIM3_MND_N_INM(m)      \
        in_dword_masked(HWIO_MSS_UIM3_MND_N_ADDR, m)
#define HWIO_MSS_UIM3_MND_N_OUT(v)      \
        out_dword(HWIO_MSS_UIM3_MND_N_ADDR,v)
#define HWIO_MSS_UIM3_MND_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_UIM3_MND_N_ADDR,m,v,HWIO_MSS_UIM3_MND_N_IN)
#define HWIO_MSS_UIM3_MND_N_NOT_N_MINUS_M_BMSK                                    0xffff
#define HWIO_MSS_UIM3_MND_N_NOT_N_MINUS_M_SHFT                                       0x0

#define HWIO_MSS_UIM3_MND_D_ADDR                                              (MPSS_PERPH_REG_BASE      + 0x00002080)
#define HWIO_MSS_UIM3_MND_D_PHYS                                              (MPSS_PERPH_REG_BASE_PHYS + 0x00002080)
#define HWIO_MSS_UIM3_MND_D_RMSK                                                  0xffff
#define HWIO_MSS_UIM3_MND_D_IN          \
        in_dword_masked(HWIO_MSS_UIM3_MND_D_ADDR, HWIO_MSS_UIM3_MND_D_RMSK)
#define HWIO_MSS_UIM3_MND_D_INM(m)      \
        in_dword_masked(HWIO_MSS_UIM3_MND_D_ADDR, m)
#define HWIO_MSS_UIM3_MND_D_OUT(v)      \
        out_dword(HWIO_MSS_UIM3_MND_D_ADDR,v)
#define HWIO_MSS_UIM3_MND_D_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_UIM3_MND_D_ADDR,m,v,HWIO_MSS_UIM3_MND_D_IN)
#define HWIO_MSS_UIM3_MND_D_NOT_2D_BMSK                                           0xffff
#define HWIO_MSS_UIM3_MND_D_NOT_2D_SHFT                                              0x0

#define HWIO_MSS_CARD_SRC_UIM3_CBCR_ADDR                                      (MPSS_PERPH_REG_BASE      + 0x00002084)
#define HWIO_MSS_CARD_SRC_UIM3_CBCR_PHYS                                      (MPSS_PERPH_REG_BASE_PHYS + 0x00002084)
#define HWIO_MSS_CARD_SRC_UIM3_CBCR_RMSK                                      0x80000001
#define HWIO_MSS_CARD_SRC_UIM3_CBCR_IN          \
        in_dword_masked(HWIO_MSS_CARD_SRC_UIM3_CBCR_ADDR, HWIO_MSS_CARD_SRC_UIM3_CBCR_RMSK)
#define HWIO_MSS_CARD_SRC_UIM3_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_CARD_SRC_UIM3_CBCR_ADDR, m)
#define HWIO_MSS_CARD_SRC_UIM3_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_CARD_SRC_UIM3_CBCR_ADDR,v)
#define HWIO_MSS_CARD_SRC_UIM3_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_CARD_SRC_UIM3_CBCR_ADDR,m,v,HWIO_MSS_CARD_SRC_UIM3_CBCR_IN)
#define HWIO_MSS_CARD_SRC_UIM3_CBCR_CLKOFF_BMSK                               0x80000000
#define HWIO_MSS_CARD_SRC_UIM3_CBCR_CLKOFF_SHFT                                     0x1f
#define HWIO_MSS_CARD_SRC_UIM3_CBCR_CLKEN_BMSK                                       0x1
#define HWIO_MSS_CARD_SRC_UIM3_CBCR_CLKEN_SHFT                                       0x0

#define HWIO_MSS_UART_BIT_UIM3_CBCR_ADDR                                      (MPSS_PERPH_REG_BASE      + 0x00002088)
#define HWIO_MSS_UART_BIT_UIM3_CBCR_PHYS                                      (MPSS_PERPH_REG_BASE_PHYS + 0x00002088)
#define HWIO_MSS_UART_BIT_UIM3_CBCR_RMSK                                      0x80000001
#define HWIO_MSS_UART_BIT_UIM3_CBCR_IN          \
        in_dword_masked(HWIO_MSS_UART_BIT_UIM3_CBCR_ADDR, HWIO_MSS_UART_BIT_UIM3_CBCR_RMSK)
#define HWIO_MSS_UART_BIT_UIM3_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_UART_BIT_UIM3_CBCR_ADDR, m)
#define HWIO_MSS_UART_BIT_UIM3_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_UART_BIT_UIM3_CBCR_ADDR,v)
#define HWIO_MSS_UART_BIT_UIM3_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_UART_BIT_UIM3_CBCR_ADDR,m,v,HWIO_MSS_UART_BIT_UIM3_CBCR_IN)
#define HWIO_MSS_UART_BIT_UIM3_CBCR_CLKOFF_BMSK                               0x80000000
#define HWIO_MSS_UART_BIT_UIM3_CBCR_CLKOFF_SHFT                                     0x1f
#define HWIO_MSS_UART_BIT_UIM3_CBCR_CLKEN_BMSK                                       0x1
#define HWIO_MSS_UART_BIT_UIM3_CBCR_CLKEN_SHFT                                       0x0

#define HWIO_MSS_BUS_UIM3_CBCR_ADDR                                           (MPSS_PERPH_REG_BASE      + 0x0000208c)
#define HWIO_MSS_BUS_UIM3_CBCR_PHYS                                           (MPSS_PERPH_REG_BASE_PHYS + 0x0000208c)
#define HWIO_MSS_BUS_UIM3_CBCR_RMSK                                           0x80007ff1
#define HWIO_MSS_BUS_UIM3_CBCR_IN          \
        in_dword_masked(HWIO_MSS_BUS_UIM3_CBCR_ADDR, HWIO_MSS_BUS_UIM3_CBCR_RMSK)
#define HWIO_MSS_BUS_UIM3_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_BUS_UIM3_CBCR_ADDR, m)
#define HWIO_MSS_BUS_UIM3_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_BUS_UIM3_CBCR_ADDR,v)
#define HWIO_MSS_BUS_UIM3_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BUS_UIM3_CBCR_ADDR,m,v,HWIO_MSS_BUS_UIM3_CBCR_IN)
#define HWIO_MSS_BUS_UIM3_CBCR_CLKOFF_BMSK                                    0x80000000
#define HWIO_MSS_BUS_UIM3_CBCR_CLKOFF_SHFT                                          0x1f
#define HWIO_MSS_BUS_UIM3_CBCR_FORCE_MEM_CORE_ON_BMSK                             0x4000
#define HWIO_MSS_BUS_UIM3_CBCR_FORCE_MEM_CORE_ON_SHFT                                0xe
#define HWIO_MSS_BUS_UIM3_CBCR_FORCE_MEM_PERIPH_ON_BMSK                           0x2000
#define HWIO_MSS_BUS_UIM3_CBCR_FORCE_MEM_PERIPH_ON_SHFT                              0xd
#define HWIO_MSS_BUS_UIM3_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                          0x1000
#define HWIO_MSS_BUS_UIM3_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                             0xc
#define HWIO_MSS_BUS_UIM3_CBCR_WAKEUP_BMSK                                         0xf00
#define HWIO_MSS_BUS_UIM3_CBCR_WAKEUP_SHFT                                           0x8
#define HWIO_MSS_BUS_UIM3_CBCR_SLEEP_BMSK                                           0xf0
#define HWIO_MSS_BUS_UIM3_CBCR_SLEEP_SHFT                                            0x4
#define HWIO_MSS_BUS_UIM3_CBCR_CLKEN_BMSK                                            0x1
#define HWIO_MSS_BUS_UIM3_CBCR_CLKEN_SHFT                                            0x0

#define HWIO_MSS_BUS_Q6SS_CBCR_ADDR                                           (MPSS_PERPH_REG_BASE      + 0x00002090)
#define HWIO_MSS_BUS_Q6SS_CBCR_PHYS                                           (MPSS_PERPH_REG_BASE_PHYS + 0x00002090)
#define HWIO_MSS_BUS_Q6SS_CBCR_RMSK                                           0x80000001
#define HWIO_MSS_BUS_Q6SS_CBCR_IN          \
        in_dword_masked(HWIO_MSS_BUS_Q6SS_CBCR_ADDR, HWIO_MSS_BUS_Q6SS_CBCR_RMSK)
#define HWIO_MSS_BUS_Q6SS_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_BUS_Q6SS_CBCR_ADDR, m)
#define HWIO_MSS_BUS_Q6SS_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_BUS_Q6SS_CBCR_ADDR,v)
#define HWIO_MSS_BUS_Q6SS_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BUS_Q6SS_CBCR_ADDR,m,v,HWIO_MSS_BUS_Q6SS_CBCR_IN)
#define HWIO_MSS_BUS_Q6SS_CBCR_CLKOFF_BMSK                                    0x80000000
#define HWIO_MSS_BUS_Q6SS_CBCR_CLKOFF_SHFT                                          0x1f
#define HWIO_MSS_BUS_Q6SS_CBCR_CLKEN_BMSK                                            0x1
#define HWIO_MSS_BUS_Q6SS_CBCR_CLKEN_SHFT                                            0x0

#define HWIO_MSS_NAV_BCR_ADDR                                                 (MPSS_PERPH_REG_BASE      + 0x00002094)
#define HWIO_MSS_NAV_BCR_PHYS                                                 (MPSS_PERPH_REG_BASE_PHYS + 0x00002094)
#define HWIO_MSS_NAV_BCR_RMSK                                                 0x80000001
#define HWIO_MSS_NAV_BCR_IN          \
        in_dword_masked(HWIO_MSS_NAV_BCR_ADDR, HWIO_MSS_NAV_BCR_RMSK)
#define HWIO_MSS_NAV_BCR_INM(m)      \
        in_dword_masked(HWIO_MSS_NAV_BCR_ADDR, m)
#define HWIO_MSS_NAV_BCR_OUT(v)      \
        out_dword(HWIO_MSS_NAV_BCR_ADDR,v)
#define HWIO_MSS_NAV_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_NAV_BCR_ADDR,m,v,HWIO_MSS_NAV_BCR_IN)
#define HWIO_MSS_NAV_BCR_STATUS_BMSK                                          0x80000000
#define HWIO_MSS_NAV_BCR_STATUS_SHFT                                                0x1f
#define HWIO_MSS_NAV_BCR_BLK_ARES_BMSK                                               0x1
#define HWIO_MSS_NAV_BCR_BLK_ARES_SHFT                                               0x0

#define HWIO_MSS_BUS_NAV_CBCR_ADDR                                            (MPSS_PERPH_REG_BASE      + 0x00002098)
#define HWIO_MSS_BUS_NAV_CBCR_PHYS                                            (MPSS_PERPH_REG_BASE_PHYS + 0x00002098)
#define HWIO_MSS_BUS_NAV_CBCR_RMSK                                            0x80000001
#define HWIO_MSS_BUS_NAV_CBCR_IN          \
        in_dword_masked(HWIO_MSS_BUS_NAV_CBCR_ADDR, HWIO_MSS_BUS_NAV_CBCR_RMSK)
#define HWIO_MSS_BUS_NAV_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_BUS_NAV_CBCR_ADDR, m)
#define HWIO_MSS_BUS_NAV_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_BUS_NAV_CBCR_ADDR,v)
#define HWIO_MSS_BUS_NAV_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BUS_NAV_CBCR_ADDR,m,v,HWIO_MSS_BUS_NAV_CBCR_IN)
#define HWIO_MSS_BUS_NAV_CBCR_CLKOFF_BMSK                                     0x80000000
#define HWIO_MSS_BUS_NAV_CBCR_CLKOFF_SHFT                                           0x1f
#define HWIO_MSS_BUS_NAV_CBCR_CLKEN_BMSK                                             0x1
#define HWIO_MSS_BUS_NAV_CBCR_CLKEN_SHFT                                             0x0

#define HWIO_MSS_AXI_NAV_CBCR_ADDR                                            (MPSS_PERPH_REG_BASE      + 0x0000209c)
#define HWIO_MSS_AXI_NAV_CBCR_PHYS                                            (MPSS_PERPH_REG_BASE_PHYS + 0x0000209c)
#define HWIO_MSS_AXI_NAV_CBCR_RMSK                                            0x80000001
#define HWIO_MSS_AXI_NAV_CBCR_IN          \
        in_dword_masked(HWIO_MSS_AXI_NAV_CBCR_ADDR, HWIO_MSS_AXI_NAV_CBCR_RMSK)
#define HWIO_MSS_AXI_NAV_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_AXI_NAV_CBCR_ADDR, m)
#define HWIO_MSS_AXI_NAV_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_AXI_NAV_CBCR_ADDR,v)
#define HWIO_MSS_AXI_NAV_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_AXI_NAV_CBCR_ADDR,m,v,HWIO_MSS_AXI_NAV_CBCR_IN)
#define HWIO_MSS_AXI_NAV_CBCR_CLKOFF_BMSK                                     0x80000000
#define HWIO_MSS_AXI_NAV_CBCR_CLKOFF_SHFT                                           0x1f
#define HWIO_MSS_AXI_NAV_CBCR_CLKEN_BMSK                                             0x1
#define HWIO_MSS_AXI_NAV_CBCR_CLKEN_SHFT                                             0x0

#define HWIO_MSS_XO_NAV_CBCR_ADDR                                             (MPSS_PERPH_REG_BASE      + 0x000020a0)
#define HWIO_MSS_XO_NAV_CBCR_PHYS                                             (MPSS_PERPH_REG_BASE_PHYS + 0x000020a0)
#define HWIO_MSS_XO_NAV_CBCR_RMSK                                             0x80000001
#define HWIO_MSS_XO_NAV_CBCR_IN          \
        in_dword_masked(HWIO_MSS_XO_NAV_CBCR_ADDR, HWIO_MSS_XO_NAV_CBCR_RMSK)
#define HWIO_MSS_XO_NAV_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_XO_NAV_CBCR_ADDR, m)
#define HWIO_MSS_XO_NAV_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_XO_NAV_CBCR_ADDR,v)
#define HWIO_MSS_XO_NAV_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_XO_NAV_CBCR_ADDR,m,v,HWIO_MSS_XO_NAV_CBCR_IN)
#define HWIO_MSS_XO_NAV_CBCR_CLKOFF_BMSK                                      0x80000000
#define HWIO_MSS_XO_NAV_CBCR_CLKOFF_SHFT                                            0x1f
#define HWIO_MSS_XO_NAV_CBCR_CLKEN_BMSK                                              0x1
#define HWIO_MSS_XO_NAV_CBCR_CLKEN_SHFT                                              0x0

#define HWIO_MSS_BUS_CRYPTO_CBCR_ADDR                                         (MPSS_PERPH_REG_BASE      + 0x000020a4)
#define HWIO_MSS_BUS_CRYPTO_CBCR_PHYS                                         (MPSS_PERPH_REG_BASE_PHYS + 0x000020a4)
#define HWIO_MSS_BUS_CRYPTO_CBCR_RMSK                                         0x80000001
#define HWIO_MSS_BUS_CRYPTO_CBCR_IN          \
        in_dword_masked(HWIO_MSS_BUS_CRYPTO_CBCR_ADDR, HWIO_MSS_BUS_CRYPTO_CBCR_RMSK)
#define HWIO_MSS_BUS_CRYPTO_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_BUS_CRYPTO_CBCR_ADDR, m)
#define HWIO_MSS_BUS_CRYPTO_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_BUS_CRYPTO_CBCR_ADDR,v)
#define HWIO_MSS_BUS_CRYPTO_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BUS_CRYPTO_CBCR_ADDR,m,v,HWIO_MSS_BUS_CRYPTO_CBCR_IN)
#define HWIO_MSS_BUS_CRYPTO_CBCR_CLKOFF_BMSK                                  0x80000000
#define HWIO_MSS_BUS_CRYPTO_CBCR_CLKOFF_SHFT                                        0x1f
#define HWIO_MSS_BUS_CRYPTO_CBCR_CLKEN_BMSK                                          0x1
#define HWIO_MSS_BUS_CRYPTO_CBCR_CLKEN_SHFT                                          0x0

#define HWIO_MSS_AXI_CRYPTO_CBCR_ADDR                                         (MPSS_PERPH_REG_BASE      + 0x000020a8)
#define HWIO_MSS_AXI_CRYPTO_CBCR_PHYS                                         (MPSS_PERPH_REG_BASE_PHYS + 0x000020a8)
#define HWIO_MSS_AXI_CRYPTO_CBCR_RMSK                                         0x80000001
#define HWIO_MSS_AXI_CRYPTO_CBCR_IN          \
        in_dword_masked(HWIO_MSS_AXI_CRYPTO_CBCR_ADDR, HWIO_MSS_AXI_CRYPTO_CBCR_RMSK)
#define HWIO_MSS_AXI_CRYPTO_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_AXI_CRYPTO_CBCR_ADDR, m)
#define HWIO_MSS_AXI_CRYPTO_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_AXI_CRYPTO_CBCR_ADDR,v)
#define HWIO_MSS_AXI_CRYPTO_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_AXI_CRYPTO_CBCR_ADDR,m,v,HWIO_MSS_AXI_CRYPTO_CBCR_IN)
#define HWIO_MSS_AXI_CRYPTO_CBCR_CLKOFF_BMSK                                  0x80000000
#define HWIO_MSS_AXI_CRYPTO_CBCR_CLKOFF_SHFT                                        0x1f
#define HWIO_MSS_AXI_CRYPTO_CBCR_CLKEN_BMSK                                          0x1
#define HWIO_MSS_AXI_CRYPTO_CBCR_CLKEN_SHFT                                          0x0

#define HWIO_MSS_AXI_DMA_CBCR_ADDR                                            (MPSS_PERPH_REG_BASE      + 0x000020ac)
#define HWIO_MSS_AXI_DMA_CBCR_PHYS                                            (MPSS_PERPH_REG_BASE_PHYS + 0x000020ac)
#define HWIO_MSS_AXI_DMA_CBCR_RMSK                                            0x80000001
#define HWIO_MSS_AXI_DMA_CBCR_IN          \
        in_dword_masked(HWIO_MSS_AXI_DMA_CBCR_ADDR, HWIO_MSS_AXI_DMA_CBCR_RMSK)
#define HWIO_MSS_AXI_DMA_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_AXI_DMA_CBCR_ADDR, m)
#define HWIO_MSS_AXI_DMA_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_AXI_DMA_CBCR_ADDR,v)
#define HWIO_MSS_AXI_DMA_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_AXI_DMA_CBCR_ADDR,m,v,HWIO_MSS_AXI_DMA_CBCR_IN)
#define HWIO_MSS_AXI_DMA_CBCR_CLKOFF_BMSK                                     0x80000000
#define HWIO_MSS_AXI_DMA_CBCR_CLKOFF_SHFT                                           0x1f
#define HWIO_MSS_AXI_DMA_CBCR_CLKEN_BMSK                                             0x1
#define HWIO_MSS_AXI_DMA_CBCR_CLKEN_SHFT                                             0x0

#define HWIO_MSS_BIT_COXM_MND_CMD_RCGR_ADDR                                   (MPSS_PERPH_REG_BASE      + 0x000020b0)
#define HWIO_MSS_BIT_COXM_MND_CMD_RCGR_PHYS                                   (MPSS_PERPH_REG_BASE_PHYS + 0x000020b0)
#define HWIO_MSS_BIT_COXM_MND_CMD_RCGR_RMSK                                   0x80000003
#define HWIO_MSS_BIT_COXM_MND_CMD_RCGR_IN          \
        in_dword_masked(HWIO_MSS_BIT_COXM_MND_CMD_RCGR_ADDR, HWIO_MSS_BIT_COXM_MND_CMD_RCGR_RMSK)
#define HWIO_MSS_BIT_COXM_MND_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_MSS_BIT_COXM_MND_CMD_RCGR_ADDR, m)
#define HWIO_MSS_BIT_COXM_MND_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_MSS_BIT_COXM_MND_CMD_RCGR_ADDR,v)
#define HWIO_MSS_BIT_COXM_MND_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BIT_COXM_MND_CMD_RCGR_ADDR,m,v,HWIO_MSS_BIT_COXM_MND_CMD_RCGR_IN)
#define HWIO_MSS_BIT_COXM_MND_CMD_RCGR_ROOT_OFF_BMSK                          0x80000000
#define HWIO_MSS_BIT_COXM_MND_CMD_RCGR_ROOT_OFF_SHFT                                0x1f
#define HWIO_MSS_BIT_COXM_MND_CMD_RCGR_ROOT_EN_BMSK                                  0x2
#define HWIO_MSS_BIT_COXM_MND_CMD_RCGR_ROOT_EN_SHFT                                  0x1
#define HWIO_MSS_BIT_COXM_MND_CMD_RCGR_UPDATE_BMSK                                   0x1
#define HWIO_MSS_BIT_COXM_MND_CMD_RCGR_UPDATE_SHFT                                   0x0

#define HWIO_MSS_BIT_COXM_MND_CFG_RCGR_ADDR                                   (MPSS_PERPH_REG_BASE      + 0x000020b4)
#define HWIO_MSS_BIT_COXM_MND_CFG_RCGR_PHYS                                   (MPSS_PERPH_REG_BASE_PHYS + 0x000020b4)
#define HWIO_MSS_BIT_COXM_MND_CFG_RCGR_RMSK                                       0x371f
#define HWIO_MSS_BIT_COXM_MND_CFG_RCGR_IN          \
        in_dword_masked(HWIO_MSS_BIT_COXM_MND_CFG_RCGR_ADDR, HWIO_MSS_BIT_COXM_MND_CFG_RCGR_RMSK)
#define HWIO_MSS_BIT_COXM_MND_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_MSS_BIT_COXM_MND_CFG_RCGR_ADDR, m)
#define HWIO_MSS_BIT_COXM_MND_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_MSS_BIT_COXM_MND_CFG_RCGR_ADDR,v)
#define HWIO_MSS_BIT_COXM_MND_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BIT_COXM_MND_CFG_RCGR_ADDR,m,v,HWIO_MSS_BIT_COXM_MND_CFG_RCGR_IN)
#define HWIO_MSS_BIT_COXM_MND_CFG_RCGR_MODE_BMSK                                  0x3000
#define HWIO_MSS_BIT_COXM_MND_CFG_RCGR_MODE_SHFT                                     0xc
#define HWIO_MSS_BIT_COXM_MND_CFG_RCGR_SRC_SEL_BMSK                                0x700
#define HWIO_MSS_BIT_COXM_MND_CFG_RCGR_SRC_SEL_SHFT                                  0x8
#define HWIO_MSS_BIT_COXM_MND_CFG_RCGR_SRC_DIV_BMSK                                 0x1f
#define HWIO_MSS_BIT_COXM_MND_CFG_RCGR_SRC_DIV_SHFT                                  0x0

#define HWIO_MSS_BIT_COXM_MND_M_ADDR                                          (MPSS_PERPH_REG_BASE      + 0x000020b8)
#define HWIO_MSS_BIT_COXM_MND_M_PHYS                                          (MPSS_PERPH_REG_BASE_PHYS + 0x000020b8)
#define HWIO_MSS_BIT_COXM_MND_M_RMSK                                              0xffff
#define HWIO_MSS_BIT_COXM_MND_M_IN          \
        in_dword_masked(HWIO_MSS_BIT_COXM_MND_M_ADDR, HWIO_MSS_BIT_COXM_MND_M_RMSK)
#define HWIO_MSS_BIT_COXM_MND_M_INM(m)      \
        in_dword_masked(HWIO_MSS_BIT_COXM_MND_M_ADDR, m)
#define HWIO_MSS_BIT_COXM_MND_M_OUT(v)      \
        out_dword(HWIO_MSS_BIT_COXM_MND_M_ADDR,v)
#define HWIO_MSS_BIT_COXM_MND_M_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BIT_COXM_MND_M_ADDR,m,v,HWIO_MSS_BIT_COXM_MND_M_IN)
#define HWIO_MSS_BIT_COXM_MND_M_M_BMSK                                            0xffff
#define HWIO_MSS_BIT_COXM_MND_M_M_SHFT                                               0x0

#define HWIO_MSS_BIT_COXM_MND_N_ADDR                                          (MPSS_PERPH_REG_BASE      + 0x000020bc)
#define HWIO_MSS_BIT_COXM_MND_N_PHYS                                          (MPSS_PERPH_REG_BASE_PHYS + 0x000020bc)
#define HWIO_MSS_BIT_COXM_MND_N_RMSK                                              0xffff
#define HWIO_MSS_BIT_COXM_MND_N_IN          \
        in_dword_masked(HWIO_MSS_BIT_COXM_MND_N_ADDR, HWIO_MSS_BIT_COXM_MND_N_RMSK)
#define HWIO_MSS_BIT_COXM_MND_N_INM(m)      \
        in_dword_masked(HWIO_MSS_BIT_COXM_MND_N_ADDR, m)
#define HWIO_MSS_BIT_COXM_MND_N_OUT(v)      \
        out_dword(HWIO_MSS_BIT_COXM_MND_N_ADDR,v)
#define HWIO_MSS_BIT_COXM_MND_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BIT_COXM_MND_N_ADDR,m,v,HWIO_MSS_BIT_COXM_MND_N_IN)
#define HWIO_MSS_BIT_COXM_MND_N_NOT_N_MINUS_M_BMSK                                0xffff
#define HWIO_MSS_BIT_COXM_MND_N_NOT_N_MINUS_M_SHFT                                   0x0

#define HWIO_MSS_BIT_COXM_MND_D_ADDR                                          (MPSS_PERPH_REG_BASE      + 0x000020c0)
#define HWIO_MSS_BIT_COXM_MND_D_PHYS                                          (MPSS_PERPH_REG_BASE_PHYS + 0x000020c0)
#define HWIO_MSS_BIT_COXM_MND_D_RMSK                                              0xffff
#define HWIO_MSS_BIT_COXM_MND_D_IN          \
        in_dword_masked(HWIO_MSS_BIT_COXM_MND_D_ADDR, HWIO_MSS_BIT_COXM_MND_D_RMSK)
#define HWIO_MSS_BIT_COXM_MND_D_INM(m)      \
        in_dword_masked(HWIO_MSS_BIT_COXM_MND_D_ADDR, m)
#define HWIO_MSS_BIT_COXM_MND_D_OUT(v)      \
        out_dword(HWIO_MSS_BIT_COXM_MND_D_ADDR,v)
#define HWIO_MSS_BIT_COXM_MND_D_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BIT_COXM_MND_D_ADDR,m,v,HWIO_MSS_BIT_COXM_MND_D_IN)
#define HWIO_MSS_BIT_COXM_MND_D_NOT_2D_BMSK                                       0xffff
#define HWIO_MSS_BIT_COXM_MND_D_NOT_2D_SHFT                                          0x0

#define HWIO_MSS_BUS_COXM_CBCR_ADDR                                           (MPSS_PERPH_REG_BASE      + 0x000020c4)
#define HWIO_MSS_BUS_COXM_CBCR_PHYS                                           (MPSS_PERPH_REG_BASE_PHYS + 0x000020c4)
#define HWIO_MSS_BUS_COXM_CBCR_RMSK                                           0x80007ff1
#define HWIO_MSS_BUS_COXM_CBCR_IN          \
        in_dword_masked(HWIO_MSS_BUS_COXM_CBCR_ADDR, HWIO_MSS_BUS_COXM_CBCR_RMSK)
#define HWIO_MSS_BUS_COXM_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_BUS_COXM_CBCR_ADDR, m)
#define HWIO_MSS_BUS_COXM_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_BUS_COXM_CBCR_ADDR,v)
#define HWIO_MSS_BUS_COXM_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BUS_COXM_CBCR_ADDR,m,v,HWIO_MSS_BUS_COXM_CBCR_IN)
#define HWIO_MSS_BUS_COXM_CBCR_CLKOFF_BMSK                                    0x80000000
#define HWIO_MSS_BUS_COXM_CBCR_CLKOFF_SHFT                                          0x1f
#define HWIO_MSS_BUS_COXM_CBCR_FORCE_MEM_CORE_ON_BMSK                             0x4000
#define HWIO_MSS_BUS_COXM_CBCR_FORCE_MEM_CORE_ON_SHFT                                0xe
#define HWIO_MSS_BUS_COXM_CBCR_FORCE_MEM_PERIPH_ON_BMSK                           0x2000
#define HWIO_MSS_BUS_COXM_CBCR_FORCE_MEM_PERIPH_ON_SHFT                              0xd
#define HWIO_MSS_BUS_COXM_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                          0x1000
#define HWIO_MSS_BUS_COXM_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                             0xc
#define HWIO_MSS_BUS_COXM_CBCR_WAKEUP_BMSK                                         0xf00
#define HWIO_MSS_BUS_COXM_CBCR_WAKEUP_SHFT                                           0x8
#define HWIO_MSS_BUS_COXM_CBCR_SLEEP_BMSK                                           0xf0
#define HWIO_MSS_BUS_COXM_CBCR_SLEEP_SHFT                                            0x4
#define HWIO_MSS_BUS_COXM_CBCR_CLKEN_BMSK                                            0x1
#define HWIO_MSS_BUS_COXM_CBCR_CLKEN_SHFT                                            0x0

#define HWIO_MSS_BIT_COXM_CBCR_ADDR                                           (MPSS_PERPH_REG_BASE      + 0x000020c8)
#define HWIO_MSS_BIT_COXM_CBCR_PHYS                                           (MPSS_PERPH_REG_BASE_PHYS + 0x000020c8)
#define HWIO_MSS_BIT_COXM_CBCR_RMSK                                           0x80000001
#define HWIO_MSS_BIT_COXM_CBCR_IN          \
        in_dword_masked(HWIO_MSS_BIT_COXM_CBCR_ADDR, HWIO_MSS_BIT_COXM_CBCR_RMSK)
#define HWIO_MSS_BIT_COXM_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_BIT_COXM_CBCR_ADDR, m)
#define HWIO_MSS_BIT_COXM_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_BIT_COXM_CBCR_ADDR,v)
#define HWIO_MSS_BIT_COXM_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BIT_COXM_CBCR_ADDR,m,v,HWIO_MSS_BIT_COXM_CBCR_IN)
#define HWIO_MSS_BIT_COXM_CBCR_CLKOFF_BMSK                                    0x80000000
#define HWIO_MSS_BIT_COXM_CBCR_CLKOFF_SHFT                                          0x1f
#define HWIO_MSS_BIT_COXM_CBCR_CLKEN_BMSK                                            0x1
#define HWIO_MSS_BIT_COXM_CBCR_CLKEN_SHFT                                            0x0

#define HWIO_MSS_RBCPR_REF_CMD_RCGR_ADDR                                      (MPSS_PERPH_REG_BASE      + 0x000020cc)
#define HWIO_MSS_RBCPR_REF_CMD_RCGR_PHYS                                      (MPSS_PERPH_REG_BASE_PHYS + 0x000020cc)
#define HWIO_MSS_RBCPR_REF_CMD_RCGR_RMSK                                      0x80000003
#define HWIO_MSS_RBCPR_REF_CMD_RCGR_IN          \
        in_dword_masked(HWIO_MSS_RBCPR_REF_CMD_RCGR_ADDR, HWIO_MSS_RBCPR_REF_CMD_RCGR_RMSK)
#define HWIO_MSS_RBCPR_REF_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_MSS_RBCPR_REF_CMD_RCGR_ADDR, m)
#define HWIO_MSS_RBCPR_REF_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_MSS_RBCPR_REF_CMD_RCGR_ADDR,v)
#define HWIO_MSS_RBCPR_REF_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_RBCPR_REF_CMD_RCGR_ADDR,m,v,HWIO_MSS_RBCPR_REF_CMD_RCGR_IN)
#define HWIO_MSS_RBCPR_REF_CMD_RCGR_ROOT_OFF_BMSK                             0x80000000
#define HWIO_MSS_RBCPR_REF_CMD_RCGR_ROOT_OFF_SHFT                                   0x1f
#define HWIO_MSS_RBCPR_REF_CMD_RCGR_ROOT_EN_BMSK                                     0x2
#define HWIO_MSS_RBCPR_REF_CMD_RCGR_ROOT_EN_SHFT                                     0x1
#define HWIO_MSS_RBCPR_REF_CMD_RCGR_UPDATE_BMSK                                      0x1
#define HWIO_MSS_RBCPR_REF_CMD_RCGR_UPDATE_SHFT                                      0x0

#define HWIO_MSS_RBCPR_REF_CFG_RCGR_ADDR                                      (MPSS_PERPH_REG_BASE      + 0x000020d0)
#define HWIO_MSS_RBCPR_REF_CFG_RCGR_PHYS                                      (MPSS_PERPH_REG_BASE_PHYS + 0x000020d0)
#define HWIO_MSS_RBCPR_REF_CFG_RCGR_RMSK                                           0x71f
#define HWIO_MSS_RBCPR_REF_CFG_RCGR_IN          \
        in_dword_masked(HWIO_MSS_RBCPR_REF_CFG_RCGR_ADDR, HWIO_MSS_RBCPR_REF_CFG_RCGR_RMSK)
#define HWIO_MSS_RBCPR_REF_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_MSS_RBCPR_REF_CFG_RCGR_ADDR, m)
#define HWIO_MSS_RBCPR_REF_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_MSS_RBCPR_REF_CFG_RCGR_ADDR,v)
#define HWIO_MSS_RBCPR_REF_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_RBCPR_REF_CFG_RCGR_ADDR,m,v,HWIO_MSS_RBCPR_REF_CFG_RCGR_IN)
#define HWIO_MSS_RBCPR_REF_CFG_RCGR_SRC_SEL_BMSK                                   0x700
#define HWIO_MSS_RBCPR_REF_CFG_RCGR_SRC_SEL_SHFT                                     0x8
#define HWIO_MSS_RBCPR_REF_CFG_RCGR_SRC_DIV_BMSK                                    0x1f
#define HWIO_MSS_RBCPR_REF_CFG_RCGR_SRC_DIV_SHFT                                     0x0

#define HWIO_MSS_BUS_RBCPR_CBCR_ADDR                                          (MPSS_PERPH_REG_BASE      + 0x000020d4)
#define HWIO_MSS_BUS_RBCPR_CBCR_PHYS                                          (MPSS_PERPH_REG_BASE_PHYS + 0x000020d4)
#define HWIO_MSS_BUS_RBCPR_CBCR_RMSK                                          0x80000001
#define HWIO_MSS_BUS_RBCPR_CBCR_IN          \
        in_dword_masked(HWIO_MSS_BUS_RBCPR_CBCR_ADDR, HWIO_MSS_BUS_RBCPR_CBCR_RMSK)
#define HWIO_MSS_BUS_RBCPR_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_BUS_RBCPR_CBCR_ADDR, m)
#define HWIO_MSS_BUS_RBCPR_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_BUS_RBCPR_CBCR_ADDR,v)
#define HWIO_MSS_BUS_RBCPR_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BUS_RBCPR_CBCR_ADDR,m,v,HWIO_MSS_BUS_RBCPR_CBCR_IN)
#define HWIO_MSS_BUS_RBCPR_CBCR_CLKOFF_BMSK                                   0x80000000
#define HWIO_MSS_BUS_RBCPR_CBCR_CLKOFF_SHFT                                         0x1f
#define HWIO_MSS_BUS_RBCPR_CBCR_CLKEN_BMSK                                           0x1
#define HWIO_MSS_BUS_RBCPR_CBCR_CLKEN_SHFT                                           0x0

#define HWIO_MSS_RBCPR_REF_CBCR_ADDR                                          (MPSS_PERPH_REG_BASE      + 0x000020d8)
#define HWIO_MSS_RBCPR_REF_CBCR_PHYS                                          (MPSS_PERPH_REG_BASE_PHYS + 0x000020d8)
#define HWIO_MSS_RBCPR_REF_CBCR_RMSK                                          0x80000001
#define HWIO_MSS_RBCPR_REF_CBCR_IN          \
        in_dword_masked(HWIO_MSS_RBCPR_REF_CBCR_ADDR, HWIO_MSS_RBCPR_REF_CBCR_RMSK)
#define HWIO_MSS_RBCPR_REF_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_RBCPR_REF_CBCR_ADDR, m)
#define HWIO_MSS_RBCPR_REF_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_RBCPR_REF_CBCR_ADDR,v)
#define HWIO_MSS_RBCPR_REF_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_RBCPR_REF_CBCR_ADDR,m,v,HWIO_MSS_RBCPR_REF_CBCR_IN)
#define HWIO_MSS_RBCPR_REF_CBCR_CLKOFF_BMSK                                   0x80000000
#define HWIO_MSS_RBCPR_REF_CBCR_CLKOFF_SHFT                                         0x1f
#define HWIO_MSS_RBCPR_REF_CBCR_CLKEN_BMSK                                           0x1
#define HWIO_MSS_RBCPR_REF_CBCR_CLKEN_SHFT                                           0x0

#define HWIO_MSS_BUS_MGPI_CBCR_ADDR                                           (MPSS_PERPH_REG_BASE      + 0x000020dc)
#define HWIO_MSS_BUS_MGPI_CBCR_PHYS                                           (MPSS_PERPH_REG_BASE_PHYS + 0x000020dc)
#define HWIO_MSS_BUS_MGPI_CBCR_RMSK                                           0x80000001
#define HWIO_MSS_BUS_MGPI_CBCR_IN          \
        in_dword_masked(HWIO_MSS_BUS_MGPI_CBCR_ADDR, HWIO_MSS_BUS_MGPI_CBCR_RMSK)
#define HWIO_MSS_BUS_MGPI_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_BUS_MGPI_CBCR_ADDR, m)
#define HWIO_MSS_BUS_MGPI_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_BUS_MGPI_CBCR_ADDR,v)
#define HWIO_MSS_BUS_MGPI_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BUS_MGPI_CBCR_ADDR,m,v,HWIO_MSS_BUS_MGPI_CBCR_IN)
#define HWIO_MSS_BUS_MGPI_CBCR_CLKOFF_BMSK                                    0x80000000
#define HWIO_MSS_BUS_MGPI_CBCR_CLKOFF_SHFT                                          0x1f
#define HWIO_MSS_BUS_MGPI_CBCR_CLKEN_BMSK                                            0x1
#define HWIO_MSS_BUS_MGPI_CBCR_CLKEN_SHFT                                            0x0

#define HWIO_MSS_BUS_SLAVE_TIMEOUT_CBCR_ADDR                                  (MPSS_PERPH_REG_BASE      + 0x000020e0)
#define HWIO_MSS_BUS_SLAVE_TIMEOUT_CBCR_PHYS                                  (MPSS_PERPH_REG_BASE_PHYS + 0x000020e0)
#define HWIO_MSS_BUS_SLAVE_TIMEOUT_CBCR_RMSK                                  0x80000001
#define HWIO_MSS_BUS_SLAVE_TIMEOUT_CBCR_IN          \
        in_dword_masked(HWIO_MSS_BUS_SLAVE_TIMEOUT_CBCR_ADDR, HWIO_MSS_BUS_SLAVE_TIMEOUT_CBCR_RMSK)
#define HWIO_MSS_BUS_SLAVE_TIMEOUT_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_BUS_SLAVE_TIMEOUT_CBCR_ADDR, m)
#define HWIO_MSS_BUS_SLAVE_TIMEOUT_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_BUS_SLAVE_TIMEOUT_CBCR_ADDR,v)
#define HWIO_MSS_BUS_SLAVE_TIMEOUT_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BUS_SLAVE_TIMEOUT_CBCR_ADDR,m,v,HWIO_MSS_BUS_SLAVE_TIMEOUT_CBCR_IN)
#define HWIO_MSS_BUS_SLAVE_TIMEOUT_CBCR_CLKOFF_BMSK                           0x80000000
#define HWIO_MSS_BUS_SLAVE_TIMEOUT_CBCR_CLKOFF_SHFT                                 0x1f
#define HWIO_MSS_BUS_SLAVE_TIMEOUT_CBCR_CLKEN_BMSK                                   0x1
#define HWIO_MSS_BUS_SLAVE_TIMEOUT_CBCR_CLKEN_SHFT                                   0x0

#define HWIO_MSS_BUS_MVC_CBCR_ADDR                                            (MPSS_PERPH_REG_BASE      + 0x000020e4)
#define HWIO_MSS_BUS_MVC_CBCR_PHYS                                            (MPSS_PERPH_REG_BASE_PHYS + 0x000020e4)
#define HWIO_MSS_BUS_MVC_CBCR_RMSK                                            0x80000001
#define HWIO_MSS_BUS_MVC_CBCR_IN          \
        in_dword_masked(HWIO_MSS_BUS_MVC_CBCR_ADDR, HWIO_MSS_BUS_MVC_CBCR_RMSK)
#define HWIO_MSS_BUS_MVC_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_BUS_MVC_CBCR_ADDR, m)
#define HWIO_MSS_BUS_MVC_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_BUS_MVC_CBCR_ADDR,v)
#define HWIO_MSS_BUS_MVC_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BUS_MVC_CBCR_ADDR,m,v,HWIO_MSS_BUS_MVC_CBCR_IN)
#define HWIO_MSS_BUS_MVC_CBCR_CLKOFF_BMSK                                     0x80000000
#define HWIO_MSS_BUS_MVC_CBCR_CLKOFF_SHFT                                           0x1f
#define HWIO_MSS_BUS_MVC_CBCR_CLKEN_BMSK                                             0x1
#define HWIO_MSS_BUS_MVC_CBCR_CLKEN_SHFT                                             0x0

#define HWIO_MSS_BUS_STMR_CBCR_ADDR                                           (MPSS_PERPH_REG_BASE      + 0x000020e8)
#define HWIO_MSS_BUS_STMR_CBCR_PHYS                                           (MPSS_PERPH_REG_BASE_PHYS + 0x000020e8)
#define HWIO_MSS_BUS_STMR_CBCR_RMSK                                           0x80000001
#define HWIO_MSS_BUS_STMR_CBCR_IN          \
        in_dword_masked(HWIO_MSS_BUS_STMR_CBCR_ADDR, HWIO_MSS_BUS_STMR_CBCR_RMSK)
#define HWIO_MSS_BUS_STMR_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_BUS_STMR_CBCR_ADDR, m)
#define HWIO_MSS_BUS_STMR_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_BUS_STMR_CBCR_ADDR,v)
#define HWIO_MSS_BUS_STMR_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BUS_STMR_CBCR_ADDR,m,v,HWIO_MSS_BUS_STMR_CBCR_IN)
#define HWIO_MSS_BUS_STMR_CBCR_CLKOFF_BMSK                                    0x80000000
#define HWIO_MSS_BUS_STMR_CBCR_CLKOFF_SHFT                                          0x1f
#define HWIO_MSS_BUS_STMR_CBCR_CLKEN_BMSK                                            0x1
#define HWIO_MSS_BUS_STMR_CBCR_CLKEN_SHFT                                            0x0

#define HWIO_MSS_XO_CX_CBCR_ADDR                                              (MPSS_PERPH_REG_BASE      + 0x000020ec)
#define HWIO_MSS_XO_CX_CBCR_PHYS                                              (MPSS_PERPH_REG_BASE_PHYS + 0x000020ec)
#define HWIO_MSS_XO_CX_CBCR_RMSK                                              0x80000001
#define HWIO_MSS_XO_CX_CBCR_IN          \
        in_dword_masked(HWIO_MSS_XO_CX_CBCR_ADDR, HWIO_MSS_XO_CX_CBCR_RMSK)
#define HWIO_MSS_XO_CX_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_XO_CX_CBCR_ADDR, m)
#define HWIO_MSS_XO_CX_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_XO_CX_CBCR_ADDR,v)
#define HWIO_MSS_XO_CX_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_XO_CX_CBCR_ADDR,m,v,HWIO_MSS_XO_CX_CBCR_IN)
#define HWIO_MSS_XO_CX_CBCR_CLKOFF_BMSK                                       0x80000000
#define HWIO_MSS_XO_CX_CBCR_CLKOFF_SHFT                                             0x1f
#define HWIO_MSS_XO_CX_CBCR_CLKEN_BMSK                                               0x1
#define HWIO_MSS_XO_CX_CBCR_CLKEN_SHFT                                               0x0

#define HWIO_MSS_BUS_RFC_CBCR_ADDR                                            (MPSS_PERPH_REG_BASE      + 0x000020f0)
#define HWIO_MSS_BUS_RFC_CBCR_PHYS                                            (MPSS_PERPH_REG_BASE_PHYS + 0x000020f0)
#define HWIO_MSS_BUS_RFC_CBCR_RMSK                                            0x80007ff1
#define HWIO_MSS_BUS_RFC_CBCR_IN          \
        in_dword_masked(HWIO_MSS_BUS_RFC_CBCR_ADDR, HWIO_MSS_BUS_RFC_CBCR_RMSK)
#define HWIO_MSS_BUS_RFC_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_BUS_RFC_CBCR_ADDR, m)
#define HWIO_MSS_BUS_RFC_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_BUS_RFC_CBCR_ADDR,v)
#define HWIO_MSS_BUS_RFC_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BUS_RFC_CBCR_ADDR,m,v,HWIO_MSS_BUS_RFC_CBCR_IN)
#define HWIO_MSS_BUS_RFC_CBCR_CLKOFF_BMSK                                     0x80000000
#define HWIO_MSS_BUS_RFC_CBCR_CLKOFF_SHFT                                           0x1f
#define HWIO_MSS_BUS_RFC_CBCR_FORCE_MEM_CORE_ON_BMSK                              0x4000
#define HWIO_MSS_BUS_RFC_CBCR_FORCE_MEM_CORE_ON_SHFT                                 0xe
#define HWIO_MSS_BUS_RFC_CBCR_FORCE_MEM_PERIPH_ON_BMSK                            0x2000
#define HWIO_MSS_BUS_RFC_CBCR_FORCE_MEM_PERIPH_ON_SHFT                               0xd
#define HWIO_MSS_BUS_RFC_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                           0x1000
#define HWIO_MSS_BUS_RFC_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                              0xc
#define HWIO_MSS_BUS_RFC_CBCR_WAKEUP_BMSK                                          0xf00
#define HWIO_MSS_BUS_RFC_CBCR_WAKEUP_SHFT                                            0x8
#define HWIO_MSS_BUS_RFC_CBCR_SLEEP_BMSK                                            0xf0
#define HWIO_MSS_BUS_RFC_CBCR_SLEEP_SHFT                                             0x4
#define HWIO_MSS_BUS_RFC_CBCR_CLKEN_BMSK                                             0x1
#define HWIO_MSS_BUS_RFC_CBCR_CLKEN_SHFT                                             0x0

#define HWIO_MSS_SW_FEATURE_FUSES_ADDR                                        (MPSS_PERPH_REG_BASE      + 0x000020f4)
#define HWIO_MSS_SW_FEATURE_FUSES_PHYS                                        (MPSS_PERPH_REG_BASE_PHYS + 0x000020f4)
#define HWIO_MSS_SW_FEATURE_FUSES_RMSK                                        0xffffffff
#define HWIO_MSS_SW_FEATURE_FUSES_IN          \
        in_dword_masked(HWIO_MSS_SW_FEATURE_FUSES_ADDR, HWIO_MSS_SW_FEATURE_FUSES_RMSK)
#define HWIO_MSS_SW_FEATURE_FUSES_INM(m)      \
        in_dword_masked(HWIO_MSS_SW_FEATURE_FUSES_ADDR, m)
#define HWIO_MSS_SW_FEATURE_FUSES_MSS_SW_FEATURE_FUSES_31_0_BMSK              0xffffffff
#define HWIO_MSS_SW_FEATURE_FUSES_MSS_SW_FEATURE_FUSES_31_0_SHFT                     0x0

#define HWIO_MSS_SW_FEATURE_FUSES2_ADDR                                       (MPSS_PERPH_REG_BASE      + 0x000020f8)
#define HWIO_MSS_SW_FEATURE_FUSES2_PHYS                                       (MPSS_PERPH_REG_BASE_PHYS + 0x000020f8)
#define HWIO_MSS_SW_FEATURE_FUSES2_RMSK                                       0xffffffff
#define HWIO_MSS_SW_FEATURE_FUSES2_IN          \
        in_dword_masked(HWIO_MSS_SW_FEATURE_FUSES2_ADDR, HWIO_MSS_SW_FEATURE_FUSES2_RMSK)
#define HWIO_MSS_SW_FEATURE_FUSES2_INM(m)      \
        in_dword_masked(HWIO_MSS_SW_FEATURE_FUSES2_ADDR, m)
#define HWIO_MSS_SW_FEATURE_FUSES2_MSS_SW_FEATURE_FUSES2_31_0_BMSK            0xffffffff
#define HWIO_MSS_SW_FEATURE_FUSES2_MSS_SW_FEATURE_FUSES2_31_0_SHFT                   0x0

#define HWIO_MSS_RESERVE_01_ADDR                                              (MPSS_PERPH_REG_BASE      + 0x000020fc)
#define HWIO_MSS_RESERVE_01_PHYS                                              (MPSS_PERPH_REG_BASE_PHYS + 0x000020fc)
#define HWIO_MSS_RESERVE_01_RMSK                                              0xffffffff
#define HWIO_MSS_RESERVE_01_IN          \
        in_dword_masked(HWIO_MSS_RESERVE_01_ADDR, HWIO_MSS_RESERVE_01_RMSK)
#define HWIO_MSS_RESERVE_01_INM(m)      \
        in_dword_masked(HWIO_MSS_RESERVE_01_ADDR, m)
#define HWIO_MSS_RESERVE_01_OUT(v)      \
        out_dword(HWIO_MSS_RESERVE_01_ADDR,v)
#define HWIO_MSS_RESERVE_01_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_RESERVE_01_ADDR,m,v,HWIO_MSS_RESERVE_01_IN)
#define HWIO_MSS_RESERVE_01_MSS_RESERVE_01_BMSK                               0xffffffff
#define HWIO_MSS_RESERVE_01_MSS_RESERVE_01_SHFT                                      0x0

#define HWIO_MSS_RESERVE_02_ADDR                                              (MPSS_PERPH_REG_BASE      + 0x00002100)
#define HWIO_MSS_RESERVE_02_PHYS                                              (MPSS_PERPH_REG_BASE_PHYS + 0x00002100)
#define HWIO_MSS_RESERVE_02_RMSK                                              0xffffffff
#define HWIO_MSS_RESERVE_02_IN          \
        in_dword_masked(HWIO_MSS_RESERVE_02_ADDR, HWIO_MSS_RESERVE_02_RMSK)
#define HWIO_MSS_RESERVE_02_INM(m)      \
        in_dword_masked(HWIO_MSS_RESERVE_02_ADDR, m)
#define HWIO_MSS_RESERVE_02_OUT(v)      \
        out_dword(HWIO_MSS_RESERVE_02_ADDR,v)
#define HWIO_MSS_RESERVE_02_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_RESERVE_02_ADDR,m,v,HWIO_MSS_RESERVE_02_IN)
#define HWIO_MSS_RESERVE_02_MSS_RESERVE_02_BMSK                               0xffffffff
#define HWIO_MSS_RESERVE_02_MSS_RESERVE_02_SHFT                                      0x0

/*----------------------------------------------------------------------------
 * MODULE: SECURITY_CONTROL_CORE
 *--------------------------------------------------------------------------*/

#define SECURITY_CONTROL_CORE_REG_BASE                                                         (SECURITY_CONTROL_BASE      + 0x00000000)
#define SECURITY_CONTROL_CORE_REG_BASE_PHYS                                                    (SECURITY_CONTROL_BASE_PHYS + 0x00000000)

#define HWIO_QFPROM_RAW_JTAG_ID_ADDR                                                           (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000000)
#define HWIO_QFPROM_RAW_JTAG_ID_PHYS                                                           (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00000000)
#define HWIO_QFPROM_RAW_JTAG_ID_RMSK                                                           0xffffffff
#define HWIO_QFPROM_RAW_JTAG_ID_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_JTAG_ID_ADDR, HWIO_QFPROM_RAW_JTAG_ID_RMSK)
#define HWIO_QFPROM_RAW_JTAG_ID_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_JTAG_ID_ADDR, m)
#define HWIO_QFPROM_RAW_JTAG_ID_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_JTAG_ID_ADDR,v)
#define HWIO_QFPROM_RAW_JTAG_ID_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_JTAG_ID_ADDR,m,v,HWIO_QFPROM_RAW_JTAG_ID_IN)
#define HWIO_QFPROM_RAW_JTAG_ID_IDDQ_CX_ON_BMSK                                                0xc0000000
#define HWIO_QFPROM_RAW_JTAG_ID_IDDQ_CX_ON_SHFT                                                      0x1e
#define HWIO_QFPROM_RAW_JTAG_ID_IDDQ_RC_BMSK                                                   0x20000000
#define HWIO_QFPROM_RAW_JTAG_ID_IDDQ_RC_SHFT                                                         0x1d
#define HWIO_QFPROM_RAW_JTAG_ID_MACCHIATO_EN_BMSK                                              0x10000000
#define HWIO_QFPROM_RAW_JTAG_ID_MACCHIATO_EN_SHFT                                                    0x1c
#define HWIO_QFPROM_RAW_JTAG_ID_FEATURE_ID_BMSK                                                 0xff00000
#define HWIO_QFPROM_RAW_JTAG_ID_FEATURE_ID_SHFT                                                      0x14
#define HWIO_QFPROM_RAW_JTAG_ID_JTAG_ID_BMSK                                                      0xfffff
#define HWIO_QFPROM_RAW_JTAG_ID_JTAG_ID_SHFT                                                          0x0

#define HWIO_QFPROM_RAW_PTE1_ADDR                                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000004)
#define HWIO_QFPROM_RAW_PTE1_PHYS                                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00000004)
#define HWIO_QFPROM_RAW_PTE1_RMSK                                                              0xffffffff
#define HWIO_QFPROM_RAW_PTE1_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_PTE1_ADDR, HWIO_QFPROM_RAW_PTE1_RMSK)
#define HWIO_QFPROM_RAW_PTE1_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_PTE1_ADDR, m)
#define HWIO_QFPROM_RAW_PTE1_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_PTE1_ADDR,v)
#define HWIO_QFPROM_RAW_PTE1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_PTE1_ADDR,m,v,HWIO_QFPROM_RAW_PTE1_IN)
#define HWIO_QFPROM_RAW_PTE1_IDDQ_BIN_BMSK                                                     0xe0000000
#define HWIO_QFPROM_RAW_PTE1_IDDQ_BIN_SHFT                                                           0x1d
#define HWIO_QFPROM_RAW_PTE1_IDDQ_MULTIPLIER_BMSK                                              0x1c000000
#define HWIO_QFPROM_RAW_PTE1_IDDQ_MULTIPLIER_SHFT                                                    0x1a
#define HWIO_QFPROM_RAW_PTE1_PROCESS_NODE_ID_BMSK                                               0x2000000
#define HWIO_QFPROM_RAW_PTE1_PROCESS_NODE_ID_SHFT                                                    0x19
#define HWIO_QFPROM_RAW_PTE1_IDDQ_MX_OFF_BMSK                                                   0x1f80000
#define HWIO_QFPROM_RAW_PTE1_IDDQ_MX_OFF_SHFT                                                        0x13
#define HWIO_QFPROM_RAW_PTE1_IDDQ_CX_OFF_BMSK                                                     0x7f000
#define HWIO_QFPROM_RAW_PTE1_IDDQ_CX_OFF_SHFT                                                         0xc
#define HWIO_QFPROM_RAW_PTE1_IDDQ_MX_ON_BMSK                                                        0xfe0
#define HWIO_QFPROM_RAW_PTE1_IDDQ_MX_ON_SHFT                                                          0x5
#define HWIO_QFPROM_RAW_PTE1_IDDQ_CX_ON_BMSK                                                         0x1f
#define HWIO_QFPROM_RAW_PTE1_IDDQ_CX_ON_SHFT                                                          0x0

#define HWIO_QFPROM_RAW_SERIAL_NUM_ADDR                                                        (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000008)
#define HWIO_QFPROM_RAW_SERIAL_NUM_PHYS                                                        (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00000008)
#define HWIO_QFPROM_RAW_SERIAL_NUM_RMSK                                                        0xffffffff
#define HWIO_QFPROM_RAW_SERIAL_NUM_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_SERIAL_NUM_ADDR, HWIO_QFPROM_RAW_SERIAL_NUM_RMSK)
#define HWIO_QFPROM_RAW_SERIAL_NUM_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_SERIAL_NUM_ADDR, m)
#define HWIO_QFPROM_RAW_SERIAL_NUM_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_SERIAL_NUM_ADDR,v)
#define HWIO_QFPROM_RAW_SERIAL_NUM_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_SERIAL_NUM_ADDR,m,v,HWIO_QFPROM_RAW_SERIAL_NUM_IN)
#define HWIO_QFPROM_RAW_SERIAL_NUM_SERIAL_NUM_BMSK                                             0xffffffff
#define HWIO_QFPROM_RAW_SERIAL_NUM_SERIAL_NUM_SHFT                                                    0x0

#define HWIO_QFPROM_RAW_PTE2_ADDR                                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000000c)
#define HWIO_QFPROM_RAW_PTE2_PHYS                                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x0000000c)
#define HWIO_QFPROM_RAW_PTE2_RMSK                                                              0xffffffff
#define HWIO_QFPROM_RAW_PTE2_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_PTE2_ADDR, HWIO_QFPROM_RAW_PTE2_RMSK)
#define HWIO_QFPROM_RAW_PTE2_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_PTE2_ADDR, m)
#define HWIO_QFPROM_RAW_PTE2_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_PTE2_ADDR,v)
#define HWIO_QFPROM_RAW_PTE2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_PTE2_ADDR,m,v,HWIO_QFPROM_RAW_PTE2_IN)
#define HWIO_QFPROM_RAW_PTE2_PTE_IDDQ_SPARE_BMSK                                               0xfc000000
#define HWIO_QFPROM_RAW_PTE2_PTE_IDDQ_SPARE_SHFT                                                     0x1a
#define HWIO_QFPROM_RAW_PTE2_IDDQ_MSS_OFF_BMSK                                                  0x3f80000
#define HWIO_QFPROM_RAW_PTE2_IDDQ_MSS_OFF_SHFT                                                       0x13
#define HWIO_QFPROM_RAW_PTE2_IDDQ_MSS_ON_BMSK                                                     0x7f800
#define HWIO_QFPROM_RAW_PTE2_IDDQ_MSS_ON_SHFT                                                         0xb
#define HWIO_QFPROM_RAW_PTE2_FOUNDRY_ID_BMSK                                                        0x700
#define HWIO_QFPROM_RAW_PTE2_FOUNDRY_ID_SHFT                                                          0x8
#define HWIO_QFPROM_RAW_PTE2_LOGIC_RETENTION_BMSK                                                    0xe0
#define HWIO_QFPROM_RAW_PTE2_LOGIC_RETENTION_SHFT                                                     0x5
#define HWIO_QFPROM_RAW_PTE2_SPEED_BIN_BMSK                                                          0x1c
#define HWIO_QFPROM_RAW_PTE2_SPEED_BIN_SHFT                                                           0x2
#define HWIO_QFPROM_RAW_PTE2_MX_RET_BIN_BMSK                                                          0x3
#define HWIO_QFPROM_RAW_PTE2_MX_RET_BIN_SHFT                                                          0x0

#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_ADDR                                                    (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000010)
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_PHYS                                                    (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00000010)
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_RMSK                                                    0xffffffff
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_RD_WR_PERM_LSB_ADDR, HWIO_QFPROM_RAW_RD_WR_PERM_LSB_RMSK)
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_RD_WR_PERM_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_RD_WR_PERM_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_RD_WR_PERM_LSB_ADDR,m,v,HWIO_QFPROM_RAW_RD_WR_PERM_LSB_IN)
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SPARE_BMSK                                              0xffc00000
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SPARE_SHFT                                                    0x16
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SPARE21_BMSK                                              0x200000
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SPARE21_SHFT                                                  0x15
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SPARE20_BMSK                                              0x100000
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SPARE20_SHFT                                                  0x14
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SPARE19_BMSK                                               0x80000
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SPARE19_SHFT                                                  0x13
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SPARE18_BMSK                                               0x40000
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SPARE18_SHFT                                                  0x12
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SPARE17_BMSK                                               0x20000
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SPARE17_SHFT                                                  0x11
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SPARE16_BMSK                                               0x10000
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SPARE16_SHFT                                                  0x10
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_FEC_EN_BMSK                                                 0x8000
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_FEC_EN_SHFT                                                    0xf
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_BOOT_ROM_PATCH_BMSK                                         0x4000
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_BOOT_ROM_PATCH_SHFT                                            0xe
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_MEM_CONFIG_BMSK                                             0x2000
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_MEM_CONFIG_SHFT                                                0xd
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_CALIB_BMSK                                                  0x1000
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_CALIB_SHFT                                                     0xc
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_PK_HASH_BMSK                                                 0x800
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_PK_HASH_SHFT                                                   0xb
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_QC_SEC_BOOT_BMSK                                             0x400
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_QC_SEC_BOOT_SHFT                                               0xa
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_OEM_SEC_BOOT_BMSK                                            0x200
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_OEM_SEC_BOOT_SHFT                                              0x9
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SEC_KEY_DERIVATION_KEY_BMSK                                  0x100
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_SEC_KEY_DERIVATION_KEY_SHFT                                    0x8
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_PRI_KEY_DERIVATION_KEY_BMSK                                   0x80
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_PRI_KEY_DERIVATION_KEY_SHFT                                    0x7
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_FEAT_CONFIG_BMSK                                              0x40
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_FEAT_CONFIG_SHFT                                               0x6
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_OEM_CONFIG_BMSK                                               0x20
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_OEM_CONFIG_SHFT                                                0x5
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_ANTI_ROLLBACK_3_BMSK                                          0x10
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_ANTI_ROLLBACK_3_SHFT                                           0x4
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_ANTI_ROLLBACK_2_BMSK                                           0x8
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_ANTI_ROLLBACK_2_SHFT                                           0x3
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_ANTI_ROLLBACK_1_BMSK                                           0x4
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_ANTI_ROLLBACK_1_SHFT                                           0x2
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_RD_WR_PERM_BMSK                                                0x2
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_RD_WR_PERM_SHFT                                                0x1
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_PTE_BMSK                                                       0x1
#define HWIO_QFPROM_RAW_RD_WR_PERM_LSB_PTE_SHFT                                                       0x0

#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_ADDR                                                    (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000014)
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_PHYS                                                    (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00000014)
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_RMSK                                                    0xffffffff
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_RD_WR_PERM_MSB_ADDR, HWIO_QFPROM_RAW_RD_WR_PERM_MSB_RMSK)
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_RD_WR_PERM_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_RD_WR_PERM_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_RD_WR_PERM_MSB_ADDR,m,v,HWIO_QFPROM_RAW_RD_WR_PERM_MSB_IN)
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SPARE_BMSK                                              0xffc00000
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SPARE_SHFT                                                    0x16
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SPARE21_BMSK                                              0x200000
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SPARE21_SHFT                                                  0x15
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SPARE20_BMSK                                              0x100000
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SPARE20_SHFT                                                  0x14
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SPARE19_BMSK                                               0x80000
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SPARE19_SHFT                                                  0x13
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SPARE18_BMSK                                               0x40000
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SPARE18_SHFT                                                  0x12
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SPARE17_BMSK                                               0x20000
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SPARE17_SHFT                                                  0x11
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SPARE16_BMSK                                               0x10000
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SPARE16_SHFT                                                  0x10
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_FEC_EN_BMSK                                                 0x8000
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_FEC_EN_SHFT                                                    0xf
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_BOOT_ROM_PATCH_BMSK                                         0x4000
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_BOOT_ROM_PATCH_SHFT                                            0xe
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_MEM_CONFIG_BMSK                                             0x2000
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_MEM_CONFIG_SHFT                                                0xd
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_CALIB_BMSK                                                  0x1000
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_CALIB_SHFT                                                     0xc
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_PK_HASH_BMSK                                                 0x800
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_PK_HASH_SHFT                                                   0xb
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_QC_SEC_BOOT_BMSK                                             0x400
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_QC_SEC_BOOT_SHFT                                               0xa
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_OEM_SEC_BOOT_BMSK                                            0x200
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_OEM_SEC_BOOT_SHFT                                              0x9
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SEC_KEY_DERIVATION_KEY_BMSK                                  0x100
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_SEC_KEY_DERIVATION_KEY_SHFT                                    0x8
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_PRI_KEY_DERIVATION_KEY_BMSK                                   0x80
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_PRI_KEY_DERIVATION_KEY_SHFT                                    0x7
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_FEAT_CONFIG_BMSK                                              0x40
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_FEAT_CONFIG_SHFT                                               0x6
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_OEM_CONFIG_BMSK                                               0x20
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_OEM_CONFIG_SHFT                                                0x5
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_ANTI_ROLLBACK_3_BMSK                                          0x10
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_ANTI_ROLLBACK_3_SHFT                                           0x4
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_ANTI_ROLLBACK_2_BMSK                                           0x8
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_ANTI_ROLLBACK_2_SHFT                                           0x3
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_ANTI_ROLLBACK_1_BMSK                                           0x4
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_ANTI_ROLLBACK_1_SHFT                                           0x2
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_RD_WR_PERM_BMSK                                                0x2
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_RD_WR_PERM_SHFT                                                0x1
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_PTE_BMSK                                                       0x1
#define HWIO_QFPROM_RAW_RD_WR_PERM_MSB_PTE_SHFT                                                       0x0

#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_ADDR                                               (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000018)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_PHYS                                               (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00000018)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_RMSK                                               0xffffffff
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_ADDR, HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_RMSK)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_ADDR,m,v,HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_IN)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_PIL_SUBSYSTEM0_BMSK                                0xfc000000
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_PIL_SUBSYSTEM0_SHFT                                      0x1a
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_TZ_BMSK                                             0x3fff000
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_TZ_SHFT                                                   0xc
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_SBL1_BMSK                                               0xffe
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_SBL1_SHFT                                                 0x1
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_RPMB_KEY_PROVISIONED_BMSK                                 0x1
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_LSB_RPMB_KEY_PROVISIONED_SHFT                                 0x0

#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_ADDR                                               (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000001c)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_PHYS                                               (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x0000001c)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_RMSK                                               0xffffffff
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_ADDR, HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_RMSK)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_ADDR,m,v,HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_IN)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_APPSBL0_BMSK                                       0xfffc0000
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_APPSBL0_SHFT                                             0x12
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_PIL_SUBSYSTEM1_BMSK                                   0x3ffff
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_1_MSB_PIL_SUBSYSTEM1_SHFT                                       0x0

#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_LSB_ADDR                                               (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000020)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_LSB_PHYS                                               (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00000020)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_LSB_RMSK                                               0xffffffff
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_LSB_ADDR, HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_LSB_RMSK)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_LSB_ADDR,m,v,HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_LSB_IN)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_LSB_APPSBL1_BMSK                                       0xffffffff
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_LSB_APPSBL1_SHFT                                              0x0

#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_ADDR                                               (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000024)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_PHYS                                               (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00000024)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_RMSK                                               0xffffffff
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_ADDR, HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_RMSK)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_ADDR,m,v,HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_IN)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_ROOT_CERT_PK_HASH_INDEX_BMSK                       0xff000000
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_ROOT_CERT_PK_HASH_INDEX_SHFT                             0x18
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_HYPERVISOR_BMSK                                      0xfff000
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_HYPERVISOR_SHFT                                           0xc
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_RPM_BMSK                                                0xff0
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_RPM_SHFT                                                  0x4
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_APPSBL2_BMSK                                              0xf
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_2_MSB_APPSBL2_SHFT                                              0x0

#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_ADDR                                               (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000028)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_PHYS                                               (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00000028)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_RMSK                                               0xffffffff
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_ADDR, HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_RMSK)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_ADDR,m,v,HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_IN)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_MSS_BMSK                                           0xffff0000
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_MSS_SHFT                                                 0x10
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_MBA_BMSK                                               0xffff
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_LSB_MBA_SHFT                                                  0x0

#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_ADDR                                               (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000002c)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_PHYS                                               (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x0000002c)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_RMSK                                               0xffffffff
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_ADDR, HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_RMSK)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_ADDR,m,v,HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_IN)
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_SPARE0_BMSK                                        0xffffff00
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_SPARE0_SHFT                                               0x8
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_MODEM_ROOT_CERT_PK_HASH_INDEX_BMSK                       0xff
#define HWIO_QFPROM_RAW_ANTI_ROLLBACK_3_MSB_MODEM_ROOT_CERT_PK_HASH_INDEX_SHFT                        0x0

#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_ADDR                                               (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000030)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_PHYS                                               (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00000030)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_RMSK                                               0xffffffff
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_ADDR, HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_RMSK)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_ADDR,m,v,HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_IN)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE0_BMSK                                        0xff000000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE0_SHFT                                              0x18
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE_REG21_SECURE_BMSK                              0x800000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE_REG21_SECURE_SHFT                                  0x17
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_PBL_LOG_DISABLE_BMSK                                 0x400000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_PBL_LOG_DISABLE_SHFT                                     0x16
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_ROOT_CERT_TOTAL_NUM_BMSK                             0x3c0000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_ROOT_CERT_TOTAL_NUM_SHFT                                 0x12
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE_REG20_SECURE_BMSK                               0x20000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE_REG20_SECURE_SHFT                                  0x11
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE_REG19_SECURE_BMSK                               0x10000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE_REG19_SECURE_SHFT                                  0x10
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE_REG18_SECURE_BMSK                                0x8000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE_REG18_SECURE_SHFT                                   0xf
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE_REG17_SECURE_BMSK                                0x4000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE_REG17_SECURE_SHFT                                   0xe
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE_REG16_SECURE_BMSK                                0x2000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE_REG16_SECURE_SHFT                                   0xd
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_WDOG_EN_BMSK                                           0x1000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_WDOG_EN_SHFT                                              0xc
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPDM_SECURE_MODE_BMSK                                   0x800
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPDM_SECURE_MODE_SHFT                                     0xb
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_ALT_SD_PORT_FOR_BOOT_BMSK                               0x400
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_ALT_SD_PORT_FOR_BOOT_SHFT                                 0xa
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SDC_EMMC_MODE1P2_GPIO_DISABLE_BMSK                      0x200
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SDC_EMMC_MODE1P2_GPIO_DISABLE_SHFT                        0x9
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SDC_EMMC_MODE1P2_EN_BMSK                                0x100
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SDC_EMMC_MODE1P2_EN_SHFT                                  0x8
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_FAST_BOOT_BMSK                                           0xe0
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_FAST_BOOT_SHFT                                            0x5
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SDCC_MCLK_BOOT_FREQ_BMSK                                 0x10
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SDCC_MCLK_BOOT_FREQ_SHFT                                  0x4
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_FORCE_DLOAD_DISABLE_BMSK                                  0x8
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_FORCE_DLOAD_DISABLE_SHFT                                  0x3
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE_BMSK                                                0x4
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_SPARE_SHFT                                                0x2
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_ENUM_TIMEOUT_BMSK                                         0x2
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_ENUM_TIMEOUT_SHFT                                         0x1
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_E_DLOAD_DISABLE_BMSK                                      0x1
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_E_DLOAD_DISABLE_SHFT                                      0x0

#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_ADDR                                               (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000034)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_PHYS                                               (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00000034)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_RMSK                                               0xffffffff
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_ADDR, HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_RMSK)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_ADDR,m,v,HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_IN)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_SPARE1_BMSK                                        0xff800000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_SPARE1_SHFT                                              0x17
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_ANTI_ROLLBACK_FEATURE_EN_BMSK                        0x780000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_ANTI_ROLLBACK_FEATURE_EN_SHFT                            0x13
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_DAP_DEVICEEN_DISABLE_BMSK                             0x40000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_DAP_DEVICEEN_DISABLE_SHFT                                0x12
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_DAP_SPNIDEN_DISABLE_BMSK                              0x20000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_DAP_SPNIDEN_DISABLE_SHFT                                 0x11
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_DAP_SPIDEN_DISABLE_BMSK                               0x10000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_DAP_SPIDEN_DISABLE_SHFT                                  0x10
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_DAP_NIDEN_DISABLE_BMSK                                 0x8000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_DAP_NIDEN_DISABLE_SHFT                                    0xf
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_DAP_DBGEN_DISABLE_BMSK                                 0x4000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_DAP_DBGEN_DISABLE_SHFT                                    0xe
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_APPS_SPNIDEN_DISABLE_BMSK                              0x2000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_APPS_SPNIDEN_DISABLE_SHFT                                 0xd
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_APPS_SPIDEN_DISABLE_BMSK                               0x1000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_APPS_SPIDEN_DISABLE_SHFT                                  0xc
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_APPS_NIDEN_DISABLE_BMSK                                 0x800
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_APPS_NIDEN_DISABLE_SHFT                                   0xb
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_APPS_DBGEN_DISABLE_BMSK                                 0x400
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_APPS_DBGEN_DISABLE_SHFT                                   0xa
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_SPARE1_DISABLE_BMSK                                     0x200
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_SPARE1_DISABLE_SHFT                                       0x9
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_SPARE0_DISABLE_BMSK                                     0x100
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_SPARE0_DISABLE_SHFT                                       0x8
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_VENUS_0_DBGEN_DISABLE_BMSK                               0x80
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_VENUS_0_DBGEN_DISABLE_SHFT                                0x7
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_RPM_DAPEN_DISABLE_BMSK                                   0x40
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_RPM_DAPEN_DISABLE_SHFT                                    0x6
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_RPM_WCSS_NIDEN_DISABLE_BMSK                              0x20
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_RPM_WCSS_NIDEN_DISABLE_SHFT                               0x5
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_RPM_DBGEN_DISABLE_BMSK                                   0x10
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_RPM_DBGEN_DISABLE_SHFT                                    0x4
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_WCSS_DBGEN_DISABLE_BMSK                                   0x8
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_WCSS_DBGEN_DISABLE_SHFT                                   0x3
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_MSS_NIDEN_DISABLE_BMSK                                    0x4
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_MSS_NIDEN_DISABLE_SHFT                                    0x2
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_MSS_DBGEN_DISABLE_BMSK                                    0x2
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_MSS_DBGEN_DISABLE_SHFT                                    0x1
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_ALL_DEBUG_DISABLE_BMSK                                    0x1
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_MSB_ALL_DEBUG_DISABLE_SHFT                                    0x0

#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_ADDR                                               (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000038)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_PHYS                                               (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00000038)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_RMSK                                               0xffffffff
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_ADDR, HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_RMSK)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_ADDR,m,v,HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_IN)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_OEM_PRODUCT_ID_BMSK                                0xffff0000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_OEM_PRODUCT_ID_SHFT                                      0x10
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_OEM_HW_ID_BMSK                                         0xffff
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_LSB_OEM_HW_ID_SHFT                                            0x0

#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_ADDR                                               (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000003c)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_PHYS                                               (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x0000003c)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_RMSK                                               0xffffffff
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_ADDR, HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_RMSK)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_ADDR,m,v,HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_IN)
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_PERIPH_VID_BMSK                                    0xffff0000
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_PERIPH_VID_SHFT                                          0x10
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_PERIPH_PID_BMSK                                        0xffff
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW1_MSB_PERIPH_PID_SHFT                                           0x0

#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000040)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_PHYS                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00000040)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_ADDR, HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_RMSK)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_ADDR,m,v,HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_IN)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_VU_EN_BMSK                             0xfc000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_VU_EN_SHFT                                   0x1a
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_NAV_EN_BMSK                                   0x2000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_NAV_EN_SHFT                                        0x19
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_LDO_EN_BMSK                                   0x1000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_LDO_EN_SHFT                                        0x18
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_ECS_EN_BMSK                                    0x800000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_ECS_EN_SHFT                                        0x17
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_WCDMA_EN_BMSK                            0x600000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_WCDMA_EN_SHFT                                0x15
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_TDSCDMA_EN_BMSK                          0x180000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_TDSCDMA_EN_SHFT                              0x13
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_LTE_EN_BMSK                               0x60000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_LTE_EN_SHFT                                  0x11
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_LTE_ABOVE_CAT2_EN_BMSK                    0x18000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_LTE_ABOVE_CAT2_EN_SHFT                        0xf
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_LTE_ABOVE_CAT1_EN_BMSK                     0x6000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_LTE_ABOVE_CAT1_EN_SHFT                        0xd
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_HSPA_MIMO_EN_BMSK                          0x1800
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_HSPA_MIMO_EN_SHFT                             0xb
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_HSPA_EN_BMSK                                0x600
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_HSPA_EN_SHFT                                  0x9
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_HSPA_DC_EN_BMSK                             0x180
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_HSPA_DC_EN_SHFT                               0x7
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_GENRAN_EN_BMSK                               0x60
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_GENRAN_EN_SHFT                                0x5
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_EN_BMSK                                      0x10
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_EN_SHFT                                       0x4
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_DO_EN_BMSK                                    0xc
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_DO_EN_SHFT                                    0x2
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_1X_EN_BMSK                                    0x3
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_1X_EN_SHFT                                    0x0

#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000044)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_PHYS                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00000044)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_ADDR, HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_RMSK)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_ADDR,m,v,HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_IN)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_SDC_EMMC_MODE1P2_FORCE_GPIO_BMSK                  0x80000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_SDC_EMMC_MODE1P2_FORCE_GPIO_SHFT                        0x1f
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_SPARE_BMSK                                        0x7ff00000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_SPARE_SHFT                                              0x14
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_MSMC_MDSP_FW_EN_BMSK                                 0xfff00
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_MSMC_MDSP_FW_EN_SHFT                                     0x8
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_MSMC_SPARE_BMSK                                         0xc0
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_MSMC_SPARE_SHFT                                          0x6
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_MSMC_MODEM_VU_EN_BMSK                                   0x3f
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW0_MSB_MSMC_MODEM_VU_EN_SHFT                                    0x0

#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000048)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_PHYS                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00000048)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_ADDR, HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_RMSK)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_ADDR,m,v,HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_IN)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_ACC_DISABLE_BMSK                                  0x80000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_ACC_DISABLE_SHFT                                        0x1f
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_SHADOW_READ_DISABLE_BMSK                          0x40000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_SHADOW_READ_DISABLE_SHFT                                0x1e
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_SPARE1_BMSK                                       0x30000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_SPARE1_SHFT                                             0x1c
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_FUSE_SMT_PERM_ENABLE_BMSK                          0x8000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_FUSE_SMT_PERM_ENABLE_SHFT                               0x1b
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_FUSE_RCP_BYPASS_ENABLE_BMSK                        0x4000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_FUSE_RCP_BYPASS_ENABLE_SHFT                             0x1a
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_MSMC_SMMU_BYPASS_DISABLE_BMSK                      0x2000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_MSMC_SMMU_BYPASS_DISABLE_SHFT                           0x19
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_MSMC_GPIO_SLIMBUS_PD_DISABLE_BMSK                  0x1000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_MSMC_GPIO_SLIMBUS_PD_DISABLE_SHFT                       0x18
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_FUSE_PCIE20_RC_DISABLE_BMSK                         0x800000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_FUSE_PCIE20_RC_DISABLE_SHFT                             0x17
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_MSMC_PCIE_DISABLE_BMSK                              0x400000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_MSMC_PCIE_DISABLE_SHFT                                  0x16
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_NDINT_DISABLE_BMSK                                  0x200000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_NDINT_DISABLE_SHFT                                      0x15
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_DAP_DEVICEEN_DISABLE_BMSK                        0x100000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_DAP_DEVICEEN_DISABLE_SHFT                            0x14
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_DAP_SPNIDEN_DISABLE_BMSK                          0x80000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_DAP_SPNIDEN_DISABLE_SHFT                             0x13
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_DAP_SPIDEN_DISABLE_BMSK                           0x40000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_DAP_SPIDEN_DISABLE_SHFT                              0x12
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_DAP_NIDEN_DISABLE_BMSK                            0x20000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_DAP_NIDEN_DISABLE_SHFT                               0x11
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_DAP_DBGEN_DISABLE_BMSK                            0x10000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_DAP_DBGEN_DISABLE_SHFT                               0x10
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_APPS_SPNIDEN_DISABLE_BMSK                          0x8000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_APPS_SPNIDEN_DISABLE_SHFT                             0xf
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_APPS_SPIDEN_DISABLE_BMSK                           0x4000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_APPS_SPIDEN_DISABLE_SHFT                              0xe
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_APPS_NIDEN_DISABLE_BMSK                            0x2000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_APPS_NIDEN_DISABLE_SHFT                               0xd
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_APPS_DBGEN_DISABLE_BMSK                            0x1000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_APPS_DBGEN_DISABLE_SHFT                               0xc
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_SPARE1_DISABLE_BMSK                                 0x800
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_SPARE1_DISABLE_SHFT                                   0xb
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_SPARE0_DISABLE_BMSK                                 0x400
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_SPARE0_DISABLE_SHFT                                   0xa
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_VENUS_0_DBGEN_DISABLE_BMSK                          0x200
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_VENUS_0_DBGEN_DISABLE_SHFT                            0x9
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_RPM_DAPEN_DISABLE_BMSK                              0x100
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_RPM_DAPEN_DISABLE_SHFT                                0x8
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_RPM_WCSS_NIDEN_DISABLE_BMSK                          0x80
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_RPM_WCSS_NIDEN_DISABLE_SHFT                           0x7
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_RPM_DBGEN_DISABLE_BMSK                               0x40
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_RPM_DBGEN_DISABLE_SHFT                                0x6
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_WCSS_DBGEN_DISABLE_BMSK                              0x20
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_WCSS_DBGEN_DISABLE_SHFT                               0x5
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_MSS_NIDEN_DISABLE_BMSK                               0x10
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_MSS_NIDEN_DISABLE_SHFT                                0x4
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_MSS_DBGEN_DISABLE_BMSK                                0x8
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QC_MSS_DBGEN_DISABLE_SHFT                                0x3
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QDI_SPMI_DISABLE_BMSK                                    0x4
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_QDI_SPMI_DISABLE_SHFT                                    0x2
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_SM_BIST_DISABLE_BMSK                                     0x2
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_SM_BIST_DISABLE_SHFT                                     0x1
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_TIC_DISABLE_BMSK                                         0x1
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_LSB_TIC_DISABLE_SHFT                                         0x0

#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000004c)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_PHYS                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x0000004c)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_ADDR, HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_RMSK)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_ADDR,m,v,HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_IN)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_SEC_TAP_ACCESS_DISABLE_BMSK                       0xf8000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_SEC_TAP_ACCESS_DISABLE_SHFT                             0x1b
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_TAP_CJI_CORE_SEL_DISABLE_BMSK                      0x4000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_TAP_CJI_CORE_SEL_DISABLE_SHFT                           0x1a
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_TAP_INSTR_DISABLE_BMSK                             0x3ffc000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_TAP_INSTR_DISABLE_SHFT                                   0xe
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_SPARE1_BMSK                                           0x3c00
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_SPARE1_SHFT                                              0xa
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_MODEM_PBL_PATCH_VERSION_BMSK                           0x3e0
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_MODEM_PBL_PATCH_VERSION_SHFT                             0x5
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_APPS_PBL_PATCH_VERSION_BMSK                             0x1f
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW1_MSB_APPS_PBL_PATCH_VERSION_SHFT                              0x0

#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000050)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_PHYS                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00000050)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_ADDR, HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_RMSK)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_ADDR,m,v,HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_IN)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_TAP_GEN_SPARE_INSTR_DISABLE_31_0_BMSK             0xffffffff
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_LSB_TAP_GEN_SPARE_INSTR_DISABLE_31_0_SHFT                    0x0

#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000054)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_PHYS                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00000054)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_ADDR, HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_RMSK)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_ADDR,m,v,HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_IN)
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_STACKED_MEMORY_ID_BMSK                            0xf0000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_STACKED_MEMORY_ID_SHFT                                  0x1c
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_APSS_BOOT_TRIGGER_DISABLE_BMSK                     0x8000000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_APSS_BOOT_TRIGGER_DISABLE_SHFT                          0x1b
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_MODEM_PBL_PLL_CTRL_BMSK                            0x7800000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_MODEM_PBL_PLL_CTRL_SHFT                                 0x17
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_APPS_PBL_PLL_CTRL_BMSK                              0x780000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_APPS_PBL_PLL_CTRL_SHFT                                  0x13
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_APPS_BOOT_FSM_CFG_BMSK                               0x40000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_APPS_BOOT_FSM_CFG_SHFT                                  0x12
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_APPS_BOOT_FSM_DELAY_BMSK                             0x30000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_APPS_BOOT_FSM_DELAY_SHFT                                0x10
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_BOOT_ROM_PATCH_SEL_EN_USB_PHY_TUNE_BMSK               0x8000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_BOOT_ROM_PATCH_SEL_EN_USB_PHY_TUNE_SHFT                  0xf
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_USB_SS_ENABLE_BMSK                                    0x4000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_USB_SS_ENABLE_SHFT                                       0xe
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_APPS_PBL_BOOT_SPEED_BMSK                              0x3000
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_APPS_PBL_BOOT_SPEED_SHFT                                 0xc
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_APPS_BOOT_FROM_ROM_BMSK                                0x800
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_APPS_BOOT_FROM_ROM_SHFT                                  0xb
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_MSA_ENA_BMSK                                           0x400
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_MSA_ENA_SHFT                                             0xa
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_FORCE_MSA_AUTH_EN_BMSK                                 0x200
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_FORCE_MSA_AUTH_EN_SHFT                                   0x9
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_SPARE_8_7_BMSK                                         0x180
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_SPARE_8_7_SHFT                                           0x7
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_MODEM_BOOT_FROM_ROM_BMSK                                0x40
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_MODEM_BOOT_FROM_ROM_SHFT                                 0x6
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_BOOT_ROM_PATCH_DISABLE_BMSK                             0x20
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_BOOT_ROM_PATCH_DISABLE_SHFT                              0x5
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_TAP_GEN_SPARE_INSTR_DISABLE_36_32_BMSK                  0x1f
#define HWIO_QFPROM_RAW_FEAT_CONFIG_ROW2_MSB_TAP_GEN_SPARE_INSTR_DISABLE_36_32_SHFT                   0x0

#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n)                                (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000058 + 0x8 * (n))
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_PHYS(n)                                (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00000058 + 0x8 * (n))
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_RMSK                                   0xffffffff
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_MAXn                                            3
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n), HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_RMSK)
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n), mask)
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_OUTI(n,val)    \
        out_dword(HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n),val)
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n),mask,val,HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_INI(n))
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_KEY_DATA0_BMSK                         0xffffffff
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_LSB_KEY_DATA0_SHFT                                0x0

#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n)                                (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000005c + 0x8 * (n))
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_PHYS(n)                                (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x0000005c + 0x8 * (n))
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_RMSK                                   0xffffffff
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_MAXn                                            3
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n), HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_RMSK)
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n), mask)
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_OUTI(n,val)    \
        out_dword(HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n),val)
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n),mask,val,HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_INI(n))
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_KEY_DATA1_BMSK                         0xffffffff
#define HWIO_QFPROM_RAW_PRI_KEY_DERIVATION_KEY_ROWn_MSB_KEY_DATA1_SHFT                                0x0

#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n)                                (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000078 + 0x8 * (n))
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_PHYS(n)                                (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00000078 + 0x8 * (n))
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_RMSK                                   0xffffffff
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_MAXn                                            3
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n), HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_RMSK)
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n), mask)
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_OUTI(n,val)    \
        out_dword(HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n),val)
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n),mask,val,HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_INI(n))
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_KEY_DATA0_BMSK                         0xffffffff
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_LSB_KEY_DATA0_SHFT                                0x0

#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n)                                (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000007c + 0x8 * (n))
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_PHYS(n)                                (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x0000007c + 0x8 * (n))
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_RMSK                                   0xffffffff
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_MAXn                                            3
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n), HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_RMSK)
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n), mask)
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_OUTI(n,val)    \
        out_dword(HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n),val)
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n),mask,val,HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_INI(n))
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_KEY_DATA1_BMSK                         0xffffffff
#define HWIO_QFPROM_RAW_SEC_KEY_DERIVATION_KEY_ROWn_MSB_KEY_DATA1_SHFT                                0x0

#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000098)
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_PHYS                                             (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00000098)
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_ADDR, HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_RMSK)
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_ADDR,m,v,HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_IN)
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_SEC_BOOT4_BMSK                                   0xff000000
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_SEC_BOOT4_SHFT                                         0x18
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_SEC_BOOT3_BMSK                                     0xff0000
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_SEC_BOOT3_SHFT                                         0x10
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_SEC_BOOT2_BMSK                                       0xff00
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_SEC_BOOT2_SHFT                                          0x8
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_SEC_BOOT1_BMSK                                         0xff
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_LSB_SEC_BOOT1_SHFT                                          0x0

#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000009c)
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_PHYS                                             (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x0000009c)
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_ADDR, HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_RMSK)
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_ADDR,m,v,HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_IN)
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_SPARE0_BMSK                                      0x80000000
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_SPARE0_SHFT                                            0x1f
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_FEC_VALUE_BMSK                                   0x7f000000
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_FEC_VALUE_SHFT                                         0x18
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_SEC_BOOT7_BMSK                                     0xff0000
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_SEC_BOOT7_SHFT                                         0x10
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_SEC_BOOT6_BMSK                                       0xff00
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_SEC_BOOT6_SHFT                                          0x8
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_SEC_BOOT5_BMSK                                         0xff
#define HWIO_QFPROM_RAW_OEM_SEC_BOOT_ROW0_MSB_SEC_BOOT5_SHFT                                          0x0

#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_LSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x000000a0)
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_LSB_PHYS                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000000a0)
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_LSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_LSB_ADDR, HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_LSB_RMSK)
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_LSB_ADDR,m,v,HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_LSB_IN)
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_LSB_SEC_BOOT4_BMSK                                    0xff000000
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_LSB_SEC_BOOT4_SHFT                                          0x18
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_LSB_SEC_BOOT3_BMSK                                      0xff0000
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_LSB_SEC_BOOT3_SHFT                                          0x10
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_LSB_SEC_BOOT2_BMSK                                        0xff00
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_LSB_SEC_BOOT2_SHFT                                           0x8
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_LSB_SEC_BOOT1_BMSK                                          0xff
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_LSB_SEC_BOOT1_SHFT                                           0x0

#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_MSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x000000a4)
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_MSB_PHYS                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000000a4)
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_MSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_MSB_ADDR, HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_MSB_RMSK)
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_MSB_ADDR,m,v,HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_MSB_IN)
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_MSB_SPARE0_BMSK                                       0x80000000
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_MSB_SPARE0_SHFT                                             0x1f
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_MSB_FEC_VALUE_BMSK                                    0x7f000000
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_MSB_FEC_VALUE_SHFT                                          0x18
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_MSB_SEC_BOOT7_BMSK                                      0xff0000
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_MSB_SEC_BOOT7_SHFT                                          0x10
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_MSB_SEC_BOOT6_BMSK                                        0xff00
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_MSB_SEC_BOOT6_SHFT                                           0x8
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_MSB_SEC_BOOT5_BMSK                                          0xff
#define HWIO_QFPROM_RAW_QC_SEC_BOOT_ROW0_MSB_SEC_BOOT5_SHFT                                           0x0

#define HWIO_QFPROM_RAW_PK_HASH_ROWn_LSB_ADDR(n)                                               (SECURITY_CONTROL_CORE_REG_BASE      + 0x000000a8 + 0x8 * (n))
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_LSB_PHYS(n)                                               (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000000a8 + 0x8 * (n))
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_LSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_LSB_MAXn                                                           3
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_LSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_RAW_PK_HASH_ROWn_LSB_ADDR(n), HWIO_QFPROM_RAW_PK_HASH_ROWn_LSB_RMSK)
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_LSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_RAW_PK_HASH_ROWn_LSB_ADDR(n), mask)
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_LSB_OUTI(n,val)    \
        out_dword(HWIO_QFPROM_RAW_PK_HASH_ROWn_LSB_ADDR(n),val)
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_LSB_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_PK_HASH_ROWn_LSB_ADDR(n),mask,val,HWIO_QFPROM_RAW_PK_HASH_ROWn_LSB_INI(n))
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_LSB_HASH_DATA0_BMSK                                       0xffffffff
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_LSB_HASH_DATA0_SHFT                                              0x0

#define HWIO_QFPROM_RAW_PK_HASH_ROWn_MSB_ADDR(n)                                               (SECURITY_CONTROL_CORE_REG_BASE      + 0x000000ac + 0x8 * (n))
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_MSB_PHYS(n)                                               (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000000ac + 0x8 * (n))
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_MSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_MSB_MAXn                                                           3
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_MSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_RAW_PK_HASH_ROWn_MSB_ADDR(n), HWIO_QFPROM_RAW_PK_HASH_ROWn_MSB_RMSK)
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_MSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_RAW_PK_HASH_ROWn_MSB_ADDR(n), mask)
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_MSB_OUTI(n,val)    \
        out_dword(HWIO_QFPROM_RAW_PK_HASH_ROWn_MSB_ADDR(n),val)
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_MSB_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_PK_HASH_ROWn_MSB_ADDR(n),mask,val,HWIO_QFPROM_RAW_PK_HASH_ROWn_MSB_INI(n))
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_MSB_SPARE0_BMSK                                           0x80000000
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_MSB_SPARE0_SHFT                                                 0x1f
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_MSB_FEC_VALUE_BMSK                                        0x7f000000
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_MSB_FEC_VALUE_SHFT                                              0x18
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_MSB_HASH_DATA1_BMSK                                         0xffffff
#define HWIO_QFPROM_RAW_PK_HASH_ROWn_MSB_HASH_DATA1_SHFT                                              0x0

#define HWIO_QFPROM_RAW_PK_HASH_ROW4_LSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x000000c8)
#define HWIO_QFPROM_RAW_PK_HASH_ROW4_LSB_PHYS                                                  (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000000c8)
#define HWIO_QFPROM_RAW_PK_HASH_ROW4_LSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_RAW_PK_HASH_ROW4_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_PK_HASH_ROW4_LSB_ADDR, HWIO_QFPROM_RAW_PK_HASH_ROW4_LSB_RMSK)
#define HWIO_QFPROM_RAW_PK_HASH_ROW4_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_PK_HASH_ROW4_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_PK_HASH_ROW4_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_PK_HASH_ROW4_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_PK_HASH_ROW4_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_PK_HASH_ROW4_LSB_ADDR,m,v,HWIO_QFPROM_RAW_PK_HASH_ROW4_LSB_IN)
#define HWIO_QFPROM_RAW_PK_HASH_ROW4_LSB_HASH_DATA0_BMSK                                       0xffffffff
#define HWIO_QFPROM_RAW_PK_HASH_ROW4_LSB_HASH_DATA0_SHFT                                              0x0

#define HWIO_QFPROM_RAW_PK_HASH_ROW4_MSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x000000cc)
#define HWIO_QFPROM_RAW_PK_HASH_ROW4_MSB_PHYS                                                  (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000000cc)
#define HWIO_QFPROM_RAW_PK_HASH_ROW4_MSB_RMSK                                                  0xff000000
#define HWIO_QFPROM_RAW_PK_HASH_ROW4_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_PK_HASH_ROW4_MSB_ADDR, HWIO_QFPROM_RAW_PK_HASH_ROW4_MSB_RMSK)
#define HWIO_QFPROM_RAW_PK_HASH_ROW4_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_PK_HASH_ROW4_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_PK_HASH_ROW4_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_PK_HASH_ROW4_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_PK_HASH_ROW4_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_PK_HASH_ROW4_MSB_ADDR,m,v,HWIO_QFPROM_RAW_PK_HASH_ROW4_MSB_IN)
#define HWIO_QFPROM_RAW_PK_HASH_ROW4_MSB_SPARE0_BMSK                                           0x80000000
#define HWIO_QFPROM_RAW_PK_HASH_ROW4_MSB_SPARE0_SHFT                                                 0x1f
#define HWIO_QFPROM_RAW_PK_HASH_ROW4_MSB_FEC_VALUE_BMSK                                        0x7f000000
#define HWIO_QFPROM_RAW_PK_HASH_ROW4_MSB_FEC_VALUE_SHFT                                              0x18

#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_ADDR                                                    (SECURITY_CONTROL_CORE_REG_BASE      + 0x000000d0)
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_PHYS                                                    (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000000d0)
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_RMSK                                                    0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW0_LSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW0_LSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW0_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW0_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW0_LSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW0_LSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_TSENS2_POINT1_BMSK                                      0xf0000000
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_TSENS2_POINT1_SHFT                                            0x1c
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_TSENS1_POINT1_BMSK                                       0xf000000
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_TSENS1_POINT1_SHFT                                            0x18
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_TSENS0_POINT1_BMSK                                        0xf00000
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_TSENS0_POINT1_SHFT                                            0x14
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_TSENS_BASE1_BMSK                                           0xffc00
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_TSENS_BASE1_SHFT                                               0xa
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_TSENS_BASE0_BMSK                                             0x3ff
#define HWIO_QFPROM_RAW_CALIB_ROW0_LSB_TSENS_BASE0_SHFT                                               0x0

#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_ADDR                                                    (SECURITY_CONTROL_CORE_REG_BASE      + 0x000000d4)
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_PHYS                                                    (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000000d4)
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_RMSK                                                    0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW0_MSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW0_MSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW0_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW0_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW0_MSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW0_MSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_FEC_BMSK                                                0xff000000
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_FEC_SHFT                                                      0x18
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_USB_PHY_TUNING_BMSK                                       0xf00000
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_USB_PHY_TUNING_SHFT                                           0x14
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_SPARE_BMSK                                                 0xff800
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_SPARE_SHFT                                                     0xb
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_TSENS_CAL_SEL_BMSK                                           0x700
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_TSENS_CAL_SEL_SHFT                                             0x8
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_TSENS4_POINT1_BMSK                                            0xf0
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_TSENS4_POINT1_SHFT                                             0x4
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_TSENS3_POINT1_BMSK                                             0xf
#define HWIO_QFPROM_RAW_CALIB_ROW0_MSB_TSENS3_POINT1_SHFT                                             0x0

#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_ADDR                                                    (SECURITY_CONTROL_CORE_REG_BASE      + 0x000000d8)
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_PHYS                                                    (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000000d8)
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_RMSK                                                    0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW1_LSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW1_LSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW1_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW1_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW1_LSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW1_LSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_CPR0_TARG_VOLT_OFFSET_TUR_BIT_0_BMSK                    0x80000000
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_CPR0_TARG_VOLT_OFFSET_TUR_BIT_0_SHFT                          0x1f
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_CPR0_TARG_VOLT_SVS2_BMSK                                0x7c000000
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_CPR0_TARG_VOLT_SVS2_SHFT                                      0x1a
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_CPR0_TARG_VOLT_OFFSET_TUR_BIT_1_BMSK                     0x2000000
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_CPR0_TARG_VOLT_OFFSET_TUR_BIT_1_SHFT                          0x19
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_CPR0_TARG_VOLT_SVS_BMSK                                  0x1f00000
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_CPR0_TARG_VOLT_SVS_SHFT                                       0x14
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_CPR0_TARG_VOLT_OFFSET_TUR_BIT_2_BMSK                       0x80000
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_CPR0_TARG_VOLT_OFFSET_TUR_BIT_2_SHFT                          0x13
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_CPR0_TARG_VOLT_NOM_BMSK                                    0x7c000
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_CPR0_TARG_VOLT_NOM_SHFT                                        0xe
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_CPR0_TARG_VOLT_OFFSET_TUR_BIT_3_BMSK                        0x2000
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_CPR0_TARG_VOLT_OFFSET_TUR_BIT_3_SHFT                           0xd
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_CPR0_TARG_VOLT_TUR_BMSK                                     0x1f00
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_CPR0_TARG_VOLT_TUR_SHFT                                        0x8
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_CPR1_TARG_VOLT_OFFSET_SVS2_BMSK                               0x80
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_CPR1_TARG_VOLT_OFFSET_SVS2_SHFT                                0x7
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_CPR2_TARG_VOLT_OFFSET_NOM_BMSK                                0x78
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_CPR2_TARG_VOLT_OFFSET_NOM_SHFT                                 0x3
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_CPR2_TARG_VOLT_OFFSET_TUR_BMSK                                 0x7
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_CPR2_TARG_VOLT_OFFSET_TUR_SHFT                                 0x0

#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_ADDR                                                    (SECURITY_CONTROL_CORE_REG_BASE      + 0x000000dc)
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_PHYS                                                    (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000000dc)
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_RMSK                                                    0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW1_MSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW1_MSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW1_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW1_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW1_MSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW1_MSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_FEC_BMSK                                                0xff000000
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_FEC_SHFT                                                      0x18
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_CPR0_TARG_VOLT_OFFSET_NOM_BIT_0_BMSK                      0x800000
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_CPR0_TARG_VOLT_OFFSET_NOM_BIT_0_SHFT                          0x17
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_CPR1_TARG_VOLT_SVS2_BMSK                                  0x7c0000
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_CPR1_TARG_VOLT_SVS2_SHFT                                      0x12
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_CPR0_TARG_VOLT_OFFSET_NOM_BIT_1_BMSK                       0x20000
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_CPR0_TARG_VOLT_OFFSET_NOM_BIT_1_SHFT                          0x11
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_CPR1_TARG_VOLT_SVS_BMSK                                    0x1f000
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_CPR1_TARG_VOLT_SVS_SHFT                                        0xc
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_CPR0_TARG_VOLT_OFFSET_NOM_BIT_2_BMSK                         0x800
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_CPR0_TARG_VOLT_OFFSET_NOM_BIT_2_SHFT                           0xb
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_CPR1_TARG_VOLT_NOM_BMSK                                      0x7c0
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_CPR1_TARG_VOLT_NOM_SHFT                                        0x6
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_CPR0_TARG_VOLT_OFFSET_NOM_BIT_3_BMSK                          0x20
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_CPR0_TARG_VOLT_OFFSET_NOM_BIT_3_SHFT                           0x5
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_CPR1_TARG_VOLT_TUR_BMSK                                       0x1f
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_CPR1_TARG_VOLT_TUR_SHFT                                        0x0

#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_ADDR                                                    (SECURITY_CONTROL_CORE_REG_BASE      + 0x000000e0)
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_PHYS                                                    (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000000e0)
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_RMSK                                                    0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW2_LSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW2_LSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW2_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW2_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW2_LSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW2_LSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_LDO_TARGET1_BMSK                                        0xf0000000
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_LDO_TARGET1_SHFT                                              0x1c
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_LDO_TARGET0_BMSK                                         0xfc00000
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_LDO_TARGET0_SHFT                                              0x16
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_LDO_ENABLE_BMSK                                           0x200000
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_LDO_ENABLE_SHFT                                               0x15
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_CPR0_TARG_VOLT_OFFSET_SVS2_BMSK                           0x1e0000
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_CPR0_TARG_VOLT_OFFSET_SVS2_SHFT                               0x11
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_CPR0_TARG_VOLT_OFFSET_SVS_BMSK                             0x1c000
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_CPR0_TARG_VOLT_OFFSET_SVS_SHFT                                 0xe
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_CPR2_TARG_VOLT_NOM_BMSK                                     0x3e00
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_CPR2_TARG_VOLT_NOM_SHFT                                        0x9
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_CPR0_TARG_VOLT_OFFSET_SVS_BIT_0_BMSK                         0x100
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_CPR0_TARG_VOLT_OFFSET_SVS_BIT_0_SHFT                           0x8
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_CPR2_TARG_VOLT_TUR_BMSK                                       0xf8
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_CPR2_TARG_VOLT_TUR_SHFT                                        0x3
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_CPR_REV_BMSK                                                   0x7
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_CPR_REV_SHFT                                                   0x0

#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_ADDR                                                    (SECURITY_CONTROL_CORE_REG_BASE      + 0x000000e4)
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_PHYS                                                    (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000000e4)
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_RMSK                                                    0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW2_MSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW2_MSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW2_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW2_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW2_MSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW2_MSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_FEC_BMSK                                                0xff000000
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_FEC_SHFT                                                      0x18
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_CPR1_TARG_VOLT_OFFSET_SVS2_BMSK                           0xc00000
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_CPR1_TARG_VOLT_OFFSET_SVS2_SHFT                               0x16
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_CPR1_TARG_VOLT_OFFSET_SVS_BMSK                            0x3c0000
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_CPR1_TARG_VOLT_OFFSET_SVS_SHFT                                0x12
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_CPR1_TARG_VOLT_OFFSET_NOM_BMSK                             0x3c000
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_CPR1_TARG_VOLT_OFFSET_NOM_SHFT                                 0xe
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_CPR1_TARG_VOLT_OFFSET_TUR_BMSK                              0x3c00
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_CPR1_TARG_VOLT_OFFSET_TUR_SHFT                                 0xa
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_SPARE_BMSK                                                   0x300
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_SPARE_SHFT                                                     0x8
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_GNSS_ADC_CH0_VCM_BMSK                                         0xc0
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_GNSS_ADC_CH0_VCM_SHFT                                          0x6
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_GNSS_ADC_CH0_LDO_BMSK                                         0x30
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_GNSS_ADC_CH0_LDO_SHFT                                          0x4
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_GNSS_ADC_CH0_VREF_BMSK                                         0xc
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_GNSS_ADC_CH0_VREF_SHFT                                         0x2
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_LDO_TARGET1_BMSK                                               0x3
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_LDO_TARGET1_SHFT                                               0x0

#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_ADDR                                                    (SECURITY_CONTROL_CORE_REG_BASE      + 0x000000e8)
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_PHYS                                                    (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000000e8)
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_RMSK                                                    0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW3_LSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW3_LSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW3_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW3_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW3_LSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW3_LSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_ECS_BHS_SLOPE_BMSK                                      0x80000000
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_ECS_BHS_SLOPE_SHFT                                            0x1f
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_ECS_BHS_INTERCEPT_BMSK                                  0x7fe00000
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_ECS_BHS_INTERCEPT_SHFT                                        0x15
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_ECS_LDO_SLOPE_BMSK                                        0x1ff800
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_ECS_LDO_SLOPE_SHFT                                             0xb
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_ECS_LDO_INTERCEPT_BMSK                                       0x7fe
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_ECS_LDO_INTERCEPT_SHFT                                         0x1
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_ECS_ENABLE_BMSK                                                0x1
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_ECS_ENABLE_SHFT                                                0x0

#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_ADDR                                                    (SECURITY_CONTROL_CORE_REG_BASE      + 0x000000ec)
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_PHYS                                                    (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000000ec)
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_RMSK                                                    0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW3_MSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW3_MSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW3_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW3_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW3_MSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW3_MSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_FEC_BMSK                                                0xff000000
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_FEC_SHFT                                                      0x18
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_G_B1_BMSK                                                 0xe00000
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_G_B1_SHFT                                                     0x15
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_SAR_LDO_ERR0_BMSK                                         0x180000
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_SAR_LDO_ERR0_SHFT                                             0x13
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_CPR2_TARG_VOLT_OFFSET_TUR_BIT_3_BMSK                       0x40000
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_CPR2_TARG_VOLT_OFFSET_TUR_BIT_3_SHFT                          0x12
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_CPR1_TARG_VOLT_OFFSET_SVS2_BIT_3_BMSK                      0x20000
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_CPR1_TARG_VOLT_OFFSET_SVS2_BIT_3_SHFT                         0x11
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_ECS_MISC_BMSK                                              0x1fe00
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_ECS_MISC_SHFT                                                  0x9
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_ECS_BHS_SLOPE_BMSK                                           0x1ff
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_ECS_BHS_SLOPE_SHFT                                             0x0

#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_ADDR                                                    (SECURITY_CONTROL_CORE_REG_BASE      + 0x000000f0)
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_PHYS                                                    (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000000f0)
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_RMSK                                                    0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW4_LSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW4_LSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW4_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW4_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW4_LSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW4_LSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_PH_B1M2_BMSK                                            0xc0000000
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_PH_B1M2_SHFT                                                  0x1e
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_PH_B1M1_BMSK                                            0x38000000
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_PH_B1M1_SHFT                                                  0x1b
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_PH_B1M0_BMSK                                             0x7000000
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_PH_B1M0_SHFT                                                  0x18
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_Q6SS0_LDO_VREF_TRIM_BMSK                                  0xf80000
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_Q6SS0_LDO_VREF_TRIM_SHFT                                      0x13
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_CLK_LDO_ERR0_BMSK                                          0x60000
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_CLK_LDO_ERR0_SHFT                                             0x11
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_VREF_ERR_B0_BMSK                                           0x18000
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_VREF_ERR_B0_SHFT                                               0xf
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_PH_B0M3_BMSK                                                0x7000
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_PH_B0M3_SHFT                                                   0xc
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_PH_B0M2_BMSK                                                 0xe00
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_PH_B0M2_SHFT                                                   0x9
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_PH_B0M1_BMSK                                                 0x1c0
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_PH_B0M1_SHFT                                                   0x6
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_PH_B0M0_BMSK                                                  0x38
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_PH_B0M0_SHFT                                                   0x3
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_G_B0_BMSK                                                      0x7
#define HWIO_QFPROM_RAW_CALIB_ROW4_LSB_G_B0_SHFT                                                      0x0

#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_ADDR                                                    (SECURITY_CONTROL_CORE_REG_BASE      + 0x000000f4)
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_PHYS                                                    (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000000f4)
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_RMSK                                                    0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW4_MSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW4_MSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW4_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW4_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW4_MSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW4_MSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_FEC_BMSK                                                0xff000000
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_FEC_SHFT                                                      0x18
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_PH_B2M3_BMSK                                              0xc00000
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_PH_B2M3_SHFT                                                  0x16
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_PH_B2M2_BMSK                                              0x380000
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_PH_B2M2_SHFT                                                  0x13
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_PH_B2M1_BMSK                                               0x70000
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_PH_B2M1_SHFT                                                  0x10
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_PH_B2M0_BMSK                                                0xe000
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_PH_B2M0_SHFT                                                   0xd
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_G_B2_BMSK                                                   0x1c00
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_G_B2_SHFT                                                      0xa
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_SAR_LDO_ERR1_BMSK                                            0x300
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_SAR_LDO_ERR1_SHFT                                              0x8
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_CLK_LDO_ERR1_BMSK                                             0xc0
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_CLK_LDO_ERR1_SHFT                                              0x6
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_VREF_ERR_B1_BMSK                                              0x30
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_VREF_ERR_B1_SHFT                                               0x4
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_PH_B1M3_BMSK                                                   0xe
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_PH_B1M3_SHFT                                                   0x1
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_PH_B1M2_BMSK                                                   0x1
#define HWIO_QFPROM_RAW_CALIB_ROW4_MSB_PH_B1M2_SHFT                                                   0x0

#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_ADDR                                                    (SECURITY_CONTROL_CORE_REG_BASE      + 0x000000f8)
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_PHYS                                                    (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000000f8)
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_RMSK                                                    0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW5_LSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW5_LSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW5_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW5_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW5_LSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW5_LSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_PH_B4M0_BMSK                                            0x80000000
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_PH_B4M0_SHFT                                                  0x1f
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_G_B4_BMSK                                               0x70000000
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_G_B4_SHFT                                                     0x1c
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_SAR_LDO_ERR3_BMSK                                        0xc000000
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_SAR_LDO_ERR3_SHFT                                             0x1a
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_CLK_LDO_ERR3_BMSK                                        0x3000000
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_CLK_LDO_ERR3_SHFT                                             0x18
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_VREF_ERR_B3_BMSK                                          0xc00000
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_VREF_ERR_B3_SHFT                                              0x16
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_PH_B3M3_BMSK                                              0x380000
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_PH_B3M3_SHFT                                                  0x13
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_PH_B3M2_BMSK                                               0x70000
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_PH_B3M2_SHFT                                                  0x10
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_PH_B3M1_BMSK                                                0xe000
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_PH_B3M1_SHFT                                                   0xd
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_PH_B3M0_BMSK                                                0x1c00
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_PH_B3M0_SHFT                                                   0xa
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_G_B3_BMSK                                                    0x380
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_G_B3_SHFT                                                      0x7
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_SAR_LDO_ERR2_BMSK                                             0x60
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_SAR_LDO_ERR2_SHFT                                              0x5
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_CLK_LDO_ERR2_BMSK                                             0x18
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_CLK_LDO_ERR2_SHFT                                              0x3
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_VREF_ERR_B2_BMSK                                               0x6
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_VREF_ERR_B2_SHFT                                               0x1
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_PH_B2M3_BMSK                                                   0x1
#define HWIO_QFPROM_RAW_CALIB_ROW5_LSB_PH_B2M3_SHFT                                                   0x0

#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_ADDR                                                    (SECURITY_CONTROL_CORE_REG_BASE      + 0x000000fc)
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_PHYS                                                    (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000000fc)
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_RMSK                                                    0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW5_MSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW5_MSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW5_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW5_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW5_MSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW5_MSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_FEC_BMSK                                                0xff000000
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_FEC_SHFT                                                      0x18
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_PH_B5M1_BMSK                                              0x800000
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_PH_B5M1_SHFT                                                  0x17
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_PH_B5M0_BMSK                                              0x700000
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_PH_B5M0_SHFT                                                  0x14
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_G_B5_BMSK                                                  0xe0000
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_G_B5_SHFT                                                     0x11
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_SAR_LDO_ERR4_BMSK                                          0x18000
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_SAR_LDO_ERR4_SHFT                                              0xf
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_CLK_LDO_ERR4_BMSK                                           0x6000
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_CLK_LDO_ERR4_SHFT                                              0xd
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_VREF_ERR_B4_BMSK                                            0x1800
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_VREF_ERR_B4_SHFT                                               0xb
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_PH_B4M3_BMSK                                                 0x700
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_PH_B4M3_SHFT                                                   0x8
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_PH_B4M2_BMSK                                                  0xe0
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_PH_B4M2_SHFT                                                   0x5
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_PH_B4M1_BMSK                                                  0x1c
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_PH_B4M1_SHFT                                                   0x2
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_PH_B4M0_BMSK                                                   0x3
#define HWIO_QFPROM_RAW_CALIB_ROW5_MSB_PH_B4M0_SHFT                                                   0x0

#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_ADDR                                                    (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000100)
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_PHYS                                                    (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00000100)
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_RMSK                                                    0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW6_LSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW6_LSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW6_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW6_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW6_LSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW6_LSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_CLK_LDO_ERR6_BMSK                                       0x80000000
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_CLK_LDO_ERR6_SHFT                                             0x1f
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_VREF_ERR_B6_BMSK                                        0x60000000
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_VREF_ERR_B6_SHFT                                              0x1d
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_PH_B6M3_BMSK                                            0x1c000000
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_PH_B6M3_SHFT                                                  0x1a
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_PH_B6M2_BMSK                                             0x3800000
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_PH_B6M2_SHFT                                                  0x17
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_PH_B6M1_BMSK                                              0x700000
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_PH_B6M1_SHFT                                                  0x14
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_PH_B6M0_BMSK                                               0xe0000
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_PH_B6M0_SHFT                                                  0x11
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_G_B6_BMSK                                                  0x1c000
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_G_B6_SHFT                                                      0xe
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_SAR_LDO_ERR5_BMSK                                           0x3000
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_SAR_LDO_ERR5_SHFT                                              0xc
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_CLK_LDO_ERR5_BMSK                                            0xc00
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_CLK_LDO_ERR5_SHFT                                              0xa
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_VREF_ERR_B5_BMSK                                             0x300
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_VREF_ERR_B5_SHFT                                               0x8
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_PH_B5M3_BMSK                                                  0xe0
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_PH_B5M3_SHFT                                                   0x5
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_PH_B5M2_BMSK                                                  0x1c
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_PH_B5M2_SHFT                                                   0x2
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_PH_B5M1_BMSK                                                   0x3
#define HWIO_QFPROM_RAW_CALIB_ROW6_LSB_PH_B5M1_SHFT                                                   0x0

#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_ADDR                                                    (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000104)
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_PHYS                                                    (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00000104)
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_RMSK                                                    0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW6_MSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW6_MSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW6_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW6_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW6_MSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW6_MSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_FEC_BMSK                                                0xff000000
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_FEC_SHFT                                                      0x18
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_MODEM_TXDAC1_CALIB_BMSK                                   0xff0000
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_MODEM_TXDAC1_CALIB_SHFT                                       0x10
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_MODEM_TXDAC0_CALIB_BMSK                                     0xff00
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_MODEM_TXDAC0_CALIB_SHFT                                        0x8
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_MODEM_TXDAC1_CALIB_AVG_ERROR_BMSK                             0x80
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_MODEM_TXDAC1_CALIB_AVG_ERROR_SHFT                              0x7
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_MODEM_TXDAC0_CALIB_AVG_ERROR_BMSK                             0x40
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_MODEM_TXDAC0_CALIB_AVG_ERROR_SHFT                              0x6
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_MODEM_TXDAC_0_1_FUSEFLAG_BMSK                                 0x20
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_MODEM_TXDAC_0_1_FUSEFLAG_SHFT                                  0x5
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_MODEM_TXDAC1_CALIB_OVERFLOW_BMSK                              0x10
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_MODEM_TXDAC1_CALIB_OVERFLOW_SHFT                               0x4
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_MODEM_TXDAC0_CALIB_OVERFLOW_BMSK                               0x8
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_MODEM_TXDAC0_CALIB_OVERFLOW_SHFT                               0x3
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_SAR_LDO_ERR6_BMSK                                              0x6
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_SAR_LDO_ERR6_SHFT                                              0x1
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_CLK_LDO_ERR6_BMSK                                              0x1
#define HWIO_QFPROM_RAW_CALIB_ROW6_MSB_CLK_LDO_ERR6_SHFT                                              0x0

#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_ADDR(n)                                            (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000108 + 0x8 * (n))
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_PHYS(n)                                            (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00000108 + 0x8 * (n))
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_RMSK                                               0xffffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_MAXn                                                       15
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_ADDR(n), HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_RMSK)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_ADDR(n), mask)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_OUTI(n,val)    \
        out_dword(HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_ADDR(n),val)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_ADDR(n),mask,val,HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_INI(n))
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_REDUN_DATA_BMSK                                    0xffffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_LSB_REDUN_DATA_SHFT                                           0x0

#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_ADDR(n)                                            (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000010c + 0x8 * (n))
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_PHYS(n)                                            (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x0000010c + 0x8 * (n))
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_RMSK                                               0xffffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_MAXn                                                       15
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_ADDR(n), HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_RMSK)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_ADDR(n), mask)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_OUTI(n,val)    \
        out_dword(HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_ADDR(n),val)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_ADDR(n),mask,val,HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_INI(n))
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_SPARE0_BMSK                                        0x80000000
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_SPARE0_SHFT                                              0x1f
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_FEC_VALUE_BMSK                                     0x7f000000
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_FEC_VALUE_SHFT                                           0x18
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_REDUN_DATA_BMSK                                      0xffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROWn_MSB_REDUN_DATA_SHFT                                           0x0

#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_LSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000188)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_LSB_PHYS                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00000188)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_LSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_LSB_ADDR, HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_LSB_RMSK)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_LSB_ADDR,m,v,HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_LSB_IN)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_LSB_MEM_ACCEL_COMPILER_BMSK                           0xffffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_LSB_MEM_ACCEL_COMPILER_SHFT                                  0x0

#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_MSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000018c)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_MSB_PHYS                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x0000018c)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_MSB_RMSK                                              0x7fffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_MSB_ADDR, HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_MSB_RMSK)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_MSB_ADDR,m,v,HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_MSB_IN)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_MSB_FEC_VALUE_BMSK                                    0x7f000000
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_MSB_FEC_VALUE_SHFT                                          0x18
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_MSB_SPARE0_BMSK                                         0xffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW16_MSB_SPARE0_SHFT                                              0x0

#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_LSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000190)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_LSB_PHYS                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00000190)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_LSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_LSB_ADDR, HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_LSB_RMSK)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_LSB_ADDR,m,v,HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_LSB_IN)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_LSB_MEM_ACCEL_CUSTOM_BMSK                             0xffffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_LSB_MEM_ACCEL_CUSTOM_SHFT                                    0x0

#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_MSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000194)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_MSB_PHYS                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00000194)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_MSB_RMSK                                              0x7fffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_MSB_ADDR, HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_MSB_RMSK)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_MSB_ADDR,m,v,HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_MSB_IN)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_MSB_FEC_VALUE_BMSK                                    0x7f000000
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_MSB_FEC_VALUE_SHFT                                          0x18
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_MSB_SPARE0_BMSK                                         0xffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW17_MSB_SPARE0_SHFT                                              0x0

#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_LSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000198)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_LSB_PHYS                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00000198)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_LSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_LSB_ADDR, HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_LSB_RMSK)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_LSB_ADDR,m,v,HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_LSB_IN)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_LSB_MEM_ACCEL_CUSTOM_BMSK                             0xffffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_LSB_MEM_ACCEL_CUSTOM_SHFT                                    0x0

#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_MSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000019c)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_MSB_PHYS                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x0000019c)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_MSB_RMSK                                              0x7fffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_MSB_ADDR, HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_MSB_RMSK)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_MSB_ADDR,m,v,HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_MSB_IN)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_MSB_FEC_VALUE_BMSK                                    0x7f000000
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_MSB_FEC_VALUE_SHFT                                          0x18
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_MSB_SPARE0_BMSK                                         0xffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW18_MSB_SPARE0_SHFT                                              0x0

#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_LSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x000001a0)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_LSB_PHYS                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000001a0)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_LSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_LSB_ADDR, HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_LSB_RMSK)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_LSB_ADDR,m,v,HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_LSB_IN)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_LSB_MEM_ACCEL_COMPILER_BMSK                           0xffffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_LSB_MEM_ACCEL_COMPILER_SHFT                                  0x0

#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_MSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x000001a4)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_MSB_PHYS                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000001a4)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_MSB_RMSK                                              0x7fffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_MSB_ADDR, HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_MSB_RMSK)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_MSB_ADDR,m,v,HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_MSB_IN)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_MSB_FEC_VALUE_BMSK                                    0x7f000000
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_MSB_FEC_VALUE_SHFT                                          0x18
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_MSB_SPARE0_BMSK                                         0xffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW19_MSB_SPARE0_SHFT                                              0x0

#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_LSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x000001a8)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_LSB_PHYS                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000001a8)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_LSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_LSB_ADDR, HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_LSB_RMSK)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_LSB_ADDR,m,v,HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_LSB_IN)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_LSB_MEM_ACCEL_CUSTOM_BMSK                             0xffffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_LSB_MEM_ACCEL_CUSTOM_SHFT                                    0x0

#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_MSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x000001ac)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_MSB_PHYS                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000001ac)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_MSB_RMSK                                              0x7fffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_MSB_ADDR, HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_MSB_RMSK)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_MSB_ADDR,m,v,HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_MSB_IN)
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_MSB_FEC_VALUE_BMSK                                    0x7f000000
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_MSB_FEC_VALUE_SHFT                                          0x18
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_MSB_SPARE0_BMSK                                         0xffffff
#define HWIO_QFPROM_RAW_MEM_CONFIG_ROW20_MSB_SPARE0_SHFT                                              0x0

#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_ADDR(n)                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x000001b0 + 0x8 * (n))
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_PHYS(n)                                             (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000001b0 + 0x8 * (n))
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_RMSK                                                0xffffffff
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_MAXn                                                        31
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_ADDR(n), HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_RMSK)
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_ADDR(n), mask)
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_OUTI(n,val)    \
        out_dword(HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_ADDR(n),val)
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_ADDR(n),mask,val,HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_INI(n))
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_PATCH_DATA_BMSK                                     0xffffffff
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_LSB_PATCH_DATA_SHFT                                            0x0

#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_ADDR(n)                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x000001b4 + 0x8 * (n))
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_PHYS(n)                                             (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000001b4 + 0x8 * (n))
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_RMSK                                                0xffffffff
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_MAXn                                                        31
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_ADDR(n), HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_RMSK)
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_ADDR(n), mask)
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_OUTI(n,val)    \
        out_dword(HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_ADDR(n),val)
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_ADDR(n),mask,val,HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_INI(n))
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_SPARE0_BMSK                                         0x80000000
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_SPARE0_SHFT                                               0x1f
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_FEC_VALUE_BMSK                                      0x7f000000
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_FEC_VALUE_SHFT                                            0x18
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_SPARE3_BMSK                                           0xfe0000
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_SPARE3_SHFT                                               0x11
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_PATCH_ADDR_BMSK                                        0x1fffe
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_PATCH_ADDR_SHFT                                            0x1
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_PATCH_EN_BMSK                                              0x1
#define HWIO_QFPROM_RAW_ROM_PATCH_ROWn_MSB_PATCH_EN_SHFT                                              0x0

#define HWIO_QFPROM_RAW_FEC_EN_LSB_ADDR                                                        (SECURITY_CONTROL_CORE_REG_BASE      + 0x000002b0)
#define HWIO_QFPROM_RAW_FEC_EN_LSB_PHYS                                                        (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000002b0)
#define HWIO_QFPROM_RAW_FEC_EN_LSB_RMSK                                                        0xffffffff
#define HWIO_QFPROM_RAW_FEC_EN_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_FEC_EN_LSB_ADDR, HWIO_QFPROM_RAW_FEC_EN_LSB_RMSK)
#define HWIO_QFPROM_RAW_FEC_EN_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_FEC_EN_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_FEC_EN_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_FEC_EN_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_FEC_EN_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_FEC_EN_LSB_ADDR,m,v,HWIO_QFPROM_RAW_FEC_EN_LSB_IN)
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION31_FEC_EN_BMSK                                        0x80000000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION31_FEC_EN_SHFT                                              0x1f
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION30_FEC_EN_BMSK                                        0x40000000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION30_FEC_EN_SHFT                                              0x1e
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION29_FEC_EN_BMSK                                        0x20000000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION29_FEC_EN_SHFT                                              0x1d
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION28_FEC_EN_BMSK                                        0x10000000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION28_FEC_EN_SHFT                                              0x1c
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION27_FEC_EN_BMSK                                         0x8000000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION27_FEC_EN_SHFT                                              0x1b
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION26_FEC_EN_BMSK                                         0x4000000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION26_FEC_EN_SHFT                                              0x1a
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION25_FEC_EN_BMSK                                         0x2000000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION25_FEC_EN_SHFT                                              0x19
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION24_FEC_EN_BMSK                                         0x1000000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION24_FEC_EN_SHFT                                              0x18
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION23_FEC_EN_BMSK                                          0x800000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION23_FEC_EN_SHFT                                              0x17
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION22_FEC_EN_BMSK                                          0x400000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION22_FEC_EN_SHFT                                              0x16
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION21_FEC_EN_BMSK                                          0x200000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION21_FEC_EN_SHFT                                              0x15
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION20_FEC_EN_BMSK                                          0x100000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION20_FEC_EN_SHFT                                              0x14
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION19_FEC_EN_BMSK                                           0x80000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION19_FEC_EN_SHFT                                              0x13
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION18_FEC_EN_BMSK                                           0x40000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION18_FEC_EN_SHFT                                              0x12
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION17_FEC_EN_BMSK                                           0x20000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION17_FEC_EN_SHFT                                              0x11
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION16_FEC_EN_BMSK                                           0x10000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION16_FEC_EN_SHFT                                              0x10
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION15_FEC_EN_BMSK                                            0x8000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION15_FEC_EN_SHFT                                               0xf
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION14_FEC_EN_BMSK                                            0x4000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION14_FEC_EN_SHFT                                               0xe
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION13_FEC_EN_BMSK                                            0x2000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION13_FEC_EN_SHFT                                               0xd
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION12_FEC_EN_BMSK                                            0x1000
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION12_FEC_EN_SHFT                                               0xc
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION11_FEC_EN_BMSK                                             0x800
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION11_FEC_EN_SHFT                                               0xb
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION10_FEC_EN_BMSK                                             0x400
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION10_FEC_EN_SHFT                                               0xa
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION9_FEC_EN_BMSK                                              0x200
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION9_FEC_EN_SHFT                                                0x9
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION8_FEC_EN_BMSK                                              0x100
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION8_FEC_EN_SHFT                                                0x8
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION7_FEC_EN_BMSK                                               0x80
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION7_FEC_EN_SHFT                                                0x7
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION6_FEC_EN_BMSK                                               0x40
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION6_FEC_EN_SHFT                                                0x6
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION5_FEC_EN_BMSK                                               0x20
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION5_FEC_EN_SHFT                                                0x5
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION4_FEC_EN_BMSK                                               0x10
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION4_FEC_EN_SHFT                                                0x4
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION3_FEC_EN_BMSK                                                0x8
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION3_FEC_EN_SHFT                                                0x3
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION2_FEC_EN_BMSK                                                0x4
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION2_FEC_EN_SHFT                                                0x2
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION1_FEC_EN_BMSK                                                0x2
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION1_FEC_EN_SHFT                                                0x1
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION0_FEC_EN_BMSK                                                0x1
#define HWIO_QFPROM_RAW_FEC_EN_LSB_REGION0_FEC_EN_SHFT                                                0x0

#define HWIO_QFPROM_RAW_FEC_EN_MSB_ADDR                                                        (SECURITY_CONTROL_CORE_REG_BASE      + 0x000002b4)
#define HWIO_QFPROM_RAW_FEC_EN_MSB_PHYS                                                        (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000002b4)
#define HWIO_QFPROM_RAW_FEC_EN_MSB_RMSK                                                        0xffffffff
#define HWIO_QFPROM_RAW_FEC_EN_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_FEC_EN_MSB_ADDR, HWIO_QFPROM_RAW_FEC_EN_MSB_RMSK)
#define HWIO_QFPROM_RAW_FEC_EN_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_FEC_EN_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_FEC_EN_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_FEC_EN_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_FEC_EN_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_FEC_EN_MSB_ADDR,m,v,HWIO_QFPROM_RAW_FEC_EN_MSB_IN)
#define HWIO_QFPROM_RAW_FEC_EN_MSB_FEC_EN_REDUNDANCY_BMSK                                      0xffffffff
#define HWIO_QFPROM_RAW_FEC_EN_MSB_FEC_EN_REDUNDANCY_SHFT                                             0x0

#define HWIO_QFPROM_RAW_SPARE_REG16_LSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000002b8)
#define HWIO_QFPROM_RAW_SPARE_REG16_LSB_PHYS                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000002b8)
#define HWIO_QFPROM_RAW_SPARE_REG16_LSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG16_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG16_LSB_ADDR, HWIO_QFPROM_RAW_SPARE_REG16_LSB_RMSK)
#define HWIO_QFPROM_RAW_SPARE_REG16_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG16_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_SPARE_REG16_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_SPARE_REG16_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_SPARE_REG16_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_SPARE_REG16_LSB_ADDR,m,v,HWIO_QFPROM_RAW_SPARE_REG16_LSB_IN)
#define HWIO_QFPROM_RAW_SPARE_REG16_LSB_SPARE0_BMSK                                            0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG16_LSB_SPARE0_SHFT                                                   0x0

#define HWIO_QFPROM_RAW_SPARE_REG16_MSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000002bc)
#define HWIO_QFPROM_RAW_SPARE_REG16_MSB_PHYS                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000002bc)
#define HWIO_QFPROM_RAW_SPARE_REG16_MSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG16_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG16_MSB_ADDR, HWIO_QFPROM_RAW_SPARE_REG16_MSB_RMSK)
#define HWIO_QFPROM_RAW_SPARE_REG16_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG16_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_SPARE_REG16_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_SPARE_REG16_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_SPARE_REG16_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_SPARE_REG16_MSB_ADDR,m,v,HWIO_QFPROM_RAW_SPARE_REG16_MSB_IN)
#define HWIO_QFPROM_RAW_SPARE_REG16_MSB_SPARE0_BMSK                                            0x80000000
#define HWIO_QFPROM_RAW_SPARE_REG16_MSB_SPARE0_SHFT                                                  0x1f
#define HWIO_QFPROM_RAW_SPARE_REG16_MSB_FEC_VALUE_BMSK                                         0x7f000000
#define HWIO_QFPROM_RAW_SPARE_REG16_MSB_FEC_VALUE_SHFT                                               0x18
#define HWIO_QFPROM_RAW_SPARE_REG16_MSB_SPARE1_BMSK                                              0xffffff
#define HWIO_QFPROM_RAW_SPARE_REG16_MSB_SPARE1_SHFT                                                   0x0

#define HWIO_QFPROM_RAW_SPARE_REG17_LSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000002c0)
#define HWIO_QFPROM_RAW_SPARE_REG17_LSB_PHYS                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000002c0)
#define HWIO_QFPROM_RAW_SPARE_REG17_LSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG17_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG17_LSB_ADDR, HWIO_QFPROM_RAW_SPARE_REG17_LSB_RMSK)
#define HWIO_QFPROM_RAW_SPARE_REG17_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG17_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_SPARE_REG17_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_SPARE_REG17_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_SPARE_REG17_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_SPARE_REG17_LSB_ADDR,m,v,HWIO_QFPROM_RAW_SPARE_REG17_LSB_IN)
#define HWIO_QFPROM_RAW_SPARE_REG17_LSB_SPARE0_BMSK                                            0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG17_LSB_SPARE0_SHFT                                                   0x0

#define HWIO_QFPROM_RAW_SPARE_REG17_MSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000002c4)
#define HWIO_QFPROM_RAW_SPARE_REG17_MSB_PHYS                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000002c4)
#define HWIO_QFPROM_RAW_SPARE_REG17_MSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG17_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG17_MSB_ADDR, HWIO_QFPROM_RAW_SPARE_REG17_MSB_RMSK)
#define HWIO_QFPROM_RAW_SPARE_REG17_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG17_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_SPARE_REG17_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_SPARE_REG17_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_SPARE_REG17_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_SPARE_REG17_MSB_ADDR,m,v,HWIO_QFPROM_RAW_SPARE_REG17_MSB_IN)
#define HWIO_QFPROM_RAW_SPARE_REG17_MSB_SPARE0_BMSK                                            0x80000000
#define HWIO_QFPROM_RAW_SPARE_REG17_MSB_SPARE0_SHFT                                                  0x1f
#define HWIO_QFPROM_RAW_SPARE_REG17_MSB_FEC_VALUE_BMSK                                         0x7f000000
#define HWIO_QFPROM_RAW_SPARE_REG17_MSB_FEC_VALUE_SHFT                                               0x18
#define HWIO_QFPROM_RAW_SPARE_REG17_MSB_SPARE1_BMSK                                              0xffffff
#define HWIO_QFPROM_RAW_SPARE_REG17_MSB_SPARE1_SHFT                                                   0x0

#define HWIO_QFPROM_RAW_SPARE_REG18_LSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000002c8)
#define HWIO_QFPROM_RAW_SPARE_REG18_LSB_PHYS                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000002c8)
#define HWIO_QFPROM_RAW_SPARE_REG18_LSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG18_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG18_LSB_ADDR, HWIO_QFPROM_RAW_SPARE_REG18_LSB_RMSK)
#define HWIO_QFPROM_RAW_SPARE_REG18_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG18_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_SPARE_REG18_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_SPARE_REG18_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_SPARE_REG18_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_SPARE_REG18_LSB_ADDR,m,v,HWIO_QFPROM_RAW_SPARE_REG18_LSB_IN)
#define HWIO_QFPROM_RAW_SPARE_REG18_LSB_SPARE0_BMSK                                            0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG18_LSB_SPARE0_SHFT                                                   0x0

#define HWIO_QFPROM_RAW_SPARE_REG18_MSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000002cc)
#define HWIO_QFPROM_RAW_SPARE_REG18_MSB_PHYS                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000002cc)
#define HWIO_QFPROM_RAW_SPARE_REG18_MSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG18_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG18_MSB_ADDR, HWIO_QFPROM_RAW_SPARE_REG18_MSB_RMSK)
#define HWIO_QFPROM_RAW_SPARE_REG18_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG18_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_SPARE_REG18_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_SPARE_REG18_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_SPARE_REG18_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_SPARE_REG18_MSB_ADDR,m,v,HWIO_QFPROM_RAW_SPARE_REG18_MSB_IN)
#define HWIO_QFPROM_RAW_SPARE_REG18_MSB_SPARE0_BMSK                                            0x80000000
#define HWIO_QFPROM_RAW_SPARE_REG18_MSB_SPARE0_SHFT                                                  0x1f
#define HWIO_QFPROM_RAW_SPARE_REG18_MSB_FEC_VALUE_BMSK                                         0x7f000000
#define HWIO_QFPROM_RAW_SPARE_REG18_MSB_FEC_VALUE_SHFT                                               0x18
#define HWIO_QFPROM_RAW_SPARE_REG18_MSB_SPARE1_BMSK                                              0xffffff
#define HWIO_QFPROM_RAW_SPARE_REG18_MSB_SPARE1_SHFT                                                   0x0

#define HWIO_QFPROM_RAW_SPARE_REG19_LSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000002d0)
#define HWIO_QFPROM_RAW_SPARE_REG19_LSB_PHYS                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000002d0)
#define HWIO_QFPROM_RAW_SPARE_REG19_LSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG19_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG19_LSB_ADDR, HWIO_QFPROM_RAW_SPARE_REG19_LSB_RMSK)
#define HWIO_QFPROM_RAW_SPARE_REG19_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG19_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_SPARE_REG19_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_SPARE_REG19_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_SPARE_REG19_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_SPARE_REG19_LSB_ADDR,m,v,HWIO_QFPROM_RAW_SPARE_REG19_LSB_IN)
#define HWIO_QFPROM_RAW_SPARE_REG19_LSB_SPARE0_BMSK                                            0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG19_LSB_SPARE0_SHFT                                                   0x0

#define HWIO_QFPROM_RAW_SPARE_REG19_MSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000002d4)
#define HWIO_QFPROM_RAW_SPARE_REG19_MSB_PHYS                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000002d4)
#define HWIO_QFPROM_RAW_SPARE_REG19_MSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG19_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG19_MSB_ADDR, HWIO_QFPROM_RAW_SPARE_REG19_MSB_RMSK)
#define HWIO_QFPROM_RAW_SPARE_REG19_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG19_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_SPARE_REG19_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_SPARE_REG19_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_SPARE_REG19_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_SPARE_REG19_MSB_ADDR,m,v,HWIO_QFPROM_RAW_SPARE_REG19_MSB_IN)
#define HWIO_QFPROM_RAW_SPARE_REG19_MSB_SPARE0_BMSK                                            0x80000000
#define HWIO_QFPROM_RAW_SPARE_REG19_MSB_SPARE0_SHFT                                                  0x1f
#define HWIO_QFPROM_RAW_SPARE_REG19_MSB_FEC_VALUE_BMSK                                         0x7f000000
#define HWIO_QFPROM_RAW_SPARE_REG19_MSB_FEC_VALUE_SHFT                                               0x18
#define HWIO_QFPROM_RAW_SPARE_REG19_MSB_SPARE1_BMSK                                              0xffffff
#define HWIO_QFPROM_RAW_SPARE_REG19_MSB_SPARE1_SHFT                                                   0x0

#define HWIO_QFPROM_RAW_SPARE_REG20_LSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000002d8)
#define HWIO_QFPROM_RAW_SPARE_REG20_LSB_PHYS                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000002d8)
#define HWIO_QFPROM_RAW_SPARE_REG20_LSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG20_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG20_LSB_ADDR, HWIO_QFPROM_RAW_SPARE_REG20_LSB_RMSK)
#define HWIO_QFPROM_RAW_SPARE_REG20_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG20_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_SPARE_REG20_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_SPARE_REG20_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_SPARE_REG20_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_SPARE_REG20_LSB_ADDR,m,v,HWIO_QFPROM_RAW_SPARE_REG20_LSB_IN)
#define HWIO_QFPROM_RAW_SPARE_REG20_LSB_SPARE0_BMSK                                            0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG20_LSB_SPARE0_SHFT                                                   0x0

#define HWIO_QFPROM_RAW_SPARE_REG20_MSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000002dc)
#define HWIO_QFPROM_RAW_SPARE_REG20_MSB_PHYS                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000002dc)
#define HWIO_QFPROM_RAW_SPARE_REG20_MSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG20_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG20_MSB_ADDR, HWIO_QFPROM_RAW_SPARE_REG20_MSB_RMSK)
#define HWIO_QFPROM_RAW_SPARE_REG20_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG20_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_SPARE_REG20_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_SPARE_REG20_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_SPARE_REG20_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_SPARE_REG20_MSB_ADDR,m,v,HWIO_QFPROM_RAW_SPARE_REG20_MSB_IN)
#define HWIO_QFPROM_RAW_SPARE_REG20_MSB_SPARE0_BMSK                                            0x80000000
#define HWIO_QFPROM_RAW_SPARE_REG20_MSB_SPARE0_SHFT                                                  0x1f
#define HWIO_QFPROM_RAW_SPARE_REG20_MSB_FEC_VALUE_BMSK                                         0x7f000000
#define HWIO_QFPROM_RAW_SPARE_REG20_MSB_FEC_VALUE_SHFT                                               0x18
#define HWIO_QFPROM_RAW_SPARE_REG20_MSB_SPARE1_BMSK                                              0xffffff
#define HWIO_QFPROM_RAW_SPARE_REG20_MSB_SPARE1_SHFT                                                   0x0

#define HWIO_QFPROM_RAW_SPARE_REG21_LSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000002e0)
#define HWIO_QFPROM_RAW_SPARE_REG21_LSB_PHYS                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000002e0)
#define HWIO_QFPROM_RAW_SPARE_REG21_LSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG21_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG21_LSB_ADDR, HWIO_QFPROM_RAW_SPARE_REG21_LSB_RMSK)
#define HWIO_QFPROM_RAW_SPARE_REG21_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG21_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_SPARE_REG21_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_SPARE_REG21_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_SPARE_REG21_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_SPARE_REG21_LSB_ADDR,m,v,HWIO_QFPROM_RAW_SPARE_REG21_LSB_IN)
#define HWIO_QFPROM_RAW_SPARE_REG21_LSB_SPARE0_BMSK                                            0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG21_LSB_SPARE0_SHFT                                                   0x0

#define HWIO_QFPROM_RAW_SPARE_REG21_MSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000002e4)
#define HWIO_QFPROM_RAW_SPARE_REG21_MSB_PHYS                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000002e4)
#define HWIO_QFPROM_RAW_SPARE_REG21_MSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_RAW_SPARE_REG21_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG21_MSB_ADDR, HWIO_QFPROM_RAW_SPARE_REG21_MSB_RMSK)
#define HWIO_QFPROM_RAW_SPARE_REG21_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_SPARE_REG21_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_SPARE_REG21_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_SPARE_REG21_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_SPARE_REG21_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_SPARE_REG21_MSB_ADDR,m,v,HWIO_QFPROM_RAW_SPARE_REG21_MSB_IN)
#define HWIO_QFPROM_RAW_SPARE_REG21_MSB_SPARE0_BMSK                                            0x80000000
#define HWIO_QFPROM_RAW_SPARE_REG21_MSB_SPARE0_SHFT                                                  0x1f
#define HWIO_QFPROM_RAW_SPARE_REG21_MSB_FEC_VALUE_BMSK                                         0x7f000000
#define HWIO_QFPROM_RAW_SPARE_REG21_MSB_FEC_VALUE_SHFT                                               0x18
#define HWIO_QFPROM_RAW_SPARE_REG21_MSB_SPARE1_BMSK                                              0xffffff
#define HWIO_QFPROM_RAW_SPARE_REG21_MSB_SPARE1_SHFT                                                   0x0

#define HWIO_QFPROM_RAW_ACC_PRIVATEn_ADDR(n)                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x00000400 + 0x4 * (n))
#define HWIO_QFPROM_RAW_ACC_PRIVATEn_PHYS(n)                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00000400 + 0x4 * (n))
#define HWIO_QFPROM_RAW_ACC_PRIVATEn_RMSK                                                      0xffffffff
#define HWIO_QFPROM_RAW_ACC_PRIVATEn_MAXn                                                              39
#define HWIO_QFPROM_RAW_ACC_PRIVATEn_INI(n)        \
        in_dword_masked(HWIO_QFPROM_RAW_ACC_PRIVATEn_ADDR(n), HWIO_QFPROM_RAW_ACC_PRIVATEn_RMSK)
#define HWIO_QFPROM_RAW_ACC_PRIVATEn_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_RAW_ACC_PRIVATEn_ADDR(n), mask)
#define HWIO_QFPROM_RAW_ACC_PRIVATEn_ACC_PRIVATE_BMSK                                          0xffffffff
#define HWIO_QFPROM_RAW_ACC_PRIVATEn_ACC_PRIVATE_SHFT                                                 0x0

#define HWIO_ACC_IR_ADDR                                                                       (SECURITY_CONTROL_CORE_REG_BASE      + 0x00002000)
#define HWIO_ACC_IR_PHYS                                                                       (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00002000)
#define HWIO_ACC_IR_RMSK                                                                             0x1f
#define HWIO_ACC_IR_OUT(v)      \
        out_dword(HWIO_ACC_IR_ADDR,v)
#define HWIO_ACC_IR_INSTRUCTION_BMSK                                                                 0x1f
#define HWIO_ACC_IR_INSTRUCTION_SHFT                                                                  0x0

#define HWIO_ACC_DR_ADDR                                                                       (SECURITY_CONTROL_CORE_REG_BASE      + 0x00002004)
#define HWIO_ACC_DR_PHYS                                                                       (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00002004)
#define HWIO_ACC_DR_RMSK                                                                       0xffffffff
#define HWIO_ACC_DR_IN          \
        in_dword_masked(HWIO_ACC_DR_ADDR, HWIO_ACC_DR_RMSK)
#define HWIO_ACC_DR_INM(m)      \
        in_dword_masked(HWIO_ACC_DR_ADDR, m)
#define HWIO_ACC_DR_OUT(v)      \
        out_dword(HWIO_ACC_DR_ADDR,v)
#define HWIO_ACC_DR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_ACC_DR_ADDR,m,v,HWIO_ACC_DR_IN)
#define HWIO_ACC_DR_DR_BMSK                                                                    0xffffffff
#define HWIO_ACC_DR_DR_SHFT                                                                           0x0

#define HWIO_ACC_VERID_ADDR                                                                    (SECURITY_CONTROL_CORE_REG_BASE      + 0x00002008)
#define HWIO_ACC_VERID_PHYS                                                                    (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00002008)
#define HWIO_ACC_VERID_RMSK                                                                        0xffff
#define HWIO_ACC_VERID_IN          \
        in_dword_masked(HWIO_ACC_VERID_ADDR, HWIO_ACC_VERID_RMSK)
#define HWIO_ACC_VERID_INM(m)      \
        in_dword_masked(HWIO_ACC_VERID_ADDR, m)
#define HWIO_ACC_VERID_FWVERID_BMSK                                                                0xff00
#define HWIO_ACC_VERID_FWVERID_SHFT                                                                   0x8
#define HWIO_ACC_VERID_HWVERID_BMSK                                                                  0xff
#define HWIO_ACC_VERID_HWVERID_SHFT                                                                   0x0

#define HWIO_ACC_FEATSETn_ADDR(n)                                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00002010 + 0x4 * (n))
#define HWIO_ACC_FEATSETn_PHYS(n)                                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00002010 + 0x4 * (n))
#define HWIO_ACC_FEATSETn_RMSK                                                                 0xffffffff
#define HWIO_ACC_FEATSETn_MAXn                                                                          7
#define HWIO_ACC_FEATSETn_INI(n)        \
        in_dword_masked(HWIO_ACC_FEATSETn_ADDR(n), HWIO_ACC_FEATSETn_RMSK)
#define HWIO_ACC_FEATSETn_INMI(n,mask)    \
        in_dword_masked(HWIO_ACC_FEATSETn_ADDR(n), mask)
#define HWIO_ACC_FEATSETn_FEAT_BMSK                                                            0xffffffff
#define HWIO_ACC_FEATSETn_FEAT_SHFT                                                                   0x0

#define HWIO_ACC_STATE_ADDR                                                                    (SECURITY_CONTROL_CORE_REG_BASE      + 0x00002038)
#define HWIO_ACC_STATE_PHYS                                                                    (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00002038)
#define HWIO_ACC_STATE_RMSK                                                                           0x7
#define HWIO_ACC_STATE_IN          \
        in_dword_masked(HWIO_ACC_STATE_ADDR, HWIO_ACC_STATE_RMSK)
#define HWIO_ACC_STATE_INM(m)      \
        in_dword_masked(HWIO_ACC_STATE_ADDR, m)
#define HWIO_ACC_STATE_ACC_READY_BMSK                                                                 0x4
#define HWIO_ACC_STATE_ACC_READY_SHFT                                                                 0x2
#define HWIO_ACC_STATE_ACC_LOCKED_BMSK                                                                0x2
#define HWIO_ACC_STATE_ACC_LOCKED_SHFT                                                                0x1
#define HWIO_ACC_STATE_ACC_STOP_BMSK                                                                  0x1
#define HWIO_ACC_STATE_ACC_STOP_SHFT                                                                  0x0

#define HWIO_QFPROM_BLOW_TIMER_ADDR                                                            (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000203c)
#define HWIO_QFPROM_BLOW_TIMER_PHYS                                                            (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x0000203c)
#define HWIO_QFPROM_BLOW_TIMER_RMSK                                                                 0xfff
#define HWIO_QFPROM_BLOW_TIMER_IN          \
        in_dword_masked(HWIO_QFPROM_BLOW_TIMER_ADDR, HWIO_QFPROM_BLOW_TIMER_RMSK)
#define HWIO_QFPROM_BLOW_TIMER_INM(m)      \
        in_dword_masked(HWIO_QFPROM_BLOW_TIMER_ADDR, m)
#define HWIO_QFPROM_BLOW_TIMER_OUT(v)      \
        out_dword(HWIO_QFPROM_BLOW_TIMER_ADDR,v)
#define HWIO_QFPROM_BLOW_TIMER_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_BLOW_TIMER_ADDR,m,v,HWIO_QFPROM_BLOW_TIMER_IN)
#define HWIO_QFPROM_BLOW_TIMER_BLOW_TIMER_BMSK                                                      0xfff
#define HWIO_QFPROM_BLOW_TIMER_BLOW_TIMER_SHFT                                                        0x0

#define HWIO_QFPROM_TEST_CTRL_ADDR                                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00002040)
#define HWIO_QFPROM_TEST_CTRL_PHYS                                                             (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00002040)
#define HWIO_QFPROM_TEST_CTRL_RMSK                                                                    0xf
#define HWIO_QFPROM_TEST_CTRL_IN          \
        in_dword_masked(HWIO_QFPROM_TEST_CTRL_ADDR, HWIO_QFPROM_TEST_CTRL_RMSK)
#define HWIO_QFPROM_TEST_CTRL_INM(m)      \
        in_dword_masked(HWIO_QFPROM_TEST_CTRL_ADDR, m)
#define HWIO_QFPROM_TEST_CTRL_OUT(v)      \
        out_dword(HWIO_QFPROM_TEST_CTRL_ADDR,v)
#define HWIO_QFPROM_TEST_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_TEST_CTRL_ADDR,m,v,HWIO_QFPROM_TEST_CTRL_IN)
#define HWIO_QFPROM_TEST_CTRL_SEL_TST_ROM_BMSK                                                        0x8
#define HWIO_QFPROM_TEST_CTRL_SEL_TST_ROM_SHFT                                                        0x3
#define HWIO_QFPROM_TEST_CTRL_SEL_TST_WL_BMSK                                                         0x4
#define HWIO_QFPROM_TEST_CTRL_SEL_TST_WL_SHFT                                                         0x2
#define HWIO_QFPROM_TEST_CTRL_SEL_TST_BL_BMSK                                                         0x2
#define HWIO_QFPROM_TEST_CTRL_SEL_TST_BL_SHFT                                                         0x1
#define HWIO_QFPROM_TEST_CTRL_EN_FUSE_RES_MEAS_BMSK                                                   0x1
#define HWIO_QFPROM_TEST_CTRL_EN_FUSE_RES_MEAS_SHFT                                                   0x0

#define HWIO_QFPROM_ACCEL_ADDR                                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x00002044)
#define HWIO_QFPROM_ACCEL_PHYS                                                                 (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00002044)
#define HWIO_QFPROM_ACCEL_RMSK                                                                      0xfff
#define HWIO_QFPROM_ACCEL_IN          \
        in_dword_masked(HWIO_QFPROM_ACCEL_ADDR, HWIO_QFPROM_ACCEL_RMSK)
#define HWIO_QFPROM_ACCEL_INM(m)      \
        in_dword_masked(HWIO_QFPROM_ACCEL_ADDR, m)
#define HWIO_QFPROM_ACCEL_OUT(v)      \
        out_dword(HWIO_QFPROM_ACCEL_ADDR,v)
#define HWIO_QFPROM_ACCEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_ACCEL_ADDR,m,v,HWIO_QFPROM_ACCEL_IN)
#define HWIO_QFPROM_ACCEL_QFPROM_GATELAST_BMSK                                                      0x800
#define HWIO_QFPROM_ACCEL_QFPROM_GATELAST_SHFT                                                        0xb
#define HWIO_QFPROM_ACCEL_QFPROM_TRIPPT_SEL_BMSK                                                    0x700
#define HWIO_QFPROM_ACCEL_QFPROM_TRIPPT_SEL_SHFT                                                      0x8
#define HWIO_QFPROM_ACCEL_QFPROM_ACCEL_BMSK                                                          0xff
#define HWIO_QFPROM_ACCEL_QFPROM_ACCEL_SHFT                                                           0x0

#define HWIO_QFPROM_BLOW_STATUS_ADDR                                                           (SECURITY_CONTROL_CORE_REG_BASE      + 0x00002048)
#define HWIO_QFPROM_BLOW_STATUS_PHYS                                                           (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00002048)
#define HWIO_QFPROM_BLOW_STATUS_RMSK                                                                  0x3
#define HWIO_QFPROM_BLOW_STATUS_IN          \
        in_dword_masked(HWIO_QFPROM_BLOW_STATUS_ADDR, HWIO_QFPROM_BLOW_STATUS_RMSK)
#define HWIO_QFPROM_BLOW_STATUS_INM(m)      \
        in_dword_masked(HWIO_QFPROM_BLOW_STATUS_ADDR, m)
#define HWIO_QFPROM_BLOW_STATUS_QFPROM_WR_ERR_BMSK                                                    0x2
#define HWIO_QFPROM_BLOW_STATUS_QFPROM_WR_ERR_SHFT                                                    0x1
#define HWIO_QFPROM_BLOW_STATUS_QFPROM_BUSY_BMSK                                                      0x1
#define HWIO_QFPROM_BLOW_STATUS_QFPROM_BUSY_SHFT                                                      0x0

#define HWIO_QFPROM_ROM_ERROR_ADDR                                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000204c)
#define HWIO_QFPROM_ROM_ERROR_PHYS                                                             (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x0000204c)
#define HWIO_QFPROM_ROM_ERROR_RMSK                                                                    0x1
#define HWIO_QFPROM_ROM_ERROR_IN          \
        in_dword_masked(HWIO_QFPROM_ROM_ERROR_ADDR, HWIO_QFPROM_ROM_ERROR_RMSK)
#define HWIO_QFPROM_ROM_ERROR_INM(m)      \
        in_dword_masked(HWIO_QFPROM_ROM_ERROR_ADDR, m)
#define HWIO_QFPROM_ROM_ERROR_ERROR_BMSK                                                              0x1
#define HWIO_QFPROM_ROM_ERROR_ERROR_SHFT                                                              0x0

#define HWIO_QFPROM_BIST_CTRL_ADDR                                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00002050)
#define HWIO_QFPROM_BIST_CTRL_PHYS                                                             (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00002050)
#define HWIO_QFPROM_BIST_CTRL_RMSK                                                                   0x7f
#define HWIO_QFPROM_BIST_CTRL_IN          \
        in_dword_masked(HWIO_QFPROM_BIST_CTRL_ADDR, HWIO_QFPROM_BIST_CTRL_RMSK)
#define HWIO_QFPROM_BIST_CTRL_INM(m)      \
        in_dword_masked(HWIO_QFPROM_BIST_CTRL_ADDR, m)
#define HWIO_QFPROM_BIST_CTRL_OUT(v)      \
        out_dword(HWIO_QFPROM_BIST_CTRL_ADDR,v)
#define HWIO_QFPROM_BIST_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_BIST_CTRL_ADDR,m,v,HWIO_QFPROM_BIST_CTRL_IN)
#define HWIO_QFPROM_BIST_CTRL_AUTH_REGION_BMSK                                                       0x7c
#define HWIO_QFPROM_BIST_CTRL_AUTH_REGION_SHFT                                                        0x2
#define HWIO_QFPROM_BIST_CTRL_SHA_ENABLE_BMSK                                                         0x2
#define HWIO_QFPROM_BIST_CTRL_SHA_ENABLE_SHFT                                                         0x1
#define HWIO_QFPROM_BIST_CTRL_START_BMSK                                                              0x1
#define HWIO_QFPROM_BIST_CTRL_START_SHFT                                                              0x0

#define HWIO_QFPROM_BIST_ERROR_ADDR                                                            (SECURITY_CONTROL_CORE_REG_BASE      + 0x00002054)
#define HWIO_QFPROM_BIST_ERROR_PHYS                                                            (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00002054)
#define HWIO_QFPROM_BIST_ERROR_RMSK                                                            0xffffffff
#define HWIO_QFPROM_BIST_ERROR_IN          \
        in_dword_masked(HWIO_QFPROM_BIST_ERROR_ADDR, HWIO_QFPROM_BIST_ERROR_RMSK)
#define HWIO_QFPROM_BIST_ERROR_INM(m)      \
        in_dword_masked(HWIO_QFPROM_BIST_ERROR_ADDR, m)
#define HWIO_QFPROM_BIST_ERROR_ERROR_BMSK                                                      0xffffffff
#define HWIO_QFPROM_BIST_ERROR_ERROR_SHFT                                                             0x0

#define HWIO_QFPROM_HASH_SIGNATUREn_ADDR(n)                                                    (SECURITY_CONTROL_CORE_REG_BASE      + 0x00002060 + 0x4 * (n))
#define HWIO_QFPROM_HASH_SIGNATUREn_PHYS(n)                                                    (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00002060 + 0x4 * (n))
#define HWIO_QFPROM_HASH_SIGNATUREn_RMSK                                                       0xffffffff
#define HWIO_QFPROM_HASH_SIGNATUREn_MAXn                                                                7
#define HWIO_QFPROM_HASH_SIGNATUREn_INI(n)        \
        in_dword_masked(HWIO_QFPROM_HASH_SIGNATUREn_ADDR(n), HWIO_QFPROM_HASH_SIGNATUREn_RMSK)
#define HWIO_QFPROM_HASH_SIGNATUREn_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_HASH_SIGNATUREn_ADDR(n), mask)
#define HWIO_QFPROM_HASH_SIGNATUREn_HASH_VALUE_BMSK                                            0xffffffff
#define HWIO_QFPROM_HASH_SIGNATUREn_HASH_VALUE_SHFT                                                   0x0

#define HWIO_HW_KEY_STATUS_ADDR                                                                (SECURITY_CONTROL_CORE_REG_BASE      + 0x00002080)
#define HWIO_HW_KEY_STATUS_PHYS                                                                (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00002080)
#define HWIO_HW_KEY_STATUS_RMSK                                                                      0x1f
#define HWIO_HW_KEY_STATUS_IN          \
        in_dword_masked(HWIO_HW_KEY_STATUS_ADDR, HWIO_HW_KEY_STATUS_RMSK)
#define HWIO_HW_KEY_STATUS_INM(m)      \
        in_dword_masked(HWIO_HW_KEY_STATUS_ADDR, m)
#define HWIO_HW_KEY_STATUS_KDF_DONE_BMSK                                                             0x10
#define HWIO_HW_KEY_STATUS_KDF_DONE_SHFT                                                              0x4
#define HWIO_HW_KEY_STATUS_MSA_KEYS_BLOCKED_BMSK                                                      0x8
#define HWIO_HW_KEY_STATUS_MSA_KEYS_BLOCKED_SHFT                                                      0x3
#define HWIO_HW_KEY_STATUS_APPS_KEYS_BLOCKED_BMSK                                                     0x4
#define HWIO_HW_KEY_STATUS_APPS_KEYS_BLOCKED_SHFT                                                     0x2
#define HWIO_HW_KEY_STATUS_SEC_KEY_DERIVATION_KEY_BLOWN_BMSK                                          0x2
#define HWIO_HW_KEY_STATUS_SEC_KEY_DERIVATION_KEY_BLOWN_SHFT                                          0x1
#define HWIO_HW_KEY_STATUS_PRI_KEY_DERIVATION_KEY_BLOWN_BMSK                                          0x1
#define HWIO_HW_KEY_STATUS_PRI_KEY_DERIVATION_KEY_BLOWN_SHFT                                          0x0

#define HWIO_RESET_JDR_STATUS_ADDR                                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00002084)
#define HWIO_RESET_JDR_STATUS_PHYS                                                             (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00002084)
#define HWIO_RESET_JDR_STATUS_RMSK                                                                    0x3
#define HWIO_RESET_JDR_STATUS_IN          \
        in_dword_masked(HWIO_RESET_JDR_STATUS_ADDR, HWIO_RESET_JDR_STATUS_RMSK)
#define HWIO_RESET_JDR_STATUS_INM(m)      \
        in_dword_masked(HWIO_RESET_JDR_STATUS_ADDR, m)
#define HWIO_RESET_JDR_STATUS_FORCE_RESET_BMSK                                                        0x2
#define HWIO_RESET_JDR_STATUS_FORCE_RESET_SHFT                                                        0x1
#define HWIO_RESET_JDR_STATUS_DISABLE_SYSTEM_RESET_BMSK                                               0x1
#define HWIO_RESET_JDR_STATUS_DISABLE_SYSTEM_RESET_SHFT                                               0x0

#define HWIO_FEC_ESR_ADDR                                                                      (SECURITY_CONTROL_CORE_REG_BASE      + 0x00002090)
#define HWIO_FEC_ESR_PHYS                                                                      (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00002090)
#define HWIO_FEC_ESR_RMSK                                                                           0xfff
#define HWIO_FEC_ESR_IN          \
        in_dword_masked(HWIO_FEC_ESR_ADDR, HWIO_FEC_ESR_RMSK)
#define HWIO_FEC_ESR_INM(m)      \
        in_dword_masked(HWIO_FEC_ESR_ADDR, m)
#define HWIO_FEC_ESR_OUT(v)      \
        out_dword(HWIO_FEC_ESR_ADDR,v)
#define HWIO_FEC_ESR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_FEC_ESR_ADDR,m,v,HWIO_FEC_ESR_IN)
#define HWIO_FEC_ESR_CORR_SW_ACC_BMSK                                                               0x800
#define HWIO_FEC_ESR_CORR_SW_ACC_SHFT                                                                 0xb
#define HWIO_FEC_ESR_CORR_BOOT_ROM_BMSK                                                             0x200
#define HWIO_FEC_ESR_CORR_BOOT_ROM_SHFT                                                               0x9
#define HWIO_FEC_ESR_CORR_FUSE_SENSE_BMSK                                                           0x100
#define HWIO_FEC_ESR_CORR_FUSE_SENSE_SHFT                                                             0x8
#define HWIO_FEC_ESR_CORR_MULT_BMSK                                                                  0x80
#define HWIO_FEC_ESR_CORR_MULT_SHFT                                                                   0x7
#define HWIO_FEC_ESR_CORR_SEEN_BMSK                                                                  0x40
#define HWIO_FEC_ESR_CORR_SEEN_SHFT                                                                   0x6
#define HWIO_FEC_ESR_ERR_SW_ACC_BMSK                                                                 0x20
#define HWIO_FEC_ESR_ERR_SW_ACC_SHFT                                                                  0x5
#define HWIO_FEC_ESR_ERR_BOOT_ROM_BMSK                                                                0x8
#define HWIO_FEC_ESR_ERR_BOOT_ROM_SHFT                                                                0x3
#define HWIO_FEC_ESR_ERR_FUSE_SENSE_BMSK                                                              0x4
#define HWIO_FEC_ESR_ERR_FUSE_SENSE_SHFT                                                              0x2
#define HWIO_FEC_ESR_ERR_MULT_BMSK                                                                    0x2
#define HWIO_FEC_ESR_ERR_MULT_SHFT                                                                    0x1
#define HWIO_FEC_ESR_ERR_SEEN_BMSK                                                                    0x1
#define HWIO_FEC_ESR_ERR_SEEN_SHFT                                                                    0x0

#define HWIO_FEC_EAR_ADDR                                                                      (SECURITY_CONTROL_CORE_REG_BASE      + 0x00002094)
#define HWIO_FEC_EAR_PHYS                                                                      (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00002094)
#define HWIO_FEC_EAR_RMSK                                                                      0xffffffff
#define HWIO_FEC_EAR_IN          \
        in_dword_masked(HWIO_FEC_EAR_ADDR, HWIO_FEC_EAR_RMSK)
#define HWIO_FEC_EAR_INM(m)      \
        in_dword_masked(HWIO_FEC_EAR_ADDR, m)
#define HWIO_FEC_EAR_CORR_ADDR_BMSK                                                            0xffff0000
#define HWIO_FEC_EAR_CORR_ADDR_SHFT                                                                  0x10
#define HWIO_FEC_EAR_ERR_ADDR_BMSK                                                                 0xffff
#define HWIO_FEC_EAR_ERR_ADDR_SHFT                                                                    0x0

#define HWIO_QFPROM0_MATCH_STATUS_ADDR                                                         (SECURITY_CONTROL_CORE_REG_BASE      + 0x00002098)
#define HWIO_QFPROM0_MATCH_STATUS_PHYS                                                         (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00002098)
#define HWIO_QFPROM0_MATCH_STATUS_RMSK                                                         0xffffffff
#define HWIO_QFPROM0_MATCH_STATUS_IN          \
        in_dword_masked(HWIO_QFPROM0_MATCH_STATUS_ADDR, HWIO_QFPROM0_MATCH_STATUS_RMSK)
#define HWIO_QFPROM0_MATCH_STATUS_INM(m)      \
        in_dword_masked(HWIO_QFPROM0_MATCH_STATUS_ADDR, m)
#define HWIO_QFPROM0_MATCH_STATUS_FLAG_BMSK                                                    0xffffffff
#define HWIO_QFPROM0_MATCH_STATUS_FLAG_SHFT                                                           0x0

#define HWIO_QFPROM1_MATCH_STATUS_ADDR                                                         (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000209c)
#define HWIO_QFPROM1_MATCH_STATUS_PHYS                                                         (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x0000209c)
#define HWIO_QFPROM1_MATCH_STATUS_RMSK                                                         0xffffffff
#define HWIO_QFPROM1_MATCH_STATUS_IN          \
        in_dword_masked(HWIO_QFPROM1_MATCH_STATUS_ADDR, HWIO_QFPROM1_MATCH_STATUS_RMSK)
#define HWIO_QFPROM1_MATCH_STATUS_INM(m)      \
        in_dword_masked(HWIO_QFPROM1_MATCH_STATUS_ADDR, m)
#define HWIO_QFPROM1_MATCH_STATUS_FLAG_BMSK                                                    0xffffffff
#define HWIO_QFPROM1_MATCH_STATUS_FLAG_SHFT                                                           0x0

#define HWIO_QFPROM_CORR_JTAG_ID_ADDR                                                          (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004000)
#define HWIO_QFPROM_CORR_JTAG_ID_PHYS                                                          (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00004000)
#define HWIO_QFPROM_CORR_JTAG_ID_RMSK                                                          0xffffffff
#define HWIO_QFPROM_CORR_JTAG_ID_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_JTAG_ID_ADDR, HWIO_QFPROM_CORR_JTAG_ID_RMSK)
#define HWIO_QFPROM_CORR_JTAG_ID_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_JTAG_ID_ADDR, m)
#define HWIO_QFPROM_CORR_JTAG_ID_IDDQ_CX_ON_BMSK                                               0xc0000000
#define HWIO_QFPROM_CORR_JTAG_ID_IDDQ_CX_ON_SHFT                                                     0x1e
#define HWIO_QFPROM_CORR_JTAG_ID_IDDQ_RC_BMSK                                                  0x20000000
#define HWIO_QFPROM_CORR_JTAG_ID_IDDQ_RC_SHFT                                                        0x1d
#define HWIO_QFPROM_CORR_JTAG_ID_MACCHIATO_EN_BMSK                                             0x10000000
#define HWIO_QFPROM_CORR_JTAG_ID_MACCHIATO_EN_SHFT                                                   0x1c
#define HWIO_QFPROM_CORR_JTAG_ID_FEATURE_ID_BMSK                                                0xff00000
#define HWIO_QFPROM_CORR_JTAG_ID_FEATURE_ID_SHFT                                                     0x14
#define HWIO_QFPROM_CORR_JTAG_ID_JTAG_ID_BMSK                                                     0xfffff
#define HWIO_QFPROM_CORR_JTAG_ID_JTAG_ID_SHFT                                                         0x0

#define HWIO_QFPROM_CORR_PTE1_ADDR                                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004004)
#define HWIO_QFPROM_CORR_PTE1_PHYS                                                             (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00004004)
#define HWIO_QFPROM_CORR_PTE1_RMSK                                                             0xffffffff
#define HWIO_QFPROM_CORR_PTE1_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_PTE1_ADDR, HWIO_QFPROM_CORR_PTE1_RMSK)
#define HWIO_QFPROM_CORR_PTE1_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_PTE1_ADDR, m)
#define HWIO_QFPROM_CORR_PTE1_IDDQ_BIN_BMSK                                                    0xe0000000
#define HWIO_QFPROM_CORR_PTE1_IDDQ_BIN_SHFT                                                          0x1d
#define HWIO_QFPROM_CORR_PTE1_IDDQ_MULTIPLIER_BMSK                                             0x1c000000
#define HWIO_QFPROM_CORR_PTE1_IDDQ_MULTIPLIER_SHFT                                                   0x1a
#define HWIO_QFPROM_CORR_PTE1_PROCESS_NODE_ID_BMSK                                              0x2000000
#define HWIO_QFPROM_CORR_PTE1_PROCESS_NODE_ID_SHFT                                                   0x19
#define HWIO_QFPROM_CORR_PTE1_IDDQ_MX_OFF_BMSK                                                  0x1f80000
#define HWIO_QFPROM_CORR_PTE1_IDDQ_MX_OFF_SHFT                                                       0x13
#define HWIO_QFPROM_CORR_PTE1_IDDQ_CX_OFF_BMSK                                                    0x7f000
#define HWIO_QFPROM_CORR_PTE1_IDDQ_CX_OFF_SHFT                                                        0xc
#define HWIO_QFPROM_CORR_PTE1_IDDQ_MX_ON_BMSK                                                       0xfe0
#define HWIO_QFPROM_CORR_PTE1_IDDQ_MX_ON_SHFT                                                         0x5
#define HWIO_QFPROM_CORR_PTE1_IDDQ_CX_ON_BMSK                                                        0x1f
#define HWIO_QFPROM_CORR_PTE1_IDDQ_CX_ON_SHFT                                                         0x0

#define HWIO_QFPROM_CORR_SERIAL_NUM_ADDR                                                       (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004008)
#define HWIO_QFPROM_CORR_SERIAL_NUM_PHYS                                                       (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00004008)
#define HWIO_QFPROM_CORR_SERIAL_NUM_RMSK                                                       0xffffffff
#define HWIO_QFPROM_CORR_SERIAL_NUM_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_SERIAL_NUM_ADDR, HWIO_QFPROM_CORR_SERIAL_NUM_RMSK)
#define HWIO_QFPROM_CORR_SERIAL_NUM_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_SERIAL_NUM_ADDR, m)
#define HWIO_QFPROM_CORR_SERIAL_NUM_SERIAL_NUM_BMSK                                            0xffffffff
#define HWIO_QFPROM_CORR_SERIAL_NUM_SERIAL_NUM_SHFT                                                   0x0

#define HWIO_QFPROM_CORR_PTE2_ADDR                                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000400c)
#define HWIO_QFPROM_CORR_PTE2_PHYS                                                             (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x0000400c)
#define HWIO_QFPROM_CORR_PTE2_RMSK                                                             0xffffffff
#define HWIO_QFPROM_CORR_PTE2_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_PTE2_ADDR, HWIO_QFPROM_CORR_PTE2_RMSK)
#define HWIO_QFPROM_CORR_PTE2_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_PTE2_ADDR, m)
#define HWIO_QFPROM_CORR_PTE2_PTE_IDDQ_SPARE_BMSK                                              0xfc000000
#define HWIO_QFPROM_CORR_PTE2_PTE_IDDQ_SPARE_SHFT                                                    0x1a
#define HWIO_QFPROM_CORR_PTE2_IDDQ_MSS_OFF_BMSK                                                 0x3f80000
#define HWIO_QFPROM_CORR_PTE2_IDDQ_MSS_OFF_SHFT                                                      0x13
#define HWIO_QFPROM_CORR_PTE2_IDDQ_MSS_ON_BMSK                                                    0x7f800
#define HWIO_QFPROM_CORR_PTE2_IDDQ_MSS_ON_SHFT                                                        0xb
#define HWIO_QFPROM_CORR_PTE2_FOUNDRY_ID_BMSK                                                       0x700
#define HWIO_QFPROM_CORR_PTE2_FOUNDRY_ID_SHFT                                                         0x8
#define HWIO_QFPROM_CORR_PTE2_LOGIC_RETENTION_BMSK                                                   0xe0
#define HWIO_QFPROM_CORR_PTE2_LOGIC_RETENTION_SHFT                                                    0x5
#define HWIO_QFPROM_CORR_PTE2_SPEED_BIN_BMSK                                                         0x1c
#define HWIO_QFPROM_CORR_PTE2_SPEED_BIN_SHFT                                                          0x2
#define HWIO_QFPROM_CORR_PTE2_MX_RET_BIN_BMSK                                                         0x3
#define HWIO_QFPROM_CORR_PTE2_MX_RET_BIN_SHFT                                                         0x0

#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004010)
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_PHYS                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00004010)
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_RD_WR_PERM_LSB_ADDR, HWIO_QFPROM_CORR_RD_WR_PERM_LSB_RMSK)
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_RD_WR_PERM_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SPARE_BMSK                                             0xffc00000
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SPARE_SHFT                                                   0x16
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SPARE21_BMSK                                             0x200000
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SPARE21_SHFT                                                 0x15
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SPARE20_BMSK                                             0x100000
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SPARE20_SHFT                                                 0x14
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SPARE19_BMSK                                              0x80000
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SPARE19_SHFT                                                 0x13
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SPARE18_BMSK                                              0x40000
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SPARE18_SHFT                                                 0x12
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SPARE17_BMSK                                              0x20000
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SPARE17_SHFT                                                 0x11
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SPARE16_BMSK                                              0x10000
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SPARE16_SHFT                                                 0x10
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_FEC_EN_BMSK                                                0x8000
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_FEC_EN_SHFT                                                   0xf
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_BOOT_ROM_PATCH_BMSK                                        0x4000
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_BOOT_ROM_PATCH_SHFT                                           0xe
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_MEM_CONFIG_BMSK                                            0x2000
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_MEM_CONFIG_SHFT                                               0xd
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_CALIB_BMSK                                                 0x1000
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_CALIB_SHFT                                                    0xc
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_PK_HASH_BMSK                                                0x800
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_PK_HASH_SHFT                                                  0xb
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_QC_SEC_BOOT_BMSK                                            0x400
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_QC_SEC_BOOT_SHFT                                              0xa
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_OEM_SEC_BOOT_BMSK                                           0x200
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_OEM_SEC_BOOT_SHFT                                             0x9
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SEC_KEY_DERIVATION_KEY_BMSK                                 0x100
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_SEC_KEY_DERIVATION_KEY_SHFT                                   0x8
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_PRI_KEY_DERIVATION_KEY_BMSK                                  0x80
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_PRI_KEY_DERIVATION_KEY_SHFT                                   0x7
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_FEAT_CONFIG_BMSK                                             0x40
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_FEAT_CONFIG_SHFT                                              0x6
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_OEM_CONFIG_BMSK                                              0x20
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_OEM_CONFIG_SHFT                                               0x5
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_ANTI_ROLLBACK_3_BMSK                                         0x10
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_ANTI_ROLLBACK_3_SHFT                                          0x4
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_ANTI_ROLLBACK_2_BMSK                                          0x8
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_ANTI_ROLLBACK_2_SHFT                                          0x3
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_ANTI_ROLLBACK_1_BMSK                                          0x4
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_ANTI_ROLLBACK_1_SHFT                                          0x2
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_RD_WR_PERM_BMSK                                               0x2
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_RD_WR_PERM_SHFT                                               0x1
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_PTE_BMSK                                                      0x1
#define HWIO_QFPROM_CORR_RD_WR_PERM_LSB_PTE_SHFT                                                      0x0

#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004014)
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_PHYS                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00004014)
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_RD_WR_PERM_MSB_ADDR, HWIO_QFPROM_CORR_RD_WR_PERM_MSB_RMSK)
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_RD_WR_PERM_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SPARE_BMSK                                             0xffc00000
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SPARE_SHFT                                                   0x16
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SPARE21_BMSK                                             0x200000
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SPARE21_SHFT                                                 0x15
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SPARE20_BMSK                                             0x100000
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SPARE20_SHFT                                                 0x14
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SPARE19_BMSK                                              0x80000
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SPARE19_SHFT                                                 0x13
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SPARE18_BMSK                                              0x40000
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SPARE18_SHFT                                                 0x12
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SPARE17_BMSK                                              0x20000
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SPARE17_SHFT                                                 0x11
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SPARE16_BMSK                                              0x10000
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SPARE16_SHFT                                                 0x10
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_FEC_EN_BMSK                                                0x8000
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_FEC_EN_SHFT                                                   0xf
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_BOOT_ROM_PATCH_BMSK                                        0x4000
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_BOOT_ROM_PATCH_SHFT                                           0xe
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_MEM_CONFIG_BMSK                                            0x2000
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_MEM_CONFIG_SHFT                                               0xd
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_CALIB_BMSK                                                 0x1000
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_CALIB_SHFT                                                    0xc
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_PK_HASH_BMSK                                                0x800
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_PK_HASH_SHFT                                                  0xb
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_QC_SEC_BOOT_BMSK                                            0x400
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_QC_SEC_BOOT_SHFT                                              0xa
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_OEM_SEC_BOOT_BMSK                                           0x200
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_OEM_SEC_BOOT_SHFT                                             0x9
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SEC_KEY_DERIVATION_KEY_BMSK                                 0x100
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_SEC_KEY_DERIVATION_KEY_SHFT                                   0x8
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_PRI_KEY_DERIVATION_KEY_BMSK                                  0x80
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_PRI_KEY_DERIVATION_KEY_SHFT                                   0x7
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_FEAT_CONFIG_BMSK                                             0x40
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_FEAT_CONFIG_SHFT                                              0x6
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_OEM_CONFIG_BMSK                                              0x20
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_OEM_CONFIG_SHFT                                               0x5
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_ANTI_ROLLBACK_3_BMSK                                         0x10
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_ANTI_ROLLBACK_3_SHFT                                          0x4
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_ANTI_ROLLBACK_2_BMSK                                          0x8
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_ANTI_ROLLBACK_2_SHFT                                          0x3
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_ANTI_ROLLBACK_1_BMSK                                          0x4
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_ANTI_ROLLBACK_1_SHFT                                          0x2
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_RD_WR_PERM_BMSK                                               0x2
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_RD_WR_PERM_SHFT                                               0x1
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_PTE_BMSK                                                      0x1
#define HWIO_QFPROM_CORR_RD_WR_PERM_MSB_PTE_SHFT                                                      0x0

#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_LSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004018)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_LSB_PHYS                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00004018)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_LSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_LSB_ADDR, HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_LSB_RMSK)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_LSB_PIL_SUBSYSTEM0_BMSK                               0xfc000000
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_LSB_PIL_SUBSYSTEM0_SHFT                                     0x1a
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_LSB_TZ_BMSK                                            0x3fff000
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_LSB_TZ_SHFT                                                  0xc
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_LSB_SBL1_BMSK                                              0xffe
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_LSB_SBL1_SHFT                                                0x1
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_LSB_RPMB_KEY_PROVISIONED_BMSK                                0x1
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_LSB_RPMB_KEY_PROVISIONED_SHFT                                0x0

#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_MSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000401c)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_MSB_PHYS                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x0000401c)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_MSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_MSB_ADDR, HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_MSB_RMSK)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_MSB_APPSBL0_BMSK                                      0xfffc0000
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_MSB_APPSBL0_SHFT                                            0x12
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_MSB_PIL_SUBSYSTEM1_BMSK                                  0x3ffff
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_1_MSB_PIL_SUBSYSTEM1_SHFT                                      0x0

#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_LSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004020)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_LSB_PHYS                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00004020)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_LSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_LSB_ADDR, HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_LSB_RMSK)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_LSB_APPSBL1_BMSK                                      0xffffffff
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_LSB_APPSBL1_SHFT                                             0x0

#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004024)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_PHYS                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00004024)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_ADDR, HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_RMSK)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_ROOT_CERT_PK_HASH_INDEX_BMSK                      0xff000000
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_ROOT_CERT_PK_HASH_INDEX_SHFT                            0x18
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_HYPERVISOR_BMSK                                     0xfff000
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_HYPERVISOR_SHFT                                          0xc
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_RPM_BMSK                                               0xff0
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_RPM_SHFT                                                 0x4
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_APPSBL2_BMSK                                             0xf
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_2_MSB_APPSBL2_SHFT                                             0x0

#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004028)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_PHYS                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00004028)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_ADDR, HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_RMSK)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_MSS_BMSK                                          0xffff0000
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_MSS_SHFT                                                0x10
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_MBA_BMSK                                              0xffff
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_LSB_MBA_SHFT                                                 0x0

#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000402c)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_PHYS                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x0000402c)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_ADDR, HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_RMSK)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_SPARE0_BMSK                                       0xffffff00
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_SPARE0_SHFT                                              0x8
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_MODEM_ROOT_CERT_PK_HASH_INDEX_BMSK                      0xff
#define HWIO_QFPROM_CORR_ANTI_ROLLBACK_3_MSB_MODEM_ROOT_CERT_PK_HASH_INDEX_SHFT                       0x0

#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004030)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_PHYS                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00004030)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_ADDR, HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_RMSK)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE0_BMSK                                       0xff000000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE0_SHFT                                             0x18
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE_REG21_SECURE_BMSK                             0x800000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE_REG21_SECURE_SHFT                                 0x17
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_PBL_LOG_DISABLE_BMSK                                0x400000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_PBL_LOG_DISABLE_SHFT                                    0x16
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_ROOT_CERT_TOTAL_NUM_BMSK                            0x3c0000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_ROOT_CERT_TOTAL_NUM_SHFT                                0x12
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE_REG20_SECURE_BMSK                              0x20000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE_REG20_SECURE_SHFT                                 0x11
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE_REG19_SECURE_BMSK                              0x10000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE_REG19_SECURE_SHFT                                 0x10
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE_REG18_SECURE_BMSK                               0x8000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE_REG18_SECURE_SHFT                                  0xf
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE_REG17_SECURE_BMSK                               0x4000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE_REG17_SECURE_SHFT                                  0xe
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE_REG16_SECURE_BMSK                               0x2000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE_REG16_SECURE_SHFT                                  0xd
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_WDOG_EN_BMSK                                          0x1000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_WDOG_EN_SHFT                                             0xc
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPDM_SECURE_MODE_BMSK                                  0x800
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPDM_SECURE_MODE_SHFT                                    0xb
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_ALT_SD_PORT_FOR_BOOT_BMSK                              0x400
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_ALT_SD_PORT_FOR_BOOT_SHFT                                0xa
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SDC_EMMC_MODE1P2_GPIO_DISABLE_BMSK                     0x200
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SDC_EMMC_MODE1P2_GPIO_DISABLE_SHFT                       0x9
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SDC_EMMC_MODE1P2_EN_BMSK                               0x100
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SDC_EMMC_MODE1P2_EN_SHFT                                 0x8
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_FAST_BOOT_BMSK                                          0xe0
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_FAST_BOOT_SHFT                                           0x5
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SDCC_MCLK_BOOT_FREQ_BMSK                                0x10
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SDCC_MCLK_BOOT_FREQ_SHFT                                 0x4
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_FORCE_DLOAD_DISABLE_BMSK                                 0x8
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_FORCE_DLOAD_DISABLE_SHFT                                 0x3
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE_BMSK                                               0x4
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_SPARE_SHFT                                               0x2
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_ENUM_TIMEOUT_BMSK                                        0x2
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_ENUM_TIMEOUT_SHFT                                        0x1
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_E_DLOAD_DISABLE_BMSK                                     0x1
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_LSB_E_DLOAD_DISABLE_SHFT                                     0x0

#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004034)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_PHYS                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00004034)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_ADDR, HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_RMSK)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_SPARE1_BMSK                                       0xff800000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_SPARE1_SHFT                                             0x17
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_ANTI_ROLLBACK_FEATURE_EN_BMSK                       0x780000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_ANTI_ROLLBACK_FEATURE_EN_SHFT                           0x13
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_DAP_DEVICEEN_DISABLE_BMSK                            0x40000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_DAP_DEVICEEN_DISABLE_SHFT                               0x12
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_DAP_SPNIDEN_DISABLE_BMSK                             0x20000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_DAP_SPNIDEN_DISABLE_SHFT                                0x11
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_DAP_SPIDEN_DISABLE_BMSK                              0x10000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_DAP_SPIDEN_DISABLE_SHFT                                 0x10
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_DAP_NIDEN_DISABLE_BMSK                                0x8000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_DAP_NIDEN_DISABLE_SHFT                                   0xf
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_DAP_DBGEN_DISABLE_BMSK                                0x4000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_DAP_DBGEN_DISABLE_SHFT                                   0xe
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_APPS_SPNIDEN_DISABLE_BMSK                             0x2000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_APPS_SPNIDEN_DISABLE_SHFT                                0xd
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_APPS_SPIDEN_DISABLE_BMSK                              0x1000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_APPS_SPIDEN_DISABLE_SHFT                                 0xc
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_APPS_NIDEN_DISABLE_BMSK                                0x800
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_APPS_NIDEN_DISABLE_SHFT                                  0xb
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_APPS_DBGEN_DISABLE_BMSK                                0x400
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_APPS_DBGEN_DISABLE_SHFT                                  0xa
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_SPARE1_DISABLE_BMSK                                    0x200
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_SPARE1_DISABLE_SHFT                                      0x9
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_SPARE0_DISABLE_BMSK                                    0x100
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_SPARE0_DISABLE_SHFT                                      0x8
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_VENUS_0_DBGEN_DISABLE_BMSK                              0x80
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_VENUS_0_DBGEN_DISABLE_SHFT                               0x7
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_RPM_DAPEN_DISABLE_BMSK                                  0x40
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_RPM_DAPEN_DISABLE_SHFT                                   0x6
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_RPM_WCSS_NIDEN_DISABLE_BMSK                             0x20
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_RPM_WCSS_NIDEN_DISABLE_SHFT                              0x5
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_RPM_DBGEN_DISABLE_BMSK                                  0x10
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_RPM_DBGEN_DISABLE_SHFT                                   0x4
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_WCSS_DBGEN_DISABLE_BMSK                                  0x8
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_WCSS_DBGEN_DISABLE_SHFT                                  0x3
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_MSS_NIDEN_DISABLE_BMSK                                   0x4
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_MSS_NIDEN_DISABLE_SHFT                                   0x2
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_MSS_DBGEN_DISABLE_BMSK                                   0x2
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_MSS_DBGEN_DISABLE_SHFT                                   0x1
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_ALL_DEBUG_DISABLE_BMSK                                   0x1
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW0_MSB_ALL_DEBUG_DISABLE_SHFT                                   0x0

#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004038)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_PHYS                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00004038)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_ADDR, HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_RMSK)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_OEM_PRODUCT_ID_BMSK                               0xffff0000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_OEM_PRODUCT_ID_SHFT                                     0x10
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_OEM_HW_ID_BMSK                                        0xffff
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_LSB_OEM_HW_ID_SHFT                                           0x0

#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_MSB_ADDR                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000403c)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_MSB_PHYS                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x0000403c)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_MSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_MSB_ADDR, HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_MSB_RMSK)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_MSB_PERIPH_VID_BMSK                                   0xffff0000
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_MSB_PERIPH_VID_SHFT                                         0x10
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_MSB_PERIPH_PID_BMSK                                       0xffff
#define HWIO_QFPROM_CORR_OEM_CONFIG_ROW1_MSB_PERIPH_PID_SHFT                                          0x0

#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004040)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_PHYS                                             (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00004040)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_ADDR, HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_RMSK)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_VU_EN_BMSK                            0xfc000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_VU_EN_SHFT                                  0x1a
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_NAV_EN_BMSK                                  0x2000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_NAV_EN_SHFT                                       0x19
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_LDO_EN_BMSK                                  0x1000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_LDO_EN_SHFT                                       0x18
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_ECS_EN_BMSK                                   0x800000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_ECS_EN_SHFT                                       0x17
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_WCDMA_EN_BMSK                           0x600000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_WCDMA_EN_SHFT                               0x15
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_TDSCDMA_EN_BMSK                         0x180000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_TDSCDMA_EN_SHFT                             0x13
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_LTE_EN_BMSK                              0x60000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_LTE_EN_SHFT                                 0x11
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_LTE_ABOVE_CAT2_EN_BMSK                   0x18000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_LTE_ABOVE_CAT2_EN_SHFT                       0xf
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_LTE_ABOVE_CAT1_EN_BMSK                    0x6000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_LTE_ABOVE_CAT1_EN_SHFT                       0xd
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_HSPA_MIMO_EN_BMSK                         0x1800
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_HSPA_MIMO_EN_SHFT                            0xb
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_HSPA_EN_BMSK                               0x600
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_HSPA_EN_SHFT                                 0x9
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_HSPA_DC_EN_BMSK                            0x180
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_HSPA_DC_EN_SHFT                              0x7
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_GENRAN_EN_BMSK                              0x60
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_GENRAN_EN_SHFT                               0x5
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_EN_BMSK                                     0x10
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_EN_SHFT                                      0x4
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_DO_EN_BMSK                                   0xc
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_DO_EN_SHFT                                   0x2
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_1X_EN_BMSK                                   0x3
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_LSB_MSMC_MODEM_1X_EN_SHFT                                   0x0

#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004044)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_PHYS                                             (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00004044)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_ADDR, HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_RMSK)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_SDC_EMMC_MODE1P2_FORCE_GPIO_BMSK                 0x80000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_SDC_EMMC_MODE1P2_FORCE_GPIO_SHFT                       0x1f
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_SPARE_BMSK                                       0x7ff00000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_SPARE_SHFT                                             0x14
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_MSMC_MDSP_FW_EN_BMSK                                0xfff00
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_MSMC_MDSP_FW_EN_SHFT                                    0x8
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_MSMC_SPARE_BMSK                                        0xc0
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_MSMC_SPARE_SHFT                                         0x6
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_MSMC_MODEM_VU_EN_BMSK                                  0x3f
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW0_MSB_MSMC_MODEM_VU_EN_SHFT                                   0x0

#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004048)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_PHYS                                             (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00004048)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_ADDR, HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_RMSK)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_ACC_DISABLE_BMSK                                 0x80000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_ACC_DISABLE_SHFT                                       0x1f
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_SHADOW_READ_DISABLE_BMSK                         0x40000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_SHADOW_READ_DISABLE_SHFT                               0x1e
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_SPARE1_BMSK                                      0x30000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_SPARE1_SHFT                                            0x1c
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_FUSE_SMT_PERM_ENABLE_BMSK                         0x8000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_FUSE_SMT_PERM_ENABLE_SHFT                              0x1b
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_FUSE_RCP_BYPASS_ENABLE_BMSK                       0x4000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_FUSE_RCP_BYPASS_ENABLE_SHFT                            0x1a
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_MSMC_SMMU_BYPASS_DISABLE_BMSK                     0x2000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_MSMC_SMMU_BYPASS_DISABLE_SHFT                          0x19
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_MSMC_GPIO_SLIMBUS_PD_DISABLE_BMSK                 0x1000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_MSMC_GPIO_SLIMBUS_PD_DISABLE_SHFT                      0x18
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_FUSE_PCIE20_RC_DISABLE_BMSK                        0x800000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_FUSE_PCIE20_RC_DISABLE_SHFT                            0x17
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_MSMC_PCIE_DISABLE_BMSK                             0x400000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_MSMC_PCIE_DISABLE_SHFT                                 0x16
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_NDINT_DISABLE_BMSK                                 0x200000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_NDINT_DISABLE_SHFT                                     0x15
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_DAP_DEVICEEN_DISABLE_BMSK                       0x100000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_DAP_DEVICEEN_DISABLE_SHFT                           0x14
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_DAP_SPNIDEN_DISABLE_BMSK                         0x80000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_DAP_SPNIDEN_DISABLE_SHFT                            0x13
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_DAP_SPIDEN_DISABLE_BMSK                          0x40000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_DAP_SPIDEN_DISABLE_SHFT                             0x12
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_DAP_NIDEN_DISABLE_BMSK                           0x20000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_DAP_NIDEN_DISABLE_SHFT                              0x11
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_DAP_DBGEN_DISABLE_BMSK                           0x10000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_DAP_DBGEN_DISABLE_SHFT                              0x10
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_APPS_SPNIDEN_DISABLE_BMSK                         0x8000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_APPS_SPNIDEN_DISABLE_SHFT                            0xf
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_APPS_SPIDEN_DISABLE_BMSK                          0x4000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_APPS_SPIDEN_DISABLE_SHFT                             0xe
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_APPS_NIDEN_DISABLE_BMSK                           0x2000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_APPS_NIDEN_DISABLE_SHFT                              0xd
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_APPS_DBGEN_DISABLE_BMSK                           0x1000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_APPS_DBGEN_DISABLE_SHFT                              0xc
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_SPARE1_DISABLE_BMSK                                0x800
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_SPARE1_DISABLE_SHFT                                  0xb
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_SPARE0_DISABLE_BMSK                                0x400
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_SPARE0_DISABLE_SHFT                                  0xa
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_VENUS_0_DBGEN_DISABLE_BMSK                         0x200
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_VENUS_0_DBGEN_DISABLE_SHFT                           0x9
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_RPM_DAPEN_DISABLE_BMSK                             0x100
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_RPM_DAPEN_DISABLE_SHFT                               0x8
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_RPM_WCSS_NIDEN_DISABLE_BMSK                         0x80
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_RPM_WCSS_NIDEN_DISABLE_SHFT                          0x7
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_RPM_DBGEN_DISABLE_BMSK                              0x40
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_RPM_DBGEN_DISABLE_SHFT                               0x6
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_WCSS_DBGEN_DISABLE_BMSK                             0x20
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_WCSS_DBGEN_DISABLE_SHFT                              0x5
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_MSS_NIDEN_DISABLE_BMSK                              0x10
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_MSS_NIDEN_DISABLE_SHFT                               0x4
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_MSS_DBGEN_DISABLE_BMSK                               0x8
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QC_MSS_DBGEN_DISABLE_SHFT                               0x3
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QDI_SPMI_DISABLE_BMSK                                   0x4
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_QDI_SPMI_DISABLE_SHFT                                   0x2
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_SM_BIST_DISABLE_BMSK                                    0x2
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_SM_BIST_DISABLE_SHFT                                    0x1
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_TIC_DISABLE_BMSK                                        0x1
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_LSB_TIC_DISABLE_SHFT                                        0x0

#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000404c)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_PHYS                                             (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x0000404c)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_ADDR, HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_RMSK)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_SEC_TAP_ACCESS_DISABLE_BMSK                      0xf8000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_SEC_TAP_ACCESS_DISABLE_SHFT                            0x1b
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_TAP_CJI_CORE_SEL_DISABLE_BMSK                     0x4000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_TAP_CJI_CORE_SEL_DISABLE_SHFT                          0x1a
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_TAP_INSTR_DISABLE_BMSK                            0x3ffc000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_TAP_INSTR_DISABLE_SHFT                                  0xe
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_SPARE1_BMSK                                          0x3c00
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_SPARE1_SHFT                                             0xa
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_MODEM_PBL_PATCH_VERSION_BMSK                          0x3e0
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_MODEM_PBL_PATCH_VERSION_SHFT                            0x5
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_APPS_PBL_PATCH_VERSION_BMSK                            0x1f
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW1_MSB_APPS_PBL_PATCH_VERSION_SHFT                             0x0

#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004050)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_PHYS                                             (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00004050)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_ADDR, HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_RMSK)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_TAP_GEN_SPARE_INSTR_DISABLE_31_0_BMSK            0xffffffff
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_LSB_TAP_GEN_SPARE_INSTR_DISABLE_31_0_SHFT                   0x0

#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004054)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_PHYS                                             (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00004054)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_ADDR, HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_RMSK)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_STACKED_MEMORY_ID_BMSK                           0xf0000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_STACKED_MEMORY_ID_SHFT                                 0x1c
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_APSS_BOOT_TRIGGER_DISABLE_BMSK                    0x8000000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_APSS_BOOT_TRIGGER_DISABLE_SHFT                         0x1b
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_MODEM_PBL_PLL_CTRL_BMSK                           0x7800000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_MODEM_PBL_PLL_CTRL_SHFT                                0x17
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_APPS_PBL_PLL_CTRL_BMSK                             0x780000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_APPS_PBL_PLL_CTRL_SHFT                                 0x13
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_APPS_BOOT_FSM_CFG_BMSK                              0x40000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_APPS_BOOT_FSM_CFG_SHFT                                 0x12
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_APPS_BOOT_FSM_DELAY_BMSK                            0x30000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_APPS_BOOT_FSM_DELAY_SHFT                               0x10
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_BOOT_ROM_PATCH_SEL_EN_USB_PHY_TUNE_BMSK              0x8000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_BOOT_ROM_PATCH_SEL_EN_USB_PHY_TUNE_SHFT                 0xf
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_USB_SS_ENABLE_BMSK                                   0x4000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_USB_SS_ENABLE_SHFT                                      0xe
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_APPS_PBL_BOOT_SPEED_BMSK                             0x3000
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_APPS_PBL_BOOT_SPEED_SHFT                                0xc
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_APPS_BOOT_FROM_ROM_BMSK                               0x800
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_APPS_BOOT_FROM_ROM_SHFT                                 0xb
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_MSA_ENA_BMSK                                          0x400
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_MSA_ENA_SHFT                                            0xa
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_FORCE_MSA_AUTH_EN_BMSK                                0x200
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_FORCE_MSA_AUTH_EN_SHFT                                  0x9
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_SPARE_8_7_BMSK                                        0x180
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_SPARE_8_7_SHFT                                          0x7
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_MODEM_BOOT_FROM_ROM_BMSK                               0x40
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_MODEM_BOOT_FROM_ROM_SHFT                                0x6
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_BOOT_ROM_PATCH_DISABLE_BMSK                            0x20
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_BOOT_ROM_PATCH_DISABLE_SHFT                             0x5
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_TAP_GEN_SPARE_INSTR_DISABLE_36_32_BMSK                 0x1f
#define HWIO_QFPROM_CORR_FEAT_CONFIG_ROW2_MSB_TAP_GEN_SPARE_INSTR_DISABLE_36_32_SHFT                  0x0

#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n)                               (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004058 + 0x8 * (n))
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_LSB_PHYS(n)                               (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00004058 + 0x8 * (n))
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_LSB_RMSK                                  0xffffffff
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_LSB_MAXn                                           3
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_LSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n), HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_LSB_RMSK)
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_LSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n), mask)
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_LSB_KEY_DATA0_BMSK                        0xffffffff
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_LSB_KEY_DATA0_SHFT                               0x0

#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n)                               (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000405c + 0x8 * (n))
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_MSB_PHYS(n)                               (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x0000405c + 0x8 * (n))
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_MSB_RMSK                                  0xffffffff
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_MSB_MAXn                                           3
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_MSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n), HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_MSB_RMSK)
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_MSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n), mask)
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_MSB_KEY_DATA1_BMSK                        0xffffffff
#define HWIO_QFPROM_CORR_PRI_KEY_DERIVATION_KEY_ROWn_MSB_KEY_DATA1_SHFT                               0x0

#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n)                               (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004078 + 0x8 * (n))
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_LSB_PHYS(n)                               (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00004078 + 0x8 * (n))
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_LSB_RMSK                                  0xffffffff
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_LSB_MAXn                                           3
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_LSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n), HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_LSB_RMSK)
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_LSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_LSB_ADDR(n), mask)
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_LSB_KEY_DATA0_BMSK                        0xffffffff
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_LSB_KEY_DATA0_SHFT                               0x0

#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n)                               (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000407c + 0x8 * (n))
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_MSB_PHYS(n)                               (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x0000407c + 0x8 * (n))
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_MSB_RMSK                                  0xffffffff
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_MSB_MAXn                                           3
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_MSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n), HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_MSB_RMSK)
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_MSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_MSB_ADDR(n), mask)
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_MSB_KEY_DATA1_BMSK                        0xffffffff
#define HWIO_QFPROM_CORR_SEC_KEY_DERIVATION_KEY_ROWn_MSB_KEY_DATA1_SHFT                               0x0

#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_LSB_ADDR                                            (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004098)
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_LSB_PHYS                                            (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00004098)
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_LSB_RMSK                                            0xffffffff
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_LSB_ADDR, HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_LSB_RMSK)
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_LSB_SEC_BOOT4_BMSK                                  0xff000000
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_LSB_SEC_BOOT4_SHFT                                        0x18
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_LSB_SEC_BOOT3_BMSK                                    0xff0000
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_LSB_SEC_BOOT3_SHFT                                        0x10
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_LSB_SEC_BOOT2_BMSK                                      0xff00
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_LSB_SEC_BOOT2_SHFT                                         0x8
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_LSB_SEC_BOOT1_BMSK                                        0xff
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_LSB_SEC_BOOT1_SHFT                                         0x0

#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_MSB_ADDR                                            (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000409c)
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_MSB_PHYS                                            (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x0000409c)
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_MSB_RMSK                                              0xffffff
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_MSB_ADDR, HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_MSB_RMSK)
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_MSB_SEC_BOOT7_BMSK                                    0xff0000
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_MSB_SEC_BOOT7_SHFT                                        0x10
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_MSB_SEC_BOOT6_BMSK                                      0xff00
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_MSB_SEC_BOOT6_SHFT                                         0x8
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_MSB_SEC_BOOT5_BMSK                                        0xff
#define HWIO_QFPROM_CORR_OEM_SEC_BOOT_ROW0_MSB_SEC_BOOT5_SHFT                                         0x0

#define HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_LSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x000040a0)
#define HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_LSB_PHYS                                             (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000040a0)
#define HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_LSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_LSB_ADDR, HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_LSB_RMSK)
#define HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_LSB_SEC_BOOT4_BMSK                                   0xff000000
#define HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_LSB_SEC_BOOT4_SHFT                                         0x18
#define HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_LSB_SEC_BOOT3_BMSK                                     0xff0000
#define HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_LSB_SEC_BOOT3_SHFT                                         0x10
#define HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_LSB_SEC_BOOT2_BMSK                                       0xff00
#define HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_LSB_SEC_BOOT2_SHFT                                          0x8
#define HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_LSB_SEC_BOOT1_BMSK                                         0xff
#define HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_LSB_SEC_BOOT1_SHFT                                          0x0

#define HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_MSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x000040a4)
#define HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_MSB_PHYS                                             (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000040a4)
#define HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_MSB_RMSK                                               0xffffff
#define HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_MSB_ADDR, HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_MSB_RMSK)
#define HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_MSB_SEC_BOOT7_BMSK                                     0xff0000
#define HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_MSB_SEC_BOOT7_SHFT                                         0x10
#define HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_MSB_SEC_BOOT6_BMSK                                       0xff00
#define HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_MSB_SEC_BOOT6_SHFT                                          0x8
#define HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_MSB_SEC_BOOT5_BMSK                                         0xff
#define HWIO_QFPROM_CORR_QC_SEC_BOOT_ROW0_MSB_SEC_BOOT5_SHFT                                          0x0

#define HWIO_QFPROM_CORR_PK_HASH_ROWn_LSB_ADDR(n)                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x000040a8 + 0x8 * (n))
#define HWIO_QFPROM_CORR_PK_HASH_ROWn_LSB_PHYS(n)                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000040a8 + 0x8 * (n))
#define HWIO_QFPROM_CORR_PK_HASH_ROWn_LSB_RMSK                                                 0xffffffff
#define HWIO_QFPROM_CORR_PK_HASH_ROWn_LSB_MAXn                                                          3
#define HWIO_QFPROM_CORR_PK_HASH_ROWn_LSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_CORR_PK_HASH_ROWn_LSB_ADDR(n), HWIO_QFPROM_CORR_PK_HASH_ROWn_LSB_RMSK)
#define HWIO_QFPROM_CORR_PK_HASH_ROWn_LSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_CORR_PK_HASH_ROWn_LSB_ADDR(n), mask)
#define HWIO_QFPROM_CORR_PK_HASH_ROWn_LSB_HASH_DATA0_BMSK                                      0xffffffff
#define HWIO_QFPROM_CORR_PK_HASH_ROWn_LSB_HASH_DATA0_SHFT                                             0x0

#define HWIO_QFPROM_CORR_PK_HASH_ROWn_MSB_ADDR(n)                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x000040ac + 0x8 * (n))
#define HWIO_QFPROM_CORR_PK_HASH_ROWn_MSB_PHYS(n)                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000040ac + 0x8 * (n))
#define HWIO_QFPROM_CORR_PK_HASH_ROWn_MSB_RMSK                                                   0xffffff
#define HWIO_QFPROM_CORR_PK_HASH_ROWn_MSB_MAXn                                                          3
#define HWIO_QFPROM_CORR_PK_HASH_ROWn_MSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_CORR_PK_HASH_ROWn_MSB_ADDR(n), HWIO_QFPROM_CORR_PK_HASH_ROWn_MSB_RMSK)
#define HWIO_QFPROM_CORR_PK_HASH_ROWn_MSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_CORR_PK_HASH_ROWn_MSB_ADDR(n), mask)
#define HWIO_QFPROM_CORR_PK_HASH_ROWn_MSB_HASH_DATA1_BMSK                                        0xffffff
#define HWIO_QFPROM_CORR_PK_HASH_ROWn_MSB_HASH_DATA1_SHFT                                             0x0

#define HWIO_QFPROM_CORR_PK_HASH_ROW4_LSB_ADDR                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x000040c8)
#define HWIO_QFPROM_CORR_PK_HASH_ROW4_LSB_PHYS                                                 (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000040c8)
#define HWIO_QFPROM_CORR_PK_HASH_ROW4_LSB_RMSK                                                 0xffffffff
#define HWIO_QFPROM_CORR_PK_HASH_ROW4_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_PK_HASH_ROW4_LSB_ADDR, HWIO_QFPROM_CORR_PK_HASH_ROW4_LSB_RMSK)
#define HWIO_QFPROM_CORR_PK_HASH_ROW4_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_PK_HASH_ROW4_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_PK_HASH_ROW4_LSB_HASH_DATA0_BMSK                                      0xffffffff
#define HWIO_QFPROM_CORR_PK_HASH_ROW4_LSB_HASH_DATA0_SHFT                                             0x0

#define HWIO_QFPROM_CORR_PK_HASH_ROW4_MSB_ADDR                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x000040cc)
#define HWIO_QFPROM_CORR_PK_HASH_ROW4_MSB_PHYS                                                 (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000040cc)
#define HWIO_QFPROM_CORR_PK_HASH_ROW4_MSB_RMSK                                                   0xffffff
#define HWIO_QFPROM_CORR_PK_HASH_ROW4_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_PK_HASH_ROW4_MSB_ADDR, HWIO_QFPROM_CORR_PK_HASH_ROW4_MSB_RMSK)
#define HWIO_QFPROM_CORR_PK_HASH_ROW4_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_PK_HASH_ROW4_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_PK_HASH_ROW4_MSB_QFPROM_CORR_PK_HASH_ROW4_MSB_BMSK                      0xffffff
#define HWIO_QFPROM_CORR_PK_HASH_ROW4_MSB_QFPROM_CORR_PK_HASH_ROW4_MSB_SHFT                           0x0

#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000040d0)
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_PHYS                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000040d0)
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW0_LSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW0_LSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW0_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_TSENS2_POINT1_BMSK                                     0xf0000000
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_TSENS2_POINT1_SHFT                                           0x1c
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_TSENS1_POINT1_BMSK                                      0xf000000
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_TSENS1_POINT1_SHFT                                           0x18
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_TSENS0_POINT1_BMSK                                       0xf00000
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_TSENS0_POINT1_SHFT                                           0x14
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_TSENS_BASE1_BMSK                                          0xffc00
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_TSENS_BASE1_SHFT                                              0xa
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_TSENS_BASE0_BMSK                                            0x3ff
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_TSENS_BASE0_SHFT                                              0x0

#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000040d4)
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_PHYS                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000040d4)
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW0_MSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW0_MSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW0_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_FEC_BMSK                                               0xff000000
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_FEC_SHFT                                                     0x18
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_USB_PHY_TUNING_BMSK                                      0xf00000
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_USB_PHY_TUNING_SHFT                                          0x14
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_SPARE_BMSK                                                0xff800
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_SPARE_SHFT                                                    0xb
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_TSENS_CAL_SEL_BMSK                                          0x700
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_TSENS_CAL_SEL_SHFT                                            0x8
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_TSENS4_POINT1_BMSK                                           0xf0
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_TSENS4_POINT1_SHFT                                            0x4
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_TSENS3_POINT1_BMSK                                            0xf
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_TSENS3_POINT1_SHFT                                            0x0

#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000040d8)
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_PHYS                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000040d8)
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW1_LSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW1_LSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW1_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_CPR0_TARG_VOLT_OFFSET_TUR_BIT_0_BMSK                   0x80000000
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_CPR0_TARG_VOLT_OFFSET_TUR_BIT_0_SHFT                         0x1f
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_CPR0_TARG_VOLT_SVS2_BMSK                               0x7c000000
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_CPR0_TARG_VOLT_SVS2_SHFT                                     0x1a
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_CPR0_TARG_VOLT_OFFSET_TUR_BIT_1_BMSK                    0x2000000
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_CPR0_TARG_VOLT_OFFSET_TUR_BIT_1_SHFT                         0x19
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_CPR0_TARG_VOLT_SVS_BMSK                                 0x1f00000
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_CPR0_TARG_VOLT_SVS_SHFT                                      0x14
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_CPR0_TARG_VOLT_OFFSET_TUR_BIT_2_BMSK                      0x80000
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_CPR0_TARG_VOLT_OFFSET_TUR_BIT_2_SHFT                         0x13
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_CPR0_TARG_VOLT_NOM_BMSK                                   0x7c000
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_CPR0_TARG_VOLT_NOM_SHFT                                       0xe
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_CPR0_TARG_VOLT_OFFSET_TUR_BIT_3_BMSK                       0x2000
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_CPR0_TARG_VOLT_OFFSET_TUR_BIT_3_SHFT                          0xd
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_CPR0_TARG_VOLT_TUR_BMSK                                    0x1f00
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_CPR0_TARG_VOLT_TUR_SHFT                                       0x8
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_CPR1_TARG_VOLT_OFFSET_SVS2_BMSK                              0x80
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_CPR1_TARG_VOLT_OFFSET_SVS2_SHFT                               0x7
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_CPR2_TARG_VOLT_OFFSET_NOM_BMSK                               0x78
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_CPR2_TARG_VOLT_OFFSET_NOM_SHFT                                0x3
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_CPR2_TARG_VOLT_OFFSET_TUR_BMSK                                0x7
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_CPR2_TARG_VOLT_OFFSET_TUR_SHFT                                0x0

#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000040dc)
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_PHYS                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000040dc)
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW1_MSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW1_MSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW1_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_FEC_BMSK                                               0xff000000
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_FEC_SHFT                                                     0x18
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_CPR0_TARG_VOLT_OFFSET_NOM_BIT_0_BMSK                     0x800000
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_CPR0_TARG_VOLT_OFFSET_NOM_BIT_0_SHFT                         0x17
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_CPR1_TARG_VOLT_SVS2_BMSK                                 0x7c0000
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_CPR1_TARG_VOLT_SVS2_SHFT                                     0x12
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_CPR0_TARG_VOLT_OFFSET_NOM_BIT_1_BMSK                      0x20000
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_CPR0_TARG_VOLT_OFFSET_NOM_BIT_1_SHFT                         0x11
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_CPR1_TARG_VOLT_SVS_BMSK                                   0x1f000
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_CPR1_TARG_VOLT_SVS_SHFT                                       0xc
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_CPR0_TARG_VOLT_OFFSET_NOM_BIT_2_BMSK                        0x800
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_CPR0_TARG_VOLT_OFFSET_NOM_BIT_2_SHFT                          0xb
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_CPR1_TARG_VOLT_NOM_BMSK                                     0x7c0
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_CPR1_TARG_VOLT_NOM_SHFT                                       0x6
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_CPR0_TARG_VOLT_OFFSET_NOM_BIT_3_BMSK                         0x20
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_CPR0_TARG_VOLT_OFFSET_NOM_BIT_3_SHFT                          0x5
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_CPR1_TARG_VOLT_TUR_BMSK                                      0x1f
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_CPR1_TARG_VOLT_TUR_SHFT                                       0x0

#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000040e0)
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_PHYS                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000040e0)
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW2_LSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW2_LSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW2_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_LDO_TARGET1_BMSK                                       0xf0000000
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_LDO_TARGET1_SHFT                                             0x1c
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_LDO_TARGET0_BMSK                                        0xfc00000
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_LDO_TARGET0_SHFT                                             0x16
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_LDO_ENABLE_BMSK                                          0x200000
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_LDO_ENABLE_SHFT                                              0x15
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_CPR0_TARG_VOLT_OFFSET_SVS2_BMSK                          0x1e0000
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_CPR0_TARG_VOLT_OFFSET_SVS2_SHFT                              0x11
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_CPR0_TARG_VOLT_OFFSET_SVS_BMSK                            0x1c000
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_CPR0_TARG_VOLT_OFFSET_SVS_SHFT                                0xe
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_CPR2_TARG_VOLT_NOM_BMSK                                    0x3e00
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_CPR2_TARG_VOLT_NOM_SHFT                                       0x9
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_CPR0_TARG_VOLT_OFFSET_SVS_BIT_3_BMSK                        0x100
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_CPR0_TARG_VOLT_OFFSET_SVS_BIT_3_SHFT                          0x8
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_CPR2_TARG_VOLT_TUR_BMSK                                      0xf8
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_CPR2_TARG_VOLT_TUR_SHFT                                       0x3
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_CPR_REV_BMSK                                                  0x7
#define HWIO_QFPROM_CORR_CALIB_ROW2_LSB_CPR_REV_SHFT                                                  0x0

#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000040e4)
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_PHYS                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000040e4)
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW2_MSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW2_MSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW2_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_FEC_BMSK                                               0xff000000
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_FEC_SHFT                                                     0x18
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_CPR1_TARG_VOLT_OFFSET_SVS2_BMSK                          0xc00000
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_CPR1_TARG_VOLT_OFFSET_SVS2_SHFT                              0x16
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_CPR1_TARG_VOLT_OFFSET_SVS_BMSK                           0x3c0000
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_CPR1_TARG_VOLT_OFFSET_SVS_SHFT                               0x12
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_CPR1_TARG_VOLT_OFFSET_NOM_BMSK                            0x3c000
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_CPR1_TARG_VOLT_OFFSET_NOM_SHFT                                0xe
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_CPR1_TARG_VOLT_OFFSET_TUR_BMSK                             0x3c00
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_CPR1_TARG_VOLT_OFFSET_TUR_SHFT                                0xa
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_SPARE_BMSK                                                  0x300
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_SPARE_SHFT                                                    0x8
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_GNSS_ADC_CH0_VCM_BMSK                                        0xc0
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_GNSS_ADC_CH0_VCM_SHFT                                         0x6
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_GNSS_ADC_CH0_LDO_BMSK                                        0x30
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_GNSS_ADC_CH0_LDO_SHFT                                         0x4
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_GNSS_ADC_CH0_VREF_BMSK                                        0xc
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_GNSS_ADC_CH0_VREF_SHFT                                        0x2
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_LDO_TARGET1_BMSK                                              0x3
#define HWIO_QFPROM_CORR_CALIB_ROW2_MSB_LDO_TARGET1_SHFT                                              0x0

#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000040e8)
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_PHYS                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000040e8)
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW3_LSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW3_LSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW3_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_ECS_BHS_SLOPE_BMSK                                     0x80000000
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_ECS_BHS_SLOPE_SHFT                                           0x1f
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_ECS_BHS_INTERCEPT_BMSK                                 0x7fe00000
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_ECS_BHS_INTERCEPT_SHFT                                       0x15
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_ECS_LDO_SLOPE_BMSK                                       0x1ff800
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_ECS_LDO_SLOPE_SHFT                                            0xb
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_ECS_LDO_INTERCEPT_BMSK                                      0x7fe
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_ECS_LDO_INTERCEPT_SHFT                                        0x1
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_ECS_ENABLE_BMSK                                               0x1
#define HWIO_QFPROM_CORR_CALIB_ROW3_LSB_ECS_ENABLE_SHFT                                               0x0

#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000040ec)
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_PHYS                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000040ec)
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW3_MSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW3_MSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW3_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_FEC_BMSK                                               0xff000000
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_FEC_SHFT                                                     0x18
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_G_B1_BMSK                                                0xe00000
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_G_B1_SHFT                                                    0x15
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_SAR_LDO_ERR0_BMSK                                        0x180000
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_SAR_LDO_ERR0_SHFT                                            0x13
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_CPR2_TARG_VOLT_OFFSET_TUR_BIT_3_BMSK                      0x40000
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_CPR2_TARG_VOLT_OFFSET_TUR_BIT_3_SHFT                         0x12
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_CPR1_TARG_VOLT_OFFSET_SVS2_BIT_3_BMSK                     0x20000
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_CPR1_TARG_VOLT_OFFSET_SVS2_BIT_3_SHFT                        0x11
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_ECS_MISC_BMSK                                             0x1fe00
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_ECS_MISC_SHFT                                                 0x9
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_ECS_BHS_SLOPE_BMSK                                          0x1ff
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_ECS_BHS_SLOPE_SHFT                                            0x0

#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000040f0)
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_PHYS                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000040f0)
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW4_LSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW4_LSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW4_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_PH_B1M2_BMSK                                           0xc0000000
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_PH_B1M2_SHFT                                                 0x1e
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_PH_B1M1_BMSK                                           0x38000000
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_PH_B1M1_SHFT                                                 0x1b
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_PH_B1M0_BMSK                                            0x7000000
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_PH_B1M0_SHFT                                                 0x18
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_Q6SS0_LDO_VREF_TRIM_BMSK                                 0xf80000
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_Q6SS0_LDO_VREF_TRIM_SHFT                                     0x13
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_CLK_LDO_ERR0_BMSK                                         0x60000
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_CLK_LDO_ERR0_SHFT                                            0x11
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_VREF_ERR_B0_BMSK                                          0x18000
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_VREF_ERR_B0_SHFT                                              0xf
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_PH_B0M3_BMSK                                               0x7000
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_PH_B0M3_SHFT                                                  0xc
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_PH_B0M2_BMSK                                                0xe00
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_PH_B0M2_SHFT                                                  0x9
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_PH_B0M1_BMSK                                                0x1c0
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_PH_B0M1_SHFT                                                  0x6
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_PH_B0M0_BMSK                                                 0x38
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_PH_B0M0_SHFT                                                  0x3
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_G_B0_BMSK                                                     0x7
#define HWIO_QFPROM_CORR_CALIB_ROW4_LSB_G_B0_SHFT                                                     0x0

#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000040f4)
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_PHYS                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000040f4)
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW4_MSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW4_MSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW4_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_FEC_BMSK                                               0xff000000
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_FEC_SHFT                                                     0x18
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_PH_B2M3_BMSK                                             0xc00000
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_PH_B2M3_SHFT                                                 0x16
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_PH_B2M2_BMSK                                             0x380000
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_PH_B2M2_SHFT                                                 0x13
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_PH_B2M1_BMSK                                              0x70000
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_PH_B2M1_SHFT                                                 0x10
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_PH_B2M0_BMSK                                               0xe000
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_PH_B2M0_SHFT                                                  0xd
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_G_B2_BMSK                                                  0x1c00
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_G_B2_SHFT                                                     0xa
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_SAR_LDO_ERR1_BMSK                                           0x300
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_SAR_LDO_ERR1_SHFT                                             0x8
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_CLK_LDO_ERR1_BMSK                                            0xc0
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_CLK_LDO_ERR1_SHFT                                             0x6
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_VREF_ERR_B1_BMSK                                             0x30
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_VREF_ERR_B1_SHFT                                              0x4
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_PH_B1M3_BMSK                                                  0xe
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_PH_B1M3_SHFT                                                  0x1
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_PH_B1M2_BMSK                                                  0x1
#define HWIO_QFPROM_CORR_CALIB_ROW4_MSB_PH_B1M2_SHFT                                                  0x0

#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000040f8)
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_PHYS                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000040f8)
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW5_LSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW5_LSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW5_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_PH_B4M0_BMSK                                           0x80000000
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_PH_B4M0_SHFT                                                 0x1f
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_G_B4_BMSK                                              0x70000000
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_G_B4_SHFT                                                    0x1c
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_SAR_LDO_ERR3_BMSK                                       0xc000000
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_SAR_LDO_ERR3_SHFT                                            0x1a
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_CLK_LDO_ERR3_BMSK                                       0x3000000
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_CLK_LDO_ERR3_SHFT                                            0x18
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_VREF_ERR_B3_BMSK                                         0xc00000
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_VREF_ERR_B3_SHFT                                             0x16
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_PH_B3M3_BMSK                                             0x380000
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_PH_B3M3_SHFT                                                 0x13
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_PH_B3M2_BMSK                                              0x70000
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_PH_B3M2_SHFT                                                 0x10
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_PH_B3M1_BMSK                                               0xe000
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_PH_B3M1_SHFT                                                  0xd
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_PH_B3M0_BMSK                                               0x1c00
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_PH_B3M0_SHFT                                                  0xa
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_G_B3_BMSK                                                   0x380
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_G_B3_SHFT                                                     0x7
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_SAR_LDO_ERR2_BMSK                                            0x60
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_SAR_LDO_ERR2_SHFT                                             0x5
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_CLK_LDO_ERR2_BMSK                                            0x18
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_CLK_LDO_ERR2_SHFT                                             0x3
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_VREF_ERR_B2_BMSK                                              0x6
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_VREF_ERR_B2_SHFT                                              0x1
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_PH_B2M3_BMSK                                                  0x1
#define HWIO_QFPROM_CORR_CALIB_ROW5_LSB_PH_B2M3_SHFT                                                  0x0

#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x000040fc)
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_PHYS                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000040fc)
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW5_MSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW5_MSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW5_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_FEC_BMSK                                               0xff000000
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_FEC_SHFT                                                     0x18
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_PH_B5M1_BMSK                                             0x800000
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_PH_B5M1_SHFT                                                 0x17
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_PH_B5M0_BMSK                                             0x700000
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_PH_B5M0_SHFT                                                 0x14
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_G_B5_BMSK                                                 0xe0000
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_G_B5_SHFT                                                    0x11
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_SAR_LDO_ERR4_BMSK                                         0x18000
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_SAR_LDO_ERR4_SHFT                                             0xf
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_CLK_LDO_ERR4_BMSK                                          0x6000
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_CLK_LDO_ERR4_SHFT                                             0xd
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_VREF_ERR_B4_BMSK                                           0x1800
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_VREF_ERR_B4_SHFT                                              0xb
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_PH_B4M3_BMSK                                                0x700
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_PH_B4M3_SHFT                                                  0x8
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_PH_B4M2_BMSK                                                 0xe0
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_PH_B4M2_SHFT                                                  0x5
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_PH_B4M1_BMSK                                                 0x1c
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_PH_B4M1_SHFT                                                  0x2
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_PH_B4M0_BMSK                                                  0x3
#define HWIO_QFPROM_CORR_CALIB_ROW5_MSB_PH_B4M0_SHFT                                                  0x0

#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004100)
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_PHYS                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00004100)
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW6_LSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW6_LSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW6_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_CLK_LDO_ERR6_BMSK                                      0x80000000
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_CLK_LDO_ERR6_SHFT                                            0x1f
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_VREF_ERR_B6_BMSK                                       0x60000000
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_VREF_ERR_B6_SHFT                                             0x1d
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_PH_B6M3_BMSK                                           0x1c000000
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_PH_B6M3_SHFT                                                 0x1a
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_PH_B6M2_BMSK                                            0x3800000
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_PH_B6M2_SHFT                                                 0x17
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_PH_B6M1_BMSK                                             0x700000
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_PH_B6M1_SHFT                                                 0x14
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_PH_B6M0_BMSK                                              0xe0000
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_PH_B6M0_SHFT                                                 0x11
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_G_B6_BMSK                                                 0x1c000
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_G_B6_SHFT                                                     0xe
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_SAR_LDO_ERR5_BMSK                                          0x3000
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_SAR_LDO_ERR5_SHFT                                             0xc
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_CLK_LDO_ERR5_BMSK                                           0xc00
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_CLK_LDO_ERR5_SHFT                                             0xa
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_VREF_ERR_B5_BMSK                                            0x300
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_VREF_ERR_B5_SHFT                                              0x8
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_PH_B5M3_BMSK                                                 0xe0
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_PH_B5M3_SHFT                                                  0x5
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_PH_B5M2_BMSK                                                 0x1c
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_PH_B5M2_SHFT                                                  0x2
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_PH_B5M1_BMSK                                                  0x3
#define HWIO_QFPROM_CORR_CALIB_ROW6_LSB_PH_B5M1_SHFT                                                  0x0

#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_ADDR                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004104)
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_PHYS                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00004104)
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_RMSK                                                   0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW6_MSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW6_MSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW6_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_FEC_BMSK                                               0xff000000
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_FEC_SHFT                                                     0x18
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_MODEM_TXDAC1_CALIB_BMSK                                  0xff0000
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_MODEM_TXDAC1_CALIB_SHFT                                      0x10
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_MODEM_TXDAC0_CALIB_BMSK                                    0xff00
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_MODEM_TXDAC0_CALIB_SHFT                                       0x8
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_MODEM_TXDAC1_CALIB_AVG_ERROR_BMSK                            0x80
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_MODEM_TXDAC1_CALIB_AVG_ERROR_SHFT                             0x7
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_MODEM_TXDAC0_CALIB_AVG_ERROR_BMSK                            0x40
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_MODEM_TXDAC0_CALIB_AVG_ERROR_SHFT                             0x6
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_MODEM_TXDAC_0_1_FUSEFLAG_BMSK                                0x20
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_MODEM_TXDAC_0_1_FUSEFLAG_SHFT                                 0x5
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_MODEM_TXDAC1_CALIB_OVERFLOW_BMSK                             0x10
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_MODEM_TXDAC1_CALIB_OVERFLOW_SHFT                              0x4
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_MODEM_TXDAC0_CALIB_OVERFLOW_BMSK                              0x8
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_MODEM_TXDAC0_CALIB_OVERFLOW_SHFT                              0x3
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_SAR_LDO_ERR6_BMSK                                             0x6
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_SAR_LDO_ERR6_SHFT                                             0x1
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_CLK_LDO_ERR6_BMSK                                             0x1
#define HWIO_QFPROM_CORR_CALIB_ROW6_MSB_CLK_LDO_ERR6_SHFT                                             0x0

#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_LSB_ADDR(n)                                           (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004108 + 0x8 * (n))
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_LSB_PHYS(n)                                           (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00004108 + 0x8 * (n))
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_LSB_RMSK                                              0xffffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_LSB_MAXn                                                      15
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_LSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_LSB_ADDR(n), HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_LSB_RMSK)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_LSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_LSB_ADDR(n), mask)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_LSB_REDUN_DATA_BMSK                                   0xffffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_LSB_REDUN_DATA_SHFT                                          0x0

#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_MSB_ADDR(n)                                           (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000410c + 0x8 * (n))
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_MSB_PHYS(n)                                           (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x0000410c + 0x8 * (n))
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_MSB_RMSK                                                0xffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_MSB_MAXn                                                      15
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_MSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_MSB_ADDR(n), HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_MSB_RMSK)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_MSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_MSB_ADDR(n), mask)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_MSB_REDUN_DATA_BMSK                                     0xffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROWn_MSB_REDUN_DATA_SHFT                                          0x0

#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW16_LSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004188)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW16_LSB_PHYS                                             (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00004188)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW16_LSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW16_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROW16_LSB_ADDR, HWIO_QFPROM_CORR_MEM_CONFIG_ROW16_LSB_RMSK)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW16_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROW16_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW16_LSB_MEM_ACCEL_BMSK                                   0xffffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW16_LSB_MEM_ACCEL_SHFT                                          0x0

#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW16_MSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000418c)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW16_MSB_PHYS                                             (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x0000418c)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW16_MSB_RMSK                                               0xffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW16_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROW16_MSB_ADDR, HWIO_QFPROM_CORR_MEM_CONFIG_ROW16_MSB_RMSK)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW16_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROW16_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW16_MSB_MEM_ACCEL_BMSK                                     0xffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW16_MSB_MEM_ACCEL_SHFT                                          0x0

#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_LSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004190)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_LSB_PHYS                                             (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00004190)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_LSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_LSB_ADDR, HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_LSB_RMSK)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_LSB_MEM_ACCEL_BMSK                                   0xffffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_LSB_MEM_ACCEL_SHFT                                          0x0

#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_MSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004194)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_MSB_PHYS                                             (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00004194)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_MSB_RMSK                                               0xffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_MSB_ADDR, HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_MSB_RMSK)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_MSB_MEM_ACCEL_BMSK                                     0xffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW17_MSB_MEM_ACCEL_SHFT                                          0x0

#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_LSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004198)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_LSB_PHYS                                             (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00004198)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_LSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_LSB_ADDR, HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_LSB_RMSK)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_LSB_MEM_ACCEL_BMSK                                   0xffffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_LSB_MEM_ACCEL_SHFT                                          0x0

#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_MSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000419c)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_MSB_PHYS                                             (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x0000419c)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_MSB_RMSK                                               0xffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_MSB_ADDR, HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_MSB_RMSK)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_MSB_MEM_ACCEL_BMSK                                     0xffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW18_MSB_MEM_ACCEL_SHFT                                          0x0

#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW19_LSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x000041a0)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW19_LSB_PHYS                                             (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000041a0)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW19_LSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW19_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROW19_LSB_ADDR, HWIO_QFPROM_CORR_MEM_CONFIG_ROW19_LSB_RMSK)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW19_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROW19_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW19_LSB_MEM_ACCEL_BMSK                                   0xffffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW19_LSB_MEM_ACCEL_SHFT                                          0x0

#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW19_MSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x000041a4)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW19_MSB_PHYS                                             (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000041a4)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW19_MSB_RMSK                                               0xffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW19_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROW19_MSB_ADDR, HWIO_QFPROM_CORR_MEM_CONFIG_ROW19_MSB_RMSK)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW19_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROW19_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW19_MSB_MEM_ACCEL_BMSK                                     0xffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW19_MSB_MEM_ACCEL_SHFT                                          0x0

#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW20_LSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x000041a8)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW20_LSB_PHYS                                             (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000041a8)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW20_LSB_RMSK                                             0xffffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW20_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROW20_LSB_ADDR, HWIO_QFPROM_CORR_MEM_CONFIG_ROW20_LSB_RMSK)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW20_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROW20_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW20_LSB_MEM_ACCEL_BMSK                                   0xffffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW20_LSB_MEM_ACCEL_SHFT                                          0x0

#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW20_MSB_ADDR                                             (SECURITY_CONTROL_CORE_REG_BASE      + 0x000041ac)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW20_MSB_PHYS                                             (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000041ac)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW20_MSB_RMSK                                               0xffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW20_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROW20_MSB_ADDR, HWIO_QFPROM_CORR_MEM_CONFIG_ROW20_MSB_RMSK)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW20_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_MEM_CONFIG_ROW20_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW20_MSB_MEM_ACCEL_BMSK                                     0xffffff
#define HWIO_QFPROM_CORR_MEM_CONFIG_ROW20_MSB_MEM_ACCEL_SHFT                                          0x0

#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_LSB_ADDR(n)                                            (SECURITY_CONTROL_CORE_REG_BASE      + 0x000041b0 + 0x8 * (n))
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_LSB_PHYS(n)                                            (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000041b0 + 0x8 * (n))
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_LSB_RMSK                                               0xffffffff
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_LSB_MAXn                                                       31
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_LSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_CORR_ROM_PATCH_ROWn_LSB_ADDR(n), HWIO_QFPROM_CORR_ROM_PATCH_ROWn_LSB_RMSK)
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_LSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_CORR_ROM_PATCH_ROWn_LSB_ADDR(n), mask)
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_LSB_PATCH_DATA_BMSK                                    0xffffffff
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_LSB_PATCH_DATA_SHFT                                           0x0

#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_MSB_ADDR(n)                                            (SECURITY_CONTROL_CORE_REG_BASE      + 0x000041b4 + 0x8 * (n))
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_MSB_PHYS(n)                                            (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000041b4 + 0x8 * (n))
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_MSB_RMSK                                                 0xffffff
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_MSB_MAXn                                                       31
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_MSB_INI(n)        \
        in_dword_masked(HWIO_QFPROM_CORR_ROM_PATCH_ROWn_MSB_ADDR(n), HWIO_QFPROM_CORR_ROM_PATCH_ROWn_MSB_RMSK)
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_MSB_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_CORR_ROM_PATCH_ROWn_MSB_ADDR(n), mask)
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_MSB_SPARE3_BMSK                                          0xfe0000
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_MSB_SPARE3_SHFT                                              0x11
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_MSB_PATCH_ADDR_BMSK                                       0x1fffe
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_MSB_PATCH_ADDR_SHFT                                           0x1
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_MSB_PATCH_EN_BMSK                                             0x1
#define HWIO_QFPROM_CORR_ROM_PATCH_ROWn_MSB_PATCH_EN_SHFT                                             0x0

#define HWIO_QFPROM_CORR_FEC_EN_LSB_ADDR                                                       (SECURITY_CONTROL_CORE_REG_BASE      + 0x000042b0)
#define HWIO_QFPROM_CORR_FEC_EN_LSB_PHYS                                                       (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000042b0)
#define HWIO_QFPROM_CORR_FEC_EN_LSB_RMSK                                                       0xffffffff
#define HWIO_QFPROM_CORR_FEC_EN_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_FEC_EN_LSB_ADDR, HWIO_QFPROM_CORR_FEC_EN_LSB_RMSK)
#define HWIO_QFPROM_CORR_FEC_EN_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_FEC_EN_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_FEC_EN_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_CORR_FEC_EN_LSB_ADDR,v)
#define HWIO_QFPROM_CORR_FEC_EN_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_CORR_FEC_EN_LSB_ADDR,m,v,HWIO_QFPROM_CORR_FEC_EN_LSB_IN)
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION31_FEC_EN_BMSK                                       0x80000000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION31_FEC_EN_SHFT                                             0x1f
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION30_FEC_EN_BMSK                                       0x40000000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION30_FEC_EN_SHFT                                             0x1e
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION29_FEC_EN_BMSK                                       0x20000000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION29_FEC_EN_SHFT                                             0x1d
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION28_FEC_EN_BMSK                                       0x10000000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION28_FEC_EN_SHFT                                             0x1c
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION27_FEC_EN_BMSK                                        0x8000000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION27_FEC_EN_SHFT                                             0x1b
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION26_FEC_EN_BMSK                                        0x4000000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION26_FEC_EN_SHFT                                             0x1a
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION25_FEC_EN_BMSK                                        0x2000000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION25_FEC_EN_SHFT                                             0x19
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION24_FEC_EN_BMSK                                        0x1000000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION24_FEC_EN_SHFT                                             0x18
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION23_FEC_EN_BMSK                                         0x800000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION23_FEC_EN_SHFT                                             0x17
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION22_FEC_EN_BMSK                                         0x400000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION22_FEC_EN_SHFT                                             0x16
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION21_FEC_EN_BMSK                                         0x200000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION21_FEC_EN_SHFT                                             0x15
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION20_FEC_EN_BMSK                                         0x100000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION20_FEC_EN_SHFT                                             0x14
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION19_FEC_EN_BMSK                                          0x80000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION19_FEC_EN_SHFT                                             0x13
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION18_FEC_EN_BMSK                                          0x40000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION18_FEC_EN_SHFT                                             0x12
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION17_FEC_EN_BMSK                                          0x20000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION17_FEC_EN_SHFT                                             0x11
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION16_FEC_EN_BMSK                                          0x10000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION16_FEC_EN_SHFT                                             0x10
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION15_FEC_EN_BMSK                                           0x8000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION15_FEC_EN_SHFT                                              0xf
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION14_FEC_EN_BMSK                                           0x4000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION14_FEC_EN_SHFT                                              0xe
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION13_FEC_EN_BMSK                                           0x2000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION13_FEC_EN_SHFT                                              0xd
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION12_FEC_EN_BMSK                                           0x1000
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION12_FEC_EN_SHFT                                              0xc
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION11_FEC_EN_BMSK                                            0x800
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION11_FEC_EN_SHFT                                              0xb
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION10_FEC_EN_BMSK                                            0x400
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION10_FEC_EN_SHFT                                              0xa
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION9_FEC_EN_BMSK                                             0x200
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION9_FEC_EN_SHFT                                               0x9
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION8_FEC_EN_BMSK                                             0x100
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION8_FEC_EN_SHFT                                               0x8
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION7_FEC_EN_BMSK                                              0x80
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION7_FEC_EN_SHFT                                               0x7
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION6_FEC_EN_BMSK                                              0x40
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION6_FEC_EN_SHFT                                               0x6
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION5_FEC_EN_BMSK                                              0x20
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION5_FEC_EN_SHFT                                               0x5
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION4_FEC_EN_BMSK                                              0x10
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION4_FEC_EN_SHFT                                               0x4
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION3_FEC_EN_BMSK                                               0x8
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION3_FEC_EN_SHFT                                               0x3
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION2_FEC_EN_BMSK                                               0x4
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION2_FEC_EN_SHFT                                               0x2
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION1_FEC_EN_BMSK                                               0x2
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION1_FEC_EN_SHFT                                               0x1
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION0_FEC_EN_BMSK                                               0x1
#define HWIO_QFPROM_CORR_FEC_EN_LSB_REGION0_FEC_EN_SHFT                                               0x0

#define HWIO_QFPROM_CORR_FEC_EN_MSB_ADDR                                                       (SECURITY_CONTROL_CORE_REG_BASE      + 0x000042b4)
#define HWIO_QFPROM_CORR_FEC_EN_MSB_PHYS                                                       (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000042b4)
#define HWIO_QFPROM_CORR_FEC_EN_MSB_RMSK                                                       0xffffffff
#define HWIO_QFPROM_CORR_FEC_EN_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_FEC_EN_MSB_ADDR, HWIO_QFPROM_CORR_FEC_EN_MSB_RMSK)
#define HWIO_QFPROM_CORR_FEC_EN_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_FEC_EN_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_FEC_EN_MSB_FEC_EN_REDUNDANCY_BMSK                                     0xffffffff
#define HWIO_QFPROM_CORR_FEC_EN_MSB_FEC_EN_REDUNDANCY_SHFT                                            0x0

#define HWIO_QFPROM_CORR_SPARE_REG16_LSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x000042b8)
#define HWIO_QFPROM_CORR_SPARE_REG16_LSB_PHYS                                                  (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000042b8)
#define HWIO_QFPROM_CORR_SPARE_REG16_LSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_CORR_SPARE_REG16_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG16_LSB_ADDR, HWIO_QFPROM_CORR_SPARE_REG16_LSB_RMSK)
#define HWIO_QFPROM_CORR_SPARE_REG16_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG16_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_SPARE_REG16_LSB_SPARE0_BMSK                                           0xffffffff
#define HWIO_QFPROM_CORR_SPARE_REG16_LSB_SPARE0_SHFT                                                  0x0

#define HWIO_QFPROM_CORR_SPARE_REG16_MSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x000042bc)
#define HWIO_QFPROM_CORR_SPARE_REG16_MSB_PHYS                                                  (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000042bc)
#define HWIO_QFPROM_CORR_SPARE_REG16_MSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_CORR_SPARE_REG16_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG16_MSB_ADDR, HWIO_QFPROM_CORR_SPARE_REG16_MSB_RMSK)
#define HWIO_QFPROM_CORR_SPARE_REG16_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG16_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_SPARE_REG16_MSB_SPARE0_BMSK                                           0x80000000
#define HWIO_QFPROM_CORR_SPARE_REG16_MSB_SPARE0_SHFT                                                 0x1f
#define HWIO_QFPROM_CORR_SPARE_REG16_MSB_FEC_VALUE_BMSK                                        0x7f000000
#define HWIO_QFPROM_CORR_SPARE_REG16_MSB_FEC_VALUE_SHFT                                              0x18
#define HWIO_QFPROM_CORR_SPARE_REG16_MSB_SPARE1_BMSK                                             0xffffff
#define HWIO_QFPROM_CORR_SPARE_REG16_MSB_SPARE1_SHFT                                                  0x0

#define HWIO_QFPROM_CORR_SPARE_REG17_LSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x000042c0)
#define HWIO_QFPROM_CORR_SPARE_REG17_LSB_PHYS                                                  (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000042c0)
#define HWIO_QFPROM_CORR_SPARE_REG17_LSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_CORR_SPARE_REG17_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG17_LSB_ADDR, HWIO_QFPROM_CORR_SPARE_REG17_LSB_RMSK)
#define HWIO_QFPROM_CORR_SPARE_REG17_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG17_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_SPARE_REG17_LSB_SPARE0_BMSK                                           0xffffffff
#define HWIO_QFPROM_CORR_SPARE_REG17_LSB_SPARE0_SHFT                                                  0x0

#define HWIO_QFPROM_CORR_SPARE_REG17_MSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x000042c4)
#define HWIO_QFPROM_CORR_SPARE_REG17_MSB_PHYS                                                  (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000042c4)
#define HWIO_QFPROM_CORR_SPARE_REG17_MSB_RMSK                                                    0xffffff
#define HWIO_QFPROM_CORR_SPARE_REG17_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG17_MSB_ADDR, HWIO_QFPROM_CORR_SPARE_REG17_MSB_RMSK)
#define HWIO_QFPROM_CORR_SPARE_REG17_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG17_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_SPARE_REG17_MSB_SPARE1_BMSK                                             0xffffff
#define HWIO_QFPROM_CORR_SPARE_REG17_MSB_SPARE1_SHFT                                                  0x0

#define HWIO_QFPROM_CORR_SPARE_REG18_LSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x000042c8)
#define HWIO_QFPROM_CORR_SPARE_REG18_LSB_PHYS                                                  (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000042c8)
#define HWIO_QFPROM_CORR_SPARE_REG18_LSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_CORR_SPARE_REG18_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG18_LSB_ADDR, HWIO_QFPROM_CORR_SPARE_REG18_LSB_RMSK)
#define HWIO_QFPROM_CORR_SPARE_REG18_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG18_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_SPARE_REG18_LSB_SPARE0_BMSK                                           0xffffffff
#define HWIO_QFPROM_CORR_SPARE_REG18_LSB_SPARE0_SHFT                                                  0x0

#define HWIO_QFPROM_CORR_SPARE_REG18_MSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x000042cc)
#define HWIO_QFPROM_CORR_SPARE_REG18_MSB_PHYS                                                  (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000042cc)
#define HWIO_QFPROM_CORR_SPARE_REG18_MSB_RMSK                                                    0xffffff
#define HWIO_QFPROM_CORR_SPARE_REG18_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG18_MSB_ADDR, HWIO_QFPROM_CORR_SPARE_REG18_MSB_RMSK)
#define HWIO_QFPROM_CORR_SPARE_REG18_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG18_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_SPARE_REG18_MSB_SPARE1_BMSK                                             0xffffff
#define HWIO_QFPROM_CORR_SPARE_REG18_MSB_SPARE1_SHFT                                                  0x0

#define HWIO_QFPROM_CORR_SPARE_REG19_LSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x000042d0)
#define HWIO_QFPROM_CORR_SPARE_REG19_LSB_PHYS                                                  (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000042d0)
#define HWIO_QFPROM_CORR_SPARE_REG19_LSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_CORR_SPARE_REG19_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG19_LSB_ADDR, HWIO_QFPROM_CORR_SPARE_REG19_LSB_RMSK)
#define HWIO_QFPROM_CORR_SPARE_REG19_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG19_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_SPARE_REG19_LSB_SPARE0_BMSK                                           0xffffffff
#define HWIO_QFPROM_CORR_SPARE_REG19_LSB_SPARE0_SHFT                                                  0x0

#define HWIO_QFPROM_CORR_SPARE_REG19_MSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x000042d4)
#define HWIO_QFPROM_CORR_SPARE_REG19_MSB_PHYS                                                  (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000042d4)
#define HWIO_QFPROM_CORR_SPARE_REG19_MSB_RMSK                                                    0xffffff
#define HWIO_QFPROM_CORR_SPARE_REG19_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG19_MSB_ADDR, HWIO_QFPROM_CORR_SPARE_REG19_MSB_RMSK)
#define HWIO_QFPROM_CORR_SPARE_REG19_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG19_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_SPARE_REG19_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_CORR_SPARE_REG19_MSB_ADDR,v)
#define HWIO_QFPROM_CORR_SPARE_REG19_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_CORR_SPARE_REG19_MSB_ADDR,m,v,HWIO_QFPROM_CORR_SPARE_REG19_MSB_IN)
#define HWIO_QFPROM_CORR_SPARE_REG19_MSB_SPARE1_BMSK                                             0xffffff
#define HWIO_QFPROM_CORR_SPARE_REG19_MSB_SPARE1_SHFT                                                  0x0

#define HWIO_QFPROM_CORR_SPARE_REG20_LSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x000042d8)
#define HWIO_QFPROM_CORR_SPARE_REG20_LSB_PHYS                                                  (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000042d8)
#define HWIO_QFPROM_CORR_SPARE_REG20_LSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_CORR_SPARE_REG20_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG20_LSB_ADDR, HWIO_QFPROM_CORR_SPARE_REG20_LSB_RMSK)
#define HWIO_QFPROM_CORR_SPARE_REG20_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG20_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_SPARE_REG20_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_CORR_SPARE_REG20_LSB_ADDR,v)
#define HWIO_QFPROM_CORR_SPARE_REG20_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_CORR_SPARE_REG20_LSB_ADDR,m,v,HWIO_QFPROM_CORR_SPARE_REG20_LSB_IN)
#define HWIO_QFPROM_CORR_SPARE_REG20_LSB_SPARE0_BMSK                                           0xffffffff
#define HWIO_QFPROM_CORR_SPARE_REG20_LSB_SPARE0_SHFT                                                  0x0

#define HWIO_QFPROM_CORR_SPARE_REG20_MSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x000042dc)
#define HWIO_QFPROM_CORR_SPARE_REG20_MSB_PHYS                                                  (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000042dc)
#define HWIO_QFPROM_CORR_SPARE_REG20_MSB_RMSK                                                    0xffffff
#define HWIO_QFPROM_CORR_SPARE_REG20_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG20_MSB_ADDR, HWIO_QFPROM_CORR_SPARE_REG20_MSB_RMSK)
#define HWIO_QFPROM_CORR_SPARE_REG20_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG20_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_SPARE_REG20_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_CORR_SPARE_REG20_MSB_ADDR,v)
#define HWIO_QFPROM_CORR_SPARE_REG20_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_CORR_SPARE_REG20_MSB_ADDR,m,v,HWIO_QFPROM_CORR_SPARE_REG20_MSB_IN)
#define HWIO_QFPROM_CORR_SPARE_REG20_MSB_SPARE1_BMSK                                             0xffffff
#define HWIO_QFPROM_CORR_SPARE_REG20_MSB_SPARE1_SHFT                                                  0x0

#define HWIO_QFPROM_CORR_SPARE_REG21_LSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x000042e0)
#define HWIO_QFPROM_CORR_SPARE_REG21_LSB_PHYS                                                  (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000042e0)
#define HWIO_QFPROM_CORR_SPARE_REG21_LSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_CORR_SPARE_REG21_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG21_LSB_ADDR, HWIO_QFPROM_CORR_SPARE_REG21_LSB_RMSK)
#define HWIO_QFPROM_CORR_SPARE_REG21_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG21_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_SPARE_REG21_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_CORR_SPARE_REG21_LSB_ADDR,v)
#define HWIO_QFPROM_CORR_SPARE_REG21_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_CORR_SPARE_REG21_LSB_ADDR,m,v,HWIO_QFPROM_CORR_SPARE_REG21_LSB_IN)
#define HWIO_QFPROM_CORR_SPARE_REG21_LSB_SPARE0_BMSK                                           0xffffffff
#define HWIO_QFPROM_CORR_SPARE_REG21_LSB_SPARE0_SHFT                                                  0x0

#define HWIO_QFPROM_CORR_SPARE_REG21_MSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x000042e4)
#define HWIO_QFPROM_CORR_SPARE_REG21_MSB_PHYS                                                  (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x000042e4)
#define HWIO_QFPROM_CORR_SPARE_REG21_MSB_RMSK                                                    0xffffff
#define HWIO_QFPROM_CORR_SPARE_REG21_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG21_MSB_ADDR, HWIO_QFPROM_CORR_SPARE_REG21_MSB_RMSK)
#define HWIO_QFPROM_CORR_SPARE_REG21_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_SPARE_REG21_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_SPARE_REG21_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_CORR_SPARE_REG21_MSB_ADDR,v)
#define HWIO_QFPROM_CORR_SPARE_REG21_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_CORR_SPARE_REG21_MSB_ADDR,m,v,HWIO_QFPROM_CORR_SPARE_REG21_MSB_IN)
#define HWIO_QFPROM_CORR_SPARE_REG21_MSB_SPARE1_BMSK                                             0xffffff
#define HWIO_QFPROM_CORR_SPARE_REG21_MSB_SPARE1_SHFT                                                  0x0

#define HWIO_QFPROM_CORR_ACC_PRIVATEn_ADDR(n)                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004400 + 0x4 * (n))
#define HWIO_QFPROM_CORR_ACC_PRIVATEn_PHYS(n)                                                  (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00004400 + 0x4 * (n))
#define HWIO_QFPROM_CORR_ACC_PRIVATEn_RMSK                                                     0xffffffff
#define HWIO_QFPROM_CORR_ACC_PRIVATEn_MAXn                                                             39
#define HWIO_QFPROM_CORR_ACC_PRIVATEn_INI(n)        \
        in_dword_masked(HWIO_QFPROM_CORR_ACC_PRIVATEn_ADDR(n), HWIO_QFPROM_CORR_ACC_PRIVATEn_RMSK)
#define HWIO_QFPROM_CORR_ACC_PRIVATEn_INMI(n,mask)    \
        in_dword_masked(HWIO_QFPROM_CORR_ACC_PRIVATEn_ADDR(n), mask)
#define HWIO_QFPROM_CORR_ACC_PRIVATEn_ACC_PRIVATE_BMSK                                         0xffffffff
#define HWIO_QFPROM_CORR_ACC_PRIVATEn_ACC_PRIVATE_SHFT                                                0x0

#define HWIO_SEC_CTRL_HW_VERSION_ADDR                                                          (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006000)
#define HWIO_SEC_CTRL_HW_VERSION_PHYS                                                          (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00006000)
#define HWIO_SEC_CTRL_HW_VERSION_RMSK                                                          0xffffffff
#define HWIO_SEC_CTRL_HW_VERSION_IN          \
        in_dword_masked(HWIO_SEC_CTRL_HW_VERSION_ADDR, HWIO_SEC_CTRL_HW_VERSION_RMSK)
#define HWIO_SEC_CTRL_HW_VERSION_INM(m)      \
        in_dword_masked(HWIO_SEC_CTRL_HW_VERSION_ADDR, m)
#define HWIO_SEC_CTRL_HW_VERSION_MAJOR_BMSK                                                    0xf0000000
#define HWIO_SEC_CTRL_HW_VERSION_MAJOR_SHFT                                                          0x1c
#define HWIO_SEC_CTRL_HW_VERSION_MINOR_BMSK                                                     0xfff0000
#define HWIO_SEC_CTRL_HW_VERSION_MINOR_SHFT                                                          0x10
#define HWIO_SEC_CTRL_HW_VERSION_STEP_BMSK                                                         0xffff
#define HWIO_SEC_CTRL_HW_VERSION_STEP_SHFT                                                            0x0

#define HWIO_FEATURE_CONFIG0_ADDR                                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006004)
#define HWIO_FEATURE_CONFIG0_PHYS                                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00006004)
#define HWIO_FEATURE_CONFIG0_RMSK                                                              0xffffffff
#define HWIO_FEATURE_CONFIG0_IN          \
        in_dword_masked(HWIO_FEATURE_CONFIG0_ADDR, HWIO_FEATURE_CONFIG0_RMSK)
#define HWIO_FEATURE_CONFIG0_INM(m)      \
        in_dword_masked(HWIO_FEATURE_CONFIG0_ADDR, m)
#define HWIO_FEATURE_CONFIG0_MSMC_MODEM_VU_EN_BMSK                                             0xfc000000
#define HWIO_FEATURE_CONFIG0_MSMC_MODEM_VU_EN_SHFT                                                   0x1a
#define HWIO_FEATURE_CONFIG0_MSMC_NAV_EN_BMSK                                                   0x2000000
#define HWIO_FEATURE_CONFIG0_MSMC_NAV_EN_SHFT                                                        0x19
#define HWIO_FEATURE_CONFIG0_MSMC_LDO_EN_BMSK                                                   0x1000000
#define HWIO_FEATURE_CONFIG0_MSMC_LDO_EN_SHFT                                                        0x18
#define HWIO_FEATURE_CONFIG0_MSMC_ECS_EN_BMSK                                                    0x800000
#define HWIO_FEATURE_CONFIG0_MSMC_ECS_EN_SHFT                                                        0x17
#define HWIO_FEATURE_CONFIG0_MSMC_MODEM_WCDMA_EN_BMSK                                            0x600000
#define HWIO_FEATURE_CONFIG0_MSMC_MODEM_WCDMA_EN_SHFT                                                0x15
#define HWIO_FEATURE_CONFIG0_MSMC_MODEM_TDSCDMA_EN_BMSK                                          0x180000
#define HWIO_FEATURE_CONFIG0_MSMC_MODEM_TDSCDMA_EN_SHFT                                              0x13
#define HWIO_FEATURE_CONFIG0_MSMC_MODEM_LTE_EN_BMSK                                               0x60000
#define HWIO_FEATURE_CONFIG0_MSMC_MODEM_LTE_EN_SHFT                                                  0x11
#define HWIO_FEATURE_CONFIG0_MSMC_MODEM_LTE_ABOVE_CAT2_EN_BMSK                                    0x18000
#define HWIO_FEATURE_CONFIG0_MSMC_MODEM_LTE_ABOVE_CAT2_EN_SHFT                                        0xf
#define HWIO_FEATURE_CONFIG0_MSMC_MODEM_LTE_ABOVE_CAT1_EN_BMSK                                     0x6000
#define HWIO_FEATURE_CONFIG0_MSMC_MODEM_LTE_ABOVE_CAT1_EN_SHFT                                        0xd
#define HWIO_FEATURE_CONFIG0_MSMC_MODEM_HSPA_MIMO_EN_BMSK                                          0x1800
#define HWIO_FEATURE_CONFIG0_MSMC_MODEM_HSPA_MIMO_EN_SHFT                                             0xb
#define HWIO_FEATURE_CONFIG0_MSMC_MODEM_HSPA_EN_BMSK                                                0x600
#define HWIO_FEATURE_CONFIG0_MSMC_MODEM_HSPA_EN_SHFT                                                  0x9
#define HWIO_FEATURE_CONFIG0_MSMC_MODEM_HSPA_DC_EN_BMSK                                             0x180
#define HWIO_FEATURE_CONFIG0_MSMC_MODEM_HSPA_DC_EN_SHFT                                               0x7
#define HWIO_FEATURE_CONFIG0_MSMC_MODEM_GENRAN_EN_BMSK                                               0x60
#define HWIO_FEATURE_CONFIG0_MSMC_MODEM_GENRAN_EN_SHFT                                                0x5
#define HWIO_FEATURE_CONFIG0_MSMC_MODEM_EN_BMSK                                                      0x10
#define HWIO_FEATURE_CONFIG0_MSMC_MODEM_EN_SHFT                                                       0x4
#define HWIO_FEATURE_CONFIG0_MSMC_MODEM_DO_EN_BMSK                                                    0xc
#define HWIO_FEATURE_CONFIG0_MSMC_MODEM_DO_EN_SHFT                                                    0x2
#define HWIO_FEATURE_CONFIG0_MSMC_MODEM_1X_EN_BMSK                                                    0x3
#define HWIO_FEATURE_CONFIG0_MSMC_MODEM_1X_EN_SHFT                                                    0x0

#define HWIO_FEATURE_CONFIG1_ADDR                                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006008)
#define HWIO_FEATURE_CONFIG1_PHYS                                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00006008)
#define HWIO_FEATURE_CONFIG1_RMSK                                                              0xffffffff
#define HWIO_FEATURE_CONFIG1_IN          \
        in_dword_masked(HWIO_FEATURE_CONFIG1_ADDR, HWIO_FEATURE_CONFIG1_RMSK)
#define HWIO_FEATURE_CONFIG1_INM(m)      \
        in_dword_masked(HWIO_FEATURE_CONFIG1_ADDR, m)
#define HWIO_FEATURE_CONFIG1_SDC_EMMC_MODE1P2_FORCE_GPIO_BMSK                                  0x80000000
#define HWIO_FEATURE_CONFIG1_SDC_EMMC_MODE1P2_FORCE_GPIO_SHFT                                        0x1f
#define HWIO_FEATURE_CONFIG1_SPARE_BMSK                                                        0x7ff00000
#define HWIO_FEATURE_CONFIG1_SPARE_SHFT                                                              0x14
#define HWIO_FEATURE_CONFIG1_MSMC_MDSP_FW_EN_BMSK                                                 0xfff00
#define HWIO_FEATURE_CONFIG1_MSMC_MDSP_FW_EN_SHFT                                                     0x8
#define HWIO_FEATURE_CONFIG1_MSMC_SPARE_BMSK                                                         0xc0
#define HWIO_FEATURE_CONFIG1_MSMC_SPARE_SHFT                                                          0x6
#define HWIO_FEATURE_CONFIG1_MSMC_MODEM_VU_EN_BMSK                                                   0x3f
#define HWIO_FEATURE_CONFIG1_MSMC_MODEM_VU_EN_SHFT                                                    0x0

#define HWIO_FEATURE_CONFIG2_ADDR                                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000600c)
#define HWIO_FEATURE_CONFIG2_PHYS                                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x0000600c)
#define HWIO_FEATURE_CONFIG2_RMSK                                                              0xffffffff
#define HWIO_FEATURE_CONFIG2_IN          \
        in_dword_masked(HWIO_FEATURE_CONFIG2_ADDR, HWIO_FEATURE_CONFIG2_RMSK)
#define HWIO_FEATURE_CONFIG2_INM(m)      \
        in_dword_masked(HWIO_FEATURE_CONFIG2_ADDR, m)
#define HWIO_FEATURE_CONFIG2_ACC_DISABLE_BMSK                                                  0x80000000
#define HWIO_FEATURE_CONFIG2_ACC_DISABLE_SHFT                                                        0x1f
#define HWIO_FEATURE_CONFIG2_SHADOW_READ_DISABLE_BMSK                                          0x40000000
#define HWIO_FEATURE_CONFIG2_SHADOW_READ_DISABLE_SHFT                                                0x1e
#define HWIO_FEATURE_CONFIG2_SPARE1_BMSK                                                       0x30000000
#define HWIO_FEATURE_CONFIG2_SPARE1_SHFT                                                             0x1c
#define HWIO_FEATURE_CONFIG2_FUSE_SMT_PERM_ENABLE_BMSK                                          0x8000000
#define HWIO_FEATURE_CONFIG2_FUSE_SMT_PERM_ENABLE_SHFT                                               0x1b
#define HWIO_FEATURE_CONFIG2_FUSE_RCP_BYPASS_ENABLE_BMSK                                        0x4000000
#define HWIO_FEATURE_CONFIG2_FUSE_RCP_BYPASS_ENABLE_SHFT                                             0x1a
#define HWIO_FEATURE_CONFIG2_MSMC_SMMU_BYPASS_DISABLE_BMSK                                      0x2000000
#define HWIO_FEATURE_CONFIG2_MSMC_SMMU_BYPASS_DISABLE_SHFT                                           0x19
#define HWIO_FEATURE_CONFIG2_MSMC_GPIO_SLIMBUS_PD_DISABLE_BMSK                                  0x1000000
#define HWIO_FEATURE_CONFIG2_MSMC_GPIO_SLIMBUS_PD_DISABLE_SHFT                                       0x18
#define HWIO_FEATURE_CONFIG2_FUSE_PCIE20_RC_DISABLE_BMSK                                         0x800000
#define HWIO_FEATURE_CONFIG2_FUSE_PCIE20_RC_DISABLE_SHFT                                             0x17
#define HWIO_FEATURE_CONFIG2_MSMC_PCIE_DISABLE_BMSK                                              0x400000
#define HWIO_FEATURE_CONFIG2_MSMC_PCIE_DISABLE_SHFT                                                  0x16
#define HWIO_FEATURE_CONFIG2_NDINT_DISABLE_BMSK                                                  0x200000
#define HWIO_FEATURE_CONFIG2_NDINT_DISABLE_SHFT                                                      0x15
#define HWIO_FEATURE_CONFIG2_QC_DAP_DEVICEEN_DISABLE_BMSK                                        0x100000
#define HWIO_FEATURE_CONFIG2_QC_DAP_DEVICEEN_DISABLE_SHFT                                            0x14
#define HWIO_FEATURE_CONFIG2_QC_DAP_SPNIDEN_DISABLE_BMSK                                          0x80000
#define HWIO_FEATURE_CONFIG2_QC_DAP_SPNIDEN_DISABLE_SHFT                                             0x13
#define HWIO_FEATURE_CONFIG2_QC_DAP_SPIDEN_DISABLE_BMSK                                           0x40000
#define HWIO_FEATURE_CONFIG2_QC_DAP_SPIDEN_DISABLE_SHFT                                              0x12
#define HWIO_FEATURE_CONFIG2_QC_DAP_NIDEN_DISABLE_BMSK                                            0x20000
#define HWIO_FEATURE_CONFIG2_QC_DAP_NIDEN_DISABLE_SHFT                                               0x11
#define HWIO_FEATURE_CONFIG2_QC_DAP_DBGEN_DISABLE_BMSK                                            0x10000
#define HWIO_FEATURE_CONFIG2_QC_DAP_DBGEN_DISABLE_SHFT                                               0x10
#define HWIO_FEATURE_CONFIG2_QC_APPS_SPNIDEN_DISABLE_BMSK                                          0x8000
#define HWIO_FEATURE_CONFIG2_QC_APPS_SPNIDEN_DISABLE_SHFT                                             0xf
#define HWIO_FEATURE_CONFIG2_QC_APPS_SPIDEN_DISABLE_BMSK                                           0x4000
#define HWIO_FEATURE_CONFIG2_QC_APPS_SPIDEN_DISABLE_SHFT                                              0xe
#define HWIO_FEATURE_CONFIG2_QC_APPS_NIDEN_DISABLE_BMSK                                            0x2000
#define HWIO_FEATURE_CONFIG2_QC_APPS_NIDEN_DISABLE_SHFT                                               0xd
#define HWIO_FEATURE_CONFIG2_QC_APPS_DBGEN_DISABLE_BMSK                                            0x1000
#define HWIO_FEATURE_CONFIG2_QC_APPS_DBGEN_DISABLE_SHFT                                               0xc
#define HWIO_FEATURE_CONFIG2_QC_SPARE1_DISABLE_BMSK                                                 0x800
#define HWIO_FEATURE_CONFIG2_QC_SPARE1_DISABLE_SHFT                                                   0xb
#define HWIO_FEATURE_CONFIG2_QC_SPARE0_DISABLE_BMSK                                                 0x400
#define HWIO_FEATURE_CONFIG2_QC_SPARE0_DISABLE_SHFT                                                   0xa
#define HWIO_FEATURE_CONFIG2_QC_VENUS_0_DBGEN_DISABLE_BMSK                                          0x200
#define HWIO_FEATURE_CONFIG2_QC_VENUS_0_DBGEN_DISABLE_SHFT                                            0x9
#define HWIO_FEATURE_CONFIG2_QC_RPM_DAPEN_DISABLE_BMSK                                              0x100
#define HWIO_FEATURE_CONFIG2_QC_RPM_DAPEN_DISABLE_SHFT                                                0x8
#define HWIO_FEATURE_CONFIG2_QC_RPM_WCSS_NIDEN_DISABLE_BMSK                                          0x80
#define HWIO_FEATURE_CONFIG2_QC_RPM_WCSS_NIDEN_DISABLE_SHFT                                           0x7
#define HWIO_FEATURE_CONFIG2_QC_RPM_DBGEN_DISABLE_BMSK                                               0x40
#define HWIO_FEATURE_CONFIG2_QC_RPM_DBGEN_DISABLE_SHFT                                                0x6
#define HWIO_FEATURE_CONFIG2_QC_WCSS_DBGEN_DISABLE_BMSK                                              0x20
#define HWIO_FEATURE_CONFIG2_QC_WCSS_DBGEN_DISABLE_SHFT                                               0x5
#define HWIO_FEATURE_CONFIG2_QC_MSS_NIDEN_DISABLE_BMSK                                               0x10
#define HWIO_FEATURE_CONFIG2_QC_MSS_NIDEN_DISABLE_SHFT                                                0x4
#define HWIO_FEATURE_CONFIG2_QC_MSS_DBGEN_DISABLE_BMSK                                                0x8
#define HWIO_FEATURE_CONFIG2_QC_MSS_DBGEN_DISABLE_SHFT                                                0x3
#define HWIO_FEATURE_CONFIG2_QDI_SPMI_DISABLE_BMSK                                                    0x4
#define HWIO_FEATURE_CONFIG2_QDI_SPMI_DISABLE_SHFT                                                    0x2
#define HWIO_FEATURE_CONFIG2_SM_BIST_DISABLE_BMSK                                                     0x2
#define HWIO_FEATURE_CONFIG2_SM_BIST_DISABLE_SHFT                                                     0x1
#define HWIO_FEATURE_CONFIG2_TIC_DISABLE_BMSK                                                         0x1
#define HWIO_FEATURE_CONFIG2_TIC_DISABLE_SHFT                                                         0x0

#define HWIO_FEATURE_CONFIG3_ADDR                                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006010)
#define HWIO_FEATURE_CONFIG3_PHYS                                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00006010)
#define HWIO_FEATURE_CONFIG3_RMSK                                                              0xffffffff
#define HWIO_FEATURE_CONFIG3_IN          \
        in_dword_masked(HWIO_FEATURE_CONFIG3_ADDR, HWIO_FEATURE_CONFIG3_RMSK)
#define HWIO_FEATURE_CONFIG3_INM(m)      \
        in_dword_masked(HWIO_FEATURE_CONFIG3_ADDR, m)
#define HWIO_FEATURE_CONFIG3_SEC_TAP_ACCESS_DISABLE_BMSK                                       0xf8000000
#define HWIO_FEATURE_CONFIG3_SEC_TAP_ACCESS_DISABLE_SHFT                                             0x1b
#define HWIO_FEATURE_CONFIG3_TAP_CJI_CORE_SEL_DISABLE_BMSK                                      0x4000000
#define HWIO_FEATURE_CONFIG3_TAP_CJI_CORE_SEL_DISABLE_SHFT                                           0x1a
#define HWIO_FEATURE_CONFIG3_TAP_INSTR_DISABLE_BMSK                                             0x3ffc000
#define HWIO_FEATURE_CONFIG3_TAP_INSTR_DISABLE_SHFT                                                   0xe
#define HWIO_FEATURE_CONFIG3_SPARE1_BMSK                                                           0x3c00
#define HWIO_FEATURE_CONFIG3_SPARE1_SHFT                                                              0xa
#define HWIO_FEATURE_CONFIG3_MODEM_PBL_PATCH_VERSION_BMSK                                           0x3e0
#define HWIO_FEATURE_CONFIG3_MODEM_PBL_PATCH_VERSION_SHFT                                             0x5
#define HWIO_FEATURE_CONFIG3_APPS_PBL_PATCH_VERSION_BMSK                                             0x1f
#define HWIO_FEATURE_CONFIG3_APPS_PBL_PATCH_VERSION_SHFT                                              0x0

#define HWIO_FEATURE_CONFIG4_ADDR                                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006014)
#define HWIO_FEATURE_CONFIG4_PHYS                                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00006014)
#define HWIO_FEATURE_CONFIG4_RMSK                                                              0xffffffff
#define HWIO_FEATURE_CONFIG4_IN          \
        in_dword_masked(HWIO_FEATURE_CONFIG4_ADDR, HWIO_FEATURE_CONFIG4_RMSK)
#define HWIO_FEATURE_CONFIG4_INM(m)      \
        in_dword_masked(HWIO_FEATURE_CONFIG4_ADDR, m)
#define HWIO_FEATURE_CONFIG4_TAP_GEN_SPARE_INSTR_DISABLE_31_0_BMSK                             0xffffffff
#define HWIO_FEATURE_CONFIG4_TAP_GEN_SPARE_INSTR_DISABLE_31_0_SHFT                                    0x0

#define HWIO_FEATURE_CONFIG5_ADDR                                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006018)
#define HWIO_FEATURE_CONFIG5_PHYS                                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00006018)
#define HWIO_FEATURE_CONFIG5_RMSK                                                              0xffffffff
#define HWIO_FEATURE_CONFIG5_IN          \
        in_dword_masked(HWIO_FEATURE_CONFIG5_ADDR, HWIO_FEATURE_CONFIG5_RMSK)
#define HWIO_FEATURE_CONFIG5_INM(m)      \
        in_dword_masked(HWIO_FEATURE_CONFIG5_ADDR, m)
#define HWIO_FEATURE_CONFIG5_STACKED_MEMORY_ID_BMSK                                            0xf0000000
#define HWIO_FEATURE_CONFIG5_STACKED_MEMORY_ID_SHFT                                                  0x1c
#define HWIO_FEATURE_CONFIG5_APSS_BOOT_TRIGGER_DISABLE_BMSK                                     0x8000000
#define HWIO_FEATURE_CONFIG5_APSS_BOOT_TRIGGER_DISABLE_SHFT                                          0x1b
#define HWIO_FEATURE_CONFIG5_MODEM_PBL_PLL_CTRL_BMSK                                            0x7800000
#define HWIO_FEATURE_CONFIG5_MODEM_PBL_PLL_CTRL_SHFT                                                 0x17
#define HWIO_FEATURE_CONFIG5_APPS_PBL_PLL_CTRL_BMSK                                              0x780000
#define HWIO_FEATURE_CONFIG5_APPS_PBL_PLL_CTRL_SHFT                                                  0x13
#define HWIO_FEATURE_CONFIG5_APPS_BOOT_FSM_CFG_BMSK                                               0x40000
#define HWIO_FEATURE_CONFIG5_APPS_BOOT_FSM_CFG_SHFT                                                  0x12
#define HWIO_FEATURE_CONFIG5_APPS_BOOT_FSM_DELAY_BMSK                                             0x30000
#define HWIO_FEATURE_CONFIG5_APPS_BOOT_FSM_DELAY_SHFT                                                0x10
#define HWIO_FEATURE_CONFIG5_BOOT_ROM_PATCH_SEL_EN_USB_PHY_TUNE_BMSK                               0x8000
#define HWIO_FEATURE_CONFIG5_BOOT_ROM_PATCH_SEL_EN_USB_PHY_TUNE_SHFT                                  0xf
#define HWIO_FEATURE_CONFIG5_USB_SS_ENABLE_BMSK                                                    0x4000
#define HWIO_FEATURE_CONFIG5_USB_SS_ENABLE_SHFT                                                       0xe
#define HWIO_FEATURE_CONFIG5_APPS_PBL_BOOT_SPEED_BMSK                                              0x3000
#define HWIO_FEATURE_CONFIG5_APPS_PBL_BOOT_SPEED_SHFT                                                 0xc
#define HWIO_FEATURE_CONFIG5_APPS_BOOT_FROM_ROM_BMSK                                                0x800
#define HWIO_FEATURE_CONFIG5_APPS_BOOT_FROM_ROM_SHFT                                                  0xb
#define HWIO_FEATURE_CONFIG5_MSA_ENA_BMSK                                                           0x400
#define HWIO_FEATURE_CONFIG5_MSA_ENA_SHFT                                                             0xa
#define HWIO_FEATURE_CONFIG5_FORCE_MSA_AUTH_EN_BMSK                                                 0x200
#define HWIO_FEATURE_CONFIG5_FORCE_MSA_AUTH_EN_SHFT                                                   0x9
#define HWIO_FEATURE_CONFIG5_SPARE_8_7_BMSK                                                         0x180
#define HWIO_FEATURE_CONFIG5_SPARE_8_7_SHFT                                                           0x7
#define HWIO_FEATURE_CONFIG5_MODEM_BOOT_FROM_ROM_BMSK                                                0x40
#define HWIO_FEATURE_CONFIG5_MODEM_BOOT_FROM_ROM_SHFT                                                 0x6
#define HWIO_FEATURE_CONFIG5_BOOT_ROM_PATCH_DISABLE_BMSK                                             0x20
#define HWIO_FEATURE_CONFIG5_BOOT_ROM_PATCH_DISABLE_SHFT                                              0x5
#define HWIO_FEATURE_CONFIG5_TAP_GEN_SPARE_INSTR_DISABLE_36_32_BMSK                                  0x1f
#define HWIO_FEATURE_CONFIG5_TAP_GEN_SPARE_INSTR_DISABLE_36_32_SHFT                                   0x0

#define HWIO_OEM_CONFIG0_ADDR                                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000601c)
#define HWIO_OEM_CONFIG0_PHYS                                                                  (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x0000601c)
#define HWIO_OEM_CONFIG0_RMSK                                                                  0xffffffff
#define HWIO_OEM_CONFIG0_IN          \
        in_dword_masked(HWIO_OEM_CONFIG0_ADDR, HWIO_OEM_CONFIG0_RMSK)
#define HWIO_OEM_CONFIG0_INM(m)      \
        in_dword_masked(HWIO_OEM_CONFIG0_ADDR, m)
#define HWIO_OEM_CONFIG0_SPARE0_BMSK                                                           0xff000000
#define HWIO_OEM_CONFIG0_SPARE0_SHFT                                                                 0x18
#define HWIO_OEM_CONFIG0_SPARE_REG21_SECURE_BMSK                                                 0x800000
#define HWIO_OEM_CONFIG0_SPARE_REG21_SECURE_SHFT                                                     0x17
#define HWIO_OEM_CONFIG0_PBL_LOG_DISABLE_BMSK                                                    0x400000
#define HWIO_OEM_CONFIG0_PBL_LOG_DISABLE_SHFT                                                        0x16
#define HWIO_OEM_CONFIG0_ROOT_CERT_TOTAL_NUM_BMSK                                                0x3c0000
#define HWIO_OEM_CONFIG0_ROOT_CERT_TOTAL_NUM_SHFT                                                    0x12
#define HWIO_OEM_CONFIG0_SPARE_REG20_SECURE_BMSK                                                  0x20000
#define HWIO_OEM_CONFIG0_SPARE_REG20_SECURE_SHFT                                                     0x11
#define HWIO_OEM_CONFIG0_SPARE_REG19_SECURE_BMSK                                                  0x10000
#define HWIO_OEM_CONFIG0_SPARE_REG19_SECURE_SHFT                                                     0x10
#define HWIO_OEM_CONFIG0_SPARE_REG18_SECURE_BMSK                                                   0x8000
#define HWIO_OEM_CONFIG0_SPARE_REG18_SECURE_SHFT                                                      0xf
#define HWIO_OEM_CONFIG0_SPARE_REG17_SECURE_BMSK                                                   0x4000
#define HWIO_OEM_CONFIG0_SPARE_REG17_SECURE_SHFT                                                      0xe
#define HWIO_OEM_CONFIG0_SPARE_REG16_SECURE_BMSK                                                   0x2000
#define HWIO_OEM_CONFIG0_SPARE_REG16_SECURE_SHFT                                                      0xd
#define HWIO_OEM_CONFIG0_WDOG_EN_BMSK                                                              0x1000
#define HWIO_OEM_CONFIG0_WDOG_EN_SHFT                                                                 0xc
#define HWIO_OEM_CONFIG0_SPDM_SECURE_MODE_BMSK                                                      0x800
#define HWIO_OEM_CONFIG0_SPDM_SECURE_MODE_SHFT                                                        0xb
#define HWIO_OEM_CONFIG0_ALT_SD_PORT_FOR_BOOT_BMSK                                                  0x400
#define HWIO_OEM_CONFIG0_ALT_SD_PORT_FOR_BOOT_SHFT                                                    0xa
#define HWIO_OEM_CONFIG0_SDC_EMMC_MODE1P2_GPIO_DISABLE_BMSK                                         0x200
#define HWIO_OEM_CONFIG0_SDC_EMMC_MODE1P2_GPIO_DISABLE_SHFT                                           0x9
#define HWIO_OEM_CONFIG0_SDC_EMMC_MODE1P2_EN_BMSK                                                   0x100
#define HWIO_OEM_CONFIG0_SDC_EMMC_MODE1P2_EN_SHFT                                                     0x8
#define HWIO_OEM_CONFIG0_FAST_BOOT_BMSK                                                              0xe0
#define HWIO_OEM_CONFIG0_FAST_BOOT_SHFT                                                               0x5
#define HWIO_OEM_CONFIG0_SDCC_MCLK_BOOT_FREQ_BMSK                                                    0x10
#define HWIO_OEM_CONFIG0_SDCC_MCLK_BOOT_FREQ_SHFT                                                     0x4
#define HWIO_OEM_CONFIG0_FORCE_DLOAD_DISABLE_BMSK                                                     0x8
#define HWIO_OEM_CONFIG0_FORCE_DLOAD_DISABLE_SHFT                                                     0x3
#define HWIO_OEM_CONFIG0_SPARE_BMSK                                                                   0x4
#define HWIO_OEM_CONFIG0_SPARE_SHFT                                                                   0x2
#define HWIO_OEM_CONFIG0_ENUM_TIMEOUT_BMSK                                                            0x2
#define HWIO_OEM_CONFIG0_ENUM_TIMEOUT_SHFT                                                            0x1
#define HWIO_OEM_CONFIG0_E_DLOAD_DISABLE_BMSK                                                         0x1
#define HWIO_OEM_CONFIG0_E_DLOAD_DISABLE_SHFT                                                         0x0

#define HWIO_OEM_CONFIG1_ADDR                                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006020)
#define HWIO_OEM_CONFIG1_PHYS                                                                  (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00006020)
#define HWIO_OEM_CONFIG1_RMSK                                                                  0xffffffff
#define HWIO_OEM_CONFIG1_IN          \
        in_dword_masked(HWIO_OEM_CONFIG1_ADDR, HWIO_OEM_CONFIG1_RMSK)
#define HWIO_OEM_CONFIG1_INM(m)      \
        in_dword_masked(HWIO_OEM_CONFIG1_ADDR, m)
#define HWIO_OEM_CONFIG1_SPARE1_BMSK                                                           0xff800000
#define HWIO_OEM_CONFIG1_SPARE1_SHFT                                                                 0x17
#define HWIO_OEM_CONFIG1_ANTI_ROLLBACK_FEATURE_EN_BMSK                                           0x780000
#define HWIO_OEM_CONFIG1_ANTI_ROLLBACK_FEATURE_EN_SHFT                                               0x13
#define HWIO_OEM_CONFIG1_DAP_DEVICEEN_DISABLE_BMSK                                                0x40000
#define HWIO_OEM_CONFIG1_DAP_DEVICEEN_DISABLE_SHFT                                                   0x12
#define HWIO_OEM_CONFIG1_DAP_SPNIDEN_DISABLE_BMSK                                                 0x20000
#define HWIO_OEM_CONFIG1_DAP_SPNIDEN_DISABLE_SHFT                                                    0x11
#define HWIO_OEM_CONFIG1_DAP_SPIDEN_DISABLE_BMSK                                                  0x10000
#define HWIO_OEM_CONFIG1_DAP_SPIDEN_DISABLE_SHFT                                                     0x10
#define HWIO_OEM_CONFIG1_DAP_NIDEN_DISABLE_BMSK                                                    0x8000
#define HWIO_OEM_CONFIG1_DAP_NIDEN_DISABLE_SHFT                                                       0xf
#define HWIO_OEM_CONFIG1_DAP_DBGEN_DISABLE_BMSK                                                    0x4000
#define HWIO_OEM_CONFIG1_DAP_DBGEN_DISABLE_SHFT                                                       0xe
#define HWIO_OEM_CONFIG1_APPS_SPNIDEN_DISABLE_BMSK                                                 0x2000
#define HWIO_OEM_CONFIG1_APPS_SPNIDEN_DISABLE_SHFT                                                    0xd
#define HWIO_OEM_CONFIG1_APPS_SPIDEN_DISABLE_BMSK                                                  0x1000
#define HWIO_OEM_CONFIG1_APPS_SPIDEN_DISABLE_SHFT                                                     0xc
#define HWIO_OEM_CONFIG1_APPS_NIDEN_DISABLE_BMSK                                                    0x800
#define HWIO_OEM_CONFIG1_APPS_NIDEN_DISABLE_SHFT                                                      0xb
#define HWIO_OEM_CONFIG1_APPS_DBGEN_DISABLE_BMSK                                                    0x400
#define HWIO_OEM_CONFIG1_APPS_DBGEN_DISABLE_SHFT                                                      0xa
#define HWIO_OEM_CONFIG1_SPARE1_DISABLE_BMSK                                                        0x200
#define HWIO_OEM_CONFIG1_SPARE1_DISABLE_SHFT                                                          0x9
#define HWIO_OEM_CONFIG1_SPARE0_DISABLE_BMSK                                                        0x100
#define HWIO_OEM_CONFIG1_SPARE0_DISABLE_SHFT                                                          0x8
#define HWIO_OEM_CONFIG1_VENUS_0_DBGEN_DISABLE_BMSK                                                  0x80
#define HWIO_OEM_CONFIG1_VENUS_0_DBGEN_DISABLE_SHFT                                                   0x7
#define HWIO_OEM_CONFIG1_RPM_DAPEN_DISABLE_BMSK                                                      0x40
#define HWIO_OEM_CONFIG1_RPM_DAPEN_DISABLE_SHFT                                                       0x6
#define HWIO_OEM_CONFIG1_RPM_WCSS_NIDEN_DISABLE_BMSK                                                 0x20
#define HWIO_OEM_CONFIG1_RPM_WCSS_NIDEN_DISABLE_SHFT                                                  0x5
#define HWIO_OEM_CONFIG1_RPM_DBGEN_DISABLE_BMSK                                                      0x10
#define HWIO_OEM_CONFIG1_RPM_DBGEN_DISABLE_SHFT                                                       0x4
#define HWIO_OEM_CONFIG1_WCSS_DBGEN_DISABLE_BMSK                                                      0x8
#define HWIO_OEM_CONFIG1_WCSS_DBGEN_DISABLE_SHFT                                                      0x3
#define HWIO_OEM_CONFIG1_MSS_NIDEN_DISABLE_BMSK                                                       0x4
#define HWIO_OEM_CONFIG1_MSS_NIDEN_DISABLE_SHFT                                                       0x2
#define HWIO_OEM_CONFIG1_MSS_DBGEN_DISABLE_BMSK                                                       0x2
#define HWIO_OEM_CONFIG1_MSS_DBGEN_DISABLE_SHFT                                                       0x1
#define HWIO_OEM_CONFIG1_ALL_DEBUG_DISABLE_BMSK                                                       0x1
#define HWIO_OEM_CONFIG1_ALL_DEBUG_DISABLE_SHFT                                                       0x0

#define HWIO_OEM_CONFIG2_ADDR                                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006024)
#define HWIO_OEM_CONFIG2_PHYS                                                                  (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00006024)
#define HWIO_OEM_CONFIG2_RMSK                                                                  0xffffffff
#define HWIO_OEM_CONFIG2_IN          \
        in_dword_masked(HWIO_OEM_CONFIG2_ADDR, HWIO_OEM_CONFIG2_RMSK)
#define HWIO_OEM_CONFIG2_INM(m)      \
        in_dword_masked(HWIO_OEM_CONFIG2_ADDR, m)
#define HWIO_OEM_CONFIG2_OEM_PRODUCT_ID_BMSK                                                   0xffff0000
#define HWIO_OEM_CONFIG2_OEM_PRODUCT_ID_SHFT                                                         0x10
#define HWIO_OEM_CONFIG2_OEM_HW_ID_BMSK                                                            0xffff
#define HWIO_OEM_CONFIG2_OEM_HW_ID_SHFT                                                               0x0

#define HWIO_OEM_CONFIG3_ADDR                                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006028)
#define HWIO_OEM_CONFIG3_PHYS                                                                  (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00006028)
#define HWIO_OEM_CONFIG3_RMSK                                                                  0xffffffff
#define HWIO_OEM_CONFIG3_IN          \
        in_dword_masked(HWIO_OEM_CONFIG3_ADDR, HWIO_OEM_CONFIG3_RMSK)
#define HWIO_OEM_CONFIG3_INM(m)      \
        in_dword_masked(HWIO_OEM_CONFIG3_ADDR, m)
#define HWIO_OEM_CONFIG3_PERIPH_VID_BMSK                                                       0xffff0000
#define HWIO_OEM_CONFIG3_PERIPH_VID_SHFT                                                             0x10
#define HWIO_OEM_CONFIG3_PERIPH_PID_BMSK                                                           0xffff
#define HWIO_OEM_CONFIG3_PERIPH_PID_SHFT                                                              0x0

#define HWIO_BOOT_CONFIG_ADDR                                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000602c)
#define HWIO_BOOT_CONFIG_PHYS                                                                  (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x0000602c)
#define HWIO_BOOT_CONFIG_RMSK                                                                       0x3ff
#define HWIO_BOOT_CONFIG_IN          \
        in_dword_masked(HWIO_BOOT_CONFIG_ADDR, HWIO_BOOT_CONFIG_RMSK)
#define HWIO_BOOT_CONFIG_INM(m)      \
        in_dword_masked(HWIO_BOOT_CONFIG_ADDR, m)
#define HWIO_BOOT_CONFIG_USB_SS_ENABLE_BMSK                                                         0x200
#define HWIO_BOOT_CONFIG_USB_SS_ENABLE_SHFT                                                           0x9
#define HWIO_BOOT_CONFIG_FORCE_MSA_AUTH_EN_BMSK                                                     0x100
#define HWIO_BOOT_CONFIG_FORCE_MSA_AUTH_EN_SHFT                                                       0x8
#define HWIO_BOOT_CONFIG_APPS_PBL_BOOT_SPEED_BMSK                                                    0xc0
#define HWIO_BOOT_CONFIG_APPS_PBL_BOOT_SPEED_SHFT                                                     0x6
#define HWIO_BOOT_CONFIG_APPS_BOOT_FROM_ROM_BMSK                                                     0x20
#define HWIO_BOOT_CONFIG_APPS_BOOT_FROM_ROM_SHFT                                                      0x5
#define HWIO_BOOT_CONFIG_FAST_BOOT_BMSK                                                              0x1e
#define HWIO_BOOT_CONFIG_FAST_BOOT_SHFT                                                               0x1
#define HWIO_BOOT_CONFIG_WDOG_EN_BMSK                                                                 0x1
#define HWIO_BOOT_CONFIG_WDOG_EN_SHFT                                                                 0x0

#define HWIO_SECURE_BOOTn_ADDR(n)                                                              (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006030 + 0x4 * (n))
#define HWIO_SECURE_BOOTn_PHYS(n)                                                              (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00006030 + 0x4 * (n))
#define HWIO_SECURE_BOOTn_RMSK                                                                      0x17f
#define HWIO_SECURE_BOOTn_MAXn                                                                          7
#define HWIO_SECURE_BOOTn_INI(n)        \
        in_dword_masked(HWIO_SECURE_BOOTn_ADDR(n), HWIO_SECURE_BOOTn_RMSK)
#define HWIO_SECURE_BOOTn_INMI(n,mask)    \
        in_dword_masked(HWIO_SECURE_BOOTn_ADDR(n), mask)
#define HWIO_SECURE_BOOTn_FUSE_SRC_BMSK                                                             0x100
#define HWIO_SECURE_BOOTn_FUSE_SRC_SHFT                                                               0x8
#define HWIO_SECURE_BOOTn_USE_SERIAL_NUM_BMSK                                                        0x40
#define HWIO_SECURE_BOOTn_USE_SERIAL_NUM_SHFT                                                         0x6
#define HWIO_SECURE_BOOTn_AUTH_EN_BMSK                                                               0x20
#define HWIO_SECURE_BOOTn_AUTH_EN_SHFT                                                                0x5
#define HWIO_SECURE_BOOTn_PK_HASH_IN_FUSE_BMSK                                                       0x10
#define HWIO_SECURE_BOOTn_PK_HASH_IN_FUSE_SHFT                                                        0x4
#define HWIO_SECURE_BOOTn_ROM_PK_HASH_INDEX_BMSK                                                      0xf
#define HWIO_SECURE_BOOTn_ROM_PK_HASH_INDEX_SHFT                                                      0x0

#define HWIO_OVERRIDE_0_ADDR                                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006058)
#define HWIO_OVERRIDE_0_PHYS                                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00006058)
#define HWIO_OVERRIDE_0_RMSK                                                                   0x10000003
#define HWIO_OVERRIDE_0_IN          \
        in_dword_masked(HWIO_OVERRIDE_0_ADDR, HWIO_OVERRIDE_0_RMSK)
#define HWIO_OVERRIDE_0_INM(m)      \
        in_dword_masked(HWIO_OVERRIDE_0_ADDR, m)
#define HWIO_OVERRIDE_0_OUT(v)      \
        out_dword(HWIO_OVERRIDE_0_ADDR,v)
#define HWIO_OVERRIDE_0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_OVERRIDE_0_ADDR,m,v,HWIO_OVERRIDE_0_IN)
#define HWIO_OVERRIDE_0_TX_DISABLE_BMSK                                                        0x10000000
#define HWIO_OVERRIDE_0_TX_DISABLE_SHFT                                                              0x1c
#define HWIO_OVERRIDE_0_SDC_EMMC_MODE1P2_EN_BMSK                                                      0x2
#define HWIO_OVERRIDE_0_SDC_EMMC_MODE1P2_EN_SHFT                                                      0x1
#define HWIO_OVERRIDE_0_SDC_EMMC_MODE1P2_OVERRIDE_BMSK                                                0x1
#define HWIO_OVERRIDE_0_SDC_EMMC_MODE1P2_OVERRIDE_SHFT                                                0x0

#define HWIO_OVERRIDE_1_ADDR                                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000605c)
#define HWIO_OVERRIDE_1_PHYS                                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x0000605c)
#define HWIO_OVERRIDE_1_RMSK                                                                   0xffffffff
#define HWIO_OVERRIDE_1_IN          \
        in_dword_masked(HWIO_OVERRIDE_1_ADDR, HWIO_OVERRIDE_1_RMSK)
#define HWIO_OVERRIDE_1_INM(m)      \
        in_dword_masked(HWIO_OVERRIDE_1_ADDR, m)
#define HWIO_OVERRIDE_1_OUT(v)      \
        out_dword(HWIO_OVERRIDE_1_ADDR,v)
#define HWIO_OVERRIDE_1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_OVERRIDE_1_ADDR,m,v,HWIO_OVERRIDE_1_IN)
#define HWIO_OVERRIDE_1_OVERRIDE_1_BMSK                                                        0xffffffff
#define HWIO_OVERRIDE_1_OVERRIDE_1_SHFT                                                               0x0

#define HWIO_OVERRIDE_2_ADDR                                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006060)
#define HWIO_OVERRIDE_2_PHYS                                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00006060)
#define HWIO_OVERRIDE_2_RMSK                                                                     0x3ffc00
#define HWIO_OVERRIDE_2_IN          \
        in_dword_masked(HWIO_OVERRIDE_2_ADDR, HWIO_OVERRIDE_2_RMSK)
#define HWIO_OVERRIDE_2_INM(m)      \
        in_dword_masked(HWIO_OVERRIDE_2_ADDR, m)
#define HWIO_OVERRIDE_2_OUT(v)      \
        out_dword(HWIO_OVERRIDE_2_ADDR,v)
#define HWIO_OVERRIDE_2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_OVERRIDE_2_ADDR,m,v,HWIO_OVERRIDE_2_IN)
#define HWIO_OVERRIDE_2_OVRID_DAP_DEVICEEN_DISABLE_BMSK                                          0x200000
#define HWIO_OVERRIDE_2_OVRID_DAP_DEVICEEN_DISABLE_SHFT                                              0x15
#define HWIO_OVERRIDE_2_OVRID_DAP_NIDEN_DISABLE_BMSK                                             0x100000
#define HWIO_OVERRIDE_2_OVRID_DAP_NIDEN_DISABLE_SHFT                                                 0x14
#define HWIO_OVERRIDE_2_OVRID_DAP_DBGEN_DISABLE_BMSK                                              0x80000
#define HWIO_OVERRIDE_2_OVRID_DAP_DBGEN_DISABLE_SHFT                                                 0x13
#define HWIO_OVERRIDE_2_OVRID_APPS_NIDEN_DISABLE_BMSK                                             0x40000
#define HWIO_OVERRIDE_2_OVRID_APPS_NIDEN_DISABLE_SHFT                                                0x12
#define HWIO_OVERRIDE_2_OVRID_APPS_DBGEN_DISABLE_BMSK                                             0x20000
#define HWIO_OVERRIDE_2_OVRID_APPS_DBGEN_DISABLE_SHFT                                                0x11
#define HWIO_OVERRIDE_2_OVRID_SPARE1_DISABLE_BMSK                                                 0x10000
#define HWIO_OVERRIDE_2_OVRID_SPARE1_DISABLE_SHFT                                                    0x10
#define HWIO_OVERRIDE_2_OVRID_SPARE0_DISABLE_BMSK                                                  0x8000
#define HWIO_OVERRIDE_2_OVRID_SPARE0_DISABLE_SHFT                                                     0xf
#define HWIO_OVERRIDE_2_OVRID_VENUS_0_DBGEN_DISABLE_BMSK                                           0x4000
#define HWIO_OVERRIDE_2_OVRID_VENUS_0_DBGEN_DISABLE_SHFT                                              0xe
#define HWIO_OVERRIDE_2_OVRID_RPM_DAPEN_DISABLE_BMSK                                               0x2000
#define HWIO_OVERRIDE_2_OVRID_RPM_DAPEN_DISABLE_SHFT                                                  0xd
#define HWIO_OVERRIDE_2_OVRID_RPM_WCSS_NIDEN_DISABLE_BMSK                                          0x1000
#define HWIO_OVERRIDE_2_OVRID_RPM_WCSS_NIDEN_DISABLE_SHFT                                             0xc
#define HWIO_OVERRIDE_2_OVRID_RPM_DBGEN_DISABLE_BMSK                                                0x800
#define HWIO_OVERRIDE_2_OVRID_RPM_DBGEN_DISABLE_SHFT                                                  0xb
#define HWIO_OVERRIDE_2_OVRID_WCSS_DBGEN_DISABLE_BMSK                                               0x400
#define HWIO_OVERRIDE_2_OVRID_WCSS_DBGEN_DISABLE_SHFT                                                 0xa

#define HWIO_OVERRIDE_3_ADDR                                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006064)
#define HWIO_OVERRIDE_3_PHYS                                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00006064)
#define HWIO_OVERRIDE_3_RMSK                                                                         0x1f
#define HWIO_OVERRIDE_3_IN          \
        in_dword_masked(HWIO_OVERRIDE_3_ADDR, HWIO_OVERRIDE_3_RMSK)
#define HWIO_OVERRIDE_3_INM(m)      \
        in_dword_masked(HWIO_OVERRIDE_3_ADDR, m)
#define HWIO_OVERRIDE_3_OUT(v)      \
        out_dword(HWIO_OVERRIDE_3_ADDR,v)
#define HWIO_OVERRIDE_3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_OVERRIDE_3_ADDR,m,v,HWIO_OVERRIDE_3_IN)
#define HWIO_OVERRIDE_3_OVRID_DAP_SPNIDEN_DISABLE_BMSK                                               0x10
#define HWIO_OVERRIDE_3_OVRID_DAP_SPNIDEN_DISABLE_SHFT                                                0x4
#define HWIO_OVERRIDE_3_OVRID_DAP_SPIDEN_DISABLE_BMSK                                                 0x8
#define HWIO_OVERRIDE_3_OVRID_DAP_SPIDEN_DISABLE_SHFT                                                 0x3
#define HWIO_OVERRIDE_3_OVRID_APPS_SPNIDEN_DISABLE_BMSK                                               0x4
#define HWIO_OVERRIDE_3_OVRID_APPS_SPNIDEN_DISABLE_SHFT                                               0x2
#define HWIO_OVERRIDE_3_OVRID_APPS_SPIDEN_DISABLE_BMSK                                                0x2
#define HWIO_OVERRIDE_3_OVRID_APPS_SPIDEN_DISABLE_SHFT                                                0x1
#define HWIO_OVERRIDE_3_OVRID_SPDM_SECURE_MODE_BMSK                                                   0x1
#define HWIO_OVERRIDE_3_OVRID_SPDM_SECURE_MODE_SHFT                                                   0x0

#define HWIO_OVERRIDE_4_ADDR                                                                   (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006068)
#define HWIO_OVERRIDE_4_PHYS                                                                   (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00006068)
#define HWIO_OVERRIDE_4_RMSK                                                                          0x3
#define HWIO_OVERRIDE_4_IN          \
        in_dword_masked(HWIO_OVERRIDE_4_ADDR, HWIO_OVERRIDE_4_RMSK)
#define HWIO_OVERRIDE_4_INM(m)      \
        in_dword_masked(HWIO_OVERRIDE_4_ADDR, m)
#define HWIO_OVERRIDE_4_OUT(v)      \
        out_dword(HWIO_OVERRIDE_4_ADDR,v)
#define HWIO_OVERRIDE_4_OUTM(m,v) \
        out_dword_masked_ns(HWIO_OVERRIDE_4_ADDR,m,v,HWIO_OVERRIDE_4_IN)
#define HWIO_OVERRIDE_4_OVRID_MSS_NIDEN_DISABLE_BMSK                                                  0x2
#define HWIO_OVERRIDE_4_OVRID_MSS_NIDEN_DISABLE_SHFT                                                  0x1
#define HWIO_OVERRIDE_4_OVRID_MSS_DBGEN_DISABLE_BMSK                                                  0x1
#define HWIO_OVERRIDE_4_OVRID_MSS_DBGEN_DISABLE_SHFT                                                  0x0

#define HWIO_CAPT_SEC_GPIO_ADDR                                                                (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000606c)
#define HWIO_CAPT_SEC_GPIO_PHYS                                                                (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x0000606c)
#define HWIO_CAPT_SEC_GPIO_RMSK                                                                   0x1ffff
#define HWIO_CAPT_SEC_GPIO_IN          \
        in_dword_masked(HWIO_CAPT_SEC_GPIO_ADDR, HWIO_CAPT_SEC_GPIO_RMSK)
#define HWIO_CAPT_SEC_GPIO_INM(m)      \
        in_dword_masked(HWIO_CAPT_SEC_GPIO_ADDR, m)
#define HWIO_CAPT_SEC_GPIO_OUT(v)      \
        out_dword(HWIO_CAPT_SEC_GPIO_ADDR,v)
#define HWIO_CAPT_SEC_GPIO_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CAPT_SEC_GPIO_ADDR,m,v,HWIO_CAPT_SEC_GPIO_IN)
#define HWIO_CAPT_SEC_GPIO_SDC_EMMC_MODE1P2_EN_BMSK                                               0x10000
#define HWIO_CAPT_SEC_GPIO_SDC_EMMC_MODE1P2_EN_SHFT                                                  0x10
#define HWIO_CAPT_SEC_GPIO_FORCE_USB_BOOT_GPIO_BMSK                                                0x8000
#define HWIO_CAPT_SEC_GPIO_FORCE_USB_BOOT_GPIO_SHFT                                                   0xf
#define HWIO_CAPT_SEC_GPIO_BOOT_CONFIG_GPIO_BMSK                                                   0x7fff
#define HWIO_CAPT_SEC_GPIO_BOOT_CONFIG_GPIO_SHFT                                                      0x0

#define HWIO_APP_PROC_CFG_ADDR                                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006070)
#define HWIO_APP_PROC_CFG_PHYS                                                                 (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00006070)
#define HWIO_APP_PROC_CFG_RMSK                                                                   0xffffff
#define HWIO_APP_PROC_CFG_IN          \
        in_dword_masked(HWIO_APP_PROC_CFG_ADDR, HWIO_APP_PROC_CFG_RMSK)
#define HWIO_APP_PROC_CFG_INM(m)      \
        in_dword_masked(HWIO_APP_PROC_CFG_ADDR, m)
#define HWIO_APP_PROC_CFG_OUT(v)      \
        out_dword(HWIO_APP_PROC_CFG_ADDR,v)
#define HWIO_APP_PROC_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APP_PROC_CFG_ADDR,m,v,HWIO_APP_PROC_CFG_IN)
#define HWIO_APP_PROC_CFG_WCSS_DBG_NIDEN_BMSK                                                    0x800000
#define HWIO_APP_PROC_CFG_WCSS_DBG_NIDEN_SHFT                                                        0x17
#define HWIO_APP_PROC_CFG_RPM_DBG_NIDEN_BMSK                                                     0x400000
#define HWIO_APP_PROC_CFG_RPM_DBG_NIDEN_SHFT                                                         0x16
#define HWIO_APP_PROC_CFG_DAP_DBG_NIDEN_BMSK                                                     0x200000
#define HWIO_APP_PROC_CFG_DAP_DBG_NIDEN_SHFT                                                         0x15
#define HWIO_APP_PROC_CFG_DAP_DBG_SPNIDEN_BMSK                                                   0x100000
#define HWIO_APP_PROC_CFG_DAP_DBG_SPNIDEN_SHFT                                                       0x14
#define HWIO_APP_PROC_CFG_APPS_CFG_MISC3_BMSK                                                     0xf0000
#define HWIO_APP_PROC_CFG_APPS_CFG_MISC3_SHFT                                                        0x10
#define HWIO_APP_PROC_CFG_APPS_CFG_MISC2_BMSK                                                      0xf000
#define HWIO_APP_PROC_CFG_APPS_CFG_MISC2_SHFT                                                         0xc
#define HWIO_APP_PROC_CFG_APPS_CFG_MISC1_BMSK                                                       0xf00
#define HWIO_APP_PROC_CFG_APPS_CFG_MISC1_SHFT                                                         0x8
#define HWIO_APP_PROC_CFG_APPS_CFG_MISC0_BMSK                                                        0xf0
#define HWIO_APP_PROC_CFG_APPS_CFG_MISC0_SHFT                                                         0x4
#define HWIO_APP_PROC_CFG_APPS_DBG_NIDEN_BMSK                                                         0x8
#define HWIO_APP_PROC_CFG_APPS_DBG_NIDEN_SHFT                                                         0x3
#define HWIO_APP_PROC_CFG_APPS_DBG_SPNIDEN_BMSK                                                       0x4
#define HWIO_APP_PROC_CFG_APPS_DBG_SPNIDEN_SHFT                                                       0x2
#define HWIO_APP_PROC_CFG_APPS_CP15_DISABLE_BMSK                                                      0x2
#define HWIO_APP_PROC_CFG_APPS_CP15_DISABLE_SHFT                                                      0x1
#define HWIO_APP_PROC_CFG_APPS_CFG_NMFI_BMSK                                                          0x1
#define HWIO_APP_PROC_CFG_APPS_CFG_NMFI_SHFT                                                          0x0

#define HWIO_MSS_PROC_CFG_ADDR                                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006074)
#define HWIO_MSS_PROC_CFG_PHYS                                                                 (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00006074)
#define HWIO_MSS_PROC_CFG_RMSK                                                                        0x1
#define HWIO_MSS_PROC_CFG_IN          \
        in_dword_masked(HWIO_MSS_PROC_CFG_ADDR, HWIO_MSS_PROC_CFG_RMSK)
#define HWIO_MSS_PROC_CFG_INM(m)      \
        in_dword_masked(HWIO_MSS_PROC_CFG_ADDR, m)
#define HWIO_MSS_PROC_CFG_OUT(v)      \
        out_dword(HWIO_MSS_PROC_CFG_ADDR,v)
#define HWIO_MSS_PROC_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_PROC_CFG_ADDR,m,v,HWIO_MSS_PROC_CFG_IN)
#define HWIO_MSS_PROC_CFG_MSS_DBG_NIDEN_BMSK                                                          0x1
#define HWIO_MSS_PROC_CFG_MSS_DBG_NIDEN_SHFT                                                          0x0

#define HWIO_QFPROM_CLK_CTL_ADDR                                                               (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006078)
#define HWIO_QFPROM_CLK_CTL_PHYS                                                               (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00006078)
#define HWIO_QFPROM_CLK_CTL_RMSK                                                                      0x1
#define HWIO_QFPROM_CLK_CTL_IN          \
        in_dword_masked(HWIO_QFPROM_CLK_CTL_ADDR, HWIO_QFPROM_CLK_CTL_RMSK)
#define HWIO_QFPROM_CLK_CTL_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CLK_CTL_ADDR, m)
#define HWIO_QFPROM_CLK_CTL_OUT(v)      \
        out_dword(HWIO_QFPROM_CLK_CTL_ADDR,v)
#define HWIO_QFPROM_CLK_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_CLK_CTL_ADDR,m,v,HWIO_QFPROM_CLK_CTL_IN)
#define HWIO_QFPROM_CLK_CTL_CLK_HALT_BMSK                                                             0x1
#define HWIO_QFPROM_CLK_CTL_CLK_HALT_SHFT                                                             0x0

#define HWIO_JTAG_ID_ADDR                                                                      (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000607c)
#define HWIO_JTAG_ID_PHYS                                                                      (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x0000607c)
#define HWIO_JTAG_ID_RMSK                                                                      0xffffffff
#define HWIO_JTAG_ID_IN          \
        in_dword_masked(HWIO_JTAG_ID_ADDR, HWIO_JTAG_ID_RMSK)
#define HWIO_JTAG_ID_INM(m)      \
        in_dword_masked(HWIO_JTAG_ID_ADDR, m)
#define HWIO_JTAG_ID_JTAG_ID_BMSK                                                              0xffffffff
#define HWIO_JTAG_ID_JTAG_ID_SHFT                                                                     0x0

#define HWIO_OEM_ID_ADDR                                                                       (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006080)
#define HWIO_OEM_ID_PHYS                                                                       (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00006080)
#define HWIO_OEM_ID_RMSK                                                                       0xffffffff
#define HWIO_OEM_ID_IN          \
        in_dword_masked(HWIO_OEM_ID_ADDR, HWIO_OEM_ID_RMSK)
#define HWIO_OEM_ID_INM(m)      \
        in_dword_masked(HWIO_OEM_ID_ADDR, m)
#define HWIO_OEM_ID_OEM_ID_BMSK                                                                0xffff0000
#define HWIO_OEM_ID_OEM_ID_SHFT                                                                      0x10
#define HWIO_OEM_ID_OEM_PRODUCT_ID_BMSK                                                            0xffff
#define HWIO_OEM_ID_OEM_PRODUCT_ID_SHFT                                                               0x0

#define HWIO_TEST_BUS_SEL_ADDR                                                                 (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006084)
#define HWIO_TEST_BUS_SEL_PHYS                                                                 (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00006084)
#define HWIO_TEST_BUS_SEL_RMSK                                                                        0xf
#define HWIO_TEST_BUS_SEL_IN          \
        in_dword_masked(HWIO_TEST_BUS_SEL_ADDR, HWIO_TEST_BUS_SEL_RMSK)
#define HWIO_TEST_BUS_SEL_INM(m)      \
        in_dword_masked(HWIO_TEST_BUS_SEL_ADDR, m)
#define HWIO_TEST_BUS_SEL_OUT(v)      \
        out_dword(HWIO_TEST_BUS_SEL_ADDR,v)
#define HWIO_TEST_BUS_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TEST_BUS_SEL_ADDR,m,v,HWIO_TEST_BUS_SEL_IN)
#define HWIO_TEST_BUS_SEL_TEST_EN_BMSK                                                                0x8
#define HWIO_TEST_BUS_SEL_TEST_EN_SHFT                                                                0x3
#define HWIO_TEST_BUS_SEL_TEST_SELECT_BMSK                                                            0x7
#define HWIO_TEST_BUS_SEL_TEST_SELECT_SHFT                                                            0x0

#define HWIO_SPDM_DYN_SECURE_MODE_ADDR                                                         (SECURITY_CONTROL_CORE_REG_BASE      + 0x00006088)
#define HWIO_SPDM_DYN_SECURE_MODE_PHYS                                                         (SECURITY_CONTROL_CORE_REG_BASE_PHYS + 0x00006088)
#define HWIO_SPDM_DYN_SECURE_MODE_RMSK                                                                0x1
#define HWIO_SPDM_DYN_SECURE_MODE_IN          \
        in_dword_masked(HWIO_SPDM_DYN_SECURE_MODE_ADDR, HWIO_SPDM_DYN_SECURE_MODE_RMSK)
#define HWIO_SPDM_DYN_SECURE_MODE_INM(m)      \
        in_dword_masked(HWIO_SPDM_DYN_SECURE_MODE_ADDR, m)
#define HWIO_SPDM_DYN_SECURE_MODE_OUT(v)      \
        out_dword(HWIO_SPDM_DYN_SECURE_MODE_ADDR,v)
#define HWIO_SPDM_DYN_SECURE_MODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_SPDM_DYN_SECURE_MODE_ADDR,m,v,HWIO_SPDM_DYN_SECURE_MODE_IN)
#define HWIO_SPDM_DYN_SECURE_MODE_SECURE_MODE_BMSK                                                    0x1
#define HWIO_SPDM_DYN_SECURE_MODE_SECURE_MODE_SHFT                                                    0x0

/*----------------------------------------------------------------------------
 * MODULE: TCSR_TCSR_REGS
 *--------------------------------------------------------------------------*/

#define TCSR_TCSR_REGS_REG_BASE                                                                             (CORE_TOP_CSR_BASE      + 0x00037000)
#define TCSR_TCSR_REGS_REG_BASE_PHYS                                                                        (CORE_TOP_CSR_BASE_PHYS + 0x00037000)

#define HWIO_TCSR_BIMC_CFG_QRIB_XPU2_INIT_ADDR                                                              (TCSR_TCSR_REGS_REG_BASE      + 0x00000100)
#define HWIO_TCSR_BIMC_CFG_QRIB_XPU2_INIT_PHYS                                                              (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00000100)
#define HWIO_TCSR_BIMC_CFG_QRIB_XPU2_INIT_RMSK                                                                 0x10001
#define HWIO_TCSR_BIMC_CFG_QRIB_XPU2_INIT_IN          \
        in_dword_masked(HWIO_TCSR_BIMC_CFG_QRIB_XPU2_INIT_ADDR, HWIO_TCSR_BIMC_CFG_QRIB_XPU2_INIT_RMSK)
#define HWIO_TCSR_BIMC_CFG_QRIB_XPU2_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_BIMC_CFG_QRIB_XPU2_INIT_ADDR, m)
#define HWIO_TCSR_BIMC_CFG_QRIB_XPU2_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_BIMC_CFG_QRIB_XPU2_INIT_ADDR,v)
#define HWIO_TCSR_BIMC_CFG_QRIB_XPU2_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_BIMC_CFG_QRIB_XPU2_INIT_ADDR,m,v,HWIO_TCSR_BIMC_CFG_QRIB_XPU2_INIT_IN)
#define HWIO_TCSR_BIMC_CFG_QRIB_XPU2_INIT_BIMC_CFG_QRIB_XPU2_NSEN_INIT_BMSK                                    0x10000
#define HWIO_TCSR_BIMC_CFG_QRIB_XPU2_INIT_BIMC_CFG_QRIB_XPU2_NSEN_INIT_SHFT                                       0x10
#define HWIO_TCSR_BIMC_CFG_QRIB_XPU2_INIT_BIMC_CFG_QRIB_XPU2_EN_TZ_BMSK                                            0x1
#define HWIO_TCSR_BIMC_CFG_QRIB_XPU2_INIT_BIMC_CFG_QRIB_XPU2_EN_TZ_SHFT                                            0x0

#define HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_INIT_ADDR                                                             (TCSR_TCSR_REGS_REG_BASE      + 0x00000104)
#define HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_INIT_PHYS                                                             (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00000104)
#define HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_INIT_RMSK                                                                0x10001
#define HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_INIT_IN          \
        in_dword_masked(HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_INIT_ADDR, HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_INIT_RMSK)
#define HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_INIT_ADDR, m)
#define HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_INIT_ADDR,v)
#define HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_INIT_ADDR,m,v,HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_INIT_IN)
#define HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_INIT_BIMC_QXS0_QRIB_XPU2_NSEN_INIT_BMSK                                  0x10000
#define HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_INIT_BIMC_QXS0_QRIB_XPU2_NSEN_INIT_SHFT                                     0x10
#define HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_INIT_BIMC_QXS0_QRIB_XPU2_EN_TZ_BMSK                                          0x1
#define HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_INIT_BIMC_QXS0_QRIB_XPU2_EN_TZ_SHFT                                          0x0

#define HWIO_TCSR_BIMC_CFG_QRIB_XPU2_ACR_ADDR                                                               (TCSR_TCSR_REGS_REG_BASE      + 0x00001000)
#define HWIO_TCSR_BIMC_CFG_QRIB_XPU2_ACR_PHYS                                                               (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00001000)
#define HWIO_TCSR_BIMC_CFG_QRIB_XPU2_ACR_RMSK                                                               0xffffffff
#define HWIO_TCSR_BIMC_CFG_QRIB_XPU2_ACR_IN          \
        in_dword_masked(HWIO_TCSR_BIMC_CFG_QRIB_XPU2_ACR_ADDR, HWIO_TCSR_BIMC_CFG_QRIB_XPU2_ACR_RMSK)
#define HWIO_TCSR_BIMC_CFG_QRIB_XPU2_ACR_INM(m)      \
        in_dword_masked(HWIO_TCSR_BIMC_CFG_QRIB_XPU2_ACR_ADDR, m)
#define HWIO_TCSR_BIMC_CFG_QRIB_XPU2_ACR_OUT(v)      \
        out_dword(HWIO_TCSR_BIMC_CFG_QRIB_XPU2_ACR_ADDR,v)
#define HWIO_TCSR_BIMC_CFG_QRIB_XPU2_ACR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_BIMC_CFG_QRIB_XPU2_ACR_ADDR,m,v,HWIO_TCSR_BIMC_CFG_QRIB_XPU2_ACR_IN)
#define HWIO_TCSR_BIMC_CFG_QRIB_XPU2_ACR_BIMC_CFG_QRIB_XPU2_ACR_BMSK                                        0xffffffff
#define HWIO_TCSR_BIMC_CFG_QRIB_XPU2_ACR_BIMC_CFG_QRIB_XPU2_ACR_SHFT                                               0x0

#define HWIO_TCSR_BIMC_CFG_QRIB_XPU2_VMIDEN_INIT_ADDR                                                       (TCSR_TCSR_REGS_REG_BASE      + 0x00001004)
#define HWIO_TCSR_BIMC_CFG_QRIB_XPU2_VMIDEN_INIT_PHYS                                                       (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00001004)
#define HWIO_TCSR_BIMC_CFG_QRIB_XPU2_VMIDEN_INIT_RMSK                                                          0x10001
#define HWIO_TCSR_BIMC_CFG_QRIB_XPU2_VMIDEN_INIT_IN          \
        in_dword_masked(HWIO_TCSR_BIMC_CFG_QRIB_XPU2_VMIDEN_INIT_ADDR, HWIO_TCSR_BIMC_CFG_QRIB_XPU2_VMIDEN_INIT_RMSK)
#define HWIO_TCSR_BIMC_CFG_QRIB_XPU2_VMIDEN_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_BIMC_CFG_QRIB_XPU2_VMIDEN_INIT_ADDR, m)
#define HWIO_TCSR_BIMC_CFG_QRIB_XPU2_VMIDEN_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_BIMC_CFG_QRIB_XPU2_VMIDEN_INIT_ADDR,v)
#define HWIO_TCSR_BIMC_CFG_QRIB_XPU2_VMIDEN_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_BIMC_CFG_QRIB_XPU2_VMIDEN_INIT_ADDR,m,v,HWIO_TCSR_BIMC_CFG_QRIB_XPU2_VMIDEN_INIT_IN)
#define HWIO_TCSR_BIMC_CFG_QRIB_XPU2_VMIDEN_INIT_BIMC_CFG_QRIB_XPU2_VMIDEN_INIT_BMSK                           0x10000
#define HWIO_TCSR_BIMC_CFG_QRIB_XPU2_VMIDEN_INIT_BIMC_CFG_QRIB_XPU2_VMIDEN_INIT_SHFT                              0x10
#define HWIO_TCSR_BIMC_CFG_QRIB_XPU2_VMIDEN_INIT_BIMC_CFG_QRIB_XPU2_VMIDEN_INIT_EN_HV_BMSK                         0x1
#define HWIO_TCSR_BIMC_CFG_QRIB_XPU2_VMIDEN_INIT_BIMC_CFG_QRIB_XPU2_VMIDEN_INIT_EN_HV_SHFT                         0x0

#define HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_ACR_ADDR                                                              (TCSR_TCSR_REGS_REG_BASE      + 0x00001008)
#define HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_ACR_PHYS                                                              (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00001008)
#define HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_ACR_RMSK                                                              0xffffffff
#define HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_ACR_IN          \
        in_dword_masked(HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_ACR_ADDR, HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_ACR_RMSK)
#define HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_ACR_INM(m)      \
        in_dword_masked(HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_ACR_ADDR, m)
#define HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_ACR_OUT(v)      \
        out_dword(HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_ACR_ADDR,v)
#define HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_ACR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_ACR_ADDR,m,v,HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_ACR_IN)
#define HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_ACR_BIMC_QXS0_QRIB_XPU2_ACR_BMSK                                      0xffffffff
#define HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_ACR_BIMC_QXS0_QRIB_XPU2_ACR_SHFT                                             0x0

#define HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_VMIDEN_INIT_ADDR                                                      (TCSR_TCSR_REGS_REG_BASE      + 0x0000100c)
#define HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_VMIDEN_INIT_PHYS                                                      (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x0000100c)
#define HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_VMIDEN_INIT_RMSK                                                         0x10001
#define HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_VMIDEN_INIT_IN          \
        in_dword_masked(HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_VMIDEN_INIT_ADDR, HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_VMIDEN_INIT_RMSK)
#define HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_VMIDEN_INIT_INM(m)      \
        in_dword_masked(HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_VMIDEN_INIT_ADDR, m)
#define HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_VMIDEN_INIT_OUT(v)      \
        out_dword(HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_VMIDEN_INIT_ADDR,v)
#define HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_VMIDEN_INIT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_VMIDEN_INIT_ADDR,m,v,HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_VMIDEN_INIT_IN)
#define HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_VMIDEN_INIT_BIMC_QXS0_QRIB_XPU2_VMIDEN_INIT_BMSK                         0x10000
#define HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_VMIDEN_INIT_BIMC_QXS0_QRIB_XPU2_VMIDEN_INIT_SHFT                            0x10
#define HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_VMIDEN_INIT_BIMC_QXS0_QRIB_XPU2_VMIDEN_INIT_EN_HV_BMSK                       0x1
#define HWIO_TCSR_BIMC_QXS0_QRIB_XPU2_VMIDEN_INIT_BIMC_QXS0_QRIB_XPU2_VMIDEN_INIT_EN_HV_SHFT                       0x0

#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ADDR                                                                (TCSR_TCSR_REGS_REG_BASE      + 0x00002000)
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_PHYS                                                                (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00002000)
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_RMSK                                                                0x1f7ffa6f
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_IN          \
        in_dword_masked(HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ADDR, HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_RMSK)
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ADDR, m)
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_O_XPU2_NON_SECURE_INTR_CE_BMSK                                      0x10000000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_O_XPU2_NON_SECURE_INTR_CE_SHFT                                            0x1c
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_O_XPU2_NON_SECURE_INTR_IPA_BMSK                                      0x8000000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_O_XPU2_NON_SECURE_INTR_IPA_SHFT                                           0x1b
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_BLSP1_APU_NON_SEC_ERROR_IRQ_BMSK                                     0x4000000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_BLSP1_APU_NON_SEC_ERROR_IRQ_SHFT                                          0x1a
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_SMMU_MPU_NON_SECURE_INTR_BMSK                                        0x2000000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_SMMU_MPU_NON_SECURE_INTR_SHFT                                             0x19
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_O_XPU2_NON_SECURE_INTR_QDSP_BMSK                                     0x1000000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_O_XPU2_NON_SECURE_INTR_QDSP_SHFT                                          0x18
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_O_XPU2_NON_SECURE_INTR_MCDMA_BMSK                                     0x400000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_O_XPU2_NON_SECURE_INTR_MCDMA_SHFT                                         0x16
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_RPM_CFG_XPU2_NON_SECURE_INTR_BMSK                                     0x200000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_RPM_CFG_XPU2_NON_SECURE_INTR_SHFT                                         0x15
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_PRNG_CFG_XPU2_NON_SECURE_INTR_BMSK                                    0x100000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_PRNG_CFG_XPU2_NON_SECURE_INTR_SHFT                                        0x14
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_PCNOC_CFG_XPU2_NON_SECURE_INTR_BMSK                                    0x80000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_PCNOC_CFG_XPU2_NON_SECURE_INTR_SHFT                                       0x13
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_SNOC_CFG_XPU2_NON_SECURE_INTR_BMSK                                     0x40000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_SNOC_CFG_XPU2_NON_SECURE_INTR_SHFT                                        0x12
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_OCIMEM_MPU_NON_SECURE_INTR_BMSK                                        0x20000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_OCIMEM_MPU_NON_SECURE_INTR_SHFT                                           0x11
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_SEC_CTRL_XPU2_NON_SEC_IRQ_BMSK                                         0x10000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_SEC_CTRL_XPU2_NON_SEC_IRQ_SHFT                                            0x10
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_DEHR_XPU_NON_SECURE_INTR_BMSK                                           0x8000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_DEHR_XPU_NON_SECURE_INTR_SHFT                                              0xf
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_MPM_XPU2_NON_SECURE_INTR_BMSK                                           0x4000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_MPM_XPU2_NON_SECURE_INTR_SHFT                                              0xe
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_BOOT_ROM_NON_SECURE_INTR_BMSK                                           0x2000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_BOOT_ROM_NON_SECURE_INTR_SHFT                                              0xd
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_CRYPTO0_BAM_APU_NON_SEC_ERROR_IRQ_BMSK                                  0x1000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_CRYPTO0_BAM_APU_NON_SEC_ERROR_IRQ_SHFT                                     0xc
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_O_TCSR_REGS_NON_SECURE_INTR_BMSK                                         0x800
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_O_TCSR_REGS_NON_SECURE_INTR_SHFT                                           0xb
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_TLMM_XPU_NON_SECURE_INTR_BMSK                                            0x200
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_TLMM_XPU_NON_SECURE_INTR_SHFT                                              0x9
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_PMIC_ARB_MPU_NON_SECURE_INTR_BMSK                                         0x40
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_PMIC_ARB_MPU_NON_SECURE_INTR_SHFT                                          0x6
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_BIMC_CH0_XPU2_NS_INTERRUPT_BMSK                                           0x20
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_BIMC_CH0_XPU2_NS_INTERRUPT_SHFT                                            0x5
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_GCC_XPU_NON_SECURE_INTR_BMSK                                               0x8
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_GCC_XPU_NON_SECURE_INTR_SHFT                                               0x3
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_QPIC_XPU2_NON_SEC_ERROR_IRQ_BMSK                                           0x4
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_QPIC_XPU2_NON_SEC_ERROR_IRQ_SHFT                                           0x2
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_RPM_APU_NON_SEC_INTR_BMSK                                                  0x2
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_RPM_APU_NON_SEC_INTR_SHFT                                                  0x1
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_IPA_BAM_APU_NON_SEC_ERROR_IRQ_BMSK                                         0x1
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_IPA_BAM_APU_NON_SEC_ERROR_IRQ_SHFT                                         0x0

#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_ADDR                                                         (TCSR_TCSR_REGS_REG_BASE      + 0x00002040)
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_PHYS                                                         (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00002040)
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_RMSK                                                         0x1f7ffa6f
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_IN          \
        in_dword_masked(HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_ADDR, HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_RMSK)
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_ADDR, m)
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_OUT(v)      \
        out_dword(HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_ADDR,v)
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_ADDR,m,v,HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_IN)
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_O_XPU2_NON_SECURE_INTR_CE_ENABLE_BMSK                        0x10000000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_O_XPU2_NON_SECURE_INTR_CE_ENABLE_SHFT                              0x1c
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_O_XPU2_NON_SECURE_INTR_IPA_ENABLE_BMSK                        0x8000000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_O_XPU2_NON_SECURE_INTR_IPA_ENABLE_SHFT                             0x1b
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_BLSP1_APU_NON_SEC_ERROR_IRQ_ENABLE_BMSK                       0x4000000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_BLSP1_APU_NON_SEC_ERROR_IRQ_ENABLE_SHFT                            0x1a
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_SMMU_MPU_NON_SECURE_INTR_ENABLE_BMSK                          0x2000000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_SMMU_MPU_NON_SECURE_INTR_ENABLE_SHFT                               0x19
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_O_XPU2_NON_SECURE_INTR_QDSP_ENABLE_BMSK                       0x1000000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_O_XPU2_NON_SECURE_INTR_QDSP_ENABLE_SHFT                            0x18
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_O_XPU2_NON_SECURE_INTR_MCDMA_ENABLE_BMSK                       0x400000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_O_XPU2_NON_SECURE_INTR_MCDMA_ENABLE_SHFT                           0x16
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_RPM_CFG_XPU2_NON_SECURE_INTR_ENABLE_BMSK                       0x200000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_RPM_CFG_XPU2_NON_SECURE_INTR_ENABLE_SHFT                           0x15
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_PRNG_CFG_XPU2_NON_SECURE_INTR_ENABLE_BMSK                      0x100000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_PRNG_CFG_XPU2_NON_SECURE_INTR_ENABLE_SHFT                          0x14
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_PCNOC_CFG_XPU2_NON_SECURE_INTR_ENABLE_BMSK                      0x80000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_PCNOC_CFG_XPU2_NON_SECURE_INTR_ENABLE_SHFT                         0x13
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_SNOC_CFG_XPU2_NON_SECURE_INTR_ENABLE_BMSK                       0x40000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_SNOC_CFG_XPU2_NON_SECURE_INTR_ENABLE_SHFT                          0x12
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_OCIMEM_MPU_NON_SECURE_INTR_ENABLE_BMSK                          0x20000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_OCIMEM_MPU_NON_SECURE_INTR_ENABLE_SHFT                             0x11
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_SEC_CTRL_XPU2_NON_SEC_IRQ_ENABLE_BMSK                           0x10000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_SEC_CTRL_XPU2_NON_SEC_IRQ_ENABLE_SHFT                              0x10
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_DEHR_XPU_NON_SECURE_INTR_ENABLE_BMSK                             0x8000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_DEHR_XPU_NON_SECURE_INTR_ENABLE_SHFT                                0xf
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_MPM_XPU2_NON_SECURE_INTR_ENABLE_BMSK                             0x4000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_MPM_XPU2_NON_SECURE_INTR_ENABLE_SHFT                                0xe
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_BOOT_ROM_NON_SECURE_INTRQ_ENABLE_BMSK                            0x2000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_BOOT_ROM_NON_SECURE_INTRQ_ENABLE_SHFT                               0xd
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_CRYPTO0_BAM_APU_NON_SEC_ERROR_IRQ_ENABLE_BMSK                    0x1000
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_CRYPTO0_BAM_APU_NON_SEC_ERROR_IRQ_ENABLE_SHFT                       0xc
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_O_TCSR_REGS_NON_SECURE_INTR_ENABLE_BMSK                           0x800
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_O_TCSR_REGS_NON_SECURE_INTR_ENABLE_SHFT                             0xb
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_TLMM_XPU_NON_SECURE_INTR_ENABLE_BMSK                              0x200
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_TLMM_XPU_NON_SECURE_INTR_ENABLE_SHFT                                0x9
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_PMIC_ARB_MPU_NON_SECURE_INTR_ENABLE_BMSK                           0x40
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_PMIC_ARB_MPU_NON_SECURE_INTR_ENABLE_SHFT                            0x6
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_BIMC_CH0_XPU2_NS_INTERRUPT_ENABLE_BMSK                             0x20
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_BIMC_CH0_XPU2_NS_INTERRUPT_ENABLE_SHFT                              0x5
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_GCC_XPU_NON_SECURE_INTR_ENABLE_BMSK                                 0x8
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_GCC_XPU_NON_SECURE_INTR_ENABLE_SHFT                                 0x3
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_QPIC_XPU2_NON_SEC_ERROR_IRQ_ENABLE_BMSK                             0x4
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_QPIC_XPU2_NON_SEC_ERROR_IRQ_ENABLE_SHFT                             0x2
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_RPM_APU_NON_SEC_INTR_ENABLE_BMSK                                    0x2
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_RPM_APU_NON_SEC_INTR_ENABLE_SHFT                                    0x1
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_IPA_BAM_APU_NON_SEC_ERROR_IRQ_ENABLE_BMSK                           0x1
#define HWIO_TCSR_SS_XPU2_NON_SEC_INTR0_ENABLE_IPA_BAM_APU_NON_SEC_ERROR_IRQ_ENABLE_SHFT                           0x0

#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ADDR                                                        (TCSR_TCSR_REGS_REG_BASE      + 0x00002010)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_PHYS                                                        (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00002010)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_RMSK                                                              0x3f
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_IN          \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ADDR, HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_RMSK)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ADDR, m)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_QPIC_BAM_VMIDMT_NSGIRPT_IRQ_BMSK                                  0x20
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_QPIC_BAM_VMIDMT_NSGIRPT_IRQ_SHFT                                   0x5
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_IPA_VMIDMT_NSGIRPT_BMSK                                           0x10
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_IPA_VMIDMT_NSGIRPT_SHFT                                            0x4
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_DEHR_VMIDMT_NSGIRPT_BMSK                                           0x8
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_DEHR_VMIDMT_NSGIRPT_SHFT                                           0x3
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_CRYPTO0_VMIDMT_NSGIRPT_IRQ_BMSK                                    0x4
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_CRYPTO0_VMIDMT_NSGIRPT_IRQ_SHFT                                    0x2
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_BLSP1_VMIDMT_NSGIRPT_BMSK                                          0x2
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_BLSP1_VMIDMT_NSGIRPT_SHFT                                          0x1
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_RPM_VMIDMT_NSG_IRQ_BMSK                                            0x1
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_RPM_VMIDMT_NSG_IRQ_SHFT                                            0x0

#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_ADDR                                                 (TCSR_TCSR_REGS_REG_BASE      + 0x00002050)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_PHYS                                                 (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00002050)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_RMSK                                                       0x3f
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_IN          \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_ADDR, HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_RMSK)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_ADDR, m)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_OUT(v)      \
        out_dword(HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_ADDR,v)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_ADDR,m,v,HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_IN)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_QPIC_BAM_VMIDMT_NSGIRPT_IRQ_ENABLE_BMSK                    0x20
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_QPIC_BAM_VMIDMT_NSGIRPT_IRQ_ENABLE_SHFT                     0x5
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_IPA_VMIDMT_NSGIRPT_ENABLE_BMSK                             0x10
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_IPA_VMIDMT_NSGIRPT_ENABLE_SHFT                              0x4
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_DEHR_VMIDMT_NSGIRPT_ENABLE_BMSK                             0x8
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_DEHR_VMIDMT_NSGIRPT_ENABLE_SHFT                             0x3
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_CRYPTO0_VMIDMT_NSGIRPT_IRQ_ENABLE_BMSK                      0x4
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_CRYPTO0_VMIDMT_NSGIRPT_IRQ_ENABLE_SHFT                      0x2
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_BLSP1_VMIDMT_NSGIRPT_ENABLE_BMSK                            0x2
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_BLSP1_VMIDMT_NSGIRPT_ENABLE_SHFT                            0x1
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_RPM_VMIDMT_NSG_IRQ_ENABLE_BMSK                              0x1
#define HWIO_TCSR_SS_VMIDMT_CLIENT_NON_SEC_INTR_ENABLE_RPM_VMIDMT_NSG_IRQ_ENABLE_SHFT                              0x0

#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ADDR                                                           (TCSR_TCSR_REGS_REG_BASE      + 0x00002090)
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_PHYS                                                           (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00002090)
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_RMSK                                                                 0x3f
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_IN          \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ADDR, HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_RMSK)
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ADDR, m)
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_QPIC_BAM_VMIDMT_NSGCFGIRPT_IRQ_BMSK                                  0x20
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_QPIC_BAM_VMIDMT_NSGCFGIRPT_IRQ_SHFT                                   0x5
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_IPA_VMIDMT_NSGCFGIRPT_BMSK                                           0x10
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_IPA_VMIDMT_NSGCFGIRPT_SHFT                                            0x4
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_DEHR_VMIDMT_NSGCFGIRPT_BMSK                                           0x8
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_DEHR_VMIDMT_NSGCFGIRPT_SHFT                                           0x3
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_CRYPTO0_VMIDMT_NSGCFGIRPT_IRQ_BMSK                                    0x4
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_CRYPTO0_VMIDMT_NSGCFGIRPT_IRQ_SHFT                                    0x2
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_BLSP1_VMIDMT_NSGCFGIRPT_BMSK                                          0x2
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_BLSP1_VMIDMT_NSGCFGIRPT_SHFT                                          0x1
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_RPM_VMIDMT_NSGCFG_IRQ_BMSK                                            0x1
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_RPM_VMIDMT_NSGCFG_IRQ_SHFT                                            0x0

#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_ADDR                                                    (TCSR_TCSR_REGS_REG_BASE      + 0x000020d0)
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_PHYS                                                    (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x000020d0)
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_RMSK                                                          0x3f
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_IN          \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_ADDR, HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_RMSK)
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_ADDR, m)
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_OUT(v)      \
        out_dword(HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_ADDR,v)
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_ADDR,m,v,HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_IN)
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_QPIC_BAM_VMIDMT_NSGCFGIRPT_IRQ_ENABLE_BMSK                    0x20
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_QPIC_BAM_VMIDMT_NSGCFGIRPT_IRQ_ENABLE_SHFT                     0x5
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_IPA_VMIDMT_NSGCFGIRPT_ENABLE_BMSK                             0x10
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_IPA_VMIDMT_NSGCFGIRPT_ENABLE_SHFT                              0x4
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_DEHR_VMIDMT_NSGCFGIRPT_ENABLE_BMSK                             0x8
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_DEHR_VMIDMT_NSGCFGIRPT_ENABLE_SHFT                             0x3
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_CRYPTO0_VMIDMT_NSGCFGIRPT_IRQ_ENABLE_BMSK                      0x4
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_CRYPTO0_VMIDMT_NSGCFGIRPT_IRQ_ENABLE_SHFT                      0x2
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_BLSP1_VMIDMT_NSGCFGIRPT_ENABLE_BMSK                            0x2
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_BLSP1_VMIDMT_NSGCFGIRPT_ENABLE_SHFT                            0x1
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_RPM_VMIDMT_NSGCFG_IRQ_ENABLE_BMSK                              0x1
#define HWIO_TCSR_SS_VMIDMT_CFG_NON_SEC_INTR_ENABLE_RPM_VMIDMT_NSGCFG_IRQ_ENABLE_SHFT                              0x0

#define HWIO_TCSR_MUTEX_REG_RESET_ADDR                                                                      (TCSR_TCSR_REGS_REG_BASE      + 0x00002800)
#define HWIO_TCSR_MUTEX_REG_RESET_PHYS                                                                      (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00002800)
#define HWIO_TCSR_MUTEX_REG_RESET_RMSK                                                                             0x1
#define HWIO_TCSR_MUTEX_REG_RESET_IN          \
        in_dword_masked(HWIO_TCSR_MUTEX_REG_RESET_ADDR, HWIO_TCSR_MUTEX_REG_RESET_RMSK)
#define HWIO_TCSR_MUTEX_REG_RESET_INM(m)      \
        in_dword_masked(HWIO_TCSR_MUTEX_REG_RESET_ADDR, m)
#define HWIO_TCSR_MUTEX_REG_RESET_OUT(v)      \
        out_dword(HWIO_TCSR_MUTEX_REG_RESET_ADDR,v)
#define HWIO_TCSR_MUTEX_REG_RESET_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MUTEX_REG_RESET_ADDR,m,v,HWIO_TCSR_MUTEX_REG_RESET_IN)
#define HWIO_TCSR_MUTEX_REG_RESET_MUTEX_RESET_BMSK                                                                 0x1
#define HWIO_TCSR_MUTEX_REG_RESET_MUTEX_RESET_SHFT                                                                 0x0

#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ADDR                                                                    (TCSR_TCSR_REGS_REG_BASE      + 0x00002400)
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_PHYS                                                                    (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00002400)
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_RMSK                                                                    0x1f7ffa6f
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_IN          \
        in_dword_masked(HWIO_TCSR_SS_XPU2_SEC_INTR0_ADDR, HWIO_TCSR_SS_XPU2_SEC_INTR0_RMSK)
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_XPU2_SEC_INTR0_ADDR, m)
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_O_XPU2_SECURE_INTR_CE_BMSK                                              0x10000000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_O_XPU2_SECURE_INTR_CE_SHFT                                                    0x1c
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_O_XPU2_SECURE_INTR_IPA_BMSK                                              0x8000000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_O_XPU2_SECURE_INTR_IPA_SHFT                                                   0x1b
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_BLSP1_APU_SEC_ERROR_IRQ_BMSK                                             0x4000000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_BLSP1_APU_SEC_ERROR_IRQ_SHFT                                                  0x1a
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_SMMU_MPU_SECURE_INTR_BMSK                                                0x2000000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_SMMU_MPU_SECURE_INTR_SHFT                                                     0x19
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_O_XPU2_SECURE_INTR_QDSP_BMSK                                             0x1000000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_O_XPU2_SECURE_INTR_QDSP_SHFT                                                  0x18
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_O_XPU2_SECURE_INTR_MCDMA_BMSK                                             0x400000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_O_XPU2_SECURE_INTR_MCDMA_SHFT                                                 0x16
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_RPM_CFG_XPU2_SECURE_INTR_BMSK                                             0x200000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_RPM_CFG_XPU2_SECURE_INTR_SHFT                                                 0x15
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_PRNG_CFG_XPU2_SECURE_INTR_BMSK                                            0x100000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_PRNG_CFG_XPU2_SECURE_INTR_SHFT                                                0x14
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_PCNOC_CFG_XPU2_SECURE_INTR_BMSK                                            0x80000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_PCNOC_CFG_XPU2_SECURE_INTR_SHFT                                               0x13
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_SNOC_CFG_XPU2_SECURE_INTR_BMSK                                             0x40000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_SNOC_CFG_XPU2_SECURE_INTR_SHFT                                                0x12
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_OCIMEM_MPU_SECURE_INTR_BMSK                                                0x20000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_OCIMEM_MPU_SECURE_INTR_SHFT                                                   0x11
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_SEC_CTRL_XPU2_SEC_IRQ_BMSK                                                 0x10000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_SEC_CTRL_XPU2_SEC_IRQ_SHFT                                                    0x10
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_DEHR_XPU_SECURE_INTR_BMSK                                                   0x8000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_DEHR_XPU_SECURE_INTR_SHFT                                                      0xf
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_MPM_XPU2_SECURE_INTR_BMSK                                                   0x4000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_MPM_XPU2_SECURE_INTR_SHFT                                                      0xe
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_BOOT_ROM_SECURE_INTR_BMSK                                                   0x2000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_BOOT_ROM_SECURE_INTR_SHFT                                                      0xd
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_CRYPTO0_BAM_APU_SEC_ERROR_IRQ_BMSK                                          0x1000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_CRYPTO0_BAM_APU_SEC_ERROR_IRQ_SHFT                                             0xc
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_O_TCSR_REGS_SECURE_INTR_BMSK                                                 0x800
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_O_TCSR_REGS_SECURE_INTR_SHFT                                                   0xb
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_TLMM_XPU_SECURE_INTR_BMSK                                                    0x200
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_TLMM_XPU_SECURE_INTR_SHFT                                                      0x9
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_PMIC_ARB_MPU_SECURE_INTR_BMSK                                                 0x40
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_PMIC_ARB_MPU_SECURE_INTR_SHFT                                                  0x6
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_BIMC_CH0_XPU2_S_INTERRUPT_BMSK                                                0x20
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_BIMC_CH0_XPU2_S_INTERRUPT_SHFT                                                 0x5
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_GCC_XPU_SECURE_INTR_BMSK                                                       0x8
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_GCC_XPU_SECURE_INTR_SHFT                                                       0x3
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_QPIC_XPU2_SEC_ERROR_IRQ_BMSK                                                   0x4
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_QPIC_XPU2_SEC_ERROR_IRQ_SHFT                                                   0x2
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_RPM_APU_SEC_INTR_BMSK                                                          0x2
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_RPM_APU_SEC_INTR_SHFT                                                          0x1
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_IPA_BAM_APU_SEC_ERROR_IRQ_BMSK                                                 0x1
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_IPA_BAM_APU_SEC_ERROR_IRQ_SHFT                                                 0x0

#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_ADDR                                                             (TCSR_TCSR_REGS_REG_BASE      + 0x00002440)
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_PHYS                                                             (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00002440)
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_RMSK                                                             0x1f7ffa6f
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_IN          \
        in_dword_masked(HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_ADDR, HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_RMSK)
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_ADDR, m)
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_OUT(v)      \
        out_dword(HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_ADDR,v)
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_ADDR,m,v,HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_IN)
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_O_XPU2_SECURE_INTR_CE_ENABLE_BMSK                                0x10000000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_O_XPU2_SECURE_INTR_CE_ENABLE_SHFT                                      0x1c
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_O_XPU2_SECURE_INTR_IPA_ENABLE_BMSK                                0x8000000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_O_XPU2_SECURE_INTR_IPA_ENABLE_SHFT                                     0x1b
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_BLSP1_APU_SEC_ERROR_IRQ_ENABLE_BMSK                               0x4000000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_BLSP1_APU_SEC_ERROR_IRQ_ENABLE_SHFT                                    0x1a
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_SMMU_MPU_SECURE_INTR_ENABLE_BMSK                                  0x2000000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_SMMU_MPU_SECURE_INTR_ENABLE_SHFT                                       0x19
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_O_XPU2_SECURE_INTR_QDSP_ENABLE_BMSK                               0x1000000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_O_XPU2_SECURE_INTR_QDSP_ENABLE_SHFT                                    0x18
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_O_XPU2_SECURE_INTR_MCDMA_ENABLE_BMSK                               0x400000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_O_XPU2_SECURE_INTR_MCDMA_ENABLE_SHFT                                   0x16
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_RPM_CFG_XPU2_SECURE_INTR_ENABLE_BMSK                               0x200000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_RPM_CFG_XPU2_SECURE_INTR_ENABLE_SHFT                                   0x15
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_PRNG_CFG_XPU2_SECURE_INTR_ENABLE_BMSK                              0x100000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_PRNG_CFG_XPU2_SECURE_INTR_ENABLE_SHFT                                  0x14
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_PCNOC_CFG_XPU2_SECURE_INTR_ENABLE_BMSK                              0x80000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_PCNOC_CFG_XPU2_SECURE_INTR_ENABLE_SHFT                                 0x13
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_SNOC_CFG_XPU2_SECURE_INTR_ENABLE_BMSK                               0x40000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_SNOC_CFG_XPU2_SECURE_INTR_ENABLE_SHFT                                  0x12
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_OCIMEM_MPU_SECURE_INTR_ENABLE_BMSK                                  0x20000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_OCIMEM_MPU_SECURE_INTR_ENABLE_SHFT                                     0x11
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_SEC_CTRL_XPU2_SEC_IRQ_ENABLE_BMSK                                   0x10000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_SEC_CTRL_XPU2_SEC_IRQ_ENABLE_SHFT                                      0x10
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_DEHR_XPU_SECURE_INTR_ENABLE_BMSK                                     0x8000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_DEHR_XPU_SECURE_INTR_ENABLE_SHFT                                        0xf
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_MPM_XPU2_SECURE_INTR_ENABLE_BMSK                                     0x4000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_MPM_XPU2_SECURE_INTR_ENABLE_SHFT                                        0xe
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_BOOT_ROM_SECURE_INTR_ENABLE_BMSK                                     0x2000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_BOOT_ROM_SECURE_INTR_ENABLE_SHFT                                        0xd
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_CRYPTO0_BAM_APU_SEC_ERROR_IRQ_ENABLE_BMSK                            0x1000
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_CRYPTO0_BAM_APU_SEC_ERROR_IRQ_ENABLE_SHFT                               0xc
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_O_TCSR_REGS_SECURE_INTR_ENABLE_BMSK                                   0x800
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_O_TCSR_REGS_SECURE_INTR_ENABLE_SHFT                                     0xb
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_TLMM_XPU_SECURE_INTR_ENABLE_BMSK                                      0x200
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_TLMM_XPU_SECURE_INTR_ENABLE_SHFT                                        0x9
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_PMIC_ARB_MPU_SECURE_INTR_ENABLE_BMSK                                   0x40
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_PMIC_ARB_MPU_SECURE_INTR_ENABLE_SHFT                                    0x6
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_BIMC_CH0_XPU2_S_INTERRUPT_ENABLE_BMSK                                  0x20
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_BIMC_CH0_XPU2_S_INTERRUPT_ENABLE_SHFT                                   0x5
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_GCC_XPU_SECURE_INTR_ENABLE_BMSK                                         0x8
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_GCC_XPU_SECURE_INTR_ENABLE_SHFT                                         0x3
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_QPIC_XPU2_SEC_ERROR_IRQ_ENABLE_BMSK                                     0x4
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_QPIC_XPU2_SEC_ERROR_IRQ_ENABLE_SHFT                                     0x2
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_RPM_APU_SEC_INTR_ENABLE_BMSK                                            0x2
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_RPM_APU_SEC_INTR_ENABLE_SHFT                                            0x1
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_IPA_BAM_APU_SEC_ERROR_IRQ_ENABLE_BMSK                                   0x1
#define HWIO_TCSR_SS_XPU2_SEC_INTR0_ENABLE_IPA_BAM_APU_SEC_ERROR_IRQ_ENABLE_SHFT                                   0x0

#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ADDR                                                            (TCSR_TCSR_REGS_REG_BASE      + 0x00002410)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_PHYS                                                            (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00002410)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_RMSK                                                                  0x3f
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_IN          \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ADDR, HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_RMSK)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ADDR, m)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_QPIC_BAM_VMIDMT_GIRPT_IRQ_BMSK                                        0x20
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_QPIC_BAM_VMIDMT_GIRPT_IRQ_SHFT                                         0x5
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_IPA_VMIDMT_GIRPT_BMSK                                                 0x10
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_IPA_VMIDMT_GIRPT_SHFT                                                  0x4
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_DEHR_VMIDMT_GIRPT_BMSK                                                 0x8
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_DEHR_VMIDMT_GIRPT_SHFT                                                 0x3
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_CRYPTO0_VMIDMT_GIRPT_IRQ_BMSK                                          0x4
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_CRYPTO0_VMIDMT_GIRPT_IRQ_SHFT                                          0x2
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_BLSP1_VMIDMT_GIRPT_BMSK                                                0x2
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_BLSP1_VMIDMT_GIRPT_SHFT                                                0x1
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_RPM_VMIDMT_SECG_IRQ_BMSK                                               0x1
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_RPM_VMIDMT_SECG_IRQ_SHFT                                               0x0

#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_ADDR                                                     (TCSR_TCSR_REGS_REG_BASE      + 0x00002450)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_PHYS                                                     (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00002450)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_RMSK                                                           0x3f
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_IN          \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_ADDR, HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_RMSK)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_ADDR, m)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_OUT(v)      \
        out_dword(HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_ADDR,v)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_ADDR,m,v,HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_IN)
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_QPIC_BAM_VMIDMT_GIRPT_IRQ_ENABLE_BMSK                          0x20
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_QPIC_BAM_VMIDMT_GIRPT_IRQ_ENABLE_SHFT                           0x5
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_IPA_VMIDMT_GIRPT_ENABLE_BMSK                                   0x10
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_IPA_VMIDMT_GIRPT_ENABLE_SHFT                                    0x4
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_DEHR_VMIDMT_GIRPT_ENABLE_BMSK                                   0x8
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_DEHR_VMIDMT_GIRPT_ENABLE_SHFT                                   0x3
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_CRYPTO0_VMIDMT_GIRPT_IRQ_ENABLE_BMSK                            0x4
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_CRYPTO0_VMIDMT_GIRPT_IRQ_ENABLE_SHFT                            0x2
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_BLSP1_VMIDMT_GIRPT_ENABLE_BMSK                                  0x2
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_BLSP1_VMIDMT_GIRPT_ENABLE_SHFT                                  0x1
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_RPM_VMIDMT_SECG_IRQ_ENABLE_BMSK                                 0x1
#define HWIO_TCSR_SS_VMIDMT_CLIENT_SEC_INTR_ENABLE_RPM_VMIDMT_SECG_IRQ_ENABLE_SHFT                                 0x0

#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ADDR                                                               (TCSR_TCSR_REGS_REG_BASE      + 0x00002490)
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_PHYS                                                               (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00002490)
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_RMSK                                                                     0x3f
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_IN          \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ADDR, HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_RMSK)
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ADDR, m)
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_QPIC_BAM_VMIDMT_GCFGIRPT_IRQ_BMSK                                        0x20
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_QPIC_BAM_VMIDMT_GCFGIRPT_IRQ_SHFT                                         0x5
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_IPA_VMIDMT_GCFGIRPT_BMSK                                                 0x10
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_IPA_VMIDMT_GCFGIRPT_SHFT                                                  0x4
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_DEHR_VMIDMT_GCFGIRPT_BMSK                                                 0x8
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_DEHR_VMIDMT_GCFGIRPT_SHFT                                                 0x3
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_CRYPTO0_VMIDMT_GCFGIRPT_IRQ_BMSK                                          0x4
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_CRYPTO0_VMIDMT_GCFGIRPT_IRQ_SHFT                                          0x2
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_BLSP1_VMIDMT_GCFGIRPT_BMSK                                                0x2
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_BLSP1_VMIDMT_GCFGIRPT_SHFT                                                0x1
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_RPM_VMIDMT_SECGCFG_IRQ_BMSK                                               0x1
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_RPM_VMIDMT_SECGCFG_IRQ_SHFT                                               0x0

#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_ADDR                                                        (TCSR_TCSR_REGS_REG_BASE      + 0x000024d0)
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_PHYS                                                        (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x000024d0)
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_RMSK                                                              0x3f
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_IN          \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_ADDR, HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_RMSK)
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_ADDR, m)
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_OUT(v)      \
        out_dword(HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_ADDR,v)
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_ADDR,m,v,HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_IN)
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_QPIC_BAM_VMIDMT_GCFGIRPT_IRQ_ENABLE_BMSK                          0x20
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_QPIC_BAM_VMIDMT_GCFGIRPT_IRQ_ENABLE_SHFT                           0x5
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_IPA_VMIDMT_GCFGIRPT_ENABLE_BMSK                                   0x10
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_IPA_VMIDMT_GCFGIRPT_ENABLE_SHFT                                    0x4
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_DEHR_VMIDMT_GCFGIRPT_ENABLE_BMSK                                   0x8
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_DEHR_VMIDMT_GCFGIRPT_ENABLE_SHFT                                   0x3
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_CRYPTO0_VMIDMT_GCFGIRPT_IRQ_ENABLE_BMSK                            0x4
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_CRYPTO0_VMIDMT_GCFGIRPT_IRQ_ENABLE_SHFT                            0x2
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_BLSP1_VMIDMT_GCFGIRPT_ENABLE_BMSK                                  0x2
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_BLSP1_VMIDMT_GCFGIRPT_ENABLE_SHFT                                  0x1
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_RPM_VMIDMT_SECGCFG_IRQ_ENABLE_BMSK                                 0x1
#define HWIO_TCSR_SS_VMIDMT_CFG_SEC_INTR_ENABLE_RPM_VMIDMT_SECGCFG_IRQ_ENABLE_SHFT                                 0x0

#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ADDR                                                                    (TCSR_TCSR_REGS_REG_BASE      + 0x00003000)
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_PHYS                                                                    (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00003000)
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_RMSK                                                                    0x1f7ffa6f
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_IN          \
        in_dword_masked(HWIO_TCSR_SS_XPU2_MSA_INTR0_ADDR, HWIO_TCSR_SS_XPU2_MSA_INTR0_RMSK)
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_XPU2_MSA_INTR0_ADDR, m)
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_O_XPU2_MSA_INTR_CE_BMSK                                                 0x10000000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_O_XPU2_MSA_INTR_CE_SHFT                                                       0x1c
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_O_XPU2_MSA_INTR_IPA_BMSK                                                 0x8000000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_O_XPU2_MSA_INTR_IPA_SHFT                                                      0x1b
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_BLSP1_XPU2_MSA_INTR_BMSK                                                 0x4000000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_BLSP1_XPU2_MSA_INTR_SHFT                                                      0x1a
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_SMMU_MPU_MSA_INTR_BMSK                                                   0x2000000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_SMMU_MPU_MSA_INTR_SHFT                                                        0x19
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_O_XPU2_MSA_INTR_QDSP_BMSK                                                0x1000000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_O_XPU2_MSA_INTR_QDSP_SHFT                                                     0x18
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_O_XPU2_MSA_INTR_MCDMA_BMSK                                                0x400000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_O_XPU2_MSA_INTR_MCDMA_SHFT                                                    0x16
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_RPM_CFG_XPU2_MSA_INTR_BMSK                                                0x200000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_RPM_CFG_XPU2_MSA_INTR_SHFT                                                    0x15
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_PRNG_CFG_XPU2_MSA_INTR_BMSK                                               0x100000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_PRNG_CFG_XPU2_MSA_INTR_SHFT                                                   0x14
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_PCNOC_CFG_XPU2_MSA_INTR_BMSK                                               0x80000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_PCNOC_CFG_XPU2_MSA_INTR_SHFT                                                  0x13
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_SNOC_CFG_XPU2_MSA_INTR_BMSK                                                0x40000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_SNOC_CFG_XPU2_MSA_INTR_SHFT                                                   0x12
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_OCIMEM_MPU_MSA_INTR_BMSK                                                   0x20000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_OCIMEM_MPU_MSA_INTR_SHFT                                                      0x11
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_SEC_CTRL_XPU2_MSA_IRQ_BMSK                                                 0x10000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_SEC_CTRL_XPU2_MSA_IRQ_SHFT                                                    0x10
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_DEHR_XPU_MSA_INTR_BMSK                                                      0x8000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_DEHR_XPU_MSA_INTR_SHFT                                                         0xf
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_MPM_XPU2_MSA_INTR_BMSK                                                      0x4000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_MPM_XPU2_MSA_INTR_SHFT                                                         0xe
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_BOOT_ROM_MSA_INTR_BMSK                                                      0x2000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_BOOT_ROM_MSA_INTR_SHFT                                                         0xd
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_CRYPTO0_BAM_XPU2_MSA_INTR_BMSK                                              0x1000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_CRYPTO0_BAM_XPU2_MSA_INTR_SHFT                                                 0xc
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_O_TCSR_REGS_MSA_INTR_BMSK                                                    0x800
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_O_TCSR_REGS_MSA_INTR_SHFT                                                      0xb
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_TLMM_XPU_MSA_INTR_BMSK                                                       0x200
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_TLMM_XPU_MSA_INTR_SHFT                                                         0x9
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_PMIC_ARB_MPU_MSA_INTR_BMSK                                                    0x40
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_PMIC_ARB_MPU_MSA_INTR_SHFT                                                     0x6
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_BIMC_CH0_XPU2_MSA_INTERRUPT_BMSK                                              0x20
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_BIMC_CH0_XPU2_MSA_INTERRUPT_SHFT                                               0x5
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_GCC_XPU_MSA_INTR_BMSK                                                          0x8
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_GCC_XPU_MSA_INTR_SHFT                                                          0x3
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_QPIC_XPU2_MSA_IRQ_BMSK                                                         0x4
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_QPIC_XPU2_MSA_IRQ_SHFT                                                         0x2
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_RPM_APU_MSA_INTR_BMSK                                                          0x2
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_RPM_APU_MSA_INTR_SHFT                                                          0x1
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_IPA_BAM_XPU2_MSA_INTR_BMSK                                                     0x1
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_IPA_BAM_XPU2_MSA_INTR_SHFT                                                     0x0

#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_ADDR                                                             (TCSR_TCSR_REGS_REG_BASE      + 0x00003010)
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_PHYS                                                             (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00003010)
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_RMSK                                                             0x1f7ffa6f
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_IN          \
        in_dword_masked(HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_ADDR, HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_RMSK)
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_INM(m)      \
        in_dword_masked(HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_ADDR, m)
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_OUT(v)      \
        out_dword(HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_ADDR,v)
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_ADDR,m,v,HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_IN)
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_O_XPU2_MSA_INTR_CE_ENABLE_BMSK                                   0x10000000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_O_XPU2_MSA_INTR_CE_ENABLE_SHFT                                         0x1c
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_O_XPU2_MSA_INTR_IPA_ENABLE_BMSK                                   0x8000000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_O_XPU2_MSA_INTR_IPA_ENABLE_SHFT                                        0x1b
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_BLSP1_XPU2_MSA_INTR_ENABLE_BMSK                                   0x4000000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_BLSP1_XPU2_MSA_INTR_ENABLE_SHFT                                        0x1a
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_SMMU_MPU_MSA_INTR_ENABLE_BMSK                                     0x2000000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_SMMU_MPU_MSA_INTR_ENABLE_SHFT                                          0x19
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_O_XPU2_MSA_INTR_QDSP_ENABLE_BMSK                                  0x1000000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_O_XPU2_MSA_INTR_QDSP_ENABLE_SHFT                                       0x18
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_O_XPU2_MSA_INTR_MCDMA_ENABLE_BMSK                                  0x400000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_O_XPU2_MSA_INTR_MCDMA_ENABLE_SHFT                                      0x16
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_RPM_CFG_XPU2_MSA_INTR_ENABLE_BMSK                                  0x200000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_RPM_CFG_XPU2_MSA_INTR_ENABLE_SHFT                                      0x15
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_PRNG_CFG_XPU2_MSA_INTR_ENABLE_BMSK                                 0x100000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_PRNG_CFG_XPU2_MSA_INTR_ENABLE_SHFT                                     0x14
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_PCNOC_CFG_XPU2_MSA_INTR_ENABLE_BMSK                                 0x80000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_PCNOC_CFG_XPU2_MSA_INTR_ENABLE_SHFT                                    0x13
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_SNOC_CFG_XPU2_MSA_INTR_ENABLE_BMSK                                  0x40000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_SNOC_CFG_XPU2_MSA_INTR_ENABLE_SHFT                                     0x12
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_OCIMEM_MPU_MSA_INTR_ENABLE_BMSK                                     0x20000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_OCIMEM_MPU_MSA_INTR_ENABLE_SHFT                                        0x11
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_SEC_CTRL_XPU2_MSA_IRQ_ENABLE_BMSK                                   0x10000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_SEC_CTRL_XPU2_MSA_IRQ_ENABLE_SHFT                                      0x10
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_DEHR_XPU_MSA_INTR_ENABLE_BMSK                                        0x8000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_DEHR_XPU_MSA_INTR_ENABLE_SHFT                                           0xf
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_MPM_XPU2_MSA_INTR_ENABLE_BMSK                                        0x4000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_MPM_XPU2_MSA_INTR_ENABLE_SHFT                                           0xe
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_BOOT_ROM_MSA_INTR_ENABLE_BMSK                                        0x2000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_BOOT_ROM_MSA_INTR_ENABLE_SHFT                                           0xd
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_CRYPTO0_BAM_XPU2_MSA_INTR_ENABLE_BMSK                                0x1000
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_CRYPTO0_BAM_XPU2_MSA_INTR_ENABLE_SHFT                                   0xc
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_O_TCSR_REGS_MSA_INTR_ENABLE_BMSK                                      0x800
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_O_TCSR_REGS_MSA_INTR_ENABLE_SHFT                                        0xb
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_TLMM_XPU_MSA_INTR_ENABLE_BMSK                                         0x200
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_TLMM_XPU_MSA_INTR_ENABLE_SHFT                                           0x9
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_PMIC_ARB_MPU_MSA_INTR_ENABLE_BMSK                                      0x40
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_PMIC_ARB_MPU_MSA_INTR_ENABLE_SHFT                                       0x6
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_BIMC_CH0_XPU2_MSA_INTERRUPT_ENABLE_BMSK                                0x20
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_BIMC_CH0_XPU2_MSA_INTERRUPT_ENABLE_SHFT                                 0x5
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_GCC_XPU_MSA_INTR_ENABLE_BMSK                                            0x8
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_GCC_XPU_MSA_INTR_ENABLE_SHFT                                            0x3
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_QPIC_XPU2_MSA_IRQ_ENABLE_BMSK                                           0x4
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_QPIC_XPU2_MSA_IRQ_ENABLE_SHFT                                           0x2
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_RPM_APU_MSA_INTR_ENABLE_BMSK                                            0x2
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_RPM_APU_MSA_INTR_ENABLE_SHFT                                            0x1
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_IPA_BAM_XPU2_MSA_INTR_BMSK                                              0x1
#define HWIO_TCSR_SS_XPU2_MSA_INTR0_ENABLE_IPA_BAM_XPU2_MSA_INTR_SHFT                                              0x0

#define HWIO_TCSR_TCSR_CLK_EN_ADDR                                                                          (TCSR_TCSR_REGS_REG_BASE      + 0x0000407c)
#define HWIO_TCSR_TCSR_CLK_EN_PHYS                                                                          (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x0000407c)
#define HWIO_TCSR_TCSR_CLK_EN_RMSK                                                                                 0x1
#define HWIO_TCSR_TCSR_CLK_EN_IN          \
        in_dword_masked(HWIO_TCSR_TCSR_CLK_EN_ADDR, HWIO_TCSR_TCSR_CLK_EN_RMSK)
#define HWIO_TCSR_TCSR_CLK_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_TCSR_CLK_EN_ADDR, m)
#define HWIO_TCSR_TCSR_CLK_EN_OUT(v)      \
        out_dword(HWIO_TCSR_TCSR_CLK_EN_ADDR,v)
#define HWIO_TCSR_TCSR_CLK_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_TCSR_CLK_EN_ADDR,m,v,HWIO_TCSR_TCSR_CLK_EN_IN)
#define HWIO_TCSR_TCSR_CLK_EN_TCSR_CLK_EN_BMSK                                                                     0x1
#define HWIO_TCSR_TCSR_CLK_EN_TCSR_CLK_EN_SHFT                                                                     0x0

#define HWIO_TCSR_TIMEOUT_SLAVE_GLB_EN_ADDR                                                                 (TCSR_TCSR_REGS_REG_BASE      + 0x00005000)
#define HWIO_TCSR_TIMEOUT_SLAVE_GLB_EN_PHYS                                                                 (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00005000)
#define HWIO_TCSR_TIMEOUT_SLAVE_GLB_EN_RMSK                                                                        0x1
#define HWIO_TCSR_TIMEOUT_SLAVE_GLB_EN_IN          \
        in_dword_masked(HWIO_TCSR_TIMEOUT_SLAVE_GLB_EN_ADDR, HWIO_TCSR_TIMEOUT_SLAVE_GLB_EN_RMSK)
#define HWIO_TCSR_TIMEOUT_SLAVE_GLB_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_TIMEOUT_SLAVE_GLB_EN_ADDR, m)
#define HWIO_TCSR_TIMEOUT_SLAVE_GLB_EN_OUT(v)      \
        out_dword(HWIO_TCSR_TIMEOUT_SLAVE_GLB_EN_ADDR,v)
#define HWIO_TCSR_TIMEOUT_SLAVE_GLB_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_TIMEOUT_SLAVE_GLB_EN_ADDR,m,v,HWIO_TCSR_TIMEOUT_SLAVE_GLB_EN_IN)
#define HWIO_TCSR_TIMEOUT_SLAVE_GLB_EN_TIMEOUT_SLAVE_GLB_EN_BMSK                                                   0x1
#define HWIO_TCSR_TIMEOUT_SLAVE_GLB_EN_TIMEOUT_SLAVE_GLB_EN_SHFT                                                   0x0

#define HWIO_TCSR_XPU_NSEN_STATUS_ADDR                                                                      (TCSR_TCSR_REGS_REG_BASE      + 0x00005004)
#define HWIO_TCSR_XPU_NSEN_STATUS_PHYS                                                                      (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00005004)
#define HWIO_TCSR_XPU_NSEN_STATUS_RMSK                                                                             0x2
#define HWIO_TCSR_XPU_NSEN_STATUS_IN          \
        in_dword_masked(HWIO_TCSR_XPU_NSEN_STATUS_ADDR, HWIO_TCSR_XPU_NSEN_STATUS_RMSK)
#define HWIO_TCSR_XPU_NSEN_STATUS_INM(m)      \
        in_dword_masked(HWIO_TCSR_XPU_NSEN_STATUS_ADDR, m)
#define HWIO_TCSR_XPU_NSEN_STATUS_REGS_XPU2_NSEN_STATUS_BMSK                                                       0x2
#define HWIO_TCSR_XPU_NSEN_STATUS_REGS_XPU2_NSEN_STATUS_SHFT                                                       0x1

#define HWIO_TCSR_XPU_VMIDEN_STATUS_ADDR                                                                    (TCSR_TCSR_REGS_REG_BASE      + 0x00005008)
#define HWIO_TCSR_XPU_VMIDEN_STATUS_PHYS                                                                    (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00005008)
#define HWIO_TCSR_XPU_VMIDEN_STATUS_RMSK                                                                           0x2
#define HWIO_TCSR_XPU_VMIDEN_STATUS_IN          \
        in_dword_masked(HWIO_TCSR_XPU_VMIDEN_STATUS_ADDR, HWIO_TCSR_XPU_VMIDEN_STATUS_RMSK)
#define HWIO_TCSR_XPU_VMIDEN_STATUS_INM(m)      \
        in_dword_masked(HWIO_TCSR_XPU_VMIDEN_STATUS_ADDR, m)
#define HWIO_TCSR_XPU_VMIDEN_STATUS_REGS_XPU2_VMIDEN_STATUS_BMSK                                                   0x2
#define HWIO_TCSR_XPU_VMIDEN_STATUS_REGS_XPU2_VMIDEN_STATUS_SHFT                                                   0x1

#define HWIO_TCSR_XPU_MSAEN_STATUS_ADDR                                                                     (TCSR_TCSR_REGS_REG_BASE      + 0x0000500c)
#define HWIO_TCSR_XPU_MSAEN_STATUS_PHYS                                                                     (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x0000500c)
#define HWIO_TCSR_XPU_MSAEN_STATUS_RMSK                                                                            0x2
#define HWIO_TCSR_XPU_MSAEN_STATUS_IN          \
        in_dword_masked(HWIO_TCSR_XPU_MSAEN_STATUS_ADDR, HWIO_TCSR_XPU_MSAEN_STATUS_RMSK)
#define HWIO_TCSR_XPU_MSAEN_STATUS_INM(m)      \
        in_dword_masked(HWIO_TCSR_XPU_MSAEN_STATUS_ADDR, m)
#define HWIO_TCSR_XPU_MSAEN_STATUS_REGS_XPU2_MSAEN_STATUS_BMSK                                                     0x2
#define HWIO_TCSR_XPU_MSAEN_STATUS_REGS_XPU2_MSAEN_STATUS_SHFT                                                     0x1

#define HWIO_TCSR_TZ_WONCE_n_ADDR(n)                                                                        (TCSR_TCSR_REGS_REG_BASE      + 0x00006000 + 0x4 * (n))
#define HWIO_TCSR_TZ_WONCE_n_PHYS(n)                                                                        (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00006000 + 0x4 * (n))
#define HWIO_TCSR_TZ_WONCE_n_RMSK                                                                           0xffffffff
#define HWIO_TCSR_TZ_WONCE_n_MAXn                                                                                   15
#define HWIO_TCSR_TZ_WONCE_n_INI(n)        \
        in_dword_masked(HWIO_TCSR_TZ_WONCE_n_ADDR(n), HWIO_TCSR_TZ_WONCE_n_RMSK)
#define HWIO_TCSR_TZ_WONCE_n_INMI(n,mask)    \
        in_dword_masked(HWIO_TCSR_TZ_WONCE_n_ADDR(n), mask)
#define HWIO_TCSR_TZ_WONCE_n_OUTI(n,val)    \
        out_dword(HWIO_TCSR_TZ_WONCE_n_ADDR(n),val)
#define HWIO_TCSR_TZ_WONCE_n_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_TCSR_TZ_WONCE_n_ADDR(n),mask,val,HWIO_TCSR_TZ_WONCE_n_INI(n))
#define HWIO_TCSR_TZ_WONCE_n_TZ_WONCE_ADDRESS_BMSK                                                          0xffffffff
#define HWIO_TCSR_TZ_WONCE_n_TZ_WONCE_ADDRESS_SHFT                                                                 0x0

#define HWIO_TCSR_GCC_CLK_MUX_SEL_ADDR                                                                      (TCSR_TCSR_REGS_REG_BASE      + 0x000060f0)
#define HWIO_TCSR_GCC_CLK_MUX_SEL_PHYS                                                                      (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x000060f0)
#define HWIO_TCSR_GCC_CLK_MUX_SEL_RMSK                                                                           0x8f1
#define HWIO_TCSR_GCC_CLK_MUX_SEL_IN          \
        in_dword_masked(HWIO_TCSR_GCC_CLK_MUX_SEL_ADDR, HWIO_TCSR_GCC_CLK_MUX_SEL_RMSK)
#define HWIO_TCSR_GCC_CLK_MUX_SEL_INM(m)      \
        in_dword_masked(HWIO_TCSR_GCC_CLK_MUX_SEL_ADDR, m)
#define HWIO_TCSR_GCC_CLK_MUX_SEL_OUT(v)      \
        out_dword(HWIO_TCSR_GCC_CLK_MUX_SEL_ADDR,v)
#define HWIO_TCSR_GCC_CLK_MUX_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_GCC_CLK_MUX_SEL_ADDR,m,v,HWIO_TCSR_GCC_CLK_MUX_SEL_IN)
#define HWIO_TCSR_GCC_CLK_MUX_SEL_TPRONTS_SEL_BMSK                                                               0x800
#define HWIO_TCSR_GCC_CLK_MUX_SEL_TPRONTS_SEL_SHFT                                                                 0xb
#define HWIO_TCSR_GCC_CLK_MUX_SEL_VMID_REG_BMSK                                                                   0xf0
#define HWIO_TCSR_GCC_CLK_MUX_SEL_VMID_REG_SHFT                                                                    0x4
#define HWIO_TCSR_GCC_CLK_MUX_SEL_CLK_MUX_SEL_BMSK                                                                 0x1
#define HWIO_TCSR_GCC_CLK_MUX_SEL_CLK_MUX_SEL_SHFT                                                                 0x0

#define HWIO_TCSR_QPDI_DISABLE_CFG_ADDR                                                                     (TCSR_TCSR_REGS_REG_BASE      + 0x0000a000)
#define HWIO_TCSR_QPDI_DISABLE_CFG_PHYS                                                                     (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x0000a000)
#define HWIO_TCSR_QPDI_DISABLE_CFG_RMSK                                                                          0x303
#define HWIO_TCSR_QPDI_DISABLE_CFG_IN          \
        in_dword_masked(HWIO_TCSR_QPDI_DISABLE_CFG_ADDR, HWIO_TCSR_QPDI_DISABLE_CFG_RMSK)
#define HWIO_TCSR_QPDI_DISABLE_CFG_INM(m)      \
        in_dword_masked(HWIO_TCSR_QPDI_DISABLE_CFG_ADDR, m)
#define HWIO_TCSR_QPDI_DISABLE_CFG_OUT(v)      \
        out_dword(HWIO_TCSR_QPDI_DISABLE_CFG_ADDR,v)
#define HWIO_TCSR_QPDI_DISABLE_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_QPDI_DISABLE_CFG_ADDR,m,v,HWIO_TCSR_QPDI_DISABLE_CFG_IN)
#define HWIO_TCSR_QPDI_DISABLE_CFG_QPDI_SPMI_DBG_ACK_BMSK                                                        0x200
#define HWIO_TCSR_QPDI_DISABLE_CFG_QPDI_SPMI_DBG_ACK_SHFT                                                          0x9
#define HWIO_TCSR_QPDI_DISABLE_CFG_QPDI_SPMI_DBG_REQ_BMSK                                                        0x100
#define HWIO_TCSR_QPDI_DISABLE_CFG_QPDI_SPMI_DBG_REQ_SHFT                                                          0x8
#define HWIO_TCSR_QPDI_DISABLE_CFG_SPMI_HANDSHAKE_DISABLE_BMSK                                                     0x2
#define HWIO_TCSR_QPDI_DISABLE_CFG_SPMI_HANDSHAKE_DISABLE_SHFT                                                     0x1
#define HWIO_TCSR_QPDI_DISABLE_CFG_QPDI_DISABLE_CFG_BMSK                                                           0x1
#define HWIO_TCSR_QPDI_DISABLE_CFG_QPDI_DISABLE_CFG_SHFT                                                           0x0

#define HWIO_TCSR_MSA_BIT_REG_ADDR                                                                          (TCSR_TCSR_REGS_REG_BASE      + 0x00003100)
#define HWIO_TCSR_MSA_BIT_REG_PHYS                                                                          (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00003100)
#define HWIO_TCSR_MSA_BIT_REG_RMSK                                                                                 0x1
#define HWIO_TCSR_MSA_BIT_REG_IN          \
        in_dword_masked(HWIO_TCSR_MSA_BIT_REG_ADDR, HWIO_TCSR_MSA_BIT_REG_RMSK)
#define HWIO_TCSR_MSA_BIT_REG_INM(m)      \
        in_dword_masked(HWIO_TCSR_MSA_BIT_REG_ADDR, m)
#define HWIO_TCSR_MSA_BIT_REG_OUT(v)      \
        out_dword(HWIO_TCSR_MSA_BIT_REG_ADDR,v)
#define HWIO_TCSR_MSA_BIT_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MSA_BIT_REG_ADDR,m,v,HWIO_TCSR_MSA_BIT_REG_IN)
#define HWIO_TCSR_MSA_BIT_REG_MSA_BIT_REG_BMSK                                                                     0x1
#define HWIO_TCSR_MSA_BIT_REG_MSA_BIT_REG_SHFT                                                                     0x0

#define HWIO_TCSR_BOOT_MISC_DETECT_ADDR                                                                     (TCSR_TCSR_REGS_REG_BASE      + 0x00006100)
#define HWIO_TCSR_BOOT_MISC_DETECT_PHYS                                                                     (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00006100)
#define HWIO_TCSR_BOOT_MISC_DETECT_RMSK                                                                     0xffffffff
#define HWIO_TCSR_BOOT_MISC_DETECT_IN          \
        in_dword_masked(HWIO_TCSR_BOOT_MISC_DETECT_ADDR, HWIO_TCSR_BOOT_MISC_DETECT_RMSK)
#define HWIO_TCSR_BOOT_MISC_DETECT_INM(m)      \
        in_dword_masked(HWIO_TCSR_BOOT_MISC_DETECT_ADDR, m)
#define HWIO_TCSR_BOOT_MISC_DETECT_OUT(v)      \
        out_dword(HWIO_TCSR_BOOT_MISC_DETECT_ADDR,v)
#define HWIO_TCSR_BOOT_MISC_DETECT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_BOOT_MISC_DETECT_ADDR,m,v,HWIO_TCSR_BOOT_MISC_DETECT_IN)
#define HWIO_TCSR_BOOT_MISC_DETECT_BOOT_MISC_DETECT_BMSK                                                    0xffffffff
#define HWIO_TCSR_BOOT_MISC_DETECT_BOOT_MISC_DETECT_SHFT                                                           0x0

#define HWIO_TCSR_APSS_VMID_ADDR                                                                            (TCSR_TCSR_REGS_REG_BASE      + 0x00006110)
#define HWIO_TCSR_APSS_VMID_PHYS                                                                            (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00006110)
#define HWIO_TCSR_APSS_VMID_RMSK                                                                                  0x1f
#define HWIO_TCSR_APSS_VMID_IN          \
        in_dword_masked(HWIO_TCSR_APSS_VMID_ADDR, HWIO_TCSR_APSS_VMID_RMSK)
#define HWIO_TCSR_APSS_VMID_INM(m)      \
        in_dword_masked(HWIO_TCSR_APSS_VMID_ADDR, m)
#define HWIO_TCSR_APSS_VMID_OUT(v)      \
        out_dword(HWIO_TCSR_APSS_VMID_ADDR,v)
#define HWIO_TCSR_APSS_VMID_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_APSS_VMID_ADDR,m,v,HWIO_TCSR_APSS_VMID_IN)
#define HWIO_TCSR_APSS_VMID_APSS_VMID_BMSK                                                                        0x1f
#define HWIO_TCSR_APSS_VMID_APSS_VMID_SHFT                                                                         0x0

#define HWIO_TCSR_MMSS_RPM_IRQ_EN_ADDR                                                                      (TCSR_TCSR_REGS_REG_BASE      + 0x00007000)
#define HWIO_TCSR_MMSS_RPM_IRQ_EN_PHYS                                                                      (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00007000)
#define HWIO_TCSR_MMSS_RPM_IRQ_EN_RMSK                                                                       0x1f1fffe
#define HWIO_TCSR_MMSS_RPM_IRQ_EN_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_RPM_IRQ_EN_ADDR, HWIO_TCSR_MMSS_RPM_IRQ_EN_RMSK)
#define HWIO_TCSR_MMSS_RPM_IRQ_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_RPM_IRQ_EN_ADDR, m)
#define HWIO_TCSR_MMSS_RPM_IRQ_EN_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_RPM_IRQ_EN_ADDR,v)
#define HWIO_TCSR_MMSS_RPM_IRQ_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_RPM_IRQ_EN_ADDR,m,v,HWIO_TCSR_MMSS_RPM_IRQ_EN_IN)
#define HWIO_TCSR_MMSS_RPM_IRQ_EN_RPM_IRQ_EN_VENUS0_IRQ_BMSK                                                 0x1000000
#define HWIO_TCSR_MMSS_RPM_IRQ_EN_RPM_IRQ_EN_VENUS0_IRQ_SHFT                                                      0x18
#define HWIO_TCSR_MMSS_RPM_IRQ_EN_RPM_IRQ_EN_CAMSS_IRQ10_BMSK                                                 0x800000
#define HWIO_TCSR_MMSS_RPM_IRQ_EN_RPM_IRQ_EN_CAMSS_IRQ10_SHFT                                                     0x17
#define HWIO_TCSR_MMSS_RPM_IRQ_EN_RPM_IRQ_EN_CAMSS_IRQ11_BMSK                                                 0x400000
#define HWIO_TCSR_MMSS_RPM_IRQ_EN_RPM_IRQ_EN_CAMSS_IRQ11_SHFT                                                     0x16
#define HWIO_TCSR_MMSS_RPM_IRQ_EN_RPM_IRQ_EN_CAMSS_IRQ12_BMSK                                                 0x200000
#define HWIO_TCSR_MMSS_RPM_IRQ_EN_RPM_IRQ_EN_CAMSS_IRQ12_SHFT                                                     0x15
#define HWIO_TCSR_MMSS_RPM_IRQ_EN_RPM_IRQ_EN_MDSS_IRQ_BMSK                                                    0x100000
#define HWIO_TCSR_MMSS_RPM_IRQ_EN_RPM_IRQ_EN_MDSS_IRQ_SHFT                                                        0x14
#define HWIO_TCSR_MMSS_RPM_IRQ_EN_RPM_IRQ_EN_GC_SYS_IRQ3_BMSK                                                  0x10000
#define HWIO_TCSR_MMSS_RPM_IRQ_EN_RPM_IRQ_EN_GC_SYS_IRQ3_SHFT                                                     0x10
#define HWIO_TCSR_MMSS_RPM_IRQ_EN_RPM_IRQ_EN_GC_SYS_IRQ2_BMSK                                                   0x8000
#define HWIO_TCSR_MMSS_RPM_IRQ_EN_RPM_IRQ_EN_GC_SYS_IRQ2_SHFT                                                      0xf
#define HWIO_TCSR_MMSS_RPM_IRQ_EN_RPM_IRQ_EN_GC_SYS_IRQ1_BMSK                                                   0x4000
#define HWIO_TCSR_MMSS_RPM_IRQ_EN_RPM_IRQ_EN_GC_SYS_IRQ1_SHFT                                                      0xe
#define HWIO_TCSR_MMSS_RPM_IRQ_EN_RPM_IRQ_EN_GC_SYS_IRQ0_BMSK                                                   0x2000
#define HWIO_TCSR_MMSS_RPM_IRQ_EN_RPM_IRQ_EN_GC_SYS_IRQ0_SHFT                                                      0xd
#define HWIO_TCSR_MMSS_RPM_IRQ_EN_RPM_IRQ_EN_CAMSS_IRQ0_BMSK                                                    0x1000
#define HWIO_TCSR_MMSS_RPM_IRQ_EN_RPM_IRQ_EN_CAMSS_IRQ0_SHFT                                                       0xc
#define HWIO_TCSR_MMSS_RPM_IRQ_EN_RPM_IRQ_EN_CAMSS_IRQ1_BMSK                                                     0x800
#define HWIO_TCSR_MMSS_RPM_IRQ_EN_RPM_IRQ_EN_CAMSS_IRQ1_SHFT                                                       0xb
#define HWIO_TCSR_MMSS_RPM_IRQ_EN_RPM_IRQ_EN_CAMSS_IRQ2_BMSK                                                     0x400
#define HWIO_TCSR_MMSS_RPM_IRQ_EN_RPM_IRQ_EN_CAMSS_IRQ2_SHFT                                                       0xa
#define HWIO_TCSR_MMSS_RPM_IRQ_EN_RPM_IRQ_EN_CAMSS_IRQ3_BMSK                                                     0x200
#define HWIO_TCSR_MMSS_RPM_IRQ_EN_RPM_IRQ_EN_CAMSS_IRQ3_SHFT                                                       0x9
#define HWIO_TCSR_MMSS_RPM_IRQ_EN_RPM_IRQ_EN_CAMSS_IRQ4_BMSK                                                     0x100
#define HWIO_TCSR_MMSS_RPM_IRQ_EN_RPM_IRQ_EN_CAMSS_IRQ4_SHFT                                                       0x8
#define HWIO_TCSR_MMSS_RPM_IRQ_EN_RPM_IRQ_EN_CAMSS_IRQ5_BMSK                                                      0x80
#define HWIO_TCSR_MMSS_RPM_IRQ_EN_RPM_IRQ_EN_CAMSS_IRQ5_SHFT                                                       0x7
#define HWIO_TCSR_MMSS_RPM_IRQ_EN_RPM_IRQ_EN_CAMSS_IRQ6_BMSK                                                      0x40
#define HWIO_TCSR_MMSS_RPM_IRQ_EN_RPM_IRQ_EN_CAMSS_IRQ6_SHFT                                                       0x6
#define HWIO_TCSR_MMSS_RPM_IRQ_EN_RPM_IRQ_EN_CAMSS_IRQ7_BMSK                                                      0x20
#define HWIO_TCSR_MMSS_RPM_IRQ_EN_RPM_IRQ_EN_CAMSS_IRQ7_SHFT                                                       0x5
#define HWIO_TCSR_MMSS_RPM_IRQ_EN_RPM_IRQ_EN_CAMSS_IRQ8_BMSK                                                      0x10
#define HWIO_TCSR_MMSS_RPM_IRQ_EN_RPM_IRQ_EN_CAMSS_IRQ8_SHFT                                                       0x4
#define HWIO_TCSR_MMSS_RPM_IRQ_EN_RPM_IRQ_EN_CAMSS_IRQ9_BMSK                                                       0x8
#define HWIO_TCSR_MMSS_RPM_IRQ_EN_RPM_IRQ_EN_CAMSS_IRQ9_SHFT                                                       0x3
#define HWIO_TCSR_MMSS_RPM_IRQ_EN_RPM_IRQ_EN_CSIPHY_0_IRQ_BMSK                                                     0x4
#define HWIO_TCSR_MMSS_RPM_IRQ_EN_RPM_IRQ_EN_CSIPHY_0_IRQ_SHFT                                                     0x2
#define HWIO_TCSR_MMSS_RPM_IRQ_EN_RPM_IRQ_EN_CSIPHY_1_IRQ_BMSK                                                     0x2
#define HWIO_TCSR_MMSS_RPM_IRQ_EN_RPM_IRQ_EN_CSIPHY_1_IRQ_SHFT                                                     0x1

#define HWIO_TCSR_MMSS_RPM_IRQ_CLEAR_ADDR                                                                   (TCSR_TCSR_REGS_REG_BASE      + 0x00007004)
#define HWIO_TCSR_MMSS_RPM_IRQ_CLEAR_PHYS                                                                   (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00007004)
#define HWIO_TCSR_MMSS_RPM_IRQ_CLEAR_RMSK                                                                    0x1f1fffe
#define HWIO_TCSR_MMSS_RPM_IRQ_CLEAR_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_RPM_IRQ_CLEAR_ADDR, HWIO_TCSR_MMSS_RPM_IRQ_CLEAR_RMSK)
#define HWIO_TCSR_MMSS_RPM_IRQ_CLEAR_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_RPM_IRQ_CLEAR_ADDR, m)
#define HWIO_TCSR_MMSS_RPM_IRQ_CLEAR_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_RPM_IRQ_CLEAR_ADDR,v)
#define HWIO_TCSR_MMSS_RPM_IRQ_CLEAR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_RPM_IRQ_CLEAR_ADDR,m,v,HWIO_TCSR_MMSS_RPM_IRQ_CLEAR_IN)
#define HWIO_TCSR_MMSS_RPM_IRQ_CLEAR_RPM_IRQ_CLEAR_VENUS0_IRQ_BMSK                                           0x1000000
#define HWIO_TCSR_MMSS_RPM_IRQ_CLEAR_RPM_IRQ_CLEAR_VENUS0_IRQ_SHFT                                                0x18
#define HWIO_TCSR_MMSS_RPM_IRQ_CLEAR_RPM_IRQ_CLEAR_CAMSS_IRQ10_BMSK                                           0x800000
#define HWIO_TCSR_MMSS_RPM_IRQ_CLEAR_RPM_IRQ_CLEAR_CAMSS_IRQ10_SHFT                                               0x17
#define HWIO_TCSR_MMSS_RPM_IRQ_CLEAR_RPM_IRQ_CLEAR_CAMSS_IRQ11_BMSK                                           0x400000
#define HWIO_TCSR_MMSS_RPM_IRQ_CLEAR_RPM_IRQ_CLEAR_CAMSS_IRQ11_SHFT                                               0x16
#define HWIO_TCSR_MMSS_RPM_IRQ_CLEAR_RPM_IRQ_CLEAR_CAMSS_IRQ12_BMSK                                           0x200000
#define HWIO_TCSR_MMSS_RPM_IRQ_CLEAR_RPM_IRQ_CLEAR_CAMSS_IRQ12_SHFT                                               0x15
#define HWIO_TCSR_MMSS_RPM_IRQ_CLEAR_RPM_IRQ_CLEAR_MDSS_IRQ_BMSK                                              0x100000
#define HWIO_TCSR_MMSS_RPM_IRQ_CLEAR_RPM_IRQ_CLEAR_MDSS_IRQ_SHFT                                                  0x14
#define HWIO_TCSR_MMSS_RPM_IRQ_CLEAR_RPM_IRQ_CLEAR_GC_SYS_IRQ3_BMSK                                            0x10000
#define HWIO_TCSR_MMSS_RPM_IRQ_CLEAR_RPM_IRQ_CLEAR_GC_SYS_IRQ3_SHFT                                               0x10
#define HWIO_TCSR_MMSS_RPM_IRQ_CLEAR_RPM_IRQ_CLEAR_GC_SYS_IRQ2_BMSK                                             0x8000
#define HWIO_TCSR_MMSS_RPM_IRQ_CLEAR_RPM_IRQ_CLEAR_GC_SYS_IRQ2_SHFT                                                0xf
#define HWIO_TCSR_MMSS_RPM_IRQ_CLEAR_RPM_IRQ_CLEAR_GC_SYS_IRQ1_BMSK                                             0x4000
#define HWIO_TCSR_MMSS_RPM_IRQ_CLEAR_RPM_IRQ_CLEAR_GC_SYS_IRQ1_SHFT                                                0xe
#define HWIO_TCSR_MMSS_RPM_IRQ_CLEAR_RPM_IRQ_CLEAR_GC_SYS_IRQ0_BMSK                                             0x2000
#define HWIO_TCSR_MMSS_RPM_IRQ_CLEAR_RPM_IRQ_CLEAR_GC_SYS_IRQ0_SHFT                                                0xd
#define HWIO_TCSR_MMSS_RPM_IRQ_CLEAR_RPM_IRQ_CLEAR_CAMSS_IRQ0_BMSK                                              0x1000
#define HWIO_TCSR_MMSS_RPM_IRQ_CLEAR_RPM_IRQ_CLEAR_CAMSS_IRQ0_SHFT                                                 0xc
#define HWIO_TCSR_MMSS_RPM_IRQ_CLEAR_RPM_IRQ_CLEAR_CAMSS_IRQ1_BMSK                                               0x800
#define HWIO_TCSR_MMSS_RPM_IRQ_CLEAR_RPM_IRQ_CLEAR_CAMSS_IRQ1_SHFT                                                 0xb
#define HWIO_TCSR_MMSS_RPM_IRQ_CLEAR_RPM_IRQ_CLEAR_CAMSS_IRQ2_BMSK                                               0x400
#define HWIO_TCSR_MMSS_RPM_IRQ_CLEAR_RPM_IRQ_CLEAR_CAMSS_IRQ2_SHFT                                                 0xa
#define HWIO_TCSR_MMSS_RPM_IRQ_CLEAR_RPM_IRQ_CLEAR_CAMSS_IRQ3_BMSK                                               0x200
#define HWIO_TCSR_MMSS_RPM_IRQ_CLEAR_RPM_IRQ_CLEAR_CAMSS_IRQ3_SHFT                                                 0x9
#define HWIO_TCSR_MMSS_RPM_IRQ_CLEAR_RPM_IRQ_CLEAR_CAMSS_IRQ4_BMSK                                               0x100
#define HWIO_TCSR_MMSS_RPM_IRQ_CLEAR_RPM_IRQ_CLEAR_CAMSS_IRQ4_SHFT                                                 0x8
#define HWIO_TCSR_MMSS_RPM_IRQ_CLEAR_RPM_IRQ_CLEAR_CAMSS_IRQ5_BMSK                                                0x80
#define HWIO_TCSR_MMSS_RPM_IRQ_CLEAR_RPM_IRQ_CLEAR_CAMSS_IRQ5_SHFT                                                 0x7
#define HWIO_TCSR_MMSS_RPM_IRQ_CLEAR_RPM_IRQ_CLEAR_CAMSS_IRQ6_BMSK                                                0x40
#define HWIO_TCSR_MMSS_RPM_IRQ_CLEAR_RPM_IRQ_CLEAR_CAMSS_IRQ6_SHFT                                                 0x6
#define HWIO_TCSR_MMSS_RPM_IRQ_CLEAR_RPM_IRQ_CLEAR_CAMSS_IRQ7_BMSK                                                0x20
#define HWIO_TCSR_MMSS_RPM_IRQ_CLEAR_RPM_IRQ_CLEAR_CAMSS_IRQ7_SHFT                                                 0x5
#define HWIO_TCSR_MMSS_RPM_IRQ_CLEAR_RPM_IRQ_CLEAR_CAMSS_IRQ8_BMSK                                                0x10
#define HWIO_TCSR_MMSS_RPM_IRQ_CLEAR_RPM_IRQ_CLEAR_CAMSS_IRQ8_SHFT                                                 0x4
#define HWIO_TCSR_MMSS_RPM_IRQ_CLEAR_RPM_IRQ_CLEAR_CAMSS_IRQ9_BMSK                                                 0x8
#define HWIO_TCSR_MMSS_RPM_IRQ_CLEAR_RPM_IRQ_CLEAR_CAMSS_IRQ9_SHFT                                                 0x3
#define HWIO_TCSR_MMSS_RPM_IRQ_CLEAR_RPM_IRQ_CLEAR_CSIPHY_0_IRQ_BMSK                                               0x4
#define HWIO_TCSR_MMSS_RPM_IRQ_CLEAR_RPM_IRQ_CLEAR_CSIPHY_0_IRQ_SHFT                                               0x2
#define HWIO_TCSR_MMSS_RPM_IRQ_CLEAR_RPM_IRQ_CLEAR_CSIPHY_1_IRQ_BMSK                                               0x2
#define HWIO_TCSR_MMSS_RPM_IRQ_CLEAR_RPM_IRQ_CLEAR_CSIPHY_1_IRQ_SHFT                                               0x1

#define HWIO_TCSR_MMSS_RPM_IRQ_STATUS_ADDR                                                                  (TCSR_TCSR_REGS_REG_BASE      + 0x00007008)
#define HWIO_TCSR_MMSS_RPM_IRQ_STATUS_PHYS                                                                  (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00007008)
#define HWIO_TCSR_MMSS_RPM_IRQ_STATUS_RMSK                                                                   0x1f1fffe
#define HWIO_TCSR_MMSS_RPM_IRQ_STATUS_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_RPM_IRQ_STATUS_ADDR, HWIO_TCSR_MMSS_RPM_IRQ_STATUS_RMSK)
#define HWIO_TCSR_MMSS_RPM_IRQ_STATUS_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_RPM_IRQ_STATUS_ADDR, m)
#define HWIO_TCSR_MMSS_RPM_IRQ_STATUS_RPM_IRQ_STATUS_VENUS0_IRQ_BMSK                                         0x1000000
#define HWIO_TCSR_MMSS_RPM_IRQ_STATUS_RPM_IRQ_STATUS_VENUS0_IRQ_SHFT                                              0x18
#define HWIO_TCSR_MMSS_RPM_IRQ_STATUS_RPM_IRQ_STATUS_CAMSS_IRQ10_BMSK                                         0x800000
#define HWIO_TCSR_MMSS_RPM_IRQ_STATUS_RPM_IRQ_STATUS_CAMSS_IRQ10_SHFT                                             0x17
#define HWIO_TCSR_MMSS_RPM_IRQ_STATUS_RPM_IRQ_STATUS_CAMSS_IRQ11_BMSK                                         0x400000
#define HWIO_TCSR_MMSS_RPM_IRQ_STATUS_RPM_IRQ_STATUS_CAMSS_IRQ11_SHFT                                             0x16
#define HWIO_TCSR_MMSS_RPM_IRQ_STATUS_RPM_IRQ_STATUS_CAMSS_IRQ12_BMSK                                         0x200000
#define HWIO_TCSR_MMSS_RPM_IRQ_STATUS_RPM_IRQ_STATUS_CAMSS_IRQ12_SHFT                                             0x15
#define HWIO_TCSR_MMSS_RPM_IRQ_STATUS_RPM_IRQ_STATUS_MDSS_IRQ_BMSK                                            0x100000
#define HWIO_TCSR_MMSS_RPM_IRQ_STATUS_RPM_IRQ_STATUS_MDSS_IRQ_SHFT                                                0x14
#define HWIO_TCSR_MMSS_RPM_IRQ_STATUS_RPM_IRQ_STATUS_GC_SYS_IRQ3_BMSK                                          0x10000
#define HWIO_TCSR_MMSS_RPM_IRQ_STATUS_RPM_IRQ_STATUS_GC_SYS_IRQ3_SHFT                                             0x10
#define HWIO_TCSR_MMSS_RPM_IRQ_STATUS_RPM_IRQ_STATUS_GC_SYS_IRQ2_BMSK                                           0x8000
#define HWIO_TCSR_MMSS_RPM_IRQ_STATUS_RPM_IRQ_STATUS_GC_SYS_IRQ2_SHFT                                              0xf
#define HWIO_TCSR_MMSS_RPM_IRQ_STATUS_RPM_IRQ_STATUS_GC_SYS_IRQ1_BMSK                                           0x4000
#define HWIO_TCSR_MMSS_RPM_IRQ_STATUS_RPM_IRQ_STATUS_GC_SYS_IRQ1_SHFT                                              0xe
#define HWIO_TCSR_MMSS_RPM_IRQ_STATUS_RPM_IRQ_STATUS_GC_SYS_IRQ0_BMSK                                           0x2000
#define HWIO_TCSR_MMSS_RPM_IRQ_STATUS_RPM_IRQ_STATUS_GC_SYS_IRQ0_SHFT                                              0xd
#define HWIO_TCSR_MMSS_RPM_IRQ_STATUS_RPM_IRQ_STATUS_CAMSS_IRQ0_BMSK                                            0x1000
#define HWIO_TCSR_MMSS_RPM_IRQ_STATUS_RPM_IRQ_STATUS_CAMSS_IRQ0_SHFT                                               0xc
#define HWIO_TCSR_MMSS_RPM_IRQ_STATUS_RPM_IRQ_STATUS_CAMSS_IRQ1_BMSK                                             0x800
#define HWIO_TCSR_MMSS_RPM_IRQ_STATUS_RPM_IRQ_STATUS_CAMSS_IRQ1_SHFT                                               0xb
#define HWIO_TCSR_MMSS_RPM_IRQ_STATUS_RPM_IRQ_STATUS_CAMSS_IRQ2_BMSK                                             0x400
#define HWIO_TCSR_MMSS_RPM_IRQ_STATUS_RPM_IRQ_STATUS_CAMSS_IRQ2_SHFT                                               0xa
#define HWIO_TCSR_MMSS_RPM_IRQ_STATUS_RPM_IRQ_STATUS_CAMSS_IRQ3_BMSK                                             0x200
#define HWIO_TCSR_MMSS_RPM_IRQ_STATUS_RPM_IRQ_STATUS_CAMSS_IRQ3_SHFT                                               0x9
#define HWIO_TCSR_MMSS_RPM_IRQ_STATUS_RPM_IRQ_STATUS_CAMSS_IRQ4_BMSK                                             0x100
#define HWIO_TCSR_MMSS_RPM_IRQ_STATUS_RPM_IRQ_STATUS_CAMSS_IRQ4_SHFT                                               0x8
#define HWIO_TCSR_MMSS_RPM_IRQ_STATUS_RPM_IRQ_STATUS_CAMSS_IRQ5_BMSK                                              0x80
#define HWIO_TCSR_MMSS_RPM_IRQ_STATUS_RPM_IRQ_STATUS_CAMSS_IRQ5_SHFT                                               0x7
#define HWIO_TCSR_MMSS_RPM_IRQ_STATUS_RPM_IRQ_STATUS_CAMSS_IRQ6_BMSK                                              0x40
#define HWIO_TCSR_MMSS_RPM_IRQ_STATUS_RPM_IRQ_STATUS_CAMSS_IRQ6_SHFT                                               0x6
#define HWIO_TCSR_MMSS_RPM_IRQ_STATUS_RPM_IRQ_STATUS_CAMSS_IRQ7_BMSK                                              0x20
#define HWIO_TCSR_MMSS_RPM_IRQ_STATUS_RPM_IRQ_STATUS_CAMSS_IRQ7_SHFT                                               0x5
#define HWIO_TCSR_MMSS_RPM_IRQ_STATUS_RPM_IRQ_STATUS_CAMSS_IRQ8_BMSK                                              0x10
#define HWIO_TCSR_MMSS_RPM_IRQ_STATUS_RPM_IRQ_STATUS_CAMSS_IRQ8_SHFT                                               0x4
#define HWIO_TCSR_MMSS_RPM_IRQ_STATUS_RPM_IRQ_STATUS_CAMSS_IRQ9_BMSK                                               0x8
#define HWIO_TCSR_MMSS_RPM_IRQ_STATUS_RPM_IRQ_STATUS_CAMSS_IRQ9_SHFT                                               0x3
#define HWIO_TCSR_MMSS_RPM_IRQ_STATUS_RPM_IRQ_STATUS_CSIPHY_0_IRQ_BMSK                                             0x4
#define HWIO_TCSR_MMSS_RPM_IRQ_STATUS_RPM_IRQ_STATUS_CSIPHY_0_IRQ_SHFT                                             0x2
#define HWIO_TCSR_MMSS_RPM_IRQ_STATUS_RPM_IRQ_STATUS_CSIPHY_1_IRQ_BMSK                                             0x2
#define HWIO_TCSR_MMSS_RPM_IRQ_STATUS_RPM_IRQ_STATUS_CSIPHY_1_IRQ_SHFT                                             0x1

#define HWIO_TCSR_MMSS_OXILI_GC_SYS_AHB_STATUS_ADDR                                                         (TCSR_TCSR_REGS_REG_BASE      + 0x00007010)
#define HWIO_TCSR_MMSS_OXILI_GC_SYS_AHB_STATUS_PHYS                                                         (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00007010)
#define HWIO_TCSR_MMSS_OXILI_GC_SYS_AHB_STATUS_RMSK                                                         0xffffffff
#define HWIO_TCSR_MMSS_OXILI_GC_SYS_AHB_STATUS_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_OXILI_GC_SYS_AHB_STATUS_ADDR, HWIO_TCSR_MMSS_OXILI_GC_SYS_AHB_STATUS_RMSK)
#define HWIO_TCSR_MMSS_OXILI_GC_SYS_AHB_STATUS_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_OXILI_GC_SYS_AHB_STATUS_ADDR, m)
#define HWIO_TCSR_MMSS_OXILI_GC_SYS_AHB_STATUS_OXILI_GC_SYS_AHB_STATUS_BMSK                                 0xffffffff
#define HWIO_TCSR_MMSS_OXILI_GC_SYS_AHB_STATUS_OXILI_GC_SYS_AHB_STATUS_SHFT                                        0x0

#define HWIO_TCSR_MMSS_OXILI_CMD_REG_ADDR                                                                   (TCSR_TCSR_REGS_REG_BASE      + 0x00007014)
#define HWIO_TCSR_MMSS_OXILI_CMD_REG_PHYS                                                                   (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00007014)
#define HWIO_TCSR_MMSS_OXILI_CMD_REG_RMSK                                                                          0xf
#define HWIO_TCSR_MMSS_OXILI_CMD_REG_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_OXILI_CMD_REG_ADDR, HWIO_TCSR_MMSS_OXILI_CMD_REG_RMSK)
#define HWIO_TCSR_MMSS_OXILI_CMD_REG_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_OXILI_CMD_REG_ADDR, m)
#define HWIO_TCSR_MMSS_OXILI_CMD_REG_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_OXILI_CMD_REG_ADDR,v)
#define HWIO_TCSR_MMSS_OXILI_CMD_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_OXILI_CMD_REG_ADDR,m,v,HWIO_TCSR_MMSS_OXILI_CMD_REG_IN)
#define HWIO_TCSR_MMSS_OXILI_CMD_REG_OXILI_CMD_BMSK                                                                0xf
#define HWIO_TCSR_MMSS_OXILI_CMD_REG_OXILI_CMD_SHFT                                                                0x0

#define HWIO_TCSR_MMSS_DSI_ULP_CLAMP_CTRL_ADDR                                                              (TCSR_TCSR_REGS_REG_BASE      + 0x00007020)
#define HWIO_TCSR_MMSS_DSI_ULP_CLAMP_CTRL_PHYS                                                              (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00007020)
#define HWIO_TCSR_MMSS_DSI_ULP_CLAMP_CTRL_RMSK                                                                  0x83ff
#define HWIO_TCSR_MMSS_DSI_ULP_CLAMP_CTRL_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_DSI_ULP_CLAMP_CTRL_ADDR, HWIO_TCSR_MMSS_DSI_ULP_CLAMP_CTRL_RMSK)
#define HWIO_TCSR_MMSS_DSI_ULP_CLAMP_CTRL_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_DSI_ULP_CLAMP_CTRL_ADDR, m)
#define HWIO_TCSR_MMSS_DSI_ULP_CLAMP_CTRL_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_DSI_ULP_CLAMP_CTRL_ADDR,v)
#define HWIO_TCSR_MMSS_DSI_ULP_CLAMP_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_DSI_ULP_CLAMP_CTRL_ADDR,m,v,HWIO_TCSR_MMSS_DSI_ULP_CLAMP_CTRL_IN)
#define HWIO_TCSR_MMSS_DSI_ULP_CLAMP_CTRL_DSI0_CLAMP_EN_BMSK                                                    0x8000
#define HWIO_TCSR_MMSS_DSI_ULP_CLAMP_CTRL_DSI0_CLAMP_EN_SHFT                                                       0xf
#define HWIO_TCSR_MMSS_DSI_ULP_CLAMP_CTRL_DSI0_CLKLN_EN_BMSK                                                     0x200
#define HWIO_TCSR_MMSS_DSI_ULP_CLAMP_CTRL_DSI0_CLKLN_EN_SHFT                                                       0x9
#define HWIO_TCSR_MMSS_DSI_ULP_CLAMP_CTRL_DSI0_CLKLN_ULPS_REQUEST_BMSK                                           0x100
#define HWIO_TCSR_MMSS_DSI_ULP_CLAMP_CTRL_DSI0_CLKLN_ULPS_REQUEST_SHFT                                             0x8
#define HWIO_TCSR_MMSS_DSI_ULP_CLAMP_CTRL_DSI0_DLN0_EN_BMSK                                                       0x80
#define HWIO_TCSR_MMSS_DSI_ULP_CLAMP_CTRL_DSI0_DLN0_EN_SHFT                                                        0x7
#define HWIO_TCSR_MMSS_DSI_ULP_CLAMP_CTRL_DSI0_DLN0_ULPS_REQUEST_BMSK                                             0x40
#define HWIO_TCSR_MMSS_DSI_ULP_CLAMP_CTRL_DSI0_DLN0_ULPS_REQUEST_SHFT                                              0x6
#define HWIO_TCSR_MMSS_DSI_ULP_CLAMP_CTRL_DSI0_DLN1_EN_BMSK                                                       0x20
#define HWIO_TCSR_MMSS_DSI_ULP_CLAMP_CTRL_DSI0_DLN1_EN_SHFT                                                        0x5
#define HWIO_TCSR_MMSS_DSI_ULP_CLAMP_CTRL_DSI0_DLN1_ULPS_REQUEST_BMSK                                             0x10
#define HWIO_TCSR_MMSS_DSI_ULP_CLAMP_CTRL_DSI0_DLN1_ULPS_REQUEST_SHFT                                              0x4
#define HWIO_TCSR_MMSS_DSI_ULP_CLAMP_CTRL_DSI0_DLN2_EN_BMSK                                                        0x8
#define HWIO_TCSR_MMSS_DSI_ULP_CLAMP_CTRL_DSI0_DLN2_EN_SHFT                                                        0x3
#define HWIO_TCSR_MMSS_DSI_ULP_CLAMP_CTRL_DSI0_DLN2_ULPS_REQUEST_BMSK                                              0x4
#define HWIO_TCSR_MMSS_DSI_ULP_CLAMP_CTRL_DSI0_DLN2_ULPS_REQUEST_SHFT                                              0x2
#define HWIO_TCSR_MMSS_DSI_ULP_CLAMP_CTRL_DSI0_DLN3_EN_BMSK                                                        0x2
#define HWIO_TCSR_MMSS_DSI_ULP_CLAMP_CTRL_DSI0_DLN3_EN_SHFT                                                        0x1
#define HWIO_TCSR_MMSS_DSI_ULP_CLAMP_CTRL_DSI0_DLN3_ULPS_REQUEST_BMSK                                              0x1
#define HWIO_TCSR_MMSS_DSI_ULP_CLAMP_CTRL_DSI0_DLN3_ULPS_REQUEST_SHFT                                              0x0

#define HWIO_TCSR_MMSS_IMEM_FSCGC_TIMERS_ADDR                                                               (TCSR_TCSR_REGS_REG_BASE      + 0x00007030)
#define HWIO_TCSR_MMSS_IMEM_FSCGC_TIMERS_PHYS                                                               (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00007030)
#define HWIO_TCSR_MMSS_IMEM_FSCGC_TIMERS_RMSK                                                                     0xff
#define HWIO_TCSR_MMSS_IMEM_FSCGC_TIMERS_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_IMEM_FSCGC_TIMERS_ADDR, HWIO_TCSR_MMSS_IMEM_FSCGC_TIMERS_RMSK)
#define HWIO_TCSR_MMSS_IMEM_FSCGC_TIMERS_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_IMEM_FSCGC_TIMERS_ADDR, m)
#define HWIO_TCSR_MMSS_IMEM_FSCGC_TIMERS_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_IMEM_FSCGC_TIMERS_ADDR,v)
#define HWIO_TCSR_MMSS_IMEM_FSCGC_TIMERS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_IMEM_FSCGC_TIMERS_ADDR,m,v,HWIO_TCSR_MMSS_IMEM_FSCGC_TIMERS_IN)
#define HWIO_TCSR_MMSS_IMEM_FSCGC_TIMERS_WAKEUP_COUNTER_BMSK                                                      0xf0
#define HWIO_TCSR_MMSS_IMEM_FSCGC_TIMERS_WAKEUP_COUNTER_SHFT                                                       0x4
#define HWIO_TCSR_MMSS_IMEM_FSCGC_TIMERS_TO_SLEEP_COUNTER_BMSK                                                     0xf
#define HWIO_TCSR_MMSS_IMEM_FSCGC_TIMERS_TO_SLEEP_COUNTER_SHFT                                                     0x0

#define HWIO_TCSR_MMSS_IMEM_EX_FSCGC_CONTROL_ADDR                                                           (TCSR_TCSR_REGS_REG_BASE      + 0x00007034)
#define HWIO_TCSR_MMSS_IMEM_EX_FSCGC_CONTROL_PHYS                                                           (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00007034)
#define HWIO_TCSR_MMSS_IMEM_EX_FSCGC_CONTROL_RMSK                                                             0xffffff
#define HWIO_TCSR_MMSS_IMEM_EX_FSCGC_CONTROL_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_IMEM_EX_FSCGC_CONTROL_ADDR, HWIO_TCSR_MMSS_IMEM_EX_FSCGC_CONTROL_RMSK)
#define HWIO_TCSR_MMSS_IMEM_EX_FSCGC_CONTROL_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_IMEM_EX_FSCGC_CONTROL_ADDR, m)
#define HWIO_TCSR_MMSS_IMEM_EX_FSCGC_CONTROL_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_IMEM_EX_FSCGC_CONTROL_ADDR,v)
#define HWIO_TCSR_MMSS_IMEM_EX_FSCGC_CONTROL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_IMEM_EX_FSCGC_CONTROL_ADDR,m,v,HWIO_TCSR_MMSS_IMEM_EX_FSCGC_CONTROL_IN)
#define HWIO_TCSR_MMSS_IMEM_EX_FSCGC_CONTROL_HALT_CLOCK_BMSK                                                  0xff0000
#define HWIO_TCSR_MMSS_IMEM_EX_FSCGC_CONTROL_HALT_CLOCK_SHFT                                                      0x10
#define HWIO_TCSR_MMSS_IMEM_EX_FSCGC_CONTROL_CORE_ON_BMSK                                                       0xff00
#define HWIO_TCSR_MMSS_IMEM_EX_FSCGC_CONTROL_CORE_ON_SHFT                                                          0x8
#define HWIO_TCSR_MMSS_IMEM_EX_FSCGC_CONTROL_PERIF_ON_BMSK                                                        0xff
#define HWIO_TCSR_MMSS_IMEM_EX_FSCGC_CONTROL_PERIF_ON_SHFT                                                         0x0

#define HWIO_TCSR_MMSS_IMEM_RAM_CONFIG_ADDR                                                                 (TCSR_TCSR_REGS_REG_BASE      + 0x00007038)
#define HWIO_TCSR_MMSS_IMEM_RAM_CONFIG_PHYS                                                                 (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00007038)
#define HWIO_TCSR_MMSS_IMEM_RAM_CONFIG_RMSK                                                                        0x7
#define HWIO_TCSR_MMSS_IMEM_RAM_CONFIG_IN          \
        in_dword_masked(HWIO_TCSR_MMSS_IMEM_RAM_CONFIG_ADDR, HWIO_TCSR_MMSS_IMEM_RAM_CONFIG_RMSK)
#define HWIO_TCSR_MMSS_IMEM_RAM_CONFIG_INM(m)      \
        in_dword_masked(HWIO_TCSR_MMSS_IMEM_RAM_CONFIG_ADDR, m)
#define HWIO_TCSR_MMSS_IMEM_RAM_CONFIG_OUT(v)      \
        out_dword(HWIO_TCSR_MMSS_IMEM_RAM_CONFIG_ADDR,v)
#define HWIO_TCSR_MMSS_IMEM_RAM_CONFIG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MMSS_IMEM_RAM_CONFIG_ADDR,m,v,HWIO_TCSR_MMSS_IMEM_RAM_CONFIG_IN)
#define HWIO_TCSR_MMSS_IMEM_RAM_CONFIG_EX_RAM_CLK_EN_BMSK                                                          0x4
#define HWIO_TCSR_MMSS_IMEM_RAM_CONFIG_EX_RAM_CLK_EN_SHFT                                                          0x2
#define HWIO_TCSR_MMSS_IMEM_RAM_CONFIG_EX_RAM_CONFIG_BMSK                                                          0x3
#define HWIO_TCSR_MMSS_IMEM_RAM_CONFIG_EX_RAM_CONFIG_SHFT                                                          0x0

#define HWIO_TCSR_TBU_BYPASS_ENABLE_ADDR                                                                    (TCSR_TCSR_REGS_REG_BASE      + 0x00008000)
#define HWIO_TCSR_TBU_BYPASS_ENABLE_PHYS                                                                    (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00008000)
#define HWIO_TCSR_TBU_BYPASS_ENABLE_RMSK                                                                          0xff
#define HWIO_TCSR_TBU_BYPASS_ENABLE_IN          \
        in_dword_masked(HWIO_TCSR_TBU_BYPASS_ENABLE_ADDR, HWIO_TCSR_TBU_BYPASS_ENABLE_RMSK)
#define HWIO_TCSR_TBU_BYPASS_ENABLE_INM(m)      \
        in_dword_masked(HWIO_TCSR_TBU_BYPASS_ENABLE_ADDR, m)
#define HWIO_TCSR_TBU_BYPASS_ENABLE_OUT(v)      \
        out_dword(HWIO_TCSR_TBU_BYPASS_ENABLE_ADDR,v)
#define HWIO_TCSR_TBU_BYPASS_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_TBU_BYPASS_ENABLE_ADDR,m,v,HWIO_TCSR_TBU_BYPASS_ENABLE_IN)
#define HWIO_TCSR_TBU_BYPASS_ENABLE_TBU_BYPASS_ENABLE_BMSK                                                        0xff
#define HWIO_TCSR_TBU_BYPASS_ENABLE_TBU_BYPASS_ENABLE_SHFT                                                         0x0

#define HWIO_TCSR_SYS_POWER_CTRL_ADDR                                                                       (TCSR_TCSR_REGS_REG_BASE      + 0x00010000)
#define HWIO_TCSR_SYS_POWER_CTRL_PHYS                                                                       (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00010000)
#define HWIO_TCSR_SYS_POWER_CTRL_RMSK                                                                           0xffff
#define HWIO_TCSR_SYS_POWER_CTRL_IN          \
        in_dword_masked(HWIO_TCSR_SYS_POWER_CTRL_ADDR, HWIO_TCSR_SYS_POWER_CTRL_RMSK)
#define HWIO_TCSR_SYS_POWER_CTRL_INM(m)      \
        in_dword_masked(HWIO_TCSR_SYS_POWER_CTRL_ADDR, m)
#define HWIO_TCSR_SYS_POWER_CTRL_OUT(v)      \
        out_dword(HWIO_TCSR_SYS_POWER_CTRL_ADDR,v)
#define HWIO_TCSR_SYS_POWER_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SYS_POWER_CTRL_ADDR,m,v,HWIO_TCSR_SYS_POWER_CTRL_IN)
#define HWIO_TCSR_SYS_POWER_CTRL_SYS_POWER_CTRL_BMSK                                                            0xffff
#define HWIO_TCSR_SYS_POWER_CTRL_SYS_POWER_CTRL_SHFT                                                               0x0

#define HWIO_TCSR_USB_CORE_ID_ADDR                                                                          (TCSR_TCSR_REGS_REG_BASE      + 0x00010004)
#define HWIO_TCSR_USB_CORE_ID_PHYS                                                                          (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00010004)
#define HWIO_TCSR_USB_CORE_ID_RMSK                                                                                 0x3
#define HWIO_TCSR_USB_CORE_ID_IN          \
        in_dword_masked(HWIO_TCSR_USB_CORE_ID_ADDR, HWIO_TCSR_USB_CORE_ID_RMSK)
#define HWIO_TCSR_USB_CORE_ID_INM(m)      \
        in_dword_masked(HWIO_TCSR_USB_CORE_ID_ADDR, m)
#define HWIO_TCSR_USB_CORE_ID_OUT(v)      \
        out_dword(HWIO_TCSR_USB_CORE_ID_ADDR,v)
#define HWIO_TCSR_USB_CORE_ID_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_USB_CORE_ID_ADDR,m,v,HWIO_TCSR_USB_CORE_ID_IN)
#define HWIO_TCSR_USB_CORE_ID_USB_CORE_ID_BMSK                                                                     0x3
#define HWIO_TCSR_USB_CORE_ID_USB_CORE_ID_SHFT                                                                     0x0

#define HWIO_TCSR_Q6_ISO_AXIM2_REQPEND_ADDR                                                                 (TCSR_TCSR_REGS_REG_BASE      + 0x00010008)
#define HWIO_TCSR_Q6_ISO_AXIM2_REQPEND_PHYS                                                                 (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00010008)
#define HWIO_TCSR_Q6_ISO_AXIM2_REQPEND_RMSK                                                                        0x1
#define HWIO_TCSR_Q6_ISO_AXIM2_REQPEND_IN          \
        in_dword_masked(HWIO_TCSR_Q6_ISO_AXIM2_REQPEND_ADDR, HWIO_TCSR_Q6_ISO_AXIM2_REQPEND_RMSK)
#define HWIO_TCSR_Q6_ISO_AXIM2_REQPEND_INM(m)      \
        in_dword_masked(HWIO_TCSR_Q6_ISO_AXIM2_REQPEND_ADDR, m)
#define HWIO_TCSR_Q6_ISO_AXIM2_REQPEND_OUT(v)      \
        out_dword(HWIO_TCSR_Q6_ISO_AXIM2_REQPEND_ADDR,v)
#define HWIO_TCSR_Q6_ISO_AXIM2_REQPEND_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_Q6_ISO_AXIM2_REQPEND_ADDR,m,v,HWIO_TCSR_Q6_ISO_AXIM2_REQPEND_IN)
#define HWIO_TCSR_Q6_ISO_AXIM2_REQPEND_Q6_ISO_AXIM2_REQPEND_STAT_BMSK                                              0x1
#define HWIO_TCSR_Q6_ISO_AXIM2_REQPEND_Q6_ISO_AXIM2_REQPEND_STAT_SHFT                                              0x0

#define HWIO_TCSR_TCSR_USB_PHY_MISC_ADDR                                                                    (TCSR_TCSR_REGS_REG_BASE      + 0x00010240)
#define HWIO_TCSR_TCSR_USB_PHY_MISC_PHYS                                                                    (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00010240)
#define HWIO_TCSR_TCSR_USB_PHY_MISC_RMSK                                                                    0xffffffff
#define HWIO_TCSR_TCSR_USB_PHY_MISC_IN          \
        in_dword_masked(HWIO_TCSR_TCSR_USB_PHY_MISC_ADDR, HWIO_TCSR_TCSR_USB_PHY_MISC_RMSK)
#define HWIO_TCSR_TCSR_USB_PHY_MISC_INM(m)      \
        in_dword_masked(HWIO_TCSR_TCSR_USB_PHY_MISC_ADDR, m)
#define HWIO_TCSR_TCSR_USB_PHY_MISC_OUT(v)      \
        out_dword(HWIO_TCSR_TCSR_USB_PHY_MISC_ADDR,v)
#define HWIO_TCSR_TCSR_USB_PHY_MISC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_TCSR_USB_PHY_MISC_ADDR,m,v,HWIO_TCSR_TCSR_USB_PHY_MISC_IN)
#define HWIO_TCSR_TCSR_USB_PHY_MISC_TCSR_USB_PHY_MISC_BMSK                                                  0xffffffff
#define HWIO_TCSR_TCSR_USB_PHY_MISC_TCSR_USB_PHY_MISC_SHFT                                                         0x0

#define HWIO_TCSR_TCSR_USB_PHY_VLS_CLAMP_ADDR                                                               (TCSR_TCSR_REGS_REG_BASE      + 0x00010244)
#define HWIO_TCSR_TCSR_USB_PHY_VLS_CLAMP_PHYS                                                               (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00010244)
#define HWIO_TCSR_TCSR_USB_PHY_VLS_CLAMP_RMSK                                                               0xffffffff
#define HWIO_TCSR_TCSR_USB_PHY_VLS_CLAMP_IN          \
        in_dword_masked(HWIO_TCSR_TCSR_USB_PHY_VLS_CLAMP_ADDR, HWIO_TCSR_TCSR_USB_PHY_VLS_CLAMP_RMSK)
#define HWIO_TCSR_TCSR_USB_PHY_VLS_CLAMP_INM(m)      \
        in_dword_masked(HWIO_TCSR_TCSR_USB_PHY_VLS_CLAMP_ADDR, m)
#define HWIO_TCSR_TCSR_USB_PHY_VLS_CLAMP_OUT(v)      \
        out_dword(HWIO_TCSR_TCSR_USB_PHY_VLS_CLAMP_ADDR,v)
#define HWIO_TCSR_TCSR_USB_PHY_VLS_CLAMP_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_TCSR_USB_PHY_VLS_CLAMP_ADDR,m,v,HWIO_TCSR_TCSR_USB_PHY_VLS_CLAMP_IN)
#define HWIO_TCSR_TCSR_USB_PHY_VLS_CLAMP_TCSR_USB_PHY_VLS_CLAMP_BMSK                                        0xffffffff
#define HWIO_TCSR_TCSR_USB_PHY_VLS_CLAMP_TCSR_USB_PHY_VLS_CLAMP_SHFT                                               0x0

#define HWIO_TCSR_LDO_SLEEP_CTRL_ADDR                                                                       (TCSR_TCSR_REGS_REG_BASE      + 0x00011000)
#define HWIO_TCSR_LDO_SLEEP_CTRL_PHYS                                                                       (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00011000)
#define HWIO_TCSR_LDO_SLEEP_CTRL_RMSK                                                                              0x1
#define HWIO_TCSR_LDO_SLEEP_CTRL_IN          \
        in_dword_masked(HWIO_TCSR_LDO_SLEEP_CTRL_ADDR, HWIO_TCSR_LDO_SLEEP_CTRL_RMSK)
#define HWIO_TCSR_LDO_SLEEP_CTRL_INM(m)      \
        in_dword_masked(HWIO_TCSR_LDO_SLEEP_CTRL_ADDR, m)
#define HWIO_TCSR_LDO_SLEEP_CTRL_OUT(v)      \
        out_dword(HWIO_TCSR_LDO_SLEEP_CTRL_ADDR,v)
#define HWIO_TCSR_LDO_SLEEP_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LDO_SLEEP_CTRL_ADDR,m,v,HWIO_TCSR_LDO_SLEEP_CTRL_IN)
#define HWIO_TCSR_LDO_SLEEP_CTRL_LDO_SLEEP_BMSK                                                                    0x1
#define HWIO_TCSR_LDO_SLEEP_CTRL_LDO_SLEEP_SHFT                                                                    0x0

#define HWIO_TCSR_LDO_UPDATE_STATE_CTRL_ADDR                                                                (TCSR_TCSR_REGS_REG_BASE      + 0x00011004)
#define HWIO_TCSR_LDO_UPDATE_STATE_CTRL_PHYS                                                                (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00011004)
#define HWIO_TCSR_LDO_UPDATE_STATE_CTRL_RMSK                                                                       0x1
#define HWIO_TCSR_LDO_UPDATE_STATE_CTRL_IN          \
        in_dword_masked(HWIO_TCSR_LDO_UPDATE_STATE_CTRL_ADDR, HWIO_TCSR_LDO_UPDATE_STATE_CTRL_RMSK)
#define HWIO_TCSR_LDO_UPDATE_STATE_CTRL_INM(m)      \
        in_dword_masked(HWIO_TCSR_LDO_UPDATE_STATE_CTRL_ADDR, m)
#define HWIO_TCSR_LDO_UPDATE_STATE_CTRL_OUT(v)      \
        out_dword(HWIO_TCSR_LDO_UPDATE_STATE_CTRL_ADDR,v)
#define HWIO_TCSR_LDO_UPDATE_STATE_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LDO_UPDATE_STATE_CTRL_ADDR,m,v,HWIO_TCSR_LDO_UPDATE_STATE_CTRL_IN)
#define HWIO_TCSR_LDO_UPDATE_STATE_CTRL_LDO_UPDATE_STATE_BMSK                                                      0x1
#define HWIO_TCSR_LDO_UPDATE_STATE_CTRL_LDO_UPDATE_STATE_SHFT                                                      0x0

#define HWIO_TCSR_LDO_OBIAS_CTRL_ADDR                                                                       (TCSR_TCSR_REGS_REG_BASE      + 0x00011008)
#define HWIO_TCSR_LDO_OBIAS_CTRL_PHYS                                                                       (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00011008)
#define HWIO_TCSR_LDO_OBIAS_CTRL_RMSK                                                                              0x1
#define HWIO_TCSR_LDO_OBIAS_CTRL_IN          \
        in_dword_masked(HWIO_TCSR_LDO_OBIAS_CTRL_ADDR, HWIO_TCSR_LDO_OBIAS_CTRL_RMSK)
#define HWIO_TCSR_LDO_OBIAS_CTRL_INM(m)      \
        in_dword_masked(HWIO_TCSR_LDO_OBIAS_CTRL_ADDR, m)
#define HWIO_TCSR_LDO_OBIAS_CTRL_OUT(v)      \
        out_dword(HWIO_TCSR_LDO_OBIAS_CTRL_ADDR,v)
#define HWIO_TCSR_LDO_OBIAS_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LDO_OBIAS_CTRL_ADDR,m,v,HWIO_TCSR_LDO_OBIAS_CTRL_IN)
#define HWIO_TCSR_LDO_OBIAS_CTRL_LDO_OBIAS_ON_BMSK                                                                 0x1
#define HWIO_TCSR_LDO_OBIAS_CTRL_LDO_OBIAS_ON_SHFT                                                                 0x0

#define HWIO_TCSR_LDO_VREF_CONFIG_ADDR                                                                      (TCSR_TCSR_REGS_REG_BASE      + 0x0001100c)
#define HWIO_TCSR_LDO_VREF_CONFIG_PHYS                                                                      (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x0001100c)
#define HWIO_TCSR_LDO_VREF_CONFIG_RMSK                                                                             0xf
#define HWIO_TCSR_LDO_VREF_CONFIG_IN          \
        in_dword_masked(HWIO_TCSR_LDO_VREF_CONFIG_ADDR, HWIO_TCSR_LDO_VREF_CONFIG_RMSK)
#define HWIO_TCSR_LDO_VREF_CONFIG_INM(m)      \
        in_dword_masked(HWIO_TCSR_LDO_VREF_CONFIG_ADDR, m)
#define HWIO_TCSR_LDO_VREF_CONFIG_OUT(v)      \
        out_dword(HWIO_TCSR_LDO_VREF_CONFIG_ADDR,v)
#define HWIO_TCSR_LDO_VREF_CONFIG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LDO_VREF_CONFIG_ADDR,m,v,HWIO_TCSR_LDO_VREF_CONFIG_IN)
#define HWIO_TCSR_LDO_VREF_CONFIG_LDO_VREF_CONFIG_BMSK                                                             0xf
#define HWIO_TCSR_LDO_VREF_CONFIG_LDO_VREF_CONFIG_SHFT                                                             0x0

#define HWIO_TCSR_LDO_IB_CONFIG_ADDR                                                                        (TCSR_TCSR_REGS_REG_BASE      + 0x00011010)
#define HWIO_TCSR_LDO_IB_CONFIG_PHYS                                                                        (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00011010)
#define HWIO_TCSR_LDO_IB_CONFIG_RMSK                                                                               0x7
#define HWIO_TCSR_LDO_IB_CONFIG_IN          \
        in_dword_masked(HWIO_TCSR_LDO_IB_CONFIG_ADDR, HWIO_TCSR_LDO_IB_CONFIG_RMSK)
#define HWIO_TCSR_LDO_IB_CONFIG_INM(m)      \
        in_dword_masked(HWIO_TCSR_LDO_IB_CONFIG_ADDR, m)
#define HWIO_TCSR_LDO_IB_CONFIG_OUT(v)      \
        out_dword(HWIO_TCSR_LDO_IB_CONFIG_ADDR,v)
#define HWIO_TCSR_LDO_IB_CONFIG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LDO_IB_CONFIG_ADDR,m,v,HWIO_TCSR_LDO_IB_CONFIG_IN)
#define HWIO_TCSR_LDO_IB_CONFIG_LDO_IB_CONFIG_BMSK                                                                 0x7
#define HWIO_TCSR_LDO_IB_CONFIG_LDO_IB_CONFIG_SHFT                                                                 0x0

#define HWIO_TCSR_LDO_BGC_CONFIG_ADDR                                                                       (TCSR_TCSR_REGS_REG_BASE      + 0x00011014)
#define HWIO_TCSR_LDO_BGC_CONFIG_PHYS                                                                       (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00011014)
#define HWIO_TCSR_LDO_BGC_CONFIG_RMSK                                                                              0x7
#define HWIO_TCSR_LDO_BGC_CONFIG_IN          \
        in_dword_masked(HWIO_TCSR_LDO_BGC_CONFIG_ADDR, HWIO_TCSR_LDO_BGC_CONFIG_RMSK)
#define HWIO_TCSR_LDO_BGC_CONFIG_INM(m)      \
        in_dword_masked(HWIO_TCSR_LDO_BGC_CONFIG_ADDR, m)
#define HWIO_TCSR_LDO_BGC_CONFIG_OUT(v)      \
        out_dword(HWIO_TCSR_LDO_BGC_CONFIG_ADDR,v)
#define HWIO_TCSR_LDO_BGC_CONFIG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LDO_BGC_CONFIG_ADDR,m,v,HWIO_TCSR_LDO_BGC_CONFIG_IN)
#define HWIO_TCSR_LDO_BGC_CONFIG_LDO_BGC_BMSK                                                                      0x7
#define HWIO_TCSR_LDO_BGC_CONFIG_LDO_BGC_SHFT                                                                      0x0

#define HWIO_TCSR_LDO_VREF_CTRL_ADDR                                                                        (TCSR_TCSR_REGS_REG_BASE      + 0x00011018)
#define HWIO_TCSR_LDO_VREF_CTRL_PHYS                                                                        (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00011018)
#define HWIO_TCSR_LDO_VREF_CTRL_RMSK                                                                           0x10001
#define HWIO_TCSR_LDO_VREF_CTRL_IN          \
        in_dword_masked(HWIO_TCSR_LDO_VREF_CTRL_ADDR, HWIO_TCSR_LDO_VREF_CTRL_RMSK)
#define HWIO_TCSR_LDO_VREF_CTRL_INM(m)      \
        in_dword_masked(HWIO_TCSR_LDO_VREF_CTRL_ADDR, m)
#define HWIO_TCSR_LDO_VREF_CTRL_OUT(v)      \
        out_dword(HWIO_TCSR_LDO_VREF_CTRL_ADDR,v)
#define HWIO_TCSR_LDO_VREF_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LDO_VREF_CTRL_ADDR,m,v,HWIO_TCSR_LDO_VREF_CTRL_IN)
#define HWIO_TCSR_LDO_VREF_CTRL_LDO_VREF_SEL_OVR_BMSK                                                          0x10000
#define HWIO_TCSR_LDO_VREF_CTRL_LDO_VREF_SEL_OVR_SHFT                                                             0x10
#define HWIO_TCSR_LDO_VREF_CTRL_LDO_VREF_SEL_SW_BMSK                                                               0x1
#define HWIO_TCSR_LDO_VREF_CTRL_LDO_VREF_SEL_SW_SHFT                                                               0x0

#define HWIO_TCSR_LDO_LD_EN_ADDR                                                                            (TCSR_TCSR_REGS_REG_BASE      + 0x0001101c)
#define HWIO_TCSR_LDO_LD_EN_PHYS                                                                            (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x0001101c)
#define HWIO_TCSR_LDO_LD_EN_RMSK                                                                            0x80000000
#define HWIO_TCSR_LDO_LD_EN_IN          \
        in_dword_masked(HWIO_TCSR_LDO_LD_EN_ADDR, HWIO_TCSR_LDO_LD_EN_RMSK)
#define HWIO_TCSR_LDO_LD_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_LDO_LD_EN_ADDR, m)
#define HWIO_TCSR_LDO_LD_EN_OUT(v)      \
        out_dword(HWIO_TCSR_LDO_LD_EN_ADDR,v)
#define HWIO_TCSR_LDO_LD_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LDO_LD_EN_ADDR,m,v,HWIO_TCSR_LDO_LD_EN_IN)
#define HWIO_TCSR_LDO_LD_EN_LDO_LD_EN_BMSK                                                                  0x80000000
#define HWIO_TCSR_LDO_LD_EN_LDO_LD_EN_SHFT                                                                        0x1f

#define HWIO_TCSR_LDO_LD_CTRL_ADDR                                                                          (TCSR_TCSR_REGS_REG_BASE      + 0x00011020)
#define HWIO_TCSR_LDO_LD_CTRL_PHYS                                                                          (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00011020)
#define HWIO_TCSR_LDO_LD_CTRL_RMSK                                                                            0xff00ff
#define HWIO_TCSR_LDO_LD_CTRL_IN          \
        in_dword_masked(HWIO_TCSR_LDO_LD_CTRL_ADDR, HWIO_TCSR_LDO_LD_CTRL_RMSK)
#define HWIO_TCSR_LDO_LD_CTRL_INM(m)      \
        in_dword_masked(HWIO_TCSR_LDO_LD_CTRL_ADDR, m)
#define HWIO_TCSR_LDO_LD_CTRL_OUT(v)      \
        out_dword(HWIO_TCSR_LDO_LD_CTRL_ADDR,v)
#define HWIO_TCSR_LDO_LD_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LDO_LD_CTRL_ADDR,m,v,HWIO_TCSR_LDO_LD_CTRL_IN)
#define HWIO_TCSR_LDO_LD_CTRL_LDO_LD_MSB_BMSK                                                                 0xff0000
#define HWIO_TCSR_LDO_LD_CTRL_LDO_LD_MSB_SHFT                                                                     0x10
#define HWIO_TCSR_LDO_LD_CTRL_LDO_LD_LSB_BMSK                                                                     0xff
#define HWIO_TCSR_LDO_LD_CTRL_LDO_LD_LSB_SHFT                                                                      0x0

#define HWIO_TCSR_LDO_OSC_RESETB_ADDR                                                                       (TCSR_TCSR_REGS_REG_BASE      + 0x00011024)
#define HWIO_TCSR_LDO_OSC_RESETB_PHYS                                                                       (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00011024)
#define HWIO_TCSR_LDO_OSC_RESETB_RMSK                                                                       0x80000000
#define HWIO_TCSR_LDO_OSC_RESETB_IN          \
        in_dword_masked(HWIO_TCSR_LDO_OSC_RESETB_ADDR, HWIO_TCSR_LDO_OSC_RESETB_RMSK)
#define HWIO_TCSR_LDO_OSC_RESETB_INM(m)      \
        in_dword_masked(HWIO_TCSR_LDO_OSC_RESETB_ADDR, m)
#define HWIO_TCSR_LDO_OSC_RESETB_OUT(v)      \
        out_dword(HWIO_TCSR_LDO_OSC_RESETB_ADDR,v)
#define HWIO_TCSR_LDO_OSC_RESETB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LDO_OSC_RESETB_ADDR,m,v,HWIO_TCSR_LDO_OSC_RESETB_IN)
#define HWIO_TCSR_LDO_OSC_RESETB_LDO_OSC_RESETB_BMSK                                                        0x80000000
#define HWIO_TCSR_LDO_OSC_RESETB_LDO_OSC_RESETB_SHFT                                                              0x1f

#define HWIO_TCSR_LDO_OSC_CTRL_ADDR                                                                         (TCSR_TCSR_REGS_REG_BASE      + 0x00011028)
#define HWIO_TCSR_LDO_OSC_CTRL_PHYS                                                                         (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00011028)
#define HWIO_TCSR_LDO_OSC_CTRL_RMSK                                                                                0x3
#define HWIO_TCSR_LDO_OSC_CTRL_IN          \
        in_dword_masked(HWIO_TCSR_LDO_OSC_CTRL_ADDR, HWIO_TCSR_LDO_OSC_CTRL_RMSK)
#define HWIO_TCSR_LDO_OSC_CTRL_INM(m)      \
        in_dword_masked(HWIO_TCSR_LDO_OSC_CTRL_ADDR, m)
#define HWIO_TCSR_LDO_OSC_CTRL_OUT(v)      \
        out_dword(HWIO_TCSR_LDO_OSC_CTRL_ADDR,v)
#define HWIO_TCSR_LDO_OSC_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LDO_OSC_CTRL_ADDR,m,v,HWIO_TCSR_LDO_OSC_CTRL_IN)
#define HWIO_TCSR_LDO_OSC_CTRL_LDO_OSC_CTRL_BMSK                                                                   0x3
#define HWIO_TCSR_LDO_OSC_CTRL_LDO_OSC_CTRL_SHFT                                                                   0x0

#define HWIO_TCSR_LDO_DFT_EN_CTRL_ADDR                                                                      (TCSR_TCSR_REGS_REG_BASE      + 0x0001102c)
#define HWIO_TCSR_LDO_DFT_EN_CTRL_PHYS                                                                      (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x0001102c)
#define HWIO_TCSR_LDO_DFT_EN_CTRL_RMSK                                                                      0x80000000
#define HWIO_TCSR_LDO_DFT_EN_CTRL_IN          \
        in_dword_masked(HWIO_TCSR_LDO_DFT_EN_CTRL_ADDR, HWIO_TCSR_LDO_DFT_EN_CTRL_RMSK)
#define HWIO_TCSR_LDO_DFT_EN_CTRL_INM(m)      \
        in_dword_masked(HWIO_TCSR_LDO_DFT_EN_CTRL_ADDR, m)
#define HWIO_TCSR_LDO_DFT_EN_CTRL_OUT(v)      \
        out_dword(HWIO_TCSR_LDO_DFT_EN_CTRL_ADDR,v)
#define HWIO_TCSR_LDO_DFT_EN_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LDO_DFT_EN_CTRL_ADDR,m,v,HWIO_TCSR_LDO_DFT_EN_CTRL_IN)
#define HWIO_TCSR_LDO_DFT_EN_CTRL_LDO_DFT_EN_BMSK                                                           0x80000000
#define HWIO_TCSR_LDO_DFT_EN_CTRL_LDO_DFT_EN_SHFT                                                                 0x1f

#define HWIO_TCSR_LDO_DFT_CTRL_ADDR                                                                         (TCSR_TCSR_REGS_REG_BASE      + 0x00011030)
#define HWIO_TCSR_LDO_DFT_CTRL_PHYS                                                                         (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00011030)
#define HWIO_TCSR_LDO_DFT_CTRL_RMSK                                                                                0x7
#define HWIO_TCSR_LDO_DFT_CTRL_IN          \
        in_dword_masked(HWIO_TCSR_LDO_DFT_CTRL_ADDR, HWIO_TCSR_LDO_DFT_CTRL_RMSK)
#define HWIO_TCSR_LDO_DFT_CTRL_INM(m)      \
        in_dword_masked(HWIO_TCSR_LDO_DFT_CTRL_ADDR, m)
#define HWIO_TCSR_LDO_DFT_CTRL_OUT(v)      \
        out_dword(HWIO_TCSR_LDO_DFT_CTRL_ADDR,v)
#define HWIO_TCSR_LDO_DFT_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_LDO_DFT_CTRL_ADDR,m,v,HWIO_TCSR_LDO_DFT_CTRL_IN)
#define HWIO_TCSR_LDO_DFT_CTRL_LDO_DFT_CONFIG_BMSK                                                                 0x7
#define HWIO_TCSR_LDO_DFT_CTRL_LDO_DFT_CONFIG_SHFT                                                                 0x0

#define HWIO_TCSR_MEM_ACC_SEL_VDDCX_ADDR                                                                    (TCSR_TCSR_REGS_REG_BASE      + 0x0000b100)
#define HWIO_TCSR_MEM_ACC_SEL_VDDCX_PHYS                                                                    (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x0000b100)
#define HWIO_TCSR_MEM_ACC_SEL_VDDCX_RMSK                                                                           0x3
#define HWIO_TCSR_MEM_ACC_SEL_VDDCX_IN          \
        in_dword_masked(HWIO_TCSR_MEM_ACC_SEL_VDDCX_ADDR, HWIO_TCSR_MEM_ACC_SEL_VDDCX_RMSK)
#define HWIO_TCSR_MEM_ACC_SEL_VDDCX_INM(m)      \
        in_dword_masked(HWIO_TCSR_MEM_ACC_SEL_VDDCX_ADDR, m)
#define HWIO_TCSR_MEM_ACC_SEL_VDDCX_OUT(v)      \
        out_dword(HWIO_TCSR_MEM_ACC_SEL_VDDCX_ADDR,v)
#define HWIO_TCSR_MEM_ACC_SEL_VDDCX_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MEM_ACC_SEL_VDDCX_ADDR,m,v,HWIO_TCSR_MEM_ACC_SEL_VDDCX_IN)

#define HWIO_TCSR_MEM_SVS_SEL_VDDCX_ADDR                                                                    (TCSR_TCSR_REGS_REG_BASE      + 0x0000b104)
#define HWIO_TCSR_MEM_SVS_SEL_VDDCX_PHYS                                                                    (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x0000b104)
#define HWIO_TCSR_MEM_SVS_SEL_VDDCX_RMSK                                                                           0x1
#define HWIO_TCSR_MEM_SVS_SEL_VDDCX_IN          \
        in_dword_masked(HWIO_TCSR_MEM_SVS_SEL_VDDCX_ADDR, HWIO_TCSR_MEM_SVS_SEL_VDDCX_RMSK)
#define HWIO_TCSR_MEM_SVS_SEL_VDDCX_INM(m)      \
        in_dword_masked(HWIO_TCSR_MEM_SVS_SEL_VDDCX_ADDR, m)
#define HWIO_TCSR_MEM_SVS_SEL_VDDCX_OUT(v)      \
        out_dword(HWIO_TCSR_MEM_SVS_SEL_VDDCX_ADDR,v)
#define HWIO_TCSR_MEM_SVS_SEL_VDDCX_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MEM_SVS_SEL_VDDCX_ADDR,m,v,HWIO_TCSR_MEM_SVS_SEL_VDDCX_IN)
#define HWIO_TCSR_MEM_SVS_SEL_VDDCX_MEM_SVS_SEL_VDDCX_BMSK                                                         0x1
#define HWIO_TCSR_MEM_SVS_SEL_VDDCX_MEM_SVS_SEL_VDDCX_SHFT                                                         0x0

#define HWIO_TCSR_SPDM_CNT_CLK_CTRL_ADDR                                                                    (TCSR_TCSR_REGS_REG_BASE      + 0x00013000)
#define HWIO_TCSR_SPDM_CNT_CLK_CTRL_PHYS                                                                    (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00013000)
#define HWIO_TCSR_SPDM_CNT_CLK_CTRL_RMSK                                                                        0xffff
#define HWIO_TCSR_SPDM_CNT_CLK_CTRL_IN          \
        in_dword_masked(HWIO_TCSR_SPDM_CNT_CLK_CTRL_ADDR, HWIO_TCSR_SPDM_CNT_CLK_CTRL_RMSK)
#define HWIO_TCSR_SPDM_CNT_CLK_CTRL_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPDM_CNT_CLK_CTRL_ADDR, m)
#define HWIO_TCSR_SPDM_CNT_CLK_CTRL_OUT(v)      \
        out_dword(HWIO_TCSR_SPDM_CNT_CLK_CTRL_ADDR,v)
#define HWIO_TCSR_SPDM_CNT_CLK_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPDM_CNT_CLK_CTRL_ADDR,m,v,HWIO_TCSR_SPDM_CNT_CLK_CTRL_IN)
#define HWIO_TCSR_SPDM_CNT_CLK_CTRL_SPDM_CNT_CLK_MUX_SEL_BMSK                                                   0xffff
#define HWIO_TCSR_SPDM_CNT_CLK_CTRL_SPDM_CNT_CLK_MUX_SEL_SHFT                                                      0x0

#define HWIO_TCSR_SPDM_DLY_FIFO_EN_ADDR                                                                     (TCSR_TCSR_REGS_REG_BASE      + 0x00013004)
#define HWIO_TCSR_SPDM_DLY_FIFO_EN_PHYS                                                                     (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00013004)
#define HWIO_TCSR_SPDM_DLY_FIFO_EN_RMSK                                                                     0xffffffff
#define HWIO_TCSR_SPDM_DLY_FIFO_EN_IN          \
        in_dword_masked(HWIO_TCSR_SPDM_DLY_FIFO_EN_ADDR, HWIO_TCSR_SPDM_DLY_FIFO_EN_RMSK)
#define HWIO_TCSR_SPDM_DLY_FIFO_EN_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPDM_DLY_FIFO_EN_ADDR, m)
#define HWIO_TCSR_SPDM_DLY_FIFO_EN_OUT(v)      \
        out_dword(HWIO_TCSR_SPDM_DLY_FIFO_EN_ADDR,v)
#define HWIO_TCSR_SPDM_DLY_FIFO_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPDM_DLY_FIFO_EN_ADDR,m,v,HWIO_TCSR_SPDM_DLY_FIFO_EN_IN)
#define HWIO_TCSR_SPDM_DLY_FIFO_EN_SPDM_DLY_FIFO_EN_BMSK                                                    0xffffffff
#define HWIO_TCSR_SPDM_DLY_FIFO_EN_SPDM_DLY_FIFO_EN_SHFT                                                           0x0

#define HWIO_TCSR_SPDM_STG1_MUX_SEL_ADDR                                                                    (TCSR_TCSR_REGS_REG_BASE      + 0x00013008)
#define HWIO_TCSR_SPDM_STG1_MUX_SEL_PHYS                                                                    (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00013008)
#define HWIO_TCSR_SPDM_STG1_MUX_SEL_RMSK                                                                        0xffff
#define HWIO_TCSR_SPDM_STG1_MUX_SEL_IN          \
        in_dword_masked(HWIO_TCSR_SPDM_STG1_MUX_SEL_ADDR, HWIO_TCSR_SPDM_STG1_MUX_SEL_RMSK)
#define HWIO_TCSR_SPDM_STG1_MUX_SEL_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPDM_STG1_MUX_SEL_ADDR, m)
#define HWIO_TCSR_SPDM_STG1_MUX_SEL_OUT(v)      \
        out_dword(HWIO_TCSR_SPDM_STG1_MUX_SEL_ADDR,v)
#define HWIO_TCSR_SPDM_STG1_MUX_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPDM_STG1_MUX_SEL_ADDR,m,v,HWIO_TCSR_SPDM_STG1_MUX_SEL_IN)
#define HWIO_TCSR_SPDM_STG1_MUX_SEL_SPDM_STG1_MUX_SEL_BMSK                                                      0xffff
#define HWIO_TCSR_SPDM_STG1_MUX_SEL_SPDM_STG1_MUX_SEL_SHFT                                                         0x0

#define HWIO_TCSR_SPDM_STG2_A_MUX_SEL_ADDR                                                                  (TCSR_TCSR_REGS_REG_BASE      + 0x0001300c)
#define HWIO_TCSR_SPDM_STG2_A_MUX_SEL_PHYS                                                                  (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x0001300c)
#define HWIO_TCSR_SPDM_STG2_A_MUX_SEL_RMSK                                                                  0xffffffff
#define HWIO_TCSR_SPDM_STG2_A_MUX_SEL_IN          \
        in_dword_masked(HWIO_TCSR_SPDM_STG2_A_MUX_SEL_ADDR, HWIO_TCSR_SPDM_STG2_A_MUX_SEL_RMSK)
#define HWIO_TCSR_SPDM_STG2_A_MUX_SEL_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPDM_STG2_A_MUX_SEL_ADDR, m)
#define HWIO_TCSR_SPDM_STG2_A_MUX_SEL_OUT(v)      \
        out_dword(HWIO_TCSR_SPDM_STG2_A_MUX_SEL_ADDR,v)
#define HWIO_TCSR_SPDM_STG2_A_MUX_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPDM_STG2_A_MUX_SEL_ADDR,m,v,HWIO_TCSR_SPDM_STG2_A_MUX_SEL_IN)
#define HWIO_TCSR_SPDM_STG2_A_MUX_SEL_SPDM_STG2_A_MUX_SEL_BMSK                                              0xffffffff
#define HWIO_TCSR_SPDM_STG2_A_MUX_SEL_SPDM_STG2_A_MUX_SEL_SHFT                                                     0x0

#define HWIO_TCSR_SPDM_STG2_B_MUX_SEL_ADDR                                                                  (TCSR_TCSR_REGS_REG_BASE      + 0x00013010)
#define HWIO_TCSR_SPDM_STG2_B_MUX_SEL_PHYS                                                                  (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00013010)
#define HWIO_TCSR_SPDM_STG2_B_MUX_SEL_RMSK                                                                  0xffffffff
#define HWIO_TCSR_SPDM_STG2_B_MUX_SEL_IN          \
        in_dword_masked(HWIO_TCSR_SPDM_STG2_B_MUX_SEL_ADDR, HWIO_TCSR_SPDM_STG2_B_MUX_SEL_RMSK)
#define HWIO_TCSR_SPDM_STG2_B_MUX_SEL_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPDM_STG2_B_MUX_SEL_ADDR, m)
#define HWIO_TCSR_SPDM_STG2_B_MUX_SEL_OUT(v)      \
        out_dword(HWIO_TCSR_SPDM_STG2_B_MUX_SEL_ADDR,v)
#define HWIO_TCSR_SPDM_STG2_B_MUX_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPDM_STG2_B_MUX_SEL_ADDR,m,v,HWIO_TCSR_SPDM_STG2_B_MUX_SEL_IN)
#define HWIO_TCSR_SPDM_STG2_B_MUX_SEL_SPDM_STG2_B_MUX_SEL_BMSK                                              0xffffffff
#define HWIO_TCSR_SPDM_STG2_B_MUX_SEL_SPDM_STG2_B_MUX_SEL_SHFT                                                     0x0

#define HWIO_TCSR_SPDM_STG3_A_MUX_SEL_ADDR                                                                  (TCSR_TCSR_REGS_REG_BASE      + 0x00013014)
#define HWIO_TCSR_SPDM_STG3_A_MUX_SEL_PHYS                                                                  (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00013014)
#define HWIO_TCSR_SPDM_STG3_A_MUX_SEL_RMSK                                                                  0xffffffff
#define HWIO_TCSR_SPDM_STG3_A_MUX_SEL_IN          \
        in_dword_masked(HWIO_TCSR_SPDM_STG3_A_MUX_SEL_ADDR, HWIO_TCSR_SPDM_STG3_A_MUX_SEL_RMSK)
#define HWIO_TCSR_SPDM_STG3_A_MUX_SEL_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPDM_STG3_A_MUX_SEL_ADDR, m)
#define HWIO_TCSR_SPDM_STG3_A_MUX_SEL_OUT(v)      \
        out_dword(HWIO_TCSR_SPDM_STG3_A_MUX_SEL_ADDR,v)
#define HWIO_TCSR_SPDM_STG3_A_MUX_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPDM_STG3_A_MUX_SEL_ADDR,m,v,HWIO_TCSR_SPDM_STG3_A_MUX_SEL_IN)
#define HWIO_TCSR_SPDM_STG3_A_MUX_SEL_SPDM_STG3_A_MUX_SEL_BMSK                                              0xffffffff
#define HWIO_TCSR_SPDM_STG3_A_MUX_SEL_SPDM_STG3_A_MUX_SEL_SHFT                                                     0x0

#define HWIO_TCSR_SPDM_STG3_B_MUX_SEL_ADDR                                                                  (TCSR_TCSR_REGS_REG_BASE      + 0x00013018)
#define HWIO_TCSR_SPDM_STG3_B_MUX_SEL_PHYS                                                                  (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00013018)
#define HWIO_TCSR_SPDM_STG3_B_MUX_SEL_RMSK                                                                  0xffffffff
#define HWIO_TCSR_SPDM_STG3_B_MUX_SEL_IN          \
        in_dword_masked(HWIO_TCSR_SPDM_STG3_B_MUX_SEL_ADDR, HWIO_TCSR_SPDM_STG3_B_MUX_SEL_RMSK)
#define HWIO_TCSR_SPDM_STG3_B_MUX_SEL_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPDM_STG3_B_MUX_SEL_ADDR, m)
#define HWIO_TCSR_SPDM_STG3_B_MUX_SEL_OUT(v)      \
        out_dword(HWIO_TCSR_SPDM_STG3_B_MUX_SEL_ADDR,v)
#define HWIO_TCSR_SPDM_STG3_B_MUX_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPDM_STG3_B_MUX_SEL_ADDR,m,v,HWIO_TCSR_SPDM_STG3_B_MUX_SEL_IN)
#define HWIO_TCSR_SPDM_STG3_B_MUX_SEL_SPDM_STG3_B_MUX_SEL_BMSK                                              0xffffffff
#define HWIO_TCSR_SPDM_STG3_B_MUX_SEL_SPDM_STG3_B_MUX_SEL_SHFT                                                     0x0

#define HWIO_TCSR_PHSS_UART_MSS_INT_SEL_n_ADDR(n)                                                           (TCSR_TCSR_REGS_REG_BASE      + 0x000140c0 + 0x10 * (n))
#define HWIO_TCSR_PHSS_UART_MSS_INT_SEL_n_PHYS(n)                                                           (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x000140c0 + 0x10 * (n))
#define HWIO_TCSR_PHSS_UART_MSS_INT_SEL_n_RMSK                                                                     0xf
#define HWIO_TCSR_PHSS_UART_MSS_INT_SEL_n_MAXn                                                                       1
#define HWIO_TCSR_PHSS_UART_MSS_INT_SEL_n_INI(n)        \
        in_dword_masked(HWIO_TCSR_PHSS_UART_MSS_INT_SEL_n_ADDR(n), HWIO_TCSR_PHSS_UART_MSS_INT_SEL_n_RMSK)
#define HWIO_TCSR_PHSS_UART_MSS_INT_SEL_n_INMI(n,mask)    \
        in_dword_masked(HWIO_TCSR_PHSS_UART_MSS_INT_SEL_n_ADDR(n), mask)
#define HWIO_TCSR_PHSS_UART_MSS_INT_SEL_n_OUTI(n,val)    \
        out_dword(HWIO_TCSR_PHSS_UART_MSS_INT_SEL_n_ADDR(n),val)
#define HWIO_TCSR_PHSS_UART_MSS_INT_SEL_n_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_TCSR_PHSS_UART_MSS_INT_SEL_n_ADDR(n),mask,val,HWIO_TCSR_PHSS_UART_MSS_INT_SEL_n_INI(n))
#define HWIO_TCSR_PHSS_UART_MSS_INT_SEL_n_MSS_BLSP1_UART_4_IRQ_ENABLE_BMSK                                         0x8
#define HWIO_TCSR_PHSS_UART_MSS_INT_SEL_n_MSS_BLSP1_UART_4_IRQ_ENABLE_SHFT                                         0x3
#define HWIO_TCSR_PHSS_UART_MSS_INT_SEL_n_MSS_BLSP1_UART_3_IRQ_ENABLE_BMSK                                         0x4
#define HWIO_TCSR_PHSS_UART_MSS_INT_SEL_n_MSS_BLSP1_UART_3_IRQ_ENABLE_SHFT                                         0x2
#define HWIO_TCSR_PHSS_UART_MSS_INT_SEL_n_MSS_BLSP1_UART_2_IRQ_ENABLE_BMSK                                         0x2
#define HWIO_TCSR_PHSS_UART_MSS_INT_SEL_n_MSS_BLSP1_UART_2_IRQ_ENABLE_SHFT                                         0x1
#define HWIO_TCSR_PHSS_UART_MSS_INT_SEL_n_MSS_BLSP1_UART_1_IRQ_ENABLE_BMSK                                         0x1
#define HWIO_TCSR_PHSS_UART_MSS_INT_SEL_n_MSS_BLSP1_UART_1_IRQ_ENABLE_SHFT                                         0x0

#define HWIO_TCSR_PHSS_QUP_MSS_INT_SEL_n_ADDR(n)                                                            (TCSR_TCSR_REGS_REG_BASE      + 0x00014140 + 0x10 * (n))
#define HWIO_TCSR_PHSS_QUP_MSS_INT_SEL_n_PHYS(n)                                                            (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00014140 + 0x10 * (n))
#define HWIO_TCSR_PHSS_QUP_MSS_INT_SEL_n_RMSK                                                                      0xf
#define HWIO_TCSR_PHSS_QUP_MSS_INT_SEL_n_MAXn                                                                        1
#define HWIO_TCSR_PHSS_QUP_MSS_INT_SEL_n_INI(n)        \
        in_dword_masked(HWIO_TCSR_PHSS_QUP_MSS_INT_SEL_n_ADDR(n), HWIO_TCSR_PHSS_QUP_MSS_INT_SEL_n_RMSK)
#define HWIO_TCSR_PHSS_QUP_MSS_INT_SEL_n_INMI(n,mask)    \
        in_dword_masked(HWIO_TCSR_PHSS_QUP_MSS_INT_SEL_n_ADDR(n), mask)
#define HWIO_TCSR_PHSS_QUP_MSS_INT_SEL_n_OUTI(n,val)    \
        out_dword(HWIO_TCSR_PHSS_QUP_MSS_INT_SEL_n_ADDR(n),val)
#define HWIO_TCSR_PHSS_QUP_MSS_INT_SEL_n_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_TCSR_PHSS_QUP_MSS_INT_SEL_n_ADDR(n),mask,val,HWIO_TCSR_PHSS_QUP_MSS_INT_SEL_n_INI(n))
#define HWIO_TCSR_PHSS_QUP_MSS_INT_SEL_n_MSS_BLSP1_QUP_4_IRQ_ENABLE_BMSK                                           0x8
#define HWIO_TCSR_PHSS_QUP_MSS_INT_SEL_n_MSS_BLSP1_QUP_4_IRQ_ENABLE_SHFT                                           0x3
#define HWIO_TCSR_PHSS_QUP_MSS_INT_SEL_n_MSS_BLSP1_QUP_3_IRQ_ENABLE_BMSK                                           0x4
#define HWIO_TCSR_PHSS_QUP_MSS_INT_SEL_n_MSS_BLSP1_QUP_3_IRQ_ENABLE_SHFT                                           0x2
#define HWIO_TCSR_PHSS_QUP_MSS_INT_SEL_n_MSS_BLSP1_QUP_2_IRQ_ENABLE_BMSK                                           0x2
#define HWIO_TCSR_PHSS_QUP_MSS_INT_SEL_n_MSS_BLSP1_QUP_2_IRQ_ENABLE_SHFT                                           0x1
#define HWIO_TCSR_PHSS_QUP_MSS_INT_SEL_n_MSS_BLSP1_QUP_1_IRQ_ENABLE_BMSK                                           0x1
#define HWIO_TCSR_PHSS_QUP_MSS_INT_SEL_n_MSS_BLSP1_QUP_1_IRQ_ENABLE_SHFT                                           0x0

#define HWIO_TCSR_COMPILER_VDDMSS_ACC_0_ADDR                                                                (TCSR_TCSR_REGS_REG_BASE      + 0x0000b000)
#define HWIO_TCSR_COMPILER_VDDMSS_ACC_0_PHYS                                                                (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x0000b000)
#define HWIO_TCSR_COMPILER_VDDMSS_ACC_0_RMSK                                                                0xffffffff
#define HWIO_TCSR_COMPILER_VDDMSS_ACC_0_IN          \
        in_dword_masked(HWIO_TCSR_COMPILER_VDDMSS_ACC_0_ADDR, HWIO_TCSR_COMPILER_VDDMSS_ACC_0_RMSK)
#define HWIO_TCSR_COMPILER_VDDMSS_ACC_0_INM(m)      \
        in_dword_masked(HWIO_TCSR_COMPILER_VDDMSS_ACC_0_ADDR, m)
#define HWIO_TCSR_COMPILER_VDDMSS_ACC_0_COMPILER_VDDMSS_ACC_0_BMSK                                          0xffffffff
#define HWIO_TCSR_COMPILER_VDDMSS_ACC_0_COMPILER_VDDMSS_ACC_0_SHFT                                                 0x0

#define HWIO_TCSR_CUSTOM_ACC_4P_TYP1_VDDMSS_ADDR                                                            (TCSR_TCSR_REGS_REG_BASE      + 0x0000b010)
#define HWIO_TCSR_CUSTOM_ACC_4P_TYP1_VDDMSS_PHYS                                                            (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x0000b010)
#define HWIO_TCSR_CUSTOM_ACC_4P_TYP1_VDDMSS_RMSK                                                                  0xff
#define HWIO_TCSR_CUSTOM_ACC_4P_TYP1_VDDMSS_IN          \
        in_dword_masked(HWIO_TCSR_CUSTOM_ACC_4P_TYP1_VDDMSS_ADDR, HWIO_TCSR_CUSTOM_ACC_4P_TYP1_VDDMSS_RMSK)
#define HWIO_TCSR_CUSTOM_ACC_4P_TYP1_VDDMSS_INM(m)      \
        in_dword_masked(HWIO_TCSR_CUSTOM_ACC_4P_TYP1_VDDMSS_ADDR, m)
#define HWIO_TCSR_CUSTOM_ACC_4P_TYP1_VDDMSS_CUSTOM_ACC_4P_TYP1_VDDMSS_BMSK                                        0xff
#define HWIO_TCSR_CUSTOM_ACC_4P_TYP1_VDDMSS_CUSTOM_ACC_4P_TYP1_VDDMSS_SHFT                                         0x0

#define HWIO_TCSR_CUSTOM_ACC_STDSP_0890_TYP2_VDDMSS_ADDR                                                    (TCSR_TCSR_REGS_REG_BASE      + 0x0000b014)
#define HWIO_TCSR_CUSTOM_ACC_STDSP_0890_TYP2_VDDMSS_PHYS                                                    (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x0000b014)
#define HWIO_TCSR_CUSTOM_ACC_STDSP_0890_TYP2_VDDMSS_RMSK                                                          0xff
#define HWIO_TCSR_CUSTOM_ACC_STDSP_0890_TYP2_VDDMSS_IN          \
        in_dword_masked(HWIO_TCSR_CUSTOM_ACC_STDSP_0890_TYP2_VDDMSS_ADDR, HWIO_TCSR_CUSTOM_ACC_STDSP_0890_TYP2_VDDMSS_RMSK)
#define HWIO_TCSR_CUSTOM_ACC_STDSP_0890_TYP2_VDDMSS_INM(m)      \
        in_dword_masked(HWIO_TCSR_CUSTOM_ACC_STDSP_0890_TYP2_VDDMSS_ADDR, m)
#define HWIO_TCSR_CUSTOM_ACC_STDSP_0890_TYP2_VDDMSS_CUSTOM_ACC_STDSP_0890_TYP2_VDDMSS_BMSK                        0xff
#define HWIO_TCSR_CUSTOM_ACC_STDSP_0890_TYP2_VDDMSS_CUSTOM_ACC_STDSP_0890_TYP2_VDDMSS_SHFT                         0x0

#define HWIO_TCSR_CUSTOM_ACC_LLSP_0810_TYP3_VDDMSS_ADDR                                                     (TCSR_TCSR_REGS_REG_BASE      + 0x0000b018)
#define HWIO_TCSR_CUSTOM_ACC_LLSP_0810_TYP3_VDDMSS_PHYS                                                     (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x0000b018)
#define HWIO_TCSR_CUSTOM_ACC_LLSP_0810_TYP3_VDDMSS_RMSK                                                           0xff
#define HWIO_TCSR_CUSTOM_ACC_LLSP_0810_TYP3_VDDMSS_IN          \
        in_dword_masked(HWIO_TCSR_CUSTOM_ACC_LLSP_0810_TYP3_VDDMSS_ADDR, HWIO_TCSR_CUSTOM_ACC_LLSP_0810_TYP3_VDDMSS_RMSK)
#define HWIO_TCSR_CUSTOM_ACC_LLSP_0810_TYP3_VDDMSS_INM(m)      \
        in_dword_masked(HWIO_TCSR_CUSTOM_ACC_LLSP_0810_TYP3_VDDMSS_ADDR, m)
#define HWIO_TCSR_CUSTOM_ACC_LLSP_0810_TYP3_VDDMSS_CUSTOM_ACC_LLSP_0810_TYP3_VDDMSS_BMSK                          0xff
#define HWIO_TCSR_CUSTOM_ACC_LLSP_0810_TYP3_VDDMSS_CUSTOM_ACC_LLSP_0810_TYP3_VDDMSS_SHFT                           0x0

#define HWIO_TCSR_CUSTOM_ACC_LLSP_0810_BOLT_TYP4_VDDMSS_ADDR                                                (TCSR_TCSR_REGS_REG_BASE      + 0x0000b01c)
#define HWIO_TCSR_CUSTOM_ACC_LLSP_0810_BOLT_TYP4_VDDMSS_PHYS                                                (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x0000b01c)
#define HWIO_TCSR_CUSTOM_ACC_LLSP_0810_BOLT_TYP4_VDDMSS_RMSK                                                      0xff
#define HWIO_TCSR_CUSTOM_ACC_LLSP_0810_BOLT_TYP4_VDDMSS_IN          \
        in_dword_masked(HWIO_TCSR_CUSTOM_ACC_LLSP_0810_BOLT_TYP4_VDDMSS_ADDR, HWIO_TCSR_CUSTOM_ACC_LLSP_0810_BOLT_TYP4_VDDMSS_RMSK)
#define HWIO_TCSR_CUSTOM_ACC_LLSP_0810_BOLT_TYP4_VDDMSS_INM(m)      \
        in_dword_masked(HWIO_TCSR_CUSTOM_ACC_LLSP_0810_BOLT_TYP4_VDDMSS_ADDR, m)
#define HWIO_TCSR_CUSTOM_ACC_LLSP_0810_BOLT_TYP4_VDDMSS_CUSTOM_ACC_LLSP_0810_BOLT_TYP4_VDDMSS_BMSK                0xff
#define HWIO_TCSR_CUSTOM_ACC_LLSP_0810_BOLT_TYP4_VDDMSS_CUSTOM_ACC_LLSP_0810_BOLT_TYP4_VDDMSS_SHFT                 0x0

#define HWIO_TCSR_COMPILER_VDDCX_ACC_0_ADDR                                                                 (TCSR_TCSR_REGS_REG_BASE      + 0x0000b080)
#define HWIO_TCSR_COMPILER_VDDCX_ACC_0_PHYS                                                                 (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x0000b080)
#define HWIO_TCSR_COMPILER_VDDCX_ACC_0_RMSK                                                                 0xffffffff
#define HWIO_TCSR_COMPILER_VDDCX_ACC_0_IN          \
        in_dword_masked(HWIO_TCSR_COMPILER_VDDCX_ACC_0_ADDR, HWIO_TCSR_COMPILER_VDDCX_ACC_0_RMSK)
#define HWIO_TCSR_COMPILER_VDDCX_ACC_0_INM(m)      \
        in_dword_masked(HWIO_TCSR_COMPILER_VDDCX_ACC_0_ADDR, m)
#define HWIO_TCSR_COMPILER_VDDCX_ACC_0_COMPILER_VDDCX_ACC_0_BMSK                                            0xffffffff
#define HWIO_TCSR_COMPILER_VDDCX_ACC_0_COMPILER_VDDCX_ACC_0_SHFT                                                   0x0

#define HWIO_TCSR_CUSTOM_ACC_4P_TYP1_VDDCX_ADDR                                                             (TCSR_TCSR_REGS_REG_BASE      + 0x0000b090)
#define HWIO_TCSR_CUSTOM_ACC_4P_TYP1_VDDCX_PHYS                                                             (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x0000b090)
#define HWIO_TCSR_CUSTOM_ACC_4P_TYP1_VDDCX_RMSK                                                                   0xff
#define HWIO_TCSR_CUSTOM_ACC_4P_TYP1_VDDCX_IN          \
        in_dword_masked(HWIO_TCSR_CUSTOM_ACC_4P_TYP1_VDDCX_ADDR, HWIO_TCSR_CUSTOM_ACC_4P_TYP1_VDDCX_RMSK)
#define HWIO_TCSR_CUSTOM_ACC_4P_TYP1_VDDCX_INM(m)      \
        in_dword_masked(HWIO_TCSR_CUSTOM_ACC_4P_TYP1_VDDCX_ADDR, m)
#define HWIO_TCSR_CUSTOM_ACC_4P_TYP1_VDDCX_CUSTOM_ACC_4P_TYP1_VDDCX_BMSK                                          0xff
#define HWIO_TCSR_CUSTOM_ACC_4P_TYP1_VDDCX_CUSTOM_ACC_4P_TYP1_VDDCX_SHFT                                           0x0

#define HWIO_TCSR_CUSTOM_ACC_STDSP_0890_TYP2_VDDCX_ADDR                                                     (TCSR_TCSR_REGS_REG_BASE      + 0x0000b094)
#define HWIO_TCSR_CUSTOM_ACC_STDSP_0890_TYP2_VDDCX_PHYS                                                     (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x0000b094)
#define HWIO_TCSR_CUSTOM_ACC_STDSP_0890_TYP2_VDDCX_RMSK                                                           0xff
#define HWIO_TCSR_CUSTOM_ACC_STDSP_0890_TYP2_VDDCX_IN          \
        in_dword_masked(HWIO_TCSR_CUSTOM_ACC_STDSP_0890_TYP2_VDDCX_ADDR, HWIO_TCSR_CUSTOM_ACC_STDSP_0890_TYP2_VDDCX_RMSK)
#define HWIO_TCSR_CUSTOM_ACC_STDSP_0890_TYP2_VDDCX_INM(m)      \
        in_dword_masked(HWIO_TCSR_CUSTOM_ACC_STDSP_0890_TYP2_VDDCX_ADDR, m)
#define HWIO_TCSR_CUSTOM_ACC_STDSP_0890_TYP2_VDDCX_CUSTOM_ACC_STDSP_0890_TYP2_VDDCX_BMSK                          0xff
#define HWIO_TCSR_CUSTOM_ACC_STDSP_0890_TYP2_VDDCX_CUSTOM_ACC_STDSP_0890_TYP2_VDDCX_SHFT                           0x0

#define HWIO_TCSR_CUSTOM_ACC_LLSP_0810_TYP3_VDDCX_ADDR                                                      (TCSR_TCSR_REGS_REG_BASE      + 0x0000b098)
#define HWIO_TCSR_CUSTOM_ACC_LLSP_0810_TYP3_VDDCX_PHYS                                                      (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x0000b098)
#define HWIO_TCSR_CUSTOM_ACC_LLSP_0810_TYP3_VDDCX_RMSK                                                            0xff
#define HWIO_TCSR_CUSTOM_ACC_LLSP_0810_TYP3_VDDCX_IN          \
        in_dword_masked(HWIO_TCSR_CUSTOM_ACC_LLSP_0810_TYP3_VDDCX_ADDR, HWIO_TCSR_CUSTOM_ACC_LLSP_0810_TYP3_VDDCX_RMSK)
#define HWIO_TCSR_CUSTOM_ACC_LLSP_0810_TYP3_VDDCX_INM(m)      \
        in_dword_masked(HWIO_TCSR_CUSTOM_ACC_LLSP_0810_TYP3_VDDCX_ADDR, m)
#define HWIO_TCSR_CUSTOM_ACC_LLSP_0810_TYP3_VDDCX_CUSTOM_ACC_LLSP_0810_TYP3_VDDCX_BMSK                            0xff
#define HWIO_TCSR_CUSTOM_ACC_LLSP_0810_TYP3_VDDCX_CUSTOM_ACC_LLSP_0810_TYP3_VDDCX_SHFT                             0x0

#define HWIO_TCSR_CUSTOM_ACC_LLSP_0810_BOLT_TYP4_VDDCX_ADDR                                                 (TCSR_TCSR_REGS_REG_BASE      + 0x0000b09c)
#define HWIO_TCSR_CUSTOM_ACC_LLSP_0810_BOLT_TYP4_VDDCX_PHYS                                                 (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x0000b09c)
#define HWIO_TCSR_CUSTOM_ACC_LLSP_0810_BOLT_TYP4_VDDCX_RMSK                                                       0xff
#define HWIO_TCSR_CUSTOM_ACC_LLSP_0810_BOLT_TYP4_VDDCX_IN          \
        in_dword_masked(HWIO_TCSR_CUSTOM_ACC_LLSP_0810_BOLT_TYP4_VDDCX_ADDR, HWIO_TCSR_CUSTOM_ACC_LLSP_0810_BOLT_TYP4_VDDCX_RMSK)
#define HWIO_TCSR_CUSTOM_ACC_LLSP_0810_BOLT_TYP4_VDDCX_INM(m)      \
        in_dword_masked(HWIO_TCSR_CUSTOM_ACC_LLSP_0810_BOLT_TYP4_VDDCX_ADDR, m)
#define HWIO_TCSR_CUSTOM_ACC_LLSP_0810_BOLT_TYP4_VDDCX_CUSTOM_ACC_LLSP_0810_BOLT_TYP4_VDDCX_BMSK                  0xff
#define HWIO_TCSR_CUSTOM_ACC_LLSP_0810_BOLT_TYP4_VDDCX_CUSTOM_ACC_LLSP_0810_BOLT_TYP4_VDDCX_SHFT                   0x0

#define HWIO_TCSR_COMPILER_Q6_ACC_0_ADDR                                                                    (TCSR_TCSR_REGS_REG_BASE      + 0x0000b0b8)
#define HWIO_TCSR_COMPILER_Q6_ACC_0_PHYS                                                                    (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x0000b0b8)
#define HWIO_TCSR_COMPILER_Q6_ACC_0_RMSK                                                                    0xffffffff
#define HWIO_TCSR_COMPILER_Q6_ACC_0_IN          \
        in_dword_masked(HWIO_TCSR_COMPILER_Q6_ACC_0_ADDR, HWIO_TCSR_COMPILER_Q6_ACC_0_RMSK)
#define HWIO_TCSR_COMPILER_Q6_ACC_0_INM(m)      \
        in_dword_masked(HWIO_TCSR_COMPILER_Q6_ACC_0_ADDR, m)
#define HWIO_TCSR_COMPILER_Q6_ACC_0_COMPILER_Q6_ACC_0_BMSK                                                  0xffffffff
#define HWIO_TCSR_COMPILER_Q6_ACC_0_COMPILER_Q6_ACC_0_SHFT                                                         0x0

#define HWIO_TCSR_MEM_ARRY_STBY_ADDR                                                                        (TCSR_TCSR_REGS_REG_BASE      + 0x00015180)
#define HWIO_TCSR_MEM_ARRY_STBY_PHYS                                                                        (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00015180)
#define HWIO_TCSR_MEM_ARRY_STBY_RMSK                                                                               0x1
#define HWIO_TCSR_MEM_ARRY_STBY_IN          \
        in_dword_masked(HWIO_TCSR_MEM_ARRY_STBY_ADDR, HWIO_TCSR_MEM_ARRY_STBY_RMSK)
#define HWIO_TCSR_MEM_ARRY_STBY_INM(m)      \
        in_dword_masked(HWIO_TCSR_MEM_ARRY_STBY_ADDR, m)
#define HWIO_TCSR_MEM_ARRY_STBY_OUT(v)      \
        out_dword(HWIO_TCSR_MEM_ARRY_STBY_ADDR,v)
#define HWIO_TCSR_MEM_ARRY_STBY_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MEM_ARRY_STBY_ADDR,m,v,HWIO_TCSR_MEM_ARRY_STBY_IN)
#define HWIO_TCSR_MEM_ARRY_STBY_MEM_ARRY_STBY_N_BMSK                                                               0x1
#define HWIO_TCSR_MEM_ARRY_STBY_MEM_ARRY_STBY_N_SHFT                                                               0x0

#define HWIO_TCSR_SOC_HW_VERSION_ADDR                                                                       (TCSR_TCSR_REGS_REG_BASE      + 0x00016000)
#define HWIO_TCSR_SOC_HW_VERSION_PHYS                                                                       (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00016000)
#define HWIO_TCSR_SOC_HW_VERSION_RMSK                                                                       0xffffffff
#define HWIO_TCSR_SOC_HW_VERSION_IN          \
        in_dword_masked(HWIO_TCSR_SOC_HW_VERSION_ADDR, HWIO_TCSR_SOC_HW_VERSION_RMSK)
#define HWIO_TCSR_SOC_HW_VERSION_INM(m)      \
        in_dword_masked(HWIO_TCSR_SOC_HW_VERSION_ADDR, m)
#define HWIO_TCSR_SOC_HW_VERSION_FAMILY_NUMBER_BMSK                                                         0xf0000000
#define HWIO_TCSR_SOC_HW_VERSION_FAMILY_NUMBER_SHFT                                                               0x1c
#define HWIO_TCSR_SOC_HW_VERSION_DEVICE_NUMBER_BMSK                                                          0xfff0000
#define HWIO_TCSR_SOC_HW_VERSION_DEVICE_NUMBER_SHFT                                                               0x10
#define HWIO_TCSR_SOC_HW_VERSION_MAJOR_VERSION_BMSK                                                             0xff00
#define HWIO_TCSR_SOC_HW_VERSION_MAJOR_VERSION_SHFT                                                                0x8
#define HWIO_TCSR_SOC_HW_VERSION_MINOR_VERSION_BMSK                                                               0xff
#define HWIO_TCSR_SOC_HW_VERSION_MINOR_VERSION_SHFT                                                                0x0

#define HWIO_TCSR_TIMEOUT_INTR_STATUS_ADDR                                                                  (TCSR_TCSR_REGS_REG_BASE      + 0x00016020)
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_PHYS                                                                  (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00016020)
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_RMSK                                                                    0x5063ff
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_IN          \
        in_dword_masked(HWIO_TCSR_TIMEOUT_INTR_STATUS_ADDR, HWIO_TCSR_TIMEOUT_INTR_STATUS_RMSK)
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_INM(m)      \
        in_dword_masked(HWIO_TCSR_TIMEOUT_INTR_STATUS_ADDR, m)
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_ULTAUDIO_AHBI_TIMEOUT_IRQ_BMSK                                          0x400000
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_ULTAUDIO_AHBI_TIMEOUT_IRQ_SHFT                                              0x16
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_MSS_CONFIG_TIMEOUT_IRQ_BMSK                                             0x100000
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_MSS_CONFIG_TIMEOUT_IRQ_SHFT                                                 0x14
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_SNOC_S1_TIMEOUT_IRQ_BMSK                                                  0x4000
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_SNOC_S1_TIMEOUT_IRQ_SHFT                                                     0xe
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_SNOC_S0_TIMEOUT_IRQ_BMSK                                                  0x2000
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_SNOC_S0_TIMEOUT_IRQ_SHFT                                                     0xd
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_PCNOC_S9_TIMEOUT_IRQ_BMSK                                                  0x200
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_PCNOC_S9_TIMEOUT_IRQ_SHFT                                                    0x9
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_PCNOC_S8_TIMEOUT_IRQ_BMSK                                                  0x100
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_PCNOC_S8_TIMEOUT_IRQ_SHFT                                                    0x8
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_PCNOC_S7_TIMEOUT_IRQ_BMSK                                                   0x80
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_PCNOC_S7_TIMEOUT_IRQ_SHFT                                                    0x7
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_PCNOC_S6_TIMEOUT_IRQ_BMSK                                                   0x40
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_PCNOC_S6_TIMEOUT_IRQ_SHFT                                                    0x6
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_PCNOC_S5_TIMEOUT_IRQ_BMSK                                                   0x20
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_PCNOC_S5_TIMEOUT_IRQ_SHFT                                                    0x5
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_PCNOC_S4_TIMEOUT_IRQ_BMSK                                                   0x10
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_PCNOC_S4_TIMEOUT_IRQ_SHFT                                                    0x4
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_PCNOC_S3_TIMEOUT_IRQ_BMSK                                                    0x8
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_PCNOC_S3_TIMEOUT_IRQ_SHFT                                                    0x3
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_PCNOC_S2_TIMEOUT_IRQ_BMSK                                                    0x4
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_PCNOC_S2_TIMEOUT_IRQ_SHFT                                                    0x2
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_PCNOC_S1_TIMEOUT_IRQ_BMSK                                                    0x2
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_PCNOC_S1_TIMEOUT_IRQ_SHFT                                                    0x1
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_PCNOC_S0_TIMEOUT_IRQ_BMSK                                                    0x1
#define HWIO_TCSR_TIMEOUT_INTR_STATUS_PCNOC_S0_TIMEOUT_IRQ_SHFT                                                    0x0

#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_ADDR                                                              (TCSR_TCSR_REGS_REG_BASE      + 0x00016030)
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_PHYS                                                              (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00016030)
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_RMSK                                                                0x5063ff
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_IN          \
        in_dword_masked(HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_ADDR, HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_RMSK)
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_INM(m)      \
        in_dword_masked(HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_ADDR, m)
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_OUT(v)      \
        out_dword(HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_ADDR,v)
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_ADDR,m,v,HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_IN)
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_ULTAUDIO_AHBI_TIMEOUT_IRQ_RPM_ENABLE_BMSK                           0x400000
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_ULTAUDIO_AHBI_TIMEOUT_IRQ_RPM_ENABLE_SHFT                               0x16
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_MSS_CONFIG_TIMEOUT_IRQ_RPM_ENABLE_BMSK                              0x100000
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_MSS_CONFIG_TIMEOUT_IRQ_RPM_ENABLE_SHFT                                  0x14
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_SNOC_S1_TIMEOUT_IRQ_RPM_ENABLE_BMSK                                   0x4000
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_SNOC_S1_TIMEOUT_IRQ_RPM_ENABLE_SHFT                                      0xe
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_SNOC_S0_TIMEOUT_IRQ_RPM_ENABLE_BMSK                                   0x2000
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_SNOC_S0_TIMEOUT_IRQ_RPM_ENABLE_SHFT                                      0xd
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_PCNOC_S9_TIMEOUT_IRQ_RPM_ENABLE_BMSK                                   0x200
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_PCNOC_S9_TIMEOUT_IRQ_RPM_ENABLE_SHFT                                     0x9
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_PCNOC_S8_TIMEOUT_IRQ_RPM_ENABLE_BMSK                                   0x100
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_PCNOC_S8_TIMEOUT_IRQ_RPM_ENABLE_SHFT                                     0x8
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_PCNOC_S7_TIMEOUT_IRQ_RPM_ENABLE_BMSK                                    0x80
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_PCNOC_S7_TIMEOUT_IRQ_RPM_ENABLE_SHFT                                     0x7
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_PCNOC_S6_TIMEOUT_IRQ_RPM_ENABLE_BMSK                                    0x40
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_PCNOC_S6_TIMEOUT_IRQ_RPM_ENABLE_SHFT                                     0x6
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_PCNOC_S5_TIMEOUT_IRQ_RPM_ENABLE_BMSK                                    0x20
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_PCNOC_S5_TIMEOUT_IRQ_RPM_ENABLE_SHFT                                     0x5
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_PCNOC_S4_TIMEOUT_IRQ_RPM_ENABLE_BMSK                                    0x10
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_PCNOC_S4_TIMEOUT_IRQ_RPM_ENABLE_SHFT                                     0x4
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_PCNOC_S3_TIMEOUT_IRQ_RPM_ENABLE_BMSK                                     0x8
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_PCNOC_S3_TIMEOUT_IRQ_RPM_ENABLE_SHFT                                     0x3
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_PCNOC_S2_TIMEOUT_IRQ_RPM_ENABLE_BMSK                                     0x4
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_PCNOC_S2_TIMEOUT_IRQ_RPM_ENABLE_SHFT                                     0x2
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_PCNOC_S1_TIMEOUT_IRQ_RPM_ENABLE_BMSK                                     0x2
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_PCNOC_S1_TIMEOUT_IRQ_RPM_ENABLE_SHFT                                     0x1
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_PCNOC_S0_TIMEOUT_IRQ_RPM_ENABLE_BMSK                                     0x1
#define HWIO_TCSR_TIMEOUT_INTR_RPM_ENABLE_PCNOC_S0_TIMEOUT_IRQ_RPM_ENABLE_SHFT                                     0x0

#define HWIO_TCSR_TIMEOUT_INTR_APSS_ENABLE_ADDR                                                             (TCSR_TCSR_REGS_REG_BASE      + 0x00016040)
#define HWIO_TCSR_TIMEOUT_INTR_APSS_ENABLE_PHYS                                                             (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00016040)
#define HWIO_TCSR_TIMEOUT_INTR_APSS_ENABLE_RMSK                                                               0x5063ff
#define HWIO_TCSR_TIMEOUT_INTR_APSS_ENABLE_IN          \
        in_dword_masked(HWIO_TCSR_TIMEOUT_INTR_APSS_ENABLE_ADDR, HWIO_TCSR_TIMEOUT_INTR_APSS_ENABLE_RMSK)
#define HWIO_TCSR_TIMEOUT_INTR_APSS_ENABLE_INM(m)      \
        in_dword_masked(HWIO_TCSR_TIMEOUT_INTR_APSS_ENABLE_ADDR, m)
#define HWIO_TCSR_TIMEOUT_INTR_APSS_ENABLE_OUT(v)      \
        out_dword(HWIO_TCSR_TIMEOUT_INTR_APSS_ENABLE_ADDR,v)
#define HWIO_TCSR_TIMEOUT_INTR_APSS_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_TIMEOUT_INTR_APSS_ENABLE_ADDR,m,v,HWIO_TCSR_TIMEOUT_INTR_APSS_ENABLE_IN)
#define HWIO_TCSR_TIMEOUT_INTR_APSS_ENABLE_ULTAUDIO_AHBI_TIMEOUT_IRQ_APSS_ENABLE_BMSK                         0x400000
#define HWIO_TCSR_TIMEOUT_INTR_APSS_ENABLE_ULTAUDIO_AHBI_TIMEOUT_IRQ_APSS_ENABLE_SHFT                             0x16
#define HWIO_TCSR_TIMEOUT_INTR_APSS_ENABLE_MSS_CONFIG_TIMEOUT_IRQ_APSS_ENABLE_BMSK                            0x100000
#define HWIO_TCSR_TIMEOUT_INTR_APSS_ENABLE_MSS_CONFIG_TIMEOUT_IRQ_APSS_ENABLE_SHFT                                0x14
#define HWIO_TCSR_TIMEOUT_INTR_APSS_ENABLE_SNOC_S1_TIMEOUT_IRQ_APSS_ENABLE_BMSK                                 0x4000
#define HWIO_TCSR_TIMEOUT_INTR_APSS_ENABLE_SNOC_S1_TIMEOUT_IRQ_APSS_ENABLE_SHFT                                    0xe
#define HWIO_TCSR_TIMEOUT_INTR_APSS_ENABLE_SNOC_S0_TIMEOUT_IRQ_APSS_ENABLE_BMSK                                 0x2000
#define HWIO_TCSR_TIMEOUT_INTR_APSS_ENABLE_SNOC_S0_TIMEOUT_IRQ_APSS_ENABLE_SHFT                                    0xd
#define HWIO_TCSR_TIMEOUT_INTR_APSS_ENABLE_PCNOC_S9_TIMEOUT_IRQ_APSS_ENABLE_BMSK                                 0x200
#define HWIO_TCSR_TIMEOUT_INTR_APSS_ENABLE_PCNOC_S9_TIMEOUT_IRQ_APSS_ENABLE_SHFT                                   0x9
#define HWIO_TCSR_TIMEOUT_INTR_APSS_ENABLE_PCNOC_S8_TIMEOUT_IRQ_APSS_ENABLE_BMSK                                 0x100
#define HWIO_TCSR_TIMEOUT_INTR_APSS_ENABLE_PCNOC_S8_TIMEOUT_IRQ_APSS_ENABLE_SHFT                                   0x8
#define HWIO_TCSR_TIMEOUT_INTR_APSS_ENABLE_PCNOC_S7_TIMEOUT_IRQ_APSS_ENABLE_BMSK                                  0x80
#define HWIO_TCSR_TIMEOUT_INTR_APSS_ENABLE_PCNOC_S7_TIMEOUT_IRQ_APSS_ENABLE_SHFT                                   0x7
#define HWIO_TCSR_TIMEOUT_INTR_APSS_ENABLE_PCNOC_S6_TIMEOUT_IRQ_APSS_ENABLE_BMSK                                  0x40
#define HWIO_TCSR_TIMEOUT_INTR_APSS_ENABLE_PCNOC_S6_TIMEOUT_IRQ_APSS_ENABLE_SHFT                                   0x6
#define HWIO_TCSR_TIMEOUT_INTR_APSS_ENABLE_PCNOC_S5_TIMEOUT_IRQ_APSS_ENABLE_BMSK                                  0x20
#define HWIO_TCSR_TIMEOUT_INTR_APSS_ENABLE_PCNOC_S5_TIMEOUT_IRQ_APSS_ENABLE_SHFT                                   0x5
#define HWIO_TCSR_TIMEOUT_INTR_APSS_ENABLE_PCNOC_S4_TIMEOUT_IRQ_APSS_ENABLE_BMSK                                  0x10
#define HWIO_TCSR_TIMEOUT_INTR_APSS_ENABLE_PCNOC_S4_TIMEOUT_IRQ_APSS_ENABLE_SHFT                                   0x4
#define HWIO_TCSR_TIMEOUT_INTR_APSS_ENABLE_PCNOC_S3_TIMEOUT_IRQ_APSS_ENABLE_BMSK                                   0x8
#define HWIO_TCSR_TIMEOUT_INTR_APSS_ENABLE_PCNOC_S3_TIMEOUT_IRQ_APSS_ENABLE_SHFT                                   0x3
#define HWIO_TCSR_TIMEOUT_INTR_APSS_ENABLE_PCNOC_S2_TIMEOUT_IRQ_APSS_ENABLE_BMSK                                   0x4
#define HWIO_TCSR_TIMEOUT_INTR_APSS_ENABLE_PCNOC_S2_TIMEOUT_IRQ_APSS_ENABLE_SHFT                                   0x2
#define HWIO_TCSR_TIMEOUT_INTR_APSS_ENABLE_PCNOC_S1_TIMEOUT_IRQ_APSS_ENABLE_BMSK                                   0x2
#define HWIO_TCSR_TIMEOUT_INTR_APSS_ENABLE_PCNOC_S1_TIMEOUT_IRQ_APSS_ENABLE_SHFT                                   0x1
#define HWIO_TCSR_TIMEOUT_INTR_APSS_ENABLE_PCNOC_S0_TIMEOUT_IRQ_APSS_ENABLE_BMSK                                   0x1
#define HWIO_TCSR_TIMEOUT_INTR_APSS_ENABLE_PCNOC_S0_TIMEOUT_IRQ_APSS_ENABLE_SHFT                                   0x0

#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_ADDR                                                              (TCSR_TCSR_REGS_REG_BASE      + 0x00016060)
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_PHYS                                                              (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00016060)
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_RMSK                                                                0x5063ff
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_IN          \
        in_dword_masked(HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_ADDR, HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_RMSK)
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_INM(m)      \
        in_dword_masked(HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_ADDR, m)
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_OUT(v)      \
        out_dword(HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_ADDR,v)
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_ADDR,m,v,HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_IN)
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_ULTAUDIO_AHBI_TIMEOUT_IRQ_MSS_ENABLE_BMSK                           0x400000
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_ULTAUDIO_AHBI_TIMEOUT_IRQ_MSS_ENABLE_SHFT                               0x16
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_MSS_CONFIG_TIMEOUT_IRQ_MSS_ENABLE_BMSK                              0x100000
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_MSS_CONFIG_TIMEOUT_IRQ_MSS_ENABLE_SHFT                                  0x14
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_SNOC_S1_TIMEOUT_IRQ_MSS_ENABLE_BMSK                                   0x4000
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_SNOC_S1_TIMEOUT_IRQ_MSS_ENABLE_SHFT                                      0xe
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_SNOC_S0_TIMEOUT_IRQ_MSS_ENABLE_BMSK                                   0x2000
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_SNOC_S0_TIMEOUT_IRQ_MSS_ENABLE_SHFT                                      0xd
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_PCNOC_S9_TIMEOUT_IRQ_MSS_ENABLE_BMSK                                   0x200
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_PCNOC_S9_TIMEOUT_IRQ_MSS_ENABLE_SHFT                                     0x9
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_PCNOC_S8_TIMEOUT_IRQ_MSS_ENABLE_BMSK                                   0x100
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_PCNOC_S8_TIMEOUT_IRQ_MSS_ENABLE_SHFT                                     0x8
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_PCNOC_S7_TIMEOUT_IRQ_MSS_ENABLE_BMSK                                    0x80
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_PCNOC_S7_TIMEOUT_IRQ_MSS_ENABLE_SHFT                                     0x7
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_PCNOC_S6_TIMEOUT_IRQ_MSS_ENABLE_BMSK                                    0x40
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_PCNOC_S6_TIMEOUT_IRQ_MSS_ENABLE_SHFT                                     0x6
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_PCNOC_S5_TIMEOUT_IRQ_MSS_ENABLE_BMSK                                    0x20
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_PCNOC_S5_TIMEOUT_IRQ_MSS_ENABLE_SHFT                                     0x5
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_PCNOC_S4_TIMEOUT_IRQ_MSS_ENABLE_BMSK                                    0x10
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_PCNOC_S4_TIMEOUT_IRQ_MSS_ENABLE_SHFT                                     0x4
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_PCNOC_S3_TIMEOUT_IRQ_MSS_ENABLE_BMSK                                     0x8
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_PCNOC_S3_TIMEOUT_IRQ_MSS_ENABLE_SHFT                                     0x3
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_PCNOC_S2_TIMEOUT_IRQ_MSS_ENABLE_BMSK                                     0x4
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_PCNOC_S2_TIMEOUT_IRQ_MSS_ENABLE_SHFT                                     0x2
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_PCNOC_S1_TIMEOUT_IRQ_MSS_ENABLE_BMSK                                     0x2
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_PCNOC_S1_TIMEOUT_IRQ_MSS_ENABLE_SHFT                                     0x1
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_PCNOC_S0_TIMEOUT_IRQ_MSS_ENABLE_BMSK                                     0x1
#define HWIO_TCSR_TIMEOUT_INTR_MSS_ENABLE_PCNOC_S0_TIMEOUT_IRQ_MSS_ENABLE_SHFT                                     0x0

#define HWIO_TCSR_PRONTO_BUS_TIMEOUT_RESET_ADDR                                                             (TCSR_TCSR_REGS_REG_BASE      + 0x00016070)
#define HWIO_TCSR_PRONTO_BUS_TIMEOUT_RESET_PHYS                                                             (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00016070)
#define HWIO_TCSR_PRONTO_BUS_TIMEOUT_RESET_RMSK                                                                    0x1
#define HWIO_TCSR_PRONTO_BUS_TIMEOUT_RESET_IN          \
        in_dword_masked(HWIO_TCSR_PRONTO_BUS_TIMEOUT_RESET_ADDR, HWIO_TCSR_PRONTO_BUS_TIMEOUT_RESET_RMSK)
#define HWIO_TCSR_PRONTO_BUS_TIMEOUT_RESET_INM(m)      \
        in_dword_masked(HWIO_TCSR_PRONTO_BUS_TIMEOUT_RESET_ADDR, m)
#define HWIO_TCSR_PRONTO_BUS_TIMEOUT_RESET_OUT(v)      \
        out_dword(HWIO_TCSR_PRONTO_BUS_TIMEOUT_RESET_ADDR,v)
#define HWIO_TCSR_PRONTO_BUS_TIMEOUT_RESET_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_PRONTO_BUS_TIMEOUT_RESET_ADDR,m,v,HWIO_TCSR_PRONTO_BUS_TIMEOUT_RESET_IN)
#define HWIO_TCSR_PRONTO_BUS_TIMEOUT_RESET_WCSS_BUS_TIMEOUT_NOC_SOFT_RESET_BMSK                                    0x1
#define HWIO_TCSR_PRONTO_BUS_TIMEOUT_RESET_WCSS_BUS_TIMEOUT_NOC_SOFT_RESET_SHFT                                    0x0

#define HWIO_TCSR_CRYPTO0_HALTREQ_ADDR                                                                      (TCSR_TCSR_REGS_REG_BASE      + 0x00017000)
#define HWIO_TCSR_CRYPTO0_HALTREQ_PHYS                                                                      (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00017000)
#define HWIO_TCSR_CRYPTO0_HALTREQ_RMSK                                                                             0x1
#define HWIO_TCSR_CRYPTO0_HALTREQ_IN          \
        in_dword_masked(HWIO_TCSR_CRYPTO0_HALTREQ_ADDR, HWIO_TCSR_CRYPTO0_HALTREQ_RMSK)
#define HWIO_TCSR_CRYPTO0_HALTREQ_INM(m)      \
        in_dword_masked(HWIO_TCSR_CRYPTO0_HALTREQ_ADDR, m)
#define HWIO_TCSR_CRYPTO0_HALTREQ_OUT(v)      \
        out_dword(HWIO_TCSR_CRYPTO0_HALTREQ_ADDR,v)
#define HWIO_TCSR_CRYPTO0_HALTREQ_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_CRYPTO0_HALTREQ_ADDR,m,v,HWIO_TCSR_CRYPTO0_HALTREQ_IN)
#define HWIO_TCSR_CRYPTO0_HALTREQ_CRYPTO0_HALTREQ_BMSK                                                             0x1
#define HWIO_TCSR_CRYPTO0_HALTREQ_CRYPTO0_HALTREQ_SHFT                                                             0x0

#define HWIO_TCSR_CRYPTO0_HALTACK_ADDR                                                                      (TCSR_TCSR_REGS_REG_BASE      + 0x00017004)
#define HWIO_TCSR_CRYPTO0_HALTACK_PHYS                                                                      (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00017004)
#define HWIO_TCSR_CRYPTO0_HALTACK_RMSK                                                                             0x1
#define HWIO_TCSR_CRYPTO0_HALTACK_IN          \
        in_dword_masked(HWIO_TCSR_CRYPTO0_HALTACK_ADDR, HWIO_TCSR_CRYPTO0_HALTACK_RMSK)
#define HWIO_TCSR_CRYPTO0_HALTACK_INM(m)      \
        in_dword_masked(HWIO_TCSR_CRYPTO0_HALTACK_ADDR, m)
#define HWIO_TCSR_CRYPTO0_HALTACK_CRYPTO0_HALTACK_BMSK                                                             0x1
#define HWIO_TCSR_CRYPTO0_HALTACK_CRYPTO0_HALTACK_SHFT                                                             0x0

#define HWIO_TCSR_CRYPTO0_MASTER_IDLE_ADDR                                                                  (TCSR_TCSR_REGS_REG_BASE      + 0x00017008)
#define HWIO_TCSR_CRYPTO0_MASTER_IDLE_PHYS                                                                  (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00017008)
#define HWIO_TCSR_CRYPTO0_MASTER_IDLE_RMSK                                                                         0x1
#define HWIO_TCSR_CRYPTO0_MASTER_IDLE_IN          \
        in_dword_masked(HWIO_TCSR_CRYPTO0_MASTER_IDLE_ADDR, HWIO_TCSR_CRYPTO0_MASTER_IDLE_RMSK)
#define HWIO_TCSR_CRYPTO0_MASTER_IDLE_INM(m)      \
        in_dword_masked(HWIO_TCSR_CRYPTO0_MASTER_IDLE_ADDR, m)
#define HWIO_TCSR_CRYPTO0_MASTER_IDLE_CRYPTO0_MASTER_IDLE_BMSK                                                     0x1
#define HWIO_TCSR_CRYPTO0_MASTER_IDLE_CRYPTO0_MASTER_IDLE_SHFT                                                     0x0

#define HWIO_TCSR_MSSQ6_HALTREQ_ADDR                                                                        (TCSR_TCSR_REGS_REG_BASE      + 0x00018000)
#define HWIO_TCSR_MSSQ6_HALTREQ_PHYS                                                                        (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00018000)
#define HWIO_TCSR_MSSQ6_HALTREQ_RMSK                                                                               0x1
#define HWIO_TCSR_MSSQ6_HALTREQ_IN          \
        in_dword_masked(HWIO_TCSR_MSSQ6_HALTREQ_ADDR, HWIO_TCSR_MSSQ6_HALTREQ_RMSK)
#define HWIO_TCSR_MSSQ6_HALTREQ_INM(m)      \
        in_dword_masked(HWIO_TCSR_MSSQ6_HALTREQ_ADDR, m)
#define HWIO_TCSR_MSSQ6_HALTREQ_OUT(v)      \
        out_dword(HWIO_TCSR_MSSQ6_HALTREQ_ADDR,v)
#define HWIO_TCSR_MSSQ6_HALTREQ_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MSSQ6_HALTREQ_ADDR,m,v,HWIO_TCSR_MSSQ6_HALTREQ_IN)
#define HWIO_TCSR_MSSQ6_HALTREQ_MSSQ6_HALTREQ_BMSK                                                                 0x1
#define HWIO_TCSR_MSSQ6_HALTREQ_MSSQ6_HALTREQ_SHFT                                                                 0x0

#define HWIO_TCSR_MSSQ6_HALTACK_ADDR                                                                        (TCSR_TCSR_REGS_REG_BASE      + 0x00018004)
#define HWIO_TCSR_MSSQ6_HALTACK_PHYS                                                                        (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00018004)
#define HWIO_TCSR_MSSQ6_HALTACK_RMSK                                                                               0x1
#define HWIO_TCSR_MSSQ6_HALTACK_IN          \
        in_dword_masked(HWIO_TCSR_MSSQ6_HALTACK_ADDR, HWIO_TCSR_MSSQ6_HALTACK_RMSK)
#define HWIO_TCSR_MSSQ6_HALTACK_INM(m)      \
        in_dword_masked(HWIO_TCSR_MSSQ6_HALTACK_ADDR, m)
#define HWIO_TCSR_MSSQ6_HALTACK_MSSQ6_HALTACK_BMSK                                                                 0x1
#define HWIO_TCSR_MSSQ6_HALTACK_MSSQ6_HALTACK_SHFT                                                                 0x0

#define HWIO_TCSR_MSSQ6_MASTER_IDLE_ADDR                                                                    (TCSR_TCSR_REGS_REG_BASE      + 0x00018008)
#define HWIO_TCSR_MSSQ6_MASTER_IDLE_PHYS                                                                    (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00018008)
#define HWIO_TCSR_MSSQ6_MASTER_IDLE_RMSK                                                                           0x1
#define HWIO_TCSR_MSSQ6_MASTER_IDLE_IN          \
        in_dword_masked(HWIO_TCSR_MSSQ6_MASTER_IDLE_ADDR, HWIO_TCSR_MSSQ6_MASTER_IDLE_RMSK)
#define HWIO_TCSR_MSSQ6_MASTER_IDLE_INM(m)      \
        in_dword_masked(HWIO_TCSR_MSSQ6_MASTER_IDLE_ADDR, m)
#define HWIO_TCSR_MSSQ6_MASTER_IDLE_MSSQ6_MASTER_IDLE_BMSK                                                         0x1
#define HWIO_TCSR_MSSQ6_MASTER_IDLE_MSSQ6_MASTER_IDLE_SHFT                                                         0x0

#define HWIO_TCSR_MSSQ6_POWER_STATE_ADDR                                                                    (TCSR_TCSR_REGS_REG_BASE      + 0x00018010)
#define HWIO_TCSR_MSSQ6_POWER_STATE_PHYS                                                                    (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00018010)
#define HWIO_TCSR_MSSQ6_POWER_STATE_RMSK                                                                           0x1
#define HWIO_TCSR_MSSQ6_POWER_STATE_IN          \
        in_dword_masked(HWIO_TCSR_MSSQ6_POWER_STATE_ADDR, HWIO_TCSR_MSSQ6_POWER_STATE_RMSK)
#define HWIO_TCSR_MSSQ6_POWER_STATE_INM(m)      \
        in_dword_masked(HWIO_TCSR_MSSQ6_POWER_STATE_ADDR, m)
#define HWIO_TCSR_MSSQ6_POWER_STATE_MSSQ6_POWER_STATE_BMSK                                                         0x1
#define HWIO_TCSR_MSSQ6_POWER_STATE_MSSQ6_POWER_STATE_SHFT                                                         0x0

#define HWIO_TCSR_MSS_OFFLINE_HALTREQ_ADDR                                                                  (TCSR_TCSR_REGS_REG_BASE      + 0x00019000)
#define HWIO_TCSR_MSS_OFFLINE_HALTREQ_PHYS                                                                  (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00019000)
#define HWIO_TCSR_MSS_OFFLINE_HALTREQ_RMSK                                                                         0x1
#define HWIO_TCSR_MSS_OFFLINE_HALTREQ_IN          \
        in_dword_masked(HWIO_TCSR_MSS_OFFLINE_HALTREQ_ADDR, HWIO_TCSR_MSS_OFFLINE_HALTREQ_RMSK)
#define HWIO_TCSR_MSS_OFFLINE_HALTREQ_INM(m)      \
        in_dword_masked(HWIO_TCSR_MSS_OFFLINE_HALTREQ_ADDR, m)
#define HWIO_TCSR_MSS_OFFLINE_HALTREQ_OUT(v)      \
        out_dword(HWIO_TCSR_MSS_OFFLINE_HALTREQ_ADDR,v)
#define HWIO_TCSR_MSS_OFFLINE_HALTREQ_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MSS_OFFLINE_HALTREQ_ADDR,m,v,HWIO_TCSR_MSS_OFFLINE_HALTREQ_IN)
#define HWIO_TCSR_MSS_OFFLINE_HALTREQ_MSS_OFFLINE_HALTREQ_BMSK                                                     0x1
#define HWIO_TCSR_MSS_OFFLINE_HALTREQ_MSS_OFFLINE_HALTREQ_SHFT                                                     0x0

#define HWIO_TCSR_MSS_OFFLINE_HALTACK_ADDR                                                                  (TCSR_TCSR_REGS_REG_BASE      + 0x00019004)
#define HWIO_TCSR_MSS_OFFLINE_HALTACK_PHYS                                                                  (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00019004)
#define HWIO_TCSR_MSS_OFFLINE_HALTACK_RMSK                                                                         0x1
#define HWIO_TCSR_MSS_OFFLINE_HALTACK_IN          \
        in_dword_masked(HWIO_TCSR_MSS_OFFLINE_HALTACK_ADDR, HWIO_TCSR_MSS_OFFLINE_HALTACK_RMSK)
#define HWIO_TCSR_MSS_OFFLINE_HALTACK_INM(m)      \
        in_dword_masked(HWIO_TCSR_MSS_OFFLINE_HALTACK_ADDR, m)
#define HWIO_TCSR_MSS_OFFLINE_HALTACK_MSS_OFFLINE_HALTACK_BMSK                                                     0x1
#define HWIO_TCSR_MSS_OFFLINE_HALTACK_MSS_OFFLINE_HALTACK_SHFT                                                     0x0

#define HWIO_TCSR_MSS_OFFLINE_MASTER_IDLE_ADDR                                                              (TCSR_TCSR_REGS_REG_BASE      + 0x00019008)
#define HWIO_TCSR_MSS_OFFLINE_MASTER_IDLE_PHYS                                                              (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00019008)
#define HWIO_TCSR_MSS_OFFLINE_MASTER_IDLE_RMSK                                                                     0x1
#define HWIO_TCSR_MSS_OFFLINE_MASTER_IDLE_IN          \
        in_dword_masked(HWIO_TCSR_MSS_OFFLINE_MASTER_IDLE_ADDR, HWIO_TCSR_MSS_OFFLINE_MASTER_IDLE_RMSK)
#define HWIO_TCSR_MSS_OFFLINE_MASTER_IDLE_INM(m)      \
        in_dword_masked(HWIO_TCSR_MSS_OFFLINE_MASTER_IDLE_ADDR, m)
#define HWIO_TCSR_MSS_OFFLINE_MASTER_IDLE_MSS_OFFLINE_MASTER_IDLE_BMSK                                             0x1
#define HWIO_TCSR_MSS_OFFLINE_MASTER_IDLE_MSS_OFFLINE_MASTER_IDLE_SHFT                                             0x0

#define HWIO_TCSR_IPA_HALTREQ_ADDR                                                                          (TCSR_TCSR_REGS_REG_BASE      + 0x0001a000)
#define HWIO_TCSR_IPA_HALTREQ_PHYS                                                                          (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x0001a000)
#define HWIO_TCSR_IPA_HALTREQ_RMSK                                                                                 0x1
#define HWIO_TCSR_IPA_HALTREQ_IN          \
        in_dword_masked(HWIO_TCSR_IPA_HALTREQ_ADDR, HWIO_TCSR_IPA_HALTREQ_RMSK)
#define HWIO_TCSR_IPA_HALTREQ_INM(m)      \
        in_dword_masked(HWIO_TCSR_IPA_HALTREQ_ADDR, m)
#define HWIO_TCSR_IPA_HALTREQ_OUT(v)      \
        out_dword(HWIO_TCSR_IPA_HALTREQ_ADDR,v)
#define HWIO_TCSR_IPA_HALTREQ_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_IPA_HALTREQ_ADDR,m,v,HWIO_TCSR_IPA_HALTREQ_IN)
#define HWIO_TCSR_IPA_HALTREQ_IPA_HALTREQ_BMSK                                                                     0x1
#define HWIO_TCSR_IPA_HALTREQ_IPA_HALTREQ_SHFT                                                                     0x0

#define HWIO_TCSR_IPA_HALTACK_ADDR                                                                          (TCSR_TCSR_REGS_REG_BASE      + 0x0001a004)
#define HWIO_TCSR_IPA_HALTACK_PHYS                                                                          (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x0001a004)
#define HWIO_TCSR_IPA_HALTACK_RMSK                                                                                 0x1
#define HWIO_TCSR_IPA_HALTACK_IN          \
        in_dword_masked(HWIO_TCSR_IPA_HALTACK_ADDR, HWIO_TCSR_IPA_HALTACK_RMSK)
#define HWIO_TCSR_IPA_HALTACK_INM(m)      \
        in_dword_masked(HWIO_TCSR_IPA_HALTACK_ADDR, m)
#define HWIO_TCSR_IPA_HALTACK_IPA_HALTACK_BMSK                                                                     0x1
#define HWIO_TCSR_IPA_HALTACK_IPA_HALTACK_SHFT                                                                     0x0

#define HWIO_TCSR_MSSCE_HALTREQ_ADDR                                                                        (TCSR_TCSR_REGS_REG_BASE      + 0x0001b000)
#define HWIO_TCSR_MSSCE_HALTREQ_PHYS                                                                        (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x0001b000)
#define HWIO_TCSR_MSSCE_HALTREQ_RMSK                                                                               0x1
#define HWIO_TCSR_MSSCE_HALTREQ_IN          \
        in_dword_masked(HWIO_TCSR_MSSCE_HALTREQ_ADDR, HWIO_TCSR_MSSCE_HALTREQ_RMSK)
#define HWIO_TCSR_MSSCE_HALTREQ_INM(m)      \
        in_dword_masked(HWIO_TCSR_MSSCE_HALTREQ_ADDR, m)
#define HWIO_TCSR_MSSCE_HALTREQ_OUT(v)      \
        out_dword(HWIO_TCSR_MSSCE_HALTREQ_ADDR,v)
#define HWIO_TCSR_MSSCE_HALTREQ_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_MSSCE_HALTREQ_ADDR,m,v,HWIO_TCSR_MSSCE_HALTREQ_IN)
#define HWIO_TCSR_MSSCE_HALTREQ_MSSCE_HALTREQ_BMSK                                                                 0x1
#define HWIO_TCSR_MSSCE_HALTREQ_MSSCE_HALTREQ_SHFT                                                                 0x0

#define HWIO_TCSR_MSSCE_HALTACK_ADDR                                                                        (TCSR_TCSR_REGS_REG_BASE      + 0x0001b004)
#define HWIO_TCSR_MSSCE_HALTACK_PHYS                                                                        (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x0001b004)
#define HWIO_TCSR_MSSCE_HALTACK_RMSK                                                                               0x1
#define HWIO_TCSR_MSSCE_HALTACK_IN          \
        in_dword_masked(HWIO_TCSR_MSSCE_HALTACK_ADDR, HWIO_TCSR_MSSCE_HALTACK_RMSK)
#define HWIO_TCSR_MSSCE_HALTACK_INM(m)      \
        in_dword_masked(HWIO_TCSR_MSSCE_HALTACK_ADDR, m)
#define HWIO_TCSR_MSSCE_HALTACK_MSSCE_HALTACK_BMSK                                                                 0x1
#define HWIO_TCSR_MSSCE_HALTACK_MSSCE_HALTACK_SHFT                                                                 0x0

#define HWIO_TCSR_MSSCE_MASTER_IDLE_ADDR                                                                    (TCSR_TCSR_REGS_REG_BASE      + 0x0001b008)
#define HWIO_TCSR_MSSCE_MASTER_IDLE_PHYS                                                                    (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x0001b008)
#define HWIO_TCSR_MSSCE_MASTER_IDLE_RMSK                                                                           0x1
#define HWIO_TCSR_MSSCE_MASTER_IDLE_IN          \
        in_dword_masked(HWIO_TCSR_MSSCE_MASTER_IDLE_ADDR, HWIO_TCSR_MSSCE_MASTER_IDLE_RMSK)
#define HWIO_TCSR_MSSCE_MASTER_IDLE_INM(m)      \
        in_dword_masked(HWIO_TCSR_MSSCE_MASTER_IDLE_ADDR, m)
#define HWIO_TCSR_MSSCE_MASTER_IDLE_MSSCE_MASTER_IDLE_BMSK                                                         0x1
#define HWIO_TCSR_MSSCE_MASTER_IDLE_MSSCE_MASTER_IDLE_SHFT                                                         0x0

#define HWIO_TCSR_SDC1_HALTREQ_ADDR                                                                         (TCSR_TCSR_REGS_REG_BASE      + 0x0001c000)
#define HWIO_TCSR_SDC1_HALTREQ_PHYS                                                                         (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x0001c000)
#define HWIO_TCSR_SDC1_HALTREQ_RMSK                                                                         0xffffffff
#define HWIO_TCSR_SDC1_HALTREQ_IN          \
        in_dword_masked(HWIO_TCSR_SDC1_HALTREQ_ADDR, HWIO_TCSR_SDC1_HALTREQ_RMSK)
#define HWIO_TCSR_SDC1_HALTREQ_INM(m)      \
        in_dword_masked(HWIO_TCSR_SDC1_HALTREQ_ADDR, m)
#define HWIO_TCSR_SDC1_HALTREQ_OUT(v)      \
        out_dword(HWIO_TCSR_SDC1_HALTREQ_ADDR,v)
#define HWIO_TCSR_SDC1_HALTREQ_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SDC1_HALTREQ_ADDR,m,v,HWIO_TCSR_SDC1_HALTREQ_IN)

#define HWIO_TCSR_SDC1_HALTACK_ADDR                                                                         (TCSR_TCSR_REGS_REG_BASE      + 0x0001c004)
#define HWIO_TCSR_SDC1_HALTACK_PHYS                                                                         (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x0001c004)
#define HWIO_TCSR_SDC1_HALTACK_RMSK                                                                                0x1
#define HWIO_TCSR_SDC1_HALTACK_IN          \
        in_dword_masked(HWIO_TCSR_SDC1_HALTACK_ADDR, HWIO_TCSR_SDC1_HALTACK_RMSK)
#define HWIO_TCSR_SDC1_HALTACK_INM(m)      \
        in_dword_masked(HWIO_TCSR_SDC1_HALTACK_ADDR, m)

#define HWIO_TCSR_SDC1_MASTER_IDLE_ADDR                                                                     (TCSR_TCSR_REGS_REG_BASE      + 0x0001c008)
#define HWIO_TCSR_SDC1_MASTER_IDLE_PHYS                                                                     (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x0001c008)
#define HWIO_TCSR_SDC1_MASTER_IDLE_RMSK                                                                            0x1
#define HWIO_TCSR_SDC1_MASTER_IDLE_IN          \
        in_dword_masked(HWIO_TCSR_SDC1_MASTER_IDLE_ADDR, HWIO_TCSR_SDC1_MASTER_IDLE_RMSK)
#define HWIO_TCSR_SDC1_MASTER_IDLE_INM(m)      \
        in_dword_masked(HWIO_TCSR_SDC1_MASTER_IDLE_ADDR, m)

#define HWIO_TCSR_ULT_AUDIO_CORE_ABORT_REQ_ADDR                                                             (TCSR_TCSR_REGS_REG_BASE      + 0x0001e000)
#define HWIO_TCSR_ULT_AUDIO_CORE_ABORT_REQ_PHYS                                                             (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x0001e000)
#define HWIO_TCSR_ULT_AUDIO_CORE_ABORT_REQ_RMSK                                                                    0x1
#define HWIO_TCSR_ULT_AUDIO_CORE_ABORT_REQ_IN          \
        in_dword_masked(HWIO_TCSR_ULT_AUDIO_CORE_ABORT_REQ_ADDR, HWIO_TCSR_ULT_AUDIO_CORE_ABORT_REQ_RMSK)
#define HWIO_TCSR_ULT_AUDIO_CORE_ABORT_REQ_INM(m)      \
        in_dword_masked(HWIO_TCSR_ULT_AUDIO_CORE_ABORT_REQ_ADDR, m)
#define HWIO_TCSR_ULT_AUDIO_CORE_ABORT_REQ_OUT(v)      \
        out_dword(HWIO_TCSR_ULT_AUDIO_CORE_ABORT_REQ_ADDR,v)
#define HWIO_TCSR_ULT_AUDIO_CORE_ABORT_REQ_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_ULT_AUDIO_CORE_ABORT_REQ_ADDR,m,v,HWIO_TCSR_ULT_AUDIO_CORE_ABORT_REQ_IN)
#define HWIO_TCSR_ULT_AUDIO_CORE_ABORT_REQ_ULT_AUDIO_CORE_ABORT_REQ_BMSK                                           0x1
#define HWIO_TCSR_ULT_AUDIO_CORE_ABORT_REQ_ULT_AUDIO_CORE_ABORT_REQ_SHFT                                           0x0

#define HWIO_TCSR_ULT_AUDIO_CORE_ABORT_ACK_ADDR                                                             (TCSR_TCSR_REGS_REG_BASE      + 0x0001e004)
#define HWIO_TCSR_ULT_AUDIO_CORE_ABORT_ACK_PHYS                                                             (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x0001e004)
#define HWIO_TCSR_ULT_AUDIO_CORE_ABORT_ACK_RMSK                                                                    0x1
#define HWIO_TCSR_ULT_AUDIO_CORE_ABORT_ACK_IN          \
        in_dword_masked(HWIO_TCSR_ULT_AUDIO_CORE_ABORT_ACK_ADDR, HWIO_TCSR_ULT_AUDIO_CORE_ABORT_ACK_RMSK)
#define HWIO_TCSR_ULT_AUDIO_CORE_ABORT_ACK_INM(m)      \
        in_dword_masked(HWIO_TCSR_ULT_AUDIO_CORE_ABORT_ACK_ADDR, m)
#define HWIO_TCSR_ULT_AUDIO_CORE_ABORT_ACK_ULT_AUDIO_CORE_ABORT_ACK_BMSK                                           0x1
#define HWIO_TCSR_ULT_AUDIO_CORE_ABORT_ACK_ULT_AUDIO_CORE_ABORT_ACK_SHFT                                           0x0

#define HWIO_TCSR_SPARE_APU_REG0_ADDR                                                                       (TCSR_TCSR_REGS_REG_BASE      + 0x0000f000)
#define HWIO_TCSR_SPARE_APU_REG0_PHYS                                                                       (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x0000f000)
#define HWIO_TCSR_SPARE_APU_REG0_RMSK                                                                       0xffffffff
#define HWIO_TCSR_SPARE_APU_REG0_IN          \
        in_dword_masked(HWIO_TCSR_SPARE_APU_REG0_ADDR, HWIO_TCSR_SPARE_APU_REG0_RMSK)
#define HWIO_TCSR_SPARE_APU_REG0_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPARE_APU_REG0_ADDR, m)
#define HWIO_TCSR_SPARE_APU_REG0_OUT(v)      \
        out_dword(HWIO_TCSR_SPARE_APU_REG0_ADDR,v)
#define HWIO_TCSR_SPARE_APU_REG0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPARE_APU_REG0_ADDR,m,v,HWIO_TCSR_SPARE_APU_REG0_IN)
#define HWIO_TCSR_SPARE_APU_REG0_SPARE_APU_REG0_BMSK                                                        0xffffffff
#define HWIO_TCSR_SPARE_APU_REG0_SPARE_APU_REG0_SHFT                                                               0x0

#define HWIO_TCSR_SPARE_APU_REG1_ADDR                                                                       (TCSR_TCSR_REGS_REG_BASE      + 0x0000f004)
#define HWIO_TCSR_SPARE_APU_REG1_PHYS                                                                       (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x0000f004)
#define HWIO_TCSR_SPARE_APU_REG1_RMSK                                                                       0xffffffff
#define HWIO_TCSR_SPARE_APU_REG1_IN          \
        in_dword_masked(HWIO_TCSR_SPARE_APU_REG1_ADDR, HWIO_TCSR_SPARE_APU_REG1_RMSK)
#define HWIO_TCSR_SPARE_APU_REG1_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPARE_APU_REG1_ADDR, m)
#define HWIO_TCSR_SPARE_APU_REG1_OUT(v)      \
        out_dword(HWIO_TCSR_SPARE_APU_REG1_ADDR,v)
#define HWIO_TCSR_SPARE_APU_REG1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPARE_APU_REG1_ADDR,m,v,HWIO_TCSR_SPARE_APU_REG1_IN)
#define HWIO_TCSR_SPARE_APU_REG1_SPARE_APU_REG1_BMSK                                                        0xffffffff
#define HWIO_TCSR_SPARE_APU_REG1_SPARE_APU_REG1_SHFT                                                               0x0

#define HWIO_TCSR_SPARE_APU_REG2_ADDR                                                                       (TCSR_TCSR_REGS_REG_BASE      + 0x0000f008)
#define HWIO_TCSR_SPARE_APU_REG2_PHYS                                                                       (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x0000f008)
#define HWIO_TCSR_SPARE_APU_REG2_RMSK                                                                       0xffffffff
#define HWIO_TCSR_SPARE_APU_REG2_IN          \
        in_dword_masked(HWIO_TCSR_SPARE_APU_REG2_ADDR, HWIO_TCSR_SPARE_APU_REG2_RMSK)
#define HWIO_TCSR_SPARE_APU_REG2_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPARE_APU_REG2_ADDR, m)
#define HWIO_TCSR_SPARE_APU_REG2_OUT(v)      \
        out_dword(HWIO_TCSR_SPARE_APU_REG2_ADDR,v)
#define HWIO_TCSR_SPARE_APU_REG2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPARE_APU_REG2_ADDR,m,v,HWIO_TCSR_SPARE_APU_REG2_IN)
#define HWIO_TCSR_SPARE_APU_REG2_SPARE_APU_REG2_BMSK                                                        0xffffffff
#define HWIO_TCSR_SPARE_APU_REG2_SPARE_APU_REG2_SHFT                                                               0x0

#define HWIO_TCSR_SPARE_APU_REG3_ADDR                                                                       (TCSR_TCSR_REGS_REG_BASE      + 0x0000f00c)
#define HWIO_TCSR_SPARE_APU_REG3_PHYS                                                                       (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x0000f00c)
#define HWIO_TCSR_SPARE_APU_REG3_RMSK                                                                       0xffffffff
#define HWIO_TCSR_SPARE_APU_REG3_IN          \
        in_dword_masked(HWIO_TCSR_SPARE_APU_REG3_ADDR, HWIO_TCSR_SPARE_APU_REG3_RMSK)
#define HWIO_TCSR_SPARE_APU_REG3_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPARE_APU_REG3_ADDR, m)
#define HWIO_TCSR_SPARE_APU_REG3_OUT(v)      \
        out_dword(HWIO_TCSR_SPARE_APU_REG3_ADDR,v)
#define HWIO_TCSR_SPARE_APU_REG3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPARE_APU_REG3_ADDR,m,v,HWIO_TCSR_SPARE_APU_REG3_IN)
#define HWIO_TCSR_SPARE_APU_REG3_SPARE_APU_REG3_BMSK                                                        0xffffffff
#define HWIO_TCSR_SPARE_APU_REG3_SPARE_APU_REG3_SHFT                                                               0x0

#define HWIO_TCSR_SPARE_REG0_ADDR                                                                           (TCSR_TCSR_REGS_REG_BASE      + 0x0001f000)
#define HWIO_TCSR_SPARE_REG0_PHYS                                                                           (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x0001f000)
#define HWIO_TCSR_SPARE_REG0_RMSK                                                                           0xffffffff
#define HWIO_TCSR_SPARE_REG0_IN          \
        in_dword_masked(HWIO_TCSR_SPARE_REG0_ADDR, HWIO_TCSR_SPARE_REG0_RMSK)
#define HWIO_TCSR_SPARE_REG0_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPARE_REG0_ADDR, m)
#define HWIO_TCSR_SPARE_REG0_OUT(v)      \
        out_dword(HWIO_TCSR_SPARE_REG0_ADDR,v)
#define HWIO_TCSR_SPARE_REG0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPARE_REG0_ADDR,m,v,HWIO_TCSR_SPARE_REG0_IN)
#define HWIO_TCSR_SPARE_REG0_SPARE_REG0_BMSK                                                                0xfffffffc
#define HWIO_TCSR_SPARE_REG0_SPARE_REG0_SHFT                                                                       0x2
#define HWIO_TCSR_SPARE_REG0_PCIE_EN_BMSK                                                                          0x2
#define HWIO_TCSR_SPARE_REG0_PCIE_EN_SHFT                                                                          0x1
#define HWIO_TCSR_SPARE_REG0_USB_EN_BMSK                                                                           0x1
#define HWIO_TCSR_SPARE_REG0_USB_EN_SHFT                                                                           0x0

#define HWIO_TCSR_SPARE_REG1_ADDR                                                                           (TCSR_TCSR_REGS_REG_BASE      + 0x0001f004)
#define HWIO_TCSR_SPARE_REG1_PHYS                                                                           (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x0001f004)
#define HWIO_TCSR_SPARE_REG1_RMSK                                                                           0xffffffff
#define HWIO_TCSR_SPARE_REG1_IN          \
        in_dword_masked(HWIO_TCSR_SPARE_REG1_ADDR, HWIO_TCSR_SPARE_REG1_RMSK)
#define HWIO_TCSR_SPARE_REG1_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPARE_REG1_ADDR, m)
#define HWIO_TCSR_SPARE_REG1_OUT(v)      \
        out_dword(HWIO_TCSR_SPARE_REG1_ADDR,v)
#define HWIO_TCSR_SPARE_REG1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPARE_REG1_ADDR,m,v,HWIO_TCSR_SPARE_REG1_IN)
#define HWIO_TCSR_SPARE_REG1_SPARE_REG1_BMSK                                                                0xffffffff
#define HWIO_TCSR_SPARE_REG1_SPARE_REG1_SHFT                                                                       0x0

#define HWIO_TCSR_SPARE_REG2_ADDR                                                                           (TCSR_TCSR_REGS_REG_BASE      + 0x0001f008)
#define HWIO_TCSR_SPARE_REG2_PHYS                                                                           (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x0001f008)
#define HWIO_TCSR_SPARE_REG2_RMSK                                                                           0xffffffff
#define HWIO_TCSR_SPARE_REG2_IN          \
        in_dword_masked(HWIO_TCSR_SPARE_REG2_ADDR, HWIO_TCSR_SPARE_REG2_RMSK)
#define HWIO_TCSR_SPARE_REG2_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPARE_REG2_ADDR, m)
#define HWIO_TCSR_SPARE_REG2_OUT(v)      \
        out_dword(HWIO_TCSR_SPARE_REG2_ADDR,v)
#define HWIO_TCSR_SPARE_REG2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPARE_REG2_ADDR,m,v,HWIO_TCSR_SPARE_REG2_IN)
#define HWIO_TCSR_SPARE_REG2_SPARE_REG2_BMSK                                                                0xffffffff
#define HWIO_TCSR_SPARE_REG2_SPARE_REG2_SHFT                                                                       0x0

#define HWIO_TCSR_SPARE_REG3_ADDR                                                                           (TCSR_TCSR_REGS_REG_BASE      + 0x0001f00c)
#define HWIO_TCSR_SPARE_REG3_PHYS                                                                           (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x0001f00c)
#define HWIO_TCSR_SPARE_REG3_RMSK                                                                           0xffffffff
#define HWIO_TCSR_SPARE_REG3_IN          \
        in_dword_masked(HWIO_TCSR_SPARE_REG3_ADDR, HWIO_TCSR_SPARE_REG3_RMSK)
#define HWIO_TCSR_SPARE_REG3_INM(m)      \
        in_dword_masked(HWIO_TCSR_SPARE_REG3_ADDR, m)
#define HWIO_TCSR_SPARE_REG3_OUT(v)      \
        out_dword(HWIO_TCSR_SPARE_REG3_ADDR,v)
#define HWIO_TCSR_SPARE_REG3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_SPARE_REG3_ADDR,m,v,HWIO_TCSR_SPARE_REG3_IN)
#define HWIO_TCSR_SPARE_REG3_SPARE_REG3_BMSK                                                                0xffffffff
#define HWIO_TCSR_SPARE_REG3_SPARE_REG3_SHFT                                                                       0x0

#define HWIO_TCSR_PNOC_SNOC_MEMTYPE_ADDR                                                                    (TCSR_TCSR_REGS_REG_BASE      + 0x00020000)
#define HWIO_TCSR_PNOC_SNOC_MEMTYPE_PHYS                                                                    (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00020000)
#define HWIO_TCSR_PNOC_SNOC_MEMTYPE_RMSK                                                                         0x11f
#define HWIO_TCSR_PNOC_SNOC_MEMTYPE_IN          \
        in_dword_masked(HWIO_TCSR_PNOC_SNOC_MEMTYPE_ADDR, HWIO_TCSR_PNOC_SNOC_MEMTYPE_RMSK)
#define HWIO_TCSR_PNOC_SNOC_MEMTYPE_INM(m)      \
        in_dword_masked(HWIO_TCSR_PNOC_SNOC_MEMTYPE_ADDR, m)
#define HWIO_TCSR_PNOC_SNOC_MEMTYPE_OUT(v)      \
        out_dword(HWIO_TCSR_PNOC_SNOC_MEMTYPE_ADDR,v)
#define HWIO_TCSR_PNOC_SNOC_MEMTYPE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_PNOC_SNOC_MEMTYPE_ADDR,m,v,HWIO_TCSR_PNOC_SNOC_MEMTYPE_IN)
#define HWIO_TCSR_PNOC_SNOC_MEMTYPE_MEM_TYPE_SEL_BMSK                                                            0x100
#define HWIO_TCSR_PNOC_SNOC_MEMTYPE_MEM_TYPE_SEL_SHFT                                                              0x8
#define HWIO_TCSR_PNOC_SNOC_MEMTYPE_INNERSHARED_VALUE_BMSK                                                        0x10
#define HWIO_TCSR_PNOC_SNOC_MEMTYPE_INNERSHARED_VALUE_SHFT                                                         0x4
#define HWIO_TCSR_PNOC_SNOC_MEMTYPE_SHARED_VALUE_BMSK                                                              0x8
#define HWIO_TCSR_PNOC_SNOC_MEMTYPE_SHARED_VALUE_SHFT                                                              0x3
#define HWIO_TCSR_PNOC_SNOC_MEMTYPE_MEM_TYPE_VALUE_BMSK                                                            0x7
#define HWIO_TCSR_PNOC_SNOC_MEMTYPE_MEM_TYPE_VALUE_SHFT                                                            0x0

#define HWIO_TCSR_RESET_DEBUG_SW_ENTRY_ADDR                                                                 (TCSR_TCSR_REGS_REG_BASE      + 0x00009000)
#define HWIO_TCSR_RESET_DEBUG_SW_ENTRY_PHYS                                                                 (TCSR_TCSR_REGS_REG_BASE_PHYS + 0x00009000)
#define HWIO_TCSR_RESET_DEBUG_SW_ENTRY_RMSK                                                                 0xffffffff
#define HWIO_TCSR_RESET_DEBUG_SW_ENTRY_IN          \
        in_dword_masked(HWIO_TCSR_RESET_DEBUG_SW_ENTRY_ADDR, HWIO_TCSR_RESET_DEBUG_SW_ENTRY_RMSK)
#define HWIO_TCSR_RESET_DEBUG_SW_ENTRY_INM(m)      \
        in_dword_masked(HWIO_TCSR_RESET_DEBUG_SW_ENTRY_ADDR, m)
#define HWIO_TCSR_RESET_DEBUG_SW_ENTRY_OUT(v)      \
        out_dword(HWIO_TCSR_RESET_DEBUG_SW_ENTRY_ADDR,v)
#define HWIO_TCSR_RESET_DEBUG_SW_ENTRY_OUTM(m,v) \
        out_dword_masked_ns(HWIO_TCSR_RESET_DEBUG_SW_ENTRY_ADDR,m,v,HWIO_TCSR_RESET_DEBUG_SW_ENTRY_IN)
#define HWIO_TCSR_RESET_DEBUG_SW_ENTRY_RESET_DEBUG_SW_ENTRY_BMSK                                            0xffffffff
#define HWIO_TCSR_RESET_DEBUG_SW_ENTRY_RESET_DEBUG_SW_ENTRY_SHFT                                                   0x0

/*----------------------------------------------------------------------------
 * MODULE: TCSR_TCSR_MUTEX
 *--------------------------------------------------------------------------*/

#define TCSR_TCSR_MUTEX_REG_BASE                  (CORE_TOP_CSR_BASE      + 0x00005000)
#define TCSR_TCSR_MUTEX_REG_BASE_PHYS             (CORE_TOP_CSR_BASE_PHYS + 0x00005000)

#define HWIO_TCSR_MUTEX_REG_n_ADDR(n)             (TCSR_TCSR_MUTEX_REG_BASE      + 0x00000000 + 0x1000 * (n))
#define HWIO_TCSR_MUTEX_REG_n_PHYS(n)             (TCSR_TCSR_MUTEX_REG_BASE_PHYS + 0x00000000 + 0x1000 * (n))
#define HWIO_TCSR_MUTEX_REG_n_RMSK                      0xff
#define HWIO_TCSR_MUTEX_REG_n_MAXn                        31
#define HWIO_TCSR_MUTEX_REG_n_INI(n)        \
        in_dword_masked(HWIO_TCSR_MUTEX_REG_n_ADDR(n), HWIO_TCSR_MUTEX_REG_n_RMSK)
#define HWIO_TCSR_MUTEX_REG_n_INMI(n,mask)    \
        in_dword_masked(HWIO_TCSR_MUTEX_REG_n_ADDR(n), mask)
#define HWIO_TCSR_MUTEX_REG_n_OUTI(n,val)    \
        out_dword(HWIO_TCSR_MUTEX_REG_n_ADDR(n),val)
#define HWIO_TCSR_MUTEX_REG_n_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_TCSR_MUTEX_REG_n_ADDR(n),mask,val,HWIO_TCSR_MUTEX_REG_n_INI(n))
#define HWIO_TCSR_MUTEX_REG_n_MUTEX_BMSK                0xff
#define HWIO_TCSR_MUTEX_REG_n_MUTEX_SHFT                 0x0


#endif /* __MODEM_HWIO_H__ */
