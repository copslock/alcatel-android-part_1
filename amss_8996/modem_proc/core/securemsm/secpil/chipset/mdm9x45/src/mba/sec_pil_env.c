/**
* @file sec_pil_env.c
* @brief Secure PIL MBA/TZ specific implementation.
*
* This file implements the TZ/MBA specific routines for secure PIL.
*
*/
/*===========================================================================
   Copyright (c) 2012 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/core.mpss/3.4.c3.11/securemsm/secpil/chipset/mdm9x45/src/mba/sec_pil_env.c#1 $
  $DateTime: 2016/03/28 23:02:17 $
  $Author: mplcsds1 $


when       who      what, where, why
--------   ---      ------------------------------------
26/03/13   dm       Added overflow checks & error checks
06/21/12   vg       fixed issues found while testing.
02/28/12   vg       Fixed review comments
02/20/12   vg       Ported from TZ PIL.
===========================================================================*/

/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include "sec_pil.h"
#include "sec_pil_env.h"
#include "sec_pil_priv.h"
#include "mba_rmb.h"
#include <IxErrno.h>
#include "HALxpu2.h"
#include <string.h>
#include <stringl.h>
#include "mba_ac.h"
#include "mbai.h"
#define SEC_BOOT_MSS_SW_TYPE 2

extern mba_rmb_pmi_t pmi_info;

/*----------------------------------------------------------------------------
 * Function Definitions 
 * -------------------------------------------------------------------------*/
 
int sec_pil_get_load_addr (sec_pil_proc_et proc, uint32 *new_start)
{
  if (!sec_pil_is_proc_supported(proc))
  {
    SEC_PIL_ERROR_CODE(PIL_GET_LOAD_ADDR_UNSUPORTED_PROC);
    return -E_INVALID_ARG;
  }

  if(new_start == NULL)
  {
    SEC_PIL_ERROR_CODE(PIL_GET_LOAD_ADDR_NULL_POINTER);
    return -E_INVALID_ARG;
  }
  
  *new_start = pmi_info.code_start;
  
  return E_SUCCESS;
}


/**
 * @brief This funnction mpas the sec pil hash enum to crypto driver hash enum
 *
 * @param[in]     alg1  sec pil hash enum
 * @param[in,out] alg2  crypto driver hash enum
 *
 * @return 0 on success, negative on failure 
 */ 
static inline int sec_pil_map_hash_enum(sec_pil_hash_algo_et alg1,
                                        CeMLHashAlgoType   *alg2)
{
  if (alg2 == NULL)
    return -E_FAILURE;

  if (alg1 == SEC_PIL_HASH_SHA1)
    *alg2 = CEML_HASH_ALGO_SHA1;

  else if (alg1 == SEC_PIL_HASH_SHA256)
    *alg2 = CEML_HASH_ALGO_SHA256;
  else
    return -E_INVALID_ARG;

  return E_SUCCESS;
}

/**
 * @brief This funnction returns digest length for given hash type.
 *
 * @param[in]     alg         hash type
 * @param[in,out] digest_len  digest length
 *
 * @return 0 on success, negative on failure 
 */
static inline int sec_pil_check_digest_len(CeMLHashAlgoType alg,
                                           uint32 *digest_len)
{
  if (digest_len == NULL)
    return -E_FAILURE;

  if ((alg == CEML_HASH_ALGO_SHA1) &&
      (*digest_len < SEC_PIL_SHA1_HASH_SZ))
    return -E_FAILURE;

  else if ((alg == CEML_HASH_ALGO_SHA256 ) &&
           (*digest_len < SEC_PIL_SHA256_HASH_SZ))
    return -E_FAILURE;

  else if ((alg != CEML_HASH_ALGO_SHA1) &&
           (alg != CEML_HASH_ALGO_SHA256))
    return -E_FAILURE;

  *digest_len = (alg == CEML_HASH_ALGO_SHA1) ?
                       SEC_PIL_SHA1_HASH_SZ : SEC_PIL_SHA256_HASH_SZ;

  return E_SUCCESS;
}
/**
 * @brief Locks the image memory area.
 *
 * @param[in] proc  The processor ID of the PIL image.
 * @param[in] hdr   Pointer to the first entry in ELF program header.
 * @param[in] count Number of program header entries in hdr.
 * @param[in] entry The entry address to the image.
 *
 * @return 0 on success.
 */
int sec_pil_lock_xpu(sec_pil_proc_et proc, const Elf32_Phdr* hdr,
                     uint32 count, uint32 entry)
{
  uint32 i = 0;
  uint32 start_mss_partition =0, end_mss_partition=0;
  uint32 mss_partition_range_initialized = 0;
  
  do
  {
    /* All image segments must be within the image area (either EBI or internal
     * memory). */
    for(i = 0; i < count; i++, hdr++)
    {
      /* If Elf Segment is a loadable segment */
      if((sec_pil_is_valid_segment(hdr) ||
       (MI_PBT_ACCESS_TYPE_VALUE(hdr->p_flags) == MI_PBT_ZI_SEGMENT)) && (hdr->p_memsz != 0))
      {
        if((start_mss_partition > hdr->p_paddr)||(mss_partition_range_initialized == 0))
          start_mss_partition = hdr->p_paddr;

        if((end_mss_partition < hdr->p_paddr)||(mss_partition_range_initialized == 0))
          end_mss_partition = hdr->p_paddr + hdr->p_memsz;
        
        mss_partition_range_initialized = 1;
      }
    }
   
    if ((start_mss_partition > entry) || (end_mss_partition < entry))
      return -E_FAILURE;
    
    mba_xpu_lock_region(start_mss_partition, end_mss_partition, MBA_XPU_PARTITION_PMI);
	
  } while(0);

  return E_SUCCESS;
}

/**
 * @brief Unlocks the image memory area.
 *
 * @param[in] proc  The processor to unlock
 *
 * @return 0 on success.
 */
int sec_pil_unlock_xpu(sec_pil_proc_et proc)
{
  mba_xpu_unlock_region(MBA_XPU_PARTITION_PMI);
  
  return E_SUCCESS;
}

/**
 * @brief This function is just a stub for MBA. 
 *
 * @return TRUE  
 */
boolean sec_pil_is_ns_range(const void* start, uint32 len)
{
  /* This function is just a stub for MBA for now */
  return TRUE;
}

/**
 * @brief This function is just a stub for MBA. 
 *
 * @return 0  
 */
int sec_pil_subsys_bring_up(sec_pil_proc_et proc, uint32 start_addr)
{
  /* This function is just a stub for MBA */
  return E_SUCCESS;
}

/**
 * @brief This function is just a stub for MBA. 
 *
 * @return 0  
 */
int32 sec_pil_prepare_subsys(sec_pil_proc_et proc)
{  
  /* Currently we dont need to do any pre configurations for modem subsytem */
  return E_SUCCESS;
}

/**
 * @brief This funnction returns PBL secboot function pointers.
 *
 * @param[in,out]  ftbl_ptr     secboot function pointers.
 * @param[in,out]  ftbl_hw_ptr  secboot hw function pointers.
 *
 * @return 0 on success. 
 */ 
int32 sec_pil_get_secboot_routines(secboot_ftbl_type *ftbl_ptr,
                                    secboot_hw_ftbl_type *ftbl_hw_ptr)
{
  /* Use Boot Rom Secure Boot */
  pbl_modem_mba_shared_data_type *pbl_data;
  pbl_data = mba_rmb_get_pbl_shared_data();

  if(pbl_data != NULL && ftbl_ptr != NULL && ftbl_hw_ptr != NULL)
  {
    *ftbl_ptr = pbl_data->pbl_modem_mba_secboot_shared_data.pbl_secboot_ftbl;
    *ftbl_hw_ptr = 
         pbl_data->pbl_modem_mba_secboot_shared_data.pbl_secboot_hw_ftbl;
    return E_SUCCESS;
  }
  return -E_FAILURE;
}

/**
 * @brief This funnction returns code segment in SECURE_BOOTn register
 *        containing authentication information of the image.
 *
 * @param[in]      proc          The peripheral image.
 * @param[in,out]  code_segment  Code segment in SECURE_BOOTn register
 *                               containing authentication information
 *                               of the image.
 *
 * @return E_SUCCESS on success and E_INVALID_ARG for invalid args.
 */
int32 sec_pil_get_code_segment(sec_pil_proc_et proc,uint32 *code_segment)
{

  if (!sec_pil_is_proc_supported(proc))
  {
    SEC_PIL_ERROR_CODE(PIL_GET_CODE_SEGMENT_UNSUPPORTED_PROC);
    return -E_INVALID_ARG;
  }

  if(code_segment == NULL)
  {
    SEC_PIL_ERROR_CODE(PIL_GET_CODE_SEGMENT_NULL_POINTER);
    return -E_INVALID_ARG;
  }
  
  *code_segment = SECBOOT_HW_MSS_CODE_SEGMENT;

  return E_SUCCESS;
}

/**
 * @brief This funnction returns sw type for the peripheral image. 
 *
 * @param[in]      proc   The peripheral image.
 * @param[in,out]  sw_id  SW type for the peripheral image. 
 *
 * @return E_SUCCESS on success and E_INVALID_ARG for invalid args.
 */
int32 sec_pil_get_sw_id(sec_pil_proc_et proc,uint32 *sw_id)
{

  if (!sec_pil_is_proc_supported(proc))
  {
    SEC_PIL_ERROR_CODE(PIL_GET_SW_ID_UNSUPORTED_PROC);
    return -E_INVALID_ARG;
  }

  if(sw_id == NULL)
  {
    SEC_PIL_ERROR_CODE(PIL_GET_SW_ID_NULL_POINTER);
    return -E_INVALID_ARG;
  }

  /* MBA/TZ needs to provide a function which i can call here to get SW ID*/
  *sw_id = SEC_BOOT_MSS_SW_TYPE;

  return E_SUCCESS;
}

/**
 * @brief This funnction returns sw version for the peripheral image. 
 *
 * @param[in]      proc   The peripheral image.
 * @param[in,out]  sw_id  SW version for the peripheral image. 
 *
 * @return E_SUCCESS on success and E_INVALID_ARG for invalid args.
 */
int32 sec_pil_get_sw_version(sec_pil_proc_et proc, uint32 *sw_version)
{

  if (!sec_pil_is_proc_supported(proc))
  {
    SEC_PIL_ERROR_CODE(PIL_GET_SW_VERISON_UNSUPPORTED_PROC);
    return -E_INVALID_ARG;
  }

  if(sw_version == NULL)
  {
    SEC_PIL_ERROR_CODE(PIL_GET_SW_VERSION_NULL_POINTER);
    return -E_INVALID_ARG;
  }
  
  /* Read the MSS Software version */
  *sw_version = mba_secctrl_get_software_version();

  return E_SUCCESS;
}


/**
 * Dynamically locks an MPU resource group (memory area). Memory area start and
 * end address alignment is dependent on the actual MPU.
 *
 * @param [in] resource   The resource group id the MPU was assigned to.
 * @param [in] start      Start of the memory area.
 * @param [in] end        End of the memory area.
 *
 * @return Zero on success.
 */
int32 sec_pil_mpu_lock_area(uint32 *resource, uint32 start, uint32 end)
{
   *resource = MBA_XPU_PARTITION_PMI;
   
   mba_xpu_lock_region(start, end, MBA_XPU_PARTITION_PMI);

   return E_SUCCESS;
}


/**
 * Dynamically unlocks an MPU resource group (memory area).
 *
 * @param [in] resource The resource group id the MPU was assigned to.
 *
 * @return Zero on success, otherwise an error code.
 */
int32 sec_pil_mpu_unlock_area(uint32 resource)
{
  
  mba_xpu_unlock_region(MBA_XPU_PARTITION_PMI); 
  
  return E_SUCCESS;
}

/**
 * @brief This function is just a stub for MBA. 
 */
void sec_pil_dcache_inval_region(void *addr, unsigned int length)
{
  return;
}

/**
 * @brief  This function will create a message digest hash using the
 *         algorithm specified.
 *
 * @param alg[in]          The hash algorithm
 * @param msg[in]          The message to hash
 * @param msg_len[in]      The length of the message
 * @param digest[in,out]   The digest to store
 * @param digest_len[in]   Length of the output message digest hash
 *                         buffer in bytes. Must be 20 bytes for SHA1 and
 *                         32 bytes for SHA256
 *
 * @return 0 on success, negative on failure
 */
int sec_pil_hash(sec_pil_hash_algo_et alg,
                 const uint8 *msg,
                 uint32 msg_len,
                 uint8 *digest,
                 uint32 digest_len)
{
  sec_pil_hash_ctx ctx;
  int ret = E_SUCCESS;

  do
  {
    if ((ret = sec_pil_hash_init(alg, &ctx)) < 0)
    {
      break;
    }

    if ((ret = sec_pil_hash_update(&ctx, msg, msg_len)) < 0)
    {
      CeMLHashDeInit(&ctx.ctx);
      break;
    }

    if ((ret = sec_pil_hash_final(&ctx, digest, digest_len)) < 0)
    {
      break;
    }

  } while (0);

  if (ret < 0)
  {
    return -E_FAILURE;
  }

  return E_SUCCESS;
}

/**
 * @brief Intialize a hash context for update and final functions.
 *
 * @param alg[in]          The algorithm standard to use
 * @param hash_ctx[in,out] The hash context
 *
 * @return 0 on success, negative on failure
 *
 */
int sec_pil_hash_init(sec_pil_hash_algo_et  alg,
                      sec_pil_hash_ctx *hash_ctx)
{

  CeMLHashAlgoType alg2;

  if (hash_ctx == NULL)
  {
    return -E_INVALID_ARG;
  }
  
  if (sec_pil_map_hash_enum(alg, &alg2) < 0)
  {
    return -E_INVALID_ARG;
  }

  hash_ctx->alg = alg2;
  hash_ctx->ctx = NULL;  

  if (CeMLHashInit(&hash_ctx->ctx, hash_ctx->alg) != CEML_ERROR_SUCCESS)
  {
    return -E_FAILURE;
  }

  return E_SUCCESS;
}

/**
 * @brief  This function will hash some data into the hash context
 *         structure, which Must have been initialized by
 *         sec_pil_hash_init().
 *
 * @param hash_ctx[in]    The hash context
 * @param msg[in]         Pointer to the msg to hash
 * @param msg_len[in]     Length of the msg to hash
 *
 * @return 0 on success, negative on failure
 *
 * @see sec_pil_hash_init
 */
int sec_pil_hash_update(const sec_pil_hash_ctx *hash_ctx,
                        const uint8 *msg, uint32 msg_len)
{

  CEMLIovecListType    iovec;
  CEMLIovecType        iovec_buf; 

  iovec.size  = 1;
  iovec.iov   = &iovec_buf;
  iovec.iov[0].pvBase = (void *)msg;
  iovec.iov[0].dwLen = msg_len;

  if (hash_ctx == NULL || msg == NULL)
  {
    return -E_INVALID_ARG;
  }  

  if(CeMLHashUpdate(hash_ctx->ctx, iovec) != CEML_ERROR_SUCCESS)
  {
    return -E_FAILURE;
  }
   
  return E_SUCCESS;
}

/**
 * @brief  Compute the digest hash value
 *
 * @param hash_ctx[in]     The hash context
 * @param digest[in]       Pointer to output message digest hash
 * @param digest_len[in]   Length of the output message digest hash
 *                         buffer in bytes. Must be 20 bytes for SHA1 and
 *                         32 bytes for SHA256
 *
 * @return 0 on success, negative on failure
 *
 * @see sec_pil_hash_init
 */
int sec_pil_hash_final(sec_pil_hash_ctx *hash_ctx,
                       uint8 *digest, uint32 digest_len)
{
  CEMLIovecListType    iovec;
  CEMLIovecType        iovec_buf;
  uint8                buf[CEML_HASH_DIGEST_SIZE_SHA256];
  
  iovec.size = 1;
  iovec.iov = &iovec_buf;
  iovec.iov[0].pvBase = buf;
  iovec.iov[0].dwLen = sizeof(buf);

  if (hash_ctx == NULL || digest == NULL)
  {
    CeMLHashDeInit(&hash_ctx->ctx);
    return -E_INVALID_ARG;
  }

  if (sec_pil_check_digest_len(hash_ctx->alg, &digest_len) < 0)
  {
    CeMLHashDeInit(&hash_ctx->ctx);
    return -E_FAILURE;
  }  

  if(CeMLHashFinal(hash_ctx->ctx, &iovec) != CEML_ERROR_SUCCESS)
  {
    CeMLHashDeInit(&hash_ctx->ctx);
    return -E_FAILURE;
  }
  
  memscpy(digest,digest_len , (uint8 *)(iovec.iov[0].pvBase), digest_len);
  
  CeMLHashDeInit(&hash_ctx->ctx);
  
  return E_SUCCESS;
}

/**
 * @brief  This function returns TRUE if the peripheral image is supported
 *         or else returns FALSE.
 *
 * @param[in] proc  The peripheral image.
 *
 * @return TRUE if the proc is supported and FLASE if the proc is not supported.
 */
boolean sec_pil_is_proc_supported(sec_pil_proc_et proc)
{
  if(proc == SEC_PIL_AUTH_MODEM_PROC)
  {
    return TRUE;
  }
  else
  {
    return FALSE;
  }
}


