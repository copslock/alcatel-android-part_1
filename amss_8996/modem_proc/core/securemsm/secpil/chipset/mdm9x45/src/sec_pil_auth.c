/**
* @file sec_pil_auth.c
* @brief Secure PIL Auth Security implementation
*
* This file implements the Secure Authentication Routines,
* including signature validation and hash validation for the
* hash table segment, elf header and program header table
* segment
*
*/
/*===========================================================================
   Copyright (c) 2012-2014 by QUALCOMM Technologies, Inc.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/core.mpss/3.4.c3.11/securemsm/secpil/chipset/mdm9x45/src/sec_pil_auth.c#1 $
  $DateTime: 2016/03/28 23:02:17 $
  $Author: mplcsds1 $


when       who      what, where, why
--------   ---      ------------------------------------
04/30/14   hw       added qpsi review fixes
26/03/13   dm       Added overflow checks & error checks
05/15/12   vg       Rounded up the addresses to multiples of 4k while calling
                    mpu lock. 
05/02/12   vg       Fixed integration level issues.
04/18/12   vg       Fixed integration issues for segmented authentication.
04/05/12   vg       Added segmented authentication changes.
02/28/12   vg       Fixed review comments
02/20/11   vg       Ported from TZ PIL.
===========================================================================*/

/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include <string.h> 
#include <comdef.h>
#include <IxErrno.h>
#include "sec_pil.h"
#include "sec_pil_env.h"
#include "sec_pil_priv.h"
#include "secboot.h"
#include "secboot_hw.h"
#include "qurt_memory.h"
#include "secboot_img_util.h"

#ifdef ENG_CERT_SECBOOT_BUILD 
extern int sec_pil_auth_with_eng_cert(secboot_crypto_hash_ftbl_type *crypto_ftbl_ptr,
                               secboot_handle_type  *sec_handle_ptr,
                               secboot_image_info_type *image_info_ptr,
                               secboot_verified_info_type *verified_info_ptr,
                               uint32 sw_type,
                               uint32 code_seg);
#endif

/*----------------------------------------------------------------------------
 * Static Function Declarations
 * -------------------------------------------------------------------------*/

static secboot_ftbl_type secboot_ftbl;

static secboot_hw_ftbl_type secboot_hw_ftbl;

extern uint8 virtio_enabled;
/*----------------------------------------------------------------------------
 * Static Function Declarations
 * -------------------------------------------------------------------------*/
static int sec_pil_auth_segment(const uint8 *hash, uint32 hash_len,
                                const uint8 *seg,  uint32 seg_len);

static int sec_pil_zero_segments(const sec_pil_elf_info_t *elf);

static int sec_pil_auth_elf_headers(const sec_pil_elf_info_t *elf);

/*----------------------------------------------------------------------------
 * Static Function Definitions
 * -------------------------------------------------------------------------*/

/**
 * @brief Authenticate the ELF and Program Headers by checking
 *        that the hash of the headers matches the hash entry
 *        in the hash table segment.
 *
 * @return 0 on success, negative on failure
 *
 * @param[in] elf Pointer to the ELF header.
 */
static int sec_pil_auth_elf_headers(const sec_pil_elf_info_t *elf)
{
  int32 ret = 0;
  uint8 *hash = NULL;
#if defined SEC_PIL_WITH_CRYPTO_ENGINE
  uint32 prog_tbl_sz = 0;
#if !defined(FEATURE_MBA_VIRTIO)
  uint8 sha_digest[SEC_PIL_HASH_SZ];
#endif
#endif

  if (virtio_enabled==TRUE)
  {
	  return E_SUCCESS;
  }
  if (elf == NULL)
  {
    SEC_PIL_ERROR_CODE(AUTH_ELF_HEADERS_ELF_IS_NULL);
    return -E_FAILURE;
  }

  if (elf->prog_hdr_num == 0)
  {
    SEC_PIL_ERROR_CODE(AUTH_ELF_HEADERS_PROG_HDR_NUM_ZERO);
    return -E_FAILURE;
  }

  /* the first hash is always the hash of elf header and program header */
  hash = elf->hash_seg;

  if (hash == NULL || elf->hash_seg_sz == 0)
  {
    SEC_PIL_ERROR_CODE(AUTH_ELF_HEADERS_NULL_HASH);
    return -E_FAILURE;
  }  

  /* There is an assumption that the program headers directly
   * follow the elf header here */
#if defined SEC_PIL_WITH_CRYPTO_ENGINE
  prog_tbl_sz =
    (sizeof (Elf32_Phdr)) * (elf->prog_hdr_num);

  /* Check for integer overflow */
  if (prog_tbl_sz != (sizeof (Elf32_Phdr) * (uint64)elf->prog_hdr_num))
  {
    SEC_PIL_ERROR_CODE(AUTH_ELF_HEADERS_PROG_HDR_NUM_OVERFLOW);
    return -E_FAILURE;
  }

  /* Integer Overflow Check */
  if((sizeof(Elf32_Ehdr) + (uint64)prog_tbl_sz) > UINT32_MAX_VALUE)
  {
    SEC_PIL_ERROR_CODE(AUTH_ELF_HEADERS_PROG_HDR_NUM_OVERFLOW);
    return -E_FAILURE;
  }
  ret = sec_pil_hash(SEC_PIL_HASH_ALG,
                     (uint8*)elf->elf_hdr,
                     sizeof(Elf32_Ehdr) + ((sizeof(Elf32_Phdr) * elf->prog_hdr_num)),
                     sha_digest,
                     sizeof(sha_digest));
#endif

  if (ret < 0)
  {
    SEC_PIL_ERROR_CODE(AUTH_ELF_HEADERS_HASH_FAILED);
    return -E_FAILURE;
  }

  /* Check the hash is correct */
#if defined SEC_PIL_WITH_CRYPTO_ENGINE
  if(memcmp(hash, sha_digest, sizeof(sha_digest)) != 0)
  {
    SEC_PIL_ERROR_CODE(AUTH_ELF_HEADERS_INVALID_DIGEST);
    return -E_FAILURE;
  }
#endif
  return E_SUCCESS;
}


/**
 * @brief In all segments if memory size is greater than file size then zero out
 *        the memory area of size equal to difference between memory size and file size.
 *        In general this difference is maintained to capture ZI data.
 *
 * @note Also verify that the segment size looks correct
 *
 * @param[in] elf Pointer to the ELF segment
 *
 * @return 0 on success, negative on failure
 */
static int sec_pil_zero_segments(const sec_pil_elf_info_t *elf)
{
  uint32 i = 0;
  uint32 len = 0;
  uint32 seg_addr_end = 0;

  for (; i < elf->prog_hdr_num; ++i)
  {
    if (sec_pil_is_valid_segment(&elf->prog_hdr[i]) && (elf->prog_hdr[i].p_filesz != 0))
    {
      /* This is a corrupted image */
      if ((elf->prog_hdr[i].p_filesz > elf->prog_hdr[i].p_memsz) ||
      (UINT32_MAX_VALUE < (elf->prog_hdr[i].p_paddr + (uint64)elf->prog_hdr[i].p_memsz)))
      {
        SEC_PIL_ERROR_CODE(PIL_ZERO_SEGMENTS_SIZE_MISMATCH);
        return -E_FAILURE;
      }

      len =  elf->prog_hdr[i].p_memsz - elf->prog_hdr[i].p_filesz;
      /* Zero out the zero segments */
      if (len)
      {
		  if (virtio_enabled==FALSE)
		  {
          if ( (elf->prog_hdr[i].p_paddr + elf->prog_hdr[i].p_filesz) 
                  < (elf->prog_hdr[i].p_paddr))
          {
            SEC_PIL_ERROR_CODE(PIL_ZERO_SEGMENTS_INTEGER_OVERFLOW);
            return -E_FAILURE;
          }
        memset((void*)(elf->prog_hdr[i].p_paddr + elf->prog_hdr[i].p_filesz), 0, len);
      }
      }

      /* Zero Out alignment gap b/w previous segment  & this segment */
      if(seg_addr_end && seg_addr_end < elf->prog_hdr[i].p_paddr)
      {
		  if (virtio_enabled==FALSE)
		  {
        memset((void*)seg_addr_end, 0, elf->prog_hdr[i].p_paddr - seg_addr_end);
      }
      }
      /* skip dummy segments */
      if(elf->prog_hdr[i].p_memsz)
      {
        seg_addr_end = elf->prog_hdr[i].p_paddr + elf->prog_hdr[i].p_memsz;
      }      
    }
  }
  return E_SUCCESS;
}

/**
 * @brief verify the segment has address in increasing order. All loadable segments must be loaded by the host 
 *        in the order of increasing addresses. Memory holes are not supported.
 *
 * @param[in] ph_hdr   program header pointer
 *
 * @param[in] ph_count program section number
 *
 * @return 0 on success, negative on failure
 */
static int 
sec_pil_verify_segment_addr
(
  Elf32_Phdr *ph_hdr,
  uint32 ph_count
)
{
  uint32 i = 0;
  uint32 seg_addr = 0;

  if (NULL == ph_hdr)
  {
    SEC_PIL_ERROR_CODE(AUTH_SEGMENT_ZERO_ARGS);
    return -E_FAILURE;
  }

  for(i = 0; i < ph_count; i++)
  {
    if (sec_pil_is_valid_segment(&ph_hdr[i]) && (ph_hdr[i].p_filesz != 0))
    {
      /* segment address should be in increasing order */
      if (seg_addr < ph_hdr[i].p_paddr)
      {
        seg_addr = ph_hdr[i].p_paddr;
      }
      else
      {
        SEC_PIL_ERROR_CODE(PIL_VERIFY_SEGMENTS_INVALID_ADDRESS);
        return -E_FAILURE;
      }   
    }
  }
  return E_SUCCESS;
}


/**
 * @brief Perform a hash and verify for a segment.
 *
 * @param[in] hash Pointer to a pre-computed SHA hash
 * @param[in] hash_len The lenght in bytes of the SHA hash
 * @param[in] seg The segment to computer the hash on
 * @param[in] seg_len The length on the segment
 *
 * @return 0 on success, negative on failure
 */
static int sec_pil_auth_segment(const uint8 *hash, uint32 hash_len,
                                const uint8 *seg,  uint32 seg_len)
{
#if defined SEC_PIL_WITH_CRYPTO_ENGINE
#if !defined(FEATURE_MBA_VIRTIO)
  uint8 sha_digest[SEC_PIL_HASH_SZ];
  int ret = -E_FAILURE;
#endif
#endif

  if (TRUE==virtio_enabled)
  {
	  return E_SUCCESS;
  }

  if (hash == NULL || seg == NULL || seg_len == 0)
  {
    SEC_PIL_ERROR_CODE(AUTH_SEGMENT_ZERO_ARGS);
    return -E_INVALID_ARG;
  }

  if (hash_len != SEC_PIL_HASH_SZ)
  {
    SEC_PIL_ERROR_CODE(AUTH_SEGMENT_INVALID_HASH_LEN);
    return -E_OUT_OF_RANGE;
  }

#if defined SEC_PIL_WITH_CRYPTO_ENGINE
  ret = qurt_mem_cache_clean((qurt_addr_t)seg,seg_len,QURT_MEM_CACHE_INVALIDATE, QURT_MEM_DCACHE );

  if(ret != QURT_EOK)
      return -E_FAILURE;

  ret = sec_pil_hash(SEC_PIL_HASH_ALG,
                     seg,
                     seg_len,
                     sha_digest,
                     sizeof(sha_digest));
  if (ret != E_SUCCESS)
  {
    SEC_PIL_ERROR_CODE(AUTH_SEGMENT_HASH_FAILED);
    return -E_FAILURE;
  }

  /** Check the hash is correct */
  if(memcmp(hash, sha_digest, sizeof(sha_digest)) != 0)
  {
    SEC_PIL_ERROR_CODE(AUTH_SEGMENT_INVALID_DIGEST);
    return -E_FAILURE;
  }
#endif

  return E_SUCCESS;
}

/**
 * @brief Update hash for the length loaded and if it completes one full 
          ELF program segment then compare the hash against its entry in the hash table.
 *
 * @param[in,out] len_tb_auth Pointer to the length of the image loaded.
 * @param[in]     pil         Pointer to Pil internal data
 * @param[in,out] seg_info    Pointer to segments auth internal data.
 *
 * @return 0 on success, negative on failure 
 */

static int sec_pil_update_hash_for_length_loaded(uint32 *len_tb_auth,
                                                 const sec_pil_priv_t *pil,
                                                 sec_pil_seg_auth_info_t *seg_info)
{
#if defined SEC_PIL_WITH_CRYPTO_ENGINE
#if !defined(FEATURE_MBA_VIRTIO)
  static sec_pil_hash_ctx ctx;
  int ret = E_SUCCESS;  
  uint8 sha_digest[SEC_PIL_HASH_SZ];  
#endif
#endif
  const sec_pil_elf_info_t *elf = &(pil->elf);  
  uint32 len_tb_update = 0;
  uint32 sz_left = 0;


  /* Check for current ELF program segment is hash initialised or not */
#if defined SEC_PIL_WITH_CRYPTO_ENGINE
  if( (seg_info->cur_elf_len == 0) && 
      ((ret = sec_pil_hash_init(SEC_PIL_HASH_ALG, &ctx)) != E_SUCCESS) )
  {    
    SEC_PIL_ERROR_CODE(PIL_UPDATE_HASH_HASH_INIT_FAILED);
    return -E_FAILURE;    
  }
#endif
  /* Integer Underflow check for safe side */
  if(seg_info->cur_elf_len > elf->prog_hdr[seg_info->cur_prog_hdr].p_filesz)
  {    
    SEC_PIL_ERROR_CODE(PIL_UPDATE_ELF_LEN_EXCEED_FILE_SZ);
    return -E_FAILURE;    
  }

  sz_left = elf->prog_hdr[seg_info->cur_prog_hdr].p_filesz - 
            seg_info->cur_elf_len;
  
  if(*len_tb_auth >= sz_left)
  {
    len_tb_update = sz_left;
  }
  else
  {
    len_tb_update = *len_tb_auth;
  }

#if defined SEC_PIL_WITH_CRYPTO_ENGINE
  ret = qurt_mem_cache_clean( (qurt_addr_t)seg_info->read_ptr, len_tb_update, 
                                   QURT_MEM_CACHE_INVALIDATE, QURT_MEM_DCACHE );
  if(ret != QURT_EOK)
      return -E_FAILURE;

  if ((ret = sec_pil_hash_update(&ctx, seg_info->read_ptr, len_tb_update)) != E_SUCCESS)
  {
    SEC_PIL_ERROR_CODE(PIL_UPDATE_HASH_HASH_UPDATE_FAILED);
    return -E_FAILURE;
  }
#endif

  seg_info->cur_elf_len += len_tb_update;
  *len_tb_auth -= len_tb_update;
  seg_info->read_ptr += len_tb_update;
  seg_info->len_read += len_tb_update;
  
  if(seg_info->cur_elf_len == elf->prog_hdr[seg_info->cur_prog_hdr].p_filesz)
  {
#if defined SEC_PIL_WITH_CRYPTO_ENGINE
  if ((ret = sec_pil_hash_final(&ctx, sha_digest, sizeof(sha_digest))) != E_SUCCESS)
    {
      SEC_PIL_ERROR_CODE(PIL_UPDATE_HASH_HASH_FINAL_FAILED);
      return -E_FAILURE;
    }
    
    /* Check the hash is correct */
    if(memcmp(seg_info->hash, sha_digest, sizeof(sha_digest)) != 0)
    {
      SEC_PIL_ERROR_CODE(PIL_UPDATE_HASH_INVALID_DIGEST);
      return -E_FAILURE;
    }          
#endif
}    
  return E_SUCCESS;
}

/*----------------------------------------------------------------------------
 * Function Defintions 
 * -------------------------------------------------------------------------*/
 
/**
 * @brief Verify the hash on all loadable program segments
 *
 * @precondition this function is called in both secure and non-secure boot. In secure boot, signature in hash
 *               segment and the elf hdr is verified; while in non-secure boot, it does not authenticate
 *               hash segment before calling into this function for segment verification.
 *
 * @param[in] proc  The peripheral image
 * @param[in] elf ELF header of the loaded image
 *
 * @return 0 on success, negative on failure
 */
int sec_pil_auth_segments(sec_pil_proc_et proc,
                          const sec_pil_elf_info_t *elf)
{
  uint32 i          = 0;
  uint8 *hash       = NULL;
  int  ret        = 0;

  if((elf == NULL) || (NULL == elf->prog_hdr) || (elf->prog_hdr_num == 0))
  {
    SEC_PIL_ERROR_CODE(PIL_AUTH_SEGMENTS_NULL_ELF);
    return -E_FAILURE;
  }

  if ((elf->hash_seg == NULL) || (elf->hash_seg_sz == 0))
  {
    SEC_PIL_ERROR_CODE(PIL_AUTH_SEGMENTS_HASH_NULL);
    return -E_FAILURE;
  }

  hash = elf->hash_seg;

  /* Skip first hash.
   * This was already checked in signature validation */  
  hash += SEC_PIL_HASH_SZ;

  if (sec_pil_lock_xpu(proc, elf->prog_hdr,
                       elf->prog_hdr_num, elf->elf_hdr->e_entry) != E_SUCCESS)
  {
    SEC_PIL_ERROR_CODE(PIL_AUTH_SEGMENTS_LOCK_XPU_FAILED);
    return -E_FAILURE;
  }

  if (sec_pil_verify_segment_addr(elf->prog_hdr, elf->prog_hdr_num) != E_SUCCESS)
  {
    SEC_PIL_ERROR_CODE(PIL_VERIFY_SEGMENTS_INVALID_ADDRESS);
    return -E_FAILURE;
  }

  if (sec_pil_zero_segments(elf) != E_SUCCESS)
  {
    SEC_PIL_ERROR_CODE(PIL_AUTH_SEGMENTS_ZERO_SEGMENTS_FAILED);
    return -E_FAILURE;
  }

  /* Skip first hash.
   * This was already checked in signature validation */ 
  for ( i = 1; i < elf->prog_hdr_num; ++i)
  {
    if (sec_pil_is_valid_segment(&elf->prog_hdr[i]) &&
        elf->prog_hdr[i].p_filesz != 0)
    {
      ret = sec_pil_auth_segment(hash, SEC_PIL_HASH_SZ,
                               (uint8*)elf->prog_hdr[i].p_paddr,
                               elf->prog_hdr[i].p_filesz);

      if (ret != E_SUCCESS)
      {
        SEC_PIL_ERROR_CODE(PIL_AUTH_SEGMENTS_SEGMENT_AUTH_FAILED);
        sec_pil_unlock_xpu(proc);
        return -E_FAILURE;
      }
    }
    hash += SEC_PIL_HASH_SZ;  
  }
  return E_SUCCESS;
}

/**
 * @brief This function checks if the image associated with the code segment
 *        needs to be authenticated. If authentication is required, callers
 *        MUST authenticate the image successfully before allowing it to execute.
 *
 * @param[in]     code_segment Code segment in SECURE_BOOTn register containing 
 *                             authentication information of the image.
 * @param[in,out] auth_enabled Pointer to a uint32 indicating whether authentication 
 *                             is required.Will be populated to 0 if authentication 
 *                             is not required,1 if authentication is required.
 *                 
 * @return 0 on success, negative on failure
 */

uint32 sec_pil_hw_is_auth_enabled(uint32 code_segment, uint32 *auth_enabled)
{
  uint32 ret = E_SUCCESS;

  if(secboot_hw_ftbl.secboot_hw_is_auth_enabled != NULL)
  {  
    ret = secboot_hw_ftbl.secboot_hw_is_auth_enabled(code_segment,auth_enabled);
  
    if(ret != E_SUCCESS)
    {
      SEC_PIL_ERROR_CODE(PIL_HW_IS_AUTH_ENBALED_FAILED);
      return -E_FAILURE;
    }
  }
  else
  {
      SEC_PIL_ERROR_CODE(PIL_HW_IS_AUTH_ENBALED_NULL_SECBOOT_ROUTINES);
      return -E_FAILURE;
  }
  
  return E_SUCCESS;
}

/**
 * @brief This function verifies hash table segment signature and validates
 *        ELF and program headers hash against thier entry in hash table.          
 *
 * @param[in] proc The peripheral image 
 * @param[in] elf  Pointer to the ELF info of the loaded image.
 *
 * @return 0 on success, negative on failure
 */

int sec_pil_verify_sig(sec_pil_proc_et proc,
                       const sec_pil_elf_info_t *elf,
                       sec_pil_verified_info_t *v_info)
{
#ifdef ENG_CERT_SECBOOT_BUILD
  secboot_handle_type  sec_handle;
#endif
  uint32 auth_enabled = TRUE;
  uint32 code_seg = 0;
  secboot_error_type   sec_err;  
  secboot_image_info_type image_info;
  secboot_verified_info_type verified_info;
  uint32 resource = 0;
  uint32 start = 0;
  uint32 end = 0;
  uint32 sw_version = 0;
  uint32 sw_id = 0;
  secboot_crypto_hash_ftbl_type crypto_ftbl;
  
  if(elf == NULL)
  {
    SEC_PIL_ERROR_CODE(PIL_VERIFY_SIG_NULL_ELF);
    return -E_FAILURE;
  }

  if((secboot_ftbl.secboot_init == NULL) ||
     (secboot_ftbl.secboot_authenticate == NULL) ||
     (secboot_ftbl.secboot_deinit == NULL))
  {
    SEC_PIL_ERROR_CODE(PIL_VERIFY_SIG_NULL_SECBOOT_ROUTINES);
    return -E_FAILURE;
  }

  if(sec_pil_get_code_segment(proc,&code_seg) != E_SUCCESS)
  {
    SEC_PIL_ERROR_CODE(PIL_GET_CODE_SEGMENT_FAILED);
    return -E_FAILURE;
  }
  
#ifndef FEATURE_NON_SECURE_IMAGE_AUTH
  if(sec_pil_hw_is_auth_enabled(code_seg,&auth_enabled) != E_SUCCESS)
  {
    SEC_PIL_ERROR_CODE(PIL_VERIFY_SIG_HW_IS_AUTH_ENABLED_FAILED);
    return -E_FAILURE;
  }
#endif

  if(auth_enabled)
  {
  crypto_ftbl.CeMLDeInit = CeMLDeInit;
  crypto_ftbl.CeMLHashAtomic = CeMLHashAtomic;
  crypto_ftbl.CeMLHashDeInit = CeMLHashDeInit;
  crypto_ftbl.CeMLHashFinal = CeMLHashFinal;
  crypto_ftbl.CeMLHashInit = CeMLHashInit;
  crypto_ftbl.CeMLHashUpdate = CeMLHashUpdate;
  crypto_ftbl.CeMLInit = CeMLInit;

  if(sec_pil_get_sw_version(proc,&sw_version) != E_SUCCESS)
  {
    SEC_PIL_ERROR_CODE(PIL_SECBOOT_GET_SW_VERSION_FAILED);
    return -E_FAILURE;
  }

  if(sec_pil_get_sw_id(proc,&sw_id) != E_SUCCESS)
  {
    SEC_PIL_ERROR_CODE(PIL_SECBOOT_GET_SW_ID_FAILED);
    return -E_FAILURE;
  }

  image_info.header_ptr_1 = elf->hash_seg_hdr;
  image_info.header_len_1 = elf->hash_seg_hdr_sz;
  image_info.code_ptr_1= elf->hash_seg;
  image_info.code_len_1 = elf->hash_seg_sz;
  image_info.signature_ptr = elf->sig_ptr;
  image_info.signature_len = elf->sig_sz;
  image_info.sw_type = sw_id;
  image_info.sw_version = sw_version;
  image_info.x509_chain_ptr = elf->cert_ptr;
  image_info.x509_chain_len = elf->cert_sz; 

  /* Lock the signature and cert area - this protects against time of use variables */
  /* Signature and certificates are consecutive in memory */
  start = SEC_PIL_FLOOR_4K((uint32)elf->sig_ptr);
  end = SEC_PIL_ROUNDUP(uint32,((uint32)elf->sig_ptr + \
            (uint32)elf->sig_sz + (uint32)elf->cert_sz),SEC_PIL_PAGE_4K) ;

    if(end <= start)
    {
      SEC_PIL_ERROR_CODE(PIL_SECBOOT_INVALID_END_ADDR);
      return -E_FAILURE;
    }
    
  sec_pil_dcache_inval_region((void *) start, (end - start));

  if (sec_pil_mpu_lock_area(&resource,start,end) != E_SUCCESS)
  {
    SEC_PIL_ERROR_CODE(PIL_VERIFY_SIG_MPU_LOCK_FAILED);
    return -E_FAILURE;
  }  
  
  sec_err = secboot_authenticate_image( 
                                                  code_seg, 
                                                  &image_info,
              &verified_info,
              &secboot_ftbl,
              &secboot_hw_ftbl,
              &crypto_ftbl
            );
#ifndef ENG_CERT_SECBOOT_BUILD
    if ( E_SECBOOT_SUCCESS != sec_err )
    {
      sec_pil_mpu_unlock_area(resource);
    SEC_PIL_ERROR_CODE(PIL_SECBOOT_AUTHENTICATE_FAILED);
    return -E_FAILURE;
  }
#else /* ENG_CERT_SECBOOT_BUILD */
  /* De-Init Sec handle / Crypto context after root cert based auth complete 
   * Don't modify sec_err, since that holds the previous auth status.
   */
   if ( E_SECBOOT_SUCCESS != secboot_ftbl.secboot_init(&crypto_ftbl, &sec_handle) )
  {
    SEC_PIL_ERROR_CODE(PIL_SECBOOT_INIT_FAILED);
    return -E_FAILURE;
  }
  
  /* Fall back to eng cert based authentication since root cert authentication fails */
  if ( E_SECBOOT_SUCCESS != sec_err )
  {
    if(E_SUCCESS != sec_pil_auth_with_eng_cert(&crypto_ftbl, &sec_handle,
                       &image_info, &verified_info, sw_id, code_seg))
    {
      sec_pil_mpu_unlock_area(resource);
      SEC_PIL_ERROR_CODE(PIL_SECBOOT_AUTHENTICATE_FAILED);
      return -E_FAILURE;
    }
  }
#endif /* ENG_CERT_SECBOOT_BUILD */
  
    v_info->sw_id = verified_info.sw_id;
    v_info->enable_debug = verified_info.enable_debug;
  
  
    if(sec_pil_mpu_unlock_area(resource) != E_SUCCESS)
    {
      SEC_PIL_ERROR_CODE(PIL_VERIFY_SIG_MPU_UNLOCK_FAILED);
      return -E_FAILURE;
    }  
  }
  
  // check the elf header for signed image
  if (sec_pil_auth_elf_headers(elf) < 0)
  {
    SEC_PIL_ERROR_CODE(PIL_VERIFY_SIG_INVALID_ELF_HEADERS);
    return -E_FAILURE;
  }

  return E_SUCCESS;  
}

/**
 * @brief This function updates the internal function table pointers for PBL 
 *        secboot routines.
 *
 * @return 0 on success, 1 on failure
 */

uint32 sec_pil_get_secboot_ftbl(void)
{  
  #if defined SEC_PIL_WITH_SECBOOT_LIB  
  if(secboot_get_ftbl(&secboot_ftbl) != E_SECBOOT_SUCCESS)
  {
    SEC_PIL_ERROR_CODE(SEC_PIL_GET_SECBOOT_FTBL_FAILED);
    return -E_FAILURE;
  }
  
  if(secboot_hw_get_ftbl(&secboot_hw_ftbl) != E_SECBOOT_HW_SUCCESS)
  {
    SEC_PIL_ERROR_CODE(SEC_PIL_GET_SECBOOT_HW_FTBL_FAILED);
    return -E_FAILURE;
  }
  
  #else
  if(sec_pil_get_secboot_routines(&secboot_ftbl, &secboot_hw_ftbl) != E_SUCCESS)
  {
    SEC_PIL_ERROR_CODE(SEC_PIL_GET_SECBOOT_FTBL_FAILED);
    return -E_FAILURE;
  }
  #endif
  
  return E_SUCCESS;  
}


/**
 * @brief This funnction initialises segments auth internal data structure.          
 *
 * @param[in]     proc             The peripheral image
 * @param[in]     image_start_addr Start address of the image loaded on to RAM.
 * @param[in]     pil              Pointer to Pil internal context data
 * @param[in,out] seg_info         Pointer to segments auth internal context data.
 *
 * @return 0 on success, negative on failure 
 */
int sec_pil_seg_auth_init(sec_pil_proc_et proc,
                          uint32 image_start_addr,
                          const sec_pil_priv_t *pil,
                          sec_pil_seg_auth_info_t *seg_info)
{
  const sec_pil_elf_info_t *elf = &(pil->elf);
  
  if(!(seg_info->init))
  {    
    seg_info->hash = elf->hash_seg;
    seg_info->hash_len = elf->hash_seg_sz;

    if (seg_info->hash == NULL || seg_info->hash_len == 0)
    {
      SEC_PIL_ERROR_CODE(PIL_AUTH_SEG_AUTH_INIT_NULL);
      return -E_FAILURE;
    }
  
    /* Skip first hash.
     * This was already checked in signature validation */  
    seg_info->hash += SEC_PIL_HASH_SZ;
    seg_info->hash_len -= SEC_PIL_HASH_SZ;
    seg_info->cur_prog_hdr = 1;

    /* Initialise the read pointer */
    seg_info->read_ptr = (uint8 *)image_start_addr;
    
    seg_info->init = TRUE;
  }

  return E_SUCCESS;
}

/**
 * @brief This function updates the hash for the length loaded and compares 
 *        the hash against their entries in the hash table.Also tracks the elf 
 *        segments loaded so far.
 *
 * @param[in]     proc         The peripheral image
 * @param[in]     len_tb_auth  Length of the image loaded 
 * @param[in]     pil          Pointer to Pil internal data
 * @param[in,out] seg_info     Pointer to segments auth internal data.
 *
 * @return 0 on success, negative on failure 
 */
int sec_pil_auth_partial_segments(sec_pil_proc_et proc,
                                  uint32 len_tb_auth,
                                  const sec_pil_priv_t *pil,
                                  sec_pil_seg_auth_info_t *seg_info)
{  
  int ret = E_SUCCESS;  
  const sec_pil_elf_info_t *elf = &(pil->elf);  
  uint32 sz_left = 0;
  uint32 len_to_clear = 0;
  uint32 align_gap = 0;  
  uint8* hash_table_start = seg_info->hash;
  uint8* hash_table_end = (seg_info->hash + seg_info->hash_len);  
  
  while(len_tb_auth)
  {
    /* Move cur_prog_hdr to the start of loadable segments.
     Skip the unloadable segments and hash table segment.*/
    for (; seg_info->cur_prog_hdr < elf->prog_hdr_num; ++(seg_info->cur_prog_hdr))
    {
      /* For each segment hash segment need to be present. */
      if (seg_info->hash_len < SEC_PIL_HASH_SZ)
      {      
        SEC_PIL_ERROR_CODE(PIL_AUTH_PARTIAL_SEGMENTS_INVALID_HASH_LEN);
        return -E_FAILURE;
      }    
      if((sec_pil_is_valid_segment(&elf->prog_hdr[seg_info->cur_prog_hdr])) 
          && elf->prog_hdr[seg_info->cur_prog_hdr].p_memsz )
      {
        break;
      }

      /* moving to the next hash value. */
      seg_info->hash += SEC_PIL_HASH_SZ;
      seg_info->hash_len -= SEC_PIL_HASH_SZ;
    }

    if(seg_info->cur_prog_hdr >= elf->prog_hdr_num) 
    {
       /* Something went wrong as all the ELF segments are validated */
       SEC_PIL_ERROR_CODE(PIL_AUTH_PARTIAL_SEGMENTS_FAILED);
       return -E_FAILURE;
    }

    /* There are gaps between segments. The read_ptr points to the end of
       comsumed memory (memory that is already verified) for the segment.
       Find the gaps when the end (read_ptr) of consumed memory in  previous 
       segment is smaller than the start of current segment, and zero it out. 
       (NOTE: All loadable segments must be loaded by the host in the order of
       increasing addresses. holes are not supported - ref:80-N8804-1_C)
    */

    if(seg_info->read_ptr && (seg_info->read_ptr <
       (uint8*)(elf->prog_hdr[seg_info->cur_prog_hdr].p_paddr)))
    {
      /* Zero out alignment gaps between segments */          
      align_gap = elf->prog_hdr[seg_info->cur_prog_hdr].p_paddr 
                  - (uint32)seg_info->read_ptr; 
	  if (FALSE==virtio_enabled)
	  {
      memset(seg_info->read_ptr, 0, align_gap);
	  }
         
      /* Move cursor to start of new segment */
      seg_info->read_ptr = (uint8*)elf->prog_hdr[seg_info->cur_prog_hdr].p_paddr;
    }

  
    if (elf->prog_hdr[seg_info->cur_prog_hdr].p_filesz != 0)
    {
      /* Do hash update for length loaded so far and if one ELF program segment 
         load is completed then compare its hash against its entry in the hash table */
      ret = sec_pil_update_hash_for_length_loaded(&len_tb_auth,
                                                  pil, seg_info);

      if(ret != E_SUCCESS)
      {
        SEC_PIL_ERROR_CODE(PIL_UPDATE_HASH_LENGTH_LOADED_FAILED);
        return -E_FAILURE;
      }
    }
    
    /* clear the ZI memory within the segment, when the memsz is larger than the file
      size, and take the ZI memory size out of the length of segment to be auth'ed. */
    if(elf->prog_hdr[seg_info->cur_prog_hdr].p_memsz != 0)
    {
      if(elf->prog_hdr[seg_info->cur_prog_hdr].p_memsz < seg_info->cur_elf_len)
      {
        /* Data corruption or something went wrong */
        SEC_PIL_ERROR_CODE(PIL_AUTH_PARTIAL_SEGMENTS_DATA_CORRUPTION);
        return -E_FAILURE;
      }
      if(seg_info->cur_elf_len >= elf->prog_hdr[seg_info->cur_prog_hdr].p_filesz)
        len_to_clear = elf->prog_hdr[seg_info->cur_prog_hdr].p_memsz 
                       - seg_info->cur_elf_len;
      sz_left = len_to_clear > len_tb_auth ? len_tb_auth : len_to_clear;
      if(seg_info->read_ptr && sz_left)
      {
        /* Zero out all this data. */        
        if (FALSE==virtio_enabled)
        {
        memset(seg_info->read_ptr, 0, sz_left);
        }

        len_tb_auth -= sz_left;
        if(sz_left > (UINT32_MAX_VALUE - seg_info->len_read) || 
           sz_left > (UINT32_MAX_VALUE - (uint32)seg_info->read_ptr))
        {
          /* Integer Overflow */
          SEC_PIL_ERROR_CODE(PIL_AUTH_PARTIAL_SEGMENTS_INTEGER_OVERFLOW);
          return -E_FAILURE;
        }
        seg_info->len_read += sz_left;
        seg_info->read_ptr += sz_left;
        seg_info->cur_elf_len += sz_left;
      }
      len_to_clear = sz_left = 0;
    }

    if(seg_info->cur_elf_len == elf->prog_hdr[seg_info->cur_prog_hdr].p_memsz)
    {
      /* Completed One segment verification */    
      seg_info->hash += SEC_PIL_HASH_SZ;
      if ( ((uint32)seg_info->hash >=  (uint32)hash_table_end) 
             && ((uint32)seg_info->hash < (uint32)hash_table_start) )
      {
        SEC_PIL_ERROR_CODE(PIL_AUTH_PARTIAL_SEGMENTS_INTEGER_OVERFLOW);
        return -E_FAILURE;
      } 

      if ( seg_info->hash_len < (seg_info->hash_len - SEC_PIL_HASH_SZ) )
      {
        SEC_PIL_ERROR_CODE(PIL_AUTH_PARTIAL_SEGMENTS_INTEGER_OVERFLOW);
        return -E_FAILURE;
      }
      seg_info->hash_len -= SEC_PIL_HASH_SZ;    

      seg_info->cur_prog_hdr++;
      seg_info->cur_elf_len = 0;        
    }
  }

  return E_SUCCESS;

}
