/**
* @file sec_pil_priv.c
* @brief Internal functions for secure PIL.
*
*/

/*===========================================================================
   Copyright (c) 2012-2014 by QUALCOMM Technologies, Inc.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/core.mpss/3.4.c3.11/securemsm/secpil/chipset/mdm9x45/src/sec_pil_priv.c#1 $
  $DateTime: 2016/03/28 23:02:17 $
  $Author: mplcsds1 $


when       who      what, where, why
--------   ---      ------------------------------------
04/30/14   hw       added qpsi review fixes
26/03/13   dm       Added overflow checks & error checks
04/05/12   vg       Added segmented authentication changes.
02/28/12   vg       Fixed review comments
02/20/12   vg       Ported from TZ PIL.
===========================================================================*/

/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include <string.h>
#include <stringl.h>
#include <IxErrno.h>
#include "sec_pil.h"
#include "sec_pil_env.h"
#include "sec_pil_priv.h"
#include "sec_pil_auth.h"


/*----------------------------------------------------------------------------
 * Static Function Definitions
 * -------------------------------------------------------------------------*/

/**
 * @brief Verify if the boot header has the correct lengths
 *
 * @note Also verify that the proc is of the supported type
 *
 * @param[in] proc Peripheral image type
 * @param[in] mi Pointer to the header of the hash segment
 *
 * @return \c TRUE if the boot header lengths are correct, otherwise \c FALSE.
 */
static boolean sec_pil_is_mi_boot_valid(sec_pil_proc_et proc,
                                        const mi_boot_image_header_type *mi)
{
  uint32 total_len = 0;

  if (!sec_pil_is_proc_supported(proc))
  {
    SEC_PIL_ERROR_CODE(MI_BOOT_VALID_UNSUPPORTED_PROC);
    return FALSE;
  }

  if (mi == NULL)
  {
    SEC_PIL_ERROR_CODE(MI_BOOT_VALID_MI_NULL);
    return FALSE;
  }

  if (mi->code_size == 0 || mi->cert_chain_size == 0 ||
      mi->signature_size == 0)
  {
    SEC_PIL_ERROR_CODE(MI_BOOT_VALID_ZERO_SIZE);
    return FALSE;
  }

  /* Verify the image_size is equal to total of code_size, cert_chain_size and
   * signature_size. We may have added 1 to 3 bytes to the image if the total
   * image size is not multiple of four. We should account for that also.
   */

  total_len = mi->code_size;
  total_len += mi->cert_chain_size;
  if(total_len < mi->cert_chain_size)
  {
    SEC_PIL_ERROR_CODE(MI_BOOT_VALID_INTEGER_OVERFLOW1);
    return FALSE;
  }
  total_len += mi->signature_size;
  if(total_len < mi->signature_size)
  {
    SEC_PIL_ERROR_CODE(MI_BOOT_VALID_INTEGER_OVERFLOW2);
    return FALSE;
  }
  total_len = SEC_PIL_ROUNDUP(uint32, total_len, 4);

  if (total_len != mi->image_size)
  {
    SEC_PIL_ERROR_CODE(MI_BOOT_VALID_LENGTH_MISMATCH);
    return FALSE;
  }

  return TRUE;
}

/*----------------------------------------------------------------------------
 * Function Definitions 
 * -------------------------------------------------------------------------*/

/**
 * @brief This funnction  frees the memory if any and zeros out data
 *        present in the data structures for pil and seg auth info.
 *
 * @param[in,out]  pil       Pointer to Pil internal context data
 * @param[in,out]  seg_info  Pointer to segmented authentication internal 
 *                           context data. 
 *
 * @return E_SUCCESS on success and E_INVALID_ARG for invalid args.
 */
void sec_pil_clean_pil_priv(sec_pil_priv_t *pil,
                            sec_pil_seg_auth_info_t *seg_info)
{
  if (pil->elf.elf_hdr != NULL)
  {
    SEC_PIL_FREE(pil->elf.elf_hdr);
  }

  memset(pil, 0, sizeof(sec_pil_priv_t));
  memset(seg_info, 0, sizeof(sec_pil_seg_auth_info_t));
  pil->state = SEC_PIL_STATE_INIT;
}

/**
 * @brief Elf header parser, to check for the correct format.
 *
 * @param[in] elf_hdr   Pointer to the ELF header.
 *
 * @result \c TRUE if the header is in correct format, \c FALSE otherwise.
 */

boolean sec_pil_is_elf(const Elf32_Ehdr * elf_hdr)
{
  if (elf_hdr->e_ident[ELFINFO_MAG0_INDEX] != ELFINFO_MAG0
      || elf_hdr->e_ident[ELFINFO_MAG1_INDEX] != ELFINFO_MAG1
      || elf_hdr->e_ident[ELFINFO_MAG2_INDEX] != ELFINFO_MAG2
      || elf_hdr->e_ident[ELFINFO_MAG3_INDEX] != ELFINFO_MAG3)
  {
    SEC_PIL_ERROR_CODE(PIL_IS_ELF_MAGIC_FAILED);
    return FALSE;
  }

  if (elf_hdr->e_ident[ELFINFO_CLASS_INDEX] != ELF_CLASS_32)
  {
    SEC_PIL_ERROR_CODE(PIL_IS_ELF_INVALID_CLASS);
    return FALSE;
  }

  if (elf_hdr->e_ident[ELFINFO_VERSION_INDEX] != ELF_VERSION_CURRENT)
  {
    SEC_PIL_ERROR_CODE(PIL_IS_ELF_INVALID_VERSION);
    return FALSE;
  }

  if (elf_hdr->e_ehsize != sizeof(Elf32_Ehdr))
  {
    SEC_PIL_ERROR_CODE(PIL_IS_ELF_INVALID_EHSIZE);
    return FALSE;
  }

  if ((elf_hdr->e_phentsize) != (sizeof(Elf32_Phdr)))
  {
    SEC_PIL_ERROR_CODE(PIL_IS_ELF_INVALID_PHENTSIZE);
    return FALSE;
  }

  return TRUE;
}

/**
 * @brief Populate the PIL info given the provided ELF image.
 *
 * @param[in] proc Peripheral image type
 * @param[in] elf_hdr Pointer to the elf header
 * @param[in] pil Elf header info struct
 * @postcondition This routine allocates memory within \c sec_pil_elf_info_t. So
 *                the caller must free this memory when it is no longer needed.
 *
 * @return 0 on success, negative on failure.
 */
int sec_pil_populate_elf_info(sec_pil_proc_et proc,
                              const Elf32_Ehdr * elf_hdr,
                              sec_pil_elf_info_t *pil)
{
  Elf32_Phdr *prog_hdr = NULL;
  mi_boot_image_header_type   *mi_boot_hdr = NULL;
  uint8* tmp = 0;

  /* Force the compiler to use stack variables. We need to be careful and make
     sure that sizes are read locally so the HLOS cannot interrupt TZ and
     re-write sizes under us.
   */
  volatile uint32 elf_hdr_sz = 0;
  volatile uint32 prog_tbl_sz = 0;
  volatile uint32 hash_seg_sz =0;
  volatile uint32 image_hdr_size = 0;
  volatile uint32 hash_seg_addr = 0;
  volatile uint32 hash_seg_hdr_addr = 0;
  volatile uint32 hash_seg_hdr_sz = 0;
  
  uint32 auth_enabled = TRUE;
  uint32 code_seg = 0;

  elf_hdr_sz = (uint32) sizeof(Elf32_Ehdr);
  prog_tbl_sz = (uint32)(sizeof(Elf32_Phdr)) * (elf_hdr->e_phnum);
  /* Check for integer overflow */
  if (prog_tbl_sz < (sizeof(Elf32_Phdr)*(uint64) elf_hdr->e_phnum))
  {
    SEC_PIL_ERROR_CODE(POPULATE_ELF_PROG_TBL_TOO_BIG);
    return -E_FAILURE;
  }
  prog_hdr = (Elf32_Phdr*)((uint8*)elf_hdr + sizeof(Elf32_Ehdr));

  /* Size of the hash table segment - Find the program header for
   * the segment of type MI_PBT_HASH_SEGMENT, and get the size
   * from that entry
   */

  if (sec_pil_auth_validate_hash_segment_entries(prog_hdr,
                           (Elf32_Phdr*)((uint8*)prog_hdr + prog_tbl_sz),
                           MI_PBT_HASH_SEGMENT) != E_SUCCESS)
  {
    SEC_PIL_ERROR_CODE(POPULATE_ELF_INCORRECT_HASH_TABLE_SEGMENTS);
    return -E_FAILURE;
  }

  hash_seg_hdr_addr = (uint32)prog_hdr + prog_tbl_sz;
  hash_seg_hdr_sz = sizeof(mi_boot_image_header_type);
  hash_seg_addr = (uint32)hash_seg_hdr_addr + sizeof(mi_boot_image_header_type);
  /* The filesize includes the certificate chain. Do not include
     this in the memory copy */
    
  hash_seg_sz = elf_hdr->e_phnum * SEC_PIL_HASH_SZ;
  /* Check for integer overflow */
  if (hash_seg_sz < (SEC_PIL_HASH_SZ * (uint64) elf_hdr->e_phnum))
  {
    SEC_PIL_ERROR_CODE(POPULATE_ELF_HASH_SEG_TOO_BIG);
    return -E_FAILURE;
  }

  /* ELF header must be fully in non-secure memory. */
  if (!sec_pil_is_ns_range((uint8*)elf_hdr, elf_hdr_sz))
  {
    SEC_PIL_ERROR_CODE(POPULATE_ELF_ELF_HDR_NOT_IN_NS_MEMORY);
    return -E_BAD_ADDRESS;
  }

  /* Program header must be fully in non-secure memory. */
  if (!sec_pil_is_ns_range((uint8*)prog_hdr, prog_tbl_sz))
  {
    SEC_PIL_ERROR_CODE(POPULATE_ELF_PROG_HDR_NOT_IN_NS_MEMORY);
    return -E_BAD_ADDRESS;
  }

  /* Hash segment must be fully in non-secure memory. */
    if (!sec_pil_is_ns_range((uint8*)hash_seg_hdr_addr, hash_seg_hdr_sz))
  {
    SEC_PIL_ERROR_CODE(POPULATE_ELF_HASH_SEG_NOT_IN_NS_MEMORY);
    return -E_BAD_ADDRESS;
  }
    
  if (!sec_pil_is_ns_range((uint8*)hash_seg_addr, hash_seg_sz))
  {
    SEC_PIL_ERROR_CODE(POPULATE_ELF_HASH_SEG_NOT_IN_NS_MEMORY);
    return -E_BAD_ADDRESS;
  }

  /* Total copy size is Elf header + array of program headers + hash segment. */
  if((elf_hdr_sz > SEC_PIL_PAGE_4K) 
     || (prog_tbl_sz > SEC_PIL_PAGE_4K)
     || (hash_seg_hdr_sz > SEC_PIL_PAGE_4K)
     || (hash_seg_sz > SEC_PIL_PAGE_4K))
  {
    SEC_PIL_ERROR_CODE(POPULATE_ELF_HDR_TOO_BIG);
    return -E_FAILURE;
  }
  
  image_hdr_size = elf_hdr_sz + prog_tbl_sz + hash_seg_hdr_sz + hash_seg_sz;

  if (image_hdr_size > SEC_PIL_PAGE_4K)
  {
    SEC_PIL_ERROR_CODE(POPULATE_ELF_IMAGE_HDR_TOO_BIG);
    return -E_FAILURE;
  }

  if(pil->elf_hdr != NULL 
     || pil->prog_hdr != NULL 
     || pil->hash_seg_hdr != NULL
     || pil->hash_seg != NULL)

  {
    SEC_PIL_ERROR_CODE(POPULATE_ELF_PREVIOUS_MALLOC_IS_NOT_FREED);
    return -E_FAILURE;
  }
  
  tmp  = (uint8 *)SEC_PIL_MALLOC(image_hdr_size);
  
  /* Allocate memory of the size of image header within TZ */
  if (NULL == tmp)
  {
    SEC_PIL_ERROR_CODE(POPULATE_ELF_MALLOC_FAILED);
    return -E_FAILURE;
  }

  pil->elf_hdr = (Elf32_Ehdr*)tmp;
  memscpy(pil->elf_hdr, elf_hdr_sz, elf_hdr, elf_hdr_sz);
  tmp += elf_hdr_sz;
  pil->prog_hdr = (Elf32_Phdr*)tmp;
  memscpy(pil->prog_hdr, prog_tbl_sz, prog_hdr, prog_tbl_sz);
  tmp += prog_tbl_sz;
  pil->hash_seg_hdr = tmp;
  memscpy(pil->hash_seg_hdr, hash_seg_hdr_sz, (uint8*)hash_seg_hdr_addr, hash_seg_hdr_sz);
  tmp += hash_seg_hdr_sz;
  pil->hash_seg_hdr_sz = hash_seg_hdr_sz;
  pil->hash_seg = tmp;
  memscpy(pil->hash_seg, hash_seg_sz, (uint8*)hash_seg_addr, hash_seg_sz);
  pil->hash_seg_sz = hash_seg_sz;
  pil->prog_hdr_num = pil->elf_hdr->e_phnum;
  

  if(sec_pil_get_code_segment(proc,&code_seg) != E_SUCCESS)
  {
    SEC_PIL_ERROR_CODE(POPULATE_ELF_GET_CODE_SEGMENT_FAILED);
    return -E_FAILURE;
  }

#ifndef FEATURE_NON_SECURE_IMAGE_AUTH
  /* This is only valid on signed images */
  if (sec_pil_hw_is_auth_enabled(code_seg,&auth_enabled) != E_SUCCESS)
  {
    SEC_PIL_ERROR_CODE(POPULATE_ELF_HW_IS_AUTH_ENBALED_FAILED);
    return -E_FAILURE;
  }
#endif

  if(auth_enabled)
  {
    /* Signature now comes after hash table */
    pil->sig_ptr = (uint8*)hash_seg_addr + hash_seg_sz;
  
    mi_boot_hdr= (mi_boot_image_header_type*)pil->hash_seg_hdr;
  
    if (sec_pil_is_mi_boot_valid(proc, mi_boot_hdr) != TRUE)
    {
      SEC_PIL_ERROR_CODE(POPULATE_ELF_MI_BOOT_INVALID);
      return -E_FAILURE;
    }
  
    pil->sig_sz = mi_boot_hdr->signature_size;
    pil->cert_ptr = (uint8*)(pil->sig_ptr + pil->sig_sz);
    pil->cert_sz = mi_boot_hdr->cert_chain_size;
  }

  return E_SUCCESS;
}

/**
 * @brief Computes the actual end address for XPU from the length loaded so far.
 *        length loaded so far might not be equal to the length on DDR as length
 *        loaded does not include alignment gaps and just includes mem size.
 *        Modem image is segmented and the real end of the whole modem image
 *        is within the last segment of the elf. (NOTE: All loadable segments must be 
 *        loaded by the host in the order of increasing addresses. holes are not 
 *        supported - ref:80-N8804-1_C)
 *
 * @param[in]     length_loaded  Length loaded so far.  
 * @param[in]     pil            Pointer to Pil internal data context
 * @param[in,out] end_addr       End address to be configured for XPU.
 *
 * @return 0 on success.
 */
int sec_pil_get_end_addr(uint32 length_loaded, 
                         const sec_pil_priv_t *pil,
                         uint32 *end_addr)
{
  const sec_pil_elf_info_t *elf = &(pil->elf);   
  uint16 cur_prog_hdr = 0;

  for (cur_prog_hdr = 0; length_loaded != 0 && cur_prog_hdr < elf->prog_hdr_num; cur_prog_hdr++)
  {
    if(sec_pil_is_valid_segment(&elf->prog_hdr[cur_prog_hdr]))
    {
       /* found the last segment when the length_loaded is smaller than the segment memsz,
               the end address of the mpss image is within this segment */
       if(length_loaded <= elf->prog_hdr[cur_prog_hdr].p_memsz)
       {
         *end_addr = elf->prog_hdr[cur_prog_hdr].p_paddr + length_loaded;
         if(*end_addr < elf->prog_hdr[cur_prog_hdr].p_paddr)
         {
           /* Integer Overflow */
           SEC_PIL_ERROR_CODE(PIL_GET_END_ADDRESS_INTEGER_OVERFLOW1);
           return -E_FAILURE;
         }
         break;
       }
       else /* decrement the length_loaded by memsz, because this is not the last segment. */
       {
         length_loaded -= elf->prog_hdr[cur_prog_hdr].p_memsz;
       }
    }
  }

  return E_SUCCESS;
}


