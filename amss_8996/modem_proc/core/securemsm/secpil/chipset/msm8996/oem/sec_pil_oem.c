/*=============================================================================

                             Boot Authenticator OEM

GENERAL DESCRIPTION
  This module is an extended version of secboot library Module
  which support eng cert based authentication.

Copyright 2014 by Qualcomm Technologies, Inc.  All Rights Reserved.
=============================================================================*/


/*=============================================================================

                            EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/core.mpss/3.4.c3.11/securemsm/secpil/chipset/msm8996/oem/sec_pil_oem.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
12/01/15   hw      add engg cert support
09/09/14   hw      Initial revision.
===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "sec_pil_util.h"
#include "secboot_x509.h"
#include "mba_rmb.h"

/*===========================================================================

                              FUNCTION DEFINITIONS

===========================================================================*/
#define SEC_BOOT_MSS_SW_TYPE        (0x2)
#define SECBOOT_ENGG_CERT_SW_TYPE   (SEC_BOOT_MSS_SW_TYPE)

/*!
* 
* @brief
* Check if eng certificate based authentication of image is allowed.
*
* @param[in]  context   the context that stored function pointers and information
*
* @param[in/out]  eng_cert_allowed   TRUE -  Allow authentication with eng cert  
*                            \c  FALSE -  Eng Cert not allowed
* @retval
*    SEC_IMG_AUTH_ERROR_TYPE Returns error code in case of eng Cert Setting error
*                  Returns 0 in case of no error
* 
*/
static SEC_IMG_AUTH_ERROR_TYPE auth_img_allow_eng_cert(
  secboot_auth_image_context*  context,
  uint32 *eng_cert_allowed
)
{
#ifdef ENG_CERT_SECBOOT_BUILD
  /* Set to Allowed */
  *eng_cert_allowed = TRUE;
#else
  /* Set to Disallowed */
  *eng_cert_allowed = FALSE;
#endif
  return SEC_IMG_AUTH_SUCCESS;
}

/*!
* 
* @brief
* Returns an alternative hw fuse config to be used for authentication
*
* @param[out]  fuse_info   Struct to define the OEM specific fuse
*                          information & root of trust for Eng Cert 
* 
* @retval
*    SEC_IMG_AUTH_ERROR_TYPE Returns error code in case of eng Cert Setting error
*                  Returns 0 in case of no error
*
*/
static SEC_IMG_AUTH_ERROR_TYPE auth_img_set_eng_cert(secboot_fuse_info_type *fuse_info)
{
  /* define Root of Trust for Eng Cert */
  
  /* hard-coded Engineering Root Cert Here */
  const uint8 root_of_trust_eng[CEML_HASH_DIGEST_SIZE_SHA256]=
  { 
    0x00
  };
  
  /* Copy Engineering Certificate Root of trust */
  MEMCPY(fuse_info->root_of_trust, (void*)root_of_trust_eng, CEML_HASH_DIGEST_SIZE_SHA256);

  /* set TRUE to only use provided root of trust pk hash from the hardcoded in this function and 
   * any hw config else like msm_hw_id is still from fuses. Set FALSE to used all hw config from 
   * this function.
   */
  fuse_info->use_root_of_trust_only = FALSE;

  /* Optional overrides, if use_root_of_trust_only == FALSE: */
  fuse_info->msm_hw_id = 0;
  fuse_info->auth_use_serial_num = 0;
  fuse_info->serial_num = 0;

  /* init multiple root cert fuse value */
  fuse_info->rot_fuse_info.disable_rot_transfer = SECBOOT_ROT_TRANSFER_DISABLE;
  fuse_info->rot_fuse_info.current_sw_rot = 0;
  fuse_info->rot_fuse_info.sw_rot_use_serial_num = 0; 

  return SEC_IMG_AUTH_SUCCESS;
}

/*!
* 
* @brief
*    This function authenticates the mpss image using the eng cert 
*
* @param[in] context              context that stores function pointers and related info.
*            image_info           secboot_image_info_type info for image
*            verified_info        secboot_verified_info_type verification info
* 
* @retval
*    SEC_IMG_AUTH_ERROR_TYPE Returns error code in case of authentication error
*                  Returns 0 in case of no error
* 
* @par Side Effects
*    Returns failure if engg cert is not allowed
* 
*/
SEC_IMG_AUTH_ERROR_TYPE auth_image_with_eng_cert( 
    secboot_auth_image_context*  context,
    secboot_image_info_type *image_info,
    secboot_verified_info_type *verified_info
)
{
  SEC_IMG_AUTH_ERROR_TYPE status = SEC_IMG_AUTH_FAILURE;                   
  do
  {
    secboot_fuse_info_type oem_fuse_info;
    secboot_error_type     sec_err       = E_SECBOOT_FAILURE;
    uint32                 is_eng_device = FALSE;
    secboot_handle_type    sbl_sec_handle;

    MEMSET(&oem_fuse_info, 0, sizeof(oem_fuse_info));
    MEMSET(&sbl_sec_handle, 0, sizeof(sbl_sec_handle));

    /* Input Validation */
    if( (NULL == context) ||
        (NULL == context->secboot_ftbl_ptr) ||
        (NULL == image_info) || 
        (NULL == verified_info) )
    {
      SEC_PIL_ERROR_CODE(PIL_SEC_IMG_AUTH_INVALID_ARG);
      status = SEC_IMG_AUTH_INVALID_ARG;
      break;
    }

    /* check whether engg cert auth is allowed or not
     */
    status = auth_img_allow_eng_cert(context, &is_eng_device);
    if(SEC_IMG_AUTH_SUCCESS != status)
    {
      /*break with SEC_IMG_AUTH_FAILURE return status */
      break;
    }

    if( (SECBOOT_ENGG_CERT_SW_TYPE == context->image_sw_id) && is_eng_device)
    {
      /* Initializes the secboot handle and Crypto HW engine */
      sec_err = context->secboot_ftbl_ptr->secboot_init( NULL, &sbl_sec_handle);
    
      if( E_SECBOOT_SUCCESS != sec_err )
      {
        status = (SEC_IMG_AUTH_ERROR_TYPE)sec_err;
        break;
      }
      
      /* Get OEM given fuse info (like Root of Trust) */
      status = auth_img_set_eng_cert(&oem_fuse_info);
      if( SEC_IMG_AUTH_SUCCESS != status )
      {
        break;
      } 
      /* initialize with provided fuse config for image authentication */
      sec_err = context->secboot_ftbl_ptr->secboot_init_fuses(&sbl_sec_handle, oem_fuse_info);
      if ( E_SECBOOT_SUCCESS != sec_err )
      {
        status = (SEC_IMG_AUTH_ERROR_TYPE)sec_err;
        break;
      }
      /* Authenticate using Eng Cert */
      sec_err = context->secboot_ftbl_ptr->secboot_authenticate( &sbl_sec_handle,
                                       SECBOOT_HW_MSS_CODE_SEGMENT,
                                       image_info,
                                       verified_info);
      if ( E_SECBOOT_SUCCESS != sec_err )
      {
        status = (SEC_IMG_AUTH_ERROR_TYPE)sec_err;
        break;
      }
      /* Deinitializes the secboot handle and Crypto HW engine */
      sec_err = context->secboot_ftbl_ptr->secboot_deinit(&sbl_sec_handle);
     
      if(E_SECBOOT_SUCCESS != sec_err)
      {
        status = (SEC_IMG_AUTH_ERROR_TYPE)sec_err;
        break;
      }

      status = SEC_IMG_AUTH_SUCCESS;
    }
    else /* Eng Cert Not allowed */
    {
      SEC_PIL_ERROR_CODE(PIL_SEC_IMG_AUTH_IMAGE_NOT_SUPPORTED);
      status = PIL_SEC_IMG_AUTH_IMAGE_NOT_SUPPORTED;
      break;          
    }
  }while(0);

  return status;  
}