/*===========================================================================

                         S E C U R I T Y    S E R V I C E S

                S E C U R E   B O O T  A U T H E N T I C A T I O N

                              A P I    M O D U L E
FILE:  secboot_util.c

DESCRIPTION:
        This is the wrapper interface into the secure boot API. Callers call into
        this API to ensure the image they have loaded and are going to execute
        if valid (i.e no changes were made to the image after is was signed).
        For more details check secboot.c file header.
 
DEPENDENCIES/ASSUMPTIONS
        

EXTERNALIZED FUNCTIONS

        Details of the functions are in the function comments and
        for help and overall design notes, read the secboot.h file


    Copyright (c) 2012 Qualcomm Technologies, Inc.  All Rights Reserved.
    Qualcomm Technologies Proprietary and Confidential.
===========================================================================*/

/*=========================================================================

                           EDIT HISTORY FOR FILE
    This section contains comments describing changes made to the module.
    Notice that changes are listed in reverse chronological order.

    $Header: //components/rel/core.mpss/3.4.c3.11/securemsm/secboot/chipset/mdm9x45/src/secboot_util.c#1 $

when       who            what, where, why
--------   ----           ---------------------------------------------------
12/04/12   vg             Removed compiler warning.
10/19/12   vg             Initial API - based on old api with some changes
===========================================================================*/

/*==========================================================================

                              INCLUDE FILES

===========================================================================*/
#include "secboot_util.h"
#include "secboot.h"
#include "secboot_hw.h"
#include <IxErrno.h>
#include <stringl.h>

#define SEC_UTIL_ROUNDUP(type, val, multiple) (((val) + (multiple)-1) & \
                                              ~(type)((multiple)-1))

static boolean secboot_util_validate_mi_header
(
  secboot_util_auth_img_hdr_t*  img_hdr_ptr
)
{
  uint8*   signed_img_ptr = (uint8*)img_hdr_ptr;
  uint32   total_len = 0;
  uint32   header_size = sizeof(secboot_util_auth_img_hdr_t); 
  uint8*   code_ptr = NULL;
  uint8*   sig_ptr = NULL;
  uint8*   cert_chain_ptr = NULL;

  if (img_hdr_ptr == NULL)
  {
    return FALSE;
  }

  /* Validate mbn header size */
  if (img_hdr_ptr->code_size == 0 || img_hdr_ptr->cert_chain_size == 0 ||
      img_hdr_ptr->signature_size == 0)
  {
    return FALSE;
  }

  /* Total Len of Cert Chain Size and Signature size should not 
   * exceed the MAX Size Allowed.
   */
  if (img_hdr_ptr->cert_chain_size > SECBOOT_MAX_CERT_CHAIN_SIZE)
  {
    return FALSE;
  }

  /* Verify the image_size is equal to total of code_size, cert_chain_size and
   * signature_size. We may have added 1 to 3 bytes to the image if the total
   * image size is not multiple of four. We should account for that also.
   */

  total_len = img_hdr_ptr->code_size;
  total_len += img_hdr_ptr->cert_chain_size;
  if(total_len < img_hdr_ptr->cert_chain_size)
  {
    return FALSE;
  }

  total_len += img_hdr_ptr->signature_size;
  if(total_len < img_hdr_ptr->signature_size)
  {
    return FALSE;
  }

  total_len = SEC_UTIL_ROUNDUP(uint32, total_len, 4);

  if (total_len != img_hdr_ptr->image_size)
  {
    return FALSE;
  }

  code_ptr = signed_img_ptr + header_size;

  /*Overflow check to make sure signature pointer do not overflow */
  if (((uint32)code_ptr  + img_hdr_ptr->code_size) < (uint32)code_ptr)
  {
    return FALSE;
  }
  sig_ptr = code_ptr + img_hdr_ptr->code_size;

  /*Overflow check to make sure cert chain pointer do not overflow */
  if (((uint32)sig_ptr + img_hdr_ptr->signature_size) < (uint32)sig_ptr)
  {
    return FALSE;
  }
  cert_chain_ptr = sig_ptr + img_hdr_ptr->signature_size;

  /* Overflow check to ensure the end of the cert chain does not overflow */
  if (((uint32)cert_chain_ptr + img_hdr_ptr->cert_chain_size) < (uint32)cert_chain_ptr)
  {
    return FALSE;
  }

  return TRUE;
}

/**
 * @brief See documentation in public header
 *
 */
boolean secboot_util_auth_img_verify
(
  secboot_util_handle_type           *secboot_handle_ptr,
  const uint8                        *signed_img_ptr,
  uint32                              signed_img_len,  
  uint32                              code_segment,
  uint64                              sw_id,  
  const secboot_util_fuse_info_type  *fuse_info_ptr,
  secboot_util_verified_info_type    *verified_info_ptr
)
{
  secboot_util_auth_img_hdr_t* img_hdr_ptr = NULL;   
  boolean                     retVal = TRUE;
  uint32                      header_size = 0;  
  secboot_error_type          sec_err    = E_SECBOOT_FAILURE;  
  secboot_image_info_type     image_info;
  secboot_verified_info_type  verified_info;
  secboot_fuse_info_type      fuses;
  uint8                       *code_ptr = NULL;
  uint8                       *sig_ptr = NULL;
  uint8                       *cert_chain_ptr = NULL;
  
  do
  {
    if ( (NULL == secboot_handle_ptr) ||         
         (NULL == signed_img_ptr) )
    {      
      retVal = FALSE;
      break;
    }

    img_hdr_ptr    = (secboot_util_auth_img_hdr_t *) signed_img_ptr;

    if(signed_img_len < (img_hdr_ptr->image_size + sizeof(secboot_util_auth_img_hdr_t)))
    {
      retVal = FALSE;
      break;
    }

    if(FALSE == secboot_util_validate_mi_header(img_hdr_ptr))
    {
      retVal = FALSE;
      break;
    }

    sec_err = secboot_init( NULL, (secboot_handle_type *)secboot_handle_ptr);
    if ( E_SECBOOT_SUCCESS != sec_err )
    {
      retVal = FALSE;
      break;
    }

	memset(&fuses, 0, sizeof(fuses));

    if(fuse_info_ptr != NULL)
    {      
      fuses.msm_hw_id = fuse_info_ptr->msm_hw_id;
      fuses.serial_num = fuse_info_ptr->serial_num;
      fuses.auth_use_serial_num = fuse_info_ptr->auth_use_serial_num;
      fuses.use_root_of_trust_only = fuse_info_ptr->use_root_of_trust_only;
	  fuses.root_sel_info.is_root_sel_enabled = fuse_info_ptr->root_sel_info.is_root_sel_enabled;
	  fuses.root_sel_info.root_cert_sel = fuse_info_ptr->root_sel_info.root_cert_sel;	
	  fuses.root_sel_info.num_root_certs = fuse_info_ptr->root_sel_info.num_root_certs;

      if(fuse_info_ptr->root_of_trust_len != SHA256_HASH_LENGTH)
      {
        retVal = FALSE;
        break;
      }
      memscpy(fuses.root_of_trust,SHA256_HASH_LENGTH,fuse_info_ptr->root_of_trust,SHA256_HASH_LENGTH);
      
      sec_err = secboot_init_fuses((secboot_handle_type *)secboot_handle_ptr,fuses);
      if ( E_SECBOOT_SUCCESS != sec_err )
      {
        secboot_deinit((secboot_handle_type *)secboot_handle_ptr);
        retVal = FALSE;       
        break;
      }
    }

    header_size = sizeof(secboot_util_auth_img_hdr_t);
    code_ptr = (uint8 *)signed_img_ptr + header_size;
    sig_ptr = code_ptr + img_hdr_ptr->code_size;
    cert_chain_ptr = sig_ptr + img_hdr_ptr->signature_size;    
    
    image_info.header_ptr_1 = signed_img_ptr;
    image_info.header_len_1 = header_size;
    image_info.code_ptr_1= code_ptr;
    image_info.code_len_1 = img_hdr_ptr->code_size;
    image_info.signature_ptr = sig_ptr;
    image_info.signature_len = img_hdr_ptr->signature_size;
    image_info.sw_type = sw_id;
    image_info.sw_version = sw_id >> 32;
    image_info.x509_chain_ptr = cert_chain_ptr;
    image_info.x509_chain_len = img_hdr_ptr->cert_chain_size; 
    
    sec_err = secboot_authenticate( (secboot_handle_type *)secboot_handle_ptr,
                                    code_segment, 
                                    &image_info,
                                    &verified_info);
    if ( E_SECBOOT_SUCCESS != sec_err )
    {
      secboot_deinit((secboot_handle_type *)secboot_handle_ptr);
      retVal = FALSE;
      break;
    }

    sec_err = secboot_deinit((secboot_handle_type *)secboot_handle_ptr);
    if ( E_SECBOOT_SUCCESS != sec_err )
    {
      retVal = FALSE;
      break;
    }

   if(verified_info_ptr != NULL)
   {
      /* Image is located after the header */
      verified_info_ptr->img_offset = header_size;
      verified_info_ptr->img_len = img_hdr_ptr->code_size;
      verified_info_ptr->sw_id = verified_info.sw_id;
    }
    
  } while(0);

  return retVal;
} /* secboot_util_auth_img_verify() */


/**
 * @brief See documentation in public header
 *
 */
uint32 secboot_util_hw_is_auth_enabled(uint32 code_segment, uint32 *auth_enabled_ptr)
{  
  secboot_hw_etype sec_err = E_SECBOOT_HW_FAILURE;

  sec_err = secboot_hw_is_auth_enabled(code_segment,auth_enabled_ptr);
 
  if(sec_err != E_SECBOOT_HW_SUCCESS)
  {
    return E_FAILURE;
  }
  
  return E_SUCCESS;
}



