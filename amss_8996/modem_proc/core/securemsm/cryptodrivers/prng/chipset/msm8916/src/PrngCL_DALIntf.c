/**
@file PrngCL_DALIntf.c 
@brief Prng Engine Core Library source file 
*/

/*===========================================================================

                       P R N G E n g i n e D r i v e r

                       S o u r c e  F i l e (e x t e r n a l)

DESCRIPTION
  This header file contains HW PRNG DAL interface specific declarations.

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None
  
Copyright (c) 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
============================================================================*/

/*===========================================================================

                           EDIT HISTORY FOR FILE

  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/core.mpss/3.4.c3.11/securemsm/cryptodrivers/prng/chipset/msm8916/src/PrngCL_DALIntf.c#1 $
  $DateTime: 2016/03/28 23:02:17 $
  $Author: mplcsds1 $ 

when         who     what, where, why
--------     ---     ----------------------------------------------------------
2014-02-21   nk      Dal cleanups for clock
2014-02-17   nk      PNOC changed to PCNOC
2013-10-24   nk      Clock counter deprecated
2012-05-02   amen    Added vote for pnoc due to rpm turning it off
2011-09-05   nk      Initial version
============================================================================*/

#include "comdef.h"
#include "DALDeviceId.h"
#include "DDIClock.h"
#include "PrngCL_DALIntf.h"
#include "PrngCL.h"
#include "npa.h"

uint32 prng_clk_init = 0;
DalDeviceHandle   *hClock = NULL;
ClockIdType       nClockID;

static npa_client_handle prng_pcnoc=NULL;

/**
 * @brief   This function enables the PRNG Engine Clock 
 *          The function uses DAL interface to enable clock.
 *
 * @param None
 *
 * @return Prng_Result_Type
 *
 * @see PrngCL_DAL_Clock_Disable
 */

PrngCL_Result_Type PrngCL_DAL_Clock_Enable( void )
{

  DALResult             eResult;
  PrngCL_Result_Type    ePrngStatus = PRNGCL_ERROR_NONE;

  if (prng_clk_init == 0)
  {
    if(prng_pcnoc == NULL) 
    {
      prng_pcnoc = npa_create_sync_client("/clk/pcnoc", "CRYPTO", NPA_CLIENT_REQUIRED);
    }
  
    if (prng_pcnoc) 
    {
      npa_issue_required_request(prng_pcnoc, NPA_MAX_STATE);
    }
    else
    {
     return PRNGCL_ERROR_FAILED;
    }

    if (hClock == NULL)
    {
      DALSYS_InitMod(NULL);

      /* Get the handle for clock device */
      eResult = DAL_DeviceAttach(DALDEVICEID_CLOCK, &hClock);
      if(eResult != DAL_SUCCESS)
      {
        ePrngStatus = PRNGCL_ERROR_FAILED;
        return ePrngStatus;
      }

      /* Get the clock ID */
      eResult = DalClock_GetClockId(hClock, "gcc_prng_ahb_clk", &nClockID);
      if(eResult != DAL_SUCCESS)
      {
        ePrngStatus = PRNGCL_ERROR_FAILED;
        return ePrngStatus;
      }
    }

    eResult = DalClock_EnableClock(hClock,nClockID);
    if(eResult != DAL_SUCCESS)
    {
      ePrngStatus = PRNGCL_ERROR_FAILED;
      return ePrngStatus;
    }
  }
  prng_clk_init++;

  return ePrngStatus;
}

/**
 * @brief   This function disables the PRNG Engine Clock 
 *          The function uses DAL interface to disable clock.
 *
 * @param None
 *
 * @return Prng_Result_Type
 *
 * @see PrngEL_DAL_Clock_Enable
 */
PrngCL_Result_Type PrngCL_DAL_Clock_Disable( void )
{
  DALResult           eResult;
  PrngCL_Result_Type    ePrngStatus = PRNGCL_ERROR_NONE;
  
  if(prng_clk_init!=0)
  {
    prng_clk_init--;
  }
  
  if (prng_clk_init == 0)
  {
    eResult = DalClock_DisableClock(hClock,nClockID);
    if(eResult != DAL_SUCCESS)
    {
      ePrngStatus = PRNGCL_ERROR_FAILED;
      return ePrngStatus;
    }

    if (prng_pcnoc)
    {
      npa_complete_request(prng_pcnoc);
    }
    else
    {
      return PRNGCL_ERROR_FAILED;
    }
  }

  return ePrngStatus;
}

