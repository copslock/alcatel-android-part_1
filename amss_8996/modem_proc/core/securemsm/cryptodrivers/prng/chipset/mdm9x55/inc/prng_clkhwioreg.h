#ifndef __PRNG_CLKHWIOREG_H__
#define __PRNG_CLKHWIOREG_H__
/*
===========================================================================
*/
/**
  @file prng_clkhwioreg.h
  @brief Auto-generated HWIO interface include file.

  This file contains HWIO register definitions for the following modules:
    GCC_CLK_CTL_REG
    MSS_PERPH
    MSS_QDSP6SS_PUB

  'Include' filters applied: 
  'Exclude' filters applied: RESERVED DUMMY 
*/
/*
  ===========================================================================

  Copyright (c) 2012 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ===========================================================================

  $Header: //components/rel/core.mpss/3.4.c3.11/securemsm/cryptodrivers/prng/chipset/mdm9x55/inc/prng_clkhwioreg.h#1 $
  $DateTime: 2016/03/28 23:02:17 $
  $Author: mplcsds1 $

  ===========================================================================
*/

#include "msmhwiobase.h"

/*----------------------------------------------------------------------------
 * MODULE: GCC_CLK_CTL_REG
 *--------------------------------------------------------------------------*/

#define GCC_CLK_CTL_REG_REG_BASE                                                                  (CLK_CTL_BASE      + 0x00000000)

#define HWIO_GCC_GPLL0_MODE_ADDR                                                                  (GCC_CLK_CTL_REG_REG_BASE      + 0x00021000)
#define HWIO_GCC_GPLL0_MODE_RMSK                                                                  0xe0ffff0f
#define HWIO_GCC_GPLL0_MODE_IN          \
        in_dword_masked(HWIO_GCC_GPLL0_MODE_ADDR, HWIO_GCC_GPLL0_MODE_RMSK)
#define HWIO_GCC_GPLL0_MODE_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL0_MODE_ADDR, m)
#define HWIO_GCC_GPLL0_MODE_OUT(v)      \
        out_dword(HWIO_GCC_GPLL0_MODE_ADDR,v)
#define HWIO_GCC_GPLL0_MODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL0_MODE_ADDR,m,v,HWIO_GCC_GPLL0_MODE_IN)
#define HWIO_GCC_GPLL0_MODE_PLL_LOCK_DET_BMSK                                                     0x80000000
#define HWIO_GCC_GPLL0_MODE_PLL_LOCK_DET_SHFT                                                           0x1f
#define HWIO_GCC_GPLL0_MODE_PLL_ACTIVE_FLAG_BMSK                                                  0x40000000
#define HWIO_GCC_GPLL0_MODE_PLL_ACTIVE_FLAG_SHFT                                                        0x1e
#define HWIO_GCC_GPLL0_MODE_PLL_ACK_LATCH_BMSK                                                    0x20000000
#define HWIO_GCC_GPLL0_MODE_PLL_ACK_LATCH_SHFT                                                          0x1d
#define HWIO_GCC_GPLL0_MODE_PLL_HW_UPADATE_LOGIC_BYPASS_BMSK                                        0x800000
#define HWIO_GCC_GPLL0_MODE_PLL_HW_UPADATE_LOGIC_BYPASS_SHFT                                            0x17
#define HWIO_GCC_GPLL0_MODE_PLL_UPDATE_BMSK                                                         0x400000
#define HWIO_GCC_GPLL0_MODE_PLL_UPDATE_SHFT                                                             0x16
#define HWIO_GCC_GPLL0_MODE_PLL_VOTE_FSM_RESET_BMSK                                                 0x200000
#define HWIO_GCC_GPLL0_MODE_PLL_VOTE_FSM_RESET_SHFT                                                     0x15
#define HWIO_GCC_GPLL0_MODE_PLL_VOTE_FSM_ENA_BMSK                                                   0x100000
#define HWIO_GCC_GPLL0_MODE_PLL_VOTE_FSM_ENA_SHFT                                                       0x14
#define HWIO_GCC_GPLL0_MODE_PLL_BIAS_COUNT_BMSK                                                      0xfc000
#define HWIO_GCC_GPLL0_MODE_PLL_BIAS_COUNT_SHFT                                                          0xe
#define HWIO_GCC_GPLL0_MODE_PLL_LOCK_COUNT_BMSK                                                       0x3f00
#define HWIO_GCC_GPLL0_MODE_PLL_LOCK_COUNT_SHFT                                                          0x8
#define HWIO_GCC_GPLL0_MODE_PLL_PLLTEST_BMSK                                                             0x8
#define HWIO_GCC_GPLL0_MODE_PLL_PLLTEST_SHFT                                                             0x3
#define HWIO_GCC_GPLL0_MODE_PLL_RESET_N_BMSK                                                             0x4
#define HWIO_GCC_GPLL0_MODE_PLL_RESET_N_SHFT                                                             0x2
#define HWIO_GCC_GPLL0_MODE_PLL_BYPASSNL_BMSK                                                            0x2
#define HWIO_GCC_GPLL0_MODE_PLL_BYPASSNL_SHFT                                                            0x1
#define HWIO_GCC_GPLL0_MODE_PLL_OUTCTRL_BMSK                                                             0x1
#define HWIO_GCC_GPLL0_MODE_PLL_OUTCTRL_SHFT                                                             0x0

#define HWIO_GCC_GPLL0_L_VAL_ADDR                                                                 (GCC_CLK_CTL_REG_REG_BASE      + 0x00021004)
#define HWIO_GCC_GPLL0_L_VAL_RMSK                                                                     0xffff
#define HWIO_GCC_GPLL0_L_VAL_IN          \
        in_dword_masked(HWIO_GCC_GPLL0_L_VAL_ADDR, HWIO_GCC_GPLL0_L_VAL_RMSK)
#define HWIO_GCC_GPLL0_L_VAL_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL0_L_VAL_ADDR, m)
#define HWIO_GCC_GPLL0_L_VAL_OUT(v)      \
        out_dword(HWIO_GCC_GPLL0_L_VAL_ADDR,v)
#define HWIO_GCC_GPLL0_L_VAL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL0_L_VAL_ADDR,m,v,HWIO_GCC_GPLL0_L_VAL_IN)
#define HWIO_GCC_GPLL0_L_VAL_PLL_L_BMSK                                                               0xffff
#define HWIO_GCC_GPLL0_L_VAL_PLL_L_SHFT                                                                  0x0

#define HWIO_GCC_GPLL0_ALPHA_VAL_ADDR                                                             (GCC_CLK_CTL_REG_REG_BASE      + 0x00021008)
#define HWIO_GCC_GPLL0_ALPHA_VAL_RMSK                                                             0xffffffff
#define HWIO_GCC_GPLL0_ALPHA_VAL_IN          \
        in_dword_masked(HWIO_GCC_GPLL0_ALPHA_VAL_ADDR, HWIO_GCC_GPLL0_ALPHA_VAL_RMSK)
#define HWIO_GCC_GPLL0_ALPHA_VAL_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL0_ALPHA_VAL_ADDR, m)
#define HWIO_GCC_GPLL0_ALPHA_VAL_OUT(v)      \
        out_dword(HWIO_GCC_GPLL0_ALPHA_VAL_ADDR,v)
#define HWIO_GCC_GPLL0_ALPHA_VAL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL0_ALPHA_VAL_ADDR,m,v,HWIO_GCC_GPLL0_ALPHA_VAL_IN)
#define HWIO_GCC_GPLL0_ALPHA_VAL_PLL_ALPHA_BMSK                                                   0xffffffff
#define HWIO_GCC_GPLL0_ALPHA_VAL_PLL_ALPHA_SHFT                                                          0x0

#define HWIO_GCC_GPLL0_ALPHA_VAL_U_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x0002100c)
#define HWIO_GCC_GPLL0_ALPHA_VAL_U_RMSK                                                                 0xff
#define HWIO_GCC_GPLL0_ALPHA_VAL_U_IN          \
        in_dword_masked(HWIO_GCC_GPLL0_ALPHA_VAL_U_ADDR, HWIO_GCC_GPLL0_ALPHA_VAL_U_RMSK)
#define HWIO_GCC_GPLL0_ALPHA_VAL_U_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL0_ALPHA_VAL_U_ADDR, m)
#define HWIO_GCC_GPLL0_ALPHA_VAL_U_OUT(v)      \
        out_dword(HWIO_GCC_GPLL0_ALPHA_VAL_U_ADDR,v)
#define HWIO_GCC_GPLL0_ALPHA_VAL_U_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL0_ALPHA_VAL_U_ADDR,m,v,HWIO_GCC_GPLL0_ALPHA_VAL_U_IN)
#define HWIO_GCC_GPLL0_ALPHA_VAL_U_PLL_ALPHA_BMSK                                                       0xff
#define HWIO_GCC_GPLL0_ALPHA_VAL_U_PLL_ALPHA_SHFT                                                        0x0

#define HWIO_GCC_GPLL0_USER_CTL_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x00021010)
#define HWIO_GCC_GPLL0_USER_CTL_RMSK                                                              0xffffffff
#define HWIO_GCC_GPLL0_USER_CTL_IN          \
        in_dword_masked(HWIO_GCC_GPLL0_USER_CTL_ADDR, HWIO_GCC_GPLL0_USER_CTL_RMSK)
#define HWIO_GCC_GPLL0_USER_CTL_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL0_USER_CTL_ADDR, m)
#define HWIO_GCC_GPLL0_USER_CTL_OUT(v)      \
        out_dword(HWIO_GCC_GPLL0_USER_CTL_ADDR,v)
#define HWIO_GCC_GPLL0_USER_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL0_USER_CTL_ADDR,m,v,HWIO_GCC_GPLL0_USER_CTL_IN)
#define HWIO_GCC_GPLL0_USER_CTL_RESERVE_BITS31_28_BMSK                                            0xf0000000
#define HWIO_GCC_GPLL0_USER_CTL_RESERVE_BITS31_28_SHFT                                                  0x1c
#define HWIO_GCC_GPLL0_USER_CTL_SSC_MODE_CONTROL_BMSK                                              0x8000000
#define HWIO_GCC_GPLL0_USER_CTL_SSC_MODE_CONTROL_SHFT                                                   0x1b
#define HWIO_GCC_GPLL0_USER_CTL_RESERVE_BITS26_25_BMSK                                             0x6000000
#define HWIO_GCC_GPLL0_USER_CTL_RESERVE_BITS26_25_SHFT                                                  0x19
#define HWIO_GCC_GPLL0_USER_CTL_ALPHA_EN_BMSK                                                      0x1000000
#define HWIO_GCC_GPLL0_USER_CTL_ALPHA_EN_SHFT                                                           0x18
#define HWIO_GCC_GPLL0_USER_CTL_RESERVE_BITS23_22_BMSK                                              0xc00000
#define HWIO_GCC_GPLL0_USER_CTL_RESERVE_BITS23_22_SHFT                                                  0x16
#define HWIO_GCC_GPLL0_USER_CTL_VCO_SEL_BMSK                                                        0x300000
#define HWIO_GCC_GPLL0_USER_CTL_VCO_SEL_SHFT                                                            0x14
#define HWIO_GCC_GPLL0_USER_CTL_RESERVE_BITS19_15_BMSK                                               0xf8000
#define HWIO_GCC_GPLL0_USER_CTL_RESERVE_BITS19_15_SHFT                                                   0xf
#define HWIO_GCC_GPLL0_USER_CTL_PRE_DIV_RATIO_BMSK                                                    0x7000
#define HWIO_GCC_GPLL0_USER_CTL_PRE_DIV_RATIO_SHFT                                                       0xc
#define HWIO_GCC_GPLL0_USER_CTL_POST_DIV_RATIO_BMSK                                                    0xf00
#define HWIO_GCC_GPLL0_USER_CTL_POST_DIV_RATIO_SHFT                                                      0x8
#define HWIO_GCC_GPLL0_USER_CTL_OUTPUT_INV_BMSK                                                         0x80
#define HWIO_GCC_GPLL0_USER_CTL_OUTPUT_INV_SHFT                                                          0x7
#define HWIO_GCC_GPLL0_USER_CTL_RESERVE_BITS6_5_BMSK                                                    0x60
#define HWIO_GCC_GPLL0_USER_CTL_RESERVE_BITS6_5_SHFT                                                     0x5
#define HWIO_GCC_GPLL0_USER_CTL_PLLOUT_LV_TEST_BMSK                                                     0x10
#define HWIO_GCC_GPLL0_USER_CTL_PLLOUT_LV_TEST_SHFT                                                      0x4
#define HWIO_GCC_GPLL0_USER_CTL_PLLOUT_LV_EARLY_BMSK                                                     0x8
#define HWIO_GCC_GPLL0_USER_CTL_PLLOUT_LV_EARLY_SHFT                                                     0x3
#define HWIO_GCC_GPLL0_USER_CTL_PLLOUT_LV_AUX2_BMSK                                                      0x4
#define HWIO_GCC_GPLL0_USER_CTL_PLLOUT_LV_AUX2_SHFT                                                      0x2
#define HWIO_GCC_GPLL0_USER_CTL_PLLOUT_LV_AUX_BMSK                                                       0x2
#define HWIO_GCC_GPLL0_USER_CTL_PLLOUT_LV_AUX_SHFT                                                       0x1
#define HWIO_GCC_GPLL0_USER_CTL_PLLOUT_LV_MAIN_BMSK                                                      0x1
#define HWIO_GCC_GPLL0_USER_CTL_PLLOUT_LV_MAIN_SHFT                                                      0x0

#define HWIO_GCC_GPLL0_USER_CTL_U_ADDR                                                            (GCC_CLK_CTL_REG_REG_BASE      + 0x00021014)
#define HWIO_GCC_GPLL0_USER_CTL_U_RMSK                                                            0xffffffff
#define HWIO_GCC_GPLL0_USER_CTL_U_IN          \
        in_dword_masked(HWIO_GCC_GPLL0_USER_CTL_U_ADDR, HWIO_GCC_GPLL0_USER_CTL_U_RMSK)
#define HWIO_GCC_GPLL0_USER_CTL_U_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL0_USER_CTL_U_ADDR, m)
#define HWIO_GCC_GPLL0_USER_CTL_U_OUT(v)      \
        out_dword(HWIO_GCC_GPLL0_USER_CTL_U_ADDR,v)
#define HWIO_GCC_GPLL0_USER_CTL_U_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL0_USER_CTL_U_ADDR,m,v,HWIO_GCC_GPLL0_USER_CTL_U_IN)
#define HWIO_GCC_GPLL0_USER_CTL_U_RESERVE_BITS31_12_BMSK                                          0xfffff000
#define HWIO_GCC_GPLL0_USER_CTL_U_RESERVE_BITS31_12_SHFT                                                 0xc
#define HWIO_GCC_GPLL0_USER_CTL_U_LATCH_INTERFACE_BYPASS_BMSK                                          0x800
#define HWIO_GCC_GPLL0_USER_CTL_U_LATCH_INTERFACE_BYPASS_SHFT                                            0xb
#define HWIO_GCC_GPLL0_USER_CTL_U_STATUS_REGISTER_BMSK                                                 0x700
#define HWIO_GCC_GPLL0_USER_CTL_U_STATUS_REGISTER_SHFT                                                   0x8
#define HWIO_GCC_GPLL0_USER_CTL_U_DSM_BMSK                                                              0x80
#define HWIO_GCC_GPLL0_USER_CTL_U_DSM_SHFT                                                               0x7
#define HWIO_GCC_GPLL0_USER_CTL_U_WRITE_STATE_BMSK                                                      0x40
#define HWIO_GCC_GPLL0_USER_CTL_U_WRITE_STATE_SHFT                                                       0x6
#define HWIO_GCC_GPLL0_USER_CTL_U_TARGET_CTL_BMSK                                                       0x38
#define HWIO_GCC_GPLL0_USER_CTL_U_TARGET_CTL_SHFT                                                        0x3
#define HWIO_GCC_GPLL0_USER_CTL_U_LOCK_DET_BMSK                                                          0x4
#define HWIO_GCC_GPLL0_USER_CTL_U_LOCK_DET_SHFT                                                          0x2
#define HWIO_GCC_GPLL0_USER_CTL_U_FREEZE_PLL_BMSK                                                        0x2
#define HWIO_GCC_GPLL0_USER_CTL_U_FREEZE_PLL_SHFT                                                        0x1
#define HWIO_GCC_GPLL0_USER_CTL_U_TOGGLE_DET_BMSK                                                        0x1
#define HWIO_GCC_GPLL0_USER_CTL_U_TOGGLE_DET_SHFT                                                        0x0

#define HWIO_GCC_GPLL0_CONFIG_CTL_ADDR                                                            (GCC_CLK_CTL_REG_REG_BASE      + 0x00021018)
#define HWIO_GCC_GPLL0_CONFIG_CTL_RMSK                                                            0xffffffff
#define HWIO_GCC_GPLL0_CONFIG_CTL_IN          \
        in_dword_masked(HWIO_GCC_GPLL0_CONFIG_CTL_ADDR, HWIO_GCC_GPLL0_CONFIG_CTL_RMSK)
#define HWIO_GCC_GPLL0_CONFIG_CTL_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL0_CONFIG_CTL_ADDR, m)
#define HWIO_GCC_GPLL0_CONFIG_CTL_OUT(v)      \
        out_dword(HWIO_GCC_GPLL0_CONFIG_CTL_ADDR,v)
#define HWIO_GCC_GPLL0_CONFIG_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL0_CONFIG_CTL_ADDR,m,v,HWIO_GCC_GPLL0_CONFIG_CTL_IN)
#define HWIO_GCC_GPLL0_CONFIG_CTL_SINGLE_DMET_MODE_ENABLE_BMSK                                    0x80000000
#define HWIO_GCC_GPLL0_CONFIG_CTL_SINGLE_DMET_MODE_ENABLE_SHFT                                          0x1f
#define HWIO_GCC_GPLL0_CONFIG_CTL_DMET_WINDOW_ENABLE_BMSK                                         0x40000000
#define HWIO_GCC_GPLL0_CONFIG_CTL_DMET_WINDOW_ENABLE_SHFT                                               0x1e
#define HWIO_GCC_GPLL0_CONFIG_CTL_TOGGLE_DET_SAMPLE_INTER_BMSK                                    0x3c000000
#define HWIO_GCC_GPLL0_CONFIG_CTL_TOGGLE_DET_SAMPLE_INTER_SHFT                                          0x1a
#define HWIO_GCC_GPLL0_CONFIG_CTL_TOGGLE_DET_THRESHOLD_BMSK                                        0x3800000
#define HWIO_GCC_GPLL0_CONFIG_CTL_TOGGLE_DET_THRESHOLD_SHFT                                             0x17
#define HWIO_GCC_GPLL0_CONFIG_CTL_TOGGLE_DET_SAMPLE_BMSK                                            0x700000
#define HWIO_GCC_GPLL0_CONFIG_CTL_TOGGLE_DET_SAMPLE_SHFT                                                0x14
#define HWIO_GCC_GPLL0_CONFIG_CTL_LOCK_DET_THRESHOLD_BMSK                                            0xff000
#define HWIO_GCC_GPLL0_CONFIG_CTL_LOCK_DET_THRESHOLD_SHFT                                                0xc
#define HWIO_GCC_GPLL0_CONFIG_CTL_LOCK_DET_SAMPLE_SIZE_BMSK                                            0xf00
#define HWIO_GCC_GPLL0_CONFIG_CTL_LOCK_DET_SAMPLE_SIZE_SHFT                                              0x8
#define HWIO_GCC_GPLL0_CONFIG_CTL_MIN_GLITCH_THRESHOLD_BMSK                                             0xc0
#define HWIO_GCC_GPLL0_CONFIG_CTL_MIN_GLITCH_THRESHOLD_SHFT                                              0x6
#define HWIO_GCC_GPLL0_CONFIG_CTL_REF_CYCLE_BMSK                                                        0x30
#define HWIO_GCC_GPLL0_CONFIG_CTL_REF_CYCLE_SHFT                                                         0x4
#define HWIO_GCC_GPLL0_CONFIG_CTL_KFN_BMSK                                                               0xf
#define HWIO_GCC_GPLL0_CONFIG_CTL_KFN_SHFT                                                               0x0

#define HWIO_GCC_GPLL0_TEST_CTL_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x0002101c)
#define HWIO_GCC_GPLL0_TEST_CTL_RMSK                                                              0xffffffff
#define HWIO_GCC_GPLL0_TEST_CTL_IN          \
        in_dword_masked(HWIO_GCC_GPLL0_TEST_CTL_ADDR, HWIO_GCC_GPLL0_TEST_CTL_RMSK)
#define HWIO_GCC_GPLL0_TEST_CTL_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL0_TEST_CTL_ADDR, m)
#define HWIO_GCC_GPLL0_TEST_CTL_OUT(v)      \
        out_dword(HWIO_GCC_GPLL0_TEST_CTL_ADDR,v)
#define HWIO_GCC_GPLL0_TEST_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL0_TEST_CTL_ADDR,m,v,HWIO_GCC_GPLL0_TEST_CTL_IN)
#define HWIO_GCC_GPLL0_TEST_CTL_BIAS_GEN_TRIM_BMSK                                                0xe0000000
#define HWIO_GCC_GPLL0_TEST_CTL_BIAS_GEN_TRIM_SHFT                                                      0x1d
#define HWIO_GCC_GPLL0_TEST_CTL_DCO_BMSK                                                          0x10000000
#define HWIO_GCC_GPLL0_TEST_CTL_DCO_SHFT                                                                0x1c
#define HWIO_GCC_GPLL0_TEST_CTL_PROCESS_CALB_BMSK                                                  0xc000000
#define HWIO_GCC_GPLL0_TEST_CTL_PROCESS_CALB_SHFT                                                       0x1a
#define HWIO_GCC_GPLL0_TEST_CTL_OVERRIDE_PROCESS_CALB_BMSK                                         0x2000000
#define HWIO_GCC_GPLL0_TEST_CTL_OVERRIDE_PROCESS_CALB_SHFT                                              0x19
#define HWIO_GCC_GPLL0_TEST_CTL_FINE_FCW_BMSK                                                      0x1f00000
#define HWIO_GCC_GPLL0_TEST_CTL_FINE_FCW_SHFT                                                           0x14
#define HWIO_GCC_GPLL0_TEST_CTL_OVERRIDE_FINE_FCW_BMSK                                               0x80000
#define HWIO_GCC_GPLL0_TEST_CTL_OVERRIDE_FINE_FCW_SHFT                                                  0x13
#define HWIO_GCC_GPLL0_TEST_CTL_COARSE_FCW_BMSK                                                      0x7e000
#define HWIO_GCC_GPLL0_TEST_CTL_COARSE_FCW_SHFT                                                          0xd
#define HWIO_GCC_GPLL0_TEST_CTL_OVERRIDE_COARSE_BMSK                                                  0x1000
#define HWIO_GCC_GPLL0_TEST_CTL_OVERRIDE_COARSE_SHFT                                                     0xc
#define HWIO_GCC_GPLL0_TEST_CTL_DISABLE_LFSR_BMSK                                                      0x800
#define HWIO_GCC_GPLL0_TEST_CTL_DISABLE_LFSR_SHFT                                                        0xb
#define HWIO_GCC_GPLL0_TEST_CTL_DTEST_SEL_BMSK                                                         0x700
#define HWIO_GCC_GPLL0_TEST_CTL_DTEST_SEL_SHFT                                                           0x8
#define HWIO_GCC_GPLL0_TEST_CTL_DTEST_EN_BMSK                                                           0x80
#define HWIO_GCC_GPLL0_TEST_CTL_DTEST_EN_SHFT                                                            0x7
#define HWIO_GCC_GPLL0_TEST_CTL_BYP_TESTAMP_BMSK                                                        0x40
#define HWIO_GCC_GPLL0_TEST_CTL_BYP_TESTAMP_SHFT                                                         0x6
#define HWIO_GCC_GPLL0_TEST_CTL_ATEST1_SEL_BMSK                                                         0x30
#define HWIO_GCC_GPLL0_TEST_CTL_ATEST1_SEL_SHFT                                                          0x4
#define HWIO_GCC_GPLL0_TEST_CTL_ATEST0_SEL_BMSK                                                          0xc
#define HWIO_GCC_GPLL0_TEST_CTL_ATEST0_SEL_SHFT                                                          0x2
#define HWIO_GCC_GPLL0_TEST_CTL_ATEST1_EN_BMSK                                                           0x2
#define HWIO_GCC_GPLL0_TEST_CTL_ATEST1_EN_SHFT                                                           0x1
#define HWIO_GCC_GPLL0_TEST_CTL_ATEST0_EN_BMSK                                                           0x1
#define HWIO_GCC_GPLL0_TEST_CTL_ATEST0_EN_SHFT                                                           0x0

#define HWIO_GCC_GPLL0_TEST_CTL_U_ADDR                                                            (GCC_CLK_CTL_REG_REG_BASE      + 0x00021020)
#define HWIO_GCC_GPLL0_TEST_CTL_U_RMSK                                                            0xffffffff
#define HWIO_GCC_GPLL0_TEST_CTL_U_IN          \
        in_dword_masked(HWIO_GCC_GPLL0_TEST_CTL_U_ADDR, HWIO_GCC_GPLL0_TEST_CTL_U_RMSK)
#define HWIO_GCC_GPLL0_TEST_CTL_U_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL0_TEST_CTL_U_ADDR, m)
#define HWIO_GCC_GPLL0_TEST_CTL_U_OUT(v)      \
        out_dword(HWIO_GCC_GPLL0_TEST_CTL_U_ADDR,v)
#define HWIO_GCC_GPLL0_TEST_CTL_U_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL0_TEST_CTL_U_ADDR,m,v,HWIO_GCC_GPLL0_TEST_CTL_U_IN)
#define HWIO_GCC_GPLL0_TEST_CTL_U_RESERVE_BITS31_14_BMSK                                          0xffffc000
#define HWIO_GCC_GPLL0_TEST_CTL_U_RESERVE_BITS31_14_SHFT                                                 0xe
#define HWIO_GCC_GPLL0_TEST_CTL_U_OVERRIDE_FINE_FCW_MSB_BMSK                                          0x2000
#define HWIO_GCC_GPLL0_TEST_CTL_U_OVERRIDE_FINE_FCW_MSB_SHFT                                             0xd
#define HWIO_GCC_GPLL0_TEST_CTL_U_DTEST_MODE_SEL_BMSK                                                 0x1800
#define HWIO_GCC_GPLL0_TEST_CTL_U_DTEST_MODE_SEL_SHFT                                                    0xb
#define HWIO_GCC_GPLL0_TEST_CTL_U_NMO_OSC_SEL_BMSK                                                     0x600
#define HWIO_GCC_GPLL0_TEST_CTL_U_NMO_OSC_SEL_SHFT                                                       0x9
#define HWIO_GCC_GPLL0_TEST_CTL_U_NMO_EN_BMSK                                                          0x100
#define HWIO_GCC_GPLL0_TEST_CTL_U_NMO_EN_SHFT                                                            0x8
#define HWIO_GCC_GPLL0_TEST_CTL_U_NOISE_MAG_BMSK                                                        0xe0
#define HWIO_GCC_GPLL0_TEST_CTL_U_NOISE_MAG_SHFT                                                         0x5
#define HWIO_GCC_GPLL0_TEST_CTL_U_NOISE_GEN_BMSK                                                        0x10
#define HWIO_GCC_GPLL0_TEST_CTL_U_NOISE_GEN_SHFT                                                         0x4
#define HWIO_GCC_GPLL0_TEST_CTL_U_OSC_BIAS_GND_BMSK                                                      0x8
#define HWIO_GCC_GPLL0_TEST_CTL_U_OSC_BIAS_GND_SHFT                                                      0x3
#define HWIO_GCC_GPLL0_TEST_CTL_U_PLL_TEST_OUT_SEL_BMSK                                                  0x6
#define HWIO_GCC_GPLL0_TEST_CTL_U_PLL_TEST_OUT_SEL_SHFT                                                  0x1
#define HWIO_GCC_GPLL0_TEST_CTL_U_CAL_CODE_UPDATE_BMSK                                                   0x1
#define HWIO_GCC_GPLL0_TEST_CTL_U_CAL_CODE_UPDATE_SHFT                                                   0x0

#define HWIO_GCC_GPLL0_FREQ_CTL_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x00021028)
#define HWIO_GCC_GPLL0_FREQ_CTL_RMSK                                                              0xffffffff
#define HWIO_GCC_GPLL0_FREQ_CTL_IN          \
        in_dword_masked(HWIO_GCC_GPLL0_FREQ_CTL_ADDR, HWIO_GCC_GPLL0_FREQ_CTL_RMSK)
#define HWIO_GCC_GPLL0_FREQ_CTL_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL0_FREQ_CTL_ADDR, m)
#define HWIO_GCC_GPLL0_FREQ_CTL_OUT(v)      \
        out_dword(HWIO_GCC_GPLL0_FREQ_CTL_ADDR,v)
#define HWIO_GCC_GPLL0_FREQ_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL0_FREQ_CTL_ADDR,m,v,HWIO_GCC_GPLL0_FREQ_CTL_IN)
#define HWIO_GCC_GPLL0_FREQ_CTL_FREQUENCY_CTL_WORD_BMSK                                           0xffffffff
#define HWIO_GCC_GPLL0_FREQ_CTL_FREQUENCY_CTL_WORD_SHFT                                                  0x0

#define HWIO_GCC_GPLL0_STATUS_ADDR                                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x00021024)
#define HWIO_GCC_GPLL0_STATUS_RMSK                                                                 0x7ffffff
#define HWIO_GCC_GPLL0_STATUS_IN          \
        in_dword_masked(HWIO_GCC_GPLL0_STATUS_ADDR, HWIO_GCC_GPLL0_STATUS_RMSK)
#define HWIO_GCC_GPLL0_STATUS_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL0_STATUS_ADDR, m)
#define HWIO_GCC_GPLL0_STATUS_STATUS_26_24_BMSK                                                    0x7000000
#define HWIO_GCC_GPLL0_STATUS_STATUS_26_24_SHFT                                                         0x18
#define HWIO_GCC_GPLL0_STATUS_STATUS_23_BMSK                                                        0x800000
#define HWIO_GCC_GPLL0_STATUS_STATUS_23_SHFT                                                            0x17
#define HWIO_GCC_GPLL0_STATUS_STATUS_22_20_BMSK                                                     0x700000
#define HWIO_GCC_GPLL0_STATUS_STATUS_22_20_SHFT                                                         0x14
#define HWIO_GCC_GPLL0_STATUS_STATUS_19_17_BMSK                                                      0xe0000
#define HWIO_GCC_GPLL0_STATUS_STATUS_19_17_SHFT                                                         0x11
#define HWIO_GCC_GPLL0_STATUS_STATUS_16_12_BMSK                                                      0x1f000
#define HWIO_GCC_GPLL0_STATUS_STATUS_16_12_SHFT                                                          0xc
#define HWIO_GCC_GPLL0_STATUS_STATUS_11_6_BMSK                                                         0xfc0
#define HWIO_GCC_GPLL0_STATUS_STATUS_11_6_SHFT                                                           0x6
#define HWIO_GCC_GPLL0_STATUS_STATUS_5_BMSK                                                             0x20
#define HWIO_GCC_GPLL0_STATUS_STATUS_5_SHFT                                                              0x5
#define HWIO_GCC_GPLL0_STATUS_STATUS_4_2_BMSK                                                           0x1c
#define HWIO_GCC_GPLL0_STATUS_STATUS_4_2_SHFT                                                            0x2
#define HWIO_GCC_GPLL0_STATUS_STATUS_1_BMSK                                                              0x2
#define HWIO_GCC_GPLL0_STATUS_STATUS_1_SHFT                                                              0x1
#define HWIO_GCC_GPLL0_STATUS_STATUS_0_BMSK                                                              0x1
#define HWIO_GCC_GPLL0_STATUS_STATUS_0_SHFT                                                              0x0

#define HWIO_GCC_GPLL1_MODE_ADDR                                                                  (GCC_CLK_CTL_REG_REG_BASE      + 0x00020000)
#define HWIO_GCC_GPLL1_MODE_RMSK                                                                  0x20ffff0f
#define HWIO_GCC_GPLL1_MODE_IN          \
        in_dword_masked(HWIO_GCC_GPLL1_MODE_ADDR, HWIO_GCC_GPLL1_MODE_RMSK)
#define HWIO_GCC_GPLL1_MODE_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL1_MODE_ADDR, m)
#define HWIO_GCC_GPLL1_MODE_OUT(v)      \
        out_dword(HWIO_GCC_GPLL1_MODE_ADDR,v)
#define HWIO_GCC_GPLL1_MODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL1_MODE_ADDR,m,v,HWIO_GCC_GPLL1_MODE_IN)
#define HWIO_GCC_GPLL1_MODE_PLL_ACK_LATCH_BMSK                                                    0x20000000
#define HWIO_GCC_GPLL1_MODE_PLL_ACK_LATCH_SHFT                                                          0x1d
#define HWIO_GCC_GPLL1_MODE_PLL_HW_UPADATE_LOGIC_BYPASS_BMSK                                        0x800000
#define HWIO_GCC_GPLL1_MODE_PLL_HW_UPADATE_LOGIC_BYPASS_SHFT                                            0x17
#define HWIO_GCC_GPLL1_MODE_PLL_UPDATE_BMSK                                                         0x400000
#define HWIO_GCC_GPLL1_MODE_PLL_UPDATE_SHFT                                                             0x16
#define HWIO_GCC_GPLL1_MODE_PLL_VOTE_FSM_RESET_BMSK                                                 0x200000
#define HWIO_GCC_GPLL1_MODE_PLL_VOTE_FSM_RESET_SHFT                                                     0x15
#define HWIO_GCC_GPLL1_MODE_PLL_VOTE_FSM_ENA_BMSK                                                   0x100000
#define HWIO_GCC_GPLL1_MODE_PLL_VOTE_FSM_ENA_SHFT                                                       0x14
#define HWIO_GCC_GPLL1_MODE_PLL_BIAS_COUNT_BMSK                                                      0xfc000
#define HWIO_GCC_GPLL1_MODE_PLL_BIAS_COUNT_SHFT                                                          0xe
#define HWIO_GCC_GPLL1_MODE_PLL_LOCK_COUNT_BMSK                                                       0x3f00
#define HWIO_GCC_GPLL1_MODE_PLL_LOCK_COUNT_SHFT                                                          0x8
#define HWIO_GCC_GPLL1_MODE_PLL_PLLTEST_BMSK                                                             0x8
#define HWIO_GCC_GPLL1_MODE_PLL_PLLTEST_SHFT                                                             0x3
#define HWIO_GCC_GPLL1_MODE_PLL_RESET_N_BMSK                                                             0x4
#define HWIO_GCC_GPLL1_MODE_PLL_RESET_N_SHFT                                                             0x2
#define HWIO_GCC_GPLL1_MODE_PLL_BYPASSNL_BMSK                                                            0x2
#define HWIO_GCC_GPLL1_MODE_PLL_BYPASSNL_SHFT                                                            0x1
#define HWIO_GCC_GPLL1_MODE_PLL_OUTCTRL_BMSK                                                             0x1
#define HWIO_GCC_GPLL1_MODE_PLL_OUTCTRL_SHFT                                                             0x0

#define HWIO_GCC_GPLL1_L_VAL_ADDR                                                                 (GCC_CLK_CTL_REG_REG_BASE      + 0x00020004)
#define HWIO_GCC_GPLL1_L_VAL_RMSK                                                                       0xff
#define HWIO_GCC_GPLL1_L_VAL_IN          \
        in_dword_masked(HWIO_GCC_GPLL1_L_VAL_ADDR, HWIO_GCC_GPLL1_L_VAL_RMSK)
#define HWIO_GCC_GPLL1_L_VAL_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL1_L_VAL_ADDR, m)
#define HWIO_GCC_GPLL1_L_VAL_OUT(v)      \
        out_dword(HWIO_GCC_GPLL1_L_VAL_ADDR,v)
#define HWIO_GCC_GPLL1_L_VAL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL1_L_VAL_ADDR,m,v,HWIO_GCC_GPLL1_L_VAL_IN)
#define HWIO_GCC_GPLL1_L_VAL_PLL_L_BMSK                                                                 0xff
#define HWIO_GCC_GPLL1_L_VAL_PLL_L_SHFT                                                                  0x0

#define HWIO_GCC_GPLL1_ALPHA_VAL_ADDR                                                             (GCC_CLK_CTL_REG_REG_BASE      + 0x00020008)
#define HWIO_GCC_GPLL1_ALPHA_VAL_RMSK                                                             0xffffffff
#define HWIO_GCC_GPLL1_ALPHA_VAL_IN          \
        in_dword_masked(HWIO_GCC_GPLL1_ALPHA_VAL_ADDR, HWIO_GCC_GPLL1_ALPHA_VAL_RMSK)
#define HWIO_GCC_GPLL1_ALPHA_VAL_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL1_ALPHA_VAL_ADDR, m)
#define HWIO_GCC_GPLL1_ALPHA_VAL_OUT(v)      \
        out_dword(HWIO_GCC_GPLL1_ALPHA_VAL_ADDR,v)
#define HWIO_GCC_GPLL1_ALPHA_VAL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL1_ALPHA_VAL_ADDR,m,v,HWIO_GCC_GPLL1_ALPHA_VAL_IN)
#define HWIO_GCC_GPLL1_ALPHA_VAL_PLL_ALPHA_BMSK                                                   0xffffffff
#define HWIO_GCC_GPLL1_ALPHA_VAL_PLL_ALPHA_SHFT                                                          0x0

#define HWIO_GCC_GPLL1_ALPHA_VAL_U_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x0002000c)
#define HWIO_GCC_GPLL1_ALPHA_VAL_U_RMSK                                                                 0xff
#define HWIO_GCC_GPLL1_ALPHA_VAL_U_IN          \
        in_dword_masked(HWIO_GCC_GPLL1_ALPHA_VAL_U_ADDR, HWIO_GCC_GPLL1_ALPHA_VAL_U_RMSK)
#define HWIO_GCC_GPLL1_ALPHA_VAL_U_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL1_ALPHA_VAL_U_ADDR, m)
#define HWIO_GCC_GPLL1_ALPHA_VAL_U_OUT(v)      \
        out_dword(HWIO_GCC_GPLL1_ALPHA_VAL_U_ADDR,v)
#define HWIO_GCC_GPLL1_ALPHA_VAL_U_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL1_ALPHA_VAL_U_ADDR,m,v,HWIO_GCC_GPLL1_ALPHA_VAL_U_IN)
#define HWIO_GCC_GPLL1_ALPHA_VAL_U_PLL_ALPHA_BMSK                                                       0xff
#define HWIO_GCC_GPLL1_ALPHA_VAL_U_PLL_ALPHA_SHFT                                                        0x0

#define HWIO_GCC_GPLL1_USER_CTL_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x00020010)
#define HWIO_GCC_GPLL1_USER_CTL_RMSK                                                               0x100339f
#define HWIO_GCC_GPLL1_USER_CTL_IN          \
        in_dword_masked(HWIO_GCC_GPLL1_USER_CTL_ADDR, HWIO_GCC_GPLL1_USER_CTL_RMSK)
#define HWIO_GCC_GPLL1_USER_CTL_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL1_USER_CTL_ADDR, m)
#define HWIO_GCC_GPLL1_USER_CTL_OUT(v)      \
        out_dword(HWIO_GCC_GPLL1_USER_CTL_ADDR,v)
#define HWIO_GCC_GPLL1_USER_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL1_USER_CTL_ADDR,m,v,HWIO_GCC_GPLL1_USER_CTL_IN)
#define HWIO_GCC_GPLL1_USER_CTL_MN_EN_BMSK                                                         0x1000000
#define HWIO_GCC_GPLL1_USER_CTL_MN_EN_SHFT                                                              0x18
#define HWIO_GCC_GPLL1_USER_CTL_PRE_DIV_RATIO_BMSK                                                    0x3000
#define HWIO_GCC_GPLL1_USER_CTL_PRE_DIV_RATIO_SHFT                                                       0xc
#define HWIO_GCC_GPLL1_USER_CTL_POST_DIV_RATIO_BMSK                                                    0x300
#define HWIO_GCC_GPLL1_USER_CTL_POST_DIV_RATIO_SHFT                                                      0x8
#define HWIO_GCC_GPLL1_USER_CTL_OUTPUT_INV_BMSK                                                         0x80
#define HWIO_GCC_GPLL1_USER_CTL_OUTPUT_INV_SHFT                                                          0x7
#define HWIO_GCC_GPLL1_USER_CTL_PLLOUT_LV_TEST_BMSK                                                     0x10
#define HWIO_GCC_GPLL1_USER_CTL_PLLOUT_LV_TEST_SHFT                                                      0x4
#define HWIO_GCC_GPLL1_USER_CTL_PLLOUT_LV_EARLY_BMSK                                                     0x8
#define HWIO_GCC_GPLL1_USER_CTL_PLLOUT_LV_EARLY_SHFT                                                     0x3
#define HWIO_GCC_GPLL1_USER_CTL_PLLOUT_LV_AUX2_BMSK                                                      0x4
#define HWIO_GCC_GPLL1_USER_CTL_PLLOUT_LV_AUX2_SHFT                                                      0x2
#define HWIO_GCC_GPLL1_USER_CTL_PLLOUT_LV_AUX_BMSK                                                       0x2
#define HWIO_GCC_GPLL1_USER_CTL_PLLOUT_LV_AUX_SHFT                                                       0x1
#define HWIO_GCC_GPLL1_USER_CTL_PLLOUT_LV_MAIN_BMSK                                                      0x1
#define HWIO_GCC_GPLL1_USER_CTL_PLLOUT_LV_MAIN_SHFT                                                      0x0

#define HWIO_GCC_GPLL1_CONFIG_CTL_ADDR                                                            (GCC_CLK_CTL_REG_REG_BASE      + 0x00020018)
#define HWIO_GCC_GPLL1_CONFIG_CTL_RMSK                                                            0xff1fffff
#define HWIO_GCC_GPLL1_CONFIG_CTL_IN          \
        in_dword_masked(HWIO_GCC_GPLL1_CONFIG_CTL_ADDR, HWIO_GCC_GPLL1_CONFIG_CTL_RMSK)
#define HWIO_GCC_GPLL1_CONFIG_CTL_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL1_CONFIG_CTL_ADDR, m)
#define HWIO_GCC_GPLL1_CONFIG_CTL_OUT(v)      \
        out_dword(HWIO_GCC_GPLL1_CONFIG_CTL_ADDR,v)
#define HWIO_GCC_GPLL1_CONFIG_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL1_CONFIG_CTL_ADDR,m,v,HWIO_GCC_GPLL1_CONFIG_CTL_IN)
#define HWIO_GCC_GPLL1_CONFIG_CTL_PLLBW_CNT_BMSK                                                  0xff000000
#define HWIO_GCC_GPLL1_CONFIG_CTL_PLLBW_CNT_SHFT                                                        0x18
#define HWIO_GCC_GPLL1_CONFIG_CTL_PLLBW_EN_BMSK                                                     0x100000
#define HWIO_GCC_GPLL1_CONFIG_CTL_PLLBW_EN_SHFT                                                         0x14
#define HWIO_GCC_GPLL1_CONFIG_CTL_PLLBW_UPDATE_BMSK                                                  0x80000
#define HWIO_GCC_GPLL1_CONFIG_CTL_PLLBW_UPDATE_SHFT                                                     0x13
#define HWIO_GCC_GPLL1_CONFIG_CTL_PLLBW_ICPF_BMSK                                                    0x40000
#define HWIO_GCC_GPLL1_CONFIG_CTL_PLLBW_ICPF_SHFT                                                       0x12
#define HWIO_GCC_GPLL1_CONFIG_CTL_DITHER_SEL_BMSK                                                    0x30000
#define HWIO_GCC_GPLL1_CONFIG_CTL_DITHER_SEL_SHFT                                                       0x10
#define HWIO_GCC_GPLL1_CONFIG_CTL_CORR_EN_BMSK                                                        0x8000
#define HWIO_GCC_GPLL1_CONFIG_CTL_CORR_EN_SHFT                                                           0xf
#define HWIO_GCC_GPLL1_CONFIG_CTL_RVSIG_DEL_BMSK                                                      0x6000
#define HWIO_GCC_GPLL1_CONFIG_CTL_RVSIG_DEL_SHFT                                                         0xd
#define HWIO_GCC_GPLL1_CONFIG_CTL_PFD_DZSEL_BMSK                                                      0x1800
#define HWIO_GCC_GPLL1_CONFIG_CTL_PFD_DZSEL_SHFT                                                         0xb
#define HWIO_GCC_GPLL1_CONFIG_CTL_FORCE_ISEED_BMSK                                                     0x400
#define HWIO_GCC_GPLL1_CONFIG_CTL_FORCE_ISEED_SHFT                                                       0xa
#define HWIO_GCC_GPLL1_CONFIG_CTL_CPI_2X_BMSK                                                          0x200
#define HWIO_GCC_GPLL1_CONFIG_CTL_CPI_2X_SHFT                                                            0x9
#define HWIO_GCC_GPLL1_CONFIG_CTL_CPI_CNT_BMSK                                                         0x1c0
#define HWIO_GCC_GPLL1_CONFIG_CTL_CPI_CNT_SHFT                                                           0x6
#define HWIO_GCC_GPLL1_CONFIG_CTL_FILT_BS_CNTL_BMSK                                                     0x30
#define HWIO_GCC_GPLL1_CONFIG_CTL_FILT_BS_CNTL_SHFT                                                      0x4
#define HWIO_GCC_GPLL1_CONFIG_CTL_SEL_IREG_OSC_BMSK                                                      0xc
#define HWIO_GCC_GPLL1_CONFIG_CTL_SEL_IREG_OSC_SHFT                                                      0x2
#define HWIO_GCC_GPLL1_CONFIG_CTL_CFG_LOCKDET_BMSK                                                       0x3
#define HWIO_GCC_GPLL1_CONFIG_CTL_CFG_LOCKDET_SHFT                                                       0x0

#define HWIO_GCC_GPLL1_TEST_CTL_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x0002001c)
#define HWIO_GCC_GPLL1_TEST_CTL_RMSK                                                              0xffffe3ff
#define HWIO_GCC_GPLL1_TEST_CTL_IN          \
        in_dword_masked(HWIO_GCC_GPLL1_TEST_CTL_ADDR, HWIO_GCC_GPLL1_TEST_CTL_RMSK)
#define HWIO_GCC_GPLL1_TEST_CTL_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL1_TEST_CTL_ADDR, m)
#define HWIO_GCC_GPLL1_TEST_CTL_OUT(v)      \
        out_dword(HWIO_GCC_GPLL1_TEST_CTL_ADDR,v)
#define HWIO_GCC_GPLL1_TEST_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL1_TEST_CTL_ADDR,m,v,HWIO_GCC_GPLL1_TEST_CTL_IN)
#define HWIO_GCC_GPLL1_TEST_CTL_PLLBW_CNTNUM_BMSK                                                 0xc0000000
#define HWIO_GCC_GPLL1_TEST_CTL_PLLBW_CNTNUM_SHFT                                                       0x1e
#define HWIO_GCC_GPLL1_TEST_CTL_PLLBW_DELTA_BMSK                                                  0x38000000
#define HWIO_GCC_GPLL1_TEST_CTL_PLLBW_DELTA_SHFT                                                        0x1b
#define HWIO_GCC_GPLL1_TEST_CTL_PLLBW_ERR_BMSK                                                     0x7800000
#define HWIO_GCC_GPLL1_TEST_CTL_PLLBW_ERR_SHFT                                                          0x17
#define HWIO_GCC_GPLL1_TEST_CTL_NOISE_CURR_SEL_BMSK                                                 0x700000
#define HWIO_GCC_GPLL1_TEST_CTL_NOISE_CURR_SEL_SHFT                                                     0x14
#define HWIO_GCC_GPLL1_TEST_CTL_PUP_EN_BMSK                                                          0x80000
#define HWIO_GCC_GPLL1_TEST_CTL_PUP_EN_SHFT                                                             0x13
#define HWIO_GCC_GPLL1_TEST_CTL_NOISE_OSC_CFG_BMSK                                                   0x60000
#define HWIO_GCC_GPLL1_TEST_CTL_NOISE_OSC_CFG_SHFT                                                      0x11
#define HWIO_GCC_GPLL1_TEST_CTL_TEST_OUT_SEL_BMSK                                                    0x10000
#define HWIO_GCC_GPLL1_TEST_CTL_TEST_OUT_SEL_SHFT                                                       0x10
#define HWIO_GCC_GPLL1_TEST_CTL_PLLBW_WAIT_BMSK                                                       0xc000
#define HWIO_GCC_GPLL1_TEST_CTL_PLLBW_WAIT_SHFT                                                          0xe
#define HWIO_GCC_GPLL1_TEST_CTL_CNT_MSB_SEL_BMSK                                                      0x2000
#define HWIO_GCC_GPLL1_TEST_CTL_CNT_MSB_SEL_SHFT                                                         0xd
#define HWIO_GCC_GPLL1_TEST_CTL_SEL_IEXT_BMSK                                                          0x200
#define HWIO_GCC_GPLL1_TEST_CTL_SEL_IEXT_SHFT                                                            0x9
#define HWIO_GCC_GPLL1_TEST_CTL_DTEST_SEL_BMSK                                                         0x180
#define HWIO_GCC_GPLL1_TEST_CTL_DTEST_SEL_SHFT                                                           0x7
#define HWIO_GCC_GPLL1_TEST_CTL_BYP_TESTAMP_BMSK                                                        0x40
#define HWIO_GCC_GPLL1_TEST_CTL_BYP_TESTAMP_SHFT                                                         0x6
#define HWIO_GCC_GPLL1_TEST_CTL_ATEST1_SEL_BMSK                                                         0x30
#define HWIO_GCC_GPLL1_TEST_CTL_ATEST1_SEL_SHFT                                                          0x4
#define HWIO_GCC_GPLL1_TEST_CTL_ATEST0_SEL_BMSK                                                          0xc
#define HWIO_GCC_GPLL1_TEST_CTL_ATEST0_SEL_SHFT                                                          0x2
#define HWIO_GCC_GPLL1_TEST_CTL_ATEST1_EN_BMSK                                                           0x2
#define HWIO_GCC_GPLL1_TEST_CTL_ATEST1_EN_SHFT                                                           0x1
#define HWIO_GCC_GPLL1_TEST_CTL_ATEST0_EN_BMSK                                                           0x1
#define HWIO_GCC_GPLL1_TEST_CTL_ATEST0_EN_SHFT                                                           0x0

#define HWIO_GCC_GPLL1_STATUS_ADDR                                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x00020024)
#define HWIO_GCC_GPLL1_STATUS_RMSK                                                                   0x2ffff
#define HWIO_GCC_GPLL1_STATUS_IN          \
        in_dword_masked(HWIO_GCC_GPLL1_STATUS_ADDR, HWIO_GCC_GPLL1_STATUS_RMSK)
#define HWIO_GCC_GPLL1_STATUS_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL1_STATUS_ADDR, m)
#define HWIO_GCC_GPLL1_STATUS_PLL_ACTIVE_FLAG_BMSK                                                   0x20000
#define HWIO_GCC_GPLL1_STATUS_PLL_ACTIVE_FLAG_SHFT                                                      0x11
#define HWIO_GCC_GPLL1_STATUS_PLL_BW_CAL_CNT_BMSK                                                     0xfe00
#define HWIO_GCC_GPLL1_STATUS_PLL_BW_CAL_CNT_SHFT                                                        0x9
#define HWIO_GCC_GPLL1_STATUS_PLL_BW_CAL_ERR_BMSK                                                      0x100
#define HWIO_GCC_GPLL1_STATUS_PLL_BW_CAL_ERR_SHFT                                                        0x8
#define HWIO_GCC_GPLL1_STATUS_PLL_BW_CAL_DONE_BMSK                                                      0x80
#define HWIO_GCC_GPLL1_STATUS_PLL_BW_CAL_DONE_SHFT                                                       0x7
#define HWIO_GCC_GPLL1_STATUS_PLL_CHARGE_PUMP_CTL_BMSK                                                  0x78
#define HWIO_GCC_GPLL1_STATUS_PLL_CHARGE_PUMP_CTL_SHFT                                                   0x3
#define HWIO_GCC_GPLL1_STATUS_PLL_LOCK_DET_BMSK                                                          0x4
#define HWIO_GCC_GPLL1_STATUS_PLL_LOCK_DET_SHFT                                                          0x2
#define HWIO_GCC_GPLL1_STATUS_PLL_ACK_LATCH_BMSK                                                         0x2
#define HWIO_GCC_GPLL1_STATUS_PLL_ACK_LATCH_SHFT                                                         0x1
#define HWIO_GCC_GPLL1_STATUS_PLL_CLOCK_DET_BMSK                                                         0x1
#define HWIO_GCC_GPLL1_STATUS_PLL_CLOCK_DET_SHFT                                                         0x0

#define HWIO_GCC_GPLL3_MODE_ADDR                                                                  (GCC_CLK_CTL_REG_REG_BASE      + 0x00023000)
#define HWIO_GCC_GPLL3_MODE_RMSK                                                                  0xe0ffff0f
#define HWIO_GCC_GPLL3_MODE_IN          \
        in_dword_masked(HWIO_GCC_GPLL3_MODE_ADDR, HWIO_GCC_GPLL3_MODE_RMSK)
#define HWIO_GCC_GPLL3_MODE_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL3_MODE_ADDR, m)
#define HWIO_GCC_GPLL3_MODE_OUT(v)      \
        out_dword(HWIO_GCC_GPLL3_MODE_ADDR,v)
#define HWIO_GCC_GPLL3_MODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL3_MODE_ADDR,m,v,HWIO_GCC_GPLL3_MODE_IN)
#define HWIO_GCC_GPLL3_MODE_PLL_LOCK_DET_BMSK                                                     0x80000000
#define HWIO_GCC_GPLL3_MODE_PLL_LOCK_DET_SHFT                                                           0x1f
#define HWIO_GCC_GPLL3_MODE_PLL_ACTIVE_FLAG_BMSK                                                  0x40000000
#define HWIO_GCC_GPLL3_MODE_PLL_ACTIVE_FLAG_SHFT                                                        0x1e
#define HWIO_GCC_GPLL3_MODE_PLL_ACK_LATCH_BMSK                                                    0x20000000
#define HWIO_GCC_GPLL3_MODE_PLL_ACK_LATCH_SHFT                                                          0x1d
#define HWIO_GCC_GPLL3_MODE_PLL_HW_UPADATE_LOGIC_BYPASS_BMSK                                        0x800000
#define HWIO_GCC_GPLL3_MODE_PLL_HW_UPADATE_LOGIC_BYPASS_SHFT                                            0x17
#define HWIO_GCC_GPLL3_MODE_PLL_UPDATE_BMSK                                                         0x400000
#define HWIO_GCC_GPLL3_MODE_PLL_UPDATE_SHFT                                                             0x16
#define HWIO_GCC_GPLL3_MODE_PLL_VOTE_FSM_RESET_BMSK                                                 0x200000
#define HWIO_GCC_GPLL3_MODE_PLL_VOTE_FSM_RESET_SHFT                                                     0x15
#define HWIO_GCC_GPLL3_MODE_PLL_VOTE_FSM_ENA_BMSK                                                   0x100000
#define HWIO_GCC_GPLL3_MODE_PLL_VOTE_FSM_ENA_SHFT                                                       0x14
#define HWIO_GCC_GPLL3_MODE_PLL_BIAS_COUNT_BMSK                                                      0xfc000
#define HWIO_GCC_GPLL3_MODE_PLL_BIAS_COUNT_SHFT                                                          0xe
#define HWIO_GCC_GPLL3_MODE_PLL_LOCK_COUNT_BMSK                                                       0x3f00
#define HWIO_GCC_GPLL3_MODE_PLL_LOCK_COUNT_SHFT                                                          0x8
#define HWIO_GCC_GPLL3_MODE_PLL_PLLTEST_BMSK                                                             0x8
#define HWIO_GCC_GPLL3_MODE_PLL_PLLTEST_SHFT                                                             0x3
#define HWIO_GCC_GPLL3_MODE_PLL_RESET_N_BMSK                                                             0x4
#define HWIO_GCC_GPLL3_MODE_PLL_RESET_N_SHFT                                                             0x2
#define HWIO_GCC_GPLL3_MODE_PLL_BYPASSNL_BMSK                                                            0x2
#define HWIO_GCC_GPLL3_MODE_PLL_BYPASSNL_SHFT                                                            0x1
#define HWIO_GCC_GPLL3_MODE_PLL_OUTCTRL_BMSK                                                             0x1
#define HWIO_GCC_GPLL3_MODE_PLL_OUTCTRL_SHFT                                                             0x0

#define HWIO_GCC_GPLL3_L_VAL_ADDR                                                                 (GCC_CLK_CTL_REG_REG_BASE      + 0x00023004)
#define HWIO_GCC_GPLL3_L_VAL_RMSK                                                                     0xffff
#define HWIO_GCC_GPLL3_L_VAL_IN          \
        in_dword_masked(HWIO_GCC_GPLL3_L_VAL_ADDR, HWIO_GCC_GPLL3_L_VAL_RMSK)
#define HWIO_GCC_GPLL3_L_VAL_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL3_L_VAL_ADDR, m)
#define HWIO_GCC_GPLL3_L_VAL_OUT(v)      \
        out_dword(HWIO_GCC_GPLL3_L_VAL_ADDR,v)
#define HWIO_GCC_GPLL3_L_VAL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL3_L_VAL_ADDR,m,v,HWIO_GCC_GPLL3_L_VAL_IN)
#define HWIO_GCC_GPLL3_L_VAL_PLL_L_BMSK                                                               0xffff
#define HWIO_GCC_GPLL3_L_VAL_PLL_L_SHFT                                                                  0x0

#define HWIO_GCC_GPLL3_ALPHA_VAL_ADDR                                                             (GCC_CLK_CTL_REG_REG_BASE      + 0x00023008)
#define HWIO_GCC_GPLL3_ALPHA_VAL_RMSK                                                             0xffffffff
#define HWIO_GCC_GPLL3_ALPHA_VAL_IN          \
        in_dword_masked(HWIO_GCC_GPLL3_ALPHA_VAL_ADDR, HWIO_GCC_GPLL3_ALPHA_VAL_RMSK)
#define HWIO_GCC_GPLL3_ALPHA_VAL_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL3_ALPHA_VAL_ADDR, m)
#define HWIO_GCC_GPLL3_ALPHA_VAL_OUT(v)      \
        out_dword(HWIO_GCC_GPLL3_ALPHA_VAL_ADDR,v)
#define HWIO_GCC_GPLL3_ALPHA_VAL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL3_ALPHA_VAL_ADDR,m,v,HWIO_GCC_GPLL3_ALPHA_VAL_IN)
#define HWIO_GCC_GPLL3_ALPHA_VAL_PLL_ALPHA_BMSK                                                   0xffffffff
#define HWIO_GCC_GPLL3_ALPHA_VAL_PLL_ALPHA_SHFT                                                          0x0

#define HWIO_GCC_GPLL3_ALPHA_VAL_U_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x0002300c)
#define HWIO_GCC_GPLL3_ALPHA_VAL_U_RMSK                                                                 0xff
#define HWIO_GCC_GPLL3_ALPHA_VAL_U_IN          \
        in_dword_masked(HWIO_GCC_GPLL3_ALPHA_VAL_U_ADDR, HWIO_GCC_GPLL3_ALPHA_VAL_U_RMSK)
#define HWIO_GCC_GPLL3_ALPHA_VAL_U_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL3_ALPHA_VAL_U_ADDR, m)
#define HWIO_GCC_GPLL3_ALPHA_VAL_U_OUT(v)      \
        out_dword(HWIO_GCC_GPLL3_ALPHA_VAL_U_ADDR,v)
#define HWIO_GCC_GPLL3_ALPHA_VAL_U_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL3_ALPHA_VAL_U_ADDR,m,v,HWIO_GCC_GPLL3_ALPHA_VAL_U_IN)
#define HWIO_GCC_GPLL3_ALPHA_VAL_U_PLL_ALPHA_BMSK                                                       0xff
#define HWIO_GCC_GPLL3_ALPHA_VAL_U_PLL_ALPHA_SHFT                                                        0x0

#define HWIO_GCC_GPLL3_USER_CTL_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x00023010)
#define HWIO_GCC_GPLL3_USER_CTL_RMSK                                                              0xffffffff
#define HWIO_GCC_GPLL3_USER_CTL_IN          \
        in_dword_masked(HWIO_GCC_GPLL3_USER_CTL_ADDR, HWIO_GCC_GPLL3_USER_CTL_RMSK)
#define HWIO_GCC_GPLL3_USER_CTL_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL3_USER_CTL_ADDR, m)
#define HWIO_GCC_GPLL3_USER_CTL_OUT(v)      \
        out_dword(HWIO_GCC_GPLL3_USER_CTL_ADDR,v)
#define HWIO_GCC_GPLL3_USER_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL3_USER_CTL_ADDR,m,v,HWIO_GCC_GPLL3_USER_CTL_IN)
#define HWIO_GCC_GPLL3_USER_CTL_RESERVE_BITS31_28_BMSK                                            0xf0000000
#define HWIO_GCC_GPLL3_USER_CTL_RESERVE_BITS31_28_SHFT                                                  0x1c
#define HWIO_GCC_GPLL3_USER_CTL_SSC_MODE_CONTROL_BMSK                                              0x8000000
#define HWIO_GCC_GPLL3_USER_CTL_SSC_MODE_CONTROL_SHFT                                                   0x1b
#define HWIO_GCC_GPLL3_USER_CTL_RESERVE_BITS26_25_BMSK                                             0x6000000
#define HWIO_GCC_GPLL3_USER_CTL_RESERVE_BITS26_25_SHFT                                                  0x19
#define HWIO_GCC_GPLL3_USER_CTL_ALPHA_EN_BMSK                                                      0x1000000
#define HWIO_GCC_GPLL3_USER_CTL_ALPHA_EN_SHFT                                                           0x18
#define HWIO_GCC_GPLL3_USER_CTL_RESERVE_BITS23_22_BMSK                                              0xc00000
#define HWIO_GCC_GPLL3_USER_CTL_RESERVE_BITS23_22_SHFT                                                  0x16
#define HWIO_GCC_GPLL3_USER_CTL_VCO_SEL_BMSK                                                        0x300000
#define HWIO_GCC_GPLL3_USER_CTL_VCO_SEL_SHFT                                                            0x14
#define HWIO_GCC_GPLL3_USER_CTL_RESERVE_BITS19_15_BMSK                                               0xf8000
#define HWIO_GCC_GPLL3_USER_CTL_RESERVE_BITS19_15_SHFT                                                   0xf
#define HWIO_GCC_GPLL3_USER_CTL_PRE_DIV_RATIO_BMSK                                                    0x7000
#define HWIO_GCC_GPLL3_USER_CTL_PRE_DIV_RATIO_SHFT                                                       0xc
#define HWIO_GCC_GPLL3_USER_CTL_POST_DIV_RATIO_BMSK                                                    0xf00
#define HWIO_GCC_GPLL3_USER_CTL_POST_DIV_RATIO_SHFT                                                      0x8
#define HWIO_GCC_GPLL3_USER_CTL_OUTPUT_INV_BMSK                                                         0x80
#define HWIO_GCC_GPLL3_USER_CTL_OUTPUT_INV_SHFT                                                          0x7
#define HWIO_GCC_GPLL3_USER_CTL_RESERVE_BITS6_5_BMSK                                                    0x60
#define HWIO_GCC_GPLL3_USER_CTL_RESERVE_BITS6_5_SHFT                                                     0x5
#define HWIO_GCC_GPLL3_USER_CTL_PLLOUT_LV_TEST_BMSK                                                     0x10
#define HWIO_GCC_GPLL3_USER_CTL_PLLOUT_LV_TEST_SHFT                                                      0x4
#define HWIO_GCC_GPLL3_USER_CTL_PLLOUT_LV_EARLY_BMSK                                                     0x8
#define HWIO_GCC_GPLL3_USER_CTL_PLLOUT_LV_EARLY_SHFT                                                     0x3
#define HWIO_GCC_GPLL3_USER_CTL_PLLOUT_LV_AUX2_BMSK                                                      0x4
#define HWIO_GCC_GPLL3_USER_CTL_PLLOUT_LV_AUX2_SHFT                                                      0x2
#define HWIO_GCC_GPLL3_USER_CTL_PLLOUT_LV_AUX_BMSK                                                       0x2
#define HWIO_GCC_GPLL3_USER_CTL_PLLOUT_LV_AUX_SHFT                                                       0x1
#define HWIO_GCC_GPLL3_USER_CTL_PLLOUT_LV_MAIN_BMSK                                                      0x1
#define HWIO_GCC_GPLL3_USER_CTL_PLLOUT_LV_MAIN_SHFT                                                      0x0

#define HWIO_GCC_GPLL3_USER_CTL_U_ADDR                                                            (GCC_CLK_CTL_REG_REG_BASE      + 0x00023014)
#define HWIO_GCC_GPLL3_USER_CTL_U_RMSK                                                            0xffffffff
#define HWIO_GCC_GPLL3_USER_CTL_U_IN          \
        in_dword_masked(HWIO_GCC_GPLL3_USER_CTL_U_ADDR, HWIO_GCC_GPLL3_USER_CTL_U_RMSK)
#define HWIO_GCC_GPLL3_USER_CTL_U_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL3_USER_CTL_U_ADDR, m)
#define HWIO_GCC_GPLL3_USER_CTL_U_OUT(v)      \
        out_dword(HWIO_GCC_GPLL3_USER_CTL_U_ADDR,v)
#define HWIO_GCC_GPLL3_USER_CTL_U_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL3_USER_CTL_U_ADDR,m,v,HWIO_GCC_GPLL3_USER_CTL_U_IN)
#define HWIO_GCC_GPLL3_USER_CTL_U_RESERVE_BITS31_12_BMSK                                          0xfffff000
#define HWIO_GCC_GPLL3_USER_CTL_U_RESERVE_BITS31_12_SHFT                                                 0xc
#define HWIO_GCC_GPLL3_USER_CTL_U_LATCH_INTERFACE_BYPASS_BMSK                                          0x800
#define HWIO_GCC_GPLL3_USER_CTL_U_LATCH_INTERFACE_BYPASS_SHFT                                            0xb
#define HWIO_GCC_GPLL3_USER_CTL_U_STATUS_REGISTER_BMSK                                                 0x700
#define HWIO_GCC_GPLL3_USER_CTL_U_STATUS_REGISTER_SHFT                                                   0x8
#define HWIO_GCC_GPLL3_USER_CTL_U_DSM_BMSK                                                              0x80
#define HWIO_GCC_GPLL3_USER_CTL_U_DSM_SHFT                                                               0x7
#define HWIO_GCC_GPLL3_USER_CTL_U_WRITE_STATE_BMSK                                                      0x40
#define HWIO_GCC_GPLL3_USER_CTL_U_WRITE_STATE_SHFT                                                       0x6
#define HWIO_GCC_GPLL3_USER_CTL_U_TARGET_CTL_BMSK                                                       0x38
#define HWIO_GCC_GPLL3_USER_CTL_U_TARGET_CTL_SHFT                                                        0x3
#define HWIO_GCC_GPLL3_USER_CTL_U_LOCK_DET_BMSK                                                          0x4
#define HWIO_GCC_GPLL3_USER_CTL_U_LOCK_DET_SHFT                                                          0x2
#define HWIO_GCC_GPLL3_USER_CTL_U_FREEZE_PLL_BMSK                                                        0x2
#define HWIO_GCC_GPLL3_USER_CTL_U_FREEZE_PLL_SHFT                                                        0x1
#define HWIO_GCC_GPLL3_USER_CTL_U_TOGGLE_DET_BMSK                                                        0x1
#define HWIO_GCC_GPLL3_USER_CTL_U_TOGGLE_DET_SHFT                                                        0x0

#define HWIO_GCC_GPLL3_CONFIG_CTL_ADDR                                                            (GCC_CLK_CTL_REG_REG_BASE      + 0x00023018)
#define HWIO_GCC_GPLL3_CONFIG_CTL_RMSK                                                            0xffffffff
#define HWIO_GCC_GPLL3_CONFIG_CTL_IN          \
        in_dword_masked(HWIO_GCC_GPLL3_CONFIG_CTL_ADDR, HWIO_GCC_GPLL3_CONFIG_CTL_RMSK)
#define HWIO_GCC_GPLL3_CONFIG_CTL_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL3_CONFIG_CTL_ADDR, m)
#define HWIO_GCC_GPLL3_CONFIG_CTL_OUT(v)      \
        out_dword(HWIO_GCC_GPLL3_CONFIG_CTL_ADDR,v)
#define HWIO_GCC_GPLL3_CONFIG_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL3_CONFIG_CTL_ADDR,m,v,HWIO_GCC_GPLL3_CONFIG_CTL_IN)
#define HWIO_GCC_GPLL3_CONFIG_CTL_SINGLE_DMET_MODE_ENABLE_BMSK                                    0x80000000
#define HWIO_GCC_GPLL3_CONFIG_CTL_SINGLE_DMET_MODE_ENABLE_SHFT                                          0x1f
#define HWIO_GCC_GPLL3_CONFIG_CTL_DMET_WINDOW_ENABLE_BMSK                                         0x40000000
#define HWIO_GCC_GPLL3_CONFIG_CTL_DMET_WINDOW_ENABLE_SHFT                                               0x1e
#define HWIO_GCC_GPLL3_CONFIG_CTL_TOGGLE_DET_SAMPLE_INTER_BMSK                                    0x3c000000
#define HWIO_GCC_GPLL3_CONFIG_CTL_TOGGLE_DET_SAMPLE_INTER_SHFT                                          0x1a
#define HWIO_GCC_GPLL3_CONFIG_CTL_TOGGLE_DET_THRESHOLD_BMSK                                        0x3800000
#define HWIO_GCC_GPLL3_CONFIG_CTL_TOGGLE_DET_THRESHOLD_SHFT                                             0x17
#define HWIO_GCC_GPLL3_CONFIG_CTL_TOGGLE_DET_SAMPLE_BMSK                                            0x700000
#define HWIO_GCC_GPLL3_CONFIG_CTL_TOGGLE_DET_SAMPLE_SHFT                                                0x14
#define HWIO_GCC_GPLL3_CONFIG_CTL_LOCK_DET_THRESHOLD_BMSK                                            0xff000
#define HWIO_GCC_GPLL3_CONFIG_CTL_LOCK_DET_THRESHOLD_SHFT                                                0xc
#define HWIO_GCC_GPLL3_CONFIG_CTL_LOCK_DET_SAMPLE_SIZE_BMSK                                            0xf00
#define HWIO_GCC_GPLL3_CONFIG_CTL_LOCK_DET_SAMPLE_SIZE_SHFT                                              0x8
#define HWIO_GCC_GPLL3_CONFIG_CTL_MIN_GLITCH_THRESHOLD_BMSK                                             0xc0
#define HWIO_GCC_GPLL3_CONFIG_CTL_MIN_GLITCH_THRESHOLD_SHFT                                              0x6
#define HWIO_GCC_GPLL3_CONFIG_CTL_REF_CYCLE_BMSK                                                        0x30
#define HWIO_GCC_GPLL3_CONFIG_CTL_REF_CYCLE_SHFT                                                         0x4
#define HWIO_GCC_GPLL3_CONFIG_CTL_KFN_BMSK                                                               0xf
#define HWIO_GCC_GPLL3_CONFIG_CTL_KFN_SHFT                                                               0x0

#define HWIO_GCC_GPLL3_TEST_CTL_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x0002301c)
#define HWIO_GCC_GPLL3_TEST_CTL_RMSK                                                              0xffffffff
#define HWIO_GCC_GPLL3_TEST_CTL_IN          \
        in_dword_masked(HWIO_GCC_GPLL3_TEST_CTL_ADDR, HWIO_GCC_GPLL3_TEST_CTL_RMSK)
#define HWIO_GCC_GPLL3_TEST_CTL_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL3_TEST_CTL_ADDR, m)
#define HWIO_GCC_GPLL3_TEST_CTL_OUT(v)      \
        out_dword(HWIO_GCC_GPLL3_TEST_CTL_ADDR,v)
#define HWIO_GCC_GPLL3_TEST_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL3_TEST_CTL_ADDR,m,v,HWIO_GCC_GPLL3_TEST_CTL_IN)
#define HWIO_GCC_GPLL3_TEST_CTL_BIAS_GEN_TRIM_BMSK                                                0xe0000000
#define HWIO_GCC_GPLL3_TEST_CTL_BIAS_GEN_TRIM_SHFT                                                      0x1d
#define HWIO_GCC_GPLL3_TEST_CTL_DCO_BMSK                                                          0x10000000
#define HWIO_GCC_GPLL3_TEST_CTL_DCO_SHFT                                                                0x1c
#define HWIO_GCC_GPLL3_TEST_CTL_PROCESS_CALB_BMSK                                                  0xc000000
#define HWIO_GCC_GPLL3_TEST_CTL_PROCESS_CALB_SHFT                                                       0x1a
#define HWIO_GCC_GPLL3_TEST_CTL_OVERRIDE_PROCESS_CALB_BMSK                                         0x2000000
#define HWIO_GCC_GPLL3_TEST_CTL_OVERRIDE_PROCESS_CALB_SHFT                                              0x19
#define HWIO_GCC_GPLL3_TEST_CTL_FINE_FCW_BMSK                                                      0x1f00000
#define HWIO_GCC_GPLL3_TEST_CTL_FINE_FCW_SHFT                                                           0x14
#define HWIO_GCC_GPLL3_TEST_CTL_OVERRIDE_FINE_FCW_BMSK                                               0x80000
#define HWIO_GCC_GPLL3_TEST_CTL_OVERRIDE_FINE_FCW_SHFT                                                  0x13
#define HWIO_GCC_GPLL3_TEST_CTL_COARSE_FCW_BMSK                                                      0x7e000
#define HWIO_GCC_GPLL3_TEST_CTL_COARSE_FCW_SHFT                                                          0xd
#define HWIO_GCC_GPLL3_TEST_CTL_OVERRIDE_COARSE_BMSK                                                  0x1000
#define HWIO_GCC_GPLL3_TEST_CTL_OVERRIDE_COARSE_SHFT                                                     0xc
#define HWIO_GCC_GPLL3_TEST_CTL_DISABLE_LFSR_BMSK                                                      0x800
#define HWIO_GCC_GPLL3_TEST_CTL_DISABLE_LFSR_SHFT                                                        0xb
#define HWIO_GCC_GPLL3_TEST_CTL_DTEST_SEL_BMSK                                                         0x700
#define HWIO_GCC_GPLL3_TEST_CTL_DTEST_SEL_SHFT                                                           0x8
#define HWIO_GCC_GPLL3_TEST_CTL_DTEST_EN_BMSK                                                           0x80
#define HWIO_GCC_GPLL3_TEST_CTL_DTEST_EN_SHFT                                                            0x7
#define HWIO_GCC_GPLL3_TEST_CTL_BYP_TESTAMP_BMSK                                                        0x40
#define HWIO_GCC_GPLL3_TEST_CTL_BYP_TESTAMP_SHFT                                                         0x6
#define HWIO_GCC_GPLL3_TEST_CTL_ATEST1_SEL_BMSK                                                         0x30
#define HWIO_GCC_GPLL3_TEST_CTL_ATEST1_SEL_SHFT                                                          0x4
#define HWIO_GCC_GPLL3_TEST_CTL_ATEST0_SEL_BMSK                                                          0xc
#define HWIO_GCC_GPLL3_TEST_CTL_ATEST0_SEL_SHFT                                                          0x2
#define HWIO_GCC_GPLL3_TEST_CTL_ATEST1_EN_BMSK                                                           0x2
#define HWIO_GCC_GPLL3_TEST_CTL_ATEST1_EN_SHFT                                                           0x1
#define HWIO_GCC_GPLL3_TEST_CTL_ATEST0_EN_BMSK                                                           0x1
#define HWIO_GCC_GPLL3_TEST_CTL_ATEST0_EN_SHFT                                                           0x0

#define HWIO_GCC_GPLL3_TEST_CTL_U_ADDR                                                            (GCC_CLK_CTL_REG_REG_BASE      + 0x00023020)
#define HWIO_GCC_GPLL3_TEST_CTL_U_RMSK                                                            0xffffffff
#define HWIO_GCC_GPLL3_TEST_CTL_U_IN          \
        in_dword_masked(HWIO_GCC_GPLL3_TEST_CTL_U_ADDR, HWIO_GCC_GPLL3_TEST_CTL_U_RMSK)
#define HWIO_GCC_GPLL3_TEST_CTL_U_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL3_TEST_CTL_U_ADDR, m)
#define HWIO_GCC_GPLL3_TEST_CTL_U_OUT(v)      \
        out_dword(HWIO_GCC_GPLL3_TEST_CTL_U_ADDR,v)
#define HWIO_GCC_GPLL3_TEST_CTL_U_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL3_TEST_CTL_U_ADDR,m,v,HWIO_GCC_GPLL3_TEST_CTL_U_IN)
#define HWIO_GCC_GPLL3_TEST_CTL_U_RESERVE_BITS31_14_BMSK                                          0xffffc000
#define HWIO_GCC_GPLL3_TEST_CTL_U_RESERVE_BITS31_14_SHFT                                                 0xe
#define HWIO_GCC_GPLL3_TEST_CTL_U_OVERRIDE_FINE_FCW_MSB_BMSK                                          0x2000
#define HWIO_GCC_GPLL3_TEST_CTL_U_OVERRIDE_FINE_FCW_MSB_SHFT                                             0xd
#define HWIO_GCC_GPLL3_TEST_CTL_U_DTEST_MODE_SEL_BMSK                                                 0x1800
#define HWIO_GCC_GPLL3_TEST_CTL_U_DTEST_MODE_SEL_SHFT                                                    0xb
#define HWIO_GCC_GPLL3_TEST_CTL_U_NMO_OSC_SEL_BMSK                                                     0x600
#define HWIO_GCC_GPLL3_TEST_CTL_U_NMO_OSC_SEL_SHFT                                                       0x9
#define HWIO_GCC_GPLL3_TEST_CTL_U_NMO_EN_BMSK                                                          0x100
#define HWIO_GCC_GPLL3_TEST_CTL_U_NMO_EN_SHFT                                                            0x8
#define HWIO_GCC_GPLL3_TEST_CTL_U_NOISE_MAG_BMSK                                                        0xe0
#define HWIO_GCC_GPLL3_TEST_CTL_U_NOISE_MAG_SHFT                                                         0x5
#define HWIO_GCC_GPLL3_TEST_CTL_U_NOISE_GEN_BMSK                                                        0x10
#define HWIO_GCC_GPLL3_TEST_CTL_U_NOISE_GEN_SHFT                                                         0x4
#define HWIO_GCC_GPLL3_TEST_CTL_U_OSC_BIAS_GND_BMSK                                                      0x8
#define HWIO_GCC_GPLL3_TEST_CTL_U_OSC_BIAS_GND_SHFT                                                      0x3
#define HWIO_GCC_GPLL3_TEST_CTL_U_PLL_TEST_OUT_SEL_BMSK                                                  0x6
#define HWIO_GCC_GPLL3_TEST_CTL_U_PLL_TEST_OUT_SEL_SHFT                                                  0x1
#define HWIO_GCC_GPLL3_TEST_CTL_U_CAL_CODE_UPDATE_BMSK                                                   0x1
#define HWIO_GCC_GPLL3_TEST_CTL_U_CAL_CODE_UPDATE_SHFT                                                   0x0

#define HWIO_GCC_GPLL3_FREQ_CTL_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x00023028)
#define HWIO_GCC_GPLL3_FREQ_CTL_RMSK                                                              0xffffffff
#define HWIO_GCC_GPLL3_FREQ_CTL_IN          \
        in_dword_masked(HWIO_GCC_GPLL3_FREQ_CTL_ADDR, HWIO_GCC_GPLL3_FREQ_CTL_RMSK)
#define HWIO_GCC_GPLL3_FREQ_CTL_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL3_FREQ_CTL_ADDR, m)
#define HWIO_GCC_GPLL3_FREQ_CTL_OUT(v)      \
        out_dword(HWIO_GCC_GPLL3_FREQ_CTL_ADDR,v)
#define HWIO_GCC_GPLL3_FREQ_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL3_FREQ_CTL_ADDR,m,v,HWIO_GCC_GPLL3_FREQ_CTL_IN)
#define HWIO_GCC_GPLL3_FREQ_CTL_FREQUENCY_CTL_WORD_BMSK                                           0xffffffff
#define HWIO_GCC_GPLL3_FREQ_CTL_FREQUENCY_CTL_WORD_SHFT                                                  0x0

#define HWIO_GCC_GPLL3_STATUS_ADDR                                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x00023024)
#define HWIO_GCC_GPLL3_STATUS_RMSK                                                                 0x7ffffff
#define HWIO_GCC_GPLL3_STATUS_IN          \
        in_dword_masked(HWIO_GCC_GPLL3_STATUS_ADDR, HWIO_GCC_GPLL3_STATUS_RMSK)
#define HWIO_GCC_GPLL3_STATUS_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL3_STATUS_ADDR, m)
#define HWIO_GCC_GPLL3_STATUS_STATUS_26_24_BMSK                                                    0x7000000
#define HWIO_GCC_GPLL3_STATUS_STATUS_26_24_SHFT                                                         0x18
#define HWIO_GCC_GPLL3_STATUS_STATUS_23_BMSK                                                        0x800000
#define HWIO_GCC_GPLL3_STATUS_STATUS_23_SHFT                                                            0x17
#define HWIO_GCC_GPLL3_STATUS_STATUS_22_20_BMSK                                                     0x700000
#define HWIO_GCC_GPLL3_STATUS_STATUS_22_20_SHFT                                                         0x14
#define HWIO_GCC_GPLL3_STATUS_STATUS_19_17_BMSK                                                      0xe0000
#define HWIO_GCC_GPLL3_STATUS_STATUS_19_17_SHFT                                                         0x11
#define HWIO_GCC_GPLL3_STATUS_STATUS_16_12_BMSK                                                      0x1f000
#define HWIO_GCC_GPLL3_STATUS_STATUS_16_12_SHFT                                                          0xc
#define HWIO_GCC_GPLL3_STATUS_STATUS_11_6_BMSK                                                         0xfc0
#define HWIO_GCC_GPLL3_STATUS_STATUS_11_6_SHFT                                                           0x6
#define HWIO_GCC_GPLL3_STATUS_STATUS_5_BMSK                                                             0x20
#define HWIO_GCC_GPLL3_STATUS_STATUS_5_SHFT                                                              0x5
#define HWIO_GCC_GPLL3_STATUS_STATUS_4_2_BMSK                                                           0x1c
#define HWIO_GCC_GPLL3_STATUS_STATUS_4_2_SHFT                                                            0x2
#define HWIO_GCC_GPLL3_STATUS_STATUS_1_BMSK                                                              0x2
#define HWIO_GCC_GPLL3_STATUS_STATUS_1_SHFT                                                              0x1
#define HWIO_GCC_GPLL3_STATUS_STATUS_0_BMSK                                                              0x1
#define HWIO_GCC_GPLL3_STATUS_STATUS_0_SHFT                                                              0x0

#define HWIO_GCC_GPLL2_MODE_ADDR                                                                  (GCC_CLK_CTL_REG_REG_BASE      + 0x00025000)
#define HWIO_GCC_GPLL2_MODE_RMSK                                                                  0xe0ffff0f
#define HWIO_GCC_GPLL2_MODE_IN          \
        in_dword_masked(HWIO_GCC_GPLL2_MODE_ADDR, HWIO_GCC_GPLL2_MODE_RMSK)
#define HWIO_GCC_GPLL2_MODE_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL2_MODE_ADDR, m)
#define HWIO_GCC_GPLL2_MODE_OUT(v)      \
        out_dword(HWIO_GCC_GPLL2_MODE_ADDR,v)
#define HWIO_GCC_GPLL2_MODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL2_MODE_ADDR,m,v,HWIO_GCC_GPLL2_MODE_IN)
#define HWIO_GCC_GPLL2_MODE_PLL_LOCK_DET_BMSK                                                     0x80000000
#define HWIO_GCC_GPLL2_MODE_PLL_LOCK_DET_SHFT                                                           0x1f
#define HWIO_GCC_GPLL2_MODE_PLL_ACTIVE_FLAG_BMSK                                                  0x40000000
#define HWIO_GCC_GPLL2_MODE_PLL_ACTIVE_FLAG_SHFT                                                        0x1e
#define HWIO_GCC_GPLL2_MODE_PLL_ACK_LATCH_BMSK                                                    0x20000000
#define HWIO_GCC_GPLL2_MODE_PLL_ACK_LATCH_SHFT                                                          0x1d
#define HWIO_GCC_GPLL2_MODE_PLL_HW_UPADATE_LOGIC_BYPASS_BMSK                                        0x800000
#define HWIO_GCC_GPLL2_MODE_PLL_HW_UPADATE_LOGIC_BYPASS_SHFT                                            0x17
#define HWIO_GCC_GPLL2_MODE_PLL_UPDATE_BMSK                                                         0x400000
#define HWIO_GCC_GPLL2_MODE_PLL_UPDATE_SHFT                                                             0x16
#define HWIO_GCC_GPLL2_MODE_PLL_VOTE_FSM_RESET_BMSK                                                 0x200000
#define HWIO_GCC_GPLL2_MODE_PLL_VOTE_FSM_RESET_SHFT                                                     0x15
#define HWIO_GCC_GPLL2_MODE_PLL_VOTE_FSM_ENA_BMSK                                                   0x100000
#define HWIO_GCC_GPLL2_MODE_PLL_VOTE_FSM_ENA_SHFT                                                       0x14
#define HWIO_GCC_GPLL2_MODE_PLL_BIAS_COUNT_BMSK                                                      0xfc000
#define HWIO_GCC_GPLL2_MODE_PLL_BIAS_COUNT_SHFT                                                          0xe
#define HWIO_GCC_GPLL2_MODE_PLL_LOCK_COUNT_BMSK                                                       0x3f00
#define HWIO_GCC_GPLL2_MODE_PLL_LOCK_COUNT_SHFT                                                          0x8
#define HWIO_GCC_GPLL2_MODE_PLL_PLLTEST_BMSK                                                             0x8
#define HWIO_GCC_GPLL2_MODE_PLL_PLLTEST_SHFT                                                             0x3
#define HWIO_GCC_GPLL2_MODE_PLL_RESET_N_BMSK                                                             0x4
#define HWIO_GCC_GPLL2_MODE_PLL_RESET_N_SHFT                                                             0x2
#define HWIO_GCC_GPLL2_MODE_PLL_BYPASSNL_BMSK                                                            0x2
#define HWIO_GCC_GPLL2_MODE_PLL_BYPASSNL_SHFT                                                            0x1
#define HWIO_GCC_GPLL2_MODE_PLL_OUTCTRL_BMSK                                                             0x1
#define HWIO_GCC_GPLL2_MODE_PLL_OUTCTRL_SHFT                                                             0x0

#define HWIO_GCC_GPLL2_L_VAL_ADDR                                                                 (GCC_CLK_CTL_REG_REG_BASE      + 0x00025004)
#define HWIO_GCC_GPLL2_L_VAL_RMSK                                                                     0xffff
#define HWIO_GCC_GPLL2_L_VAL_IN          \
        in_dword_masked(HWIO_GCC_GPLL2_L_VAL_ADDR, HWIO_GCC_GPLL2_L_VAL_RMSK)
#define HWIO_GCC_GPLL2_L_VAL_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL2_L_VAL_ADDR, m)
#define HWIO_GCC_GPLL2_L_VAL_OUT(v)      \
        out_dword(HWIO_GCC_GPLL2_L_VAL_ADDR,v)
#define HWIO_GCC_GPLL2_L_VAL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL2_L_VAL_ADDR,m,v,HWIO_GCC_GPLL2_L_VAL_IN)
#define HWIO_GCC_GPLL2_L_VAL_PLL_L_BMSK                                                               0xffff
#define HWIO_GCC_GPLL2_L_VAL_PLL_L_SHFT                                                                  0x0

#define HWIO_GCC_GPLL2_ALPHA_VAL_ADDR                                                             (GCC_CLK_CTL_REG_REG_BASE      + 0x00025008)
#define HWIO_GCC_GPLL2_ALPHA_VAL_RMSK                                                             0xffffffff
#define HWIO_GCC_GPLL2_ALPHA_VAL_IN          \
        in_dword_masked(HWIO_GCC_GPLL2_ALPHA_VAL_ADDR, HWIO_GCC_GPLL2_ALPHA_VAL_RMSK)
#define HWIO_GCC_GPLL2_ALPHA_VAL_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL2_ALPHA_VAL_ADDR, m)
#define HWIO_GCC_GPLL2_ALPHA_VAL_OUT(v)      \
        out_dword(HWIO_GCC_GPLL2_ALPHA_VAL_ADDR,v)
#define HWIO_GCC_GPLL2_ALPHA_VAL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL2_ALPHA_VAL_ADDR,m,v,HWIO_GCC_GPLL2_ALPHA_VAL_IN)
#define HWIO_GCC_GPLL2_ALPHA_VAL_PLL_ALPHA_BMSK                                                   0xffffffff
#define HWIO_GCC_GPLL2_ALPHA_VAL_PLL_ALPHA_SHFT                                                          0x0

#define HWIO_GCC_GPLL2_ALPHA_VAL_U_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x0002500c)
#define HWIO_GCC_GPLL2_ALPHA_VAL_U_RMSK                                                                 0xff
#define HWIO_GCC_GPLL2_ALPHA_VAL_U_IN          \
        in_dword_masked(HWIO_GCC_GPLL2_ALPHA_VAL_U_ADDR, HWIO_GCC_GPLL2_ALPHA_VAL_U_RMSK)
#define HWIO_GCC_GPLL2_ALPHA_VAL_U_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL2_ALPHA_VAL_U_ADDR, m)
#define HWIO_GCC_GPLL2_ALPHA_VAL_U_OUT(v)      \
        out_dword(HWIO_GCC_GPLL2_ALPHA_VAL_U_ADDR,v)
#define HWIO_GCC_GPLL2_ALPHA_VAL_U_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL2_ALPHA_VAL_U_ADDR,m,v,HWIO_GCC_GPLL2_ALPHA_VAL_U_IN)
#define HWIO_GCC_GPLL2_ALPHA_VAL_U_PLL_ALPHA_BMSK                                                       0xff
#define HWIO_GCC_GPLL2_ALPHA_VAL_U_PLL_ALPHA_SHFT                                                        0x0

#define HWIO_GCC_GPLL2_USER_CTL_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x00025010)
#define HWIO_GCC_GPLL2_USER_CTL_RMSK                                                              0xffffffff
#define HWIO_GCC_GPLL2_USER_CTL_IN          \
        in_dword_masked(HWIO_GCC_GPLL2_USER_CTL_ADDR, HWIO_GCC_GPLL2_USER_CTL_RMSK)
#define HWIO_GCC_GPLL2_USER_CTL_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL2_USER_CTL_ADDR, m)
#define HWIO_GCC_GPLL2_USER_CTL_OUT(v)      \
        out_dword(HWIO_GCC_GPLL2_USER_CTL_ADDR,v)
#define HWIO_GCC_GPLL2_USER_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL2_USER_CTL_ADDR,m,v,HWIO_GCC_GPLL2_USER_CTL_IN)
#define HWIO_GCC_GPLL2_USER_CTL_RESERVE_BITS31_28_BMSK                                            0xf0000000
#define HWIO_GCC_GPLL2_USER_CTL_RESERVE_BITS31_28_SHFT                                                  0x1c
#define HWIO_GCC_GPLL2_USER_CTL_SSC_MODE_CONTROL_BMSK                                              0x8000000
#define HWIO_GCC_GPLL2_USER_CTL_SSC_MODE_CONTROL_SHFT                                                   0x1b
#define HWIO_GCC_GPLL2_USER_CTL_RESERVE_BITS26_25_BMSK                                             0x6000000
#define HWIO_GCC_GPLL2_USER_CTL_RESERVE_BITS26_25_SHFT                                                  0x19
#define HWIO_GCC_GPLL2_USER_CTL_ALPHA_EN_BMSK                                                      0x1000000
#define HWIO_GCC_GPLL2_USER_CTL_ALPHA_EN_SHFT                                                           0x18
#define HWIO_GCC_GPLL2_USER_CTL_RESERVE_BITS23_22_BMSK                                              0xc00000
#define HWIO_GCC_GPLL2_USER_CTL_RESERVE_BITS23_22_SHFT                                                  0x16
#define HWIO_GCC_GPLL2_USER_CTL_VCO_SEL_BMSK                                                        0x300000
#define HWIO_GCC_GPLL2_USER_CTL_VCO_SEL_SHFT                                                            0x14
#define HWIO_GCC_GPLL2_USER_CTL_RESERVE_BITS19_15_BMSK                                               0xf8000
#define HWIO_GCC_GPLL2_USER_CTL_RESERVE_BITS19_15_SHFT                                                   0xf
#define HWIO_GCC_GPLL2_USER_CTL_PRE_DIV_RATIO_BMSK                                                    0x7000
#define HWIO_GCC_GPLL2_USER_CTL_PRE_DIV_RATIO_SHFT                                                       0xc
#define HWIO_GCC_GPLL2_USER_CTL_POST_DIV_RATIO_BMSK                                                    0xf00
#define HWIO_GCC_GPLL2_USER_CTL_POST_DIV_RATIO_SHFT                                                      0x8
#define HWIO_GCC_GPLL2_USER_CTL_OUTPUT_INV_BMSK                                                         0x80
#define HWIO_GCC_GPLL2_USER_CTL_OUTPUT_INV_SHFT                                                          0x7
#define HWIO_GCC_GPLL2_USER_CTL_RESERVE_BITS6_5_BMSK                                                    0x60
#define HWIO_GCC_GPLL2_USER_CTL_RESERVE_BITS6_5_SHFT                                                     0x5
#define HWIO_GCC_GPLL2_USER_CTL_PLLOUT_LV_TEST_BMSK                                                     0x10
#define HWIO_GCC_GPLL2_USER_CTL_PLLOUT_LV_TEST_SHFT                                                      0x4
#define HWIO_GCC_GPLL2_USER_CTL_PLLOUT_LV_EARLY_BMSK                                                     0x8
#define HWIO_GCC_GPLL2_USER_CTL_PLLOUT_LV_EARLY_SHFT                                                     0x3
#define HWIO_GCC_GPLL2_USER_CTL_PLLOUT_LV_AUX2_BMSK                                                      0x4
#define HWIO_GCC_GPLL2_USER_CTL_PLLOUT_LV_AUX2_SHFT                                                      0x2
#define HWIO_GCC_GPLL2_USER_CTL_PLLOUT_LV_AUX_BMSK                                                       0x2
#define HWIO_GCC_GPLL2_USER_CTL_PLLOUT_LV_AUX_SHFT                                                       0x1
#define HWIO_GCC_GPLL2_USER_CTL_PLLOUT_LV_MAIN_BMSK                                                      0x1
#define HWIO_GCC_GPLL2_USER_CTL_PLLOUT_LV_MAIN_SHFT                                                      0x0

#define HWIO_GCC_GPLL2_USER_CTL_U_ADDR                                                            (GCC_CLK_CTL_REG_REG_BASE      + 0x00025014)
#define HWIO_GCC_GPLL2_USER_CTL_U_RMSK                                                            0xffffffff
#define HWIO_GCC_GPLL2_USER_CTL_U_IN          \
        in_dword_masked(HWIO_GCC_GPLL2_USER_CTL_U_ADDR, HWIO_GCC_GPLL2_USER_CTL_U_RMSK)
#define HWIO_GCC_GPLL2_USER_CTL_U_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL2_USER_CTL_U_ADDR, m)
#define HWIO_GCC_GPLL2_USER_CTL_U_OUT(v)      \
        out_dword(HWIO_GCC_GPLL2_USER_CTL_U_ADDR,v)
#define HWIO_GCC_GPLL2_USER_CTL_U_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL2_USER_CTL_U_ADDR,m,v,HWIO_GCC_GPLL2_USER_CTL_U_IN)
#define HWIO_GCC_GPLL2_USER_CTL_U_RESERVE_BITS31_12_BMSK                                          0xfffff000
#define HWIO_GCC_GPLL2_USER_CTL_U_RESERVE_BITS31_12_SHFT                                                 0xc
#define HWIO_GCC_GPLL2_USER_CTL_U_LATCH_INTERFACE_BYPASS_BMSK                                          0x800
#define HWIO_GCC_GPLL2_USER_CTL_U_LATCH_INTERFACE_BYPASS_SHFT                                            0xb
#define HWIO_GCC_GPLL2_USER_CTL_U_STATUS_REGISTER_BMSK                                                 0x700
#define HWIO_GCC_GPLL2_USER_CTL_U_STATUS_REGISTER_SHFT                                                   0x8
#define HWIO_GCC_GPLL2_USER_CTL_U_DSM_BMSK                                                              0x80
#define HWIO_GCC_GPLL2_USER_CTL_U_DSM_SHFT                                                               0x7
#define HWIO_GCC_GPLL2_USER_CTL_U_WRITE_STATE_BMSK                                                      0x40
#define HWIO_GCC_GPLL2_USER_CTL_U_WRITE_STATE_SHFT                                                       0x6
#define HWIO_GCC_GPLL2_USER_CTL_U_TARGET_CTL_BMSK                                                       0x38
#define HWIO_GCC_GPLL2_USER_CTL_U_TARGET_CTL_SHFT                                                        0x3
#define HWIO_GCC_GPLL2_USER_CTL_U_LOCK_DET_BMSK                                                          0x4
#define HWIO_GCC_GPLL2_USER_CTL_U_LOCK_DET_SHFT                                                          0x2
#define HWIO_GCC_GPLL2_USER_CTL_U_FREEZE_PLL_BMSK                                                        0x2
#define HWIO_GCC_GPLL2_USER_CTL_U_FREEZE_PLL_SHFT                                                        0x1
#define HWIO_GCC_GPLL2_USER_CTL_U_TOGGLE_DET_BMSK                                                        0x1
#define HWIO_GCC_GPLL2_USER_CTL_U_TOGGLE_DET_SHFT                                                        0x0

#define HWIO_GCC_GPLL2_CONFIG_CTL_ADDR                                                            (GCC_CLK_CTL_REG_REG_BASE      + 0x00025018)
#define HWIO_GCC_GPLL2_CONFIG_CTL_RMSK                                                            0xffffffff
#define HWIO_GCC_GPLL2_CONFIG_CTL_IN          \
        in_dword_masked(HWIO_GCC_GPLL2_CONFIG_CTL_ADDR, HWIO_GCC_GPLL2_CONFIG_CTL_RMSK)
#define HWIO_GCC_GPLL2_CONFIG_CTL_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL2_CONFIG_CTL_ADDR, m)
#define HWIO_GCC_GPLL2_CONFIG_CTL_OUT(v)      \
        out_dword(HWIO_GCC_GPLL2_CONFIG_CTL_ADDR,v)
#define HWIO_GCC_GPLL2_CONFIG_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL2_CONFIG_CTL_ADDR,m,v,HWIO_GCC_GPLL2_CONFIG_CTL_IN)
#define HWIO_GCC_GPLL2_CONFIG_CTL_SINGLE_DMET_MODE_ENABLE_BMSK                                    0x80000000
#define HWIO_GCC_GPLL2_CONFIG_CTL_SINGLE_DMET_MODE_ENABLE_SHFT                                          0x1f
#define HWIO_GCC_GPLL2_CONFIG_CTL_DMET_WINDOW_ENABLE_BMSK                                         0x40000000
#define HWIO_GCC_GPLL2_CONFIG_CTL_DMET_WINDOW_ENABLE_SHFT                                               0x1e
#define HWIO_GCC_GPLL2_CONFIG_CTL_TOGGLE_DET_SAMPLE_INTER_BMSK                                    0x3c000000
#define HWIO_GCC_GPLL2_CONFIG_CTL_TOGGLE_DET_SAMPLE_INTER_SHFT                                          0x1a
#define HWIO_GCC_GPLL2_CONFIG_CTL_TOGGLE_DET_THRESHOLD_BMSK                                        0x3800000
#define HWIO_GCC_GPLL2_CONFIG_CTL_TOGGLE_DET_THRESHOLD_SHFT                                             0x17
#define HWIO_GCC_GPLL2_CONFIG_CTL_TOGGLE_DET_SAMPLE_BMSK                                            0x700000
#define HWIO_GCC_GPLL2_CONFIG_CTL_TOGGLE_DET_SAMPLE_SHFT                                                0x14
#define HWIO_GCC_GPLL2_CONFIG_CTL_LOCK_DET_THRESHOLD_BMSK                                            0xff000
#define HWIO_GCC_GPLL2_CONFIG_CTL_LOCK_DET_THRESHOLD_SHFT                                                0xc
#define HWIO_GCC_GPLL2_CONFIG_CTL_LOCK_DET_SAMPLE_SIZE_BMSK                                            0xf00
#define HWIO_GCC_GPLL2_CONFIG_CTL_LOCK_DET_SAMPLE_SIZE_SHFT                                              0x8
#define HWIO_GCC_GPLL2_CONFIG_CTL_MIN_GLITCH_THRESHOLD_BMSK                                             0xc0
#define HWIO_GCC_GPLL2_CONFIG_CTL_MIN_GLITCH_THRESHOLD_SHFT                                              0x6
#define HWIO_GCC_GPLL2_CONFIG_CTL_REF_CYCLE_BMSK                                                        0x30
#define HWIO_GCC_GPLL2_CONFIG_CTL_REF_CYCLE_SHFT                                                         0x4
#define HWIO_GCC_GPLL2_CONFIG_CTL_KFN_BMSK                                                               0xf
#define HWIO_GCC_GPLL2_CONFIG_CTL_KFN_SHFT                                                               0x0

#define HWIO_GCC_GPLL2_TEST_CTL_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x0002501c)
#define HWIO_GCC_GPLL2_TEST_CTL_RMSK                                                              0xffffffff
#define HWIO_GCC_GPLL2_TEST_CTL_IN          \
        in_dword_masked(HWIO_GCC_GPLL2_TEST_CTL_ADDR, HWIO_GCC_GPLL2_TEST_CTL_RMSK)
#define HWIO_GCC_GPLL2_TEST_CTL_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL2_TEST_CTL_ADDR, m)
#define HWIO_GCC_GPLL2_TEST_CTL_OUT(v)      \
        out_dword(HWIO_GCC_GPLL2_TEST_CTL_ADDR,v)
#define HWIO_GCC_GPLL2_TEST_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL2_TEST_CTL_ADDR,m,v,HWIO_GCC_GPLL2_TEST_CTL_IN)
#define HWIO_GCC_GPLL2_TEST_CTL_BIAS_GEN_TRIM_BMSK                                                0xe0000000
#define HWIO_GCC_GPLL2_TEST_CTL_BIAS_GEN_TRIM_SHFT                                                      0x1d
#define HWIO_GCC_GPLL2_TEST_CTL_DCO_BMSK                                                          0x10000000
#define HWIO_GCC_GPLL2_TEST_CTL_DCO_SHFT                                                                0x1c
#define HWIO_GCC_GPLL2_TEST_CTL_PROCESS_CALB_BMSK                                                  0xc000000
#define HWIO_GCC_GPLL2_TEST_CTL_PROCESS_CALB_SHFT                                                       0x1a
#define HWIO_GCC_GPLL2_TEST_CTL_OVERRIDE_PROCESS_CALB_BMSK                                         0x2000000
#define HWIO_GCC_GPLL2_TEST_CTL_OVERRIDE_PROCESS_CALB_SHFT                                              0x19
#define HWIO_GCC_GPLL2_TEST_CTL_FINE_FCW_BMSK                                                      0x1f00000
#define HWIO_GCC_GPLL2_TEST_CTL_FINE_FCW_SHFT                                                           0x14
#define HWIO_GCC_GPLL2_TEST_CTL_OVERRIDE_FINE_FCW_BMSK                                               0x80000
#define HWIO_GCC_GPLL2_TEST_CTL_OVERRIDE_FINE_FCW_SHFT                                                  0x13
#define HWIO_GCC_GPLL2_TEST_CTL_COARSE_FCW_BMSK                                                      0x7e000
#define HWIO_GCC_GPLL2_TEST_CTL_COARSE_FCW_SHFT                                                          0xd
#define HWIO_GCC_GPLL2_TEST_CTL_OVERRIDE_COARSE_BMSK                                                  0x1000
#define HWIO_GCC_GPLL2_TEST_CTL_OVERRIDE_COARSE_SHFT                                                     0xc
#define HWIO_GCC_GPLL2_TEST_CTL_DISABLE_LFSR_BMSK                                                      0x800
#define HWIO_GCC_GPLL2_TEST_CTL_DISABLE_LFSR_SHFT                                                        0xb
#define HWIO_GCC_GPLL2_TEST_CTL_DTEST_SEL_BMSK                                                         0x700
#define HWIO_GCC_GPLL2_TEST_CTL_DTEST_SEL_SHFT                                                           0x8
#define HWIO_GCC_GPLL2_TEST_CTL_DTEST_EN_BMSK                                                           0x80
#define HWIO_GCC_GPLL2_TEST_CTL_DTEST_EN_SHFT                                                            0x7
#define HWIO_GCC_GPLL2_TEST_CTL_BYP_TESTAMP_BMSK                                                        0x40
#define HWIO_GCC_GPLL2_TEST_CTL_BYP_TESTAMP_SHFT                                                         0x6
#define HWIO_GCC_GPLL2_TEST_CTL_ATEST1_SEL_BMSK                                                         0x30
#define HWIO_GCC_GPLL2_TEST_CTL_ATEST1_SEL_SHFT                                                          0x4
#define HWIO_GCC_GPLL2_TEST_CTL_ATEST0_SEL_BMSK                                                          0xc
#define HWIO_GCC_GPLL2_TEST_CTL_ATEST0_SEL_SHFT                                                          0x2
#define HWIO_GCC_GPLL2_TEST_CTL_ATEST1_EN_BMSK                                                           0x2
#define HWIO_GCC_GPLL2_TEST_CTL_ATEST1_EN_SHFT                                                           0x1
#define HWIO_GCC_GPLL2_TEST_CTL_ATEST0_EN_BMSK                                                           0x1
#define HWIO_GCC_GPLL2_TEST_CTL_ATEST0_EN_SHFT                                                           0x0

#define HWIO_GCC_GPLL2_TEST_CTL_U_ADDR                                                            (GCC_CLK_CTL_REG_REG_BASE      + 0x00025020)
#define HWIO_GCC_GPLL2_TEST_CTL_U_RMSK                                                            0xffffffff
#define HWIO_GCC_GPLL2_TEST_CTL_U_IN          \
        in_dword_masked(HWIO_GCC_GPLL2_TEST_CTL_U_ADDR, HWIO_GCC_GPLL2_TEST_CTL_U_RMSK)
#define HWIO_GCC_GPLL2_TEST_CTL_U_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL2_TEST_CTL_U_ADDR, m)
#define HWIO_GCC_GPLL2_TEST_CTL_U_OUT(v)      \
        out_dword(HWIO_GCC_GPLL2_TEST_CTL_U_ADDR,v)
#define HWIO_GCC_GPLL2_TEST_CTL_U_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL2_TEST_CTL_U_ADDR,m,v,HWIO_GCC_GPLL2_TEST_CTL_U_IN)
#define HWIO_GCC_GPLL2_TEST_CTL_U_RESERVE_BITS31_14_BMSK                                          0xffffc000
#define HWIO_GCC_GPLL2_TEST_CTL_U_RESERVE_BITS31_14_SHFT                                                 0xe
#define HWIO_GCC_GPLL2_TEST_CTL_U_OVERRIDE_FINE_FCW_MSB_BMSK                                          0x2000
#define HWIO_GCC_GPLL2_TEST_CTL_U_OVERRIDE_FINE_FCW_MSB_SHFT                                             0xd
#define HWIO_GCC_GPLL2_TEST_CTL_U_DTEST_MODE_SEL_BMSK                                                 0x1800
#define HWIO_GCC_GPLL2_TEST_CTL_U_DTEST_MODE_SEL_SHFT                                                    0xb
#define HWIO_GCC_GPLL2_TEST_CTL_U_NMO_OSC_SEL_BMSK                                                     0x600
#define HWIO_GCC_GPLL2_TEST_CTL_U_NMO_OSC_SEL_SHFT                                                       0x9
#define HWIO_GCC_GPLL2_TEST_CTL_U_NMO_EN_BMSK                                                          0x100
#define HWIO_GCC_GPLL2_TEST_CTL_U_NMO_EN_SHFT                                                            0x8
#define HWIO_GCC_GPLL2_TEST_CTL_U_NOISE_MAG_BMSK                                                        0xe0
#define HWIO_GCC_GPLL2_TEST_CTL_U_NOISE_MAG_SHFT                                                         0x5
#define HWIO_GCC_GPLL2_TEST_CTL_U_NOISE_GEN_BMSK                                                        0x10
#define HWIO_GCC_GPLL2_TEST_CTL_U_NOISE_GEN_SHFT                                                         0x4
#define HWIO_GCC_GPLL2_TEST_CTL_U_OSC_BIAS_GND_BMSK                                                      0x8
#define HWIO_GCC_GPLL2_TEST_CTL_U_OSC_BIAS_GND_SHFT                                                      0x3
#define HWIO_GCC_GPLL2_TEST_CTL_U_PLL_TEST_OUT_SEL_BMSK                                                  0x6
#define HWIO_GCC_GPLL2_TEST_CTL_U_PLL_TEST_OUT_SEL_SHFT                                                  0x1
#define HWIO_GCC_GPLL2_TEST_CTL_U_CAL_CODE_UPDATE_BMSK                                                   0x1
#define HWIO_GCC_GPLL2_TEST_CTL_U_CAL_CODE_UPDATE_SHFT                                                   0x0

#define HWIO_GCC_GPLL2_FREQ_CTL_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x00025028)
#define HWIO_GCC_GPLL2_FREQ_CTL_RMSK                                                              0xffffffff
#define HWIO_GCC_GPLL2_FREQ_CTL_IN          \
        in_dword_masked(HWIO_GCC_GPLL2_FREQ_CTL_ADDR, HWIO_GCC_GPLL2_FREQ_CTL_RMSK)
#define HWIO_GCC_GPLL2_FREQ_CTL_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL2_FREQ_CTL_ADDR, m)
#define HWIO_GCC_GPLL2_FREQ_CTL_OUT(v)      \
        out_dword(HWIO_GCC_GPLL2_FREQ_CTL_ADDR,v)
#define HWIO_GCC_GPLL2_FREQ_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GPLL2_FREQ_CTL_ADDR,m,v,HWIO_GCC_GPLL2_FREQ_CTL_IN)
#define HWIO_GCC_GPLL2_FREQ_CTL_FREQUENCY_CTL_WORD_BMSK                                           0xffffffff
#define HWIO_GCC_GPLL2_FREQ_CTL_FREQUENCY_CTL_WORD_SHFT                                                  0x0

#define HWIO_GCC_GPLL2_STATUS_ADDR                                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x00025024)
#define HWIO_GCC_GPLL2_STATUS_RMSK                                                                 0x7ffffff
#define HWIO_GCC_GPLL2_STATUS_IN          \
        in_dword_masked(HWIO_GCC_GPLL2_STATUS_ADDR, HWIO_GCC_GPLL2_STATUS_RMSK)
#define HWIO_GCC_GPLL2_STATUS_INM(m)      \
        in_dword_masked(HWIO_GCC_GPLL2_STATUS_ADDR, m)
#define HWIO_GCC_GPLL2_STATUS_STATUS_26_24_BMSK                                                    0x7000000
#define HWIO_GCC_GPLL2_STATUS_STATUS_26_24_SHFT                                                         0x18
#define HWIO_GCC_GPLL2_STATUS_STATUS_23_BMSK                                                        0x800000
#define HWIO_GCC_GPLL2_STATUS_STATUS_23_SHFT                                                            0x17
#define HWIO_GCC_GPLL2_STATUS_STATUS_22_20_BMSK                                                     0x700000
#define HWIO_GCC_GPLL2_STATUS_STATUS_22_20_SHFT                                                         0x14
#define HWIO_GCC_GPLL2_STATUS_STATUS_19_17_BMSK                                                      0xe0000
#define HWIO_GCC_GPLL2_STATUS_STATUS_19_17_SHFT                                                         0x11
#define HWIO_GCC_GPLL2_STATUS_STATUS_16_12_BMSK                                                      0x1f000
#define HWIO_GCC_GPLL2_STATUS_STATUS_16_12_SHFT                                                          0xc
#define HWIO_GCC_GPLL2_STATUS_STATUS_11_6_BMSK                                                         0xfc0
#define HWIO_GCC_GPLL2_STATUS_STATUS_11_6_SHFT                                                           0x6
#define HWIO_GCC_GPLL2_STATUS_STATUS_5_BMSK                                                             0x20
#define HWIO_GCC_GPLL2_STATUS_STATUS_5_SHFT                                                              0x5
#define HWIO_GCC_GPLL2_STATUS_STATUS_4_2_BMSK                                                           0x1c
#define HWIO_GCC_GPLL2_STATUS_STATUS_4_2_SHFT                                                            0x2
#define HWIO_GCC_GPLL2_STATUS_STATUS_1_BMSK                                                              0x2
#define HWIO_GCC_GPLL2_STATUS_STATUS_1_SHFT                                                              0x1
#define HWIO_GCC_GPLL2_STATUS_STATUS_0_BMSK                                                              0x1
#define HWIO_GCC_GPLL2_STATUS_STATUS_0_SHFT                                                              0x0

#define HWIO_GCC_SYSTEM_NOC_BCR_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x00026000)
#define HWIO_GCC_SYSTEM_NOC_BCR_RMSK                                                                     0x1
#define HWIO_GCC_SYSTEM_NOC_BCR_IN          \
        in_dword_masked(HWIO_GCC_SYSTEM_NOC_BCR_ADDR, HWIO_GCC_SYSTEM_NOC_BCR_RMSK)
#define HWIO_GCC_SYSTEM_NOC_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SYSTEM_NOC_BCR_ADDR, m)
#define HWIO_GCC_SYSTEM_NOC_BCR_OUT(v)      \
        out_dword(HWIO_GCC_SYSTEM_NOC_BCR_ADDR,v)
#define HWIO_GCC_SYSTEM_NOC_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SYSTEM_NOC_BCR_ADDR,m,v,HWIO_GCC_SYSTEM_NOC_BCR_IN)
#define HWIO_GCC_SYSTEM_NOC_BCR_BLK_ARES_BMSK                                                            0x1
#define HWIO_GCC_SYSTEM_NOC_BCR_BLK_ARES_SHFT                                                            0x0

#define HWIO_GCC_SYSTEM_NOC_BFDCD_CMD_RCGR_ADDR                                                   (GCC_CLK_CTL_REG_REG_BASE      + 0x00026004)
#define HWIO_GCC_SYSTEM_NOC_BFDCD_CMD_RCGR_RMSK                                                   0x80000013
#define HWIO_GCC_SYSTEM_NOC_BFDCD_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_SYSTEM_NOC_BFDCD_CMD_RCGR_ADDR, HWIO_GCC_SYSTEM_NOC_BFDCD_CMD_RCGR_RMSK)
#define HWIO_GCC_SYSTEM_NOC_BFDCD_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_SYSTEM_NOC_BFDCD_CMD_RCGR_ADDR, m)
#define HWIO_GCC_SYSTEM_NOC_BFDCD_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_SYSTEM_NOC_BFDCD_CMD_RCGR_ADDR,v)
#define HWIO_GCC_SYSTEM_NOC_BFDCD_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SYSTEM_NOC_BFDCD_CMD_RCGR_ADDR,m,v,HWIO_GCC_SYSTEM_NOC_BFDCD_CMD_RCGR_IN)
#define HWIO_GCC_SYSTEM_NOC_BFDCD_CMD_RCGR_ROOT_OFF_BMSK                                          0x80000000
#define HWIO_GCC_SYSTEM_NOC_BFDCD_CMD_RCGR_ROOT_OFF_SHFT                                                0x1f
#define HWIO_GCC_SYSTEM_NOC_BFDCD_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                          0x10
#define HWIO_GCC_SYSTEM_NOC_BFDCD_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                           0x4
#define HWIO_GCC_SYSTEM_NOC_BFDCD_CMD_RCGR_ROOT_EN_BMSK                                                  0x2
#define HWIO_GCC_SYSTEM_NOC_BFDCD_CMD_RCGR_ROOT_EN_SHFT                                                  0x1
#define HWIO_GCC_SYSTEM_NOC_BFDCD_CMD_RCGR_UPDATE_BMSK                                                   0x1
#define HWIO_GCC_SYSTEM_NOC_BFDCD_CMD_RCGR_UPDATE_SHFT                                                   0x0

#define HWIO_GCC_SYSTEM_NOC_BFDCD_CFG_RCGR_ADDR                                                   (GCC_CLK_CTL_REG_REG_BASE      + 0x00026008)
#define HWIO_GCC_SYSTEM_NOC_BFDCD_CFG_RCGR_RMSK                                                        0x71f
#define HWIO_GCC_SYSTEM_NOC_BFDCD_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_SYSTEM_NOC_BFDCD_CFG_RCGR_ADDR, HWIO_GCC_SYSTEM_NOC_BFDCD_CFG_RCGR_RMSK)
#define HWIO_GCC_SYSTEM_NOC_BFDCD_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_SYSTEM_NOC_BFDCD_CFG_RCGR_ADDR, m)
#define HWIO_GCC_SYSTEM_NOC_BFDCD_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_SYSTEM_NOC_BFDCD_CFG_RCGR_ADDR,v)
#define HWIO_GCC_SYSTEM_NOC_BFDCD_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SYSTEM_NOC_BFDCD_CFG_RCGR_ADDR,m,v,HWIO_GCC_SYSTEM_NOC_BFDCD_CFG_RCGR_IN)
#define HWIO_GCC_SYSTEM_NOC_BFDCD_CFG_RCGR_SRC_SEL_BMSK                                                0x700
#define HWIO_GCC_SYSTEM_NOC_BFDCD_CFG_RCGR_SRC_SEL_SHFT                                                  0x8
#define HWIO_GCC_SYSTEM_NOC_BFDCD_CFG_RCGR_SRC_DIV_BMSK                                                 0x1f
#define HWIO_GCC_SYSTEM_NOC_BFDCD_CFG_RCGR_SRC_DIV_SHFT                                                  0x0

#define HWIO_GCC_SNOC_QOSGEN_EXTREF_CTL_ADDR                                                      (GCC_CLK_CTL_REG_REG_BASE      + 0x0002601c)
#define HWIO_GCC_SNOC_QOSGEN_EXTREF_CTL_RMSK                                                         0x30001
#define HWIO_GCC_SNOC_QOSGEN_EXTREF_CTL_IN          \
        in_dword_masked(HWIO_GCC_SNOC_QOSGEN_EXTREF_CTL_ADDR, HWIO_GCC_SNOC_QOSGEN_EXTREF_CTL_RMSK)
#define HWIO_GCC_SNOC_QOSGEN_EXTREF_CTL_INM(m)      \
        in_dword_masked(HWIO_GCC_SNOC_QOSGEN_EXTREF_CTL_ADDR, m)
#define HWIO_GCC_SNOC_QOSGEN_EXTREF_CTL_OUT(v)      \
        out_dword(HWIO_GCC_SNOC_QOSGEN_EXTREF_CTL_ADDR,v)
#define HWIO_GCC_SNOC_QOSGEN_EXTREF_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SNOC_QOSGEN_EXTREF_CTL_ADDR,m,v,HWIO_GCC_SNOC_QOSGEN_EXTREF_CTL_IN)
#define HWIO_GCC_SNOC_QOSGEN_EXTREF_CTL_DIVIDE_BMSK                                                  0x30000
#define HWIO_GCC_SNOC_QOSGEN_EXTREF_CTL_DIVIDE_SHFT                                                     0x10
#define HWIO_GCC_SNOC_QOSGEN_EXTREF_CTL_ENABLE_BMSK                                                      0x1
#define HWIO_GCC_SNOC_QOSGEN_EXTREF_CTL_ENABLE_SHFT                                                      0x0

#define HWIO_GCC_PCNOC_BFDCD_CMD_RCGR_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x00027000)
#define HWIO_GCC_PCNOC_BFDCD_CMD_RCGR_RMSK                                                        0x80000013
#define HWIO_GCC_PCNOC_BFDCD_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_PCNOC_BFDCD_CMD_RCGR_ADDR, HWIO_GCC_PCNOC_BFDCD_CMD_RCGR_RMSK)
#define HWIO_GCC_PCNOC_BFDCD_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_PCNOC_BFDCD_CMD_RCGR_ADDR, m)
#define HWIO_GCC_PCNOC_BFDCD_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_PCNOC_BFDCD_CMD_RCGR_ADDR,v)
#define HWIO_GCC_PCNOC_BFDCD_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PCNOC_BFDCD_CMD_RCGR_ADDR,m,v,HWIO_GCC_PCNOC_BFDCD_CMD_RCGR_IN)
#define HWIO_GCC_PCNOC_BFDCD_CMD_RCGR_ROOT_OFF_BMSK                                               0x80000000
#define HWIO_GCC_PCNOC_BFDCD_CMD_RCGR_ROOT_OFF_SHFT                                                     0x1f
#define HWIO_GCC_PCNOC_BFDCD_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                               0x10
#define HWIO_GCC_PCNOC_BFDCD_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                                0x4
#define HWIO_GCC_PCNOC_BFDCD_CMD_RCGR_ROOT_EN_BMSK                                                       0x2
#define HWIO_GCC_PCNOC_BFDCD_CMD_RCGR_ROOT_EN_SHFT                                                       0x1
#define HWIO_GCC_PCNOC_BFDCD_CMD_RCGR_UPDATE_BMSK                                                        0x1
#define HWIO_GCC_PCNOC_BFDCD_CMD_RCGR_UPDATE_SHFT                                                        0x0

#define HWIO_GCC_PCNOC_BFDCD_CFG_RCGR_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x00027004)
#define HWIO_GCC_PCNOC_BFDCD_CFG_RCGR_RMSK                                                             0x71f
#define HWIO_GCC_PCNOC_BFDCD_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_PCNOC_BFDCD_CFG_RCGR_ADDR, HWIO_GCC_PCNOC_BFDCD_CFG_RCGR_RMSK)
#define HWIO_GCC_PCNOC_BFDCD_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_PCNOC_BFDCD_CFG_RCGR_ADDR, m)
#define HWIO_GCC_PCNOC_BFDCD_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_PCNOC_BFDCD_CFG_RCGR_ADDR,v)
#define HWIO_GCC_PCNOC_BFDCD_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PCNOC_BFDCD_CFG_RCGR_ADDR,m,v,HWIO_GCC_PCNOC_BFDCD_CFG_RCGR_IN)
#define HWIO_GCC_PCNOC_BFDCD_CFG_RCGR_SRC_SEL_BMSK                                                     0x700
#define HWIO_GCC_PCNOC_BFDCD_CFG_RCGR_SRC_SEL_SHFT                                                       0x8
#define HWIO_GCC_PCNOC_BFDCD_CFG_RCGR_SRC_DIV_BMSK                                                      0x1f
#define HWIO_GCC_PCNOC_BFDCD_CFG_RCGR_SRC_DIV_SHFT                                                       0x0

#define HWIO_GCC_SYS_NOC_AXI_CBCR_ADDR                                                            (GCC_CLK_CTL_REG_REG_BASE      + 0x00026020)
#define HWIO_GCC_SYS_NOC_AXI_CBCR_RMSK                                                            0x80000001
#define HWIO_GCC_SYS_NOC_AXI_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SYS_NOC_AXI_CBCR_ADDR, HWIO_GCC_SYS_NOC_AXI_CBCR_RMSK)
#define HWIO_GCC_SYS_NOC_AXI_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SYS_NOC_AXI_CBCR_ADDR, m)
#define HWIO_GCC_SYS_NOC_AXI_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_SYS_NOC_AXI_CBCR_ADDR,v)
#define HWIO_GCC_SYS_NOC_AXI_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SYS_NOC_AXI_CBCR_ADDR,m,v,HWIO_GCC_SYS_NOC_AXI_CBCR_IN)
#define HWIO_GCC_SYS_NOC_AXI_CBCR_CLK_OFF_BMSK                                                    0x80000000
#define HWIO_GCC_SYS_NOC_AXI_CBCR_CLK_OFF_SHFT                                                          0x1f
#define HWIO_GCC_SYS_NOC_AXI_CBCR_CLK_ENABLE_BMSK                                                        0x1
#define HWIO_GCC_SYS_NOC_AXI_CBCR_CLK_ENABLE_SHFT                                                        0x0

#define HWIO_GCC_SYS_NOC_QDSS_STM_AXI_CBCR_ADDR                                                   (GCC_CLK_CTL_REG_REG_BASE      + 0x00026024)
#define HWIO_GCC_SYS_NOC_QDSS_STM_AXI_CBCR_RMSK                                                   0x80000001
#define HWIO_GCC_SYS_NOC_QDSS_STM_AXI_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SYS_NOC_QDSS_STM_AXI_CBCR_ADDR, HWIO_GCC_SYS_NOC_QDSS_STM_AXI_CBCR_RMSK)
#define HWIO_GCC_SYS_NOC_QDSS_STM_AXI_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SYS_NOC_QDSS_STM_AXI_CBCR_ADDR, m)
#define HWIO_GCC_SYS_NOC_QDSS_STM_AXI_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_SYS_NOC_QDSS_STM_AXI_CBCR_ADDR,v)
#define HWIO_GCC_SYS_NOC_QDSS_STM_AXI_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SYS_NOC_QDSS_STM_AXI_CBCR_ADDR,m,v,HWIO_GCC_SYS_NOC_QDSS_STM_AXI_CBCR_IN)
#define HWIO_GCC_SYS_NOC_QDSS_STM_AXI_CBCR_CLK_OFF_BMSK                                           0x80000000
#define HWIO_GCC_SYS_NOC_QDSS_STM_AXI_CBCR_CLK_OFF_SHFT                                                 0x1f
#define HWIO_GCC_SYS_NOC_QDSS_STM_AXI_CBCR_CLK_ENABLE_BMSK                                               0x1
#define HWIO_GCC_SYS_NOC_QDSS_STM_AXI_CBCR_CLK_ENABLE_SHFT                                               0x0

#define HWIO_GCC_SYS_NOC_APSS_AHB_CBCR_ADDR                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x00026028)
#define HWIO_GCC_SYS_NOC_APSS_AHB_CBCR_RMSK                                                       0x80000000
#define HWIO_GCC_SYS_NOC_APSS_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SYS_NOC_APSS_AHB_CBCR_ADDR, HWIO_GCC_SYS_NOC_APSS_AHB_CBCR_RMSK)
#define HWIO_GCC_SYS_NOC_APSS_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SYS_NOC_APSS_AHB_CBCR_ADDR, m)
#define HWIO_GCC_SYS_NOC_APSS_AHB_CBCR_CLK_OFF_BMSK                                               0x80000000
#define HWIO_GCC_SYS_NOC_APSS_AHB_CBCR_CLK_OFF_SHFT                                                     0x1f

#define HWIO_GCC_SNOC_PCNOC_AHB_CBCR_ADDR                                                         (GCC_CLK_CTL_REG_REG_BASE      + 0x0002602c)
#define HWIO_GCC_SNOC_PCNOC_AHB_CBCR_RMSK                                                         0x80000001
#define HWIO_GCC_SNOC_PCNOC_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SNOC_PCNOC_AHB_CBCR_ADDR, HWIO_GCC_SNOC_PCNOC_AHB_CBCR_RMSK)
#define HWIO_GCC_SNOC_PCNOC_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SNOC_PCNOC_AHB_CBCR_ADDR, m)
#define HWIO_GCC_SNOC_PCNOC_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_SNOC_PCNOC_AHB_CBCR_ADDR,v)
#define HWIO_GCC_SNOC_PCNOC_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SNOC_PCNOC_AHB_CBCR_ADDR,m,v,HWIO_GCC_SNOC_PCNOC_AHB_CBCR_IN)
#define HWIO_GCC_SNOC_PCNOC_AHB_CBCR_CLK_OFF_BMSK                                                 0x80000000
#define HWIO_GCC_SNOC_PCNOC_AHB_CBCR_CLK_OFF_SHFT                                                       0x1f
#define HWIO_GCC_SNOC_PCNOC_AHB_CBCR_CLK_ENABLE_BMSK                                                     0x1
#define HWIO_GCC_SNOC_PCNOC_AHB_CBCR_CLK_ENABLE_SHFT                                                     0x0

#define HWIO_GCC_SYS_NOC_AT_CBCR_ADDR                                                             (GCC_CLK_CTL_REG_REG_BASE      + 0x00026030)
#define HWIO_GCC_SYS_NOC_AT_CBCR_RMSK                                                             0x80000001
#define HWIO_GCC_SYS_NOC_AT_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SYS_NOC_AT_CBCR_ADDR, HWIO_GCC_SYS_NOC_AT_CBCR_RMSK)
#define HWIO_GCC_SYS_NOC_AT_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SYS_NOC_AT_CBCR_ADDR, m)
#define HWIO_GCC_SYS_NOC_AT_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_SYS_NOC_AT_CBCR_ADDR,v)
#define HWIO_GCC_SYS_NOC_AT_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SYS_NOC_AT_CBCR_ADDR,m,v,HWIO_GCC_SYS_NOC_AT_CBCR_IN)
#define HWIO_GCC_SYS_NOC_AT_CBCR_CLK_OFF_BMSK                                                     0x80000000
#define HWIO_GCC_SYS_NOC_AT_CBCR_CLK_OFF_SHFT                                                           0x1f
#define HWIO_GCC_SYS_NOC_AT_CBCR_CLK_ENABLE_BMSK                                                         0x1
#define HWIO_GCC_SYS_NOC_AT_CBCR_CLK_ENABLE_SHFT                                                         0x0

#define HWIO_GCC_PCNOC_BCR_ADDR                                                                   (GCC_CLK_CTL_REG_REG_BASE      + 0x00027018)
#define HWIO_GCC_PCNOC_BCR_RMSK                                                                          0x1
#define HWIO_GCC_PCNOC_BCR_IN          \
        in_dword_masked(HWIO_GCC_PCNOC_BCR_ADDR, HWIO_GCC_PCNOC_BCR_RMSK)
#define HWIO_GCC_PCNOC_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PCNOC_BCR_ADDR, m)
#define HWIO_GCC_PCNOC_BCR_OUT(v)      \
        out_dword(HWIO_GCC_PCNOC_BCR_ADDR,v)
#define HWIO_GCC_PCNOC_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PCNOC_BCR_ADDR,m,v,HWIO_GCC_PCNOC_BCR_IN)
#define HWIO_GCC_PCNOC_BCR_BLK_ARES_BMSK                                                                 0x1
#define HWIO_GCC_PCNOC_BCR_BLK_ARES_SHFT                                                                 0x0

#define HWIO_GCC_PCNOC_AHB_CBCR_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x0002701c)
#define HWIO_GCC_PCNOC_AHB_CBCR_RMSK                                                              0x80000001
#define HWIO_GCC_PCNOC_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_PCNOC_AHB_CBCR_ADDR, HWIO_GCC_PCNOC_AHB_CBCR_RMSK)
#define HWIO_GCC_PCNOC_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PCNOC_AHB_CBCR_ADDR, m)
#define HWIO_GCC_PCNOC_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_PCNOC_AHB_CBCR_ADDR,v)
#define HWIO_GCC_PCNOC_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PCNOC_AHB_CBCR_ADDR,m,v,HWIO_GCC_PCNOC_AHB_CBCR_IN)
#define HWIO_GCC_PCNOC_AHB_CBCR_CLK_OFF_BMSK                                                      0x80000000
#define HWIO_GCC_PCNOC_AHB_CBCR_CLK_OFF_SHFT                                                            0x1f
#define HWIO_GCC_PCNOC_AHB_CBCR_CLK_ENABLE_BMSK                                                          0x1
#define HWIO_GCC_PCNOC_AHB_CBCR_CLK_ENABLE_SHFT                                                          0x0

#define HWIO_GCC_PCNOC_DDR_CFG_CBCR_ADDR                                                          (GCC_CLK_CTL_REG_REG_BASE      + 0x00027020)
#define HWIO_GCC_PCNOC_DDR_CFG_CBCR_RMSK                                                          0x80000001
#define HWIO_GCC_PCNOC_DDR_CFG_CBCR_IN          \
        in_dword_masked(HWIO_GCC_PCNOC_DDR_CFG_CBCR_ADDR, HWIO_GCC_PCNOC_DDR_CFG_CBCR_RMSK)
#define HWIO_GCC_PCNOC_DDR_CFG_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PCNOC_DDR_CFG_CBCR_ADDR, m)
#define HWIO_GCC_PCNOC_DDR_CFG_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_PCNOC_DDR_CFG_CBCR_ADDR,v)
#define HWIO_GCC_PCNOC_DDR_CFG_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PCNOC_DDR_CFG_CBCR_ADDR,m,v,HWIO_GCC_PCNOC_DDR_CFG_CBCR_IN)
#define HWIO_GCC_PCNOC_DDR_CFG_CBCR_CLK_OFF_BMSK                                                  0x80000000
#define HWIO_GCC_PCNOC_DDR_CFG_CBCR_CLK_OFF_SHFT                                                        0x1f
#define HWIO_GCC_PCNOC_DDR_CFG_CBCR_CLK_ENABLE_BMSK                                                      0x1
#define HWIO_GCC_PCNOC_DDR_CFG_CBCR_CLK_ENABLE_SHFT                                                      0x0

#define HWIO_GCC_PCNOC_RPM_AHB_CBCR_ADDR                                                          (GCC_CLK_CTL_REG_REG_BASE      + 0x00027024)
#define HWIO_GCC_PCNOC_RPM_AHB_CBCR_RMSK                                                          0x80000001
#define HWIO_GCC_PCNOC_RPM_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_PCNOC_RPM_AHB_CBCR_ADDR, HWIO_GCC_PCNOC_RPM_AHB_CBCR_RMSK)
#define HWIO_GCC_PCNOC_RPM_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PCNOC_RPM_AHB_CBCR_ADDR, m)
#define HWIO_GCC_PCNOC_RPM_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_PCNOC_RPM_AHB_CBCR_ADDR,v)
#define HWIO_GCC_PCNOC_RPM_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PCNOC_RPM_AHB_CBCR_ADDR,m,v,HWIO_GCC_PCNOC_RPM_AHB_CBCR_IN)
#define HWIO_GCC_PCNOC_RPM_AHB_CBCR_CLK_OFF_BMSK                                                  0x80000000
#define HWIO_GCC_PCNOC_RPM_AHB_CBCR_CLK_OFF_SHFT                                                        0x1f
#define HWIO_GCC_PCNOC_RPM_AHB_CBCR_CLK_ENABLE_BMSK                                                      0x1
#define HWIO_GCC_PCNOC_RPM_AHB_CBCR_CLK_ENABLE_SHFT                                                      0x0

#define HWIO_GCC_PCNOC_AT_CBCR_ADDR                                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x00027028)
#define HWIO_GCC_PCNOC_AT_CBCR_RMSK                                                               0x80000001
#define HWIO_GCC_PCNOC_AT_CBCR_IN          \
        in_dword_masked(HWIO_GCC_PCNOC_AT_CBCR_ADDR, HWIO_GCC_PCNOC_AT_CBCR_RMSK)
#define HWIO_GCC_PCNOC_AT_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PCNOC_AT_CBCR_ADDR, m)
#define HWIO_GCC_PCNOC_AT_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_PCNOC_AT_CBCR_ADDR,v)
#define HWIO_GCC_PCNOC_AT_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PCNOC_AT_CBCR_ADDR,m,v,HWIO_GCC_PCNOC_AT_CBCR_IN)
#define HWIO_GCC_PCNOC_AT_CBCR_CLK_OFF_BMSK                                                       0x80000000
#define HWIO_GCC_PCNOC_AT_CBCR_CLK_OFF_SHFT                                                             0x1f
#define HWIO_GCC_PCNOC_AT_CBCR_CLK_ENABLE_BMSK                                                           0x1
#define HWIO_GCC_PCNOC_AT_CBCR_CLK_ENABLE_SHFT                                                           0x0

#define HWIO_GCC_NOC_CONF_XPU_AHB_CBCR_ADDR                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x00017000)
#define HWIO_GCC_NOC_CONF_XPU_AHB_CBCR_RMSK                                                       0xf0008001
#define HWIO_GCC_NOC_CONF_XPU_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_NOC_CONF_XPU_AHB_CBCR_ADDR, HWIO_GCC_NOC_CONF_XPU_AHB_CBCR_RMSK)
#define HWIO_GCC_NOC_CONF_XPU_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_NOC_CONF_XPU_AHB_CBCR_ADDR, m)
#define HWIO_GCC_NOC_CONF_XPU_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_NOC_CONF_XPU_AHB_CBCR_ADDR,v)
#define HWIO_GCC_NOC_CONF_XPU_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_NOC_CONF_XPU_AHB_CBCR_ADDR,m,v,HWIO_GCC_NOC_CONF_XPU_AHB_CBCR_IN)
#define HWIO_GCC_NOC_CONF_XPU_AHB_CBCR_CLK_OFF_BMSK                                               0x80000000
#define HWIO_GCC_NOC_CONF_XPU_AHB_CBCR_CLK_OFF_SHFT                                                     0x1f
#define HWIO_GCC_NOC_CONF_XPU_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                              0x70000000
#define HWIO_GCC_NOC_CONF_XPU_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                    0x1c
#define HWIO_GCC_NOC_CONF_XPU_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                      0x8000
#define HWIO_GCC_NOC_CONF_XPU_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                         0xf
#define HWIO_GCC_NOC_CONF_XPU_AHB_CBCR_CLK_ENABLE_BMSK                                                   0x1
#define HWIO_GCC_NOC_CONF_XPU_AHB_CBCR_CLK_ENABLE_SHFT                                                   0x0

#define HWIO_GCC_IMEM_BCR_ADDR                                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x0000e000)
#define HWIO_GCC_IMEM_BCR_RMSK                                                                           0x1
#define HWIO_GCC_IMEM_BCR_IN          \
        in_dword_masked(HWIO_GCC_IMEM_BCR_ADDR, HWIO_GCC_IMEM_BCR_RMSK)
#define HWIO_GCC_IMEM_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_IMEM_BCR_ADDR, m)
#define HWIO_GCC_IMEM_BCR_OUT(v)      \
        out_dword(HWIO_GCC_IMEM_BCR_ADDR,v)
#define HWIO_GCC_IMEM_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_IMEM_BCR_ADDR,m,v,HWIO_GCC_IMEM_BCR_IN)
#define HWIO_GCC_IMEM_BCR_BLK_ARES_BMSK                                                                  0x1
#define HWIO_GCC_IMEM_BCR_BLK_ARES_SHFT                                                                  0x0

#define HWIO_GCC_IMEM_AXI_CBCR_ADDR                                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x0000e004)
#define HWIO_GCC_IMEM_AXI_CBCR_RMSK                                                               0xf000fff0
#define HWIO_GCC_IMEM_AXI_CBCR_IN          \
        in_dword_masked(HWIO_GCC_IMEM_AXI_CBCR_ADDR, HWIO_GCC_IMEM_AXI_CBCR_RMSK)
#define HWIO_GCC_IMEM_AXI_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_IMEM_AXI_CBCR_ADDR, m)
#define HWIO_GCC_IMEM_AXI_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_IMEM_AXI_CBCR_ADDR,v)
#define HWIO_GCC_IMEM_AXI_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_IMEM_AXI_CBCR_ADDR,m,v,HWIO_GCC_IMEM_AXI_CBCR_IN)
#define HWIO_GCC_IMEM_AXI_CBCR_CLK_OFF_BMSK                                                       0x80000000
#define HWIO_GCC_IMEM_AXI_CBCR_CLK_OFF_SHFT                                                             0x1f
#define HWIO_GCC_IMEM_AXI_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                      0x70000000
#define HWIO_GCC_IMEM_AXI_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                            0x1c
#define HWIO_GCC_IMEM_AXI_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                              0x8000
#define HWIO_GCC_IMEM_AXI_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                                 0xf
#define HWIO_GCC_IMEM_AXI_CBCR_FORCE_MEM_CORE_ON_BMSK                                                 0x4000
#define HWIO_GCC_IMEM_AXI_CBCR_FORCE_MEM_CORE_ON_SHFT                                                    0xe
#define HWIO_GCC_IMEM_AXI_CBCR_FORCE_MEM_PERIPH_ON_BMSK                                               0x2000
#define HWIO_GCC_IMEM_AXI_CBCR_FORCE_MEM_PERIPH_ON_SHFT                                                  0xd
#define HWIO_GCC_IMEM_AXI_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                                              0x1000
#define HWIO_GCC_IMEM_AXI_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                                                 0xc
#define HWIO_GCC_IMEM_AXI_CBCR_WAKEUP_BMSK                                                             0xf00
#define HWIO_GCC_IMEM_AXI_CBCR_WAKEUP_SHFT                                                               0x8
#define HWIO_GCC_IMEM_AXI_CBCR_SLEEP_BMSK                                                               0xf0
#define HWIO_GCC_IMEM_AXI_CBCR_SLEEP_SHFT                                                                0x4

#define HWIO_GCC_IMEM_CFG_AHB_CBCR_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x0000e008)
#define HWIO_GCC_IMEM_CFG_AHB_CBCR_RMSK                                                           0xf0008001
#define HWIO_GCC_IMEM_CFG_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_IMEM_CFG_AHB_CBCR_ADDR, HWIO_GCC_IMEM_CFG_AHB_CBCR_RMSK)
#define HWIO_GCC_IMEM_CFG_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_IMEM_CFG_AHB_CBCR_ADDR, m)
#define HWIO_GCC_IMEM_CFG_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_IMEM_CFG_AHB_CBCR_ADDR,v)
#define HWIO_GCC_IMEM_CFG_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_IMEM_CFG_AHB_CBCR_ADDR,m,v,HWIO_GCC_IMEM_CFG_AHB_CBCR_IN)
#define HWIO_GCC_IMEM_CFG_AHB_CBCR_CLK_OFF_BMSK                                                   0x80000000
#define HWIO_GCC_IMEM_CFG_AHB_CBCR_CLK_OFF_SHFT                                                         0x1f
#define HWIO_GCC_IMEM_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                  0x70000000
#define HWIO_GCC_IMEM_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                        0x1c
#define HWIO_GCC_IMEM_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                          0x8000
#define HWIO_GCC_IMEM_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                             0xf
#define HWIO_GCC_IMEM_CFG_AHB_CBCR_CLK_ENABLE_BMSK                                                       0x1
#define HWIO_GCC_IMEM_CFG_AHB_CBCR_CLK_ENABLE_SHFT                                                       0x0

#define HWIO_GCC_MSS_CFG_AHB_CBCR_ADDR                                                            (GCC_CLK_CTL_REG_REG_BASE      + 0x00049000)
#define HWIO_GCC_MSS_CFG_AHB_CBCR_RMSK                                                            0xf0008001
#define HWIO_GCC_MSS_CFG_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_MSS_CFG_AHB_CBCR_ADDR, HWIO_GCC_MSS_CFG_AHB_CBCR_RMSK)
#define HWIO_GCC_MSS_CFG_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_MSS_CFG_AHB_CBCR_ADDR, m)
#define HWIO_GCC_MSS_CFG_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_MSS_CFG_AHB_CBCR_ADDR,v)
#define HWIO_GCC_MSS_CFG_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_MSS_CFG_AHB_CBCR_ADDR,m,v,HWIO_GCC_MSS_CFG_AHB_CBCR_IN)
#define HWIO_GCC_MSS_CFG_AHB_CBCR_CLK_OFF_BMSK                                                    0x80000000
#define HWIO_GCC_MSS_CFG_AHB_CBCR_CLK_OFF_SHFT                                                          0x1f
#define HWIO_GCC_MSS_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                   0x70000000
#define HWIO_GCC_MSS_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                         0x1c
#define HWIO_GCC_MSS_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                           0x8000
#define HWIO_GCC_MSS_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                              0xf
#define HWIO_GCC_MSS_CFG_AHB_CBCR_CLK_ENABLE_BMSK                                                        0x1
#define HWIO_GCC_MSS_CFG_AHB_CBCR_CLK_ENABLE_SHFT                                                        0x0

#define HWIO_GCC_MSS_Q6_BIMC_AXI_CBCR_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x00049004)
#define HWIO_GCC_MSS_Q6_BIMC_AXI_CBCR_RMSK                                                        0x80000003
#define HWIO_GCC_MSS_Q6_BIMC_AXI_CBCR_IN          \
        in_dword_masked(HWIO_GCC_MSS_Q6_BIMC_AXI_CBCR_ADDR, HWIO_GCC_MSS_Q6_BIMC_AXI_CBCR_RMSK)
#define HWIO_GCC_MSS_Q6_BIMC_AXI_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_MSS_Q6_BIMC_AXI_CBCR_ADDR, m)
#define HWIO_GCC_MSS_Q6_BIMC_AXI_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_MSS_Q6_BIMC_AXI_CBCR_ADDR,v)
#define HWIO_GCC_MSS_Q6_BIMC_AXI_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_MSS_Q6_BIMC_AXI_CBCR_ADDR,m,v,HWIO_GCC_MSS_Q6_BIMC_AXI_CBCR_IN)
#define HWIO_GCC_MSS_Q6_BIMC_AXI_CBCR_CLK_OFF_BMSK                                                0x80000000
#define HWIO_GCC_MSS_Q6_BIMC_AXI_CBCR_CLK_OFF_SHFT                                                      0x1f
#define HWIO_GCC_MSS_Q6_BIMC_AXI_CBCR_HW_CTL_BMSK                                                        0x2
#define HWIO_GCC_MSS_Q6_BIMC_AXI_CBCR_HW_CTL_SHFT                                                        0x1
#define HWIO_GCC_MSS_Q6_BIMC_AXI_CBCR_CLK_ENABLE_BMSK                                                    0x1
#define HWIO_GCC_MSS_Q6_BIMC_AXI_CBCR_CLK_ENABLE_SHFT                                                    0x0

#define HWIO_GCC_RPM_CFG_XPU_CBCR_ADDR                                                            (GCC_CLK_CTL_REG_REG_BASE      + 0x00017004)
#define HWIO_GCC_RPM_CFG_XPU_CBCR_RMSK                                                            0xf0008001
#define HWIO_GCC_RPM_CFG_XPU_CBCR_IN          \
        in_dword_masked(HWIO_GCC_RPM_CFG_XPU_CBCR_ADDR, HWIO_GCC_RPM_CFG_XPU_CBCR_RMSK)
#define HWIO_GCC_RPM_CFG_XPU_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_RPM_CFG_XPU_CBCR_ADDR, m)
#define HWIO_GCC_RPM_CFG_XPU_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_RPM_CFG_XPU_CBCR_ADDR,v)
#define HWIO_GCC_RPM_CFG_XPU_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_RPM_CFG_XPU_CBCR_ADDR,m,v,HWIO_GCC_RPM_CFG_XPU_CBCR_IN)
#define HWIO_GCC_RPM_CFG_XPU_CBCR_CLK_OFF_BMSK                                                    0x80000000
#define HWIO_GCC_RPM_CFG_XPU_CBCR_CLK_OFF_SHFT                                                          0x1f
#define HWIO_GCC_RPM_CFG_XPU_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                   0x70000000
#define HWIO_GCC_RPM_CFG_XPU_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                         0x1c
#define HWIO_GCC_RPM_CFG_XPU_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                           0x8000
#define HWIO_GCC_RPM_CFG_XPU_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                              0xf
#define HWIO_GCC_RPM_CFG_XPU_CBCR_CLK_ENABLE_BMSK                                                        0x1
#define HWIO_GCC_RPM_CFG_XPU_CBCR_CLK_ENABLE_SHFT                                                        0x0

#define HWIO_GCC_QDSS_BCR_ADDR                                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x00029000)
#define HWIO_GCC_QDSS_BCR_RMSK                                                                           0x1
#define HWIO_GCC_QDSS_BCR_IN          \
        in_dword_masked(HWIO_GCC_QDSS_BCR_ADDR, HWIO_GCC_QDSS_BCR_RMSK)
#define HWIO_GCC_QDSS_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_BCR_ADDR, m)
#define HWIO_GCC_QDSS_BCR_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_BCR_ADDR,v)
#define HWIO_GCC_QDSS_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_BCR_ADDR,m,v,HWIO_GCC_QDSS_BCR_IN)
#define HWIO_GCC_QDSS_BCR_BLK_ARES_BMSK                                                                  0x1
#define HWIO_GCC_QDSS_BCR_BLK_ARES_SHFT                                                                  0x0

#define HWIO_GCC_QDSS_DAP_AHB_CBCR_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x00029004)
#define HWIO_GCC_QDSS_DAP_AHB_CBCR_RMSK                                                           0x80000001
#define HWIO_GCC_QDSS_DAP_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_QDSS_DAP_AHB_CBCR_ADDR, HWIO_GCC_QDSS_DAP_AHB_CBCR_RMSK)
#define HWIO_GCC_QDSS_DAP_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_DAP_AHB_CBCR_ADDR, m)
#define HWIO_GCC_QDSS_DAP_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_DAP_AHB_CBCR_ADDR,v)
#define HWIO_GCC_QDSS_DAP_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_DAP_AHB_CBCR_ADDR,m,v,HWIO_GCC_QDSS_DAP_AHB_CBCR_IN)
#define HWIO_GCC_QDSS_DAP_AHB_CBCR_CLK_OFF_BMSK                                                   0x80000000
#define HWIO_GCC_QDSS_DAP_AHB_CBCR_CLK_OFF_SHFT                                                         0x1f
#define HWIO_GCC_QDSS_DAP_AHB_CBCR_CLK_ENABLE_BMSK                                                       0x1
#define HWIO_GCC_QDSS_DAP_AHB_CBCR_CLK_ENABLE_SHFT                                                       0x0

#define HWIO_GCC_QDSS_CFG_AHB_CBCR_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x00029008)
#define HWIO_GCC_QDSS_CFG_AHB_CBCR_RMSK                                                           0xf0008001
#define HWIO_GCC_QDSS_CFG_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_QDSS_CFG_AHB_CBCR_ADDR, HWIO_GCC_QDSS_CFG_AHB_CBCR_RMSK)
#define HWIO_GCC_QDSS_CFG_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_CFG_AHB_CBCR_ADDR, m)
#define HWIO_GCC_QDSS_CFG_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_CFG_AHB_CBCR_ADDR,v)
#define HWIO_GCC_QDSS_CFG_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_CFG_AHB_CBCR_ADDR,m,v,HWIO_GCC_QDSS_CFG_AHB_CBCR_IN)
#define HWIO_GCC_QDSS_CFG_AHB_CBCR_CLK_OFF_BMSK                                                   0x80000000
#define HWIO_GCC_QDSS_CFG_AHB_CBCR_CLK_OFF_SHFT                                                         0x1f
#define HWIO_GCC_QDSS_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                  0x70000000
#define HWIO_GCC_QDSS_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                        0x1c
#define HWIO_GCC_QDSS_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                          0x8000
#define HWIO_GCC_QDSS_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                             0xf
#define HWIO_GCC_QDSS_CFG_AHB_CBCR_CLK_ENABLE_BMSK                                                       0x1
#define HWIO_GCC_QDSS_CFG_AHB_CBCR_CLK_ENABLE_SHFT                                                       0x0

#define HWIO_GCC_QDSS_AT_CMD_RCGR_ADDR                                                            (GCC_CLK_CTL_REG_REG_BASE      + 0x0002900c)
#define HWIO_GCC_QDSS_AT_CMD_RCGR_RMSK                                                            0x80000013
#define HWIO_GCC_QDSS_AT_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_QDSS_AT_CMD_RCGR_ADDR, HWIO_GCC_QDSS_AT_CMD_RCGR_RMSK)
#define HWIO_GCC_QDSS_AT_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_AT_CMD_RCGR_ADDR, m)
#define HWIO_GCC_QDSS_AT_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_AT_CMD_RCGR_ADDR,v)
#define HWIO_GCC_QDSS_AT_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_AT_CMD_RCGR_ADDR,m,v,HWIO_GCC_QDSS_AT_CMD_RCGR_IN)
#define HWIO_GCC_QDSS_AT_CMD_RCGR_ROOT_OFF_BMSK                                                   0x80000000
#define HWIO_GCC_QDSS_AT_CMD_RCGR_ROOT_OFF_SHFT                                                         0x1f
#define HWIO_GCC_QDSS_AT_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                                   0x10
#define HWIO_GCC_QDSS_AT_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                                    0x4
#define HWIO_GCC_QDSS_AT_CMD_RCGR_ROOT_EN_BMSK                                                           0x2
#define HWIO_GCC_QDSS_AT_CMD_RCGR_ROOT_EN_SHFT                                                           0x1
#define HWIO_GCC_QDSS_AT_CMD_RCGR_UPDATE_BMSK                                                            0x1
#define HWIO_GCC_QDSS_AT_CMD_RCGR_UPDATE_SHFT                                                            0x0

#define HWIO_GCC_QDSS_AT_CFG_RCGR_ADDR                                                            (GCC_CLK_CTL_REG_REG_BASE      + 0x00029010)
#define HWIO_GCC_QDSS_AT_CFG_RCGR_RMSK                                                                 0x71f
#define HWIO_GCC_QDSS_AT_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_QDSS_AT_CFG_RCGR_ADDR, HWIO_GCC_QDSS_AT_CFG_RCGR_RMSK)
#define HWIO_GCC_QDSS_AT_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_AT_CFG_RCGR_ADDR, m)
#define HWIO_GCC_QDSS_AT_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_AT_CFG_RCGR_ADDR,v)
#define HWIO_GCC_QDSS_AT_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_AT_CFG_RCGR_ADDR,m,v,HWIO_GCC_QDSS_AT_CFG_RCGR_IN)
#define HWIO_GCC_QDSS_AT_CFG_RCGR_SRC_SEL_BMSK                                                         0x700
#define HWIO_GCC_QDSS_AT_CFG_RCGR_SRC_SEL_SHFT                                                           0x8
#define HWIO_GCC_QDSS_AT_CFG_RCGR_SRC_DIV_BMSK                                                          0x1f
#define HWIO_GCC_QDSS_AT_CFG_RCGR_SRC_DIV_SHFT                                                           0x0

#define HWIO_GCC_QDSS_AT_CBCR_ADDR                                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x00029024)
#define HWIO_GCC_QDSS_AT_CBCR_RMSK                                                                0x80007ff1
#define HWIO_GCC_QDSS_AT_CBCR_IN          \
        in_dword_masked(HWIO_GCC_QDSS_AT_CBCR_ADDR, HWIO_GCC_QDSS_AT_CBCR_RMSK)
#define HWIO_GCC_QDSS_AT_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_AT_CBCR_ADDR, m)
#define HWIO_GCC_QDSS_AT_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_AT_CBCR_ADDR,v)
#define HWIO_GCC_QDSS_AT_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_AT_CBCR_ADDR,m,v,HWIO_GCC_QDSS_AT_CBCR_IN)
#define HWIO_GCC_QDSS_AT_CBCR_CLK_OFF_BMSK                                                        0x80000000
#define HWIO_GCC_QDSS_AT_CBCR_CLK_OFF_SHFT                                                              0x1f
#define HWIO_GCC_QDSS_AT_CBCR_FORCE_MEM_CORE_ON_BMSK                                                  0x4000
#define HWIO_GCC_QDSS_AT_CBCR_FORCE_MEM_CORE_ON_SHFT                                                     0xe
#define HWIO_GCC_QDSS_AT_CBCR_FORCE_MEM_PERIPH_ON_BMSK                                                0x2000
#define HWIO_GCC_QDSS_AT_CBCR_FORCE_MEM_PERIPH_ON_SHFT                                                   0xd
#define HWIO_GCC_QDSS_AT_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                                               0x1000
#define HWIO_GCC_QDSS_AT_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                                                  0xc
#define HWIO_GCC_QDSS_AT_CBCR_WAKEUP_BMSK                                                              0xf00
#define HWIO_GCC_QDSS_AT_CBCR_WAKEUP_SHFT                                                                0x8
#define HWIO_GCC_QDSS_AT_CBCR_SLEEP_BMSK                                                                0xf0
#define HWIO_GCC_QDSS_AT_CBCR_SLEEP_SHFT                                                                 0x4
#define HWIO_GCC_QDSS_AT_CBCR_CLK_ENABLE_BMSK                                                            0x1
#define HWIO_GCC_QDSS_AT_CBCR_CLK_ENABLE_SHFT                                                            0x0

#define HWIO_GCC_QDSS_ETR_USB_CBCR_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x00029028)
#define HWIO_GCC_QDSS_ETR_USB_CBCR_RMSK                                                           0x80000001
#define HWIO_GCC_QDSS_ETR_USB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_QDSS_ETR_USB_CBCR_ADDR, HWIO_GCC_QDSS_ETR_USB_CBCR_RMSK)
#define HWIO_GCC_QDSS_ETR_USB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_ETR_USB_CBCR_ADDR, m)
#define HWIO_GCC_QDSS_ETR_USB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_ETR_USB_CBCR_ADDR,v)
#define HWIO_GCC_QDSS_ETR_USB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_ETR_USB_CBCR_ADDR,m,v,HWIO_GCC_QDSS_ETR_USB_CBCR_IN)
#define HWIO_GCC_QDSS_ETR_USB_CBCR_CLK_OFF_BMSK                                                   0x80000000
#define HWIO_GCC_QDSS_ETR_USB_CBCR_CLK_OFF_SHFT                                                         0x1f
#define HWIO_GCC_QDSS_ETR_USB_CBCR_CLK_ENABLE_BMSK                                                       0x1
#define HWIO_GCC_QDSS_ETR_USB_CBCR_CLK_ENABLE_SHFT                                                       0x0

#define HWIO_GCC_QDSS_STM_CMD_RCGR_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x0002902c)
#define HWIO_GCC_QDSS_STM_CMD_RCGR_RMSK                                                           0x80000013
#define HWIO_GCC_QDSS_STM_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_QDSS_STM_CMD_RCGR_ADDR, HWIO_GCC_QDSS_STM_CMD_RCGR_RMSK)
#define HWIO_GCC_QDSS_STM_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_STM_CMD_RCGR_ADDR, m)
#define HWIO_GCC_QDSS_STM_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_STM_CMD_RCGR_ADDR,v)
#define HWIO_GCC_QDSS_STM_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_STM_CMD_RCGR_ADDR,m,v,HWIO_GCC_QDSS_STM_CMD_RCGR_IN)
#define HWIO_GCC_QDSS_STM_CMD_RCGR_ROOT_OFF_BMSK                                                  0x80000000
#define HWIO_GCC_QDSS_STM_CMD_RCGR_ROOT_OFF_SHFT                                                        0x1f
#define HWIO_GCC_QDSS_STM_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                                  0x10
#define HWIO_GCC_QDSS_STM_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                                   0x4
#define HWIO_GCC_QDSS_STM_CMD_RCGR_ROOT_EN_BMSK                                                          0x2
#define HWIO_GCC_QDSS_STM_CMD_RCGR_ROOT_EN_SHFT                                                          0x1
#define HWIO_GCC_QDSS_STM_CMD_RCGR_UPDATE_BMSK                                                           0x1
#define HWIO_GCC_QDSS_STM_CMD_RCGR_UPDATE_SHFT                                                           0x0

#define HWIO_GCC_QDSS_STM_CFG_RCGR_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x00029030)
#define HWIO_GCC_QDSS_STM_CFG_RCGR_RMSK                                                                0x71f
#define HWIO_GCC_QDSS_STM_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_QDSS_STM_CFG_RCGR_ADDR, HWIO_GCC_QDSS_STM_CFG_RCGR_RMSK)
#define HWIO_GCC_QDSS_STM_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_STM_CFG_RCGR_ADDR, m)
#define HWIO_GCC_QDSS_STM_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_STM_CFG_RCGR_ADDR,v)
#define HWIO_GCC_QDSS_STM_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_STM_CFG_RCGR_ADDR,m,v,HWIO_GCC_QDSS_STM_CFG_RCGR_IN)
#define HWIO_GCC_QDSS_STM_CFG_RCGR_SRC_SEL_BMSK                                                        0x700
#define HWIO_GCC_QDSS_STM_CFG_RCGR_SRC_SEL_SHFT                                                          0x8
#define HWIO_GCC_QDSS_STM_CFG_RCGR_SRC_DIV_BMSK                                                         0x1f
#define HWIO_GCC_QDSS_STM_CFG_RCGR_SRC_DIV_SHFT                                                          0x0

#define HWIO_GCC_QDSS_STM_CBCR_ADDR                                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x00029044)
#define HWIO_GCC_QDSS_STM_CBCR_RMSK                                                               0xf0008001
#define HWIO_GCC_QDSS_STM_CBCR_IN          \
        in_dword_masked(HWIO_GCC_QDSS_STM_CBCR_ADDR, HWIO_GCC_QDSS_STM_CBCR_RMSK)
#define HWIO_GCC_QDSS_STM_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_STM_CBCR_ADDR, m)
#define HWIO_GCC_QDSS_STM_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_STM_CBCR_ADDR,v)
#define HWIO_GCC_QDSS_STM_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_STM_CBCR_ADDR,m,v,HWIO_GCC_QDSS_STM_CBCR_IN)
#define HWIO_GCC_QDSS_STM_CBCR_CLK_OFF_BMSK                                                       0x80000000
#define HWIO_GCC_QDSS_STM_CBCR_CLK_OFF_SHFT                                                             0x1f
#define HWIO_GCC_QDSS_STM_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                      0x70000000
#define HWIO_GCC_QDSS_STM_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                            0x1c
#define HWIO_GCC_QDSS_STM_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                              0x8000
#define HWIO_GCC_QDSS_STM_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                                 0xf
#define HWIO_GCC_QDSS_STM_CBCR_CLK_ENABLE_BMSK                                                           0x1
#define HWIO_GCC_QDSS_STM_CBCR_CLK_ENABLE_SHFT                                                           0x0

#define HWIO_GCC_QDSS_TRACECLKIN_CMD_RCGR_ADDR                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x00029048)
#define HWIO_GCC_QDSS_TRACECLKIN_CMD_RCGR_RMSK                                                    0x80000013
#define HWIO_GCC_QDSS_TRACECLKIN_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_QDSS_TRACECLKIN_CMD_RCGR_ADDR, HWIO_GCC_QDSS_TRACECLKIN_CMD_RCGR_RMSK)
#define HWIO_GCC_QDSS_TRACECLKIN_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_TRACECLKIN_CMD_RCGR_ADDR, m)
#define HWIO_GCC_QDSS_TRACECLKIN_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_TRACECLKIN_CMD_RCGR_ADDR,v)
#define HWIO_GCC_QDSS_TRACECLKIN_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_TRACECLKIN_CMD_RCGR_ADDR,m,v,HWIO_GCC_QDSS_TRACECLKIN_CMD_RCGR_IN)
#define HWIO_GCC_QDSS_TRACECLKIN_CMD_RCGR_ROOT_OFF_BMSK                                           0x80000000
#define HWIO_GCC_QDSS_TRACECLKIN_CMD_RCGR_ROOT_OFF_SHFT                                                 0x1f
#define HWIO_GCC_QDSS_TRACECLKIN_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                           0x10
#define HWIO_GCC_QDSS_TRACECLKIN_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                            0x4
#define HWIO_GCC_QDSS_TRACECLKIN_CMD_RCGR_ROOT_EN_BMSK                                                   0x2
#define HWIO_GCC_QDSS_TRACECLKIN_CMD_RCGR_ROOT_EN_SHFT                                                   0x1
#define HWIO_GCC_QDSS_TRACECLKIN_CMD_RCGR_UPDATE_BMSK                                                    0x1
#define HWIO_GCC_QDSS_TRACECLKIN_CMD_RCGR_UPDATE_SHFT                                                    0x0

#define HWIO_GCC_QDSS_TRACECLKIN_CFG_RCGR_ADDR                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x0002904c)
#define HWIO_GCC_QDSS_TRACECLKIN_CFG_RCGR_RMSK                                                         0x71f
#define HWIO_GCC_QDSS_TRACECLKIN_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_QDSS_TRACECLKIN_CFG_RCGR_ADDR, HWIO_GCC_QDSS_TRACECLKIN_CFG_RCGR_RMSK)
#define HWIO_GCC_QDSS_TRACECLKIN_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_TRACECLKIN_CFG_RCGR_ADDR, m)
#define HWIO_GCC_QDSS_TRACECLKIN_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_TRACECLKIN_CFG_RCGR_ADDR,v)
#define HWIO_GCC_QDSS_TRACECLKIN_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_TRACECLKIN_CFG_RCGR_ADDR,m,v,HWIO_GCC_QDSS_TRACECLKIN_CFG_RCGR_IN)
#define HWIO_GCC_QDSS_TRACECLKIN_CFG_RCGR_SRC_SEL_BMSK                                                 0x700
#define HWIO_GCC_QDSS_TRACECLKIN_CFG_RCGR_SRC_SEL_SHFT                                                   0x8
#define HWIO_GCC_QDSS_TRACECLKIN_CFG_RCGR_SRC_DIV_BMSK                                                  0x1f
#define HWIO_GCC_QDSS_TRACECLKIN_CFG_RCGR_SRC_DIV_SHFT                                                   0x0

#define HWIO_GCC_QDSS_TRACECLKIN_CBCR_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x00029060)
#define HWIO_GCC_QDSS_TRACECLKIN_CBCR_RMSK                                                        0x80000001
#define HWIO_GCC_QDSS_TRACECLKIN_CBCR_IN          \
        in_dword_masked(HWIO_GCC_QDSS_TRACECLKIN_CBCR_ADDR, HWIO_GCC_QDSS_TRACECLKIN_CBCR_RMSK)
#define HWIO_GCC_QDSS_TRACECLKIN_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_TRACECLKIN_CBCR_ADDR, m)
#define HWIO_GCC_QDSS_TRACECLKIN_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_TRACECLKIN_CBCR_ADDR,v)
#define HWIO_GCC_QDSS_TRACECLKIN_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_TRACECLKIN_CBCR_ADDR,m,v,HWIO_GCC_QDSS_TRACECLKIN_CBCR_IN)
#define HWIO_GCC_QDSS_TRACECLKIN_CBCR_CLK_OFF_BMSK                                                0x80000000
#define HWIO_GCC_QDSS_TRACECLKIN_CBCR_CLK_OFF_SHFT                                                      0x1f
#define HWIO_GCC_QDSS_TRACECLKIN_CBCR_CLK_ENABLE_BMSK                                                    0x1
#define HWIO_GCC_QDSS_TRACECLKIN_CBCR_CLK_ENABLE_SHFT                                                    0x0

#define HWIO_GCC_QDSS_TSCTR_CMD_RCGR_ADDR                                                         (GCC_CLK_CTL_REG_REG_BASE      + 0x00029064)
#define HWIO_GCC_QDSS_TSCTR_CMD_RCGR_RMSK                                                         0x80000013
#define HWIO_GCC_QDSS_TSCTR_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_QDSS_TSCTR_CMD_RCGR_ADDR, HWIO_GCC_QDSS_TSCTR_CMD_RCGR_RMSK)
#define HWIO_GCC_QDSS_TSCTR_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_TSCTR_CMD_RCGR_ADDR, m)
#define HWIO_GCC_QDSS_TSCTR_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_TSCTR_CMD_RCGR_ADDR,v)
#define HWIO_GCC_QDSS_TSCTR_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_TSCTR_CMD_RCGR_ADDR,m,v,HWIO_GCC_QDSS_TSCTR_CMD_RCGR_IN)
#define HWIO_GCC_QDSS_TSCTR_CMD_RCGR_ROOT_OFF_BMSK                                                0x80000000
#define HWIO_GCC_QDSS_TSCTR_CMD_RCGR_ROOT_OFF_SHFT                                                      0x1f
#define HWIO_GCC_QDSS_TSCTR_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                                0x10
#define HWIO_GCC_QDSS_TSCTR_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                                 0x4
#define HWIO_GCC_QDSS_TSCTR_CMD_RCGR_ROOT_EN_BMSK                                                        0x2
#define HWIO_GCC_QDSS_TSCTR_CMD_RCGR_ROOT_EN_SHFT                                                        0x1
#define HWIO_GCC_QDSS_TSCTR_CMD_RCGR_UPDATE_BMSK                                                         0x1
#define HWIO_GCC_QDSS_TSCTR_CMD_RCGR_UPDATE_SHFT                                                         0x0

#define HWIO_GCC_QDSS_TSCTR_CFG_RCGR_ADDR                                                         (GCC_CLK_CTL_REG_REG_BASE      + 0x00029068)
#define HWIO_GCC_QDSS_TSCTR_CFG_RCGR_RMSK                                                              0x71f
#define HWIO_GCC_QDSS_TSCTR_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_QDSS_TSCTR_CFG_RCGR_ADDR, HWIO_GCC_QDSS_TSCTR_CFG_RCGR_RMSK)
#define HWIO_GCC_QDSS_TSCTR_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_TSCTR_CFG_RCGR_ADDR, m)
#define HWIO_GCC_QDSS_TSCTR_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_TSCTR_CFG_RCGR_ADDR,v)
#define HWIO_GCC_QDSS_TSCTR_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_TSCTR_CFG_RCGR_ADDR,m,v,HWIO_GCC_QDSS_TSCTR_CFG_RCGR_IN)
#define HWIO_GCC_QDSS_TSCTR_CFG_RCGR_SRC_SEL_BMSK                                                      0x700
#define HWIO_GCC_QDSS_TSCTR_CFG_RCGR_SRC_SEL_SHFT                                                        0x8
#define HWIO_GCC_QDSS_TSCTR_CFG_RCGR_SRC_DIV_BMSK                                                       0x1f
#define HWIO_GCC_QDSS_TSCTR_CFG_RCGR_SRC_DIV_SHFT                                                        0x0

#define HWIO_GCC_QDSS_TSCTR_DIV2_CBCR_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x0002907c)
#define HWIO_GCC_QDSS_TSCTR_DIV2_CBCR_RMSK                                                        0x80000001
#define HWIO_GCC_QDSS_TSCTR_DIV2_CBCR_IN          \
        in_dword_masked(HWIO_GCC_QDSS_TSCTR_DIV2_CBCR_ADDR, HWIO_GCC_QDSS_TSCTR_DIV2_CBCR_RMSK)
#define HWIO_GCC_QDSS_TSCTR_DIV2_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_TSCTR_DIV2_CBCR_ADDR, m)
#define HWIO_GCC_QDSS_TSCTR_DIV2_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_TSCTR_DIV2_CBCR_ADDR,v)
#define HWIO_GCC_QDSS_TSCTR_DIV2_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_TSCTR_DIV2_CBCR_ADDR,m,v,HWIO_GCC_QDSS_TSCTR_DIV2_CBCR_IN)
#define HWIO_GCC_QDSS_TSCTR_DIV2_CBCR_CLK_OFF_BMSK                                                0x80000000
#define HWIO_GCC_QDSS_TSCTR_DIV2_CBCR_CLK_OFF_SHFT                                                      0x1f
#define HWIO_GCC_QDSS_TSCTR_DIV2_CBCR_CLK_ENABLE_BMSK                                                    0x1
#define HWIO_GCC_QDSS_TSCTR_DIV2_CBCR_CLK_ENABLE_SHFT                                                    0x0

#define HWIO_GCC_QDSS_TSCTR_DIV3_CBCR_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x00029080)
#define HWIO_GCC_QDSS_TSCTR_DIV3_CBCR_RMSK                                                        0x80000001
#define HWIO_GCC_QDSS_TSCTR_DIV3_CBCR_IN          \
        in_dword_masked(HWIO_GCC_QDSS_TSCTR_DIV3_CBCR_ADDR, HWIO_GCC_QDSS_TSCTR_DIV3_CBCR_RMSK)
#define HWIO_GCC_QDSS_TSCTR_DIV3_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_TSCTR_DIV3_CBCR_ADDR, m)
#define HWIO_GCC_QDSS_TSCTR_DIV3_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_TSCTR_DIV3_CBCR_ADDR,v)
#define HWIO_GCC_QDSS_TSCTR_DIV3_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_TSCTR_DIV3_CBCR_ADDR,m,v,HWIO_GCC_QDSS_TSCTR_DIV3_CBCR_IN)
#define HWIO_GCC_QDSS_TSCTR_DIV3_CBCR_CLK_OFF_BMSK                                                0x80000000
#define HWIO_GCC_QDSS_TSCTR_DIV3_CBCR_CLK_OFF_SHFT                                                      0x1f
#define HWIO_GCC_QDSS_TSCTR_DIV3_CBCR_CLK_ENABLE_BMSK                                                    0x1
#define HWIO_GCC_QDSS_TSCTR_DIV3_CBCR_CLK_ENABLE_SHFT                                                    0x0

#define HWIO_GCC_QDSS_DAP_CBCR_ADDR                                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x00029084)
#define HWIO_GCC_QDSS_DAP_CBCR_RMSK                                                               0x80000001
#define HWIO_GCC_QDSS_DAP_CBCR_IN          \
        in_dword_masked(HWIO_GCC_QDSS_DAP_CBCR_ADDR, HWIO_GCC_QDSS_DAP_CBCR_RMSK)
#define HWIO_GCC_QDSS_DAP_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_DAP_CBCR_ADDR, m)
#define HWIO_GCC_QDSS_DAP_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_DAP_CBCR_ADDR,v)
#define HWIO_GCC_QDSS_DAP_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_DAP_CBCR_ADDR,m,v,HWIO_GCC_QDSS_DAP_CBCR_IN)
#define HWIO_GCC_QDSS_DAP_CBCR_CLK_OFF_BMSK                                                       0x80000000
#define HWIO_GCC_QDSS_DAP_CBCR_CLK_OFF_SHFT                                                             0x1f
#define HWIO_GCC_QDSS_DAP_CBCR_CLK_ENABLE_BMSK                                                           0x1
#define HWIO_GCC_QDSS_DAP_CBCR_CLK_ENABLE_SHFT                                                           0x0

#define HWIO_GCC_QDSS_TSCTR_DIV4_CBCR_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x00029088)
#define HWIO_GCC_QDSS_TSCTR_DIV4_CBCR_RMSK                                                        0x80000001
#define HWIO_GCC_QDSS_TSCTR_DIV4_CBCR_IN          \
        in_dword_masked(HWIO_GCC_QDSS_TSCTR_DIV4_CBCR_ADDR, HWIO_GCC_QDSS_TSCTR_DIV4_CBCR_RMSK)
#define HWIO_GCC_QDSS_TSCTR_DIV4_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_TSCTR_DIV4_CBCR_ADDR, m)
#define HWIO_GCC_QDSS_TSCTR_DIV4_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_TSCTR_DIV4_CBCR_ADDR,v)
#define HWIO_GCC_QDSS_TSCTR_DIV4_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_TSCTR_DIV4_CBCR_ADDR,m,v,HWIO_GCC_QDSS_TSCTR_DIV4_CBCR_IN)
#define HWIO_GCC_QDSS_TSCTR_DIV4_CBCR_CLK_OFF_BMSK                                                0x80000000
#define HWIO_GCC_QDSS_TSCTR_DIV4_CBCR_CLK_OFF_SHFT                                                      0x1f
#define HWIO_GCC_QDSS_TSCTR_DIV4_CBCR_CLK_ENABLE_BMSK                                                    0x1
#define HWIO_GCC_QDSS_TSCTR_DIV4_CBCR_CLK_ENABLE_SHFT                                                    0x0

#define HWIO_GCC_QDSS_TSCTR_DIV8_CBCR_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x0002908c)
#define HWIO_GCC_QDSS_TSCTR_DIV8_CBCR_RMSK                                                        0x80000001
#define HWIO_GCC_QDSS_TSCTR_DIV8_CBCR_IN          \
        in_dword_masked(HWIO_GCC_QDSS_TSCTR_DIV8_CBCR_ADDR, HWIO_GCC_QDSS_TSCTR_DIV8_CBCR_RMSK)
#define HWIO_GCC_QDSS_TSCTR_DIV8_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_TSCTR_DIV8_CBCR_ADDR, m)
#define HWIO_GCC_QDSS_TSCTR_DIV8_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_TSCTR_DIV8_CBCR_ADDR,v)
#define HWIO_GCC_QDSS_TSCTR_DIV8_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_TSCTR_DIV8_CBCR_ADDR,m,v,HWIO_GCC_QDSS_TSCTR_DIV8_CBCR_IN)
#define HWIO_GCC_QDSS_TSCTR_DIV8_CBCR_CLK_OFF_BMSK                                                0x80000000
#define HWIO_GCC_QDSS_TSCTR_DIV8_CBCR_CLK_OFF_SHFT                                                      0x1f
#define HWIO_GCC_QDSS_TSCTR_DIV8_CBCR_CLK_ENABLE_BMSK                                                    0x1
#define HWIO_GCC_QDSS_TSCTR_DIV8_CBCR_CLK_ENABLE_SHFT                                                    0x0

#define HWIO_GCC_QDSS_TSCTR_DIV16_CBCR_ADDR                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x00029090)
#define HWIO_GCC_QDSS_TSCTR_DIV16_CBCR_RMSK                                                       0x80000001
#define HWIO_GCC_QDSS_TSCTR_DIV16_CBCR_IN          \
        in_dword_masked(HWIO_GCC_QDSS_TSCTR_DIV16_CBCR_ADDR, HWIO_GCC_QDSS_TSCTR_DIV16_CBCR_RMSK)
#define HWIO_GCC_QDSS_TSCTR_DIV16_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_QDSS_TSCTR_DIV16_CBCR_ADDR, m)
#define HWIO_GCC_QDSS_TSCTR_DIV16_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_QDSS_TSCTR_DIV16_CBCR_ADDR,v)
#define HWIO_GCC_QDSS_TSCTR_DIV16_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QDSS_TSCTR_DIV16_CBCR_ADDR,m,v,HWIO_GCC_QDSS_TSCTR_DIV16_CBCR_IN)
#define HWIO_GCC_QDSS_TSCTR_DIV16_CBCR_CLK_OFF_BMSK                                               0x80000000
#define HWIO_GCC_QDSS_TSCTR_DIV16_CBCR_CLK_OFF_SHFT                                                     0x1f
#define HWIO_GCC_QDSS_TSCTR_DIV16_CBCR_CLK_ENABLE_BMSK                                                   0x1
#define HWIO_GCC_QDSS_TSCTR_DIV16_CBCR_CLK_ENABLE_SHFT                                                   0x0

#define HWIO_GCC_GCC_SLEEP_CMD_RCGR_ADDR                                                          (GCC_CLK_CTL_REG_REG_BASE      + 0x00030000)
#define HWIO_GCC_GCC_SLEEP_CMD_RCGR_RMSK                                                          0x80000002
#define HWIO_GCC_GCC_SLEEP_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_GCC_SLEEP_CMD_RCGR_ADDR, HWIO_GCC_GCC_SLEEP_CMD_RCGR_RMSK)
#define HWIO_GCC_GCC_SLEEP_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_GCC_SLEEP_CMD_RCGR_ADDR, m)
#define HWIO_GCC_GCC_SLEEP_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_GCC_SLEEP_CMD_RCGR_ADDR,v)
#define HWIO_GCC_GCC_SLEEP_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GCC_SLEEP_CMD_RCGR_ADDR,m,v,HWIO_GCC_GCC_SLEEP_CMD_RCGR_IN)
#define HWIO_GCC_GCC_SLEEP_CMD_RCGR_ROOT_OFF_BMSK                                                 0x80000000
#define HWIO_GCC_GCC_SLEEP_CMD_RCGR_ROOT_OFF_SHFT                                                       0x1f
#define HWIO_GCC_GCC_SLEEP_CMD_RCGR_ROOT_EN_BMSK                                                         0x2
#define HWIO_GCC_GCC_SLEEP_CMD_RCGR_ROOT_EN_SHFT                                                         0x1

#define HWIO_GCC_QUSB2A_PHY_BCR_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x00041028)
#define HWIO_GCC_QUSB2A_PHY_BCR_RMSK                                                                     0x1
#define HWIO_GCC_QUSB2A_PHY_BCR_IN          \
        in_dword_masked(HWIO_GCC_QUSB2A_PHY_BCR_ADDR, HWIO_GCC_QUSB2A_PHY_BCR_RMSK)
#define HWIO_GCC_QUSB2A_PHY_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_QUSB2A_PHY_BCR_ADDR, m)
#define HWIO_GCC_QUSB2A_PHY_BCR_OUT(v)      \
        out_dword(HWIO_GCC_QUSB2A_PHY_BCR_ADDR,v)
#define HWIO_GCC_QUSB2A_PHY_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QUSB2A_PHY_BCR_ADDR,m,v,HWIO_GCC_QUSB2A_PHY_BCR_IN)
#define HWIO_GCC_QUSB2A_PHY_BCR_BLK_ARES_BMSK                                                            0x1
#define HWIO_GCC_QUSB2A_PHY_BCR_BLK_ARES_SHFT                                                            0x0

#define HWIO_GCC_SDCC1_BCR_ADDR                                                                   (GCC_CLK_CTL_REG_REG_BASE      + 0x00042000)
#define HWIO_GCC_SDCC1_BCR_RMSK                                                                          0x1
#define HWIO_GCC_SDCC1_BCR_IN          \
        in_dword_masked(HWIO_GCC_SDCC1_BCR_ADDR, HWIO_GCC_SDCC1_BCR_RMSK)
#define HWIO_GCC_SDCC1_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SDCC1_BCR_ADDR, m)
#define HWIO_GCC_SDCC1_BCR_OUT(v)      \
        out_dword(HWIO_GCC_SDCC1_BCR_ADDR,v)
#define HWIO_GCC_SDCC1_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SDCC1_BCR_ADDR,m,v,HWIO_GCC_SDCC1_BCR_IN)
#define HWIO_GCC_SDCC1_BCR_BLK_ARES_BMSK                                                                 0x1
#define HWIO_GCC_SDCC1_BCR_BLK_ARES_SHFT                                                                 0x0

#define HWIO_GCC_SDCC1_APPS_CMD_RCGR_ADDR                                                         (GCC_CLK_CTL_REG_REG_BASE      + 0x00042004)
#define HWIO_GCC_SDCC1_APPS_CMD_RCGR_RMSK                                                         0x800000f3
#define HWIO_GCC_SDCC1_APPS_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_SDCC1_APPS_CMD_RCGR_ADDR, HWIO_GCC_SDCC1_APPS_CMD_RCGR_RMSK)
#define HWIO_GCC_SDCC1_APPS_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_SDCC1_APPS_CMD_RCGR_ADDR, m)
#define HWIO_GCC_SDCC1_APPS_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_SDCC1_APPS_CMD_RCGR_ADDR,v)
#define HWIO_GCC_SDCC1_APPS_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SDCC1_APPS_CMD_RCGR_ADDR,m,v,HWIO_GCC_SDCC1_APPS_CMD_RCGR_IN)
#define HWIO_GCC_SDCC1_APPS_CMD_RCGR_ROOT_OFF_BMSK                                                0x80000000
#define HWIO_GCC_SDCC1_APPS_CMD_RCGR_ROOT_OFF_SHFT                                                      0x1f
#define HWIO_GCC_SDCC1_APPS_CMD_RCGR_DIRTY_D_BMSK                                                       0x80
#define HWIO_GCC_SDCC1_APPS_CMD_RCGR_DIRTY_D_SHFT                                                        0x7
#define HWIO_GCC_SDCC1_APPS_CMD_RCGR_DIRTY_M_BMSK                                                       0x40
#define HWIO_GCC_SDCC1_APPS_CMD_RCGR_DIRTY_M_SHFT                                                        0x6
#define HWIO_GCC_SDCC1_APPS_CMD_RCGR_DIRTY_N_BMSK                                                       0x20
#define HWIO_GCC_SDCC1_APPS_CMD_RCGR_DIRTY_N_SHFT                                                        0x5
#define HWIO_GCC_SDCC1_APPS_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                                0x10
#define HWIO_GCC_SDCC1_APPS_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                                 0x4
#define HWIO_GCC_SDCC1_APPS_CMD_RCGR_ROOT_EN_BMSK                                                        0x2
#define HWIO_GCC_SDCC1_APPS_CMD_RCGR_ROOT_EN_SHFT                                                        0x1
#define HWIO_GCC_SDCC1_APPS_CMD_RCGR_UPDATE_BMSK                                                         0x1
#define HWIO_GCC_SDCC1_APPS_CMD_RCGR_UPDATE_SHFT                                                         0x0

#define HWIO_GCC_SDCC1_APPS_CFG_RCGR_ADDR                                                         (GCC_CLK_CTL_REG_REG_BASE      + 0x00042008)
#define HWIO_GCC_SDCC1_APPS_CFG_RCGR_RMSK                                                             0x371f
#define HWIO_GCC_SDCC1_APPS_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_SDCC1_APPS_CFG_RCGR_ADDR, HWIO_GCC_SDCC1_APPS_CFG_RCGR_RMSK)
#define HWIO_GCC_SDCC1_APPS_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_SDCC1_APPS_CFG_RCGR_ADDR, m)
#define HWIO_GCC_SDCC1_APPS_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_SDCC1_APPS_CFG_RCGR_ADDR,v)
#define HWIO_GCC_SDCC1_APPS_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SDCC1_APPS_CFG_RCGR_ADDR,m,v,HWIO_GCC_SDCC1_APPS_CFG_RCGR_IN)
#define HWIO_GCC_SDCC1_APPS_CFG_RCGR_MODE_BMSK                                                        0x3000
#define HWIO_GCC_SDCC1_APPS_CFG_RCGR_MODE_SHFT                                                           0xc
#define HWIO_GCC_SDCC1_APPS_CFG_RCGR_SRC_SEL_BMSK                                                      0x700
#define HWIO_GCC_SDCC1_APPS_CFG_RCGR_SRC_SEL_SHFT                                                        0x8
#define HWIO_GCC_SDCC1_APPS_CFG_RCGR_SRC_DIV_BMSK                                                       0x1f
#define HWIO_GCC_SDCC1_APPS_CFG_RCGR_SRC_DIV_SHFT                                                        0x0

#define HWIO_GCC_SDCC1_APPS_M_ADDR                                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x0004200c)
#define HWIO_GCC_SDCC1_APPS_M_RMSK                                                                      0xff
#define HWIO_GCC_SDCC1_APPS_M_IN          \
        in_dword_masked(HWIO_GCC_SDCC1_APPS_M_ADDR, HWIO_GCC_SDCC1_APPS_M_RMSK)
#define HWIO_GCC_SDCC1_APPS_M_INM(m)      \
        in_dword_masked(HWIO_GCC_SDCC1_APPS_M_ADDR, m)
#define HWIO_GCC_SDCC1_APPS_M_OUT(v)      \
        out_dword(HWIO_GCC_SDCC1_APPS_M_ADDR,v)
#define HWIO_GCC_SDCC1_APPS_M_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SDCC1_APPS_M_ADDR,m,v,HWIO_GCC_SDCC1_APPS_M_IN)
#define HWIO_GCC_SDCC1_APPS_M_M_BMSK                                                                    0xff
#define HWIO_GCC_SDCC1_APPS_M_M_SHFT                                                                     0x0

#define HWIO_GCC_SDCC1_APPS_N_ADDR                                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x00042010)
#define HWIO_GCC_SDCC1_APPS_N_RMSK                                                                      0xff
#define HWIO_GCC_SDCC1_APPS_N_IN          \
        in_dword_masked(HWIO_GCC_SDCC1_APPS_N_ADDR, HWIO_GCC_SDCC1_APPS_N_RMSK)
#define HWIO_GCC_SDCC1_APPS_N_INM(m)      \
        in_dword_masked(HWIO_GCC_SDCC1_APPS_N_ADDR, m)
#define HWIO_GCC_SDCC1_APPS_N_OUT(v)      \
        out_dword(HWIO_GCC_SDCC1_APPS_N_ADDR,v)
#define HWIO_GCC_SDCC1_APPS_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SDCC1_APPS_N_ADDR,m,v,HWIO_GCC_SDCC1_APPS_N_IN)
#define HWIO_GCC_SDCC1_APPS_N_NOT_N_MINUS_M_BMSK                                                        0xff
#define HWIO_GCC_SDCC1_APPS_N_NOT_N_MINUS_M_SHFT                                                         0x0

#define HWIO_GCC_SDCC1_APPS_D_ADDR                                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x00042014)
#define HWIO_GCC_SDCC1_APPS_D_RMSK                                                                      0xff
#define HWIO_GCC_SDCC1_APPS_D_IN          \
        in_dword_masked(HWIO_GCC_SDCC1_APPS_D_ADDR, HWIO_GCC_SDCC1_APPS_D_RMSK)
#define HWIO_GCC_SDCC1_APPS_D_INM(m)      \
        in_dword_masked(HWIO_GCC_SDCC1_APPS_D_ADDR, m)
#define HWIO_GCC_SDCC1_APPS_D_OUT(v)      \
        out_dword(HWIO_GCC_SDCC1_APPS_D_ADDR,v)
#define HWIO_GCC_SDCC1_APPS_D_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SDCC1_APPS_D_ADDR,m,v,HWIO_GCC_SDCC1_APPS_D_IN)
#define HWIO_GCC_SDCC1_APPS_D_NOT_2D_BMSK                                                               0xff
#define HWIO_GCC_SDCC1_APPS_D_NOT_2D_SHFT                                                                0x0

#define HWIO_GCC_SDCC1_APPS_CBCR_ADDR                                                             (GCC_CLK_CTL_REG_REG_BASE      + 0x00042018)
#define HWIO_GCC_SDCC1_APPS_CBCR_RMSK                                                             0x80007ff1
#define HWIO_GCC_SDCC1_APPS_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SDCC1_APPS_CBCR_ADDR, HWIO_GCC_SDCC1_APPS_CBCR_RMSK)
#define HWIO_GCC_SDCC1_APPS_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SDCC1_APPS_CBCR_ADDR, m)
#define HWIO_GCC_SDCC1_APPS_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_SDCC1_APPS_CBCR_ADDR,v)
#define HWIO_GCC_SDCC1_APPS_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SDCC1_APPS_CBCR_ADDR,m,v,HWIO_GCC_SDCC1_APPS_CBCR_IN)
#define HWIO_GCC_SDCC1_APPS_CBCR_CLK_OFF_BMSK                                                     0x80000000
#define HWIO_GCC_SDCC1_APPS_CBCR_CLK_OFF_SHFT                                                           0x1f
#define HWIO_GCC_SDCC1_APPS_CBCR_FORCE_MEM_CORE_ON_BMSK                                               0x4000
#define HWIO_GCC_SDCC1_APPS_CBCR_FORCE_MEM_CORE_ON_SHFT                                                  0xe
#define HWIO_GCC_SDCC1_APPS_CBCR_FORCE_MEM_PERIPH_ON_BMSK                                             0x2000
#define HWIO_GCC_SDCC1_APPS_CBCR_FORCE_MEM_PERIPH_ON_SHFT                                                0xd
#define HWIO_GCC_SDCC1_APPS_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                                            0x1000
#define HWIO_GCC_SDCC1_APPS_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                                               0xc
#define HWIO_GCC_SDCC1_APPS_CBCR_WAKEUP_BMSK                                                           0xf00
#define HWIO_GCC_SDCC1_APPS_CBCR_WAKEUP_SHFT                                                             0x8
#define HWIO_GCC_SDCC1_APPS_CBCR_SLEEP_BMSK                                                             0xf0
#define HWIO_GCC_SDCC1_APPS_CBCR_SLEEP_SHFT                                                              0x4
#define HWIO_GCC_SDCC1_APPS_CBCR_CLK_ENABLE_BMSK                                                         0x1
#define HWIO_GCC_SDCC1_APPS_CBCR_CLK_ENABLE_SHFT                                                         0x0

#define HWIO_GCC_SDCC1_AHB_CBCR_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x0004201c)
#define HWIO_GCC_SDCC1_AHB_CBCR_RMSK                                                              0xf000fff1
#define HWIO_GCC_SDCC1_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SDCC1_AHB_CBCR_ADDR, HWIO_GCC_SDCC1_AHB_CBCR_RMSK)
#define HWIO_GCC_SDCC1_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SDCC1_AHB_CBCR_ADDR, m)
#define HWIO_GCC_SDCC1_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_SDCC1_AHB_CBCR_ADDR,v)
#define HWIO_GCC_SDCC1_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SDCC1_AHB_CBCR_ADDR,m,v,HWIO_GCC_SDCC1_AHB_CBCR_IN)
#define HWIO_GCC_SDCC1_AHB_CBCR_CLK_OFF_BMSK                                                      0x80000000
#define HWIO_GCC_SDCC1_AHB_CBCR_CLK_OFF_SHFT                                                            0x1f
#define HWIO_GCC_SDCC1_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                     0x70000000
#define HWIO_GCC_SDCC1_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                           0x1c
#define HWIO_GCC_SDCC1_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                             0x8000
#define HWIO_GCC_SDCC1_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                                0xf
#define HWIO_GCC_SDCC1_AHB_CBCR_FORCE_MEM_CORE_ON_BMSK                                                0x4000
#define HWIO_GCC_SDCC1_AHB_CBCR_FORCE_MEM_CORE_ON_SHFT                                                   0xe
#define HWIO_GCC_SDCC1_AHB_CBCR_FORCE_MEM_PERIPH_ON_BMSK                                              0x2000
#define HWIO_GCC_SDCC1_AHB_CBCR_FORCE_MEM_PERIPH_ON_SHFT                                                 0xd
#define HWIO_GCC_SDCC1_AHB_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                                             0x1000
#define HWIO_GCC_SDCC1_AHB_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                                                0xc
#define HWIO_GCC_SDCC1_AHB_CBCR_WAKEUP_BMSK                                                            0xf00
#define HWIO_GCC_SDCC1_AHB_CBCR_WAKEUP_SHFT                                                              0x8
#define HWIO_GCC_SDCC1_AHB_CBCR_SLEEP_BMSK                                                              0xf0
#define HWIO_GCC_SDCC1_AHB_CBCR_SLEEP_SHFT                                                               0x4
#define HWIO_GCC_SDCC1_AHB_CBCR_CLK_ENABLE_BMSK                                                          0x1
#define HWIO_GCC_SDCC1_AHB_CBCR_CLK_ENABLE_SHFT                                                          0x0

#define HWIO_GCC_BLSP1_BCR_ADDR                                                                   (GCC_CLK_CTL_REG_REG_BASE      + 0x00001000)
#define HWIO_GCC_BLSP1_BCR_RMSK                                                                          0x1
#define HWIO_GCC_BLSP1_BCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_BCR_ADDR, HWIO_GCC_BLSP1_BCR_RMSK)
#define HWIO_GCC_BLSP1_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_BCR_ADDR, m)
#define HWIO_GCC_BLSP1_BCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_BCR_ADDR,v)
#define HWIO_GCC_BLSP1_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_BCR_ADDR,m,v,HWIO_GCC_BLSP1_BCR_IN)
#define HWIO_GCC_BLSP1_BCR_BLK_ARES_BMSK                                                                 0x1
#define HWIO_GCC_BLSP1_BCR_BLK_ARES_SHFT                                                                 0x0

#define HWIO_GCC_BLSP1_SLEEP_CBCR_ADDR                                                            (GCC_CLK_CTL_REG_REG_BASE      + 0x00001004)
#define HWIO_GCC_BLSP1_SLEEP_CBCR_RMSK                                                            0x80000000
#define HWIO_GCC_BLSP1_SLEEP_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_SLEEP_CBCR_ADDR, HWIO_GCC_BLSP1_SLEEP_CBCR_RMSK)
#define HWIO_GCC_BLSP1_SLEEP_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_SLEEP_CBCR_ADDR, m)
#define HWIO_GCC_BLSP1_SLEEP_CBCR_CLK_OFF_BMSK                                                    0x80000000
#define HWIO_GCC_BLSP1_SLEEP_CBCR_CLK_OFF_SHFT                                                          0x1f

#define HWIO_GCC_BLSP1_AHB_CBCR_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x00001008)
#define HWIO_GCC_BLSP1_AHB_CBCR_RMSK                                                              0xf000fff0
#define HWIO_GCC_BLSP1_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_AHB_CBCR_ADDR, HWIO_GCC_BLSP1_AHB_CBCR_RMSK)
#define HWIO_GCC_BLSP1_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_AHB_CBCR_ADDR, m)
#define HWIO_GCC_BLSP1_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_AHB_CBCR_ADDR,v)
#define HWIO_GCC_BLSP1_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_AHB_CBCR_ADDR,m,v,HWIO_GCC_BLSP1_AHB_CBCR_IN)
#define HWIO_GCC_BLSP1_AHB_CBCR_CLK_OFF_BMSK                                                      0x80000000
#define HWIO_GCC_BLSP1_AHB_CBCR_CLK_OFF_SHFT                                                            0x1f
#define HWIO_GCC_BLSP1_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                     0x70000000
#define HWIO_GCC_BLSP1_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                           0x1c
#define HWIO_GCC_BLSP1_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                             0x8000
#define HWIO_GCC_BLSP1_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                                0xf
#define HWIO_GCC_BLSP1_AHB_CBCR_FORCE_MEM_CORE_ON_BMSK                                                0x4000
#define HWIO_GCC_BLSP1_AHB_CBCR_FORCE_MEM_CORE_ON_SHFT                                                   0xe
#define HWIO_GCC_BLSP1_AHB_CBCR_FORCE_MEM_PERIPH_ON_BMSK                                              0x2000
#define HWIO_GCC_BLSP1_AHB_CBCR_FORCE_MEM_PERIPH_ON_SHFT                                                 0xd
#define HWIO_GCC_BLSP1_AHB_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                                             0x1000
#define HWIO_GCC_BLSP1_AHB_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                                                0xc
#define HWIO_GCC_BLSP1_AHB_CBCR_WAKEUP_BMSK                                                            0xf00
#define HWIO_GCC_BLSP1_AHB_CBCR_WAKEUP_SHFT                                                              0x8
#define HWIO_GCC_BLSP1_AHB_CBCR_SLEEP_BMSK                                                              0xf0
#define HWIO_GCC_BLSP1_AHB_CBCR_SLEEP_SHFT                                                               0x4

#define HWIO_GCC_BLSP1_QUP1_BCR_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x00002000)
#define HWIO_GCC_BLSP1_QUP1_BCR_RMSK                                                                     0x1
#define HWIO_GCC_BLSP1_QUP1_BCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP1_BCR_ADDR, HWIO_GCC_BLSP1_QUP1_BCR_RMSK)
#define HWIO_GCC_BLSP1_QUP1_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP1_BCR_ADDR, m)
#define HWIO_GCC_BLSP1_QUP1_BCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP1_BCR_ADDR,v)
#define HWIO_GCC_BLSP1_QUP1_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP1_BCR_ADDR,m,v,HWIO_GCC_BLSP1_QUP1_BCR_IN)
#define HWIO_GCC_BLSP1_QUP1_BCR_BLK_ARES_BMSK                                                            0x1
#define HWIO_GCC_BLSP1_QUP1_BCR_BLK_ARES_SHFT                                                            0x0

#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CBCR_ADDR                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x00002004)
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CBCR_RMSK                                                    0x80000001
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP1_SPI_APPS_CBCR_ADDR, HWIO_GCC_BLSP1_QUP1_SPI_APPS_CBCR_RMSK)
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP1_SPI_APPS_CBCR_ADDR, m)
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP1_SPI_APPS_CBCR_ADDR,v)
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP1_SPI_APPS_CBCR_ADDR,m,v,HWIO_GCC_BLSP1_QUP1_SPI_APPS_CBCR_IN)
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CBCR_CLK_OFF_BMSK                                            0x80000000
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CBCR_CLK_OFF_SHFT                                                  0x1f
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CBCR_CLK_ENABLE_BMSK                                                0x1
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CBCR_CLK_ENABLE_SHFT                                                0x0

#define HWIO_GCC_BLSP1_QUP1_I2C_APPS_CBCR_ADDR                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x00002008)
#define HWIO_GCC_BLSP1_QUP1_I2C_APPS_CBCR_RMSK                                                    0x80000001
#define HWIO_GCC_BLSP1_QUP1_I2C_APPS_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP1_I2C_APPS_CBCR_ADDR, HWIO_GCC_BLSP1_QUP1_I2C_APPS_CBCR_RMSK)
#define HWIO_GCC_BLSP1_QUP1_I2C_APPS_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP1_I2C_APPS_CBCR_ADDR, m)
#define HWIO_GCC_BLSP1_QUP1_I2C_APPS_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP1_I2C_APPS_CBCR_ADDR,v)
#define HWIO_GCC_BLSP1_QUP1_I2C_APPS_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP1_I2C_APPS_CBCR_ADDR,m,v,HWIO_GCC_BLSP1_QUP1_I2C_APPS_CBCR_IN)
#define HWIO_GCC_BLSP1_QUP1_I2C_APPS_CBCR_CLK_OFF_BMSK                                            0x80000000
#define HWIO_GCC_BLSP1_QUP1_I2C_APPS_CBCR_CLK_OFF_SHFT                                                  0x1f
#define HWIO_GCC_BLSP1_QUP1_I2C_APPS_CBCR_CLK_ENABLE_BMSK                                                0x1
#define HWIO_GCC_BLSP1_QUP1_I2C_APPS_CBCR_CLK_ENABLE_SHFT                                                0x0

#define HWIO_GCC_BLSP1_QUP1_I2C_APPS_CMD_RCGR_ADDR                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x0000200c)
#define HWIO_GCC_BLSP1_QUP1_I2C_APPS_CMD_RCGR_RMSK                                                0x80000013
#define HWIO_GCC_BLSP1_QUP1_I2C_APPS_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP1_I2C_APPS_CMD_RCGR_ADDR, HWIO_GCC_BLSP1_QUP1_I2C_APPS_CMD_RCGR_RMSK)
#define HWIO_GCC_BLSP1_QUP1_I2C_APPS_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP1_I2C_APPS_CMD_RCGR_ADDR, m)
#define HWIO_GCC_BLSP1_QUP1_I2C_APPS_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP1_I2C_APPS_CMD_RCGR_ADDR,v)
#define HWIO_GCC_BLSP1_QUP1_I2C_APPS_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP1_I2C_APPS_CMD_RCGR_ADDR,m,v,HWIO_GCC_BLSP1_QUP1_I2C_APPS_CMD_RCGR_IN)
#define HWIO_GCC_BLSP1_QUP1_I2C_APPS_CMD_RCGR_ROOT_OFF_BMSK                                       0x80000000
#define HWIO_GCC_BLSP1_QUP1_I2C_APPS_CMD_RCGR_ROOT_OFF_SHFT                                             0x1f
#define HWIO_GCC_BLSP1_QUP1_I2C_APPS_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                       0x10
#define HWIO_GCC_BLSP1_QUP1_I2C_APPS_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                        0x4
#define HWIO_GCC_BLSP1_QUP1_I2C_APPS_CMD_RCGR_ROOT_EN_BMSK                                               0x2
#define HWIO_GCC_BLSP1_QUP1_I2C_APPS_CMD_RCGR_ROOT_EN_SHFT                                               0x1
#define HWIO_GCC_BLSP1_QUP1_I2C_APPS_CMD_RCGR_UPDATE_BMSK                                                0x1
#define HWIO_GCC_BLSP1_QUP1_I2C_APPS_CMD_RCGR_UPDATE_SHFT                                                0x0

#define HWIO_GCC_BLSP1_QUP1_I2C_APPS_CFG_RCGR_ADDR                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x00002010)
#define HWIO_GCC_BLSP1_QUP1_I2C_APPS_CFG_RCGR_RMSK                                                     0x71f
#define HWIO_GCC_BLSP1_QUP1_I2C_APPS_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP1_I2C_APPS_CFG_RCGR_ADDR, HWIO_GCC_BLSP1_QUP1_I2C_APPS_CFG_RCGR_RMSK)
#define HWIO_GCC_BLSP1_QUP1_I2C_APPS_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP1_I2C_APPS_CFG_RCGR_ADDR, m)
#define HWIO_GCC_BLSP1_QUP1_I2C_APPS_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP1_I2C_APPS_CFG_RCGR_ADDR,v)
#define HWIO_GCC_BLSP1_QUP1_I2C_APPS_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP1_I2C_APPS_CFG_RCGR_ADDR,m,v,HWIO_GCC_BLSP1_QUP1_I2C_APPS_CFG_RCGR_IN)
#define HWIO_GCC_BLSP1_QUP1_I2C_APPS_CFG_RCGR_SRC_SEL_BMSK                                             0x700
#define HWIO_GCC_BLSP1_QUP1_I2C_APPS_CFG_RCGR_SRC_SEL_SHFT                                               0x8
#define HWIO_GCC_BLSP1_QUP1_I2C_APPS_CFG_RCGR_SRC_DIV_BMSK                                              0x1f
#define HWIO_GCC_BLSP1_QUP1_I2C_APPS_CFG_RCGR_SRC_DIV_SHFT                                               0x0

#define HWIO_GCC_BLSP1_QUP2_I2C_APPS_CMD_RCGR_ADDR                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x00003000)
#define HWIO_GCC_BLSP1_QUP2_I2C_APPS_CMD_RCGR_RMSK                                                0x80000013
#define HWIO_GCC_BLSP1_QUP2_I2C_APPS_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP2_I2C_APPS_CMD_RCGR_ADDR, HWIO_GCC_BLSP1_QUP2_I2C_APPS_CMD_RCGR_RMSK)
#define HWIO_GCC_BLSP1_QUP2_I2C_APPS_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP2_I2C_APPS_CMD_RCGR_ADDR, m)
#define HWIO_GCC_BLSP1_QUP2_I2C_APPS_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP2_I2C_APPS_CMD_RCGR_ADDR,v)
#define HWIO_GCC_BLSP1_QUP2_I2C_APPS_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP2_I2C_APPS_CMD_RCGR_ADDR,m,v,HWIO_GCC_BLSP1_QUP2_I2C_APPS_CMD_RCGR_IN)
#define HWIO_GCC_BLSP1_QUP2_I2C_APPS_CMD_RCGR_ROOT_OFF_BMSK                                       0x80000000
#define HWIO_GCC_BLSP1_QUP2_I2C_APPS_CMD_RCGR_ROOT_OFF_SHFT                                             0x1f
#define HWIO_GCC_BLSP1_QUP2_I2C_APPS_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                       0x10
#define HWIO_GCC_BLSP1_QUP2_I2C_APPS_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                        0x4
#define HWIO_GCC_BLSP1_QUP2_I2C_APPS_CMD_RCGR_ROOT_EN_BMSK                                               0x2
#define HWIO_GCC_BLSP1_QUP2_I2C_APPS_CMD_RCGR_ROOT_EN_SHFT                                               0x1
#define HWIO_GCC_BLSP1_QUP2_I2C_APPS_CMD_RCGR_UPDATE_BMSK                                                0x1
#define HWIO_GCC_BLSP1_QUP2_I2C_APPS_CMD_RCGR_UPDATE_SHFT                                                0x0

#define HWIO_GCC_BLSP1_QUP2_I2C_APPS_CFG_RCGR_ADDR                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x00003004)
#define HWIO_GCC_BLSP1_QUP2_I2C_APPS_CFG_RCGR_RMSK                                                     0x71f
#define HWIO_GCC_BLSP1_QUP2_I2C_APPS_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP2_I2C_APPS_CFG_RCGR_ADDR, HWIO_GCC_BLSP1_QUP2_I2C_APPS_CFG_RCGR_RMSK)
#define HWIO_GCC_BLSP1_QUP2_I2C_APPS_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP2_I2C_APPS_CFG_RCGR_ADDR, m)
#define HWIO_GCC_BLSP1_QUP2_I2C_APPS_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP2_I2C_APPS_CFG_RCGR_ADDR,v)
#define HWIO_GCC_BLSP1_QUP2_I2C_APPS_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP2_I2C_APPS_CFG_RCGR_ADDR,m,v,HWIO_GCC_BLSP1_QUP2_I2C_APPS_CFG_RCGR_IN)
#define HWIO_GCC_BLSP1_QUP2_I2C_APPS_CFG_RCGR_SRC_SEL_BMSK                                             0x700
#define HWIO_GCC_BLSP1_QUP2_I2C_APPS_CFG_RCGR_SRC_SEL_SHFT                                               0x8
#define HWIO_GCC_BLSP1_QUP2_I2C_APPS_CFG_RCGR_SRC_DIV_BMSK                                              0x1f
#define HWIO_GCC_BLSP1_QUP2_I2C_APPS_CFG_RCGR_SRC_DIV_SHFT                                               0x0

#define HWIO_GCC_BLSP1_QUP3_I2C_APPS_CMD_RCGR_ADDR                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x00004000)
#define HWIO_GCC_BLSP1_QUP3_I2C_APPS_CMD_RCGR_RMSK                                                0x80000013
#define HWIO_GCC_BLSP1_QUP3_I2C_APPS_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP3_I2C_APPS_CMD_RCGR_ADDR, HWIO_GCC_BLSP1_QUP3_I2C_APPS_CMD_RCGR_RMSK)
#define HWIO_GCC_BLSP1_QUP3_I2C_APPS_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP3_I2C_APPS_CMD_RCGR_ADDR, m)
#define HWIO_GCC_BLSP1_QUP3_I2C_APPS_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP3_I2C_APPS_CMD_RCGR_ADDR,v)
#define HWIO_GCC_BLSP1_QUP3_I2C_APPS_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP3_I2C_APPS_CMD_RCGR_ADDR,m,v,HWIO_GCC_BLSP1_QUP3_I2C_APPS_CMD_RCGR_IN)
#define HWIO_GCC_BLSP1_QUP3_I2C_APPS_CMD_RCGR_ROOT_OFF_BMSK                                       0x80000000
#define HWIO_GCC_BLSP1_QUP3_I2C_APPS_CMD_RCGR_ROOT_OFF_SHFT                                             0x1f
#define HWIO_GCC_BLSP1_QUP3_I2C_APPS_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                       0x10
#define HWIO_GCC_BLSP1_QUP3_I2C_APPS_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                        0x4
#define HWIO_GCC_BLSP1_QUP3_I2C_APPS_CMD_RCGR_ROOT_EN_BMSK                                               0x2
#define HWIO_GCC_BLSP1_QUP3_I2C_APPS_CMD_RCGR_ROOT_EN_SHFT                                               0x1
#define HWIO_GCC_BLSP1_QUP3_I2C_APPS_CMD_RCGR_UPDATE_BMSK                                                0x1
#define HWIO_GCC_BLSP1_QUP3_I2C_APPS_CMD_RCGR_UPDATE_SHFT                                                0x0

#define HWIO_GCC_BLSP1_QUP3_I2C_APPS_CFG_RCGR_ADDR                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x00004004)
#define HWIO_GCC_BLSP1_QUP3_I2C_APPS_CFG_RCGR_RMSK                                                     0x71f
#define HWIO_GCC_BLSP1_QUP3_I2C_APPS_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP3_I2C_APPS_CFG_RCGR_ADDR, HWIO_GCC_BLSP1_QUP3_I2C_APPS_CFG_RCGR_RMSK)
#define HWIO_GCC_BLSP1_QUP3_I2C_APPS_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP3_I2C_APPS_CFG_RCGR_ADDR, m)
#define HWIO_GCC_BLSP1_QUP3_I2C_APPS_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP3_I2C_APPS_CFG_RCGR_ADDR,v)
#define HWIO_GCC_BLSP1_QUP3_I2C_APPS_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP3_I2C_APPS_CFG_RCGR_ADDR,m,v,HWIO_GCC_BLSP1_QUP3_I2C_APPS_CFG_RCGR_IN)
#define HWIO_GCC_BLSP1_QUP3_I2C_APPS_CFG_RCGR_SRC_SEL_BMSK                                             0x700
#define HWIO_GCC_BLSP1_QUP3_I2C_APPS_CFG_RCGR_SRC_SEL_SHFT                                               0x8
#define HWIO_GCC_BLSP1_QUP3_I2C_APPS_CFG_RCGR_SRC_DIV_BMSK                                              0x1f
#define HWIO_GCC_BLSP1_QUP3_I2C_APPS_CFG_RCGR_SRC_DIV_SHFT                                               0x0

#define HWIO_GCC_BLSP1_QUP4_I2C_APPS_CMD_RCGR_ADDR                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x00005000)
#define HWIO_GCC_BLSP1_QUP4_I2C_APPS_CMD_RCGR_RMSK                                                0x80000013
#define HWIO_GCC_BLSP1_QUP4_I2C_APPS_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP4_I2C_APPS_CMD_RCGR_ADDR, HWIO_GCC_BLSP1_QUP4_I2C_APPS_CMD_RCGR_RMSK)
#define HWIO_GCC_BLSP1_QUP4_I2C_APPS_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP4_I2C_APPS_CMD_RCGR_ADDR, m)
#define HWIO_GCC_BLSP1_QUP4_I2C_APPS_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP4_I2C_APPS_CMD_RCGR_ADDR,v)
#define HWIO_GCC_BLSP1_QUP4_I2C_APPS_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP4_I2C_APPS_CMD_RCGR_ADDR,m,v,HWIO_GCC_BLSP1_QUP4_I2C_APPS_CMD_RCGR_IN)
#define HWIO_GCC_BLSP1_QUP4_I2C_APPS_CMD_RCGR_ROOT_OFF_BMSK                                       0x80000000
#define HWIO_GCC_BLSP1_QUP4_I2C_APPS_CMD_RCGR_ROOT_OFF_SHFT                                             0x1f
#define HWIO_GCC_BLSP1_QUP4_I2C_APPS_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                       0x10
#define HWIO_GCC_BLSP1_QUP4_I2C_APPS_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                        0x4
#define HWIO_GCC_BLSP1_QUP4_I2C_APPS_CMD_RCGR_ROOT_EN_BMSK                                               0x2
#define HWIO_GCC_BLSP1_QUP4_I2C_APPS_CMD_RCGR_ROOT_EN_SHFT                                               0x1
#define HWIO_GCC_BLSP1_QUP4_I2C_APPS_CMD_RCGR_UPDATE_BMSK                                                0x1
#define HWIO_GCC_BLSP1_QUP4_I2C_APPS_CMD_RCGR_UPDATE_SHFT                                                0x0

#define HWIO_GCC_BLSP1_QUP4_I2C_APPS_CFG_RCGR_ADDR                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x00005004)
#define HWIO_GCC_BLSP1_QUP4_I2C_APPS_CFG_RCGR_RMSK                                                     0x71f
#define HWIO_GCC_BLSP1_QUP4_I2C_APPS_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP4_I2C_APPS_CFG_RCGR_ADDR, HWIO_GCC_BLSP1_QUP4_I2C_APPS_CFG_RCGR_RMSK)
#define HWIO_GCC_BLSP1_QUP4_I2C_APPS_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP4_I2C_APPS_CFG_RCGR_ADDR, m)
#define HWIO_GCC_BLSP1_QUP4_I2C_APPS_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP4_I2C_APPS_CFG_RCGR_ADDR,v)
#define HWIO_GCC_BLSP1_QUP4_I2C_APPS_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP4_I2C_APPS_CFG_RCGR_ADDR,m,v,HWIO_GCC_BLSP1_QUP4_I2C_APPS_CFG_RCGR_IN)
#define HWIO_GCC_BLSP1_QUP4_I2C_APPS_CFG_RCGR_SRC_SEL_BMSK                                             0x700
#define HWIO_GCC_BLSP1_QUP4_I2C_APPS_CFG_RCGR_SRC_SEL_SHFT                                               0x8
#define HWIO_GCC_BLSP1_QUP4_I2C_APPS_CFG_RCGR_SRC_DIV_BMSK                                              0x1f
#define HWIO_GCC_BLSP1_QUP4_I2C_APPS_CFG_RCGR_SRC_DIV_SHFT                                               0x0

#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CMD_RCGR_ADDR                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x00002024)
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CMD_RCGR_RMSK                                                0x800000f3
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP1_SPI_APPS_CMD_RCGR_ADDR, HWIO_GCC_BLSP1_QUP1_SPI_APPS_CMD_RCGR_RMSK)
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP1_SPI_APPS_CMD_RCGR_ADDR, m)
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP1_SPI_APPS_CMD_RCGR_ADDR,v)
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP1_SPI_APPS_CMD_RCGR_ADDR,m,v,HWIO_GCC_BLSP1_QUP1_SPI_APPS_CMD_RCGR_IN)
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CMD_RCGR_ROOT_OFF_BMSK                                       0x80000000
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CMD_RCGR_ROOT_OFF_SHFT                                             0x1f
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CMD_RCGR_DIRTY_D_BMSK                                              0x80
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CMD_RCGR_DIRTY_D_SHFT                                               0x7
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CMD_RCGR_DIRTY_M_BMSK                                              0x40
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CMD_RCGR_DIRTY_M_SHFT                                               0x6
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CMD_RCGR_DIRTY_N_BMSK                                              0x20
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CMD_RCGR_DIRTY_N_SHFT                                               0x5
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                       0x10
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                        0x4
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CMD_RCGR_ROOT_EN_BMSK                                               0x2
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CMD_RCGR_ROOT_EN_SHFT                                               0x1
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CMD_RCGR_UPDATE_BMSK                                                0x1
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CMD_RCGR_UPDATE_SHFT                                                0x0

#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CFG_RCGR_ADDR                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x00002028)
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CFG_RCGR_RMSK                                                    0x371f
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP1_SPI_APPS_CFG_RCGR_ADDR, HWIO_GCC_BLSP1_QUP1_SPI_APPS_CFG_RCGR_RMSK)
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP1_SPI_APPS_CFG_RCGR_ADDR, m)
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP1_SPI_APPS_CFG_RCGR_ADDR,v)
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP1_SPI_APPS_CFG_RCGR_ADDR,m,v,HWIO_GCC_BLSP1_QUP1_SPI_APPS_CFG_RCGR_IN)
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CFG_RCGR_MODE_BMSK                                               0x3000
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CFG_RCGR_MODE_SHFT                                                  0xc
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CFG_RCGR_SRC_SEL_BMSK                                             0x700
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CFG_RCGR_SRC_SEL_SHFT                                               0x8
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CFG_RCGR_SRC_DIV_BMSK                                              0x1f
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_CFG_RCGR_SRC_DIV_SHFT                                               0x0

#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_M_ADDR                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x0000202c)
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_M_RMSK                                                             0xff
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_M_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP1_SPI_APPS_M_ADDR, HWIO_GCC_BLSP1_QUP1_SPI_APPS_M_RMSK)
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_M_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP1_SPI_APPS_M_ADDR, m)
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_M_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP1_SPI_APPS_M_ADDR,v)
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_M_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP1_SPI_APPS_M_ADDR,m,v,HWIO_GCC_BLSP1_QUP1_SPI_APPS_M_IN)
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_M_M_BMSK                                                           0xff
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_M_M_SHFT                                                            0x0

#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_N_ADDR                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x00002030)
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_N_RMSK                                                             0xff
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_N_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP1_SPI_APPS_N_ADDR, HWIO_GCC_BLSP1_QUP1_SPI_APPS_N_RMSK)
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_N_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP1_SPI_APPS_N_ADDR, m)
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_N_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP1_SPI_APPS_N_ADDR,v)
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP1_SPI_APPS_N_ADDR,m,v,HWIO_GCC_BLSP1_QUP1_SPI_APPS_N_IN)
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_N_NOT_N_MINUS_M_BMSK                                               0xff
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_N_NOT_N_MINUS_M_SHFT                                                0x0

#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_D_ADDR                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x00002034)
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_D_RMSK                                                             0xff
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_D_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP1_SPI_APPS_D_ADDR, HWIO_GCC_BLSP1_QUP1_SPI_APPS_D_RMSK)
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_D_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP1_SPI_APPS_D_ADDR, m)
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_D_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP1_SPI_APPS_D_ADDR,v)
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_D_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP1_SPI_APPS_D_ADDR,m,v,HWIO_GCC_BLSP1_QUP1_SPI_APPS_D_IN)
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_D_NOT_2D_BMSK                                                      0xff
#define HWIO_GCC_BLSP1_QUP1_SPI_APPS_D_NOT_2D_SHFT                                                       0x0

#define HWIO_GCC_BLSP1_UART1_BCR_ADDR                                                             (GCC_CLK_CTL_REG_REG_BASE      + 0x00002038)
#define HWIO_GCC_BLSP1_UART1_BCR_RMSK                                                                    0x1
#define HWIO_GCC_BLSP1_UART1_BCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART1_BCR_ADDR, HWIO_GCC_BLSP1_UART1_BCR_RMSK)
#define HWIO_GCC_BLSP1_UART1_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART1_BCR_ADDR, m)
#define HWIO_GCC_BLSP1_UART1_BCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART1_BCR_ADDR,v)
#define HWIO_GCC_BLSP1_UART1_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART1_BCR_ADDR,m,v,HWIO_GCC_BLSP1_UART1_BCR_IN)
#define HWIO_GCC_BLSP1_UART1_BCR_BLK_ARES_BMSK                                                           0x1
#define HWIO_GCC_BLSP1_UART1_BCR_BLK_ARES_SHFT                                                           0x0

#define HWIO_GCC_BLSP1_UART1_APPS_CBCR_ADDR                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x0000203c)
#define HWIO_GCC_BLSP1_UART1_APPS_CBCR_RMSK                                                       0x80000001
#define HWIO_GCC_BLSP1_UART1_APPS_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART1_APPS_CBCR_ADDR, HWIO_GCC_BLSP1_UART1_APPS_CBCR_RMSK)
#define HWIO_GCC_BLSP1_UART1_APPS_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART1_APPS_CBCR_ADDR, m)
#define HWIO_GCC_BLSP1_UART1_APPS_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART1_APPS_CBCR_ADDR,v)
#define HWIO_GCC_BLSP1_UART1_APPS_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART1_APPS_CBCR_ADDR,m,v,HWIO_GCC_BLSP1_UART1_APPS_CBCR_IN)
#define HWIO_GCC_BLSP1_UART1_APPS_CBCR_CLK_OFF_BMSK                                               0x80000000
#define HWIO_GCC_BLSP1_UART1_APPS_CBCR_CLK_OFF_SHFT                                                     0x1f
#define HWIO_GCC_BLSP1_UART1_APPS_CBCR_CLK_ENABLE_BMSK                                                   0x1
#define HWIO_GCC_BLSP1_UART1_APPS_CBCR_CLK_ENABLE_SHFT                                                   0x0

#define HWIO_GCC_BLSP1_UART1_SIM_CBCR_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x00002040)
#define HWIO_GCC_BLSP1_UART1_SIM_CBCR_RMSK                                                        0x80000001
#define HWIO_GCC_BLSP1_UART1_SIM_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART1_SIM_CBCR_ADDR, HWIO_GCC_BLSP1_UART1_SIM_CBCR_RMSK)
#define HWIO_GCC_BLSP1_UART1_SIM_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART1_SIM_CBCR_ADDR, m)
#define HWIO_GCC_BLSP1_UART1_SIM_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART1_SIM_CBCR_ADDR,v)
#define HWIO_GCC_BLSP1_UART1_SIM_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART1_SIM_CBCR_ADDR,m,v,HWIO_GCC_BLSP1_UART1_SIM_CBCR_IN)
#define HWIO_GCC_BLSP1_UART1_SIM_CBCR_CLK_OFF_BMSK                                                0x80000000
#define HWIO_GCC_BLSP1_UART1_SIM_CBCR_CLK_OFF_SHFT                                                      0x1f
#define HWIO_GCC_BLSP1_UART1_SIM_CBCR_CLK_ENABLE_BMSK                                                    0x1
#define HWIO_GCC_BLSP1_UART1_SIM_CBCR_CLK_ENABLE_SHFT                                                    0x0

#define HWIO_GCC_BLSP1_UART1_APPS_CMD_RCGR_ADDR                                                   (GCC_CLK_CTL_REG_REG_BASE      + 0x00002044)
#define HWIO_GCC_BLSP1_UART1_APPS_CMD_RCGR_RMSK                                                   0x800000f3
#define HWIO_GCC_BLSP1_UART1_APPS_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART1_APPS_CMD_RCGR_ADDR, HWIO_GCC_BLSP1_UART1_APPS_CMD_RCGR_RMSK)
#define HWIO_GCC_BLSP1_UART1_APPS_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART1_APPS_CMD_RCGR_ADDR, m)
#define HWIO_GCC_BLSP1_UART1_APPS_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART1_APPS_CMD_RCGR_ADDR,v)
#define HWIO_GCC_BLSP1_UART1_APPS_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART1_APPS_CMD_RCGR_ADDR,m,v,HWIO_GCC_BLSP1_UART1_APPS_CMD_RCGR_IN)
#define HWIO_GCC_BLSP1_UART1_APPS_CMD_RCGR_ROOT_OFF_BMSK                                          0x80000000
#define HWIO_GCC_BLSP1_UART1_APPS_CMD_RCGR_ROOT_OFF_SHFT                                                0x1f
#define HWIO_GCC_BLSP1_UART1_APPS_CMD_RCGR_DIRTY_D_BMSK                                                 0x80
#define HWIO_GCC_BLSP1_UART1_APPS_CMD_RCGR_DIRTY_D_SHFT                                                  0x7
#define HWIO_GCC_BLSP1_UART1_APPS_CMD_RCGR_DIRTY_M_BMSK                                                 0x40
#define HWIO_GCC_BLSP1_UART1_APPS_CMD_RCGR_DIRTY_M_SHFT                                                  0x6
#define HWIO_GCC_BLSP1_UART1_APPS_CMD_RCGR_DIRTY_N_BMSK                                                 0x20
#define HWIO_GCC_BLSP1_UART1_APPS_CMD_RCGR_DIRTY_N_SHFT                                                  0x5
#define HWIO_GCC_BLSP1_UART1_APPS_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                          0x10
#define HWIO_GCC_BLSP1_UART1_APPS_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                           0x4
#define HWIO_GCC_BLSP1_UART1_APPS_CMD_RCGR_ROOT_EN_BMSK                                                  0x2
#define HWIO_GCC_BLSP1_UART1_APPS_CMD_RCGR_ROOT_EN_SHFT                                                  0x1
#define HWIO_GCC_BLSP1_UART1_APPS_CMD_RCGR_UPDATE_BMSK                                                   0x1
#define HWIO_GCC_BLSP1_UART1_APPS_CMD_RCGR_UPDATE_SHFT                                                   0x0

#define HWIO_GCC_BLSP1_UART1_APPS_CFG_RCGR_ADDR                                                   (GCC_CLK_CTL_REG_REG_BASE      + 0x00002048)
#define HWIO_GCC_BLSP1_UART1_APPS_CFG_RCGR_RMSK                                                       0x371f
#define HWIO_GCC_BLSP1_UART1_APPS_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART1_APPS_CFG_RCGR_ADDR, HWIO_GCC_BLSP1_UART1_APPS_CFG_RCGR_RMSK)
#define HWIO_GCC_BLSP1_UART1_APPS_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART1_APPS_CFG_RCGR_ADDR, m)
#define HWIO_GCC_BLSP1_UART1_APPS_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART1_APPS_CFG_RCGR_ADDR,v)
#define HWIO_GCC_BLSP1_UART1_APPS_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART1_APPS_CFG_RCGR_ADDR,m,v,HWIO_GCC_BLSP1_UART1_APPS_CFG_RCGR_IN)
#define HWIO_GCC_BLSP1_UART1_APPS_CFG_RCGR_MODE_BMSK                                                  0x3000
#define HWIO_GCC_BLSP1_UART1_APPS_CFG_RCGR_MODE_SHFT                                                     0xc
#define HWIO_GCC_BLSP1_UART1_APPS_CFG_RCGR_SRC_SEL_BMSK                                                0x700
#define HWIO_GCC_BLSP1_UART1_APPS_CFG_RCGR_SRC_SEL_SHFT                                                  0x8
#define HWIO_GCC_BLSP1_UART1_APPS_CFG_RCGR_SRC_DIV_BMSK                                                 0x1f
#define HWIO_GCC_BLSP1_UART1_APPS_CFG_RCGR_SRC_DIV_SHFT                                                  0x0

#define HWIO_GCC_BLSP1_UART1_APPS_M_ADDR                                                          (GCC_CLK_CTL_REG_REG_BASE      + 0x0000204c)
#define HWIO_GCC_BLSP1_UART1_APPS_M_RMSK                                                              0xffff
#define HWIO_GCC_BLSP1_UART1_APPS_M_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART1_APPS_M_ADDR, HWIO_GCC_BLSP1_UART1_APPS_M_RMSK)
#define HWIO_GCC_BLSP1_UART1_APPS_M_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART1_APPS_M_ADDR, m)
#define HWIO_GCC_BLSP1_UART1_APPS_M_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART1_APPS_M_ADDR,v)
#define HWIO_GCC_BLSP1_UART1_APPS_M_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART1_APPS_M_ADDR,m,v,HWIO_GCC_BLSP1_UART1_APPS_M_IN)
#define HWIO_GCC_BLSP1_UART1_APPS_M_M_BMSK                                                            0xffff
#define HWIO_GCC_BLSP1_UART1_APPS_M_M_SHFT                                                               0x0

#define HWIO_GCC_BLSP1_UART1_APPS_N_ADDR                                                          (GCC_CLK_CTL_REG_REG_BASE      + 0x00002050)
#define HWIO_GCC_BLSP1_UART1_APPS_N_RMSK                                                              0xffff
#define HWIO_GCC_BLSP1_UART1_APPS_N_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART1_APPS_N_ADDR, HWIO_GCC_BLSP1_UART1_APPS_N_RMSK)
#define HWIO_GCC_BLSP1_UART1_APPS_N_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART1_APPS_N_ADDR, m)
#define HWIO_GCC_BLSP1_UART1_APPS_N_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART1_APPS_N_ADDR,v)
#define HWIO_GCC_BLSP1_UART1_APPS_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART1_APPS_N_ADDR,m,v,HWIO_GCC_BLSP1_UART1_APPS_N_IN)
#define HWIO_GCC_BLSP1_UART1_APPS_N_NOT_N_MINUS_M_BMSK                                                0xffff
#define HWIO_GCC_BLSP1_UART1_APPS_N_NOT_N_MINUS_M_SHFT                                                   0x0

#define HWIO_GCC_BLSP1_UART1_APPS_D_ADDR                                                          (GCC_CLK_CTL_REG_REG_BASE      + 0x00002054)
#define HWIO_GCC_BLSP1_UART1_APPS_D_RMSK                                                              0xffff
#define HWIO_GCC_BLSP1_UART1_APPS_D_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART1_APPS_D_ADDR, HWIO_GCC_BLSP1_UART1_APPS_D_RMSK)
#define HWIO_GCC_BLSP1_UART1_APPS_D_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART1_APPS_D_ADDR, m)
#define HWIO_GCC_BLSP1_UART1_APPS_D_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART1_APPS_D_ADDR,v)
#define HWIO_GCC_BLSP1_UART1_APPS_D_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART1_APPS_D_ADDR,m,v,HWIO_GCC_BLSP1_UART1_APPS_D_IN)
#define HWIO_GCC_BLSP1_UART1_APPS_D_NOT_2D_BMSK                                                       0xffff
#define HWIO_GCC_BLSP1_UART1_APPS_D_NOT_2D_SHFT                                                          0x0

#define HWIO_GCC_BLSP1_QUP2_BCR_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x00003008)
#define HWIO_GCC_BLSP1_QUP2_BCR_RMSK                                                                     0x1
#define HWIO_GCC_BLSP1_QUP2_BCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP2_BCR_ADDR, HWIO_GCC_BLSP1_QUP2_BCR_RMSK)
#define HWIO_GCC_BLSP1_QUP2_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP2_BCR_ADDR, m)
#define HWIO_GCC_BLSP1_QUP2_BCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP2_BCR_ADDR,v)
#define HWIO_GCC_BLSP1_QUP2_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP2_BCR_ADDR,m,v,HWIO_GCC_BLSP1_QUP2_BCR_IN)
#define HWIO_GCC_BLSP1_QUP2_BCR_BLK_ARES_BMSK                                                            0x1
#define HWIO_GCC_BLSP1_QUP2_BCR_BLK_ARES_SHFT                                                            0x0

#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CBCR_ADDR                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x0000300c)
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CBCR_RMSK                                                    0x80000001
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP2_SPI_APPS_CBCR_ADDR, HWIO_GCC_BLSP1_QUP2_SPI_APPS_CBCR_RMSK)
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP2_SPI_APPS_CBCR_ADDR, m)
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP2_SPI_APPS_CBCR_ADDR,v)
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP2_SPI_APPS_CBCR_ADDR,m,v,HWIO_GCC_BLSP1_QUP2_SPI_APPS_CBCR_IN)
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CBCR_CLK_OFF_BMSK                                            0x80000000
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CBCR_CLK_OFF_SHFT                                                  0x1f
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CBCR_CLK_ENABLE_BMSK                                                0x1
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CBCR_CLK_ENABLE_SHFT                                                0x0

#define HWIO_GCC_BLSP1_QUP2_I2C_APPS_CBCR_ADDR                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x00003010)
#define HWIO_GCC_BLSP1_QUP2_I2C_APPS_CBCR_RMSK                                                    0x80000001
#define HWIO_GCC_BLSP1_QUP2_I2C_APPS_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP2_I2C_APPS_CBCR_ADDR, HWIO_GCC_BLSP1_QUP2_I2C_APPS_CBCR_RMSK)
#define HWIO_GCC_BLSP1_QUP2_I2C_APPS_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP2_I2C_APPS_CBCR_ADDR, m)
#define HWIO_GCC_BLSP1_QUP2_I2C_APPS_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP2_I2C_APPS_CBCR_ADDR,v)
#define HWIO_GCC_BLSP1_QUP2_I2C_APPS_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP2_I2C_APPS_CBCR_ADDR,m,v,HWIO_GCC_BLSP1_QUP2_I2C_APPS_CBCR_IN)
#define HWIO_GCC_BLSP1_QUP2_I2C_APPS_CBCR_CLK_OFF_BMSK                                            0x80000000
#define HWIO_GCC_BLSP1_QUP2_I2C_APPS_CBCR_CLK_OFF_SHFT                                                  0x1f
#define HWIO_GCC_BLSP1_QUP2_I2C_APPS_CBCR_CLK_ENABLE_BMSK                                                0x1
#define HWIO_GCC_BLSP1_QUP2_I2C_APPS_CBCR_CLK_ENABLE_SHFT                                                0x0

#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CMD_RCGR_ADDR                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x00003014)
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CMD_RCGR_RMSK                                                0x800000f3
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP2_SPI_APPS_CMD_RCGR_ADDR, HWIO_GCC_BLSP1_QUP2_SPI_APPS_CMD_RCGR_RMSK)
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP2_SPI_APPS_CMD_RCGR_ADDR, m)
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP2_SPI_APPS_CMD_RCGR_ADDR,v)
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP2_SPI_APPS_CMD_RCGR_ADDR,m,v,HWIO_GCC_BLSP1_QUP2_SPI_APPS_CMD_RCGR_IN)
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CMD_RCGR_ROOT_OFF_BMSK                                       0x80000000
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CMD_RCGR_ROOT_OFF_SHFT                                             0x1f
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CMD_RCGR_DIRTY_D_BMSK                                              0x80
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CMD_RCGR_DIRTY_D_SHFT                                               0x7
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CMD_RCGR_DIRTY_M_BMSK                                              0x40
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CMD_RCGR_DIRTY_M_SHFT                                               0x6
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CMD_RCGR_DIRTY_N_BMSK                                              0x20
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CMD_RCGR_DIRTY_N_SHFT                                               0x5
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                       0x10
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                        0x4
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CMD_RCGR_ROOT_EN_BMSK                                               0x2
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CMD_RCGR_ROOT_EN_SHFT                                               0x1
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CMD_RCGR_UPDATE_BMSK                                                0x1
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CMD_RCGR_UPDATE_SHFT                                                0x0

#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CFG_RCGR_ADDR                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x00003018)
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CFG_RCGR_RMSK                                                    0x371f
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP2_SPI_APPS_CFG_RCGR_ADDR, HWIO_GCC_BLSP1_QUP2_SPI_APPS_CFG_RCGR_RMSK)
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP2_SPI_APPS_CFG_RCGR_ADDR, m)
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP2_SPI_APPS_CFG_RCGR_ADDR,v)
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP2_SPI_APPS_CFG_RCGR_ADDR,m,v,HWIO_GCC_BLSP1_QUP2_SPI_APPS_CFG_RCGR_IN)
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CFG_RCGR_MODE_BMSK                                               0x3000
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CFG_RCGR_MODE_SHFT                                                  0xc
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CFG_RCGR_SRC_SEL_BMSK                                             0x700
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CFG_RCGR_SRC_SEL_SHFT                                               0x8
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CFG_RCGR_SRC_DIV_BMSK                                              0x1f
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_CFG_RCGR_SRC_DIV_SHFT                                               0x0

#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_M_ADDR                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x0000301c)
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_M_RMSK                                                             0xff
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_M_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP2_SPI_APPS_M_ADDR, HWIO_GCC_BLSP1_QUP2_SPI_APPS_M_RMSK)
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_M_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP2_SPI_APPS_M_ADDR, m)
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_M_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP2_SPI_APPS_M_ADDR,v)
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_M_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP2_SPI_APPS_M_ADDR,m,v,HWIO_GCC_BLSP1_QUP2_SPI_APPS_M_IN)
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_M_M_BMSK                                                           0xff
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_M_M_SHFT                                                            0x0

#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_N_ADDR                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x00003020)
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_N_RMSK                                                             0xff
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_N_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP2_SPI_APPS_N_ADDR, HWIO_GCC_BLSP1_QUP2_SPI_APPS_N_RMSK)
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_N_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP2_SPI_APPS_N_ADDR, m)
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_N_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP2_SPI_APPS_N_ADDR,v)
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP2_SPI_APPS_N_ADDR,m,v,HWIO_GCC_BLSP1_QUP2_SPI_APPS_N_IN)
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_N_NOT_N_MINUS_M_BMSK                                               0xff
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_N_NOT_N_MINUS_M_SHFT                                                0x0

#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_D_ADDR                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x00003024)
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_D_RMSK                                                             0xff
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_D_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP2_SPI_APPS_D_ADDR, HWIO_GCC_BLSP1_QUP2_SPI_APPS_D_RMSK)
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_D_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP2_SPI_APPS_D_ADDR, m)
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_D_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP2_SPI_APPS_D_ADDR,v)
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_D_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP2_SPI_APPS_D_ADDR,m,v,HWIO_GCC_BLSP1_QUP2_SPI_APPS_D_IN)
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_D_NOT_2D_BMSK                                                      0xff
#define HWIO_GCC_BLSP1_QUP2_SPI_APPS_D_NOT_2D_SHFT                                                       0x0

#define HWIO_GCC_BLSP1_UART2_BCR_ADDR                                                             (GCC_CLK_CTL_REG_REG_BASE      + 0x00003028)
#define HWIO_GCC_BLSP1_UART2_BCR_RMSK                                                                    0x1
#define HWIO_GCC_BLSP1_UART2_BCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART2_BCR_ADDR, HWIO_GCC_BLSP1_UART2_BCR_RMSK)
#define HWIO_GCC_BLSP1_UART2_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART2_BCR_ADDR, m)
#define HWIO_GCC_BLSP1_UART2_BCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART2_BCR_ADDR,v)
#define HWIO_GCC_BLSP1_UART2_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART2_BCR_ADDR,m,v,HWIO_GCC_BLSP1_UART2_BCR_IN)
#define HWIO_GCC_BLSP1_UART2_BCR_BLK_ARES_BMSK                                                           0x1
#define HWIO_GCC_BLSP1_UART2_BCR_BLK_ARES_SHFT                                                           0x0

#define HWIO_GCC_BLSP1_UART2_APPS_CBCR_ADDR                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x0000302c)
#define HWIO_GCC_BLSP1_UART2_APPS_CBCR_RMSK                                                       0x80000001
#define HWIO_GCC_BLSP1_UART2_APPS_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART2_APPS_CBCR_ADDR, HWIO_GCC_BLSP1_UART2_APPS_CBCR_RMSK)
#define HWIO_GCC_BLSP1_UART2_APPS_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART2_APPS_CBCR_ADDR, m)
#define HWIO_GCC_BLSP1_UART2_APPS_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART2_APPS_CBCR_ADDR,v)
#define HWIO_GCC_BLSP1_UART2_APPS_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART2_APPS_CBCR_ADDR,m,v,HWIO_GCC_BLSP1_UART2_APPS_CBCR_IN)
#define HWIO_GCC_BLSP1_UART2_APPS_CBCR_CLK_OFF_BMSK                                               0x80000000
#define HWIO_GCC_BLSP1_UART2_APPS_CBCR_CLK_OFF_SHFT                                                     0x1f
#define HWIO_GCC_BLSP1_UART2_APPS_CBCR_CLK_ENABLE_BMSK                                                   0x1
#define HWIO_GCC_BLSP1_UART2_APPS_CBCR_CLK_ENABLE_SHFT                                                   0x0

#define HWIO_GCC_BLSP1_UART2_SIM_CBCR_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x00003030)
#define HWIO_GCC_BLSP1_UART2_SIM_CBCR_RMSK                                                        0x80000001
#define HWIO_GCC_BLSP1_UART2_SIM_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART2_SIM_CBCR_ADDR, HWIO_GCC_BLSP1_UART2_SIM_CBCR_RMSK)
#define HWIO_GCC_BLSP1_UART2_SIM_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART2_SIM_CBCR_ADDR, m)
#define HWIO_GCC_BLSP1_UART2_SIM_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART2_SIM_CBCR_ADDR,v)
#define HWIO_GCC_BLSP1_UART2_SIM_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART2_SIM_CBCR_ADDR,m,v,HWIO_GCC_BLSP1_UART2_SIM_CBCR_IN)
#define HWIO_GCC_BLSP1_UART2_SIM_CBCR_CLK_OFF_BMSK                                                0x80000000
#define HWIO_GCC_BLSP1_UART2_SIM_CBCR_CLK_OFF_SHFT                                                      0x1f
#define HWIO_GCC_BLSP1_UART2_SIM_CBCR_CLK_ENABLE_BMSK                                                    0x1
#define HWIO_GCC_BLSP1_UART2_SIM_CBCR_CLK_ENABLE_SHFT                                                    0x0

#define HWIO_GCC_BLSP1_UART2_APPS_CMD_RCGR_ADDR                                                   (GCC_CLK_CTL_REG_REG_BASE      + 0x00003034)
#define HWIO_GCC_BLSP1_UART2_APPS_CMD_RCGR_RMSK                                                   0x800000f3
#define HWIO_GCC_BLSP1_UART2_APPS_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART2_APPS_CMD_RCGR_ADDR, HWIO_GCC_BLSP1_UART2_APPS_CMD_RCGR_RMSK)
#define HWIO_GCC_BLSP1_UART2_APPS_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART2_APPS_CMD_RCGR_ADDR, m)
#define HWIO_GCC_BLSP1_UART2_APPS_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART2_APPS_CMD_RCGR_ADDR,v)
#define HWIO_GCC_BLSP1_UART2_APPS_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART2_APPS_CMD_RCGR_ADDR,m,v,HWIO_GCC_BLSP1_UART2_APPS_CMD_RCGR_IN)
#define HWIO_GCC_BLSP1_UART2_APPS_CMD_RCGR_ROOT_OFF_BMSK                                          0x80000000
#define HWIO_GCC_BLSP1_UART2_APPS_CMD_RCGR_ROOT_OFF_SHFT                                                0x1f
#define HWIO_GCC_BLSP1_UART2_APPS_CMD_RCGR_DIRTY_D_BMSK                                                 0x80
#define HWIO_GCC_BLSP1_UART2_APPS_CMD_RCGR_DIRTY_D_SHFT                                                  0x7
#define HWIO_GCC_BLSP1_UART2_APPS_CMD_RCGR_DIRTY_M_BMSK                                                 0x40
#define HWIO_GCC_BLSP1_UART2_APPS_CMD_RCGR_DIRTY_M_SHFT                                                  0x6
#define HWIO_GCC_BLSP1_UART2_APPS_CMD_RCGR_DIRTY_N_BMSK                                                 0x20
#define HWIO_GCC_BLSP1_UART2_APPS_CMD_RCGR_DIRTY_N_SHFT                                                  0x5
#define HWIO_GCC_BLSP1_UART2_APPS_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                          0x10
#define HWIO_GCC_BLSP1_UART2_APPS_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                           0x4
#define HWIO_GCC_BLSP1_UART2_APPS_CMD_RCGR_ROOT_EN_BMSK                                                  0x2
#define HWIO_GCC_BLSP1_UART2_APPS_CMD_RCGR_ROOT_EN_SHFT                                                  0x1
#define HWIO_GCC_BLSP1_UART2_APPS_CMD_RCGR_UPDATE_BMSK                                                   0x1
#define HWIO_GCC_BLSP1_UART2_APPS_CMD_RCGR_UPDATE_SHFT                                                   0x0

#define HWIO_GCC_BLSP1_UART2_APPS_CFG_RCGR_ADDR                                                   (GCC_CLK_CTL_REG_REG_BASE      + 0x00003038)
#define HWIO_GCC_BLSP1_UART2_APPS_CFG_RCGR_RMSK                                                       0x371f
#define HWIO_GCC_BLSP1_UART2_APPS_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART2_APPS_CFG_RCGR_ADDR, HWIO_GCC_BLSP1_UART2_APPS_CFG_RCGR_RMSK)
#define HWIO_GCC_BLSP1_UART2_APPS_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART2_APPS_CFG_RCGR_ADDR, m)
#define HWIO_GCC_BLSP1_UART2_APPS_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART2_APPS_CFG_RCGR_ADDR,v)
#define HWIO_GCC_BLSP1_UART2_APPS_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART2_APPS_CFG_RCGR_ADDR,m,v,HWIO_GCC_BLSP1_UART2_APPS_CFG_RCGR_IN)
#define HWIO_GCC_BLSP1_UART2_APPS_CFG_RCGR_MODE_BMSK                                                  0x3000
#define HWIO_GCC_BLSP1_UART2_APPS_CFG_RCGR_MODE_SHFT                                                     0xc
#define HWIO_GCC_BLSP1_UART2_APPS_CFG_RCGR_SRC_SEL_BMSK                                                0x700
#define HWIO_GCC_BLSP1_UART2_APPS_CFG_RCGR_SRC_SEL_SHFT                                                  0x8
#define HWIO_GCC_BLSP1_UART2_APPS_CFG_RCGR_SRC_DIV_BMSK                                                 0x1f
#define HWIO_GCC_BLSP1_UART2_APPS_CFG_RCGR_SRC_DIV_SHFT                                                  0x0

#define HWIO_GCC_BLSP1_UART2_APPS_M_ADDR                                                          (GCC_CLK_CTL_REG_REG_BASE      + 0x0000303c)
#define HWIO_GCC_BLSP1_UART2_APPS_M_RMSK                                                              0xffff
#define HWIO_GCC_BLSP1_UART2_APPS_M_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART2_APPS_M_ADDR, HWIO_GCC_BLSP1_UART2_APPS_M_RMSK)
#define HWIO_GCC_BLSP1_UART2_APPS_M_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART2_APPS_M_ADDR, m)
#define HWIO_GCC_BLSP1_UART2_APPS_M_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART2_APPS_M_ADDR,v)
#define HWIO_GCC_BLSP1_UART2_APPS_M_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART2_APPS_M_ADDR,m,v,HWIO_GCC_BLSP1_UART2_APPS_M_IN)
#define HWIO_GCC_BLSP1_UART2_APPS_M_M_BMSK                                                            0xffff
#define HWIO_GCC_BLSP1_UART2_APPS_M_M_SHFT                                                               0x0

#define HWIO_GCC_BLSP1_UART2_APPS_N_ADDR                                                          (GCC_CLK_CTL_REG_REG_BASE      + 0x00003040)
#define HWIO_GCC_BLSP1_UART2_APPS_N_RMSK                                                              0xffff
#define HWIO_GCC_BLSP1_UART2_APPS_N_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART2_APPS_N_ADDR, HWIO_GCC_BLSP1_UART2_APPS_N_RMSK)
#define HWIO_GCC_BLSP1_UART2_APPS_N_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART2_APPS_N_ADDR, m)
#define HWIO_GCC_BLSP1_UART2_APPS_N_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART2_APPS_N_ADDR,v)
#define HWIO_GCC_BLSP1_UART2_APPS_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART2_APPS_N_ADDR,m,v,HWIO_GCC_BLSP1_UART2_APPS_N_IN)
#define HWIO_GCC_BLSP1_UART2_APPS_N_NOT_N_MINUS_M_BMSK                                                0xffff
#define HWIO_GCC_BLSP1_UART2_APPS_N_NOT_N_MINUS_M_SHFT                                                   0x0

#define HWIO_GCC_BLSP1_UART2_APPS_D_ADDR                                                          (GCC_CLK_CTL_REG_REG_BASE      + 0x00003044)
#define HWIO_GCC_BLSP1_UART2_APPS_D_RMSK                                                              0xffff
#define HWIO_GCC_BLSP1_UART2_APPS_D_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART2_APPS_D_ADDR, HWIO_GCC_BLSP1_UART2_APPS_D_RMSK)
#define HWIO_GCC_BLSP1_UART2_APPS_D_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART2_APPS_D_ADDR, m)
#define HWIO_GCC_BLSP1_UART2_APPS_D_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART2_APPS_D_ADDR,v)
#define HWIO_GCC_BLSP1_UART2_APPS_D_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART2_APPS_D_ADDR,m,v,HWIO_GCC_BLSP1_UART2_APPS_D_IN)
#define HWIO_GCC_BLSP1_UART2_APPS_D_NOT_2D_BMSK                                                       0xffff
#define HWIO_GCC_BLSP1_UART2_APPS_D_NOT_2D_SHFT                                                          0x0

#define HWIO_GCC_BLSP1_QUP3_BCR_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x00004018)
#define HWIO_GCC_BLSP1_QUP3_BCR_RMSK                                                                     0x1
#define HWIO_GCC_BLSP1_QUP3_BCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP3_BCR_ADDR, HWIO_GCC_BLSP1_QUP3_BCR_RMSK)
#define HWIO_GCC_BLSP1_QUP3_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP3_BCR_ADDR, m)
#define HWIO_GCC_BLSP1_QUP3_BCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP3_BCR_ADDR,v)
#define HWIO_GCC_BLSP1_QUP3_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP3_BCR_ADDR,m,v,HWIO_GCC_BLSP1_QUP3_BCR_IN)
#define HWIO_GCC_BLSP1_QUP3_BCR_BLK_ARES_BMSK                                                            0x1
#define HWIO_GCC_BLSP1_QUP3_BCR_BLK_ARES_SHFT                                                            0x0

#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CBCR_ADDR                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x0000401c)
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CBCR_RMSK                                                    0x80000001
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP3_SPI_APPS_CBCR_ADDR, HWIO_GCC_BLSP1_QUP3_SPI_APPS_CBCR_RMSK)
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP3_SPI_APPS_CBCR_ADDR, m)
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP3_SPI_APPS_CBCR_ADDR,v)
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP3_SPI_APPS_CBCR_ADDR,m,v,HWIO_GCC_BLSP1_QUP3_SPI_APPS_CBCR_IN)
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CBCR_CLK_OFF_BMSK                                            0x80000000
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CBCR_CLK_OFF_SHFT                                                  0x1f
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CBCR_CLK_ENABLE_BMSK                                                0x1
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CBCR_CLK_ENABLE_SHFT                                                0x0

#define HWIO_GCC_BLSP1_QUP3_I2C_APPS_CBCR_ADDR                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x00004020)
#define HWIO_GCC_BLSP1_QUP3_I2C_APPS_CBCR_RMSK                                                    0x80000001
#define HWIO_GCC_BLSP1_QUP3_I2C_APPS_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP3_I2C_APPS_CBCR_ADDR, HWIO_GCC_BLSP1_QUP3_I2C_APPS_CBCR_RMSK)
#define HWIO_GCC_BLSP1_QUP3_I2C_APPS_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP3_I2C_APPS_CBCR_ADDR, m)
#define HWIO_GCC_BLSP1_QUP3_I2C_APPS_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP3_I2C_APPS_CBCR_ADDR,v)
#define HWIO_GCC_BLSP1_QUP3_I2C_APPS_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP3_I2C_APPS_CBCR_ADDR,m,v,HWIO_GCC_BLSP1_QUP3_I2C_APPS_CBCR_IN)
#define HWIO_GCC_BLSP1_QUP3_I2C_APPS_CBCR_CLK_OFF_BMSK                                            0x80000000
#define HWIO_GCC_BLSP1_QUP3_I2C_APPS_CBCR_CLK_OFF_SHFT                                                  0x1f
#define HWIO_GCC_BLSP1_QUP3_I2C_APPS_CBCR_CLK_ENABLE_BMSK                                                0x1
#define HWIO_GCC_BLSP1_QUP3_I2C_APPS_CBCR_CLK_ENABLE_SHFT                                                0x0

#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CMD_RCGR_ADDR                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x00004024)
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CMD_RCGR_RMSK                                                0x800000f3
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP3_SPI_APPS_CMD_RCGR_ADDR, HWIO_GCC_BLSP1_QUP3_SPI_APPS_CMD_RCGR_RMSK)
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP3_SPI_APPS_CMD_RCGR_ADDR, m)
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP3_SPI_APPS_CMD_RCGR_ADDR,v)
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP3_SPI_APPS_CMD_RCGR_ADDR,m,v,HWIO_GCC_BLSP1_QUP3_SPI_APPS_CMD_RCGR_IN)
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CMD_RCGR_ROOT_OFF_BMSK                                       0x80000000
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CMD_RCGR_ROOT_OFF_SHFT                                             0x1f
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CMD_RCGR_DIRTY_D_BMSK                                              0x80
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CMD_RCGR_DIRTY_D_SHFT                                               0x7
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CMD_RCGR_DIRTY_M_BMSK                                              0x40
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CMD_RCGR_DIRTY_M_SHFT                                               0x6
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CMD_RCGR_DIRTY_N_BMSK                                              0x20
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CMD_RCGR_DIRTY_N_SHFT                                               0x5
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                       0x10
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                        0x4
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CMD_RCGR_ROOT_EN_BMSK                                               0x2
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CMD_RCGR_ROOT_EN_SHFT                                               0x1
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CMD_RCGR_UPDATE_BMSK                                                0x1
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CMD_RCGR_UPDATE_SHFT                                                0x0

#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CFG_RCGR_ADDR                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x00004028)
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CFG_RCGR_RMSK                                                    0x371f
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP3_SPI_APPS_CFG_RCGR_ADDR, HWIO_GCC_BLSP1_QUP3_SPI_APPS_CFG_RCGR_RMSK)
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP3_SPI_APPS_CFG_RCGR_ADDR, m)
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP3_SPI_APPS_CFG_RCGR_ADDR,v)
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP3_SPI_APPS_CFG_RCGR_ADDR,m,v,HWIO_GCC_BLSP1_QUP3_SPI_APPS_CFG_RCGR_IN)
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CFG_RCGR_MODE_BMSK                                               0x3000
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CFG_RCGR_MODE_SHFT                                                  0xc
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CFG_RCGR_SRC_SEL_BMSK                                             0x700
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CFG_RCGR_SRC_SEL_SHFT                                               0x8
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CFG_RCGR_SRC_DIV_BMSK                                              0x1f
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_CFG_RCGR_SRC_DIV_SHFT                                               0x0

#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_M_ADDR                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x0000402c)
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_M_RMSK                                                             0xff
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_M_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP3_SPI_APPS_M_ADDR, HWIO_GCC_BLSP1_QUP3_SPI_APPS_M_RMSK)
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_M_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP3_SPI_APPS_M_ADDR, m)
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_M_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP3_SPI_APPS_M_ADDR,v)
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_M_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP3_SPI_APPS_M_ADDR,m,v,HWIO_GCC_BLSP1_QUP3_SPI_APPS_M_IN)
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_M_M_BMSK                                                           0xff
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_M_M_SHFT                                                            0x0

#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_N_ADDR                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x00004030)
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_N_RMSK                                                             0xff
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_N_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP3_SPI_APPS_N_ADDR, HWIO_GCC_BLSP1_QUP3_SPI_APPS_N_RMSK)
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_N_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP3_SPI_APPS_N_ADDR, m)
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_N_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP3_SPI_APPS_N_ADDR,v)
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP3_SPI_APPS_N_ADDR,m,v,HWIO_GCC_BLSP1_QUP3_SPI_APPS_N_IN)
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_N_NOT_N_MINUS_M_BMSK                                               0xff
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_N_NOT_N_MINUS_M_SHFT                                                0x0

#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_D_ADDR                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x00004034)
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_D_RMSK                                                             0xff
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_D_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP3_SPI_APPS_D_ADDR, HWIO_GCC_BLSP1_QUP3_SPI_APPS_D_RMSK)
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_D_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP3_SPI_APPS_D_ADDR, m)
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_D_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP3_SPI_APPS_D_ADDR,v)
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_D_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP3_SPI_APPS_D_ADDR,m,v,HWIO_GCC_BLSP1_QUP3_SPI_APPS_D_IN)
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_D_NOT_2D_BMSK                                                      0xff
#define HWIO_GCC_BLSP1_QUP3_SPI_APPS_D_NOT_2D_SHFT                                                       0x0

#define HWIO_GCC_BLSP1_UART3_BCR_ADDR                                                             (GCC_CLK_CTL_REG_REG_BASE      + 0x00004038)
#define HWIO_GCC_BLSP1_UART3_BCR_RMSK                                                                    0x1
#define HWIO_GCC_BLSP1_UART3_BCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART3_BCR_ADDR, HWIO_GCC_BLSP1_UART3_BCR_RMSK)
#define HWIO_GCC_BLSP1_UART3_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART3_BCR_ADDR, m)
#define HWIO_GCC_BLSP1_UART3_BCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART3_BCR_ADDR,v)
#define HWIO_GCC_BLSP1_UART3_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART3_BCR_ADDR,m,v,HWIO_GCC_BLSP1_UART3_BCR_IN)
#define HWIO_GCC_BLSP1_UART3_BCR_BLK_ARES_BMSK                                                           0x1
#define HWIO_GCC_BLSP1_UART3_BCR_BLK_ARES_SHFT                                                           0x0

#define HWIO_GCC_BLSP1_UART3_APPS_CBCR_ADDR                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x0000403c)
#define HWIO_GCC_BLSP1_UART3_APPS_CBCR_RMSK                                                       0x80000001
#define HWIO_GCC_BLSP1_UART3_APPS_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART3_APPS_CBCR_ADDR, HWIO_GCC_BLSP1_UART3_APPS_CBCR_RMSK)
#define HWIO_GCC_BLSP1_UART3_APPS_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART3_APPS_CBCR_ADDR, m)
#define HWIO_GCC_BLSP1_UART3_APPS_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART3_APPS_CBCR_ADDR,v)
#define HWIO_GCC_BLSP1_UART3_APPS_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART3_APPS_CBCR_ADDR,m,v,HWIO_GCC_BLSP1_UART3_APPS_CBCR_IN)
#define HWIO_GCC_BLSP1_UART3_APPS_CBCR_CLK_OFF_BMSK                                               0x80000000
#define HWIO_GCC_BLSP1_UART3_APPS_CBCR_CLK_OFF_SHFT                                                     0x1f
#define HWIO_GCC_BLSP1_UART3_APPS_CBCR_CLK_ENABLE_BMSK                                                   0x1
#define HWIO_GCC_BLSP1_UART3_APPS_CBCR_CLK_ENABLE_SHFT                                                   0x0

#define HWIO_GCC_BLSP1_UART3_SIM_CBCR_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x00004040)
#define HWIO_GCC_BLSP1_UART3_SIM_CBCR_RMSK                                                        0x80000001
#define HWIO_GCC_BLSP1_UART3_SIM_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART3_SIM_CBCR_ADDR, HWIO_GCC_BLSP1_UART3_SIM_CBCR_RMSK)
#define HWIO_GCC_BLSP1_UART3_SIM_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART3_SIM_CBCR_ADDR, m)
#define HWIO_GCC_BLSP1_UART3_SIM_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART3_SIM_CBCR_ADDR,v)
#define HWIO_GCC_BLSP1_UART3_SIM_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART3_SIM_CBCR_ADDR,m,v,HWIO_GCC_BLSP1_UART3_SIM_CBCR_IN)
#define HWIO_GCC_BLSP1_UART3_SIM_CBCR_CLK_OFF_BMSK                                                0x80000000
#define HWIO_GCC_BLSP1_UART3_SIM_CBCR_CLK_OFF_SHFT                                                      0x1f
#define HWIO_GCC_BLSP1_UART3_SIM_CBCR_CLK_ENABLE_BMSK                                                    0x1
#define HWIO_GCC_BLSP1_UART3_SIM_CBCR_CLK_ENABLE_SHFT                                                    0x0

#define HWIO_GCC_BLSP1_UART3_APPS_CMD_RCGR_ADDR                                                   (GCC_CLK_CTL_REG_REG_BASE      + 0x00004044)
#define HWIO_GCC_BLSP1_UART3_APPS_CMD_RCGR_RMSK                                                   0x800000f3
#define HWIO_GCC_BLSP1_UART3_APPS_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART3_APPS_CMD_RCGR_ADDR, HWIO_GCC_BLSP1_UART3_APPS_CMD_RCGR_RMSK)
#define HWIO_GCC_BLSP1_UART3_APPS_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART3_APPS_CMD_RCGR_ADDR, m)
#define HWIO_GCC_BLSP1_UART3_APPS_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART3_APPS_CMD_RCGR_ADDR,v)
#define HWIO_GCC_BLSP1_UART3_APPS_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART3_APPS_CMD_RCGR_ADDR,m,v,HWIO_GCC_BLSP1_UART3_APPS_CMD_RCGR_IN)
#define HWIO_GCC_BLSP1_UART3_APPS_CMD_RCGR_ROOT_OFF_BMSK                                          0x80000000
#define HWIO_GCC_BLSP1_UART3_APPS_CMD_RCGR_ROOT_OFF_SHFT                                                0x1f
#define HWIO_GCC_BLSP1_UART3_APPS_CMD_RCGR_DIRTY_D_BMSK                                                 0x80
#define HWIO_GCC_BLSP1_UART3_APPS_CMD_RCGR_DIRTY_D_SHFT                                                  0x7
#define HWIO_GCC_BLSP1_UART3_APPS_CMD_RCGR_DIRTY_M_BMSK                                                 0x40
#define HWIO_GCC_BLSP1_UART3_APPS_CMD_RCGR_DIRTY_M_SHFT                                                  0x6
#define HWIO_GCC_BLSP1_UART3_APPS_CMD_RCGR_DIRTY_N_BMSK                                                 0x20
#define HWIO_GCC_BLSP1_UART3_APPS_CMD_RCGR_DIRTY_N_SHFT                                                  0x5
#define HWIO_GCC_BLSP1_UART3_APPS_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                          0x10
#define HWIO_GCC_BLSP1_UART3_APPS_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                           0x4
#define HWIO_GCC_BLSP1_UART3_APPS_CMD_RCGR_ROOT_EN_BMSK                                                  0x2
#define HWIO_GCC_BLSP1_UART3_APPS_CMD_RCGR_ROOT_EN_SHFT                                                  0x1
#define HWIO_GCC_BLSP1_UART3_APPS_CMD_RCGR_UPDATE_BMSK                                                   0x1
#define HWIO_GCC_BLSP1_UART3_APPS_CMD_RCGR_UPDATE_SHFT                                                   0x0

#define HWIO_GCC_BLSP1_UART3_APPS_CFG_RCGR_ADDR                                                   (GCC_CLK_CTL_REG_REG_BASE      + 0x00004048)
#define HWIO_GCC_BLSP1_UART3_APPS_CFG_RCGR_RMSK                                                       0x371f
#define HWIO_GCC_BLSP1_UART3_APPS_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART3_APPS_CFG_RCGR_ADDR, HWIO_GCC_BLSP1_UART3_APPS_CFG_RCGR_RMSK)
#define HWIO_GCC_BLSP1_UART3_APPS_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART3_APPS_CFG_RCGR_ADDR, m)
#define HWIO_GCC_BLSP1_UART3_APPS_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART3_APPS_CFG_RCGR_ADDR,v)
#define HWIO_GCC_BLSP1_UART3_APPS_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART3_APPS_CFG_RCGR_ADDR,m,v,HWIO_GCC_BLSP1_UART3_APPS_CFG_RCGR_IN)
#define HWIO_GCC_BLSP1_UART3_APPS_CFG_RCGR_MODE_BMSK                                                  0x3000
#define HWIO_GCC_BLSP1_UART3_APPS_CFG_RCGR_MODE_SHFT                                                     0xc
#define HWIO_GCC_BLSP1_UART3_APPS_CFG_RCGR_SRC_SEL_BMSK                                                0x700
#define HWIO_GCC_BLSP1_UART3_APPS_CFG_RCGR_SRC_SEL_SHFT                                                  0x8
#define HWIO_GCC_BLSP1_UART3_APPS_CFG_RCGR_SRC_DIV_BMSK                                                 0x1f
#define HWIO_GCC_BLSP1_UART3_APPS_CFG_RCGR_SRC_DIV_SHFT                                                  0x0

#define HWIO_GCC_BLSP1_UART3_APPS_M_ADDR                                                          (GCC_CLK_CTL_REG_REG_BASE      + 0x0000404c)
#define HWIO_GCC_BLSP1_UART3_APPS_M_RMSK                                                              0xffff
#define HWIO_GCC_BLSP1_UART3_APPS_M_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART3_APPS_M_ADDR, HWIO_GCC_BLSP1_UART3_APPS_M_RMSK)
#define HWIO_GCC_BLSP1_UART3_APPS_M_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART3_APPS_M_ADDR, m)
#define HWIO_GCC_BLSP1_UART3_APPS_M_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART3_APPS_M_ADDR,v)
#define HWIO_GCC_BLSP1_UART3_APPS_M_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART3_APPS_M_ADDR,m,v,HWIO_GCC_BLSP1_UART3_APPS_M_IN)
#define HWIO_GCC_BLSP1_UART3_APPS_M_M_BMSK                                                            0xffff
#define HWIO_GCC_BLSP1_UART3_APPS_M_M_SHFT                                                               0x0

#define HWIO_GCC_BLSP1_UART3_APPS_N_ADDR                                                          (GCC_CLK_CTL_REG_REG_BASE      + 0x00004050)
#define HWIO_GCC_BLSP1_UART3_APPS_N_RMSK                                                              0xffff
#define HWIO_GCC_BLSP1_UART3_APPS_N_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART3_APPS_N_ADDR, HWIO_GCC_BLSP1_UART3_APPS_N_RMSK)
#define HWIO_GCC_BLSP1_UART3_APPS_N_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART3_APPS_N_ADDR, m)
#define HWIO_GCC_BLSP1_UART3_APPS_N_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART3_APPS_N_ADDR,v)
#define HWIO_GCC_BLSP1_UART3_APPS_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART3_APPS_N_ADDR,m,v,HWIO_GCC_BLSP1_UART3_APPS_N_IN)
#define HWIO_GCC_BLSP1_UART3_APPS_N_NOT_N_MINUS_M_BMSK                                                0xffff
#define HWIO_GCC_BLSP1_UART3_APPS_N_NOT_N_MINUS_M_SHFT                                                   0x0

#define HWIO_GCC_BLSP1_UART3_APPS_D_ADDR                                                          (GCC_CLK_CTL_REG_REG_BASE      + 0x00004054)
#define HWIO_GCC_BLSP1_UART3_APPS_D_RMSK                                                              0xffff
#define HWIO_GCC_BLSP1_UART3_APPS_D_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART3_APPS_D_ADDR, HWIO_GCC_BLSP1_UART3_APPS_D_RMSK)
#define HWIO_GCC_BLSP1_UART3_APPS_D_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART3_APPS_D_ADDR, m)
#define HWIO_GCC_BLSP1_UART3_APPS_D_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART3_APPS_D_ADDR,v)
#define HWIO_GCC_BLSP1_UART3_APPS_D_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART3_APPS_D_ADDR,m,v,HWIO_GCC_BLSP1_UART3_APPS_D_IN)
#define HWIO_GCC_BLSP1_UART3_APPS_D_NOT_2D_BMSK                                                       0xffff
#define HWIO_GCC_BLSP1_UART3_APPS_D_NOT_2D_SHFT                                                          0x0

#define HWIO_GCC_BLSP1_QUP4_BCR_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x00005018)
#define HWIO_GCC_BLSP1_QUP4_BCR_RMSK                                                                     0x1
#define HWIO_GCC_BLSP1_QUP4_BCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP4_BCR_ADDR, HWIO_GCC_BLSP1_QUP4_BCR_RMSK)
#define HWIO_GCC_BLSP1_QUP4_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP4_BCR_ADDR, m)
#define HWIO_GCC_BLSP1_QUP4_BCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP4_BCR_ADDR,v)
#define HWIO_GCC_BLSP1_QUP4_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP4_BCR_ADDR,m,v,HWIO_GCC_BLSP1_QUP4_BCR_IN)
#define HWIO_GCC_BLSP1_QUP4_BCR_BLK_ARES_BMSK                                                            0x1
#define HWIO_GCC_BLSP1_QUP4_BCR_BLK_ARES_SHFT                                                            0x0

#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CBCR_ADDR                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x0000501c)
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CBCR_RMSK                                                    0x80000001
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP4_SPI_APPS_CBCR_ADDR, HWIO_GCC_BLSP1_QUP4_SPI_APPS_CBCR_RMSK)
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP4_SPI_APPS_CBCR_ADDR, m)
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP4_SPI_APPS_CBCR_ADDR,v)
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP4_SPI_APPS_CBCR_ADDR,m,v,HWIO_GCC_BLSP1_QUP4_SPI_APPS_CBCR_IN)
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CBCR_CLK_OFF_BMSK                                            0x80000000
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CBCR_CLK_OFF_SHFT                                                  0x1f
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CBCR_CLK_ENABLE_BMSK                                                0x1
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CBCR_CLK_ENABLE_SHFT                                                0x0

#define HWIO_GCC_BLSP1_QUP4_I2C_APPS_CBCR_ADDR                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x00005020)
#define HWIO_GCC_BLSP1_QUP4_I2C_APPS_CBCR_RMSK                                                    0x80000001
#define HWIO_GCC_BLSP1_QUP4_I2C_APPS_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP4_I2C_APPS_CBCR_ADDR, HWIO_GCC_BLSP1_QUP4_I2C_APPS_CBCR_RMSK)
#define HWIO_GCC_BLSP1_QUP4_I2C_APPS_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP4_I2C_APPS_CBCR_ADDR, m)
#define HWIO_GCC_BLSP1_QUP4_I2C_APPS_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP4_I2C_APPS_CBCR_ADDR,v)
#define HWIO_GCC_BLSP1_QUP4_I2C_APPS_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP4_I2C_APPS_CBCR_ADDR,m,v,HWIO_GCC_BLSP1_QUP4_I2C_APPS_CBCR_IN)
#define HWIO_GCC_BLSP1_QUP4_I2C_APPS_CBCR_CLK_OFF_BMSK                                            0x80000000
#define HWIO_GCC_BLSP1_QUP4_I2C_APPS_CBCR_CLK_OFF_SHFT                                                  0x1f
#define HWIO_GCC_BLSP1_QUP4_I2C_APPS_CBCR_CLK_ENABLE_BMSK                                                0x1
#define HWIO_GCC_BLSP1_QUP4_I2C_APPS_CBCR_CLK_ENABLE_SHFT                                                0x0

#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CMD_RCGR_ADDR                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x00005024)
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CMD_RCGR_RMSK                                                0x800000f3
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP4_SPI_APPS_CMD_RCGR_ADDR, HWIO_GCC_BLSP1_QUP4_SPI_APPS_CMD_RCGR_RMSK)
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP4_SPI_APPS_CMD_RCGR_ADDR, m)
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP4_SPI_APPS_CMD_RCGR_ADDR,v)
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP4_SPI_APPS_CMD_RCGR_ADDR,m,v,HWIO_GCC_BLSP1_QUP4_SPI_APPS_CMD_RCGR_IN)
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CMD_RCGR_ROOT_OFF_BMSK                                       0x80000000
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CMD_RCGR_ROOT_OFF_SHFT                                             0x1f
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CMD_RCGR_DIRTY_D_BMSK                                              0x80
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CMD_RCGR_DIRTY_D_SHFT                                               0x7
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CMD_RCGR_DIRTY_M_BMSK                                              0x40
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CMD_RCGR_DIRTY_M_SHFT                                               0x6
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CMD_RCGR_DIRTY_N_BMSK                                              0x20
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CMD_RCGR_DIRTY_N_SHFT                                               0x5
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                       0x10
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                        0x4
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CMD_RCGR_ROOT_EN_BMSK                                               0x2
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CMD_RCGR_ROOT_EN_SHFT                                               0x1
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CMD_RCGR_UPDATE_BMSK                                                0x1
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CMD_RCGR_UPDATE_SHFT                                                0x0

#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CFG_RCGR_ADDR                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x00005028)
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CFG_RCGR_RMSK                                                    0x371f
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP4_SPI_APPS_CFG_RCGR_ADDR, HWIO_GCC_BLSP1_QUP4_SPI_APPS_CFG_RCGR_RMSK)
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP4_SPI_APPS_CFG_RCGR_ADDR, m)
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP4_SPI_APPS_CFG_RCGR_ADDR,v)
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP4_SPI_APPS_CFG_RCGR_ADDR,m,v,HWIO_GCC_BLSP1_QUP4_SPI_APPS_CFG_RCGR_IN)
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CFG_RCGR_MODE_BMSK                                               0x3000
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CFG_RCGR_MODE_SHFT                                                  0xc
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CFG_RCGR_SRC_SEL_BMSK                                             0x700
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CFG_RCGR_SRC_SEL_SHFT                                               0x8
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CFG_RCGR_SRC_DIV_BMSK                                              0x1f
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_CFG_RCGR_SRC_DIV_SHFT                                               0x0

#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_M_ADDR                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x0000502c)
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_M_RMSK                                                             0xff
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_M_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP4_SPI_APPS_M_ADDR, HWIO_GCC_BLSP1_QUP4_SPI_APPS_M_RMSK)
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_M_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP4_SPI_APPS_M_ADDR, m)
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_M_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP4_SPI_APPS_M_ADDR,v)
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_M_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP4_SPI_APPS_M_ADDR,m,v,HWIO_GCC_BLSP1_QUP4_SPI_APPS_M_IN)
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_M_M_BMSK                                                           0xff
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_M_M_SHFT                                                            0x0

#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_N_ADDR                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x00005030)
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_N_RMSK                                                             0xff
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_N_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP4_SPI_APPS_N_ADDR, HWIO_GCC_BLSP1_QUP4_SPI_APPS_N_RMSK)
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_N_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP4_SPI_APPS_N_ADDR, m)
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_N_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP4_SPI_APPS_N_ADDR,v)
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP4_SPI_APPS_N_ADDR,m,v,HWIO_GCC_BLSP1_QUP4_SPI_APPS_N_IN)
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_N_NOT_N_MINUS_M_BMSK                                               0xff
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_N_NOT_N_MINUS_M_SHFT                                                0x0

#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_D_ADDR                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x00005034)
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_D_RMSK                                                             0xff
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_D_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_QUP4_SPI_APPS_D_ADDR, HWIO_GCC_BLSP1_QUP4_SPI_APPS_D_RMSK)
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_D_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_QUP4_SPI_APPS_D_ADDR, m)
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_D_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_QUP4_SPI_APPS_D_ADDR,v)
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_D_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_QUP4_SPI_APPS_D_ADDR,m,v,HWIO_GCC_BLSP1_QUP4_SPI_APPS_D_IN)
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_D_NOT_2D_BMSK                                                      0xff
#define HWIO_GCC_BLSP1_QUP4_SPI_APPS_D_NOT_2D_SHFT                                                       0x0

#define HWIO_GCC_BLSP1_UART4_BCR_ADDR                                                             (GCC_CLK_CTL_REG_REG_BASE      + 0x00005038)
#define HWIO_GCC_BLSP1_UART4_BCR_RMSK                                                                    0x1
#define HWIO_GCC_BLSP1_UART4_BCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART4_BCR_ADDR, HWIO_GCC_BLSP1_UART4_BCR_RMSK)
#define HWIO_GCC_BLSP1_UART4_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART4_BCR_ADDR, m)
#define HWIO_GCC_BLSP1_UART4_BCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART4_BCR_ADDR,v)
#define HWIO_GCC_BLSP1_UART4_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART4_BCR_ADDR,m,v,HWIO_GCC_BLSP1_UART4_BCR_IN)
#define HWIO_GCC_BLSP1_UART4_BCR_BLK_ARES_BMSK                                                           0x1
#define HWIO_GCC_BLSP1_UART4_BCR_BLK_ARES_SHFT                                                           0x0

#define HWIO_GCC_BLSP1_UART4_APPS_CBCR_ADDR                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x0000503c)
#define HWIO_GCC_BLSP1_UART4_APPS_CBCR_RMSK                                                       0x80000001
#define HWIO_GCC_BLSP1_UART4_APPS_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART4_APPS_CBCR_ADDR, HWIO_GCC_BLSP1_UART4_APPS_CBCR_RMSK)
#define HWIO_GCC_BLSP1_UART4_APPS_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART4_APPS_CBCR_ADDR, m)
#define HWIO_GCC_BLSP1_UART4_APPS_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART4_APPS_CBCR_ADDR,v)
#define HWIO_GCC_BLSP1_UART4_APPS_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART4_APPS_CBCR_ADDR,m,v,HWIO_GCC_BLSP1_UART4_APPS_CBCR_IN)
#define HWIO_GCC_BLSP1_UART4_APPS_CBCR_CLK_OFF_BMSK                                               0x80000000
#define HWIO_GCC_BLSP1_UART4_APPS_CBCR_CLK_OFF_SHFT                                                     0x1f
#define HWIO_GCC_BLSP1_UART4_APPS_CBCR_CLK_ENABLE_BMSK                                                   0x1
#define HWIO_GCC_BLSP1_UART4_APPS_CBCR_CLK_ENABLE_SHFT                                                   0x0

#define HWIO_GCC_BLSP1_UART4_SIM_CBCR_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x00005040)
#define HWIO_GCC_BLSP1_UART4_SIM_CBCR_RMSK                                                        0x80000001
#define HWIO_GCC_BLSP1_UART4_SIM_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART4_SIM_CBCR_ADDR, HWIO_GCC_BLSP1_UART4_SIM_CBCR_RMSK)
#define HWIO_GCC_BLSP1_UART4_SIM_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART4_SIM_CBCR_ADDR, m)
#define HWIO_GCC_BLSP1_UART4_SIM_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART4_SIM_CBCR_ADDR,v)
#define HWIO_GCC_BLSP1_UART4_SIM_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART4_SIM_CBCR_ADDR,m,v,HWIO_GCC_BLSP1_UART4_SIM_CBCR_IN)
#define HWIO_GCC_BLSP1_UART4_SIM_CBCR_CLK_OFF_BMSK                                                0x80000000
#define HWIO_GCC_BLSP1_UART4_SIM_CBCR_CLK_OFF_SHFT                                                      0x1f
#define HWIO_GCC_BLSP1_UART4_SIM_CBCR_CLK_ENABLE_BMSK                                                    0x1
#define HWIO_GCC_BLSP1_UART4_SIM_CBCR_CLK_ENABLE_SHFT                                                    0x0

#define HWIO_GCC_BLSP1_UART4_APPS_CMD_RCGR_ADDR                                                   (GCC_CLK_CTL_REG_REG_BASE      + 0x00005044)
#define HWIO_GCC_BLSP1_UART4_APPS_CMD_RCGR_RMSK                                                   0x800000f3
#define HWIO_GCC_BLSP1_UART4_APPS_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART4_APPS_CMD_RCGR_ADDR, HWIO_GCC_BLSP1_UART4_APPS_CMD_RCGR_RMSK)
#define HWIO_GCC_BLSP1_UART4_APPS_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART4_APPS_CMD_RCGR_ADDR, m)
#define HWIO_GCC_BLSP1_UART4_APPS_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART4_APPS_CMD_RCGR_ADDR,v)
#define HWIO_GCC_BLSP1_UART4_APPS_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART4_APPS_CMD_RCGR_ADDR,m,v,HWIO_GCC_BLSP1_UART4_APPS_CMD_RCGR_IN)
#define HWIO_GCC_BLSP1_UART4_APPS_CMD_RCGR_ROOT_OFF_BMSK                                          0x80000000
#define HWIO_GCC_BLSP1_UART4_APPS_CMD_RCGR_ROOT_OFF_SHFT                                                0x1f
#define HWIO_GCC_BLSP1_UART4_APPS_CMD_RCGR_DIRTY_D_BMSK                                                 0x80
#define HWIO_GCC_BLSP1_UART4_APPS_CMD_RCGR_DIRTY_D_SHFT                                                  0x7
#define HWIO_GCC_BLSP1_UART4_APPS_CMD_RCGR_DIRTY_M_BMSK                                                 0x40
#define HWIO_GCC_BLSP1_UART4_APPS_CMD_RCGR_DIRTY_M_SHFT                                                  0x6
#define HWIO_GCC_BLSP1_UART4_APPS_CMD_RCGR_DIRTY_N_BMSK                                                 0x20
#define HWIO_GCC_BLSP1_UART4_APPS_CMD_RCGR_DIRTY_N_SHFT                                                  0x5
#define HWIO_GCC_BLSP1_UART4_APPS_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                          0x10
#define HWIO_GCC_BLSP1_UART4_APPS_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                           0x4
#define HWIO_GCC_BLSP1_UART4_APPS_CMD_RCGR_ROOT_EN_BMSK                                                  0x2
#define HWIO_GCC_BLSP1_UART4_APPS_CMD_RCGR_ROOT_EN_SHFT                                                  0x1
#define HWIO_GCC_BLSP1_UART4_APPS_CMD_RCGR_UPDATE_BMSK                                                   0x1
#define HWIO_GCC_BLSP1_UART4_APPS_CMD_RCGR_UPDATE_SHFT                                                   0x0

#define HWIO_GCC_BLSP1_UART4_APPS_CFG_RCGR_ADDR                                                   (GCC_CLK_CTL_REG_REG_BASE      + 0x00005048)
#define HWIO_GCC_BLSP1_UART4_APPS_CFG_RCGR_RMSK                                                       0x371f
#define HWIO_GCC_BLSP1_UART4_APPS_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART4_APPS_CFG_RCGR_ADDR, HWIO_GCC_BLSP1_UART4_APPS_CFG_RCGR_RMSK)
#define HWIO_GCC_BLSP1_UART4_APPS_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART4_APPS_CFG_RCGR_ADDR, m)
#define HWIO_GCC_BLSP1_UART4_APPS_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART4_APPS_CFG_RCGR_ADDR,v)
#define HWIO_GCC_BLSP1_UART4_APPS_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART4_APPS_CFG_RCGR_ADDR,m,v,HWIO_GCC_BLSP1_UART4_APPS_CFG_RCGR_IN)
#define HWIO_GCC_BLSP1_UART4_APPS_CFG_RCGR_MODE_BMSK                                                  0x3000
#define HWIO_GCC_BLSP1_UART4_APPS_CFG_RCGR_MODE_SHFT                                                     0xc
#define HWIO_GCC_BLSP1_UART4_APPS_CFG_RCGR_SRC_SEL_BMSK                                                0x700
#define HWIO_GCC_BLSP1_UART4_APPS_CFG_RCGR_SRC_SEL_SHFT                                                  0x8
#define HWIO_GCC_BLSP1_UART4_APPS_CFG_RCGR_SRC_DIV_BMSK                                                 0x1f
#define HWIO_GCC_BLSP1_UART4_APPS_CFG_RCGR_SRC_DIV_SHFT                                                  0x0

#define HWIO_GCC_BLSP1_UART4_APPS_M_ADDR                                                          (GCC_CLK_CTL_REG_REG_BASE      + 0x0000504c)
#define HWIO_GCC_BLSP1_UART4_APPS_M_RMSK                                                              0xffff
#define HWIO_GCC_BLSP1_UART4_APPS_M_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART4_APPS_M_ADDR, HWIO_GCC_BLSP1_UART4_APPS_M_RMSK)
#define HWIO_GCC_BLSP1_UART4_APPS_M_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART4_APPS_M_ADDR, m)
#define HWIO_GCC_BLSP1_UART4_APPS_M_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART4_APPS_M_ADDR,v)
#define HWIO_GCC_BLSP1_UART4_APPS_M_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART4_APPS_M_ADDR,m,v,HWIO_GCC_BLSP1_UART4_APPS_M_IN)
#define HWIO_GCC_BLSP1_UART4_APPS_M_M_BMSK                                                            0xffff
#define HWIO_GCC_BLSP1_UART4_APPS_M_M_SHFT                                                               0x0

#define HWIO_GCC_BLSP1_UART4_APPS_N_ADDR                                                          (GCC_CLK_CTL_REG_REG_BASE      + 0x00005050)
#define HWIO_GCC_BLSP1_UART4_APPS_N_RMSK                                                              0xffff
#define HWIO_GCC_BLSP1_UART4_APPS_N_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART4_APPS_N_ADDR, HWIO_GCC_BLSP1_UART4_APPS_N_RMSK)
#define HWIO_GCC_BLSP1_UART4_APPS_N_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART4_APPS_N_ADDR, m)
#define HWIO_GCC_BLSP1_UART4_APPS_N_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART4_APPS_N_ADDR,v)
#define HWIO_GCC_BLSP1_UART4_APPS_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART4_APPS_N_ADDR,m,v,HWIO_GCC_BLSP1_UART4_APPS_N_IN)
#define HWIO_GCC_BLSP1_UART4_APPS_N_NOT_N_MINUS_M_BMSK                                                0xffff
#define HWIO_GCC_BLSP1_UART4_APPS_N_NOT_N_MINUS_M_SHFT                                                   0x0

#define HWIO_GCC_BLSP1_UART4_APPS_D_ADDR                                                          (GCC_CLK_CTL_REG_REG_BASE      + 0x00005054)
#define HWIO_GCC_BLSP1_UART4_APPS_D_RMSK                                                              0xffff
#define HWIO_GCC_BLSP1_UART4_APPS_D_IN          \
        in_dword_masked(HWIO_GCC_BLSP1_UART4_APPS_D_ADDR, HWIO_GCC_BLSP1_UART4_APPS_D_RMSK)
#define HWIO_GCC_BLSP1_UART4_APPS_D_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP1_UART4_APPS_D_ADDR, m)
#define HWIO_GCC_BLSP1_UART4_APPS_D_OUT(v)      \
        out_dword(HWIO_GCC_BLSP1_UART4_APPS_D_ADDR,v)
#define HWIO_GCC_BLSP1_UART4_APPS_D_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP1_UART4_APPS_D_ADDR,m,v,HWIO_GCC_BLSP1_UART4_APPS_D_IN)
#define HWIO_GCC_BLSP1_UART4_APPS_D_NOT_2D_BMSK                                                       0xffff
#define HWIO_GCC_BLSP1_UART4_APPS_D_NOT_2D_SHFT                                                          0x0

#define HWIO_GCC_BLSP_UART_SIM_CMD_RCGR_ADDR                                                      (GCC_CLK_CTL_REG_REG_BASE      + 0x0000100c)
#define HWIO_GCC_BLSP_UART_SIM_CMD_RCGR_RMSK                                                      0x80000013
#define HWIO_GCC_BLSP_UART_SIM_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_BLSP_UART_SIM_CMD_RCGR_ADDR, HWIO_GCC_BLSP_UART_SIM_CMD_RCGR_RMSK)
#define HWIO_GCC_BLSP_UART_SIM_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP_UART_SIM_CMD_RCGR_ADDR, m)
#define HWIO_GCC_BLSP_UART_SIM_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP_UART_SIM_CMD_RCGR_ADDR,v)
#define HWIO_GCC_BLSP_UART_SIM_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP_UART_SIM_CMD_RCGR_ADDR,m,v,HWIO_GCC_BLSP_UART_SIM_CMD_RCGR_IN)
#define HWIO_GCC_BLSP_UART_SIM_CMD_RCGR_ROOT_OFF_BMSK                                             0x80000000
#define HWIO_GCC_BLSP_UART_SIM_CMD_RCGR_ROOT_OFF_SHFT                                                   0x1f
#define HWIO_GCC_BLSP_UART_SIM_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                             0x10
#define HWIO_GCC_BLSP_UART_SIM_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                              0x4
#define HWIO_GCC_BLSP_UART_SIM_CMD_RCGR_ROOT_EN_BMSK                                                     0x2
#define HWIO_GCC_BLSP_UART_SIM_CMD_RCGR_ROOT_EN_SHFT                                                     0x1
#define HWIO_GCC_BLSP_UART_SIM_CMD_RCGR_UPDATE_BMSK                                                      0x1
#define HWIO_GCC_BLSP_UART_SIM_CMD_RCGR_UPDATE_SHFT                                                      0x0

#define HWIO_GCC_BLSP_UART_SIM_CFG_RCGR_ADDR                                                      (GCC_CLK_CTL_REG_REG_BASE      + 0x00001010)
#define HWIO_GCC_BLSP_UART_SIM_CFG_RCGR_RMSK                                                            0x1f
#define HWIO_GCC_BLSP_UART_SIM_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_BLSP_UART_SIM_CFG_RCGR_ADDR, HWIO_GCC_BLSP_UART_SIM_CFG_RCGR_RMSK)
#define HWIO_GCC_BLSP_UART_SIM_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_BLSP_UART_SIM_CFG_RCGR_ADDR, m)
#define HWIO_GCC_BLSP_UART_SIM_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_BLSP_UART_SIM_CFG_RCGR_ADDR,v)
#define HWIO_GCC_BLSP_UART_SIM_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BLSP_UART_SIM_CFG_RCGR_ADDR,m,v,HWIO_GCC_BLSP_UART_SIM_CFG_RCGR_IN)
#define HWIO_GCC_BLSP_UART_SIM_CFG_RCGR_SRC_DIV_BMSK                                                    0x1f
#define HWIO_GCC_BLSP_UART_SIM_CFG_RCGR_SRC_DIV_SHFT                                                     0x0

#define HWIO_GCC_PRNG_XPU_CFG_AHB_CBCR_ADDR                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x00017008)
#define HWIO_GCC_PRNG_XPU_CFG_AHB_CBCR_RMSK                                                       0xf0008001
#define HWIO_GCC_PRNG_XPU_CFG_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_PRNG_XPU_CFG_AHB_CBCR_ADDR, HWIO_GCC_PRNG_XPU_CFG_AHB_CBCR_RMSK)
#define HWIO_GCC_PRNG_XPU_CFG_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PRNG_XPU_CFG_AHB_CBCR_ADDR, m)
#define HWIO_GCC_PRNG_XPU_CFG_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_PRNG_XPU_CFG_AHB_CBCR_ADDR,v)
#define HWIO_GCC_PRNG_XPU_CFG_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PRNG_XPU_CFG_AHB_CBCR_ADDR,m,v,HWIO_GCC_PRNG_XPU_CFG_AHB_CBCR_IN)
#define HWIO_GCC_PRNG_XPU_CFG_AHB_CBCR_CLK_OFF_BMSK                                               0x80000000
#define HWIO_GCC_PRNG_XPU_CFG_AHB_CBCR_CLK_OFF_SHFT                                                     0x1f
#define HWIO_GCC_PRNG_XPU_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                              0x70000000
#define HWIO_GCC_PRNG_XPU_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                    0x1c
#define HWIO_GCC_PRNG_XPU_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                      0x8000
#define HWIO_GCC_PRNG_XPU_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                         0xf
#define HWIO_GCC_PRNG_XPU_CFG_AHB_CBCR_CLK_ENABLE_BMSK                                                   0x1
#define HWIO_GCC_PRNG_XPU_CFG_AHB_CBCR_CLK_ENABLE_SHFT                                                   0x0

#define HWIO_GCC_PDM_BCR_ADDR                                                                     (GCC_CLK_CTL_REG_REG_BASE      + 0x00044000)
#define HWIO_GCC_PDM_BCR_RMSK                                                                            0x1
#define HWIO_GCC_PDM_BCR_IN          \
        in_dword_masked(HWIO_GCC_PDM_BCR_ADDR, HWIO_GCC_PDM_BCR_RMSK)
#define HWIO_GCC_PDM_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PDM_BCR_ADDR, m)
#define HWIO_GCC_PDM_BCR_OUT(v)      \
        out_dword(HWIO_GCC_PDM_BCR_ADDR,v)
#define HWIO_GCC_PDM_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PDM_BCR_ADDR,m,v,HWIO_GCC_PDM_BCR_IN)
#define HWIO_GCC_PDM_BCR_BLK_ARES_BMSK                                                                   0x1
#define HWIO_GCC_PDM_BCR_BLK_ARES_SHFT                                                                   0x0

#define HWIO_GCC_PDM_AHB_CBCR_ADDR                                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x00044004)
#define HWIO_GCC_PDM_AHB_CBCR_RMSK                                                                0xf0008001
#define HWIO_GCC_PDM_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_PDM_AHB_CBCR_ADDR, HWIO_GCC_PDM_AHB_CBCR_RMSK)
#define HWIO_GCC_PDM_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PDM_AHB_CBCR_ADDR, m)
#define HWIO_GCC_PDM_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_PDM_AHB_CBCR_ADDR,v)
#define HWIO_GCC_PDM_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PDM_AHB_CBCR_ADDR,m,v,HWIO_GCC_PDM_AHB_CBCR_IN)
#define HWIO_GCC_PDM_AHB_CBCR_CLK_OFF_BMSK                                                        0x80000000
#define HWIO_GCC_PDM_AHB_CBCR_CLK_OFF_SHFT                                                              0x1f
#define HWIO_GCC_PDM_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                       0x70000000
#define HWIO_GCC_PDM_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                             0x1c
#define HWIO_GCC_PDM_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                               0x8000
#define HWIO_GCC_PDM_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                                  0xf
#define HWIO_GCC_PDM_AHB_CBCR_CLK_ENABLE_BMSK                                                            0x1
#define HWIO_GCC_PDM_AHB_CBCR_CLK_ENABLE_SHFT                                                            0x0

#define HWIO_GCC_PDM_XO4_CBCR_ADDR                                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x00044008)
#define HWIO_GCC_PDM_XO4_CBCR_RMSK                                                                0x80030001
#define HWIO_GCC_PDM_XO4_CBCR_IN          \
        in_dword_masked(HWIO_GCC_PDM_XO4_CBCR_ADDR, HWIO_GCC_PDM_XO4_CBCR_RMSK)
#define HWIO_GCC_PDM_XO4_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PDM_XO4_CBCR_ADDR, m)
#define HWIO_GCC_PDM_XO4_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_PDM_XO4_CBCR_ADDR,v)
#define HWIO_GCC_PDM_XO4_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PDM_XO4_CBCR_ADDR,m,v,HWIO_GCC_PDM_XO4_CBCR_IN)
#define HWIO_GCC_PDM_XO4_CBCR_CLK_OFF_BMSK                                                        0x80000000
#define HWIO_GCC_PDM_XO4_CBCR_CLK_OFF_SHFT                                                              0x1f
#define HWIO_GCC_PDM_XO4_CBCR_CLK_DIV_BMSK                                                           0x30000
#define HWIO_GCC_PDM_XO4_CBCR_CLK_DIV_SHFT                                                              0x10
#define HWIO_GCC_PDM_XO4_CBCR_CLK_ENABLE_BMSK                                                            0x1
#define HWIO_GCC_PDM_XO4_CBCR_CLK_ENABLE_SHFT                                                            0x0

#define HWIO_GCC_PDM2_CBCR_ADDR                                                                   (GCC_CLK_CTL_REG_REG_BASE      + 0x0004400c)
#define HWIO_GCC_PDM2_CBCR_RMSK                                                                   0x80000001
#define HWIO_GCC_PDM2_CBCR_IN          \
        in_dword_masked(HWIO_GCC_PDM2_CBCR_ADDR, HWIO_GCC_PDM2_CBCR_RMSK)
#define HWIO_GCC_PDM2_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PDM2_CBCR_ADDR, m)
#define HWIO_GCC_PDM2_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_PDM2_CBCR_ADDR,v)
#define HWIO_GCC_PDM2_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PDM2_CBCR_ADDR,m,v,HWIO_GCC_PDM2_CBCR_IN)
#define HWIO_GCC_PDM2_CBCR_CLK_OFF_BMSK                                                           0x80000000
#define HWIO_GCC_PDM2_CBCR_CLK_OFF_SHFT                                                                 0x1f
#define HWIO_GCC_PDM2_CBCR_CLK_ENABLE_BMSK                                                               0x1
#define HWIO_GCC_PDM2_CBCR_CLK_ENABLE_SHFT                                                               0x0

#define HWIO_GCC_PDM2_CMD_RCGR_ADDR                                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x00044010)
#define HWIO_GCC_PDM2_CMD_RCGR_RMSK                                                               0x80000013
#define HWIO_GCC_PDM2_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_PDM2_CMD_RCGR_ADDR, HWIO_GCC_PDM2_CMD_RCGR_RMSK)
#define HWIO_GCC_PDM2_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_PDM2_CMD_RCGR_ADDR, m)
#define HWIO_GCC_PDM2_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_PDM2_CMD_RCGR_ADDR,v)
#define HWIO_GCC_PDM2_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PDM2_CMD_RCGR_ADDR,m,v,HWIO_GCC_PDM2_CMD_RCGR_IN)
#define HWIO_GCC_PDM2_CMD_RCGR_ROOT_OFF_BMSK                                                      0x80000000
#define HWIO_GCC_PDM2_CMD_RCGR_ROOT_OFF_SHFT                                                            0x1f
#define HWIO_GCC_PDM2_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                                      0x10
#define HWIO_GCC_PDM2_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                                       0x4
#define HWIO_GCC_PDM2_CMD_RCGR_ROOT_EN_BMSK                                                              0x2
#define HWIO_GCC_PDM2_CMD_RCGR_ROOT_EN_SHFT                                                              0x1
#define HWIO_GCC_PDM2_CMD_RCGR_UPDATE_BMSK                                                               0x1
#define HWIO_GCC_PDM2_CMD_RCGR_UPDATE_SHFT                                                               0x0

#define HWIO_GCC_PDM2_CFG_RCGR_ADDR                                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x00044014)
#define HWIO_GCC_PDM2_CFG_RCGR_RMSK                                                                    0x71f
#define HWIO_GCC_PDM2_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_PDM2_CFG_RCGR_ADDR, HWIO_GCC_PDM2_CFG_RCGR_RMSK)
#define HWIO_GCC_PDM2_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_PDM2_CFG_RCGR_ADDR, m)
#define HWIO_GCC_PDM2_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_PDM2_CFG_RCGR_ADDR,v)
#define HWIO_GCC_PDM2_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PDM2_CFG_RCGR_ADDR,m,v,HWIO_GCC_PDM2_CFG_RCGR_IN)
#define HWIO_GCC_PDM2_CFG_RCGR_SRC_SEL_BMSK                                                            0x700
#define HWIO_GCC_PDM2_CFG_RCGR_SRC_SEL_SHFT                                                              0x8
#define HWIO_GCC_PDM2_CFG_RCGR_SRC_DIV_BMSK                                                             0x1f
#define HWIO_GCC_PDM2_CFG_RCGR_SRC_DIV_SHFT                                                              0x0

#define HWIO_GCC_PRNG_BCR_ADDR                                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x00013000)
#define HWIO_GCC_PRNG_BCR_RMSK                                                                           0x1
#define HWIO_GCC_PRNG_BCR_IN          \
        in_dword_masked(HWIO_GCC_PRNG_BCR_ADDR, HWIO_GCC_PRNG_BCR_RMSK)
#define HWIO_GCC_PRNG_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PRNG_BCR_ADDR, m)
#define HWIO_GCC_PRNG_BCR_OUT(v)      \
        out_dword(HWIO_GCC_PRNG_BCR_ADDR,v)
#define HWIO_GCC_PRNG_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PRNG_BCR_ADDR,m,v,HWIO_GCC_PRNG_BCR_IN)
#define HWIO_GCC_PRNG_BCR_BLK_ARES_BMSK                                                                  0x1
#define HWIO_GCC_PRNG_BCR_BLK_ARES_SHFT                                                                  0x0

#define HWIO_GCC_PRNG_AHB_CBCR_ADDR                                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x00013004)
#define HWIO_GCC_PRNG_AHB_CBCR_RMSK                                                               0xf0008000
#define HWIO_GCC_PRNG_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_PRNG_AHB_CBCR_ADDR, HWIO_GCC_PRNG_AHB_CBCR_RMSK)
#define HWIO_GCC_PRNG_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PRNG_AHB_CBCR_ADDR, m)
#define HWIO_GCC_PRNG_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_PRNG_AHB_CBCR_ADDR,v)
#define HWIO_GCC_PRNG_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PRNG_AHB_CBCR_ADDR,m,v,HWIO_GCC_PRNG_AHB_CBCR_IN)
#define HWIO_GCC_PRNG_AHB_CBCR_CLK_OFF_BMSK                                                       0x80000000
#define HWIO_GCC_PRNG_AHB_CBCR_CLK_OFF_SHFT                                                             0x1f
#define HWIO_GCC_PRNG_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                      0x70000000
#define HWIO_GCC_PRNG_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                            0x1c
#define HWIO_GCC_PRNG_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                              0x8000
#define HWIO_GCC_PRNG_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                                 0xf

#define HWIO_GCC_TCSR_BCR_ADDR                                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x00028000)
#define HWIO_GCC_TCSR_BCR_RMSK                                                                           0x1
#define HWIO_GCC_TCSR_BCR_IN          \
        in_dword_masked(HWIO_GCC_TCSR_BCR_ADDR, HWIO_GCC_TCSR_BCR_RMSK)
#define HWIO_GCC_TCSR_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_TCSR_BCR_ADDR, m)
#define HWIO_GCC_TCSR_BCR_OUT(v)      \
        out_dword(HWIO_GCC_TCSR_BCR_ADDR,v)
#define HWIO_GCC_TCSR_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_TCSR_BCR_ADDR,m,v,HWIO_GCC_TCSR_BCR_IN)
#define HWIO_GCC_TCSR_BCR_BLK_ARES_BMSK                                                                  0x1
#define HWIO_GCC_TCSR_BCR_BLK_ARES_SHFT                                                                  0x0

#define HWIO_GCC_TCSR_AHB_CBCR_ADDR                                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x00028004)
#define HWIO_GCC_TCSR_AHB_CBCR_RMSK                                                               0xf0008001
#define HWIO_GCC_TCSR_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_TCSR_AHB_CBCR_ADDR, HWIO_GCC_TCSR_AHB_CBCR_RMSK)
#define HWIO_GCC_TCSR_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_TCSR_AHB_CBCR_ADDR, m)
#define HWIO_GCC_TCSR_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_TCSR_AHB_CBCR_ADDR,v)
#define HWIO_GCC_TCSR_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_TCSR_AHB_CBCR_ADDR,m,v,HWIO_GCC_TCSR_AHB_CBCR_IN)
#define HWIO_GCC_TCSR_AHB_CBCR_CLK_OFF_BMSK                                                       0x80000000
#define HWIO_GCC_TCSR_AHB_CBCR_CLK_OFF_SHFT                                                             0x1f
#define HWIO_GCC_TCSR_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                      0x70000000
#define HWIO_GCC_TCSR_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                            0x1c
#define HWIO_GCC_TCSR_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                              0x8000
#define HWIO_GCC_TCSR_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                                 0xf
#define HWIO_GCC_TCSR_AHB_CBCR_CLK_ENABLE_BMSK                                                           0x1
#define HWIO_GCC_TCSR_AHB_CBCR_CLK_ENABLE_SHFT                                                           0x0

#define HWIO_GCC_BOOT_ROM_BCR_ADDR                                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x00013008)
#define HWIO_GCC_BOOT_ROM_BCR_RMSK                                                                       0x1
#define HWIO_GCC_BOOT_ROM_BCR_IN          \
        in_dword_masked(HWIO_GCC_BOOT_ROM_BCR_ADDR, HWIO_GCC_BOOT_ROM_BCR_RMSK)
#define HWIO_GCC_BOOT_ROM_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BOOT_ROM_BCR_ADDR, m)
#define HWIO_GCC_BOOT_ROM_BCR_OUT(v)      \
        out_dword(HWIO_GCC_BOOT_ROM_BCR_ADDR,v)
#define HWIO_GCC_BOOT_ROM_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BOOT_ROM_BCR_ADDR,m,v,HWIO_GCC_BOOT_ROM_BCR_IN)
#define HWIO_GCC_BOOT_ROM_BCR_BLK_ARES_BMSK                                                              0x1
#define HWIO_GCC_BOOT_ROM_BCR_BLK_ARES_SHFT                                                              0x0

#define HWIO_GCC_BOOT_ROM_AHB_CBCR_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x0001300c)
#define HWIO_GCC_BOOT_ROM_AHB_CBCR_RMSK                                                           0xf000fff0
#define HWIO_GCC_BOOT_ROM_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BOOT_ROM_AHB_CBCR_ADDR, HWIO_GCC_BOOT_ROM_AHB_CBCR_RMSK)
#define HWIO_GCC_BOOT_ROM_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BOOT_ROM_AHB_CBCR_ADDR, m)
#define HWIO_GCC_BOOT_ROM_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_BOOT_ROM_AHB_CBCR_ADDR,v)
#define HWIO_GCC_BOOT_ROM_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BOOT_ROM_AHB_CBCR_ADDR,m,v,HWIO_GCC_BOOT_ROM_AHB_CBCR_IN)
#define HWIO_GCC_BOOT_ROM_AHB_CBCR_CLK_OFF_BMSK                                                   0x80000000
#define HWIO_GCC_BOOT_ROM_AHB_CBCR_CLK_OFF_SHFT                                                         0x1f
#define HWIO_GCC_BOOT_ROM_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                  0x70000000
#define HWIO_GCC_BOOT_ROM_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                        0x1c
#define HWIO_GCC_BOOT_ROM_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                          0x8000
#define HWIO_GCC_BOOT_ROM_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                             0xf
#define HWIO_GCC_BOOT_ROM_AHB_CBCR_FORCE_MEM_CORE_ON_BMSK                                             0x4000
#define HWIO_GCC_BOOT_ROM_AHB_CBCR_FORCE_MEM_CORE_ON_SHFT                                                0xe
#define HWIO_GCC_BOOT_ROM_AHB_CBCR_FORCE_MEM_PERIPH_ON_BMSK                                           0x2000
#define HWIO_GCC_BOOT_ROM_AHB_CBCR_FORCE_MEM_PERIPH_ON_SHFT                                              0xd
#define HWIO_GCC_BOOT_ROM_AHB_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                                          0x1000
#define HWIO_GCC_BOOT_ROM_AHB_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                                             0xc
#define HWIO_GCC_BOOT_ROM_AHB_CBCR_WAKEUP_BMSK                                                         0xf00
#define HWIO_GCC_BOOT_ROM_AHB_CBCR_WAKEUP_SHFT                                                           0x8
#define HWIO_GCC_BOOT_ROM_AHB_CBCR_SLEEP_BMSK                                                           0xf0
#define HWIO_GCC_BOOT_ROM_AHB_CBCR_SLEEP_SHFT                                                            0x4

#define HWIO_GCC_MSG_RAM_BCR_ADDR                                                                 (GCC_CLK_CTL_REG_REG_BASE      + 0x0002b000)
#define HWIO_GCC_MSG_RAM_BCR_RMSK                                                                        0x1
#define HWIO_GCC_MSG_RAM_BCR_IN          \
        in_dword_masked(HWIO_GCC_MSG_RAM_BCR_ADDR, HWIO_GCC_MSG_RAM_BCR_RMSK)
#define HWIO_GCC_MSG_RAM_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_MSG_RAM_BCR_ADDR, m)
#define HWIO_GCC_MSG_RAM_BCR_OUT(v)      \
        out_dword(HWIO_GCC_MSG_RAM_BCR_ADDR,v)
#define HWIO_GCC_MSG_RAM_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_MSG_RAM_BCR_ADDR,m,v,HWIO_GCC_MSG_RAM_BCR_IN)
#define HWIO_GCC_MSG_RAM_BCR_BLK_ARES_BMSK                                                               0x1
#define HWIO_GCC_MSG_RAM_BCR_BLK_ARES_SHFT                                                               0x0

#define HWIO_GCC_MSG_RAM_AHB_CBCR_ADDR                                                            (GCC_CLK_CTL_REG_REG_BASE      + 0x0002b004)
#define HWIO_GCC_MSG_RAM_AHB_CBCR_RMSK                                                            0xf000fff0
#define HWIO_GCC_MSG_RAM_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_MSG_RAM_AHB_CBCR_ADDR, HWIO_GCC_MSG_RAM_AHB_CBCR_RMSK)
#define HWIO_GCC_MSG_RAM_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_MSG_RAM_AHB_CBCR_ADDR, m)
#define HWIO_GCC_MSG_RAM_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_MSG_RAM_AHB_CBCR_ADDR,v)
#define HWIO_GCC_MSG_RAM_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_MSG_RAM_AHB_CBCR_ADDR,m,v,HWIO_GCC_MSG_RAM_AHB_CBCR_IN)
#define HWIO_GCC_MSG_RAM_AHB_CBCR_CLK_OFF_BMSK                                                    0x80000000
#define HWIO_GCC_MSG_RAM_AHB_CBCR_CLK_OFF_SHFT                                                          0x1f
#define HWIO_GCC_MSG_RAM_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                   0x70000000
#define HWIO_GCC_MSG_RAM_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                         0x1c
#define HWIO_GCC_MSG_RAM_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                           0x8000
#define HWIO_GCC_MSG_RAM_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                              0xf
#define HWIO_GCC_MSG_RAM_AHB_CBCR_FORCE_MEM_CORE_ON_BMSK                                              0x4000
#define HWIO_GCC_MSG_RAM_AHB_CBCR_FORCE_MEM_CORE_ON_SHFT                                                 0xe
#define HWIO_GCC_MSG_RAM_AHB_CBCR_FORCE_MEM_PERIPH_ON_BMSK                                            0x2000
#define HWIO_GCC_MSG_RAM_AHB_CBCR_FORCE_MEM_PERIPH_ON_SHFT                                               0xd
#define HWIO_GCC_MSG_RAM_AHB_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                                           0x1000
#define HWIO_GCC_MSG_RAM_AHB_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                                              0xc
#define HWIO_GCC_MSG_RAM_AHB_CBCR_WAKEUP_BMSK                                                          0xf00
#define HWIO_GCC_MSG_RAM_AHB_CBCR_WAKEUP_SHFT                                                            0x8
#define HWIO_GCC_MSG_RAM_AHB_CBCR_SLEEP_BMSK                                                            0xf0
#define HWIO_GCC_MSG_RAM_AHB_CBCR_SLEEP_SHFT                                                             0x4

#define HWIO_GCC_TLMM_BCR_ADDR                                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x00034000)
#define HWIO_GCC_TLMM_BCR_RMSK                                                                           0x1
#define HWIO_GCC_TLMM_BCR_IN          \
        in_dword_masked(HWIO_GCC_TLMM_BCR_ADDR, HWIO_GCC_TLMM_BCR_RMSK)
#define HWIO_GCC_TLMM_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_TLMM_BCR_ADDR, m)
#define HWIO_GCC_TLMM_BCR_OUT(v)      \
        out_dword(HWIO_GCC_TLMM_BCR_ADDR,v)
#define HWIO_GCC_TLMM_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_TLMM_BCR_ADDR,m,v,HWIO_GCC_TLMM_BCR_IN)
#define HWIO_GCC_TLMM_BCR_BLK_ARES_BMSK                                                                  0x1
#define HWIO_GCC_TLMM_BCR_BLK_ARES_SHFT                                                                  0x0

#define HWIO_GCC_TLMM_AHB_CBCR_ADDR                                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x00034004)
#define HWIO_GCC_TLMM_AHB_CBCR_RMSK                                                               0xf0008000
#define HWIO_GCC_TLMM_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_TLMM_AHB_CBCR_ADDR, HWIO_GCC_TLMM_AHB_CBCR_RMSK)
#define HWIO_GCC_TLMM_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_TLMM_AHB_CBCR_ADDR, m)
#define HWIO_GCC_TLMM_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_TLMM_AHB_CBCR_ADDR,v)
#define HWIO_GCC_TLMM_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_TLMM_AHB_CBCR_ADDR,m,v,HWIO_GCC_TLMM_AHB_CBCR_IN)
#define HWIO_GCC_TLMM_AHB_CBCR_CLK_OFF_BMSK                                                       0x80000000
#define HWIO_GCC_TLMM_AHB_CBCR_CLK_OFF_SHFT                                                             0x1f
#define HWIO_GCC_TLMM_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                      0x70000000
#define HWIO_GCC_TLMM_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                            0x1c
#define HWIO_GCC_TLMM_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                              0x8000
#define HWIO_GCC_TLMM_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                                 0xf

#define HWIO_GCC_TLMM_CBCR_ADDR                                                                   (GCC_CLK_CTL_REG_REG_BASE      + 0x00034008)
#define HWIO_GCC_TLMM_CBCR_RMSK                                                                   0x80000000
#define HWIO_GCC_TLMM_CBCR_IN          \
        in_dword_masked(HWIO_GCC_TLMM_CBCR_ADDR, HWIO_GCC_TLMM_CBCR_RMSK)
#define HWIO_GCC_TLMM_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_TLMM_CBCR_ADDR, m)
#define HWIO_GCC_TLMM_CBCR_CLK_OFF_BMSK                                                           0x80000000
#define HWIO_GCC_TLMM_CBCR_CLK_OFF_SHFT                                                                 0x1f

#define HWIO_GCC_MPM_BCR_ADDR                                                                     (GCC_CLK_CTL_REG_REG_BASE      + 0x0002c000)
#define HWIO_GCC_MPM_BCR_RMSK                                                                            0x1
#define HWIO_GCC_MPM_BCR_IN          \
        in_dword_masked(HWIO_GCC_MPM_BCR_ADDR, HWIO_GCC_MPM_BCR_RMSK)
#define HWIO_GCC_MPM_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_MPM_BCR_ADDR, m)
#define HWIO_GCC_MPM_BCR_OUT(v)      \
        out_dword(HWIO_GCC_MPM_BCR_ADDR,v)
#define HWIO_GCC_MPM_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_MPM_BCR_ADDR,m,v,HWIO_GCC_MPM_BCR_IN)
#define HWIO_GCC_MPM_BCR_BLK_ARES_BMSK                                                                   0x1
#define HWIO_GCC_MPM_BCR_BLK_ARES_SHFT                                                                   0x0

#define HWIO_GCC_MPM_MISC_ADDR                                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x0002c004)
#define HWIO_GCC_MPM_MISC_RMSK                                                                           0x7
#define HWIO_GCC_MPM_MISC_IN          \
        in_dword_masked(HWIO_GCC_MPM_MISC_ADDR, HWIO_GCC_MPM_MISC_RMSK)
#define HWIO_GCC_MPM_MISC_INM(m)      \
        in_dword_masked(HWIO_GCC_MPM_MISC_ADDR, m)
#define HWIO_GCC_MPM_MISC_OUT(v)      \
        out_dword(HWIO_GCC_MPM_MISC_ADDR,v)
#define HWIO_GCC_MPM_MISC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_MPM_MISC_ADDR,m,v,HWIO_GCC_MPM_MISC_IN)
#define HWIO_GCC_MPM_MISC_MPM_NON_AHB_RESET_BMSK                                                         0x4
#define HWIO_GCC_MPM_MISC_MPM_NON_AHB_RESET_SHFT                                                         0x2
#define HWIO_GCC_MPM_MISC_MPM_AHB_RESET_BMSK                                                             0x2
#define HWIO_GCC_MPM_MISC_MPM_AHB_RESET_SHFT                                                             0x1
#define HWIO_GCC_MPM_MISC_MPM_REF_CLK_EN_BMSK                                                            0x1
#define HWIO_GCC_MPM_MISC_MPM_REF_CLK_EN_SHFT                                                            0x0

#define HWIO_GCC_MPM_AHB_CBCR_ADDR                                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x0002c008)
#define HWIO_GCC_MPM_AHB_CBCR_RMSK                                                                0xf0008000
#define HWIO_GCC_MPM_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_MPM_AHB_CBCR_ADDR, HWIO_GCC_MPM_AHB_CBCR_RMSK)
#define HWIO_GCC_MPM_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_MPM_AHB_CBCR_ADDR, m)
#define HWIO_GCC_MPM_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_MPM_AHB_CBCR_ADDR,v)
#define HWIO_GCC_MPM_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_MPM_AHB_CBCR_ADDR,m,v,HWIO_GCC_MPM_AHB_CBCR_IN)
#define HWIO_GCC_MPM_AHB_CBCR_CLK_OFF_BMSK                                                        0x80000000
#define HWIO_GCC_MPM_AHB_CBCR_CLK_OFF_SHFT                                                              0x1f
#define HWIO_GCC_MPM_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                       0x70000000
#define HWIO_GCC_MPM_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                             0x1c
#define HWIO_GCC_MPM_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                               0x8000
#define HWIO_GCC_MPM_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                                  0xf

#define HWIO_GCC_RPM_PROC_HCLK_CBCR_ADDR                                                          (GCC_CLK_CTL_REG_REG_BASE      + 0x0002d000)
#define HWIO_GCC_RPM_PROC_HCLK_CBCR_RMSK                                                          0x80000000
#define HWIO_GCC_RPM_PROC_HCLK_CBCR_IN          \
        in_dword_masked(HWIO_GCC_RPM_PROC_HCLK_CBCR_ADDR, HWIO_GCC_RPM_PROC_HCLK_CBCR_RMSK)
#define HWIO_GCC_RPM_PROC_HCLK_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_RPM_PROC_HCLK_CBCR_ADDR, m)
#define HWIO_GCC_RPM_PROC_HCLK_CBCR_CLK_OFF_BMSK                                                  0x80000000
#define HWIO_GCC_RPM_PROC_HCLK_CBCR_CLK_OFF_SHFT                                                        0x1f

#define HWIO_GCC_RPM_BUS_AHB_CBCR_ADDR                                                            (GCC_CLK_CTL_REG_REG_BASE      + 0x0002d004)
#define HWIO_GCC_RPM_BUS_AHB_CBCR_RMSK                                                            0xf000fff0
#define HWIO_GCC_RPM_BUS_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_RPM_BUS_AHB_CBCR_ADDR, HWIO_GCC_RPM_BUS_AHB_CBCR_RMSK)
#define HWIO_GCC_RPM_BUS_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_RPM_BUS_AHB_CBCR_ADDR, m)
#define HWIO_GCC_RPM_BUS_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_RPM_BUS_AHB_CBCR_ADDR,v)
#define HWIO_GCC_RPM_BUS_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_RPM_BUS_AHB_CBCR_ADDR,m,v,HWIO_GCC_RPM_BUS_AHB_CBCR_IN)
#define HWIO_GCC_RPM_BUS_AHB_CBCR_CLK_OFF_BMSK                                                    0x80000000
#define HWIO_GCC_RPM_BUS_AHB_CBCR_CLK_OFF_SHFT                                                          0x1f
#define HWIO_GCC_RPM_BUS_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                   0x70000000
#define HWIO_GCC_RPM_BUS_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                         0x1c
#define HWIO_GCC_RPM_BUS_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                           0x8000
#define HWIO_GCC_RPM_BUS_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                              0xf
#define HWIO_GCC_RPM_BUS_AHB_CBCR_FORCE_MEM_CORE_ON_BMSK                                              0x4000
#define HWIO_GCC_RPM_BUS_AHB_CBCR_FORCE_MEM_CORE_ON_SHFT                                                 0xe
#define HWIO_GCC_RPM_BUS_AHB_CBCR_FORCE_MEM_PERIPH_ON_BMSK                                            0x2000
#define HWIO_GCC_RPM_BUS_AHB_CBCR_FORCE_MEM_PERIPH_ON_SHFT                                               0xd
#define HWIO_GCC_RPM_BUS_AHB_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                                           0x1000
#define HWIO_GCC_RPM_BUS_AHB_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                                              0xc
#define HWIO_GCC_RPM_BUS_AHB_CBCR_WAKEUP_BMSK                                                          0xf00
#define HWIO_GCC_RPM_BUS_AHB_CBCR_WAKEUP_SHFT                                                            0x8
#define HWIO_GCC_RPM_BUS_AHB_CBCR_SLEEP_BMSK                                                            0xf0
#define HWIO_GCC_RPM_BUS_AHB_CBCR_SLEEP_SHFT                                                             0x4

#define HWIO_GCC_RPM_SLEEP_CBCR_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x0002d008)
#define HWIO_GCC_RPM_SLEEP_CBCR_RMSK                                                              0x80000001
#define HWIO_GCC_RPM_SLEEP_CBCR_IN          \
        in_dword_masked(HWIO_GCC_RPM_SLEEP_CBCR_ADDR, HWIO_GCC_RPM_SLEEP_CBCR_RMSK)
#define HWIO_GCC_RPM_SLEEP_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_RPM_SLEEP_CBCR_ADDR, m)
#define HWIO_GCC_RPM_SLEEP_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_RPM_SLEEP_CBCR_ADDR,v)
#define HWIO_GCC_RPM_SLEEP_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_RPM_SLEEP_CBCR_ADDR,m,v,HWIO_GCC_RPM_SLEEP_CBCR_IN)
#define HWIO_GCC_RPM_SLEEP_CBCR_CLK_OFF_BMSK                                                      0x80000000
#define HWIO_GCC_RPM_SLEEP_CBCR_CLK_OFF_SHFT                                                            0x1f
#define HWIO_GCC_RPM_SLEEP_CBCR_CLK_ENABLE_BMSK                                                          0x1
#define HWIO_GCC_RPM_SLEEP_CBCR_CLK_ENABLE_SHFT                                                          0x0

#define HWIO_GCC_RPM_TIMER_CBCR_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x0002d00c)
#define HWIO_GCC_RPM_TIMER_CBCR_RMSK                                                              0x80000003
#define HWIO_GCC_RPM_TIMER_CBCR_IN          \
        in_dword_masked(HWIO_GCC_RPM_TIMER_CBCR_ADDR, HWIO_GCC_RPM_TIMER_CBCR_RMSK)
#define HWIO_GCC_RPM_TIMER_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_RPM_TIMER_CBCR_ADDR, m)
#define HWIO_GCC_RPM_TIMER_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_RPM_TIMER_CBCR_ADDR,v)
#define HWIO_GCC_RPM_TIMER_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_RPM_TIMER_CBCR_ADDR,m,v,HWIO_GCC_RPM_TIMER_CBCR_IN)
#define HWIO_GCC_RPM_TIMER_CBCR_CLK_OFF_BMSK                                                      0x80000000
#define HWIO_GCC_RPM_TIMER_CBCR_CLK_OFF_SHFT                                                            0x1f
#define HWIO_GCC_RPM_TIMER_CBCR_HW_CTL_BMSK                                                              0x2
#define HWIO_GCC_RPM_TIMER_CBCR_HW_CTL_SHFT                                                              0x1
#define HWIO_GCC_RPM_TIMER_CBCR_CLK_ENABLE_BMSK                                                          0x1
#define HWIO_GCC_RPM_TIMER_CBCR_CLK_ENABLE_SHFT                                                          0x0

#define HWIO_GCC_RPM_CMD_RCGR_ADDR                                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x0002d010)
#define HWIO_GCC_RPM_CMD_RCGR_RMSK                                                                0x80000013
#define HWIO_GCC_RPM_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_RPM_CMD_RCGR_ADDR, HWIO_GCC_RPM_CMD_RCGR_RMSK)
#define HWIO_GCC_RPM_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_RPM_CMD_RCGR_ADDR, m)
#define HWIO_GCC_RPM_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_RPM_CMD_RCGR_ADDR,v)
#define HWIO_GCC_RPM_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_RPM_CMD_RCGR_ADDR,m,v,HWIO_GCC_RPM_CMD_RCGR_IN)
#define HWIO_GCC_RPM_CMD_RCGR_ROOT_OFF_BMSK                                                       0x80000000
#define HWIO_GCC_RPM_CMD_RCGR_ROOT_OFF_SHFT                                                             0x1f
#define HWIO_GCC_RPM_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                                       0x10
#define HWIO_GCC_RPM_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                                        0x4
#define HWIO_GCC_RPM_CMD_RCGR_ROOT_EN_BMSK                                                               0x2
#define HWIO_GCC_RPM_CMD_RCGR_ROOT_EN_SHFT                                                               0x1
#define HWIO_GCC_RPM_CMD_RCGR_UPDATE_BMSK                                                                0x1
#define HWIO_GCC_RPM_CMD_RCGR_UPDATE_SHFT                                                                0x0

#define HWIO_GCC_RPM_CFG_RCGR_ADDR                                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x0002d014)
#define HWIO_GCC_RPM_CFG_RCGR_RMSK                                                                     0x71f
#define HWIO_GCC_RPM_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_RPM_CFG_RCGR_ADDR, HWIO_GCC_RPM_CFG_RCGR_RMSK)
#define HWIO_GCC_RPM_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_RPM_CFG_RCGR_ADDR, m)
#define HWIO_GCC_RPM_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_RPM_CFG_RCGR_ADDR,v)
#define HWIO_GCC_RPM_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_RPM_CFG_RCGR_ADDR,m,v,HWIO_GCC_RPM_CFG_RCGR_IN)
#define HWIO_GCC_RPM_CFG_RCGR_SRC_SEL_BMSK                                                             0x700
#define HWIO_GCC_RPM_CFG_RCGR_SRC_SEL_SHFT                                                               0x8
#define HWIO_GCC_RPM_CFG_RCGR_SRC_DIV_BMSK                                                              0x1f
#define HWIO_GCC_RPM_CFG_RCGR_SRC_DIV_SHFT                                                               0x0

#define HWIO_GCC_RPM_MISC_ADDR                                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x0002d028)
#define HWIO_GCC_RPM_MISC_RMSK                                                                          0xf1
#define HWIO_GCC_RPM_MISC_IN          \
        in_dword_masked(HWIO_GCC_RPM_MISC_ADDR, HWIO_GCC_RPM_MISC_RMSK)
#define HWIO_GCC_RPM_MISC_INM(m)      \
        in_dword_masked(HWIO_GCC_RPM_MISC_ADDR, m)
#define HWIO_GCC_RPM_MISC_OUT(v)      \
        out_dword(HWIO_GCC_RPM_MISC_ADDR,v)
#define HWIO_GCC_RPM_MISC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_RPM_MISC_ADDR,m,v,HWIO_GCC_RPM_MISC_IN)
#define HWIO_GCC_RPM_MISC_RPM_CLK_AUTO_SCALE_DIV_BMSK                                                   0xf0
#define HWIO_GCC_RPM_MISC_RPM_CLK_AUTO_SCALE_DIV_SHFT                                                    0x4
#define HWIO_GCC_RPM_MISC_RPM_CLK_AUTO_SCALE_DIS_BMSK                                                    0x1
#define HWIO_GCC_RPM_MISC_RPM_CLK_AUTO_SCALE_DIS_SHFT                                                    0x0

#define HWIO_GCC_SEC_CTRL_BCR_ADDR                                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x0001a000)
#define HWIO_GCC_SEC_CTRL_BCR_RMSK                                                                       0x1
#define HWIO_GCC_SEC_CTRL_BCR_IN          \
        in_dword_masked(HWIO_GCC_SEC_CTRL_BCR_ADDR, HWIO_GCC_SEC_CTRL_BCR_RMSK)
#define HWIO_GCC_SEC_CTRL_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SEC_CTRL_BCR_ADDR, m)
#define HWIO_GCC_SEC_CTRL_BCR_OUT(v)      \
        out_dword(HWIO_GCC_SEC_CTRL_BCR_ADDR,v)
#define HWIO_GCC_SEC_CTRL_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SEC_CTRL_BCR_ADDR,m,v,HWIO_GCC_SEC_CTRL_BCR_IN)
#define HWIO_GCC_SEC_CTRL_BCR_BLK_ARES_BMSK                                                              0x1
#define HWIO_GCC_SEC_CTRL_BCR_BLK_ARES_SHFT                                                              0x0

#define HWIO_GCC_ACC_CMD_RCGR_ADDR                                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x0001a004)
#define HWIO_GCC_ACC_CMD_RCGR_RMSK                                                                0x80000013
#define HWIO_GCC_ACC_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_ACC_CMD_RCGR_ADDR, HWIO_GCC_ACC_CMD_RCGR_RMSK)
#define HWIO_GCC_ACC_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_ACC_CMD_RCGR_ADDR, m)
#define HWIO_GCC_ACC_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_ACC_CMD_RCGR_ADDR,v)
#define HWIO_GCC_ACC_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ACC_CMD_RCGR_ADDR,m,v,HWIO_GCC_ACC_CMD_RCGR_IN)
#define HWIO_GCC_ACC_CMD_RCGR_ROOT_OFF_BMSK                                                       0x80000000
#define HWIO_GCC_ACC_CMD_RCGR_ROOT_OFF_SHFT                                                             0x1f
#define HWIO_GCC_ACC_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                                       0x10
#define HWIO_GCC_ACC_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                                        0x4
#define HWIO_GCC_ACC_CMD_RCGR_ROOT_EN_BMSK                                                               0x2
#define HWIO_GCC_ACC_CMD_RCGR_ROOT_EN_SHFT                                                               0x1
#define HWIO_GCC_ACC_CMD_RCGR_UPDATE_BMSK                                                                0x1
#define HWIO_GCC_ACC_CMD_RCGR_UPDATE_SHFT                                                                0x0

#define HWIO_GCC_ACC_CFG_RCGR_ADDR                                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x0001a008)
#define HWIO_GCC_ACC_CFG_RCGR_RMSK                                                                     0x71f
#define HWIO_GCC_ACC_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_ACC_CFG_RCGR_ADDR, HWIO_GCC_ACC_CFG_RCGR_RMSK)
#define HWIO_GCC_ACC_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_ACC_CFG_RCGR_ADDR, m)
#define HWIO_GCC_ACC_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_ACC_CFG_RCGR_ADDR,v)
#define HWIO_GCC_ACC_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ACC_CFG_RCGR_ADDR,m,v,HWIO_GCC_ACC_CFG_RCGR_IN)
#define HWIO_GCC_ACC_CFG_RCGR_SRC_SEL_BMSK                                                             0x700
#define HWIO_GCC_ACC_CFG_RCGR_SRC_SEL_SHFT                                                               0x8
#define HWIO_GCC_ACC_CFG_RCGR_SRC_DIV_BMSK                                                              0x1f
#define HWIO_GCC_ACC_CFG_RCGR_SRC_DIV_SHFT                                                               0x0

#define HWIO_GCC_ACC_MISC_ADDR                                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x0001a01c)
#define HWIO_GCC_ACC_MISC_RMSK                                                                           0x1
#define HWIO_GCC_ACC_MISC_IN          \
        in_dword_masked(HWIO_GCC_ACC_MISC_ADDR, HWIO_GCC_ACC_MISC_RMSK)
#define HWIO_GCC_ACC_MISC_INM(m)      \
        in_dword_masked(HWIO_GCC_ACC_MISC_ADDR, m)
#define HWIO_GCC_ACC_MISC_OUT(v)      \
        out_dword(HWIO_GCC_ACC_MISC_ADDR,v)
#define HWIO_GCC_ACC_MISC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ACC_MISC_ADDR,m,v,HWIO_GCC_ACC_MISC_IN)
#define HWIO_GCC_ACC_MISC_JTAG_ACC_SRC_SEL_EN_BMSK                                                       0x1
#define HWIO_GCC_ACC_MISC_JTAG_ACC_SRC_SEL_EN_SHFT                                                       0x0

#define HWIO_GCC_SEC_CTRL_ACC_CBCR_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x0001a020)
#define HWIO_GCC_SEC_CTRL_ACC_CBCR_RMSK                                                           0x80007ff1
#define HWIO_GCC_SEC_CTRL_ACC_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SEC_CTRL_ACC_CBCR_ADDR, HWIO_GCC_SEC_CTRL_ACC_CBCR_RMSK)
#define HWIO_GCC_SEC_CTRL_ACC_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SEC_CTRL_ACC_CBCR_ADDR, m)
#define HWIO_GCC_SEC_CTRL_ACC_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_SEC_CTRL_ACC_CBCR_ADDR,v)
#define HWIO_GCC_SEC_CTRL_ACC_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SEC_CTRL_ACC_CBCR_ADDR,m,v,HWIO_GCC_SEC_CTRL_ACC_CBCR_IN)
#define HWIO_GCC_SEC_CTRL_ACC_CBCR_CLK_OFF_BMSK                                                   0x80000000
#define HWIO_GCC_SEC_CTRL_ACC_CBCR_CLK_OFF_SHFT                                                         0x1f
#define HWIO_GCC_SEC_CTRL_ACC_CBCR_FORCE_MEM_CORE_ON_BMSK                                             0x4000
#define HWIO_GCC_SEC_CTRL_ACC_CBCR_FORCE_MEM_CORE_ON_SHFT                                                0xe
#define HWIO_GCC_SEC_CTRL_ACC_CBCR_FORCE_MEM_PERIPH_ON_BMSK                                           0x2000
#define HWIO_GCC_SEC_CTRL_ACC_CBCR_FORCE_MEM_PERIPH_ON_SHFT                                              0xd
#define HWIO_GCC_SEC_CTRL_ACC_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                                          0x1000
#define HWIO_GCC_SEC_CTRL_ACC_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                                             0xc
#define HWIO_GCC_SEC_CTRL_ACC_CBCR_WAKEUP_BMSK                                                         0xf00
#define HWIO_GCC_SEC_CTRL_ACC_CBCR_WAKEUP_SHFT                                                           0x8
#define HWIO_GCC_SEC_CTRL_ACC_CBCR_SLEEP_BMSK                                                           0xf0
#define HWIO_GCC_SEC_CTRL_ACC_CBCR_SLEEP_SHFT                                                            0x4
#define HWIO_GCC_SEC_CTRL_ACC_CBCR_CLK_ENABLE_BMSK                                                       0x1
#define HWIO_GCC_SEC_CTRL_ACC_CBCR_CLK_ENABLE_SHFT                                                       0x0

#define HWIO_GCC_SEC_CTRL_AHB_CBCR_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x0001a024)
#define HWIO_GCC_SEC_CTRL_AHB_CBCR_RMSK                                                           0xf0008001
#define HWIO_GCC_SEC_CTRL_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SEC_CTRL_AHB_CBCR_ADDR, HWIO_GCC_SEC_CTRL_AHB_CBCR_RMSK)
#define HWIO_GCC_SEC_CTRL_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SEC_CTRL_AHB_CBCR_ADDR, m)
#define HWIO_GCC_SEC_CTRL_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_SEC_CTRL_AHB_CBCR_ADDR,v)
#define HWIO_GCC_SEC_CTRL_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SEC_CTRL_AHB_CBCR_ADDR,m,v,HWIO_GCC_SEC_CTRL_AHB_CBCR_IN)
#define HWIO_GCC_SEC_CTRL_AHB_CBCR_CLK_OFF_BMSK                                                   0x80000000
#define HWIO_GCC_SEC_CTRL_AHB_CBCR_CLK_OFF_SHFT                                                         0x1f
#define HWIO_GCC_SEC_CTRL_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                  0x70000000
#define HWIO_GCC_SEC_CTRL_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                        0x1c
#define HWIO_GCC_SEC_CTRL_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                          0x8000
#define HWIO_GCC_SEC_CTRL_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                             0xf
#define HWIO_GCC_SEC_CTRL_AHB_CBCR_CLK_ENABLE_BMSK                                                       0x1
#define HWIO_GCC_SEC_CTRL_AHB_CBCR_CLK_ENABLE_SHFT                                                       0x0

#define HWIO_GCC_SEC_CTRL_CBCR_ADDR                                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x0001a028)
#define HWIO_GCC_SEC_CTRL_CBCR_RMSK                                                               0x80007ff1
#define HWIO_GCC_SEC_CTRL_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SEC_CTRL_CBCR_ADDR, HWIO_GCC_SEC_CTRL_CBCR_RMSK)
#define HWIO_GCC_SEC_CTRL_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SEC_CTRL_CBCR_ADDR, m)
#define HWIO_GCC_SEC_CTRL_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_SEC_CTRL_CBCR_ADDR,v)
#define HWIO_GCC_SEC_CTRL_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SEC_CTRL_CBCR_ADDR,m,v,HWIO_GCC_SEC_CTRL_CBCR_IN)
#define HWIO_GCC_SEC_CTRL_CBCR_CLK_OFF_BMSK                                                       0x80000000
#define HWIO_GCC_SEC_CTRL_CBCR_CLK_OFF_SHFT                                                             0x1f
#define HWIO_GCC_SEC_CTRL_CBCR_FORCE_MEM_CORE_ON_BMSK                                                 0x4000
#define HWIO_GCC_SEC_CTRL_CBCR_FORCE_MEM_CORE_ON_SHFT                                                    0xe
#define HWIO_GCC_SEC_CTRL_CBCR_FORCE_MEM_PERIPH_ON_BMSK                                               0x2000
#define HWIO_GCC_SEC_CTRL_CBCR_FORCE_MEM_PERIPH_ON_SHFT                                                  0xd
#define HWIO_GCC_SEC_CTRL_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                                              0x1000
#define HWIO_GCC_SEC_CTRL_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                                                 0xc
#define HWIO_GCC_SEC_CTRL_CBCR_WAKEUP_BMSK                                                             0xf00
#define HWIO_GCC_SEC_CTRL_CBCR_WAKEUP_SHFT                                                               0x8
#define HWIO_GCC_SEC_CTRL_CBCR_SLEEP_BMSK                                                               0xf0
#define HWIO_GCC_SEC_CTRL_CBCR_SLEEP_SHFT                                                                0x4
#define HWIO_GCC_SEC_CTRL_CBCR_CLK_ENABLE_BMSK                                                           0x1
#define HWIO_GCC_SEC_CTRL_CBCR_CLK_ENABLE_SHFT                                                           0x0

#define HWIO_GCC_SEC_CTRL_SENSE_CBCR_ADDR                                                         (GCC_CLK_CTL_REG_REG_BASE      + 0x0001a02c)
#define HWIO_GCC_SEC_CTRL_SENSE_CBCR_RMSK                                                         0x80000001
#define HWIO_GCC_SEC_CTRL_SENSE_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SEC_CTRL_SENSE_CBCR_ADDR, HWIO_GCC_SEC_CTRL_SENSE_CBCR_RMSK)
#define HWIO_GCC_SEC_CTRL_SENSE_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SEC_CTRL_SENSE_CBCR_ADDR, m)
#define HWIO_GCC_SEC_CTRL_SENSE_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_SEC_CTRL_SENSE_CBCR_ADDR,v)
#define HWIO_GCC_SEC_CTRL_SENSE_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SEC_CTRL_SENSE_CBCR_ADDR,m,v,HWIO_GCC_SEC_CTRL_SENSE_CBCR_IN)
#define HWIO_GCC_SEC_CTRL_SENSE_CBCR_CLK_OFF_BMSK                                                 0x80000000
#define HWIO_GCC_SEC_CTRL_SENSE_CBCR_CLK_OFF_SHFT                                                       0x1f
#define HWIO_GCC_SEC_CTRL_SENSE_CBCR_CLK_ENABLE_BMSK                                                     0x1
#define HWIO_GCC_SEC_CTRL_SENSE_CBCR_CLK_ENABLE_SHFT                                                     0x0

#define HWIO_GCC_SEC_CTRL_BOOT_ROM_PATCH_CBCR_ADDR                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x0001a030)
#define HWIO_GCC_SEC_CTRL_BOOT_ROM_PATCH_CBCR_RMSK                                                0x80000001
#define HWIO_GCC_SEC_CTRL_BOOT_ROM_PATCH_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SEC_CTRL_BOOT_ROM_PATCH_CBCR_ADDR, HWIO_GCC_SEC_CTRL_BOOT_ROM_PATCH_CBCR_RMSK)
#define HWIO_GCC_SEC_CTRL_BOOT_ROM_PATCH_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SEC_CTRL_BOOT_ROM_PATCH_CBCR_ADDR, m)
#define HWIO_GCC_SEC_CTRL_BOOT_ROM_PATCH_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_SEC_CTRL_BOOT_ROM_PATCH_CBCR_ADDR,v)
#define HWIO_GCC_SEC_CTRL_BOOT_ROM_PATCH_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SEC_CTRL_BOOT_ROM_PATCH_CBCR_ADDR,m,v,HWIO_GCC_SEC_CTRL_BOOT_ROM_PATCH_CBCR_IN)
#define HWIO_GCC_SEC_CTRL_BOOT_ROM_PATCH_CBCR_CLK_OFF_BMSK                                        0x80000000
#define HWIO_GCC_SEC_CTRL_BOOT_ROM_PATCH_CBCR_CLK_OFF_SHFT                                              0x1f
#define HWIO_GCC_SEC_CTRL_BOOT_ROM_PATCH_CBCR_CLK_ENABLE_BMSK                                            0x1
#define HWIO_GCC_SEC_CTRL_BOOT_ROM_PATCH_CBCR_CLK_ENABLE_SHFT                                            0x0

#define HWIO_GCC_SEC_CTRL_CMD_RCGR_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x0001a034)
#define HWIO_GCC_SEC_CTRL_CMD_RCGR_RMSK                                                           0x80000013
#define HWIO_GCC_SEC_CTRL_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_SEC_CTRL_CMD_RCGR_ADDR, HWIO_GCC_SEC_CTRL_CMD_RCGR_RMSK)
#define HWIO_GCC_SEC_CTRL_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_SEC_CTRL_CMD_RCGR_ADDR, m)
#define HWIO_GCC_SEC_CTRL_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_SEC_CTRL_CMD_RCGR_ADDR,v)
#define HWIO_GCC_SEC_CTRL_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SEC_CTRL_CMD_RCGR_ADDR,m,v,HWIO_GCC_SEC_CTRL_CMD_RCGR_IN)
#define HWIO_GCC_SEC_CTRL_CMD_RCGR_ROOT_OFF_BMSK                                                  0x80000000
#define HWIO_GCC_SEC_CTRL_CMD_RCGR_ROOT_OFF_SHFT                                                        0x1f
#define HWIO_GCC_SEC_CTRL_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                                  0x10
#define HWIO_GCC_SEC_CTRL_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                                   0x4
#define HWIO_GCC_SEC_CTRL_CMD_RCGR_ROOT_EN_BMSK                                                          0x2
#define HWIO_GCC_SEC_CTRL_CMD_RCGR_ROOT_EN_SHFT                                                          0x1
#define HWIO_GCC_SEC_CTRL_CMD_RCGR_UPDATE_BMSK                                                           0x1
#define HWIO_GCC_SEC_CTRL_CMD_RCGR_UPDATE_SHFT                                                           0x0

#define HWIO_GCC_SEC_CTRL_CFG_RCGR_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x0001a038)
#define HWIO_GCC_SEC_CTRL_CFG_RCGR_RMSK                                                                0x71f
#define HWIO_GCC_SEC_CTRL_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_SEC_CTRL_CFG_RCGR_ADDR, HWIO_GCC_SEC_CTRL_CFG_RCGR_RMSK)
#define HWIO_GCC_SEC_CTRL_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_SEC_CTRL_CFG_RCGR_ADDR, m)
#define HWIO_GCC_SEC_CTRL_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_SEC_CTRL_CFG_RCGR_ADDR,v)
#define HWIO_GCC_SEC_CTRL_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SEC_CTRL_CFG_RCGR_ADDR,m,v,HWIO_GCC_SEC_CTRL_CFG_RCGR_IN)
#define HWIO_GCC_SEC_CTRL_CFG_RCGR_SRC_SEL_BMSK                                                        0x700
#define HWIO_GCC_SEC_CTRL_CFG_RCGR_SRC_SEL_SHFT                                                          0x8
#define HWIO_GCC_SEC_CTRL_CFG_RCGR_SRC_DIV_BMSK                                                         0x1f
#define HWIO_GCC_SEC_CTRL_CFG_RCGR_SRC_DIV_SHFT                                                          0x0

#define HWIO_GCC_SPMI_BCR_ADDR                                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x0002e000)
#define HWIO_GCC_SPMI_BCR_RMSK                                                                           0x1
#define HWIO_GCC_SPMI_BCR_IN          \
        in_dword_masked(HWIO_GCC_SPMI_BCR_ADDR, HWIO_GCC_SPMI_BCR_RMSK)
#define HWIO_GCC_SPMI_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SPMI_BCR_ADDR, m)
#define HWIO_GCC_SPMI_BCR_OUT(v)      \
        out_dword(HWIO_GCC_SPMI_BCR_ADDR,v)
#define HWIO_GCC_SPMI_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SPMI_BCR_ADDR,m,v,HWIO_GCC_SPMI_BCR_IN)
#define HWIO_GCC_SPMI_BCR_BLK_ARES_BMSK                                                                  0x1
#define HWIO_GCC_SPMI_BCR_BLK_ARES_SHFT                                                                  0x0

#define HWIO_GCC_SPMI_SER_CMD_RCGR_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x0002e004)
#define HWIO_GCC_SPMI_SER_CMD_RCGR_RMSK                                                           0x80000013
#define HWIO_GCC_SPMI_SER_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_SPMI_SER_CMD_RCGR_ADDR, HWIO_GCC_SPMI_SER_CMD_RCGR_RMSK)
#define HWIO_GCC_SPMI_SER_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_SPMI_SER_CMD_RCGR_ADDR, m)
#define HWIO_GCC_SPMI_SER_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_SPMI_SER_CMD_RCGR_ADDR,v)
#define HWIO_GCC_SPMI_SER_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SPMI_SER_CMD_RCGR_ADDR,m,v,HWIO_GCC_SPMI_SER_CMD_RCGR_IN)
#define HWIO_GCC_SPMI_SER_CMD_RCGR_ROOT_OFF_BMSK                                                  0x80000000
#define HWIO_GCC_SPMI_SER_CMD_RCGR_ROOT_OFF_SHFT                                                        0x1f
#define HWIO_GCC_SPMI_SER_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                                  0x10
#define HWIO_GCC_SPMI_SER_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                                   0x4
#define HWIO_GCC_SPMI_SER_CMD_RCGR_ROOT_EN_BMSK                                                          0x2
#define HWIO_GCC_SPMI_SER_CMD_RCGR_ROOT_EN_SHFT                                                          0x1
#define HWIO_GCC_SPMI_SER_CMD_RCGR_UPDATE_BMSK                                                           0x1
#define HWIO_GCC_SPMI_SER_CMD_RCGR_UPDATE_SHFT                                                           0x0

#define HWIO_GCC_SPMI_SER_CFG_RCGR_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x0002e008)
#define HWIO_GCC_SPMI_SER_CFG_RCGR_RMSK                                                                0x71f
#define HWIO_GCC_SPMI_SER_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_SPMI_SER_CFG_RCGR_ADDR, HWIO_GCC_SPMI_SER_CFG_RCGR_RMSK)
#define HWIO_GCC_SPMI_SER_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_SPMI_SER_CFG_RCGR_ADDR, m)
#define HWIO_GCC_SPMI_SER_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_SPMI_SER_CFG_RCGR_ADDR,v)
#define HWIO_GCC_SPMI_SER_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SPMI_SER_CFG_RCGR_ADDR,m,v,HWIO_GCC_SPMI_SER_CFG_RCGR_IN)
#define HWIO_GCC_SPMI_SER_CFG_RCGR_SRC_SEL_BMSK                                                        0x700
#define HWIO_GCC_SPMI_SER_CFG_RCGR_SRC_SEL_SHFT                                                          0x8
#define HWIO_GCC_SPMI_SER_CFG_RCGR_SRC_DIV_BMSK                                                         0x1f
#define HWIO_GCC_SPMI_SER_CFG_RCGR_SRC_DIV_SHFT                                                          0x0

#define HWIO_GCC_SPMI_SER_CBCR_ADDR                                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x0002e01c)
#define HWIO_GCC_SPMI_SER_CBCR_RMSK                                                               0x80007ff1
#define HWIO_GCC_SPMI_SER_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SPMI_SER_CBCR_ADDR, HWIO_GCC_SPMI_SER_CBCR_RMSK)
#define HWIO_GCC_SPMI_SER_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SPMI_SER_CBCR_ADDR, m)
#define HWIO_GCC_SPMI_SER_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_SPMI_SER_CBCR_ADDR,v)
#define HWIO_GCC_SPMI_SER_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SPMI_SER_CBCR_ADDR,m,v,HWIO_GCC_SPMI_SER_CBCR_IN)
#define HWIO_GCC_SPMI_SER_CBCR_CLK_OFF_BMSK                                                       0x80000000
#define HWIO_GCC_SPMI_SER_CBCR_CLK_OFF_SHFT                                                             0x1f
#define HWIO_GCC_SPMI_SER_CBCR_FORCE_MEM_CORE_ON_BMSK                                                 0x4000
#define HWIO_GCC_SPMI_SER_CBCR_FORCE_MEM_CORE_ON_SHFT                                                    0xe
#define HWIO_GCC_SPMI_SER_CBCR_FORCE_MEM_PERIPH_ON_BMSK                                               0x2000
#define HWIO_GCC_SPMI_SER_CBCR_FORCE_MEM_PERIPH_ON_SHFT                                                  0xd
#define HWIO_GCC_SPMI_SER_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                                              0x1000
#define HWIO_GCC_SPMI_SER_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                                                 0xc
#define HWIO_GCC_SPMI_SER_CBCR_WAKEUP_BMSK                                                             0xf00
#define HWIO_GCC_SPMI_SER_CBCR_WAKEUP_SHFT                                                               0x8
#define HWIO_GCC_SPMI_SER_CBCR_SLEEP_BMSK                                                               0xf0
#define HWIO_GCC_SPMI_SER_CBCR_SLEEP_SHFT                                                                0x4
#define HWIO_GCC_SPMI_SER_CBCR_CLK_ENABLE_BMSK                                                           0x1
#define HWIO_GCC_SPMI_SER_CBCR_CLK_ENABLE_SHFT                                                           0x0

#define HWIO_GCC_SPMI_PCNOC_AHB_CBCR_ADDR                                                         (GCC_CLK_CTL_REG_REG_BASE      + 0x0002e020)
#define HWIO_GCC_SPMI_PCNOC_AHB_CBCR_RMSK                                                         0xf0008000
#define HWIO_GCC_SPMI_PCNOC_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SPMI_PCNOC_AHB_CBCR_ADDR, HWIO_GCC_SPMI_PCNOC_AHB_CBCR_RMSK)
#define HWIO_GCC_SPMI_PCNOC_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SPMI_PCNOC_AHB_CBCR_ADDR, m)
#define HWIO_GCC_SPMI_PCNOC_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_SPMI_PCNOC_AHB_CBCR_ADDR,v)
#define HWIO_GCC_SPMI_PCNOC_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SPMI_PCNOC_AHB_CBCR_ADDR,m,v,HWIO_GCC_SPMI_PCNOC_AHB_CBCR_IN)
#define HWIO_GCC_SPMI_PCNOC_AHB_CBCR_CLK_OFF_BMSK                                                 0x80000000
#define HWIO_GCC_SPMI_PCNOC_AHB_CBCR_CLK_OFF_SHFT                                                       0x1f
#define HWIO_GCC_SPMI_PCNOC_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                0x70000000
#define HWIO_GCC_SPMI_PCNOC_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                      0x1c
#define HWIO_GCC_SPMI_PCNOC_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                        0x8000
#define HWIO_GCC_SPMI_PCNOC_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                           0xf

#define HWIO_GCC_SPMI_AHB_CMD_RCGR_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x0002e024)
#define HWIO_GCC_SPMI_AHB_CMD_RCGR_RMSK                                                           0x80000013
#define HWIO_GCC_SPMI_AHB_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_SPMI_AHB_CMD_RCGR_ADDR, HWIO_GCC_SPMI_AHB_CMD_RCGR_RMSK)
#define HWIO_GCC_SPMI_AHB_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_SPMI_AHB_CMD_RCGR_ADDR, m)
#define HWIO_GCC_SPMI_AHB_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_SPMI_AHB_CMD_RCGR_ADDR,v)
#define HWIO_GCC_SPMI_AHB_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SPMI_AHB_CMD_RCGR_ADDR,m,v,HWIO_GCC_SPMI_AHB_CMD_RCGR_IN)
#define HWIO_GCC_SPMI_AHB_CMD_RCGR_ROOT_OFF_BMSK                                                  0x80000000
#define HWIO_GCC_SPMI_AHB_CMD_RCGR_ROOT_OFF_SHFT                                                        0x1f
#define HWIO_GCC_SPMI_AHB_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                                  0x10
#define HWIO_GCC_SPMI_AHB_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                                   0x4
#define HWIO_GCC_SPMI_AHB_CMD_RCGR_ROOT_EN_BMSK                                                          0x2
#define HWIO_GCC_SPMI_AHB_CMD_RCGR_ROOT_EN_SHFT                                                          0x1
#define HWIO_GCC_SPMI_AHB_CMD_RCGR_UPDATE_BMSK                                                           0x1
#define HWIO_GCC_SPMI_AHB_CMD_RCGR_UPDATE_SHFT                                                           0x0

#define HWIO_GCC_SPMI_AHB_CFG_RCGR_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x0002e028)
#define HWIO_GCC_SPMI_AHB_CFG_RCGR_RMSK                                                                0x71f
#define HWIO_GCC_SPMI_AHB_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_SPMI_AHB_CFG_RCGR_ADDR, HWIO_GCC_SPMI_AHB_CFG_RCGR_RMSK)
#define HWIO_GCC_SPMI_AHB_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_SPMI_AHB_CFG_RCGR_ADDR, m)
#define HWIO_GCC_SPMI_AHB_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_SPMI_AHB_CFG_RCGR_ADDR,v)
#define HWIO_GCC_SPMI_AHB_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SPMI_AHB_CFG_RCGR_ADDR,m,v,HWIO_GCC_SPMI_AHB_CFG_RCGR_IN)
#define HWIO_GCC_SPMI_AHB_CFG_RCGR_SRC_SEL_BMSK                                                        0x700
#define HWIO_GCC_SPMI_AHB_CFG_RCGR_SRC_SEL_SHFT                                                          0x8
#define HWIO_GCC_SPMI_AHB_CFG_RCGR_SRC_DIV_BMSK                                                         0x1f
#define HWIO_GCC_SPMI_AHB_CFG_RCGR_SRC_DIV_SHFT                                                          0x0

#define HWIO_GCC_SPMI_AHB_CBCR_ADDR                                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x0002e03c)
#define HWIO_GCC_SPMI_AHB_CBCR_RMSK                                                               0x80007ff1
#define HWIO_GCC_SPMI_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SPMI_AHB_CBCR_ADDR, HWIO_GCC_SPMI_AHB_CBCR_RMSK)
#define HWIO_GCC_SPMI_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SPMI_AHB_CBCR_ADDR, m)
#define HWIO_GCC_SPMI_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_SPMI_AHB_CBCR_ADDR,v)
#define HWIO_GCC_SPMI_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SPMI_AHB_CBCR_ADDR,m,v,HWIO_GCC_SPMI_AHB_CBCR_IN)
#define HWIO_GCC_SPMI_AHB_CBCR_CLK_OFF_BMSK                                                       0x80000000
#define HWIO_GCC_SPMI_AHB_CBCR_CLK_OFF_SHFT                                                             0x1f
#define HWIO_GCC_SPMI_AHB_CBCR_FORCE_MEM_CORE_ON_BMSK                                                 0x4000
#define HWIO_GCC_SPMI_AHB_CBCR_FORCE_MEM_CORE_ON_SHFT                                                    0xe
#define HWIO_GCC_SPMI_AHB_CBCR_FORCE_MEM_PERIPH_ON_BMSK                                               0x2000
#define HWIO_GCC_SPMI_AHB_CBCR_FORCE_MEM_PERIPH_ON_SHFT                                                  0xd
#define HWIO_GCC_SPMI_AHB_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                                              0x1000
#define HWIO_GCC_SPMI_AHB_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                                                 0xc
#define HWIO_GCC_SPMI_AHB_CBCR_WAKEUP_BMSK                                                             0xf00
#define HWIO_GCC_SPMI_AHB_CBCR_WAKEUP_SHFT                                                               0x8
#define HWIO_GCC_SPMI_AHB_CBCR_SLEEP_BMSK                                                               0xf0
#define HWIO_GCC_SPMI_AHB_CBCR_SLEEP_SHFT                                                                0x4
#define HWIO_GCC_SPMI_AHB_CBCR_CLK_ENABLE_BMSK                                                           0x1
#define HWIO_GCC_SPMI_AHB_CBCR_CLK_ENABLE_SHFT                                                           0x0

#define HWIO_GCC_SPDM_BCR_ADDR                                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x0002f000)
#define HWIO_GCC_SPDM_BCR_RMSK                                                                           0x1
#define HWIO_GCC_SPDM_BCR_IN          \
        in_dword_masked(HWIO_GCC_SPDM_BCR_ADDR, HWIO_GCC_SPDM_BCR_RMSK)
#define HWIO_GCC_SPDM_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SPDM_BCR_ADDR, m)
#define HWIO_GCC_SPDM_BCR_OUT(v)      \
        out_dword(HWIO_GCC_SPDM_BCR_ADDR,v)
#define HWIO_GCC_SPDM_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SPDM_BCR_ADDR,m,v,HWIO_GCC_SPDM_BCR_IN)
#define HWIO_GCC_SPDM_BCR_BLK_ARES_BMSK                                                                  0x1
#define HWIO_GCC_SPDM_BCR_BLK_ARES_SHFT                                                                  0x0

#define HWIO_GCC_SPDM_CFG_AHB_CBCR_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x0002f004)
#define HWIO_GCC_SPDM_CFG_AHB_CBCR_RMSK                                                           0xf0008001
#define HWIO_GCC_SPDM_CFG_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SPDM_CFG_AHB_CBCR_ADDR, HWIO_GCC_SPDM_CFG_AHB_CBCR_RMSK)
#define HWIO_GCC_SPDM_CFG_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SPDM_CFG_AHB_CBCR_ADDR, m)
#define HWIO_GCC_SPDM_CFG_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_SPDM_CFG_AHB_CBCR_ADDR,v)
#define HWIO_GCC_SPDM_CFG_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SPDM_CFG_AHB_CBCR_ADDR,m,v,HWIO_GCC_SPDM_CFG_AHB_CBCR_IN)
#define HWIO_GCC_SPDM_CFG_AHB_CBCR_CLK_OFF_BMSK                                                   0x80000000
#define HWIO_GCC_SPDM_CFG_AHB_CBCR_CLK_OFF_SHFT                                                         0x1f
#define HWIO_GCC_SPDM_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                  0x70000000
#define HWIO_GCC_SPDM_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                        0x1c
#define HWIO_GCC_SPDM_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                          0x8000
#define HWIO_GCC_SPDM_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                             0xf
#define HWIO_GCC_SPDM_CFG_AHB_CBCR_CLK_ENABLE_BMSK                                                       0x1
#define HWIO_GCC_SPDM_CFG_AHB_CBCR_CLK_ENABLE_SHFT                                                       0x0

#define HWIO_GCC_SPDM_MSTR_AHB_CBCR_ADDR                                                          (GCC_CLK_CTL_REG_REG_BASE      + 0x0002f008)
#define HWIO_GCC_SPDM_MSTR_AHB_CBCR_RMSK                                                          0x80000001
#define HWIO_GCC_SPDM_MSTR_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SPDM_MSTR_AHB_CBCR_ADDR, HWIO_GCC_SPDM_MSTR_AHB_CBCR_RMSK)
#define HWIO_GCC_SPDM_MSTR_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SPDM_MSTR_AHB_CBCR_ADDR, m)
#define HWIO_GCC_SPDM_MSTR_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_SPDM_MSTR_AHB_CBCR_ADDR,v)
#define HWIO_GCC_SPDM_MSTR_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SPDM_MSTR_AHB_CBCR_ADDR,m,v,HWIO_GCC_SPDM_MSTR_AHB_CBCR_IN)
#define HWIO_GCC_SPDM_MSTR_AHB_CBCR_CLK_OFF_BMSK                                                  0x80000000
#define HWIO_GCC_SPDM_MSTR_AHB_CBCR_CLK_OFF_SHFT                                                        0x1f
#define HWIO_GCC_SPDM_MSTR_AHB_CBCR_CLK_ENABLE_BMSK                                                      0x1
#define HWIO_GCC_SPDM_MSTR_AHB_CBCR_CLK_ENABLE_SHFT                                                      0x0

#define HWIO_GCC_SPDM_FF_CBCR_ADDR                                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x0002f00c)
#define HWIO_GCC_SPDM_FF_CBCR_RMSK                                                                0x80000001
#define HWIO_GCC_SPDM_FF_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SPDM_FF_CBCR_ADDR, HWIO_GCC_SPDM_FF_CBCR_RMSK)
#define HWIO_GCC_SPDM_FF_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SPDM_FF_CBCR_ADDR, m)
#define HWIO_GCC_SPDM_FF_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_SPDM_FF_CBCR_ADDR,v)
#define HWIO_GCC_SPDM_FF_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SPDM_FF_CBCR_ADDR,m,v,HWIO_GCC_SPDM_FF_CBCR_IN)
#define HWIO_GCC_SPDM_FF_CBCR_CLK_OFF_BMSK                                                        0x80000000
#define HWIO_GCC_SPDM_FF_CBCR_CLK_OFF_SHFT                                                              0x1f
#define HWIO_GCC_SPDM_FF_CBCR_CLK_ENABLE_BMSK                                                            0x1
#define HWIO_GCC_SPDM_FF_CBCR_CLK_ENABLE_SHFT                                                            0x0

#define HWIO_GCC_SPDM_BIMC_CY_CBCR_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x0002f010)
#define HWIO_GCC_SPDM_BIMC_CY_CBCR_RMSK                                                           0x80000001
#define HWIO_GCC_SPDM_BIMC_CY_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SPDM_BIMC_CY_CBCR_ADDR, HWIO_GCC_SPDM_BIMC_CY_CBCR_RMSK)
#define HWIO_GCC_SPDM_BIMC_CY_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SPDM_BIMC_CY_CBCR_ADDR, m)
#define HWIO_GCC_SPDM_BIMC_CY_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_SPDM_BIMC_CY_CBCR_ADDR,v)
#define HWIO_GCC_SPDM_BIMC_CY_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SPDM_BIMC_CY_CBCR_ADDR,m,v,HWIO_GCC_SPDM_BIMC_CY_CBCR_IN)
#define HWIO_GCC_SPDM_BIMC_CY_CBCR_CLK_OFF_BMSK                                                   0x80000000
#define HWIO_GCC_SPDM_BIMC_CY_CBCR_CLK_OFF_SHFT                                                         0x1f
#define HWIO_GCC_SPDM_BIMC_CY_CBCR_CLK_ENABLE_BMSK                                                       0x1
#define HWIO_GCC_SPDM_BIMC_CY_CBCR_CLK_ENABLE_SHFT                                                       0x0

#define HWIO_GCC_SPDM_SNOC_CY_CBCR_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x0002f014)
#define HWIO_GCC_SPDM_SNOC_CY_CBCR_RMSK                                                           0x80000001
#define HWIO_GCC_SPDM_SNOC_CY_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SPDM_SNOC_CY_CBCR_ADDR, HWIO_GCC_SPDM_SNOC_CY_CBCR_RMSK)
#define HWIO_GCC_SPDM_SNOC_CY_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SPDM_SNOC_CY_CBCR_ADDR, m)
#define HWIO_GCC_SPDM_SNOC_CY_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_SPDM_SNOC_CY_CBCR_ADDR,v)
#define HWIO_GCC_SPDM_SNOC_CY_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SPDM_SNOC_CY_CBCR_ADDR,m,v,HWIO_GCC_SPDM_SNOC_CY_CBCR_IN)
#define HWIO_GCC_SPDM_SNOC_CY_CBCR_CLK_OFF_BMSK                                                   0x80000000
#define HWIO_GCC_SPDM_SNOC_CY_CBCR_CLK_OFF_SHFT                                                         0x1f
#define HWIO_GCC_SPDM_SNOC_CY_CBCR_CLK_ENABLE_BMSK                                                       0x1
#define HWIO_GCC_SPDM_SNOC_CY_CBCR_CLK_ENABLE_SHFT                                                       0x0

#define HWIO_GCC_SPDM_DEBUG_CY_CBCR_ADDR                                                          (GCC_CLK_CTL_REG_REG_BASE      + 0x0002f018)
#define HWIO_GCC_SPDM_DEBUG_CY_CBCR_RMSK                                                          0x80000001
#define HWIO_GCC_SPDM_DEBUG_CY_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SPDM_DEBUG_CY_CBCR_ADDR, HWIO_GCC_SPDM_DEBUG_CY_CBCR_RMSK)
#define HWIO_GCC_SPDM_DEBUG_CY_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SPDM_DEBUG_CY_CBCR_ADDR, m)
#define HWIO_GCC_SPDM_DEBUG_CY_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_SPDM_DEBUG_CY_CBCR_ADDR,v)
#define HWIO_GCC_SPDM_DEBUG_CY_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SPDM_DEBUG_CY_CBCR_ADDR,m,v,HWIO_GCC_SPDM_DEBUG_CY_CBCR_IN)
#define HWIO_GCC_SPDM_DEBUG_CY_CBCR_CLK_OFF_BMSK                                                  0x80000000
#define HWIO_GCC_SPDM_DEBUG_CY_CBCR_CLK_OFF_SHFT                                                        0x1f
#define HWIO_GCC_SPDM_DEBUG_CY_CBCR_CLK_ENABLE_BMSK                                                      0x1
#define HWIO_GCC_SPDM_DEBUG_CY_CBCR_CLK_ENABLE_SHFT                                                      0x0

#define HWIO_GCC_SPDM_PCNOC_CY_CBCR_ADDR                                                          (GCC_CLK_CTL_REG_REG_BASE      + 0x0002f01c)
#define HWIO_GCC_SPDM_PCNOC_CY_CBCR_RMSK                                                          0x80000001
#define HWIO_GCC_SPDM_PCNOC_CY_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SPDM_PCNOC_CY_CBCR_ADDR, HWIO_GCC_SPDM_PCNOC_CY_CBCR_RMSK)
#define HWIO_GCC_SPDM_PCNOC_CY_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SPDM_PCNOC_CY_CBCR_ADDR, m)
#define HWIO_GCC_SPDM_PCNOC_CY_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_SPDM_PCNOC_CY_CBCR_ADDR,v)
#define HWIO_GCC_SPDM_PCNOC_CY_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SPDM_PCNOC_CY_CBCR_ADDR,m,v,HWIO_GCC_SPDM_PCNOC_CY_CBCR_IN)
#define HWIO_GCC_SPDM_PCNOC_CY_CBCR_CLK_OFF_BMSK                                                  0x80000000
#define HWIO_GCC_SPDM_PCNOC_CY_CBCR_CLK_OFF_SHFT                                                        0x1f
#define HWIO_GCC_SPDM_PCNOC_CY_CBCR_CLK_ENABLE_BMSK                                                      0x1
#define HWIO_GCC_SPDM_PCNOC_CY_CBCR_CLK_ENABLE_SHFT                                                      0x0

#define HWIO_GCC_SPDM_RPM_CY_CBCR_ADDR                                                            (GCC_CLK_CTL_REG_REG_BASE      + 0x0002f020)
#define HWIO_GCC_SPDM_RPM_CY_CBCR_RMSK                                                            0x80000001
#define HWIO_GCC_SPDM_RPM_CY_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SPDM_RPM_CY_CBCR_ADDR, HWIO_GCC_SPDM_RPM_CY_CBCR_RMSK)
#define HWIO_GCC_SPDM_RPM_CY_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SPDM_RPM_CY_CBCR_ADDR, m)
#define HWIO_GCC_SPDM_RPM_CY_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_SPDM_RPM_CY_CBCR_ADDR,v)
#define HWIO_GCC_SPDM_RPM_CY_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SPDM_RPM_CY_CBCR_ADDR,m,v,HWIO_GCC_SPDM_RPM_CY_CBCR_IN)
#define HWIO_GCC_SPDM_RPM_CY_CBCR_CLK_OFF_BMSK                                                    0x80000000
#define HWIO_GCC_SPDM_RPM_CY_CBCR_CLK_OFF_SHFT                                                          0x1f
#define HWIO_GCC_SPDM_RPM_CY_CBCR_CLK_ENABLE_BMSK                                                        0x1
#define HWIO_GCC_SPDM_RPM_CY_CBCR_CLK_ENABLE_SHFT                                                        0x0

#define HWIO_GCC_CRYPTO_BCR_ADDR                                                                  (GCC_CLK_CTL_REG_REG_BASE      + 0x00016000)
#define HWIO_GCC_CRYPTO_BCR_RMSK                                                                         0x1
#define HWIO_GCC_CRYPTO_BCR_IN          \
        in_dword_masked(HWIO_GCC_CRYPTO_BCR_ADDR, HWIO_GCC_CRYPTO_BCR_RMSK)
#define HWIO_GCC_CRYPTO_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_CRYPTO_BCR_ADDR, m)
#define HWIO_GCC_CRYPTO_BCR_OUT(v)      \
        out_dword(HWIO_GCC_CRYPTO_BCR_ADDR,v)
#define HWIO_GCC_CRYPTO_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_CRYPTO_BCR_ADDR,m,v,HWIO_GCC_CRYPTO_BCR_IN)
#define HWIO_GCC_CRYPTO_BCR_BLK_ARES_BMSK                                                                0x1
#define HWIO_GCC_CRYPTO_BCR_BLK_ARES_SHFT                                                                0x0

#define HWIO_GCC_CRYPTO_CMD_RCGR_ADDR                                                             (GCC_CLK_CTL_REG_REG_BASE      + 0x00016004)
#define HWIO_GCC_CRYPTO_CMD_RCGR_RMSK                                                             0x80000013
#define HWIO_GCC_CRYPTO_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_CRYPTO_CMD_RCGR_ADDR, HWIO_GCC_CRYPTO_CMD_RCGR_RMSK)
#define HWIO_GCC_CRYPTO_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_CRYPTO_CMD_RCGR_ADDR, m)
#define HWIO_GCC_CRYPTO_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_CRYPTO_CMD_RCGR_ADDR,v)
#define HWIO_GCC_CRYPTO_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_CRYPTO_CMD_RCGR_ADDR,m,v,HWIO_GCC_CRYPTO_CMD_RCGR_IN)
#define HWIO_GCC_CRYPTO_CMD_RCGR_ROOT_OFF_BMSK                                                    0x80000000
#define HWIO_GCC_CRYPTO_CMD_RCGR_ROOT_OFF_SHFT                                                          0x1f
#define HWIO_GCC_CRYPTO_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                                    0x10
#define HWIO_GCC_CRYPTO_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                                     0x4
#define HWIO_GCC_CRYPTO_CMD_RCGR_ROOT_EN_BMSK                                                            0x2
#define HWIO_GCC_CRYPTO_CMD_RCGR_ROOT_EN_SHFT                                                            0x1
#define HWIO_GCC_CRYPTO_CMD_RCGR_UPDATE_BMSK                                                             0x1
#define HWIO_GCC_CRYPTO_CMD_RCGR_UPDATE_SHFT                                                             0x0

#define HWIO_GCC_CRYPTO_CFG_RCGR_ADDR                                                             (GCC_CLK_CTL_REG_REG_BASE      + 0x00016008)
#define HWIO_GCC_CRYPTO_CFG_RCGR_RMSK                                                                  0x71f
#define HWIO_GCC_CRYPTO_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_CRYPTO_CFG_RCGR_ADDR, HWIO_GCC_CRYPTO_CFG_RCGR_RMSK)
#define HWIO_GCC_CRYPTO_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_CRYPTO_CFG_RCGR_ADDR, m)
#define HWIO_GCC_CRYPTO_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_CRYPTO_CFG_RCGR_ADDR,v)
#define HWIO_GCC_CRYPTO_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_CRYPTO_CFG_RCGR_ADDR,m,v,HWIO_GCC_CRYPTO_CFG_RCGR_IN)
#define HWIO_GCC_CRYPTO_CFG_RCGR_SRC_SEL_BMSK                                                          0x700
#define HWIO_GCC_CRYPTO_CFG_RCGR_SRC_SEL_SHFT                                                            0x8
#define HWIO_GCC_CRYPTO_CFG_RCGR_SRC_DIV_BMSK                                                           0x1f
#define HWIO_GCC_CRYPTO_CFG_RCGR_SRC_DIV_SHFT                                                            0x0

#define HWIO_GCC_CRYPTO_CBCR_ADDR                                                                 (GCC_CLK_CTL_REG_REG_BASE      + 0x0001601c)
#define HWIO_GCC_CRYPTO_CBCR_RMSK                                                                 0x80007ff0
#define HWIO_GCC_CRYPTO_CBCR_IN          \
        in_dword_masked(HWIO_GCC_CRYPTO_CBCR_ADDR, HWIO_GCC_CRYPTO_CBCR_RMSK)
#define HWIO_GCC_CRYPTO_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_CRYPTO_CBCR_ADDR, m)
#define HWIO_GCC_CRYPTO_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_CRYPTO_CBCR_ADDR,v)
#define HWIO_GCC_CRYPTO_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_CRYPTO_CBCR_ADDR,m,v,HWIO_GCC_CRYPTO_CBCR_IN)
#define HWIO_GCC_CRYPTO_CBCR_CLK_OFF_BMSK                                                         0x80000000
#define HWIO_GCC_CRYPTO_CBCR_CLK_OFF_SHFT                                                               0x1f
#define HWIO_GCC_CRYPTO_CBCR_FORCE_MEM_CORE_ON_BMSK                                                   0x4000
#define HWIO_GCC_CRYPTO_CBCR_FORCE_MEM_CORE_ON_SHFT                                                      0xe
#define HWIO_GCC_CRYPTO_CBCR_FORCE_MEM_PERIPH_ON_BMSK                                                 0x2000
#define HWIO_GCC_CRYPTO_CBCR_FORCE_MEM_PERIPH_ON_SHFT                                                    0xd
#define HWIO_GCC_CRYPTO_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                                                0x1000
#define HWIO_GCC_CRYPTO_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                                                   0xc
#define HWIO_GCC_CRYPTO_CBCR_WAKEUP_BMSK                                                               0xf00
#define HWIO_GCC_CRYPTO_CBCR_WAKEUP_SHFT                                                                 0x8
#define HWIO_GCC_CRYPTO_CBCR_SLEEP_BMSK                                                                 0xf0
#define HWIO_GCC_CRYPTO_CBCR_SLEEP_SHFT                                                                  0x4

#define HWIO_GCC_CRYPTO_AXI_CBCR_ADDR                                                             (GCC_CLK_CTL_REG_REG_BASE      + 0x00016020)
#define HWIO_GCC_CRYPTO_AXI_CBCR_RMSK                                                             0x80000000
#define HWIO_GCC_CRYPTO_AXI_CBCR_IN          \
        in_dword_masked(HWIO_GCC_CRYPTO_AXI_CBCR_ADDR, HWIO_GCC_CRYPTO_AXI_CBCR_RMSK)
#define HWIO_GCC_CRYPTO_AXI_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_CRYPTO_AXI_CBCR_ADDR, m)
#define HWIO_GCC_CRYPTO_AXI_CBCR_CLK_OFF_BMSK                                                     0x80000000
#define HWIO_GCC_CRYPTO_AXI_CBCR_CLK_OFF_SHFT                                                           0x1f

#define HWIO_GCC_CRYPTO_AHB_CBCR_ADDR                                                             (GCC_CLK_CTL_REG_REG_BASE      + 0x00016024)
#define HWIO_GCC_CRYPTO_AHB_CBCR_RMSK                                                             0xf0008000
#define HWIO_GCC_CRYPTO_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_CRYPTO_AHB_CBCR_ADDR, HWIO_GCC_CRYPTO_AHB_CBCR_RMSK)
#define HWIO_GCC_CRYPTO_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_CRYPTO_AHB_CBCR_ADDR, m)
#define HWIO_GCC_CRYPTO_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_CRYPTO_AHB_CBCR_ADDR,v)
#define HWIO_GCC_CRYPTO_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_CRYPTO_AHB_CBCR_ADDR,m,v,HWIO_GCC_CRYPTO_AHB_CBCR_IN)
#define HWIO_GCC_CRYPTO_AHB_CBCR_CLK_OFF_BMSK                                                     0x80000000
#define HWIO_GCC_CRYPTO_AHB_CBCR_CLK_OFF_SHFT                                                           0x1f
#define HWIO_GCC_CRYPTO_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                    0x70000000
#define HWIO_GCC_CRYPTO_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                          0x1c
#define HWIO_GCC_CRYPTO_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                            0x8000
#define HWIO_GCC_CRYPTO_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                               0xf

#define HWIO_GCC_GCC_AHB_CBCR_ADDR                                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x00030014)
#define HWIO_GCC_GCC_AHB_CBCR_RMSK                                                                0x80000001
#define HWIO_GCC_GCC_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_GCC_AHB_CBCR_ADDR, HWIO_GCC_GCC_AHB_CBCR_RMSK)
#define HWIO_GCC_GCC_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_GCC_AHB_CBCR_ADDR, m)
#define HWIO_GCC_GCC_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_GCC_AHB_CBCR_ADDR,v)
#define HWIO_GCC_GCC_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GCC_AHB_CBCR_ADDR,m,v,HWIO_GCC_GCC_AHB_CBCR_IN)
#define HWIO_GCC_GCC_AHB_CBCR_CLK_OFF_BMSK                                                        0x80000000
#define HWIO_GCC_GCC_AHB_CBCR_CLK_OFF_SHFT                                                              0x1f
#define HWIO_GCC_GCC_AHB_CBCR_CLK_ENABLE_BMSK                                                            0x1
#define HWIO_GCC_GCC_AHB_CBCR_CLK_ENABLE_SHFT                                                            0x0

#define HWIO_GCC_GCC_XO_CMD_RCGR_ADDR                                                             (GCC_CLK_CTL_REG_REG_BASE      + 0x00030018)
#define HWIO_GCC_GCC_XO_CMD_RCGR_RMSK                                                             0x80000002
#define HWIO_GCC_GCC_XO_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_GCC_XO_CMD_RCGR_ADDR, HWIO_GCC_GCC_XO_CMD_RCGR_RMSK)
#define HWIO_GCC_GCC_XO_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_GCC_XO_CMD_RCGR_ADDR, m)
#define HWIO_GCC_GCC_XO_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_GCC_XO_CMD_RCGR_ADDR,v)
#define HWIO_GCC_GCC_XO_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GCC_XO_CMD_RCGR_ADDR,m,v,HWIO_GCC_GCC_XO_CMD_RCGR_IN)
#define HWIO_GCC_GCC_XO_CMD_RCGR_ROOT_OFF_BMSK                                                    0x80000000
#define HWIO_GCC_GCC_XO_CMD_RCGR_ROOT_OFF_SHFT                                                          0x1f
#define HWIO_GCC_GCC_XO_CMD_RCGR_ROOT_EN_BMSK                                                            0x2
#define HWIO_GCC_GCC_XO_CMD_RCGR_ROOT_EN_SHFT                                                            0x1

#define HWIO_GCC_GCC_XO_CBCR_ADDR                                                                 (GCC_CLK_CTL_REG_REG_BASE      + 0x00030030)
#define HWIO_GCC_GCC_XO_CBCR_RMSK                                                                 0x80000001
#define HWIO_GCC_GCC_XO_CBCR_IN          \
        in_dword_masked(HWIO_GCC_GCC_XO_CBCR_ADDR, HWIO_GCC_GCC_XO_CBCR_RMSK)
#define HWIO_GCC_GCC_XO_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_GCC_XO_CBCR_ADDR, m)
#define HWIO_GCC_GCC_XO_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_GCC_XO_CBCR_ADDR,v)
#define HWIO_GCC_GCC_XO_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GCC_XO_CBCR_ADDR,m,v,HWIO_GCC_GCC_XO_CBCR_IN)
#define HWIO_GCC_GCC_XO_CBCR_CLK_OFF_BMSK                                                         0x80000000
#define HWIO_GCC_GCC_XO_CBCR_CLK_OFF_SHFT                                                               0x1f
#define HWIO_GCC_GCC_XO_CBCR_CLK_ENABLE_BMSK                                                             0x1
#define HWIO_GCC_GCC_XO_CBCR_CLK_ENABLE_SHFT                                                             0x0

#define HWIO_GCC_GCC_XO_DIV4_CBCR_ADDR                                                            (GCC_CLK_CTL_REG_REG_BASE      + 0x00030034)
#define HWIO_GCC_GCC_XO_DIV4_CBCR_RMSK                                                            0x80000001
#define HWIO_GCC_GCC_XO_DIV4_CBCR_IN          \
        in_dword_masked(HWIO_GCC_GCC_XO_DIV4_CBCR_ADDR, HWIO_GCC_GCC_XO_DIV4_CBCR_RMSK)
#define HWIO_GCC_GCC_XO_DIV4_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_GCC_XO_DIV4_CBCR_ADDR, m)
#define HWIO_GCC_GCC_XO_DIV4_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_GCC_XO_DIV4_CBCR_ADDR,v)
#define HWIO_GCC_GCC_XO_DIV4_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GCC_XO_DIV4_CBCR_ADDR,m,v,HWIO_GCC_GCC_XO_DIV4_CBCR_IN)
#define HWIO_GCC_GCC_XO_DIV4_CBCR_CLK_OFF_BMSK                                                    0x80000000
#define HWIO_GCC_GCC_XO_DIV4_CBCR_CLK_OFF_SHFT                                                          0x1f
#define HWIO_GCC_GCC_XO_DIV4_CBCR_CLK_ENABLE_BMSK                                                        0x1
#define HWIO_GCC_GCC_XO_DIV4_CBCR_CLK_ENABLE_SHFT                                                        0x0

#define HWIO_GCC_GCC_IM_SLEEP_CBCR_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x00030038)
#define HWIO_GCC_GCC_IM_SLEEP_CBCR_RMSK                                                           0x80000001
#define HWIO_GCC_GCC_IM_SLEEP_CBCR_IN          \
        in_dword_masked(HWIO_GCC_GCC_IM_SLEEP_CBCR_ADDR, HWIO_GCC_GCC_IM_SLEEP_CBCR_RMSK)
#define HWIO_GCC_GCC_IM_SLEEP_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_GCC_IM_SLEEP_CBCR_ADDR, m)
#define HWIO_GCC_GCC_IM_SLEEP_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_GCC_IM_SLEEP_CBCR_ADDR,v)
#define HWIO_GCC_GCC_IM_SLEEP_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GCC_IM_SLEEP_CBCR_ADDR,m,v,HWIO_GCC_GCC_IM_SLEEP_CBCR_IN)
#define HWIO_GCC_GCC_IM_SLEEP_CBCR_CLK_OFF_BMSK                                                   0x80000000
#define HWIO_GCC_GCC_IM_SLEEP_CBCR_CLK_OFF_SHFT                                                         0x1f
#define HWIO_GCC_GCC_IM_SLEEP_CBCR_CLK_ENABLE_BMSK                                                       0x1
#define HWIO_GCC_GCC_IM_SLEEP_CBCR_CLK_ENABLE_SHFT                                                       0x0

#define HWIO_GCC_BIMC_BCR_ADDR                                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x00031000)
#define HWIO_GCC_BIMC_BCR_RMSK                                                                           0x1
#define HWIO_GCC_BIMC_BCR_IN          \
        in_dword_masked(HWIO_GCC_BIMC_BCR_ADDR, HWIO_GCC_BIMC_BCR_RMSK)
#define HWIO_GCC_BIMC_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BIMC_BCR_ADDR, m)
#define HWIO_GCC_BIMC_BCR_OUT(v)      \
        out_dword(HWIO_GCC_BIMC_BCR_ADDR,v)
#define HWIO_GCC_BIMC_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BIMC_BCR_ADDR,m,v,HWIO_GCC_BIMC_BCR_IN)
#define HWIO_GCC_BIMC_BCR_BLK_ARES_BMSK                                                                  0x1
#define HWIO_GCC_BIMC_BCR_BLK_ARES_SHFT                                                                  0x0

#define HWIO_GCC_BIMC_GDSCR_ADDR                                                                  (GCC_CLK_CTL_REG_REG_BASE      + 0x00031004)
#define HWIO_GCC_BIMC_GDSCR_RMSK                                                                  0xf8ffffff
#define HWIO_GCC_BIMC_GDSCR_IN          \
        in_dword_masked(HWIO_GCC_BIMC_GDSCR_ADDR, HWIO_GCC_BIMC_GDSCR_RMSK)
#define HWIO_GCC_BIMC_GDSCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BIMC_GDSCR_ADDR, m)
#define HWIO_GCC_BIMC_GDSCR_OUT(v)      \
        out_dword(HWIO_GCC_BIMC_GDSCR_ADDR,v)
#define HWIO_GCC_BIMC_GDSCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BIMC_GDSCR_ADDR,m,v,HWIO_GCC_BIMC_GDSCR_IN)
#define HWIO_GCC_BIMC_GDSCR_PWR_ON_BMSK                                                           0x80000000
#define HWIO_GCC_BIMC_GDSCR_PWR_ON_SHFT                                                                 0x1f
#define HWIO_GCC_BIMC_GDSCR_GDSC_STATE_BMSK                                                       0x78000000
#define HWIO_GCC_BIMC_GDSCR_GDSC_STATE_SHFT                                                             0x1b
#define HWIO_GCC_BIMC_GDSCR_EN_REST_WAIT_BMSK                                                       0xf00000
#define HWIO_GCC_BIMC_GDSCR_EN_REST_WAIT_SHFT                                                           0x14
#define HWIO_GCC_BIMC_GDSCR_EN_FEW_WAIT_BMSK                                                         0xf0000
#define HWIO_GCC_BIMC_GDSCR_EN_FEW_WAIT_SHFT                                                            0x10
#define HWIO_GCC_BIMC_GDSCR_CLK_DIS_WAIT_BMSK                                                         0xf000
#define HWIO_GCC_BIMC_GDSCR_CLK_DIS_WAIT_SHFT                                                            0xc
#define HWIO_GCC_BIMC_GDSCR_RETAIN_FF_ENABLE_BMSK                                                      0x800
#define HWIO_GCC_BIMC_GDSCR_RETAIN_FF_ENABLE_SHFT                                                        0xb
#define HWIO_GCC_BIMC_GDSCR_RESTORE_BMSK                                                               0x400
#define HWIO_GCC_BIMC_GDSCR_RESTORE_SHFT                                                                 0xa
#define HWIO_GCC_BIMC_GDSCR_SAVE_BMSK                                                                  0x200
#define HWIO_GCC_BIMC_GDSCR_SAVE_SHFT                                                                    0x9
#define HWIO_GCC_BIMC_GDSCR_RETAIN_BMSK                                                                0x100
#define HWIO_GCC_BIMC_GDSCR_RETAIN_SHFT                                                                  0x8
#define HWIO_GCC_BIMC_GDSCR_EN_REST_BMSK                                                                0x80
#define HWIO_GCC_BIMC_GDSCR_EN_REST_SHFT                                                                 0x7
#define HWIO_GCC_BIMC_GDSCR_EN_FEW_BMSK                                                                 0x40
#define HWIO_GCC_BIMC_GDSCR_EN_FEW_SHFT                                                                  0x6
#define HWIO_GCC_BIMC_GDSCR_CLAMP_IO_BMSK                                                               0x20
#define HWIO_GCC_BIMC_GDSCR_CLAMP_IO_SHFT                                                                0x5
#define HWIO_GCC_BIMC_GDSCR_CLK_DISABLE_BMSK                                                            0x10
#define HWIO_GCC_BIMC_GDSCR_CLK_DISABLE_SHFT                                                             0x4
#define HWIO_GCC_BIMC_GDSCR_PD_ARES_BMSK                                                                 0x8
#define HWIO_GCC_BIMC_GDSCR_PD_ARES_SHFT                                                                 0x3
#define HWIO_GCC_BIMC_GDSCR_SW_OVERRIDE_BMSK                                                             0x4
#define HWIO_GCC_BIMC_GDSCR_SW_OVERRIDE_SHFT                                                             0x2
#define HWIO_GCC_BIMC_GDSCR_HW_CONTROL_BMSK                                                              0x2
#define HWIO_GCC_BIMC_GDSCR_HW_CONTROL_SHFT                                                              0x1
#define HWIO_GCC_BIMC_GDSCR_SW_COLLAPSE_BMSK                                                             0x1
#define HWIO_GCC_BIMC_GDSCR_SW_COLLAPSE_SHFT                                                             0x0

#define HWIO_GCC_BIMC_DDR_XO_CMD_RCGR_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x00032000)
#define HWIO_GCC_BIMC_DDR_XO_CMD_RCGR_RMSK                                                        0x80000002
#define HWIO_GCC_BIMC_DDR_XO_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_BIMC_DDR_XO_CMD_RCGR_ADDR, HWIO_GCC_BIMC_DDR_XO_CMD_RCGR_RMSK)
#define HWIO_GCC_BIMC_DDR_XO_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_BIMC_DDR_XO_CMD_RCGR_ADDR, m)
#define HWIO_GCC_BIMC_DDR_XO_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_BIMC_DDR_XO_CMD_RCGR_ADDR,v)
#define HWIO_GCC_BIMC_DDR_XO_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BIMC_DDR_XO_CMD_RCGR_ADDR,m,v,HWIO_GCC_BIMC_DDR_XO_CMD_RCGR_IN)
#define HWIO_GCC_BIMC_DDR_XO_CMD_RCGR_ROOT_OFF_BMSK                                               0x80000000
#define HWIO_GCC_BIMC_DDR_XO_CMD_RCGR_ROOT_OFF_SHFT                                                     0x1f
#define HWIO_GCC_BIMC_DDR_XO_CMD_RCGR_ROOT_EN_BMSK                                                       0x2
#define HWIO_GCC_BIMC_DDR_XO_CMD_RCGR_ROOT_EN_SHFT                                                       0x1

#define HWIO_GCC_BIMC_XO_CBCR_ADDR                                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x00031008)
#define HWIO_GCC_BIMC_XO_CBCR_RMSK                                                                0x80000001
#define HWIO_GCC_BIMC_XO_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BIMC_XO_CBCR_ADDR, HWIO_GCC_BIMC_XO_CBCR_RMSK)
#define HWIO_GCC_BIMC_XO_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BIMC_XO_CBCR_ADDR, m)
#define HWIO_GCC_BIMC_XO_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_BIMC_XO_CBCR_ADDR,v)
#define HWIO_GCC_BIMC_XO_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BIMC_XO_CBCR_ADDR,m,v,HWIO_GCC_BIMC_XO_CBCR_IN)
#define HWIO_GCC_BIMC_XO_CBCR_CLK_OFF_BMSK                                                        0x80000000
#define HWIO_GCC_BIMC_XO_CBCR_CLK_OFF_SHFT                                                              0x1f
#define HWIO_GCC_BIMC_XO_CBCR_CLK_ENABLE_BMSK                                                            0x1
#define HWIO_GCC_BIMC_XO_CBCR_CLK_ENABLE_SHFT                                                            0x0

#define HWIO_GCC_BIMC_CFG_AHB_CBCR_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x0003100c)
#define HWIO_GCC_BIMC_CFG_AHB_CBCR_RMSK                                                           0xf0008001
#define HWIO_GCC_BIMC_CFG_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BIMC_CFG_AHB_CBCR_ADDR, HWIO_GCC_BIMC_CFG_AHB_CBCR_RMSK)
#define HWIO_GCC_BIMC_CFG_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BIMC_CFG_AHB_CBCR_ADDR, m)
#define HWIO_GCC_BIMC_CFG_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_BIMC_CFG_AHB_CBCR_ADDR,v)
#define HWIO_GCC_BIMC_CFG_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BIMC_CFG_AHB_CBCR_ADDR,m,v,HWIO_GCC_BIMC_CFG_AHB_CBCR_IN)
#define HWIO_GCC_BIMC_CFG_AHB_CBCR_CLK_OFF_BMSK                                                   0x80000000
#define HWIO_GCC_BIMC_CFG_AHB_CBCR_CLK_OFF_SHFT                                                         0x1f
#define HWIO_GCC_BIMC_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                  0x70000000
#define HWIO_GCC_BIMC_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                        0x1c
#define HWIO_GCC_BIMC_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                          0x8000
#define HWIO_GCC_BIMC_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                             0xf
#define HWIO_GCC_BIMC_CFG_AHB_CBCR_CLK_ENABLE_BMSK                                                       0x1
#define HWIO_GCC_BIMC_CFG_AHB_CBCR_CLK_ENABLE_SHFT                                                       0x0

#define HWIO_GCC_BIMC_SLEEP_CBCR_ADDR                                                             (GCC_CLK_CTL_REG_REG_BASE      + 0x00031010)
#define HWIO_GCC_BIMC_SLEEP_CBCR_RMSK                                                             0x80000001
#define HWIO_GCC_BIMC_SLEEP_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BIMC_SLEEP_CBCR_ADDR, HWIO_GCC_BIMC_SLEEP_CBCR_RMSK)
#define HWIO_GCC_BIMC_SLEEP_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BIMC_SLEEP_CBCR_ADDR, m)
#define HWIO_GCC_BIMC_SLEEP_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_BIMC_SLEEP_CBCR_ADDR,v)
#define HWIO_GCC_BIMC_SLEEP_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BIMC_SLEEP_CBCR_ADDR,m,v,HWIO_GCC_BIMC_SLEEP_CBCR_IN)
#define HWIO_GCC_BIMC_SLEEP_CBCR_CLK_OFF_BMSK                                                     0x80000000
#define HWIO_GCC_BIMC_SLEEP_CBCR_CLK_OFF_SHFT                                                           0x1f
#define HWIO_GCC_BIMC_SLEEP_CBCR_CLK_ENABLE_BMSK                                                         0x1
#define HWIO_GCC_BIMC_SLEEP_CBCR_CLK_ENABLE_SHFT                                                         0x0

#define HWIO_GCC_BIMC_SYSNOC_AXI_CBCR_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x00031014)
#define HWIO_GCC_BIMC_SYSNOC_AXI_CBCR_RMSK                                                        0xf0008001
#define HWIO_GCC_BIMC_SYSNOC_AXI_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BIMC_SYSNOC_AXI_CBCR_ADDR, HWIO_GCC_BIMC_SYSNOC_AXI_CBCR_RMSK)
#define HWIO_GCC_BIMC_SYSNOC_AXI_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BIMC_SYSNOC_AXI_CBCR_ADDR, m)
#define HWIO_GCC_BIMC_SYSNOC_AXI_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_BIMC_SYSNOC_AXI_CBCR_ADDR,v)
#define HWIO_GCC_BIMC_SYSNOC_AXI_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BIMC_SYSNOC_AXI_CBCR_ADDR,m,v,HWIO_GCC_BIMC_SYSNOC_AXI_CBCR_IN)
#define HWIO_GCC_BIMC_SYSNOC_AXI_CBCR_CLK_OFF_BMSK                                                0x80000000
#define HWIO_GCC_BIMC_SYSNOC_AXI_CBCR_CLK_OFF_SHFT                                                      0x1f
#define HWIO_GCC_BIMC_SYSNOC_AXI_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                               0x70000000
#define HWIO_GCC_BIMC_SYSNOC_AXI_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                     0x1c
#define HWIO_GCC_BIMC_SYSNOC_AXI_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                       0x8000
#define HWIO_GCC_BIMC_SYSNOC_AXI_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                          0xf
#define HWIO_GCC_BIMC_SYSNOC_AXI_CBCR_CLK_ENABLE_BMSK                                                    0x1
#define HWIO_GCC_BIMC_SYSNOC_AXI_CBCR_CLK_ENABLE_SHFT                                                    0x0

#define HWIO_GCC_BIMC_DDR_CMD_RCGR_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x00032004)
#define HWIO_GCC_BIMC_DDR_CMD_RCGR_RMSK                                                           0x80000013
#define HWIO_GCC_BIMC_DDR_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_BIMC_DDR_CMD_RCGR_ADDR, HWIO_GCC_BIMC_DDR_CMD_RCGR_RMSK)
#define HWIO_GCC_BIMC_DDR_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_BIMC_DDR_CMD_RCGR_ADDR, m)
#define HWIO_GCC_BIMC_DDR_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_BIMC_DDR_CMD_RCGR_ADDR,v)
#define HWIO_GCC_BIMC_DDR_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BIMC_DDR_CMD_RCGR_ADDR,m,v,HWIO_GCC_BIMC_DDR_CMD_RCGR_IN)
#define HWIO_GCC_BIMC_DDR_CMD_RCGR_ROOT_OFF_BMSK                                                  0x80000000
#define HWIO_GCC_BIMC_DDR_CMD_RCGR_ROOT_OFF_SHFT                                                        0x1f
#define HWIO_GCC_BIMC_DDR_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                                  0x10
#define HWIO_GCC_BIMC_DDR_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                                   0x4
#define HWIO_GCC_BIMC_DDR_CMD_RCGR_ROOT_EN_BMSK                                                          0x2
#define HWIO_GCC_BIMC_DDR_CMD_RCGR_ROOT_EN_SHFT                                                          0x1
#define HWIO_GCC_BIMC_DDR_CMD_RCGR_UPDATE_BMSK                                                           0x1
#define HWIO_GCC_BIMC_DDR_CMD_RCGR_UPDATE_SHFT                                                           0x0

#define HWIO_GCC_BIMC_DDR_CFG_RCGR_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x00032008)
#define HWIO_GCC_BIMC_DDR_CFG_RCGR_RMSK                                                                0x71f
#define HWIO_GCC_BIMC_DDR_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_BIMC_DDR_CFG_RCGR_ADDR, HWIO_GCC_BIMC_DDR_CFG_RCGR_RMSK)
#define HWIO_GCC_BIMC_DDR_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_BIMC_DDR_CFG_RCGR_ADDR, m)
#define HWIO_GCC_BIMC_DDR_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_BIMC_DDR_CFG_RCGR_ADDR,v)
#define HWIO_GCC_BIMC_DDR_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BIMC_DDR_CFG_RCGR_ADDR,m,v,HWIO_GCC_BIMC_DDR_CFG_RCGR_IN)
#define HWIO_GCC_BIMC_DDR_CFG_RCGR_SRC_SEL_BMSK                                                        0x700
#define HWIO_GCC_BIMC_DDR_CFG_RCGR_SRC_SEL_SHFT                                                          0x8
#define HWIO_GCC_BIMC_DDR_CFG_RCGR_SRC_DIV_BMSK                                                         0x1f
#define HWIO_GCC_BIMC_DDR_CFG_RCGR_SRC_DIV_SHFT                                                          0x0

#define HWIO_GCC_BIMC_MISC_ADDR                                                                   (GCC_CLK_CTL_REG_REG_BASE      + 0x00031018)
#define HWIO_GCC_BIMC_MISC_RMSK                                                                     0x3f0f00
#define HWIO_GCC_BIMC_MISC_IN          \
        in_dword_masked(HWIO_GCC_BIMC_MISC_ADDR, HWIO_GCC_BIMC_MISC_RMSK)
#define HWIO_GCC_BIMC_MISC_INM(m)      \
        in_dword_masked(HWIO_GCC_BIMC_MISC_ADDR, m)
#define HWIO_GCC_BIMC_MISC_OUT(v)      \
        out_dword(HWIO_GCC_BIMC_MISC_ADDR,v)
#define HWIO_GCC_BIMC_MISC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BIMC_MISC_ADDR,m,v,HWIO_GCC_BIMC_MISC_IN)
#define HWIO_GCC_BIMC_MISC_BIMC_DDR_FRQSW_FSM_STATUS_BMSK                                           0x3f0000
#define HWIO_GCC_BIMC_MISC_BIMC_DDR_FRQSW_FSM_STATUS_SHFT                                               0x10
#define HWIO_GCC_BIMC_MISC_BIMC_FSM_CLK_GATE_DIS_BMSK                                                  0x800
#define HWIO_GCC_BIMC_MISC_BIMC_FSM_CLK_GATE_DIS_SHFT                                                    0xb
#define HWIO_GCC_BIMC_MISC_BIMC_DDR_LEGACY_2X_MODE_EN_BMSK                                             0x400
#define HWIO_GCC_BIMC_MISC_BIMC_DDR_LEGACY_2X_MODE_EN_SHFT                                               0xa
#define HWIO_GCC_BIMC_MISC_BIMC_FSM_DIS_DDR_UPDATE_BMSK                                                0x200
#define HWIO_GCC_BIMC_MISC_BIMC_FSM_DIS_DDR_UPDATE_SHFT                                                  0x9
#define HWIO_GCC_BIMC_MISC_BIMC_FRQSW_FSM_DIS_BMSK                                                     0x100
#define HWIO_GCC_BIMC_MISC_BIMC_FRQSW_FSM_DIS_SHFT                                                       0x8

#define HWIO_GCC_BIMC_CBCR_ADDR                                                                   (GCC_CLK_CTL_REG_REG_BASE      + 0x0003101c)
#define HWIO_GCC_BIMC_CBCR_RMSK                                                                   0x80000001
#define HWIO_GCC_BIMC_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BIMC_CBCR_ADDR, HWIO_GCC_BIMC_CBCR_RMSK)
#define HWIO_GCC_BIMC_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BIMC_CBCR_ADDR, m)
#define HWIO_GCC_BIMC_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_BIMC_CBCR_ADDR,v)
#define HWIO_GCC_BIMC_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BIMC_CBCR_ADDR,m,v,HWIO_GCC_BIMC_CBCR_IN)
#define HWIO_GCC_BIMC_CBCR_CLK_OFF_BMSK                                                           0x80000000
#define HWIO_GCC_BIMC_CBCR_CLK_OFF_SHFT                                                                 0x1f
#define HWIO_GCC_BIMC_CBCR_CLK_ENABLE_BMSK                                                               0x1
#define HWIO_GCC_BIMC_CBCR_CLK_ENABLE_SHFT                                                               0x0

#define HWIO_GCC_BIMC_APSS_AXI_CBCR_ADDR                                                          (GCC_CLK_CTL_REG_REG_BASE      + 0x00031020)
#define HWIO_GCC_BIMC_APSS_AXI_CBCR_RMSK                                                          0x80000000
#define HWIO_GCC_BIMC_APSS_AXI_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BIMC_APSS_AXI_CBCR_ADDR, HWIO_GCC_BIMC_APSS_AXI_CBCR_RMSK)
#define HWIO_GCC_BIMC_APSS_AXI_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BIMC_APSS_AXI_CBCR_ADDR, m)
#define HWIO_GCC_BIMC_APSS_AXI_CBCR_CLK_OFF_BMSK                                                  0x80000000
#define HWIO_GCC_BIMC_APSS_AXI_CBCR_CLK_OFF_SHFT                                                        0x1f

#define HWIO_GCC_DDR_DIM_CFG_CBCR_ADDR                                                            (GCC_CLK_CTL_REG_REG_BASE      + 0x0003201c)
#define HWIO_GCC_DDR_DIM_CFG_CBCR_RMSK                                                            0xf0008001
#define HWIO_GCC_DDR_DIM_CFG_CBCR_IN          \
        in_dword_masked(HWIO_GCC_DDR_DIM_CFG_CBCR_ADDR, HWIO_GCC_DDR_DIM_CFG_CBCR_RMSK)
#define HWIO_GCC_DDR_DIM_CFG_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_DDR_DIM_CFG_CBCR_ADDR, m)
#define HWIO_GCC_DDR_DIM_CFG_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_DDR_DIM_CFG_CBCR_ADDR,v)
#define HWIO_GCC_DDR_DIM_CFG_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_DDR_DIM_CFG_CBCR_ADDR,m,v,HWIO_GCC_DDR_DIM_CFG_CBCR_IN)
#define HWIO_GCC_DDR_DIM_CFG_CBCR_CLK_OFF_BMSK                                                    0x80000000
#define HWIO_GCC_DDR_DIM_CFG_CBCR_CLK_OFF_SHFT                                                          0x1f
#define HWIO_GCC_DDR_DIM_CFG_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                   0x70000000
#define HWIO_GCC_DDR_DIM_CFG_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                         0x1c
#define HWIO_GCC_DDR_DIM_CFG_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                           0x8000
#define HWIO_GCC_DDR_DIM_CFG_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                              0xf
#define HWIO_GCC_DDR_DIM_CFG_CBCR_CLK_ENABLE_BMSK                                                        0x1
#define HWIO_GCC_DDR_DIM_CFG_CBCR_CLK_ENABLE_SHFT                                                        0x0

#define HWIO_GCC_DDR_DIM_SLEEP_CBCR_ADDR                                                          (GCC_CLK_CTL_REG_REG_BASE      + 0x00032020)
#define HWIO_GCC_DDR_DIM_SLEEP_CBCR_RMSK                                                          0x80000001
#define HWIO_GCC_DDR_DIM_SLEEP_CBCR_IN          \
        in_dword_masked(HWIO_GCC_DDR_DIM_SLEEP_CBCR_ADDR, HWIO_GCC_DDR_DIM_SLEEP_CBCR_RMSK)
#define HWIO_GCC_DDR_DIM_SLEEP_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_DDR_DIM_SLEEP_CBCR_ADDR, m)
#define HWIO_GCC_DDR_DIM_SLEEP_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_DDR_DIM_SLEEP_CBCR_ADDR,v)
#define HWIO_GCC_DDR_DIM_SLEEP_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_DDR_DIM_SLEEP_CBCR_ADDR,m,v,HWIO_GCC_DDR_DIM_SLEEP_CBCR_IN)
#define HWIO_GCC_DDR_DIM_SLEEP_CBCR_CLK_OFF_BMSK                                                  0x80000000
#define HWIO_GCC_DDR_DIM_SLEEP_CBCR_CLK_OFF_SHFT                                                        0x1f
#define HWIO_GCC_DDR_DIM_SLEEP_CBCR_CLK_ENABLE_BMSK                                                      0x1
#define HWIO_GCC_DDR_DIM_SLEEP_CBCR_CLK_ENABLE_SHFT                                                      0x0

#define HWIO_GCC_BIMC_TCU_CBCR_ADDR                                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x00031044)
#define HWIO_GCC_BIMC_TCU_CBCR_RMSK                                                               0x80000000
#define HWIO_GCC_BIMC_TCU_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BIMC_TCU_CBCR_ADDR, HWIO_GCC_BIMC_TCU_CBCR_RMSK)
#define HWIO_GCC_BIMC_TCU_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BIMC_TCU_CBCR_ADDR, m)
#define HWIO_GCC_BIMC_TCU_CBCR_CLK_OFF_BMSK                                                       0x80000000
#define HWIO_GCC_BIMC_TCU_CBCR_CLK_OFF_SHFT                                                             0x1f

#define HWIO_GCC_ULTAUDIO_PCNOC_MPORT_CBCR_ADDR                                                   (GCC_CLK_CTL_REG_REG_BASE      + 0x0001c000)
#define HWIO_GCC_ULTAUDIO_PCNOC_MPORT_CBCR_RMSK                                                   0x80000001
#define HWIO_GCC_ULTAUDIO_PCNOC_MPORT_CBCR_IN          \
        in_dword_masked(HWIO_GCC_ULTAUDIO_PCNOC_MPORT_CBCR_ADDR, HWIO_GCC_ULTAUDIO_PCNOC_MPORT_CBCR_RMSK)
#define HWIO_GCC_ULTAUDIO_PCNOC_MPORT_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_ULTAUDIO_PCNOC_MPORT_CBCR_ADDR, m)
#define HWIO_GCC_ULTAUDIO_PCNOC_MPORT_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_ULTAUDIO_PCNOC_MPORT_CBCR_ADDR,v)
#define HWIO_GCC_ULTAUDIO_PCNOC_MPORT_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ULTAUDIO_PCNOC_MPORT_CBCR_ADDR,m,v,HWIO_GCC_ULTAUDIO_PCNOC_MPORT_CBCR_IN)
#define HWIO_GCC_ULTAUDIO_PCNOC_MPORT_CBCR_CLK_OFF_BMSK                                           0x80000000
#define HWIO_GCC_ULTAUDIO_PCNOC_MPORT_CBCR_CLK_OFF_SHFT                                                 0x1f
#define HWIO_GCC_ULTAUDIO_PCNOC_MPORT_CBCR_CLK_ENABLE_BMSK                                               0x1
#define HWIO_GCC_ULTAUDIO_PCNOC_MPORT_CBCR_CLK_ENABLE_SHFT                                               0x0

#define HWIO_GCC_ULTAUDIO_PCNOC_SWAY_CBCR_ADDR                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x0001c004)
#define HWIO_GCC_ULTAUDIO_PCNOC_SWAY_CBCR_RMSK                                                    0xf0008001
#define HWIO_GCC_ULTAUDIO_PCNOC_SWAY_CBCR_IN          \
        in_dword_masked(HWIO_GCC_ULTAUDIO_PCNOC_SWAY_CBCR_ADDR, HWIO_GCC_ULTAUDIO_PCNOC_SWAY_CBCR_RMSK)
#define HWIO_GCC_ULTAUDIO_PCNOC_SWAY_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_ULTAUDIO_PCNOC_SWAY_CBCR_ADDR, m)
#define HWIO_GCC_ULTAUDIO_PCNOC_SWAY_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_ULTAUDIO_PCNOC_SWAY_CBCR_ADDR,v)
#define HWIO_GCC_ULTAUDIO_PCNOC_SWAY_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ULTAUDIO_PCNOC_SWAY_CBCR_ADDR,m,v,HWIO_GCC_ULTAUDIO_PCNOC_SWAY_CBCR_IN)
#define HWIO_GCC_ULTAUDIO_PCNOC_SWAY_CBCR_CLK_OFF_BMSK                                            0x80000000
#define HWIO_GCC_ULTAUDIO_PCNOC_SWAY_CBCR_CLK_OFF_SHFT                                                  0x1f
#define HWIO_GCC_ULTAUDIO_PCNOC_SWAY_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                           0x70000000
#define HWIO_GCC_ULTAUDIO_PCNOC_SWAY_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                 0x1c
#define HWIO_GCC_ULTAUDIO_PCNOC_SWAY_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                   0x8000
#define HWIO_GCC_ULTAUDIO_PCNOC_SWAY_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                      0xf
#define HWIO_GCC_ULTAUDIO_PCNOC_SWAY_CBCR_CLK_ENABLE_BMSK                                                0x1
#define HWIO_GCC_ULTAUDIO_PCNOC_SWAY_CBCR_CLK_ENABLE_SHFT                                                0x0

#define HWIO_GCC_ULT_AUDIO_BCR_ADDR                                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x0001c0b4)
#define HWIO_GCC_ULT_AUDIO_BCR_RMSK                                                                      0x1
#define HWIO_GCC_ULT_AUDIO_BCR_IN          \
        in_dword_masked(HWIO_GCC_ULT_AUDIO_BCR_ADDR, HWIO_GCC_ULT_AUDIO_BCR_RMSK)
#define HWIO_GCC_ULT_AUDIO_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_ULT_AUDIO_BCR_ADDR, m)
#define HWIO_GCC_ULT_AUDIO_BCR_OUT(v)      \
        out_dword(HWIO_GCC_ULT_AUDIO_BCR_ADDR,v)
#define HWIO_GCC_ULT_AUDIO_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ULT_AUDIO_BCR_ADDR,m,v,HWIO_GCC_ULT_AUDIO_BCR_IN)
#define HWIO_GCC_ULT_AUDIO_BCR_BLK_ARES_BMSK                                                             0x1
#define HWIO_GCC_ULT_AUDIO_BCR_BLK_ARES_SHFT                                                             0x0

#define HWIO_GCC_APSS_AHB_CMD_RCGR_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x00046000)
#define HWIO_GCC_APSS_AHB_CMD_RCGR_RMSK                                                           0x80000013
#define HWIO_GCC_APSS_AHB_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_APSS_AHB_CMD_RCGR_ADDR, HWIO_GCC_APSS_AHB_CMD_RCGR_RMSK)
#define HWIO_GCC_APSS_AHB_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_APSS_AHB_CMD_RCGR_ADDR, m)
#define HWIO_GCC_APSS_AHB_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_APSS_AHB_CMD_RCGR_ADDR,v)
#define HWIO_GCC_APSS_AHB_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_APSS_AHB_CMD_RCGR_ADDR,m,v,HWIO_GCC_APSS_AHB_CMD_RCGR_IN)
#define HWIO_GCC_APSS_AHB_CMD_RCGR_ROOT_OFF_BMSK                                                  0x80000000
#define HWIO_GCC_APSS_AHB_CMD_RCGR_ROOT_OFF_SHFT                                                        0x1f
#define HWIO_GCC_APSS_AHB_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                                  0x10
#define HWIO_GCC_APSS_AHB_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                                   0x4
#define HWIO_GCC_APSS_AHB_CMD_RCGR_ROOT_EN_BMSK                                                          0x2
#define HWIO_GCC_APSS_AHB_CMD_RCGR_ROOT_EN_SHFT                                                          0x1
#define HWIO_GCC_APSS_AHB_CMD_RCGR_UPDATE_BMSK                                                           0x1
#define HWIO_GCC_APSS_AHB_CMD_RCGR_UPDATE_SHFT                                                           0x0

#define HWIO_GCC_APSS_AHB_CFG_RCGR_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x00046004)
#define HWIO_GCC_APSS_AHB_CFG_RCGR_RMSK                                                                0x71f
#define HWIO_GCC_APSS_AHB_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_APSS_AHB_CFG_RCGR_ADDR, HWIO_GCC_APSS_AHB_CFG_RCGR_RMSK)
#define HWIO_GCC_APSS_AHB_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_APSS_AHB_CFG_RCGR_ADDR, m)
#define HWIO_GCC_APSS_AHB_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_APSS_AHB_CFG_RCGR_ADDR,v)
#define HWIO_GCC_APSS_AHB_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_APSS_AHB_CFG_RCGR_ADDR,m,v,HWIO_GCC_APSS_AHB_CFG_RCGR_IN)
#define HWIO_GCC_APSS_AHB_CFG_RCGR_SRC_SEL_BMSK                                                        0x700
#define HWIO_GCC_APSS_AHB_CFG_RCGR_SRC_SEL_SHFT                                                          0x8
#define HWIO_GCC_APSS_AHB_CFG_RCGR_SRC_DIV_BMSK                                                         0x1f
#define HWIO_GCC_APSS_AHB_CFG_RCGR_SRC_DIV_SHFT                                                          0x0

#define HWIO_GCC_APSS_AHB_MISC_ADDR                                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x00046018)
#define HWIO_GCC_APSS_AHB_MISC_RMSK                                                                     0xf1
#define HWIO_GCC_APSS_AHB_MISC_IN          \
        in_dword_masked(HWIO_GCC_APSS_AHB_MISC_ADDR, HWIO_GCC_APSS_AHB_MISC_RMSK)
#define HWIO_GCC_APSS_AHB_MISC_INM(m)      \
        in_dword_masked(HWIO_GCC_APSS_AHB_MISC_ADDR, m)
#define HWIO_GCC_APSS_AHB_MISC_OUT(v)      \
        out_dword(HWIO_GCC_APSS_AHB_MISC_ADDR,v)
#define HWIO_GCC_APSS_AHB_MISC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_APSS_AHB_MISC_ADDR,m,v,HWIO_GCC_APSS_AHB_MISC_IN)
#define HWIO_GCC_APSS_AHB_MISC_APSS_AHB_CLK_AUTO_SCALE_DIV_BMSK                                         0xf0
#define HWIO_GCC_APSS_AHB_MISC_APSS_AHB_CLK_AUTO_SCALE_DIV_SHFT                                          0x4
#define HWIO_GCC_APSS_AHB_MISC_APSS_AHB_CLK_AUTO_SCALE_DIS_BMSK                                          0x1
#define HWIO_GCC_APSS_AHB_MISC_APSS_AHB_CLK_AUTO_SCALE_DIS_SHFT                                          0x0

#define HWIO_GCC_APSS_AHB_CBCR_ADDR                                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x0004601c)
#define HWIO_GCC_APSS_AHB_CBCR_RMSK                                                               0xf0008000
#define HWIO_GCC_APSS_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_APSS_AHB_CBCR_ADDR, HWIO_GCC_APSS_AHB_CBCR_RMSK)
#define HWIO_GCC_APSS_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_APSS_AHB_CBCR_ADDR, m)
#define HWIO_GCC_APSS_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_APSS_AHB_CBCR_ADDR,v)
#define HWIO_GCC_APSS_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_APSS_AHB_CBCR_ADDR,m,v,HWIO_GCC_APSS_AHB_CBCR_IN)
#define HWIO_GCC_APSS_AHB_CBCR_CLK_OFF_BMSK                                                       0x80000000
#define HWIO_GCC_APSS_AHB_CBCR_CLK_OFF_SHFT                                                             0x1f
#define HWIO_GCC_APSS_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                      0x70000000
#define HWIO_GCC_APSS_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                            0x1c
#define HWIO_GCC_APSS_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                              0x8000
#define HWIO_GCC_APSS_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                                 0xf

#define HWIO_GCC_APSS_AXI_CBCR_ADDR                                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x00046020)
#define HWIO_GCC_APSS_AXI_CBCR_RMSK                                                               0x80000000
#define HWIO_GCC_APSS_AXI_CBCR_IN          \
        in_dword_masked(HWIO_GCC_APSS_AXI_CBCR_ADDR, HWIO_GCC_APSS_AXI_CBCR_RMSK)
#define HWIO_GCC_APSS_AXI_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_APSS_AXI_CBCR_ADDR, m)
#define HWIO_GCC_APSS_AXI_CBCR_CLK_OFF_BMSK                                                       0x80000000
#define HWIO_GCC_APSS_AXI_CBCR_CLK_OFF_SHFT                                                             0x1f

#define HWIO_GCC_SNOC_BUS_TIMEOUT0_BCR_ADDR                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x00047000)
#define HWIO_GCC_SNOC_BUS_TIMEOUT0_BCR_RMSK                                                              0x1
#define HWIO_GCC_SNOC_BUS_TIMEOUT0_BCR_IN          \
        in_dword_masked(HWIO_GCC_SNOC_BUS_TIMEOUT0_BCR_ADDR, HWIO_GCC_SNOC_BUS_TIMEOUT0_BCR_RMSK)
#define HWIO_GCC_SNOC_BUS_TIMEOUT0_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SNOC_BUS_TIMEOUT0_BCR_ADDR, m)
#define HWIO_GCC_SNOC_BUS_TIMEOUT0_BCR_OUT(v)      \
        out_dword(HWIO_GCC_SNOC_BUS_TIMEOUT0_BCR_ADDR,v)
#define HWIO_GCC_SNOC_BUS_TIMEOUT0_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SNOC_BUS_TIMEOUT0_BCR_ADDR,m,v,HWIO_GCC_SNOC_BUS_TIMEOUT0_BCR_IN)
#define HWIO_GCC_SNOC_BUS_TIMEOUT0_BCR_BLK_ARES_BMSK                                                     0x1
#define HWIO_GCC_SNOC_BUS_TIMEOUT0_BCR_BLK_ARES_SHFT                                                     0x0

#define HWIO_GCC_SNOC_BUS_TIMEOUT0_AHB_CBCR_ADDR                                                  (GCC_CLK_CTL_REG_REG_BASE      + 0x00047004)
#define HWIO_GCC_SNOC_BUS_TIMEOUT0_AHB_CBCR_RMSK                                                  0x80000001
#define HWIO_GCC_SNOC_BUS_TIMEOUT0_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SNOC_BUS_TIMEOUT0_AHB_CBCR_ADDR, HWIO_GCC_SNOC_BUS_TIMEOUT0_AHB_CBCR_RMSK)
#define HWIO_GCC_SNOC_BUS_TIMEOUT0_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SNOC_BUS_TIMEOUT0_AHB_CBCR_ADDR, m)
#define HWIO_GCC_SNOC_BUS_TIMEOUT0_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_SNOC_BUS_TIMEOUT0_AHB_CBCR_ADDR,v)
#define HWIO_GCC_SNOC_BUS_TIMEOUT0_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SNOC_BUS_TIMEOUT0_AHB_CBCR_ADDR,m,v,HWIO_GCC_SNOC_BUS_TIMEOUT0_AHB_CBCR_IN)
#define HWIO_GCC_SNOC_BUS_TIMEOUT0_AHB_CBCR_CLK_OFF_BMSK                                          0x80000000
#define HWIO_GCC_SNOC_BUS_TIMEOUT0_AHB_CBCR_CLK_OFF_SHFT                                                0x1f
#define HWIO_GCC_SNOC_BUS_TIMEOUT0_AHB_CBCR_CLK_ENABLE_BMSK                                              0x1
#define HWIO_GCC_SNOC_BUS_TIMEOUT0_AHB_CBCR_CLK_ENABLE_SHFT                                              0x0

#define HWIO_GCC_PCNOC_BUS_TIMEOUT0_BCR_ADDR                                                      (GCC_CLK_CTL_REG_REG_BASE      + 0x00048000)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT0_BCR_RMSK                                                             0x1
#define HWIO_GCC_PCNOC_BUS_TIMEOUT0_BCR_IN          \
        in_dword_masked(HWIO_GCC_PCNOC_BUS_TIMEOUT0_BCR_ADDR, HWIO_GCC_PCNOC_BUS_TIMEOUT0_BCR_RMSK)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT0_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PCNOC_BUS_TIMEOUT0_BCR_ADDR, m)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT0_BCR_OUT(v)      \
        out_dword(HWIO_GCC_PCNOC_BUS_TIMEOUT0_BCR_ADDR,v)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT0_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PCNOC_BUS_TIMEOUT0_BCR_ADDR,m,v,HWIO_GCC_PCNOC_BUS_TIMEOUT0_BCR_IN)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT0_BCR_BLK_ARES_BMSK                                                    0x1
#define HWIO_GCC_PCNOC_BUS_TIMEOUT0_BCR_BLK_ARES_SHFT                                                    0x0

#define HWIO_GCC_PCNOC_BUS_TIMEOUT0_AHB_CBCR_ADDR                                                 (GCC_CLK_CTL_REG_REG_BASE      + 0x00048004)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT0_AHB_CBCR_RMSK                                                 0x80000001
#define HWIO_GCC_PCNOC_BUS_TIMEOUT0_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_PCNOC_BUS_TIMEOUT0_AHB_CBCR_ADDR, HWIO_GCC_PCNOC_BUS_TIMEOUT0_AHB_CBCR_RMSK)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT0_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PCNOC_BUS_TIMEOUT0_AHB_CBCR_ADDR, m)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT0_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_PCNOC_BUS_TIMEOUT0_AHB_CBCR_ADDR,v)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT0_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PCNOC_BUS_TIMEOUT0_AHB_CBCR_ADDR,m,v,HWIO_GCC_PCNOC_BUS_TIMEOUT0_AHB_CBCR_IN)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT0_AHB_CBCR_CLK_OFF_BMSK                                         0x80000000
#define HWIO_GCC_PCNOC_BUS_TIMEOUT0_AHB_CBCR_CLK_OFF_SHFT                                               0x1f
#define HWIO_GCC_PCNOC_BUS_TIMEOUT0_AHB_CBCR_CLK_ENABLE_BMSK                                             0x1
#define HWIO_GCC_PCNOC_BUS_TIMEOUT0_AHB_CBCR_CLK_ENABLE_SHFT                                             0x0

#define HWIO_GCC_PCNOC_BUS_TIMEOUT1_BCR_ADDR                                                      (GCC_CLK_CTL_REG_REG_BASE      + 0x00048008)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT1_BCR_RMSK                                                             0x1
#define HWIO_GCC_PCNOC_BUS_TIMEOUT1_BCR_IN          \
        in_dword_masked(HWIO_GCC_PCNOC_BUS_TIMEOUT1_BCR_ADDR, HWIO_GCC_PCNOC_BUS_TIMEOUT1_BCR_RMSK)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT1_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PCNOC_BUS_TIMEOUT1_BCR_ADDR, m)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT1_BCR_OUT(v)      \
        out_dword(HWIO_GCC_PCNOC_BUS_TIMEOUT1_BCR_ADDR,v)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT1_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PCNOC_BUS_TIMEOUT1_BCR_ADDR,m,v,HWIO_GCC_PCNOC_BUS_TIMEOUT1_BCR_IN)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT1_BCR_BLK_ARES_BMSK                                                    0x1
#define HWIO_GCC_PCNOC_BUS_TIMEOUT1_BCR_BLK_ARES_SHFT                                                    0x0

#define HWIO_GCC_PCNOC_BUS_TIMEOUT1_AHB_CBCR_ADDR                                                 (GCC_CLK_CTL_REG_REG_BASE      + 0x0004800c)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT1_AHB_CBCR_RMSK                                                 0x80000001
#define HWIO_GCC_PCNOC_BUS_TIMEOUT1_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_PCNOC_BUS_TIMEOUT1_AHB_CBCR_ADDR, HWIO_GCC_PCNOC_BUS_TIMEOUT1_AHB_CBCR_RMSK)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT1_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PCNOC_BUS_TIMEOUT1_AHB_CBCR_ADDR, m)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT1_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_PCNOC_BUS_TIMEOUT1_AHB_CBCR_ADDR,v)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT1_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PCNOC_BUS_TIMEOUT1_AHB_CBCR_ADDR,m,v,HWIO_GCC_PCNOC_BUS_TIMEOUT1_AHB_CBCR_IN)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT1_AHB_CBCR_CLK_OFF_BMSK                                         0x80000000
#define HWIO_GCC_PCNOC_BUS_TIMEOUT1_AHB_CBCR_CLK_OFF_SHFT                                               0x1f
#define HWIO_GCC_PCNOC_BUS_TIMEOUT1_AHB_CBCR_CLK_ENABLE_BMSK                                             0x1
#define HWIO_GCC_PCNOC_BUS_TIMEOUT1_AHB_CBCR_CLK_ENABLE_SHFT                                             0x0

#define HWIO_GCC_PCNOC_BUS_TIMEOUT2_BCR_ADDR                                                      (GCC_CLK_CTL_REG_REG_BASE      + 0x00048010)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT2_BCR_RMSK                                                             0x1
#define HWIO_GCC_PCNOC_BUS_TIMEOUT2_BCR_IN          \
        in_dword_masked(HWIO_GCC_PCNOC_BUS_TIMEOUT2_BCR_ADDR, HWIO_GCC_PCNOC_BUS_TIMEOUT2_BCR_RMSK)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT2_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PCNOC_BUS_TIMEOUT2_BCR_ADDR, m)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT2_BCR_OUT(v)      \
        out_dword(HWIO_GCC_PCNOC_BUS_TIMEOUT2_BCR_ADDR,v)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT2_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PCNOC_BUS_TIMEOUT2_BCR_ADDR,m,v,HWIO_GCC_PCNOC_BUS_TIMEOUT2_BCR_IN)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT2_BCR_BLK_ARES_BMSK                                                    0x1
#define HWIO_GCC_PCNOC_BUS_TIMEOUT2_BCR_BLK_ARES_SHFT                                                    0x0

#define HWIO_GCC_PCNOC_BUS_TIMEOUT2_AHB_CBCR_ADDR                                                 (GCC_CLK_CTL_REG_REG_BASE      + 0x00048014)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT2_AHB_CBCR_RMSK                                                 0x80000001
#define HWIO_GCC_PCNOC_BUS_TIMEOUT2_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_PCNOC_BUS_TIMEOUT2_AHB_CBCR_ADDR, HWIO_GCC_PCNOC_BUS_TIMEOUT2_AHB_CBCR_RMSK)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT2_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PCNOC_BUS_TIMEOUT2_AHB_CBCR_ADDR, m)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT2_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_PCNOC_BUS_TIMEOUT2_AHB_CBCR_ADDR,v)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT2_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PCNOC_BUS_TIMEOUT2_AHB_CBCR_ADDR,m,v,HWIO_GCC_PCNOC_BUS_TIMEOUT2_AHB_CBCR_IN)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT2_AHB_CBCR_CLK_OFF_BMSK                                         0x80000000
#define HWIO_GCC_PCNOC_BUS_TIMEOUT2_AHB_CBCR_CLK_OFF_SHFT                                               0x1f
#define HWIO_GCC_PCNOC_BUS_TIMEOUT2_AHB_CBCR_CLK_ENABLE_BMSK                                             0x1
#define HWIO_GCC_PCNOC_BUS_TIMEOUT2_AHB_CBCR_CLK_ENABLE_SHFT                                             0x0

#define HWIO_GCC_PCNOC_BUS_TIMEOUT3_BCR_ADDR                                                      (GCC_CLK_CTL_REG_REG_BASE      + 0x00048018)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT3_BCR_RMSK                                                             0x1
#define HWIO_GCC_PCNOC_BUS_TIMEOUT3_BCR_IN          \
        in_dword_masked(HWIO_GCC_PCNOC_BUS_TIMEOUT3_BCR_ADDR, HWIO_GCC_PCNOC_BUS_TIMEOUT3_BCR_RMSK)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT3_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PCNOC_BUS_TIMEOUT3_BCR_ADDR, m)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT3_BCR_OUT(v)      \
        out_dword(HWIO_GCC_PCNOC_BUS_TIMEOUT3_BCR_ADDR,v)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT3_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PCNOC_BUS_TIMEOUT3_BCR_ADDR,m,v,HWIO_GCC_PCNOC_BUS_TIMEOUT3_BCR_IN)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT3_BCR_BLK_ARES_BMSK                                                    0x1
#define HWIO_GCC_PCNOC_BUS_TIMEOUT3_BCR_BLK_ARES_SHFT                                                    0x0

#define HWIO_GCC_PCNOC_BUS_TIMEOUT3_AHB_CBCR_ADDR                                                 (GCC_CLK_CTL_REG_REG_BASE      + 0x0004801c)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT3_AHB_CBCR_RMSK                                                 0x80000001
#define HWIO_GCC_PCNOC_BUS_TIMEOUT3_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_PCNOC_BUS_TIMEOUT3_AHB_CBCR_ADDR, HWIO_GCC_PCNOC_BUS_TIMEOUT3_AHB_CBCR_RMSK)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT3_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PCNOC_BUS_TIMEOUT3_AHB_CBCR_ADDR, m)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT3_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_PCNOC_BUS_TIMEOUT3_AHB_CBCR_ADDR,v)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT3_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PCNOC_BUS_TIMEOUT3_AHB_CBCR_ADDR,m,v,HWIO_GCC_PCNOC_BUS_TIMEOUT3_AHB_CBCR_IN)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT3_AHB_CBCR_CLK_OFF_BMSK                                         0x80000000
#define HWIO_GCC_PCNOC_BUS_TIMEOUT3_AHB_CBCR_CLK_OFF_SHFT                                               0x1f
#define HWIO_GCC_PCNOC_BUS_TIMEOUT3_AHB_CBCR_CLK_ENABLE_BMSK                                             0x1
#define HWIO_GCC_PCNOC_BUS_TIMEOUT3_AHB_CBCR_CLK_ENABLE_SHFT                                             0x0

#define HWIO_GCC_PCNOC_BUS_TIMEOUT4_BCR_ADDR                                                      (GCC_CLK_CTL_REG_REG_BASE      + 0x00048020)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT4_BCR_RMSK                                                             0x1
#define HWIO_GCC_PCNOC_BUS_TIMEOUT4_BCR_IN          \
        in_dword_masked(HWIO_GCC_PCNOC_BUS_TIMEOUT4_BCR_ADDR, HWIO_GCC_PCNOC_BUS_TIMEOUT4_BCR_RMSK)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT4_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PCNOC_BUS_TIMEOUT4_BCR_ADDR, m)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT4_BCR_OUT(v)      \
        out_dword(HWIO_GCC_PCNOC_BUS_TIMEOUT4_BCR_ADDR,v)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT4_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PCNOC_BUS_TIMEOUT4_BCR_ADDR,m,v,HWIO_GCC_PCNOC_BUS_TIMEOUT4_BCR_IN)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT4_BCR_BLK_ARES_BMSK                                                    0x1
#define HWIO_GCC_PCNOC_BUS_TIMEOUT4_BCR_BLK_ARES_SHFT                                                    0x0

#define HWIO_GCC_PCNOC_BUS_TIMEOUT4_AHB_CBCR_ADDR                                                 (GCC_CLK_CTL_REG_REG_BASE      + 0x00048024)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT4_AHB_CBCR_RMSK                                                 0x80000001
#define HWIO_GCC_PCNOC_BUS_TIMEOUT4_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_PCNOC_BUS_TIMEOUT4_AHB_CBCR_ADDR, HWIO_GCC_PCNOC_BUS_TIMEOUT4_AHB_CBCR_RMSK)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT4_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PCNOC_BUS_TIMEOUT4_AHB_CBCR_ADDR, m)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT4_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_PCNOC_BUS_TIMEOUT4_AHB_CBCR_ADDR,v)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT4_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PCNOC_BUS_TIMEOUT4_AHB_CBCR_ADDR,m,v,HWIO_GCC_PCNOC_BUS_TIMEOUT4_AHB_CBCR_IN)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT4_AHB_CBCR_CLK_OFF_BMSK                                         0x80000000
#define HWIO_GCC_PCNOC_BUS_TIMEOUT4_AHB_CBCR_CLK_OFF_SHFT                                               0x1f
#define HWIO_GCC_PCNOC_BUS_TIMEOUT4_AHB_CBCR_CLK_ENABLE_BMSK                                             0x1
#define HWIO_GCC_PCNOC_BUS_TIMEOUT4_AHB_CBCR_CLK_ENABLE_SHFT                                             0x0

#define HWIO_GCC_PCNOC_BUS_TIMEOUT5_BCR_ADDR                                                      (GCC_CLK_CTL_REG_REG_BASE      + 0x00048028)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT5_BCR_RMSK                                                             0x1
#define HWIO_GCC_PCNOC_BUS_TIMEOUT5_BCR_IN          \
        in_dword_masked(HWIO_GCC_PCNOC_BUS_TIMEOUT5_BCR_ADDR, HWIO_GCC_PCNOC_BUS_TIMEOUT5_BCR_RMSK)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT5_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PCNOC_BUS_TIMEOUT5_BCR_ADDR, m)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT5_BCR_OUT(v)      \
        out_dword(HWIO_GCC_PCNOC_BUS_TIMEOUT5_BCR_ADDR,v)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT5_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PCNOC_BUS_TIMEOUT5_BCR_ADDR,m,v,HWIO_GCC_PCNOC_BUS_TIMEOUT5_BCR_IN)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT5_BCR_BLK_ARES_BMSK                                                    0x1
#define HWIO_GCC_PCNOC_BUS_TIMEOUT5_BCR_BLK_ARES_SHFT                                                    0x0

#define HWIO_GCC_PCNOC_BUS_TIMEOUT5_AHB_CBCR_ADDR                                                 (GCC_CLK_CTL_REG_REG_BASE      + 0x0004802c)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT5_AHB_CBCR_RMSK                                                 0x80000001
#define HWIO_GCC_PCNOC_BUS_TIMEOUT5_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_PCNOC_BUS_TIMEOUT5_AHB_CBCR_ADDR, HWIO_GCC_PCNOC_BUS_TIMEOUT5_AHB_CBCR_RMSK)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT5_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PCNOC_BUS_TIMEOUT5_AHB_CBCR_ADDR, m)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT5_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_PCNOC_BUS_TIMEOUT5_AHB_CBCR_ADDR,v)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT5_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PCNOC_BUS_TIMEOUT5_AHB_CBCR_ADDR,m,v,HWIO_GCC_PCNOC_BUS_TIMEOUT5_AHB_CBCR_IN)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT5_AHB_CBCR_CLK_OFF_BMSK                                         0x80000000
#define HWIO_GCC_PCNOC_BUS_TIMEOUT5_AHB_CBCR_CLK_OFF_SHFT                                               0x1f
#define HWIO_GCC_PCNOC_BUS_TIMEOUT5_AHB_CBCR_CLK_ENABLE_BMSK                                             0x1
#define HWIO_GCC_PCNOC_BUS_TIMEOUT5_AHB_CBCR_CLK_ENABLE_SHFT                                             0x0

#define HWIO_GCC_PCNOC_BUS_TIMEOUT6_BCR_ADDR                                                      (GCC_CLK_CTL_REG_REG_BASE      + 0x00048030)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT6_BCR_RMSK                                                             0x1
#define HWIO_GCC_PCNOC_BUS_TIMEOUT6_BCR_IN          \
        in_dword_masked(HWIO_GCC_PCNOC_BUS_TIMEOUT6_BCR_ADDR, HWIO_GCC_PCNOC_BUS_TIMEOUT6_BCR_RMSK)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT6_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PCNOC_BUS_TIMEOUT6_BCR_ADDR, m)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT6_BCR_OUT(v)      \
        out_dword(HWIO_GCC_PCNOC_BUS_TIMEOUT6_BCR_ADDR,v)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT6_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PCNOC_BUS_TIMEOUT6_BCR_ADDR,m,v,HWIO_GCC_PCNOC_BUS_TIMEOUT6_BCR_IN)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT6_BCR_BLK_ARES_BMSK                                                    0x1
#define HWIO_GCC_PCNOC_BUS_TIMEOUT6_BCR_BLK_ARES_SHFT                                                    0x0

#define HWIO_GCC_PCNOC_BUS_TIMEOUT6_AHB_CBCR_ADDR                                                 (GCC_CLK_CTL_REG_REG_BASE      + 0x00048034)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT6_AHB_CBCR_RMSK                                                 0x80000001
#define HWIO_GCC_PCNOC_BUS_TIMEOUT6_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_PCNOC_BUS_TIMEOUT6_AHB_CBCR_ADDR, HWIO_GCC_PCNOC_BUS_TIMEOUT6_AHB_CBCR_RMSK)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT6_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PCNOC_BUS_TIMEOUT6_AHB_CBCR_ADDR, m)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT6_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_PCNOC_BUS_TIMEOUT6_AHB_CBCR_ADDR,v)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT6_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PCNOC_BUS_TIMEOUT6_AHB_CBCR_ADDR,m,v,HWIO_GCC_PCNOC_BUS_TIMEOUT6_AHB_CBCR_IN)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT6_AHB_CBCR_CLK_OFF_BMSK                                         0x80000000
#define HWIO_GCC_PCNOC_BUS_TIMEOUT6_AHB_CBCR_CLK_OFF_SHFT                                               0x1f
#define HWIO_GCC_PCNOC_BUS_TIMEOUT6_AHB_CBCR_CLK_ENABLE_BMSK                                             0x1
#define HWIO_GCC_PCNOC_BUS_TIMEOUT6_AHB_CBCR_CLK_ENABLE_SHFT                                             0x0

#define HWIO_GCC_PCNOC_BUS_TIMEOUT7_BCR_ADDR                                                      (GCC_CLK_CTL_REG_REG_BASE      + 0x00048038)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT7_BCR_RMSK                                                             0x1
#define HWIO_GCC_PCNOC_BUS_TIMEOUT7_BCR_IN          \
        in_dword_masked(HWIO_GCC_PCNOC_BUS_TIMEOUT7_BCR_ADDR, HWIO_GCC_PCNOC_BUS_TIMEOUT7_BCR_RMSK)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT7_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PCNOC_BUS_TIMEOUT7_BCR_ADDR, m)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT7_BCR_OUT(v)      \
        out_dword(HWIO_GCC_PCNOC_BUS_TIMEOUT7_BCR_ADDR,v)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT7_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PCNOC_BUS_TIMEOUT7_BCR_ADDR,m,v,HWIO_GCC_PCNOC_BUS_TIMEOUT7_BCR_IN)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT7_BCR_BLK_ARES_BMSK                                                    0x1
#define HWIO_GCC_PCNOC_BUS_TIMEOUT7_BCR_BLK_ARES_SHFT                                                    0x0

#define HWIO_GCC_PCNOC_BUS_TIMEOUT7_AHB_CBCR_ADDR                                                 (GCC_CLK_CTL_REG_REG_BASE      + 0x0004803c)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT7_AHB_CBCR_RMSK                                                 0x80000001
#define HWIO_GCC_PCNOC_BUS_TIMEOUT7_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_PCNOC_BUS_TIMEOUT7_AHB_CBCR_ADDR, HWIO_GCC_PCNOC_BUS_TIMEOUT7_AHB_CBCR_RMSK)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT7_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PCNOC_BUS_TIMEOUT7_AHB_CBCR_ADDR, m)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT7_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_PCNOC_BUS_TIMEOUT7_AHB_CBCR_ADDR,v)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT7_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PCNOC_BUS_TIMEOUT7_AHB_CBCR_ADDR,m,v,HWIO_GCC_PCNOC_BUS_TIMEOUT7_AHB_CBCR_IN)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT7_AHB_CBCR_CLK_OFF_BMSK                                         0x80000000
#define HWIO_GCC_PCNOC_BUS_TIMEOUT7_AHB_CBCR_CLK_OFF_SHFT                                               0x1f
#define HWIO_GCC_PCNOC_BUS_TIMEOUT7_AHB_CBCR_CLK_ENABLE_BMSK                                             0x1
#define HWIO_GCC_PCNOC_BUS_TIMEOUT7_AHB_CBCR_CLK_ENABLE_SHFT                                             0x0

#define HWIO_GCC_PCNOC_BUS_TIMEOUT8_BCR_ADDR                                                      (GCC_CLK_CTL_REG_REG_BASE      + 0x00048040)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT8_BCR_RMSK                                                             0x1
#define HWIO_GCC_PCNOC_BUS_TIMEOUT8_BCR_IN          \
        in_dword_masked(HWIO_GCC_PCNOC_BUS_TIMEOUT8_BCR_ADDR, HWIO_GCC_PCNOC_BUS_TIMEOUT8_BCR_RMSK)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT8_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PCNOC_BUS_TIMEOUT8_BCR_ADDR, m)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT8_BCR_OUT(v)      \
        out_dword(HWIO_GCC_PCNOC_BUS_TIMEOUT8_BCR_ADDR,v)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT8_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PCNOC_BUS_TIMEOUT8_BCR_ADDR,m,v,HWIO_GCC_PCNOC_BUS_TIMEOUT8_BCR_IN)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT8_BCR_BLK_ARES_BMSK                                                    0x1
#define HWIO_GCC_PCNOC_BUS_TIMEOUT8_BCR_BLK_ARES_SHFT                                                    0x0

#define HWIO_GCC_PCNOC_BUS_TIMEOUT8_AHB_CBCR_ADDR                                                 (GCC_CLK_CTL_REG_REG_BASE      + 0x00048044)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT8_AHB_CBCR_RMSK                                                 0x80000001
#define HWIO_GCC_PCNOC_BUS_TIMEOUT8_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_PCNOC_BUS_TIMEOUT8_AHB_CBCR_ADDR, HWIO_GCC_PCNOC_BUS_TIMEOUT8_AHB_CBCR_RMSK)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT8_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PCNOC_BUS_TIMEOUT8_AHB_CBCR_ADDR, m)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT8_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_PCNOC_BUS_TIMEOUT8_AHB_CBCR_ADDR,v)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT8_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PCNOC_BUS_TIMEOUT8_AHB_CBCR_ADDR,m,v,HWIO_GCC_PCNOC_BUS_TIMEOUT8_AHB_CBCR_IN)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT8_AHB_CBCR_CLK_OFF_BMSK                                         0x80000000
#define HWIO_GCC_PCNOC_BUS_TIMEOUT8_AHB_CBCR_CLK_OFF_SHFT                                               0x1f
#define HWIO_GCC_PCNOC_BUS_TIMEOUT8_AHB_CBCR_CLK_ENABLE_BMSK                                             0x1
#define HWIO_GCC_PCNOC_BUS_TIMEOUT8_AHB_CBCR_CLK_ENABLE_SHFT                                             0x0

#define HWIO_GCC_PCNOC_BUS_TIMEOUT9_BCR_ADDR                                                      (GCC_CLK_CTL_REG_REG_BASE      + 0x00048048)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT9_BCR_RMSK                                                             0x1
#define HWIO_GCC_PCNOC_BUS_TIMEOUT9_BCR_IN          \
        in_dword_masked(HWIO_GCC_PCNOC_BUS_TIMEOUT9_BCR_ADDR, HWIO_GCC_PCNOC_BUS_TIMEOUT9_BCR_RMSK)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT9_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PCNOC_BUS_TIMEOUT9_BCR_ADDR, m)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT9_BCR_OUT(v)      \
        out_dword(HWIO_GCC_PCNOC_BUS_TIMEOUT9_BCR_ADDR,v)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT9_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PCNOC_BUS_TIMEOUT9_BCR_ADDR,m,v,HWIO_GCC_PCNOC_BUS_TIMEOUT9_BCR_IN)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT9_BCR_BLK_ARES_BMSK                                                    0x1
#define HWIO_GCC_PCNOC_BUS_TIMEOUT9_BCR_BLK_ARES_SHFT                                                    0x0

#define HWIO_GCC_PCNOC_BUS_TIMEOUT9_AHB_CBCR_ADDR                                                 (GCC_CLK_CTL_REG_REG_BASE      + 0x0004804c)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT9_AHB_CBCR_RMSK                                                 0x80000001
#define HWIO_GCC_PCNOC_BUS_TIMEOUT9_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_PCNOC_BUS_TIMEOUT9_AHB_CBCR_ADDR, HWIO_GCC_PCNOC_BUS_TIMEOUT9_AHB_CBCR_RMSK)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT9_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PCNOC_BUS_TIMEOUT9_AHB_CBCR_ADDR, m)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT9_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_PCNOC_BUS_TIMEOUT9_AHB_CBCR_ADDR,v)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT9_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PCNOC_BUS_TIMEOUT9_AHB_CBCR_ADDR,m,v,HWIO_GCC_PCNOC_BUS_TIMEOUT9_AHB_CBCR_IN)
#define HWIO_GCC_PCNOC_BUS_TIMEOUT9_AHB_CBCR_CLK_OFF_BMSK                                         0x80000000
#define HWIO_GCC_PCNOC_BUS_TIMEOUT9_AHB_CBCR_CLK_OFF_SHFT                                               0x1f
#define HWIO_GCC_PCNOC_BUS_TIMEOUT9_AHB_CBCR_CLK_ENABLE_BMSK                                             0x1
#define HWIO_GCC_PCNOC_BUS_TIMEOUT9_AHB_CBCR_CLK_ENABLE_SHFT                                             0x0

#define HWIO_GCC_DEHR_BCR_ADDR                                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x0001f000)
#define HWIO_GCC_DEHR_BCR_RMSK                                                                           0x1
#define HWIO_GCC_DEHR_BCR_IN          \
        in_dword_masked(HWIO_GCC_DEHR_BCR_ADDR, HWIO_GCC_DEHR_BCR_RMSK)
#define HWIO_GCC_DEHR_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_DEHR_BCR_ADDR, m)
#define HWIO_GCC_DEHR_BCR_OUT(v)      \
        out_dword(HWIO_GCC_DEHR_BCR_ADDR,v)
#define HWIO_GCC_DEHR_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_DEHR_BCR_ADDR,m,v,HWIO_GCC_DEHR_BCR_IN)
#define HWIO_GCC_DEHR_BCR_BLK_ARES_BMSK                                                                  0x1
#define HWIO_GCC_DEHR_BCR_BLK_ARES_SHFT                                                                  0x0

#define HWIO_GCC_DEHR_CBCR_ADDR                                                                   (GCC_CLK_CTL_REG_REG_BASE      + 0x0001f004)
#define HWIO_GCC_DEHR_CBCR_RMSK                                                                   0xf000fff1
#define HWIO_GCC_DEHR_CBCR_IN          \
        in_dword_masked(HWIO_GCC_DEHR_CBCR_ADDR, HWIO_GCC_DEHR_CBCR_RMSK)
#define HWIO_GCC_DEHR_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_DEHR_CBCR_ADDR, m)
#define HWIO_GCC_DEHR_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_DEHR_CBCR_ADDR,v)
#define HWIO_GCC_DEHR_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_DEHR_CBCR_ADDR,m,v,HWIO_GCC_DEHR_CBCR_IN)
#define HWIO_GCC_DEHR_CBCR_CLK_OFF_BMSK                                                           0x80000000
#define HWIO_GCC_DEHR_CBCR_CLK_OFF_SHFT                                                                 0x1f
#define HWIO_GCC_DEHR_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                          0x70000000
#define HWIO_GCC_DEHR_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                                0x1c
#define HWIO_GCC_DEHR_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                                  0x8000
#define HWIO_GCC_DEHR_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                                     0xf
#define HWIO_GCC_DEHR_CBCR_FORCE_MEM_CORE_ON_BMSK                                                     0x4000
#define HWIO_GCC_DEHR_CBCR_FORCE_MEM_CORE_ON_SHFT                                                        0xe
#define HWIO_GCC_DEHR_CBCR_FORCE_MEM_PERIPH_ON_BMSK                                                   0x2000
#define HWIO_GCC_DEHR_CBCR_FORCE_MEM_PERIPH_ON_SHFT                                                      0xd
#define HWIO_GCC_DEHR_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                                                  0x1000
#define HWIO_GCC_DEHR_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                                                     0xc
#define HWIO_GCC_DEHR_CBCR_WAKEUP_BMSK                                                                 0xf00
#define HWIO_GCC_DEHR_CBCR_WAKEUP_SHFT                                                                   0x8
#define HWIO_GCC_DEHR_CBCR_SLEEP_BMSK                                                                   0xf0
#define HWIO_GCC_DEHR_CBCR_SLEEP_SHFT                                                                    0x4
#define HWIO_GCC_DEHR_CBCR_CLK_ENABLE_BMSK                                                               0x1
#define HWIO_GCC_DEHR_CBCR_CLK_ENABLE_SHFT                                                               0x0

#define HWIO_GCC_PCNOC_MPU_CFG_AHB_CBCR_ADDR                                                      (GCC_CLK_CTL_REG_REG_BASE      + 0x0001700c)
#define HWIO_GCC_PCNOC_MPU_CFG_AHB_CBCR_RMSK                                                      0xf0008001
#define HWIO_GCC_PCNOC_MPU_CFG_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_PCNOC_MPU_CFG_AHB_CBCR_ADDR, HWIO_GCC_PCNOC_MPU_CFG_AHB_CBCR_RMSK)
#define HWIO_GCC_PCNOC_MPU_CFG_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PCNOC_MPU_CFG_AHB_CBCR_ADDR, m)
#define HWIO_GCC_PCNOC_MPU_CFG_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_PCNOC_MPU_CFG_AHB_CBCR_ADDR,v)
#define HWIO_GCC_PCNOC_MPU_CFG_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PCNOC_MPU_CFG_AHB_CBCR_ADDR,m,v,HWIO_GCC_PCNOC_MPU_CFG_AHB_CBCR_IN)
#define HWIO_GCC_PCNOC_MPU_CFG_AHB_CBCR_CLK_OFF_BMSK                                              0x80000000
#define HWIO_GCC_PCNOC_MPU_CFG_AHB_CBCR_CLK_OFF_SHFT                                                    0x1f
#define HWIO_GCC_PCNOC_MPU_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                             0x70000000
#define HWIO_GCC_PCNOC_MPU_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                   0x1c
#define HWIO_GCC_PCNOC_MPU_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                     0x8000
#define HWIO_GCC_PCNOC_MPU_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                        0xf
#define HWIO_GCC_PCNOC_MPU_CFG_AHB_CBCR_CLK_ENABLE_BMSK                                                  0x1
#define HWIO_GCC_PCNOC_MPU_CFG_AHB_CBCR_CLK_ENABLE_SHFT                                                  0x0

#define HWIO_GCC_APSS_TCU_CBCR_ADDR                                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x00012018)
#define HWIO_GCC_APSS_TCU_CBCR_RMSK                                                               0x80007ff0
#define HWIO_GCC_APSS_TCU_CBCR_IN          \
        in_dword_masked(HWIO_GCC_APSS_TCU_CBCR_ADDR, HWIO_GCC_APSS_TCU_CBCR_RMSK)
#define HWIO_GCC_APSS_TCU_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_APSS_TCU_CBCR_ADDR, m)
#define HWIO_GCC_APSS_TCU_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_APSS_TCU_CBCR_ADDR,v)
#define HWIO_GCC_APSS_TCU_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_APSS_TCU_CBCR_ADDR,m,v,HWIO_GCC_APSS_TCU_CBCR_IN)
#define HWIO_GCC_APSS_TCU_CBCR_CLK_OFF_BMSK                                                       0x80000000
#define HWIO_GCC_APSS_TCU_CBCR_CLK_OFF_SHFT                                                             0x1f
#define HWIO_GCC_APSS_TCU_CBCR_FORCE_MEM_CORE_ON_BMSK                                                 0x4000
#define HWIO_GCC_APSS_TCU_CBCR_FORCE_MEM_CORE_ON_SHFT                                                    0xe
#define HWIO_GCC_APSS_TCU_CBCR_FORCE_MEM_PERIPH_ON_BMSK                                               0x2000
#define HWIO_GCC_APSS_TCU_CBCR_FORCE_MEM_PERIPH_ON_SHFT                                                  0xd
#define HWIO_GCC_APSS_TCU_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                                              0x1000
#define HWIO_GCC_APSS_TCU_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                                                 0xc
#define HWIO_GCC_APSS_TCU_CBCR_WAKEUP_BMSK                                                             0xf00
#define HWIO_GCC_APSS_TCU_CBCR_WAKEUP_SHFT                                                               0x8
#define HWIO_GCC_APSS_TCU_CBCR_SLEEP_BMSK                                                               0xf0
#define HWIO_GCC_APSS_TCU_CBCR_SLEEP_SHFT                                                                0x4

#define HWIO_GCC_MSS_TBU_AXI_CBCR_ADDR                                                            (GCC_CLK_CTL_REG_REG_BASE      + 0x00012024)
#define HWIO_GCC_MSS_TBU_AXI_CBCR_RMSK                                                            0x80000000
#define HWIO_GCC_MSS_TBU_AXI_CBCR_IN          \
        in_dword_masked(HWIO_GCC_MSS_TBU_AXI_CBCR_ADDR, HWIO_GCC_MSS_TBU_AXI_CBCR_RMSK)
#define HWIO_GCC_MSS_TBU_AXI_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_MSS_TBU_AXI_CBCR_ADDR, m)
#define HWIO_GCC_MSS_TBU_AXI_CBCR_CLK_OFF_BMSK                                                    0x80000000
#define HWIO_GCC_MSS_TBU_AXI_CBCR_CLK_OFF_SHFT                                                          0x1f

#define HWIO_GCC_MSS_TBU_Q6_AXI_CBCR_ADDR                                                         (GCC_CLK_CTL_REG_REG_BASE      + 0x0001202c)
#define HWIO_GCC_MSS_TBU_Q6_AXI_CBCR_RMSK                                                         0x80007ff0
#define HWIO_GCC_MSS_TBU_Q6_AXI_CBCR_IN          \
        in_dword_masked(HWIO_GCC_MSS_TBU_Q6_AXI_CBCR_ADDR, HWIO_GCC_MSS_TBU_Q6_AXI_CBCR_RMSK)
#define HWIO_GCC_MSS_TBU_Q6_AXI_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_MSS_TBU_Q6_AXI_CBCR_ADDR, m)
#define HWIO_GCC_MSS_TBU_Q6_AXI_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_MSS_TBU_Q6_AXI_CBCR_ADDR,v)
#define HWIO_GCC_MSS_TBU_Q6_AXI_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_MSS_TBU_Q6_AXI_CBCR_ADDR,m,v,HWIO_GCC_MSS_TBU_Q6_AXI_CBCR_IN)
#define HWIO_GCC_MSS_TBU_Q6_AXI_CBCR_CLK_OFF_BMSK                                                 0x80000000
#define HWIO_GCC_MSS_TBU_Q6_AXI_CBCR_CLK_OFF_SHFT                                                       0x1f
#define HWIO_GCC_MSS_TBU_Q6_AXI_CBCR_FORCE_MEM_CORE_ON_BMSK                                           0x4000
#define HWIO_GCC_MSS_TBU_Q6_AXI_CBCR_FORCE_MEM_CORE_ON_SHFT                                              0xe
#define HWIO_GCC_MSS_TBU_Q6_AXI_CBCR_FORCE_MEM_PERIPH_ON_BMSK                                         0x2000
#define HWIO_GCC_MSS_TBU_Q6_AXI_CBCR_FORCE_MEM_PERIPH_ON_SHFT                                            0xd
#define HWIO_GCC_MSS_TBU_Q6_AXI_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                                        0x1000
#define HWIO_GCC_MSS_TBU_Q6_AXI_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                                           0xc
#define HWIO_GCC_MSS_TBU_Q6_AXI_CBCR_WAKEUP_BMSK                                                       0xf00
#define HWIO_GCC_MSS_TBU_Q6_AXI_CBCR_WAKEUP_SHFT                                                         0x8
#define HWIO_GCC_MSS_TBU_Q6_AXI_CBCR_SLEEP_BMSK                                                         0xf0
#define HWIO_GCC_MSS_TBU_Q6_AXI_CBCR_SLEEP_SHFT                                                          0x4

#define HWIO_GCC_PCNOC_TBU_CBCR_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x00012030)
#define HWIO_GCC_PCNOC_TBU_CBCR_RMSK                                                              0x80007ff0
#define HWIO_GCC_PCNOC_TBU_CBCR_IN          \
        in_dword_masked(HWIO_GCC_PCNOC_TBU_CBCR_ADDR, HWIO_GCC_PCNOC_TBU_CBCR_RMSK)
#define HWIO_GCC_PCNOC_TBU_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PCNOC_TBU_CBCR_ADDR, m)
#define HWIO_GCC_PCNOC_TBU_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_PCNOC_TBU_CBCR_ADDR,v)
#define HWIO_GCC_PCNOC_TBU_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PCNOC_TBU_CBCR_ADDR,m,v,HWIO_GCC_PCNOC_TBU_CBCR_IN)
#define HWIO_GCC_PCNOC_TBU_CBCR_CLK_OFF_BMSK                                                      0x80000000
#define HWIO_GCC_PCNOC_TBU_CBCR_CLK_OFF_SHFT                                                            0x1f
#define HWIO_GCC_PCNOC_TBU_CBCR_FORCE_MEM_CORE_ON_BMSK                                                0x4000
#define HWIO_GCC_PCNOC_TBU_CBCR_FORCE_MEM_CORE_ON_SHFT                                                   0xe
#define HWIO_GCC_PCNOC_TBU_CBCR_FORCE_MEM_PERIPH_ON_BMSK                                              0x2000
#define HWIO_GCC_PCNOC_TBU_CBCR_FORCE_MEM_PERIPH_ON_SHFT                                                 0xd
#define HWIO_GCC_PCNOC_TBU_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                                             0x1000
#define HWIO_GCC_PCNOC_TBU_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                                                0xc
#define HWIO_GCC_PCNOC_TBU_CBCR_WAKEUP_BMSK                                                            0xf00
#define HWIO_GCC_PCNOC_TBU_CBCR_WAKEUP_SHFT                                                              0x8
#define HWIO_GCC_PCNOC_TBU_CBCR_SLEEP_BMSK                                                              0xf0
#define HWIO_GCC_PCNOC_TBU_CBCR_SLEEP_SHFT                                                               0x4

#define HWIO_GCC_SMMU_CFG_CBCR_ADDR                                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x00012038)
#define HWIO_GCC_SMMU_CFG_CBCR_RMSK                                                               0xf0008000
#define HWIO_GCC_SMMU_CFG_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SMMU_CFG_CBCR_ADDR, HWIO_GCC_SMMU_CFG_CBCR_RMSK)
#define HWIO_GCC_SMMU_CFG_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SMMU_CFG_CBCR_ADDR, m)
#define HWIO_GCC_SMMU_CFG_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_SMMU_CFG_CBCR_ADDR,v)
#define HWIO_GCC_SMMU_CFG_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SMMU_CFG_CBCR_ADDR,m,v,HWIO_GCC_SMMU_CFG_CBCR_IN)
#define HWIO_GCC_SMMU_CFG_CBCR_CLK_OFF_BMSK                                                       0x80000000
#define HWIO_GCC_SMMU_CFG_CBCR_CLK_OFF_SHFT                                                             0x1f
#define HWIO_GCC_SMMU_CFG_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                      0x70000000
#define HWIO_GCC_SMMU_CFG_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                            0x1c
#define HWIO_GCC_SMMU_CFG_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                              0x8000
#define HWIO_GCC_SMMU_CFG_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                                 0xf

#define HWIO_GCC_SMMU_XPU_CBCR_ADDR                                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x00017010)
#define HWIO_GCC_SMMU_XPU_CBCR_RMSK                                                               0xf0008001
#define HWIO_GCC_SMMU_XPU_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SMMU_XPU_CBCR_ADDR, HWIO_GCC_SMMU_XPU_CBCR_RMSK)
#define HWIO_GCC_SMMU_XPU_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SMMU_XPU_CBCR_ADDR, m)
#define HWIO_GCC_SMMU_XPU_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_SMMU_XPU_CBCR_ADDR,v)
#define HWIO_GCC_SMMU_XPU_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SMMU_XPU_CBCR_ADDR,m,v,HWIO_GCC_SMMU_XPU_CBCR_IN)
#define HWIO_GCC_SMMU_XPU_CBCR_CLK_OFF_BMSK                                                       0x80000000
#define HWIO_GCC_SMMU_XPU_CBCR_CLK_OFF_SHFT                                                             0x1f
#define HWIO_GCC_SMMU_XPU_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                      0x70000000
#define HWIO_GCC_SMMU_XPU_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                            0x1c
#define HWIO_GCC_SMMU_XPU_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                              0x8000
#define HWIO_GCC_SMMU_XPU_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                                 0xf
#define HWIO_GCC_SMMU_XPU_CBCR_CLK_ENABLE_BMSK                                                           0x1
#define HWIO_GCC_SMMU_XPU_CBCR_CLK_ENABLE_SHFT                                                           0x0

#define HWIO_GCC_SMMU_CATS64_CBCR_ADDR                                                            (GCC_CLK_CTL_REG_REG_BASE      + 0x0007c004)
#define HWIO_GCC_SMMU_CATS64_CBCR_RMSK                                                            0xf0008001
#define HWIO_GCC_SMMU_CATS64_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SMMU_CATS64_CBCR_ADDR, HWIO_GCC_SMMU_CATS64_CBCR_RMSK)
#define HWIO_GCC_SMMU_CATS64_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SMMU_CATS64_CBCR_ADDR, m)
#define HWIO_GCC_SMMU_CATS64_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_SMMU_CATS64_CBCR_ADDR,v)
#define HWIO_GCC_SMMU_CATS64_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SMMU_CATS64_CBCR_ADDR,m,v,HWIO_GCC_SMMU_CATS64_CBCR_IN)
#define HWIO_GCC_SMMU_CATS64_CBCR_CLK_OFF_BMSK                                                    0x80000000
#define HWIO_GCC_SMMU_CATS64_CBCR_CLK_OFF_SHFT                                                          0x1f
#define HWIO_GCC_SMMU_CATS64_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                   0x70000000
#define HWIO_GCC_SMMU_CATS64_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                         0x1c
#define HWIO_GCC_SMMU_CATS64_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                           0x8000
#define HWIO_GCC_SMMU_CATS64_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                              0xf
#define HWIO_GCC_SMMU_CATS64_CBCR_CLK_ENABLE_BMSK                                                        0x1
#define HWIO_GCC_SMMU_CATS64_CBCR_CLK_ENABLE_SHFT                                                        0x0

#define HWIO_GCC_SMMU_CATS_BCR_ADDR                                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x0007c000)
#define HWIO_GCC_SMMU_CATS_BCR_RMSK                                                                      0x7
#define HWIO_GCC_SMMU_CATS_BCR_IN          \
        in_dword_masked(HWIO_GCC_SMMU_CATS_BCR_ADDR, HWIO_GCC_SMMU_CATS_BCR_RMSK)
#define HWIO_GCC_SMMU_CATS_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SMMU_CATS_BCR_ADDR, m)
#define HWIO_GCC_SMMU_CATS_BCR_OUT(v)      \
        out_dword(HWIO_GCC_SMMU_CATS_BCR_ADDR,v)
#define HWIO_GCC_SMMU_CATS_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SMMU_CATS_BCR_ADDR,m,v,HWIO_GCC_SMMU_CATS_BCR_IN)
#define HWIO_GCC_SMMU_CATS_BCR_CATS128_BLK_ARES_BMSK                                                     0x4
#define HWIO_GCC_SMMU_CATS_BCR_CATS128_BLK_ARES_SHFT                                                     0x2
#define HWIO_GCC_SMMU_CATS_BCR_CATS64_BLK_ARES_BMSK                                                      0x2
#define HWIO_GCC_SMMU_CATS_BCR_CATS64_BLK_ARES_SHFT                                                      0x1
#define HWIO_GCC_SMMU_CATS_BCR_BLK_ARES_BMSK                                                             0x1
#define HWIO_GCC_SMMU_CATS_BCR_BLK_ARES_SHFT                                                             0x0

#define HWIO_GCC_SMMU_BCR_ADDR                                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x00012000)
#define HWIO_GCC_SMMU_BCR_RMSK                                                                           0x1
#define HWIO_GCC_SMMU_BCR_IN          \
        in_dword_masked(HWIO_GCC_SMMU_BCR_ADDR, HWIO_GCC_SMMU_BCR_RMSK)
#define HWIO_GCC_SMMU_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SMMU_BCR_ADDR, m)
#define HWIO_GCC_SMMU_BCR_OUT(v)      \
        out_dword(HWIO_GCC_SMMU_BCR_ADDR,v)
#define HWIO_GCC_SMMU_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SMMU_BCR_ADDR,m,v,HWIO_GCC_SMMU_BCR_IN)
#define HWIO_GCC_SMMU_BCR_BLK_ARES_BMSK                                                                  0x1
#define HWIO_GCC_SMMU_BCR_BLK_ARES_SHFT                                                                  0x0

#define HWIO_GCC_APSS_TCU_BCR_ADDR                                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x00012050)
#define HWIO_GCC_APSS_TCU_BCR_RMSK                                                                       0x1
#define HWIO_GCC_APSS_TCU_BCR_IN          \
        in_dword_masked(HWIO_GCC_APSS_TCU_BCR_ADDR, HWIO_GCC_APSS_TCU_BCR_RMSK)
#define HWIO_GCC_APSS_TCU_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_APSS_TCU_BCR_ADDR, m)
#define HWIO_GCC_APSS_TCU_BCR_OUT(v)      \
        out_dword(HWIO_GCC_APSS_TCU_BCR_ADDR,v)
#define HWIO_GCC_APSS_TCU_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_APSS_TCU_BCR_ADDR,m,v,HWIO_GCC_APSS_TCU_BCR_IN)
#define HWIO_GCC_APSS_TCU_BCR_BLK_ARES_BMSK                                                              0x1
#define HWIO_GCC_APSS_TCU_BCR_BLK_ARES_SHFT                                                              0x0

#define HWIO_GCC_MSS_TBU_AXI_BCR_ADDR                                                             (GCC_CLK_CTL_REG_REG_BASE      + 0x00065000)
#define HWIO_GCC_MSS_TBU_AXI_BCR_RMSK                                                                    0x1
#define HWIO_GCC_MSS_TBU_AXI_BCR_IN          \
        in_dword_masked(HWIO_GCC_MSS_TBU_AXI_BCR_ADDR, HWIO_GCC_MSS_TBU_AXI_BCR_RMSK)
#define HWIO_GCC_MSS_TBU_AXI_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_MSS_TBU_AXI_BCR_ADDR, m)
#define HWIO_GCC_MSS_TBU_AXI_BCR_OUT(v)      \
        out_dword(HWIO_GCC_MSS_TBU_AXI_BCR_ADDR,v)
#define HWIO_GCC_MSS_TBU_AXI_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_MSS_TBU_AXI_BCR_ADDR,m,v,HWIO_GCC_MSS_TBU_AXI_BCR_IN)
#define HWIO_GCC_MSS_TBU_AXI_BCR_BLK_ARES_BMSK                                                           0x1
#define HWIO_GCC_MSS_TBU_AXI_BCR_BLK_ARES_SHFT                                                           0x0

#define HWIO_GCC_MSS_TBU_Q6_AXI_BCR_ADDR                                                          (GCC_CLK_CTL_REG_REG_BASE      + 0x00067000)
#define HWIO_GCC_MSS_TBU_Q6_AXI_BCR_RMSK                                                                 0x1
#define HWIO_GCC_MSS_TBU_Q6_AXI_BCR_IN          \
        in_dword_masked(HWIO_GCC_MSS_TBU_Q6_AXI_BCR_ADDR, HWIO_GCC_MSS_TBU_Q6_AXI_BCR_RMSK)
#define HWIO_GCC_MSS_TBU_Q6_AXI_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_MSS_TBU_Q6_AXI_BCR_ADDR, m)
#define HWIO_GCC_MSS_TBU_Q6_AXI_BCR_OUT(v)      \
        out_dword(HWIO_GCC_MSS_TBU_Q6_AXI_BCR_ADDR,v)
#define HWIO_GCC_MSS_TBU_Q6_AXI_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_MSS_TBU_Q6_AXI_BCR_ADDR,m,v,HWIO_GCC_MSS_TBU_Q6_AXI_BCR_IN)
#define HWIO_GCC_MSS_TBU_Q6_AXI_BCR_BLK_ARES_BMSK                                                        0x1
#define HWIO_GCC_MSS_TBU_Q6_AXI_BCR_BLK_ARES_SHFT                                                        0x0

#define HWIO_GCC_SMMU_XPU_BCR_ADDR                                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x00012054)
#define HWIO_GCC_SMMU_XPU_BCR_RMSK                                                                       0x1
#define HWIO_GCC_SMMU_XPU_BCR_IN          \
        in_dword_masked(HWIO_GCC_SMMU_XPU_BCR_ADDR, HWIO_GCC_SMMU_XPU_BCR_RMSK)
#define HWIO_GCC_SMMU_XPU_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SMMU_XPU_BCR_ADDR, m)
#define HWIO_GCC_SMMU_XPU_BCR_OUT(v)      \
        out_dword(HWIO_GCC_SMMU_XPU_BCR_ADDR,v)
#define HWIO_GCC_SMMU_XPU_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SMMU_XPU_BCR_ADDR,m,v,HWIO_GCC_SMMU_XPU_BCR_IN)
#define HWIO_GCC_SMMU_XPU_BCR_BLK_ARES_BMSK                                                              0x1
#define HWIO_GCC_SMMU_XPU_BCR_BLK_ARES_SHFT                                                              0x0

#define HWIO_GCC_SMMU_CFG_BCR_ADDR                                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x00012094)
#define HWIO_GCC_SMMU_CFG_BCR_RMSK                                                                       0x1
#define HWIO_GCC_SMMU_CFG_BCR_IN          \
        in_dword_masked(HWIO_GCC_SMMU_CFG_BCR_ADDR, HWIO_GCC_SMMU_CFG_BCR_RMSK)
#define HWIO_GCC_SMMU_CFG_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SMMU_CFG_BCR_ADDR, m)
#define HWIO_GCC_SMMU_CFG_BCR_OUT(v)      \
        out_dword(HWIO_GCC_SMMU_CFG_BCR_ADDR,v)
#define HWIO_GCC_SMMU_CFG_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SMMU_CFG_BCR_ADDR,m,v,HWIO_GCC_SMMU_CFG_BCR_IN)
#define HWIO_GCC_SMMU_CFG_BCR_BLK_ARES_BMSK                                                              0x1
#define HWIO_GCC_SMMU_CFG_BCR_BLK_ARES_SHFT                                                              0x0

#define HWIO_GCC_PCNOC_TBU_BCR_ADDR                                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x00012058)
#define HWIO_GCC_PCNOC_TBU_BCR_RMSK                                                                      0x1
#define HWIO_GCC_PCNOC_TBU_BCR_IN          \
        in_dword_masked(HWIO_GCC_PCNOC_TBU_BCR_ADDR, HWIO_GCC_PCNOC_TBU_BCR_RMSK)
#define HWIO_GCC_PCNOC_TBU_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PCNOC_TBU_BCR_ADDR, m)
#define HWIO_GCC_PCNOC_TBU_BCR_OUT(v)      \
        out_dword(HWIO_GCC_PCNOC_TBU_BCR_ADDR,v)
#define HWIO_GCC_PCNOC_TBU_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PCNOC_TBU_BCR_ADDR,m,v,HWIO_GCC_PCNOC_TBU_BCR_IN)
#define HWIO_GCC_PCNOC_TBU_BCR_BLK_ARES_BMSK                                                             0x1
#define HWIO_GCC_PCNOC_TBU_BCR_BLK_ARES_SHFT                                                             0x0

#define HWIO_GCC_RBCPR_BCR_ADDR                                                                   (GCC_CLK_CTL_REG_REG_BASE      + 0x00033000)
#define HWIO_GCC_RBCPR_BCR_RMSK                                                                          0x1
#define HWIO_GCC_RBCPR_BCR_IN          \
        in_dword_masked(HWIO_GCC_RBCPR_BCR_ADDR, HWIO_GCC_RBCPR_BCR_RMSK)
#define HWIO_GCC_RBCPR_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_RBCPR_BCR_ADDR, m)
#define HWIO_GCC_RBCPR_BCR_OUT(v)      \
        out_dword(HWIO_GCC_RBCPR_BCR_ADDR,v)
#define HWIO_GCC_RBCPR_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_RBCPR_BCR_ADDR,m,v,HWIO_GCC_RBCPR_BCR_IN)
#define HWIO_GCC_RBCPR_BCR_BLK_ARES_BMSK                                                                 0x1
#define HWIO_GCC_RBCPR_BCR_BLK_ARES_SHFT                                                                 0x0

#define HWIO_GCC_RBCPR_CBCR_ADDR                                                                  (GCC_CLK_CTL_REG_REG_BASE      + 0x00033004)
#define HWIO_GCC_RBCPR_CBCR_RMSK                                                                  0x80000001
#define HWIO_GCC_RBCPR_CBCR_IN          \
        in_dword_masked(HWIO_GCC_RBCPR_CBCR_ADDR, HWIO_GCC_RBCPR_CBCR_RMSK)
#define HWIO_GCC_RBCPR_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_RBCPR_CBCR_ADDR, m)
#define HWIO_GCC_RBCPR_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_RBCPR_CBCR_ADDR,v)
#define HWIO_GCC_RBCPR_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_RBCPR_CBCR_ADDR,m,v,HWIO_GCC_RBCPR_CBCR_IN)
#define HWIO_GCC_RBCPR_CBCR_CLK_OFF_BMSK                                                          0x80000000
#define HWIO_GCC_RBCPR_CBCR_CLK_OFF_SHFT                                                                0x1f
#define HWIO_GCC_RBCPR_CBCR_CLK_ENABLE_BMSK                                                              0x1
#define HWIO_GCC_RBCPR_CBCR_CLK_ENABLE_SHFT                                                              0x0

#define HWIO_GCC_RBCPR_AHB_CBCR_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x00033008)
#define HWIO_GCC_RBCPR_AHB_CBCR_RMSK                                                              0xf0008001
#define HWIO_GCC_RBCPR_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_RBCPR_AHB_CBCR_ADDR, HWIO_GCC_RBCPR_AHB_CBCR_RMSK)
#define HWIO_GCC_RBCPR_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_RBCPR_AHB_CBCR_ADDR, m)
#define HWIO_GCC_RBCPR_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_RBCPR_AHB_CBCR_ADDR,v)
#define HWIO_GCC_RBCPR_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_RBCPR_AHB_CBCR_ADDR,m,v,HWIO_GCC_RBCPR_AHB_CBCR_IN)
#define HWIO_GCC_RBCPR_AHB_CBCR_CLK_OFF_BMSK                                                      0x80000000
#define HWIO_GCC_RBCPR_AHB_CBCR_CLK_OFF_SHFT                                                            0x1f
#define HWIO_GCC_RBCPR_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                     0x70000000
#define HWIO_GCC_RBCPR_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                           0x1c
#define HWIO_GCC_RBCPR_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                             0x8000
#define HWIO_GCC_RBCPR_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                                0xf
#define HWIO_GCC_RBCPR_AHB_CBCR_CLK_ENABLE_BMSK                                                          0x1
#define HWIO_GCC_RBCPR_AHB_CBCR_CLK_ENABLE_SHFT                                                          0x0

#define HWIO_GCC_RBCPR_CMD_RCGR_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x0003300c)
#define HWIO_GCC_RBCPR_CMD_RCGR_RMSK                                                              0x80000013
#define HWIO_GCC_RBCPR_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_RBCPR_CMD_RCGR_ADDR, HWIO_GCC_RBCPR_CMD_RCGR_RMSK)
#define HWIO_GCC_RBCPR_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_RBCPR_CMD_RCGR_ADDR, m)
#define HWIO_GCC_RBCPR_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_RBCPR_CMD_RCGR_ADDR,v)
#define HWIO_GCC_RBCPR_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_RBCPR_CMD_RCGR_ADDR,m,v,HWIO_GCC_RBCPR_CMD_RCGR_IN)
#define HWIO_GCC_RBCPR_CMD_RCGR_ROOT_OFF_BMSK                                                     0x80000000
#define HWIO_GCC_RBCPR_CMD_RCGR_ROOT_OFF_SHFT                                                           0x1f
#define HWIO_GCC_RBCPR_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                                     0x10
#define HWIO_GCC_RBCPR_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                                      0x4
#define HWIO_GCC_RBCPR_CMD_RCGR_ROOT_EN_BMSK                                                             0x2
#define HWIO_GCC_RBCPR_CMD_RCGR_ROOT_EN_SHFT                                                             0x1
#define HWIO_GCC_RBCPR_CMD_RCGR_UPDATE_BMSK                                                              0x1
#define HWIO_GCC_RBCPR_CMD_RCGR_UPDATE_SHFT                                                              0x0

#define HWIO_GCC_RBCPR_CFG_RCGR_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x00033010)
#define HWIO_GCC_RBCPR_CFG_RCGR_RMSK                                                                   0x71f
#define HWIO_GCC_RBCPR_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_RBCPR_CFG_RCGR_ADDR, HWIO_GCC_RBCPR_CFG_RCGR_RMSK)
#define HWIO_GCC_RBCPR_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_RBCPR_CFG_RCGR_ADDR, m)
#define HWIO_GCC_RBCPR_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_RBCPR_CFG_RCGR_ADDR,v)
#define HWIO_GCC_RBCPR_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_RBCPR_CFG_RCGR_ADDR,m,v,HWIO_GCC_RBCPR_CFG_RCGR_IN)
#define HWIO_GCC_RBCPR_CFG_RCGR_SRC_SEL_BMSK                                                           0x700
#define HWIO_GCC_RBCPR_CFG_RCGR_SRC_SEL_SHFT                                                             0x8
#define HWIO_GCC_RBCPR_CFG_RCGR_SRC_DIV_BMSK                                                            0x1f
#define HWIO_GCC_RBCPR_CFG_RCGR_SRC_DIV_SHFT                                                             0x0

#define HWIO_GCC_RPM_GPLL_ENA_VOTE_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x00036000)
#define HWIO_GCC_RPM_GPLL_ENA_VOTE_RMSK                                                                  0xf
#define HWIO_GCC_RPM_GPLL_ENA_VOTE_IN          \
        in_dword_masked(HWIO_GCC_RPM_GPLL_ENA_VOTE_ADDR, HWIO_GCC_RPM_GPLL_ENA_VOTE_RMSK)
#define HWIO_GCC_RPM_GPLL_ENA_VOTE_INM(m)      \
        in_dword_masked(HWIO_GCC_RPM_GPLL_ENA_VOTE_ADDR, m)
#define HWIO_GCC_RPM_GPLL_ENA_VOTE_OUT(v)      \
        out_dword(HWIO_GCC_RPM_GPLL_ENA_VOTE_ADDR,v)
#define HWIO_GCC_RPM_GPLL_ENA_VOTE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_RPM_GPLL_ENA_VOTE_ADDR,m,v,HWIO_GCC_RPM_GPLL_ENA_VOTE_IN)
#define HWIO_GCC_RPM_GPLL_ENA_VOTE_GPLL3_BMSK                                                            0x8
#define HWIO_GCC_RPM_GPLL_ENA_VOTE_GPLL3_SHFT                                                            0x3
#define HWIO_GCC_RPM_GPLL_ENA_VOTE_GPLL2_BMSK                                                            0x4
#define HWIO_GCC_RPM_GPLL_ENA_VOTE_GPLL2_SHFT                                                            0x2
#define HWIO_GCC_RPM_GPLL_ENA_VOTE_GPLL1_BMSK                                                            0x2
#define HWIO_GCC_RPM_GPLL_ENA_VOTE_GPLL1_SHFT                                                            0x1
#define HWIO_GCC_RPM_GPLL_ENA_VOTE_GPLL0_BMSK                                                            0x1
#define HWIO_GCC_RPM_GPLL_ENA_VOTE_GPLL0_SHFT                                                            0x0

#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_ADDR                                                   (GCC_CLK_CTL_REG_REG_BASE      + 0x00036004)
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_RMSK                                                      0x7f7ff
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_IN          \
        in_dword_masked(HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_ADDR, HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_RMSK)
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_INM(m)      \
        in_dword_masked(HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_ADDR, m)
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_OUT(v)      \
        out_dword(HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_ADDR,v)
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_ADDR,m,v,HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_IN)
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_MSS_GPLL0_CLK_SRC_ENA_BMSK                                0x40000
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_MSS_GPLL0_CLK_SRC_ENA_SHFT                                   0x12
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_IMEM_AXI_CLK_ENA_BMSK                                     0x20000
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_IMEM_AXI_CLK_ENA_SHFT                                        0x11
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_SYS_NOC_APSS_AHB_CLK_ENA_BMSK                             0x10000
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_SYS_NOC_APSS_AHB_CLK_ENA_SHFT                                0x10
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_BIMC_APSS_AXI_CLK_ENA_BMSK                                 0x8000
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_BIMC_APSS_AXI_CLK_ENA_SHFT                                    0xf
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_APSS_AHB_CLK_ENA_BMSK                                      0x4000
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_APSS_AHB_CLK_ENA_SHFT                                         0xe
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_APSS_AXI_CLK_ENA_BMSK                                      0x2000
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_APSS_AXI_CLK_ENA_SHFT                                         0xd
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_MPM_AHB_CLK_ENA_BMSK                                       0x1000
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_MPM_AHB_CLK_ENA_SHFT                                          0xc
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_BLSP1_AHB_CLK_ENA_BMSK                                      0x400
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_BLSP1_AHB_CLK_ENA_SHFT                                        0xa
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_BLSP1_SLEEP_CLK_ENA_BMSK                                    0x200
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_BLSP1_SLEEP_CLK_ENA_SHFT                                      0x9
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_PRNG_AHB_CLK_ENA_BMSK                                       0x100
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_PRNG_AHB_CLK_ENA_SHFT                                         0x8
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_BOOT_ROM_AHB_CLK_ENA_BMSK                                    0x80
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_BOOT_ROM_AHB_CLK_ENA_SHFT                                     0x7
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_MSG_RAM_AHB_CLK_ENA_BMSK                                     0x40
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_MSG_RAM_AHB_CLK_ENA_SHFT                                      0x6
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_TLMM_AHB_CLK_ENA_BMSK                                        0x20
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_TLMM_AHB_CLK_ENA_SHFT                                         0x5
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_TLMM_CLK_ENA_BMSK                                            0x10
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_TLMM_CLK_ENA_SHFT                                             0x4
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_SPMI_PCNOC_AHB_CLK_ENA_BMSK                                   0x8
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_SPMI_PCNOC_AHB_CLK_ENA_SHFT                                   0x3
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_CRYPTO_CLK_ENA_BMSK                                           0x4
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_CRYPTO_CLK_ENA_SHFT                                           0x2
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_CRYPTO_AXI_CLK_ENA_BMSK                                       0x2
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_CRYPTO_AXI_CLK_ENA_SHFT                                       0x1
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_CRYPTO_AHB_CLK_ENA_BMSK                                       0x1
#define HWIO_GCC_RPM_CLOCK_BRANCH_ENA_VOTE_CRYPTO_AHB_CLK_ENA_SHFT                                       0x0

#define HWIO_GCC_RPM_SMMU_CLOCK_BRANCH_ENA_VOTE_ADDR                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x0003600c)
#define HWIO_GCC_RPM_SMMU_CLOCK_BRANCH_ENA_VOTE_RMSK                                                 0x3d942
#define HWIO_GCC_RPM_SMMU_CLOCK_BRANCH_ENA_VOTE_IN          \
        in_dword_masked(HWIO_GCC_RPM_SMMU_CLOCK_BRANCH_ENA_VOTE_ADDR, HWIO_GCC_RPM_SMMU_CLOCK_BRANCH_ENA_VOTE_RMSK)
#define HWIO_GCC_RPM_SMMU_CLOCK_BRANCH_ENA_VOTE_INM(m)      \
        in_dword_masked(HWIO_GCC_RPM_SMMU_CLOCK_BRANCH_ENA_VOTE_ADDR, m)
#define HWIO_GCC_RPM_SMMU_CLOCK_BRANCH_ENA_VOTE_OUT(v)      \
        out_dword(HWIO_GCC_RPM_SMMU_CLOCK_BRANCH_ENA_VOTE_ADDR,v)
#define HWIO_GCC_RPM_SMMU_CLOCK_BRANCH_ENA_VOTE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_RPM_SMMU_CLOCK_BRANCH_ENA_VOTE_ADDR,m,v,HWIO_GCC_RPM_SMMU_CLOCK_BRANCH_ENA_VOTE_IN)
#define HWIO_GCC_RPM_SMMU_CLOCK_BRANCH_ENA_VOTE_MSS_TBU_MCDMA_AXI_CLK_ENA_BMSK                       0x20000
#define HWIO_GCC_RPM_SMMU_CLOCK_BRANCH_ENA_VOTE_MSS_TBU_MCDMA_AXI_CLK_ENA_SHFT                          0x11
#define HWIO_GCC_RPM_SMMU_CLOCK_BRANCH_ENA_VOTE_PCIE_AXI_TBU_CLK_ENA_BMSK                            0x10000
#define HWIO_GCC_RPM_SMMU_CLOCK_BRANCH_ENA_VOTE_PCIE_AXI_TBU_CLK_ENA_SHFT                               0x10
#define HWIO_GCC_RPM_SMMU_CLOCK_BRANCH_ENA_VOTE_USB3_AXI_TBU_CLK_ENA_BMSK                             0x8000
#define HWIO_GCC_RPM_SMMU_CLOCK_BRANCH_ENA_VOTE_USB3_AXI_TBU_CLK_ENA_SHFT                                0xf
#define HWIO_GCC_RPM_SMMU_CLOCK_BRANCH_ENA_VOTE_IPA_TBU_CLK_ENA_BMSK                                  0x4000
#define HWIO_GCC_RPM_SMMU_CLOCK_BRANCH_ENA_VOTE_IPA_TBU_CLK_ENA_SHFT                                     0xe
#define HWIO_GCC_RPM_SMMU_CLOCK_BRANCH_ENA_VOTE_SMMU_CFG_CLK_ENA_BMSK                                 0x1000
#define HWIO_GCC_RPM_SMMU_CLOCK_BRANCH_ENA_VOTE_SMMU_CFG_CLK_ENA_SHFT                                    0xc
#define HWIO_GCC_RPM_SMMU_CLOCK_BRANCH_ENA_VOTE_PCNOC_TBU_CLK_ENA_BMSK                                 0x800
#define HWIO_GCC_RPM_SMMU_CLOCK_BRANCH_ENA_VOTE_PCNOC_TBU_CLK_ENA_SHFT                                   0xb
#define HWIO_GCC_RPM_SMMU_CLOCK_BRANCH_ENA_VOTE_MSS_TBU_Q6_AXI_CLK_ENA_BMSK                            0x100
#define HWIO_GCC_RPM_SMMU_CLOCK_BRANCH_ENA_VOTE_MSS_TBU_Q6_AXI_CLK_ENA_SHFT                              0x8
#define HWIO_GCC_RPM_SMMU_CLOCK_BRANCH_ENA_VOTE_MSS_TBU_AXI_CLK_ENA_BMSK                                0x40
#define HWIO_GCC_RPM_SMMU_CLOCK_BRANCH_ENA_VOTE_MSS_TBU_AXI_CLK_ENA_SHFT                                 0x6
#define HWIO_GCC_RPM_SMMU_CLOCK_BRANCH_ENA_VOTE_APSS_TCU_CLK_ENA_BMSK                                    0x2
#define HWIO_GCC_RPM_SMMU_CLOCK_BRANCH_ENA_VOTE_APSS_TCU_CLK_ENA_SHFT                                    0x1

#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_ADDR                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x00036008)
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_RMSK                                                       0x3f7ff
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_IN          \
        in_dword_masked(HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_ADDR, HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_RMSK)
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_INM(m)      \
        in_dword_masked(HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_ADDR, m)
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_OUT(v)      \
        out_dword(HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_ADDR,v)
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_ADDR,m,v,HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_IN)
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_IMEM_AXI_CLK_SLEEP_ENA_BMSK                                0x20000
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_IMEM_AXI_CLK_SLEEP_ENA_SHFT                                   0x11
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_SYS_NOC_APSS_AHB_CLK_SLEEP_ENA_BMSK                        0x10000
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_SYS_NOC_APSS_AHB_CLK_SLEEP_ENA_SHFT                           0x10
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_BIMC_APSS_AXI_CLK_SLEEP_ENA_BMSK                            0x8000
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_BIMC_APSS_AXI_CLK_SLEEP_ENA_SHFT                               0xf
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_APSS_AHB_CLK_SLEEP_ENA_BMSK                                 0x4000
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_APSS_AHB_CLK_SLEEP_ENA_SHFT                                    0xe
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_APSS_AXI_CLK_SLEEP_ENA_BMSK                                 0x2000
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_APSS_AXI_CLK_SLEEP_ENA_SHFT                                    0xd
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_MPM_AHB_CLK_SLEEP_ENA_BMSK                                  0x1000
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_MPM_AHB_CLK_SLEEP_ENA_SHFT                                     0xc
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_BLSP1_AHB_CLK_SLEEP_ENA_BMSK                                 0x400
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_BLSP1_AHB_CLK_SLEEP_ENA_SHFT                                   0xa
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_BLSP1_SLEEP_CLK_SLEEP_ENA_BMSK                               0x200
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_BLSP1_SLEEP_CLK_SLEEP_ENA_SHFT                                 0x9
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_PRNG_AHB_CLK_SLEEP_ENA_BMSK                                  0x100
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_PRNG_AHB_CLK_SLEEP_ENA_SHFT                                    0x8
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_BOOT_ROM_AHB_CLK_SLEEP_ENA_BMSK                               0x80
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_BOOT_ROM_AHB_CLK_SLEEP_ENA_SHFT                                0x7
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_MSG_RAM_AHB_CLK_SLEEP_ENA_BMSK                                0x40
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_MSG_RAM_AHB_CLK_SLEEP_ENA_SHFT                                 0x6
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_TLMM_AHB_CLK_SLEEP_ENA_BMSK                                   0x20
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_TLMM_AHB_CLK_SLEEP_ENA_SHFT                                    0x5
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_TLMM_CLK_SLEEP_ENA_BMSK                                       0x10
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_TLMM_CLK_SLEEP_ENA_SHFT                                        0x4
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_SPMI_PCNOC_AHB_CLK_SLEEP_ENA_BMSK                              0x8
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_SPMI_PCNOC_AHB_CLK_SLEEP_ENA_SHFT                              0x3
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_CRYPTO_CLK_SLEEP_ENA_BMSK                                      0x4
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_CRYPTO_CLK_SLEEP_ENA_SHFT                                      0x2
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_CRYPTO_AXI_CLK_SLEEP_ENA_BMSK                                  0x2
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_CRYPTO_AXI_CLK_SLEEP_ENA_SHFT                                  0x1
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_CRYPTO_AHB_CLK_SLEEP_ENA_BMSK                                  0x1
#define HWIO_GCC_RPM_CLOCK_SLEEP_ENA_VOTE_CRYPTO_AHB_CLK_SLEEP_ENA_SHFT                                  0x0

#define HWIO_GCC_RPM_SMMU_CLOCK_SLEEP_ENA_VOTE_ADDR                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x00036010)
#define HWIO_GCC_RPM_SMMU_CLOCK_SLEEP_ENA_VOTE_RMSK                                                  0x3d942
#define HWIO_GCC_RPM_SMMU_CLOCK_SLEEP_ENA_VOTE_IN          \
        in_dword_masked(HWIO_GCC_RPM_SMMU_CLOCK_SLEEP_ENA_VOTE_ADDR, HWIO_GCC_RPM_SMMU_CLOCK_SLEEP_ENA_VOTE_RMSK)
#define HWIO_GCC_RPM_SMMU_CLOCK_SLEEP_ENA_VOTE_INM(m)      \
        in_dword_masked(HWIO_GCC_RPM_SMMU_CLOCK_SLEEP_ENA_VOTE_ADDR, m)
#define HWIO_GCC_RPM_SMMU_CLOCK_SLEEP_ENA_VOTE_OUT(v)      \
        out_dword(HWIO_GCC_RPM_SMMU_CLOCK_SLEEP_ENA_VOTE_ADDR,v)
#define HWIO_GCC_RPM_SMMU_CLOCK_SLEEP_ENA_VOTE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_RPM_SMMU_CLOCK_SLEEP_ENA_VOTE_ADDR,m,v,HWIO_GCC_RPM_SMMU_CLOCK_SLEEP_ENA_VOTE_IN)
#define HWIO_GCC_RPM_SMMU_CLOCK_SLEEP_ENA_VOTE_MSS_TBU_MCDMA_AXI_CLK_SLEEP_ENA_BMSK                  0x20000
#define HWIO_GCC_RPM_SMMU_CLOCK_SLEEP_ENA_VOTE_MSS_TBU_MCDMA_AXI_CLK_SLEEP_ENA_SHFT                     0x11
#define HWIO_GCC_RPM_SMMU_CLOCK_SLEEP_ENA_VOTE_PCIE_AXI_TBU_CLK_SLEEP_ENA_BMSK                       0x10000
#define HWIO_GCC_RPM_SMMU_CLOCK_SLEEP_ENA_VOTE_PCIE_AXI_TBU_CLK_SLEEP_ENA_SHFT                          0x10
#define HWIO_GCC_RPM_SMMU_CLOCK_SLEEP_ENA_VOTE_USB3_AXI_TBU_CLK_SLEEP_ENA_BMSK                        0x8000
#define HWIO_GCC_RPM_SMMU_CLOCK_SLEEP_ENA_VOTE_USB3_AXI_TBU_CLK_SLEEP_ENA_SHFT                           0xf
#define HWIO_GCC_RPM_SMMU_CLOCK_SLEEP_ENA_VOTE_IPA_TBU_CLK_SLEEP_ENA_BMSK                             0x4000
#define HWIO_GCC_RPM_SMMU_CLOCK_SLEEP_ENA_VOTE_IPA_TBU_CLK_SLEEP_ENA_SHFT                                0xe
#define HWIO_GCC_RPM_SMMU_CLOCK_SLEEP_ENA_VOTE_SMMU_CFG_CLK_SLEEP_ENA_BMSK                            0x1000
#define HWIO_GCC_RPM_SMMU_CLOCK_SLEEP_ENA_VOTE_SMMU_CFG_CLK_SLEEP_ENA_SHFT                               0xc
#define HWIO_GCC_RPM_SMMU_CLOCK_SLEEP_ENA_VOTE_PCNOC_TBU_CLK_SLEEP_ENA_BMSK                            0x800
#define HWIO_GCC_RPM_SMMU_CLOCK_SLEEP_ENA_VOTE_PCNOC_TBU_CLK_SLEEP_ENA_SHFT                              0xb
#define HWIO_GCC_RPM_SMMU_CLOCK_SLEEP_ENA_VOTE_MSS_TBU_Q6_AXI_CLK_SLEEP_ENA_BMSK                       0x100
#define HWIO_GCC_RPM_SMMU_CLOCK_SLEEP_ENA_VOTE_MSS_TBU_Q6_AXI_CLK_SLEEP_ENA_SHFT                         0x8
#define HWIO_GCC_RPM_SMMU_CLOCK_SLEEP_ENA_VOTE_MSS_TBU_AXI_CLK_SLEEP_ENA_BMSK                           0x40
#define HWIO_GCC_RPM_SMMU_CLOCK_SLEEP_ENA_VOTE_MSS_TBU_AXI_CLK_SLEEP_ENA_SHFT                            0x6
#define HWIO_GCC_RPM_SMMU_CLOCK_SLEEP_ENA_VOTE_APSS_TCU_CLK_SLEEP_ENA_BMSK                               0x2
#define HWIO_GCC_RPM_SMMU_CLOCK_SLEEP_ENA_VOTE_APSS_TCU_CLK_SLEEP_ENA_SHFT                               0x1

#define HWIO_GCC_APCS_GPLL_ENA_VOTE_ADDR                                                          (GCC_CLK_CTL_REG_REG_BASE      + 0x00045000)
#define HWIO_GCC_APCS_GPLL_ENA_VOTE_RMSK                                                                 0xf
#define HWIO_GCC_APCS_GPLL_ENA_VOTE_IN          \
        in_dword_masked(HWIO_GCC_APCS_GPLL_ENA_VOTE_ADDR, HWIO_GCC_APCS_GPLL_ENA_VOTE_RMSK)
#define HWIO_GCC_APCS_GPLL_ENA_VOTE_INM(m)      \
        in_dword_masked(HWIO_GCC_APCS_GPLL_ENA_VOTE_ADDR, m)
#define HWIO_GCC_APCS_GPLL_ENA_VOTE_OUT(v)      \
        out_dword(HWIO_GCC_APCS_GPLL_ENA_VOTE_ADDR,v)
#define HWIO_GCC_APCS_GPLL_ENA_VOTE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_APCS_GPLL_ENA_VOTE_ADDR,m,v,HWIO_GCC_APCS_GPLL_ENA_VOTE_IN)
#define HWIO_GCC_APCS_GPLL_ENA_VOTE_GPLL3_BMSK                                                           0x8
#define HWIO_GCC_APCS_GPLL_ENA_VOTE_GPLL3_SHFT                                                           0x3
#define HWIO_GCC_APCS_GPLL_ENA_VOTE_GPLL2_BMSK                                                           0x4
#define HWIO_GCC_APCS_GPLL_ENA_VOTE_GPLL2_SHFT                                                           0x2
#define HWIO_GCC_APCS_GPLL_ENA_VOTE_GPLL1_BMSK                                                           0x2
#define HWIO_GCC_APCS_GPLL_ENA_VOTE_GPLL1_SHFT                                                           0x1
#define HWIO_GCC_APCS_GPLL_ENA_VOTE_GPLL0_BMSK                                                           0x1
#define HWIO_GCC_APCS_GPLL_ENA_VOTE_GPLL0_SHFT                                                           0x0

#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_ADDR                                                  (GCC_CLK_CTL_REG_REG_BASE      + 0x00045004)
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_RMSK                                                     0x3f7ff
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_IN          \
        in_dword_masked(HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_ADDR, HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_RMSK)
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_INM(m)      \
        in_dword_masked(HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_ADDR, m)
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_OUT(v)      \
        out_dword(HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_ADDR,v)
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_ADDR,m,v,HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_IN)
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_IMEM_AXI_CLK_ENA_BMSK                                    0x20000
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_IMEM_AXI_CLK_ENA_SHFT                                       0x11
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_SYS_NOC_APSS_AHB_CLK_ENA_BMSK                            0x10000
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_SYS_NOC_APSS_AHB_CLK_ENA_SHFT                               0x10
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_BIMC_APSS_AXI_CLK_ENA_BMSK                                0x8000
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_BIMC_APSS_AXI_CLK_ENA_SHFT                                   0xf
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_APSS_AHB_CLK_ENA_BMSK                                     0x4000
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_APSS_AHB_CLK_ENA_SHFT                                        0xe
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_APSS_AXI_CLK_ENA_BMSK                                     0x2000
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_APSS_AXI_CLK_ENA_SHFT                                        0xd
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_MPM_AHB_CLK_ENA_BMSK                                      0x1000
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_MPM_AHB_CLK_ENA_SHFT                                         0xc
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_BLSP1_AHB_CLK_ENA_BMSK                                     0x400
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_BLSP1_AHB_CLK_ENA_SHFT                                       0xa
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_BLSP1_SLEEP_CLK_ENA_BMSK                                   0x200
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_BLSP1_SLEEP_CLK_ENA_SHFT                                     0x9
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_PRNG_AHB_CLK_ENA_BMSK                                      0x100
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_PRNG_AHB_CLK_ENA_SHFT                                        0x8
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_BOOT_ROM_AHB_CLK_ENA_BMSK                                   0x80
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_BOOT_ROM_AHB_CLK_ENA_SHFT                                    0x7
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_MSG_RAM_AHB_CLK_ENA_BMSK                                    0x40
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_MSG_RAM_AHB_CLK_ENA_SHFT                                     0x6
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_TLMM_AHB_CLK_ENA_BMSK                                       0x20
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_TLMM_AHB_CLK_ENA_SHFT                                        0x5
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_TLMM_CLK_ENA_BMSK                                           0x10
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_TLMM_CLK_ENA_SHFT                                            0x4
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_SPMI_PCNOC_AHB_CLK_ENA_BMSK                                  0x8
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_SPMI_PCNOC_AHB_CLK_ENA_SHFT                                  0x3
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_CRYPTO_CLK_ENA_BMSK                                          0x4
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_CRYPTO_CLK_ENA_SHFT                                          0x2
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_CRYPTO_AXI_CLK_ENA_BMSK                                      0x2
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_CRYPTO_AXI_CLK_ENA_SHFT                                      0x1
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_CRYPTO_AHB_CLK_ENA_BMSK                                      0x1
#define HWIO_GCC_APCS_CLOCK_BRANCH_ENA_VOTE_CRYPTO_AHB_CLK_ENA_SHFT                                      0x0

#define HWIO_GCC_APCS_SMMU_CLOCK_BRANCH_ENA_VOTE_ADDR                                             (GCC_CLK_CTL_REG_REG_BASE      + 0x0004500c)
#define HWIO_GCC_APCS_SMMU_CLOCK_BRANCH_ENA_VOTE_RMSK                                                0x3d942
#define HWIO_GCC_APCS_SMMU_CLOCK_BRANCH_ENA_VOTE_IN          \
        in_dword_masked(HWIO_GCC_APCS_SMMU_CLOCK_BRANCH_ENA_VOTE_ADDR, HWIO_GCC_APCS_SMMU_CLOCK_BRANCH_ENA_VOTE_RMSK)
#define HWIO_GCC_APCS_SMMU_CLOCK_BRANCH_ENA_VOTE_INM(m)      \
        in_dword_masked(HWIO_GCC_APCS_SMMU_CLOCK_BRANCH_ENA_VOTE_ADDR, m)
#define HWIO_GCC_APCS_SMMU_CLOCK_BRANCH_ENA_VOTE_OUT(v)      \
        out_dword(HWIO_GCC_APCS_SMMU_CLOCK_BRANCH_ENA_VOTE_ADDR,v)
#define HWIO_GCC_APCS_SMMU_CLOCK_BRANCH_ENA_VOTE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_APCS_SMMU_CLOCK_BRANCH_ENA_VOTE_ADDR,m,v,HWIO_GCC_APCS_SMMU_CLOCK_BRANCH_ENA_VOTE_IN)
#define HWIO_GCC_APCS_SMMU_CLOCK_BRANCH_ENA_VOTE_MSS_TBU_MCDMA_AXI_CLK_ENA_BMSK                      0x20000
#define HWIO_GCC_APCS_SMMU_CLOCK_BRANCH_ENA_VOTE_MSS_TBU_MCDMA_AXI_CLK_ENA_SHFT                         0x11
#define HWIO_GCC_APCS_SMMU_CLOCK_BRANCH_ENA_VOTE_PCIE_AXI_TBU_CLK_ENA_BMSK                           0x10000
#define HWIO_GCC_APCS_SMMU_CLOCK_BRANCH_ENA_VOTE_PCIE_AXI_TBU_CLK_ENA_SHFT                              0x10
#define HWIO_GCC_APCS_SMMU_CLOCK_BRANCH_ENA_VOTE_USB3_AXI_TBU_CLK_ENA_BMSK                            0x8000
#define HWIO_GCC_APCS_SMMU_CLOCK_BRANCH_ENA_VOTE_USB3_AXI_TBU_CLK_ENA_SHFT                               0xf
#define HWIO_GCC_APCS_SMMU_CLOCK_BRANCH_ENA_VOTE_IPA_TBU_CLK_ENA_BMSK                                 0x4000
#define HWIO_GCC_APCS_SMMU_CLOCK_BRANCH_ENA_VOTE_IPA_TBU_CLK_ENA_SHFT                                    0xe
#define HWIO_GCC_APCS_SMMU_CLOCK_BRANCH_ENA_VOTE_SMMU_CFG_CLK_ENA_BMSK                                0x1000
#define HWIO_GCC_APCS_SMMU_CLOCK_BRANCH_ENA_VOTE_SMMU_CFG_CLK_ENA_SHFT                                   0xc
#define HWIO_GCC_APCS_SMMU_CLOCK_BRANCH_ENA_VOTE_PCNOC_TBU_CLK_ENA_BMSK                                0x800
#define HWIO_GCC_APCS_SMMU_CLOCK_BRANCH_ENA_VOTE_PCNOC_TBU_CLK_ENA_SHFT                                  0xb
#define HWIO_GCC_APCS_SMMU_CLOCK_BRANCH_ENA_VOTE_MSS_TBU_Q6_AXI_CLK_ENA_BMSK                           0x100
#define HWIO_GCC_APCS_SMMU_CLOCK_BRANCH_ENA_VOTE_MSS_TBU_Q6_AXI_CLK_ENA_SHFT                             0x8
#define HWIO_GCC_APCS_SMMU_CLOCK_BRANCH_ENA_VOTE_MSS_TBU_AXI_CLK_ENA_BMSK                               0x40
#define HWIO_GCC_APCS_SMMU_CLOCK_BRANCH_ENA_VOTE_MSS_TBU_AXI_CLK_ENA_SHFT                                0x6
#define HWIO_GCC_APCS_SMMU_CLOCK_BRANCH_ENA_VOTE_APSS_TCU_CLK_ENA_BMSK                                   0x2
#define HWIO_GCC_APCS_SMMU_CLOCK_BRANCH_ENA_VOTE_APSS_TCU_CLK_ENA_SHFT                                   0x1

#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_ADDR                                                   (GCC_CLK_CTL_REG_REG_BASE      + 0x00045008)
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_RMSK                                                      0x3f7ff
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_IN          \
        in_dword_masked(HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_ADDR, HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_RMSK)
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_INM(m)      \
        in_dword_masked(HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_ADDR, m)
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_OUT(v)      \
        out_dword(HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_ADDR,v)
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_ADDR,m,v,HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_IN)
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_IMEM_AXI_CLK_SLEEP_ENA_BMSK                               0x20000
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_IMEM_AXI_CLK_SLEEP_ENA_SHFT                                  0x11
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_SYS_NOC_APSS_AHB_CLK_SLEEP_ENA_BMSK                       0x10000
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_SYS_NOC_APSS_AHB_CLK_SLEEP_ENA_SHFT                          0x10
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_BIMC_APSS_AXI_CLK_SLEEP_ENA_BMSK                           0x8000
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_BIMC_APSS_AXI_CLK_SLEEP_ENA_SHFT                              0xf
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_APSS_AHB_CLK_SLEEP_ENA_BMSK                                0x4000
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_APSS_AHB_CLK_SLEEP_ENA_SHFT                                   0xe
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_APSS_AXI_CLK_SLEEP_ENA_BMSK                                0x2000
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_APSS_AXI_CLK_SLEEP_ENA_SHFT                                   0xd
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_MPM_AHB_CLK_SLEEP_ENA_BMSK                                 0x1000
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_MPM_AHB_CLK_SLEEP_ENA_SHFT                                    0xc
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_BLSP1_AHB_CLK_SLEEP_ENA_BMSK                                0x400
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_BLSP1_AHB_CLK_SLEEP_ENA_SHFT                                  0xa
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_BLSP1_SLEEP_CLK_SLEEP_ENA_BMSK                              0x200
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_BLSP1_SLEEP_CLK_SLEEP_ENA_SHFT                                0x9
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_PRNG_AHB_CLK_SLEEP_ENA_BMSK                                 0x100
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_PRNG_AHB_CLK_SLEEP_ENA_SHFT                                   0x8
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_BOOT_ROM_AHB_CLK_SLEEP_ENA_BMSK                              0x80
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_BOOT_ROM_AHB_CLK_SLEEP_ENA_SHFT                               0x7
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_MSG_RAM_AHB_CLK_SLEEP_ENA_BMSK                               0x40
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_MSG_RAM_AHB_CLK_SLEEP_ENA_SHFT                                0x6
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_TLMM_AHB_CLK_SLEEP_ENA_BMSK                                  0x20
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_TLMM_AHB_CLK_SLEEP_ENA_SHFT                                   0x5
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_TLMM_CLK_SLEEP_ENA_BMSK                                      0x10
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_TLMM_CLK_SLEEP_ENA_SHFT                                       0x4
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_SPMI_PCNOC_AHB_CLK_SLEEP_ENA_BMSK                             0x8
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_SPMI_PCNOC_AHB_CLK_SLEEP_ENA_SHFT                             0x3
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_CRYPTO_CLK_SLEEP_ENA_BMSK                                     0x4
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_CRYPTO_CLK_SLEEP_ENA_SHFT                                     0x2
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_CRYPTO_AXI_CLK_SLEEP_ENA_BMSK                                 0x2
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_CRYPTO_AXI_CLK_SLEEP_ENA_SHFT                                 0x1
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_CRYPTO_AHB_CLK_SLEEP_ENA_BMSK                                 0x1
#define HWIO_GCC_APCS_CLOCK_SLEEP_ENA_VOTE_CRYPTO_AHB_CLK_SLEEP_ENA_SHFT                                 0x0

#define HWIO_GCC_APCS_SMMU_CLOCK_SLEEP_ENA_VOTE_ADDR                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x00045010)
#define HWIO_GCC_APCS_SMMU_CLOCK_SLEEP_ENA_VOTE_RMSK                                                 0x3d942
#define HWIO_GCC_APCS_SMMU_CLOCK_SLEEP_ENA_VOTE_IN          \
        in_dword_masked(HWIO_GCC_APCS_SMMU_CLOCK_SLEEP_ENA_VOTE_ADDR, HWIO_GCC_APCS_SMMU_CLOCK_SLEEP_ENA_VOTE_RMSK)
#define HWIO_GCC_APCS_SMMU_CLOCK_SLEEP_ENA_VOTE_INM(m)      \
        in_dword_masked(HWIO_GCC_APCS_SMMU_CLOCK_SLEEP_ENA_VOTE_ADDR, m)
#define HWIO_GCC_APCS_SMMU_CLOCK_SLEEP_ENA_VOTE_OUT(v)      \
        out_dword(HWIO_GCC_APCS_SMMU_CLOCK_SLEEP_ENA_VOTE_ADDR,v)
#define HWIO_GCC_APCS_SMMU_CLOCK_SLEEP_ENA_VOTE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_APCS_SMMU_CLOCK_SLEEP_ENA_VOTE_ADDR,m,v,HWIO_GCC_APCS_SMMU_CLOCK_SLEEP_ENA_VOTE_IN)
#define HWIO_GCC_APCS_SMMU_CLOCK_SLEEP_ENA_VOTE_MSS_TBU_MCDMA_AXI_CLK_SLEEP_ENA_BMSK                 0x20000
#define HWIO_GCC_APCS_SMMU_CLOCK_SLEEP_ENA_VOTE_MSS_TBU_MCDMA_AXI_CLK_SLEEP_ENA_SHFT                    0x11
#define HWIO_GCC_APCS_SMMU_CLOCK_SLEEP_ENA_VOTE_PCIE_AXI_TBU_CLK_SLEEP_ENA_BMSK                      0x10000
#define HWIO_GCC_APCS_SMMU_CLOCK_SLEEP_ENA_VOTE_PCIE_AXI_TBU_CLK_SLEEP_ENA_SHFT                         0x10
#define HWIO_GCC_APCS_SMMU_CLOCK_SLEEP_ENA_VOTE_USB3_AXI_TBU_CLK_SLEEP_ENA_BMSK                       0x8000
#define HWIO_GCC_APCS_SMMU_CLOCK_SLEEP_ENA_VOTE_USB3_AXI_TBU_CLK_SLEEP_ENA_SHFT                          0xf
#define HWIO_GCC_APCS_SMMU_CLOCK_SLEEP_ENA_VOTE_IPA_TBU_CLK_SLEEP_ENA_BMSK                            0x4000
#define HWIO_GCC_APCS_SMMU_CLOCK_SLEEP_ENA_VOTE_IPA_TBU_CLK_SLEEP_ENA_SHFT                               0xe
#define HWIO_GCC_APCS_SMMU_CLOCK_SLEEP_ENA_VOTE_SMMU_CFG_CLK_SLEEP_ENA_BMSK                           0x1000
#define HWIO_GCC_APCS_SMMU_CLOCK_SLEEP_ENA_VOTE_SMMU_CFG_CLK_SLEEP_ENA_SHFT                              0xc
#define HWIO_GCC_APCS_SMMU_CLOCK_SLEEP_ENA_VOTE_PCNOC_TBU_CLK_SLEEP_ENA_BMSK                           0x800
#define HWIO_GCC_APCS_SMMU_CLOCK_SLEEP_ENA_VOTE_PCNOC_TBU_CLK_SLEEP_ENA_SHFT                             0xb
#define HWIO_GCC_APCS_SMMU_CLOCK_SLEEP_ENA_VOTE_MSS_TBU_Q6_AXI_CLK_SLEEP_ENA_BMSK                      0x100
#define HWIO_GCC_APCS_SMMU_CLOCK_SLEEP_ENA_VOTE_MSS_TBU_Q6_AXI_CLK_SLEEP_ENA_SHFT                        0x8
#define HWIO_GCC_APCS_SMMU_CLOCK_SLEEP_ENA_VOTE_MSS_TBU_AXI_CLK_SLEEP_ENA_BMSK                          0x40
#define HWIO_GCC_APCS_SMMU_CLOCK_SLEEP_ENA_VOTE_MSS_TBU_AXI_CLK_SLEEP_ENA_SHFT                           0x6
#define HWIO_GCC_APCS_SMMU_CLOCK_SLEEP_ENA_VOTE_APSS_TCU_CLK_SLEEP_ENA_BMSK                              0x2
#define HWIO_GCC_APCS_SMMU_CLOCK_SLEEP_ENA_VOTE_APSS_TCU_CLK_SLEEP_ENA_SHFT                              0x1

#define HWIO_GCC_APCS_TZ_GPLL_ENA_VOTE_ADDR                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x00013010)
#define HWIO_GCC_APCS_TZ_GPLL_ENA_VOTE_RMSK                                                              0xf
#define HWIO_GCC_APCS_TZ_GPLL_ENA_VOTE_IN          \
        in_dword_masked(HWIO_GCC_APCS_TZ_GPLL_ENA_VOTE_ADDR, HWIO_GCC_APCS_TZ_GPLL_ENA_VOTE_RMSK)
#define HWIO_GCC_APCS_TZ_GPLL_ENA_VOTE_INM(m)      \
        in_dword_masked(HWIO_GCC_APCS_TZ_GPLL_ENA_VOTE_ADDR, m)
#define HWIO_GCC_APCS_TZ_GPLL_ENA_VOTE_OUT(v)      \
        out_dword(HWIO_GCC_APCS_TZ_GPLL_ENA_VOTE_ADDR,v)
#define HWIO_GCC_APCS_TZ_GPLL_ENA_VOTE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_APCS_TZ_GPLL_ENA_VOTE_ADDR,m,v,HWIO_GCC_APCS_TZ_GPLL_ENA_VOTE_IN)
#define HWIO_GCC_APCS_TZ_GPLL_ENA_VOTE_GPLL3_BMSK                                                        0x8
#define HWIO_GCC_APCS_TZ_GPLL_ENA_VOTE_GPLL3_SHFT                                                        0x3
#define HWIO_GCC_APCS_TZ_GPLL_ENA_VOTE_GPLL2_BMSK                                                        0x4
#define HWIO_GCC_APCS_TZ_GPLL_ENA_VOTE_GPLL2_SHFT                                                        0x2
#define HWIO_GCC_APCS_TZ_GPLL_ENA_VOTE_GPLL1_BMSK                                                        0x2
#define HWIO_GCC_APCS_TZ_GPLL_ENA_VOTE_GPLL1_SHFT                                                        0x1
#define HWIO_GCC_APCS_TZ_GPLL_ENA_VOTE_GPLL0_BMSK                                                        0x1
#define HWIO_GCC_APCS_TZ_GPLL_ENA_VOTE_GPLL0_SHFT                                                        0x0

#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_ADDR                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x00013014)
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_RMSK                                                  0x3f7ff
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_IN          \
        in_dword_masked(HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_ADDR, HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_RMSK)
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_INM(m)      \
        in_dword_masked(HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_ADDR, m)
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_OUT(v)      \
        out_dword(HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_ADDR,v)
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_ADDR,m,v,HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_IN)
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_IMEM_AXI_CLK_ENA_BMSK                                 0x20000
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_IMEM_AXI_CLK_ENA_SHFT                                    0x11
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_SYS_NOC_APSS_AHB_CLK_ENA_BMSK                         0x10000
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_SYS_NOC_APSS_AHB_CLK_ENA_SHFT                            0x10
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_BIMC_APSS_AXI_CLK_ENA_BMSK                             0x8000
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_BIMC_APSS_AXI_CLK_ENA_SHFT                                0xf
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_APSS_AHB_CLK_ENA_BMSK                                  0x4000
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_APSS_AHB_CLK_ENA_SHFT                                     0xe
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_APSS_AXI_CLK_ENA_BMSK                                  0x2000
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_APSS_AXI_CLK_ENA_SHFT                                     0xd
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_MPM_AHB_CLK_ENA_BMSK                                   0x1000
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_MPM_AHB_CLK_ENA_SHFT                                      0xc
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_BLSP1_AHB_CLK_ENA_BMSK                                  0x400
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_BLSP1_AHB_CLK_ENA_SHFT                                    0xa
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_BLSP1_SLEEP_CLK_ENA_BMSK                                0x200
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_BLSP1_SLEEP_CLK_ENA_SHFT                                  0x9
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_PRNG_AHB_CLK_ENA_BMSK                                   0x100
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_PRNG_AHB_CLK_ENA_SHFT                                     0x8
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_BOOT_ROM_AHB_CLK_ENA_BMSK                                0x80
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_BOOT_ROM_AHB_CLK_ENA_SHFT                                 0x7
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_MSG_RAM_AHB_CLK_ENA_BMSK                                 0x40
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_MSG_RAM_AHB_CLK_ENA_SHFT                                  0x6
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_TLMM_AHB_CLK_ENA_BMSK                                    0x20
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_TLMM_AHB_CLK_ENA_SHFT                                     0x5
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_TLMM_CLK_ENA_BMSK                                        0x10
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_TLMM_CLK_ENA_SHFT                                         0x4
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_SPMI_PCNOC_AHB_CLK_ENA_BMSK                               0x8
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_SPMI_PCNOC_AHB_CLK_ENA_SHFT                               0x3
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_CRYPTO_CLK_ENA_BMSK                                       0x4
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_CRYPTO_CLK_ENA_SHFT                                       0x2
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_CRYPTO_AXI_CLK_ENA_BMSK                                   0x2
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_CRYPTO_AXI_CLK_ENA_SHFT                                   0x1
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_CRYPTO_AHB_CLK_ENA_BMSK                                   0x1
#define HWIO_GCC_APCS_TZ_CLOCK_BRANCH_ENA_VOTE_CRYPTO_AHB_CLK_ENA_SHFT                                   0x0

#define HWIO_GCC_APCS_TZ_SMMU_CLOCK_BRANCH_ENA_VOTE_ADDR                                          (GCC_CLK_CTL_REG_REG_BASE      + 0x00013020)
#define HWIO_GCC_APCS_TZ_SMMU_CLOCK_BRANCH_ENA_VOTE_RMSK                                             0x3d942
#define HWIO_GCC_APCS_TZ_SMMU_CLOCK_BRANCH_ENA_VOTE_IN          \
        in_dword_masked(HWIO_GCC_APCS_TZ_SMMU_CLOCK_BRANCH_ENA_VOTE_ADDR, HWIO_GCC_APCS_TZ_SMMU_CLOCK_BRANCH_ENA_VOTE_RMSK)
#define HWIO_GCC_APCS_TZ_SMMU_CLOCK_BRANCH_ENA_VOTE_INM(m)      \
        in_dword_masked(HWIO_GCC_APCS_TZ_SMMU_CLOCK_BRANCH_ENA_VOTE_ADDR, m)
#define HWIO_GCC_APCS_TZ_SMMU_CLOCK_BRANCH_ENA_VOTE_OUT(v)      \
        out_dword(HWIO_GCC_APCS_TZ_SMMU_CLOCK_BRANCH_ENA_VOTE_ADDR,v)
#define HWIO_GCC_APCS_TZ_SMMU_CLOCK_BRANCH_ENA_VOTE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_APCS_TZ_SMMU_CLOCK_BRANCH_ENA_VOTE_ADDR,m,v,HWIO_GCC_APCS_TZ_SMMU_CLOCK_BRANCH_ENA_VOTE_IN)
#define HWIO_GCC_APCS_TZ_SMMU_CLOCK_BRANCH_ENA_VOTE_MSS_TBU_MCDMA_AXI_CLK_ENA_BMSK                   0x20000
#define HWIO_GCC_APCS_TZ_SMMU_CLOCK_BRANCH_ENA_VOTE_MSS_TBU_MCDMA_AXI_CLK_ENA_SHFT                      0x11
#define HWIO_GCC_APCS_TZ_SMMU_CLOCK_BRANCH_ENA_VOTE_PCIE_AXI_TBU_CLK_ENA_BMSK                        0x10000
#define HWIO_GCC_APCS_TZ_SMMU_CLOCK_BRANCH_ENA_VOTE_PCIE_AXI_TBU_CLK_ENA_SHFT                           0x10
#define HWIO_GCC_APCS_TZ_SMMU_CLOCK_BRANCH_ENA_VOTE_USB3_AXI_TBU_CLK_ENA_BMSK                         0x8000
#define HWIO_GCC_APCS_TZ_SMMU_CLOCK_BRANCH_ENA_VOTE_USB3_AXI_TBU_CLK_ENA_SHFT                            0xf
#define HWIO_GCC_APCS_TZ_SMMU_CLOCK_BRANCH_ENA_VOTE_IPA_TBU_CLK_ENA_BMSK                              0x4000
#define HWIO_GCC_APCS_TZ_SMMU_CLOCK_BRANCH_ENA_VOTE_IPA_TBU_CLK_ENA_SHFT                                 0xe
#define HWIO_GCC_APCS_TZ_SMMU_CLOCK_BRANCH_ENA_VOTE_SMMU_CFG_CLK_ENA_BMSK                             0x1000
#define HWIO_GCC_APCS_TZ_SMMU_CLOCK_BRANCH_ENA_VOTE_SMMU_CFG_CLK_ENA_SHFT                                0xc
#define HWIO_GCC_APCS_TZ_SMMU_CLOCK_BRANCH_ENA_VOTE_PCNOC_TBU_CLK_ENA_BMSK                             0x800
#define HWIO_GCC_APCS_TZ_SMMU_CLOCK_BRANCH_ENA_VOTE_PCNOC_TBU_CLK_ENA_SHFT                               0xb
#define HWIO_GCC_APCS_TZ_SMMU_CLOCK_BRANCH_ENA_VOTE_MSS_TBU_Q6_AXI_CLK_ENA_BMSK                        0x100
#define HWIO_GCC_APCS_TZ_SMMU_CLOCK_BRANCH_ENA_VOTE_MSS_TBU_Q6_AXI_CLK_ENA_SHFT                          0x8
#define HWIO_GCC_APCS_TZ_SMMU_CLOCK_BRANCH_ENA_VOTE_MSS_TBU_AXI_CLK_ENA_BMSK                            0x40
#define HWIO_GCC_APCS_TZ_SMMU_CLOCK_BRANCH_ENA_VOTE_MSS_TBU_AXI_CLK_ENA_SHFT                             0x6
#define HWIO_GCC_APCS_TZ_SMMU_CLOCK_BRANCH_ENA_VOTE_APSS_TCU_CLK_ENA_BMSK                                0x2
#define HWIO_GCC_APCS_TZ_SMMU_CLOCK_BRANCH_ENA_VOTE_APSS_TCU_CLK_ENA_SHFT                                0x1

#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_ADDR                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x00013018)
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_RMSK                                                   0x3f7ff
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_IN          \
        in_dword_masked(HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_ADDR, HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_RMSK)
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_INM(m)      \
        in_dword_masked(HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_ADDR, m)
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_OUT(v)      \
        out_dword(HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_ADDR,v)
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_ADDR,m,v,HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_IN)
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_IMEM_AXI_CLK_SLEEP_ENA_BMSK                            0x20000
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_IMEM_AXI_CLK_SLEEP_ENA_SHFT                               0x11
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_SYS_NOC_APSS_AHB_CLK_SLEEP_ENA_BMSK                    0x10000
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_SYS_NOC_APSS_AHB_CLK_SLEEP_ENA_SHFT                       0x10
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_BIMC_APSS_AXI_CLK_SLEEP_ENA_BMSK                        0x8000
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_BIMC_APSS_AXI_CLK_SLEEP_ENA_SHFT                           0xf
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_APSS_AHB_CLK_SLEEP_ENA_BMSK                             0x4000
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_APSS_AHB_CLK_SLEEP_ENA_SHFT                                0xe
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_APSS_AXI_CLK_SLEEP_ENA_BMSK                             0x2000
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_APSS_AXI_CLK_SLEEP_ENA_SHFT                                0xd
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_MPM_AHB_CLK_SLEEP_ENA_BMSK                              0x1000
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_MPM_AHB_CLK_SLEEP_ENA_SHFT                                 0xc
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_BLSP1_AHB_CLK_SLEEP_ENA_BMSK                             0x400
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_BLSP1_AHB_CLK_SLEEP_ENA_SHFT                               0xa
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_BLSP1_SLEEP_CLK_SLEEP_ENA_BMSK                           0x200
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_BLSP1_SLEEP_CLK_SLEEP_ENA_SHFT                             0x9
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_PRNG_AHB_CLK_SLEEP_ENA_BMSK                              0x100
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_PRNG_AHB_CLK_SLEEP_ENA_SHFT                                0x8
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_BOOT_ROM_AHB_CLK_SLEEP_ENA_BMSK                           0x80
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_BOOT_ROM_AHB_CLK_SLEEP_ENA_SHFT                            0x7
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_MSG_RAM_AHB_CLK_SLEEP_ENA_BMSK                            0x40
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_MSG_RAM_AHB_CLK_SLEEP_ENA_SHFT                             0x6
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_TLMM_AHB_CLK_SLEEP_ENA_BMSK                               0x20
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_TLMM_AHB_CLK_SLEEP_ENA_SHFT                                0x5
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_TLMM_CLK_SLEEP_ENA_BMSK                                   0x10
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_TLMM_CLK_SLEEP_ENA_SHFT                                    0x4
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_SPMI_PCNOC_AHB_CLK_SLEEP_ENA_BMSK                          0x8
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_SPMI_PCNOC_AHB_CLK_SLEEP_ENA_SHFT                          0x3
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_CRYPTO_CLK_SLEEP_ENA_BMSK                                  0x4
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_CRYPTO_CLK_SLEEP_ENA_SHFT                                  0x2
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_CRYPTO_AXI_CLK_SLEEP_ENA_BMSK                              0x2
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_CRYPTO_AXI_CLK_SLEEP_ENA_SHFT                              0x1
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_CRYPTO_AHB_CLK_SLEEP_ENA_BMSK                              0x1
#define HWIO_GCC_APCS_TZ_CLOCK_SLEEP_ENA_VOTE_CRYPTO_AHB_CLK_SLEEP_ENA_SHFT                              0x0

#define HWIO_GCC_APCS_TZ_SMMU_CLOCK_SLEEP_ENA_VOTE_ADDR                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x00013024)
#define HWIO_GCC_APCS_TZ_SMMU_CLOCK_SLEEP_ENA_VOTE_RMSK                                              0x3d942
#define HWIO_GCC_APCS_TZ_SMMU_CLOCK_SLEEP_ENA_VOTE_IN          \
        in_dword_masked(HWIO_GCC_APCS_TZ_SMMU_CLOCK_SLEEP_ENA_VOTE_ADDR, HWIO_GCC_APCS_TZ_SMMU_CLOCK_SLEEP_ENA_VOTE_RMSK)
#define HWIO_GCC_APCS_TZ_SMMU_CLOCK_SLEEP_ENA_VOTE_INM(m)      \
        in_dword_masked(HWIO_GCC_APCS_TZ_SMMU_CLOCK_SLEEP_ENA_VOTE_ADDR, m)
#define HWIO_GCC_APCS_TZ_SMMU_CLOCK_SLEEP_ENA_VOTE_OUT(v)      \
        out_dword(HWIO_GCC_APCS_TZ_SMMU_CLOCK_SLEEP_ENA_VOTE_ADDR,v)
#define HWIO_GCC_APCS_TZ_SMMU_CLOCK_SLEEP_ENA_VOTE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_APCS_TZ_SMMU_CLOCK_SLEEP_ENA_VOTE_ADDR,m,v,HWIO_GCC_APCS_TZ_SMMU_CLOCK_SLEEP_ENA_VOTE_IN)
#define HWIO_GCC_APCS_TZ_SMMU_CLOCK_SLEEP_ENA_VOTE_MSS_TBU_MCDMA_AXI_CLK_SLEEP_ENA_BMSK              0x20000
#define HWIO_GCC_APCS_TZ_SMMU_CLOCK_SLEEP_ENA_VOTE_MSS_TBU_MCDMA_AXI_CLK_SLEEP_ENA_SHFT                 0x11
#define HWIO_GCC_APCS_TZ_SMMU_CLOCK_SLEEP_ENA_VOTE_PCIE_AXI_TBU_CLK_SLEEP_ENA_BMSK                   0x10000
#define HWIO_GCC_APCS_TZ_SMMU_CLOCK_SLEEP_ENA_VOTE_PCIE_AXI_TBU_CLK_SLEEP_ENA_SHFT                      0x10
#define HWIO_GCC_APCS_TZ_SMMU_CLOCK_SLEEP_ENA_VOTE_USB3_AXI_TBU_CLK_SLEEP_ENA_BMSK                    0x8000
#define HWIO_GCC_APCS_TZ_SMMU_CLOCK_SLEEP_ENA_VOTE_USB3_AXI_TBU_CLK_SLEEP_ENA_SHFT                       0xf
#define HWIO_GCC_APCS_TZ_SMMU_CLOCK_SLEEP_ENA_VOTE_IPA_TBU_CLK_SLEEP_ENA_BMSK                         0x4000
#define HWIO_GCC_APCS_TZ_SMMU_CLOCK_SLEEP_ENA_VOTE_IPA_TBU_CLK_SLEEP_ENA_SHFT                            0xe
#define HWIO_GCC_APCS_TZ_SMMU_CLOCK_SLEEP_ENA_VOTE_SMMU_CFG_CLK_SLEEP_ENA_BMSK                        0x1000
#define HWIO_GCC_APCS_TZ_SMMU_CLOCK_SLEEP_ENA_VOTE_SMMU_CFG_CLK_SLEEP_ENA_SHFT                           0xc
#define HWIO_GCC_APCS_TZ_SMMU_CLOCK_SLEEP_ENA_VOTE_PCNOC_TBU_CLK_SLEEP_ENA_BMSK                        0x800
#define HWIO_GCC_APCS_TZ_SMMU_CLOCK_SLEEP_ENA_VOTE_PCNOC_TBU_CLK_SLEEP_ENA_SHFT                          0xb
#define HWIO_GCC_APCS_TZ_SMMU_CLOCK_SLEEP_ENA_VOTE_MSS_TBU_Q6_AXI_CLK_SLEEP_ENA_BMSK                   0x100
#define HWIO_GCC_APCS_TZ_SMMU_CLOCK_SLEEP_ENA_VOTE_MSS_TBU_Q6_AXI_CLK_SLEEP_ENA_SHFT                     0x8
#define HWIO_GCC_APCS_TZ_SMMU_CLOCK_SLEEP_ENA_VOTE_MSS_TBU_AXI_CLK_SLEEP_ENA_BMSK                       0x40
#define HWIO_GCC_APCS_TZ_SMMU_CLOCK_SLEEP_ENA_VOTE_MSS_TBU_AXI_CLK_SLEEP_ENA_SHFT                        0x6
#define HWIO_GCC_APCS_TZ_SMMU_CLOCK_SLEEP_ENA_VOTE_APSS_TCU_CLK_SLEEP_ENA_BMSK                           0x2
#define HWIO_GCC_APCS_TZ_SMMU_CLOCK_SLEEP_ENA_VOTE_APSS_TCU_CLK_SLEEP_ENA_SHFT                           0x1

#define HWIO_GCC_APCS_HYP_GPLL_ENA_VOTE_ADDR                                                      (GCC_CLK_CTL_REG_REG_BASE      + 0x00061000)
#define HWIO_GCC_APCS_HYP_GPLL_ENA_VOTE_RMSK                                                             0xf
#define HWIO_GCC_APCS_HYP_GPLL_ENA_VOTE_IN          \
        in_dword_masked(HWIO_GCC_APCS_HYP_GPLL_ENA_VOTE_ADDR, HWIO_GCC_APCS_HYP_GPLL_ENA_VOTE_RMSK)
#define HWIO_GCC_APCS_HYP_GPLL_ENA_VOTE_INM(m)      \
        in_dword_masked(HWIO_GCC_APCS_HYP_GPLL_ENA_VOTE_ADDR, m)
#define HWIO_GCC_APCS_HYP_GPLL_ENA_VOTE_OUT(v)      \
        out_dword(HWIO_GCC_APCS_HYP_GPLL_ENA_VOTE_ADDR,v)
#define HWIO_GCC_APCS_HYP_GPLL_ENA_VOTE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_APCS_HYP_GPLL_ENA_VOTE_ADDR,m,v,HWIO_GCC_APCS_HYP_GPLL_ENA_VOTE_IN)
#define HWIO_GCC_APCS_HYP_GPLL_ENA_VOTE_GPLL3_BMSK                                                       0x8
#define HWIO_GCC_APCS_HYP_GPLL_ENA_VOTE_GPLL3_SHFT                                                       0x3
#define HWIO_GCC_APCS_HYP_GPLL_ENA_VOTE_GPLL2_BMSK                                                       0x4
#define HWIO_GCC_APCS_HYP_GPLL_ENA_VOTE_GPLL2_SHFT                                                       0x2
#define HWIO_GCC_APCS_HYP_GPLL_ENA_VOTE_GPLL1_BMSK                                                       0x2
#define HWIO_GCC_APCS_HYP_GPLL_ENA_VOTE_GPLL1_SHFT                                                       0x1
#define HWIO_GCC_APCS_HYP_GPLL_ENA_VOTE_GPLL0_BMSK                                                       0x1
#define HWIO_GCC_APCS_HYP_GPLL_ENA_VOTE_GPLL0_SHFT                                                       0x0

#define HWIO_GCC_APCS_HYP_CLOCK_BRANCH_ENA_VOTE_ADDR                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x00061004)
#define HWIO_GCC_APCS_HYP_CLOCK_BRANCH_ENA_VOTE_RMSK                                                 0x3f7ff
#define HWIO_GCC_APCS_HYP_CLOCK_BRANCH_ENA_VOTE_IN          \
        in_dword_masked(HWIO_GCC_APCS_HYP_CLOCK_BRANCH_ENA_VOTE_ADDR, HWIO_GCC_APCS_HYP_CLOCK_BRANCH_ENA_VOTE_RMSK)
#define HWIO_GCC_APCS_HYP_CLOCK_BRANCH_ENA_VOTE_INM(m)      \
        in_dword_masked(HWIO_GCC_APCS_HYP_CLOCK_BRANCH_ENA_VOTE_ADDR, m)
#define HWIO_GCC_APCS_HYP_CLOCK_BRANCH_ENA_VOTE_OUT(v)      \
        out_dword(HWIO_GCC_APCS_HYP_CLOCK_BRANCH_ENA_VOTE_ADDR,v)
#define HWIO_GCC_APCS_HYP_CLOCK_BRANCH_ENA_VOTE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_APCS_HYP_CLOCK_BRANCH_ENA_VOTE_ADDR,m,v,HWIO_GCC_APCS_HYP_CLOCK_BRANCH_ENA_VOTE_IN)
#define HWIO_GCC_APCS_HYP_CLOCK_BRANCH_ENA_VOTE_IMEM_AXI_CLK_ENA_BMSK                                0x20000
#define HWIO_GCC_APCS_HYP_CLOCK_BRANCH_ENA_VOTE_IMEM_AXI_CLK_ENA_SHFT                                   0x11
#define HWIO_GCC_APCS_HYP_CLOCK_BRANCH_ENA_VOTE_SYS_NOC_APSS_AHB_CLK_ENA_BMSK                        0x10000
#define HWIO_GCC_APCS_HYP_CLOCK_BRANCH_ENA_VOTE_SYS_NOC_APSS_AHB_CLK_ENA_SHFT                           0x10
#define HWIO_GCC_APCS_HYP_CLOCK_BRANCH_ENA_VOTE_BIMC_APSS_AXI_CLK_ENA_BMSK                            0x8000
#define HWIO_GCC_APCS_HYP_CLOCK_BRANCH_ENA_VOTE_BIMC_APSS_AXI_CLK_ENA_SHFT                               0xf
#define HWIO_GCC_APCS_HYP_CLOCK_BRANCH_ENA_VOTE_APSS_AHB_CLK_ENA_BMSK                                 0x4000
#define HWIO_GCC_APCS_HYP_CLOCK_BRANCH_ENA_VOTE_APSS_AHB_CLK_ENA_SHFT                                    0xe
#define HWIO_GCC_APCS_HYP_CLOCK_BRANCH_ENA_VOTE_APSS_AXI_CLK_ENA_BMSK                                 0x2000
#define HWIO_GCC_APCS_HYP_CLOCK_BRANCH_ENA_VOTE_APSS_AXI_CLK_ENA_SHFT                                    0xd
#define HWIO_GCC_APCS_HYP_CLOCK_BRANCH_ENA_VOTE_MPM_AHB_CLK_ENA_BMSK                                  0x1000
#define HWIO_GCC_APCS_HYP_CLOCK_BRANCH_ENA_VOTE_MPM_AHB_CLK_ENA_SHFT                                     0xc
#define HWIO_GCC_APCS_HYP_CLOCK_BRANCH_ENA_VOTE_BLSP1_AHB_CLK_ENA_BMSK                                 0x400
#define HWIO_GCC_APCS_HYP_CLOCK_BRANCH_ENA_VOTE_BLSP1_AHB_CLK_ENA_SHFT                                   0xa
#define HWIO_GCC_APCS_HYP_CLOCK_BRANCH_ENA_VOTE_BLSP1_SLEEP_CLK_ENA_BMSK                               0x200
#define HWIO_GCC_APCS_HYP_CLOCK_BRANCH_ENA_VOTE_BLSP1_SLEEP_CLK_ENA_SHFT                                 0x9
#define HWIO_GCC_APCS_HYP_CLOCK_BRANCH_ENA_VOTE_PRNG_AHB_CLK_ENA_BMSK                                  0x100
#define HWIO_GCC_APCS_HYP_CLOCK_BRANCH_ENA_VOTE_PRNG_AHB_CLK_ENA_SHFT                                    0x8
#define HWIO_GCC_APCS_HYP_CLOCK_BRANCH_ENA_VOTE_BOOT_ROM_AHB_CLK_ENA_BMSK                               0x80
#define HWIO_GCC_APCS_HYP_CLOCK_BRANCH_ENA_VOTE_BOOT_ROM_AHB_CLK_ENA_SHFT                                0x7
#define HWIO_GCC_APCS_HYP_CLOCK_BRANCH_ENA_VOTE_MSG_RAM_AHB_CLK_ENA_BMSK                                0x40
#define HWIO_GCC_APCS_HYP_CLOCK_BRANCH_ENA_VOTE_MSG_RAM_AHB_CLK_ENA_SHFT                                 0x6
#define HWIO_GCC_APCS_HYP_CLOCK_BRANCH_ENA_VOTE_TLMM_AHB_CLK_ENA_BMSK                                   0x20
#define HWIO_GCC_APCS_HYP_CLOCK_BRANCH_ENA_VOTE_TLMM_AHB_CLK_ENA_SHFT                                    0x5
#define HWIO_GCC_APCS_HYP_CLOCK_BRANCH_ENA_VOTE_TLMM_CLK_ENA_BMSK                                       0x10
#define HWIO_GCC_APCS_HYP_CLOCK_BRANCH_ENA_VOTE_TLMM_CLK_ENA_SHFT                                        0x4
#define HWIO_GCC_APCS_HYP_CLOCK_BRANCH_ENA_VOTE_SPMI_PCNOC_AHB_CLK_ENA_BMSK                              0x8
#define HWIO_GCC_APCS_HYP_CLOCK_BRANCH_ENA_VOTE_SPMI_PCNOC_AHB_CLK_ENA_SHFT                              0x3
#define HWIO_GCC_APCS_HYP_CLOCK_BRANCH_ENA_VOTE_CRYPTO_CLK_ENA_BMSK                                      0x4
#define HWIO_GCC_APCS_HYP_CLOCK_BRANCH_ENA_VOTE_CRYPTO_CLK_ENA_SHFT                                      0x2
#define HWIO_GCC_APCS_HYP_CLOCK_BRANCH_ENA_VOTE_CRYPTO_AXI_CLK_ENA_BMSK                                  0x2
#define HWIO_GCC_APCS_HYP_CLOCK_BRANCH_ENA_VOTE_CRYPTO_AXI_CLK_ENA_SHFT                                  0x1
#define HWIO_GCC_APCS_HYP_CLOCK_BRANCH_ENA_VOTE_CRYPTO_AHB_CLK_ENA_BMSK                                  0x1
#define HWIO_GCC_APCS_HYP_CLOCK_BRANCH_ENA_VOTE_CRYPTO_AHB_CLK_ENA_SHFT                                  0x0

#define HWIO_GCC_APCS_HYP_SMMU_CLOCK_BRANCH_ENA_VOTE_ADDR                                         (GCC_CLK_CTL_REG_REG_BASE      + 0x0006100c)
#define HWIO_GCC_APCS_HYP_SMMU_CLOCK_BRANCH_ENA_VOTE_RMSK                                            0x3d942
#define HWIO_GCC_APCS_HYP_SMMU_CLOCK_BRANCH_ENA_VOTE_IN          \
        in_dword_masked(HWIO_GCC_APCS_HYP_SMMU_CLOCK_BRANCH_ENA_VOTE_ADDR, HWIO_GCC_APCS_HYP_SMMU_CLOCK_BRANCH_ENA_VOTE_RMSK)
#define HWIO_GCC_APCS_HYP_SMMU_CLOCK_BRANCH_ENA_VOTE_INM(m)      \
        in_dword_masked(HWIO_GCC_APCS_HYP_SMMU_CLOCK_BRANCH_ENA_VOTE_ADDR, m)
#define HWIO_GCC_APCS_HYP_SMMU_CLOCK_BRANCH_ENA_VOTE_OUT(v)      \
        out_dword(HWIO_GCC_APCS_HYP_SMMU_CLOCK_BRANCH_ENA_VOTE_ADDR,v)
#define HWIO_GCC_APCS_HYP_SMMU_CLOCK_BRANCH_ENA_VOTE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_APCS_HYP_SMMU_CLOCK_BRANCH_ENA_VOTE_ADDR,m,v,HWIO_GCC_APCS_HYP_SMMU_CLOCK_BRANCH_ENA_VOTE_IN)
#define HWIO_GCC_APCS_HYP_SMMU_CLOCK_BRANCH_ENA_VOTE_MSS_TBU_MCDMA_AXI_CLK_ENA_BMSK                  0x20000
#define HWIO_GCC_APCS_HYP_SMMU_CLOCK_BRANCH_ENA_VOTE_MSS_TBU_MCDMA_AXI_CLK_ENA_SHFT                     0x11
#define HWIO_GCC_APCS_HYP_SMMU_CLOCK_BRANCH_ENA_VOTE_PCIE_AXI_TBU_CLK_ENA_BMSK                       0x10000
#define HWIO_GCC_APCS_HYP_SMMU_CLOCK_BRANCH_ENA_VOTE_PCIE_AXI_TBU_CLK_ENA_SHFT                          0x10
#define HWIO_GCC_APCS_HYP_SMMU_CLOCK_BRANCH_ENA_VOTE_USB3_AXI_TBU_CLK_ENA_BMSK                        0x8000
#define HWIO_GCC_APCS_HYP_SMMU_CLOCK_BRANCH_ENA_VOTE_USB3_AXI_TBU_CLK_ENA_SHFT                           0xf
#define HWIO_GCC_APCS_HYP_SMMU_CLOCK_BRANCH_ENA_VOTE_IPA_TBU_CLK_ENA_BMSK                             0x4000
#define HWIO_GCC_APCS_HYP_SMMU_CLOCK_BRANCH_ENA_VOTE_IPA_TBU_CLK_ENA_SHFT                                0xe
#define HWIO_GCC_APCS_HYP_SMMU_CLOCK_BRANCH_ENA_VOTE_SMMU_CFG_CLK_ENA_BMSK                            0x1000
#define HWIO_GCC_APCS_HYP_SMMU_CLOCK_BRANCH_ENA_VOTE_SMMU_CFG_CLK_ENA_SHFT                               0xc
#define HWIO_GCC_APCS_HYP_SMMU_CLOCK_BRANCH_ENA_VOTE_PCNOC_TBU_CLK_ENA_BMSK                            0x800
#define HWIO_GCC_APCS_HYP_SMMU_CLOCK_BRANCH_ENA_VOTE_PCNOC_TBU_CLK_ENA_SHFT                              0xb
#define HWIO_GCC_APCS_HYP_SMMU_CLOCK_BRANCH_ENA_VOTE_MSS_TBU_Q6_AXI_CLK_ENA_BMSK                       0x100
#define HWIO_GCC_APCS_HYP_SMMU_CLOCK_BRANCH_ENA_VOTE_MSS_TBU_Q6_AXI_CLK_ENA_SHFT                         0x8
#define HWIO_GCC_APCS_HYP_SMMU_CLOCK_BRANCH_ENA_VOTE_MSS_TBU_AXI_CLK_ENA_BMSK                           0x40
#define HWIO_GCC_APCS_HYP_SMMU_CLOCK_BRANCH_ENA_VOTE_MSS_TBU_AXI_CLK_ENA_SHFT                            0x6
#define HWIO_GCC_APCS_HYP_SMMU_CLOCK_BRANCH_ENA_VOTE_APSS_TCU_CLK_ENA_BMSK                               0x2
#define HWIO_GCC_APCS_HYP_SMMU_CLOCK_BRANCH_ENA_VOTE_APSS_TCU_CLK_ENA_SHFT                               0x1

#define HWIO_GCC_APCS_HYP_CLOCK_SLEEP_ENA_VOTE_ADDR                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x00061008)
#define HWIO_GCC_APCS_HYP_CLOCK_SLEEP_ENA_VOTE_RMSK                                                  0x3f7ff
#define HWIO_GCC_APCS_HYP_CLOCK_SLEEP_ENA_VOTE_IN          \
        in_dword_masked(HWIO_GCC_APCS_HYP_CLOCK_SLEEP_ENA_VOTE_ADDR, HWIO_GCC_APCS_HYP_CLOCK_SLEEP_ENA_VOTE_RMSK)
#define HWIO_GCC_APCS_HYP_CLOCK_SLEEP_ENA_VOTE_INM(m)      \
        in_dword_masked(HWIO_GCC_APCS_HYP_CLOCK_SLEEP_ENA_VOTE_ADDR, m)
#define HWIO_GCC_APCS_HYP_CLOCK_SLEEP_ENA_VOTE_OUT(v)      \
        out_dword(HWIO_GCC_APCS_HYP_CLOCK_SLEEP_ENA_VOTE_ADDR,v)
#define HWIO_GCC_APCS_HYP_CLOCK_SLEEP_ENA_VOTE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_APCS_HYP_CLOCK_SLEEP_ENA_VOTE_ADDR,m,v,HWIO_GCC_APCS_HYP_CLOCK_SLEEP_ENA_VOTE_IN)
#define HWIO_GCC_APCS_HYP_CLOCK_SLEEP_ENA_VOTE_IMEM_AXI_CLK_SLEEP_ENA_BMSK                           0x20000
#define HWIO_GCC_APCS_HYP_CLOCK_SLEEP_ENA_VOTE_IMEM_AXI_CLK_SLEEP_ENA_SHFT                              0x11
#define HWIO_GCC_APCS_HYP_CLOCK_SLEEP_ENA_VOTE_SYS_NOC_APSS_AHB_CLK_SLEEP_ENA_BMSK                   0x10000
#define HWIO_GCC_APCS_HYP_CLOCK_SLEEP_ENA_VOTE_SYS_NOC_APSS_AHB_CLK_SLEEP_ENA_SHFT                      0x10
#define HWIO_GCC_APCS_HYP_CLOCK_SLEEP_ENA_VOTE_BIMC_APSS_AXI_CLK_SLEEP_ENA_BMSK                       0x8000
#define HWIO_GCC_APCS_HYP_CLOCK_SLEEP_ENA_VOTE_BIMC_APSS_AXI_CLK_SLEEP_ENA_SHFT                          0xf
#define HWIO_GCC_APCS_HYP_CLOCK_SLEEP_ENA_VOTE_APSS_AHB_CLK_SLEEP_ENA_BMSK                            0x4000
#define HWIO_GCC_APCS_HYP_CLOCK_SLEEP_ENA_VOTE_APSS_AHB_CLK_SLEEP_ENA_SHFT                               0xe
#define HWIO_GCC_APCS_HYP_CLOCK_SLEEP_ENA_VOTE_APSS_AXI_CLK_SLEEP_ENA_BMSK                            0x2000
#define HWIO_GCC_APCS_HYP_CLOCK_SLEEP_ENA_VOTE_APSS_AXI_CLK_SLEEP_ENA_SHFT                               0xd
#define HWIO_GCC_APCS_HYP_CLOCK_SLEEP_ENA_VOTE_MPM_AHB_CLK_SLEEP_ENA_BMSK                             0x1000
#define HWIO_GCC_APCS_HYP_CLOCK_SLEEP_ENA_VOTE_MPM_AHB_CLK_SLEEP_ENA_SHFT                                0xc
#define HWIO_GCC_APCS_HYP_CLOCK_SLEEP_ENA_VOTE_BLSP1_AHB_CLK_SLEEP_ENA_BMSK                            0x400
#define HWIO_GCC_APCS_HYP_CLOCK_SLEEP_ENA_VOTE_BLSP1_AHB_CLK_SLEEP_ENA_SHFT                              0xa
#define HWIO_GCC_APCS_HYP_CLOCK_SLEEP_ENA_VOTE_BLSP1_SLEEP_CLK_SLEEP_ENA_BMSK                          0x200
#define HWIO_GCC_APCS_HYP_CLOCK_SLEEP_ENA_VOTE_BLSP1_SLEEP_CLK_SLEEP_ENA_SHFT                            0x9
#define HWIO_GCC_APCS_HYP_CLOCK_SLEEP_ENA_VOTE_PRNG_AHB_CLK_SLEEP_ENA_BMSK                             0x100
#define HWIO_GCC_APCS_HYP_CLOCK_SLEEP_ENA_VOTE_PRNG_AHB_CLK_SLEEP_ENA_SHFT                               0x8
#define HWIO_GCC_APCS_HYP_CLOCK_SLEEP_ENA_VOTE_BOOT_ROM_AHB_CLK_SLEEP_ENA_BMSK                          0x80
#define HWIO_GCC_APCS_HYP_CLOCK_SLEEP_ENA_VOTE_BOOT_ROM_AHB_CLK_SLEEP_ENA_SHFT                           0x7
#define HWIO_GCC_APCS_HYP_CLOCK_SLEEP_ENA_VOTE_MSG_RAM_AHB_CLK_SLEEP_ENA_BMSK                           0x40
#define HWIO_GCC_APCS_HYP_CLOCK_SLEEP_ENA_VOTE_MSG_RAM_AHB_CLK_SLEEP_ENA_SHFT                            0x6
#define HWIO_GCC_APCS_HYP_CLOCK_SLEEP_ENA_VOTE_TLMM_AHB_CLK_SLEEP_ENA_BMSK                              0x20
#define HWIO_GCC_APCS_HYP_CLOCK_SLEEP_ENA_VOTE_TLMM_AHB_CLK_SLEEP_ENA_SHFT                               0x5
#define HWIO_GCC_APCS_HYP_CLOCK_SLEEP_ENA_VOTE_TLMM_CLK_SLEEP_ENA_BMSK                                  0x10
#define HWIO_GCC_APCS_HYP_CLOCK_SLEEP_ENA_VOTE_TLMM_CLK_SLEEP_ENA_SHFT                                   0x4
#define HWIO_GCC_APCS_HYP_CLOCK_SLEEP_ENA_VOTE_SPMI_PCNOC_AHB_CLK_SLEEP_ENA_BMSK                         0x8
#define HWIO_GCC_APCS_HYP_CLOCK_SLEEP_ENA_VOTE_SPMI_PCNOC_AHB_CLK_SLEEP_ENA_SHFT                         0x3
#define HWIO_GCC_APCS_HYP_CLOCK_SLEEP_ENA_VOTE_CRYPTO_CLK_SLEEP_ENA_BMSK                                 0x4
#define HWIO_GCC_APCS_HYP_CLOCK_SLEEP_ENA_VOTE_CRYPTO_CLK_SLEEP_ENA_SHFT                                 0x2
#define HWIO_GCC_APCS_HYP_CLOCK_SLEEP_ENA_VOTE_CRYPTO_AXI_CLK_SLEEP_ENA_BMSK                             0x2
#define HWIO_GCC_APCS_HYP_CLOCK_SLEEP_ENA_VOTE_CRYPTO_AXI_CLK_SLEEP_ENA_SHFT                             0x1
#define HWIO_GCC_APCS_HYP_CLOCK_SLEEP_ENA_VOTE_CRYPTO_AHB_CLK_SLEEP_ENA_BMSK                             0x1
#define HWIO_GCC_APCS_HYP_CLOCK_SLEEP_ENA_VOTE_CRYPTO_AHB_CLK_SLEEP_ENA_SHFT                             0x0

#define HWIO_GCC_APCS_HYP_SMMU_CLOCK_SLEEP_ENA_VOTE_ADDR                                          (GCC_CLK_CTL_REG_REG_BASE      + 0x00061010)
#define HWIO_GCC_APCS_HYP_SMMU_CLOCK_SLEEP_ENA_VOTE_RMSK                                             0x3d942
#define HWIO_GCC_APCS_HYP_SMMU_CLOCK_SLEEP_ENA_VOTE_IN          \
        in_dword_masked(HWIO_GCC_APCS_HYP_SMMU_CLOCK_SLEEP_ENA_VOTE_ADDR, HWIO_GCC_APCS_HYP_SMMU_CLOCK_SLEEP_ENA_VOTE_RMSK)
#define HWIO_GCC_APCS_HYP_SMMU_CLOCK_SLEEP_ENA_VOTE_INM(m)      \
        in_dword_masked(HWIO_GCC_APCS_HYP_SMMU_CLOCK_SLEEP_ENA_VOTE_ADDR, m)
#define HWIO_GCC_APCS_HYP_SMMU_CLOCK_SLEEP_ENA_VOTE_OUT(v)      \
        out_dword(HWIO_GCC_APCS_HYP_SMMU_CLOCK_SLEEP_ENA_VOTE_ADDR,v)
#define HWIO_GCC_APCS_HYP_SMMU_CLOCK_SLEEP_ENA_VOTE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_APCS_HYP_SMMU_CLOCK_SLEEP_ENA_VOTE_ADDR,m,v,HWIO_GCC_APCS_HYP_SMMU_CLOCK_SLEEP_ENA_VOTE_IN)
#define HWIO_GCC_APCS_HYP_SMMU_CLOCK_SLEEP_ENA_VOTE_MSS_TBU_MCDMA_AXI_CLK_SLEEP_ENA_BMSK             0x20000
#define HWIO_GCC_APCS_HYP_SMMU_CLOCK_SLEEP_ENA_VOTE_MSS_TBU_MCDMA_AXI_CLK_SLEEP_ENA_SHFT                0x11
#define HWIO_GCC_APCS_HYP_SMMU_CLOCK_SLEEP_ENA_VOTE_PCIE_AXI_TBU_CLK_SLEEP_ENA_BMSK                  0x10000
#define HWIO_GCC_APCS_HYP_SMMU_CLOCK_SLEEP_ENA_VOTE_PCIE_AXI_TBU_CLK_SLEEP_ENA_SHFT                     0x10
#define HWIO_GCC_APCS_HYP_SMMU_CLOCK_SLEEP_ENA_VOTE_USB3_AXI_TBU_CLK_SLEEP_ENA_BMSK                   0x8000
#define HWIO_GCC_APCS_HYP_SMMU_CLOCK_SLEEP_ENA_VOTE_USB3_AXI_TBU_CLK_SLEEP_ENA_SHFT                      0xf
#define HWIO_GCC_APCS_HYP_SMMU_CLOCK_SLEEP_ENA_VOTE_IPA_TBU_CLK_SLEEP_ENA_BMSK                        0x4000
#define HWIO_GCC_APCS_HYP_SMMU_CLOCK_SLEEP_ENA_VOTE_IPA_TBU_CLK_SLEEP_ENA_SHFT                           0xe
#define HWIO_GCC_APCS_HYP_SMMU_CLOCK_SLEEP_ENA_VOTE_SMMU_CFG_CLK_SLEEP_ENA_BMSK                       0x1000
#define HWIO_GCC_APCS_HYP_SMMU_CLOCK_SLEEP_ENA_VOTE_SMMU_CFG_CLK_SLEEP_ENA_SHFT                          0xc
#define HWIO_GCC_APCS_HYP_SMMU_CLOCK_SLEEP_ENA_VOTE_PCNOC_TBU_CLK_SLEEP_ENA_BMSK                       0x800
#define HWIO_GCC_APCS_HYP_SMMU_CLOCK_SLEEP_ENA_VOTE_PCNOC_TBU_CLK_SLEEP_ENA_SHFT                         0xb
#define HWIO_GCC_APCS_HYP_SMMU_CLOCK_SLEEP_ENA_VOTE_MSS_TBU_Q6_AXI_CLK_SLEEP_ENA_BMSK                  0x100
#define HWIO_GCC_APCS_HYP_SMMU_CLOCK_SLEEP_ENA_VOTE_MSS_TBU_Q6_AXI_CLK_SLEEP_ENA_SHFT                    0x8
#define HWIO_GCC_APCS_HYP_SMMU_CLOCK_SLEEP_ENA_VOTE_MSS_TBU_AXI_CLK_SLEEP_ENA_BMSK                      0x40
#define HWIO_GCC_APCS_HYP_SMMU_CLOCK_SLEEP_ENA_VOTE_MSS_TBU_AXI_CLK_SLEEP_ENA_SHFT                       0x6
#define HWIO_GCC_APCS_HYP_SMMU_CLOCK_SLEEP_ENA_VOTE_APSS_TCU_CLK_SLEEP_ENA_BMSK                          0x2
#define HWIO_GCC_APCS_HYP_SMMU_CLOCK_SLEEP_ENA_VOTE_APSS_TCU_CLK_SLEEP_ENA_SHFT                          0x1

#define HWIO_GCC_MSS_Q6_GPLL_ENA_VOTE_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x0001b000)
#define HWIO_GCC_MSS_Q6_GPLL_ENA_VOTE_RMSK                                                               0xf
#define HWIO_GCC_MSS_Q6_GPLL_ENA_VOTE_IN          \
        in_dword_masked(HWIO_GCC_MSS_Q6_GPLL_ENA_VOTE_ADDR, HWIO_GCC_MSS_Q6_GPLL_ENA_VOTE_RMSK)
#define HWIO_GCC_MSS_Q6_GPLL_ENA_VOTE_INM(m)      \
        in_dword_masked(HWIO_GCC_MSS_Q6_GPLL_ENA_VOTE_ADDR, m)
#define HWIO_GCC_MSS_Q6_GPLL_ENA_VOTE_OUT(v)      \
        out_dword(HWIO_GCC_MSS_Q6_GPLL_ENA_VOTE_ADDR,v)
#define HWIO_GCC_MSS_Q6_GPLL_ENA_VOTE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_MSS_Q6_GPLL_ENA_VOTE_ADDR,m,v,HWIO_GCC_MSS_Q6_GPLL_ENA_VOTE_IN)
#define HWIO_GCC_MSS_Q6_GPLL_ENA_VOTE_GPLL3_BMSK                                                         0x8
#define HWIO_GCC_MSS_Q6_GPLL_ENA_VOTE_GPLL3_SHFT                                                         0x3
#define HWIO_GCC_MSS_Q6_GPLL_ENA_VOTE_GPLL2_BMSK                                                         0x4
#define HWIO_GCC_MSS_Q6_GPLL_ENA_VOTE_GPLL2_SHFT                                                         0x2
#define HWIO_GCC_MSS_Q6_GPLL_ENA_VOTE_GPLL1_BMSK                                                         0x2
#define HWIO_GCC_MSS_Q6_GPLL_ENA_VOTE_GPLL1_SHFT                                                         0x1
#define HWIO_GCC_MSS_Q6_GPLL_ENA_VOTE_GPLL0_BMSK                                                         0x1
#define HWIO_GCC_MSS_Q6_GPLL_ENA_VOTE_GPLL0_SHFT                                                         0x0

#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_ADDR                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x0001b004)
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_RMSK                                                   0x3f7ff
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_IN          \
        in_dword_masked(HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_ADDR, HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_RMSK)
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_INM(m)      \
        in_dword_masked(HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_ADDR, m)
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_OUT(v)      \
        out_dword(HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_ADDR,v)
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_ADDR,m,v,HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_IN)
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_IMEM_AXI_CLK_ENA_BMSK                                  0x20000
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_IMEM_AXI_CLK_ENA_SHFT                                     0x11
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_SYS_NOC_APSS_AHB_CLK_ENA_BMSK                          0x10000
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_SYS_NOC_APSS_AHB_CLK_ENA_SHFT                             0x10
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_BIMC_APSS_AXI_CLK_ENA_BMSK                              0x8000
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_BIMC_APSS_AXI_CLK_ENA_SHFT                                 0xf
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_APSS_AHB_CLK_ENA_BMSK                                   0x4000
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_APSS_AHB_CLK_ENA_SHFT                                      0xe
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_APSS_AXI_CLK_ENA_BMSK                                   0x2000
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_APSS_AXI_CLK_ENA_SHFT                                      0xd
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_MPM_AHB_CLK_ENA_BMSK                                    0x1000
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_MPM_AHB_CLK_ENA_SHFT                                       0xc
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_BLSP1_AHB_CLK_ENA_BMSK                                   0x400
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_BLSP1_AHB_CLK_ENA_SHFT                                     0xa
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_BLSP1_SLEEP_CLK_ENA_BMSK                                 0x200
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_BLSP1_SLEEP_CLK_ENA_SHFT                                   0x9
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_PRNG_AHB_CLK_ENA_BMSK                                    0x100
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_PRNG_AHB_CLK_ENA_SHFT                                      0x8
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_BOOT_ROM_AHB_CLK_ENA_BMSK                                 0x80
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_BOOT_ROM_AHB_CLK_ENA_SHFT                                  0x7
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_MSG_RAM_AHB_CLK_ENA_BMSK                                  0x40
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_MSG_RAM_AHB_CLK_ENA_SHFT                                   0x6
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_TLMM_AHB_CLK_ENA_BMSK                                     0x20
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_TLMM_AHB_CLK_ENA_SHFT                                      0x5
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_TLMM_CLK_ENA_BMSK                                         0x10
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_TLMM_CLK_ENA_SHFT                                          0x4
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_SPMI_PCNOC_AHB_CLK_ENA_BMSK                                0x8
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_SPMI_PCNOC_AHB_CLK_ENA_SHFT                                0x3
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_CRYPTO_CLK_ENA_BMSK                                        0x4
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_CRYPTO_CLK_ENA_SHFT                                        0x2
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_CRYPTO_AXI_CLK_ENA_BMSK                                    0x2
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_CRYPTO_AXI_CLK_ENA_SHFT                                    0x1
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_CRYPTO_AHB_CLK_ENA_BMSK                                    0x1
#define HWIO_GCC_MSS_Q6_CLOCK_BRANCH_ENA_VOTE_CRYPTO_AHB_CLK_ENA_SHFT                                    0x0

#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_ADDR                                                 (GCC_CLK_CTL_REG_REG_BASE      + 0x0001b008)
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_RMSK                                                    0x3f7ff
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_IN          \
        in_dword_masked(HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_ADDR, HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_RMSK)
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_INM(m)      \
        in_dword_masked(HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_ADDR, m)
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_OUT(v)      \
        out_dword(HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_ADDR,v)
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_ADDR,m,v,HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_IN)
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_IMEM_AXI_CLK_SLEEP_ENA_BMSK                             0x20000
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_IMEM_AXI_CLK_SLEEP_ENA_SHFT                                0x11
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_SYS_NOC_APSS_AHB_CLK_SLEEP_ENA_BMSK                     0x10000
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_SYS_NOC_APSS_AHB_CLK_SLEEP_ENA_SHFT                        0x10
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_BIMC_APSS_AXI_CLK_SLEEP_ENA_BMSK                         0x8000
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_BIMC_APSS_AXI_CLK_SLEEP_ENA_SHFT                            0xf
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_APSS_AHB_CLK_SLEEP_ENA_BMSK                              0x4000
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_APSS_AHB_CLK_SLEEP_ENA_SHFT                                 0xe
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_APSS_AXI_CLK_SLEEP_ENA_BMSK                              0x2000
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_APSS_AXI_CLK_SLEEP_ENA_SHFT                                 0xd
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_MPM_AHB_CLK_SLEEP_ENA_BMSK                               0x1000
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_MPM_AHB_CLK_SLEEP_ENA_SHFT                                  0xc
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_BLSP1_AHB_CLK_SLEEP_ENA_BMSK                              0x400
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_BLSP1_AHB_CLK_SLEEP_ENA_SHFT                                0xa
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_BLSP1_SLEEP_CLK_SLEEP_ENA_BMSK                            0x200
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_BLSP1_SLEEP_CLK_SLEEP_ENA_SHFT                              0x9
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_PRNG_AHB_CLK_SLEEP_ENA_BMSK                               0x100
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_PRNG_AHB_CLK_SLEEP_ENA_SHFT                                 0x8
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_BOOT_ROM_AHB_CLK_SLEEP_ENA_BMSK                            0x80
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_BOOT_ROM_AHB_CLK_SLEEP_ENA_SHFT                             0x7
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_MSG_RAM_AHB_CLK_SLEEP_ENA_BMSK                             0x40
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_MSG_RAM_AHB_CLK_SLEEP_ENA_SHFT                              0x6
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_TLMM_AHB_CLK_SLEEP_ENA_BMSK                                0x20
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_TLMM_AHB_CLK_SLEEP_ENA_SHFT                                 0x5
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_TLMM_CLK_SLEEP_ENA_BMSK                                    0x10
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_TLMM_CLK_SLEEP_ENA_SHFT                                     0x4
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_SPMI_PCNOC_AHB_CLK_SLEEP_ENA_BMSK                           0x8
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_SPMI_PCNOC_AHB_CLK_SLEEP_ENA_SHFT                           0x3
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_CRYPTO_CLK_SLEEP_ENA_BMSK                                   0x4
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_CRYPTO_CLK_SLEEP_ENA_SHFT                                   0x2
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_CRYPTO_AXI_CLK_SLEEP_ENA_BMSK                               0x2
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_CRYPTO_AXI_CLK_SLEEP_ENA_SHFT                               0x1
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_CRYPTO_AHB_CLK_SLEEP_ENA_BMSK                               0x1
#define HWIO_GCC_MSS_Q6_CLOCK_SLEEP_ENA_VOTE_CRYPTO_AHB_CLK_SLEEP_ENA_SHFT                               0x0

#define HWIO_GCC_SPARE_GPLL_ENA_VOTE_ADDR                                                         (GCC_CLK_CTL_REG_REG_BASE      + 0x00000000)
#define HWIO_GCC_SPARE_GPLL_ENA_VOTE_RMSK                                                                0xf
#define HWIO_GCC_SPARE_GPLL_ENA_VOTE_IN          \
        in_dword_masked(HWIO_GCC_SPARE_GPLL_ENA_VOTE_ADDR, HWIO_GCC_SPARE_GPLL_ENA_VOTE_RMSK)
#define HWIO_GCC_SPARE_GPLL_ENA_VOTE_INM(m)      \
        in_dword_masked(HWIO_GCC_SPARE_GPLL_ENA_VOTE_ADDR, m)
#define HWIO_GCC_SPARE_GPLL_ENA_VOTE_OUT(v)      \
        out_dword(HWIO_GCC_SPARE_GPLL_ENA_VOTE_ADDR,v)
#define HWIO_GCC_SPARE_GPLL_ENA_VOTE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SPARE_GPLL_ENA_VOTE_ADDR,m,v,HWIO_GCC_SPARE_GPLL_ENA_VOTE_IN)
#define HWIO_GCC_SPARE_GPLL_ENA_VOTE_GPLL3_BMSK                                                          0x8
#define HWIO_GCC_SPARE_GPLL_ENA_VOTE_GPLL3_SHFT                                                          0x3
#define HWIO_GCC_SPARE_GPLL_ENA_VOTE_GPLL2_BMSK                                                          0x4
#define HWIO_GCC_SPARE_GPLL_ENA_VOTE_GPLL2_SHFT                                                          0x2
#define HWIO_GCC_SPARE_GPLL_ENA_VOTE_GPLL1_BMSK                                                          0x2
#define HWIO_GCC_SPARE_GPLL_ENA_VOTE_GPLL1_SHFT                                                          0x1
#define HWIO_GCC_SPARE_GPLL_ENA_VOTE_GPLL0_BMSK                                                          0x1
#define HWIO_GCC_SPARE_GPLL_ENA_VOTE_GPLL0_SHFT                                                          0x0

#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_ADDR                                                 (GCC_CLK_CTL_REG_REG_BASE      + 0x00000004)
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_RMSK                                                    0x3f7ff
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_IN          \
        in_dword_masked(HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_ADDR, HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_RMSK)
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_INM(m)      \
        in_dword_masked(HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_ADDR, m)
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_OUT(v)      \
        out_dword(HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_ADDR,v)
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_ADDR,m,v,HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_IN)
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_IMEM_AXI_CLK_ENA_BMSK                                   0x20000
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_IMEM_AXI_CLK_ENA_SHFT                                      0x11
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_SYS_NOC_APSS_AHB_CLK_ENA_BMSK                           0x10000
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_SYS_NOC_APSS_AHB_CLK_ENA_SHFT                              0x10
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_BIMC_APSS_AXI_CLK_ENA_BMSK                               0x8000
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_BIMC_APSS_AXI_CLK_ENA_SHFT                                  0xf
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_APSS_AHB_CLK_ENA_BMSK                                    0x4000
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_APSS_AHB_CLK_ENA_SHFT                                       0xe
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_APSS_AXI_CLK_ENA_BMSK                                    0x2000
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_APSS_AXI_CLK_ENA_SHFT                                       0xd
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_MPM_AHB_CLK_ENA_BMSK                                     0x1000
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_MPM_AHB_CLK_ENA_SHFT                                        0xc
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_BLSP1_AHB_CLK_ENA_BMSK                                    0x400
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_BLSP1_AHB_CLK_ENA_SHFT                                      0xa
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_BLSP1_SLEEP_CLK_ENA_BMSK                                  0x200
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_BLSP1_SLEEP_CLK_ENA_SHFT                                    0x9
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_PRNG_AHB_CLK_ENA_BMSK                                     0x100
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_PRNG_AHB_CLK_ENA_SHFT                                       0x8
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_BOOT_ROM_AHB_CLK_ENA_BMSK                                  0x80
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_BOOT_ROM_AHB_CLK_ENA_SHFT                                   0x7
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_MSG_RAM_AHB_CLK_ENA_BMSK                                   0x40
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_MSG_RAM_AHB_CLK_ENA_SHFT                                    0x6
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_TLMM_AHB_CLK_ENA_BMSK                                      0x20
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_TLMM_AHB_CLK_ENA_SHFT                                       0x5
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_TLMM_CLK_ENA_BMSK                                          0x10
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_TLMM_CLK_ENA_SHFT                                           0x4
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_SPMI_PCNOC_AHB_CLK_ENA_BMSK                                 0x8
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_SPMI_PCNOC_AHB_CLK_ENA_SHFT                                 0x3
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_CRYPTO_CLK_ENA_BMSK                                         0x4
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_CRYPTO_CLK_ENA_SHFT                                         0x2
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_CRYPTO_AXI_CLK_ENA_BMSK                                     0x2
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_CRYPTO_AXI_CLK_ENA_SHFT                                     0x1
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_CRYPTO_AHB_CLK_ENA_BMSK                                     0x1
#define HWIO_GCC_SPARE_CLOCK_BRANCH_ENA_VOTE_CRYPTO_AHB_CLK_ENA_SHFT                                     0x0

#define HWIO_GCC_MSS_RESTART_ADDR                                                                 (GCC_CLK_CTL_REG_REG_BASE      + 0x00010000)
#define HWIO_GCC_MSS_RESTART_RMSK                                                                        0x1
#define HWIO_GCC_MSS_RESTART_IN          \
        in_dword_masked(HWIO_GCC_MSS_RESTART_ADDR, HWIO_GCC_MSS_RESTART_RMSK)
#define HWIO_GCC_MSS_RESTART_INM(m)      \
        in_dword_masked(HWIO_GCC_MSS_RESTART_ADDR, m)
#define HWIO_GCC_MSS_RESTART_OUT(v)      \
        out_dword(HWIO_GCC_MSS_RESTART_ADDR,v)
#define HWIO_GCC_MSS_RESTART_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_MSS_RESTART_ADDR,m,v,HWIO_GCC_MSS_RESTART_IN)
#define HWIO_GCC_MSS_RESTART_MSS_RESTART_BMSK                                                            0x1
#define HWIO_GCC_MSS_RESTART_MSS_RESTART_SHFT                                                            0x0

#define HWIO_GCC_RESET_DEBUG_ADDR                                                                 (GCC_CLK_CTL_REG_REG_BASE      + 0x00014000)
#define HWIO_GCC_RESET_DEBUG_RMSK                                                                   0xffffff
#define HWIO_GCC_RESET_DEBUG_IN          \
        in_dword_masked(HWIO_GCC_RESET_DEBUG_ADDR, HWIO_GCC_RESET_DEBUG_RMSK)
#define HWIO_GCC_RESET_DEBUG_INM(m)      \
        in_dword_masked(HWIO_GCC_RESET_DEBUG_ADDR, m)
#define HWIO_GCC_RESET_DEBUG_OUT(v)      \
        out_dword(HWIO_GCC_RESET_DEBUG_ADDR,v)
#define HWIO_GCC_RESET_DEBUG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_RESET_DEBUG_ADDR,m,v,HWIO_GCC_RESET_DEBUG_IN)
#define HWIO_GCC_RESET_DEBUG_MSFT_DBG_RQST_BMSK                                                     0x800000
#define HWIO_GCC_RESET_DEBUG_MSFT_DBG_RQST_SHFT                                                         0x17
#define HWIO_GCC_RESET_DEBUG_BLOCK_RESIN_BMSK                                                       0x400000
#define HWIO_GCC_RESET_DEBUG_BLOCK_RESIN_SHFT                                                           0x16
#define HWIO_GCC_RESET_DEBUG_RESET_ACCESS_FIRST_PASS_BMSK                                           0x200000
#define HWIO_GCC_RESET_DEBUG_RESET_ACCESS_FIRST_PASS_SHFT                                               0x15
#define HWIO_GCC_RESET_DEBUG_RESET_DEBUG_FIRST_PASS_BMSK                                            0x100000
#define HWIO_GCC_RESET_DEBUG_RESET_DEBUG_FIRST_PASS_SHFT                                                0x14
#define HWIO_GCC_RESET_DEBUG_MSM_TSENSE_RESET_DEBUG_EN_BMSK                                          0x80000
#define HWIO_GCC_RESET_DEBUG_MSM_TSENSE_RESET_DEBUG_EN_SHFT                                             0x13
#define HWIO_GCC_RESET_DEBUG_PMIC_ABNORMAL_RESET_DEBUG_EN_BMSK                                       0x40000
#define HWIO_GCC_RESET_DEBUG_PMIC_ABNORMAL_RESET_DEBUG_EN_SHFT                                          0x12
#define HWIO_GCC_RESET_DEBUG_SECURE_WDOG_DEBUG_EN_BMSK                                               0x20000
#define HWIO_GCC_RESET_DEBUG_SECURE_WDOG_DEBUG_EN_SHFT                                                  0x11
#define HWIO_GCC_RESET_DEBUG_PROC_HALT_EN_BMSK                                                       0x10000
#define HWIO_GCC_RESET_DEBUG_PROC_HALT_EN_SHFT                                                          0x10
#define HWIO_GCC_RESET_DEBUG_SRST_V1_MODE_BMSK                                                        0x8000
#define HWIO_GCC_RESET_DEBUG_SRST_V1_MODE_SHFT                                                           0xf
#define HWIO_GCC_RESET_DEBUG_PRE_ARES_DEBUG_TIMER_VAL_BMSK                                            0x7fff
#define HWIO_GCC_RESET_DEBUG_PRE_ARES_DEBUG_TIMER_VAL_SHFT                                               0x0

#define HWIO_GCC_FLUSH_ETR_DEBUG_TIMER_ADDR                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x00015000)
#define HWIO_GCC_FLUSH_ETR_DEBUG_TIMER_RMSK                                                           0xffff
#define HWIO_GCC_FLUSH_ETR_DEBUG_TIMER_IN          \
        in_dword_masked(HWIO_GCC_FLUSH_ETR_DEBUG_TIMER_ADDR, HWIO_GCC_FLUSH_ETR_DEBUG_TIMER_RMSK)
#define HWIO_GCC_FLUSH_ETR_DEBUG_TIMER_INM(m)      \
        in_dword_masked(HWIO_GCC_FLUSH_ETR_DEBUG_TIMER_ADDR, m)
#define HWIO_GCC_FLUSH_ETR_DEBUG_TIMER_OUT(v)      \
        out_dword(HWIO_GCC_FLUSH_ETR_DEBUG_TIMER_ADDR,v)
#define HWIO_GCC_FLUSH_ETR_DEBUG_TIMER_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_FLUSH_ETR_DEBUG_TIMER_ADDR,m,v,HWIO_GCC_FLUSH_ETR_DEBUG_TIMER_IN)
#define HWIO_GCC_FLUSH_ETR_DEBUG_TIMER_FLUSH_ETR_DEBUG_TIMER_VAL_BMSK                                 0xffff
#define HWIO_GCC_FLUSH_ETR_DEBUG_TIMER_FLUSH_ETR_DEBUG_TIMER_VAL_SHFT                                    0x0

#define HWIO_GCC_STOP_CAPTURE_DEBUG_TIMER_ADDR                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x00015004)
#define HWIO_GCC_STOP_CAPTURE_DEBUG_TIMER_RMSK                                                    0xffffffff
#define HWIO_GCC_STOP_CAPTURE_DEBUG_TIMER_IN          \
        in_dword_masked(HWIO_GCC_STOP_CAPTURE_DEBUG_TIMER_ADDR, HWIO_GCC_STOP_CAPTURE_DEBUG_TIMER_RMSK)
#define HWIO_GCC_STOP_CAPTURE_DEBUG_TIMER_INM(m)      \
        in_dword_masked(HWIO_GCC_STOP_CAPTURE_DEBUG_TIMER_ADDR, m)
#define HWIO_GCC_STOP_CAPTURE_DEBUG_TIMER_OUT(v)      \
        out_dword(HWIO_GCC_STOP_CAPTURE_DEBUG_TIMER_ADDR,v)
#define HWIO_GCC_STOP_CAPTURE_DEBUG_TIMER_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_STOP_CAPTURE_DEBUG_TIMER_ADDR,m,v,HWIO_GCC_STOP_CAPTURE_DEBUG_TIMER_IN)
#define HWIO_GCC_STOP_CAPTURE_DEBUG_TIMER_RESERVE_BITS31_16_BMSK                                  0xffff0000
#define HWIO_GCC_STOP_CAPTURE_DEBUG_TIMER_RESERVE_BITS31_16_SHFT                                        0x10
#define HWIO_GCC_STOP_CAPTURE_DEBUG_TIMER_STOP_CAPTURE_DEBUG_TIMER_VAL_BMSK                           0xffff
#define HWIO_GCC_STOP_CAPTURE_DEBUG_TIMER_STOP_CAPTURE_DEBUG_TIMER_VAL_SHFT                              0x0

#define HWIO_GCC_RESET_STATUS_ADDR                                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x00015008)
#define HWIO_GCC_RESET_STATUS_RMSK                                                                      0x3f
#define HWIO_GCC_RESET_STATUS_IN          \
        in_dword_masked(HWIO_GCC_RESET_STATUS_ADDR, HWIO_GCC_RESET_STATUS_RMSK)
#define HWIO_GCC_RESET_STATUS_INM(m)      \
        in_dword_masked(HWIO_GCC_RESET_STATUS_ADDR, m)
#define HWIO_GCC_RESET_STATUS_OUT(v)      \
        out_dword(HWIO_GCC_RESET_STATUS_ADDR,v)
#define HWIO_GCC_RESET_STATUS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_RESET_STATUS_ADDR,m,v,HWIO_GCC_RESET_STATUS_IN)
#define HWIO_GCC_RESET_STATUS_SECURE_WDOG_EXPIRE_STATUS_BMSK                                            0x20
#define HWIO_GCC_RESET_STATUS_SECURE_WDOG_EXPIRE_STATUS_SHFT                                             0x5
#define HWIO_GCC_RESET_STATUS_PMIC_ABNORMAL_RESIN_STATUS_BMSK                                           0x10
#define HWIO_GCC_RESET_STATUS_PMIC_ABNORMAL_RESIN_STATUS_SHFT                                            0x4
#define HWIO_GCC_RESET_STATUS_TSENSE_RESET_STATUS_BMSK                                                   0x8
#define HWIO_GCC_RESET_STATUS_TSENSE_RESET_STATUS_SHFT                                                   0x3
#define HWIO_GCC_RESET_STATUS_SRST_STATUS_BMSK                                                           0x4
#define HWIO_GCC_RESET_STATUS_SRST_STATUS_SHFT                                                           0x2
#define HWIO_GCC_RESET_STATUS_DEBUG_RESET_STATUS_BMSK                                                    0x3
#define HWIO_GCC_RESET_STATUS_DEBUG_RESET_STATUS_SHFT                                                    0x0

#define HWIO_GCC_SW_SRST_ADDR                                                                     (GCC_CLK_CTL_REG_REG_BASE      + 0x0001500c)
#define HWIO_GCC_SW_SRST_RMSK                                                                            0x1
#define HWIO_GCC_SW_SRST_IN          \
        in_dword_masked(HWIO_GCC_SW_SRST_ADDR, HWIO_GCC_SW_SRST_RMSK)
#define HWIO_GCC_SW_SRST_INM(m)      \
        in_dword_masked(HWIO_GCC_SW_SRST_ADDR, m)
#define HWIO_GCC_SW_SRST_OUT(v)      \
        out_dword(HWIO_GCC_SW_SRST_ADDR,v)
#define HWIO_GCC_SW_SRST_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SW_SRST_ADDR,m,v,HWIO_GCC_SW_SRST_IN)
#define HWIO_GCC_SW_SRST_SW_SRST_BMSK                                                                    0x1
#define HWIO_GCC_SW_SRST_SW_SRST_SHFT                                                                    0x0

#define HWIO_GCC_PROC_HALT_ADDR                                                                   (GCC_CLK_CTL_REG_REG_BASE      + 0x0001301c)
#define HWIO_GCC_PROC_HALT_RMSK                                                                          0x1
#define HWIO_GCC_PROC_HALT_IN          \
        in_dword_masked(HWIO_GCC_PROC_HALT_ADDR, HWIO_GCC_PROC_HALT_RMSK)
#define HWIO_GCC_PROC_HALT_INM(m)      \
        in_dword_masked(HWIO_GCC_PROC_HALT_ADDR, m)
#define HWIO_GCC_PROC_HALT_OUT(v)      \
        out_dword(HWIO_GCC_PROC_HALT_ADDR,v)
#define HWIO_GCC_PROC_HALT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PROC_HALT_ADDR,m,v,HWIO_GCC_PROC_HALT_IN)
#define HWIO_GCC_PROC_HALT_PROC_HALT_BMSK                                                                0x1
#define HWIO_GCC_PROC_HALT_PROC_HALT_SHFT                                                                0x0

#define HWIO_GCC_GCC_DEBUG_CLK_CTL_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x00074000)
#define HWIO_GCC_GCC_DEBUG_CLK_CTL_RMSK                                                           0xffc1f3ff
#define HWIO_GCC_GCC_DEBUG_CLK_CTL_IN          \
        in_dword_masked(HWIO_GCC_GCC_DEBUG_CLK_CTL_ADDR, HWIO_GCC_GCC_DEBUG_CLK_CTL_RMSK)
#define HWIO_GCC_GCC_DEBUG_CLK_CTL_INM(m)      \
        in_dword_masked(HWIO_GCC_GCC_DEBUG_CLK_CTL_ADDR, m)
#define HWIO_GCC_GCC_DEBUG_CLK_CTL_OUT(v)      \
        out_dword(HWIO_GCC_GCC_DEBUG_CLK_CTL_ADDR,v)
#define HWIO_GCC_GCC_DEBUG_CLK_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GCC_DEBUG_CLK_CTL_ADDR,m,v,HWIO_GCC_GCC_DEBUG_CLK_CTL_IN)
#define HWIO_GCC_GCC_DEBUG_CLK_CTL_RESETN_MUX_SEL_BMSK                                            0xe0000000
#define HWIO_GCC_GCC_DEBUG_CLK_CTL_RESETN_MUX_SEL_SHFT                                                  0x1d
#define HWIO_GCC_GCC_DEBUG_CLK_CTL_BYPASSNL_MUX_SEL_BMSK                                          0x1c000000
#define HWIO_GCC_GCC_DEBUG_CLK_CTL_BYPASSNL_MUX_SEL_SHFT                                                0x1a
#define HWIO_GCC_GCC_DEBUG_CLK_CTL_PLL_DEBUG_BUS_STATUS_SEL_BMSK                                   0x3800000
#define HWIO_GCC_GCC_DEBUG_CLK_CTL_PLL_DEBUG_BUS_STATUS_SEL_SHFT                                        0x17
#define HWIO_GCC_GCC_DEBUG_CLK_CTL_GCC_DEBUG_BUS_ENABLE_BMSK                                        0x400000
#define HWIO_GCC_GCC_DEBUG_CLK_CTL_GCC_DEBUG_BUS_ENABLE_SHFT                                            0x16
#define HWIO_GCC_GCC_DEBUG_CLK_CTL_CLK_ENABLE_BMSK                                                   0x10000
#define HWIO_GCC_GCC_DEBUG_CLK_CTL_CLK_ENABLE_SHFT                                                      0x10
#define HWIO_GCC_GCC_DEBUG_CLK_CTL_POST_DIV_BMSK                                                      0xf000
#define HWIO_GCC_GCC_DEBUG_CLK_CTL_POST_DIV_SHFT                                                         0xc
#define HWIO_GCC_GCC_DEBUG_CLK_CTL_MUX_SEL_BMSK                                                        0x3ff
#define HWIO_GCC_GCC_DEBUG_CLK_CTL_MUX_SEL_SHFT                                                          0x0

#define HWIO_GCC_CLOCK_FRQ_MEASURE_CTL_ADDR                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x00074004)
#define HWIO_GCC_CLOCK_FRQ_MEASURE_CTL_RMSK                                                         0x1fffff
#define HWIO_GCC_CLOCK_FRQ_MEASURE_CTL_IN          \
        in_dword_masked(HWIO_GCC_CLOCK_FRQ_MEASURE_CTL_ADDR, HWIO_GCC_CLOCK_FRQ_MEASURE_CTL_RMSK)
#define HWIO_GCC_CLOCK_FRQ_MEASURE_CTL_INM(m)      \
        in_dword_masked(HWIO_GCC_CLOCK_FRQ_MEASURE_CTL_ADDR, m)
#define HWIO_GCC_CLOCK_FRQ_MEASURE_CTL_OUT(v)      \
        out_dword(HWIO_GCC_CLOCK_FRQ_MEASURE_CTL_ADDR,v)
#define HWIO_GCC_CLOCK_FRQ_MEASURE_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_CLOCK_FRQ_MEASURE_CTL_ADDR,m,v,HWIO_GCC_CLOCK_FRQ_MEASURE_CTL_IN)
#define HWIO_GCC_CLOCK_FRQ_MEASURE_CTL_CNT_EN_BMSK                                                  0x100000
#define HWIO_GCC_CLOCK_FRQ_MEASURE_CTL_CNT_EN_SHFT                                                      0x14
#define HWIO_GCC_CLOCK_FRQ_MEASURE_CTL_XO_DIV4_TERM_CNT_BMSK                                         0xfffff
#define HWIO_GCC_CLOCK_FRQ_MEASURE_CTL_XO_DIV4_TERM_CNT_SHFT                                             0x0

#define HWIO_GCC_CLOCK_FRQ_MEASURE_STATUS_ADDR                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x00074008)
#define HWIO_GCC_CLOCK_FRQ_MEASURE_STATUS_RMSK                                                     0x3ffffff
#define HWIO_GCC_CLOCK_FRQ_MEASURE_STATUS_IN          \
        in_dword_masked(HWIO_GCC_CLOCK_FRQ_MEASURE_STATUS_ADDR, HWIO_GCC_CLOCK_FRQ_MEASURE_STATUS_RMSK)
#define HWIO_GCC_CLOCK_FRQ_MEASURE_STATUS_INM(m)      \
        in_dword_masked(HWIO_GCC_CLOCK_FRQ_MEASURE_STATUS_ADDR, m)
#define HWIO_GCC_CLOCK_FRQ_MEASURE_STATUS_XO_DIV4_CNT_DONE_BMSK                                    0x2000000
#define HWIO_GCC_CLOCK_FRQ_MEASURE_STATUS_XO_DIV4_CNT_DONE_SHFT                                         0x19
#define HWIO_GCC_CLOCK_FRQ_MEASURE_STATUS_MEASURE_CNT_BMSK                                         0x1ffffff
#define HWIO_GCC_CLOCK_FRQ_MEASURE_STATUS_MEASURE_CNT_SHFT                                               0x0

#define HWIO_GCC_PLLTEST_PAD_CFG_ADDR                                                             (GCC_CLK_CTL_REG_REG_BASE      + 0x0007400c)
#define HWIO_GCC_PLLTEST_PAD_CFG_RMSK                                                             0xfc0f3f3f
#define HWIO_GCC_PLLTEST_PAD_CFG_IN          \
        in_dword_masked(HWIO_GCC_PLLTEST_PAD_CFG_ADDR, HWIO_GCC_PLLTEST_PAD_CFG_RMSK)
#define HWIO_GCC_PLLTEST_PAD_CFG_INM(m)      \
        in_dword_masked(HWIO_GCC_PLLTEST_PAD_CFG_ADDR, m)
#define HWIO_GCC_PLLTEST_PAD_CFG_OUT(v)      \
        out_dword(HWIO_GCC_PLLTEST_PAD_CFG_ADDR,v)
#define HWIO_GCC_PLLTEST_PAD_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PLLTEST_PAD_CFG_ADDR,m,v,HWIO_GCC_PLLTEST_PAD_CFG_IN)
#define HWIO_GCC_PLLTEST_PAD_CFG_RESETN_OUT_SEL_BMSK                                              0xe0000000
#define HWIO_GCC_PLLTEST_PAD_CFG_RESETN_OUT_SEL_SHFT                                                    0x1d
#define HWIO_GCC_PLLTEST_PAD_CFG_BYPASSNL_OUT_SEL_BMSK                                            0x1c000000
#define HWIO_GCC_PLLTEST_PAD_CFG_BYPASSNL_OUT_SEL_SHFT                                                  0x1a
#define HWIO_GCC_PLLTEST_PAD_CFG_PLL_INPUT_N_BMSK                                                    0xc0000
#define HWIO_GCC_PLLTEST_PAD_CFG_PLL_INPUT_N_SHFT                                                       0x12
#define HWIO_GCC_PLLTEST_PAD_CFG_PLL_INPUT_P_BMSK                                                    0x30000
#define HWIO_GCC_PLLTEST_PAD_CFG_PLL_INPUT_P_SHFT                                                       0x10
#define HWIO_GCC_PLLTEST_PAD_CFG_RT_EN_BMSK                                                           0x2000
#define HWIO_GCC_PLLTEST_PAD_CFG_RT_EN_SHFT                                                              0xd
#define HWIO_GCC_PLLTEST_PAD_CFG_CORE_OE_BMSK                                                         0x1000
#define HWIO_GCC_PLLTEST_PAD_CFG_CORE_OE_SHFT                                                            0xc
#define HWIO_GCC_PLLTEST_PAD_CFG_CORE_IE_N_BMSK                                                        0x800
#define HWIO_GCC_PLLTEST_PAD_CFG_CORE_IE_N_SHFT                                                          0xb
#define HWIO_GCC_PLLTEST_PAD_CFG_CORE_IE_P_BMSK                                                        0x400
#define HWIO_GCC_PLLTEST_PAD_CFG_CORE_IE_P_SHFT                                                          0xa
#define HWIO_GCC_PLLTEST_PAD_CFG_CORE_DRIVE_BMSK                                                       0x300
#define HWIO_GCC_PLLTEST_PAD_CFG_CORE_DRIVE_SHFT                                                         0x8
#define HWIO_GCC_PLLTEST_PAD_CFG_OUT_SEL_BMSK                                                           0x3f
#define HWIO_GCC_PLLTEST_PAD_CFG_OUT_SEL_SHFT                                                            0x0

#define HWIO_GCC_JITTER_PROBE_CFG_ADDR                                                            (GCC_CLK_CTL_REG_REG_BASE      + 0x00074010)
#define HWIO_GCC_JITTER_PROBE_CFG_RMSK                                                                 0x1ff
#define HWIO_GCC_JITTER_PROBE_CFG_IN          \
        in_dword_masked(HWIO_GCC_JITTER_PROBE_CFG_ADDR, HWIO_GCC_JITTER_PROBE_CFG_RMSK)
#define HWIO_GCC_JITTER_PROBE_CFG_INM(m)      \
        in_dword_masked(HWIO_GCC_JITTER_PROBE_CFG_ADDR, m)
#define HWIO_GCC_JITTER_PROBE_CFG_OUT(v)      \
        out_dword(HWIO_GCC_JITTER_PROBE_CFG_ADDR,v)
#define HWIO_GCC_JITTER_PROBE_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_JITTER_PROBE_CFG_ADDR,m,v,HWIO_GCC_JITTER_PROBE_CFG_IN)
#define HWIO_GCC_JITTER_PROBE_CFG_JITTER_PROBE_EN_BMSK                                                 0x100
#define HWIO_GCC_JITTER_PROBE_CFG_JITTER_PROBE_EN_SHFT                                                   0x8
#define HWIO_GCC_JITTER_PROBE_CFG_INIT_COUNTER_BMSK                                                     0xff
#define HWIO_GCC_JITTER_PROBE_CFG_INIT_COUNTER_SHFT                                                      0x0

#define HWIO_GCC_JITTER_PROBE_VAL_ADDR                                                            (GCC_CLK_CTL_REG_REG_BASE      + 0x00074014)
#define HWIO_GCC_JITTER_PROBE_VAL_RMSK                                                                  0xff
#define HWIO_GCC_JITTER_PROBE_VAL_IN          \
        in_dword_masked(HWIO_GCC_JITTER_PROBE_VAL_ADDR, HWIO_GCC_JITTER_PROBE_VAL_RMSK)
#define HWIO_GCC_JITTER_PROBE_VAL_INM(m)      \
        in_dword_masked(HWIO_GCC_JITTER_PROBE_VAL_ADDR, m)
#define HWIO_GCC_JITTER_PROBE_VAL_COUNT_VALUE_BMSK                                                      0xff
#define HWIO_GCC_JITTER_PROBE_VAL_COUNT_VALUE_SHFT                                                       0x0

#define HWIO_GCC_GP1_CBCR_ADDR                                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x00008000)
#define HWIO_GCC_GP1_CBCR_RMSK                                                                    0x80000001
#define HWIO_GCC_GP1_CBCR_IN          \
        in_dword_masked(HWIO_GCC_GP1_CBCR_ADDR, HWIO_GCC_GP1_CBCR_RMSK)
#define HWIO_GCC_GP1_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_GP1_CBCR_ADDR, m)
#define HWIO_GCC_GP1_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_GP1_CBCR_ADDR,v)
#define HWIO_GCC_GP1_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GP1_CBCR_ADDR,m,v,HWIO_GCC_GP1_CBCR_IN)
#define HWIO_GCC_GP1_CBCR_CLK_OFF_BMSK                                                            0x80000000
#define HWIO_GCC_GP1_CBCR_CLK_OFF_SHFT                                                                  0x1f
#define HWIO_GCC_GP1_CBCR_CLK_ENABLE_BMSK                                                                0x1
#define HWIO_GCC_GP1_CBCR_CLK_ENABLE_SHFT                                                                0x0

#define HWIO_GCC_GP1_CMD_RCGR_ADDR                                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x00008004)
#define HWIO_GCC_GP1_CMD_RCGR_RMSK                                                                0x800000f3
#define HWIO_GCC_GP1_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_GP1_CMD_RCGR_ADDR, HWIO_GCC_GP1_CMD_RCGR_RMSK)
#define HWIO_GCC_GP1_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_GP1_CMD_RCGR_ADDR, m)
#define HWIO_GCC_GP1_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_GP1_CMD_RCGR_ADDR,v)
#define HWIO_GCC_GP1_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GP1_CMD_RCGR_ADDR,m,v,HWIO_GCC_GP1_CMD_RCGR_IN)
#define HWIO_GCC_GP1_CMD_RCGR_ROOT_OFF_BMSK                                                       0x80000000
#define HWIO_GCC_GP1_CMD_RCGR_ROOT_OFF_SHFT                                                             0x1f
#define HWIO_GCC_GP1_CMD_RCGR_DIRTY_D_BMSK                                                              0x80
#define HWIO_GCC_GP1_CMD_RCGR_DIRTY_D_SHFT                                                               0x7
#define HWIO_GCC_GP1_CMD_RCGR_DIRTY_M_BMSK                                                              0x40
#define HWIO_GCC_GP1_CMD_RCGR_DIRTY_M_SHFT                                                               0x6
#define HWIO_GCC_GP1_CMD_RCGR_DIRTY_N_BMSK                                                              0x20
#define HWIO_GCC_GP1_CMD_RCGR_DIRTY_N_SHFT                                                               0x5
#define HWIO_GCC_GP1_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                                       0x10
#define HWIO_GCC_GP1_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                                        0x4
#define HWIO_GCC_GP1_CMD_RCGR_ROOT_EN_BMSK                                                               0x2
#define HWIO_GCC_GP1_CMD_RCGR_ROOT_EN_SHFT                                                               0x1
#define HWIO_GCC_GP1_CMD_RCGR_UPDATE_BMSK                                                                0x1
#define HWIO_GCC_GP1_CMD_RCGR_UPDATE_SHFT                                                                0x0

#define HWIO_GCC_GP1_CFG_RCGR_ADDR                                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x00008008)
#define HWIO_GCC_GP1_CFG_RCGR_RMSK                                                                    0x371f
#define HWIO_GCC_GP1_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_GP1_CFG_RCGR_ADDR, HWIO_GCC_GP1_CFG_RCGR_RMSK)
#define HWIO_GCC_GP1_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_GP1_CFG_RCGR_ADDR, m)
#define HWIO_GCC_GP1_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_GP1_CFG_RCGR_ADDR,v)
#define HWIO_GCC_GP1_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GP1_CFG_RCGR_ADDR,m,v,HWIO_GCC_GP1_CFG_RCGR_IN)
#define HWIO_GCC_GP1_CFG_RCGR_MODE_BMSK                                                               0x3000
#define HWIO_GCC_GP1_CFG_RCGR_MODE_SHFT                                                                  0xc
#define HWIO_GCC_GP1_CFG_RCGR_SRC_SEL_BMSK                                                             0x700
#define HWIO_GCC_GP1_CFG_RCGR_SRC_SEL_SHFT                                                               0x8
#define HWIO_GCC_GP1_CFG_RCGR_SRC_DIV_BMSK                                                              0x1f
#define HWIO_GCC_GP1_CFG_RCGR_SRC_DIV_SHFT                                                               0x0

#define HWIO_GCC_GP1_M_ADDR                                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x0000800c)
#define HWIO_GCC_GP1_M_RMSK                                                                             0xff
#define HWIO_GCC_GP1_M_IN          \
        in_dword_masked(HWIO_GCC_GP1_M_ADDR, HWIO_GCC_GP1_M_RMSK)
#define HWIO_GCC_GP1_M_INM(m)      \
        in_dword_masked(HWIO_GCC_GP1_M_ADDR, m)
#define HWIO_GCC_GP1_M_OUT(v)      \
        out_dword(HWIO_GCC_GP1_M_ADDR,v)
#define HWIO_GCC_GP1_M_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GP1_M_ADDR,m,v,HWIO_GCC_GP1_M_IN)
#define HWIO_GCC_GP1_M_M_BMSK                                                                           0xff
#define HWIO_GCC_GP1_M_M_SHFT                                                                            0x0

#define HWIO_GCC_GP1_N_ADDR                                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x00008010)
#define HWIO_GCC_GP1_N_RMSK                                                                             0xff
#define HWIO_GCC_GP1_N_IN          \
        in_dword_masked(HWIO_GCC_GP1_N_ADDR, HWIO_GCC_GP1_N_RMSK)
#define HWIO_GCC_GP1_N_INM(m)      \
        in_dword_masked(HWIO_GCC_GP1_N_ADDR, m)
#define HWIO_GCC_GP1_N_OUT(v)      \
        out_dword(HWIO_GCC_GP1_N_ADDR,v)
#define HWIO_GCC_GP1_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GP1_N_ADDR,m,v,HWIO_GCC_GP1_N_IN)
#define HWIO_GCC_GP1_N_NOT_N_MINUS_M_BMSK                                                               0xff
#define HWIO_GCC_GP1_N_NOT_N_MINUS_M_SHFT                                                                0x0

#define HWIO_GCC_GP1_D_ADDR                                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x00008014)
#define HWIO_GCC_GP1_D_RMSK                                                                             0xff
#define HWIO_GCC_GP1_D_IN          \
        in_dword_masked(HWIO_GCC_GP1_D_ADDR, HWIO_GCC_GP1_D_RMSK)
#define HWIO_GCC_GP1_D_INM(m)      \
        in_dword_masked(HWIO_GCC_GP1_D_ADDR, m)
#define HWIO_GCC_GP1_D_OUT(v)      \
        out_dword(HWIO_GCC_GP1_D_ADDR,v)
#define HWIO_GCC_GP1_D_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GP1_D_ADDR,m,v,HWIO_GCC_GP1_D_IN)
#define HWIO_GCC_GP1_D_NOT_2D_BMSK                                                                      0xff
#define HWIO_GCC_GP1_D_NOT_2D_SHFT                                                                       0x0

#define HWIO_GCC_GP2_CBCR_ADDR                                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x00009000)
#define HWIO_GCC_GP2_CBCR_RMSK                                                                    0x80000001
#define HWIO_GCC_GP2_CBCR_IN          \
        in_dword_masked(HWIO_GCC_GP2_CBCR_ADDR, HWIO_GCC_GP2_CBCR_RMSK)
#define HWIO_GCC_GP2_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_GP2_CBCR_ADDR, m)
#define HWIO_GCC_GP2_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_GP2_CBCR_ADDR,v)
#define HWIO_GCC_GP2_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GP2_CBCR_ADDR,m,v,HWIO_GCC_GP2_CBCR_IN)
#define HWIO_GCC_GP2_CBCR_CLK_OFF_BMSK                                                            0x80000000
#define HWIO_GCC_GP2_CBCR_CLK_OFF_SHFT                                                                  0x1f
#define HWIO_GCC_GP2_CBCR_CLK_ENABLE_BMSK                                                                0x1
#define HWIO_GCC_GP2_CBCR_CLK_ENABLE_SHFT                                                                0x0

#define HWIO_GCC_GP2_CMD_RCGR_ADDR                                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x00009004)
#define HWIO_GCC_GP2_CMD_RCGR_RMSK                                                                0x800000f3
#define HWIO_GCC_GP2_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_GP2_CMD_RCGR_ADDR, HWIO_GCC_GP2_CMD_RCGR_RMSK)
#define HWIO_GCC_GP2_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_GP2_CMD_RCGR_ADDR, m)
#define HWIO_GCC_GP2_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_GP2_CMD_RCGR_ADDR,v)
#define HWIO_GCC_GP2_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GP2_CMD_RCGR_ADDR,m,v,HWIO_GCC_GP2_CMD_RCGR_IN)
#define HWIO_GCC_GP2_CMD_RCGR_ROOT_OFF_BMSK                                                       0x80000000
#define HWIO_GCC_GP2_CMD_RCGR_ROOT_OFF_SHFT                                                             0x1f
#define HWIO_GCC_GP2_CMD_RCGR_DIRTY_D_BMSK                                                              0x80
#define HWIO_GCC_GP2_CMD_RCGR_DIRTY_D_SHFT                                                               0x7
#define HWIO_GCC_GP2_CMD_RCGR_DIRTY_M_BMSK                                                              0x40
#define HWIO_GCC_GP2_CMD_RCGR_DIRTY_M_SHFT                                                               0x6
#define HWIO_GCC_GP2_CMD_RCGR_DIRTY_N_BMSK                                                              0x20
#define HWIO_GCC_GP2_CMD_RCGR_DIRTY_N_SHFT                                                               0x5
#define HWIO_GCC_GP2_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                                       0x10
#define HWIO_GCC_GP2_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                                        0x4
#define HWIO_GCC_GP2_CMD_RCGR_ROOT_EN_BMSK                                                               0x2
#define HWIO_GCC_GP2_CMD_RCGR_ROOT_EN_SHFT                                                               0x1
#define HWIO_GCC_GP2_CMD_RCGR_UPDATE_BMSK                                                                0x1
#define HWIO_GCC_GP2_CMD_RCGR_UPDATE_SHFT                                                                0x0

#define HWIO_GCC_GP2_CFG_RCGR_ADDR                                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x00009008)
#define HWIO_GCC_GP2_CFG_RCGR_RMSK                                                                    0x371f
#define HWIO_GCC_GP2_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_GP2_CFG_RCGR_ADDR, HWIO_GCC_GP2_CFG_RCGR_RMSK)
#define HWIO_GCC_GP2_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_GP2_CFG_RCGR_ADDR, m)
#define HWIO_GCC_GP2_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_GP2_CFG_RCGR_ADDR,v)
#define HWIO_GCC_GP2_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GP2_CFG_RCGR_ADDR,m,v,HWIO_GCC_GP2_CFG_RCGR_IN)
#define HWIO_GCC_GP2_CFG_RCGR_MODE_BMSK                                                               0x3000
#define HWIO_GCC_GP2_CFG_RCGR_MODE_SHFT                                                                  0xc
#define HWIO_GCC_GP2_CFG_RCGR_SRC_SEL_BMSK                                                             0x700
#define HWIO_GCC_GP2_CFG_RCGR_SRC_SEL_SHFT                                                               0x8
#define HWIO_GCC_GP2_CFG_RCGR_SRC_DIV_BMSK                                                              0x1f
#define HWIO_GCC_GP2_CFG_RCGR_SRC_DIV_SHFT                                                               0x0

#define HWIO_GCC_GP2_M_ADDR                                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x0000900c)
#define HWIO_GCC_GP2_M_RMSK                                                                             0xff
#define HWIO_GCC_GP2_M_IN          \
        in_dword_masked(HWIO_GCC_GP2_M_ADDR, HWIO_GCC_GP2_M_RMSK)
#define HWIO_GCC_GP2_M_INM(m)      \
        in_dword_masked(HWIO_GCC_GP2_M_ADDR, m)
#define HWIO_GCC_GP2_M_OUT(v)      \
        out_dword(HWIO_GCC_GP2_M_ADDR,v)
#define HWIO_GCC_GP2_M_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GP2_M_ADDR,m,v,HWIO_GCC_GP2_M_IN)
#define HWIO_GCC_GP2_M_M_BMSK                                                                           0xff
#define HWIO_GCC_GP2_M_M_SHFT                                                                            0x0

#define HWIO_GCC_GP2_N_ADDR                                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x00009010)
#define HWIO_GCC_GP2_N_RMSK                                                                             0xff
#define HWIO_GCC_GP2_N_IN          \
        in_dword_masked(HWIO_GCC_GP2_N_ADDR, HWIO_GCC_GP2_N_RMSK)
#define HWIO_GCC_GP2_N_INM(m)      \
        in_dword_masked(HWIO_GCC_GP2_N_ADDR, m)
#define HWIO_GCC_GP2_N_OUT(v)      \
        out_dword(HWIO_GCC_GP2_N_ADDR,v)
#define HWIO_GCC_GP2_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GP2_N_ADDR,m,v,HWIO_GCC_GP2_N_IN)
#define HWIO_GCC_GP2_N_NOT_N_MINUS_M_BMSK                                                               0xff
#define HWIO_GCC_GP2_N_NOT_N_MINUS_M_SHFT                                                                0x0

#define HWIO_GCC_GP2_D_ADDR                                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x00009014)
#define HWIO_GCC_GP2_D_RMSK                                                                             0xff
#define HWIO_GCC_GP2_D_IN          \
        in_dword_masked(HWIO_GCC_GP2_D_ADDR, HWIO_GCC_GP2_D_RMSK)
#define HWIO_GCC_GP2_D_INM(m)      \
        in_dword_masked(HWIO_GCC_GP2_D_ADDR, m)
#define HWIO_GCC_GP2_D_OUT(v)      \
        out_dword(HWIO_GCC_GP2_D_ADDR,v)
#define HWIO_GCC_GP2_D_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GP2_D_ADDR,m,v,HWIO_GCC_GP2_D_IN)
#define HWIO_GCC_GP2_D_NOT_2D_BMSK                                                                      0xff
#define HWIO_GCC_GP2_D_NOT_2D_SHFT                                                                       0x0

#define HWIO_GCC_GP3_CBCR_ADDR                                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x0000a000)
#define HWIO_GCC_GP3_CBCR_RMSK                                                                    0x80000001
#define HWIO_GCC_GP3_CBCR_IN          \
        in_dword_masked(HWIO_GCC_GP3_CBCR_ADDR, HWIO_GCC_GP3_CBCR_RMSK)
#define HWIO_GCC_GP3_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_GP3_CBCR_ADDR, m)
#define HWIO_GCC_GP3_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_GP3_CBCR_ADDR,v)
#define HWIO_GCC_GP3_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GP3_CBCR_ADDR,m,v,HWIO_GCC_GP3_CBCR_IN)
#define HWIO_GCC_GP3_CBCR_CLK_OFF_BMSK                                                            0x80000000
#define HWIO_GCC_GP3_CBCR_CLK_OFF_SHFT                                                                  0x1f
#define HWIO_GCC_GP3_CBCR_CLK_ENABLE_BMSK                                                                0x1
#define HWIO_GCC_GP3_CBCR_CLK_ENABLE_SHFT                                                                0x0

#define HWIO_GCC_GP3_CMD_RCGR_ADDR                                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x0000a004)
#define HWIO_GCC_GP3_CMD_RCGR_RMSK                                                                0x800000f3
#define HWIO_GCC_GP3_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_GP3_CMD_RCGR_ADDR, HWIO_GCC_GP3_CMD_RCGR_RMSK)
#define HWIO_GCC_GP3_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_GP3_CMD_RCGR_ADDR, m)
#define HWIO_GCC_GP3_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_GP3_CMD_RCGR_ADDR,v)
#define HWIO_GCC_GP3_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GP3_CMD_RCGR_ADDR,m,v,HWIO_GCC_GP3_CMD_RCGR_IN)
#define HWIO_GCC_GP3_CMD_RCGR_ROOT_OFF_BMSK                                                       0x80000000
#define HWIO_GCC_GP3_CMD_RCGR_ROOT_OFF_SHFT                                                             0x1f
#define HWIO_GCC_GP3_CMD_RCGR_DIRTY_D_BMSK                                                              0x80
#define HWIO_GCC_GP3_CMD_RCGR_DIRTY_D_SHFT                                                               0x7
#define HWIO_GCC_GP3_CMD_RCGR_DIRTY_M_BMSK                                                              0x40
#define HWIO_GCC_GP3_CMD_RCGR_DIRTY_M_SHFT                                                               0x6
#define HWIO_GCC_GP3_CMD_RCGR_DIRTY_N_BMSK                                                              0x20
#define HWIO_GCC_GP3_CMD_RCGR_DIRTY_N_SHFT                                                               0x5
#define HWIO_GCC_GP3_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                                       0x10
#define HWIO_GCC_GP3_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                                        0x4
#define HWIO_GCC_GP3_CMD_RCGR_ROOT_EN_BMSK                                                               0x2
#define HWIO_GCC_GP3_CMD_RCGR_ROOT_EN_SHFT                                                               0x1
#define HWIO_GCC_GP3_CMD_RCGR_UPDATE_BMSK                                                                0x1
#define HWIO_GCC_GP3_CMD_RCGR_UPDATE_SHFT                                                                0x0

#define HWIO_GCC_GP3_CFG_RCGR_ADDR                                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x0000a008)
#define HWIO_GCC_GP3_CFG_RCGR_RMSK                                                                    0x371f
#define HWIO_GCC_GP3_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_GP3_CFG_RCGR_ADDR, HWIO_GCC_GP3_CFG_RCGR_RMSK)
#define HWIO_GCC_GP3_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_GP3_CFG_RCGR_ADDR, m)
#define HWIO_GCC_GP3_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_GP3_CFG_RCGR_ADDR,v)
#define HWIO_GCC_GP3_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GP3_CFG_RCGR_ADDR,m,v,HWIO_GCC_GP3_CFG_RCGR_IN)
#define HWIO_GCC_GP3_CFG_RCGR_MODE_BMSK                                                               0x3000
#define HWIO_GCC_GP3_CFG_RCGR_MODE_SHFT                                                                  0xc
#define HWIO_GCC_GP3_CFG_RCGR_SRC_SEL_BMSK                                                             0x700
#define HWIO_GCC_GP3_CFG_RCGR_SRC_SEL_SHFT                                                               0x8
#define HWIO_GCC_GP3_CFG_RCGR_SRC_DIV_BMSK                                                              0x1f
#define HWIO_GCC_GP3_CFG_RCGR_SRC_DIV_SHFT                                                               0x0

#define HWIO_GCC_GP3_M_ADDR                                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x0000a00c)
#define HWIO_GCC_GP3_M_RMSK                                                                             0xff
#define HWIO_GCC_GP3_M_IN          \
        in_dword_masked(HWIO_GCC_GP3_M_ADDR, HWIO_GCC_GP3_M_RMSK)
#define HWIO_GCC_GP3_M_INM(m)      \
        in_dword_masked(HWIO_GCC_GP3_M_ADDR, m)
#define HWIO_GCC_GP3_M_OUT(v)      \
        out_dword(HWIO_GCC_GP3_M_ADDR,v)
#define HWIO_GCC_GP3_M_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GP3_M_ADDR,m,v,HWIO_GCC_GP3_M_IN)
#define HWIO_GCC_GP3_M_M_BMSK                                                                           0xff
#define HWIO_GCC_GP3_M_M_SHFT                                                                            0x0

#define HWIO_GCC_GP3_N_ADDR                                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x0000a010)
#define HWIO_GCC_GP3_N_RMSK                                                                             0xff
#define HWIO_GCC_GP3_N_IN          \
        in_dword_masked(HWIO_GCC_GP3_N_ADDR, HWIO_GCC_GP3_N_RMSK)
#define HWIO_GCC_GP3_N_INM(m)      \
        in_dword_masked(HWIO_GCC_GP3_N_ADDR, m)
#define HWIO_GCC_GP3_N_OUT(v)      \
        out_dword(HWIO_GCC_GP3_N_ADDR,v)
#define HWIO_GCC_GP3_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GP3_N_ADDR,m,v,HWIO_GCC_GP3_N_IN)
#define HWIO_GCC_GP3_N_NOT_N_MINUS_M_BMSK                                                               0xff
#define HWIO_GCC_GP3_N_NOT_N_MINUS_M_SHFT                                                                0x0

#define HWIO_GCC_GP3_D_ADDR                                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x0000a014)
#define HWIO_GCC_GP3_D_RMSK                                                                             0xff
#define HWIO_GCC_GP3_D_IN          \
        in_dword_masked(HWIO_GCC_GP3_D_ADDR, HWIO_GCC_GP3_D_RMSK)
#define HWIO_GCC_GP3_D_INM(m)      \
        in_dword_masked(HWIO_GCC_GP3_D_ADDR, m)
#define HWIO_GCC_GP3_D_OUT(v)      \
        out_dword(HWIO_GCC_GP3_D_ADDR,v)
#define HWIO_GCC_GP3_D_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GP3_D_ADDR,m,v,HWIO_GCC_GP3_D_IN)
#define HWIO_GCC_GP3_D_NOT_2D_BMSK                                                                      0xff
#define HWIO_GCC_GP3_D_NOT_2D_SHFT                                                                       0x0

#define HWIO_GCC_APSS_MISC_ADDR                                                                   (GCC_CLK_CTL_REG_REG_BASE      + 0x00060000)
#define HWIO_GCC_APSS_MISC_RMSK                                                                          0x1
#define HWIO_GCC_APSS_MISC_IN          \
        in_dword_masked(HWIO_GCC_APSS_MISC_ADDR, HWIO_GCC_APSS_MISC_RMSK)
#define HWIO_GCC_APSS_MISC_INM(m)      \
        in_dword_masked(HWIO_GCC_APSS_MISC_ADDR, m)
#define HWIO_GCC_APSS_MISC_OUT(v)      \
        out_dword(HWIO_GCC_APSS_MISC_ADDR,v)
#define HWIO_GCC_APSS_MISC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_APSS_MISC_ADDR,m,v,HWIO_GCC_APSS_MISC_IN)
#define HWIO_GCC_APSS_MISC_RPM_RESET_REMOVAL_BMSK                                                        0x1
#define HWIO_GCC_APSS_MISC_RPM_RESET_REMOVAL_SHFT                                                        0x0

#define HWIO_GCC_TIC_MODE_APSS_BOOT_ADDR                                                          (GCC_CLK_CTL_REG_REG_BASE      + 0x0007f000)
#define HWIO_GCC_TIC_MODE_APSS_BOOT_RMSK                                                                 0x1
#define HWIO_GCC_TIC_MODE_APSS_BOOT_IN          \
        in_dword_masked(HWIO_GCC_TIC_MODE_APSS_BOOT_ADDR, HWIO_GCC_TIC_MODE_APSS_BOOT_RMSK)
#define HWIO_GCC_TIC_MODE_APSS_BOOT_INM(m)      \
        in_dword_masked(HWIO_GCC_TIC_MODE_APSS_BOOT_ADDR, m)
#define HWIO_GCC_TIC_MODE_APSS_BOOT_OUT(v)      \
        out_dword(HWIO_GCC_TIC_MODE_APSS_BOOT_ADDR,v)
#define HWIO_GCC_TIC_MODE_APSS_BOOT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_TIC_MODE_APSS_BOOT_ADDR,m,v,HWIO_GCC_TIC_MODE_APSS_BOOT_IN)
#define HWIO_GCC_TIC_MODE_APSS_BOOT_APSS_BOOT_IN_TIC_MODE_BMSK                                           0x1
#define HWIO_GCC_TIC_MODE_APSS_BOOT_APSS_BOOT_IN_TIC_MODE_SHFT                                           0x0

#define HWIO_GCC_USB_BOOT_CLOCK_CTL_ADDR                                                          (GCC_CLK_CTL_REG_REG_BASE      + 0x00040000)
#define HWIO_GCC_USB_BOOT_CLOCK_CTL_RMSK                                                                 0x1
#define HWIO_GCC_USB_BOOT_CLOCK_CTL_IN          \
        in_dword_masked(HWIO_GCC_USB_BOOT_CLOCK_CTL_ADDR, HWIO_GCC_USB_BOOT_CLOCK_CTL_RMSK)
#define HWIO_GCC_USB_BOOT_CLOCK_CTL_INM(m)      \
        in_dword_masked(HWIO_GCC_USB_BOOT_CLOCK_CTL_ADDR, m)
#define HWIO_GCC_USB_BOOT_CLOCK_CTL_OUT(v)      \
        out_dword(HWIO_GCC_USB_BOOT_CLOCK_CTL_ADDR,v)
#define HWIO_GCC_USB_BOOT_CLOCK_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_USB_BOOT_CLOCK_CTL_ADDR,m,v,HWIO_GCC_USB_BOOT_CLOCK_CTL_IN)
#define HWIO_GCC_USB_BOOT_CLOCK_CTL_CLK_ENABLE_BMSK                                                      0x1
#define HWIO_GCC_USB_BOOT_CLOCK_CTL_CLK_ENABLE_SHFT                                                      0x0

#define HWIO_GCC_RAW_SLEEP_CLK_CTRL_ADDR                                                          (GCC_CLK_CTL_REG_REG_BASE      + 0x00035000)
#define HWIO_GCC_RAW_SLEEP_CLK_CTRL_RMSK                                                                 0x1
#define HWIO_GCC_RAW_SLEEP_CLK_CTRL_IN          \
        in_dword_masked(HWIO_GCC_RAW_SLEEP_CLK_CTRL_ADDR, HWIO_GCC_RAW_SLEEP_CLK_CTRL_RMSK)
#define HWIO_GCC_RAW_SLEEP_CLK_CTRL_INM(m)      \
        in_dword_masked(HWIO_GCC_RAW_SLEEP_CLK_CTRL_ADDR, m)
#define HWIO_GCC_RAW_SLEEP_CLK_CTRL_OUT(v)      \
        out_dword(HWIO_GCC_RAW_SLEEP_CLK_CTRL_ADDR,v)
#define HWIO_GCC_RAW_SLEEP_CLK_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_RAW_SLEEP_CLK_CTRL_ADDR,m,v,HWIO_GCC_RAW_SLEEP_CLK_CTRL_IN)
#define HWIO_GCC_RAW_SLEEP_CLK_CTRL_GATING_DISABLE_BMSK                                                  0x1
#define HWIO_GCC_RAW_SLEEP_CLK_CTRL_GATING_DISABLE_SHFT                                                  0x0

#define HWIO_GCC_GCC_SPARE0_REG_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x00000008)
#define HWIO_GCC_GCC_SPARE0_REG_RMSK                                                              0xffffffff
#define HWIO_GCC_GCC_SPARE0_REG_IN          \
        in_dword_masked(HWIO_GCC_GCC_SPARE0_REG_ADDR, HWIO_GCC_GCC_SPARE0_REG_RMSK)
#define HWIO_GCC_GCC_SPARE0_REG_INM(m)      \
        in_dword_masked(HWIO_GCC_GCC_SPARE0_REG_ADDR, m)
#define HWIO_GCC_GCC_SPARE0_REG_OUT(v)      \
        out_dword(HWIO_GCC_GCC_SPARE0_REG_ADDR,v)
#define HWIO_GCC_GCC_SPARE0_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GCC_SPARE0_REG_ADDR,m,v,HWIO_GCC_GCC_SPARE0_REG_IN)
#define HWIO_GCC_GCC_SPARE0_REG_SPARE_BITS_BMSK                                                   0xffffffff
#define HWIO_GCC_GCC_SPARE0_REG_SPARE_BITS_SHFT                                                          0x0

#define HWIO_GCC_GCC_SPARE1_REG_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x0000000c)
#define HWIO_GCC_GCC_SPARE1_REG_RMSK                                                              0xffffffff
#define HWIO_GCC_GCC_SPARE1_REG_IN          \
        in_dword_masked(HWIO_GCC_GCC_SPARE1_REG_ADDR, HWIO_GCC_GCC_SPARE1_REG_RMSK)
#define HWIO_GCC_GCC_SPARE1_REG_INM(m)      \
        in_dword_masked(HWIO_GCC_GCC_SPARE1_REG_ADDR, m)
#define HWIO_GCC_GCC_SPARE1_REG_OUT(v)      \
        out_dword(HWIO_GCC_GCC_SPARE1_REG_ADDR,v)
#define HWIO_GCC_GCC_SPARE1_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GCC_SPARE1_REG_ADDR,m,v,HWIO_GCC_GCC_SPARE1_REG_IN)
#define HWIO_GCC_GCC_SPARE1_REG_SPARE_BITS_BMSK                                                   0xffffffff
#define HWIO_GCC_GCC_SPARE1_REG_SPARE_BITS_SHFT                                                          0x0

#define HWIO_GCC_GCC_SPARE2_REG_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x0007e000)
#define HWIO_GCC_GCC_SPARE2_REG_RMSK                                                              0xffffffff
#define HWIO_GCC_GCC_SPARE2_REG_IN          \
        in_dword_masked(HWIO_GCC_GCC_SPARE2_REG_ADDR, HWIO_GCC_GCC_SPARE2_REG_RMSK)
#define HWIO_GCC_GCC_SPARE2_REG_INM(m)      \
        in_dword_masked(HWIO_GCC_GCC_SPARE2_REG_ADDR, m)
#define HWIO_GCC_GCC_SPARE2_REG_OUT(v)      \
        out_dword(HWIO_GCC_GCC_SPARE2_REG_ADDR,v)
#define HWIO_GCC_GCC_SPARE2_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GCC_SPARE2_REG_ADDR,m,v,HWIO_GCC_GCC_SPARE2_REG_IN)
#define HWIO_GCC_GCC_SPARE2_REG_SPARE_BITS_BMSK                                                   0xffffffff
#define HWIO_GCC_GCC_SPARE2_REG_SPARE_BITS_SHFT                                                          0x0

#define HWIO_GCC_GCC_SPARE3_REG_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x0007e004)
#define HWIO_GCC_GCC_SPARE3_REG_RMSK                                                              0xffffffff
#define HWIO_GCC_GCC_SPARE3_REG_IN          \
        in_dword_masked(HWIO_GCC_GCC_SPARE3_REG_ADDR, HWIO_GCC_GCC_SPARE3_REG_RMSK)
#define HWIO_GCC_GCC_SPARE3_REG_INM(m)      \
        in_dword_masked(HWIO_GCC_GCC_SPARE3_REG_ADDR, m)
#define HWIO_GCC_GCC_SPARE3_REG_OUT(v)      \
        out_dword(HWIO_GCC_GCC_SPARE3_REG_ADDR,v)
#define HWIO_GCC_GCC_SPARE3_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_GCC_SPARE3_REG_ADDR,m,v,HWIO_GCC_GCC_SPARE3_REG_IN)
#define HWIO_GCC_GCC_SPARE3_REG_SPARE_BITS_BMSK                                                   0xffffffff
#define HWIO_GCC_GCC_SPARE3_REG_SPARE_BITS_SHFT                                                          0x0

#define HWIO_GCC_DCD_BCR_ADDR                                                                     (GCC_CLK_CTL_REG_REG_BASE      + 0x0002a000)
#define HWIO_GCC_DCD_BCR_RMSK                                                                            0x1
#define HWIO_GCC_DCD_BCR_IN          \
        in_dword_masked(HWIO_GCC_DCD_BCR_ADDR, HWIO_GCC_DCD_BCR_RMSK)
#define HWIO_GCC_DCD_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_DCD_BCR_ADDR, m)
#define HWIO_GCC_DCD_BCR_OUT(v)      \
        out_dword(HWIO_GCC_DCD_BCR_ADDR,v)
#define HWIO_GCC_DCD_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_DCD_BCR_ADDR,m,v,HWIO_GCC_DCD_BCR_IN)
#define HWIO_GCC_DCD_BCR_BLK_ARES_BMSK                                                                   0x1
#define HWIO_GCC_DCD_BCR_BLK_ARES_SHFT                                                                   0x0

#define HWIO_GCC_DCD_XO_CBCR_ADDR                                                                 (GCC_CLK_CTL_REG_REG_BASE      + 0x0002a004)
#define HWIO_GCC_DCD_XO_CBCR_RMSK                                                                 0x80000001
#define HWIO_GCC_DCD_XO_CBCR_IN          \
        in_dword_masked(HWIO_GCC_DCD_XO_CBCR_ADDR, HWIO_GCC_DCD_XO_CBCR_RMSK)
#define HWIO_GCC_DCD_XO_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_DCD_XO_CBCR_ADDR, m)
#define HWIO_GCC_DCD_XO_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_DCD_XO_CBCR_ADDR,v)
#define HWIO_GCC_DCD_XO_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_DCD_XO_CBCR_ADDR,m,v,HWIO_GCC_DCD_XO_CBCR_IN)
#define HWIO_GCC_DCD_XO_CBCR_CLK_OFF_BMSK                                                         0x80000000
#define HWIO_GCC_DCD_XO_CBCR_CLK_OFF_SHFT                                                               0x1f
#define HWIO_GCC_DCD_XO_CBCR_CLK_ENABLE_BMSK                                                             0x1
#define HWIO_GCC_DCD_XO_CBCR_CLK_ENABLE_SHFT                                                             0x0

#define HWIO_GCC_SNOC_DCD_CONFIG_ADDR                                                             (GCC_CLK_CTL_REG_REG_BASE      + 0x0002a008)
#define HWIO_GCC_SNOC_DCD_CONFIG_RMSK                                                             0x80007fff
#define HWIO_GCC_SNOC_DCD_CONFIG_IN          \
        in_dword_masked(HWIO_GCC_SNOC_DCD_CONFIG_ADDR, HWIO_GCC_SNOC_DCD_CONFIG_RMSK)
#define HWIO_GCC_SNOC_DCD_CONFIG_INM(m)      \
        in_dword_masked(HWIO_GCC_SNOC_DCD_CONFIG_ADDR, m)
#define HWIO_GCC_SNOC_DCD_CONFIG_OUT(v)      \
        out_dword(HWIO_GCC_SNOC_DCD_CONFIG_ADDR,v)
#define HWIO_GCC_SNOC_DCD_CONFIG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SNOC_DCD_CONFIG_ADDR,m,v,HWIO_GCC_SNOC_DCD_CONFIG_IN)
#define HWIO_GCC_SNOC_DCD_CONFIG_DCD_ENABLE_BMSK                                                  0x80000000
#define HWIO_GCC_SNOC_DCD_CONFIG_DCD_ENABLE_SHFT                                                        0x1f
#define HWIO_GCC_SNOC_DCD_CONFIG_ALLOWED_DIV_BMSK                                                     0x7fff
#define HWIO_GCC_SNOC_DCD_CONFIG_ALLOWED_DIV_SHFT                                                        0x0

#define HWIO_GCC_SNOC_DCD_HYSTERESIS_CNT_ADDR                                                     (GCC_CLK_CTL_REG_REG_BASE      + 0x0002a00c)
#define HWIO_GCC_SNOC_DCD_HYSTERESIS_CNT_RMSK                                                     0xffffffff
#define HWIO_GCC_SNOC_DCD_HYSTERESIS_CNT_IN          \
        in_dword_masked(HWIO_GCC_SNOC_DCD_HYSTERESIS_CNT_ADDR, HWIO_GCC_SNOC_DCD_HYSTERESIS_CNT_RMSK)
#define HWIO_GCC_SNOC_DCD_HYSTERESIS_CNT_INM(m)      \
        in_dword_masked(HWIO_GCC_SNOC_DCD_HYSTERESIS_CNT_ADDR, m)
#define HWIO_GCC_SNOC_DCD_HYSTERESIS_CNT_OUT(v)      \
        out_dword(HWIO_GCC_SNOC_DCD_HYSTERESIS_CNT_ADDR,v)
#define HWIO_GCC_SNOC_DCD_HYSTERESIS_CNT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SNOC_DCD_HYSTERESIS_CNT_ADDR,m,v,HWIO_GCC_SNOC_DCD_HYSTERESIS_CNT_IN)
#define HWIO_GCC_SNOC_DCD_HYSTERESIS_CNT_FIRST_CNT_BMSK                                           0xfffff000
#define HWIO_GCC_SNOC_DCD_HYSTERESIS_CNT_FIRST_CNT_SHFT                                                  0xc
#define HWIO_GCC_SNOC_DCD_HYSTERESIS_CNT_NEXT_CNT_BMSK                                                 0xfff
#define HWIO_GCC_SNOC_DCD_HYSTERESIS_CNT_NEXT_CNT_SHFT                                                   0x0

#define HWIO_GCC_PCNOC_DCD_CONFIG_ADDR                                                            (GCC_CLK_CTL_REG_REG_BASE      + 0x0002a010)
#define HWIO_GCC_PCNOC_DCD_CONFIG_RMSK                                                            0x80007fff
#define HWIO_GCC_PCNOC_DCD_CONFIG_IN          \
        in_dword_masked(HWIO_GCC_PCNOC_DCD_CONFIG_ADDR, HWIO_GCC_PCNOC_DCD_CONFIG_RMSK)
#define HWIO_GCC_PCNOC_DCD_CONFIG_INM(m)      \
        in_dword_masked(HWIO_GCC_PCNOC_DCD_CONFIG_ADDR, m)
#define HWIO_GCC_PCNOC_DCD_CONFIG_OUT(v)      \
        out_dword(HWIO_GCC_PCNOC_DCD_CONFIG_ADDR,v)
#define HWIO_GCC_PCNOC_DCD_CONFIG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PCNOC_DCD_CONFIG_ADDR,m,v,HWIO_GCC_PCNOC_DCD_CONFIG_IN)
#define HWIO_GCC_PCNOC_DCD_CONFIG_DCD_ENABLE_BMSK                                                 0x80000000
#define HWIO_GCC_PCNOC_DCD_CONFIG_DCD_ENABLE_SHFT                                                       0x1f
#define HWIO_GCC_PCNOC_DCD_CONFIG_ALLOWED_DIV_BMSK                                                    0x7fff
#define HWIO_GCC_PCNOC_DCD_CONFIG_ALLOWED_DIV_SHFT                                                       0x0

#define HWIO_GCC_PCNOC_DCD_HYSTERESIS_CNT_ADDR                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x0002a014)
#define HWIO_GCC_PCNOC_DCD_HYSTERESIS_CNT_RMSK                                                    0xffffffff
#define HWIO_GCC_PCNOC_DCD_HYSTERESIS_CNT_IN          \
        in_dword_masked(HWIO_GCC_PCNOC_DCD_HYSTERESIS_CNT_ADDR, HWIO_GCC_PCNOC_DCD_HYSTERESIS_CNT_RMSK)
#define HWIO_GCC_PCNOC_DCD_HYSTERESIS_CNT_INM(m)      \
        in_dword_masked(HWIO_GCC_PCNOC_DCD_HYSTERESIS_CNT_ADDR, m)
#define HWIO_GCC_PCNOC_DCD_HYSTERESIS_CNT_OUT(v)      \
        out_dword(HWIO_GCC_PCNOC_DCD_HYSTERESIS_CNT_ADDR,v)
#define HWIO_GCC_PCNOC_DCD_HYSTERESIS_CNT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PCNOC_DCD_HYSTERESIS_CNT_ADDR,m,v,HWIO_GCC_PCNOC_DCD_HYSTERESIS_CNT_IN)
#define HWIO_GCC_PCNOC_DCD_HYSTERESIS_CNT_FIRST_CNT_BMSK                                          0xfffff000
#define HWIO_GCC_PCNOC_DCD_HYSTERESIS_CNT_FIRST_CNT_SHFT                                                 0xc
#define HWIO_GCC_PCNOC_DCD_HYSTERESIS_CNT_NEXT_CNT_BMSK                                                0xfff
#define HWIO_GCC_PCNOC_DCD_HYSTERESIS_CNT_NEXT_CNT_SHFT                                                  0x0

#define HWIO_GCC_AUDIO_CORE_BCR_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x0001c008)
#define HWIO_GCC_AUDIO_CORE_BCR_RMSK                                                              0x80000007
#define HWIO_GCC_AUDIO_CORE_BCR_IN          \
        in_dword_masked(HWIO_GCC_AUDIO_CORE_BCR_ADDR, HWIO_GCC_AUDIO_CORE_BCR_RMSK)
#define HWIO_GCC_AUDIO_CORE_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_AUDIO_CORE_BCR_ADDR, m)
#define HWIO_GCC_AUDIO_CORE_BCR_OUT(v)      \
        out_dword(HWIO_GCC_AUDIO_CORE_BCR_ADDR,v)
#define HWIO_GCC_AUDIO_CORE_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_AUDIO_CORE_BCR_ADDR,m,v,HWIO_GCC_AUDIO_CORE_BCR_IN)
#define HWIO_GCC_AUDIO_CORE_BCR_DFD_STATUS_BMSK                                                   0x80000000
#define HWIO_GCC_AUDIO_CORE_BCR_DFD_STATUS_SHFT                                                         0x1f
#define HWIO_GCC_AUDIO_CORE_BCR_FORCE_RESET_BMSK                                                         0x4
#define HWIO_GCC_AUDIO_CORE_BCR_FORCE_RESET_SHFT                                                         0x2
#define HWIO_GCC_AUDIO_CORE_BCR_DFD_EN_BMSK                                                              0x2
#define HWIO_GCC_AUDIO_CORE_BCR_DFD_EN_SHFT                                                              0x1
#define HWIO_GCC_AUDIO_CORE_BCR_BLK_ARES_BMSK                                                            0x1
#define HWIO_GCC_AUDIO_CORE_BCR_BLK_ARES_SHFT                                                            0x0

#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_CMD_RCGR_ADDR                                             (GCC_CLK_CTL_REG_REG_BASE      + 0x0001c054)
#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_CMD_RCGR_RMSK                                             0x800000f3
#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_CMD_RCGR_ADDR, HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_CMD_RCGR_RMSK)
#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_CMD_RCGR_ADDR, m)
#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_CMD_RCGR_ADDR,v)
#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_CMD_RCGR_ADDR,m,v,HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_CMD_RCGR_IN)
#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_CMD_RCGR_ROOT_OFF_BMSK                                    0x80000000
#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_CMD_RCGR_ROOT_OFF_SHFT                                          0x1f
#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_CMD_RCGR_DIRTY_D_BMSK                                           0x80
#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_CMD_RCGR_DIRTY_D_SHFT                                            0x7
#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_CMD_RCGR_DIRTY_N_BMSK                                           0x40
#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_CMD_RCGR_DIRTY_N_SHFT                                            0x6
#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_CMD_RCGR_DIRTY_M_BMSK                                           0x20
#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_CMD_RCGR_DIRTY_M_SHFT                                            0x5
#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                    0x10
#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                     0x4
#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_CMD_RCGR_ROOT_EN_BMSK                                            0x2
#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_CMD_RCGR_ROOT_EN_SHFT                                            0x1
#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_CMD_RCGR_UPDATE_BMSK                                             0x1
#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_CMD_RCGR_UPDATE_SHFT                                             0x0

#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_CFG_RCGR_ADDR                                             (GCC_CLK_CTL_REG_REG_BASE      + 0x0001c058)
#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_CFG_RCGR_RMSK                                                 0x371f
#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_CFG_RCGR_ADDR, HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_CFG_RCGR_RMSK)
#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_CFG_RCGR_ADDR, m)
#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_CFG_RCGR_ADDR,v)
#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_CFG_RCGR_ADDR,m,v,HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_CFG_RCGR_IN)
#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_CFG_RCGR_MODE_BMSK                                            0x3000
#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_CFG_RCGR_MODE_SHFT                                               0xc
#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_CFG_RCGR_SRC_SEL_BMSK                                          0x700
#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_CFG_RCGR_SRC_SEL_SHFT                                            0x8
#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_CFG_RCGR_SRC_DIV_BMSK                                           0x1f
#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_CFG_RCGR_SRC_DIV_SHFT                                            0x0

#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_M_ADDR                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x0001c05c)
#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_M_RMSK                                                          0xff
#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_M_IN          \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_M_ADDR, HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_M_RMSK)
#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_M_INM(m)      \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_M_ADDR, m)
#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_M_OUT(v)      \
        out_dword(HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_M_ADDR,v)
#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_M_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_M_ADDR,m,v,HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_M_IN)
#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_M_M_BMSK                                                        0xff
#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_M_M_SHFT                                                         0x0

#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_N_ADDR                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x0001c060)
#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_N_RMSK                                                          0xff
#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_N_IN          \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_N_ADDR, HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_N_RMSK)
#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_N_INM(m)      \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_N_ADDR, m)
#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_N_OUT(v)      \
        out_dword(HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_N_ADDR,v)
#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_N_ADDR,m,v,HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_N_IN)
#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_N_NOT_N_MINUS_M_BMSK                                            0xff
#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_N_NOT_N_MINUS_M_SHFT                                             0x0

#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_D_ADDR                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x0001c064)
#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_D_RMSK                                                          0xff
#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_D_IN          \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_D_ADDR, HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_D_RMSK)
#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_D_INM(m)      \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_D_ADDR, m)
#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_D_OUT(v)      \
        out_dword(HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_D_ADDR,v)
#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_D_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_D_ADDR,m,v,HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_D_IN)
#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_D_NOT_2D_BMSK                                                   0xff
#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_D_NOT_2D_SHFT                                                    0x0

#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_CBCR_ADDR                                                 (GCC_CLK_CTL_REG_REG_BASE      + 0x0001c068)
#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_CBCR_RMSK                                                 0x80000001
#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_CBCR_IN          \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_CBCR_ADDR, HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_CBCR_RMSK)
#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_CBCR_ADDR, m)
#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_CBCR_ADDR,v)
#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_CBCR_ADDR,m,v,HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_CBCR_IN)
#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_CBCR_CLK_OFF_BMSK                                         0x80000000
#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_CBCR_CLK_OFF_SHFT                                               0x1f
#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_CBCR_CLK_ENABLE_BMSK                                             0x1
#define HWIO_GCC_ULTAUDIO_LPAIF_PRI_I2S_CBCR_CLK_ENABLE_SHFT                                             0x0

#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_CMD_RCGR_ADDR                                             (GCC_CLK_CTL_REG_REG_BASE      + 0x0001c06c)
#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_CMD_RCGR_RMSK                                             0x800000f3
#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_CMD_RCGR_ADDR, HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_CMD_RCGR_RMSK)
#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_CMD_RCGR_ADDR, m)
#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_CMD_RCGR_ADDR,v)
#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_CMD_RCGR_ADDR,m,v,HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_CMD_RCGR_IN)
#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_CMD_RCGR_ROOT_OFF_BMSK                                    0x80000000
#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_CMD_RCGR_ROOT_OFF_SHFT                                          0x1f
#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_CMD_RCGR_DIRTY_D_BMSK                                           0x80
#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_CMD_RCGR_DIRTY_D_SHFT                                            0x7
#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_CMD_RCGR_DIRTY_N_BMSK                                           0x40
#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_CMD_RCGR_DIRTY_N_SHFT                                            0x6
#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_CMD_RCGR_DIRTY_M_BMSK                                           0x20
#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_CMD_RCGR_DIRTY_M_SHFT                                            0x5
#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                    0x10
#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                     0x4
#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_CMD_RCGR_ROOT_EN_BMSK                                            0x2
#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_CMD_RCGR_ROOT_EN_SHFT                                            0x1
#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_CMD_RCGR_UPDATE_BMSK                                             0x1
#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_CMD_RCGR_UPDATE_SHFT                                             0x0

#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_CFG_RCGR_ADDR                                             (GCC_CLK_CTL_REG_REG_BASE      + 0x0001c070)
#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_CFG_RCGR_RMSK                                                 0x371f
#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_CFG_RCGR_ADDR, HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_CFG_RCGR_RMSK)
#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_CFG_RCGR_ADDR, m)
#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_CFG_RCGR_ADDR,v)
#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_CFG_RCGR_ADDR,m,v,HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_CFG_RCGR_IN)
#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_CFG_RCGR_MODE_BMSK                                            0x3000
#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_CFG_RCGR_MODE_SHFT                                               0xc
#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_CFG_RCGR_SRC_SEL_BMSK                                          0x700
#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_CFG_RCGR_SRC_SEL_SHFT                                            0x8
#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_CFG_RCGR_SRC_DIV_BMSK                                           0x1f
#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_CFG_RCGR_SRC_DIV_SHFT                                            0x0

#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_M_ADDR                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x0001c074)
#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_M_RMSK                                                          0xff
#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_M_IN          \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_M_ADDR, HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_M_RMSK)
#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_M_INM(m)      \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_M_ADDR, m)
#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_M_OUT(v)      \
        out_dword(HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_M_ADDR,v)
#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_M_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_M_ADDR,m,v,HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_M_IN)
#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_M_M_BMSK                                                        0xff
#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_M_M_SHFT                                                         0x0

#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_N_ADDR                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x0001c078)
#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_N_RMSK                                                          0xff
#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_N_IN          \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_N_ADDR, HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_N_RMSK)
#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_N_INM(m)      \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_N_ADDR, m)
#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_N_OUT(v)      \
        out_dword(HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_N_ADDR,v)
#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_N_ADDR,m,v,HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_N_IN)
#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_N_NOT_N_MINUS_M_BMSK                                            0xff
#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_N_NOT_N_MINUS_M_SHFT                                             0x0

#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_D_ADDR                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x0001c07c)
#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_D_RMSK                                                          0xff
#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_D_IN          \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_D_ADDR, HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_D_RMSK)
#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_D_INM(m)      \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_D_ADDR, m)
#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_D_OUT(v)      \
        out_dword(HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_D_ADDR,v)
#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_D_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_D_ADDR,m,v,HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_D_IN)
#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_D_NOT_2D_BMSK                                                   0xff
#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_D_NOT_2D_SHFT                                                    0x0

#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_CBCR_ADDR                                                 (GCC_CLK_CTL_REG_REG_BASE      + 0x0001c080)
#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_CBCR_RMSK                                                 0x80000001
#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_CBCR_IN          \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_CBCR_ADDR, HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_CBCR_RMSK)
#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_CBCR_ADDR, m)
#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_CBCR_ADDR,v)
#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_CBCR_ADDR,m,v,HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_CBCR_IN)
#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_CBCR_CLK_OFF_BMSK                                         0x80000000
#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_CBCR_CLK_OFF_SHFT                                               0x1f
#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_CBCR_CLK_ENABLE_BMSK                                             0x1
#define HWIO_GCC_ULTAUDIO_LPAIF_SEC_I2S_CBCR_CLK_ENABLE_SHFT                                             0x0

#define HWIO_GCC_ULTAUDIO_XO_CMD_RCGR_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x0001c034)
#define HWIO_GCC_ULTAUDIO_XO_CMD_RCGR_RMSK                                                        0x800000f3
#define HWIO_GCC_ULTAUDIO_XO_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_ULTAUDIO_XO_CMD_RCGR_ADDR, HWIO_GCC_ULTAUDIO_XO_CMD_RCGR_RMSK)
#define HWIO_GCC_ULTAUDIO_XO_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_ULTAUDIO_XO_CMD_RCGR_ADDR, m)
#define HWIO_GCC_ULTAUDIO_XO_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_ULTAUDIO_XO_CMD_RCGR_ADDR,v)
#define HWIO_GCC_ULTAUDIO_XO_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ULTAUDIO_XO_CMD_RCGR_ADDR,m,v,HWIO_GCC_ULTAUDIO_XO_CMD_RCGR_IN)
#define HWIO_GCC_ULTAUDIO_XO_CMD_RCGR_ROOT_OFF_BMSK                                               0x80000000
#define HWIO_GCC_ULTAUDIO_XO_CMD_RCGR_ROOT_OFF_SHFT                                                     0x1f
#define HWIO_GCC_ULTAUDIO_XO_CMD_RCGR_DIRTY_D_BMSK                                                      0x80
#define HWIO_GCC_ULTAUDIO_XO_CMD_RCGR_DIRTY_D_SHFT                                                       0x7
#define HWIO_GCC_ULTAUDIO_XO_CMD_RCGR_DIRTY_N_BMSK                                                      0x40
#define HWIO_GCC_ULTAUDIO_XO_CMD_RCGR_DIRTY_N_SHFT                                                       0x6
#define HWIO_GCC_ULTAUDIO_XO_CMD_RCGR_DIRTY_M_BMSK                                                      0x20
#define HWIO_GCC_ULTAUDIO_XO_CMD_RCGR_DIRTY_M_SHFT                                                       0x5
#define HWIO_GCC_ULTAUDIO_XO_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                               0x10
#define HWIO_GCC_ULTAUDIO_XO_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                                0x4
#define HWIO_GCC_ULTAUDIO_XO_CMD_RCGR_ROOT_EN_BMSK                                                       0x2
#define HWIO_GCC_ULTAUDIO_XO_CMD_RCGR_ROOT_EN_SHFT                                                       0x1
#define HWIO_GCC_ULTAUDIO_XO_CMD_RCGR_UPDATE_BMSK                                                        0x1
#define HWIO_GCC_ULTAUDIO_XO_CMD_RCGR_UPDATE_SHFT                                                        0x0

#define HWIO_GCC_ULTAUDIO_XO_CFG_RCGR_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x0001c038)
#define HWIO_GCC_ULTAUDIO_XO_CFG_RCGR_RMSK                                                            0x371f
#define HWIO_GCC_ULTAUDIO_XO_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_ULTAUDIO_XO_CFG_RCGR_ADDR, HWIO_GCC_ULTAUDIO_XO_CFG_RCGR_RMSK)
#define HWIO_GCC_ULTAUDIO_XO_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_ULTAUDIO_XO_CFG_RCGR_ADDR, m)
#define HWIO_GCC_ULTAUDIO_XO_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_ULTAUDIO_XO_CFG_RCGR_ADDR,v)
#define HWIO_GCC_ULTAUDIO_XO_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ULTAUDIO_XO_CFG_RCGR_ADDR,m,v,HWIO_GCC_ULTAUDIO_XO_CFG_RCGR_IN)
#define HWIO_GCC_ULTAUDIO_XO_CFG_RCGR_MODE_BMSK                                                       0x3000
#define HWIO_GCC_ULTAUDIO_XO_CFG_RCGR_MODE_SHFT                                                          0xc
#define HWIO_GCC_ULTAUDIO_XO_CFG_RCGR_SRC_SEL_BMSK                                                     0x700
#define HWIO_GCC_ULTAUDIO_XO_CFG_RCGR_SRC_SEL_SHFT                                                       0x8
#define HWIO_GCC_ULTAUDIO_XO_CFG_RCGR_SRC_DIV_BMSK                                                      0x1f
#define HWIO_GCC_ULTAUDIO_XO_CFG_RCGR_SRC_DIV_SHFT                                                       0x0

#define HWIO_GCC_ULTAUDIO_AHBFABRIC_CMD_RCGR_ADDR                                                 (GCC_CLK_CTL_REG_REG_BASE      + 0x0001c010)
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_CMD_RCGR_RMSK                                                 0x800000f3
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_ULTAUDIO_AHBFABRIC_CMD_RCGR_ADDR, HWIO_GCC_ULTAUDIO_AHBFABRIC_CMD_RCGR_RMSK)
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_ULTAUDIO_AHBFABRIC_CMD_RCGR_ADDR, m)
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_ULTAUDIO_AHBFABRIC_CMD_RCGR_ADDR,v)
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ULTAUDIO_AHBFABRIC_CMD_RCGR_ADDR,m,v,HWIO_GCC_ULTAUDIO_AHBFABRIC_CMD_RCGR_IN)
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_CMD_RCGR_ROOT_OFF_BMSK                                        0x80000000
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_CMD_RCGR_ROOT_OFF_SHFT                                              0x1f
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_CMD_RCGR_DIRTY_D_BMSK                                               0x80
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_CMD_RCGR_DIRTY_D_SHFT                                                0x7
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_CMD_RCGR_DIRTY_N_BMSK                                               0x40
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_CMD_RCGR_DIRTY_N_SHFT                                                0x6
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_CMD_RCGR_DIRTY_M_BMSK                                               0x20
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_CMD_RCGR_DIRTY_M_SHFT                                                0x5
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                        0x10
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                         0x4
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_CMD_RCGR_ROOT_EN_BMSK                                                0x2
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_CMD_RCGR_ROOT_EN_SHFT                                                0x1
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_CMD_RCGR_UPDATE_BMSK                                                 0x1
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_CMD_RCGR_UPDATE_SHFT                                                 0x0

#define HWIO_GCC_ULTAUDIO_AHBFABRIC_CFG_RCGR_ADDR                                                 (GCC_CLK_CTL_REG_REG_BASE      + 0x0001c014)
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_CFG_RCGR_RMSK                                                     0x371f
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_ULTAUDIO_AHBFABRIC_CFG_RCGR_ADDR, HWIO_GCC_ULTAUDIO_AHBFABRIC_CFG_RCGR_RMSK)
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_ULTAUDIO_AHBFABRIC_CFG_RCGR_ADDR, m)
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_ULTAUDIO_AHBFABRIC_CFG_RCGR_ADDR,v)
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ULTAUDIO_AHBFABRIC_CFG_RCGR_ADDR,m,v,HWIO_GCC_ULTAUDIO_AHBFABRIC_CFG_RCGR_IN)
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_CFG_RCGR_MODE_BMSK                                                0x3000
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_CFG_RCGR_MODE_SHFT                                                   0xc
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_CFG_RCGR_SRC_SEL_BMSK                                              0x700
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_CFG_RCGR_SRC_SEL_SHFT                                                0x8
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_CFG_RCGR_SRC_DIV_BMSK                                               0x1f
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_CFG_RCGR_SRC_DIV_SHFT                                                0x0

#define HWIO_GCC_ULTAUDIO_AHBFABRIC_M_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x0001c018)
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_M_RMSK                                                              0xff
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_M_IN          \
        in_dword_masked(HWIO_GCC_ULTAUDIO_AHBFABRIC_M_ADDR, HWIO_GCC_ULTAUDIO_AHBFABRIC_M_RMSK)
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_M_INM(m)      \
        in_dword_masked(HWIO_GCC_ULTAUDIO_AHBFABRIC_M_ADDR, m)
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_M_OUT(v)      \
        out_dword(HWIO_GCC_ULTAUDIO_AHBFABRIC_M_ADDR,v)
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_M_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ULTAUDIO_AHBFABRIC_M_ADDR,m,v,HWIO_GCC_ULTAUDIO_AHBFABRIC_M_IN)
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_M_M_BMSK                                                            0xff
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_M_M_SHFT                                                             0x0

#define HWIO_GCC_ULTAUDIO_AHBFABRIC_N_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x0001c01c)
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_N_RMSK                                                              0xff
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_N_IN          \
        in_dword_masked(HWIO_GCC_ULTAUDIO_AHBFABRIC_N_ADDR, HWIO_GCC_ULTAUDIO_AHBFABRIC_N_RMSK)
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_N_INM(m)      \
        in_dword_masked(HWIO_GCC_ULTAUDIO_AHBFABRIC_N_ADDR, m)
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_N_OUT(v)      \
        out_dword(HWIO_GCC_ULTAUDIO_AHBFABRIC_N_ADDR,v)
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ULTAUDIO_AHBFABRIC_N_ADDR,m,v,HWIO_GCC_ULTAUDIO_AHBFABRIC_N_IN)
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_N_NOT_N_MINUS_M_BMSK                                                0xff
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_N_NOT_N_MINUS_M_SHFT                                                 0x0

#define HWIO_GCC_ULTAUDIO_AHBFABRIC_D_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x0001c020)
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_D_RMSK                                                              0xff
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_D_IN          \
        in_dword_masked(HWIO_GCC_ULTAUDIO_AHBFABRIC_D_ADDR, HWIO_GCC_ULTAUDIO_AHBFABRIC_D_RMSK)
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_D_INM(m)      \
        in_dword_masked(HWIO_GCC_ULTAUDIO_AHBFABRIC_D_ADDR, m)
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_D_OUT(v)      \
        out_dword(HWIO_GCC_ULTAUDIO_AHBFABRIC_D_ADDR,v)
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_D_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ULTAUDIO_AHBFABRIC_D_ADDR,m,v,HWIO_GCC_ULTAUDIO_AHBFABRIC_D_IN)
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_D_NOT_2D_BMSK                                                       0xff
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_D_NOT_2D_SHFT                                                        0x0

#define HWIO_GCC_ULTAUDIO_AHBFABRIC_IXFABRIC_LPM_CBCR_ADDR                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x0001c024)
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_IXFABRIC_LPM_CBCR_RMSK                                        0x80007ff1
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_IXFABRIC_LPM_CBCR_IN          \
        in_dword_masked(HWIO_GCC_ULTAUDIO_AHBFABRIC_IXFABRIC_LPM_CBCR_ADDR, HWIO_GCC_ULTAUDIO_AHBFABRIC_IXFABRIC_LPM_CBCR_RMSK)
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_IXFABRIC_LPM_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_ULTAUDIO_AHBFABRIC_IXFABRIC_LPM_CBCR_ADDR, m)
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_IXFABRIC_LPM_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_ULTAUDIO_AHBFABRIC_IXFABRIC_LPM_CBCR_ADDR,v)
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_IXFABRIC_LPM_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ULTAUDIO_AHBFABRIC_IXFABRIC_LPM_CBCR_ADDR,m,v,HWIO_GCC_ULTAUDIO_AHBFABRIC_IXFABRIC_LPM_CBCR_IN)
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_IXFABRIC_LPM_CBCR_CLK_OFF_BMSK                                0x80000000
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_IXFABRIC_LPM_CBCR_CLK_OFF_SHFT                                      0x1f
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_IXFABRIC_LPM_CBCR_FORCE_MEM_CORE_ON_BMSK                          0x4000
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_IXFABRIC_LPM_CBCR_FORCE_MEM_CORE_ON_SHFT                             0xe
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_IXFABRIC_LPM_CBCR_FORCE_MEM_PERIPH_ON_BMSK                        0x2000
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_IXFABRIC_LPM_CBCR_FORCE_MEM_PERIPH_ON_SHFT                           0xd
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_IXFABRIC_LPM_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                       0x1000
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_IXFABRIC_LPM_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                          0xc
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_IXFABRIC_LPM_CBCR_WAKEUP_BMSK                                      0xf00
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_IXFABRIC_LPM_CBCR_WAKEUP_SHFT                                        0x8
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_IXFABRIC_LPM_CBCR_SLEEP_BMSK                                        0xf0
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_IXFABRIC_LPM_CBCR_SLEEP_SHFT                                         0x4
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_IXFABRIC_LPM_CBCR_CLK_ENABLE_BMSK                                    0x1
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_IXFABRIC_LPM_CBCR_CLK_ENABLE_SHFT                                    0x0

#define HWIO_GCC_ULTAUDIO_AHBFABRIC_IXFABRIC_CBCR_ADDR                                            (GCC_CLK_CTL_REG_REG_BASE      + 0x0001c028)
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_IXFABRIC_CBCR_RMSK                                            0x80000001
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_IXFABRIC_CBCR_IN          \
        in_dword_masked(HWIO_GCC_ULTAUDIO_AHBFABRIC_IXFABRIC_CBCR_ADDR, HWIO_GCC_ULTAUDIO_AHBFABRIC_IXFABRIC_CBCR_RMSK)
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_IXFABRIC_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_ULTAUDIO_AHBFABRIC_IXFABRIC_CBCR_ADDR, m)
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_IXFABRIC_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_ULTAUDIO_AHBFABRIC_IXFABRIC_CBCR_ADDR,v)
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_IXFABRIC_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ULTAUDIO_AHBFABRIC_IXFABRIC_CBCR_ADDR,m,v,HWIO_GCC_ULTAUDIO_AHBFABRIC_IXFABRIC_CBCR_IN)
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_IXFABRIC_CBCR_CLK_OFF_BMSK                                    0x80000000
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_IXFABRIC_CBCR_CLK_OFF_SHFT                                          0x1f
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_IXFABRIC_CBCR_CLK_ENABLE_BMSK                                        0x1
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_IXFABRIC_CBCR_CLK_ENABLE_SHFT                                        0x0

#define HWIO_GCC_ULTAUDIO_AHBFABRIC_EFABRIC_SPDM_CBCR_ADDR                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x0001c030)
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_EFABRIC_SPDM_CBCR_RMSK                                        0x80000001
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_EFABRIC_SPDM_CBCR_IN          \
        in_dword_masked(HWIO_GCC_ULTAUDIO_AHBFABRIC_EFABRIC_SPDM_CBCR_ADDR, HWIO_GCC_ULTAUDIO_AHBFABRIC_EFABRIC_SPDM_CBCR_RMSK)
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_EFABRIC_SPDM_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_ULTAUDIO_AHBFABRIC_EFABRIC_SPDM_CBCR_ADDR, m)
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_EFABRIC_SPDM_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_ULTAUDIO_AHBFABRIC_EFABRIC_SPDM_CBCR_ADDR,v)
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_EFABRIC_SPDM_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ULTAUDIO_AHBFABRIC_EFABRIC_SPDM_CBCR_ADDR,m,v,HWIO_GCC_ULTAUDIO_AHBFABRIC_EFABRIC_SPDM_CBCR_IN)
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_EFABRIC_SPDM_CBCR_CLK_OFF_BMSK                                0x80000000
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_EFABRIC_SPDM_CBCR_CLK_OFF_SHFT                                      0x1f
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_EFABRIC_SPDM_CBCR_CLK_ENABLE_BMSK                                    0x1
#define HWIO_GCC_ULTAUDIO_AHBFABRIC_EFABRIC_SPDM_CBCR_CLK_ENABLE_SHFT                                    0x0

#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_CMD_RCGR_ADDR                                             (GCC_CLK_CTL_REG_REG_BASE      + 0x0001c084)
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_CMD_RCGR_RMSK                                             0x800000f3
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_CMD_RCGR_ADDR, HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_CMD_RCGR_RMSK)
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_CMD_RCGR_ADDR, m)
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_CMD_RCGR_ADDR,v)
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_CMD_RCGR_ADDR,m,v,HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_CMD_RCGR_IN)
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_CMD_RCGR_ROOT_OFF_BMSK                                    0x80000000
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_CMD_RCGR_ROOT_OFF_SHFT                                          0x1f
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_CMD_RCGR_DIRTY_D_BMSK                                           0x80
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_CMD_RCGR_DIRTY_D_SHFT                                            0x7
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_CMD_RCGR_DIRTY_N_BMSK                                           0x40
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_CMD_RCGR_DIRTY_N_SHFT                                            0x6
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_CMD_RCGR_DIRTY_M_BMSK                                           0x20
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_CMD_RCGR_DIRTY_M_SHFT                                            0x5
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                    0x10
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                     0x4
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_CMD_RCGR_ROOT_EN_BMSK                                            0x2
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_CMD_RCGR_ROOT_EN_SHFT                                            0x1
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_CMD_RCGR_UPDATE_BMSK                                             0x1
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_CMD_RCGR_UPDATE_SHFT                                             0x0

#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_CFG_RCGR_ADDR                                             (GCC_CLK_CTL_REG_REG_BASE      + 0x0001c088)
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_CFG_RCGR_RMSK                                                 0x371f
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_CFG_RCGR_ADDR, HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_CFG_RCGR_RMSK)
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_CFG_RCGR_ADDR, m)
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_CFG_RCGR_ADDR,v)
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_CFG_RCGR_ADDR,m,v,HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_CFG_RCGR_IN)
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_CFG_RCGR_MODE_BMSK                                            0x3000
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_CFG_RCGR_MODE_SHFT                                               0xc
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_CFG_RCGR_SRC_SEL_BMSK                                          0x700
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_CFG_RCGR_SRC_SEL_SHFT                                            0x8
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_CFG_RCGR_SRC_DIV_BMSK                                           0x1f
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_CFG_RCGR_SRC_DIV_SHFT                                            0x0

#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_M_ADDR                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x0001c08c)
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_M_RMSK                                                          0xff
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_M_IN          \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_M_ADDR, HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_M_RMSK)
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_M_INM(m)      \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_M_ADDR, m)
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_M_OUT(v)      \
        out_dword(HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_M_ADDR,v)
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_M_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_M_ADDR,m,v,HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_M_IN)
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_M_M_BMSK                                                        0xff
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_M_M_SHFT                                                         0x0

#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_N_ADDR                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x0001c090)
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_N_RMSK                                                          0xff
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_N_IN          \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_N_ADDR, HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_N_RMSK)
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_N_INM(m)      \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_N_ADDR, m)
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_N_OUT(v)      \
        out_dword(HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_N_ADDR,v)
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_N_ADDR,m,v,HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_N_IN)
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_N_NOT_N_MINUS_M_BMSK                                            0xff
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_N_NOT_N_MINUS_M_SHFT                                             0x0

#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_D_ADDR                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x0001c094)
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_D_RMSK                                                          0xff
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_D_IN          \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_D_ADDR, HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_D_RMSK)
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_D_INM(m)      \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_D_ADDR, m)
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_D_OUT(v)      \
        out_dword(HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_D_ADDR,v)
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_D_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_D_ADDR,m,v,HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_D_IN)
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_D_NOT_2D_BMSK                                                   0xff
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_D_NOT_2D_SHFT                                                    0x0

#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_CBCR_ADDR                                                 (GCC_CLK_CTL_REG_REG_BASE      + 0x0001c098)
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_CBCR_RMSK                                                 0x80000001
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_CBCR_IN          \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_CBCR_ADDR, HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_CBCR_RMSK)
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_CBCR_ADDR, m)
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_CBCR_ADDR,v)
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_CBCR_ADDR,m,v,HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_CBCR_IN)
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_CBCR_CLK_OFF_BMSK                                         0x80000000
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_CBCR_CLK_OFF_SHFT                                               0x1f
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_CBCR_CLK_ENABLE_BMSK                                             0x1
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_I2S_CBCR_CLK_ENABLE_SHFT                                             0x0

#define HWIO_GCC_SYS_NOC_IPA_AXI_CBCR_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x0003e028)
#define HWIO_GCC_SYS_NOC_IPA_AXI_CBCR_RMSK                                                        0x80000001
#define HWIO_GCC_SYS_NOC_IPA_AXI_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SYS_NOC_IPA_AXI_CBCR_ADDR, HWIO_GCC_SYS_NOC_IPA_AXI_CBCR_RMSK)
#define HWIO_GCC_SYS_NOC_IPA_AXI_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SYS_NOC_IPA_AXI_CBCR_ADDR, m)
#define HWIO_GCC_SYS_NOC_IPA_AXI_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_SYS_NOC_IPA_AXI_CBCR_ADDR,v)
#define HWIO_GCC_SYS_NOC_IPA_AXI_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SYS_NOC_IPA_AXI_CBCR_ADDR,m,v,HWIO_GCC_SYS_NOC_IPA_AXI_CBCR_IN)
#define HWIO_GCC_SYS_NOC_IPA_AXI_CBCR_CLK_OFF_BMSK                                                0x80000000
#define HWIO_GCC_SYS_NOC_IPA_AXI_CBCR_CLK_OFF_SHFT                                                      0x1f
#define HWIO_GCC_SYS_NOC_IPA_AXI_CBCR_CLK_ENABLE_BMSK                                                    0x1
#define HWIO_GCC_SYS_NOC_IPA_AXI_CBCR_CLK_ENABLE_SHFT                                                    0x0

#define HWIO_GCC_IPA_BCR_ADDR                                                                     (GCC_CLK_CTL_REG_REG_BASE      + 0x0003e000)
#define HWIO_GCC_IPA_BCR_RMSK                                                                            0x1
#define HWIO_GCC_IPA_BCR_IN          \
        in_dword_masked(HWIO_GCC_IPA_BCR_ADDR, HWIO_GCC_IPA_BCR_RMSK)
#define HWIO_GCC_IPA_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_IPA_BCR_ADDR, m)
#define HWIO_GCC_IPA_BCR_OUT(v)      \
        out_dword(HWIO_GCC_IPA_BCR_ADDR,v)
#define HWIO_GCC_IPA_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_IPA_BCR_ADDR,m,v,HWIO_GCC_IPA_BCR_IN)
#define HWIO_GCC_IPA_BCR_BLK_ARES_BMSK                                                                   0x1
#define HWIO_GCC_IPA_BCR_BLK_ARES_SHFT                                                                   0x0

#define HWIO_GCC_IPA_CMD_RCGR_ADDR                                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x0003e004)
#define HWIO_GCC_IPA_CMD_RCGR_RMSK                                                                0x800000f3
#define HWIO_GCC_IPA_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_IPA_CMD_RCGR_ADDR, HWIO_GCC_IPA_CMD_RCGR_RMSK)
#define HWIO_GCC_IPA_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_IPA_CMD_RCGR_ADDR, m)
#define HWIO_GCC_IPA_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_IPA_CMD_RCGR_ADDR,v)
#define HWIO_GCC_IPA_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_IPA_CMD_RCGR_ADDR,m,v,HWIO_GCC_IPA_CMD_RCGR_IN)
#define HWIO_GCC_IPA_CMD_RCGR_ROOT_OFF_BMSK                                                       0x80000000
#define HWIO_GCC_IPA_CMD_RCGR_ROOT_OFF_SHFT                                                             0x1f
#define HWIO_GCC_IPA_CMD_RCGR_DIRTY_D_BMSK                                                              0x80
#define HWIO_GCC_IPA_CMD_RCGR_DIRTY_D_SHFT                                                               0x7
#define HWIO_GCC_IPA_CMD_RCGR_DIRTY_M_BMSK                                                              0x40
#define HWIO_GCC_IPA_CMD_RCGR_DIRTY_M_SHFT                                                               0x6
#define HWIO_GCC_IPA_CMD_RCGR_DIRTY_N_BMSK                                                              0x20
#define HWIO_GCC_IPA_CMD_RCGR_DIRTY_N_SHFT                                                               0x5
#define HWIO_GCC_IPA_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                                       0x10
#define HWIO_GCC_IPA_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                                        0x4
#define HWIO_GCC_IPA_CMD_RCGR_ROOT_EN_BMSK                                                               0x2
#define HWIO_GCC_IPA_CMD_RCGR_ROOT_EN_SHFT                                                               0x1
#define HWIO_GCC_IPA_CMD_RCGR_UPDATE_BMSK                                                                0x1
#define HWIO_GCC_IPA_CMD_RCGR_UPDATE_SHFT                                                                0x0

#define HWIO_GCC_IPA_CFG_RCGR_ADDR                                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x0003e008)
#define HWIO_GCC_IPA_CFG_RCGR_RMSK                                                                    0x371f
#define HWIO_GCC_IPA_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_IPA_CFG_RCGR_ADDR, HWIO_GCC_IPA_CFG_RCGR_RMSK)
#define HWIO_GCC_IPA_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_IPA_CFG_RCGR_ADDR, m)
#define HWIO_GCC_IPA_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_IPA_CFG_RCGR_ADDR,v)
#define HWIO_GCC_IPA_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_IPA_CFG_RCGR_ADDR,m,v,HWIO_GCC_IPA_CFG_RCGR_IN)
#define HWIO_GCC_IPA_CFG_RCGR_MODE_BMSK                                                               0x3000
#define HWIO_GCC_IPA_CFG_RCGR_MODE_SHFT                                                                  0xc
#define HWIO_GCC_IPA_CFG_RCGR_SRC_SEL_BMSK                                                             0x700
#define HWIO_GCC_IPA_CFG_RCGR_SRC_SEL_SHFT                                                               0x8
#define HWIO_GCC_IPA_CFG_RCGR_SRC_DIV_BMSK                                                              0x1f
#define HWIO_GCC_IPA_CFG_RCGR_SRC_DIV_SHFT                                                               0x0

#define HWIO_GCC_IPA_M_ADDR                                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x0003e00c)
#define HWIO_GCC_IPA_M_RMSK                                                                             0xff
#define HWIO_GCC_IPA_M_IN          \
        in_dword_masked(HWIO_GCC_IPA_M_ADDR, HWIO_GCC_IPA_M_RMSK)
#define HWIO_GCC_IPA_M_INM(m)      \
        in_dword_masked(HWIO_GCC_IPA_M_ADDR, m)
#define HWIO_GCC_IPA_M_OUT(v)      \
        out_dword(HWIO_GCC_IPA_M_ADDR,v)
#define HWIO_GCC_IPA_M_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_IPA_M_ADDR,m,v,HWIO_GCC_IPA_M_IN)
#define HWIO_GCC_IPA_M_M_BMSK                                                                           0xff
#define HWIO_GCC_IPA_M_M_SHFT                                                                            0x0

#define HWIO_GCC_IPA_N_ADDR                                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x0003e010)
#define HWIO_GCC_IPA_N_RMSK                                                                             0xff
#define HWIO_GCC_IPA_N_IN          \
        in_dword_masked(HWIO_GCC_IPA_N_ADDR, HWIO_GCC_IPA_N_RMSK)
#define HWIO_GCC_IPA_N_INM(m)      \
        in_dword_masked(HWIO_GCC_IPA_N_ADDR, m)
#define HWIO_GCC_IPA_N_OUT(v)      \
        out_dword(HWIO_GCC_IPA_N_ADDR,v)
#define HWIO_GCC_IPA_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_IPA_N_ADDR,m,v,HWIO_GCC_IPA_N_IN)
#define HWIO_GCC_IPA_N_NOT_N_MINUS_M_BMSK                                                               0xff
#define HWIO_GCC_IPA_N_NOT_N_MINUS_M_SHFT                                                                0x0

#define HWIO_GCC_IPA_D_ADDR                                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x0003e014)
#define HWIO_GCC_IPA_D_RMSK                                                                             0xff
#define HWIO_GCC_IPA_D_IN          \
        in_dword_masked(HWIO_GCC_IPA_D_ADDR, HWIO_GCC_IPA_D_RMSK)
#define HWIO_GCC_IPA_D_INM(m)      \
        in_dword_masked(HWIO_GCC_IPA_D_ADDR, m)
#define HWIO_GCC_IPA_D_OUT(v)      \
        out_dword(HWIO_GCC_IPA_D_ADDR,v)
#define HWIO_GCC_IPA_D_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_IPA_D_ADDR,m,v,HWIO_GCC_IPA_D_IN)
#define HWIO_GCC_IPA_D_NOT_2D_BMSK                                                                      0xff
#define HWIO_GCC_IPA_D_NOT_2D_SHFT                                                                       0x0

#define HWIO_GCC_IPA_CBCR_ADDR                                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x0003e018)
#define HWIO_GCC_IPA_CBCR_RMSK                                                                    0x80007ff1
#define HWIO_GCC_IPA_CBCR_IN          \
        in_dword_masked(HWIO_GCC_IPA_CBCR_ADDR, HWIO_GCC_IPA_CBCR_RMSK)
#define HWIO_GCC_IPA_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_IPA_CBCR_ADDR, m)
#define HWIO_GCC_IPA_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_IPA_CBCR_ADDR,v)
#define HWIO_GCC_IPA_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_IPA_CBCR_ADDR,m,v,HWIO_GCC_IPA_CBCR_IN)
#define HWIO_GCC_IPA_CBCR_CLK_OFF_BMSK                                                            0x80000000
#define HWIO_GCC_IPA_CBCR_CLK_OFF_SHFT                                                                  0x1f
#define HWIO_GCC_IPA_CBCR_FORCE_MEM_CORE_ON_BMSK                                                      0x4000
#define HWIO_GCC_IPA_CBCR_FORCE_MEM_CORE_ON_SHFT                                                         0xe
#define HWIO_GCC_IPA_CBCR_FORCE_MEM_PERIPH_ON_BMSK                                                    0x2000
#define HWIO_GCC_IPA_CBCR_FORCE_MEM_PERIPH_ON_SHFT                                                       0xd
#define HWIO_GCC_IPA_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                                                   0x1000
#define HWIO_GCC_IPA_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                                                      0xc
#define HWIO_GCC_IPA_CBCR_WAKEUP_BMSK                                                                  0xf00
#define HWIO_GCC_IPA_CBCR_WAKEUP_SHFT                                                                    0x8
#define HWIO_GCC_IPA_CBCR_SLEEP_BMSK                                                                    0xf0
#define HWIO_GCC_IPA_CBCR_SLEEP_SHFT                                                                     0x4
#define HWIO_GCC_IPA_CBCR_CLK_ENABLE_BMSK                                                                0x1
#define HWIO_GCC_IPA_CBCR_CLK_ENABLE_SHFT                                                                0x0

#define HWIO_GCC_IPA_AHB_CBCR_ADDR                                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x0003e01c)
#define HWIO_GCC_IPA_AHB_CBCR_RMSK                                                                0xf0008001
#define HWIO_GCC_IPA_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_IPA_AHB_CBCR_ADDR, HWIO_GCC_IPA_AHB_CBCR_RMSK)
#define HWIO_GCC_IPA_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_IPA_AHB_CBCR_ADDR, m)
#define HWIO_GCC_IPA_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_IPA_AHB_CBCR_ADDR,v)
#define HWIO_GCC_IPA_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_IPA_AHB_CBCR_ADDR,m,v,HWIO_GCC_IPA_AHB_CBCR_IN)
#define HWIO_GCC_IPA_AHB_CBCR_CLK_OFF_BMSK                                                        0x80000000
#define HWIO_GCC_IPA_AHB_CBCR_CLK_OFF_SHFT                                                              0x1f
#define HWIO_GCC_IPA_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                       0x70000000
#define HWIO_GCC_IPA_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                             0x1c
#define HWIO_GCC_IPA_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                               0x8000
#define HWIO_GCC_IPA_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                                  0xf
#define HWIO_GCC_IPA_AHB_CBCR_CLK_ENABLE_BMSK                                                            0x1
#define HWIO_GCC_IPA_AHB_CBCR_CLK_ENABLE_SHFT                                                            0x0

#define HWIO_GCC_IPA_SLEEP_CBCR_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x0003e020)
#define HWIO_GCC_IPA_SLEEP_CBCR_RMSK                                                              0x80000001
#define HWIO_GCC_IPA_SLEEP_CBCR_IN          \
        in_dword_masked(HWIO_GCC_IPA_SLEEP_CBCR_ADDR, HWIO_GCC_IPA_SLEEP_CBCR_RMSK)
#define HWIO_GCC_IPA_SLEEP_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_IPA_SLEEP_CBCR_ADDR, m)
#define HWIO_GCC_IPA_SLEEP_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_IPA_SLEEP_CBCR_ADDR,v)
#define HWIO_GCC_IPA_SLEEP_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_IPA_SLEEP_CBCR_ADDR,m,v,HWIO_GCC_IPA_SLEEP_CBCR_IN)
#define HWIO_GCC_IPA_SLEEP_CBCR_CLK_OFF_BMSK                                                      0x80000000
#define HWIO_GCC_IPA_SLEEP_CBCR_CLK_OFF_SHFT                                                            0x1f
#define HWIO_GCC_IPA_SLEEP_CBCR_CLK_ENABLE_BMSK                                                          0x1
#define HWIO_GCC_IPA_SLEEP_CBCR_CLK_ENABLE_SHFT                                                          0x0

#define HWIO_GCC_RBCPR_MX_BCR_ADDR                                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x0003d000)
#define HWIO_GCC_RBCPR_MX_BCR_RMSK                                                                       0x1
#define HWIO_GCC_RBCPR_MX_BCR_IN          \
        in_dword_masked(HWIO_GCC_RBCPR_MX_BCR_ADDR, HWIO_GCC_RBCPR_MX_BCR_RMSK)
#define HWIO_GCC_RBCPR_MX_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_RBCPR_MX_BCR_ADDR, m)
#define HWIO_GCC_RBCPR_MX_BCR_OUT(v)      \
        out_dword(HWIO_GCC_RBCPR_MX_BCR_ADDR,v)
#define HWIO_GCC_RBCPR_MX_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_RBCPR_MX_BCR_ADDR,m,v,HWIO_GCC_RBCPR_MX_BCR_IN)
#define HWIO_GCC_RBCPR_MX_BCR_BLK_ARES_BMSK                                                              0x1
#define HWIO_GCC_RBCPR_MX_BCR_BLK_ARES_SHFT                                                              0x0

#define HWIO_GCC_RBCPR_MX_CBCR_ADDR                                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x0003d004)
#define HWIO_GCC_RBCPR_MX_CBCR_RMSK                                                               0x80000001
#define HWIO_GCC_RBCPR_MX_CBCR_IN          \
        in_dword_masked(HWIO_GCC_RBCPR_MX_CBCR_ADDR, HWIO_GCC_RBCPR_MX_CBCR_RMSK)
#define HWIO_GCC_RBCPR_MX_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_RBCPR_MX_CBCR_ADDR, m)
#define HWIO_GCC_RBCPR_MX_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_RBCPR_MX_CBCR_ADDR,v)
#define HWIO_GCC_RBCPR_MX_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_RBCPR_MX_CBCR_ADDR,m,v,HWIO_GCC_RBCPR_MX_CBCR_IN)
#define HWIO_GCC_RBCPR_MX_CBCR_CLK_OFF_BMSK                                                       0x80000000
#define HWIO_GCC_RBCPR_MX_CBCR_CLK_OFF_SHFT                                                             0x1f
#define HWIO_GCC_RBCPR_MX_CBCR_CLK_ENABLE_BMSK                                                           0x1
#define HWIO_GCC_RBCPR_MX_CBCR_CLK_ENABLE_SHFT                                                           0x0

#define HWIO_GCC_RBCPR_MX_AHB_CBCR_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x0003d008)
#define HWIO_GCC_RBCPR_MX_AHB_CBCR_RMSK                                                           0xf0008001
#define HWIO_GCC_RBCPR_MX_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_RBCPR_MX_AHB_CBCR_ADDR, HWIO_GCC_RBCPR_MX_AHB_CBCR_RMSK)
#define HWIO_GCC_RBCPR_MX_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_RBCPR_MX_AHB_CBCR_ADDR, m)
#define HWIO_GCC_RBCPR_MX_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_RBCPR_MX_AHB_CBCR_ADDR,v)
#define HWIO_GCC_RBCPR_MX_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_RBCPR_MX_AHB_CBCR_ADDR,m,v,HWIO_GCC_RBCPR_MX_AHB_CBCR_IN)
#define HWIO_GCC_RBCPR_MX_AHB_CBCR_CLK_OFF_BMSK                                                   0x80000000
#define HWIO_GCC_RBCPR_MX_AHB_CBCR_CLK_OFF_SHFT                                                         0x1f
#define HWIO_GCC_RBCPR_MX_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                  0x70000000
#define HWIO_GCC_RBCPR_MX_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                        0x1c
#define HWIO_GCC_RBCPR_MX_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                          0x8000
#define HWIO_GCC_RBCPR_MX_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                             0xf
#define HWIO_GCC_RBCPR_MX_AHB_CBCR_CLK_ENABLE_BMSK                                                       0x1
#define HWIO_GCC_RBCPR_MX_AHB_CBCR_CLK_ENABLE_SHFT                                                       0x0

#define HWIO_GCC_RBCPR_MX_CMD_RCGR_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x0003d00c)
#define HWIO_GCC_RBCPR_MX_CMD_RCGR_RMSK                                                           0x80000013
#define HWIO_GCC_RBCPR_MX_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_RBCPR_MX_CMD_RCGR_ADDR, HWIO_GCC_RBCPR_MX_CMD_RCGR_RMSK)
#define HWIO_GCC_RBCPR_MX_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_RBCPR_MX_CMD_RCGR_ADDR, m)
#define HWIO_GCC_RBCPR_MX_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_RBCPR_MX_CMD_RCGR_ADDR,v)
#define HWIO_GCC_RBCPR_MX_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_RBCPR_MX_CMD_RCGR_ADDR,m,v,HWIO_GCC_RBCPR_MX_CMD_RCGR_IN)
#define HWIO_GCC_RBCPR_MX_CMD_RCGR_ROOT_OFF_BMSK                                                  0x80000000
#define HWIO_GCC_RBCPR_MX_CMD_RCGR_ROOT_OFF_SHFT                                                        0x1f
#define HWIO_GCC_RBCPR_MX_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                                  0x10
#define HWIO_GCC_RBCPR_MX_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                                   0x4
#define HWIO_GCC_RBCPR_MX_CMD_RCGR_ROOT_EN_BMSK                                                          0x2
#define HWIO_GCC_RBCPR_MX_CMD_RCGR_ROOT_EN_SHFT                                                          0x1
#define HWIO_GCC_RBCPR_MX_CMD_RCGR_UPDATE_BMSK                                                           0x1
#define HWIO_GCC_RBCPR_MX_CMD_RCGR_UPDATE_SHFT                                                           0x0

#define HWIO_GCC_RBCPR_MX_CFG_RCGR_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x0003d010)
#define HWIO_GCC_RBCPR_MX_CFG_RCGR_RMSK                                                                0x71f
#define HWIO_GCC_RBCPR_MX_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_RBCPR_MX_CFG_RCGR_ADDR, HWIO_GCC_RBCPR_MX_CFG_RCGR_RMSK)
#define HWIO_GCC_RBCPR_MX_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_RBCPR_MX_CFG_RCGR_ADDR, m)
#define HWIO_GCC_RBCPR_MX_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_RBCPR_MX_CFG_RCGR_ADDR,v)
#define HWIO_GCC_RBCPR_MX_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_RBCPR_MX_CFG_RCGR_ADDR,m,v,HWIO_GCC_RBCPR_MX_CFG_RCGR_IN)
#define HWIO_GCC_RBCPR_MX_CFG_RCGR_SRC_SEL_BMSK                                                        0x700
#define HWIO_GCC_RBCPR_MX_CFG_RCGR_SRC_SEL_SHFT                                                          0x8
#define HWIO_GCC_RBCPR_MX_CFG_RCGR_SRC_DIV_BMSK                                                         0x1f
#define HWIO_GCC_RBCPR_MX_CFG_RCGR_SRC_DIV_SHFT                                                          0x0

#define HWIO_GCC_QPIC_BCR_ADDR                                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x0003f000)
#define HWIO_GCC_QPIC_BCR_RMSK                                                                           0x1
#define HWIO_GCC_QPIC_BCR_IN          \
        in_dword_masked(HWIO_GCC_QPIC_BCR_ADDR, HWIO_GCC_QPIC_BCR_RMSK)
#define HWIO_GCC_QPIC_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_QPIC_BCR_ADDR, m)
#define HWIO_GCC_QPIC_BCR_OUT(v)      \
        out_dword(HWIO_GCC_QPIC_BCR_ADDR,v)
#define HWIO_GCC_QPIC_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QPIC_BCR_ADDR,m,v,HWIO_GCC_QPIC_BCR_IN)
#define HWIO_GCC_QPIC_BCR_BLK_ARES_BMSK                                                                  0x1
#define HWIO_GCC_QPIC_BCR_BLK_ARES_SHFT                                                                  0x0

#define HWIO_GCC_QPIC_CMD_RCGR_ADDR                                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x0003f004)
#define HWIO_GCC_QPIC_CMD_RCGR_RMSK                                                               0x800000f3
#define HWIO_GCC_QPIC_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_QPIC_CMD_RCGR_ADDR, HWIO_GCC_QPIC_CMD_RCGR_RMSK)
#define HWIO_GCC_QPIC_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_QPIC_CMD_RCGR_ADDR, m)
#define HWIO_GCC_QPIC_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_QPIC_CMD_RCGR_ADDR,v)
#define HWIO_GCC_QPIC_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QPIC_CMD_RCGR_ADDR,m,v,HWIO_GCC_QPIC_CMD_RCGR_IN)
#define HWIO_GCC_QPIC_CMD_RCGR_ROOT_OFF_BMSK                                                      0x80000000
#define HWIO_GCC_QPIC_CMD_RCGR_ROOT_OFF_SHFT                                                            0x1f
#define HWIO_GCC_QPIC_CMD_RCGR_DIRTY_D_BMSK                                                             0x80
#define HWIO_GCC_QPIC_CMD_RCGR_DIRTY_D_SHFT                                                              0x7
#define HWIO_GCC_QPIC_CMD_RCGR_DIRTY_M_BMSK                                                             0x40
#define HWIO_GCC_QPIC_CMD_RCGR_DIRTY_M_SHFT                                                              0x6
#define HWIO_GCC_QPIC_CMD_RCGR_DIRTY_N_BMSK                                                             0x20
#define HWIO_GCC_QPIC_CMD_RCGR_DIRTY_N_SHFT                                                              0x5
#define HWIO_GCC_QPIC_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                                      0x10
#define HWIO_GCC_QPIC_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                                       0x4
#define HWIO_GCC_QPIC_CMD_RCGR_ROOT_EN_BMSK                                                              0x2
#define HWIO_GCC_QPIC_CMD_RCGR_ROOT_EN_SHFT                                                              0x1
#define HWIO_GCC_QPIC_CMD_RCGR_UPDATE_BMSK                                                               0x1
#define HWIO_GCC_QPIC_CMD_RCGR_UPDATE_SHFT                                                               0x0

#define HWIO_GCC_QPIC_CFG_RCGR_ADDR                                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x0003f008)
#define HWIO_GCC_QPIC_CFG_RCGR_RMSK                                                                   0x371f
#define HWIO_GCC_QPIC_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_QPIC_CFG_RCGR_ADDR, HWIO_GCC_QPIC_CFG_RCGR_RMSK)
#define HWIO_GCC_QPIC_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_QPIC_CFG_RCGR_ADDR, m)
#define HWIO_GCC_QPIC_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_QPIC_CFG_RCGR_ADDR,v)
#define HWIO_GCC_QPIC_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QPIC_CFG_RCGR_ADDR,m,v,HWIO_GCC_QPIC_CFG_RCGR_IN)
#define HWIO_GCC_QPIC_CFG_RCGR_MODE_BMSK                                                              0x3000
#define HWIO_GCC_QPIC_CFG_RCGR_MODE_SHFT                                                                 0xc
#define HWIO_GCC_QPIC_CFG_RCGR_SRC_SEL_BMSK                                                            0x700
#define HWIO_GCC_QPIC_CFG_RCGR_SRC_SEL_SHFT                                                              0x8
#define HWIO_GCC_QPIC_CFG_RCGR_SRC_DIV_BMSK                                                             0x1f
#define HWIO_GCC_QPIC_CFG_RCGR_SRC_DIV_SHFT                                                              0x0

#define HWIO_GCC_QPIC_M_ADDR                                                                      (GCC_CLK_CTL_REG_REG_BASE      + 0x0003f00c)
#define HWIO_GCC_QPIC_M_RMSK                                                                            0xff
#define HWIO_GCC_QPIC_M_IN          \
        in_dword_masked(HWIO_GCC_QPIC_M_ADDR, HWIO_GCC_QPIC_M_RMSK)
#define HWIO_GCC_QPIC_M_INM(m)      \
        in_dword_masked(HWIO_GCC_QPIC_M_ADDR, m)
#define HWIO_GCC_QPIC_M_OUT(v)      \
        out_dword(HWIO_GCC_QPIC_M_ADDR,v)
#define HWIO_GCC_QPIC_M_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QPIC_M_ADDR,m,v,HWIO_GCC_QPIC_M_IN)
#define HWIO_GCC_QPIC_M_M_BMSK                                                                          0xff
#define HWIO_GCC_QPIC_M_M_SHFT                                                                           0x0

#define HWIO_GCC_QPIC_N_ADDR                                                                      (GCC_CLK_CTL_REG_REG_BASE      + 0x0003f010)
#define HWIO_GCC_QPIC_N_RMSK                                                                            0xff
#define HWIO_GCC_QPIC_N_IN          \
        in_dword_masked(HWIO_GCC_QPIC_N_ADDR, HWIO_GCC_QPIC_N_RMSK)
#define HWIO_GCC_QPIC_N_INM(m)      \
        in_dword_masked(HWIO_GCC_QPIC_N_ADDR, m)
#define HWIO_GCC_QPIC_N_OUT(v)      \
        out_dword(HWIO_GCC_QPIC_N_ADDR,v)
#define HWIO_GCC_QPIC_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QPIC_N_ADDR,m,v,HWIO_GCC_QPIC_N_IN)
#define HWIO_GCC_QPIC_N_NOT_N_MINUS_M_BMSK                                                              0xff
#define HWIO_GCC_QPIC_N_NOT_N_MINUS_M_SHFT                                                               0x0

#define HWIO_GCC_QPIC_D_ADDR                                                                      (GCC_CLK_CTL_REG_REG_BASE      + 0x0003f014)
#define HWIO_GCC_QPIC_D_RMSK                                                                            0xff
#define HWIO_GCC_QPIC_D_IN          \
        in_dword_masked(HWIO_GCC_QPIC_D_ADDR, HWIO_GCC_QPIC_D_RMSK)
#define HWIO_GCC_QPIC_D_INM(m)      \
        in_dword_masked(HWIO_GCC_QPIC_D_ADDR, m)
#define HWIO_GCC_QPIC_D_OUT(v)      \
        out_dword(HWIO_GCC_QPIC_D_ADDR,v)
#define HWIO_GCC_QPIC_D_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QPIC_D_ADDR,m,v,HWIO_GCC_QPIC_D_IN)
#define HWIO_GCC_QPIC_D_NOT_2D_BMSK                                                                     0xff
#define HWIO_GCC_QPIC_D_NOT_2D_SHFT                                                                      0x0

#define HWIO_GCC_QPIC_CBCR_ADDR                                                                   (GCC_CLK_CTL_REG_REG_BASE      + 0x0003f018)
#define HWIO_GCC_QPIC_CBCR_RMSK                                                                   0x80007ff1
#define HWIO_GCC_QPIC_CBCR_IN          \
        in_dword_masked(HWIO_GCC_QPIC_CBCR_ADDR, HWIO_GCC_QPIC_CBCR_RMSK)
#define HWIO_GCC_QPIC_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_QPIC_CBCR_ADDR, m)
#define HWIO_GCC_QPIC_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_QPIC_CBCR_ADDR,v)
#define HWIO_GCC_QPIC_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QPIC_CBCR_ADDR,m,v,HWIO_GCC_QPIC_CBCR_IN)
#define HWIO_GCC_QPIC_CBCR_CLK_OFF_BMSK                                                           0x80000000
#define HWIO_GCC_QPIC_CBCR_CLK_OFF_SHFT                                                                 0x1f
#define HWIO_GCC_QPIC_CBCR_FORCE_MEM_CORE_ON_BMSK                                                     0x4000
#define HWIO_GCC_QPIC_CBCR_FORCE_MEM_CORE_ON_SHFT                                                        0xe
#define HWIO_GCC_QPIC_CBCR_FORCE_MEM_PERIPH_ON_BMSK                                                   0x2000
#define HWIO_GCC_QPIC_CBCR_FORCE_MEM_PERIPH_ON_SHFT                                                      0xd
#define HWIO_GCC_QPIC_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                                                  0x1000
#define HWIO_GCC_QPIC_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                                                     0xc
#define HWIO_GCC_QPIC_CBCR_WAKEUP_BMSK                                                                 0xf00
#define HWIO_GCC_QPIC_CBCR_WAKEUP_SHFT                                                                   0x8
#define HWIO_GCC_QPIC_CBCR_SLEEP_BMSK                                                                   0xf0
#define HWIO_GCC_QPIC_CBCR_SLEEP_SHFT                                                                    0x4
#define HWIO_GCC_QPIC_CBCR_CLK_ENABLE_BMSK                                                               0x1
#define HWIO_GCC_QPIC_CBCR_CLK_ENABLE_SHFT                                                               0x0

#define HWIO_GCC_QPIC_AHB_CBCR_ADDR                                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x0003f01c)
#define HWIO_GCC_QPIC_AHB_CBCR_RMSK                                                               0xf000fff1
#define HWIO_GCC_QPIC_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_QPIC_AHB_CBCR_ADDR, HWIO_GCC_QPIC_AHB_CBCR_RMSK)
#define HWIO_GCC_QPIC_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_QPIC_AHB_CBCR_ADDR, m)
#define HWIO_GCC_QPIC_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_QPIC_AHB_CBCR_ADDR,v)
#define HWIO_GCC_QPIC_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QPIC_AHB_CBCR_ADDR,m,v,HWIO_GCC_QPIC_AHB_CBCR_IN)
#define HWIO_GCC_QPIC_AHB_CBCR_CLK_OFF_BMSK                                                       0x80000000
#define HWIO_GCC_QPIC_AHB_CBCR_CLK_OFF_SHFT                                                             0x1f
#define HWIO_GCC_QPIC_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                      0x70000000
#define HWIO_GCC_QPIC_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                            0x1c
#define HWIO_GCC_QPIC_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                              0x8000
#define HWIO_GCC_QPIC_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                                 0xf
#define HWIO_GCC_QPIC_AHB_CBCR_FORCE_MEM_CORE_ON_BMSK                                                 0x4000
#define HWIO_GCC_QPIC_AHB_CBCR_FORCE_MEM_CORE_ON_SHFT                                                    0xe
#define HWIO_GCC_QPIC_AHB_CBCR_FORCE_MEM_PERIPH_ON_BMSK                                               0x2000
#define HWIO_GCC_QPIC_AHB_CBCR_FORCE_MEM_PERIPH_ON_SHFT                                                  0xd
#define HWIO_GCC_QPIC_AHB_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                                              0x1000
#define HWIO_GCC_QPIC_AHB_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                                                 0xc
#define HWIO_GCC_QPIC_AHB_CBCR_WAKEUP_BMSK                                                             0xf00
#define HWIO_GCC_QPIC_AHB_CBCR_WAKEUP_SHFT                                                               0x8
#define HWIO_GCC_QPIC_AHB_CBCR_SLEEP_BMSK                                                               0xf0
#define HWIO_GCC_QPIC_AHB_CBCR_SLEEP_SHFT                                                                0x4
#define HWIO_GCC_QPIC_AHB_CBCR_CLK_ENABLE_BMSK                                                           0x1
#define HWIO_GCC_QPIC_AHB_CBCR_CLK_ENABLE_SHFT                                                           0x0

#define HWIO_GCC_QPIC_SYSTEM_CBCR_ADDR                                                            (GCC_CLK_CTL_REG_REG_BASE      + 0x0003f020)
#define HWIO_GCC_QPIC_SYSTEM_CBCR_RMSK                                                            0x80000001
#define HWIO_GCC_QPIC_SYSTEM_CBCR_IN          \
        in_dword_masked(HWIO_GCC_QPIC_SYSTEM_CBCR_ADDR, HWIO_GCC_QPIC_SYSTEM_CBCR_RMSK)
#define HWIO_GCC_QPIC_SYSTEM_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_QPIC_SYSTEM_CBCR_ADDR, m)
#define HWIO_GCC_QPIC_SYSTEM_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_QPIC_SYSTEM_CBCR_ADDR,v)
#define HWIO_GCC_QPIC_SYSTEM_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_QPIC_SYSTEM_CBCR_ADDR,m,v,HWIO_GCC_QPIC_SYSTEM_CBCR_IN)
#define HWIO_GCC_QPIC_SYSTEM_CBCR_CLK_OFF_BMSK                                                    0x80000000
#define HWIO_GCC_QPIC_SYSTEM_CBCR_CLK_OFF_SHFT                                                          0x1f
#define HWIO_GCC_QPIC_SYSTEM_CBCR_CLK_ENABLE_BMSK                                                        0x1
#define HWIO_GCC_QPIC_SYSTEM_CBCR_CLK_ENABLE_SHFT                                                        0x0

#define HWIO_GCC_SNOC_BUS_TIMEOUT2_BCR_ADDR                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x00047008)
#define HWIO_GCC_SNOC_BUS_TIMEOUT2_BCR_RMSK                                                              0x1
#define HWIO_GCC_SNOC_BUS_TIMEOUT2_BCR_IN          \
        in_dword_masked(HWIO_GCC_SNOC_BUS_TIMEOUT2_BCR_ADDR, HWIO_GCC_SNOC_BUS_TIMEOUT2_BCR_RMSK)
#define HWIO_GCC_SNOC_BUS_TIMEOUT2_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SNOC_BUS_TIMEOUT2_BCR_ADDR, m)
#define HWIO_GCC_SNOC_BUS_TIMEOUT2_BCR_OUT(v)      \
        out_dword(HWIO_GCC_SNOC_BUS_TIMEOUT2_BCR_ADDR,v)
#define HWIO_GCC_SNOC_BUS_TIMEOUT2_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SNOC_BUS_TIMEOUT2_BCR_ADDR,m,v,HWIO_GCC_SNOC_BUS_TIMEOUT2_BCR_IN)
#define HWIO_GCC_SNOC_BUS_TIMEOUT2_BCR_BLK_ARES_BMSK                                                     0x1
#define HWIO_GCC_SNOC_BUS_TIMEOUT2_BCR_BLK_ARES_SHFT                                                     0x0

#define HWIO_GCC_SNOC_BUS_TIMEOUT2_AHB_CBCR_ADDR                                                  (GCC_CLK_CTL_REG_REG_BASE      + 0x0004700c)
#define HWIO_GCC_SNOC_BUS_TIMEOUT2_AHB_CBCR_RMSK                                                  0x80000001
#define HWIO_GCC_SNOC_BUS_TIMEOUT2_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SNOC_BUS_TIMEOUT2_AHB_CBCR_ADDR, HWIO_GCC_SNOC_BUS_TIMEOUT2_AHB_CBCR_RMSK)
#define HWIO_GCC_SNOC_BUS_TIMEOUT2_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SNOC_BUS_TIMEOUT2_AHB_CBCR_ADDR, m)
#define HWIO_GCC_SNOC_BUS_TIMEOUT2_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_SNOC_BUS_TIMEOUT2_AHB_CBCR_ADDR,v)
#define HWIO_GCC_SNOC_BUS_TIMEOUT2_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SNOC_BUS_TIMEOUT2_AHB_CBCR_ADDR,m,v,HWIO_GCC_SNOC_BUS_TIMEOUT2_AHB_CBCR_IN)
#define HWIO_GCC_SNOC_BUS_TIMEOUT2_AHB_CBCR_CLK_OFF_BMSK                                          0x80000000
#define HWIO_GCC_SNOC_BUS_TIMEOUT2_AHB_CBCR_CLK_OFF_SHFT                                                0x1f
#define HWIO_GCC_SNOC_BUS_TIMEOUT2_AHB_CBCR_CLK_ENABLE_BMSK                                              0x1
#define HWIO_GCC_SNOC_BUS_TIMEOUT2_AHB_CBCR_CLK_ENABLE_SHFT                                              0x0

#define HWIO_GCC_PCIE_BOOT_CLOCK_CTL_ADDR                                                         (GCC_CLK_CTL_REG_REG_BASE      + 0x0005d000)
#define HWIO_GCC_PCIE_BOOT_CLOCK_CTL_RMSK                                                                0x1
#define HWIO_GCC_PCIE_BOOT_CLOCK_CTL_IN          \
        in_dword_masked(HWIO_GCC_PCIE_BOOT_CLOCK_CTL_ADDR, HWIO_GCC_PCIE_BOOT_CLOCK_CTL_RMSK)
#define HWIO_GCC_PCIE_BOOT_CLOCK_CTL_INM(m)      \
        in_dword_masked(HWIO_GCC_PCIE_BOOT_CLOCK_CTL_ADDR, m)
#define HWIO_GCC_PCIE_BOOT_CLOCK_CTL_OUT(v)      \
        out_dword(HWIO_GCC_PCIE_BOOT_CLOCK_CTL_ADDR,v)
#define HWIO_GCC_PCIE_BOOT_CLOCK_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PCIE_BOOT_CLOCK_CTL_ADDR,m,v,HWIO_GCC_PCIE_BOOT_CLOCK_CTL_IN)
#define HWIO_GCC_PCIE_BOOT_CLOCK_CTL_CLK_ENABLE_BMSK                                                     0x1
#define HWIO_GCC_PCIE_BOOT_CLOCK_CTL_CLK_ENABLE_SHFT                                                     0x0

#define HWIO_GCC_PCIE_BCR_ADDR                                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x0005d004)
#define HWIO_GCC_PCIE_BCR_RMSK                                                                           0x1
#define HWIO_GCC_PCIE_BCR_IN          \
        in_dword_masked(HWIO_GCC_PCIE_BCR_ADDR, HWIO_GCC_PCIE_BCR_RMSK)
#define HWIO_GCC_PCIE_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PCIE_BCR_ADDR, m)
#define HWIO_GCC_PCIE_BCR_OUT(v)      \
        out_dword(HWIO_GCC_PCIE_BCR_ADDR,v)
#define HWIO_GCC_PCIE_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PCIE_BCR_ADDR,m,v,HWIO_GCC_PCIE_BCR_IN)
#define HWIO_GCC_PCIE_BCR_BLK_ARES_BMSK                                                                  0x1
#define HWIO_GCC_PCIE_BCR_BLK_ARES_SHFT                                                                  0x0

#define HWIO_GCC_PCIE_CFG_AHB_CBCR_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x0005d008)
#define HWIO_GCC_PCIE_CFG_AHB_CBCR_RMSK                                                           0xf0008001
#define HWIO_GCC_PCIE_CFG_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_PCIE_CFG_AHB_CBCR_ADDR, HWIO_GCC_PCIE_CFG_AHB_CBCR_RMSK)
#define HWIO_GCC_PCIE_CFG_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PCIE_CFG_AHB_CBCR_ADDR, m)
#define HWIO_GCC_PCIE_CFG_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_PCIE_CFG_AHB_CBCR_ADDR,v)
#define HWIO_GCC_PCIE_CFG_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PCIE_CFG_AHB_CBCR_ADDR,m,v,HWIO_GCC_PCIE_CFG_AHB_CBCR_IN)
#define HWIO_GCC_PCIE_CFG_AHB_CBCR_CLK_OFF_BMSK                                                   0x80000000
#define HWIO_GCC_PCIE_CFG_AHB_CBCR_CLK_OFF_SHFT                                                         0x1f
#define HWIO_GCC_PCIE_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                  0x70000000
#define HWIO_GCC_PCIE_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                        0x1c
#define HWIO_GCC_PCIE_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                          0x8000
#define HWIO_GCC_PCIE_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                             0xf
#define HWIO_GCC_PCIE_CFG_AHB_CBCR_CLK_ENABLE_BMSK                                                       0x1
#define HWIO_GCC_PCIE_CFG_AHB_CBCR_CLK_ENABLE_SHFT                                                       0x0

#define HWIO_GCC_PCIE_PIPE_CBCR_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x0005d00c)
#define HWIO_GCC_PCIE_PIPE_CBCR_RMSK                                                              0x80007ff1
#define HWIO_GCC_PCIE_PIPE_CBCR_IN          \
        in_dword_masked(HWIO_GCC_PCIE_PIPE_CBCR_ADDR, HWIO_GCC_PCIE_PIPE_CBCR_RMSK)
#define HWIO_GCC_PCIE_PIPE_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PCIE_PIPE_CBCR_ADDR, m)
#define HWIO_GCC_PCIE_PIPE_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_PCIE_PIPE_CBCR_ADDR,v)
#define HWIO_GCC_PCIE_PIPE_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PCIE_PIPE_CBCR_ADDR,m,v,HWIO_GCC_PCIE_PIPE_CBCR_IN)
#define HWIO_GCC_PCIE_PIPE_CBCR_CLK_OFF_BMSK                                                      0x80000000
#define HWIO_GCC_PCIE_PIPE_CBCR_CLK_OFF_SHFT                                                            0x1f
#define HWIO_GCC_PCIE_PIPE_CBCR_FORCE_MEM_CORE_ON_BMSK                                                0x4000
#define HWIO_GCC_PCIE_PIPE_CBCR_FORCE_MEM_CORE_ON_SHFT                                                   0xe
#define HWIO_GCC_PCIE_PIPE_CBCR_FORCE_MEM_PERIPH_ON_BMSK                                              0x2000
#define HWIO_GCC_PCIE_PIPE_CBCR_FORCE_MEM_PERIPH_ON_SHFT                                                 0xd
#define HWIO_GCC_PCIE_PIPE_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                                             0x1000
#define HWIO_GCC_PCIE_PIPE_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                                                0xc
#define HWIO_GCC_PCIE_PIPE_CBCR_WAKEUP_BMSK                                                            0xf00
#define HWIO_GCC_PCIE_PIPE_CBCR_WAKEUP_SHFT                                                              0x8
#define HWIO_GCC_PCIE_PIPE_CBCR_SLEEP_BMSK                                                              0xf0
#define HWIO_GCC_PCIE_PIPE_CBCR_SLEEP_SHFT                                                               0x4
#define HWIO_GCC_PCIE_PIPE_CBCR_CLK_ENABLE_BMSK                                                          0x1
#define HWIO_GCC_PCIE_PIPE_CBCR_CLK_ENABLE_SHFT                                                          0x0

#define HWIO_GCC_PCIE_AXI_CBCR_ADDR                                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x0005d010)
#define HWIO_GCC_PCIE_AXI_CBCR_RMSK                                                               0xf000fff1
#define HWIO_GCC_PCIE_AXI_CBCR_IN          \
        in_dword_masked(HWIO_GCC_PCIE_AXI_CBCR_ADDR, HWIO_GCC_PCIE_AXI_CBCR_RMSK)
#define HWIO_GCC_PCIE_AXI_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PCIE_AXI_CBCR_ADDR, m)
#define HWIO_GCC_PCIE_AXI_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_PCIE_AXI_CBCR_ADDR,v)
#define HWIO_GCC_PCIE_AXI_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PCIE_AXI_CBCR_ADDR,m,v,HWIO_GCC_PCIE_AXI_CBCR_IN)
#define HWIO_GCC_PCIE_AXI_CBCR_CLK_OFF_BMSK                                                       0x80000000
#define HWIO_GCC_PCIE_AXI_CBCR_CLK_OFF_SHFT                                                             0x1f
#define HWIO_GCC_PCIE_AXI_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                      0x70000000
#define HWIO_GCC_PCIE_AXI_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                            0x1c
#define HWIO_GCC_PCIE_AXI_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                              0x8000
#define HWIO_GCC_PCIE_AXI_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                                 0xf
#define HWIO_GCC_PCIE_AXI_CBCR_FORCE_MEM_CORE_ON_BMSK                                                 0x4000
#define HWIO_GCC_PCIE_AXI_CBCR_FORCE_MEM_CORE_ON_SHFT                                                    0xe
#define HWIO_GCC_PCIE_AXI_CBCR_FORCE_MEM_PERIPH_ON_BMSK                                               0x2000
#define HWIO_GCC_PCIE_AXI_CBCR_FORCE_MEM_PERIPH_ON_SHFT                                                  0xd
#define HWIO_GCC_PCIE_AXI_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                                              0x1000
#define HWIO_GCC_PCIE_AXI_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                                                 0xc
#define HWIO_GCC_PCIE_AXI_CBCR_WAKEUP_BMSK                                                             0xf00
#define HWIO_GCC_PCIE_AXI_CBCR_WAKEUP_SHFT                                                               0x8
#define HWIO_GCC_PCIE_AXI_CBCR_SLEEP_BMSK                                                               0xf0
#define HWIO_GCC_PCIE_AXI_CBCR_SLEEP_SHFT                                                                0x4
#define HWIO_GCC_PCIE_AXI_CBCR_CLK_ENABLE_BMSK                                                           0x1
#define HWIO_GCC_PCIE_AXI_CBCR_CLK_ENABLE_SHFT                                                           0x0

#define HWIO_GCC_PCIE_SLEEP_CBCR_ADDR                                                             (GCC_CLK_CTL_REG_REG_BASE      + 0x0005d014)
#define HWIO_GCC_PCIE_SLEEP_CBCR_RMSK                                                             0x80000001
#define HWIO_GCC_PCIE_SLEEP_CBCR_IN          \
        in_dword_masked(HWIO_GCC_PCIE_SLEEP_CBCR_ADDR, HWIO_GCC_PCIE_SLEEP_CBCR_RMSK)
#define HWIO_GCC_PCIE_SLEEP_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PCIE_SLEEP_CBCR_ADDR, m)
#define HWIO_GCC_PCIE_SLEEP_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_PCIE_SLEEP_CBCR_ADDR,v)
#define HWIO_GCC_PCIE_SLEEP_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PCIE_SLEEP_CBCR_ADDR,m,v,HWIO_GCC_PCIE_SLEEP_CBCR_IN)
#define HWIO_GCC_PCIE_SLEEP_CBCR_CLK_OFF_BMSK                                                     0x80000000
#define HWIO_GCC_PCIE_SLEEP_CBCR_CLK_OFF_SHFT                                                           0x1f
#define HWIO_GCC_PCIE_SLEEP_CBCR_CLK_ENABLE_BMSK                                                         0x1
#define HWIO_GCC_PCIE_SLEEP_CBCR_CLK_ENABLE_SHFT                                                         0x0

#define HWIO_GCC_PCIE_AXI_MSTR_CBCR_ADDR                                                          (GCC_CLK_CTL_REG_REG_BASE      + 0x0005d018)
#define HWIO_GCC_PCIE_AXI_MSTR_CBCR_RMSK                                                          0x80007ff1
#define HWIO_GCC_PCIE_AXI_MSTR_CBCR_IN          \
        in_dword_masked(HWIO_GCC_PCIE_AXI_MSTR_CBCR_ADDR, HWIO_GCC_PCIE_AXI_MSTR_CBCR_RMSK)
#define HWIO_GCC_PCIE_AXI_MSTR_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PCIE_AXI_MSTR_CBCR_ADDR, m)
#define HWIO_GCC_PCIE_AXI_MSTR_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_PCIE_AXI_MSTR_CBCR_ADDR,v)
#define HWIO_GCC_PCIE_AXI_MSTR_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PCIE_AXI_MSTR_CBCR_ADDR,m,v,HWIO_GCC_PCIE_AXI_MSTR_CBCR_IN)
#define HWIO_GCC_PCIE_AXI_MSTR_CBCR_CLK_OFF_BMSK                                                  0x80000000
#define HWIO_GCC_PCIE_AXI_MSTR_CBCR_CLK_OFF_SHFT                                                        0x1f
#define HWIO_GCC_PCIE_AXI_MSTR_CBCR_FORCE_MEM_CORE_ON_BMSK                                            0x4000
#define HWIO_GCC_PCIE_AXI_MSTR_CBCR_FORCE_MEM_CORE_ON_SHFT                                               0xe
#define HWIO_GCC_PCIE_AXI_MSTR_CBCR_FORCE_MEM_PERIPH_ON_BMSK                                          0x2000
#define HWIO_GCC_PCIE_AXI_MSTR_CBCR_FORCE_MEM_PERIPH_ON_SHFT                                             0xd
#define HWIO_GCC_PCIE_AXI_MSTR_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                                         0x1000
#define HWIO_GCC_PCIE_AXI_MSTR_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                                            0xc
#define HWIO_GCC_PCIE_AXI_MSTR_CBCR_WAKEUP_BMSK                                                        0xf00
#define HWIO_GCC_PCIE_AXI_MSTR_CBCR_WAKEUP_SHFT                                                          0x8
#define HWIO_GCC_PCIE_AXI_MSTR_CBCR_SLEEP_BMSK                                                          0xf0
#define HWIO_GCC_PCIE_AXI_MSTR_CBCR_SLEEP_SHFT                                                           0x4
#define HWIO_GCC_PCIE_AXI_MSTR_CBCR_CLK_ENABLE_BMSK                                                      0x1
#define HWIO_GCC_PCIE_AXI_MSTR_CBCR_CLK_ENABLE_SHFT                                                      0x0

#define HWIO_GCC_PCIE_PIPE_CMD_RCGR_ADDR                                                          (GCC_CLK_CTL_REG_REG_BASE      + 0x0005d01c)
#define HWIO_GCC_PCIE_PIPE_CMD_RCGR_RMSK                                                          0x80000013
#define HWIO_GCC_PCIE_PIPE_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_PCIE_PIPE_CMD_RCGR_ADDR, HWIO_GCC_PCIE_PIPE_CMD_RCGR_RMSK)
#define HWIO_GCC_PCIE_PIPE_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_PCIE_PIPE_CMD_RCGR_ADDR, m)
#define HWIO_GCC_PCIE_PIPE_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_PCIE_PIPE_CMD_RCGR_ADDR,v)
#define HWIO_GCC_PCIE_PIPE_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PCIE_PIPE_CMD_RCGR_ADDR,m,v,HWIO_GCC_PCIE_PIPE_CMD_RCGR_IN)
#define HWIO_GCC_PCIE_PIPE_CMD_RCGR_ROOT_OFF_BMSK                                                 0x80000000
#define HWIO_GCC_PCIE_PIPE_CMD_RCGR_ROOT_OFF_SHFT                                                       0x1f
#define HWIO_GCC_PCIE_PIPE_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                                 0x10
#define HWIO_GCC_PCIE_PIPE_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                                  0x4
#define HWIO_GCC_PCIE_PIPE_CMD_RCGR_ROOT_EN_BMSK                                                         0x2
#define HWIO_GCC_PCIE_PIPE_CMD_RCGR_ROOT_EN_SHFT                                                         0x1
#define HWIO_GCC_PCIE_PIPE_CMD_RCGR_UPDATE_BMSK                                                          0x1
#define HWIO_GCC_PCIE_PIPE_CMD_RCGR_UPDATE_SHFT                                                          0x0

#define HWIO_GCC_PCIE_PIPE_CFG_RCGR_ADDR                                                          (GCC_CLK_CTL_REG_REG_BASE      + 0x0005d020)
#define HWIO_GCC_PCIE_PIPE_CFG_RCGR_RMSK                                                               0x71f
#define HWIO_GCC_PCIE_PIPE_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_PCIE_PIPE_CFG_RCGR_ADDR, HWIO_GCC_PCIE_PIPE_CFG_RCGR_RMSK)
#define HWIO_GCC_PCIE_PIPE_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_PCIE_PIPE_CFG_RCGR_ADDR, m)
#define HWIO_GCC_PCIE_PIPE_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_PCIE_PIPE_CFG_RCGR_ADDR,v)
#define HWIO_GCC_PCIE_PIPE_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PCIE_PIPE_CFG_RCGR_ADDR,m,v,HWIO_GCC_PCIE_PIPE_CFG_RCGR_IN)
#define HWIO_GCC_PCIE_PIPE_CFG_RCGR_SRC_SEL_BMSK                                                       0x700
#define HWIO_GCC_PCIE_PIPE_CFG_RCGR_SRC_SEL_SHFT                                                         0x8
#define HWIO_GCC_PCIE_PIPE_CFG_RCGR_SRC_DIV_BMSK                                                        0x1f
#define HWIO_GCC_PCIE_PIPE_CFG_RCGR_SRC_DIV_SHFT                                                         0x0

#define HWIO_GCC_PCIE_AUX_CMD_RCGR_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x0005d030)
#define HWIO_GCC_PCIE_AUX_CMD_RCGR_RMSK                                                           0x800000f3
#define HWIO_GCC_PCIE_AUX_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_PCIE_AUX_CMD_RCGR_ADDR, HWIO_GCC_PCIE_AUX_CMD_RCGR_RMSK)
#define HWIO_GCC_PCIE_AUX_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_PCIE_AUX_CMD_RCGR_ADDR, m)
#define HWIO_GCC_PCIE_AUX_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_PCIE_AUX_CMD_RCGR_ADDR,v)
#define HWIO_GCC_PCIE_AUX_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PCIE_AUX_CMD_RCGR_ADDR,m,v,HWIO_GCC_PCIE_AUX_CMD_RCGR_IN)
#define HWIO_GCC_PCIE_AUX_CMD_RCGR_ROOT_OFF_BMSK                                                  0x80000000
#define HWIO_GCC_PCIE_AUX_CMD_RCGR_ROOT_OFF_SHFT                                                        0x1f
#define HWIO_GCC_PCIE_AUX_CMD_RCGR_DIRTY_D_BMSK                                                         0x80
#define HWIO_GCC_PCIE_AUX_CMD_RCGR_DIRTY_D_SHFT                                                          0x7
#define HWIO_GCC_PCIE_AUX_CMD_RCGR_DIRTY_M_BMSK                                                         0x40
#define HWIO_GCC_PCIE_AUX_CMD_RCGR_DIRTY_M_SHFT                                                          0x6
#define HWIO_GCC_PCIE_AUX_CMD_RCGR_DIRTY_N_BMSK                                                         0x20
#define HWIO_GCC_PCIE_AUX_CMD_RCGR_DIRTY_N_SHFT                                                          0x5
#define HWIO_GCC_PCIE_AUX_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                                  0x10
#define HWIO_GCC_PCIE_AUX_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                                   0x4
#define HWIO_GCC_PCIE_AUX_CMD_RCGR_ROOT_EN_BMSK                                                          0x2
#define HWIO_GCC_PCIE_AUX_CMD_RCGR_ROOT_EN_SHFT                                                          0x1
#define HWIO_GCC_PCIE_AUX_CMD_RCGR_UPDATE_BMSK                                                           0x1
#define HWIO_GCC_PCIE_AUX_CMD_RCGR_UPDATE_SHFT                                                           0x0

#define HWIO_GCC_PCIE_AUX_CFG_RCGR_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x0005d034)
#define HWIO_GCC_PCIE_AUX_CFG_RCGR_RMSK                                                               0x371f
#define HWIO_GCC_PCIE_AUX_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_PCIE_AUX_CFG_RCGR_ADDR, HWIO_GCC_PCIE_AUX_CFG_RCGR_RMSK)
#define HWIO_GCC_PCIE_AUX_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_PCIE_AUX_CFG_RCGR_ADDR, m)
#define HWIO_GCC_PCIE_AUX_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_PCIE_AUX_CFG_RCGR_ADDR,v)
#define HWIO_GCC_PCIE_AUX_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PCIE_AUX_CFG_RCGR_ADDR,m,v,HWIO_GCC_PCIE_AUX_CFG_RCGR_IN)
#define HWIO_GCC_PCIE_AUX_CFG_RCGR_MODE_BMSK                                                          0x3000
#define HWIO_GCC_PCIE_AUX_CFG_RCGR_MODE_SHFT                                                             0xc
#define HWIO_GCC_PCIE_AUX_CFG_RCGR_SRC_SEL_BMSK                                                        0x700
#define HWIO_GCC_PCIE_AUX_CFG_RCGR_SRC_SEL_SHFT                                                          0x8
#define HWIO_GCC_PCIE_AUX_CFG_RCGR_SRC_DIV_BMSK                                                         0x1f
#define HWIO_GCC_PCIE_AUX_CFG_RCGR_SRC_DIV_SHFT                                                          0x0

#define HWIO_GCC_PCIE_AUX_M_ADDR                                                                  (GCC_CLK_CTL_REG_REG_BASE      + 0x0005d038)
#define HWIO_GCC_PCIE_AUX_M_RMSK                                                                      0xffff
#define HWIO_GCC_PCIE_AUX_M_IN          \
        in_dword_masked(HWIO_GCC_PCIE_AUX_M_ADDR, HWIO_GCC_PCIE_AUX_M_RMSK)
#define HWIO_GCC_PCIE_AUX_M_INM(m)      \
        in_dword_masked(HWIO_GCC_PCIE_AUX_M_ADDR, m)
#define HWIO_GCC_PCIE_AUX_M_OUT(v)      \
        out_dword(HWIO_GCC_PCIE_AUX_M_ADDR,v)
#define HWIO_GCC_PCIE_AUX_M_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PCIE_AUX_M_ADDR,m,v,HWIO_GCC_PCIE_AUX_M_IN)
#define HWIO_GCC_PCIE_AUX_M_M_BMSK                                                                    0xffff
#define HWIO_GCC_PCIE_AUX_M_M_SHFT                                                                       0x0

#define HWIO_GCC_PCIE_AUX_N_ADDR                                                                  (GCC_CLK_CTL_REG_REG_BASE      + 0x0005d03c)
#define HWIO_GCC_PCIE_AUX_N_RMSK                                                                      0xffff
#define HWIO_GCC_PCIE_AUX_N_IN          \
        in_dword_masked(HWIO_GCC_PCIE_AUX_N_ADDR, HWIO_GCC_PCIE_AUX_N_RMSK)
#define HWIO_GCC_PCIE_AUX_N_INM(m)      \
        in_dword_masked(HWIO_GCC_PCIE_AUX_N_ADDR, m)
#define HWIO_GCC_PCIE_AUX_N_OUT(v)      \
        out_dword(HWIO_GCC_PCIE_AUX_N_ADDR,v)
#define HWIO_GCC_PCIE_AUX_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PCIE_AUX_N_ADDR,m,v,HWIO_GCC_PCIE_AUX_N_IN)
#define HWIO_GCC_PCIE_AUX_N_NOT_N_MINUS_M_BMSK                                                        0xffff
#define HWIO_GCC_PCIE_AUX_N_NOT_N_MINUS_M_SHFT                                                           0x0

#define HWIO_GCC_PCIE_AUX_D_ADDR                                                                  (GCC_CLK_CTL_REG_REG_BASE      + 0x0005d040)
#define HWIO_GCC_PCIE_AUX_D_RMSK                                                                      0xffff
#define HWIO_GCC_PCIE_AUX_D_IN          \
        in_dword_masked(HWIO_GCC_PCIE_AUX_D_ADDR, HWIO_GCC_PCIE_AUX_D_RMSK)
#define HWIO_GCC_PCIE_AUX_D_INM(m)      \
        in_dword_masked(HWIO_GCC_PCIE_AUX_D_ADDR, m)
#define HWIO_GCC_PCIE_AUX_D_OUT(v)      \
        out_dword(HWIO_GCC_PCIE_AUX_D_ADDR,v)
#define HWIO_GCC_PCIE_AUX_D_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PCIE_AUX_D_ADDR,m,v,HWIO_GCC_PCIE_AUX_D_IN)
#define HWIO_GCC_PCIE_AUX_D_NOT_2D_BMSK                                                               0xffff
#define HWIO_GCC_PCIE_AUX_D_NOT_2D_SHFT                                                                  0x0

#define HWIO_GCC_PCIE_GDSCR_ADDR                                                                  (GCC_CLK_CTL_REG_REG_BASE      + 0x0005d044)
#define HWIO_GCC_PCIE_GDSCR_RMSK                                                                  0xf8ffffff
#define HWIO_GCC_PCIE_GDSCR_IN          \
        in_dword_masked(HWIO_GCC_PCIE_GDSCR_ADDR, HWIO_GCC_PCIE_GDSCR_RMSK)
#define HWIO_GCC_PCIE_GDSCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PCIE_GDSCR_ADDR, m)
#define HWIO_GCC_PCIE_GDSCR_OUT(v)      \
        out_dword(HWIO_GCC_PCIE_GDSCR_ADDR,v)
#define HWIO_GCC_PCIE_GDSCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PCIE_GDSCR_ADDR,m,v,HWIO_GCC_PCIE_GDSCR_IN)
#define HWIO_GCC_PCIE_GDSCR_PWR_ON_BMSK                                                           0x80000000
#define HWIO_GCC_PCIE_GDSCR_PWR_ON_SHFT                                                                 0x1f
#define HWIO_GCC_PCIE_GDSCR_GDSC_STATE_BMSK                                                       0x78000000
#define HWIO_GCC_PCIE_GDSCR_GDSC_STATE_SHFT                                                             0x1b
#define HWIO_GCC_PCIE_GDSCR_EN_REST_WAIT_BMSK                                                       0xf00000
#define HWIO_GCC_PCIE_GDSCR_EN_REST_WAIT_SHFT                                                           0x14
#define HWIO_GCC_PCIE_GDSCR_EN_FEW_WAIT_BMSK                                                         0xf0000
#define HWIO_GCC_PCIE_GDSCR_EN_FEW_WAIT_SHFT                                                            0x10
#define HWIO_GCC_PCIE_GDSCR_CLK_DIS_WAIT_BMSK                                                         0xf000
#define HWIO_GCC_PCIE_GDSCR_CLK_DIS_WAIT_SHFT                                                            0xc
#define HWIO_GCC_PCIE_GDSCR_RETAIN_FF_ENABLE_BMSK                                                      0x800
#define HWIO_GCC_PCIE_GDSCR_RETAIN_FF_ENABLE_SHFT                                                        0xb
#define HWIO_GCC_PCIE_GDSCR_RESTORE_BMSK                                                               0x400
#define HWIO_GCC_PCIE_GDSCR_RESTORE_SHFT                                                                 0xa
#define HWIO_GCC_PCIE_GDSCR_SAVE_BMSK                                                                  0x200
#define HWIO_GCC_PCIE_GDSCR_SAVE_SHFT                                                                    0x9
#define HWIO_GCC_PCIE_GDSCR_RETAIN_BMSK                                                                0x100
#define HWIO_GCC_PCIE_GDSCR_RETAIN_SHFT                                                                  0x8
#define HWIO_GCC_PCIE_GDSCR_EN_REST_BMSK                                                                0x80
#define HWIO_GCC_PCIE_GDSCR_EN_REST_SHFT                                                                 0x7
#define HWIO_GCC_PCIE_GDSCR_EN_FEW_BMSK                                                                 0x40
#define HWIO_GCC_PCIE_GDSCR_EN_FEW_SHFT                                                                  0x6
#define HWIO_GCC_PCIE_GDSCR_CLAMP_IO_BMSK                                                               0x20
#define HWIO_GCC_PCIE_GDSCR_CLAMP_IO_SHFT                                                                0x5
#define HWIO_GCC_PCIE_GDSCR_CLK_DISABLE_BMSK                                                            0x10
#define HWIO_GCC_PCIE_GDSCR_CLK_DISABLE_SHFT                                                             0x4
#define HWIO_GCC_PCIE_GDSCR_PD_ARES_BMSK                                                                 0x8
#define HWIO_GCC_PCIE_GDSCR_PD_ARES_SHFT                                                                 0x3
#define HWIO_GCC_PCIE_GDSCR_SW_OVERRIDE_BMSK                                                             0x4
#define HWIO_GCC_PCIE_GDSCR_SW_OVERRIDE_SHFT                                                             0x2
#define HWIO_GCC_PCIE_GDSCR_HW_CONTROL_BMSK                                                              0x2
#define HWIO_GCC_PCIE_GDSCR_HW_CONTROL_SHFT                                                              0x1
#define HWIO_GCC_PCIE_GDSCR_SW_COLLAPSE_BMSK                                                             0x1
#define HWIO_GCC_PCIE_GDSCR_SW_COLLAPSE_SHFT                                                             0x0

#define HWIO_GCC_PCIEPHY_PHY_BCR_ADDR                                                             (GCC_CLK_CTL_REG_REG_BASE      + 0x0005d048)
#define HWIO_GCC_PCIEPHY_PHY_BCR_RMSK                                                                    0x1
#define HWIO_GCC_PCIEPHY_PHY_BCR_IN          \
        in_dword_masked(HWIO_GCC_PCIEPHY_PHY_BCR_ADDR, HWIO_GCC_PCIEPHY_PHY_BCR_RMSK)
#define HWIO_GCC_PCIEPHY_PHY_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PCIEPHY_PHY_BCR_ADDR, m)
#define HWIO_GCC_PCIEPHY_PHY_BCR_OUT(v)      \
        out_dword(HWIO_GCC_PCIEPHY_PHY_BCR_ADDR,v)
#define HWIO_GCC_PCIEPHY_PHY_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PCIEPHY_PHY_BCR_ADDR,m,v,HWIO_GCC_PCIEPHY_PHY_BCR_IN)
#define HWIO_GCC_PCIEPHY_PHY_BCR_BLK_ARES_BMSK                                                           0x1
#define HWIO_GCC_PCIEPHY_PHY_BCR_BLK_ARES_SHFT                                                           0x0

#define HWIO_GCC_PCIE_GPIO_LDO_EN_ADDR                                                            (GCC_CLK_CTL_REG_REG_BASE      + 0x0005d04c)
#define HWIO_GCC_PCIE_GPIO_LDO_EN_RMSK                                                                   0x1
#define HWIO_GCC_PCIE_GPIO_LDO_EN_IN          \
        in_dword_masked(HWIO_GCC_PCIE_GPIO_LDO_EN_ADDR, HWIO_GCC_PCIE_GPIO_LDO_EN_RMSK)
#define HWIO_GCC_PCIE_GPIO_LDO_EN_INM(m)      \
        in_dword_masked(HWIO_GCC_PCIE_GPIO_LDO_EN_ADDR, m)
#define HWIO_GCC_PCIE_GPIO_LDO_EN_OUT(v)      \
        out_dword(HWIO_GCC_PCIE_GPIO_LDO_EN_ADDR,v)
#define HWIO_GCC_PCIE_GPIO_LDO_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PCIE_GPIO_LDO_EN_ADDR,m,v,HWIO_GCC_PCIE_GPIO_LDO_EN_IN)
#define HWIO_GCC_PCIE_GPIO_LDO_EN_LDO_ENABLE_BMSK                                                        0x1
#define HWIO_GCC_PCIE_GPIO_LDO_EN_LDO_ENABLE_SHFT                                                        0x0

#define HWIO_GCC_PCIE_PHY_BCR_ADDR                                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x0005d050)
#define HWIO_GCC_PCIE_PHY_BCR_RMSK                                                                       0x1
#define HWIO_GCC_PCIE_PHY_BCR_IN          \
        in_dword_masked(HWIO_GCC_PCIE_PHY_BCR_ADDR, HWIO_GCC_PCIE_PHY_BCR_RMSK)
#define HWIO_GCC_PCIE_PHY_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PCIE_PHY_BCR_ADDR, m)
#define HWIO_GCC_PCIE_PHY_BCR_OUT(v)      \
        out_dword(HWIO_GCC_PCIE_PHY_BCR_ADDR,v)
#define HWIO_GCC_PCIE_PHY_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PCIE_PHY_BCR_ADDR,m,v,HWIO_GCC_PCIE_PHY_BCR_IN)
#define HWIO_GCC_PCIE_PHY_BCR_BLK_ARES_BMSK                                                              0x1
#define HWIO_GCC_PCIE_PHY_BCR_BLK_ARES_SHFT                                                              0x0

#define HWIO_GCC_PCIE_MISC_RESET_ADDR                                                             (GCC_CLK_CTL_REG_REG_BASE      + 0x0005d054)
#define HWIO_GCC_PCIE_MISC_RESET_RMSK                                                                   0x7f
#define HWIO_GCC_PCIE_MISC_RESET_IN          \
        in_dword_masked(HWIO_GCC_PCIE_MISC_RESET_ADDR, HWIO_GCC_PCIE_MISC_RESET_RMSK)
#define HWIO_GCC_PCIE_MISC_RESET_INM(m)      \
        in_dword_masked(HWIO_GCC_PCIE_MISC_RESET_ADDR, m)
#define HWIO_GCC_PCIE_MISC_RESET_OUT(v)      \
        out_dword(HWIO_GCC_PCIE_MISC_RESET_ADDR,v)
#define HWIO_GCC_PCIE_MISC_RESET_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PCIE_MISC_RESET_ADDR,m,v,HWIO_GCC_PCIE_MISC_RESET_IN)
#define HWIO_GCC_PCIE_MISC_RESET_AXI_MASTER_STICKY_ARES_BMSK                                            0x40
#define HWIO_GCC_PCIE_MISC_RESET_AXI_MASTER_STICKY_ARES_SHFT                                             0x6
#define HWIO_GCC_PCIE_MISC_RESET_AHB_ARES_BMSK                                                          0x20
#define HWIO_GCC_PCIE_MISC_RESET_AHB_ARES_SHFT                                                           0x5
#define HWIO_GCC_PCIE_MISC_RESET_AXI_SLAVE_ARES_BMSK                                                    0x10
#define HWIO_GCC_PCIE_MISC_RESET_AXI_SLAVE_ARES_SHFT                                                     0x4
#define HWIO_GCC_PCIE_MISC_RESET_AXI_MASTER_ARES_BMSK                                                    0x8
#define HWIO_GCC_PCIE_MISC_RESET_AXI_MASTER_ARES_SHFT                                                    0x3
#define HWIO_GCC_PCIE_MISC_RESET_CORE_STICKY_ARES_BMSK                                                   0x4
#define HWIO_GCC_PCIE_MISC_RESET_CORE_STICKY_ARES_SHFT                                                   0x2
#define HWIO_GCC_PCIE_MISC_RESET_SLEEP_ARES_BMSK                                                         0x2
#define HWIO_GCC_PCIE_MISC_RESET_SLEEP_ARES_SHFT                                                         0x1
#define HWIO_GCC_PCIE_MISC_RESET_PIPE_ARES_BMSK                                                          0x1
#define HWIO_GCC_PCIE_MISC_RESET_PIPE_ARES_SHFT                                                          0x0

#define HWIO_GCC_PCIE_LINK_DOWN_BCR_ADDR                                                          (GCC_CLK_CTL_REG_REG_BASE      + 0x0005d05c)
#define HWIO_GCC_PCIE_LINK_DOWN_BCR_RMSK                                                                 0x1
#define HWIO_GCC_PCIE_LINK_DOWN_BCR_IN          \
        in_dword_masked(HWIO_GCC_PCIE_LINK_DOWN_BCR_ADDR, HWIO_GCC_PCIE_LINK_DOWN_BCR_RMSK)
#define HWIO_GCC_PCIE_LINK_DOWN_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PCIE_LINK_DOWN_BCR_ADDR, m)
#define HWIO_GCC_PCIE_LINK_DOWN_BCR_OUT(v)      \
        out_dword(HWIO_GCC_PCIE_LINK_DOWN_BCR_ADDR,v)
#define HWIO_GCC_PCIE_LINK_DOWN_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PCIE_LINK_DOWN_BCR_ADDR,m,v,HWIO_GCC_PCIE_LINK_DOWN_BCR_IN)
#define HWIO_GCC_PCIE_LINK_DOWN_BCR_BLK_ARES_BMSK                                                        0x1
#define HWIO_GCC_PCIE_LINK_DOWN_BCR_BLK_ARES_SHFT                                                        0x0

#define HWIO_GCC_USB_SS_LDO_EN_ADDR                                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x0005e07c)
#define HWIO_GCC_USB_SS_LDO_EN_RMSK                                                                      0x1
#define HWIO_GCC_USB_SS_LDO_EN_IN          \
        in_dword_masked(HWIO_GCC_USB_SS_LDO_EN_ADDR, HWIO_GCC_USB_SS_LDO_EN_RMSK)
#define HWIO_GCC_USB_SS_LDO_EN_INM(m)      \
        in_dword_masked(HWIO_GCC_USB_SS_LDO_EN_ADDR, m)
#define HWIO_GCC_USB_SS_LDO_EN_OUT(v)      \
        out_dword(HWIO_GCC_USB_SS_LDO_EN_ADDR,v)
#define HWIO_GCC_USB_SS_LDO_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_USB_SS_LDO_EN_ADDR,m,v,HWIO_GCC_USB_SS_LDO_EN_IN)
#define HWIO_GCC_USB_SS_LDO_EN_LDO_ENABLE_BMSK                                                           0x1
#define HWIO_GCC_USB_SS_LDO_EN_LDO_ENABLE_SHFT                                                           0x0

#define HWIO_GCC_USB_PHY_CFG_AHB_CBCR_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x0005e080)
#define HWIO_GCC_USB_PHY_CFG_AHB_CBCR_RMSK                                                        0xf0008001
#define HWIO_GCC_USB_PHY_CFG_AHB_CBCR_IN          \
        in_dword_masked(HWIO_GCC_USB_PHY_CFG_AHB_CBCR_ADDR, HWIO_GCC_USB_PHY_CFG_AHB_CBCR_RMSK)
#define HWIO_GCC_USB_PHY_CFG_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_USB_PHY_CFG_AHB_CBCR_ADDR, m)
#define HWIO_GCC_USB_PHY_CFG_AHB_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_USB_PHY_CFG_AHB_CBCR_ADDR,v)
#define HWIO_GCC_USB_PHY_CFG_AHB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_USB_PHY_CFG_AHB_CBCR_ADDR,m,v,HWIO_GCC_USB_PHY_CFG_AHB_CBCR_IN)
#define HWIO_GCC_USB_PHY_CFG_AHB_CBCR_CLK_OFF_BMSK                                                0x80000000
#define HWIO_GCC_USB_PHY_CFG_AHB_CBCR_CLK_OFF_SHFT                                                      0x1f
#define HWIO_GCC_USB_PHY_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                               0x70000000
#define HWIO_GCC_USB_PHY_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                     0x1c
#define HWIO_GCC_USB_PHY_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                       0x8000
#define HWIO_GCC_USB_PHY_CFG_AHB_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                          0xf
#define HWIO_GCC_USB_PHY_CFG_AHB_CBCR_CLK_ENABLE_BMSK                                                    0x1
#define HWIO_GCC_USB_PHY_CFG_AHB_CBCR_CLK_ENABLE_SHFT                                                    0x0

#define HWIO_GCC_SYS_NOC_USB3_AXI_CBCR_ADDR                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x0005e084)
#define HWIO_GCC_SYS_NOC_USB3_AXI_CBCR_RMSK                                                       0x80000001
#define HWIO_GCC_SYS_NOC_USB3_AXI_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SYS_NOC_USB3_AXI_CBCR_ADDR, HWIO_GCC_SYS_NOC_USB3_AXI_CBCR_RMSK)
#define HWIO_GCC_SYS_NOC_USB3_AXI_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SYS_NOC_USB3_AXI_CBCR_ADDR, m)
#define HWIO_GCC_SYS_NOC_USB3_AXI_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_SYS_NOC_USB3_AXI_CBCR_ADDR,v)
#define HWIO_GCC_SYS_NOC_USB3_AXI_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SYS_NOC_USB3_AXI_CBCR_ADDR,m,v,HWIO_GCC_SYS_NOC_USB3_AXI_CBCR_IN)
#define HWIO_GCC_SYS_NOC_USB3_AXI_CBCR_CLK_OFF_BMSK                                               0x80000000
#define HWIO_GCC_SYS_NOC_USB3_AXI_CBCR_CLK_OFF_SHFT                                                     0x1f
#define HWIO_GCC_SYS_NOC_USB3_AXI_CBCR_CLK_ENABLE_BMSK                                                   0x1
#define HWIO_GCC_SYS_NOC_USB3_AXI_CBCR_CLK_ENABLE_SHFT                                                   0x0

#define HWIO_GCC_USB_30_BCR_ADDR                                                                  (GCC_CLK_CTL_REG_REG_BASE      + 0x0005e070)
#define HWIO_GCC_USB_30_BCR_RMSK                                                                         0x1
#define HWIO_GCC_USB_30_BCR_IN          \
        in_dword_masked(HWIO_GCC_USB_30_BCR_ADDR, HWIO_GCC_USB_30_BCR_RMSK)
#define HWIO_GCC_USB_30_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_USB_30_BCR_ADDR, m)
#define HWIO_GCC_USB_30_BCR_OUT(v)      \
        out_dword(HWIO_GCC_USB_30_BCR_ADDR,v)
#define HWIO_GCC_USB_30_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_USB_30_BCR_ADDR,m,v,HWIO_GCC_USB_30_BCR_IN)
#define HWIO_GCC_USB_30_BCR_BLK_ARES_BMSK                                                                0x1
#define HWIO_GCC_USB_30_BCR_BLK_ARES_SHFT                                                                0x0

#define HWIO_GCC_USB_30_MISC_ADDR                                                                 (GCC_CLK_CTL_REG_REG_BASE      + 0x0005e074)
#define HWIO_GCC_USB_30_MISC_RMSK                                                                        0x1
#define HWIO_GCC_USB_30_MISC_IN          \
        in_dword_masked(HWIO_GCC_USB_30_MISC_ADDR, HWIO_GCC_USB_30_MISC_RMSK)
#define HWIO_GCC_USB_30_MISC_INM(m)      \
        in_dword_masked(HWIO_GCC_USB_30_MISC_ADDR, m)
#define HWIO_GCC_USB_30_MISC_OUT(v)      \
        out_dword(HWIO_GCC_USB_30_MISC_ADDR,v)
#define HWIO_GCC_USB_30_MISC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_USB_30_MISC_ADDR,m,v,HWIO_GCC_USB_30_MISC_IN)
#define HWIO_GCC_USB_30_MISC_BLK_ARES_ALL_BMSK                                                           0x1
#define HWIO_GCC_USB_30_MISC_BLK_ARES_ALL_SHFT                                                           0x0

#define HWIO_GCC_USB30_MASTER_CBCR_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x0005e000)
#define HWIO_GCC_USB30_MASTER_CBCR_RMSK                                                           0xf000fff1
#define HWIO_GCC_USB30_MASTER_CBCR_IN          \
        in_dword_masked(HWIO_GCC_USB30_MASTER_CBCR_ADDR, HWIO_GCC_USB30_MASTER_CBCR_RMSK)
#define HWIO_GCC_USB30_MASTER_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_USB30_MASTER_CBCR_ADDR, m)
#define HWIO_GCC_USB30_MASTER_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_USB30_MASTER_CBCR_ADDR,v)
#define HWIO_GCC_USB30_MASTER_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_USB30_MASTER_CBCR_ADDR,m,v,HWIO_GCC_USB30_MASTER_CBCR_IN)
#define HWIO_GCC_USB30_MASTER_CBCR_CLK_OFF_BMSK                                                   0x80000000
#define HWIO_GCC_USB30_MASTER_CBCR_CLK_OFF_SHFT                                                         0x1f
#define HWIO_GCC_USB30_MASTER_CBCR_NOC_HANDSHAKE_FSM_STATUS_BMSK                                  0x70000000
#define HWIO_GCC_USB30_MASTER_CBCR_NOC_HANDSHAKE_FSM_STATUS_SHFT                                        0x1c
#define HWIO_GCC_USB30_MASTER_CBCR_NOC_HANDSHAKE_FSM_EN_BMSK                                          0x8000
#define HWIO_GCC_USB30_MASTER_CBCR_NOC_HANDSHAKE_FSM_EN_SHFT                                             0xf
#define HWIO_GCC_USB30_MASTER_CBCR_FORCE_MEM_CORE_ON_BMSK                                             0x4000
#define HWIO_GCC_USB30_MASTER_CBCR_FORCE_MEM_CORE_ON_SHFT                                                0xe
#define HWIO_GCC_USB30_MASTER_CBCR_FORCE_MEM_PERIPH_ON_BMSK                                           0x2000
#define HWIO_GCC_USB30_MASTER_CBCR_FORCE_MEM_PERIPH_ON_SHFT                                              0xd
#define HWIO_GCC_USB30_MASTER_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                                          0x1000
#define HWIO_GCC_USB30_MASTER_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                                             0xc
#define HWIO_GCC_USB30_MASTER_CBCR_WAKEUP_BMSK                                                         0xf00
#define HWIO_GCC_USB30_MASTER_CBCR_WAKEUP_SHFT                                                           0x8
#define HWIO_GCC_USB30_MASTER_CBCR_SLEEP_BMSK                                                           0xf0
#define HWIO_GCC_USB30_MASTER_CBCR_SLEEP_SHFT                                                            0x4
#define HWIO_GCC_USB30_MASTER_CBCR_CLK_ENABLE_BMSK                                                       0x1
#define HWIO_GCC_USB30_MASTER_CBCR_CLK_ENABLE_SHFT                                                       0x0

#define HWIO_GCC_USB30_SLEEP_CBCR_ADDR                                                            (GCC_CLK_CTL_REG_REG_BASE      + 0x0005e004)
#define HWIO_GCC_USB30_SLEEP_CBCR_RMSK                                                            0x80000001
#define HWIO_GCC_USB30_SLEEP_CBCR_IN          \
        in_dword_masked(HWIO_GCC_USB30_SLEEP_CBCR_ADDR, HWIO_GCC_USB30_SLEEP_CBCR_RMSK)
#define HWIO_GCC_USB30_SLEEP_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_USB30_SLEEP_CBCR_ADDR, m)
#define HWIO_GCC_USB30_SLEEP_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_USB30_SLEEP_CBCR_ADDR,v)
#define HWIO_GCC_USB30_SLEEP_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_USB30_SLEEP_CBCR_ADDR,m,v,HWIO_GCC_USB30_SLEEP_CBCR_IN)
#define HWIO_GCC_USB30_SLEEP_CBCR_CLK_OFF_BMSK                                                    0x80000000
#define HWIO_GCC_USB30_SLEEP_CBCR_CLK_OFF_SHFT                                                          0x1f
#define HWIO_GCC_USB30_SLEEP_CBCR_CLK_ENABLE_BMSK                                                        0x1
#define HWIO_GCC_USB30_SLEEP_CBCR_CLK_ENABLE_SHFT                                                        0x0

#define HWIO_GCC_USB30_MOCK_UTMI_CBCR_ADDR                                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x0005e008)
#define HWIO_GCC_USB30_MOCK_UTMI_CBCR_RMSK                                                        0x80030001
#define HWIO_GCC_USB30_MOCK_UTMI_CBCR_IN          \
        in_dword_masked(HWIO_GCC_USB30_MOCK_UTMI_CBCR_ADDR, HWIO_GCC_USB30_MOCK_UTMI_CBCR_RMSK)
#define HWIO_GCC_USB30_MOCK_UTMI_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_USB30_MOCK_UTMI_CBCR_ADDR, m)
#define HWIO_GCC_USB30_MOCK_UTMI_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_USB30_MOCK_UTMI_CBCR_ADDR,v)
#define HWIO_GCC_USB30_MOCK_UTMI_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_USB30_MOCK_UTMI_CBCR_ADDR,m,v,HWIO_GCC_USB30_MOCK_UTMI_CBCR_IN)
#define HWIO_GCC_USB30_MOCK_UTMI_CBCR_CLK_OFF_BMSK                                                0x80000000
#define HWIO_GCC_USB30_MOCK_UTMI_CBCR_CLK_OFF_SHFT                                                      0x1f
#define HWIO_GCC_USB30_MOCK_UTMI_CBCR_CLK_DIV_BMSK                                                   0x30000
#define HWIO_GCC_USB30_MOCK_UTMI_CBCR_CLK_DIV_SHFT                                                      0x10
#define HWIO_GCC_USB30_MOCK_UTMI_CBCR_CLK_ENABLE_BMSK                                                    0x1
#define HWIO_GCC_USB30_MOCK_UTMI_CBCR_CLK_ENABLE_SHFT                                                    0x0

#define HWIO_GCC_USB30_MASTER_CMD_RCGR_ADDR                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x0005e00c)
#define HWIO_GCC_USB30_MASTER_CMD_RCGR_RMSK                                                       0x800000f3
#define HWIO_GCC_USB30_MASTER_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_USB30_MASTER_CMD_RCGR_ADDR, HWIO_GCC_USB30_MASTER_CMD_RCGR_RMSK)
#define HWIO_GCC_USB30_MASTER_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_USB30_MASTER_CMD_RCGR_ADDR, m)
#define HWIO_GCC_USB30_MASTER_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_USB30_MASTER_CMD_RCGR_ADDR,v)
#define HWIO_GCC_USB30_MASTER_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_USB30_MASTER_CMD_RCGR_ADDR,m,v,HWIO_GCC_USB30_MASTER_CMD_RCGR_IN)
#define HWIO_GCC_USB30_MASTER_CMD_RCGR_ROOT_OFF_BMSK                                              0x80000000
#define HWIO_GCC_USB30_MASTER_CMD_RCGR_ROOT_OFF_SHFT                                                    0x1f
#define HWIO_GCC_USB30_MASTER_CMD_RCGR_DIRTY_D_BMSK                                                     0x80
#define HWIO_GCC_USB30_MASTER_CMD_RCGR_DIRTY_D_SHFT                                                      0x7
#define HWIO_GCC_USB30_MASTER_CMD_RCGR_DIRTY_M_BMSK                                                     0x40
#define HWIO_GCC_USB30_MASTER_CMD_RCGR_DIRTY_M_SHFT                                                      0x6
#define HWIO_GCC_USB30_MASTER_CMD_RCGR_DIRTY_N_BMSK                                                     0x20
#define HWIO_GCC_USB30_MASTER_CMD_RCGR_DIRTY_N_SHFT                                                      0x5
#define HWIO_GCC_USB30_MASTER_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                              0x10
#define HWIO_GCC_USB30_MASTER_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                               0x4
#define HWIO_GCC_USB30_MASTER_CMD_RCGR_ROOT_EN_BMSK                                                      0x2
#define HWIO_GCC_USB30_MASTER_CMD_RCGR_ROOT_EN_SHFT                                                      0x1
#define HWIO_GCC_USB30_MASTER_CMD_RCGR_UPDATE_BMSK                                                       0x1
#define HWIO_GCC_USB30_MASTER_CMD_RCGR_UPDATE_SHFT                                                       0x0

#define HWIO_GCC_USB30_MASTER_CFG_RCGR_ADDR                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x0005e010)
#define HWIO_GCC_USB30_MASTER_CFG_RCGR_RMSK                                                           0x371f
#define HWIO_GCC_USB30_MASTER_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_USB30_MASTER_CFG_RCGR_ADDR, HWIO_GCC_USB30_MASTER_CFG_RCGR_RMSK)
#define HWIO_GCC_USB30_MASTER_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_USB30_MASTER_CFG_RCGR_ADDR, m)
#define HWIO_GCC_USB30_MASTER_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_USB30_MASTER_CFG_RCGR_ADDR,v)
#define HWIO_GCC_USB30_MASTER_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_USB30_MASTER_CFG_RCGR_ADDR,m,v,HWIO_GCC_USB30_MASTER_CFG_RCGR_IN)
#define HWIO_GCC_USB30_MASTER_CFG_RCGR_MODE_BMSK                                                      0x3000
#define HWIO_GCC_USB30_MASTER_CFG_RCGR_MODE_SHFT                                                         0xc
#define HWIO_GCC_USB30_MASTER_CFG_RCGR_SRC_SEL_BMSK                                                    0x700
#define HWIO_GCC_USB30_MASTER_CFG_RCGR_SRC_SEL_SHFT                                                      0x8
#define HWIO_GCC_USB30_MASTER_CFG_RCGR_SRC_DIV_BMSK                                                     0x1f
#define HWIO_GCC_USB30_MASTER_CFG_RCGR_SRC_DIV_SHFT                                                      0x0

#define HWIO_GCC_USB30_MASTER_M_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x0005e014)
#define HWIO_GCC_USB30_MASTER_M_RMSK                                                                    0xff
#define HWIO_GCC_USB30_MASTER_M_IN          \
        in_dword_masked(HWIO_GCC_USB30_MASTER_M_ADDR, HWIO_GCC_USB30_MASTER_M_RMSK)
#define HWIO_GCC_USB30_MASTER_M_INM(m)      \
        in_dword_masked(HWIO_GCC_USB30_MASTER_M_ADDR, m)
#define HWIO_GCC_USB30_MASTER_M_OUT(v)      \
        out_dword(HWIO_GCC_USB30_MASTER_M_ADDR,v)
#define HWIO_GCC_USB30_MASTER_M_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_USB30_MASTER_M_ADDR,m,v,HWIO_GCC_USB30_MASTER_M_IN)
#define HWIO_GCC_USB30_MASTER_M_M_BMSK                                                                  0xff
#define HWIO_GCC_USB30_MASTER_M_M_SHFT                                                                   0x0

#define HWIO_GCC_USB30_MASTER_N_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x0005e018)
#define HWIO_GCC_USB30_MASTER_N_RMSK                                                                    0xff
#define HWIO_GCC_USB30_MASTER_N_IN          \
        in_dword_masked(HWIO_GCC_USB30_MASTER_N_ADDR, HWIO_GCC_USB30_MASTER_N_RMSK)
#define HWIO_GCC_USB30_MASTER_N_INM(m)      \
        in_dword_masked(HWIO_GCC_USB30_MASTER_N_ADDR, m)
#define HWIO_GCC_USB30_MASTER_N_OUT(v)      \
        out_dword(HWIO_GCC_USB30_MASTER_N_ADDR,v)
#define HWIO_GCC_USB30_MASTER_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_USB30_MASTER_N_ADDR,m,v,HWIO_GCC_USB30_MASTER_N_IN)
#define HWIO_GCC_USB30_MASTER_N_NOT_N_MINUS_M_BMSK                                                      0xff
#define HWIO_GCC_USB30_MASTER_N_NOT_N_MINUS_M_SHFT                                                       0x0

#define HWIO_GCC_USB30_MASTER_D_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x0005e01c)
#define HWIO_GCC_USB30_MASTER_D_RMSK                                                                    0xff
#define HWIO_GCC_USB30_MASTER_D_IN          \
        in_dword_masked(HWIO_GCC_USB30_MASTER_D_ADDR, HWIO_GCC_USB30_MASTER_D_RMSK)
#define HWIO_GCC_USB30_MASTER_D_INM(m)      \
        in_dword_masked(HWIO_GCC_USB30_MASTER_D_ADDR, m)
#define HWIO_GCC_USB30_MASTER_D_OUT(v)      \
        out_dword(HWIO_GCC_USB30_MASTER_D_ADDR,v)
#define HWIO_GCC_USB30_MASTER_D_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_USB30_MASTER_D_ADDR,m,v,HWIO_GCC_USB30_MASTER_D_IN)
#define HWIO_GCC_USB30_MASTER_D_NOT_2D_BMSK                                                             0xff
#define HWIO_GCC_USB30_MASTER_D_NOT_2D_SHFT                                                              0x0

#define HWIO_GCC_USB30_MOCK_UTMI_CMD_RCGR_ADDR                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x0005e020)
#define HWIO_GCC_USB30_MOCK_UTMI_CMD_RCGR_RMSK                                                    0x80000013
#define HWIO_GCC_USB30_MOCK_UTMI_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_USB30_MOCK_UTMI_CMD_RCGR_ADDR, HWIO_GCC_USB30_MOCK_UTMI_CMD_RCGR_RMSK)
#define HWIO_GCC_USB30_MOCK_UTMI_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_USB30_MOCK_UTMI_CMD_RCGR_ADDR, m)
#define HWIO_GCC_USB30_MOCK_UTMI_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_USB30_MOCK_UTMI_CMD_RCGR_ADDR,v)
#define HWIO_GCC_USB30_MOCK_UTMI_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_USB30_MOCK_UTMI_CMD_RCGR_ADDR,m,v,HWIO_GCC_USB30_MOCK_UTMI_CMD_RCGR_IN)
#define HWIO_GCC_USB30_MOCK_UTMI_CMD_RCGR_ROOT_OFF_BMSK                                           0x80000000
#define HWIO_GCC_USB30_MOCK_UTMI_CMD_RCGR_ROOT_OFF_SHFT                                                 0x1f
#define HWIO_GCC_USB30_MOCK_UTMI_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                           0x10
#define HWIO_GCC_USB30_MOCK_UTMI_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                            0x4
#define HWIO_GCC_USB30_MOCK_UTMI_CMD_RCGR_ROOT_EN_BMSK                                                   0x2
#define HWIO_GCC_USB30_MOCK_UTMI_CMD_RCGR_ROOT_EN_SHFT                                                   0x1
#define HWIO_GCC_USB30_MOCK_UTMI_CMD_RCGR_UPDATE_BMSK                                                    0x1
#define HWIO_GCC_USB30_MOCK_UTMI_CMD_RCGR_UPDATE_SHFT                                                    0x0

#define HWIO_GCC_USB30_MOCK_UTMI_CFG_RCGR_ADDR                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x0005e024)
#define HWIO_GCC_USB30_MOCK_UTMI_CFG_RCGR_RMSK                                                         0x71f
#define HWIO_GCC_USB30_MOCK_UTMI_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_USB30_MOCK_UTMI_CFG_RCGR_ADDR, HWIO_GCC_USB30_MOCK_UTMI_CFG_RCGR_RMSK)
#define HWIO_GCC_USB30_MOCK_UTMI_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_USB30_MOCK_UTMI_CFG_RCGR_ADDR, m)
#define HWIO_GCC_USB30_MOCK_UTMI_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_USB30_MOCK_UTMI_CFG_RCGR_ADDR,v)
#define HWIO_GCC_USB30_MOCK_UTMI_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_USB30_MOCK_UTMI_CFG_RCGR_ADDR,m,v,HWIO_GCC_USB30_MOCK_UTMI_CFG_RCGR_IN)
#define HWIO_GCC_USB30_MOCK_UTMI_CFG_RCGR_SRC_SEL_BMSK                                                 0x700
#define HWIO_GCC_USB30_MOCK_UTMI_CFG_RCGR_SRC_SEL_SHFT                                                   0x8
#define HWIO_GCC_USB30_MOCK_UTMI_CFG_RCGR_SRC_DIV_BMSK                                                  0x1f
#define HWIO_GCC_USB30_MOCK_UTMI_CFG_RCGR_SRC_DIV_SHFT                                                   0x0

#define HWIO_GCC_USB3_PHY_BCR_ADDR                                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x0005e034)
#define HWIO_GCC_USB3_PHY_BCR_RMSK                                                                       0x1
#define HWIO_GCC_USB3_PHY_BCR_IN          \
        in_dword_masked(HWIO_GCC_USB3_PHY_BCR_ADDR, HWIO_GCC_USB3_PHY_BCR_RMSK)
#define HWIO_GCC_USB3_PHY_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_USB3_PHY_BCR_ADDR, m)
#define HWIO_GCC_USB3_PHY_BCR_OUT(v)      \
        out_dword(HWIO_GCC_USB3_PHY_BCR_ADDR,v)
#define HWIO_GCC_USB3_PHY_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_USB3_PHY_BCR_ADDR,m,v,HWIO_GCC_USB3_PHY_BCR_IN)
#define HWIO_GCC_USB3_PHY_BCR_BLK_ARES_BMSK                                                              0x1
#define HWIO_GCC_USB3_PHY_BCR_BLK_ARES_SHFT                                                              0x0

#define HWIO_GCC_USB3PHY_PHY_BCR_ADDR                                                             (GCC_CLK_CTL_REG_REG_BASE      + 0x0005e03c)
#define HWIO_GCC_USB3PHY_PHY_BCR_RMSK                                                                    0x1
#define HWIO_GCC_USB3PHY_PHY_BCR_IN          \
        in_dword_masked(HWIO_GCC_USB3PHY_PHY_BCR_ADDR, HWIO_GCC_USB3PHY_PHY_BCR_RMSK)
#define HWIO_GCC_USB3PHY_PHY_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_USB3PHY_PHY_BCR_ADDR, m)
#define HWIO_GCC_USB3PHY_PHY_BCR_OUT(v)      \
        out_dword(HWIO_GCC_USB3PHY_PHY_BCR_ADDR,v)
#define HWIO_GCC_USB3PHY_PHY_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_USB3PHY_PHY_BCR_ADDR,m,v,HWIO_GCC_USB3PHY_PHY_BCR_IN)
#define HWIO_GCC_USB3PHY_PHY_BCR_BLK_ARES_BMSK                                                           0x1
#define HWIO_GCC_USB3PHY_PHY_BCR_BLK_ARES_SHFT                                                           0x0

#define HWIO_GCC_USB3_PIPE_CBCR_ADDR                                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x0005e040)
#define HWIO_GCC_USB3_PIPE_CBCR_RMSK                                                              0x80000001
#define HWIO_GCC_USB3_PIPE_CBCR_IN          \
        in_dword_masked(HWIO_GCC_USB3_PIPE_CBCR_ADDR, HWIO_GCC_USB3_PIPE_CBCR_RMSK)
#define HWIO_GCC_USB3_PIPE_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_USB3_PIPE_CBCR_ADDR, m)
#define HWIO_GCC_USB3_PIPE_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_USB3_PIPE_CBCR_ADDR,v)
#define HWIO_GCC_USB3_PIPE_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_USB3_PIPE_CBCR_ADDR,m,v,HWIO_GCC_USB3_PIPE_CBCR_IN)
#define HWIO_GCC_USB3_PIPE_CBCR_CLK_OFF_BMSK                                                      0x80000000
#define HWIO_GCC_USB3_PIPE_CBCR_CLK_OFF_SHFT                                                            0x1f
#define HWIO_GCC_USB3_PIPE_CBCR_CLK_ENABLE_BMSK                                                          0x1
#define HWIO_GCC_USB3_PIPE_CBCR_CLK_ENABLE_SHFT                                                          0x0

#define HWIO_GCC_USB3_AUX_CBCR_ADDR                                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x0005e044)
#define HWIO_GCC_USB3_AUX_CBCR_RMSK                                                               0x80000001
#define HWIO_GCC_USB3_AUX_CBCR_IN          \
        in_dword_masked(HWIO_GCC_USB3_AUX_CBCR_ADDR, HWIO_GCC_USB3_AUX_CBCR_RMSK)
#define HWIO_GCC_USB3_AUX_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_USB3_AUX_CBCR_ADDR, m)
#define HWIO_GCC_USB3_AUX_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_USB3_AUX_CBCR_ADDR,v)
#define HWIO_GCC_USB3_AUX_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_USB3_AUX_CBCR_ADDR,m,v,HWIO_GCC_USB3_AUX_CBCR_IN)
#define HWIO_GCC_USB3_AUX_CBCR_CLK_OFF_BMSK                                                       0x80000000
#define HWIO_GCC_USB3_AUX_CBCR_CLK_OFF_SHFT                                                             0x1f
#define HWIO_GCC_USB3_AUX_CBCR_CLK_ENABLE_BMSK                                                           0x1
#define HWIO_GCC_USB3_AUX_CBCR_CLK_ENABLE_SHFT                                                           0x0

#define HWIO_GCC_USB3_PIPE_CMD_RCGR_ADDR                                                          (GCC_CLK_CTL_REG_REG_BASE      + 0x0005e048)
#define HWIO_GCC_USB3_PIPE_CMD_RCGR_RMSK                                                          0x80000013
#define HWIO_GCC_USB3_PIPE_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_USB3_PIPE_CMD_RCGR_ADDR, HWIO_GCC_USB3_PIPE_CMD_RCGR_RMSK)
#define HWIO_GCC_USB3_PIPE_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_USB3_PIPE_CMD_RCGR_ADDR, m)
#define HWIO_GCC_USB3_PIPE_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_USB3_PIPE_CMD_RCGR_ADDR,v)
#define HWIO_GCC_USB3_PIPE_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_USB3_PIPE_CMD_RCGR_ADDR,m,v,HWIO_GCC_USB3_PIPE_CMD_RCGR_IN)
#define HWIO_GCC_USB3_PIPE_CMD_RCGR_ROOT_OFF_BMSK                                                 0x80000000
#define HWIO_GCC_USB3_PIPE_CMD_RCGR_ROOT_OFF_SHFT                                                       0x1f
#define HWIO_GCC_USB3_PIPE_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                                 0x10
#define HWIO_GCC_USB3_PIPE_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                                  0x4
#define HWIO_GCC_USB3_PIPE_CMD_RCGR_ROOT_EN_BMSK                                                         0x2
#define HWIO_GCC_USB3_PIPE_CMD_RCGR_ROOT_EN_SHFT                                                         0x1
#define HWIO_GCC_USB3_PIPE_CMD_RCGR_UPDATE_BMSK                                                          0x1
#define HWIO_GCC_USB3_PIPE_CMD_RCGR_UPDATE_SHFT                                                          0x0

#define HWIO_GCC_USB3_PIPE_CFG_RCGR_ADDR                                                          (GCC_CLK_CTL_REG_REG_BASE      + 0x0005e04c)
#define HWIO_GCC_USB3_PIPE_CFG_RCGR_RMSK                                                               0x71f
#define HWIO_GCC_USB3_PIPE_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_USB3_PIPE_CFG_RCGR_ADDR, HWIO_GCC_USB3_PIPE_CFG_RCGR_RMSK)
#define HWIO_GCC_USB3_PIPE_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_USB3_PIPE_CFG_RCGR_ADDR, m)
#define HWIO_GCC_USB3_PIPE_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_USB3_PIPE_CFG_RCGR_ADDR,v)
#define HWIO_GCC_USB3_PIPE_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_USB3_PIPE_CFG_RCGR_ADDR,m,v,HWIO_GCC_USB3_PIPE_CFG_RCGR_IN)
#define HWIO_GCC_USB3_PIPE_CFG_RCGR_SRC_SEL_BMSK                                                       0x700
#define HWIO_GCC_USB3_PIPE_CFG_RCGR_SRC_SEL_SHFT                                                         0x8
#define HWIO_GCC_USB3_PIPE_CFG_RCGR_SRC_DIV_BMSK                                                        0x1f
#define HWIO_GCC_USB3_PIPE_CFG_RCGR_SRC_DIV_SHFT                                                         0x0

#define HWIO_GCC_USB3_AUX_CMD_RCGR_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x0005e05c)
#define HWIO_GCC_USB3_AUX_CMD_RCGR_RMSK                                                           0x800000f3
#define HWIO_GCC_USB3_AUX_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_USB3_AUX_CMD_RCGR_ADDR, HWIO_GCC_USB3_AUX_CMD_RCGR_RMSK)
#define HWIO_GCC_USB3_AUX_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_USB3_AUX_CMD_RCGR_ADDR, m)
#define HWIO_GCC_USB3_AUX_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_USB3_AUX_CMD_RCGR_ADDR,v)
#define HWIO_GCC_USB3_AUX_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_USB3_AUX_CMD_RCGR_ADDR,m,v,HWIO_GCC_USB3_AUX_CMD_RCGR_IN)
#define HWIO_GCC_USB3_AUX_CMD_RCGR_ROOT_OFF_BMSK                                                  0x80000000
#define HWIO_GCC_USB3_AUX_CMD_RCGR_ROOT_OFF_SHFT                                                        0x1f
#define HWIO_GCC_USB3_AUX_CMD_RCGR_DIRTY_D_BMSK                                                         0x80
#define HWIO_GCC_USB3_AUX_CMD_RCGR_DIRTY_D_SHFT                                                          0x7
#define HWIO_GCC_USB3_AUX_CMD_RCGR_DIRTY_M_BMSK                                                         0x40
#define HWIO_GCC_USB3_AUX_CMD_RCGR_DIRTY_M_SHFT                                                          0x6
#define HWIO_GCC_USB3_AUX_CMD_RCGR_DIRTY_N_BMSK                                                         0x20
#define HWIO_GCC_USB3_AUX_CMD_RCGR_DIRTY_N_SHFT                                                          0x5
#define HWIO_GCC_USB3_AUX_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                                  0x10
#define HWIO_GCC_USB3_AUX_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                                   0x4
#define HWIO_GCC_USB3_AUX_CMD_RCGR_ROOT_EN_BMSK                                                          0x2
#define HWIO_GCC_USB3_AUX_CMD_RCGR_ROOT_EN_SHFT                                                          0x1
#define HWIO_GCC_USB3_AUX_CMD_RCGR_UPDATE_BMSK                                                           0x1
#define HWIO_GCC_USB3_AUX_CMD_RCGR_UPDATE_SHFT                                                           0x0

#define HWIO_GCC_USB3_AUX_CFG_RCGR_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x0005e060)
#define HWIO_GCC_USB3_AUX_CFG_RCGR_RMSK                                                               0x371f
#define HWIO_GCC_USB3_AUX_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_USB3_AUX_CFG_RCGR_ADDR, HWIO_GCC_USB3_AUX_CFG_RCGR_RMSK)
#define HWIO_GCC_USB3_AUX_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_USB3_AUX_CFG_RCGR_ADDR, m)
#define HWIO_GCC_USB3_AUX_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_USB3_AUX_CFG_RCGR_ADDR,v)
#define HWIO_GCC_USB3_AUX_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_USB3_AUX_CFG_RCGR_ADDR,m,v,HWIO_GCC_USB3_AUX_CFG_RCGR_IN)
#define HWIO_GCC_USB3_AUX_CFG_RCGR_MODE_BMSK                                                          0x3000
#define HWIO_GCC_USB3_AUX_CFG_RCGR_MODE_SHFT                                                             0xc
#define HWIO_GCC_USB3_AUX_CFG_RCGR_SRC_SEL_BMSK                                                        0x700
#define HWIO_GCC_USB3_AUX_CFG_RCGR_SRC_SEL_SHFT                                                          0x8
#define HWIO_GCC_USB3_AUX_CFG_RCGR_SRC_DIV_BMSK                                                         0x1f
#define HWIO_GCC_USB3_AUX_CFG_RCGR_SRC_DIV_SHFT                                                          0x0

#define HWIO_GCC_USB3_AUX_M_ADDR                                                                  (GCC_CLK_CTL_REG_REG_BASE      + 0x0005e064)
#define HWIO_GCC_USB3_AUX_M_RMSK                                                                      0xffff
#define HWIO_GCC_USB3_AUX_M_IN          \
        in_dword_masked(HWIO_GCC_USB3_AUX_M_ADDR, HWIO_GCC_USB3_AUX_M_RMSK)
#define HWIO_GCC_USB3_AUX_M_INM(m)      \
        in_dword_masked(HWIO_GCC_USB3_AUX_M_ADDR, m)
#define HWIO_GCC_USB3_AUX_M_OUT(v)      \
        out_dword(HWIO_GCC_USB3_AUX_M_ADDR,v)
#define HWIO_GCC_USB3_AUX_M_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_USB3_AUX_M_ADDR,m,v,HWIO_GCC_USB3_AUX_M_IN)
#define HWIO_GCC_USB3_AUX_M_M_BMSK                                                                    0xffff
#define HWIO_GCC_USB3_AUX_M_M_SHFT                                                                       0x0

#define HWIO_GCC_USB3_AUX_N_ADDR                                                                  (GCC_CLK_CTL_REG_REG_BASE      + 0x0005e068)
#define HWIO_GCC_USB3_AUX_N_RMSK                                                                      0xffff
#define HWIO_GCC_USB3_AUX_N_IN          \
        in_dword_masked(HWIO_GCC_USB3_AUX_N_ADDR, HWIO_GCC_USB3_AUX_N_RMSK)
#define HWIO_GCC_USB3_AUX_N_INM(m)      \
        in_dword_masked(HWIO_GCC_USB3_AUX_N_ADDR, m)
#define HWIO_GCC_USB3_AUX_N_OUT(v)      \
        out_dword(HWIO_GCC_USB3_AUX_N_ADDR,v)
#define HWIO_GCC_USB3_AUX_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_USB3_AUX_N_ADDR,m,v,HWIO_GCC_USB3_AUX_N_IN)
#define HWIO_GCC_USB3_AUX_N_NOT_N_MINUS_M_BMSK                                                        0xffff
#define HWIO_GCC_USB3_AUX_N_NOT_N_MINUS_M_SHFT                                                           0x0

#define HWIO_GCC_USB3_AUX_D_ADDR                                                                  (GCC_CLK_CTL_REG_REG_BASE      + 0x0005e06c)
#define HWIO_GCC_USB3_AUX_D_RMSK                                                                      0xffff
#define HWIO_GCC_USB3_AUX_D_IN          \
        in_dword_masked(HWIO_GCC_USB3_AUX_D_ADDR, HWIO_GCC_USB3_AUX_D_RMSK)
#define HWIO_GCC_USB3_AUX_D_INM(m)      \
        in_dword_masked(HWIO_GCC_USB3_AUX_D_ADDR, m)
#define HWIO_GCC_USB3_AUX_D_OUT(v)      \
        out_dword(HWIO_GCC_USB3_AUX_D_ADDR,v)
#define HWIO_GCC_USB3_AUX_D_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_USB3_AUX_D_ADDR,m,v,HWIO_GCC_USB3_AUX_D_IN)
#define HWIO_GCC_USB3_AUX_D_NOT_2D_BMSK                                                               0xffff
#define HWIO_GCC_USB3_AUX_D_NOT_2D_SHFT                                                                  0x0

#define HWIO_GCC_USB_30_GDSCR_ADDR                                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x0005e078)
#define HWIO_GCC_USB_30_GDSCR_RMSK                                                                0xf8ffffff
#define HWIO_GCC_USB_30_GDSCR_IN          \
        in_dword_masked(HWIO_GCC_USB_30_GDSCR_ADDR, HWIO_GCC_USB_30_GDSCR_RMSK)
#define HWIO_GCC_USB_30_GDSCR_INM(m)      \
        in_dword_masked(HWIO_GCC_USB_30_GDSCR_ADDR, m)
#define HWIO_GCC_USB_30_GDSCR_OUT(v)      \
        out_dword(HWIO_GCC_USB_30_GDSCR_ADDR,v)
#define HWIO_GCC_USB_30_GDSCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_USB_30_GDSCR_ADDR,m,v,HWIO_GCC_USB_30_GDSCR_IN)
#define HWIO_GCC_USB_30_GDSCR_PWR_ON_BMSK                                                         0x80000000
#define HWIO_GCC_USB_30_GDSCR_PWR_ON_SHFT                                                               0x1f
#define HWIO_GCC_USB_30_GDSCR_GDSC_STATE_BMSK                                                     0x78000000
#define HWIO_GCC_USB_30_GDSCR_GDSC_STATE_SHFT                                                           0x1b
#define HWIO_GCC_USB_30_GDSCR_EN_REST_WAIT_BMSK                                                     0xf00000
#define HWIO_GCC_USB_30_GDSCR_EN_REST_WAIT_SHFT                                                         0x14
#define HWIO_GCC_USB_30_GDSCR_EN_FEW_WAIT_BMSK                                                       0xf0000
#define HWIO_GCC_USB_30_GDSCR_EN_FEW_WAIT_SHFT                                                          0x10
#define HWIO_GCC_USB_30_GDSCR_CLK_DIS_WAIT_BMSK                                                       0xf000
#define HWIO_GCC_USB_30_GDSCR_CLK_DIS_WAIT_SHFT                                                          0xc
#define HWIO_GCC_USB_30_GDSCR_RETAIN_FF_ENABLE_BMSK                                                    0x800
#define HWIO_GCC_USB_30_GDSCR_RETAIN_FF_ENABLE_SHFT                                                      0xb
#define HWIO_GCC_USB_30_GDSCR_RESTORE_BMSK                                                             0x400
#define HWIO_GCC_USB_30_GDSCR_RESTORE_SHFT                                                               0xa
#define HWIO_GCC_USB_30_GDSCR_SAVE_BMSK                                                                0x200
#define HWIO_GCC_USB_30_GDSCR_SAVE_SHFT                                                                  0x9
#define HWIO_GCC_USB_30_GDSCR_RETAIN_BMSK                                                              0x100
#define HWIO_GCC_USB_30_GDSCR_RETAIN_SHFT                                                                0x8
#define HWIO_GCC_USB_30_GDSCR_EN_REST_BMSK                                                              0x80
#define HWIO_GCC_USB_30_GDSCR_EN_REST_SHFT                                                               0x7
#define HWIO_GCC_USB_30_GDSCR_EN_FEW_BMSK                                                               0x40
#define HWIO_GCC_USB_30_GDSCR_EN_FEW_SHFT                                                                0x6
#define HWIO_GCC_USB_30_GDSCR_CLAMP_IO_BMSK                                                             0x20
#define HWIO_GCC_USB_30_GDSCR_CLAMP_IO_SHFT                                                              0x5
#define HWIO_GCC_USB_30_GDSCR_CLK_DISABLE_BMSK                                                          0x10
#define HWIO_GCC_USB_30_GDSCR_CLK_DISABLE_SHFT                                                           0x4
#define HWIO_GCC_USB_30_GDSCR_PD_ARES_BMSK                                                               0x8
#define HWIO_GCC_USB_30_GDSCR_PD_ARES_SHFT                                                               0x3
#define HWIO_GCC_USB_30_GDSCR_SW_OVERRIDE_BMSK                                                           0x4
#define HWIO_GCC_USB_30_GDSCR_SW_OVERRIDE_SHFT                                                           0x2
#define HWIO_GCC_USB_30_GDSCR_HW_CONTROL_BMSK                                                            0x2
#define HWIO_GCC_USB_30_GDSCR_HW_CONTROL_SHFT                                                            0x1
#define HWIO_GCC_USB_30_GDSCR_SW_COLLAPSE_BMSK                                                           0x1
#define HWIO_GCC_USB_30_GDSCR_SW_COLLAPSE_SHFT                                                           0x0

#define HWIO_GCC_IPA_TBU_CBCR_ADDR                                                                (GCC_CLK_CTL_REG_REG_BASE      + 0x0001205c)
#define HWIO_GCC_IPA_TBU_CBCR_RMSK                                                                0x80007ff0
#define HWIO_GCC_IPA_TBU_CBCR_IN          \
        in_dword_masked(HWIO_GCC_IPA_TBU_CBCR_ADDR, HWIO_GCC_IPA_TBU_CBCR_RMSK)
#define HWIO_GCC_IPA_TBU_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_IPA_TBU_CBCR_ADDR, m)
#define HWIO_GCC_IPA_TBU_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_IPA_TBU_CBCR_ADDR,v)
#define HWIO_GCC_IPA_TBU_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_IPA_TBU_CBCR_ADDR,m,v,HWIO_GCC_IPA_TBU_CBCR_IN)
#define HWIO_GCC_IPA_TBU_CBCR_CLK_OFF_BMSK                                                        0x80000000
#define HWIO_GCC_IPA_TBU_CBCR_CLK_OFF_SHFT                                                              0x1f
#define HWIO_GCC_IPA_TBU_CBCR_FORCE_MEM_CORE_ON_BMSK                                                  0x4000
#define HWIO_GCC_IPA_TBU_CBCR_FORCE_MEM_CORE_ON_SHFT                                                     0xe
#define HWIO_GCC_IPA_TBU_CBCR_FORCE_MEM_PERIPH_ON_BMSK                                                0x2000
#define HWIO_GCC_IPA_TBU_CBCR_FORCE_MEM_PERIPH_ON_SHFT                                                   0xd
#define HWIO_GCC_IPA_TBU_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                                               0x1000
#define HWIO_GCC_IPA_TBU_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                                                  0xc
#define HWIO_GCC_IPA_TBU_CBCR_WAKEUP_BMSK                                                              0xf00
#define HWIO_GCC_IPA_TBU_CBCR_WAKEUP_SHFT                                                                0x8
#define HWIO_GCC_IPA_TBU_CBCR_SLEEP_BMSK                                                                0xf0
#define HWIO_GCC_IPA_TBU_CBCR_SLEEP_SHFT                                                                 0x4

#define HWIO_GCC_USB3_AXI_TBU_CBCR_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x00012060)
#define HWIO_GCC_USB3_AXI_TBU_CBCR_RMSK                                                           0x80000000
#define HWIO_GCC_USB3_AXI_TBU_CBCR_IN          \
        in_dword_masked(HWIO_GCC_USB3_AXI_TBU_CBCR_ADDR, HWIO_GCC_USB3_AXI_TBU_CBCR_RMSK)
#define HWIO_GCC_USB3_AXI_TBU_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_USB3_AXI_TBU_CBCR_ADDR, m)
#define HWIO_GCC_USB3_AXI_TBU_CBCR_CLK_OFF_BMSK                                                   0x80000000
#define HWIO_GCC_USB3_AXI_TBU_CBCR_CLK_OFF_SHFT                                                         0x1f

#define HWIO_GCC_PCIE_AXI_TBU_CBCR_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x00012064)
#define HWIO_GCC_PCIE_AXI_TBU_CBCR_RMSK                                                           0x80000000
#define HWIO_GCC_PCIE_AXI_TBU_CBCR_IN          \
        in_dword_masked(HWIO_GCC_PCIE_AXI_TBU_CBCR_ADDR, HWIO_GCC_PCIE_AXI_TBU_CBCR_RMSK)
#define HWIO_GCC_PCIE_AXI_TBU_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PCIE_AXI_TBU_CBCR_ADDR, m)
#define HWIO_GCC_PCIE_AXI_TBU_CBCR_CLK_OFF_BMSK                                                   0x80000000
#define HWIO_GCC_PCIE_AXI_TBU_CBCR_CLK_OFF_SHFT                                                         0x1f

#define HWIO_GCC_MSS_TBU_MCDMA_AXI_CBCR_ADDR                                                      (GCC_CLK_CTL_REG_REG_BASE      + 0x00012068)
#define HWIO_GCC_MSS_TBU_MCDMA_AXI_CBCR_RMSK                                                      0x80000000
#define HWIO_GCC_MSS_TBU_MCDMA_AXI_CBCR_IN          \
        in_dword_masked(HWIO_GCC_MSS_TBU_MCDMA_AXI_CBCR_ADDR, HWIO_GCC_MSS_TBU_MCDMA_AXI_CBCR_RMSK)
#define HWIO_GCC_MSS_TBU_MCDMA_AXI_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_MSS_TBU_MCDMA_AXI_CBCR_ADDR, m)
#define HWIO_GCC_MSS_TBU_MCDMA_AXI_CBCR_CLK_OFF_BMSK                                              0x80000000
#define HWIO_GCC_MSS_TBU_MCDMA_AXI_CBCR_CLK_OFF_SHFT                                                    0x1f

#define HWIO_GCC_IPA_TBU_BCR_ADDR                                                                 (GCC_CLK_CTL_REG_REG_BASE      + 0x0006e000)
#define HWIO_GCC_IPA_TBU_BCR_RMSK                                                                        0x1
#define HWIO_GCC_IPA_TBU_BCR_IN          \
        in_dword_masked(HWIO_GCC_IPA_TBU_BCR_ADDR, HWIO_GCC_IPA_TBU_BCR_RMSK)
#define HWIO_GCC_IPA_TBU_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_IPA_TBU_BCR_ADDR, m)
#define HWIO_GCC_IPA_TBU_BCR_OUT(v)      \
        out_dword(HWIO_GCC_IPA_TBU_BCR_ADDR,v)
#define HWIO_GCC_IPA_TBU_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_IPA_TBU_BCR_ADDR,m,v,HWIO_GCC_IPA_TBU_BCR_IN)
#define HWIO_GCC_IPA_TBU_BCR_BLK_ARES_BMSK                                                               0x1
#define HWIO_GCC_IPA_TBU_BCR_BLK_ARES_SHFT                                                               0x0

#define HWIO_GCC_USB3_AXI_TBU_BCR_ADDR                                                            (GCC_CLK_CTL_REG_REG_BASE      + 0x0006f000)
#define HWIO_GCC_USB3_AXI_TBU_BCR_RMSK                                                                   0x1
#define HWIO_GCC_USB3_AXI_TBU_BCR_IN          \
        in_dword_masked(HWIO_GCC_USB3_AXI_TBU_BCR_ADDR, HWIO_GCC_USB3_AXI_TBU_BCR_RMSK)
#define HWIO_GCC_USB3_AXI_TBU_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_USB3_AXI_TBU_BCR_ADDR, m)
#define HWIO_GCC_USB3_AXI_TBU_BCR_OUT(v)      \
        out_dword(HWIO_GCC_USB3_AXI_TBU_BCR_ADDR,v)
#define HWIO_GCC_USB3_AXI_TBU_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_USB3_AXI_TBU_BCR_ADDR,m,v,HWIO_GCC_USB3_AXI_TBU_BCR_IN)
#define HWIO_GCC_USB3_AXI_TBU_BCR_BLK_ARES_BMSK                                                          0x1
#define HWIO_GCC_USB3_AXI_TBU_BCR_BLK_ARES_SHFT                                                          0x0

#define HWIO_GCC_PCIE_AXI_TBU_BCR_ADDR                                                            (GCC_CLK_CTL_REG_REG_BASE      + 0x0007d000)
#define HWIO_GCC_PCIE_AXI_TBU_BCR_RMSK                                                                   0x1
#define HWIO_GCC_PCIE_AXI_TBU_BCR_IN          \
        in_dword_masked(HWIO_GCC_PCIE_AXI_TBU_BCR_ADDR, HWIO_GCC_PCIE_AXI_TBU_BCR_RMSK)
#define HWIO_GCC_PCIE_AXI_TBU_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_PCIE_AXI_TBU_BCR_ADDR, m)
#define HWIO_GCC_PCIE_AXI_TBU_BCR_OUT(v)      \
        out_dword(HWIO_GCC_PCIE_AXI_TBU_BCR_ADDR,v)
#define HWIO_GCC_PCIE_AXI_TBU_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_PCIE_AXI_TBU_BCR_ADDR,m,v,HWIO_GCC_PCIE_AXI_TBU_BCR_IN)
#define HWIO_GCC_PCIE_AXI_TBU_BCR_BLK_ARES_BMSK                                                          0x1
#define HWIO_GCC_PCIE_AXI_TBU_BCR_BLK_ARES_SHFT                                                          0x0

#define HWIO_GCC_MSS_TBU_MCDMA_AXI_BCR_ADDR                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x0007b000)
#define HWIO_GCC_MSS_TBU_MCDMA_AXI_BCR_RMSK                                                              0x1
#define HWIO_GCC_MSS_TBU_MCDMA_AXI_BCR_IN          \
        in_dword_masked(HWIO_GCC_MSS_TBU_MCDMA_AXI_BCR_ADDR, HWIO_GCC_MSS_TBU_MCDMA_AXI_BCR_RMSK)
#define HWIO_GCC_MSS_TBU_MCDMA_AXI_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_MSS_TBU_MCDMA_AXI_BCR_ADDR, m)
#define HWIO_GCC_MSS_TBU_MCDMA_AXI_BCR_OUT(v)      \
        out_dword(HWIO_GCC_MSS_TBU_MCDMA_AXI_BCR_ADDR,v)
#define HWIO_GCC_MSS_TBU_MCDMA_AXI_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_MSS_TBU_MCDMA_AXI_BCR_ADDR,m,v,HWIO_GCC_MSS_TBU_MCDMA_AXI_BCR_IN)
#define HWIO_GCC_MSS_TBU_MCDMA_AXI_BCR_BLK_ARES_BMSK                                                     0x1
#define HWIO_GCC_MSS_TBU_MCDMA_AXI_BCR_BLK_ARES_SHFT                                                     0x0

#define HWIO_GCC_MSS_MCDMA_BIMC_CMD_RCGR_ADDR                                                     (GCC_CLK_CTL_REG_REG_BASE      + 0x0004900c)
#define HWIO_GCC_MSS_MCDMA_BIMC_CMD_RCGR_RMSK                                                     0x80000013
#define HWIO_GCC_MSS_MCDMA_BIMC_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_MSS_MCDMA_BIMC_CMD_RCGR_ADDR, HWIO_GCC_MSS_MCDMA_BIMC_CMD_RCGR_RMSK)
#define HWIO_GCC_MSS_MCDMA_BIMC_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_MSS_MCDMA_BIMC_CMD_RCGR_ADDR, m)
#define HWIO_GCC_MSS_MCDMA_BIMC_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_MSS_MCDMA_BIMC_CMD_RCGR_ADDR,v)
#define HWIO_GCC_MSS_MCDMA_BIMC_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_MSS_MCDMA_BIMC_CMD_RCGR_ADDR,m,v,HWIO_GCC_MSS_MCDMA_BIMC_CMD_RCGR_IN)
#define HWIO_GCC_MSS_MCDMA_BIMC_CMD_RCGR_ROOT_OFF_BMSK                                            0x80000000
#define HWIO_GCC_MSS_MCDMA_BIMC_CMD_RCGR_ROOT_OFF_SHFT                                                  0x1f
#define HWIO_GCC_MSS_MCDMA_BIMC_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                            0x10
#define HWIO_GCC_MSS_MCDMA_BIMC_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                             0x4
#define HWIO_GCC_MSS_MCDMA_BIMC_CMD_RCGR_ROOT_EN_BMSK                                                    0x2
#define HWIO_GCC_MSS_MCDMA_BIMC_CMD_RCGR_ROOT_EN_SHFT                                                    0x1
#define HWIO_GCC_MSS_MCDMA_BIMC_CMD_RCGR_UPDATE_BMSK                                                     0x1
#define HWIO_GCC_MSS_MCDMA_BIMC_CMD_RCGR_UPDATE_SHFT                                                     0x0

#define HWIO_GCC_MSS_MCDMA_BIMC_CFG_RCGR_ADDR                                                     (GCC_CLK_CTL_REG_REG_BASE      + 0x00049010)
#define HWIO_GCC_MSS_MCDMA_BIMC_CFG_RCGR_RMSK                                                          0x71f
#define HWIO_GCC_MSS_MCDMA_BIMC_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_MSS_MCDMA_BIMC_CFG_RCGR_ADDR, HWIO_GCC_MSS_MCDMA_BIMC_CFG_RCGR_RMSK)
#define HWIO_GCC_MSS_MCDMA_BIMC_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_MSS_MCDMA_BIMC_CFG_RCGR_ADDR, m)
#define HWIO_GCC_MSS_MCDMA_BIMC_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_MSS_MCDMA_BIMC_CFG_RCGR_ADDR,v)
#define HWIO_GCC_MSS_MCDMA_BIMC_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_MSS_MCDMA_BIMC_CFG_RCGR_ADDR,m,v,HWIO_GCC_MSS_MCDMA_BIMC_CFG_RCGR_IN)
#define HWIO_GCC_MSS_MCDMA_BIMC_CFG_RCGR_SRC_SEL_BMSK                                                  0x700
#define HWIO_GCC_MSS_MCDMA_BIMC_CFG_RCGR_SRC_SEL_SHFT                                                    0x8
#define HWIO_GCC_MSS_MCDMA_BIMC_CFG_RCGR_SRC_DIV_BMSK                                                   0x1f
#define HWIO_GCC_MSS_MCDMA_BIMC_CFG_RCGR_SRC_DIV_SHFT                                                    0x0

#define HWIO_GCC_BIMC_MCDMA_AXI_CBCR_ADDR                                                         (GCC_CLK_CTL_REG_REG_BASE      + 0x00049024)
#define HWIO_GCC_BIMC_MCDMA_AXI_CBCR_RMSK                                                         0x80000001
#define HWIO_GCC_BIMC_MCDMA_AXI_CBCR_IN          \
        in_dword_masked(HWIO_GCC_BIMC_MCDMA_AXI_CBCR_ADDR, HWIO_GCC_BIMC_MCDMA_AXI_CBCR_RMSK)
#define HWIO_GCC_BIMC_MCDMA_AXI_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_BIMC_MCDMA_AXI_CBCR_ADDR, m)
#define HWIO_GCC_BIMC_MCDMA_AXI_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_BIMC_MCDMA_AXI_CBCR_ADDR,v)
#define HWIO_GCC_BIMC_MCDMA_AXI_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_BIMC_MCDMA_AXI_CBCR_ADDR,m,v,HWIO_GCC_BIMC_MCDMA_AXI_CBCR_IN)
#define HWIO_GCC_BIMC_MCDMA_AXI_CBCR_CLK_OFF_BMSK                                                 0x80000000
#define HWIO_GCC_BIMC_MCDMA_AXI_CBCR_CLK_OFF_SHFT                                                       0x1f
#define HWIO_GCC_BIMC_MCDMA_AXI_CBCR_CLK_ENABLE_BMSK                                                     0x1
#define HWIO_GCC_BIMC_MCDMA_AXI_CBCR_CLK_ENABLE_SHFT                                                     0x0

#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_CMD_RCGR_ADDR                                             (GCC_CLK_CTL_REG_REG_BASE      + 0x0001c0d8)
#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_CMD_RCGR_RMSK                                             0x800000f3
#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_CMD_RCGR_ADDR, HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_CMD_RCGR_RMSK)
#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_CMD_RCGR_ADDR, m)
#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_CMD_RCGR_ADDR,v)
#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_CMD_RCGR_ADDR,m,v,HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_CMD_RCGR_IN)
#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_CMD_RCGR_ROOT_OFF_BMSK                                    0x80000000
#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_CMD_RCGR_ROOT_OFF_SHFT                                          0x1f
#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_CMD_RCGR_DIRTY_D_BMSK                                           0x80
#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_CMD_RCGR_DIRTY_D_SHFT                                            0x7
#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_CMD_RCGR_DIRTY_N_BMSK                                           0x40
#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_CMD_RCGR_DIRTY_N_SHFT                                            0x6
#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_CMD_RCGR_DIRTY_M_BMSK                                           0x20
#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_CMD_RCGR_DIRTY_M_SHFT                                            0x5
#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                    0x10
#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                     0x4
#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_CMD_RCGR_ROOT_EN_BMSK                                            0x2
#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_CMD_RCGR_ROOT_EN_SHFT                                            0x1
#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_CMD_RCGR_UPDATE_BMSK                                             0x1
#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_CMD_RCGR_UPDATE_SHFT                                             0x0

#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_CFG_RCGR_ADDR                                             (GCC_CLK_CTL_REG_REG_BASE      + 0x0001c0dc)
#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_CFG_RCGR_RMSK                                                 0x371f
#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_CFG_RCGR_ADDR, HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_CFG_RCGR_RMSK)
#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_CFG_RCGR_ADDR, m)
#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_CFG_RCGR_ADDR,v)
#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_CFG_RCGR_ADDR,m,v,HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_CFG_RCGR_IN)
#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_CFG_RCGR_MODE_BMSK                                            0x3000
#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_CFG_RCGR_MODE_SHFT                                               0xc
#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_CFG_RCGR_SRC_SEL_BMSK                                          0x700
#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_CFG_RCGR_SRC_SEL_SHFT                                            0x8
#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_CFG_RCGR_SRC_DIV_BMSK                                           0x1f
#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_CFG_RCGR_SRC_DIV_SHFT                                            0x0

#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_M_ADDR                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x0001c0e0)
#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_M_RMSK                                                          0xff
#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_M_IN          \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_M_ADDR, HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_M_RMSK)
#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_M_INM(m)      \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_M_ADDR, m)
#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_M_OUT(v)      \
        out_dword(HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_M_ADDR,v)
#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_M_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_M_ADDR,m,v,HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_M_IN)
#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_M_M_BMSK                                                        0xff
#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_M_M_SHFT                                                         0x0

#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_N_ADDR                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x0001c0e4)
#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_N_RMSK                                                          0xff
#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_N_IN          \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_N_ADDR, HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_N_RMSK)
#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_N_INM(m)      \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_N_ADDR, m)
#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_N_OUT(v)      \
        out_dword(HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_N_ADDR,v)
#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_N_ADDR,m,v,HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_N_IN)
#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_N_NOT_N_MINUS_M_BMSK                                            0xff
#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_N_NOT_N_MINUS_M_SHFT                                             0x0

#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_D_ADDR                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x0001c0e8)
#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_D_RMSK                                                          0xff
#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_D_IN          \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_D_ADDR, HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_D_RMSK)
#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_D_INM(m)      \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_D_ADDR, m)
#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_D_OUT(v)      \
        out_dword(HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_D_ADDR,v)
#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_D_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_D_ADDR,m,v,HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_D_IN)
#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_D_NOT_2D_BMSK                                                   0xff
#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_D_NOT_2D_SHFT                                                    0x0

#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_CBCR_ADDR                                                 (GCC_CLK_CTL_REG_REG_BASE      + 0x0001c0ec)
#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_CBCR_RMSK                                                 0x80000001
#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_CBCR_IN          \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_CBCR_ADDR, HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_CBCR_RMSK)
#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_CBCR_ADDR, m)
#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_CBCR_ADDR,v)
#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_CBCR_ADDR,m,v,HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_CBCR_IN)
#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_CBCR_CLK_OFF_BMSK                                         0x80000000
#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_CBCR_CLK_OFF_SHFT                                               0x1f
#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_CBCR_CLK_ENABLE_BMSK                                             0x1
#define HWIO_GCC_ULTAUDIO_LPAIF_EXT_I2S_CBCR_CLK_ENABLE_SHFT                                             0x0

#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_CMD_RCGR_ADDR                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x0001c0c0)
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_CMD_RCGR_RMSK                                        0x800000f3
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_CMD_RCGR_ADDR, HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_CMD_RCGR_RMSK)
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_CMD_RCGR_ADDR, m)
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_CMD_RCGR_ADDR,v)
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_CMD_RCGR_ADDR,m,v,HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_CMD_RCGR_IN)
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_CMD_RCGR_ROOT_OFF_BMSK                               0x80000000
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_CMD_RCGR_ROOT_OFF_SHFT                                     0x1f
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_CMD_RCGR_DIRTY_D_BMSK                                      0x80
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_CMD_RCGR_DIRTY_D_SHFT                                       0x7
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_CMD_RCGR_DIRTY_N_BMSK                                      0x40
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_CMD_RCGR_DIRTY_N_SHFT                                       0x6
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_CMD_RCGR_DIRTY_M_BMSK                                      0x20
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_CMD_RCGR_DIRTY_M_SHFT                                       0x5
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                               0x10
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                0x4
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_CMD_RCGR_ROOT_EN_BMSK                                       0x2
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_CMD_RCGR_ROOT_EN_SHFT                                       0x1
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_CMD_RCGR_UPDATE_BMSK                                        0x1
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_CMD_RCGR_UPDATE_SHFT                                        0x0

#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_CFG_RCGR_ADDR                                        (GCC_CLK_CTL_REG_REG_BASE      + 0x0001c0c4)
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_CFG_RCGR_RMSK                                            0x371f
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_CFG_RCGR_ADDR, HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_CFG_RCGR_RMSK)
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_CFG_RCGR_ADDR, m)
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_CFG_RCGR_ADDR,v)
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_CFG_RCGR_ADDR,m,v,HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_CFG_RCGR_IN)
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_CFG_RCGR_MODE_BMSK                                       0x3000
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_CFG_RCGR_MODE_SHFT                                          0xc
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_CFG_RCGR_SRC_SEL_BMSK                                     0x700
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_CFG_RCGR_SRC_SEL_SHFT                                       0x8
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_CFG_RCGR_SRC_DIV_BMSK                                      0x1f
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_CFG_RCGR_SRC_DIV_SHFT                                       0x0

#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_M_ADDR                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x0001c0c8)
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_M_RMSK                                                     0xff
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_M_IN          \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_M_ADDR, HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_M_RMSK)
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_M_INM(m)      \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_M_ADDR, m)
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_M_OUT(v)      \
        out_dword(HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_M_ADDR,v)
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_M_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_M_ADDR,m,v,HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_M_IN)
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_M_M_BMSK                                                   0xff
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_M_M_SHFT                                                    0x0

#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_N_ADDR                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x0001c0cc)
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_N_RMSK                                                     0xff
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_N_IN          \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_N_ADDR, HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_N_RMSK)
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_N_INM(m)      \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_N_ADDR, m)
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_N_OUT(v)      \
        out_dword(HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_N_ADDR,v)
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_N_ADDR,m,v,HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_N_IN)
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_N_NOT_N_MINUS_M_BMSK                                       0xff
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_N_NOT_N_MINUS_M_SHFT                                        0x0

#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_D_ADDR                                               (GCC_CLK_CTL_REG_REG_BASE      + 0x0001c0d0)
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_D_RMSK                                                     0xff
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_D_IN          \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_D_ADDR, HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_D_RMSK)
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_D_INM(m)      \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_D_ADDR, m)
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_D_OUT(v)      \
        out_dword(HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_D_ADDR,v)
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_D_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_D_ADDR,m,v,HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_D_IN)
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_D_NOT_2D_BMSK                                              0xff
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_D_NOT_2D_SHFT                                               0x0

#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_CBCR_ADDR                                            (GCC_CLK_CTL_REG_REG_BASE      + 0x0001c0d4)
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_CBCR_RMSK                                            0x80000001
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_CBCR_IN          \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_CBCR_ADDR, HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_CBCR_RMSK)
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_CBCR_ADDR, m)
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_CBCR_ADDR,v)
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_CBCR_ADDR,m,v,HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_CBCR_IN)
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_CBCR_CLK_OFF_BMSK                                    0x80000000
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_CBCR_CLK_OFF_SHFT                                          0x1f
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_CBCR_CLK_ENABLE_BMSK                                        0x1
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_CORE_CBCR_CLK_ENABLE_SHFT                                        0x0

#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_BAM_CBCR_ADDR                                             (GCC_CLK_CTL_REG_REG_BASE      + 0x0001c0bc)
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_BAM_CBCR_RMSK                                             0x80007ff1
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_BAM_CBCR_IN          \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_BAM_CBCR_ADDR, HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_BAM_CBCR_RMSK)
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_BAM_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_BAM_CBCR_ADDR, m)
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_BAM_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_BAM_CBCR_ADDR,v)
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_BAM_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_BAM_CBCR_ADDR,m,v,HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_BAM_CBCR_IN)
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_BAM_CBCR_CLK_OFF_BMSK                                     0x80000000
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_BAM_CBCR_CLK_OFF_SHFT                                           0x1f
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_BAM_CBCR_FORCE_MEM_CORE_ON_BMSK                               0x4000
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_BAM_CBCR_FORCE_MEM_CORE_ON_SHFT                                  0xe
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_BAM_CBCR_FORCE_MEM_PERIPH_ON_BMSK                             0x2000
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_BAM_CBCR_FORCE_MEM_PERIPH_ON_SHFT                                0xd
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_BAM_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                            0x1000
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_BAM_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                               0xc
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_BAM_CBCR_WAKEUP_BMSK                                           0xf00
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_BAM_CBCR_WAKEUP_SHFT                                             0x8
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_BAM_CBCR_SLEEP_BMSK                                             0xf0
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_BAM_CBCR_SLEEP_SHFT                                              0x4
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_BAM_CBCR_CLK_ENABLE_BMSK                                         0x1
#define HWIO_GCC_ULTAUDIO_LPAIF_SLIMBUS_BAM_CBCR_CLK_ENABLE_SHFT                                         0x0

#define HWIO_GCC_AUDIO_EXT_REF_CLK_SEL_ADDR                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x0001c0b8)
#define HWIO_GCC_AUDIO_EXT_REF_CLK_SEL_RMSK                                                              0x1
#define HWIO_GCC_AUDIO_EXT_REF_CLK_SEL_IN          \
        in_dword_masked(HWIO_GCC_AUDIO_EXT_REF_CLK_SEL_ADDR, HWIO_GCC_AUDIO_EXT_REF_CLK_SEL_RMSK)
#define HWIO_GCC_AUDIO_EXT_REF_CLK_SEL_INM(m)      \
        in_dword_masked(HWIO_GCC_AUDIO_EXT_REF_CLK_SEL_ADDR, m)
#define HWIO_GCC_AUDIO_EXT_REF_CLK_SEL_OUT(v)      \
        out_dword(HWIO_GCC_AUDIO_EXT_REF_CLK_SEL_ADDR,v)
#define HWIO_GCC_AUDIO_EXT_REF_CLK_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_AUDIO_EXT_REF_CLK_SEL_ADDR,m,v,HWIO_GCC_AUDIO_EXT_REF_CLK_SEL_IN)
#define HWIO_GCC_AUDIO_EXT_REF_CLK_SEL_EXT_CLK_ENABLE_BMSK                                               0x1
#define HWIO_GCC_AUDIO_EXT_REF_CLK_SEL_EXT_CLK_ENABLE_SHFT                                               0x0

#define HWIO_GCC_SMMU_TCU_BIMC_CMD_RCGR_ADDR                                                      (GCC_CLK_CTL_REG_REG_BASE      + 0x0001206c)
#define HWIO_GCC_SMMU_TCU_BIMC_CMD_RCGR_RMSK                                                      0x80000013
#define HWIO_GCC_SMMU_TCU_BIMC_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_SMMU_TCU_BIMC_CMD_RCGR_ADDR, HWIO_GCC_SMMU_TCU_BIMC_CMD_RCGR_RMSK)
#define HWIO_GCC_SMMU_TCU_BIMC_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_SMMU_TCU_BIMC_CMD_RCGR_ADDR, m)
#define HWIO_GCC_SMMU_TCU_BIMC_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_SMMU_TCU_BIMC_CMD_RCGR_ADDR,v)
#define HWIO_GCC_SMMU_TCU_BIMC_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SMMU_TCU_BIMC_CMD_RCGR_ADDR,m,v,HWIO_GCC_SMMU_TCU_BIMC_CMD_RCGR_IN)
#define HWIO_GCC_SMMU_TCU_BIMC_CMD_RCGR_ROOT_OFF_BMSK                                             0x80000000
#define HWIO_GCC_SMMU_TCU_BIMC_CMD_RCGR_ROOT_OFF_SHFT                                                   0x1f
#define HWIO_GCC_SMMU_TCU_BIMC_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                             0x10
#define HWIO_GCC_SMMU_TCU_BIMC_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                              0x4
#define HWIO_GCC_SMMU_TCU_BIMC_CMD_RCGR_ROOT_EN_BMSK                                                     0x2
#define HWIO_GCC_SMMU_TCU_BIMC_CMD_RCGR_ROOT_EN_SHFT                                                     0x1
#define HWIO_GCC_SMMU_TCU_BIMC_CMD_RCGR_UPDATE_BMSK                                                      0x1
#define HWIO_GCC_SMMU_TCU_BIMC_CMD_RCGR_UPDATE_SHFT                                                      0x0

#define HWIO_GCC_SMMU_TCU_BIMC_CFG_RCGR_ADDR                                                      (GCC_CLK_CTL_REG_REG_BASE      + 0x00012070)
#define HWIO_GCC_SMMU_TCU_BIMC_CFG_RCGR_RMSK                                                           0x71f
#define HWIO_GCC_SMMU_TCU_BIMC_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_SMMU_TCU_BIMC_CFG_RCGR_ADDR, HWIO_GCC_SMMU_TCU_BIMC_CFG_RCGR_RMSK)
#define HWIO_GCC_SMMU_TCU_BIMC_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_SMMU_TCU_BIMC_CFG_RCGR_ADDR, m)
#define HWIO_GCC_SMMU_TCU_BIMC_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_SMMU_TCU_BIMC_CFG_RCGR_ADDR,v)
#define HWIO_GCC_SMMU_TCU_BIMC_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SMMU_TCU_BIMC_CFG_RCGR_ADDR,m,v,HWIO_GCC_SMMU_TCU_BIMC_CFG_RCGR_IN)
#define HWIO_GCC_SMMU_TCU_BIMC_CFG_RCGR_SRC_SEL_BMSK                                                   0x700
#define HWIO_GCC_SMMU_TCU_BIMC_CFG_RCGR_SRC_SEL_SHFT                                                     0x8
#define HWIO_GCC_SMMU_TCU_BIMC_CFG_RCGR_SRC_DIV_BMSK                                                    0x1f
#define HWIO_GCC_SMMU_TCU_BIMC_CFG_RCGR_SRC_DIV_SHFT                                                     0x0

#define HWIO_GCC_SMMU_CATS_2X_CMD_RCGR_ADDR                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x0007c010)
#define HWIO_GCC_SMMU_CATS_2X_CMD_RCGR_RMSK                                                       0x80000013
#define HWIO_GCC_SMMU_CATS_2X_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_SMMU_CATS_2X_CMD_RCGR_ADDR, HWIO_GCC_SMMU_CATS_2X_CMD_RCGR_RMSK)
#define HWIO_GCC_SMMU_CATS_2X_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_SMMU_CATS_2X_CMD_RCGR_ADDR, m)
#define HWIO_GCC_SMMU_CATS_2X_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_SMMU_CATS_2X_CMD_RCGR_ADDR,v)
#define HWIO_GCC_SMMU_CATS_2X_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SMMU_CATS_2X_CMD_RCGR_ADDR,m,v,HWIO_GCC_SMMU_CATS_2X_CMD_RCGR_IN)
#define HWIO_GCC_SMMU_CATS_2X_CMD_RCGR_ROOT_OFF_BMSK                                              0x80000000
#define HWIO_GCC_SMMU_CATS_2X_CMD_RCGR_ROOT_OFF_SHFT                                                    0x1f
#define HWIO_GCC_SMMU_CATS_2X_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                              0x10
#define HWIO_GCC_SMMU_CATS_2X_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                               0x4
#define HWIO_GCC_SMMU_CATS_2X_CMD_RCGR_ROOT_EN_BMSK                                                      0x2
#define HWIO_GCC_SMMU_CATS_2X_CMD_RCGR_ROOT_EN_SHFT                                                      0x1
#define HWIO_GCC_SMMU_CATS_2X_CMD_RCGR_UPDATE_BMSK                                                       0x1
#define HWIO_GCC_SMMU_CATS_2X_CMD_RCGR_UPDATE_SHFT                                                       0x0

#define HWIO_GCC_SMMU_CATS_2X_CFG_RCGR_ADDR                                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x0007c014)
#define HWIO_GCC_SMMU_CATS_2X_CFG_RCGR_RMSK                                                            0x71f
#define HWIO_GCC_SMMU_CATS_2X_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_SMMU_CATS_2X_CFG_RCGR_ADDR, HWIO_GCC_SMMU_CATS_2X_CFG_RCGR_RMSK)
#define HWIO_GCC_SMMU_CATS_2X_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_SMMU_CATS_2X_CFG_RCGR_ADDR, m)
#define HWIO_GCC_SMMU_CATS_2X_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_SMMU_CATS_2X_CFG_RCGR_ADDR,v)
#define HWIO_GCC_SMMU_CATS_2X_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SMMU_CATS_2X_CFG_RCGR_ADDR,m,v,HWIO_GCC_SMMU_CATS_2X_CFG_RCGR_IN)
#define HWIO_GCC_SMMU_CATS_2X_CFG_RCGR_SRC_SEL_BMSK                                                    0x700
#define HWIO_GCC_SMMU_CATS_2X_CFG_RCGR_SRC_SEL_SHFT                                                      0x8
#define HWIO_GCC_SMMU_CATS_2X_CFG_RCGR_SRC_DIV_BMSK                                                     0x1f
#define HWIO_GCC_SMMU_CATS_2X_CFG_RCGR_SRC_DIV_SHFT                                                      0x0

#define HWIO_GCC_SYS_NOC_CATS_CBCR_ADDR                                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x0007c00c)
#define HWIO_GCC_SYS_NOC_CATS_CBCR_RMSK                                                           0x80000001
#define HWIO_GCC_SYS_NOC_CATS_CBCR_IN          \
        in_dword_masked(HWIO_GCC_SYS_NOC_CATS_CBCR_ADDR, HWIO_GCC_SYS_NOC_CATS_CBCR_RMSK)
#define HWIO_GCC_SYS_NOC_CATS_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_SYS_NOC_CATS_CBCR_ADDR, m)
#define HWIO_GCC_SYS_NOC_CATS_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_SYS_NOC_CATS_CBCR_ADDR,v)
#define HWIO_GCC_SYS_NOC_CATS_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_SYS_NOC_CATS_CBCR_ADDR,m,v,HWIO_GCC_SYS_NOC_CATS_CBCR_IN)
#define HWIO_GCC_SYS_NOC_CATS_CBCR_CLK_OFF_BMSK                                                   0x80000000
#define HWIO_GCC_SYS_NOC_CATS_CBCR_CLK_OFF_SHFT                                                         0x1f
#define HWIO_GCC_SYS_NOC_CATS_CBCR_CLK_ENABLE_BMSK                                                       0x1
#define HWIO_GCC_SYS_NOC_CATS_CBCR_CLK_ENABLE_SHFT                                                       0x0

#define HWIO_GCC_DDR_VOLTAGE_DROOP_DETECTOR_GPLL0_CBCR_ADDR                                       (GCC_CLK_CTL_REG_REG_BASE      + 0x0007a004)
#define HWIO_GCC_DDR_VOLTAGE_DROOP_DETECTOR_GPLL0_CBCR_RMSK                                       0x80000001
#define HWIO_GCC_DDR_VOLTAGE_DROOP_DETECTOR_GPLL0_CBCR_IN          \
        in_dword_masked(HWIO_GCC_DDR_VOLTAGE_DROOP_DETECTOR_GPLL0_CBCR_ADDR, HWIO_GCC_DDR_VOLTAGE_DROOP_DETECTOR_GPLL0_CBCR_RMSK)
#define HWIO_GCC_DDR_VOLTAGE_DROOP_DETECTOR_GPLL0_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_DDR_VOLTAGE_DROOP_DETECTOR_GPLL0_CBCR_ADDR, m)
#define HWIO_GCC_DDR_VOLTAGE_DROOP_DETECTOR_GPLL0_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_DDR_VOLTAGE_DROOP_DETECTOR_GPLL0_CBCR_ADDR,v)
#define HWIO_GCC_DDR_VOLTAGE_DROOP_DETECTOR_GPLL0_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_DDR_VOLTAGE_DROOP_DETECTOR_GPLL0_CBCR_ADDR,m,v,HWIO_GCC_DDR_VOLTAGE_DROOP_DETECTOR_GPLL0_CBCR_IN)
#define HWIO_GCC_DDR_VOLTAGE_DROOP_DETECTOR_GPLL0_CBCR_CLK_OFF_BMSK                               0x80000000
#define HWIO_GCC_DDR_VOLTAGE_DROOP_DETECTOR_GPLL0_CBCR_CLK_OFF_SHFT                                     0x1f
#define HWIO_GCC_DDR_VOLTAGE_DROOP_DETECTOR_GPLL0_CBCR_CLK_ENABLE_BMSK                                   0x1
#define HWIO_GCC_DDR_VOLTAGE_DROOP_DETECTOR_GPLL0_CBCR_CLK_ENABLE_SHFT                                   0x0

#define HWIO_GCC_VOLTAGE_DROOP_DETECTOR_BCR_ADDR                                                  (GCC_CLK_CTL_REG_REG_BASE      + 0x0007a000)
#define HWIO_GCC_VOLTAGE_DROOP_DETECTOR_BCR_RMSK                                                         0x1
#define HWIO_GCC_VOLTAGE_DROOP_DETECTOR_BCR_IN          \
        in_dword_masked(HWIO_GCC_VOLTAGE_DROOP_DETECTOR_BCR_ADDR, HWIO_GCC_VOLTAGE_DROOP_DETECTOR_BCR_RMSK)
#define HWIO_GCC_VOLTAGE_DROOP_DETECTOR_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_VOLTAGE_DROOP_DETECTOR_BCR_ADDR, m)
#define HWIO_GCC_VOLTAGE_DROOP_DETECTOR_BCR_OUT(v)      \
        out_dword(HWIO_GCC_VOLTAGE_DROOP_DETECTOR_BCR_ADDR,v)
#define HWIO_GCC_VOLTAGE_DROOP_DETECTOR_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_VOLTAGE_DROOP_DETECTOR_BCR_ADDR,m,v,HWIO_GCC_VOLTAGE_DROOP_DETECTOR_BCR_IN)
#define HWIO_GCC_VOLTAGE_DROOP_DETECTOR_BCR_BLK_ARES_BMSK                                                0x1
#define HWIO_GCC_VOLTAGE_DROOP_DETECTOR_BCR_BLK_ARES_SHFT                                                0x0

#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_CMD_RCGR_ADDR                                          (GCC_CLK_CTL_REG_REG_BASE      + 0x0001c0f0)
#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_CMD_RCGR_RMSK                                          0x800000f3
#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_CMD_RCGR_ADDR, HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_CMD_RCGR_RMSK)
#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_CMD_RCGR_ADDR, m)
#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_CMD_RCGR_ADDR,v)
#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_CMD_RCGR_ADDR,m,v,HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_CMD_RCGR_IN)
#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_CMD_RCGR_ROOT_OFF_BMSK                                 0x80000000
#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_CMD_RCGR_ROOT_OFF_SHFT                                       0x1f
#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_CMD_RCGR_DIRTY_D_BMSK                                        0x80
#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_CMD_RCGR_DIRTY_D_SHFT                                         0x7
#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_CMD_RCGR_DIRTY_N_BMSK                                        0x40
#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_CMD_RCGR_DIRTY_N_SHFT                                         0x6
#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_CMD_RCGR_DIRTY_M_BMSK                                        0x20
#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_CMD_RCGR_DIRTY_M_SHFT                                         0x5
#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                                 0x10
#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                                  0x4
#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_CMD_RCGR_ROOT_EN_BMSK                                         0x2
#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_CMD_RCGR_ROOT_EN_SHFT                                         0x1
#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_CMD_RCGR_UPDATE_BMSK                                          0x1
#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_CMD_RCGR_UPDATE_SHFT                                          0x0

#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_CFG_RCGR_ADDR                                          (GCC_CLK_CTL_REG_REG_BASE      + 0x0001c0f4)
#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_CFG_RCGR_RMSK                                              0x371f
#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_CFG_RCGR_ADDR, HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_CFG_RCGR_RMSK)
#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_CFG_RCGR_ADDR, m)
#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_CFG_RCGR_ADDR,v)
#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_CFG_RCGR_ADDR,m,v,HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_CFG_RCGR_IN)
#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_CFG_RCGR_MODE_BMSK                                         0x3000
#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_CFG_RCGR_MODE_SHFT                                            0xc
#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_CFG_RCGR_SRC_SEL_BMSK                                       0x700
#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_CFG_RCGR_SRC_SEL_SHFT                                         0x8
#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_CFG_RCGR_SRC_DIV_BMSK                                        0x1f
#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_CFG_RCGR_SRC_DIV_SHFT                                         0x0

#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_M_ADDR                                                 (GCC_CLK_CTL_REG_REG_BASE      + 0x0001c0f8)
#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_M_RMSK                                                       0xff
#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_M_IN          \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_M_ADDR, HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_M_RMSK)
#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_M_INM(m)      \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_M_ADDR, m)
#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_M_OUT(v)      \
        out_dword(HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_M_ADDR,v)
#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_M_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_M_ADDR,m,v,HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_M_IN)
#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_M_M_BMSK                                                     0xff
#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_M_M_SHFT                                                      0x0

#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_N_ADDR                                                 (GCC_CLK_CTL_REG_REG_BASE      + 0x0001c0fc)
#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_N_RMSK                                                       0xff
#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_N_IN          \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_N_ADDR, HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_N_RMSK)
#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_N_INM(m)      \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_N_ADDR, m)
#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_N_OUT(v)      \
        out_dword(HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_N_ADDR,v)
#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_N_ADDR,m,v,HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_N_IN)
#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_N_NOT_N_MINUS_M_BMSK                                         0xff
#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_N_NOT_N_MINUS_M_SHFT                                          0x0

#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_D_ADDR                                                 (GCC_CLK_CTL_REG_REG_BASE      + 0x0001c100)
#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_D_RMSK                                                       0xff
#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_D_IN          \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_D_ADDR, HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_D_RMSK)
#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_D_INM(m)      \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_D_ADDR, m)
#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_D_OUT(v)      \
        out_dword(HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_D_ADDR,v)
#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_D_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_D_ADDR,m,v,HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_D_IN)
#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_D_NOT_2D_BMSK                                                0xff
#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_D_NOT_2D_SHFT                                                 0x0

#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_CBCR_ADDR                                              (GCC_CLK_CTL_REG_REG_BASE      + 0x0001c104)
#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_CBCR_RMSK                                              0x80000001
#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_CBCR_IN          \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_CBCR_ADDR, HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_CBCR_RMSK)
#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_CBCR_ADDR, m)
#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_CBCR_ADDR,v)
#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_CBCR_ADDR,m,v,HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_CBCR_IN)
#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_CBCR_CLK_OFF_BMSK                                      0x80000000
#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_CBCR_CLK_OFF_SHFT                                            0x1f
#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_CBCR_CLK_ENABLE_BMSK                                          0x1
#define HWIO_GCC_ULTAUDIO_LPAIF_PCM_DATAOE_CBCR_CLK_ENABLE_SHFT                                          0x0

#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_CMD_RCGR_ADDR                                      (GCC_CLK_CTL_REG_REG_BASE      + 0x0001c108)
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_CMD_RCGR_RMSK                                      0x800000f3
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_CMD_RCGR_IN          \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_CMD_RCGR_ADDR, HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_CMD_RCGR_RMSK)
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_CMD_RCGR_ADDR, m)
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_CMD_RCGR_ADDR,v)
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_CMD_RCGR_ADDR,m,v,HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_CMD_RCGR_IN)
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_CMD_RCGR_ROOT_OFF_BMSK                             0x80000000
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_CMD_RCGR_ROOT_OFF_SHFT                                   0x1f
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_CMD_RCGR_DIRTY_D_BMSK                                    0x80
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_CMD_RCGR_DIRTY_D_SHFT                                     0x7
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_CMD_RCGR_DIRTY_N_BMSK                                    0x40
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_CMD_RCGR_DIRTY_N_SHFT                                     0x6
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_CMD_RCGR_DIRTY_M_BMSK                                    0x20
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_CMD_RCGR_DIRTY_M_SHFT                                     0x5
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_CMD_RCGR_DIRTY_CFG_RCGR_BMSK                             0x10
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_CMD_RCGR_DIRTY_CFG_RCGR_SHFT                              0x4
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_CMD_RCGR_ROOT_EN_BMSK                                     0x2
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_CMD_RCGR_ROOT_EN_SHFT                                     0x1
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_CMD_RCGR_UPDATE_BMSK                                      0x1
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_CMD_RCGR_UPDATE_SHFT                                      0x0

#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_CFG_RCGR_ADDR                                      (GCC_CLK_CTL_REG_REG_BASE      + 0x0001c10c)
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_CFG_RCGR_RMSK                                          0x371f
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_CFG_RCGR_IN          \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_CFG_RCGR_ADDR, HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_CFG_RCGR_RMSK)
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_CFG_RCGR_ADDR, m)
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_CFG_RCGR_ADDR,v)
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_CFG_RCGR_ADDR,m,v,HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_CFG_RCGR_IN)
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_CFG_RCGR_MODE_BMSK                                     0x3000
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_CFG_RCGR_MODE_SHFT                                        0xc
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_CFG_RCGR_SRC_SEL_BMSK                                   0x700
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_CFG_RCGR_SRC_SEL_SHFT                                     0x8
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_CFG_RCGR_SRC_DIV_BMSK                                    0x1f
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_CFG_RCGR_SRC_DIV_SHFT                                     0x0

#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_M_ADDR                                             (GCC_CLK_CTL_REG_REG_BASE      + 0x0001c110)
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_M_RMSK                                                   0xff
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_M_IN          \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_M_ADDR, HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_M_RMSK)
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_M_INM(m)      \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_M_ADDR, m)
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_M_OUT(v)      \
        out_dword(HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_M_ADDR,v)
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_M_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_M_ADDR,m,v,HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_M_IN)
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_M_M_BMSK                                                 0xff
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_M_M_SHFT                                                  0x0

#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_N_ADDR                                             (GCC_CLK_CTL_REG_REG_BASE      + 0x0001c114)
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_N_RMSK                                                   0xff
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_N_IN          \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_N_ADDR, HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_N_RMSK)
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_N_INM(m)      \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_N_ADDR, m)
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_N_OUT(v)      \
        out_dword(HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_N_ADDR,v)
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_N_ADDR,m,v,HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_N_IN)
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_N_NOT_N_MINUS_M_BMSK                                     0xff
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_N_NOT_N_MINUS_M_SHFT                                      0x0

#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_D_ADDR                                             (GCC_CLK_CTL_REG_REG_BASE      + 0x0001c118)
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_D_RMSK                                                   0xff
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_D_IN          \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_D_ADDR, HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_D_RMSK)
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_D_INM(m)      \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_D_ADDR, m)
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_D_OUT(v)      \
        out_dword(HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_D_ADDR,v)
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_D_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_D_ADDR,m,v,HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_D_IN)
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_D_NOT_2D_BMSK                                            0xff
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_D_NOT_2D_SHFT                                             0x0

#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_CBCR_ADDR                                          (GCC_CLK_CTL_REG_REG_BASE      + 0x0001c11c)
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_CBCR_RMSK                                          0x80000001
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_CBCR_IN          \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_CBCR_ADDR, HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_CBCR_RMSK)
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_CBCR_ADDR, m)
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_CBCR_ADDR,v)
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_CBCR_ADDR,m,v,HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_CBCR_IN)
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_CBCR_CLK_OFF_BMSK                                  0x80000000
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_CBCR_CLK_OFF_SHFT                                        0x1f
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_CBCR_CLK_ENABLE_BMSK                                      0x1
#define HWIO_GCC_ULTAUDIO_LPAIF_AUX_PCM_DATAOE_CBCR_CLK_ENABLE_SHFT                                      0x0

#define HWIO_GCC_ULTAUDIO_AVSYNC_XO_CBCR_ADDR                                                     (GCC_CLK_CTL_REG_REG_BASE      + 0x0001c04c)
#define HWIO_GCC_ULTAUDIO_AVSYNC_XO_CBCR_RMSK                                                     0x80000001
#define HWIO_GCC_ULTAUDIO_AVSYNC_XO_CBCR_IN          \
        in_dword_masked(HWIO_GCC_ULTAUDIO_AVSYNC_XO_CBCR_ADDR, HWIO_GCC_ULTAUDIO_AVSYNC_XO_CBCR_RMSK)
#define HWIO_GCC_ULTAUDIO_AVSYNC_XO_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_ULTAUDIO_AVSYNC_XO_CBCR_ADDR, m)
#define HWIO_GCC_ULTAUDIO_AVSYNC_XO_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_ULTAUDIO_AVSYNC_XO_CBCR_ADDR,v)
#define HWIO_GCC_ULTAUDIO_AVSYNC_XO_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_ULTAUDIO_AVSYNC_XO_CBCR_ADDR,m,v,HWIO_GCC_ULTAUDIO_AVSYNC_XO_CBCR_IN)
#define HWIO_GCC_ULTAUDIO_AVSYNC_XO_CBCR_CLK_OFF_BMSK                                             0x80000000
#define HWIO_GCC_ULTAUDIO_AVSYNC_XO_CBCR_CLK_OFF_SHFT                                                   0x1f
#define HWIO_GCC_ULTAUDIO_AVSYNC_XO_CBCR_CLK_ENABLE_BMSK                                                 0x1
#define HWIO_GCC_ULTAUDIO_AVSYNC_XO_CBCR_CLK_ENABLE_SHFT                                                 0x0

/*----------------------------------------------------------------------------
 * MODULE: MSS_PERPH
 *--------------------------------------------------------------------------*/

#define MSS_PERPH_REG_BASE                                                (MSS_TOP_BASE      + 0x00180000)

#define HWIO_MSS_ENABLE_ADDR                                              (MSS_PERPH_REG_BASE      + 0x00000000)
#define HWIO_MSS_ENABLE_RMSK                                                    0xff
#define HWIO_MSS_ENABLE_IN          \
        in_dword_masked(HWIO_MSS_ENABLE_ADDR, HWIO_MSS_ENABLE_RMSK)
#define HWIO_MSS_ENABLE_INM(m)      \
        in_dword_masked(HWIO_MSS_ENABLE_ADDR, m)
#define HWIO_MSS_ENABLE_OUT(v)      \
        out_dword(HWIO_MSS_ENABLE_ADDR,v)
#define HWIO_MSS_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_ENABLE_ADDR,m,v,HWIO_MSS_ENABLE_IN)
#define HWIO_MSS_ENABLE_COXM_BMSK                                               0x80
#define HWIO_MSS_ENABLE_COXM_SHFT                                                0x7
#define HWIO_MSS_ENABLE_RBCPR_BMSK                                              0x40
#define HWIO_MSS_ENABLE_RBCPR_SHFT                                               0x6
#define HWIO_MSS_ENABLE_CRYPTO5_BMSK                                            0x20
#define HWIO_MSS_ENABLE_CRYPTO5_SHFT                                             0x5
#define HWIO_MSS_ENABLE_UIM1_BMSK                                               0x10
#define HWIO_MSS_ENABLE_UIM1_SHFT                                                0x4
#define HWIO_MSS_ENABLE_UIM0_BMSK                                                0x8
#define HWIO_MSS_ENABLE_UIM0_SHFT                                                0x3
#define HWIO_MSS_ENABLE_NAV_BMSK                                                 0x4
#define HWIO_MSS_ENABLE_NAV_SHFT                                                 0x2
#define HWIO_MSS_ENABLE_Q6_BMSK                                                  0x2
#define HWIO_MSS_ENABLE_Q6_SHFT                                                  0x1
#define HWIO_MSS_ENABLE_MODEM_BMSK                                               0x1
#define HWIO_MSS_ENABLE_MODEM_SHFT                                               0x0

#define HWIO_MSS_CLAMP_MEM_ADDR                                           (MSS_PERPH_REG_BASE      + 0x00000004)
#define HWIO_MSS_CLAMP_MEM_RMSK                                                  0x7
#define HWIO_MSS_CLAMP_MEM_IN          \
        in_dword_masked(HWIO_MSS_CLAMP_MEM_ADDR, HWIO_MSS_CLAMP_MEM_RMSK)
#define HWIO_MSS_CLAMP_MEM_INM(m)      \
        in_dword_masked(HWIO_MSS_CLAMP_MEM_ADDR, m)
#define HWIO_MSS_CLAMP_MEM_OUT(v)      \
        out_dword(HWIO_MSS_CLAMP_MEM_ADDR,v)
#define HWIO_MSS_CLAMP_MEM_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_CLAMP_MEM_ADDR,m,v,HWIO_MSS_CLAMP_MEM_IN)
#define HWIO_MSS_CLAMP_MEM_SPARE_BMSK                                            0x4
#define HWIO_MSS_CLAMP_MEM_SPARE_SHFT                                            0x2
#define HWIO_MSS_CLAMP_MEM_UNCLAMP_ALL_BMSK                                      0x2
#define HWIO_MSS_CLAMP_MEM_UNCLAMP_ALL_SHFT                                      0x1
#define HWIO_MSS_CLAMP_MEM_HM_CLAMP_BMSK                                         0x1
#define HWIO_MSS_CLAMP_MEM_HM_CLAMP_SHFT                                         0x0

#define HWIO_MSS_CLAMP_IO_ADDR                                            (MSS_PERPH_REG_BASE      + 0x00000008)
#define HWIO_MSS_CLAMP_IO_RMSK                                                  0xff
#define HWIO_MSS_CLAMP_IO_IN          \
        in_dword_masked(HWIO_MSS_CLAMP_IO_ADDR, HWIO_MSS_CLAMP_IO_RMSK)
#define HWIO_MSS_CLAMP_IO_INM(m)      \
        in_dword_masked(HWIO_MSS_CLAMP_IO_ADDR, m)
#define HWIO_MSS_CLAMP_IO_OUT(v)      \
        out_dword(HWIO_MSS_CLAMP_IO_ADDR,v)
#define HWIO_MSS_CLAMP_IO_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_CLAMP_IO_ADDR,m,v,HWIO_MSS_CLAMP_IO_IN)
#define HWIO_MSS_CLAMP_IO_SPARE_7_BMSK                                          0x80
#define HWIO_MSS_CLAMP_IO_SPARE_7_SHFT                                           0x7
#define HWIO_MSS_CLAMP_IO_UNCLAMP_ALL_BMSK                                      0x40
#define HWIO_MSS_CLAMP_IO_UNCLAMP_ALL_SHFT                                       0x6
#define HWIO_MSS_CLAMP_IO_RBCPR_BMSK                                            0x20
#define HWIO_MSS_CLAMP_IO_RBCPR_SHFT                                             0x5
#define HWIO_MSS_CLAMP_IO_BBRX_ADC_BMSK                                         0x10
#define HWIO_MSS_CLAMP_IO_BBRX_ADC_SHFT                                          0x4
#define HWIO_MSS_CLAMP_IO_GNSS_DAC_BMSK                                          0x8
#define HWIO_MSS_CLAMP_IO_GNSS_DAC_SHFT                                          0x3
#define HWIO_MSS_CLAMP_IO_COM_COMP_BMSK                                          0x4
#define HWIO_MSS_CLAMP_IO_COM_COMP_SHFT                                          0x2
#define HWIO_MSS_CLAMP_IO_NC_HM_BMSK                                             0x2
#define HWIO_MSS_CLAMP_IO_NC_HM_SHFT                                             0x1
#define HWIO_MSS_CLAMP_IO_MODEM_BMSK                                             0x1
#define HWIO_MSS_CLAMP_IO_MODEM_SHFT                                             0x0

#define HWIO_MSS_BUS_AHB2AHB_CFG_ADDR                                     (MSS_PERPH_REG_BASE      + 0x0000000c)
#define HWIO_MSS_BUS_AHB2AHB_CFG_RMSK                                            0x3
#define HWIO_MSS_BUS_AHB2AHB_CFG_IN          \
        in_dword_masked(HWIO_MSS_BUS_AHB2AHB_CFG_ADDR, HWIO_MSS_BUS_AHB2AHB_CFG_RMSK)
#define HWIO_MSS_BUS_AHB2AHB_CFG_INM(m)      \
        in_dword_masked(HWIO_MSS_BUS_AHB2AHB_CFG_ADDR, m)
#define HWIO_MSS_BUS_AHB2AHB_CFG_OUT(v)      \
        out_dword(HWIO_MSS_BUS_AHB2AHB_CFG_ADDR,v)
#define HWIO_MSS_BUS_AHB2AHB_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BUS_AHB2AHB_CFG_ADDR,m,v,HWIO_MSS_BUS_AHB2AHB_CFG_IN)
#define HWIO_MSS_BUS_AHB2AHB_CFG_POST_EN_AHB2AHB_NAV_BMSK                        0x2
#define HWIO_MSS_BUS_AHB2AHB_CFG_POST_EN_AHB2AHB_NAV_SHFT                        0x1
#define HWIO_MSS_BUS_AHB2AHB_CFG_POST_EN_AHB2AHB_BMSK                            0x1
#define HWIO_MSS_BUS_AHB2AHB_CFG_POST_EN_AHB2AHB_SHFT                            0x0

#define HWIO_MSS_BUS_MAXI2AXI_CFG_ADDR                                    (MSS_PERPH_REG_BASE      + 0x00000010)
#define HWIO_MSS_BUS_MAXI2AXI_CFG_RMSK                                           0xf
#define HWIO_MSS_BUS_MAXI2AXI_CFG_IN          \
        in_dword_masked(HWIO_MSS_BUS_MAXI2AXI_CFG_ADDR, HWIO_MSS_BUS_MAXI2AXI_CFG_RMSK)
#define HWIO_MSS_BUS_MAXI2AXI_CFG_INM(m)      \
        in_dword_masked(HWIO_MSS_BUS_MAXI2AXI_CFG_ADDR, m)
#define HWIO_MSS_BUS_MAXI2AXI_CFG_OUT(v)      \
        out_dword(HWIO_MSS_BUS_MAXI2AXI_CFG_ADDR,v)
#define HWIO_MSS_BUS_MAXI2AXI_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BUS_MAXI2AXI_CFG_ADDR,m,v,HWIO_MSS_BUS_MAXI2AXI_CFG_IN)
#define HWIO_MSS_BUS_MAXI2AXI_CFG_I_AXI_NAV_AREQPRIORITY_BMSK                    0xc
#define HWIO_MSS_BUS_MAXI2AXI_CFG_I_AXI_NAV_AREQPRIORITY_SHFT                    0x2
#define HWIO_MSS_BUS_MAXI2AXI_CFG_I_AXI_CRYPTO_AREQPRIORITY_BMSK                 0x3
#define HWIO_MSS_BUS_MAXI2AXI_CFG_I_AXI_CRYPTO_AREQPRIORITY_SHFT                 0x0

#define HWIO_MSS_CUSTOM_MEM_ARRSTBYN_ADDR                                 (MSS_PERPH_REG_BASE      + 0x00000014)
#define HWIO_MSS_CUSTOM_MEM_ARRSTBYN_RMSK                                       0xff
#define HWIO_MSS_CUSTOM_MEM_ARRSTBYN_IN          \
        in_dword_masked(HWIO_MSS_CUSTOM_MEM_ARRSTBYN_ADDR, HWIO_MSS_CUSTOM_MEM_ARRSTBYN_RMSK)
#define HWIO_MSS_CUSTOM_MEM_ARRSTBYN_INM(m)      \
        in_dword_masked(HWIO_MSS_CUSTOM_MEM_ARRSTBYN_ADDR, m)
#define HWIO_MSS_CUSTOM_MEM_ARRSTBYN_OUT(v)      \
        out_dword(HWIO_MSS_CUSTOM_MEM_ARRSTBYN_ADDR,v)
#define HWIO_MSS_CUSTOM_MEM_ARRSTBYN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_CUSTOM_MEM_ARRSTBYN_ADDR,m,v,HWIO_MSS_CUSTOM_MEM_ARRSTBYN_IN)
#define HWIO_MSS_CUSTOM_MEM_ARRSTBYN_CTL_BMSK                                   0xff
#define HWIO_MSS_CUSTOM_MEM_ARRSTBYN_CTL_SHFT                                    0x0

#define HWIO_MSS_ANALOG_IP_TEST_CTL_ADDR                                  (MSS_PERPH_REG_BASE      + 0x00000018)
#define HWIO_MSS_ANALOG_IP_TEST_CTL_RMSK                                         0x3
#define HWIO_MSS_ANALOG_IP_TEST_CTL_IN          \
        in_dword_masked(HWIO_MSS_ANALOG_IP_TEST_CTL_ADDR, HWIO_MSS_ANALOG_IP_TEST_CTL_RMSK)
#define HWIO_MSS_ANALOG_IP_TEST_CTL_INM(m)      \
        in_dword_masked(HWIO_MSS_ANALOG_IP_TEST_CTL_ADDR, m)
#define HWIO_MSS_ANALOG_IP_TEST_CTL_OUT(v)      \
        out_dword(HWIO_MSS_ANALOG_IP_TEST_CTL_ADDR,v)
#define HWIO_MSS_ANALOG_IP_TEST_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_ANALOG_IP_TEST_CTL_ADDR,m,v,HWIO_MSS_ANALOG_IP_TEST_CTL_IN)
#define HWIO_MSS_ANALOG_IP_TEST_CTL_EXTERNAL_IQDATA_EN_BMSK                      0x2
#define HWIO_MSS_ANALOG_IP_TEST_CTL_EXTERNAL_IQDATA_EN_SHFT                      0x1
#define HWIO_MSS_ANALOG_IP_TEST_CTL_EXTERNAL_Y1Y2_EN_BMSK                        0x1
#define HWIO_MSS_ANALOG_IP_TEST_CTL_EXTERNAL_Y1Y2_EN_SHFT                        0x0

#define HWIO_MSS_ATB_ID_ADDR                                              (MSS_PERPH_REG_BASE      + 0x0000001c)
#define HWIO_MSS_ATB_ID_RMSK                                                    0x7f
#define HWIO_MSS_ATB_ID_IN          \
        in_dword_masked(HWIO_MSS_ATB_ID_ADDR, HWIO_MSS_ATB_ID_RMSK)
#define HWIO_MSS_ATB_ID_INM(m)      \
        in_dword_masked(HWIO_MSS_ATB_ID_ADDR, m)
#define HWIO_MSS_ATB_ID_OUT(v)      \
        out_dword(HWIO_MSS_ATB_ID_ADDR,v)
#define HWIO_MSS_ATB_ID_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_ATB_ID_ADDR,m,v,HWIO_MSS_ATB_ID_IN)
#define HWIO_MSS_ATB_ID_ATB_ID_BMSK                                             0x7f
#define HWIO_MSS_ATB_ID_ATB_ID_SHFT                                              0x0

#define HWIO_MSS_DBG_BUS_CTL_ADDR                                         (MSS_PERPH_REG_BASE      + 0x00000020)
#define HWIO_MSS_DBG_BUS_CTL_RMSK                                            0x7ffff
#define HWIO_MSS_DBG_BUS_CTL_IN          \
        in_dword_masked(HWIO_MSS_DBG_BUS_CTL_ADDR, HWIO_MSS_DBG_BUS_CTL_RMSK)
#define HWIO_MSS_DBG_BUS_CTL_INM(m)      \
        in_dword_masked(HWIO_MSS_DBG_BUS_CTL_ADDR, m)
#define HWIO_MSS_DBG_BUS_CTL_OUT(v)      \
        out_dword(HWIO_MSS_DBG_BUS_CTL_ADDR,v)
#define HWIO_MSS_DBG_BUS_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_DBG_BUS_CTL_ADDR,m,v,HWIO_MSS_DBG_BUS_CTL_IN)
#define HWIO_MSS_DBG_BUS_CTL_BRIC_AXI2AXI_NAV_SEL_BMSK                       0x70000
#define HWIO_MSS_DBG_BUS_CTL_BRIC_AXI2AXI_NAV_SEL_SHFT                          0x10
#define HWIO_MSS_DBG_BUS_CTL_AHB2AXI_NAV_SEL_BMSK                             0xe000
#define HWIO_MSS_DBG_BUS_CTL_AHB2AXI_NAV_SEL_SHFT                                0xd
#define HWIO_MSS_DBG_BUS_CTL_AHB2AHB_NAV_CONFIG_SEL_BMSK                      0x1800
#define HWIO_MSS_DBG_BUS_CTL_AHB2AHB_NAV_CONFIG_SEL_SHFT                         0xb
#define HWIO_MSS_DBG_BUS_CTL_AHB2AHB_SEL_BMSK                                  0x600
#define HWIO_MSS_DBG_BUS_CTL_AHB2AHB_SEL_SHFT                                    0x9
#define HWIO_MSS_DBG_BUS_CTL_MSS_DBG_BUS_TOP_SEL_BMSK                          0x1e0
#define HWIO_MSS_DBG_BUS_CTL_MSS_DBG_BUS_TOP_SEL_SHFT                            0x5
#define HWIO_MSS_DBG_BUS_CTL_MSS_DBG_BUS_NC_HM_SEL_BMSK                         0x1c
#define HWIO_MSS_DBG_BUS_CTL_MSS_DBG_BUS_NC_HM_SEL_SHFT                          0x2
#define HWIO_MSS_DBG_BUS_CTL_MSS_DBG_GPIO_ATB_SEL_BMSK                           0x3
#define HWIO_MSS_DBG_BUS_CTL_MSS_DBG_GPIO_ATB_SEL_SHFT                           0x0

#define HWIO_MSS_AHB_ACCESS_ERR_IRQ_EN_ADDR                               (MSS_PERPH_REG_BASE      + 0x00000024)
#define HWIO_MSS_AHB_ACCESS_ERR_IRQ_EN_RMSK                                      0x1
#define HWIO_MSS_AHB_ACCESS_ERR_IRQ_EN_IN          \
        in_dword_masked(HWIO_MSS_AHB_ACCESS_ERR_IRQ_EN_ADDR, HWIO_MSS_AHB_ACCESS_ERR_IRQ_EN_RMSK)
#define HWIO_MSS_AHB_ACCESS_ERR_IRQ_EN_INM(m)      \
        in_dword_masked(HWIO_MSS_AHB_ACCESS_ERR_IRQ_EN_ADDR, m)
#define HWIO_MSS_AHB_ACCESS_ERR_IRQ_EN_OUT(v)      \
        out_dword(HWIO_MSS_AHB_ACCESS_ERR_IRQ_EN_ADDR,v)
#define HWIO_MSS_AHB_ACCESS_ERR_IRQ_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_AHB_ACCESS_ERR_IRQ_EN_ADDR,m,v,HWIO_MSS_AHB_ACCESS_ERR_IRQ_EN_IN)
#define HWIO_MSS_AHB_ACCESS_ERR_IRQ_EN_EN_BMSK                                   0x1
#define HWIO_MSS_AHB_ACCESS_ERR_IRQ_EN_EN_SHFT                                   0x0

#define HWIO_MSS_AHB_ACCESS_ERR_IRQ_STATUS_ADDR                           (MSS_PERPH_REG_BASE      + 0x00000028)
#define HWIO_MSS_AHB_ACCESS_ERR_IRQ_STATUS_RMSK                                  0x1
#define HWIO_MSS_AHB_ACCESS_ERR_IRQ_STATUS_IN          \
        in_dword_masked(HWIO_MSS_AHB_ACCESS_ERR_IRQ_STATUS_ADDR, HWIO_MSS_AHB_ACCESS_ERR_IRQ_STATUS_RMSK)
#define HWIO_MSS_AHB_ACCESS_ERR_IRQ_STATUS_INM(m)      \
        in_dword_masked(HWIO_MSS_AHB_ACCESS_ERR_IRQ_STATUS_ADDR, m)
#define HWIO_MSS_AHB_ACCESS_ERR_IRQ_STATUS_STATUS_BMSK                           0x1
#define HWIO_MSS_AHB_ACCESS_ERR_IRQ_STATUS_STATUS_SHFT                           0x0

#define HWIO_MSS_AHB_ACCESS_ERR_IRQ_CLR_ADDR                              (MSS_PERPH_REG_BASE      + 0x0000002c)
#define HWIO_MSS_AHB_ACCESS_ERR_IRQ_CLR_RMSK                                     0x1
#define HWIO_MSS_AHB_ACCESS_ERR_IRQ_CLR_OUT(v)      \
        out_dword(HWIO_MSS_AHB_ACCESS_ERR_IRQ_CLR_ADDR,v)
#define HWIO_MSS_AHB_ACCESS_ERR_IRQ_CLR_CMD_BMSK                                 0x1
#define HWIO_MSS_AHB_ACCESS_ERR_IRQ_CLR_CMD_SHFT                                 0x0

#define HWIO_MSS_BUS_CTL_CFG_ADDR                                         (MSS_PERPH_REG_BASE      + 0x00000030)
#define HWIO_MSS_BUS_CTL_CFG_RMSK                                                0x1
#define HWIO_MSS_BUS_CTL_CFG_IN          \
        in_dword_masked(HWIO_MSS_BUS_CTL_CFG_ADDR, HWIO_MSS_BUS_CTL_CFG_RMSK)
#define HWIO_MSS_BUS_CTL_CFG_INM(m)      \
        in_dword_masked(HWIO_MSS_BUS_CTL_CFG_ADDR, m)
#define HWIO_MSS_BUS_CTL_CFG_OUT(v)      \
        out_dword(HWIO_MSS_BUS_CTL_CFG_ADDR,v)
#define HWIO_MSS_BUS_CTL_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BUS_CTL_CFG_ADDR,m,v,HWIO_MSS_BUS_CTL_CFG_IN)
#define HWIO_MSS_BUS_CTL_CFG_Q6_FORCE_UNBUFFERED_BMSK                            0x1
#define HWIO_MSS_BUS_CTL_CFG_Q6_FORCE_UNBUFFERED_SHFT                            0x0

#define HWIO_MSS_RELAY_MSG_SHADOW0_ADDR                                   (MSS_PERPH_REG_BASE      + 0x00000034)
#define HWIO_MSS_RELAY_MSG_SHADOW0_RMSK                                   0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW0_IN          \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW0_ADDR, HWIO_MSS_RELAY_MSG_SHADOW0_RMSK)
#define HWIO_MSS_RELAY_MSG_SHADOW0_INM(m)      \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW0_ADDR, m)
#define HWIO_MSS_RELAY_MSG_SHADOW0_OUT(v)      \
        out_dword(HWIO_MSS_RELAY_MSG_SHADOW0_ADDR,v)
#define HWIO_MSS_RELAY_MSG_SHADOW0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_RELAY_MSG_SHADOW0_ADDR,m,v,HWIO_MSS_RELAY_MSG_SHADOW0_IN)
#define HWIO_MSS_RELAY_MSG_SHADOW0_RELAY_MSG_SHADOW0_BMSK                 0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW0_RELAY_MSG_SHADOW0_SHFT                        0x0

#define HWIO_MSS_RELAY_MSG_SHADOW1_ADDR                                   (MSS_PERPH_REG_BASE      + 0x00000038)
#define HWIO_MSS_RELAY_MSG_SHADOW1_RMSK                                   0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW1_IN          \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW1_ADDR, HWIO_MSS_RELAY_MSG_SHADOW1_RMSK)
#define HWIO_MSS_RELAY_MSG_SHADOW1_INM(m)      \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW1_ADDR, m)
#define HWIO_MSS_RELAY_MSG_SHADOW1_OUT(v)      \
        out_dword(HWIO_MSS_RELAY_MSG_SHADOW1_ADDR,v)
#define HWIO_MSS_RELAY_MSG_SHADOW1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_RELAY_MSG_SHADOW1_ADDR,m,v,HWIO_MSS_RELAY_MSG_SHADOW1_IN)
#define HWIO_MSS_RELAY_MSG_SHADOW1_RELAY_MSG_SHADOW1_BMSK                 0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW1_RELAY_MSG_SHADOW1_SHFT                        0x0

#define HWIO_MSS_RELAY_MSG_SHADOW2_ADDR                                   (MSS_PERPH_REG_BASE      + 0x0000003c)
#define HWIO_MSS_RELAY_MSG_SHADOW2_RMSK                                   0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW2_IN          \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW2_ADDR, HWIO_MSS_RELAY_MSG_SHADOW2_RMSK)
#define HWIO_MSS_RELAY_MSG_SHADOW2_INM(m)      \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW2_ADDR, m)
#define HWIO_MSS_RELAY_MSG_SHADOW2_OUT(v)      \
        out_dword(HWIO_MSS_RELAY_MSG_SHADOW2_ADDR,v)
#define HWIO_MSS_RELAY_MSG_SHADOW2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_RELAY_MSG_SHADOW2_ADDR,m,v,HWIO_MSS_RELAY_MSG_SHADOW2_IN)
#define HWIO_MSS_RELAY_MSG_SHADOW2_RELAY_MSG_SHADOW2_BMSK                 0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW2_RELAY_MSG_SHADOW2_SHFT                        0x0

#define HWIO_MSS_RELAY_MSG_SHADOW3_ADDR                                   (MSS_PERPH_REG_BASE      + 0x00000040)
#define HWIO_MSS_RELAY_MSG_SHADOW3_RMSK                                   0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW3_IN          \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW3_ADDR, HWIO_MSS_RELAY_MSG_SHADOW3_RMSK)
#define HWIO_MSS_RELAY_MSG_SHADOW3_INM(m)      \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW3_ADDR, m)
#define HWIO_MSS_RELAY_MSG_SHADOW3_OUT(v)      \
        out_dword(HWIO_MSS_RELAY_MSG_SHADOW3_ADDR,v)
#define HWIO_MSS_RELAY_MSG_SHADOW3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_RELAY_MSG_SHADOW3_ADDR,m,v,HWIO_MSS_RELAY_MSG_SHADOW3_IN)
#define HWIO_MSS_RELAY_MSG_SHADOW3_RELAY_MSG_SHADOW3_BMSK                 0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW3_RELAY_MSG_SHADOW3_SHFT                        0x0

#define HWIO_MSS_RELAY_MSG_SHADOW4_ADDR                                   (MSS_PERPH_REG_BASE      + 0x00000044)
#define HWIO_MSS_RELAY_MSG_SHADOW4_RMSK                                   0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW4_IN          \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW4_ADDR, HWIO_MSS_RELAY_MSG_SHADOW4_RMSK)
#define HWIO_MSS_RELAY_MSG_SHADOW4_INM(m)      \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW4_ADDR, m)
#define HWIO_MSS_RELAY_MSG_SHADOW4_OUT(v)      \
        out_dword(HWIO_MSS_RELAY_MSG_SHADOW4_ADDR,v)
#define HWIO_MSS_RELAY_MSG_SHADOW4_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_RELAY_MSG_SHADOW4_ADDR,m,v,HWIO_MSS_RELAY_MSG_SHADOW4_IN)
#define HWIO_MSS_RELAY_MSG_SHADOW4_RELAY_MSG_SHADOW4_BMSK                 0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW4_RELAY_MSG_SHADOW4_SHFT                        0x0

#define HWIO_MSS_RELAY_MSG_SHADOW5_ADDR                                   (MSS_PERPH_REG_BASE      + 0x00000048)
#define HWIO_MSS_RELAY_MSG_SHADOW5_RMSK                                   0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW5_IN          \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW5_ADDR, HWIO_MSS_RELAY_MSG_SHADOW5_RMSK)
#define HWIO_MSS_RELAY_MSG_SHADOW5_INM(m)      \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW5_ADDR, m)
#define HWIO_MSS_RELAY_MSG_SHADOW5_OUT(v)      \
        out_dword(HWIO_MSS_RELAY_MSG_SHADOW5_ADDR,v)
#define HWIO_MSS_RELAY_MSG_SHADOW5_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_RELAY_MSG_SHADOW5_ADDR,m,v,HWIO_MSS_RELAY_MSG_SHADOW5_IN)
#define HWIO_MSS_RELAY_MSG_SHADOW5_RELAY_MSG_SHADOW5_BMSK                 0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW5_RELAY_MSG_SHADOW5_SHFT                        0x0

#define HWIO_MSS_RELAY_MSG_SHADOW6_ADDR                                   (MSS_PERPH_REG_BASE      + 0x0000004c)
#define HWIO_MSS_RELAY_MSG_SHADOW6_RMSK                                   0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW6_IN          \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW6_ADDR, HWIO_MSS_RELAY_MSG_SHADOW6_RMSK)
#define HWIO_MSS_RELAY_MSG_SHADOW6_INM(m)      \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW6_ADDR, m)
#define HWIO_MSS_RELAY_MSG_SHADOW6_OUT(v)      \
        out_dword(HWIO_MSS_RELAY_MSG_SHADOW6_ADDR,v)
#define HWIO_MSS_RELAY_MSG_SHADOW6_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_RELAY_MSG_SHADOW6_ADDR,m,v,HWIO_MSS_RELAY_MSG_SHADOW6_IN)
#define HWIO_MSS_RELAY_MSG_SHADOW6_RELAY_MSG_SHADOW6_BMSK                 0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW6_RELAY_MSG_SHADOW6_SHFT                        0x0

#define HWIO_MSS_RELAY_MSG_SHADOW7_ADDR                                   (MSS_PERPH_REG_BASE      + 0x00000050)
#define HWIO_MSS_RELAY_MSG_SHADOW7_RMSK                                   0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW7_IN          \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW7_ADDR, HWIO_MSS_RELAY_MSG_SHADOW7_RMSK)
#define HWIO_MSS_RELAY_MSG_SHADOW7_INM(m)      \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW7_ADDR, m)
#define HWIO_MSS_RELAY_MSG_SHADOW7_OUT(v)      \
        out_dword(HWIO_MSS_RELAY_MSG_SHADOW7_ADDR,v)
#define HWIO_MSS_RELAY_MSG_SHADOW7_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_RELAY_MSG_SHADOW7_ADDR,m,v,HWIO_MSS_RELAY_MSG_SHADOW7_IN)
#define HWIO_MSS_RELAY_MSG_SHADOW7_RELAY_MSG_SHADOW7_BMSK                 0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW7_RELAY_MSG_SHADOW7_SHFT                        0x0

#define HWIO_MSS_RELAY_MSG_SHADOW8_ADDR                                   (MSS_PERPH_REG_BASE      + 0x00000054)
#define HWIO_MSS_RELAY_MSG_SHADOW8_RMSK                                   0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW8_IN          \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW8_ADDR, HWIO_MSS_RELAY_MSG_SHADOW8_RMSK)
#define HWIO_MSS_RELAY_MSG_SHADOW8_INM(m)      \
        in_dword_masked(HWIO_MSS_RELAY_MSG_SHADOW8_ADDR, m)
#define HWIO_MSS_RELAY_MSG_SHADOW8_OUT(v)      \
        out_dword(HWIO_MSS_RELAY_MSG_SHADOW8_ADDR,v)
#define HWIO_MSS_RELAY_MSG_SHADOW8_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_RELAY_MSG_SHADOW8_ADDR,m,v,HWIO_MSS_RELAY_MSG_SHADOW8_IN)
#define HWIO_MSS_RELAY_MSG_SHADOW8_RELAY_MSG_SHADOW8_BMSK                 0xffffffff
#define HWIO_MSS_RELAY_MSG_SHADOW8_RELAY_MSG_SHADOW8_SHFT                        0x0

#define HWIO_MSS_MSA_ADDR                                                 (MSS_PERPH_REG_BASE      + 0x00000058)
#define HWIO_MSS_MSA_RMSK                                                        0x3
#define HWIO_MSS_MSA_IN          \
        in_dword_masked(HWIO_MSS_MSA_ADDR, HWIO_MSS_MSA_RMSK)
#define HWIO_MSS_MSA_INM(m)      \
        in_dword_masked(HWIO_MSS_MSA_ADDR, m)
#define HWIO_MSS_MSA_OUT(v)      \
        out_dword(HWIO_MSS_MSA_ADDR,v)
#define HWIO_MSS_MSA_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_MSA_ADDR,m,v,HWIO_MSS_MSA_IN)
#define HWIO_MSS_MSA_MBA_OK_BMSK                                                 0x2
#define HWIO_MSS_MSA_MBA_OK_SHFT                                                 0x1
#define HWIO_MSS_MSA_CONFIG_LOCK_BMSK                                            0x1
#define HWIO_MSS_MSA_CONFIG_LOCK_SHFT                                            0x0

#define HWIO_MSS_HW_VERSION_ADDR                                          (MSS_PERPH_REG_BASE      + 0x0000005c)
#define HWIO_MSS_HW_VERSION_RMSK                                          0xffffffff
#define HWIO_MSS_HW_VERSION_IN          \
        in_dword_masked(HWIO_MSS_HW_VERSION_ADDR, HWIO_MSS_HW_VERSION_RMSK)
#define HWIO_MSS_HW_VERSION_INM(m)      \
        in_dword_masked(HWIO_MSS_HW_VERSION_ADDR, m)
#define HWIO_MSS_HW_VERSION_MAJOR_BMSK                                    0xf0000000
#define HWIO_MSS_HW_VERSION_MAJOR_SHFT                                          0x1c
#define HWIO_MSS_HW_VERSION_MINOR_BMSK                                     0xfff0000
#define HWIO_MSS_HW_VERSION_MINOR_SHFT                                          0x10
#define HWIO_MSS_HW_VERSION_STEP_BMSK                                         0xffff
#define HWIO_MSS_HW_VERSION_STEP_SHFT                                            0x0

#define HWIO_MSS_DIME_MEM_SLP_CNTL_ADDR                                   (MSS_PERPH_REG_BASE      + 0x00000060)
#define HWIO_MSS_DIME_MEM_SLP_CNTL_RMSK                                    0x302030f
#define HWIO_MSS_DIME_MEM_SLP_CNTL_IN          \
        in_dword_masked(HWIO_MSS_DIME_MEM_SLP_CNTL_ADDR, HWIO_MSS_DIME_MEM_SLP_CNTL_RMSK)
#define HWIO_MSS_DIME_MEM_SLP_CNTL_INM(m)      \
        in_dword_masked(HWIO_MSS_DIME_MEM_SLP_CNTL_ADDR, m)
#define HWIO_MSS_DIME_MEM_SLP_CNTL_OUT(v)      \
        out_dword(HWIO_MSS_DIME_MEM_SLP_CNTL_ADDR,v)
#define HWIO_MSS_DIME_MEM_SLP_CNTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_DIME_MEM_SLP_CNTL_ADDR,m,v,HWIO_MSS_DIME_MEM_SLP_CNTL_IN)
#define HWIO_MSS_DIME_MEM_SLP_CNTL_MEM_SLP_SPM_BMSK                        0x2000000
#define HWIO_MSS_DIME_MEM_SLP_CNTL_MEM_SLP_SPM_SHFT                             0x19
#define HWIO_MSS_DIME_MEM_SLP_CNTL_MEM_SLP_NOW_BMSK                        0x1000000
#define HWIO_MSS_DIME_MEM_SLP_CNTL_MEM_SLP_NOW_SHFT                             0x18
#define HWIO_MSS_DIME_MEM_SLP_CNTL_ALL_NR_SLP_NRET_N_BMSK                    0x20000
#define HWIO_MSS_DIME_MEM_SLP_CNTL_ALL_NR_SLP_NRET_N_SHFT                       0x11
#define HWIO_MSS_DIME_MEM_SLP_CNTL_CCS_SLP_NRET_N_BMSK                         0x200
#define HWIO_MSS_DIME_MEM_SLP_CNTL_CCS_SLP_NRET_N_SHFT                           0x9
#define HWIO_MSS_DIME_MEM_SLP_CNTL_CCS_SLP_RET_N_BMSK                          0x100
#define HWIO_MSS_DIME_MEM_SLP_CNTL_CCS_SLP_RET_N_SHFT                            0x8
#define HWIO_MSS_DIME_MEM_SLP_CNTL_VPE1_SLP_NRET_N_BMSK                          0x8
#define HWIO_MSS_DIME_MEM_SLP_CNTL_VPE1_SLP_NRET_N_SHFT                          0x3
#define HWIO_MSS_DIME_MEM_SLP_CNTL_VPE1_SLP_RET_N_BMSK                           0x4
#define HWIO_MSS_DIME_MEM_SLP_CNTL_VPE1_SLP_RET_N_SHFT                           0x2
#define HWIO_MSS_DIME_MEM_SLP_CNTL_VPE0_SLP_NRET_N_BMSK                          0x2
#define HWIO_MSS_DIME_MEM_SLP_CNTL_VPE0_SLP_NRET_N_SHFT                          0x1
#define HWIO_MSS_DIME_MEM_SLP_CNTL_VPE0_SLP_RET_N_BMSK                           0x1
#define HWIO_MSS_DIME_MEM_SLP_CNTL_VPE0_SLP_RET_N_SHFT                           0x0

#define HWIO_MSS_CLOCK_SPDM_MON_ADDR                                      (MSS_PERPH_REG_BASE      + 0x00000064)
#define HWIO_MSS_CLOCK_SPDM_MON_RMSK                                             0x3
#define HWIO_MSS_CLOCK_SPDM_MON_IN          \
        in_dword_masked(HWIO_MSS_CLOCK_SPDM_MON_ADDR, HWIO_MSS_CLOCK_SPDM_MON_RMSK)
#define HWIO_MSS_CLOCK_SPDM_MON_INM(m)      \
        in_dword_masked(HWIO_MSS_CLOCK_SPDM_MON_ADDR, m)
#define HWIO_MSS_CLOCK_SPDM_MON_OUT(v)      \
        out_dword(HWIO_MSS_CLOCK_SPDM_MON_ADDR,v)
#define HWIO_MSS_CLOCK_SPDM_MON_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_CLOCK_SPDM_MON_ADDR,m,v,HWIO_MSS_CLOCK_SPDM_MON_IN)
#define HWIO_MSS_CLOCK_SPDM_MON_Q6_MON_CLKEN_BMSK                                0x2
#define HWIO_MSS_CLOCK_SPDM_MON_Q6_MON_CLKEN_SHFT                                0x1
#define HWIO_MSS_CLOCK_SPDM_MON_BUS_MON_CLKEN_BMSK                               0x1
#define HWIO_MSS_CLOCK_SPDM_MON_BUS_MON_CLKEN_SHFT                               0x0

#define HWIO_MSS_BBRX0_MUX_SEL_ADDR                                       (MSS_PERPH_REG_BASE      + 0x00000068)
#define HWIO_MSS_BBRX0_MUX_SEL_RMSK                                              0x3
#define HWIO_MSS_BBRX0_MUX_SEL_IN          \
        in_dword_masked(HWIO_MSS_BBRX0_MUX_SEL_ADDR, HWIO_MSS_BBRX0_MUX_SEL_RMSK)
#define HWIO_MSS_BBRX0_MUX_SEL_INM(m)      \
        in_dword_masked(HWIO_MSS_BBRX0_MUX_SEL_ADDR, m)
#define HWIO_MSS_BBRX0_MUX_SEL_OUT(v)      \
        out_dword(HWIO_MSS_BBRX0_MUX_SEL_ADDR,v)
#define HWIO_MSS_BBRX0_MUX_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BBRX0_MUX_SEL_ADDR,m,v,HWIO_MSS_BBRX0_MUX_SEL_IN)
#define HWIO_MSS_BBRX0_MUX_SEL_SECOND_MUX_SEL_BMSK                               0x2
#define HWIO_MSS_BBRX0_MUX_SEL_SECOND_MUX_SEL_SHFT                               0x1
#define HWIO_MSS_BBRX0_MUX_SEL_FIRST_MUX_SEL_BMSK                                0x1
#define HWIO_MSS_BBRX0_MUX_SEL_FIRST_MUX_SEL_SHFT                                0x0

#define HWIO_MSS_BBRX1_MUX_SEL_ADDR                                       (MSS_PERPH_REG_BASE      + 0x0000006c)
#define HWIO_MSS_BBRX1_MUX_SEL_RMSK                                              0x3
#define HWIO_MSS_BBRX1_MUX_SEL_IN          \
        in_dword_masked(HWIO_MSS_BBRX1_MUX_SEL_ADDR, HWIO_MSS_BBRX1_MUX_SEL_RMSK)
#define HWIO_MSS_BBRX1_MUX_SEL_INM(m)      \
        in_dword_masked(HWIO_MSS_BBRX1_MUX_SEL_ADDR, m)
#define HWIO_MSS_BBRX1_MUX_SEL_OUT(v)      \
        out_dword(HWIO_MSS_BBRX1_MUX_SEL_ADDR,v)
#define HWIO_MSS_BBRX1_MUX_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BBRX1_MUX_SEL_ADDR,m,v,HWIO_MSS_BBRX1_MUX_SEL_IN)
#define HWIO_MSS_BBRX1_MUX_SEL_SECOND_MUX_SEL_BMSK                               0x2
#define HWIO_MSS_BBRX1_MUX_SEL_SECOND_MUX_SEL_SHFT                               0x1
#define HWIO_MSS_BBRX1_MUX_SEL_FIRST_MUX_SEL_BMSK                                0x1
#define HWIO_MSS_BBRX1_MUX_SEL_FIRST_MUX_SEL_SHFT                                0x0

#define HWIO_MSS_BBRX2_MUX_SEL_ADDR                                       (MSS_PERPH_REG_BASE      + 0x00000070)
#define HWIO_MSS_BBRX2_MUX_SEL_RMSK                                              0x3
#define HWIO_MSS_BBRX2_MUX_SEL_IN          \
        in_dword_masked(HWIO_MSS_BBRX2_MUX_SEL_ADDR, HWIO_MSS_BBRX2_MUX_SEL_RMSK)
#define HWIO_MSS_BBRX2_MUX_SEL_INM(m)      \
        in_dword_masked(HWIO_MSS_BBRX2_MUX_SEL_ADDR, m)
#define HWIO_MSS_BBRX2_MUX_SEL_OUT(v)      \
        out_dword(HWIO_MSS_BBRX2_MUX_SEL_ADDR,v)
#define HWIO_MSS_BBRX2_MUX_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BBRX2_MUX_SEL_ADDR,m,v,HWIO_MSS_BBRX2_MUX_SEL_IN)
#define HWIO_MSS_BBRX2_MUX_SEL_SECOND_MUX_SEL_BMSK                               0x2
#define HWIO_MSS_BBRX2_MUX_SEL_SECOND_MUX_SEL_SHFT                               0x1
#define HWIO_MSS_BBRX2_MUX_SEL_FIRST_MUX_SEL_BMSK                                0x1
#define HWIO_MSS_BBRX2_MUX_SEL_FIRST_MUX_SEL_SHFT                                0x0

#define HWIO_MSS_BBRX3_MUX_SEL_ADDR                                       (MSS_PERPH_REG_BASE      + 0x00000074)
#define HWIO_MSS_BBRX3_MUX_SEL_RMSK                                              0x3
#define HWIO_MSS_BBRX3_MUX_SEL_IN          \
        in_dword_masked(HWIO_MSS_BBRX3_MUX_SEL_ADDR, HWIO_MSS_BBRX3_MUX_SEL_RMSK)
#define HWIO_MSS_BBRX3_MUX_SEL_INM(m)      \
        in_dword_masked(HWIO_MSS_BBRX3_MUX_SEL_ADDR, m)
#define HWIO_MSS_BBRX3_MUX_SEL_OUT(v)      \
        out_dword(HWIO_MSS_BBRX3_MUX_SEL_ADDR,v)
#define HWIO_MSS_BBRX3_MUX_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BBRX3_MUX_SEL_ADDR,m,v,HWIO_MSS_BBRX3_MUX_SEL_IN)
#define HWIO_MSS_BBRX3_MUX_SEL_SECOND_MUX_SEL_BMSK                               0x2
#define HWIO_MSS_BBRX3_MUX_SEL_SECOND_MUX_SEL_SHFT                               0x1
#define HWIO_MSS_BBRX3_MUX_SEL_FIRST_MUX_SEL_BMSK                                0x1
#define HWIO_MSS_BBRX3_MUX_SEL_FIRST_MUX_SEL_SHFT                                0x0

#define HWIO_MSS_DEBUG_CLOCK_CTL_ADDR                                     (MSS_PERPH_REG_BASE      + 0x00000078)
#define HWIO_MSS_DEBUG_CLOCK_CTL_RMSK                                           0x3f
#define HWIO_MSS_DEBUG_CLOCK_CTL_IN          \
        in_dword_masked(HWIO_MSS_DEBUG_CLOCK_CTL_ADDR, HWIO_MSS_DEBUG_CLOCK_CTL_RMSK)
#define HWIO_MSS_DEBUG_CLOCK_CTL_INM(m)      \
        in_dword_masked(HWIO_MSS_DEBUG_CLOCK_CTL_ADDR, m)
#define HWIO_MSS_DEBUG_CLOCK_CTL_OUT(v)      \
        out_dword(HWIO_MSS_DEBUG_CLOCK_CTL_ADDR,v)
#define HWIO_MSS_DEBUG_CLOCK_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_DEBUG_CLOCK_CTL_ADDR,m,v,HWIO_MSS_DEBUG_CLOCK_CTL_IN)
#define HWIO_MSS_DEBUG_CLOCK_CTL_DBG_MUX_SEL_BMSK                               0x20
#define HWIO_MSS_DEBUG_CLOCK_CTL_DBG_MUX_SEL_SHFT                                0x5
#define HWIO_MSS_DEBUG_CLOCK_CTL_DBG_LEVEL5_MUX_SEL_BMSK                        0x10
#define HWIO_MSS_DEBUG_CLOCK_CTL_DBG_LEVEL5_MUX_SEL_SHFT                         0x4
#define HWIO_MSS_DEBUG_CLOCK_CTL_DBG_LEVEL4_MUX_SEL_BMSK                         0x8
#define HWIO_MSS_DEBUG_CLOCK_CTL_DBG_LEVEL4_MUX_SEL_SHFT                         0x3
#define HWIO_MSS_DEBUG_CLOCK_CTL_DBG_LEVEL3_MUX_SEL_BMSK                         0x4
#define HWIO_MSS_DEBUG_CLOCK_CTL_DBG_LEVEL3_MUX_SEL_SHFT                         0x2
#define HWIO_MSS_DEBUG_CLOCK_CTL_DBG_LEVEL2_MUX_SEL_BMSK                         0x2
#define HWIO_MSS_DEBUG_CLOCK_CTL_DBG_LEVEL2_MUX_SEL_SHFT                         0x1
#define HWIO_MSS_DEBUG_CLOCK_CTL_DBG_LEVEL1_MUX_SEL_BMSK                         0x1
#define HWIO_MSS_DEBUG_CLOCK_CTL_DBG_LEVEL1_MUX_SEL_SHFT                         0x0

#define HWIO_MSS_BBRX_EXT_CLOCK_MUX_SEL_ADDR                              (MSS_PERPH_REG_BASE      + 0x0000007c)
#define HWIO_MSS_BBRX_EXT_CLOCK_MUX_SEL_RMSK                                     0x3
#define HWIO_MSS_BBRX_EXT_CLOCK_MUX_SEL_IN          \
        in_dword_masked(HWIO_MSS_BBRX_EXT_CLOCK_MUX_SEL_ADDR, HWIO_MSS_BBRX_EXT_CLOCK_MUX_SEL_RMSK)
#define HWIO_MSS_BBRX_EXT_CLOCK_MUX_SEL_INM(m)      \
        in_dword_masked(HWIO_MSS_BBRX_EXT_CLOCK_MUX_SEL_ADDR, m)
#define HWIO_MSS_BBRX_EXT_CLOCK_MUX_SEL_OUT(v)      \
        out_dword(HWIO_MSS_BBRX_EXT_CLOCK_MUX_SEL_ADDR,v)
#define HWIO_MSS_BBRX_EXT_CLOCK_MUX_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BBRX_EXT_CLOCK_MUX_SEL_ADDR,m,v,HWIO_MSS_BBRX_EXT_CLOCK_MUX_SEL_IN)
#define HWIO_MSS_BBRX_EXT_CLOCK_MUX_SEL_PLLTEST_BMSK                             0x2
#define HWIO_MSS_BBRX_EXT_CLOCK_MUX_SEL_PLLTEST_SHFT                             0x1
#define HWIO_MSS_BBRX_EXT_CLOCK_MUX_SEL_EXT_CLOCK_MUX_SEL_BMSK                   0x1
#define HWIO_MSS_BBRX_EXT_CLOCK_MUX_SEL_EXT_CLOCK_MUX_SEL_SHFT                   0x0

#define HWIO_MSS_DSM_PHASE_HIGHZ_ENABLE_ADDR                              (MSS_PERPH_REG_BASE      + 0x00000080)
#define HWIO_MSS_DSM_PHASE_HIGHZ_ENABLE_RMSK                                     0x1
#define HWIO_MSS_DSM_PHASE_HIGHZ_ENABLE_IN          \
        in_dword_masked(HWIO_MSS_DSM_PHASE_HIGHZ_ENABLE_ADDR, HWIO_MSS_DSM_PHASE_HIGHZ_ENABLE_RMSK)
#define HWIO_MSS_DSM_PHASE_HIGHZ_ENABLE_INM(m)      \
        in_dword_masked(HWIO_MSS_DSM_PHASE_HIGHZ_ENABLE_ADDR, m)
#define HWIO_MSS_DSM_PHASE_HIGHZ_ENABLE_OUT(v)      \
        out_dword(HWIO_MSS_DSM_PHASE_HIGHZ_ENABLE_ADDR,v)
#define HWIO_MSS_DSM_PHASE_HIGHZ_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_DSM_PHASE_HIGHZ_ENABLE_ADDR,m,v,HWIO_MSS_DSM_PHASE_HIGHZ_ENABLE_IN)
#define HWIO_MSS_DSM_PHASE_HIGHZ_ENABLE_ENABLE_BMSK                              0x1
#define HWIO_MSS_DSM_PHASE_HIGHZ_ENABLE_ENABLE_SHFT                              0x0

#define HWIO_MSS_GNSSADC_BIST_CONFIG_ADDR                                 (MSS_PERPH_REG_BASE      + 0x00000084)
#define HWIO_MSS_GNSSADC_BIST_CONFIG_RMSK                                       0x7f
#define HWIO_MSS_GNSSADC_BIST_CONFIG_IN          \
        in_dword_masked(HWIO_MSS_GNSSADC_BIST_CONFIG_ADDR, HWIO_MSS_GNSSADC_BIST_CONFIG_RMSK)
#define HWIO_MSS_GNSSADC_BIST_CONFIG_INM(m)      \
        in_dword_masked(HWIO_MSS_GNSSADC_BIST_CONFIG_ADDR, m)
#define HWIO_MSS_GNSSADC_BIST_CONFIG_OUT(v)      \
        out_dword(HWIO_MSS_GNSSADC_BIST_CONFIG_ADDR,v)
#define HWIO_MSS_GNSSADC_BIST_CONFIG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_GNSSADC_BIST_CONFIG_ADDR,m,v,HWIO_MSS_GNSSADC_BIST_CONFIG_IN)
#define HWIO_MSS_GNSSADC_BIST_CONFIG_GNSSADC_EXT_CLK_SEL_BMSK                   0x40
#define HWIO_MSS_GNSSADC_BIST_CONFIG_GNSSADC_EXT_CLK_SEL_SHFT                    0x6
#define HWIO_MSS_GNSSADC_BIST_CONFIG_GNSSADC_BIST_EN_BMSK                       0x20
#define HWIO_MSS_GNSSADC_BIST_CONFIG_GNSSADC_BIST_EN_SHFT                        0x5
#define HWIO_MSS_GNSSADC_BIST_CONFIG_HITSIDEAL_BMSK                             0x1f
#define HWIO_MSS_GNSSADC_BIST_CONFIG_HITSIDEAL_SHFT                              0x0

#define HWIO_MSS_GNSSADC_BIST_STATUS_1_ADDR                               (MSS_PERPH_REG_BASE      + 0x00000088)
#define HWIO_MSS_GNSSADC_BIST_STATUS_1_RMSK                                0xfffffff
#define HWIO_MSS_GNSSADC_BIST_STATUS_1_IN          \
        in_dword_masked(HWIO_MSS_GNSSADC_BIST_STATUS_1_ADDR, HWIO_MSS_GNSSADC_BIST_STATUS_1_RMSK)
#define HWIO_MSS_GNSSADC_BIST_STATUS_1_INM(m)      \
        in_dword_masked(HWIO_MSS_GNSSADC_BIST_STATUS_1_ADDR, m)
#define HWIO_MSS_GNSSADC_BIST_STATUS_1_GNSSADC_INL_I_MAX_BMSK              0xff80000
#define HWIO_MSS_GNSSADC_BIST_STATUS_1_GNSSADC_INL_I_MAX_SHFT                   0x13
#define HWIO_MSS_GNSSADC_BIST_STATUS_1_GNSSADC_INL_I_MIN_BMSK                0x7fc00
#define HWIO_MSS_GNSSADC_BIST_STATUS_1_GNSSADC_INL_I_MIN_SHFT                    0xa
#define HWIO_MSS_GNSSADC_BIST_STATUS_1_GNSSADC_DNL_I_POS_BMSK                  0x3e0
#define HWIO_MSS_GNSSADC_BIST_STATUS_1_GNSSADC_DNL_I_POS_SHFT                    0x5
#define HWIO_MSS_GNSSADC_BIST_STATUS_1_GNSSADC_DNL_I_NEG_BMSK                   0x1f
#define HWIO_MSS_GNSSADC_BIST_STATUS_1_GNSSADC_DNL_I_NEG_SHFT                    0x0

#define HWIO_MSS_GNSSADC_BIST_STATUS_2_ADDR                               (MSS_PERPH_REG_BASE      + 0x0000008c)
#define HWIO_MSS_GNSSADC_BIST_STATUS_2_RMSK                               0xffffffff
#define HWIO_MSS_GNSSADC_BIST_STATUS_2_IN          \
        in_dword_masked(HWIO_MSS_GNSSADC_BIST_STATUS_2_ADDR, HWIO_MSS_GNSSADC_BIST_STATUS_2_RMSK)
#define HWIO_MSS_GNSSADC_BIST_STATUS_2_INM(m)      \
        in_dword_masked(HWIO_MSS_GNSSADC_BIST_STATUS_2_ADDR, m)
#define HWIO_MSS_GNSSADC_BIST_STATUS_2_GNSSADC_DNL_Q_POS_BMSK             0xf8000000
#define HWIO_MSS_GNSSADC_BIST_STATUS_2_GNSSADC_DNL_Q_POS_SHFT                   0x1b
#define HWIO_MSS_GNSSADC_BIST_STATUS_2_GNSSADC_GAIN_I_BMSK                 0x7fc0000
#define HWIO_MSS_GNSSADC_BIST_STATUS_2_GNSSADC_GAIN_I_SHFT                      0x12
#define HWIO_MSS_GNSSADC_BIST_STATUS_2_GNSSADC_INL_Q_MAX_BMSK                0x3fe00
#define HWIO_MSS_GNSSADC_BIST_STATUS_2_GNSSADC_INL_Q_MAX_SHFT                    0x9
#define HWIO_MSS_GNSSADC_BIST_STATUS_2_GNSSADC_INL_Q_MIN_BMSK                  0x1ff
#define HWIO_MSS_GNSSADC_BIST_STATUS_2_GNSSADC_INL_Q_MIN_SHFT                    0x0

#define HWIO_MSS_GNSSADC_BIST_STATUS_3_ADDR                               (MSS_PERPH_REG_BASE      + 0x00000090)
#define HWIO_MSS_GNSSADC_BIST_STATUS_3_RMSK                               0xffffffff
#define HWIO_MSS_GNSSADC_BIST_STATUS_3_IN          \
        in_dword_masked(HWIO_MSS_GNSSADC_BIST_STATUS_3_ADDR, HWIO_MSS_GNSSADC_BIST_STATUS_3_RMSK)
#define HWIO_MSS_GNSSADC_BIST_STATUS_3_INM(m)      \
        in_dword_masked(HWIO_MSS_GNSSADC_BIST_STATUS_3_ADDR, m)
#define HWIO_MSS_GNSSADC_BIST_STATUS_3_GNSSADC_DNL_Q_NEG_BMSK             0xf8000000
#define HWIO_MSS_GNSSADC_BIST_STATUS_3_GNSSADC_DNL_Q_NEG_SHFT                   0x1b
#define HWIO_MSS_GNSSADC_BIST_STATUS_3_GNSSADC_OFFSET_I_BMSK               0x7fc0000
#define HWIO_MSS_GNSSADC_BIST_STATUS_3_GNSSADC_OFFSET_I_SHFT                    0x12
#define HWIO_MSS_GNSSADC_BIST_STATUS_3_GNSSADC_GAIN_Q_BMSK                   0x3fe00
#define HWIO_MSS_GNSSADC_BIST_STATUS_3_GNSSADC_GAIN_Q_SHFT                       0x9
#define HWIO_MSS_GNSSADC_BIST_STATUS_3_GNSSADC_OFFSET_Q_BMSK                   0x1ff
#define HWIO_MSS_GNSSADC_BIST_STATUS_3_GNSSADC_OFFSET_Q_SHFT                     0x0

#define HWIO_MSS_BBRX_CTL_ADDR                                            (MSS_PERPH_REG_BASE      + 0x00000094)
#define HWIO_MSS_BBRX_CTL_RMSK                                                   0x3
#define HWIO_MSS_BBRX_CTL_IN          \
        in_dword_masked(HWIO_MSS_BBRX_CTL_ADDR, HWIO_MSS_BBRX_CTL_RMSK)
#define HWIO_MSS_BBRX_CTL_INM(m)      \
        in_dword_masked(HWIO_MSS_BBRX_CTL_ADDR, m)
#define HWIO_MSS_BBRX_CTL_OUT(v)      \
        out_dword(HWIO_MSS_BBRX_CTL_ADDR,v)
#define HWIO_MSS_BBRX_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BBRX_CTL_ADDR,m,v,HWIO_MSS_BBRX_CTL_IN)
#define HWIO_MSS_BBRX_CTL_BBRX_HS_TEST_MUX_CTL_BMSK                              0x3
#define HWIO_MSS_BBRX_CTL_BBRX_HS_TEST_MUX_CTL_SHFT                              0x0

#define HWIO_MSS_DEBUG_CTL_ADDR                                           (MSS_PERPH_REG_BASE      + 0x00000098)
#define HWIO_MSS_DEBUG_CTL_RMSK                                                  0x7
#define HWIO_MSS_DEBUG_CTL_IN          \
        in_dword_masked(HWIO_MSS_DEBUG_CTL_ADDR, HWIO_MSS_DEBUG_CTL_RMSK)
#define HWIO_MSS_DEBUG_CTL_INM(m)      \
        in_dword_masked(HWIO_MSS_DEBUG_CTL_ADDR, m)
#define HWIO_MSS_DEBUG_CTL_OUT(v)      \
        out_dword(HWIO_MSS_DEBUG_CTL_ADDR,v)
#define HWIO_MSS_DEBUG_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_DEBUG_CTL_ADDR,m,v,HWIO_MSS_DEBUG_CTL_IN)
#define HWIO_MSS_DEBUG_CTL_DAC_DISABLE_ON_Q6_DBG_BMSK                            0x4
#define HWIO_MSS_DEBUG_CTL_DAC_DISABLE_ON_Q6_DBG_SHFT                            0x2
#define HWIO_MSS_DEBUG_CTL_GRFC_DISABLE_Q6_DBG_BMSK                              0x2
#define HWIO_MSS_DEBUG_CTL_GRFC_DISABLE_Q6_DBG_SHFT                              0x1
#define HWIO_MSS_DEBUG_CTL_GRFC_DISABLE_Q6_WDOG_BMSK                             0x1
#define HWIO_MSS_DEBUG_CTL_GRFC_DISABLE_Q6_WDOG_SHFT                             0x0

#define HWIO_MSS_MPLL0_MODE_ADDR                                          (MSS_PERPH_REG_BASE      + 0x00001000)
#define HWIO_MSS_MPLL0_MODE_RMSK                                            0x3fff3f
#define HWIO_MSS_MPLL0_MODE_IN          \
        in_dword_masked(HWIO_MSS_MPLL0_MODE_ADDR, HWIO_MSS_MPLL0_MODE_RMSK)
#define HWIO_MSS_MPLL0_MODE_INM(m)      \
        in_dword_masked(HWIO_MSS_MPLL0_MODE_ADDR, m)
#define HWIO_MSS_MPLL0_MODE_OUT(v)      \
        out_dword(HWIO_MSS_MPLL0_MODE_ADDR,v)
#define HWIO_MSS_MPLL0_MODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_MPLL0_MODE_ADDR,m,v,HWIO_MSS_MPLL0_MODE_IN)
#define HWIO_MSS_MPLL0_MODE_PLL_VOTE_FSM_RESET_BMSK                         0x200000
#define HWIO_MSS_MPLL0_MODE_PLL_VOTE_FSM_RESET_SHFT                             0x15
#define HWIO_MSS_MPLL0_MODE_PLL_VOTE_FSM_ENA_BMSK                           0x100000
#define HWIO_MSS_MPLL0_MODE_PLL_VOTE_FSM_ENA_SHFT                               0x14
#define HWIO_MSS_MPLL0_MODE_PLL_BIAS_COUNT_BMSK                              0xfc000
#define HWIO_MSS_MPLL0_MODE_PLL_BIAS_COUNT_SHFT                                  0xe
#define HWIO_MSS_MPLL0_MODE_PLL_LOCK_COUNT_BMSK                               0x3f00
#define HWIO_MSS_MPLL0_MODE_PLL_LOCK_COUNT_SHFT                                  0x8
#define HWIO_MSS_MPLL0_MODE_PLL_REF_XO_SEL_BMSK                                 0x30
#define HWIO_MSS_MPLL0_MODE_PLL_REF_XO_SEL_SHFT                                  0x4
#define HWIO_MSS_MPLL0_MODE_PLL_PLLTEST_BMSK                                     0x8
#define HWIO_MSS_MPLL0_MODE_PLL_PLLTEST_SHFT                                     0x3
#define HWIO_MSS_MPLL0_MODE_PLL_RESET_N_BMSK                                     0x4
#define HWIO_MSS_MPLL0_MODE_PLL_RESET_N_SHFT                                     0x2
#define HWIO_MSS_MPLL0_MODE_PLL_BYPASSNL_BMSK                                    0x2
#define HWIO_MSS_MPLL0_MODE_PLL_BYPASSNL_SHFT                                    0x1
#define HWIO_MSS_MPLL0_MODE_PLL_OUTCTRL_BMSK                                     0x1
#define HWIO_MSS_MPLL0_MODE_PLL_OUTCTRL_SHFT                                     0x0

#define HWIO_MSS_MPLL0_L_VAL_ADDR                                         (MSS_PERPH_REG_BASE      + 0x00001004)
#define HWIO_MSS_MPLL0_L_VAL_RMSK                                               0xff
#define HWIO_MSS_MPLL0_L_VAL_IN          \
        in_dword_masked(HWIO_MSS_MPLL0_L_VAL_ADDR, HWIO_MSS_MPLL0_L_VAL_RMSK)
#define HWIO_MSS_MPLL0_L_VAL_INM(m)      \
        in_dword_masked(HWIO_MSS_MPLL0_L_VAL_ADDR, m)
#define HWIO_MSS_MPLL0_L_VAL_OUT(v)      \
        out_dword(HWIO_MSS_MPLL0_L_VAL_ADDR,v)
#define HWIO_MSS_MPLL0_L_VAL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_MPLL0_L_VAL_ADDR,m,v,HWIO_MSS_MPLL0_L_VAL_IN)
#define HWIO_MSS_MPLL0_L_VAL_PLL_L_BMSK                                         0xff
#define HWIO_MSS_MPLL0_L_VAL_PLL_L_SHFT                                          0x0

#define HWIO_MSS_MPLL0_M_VAL_ADDR                                         (MSS_PERPH_REG_BASE      + 0x00001008)
#define HWIO_MSS_MPLL0_M_VAL_RMSK                                            0x7ffff
#define HWIO_MSS_MPLL0_M_VAL_IN          \
        in_dword_masked(HWIO_MSS_MPLL0_M_VAL_ADDR, HWIO_MSS_MPLL0_M_VAL_RMSK)
#define HWIO_MSS_MPLL0_M_VAL_INM(m)      \
        in_dword_masked(HWIO_MSS_MPLL0_M_VAL_ADDR, m)
#define HWIO_MSS_MPLL0_M_VAL_OUT(v)      \
        out_dword(HWIO_MSS_MPLL0_M_VAL_ADDR,v)
#define HWIO_MSS_MPLL0_M_VAL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_MPLL0_M_VAL_ADDR,m,v,HWIO_MSS_MPLL0_M_VAL_IN)
#define HWIO_MSS_MPLL0_M_VAL_PLL_M_BMSK                                      0x7ffff
#define HWIO_MSS_MPLL0_M_VAL_PLL_M_SHFT                                          0x0

#define HWIO_MSS_MPLL0_N_VAL_ADDR                                         (MSS_PERPH_REG_BASE      + 0x0000100c)
#define HWIO_MSS_MPLL0_N_VAL_RMSK                                            0x7ffff
#define HWIO_MSS_MPLL0_N_VAL_IN          \
        in_dword_masked(HWIO_MSS_MPLL0_N_VAL_ADDR, HWIO_MSS_MPLL0_N_VAL_RMSK)
#define HWIO_MSS_MPLL0_N_VAL_INM(m)      \
        in_dword_masked(HWIO_MSS_MPLL0_N_VAL_ADDR, m)
#define HWIO_MSS_MPLL0_N_VAL_OUT(v)      \
        out_dword(HWIO_MSS_MPLL0_N_VAL_ADDR,v)
#define HWIO_MSS_MPLL0_N_VAL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_MPLL0_N_VAL_ADDR,m,v,HWIO_MSS_MPLL0_N_VAL_IN)
#define HWIO_MSS_MPLL0_N_VAL_PLL_N_BMSK                                      0x7ffff
#define HWIO_MSS_MPLL0_N_VAL_PLL_N_SHFT                                          0x0

#define HWIO_MSS_MPLL0_USER_CTL_ADDR                                      (MSS_PERPH_REG_BASE      + 0x00001010)
#define HWIO_MSS_MPLL0_USER_CTL_RMSK                                      0xff3073ff
#define HWIO_MSS_MPLL0_USER_CTL_IN          \
        in_dword_masked(HWIO_MSS_MPLL0_USER_CTL_ADDR, HWIO_MSS_MPLL0_USER_CTL_RMSK)
#define HWIO_MSS_MPLL0_USER_CTL_INM(m)      \
        in_dword_masked(HWIO_MSS_MPLL0_USER_CTL_ADDR, m)
#define HWIO_MSS_MPLL0_USER_CTL_OUT(v)      \
        out_dword(HWIO_MSS_MPLL0_USER_CTL_ADDR,v)
#define HWIO_MSS_MPLL0_USER_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_MPLL0_USER_CTL_ADDR,m,v,HWIO_MSS_MPLL0_USER_CTL_IN)
#define HWIO_MSS_MPLL0_USER_CTL_RESERVE_31_25_BMSK                        0xfe000000
#define HWIO_MSS_MPLL0_USER_CTL_RESERVE_31_25_SHFT                              0x19
#define HWIO_MSS_MPLL0_USER_CTL_MN_EN_BMSK                                 0x1000000
#define HWIO_MSS_MPLL0_USER_CTL_MN_EN_SHFT                                      0x18
#define HWIO_MSS_MPLL0_USER_CTL_VCO_SEL_BMSK                                0x300000
#define HWIO_MSS_MPLL0_USER_CTL_VCO_SEL_SHFT                                    0x14
#define HWIO_MSS_MPLL0_USER_CTL_PRE_DIV_RATIO_BMSK                            0x7000
#define HWIO_MSS_MPLL0_USER_CTL_PRE_DIV_RATIO_SHFT                               0xc
#define HWIO_MSS_MPLL0_USER_CTL_POST_DIV_RATIO_BMSK                            0x300
#define HWIO_MSS_MPLL0_USER_CTL_POST_DIV_RATIO_SHFT                              0x8
#define HWIO_MSS_MPLL0_USER_CTL_OUTPUT_INV_BMSK                                 0x80
#define HWIO_MSS_MPLL0_USER_CTL_OUTPUT_INV_SHFT                                  0x7
#define HWIO_MSS_MPLL0_USER_CTL_PLLOUT_DIFF_90_BMSK                             0x40
#define HWIO_MSS_MPLL0_USER_CTL_PLLOUT_DIFF_90_SHFT                              0x6
#define HWIO_MSS_MPLL0_USER_CTL_PLLOUT_DIFF_0_BMSK                              0x20
#define HWIO_MSS_MPLL0_USER_CTL_PLLOUT_DIFF_0_SHFT                               0x5
#define HWIO_MSS_MPLL0_USER_CTL_PLLOUT_LV_TEST_BMSK                             0x10
#define HWIO_MSS_MPLL0_USER_CTL_PLLOUT_LV_TEST_SHFT                              0x4
#define HWIO_MSS_MPLL0_USER_CTL_PLLOUT_LV_EARLY_BMSK                             0x8
#define HWIO_MSS_MPLL0_USER_CTL_PLLOUT_LV_EARLY_SHFT                             0x3
#define HWIO_MSS_MPLL0_USER_CTL_PLLOUT_LV_AUX2_BMSK                              0x4
#define HWIO_MSS_MPLL0_USER_CTL_PLLOUT_LV_AUX2_SHFT                              0x2
#define HWIO_MSS_MPLL0_USER_CTL_PLLOUT_LV_AUX_BMSK                               0x2
#define HWIO_MSS_MPLL0_USER_CTL_PLLOUT_LV_AUX_SHFT                               0x1
#define HWIO_MSS_MPLL0_USER_CTL_PLLOUT_LV_MAIN_BMSK                              0x1
#define HWIO_MSS_MPLL0_USER_CTL_PLLOUT_LV_MAIN_SHFT                              0x0

#define HWIO_MSS_MPLL0_CONFIG_CTL_ADDR                                    (MSS_PERPH_REG_BASE      + 0x00001014)
#define HWIO_MSS_MPLL0_CONFIG_CTL_RMSK                                    0xfffcfff0
#define HWIO_MSS_MPLL0_CONFIG_CTL_IN          \
        in_dword_masked(HWIO_MSS_MPLL0_CONFIG_CTL_ADDR, HWIO_MSS_MPLL0_CONFIG_CTL_RMSK)
#define HWIO_MSS_MPLL0_CONFIG_CTL_INM(m)      \
        in_dword_masked(HWIO_MSS_MPLL0_CONFIG_CTL_ADDR, m)
#define HWIO_MSS_MPLL0_CONFIG_CTL_OUT(v)      \
        out_dword(HWIO_MSS_MPLL0_CONFIG_CTL_ADDR,v)
#define HWIO_MSS_MPLL0_CONFIG_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_MPLL0_CONFIG_CTL_ADDR,m,v,HWIO_MSS_MPLL0_CONFIG_CTL_IN)
#define HWIO_MSS_MPLL0_CONFIG_CTL_RESERVE_31_24_BMSK                      0xff000000
#define HWIO_MSS_MPLL0_CONFIG_CTL_RESERVE_31_24_SHFT                            0x18
#define HWIO_MSS_MPLL0_CONFIG_CTL_PFD_DZSEL_BMSK                            0xc00000
#define HWIO_MSS_MPLL0_CONFIG_CTL_PFD_DZSEL_SHFT                                0x16
#define HWIO_MSS_MPLL0_CONFIG_CTL_ICP_DIV_BMSK                              0x300000
#define HWIO_MSS_MPLL0_CONFIG_CTL_ICP_DIV_SHFT                                  0x14
#define HWIO_MSS_MPLL0_CONFIG_CTL_IREG_DIV_BMSK                              0xc0000
#define HWIO_MSS_MPLL0_CONFIG_CTL_IREG_DIV_SHFT                                 0x12
#define HWIO_MSS_MPLL0_CONFIG_CTL_VREG_REF_MODE_BMSK                          0x8000
#define HWIO_MSS_MPLL0_CONFIG_CTL_VREG_REF_MODE_SHFT                             0xf
#define HWIO_MSS_MPLL0_CONFIG_CTL_VCO_REF_MODE_BMSK                           0x4000
#define HWIO_MSS_MPLL0_CONFIG_CTL_VCO_REF_MODE_SHFT                              0xe
#define HWIO_MSS_MPLL0_CONFIG_CTL_CFG_LOCKDET_BMSK                            0x3000
#define HWIO_MSS_MPLL0_CONFIG_CTL_CFG_LOCKDET_SHFT                               0xc
#define HWIO_MSS_MPLL0_CONFIG_CTL_FORCE_ISEED_BMSK                             0x800
#define HWIO_MSS_MPLL0_CONFIG_CTL_FORCE_ISEED_SHFT                               0xb
#define HWIO_MSS_MPLL0_CONFIG_CTL_COARSE_GM_BMSK                               0x600
#define HWIO_MSS_MPLL0_CONFIG_CTL_COARSE_GM_SHFT                                 0x9
#define HWIO_MSS_MPLL0_CONFIG_CTL_FILTER_LOOP_ZERO_BMSK                        0x100
#define HWIO_MSS_MPLL0_CONFIG_CTL_FILTER_LOOP_ZERO_SHFT                          0x8
#define HWIO_MSS_MPLL0_CONFIG_CTL_FILTER_LOOP_MODE_BMSK                         0x80
#define HWIO_MSS_MPLL0_CONFIG_CTL_FILTER_LOOP_MODE_SHFT                          0x7
#define HWIO_MSS_MPLL0_CONFIG_CTL_FILTER_RESISTOR_BMSK                          0x60
#define HWIO_MSS_MPLL0_CONFIG_CTL_FILTER_RESISTOR_SHFT                           0x5
#define HWIO_MSS_MPLL0_CONFIG_CTL_GMC_SLEW_MODE_BMSK                            0x10
#define HWIO_MSS_MPLL0_CONFIG_CTL_GMC_SLEW_MODE_SHFT                             0x4

#define HWIO_MSS_MPLL0_TEST_CTL_ADDR                                      (MSS_PERPH_REG_BASE      + 0x00001018)
#define HWIO_MSS_MPLL0_TEST_CTL_RMSK                                      0xffffffff
#define HWIO_MSS_MPLL0_TEST_CTL_IN          \
        in_dword_masked(HWIO_MSS_MPLL0_TEST_CTL_ADDR, HWIO_MSS_MPLL0_TEST_CTL_RMSK)
#define HWIO_MSS_MPLL0_TEST_CTL_INM(m)      \
        in_dword_masked(HWIO_MSS_MPLL0_TEST_CTL_ADDR, m)
#define HWIO_MSS_MPLL0_TEST_CTL_OUT(v)      \
        out_dword(HWIO_MSS_MPLL0_TEST_CTL_ADDR,v)
#define HWIO_MSS_MPLL0_TEST_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_MPLL0_TEST_CTL_ADDR,m,v,HWIO_MSS_MPLL0_TEST_CTL_IN)
#define HWIO_MSS_MPLL0_TEST_CTL_RESERVE_31_21_BMSK                        0xffe00000
#define HWIO_MSS_MPLL0_TEST_CTL_RESERVE_31_21_SHFT                              0x15
#define HWIO_MSS_MPLL0_TEST_CTL_NGEN_CFG_BMSK                               0x1c0000
#define HWIO_MSS_MPLL0_TEST_CTL_NGEN_CFG_SHFT                                   0x12
#define HWIO_MSS_MPLL0_TEST_CTL_NGEN_EN_BMSK                                 0x20000
#define HWIO_MSS_MPLL0_TEST_CTL_NGEN_EN_SHFT                                    0x11
#define HWIO_MSS_MPLL0_TEST_CTL_NMOSC_FREQ_CTRL_BMSK                         0x18000
#define HWIO_MSS_MPLL0_TEST_CTL_NMOSC_FREQ_CTRL_SHFT                             0xf
#define HWIO_MSS_MPLL0_TEST_CTL_NMOSC_EN_BMSK                                 0x4000
#define HWIO_MSS_MPLL0_TEST_CTL_NMOSC_EN_SHFT                                    0xe
#define HWIO_MSS_MPLL0_TEST_CTL_FORCE_PFD_UP_BMSK                             0x2000
#define HWIO_MSS_MPLL0_TEST_CTL_FORCE_PFD_UP_SHFT                                0xd
#define HWIO_MSS_MPLL0_TEST_CTL_FORCE_PFD_DOWN_BMSK                           0x1000
#define HWIO_MSS_MPLL0_TEST_CTL_FORCE_PFD_DOWN_SHFT                              0xc
#define HWIO_MSS_MPLL0_TEST_CTL_TEST_OUT_SEL_BMSK                              0x800
#define HWIO_MSS_MPLL0_TEST_CTL_TEST_OUT_SEL_SHFT                                0xb
#define HWIO_MSS_MPLL0_TEST_CTL_ICP_TST_EN_BMSK                                0x400
#define HWIO_MSS_MPLL0_TEST_CTL_ICP_TST_EN_SHFT                                  0xa
#define HWIO_MSS_MPLL0_TEST_CTL_ICP_EXT_SEL_BMSK                               0x200
#define HWIO_MSS_MPLL0_TEST_CTL_ICP_EXT_SEL_SHFT                                 0x9
#define HWIO_MSS_MPLL0_TEST_CTL_DTEST_SEL_BMSK                                 0x180
#define HWIO_MSS_MPLL0_TEST_CTL_DTEST_SEL_SHFT                                   0x7
#define HWIO_MSS_MPLL0_TEST_CTL_BYP_TESTAMP_BMSK                                0x40
#define HWIO_MSS_MPLL0_TEST_CTL_BYP_TESTAMP_SHFT                                 0x6
#define HWIO_MSS_MPLL0_TEST_CTL_ATEST1_SEL_BMSK                                 0x30
#define HWIO_MSS_MPLL0_TEST_CTL_ATEST1_SEL_SHFT                                  0x4
#define HWIO_MSS_MPLL0_TEST_CTL_ATEST0_SEL_BMSK                                  0xc
#define HWIO_MSS_MPLL0_TEST_CTL_ATEST0_SEL_SHFT                                  0x2
#define HWIO_MSS_MPLL0_TEST_CTL_ATEST1_EN_BMSK                                   0x2
#define HWIO_MSS_MPLL0_TEST_CTL_ATEST1_EN_SHFT                                   0x1
#define HWIO_MSS_MPLL0_TEST_CTL_ATEST0_EN_BMSK                                   0x1
#define HWIO_MSS_MPLL0_TEST_CTL_ATEST0_EN_SHFT                                   0x0

#define HWIO_MSS_MPLL0_STATUS_ADDR                                        (MSS_PERPH_REG_BASE      + 0x0000101c)
#define HWIO_MSS_MPLL0_STATUS_RMSK                                           0x3ffff
#define HWIO_MSS_MPLL0_STATUS_IN          \
        in_dword_masked(HWIO_MSS_MPLL0_STATUS_ADDR, HWIO_MSS_MPLL0_STATUS_RMSK)
#define HWIO_MSS_MPLL0_STATUS_INM(m)      \
        in_dword_masked(HWIO_MSS_MPLL0_STATUS_ADDR, m)
#define HWIO_MSS_MPLL0_STATUS_PLL_ACTIVE_FLAG_BMSK                           0x20000
#define HWIO_MSS_MPLL0_STATUS_PLL_ACTIVE_FLAG_SHFT                              0x11
#define HWIO_MSS_MPLL0_STATUS_PLL_LOCK_DET_BMSK                              0x10000
#define HWIO_MSS_MPLL0_STATUS_PLL_LOCK_DET_SHFT                                 0x10
#define HWIO_MSS_MPLL0_STATUS_PLL_D_BMSK                                      0xffff
#define HWIO_MSS_MPLL0_STATUS_PLL_D_SHFT                                         0x0

#define HWIO_MSS_MPLL1_MODE_ADDR                                          (MSS_PERPH_REG_BASE      + 0x00001020)
#define HWIO_MSS_MPLL1_MODE_RMSK                                            0x3fff3f
#define HWIO_MSS_MPLL1_MODE_IN          \
        in_dword_masked(HWIO_MSS_MPLL1_MODE_ADDR, HWIO_MSS_MPLL1_MODE_RMSK)
#define HWIO_MSS_MPLL1_MODE_INM(m)      \
        in_dword_masked(HWIO_MSS_MPLL1_MODE_ADDR, m)
#define HWIO_MSS_MPLL1_MODE_OUT(v)      \
        out_dword(HWIO_MSS_MPLL1_MODE_ADDR,v)
#define HWIO_MSS_MPLL1_MODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_MPLL1_MODE_ADDR,m,v,HWIO_MSS_MPLL1_MODE_IN)
#define HWIO_MSS_MPLL1_MODE_PLL_VOTE_FSM_RESET_BMSK                         0x200000
#define HWIO_MSS_MPLL1_MODE_PLL_VOTE_FSM_RESET_SHFT                             0x15
#define HWIO_MSS_MPLL1_MODE_PLL_VOTE_FSM_ENA_BMSK                           0x100000
#define HWIO_MSS_MPLL1_MODE_PLL_VOTE_FSM_ENA_SHFT                               0x14
#define HWIO_MSS_MPLL1_MODE_PLL_BIAS_COUNT_BMSK                              0xfc000
#define HWIO_MSS_MPLL1_MODE_PLL_BIAS_COUNT_SHFT                                  0xe
#define HWIO_MSS_MPLL1_MODE_PLL_LOCK_COUNT_BMSK                               0x3f00
#define HWIO_MSS_MPLL1_MODE_PLL_LOCK_COUNT_SHFT                                  0x8
#define HWIO_MSS_MPLL1_MODE_PLL_REF_XO_SEL_BMSK                                 0x30
#define HWIO_MSS_MPLL1_MODE_PLL_REF_XO_SEL_SHFT                                  0x4
#define HWIO_MSS_MPLL1_MODE_PLL_PLLTEST_BMSK                                     0x8
#define HWIO_MSS_MPLL1_MODE_PLL_PLLTEST_SHFT                                     0x3
#define HWIO_MSS_MPLL1_MODE_PLL_RESET_N_BMSK                                     0x4
#define HWIO_MSS_MPLL1_MODE_PLL_RESET_N_SHFT                                     0x2
#define HWIO_MSS_MPLL1_MODE_PLL_BYPASSNL_BMSK                                    0x2
#define HWIO_MSS_MPLL1_MODE_PLL_BYPASSNL_SHFT                                    0x1
#define HWIO_MSS_MPLL1_MODE_PLL_OUTCTRL_BMSK                                     0x1
#define HWIO_MSS_MPLL1_MODE_PLL_OUTCTRL_SHFT                                     0x0

#define HWIO_MSS_MPLL1_L_VAL_ADDR                                         (MSS_PERPH_REG_BASE      + 0x00001024)
#define HWIO_MSS_MPLL1_L_VAL_RMSK                                               0x7f
#define HWIO_MSS_MPLL1_L_VAL_IN          \
        in_dword_masked(HWIO_MSS_MPLL1_L_VAL_ADDR, HWIO_MSS_MPLL1_L_VAL_RMSK)
#define HWIO_MSS_MPLL1_L_VAL_INM(m)      \
        in_dword_masked(HWIO_MSS_MPLL1_L_VAL_ADDR, m)
#define HWIO_MSS_MPLL1_L_VAL_OUT(v)      \
        out_dword(HWIO_MSS_MPLL1_L_VAL_ADDR,v)
#define HWIO_MSS_MPLL1_L_VAL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_MPLL1_L_VAL_ADDR,m,v,HWIO_MSS_MPLL1_L_VAL_IN)
#define HWIO_MSS_MPLL1_L_VAL_PLL_L_BMSK                                         0x7f
#define HWIO_MSS_MPLL1_L_VAL_PLL_L_SHFT                                          0x0

#define HWIO_MSS_MPLL1_M_VAL_ADDR                                         (MSS_PERPH_REG_BASE      + 0x00001028)
#define HWIO_MSS_MPLL1_M_VAL_RMSK                                            0x7ffff
#define HWIO_MSS_MPLL1_M_VAL_IN          \
        in_dword_masked(HWIO_MSS_MPLL1_M_VAL_ADDR, HWIO_MSS_MPLL1_M_VAL_RMSK)
#define HWIO_MSS_MPLL1_M_VAL_INM(m)      \
        in_dword_masked(HWIO_MSS_MPLL1_M_VAL_ADDR, m)
#define HWIO_MSS_MPLL1_M_VAL_OUT(v)      \
        out_dword(HWIO_MSS_MPLL1_M_VAL_ADDR,v)
#define HWIO_MSS_MPLL1_M_VAL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_MPLL1_M_VAL_ADDR,m,v,HWIO_MSS_MPLL1_M_VAL_IN)
#define HWIO_MSS_MPLL1_M_VAL_PLL_M_BMSK                                      0x7ffff
#define HWIO_MSS_MPLL1_M_VAL_PLL_M_SHFT                                          0x0

#define HWIO_MSS_MPLL1_N_VAL_ADDR                                         (MSS_PERPH_REG_BASE      + 0x0000102c)
#define HWIO_MSS_MPLL1_N_VAL_RMSK                                            0x7ffff
#define HWIO_MSS_MPLL1_N_VAL_IN          \
        in_dword_masked(HWIO_MSS_MPLL1_N_VAL_ADDR, HWIO_MSS_MPLL1_N_VAL_RMSK)
#define HWIO_MSS_MPLL1_N_VAL_INM(m)      \
        in_dword_masked(HWIO_MSS_MPLL1_N_VAL_ADDR, m)
#define HWIO_MSS_MPLL1_N_VAL_OUT(v)      \
        out_dword(HWIO_MSS_MPLL1_N_VAL_ADDR,v)
#define HWIO_MSS_MPLL1_N_VAL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_MPLL1_N_VAL_ADDR,m,v,HWIO_MSS_MPLL1_N_VAL_IN)
#define HWIO_MSS_MPLL1_N_VAL_PLL_N_BMSK                                      0x7ffff
#define HWIO_MSS_MPLL1_N_VAL_PLL_N_SHFT                                          0x0

#define HWIO_MSS_MPLL1_USER_CTL_ADDR                                      (MSS_PERPH_REG_BASE      + 0x00001030)
#define HWIO_MSS_MPLL1_USER_CTL_RMSK                                      0xffffffff
#define HWIO_MSS_MPLL1_USER_CTL_IN          \
        in_dword_masked(HWIO_MSS_MPLL1_USER_CTL_ADDR, HWIO_MSS_MPLL1_USER_CTL_RMSK)
#define HWIO_MSS_MPLL1_USER_CTL_INM(m)      \
        in_dword_masked(HWIO_MSS_MPLL1_USER_CTL_ADDR, m)
#define HWIO_MSS_MPLL1_USER_CTL_OUT(v)      \
        out_dword(HWIO_MSS_MPLL1_USER_CTL_ADDR,v)
#define HWIO_MSS_MPLL1_USER_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_MPLL1_USER_CTL_ADDR,m,v,HWIO_MSS_MPLL1_USER_CTL_IN)
#define HWIO_MSS_MPLL1_USER_CTL_ADDITIVE_FACTOR_BMSK                      0xc0000000
#define HWIO_MSS_MPLL1_USER_CTL_ADDITIVE_FACTOR_SHFT                            0x1e
#define HWIO_MSS_MPLL1_USER_CTL_VCO_DOUBLER_QUADRUPLER_MODE_BMSK          0x30000000
#define HWIO_MSS_MPLL1_USER_CTL_VCO_DOUBLER_QUADRUPLER_MODE_SHFT                0x1c
#define HWIO_MSS_MPLL1_USER_CTL_RESERVE_BIT_27_25_BMSK                     0xe000000
#define HWIO_MSS_MPLL1_USER_CTL_RESERVE_BIT_27_25_SHFT                          0x19
#define HWIO_MSS_MPLL1_USER_CTL_MN_EN_BMSK                                 0x1000000
#define HWIO_MSS_MPLL1_USER_CTL_MN_EN_SHFT                                      0x18
#define HWIO_MSS_MPLL1_USER_CTL_RESERVE_BITS_23_13_BMSK                     0xffe000
#define HWIO_MSS_MPLL1_USER_CTL_RESERVE_BITS_23_13_SHFT                          0xd
#define HWIO_MSS_MPLL1_USER_CTL_PREDIV2_EN_BMSK                               0x1000
#define HWIO_MSS_MPLL1_USER_CTL_PREDIV2_EN_SHFT                                  0xc
#define HWIO_MSS_MPLL1_USER_CTL_RESERVE_BITS_11_10_BMSK                        0xc00
#define HWIO_MSS_MPLL1_USER_CTL_RESERVE_BITS_11_10_SHFT                          0xa
#define HWIO_MSS_MPLL1_USER_CTL_POSTDIV_CTL_BMSK                               0x300
#define HWIO_MSS_MPLL1_USER_CTL_POSTDIV_CTL_SHFT                                 0x8
#define HWIO_MSS_MPLL1_USER_CTL_INV_OUTPUT_BMSK                                 0x80
#define HWIO_MSS_MPLL1_USER_CTL_INV_OUTPUT_SHFT                                  0x7
#define HWIO_MSS_MPLL1_USER_CTL_RESERVE_BIT_6_5_BMSK                            0x60
#define HWIO_MSS_MPLL1_USER_CTL_RESERVE_BIT_6_5_SHFT                             0x5
#define HWIO_MSS_MPLL1_USER_CTL_LVTEST_EN_BMSK                                  0x10
#define HWIO_MSS_MPLL1_USER_CTL_LVTEST_EN_SHFT                                   0x4
#define HWIO_MSS_MPLL1_USER_CTL_LVEARLY_EN_BMSK                                  0x8
#define HWIO_MSS_MPLL1_USER_CTL_LVEARLY_EN_SHFT                                  0x3
#define HWIO_MSS_MPLL1_USER_CTL_LVAUX2_EN_BMSK                                   0x4
#define HWIO_MSS_MPLL1_USER_CTL_LVAUX2_EN_SHFT                                   0x2
#define HWIO_MSS_MPLL1_USER_CTL_LVAUX_EN_BMSK                                    0x2
#define HWIO_MSS_MPLL1_USER_CTL_LVAUX_EN_SHFT                                    0x1
#define HWIO_MSS_MPLL1_USER_CTL_LVMAIN_EN_BMSK                                   0x1
#define HWIO_MSS_MPLL1_USER_CTL_LVMAIN_EN_SHFT                                   0x0

#define HWIO_MSS_MPLL1_CONFIG_CTL_ADDR                                    (MSS_PERPH_REG_BASE      + 0x00001034)
#define HWIO_MSS_MPLL1_CONFIG_CTL_RMSK                                    0xffffffff
#define HWIO_MSS_MPLL1_CONFIG_CTL_IN          \
        in_dword_masked(HWIO_MSS_MPLL1_CONFIG_CTL_ADDR, HWIO_MSS_MPLL1_CONFIG_CTL_RMSK)
#define HWIO_MSS_MPLL1_CONFIG_CTL_INM(m)      \
        in_dword_masked(HWIO_MSS_MPLL1_CONFIG_CTL_ADDR, m)
#define HWIO_MSS_MPLL1_CONFIG_CTL_OUT(v)      \
        out_dword(HWIO_MSS_MPLL1_CONFIG_CTL_ADDR,v)
#define HWIO_MSS_MPLL1_CONFIG_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_MPLL1_CONFIG_CTL_ADDR,m,v,HWIO_MSS_MPLL1_CONFIG_CTL_IN)
#define HWIO_MSS_MPLL1_CONFIG_CTL_MULTIPLICATIONFACTOR_BMSK               0xc0000000
#define HWIO_MSS_MPLL1_CONFIG_CTL_MULTIPLICATIONFACTOR_SHFT                     0x1e
#define HWIO_MSS_MPLL1_CONFIG_CTL_RESERVE_BIT_29_27_BMSK                  0x38000000
#define HWIO_MSS_MPLL1_CONFIG_CTL_RESERVE_BIT_29_27_SHFT                        0x1b
#define HWIO_MSS_MPLL1_CONFIG_CTL_OVDREFCFG_BMSK                           0x6000000
#define HWIO_MSS_MPLL1_CONFIG_CTL_OVDREFCFG_SHFT                                0x19
#define HWIO_MSS_MPLL1_CONFIG_CTL_RING_OSC_LATCH_EN_BMSK                   0x1000000
#define HWIO_MSS_MPLL1_CONFIG_CTL_RING_OSC_LATCH_EN_SHFT                        0x18
#define HWIO_MSS_MPLL1_CONFIG_CTL_RESERVE_BIT_23_18_BMSK                    0xfc0000
#define HWIO_MSS_MPLL1_CONFIG_CTL_RESERVE_BIT_23_18_SHFT                        0x12
#define HWIO_MSS_MPLL1_CONFIG_CTL_PDN_OVDCNT_BMSK                            0x20000
#define HWIO_MSS_MPLL1_CONFIG_CTL_PDN_OVDCNT_SHFT                               0x11
#define HWIO_MSS_MPLL1_CONFIG_CTL_CFG_OVDCNT_BMSK                            0x18000
#define HWIO_MSS_MPLL1_CONFIG_CTL_CFG_OVDCNT_SHFT                                0xf
#define HWIO_MSS_MPLL1_CONFIG_CTL_RVSIG_DEL_BMSK                              0x6000
#define HWIO_MSS_MPLL1_CONFIG_CTL_RVSIG_DEL_SHFT                                 0xd
#define HWIO_MSS_MPLL1_CONFIG_CTL_PFD_DZSEL_BMSK                              0x1800
#define HWIO_MSS_MPLL1_CONFIG_CTL_PFD_DZSEL_SHFT                                 0xb
#define HWIO_MSS_MPLL1_CONFIG_CTL_FORCE_ISEED_BMSK                             0x400
#define HWIO_MSS_MPLL1_CONFIG_CTL_FORCE_ISEED_SHFT                               0xa
#define HWIO_MSS_MPLL1_CONFIG_CTL_ICP_SEED_SEL_BMSK                            0x300
#define HWIO_MSS_MPLL1_CONFIG_CTL_ICP_SEED_SEL_SHFT                              0x8
#define HWIO_MSS_MPLL1_CONFIG_CTL_SEL_IREG_REP_BMSK                             0xc0
#define HWIO_MSS_MPLL1_CONFIG_CTL_SEL_IREG_REP_SHFT                              0x6
#define HWIO_MSS_MPLL1_CONFIG_CTL_SEL_IREG_OSC_BMSK                             0x30
#define HWIO_MSS_MPLL1_CONFIG_CTL_SEL_IREG_OSC_SHFT                              0x4
#define HWIO_MSS_MPLL1_CONFIG_CTL_CAPSEL_BMSK                                    0xc
#define HWIO_MSS_MPLL1_CONFIG_CTL_CAPSEL_SHFT                                    0x2
#define HWIO_MSS_MPLL1_CONFIG_CTL_CFG_LOCKDET_BMSK                               0x3
#define HWIO_MSS_MPLL1_CONFIG_CTL_CFG_LOCKDET_SHFT                               0x0

#define HWIO_MSS_MPLL1_TEST_CTL_ADDR                                      (MSS_PERPH_REG_BASE      + 0x00001038)
#define HWIO_MSS_MPLL1_TEST_CTL_RMSK                                      0xffffffff
#define HWIO_MSS_MPLL1_TEST_CTL_IN          \
        in_dword_masked(HWIO_MSS_MPLL1_TEST_CTL_ADDR, HWIO_MSS_MPLL1_TEST_CTL_RMSK)
#define HWIO_MSS_MPLL1_TEST_CTL_INM(m)      \
        in_dword_masked(HWIO_MSS_MPLL1_TEST_CTL_ADDR, m)
#define HWIO_MSS_MPLL1_TEST_CTL_OUT(v)      \
        out_dword(HWIO_MSS_MPLL1_TEST_CTL_ADDR,v)
#define HWIO_MSS_MPLL1_TEST_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_MPLL1_TEST_CTL_ADDR,m,v,HWIO_MSS_MPLL1_TEST_CTL_IN)
#define HWIO_MSS_MPLL1_TEST_CTL_RESERVE_31_10_BMSK                        0xfffffc00
#define HWIO_MSS_MPLL1_TEST_CTL_RESERVE_31_10_SHFT                               0xa
#define HWIO_MSS_MPLL1_TEST_CTL_IEXT_SEL_BMSK                                  0x200
#define HWIO_MSS_MPLL1_TEST_CTL_IEXT_SEL_SHFT                                    0x9
#define HWIO_MSS_MPLL1_TEST_CTL_DTEST_SEL_BMSK                                 0x180
#define HWIO_MSS_MPLL1_TEST_CTL_DTEST_SEL_SHFT                                   0x7
#define HWIO_MSS_MPLL1_TEST_CTL_BYP_TESTAMP_BMSK                                0x40
#define HWIO_MSS_MPLL1_TEST_CTL_BYP_TESTAMP_SHFT                                 0x6
#define HWIO_MSS_MPLL1_TEST_CTL_ATEST1_SEL_BMSK                                 0x30
#define HWIO_MSS_MPLL1_TEST_CTL_ATEST1_SEL_SHFT                                  0x4
#define HWIO_MSS_MPLL1_TEST_CTL_ATEST0_SEL_BMSK                                  0xc
#define HWIO_MSS_MPLL1_TEST_CTL_ATEST0_SEL_SHFT                                  0x2
#define HWIO_MSS_MPLL1_TEST_CTL_ATEST1_EN_BMSK                                   0x2
#define HWIO_MSS_MPLL1_TEST_CTL_ATEST1_EN_SHFT                                   0x1
#define HWIO_MSS_MPLL1_TEST_CTL_ATEST0_EN_BMSK                                   0x1
#define HWIO_MSS_MPLL1_TEST_CTL_ATEST0_EN_SHFT                                   0x0

#define HWIO_MSS_MPLL1_STATUS_ADDR                                        (MSS_PERPH_REG_BASE      + 0x0000103c)
#define HWIO_MSS_MPLL1_STATUS_RMSK                                           0x3ffff
#define HWIO_MSS_MPLL1_STATUS_IN          \
        in_dword_masked(HWIO_MSS_MPLL1_STATUS_ADDR, HWIO_MSS_MPLL1_STATUS_RMSK)
#define HWIO_MSS_MPLL1_STATUS_INM(m)      \
        in_dword_masked(HWIO_MSS_MPLL1_STATUS_ADDR, m)
#define HWIO_MSS_MPLL1_STATUS_PLL_ACTIVE_FLAG_BMSK                           0x20000
#define HWIO_MSS_MPLL1_STATUS_PLL_ACTIVE_FLAG_SHFT                              0x11
#define HWIO_MSS_MPLL1_STATUS_PLL_LOCK_DET_BMSK                              0x10000
#define HWIO_MSS_MPLL1_STATUS_PLL_LOCK_DET_SHFT                                 0x10
#define HWIO_MSS_MPLL1_STATUS_PLL_D_BMSK                                      0xffff
#define HWIO_MSS_MPLL1_STATUS_PLL_D_SHFT                                         0x0

#define HWIO_MSS_MPLL2_MODE_ADDR                                          (MSS_PERPH_REG_BASE      + 0x00001040)
#define HWIO_MSS_MPLL2_MODE_RMSK                                            0x3fff3f
#define HWIO_MSS_MPLL2_MODE_IN          \
        in_dword_masked(HWIO_MSS_MPLL2_MODE_ADDR, HWIO_MSS_MPLL2_MODE_RMSK)
#define HWIO_MSS_MPLL2_MODE_INM(m)      \
        in_dword_masked(HWIO_MSS_MPLL2_MODE_ADDR, m)
#define HWIO_MSS_MPLL2_MODE_OUT(v)      \
        out_dword(HWIO_MSS_MPLL2_MODE_ADDR,v)
#define HWIO_MSS_MPLL2_MODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_MPLL2_MODE_ADDR,m,v,HWIO_MSS_MPLL2_MODE_IN)
#define HWIO_MSS_MPLL2_MODE_PLL_VOTE_FSM_RESET_BMSK                         0x200000
#define HWIO_MSS_MPLL2_MODE_PLL_VOTE_FSM_RESET_SHFT                             0x15
#define HWIO_MSS_MPLL2_MODE_PLL_VOTE_FSM_ENA_BMSK                           0x100000
#define HWIO_MSS_MPLL2_MODE_PLL_VOTE_FSM_ENA_SHFT                               0x14
#define HWIO_MSS_MPLL2_MODE_PLL_BIAS_COUNT_BMSK                              0xfc000
#define HWIO_MSS_MPLL2_MODE_PLL_BIAS_COUNT_SHFT                                  0xe
#define HWIO_MSS_MPLL2_MODE_PLL_LOCK_COUNT_BMSK                               0x3f00
#define HWIO_MSS_MPLL2_MODE_PLL_LOCK_COUNT_SHFT                                  0x8
#define HWIO_MSS_MPLL2_MODE_PLL_REF_XO_SEL_BMSK                                 0x30
#define HWIO_MSS_MPLL2_MODE_PLL_REF_XO_SEL_SHFT                                  0x4
#define HWIO_MSS_MPLL2_MODE_PLL_PLLTEST_BMSK                                     0x8
#define HWIO_MSS_MPLL2_MODE_PLL_PLLTEST_SHFT                                     0x3
#define HWIO_MSS_MPLL2_MODE_PLL_RESET_N_BMSK                                     0x4
#define HWIO_MSS_MPLL2_MODE_PLL_RESET_N_SHFT                                     0x2
#define HWIO_MSS_MPLL2_MODE_PLL_BYPASSNL_BMSK                                    0x2
#define HWIO_MSS_MPLL2_MODE_PLL_BYPASSNL_SHFT                                    0x1
#define HWIO_MSS_MPLL2_MODE_PLL_OUTCTRL_BMSK                                     0x1
#define HWIO_MSS_MPLL2_MODE_PLL_OUTCTRL_SHFT                                     0x0

#define HWIO_MSS_MPLL2_L_VAL_ADDR                                         (MSS_PERPH_REG_BASE      + 0x00001044)
#define HWIO_MSS_MPLL2_L_VAL_RMSK                                               0xff
#define HWIO_MSS_MPLL2_L_VAL_IN          \
        in_dword_masked(HWIO_MSS_MPLL2_L_VAL_ADDR, HWIO_MSS_MPLL2_L_VAL_RMSK)
#define HWIO_MSS_MPLL2_L_VAL_INM(m)      \
        in_dword_masked(HWIO_MSS_MPLL2_L_VAL_ADDR, m)
#define HWIO_MSS_MPLL2_L_VAL_OUT(v)      \
        out_dword(HWIO_MSS_MPLL2_L_VAL_ADDR,v)
#define HWIO_MSS_MPLL2_L_VAL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_MPLL2_L_VAL_ADDR,m,v,HWIO_MSS_MPLL2_L_VAL_IN)
#define HWIO_MSS_MPLL2_L_VAL_PLL_L_BMSK                                         0xff
#define HWIO_MSS_MPLL2_L_VAL_PLL_L_SHFT                                          0x0

#define HWIO_MSS_MPLL2_M_VAL_ADDR                                         (MSS_PERPH_REG_BASE      + 0x00001048)
#define HWIO_MSS_MPLL2_M_VAL_RMSK                                            0x7ffff
#define HWIO_MSS_MPLL2_M_VAL_IN          \
        in_dword_masked(HWIO_MSS_MPLL2_M_VAL_ADDR, HWIO_MSS_MPLL2_M_VAL_RMSK)
#define HWIO_MSS_MPLL2_M_VAL_INM(m)      \
        in_dword_masked(HWIO_MSS_MPLL2_M_VAL_ADDR, m)
#define HWIO_MSS_MPLL2_M_VAL_OUT(v)      \
        out_dword(HWIO_MSS_MPLL2_M_VAL_ADDR,v)
#define HWIO_MSS_MPLL2_M_VAL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_MPLL2_M_VAL_ADDR,m,v,HWIO_MSS_MPLL2_M_VAL_IN)
#define HWIO_MSS_MPLL2_M_VAL_PLL_M_BMSK                                      0x7ffff
#define HWIO_MSS_MPLL2_M_VAL_PLL_M_SHFT                                          0x0

#define HWIO_MSS_MPLL2_N_VAL_ADDR                                         (MSS_PERPH_REG_BASE      + 0x0000104c)
#define HWIO_MSS_MPLL2_N_VAL_RMSK                                            0x7ffff
#define HWIO_MSS_MPLL2_N_VAL_IN          \
        in_dword_masked(HWIO_MSS_MPLL2_N_VAL_ADDR, HWIO_MSS_MPLL2_N_VAL_RMSK)
#define HWIO_MSS_MPLL2_N_VAL_INM(m)      \
        in_dword_masked(HWIO_MSS_MPLL2_N_VAL_ADDR, m)
#define HWIO_MSS_MPLL2_N_VAL_OUT(v)      \
        out_dword(HWIO_MSS_MPLL2_N_VAL_ADDR,v)
#define HWIO_MSS_MPLL2_N_VAL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_MPLL2_N_VAL_ADDR,m,v,HWIO_MSS_MPLL2_N_VAL_IN)
#define HWIO_MSS_MPLL2_N_VAL_PLL_N_BMSK                                      0x7ffff
#define HWIO_MSS_MPLL2_N_VAL_PLL_N_SHFT                                          0x0

#define HWIO_MSS_MPLL2_USER_CTL_ADDR                                      (MSS_PERPH_REG_BASE      + 0x00001050)
#define HWIO_MSS_MPLL2_USER_CTL_RMSK                                      0xff3073ff
#define HWIO_MSS_MPLL2_USER_CTL_IN          \
        in_dword_masked(HWIO_MSS_MPLL2_USER_CTL_ADDR, HWIO_MSS_MPLL2_USER_CTL_RMSK)
#define HWIO_MSS_MPLL2_USER_CTL_INM(m)      \
        in_dword_masked(HWIO_MSS_MPLL2_USER_CTL_ADDR, m)
#define HWIO_MSS_MPLL2_USER_CTL_OUT(v)      \
        out_dword(HWIO_MSS_MPLL2_USER_CTL_ADDR,v)
#define HWIO_MSS_MPLL2_USER_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_MPLL2_USER_CTL_ADDR,m,v,HWIO_MSS_MPLL2_USER_CTL_IN)
#define HWIO_MSS_MPLL2_USER_CTL_RESERVE_31_25_BMSK                        0xfe000000
#define HWIO_MSS_MPLL2_USER_CTL_RESERVE_31_25_SHFT                              0x19
#define HWIO_MSS_MPLL2_USER_CTL_MN_EN_BMSK                                 0x1000000
#define HWIO_MSS_MPLL2_USER_CTL_MN_EN_SHFT                                      0x18
#define HWIO_MSS_MPLL2_USER_CTL_VCO_SEL_BMSK                                0x300000
#define HWIO_MSS_MPLL2_USER_CTL_VCO_SEL_SHFT                                    0x14
#define HWIO_MSS_MPLL2_USER_CTL_PRE_DIV_RATIO_BMSK                            0x7000
#define HWIO_MSS_MPLL2_USER_CTL_PRE_DIV_RATIO_SHFT                               0xc
#define HWIO_MSS_MPLL2_USER_CTL_POST_DIV_RATIO_BMSK                            0x300
#define HWIO_MSS_MPLL2_USER_CTL_POST_DIV_RATIO_SHFT                              0x8
#define HWIO_MSS_MPLL2_USER_CTL_OUTPUT_INV_BMSK                                 0x80
#define HWIO_MSS_MPLL2_USER_CTL_OUTPUT_INV_SHFT                                  0x7
#define HWIO_MSS_MPLL2_USER_CTL_PLLOUT_DIFF_90_BMSK                             0x40
#define HWIO_MSS_MPLL2_USER_CTL_PLLOUT_DIFF_90_SHFT                              0x6
#define HWIO_MSS_MPLL2_USER_CTL_PLLOUT_DIFF_0_BMSK                              0x20
#define HWIO_MSS_MPLL2_USER_CTL_PLLOUT_DIFF_0_SHFT                               0x5
#define HWIO_MSS_MPLL2_USER_CTL_PLLOUT_LV_TEST_BMSK                             0x10
#define HWIO_MSS_MPLL2_USER_CTL_PLLOUT_LV_TEST_SHFT                              0x4
#define HWIO_MSS_MPLL2_USER_CTL_PLLOUT_LV_EARLY_BMSK                             0x8
#define HWIO_MSS_MPLL2_USER_CTL_PLLOUT_LV_EARLY_SHFT                             0x3
#define HWIO_MSS_MPLL2_USER_CTL_PLLOUT_LV_AUX2_BMSK                              0x4
#define HWIO_MSS_MPLL2_USER_CTL_PLLOUT_LV_AUX2_SHFT                              0x2
#define HWIO_MSS_MPLL2_USER_CTL_PLLOUT_LV_AUX_BMSK                               0x2
#define HWIO_MSS_MPLL2_USER_CTL_PLLOUT_LV_AUX_SHFT                               0x1
#define HWIO_MSS_MPLL2_USER_CTL_PLLOUT_LV_MAIN_BMSK                              0x1
#define HWIO_MSS_MPLL2_USER_CTL_PLLOUT_LV_MAIN_SHFT                              0x0

#define HWIO_MSS_MPLL2_CONFIG_CTL_ADDR                                    (MSS_PERPH_REG_BASE      + 0x00001054)
#define HWIO_MSS_MPLL2_CONFIG_CTL_RMSK                                    0xfffcfff0
#define HWIO_MSS_MPLL2_CONFIG_CTL_IN          \
        in_dword_masked(HWIO_MSS_MPLL2_CONFIG_CTL_ADDR, HWIO_MSS_MPLL2_CONFIG_CTL_RMSK)
#define HWIO_MSS_MPLL2_CONFIG_CTL_INM(m)      \
        in_dword_masked(HWIO_MSS_MPLL2_CONFIG_CTL_ADDR, m)
#define HWIO_MSS_MPLL2_CONFIG_CTL_OUT(v)      \
        out_dword(HWIO_MSS_MPLL2_CONFIG_CTL_ADDR,v)
#define HWIO_MSS_MPLL2_CONFIG_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_MPLL2_CONFIG_CTL_ADDR,m,v,HWIO_MSS_MPLL2_CONFIG_CTL_IN)
#define HWIO_MSS_MPLL2_CONFIG_CTL_RESERVE_31_24_BMSK                      0xff000000
#define HWIO_MSS_MPLL2_CONFIG_CTL_RESERVE_31_24_SHFT                            0x18
#define HWIO_MSS_MPLL2_CONFIG_CTL_PFD_DZSEL_BMSK                            0xc00000
#define HWIO_MSS_MPLL2_CONFIG_CTL_PFD_DZSEL_SHFT                                0x16
#define HWIO_MSS_MPLL2_CONFIG_CTL_ICP_DIV_BMSK                              0x300000
#define HWIO_MSS_MPLL2_CONFIG_CTL_ICP_DIV_SHFT                                  0x14
#define HWIO_MSS_MPLL2_CONFIG_CTL_IREG_DIV_BMSK                              0xc0000
#define HWIO_MSS_MPLL2_CONFIG_CTL_IREG_DIV_SHFT                                 0x12
#define HWIO_MSS_MPLL2_CONFIG_CTL_VREG_REF_MODE_BMSK                          0x8000
#define HWIO_MSS_MPLL2_CONFIG_CTL_VREG_REF_MODE_SHFT                             0xf
#define HWIO_MSS_MPLL2_CONFIG_CTL_VCO_REF_MODE_BMSK                           0x4000
#define HWIO_MSS_MPLL2_CONFIG_CTL_VCO_REF_MODE_SHFT                              0xe
#define HWIO_MSS_MPLL2_CONFIG_CTL_CFG_LOCKDET_BMSK                            0x3000
#define HWIO_MSS_MPLL2_CONFIG_CTL_CFG_LOCKDET_SHFT                               0xc
#define HWIO_MSS_MPLL2_CONFIG_CTL_FORCE_ISEED_BMSK                             0x800
#define HWIO_MSS_MPLL2_CONFIG_CTL_FORCE_ISEED_SHFT                               0xb
#define HWIO_MSS_MPLL2_CONFIG_CTL_COARSE_GM_BMSK                               0x600
#define HWIO_MSS_MPLL2_CONFIG_CTL_COARSE_GM_SHFT                                 0x9
#define HWIO_MSS_MPLL2_CONFIG_CTL_FILTER_LOOP_ZERO_BMSK                        0x100
#define HWIO_MSS_MPLL2_CONFIG_CTL_FILTER_LOOP_ZERO_SHFT                          0x8
#define HWIO_MSS_MPLL2_CONFIG_CTL_FILTER_LOOP_MODE_BMSK                         0x80
#define HWIO_MSS_MPLL2_CONFIG_CTL_FILTER_LOOP_MODE_SHFT                          0x7
#define HWIO_MSS_MPLL2_CONFIG_CTL_FILTER_RESISTOR_BMSK                          0x60
#define HWIO_MSS_MPLL2_CONFIG_CTL_FILTER_RESISTOR_SHFT                           0x5
#define HWIO_MSS_MPLL2_CONFIG_CTL_GMC_SLEW_MODE_BMSK                            0x10
#define HWIO_MSS_MPLL2_CONFIG_CTL_GMC_SLEW_MODE_SHFT                             0x4

#define HWIO_MSS_MPLL2_TEST_CTL_ADDR                                      (MSS_PERPH_REG_BASE      + 0x00001058)
#define HWIO_MSS_MPLL2_TEST_CTL_RMSK                                      0xffffffff
#define HWIO_MSS_MPLL2_TEST_CTL_IN          \
        in_dword_masked(HWIO_MSS_MPLL2_TEST_CTL_ADDR, HWIO_MSS_MPLL2_TEST_CTL_RMSK)
#define HWIO_MSS_MPLL2_TEST_CTL_INM(m)      \
        in_dword_masked(HWIO_MSS_MPLL2_TEST_CTL_ADDR, m)
#define HWIO_MSS_MPLL2_TEST_CTL_OUT(v)      \
        out_dword(HWIO_MSS_MPLL2_TEST_CTL_ADDR,v)
#define HWIO_MSS_MPLL2_TEST_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_MPLL2_TEST_CTL_ADDR,m,v,HWIO_MSS_MPLL2_TEST_CTL_IN)
#define HWIO_MSS_MPLL2_TEST_CTL_RESERVE_31_21_BMSK                        0xffe00000
#define HWIO_MSS_MPLL2_TEST_CTL_RESERVE_31_21_SHFT                              0x15
#define HWIO_MSS_MPLL2_TEST_CTL_NGEN_CFG_BMSK                               0x1c0000
#define HWIO_MSS_MPLL2_TEST_CTL_NGEN_CFG_SHFT                                   0x12
#define HWIO_MSS_MPLL2_TEST_CTL_NGEN_EN_BMSK                                 0x20000
#define HWIO_MSS_MPLL2_TEST_CTL_NGEN_EN_SHFT                                    0x11
#define HWIO_MSS_MPLL2_TEST_CTL_NMOSC_FREQ_CTRL_BMSK                         0x18000
#define HWIO_MSS_MPLL2_TEST_CTL_NMOSC_FREQ_CTRL_SHFT                             0xf
#define HWIO_MSS_MPLL2_TEST_CTL_NMOSC_EN_BMSK                                 0x4000
#define HWIO_MSS_MPLL2_TEST_CTL_NMOSC_EN_SHFT                                    0xe
#define HWIO_MSS_MPLL2_TEST_CTL_FORCE_PFD_UP_BMSK                             0x2000
#define HWIO_MSS_MPLL2_TEST_CTL_FORCE_PFD_UP_SHFT                                0xd
#define HWIO_MSS_MPLL2_TEST_CTL_FORCE_PFD_DOWN_BMSK                           0x1000
#define HWIO_MSS_MPLL2_TEST_CTL_FORCE_PFD_DOWN_SHFT                              0xc
#define HWIO_MSS_MPLL2_TEST_CTL_TEST_OUT_SEL_BMSK                              0x800
#define HWIO_MSS_MPLL2_TEST_CTL_TEST_OUT_SEL_SHFT                                0xb
#define HWIO_MSS_MPLL2_TEST_CTL_ICP_TST_EN_BMSK                                0x400
#define HWIO_MSS_MPLL2_TEST_CTL_ICP_TST_EN_SHFT                                  0xa
#define HWIO_MSS_MPLL2_TEST_CTL_ICP_EXT_SEL_BMSK                               0x200
#define HWIO_MSS_MPLL2_TEST_CTL_ICP_EXT_SEL_SHFT                                 0x9
#define HWIO_MSS_MPLL2_TEST_CTL_DTEST_SEL_BMSK                                 0x180
#define HWIO_MSS_MPLL2_TEST_CTL_DTEST_SEL_SHFT                                   0x7
#define HWIO_MSS_MPLL2_TEST_CTL_BYP_TESTAMP_BMSK                                0x40
#define HWIO_MSS_MPLL2_TEST_CTL_BYP_TESTAMP_SHFT                                 0x6
#define HWIO_MSS_MPLL2_TEST_CTL_ATEST1_SEL_BMSK                                 0x30
#define HWIO_MSS_MPLL2_TEST_CTL_ATEST1_SEL_SHFT                                  0x4
#define HWIO_MSS_MPLL2_TEST_CTL_ATEST0_SEL_BMSK                                  0xc
#define HWIO_MSS_MPLL2_TEST_CTL_ATEST0_SEL_SHFT                                  0x2
#define HWIO_MSS_MPLL2_TEST_CTL_ATEST1_EN_BMSK                                   0x2
#define HWIO_MSS_MPLL2_TEST_CTL_ATEST1_EN_SHFT                                   0x1
#define HWIO_MSS_MPLL2_TEST_CTL_ATEST0_EN_BMSK                                   0x1
#define HWIO_MSS_MPLL2_TEST_CTL_ATEST0_EN_SHFT                                   0x0

#define HWIO_MSS_MPLL2_STATUS_ADDR                                        (MSS_PERPH_REG_BASE      + 0x0000105c)
#define HWIO_MSS_MPLL2_STATUS_RMSK                                           0x3ffff
#define HWIO_MSS_MPLL2_STATUS_IN          \
        in_dword_masked(HWIO_MSS_MPLL2_STATUS_ADDR, HWIO_MSS_MPLL2_STATUS_RMSK)
#define HWIO_MSS_MPLL2_STATUS_INM(m)      \
        in_dword_masked(HWIO_MSS_MPLL2_STATUS_ADDR, m)
#define HWIO_MSS_MPLL2_STATUS_PLL_ACTIVE_FLAG_BMSK                           0x20000
#define HWIO_MSS_MPLL2_STATUS_PLL_ACTIVE_FLAG_SHFT                              0x11
#define HWIO_MSS_MPLL2_STATUS_PLL_LOCK_DET_BMSK                              0x10000
#define HWIO_MSS_MPLL2_STATUS_PLL_LOCK_DET_SHFT                                 0x10
#define HWIO_MSS_MPLL2_STATUS_PLL_D_BMSK                                      0xffff
#define HWIO_MSS_MPLL2_STATUS_PLL_D_SHFT                                         0x0

#define HWIO_MSS_UIM0_BCR_ADDR                                            (MSS_PERPH_REG_BASE      + 0x00001060)
#define HWIO_MSS_UIM0_BCR_RMSK                                                   0x1
#define HWIO_MSS_UIM0_BCR_IN          \
        in_dword_masked(HWIO_MSS_UIM0_BCR_ADDR, HWIO_MSS_UIM0_BCR_RMSK)
#define HWIO_MSS_UIM0_BCR_INM(m)      \
        in_dword_masked(HWIO_MSS_UIM0_BCR_ADDR, m)
#define HWIO_MSS_UIM0_BCR_OUT(v)      \
        out_dword(HWIO_MSS_UIM0_BCR_ADDR,v)
#define HWIO_MSS_UIM0_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_UIM0_BCR_ADDR,m,v,HWIO_MSS_UIM0_BCR_IN)
#define HWIO_MSS_UIM0_BCR_BLK_ARES_BMSK                                          0x1
#define HWIO_MSS_UIM0_BCR_BLK_ARES_SHFT                                          0x0

#define HWIO_MSS_UIM1_BCR_ADDR                                            (MSS_PERPH_REG_BASE      + 0x00001064)
#define HWIO_MSS_UIM1_BCR_RMSK                                                   0x1
#define HWIO_MSS_UIM1_BCR_IN          \
        in_dword_masked(HWIO_MSS_UIM1_BCR_ADDR, HWIO_MSS_UIM1_BCR_RMSK)
#define HWIO_MSS_UIM1_BCR_INM(m)      \
        in_dword_masked(HWIO_MSS_UIM1_BCR_ADDR, m)
#define HWIO_MSS_UIM1_BCR_OUT(v)      \
        out_dword(HWIO_MSS_UIM1_BCR_ADDR,v)
#define HWIO_MSS_UIM1_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_UIM1_BCR_ADDR,m,v,HWIO_MSS_UIM1_BCR_IN)
#define HWIO_MSS_UIM1_BCR_BLK_ARES_BMSK                                          0x1
#define HWIO_MSS_UIM1_BCR_BLK_ARES_SHFT                                          0x0

#define HWIO_MSS_Q6SS_BCR_ADDR                                            (MSS_PERPH_REG_BASE      + 0x00001068)
#define HWIO_MSS_Q6SS_BCR_RMSK                                                   0x1
#define HWIO_MSS_Q6SS_BCR_IN          \
        in_dword_masked(HWIO_MSS_Q6SS_BCR_ADDR, HWIO_MSS_Q6SS_BCR_RMSK)
#define HWIO_MSS_Q6SS_BCR_INM(m)      \
        in_dword_masked(HWIO_MSS_Q6SS_BCR_ADDR, m)
#define HWIO_MSS_Q6SS_BCR_OUT(v)      \
        out_dword(HWIO_MSS_Q6SS_BCR_ADDR,v)
#define HWIO_MSS_Q6SS_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_Q6SS_BCR_ADDR,m,v,HWIO_MSS_Q6SS_BCR_IN)
#define HWIO_MSS_Q6SS_BCR_BLK_ARES_BMSK                                          0x1
#define HWIO_MSS_Q6SS_BCR_BLK_ARES_SHFT                                          0x0

#define HWIO_MSS_NC_HM_BCR_ADDR                                           (MSS_PERPH_REG_BASE      + 0x0000106c)
#define HWIO_MSS_NC_HM_BCR_RMSK                                                  0x1
#define HWIO_MSS_NC_HM_BCR_IN          \
        in_dword_masked(HWIO_MSS_NC_HM_BCR_ADDR, HWIO_MSS_NC_HM_BCR_RMSK)
#define HWIO_MSS_NC_HM_BCR_INM(m)      \
        in_dword_masked(HWIO_MSS_NC_HM_BCR_ADDR, m)
#define HWIO_MSS_NC_HM_BCR_OUT(v)      \
        out_dword(HWIO_MSS_NC_HM_BCR_ADDR,v)
#define HWIO_MSS_NC_HM_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_NC_HM_BCR_ADDR,m,v,HWIO_MSS_NC_HM_BCR_IN)
#define HWIO_MSS_NC_HM_BCR_BLK_ARES_BMSK                                         0x1
#define HWIO_MSS_NC_HM_BCR_BLK_ARES_SHFT                                         0x0

#define HWIO_MSS_RBCPR_BCR_ADDR                                           (MSS_PERPH_REG_BASE      + 0x00001070)
#define HWIO_MSS_RBCPR_BCR_RMSK                                                  0x1
#define HWIO_MSS_RBCPR_BCR_IN          \
        in_dword_masked(HWIO_MSS_RBCPR_BCR_ADDR, HWIO_MSS_RBCPR_BCR_RMSK)
#define HWIO_MSS_RBCPR_BCR_INM(m)      \
        in_dword_masked(HWIO_MSS_RBCPR_BCR_ADDR, m)
#define HWIO_MSS_RBCPR_BCR_OUT(v)      \
        out_dword(HWIO_MSS_RBCPR_BCR_ADDR,v)
#define HWIO_MSS_RBCPR_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_RBCPR_BCR_ADDR,m,v,HWIO_MSS_RBCPR_BCR_IN)
#define HWIO_MSS_RBCPR_BCR_BLK_ARES_BMSK                                         0x1
#define HWIO_MSS_RBCPR_BCR_BLK_ARES_SHFT                                         0x0

#define HWIO_MSS_COXM_BCR_ADDR                                            (MSS_PERPH_REG_BASE      + 0x00001074)
#define HWIO_MSS_COXM_BCR_RMSK                                                   0x1
#define HWIO_MSS_COXM_BCR_IN          \
        in_dword_masked(HWIO_MSS_COXM_BCR_ADDR, HWIO_MSS_COXM_BCR_RMSK)
#define HWIO_MSS_COXM_BCR_INM(m)      \
        in_dword_masked(HWIO_MSS_COXM_BCR_ADDR, m)
#define HWIO_MSS_COXM_BCR_OUT(v)      \
        out_dword(HWIO_MSS_COXM_BCR_ADDR,v)
#define HWIO_MSS_COXM_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_COXM_BCR_ADDR,m,v,HWIO_MSS_COXM_BCR_IN)
#define HWIO_MSS_COXM_BCR_BLK_ARES_BMSK                                          0x1
#define HWIO_MSS_COXM_BCR_BLK_ARES_SHFT                                          0x0

#define HWIO_MSS_UIM0_CBCR_ADDR                                           (MSS_PERPH_REG_BASE      + 0x00001078)
#define HWIO_MSS_UIM0_CBCR_RMSK                                           0x80000001
#define HWIO_MSS_UIM0_CBCR_IN          \
        in_dword_masked(HWIO_MSS_UIM0_CBCR_ADDR, HWIO_MSS_UIM0_CBCR_RMSK)
#define HWIO_MSS_UIM0_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_UIM0_CBCR_ADDR, m)
#define HWIO_MSS_UIM0_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_UIM0_CBCR_ADDR,v)
#define HWIO_MSS_UIM0_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_UIM0_CBCR_ADDR,m,v,HWIO_MSS_UIM0_CBCR_IN)
#define HWIO_MSS_UIM0_CBCR_CLKOFF_BMSK                                    0x80000000
#define HWIO_MSS_UIM0_CBCR_CLKOFF_SHFT                                          0x1f
#define HWIO_MSS_UIM0_CBCR_CLKEN_BMSK                                            0x1
#define HWIO_MSS_UIM0_CBCR_CLKEN_SHFT                                            0x0

#define HWIO_MSS_UIM1_CBCR_ADDR                                           (MSS_PERPH_REG_BASE      + 0x0000107c)
#define HWIO_MSS_UIM1_CBCR_RMSK                                           0x80000001
#define HWIO_MSS_UIM1_CBCR_IN          \
        in_dword_masked(HWIO_MSS_UIM1_CBCR_ADDR, HWIO_MSS_UIM1_CBCR_RMSK)
#define HWIO_MSS_UIM1_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_UIM1_CBCR_ADDR, m)
#define HWIO_MSS_UIM1_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_UIM1_CBCR_ADDR,v)
#define HWIO_MSS_UIM1_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_UIM1_CBCR_ADDR,m,v,HWIO_MSS_UIM1_CBCR_IN)
#define HWIO_MSS_UIM1_CBCR_CLKOFF_BMSK                                    0x80000000
#define HWIO_MSS_UIM1_CBCR_CLKOFF_SHFT                                          0x1f
#define HWIO_MSS_UIM1_CBCR_CLKEN_BMSK                                            0x1
#define HWIO_MSS_UIM1_CBCR_CLKEN_SHFT                                            0x0

#define HWIO_MSS_XO_UIM0_CBCR_ADDR                                        (MSS_PERPH_REG_BASE      + 0x00001080)
#define HWIO_MSS_XO_UIM0_CBCR_RMSK                                        0x80000001
#define HWIO_MSS_XO_UIM0_CBCR_IN          \
        in_dword_masked(HWIO_MSS_XO_UIM0_CBCR_ADDR, HWIO_MSS_XO_UIM0_CBCR_RMSK)
#define HWIO_MSS_XO_UIM0_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_XO_UIM0_CBCR_ADDR, m)
#define HWIO_MSS_XO_UIM0_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_XO_UIM0_CBCR_ADDR,v)
#define HWIO_MSS_XO_UIM0_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_XO_UIM0_CBCR_ADDR,m,v,HWIO_MSS_XO_UIM0_CBCR_IN)
#define HWIO_MSS_XO_UIM0_CBCR_CLKOFF_BMSK                                 0x80000000
#define HWIO_MSS_XO_UIM0_CBCR_CLKOFF_SHFT                                       0x1f
#define HWIO_MSS_XO_UIM0_CBCR_CLKEN_BMSK                                         0x1
#define HWIO_MSS_XO_UIM0_CBCR_CLKEN_SHFT                                         0x0

#define HWIO_MSS_XO_UIM1_CBCR_ADDR                                        (MSS_PERPH_REG_BASE      + 0x00001084)
#define HWIO_MSS_XO_UIM1_CBCR_RMSK                                        0x80000001
#define HWIO_MSS_XO_UIM1_CBCR_IN          \
        in_dword_masked(HWIO_MSS_XO_UIM1_CBCR_ADDR, HWIO_MSS_XO_UIM1_CBCR_RMSK)
#define HWIO_MSS_XO_UIM1_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_XO_UIM1_CBCR_ADDR, m)
#define HWIO_MSS_XO_UIM1_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_XO_UIM1_CBCR_ADDR,v)
#define HWIO_MSS_XO_UIM1_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_XO_UIM1_CBCR_ADDR,m,v,HWIO_MSS_XO_UIM1_CBCR_IN)
#define HWIO_MSS_XO_UIM1_CBCR_CLKOFF_BMSK                                 0x80000000
#define HWIO_MSS_XO_UIM1_CBCR_CLKOFF_SHFT                                       0x1f
#define HWIO_MSS_XO_UIM1_CBCR_CLKEN_BMSK                                         0x1
#define HWIO_MSS_XO_UIM1_CBCR_CLKEN_SHFT                                         0x0

#define HWIO_MSS_XO_MODEM_CX_TO_LS_CBCR_ADDR                              (MSS_PERPH_REG_BASE      + 0x00001088)
#define HWIO_MSS_XO_MODEM_CX_TO_LS_CBCR_RMSK                              0x80000000
#define HWIO_MSS_XO_MODEM_CX_TO_LS_CBCR_IN          \
        in_dword_masked(HWIO_MSS_XO_MODEM_CX_TO_LS_CBCR_ADDR, HWIO_MSS_XO_MODEM_CX_TO_LS_CBCR_RMSK)
#define HWIO_MSS_XO_MODEM_CX_TO_LS_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_XO_MODEM_CX_TO_LS_CBCR_ADDR, m)
#define HWIO_MSS_XO_MODEM_CX_TO_LS_CBCR_CLKOFF_BMSK                       0x80000000
#define HWIO_MSS_XO_MODEM_CX_TO_LS_CBCR_CLKOFF_SHFT                             0x1f

#define HWIO_MSS_XO_Q6_CBCR_ADDR                                          (MSS_PERPH_REG_BASE      + 0x0000108c)
#define HWIO_MSS_XO_Q6_CBCR_RMSK                                          0x80000001
#define HWIO_MSS_XO_Q6_CBCR_IN          \
        in_dword_masked(HWIO_MSS_XO_Q6_CBCR_ADDR, HWIO_MSS_XO_Q6_CBCR_RMSK)
#define HWIO_MSS_XO_Q6_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_XO_Q6_CBCR_ADDR, m)
#define HWIO_MSS_XO_Q6_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_XO_Q6_CBCR_ADDR,v)
#define HWIO_MSS_XO_Q6_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_XO_Q6_CBCR_ADDR,m,v,HWIO_MSS_XO_Q6_CBCR_IN)
#define HWIO_MSS_XO_Q6_CBCR_CLKOFF_BMSK                                   0x80000000
#define HWIO_MSS_XO_Q6_CBCR_CLKOFF_SHFT                                         0x1f
#define HWIO_MSS_XO_Q6_CBCR_CLKEN_BMSK                                           0x1
#define HWIO_MSS_XO_Q6_CBCR_CLKEN_SHFT                                           0x0

#define HWIO_MSS_BUS_UIM0_CBCR_ADDR                                       (MSS_PERPH_REG_BASE      + 0x00001090)
#define HWIO_MSS_BUS_UIM0_CBCR_RMSK                                       0x80007ff1
#define HWIO_MSS_BUS_UIM0_CBCR_IN          \
        in_dword_masked(HWIO_MSS_BUS_UIM0_CBCR_ADDR, HWIO_MSS_BUS_UIM0_CBCR_RMSK)
#define HWIO_MSS_BUS_UIM0_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_BUS_UIM0_CBCR_ADDR, m)
#define HWIO_MSS_BUS_UIM0_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_BUS_UIM0_CBCR_ADDR,v)
#define HWIO_MSS_BUS_UIM0_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BUS_UIM0_CBCR_ADDR,m,v,HWIO_MSS_BUS_UIM0_CBCR_IN)
#define HWIO_MSS_BUS_UIM0_CBCR_CLKOFF_BMSK                                0x80000000
#define HWIO_MSS_BUS_UIM0_CBCR_CLKOFF_SHFT                                      0x1f
#define HWIO_MSS_BUS_UIM0_CBCR_FORCE_MEM_CORE_ON_BMSK                         0x4000
#define HWIO_MSS_BUS_UIM0_CBCR_FORCE_MEM_CORE_ON_SHFT                            0xe
#define HWIO_MSS_BUS_UIM0_CBCR_FORCE_MEM_PERIPH_ON_BMSK                       0x2000
#define HWIO_MSS_BUS_UIM0_CBCR_FORCE_MEM_PERIPH_ON_SHFT                          0xd
#define HWIO_MSS_BUS_UIM0_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                      0x1000
#define HWIO_MSS_BUS_UIM0_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                         0xc
#define HWIO_MSS_BUS_UIM0_CBCR_WAKEUP_BMSK                                     0xf00
#define HWIO_MSS_BUS_UIM0_CBCR_WAKEUP_SHFT                                       0x8
#define HWIO_MSS_BUS_UIM0_CBCR_SLEEP_BMSK                                       0xf0
#define HWIO_MSS_BUS_UIM0_CBCR_SLEEP_SHFT                                        0x4
#define HWIO_MSS_BUS_UIM0_CBCR_CLKEN_BMSK                                        0x1
#define HWIO_MSS_BUS_UIM0_CBCR_CLKEN_SHFT                                        0x0

#define HWIO_MSS_BUS_UIM1_CBCR_ADDR                                       (MSS_PERPH_REG_BASE      + 0x00001094)
#define HWIO_MSS_BUS_UIM1_CBCR_RMSK                                       0x80007ff1
#define HWIO_MSS_BUS_UIM1_CBCR_IN          \
        in_dword_masked(HWIO_MSS_BUS_UIM1_CBCR_ADDR, HWIO_MSS_BUS_UIM1_CBCR_RMSK)
#define HWIO_MSS_BUS_UIM1_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_BUS_UIM1_CBCR_ADDR, m)
#define HWIO_MSS_BUS_UIM1_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_BUS_UIM1_CBCR_ADDR,v)
#define HWIO_MSS_BUS_UIM1_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BUS_UIM1_CBCR_ADDR,m,v,HWIO_MSS_BUS_UIM1_CBCR_IN)
#define HWIO_MSS_BUS_UIM1_CBCR_CLKOFF_BMSK                                0x80000000
#define HWIO_MSS_BUS_UIM1_CBCR_CLKOFF_SHFT                                      0x1f
#define HWIO_MSS_BUS_UIM1_CBCR_FORCE_MEM_CORE_ON_BMSK                         0x4000
#define HWIO_MSS_BUS_UIM1_CBCR_FORCE_MEM_CORE_ON_SHFT                            0xe
#define HWIO_MSS_BUS_UIM1_CBCR_FORCE_MEM_PERIPH_ON_BMSK                       0x2000
#define HWIO_MSS_BUS_UIM1_CBCR_FORCE_MEM_PERIPH_ON_SHFT                          0xd
#define HWIO_MSS_BUS_UIM1_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                      0x1000
#define HWIO_MSS_BUS_UIM1_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                         0xc
#define HWIO_MSS_BUS_UIM1_CBCR_WAKEUP_BMSK                                     0xf00
#define HWIO_MSS_BUS_UIM1_CBCR_WAKEUP_SHFT                                       0x8
#define HWIO_MSS_BUS_UIM1_CBCR_SLEEP_BMSK                                       0xf0
#define HWIO_MSS_BUS_UIM1_CBCR_SLEEP_SHFT                                        0x4
#define HWIO_MSS_BUS_UIM1_CBCR_CLKEN_BMSK                                        0x1
#define HWIO_MSS_BUS_UIM1_CBCR_CLKEN_SHFT                                        0x0

#define HWIO_MSS_BUS_CSR_CBCR_ADDR                                        (MSS_PERPH_REG_BASE      + 0x00001098)
#define HWIO_MSS_BUS_CSR_CBCR_RMSK                                        0x80000001
#define HWIO_MSS_BUS_CSR_CBCR_IN          \
        in_dword_masked(HWIO_MSS_BUS_CSR_CBCR_ADDR, HWIO_MSS_BUS_CSR_CBCR_RMSK)
#define HWIO_MSS_BUS_CSR_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_BUS_CSR_CBCR_ADDR, m)
#define HWIO_MSS_BUS_CSR_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_BUS_CSR_CBCR_ADDR,v)
#define HWIO_MSS_BUS_CSR_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BUS_CSR_CBCR_ADDR,m,v,HWIO_MSS_BUS_CSR_CBCR_IN)
#define HWIO_MSS_BUS_CSR_CBCR_CLKOFF_BMSK                                 0x80000000
#define HWIO_MSS_BUS_CSR_CBCR_CLKOFF_SHFT                                       0x1f
#define HWIO_MSS_BUS_CSR_CBCR_CLKEN_BMSK                                         0x1
#define HWIO_MSS_BUS_CSR_CBCR_CLKEN_SHFT                                         0x0

#define HWIO_MSS_BUS_BRIDGE_CBCR_ADDR                                     (MSS_PERPH_REG_BASE      + 0x0000109c)
#define HWIO_MSS_BUS_BRIDGE_CBCR_RMSK                                     0x80000001
#define HWIO_MSS_BUS_BRIDGE_CBCR_IN          \
        in_dword_masked(HWIO_MSS_BUS_BRIDGE_CBCR_ADDR, HWIO_MSS_BUS_BRIDGE_CBCR_RMSK)
#define HWIO_MSS_BUS_BRIDGE_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_BUS_BRIDGE_CBCR_ADDR, m)
#define HWIO_MSS_BUS_BRIDGE_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_BUS_BRIDGE_CBCR_ADDR,v)
#define HWIO_MSS_BUS_BRIDGE_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BUS_BRIDGE_CBCR_ADDR,m,v,HWIO_MSS_BUS_BRIDGE_CBCR_IN)
#define HWIO_MSS_BUS_BRIDGE_CBCR_CLKOFF_BMSK                              0x80000000
#define HWIO_MSS_BUS_BRIDGE_CBCR_CLKOFF_SHFT                                    0x1f
#define HWIO_MSS_BUS_BRIDGE_CBCR_CLKEN_BMSK                                      0x1
#define HWIO_MSS_BUS_BRIDGE_CBCR_CLKEN_SHFT                                      0x0

#define HWIO_MSS_BUS_MODEM_CX_TO_LS_CBCR_ADDR                             (MSS_PERPH_REG_BASE      + 0x000010a0)
#define HWIO_MSS_BUS_MODEM_CX_TO_LS_CBCR_RMSK                             0x80000000
#define HWIO_MSS_BUS_MODEM_CX_TO_LS_CBCR_IN          \
        in_dword_masked(HWIO_MSS_BUS_MODEM_CX_TO_LS_CBCR_ADDR, HWIO_MSS_BUS_MODEM_CX_TO_LS_CBCR_RMSK)
#define HWIO_MSS_BUS_MODEM_CX_TO_LS_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_BUS_MODEM_CX_TO_LS_CBCR_ADDR, m)
#define HWIO_MSS_BUS_MODEM_CX_TO_LS_CBCR_CLKOFF_BMSK                      0x80000000
#define HWIO_MSS_BUS_MODEM_CX_TO_LS_CBCR_CLKOFF_SHFT                            0x1f

#define HWIO_MSS_BUS_Q6_CBCR_ADDR                                         (MSS_PERPH_REG_BASE      + 0x000010a4)
#define HWIO_MSS_BUS_Q6_CBCR_RMSK                                         0x80000001
#define HWIO_MSS_BUS_Q6_CBCR_IN          \
        in_dword_masked(HWIO_MSS_BUS_Q6_CBCR_ADDR, HWIO_MSS_BUS_Q6_CBCR_RMSK)
#define HWIO_MSS_BUS_Q6_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_BUS_Q6_CBCR_ADDR, m)
#define HWIO_MSS_BUS_Q6_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_BUS_Q6_CBCR_ADDR,v)
#define HWIO_MSS_BUS_Q6_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BUS_Q6_CBCR_ADDR,m,v,HWIO_MSS_BUS_Q6_CBCR_IN)
#define HWIO_MSS_BUS_Q6_CBCR_CLKOFF_BMSK                                  0x80000000
#define HWIO_MSS_BUS_Q6_CBCR_CLKOFF_SHFT                                        0x1f
#define HWIO_MSS_BUS_Q6_CBCR_CLKEN_BMSK                                          0x1
#define HWIO_MSS_BUS_Q6_CBCR_CLKEN_SHFT                                          0x0

#define HWIO_MSS_BUS_NC_HM_BRIDGE_CBCR_ADDR                               (MSS_PERPH_REG_BASE      + 0x000010a8)
#define HWIO_MSS_BUS_NC_HM_BRIDGE_CBCR_RMSK                               0x80000001
#define HWIO_MSS_BUS_NC_HM_BRIDGE_CBCR_IN          \
        in_dword_masked(HWIO_MSS_BUS_NC_HM_BRIDGE_CBCR_ADDR, HWIO_MSS_BUS_NC_HM_BRIDGE_CBCR_RMSK)
#define HWIO_MSS_BUS_NC_HM_BRIDGE_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_BUS_NC_HM_BRIDGE_CBCR_ADDR, m)
#define HWIO_MSS_BUS_NC_HM_BRIDGE_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_BUS_NC_HM_BRIDGE_CBCR_ADDR,v)
#define HWIO_MSS_BUS_NC_HM_BRIDGE_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BUS_NC_HM_BRIDGE_CBCR_ADDR,m,v,HWIO_MSS_BUS_NC_HM_BRIDGE_CBCR_IN)
#define HWIO_MSS_BUS_NC_HM_BRIDGE_CBCR_CLKOFF_BMSK                        0x80000000
#define HWIO_MSS_BUS_NC_HM_BRIDGE_CBCR_CLKOFF_SHFT                              0x1f
#define HWIO_MSS_BUS_NC_HM_BRIDGE_CBCR_CLKEN_BMSK                                0x1
#define HWIO_MSS_BUS_NC_HM_BRIDGE_CBCR_CLKEN_SHFT                                0x0

#define HWIO_MSS_BUS_CRYPTO_CBCR_ADDR                                     (MSS_PERPH_REG_BASE      + 0x000010ac)
#define HWIO_MSS_BUS_CRYPTO_CBCR_RMSK                                     0x80000001
#define HWIO_MSS_BUS_CRYPTO_CBCR_IN          \
        in_dword_masked(HWIO_MSS_BUS_CRYPTO_CBCR_ADDR, HWIO_MSS_BUS_CRYPTO_CBCR_RMSK)
#define HWIO_MSS_BUS_CRYPTO_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_BUS_CRYPTO_CBCR_ADDR, m)
#define HWIO_MSS_BUS_CRYPTO_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_BUS_CRYPTO_CBCR_ADDR,v)
#define HWIO_MSS_BUS_CRYPTO_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BUS_CRYPTO_CBCR_ADDR,m,v,HWIO_MSS_BUS_CRYPTO_CBCR_IN)
#define HWIO_MSS_BUS_CRYPTO_CBCR_CLKOFF_BMSK                              0x80000000
#define HWIO_MSS_BUS_CRYPTO_CBCR_CLKOFF_SHFT                                    0x1f
#define HWIO_MSS_BUS_CRYPTO_CBCR_CLKEN_BMSK                                      0x1
#define HWIO_MSS_BUS_CRYPTO_CBCR_CLKEN_SHFT                                      0x0

#define HWIO_MSS_BUS_NAV_CBCR_ADDR                                        (MSS_PERPH_REG_BASE      + 0x000010b0)
#define HWIO_MSS_BUS_NAV_CBCR_RMSK                                        0x80000001
#define HWIO_MSS_BUS_NAV_CBCR_IN          \
        in_dword_masked(HWIO_MSS_BUS_NAV_CBCR_ADDR, HWIO_MSS_BUS_NAV_CBCR_RMSK)
#define HWIO_MSS_BUS_NAV_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_BUS_NAV_CBCR_ADDR, m)
#define HWIO_MSS_BUS_NAV_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_BUS_NAV_CBCR_ADDR,v)
#define HWIO_MSS_BUS_NAV_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BUS_NAV_CBCR_ADDR,m,v,HWIO_MSS_BUS_NAV_CBCR_IN)
#define HWIO_MSS_BUS_NAV_CBCR_CLKOFF_BMSK                                 0x80000000
#define HWIO_MSS_BUS_NAV_CBCR_CLKOFF_SHFT                                       0x1f
#define HWIO_MSS_BUS_NAV_CBCR_CLKEN_BMSK                                         0x1
#define HWIO_MSS_BUS_NAV_CBCR_CLKEN_SHFT                                         0x0

#define HWIO_MSS_BUS_NC_HM_BRIDGE_CX_CBCR_ADDR                            (MSS_PERPH_REG_BASE      + 0x000010b4)
#define HWIO_MSS_BUS_NC_HM_BRIDGE_CX_CBCR_RMSK                            0x80000001
#define HWIO_MSS_BUS_NC_HM_BRIDGE_CX_CBCR_IN          \
        in_dword_masked(HWIO_MSS_BUS_NC_HM_BRIDGE_CX_CBCR_ADDR, HWIO_MSS_BUS_NC_HM_BRIDGE_CX_CBCR_RMSK)
#define HWIO_MSS_BUS_NC_HM_BRIDGE_CX_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_BUS_NC_HM_BRIDGE_CX_CBCR_ADDR, m)
#define HWIO_MSS_BUS_NC_HM_BRIDGE_CX_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_BUS_NC_HM_BRIDGE_CX_CBCR_ADDR,v)
#define HWIO_MSS_BUS_NC_HM_BRIDGE_CX_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BUS_NC_HM_BRIDGE_CX_CBCR_ADDR,m,v,HWIO_MSS_BUS_NC_HM_BRIDGE_CX_CBCR_IN)
#define HWIO_MSS_BUS_NC_HM_BRIDGE_CX_CBCR_CLKOFF_BMSK                     0x80000000
#define HWIO_MSS_BUS_NC_HM_BRIDGE_CX_CBCR_CLKOFF_SHFT                           0x1f
#define HWIO_MSS_BUS_NC_HM_BRIDGE_CX_CBCR_CLKEN_BMSK                             0x1
#define HWIO_MSS_BUS_NC_HM_BRIDGE_CX_CBCR_CLKEN_SHFT                             0x0

#define HWIO_MSS_BUS_ATB_CBCR_ADDR                                        (MSS_PERPH_REG_BASE      + 0x000010b8)
#define HWIO_MSS_BUS_ATB_CBCR_RMSK                                        0x80000001
#define HWIO_MSS_BUS_ATB_CBCR_IN          \
        in_dword_masked(HWIO_MSS_BUS_ATB_CBCR_ADDR, HWIO_MSS_BUS_ATB_CBCR_RMSK)
#define HWIO_MSS_BUS_ATB_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_BUS_ATB_CBCR_ADDR, m)
#define HWIO_MSS_BUS_ATB_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_BUS_ATB_CBCR_ADDR,v)
#define HWIO_MSS_BUS_ATB_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BUS_ATB_CBCR_ADDR,m,v,HWIO_MSS_BUS_ATB_CBCR_IN)
#define HWIO_MSS_BUS_ATB_CBCR_CLKOFF_BMSK                                 0x80000000
#define HWIO_MSS_BUS_ATB_CBCR_CLKOFF_SHFT                                       0x1f
#define HWIO_MSS_BUS_ATB_CBCR_CLKEN_BMSK                                         0x1
#define HWIO_MSS_BUS_ATB_CBCR_CLKEN_SHFT                                         0x0

#define HWIO_MSS_BUS_COXM_CBCR_ADDR                                       (MSS_PERPH_REG_BASE      + 0x000010bc)
#define HWIO_MSS_BUS_COXM_CBCR_RMSK                                       0x80007ff1
#define HWIO_MSS_BUS_COXM_CBCR_IN          \
        in_dword_masked(HWIO_MSS_BUS_COXM_CBCR_ADDR, HWIO_MSS_BUS_COXM_CBCR_RMSK)
#define HWIO_MSS_BUS_COXM_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_BUS_COXM_CBCR_ADDR, m)
#define HWIO_MSS_BUS_COXM_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_BUS_COXM_CBCR_ADDR,v)
#define HWIO_MSS_BUS_COXM_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BUS_COXM_CBCR_ADDR,m,v,HWIO_MSS_BUS_COXM_CBCR_IN)
#define HWIO_MSS_BUS_COXM_CBCR_CLKOFF_BMSK                                0x80000000
#define HWIO_MSS_BUS_COXM_CBCR_CLKOFF_SHFT                                      0x1f
#define HWIO_MSS_BUS_COXM_CBCR_FORCE_MEM_CORE_ON_BMSK                         0x4000
#define HWIO_MSS_BUS_COXM_CBCR_FORCE_MEM_CORE_ON_SHFT                            0xe
#define HWIO_MSS_BUS_COXM_CBCR_FORCE_MEM_PERIPH_ON_BMSK                       0x2000
#define HWIO_MSS_BUS_COXM_CBCR_FORCE_MEM_PERIPH_ON_SHFT                          0xd
#define HWIO_MSS_BUS_COXM_CBCR_FORCE_MEM_PERIPH_OFF_BMSK                      0x1000
#define HWIO_MSS_BUS_COXM_CBCR_FORCE_MEM_PERIPH_OFF_SHFT                         0xc
#define HWIO_MSS_BUS_COXM_CBCR_WAKEUP_BMSK                                     0xf00
#define HWIO_MSS_BUS_COXM_CBCR_WAKEUP_SHFT                                       0x8
#define HWIO_MSS_BUS_COXM_CBCR_SLEEP_BMSK                                       0xf0
#define HWIO_MSS_BUS_COXM_CBCR_SLEEP_SHFT                                        0x4
#define HWIO_MSS_BUS_COXM_CBCR_CLKEN_BMSK                                        0x1
#define HWIO_MSS_BUS_COXM_CBCR_CLKEN_SHFT                                        0x0

#define HWIO_MSS_BUS_MODEM_BRIDGE_CX_CBCR_ADDR                            (MSS_PERPH_REG_BASE      + 0x000010c0)
#define HWIO_MSS_BUS_MODEM_BRIDGE_CX_CBCR_RMSK                            0x80000000
#define HWIO_MSS_BUS_MODEM_BRIDGE_CX_CBCR_IN          \
        in_dword_masked(HWIO_MSS_BUS_MODEM_BRIDGE_CX_CBCR_ADDR, HWIO_MSS_BUS_MODEM_BRIDGE_CX_CBCR_RMSK)
#define HWIO_MSS_BUS_MODEM_BRIDGE_CX_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_BUS_MODEM_BRIDGE_CX_CBCR_ADDR, m)
#define HWIO_MSS_BUS_MODEM_BRIDGE_CX_CBCR_CLKOFF_BMSK                     0x80000000
#define HWIO_MSS_BUS_MODEM_BRIDGE_CX_CBCR_CLKOFF_SHFT                           0x1f

#define HWIO_MSS_BUS_SLAVE_TIMEOUT_CBCR_ADDR                              (MSS_PERPH_REG_BASE      + 0x000010c4)
#define HWIO_MSS_BUS_SLAVE_TIMEOUT_CBCR_RMSK                              0x80000001
#define HWIO_MSS_BUS_SLAVE_TIMEOUT_CBCR_IN          \
        in_dword_masked(HWIO_MSS_BUS_SLAVE_TIMEOUT_CBCR_ADDR, HWIO_MSS_BUS_SLAVE_TIMEOUT_CBCR_RMSK)
#define HWIO_MSS_BUS_SLAVE_TIMEOUT_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_BUS_SLAVE_TIMEOUT_CBCR_ADDR, m)
#define HWIO_MSS_BUS_SLAVE_TIMEOUT_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_BUS_SLAVE_TIMEOUT_CBCR_ADDR,v)
#define HWIO_MSS_BUS_SLAVE_TIMEOUT_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BUS_SLAVE_TIMEOUT_CBCR_ADDR,m,v,HWIO_MSS_BUS_SLAVE_TIMEOUT_CBCR_IN)
#define HWIO_MSS_BUS_SLAVE_TIMEOUT_CBCR_CLKOFF_BMSK                       0x80000000
#define HWIO_MSS_BUS_SLAVE_TIMEOUT_CBCR_CLKOFF_SHFT                             0x1f
#define HWIO_MSS_BUS_SLAVE_TIMEOUT_CBCR_CLKEN_BMSK                               0x1
#define HWIO_MSS_BUS_SLAVE_TIMEOUT_CBCR_CLKEN_SHFT                               0x0

#define HWIO_MSS_BUS_RBCPR_CBCR_ADDR                                      (MSS_PERPH_REG_BASE      + 0x000010c8)
#define HWIO_MSS_BUS_RBCPR_CBCR_RMSK                                      0x80000001
#define HWIO_MSS_BUS_RBCPR_CBCR_IN          \
        in_dword_masked(HWIO_MSS_BUS_RBCPR_CBCR_ADDR, HWIO_MSS_BUS_RBCPR_CBCR_RMSK)
#define HWIO_MSS_BUS_RBCPR_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_BUS_RBCPR_CBCR_ADDR, m)
#define HWIO_MSS_BUS_RBCPR_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_BUS_RBCPR_CBCR_ADDR,v)
#define HWIO_MSS_BUS_RBCPR_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BUS_RBCPR_CBCR_ADDR,m,v,HWIO_MSS_BUS_RBCPR_CBCR_IN)
#define HWIO_MSS_BUS_RBCPR_CBCR_CLKOFF_BMSK                               0x80000000
#define HWIO_MSS_BUS_RBCPR_CBCR_CLKOFF_SHFT                                     0x1f
#define HWIO_MSS_BUS_RBCPR_CBCR_CLKEN_BMSK                                       0x1
#define HWIO_MSS_BUS_RBCPR_CBCR_CLKEN_SHFT                                       0x0

#define HWIO_MSS_BUS_COMBODAC_COMP_CBCR_ADDR                              (MSS_PERPH_REG_BASE      + 0x000010cc)
#define HWIO_MSS_BUS_COMBODAC_COMP_CBCR_RMSK                              0x80000000
#define HWIO_MSS_BUS_COMBODAC_COMP_CBCR_IN          \
        in_dword_masked(HWIO_MSS_BUS_COMBODAC_COMP_CBCR_ADDR, HWIO_MSS_BUS_COMBODAC_COMP_CBCR_RMSK)
#define HWIO_MSS_BUS_COMBODAC_COMP_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_BUS_COMBODAC_COMP_CBCR_ADDR, m)
#define HWIO_MSS_BUS_COMBODAC_COMP_CBCR_CLKOFF_BMSK                       0x80000000
#define HWIO_MSS_BUS_COMBODAC_COMP_CBCR_CLKOFF_SHFT                             0x1f

#define HWIO_MSS_BUS_RBCPR_REF_CBCR_ADDR                                  (MSS_PERPH_REG_BASE      + 0x000010d0)
#define HWIO_MSS_BUS_RBCPR_REF_CBCR_RMSK                                  0x80000001
#define HWIO_MSS_BUS_RBCPR_REF_CBCR_IN          \
        in_dword_masked(HWIO_MSS_BUS_RBCPR_REF_CBCR_ADDR, HWIO_MSS_BUS_RBCPR_REF_CBCR_RMSK)
#define HWIO_MSS_BUS_RBCPR_REF_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_BUS_RBCPR_REF_CBCR_ADDR, m)
#define HWIO_MSS_BUS_RBCPR_REF_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_BUS_RBCPR_REF_CBCR_ADDR,v)
#define HWIO_MSS_BUS_RBCPR_REF_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BUS_RBCPR_REF_CBCR_ADDR,m,v,HWIO_MSS_BUS_RBCPR_REF_CBCR_IN)
#define HWIO_MSS_BUS_RBCPR_REF_CBCR_CLKOFF_BMSK                           0x80000000
#define HWIO_MSS_BUS_RBCPR_REF_CBCR_CLKOFF_SHFT                                 0x1f
#define HWIO_MSS_BUS_RBCPR_REF_CBCR_CLKEN_BMSK                                   0x1
#define HWIO_MSS_BUS_RBCPR_REF_CBCR_CLKEN_SHFT                                   0x0

#define HWIO_MSS_MODEM_CFG_AHB_CBCR_ADDR                                  (MSS_PERPH_REG_BASE      + 0x000010d4)
#define HWIO_MSS_MODEM_CFG_AHB_CBCR_RMSK                                  0x80000000
#define HWIO_MSS_MODEM_CFG_AHB_CBCR_IN          \
        in_dword_masked(HWIO_MSS_MODEM_CFG_AHB_CBCR_ADDR, HWIO_MSS_MODEM_CFG_AHB_CBCR_RMSK)
#define HWIO_MSS_MODEM_CFG_AHB_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_MODEM_CFG_AHB_CBCR_ADDR, m)
#define HWIO_MSS_MODEM_CFG_AHB_CBCR_CLKOFF_BMSK                           0x80000000
#define HWIO_MSS_MODEM_CFG_AHB_CBCR_CLKOFF_SHFT                                 0x1f

#define HWIO_MSS_MODEM_SNOC_AXI_CBCR_ADDR                                 (MSS_PERPH_REG_BASE      + 0x000010d8)
#define HWIO_MSS_MODEM_SNOC_AXI_CBCR_RMSK                                 0x80000000
#define HWIO_MSS_MODEM_SNOC_AXI_CBCR_IN          \
        in_dword_masked(HWIO_MSS_MODEM_SNOC_AXI_CBCR_ADDR, HWIO_MSS_MODEM_SNOC_AXI_CBCR_RMSK)
#define HWIO_MSS_MODEM_SNOC_AXI_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_MODEM_SNOC_AXI_CBCR_ADDR, m)
#define HWIO_MSS_MODEM_SNOC_AXI_CBCR_CLKOFF_BMSK                          0x80000000
#define HWIO_MSS_MODEM_SNOC_AXI_CBCR_CLKOFF_SHFT                                0x1f

#define HWIO_MSS_NAV_SNOC_AXI_CBCR_ADDR                                   (MSS_PERPH_REG_BASE      + 0x000010dc)
#define HWIO_MSS_NAV_SNOC_AXI_CBCR_RMSK                                   0x80000001
#define HWIO_MSS_NAV_SNOC_AXI_CBCR_IN          \
        in_dword_masked(HWIO_MSS_NAV_SNOC_AXI_CBCR_ADDR, HWIO_MSS_NAV_SNOC_AXI_CBCR_RMSK)
#define HWIO_MSS_NAV_SNOC_AXI_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_NAV_SNOC_AXI_CBCR_ADDR, m)
#define HWIO_MSS_NAV_SNOC_AXI_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_NAV_SNOC_AXI_CBCR_ADDR,v)
#define HWIO_MSS_NAV_SNOC_AXI_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_NAV_SNOC_AXI_CBCR_ADDR,m,v,HWIO_MSS_NAV_SNOC_AXI_CBCR_IN)
#define HWIO_MSS_NAV_SNOC_AXI_CBCR_CLKOFF_BMSK                            0x80000000
#define HWIO_MSS_NAV_SNOC_AXI_CBCR_CLKOFF_SHFT                                  0x1f
#define HWIO_MSS_NAV_SNOC_AXI_CBCR_CLKEN_BMSK                                    0x1
#define HWIO_MSS_NAV_SNOC_AXI_CBCR_CLKEN_SHFT                                    0x0

#define HWIO_MSS_VMIDMT_SNOC_AXI_CBCR_ADDR                                (MSS_PERPH_REG_BASE      + 0x000010e0)
#define HWIO_MSS_VMIDMT_SNOC_AXI_CBCR_RMSK                                0x80000001
#define HWIO_MSS_VMIDMT_SNOC_AXI_CBCR_IN          \
        in_dword_masked(HWIO_MSS_VMIDMT_SNOC_AXI_CBCR_ADDR, HWIO_MSS_VMIDMT_SNOC_AXI_CBCR_RMSK)
#define HWIO_MSS_VMIDMT_SNOC_AXI_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_VMIDMT_SNOC_AXI_CBCR_ADDR, m)
#define HWIO_MSS_VMIDMT_SNOC_AXI_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_VMIDMT_SNOC_AXI_CBCR_ADDR,v)
#define HWIO_MSS_VMIDMT_SNOC_AXI_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_VMIDMT_SNOC_AXI_CBCR_ADDR,m,v,HWIO_MSS_VMIDMT_SNOC_AXI_CBCR_IN)
#define HWIO_MSS_VMIDMT_SNOC_AXI_CBCR_CLKOFF_BMSK                         0x80000000
#define HWIO_MSS_VMIDMT_SNOC_AXI_CBCR_CLKOFF_SHFT                               0x1f
#define HWIO_MSS_VMIDMT_SNOC_AXI_CBCR_CLKEN_BMSK                                 0x1
#define HWIO_MSS_VMIDMT_SNOC_AXI_CBCR_CLKEN_SHFT                                 0x0

#define HWIO_MSS_MPLL0_MAIN_MODEM_CX_TO_LS_CBCR_ADDR                      (MSS_PERPH_REG_BASE      + 0x000010e4)
#define HWIO_MSS_MPLL0_MAIN_MODEM_CX_TO_LS_CBCR_RMSK                      0x80000001
#define HWIO_MSS_MPLL0_MAIN_MODEM_CX_TO_LS_CBCR_IN          \
        in_dword_masked(HWIO_MSS_MPLL0_MAIN_MODEM_CX_TO_LS_CBCR_ADDR, HWIO_MSS_MPLL0_MAIN_MODEM_CX_TO_LS_CBCR_RMSK)
#define HWIO_MSS_MPLL0_MAIN_MODEM_CX_TO_LS_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_MPLL0_MAIN_MODEM_CX_TO_LS_CBCR_ADDR, m)
#define HWIO_MSS_MPLL0_MAIN_MODEM_CX_TO_LS_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_MPLL0_MAIN_MODEM_CX_TO_LS_CBCR_ADDR,v)
#define HWIO_MSS_MPLL0_MAIN_MODEM_CX_TO_LS_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_MPLL0_MAIN_MODEM_CX_TO_LS_CBCR_ADDR,m,v,HWIO_MSS_MPLL0_MAIN_MODEM_CX_TO_LS_CBCR_IN)
#define HWIO_MSS_MPLL0_MAIN_MODEM_CX_TO_LS_CBCR_CLKOFF_BMSK               0x80000000
#define HWIO_MSS_MPLL0_MAIN_MODEM_CX_TO_LS_CBCR_CLKOFF_SHFT                     0x1f
#define HWIO_MSS_MPLL0_MAIN_MODEM_CX_TO_LS_CBCR_CLKEN_BMSK                       0x1
#define HWIO_MSS_MPLL0_MAIN_MODEM_CX_TO_LS_CBCR_CLKEN_SHFT                       0x0

#define HWIO_MSS_MPLL1_MAIN_CBCR_ADDR                                     (MSS_PERPH_REG_BASE      + 0x000010e8)
#define HWIO_MSS_MPLL1_MAIN_CBCR_RMSK                                     0x80000001
#define HWIO_MSS_MPLL1_MAIN_CBCR_IN          \
        in_dword_masked(HWIO_MSS_MPLL1_MAIN_CBCR_ADDR, HWIO_MSS_MPLL1_MAIN_CBCR_RMSK)
#define HWIO_MSS_MPLL1_MAIN_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_MPLL1_MAIN_CBCR_ADDR, m)
#define HWIO_MSS_MPLL1_MAIN_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_MPLL1_MAIN_CBCR_ADDR,v)
#define HWIO_MSS_MPLL1_MAIN_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_MPLL1_MAIN_CBCR_ADDR,m,v,HWIO_MSS_MPLL1_MAIN_CBCR_IN)
#define HWIO_MSS_MPLL1_MAIN_CBCR_CLKOFF_BMSK                              0x80000000
#define HWIO_MSS_MPLL1_MAIN_CBCR_CLKOFF_SHFT                                    0x1f
#define HWIO_MSS_MPLL1_MAIN_CBCR_CLKEN_BMSK                                      0x1
#define HWIO_MSS_MPLL1_MAIN_CBCR_CLKEN_SHFT                                      0x0

#define HWIO_MSS_MPLL1_MAIN_BUS_CBCR_ADDR                                 (MSS_PERPH_REG_BASE      + 0x000010ec)
#define HWIO_MSS_MPLL1_MAIN_BUS_CBCR_RMSK                                 0x80000000
#define HWIO_MSS_MPLL1_MAIN_BUS_CBCR_IN          \
        in_dword_masked(HWIO_MSS_MPLL1_MAIN_BUS_CBCR_ADDR, HWIO_MSS_MPLL1_MAIN_BUS_CBCR_RMSK)
#define HWIO_MSS_MPLL1_MAIN_BUS_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_MPLL1_MAIN_BUS_CBCR_ADDR, m)
#define HWIO_MSS_MPLL1_MAIN_BUS_CBCR_CLKOFF_BMSK                          0x80000000
#define HWIO_MSS_MPLL1_MAIN_BUS_CBCR_CLKOFF_SHFT                                0x1f

#define HWIO_MSS_MPLL1_EARLY_DIV5_CBCR_ADDR                               (MSS_PERPH_REG_BASE      + 0x000010f0)
#define HWIO_MSS_MPLL1_EARLY_DIV5_CBCR_RMSK                               0x80000001
#define HWIO_MSS_MPLL1_EARLY_DIV5_CBCR_IN          \
        in_dword_masked(HWIO_MSS_MPLL1_EARLY_DIV5_CBCR_ADDR, HWIO_MSS_MPLL1_EARLY_DIV5_CBCR_RMSK)
#define HWIO_MSS_MPLL1_EARLY_DIV5_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_MPLL1_EARLY_DIV5_CBCR_ADDR, m)
#define HWIO_MSS_MPLL1_EARLY_DIV5_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_MPLL1_EARLY_DIV5_CBCR_ADDR,v)
#define HWIO_MSS_MPLL1_EARLY_DIV5_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_MPLL1_EARLY_DIV5_CBCR_ADDR,m,v,HWIO_MSS_MPLL1_EARLY_DIV5_CBCR_IN)
#define HWIO_MSS_MPLL1_EARLY_DIV5_CBCR_CLKOFF_BMSK                        0x80000000
#define HWIO_MSS_MPLL1_EARLY_DIV5_CBCR_CLKOFF_SHFT                              0x1f
#define HWIO_MSS_MPLL1_EARLY_DIV5_CBCR_CLKEN_BMSK                                0x1
#define HWIO_MSS_MPLL1_EARLY_DIV5_CBCR_CLKEN_SHFT                                0x0

#define HWIO_MSS_MPLL1_EARLY_DIV3_CBCR_ADDR                               (MSS_PERPH_REG_BASE      + 0x000010f4)
#define HWIO_MSS_MPLL1_EARLY_DIV3_CBCR_RMSK                               0x80000001
#define HWIO_MSS_MPLL1_EARLY_DIV3_CBCR_IN          \
        in_dword_masked(HWIO_MSS_MPLL1_EARLY_DIV3_CBCR_ADDR, HWIO_MSS_MPLL1_EARLY_DIV3_CBCR_RMSK)
#define HWIO_MSS_MPLL1_EARLY_DIV3_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_MPLL1_EARLY_DIV3_CBCR_ADDR, m)
#define HWIO_MSS_MPLL1_EARLY_DIV3_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_MPLL1_EARLY_DIV3_CBCR_ADDR,v)
#define HWIO_MSS_MPLL1_EARLY_DIV3_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_MPLL1_EARLY_DIV3_CBCR_ADDR,m,v,HWIO_MSS_MPLL1_EARLY_DIV3_CBCR_IN)
#define HWIO_MSS_MPLL1_EARLY_DIV3_CBCR_CLKOFF_BMSK                        0x80000000
#define HWIO_MSS_MPLL1_EARLY_DIV3_CBCR_CLKOFF_SHFT                              0x1f
#define HWIO_MSS_MPLL1_EARLY_DIV3_CBCR_CLKEN_BMSK                                0x1
#define HWIO_MSS_MPLL1_EARLY_DIV3_CBCR_CLKEN_SHFT                                0x0

#define HWIO_MSS_BIT_COXM_CBCR_ADDR                                       (MSS_PERPH_REG_BASE      + 0x000010f8)
#define HWIO_MSS_BIT_COXM_CBCR_RMSK                                       0x80000001
#define HWIO_MSS_BIT_COXM_CBCR_IN          \
        in_dword_masked(HWIO_MSS_BIT_COXM_CBCR_ADDR, HWIO_MSS_BIT_COXM_CBCR_RMSK)
#define HWIO_MSS_BIT_COXM_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_BIT_COXM_CBCR_ADDR, m)
#define HWIO_MSS_BIT_COXM_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_BIT_COXM_CBCR_ADDR,v)
#define HWIO_MSS_BIT_COXM_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BIT_COXM_CBCR_ADDR,m,v,HWIO_MSS_BIT_COXM_CBCR_IN)
#define HWIO_MSS_BIT_COXM_CBCR_CLKOFF_BMSK                                0x80000000
#define HWIO_MSS_BIT_COXM_CBCR_CLKOFF_SHFT                                      0x1f
#define HWIO_MSS_BIT_COXM_CBCR_CLKEN_BMSK                                        0x1
#define HWIO_MSS_BIT_COXM_CBCR_CLKEN_SHFT                                        0x0

#define HWIO_MSS_SLEEP_Q6_CBCR_ADDR                                       (MSS_PERPH_REG_BASE      + 0x000010fc)
#define HWIO_MSS_SLEEP_Q6_CBCR_RMSK                                       0x80000001
#define HWIO_MSS_SLEEP_Q6_CBCR_IN          \
        in_dword_masked(HWIO_MSS_SLEEP_Q6_CBCR_ADDR, HWIO_MSS_SLEEP_Q6_CBCR_RMSK)
#define HWIO_MSS_SLEEP_Q6_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_SLEEP_Q6_CBCR_ADDR, m)
#define HWIO_MSS_SLEEP_Q6_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_SLEEP_Q6_CBCR_ADDR,v)
#define HWIO_MSS_SLEEP_Q6_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_SLEEP_Q6_CBCR_ADDR,m,v,HWIO_MSS_SLEEP_Q6_CBCR_IN)
#define HWIO_MSS_SLEEP_Q6_CBCR_CLKOFF_BMSK                                0x80000000
#define HWIO_MSS_SLEEP_Q6_CBCR_CLKOFF_SHFT                                      0x1f
#define HWIO_MSS_SLEEP_Q6_CBCR_CLKEN_BMSK                                        0x1
#define HWIO_MSS_SLEEP_Q6_CBCR_CLKEN_SHFT                                        0x0

#define HWIO_MSS_MPLL1_OUT_EARLY_DIV3_CBCR_ADDR                           (MSS_PERPH_REG_BASE      + 0x00001100)
#define HWIO_MSS_MPLL1_OUT_EARLY_DIV3_CBCR_RMSK                           0x80000001
#define HWIO_MSS_MPLL1_OUT_EARLY_DIV3_CBCR_IN          \
        in_dword_masked(HWIO_MSS_MPLL1_OUT_EARLY_DIV3_CBCR_ADDR, HWIO_MSS_MPLL1_OUT_EARLY_DIV3_CBCR_RMSK)
#define HWIO_MSS_MPLL1_OUT_EARLY_DIV3_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_MPLL1_OUT_EARLY_DIV3_CBCR_ADDR, m)
#define HWIO_MSS_MPLL1_OUT_EARLY_DIV3_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_MPLL1_OUT_EARLY_DIV3_CBCR_ADDR,v)
#define HWIO_MSS_MPLL1_OUT_EARLY_DIV3_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_MPLL1_OUT_EARLY_DIV3_CBCR_ADDR,m,v,HWIO_MSS_MPLL1_OUT_EARLY_DIV3_CBCR_IN)
#define HWIO_MSS_MPLL1_OUT_EARLY_DIV3_CBCR_CLKOFF_BMSK                    0x80000000
#define HWIO_MSS_MPLL1_OUT_EARLY_DIV3_CBCR_CLKOFF_SHFT                          0x1f
#define HWIO_MSS_MPLL1_OUT_EARLY_DIV3_CBCR_CLKEN_BMSK                            0x1
#define HWIO_MSS_MPLL1_OUT_EARLY_DIV3_CBCR_CLKEN_SHFT                            0x0

#define HWIO_MSS_MPLL1_OUT_EARLY_DIV5_CBCR_ADDR                           (MSS_PERPH_REG_BASE      + 0x00001104)
#define HWIO_MSS_MPLL1_OUT_EARLY_DIV5_CBCR_RMSK                           0x80000001
#define HWIO_MSS_MPLL1_OUT_EARLY_DIV5_CBCR_IN          \
        in_dword_masked(HWIO_MSS_MPLL1_OUT_EARLY_DIV5_CBCR_ADDR, HWIO_MSS_MPLL1_OUT_EARLY_DIV5_CBCR_RMSK)
#define HWIO_MSS_MPLL1_OUT_EARLY_DIV5_CBCR_INM(m)      \
        in_dword_masked(HWIO_MSS_MPLL1_OUT_EARLY_DIV5_CBCR_ADDR, m)
#define HWIO_MSS_MPLL1_OUT_EARLY_DIV5_CBCR_OUT(v)      \
        out_dword(HWIO_MSS_MPLL1_OUT_EARLY_DIV5_CBCR_ADDR,v)
#define HWIO_MSS_MPLL1_OUT_EARLY_DIV5_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_MPLL1_OUT_EARLY_DIV5_CBCR_ADDR,m,v,HWIO_MSS_MPLL1_OUT_EARLY_DIV5_CBCR_IN)
#define HWIO_MSS_MPLL1_OUT_EARLY_DIV5_CBCR_CLKOFF_BMSK                    0x80000000
#define HWIO_MSS_MPLL1_OUT_EARLY_DIV5_CBCR_CLKOFF_SHFT                          0x1f
#define HWIO_MSS_MPLL1_OUT_EARLY_DIV5_CBCR_CLKEN_BMSK                            0x1
#define HWIO_MSS_MPLL1_OUT_EARLY_DIV5_CBCR_CLKEN_SHFT                            0x0

#define HWIO_MSS_BUS_CMD_RCGR_ADDR                                        (MSS_PERPH_REG_BASE      + 0x00001108)
#define HWIO_MSS_BUS_CMD_RCGR_RMSK                                        0x80000001
#define HWIO_MSS_BUS_CMD_RCGR_IN          \
        in_dword_masked(HWIO_MSS_BUS_CMD_RCGR_ADDR, HWIO_MSS_BUS_CMD_RCGR_RMSK)
#define HWIO_MSS_BUS_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_MSS_BUS_CMD_RCGR_ADDR, m)
#define HWIO_MSS_BUS_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_MSS_BUS_CMD_RCGR_ADDR,v)
#define HWIO_MSS_BUS_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BUS_CMD_RCGR_ADDR,m,v,HWIO_MSS_BUS_CMD_RCGR_IN)
#define HWIO_MSS_BUS_CMD_RCGR_ROOT_OFF_BMSK                               0x80000000
#define HWIO_MSS_BUS_CMD_RCGR_ROOT_OFF_SHFT                                     0x1f
#define HWIO_MSS_BUS_CMD_RCGR_UPDATE_BMSK                                        0x1
#define HWIO_MSS_BUS_CMD_RCGR_UPDATE_SHFT                                        0x0

#define HWIO_MSS_BUS_CFG_RCGR_ADDR                                        (MSS_PERPH_REG_BASE      + 0x0000110c)
#define HWIO_MSS_BUS_CFG_RCGR_RMSK                                             0x71f
#define HWIO_MSS_BUS_CFG_RCGR_IN          \
        in_dword_masked(HWIO_MSS_BUS_CFG_RCGR_ADDR, HWIO_MSS_BUS_CFG_RCGR_RMSK)
#define HWIO_MSS_BUS_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_MSS_BUS_CFG_RCGR_ADDR, m)
#define HWIO_MSS_BUS_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_MSS_BUS_CFG_RCGR_ADDR,v)
#define HWIO_MSS_BUS_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BUS_CFG_RCGR_ADDR,m,v,HWIO_MSS_BUS_CFG_RCGR_IN)
#define HWIO_MSS_BUS_CFG_RCGR_SRC_SEL_BMSK                                     0x700
#define HWIO_MSS_BUS_CFG_RCGR_SRC_SEL_SHFT                                       0x8
#define HWIO_MSS_BUS_CFG_RCGR_SRC_DIV_BMSK                                      0x1f
#define HWIO_MSS_BUS_CFG_RCGR_SRC_DIV_SHFT                                       0x0

#define HWIO_MSS_Q6_CMD_RCGR_ADDR                                         (MSS_PERPH_REG_BASE      + 0x00001110)
#define HWIO_MSS_Q6_CMD_RCGR_RMSK                                         0x80000003
#define HWIO_MSS_Q6_CMD_RCGR_IN          \
        in_dword_masked(HWIO_MSS_Q6_CMD_RCGR_ADDR, HWIO_MSS_Q6_CMD_RCGR_RMSK)
#define HWIO_MSS_Q6_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_MSS_Q6_CMD_RCGR_ADDR, m)
#define HWIO_MSS_Q6_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_MSS_Q6_CMD_RCGR_ADDR,v)
#define HWIO_MSS_Q6_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_Q6_CMD_RCGR_ADDR,m,v,HWIO_MSS_Q6_CMD_RCGR_IN)
#define HWIO_MSS_Q6_CMD_RCGR_ROOT_OFF_BMSK                                0x80000000
#define HWIO_MSS_Q6_CMD_RCGR_ROOT_OFF_SHFT                                      0x1f
#define HWIO_MSS_Q6_CMD_RCGR_ROOT_EN_BMSK                                        0x2
#define HWIO_MSS_Q6_CMD_RCGR_ROOT_EN_SHFT                                        0x1
#define HWIO_MSS_Q6_CMD_RCGR_UPDATE_BMSK                                         0x1
#define HWIO_MSS_Q6_CMD_RCGR_UPDATE_SHFT                                         0x0

#define HWIO_MSS_Q6_CFG_RCGR_ADDR                                         (MSS_PERPH_REG_BASE      + 0x00001114)
#define HWIO_MSS_Q6_CFG_RCGR_RMSK                                              0x71f
#define HWIO_MSS_Q6_CFG_RCGR_IN          \
        in_dword_masked(HWIO_MSS_Q6_CFG_RCGR_ADDR, HWIO_MSS_Q6_CFG_RCGR_RMSK)
#define HWIO_MSS_Q6_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_MSS_Q6_CFG_RCGR_ADDR, m)
#define HWIO_MSS_Q6_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_MSS_Q6_CFG_RCGR_ADDR,v)
#define HWIO_MSS_Q6_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_Q6_CFG_RCGR_ADDR,m,v,HWIO_MSS_Q6_CFG_RCGR_IN)
#define HWIO_MSS_Q6_CFG_RCGR_SRC_SEL_BMSK                                      0x700
#define HWIO_MSS_Q6_CFG_RCGR_SRC_SEL_SHFT                                        0x8
#define HWIO_MSS_Q6_CFG_RCGR_SRC_DIV_BMSK                                       0x1f
#define HWIO_MSS_Q6_CFG_RCGR_SRC_DIV_SHFT                                        0x0

#define HWIO_MSS_UIM_CMD_RCGR_ADDR                                        (MSS_PERPH_REG_BASE      + 0x00001118)
#define HWIO_MSS_UIM_CMD_RCGR_RMSK                                        0x80000003
#define HWIO_MSS_UIM_CMD_RCGR_IN          \
        in_dword_masked(HWIO_MSS_UIM_CMD_RCGR_ADDR, HWIO_MSS_UIM_CMD_RCGR_RMSK)
#define HWIO_MSS_UIM_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_MSS_UIM_CMD_RCGR_ADDR, m)
#define HWIO_MSS_UIM_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_MSS_UIM_CMD_RCGR_ADDR,v)
#define HWIO_MSS_UIM_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_UIM_CMD_RCGR_ADDR,m,v,HWIO_MSS_UIM_CMD_RCGR_IN)
#define HWIO_MSS_UIM_CMD_RCGR_ROOT_OFF_BMSK                               0x80000000
#define HWIO_MSS_UIM_CMD_RCGR_ROOT_OFF_SHFT                                     0x1f
#define HWIO_MSS_UIM_CMD_RCGR_ROOT_EN_BMSK                                       0x2
#define HWIO_MSS_UIM_CMD_RCGR_ROOT_EN_SHFT                                       0x1
#define HWIO_MSS_UIM_CMD_RCGR_UPDATE_BMSK                                        0x1
#define HWIO_MSS_UIM_CMD_RCGR_UPDATE_SHFT                                        0x0

#define HWIO_MSS_UIM_CFG_RCGR_ADDR                                        (MSS_PERPH_REG_BASE      + 0x0000111c)
#define HWIO_MSS_UIM_CFG_RCGR_RMSK                                              0x1f
#define HWIO_MSS_UIM_CFG_RCGR_IN          \
        in_dword_masked(HWIO_MSS_UIM_CFG_RCGR_ADDR, HWIO_MSS_UIM_CFG_RCGR_RMSK)
#define HWIO_MSS_UIM_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_MSS_UIM_CFG_RCGR_ADDR, m)
#define HWIO_MSS_UIM_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_MSS_UIM_CFG_RCGR_ADDR,v)
#define HWIO_MSS_UIM_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_UIM_CFG_RCGR_ADDR,m,v,HWIO_MSS_UIM_CFG_RCGR_IN)
#define HWIO_MSS_UIM_CFG_RCGR_SRC_DIV_BMSK                                      0x1f
#define HWIO_MSS_UIM_CFG_RCGR_SRC_DIV_SHFT                                       0x0

#define HWIO_MSS_BBRX0_ACGC_ADDR                                          (MSS_PERPH_REG_BASE      + 0x00001120)
#define HWIO_MSS_BBRX0_ACGC_RMSK                                                 0x2
#define HWIO_MSS_BBRX0_ACGC_IN          \
        in_dword_masked(HWIO_MSS_BBRX0_ACGC_ADDR, HWIO_MSS_BBRX0_ACGC_RMSK)
#define HWIO_MSS_BBRX0_ACGC_INM(m)      \
        in_dword_masked(HWIO_MSS_BBRX0_ACGC_ADDR, m)
#define HWIO_MSS_BBRX0_ACGC_CLKOFF_BMSK                                          0x2
#define HWIO_MSS_BBRX0_ACGC_CLKOFF_SHFT                                          0x1

#define HWIO_MSS_BBRX1_ACGC_ADDR                                          (MSS_PERPH_REG_BASE      + 0x00001124)
#define HWIO_MSS_BBRX1_ACGC_RMSK                                                 0x2
#define HWIO_MSS_BBRX1_ACGC_IN          \
        in_dword_masked(HWIO_MSS_BBRX1_ACGC_ADDR, HWIO_MSS_BBRX1_ACGC_RMSK)
#define HWIO_MSS_BBRX1_ACGC_INM(m)      \
        in_dword_masked(HWIO_MSS_BBRX1_ACGC_ADDR, m)
#define HWIO_MSS_BBRX1_ACGC_CLKOFF_BMSK                                          0x2
#define HWIO_MSS_BBRX1_ACGC_CLKOFF_SHFT                                          0x1

#define HWIO_MSS_BBRX2_ACGC_ADDR                                          (MSS_PERPH_REG_BASE      + 0x00001128)
#define HWIO_MSS_BBRX2_ACGC_RMSK                                                 0x2
#define HWIO_MSS_BBRX2_ACGC_IN          \
        in_dword_masked(HWIO_MSS_BBRX2_ACGC_ADDR, HWIO_MSS_BBRX2_ACGC_RMSK)
#define HWIO_MSS_BBRX2_ACGC_INM(m)      \
        in_dword_masked(HWIO_MSS_BBRX2_ACGC_ADDR, m)
#define HWIO_MSS_BBRX2_ACGC_CLKOFF_BMSK                                          0x2
#define HWIO_MSS_BBRX2_ACGC_CLKOFF_SHFT                                          0x1

#define HWIO_MSS_BBRX3_ACGC_ADDR                                          (MSS_PERPH_REG_BASE      + 0x0000112c)
#define HWIO_MSS_BBRX3_ACGC_RMSK                                                 0x2
#define HWIO_MSS_BBRX3_ACGC_IN          \
        in_dword_masked(HWIO_MSS_BBRX3_ACGC_ADDR, HWIO_MSS_BBRX3_ACGC_RMSK)
#define HWIO_MSS_BBRX3_ACGC_INM(m)      \
        in_dword_masked(HWIO_MSS_BBRX3_ACGC_ADDR, m)
#define HWIO_MSS_BBRX3_ACGC_CLKOFF_BMSK                                          0x2
#define HWIO_MSS_BBRX3_ACGC_CLKOFF_SHFT                                          0x1

#define HWIO_MSS_RESERVE_01_ADDR                                          (MSS_PERPH_REG_BASE      + 0x00001130)
#define HWIO_MSS_RESERVE_01_RMSK                                          0xffffffff
#define HWIO_MSS_RESERVE_01_IN          \
        in_dword_masked(HWIO_MSS_RESERVE_01_ADDR, HWIO_MSS_RESERVE_01_RMSK)
#define HWIO_MSS_RESERVE_01_INM(m)      \
        in_dword_masked(HWIO_MSS_RESERVE_01_ADDR, m)
#define HWIO_MSS_RESERVE_01_OUT(v)      \
        out_dword(HWIO_MSS_RESERVE_01_ADDR,v)
#define HWIO_MSS_RESERVE_01_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_RESERVE_01_ADDR,m,v,HWIO_MSS_RESERVE_01_IN)
#define HWIO_MSS_RESERVE_01_MSS_RESERVE_01_BMSK                           0xffffffff
#define HWIO_MSS_RESERVE_01_MSS_RESERVE_01_SHFT                                  0x0

#define HWIO_MSS_RESERVE_02_ADDR                                          (MSS_PERPH_REG_BASE      + 0x00001134)
#define HWIO_MSS_RESERVE_02_RMSK                                          0xffffffff
#define HWIO_MSS_RESERVE_02_IN          \
        in_dword_masked(HWIO_MSS_RESERVE_02_ADDR, HWIO_MSS_RESERVE_02_RMSK)
#define HWIO_MSS_RESERVE_02_INM(m)      \
        in_dword_masked(HWIO_MSS_RESERVE_02_ADDR, m)
#define HWIO_MSS_RESERVE_02_OUT(v)      \
        out_dword(HWIO_MSS_RESERVE_02_ADDR,v)
#define HWIO_MSS_RESERVE_02_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_RESERVE_02_ADDR,m,v,HWIO_MSS_RESERVE_02_IN)
#define HWIO_MSS_RESERVE_02_MSS_RESERVE_02_BMSK                           0xffffffff
#define HWIO_MSS_RESERVE_02_MSS_RESERVE_02_SHFT                                  0x0

#define HWIO_MSS_BBRX0_MISC_ADDR                                          (MSS_PERPH_REG_BASE      + 0x00001138)
#define HWIO_MSS_BBRX0_MISC_RMSK                                                 0xf
#define HWIO_MSS_BBRX0_MISC_IN          \
        in_dword_masked(HWIO_MSS_BBRX0_MISC_ADDR, HWIO_MSS_BBRX0_MISC_RMSK)
#define HWIO_MSS_BBRX0_MISC_INM(m)      \
        in_dword_masked(HWIO_MSS_BBRX0_MISC_ADDR, m)
#define HWIO_MSS_BBRX0_MISC_OUT(v)      \
        out_dword(HWIO_MSS_BBRX0_MISC_ADDR,v)
#define HWIO_MSS_BBRX0_MISC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BBRX0_MISC_ADDR,m,v,HWIO_MSS_BBRX0_MISC_IN)
#define HWIO_MSS_BBRX0_MISC_SRC_DIV_BMSK                                         0xf
#define HWIO_MSS_BBRX0_MISC_SRC_DIV_SHFT                                         0x0

#define HWIO_MSS_BBRX1_MISC_ADDR                                          (MSS_PERPH_REG_BASE      + 0x0000113c)
#define HWIO_MSS_BBRX1_MISC_RMSK                                                 0xf
#define HWIO_MSS_BBRX1_MISC_IN          \
        in_dword_masked(HWIO_MSS_BBRX1_MISC_ADDR, HWIO_MSS_BBRX1_MISC_RMSK)
#define HWIO_MSS_BBRX1_MISC_INM(m)      \
        in_dword_masked(HWIO_MSS_BBRX1_MISC_ADDR, m)
#define HWIO_MSS_BBRX1_MISC_OUT(v)      \
        out_dword(HWIO_MSS_BBRX1_MISC_ADDR,v)
#define HWIO_MSS_BBRX1_MISC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BBRX1_MISC_ADDR,m,v,HWIO_MSS_BBRX1_MISC_IN)
#define HWIO_MSS_BBRX1_MISC_SRC_DIV_BMSK                                         0xf
#define HWIO_MSS_BBRX1_MISC_SRC_DIV_SHFT                                         0x0

#define HWIO_MSS_BBRX2_MISC_ADDR                                          (MSS_PERPH_REG_BASE      + 0x00001140)
#define HWIO_MSS_BBRX2_MISC_RMSK                                                 0xf
#define HWIO_MSS_BBRX2_MISC_IN          \
        in_dword_masked(HWIO_MSS_BBRX2_MISC_ADDR, HWIO_MSS_BBRX2_MISC_RMSK)
#define HWIO_MSS_BBRX2_MISC_INM(m)      \
        in_dword_masked(HWIO_MSS_BBRX2_MISC_ADDR, m)
#define HWIO_MSS_BBRX2_MISC_OUT(v)      \
        out_dword(HWIO_MSS_BBRX2_MISC_ADDR,v)
#define HWIO_MSS_BBRX2_MISC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BBRX2_MISC_ADDR,m,v,HWIO_MSS_BBRX2_MISC_IN)
#define HWIO_MSS_BBRX2_MISC_SRC_DIV_BMSK                                         0xf
#define HWIO_MSS_BBRX2_MISC_SRC_DIV_SHFT                                         0x0

#define HWIO_MSS_BBRX3_MISC_ADDR                                          (MSS_PERPH_REG_BASE      + 0x00001144)
#define HWIO_MSS_BBRX3_MISC_RMSK                                                 0xf
#define HWIO_MSS_BBRX3_MISC_IN          \
        in_dword_masked(HWIO_MSS_BBRX3_MISC_ADDR, HWIO_MSS_BBRX3_MISC_RMSK)
#define HWIO_MSS_BBRX3_MISC_INM(m)      \
        in_dword_masked(HWIO_MSS_BBRX3_MISC_ADDR, m)
#define HWIO_MSS_BBRX3_MISC_OUT(v)      \
        out_dword(HWIO_MSS_BBRX3_MISC_ADDR,v)
#define HWIO_MSS_BBRX3_MISC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BBRX3_MISC_ADDR,m,v,HWIO_MSS_BBRX3_MISC_IN)
#define HWIO_MSS_BBRX3_MISC_SRC_DIV_BMSK                                         0xf
#define HWIO_MSS_BBRX3_MISC_SRC_DIV_SHFT                                         0x0

#define HWIO_MSS_BIT_COXM_DIV_MISC_ADDR                                   (MSS_PERPH_REG_BASE      + 0x00001148)
#define HWIO_MSS_BIT_COXM_DIV_MISC_RMSK                                          0xf
#define HWIO_MSS_BIT_COXM_DIV_MISC_IN          \
        in_dword_masked(HWIO_MSS_BIT_COXM_DIV_MISC_ADDR, HWIO_MSS_BIT_COXM_DIV_MISC_RMSK)
#define HWIO_MSS_BIT_COXM_DIV_MISC_INM(m)      \
        in_dword_masked(HWIO_MSS_BIT_COXM_DIV_MISC_ADDR, m)
#define HWIO_MSS_BIT_COXM_DIV_MISC_OUT(v)      \
        out_dword(HWIO_MSS_BIT_COXM_DIV_MISC_ADDR,v)
#define HWIO_MSS_BIT_COXM_DIV_MISC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BIT_COXM_DIV_MISC_ADDR,m,v,HWIO_MSS_BIT_COXM_DIV_MISC_IN)
#define HWIO_MSS_BIT_COXM_DIV_MISC_SRC_DIV_BMSK                                  0xf
#define HWIO_MSS_BIT_COXM_DIV_MISC_SRC_DIV_SHFT                                  0x0

#define HWIO_MSS_BUS_RBCPR_REF_DIV_MISC_ADDR                              (MSS_PERPH_REG_BASE      + 0x0000114c)
#define HWIO_MSS_BUS_RBCPR_REF_DIV_MISC_RMSK                                     0x3
#define HWIO_MSS_BUS_RBCPR_REF_DIV_MISC_IN          \
        in_dword_masked(HWIO_MSS_BUS_RBCPR_REF_DIV_MISC_ADDR, HWIO_MSS_BUS_RBCPR_REF_DIV_MISC_RMSK)
#define HWIO_MSS_BUS_RBCPR_REF_DIV_MISC_INM(m)      \
        in_dword_masked(HWIO_MSS_BUS_RBCPR_REF_DIV_MISC_ADDR, m)
#define HWIO_MSS_BUS_RBCPR_REF_DIV_MISC_OUT(v)      \
        out_dword(HWIO_MSS_BUS_RBCPR_REF_DIV_MISC_ADDR,v)
#define HWIO_MSS_BUS_RBCPR_REF_DIV_MISC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_BUS_RBCPR_REF_DIV_MISC_ADDR,m,v,HWIO_MSS_BUS_RBCPR_REF_DIV_MISC_IN)
#define HWIO_MSS_BUS_RBCPR_REF_DIV_MISC_SRC_DIV_BMSK                             0x3
#define HWIO_MSS_BUS_RBCPR_REF_DIV_MISC_SRC_DIV_SHFT                             0x0

#define HWIO_MSS_UIM0_MND_CMD_RCGR_ADDR                                   (MSS_PERPH_REG_BASE      + 0x00001150)
#define HWIO_MSS_UIM0_MND_CMD_RCGR_RMSK                                   0x80000003
#define HWIO_MSS_UIM0_MND_CMD_RCGR_IN          \
        in_dword_masked(HWIO_MSS_UIM0_MND_CMD_RCGR_ADDR, HWIO_MSS_UIM0_MND_CMD_RCGR_RMSK)
#define HWIO_MSS_UIM0_MND_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_MSS_UIM0_MND_CMD_RCGR_ADDR, m)
#define HWIO_MSS_UIM0_MND_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_MSS_UIM0_MND_CMD_RCGR_ADDR,v)
#define HWIO_MSS_UIM0_MND_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_UIM0_MND_CMD_RCGR_ADDR,m,v,HWIO_MSS_UIM0_MND_CMD_RCGR_IN)
#define HWIO_MSS_UIM0_MND_CMD_RCGR_ROOT_OFF_BMSK                          0x80000000
#define HWIO_MSS_UIM0_MND_CMD_RCGR_ROOT_OFF_SHFT                                0x1f
#define HWIO_MSS_UIM0_MND_CMD_RCGR_ROOT_EN_BMSK                                  0x2
#define HWIO_MSS_UIM0_MND_CMD_RCGR_ROOT_EN_SHFT                                  0x1
#define HWIO_MSS_UIM0_MND_CMD_RCGR_UPDATE_BMSK                                   0x1
#define HWIO_MSS_UIM0_MND_CMD_RCGR_UPDATE_SHFT                                   0x0

#define HWIO_MSS_UIM0_MND_CFG_RCGR_ADDR                                   (MSS_PERPH_REG_BASE      + 0x00001154)
#define HWIO_MSS_UIM0_MND_CFG_RCGR_RMSK                                       0x3000
#define HWIO_MSS_UIM0_MND_CFG_RCGR_IN          \
        in_dword_masked(HWIO_MSS_UIM0_MND_CFG_RCGR_ADDR, HWIO_MSS_UIM0_MND_CFG_RCGR_RMSK)
#define HWIO_MSS_UIM0_MND_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_MSS_UIM0_MND_CFG_RCGR_ADDR, m)
#define HWIO_MSS_UIM0_MND_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_MSS_UIM0_MND_CFG_RCGR_ADDR,v)
#define HWIO_MSS_UIM0_MND_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_UIM0_MND_CFG_RCGR_ADDR,m,v,HWIO_MSS_UIM0_MND_CFG_RCGR_IN)
#define HWIO_MSS_UIM0_MND_CFG_RCGR_MODE_BMSK                                  0x3000
#define HWIO_MSS_UIM0_MND_CFG_RCGR_MODE_SHFT                                     0xc

#define HWIO_MSS_UIM0_MND_M_ADDR                                          (MSS_PERPH_REG_BASE      + 0x00001158)
#define HWIO_MSS_UIM0_MND_M_RMSK                                              0xffff
#define HWIO_MSS_UIM0_MND_M_IN          \
        in_dword_masked(HWIO_MSS_UIM0_MND_M_ADDR, HWIO_MSS_UIM0_MND_M_RMSK)
#define HWIO_MSS_UIM0_MND_M_INM(m)      \
        in_dword_masked(HWIO_MSS_UIM0_MND_M_ADDR, m)
#define HWIO_MSS_UIM0_MND_M_OUT(v)      \
        out_dword(HWIO_MSS_UIM0_MND_M_ADDR,v)
#define HWIO_MSS_UIM0_MND_M_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_UIM0_MND_M_ADDR,m,v,HWIO_MSS_UIM0_MND_M_IN)
#define HWIO_MSS_UIM0_MND_M_M_VALUE_BMSK                                      0xffff
#define HWIO_MSS_UIM0_MND_M_M_VALUE_SHFT                                         0x0

#define HWIO_MSS_UIM0_MND_N_ADDR                                          (MSS_PERPH_REG_BASE      + 0x0000115c)
#define HWIO_MSS_UIM0_MND_N_RMSK                                              0xffff
#define HWIO_MSS_UIM0_MND_N_IN          \
        in_dword_masked(HWIO_MSS_UIM0_MND_N_ADDR, HWIO_MSS_UIM0_MND_N_RMSK)
#define HWIO_MSS_UIM0_MND_N_INM(m)      \
        in_dword_masked(HWIO_MSS_UIM0_MND_N_ADDR, m)
#define HWIO_MSS_UIM0_MND_N_OUT(v)      \
        out_dword(HWIO_MSS_UIM0_MND_N_ADDR,v)
#define HWIO_MSS_UIM0_MND_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_UIM0_MND_N_ADDR,m,v,HWIO_MSS_UIM0_MND_N_IN)
#define HWIO_MSS_UIM0_MND_N_N_VALUE_BMSK                                      0xffff
#define HWIO_MSS_UIM0_MND_N_N_VALUE_SHFT                                         0x0

#define HWIO_MSS_UIM0_MND_D_ADDR                                          (MSS_PERPH_REG_BASE      + 0x00001160)
#define HWIO_MSS_UIM0_MND_D_RMSK                                              0xffff
#define HWIO_MSS_UIM0_MND_D_IN          \
        in_dword_masked(HWIO_MSS_UIM0_MND_D_ADDR, HWIO_MSS_UIM0_MND_D_RMSK)
#define HWIO_MSS_UIM0_MND_D_INM(m)      \
        in_dword_masked(HWIO_MSS_UIM0_MND_D_ADDR, m)
#define HWIO_MSS_UIM0_MND_D_OUT(v)      \
        out_dword(HWIO_MSS_UIM0_MND_D_ADDR,v)
#define HWIO_MSS_UIM0_MND_D_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_UIM0_MND_D_ADDR,m,v,HWIO_MSS_UIM0_MND_D_IN)
#define HWIO_MSS_UIM0_MND_D_D_VALUE_BMSK                                      0xffff
#define HWIO_MSS_UIM0_MND_D_D_VALUE_SHFT                                         0x0

#define HWIO_MSS_UIM1_MND_CMD_RCGR_ADDR                                   (MSS_PERPH_REG_BASE      + 0x00001164)
#define HWIO_MSS_UIM1_MND_CMD_RCGR_RMSK                                   0x80000003
#define HWIO_MSS_UIM1_MND_CMD_RCGR_IN          \
        in_dword_masked(HWIO_MSS_UIM1_MND_CMD_RCGR_ADDR, HWIO_MSS_UIM1_MND_CMD_RCGR_RMSK)
#define HWIO_MSS_UIM1_MND_CMD_RCGR_INM(m)      \
        in_dword_masked(HWIO_MSS_UIM1_MND_CMD_RCGR_ADDR, m)
#define HWIO_MSS_UIM1_MND_CMD_RCGR_OUT(v)      \
        out_dword(HWIO_MSS_UIM1_MND_CMD_RCGR_ADDR,v)
#define HWIO_MSS_UIM1_MND_CMD_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_UIM1_MND_CMD_RCGR_ADDR,m,v,HWIO_MSS_UIM1_MND_CMD_RCGR_IN)
#define HWIO_MSS_UIM1_MND_CMD_RCGR_ROOT_OFF_BMSK                          0x80000000
#define HWIO_MSS_UIM1_MND_CMD_RCGR_ROOT_OFF_SHFT                                0x1f
#define HWIO_MSS_UIM1_MND_CMD_RCGR_ROOT_EN_BMSK                                  0x2
#define HWIO_MSS_UIM1_MND_CMD_RCGR_ROOT_EN_SHFT                                  0x1
#define HWIO_MSS_UIM1_MND_CMD_RCGR_UPDATE_BMSK                                   0x1
#define HWIO_MSS_UIM1_MND_CMD_RCGR_UPDATE_SHFT                                   0x0

#define HWIO_MSS_UIM1_MND_CFG_RCGR_ADDR                                   (MSS_PERPH_REG_BASE      + 0x00001168)
#define HWIO_MSS_UIM1_MND_CFG_RCGR_RMSK                                       0x3000
#define HWIO_MSS_UIM1_MND_CFG_RCGR_IN          \
        in_dword_masked(HWIO_MSS_UIM1_MND_CFG_RCGR_ADDR, HWIO_MSS_UIM1_MND_CFG_RCGR_RMSK)
#define HWIO_MSS_UIM1_MND_CFG_RCGR_INM(m)      \
        in_dword_masked(HWIO_MSS_UIM1_MND_CFG_RCGR_ADDR, m)
#define HWIO_MSS_UIM1_MND_CFG_RCGR_OUT(v)      \
        out_dword(HWIO_MSS_UIM1_MND_CFG_RCGR_ADDR,v)
#define HWIO_MSS_UIM1_MND_CFG_RCGR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_UIM1_MND_CFG_RCGR_ADDR,m,v,HWIO_MSS_UIM1_MND_CFG_RCGR_IN)
#define HWIO_MSS_UIM1_MND_CFG_RCGR_MODE_BMSK                                  0x3000
#define HWIO_MSS_UIM1_MND_CFG_RCGR_MODE_SHFT                                     0xc

#define HWIO_MSS_UIM1_MND_M_ADDR                                          (MSS_PERPH_REG_BASE      + 0x0000116c)
#define HWIO_MSS_UIM1_MND_M_RMSK                                              0xffff
#define HWIO_MSS_UIM1_MND_M_IN          \
        in_dword_masked(HWIO_MSS_UIM1_MND_M_ADDR, HWIO_MSS_UIM1_MND_M_RMSK)
#define HWIO_MSS_UIM1_MND_M_INM(m)      \
        in_dword_masked(HWIO_MSS_UIM1_MND_M_ADDR, m)
#define HWIO_MSS_UIM1_MND_M_OUT(v)      \
        out_dword(HWIO_MSS_UIM1_MND_M_ADDR,v)
#define HWIO_MSS_UIM1_MND_M_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_UIM1_MND_M_ADDR,m,v,HWIO_MSS_UIM1_MND_M_IN)
#define HWIO_MSS_UIM1_MND_M_M_VALUE_BMSK                                      0xffff
#define HWIO_MSS_UIM1_MND_M_M_VALUE_SHFT                                         0x0

#define HWIO_MSS_UIM1_MND_N_ADDR                                          (MSS_PERPH_REG_BASE      + 0x00001170)
#define HWIO_MSS_UIM1_MND_N_RMSK                                              0xffff
#define HWIO_MSS_UIM1_MND_N_IN          \
        in_dword_masked(HWIO_MSS_UIM1_MND_N_ADDR, HWIO_MSS_UIM1_MND_N_RMSK)
#define HWIO_MSS_UIM1_MND_N_INM(m)      \
        in_dword_masked(HWIO_MSS_UIM1_MND_N_ADDR, m)
#define HWIO_MSS_UIM1_MND_N_OUT(v)      \
        out_dword(HWIO_MSS_UIM1_MND_N_ADDR,v)
#define HWIO_MSS_UIM1_MND_N_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_UIM1_MND_N_ADDR,m,v,HWIO_MSS_UIM1_MND_N_IN)
#define HWIO_MSS_UIM1_MND_N_N_VALUE_BMSK                                      0xffff
#define HWIO_MSS_UIM1_MND_N_N_VALUE_SHFT                                         0x0

#define HWIO_MSS_UIM1_MND_D_ADDR                                          (MSS_PERPH_REG_BASE      + 0x00001174)
#define HWIO_MSS_UIM1_MND_D_RMSK                                              0xffff
#define HWIO_MSS_UIM1_MND_D_IN          \
        in_dword_masked(HWIO_MSS_UIM1_MND_D_ADDR, HWIO_MSS_UIM1_MND_D_RMSK)
#define HWIO_MSS_UIM1_MND_D_INM(m)      \
        in_dword_masked(HWIO_MSS_UIM1_MND_D_ADDR, m)
#define HWIO_MSS_UIM1_MND_D_OUT(v)      \
        out_dword(HWIO_MSS_UIM1_MND_D_ADDR,v)
#define HWIO_MSS_UIM1_MND_D_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_UIM1_MND_D_ADDR,m,v,HWIO_MSS_UIM1_MND_D_IN)
#define HWIO_MSS_UIM1_MND_D_D_VALUE_BMSK                                      0xffff
#define HWIO_MSS_UIM1_MND_D_D_VALUE_SHFT                                         0x0

#define HWIO_MSS_TCSR_ACC_SEL_ADDR                                        (MSS_PERPH_REG_BASE      + 0x0000f000)
#define HWIO_MSS_TCSR_ACC_SEL_RMSK                                               0x3
#define HWIO_MSS_TCSR_ACC_SEL_IN          \
        in_dword_masked(HWIO_MSS_TCSR_ACC_SEL_ADDR, HWIO_MSS_TCSR_ACC_SEL_RMSK)
#define HWIO_MSS_TCSR_ACC_SEL_INM(m)      \
        in_dword_masked(HWIO_MSS_TCSR_ACC_SEL_ADDR, m)
#define HWIO_MSS_TCSR_ACC_SEL_OUT(v)      \
        out_dword(HWIO_MSS_TCSR_ACC_SEL_ADDR,v)
#define HWIO_MSS_TCSR_ACC_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MSS_TCSR_ACC_SEL_ADDR,m,v,HWIO_MSS_TCSR_ACC_SEL_IN)
#define HWIO_MSS_TCSR_ACC_SEL_ACC_MEM_SEL_BMSK                                   0x3
#define HWIO_MSS_TCSR_ACC_SEL_ACC_MEM_SEL_SHFT                                   0x0

#endif /* __PRNG_CLKHWIOREG_H__ */


