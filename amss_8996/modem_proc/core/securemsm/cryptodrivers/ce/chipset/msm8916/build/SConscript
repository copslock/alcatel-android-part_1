#===============================================================================
#
# cryptoLibs
#
# GENERAL DESCRIPTION
#    build script
#
# Copyright (c) 2009-2009 by QUALCOMM, Incorporated.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#
#-------------------------------------------------------------------------------
#
#  $Header: //components/rel/core.mpss/3.4.c3.11/securemsm/cryptodrivers/ce/chipset/msm8916/build/SConscript#1 $
#  $DateTime: 2016/03/28 23:02:17 $
#  $Author: mplcsds1 $
#  $Change: 10156097 $
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
# 11/10/10   ejt     Added boot images from CE1
#
#===============================================================================
Import('env')
env = env.Clone()

#-------------------------------------------------------------------------------
# Publish Private APIs
#-------------------------------------------------------------------------------
env.PublishPrivateApi('CECL', [
   '${INC_ROOT}/core/securemsm/cryptodrivers/ce/chipset/${CHIPSET}/inc',
   '${INC_ROOT}/core/securemsm/cryptodrivers/ce/environment/inc', 
   '${INC_ROOT}/core/securemsm/cryptodrivers/ce/environment/src',
])

#-------------------------------------------------------------------------------
# Source PATH
#-------------------------------------------------------------------------------
SRCPATH = "${BUILD_ROOT}/core/securemsm/cryptodrivers/ce/chipset/${CHIPSET}/src"

if env.has_key('USES_NO_DEBUG'):
   env.Replace(DBUG_MODE = "NoDebug")
else:
   env.Replace(DBUG_MODE = "Debug")

if env.has_key('IMAGE_NAME'):
   SECUREMSMLIBPATH = '${IMAGE_NAME}/${PROC}/${ARMTOOLS}'
#   SECUREMSMLIBPATH = '${IMAGE_NAME}/${PROC}/${ARMTOOLS}/${DBUG_MODE}'
else:
   SECUREMSMLIBPATH = 'unknown/${PROC}/${ARMTOOLS}'
#   SECUREMSMLIBPATH = 'unknown/${PROC}/${ARMTOOLS}/${DBUG_MODE}'

env.Replace(SECUREMSMLIBPATH = SECUREMSMLIBPATH)
env.VariantDir('${SECUREMSMLIBPATH}', SRCPATH, duplicate=0)

#-------------------------------------------------------------------------------
# External depends outside CoreBSP
#-------------------------------------------------------------------------------
env.RequireExternalApi([
   'BREW',
   'CS',
   'DSM',
])

#-------------------------------------------------------------------------------
# Internal depends within CoreBSP
#-------------------------------------------------------------------------------
CBSP_API = [
   'DAL',
   'DEBUGTOOLS',
   'HAL',
   'MPROC',
   'SECUREMSM',
   'SERVICES',
   'STORAGE',
   'SYSTEMDRIVERS',
   'WIREDCONNECTIVITY',
   'BOOT',
   'HWENGINES',
   'POWER',
   # needs to be last also contains wrong comdef.h
   'KERNEL',
]

env.RequirePublicApi(CBSP_API)
env.RequireRestrictedApi(CBSP_API)

#-------------------------------------------------------------------------------
# Compiler, object, and linker definitions
#-------------------------------------------------------------------------------
env.Append(ARMCC_CODE = " ${ARM_SPLIT_SECTIONS}")
env.Replace(ARMLD_NOREMOVE_CMD = "")

env.Append(CPPDEFINES = ['_ARM_', '__arm', 'ARM', 'ARM_BREW_BUILD'])
env.Append(ARMCC_OPT = ' -EC --loose_implicit_cast ')
env.Append(ARMCPP_OPT = ' -EC --loose_implicit_cast ')
if env.has_key('BUILD_BOOT_CHAIN'):
   env.Append(CPPDEFINES = ['FEATURE_LIBRARY_ONLY'])

#-------------------------------------------------------------------------------
# Sources, libraries
#-------------------------------------------------------------------------------

CECL_LIB_SOURCES = [
        '${SECUREMSMLIBPATH}/CeCL.c',
        '${SECUREMSMLIBPATH}/CeCL_Mss.c',
]

CLEAN_SOURCES = env.FindFiles("*", "${BUILD_ROOT}/core/securemsm/cryptodrivers/ce/chipset/mdm9x25/")

env.CleanPack(['SINGLE_IMAGE', 'CBSP_SINGLE_IMAGE',
      'MODEM_IMAGE',  'CBSP_MODEM_IMAGE',
      'APPS_IMAGE',   'CBSP_APPS_IMAGE',
      'QDSP6_SW_IMAGE', 'CBSP_QDSP6_SW_IMAGE',
      'MBA_CORE_SW'], CLEAN_SOURCES)

CLEAN_SOURCES = env.FindFiles("*", "${BUILD_ROOT}/core/securemsm/cryptodrivers/ce/chipset/msm8x10/")

env.CleanPack(['SINGLE_IMAGE', 'CBSP_SINGLE_IMAGE',
      'MODEM_IMAGE',  'CBSP_MODEM_IMAGE',
      'APPS_IMAGE',   'CBSP_APPS_IMAGE',
      'QDSP6_SW_IMAGE', 'CBSP_QDSP6_SW_IMAGE',
      'MBA_CORE_SW'], CLEAN_SOURCES)

CLEAN_SOURCES = env.FindFiles("*", "${BUILD_ROOT}/core/securemsm/cryptodrivers/ce/chipset/msm8x26/")

env.CleanPack(['SINGLE_IMAGE', 'CBSP_SINGLE_IMAGE',
      'MODEM_IMAGE',  'CBSP_MODEM_IMAGE',
      'APPS_IMAGE',   'CBSP_APPS_IMAGE',
      'QDSP6_SW_IMAGE', 'CBSP_QDSP6_SW_IMAGE',
      'MBA_CORE_SW'], CLEAN_SOURCES) 

CLEAN_SOURCES = env.FindFiles("*", "${BUILD_ROOT}/core/securemsm/cryptodrivers/ce/chipset/msm8974/")

env.CleanPack(['SINGLE_IMAGE', 'CBSP_SINGLE_IMAGE',
      'MODEM_IMAGE',  'CBSP_MODEM_IMAGE',
      'APPS_IMAGE',   'CBSP_APPS_IMAGE',
      'QDSP6_SW_IMAGE', 'CBSP_QDSP6_SW_IMAGE',
      'MBA_CORE_SW'], CLEAN_SOURCES)

CLEAN_SOURCES = env.FindFiles("*.h", "${BUILD_ROOT}/core/securemsm/cryptodrivers/ce/chipset/msm8926/")

env.CleanPack(['SINGLE_IMAGE', 'CBSP_SINGLE_IMAGE',
      'MODEM_IMAGE',  'CBSP_MODEM_IMAGE',
      'APPS_IMAGE',   'CBSP_APPS_IMAGE',
      'QDSP6_SW_IMAGE', 'CBSP_QDSP6_SW_IMAGE',
      'MBA_CORE_SW'], CLEAN_SOURCES)


#-------------------------------------------------------------------------------
# Add Libraries to image
#-------------------------------------------------------------------------------
env.AddBinaryLibrary(
     ['SINGLE_IMAGE', 'CBSP_SINGLE_IMAGE',
      'MODEM_IMAGE',  'CBSP_MODEM_IMAGE',
      'APPS_IMAGE',   'CBSP_APPS_IMAGE',
      'QDSP6_SW_IMAGE', 'CBSP_QDSP6_SW_IMAGE',
      'MBA_CORE_SW'
     ],
     '${SECUREMSMLIBPATH}/CeCL',
     CECL_LIB_SOURCES)


