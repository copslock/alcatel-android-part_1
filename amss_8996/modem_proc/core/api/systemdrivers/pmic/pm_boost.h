#ifndef PM_BOOST__H
#define PM_BOOST__H

/** @file pm_boost.h
*  
* This header file contains enums and API definitions for PMIC 
*   Boost power rail driver.
*/
/*
*   Copyright (c) 2012-2015 Qualcomm Technologies, Inc.
*   All Rights Reserved.
*   Confidential and Proprietary - Qualcomm Technologies, Inc.
 */
/* ======================================================================= */

/* =======================================================================
                             Edit History
  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/core.mpss/3.4.c3.11/api/systemdrivers/pmic/pm_boost.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
12/06/12   hw      Rearchitecturing module driver to peripheral driver
=============================================================================*/
/*===========================================================================

                        HEADER FILES

===========================================================================*/

#include "pm_err_flags.h"
#include "pm_resources_and_types.h"
#include "com_dtypes.h"

/*===========================================================================

                        TYPE DEFINITIONS 

===========================================================================*/
/** @addtogroup pm_boost
@{ */

/** Boost peripheral index */
enum
{
  PM_BOOST_1,  /**< Boost peripheral index 1. */
  PM_BOOST_INVALID
};

/*===========================================================================

                        API PROTOTYPE

============================================================================*/
/**
 * @name pm_boost_volt_level_status 
 * 
 * @brief Gets the voltage of a regulator. 
 *
 * @note1hang Different voltage regulator types (LDO, HFS, etc.) may have
 *   different programmable voltage
 *   steps. Only support the correct programmable steps. Do not
 *   round voltages if the voltage selected is not part of
 *     the programmable steps.
 * 
 * @param[in] pmic_chip Primary -- 0; Secondary -- 1.
 * @param[in] boost_peripheral_index Boost peripheral index.
 * @param[out] volt_level Returned voltage level.
 * 
 * @return 
 *   SUCCESS or Error -- See #pm_err_flag_type.
 */
pm_err_flag_type pm_boost_volt_level_status
(uint8 pmic_chip, uint8 boost_peripheral_index, pm_volt_level_type* volt_level);

/**
 * @name pm_boost_sw_enable_status 
 *  
 * @brief Returns the sw enabled/disabled status of the rail.
 * 
 * @param[in] pmic_chip Primary -- 0; Secondary -- 1.
 * @param[in] boost_peripheral_index Boost peripheral index.
 * @param[out] on_off sw enable status.
 * 
 * @return 
 *   SUCCESS or Error -- See #pm_err_flag_type.
 */
pm_err_flag_type pm_boost_sw_enable_status
(uint8 pmic_chip, uint8 boost_peripheral_index, pm_on_off_type* on_off);

/**
 * @name pm_boost_vreg_ok_status 
 *  
 * @brief Returns the VREG_OK/VREG_READY status of the rail.
 * 
 * @param[in] pmic_chip Primary -- 0; Secondary -- 1.
 * @param[in] boost_peripheral_index BOOST peripheral index. 
 *       Starts from 0 (for the first BOOST peripheral).
 * @param[out] on_off On/Off status.
 * 
 * @return 
 *   SUCCESS or Error -- See #pm_err_flag_type.
 */
pm_err_flag_type pm_boost_vreg_ok_status
(uint8 pmic_chip, uint8 boost_peripheral_index, boolean* on_off);

/** @} */ /* end_addtogroup pm_boost */

#endif /* PM_BOOST__H */

