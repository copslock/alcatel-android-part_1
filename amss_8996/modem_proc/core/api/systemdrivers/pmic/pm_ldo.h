#ifndef PM_LDO__H
#define PM_LDO__H

/** @file pm_ldo.h 
 *  
 *  This header file contains enums and API definitions for LDO
 *  power rail driver.
*/
/*
 *  Copyright (c) 2012-2015 Qualcomm Technologies, Inc.
 *  All Rights Reserved.
 *  Confidential and Proprietary - Qualcomm Technologies, Inc.
 */
/* =======================================================================
                             Edit History
  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/core.mpss/3.4.c3.11/api/systemdrivers/pmic/pm_ldo.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
12/06/12   hw      Rearchitecturing module driver to peripheral driver
=============================================================================*/

/*===========================================================================

                 HEADER FILE INCLUDE

===========================================================================*/
#include "pm_err_flags.h"
#include "pm_resources_and_types.h"
#include "com_dtypes.h"

/*===========================================================================

                        TYPE DEFINITIONS 

===========================================================================*/

/** @addtogroup pm_ldo
@{ */

/**
 * LDO peripheral index. This enum type contains all LDO regulators that are
 * required. 
 */
enum
{
  PM_LDO_1,       /**< LDO 1. */
  PM_LDO_2,       /**< LDO 2. */
  PM_LDO_3,       /**< LDO 3. */
  PM_LDO_4,       /**< LDO 4. */
  PM_LDO_5,       /**< LDO 5. */
  PM_LDO_6,       /**< LDO 6. */
  PM_LDO_7,       /**< LDO 7. */
  PM_LDO_8,       /**< LDO 8. */
  PM_LDO_9,       /**< LDO 9. */
  PM_LDO_10,      /**< LDO 10. */
  PM_LDO_11,      /**< LDO 11. */
  PM_LDO_12,      /**< LDO 12. */
  PM_LDO_13,      /**< LDO 13. */
  PM_LDO_14,      /**< LDO 14. */
  PM_LDO_15,      /**< LDO 15. */
  PM_LDO_16,      /**< LDO 16. */
  PM_LDO_17,      /**< LDO 17. */
  PM_LDO_18,      /**< LDO 18. */
  PM_LDO_19,      /**< LDO 19. */
  PM_LDO_20,      /**< LDO 20. */
  PM_LDO_21,      /**< LDO 21. */
  PM_LDO_22,      /**< LDO 22. */
  PM_LDO_23,      /**< LDO 23. */
  PM_LDO_24,      /**< LDO 24. */
  PM_LDO_25,      /**< LDO 25. */
  PM_LDO_26,      /**< LDO 26. */
  PM_LDO_27,      /**< LDO 27. */
  PM_LDO_28,      /**< LDO 28. */
  PM_LDO_29,      /**< LDO 29. */
  PM_LDO_30,      /**< LDO 30. */
  PM_LDO_31,      /**< LDO 31. */
  PM_LDO_32,      /**< LDO 32. */
  PM_LDO_33,      /**< LDO 33. */
  PM_LDO_INVALID  /**< Invalid LDO. */
};

/*===========================================================================

                        API PROTOTYPE

===========================================================================*/

/** 
 *  Returns the mode status (LPM, NPM, AUTO, BYPASS)
 *  of the selected power rail.
 *  
 * @note1hang The mode of a regulator changes dynamically.
 * 
 * @param[in]  pmic_chip Primary -- 0; Secondary -- 1.
 * @param[in]  ldo_peripheral_index LDO peripheral index.
 *                Starts from 0 (for the first LDO peripheral).
 * @param[out] sw_mode Variable returned to the caller with the mode status.
 *                Refer to pm_resources_and_types.h for the enum info.
 *
 * @return 
 *  SUCCESS or Error -- See #pm_err_flag_type.
 */
pm_err_flag_type pm_ldo_sw_mode_status
(uint8 pmic_chip, uint8 ldo_peripheral_index, pm_sw_mode_type* sw_mode);

/** 
 *  Returns the pin controlled status or the hardware 
 *        enable status (On/Off) of the selected power rail.
 * 
 * @param[in]  pmic_chip Primary -- 0; Secondary -- 1.
 * @param[in]  ldo_peripheral_index LDO peripheral index.
 *                Starts from 0 (for the first LDO peripheral).
 * @param[out] on_off Variable returned to the caller with pin control
 *                status. Refer to pm_resources_and_types.h for
 *                the enum info.
 *
 * @return 
 *  SUCCESS or Error -- See #pm_err_flag_type.
 */
pm_err_flag_type pm_ldo_pin_ctrled_status
(uint8 pmic_chip, uint8 ldo_peripheral_index, pm_on_off_type* on_off);

/** 
 * @name pm_ldo_pin_ctrl_status 
 *  
 * @brief This function returns the pin control enable 
 *  status and also the selected pins being followed by the
 *  selected power rail enable.
 * 
 * @param[in]  pmic_chip. Primary: 0. Secondary: 1
 * @param[in]  perph_index:
 *                Starts from 0 (for first LDO peripheral)
 * @param[out] select_pin Variable returned to the caller with 
 *                        selected pins being followed by the
 *                        power rail.
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS else ERROR.
 *
 */
pm_err_flag_type pm_ldo_pin_ctrl_status
(uint8 pmic_chip, uint8 perph_index, uint8 *select_pin);

/** 
 *  Returns the voltage level (in microvolts) of the selected power rail.
 *  
 * @param[in] pmic_chip Primary -- 0; Secondary -- 1.
 * @param[in] ldo_peripheral_index LDO peripheral index.
 *                Starts from 0 (for the first LDO peripheral).
 * @param[out] volt_level Variable returned to the caller with the volt
 *                level status in micro volts (uint32).
 *
 * @return 
 *  SUCCESS or Error -- See #pm_err_flag_type.
 */
pm_err_flag_type pm_ldo_volt_level_status
(uint8 pmic_chip, uint8 ldo_peripheral_index, pm_volt_level_type* volt_level);

/** 
 *  Returns the software enable status (On/Off) 
 *        of the selected power rail.
 * 
 * @param[in] pmic_chip Primary -- 0; Secondary -- 1.
 * @param[in] ldo_peripheral_index LDO peripheral index.
 *                Starts from 0 (for the first LDO peripheral).
 * @param[out] on_off Variable returned to the caller with the software
 *                status. Refer to pm_resources_and_types.h for
 *                the enum info.
 *
 * @return 
 *  SUCCESS or Error -- See #pm_err_flag_type.
 */
pm_err_flag_type pm_ldo_sw_enable_status
(uint8 pmic_chip, uint8 ldo_peripheral_index, pm_on_off_type* on_off);

/**
 * @name pm_ldo_vreg_ok_status 
 *  
 * @brief Returns the VREG_OK/VREG_READY status of the rail.
 * 
 * @param[in] pmic_chip Primary -- 0; Secondary -- 1.
 * @param[in] ldo_peripheral_index LDO peripheral index. Starts 
 *       from 0 (for the first LDO peripheral).
 * @param[out] on_off On/Off status.
 * 
 * @return 
 *   SUCCESS or Error -- See #pm_err_flag_type.
 */
pm_err_flag_type pm_ldo_vreg_ok_status
(uint8 pmic_chip, uint8 ldo_peripheral_index, boolean* on_off);

/** 
 *  Returns the local soft reset status of 
 *        the selected power rail.
 * 
 * @param[in] pmic_chip Primary -- 0; Secondary -- 1
 * @param[in] ldo_peripheral_index LDO peripheral index.
 *                Starts from 0 (for the first LDO peripheral).
 * @param[out] status Variable returned to the caller with the soft reset
 *                status. Returns TRUE if the LDO is in soft reset
 *                and FALSE otherwise.
 *
 * @return 
 *  SUCCESS or Error -- See #pm_err_flag_type.
 */
pm_err_flag_type pm_ldo_soft_reset_status
(uint8 pmic_chip, uint8 ldo_peripheral_index, boolean* status);

/** 
 *  Brings the selected power rail out of 
 *        local soft reset.
 * 
 * @param[in] pmic_chip Primary -- 0; Secondary -- 1.
 * @param[in] ldo_peripheral_index LDO peripheral index.
 *                Starts from 0 (for the first LDO peripheral).
 *
 * @return 
 *  SUCCESS or Error -- See #pm_err_flag_type.
 */
pm_err_flag_type pm_ldo_soft_reset_exit
(uint8 pmic_chip, uint8 ldo_peripheral_index);

/** 
 * Calculates the PMIC register value for
 *        a supplied voltage
 * 
 * @param[in]  pmic_chip. Primary: 0. Secondary: 1
 * @param[in]  ldo_peripheral_index:
 *                Starts from 0 (for first LDO peripheral)
 * @param[in]  volt_level:
 *                Voltage to convert to PMIC register value
 * @param[out] vset:
 *                PMIC register value for supplied voltage
 *
 * @return 
 *  SUCCESS or Error -- See #pm_err_flag_type.
 *
 */
pm_err_flag_type pm_ldo_volt_calculate_vset
(uint8 pmic_chip, uint8 ldo_peripheral_index, pm_volt_level_type  volt_level, uint32* vset);

/** 
 * Returns the stepper done status for
 *        the selected power rail
 * 
 * @param[in]  pmic_chip. Primary: 0. Secondary: 1
 * @param[in]  ldo_peripheral_index:
 *                Starts from 0 (for first LDO peripheral)
 * @param[out]  stepper_done:
 *                Value of stepper done register
 *
 * @return 
 *  SUCCESS or Error -- See #pm_err_flag_type.
 *
 */
pm_err_flag_type pm_ldo_volt_level_stepper_done_status
(uint8 pmic_chip, uint8 ldo_peripheral_index, boolean *stepper_done);

/** @} */ /* end_addtogroup pm_ldo */

#endif /* PM_LDO__H */
