#ifndef PM_VS__H
#define PM_VS__H

/** @file pm_vs.h 
 *
 *  This header file contains enums and API definitions for the
 *  voltage switch power rail driver.
 */
/*
 *  Copyright (c) 2010-2015 Qualcomm Technologies, Inc.
 *  All Rights Reserved.
 *  Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

/* =======================================================================
                             Edit History
  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/core.mpss/3.4.c3.11/api/systemdrivers/pmic/pm_vs.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
01/10/13   kt      Changing the file as per Henry's Phase1 changes.  
02/08/11   hw      Merging changes from the PMIC Distributed Driver Arch branch
06/04/10   hw      Creation of Voltage Switch (VS) Module
========================================================================== */
/*===========================================================================

                        HEADER FILES

===========================================================================*/

#include "pm_err_flags.h"
#include "pm_resources_and_types.h"
#include "com_dtypes.h"

/*===========================================================================

                        API PROTOTYPE

===========================================================================*/
/** @addtogroup pm_vs
@{ */
/**
 *  Voltage switch peripheral index. This enum type contains all required
 *  VS regulators. 
 */
enum
{
  PM_VS_LVS_1,
  PM_VS_LVS_2,
  PM_VS_LVS_3,
  PM_VS_LVS_4,
  PM_VS_LVS_5,
  PM_VS_LVS_6,
  PM_VS_LVS_7,
  PM_VS_LVS_8,
  PM_VS_LVS_9,
  PM_VS_LVS_10,
  PM_VS_LVS_11,
  PM_VS_LVS_12,
  PM_VS_MVS_1,
  PM_VS_OTG_1,
  PM_VS_HDMI_1,
  PM_VS_INVALID
};

/*===========================================================================

                        API PROTOTYPE

===========================================================================*/

/* pm_vs_sw_mode_status */
/**
 *  Returns the mode status (LPM, NPM, AUTO, BYPASS)
 *  of the selected power rail.
 * @note1hang The mode of a regulator changes dynamically.
 *
 * @param[in] pmic_chip Primary PMIC -- 0; Secondary PMIC -- 1.
 * @param[in] perph_index VS peripheral index.
 *                           Starts from 0 (for the first VS peripheral).
 * @param[out] sw_mode Variable returned to the caller with the mode status.
 * 
 * 
 * @return 
 *  SUCCESS or Error -- See #pm_err_flag_type.
 */
pm_err_flag_type pm_vs_sw_mode_status
(uint8 pmic_chip, uint8 perph_index, pm_sw_mode_type* sw_mode);


/* pm_vs_pin_ctrled_status */

/**
 *  Returns the pin controlled status or hardware 
 *        enable status (On/Off) of the selected power rail.
 * 
 * @param[in] pmic_chip Primary PMIC -- 0; Secondary PMIC -- 1.
 * @param[in] perph_index VS peripheral index.
 *                           Starts from 0 (for the first VS peripheral).
 * @param[out] on_off Variable returned to the caller with pin control
 *                status. Refer to pm_resources_and_types.h for
 *                the enum info.
 * 
 * @return 
 *  SUCCESS or Error -- See #pm_err_flag_type.
 */
pm_err_flag_type pm_vs_pin_ctrled_status
(uint8 pmic_chip, uint8 perph_index, pm_on_off_type* on_off);

/** 
 * @name pm_vs_pin_ctrl_status 
 *  
 * @brief This function returns the pin control enable 
 *  status and also the selected pins being followed by the
 *  selected power rail enable.
 * 
 * @param[in]  pmic_chip. Primary: 0. Secondary: 1
 * @param[in]  perph_index:
 *                Starts from 0 (for first VS peripheral)
 * @param[out] select_pin Variable returned to the caller with 
 *                        selected pins being followed by the
 *                        power rail.
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS else ERROR.
 *
 */
pm_err_flag_type pm_vs_pin_ctrl_status
(uint8 pmic_chip, uint8 perph_index, uint8 *select_pin);

/* pm_vs_sw_enable_status */

/**
 *  Returns the software enable status (On/Off) 
 *        of the selected power rail.
 * 
 * @param[in] pmic_chip Primary PMIC -- 0; Secondary PMIC -- 1.
 * @param[in] perph_index VS peripheral index.
 *                           Starts from 0 (for the first VS peripheral).
 * @param[out] on_off Variable returned to the caller with the software
 *                status. Refer to pm_resources_and_types.h for
 *                the enum info.
 *
 * @return 
 *  SUCCESS or Error -- See #pm_err_flag_type.
 */
pm_err_flag_type pm_vs_sw_enable_status
(uint8 pmic_chip, uint8 perph_index, pm_on_off_type* on_off);

/**
 * @name pm_vs_vreg_ok_status 
 *  
 * @brief Returns the VREG_OK/VREG_READY status of the rail.
 * 
 * @param[in] pmic_chip Primary -- 0; Secondary -- 1.
 * @param[in] vs_peripheral_index VS peripheral index. Starts 
 *       from 0 (for the first VS peripheral).
 * @param[out] on_off On/Off status.
 * 
 * @return 
 *   SUCCESS or Error -- See #pm_err_flag_type.
 */
pm_err_flag_type pm_vs_vreg_ok_status
(uint8 pmic_chip, uint8 vs_peripheral_index, boolean* on_off);

/** @} */ /* end_addtogroup pm_vs */

#endif /* PM_VS__H */
