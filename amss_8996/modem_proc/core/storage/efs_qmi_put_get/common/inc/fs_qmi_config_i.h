/***********************************************************************
 * fs_qmi_config_i.h
 *
 * Configuration items for fs_qmi module.
 * Copyright (C) 2015 QUALCOMM Technologies, Inc.
 *
 ***********************************************************************/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/core.mpss/3.4.c3.11/storage/efs_qmi_put_get/common/inc/fs_qmi_config_i.h#1 $ $DateTime: 2016/03/28 23:02:17 $ $Author: mplcsds1 $

when         who   what, where, why
----------   ---   ---------------------------------------------------------
2015-03-24   dks   Create

===========================================================================*/

#ifndef __FS_QMI_CONFIG_I_H__
#define __FS_QMI_CONFIG_I_H__

#include "customer.h"

#endif /* not __FS_QMI_CONFIG_I_H__ */
