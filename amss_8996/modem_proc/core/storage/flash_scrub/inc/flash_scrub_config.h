/***********************************************************************
 * flash_scrub_config.h
 *
 * Flash-Scrub configuration file.
 * Copyright (C) 2013 QUALCOMM Technologies, Inc.
 *
 * Flash-scrub configuration file.
 *
 ***********************************************************************/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/core.mpss/3.4.c3.11/storage/flash_scrub/inc/flash_scrub_config.h#1 $ $DateTime: 2016/03/28 23:02:17 $ $Author: mplcsds1 $

when         who   what, where, why
----------   ---   ---------------------------------------------------------
2013-08-01   rp    Create

===========================================================================*/

#ifndef __FLASH_SCRUB_CONFIG_H__
#define __FLASH_SCRUB_CONFIG_H__

#include "customer.h"

#endif /* not __FLASH_SCRUB_CONFIG_H__ */
