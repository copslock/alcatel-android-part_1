/***********************************************************************
 * fds_config.h
 *
 * Configuration items for FDS
 * Copyright (C) 2013 QUALCOMM Technologies, Inc.
 *
 * Configuration items for Flash Driver Service.
 *
 ***********************************************************************/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/core.mpss/3.4.c3.11/storage/fds/inc/fds_config.h#1 $ $DateTime: 2016/03/28 23:02:17 $ $Author: mplcsds1 $

when         who   what, where, why
----------   ---   ---------------------------------------------------------
2013-10-04   dks   Create

===========================================================================*/

#ifndef __FDS_CONFIG_H__
#define __FDS_CONFIG_H__

#include "customer.h"

#endif /* not __FDS_CONFIG_H__ */
