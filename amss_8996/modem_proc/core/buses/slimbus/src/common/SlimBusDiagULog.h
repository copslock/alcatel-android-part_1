#ifndef DALSLIMBUSLOG_H
#define DALSLIMBUSLOG_H
/*
===========================================================================

FILE:         DalSlimBusULog.h

DESCRIPTION:  This file defines ULog-based logging data structures and macros
              used by the SLIMbus Device Access Library.

===========================================================================

                             Edit History

$Header: //components/rel/core.mpss/3.4.c3.11/buses/slimbus/src/common/SlimBusDiagULog.h#1 $

when       who     what, where, why
--------   ---     --------------------------------------------------------
04/01/15   MJS     Add support for QShrink 4.0.
07/31/13   MJS     Revert change for verbose msg level.
06/19/13   MJS     Make info and verbose same msg level.
05/12/13   MJS     Support for QShrink 2.0.
11/29/12   MJS     Add default ulog name.
05/16/12   MJS     Update logging macros.
09/12/11   MJS     Improved logging for error conditions, added DIAG logging.
04/13/11   MJS     Initial revision.

===========================================================================
             Copyright (c) 2011, 2012, 2013, 2015 QUALCOMM Technologies, Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
===========================================================================
*/

#include "ULogFront.h"
#include "msg.h"


#undef SB_LOG_DECLARE
#undef SB_LOG_INIT
#undef SB_LOG_ERR_0
#undef SB_LOG_ERR_1
#undef SB_LOG_ERR_2
#undef SB_LOG_ERR_3
#undef SB_LOG_ERR_4
#undef SB_LOG_ERR_5
#undef SB_LOG_ERR_6
#undef SB_LOG_ERR_7
#undef SB_LOG_WARN_0
#undef SB_LOG_WARN_1
#undef SB_LOG_WARN_2
#undef SB_LOG_WARN_3
#undef SB_LOG_WARN_4
#undef SB_LOG_WARN_5
#undef SB_LOG_WARN_6
#undef SB_LOG_WARN_7
#undef SB_LOG_INFO_0
#undef SB_LOG_INFO_1
#undef SB_LOG_INFO_2
#undef SB_LOG_INFO_3
#undef SB_LOG_INFO_4
#undef SB_LOG_INFO_5
#undef SB_LOG_INFO_6
#undef SB_LOG_INFO_7
#undef SB_LOG_VERB_0
#undef SB_LOG_VERB_1
#undef SB_LOG_VERB_2
#undef SB_LOG_VERB_3
#undef SB_LOG_VERB_4
#undef SB_LOG_VERB_5
#undef SB_LOG_VERB_6
#undef SB_LOG_VERB_7
#undef SB_LOG_VERB_8


#define SB_LOG_DECLARE() \
  uint32 uLogLevel; \
  ULogHandle hULog

#define SB_LOG_INIT(pDevCtxt) \
if (NULL == (pDevCtxt)->hULog) { \
  DALSYSPropertyVar PropVar; \
  uint32 uLogSize = 4096; \
  const char *pszInstName = "SLIMBUS"; \
  if ( NULL != (pDevCtxt)->pBSP ) pszInstName = (pDevCtxt)->pBSP->pszInstName; \
  SlimBus_LogLevelInit(pDevCtxt); \
  if ( DAL_SUCCESS == DALSYS_GetPropertyValue( pDevCtxt->Base.hProp, \
                                               "log_size", 0, &PropVar ) && \
      DALSYS_PROP_TYPE_UINT32 == PropVar.dwType ) \
    uLogSize = PropVar.Val.dwVal; \
  ULogFront_RealTimeInit(&(pDevCtxt)->hULog, pszInstName, \
                         uLogSize, ULOG_MEMORY_LOCAL, ULOG_LOCK_NONE); \
}

#define MSG_0(xx_ss_id, xx_ss_mask, xx_fmt) MSG(xx_ss_id, xx_ss_mask, xx_fmt)

#define MSG_BUILD_MASK_8500 MSG_BUILD_MASK_MSG_SSID_QDSP6

#define SB_LOG_DECLARE_PDEVCTXT(pCtxt) \
  SlimBusDevCtxt *pDevCtxt = (pCtxt)

#define SB_LOG_PRINTF_ERR(count, format, args...) \
  if ( 1 <= (pDevCtxt)->uLogLevel ) \
  { \
    MSG_##count(MSG_SSID_QDSP6, 0x00000008, "[ERR ] " format, ##args); \
  } \
  ULogFront_RealTimePrintf((pDevCtxt)->hULog, (count), ("[ERR ] " format), ##args)

#define SB_LOG_PRINTF_WARN(count, format, args...) \
  if ( 2 <= (pDevCtxt)->uLogLevel ) \
  { \
    MSG_##count(MSG_SSID_QDSP6, 0x00000004, "[WARN] " format, ##args); \
  } \
  ULogFront_RealTimePrintf((pDevCtxt)->hULog, (count), ("[WARN] " format), ##args)

#define SB_LOG_PRINTF_INFO(count, format, args...) \
  if ( 3 <= (pDevCtxt)->uLogLevel ) \
  { \
    MSG_##count(MSG_SSID_QDSP6, 0x00000002, "[INFO] " format, ##args); \
  } \
  ULogFront_RealTimePrintf((pDevCtxt)->hULog, (count), ("[INFO] " format), ##args)

#define SB_LOG_PRINTF_VERB(count, format, args...) \
  if ( 4 <= (pDevCtxt)->uLogLevel ) \
  { \
    MSG_##count(MSG_SSID_QDSP6, 0x00000001, "[INFO] " format, ##args); \
  } \
  ULogFront_RealTimePrintf((pDevCtxt)->hULog, (count), ("[INFO] " format), ##args)


#define SB_LOG_ERR_0(format)  SB_LOG_PRINTF_ERR(0, format)
#define SB_LOG_ERR_1(format, a1)  SB_LOG_PRINTF_ERR(1, format, a1)
#define SB_LOG_ERR_2(format, a1, a2)  SB_LOG_PRINTF_ERR(2, format, a1, a2)
#define SB_LOG_ERR_3(format, a1, a2, a3)  SB_LOG_PRINTF_ERR(3, format, a1, a2, a3)
#define SB_LOG_ERR_4(format, a1, a2, a3, a4)  SB_LOG_PRINTF_ERR(4, format, a1, a2, a3, a4)
#define SB_LOG_ERR_5(format, a1, a2, a3, a4, a5)  SB_LOG_PRINTF_ERR(5, format, a1, a2, a3, a4, a5)
#define SB_LOG_ERR_6(format, a1, a2, a3, a4, a5, a6)  SB_LOG_PRINTF_ERR(6, format, a1, a2, a3, a4, a5, a6)
#define SB_LOG_ERR_7(format, a1, a2, a3, a4, a5, a6, a7)  SB_LOG_PRINTF_ERR(7, format, a1, a2, a3, a4, a5, a6, a7)

#define SB_LOG_WARN_0(format)  SB_LOG_PRINTF_WARN(0, format)
#define SB_LOG_WARN_1(format, a1)  SB_LOG_PRINTF_WARN(1, format, a1)
#define SB_LOG_WARN_2(format, a1, a2)  SB_LOG_PRINTF_WARN(2, format, a1, a2)
#define SB_LOG_WARN_3(format, a1, a2, a3)  SB_LOG_PRINTF_WARN(3, format, a1, a2, a3)
#define SB_LOG_WARN_4(format, a1, a2, a3, a4)  SB_LOG_PRINTF_WARN(4, format, a1, a2, a3, a4)
#define SB_LOG_WARN_5(format, a1, a2, a3, a4, a5)  SB_LOG_PRINTF_WARN(5, format, a1, a2, a3, a4, a5)
#define SB_LOG_WARN_6(format, a1, a2, a3, a4, a5, a6)  SB_LOG_PRINTF_WARN(6, format, a1, a2, a3, a4, a5, a6)
#define SB_LOG_WARN_7(format, a1, a2, a3, a4, a5, a6, a7)  SB_LOG_PRINTF_WARN(7, format, a1, a2, a3, a4, a5, a6, a7)

#define SB_LOG_INFO_0(format)  SB_LOG_PRINTF_INFO(0, format)
#define SB_LOG_INFO_1(format, a1)  SB_LOG_PRINTF_INFO(1, format, a1)
#define SB_LOG_INFO_2(format, a1, a2)  SB_LOG_PRINTF_INFO(2, format, a1, a2)
#define SB_LOG_INFO_3(format, a1, a2, a3)  SB_LOG_PRINTF_INFO(3, format, a1, a2, a3)
#define SB_LOG_INFO_4(format, a1, a2, a3, a4)  SB_LOG_PRINTF_INFO(4, format, a1, a2, a3, a4)
#define SB_LOG_INFO_5(format, a1, a2, a3, a4, a5)  SB_LOG_PRINTF_INFO(5, format, a1, a2, a3, a4, a5)
#define SB_LOG_INFO_6(format, a1, a2, a3, a4, a5, a6)  SB_LOG_PRINTF_INFO(6, format, a1, a2, a3, a4, a5, a6)
#define SB_LOG_INFO_7(format, a1, a2, a3, a4, a5, a6, a7)  SB_LOG_PRINTF_INFO(7, format, a1, a2, a3, a4, a5, a6, a7)

#define SB_LOG_VERB_0(format)  SB_LOG_PRINTF_VERB(0, format)
#define SB_LOG_VERB_1(format, a1)  SB_LOG_PRINTF_VERB(1, format, a1)
#define SB_LOG_VERB_2(format, a1, a2)  SB_LOG_PRINTF_VERB(2, format, a1, a2)
#define SB_LOG_VERB_3(format, a1, a2, a3)  SB_LOG_PRINTF_VERB(3, format, a1, a2, a3)
#define SB_LOG_VERB_4(format, a1, a2, a3, a4)  SB_LOG_PRINTF_VERB(4, format, a1, a2, a3, a4)
#define SB_LOG_VERB_5(format, a1, a2, a3, a4, a5)  SB_LOG_PRINTF_VERB(5, format, a1, a2, a3, a4, a5)
#define SB_LOG_VERB_6(format, a1, a2, a3, a4, a5, a6)  SB_LOG_PRINTF_VERB(6, format, a1, a2, a3, a4, a5, a6)
#define SB_LOG_VERB_7(format, a1, a2, a3, a4, a5, a6, a7)  SB_LOG_PRINTF_VERB(7, format, a1, a2, a3, a4, a5, a6, a7)
#define SB_LOG_VERB_8(format, a1, a2, a3, a4, a5, a6, a7, a8)  SB_LOG_PRINTF_VERB(8, format, a1, a2, a3, a4, a5, a6, a7, a8)

#endif /* DALSLIMBUSLOG_H */

