#ifndef SERVREG_INTERNAL_H
#define SERVREG_INTERNAL_H
/*
#============================================================================
#  Name:
#    servreg_internal.h 
#
#  Description:
#     Common header file for Service Registry feature
#
# Copyright (c) 2015 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
#============================================================================
*/
#include "stdlib.h"
#include "servreg_common.h"
#include "DALSys.h"

#if !defined(SERVREG_EXCLUDE_KERNEL_QURT)
#include "rcevt_qurt.h"
#include "rcesn_qurt.h"
#include "servreg_monitor_qurt.h"
#endif

#if !defined(SERVREG_EXCLUDE_KERNEL_REX)
#include "rcevt_rex.h"
#include "rcesn_rex.h"
#include "servreg_monitor_rex.h"
#endif

#if defined(__cplusplus)
extern "C"
{
#endif

#define servreg_malloc(x)    (malloc(x))
#define servreg_free(x)      (free(x))

/* Task information */
#define SERVREG_QMI_NOTIF_REQ_SERVER_TASK_NAME            "sr_qmi_ind_server"
#define SERVREG_QMI_NOTIF_IND_SERVER_TASK_NAME            "sr_qmi_req_server"
#define SERVREG_QMI_NOTIF_SERVER_TASK_STACK               2048

/* Pool sizes */
#define SERVREG_LOC_ENTRY_POOL_SIZE                3
#define SERVREG_LOC_STRUCT_POOL_SIZE               3

//#define SERVREG_IND_MSG_ENTRY_POOL_SIZE            3

#define SERVREG_MON_NODE_POOL_SIZE                 3
#define SERVREG_MON_NODE_HASHTABLE_BUCKETS         63

#define SERVREG_QDI_NOTIF_NODE_POOL_SIZE           3

#define SERVREG_QMI_NOTIF_CLIENT_NODE_POOL_SIZE    3
#define SERVREG_QMI_NOTIF_SERVER_NODE_POOL_SIZE    3
#define SERVREG_QMI_NOTIF_SERVER_ENTRY_POOL_SIZE   3

/* Service Registry Signatures */
#define SERVREG_DL_SIGNATURE                       0xaaaabbbb
//#define SERVREG_IND_SIGNATURE                      0xbbbbcccc
#define SERVREG_MON_SIGNATURE                      0xccccdddd
#define SERVREG_QDI_NOTIF_SIGNATURE                0xeeeeffff
#define SERVREG_QMI_NOTIF_CLIENT_SIGNATURE         0xffffaaaa
#define SERVREG_QMI_NOTIF_SERVER_SIGNATURE         0xffffbbbb

typedef void* SERVREG_IND_HANDLE;
typedef void* SERVREG_QDI_NOTIF_HANDLE;
typedef void* SERVREG_QMI_NOTIF_CLIENT_HANDLE;
typedef void* SERVREG_QMI_NOTIF_SERVER_HANDLE;

struct servreg_mutex_s
{
   DALSYSSyncHandle mutex;
   DALSYSSyncObj mutexObject;

};
typedef struct servreg_mutex_s servreg_mutex_t, * servreg_mutex_p;

typedef unsigned long servreg_hash_t;

/** =====================================================================
 * MACRO:
 *     SERVREG_RCEVT_REGISTER_NAME_QURT & SERVREG_RCEVT_REGISTER_NAME_REX
 *
 * Description:
 *     Registers name with rcevt
 *
 * Parameters:
 *     sr_name        : Service Name to be registered
 *     sr_signal   : NHLOS specific notification signal information
 *
 * Returns:
 *     RCEVT_HANDLE
 * =====================================================================  */
#if !defined(SERVREG_EXCLUDE_KERNEL_QURT)
#define SERVREG_RCEVT_REGISTER_NAME_QURT(sr_name, sr_signal) (\
   {\
      RCEVT_HANDLE ret = RCEVT_NULL;\
      SERVREG_MON_SIGQURT *sig = (SERVREG_MON_SIGQURT*)sr_signal;\
\
      ret = rcevt_register_name_qurt(SERVREG_LOC_INIT_NAME, sig->signal, sig->mask);\
      ret;\
   }\
)
#else
#define SERVREG_RCEVT_REGISTER_NAME_QURT(sr_name, sr_signal) RCEVT_NULL
#endif


#if !defined(SERVREG_EXCLUDE_KERNEL_REX)
#define SERVREG_RCEVT_REGISTER_NAME_REX(sr_name, sr_signal) (\
   {\
      RCEVT_HANDLE ret = RCEVT_NULL;\
      SERVREG_MON_SIGREX *sig = (SERVREG_MON_SIGREX*)sr_signal;\
\
      ret = rcevt_register_name_rex(SERVREG_LOC_INIT_NAME, sig->mask);\
      ret;\
   }\
)
#else
#define SERVREG_RCEVT_REGISTER_NAME_REX(sr_name, sr_signal) RCEVT_NULL
#endif

/** =====================================================================
 * MACRO:
 *     SERVREG_RCESN_REGISTER_HANDLE_QURT & SERVREG_RCESN_REGISTER_HANDLE_REX
 *
 * Description:
 *     Registers handle with rcesn
 *
 * Parameters:
 *     sr_rcesn_handle     : rcesn handle
 *     sr_rcesn_comp_func  : rcesn compare function
 *     sr_signal           : NHLOS specific notification signal information
 *
 * Returns:
 *     RCESN_HANDLE
 * =====================================================================  */
#if !defined(SERVREG_EXCLUDE_KERNEL_QURT)
#define SERVREG_RCESN_REGISTER_HANDLE_QURT(sr_rcesn_handle, sr_rcesn_comp_func, sr_signal) (\
   {\
      RCESN_HANDLE ret = RCESN_NULL;\
      SERVREG_MON_SIGQURT *sig = (SERVREG_MON_SIGQURT*)sr_signal;\
\
      ret = rcesn_register_handle_qurt(sr_rcesn_handle, sr_rcesn_comp_func, sig->signal, sig->mask);\
      ret;\
   }\
)
#else
#define SERVREG_RCESN_REGISTER_HANDLE_QURT(sr_rcesn_handle, sr_rcesn_comp_func, sr_signal) RCESN_NULL
#endif


#if !defined(SERVREG_EXCLUDE_KERNEL_REX)
#define SERVREG_RCESN_REGISTER_HANDLE_REX(sr_rcesn_handle, sr_rcesn_comp_func, sr_signal) (\
   {\
      RCESN_HANDLE ret = RCESN_NULL;\
      SERVREG_MON_SIGREX *sig = (SERVREG_MON_SIGREX*)sr_signal;\
\
      ret = rcesn_register_handle_rex(sr_rcesn_handle, sr_rcesn_comp_func, sig->mask);\
      ret;\
   }\
)
#else
#define SERVREG_RCESN_REGISTER_HANDLE_REX(sr_rcesn_handle, sr_rcesn_comp_func, sr_signal) RCESN_NULL
#endif

/** =====================================================================
 * MACRO:
 *     SERVREG_RCESN_UNREGISTER_HANDLE_QURT & SERVREG_RCESN_UNREGISTER_HANDLE_REX
 *
 * Description:
 *     De-Registers handle with rcesn
 *
 * Parameters:
 *     sr_rcesn_handle     : rcesn handle
 *     sr_rcesn_comp_func  : rcesn compare function
 *     sr_signal           : NHLOS specific notification signal information
 *
 * Returns:
 *     RCESN_HANDLE
 * =====================================================================  */
#if !defined(SERVREG_EXCLUDE_KERNEL_QURT)
#define SERVREG_RCESN_UNREGISTER_HANDLE_QURT(sr_rcesn_handle, sr_rcesn_comp_func, sr_signal) (\
   {\
      RCESN_HANDLE ret = RCESN_NULL;\
      SERVREG_MON_SIGQURT *sig = (SERVREG_MON_SIGQURT*)sr_signal;\
\
      ret = rcesn_unregister_handle_qurt(sr_rcesn_handle, sr_rcesn_comp_func, sig->signal, sig->mask);\
      ret;\
   }\
)
#else
#define SERVREG_RCESN_UNREGISTER_HANDLE_QURT(sr_rcesn_handle, sr_rcesn_comp_func, sr_signal) RCESN_NULL
#endif


#if !defined(SERVREG_EXCLUDE_KERNEL_REX)
#define SERVREG_RCESN_UNREGISTER_HANDLE_REX(sr_rcesn_handle, sr_rcesn_comp_func, sr_signal) (\
   {\
      RCESN_HANDLE ret = RCESN_NULL;\
      SERVREG_MON_SIGREX *sig = (SERVREG_MON_SIGREX*)sr_signal;\
\
      ret = rcesn_unregister_handle_rex(sr_rcesn_handle, sr_rcesn_comp_func, sig->mask);\
      ret;\
   }\
)
#else
#define SERVREG_RCESN_UNREGISTER_HANDLE_REX(sr_rcesn_handle, sr_rcesn_comp_func, sr_signal) RCESN_NULL
#endif

/** =====================================================================
 * Function: INTERNAL not for PUBLIC use
 *     servreg_get_sr_mon_handle
 *
 * Description:
 *     Checks if a MON node already exists with the given name.
 *
 * Parameters:
 *     name : "soc/domain/subdomain/provider/service" or just "soc/domain/subdomain" name
 *
 * Returns:
 *    SERVREG_MON_HANDLE : handle to the mon node
 *
 * Note :
 *    No need of locks as this function will be called internally only by servreg_create_mon_node()
 * =====================================================================  */
#if (defined(__GNUC__) && __GNUC__ >= 4) || defined(__clang__)
__attribute__((nonnull(1)))
#endif
SERVREG_MON_HANDLE servreg_get_sr_mon_handle(SERVREG_NAME const name);

/** =====================================================================
 * Function: INTERNAL not for PUBLIC use
 *     servreg_set_transaction_id
 *
 * Description:
 *     Set the current transaction_id.
 *
 * Parameters:
 *     sr_mon_handle : Handle to an existing service state which is mapped by domain + service 
 *                     or just domain name
 *
 * Returns:
 * =====================================================================  */
#if (defined(__GNUC__) && __GNUC__ >= 4) || defined(__clang__)
__attribute__((nonnull(1)))
#endif
void servreg_set_transaction_id(SERVREG_MON_HANDLE sr_mon_handle, uint32_t sr_transaction_id);

/** =====================================================================
 * Function:
 *     servreg_mutex_init_dal
 *
 * Description:
 *     Initialize a DAL mutex
 *
 * Parameters:
 *     mutex_p : mutex to be initialized
 *
 * Returns:
 *     None
 * =====================================================================  */
void servreg_mutex_init_dal(servreg_mutex_p const mutex_p);

/** =====================================================================
 * Function:
 *     servreg_mutex_lock_dal
 *
 * Description:
 *     Lock the mutex
 *
 * Parameters:
 *    mutex_p : mutex to be locked
 *
 * Returns:
 *    None
 * =====================================================================  */
void servreg_mutex_lock_dal(servreg_mutex_p const mutex_p);

/** =====================================================================
 * Function:
 *     servreg_mutex_unlock_dal
 *
 * Description:
 *     Unlock the mutex
 *
 * Parameters:
 *     mutex_p : mutex to be unlocked
 *
 * Returns:
 *     None
 * =====================================================================  */
void servreg_mutex_unlock_dal(servreg_mutex_p const mutex_p);

/** =====================================================================
 * Function:
 *     servreg_nmelen
 *
 * Description:
 *     Function to find out the string length
 *
 * Parameters:
 *     name : string
 *
 * Returns:
 *     int : string length
 * =====================================================================  */
int servreg_nmelen(SERVREG_NAME const name);

/** =====================================================================
 * Function:
 *     servreg_nmecmp
 *
 * Description:
 *     Compares two strings
 *
 * Parameters:
 *     name_1 : string 1
 *     name_2 : string 2
 *     len    : length of the string
 *
 * Returns:
 *     -1, 0 or 1
 * =====================================================================  */
int servreg_nmecmp(SERVREG_NAME const name_1, SERVREG_NAME const name_2, int len);

#if defined(__cplusplus)
}
#endif

#endif