#ifndef QURT_PRINTF_H
#define QURT_PRINTF_H
/*=============================================================================

                 qurt_power.h -- H E A D E R  F I L E

GENERAL DESCRIPTION
   Prototypes of printf API  

EXTERNAL FUNCTIONS
   None.

INITIALIZATION AND SEQUENCING REQUIREMENTS
   None.

      Copyright (c) 2010
                    by Qualcomm Technologies Incorporated.  All Rights Reserved.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

 This section contains comments describing changes made to the module.
 Notice that changes are listed in reverse chronological order.


$Header: //components/rel/core.mpss/3.4.c3.11/kernel/qurt_mba/install/MBAv5/include/qurt_printf.h#1 $ 
$DateTime: 2016/03/28 23:02:17 $ $Author: mplcsds1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
03/03/11   op      Add header file 
=============================================================================*/

/*=============================================================================
                        CONSTANTS AND MACROS
=============================================================================*/

#define qurt_printf printf

#endif /* QURT_PRINTF_H */

