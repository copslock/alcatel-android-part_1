#!/usr/bin/env python

class qurt_config:
    def genheader_subcommand(self, arglist):
        from lib.genheader import genheader_cmd
        return genheader_cmd(arglist)
    def update_subcommand(self, arglist):
        from lib.merge import merge_cmd
        return merge_cmd(arglist)
    def usage(self):
        cmds = sorted([z.rsplit('_',1)[0] for z in dir(self) if z.endswith('_subcommand')])
        str = 'First argument must be one of:\n  ' + ', '.join(cmds)
        raise Exception(str)
    def run_command(self, argv):
        from traceback import format_exc as tbstr
        progname = argv[0]
        try:
            print ' '.join(argv)
            raw_args = argv[1:]
            args = [s for s in raw_args if not s == '--traceback']
            if args == raw_args:
                tbstr = None
            try:
                subfunc = getattr(self, '%s_subcommand' % args[0])
            except StandardError:
                self.usage()
            return subfunc(args[1:])
        except (SystemExit, KeyboardInterrupt):
            raise
        except Exception, err:
            if tbstr:
                print tbstr()
            print '%s: Error:\n*** %s' % (progname, err)
        except:
            raise
        return 1
    def main(self):
        import sys
        sys.exit(self.run_command(sys.argv))

qurt_config().main()    # Never returns

# Signatures of the files that this depends on
# 26fb37968334431db86b63e4a49533e7 /local/mnt/workspace/CRMBuilds/MPSS.TH.2.0.c1.9-00078-M8996FAAAANAZM-1_20161030_151805/b/modem_proc/core/kernel/qurt/build/root_pd_img/qdsp6/8996.gps.prod/install/modemv56/scripts/Input/cust_config_template.c
# 3bd5da2f345e6a10068fbeed0395e2b9 /local/mnt/workspace/CRMBuilds/MPSS.TH.2.0.c1.9-00078-M8996FAAAANAZM-1_20161030_151805/b/modem_proc/core/kernel/qurt/build/root_pd_img/qdsp6/8996.gps.prod/install/modemv56/scripts/Input/default_build_config.def
# fb5a5ddd23d5de640e0e4492863fd07d /local/mnt/workspace/CRMBuilds/MPSS.TH.2.0.c1.9-00078-M8996FAAAANAZM-1_20161030_151805/b/modem_proc/core/kernel/qurt/build/root_pd_img/qdsp6/8996.gps.prod/install/modemv56/scripts/Input/static_build_config.def
# fb029620d16a7ed5f97289883f996027 /local/mnt/workspace/CRMBuilds/MPSS.TH.2.0.c1.9-00078-M8996FAAAANAZM-1_20161030_151805/b/modem_proc/core/kernel/qurt/build/root_pd_img/qdsp6/8996.gps.prod/install/modemv56/scripts/Input/qurt_tlb_unlock.xml
# 206b9e12f5643aab9b5c41da144c95f3 /local/mnt/workspace/CRMBuilds/MPSS.TH.2.0.c1.9-00078-M8996FAAAANAZM-1_20161030_151805/b/modem_proc/core/kernel/qurt/build/root_pd_img/qdsp6/8996.gps.prod/install/modemv56/scripts/lib/__init__.py
# 992309777e70b115ee88d151ceea0f4d /local/mnt/workspace/CRMBuilds/MPSS.TH.2.0.c1.9-00078-M8996FAAAANAZM-1_20161030_151805/b/modem_proc/core/kernel/qurt/build/root_pd_img/qdsp6/8996.gps.prod/install/modemv56/scripts/lib/build_qurt_config.py
# 9dbe95fb17059e23e02557c064bf4bee /local/mnt/workspace/CRMBuilds/MPSS.TH.2.0.c1.9-00078-M8996FAAAANAZM-1_20161030_151805/b/modem_proc/core/kernel/qurt/build/root_pd_img/qdsp6/8996.gps.prod/install/modemv56/scripts/lib/build_xml.py
# 85c5b7ff7ae8a1f0340321ae61bf0367 /local/mnt/workspace/CRMBuilds/MPSS.TH.2.0.c1.9-00078-M8996FAAAANAZM-1_20161030_151805/b/modem_proc/core/kernel/qurt/build/root_pd_img/qdsp6/8996.gps.prod/install/modemv56/scripts/lib/ezxml.py
# 476321ba022da3266d0982e4e716cad1 /local/mnt/workspace/CRMBuilds/MPSS.TH.2.0.c1.9-00078-M8996FAAAANAZM-1_20161030_151805/b/modem_proc/core/kernel/qurt/build/root_pd_img/qdsp6/8996.gps.prod/install/modemv56/scripts/lib/genheader.py
# c083132b08dbc9fe4f0e2ad37e4cab81 /local/mnt/workspace/CRMBuilds/MPSS.TH.2.0.c1.9-00078-M8996FAAAANAZM-1_20161030_151805/b/modem_proc/core/kernel/qurt/build/root_pd_img/qdsp6/8996.gps.prod/install/modemv56/scripts/lib/interrupt_xml.py
# e4158f519f3cc00f4d789100b2932607 /local/mnt/workspace/CRMBuilds/MPSS.TH.2.0.c1.9-00078-M8996FAAAANAZM-1_20161030_151805/b/modem_proc/core/kernel/qurt/build/root_pd_img/qdsp6/8996.gps.prod/install/modemv56/scripts/lib/kernel_xml.py
# 25a5e3f8200e2cbfd51b4fc9419e51ce /local/mnt/workspace/CRMBuilds/MPSS.TH.2.0.c1.9-00078-M8996FAAAANAZM-1_20161030_151805/b/modem_proc/core/kernel/qurt/build/root_pd_img/qdsp6/8996.gps.prod/install/modemv56/scripts/lib/machine_xml.py
# 3ecbf3d77adf335100794a93a5b4ce91 /local/mnt/workspace/CRMBuilds/MPSS.TH.2.0.c1.9-00078-M8996FAAAANAZM-1_20161030_151805/b/modem_proc/core/kernel/qurt/build/root_pd_img/qdsp6/8996.gps.prod/install/modemv56/scripts/lib/merge.py
# 4ffa9ed630bd335beb3517b8494d7c1b /local/mnt/workspace/CRMBuilds/MPSS.TH.2.0.c1.9-00078-M8996FAAAANAZM-1_20161030_151805/b/modem_proc/core/kernel/qurt/build/root_pd_img/qdsp6/8996.gps.prod/install/modemv56/scripts/lib/parse_build_params.py
# 7ce8928c359e18db33c8cd5dd370b30f /local/mnt/workspace/CRMBuilds/MPSS.TH.2.0.c1.9-00078-M8996FAAAANAZM-1_20161030_151805/b/modem_proc/core/kernel/qurt/build/root_pd_img/qdsp6/8996.gps.prod/install/modemv56/scripts/lib/parse_spec.py
# 31733f66f57d2783f3b7e99338fdb9ae /local/mnt/workspace/CRMBuilds/MPSS.TH.2.0.c1.9-00078-M8996FAAAANAZM-1_20161030_151805/b/modem_proc/core/kernel/qurt/build/root_pd_img/qdsp6/8996.gps.prod/install/modemv56/scripts/lib/physpool_xml.py
# b9f6f32884a32971e856ae24cd47ad5b /local/mnt/workspace/CRMBuilds/MPSS.TH.2.0.c1.9-00078-M8996FAAAANAZM-1_20161030_151805/b/modem_proc/core/kernel/qurt/build/root_pd_img/qdsp6/8996.gps.prod/install/modemv56/scripts/lib/program_xml.py
# 8dc1fab9af2d318d04c7fd08524ca7b3 /local/mnt/workspace/CRMBuilds/MPSS.TH.2.0.c1.9-00078-M8996FAAAANAZM-1_20161030_151805/b/modem_proc/core/kernel/qurt/build/root_pd_img/qdsp6/8996.gps.prod/install/modemv56/scripts/lib/qurt.py
# 3c6d6023399634c88613a2d8e0a701e2 /local/mnt/workspace/CRMBuilds/MPSS.TH.2.0.c1.9-00078-M8996FAAAANAZM-1_20161030_151805/b/modem_proc/core/kernel/qurt/build/root_pd_img/qdsp6/8996.gps.prod/install/modemv56/scripts/Input/build_params.txt
# 09c5f5d01c138ca2e6284521f7b62199 /local/mnt/workspace/CRMBuilds/MPSS.TH.2.0.c1.9-00078-M8996FAAAANAZM-1_20161030_151805/b/modem_proc/core/kernel/qurt/build/root_pd_img/qdsp6/8996.gps.prod/install/modemv56/scripts/Input/cust_config.c
# a755941d1b2a37dc441c06a5b3e8f467 /local/mnt/workspace/CRMBuilds/MPSS.TH.2.0.c1.9-00078-M8996FAAAANAZM-1_20161030_151805/b/modem_proc/core/kernel/qurt/build/root_pd_img/qdsp6/8996.gps.prod/install/modemv56/scripts/qurt-image-build.py
