#ifndef __DLPAGER_SWAPMEM_H__
#define __DLPAGER_SWAPMEM_H__

/*===========================================================================
 * FILE:         dlpager_swapmem.h
 *
 * SERVICES:     DL PAGER
 *
 * DESCRIPTION:  dlpager swap pool memory managment
 *
 * Copyright (c) 2015 Qualcomm Technologies Incorporated.
 * All Rights Reserved. QUALCOMM Proprietary and Confidential.
 ===========================================================================*/

/*===========================================================================

  EDIT HISTORY FOR MODULE

  $Header: //components/rel/core.mpss/3.4.c3.11/kernel/dlpager/inc/dlpager_swapmem.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
05/15/15   ao      Initial revision
===========================================================================*/

#include <qurt.h>

typedef enum {
    DLPAGER_SWAPMEM_ATTR_RX,
    DLPAGER_SWAPMEM_ATTR_RW,
} dlpager_swapmem_attr_type_t;

void        dlpager_swapmem_create(unsigned pool_index, int requested_size,
                                   dlpager_swapmem_attr_type_t type);
qurt_addr_t dlpager_swapmem_get_rw(unsigned pool_index, unsigned pool_offset);
qurt_addr_t dlpager_swapmem_get_rx(unsigned pool_index, unsigned pool_offset);
qurt_addr_t dlpager_swapmem_get_pa(unsigned pool_index, unsigned pool_offset);
void        dlpager_swapmem_get_pool_and_offset(int* pool_index,
                                                int* pool_offset,
                                                qurt_addr_t pa);
void        dlpager_swapmem_dump_status(void);

#endif
