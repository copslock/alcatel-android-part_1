/*==========================================================================
 * FILE:         dlpager_main.c
 *
 * SERVICES:     DL PAGER
 *
 * DESCRIPTION:  This file provides the implementation of pager service
 *               initilaiztion and creation of pager thread. It servers as an
 *               interface to Kernel.
 *
 * Copyright (c) 2010-2013 Qualcomm Technologies Incorporated.
 * All Rights Reserved. QUALCOMM Proprietary and Confidential.
=============================================================================*/


/*===========================================================================

            EDIT HISTORY FOR MODULE

$Header: //components/rel/core.mpss/3.4.c3.11/kernel/dlpager/src/dlpager_main.c#1 $ $DateTime: 2016/03/28 23:02:17 $ $Author: mplcsds1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
07/01/13   bc      Re-write dlpager_posix_main.c
===========================================================================*/
#include <stdlib.h>
#ifndef DLPAGER_UNIT_TEST
#include <stringl.h>
#endif
#include <assert.h>
#include <qurt.h>

#include "dlpager_main.h"
#include "memload_handler.h"
#include "dlpager_log.h"
#include "dlpager_aux.h"

#define PAGE_SIZE_4K     (4*1024)      /* 4K page, the smallest pages supported */
#define PAGE_SIZE_4M     (4*1024*1024) /* 4M page, the largest page supported */

/* dlpager Thread ID */
qurt_thread_t dlpager_tid;

/* dlpager attributes set externally */
dlpager_attr_t  dlpager_attr;


extern unsigned int __attribute__ ((weak)) __swapped_segments_start__;
extern unsigned int __attribute__ ((weak)) __swapped_segments_end__;

unsigned int __attribute__((section (".data"))) start_va_uncompressed_text = 0;
unsigned int __attribute__((section (".data"))) end_va_uncompressed_text = 0;
unsigned int __attribute__((section (".data"))) start_va_compressed_text = 0;
unsigned int __attribute__((section (".data"))) end_va_compressed_text = 0;
unsigned int __attribute__((section (".data"))) start_va_uncompressed_rw = 0;
unsigned int __attribute__((section (".data"))) end_va_uncompressed_rw = 0;
unsigned int __attribute__((section (".data"))) start_va_compressed_rw = 0;
unsigned int __attribute__((section (".data"))) end_va_compressed_rw = 0;

/* DL PAGER Versioning*/
static volatile char dlpager_version[] = DLPAGER_VERSION;

/*
 * dlpager_main
 *
 * DL pager thread. It creates two regions to create a shadow mapping to the
 * swap pool (physical) for maintenance. One regions is RW to help loading the
 * page and the other regions is to help invalidate I-cache.
 */
void dlpager_main (void *pArg)
{
    int i, ret;
    qurt_sysevent_pagefault_t pf_data;
    unsigned seg_base, seg_size;

    for (i = 0; i < dlpager_attr.num_swap_pools; i++) {
        /* Initialize swap pools for paging */
        DL_DEBUG("Creating_swap_pool  : n %d  %dKB  seg_base %08X  %dKB\n",
                 i,
                 dlpager_attr.swap_pool[i].size / 1024,
                 dlpager_attr.swap_pool[i].seg_base,
                 dlpager_attr.swap_pool[i].seg_size / 1024);

        dlpager_memload_init_pool (i,
                                   dlpager_attr.swap_pool[i].size,
                                   dlpager_attr.swap_pool[i].seg_base,
                                   dlpager_attr.swap_pool[i].seg_size);
    }

    while (1) {
        unsigned int ssr_cause = 0;
        pf_data.thread_id = 0;
        pf_data.fault_addr = 0;

        ret = qurt_exception_wait_pagefault (&pf_data);

        assert(ret == QURT_EOK &&
               (unsigned)&__swapped_segments_start__ <= pf_data.fault_addr &&
               pf_data.fault_addr < (unsigned)&__swapped_segments_end__);

#ifdef DLPAGER_QURT_SUPPORTS_SSR
        ssr_cause = pf_data.ssr_cause;
#else
        ssr_cause = CAUSE_ANY_TLB_MISS;
#endif

        DL_DEBUG("receive page fault info: threadID=%x, faulting address=%x, cause=%x\n",
                 pf_data.thread_id, pf_data.fault_addr, ssr_cause);

        dlpager_memload_fault_handler(pf_data.fault_addr, pf_data.thread_id, ssr_cause);

        /* resume the faultiing thread */
        ret = qurt_thread_resume(pf_data.thread_id);
        assert(ret == QURT_EOK);

        dlpager_memload_postfault_handler(pf_data.fault_addr, pf_data.thread_id, ssr_cause);
    }
}

/*
 * dlpager_init
 *
 * Initialization function to start pager thread. Input parameters decides
 * the configuration of swap pool size, page size etc.
 */
int dlpager_init (void)
{
    qurt_thread_attr_t thread_attr;
    int i, rc, status;
    void *pStack;
    unsigned int page_size;

    /* No need to start pager, if swap segments are not available */
    if (&__swapped_segments_start__ == &__swapped_segments_end__) {
        return 0;
    }

    i = strlen( DLPAGER_VERSION );
    memscpy( (void*)&dlpager_version, i, DLPAGER_VERSION, i );

    dlpager_log_init();
    dlpager_memload_init();

    rc = dlpager_get_attr (&dlpager_attr);

    assert (rc == 0);

    assert (dlpager_attr.num_swap_pools <= DLPAGER_MAX_SWAP_POOLS);

    /* If there are no swap pools, there is no point in starting DL Pager */
    if (dlpager_attr.num_swap_pools == 0) {
        DL_DEBUG("No need to start DL Pager\n");
        return -1;
    }

    /* Demand paging Thread */
    qurt_thread_attr_init (&thread_attr);
    pStack = malloc (DLPAGER_MAIN_STACK_SIZE);
    assert (pStack != NULL);
    qurt_thread_attr_set_name (&thread_attr, "DLPager_main");
    qurt_thread_attr_set_stack_addr (&thread_attr, pStack);
    qurt_thread_attr_set_stack_size (&thread_attr, DLPAGER_MAIN_STACK_SIZE);
    qurt_thread_attr_set_priority (&thread_attr, DLPAGER_MAIN_PRIO);
    status = qurt_thread_create (&dlpager_tid, &thread_attr, dlpager_main, (void *)0);
    assert(status == 0);
    DL_DEBUG(" Starting DL main Thread , id=%d\n", dlpager_tid);

    return 0;
}
