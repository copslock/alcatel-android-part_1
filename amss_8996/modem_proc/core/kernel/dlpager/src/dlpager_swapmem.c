/*===========================================================================
 * FILE:         dlpager_swapmem.c
 *
 * SERVICES:     DL PAGER
 *
 * DESCRIPTION:  dlpager swap pool memory managment
 *
 * Copyright (c) 2015 Qualcomm Technologies Incorporated.
 * All Rights Reserved. QUALCOMM Proprietary and Confidential.
 ===========================================================================*/

/*===========================================================================

  EDIT HISTORY FOR MODULE

  $Header: //components/rel/core.mpss/3.4.c3.11/kernel/dlpager/src/dlpager_swapmem.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
05/15/15   ao      Initial revision
===========================================================================*/

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <qurt.h>

#include "dlpager_swapmem.h"
#include "dlpager_constants.h"

#ifndef DLPAGER_MAX_SWAPMEMS
#define DLPAGER_MAX_SWAPMEMS 2
#endif

#ifndef DLPAGER_MAX_GAPS_PER_SWAPMEM
#define DLPAGER_MAX_GAPS_PER_SWAPMEM 16
#endif

#ifndef DLPAGER_MIN_REUSE_GAP_SIZE
#define DLPAGER_MIN_REUSE_GAP_SIZE 4096
#endif

extern uint32_t __attribute__((section (".data"))) start_va_compressed_rw;
extern uint32_t __attribute__((section (".data"))) end_va_compressed_rw;

unsigned DLPager_rx_swap_vaddr  __attribute((section(".data"))) = 0;
unsigned DLPager_rx_swap_size   __attribute((section(".data"))) = 0;
unsigned DLPager_rw_swap_vaddr  __attribute((section(".data"))) = 0;
unsigned DLPager_rw_swap_size   __attribute((section(".data"))) = 0;
unsigned DLPager_rwx_swap_vaddr __attribute((section(".data"))) = 0;
unsigned DLPager_rwx_swap_size  __attribute((section(".data"))) = 0;

#define SWAPMEM_RECLAIM_LINKER_GAP_RX(n)                               \
    extern uint32_t __attribute__ ((weak)) __gaprec_start_rx_ ## n ## __; \
    extern uint32_t __attribute__ ((weak)) __gaprec_end_rx_ ## n ## __; \
    if (requested_size >= 0)                                            \
        requested_size -= swapmem_gap_acquire(pool_index,               \
            (unsigned int)&__gaprec_start_rx_ ## n ## __,               \
            (unsigned int)&__gaprec_end_rx_ ## n ## __, type, requested_size)
#define SWAPMEM_RECLAIM_LINKER_GAP_RW(n)                               \
    extern uint32_t __attribute__ ((weak)) __gaprec_start_rw_ ## n ## __; \
    extern uint32_t __attribute__ ((weak)) __gaprec_end_rw_ ## n ## __; \
    if (requested_size >= 0)                                            \
        requested_size -= swapmem_gap_acquire(pool_index,               \
            (unsigned int)&__gaprec_start_rw_ ## n ## __,               \
            (unsigned int)&__gaprec_end_rw_ ## n ## __, type, requested_size)

typedef struct swapmem_gap_ {
    qurt_addr_t base_pa;
    unsigned    size;
    unsigned    offset;
    dlpager_swapmem_attr_type_t type;
} swapmem_gap_t;

struct swapmem_swapmem_ {
    qurt_addr_t           base_rw;
    qurt_addr_t           base_rx;
    qurt_addr_t*          pa_mappings;
    unsigned              size;
    swapmem_gap_t         gaps[DLPAGER_MAX_GAPS_PER_SWAPMEM];
    unsigned              last_gap_idx;
    unsigned              last_gap_offset;
} swapmem_mems[DLPAGER_MAX_SWAPMEMS];

static qurt_mem_pool_t swapmem_physpool;

static void swapmem_invalidate_cache(int pool_index);
static int  swapmem_gap_acquire(int pool_index, uint32_t start, uint32_t end,
                                dlpager_swapmem_attr_type_t type, int size);
static int  swapmem_get_pyhspool_mem(int pool_index, unsigned size,
                                     dlpager_swapmem_attr_type_t type);


/*****************************************************************************
 dlpager_swapmem_create - Creates swap memory.
 It is composed of a number of gaps, these are created from:
     o Reclaimable gap areas:
       (e.g. gaps and RW compressed data copied elsewhere) and
     o Requested from the kernel's physpool.

 Swapmem provides:
     o Actual memory allocation from either gaps or physpool
     o Access to address for a page (4K) of RX VA, RW VA and PA
     o Mappings for VAs to corresponding PAs except for unsecure mappings:
       - RW for RX pool
       - RX for RW pool
*****************************************************************************/
void dlpager_swapmem_create(unsigned pool_index,
                            int requested_size,
                            dlpager_swapmem_attr_type_t type)
{
    int ret;
    qurt_mem_region_attr_t attr;
    qurt_mem_region_t region;

    if (requested_size <= 0) return;
    if (pool_index >= DLPAGER_MAX_SWAPMEMS) return;
    swapmem_mems[pool_index].size = requested_size;
    swapmem_mems[pool_index].pa_mappings =
        malloc(requested_size / PAGE_SIZE * sizeof(uint32_t));

    DL_DEBUG("Creating_swap_memory: %dKB type %d\n", requested_size / 1024, type);

    /* Set base_rw to contiguous VA range with no memory associated yet.
       This VA range is used by both RX and RW pools.
       Mapping will be created only for RW pools */
    qurt_mem_pool_attach ("DEFAULT_PHYSPOOL", &swapmem_physpool);
    qurt_mem_region_attr_init(&attr);
    qurt_mem_region_attr_set_mapping(&attr, QURT_MEM_MAPPING_NONE);
    ret = qurt_mem_region_create(&region, requested_size, swapmem_physpool, &attr);
    assert(ret == QURT_EOK);
    qurt_mem_region_attr_get(region, &attr);
    qurt_mem_region_attr_get_virtaddr(
        &attr, &swapmem_mems[pool_index].base_rw);

    /* Set base_rx to contiguous VA range with no memory associated yet.
       This VA range is used by RX pools only.
       Mapping will be created only for RX pools */
    if (type == DLPAGER_SWAPMEM_ATTR_RX)
    {
        qurt_mem_region_attr_init (&attr);
        qurt_mem_region_attr_set_mapping (&attr, QURT_MEM_MAPPING_NONE);
        ret = qurt_mem_region_create (&region, requested_size,
                                      swapmem_physpool, &attr);
        assert (ret == QURT_EOK);
        qurt_mem_region_attr_get(region, &attr);
        qurt_mem_region_attr_get_virtaddr(
            &attr, &swapmem_mems[pool_index].base_rx);
    }

#ifdef Q6ZIP_GAP_RECLAIMING

#ifdef Q6ZIP_QURT_GAPS


    /* Set va and va_size to mappings from QuRT collected gaps
       Also acquire the RW compressed area gap that QuRT doesn't know about */
    qurt_addr_t va;
    int va_size;
    if (type == DLPAGER_SWAPMEM_ATTR_RX)
    {
        va = DLPager_rx_swap_vaddr;
        va_size = DLPager_rx_swap_size;
   }
    else
    {
        requested_size -= swapmem_gap_acquire(pool_index,
            start_va_compressed_rw, end_va_compressed_rw,
            type, requested_size);

        va = DLPager_rw_swap_vaddr;
        va_size = DLPager_rw_swap_size;
    }

    /* Acquire gaps in the QuRT mapping */
    if (va_size > PAGE_SIZE)
    {
        qurt_addr_t last_start = va;
        volatile int p = *((volatile int*)va);
        (void)p;
        qurt_addr_t last_pa = qurt_lookup_physaddr(va);
        va += PAGE_SIZE;
        int i;
        for (i = 1; i < va_size / PAGE_SIZE; ++i)
        {
            volatile int p = *((volatile int*)va);
            (void)p;
            qurt_addr_t pa = qurt_lookup_physaddr(va);
            if (pa != last_pa + PAGE_SIZE)
            {
                requested_size -= swapmem_gap_acquire(pool_index,
                    last_start, va, type, requested_size);
                last_start = va;
            }
            last_pa = pa;
            va += PAGE_SIZE;
        }
        requested_size -= swapmem_gap_acquire(pool_index,
            last_start, va, type, requested_size);
    }
#else

    /* Acquire gaps from linker symbols and RW compressed area */
    if (type == DLPAGER_SWAPMEM_ATTR_RX)
    {
        SWAPMEM_RECLAIM_LINKER_GAP_RX(0);
        SWAPMEM_RECLAIM_LINKER_GAP_RX(1);
        SWAPMEM_RECLAIM_LINKER_GAP_RX(2);
        SWAPMEM_RECLAIM_LINKER_GAP_RX(3);
        SWAPMEM_RECLAIM_LINKER_GAP_RX(4);
        SWAPMEM_RECLAIM_LINKER_GAP_RX(5);
        SWAPMEM_RECLAIM_LINKER_GAP_RX(6);
        SWAPMEM_RECLAIM_LINKER_GAP_RX(7);
        SWAPMEM_RECLAIM_LINKER_GAP_RX(8);
        SWAPMEM_RECLAIM_LINKER_GAP_RX(9);
        SWAPMEM_RECLAIM_LINKER_GAP_RX(10);
        SWAPMEM_RECLAIM_LINKER_GAP_RX(11);
        SWAPMEM_RECLAIM_LINKER_GAP_RX(12);
        SWAPMEM_RECLAIM_LINKER_GAP_RX(13);
        SWAPMEM_RECLAIM_LINKER_GAP_RX(15);
        SWAPMEM_RECLAIM_LINKER_GAP_RX(15);
    }
    else
    {
        requested_size -= swapmem_gap_acquire(pool_index,
            start_va_compressed_rw, end_va_compressed_rw,
            type, requested_size);
        SWAPMEM_RECLAIM_LINKER_GAP_RW(0);
        SWAPMEM_RECLAIM_LINKER_GAP_RW(1);
        SWAPMEM_RECLAIM_LINKER_GAP_RW(2);
        SWAPMEM_RECLAIM_LINKER_GAP_RW(3);
        SWAPMEM_RECLAIM_LINKER_GAP_RW(4);
        SWAPMEM_RECLAIM_LINKER_GAP_RW(5);
        SWAPMEM_RECLAIM_LINKER_GAP_RW(6);
        SWAPMEM_RECLAIM_LINKER_GAP_RW(7);
        SWAPMEM_RECLAIM_LINKER_GAP_RW(8);
        SWAPMEM_RECLAIM_LINKER_GAP_RW(9);
        SWAPMEM_RECLAIM_LINKER_GAP_RW(10);
        SWAPMEM_RECLAIM_LINKER_GAP_RW(11);
        SWAPMEM_RECLAIM_LINKER_GAP_RW(12);
        SWAPMEM_RECLAIM_LINKER_GAP_RW(13);
        SWAPMEM_RECLAIM_LINKER_GAP_RW(15);
        SWAPMEM_RECLAIM_LINKER_GAP_RW(15);
    }
#endif
#endif
    swapmem_get_pyhspool_mem(pool_index, requested_size, type);
    swapmem_invalidate_cache(pool_index);

    DL_DEBUG("\n");
}


/*****************************************************************************
 dlpager_swapmem_get_rw - Return RW VA address for pool at offset.
 Only guaranteed to be valid for 4KB.
 For RX pools this VA is not mapped to the corresponding PA
*****************************************************************************/
qurt_addr_t dlpager_swapmem_get_rw(unsigned pool_index, unsigned pool_offset)
{
    qurt_addr_t ret = 0;
    if (swapmem_mems[pool_index].base_rw)
        ret = swapmem_mems[pool_index].base_rw + pool_offset;
    return ret;
}

/*****************************************************************************
 dlpager_swapmem_get_rx - Return RX VA address for pool at offset.
 Only guaranteed to be valid for 4KB.
 For RW pools this VA address range does not exist
*****************************************************************************/
qurt_addr_t dlpager_swapmem_get_rx(unsigned pool_index, unsigned pool_offset)
{
    qurt_addr_t ret = 0;
    if (swapmem_mems[pool_index].base_rx)
        ret = swapmem_mems[pool_index].base_rx + pool_offset;
    return ret;
}

/*****************************************************************************
 dlpager_swapmem_get_pa - Return PA address for pool at offset.
 Only guaranteed to be valid for 4KB.
*****************************************************************************/
qurt_addr_t dlpager_swapmem_get_pa(unsigned pool_index, unsigned pool_offset)
{
    qurt_addr_t ret = 0;
    if (swapmem_mems[pool_index].pa_mappings)
        ret = swapmem_mems[pool_index].pa_mappings[pool_offset / PAGE_SIZE];
    return ret;
}

/*****************************************************************************
 dlpager_swapmem_get_pool_and_offset - Return pool and offset for PA.
*****************************************************************************/
void dlpager_swapmem_get_pool_and_offset(int* pool_index,
                                         int* pool_offset,
                                         qurt_addr_t pa)
{
    int i, j;
    *pool_index  = -1; /* return -1 if not found */
    *pool_offset = -1; /* return -1 if not found */

    for (i = 0; i < DLPAGER_MAX_SWAPMEMS; ++i)
    {
        for (j = 0; j < DLPAGER_MAX_GAPS_PER_SWAPMEM; ++j)
        {
            qurt_addr_t base_pa = swapmem_mems[i].gaps[j].base_pa;
            if (base_pa == 0) break;

            if (pa >= base_pa &&
                pa < (base_pa + swapmem_mems[i].gaps[j].size))
            {
                *pool_index = i;
                *pool_offset = swapmem_mems[i].gaps[j].offset +
                    (pa - base_pa);
                return;
            }
        }
    }

}

/*****************************************************************************
 swapmem_invalidate_cache - Invalidates cache for pool.
*****************************************************************************/
static void swapmem_invalidate_cache(int pool_index)
{
    int i, ret;
    for (i = 0; i < DLPAGER_MAX_GAPS_PER_SWAPMEM; ++i)
    {
        swapmem_gap_t* p = &(swapmem_mems[pool_index].gaps[i]);
        if (p->base_pa)
        {
            qurt_addr_t va = swapmem_mems[pool_index].base_rw + p->offset;
            DL_DEBUG("Invalidating_RW     : %08X-%08X PA:%08X-%08X  %dKB\n",
                     va, va + p->size, p->base_pa, p->base_pa + p->size,
                     p->size / 1024);

            /* For type RX there is no RW mapping created by swapmem.
               Clean RW cache (create/remove RW mapping for it) and
               clean RX cache (mapping already avaiable) */
            if (swapmem_mems[pool_index].base_rx)
            {
                ret = qurt_mapping_create(va, p->base_pa, p->size,
                                          QURT_MEM_CACHE_WRITEBACK,
                                          QURT_PERM_READ | QURT_PERM_WRITE);
                assert (ret == QURT_EOK);

                qurt_mem_cache_clean(va, p->size,
                                     QURT_MEM_CACHE_INVALIDATE, QURT_MEM_DCACHE);

                ret = qurt_mapping_remove (va, p->base_pa, p->size);
                assert( ret == QURT_EOK );

                va = swapmem_mems[pool_index].base_rx + p->offset;
                DL_DEBUG("Invalidating_RX     : %08X-%08X PA:%08X-%08X  %dKB\n",
                         va, va + p->size, p->base_pa, p->base_pa + p->size,
                         p->size / 1024);
                qurt_mem_cache_clean(va, p->size,
                                     QURT_MEM_CACHE_INVALIDATE,
                                     QURT_MEM_ICACHE);
            }
            else
            {
                /* For type RW, only invalidate RW, there is no RX */
                qurt_mem_cache_clean(va, p->size,
                                     QURT_MEM_CACHE_INVALIDATE, QURT_MEM_DCACHE);

            }
        }
    }
}

/*****************************************************************************
 swapmem_gap_acquire - Acquires up to req_size bytes from VAs start/end
 Returns amount of memory acquired in bytes
*****************************************************************************/
static int swapmem_gap_acquire(int pool_index, uint32_t start, uint32_t end,
                               dlpager_swapmem_attr_type_t type,
                               int req_size)
{
    int ret;
    int size = end - start;

    /* Check for space in gap array */
    if (swapmem_mems[pool_index].last_gap_idx >= DLPAGER_MAX_GAPS_PER_SWAPMEM)
        return 0;

    /* Align gap borders */
    start = (start + (0x1000 - 1)) & ~0xFFF;
    end = end & ~0xFFF;
    size = end - start;

    /* if last gap can't fit last size, then skip so physpool is used */
    if (size < req_size &&
        swapmem_mems[pool_index].last_gap_idx >= (DLPAGER_MAX_GAPS_PER_SWAPMEM - 1))
        return 0;

    if (size <= DLPAGER_MIN_REUSE_GAP_SIZE) return 0;
    if (req_size <= 0) return 0;

    DL_DEBUG("Acquiring_gap       : %08X-%08X  %dKB  Requested: %dKB\n",
             (unsigned)start, (unsigned)end, size / 1024, req_size / 1024);

    /* Only acquire up to req_size */
    if (size > req_size) size = req_size;

    /* Create permanent mappings, RX for RX pool and RW for RW pool */
    volatile int p = *((volatile int*)start); /* in order to lookup */
    (void)p;
    qurt_addr_t pa = qurt_lookup_physaddr(start);
    if (type == DLPAGER_SWAPMEM_ATTR_RX)
    {
        qurt_addr_t va = swapmem_mems[pool_index].base_rx +
            swapmem_mems[pool_index].last_gap_offset;
        DL_DEBUG("Mapping_RX          : %08X-%08X  PA:%08X-%08X  %dKB\n",
                 va, va + size, pa, pa + size, size / 1024);
        ret = qurt_mapping_create(va, pa, size, QURT_MEM_CACHE_WRITEBACK,
                                  QURT_PERM_READ | QURT_PERM_EXECUTE);
        assert (ret == QURT_EOK);
    }
    else
    {
        qurt_addr_t va = swapmem_mems[pool_index].base_rw +
            swapmem_mems[pool_index].last_gap_offset;
        DL_DEBUG("Mapping_RW          : %08X-%08X  PA:%08X-%08X  %dKB\n",
                 va, va + size, pa, pa + size, size / 1024);
        ret = qurt_mapping_create(va, pa, size, QURT_MEM_CACHE_WRITEBACK,
                                  QURT_PERM_READ | QURT_PERM_WRITE);
        assert (ret == QURT_EOK);
    }

    /* Populate pa_mappings array for fast PA lookup by get_pa API */
    int i;
    for (i = 0; i < (size / PAGE_SIZE); ++i)
    {
        int m = swapmem_mems[pool_index]. last_gap_offset / PAGE_SIZE + i;
        swapmem_mems[pool_index].pa_mappings[m] = pa + i * PAGE_SIZE;
    }

    /* Update gap info */
    int last_gap_idx = swapmem_mems[pool_index].last_gap_idx;
    swapmem_gap_t* p2 = &(swapmem_mems[pool_index].gaps[last_gap_idx]);
    p2->base_pa = pa;
    p2->size    = size;
    p2->offset  = swapmem_mems[pool_index].last_gap_offset;
    p2->type    = type;
    swapmem_mems[pool_index].last_gap_idx++;
    swapmem_mems[pool_index].last_gap_offset += size;

    DL_DEBUG("Acquired_gap        : %08X-%08X  PA:%08X-%08X  %dKB\n",
             (unsigned)start, (unsigned)end, pa, pa + size, size / 1024);
    return size;
}

/*****************************************************************************
 swapmem_get_pyhspool_mem - Acquires size bytes from the physpool
 Returns amount of memory acquired in bytes
*****************************************************************************/
static int swapmem_get_pyhspool_mem(int pool_index, unsigned size,
                                    dlpager_swapmem_attr_type_t type)
{
    int ret;
    qurt_mem_region_attr_t attr;
    qurt_mem_region_t region;
    qurt_addr_t pa, va;

    if (size <= 0) return 0;

    DL_DEBUG("Requesting_physpool : %dKB\n", size / 1024);

    /* Allocates memory region for swap pool from the physpool */
    qurt_mem_region_attr_init(&attr);
    qurt_mem_region_attr_set_mapping(&attr, QURT_MEM_MAPPING_VIRTUAL_RANDOM);
    ret = qurt_mem_region_create(&region, size, swapmem_physpool, &attr);
    assert (ret == QURT_EOK);
    qurt_mem_region_attr_get(region, &attr);
    qurt_mem_region_attr_get_physaddr(&attr, &pa);
    qurt_mem_region_attr_get_virtaddr(&attr, &va);
    qurt_mem_region_attr_get_size(&attr, &size);

    /* Now that we have the memory we can acquire it as any gap */
    size = swapmem_gap_acquire(pool_index, va, va + size, type, size);

    /* Remove default RW mapping added by memory creation above
       Note that gap_acquire needs to be able to get PA for the VA
       so this needs mapping needs to be removed after gap_acquire */
    DL_DEBUG("Removing_mapping    : %08X-%08X  PA:%08X-%08X  %dKB\n",
             va, va + size, pa, pa + size, size / 1024);
    ret = qurt_mapping_remove (va, pa, size);
    assert( ret == QURT_EOK );

    DL_DEBUG("Acquired_physpool   : PA:%08X-%08X  %dKB\n",
             pa, pa + size, size / 1024);

    return size;
}

/*****************************************************************************
 dlpager_swapmem_dump_status - prints swapmem stats

*****************************************************************************/
void dlpager_swapmem_dump_status(void)
{
    int i, j;
    printf("============================================================\n");
    printf("Swap Pools Memory Status\n");
    printf("============================================================\n");
    for (j = 0; j < DLPAGER_MAX_SWAPMEMS; ++j)
    {
        printf("Swap pool: %d  %dKB\n", j, swapmem_mems[j].size / 1024);
        printf("RW range %08X  %08X\n",
               swapmem_mems[j].base_rw,
               (unsigned)swapmem_mems[j].base_rw + swapmem_mems[j].size);
        if (swapmem_mems[j].base_rx)
            printf("RX range %08X  %08X\n",
                   swapmem_mems[j].base_rx,
                   (unsigned)swapmem_mems[j].base_rx + swapmem_mems[j].size);
        printf("============================================================\n");
        printf("Id KBs offset PA\n");
        printf("============================================================\n");
        if (swapmem_mems[j].size == 0) continue;
        for (i = 0; i < DLPAGER_MAX_GAPS_PER_SWAPMEM; ++i)
        {
            swapmem_gap_t* p = &(swapmem_mems[j].gaps[i]);
            if (p->base_pa)
            {
                printf("%2d %3u   %4X %08X\n",
                       i,
                       p->size / 1024,
                       p->offset,
                       p->base_pa);
            }
        }
        printf("============================================================\n");
        if (swapmem_mems[j].base_rx)
        {
            printf("Id RW       RX       PA\n");
            printf("============================================================\n");
            for (i = 0; i < swapmem_mems[j].size / PAGE_SIZE; ++i)
            {
                printf("%2d %08X %08X %08X\n", i, swapmem_mems[j].base_rw + i * PAGE_SIZE,
                       swapmem_mems[j].base_rx + i * PAGE_SIZE,
                       swapmem_mems[j].pa_mappings[i]);
            }
        }
        else
        {
            printf("Id RW       PA\n");
            printf("============================================================\n");
            for (i = 0; i < swapmem_mems[j].size / PAGE_SIZE; ++i)
            {
                printf("%2d %08X %08X\n", i, swapmem_mems[j].base_rw + i * PAGE_SIZE,
                       swapmem_mems[j].pa_mappings[i]);
            }
        }
        printf("\n");
    }
}
