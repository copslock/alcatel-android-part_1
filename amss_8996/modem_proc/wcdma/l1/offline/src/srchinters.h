#ifndef SRCHINTERS_H
#define SRCHINTERS_H
/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                         S R C H I N T E R S . H                  

GENERAL DESCRIPTION
  This module handles processing of measurement requests information from RRC
  for inter-RAT neighbors.

EXTERNALIZED FUNCTIONS

INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2003 by Qualcomm Technologies, Inc. All Rights Reserved.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/wcdma/l1/offline/src/srchinters.h#1 $ $DateTime: 2016/03/28 23:02:57 $ $Author: mplcsds1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
01/21/03   ddh     Created file.

===========================================================================*/

/***************************************************************************

    D A T A     S T R U C T U R E S 
    
 ***************************************************************************/


/*=========================================================================

    V A R I A B L E     D E C L A R A T I O N S                                                                             
  
 ===========================================================================*/
 
/*=========================================================================

    F U N C T I O N     D E C L A R A T I O N S                                                                             
  
 ===========================================================================*/

#endif /* SRCHINTERS_H */
