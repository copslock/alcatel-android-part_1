#ifndef DLPHCFG_H
#define DLPHCFG_H

/*============================================================================
                           D L _ P H C H _ C F G . H
GENERAL DESCRIPTION

EXTERNALIZED FUNCTIONS


INTERNALIZED FUNCTIONS


INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2007 by Qualcomm Technologies, Inc.  All Rights Reserved.

============================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$PVCSPath:  L:/src/asw/MSM5200/l1/vcs/dldec.h_v   1.33   11 Jul 2002 22:14:10   gurdeeps  $
$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/wcdma/l1/offline/src/dl_phch_cfg.h#1 $ $DateTime: 2016/03/28 23:02:57 $ $Author: mplcsds1 $

when        who     what, where, why
--------    ---     --------------------------------------------------------
04/02/13    pr      Mainlined FEATURE_WCDMA_FDPCH 
02/26/09    rgn     Added align type to phychan db
12/10/08    ks      Mainlining FEATURE_WCDMA_DL_ENHANCED
04/20/07    vb      added rf chan field to add struct
12/12/06    mc      Synced up for MBMS Phase2A integration
12/12/06    mc      Synced up for MBMS demo - 3042 baseline
11/29/06    mc      Initial version for MBMS demo integration.
============================================================================*/

#include "wcdma_variation.h"
#include "comdef.h"
#include "customer.h"
#include "l1dlphychancfg.h"
#include "tlm.h"

#endif /* DLPHCFG_H */

