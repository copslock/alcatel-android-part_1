#ifndef WTR3950_COMMON_DEVICE_INTF_H
#define WTR3950_COMMON_DEVICE_INTF_H
/*! 
  @file
  wtr3950_common_device_intf.h
 
  @brief
  Contains the interface for RFC to WTR3950 Common driver

  @details
  Contains function prototypes to create WTR3950 common devices and any other
  direct configuration from RFC.

  This header is wtr3950 specific and hence must be included by WTR3950 RF cards 
  only.

  This file will have to be included by RFC to communicate with WTR3950 and must 
  be shipped.

  @addtogroup WTR3950_COMMON
  @{
*/

/*==============================================================================

Copyright (c) 2013, 2014 by QUALCOMM Technologies, Inc.  All Rights Reserved.
Copyright (c) 2015, 2016 by QUALCOMM Technologies, Inc.  All Rights Reserved.

Qualcomm Technologies Proprietary and Confidential

  Export of this technology or software is regulated by the U.S. 
  Government. Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of QUALCOMM Technologies Incorporated.

===============================================================================*/

/*==============================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

  $Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/rfdevice_wtr3950/api/wtr3950_common_device_intf.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------- 
01/01/15   tks     Initial Revision

==============================================================================*/

#include "rfdevice_cmn_intf.h"
#include "rfc_common.h"
#include "rfdevice_rxtx_common_class.h"


#ifdef __cplusplus
extern "C" {
#endif


/*! Max number of Common devices supported by WTR3950 */
#define WTR3950_MAX_DEVICES 2

/*----------------------------------------------------------------------------*/
rfdevice_rxtx_common_class*
wtr3950_common_create_device
(
  rfc_device_cfg_info_type wtr3950_rffe_info,
  rfdevice_cmn_int_dev_cal_data_type cal_data,
  const char* efs_file_path
);

boolean wtr3950_common_create_wxe_device
(
  rfdevice_rxtx_common_class* instance,
  rfc_device_cfg_info_type* cfg
);

/*----------------------------------------------------------------------------*/
rfdevice_class*
wtr3950_common_create_hdet_device
(
  rfdevice_rxtx_common_class* common_device
);

#ifdef __cplusplus
}
#endif


/*! @} */

#endif /* WTR3950_COMMON_DEVICE_INTF_H */
