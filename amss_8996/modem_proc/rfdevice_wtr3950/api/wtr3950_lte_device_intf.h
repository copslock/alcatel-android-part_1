/*!
  @file wtr3950_lte_api.h

  @brief
  Exports prototypes for WTR3950 device interface maping functions.

  @addtogroup WTR3950_LTE
  @{
*/

/*============================================================================== 
   
Copyright (c) 2013, 2014 by QUALCOMM Technologies, Inc.  All Rights Reserved.
Copyright (c) 2015, 2016 by QUALCOMM Technologies, Inc.  All Rights Reserved.

Qualcomm Technologies Proprietary and Confidential

  Export of this technology or software is regulated by the U.S. 
  Government. Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of QUALCOMM Technologies Incorporated.
   
==============================================================================*/ 

/*==============================================================================
   
                        EDIT HISTORY FOR MODULE 
   
This section contains comments describing changes made to the module. 
Notice that changes are listed in reverse chronological order. 
 
$DateTime: 2016/03/28 23:07:39 $ $Author: mplcsds1 $
$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/rfdevice_wtr3950/api/wtr3950_lte_device_intf.h#1 $ 
   
when       who     what, where, why 
--------   ---     ------------------------------------------------------------- 
01/01/15   tks     Initial version.
==============================================================================*/ 

#ifndef WTR3950_LTE_API_H
#define WTR3950_LTE_API_H

#ifdef __cplusplus
extern "C" {
#endif

/*============================================================================== 
 
                                 INCLUDE FILES
 
==============================================================================*/

#include "rfcom.h"
#include "rfdevice_lte_interface.h"
#include "rfdevice_cmn_intf.h"

#include "rfdevice_rxtx_common_class.h"


/*============================================================================== 
 
                                DATA DEFINITIONS
 
==============================================================================*/
/*! Max number of devices supported by WTR3950 */
#define WTR3950_MAX_LTE_DEVICES 2


/*============================================================================== 
 
                             FUNCTION DECLARATIONS
 
==============================================================================*/

boolean
wtr3950_lte_create_device
(
  rfdevice_rxtx_common_class* common
);


#ifdef __cplusplus
}
#endif

/*! @} */

#endif  /* wtr3950_API_H */
