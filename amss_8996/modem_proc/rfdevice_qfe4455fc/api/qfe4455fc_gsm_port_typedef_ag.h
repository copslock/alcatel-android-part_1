#ifndef QFE4455FC_GSM_TYPEDEF_AG_H
#define QFE4455FC_GSM_TYPEDEF_AG_H


#include "comdef.h"

/*----------------------------------------------------------------------------*/
/*!
  It defines the QFE4455FC_GSM pa_port enum.
*/
typedef enum 
{
  QFE4455FC_GSM_GSM_BAND850_PORT_ANT, 
  QFE4455FC_GSM_GSM_BAND900_PORT_ANT, 
  QFE4455FC_GSM_PORT_NUM, 
  QFE4455FC_GSM_PORT_INVALID, 
} qfe4455fc_gsm_pa_port_data_type;


#endif