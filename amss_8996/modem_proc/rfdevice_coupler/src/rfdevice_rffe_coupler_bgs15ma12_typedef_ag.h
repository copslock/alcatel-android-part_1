#ifndef RFDEVICE_RFFE_COUPLER_BGS15MA12_TYPEDEF_AG_H
#define RFDEVICE_RFFE_COUPLER_BGS15MA12_TYPEDEF_AG_H
/*
WARNING: This BGS15MA12 driver is auto-generated.

Generated using: coupler_autogen.pl 
Generated from:
	File: rfdevice_coupler.xlsm 
	Released: 8/18/2015
	Author: BGS15MA12: update product revision; REV setting update for MB/HB; Update Defualt USID.
	Revision: v1.06
	Change Note: hsinih
	Tab: cpl_bgs15ma12
*/

/*=============================================================================

          RF DEVICE  A U T O G E N    F I L E

GENERAL DESCRIPTION
  This file is auto-generated and it captures the configuration of the RF Card.

  Copyright (c) 2015 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies, Inc.

$Header:
=============================================================================*/

/*=============================================================================
                           INCLUDE FILES
=============================================================================*/

#include "comdef.h"
#ifdef __cplusplus
extern "C" {
#endif   

/* Device Identifiers */
#define BGS15MA12_COUPLER_MANUFACTURER_ID 0x011A
#define BGS15MA12_COUPLER_PRODUCT_ID 0xD0

#define BGS15MA12_COUPLER_CHIP_REV 
#define BGS15MA12_COUPLER_CHIP_REV_ADDR 
#define BGS15MA12_COUPLER_PRODUCT_REVISION 0
#define BGS15MA12_COUPLER_INSTANCE 
#define RFDEVICE_COUPLER_REG_INVALID -1

/* Port specific settings */
#define BGS15MA12_COUPLER_FWD_SCRIPT_SIZE 1
#define BGS15MA12_COUPLER_REV_SCRIPT_SIZE 1
#define BGS15MA12_COUPLER_GAIN_0_SCRIPT_SIZE 0
#define BGS15MA12_COUPLER_GAIN_1_SCRIPT_SIZE 1
#define BGS15MA12_COUPLER_GAIN_2_SCRIPT_SIZE 1
#define BGS15MA12_COUPLER_GAIN_3_SCRIPT_SIZE 1
#define BGS15MA12_COUPLER_FILTER_SCRIPT_SIZE 0
#define BGS15MA12_COUPLER_OUTPUT_TYPE_0_SCRIPT_SIZE 0
#define BGS15MA12_COUPLER_OUTPUT_TYPE_1_SCRIPT_SIZE 1

/* Common settings */
#define BGS15MA12_COUPLER_TRIGGER_SCRIPT_SIZE 1
#define BGS15MA12_COUPLER_INIT_SCRIPT_SIZE 0
#define BGS15MA12_COUPLER_WAKEUP_SCRIPT_SIZE 0
#define BGS15MA12_COUPLER_SLEEP_SCRIPT_SIZE 0

typedef enum
{
  BGS15MA12_COUPLER_PORT_0,
  BGS15MA12_COUPLER_PORT_1,
  BGS15MA12_COUPLER_PORT_2,
  BGS15MA12_COUPLER_PORT_NUM,
  BGS15MA12_COUPLER_PORT_INVALID,
}bgs15ma12_coupler_port_enum_type;

#ifdef __cplusplus
}
#endif
#endif