/*!
    @file
    lte_ml1_qsh_ext.h
    
    @brief
    QSH frame work for live event/metrics and dump

    @detail
*/

/*===========================================================================

    Copyright (c) 2008-2015 Qualcomm Technologies Incorporated. All Rights Reserved

    Qualcomm Proprietary

    Export of this technology or software is regulated by the U.S. Government.
    Diversion contrary to U.S. law prohibited.

    All ideas, data and information contained in or disclosed by
    this document are confidential and proprietary information of
    Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
    By accepting this material the recipient agrees that this material
    and the information contained therein are held in confidence and in
    trust and will not be used, copied, reproduced in whole or in part,
    nor its contents revealed in any manner to others without the express
    written permission of Qualcomm Technologies Incorporated.

===========================================================================*/

/*===========================================================================

                                                EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/lte/api/lml1_qsh_ext.h#1 $

when         who     what, where, why
--------   ---     ----------------------------------------------------------
===========================================================================*/
#ifndef _LML1_QSH_EXT_H_
#define _LML1_QSH_EXT_H_

#include <qsh.h>
#include "lte_l1_ftm.h"
#include "lml1_qsh_mini_dump_ext.h"

/*****************************************************************************
 *
 * QSH version
 *
 ****************************************************************************/
#define LML1_QSH_MAJOR_VER 1
#define LML1_QSH_MINOR_VER 0

/*****************************************************************************
 *
 * QSH Metrics
 *
 ****************************************************************************/
 
#define LML1_QSH_ANT_NUM      2
 
typedef enum 
{
  CC_DISABLE = 0,
  CC_CONFIGURED = 1,
  CC_ENABLED = 2
} cc_status_e;
 
/*
 * Enum for metrics
 */
typedef enum 
{
  LML1_QSH_METRIC_RSRP,
  LML1_QSH_METRIC_RSSI,
  LML1_QSH_METRIC_SINR,
  LML1_QSH_METRIC_BLER,
  LML1_QSH_METRIC_MCS,
  LML1_QSH_METRIC_RACH_PWR,
  LML1_QSH_METRIC_NON_RACH_PWR,
  LML1_QSH_METRIC_GRANT,
  LML1_QSH_METRIC_EVENT,
  LML1_QSH_METRIC_RRC_MSG,
  LML1_QSH_METRIC_RSRQ,
  LML1_QSH_METRIC_ANT_RF_DATA,
  LML1_QSH_METRIC_MAX
} lml1_qsh_metric_e;

/* 
 * LTE ML1 runtime metrics
 */
typedef struct
{
  qsh_metric_hdr_s             hdr;
  int32                        rsrp_avg; /* Avg RSRP within sampling period */
} lml1_qsh_metric_rsrp_s;

typedef struct
{
  qsh_metric_hdr_s             hdr;
  int32                        rssi_avg; /* Avg RSSI within sampling period */
} lml1_qsh_metric_rssi_s;

typedef struct
{
  qsh_metric_hdr_s             hdr;
  int32                        sinr_avg; /* Avg RSSI within sampling period */
} lml1_qsh_metric_sinr_s;

typedef struct
{
  qsh_metric_hdr_s             hdr;
  uint32                       tb_count; /* Accumulated BLER within sampling period */
  uint32                       tb_err_count;
} lml1_qsh_metric_bler_s;

typedef struct
{
  qsh_metric_hdr_s             hdr;
  uint8                        ul_mcs; /* Avg UL MCS within sampling period */
} lml1_qsh_metric_mcs_s;

typedef struct
{
  qsh_metric_hdr_s             hdr;
  int8                         avg_rach_pwr; /* Avg Rach power in dbm sampling period */
} lml1_qsh_metric_rach_tx_power_s;

typedef struct
{
  qsh_metric_hdr_s             hdr;
  int8                         avg_non_rach_pwr; /* Avg Rach power in dbm sampling period */
} lml1_qsh_metric_non_rach_tx_power_s;

typedef struct
{
  qsh_metric_hdr_s             hdr;
  uint8                        ul_rb;  /* Avg #RBs in grant within sampling period */
} lml1_qsh_metric_grant_s;

typedef struct
{
  qsh_metric_hdr_s             hdr;
  int32                        rsrq_avg; /* Avg RSRQ within sampling period */
} lml1_qsh_metric_rsrq_s;

/* 
 * LTE ML1 runtime events
 */
typedef struct 
{
  qsh_metric_hdr_s    hdr;
  uint32              earfcn;         /* EARFCN/ UARFCN */
  uint16              pci;            /* PCI/PSC of Serving Cell */
  uint32              bw;             /* Total Bandwidth for LTE */
  int16               tdd_config;     /* LTE UL/DL Configuration */
  cc_status_e         cc_status;      /* Current Carrier Agg Status */
} lml1_qsh_metric_event_s;

/*
 * LTE ML1 RRC message
 */
typedef struct
{
  uint8      status;
} lml1_qsh_metric_cphy_start_cnf_s;


typedef struct
{
  uint8      status;
} lml1_qsh_metric_cphy_stop_cnf_s;

typedef struct
{
  uint8            status;
  uint8            suspend_cause;
}lml1_qsh_metric_cphy_suspend_cnf_s;

typedef struct
{
  uint8            status;
  uint8            cause;
}lml1_qsh_metric_cphy_resume_cnf_s;

typedef struct
{
  uint8      status;
  uint32     earfcn;
  uint16     phy_cell_id;
} lml1_qsh_metric_cphy_cell_select_cnf_s;

typedef struct
{
  uint8      status;
  uint16     common_cfg_validity;
} lml1_qsh_metric_cphy_common_cfg_cnf_s;

typedef struct
{
  uint8                       status;
  boolean                     is_acq_needed;
} lml1_qsh_metric_cphy_con_release_cnf_s;

typedef struct
{
  uint8      status;
  uint32     dedicated_cfg_validity;
} lml1_qsh_metric_cphy_dedicated_cfg_cnf_s;

typedef struct
{
  uint8      status;
  uint16 com_mob_parm_validity;
  uint32 dedicated_parm_validity;
} lml1_qsh_metric_cphy_handover_cnf_s;

typedef struct
{
  uint8 status;
} lml1_qsh_metric_cphy_idle_meas_cfg_cnf_s;

typedef struct
{
  uint8 status;
} lml1_qsh_metric_cphy_conn_meas_cfg_cnf_s;

typedef struct
{
  boolean result;
  uint8 scell_id;
} lml1_qsh_metric_cphy_start_rach_cnf_s;

typedef struct
{
  uint8 trans_id;
  uint8 status;
  uint32 earfcn;
  boolean cell_barred;
} lml1_qsh_metric_cphy_acq_cnf_s;

typedef struct
{
  uint8 rlf_reason;
} lml1_qsh_metric_cphy_rl_failure_ind_s;



typedef struct 
{
   qsh_metric_hdr_s            hdr;
   /*! UMID*/
   uint32 umid;
   /*! UMID Info*/
   union
   {
     lml1_qsh_metric_cphy_start_cnf_s       cphy_start_cnf;       /* LTE_CPHY_START_CNF */
     lml1_qsh_metric_cphy_stop_cnf_s        cphy_stop_cnf;        /* LTE_CPHY_STOP_CNF */
     lml1_qsh_metric_cphy_suspend_cnf_s     cphy_suspend_cnf;     /* LTE_CPHY_SUSPEND_CNF */
     lml1_qsh_metric_cphy_resume_cnf_s      cphy_resume_cnf;    /* LTE_CPHY_RESUME_CNF */
     lml1_qsh_metric_cphy_cell_select_cnf_s cphy_cell_select_cnf;  /* LTE_CPHY_CELL_SELECT_CNF */
     lml1_qsh_metric_cphy_common_cfg_cnf_s  cphy_common_cfg_cnf;   /* LTE_CPHY_COMMON_CFG_CNF */
     lml1_qsh_metric_cphy_con_release_cnf_s cphy_con_release_cnf;  /* LTE_CPHY_CON_RELEASE_CNF */
     lml1_qsh_metric_cphy_dedicated_cfg_cnf_s cphy_dedicated_cfg_cnf;  /* LTE_CPHY_DEDICATED_CFG_CNF */
     lml1_qsh_metric_cphy_handover_cnf_s    cphy_handover_cnf;         /* LTE_CPHY_HANDOVER_CNF */
     lml1_qsh_metric_cphy_idle_meas_cfg_cnf_s cphy_idle_meas_cfg_cnf;  /* LTE_CPHY_IDLE_MEAS_CFG_CNF */
     lml1_qsh_metric_cphy_conn_meas_cfg_cnf_s cphy_conn_meas_cfg_cnf;  /* LTE_CPHY_CONN_MEAS_CFG_CNF */
     lml1_qsh_metric_cphy_start_rach_cnf_s  cphy_start_rach_cnf;       /* LTE_CPHY_START_RACH_CNF */
     lml1_qsh_metric_cphy_acq_cnf_s         cphy_acq_cnf;              /* LTE_CPHY_ACQ_CNF */
     lml1_qsh_metric_cphy_rl_failure_ind_s  cphy_rl_failure_ind;       /* LTE_CPHY_RL_FAILURE_IND */
   } umid_info_u;   
} lml1_qsh_metrics_rrc_msg_s; 

/*
 * Antenna rf data
 */
typedef struct
{
  qsh_metric_hdr_s             hdr;
  int32                        rssi[LML1_QSH_ANT_NUM];
  int32                        rsrp[LML1_QSH_ANT_NUM];
  int32                        rsrq[LML1_QSH_ANT_NUM];
  int32                        sinr[LML1_QSH_ANT_NUM];
}lml1_qsh_metric_ant_rf_data_s;

/*****************************************************************************
 *
 * QSH Dump
 *
 ****************************************************************************/
/*
 * Enum for dump
 */
typedef enum 
{
  LML1_QSH_DUMP_TAG_MGR,
  LML1_QSH_DUMP_TAG_DLM,
  LML1_QSH_DUMP_TAG_ULM,
  LML1_QSH_DUMP_TAG_GM,
  LML1_QSH_DUMP_TAG_SM_CONN,
  LML1_QSH_DUMP_TAG_SM_IDLE,
  LML1_QSH_DUMP_TAG_GAPMGR,
  LML1_QSH_DUMP_TAG_RFMGR,
  LML1_QSH_DUMP_TAG_IRAT,
  LML1_QSH_DUMP_TAG_SCHDLR,
  LML1_QSH_DUMP_TAG_MSG,
  LML1_QSH_DUMP_TAG_SLEEPMGR,
  LML1_QSH_DUMP_TAG_MCLK,
  LML1_QSH_DUMP_TAG_MAX
} lml1_qsh_dump_tag_e;

/*
 * LTE ML1 minidump (mini set)
 */
typedef PACK(struct)
{
  qsh_dump_tag_hdr_s hdr;
  lte_ml1_qsh_dump_tag_mini_stm_state_machine_t MGR_SM;
  lte_ml1_qsh_dump_tag_mini_lte_ml1_manager_data_s manager_data;
  uint8                                  ta_category_index; 
}lml1_qsh_dump_tag_mgr_s;

typedef PACK(struct)
{
  qsh_dump_tag_hdr_s hdr;
  lte_ml1_qsh_dump_tag_mini_stm_state_machine_t   ULM_SM;
  lte_ml1_qsh_dump_tag_mini_lte_ml1_ulm_s         ml1_ulm;
}lml1_qsh_dump_tag_ulm_s;

typedef PACK(struct)
{
  qsh_dump_tag_hdr_s hdr;
  lte_ml1_qsh_dump_tag_mini_stm_state_machine_t   GM_SM;
  lte_ml1_qsh_dump_tag_mini_lte_ml1_gm_data_s     gm_db;
}lml1_qsh_dump_tag_gm_s;

typedef PACK(struct)
{
  qsh_dump_tag_hdr_s hdr;
  lte_ml1_qsh_dump_tag_mini_stm_state_machine_t   GAPMGR_STM;
  lte_ml1_qsh_dump_tag_mini_lte_ml1_gapmgr_data_s gapmgr_data;
} lml1_qsh_dump_tag_gapmgr_s;

typedef PACK(struct)
{
  qsh_dump_tag_hdr_s hdr;
  lte_ml1_qsh_dump_tag_mini_stm_state_machine_t RFMGR_STM;
  lte_ml1_qsh_dump_tag_mini_lte_ml1_rfmgr_trm_s rfmgr_trm;
}lml1_qsh_dump_tag_rfmgr_s;

typedef PACK(struct)
{
  qsh_dump_tag_hdr_s hdr;
  lte_ml1_qsh_dump_tag_mini_stm_state_machine_t SCHDLR2_STM;
  lte_ml1_qsh_dump_tag_mini_lte_ml1_schdlr_mode_data_s schdlr_mode;
  lte_ml1_qsh_dump_tag_mini_lte_ml1_schdlr_systime_info_s schdlr_systime;
  lte_ml1_qsh_dump_tag_mini_lte_ml1_schdlr_log_buffer_s  schdlr_log;
}lml1_qsh_dump_tag_schdlr_s;

typedef PACK(struct)
{
  qsh_dump_tag_hdr_s hdr;
  lte_ml1_qsh_dump_tag_mini_lte_ml1_debug_log_s mgr_gm_msg_log;
}lml1_qsh_dump_tag_msg_s;

typedef PACK(struct)
{
  qsh_dump_tag_hdr_s hdr;
  lte_ml1_qsh_dump_tag_mini_stm_state_machine_t DLM_SM;
  lte_ml1_qsh_dump_tag_mini_lte_ml1_dlm_rx_cfg_proc_status_s rx_cfg_proc_status;
  lte_ml1_qsh_dump_tag_mini_lte_ml1_dlm_ca_data_s            ca_data;
} lml1_qsh_dump_tag_dlm_s;

typedef PACK(struct)
{
  qsh_dump_tag_hdr_s hdr;
  lte_ml1_qsh_dump_tag_mini_stm_state_machine_t SM_CON_STM;
  lte_ml1_qsh_dump_tag_mini_stm_state_machine_t SM_CONN_TL_MGR_STM;
  lte_ml1_qsh_dump_tag_mini_lte_ml1_sm_conn_tl_mgr_stm_s tl_mgr_stm;
  lte_ml1_qsh_dump_tag_mini_lte_ml1_sm_conn_pbch_mgr_stm_s pbch_mgr_stm;
  lte_ml1_qsh_dump_tag_mini_lte_ml1_sm_conn_meas_inter_freq_meas_s  inter_freq_meas;
  lte_ml1_qsh_dump_tag_mini_lte_ml1_sm_conn_inter_freq_stm_info_s    inter_freq_stm;
} lml1_qsh_dump_tag_sm_conn_s;

typedef PACK(struct)
{
  qsh_dump_tag_hdr_s hdr;
  int32       curr_op_mode;
  union 
  {
    struct 
    {
      lte_ml1_qsh_dump_tag_mini_stm_state_machine_t  ACQ_STM;
      int32                                config_request;
      uint32                               last_acq_earfcn;
      int32                                acq_rx_cfg_req_type;
      int32                                prev_state;
      lte_ml1_qsh_dump_tag_mini_lte_ml1_sm_acq_pbch_info_s pbch_info;
      lte_ml1_qsh_dump_tag_mini_lte_ml1_sm_acq_s     sm_acq_data;
    } acq_data;
    struct
    {
      lte_ml1_qsh_dump_tag_mini_stm_state_machine_t  IDLE_STM;    
      lte_ml1_qsh_dump_tag_mini_lte_ml1_sm_idle_s    sm_idle;
      lte_ml1_qsh_dump_tag_mini_lte_ml1_sm_idle_lte_search_meas_pbch_info_s meas_pbch_info;
      uint16                                         paging_cycle;
      lte_ml1_qsh_dump_tag_mini_lte_ml1_mdb_cell_struct_s serving_cell;      
    } idle_data;
  } stm;
} lml1_qsh_dump_tag_sm_idle_s;

typedef PACK(struct)
{
  qsh_dump_tag_hdr_s hdr;
  lte_ml1_qsh_dump_tag_mini_lte_ml1_sm_irat_meas_s    irat_meas_s;
} lml1_qsh_dump_tag_irat_s;

typedef PACK(struct)
{
  qsh_dump_tag_hdr_s hdr;
  lte_ml1_qsh_dump_tag_mini_lte_ml1_sleepmgr_t        sleepmgr_data;
  lte_ml1_qsh_dump_tag_mini_stm_state_machine_t       SLEEPMGR_STM;
} lml1_qsh_dump_tag_sleepmgr_s;

typedef PACK(struct)
{
  qsh_dump_tag_hdr_s hdr;
  lte_ml1_qsh_dump_tag_mini_lte_ml1_mcvs_ctrl_s     mcvs;
  lte_ml1_qsh_dump_tag_mini_lte_ml1_mcpm_s          mcpm;
} lml1_qsh_dump_tag_mclk_s;
#endif /* _LML1_QSH_EXT_H_ */
