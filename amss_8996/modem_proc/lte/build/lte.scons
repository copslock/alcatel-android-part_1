#===============================================================================
#
# TCXOMGR Scons
#
# GENERAL DESCRIPTION
#    build script
#
# Copyright (c) 2010 by Qualcomm Technologies, Incorporated.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#
#-------------------------------------------------------------------------------
#
#  $Header: //source/qcom/qct/modem/lte/build/lte.scons#1 $
#
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
# 02/27/15    xl     Removed uselss code
# 02/25/15    xl     Clean up SCONSCOP violations
# 04/11/14    mg     CR647594: QSH Phase 1
# 09/21/10    ag     Support for logging msg packets.
# 09/21/10    ag     Added BUSES, UIM and HDR paths.
# 09/21/10    ag     Initial version.
#===============================================================================
Import('env')

# Enable warnings -> errors for all, except LLVM toolchain (6.x.x) during migration
# Copy the CFLAGS list to a new environment for us
# (the list is a reference when cloned, so use deepcopy)
import copy
orig_env = env
env = env.Clone()
env['CFLAGS'] = copy.deepcopy(orig_env['CFLAGS'])

# Remove any "disables" from top-level
if env['CFLAGS'].count('-Wno-low') > 0:
  env['CFLAGS'].remove('-Wno-low')

if env['CFLAGS'].count('-Wno-medium') > 0:
  env['CFLAGS'].remove('-Wno-medium')

if env['CFLAGS'].count('-Wno-high') > 0:
  env['CFLAGS'].remove('-Wno-high')

if env['CFLAGS'].count('-Wno-error') > 0:
  env['CFLAGS'].remove('-Wno-error')
# Enable -Werror
env.Append(HEXAGONCC_WARN = ' -Werror ')
env.Append(HEXAGONCXX_WARN = ' -Werror ')

if env['PRODUCT_LINE'].startswith("MPSS.AT"):
  env.Append(HEXAGON_WARN = ' -Wno-error-high -Wno-error-medium -Wno-error-low ')   
if env['PRODUCT_LINE'].startswith("MPSS.AT"):
  env.Append(CFLAGS = "-DFEATURE_LTE_ATLAS_MODEM")

env.Append(CFLAGS = "-DFEATURE_LTE_ELS_ENABLED")
env.Append(CFLAGS = "-DFEATURE_LTE_UDC_ENABLED")  

# Set -Wdeclaration-after-statement to disallow C99 style variable declarations
# Required for QTF development
env.Append(HEXAGONCC_WARN = ' -Wdeclaration-after-statement ')

if env['PRODUCT_LINE'] == 'MPSS.BO.2.1':
    env.Append(CPPDEFINES = ["FEATURE_LTE_CDRX_IMS_VOLTE_OPT"])

env.Append(CPPDEFINES = ["FEATURE_DEVICE_HOP"])
                                
if env['PRODUCT_LINE'].startswith('MPSS.TH.2'):
    env.Append(CPPDEFINES = ["FEATURE_LTE_ML1_HSIC"])

env.Replace(LTE_ROOT = '${INC_ROOT}/lte')

# Load cleanpack script:
import os
if os.path.exists(env.subst('${LTE_ROOT}/pack/lte_cleanpack.py')):
   env.LoadToolScript('${LTE_ROOT}/pack/lte_cleanpack.py')

    
env.RequirePublicApi([
        'KERNEL',
        ],
        area='CORE')

env.RequirePublicApi([
        'LTE',
        ],
        area='FW')

env.RequirePublicApi([
        'FW_LTE',
        ],
        area='FW_LTE')
        
env.RequirePublicApi([
        'LTE',
        ],
        area='LTE')

env.RequirePublicApi([
        'MCS',
        ],
        area='MCS')

env.RequirePublicApi([
        'COMMON',
        ],
        area='RFA')

env.RequirePublicApi([
        'DAL',
        ],
        area='CORE')

env.RequirePublicApi([
        'MEAS',
        ],
        area='RFA')

env.RequirePublicApi([
        'PUBLIC',
        ],
        area='MMCP')

env.RequirePublicApi([
        'CDMA',
        ],
        area='RFA')

env.RequirePublicApi([
        'MMCP',
        ],
        area='MMCP')

env.RequirePublicApi([
        'SERVICES',
        'SYSTEMDRIVERS',
        ],
        area='CORE')

env.RequirePublicApi([
        'GSM',
        ],
        area='RFA')

env.RequirePublicApi([
        'RF',
        ],
        area='FW')

env.RequirePublicApi([
        'FW_CCS',
        ],
        area='FW_CCS')

env.RequirePublicApi([
        'FW_COMMON',
        ],
        area='FW_COMMON')

env.RequirePublicApi([
        'MEMORY',
        'MPROC',
        'POWER',
        ],
        area='CORE')

env.RequirePublicApi([
        'WCDMA',
        ],
        area='RFA')

env.RequirePublicApi([
        'GERAN',
        ],
        area='FW')

env.RequirePublicApi([
        'FW_GERAN',
        ],
        area='FW_GERAN')

env.RequirePublicApi([
        'DEBUGTOOLS',
        ],
        area='CORE')

env.RequirePublicApi([
        'GERAN',
        ],
        area='GERAN')

env.RequirePublicApi([
        'LTE',
        ],
        area='RFA')

env.RequirePublicApi([
        'OSYS',
        'RTXSRC',
        'RTPERSRC',
        ],
        area='UTILS')

env.RequirePublicApi([
        'PUBLIC',
        ],
        area='ONEX')

env.RequirePublicApi([
        'GNSS',
        ],
        area='RFA')

env.RequirePublicApi([
        'WCDMA',
        ],
        area='WCDMA')

env.RequirePublicApi([
        'STORAGE',
        ],
        area='CORE')

env.RequirePublicApi([
        'HDR',
        ],
        area='HDR')

env.RequirePublicApi([
        'LM',
        ],
        area='RFA')

env.RequirePublicApi([
        'TDSCDMA',
        ],
        area='TDSCDMA')

env.RequirePublicApi([
        'MPOWER',
        ],
        area='MPOWER')

env.RequirePublicApi([
        'A2',
        ],
        area='UTILS')

env.RequirePublicApi([
        'ONEX',
        ],
        area='ONEX')

env.RequirePublicApi([
        'CFM',
        'COMMON',
        'QSH',
        ],
        area='UTILS')

env.RequirePublicApi([
        'PUBLIC',
        ],
        area='UIM')

env.RequirePublicApi([
        'PUBLIC',
        ],
        area='HDR')

env.RequirePublicApi([
        'PUBLIC',
        ],
        area='DATAMODEM')

env.RequirePublicApi([
        'PUBLIC',
        ],
        area='UTILS')

env.RequirePublicApi([
        'WCDMA',
        ],
        area='FW')

env.RequirePublicApi([
        'FW_WCDMA',
        ],
        area='FW_WCDMA')

env.RequirePublicApi([
        'DATAMODEM',
        ],
        area='DATAMODEM')

env.RequirePublicApi([
        'SECUREMSM',
        'WIREDCONNECTIVITY',
        ],
        area='CORE')

env.RequirePublicApi([
        'C2K',
        ],
        area='FW')

env.RequirePublicApi([
        'FW_C2K',
        ],
        area='FW_C2K')
        
env.RequirePublicApi([
        'TDSCDMA',
        ],
        area='RFA')

env.RequirePublicApi([
        'GPS',
        ],
        area='GPS')
        
env.RequirePublicApi([
        'MCFG',
        ],
        area='MCFG')

env.RequirePublicApi([
        'UIM',
        ],
        area='UIM')
        
env.RequirePublicApi([
        'MVS',
        ],
        area='AVS')

env.RequirePublicApi([
        'RFLM',
        ],
        area='RFLM')
env.RequireRestrictedApi(['VIOLATIONS'])


env.PublishProtectedApi('LTE', [
        '${LTE_ROOT}/cust/inc',
        '${LTE_ROOT}/variation/inc',
        '${LTE_ROOT}/RRC/src',
        '${LTE_ROOT}/cust/inc',
        '${LTE_ROOT}/common/inc',
        '${LTE_ROOT}/variation/inc',
        '${LTE_ROOT}/L2/rlc/src',
        '${LTE_ROOT}/L2/common/inc',
        '${LTE_ROOT}/L2/mac/src',
        '${LTE_ROOT}/L2/rlc/inc',
        '${LTE_ROOT}/L2/pdcp/src',
        '${LTE_ROOT}/L2/mac/inc',
        '${LTE_ROOT}/L2/pdcp/inc',
        '${LTE_ROOT}/PLT/src',
        '${LTE_ROOT}/PLT/inc',
        '${LTE_ROOT}/tlb/src',
        '${LTE_ROOT}/RRC/inc',
        ])

env.RequireProtectedApi(['LTE'])


if 'USES_MSGR' in env:
   env.AddUMID('${BUILDPATH}/lte.umid', ['${LTE_ROOT}/api/lte_cphy_ftm_msg.h',
                                         '${LTE_ROOT}/api/lte_cphy_irat_meas_msg.h',
                                         '${LTE_ROOT}/api/lte_cphy_msg.h',
                                         '${LTE_ROOT}/api/lte_cphy_rssi_msg.h',
                                         '${LTE_ROOT}/api/lte_ind_msg.h',
                                         '${LTE_ROOT}/api/lte_mac_msg.h',
                                         '${LTE_ROOT}/api/lte_pdcp_msg.h',
                                         '${LTE_ROOT}/api/lte_rlc_msg.h',
                                         '${LTE_ROOT}/api/lte_rrc_ext_msg.h',
                                         '${LTE_ROOT}/api/lte_rrc_irat_msg.h',
                                         '${LTE_ROOT}/api/lte_tlb_msg.h',
                                         '${LTE_ROOT}/api/lte_cxm_msg.h',
                                     ])

#----------------------------------------------------.---------------------------
# Continue loading software units
#-------------------------------------------------------------------------------


env.RequirePublicApi([
               'DAL',
               'DEBUGTOOLS',
               'MPROC',
               'SERVICES',
               'SYSTEMDRIVERS',
               'KERNEL',          # needs to be last
               ], area='core')

env.RequirePublicApi([
               'OSYS_LTE',
               'OSYS_RTPERSRC_LTE',
               'OSYS_RTSRC_LTE',
               'OSYS_RTXSRC_LTE',
               'QSH_API',
               ],area='UTILS')


if 'LTE_ENABLE_LLVM_STATIC_ANALYZER' in env:
  env.Replace(LTE_SA_DIR = '${INC_ROOT}/lte/sa/')
  env.Append(CFLAGS = '--compile-and-analyze ${LTE_SA_DIR}')

#-------------------------------------------------------------------------------
# Register All Dynamic Task info (name/priority/stack size) with RCINIT.  
#
# Format:
# ['RCINIT_GROUP_7', <TASK_NAME>, 'RCINIT_TASK_POSIX', 'RCINIT_NULL', <STACK_SIZE>,<PRIORITY_ORDER>]
# <PRIORITY_ORDER> - must be an alias defined in rcinit_task_prio.csv
#-------------------------------------------------------------------------------
if 'USES_RCINIT' in env:

  RCINIT_IMG = ['MODEM_MODEM', 'MOB_LTE']

  RCINIT_INIT_TABLE = [
   # L2
   ['RCINIT_GROUP_7','LTE_MAC_CTRL', 'RCINIT_TASK_POSIX', 'RCINIT_NULL', '8192', 'LTEMACCTRL_PRI_ORDER'],
   ['RCINIT_GROUP_7','LTE_MAC_DL', 'RCINIT_TASK_POSIX', 'RCINIT_NULL', '8192', 'LTEMACDL_PRI_ORDER'],
   ['RCINIT_GROUP_7','LTE_MAC_UL', 'RCINIT_TASK_POSIX', 'RCINIT_NULL', '8192', 'LTEMACUL_PRI_ORDER'],
   ['RCINIT_GROUP_7','LTE_PDCP_DL', 'RCINIT_TASK_POSIX', 'RCINIT_NULL', '16384', 'LTEPDCPDL_PRI_ORDER'],
   ['RCINIT_GROUP_7','LTE_PDCPOFFLOAD', 'RCINIT_TASK_POSIX', 'RCINIT_NULL', '8192', 'LTEPDCPOFFLOAD_PRI_ORDER'],
   ['RCINIT_GROUP_7','LTE_RLC_UL', 'RCINIT_TASK_POSIX', 'RCINIT_NULL', '16384', 'LTERLCUL_PRI_ORDER'],
   ['RCINIT_GROUP_7','LTE_RLC_DL', 'RCINIT_TASK_POSIX', 'RCINIT_NULL', '8192', 'LTERLCDL_PRI_ORDER'],
   # ML1
   ['RCINIT_GROUP_7','ML1_GM', 'RCINIT_TASK_POSIX', 'RCINIT_NULL', '8192', 'ML1GM_PRI_ORDER'],
   ['RCINIT_GROUP_7','ML1_OFFLOAD', 'RCINIT_TASK_POSIX', 'RCINIT_NULL', '4096', 'ML1OFFLOAD_PRI_ORDER'],
   ['RCINIT_GROUP_7','ML1_MGR', 'RCINIT_TASK_POSIX', 'RCINIT_NULL', '8192', 'ML1MGR_PRI_ORDER'],
   ['RCINIT_GROUP_7','ML1_SM_FSCAN', 'RCINIT_TASK_POSIX', 'RCINIT_NULL', '5632', 'ML1MGR_PRI_ORDER'],
   # PLT
   ['RCINIT_GROUP_7','PLT', 'RCINIT_TASK_POSIX', 'RCINIT_NULL', '4096', 'PLT_PRI_ORDER'],
   # RRC
   ['RCINIT_GROUP_7','LTE_RRC', 'RCINIT_TASK_POSIX', 'RCINIT_NULL', '32768', 'LTERRC_PRI_ORDER'],
   # TLB
   ['RCINIT_GROUP_7','LTE_TLB_CTRL', 'RCINIT_TASK_POSIX', 'RCINIT_NULL', '8192', 'LTETLBCTRL_PRI_ORDER'],
  ]

  for row in RCINIT_INIT_TABLE:
    env.AddRCInitTask(
     RCINIT_IMG,
     {
      'sequence_group'             : row[0],                  
      'thread_name'                : row[1],   
      'thread_type'                : row[2],                     
      'thread_entry'               : row[3],                   
      'stack_size_bytes'           : row[4],
      'priority_amss_order'        : row[5],
     })

#------------------------------------------------------------------------------

  
if 'USES_LTE' in env:
  env.LoadSoftwareUnits()
