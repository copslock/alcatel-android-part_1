#!/usr/bin/env python
'''
===============================================================================

  GENERAL DESCRIPTION:

     MPSS Top-Level Build Script for building a single variant.

  USAGE:

     python build_variant.py <variant_cluster> <options> <QC-SCons Options>

        <variant_cluster(s)>:

            Zero or more variant clusters may be specified to indicate what is
            to be built.  A variant cluster consists of a tri-mer of information
            as follows:

                  <chip_id>.<variant>.<purpose_name>

            The chip_id, variant and purpose name values will used to index into
            the build_cfg.xml file to find parameters for the build.

            The namespace for these values is mutually exclusive, so that you 
            can specify them in any order without confusion of meaning.  This 
            also means that you could leave any of them out, and the default 
            value will be used.

            The default value will be whatever is listed first in the 
            build_cfg.xml file.

        <options>:

            -h, --help:

                Print this message, and then call QC-SCons with this parameter
                so that it can print its help message.

            --strip

                Add USES_NO_STRIP_NO_ODM and USES_NO_DEBUG to the list of USES 
                flags.

            image=<image>

                Used to pass in images to QC-SCons.  Default images are read 
                from build_cfg.xml.  This parameter is only needed when 
                specifying images other than the default.

        <QC-SCons Options>:

            All other command line parameters are passed unchanged to QC-SCons

-------------------------------------------------------------------------------

  Copyright (c) 2009-2012 by QUALCOMM, Incorporated.
  All Rights Reserved.
  QUALCOMM Proprietary/GTDR

-------------------------------------------------------------------------------

  $Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/build/ms/build_variant.py#1 $
  $DateTime: 2016/03/28 23:03:59 $
  $Change: 10156249 $

===============================================================================
'''

#================================
# Import Python Libraries
#================================
import sys
import os
import shutil
import time
from xml.etree import ElementTree as et
from glob import glob
from copy import copy

build_root         = os.path.realpath(os.path.join(os.path.dirname(__file__), "../.."))
targ_root          = os.path.realpath(os.path.join(os.path.dirname(__file__), "../../.."))
tools_root         = os.path.join(build_root, "tools")
qcscons_root       = os.path.join(tools_root, "build/scons")
qcscons_build_dir  = os.path.join(qcscons_root, "build")
qcom_root_dir = os.path.realpath(os.path.join(os.path.dirname(__file__), ".."))

# Allow this script to be called from the OS command shell,
# or from another Python script.
if __name__ == "__main__":

    log = None
    try:

        #================================
        # Initialize logging
        #================================

        log_file_name = 'build'
        log_to_stdout = True
        log_resume = False
        params = sys.argv[1:]
        for arg in sys.argv[1:]:
            if arg.lower().startswith('--log_name'):
                log_file_name = arg.rsplit('=', 1)[1]
                params.remove(arg)
            elif arg.lower() == '--no_stdout':
                log_to_stdout = False
                params.remove(arg)
            elif arg.lower() == '--restarted':
                log_resume = True
                params.remove(arg)

        sys.path.append(qcscons_build_dir)
        sys.path.append(qcom_root_dir)

        import logger

        log = logger.Logger(log_file_name, 
                           log_to_stdout = log_to_stdout, 
                           resume=log_resume)

        # Log helpful debug info
        log.log("Running $Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/build/ms/build_variant.py#1 $")
        log.log("Start directory is: " + os.getcwd())
        log.log("Python Version is: " + sys.version)
        log.log("Platform is: " + sys.platform)

        import qcom.apps.build_qc.build_cfg as build_cfg

        # Validate the environment against our build_cfg.
        # Restart after setting the PATH to the correct version of Python, 
        # if needed.
        build_cfg.setenv_and_restart_if_needed (sys.argv, log)

        import qcom.apps.build_qc.build_qc as build_qc

        ret = build_qc.build(log, params)

        if ret != 0:
            sys.exit(ret)

    except:
        # Log any crashes to the log file
        if log:
            log.log_crash()
        raise
    finally:
        if log:
            log.close()

