
/*
WARNING: This file is auto-generated.

Generated using: asm_autogen.exe
Generated from:  v2.3.93 of RFDevice_ASM.xlsm
*/

/*=============================================================================

          R F C     A U T O G E N    F I L E

GENERAL DESCRIPTION
  This file is auto-generated and it captures the configuration of the RF Card.

Copyright (c) 2009, 2010, 2011, 2012 by Qualcomm Technologies, Inc.  All Rights Reserved.

$Header: //source/qcom/qct/modem/rfdevice/asm/main/latest/etc/asm_autogen.pl#6 n

=============================================================================*/

/*=============================================================================
                           INCLUDE FILES
=============================================================================*/
#include "comdef.h"

#include "rfdevice_asm_rfmd8115_data_ag.h"


#define RFDEVICE_ASM_rfmd8115_NUM_PORTS 3

#define RFDEVICE_ASM_rfmd8115_ASM_ON_NUM_REGS 1
static uint8 rfdevice_asm_rfmd8115_asm_on_regs[RFDEVICE_ASM_rfmd8115_ASM_ON_NUM_REGS] =  {0x00, };
static int16 rfdevice_asm_rfmd8115_asm_on_data[RFDEVICE_ASM_rfmd8115_NUM_PORTS][RFDEVICE_ASM_rfmd8115_ASM_ON_NUM_REGS] =
{
  { /* PORT NUM: 0 HB2_RX IN:PIN12 OUT:PIN14 B40(Rx)*/
    0x54, 
  },
  { /* PORT NUM: 1 HB3/4_RX IN:PIN11 OUT:PIN15 B38/B41(Rx)*/
    0x58, 
  },
  { /* PORT NUM: 2 HB3/4_RX IN:PIN10 OUT:PIN15 B38/B41(Rx)*/
    0x5C, 
  },
};


#define RFDEVICE_ASM_rfmd8115_ASM_OFF_NUM_REGS 1
static uint8 rfdevice_asm_rfmd8115_asm_off_regs[RFDEVICE_ASM_rfmd8115_ASM_OFF_NUM_REGS] =  {0x00, };
static int16 rfdevice_asm_rfmd8115_asm_off_data[RFDEVICE_ASM_rfmd8115_NUM_PORTS][RFDEVICE_ASM_rfmd8115_ASM_OFF_NUM_REGS] =
{
  { /* PORT NUM: 0 */
    0x00, 
  },
  { /* PORT NUM: 1 */
    0x00, 
  },
  { /* PORT NUM: 2 */
    0x00, 
  },
};


#define RFDEVICE_ASM_RFMD8115_ASM_TRIGGER_NUM_REGS 1
static uint8 rfdevice_asm_rfmd8115_asm_trigger_regs[RFDEVICE_ASM_RFMD8115_ASM_TRIGGER_NUM_REGS] =  {0x1C, };
static int16 rfdevice_asm_rfmd8115_asm_trigger_data[RFDEVICE_ASM_rfmd8115_NUM_PORTS][RFDEVICE_ASM_RFMD8115_ASM_TRIGGER_NUM_REGS] =
{
  { /* PORT NUM: 0 */
    0x07, 
  },
  { /* PORT NUM: 1 */
    0x07, 
  },
  { /* PORT NUM: 2 */
    0x07, 
  },
};

/* singleton instance ptr */
rfdevice_asm_data *rfdevice_asm_rfmd8115_data_ag::rfdevice_asm_rfmd8115_data_ptr = NULL;

rfdevice_asm_data * rfdevice_asm_rfmd8115_data_ag::get_instance()
{
  if (rfdevice_asm_rfmd8115_data_ptr == NULL)
  {
    rfdevice_asm_rfmd8115_data_ptr = (rfdevice_asm_data *)new rfdevice_asm_rfmd8115_data_ag();
  }
  return( (rfdevice_asm_data *)rfdevice_asm_rfmd8115_data_ptr);
}

//constructor
rfdevice_asm_rfmd8115_data_ag::rfdevice_asm_rfmd8115_data_ag()
  :rfdevice_asm_data()
{
}

//destructor
rfdevice_asm_rfmd8115_data_ag::~rfdevice_asm_rfmd8115_data_ag()
{
  rfdevice_asm_rfmd8115_data_ptr = NULL;
}

boolean rfdevice_asm_rfmd8115_data_ag::settings_data_get( rfdevice_asm_cfg_params_type *cfg, 
                                                          rfdevice_asm_reg_settings_type *settings)
{
  boolean ret_val = FALSE;

  if (NULL == settings || NULL == cfg)
  {
    return FALSE;
  }

  if (cfg->port >= RFDEVICE_ASM_rfmd8115_NUM_PORTS)
  {
    settings->addr = NULL;
    settings->data = NULL;
    settings->num_regs = 0;
    return FALSE;
  }

  if (cfg->req == RFDEVICE_ASM_ON_DATA)
  {
    settings->addr = &(rfdevice_asm_rfmd8115_asm_on_regs[0]);
    settings->data = &(rfdevice_asm_rfmd8115_asm_on_data[cfg->port][0]);
    settings->num_regs = RFDEVICE_ASM_rfmd8115_ASM_ON_NUM_REGS;
    ret_val = TRUE;
  }

  if (cfg->req == RFDEVICE_ASM_OFF_DATA)
  {
    settings->addr = &(rfdevice_asm_rfmd8115_asm_off_regs[0]);
    settings->data = &(rfdevice_asm_rfmd8115_asm_off_data[cfg->port][0]);
    settings->num_regs = RFDEVICE_ASM_rfmd8115_ASM_OFF_NUM_REGS;
    ret_val = TRUE;
  }

  if (cfg->req == RFDEVICE_ASM_TRIGGER_DATA)
  {
    settings->addr = &(rfdevice_asm_rfmd8115_asm_trigger_regs[0]);
    settings->data = &(rfdevice_asm_rfmd8115_asm_trigger_data[cfg->port][0]);
    settings->num_regs = RFDEVICE_ASM_RFMD8115_ASM_TRIGGER_NUM_REGS;
    ret_val = TRUE;
  }

  return ret_val;
}

boolean rfdevice_asm_rfmd8115_data_ag::sequence_data_get( rfdevice_asm_cfg_params_type *cfg, 
                                                          rfdevice_asm_cmd_seq_type *cmd_seq)
{
  boolean ret_val = FALSE;

  if (NULL == cmd_seq || NULL == cfg)
  {
    return FALSE;
  }

  if (RFDEVICE_ASM_RESET_DATA == cfg->req)
  {
    cmd_seq->cmds = NULL;
    cmd_seq->num_cmds = 0;
    ret_val = TRUE;
  }
  else
  {
    cmd_seq->cmds = NULL;
    cmd_seq->num_cmds = 0;
  }

  return ret_val;
}

boolean rfdevice_asm_rfmd8115_data_ag::device_info_get( rfdevice_asm_info_type *asm_info )
{
  boolean ret_val = FALSE;

  if ( NULL == asm_info )
  {
    return FALSE;
  }
  else
  {
    asm_info->mfg_id = 0x134;
    asm_info->prd_id = 0x3D;
    asm_info->prd_rev = 0;
    asm_info->num_ports = RFDEVICE_ASM_rfmd8115_NUM_PORTS;
    ret_val = TRUE;
  }
  return ret_val;
}

