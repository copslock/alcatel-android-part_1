/*==============================================================================
  Copyright (c) 2012-2013 Qualcomm Technologies, Inc.
  All rights reserved. Qualcomm Proprietary and Confidential.
==============================================================================*/

#include "fir_block.h"
#include "q6protos.h"

#define LL_shr(x, n) (Q6_P_asr_PR((x),(n)))
#define L_sat(x) (Q6_R_sat_P( (x) ))
#define extract_h(x) ((int16_t)Q6_R_asrh_R( (x) ))

/*------------------------------------------------------------------------
  Function name: bkfir_example
  Performs block FIR on an input signal (*in_ptr) using the coefficients
  specified by *coeff_ptr and returns output (*out_ptr)
  NOTE - This function just does the multiply for faster performance. It 
  does not maintain any state information. All state information must 
  be managed by user.
 * -----------------------------------------------------------------------*/
void fir_block(
   int16_t* in_ptr,
   int16_t* coeff_ptr,
   int32_t num_taps,
   int32_t num_samples,
   int32_t shift_fac,
   int16_t* out_ptr)
{
  int32_t i, j;
  int64_t sum;

  for (i = 0; i < num_samples; i++)  {
    sum = 0;
    for (j = 0; j < num_taps; j++) {
      sum += coeff_ptr[j] * in_ptr[i + j];
    }
    sum = LL_shr(sum, shift_fac);
    out_ptr[i] = extract_h(L_sat(sum));
  }
}

