/**
	Auto Generated File
	File: custom_adsp_media_fmt.h

	Header file for adding custom static modules into database.
	custom_capi_integration.py in <ROOT>\hap\integration\audencdec\build
	will generate this file when OEM_ROOT is defined.
*/

/* =======================================================================

Copyright (c) 2013 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government, Diversion contrary to U.S. law prohibited.

==========================================================================*/
#ifndef __CUSTOM_ADSP_MEDIA_FMAT_H__
#define __CUSTOM_ADSP_MEDIA_FMAT_H__

/**
 * Define Media format ID for the custom static CAPI modules.
 * To acquire range of unique Media format ID's, email support.cdmatech@qualcomm.com
 */


#endif // #ifdef __CUSTOM_ADSP_MEDIA_FMAT_H__ 
