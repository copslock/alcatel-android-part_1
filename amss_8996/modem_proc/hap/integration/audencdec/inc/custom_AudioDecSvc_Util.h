/**
	Auto Generated File
	File: custom_AudioDecSvc_Util.h

	Header file contains all the modules and topologies information.
	custom_capi_integration.py in <ROOT>\hap\integration\audencdec\build
	will generate this file when OEM_ROOT is defined.
*/

/*==========================================================================
Copyright (c) 2013 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government, Diversion contrary to U.S. law prohibited.

==========================================================================*/
#ifndef __CUSTOM_AUDIODECSVC_UTIL_H__
#define __CUSTOM_AUDIODECSVC_UTIL_H__

/*--------------------------------------------------------------------------
 * Includes 
 * -------------------------------------------------------------------------*/
#include "custom_adsp_media_fmt.h"
#include "adsp_amdb_static.h"

/*------------------------------------------------------------------------
 * Include files of CAPI modules
 *------------------------------------------------------------------------*/

/*------------------------------------------------------------------------
 * Function Declrations
 *------------------------------------------------------------------------*/
 ADSPResult hap_adsp_amdb_add_static_capi(void);

/*--------------------------------------------------------------------------
 * Module Definition Structure 
 * -------------------------------------------------------------------------*/
typedef struct _capi_module_definition_t
{
	uint32_t media_fmt_id;
	capi_getsize_f getsize_f;
	capi_ctor_f ctor_f;
}capi_module_definition_t;

/*--------------------------------------------------------------------------
 * Array Of Module Definition For All Custom Static Modules 
 * -------------------------------------------------------------------------*/
const capi_module_definition_t custom_capi_module_definitions[] =
{
};

#endif // #ifdef __CUSTOM_AUDIODECSVC_UTIL_H__ 
