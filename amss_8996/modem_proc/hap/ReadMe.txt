1)      RSRC_MODEM_HAP is the patch package for HAP licensees and it is expected that customers should copy contents from RSRC_MODEM_HAP to HY11.
        NOTE: Existing content in HY11 needs to be present along with extra files taken from RSRC_MODEM_HAP package to make it compliable in source.
        Open command prompt and execute the following command
        cp -R <ROOT>\RSRC_MODEM_HAP\modem_proc <ROOT>\HY11_1\
        
2)      cd <ROOT>\HY11_1\modem_proc\build\ms

3)      On command prompt set the following variable to enable HAP component :
        set OEM_ROOT = 1 
        
4)      To enable audio examples(present in 'hap\examples\audio_avs\audioproc') to be compiled with modem binary, set the following flag
        set HAP_AUDIO_EXAMPLES=1
        (Also modify xml files present in 'hap\integration\audproc' to add custom audio modules to Elite framework dynamic services).
        
5)      To enable voice examples(present in 'hap\examples\audio_avs\vocproc') to be compiled with modem binary, set the following flag
        set HAP_VOICE_EXAMPLES=1
        (To add custom voice modules to Elite framework voice services, modify source code in VpTx and VpRx services).

