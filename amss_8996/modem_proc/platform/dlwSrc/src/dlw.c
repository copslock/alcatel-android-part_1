/*==============================================================================

Copyright (c) 2011 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government, Diversion contrary to U.S. law prohibited.

==============================================================================*/

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

#include "dlw.h"
#include "platform_libs.h"
#include "dlw_dlfcn.h"

#include "AEEatomic.h"

static uint32 dlw_ref = 0;

// if two threads contend for the first call to dlinit assume success for
// thread that loses
static int dlw_state = 1;

int dlw_Init(void)
{
  if (1 == atomic_Add(&dlw_ref, 1)) {
    char* builtin[] = {
      (char*)"libc.so",
      (char*)"libgcc.so",
    };
    dlw_state = dlinit(2, builtin);
  }

  return dlw_state;
}

void* dlw_Open(const char* file, int mode)
{
  return dlopen(file, mode);
}

int dlw_Close(void* h)
{
  return dlclose(h);
}

void* dlw_Sym(void* __restrict h, const char* __restrict name)
{
  return dlsym(h, name);
}

int dlw_Addr(const void* __restrict h, dlw_info* __restrict info)
{
  return dladdr(h, (Dl_info*)info);
}

PL_DEP(fastrpc_smd)
char* dlw_Error(void)
{
  const char* pszNone = "dlerror returned NULL";
  char* psz = dlerror();

  if (psz) {
    return psz;
  }
  return (char*)pszNone;
}

static void pl_dlw_DeInit(void) {
   PL_DEINIT(fastrpc_smd);
}
static int pl_dlw_Init(void) {
   int nErr = PL_INIT(fastrpc_smd);
   return dlw_Init() == 1 && nErr == 0 ? 0 : -1;
}
PL_DEFINE(dlw, pl_dlw_Init, pl_dlw_DeInit)
