#ifndef MOD_TABLE_CACHE_H
#define MOD_TABLE_CACHE_H
#include "qurt_types.h"

/**
  * get and set the modules prefered caching attributes for input and output buffers
  * this can only be called from the invoking thread.
  *
  * @param bInBuf, if TRUE the value is set for input buffers, if FALSE for output buffers
  * @param attr, one of qurts attributes
  * 
  */
int mod_table_set_cache_attr(boolean bInBuf, qurt_mem_cache_mode_t attr);
int mod_table_get_cache_attr(boolean bInBuf, qurt_mem_cache_mode_t* attr);

#endif //MOD_TABLE_CACHE_H
