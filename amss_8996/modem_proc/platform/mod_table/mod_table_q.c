#include "qtest_verify.h"
#include "verify.h"
#include "AEEstd.h"
#include "qtest_stdlib.h"
#include "qtest_rw_mutex.h"
#include "remote.h"
#include <assert.h>
#include <stdio.h>

#define DLW_RTLD_NOW 2

#define LOGL(...) (void)0
#define dlw_Open my_dlw_Open
#define dlw_Sym my_dlw_Sym
#define dlw_Close my_dlw_Close
#define dlw_Error my_dlw_Error

void* dlw_Open(const char* name, int flags) {
   if(0 == std_strcmp(name, "libtest_skel.so")) {
      return MALLOC(sizeof(int));
   } else {
      return 0;
   }
}

static __inline int dynamic_test_skel_invoke(uint32 sc, remote_arg* pra) {
   return (int)dynamic_test_skel_invoke;
}

static __inline int static_test_skel_invoke(uint32 sc, remote_arg* pra) {
   return (int)static_test_skel_invoke;
}
static __inline int override_test_skel_invoke(uint32 sc, remote_arg* pra) {
   return (int)override_test_skel_invoke;
}


void* dlw_Sym(void* h, const char* name) {
   if(0 == std_strcmp(name, "test_skel_invoke")) {
      return (void*)(dynamic_test_skel_invoke);
   } else {
      return 0;
   }
}

int dlw_Close(void* h) {
   int rv = qtest_test_failure();
   FREE(h);
   return rv;
}

const char* dlw_Error(void) {
   return "fake dlw_Error";
}


#include "mod_table_imp.h"

int runtest(int(*pfn)(void)) {
   int nErr = -1;
   int ii;
   for(ii = 0; nErr != 0 && ii <= 1000; ++ii) {
      nErr = 0;
      qtest_set_pass_count(ii);
      if(pfn) {
         VERIFY(0 == (nErr = pfn()));
      }
bail:
      qtest_done();
   }
   return nErr;
}

static int test_ctor(void) {
   int nErr = 0;
   struct mod_table mt = {0};
   VERIFY(0 == mod_table_ctor_imp(&mt));
   mod_table_dtor_imp(&mt);
bail:
   return nErr;
}

static int test_open_dynamic(void) {
   int nErr = 0;
   struct mod_table mt = {0};
   remote_handle handle = 0;
   int dlErr = 0;
   VERIFY(0 == mod_table_ctor_imp(&mt));
   VERIFY(0 == mod_table_open_dynamic(&mt, "test", &handle, 0, 0, &dlErr));
   VERIFY(0 == dlErr);
   VERIFY(0 != handle);
   mod_table_close_imp(&mt, handle, 0, 0, &dlErr);
   handle = 0;

   VERIFY(0 == dlErr);

bail:
   if(handle) {
      mod_table_close_imp(&mt, handle, 0, 0, &dlErr);
   }
   mod_table_dtor_imp(&mt);
   return nErr;
}

static int test_open_dynamic2(void) {
   int nErr = 0;
   struct mod_table mt = {0};
   remote_handle handle = 0;
   int dlErr = 0;
   VERIFY(0 == mod_table_ctor_imp(&mt));
   VERIFY(0 == mod_table_open_dynamic(&mt, "test", &handle, 0, 0, &dlErr));
   VERIFY(0 == dlErr);
   VERIFY(0 != handle);
bail:
   mod_table_dtor_imp(&mt);
   return nErr;
}

static __inline int test_open_dynamic_uri(void) {
   int nErr = 0;
   struct mod_table mt = {0};
   remote_handle handle = 0;
   int dlErr = 0;
   VERIFY(0 == mod_table_ctor_imp(&mt));
   VERIFY(0 == mod_table_open_dynamic(&mt, "file://libtest_skel.so?test", &handle, 0, 0, &dlErr));
   VERIFY(0 == dlErr);
   VERIFY(0 != handle);
bail:
   mod_table_dtor_imp(&mt);
   return nErr;
}

static int test_open_invalid(void) {
   int nErr = 0, nErrOpen;
   int dlErr = 0;
   remote_handle handle = -1;
   struct mod_table mt = {0};
   VERIFY(0 == mod_table_ctor_imp(&mt));
   nErrOpen = mod_table_open_dynamic(&mt, "invalid", &handle, 0, 0, &dlErr);
   VERIFY(0 != dlErr);
   VERIFY(-1 == handle);
   VERIFY(0 == nErrOpen);
bail:
   mod_table_dtor_imp(&mt);
   return nErr;
}


static int test_open_static(void) {
   int nErr = 0, nErrOpen;
   remote_handle handle = -1;
   struct mod_table mt = {0};
   VERIFY(0 == mod_table_ctor_imp(&mt));
   nErrOpen = mod_table_open_static(&mt, "static", &handle);
   VERIFY(0 != nErrOpen);
   VERIFY(-1 == handle);
bail:
   mod_table_dtor_imp(&mt);
   return nErr;
}

static int test_register_static(void) {
   int nErr = 0, dlErr = 0;
   remote_handle handle = 0;
   struct mod_table mt = {0};
   VERIFY(0 == mod_table_ctor_imp(&mt));
   VERIFY(0 == mod_table_register_static_imp(&mt, "static", static_test_skel_invoke));
   VERIFY(0 == mod_table_open_static(&mt, "static", &handle));
   VERIFY(0 != handle);
   mod_table_close_imp(&mt, handle, 0, 0, &dlErr);
   handle = 0;
   VERIFY(0 == dlErr);
bail:
   if(handle) {
      mod_table_close_imp(&mt, handle, 0, 0, &dlErr);
   }
   mod_table_dtor_imp(&mt);
   return nErr;
}

static int test_invoke(void) {
   int nErr = 0, dlErr, invokeRet;
   remote_handle handle;
   struct mod_table mt = {0};
   VERIFY(0 == mod_table_ctor_imp(&mt));
   VERIFY(0 == mod_table_register_static_imp(&mt, "static", static_test_skel_invoke));
   VERIFY(0 == mod_table_open_static(&mt, "static", &handle));
   invokeRet = mod_table_handle_invoke(&mt, handle, 0, 0);

   VERIFY(0 == mod_table_close_imp(&mt, handle, 0, 0, &dlErr));

   VERIFY(invokeRet == (int)static_test_skel_invoke);
   VERIFY(0 == mod_table_open_dynamic(&mt, "test", &handle, 0, 0, 0));
   invokeRet = mod_table_handle_invoke(&mt, handle, 0, 0);
   VERIFY(invokeRet == (int)dynamic_test_skel_invoke);
   VERIFY(0 == mod_table_close_imp(&mt, handle, 0, 0, &dlErr));
bail:
   mod_table_dtor_imp(&mt);
   return nErr;
}

static int test_dynamic_override(void) {
   int nErr = 0, dlErr, invokeRet;
   remote_handle handle;
   struct mod_table mt = {0};
   VERIFY(0 == mod_table_ctor_imp(&mt));
   VERIFY(0 == mod_table_register_static_imp(&mt, "test", static_test_skel_invoke));

   VERIFY(0 == mod_table_open_imp(&mt, "test", &handle, 0, 0, 0));
   invokeRet = mod_table_handle_invoke(&mt, handle, 0, 0);
   VERIFY(invokeRet == (int)dynamic_test_skel_invoke);
   VERIFY(0 == mod_table_close_imp(&mt, handle, 0, 0, &dlErr));
bail:
   mod_table_dtor_imp(&mt);
   return nErr;
}

static int test_static_override(void) {
   int nErr = 0, dlErr, invokeRet;
   remote_handle handle;
   struct mod_table mt = {0};
   VERIFY(0 == mod_table_ctor_imp(&mt));
   VERIFY(0 == mod_table_register_static_imp(&mt, "test", static_test_skel_invoke));
   VERIFY(0 == mod_table_register_static_override_imp(&mt, "test", override_test_skel_invoke));

   VERIFY(0 == mod_table_open_imp(&mt, "test", &handle, 0, 0, 0));
   invokeRet = mod_table_handle_invoke(&mt, handle, 0, 0);
   VERIFY(invokeRet == (int)override_test_skel_invoke);
   VERIFY(0 == mod_table_close_imp(&mt, handle, 0, 0, &dlErr));

bail:
   mod_table_dtor_imp(&mt);
   return nErr;
}

static int test_static_override_open_dynamic(void) {
   int nErr = 0, dlErr, invokeRet;
   remote_handle handle;
   struct mod_table mt = {0};
   VERIFY(0 == mod_table_ctor_imp(&mt));
   VERIFY(0 == mod_table_register_static_imp(&mt, "static", static_test_skel_invoke));
   VERIFY(0 == mod_table_register_static_imp(&mt, "test", static_test_skel_invoke));
   VERIFY(0 == mod_table_register_static_override_imp(&mt, "override", override_test_skel_invoke));

   VERIFY(0 == mod_table_open_imp(&mt, "test", &handle, 0, 0, 0));
   invokeRet = mod_table_handle_invoke(&mt, handle, 0, 0);
   VERIFY(invokeRet == (int)dynamic_test_skel_invoke);
   VERIFY(0 == mod_table_close_imp(&mt, handle, 0, 0, &dlErr));

   VERIFY(0 == mod_table_open_imp(&mt, "override", &handle, 0, 0, 0));
   invokeRet = mod_table_handle_invoke(&mt, handle, 0, 0);
   VERIFY(invokeRet == (int)override_test_skel_invoke);
   VERIFY(0 == mod_table_close_imp(&mt, handle, 0, 0, &dlErr));

   VERIFY(0 == mod_table_open_imp(&mt, "static", &handle, 0, 0, 0));
   invokeRet = mod_table_handle_invoke(&mt, handle, 0, 0);
   VERIFY(invokeRet == (int)static_test_skel_invoke);
   VERIFY(0 == mod_table_close_imp(&mt, handle, 0, 0, &dlErr));

bail:
   mod_table_dtor_imp(&mt);
   return nErr;
}

static int test_invoke_const_handle(void) {
   int nErr = 0, invokeRet;
   struct mod_table mt = {0};
   VERIFY(0 == mod_table_ctor_imp(&mt));
   VERIFY(0 == mod_table_register_const_handle_imp(&mt, 0, "static0", static_test_skel_invoke));

   invokeRet = mod_table_handle_invoke(&mt, 0, 0, 0);
   VERIFY(invokeRet == (int)static_test_skel_invoke);

   invokeRet = mod_table_handle_invoke(&mt, 1, 0, 0);
   VERIFY(invokeRet != (int)static_test_skel_invoke);
   VERIFY(invokeRet != 0);

   VERIFY(0 == mod_table_register_const_handle_imp(&mt, 1, "static1", static_test_skel_invoke));
   invokeRet = mod_table_handle_invoke(&mt, 1, 0, 0);
   VERIFY(invokeRet == (int)static_test_skel_invoke);

   VERIFY(0 != mod_table_register_const_handle_imp(&mt, 0, "static0", static_test_skel_invoke));
   VERIFY(0 != mod_table_register_const_handle_imp(&mt, 1, "static1", static_test_skel_invoke));

   mod_table_close_imp(&mt, 1, 0, 0, 0);
   invokeRet = mod_table_handle_invoke(&mt, 1, 0, 0);
   VERIFY(invokeRet == (int)static_test_skel_invoke);

   mod_table_close_imp(&mt, 0, 0, 0, 0);
   invokeRet = mod_table_handle_invoke(&mt, 0, 0, 0);
   VERIFY(invokeRet == (int)static_test_skel_invoke);
bail:
   mod_table_dtor_imp(&mt);
   return nErr;
}
static int test_open(void) {
   int nErr = 0, nErrOpen = 0, dlErr = 0;
   remote_handle handle = -1;
   struct mod_table mt = {0};
   VERIFY(0 == mod_table_ctor_imp(&mt));
   nErrOpen = mod_table_open_imp(&mt, "static", &handle, 0, 0, &dlErr);
   VERIFY(0 == nErrOpen);
   VERIFY(0 != dlErr);

   VERIFY(0 == mod_table_register_static_imp(&mt, "static", static_test_skel_invoke));
   nErrOpen = mod_table_open_imp(&mt, "static", &handle, 0, 0, &dlErr);
   VERIFY(0 == nErrOpen);
   VERIFY(0 == dlErr);
bail:
   mod_table_dtor_imp(&mt);
   return nErr;
}


int main(void) {
   int nErr = 0;
   VERIFY(0 == runtest(test_ctor));
   VERIFY(0 == runtest(test_open_dynamic));
   VERIFY(0 == runtest(test_open_invalid));
   VERIFY(0 == runtest(test_open_dynamic2));
   VERIFY(0 == runtest(test_open_static));
   VERIFY(0 == runtest(test_register_static));
   VERIFY(0 == runtest(test_invoke));
   VERIFY(0 == runtest(test_dynamic_override));
   VERIFY(0 == runtest(test_static_override));
   VERIFY(0 == runtest(test_static_override_open_dynamic));
   VERIFY(0 == runtest(test_invoke_const_handle));
   VERIFY(0 == runtest(test_open));
   VERIFY(0 == runtest(test_open_dynamic_uri));
bail:
   return nErr;
}
void HAP_debug(const char *msg, int level, const char *filename, int line) {
   printf("%s:%d:farf %d:%s\n", filename, line, level, msg);
}
