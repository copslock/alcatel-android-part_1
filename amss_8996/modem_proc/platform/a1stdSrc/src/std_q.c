#include <string.h>
#include <stdio.h>

#include "AEEstd.h"

#ifndef __FILENAME__
#define __FILENAME__ __FILE__
#endif

/* 
   cheap and cheesey, necessary because gcc defines wchar_t strings,
   or L"" to be words not shorts
*/
#define L(x) _L(x, sizeof(x))

static const AECHAR* _L(const char* p, int nLen)
{
   static int nIdx = 0;
   static AECHAR asz[8][256];
   AECHAR* pwszRet = asz[nIdx++];
   AECHAR* pwsz = pwszRet;
   
   if (nIdx == STD_ARRAY_SIZE(asz)) {
      nIdx = 0;
   }
   
   while (nLen-- > 0) {
      *pwsz++ = *p++;
   }

   return pwszRet;
}

static int StrEQ(const char *a, const char *b)
{
   return 0 == std_strcmp(a,b);
}

#define TEST(x) \
   do { if (!(x)) { \
      printf(__FILENAME__ ":%d: test failed: %s\n", __LINE__, #x); \
      errs++; \
   } } while (0)


static int std_getversion_test(void)
{
   char vs[512];
   int nLen = 0;
   int errs = 0;

   memset(vs, 0, sizeof(vs));

   nLen = std_getversion(vs, 512);

   if (512 < nLen) {
      printf("std_getversion buffer too small\n");
      errs++;
   }

   printf("std_getversion returns %s\n", vs);

   return errs;
}

static int std_misc_test(void)
{
   int errs = 0;

   TEST(std_tolower(0) == 0);
   TEST(std_tolower('A'-1) == 'A'-1);
   TEST(std_tolower('A') == 'a');
   TEST(std_tolower('Z') == 'z');
   TEST(std_tolower('Z'+1) == 'Z'+1);
   TEST((uint8)std_tolower((uint8)255) == 255);

   TEST(std_toupper(0) == 0);
   TEST(std_toupper('a'-1) == 'a'-1);
   TEST(std_toupper('a') == 'A');
   TEST(std_toupper('z') == 'Z');
   TEST(std_toupper('z'+1) == 'z'+1);
   TEST((uint8)std_toupper((uint8)255) == 255);

#define STD_STRLEN_TEST(literal)  TEST( std_strlen(literal) == sizeof(literal)-1 )

   STD_STRLEN_TEST("hi");
   STD_STRLEN_TEST("\xff\xfe\1");
   STD_STRLEN_TEST("");

   return errs;
}

static void SetWords(uint16 *pwc, const uint16 *pstr, int n)
{
   while (n) {
      *pwc++ = *pstr++;
      --n;
   }
}

static int CmpWords(const uint16 *pwc, const uint16 *pstr, int n)
{
   while (n) {
      if (*pstr != *pwc) {
         return 0;
      }
      ++pwc;
      ++pstr;
      --n;
   }
   return 1;
}

static int std_wstrlen_test(void)
{
   int errs = 0;

   TEST( std_wstrlen(L("abc")) == 3 );
   TEST( std_wstrlen(L("")) == 0 );
   TEST( std_wstrlen(L("\xff")) == 1 );

   {
      AECHAR wszTmp[10] = { 'a','b','c','d','e','f','g','h','i',0};
      TEST(9 == std_wstrlen(wszTmp));
   }

   {
      AECHAR wszTmp[10] = { 0,'b','c','d','e','f','g','h','i',0};
      TEST(0 == std_wstrlen(wszTmp));
   }

   {
      AECHAR wszTmp[10] = { 'a','b',0,'d','e','f','g','h','i',0};
      TEST(2 == std_wstrlen(wszTmp));
   }

   return errs;
}

static int std_wstrlcpy_test(void)
{
   int errs = 0;
   uint16 awc[20];

#define INITWC(chk)  SetWords(awc, L(chk), STD_ARRAY_SIZE(chk)-1)
#define TESTWC(chk)  TEST( CmpWords(awc, L(chk), STD_ARRAY_SIZE(chk)-1) )

   INITWC( "abcde" );
   TEST( std_wstrlcpy(awc+1, L("hi"), 0) == 2 );
   TESTWC( "abcde" );

   TEST( std_wstrlcpy(awc+1, L("hi"), MIN_INT32) == 2 );
   TESTWC( "abcde" );

   TEST( std_wstrlcpy(awc+1, L("hi"), 1) == 2 );
   TESTWC( "a\0cde" );

   INITWC( "abcde" );
   TEST( std_wstrlcpy(awc+1, L("hi"), 2) == 2 );
   TESTWC( "ah\0de" );

   INITWC( "abcde" );
   TEST( std_wstrlcpy(awc+1, L("hi"), 3) == 2 );
   TESTWC( "ahi\0e" );

   {
      int  n;
      AECHAR pwcBuf[10];
      unsigned ulBufSize = sizeof(pwcBuf)/sizeof(pwcBuf[0]);

      const AECHAR wszAlphaA[2] = {'a',0};
      const AECHAR wszAlphaAtoI[] = {'a','b','c','d','e','f','g','h','i',0, 0xaaaa};
      const AECHAR wszAlphaAtoJ[] = {'a','b','c','d','e','f','g','h','i','j',0};
      const AECHAR *wszTmp;

      wszTmp = wszAlphaA;  // "a"
      n = std_wstrlcpy(pwcBuf, wszTmp, ulBufSize);
      TEST(std_wstrlen(wszTmp) == n && 
           0 == std_memcmp(wszTmp, pwcBuf, std_wstrlen(wszTmp)));

      wszTmp = wszAlphaAtoI; // "abcdefghi"
      n = std_wstrlcpy(pwcBuf, wszTmp, ulBufSize);
      TEST(std_wstrlen(wszTmp) == n && 
           0 == std_memcmp(wszTmp, pwcBuf, ulBufSize));

      wszTmp = wszAlphaAtoJ; // "abcdefghij"
      n = std_wstrlcpy(pwcBuf, wszTmp, ulBufSize);
      TEST(std_wstrlen(wszTmp) == n && 
           0 <= std_memcmp(wszTmp, pwcBuf, sizeof(pwcBuf[0]) * std_wstrlen(wszTmp)) && 
           0 == std_memcmp(wszAlphaAtoI, pwcBuf, sizeof(pwcBuf)));
   }

   return errs;
}

static int std_wstrlcat_test(void)
{
   int errs = 0;
   uint16 awc[20];

   INITWC( "abcde" );
   TEST( std_wstrlcat(awc+1, L("hi"), 0) == 2 );
   TESTWC( "abcde" );

   TEST( std_wstrlcat(awc+1, L("hi"), 1) == 3 );
   TESTWC( "abcde" );

   INITWC( "ab\0de" );
   TEST( std_wstrlcat(awc+1, L("hi"), 3) == 3 );
   TESTWC( "abh\0e" );

   INITWC( "ab\0de" );
   TEST( std_wstrlcat(awc+1, L("hi"), 9) == 3 );
   TESTWC( "abhi\0" );

   {
      int  n;
      AECHAR pwcBuf[10];
      unsigned ulBufSize = sizeof(pwcBuf)/sizeof(pwcBuf[0]);

      const AECHAR wszAlphaABC[] = {'a','b','c',0};
      const AECHAR wszAlphaIJK[] = {'i','j','k',0};
      const AECHAR wszAlphaDtoI[] = {'d','e','f','g','h','i',0};
      const AECHAR wszAlphaAtoH[] = {'a','b','c','d','e','f','g','h',0};
      const AECHAR wszAlphaAtoI[] = {'a','b','c','d','e','f','g','h','i',0, 0xaaaa};
      const AECHAR wszAlphaAtoK[] = {'a','b','c','d','e','f','g','h','i','j','k',0};

      std_wstrlcpy(pwcBuf, wszAlphaABC, ulBufSize);
      n = std_wstrlcat(pwcBuf, wszAlphaDtoI, ulBufSize);
      TEST((9 == n && 0 == std_memcmp(pwcBuf, wszAlphaAtoI, 
                                      sizeof(pwcBuf[0]) * std_wstrlen(wszAlphaAtoI))));

      std_wstrlcpy(pwcBuf, wszAlphaAtoH, ulBufSize);
      n = std_wstrlcat(pwcBuf, wszAlphaIJK, ulBufSize);
      TEST((11 == n && 
            0 >= std_memcmp(pwcBuf, wszAlphaAtoK, sizeof(pwcBuf[0])*std_wstrlen(wszAlphaAtoK)) &&
            0 == std_memcmp(pwcBuf, wszAlphaAtoI, sizeof(pwcBuf[0])*std_wstrlen(wszAlphaAtoI))));
   }

   return errs;
}

static int std_strcmp_test(void)
{
   int errs = 0;

   TEST(0 == std_strcmp("h", "h"));
   TEST(0 > std_strcmp("h", "hh"));
   TEST(0 == std_strcmp("h\0i", "h\0x"));
   TEST(std_strcmp("hi","hi") == 0);
   TEST(std_strcmp("","") == 0);
   TEST(std_strcmp("h","i") < 0);
   TEST(std_strcmp("i","h") > 0);
   TEST(std_strcmp("hi","i") < 0);
   TEST(std_strcmp("i","hi") > 0);
   TEST(std_strcmp("\1\1\1\1","\2\1\1\1") < 0);
   TEST(std_strcmp("\1\1\1\1","\1\1\1\2") < 0);
   TEST(std_strcmp("\xff","\1") > 0);

   return errs;
}

static int std_strncmp_test(void)
{
   int errs = 0;

   TEST(0 == std_strncmp("h", "h", 1));
   TEST(0 > std_strncmp("h", "hh", 2));
   TEST(0 == std_strncmp("h\0i", "h\0x", 3));

   TEST(std_strncmp("hi", "hi",22) == 0);
   TEST(std_strncmp("hia","hib",2) == 0);
   TEST(std_strncmp("hia","hi",2) == 0);

   TEST(std_strncmp("hi", "h",2) > 0);
   TEST(std_strncmp("hi", "h",1) == 0);
   TEST(std_strncmp("hi", "h",0) == 0);
   TEST(std_strncmp("hi", "h",MAX_INT32) > 0);
   TEST(std_strncmp("hi", "i",MIN_INT32) == 0);

   TEST(std_strncmp("h", "i",1) < 0);
   TEST(std_strncmp("i", "h",9) > 0);
   TEST(std_strncmp("hi","i",1) < 0);
   TEST(std_strncmp("hi","i",2) < 0);
   TEST(std_strncmp("i","hi",3) > 0);
   TEST(std_strncmp("\1\1\1\1","\2\1\1\1",9) < 0);
   TEST(std_strncmp("\1\1\1\1","\1\1\1\2",9) < 0);
   TEST(std_strncmp("\xff","\1",2) > 0);

   return errs;
}

static int std_stricmp_test(void)
{
   int errs = 0;

   TEST(0 > std_stricmp("a", "aa"));
   TEST(0 > std_stricmp("a", "Aa"));
   TEST(0 > std_stricmp("a", "b"));
   TEST(0 > std_stricmp("A", "b"));
   TEST(0 == std_stricmp("a", "a"));
   TEST(0 == std_stricmp("a", "A"));
   TEST(0 == std_stricmp("a\0b", "a\0c"));
   TEST(0 == std_stricmp("A\0b", "a\0c"));
   TEST(0 < std_stricmp("aa", "a"));
   TEST(0 < std_stricmp("aA", "a"));
   TEST(0 < std_stricmp("b", "a"));
   TEST(0 < std_stricmp("b", "A"));

   TEST(std_stricmp("aZ","Az") == 0);
   TEST(std_stricmp("Az","aZ") == 0);

   TEST(std_stricmp("\x60","\x40") == 0x20);
   TEST(std_stricmp("\x7B","\x5b") == 0x20);

   TEST(std_stricmp("Az","aY") == (int)'z'-'y');
   TEST(std_stricmp("AZ","ay") == (int)'z'-'y');
   TEST(std_stricmp("Ay","aZ") == (int)'y'-'z');
   TEST(std_stricmp("Ay","Az") == (int)'y'-'z');

   TEST(std_stricmp("a","@") == 'a'-'@');
   TEST(std_stricmp("A","@") == 'a'-'@');

   TEST(std_stricmp("\1\1\1\1","\2\1\1\1") < 0);
   TEST(std_stricmp("\1\1\1\1","\1\1\1\2") < 0);
   TEST(std_stricmp("\xff","\1") > 0);

   return errs;
}

static int std_strnicmp_test(void)
{
   int errs = 0;

   TEST(0 > std_strnicmp("a", "aa", 2));
   TEST(0 > std_strnicmp("a", "aA", 2));
   TEST(0 > std_strnicmp("a", "b", 1));
   TEST(0 > std_strnicmp("A", "b", 1));
   TEST(0 == std_strnicmp("a", "a", 1));
   TEST(0 == std_strnicmp("a", "A", 1));
   TEST(0 == std_strnicmp("a", "ab", 1));
   TEST(0 == std_strnicmp("A", "AB", 1));
   TEST(0 == std_strnicmp("a\0b", "a\0c", 3));
   TEST(0 == std_strnicmp("A\0B", "a\0c", 3));
   TEST(0 == std_strnicmp("b", "a", 0));
   TEST(0 == std_strnicmp("B", "a", 0));
   TEST(0 < std_strnicmp("aa", "a", 2));
   TEST(0 < std_strnicmp("aa", "A", 2));
   TEST(0 < std_strnicmp("b", "a", 1));
   TEST(0 < std_strnicmp("B", "a", 1));

   TEST(std_strnicmp("aZ","Az",2) == 0);
   TEST(std_strnicmp("Az","aZ",2) == 0);

   TEST(std_strnicmp("a", "A",0) == 0);
   TEST(std_strnicmp("a", "A",1) == 0);
   TEST(std_strnicmp("az","A",2) > 0);
   TEST(std_strnicmp("az","A",3) > 0);

   TEST(std_strnicmp("Az","aY",2) == (int)'z'-'y');
   TEST(std_strnicmp("AZ","ay",2) == (int)'z'-'y');
   TEST(std_strnicmp("Ay","aZ",2) == (int)'y'-'z');
   TEST(std_strnicmp("Ay","Az",2) == (int)'y'-'z');

   TEST(std_strnicmp("a","@",9) == 'a'-'@');
   TEST(std_strnicmp("A","@",9) == 'a'-'@');
   TEST(std_strnicmp("\xff","\1",1) > 0);

   return errs;
}

static int std_strbegins_test(void)
{
   int errs = 0;
   char szBuf0[] = "";
   char szBuf1[] = "a";
   char szBuf2[] = "abcd";
   char szBuf3[] = "ABCD";
   char szBuf4[] = "Abcd";
   char szBuf5[] = "abcd\0efg";

   TEST( std_strbegins("", "") );
   TEST( std_strbegins("abc", "") );
   TEST( std_strbegins("abc", "a") );
   TEST( std_strbegins("abc", "abc") );
   TEST( !std_strbegins("abc", "abC") );
   TEST( !std_strbegins("abc", "abcd") );
   TEST( !std_strbegins("abc", "bc") );
   TEST( !std_strbegins("", "bc") );

   TEST(szBuf0 + 0 == std_strbegins(szBuf0, ""));
   TEST(0          == std_strbegins(szBuf0, "a"));
   TEST(szBuf1 + 1 == std_strbegins(szBuf1, "a"));
   TEST(szBuf1 + 0 == std_strbegins(szBuf1, ""));
   TEST(szBuf2 + 1 == std_strbegins(szBuf2, "a"));
   TEST(0          == std_strbegins(szBuf2, "b"));
   TEST(0          == std_strbegins(szBuf2, "c"));
   TEST(0          == std_strbegins(szBuf2, "d"));
   TEST(szBuf4 + 1 == std_strbegins(szBuf4, "A"));
   TEST(0          == std_strbegins(szBuf4, "a"));
   TEST(szBuf2 + 4 == std_strbegins(szBuf2, "abcd"));
   TEST(szBuf3 + 4 == std_strbegins(szBuf3, "ABCD"));
   TEST(0          == std_strbegins(szBuf2, "aabcde"));
   TEST(0          == std_strbegins(szBuf2, "abcde"));
   TEST(0          == std_strbegins(szBuf2, "Abcd"));
   TEST(szBuf5 + 1 == std_strbegins(szBuf5, "a"));
   TEST(szBuf5 + 2 == std_strbegins(szBuf5, "ab"));
   TEST(0          == std_strbegins(szBuf5, "efg"));

   return errs;
}

static int std_stribegins_test(void)
{
   int errs = 0;
   char szBuf0[] = "";
   char szBuf1[] = "a";
   char szBuf2[] = "abcd";
   char szBuf3[] = "ABCD";
   char szBuf4[] = "Abcd";
   char szBuf5[] = "abcd\0efg";

   TEST( std_stribegins("", "") );
   TEST( std_stribegins("abc", "") );
   TEST( std_stribegins("abc", "a") );
   TEST( std_stribegins("abc", "abc") );
   TEST( std_stribegins("aBc", "Ab") );
   TEST( std_stribegins("Abc", "aB") );
   TEST( !std_stribegins("abc", "abcd") );
   TEST( !std_stribegins("abc", "bc") );
   TEST( !std_stribegins("", "a") );

   TEST(szBuf0 + 0 == std_stribegins(szBuf0, ""));
   TEST(0          == std_stribegins(szBuf0, "a"));
   TEST(szBuf1 + 1 == std_stribegins(szBuf1, "a"));
   TEST(szBuf1 + 0 == std_stribegins(szBuf1, ""));
   TEST(szBuf2 + 1 == std_stribegins(szBuf2, "a"));
   TEST(0          == std_stribegins(szBuf2, "b"));
   TEST(0          == std_stribegins(szBuf2, "c"));
   TEST(0          == std_stribegins(szBuf2, "d"));
   TEST(szBuf4 + 1 == std_stribegins(szBuf4, "A"));
   TEST(szBuf4 + 1 == std_stribegins(szBuf4, "a"));
   TEST(szBuf2 + 4 == std_stribegins(szBuf2, "abcd"));
   TEST(szBuf3 + 4 == std_stribegins(szBuf3, "ABCD"));
   TEST(0          == std_stribegins(szBuf2, "aabcde"));
   TEST(0          == std_stribegins(szBuf2, "abcde"));
   TEST(szBuf2 + 4 == std_stribegins(szBuf2, "Abcd"));
   TEST(szBuf5 + 1 == std_stribegins(szBuf5, "a"));
   TEST(szBuf5 + 2 == std_stribegins(szBuf5, "ab"));
   TEST(0          == std_stribegins(szBuf5, "efg"));

   return errs;
}

static int std_strends_test(void)
{
   int errs = 0;
   char szBuf0[] = "";
   char szBuf1[] = "a";
   char szBuf2[] = "abcd";
   char szBuf3[] = "ABCD";
   char szBuf4[] = "abcD";
   char szBuf5[] = "abcd\0efg";

   TEST( std_strends("", "") );
   TEST( std_strends("abc", "") );
   TEST( std_strends("abc", "c") );
   TEST( std_strends("abc", "abc") );
   TEST( !std_strends("abc", "bC") );
   TEST( !std_strends("abc", "aabc") );
   TEST( !std_strends("abc", "ab") );
   TEST( !std_strends("", "a") );

   TEST(szBuf0 + 0 == std_strends(szBuf0, ""));
   TEST(0          == std_strends(szBuf0, "a"));
   TEST(szBuf1 + 0 == std_strends(szBuf1, "a"));
   TEST(szBuf1 + 1 == std_strends(szBuf1, ""));
   TEST(0          == std_strends(szBuf2, "a"));
   TEST(0          == std_strends(szBuf2, "b"));
   TEST(0          == std_strends(szBuf2, "c"));
   TEST(szBuf2 + 3 == std_strends(szBuf2, "d"));
   TEST(szBuf4 + 3 == std_strends(szBuf4, "D"));
   TEST(0          == std_strends(szBuf2, "D"));
   TEST(szBuf2 + 0 == std_strends(szBuf2, "abcd"));
   TEST(szBuf3 + 0 == std_strends(szBuf3, "ABCD"));
   TEST(0          == std_strends(szBuf2, "aabcde"));
   TEST(0          == std_strends(szBuf2, "abcde"));
   TEST(0          == std_strends(szBuf2, "Abcd"));
   TEST(szBuf5 + 3 == std_strends(szBuf5, "d"));
   TEST(szBuf5 + 2 == std_strends(szBuf5, "cd"));
   TEST(0          == std_strends(szBuf5, "efg"));

   return errs;
}

static int std_striends_test(void)
{
   int errs = 0;
   char szBuf0[] = "";
   char szBuf1[] = "a";
   char szBuf2[] = "abcd";
   char szBuf3[] = "ABCD";
   char szBuf4[] = "abcD";
   char szBuf5[] = "abcd\0efg";

   TEST( std_striends("", "") );
   TEST( std_striends("abc", "") );
   TEST( std_striends("abc", "c") );
   TEST( std_striends("abc", "abc") );
   TEST( std_striends("aBc", "bC") );
   TEST( std_striends("abC", "Bc") );
   TEST( !std_striends("abc", "aabc") );
   TEST( !std_striends("abc", "a") );
   TEST( !std_striends("", "a") );

   TEST(szBuf0 + 0 == std_strends(szBuf0, ""));
   TEST(0          == std_strends(szBuf0, "a"));
   TEST(szBuf1 + 0 == std_strends(szBuf1, "a"));
   TEST(szBuf1 + 1 == std_strends(szBuf1, ""));
   TEST(0          == std_strends(szBuf2, "a"));
   TEST(0          == std_strends(szBuf2, "b"));
   TEST(0          == std_strends(szBuf2, "c"));
   TEST(szBuf2 + 3 == std_strends(szBuf2, "d"));
   TEST(szBuf4 + 3 == std_strends(szBuf4, "D"));
   TEST(0          == std_strends(szBuf2, "D"));
   TEST(szBuf2 + 0 == std_strends(szBuf2, "abcd"));
   TEST(szBuf3 + 0 == std_strends(szBuf3, "ABCD"));
   TEST(0          == std_strends(szBuf2, "aabcde"));
   TEST(0          == std_strends(szBuf2, "abcde"));
   TEST(0          == std_strends(szBuf2, "Abcd"));
   TEST(szBuf5 + 3 == std_strends(szBuf5, "d"));
   TEST(szBuf5 + 2 == std_strends(szBuf5, "cd"));
   TEST(0          == std_strends(szBuf5, "efg"));

   return errs;
}

static int std_strstr_test(void)
{
   int errs = 0;
   const char* cpszHaystack = "abc\01";
   static const struct {
      const char* cpszNeedle;
      int         nExpectedDiff; /* distance into haystack */
   } tests [] = {
      {
         "",
         0,
      },
      {
         "a",
         0,
      },
      {
         "x",
         -1,
      },
      {
         "\02",
         -1,
      },
      {
         "\01",
         3,
      },
      {
         "b",
         1,
      },
   };
   int i;
   char* pszRet;

   if (0 == std_strstr("","")) {
      errs++;
      printf("std_strstr(\"\", \"\") got NULL, expected \"\"\n");
   }

   /* sick, sick case */
   if ((char*)0xfefe != (pszRet = std_strstr((const char*)0xfefe, ""))) {
      errs++;
      printf("std_strstr(0xfefe, \"\") got 0x%p, expected 0xfefe\n",pszRet);
   }


   for (i = 0; i < STD_ARRAY_SIZE(tests); i++) {
      pszRet = std_strstr(cpszHaystack,tests[i].cpszNeedle);

      if (tests[i].nExpectedDiff == -1 && pszRet != 0) {
         printf("std_strstr(\"%s\", \"%s\") got \"%s\", expected NULL\n",
                cpszHaystack,
                tests[i].cpszNeedle,
                pszRet);
         errs++;
      } else if (tests[i].nExpectedDiff != -1 && 
                 pszRet - cpszHaystack != tests[i].nExpectedDiff) {
         printf("std_strstr(\"%s\", \"%s\") got \"%s\", expected \"%s\"\n",
                cpszHaystack,
                tests[i].cpszNeedle,
                pszRet?pszRet:"NULL",
                cpszHaystack+tests[i].nExpectedDiff);
         errs++;
      }
   }

   {
      const char *szTmp;

      szTmp = std_strstr("abcdefg", "a");
      TEST(0 == std_strncmp(szTmp, "a", 1));

      szTmp = std_strstr("abcdefg", "efg");
      TEST(0 == std_strncmp(szTmp, "efg", 3));

      szTmp = std_strstr("abcdefg", "abcdefg");
      TEST(0 == std_strncmp(szTmp, "abcdefg", 7));

      szTmp = std_strstr("abcdefg", "not found");
      TEST(NULL == szTmp);

      szTmp = std_strstr("abcdebcfg", "bcf");
      TEST(0 == std_strncmp(szTmp, "bcf", 3));

      szTmp = std_strstr("abcdebcbcf", "bcf");
      TEST(0 == std_strncmp(szTmp, "bcf", 3));

      szTmp = std_strstr("TTest", "Test");
      TEST(0 == std_strncmp(szTmp, "Test", 4));

      szTmp = std_strstr("ssssample", "sssam");
      TEST(0 == std_strncmp(szTmp, "sssam", 5));
   }

   return errs;
}

static int std_strchr_test(void)
{
   int errs = 0;
   char szBuf[] = {'a','b','c','d','\0'};

   TEST(0         == std_strchr(szBuf, 'e'));
   TEST(szBuf + 0 == std_strchr(szBuf, 'a'));
   TEST(szBuf + 1 == std_strchr(szBuf, 'b'));
   TEST(szBuf + 2 == std_strchr(szBuf, 'c'));
   TEST(szBuf + 3 == std_strchr(szBuf, 'd'));
   TEST(szBuf + 4 == std_strchr(szBuf, '\0'));

   return errs;
}

static int std_strrchr_test(void)
{
   int errs = 0;
   char szBuf[] = {'a','b','c','d','a','b','c','d','d','\0'};

   TEST(0         == std_strrchr(szBuf, 'e'));
   TEST(szBuf + 4 == std_strrchr(szBuf, 'a'));
   TEST(szBuf + 5 == std_strrchr(szBuf, 'b'));
   TEST(szBuf + 6 == std_strrchr(szBuf, 'c'));
   TEST(szBuf + 8 == std_strrchr(szBuf, 'd'));
   TEST(szBuf + 9 == std_strrchr(szBuf, '\0'));

   return errs;
}

static int std_strchrend_test(void)
{
   int errs = 0;
   char szBuf[] = {'a','b','c','d','\0'};

   TEST(szBuf + 4 == std_strchrend(szBuf, 'e'));
   TEST(szBuf + 0 == std_strchrend(szBuf, 'a'));
   TEST(szBuf + 1 == std_strchrend(szBuf, 'b'));
   TEST(szBuf + 2 == std_strchrend(szBuf, 'c'));
   TEST(szBuf + 3 == std_strchrend(szBuf, 'd'));
   TEST(szBuf + 4 == std_strchrend(szBuf, '\0'));

   return errs;
}

static int std_strchrsend_test(void)
{
   int errs = 0;
   char szBuf[] = {'a','b','c','d','\0'};

   TEST(szBuf + 4 == std_strchrsend(szBuf, "z"));
   TEST(szBuf + 0 == std_strchrsend(szBuf, "a"));
   TEST(szBuf + 1 == std_strchrsend(szBuf, "b"));
   TEST(szBuf + 2 == std_strchrsend(szBuf, "c"));
   TEST(szBuf + 3 == std_strchrsend(szBuf, "d"));
   TEST(szBuf + 4 == std_strchrsend(szBuf, "\0"));
   TEST(szBuf + 0 == std_strchrsend(szBuf, "abcd"));
   TEST(szBuf + 0 == std_strchrsend(szBuf, "eabcd"));
   TEST(szBuf + 0 == std_strchrsend(szBuf, "bacd"));
   TEST(szBuf + 4 == std_strchrsend(szBuf, "zyxw"));
   TEST(szBuf + 0 == std_strchrsend(szBuf, "zyxwa"));

   return errs;
}

#define INIT(dest,str) std_memmove(dest, str, sizeof(str))
static int std_strlcat_test(void)
{
   char ac[80];
   int n;
   int errs = 0;

   std_strlcpy(ac, "abc", sizeof(ac));
   n = std_strlcat(ac, "def", sizeof(ac));
   TEST(6 == n);
   TEST(0 == std_strcmp(ac, "abcdef"));

   std_strlcpy(ac, "abcd", sizeof(ac));
   n = std_strlcat(ac, "efg", sizeof(ac));
   TEST(7 == n);
   TEST(0 == std_strcmp(ac, "abcdefg"));

   std_strlcpy(ac, "abcdefgh", sizeof(ac));
   n = std_strlcat(ac, "ijk", sizeof(ac));
   TEST(11 == n);
   TEST(0 == std_strcmp(ac, "abcdefghijk"));

   std_strlcpy(ac, "abc", sizeof(ac));
   n = std_strlcat(ac, "defghijkl", sizeof(ac));
   TEST(12 == n);
   TEST(0 == std_strcmp(ac, "abcdefghijkl"));

   INIT(ac, "abc\0defghi");
   TEST( std_strlcat(ac, "def", 0) == 3 && StrEQ(ac, "abc") );

   // strlcat() does NOT zero-terminate when dest is not already
   INIT(ac, "abc\0defghi");
   TEST( std_strlcat(ac, "def", 1) == 4 && StrEQ(ac, "abc") );

   INIT(ac, "abc\0defghi");
   TEST( std_strlcat(ac, "def", 2) == 5 && StrEQ(ac, "abc") );

   INIT(ac, "abc\0defghi");
   TEST( std_strlcat(ac, "def", 3) == 6 && StrEQ(ac, "abc") );

   INIT(ac, "abc\0defghi");
   TEST( std_strlcat(ac, "def", 4) == 6 && StrEQ(ac, "abc") );

   INIT(ac, "abc\0defghi");
   TEST( std_strlcat(ac, "def", 5) == 6 && StrEQ(ac, "abcd") );

   INIT(ac, "abc\0defghi");
   TEST( std_strlcat(ac, "def", 6) == 6 && StrEQ(ac, "abcde") );

   INIT(ac, "abc\0defghi");
   TEST( std_strlcat(ac, "def", 7) == 6 && StrEQ(ac, "abcdef") );

   return errs;
}

static int std_strlcpy_test(void)
{
   int  n;
   char pcBuf[10];
   const char *szTmp;
   int errs = 0;

   szTmp = "a";
   n = std_strlcpy(pcBuf, szTmp, sizeof(pcBuf));
   TEST(std_strlen(szTmp)==n && 0 == std_strcmp(szTmp, pcBuf));

   szTmp = "abcdefghi";
   n = std_strlcpy(pcBuf, szTmp, sizeof(pcBuf));
   TEST(std_strlen(szTmp)==n && 0 == std_strcmp(szTmp, pcBuf));

   szTmp = "abcdefghij";
   n = std_strlcpy(pcBuf, szTmp, sizeof(pcBuf));
   TEST(std_strlen(szTmp)==n && 0 < std_strcmp(szTmp, pcBuf) && 0 == std_strcmp("abcdefghi", pcBuf));

   szTmp = "abcdefghijklmnopqrstuvwxyz";
   n = std_strlcpy(pcBuf, szTmp, sizeof(pcBuf));
   TEST(std_strlen(szTmp)==n && 0 < std_strcmp(szTmp, pcBuf) && 0 == std_strcmp("abcdefghi", pcBuf));

   INIT(pcBuf, "abcd");
   szTmp = "whatever";
   n = std_strlcpy(pcBuf, szTmp, -2);
   TEST(std_strlen(szTmp)==n && StrEQ(pcBuf, "abcd"));

   n = std_strlcpy(pcBuf, szTmp, 0);
   TEST(std_strlen(szTmp)==n && StrEQ(pcBuf, "abcd"));

   n = std_strlcpy(pcBuf, szTmp, 1);
   TEST(std_strlen(szTmp)==n && StrEQ(pcBuf, ""));

   return errs;
}

// nExp = expected results from ...end() functions
//
static int xTestChrs(const char *a, int nLenA,
                     const char *b,
                     int nExp, int nLine)
{
   const char *pcExpEnd = a + nExp;
   const char *pcExp = (nExp != nLenA ? a+nExp : NULL);
   const char *pc;

   pc = std_memchrsend(a, b, nLenA);
   if (pc != pcExpEnd) {
      printf("%s:%d: memchrsend() failed.", __FILENAME__, nLine);
      return 1;
   }

   if (nLenA != std_strlen(a)) {
      // this test not intended for str...() functions
      return 0;
   }

   pc = std_strchrs(a, b);
   if (pc != pcExp) {
      printf("%s:%d: strchrs() failed.", __FILENAME__, nLine);
      return 1;
   }

   pc = std_strchrsend(a, b);
   if (pc != pcExpEnd) {
      printf("%s:%d: strchrsend() failed.", __FILENAME__, nLine);
      return 1;
   }

   return 0;
}

#define TEST_CHRS(a,b,nExp) \
   (errs += xTestChrs(a, sizeof(a)-1, b, nExp, __LINE__))

static int std_Xchr_test(void)
{
   int errs = 0;

   const char *apsz[] = {
      "",
      "qwerSQWERq",
      "\ff\1\ff\1asdjfashfakf\ff\1",
      0
   };
   const char **ppsz = &apsz[0];
   const char *pszTest;

   while ((pszTest = *ppsz++) != 0) {
      int cbTest = std_strlen(pszTest) + 1;
      const char *psz = pszTest;
      int nChar;

      // Find all characters in the string?

      do {
         char ch    = *psz;
         char *pc   = std_strchr(pszTest, ch);
         char *pcE  = std_strchrend(pszTest, ch);
         char *pcR  = std_strrchr(pszTest, ch);
         char *pcMR = std_memrchr(pszTest, ch, cbTest);

         TEST( pc != NULL );
         if (NULL == pc) {
            continue;
         }
         TEST(*pc == ch );
         TEST( pc == pcE );
         TEST( pc >= pszTest );
         TEST( pc <= psz );

         TEST( pcR != NULL );
         if (NULL == pcR) {
            continue;
         }
         TEST(*pcR == ch );
         TEST( pcR >= psz );
         TEST( pcR < pszTest + cbTest );
         TEST( pcR == pcMR );
      } while (*psz++);

      // NOT find chars not in the string
      for (nChar = 0; nChar < 256; ++nChar) {
         char *pc = std_strchr(pszTest, nChar);

         if (pc) {
            TEST( (unsigned char)*pc == nChar );
            TEST( pc >= pszTest );
            TEST( pc < pszTest + cbTest);
            continue;  // already tested
         }

         TEST( std_strchrend(pszTest, nChar) == pszTest+cbTest-1 );
         TEST( std_strrchr(pszTest, nChar) == 0 );
         TEST( std_memrchr(pszTest, nChar, cbTest) == 0 );
      }
   }

   return errs;
}

static int std_Xchrs_test(void)
{
   int errs = 0;
   char *psz = "abc";

   TEST_CHRS("abc", "def", 3);
   TEST_CHRS("abc", "cde", 2);
   TEST_CHRS("abc", "dce", 2);
   TEST_CHRS("abc", "dec", 2);

   TEST_CHRS("Axx",     "xA", 0);
   TEST_CHRS("\101xx", "xA", 0);
   TEST_CHRS("\100xx", "xA", 1);
   TEST_CHRS("\121xx", "xA", 1);
   TEST_CHRS("\201xx", "xA\102\100\121\141\202", 1);

   // memchrs skips '\0'

   errs += xTestChrs("a\0b", 3, "b", 2, __LINE__);

   TEST( std_memchrsend(psz, "a", -1) == psz );
   TEST( std_memchrsend(psz, "", -1) == psz );
   TEST( std_memchrsend(psz, "", 3) == psz+3 );

   return errs;
}

static int std_memcmp_test(void)
{
   int errs = 0;

   TEST(0 == std_memcmp("12345", "12345", sizeof("12345")));
   TEST(0 == std_memcmp("12345", "12345", 3));
   TEST(0 == std_memcmp("12345", "12345", 0));
   TEST(0 >  std_memcmp("12245", "12345", sizeof("12345")));
   TEST(0 <  std_memcmp("12445", "12345", sizeof("12345")));
   TEST(0 <  std_memcmp("12345", "12245", sizeof("12245")));
   TEST(0 >  std_memcmp("12345", "12445", sizeof("12445")));

   TEST(std_memcmp("hi","hi", 2) == 0);
   TEST(std_memcmp("","", 0) == 0);
   TEST(std_memcmp("h","i", 1) < 0);
   TEST(std_memcmp("i","h", 1) > 0);
   TEST(std_memcmp("\1\1\1\1","\2\1\1\1", 5) < 0);
   TEST(std_memcmp("\1\1\1\1","\1\1\1\2", 5) < 0);
   TEST(std_memcmp("\xff","\1", 1) > 0);

   return errs;
}

static int std_memstr_test(void)
{
   int errs = 0;
   const char *szTmp;

   szTmp = std_memstr("abcdefg", "a", std_strlen("abcdefg") +1);
   TEST(0 == std_strncmp(szTmp, "a", 1));

   szTmp = std_memstr("abcdefg", "efg", std_strlen("abcdefg") +1);
   TEST(0 == std_strncmp(szTmp, "efg", 3));

   szTmp = std_memstr("abcdefg", "abcdefg", std_strlen("abcdefg") +1);
   TEST(0 == std_strncmp(szTmp, "abcdefg", 7));

   szTmp = std_memstr("abcdefg", "not found", std_strlen("abcdefg") +1);
   TEST(0 == szTmp);

   szTmp = std_memstr("abcdefg", "", std_strlen("abcdefg"));
   TEST(0 == std_strncmp(szTmp, "abcdefg", 7));

   return errs;
}

static int std_memchr_test(void)
{
   int i;
   int errs = 0;
   static const char p[] = "hello";
   static const struct {
      int         line;
      const char* p;
      char        c;
      int         len;
      const char* pExpect;
   } tests[] = {
      { __LINE__, p, 'h',  sizeof("hello"), p,},
      { __LINE__, p, 'e',  sizeof("hello"), p + 1,},
      { __LINE__, p, 'l',  sizeof("hello"), p + 2,},
      { __LINE__, p, 'o',  sizeof("hello"), p + 4,},
      { __LINE__, p, '\0', sizeof("hello"), p + 5,},
      { __LINE__, p, 'x',  sizeof("hello"), 0,},
      { __LINE__, p, 'x',   0,              0,},
      { __LINE__, p, 'x',  -1,              0,},
      { __LINE__, 0, 'x',   0,              0,},
      { __LINE__, 0, 'x',  -1,              0,},
   };

   for (i = 0; i < STD_ARRAY_SIZE(tests); i++) {
      const char* pGot = std_memchr(tests[i].p, tests[i].c, tests[i].len);
      if (tests[i].pExpect != pGot) {
         fprintf(stderr, __FILENAME__"(%d) test failed expected 0x%p, got 0x%p\n", 
                 tests[i].line, tests[i].pExpect, pGot);
         errs++;
      }
   }

   {
      char szBuf[] = {'a','b','c','d','\0'};

      TEST(0         == std_memchr(szBuf, 'e', sizeof(szBuf)));
      TEST(szBuf + 0 == std_memchr(szBuf, 'a', sizeof(szBuf)));
      TEST(szBuf + 1 == std_memchr(szBuf, 'b', sizeof(szBuf)));
      TEST(szBuf + 2 == std_memchr(szBuf, 'c', sizeof(szBuf)));
      TEST(szBuf + 3 == std_memchr(szBuf, 'd', sizeof(szBuf)));
      TEST(szBuf + 4 == std_memchr(szBuf, '\0', sizeof(szBuf)));
   }

   return errs;
}

static int std_memrchrbegin_test(void)
{
   int i;
   int errs = 0;
   static const char p[] = "hello";
   static const struct {
      int         line;
      const char* p;
      char        c;
      int         len;
      const char* pExpect;
   } tests[] = {
      { __LINE__, p, 'h',  sizeof("hello"), p,},
      { __LINE__, p, 'e',  sizeof("hello"), p + 1,},
      { __LINE__, p, 'l',  sizeof("hello"), p + 3,},
      { __LINE__, p, 'o',  sizeof("hello"), p + 4,},
      { __LINE__, p, '\0', sizeof("hello"), p + 5,},
      { __LINE__, p, 'x',  sizeof("hello"), p,},
      { __LINE__, p, 'x',   0,              p,},
      { __LINE__, p, 'x',  -1,              p,},
      { __LINE__, 0, 'x',   0,              0,},
      { __LINE__, 0, 'x',  -1,              0,},
   };

   for (i = 0; i < STD_ARRAY_SIZE(tests); i++) {
      const char* pGot = std_memrchrbegin(tests[i].p, tests[i].c, tests[i].len);
      if (tests[i].pExpect != pGot) {
         fprintf(stderr, __FILENAME__"(%d) test failed expected 0x%p, got 0x%p\n", 
                 tests[i].line, tests[i].pExpect, pGot);
         errs++;
      }
   }

   {
      char szBuf[] = {'a','b','c','d','\0'};

      TEST(szBuf + 0 == std_memrchrbegin(szBuf, 'z', sizeof(szBuf)));
      TEST(szBuf + 0 == std_memrchrbegin(szBuf, 'a', sizeof(szBuf)));
      TEST(szBuf + 1 == std_memrchrbegin(szBuf, 'b', sizeof(szBuf)));
      TEST(szBuf + 2 == std_memrchrbegin(szBuf, 'c', sizeof(szBuf)));
      TEST(szBuf + 3 == std_memrchrbegin(szBuf, 'd', sizeof(szBuf)));
      TEST(szBuf + 4 == std_memrchrbegin(szBuf, '\0', sizeof(szBuf)));
   }

   return errs;
}

static int std_memrchr_test(void)
{
   int i;
   int errs = 0;
   static const char p[] = "hello";
   static const struct {
      int         line;
      const char* p;
      char        c;
      int         len;
      const char* pExpect;
   } tests[] = {
      { __LINE__, p, 'h',  sizeof("hello"), p,},
      { __LINE__, p, 'e',  sizeof("hello"), p + 1,},
      { __LINE__, p, 'l',  sizeof("hello"), p + 3,},
      { __LINE__, p, 'o',  sizeof("hello"), p + 4,},
      { __LINE__, p, '\0', sizeof("hello"), p + 5,},
      { __LINE__, p, 'x',  sizeof("hello"), 0,},
      { __LINE__, p, 'x',   0,              0,},
      { __LINE__, p, 'x',  -1,              0,},
      { __LINE__, 0, 'x',   0,              0,},
      { __LINE__, 0, 'x',  -1,              0,},
   };

   for (i = 0; i < STD_ARRAY_SIZE(tests); i++) {
      const char* pGot = std_memrchr(tests[i].p, tests[i].c, tests[i].len);
      if (tests[i].pExpect != pGot) {
         fprintf(stderr, __FILENAME__"(%d) test failed expected 0x%p, got 0x%p\n", 
                 tests[i].line, tests[i].pExpect, pGot);
         errs++;
      }
   }

   {
      char szBuf[] = {'a','b','c','d','a','b','c','d','d','\0'};

      TEST(0 == std_memrchr(szBuf, 'e', sizeof(szBuf)));
      TEST(szBuf + 4 == std_memrchr(szBuf, 'a', sizeof(szBuf)));
      TEST(szBuf + 5 == std_memrchr(szBuf, 'b', sizeof(szBuf)));
      TEST(szBuf + 6 == std_memrchr(szBuf, 'c', sizeof(szBuf)));
      TEST(szBuf + 8 == std_memrchr(szBuf, 'd', sizeof(szBuf)));
      TEST(szBuf + 9 == std_memrchr(szBuf, '\0', sizeof(szBuf)));
   }

   return errs;
}

static int x_memchrend(const char *pc, char c, int nLen)
{
   return(char*)std_memchrend(pc, c, nLen) - pc;
}

static int std_memchrend_test(void)
{
   int errs = 0;
   char *pc = "abcbABCDabcd";

   TEST( x_memchrend(pc,'a',1) == 0);
   TEST( x_memchrend(pc,'a',0) == 0);
   TEST( x_memchrend(pc,'a',-1) == 0);

   TEST( x_memchrend(pc,'b',0) == 0);
   TEST( x_memchrend(pc,'b',1) == 1);
   TEST( x_memchrend(pc,'b',2) == 1);
   TEST( x_memchrend(pc,'b',3) == 1);

   {
      char szBuf[] = {'a','b','c','d','\0'};

      TEST(szBuf + 5 == std_memchrend(szBuf, 'e', sizeof(szBuf)));
      TEST(szBuf + 0 == std_memchrend(szBuf, 'a', sizeof(szBuf)));
      TEST(szBuf + 1 == std_memchrend(szBuf, 'b', sizeof(szBuf)));
      TEST(szBuf + 2 == std_memchrend(szBuf, 'c', sizeof(szBuf)));
      TEST(szBuf + 3 == std_memchrend(szBuf, 'd', sizeof(szBuf)));
      TEST(szBuf + 4 == std_memchrend(szBuf, '\0', sizeof(szBuf)));
   }

   return errs;
}

static int std_strcspn_test(void)
{
   int errs = 0;
   char szBuf[] = {'a','b','c','d','d','c','a','b','\0'};

   TEST(8 == std_strcspn(szBuf, "z"));
   TEST(0 == std_strcspn(szBuf, "a"));
   TEST(1 == std_strcspn(szBuf, "b"));
   TEST(2 == std_strcspn(szBuf, "c"));
   TEST(3 == std_strcspn(szBuf, "d"));
   TEST(8 == std_strcspn(szBuf, ""));
   TEST(8 == std_strcspn(szBuf, " "));
   TEST(0 == std_strcspn(szBuf, "abcd"));
   TEST(0 == std_strcspn(szBuf, "eabcd"));
   TEST(0 == std_strcspn(szBuf, "bacd"));
   TEST(8 == std_strcspn(szBuf, "zyxw"));
   TEST(0 == std_strcspn(szBuf, "zyxwa"));

   return errs;
}

static int std_strspn_test(void)
{
   int errs = 0;
   char szBuf[] = {'a','a','b','c','d','\0'};

   TEST(0 == std_strspn(szBuf, "z"));
   TEST(2 == std_strspn(szBuf, "a"));
   TEST(0 == std_strspn(szBuf, "b"));
   TEST(0 == std_strspn(szBuf, "c"));
   TEST(0 == std_strspn(szBuf, "d"));
   TEST(0 == std_strspn(szBuf, ""));
   TEST(0 == std_strspn(szBuf, " "));
   TEST(5 == std_strspn(szBuf, "abcd"));
   TEST(5 == std_strspn(szBuf, "eabcd"));
   TEST(5 == std_strspn(szBuf, "bacd"));
   TEST(0 == std_strspn(szBuf, "zyxw"));
   TEST(2 == std_strspn(szBuf, "zyxwa"));

   return errs;
}

// reference function
static unsigned long xSwapL(unsigned long ul)
{
   return( (0xFF000000 & ( ul << 24)) |
           (0x00FF0000 & ( ul <<  8)) |
           (0x0000FF00 & ( ul >>  8)) |
           (0x000000FF & ( ul >> 24)) );
}

// reference function
static unsigned short xSwapS(unsigned short us)
{
   return( (0xFF00 & ( us << 8)) |
           (0x00FF & ( us >> 8)) );
}

static int std_swap_test(void)
{
   int errs = 0;

   unsigned short usOne = 1;
   unsigned long  ulTest = 0xf122c344;
   unsigned short usTest = 0xf122;
   unsigned long  ulN, ulL;
   unsigned short usN, usL;

   TEST( std_swapl(ulTest) == xSwapL(ulTest) );
   // TEST( STD_SWAPS(usTest) == xSwapS(usTest) );

   if (*(uint8*)&usOne == 1) {
      // little endian
      ulN = xSwapL(ulTest);
      ulL = ulTest;
      usN = xSwapS(usTest);
      usL = usTest;
   } else {
      // big-endian (network)
      ulN = ulTest;
      ulL = xSwapL(ulTest);
      usN = usTest;
      usL = xSwapS(usTest);
   }

   TEST( std_htonl(ulTest) == ulN );
   TEST( std_ntohl(ulTest) == ulN );
   TEST( std_htons(usTest) == usN );
   TEST( std_ntohs(usTest) == usN );

   TEST( std_htolel(ulTest) == ulL );
   TEST( std_letohl(ulTest) == ulL );
   TEST( std_htoles(usTest) == usL );
   TEST( std_letohs(usTest) == usL );

   TEST(0x00000000 == std_swapl(0x00000000)); 
   TEST(0x78563412 == std_swapl(0x12345678)); 
   TEST(0x34120000 == std_swapl(0x00001234)); 

   TEST(0x0000 == std_swaps(0x0000)); 
   TEST(0x3412 == std_swaps(0x1234)); 
   TEST(0x1200 == std_swaps(0x0012));  

   return errs;
}

static int std_SwapBytes_test(void)
{
   int errs = 0;
   typedef struct {
      unsigned short us;
      char           c1;
      char           c2;
      unsigned long  ul;
      uint64         u64;
      char           ac[8];
   } EndianRec;
   const char achSizes[] = "S2LQ8";
   unsigned char buf[28];
   EndianRec a, b, c;

   // Test CopyBE / CopyLE against htonX() ad htoleX()

   typedef struct {
      const char *psz;
      int (*pfnCopy)(void *,int,const void *,int,const char *);
      unsigned long (*pfnSwapL)(unsigned long);
      unsigned short (*pfnSwapS)(unsigned short);
   } SwapTest;
   SwapTest tests[] = {
      { "BE", std_CopyBE,  std_htonl,  std_htons},
      { "LE", std_CopyLE,  std_htolel, std_htoles},
      { 0, 0, 0, 0}
   };
   SwapTest *pt;

   // initialize source record

   a.u64 = 0x0102030405060708LL;
   a.us = 0xfe23;
   a.ul = 0xe133b255;
   a.c1 = 0x01;
   a.c2 = 0x02;
   std_memmove(a.ac, "abcdefgh", 8);

   for (pt = &tests[0]; pt->pfnCopy; ++pt) {
      uint32 cbDest, nRet;
      int bSwapping = (pt->pfnSwapS(1) != 1);
      uint64 u64Expect = (bSwapping ? 0x0807060504030201LL : a.u64);

      pt->pfnCopy(&b, sizeof(b), &a, sizeof(a), achSizes);

      TEST( b.u64 == u64Expect );
      TEST( b.us == pt->pfnSwapS(a.us) );
      TEST( b.ul == pt->pfnSwapL(a.ul) );
      TEST( b.c1 == a.c1 );
      TEST( b.c2 == a.c2 );
      TEST( b.ac[0] == 'a' );
      TEST( b.ac[2] == 'c' );
      TEST( b.ac[3] == 'd' );
      TEST( b.ac[7] == 'h' );

      // Test in-place conversion

      std_memmove(&c, &a, sizeof(c));
      pt->pfnCopy(&c, sizeof(c), &c, sizeof(c), achSizes);

      TEST( 0 == std_memcmp(&b, &c, sizeof(c)) );

      // Test different destination sizes

      for (cbDest = 0; cbDest < sizeof(buf); ++cbDest) {
         uint32 cbSwap = STD_MIN(cbDest, sizeof(a));

         memset(buf, 255, sizeof(buf));
         nRet = pt->pfnCopy(buf, cbDest, &a, sizeof(a), achSizes);

         TEST( nRet == cbSwap );
         TEST( 0 == std_memcmp(buf, &b, cbSwap) );
         TEST( buf[cbSwap] == 255 );
      }

      // Test different source sizes (will not swap partial words)

      memset(buf, 255, sizeof(buf));
      nRet = pt->pfnCopy(buf, sizeof(buf), &a, 1, achSizes);
      TEST( nRet == 1 );
      TEST( 0 == std_memcmp(buf, &a, 1) );

      memset(buf, 255, sizeof(buf));
      nRet = pt->pfnCopy(buf, sizeof(buf), &a, 5, achSizes);
      TEST( nRet == 5 );
      TEST( 0 == std_memcmp(buf, &b, 4) );
      TEST( 0 == std_memcmp(buf+4, ((char*)(void*)&a)+4, 1) );

      memset(buf, 255, sizeof(buf));
      nRet = pt->pfnCopy(buf, sizeof(buf), &a, 6, achSizes);
      TEST( nRet == 6 );
      TEST( 0 == std_memcmp(buf, &b, 4) );
      TEST( 0 == std_memcmp(buf+4, ((char*)(void*)&a)+4, 2) );

      memset(buf, 255, sizeof(buf));
      nRet = pt->pfnCopy(buf, sizeof(buf), &a, 7, achSizes);
      TEST( nRet == 7 );
      TEST( 0 == std_memcmp(buf, &b, 4) );
      TEST( 0 == std_memcmp(buf+4, ((char*)(void*)&a)+4, 3) );

      memset(buf, 255, sizeof(buf));
      nRet = pt->pfnCopy(buf, sizeof(buf), &a, 8, achSizes);
      TEST( nRet == 8 );
      TEST( 0 == std_memcmp(buf, &b, 8) );

      // Test bad size string (zero length) => copy, don't swap

      nRet = pt->pfnCopy(buf, sizeof(buf), &a, sizeof(a), "");
      TEST( nRet == sizeof(a) );
      TEST( 0 == std_memcmp(buf, &a, sizeof(a)) );


      // Multiple structures
      {
         struct {
            uint32 u;
            uint16 us1;
            uint16 us2;
         } aa[5], ab[5];
         int ii;

         for (ii = 0; ii < STD_ARRAY_SIZE(aa); ++ii) {
            aa[ii].u   = ii;
            aa[ii].us1 = ii*2;
            aa[ii].us2 = ii*3;
            ab[ii].u   = pt->pfnSwapL(ii);   // L
            ab[ii].us1 = pt->pfnSwapS(ii*2); // S
            ab[ii].us2 = ii*3;               // 2
         }

         nRet = pt->pfnCopy(&aa, sizeof(aa), &aa, sizeof(aa), "LS2");
         TEST( nRet == sizeof(aa) );
         TEST( 0 == std_memcmp(aa, ab, sizeof(aa)) );
      }
   }

   return errs;
}

static int std_wstrncmp_test(void)
{
   int errs = 0;

   TEST(0 == std_wstrncmp(L("h"), L("h"), 1));
   TEST(0 > std_wstrncmp(L("h"), L("hh"), 2));
   TEST(0 == std_wstrncmp(L("h\0i"), L("h\0x"), 3));

   TEST(std_wstrncmp(L("hi"), L("hi"),22) == 0);
   TEST(std_wstrncmp(L("hia"),L("hib"),2) == 0);
   TEST(std_wstrncmp(L("hia"),L("hi"),2) == 0);

   TEST(std_wstrncmp(L("hi"), L("h"),2) > 0);
   TEST(std_wstrncmp(L("hi"), L("h"),1) == 0);
   TEST(std_wstrncmp(L("hi"), L("h"),0) == 0);
   TEST(std_wstrncmp(L("hi"), L("h"),MAX_INT32) > 0);
   TEST(std_wstrncmp(L("hi"), L("i"),MIN_INT32) == 0);

   TEST(std_wstrncmp(L("h"), L("i"),1) < 0);
   TEST(std_wstrncmp(L("i"), L("h"),9) > 0);
   TEST(std_wstrncmp(L("hi"),L("i"),1) < 0);
   TEST(std_wstrncmp(L("hi"),L("i"),2) < 0);
   TEST(std_wstrncmp(L("i"),L("hi"),3) > 0);
   TEST(std_wstrncmp(L("\1\1\1\1"),L("\2\1\1\1"),9) < 0);
   TEST(std_wstrncmp(L("\1\1\1\1"),L("\1\1\1\2"),9) < 0);
   TEST(std_wstrncmp(L("\xff"),L("\1"),2) > 0);

   return errs;
}

static int std_wstrcmp_test(void)
{
   int errs = 0;
   uint16 awc1[20];
   uint16 awc2[20];

#define INITWC_EX(awci, chk)  SetWords(awci, L(chk), STD_ARRAY_SIZE(chk)-1)

   TEST(0 == std_wstrcmp(L("h"), L("h")));
   TEST(0 > std_wstrcmp(L("h"), L("hh")));

   INITWC_EX(awc1, "h\0i");
   INITWC_EX(awc2, "h\0x");
   TEST(0 == std_wstrcmp(awc1, awc2));

   TEST(0 == std_wstrcmp(L("hi"), L("hi")));
   TEST(0 == std_wstrcmp(L(""), L("")));
   TEST(0 > std_wstrcmp(L("h"), L("i")));
   TEST(0 < std_wstrcmp(L("i"), L("h")));
   TEST(0 > std_wstrcmp(L("hi"), L("i")));
   TEST(0 < std_wstrcmp(L("i"), L("hi")));

   INITWC_EX(awc1, "\1\1\1\1");
   INITWC_EX(awc2, "\2\1\1\1");
   TEST(0 > std_wstrcmp(awc1,awc2));

   INITWC_EX(awc1, "\1\1\1\1");
   INITWC_EX(awc2, "\1\1\1\2");
   TEST(0 > std_wstrcmp(awc1,awc2));

   INITWC_EX(awc1, "\xff");
   INITWC_EX(awc2, "\1");
   TEST(0 < std_wstrcmp(awc1,awc2));

   return errs;
}

static int std_wstrchr_test(void)
{
   int errs = 0;
   AECHAR wszBuf[] = {'a','b','c','d','a',0x200,'\0'};

   TEST(0 == std_wstrchr(wszBuf, (AECHAR)'e'));
   TEST(wszBuf + 0 == std_wstrchr(wszBuf, (AECHAR)'a'));
   TEST(wszBuf + 1 == std_wstrchr(wszBuf, (AECHAR)'b'));
   TEST(wszBuf + 2 == std_wstrchr(wszBuf, (AECHAR)'c'));
   TEST(wszBuf + 3 == std_wstrchr(wszBuf, (AECHAR)'d'));
   TEST(wszBuf + 5 == std_wstrchr(wszBuf, (AECHAR)0x200));
   TEST(wszBuf + 6 == std_wstrchr(wszBuf, (AECHAR)'\0'));

   return errs;
}

static int std_wstrrchr_test(void)
{
   int errs = 0;
   AECHAR wszBuf[] = {0x200,'a','b','c','d','a','b','c','d','d','\0'};

   TEST(0 == std_wstrrchr(wszBuf, (AECHAR)'e'));
   TEST(wszBuf + 0 == std_wstrrchr(wszBuf, (AECHAR)0x200));
   TEST(wszBuf + 5 == std_wstrrchr(wszBuf, (AECHAR)'a'));
   TEST(wszBuf + 6 == std_wstrrchr(wszBuf, (AECHAR)'b'));
   TEST(wszBuf + 7 == std_wstrrchr(wszBuf, (AECHAR)'c'));
   TEST(wszBuf + 9 == std_wstrrchr(wszBuf, (AECHAR)'d'));
   TEST(wszBuf + 10 == std_wstrrchr(wszBuf, (AECHAR)'\0'));

   return errs;
}

int main(void)
{
   return
   std_getversion_test() +
   std_misc_test() +
   std_wstrlen_test() +
   std_wstrlcpy_test() +
   std_wstrlcat_test() +
   std_strcmp_test() +
   std_strncmp_test() +
   std_stricmp_test() +
   std_strnicmp_test() +
   std_strbegins_test() +
   std_stribegins_test() +
   std_strends_test() +
   std_striends_test() +
   std_strstr_test() +
   std_strchr_test() +
   std_strrchr_test() +
   std_strchrend_test() +
   std_strchrsend_test() +
   std_strlcat_test() +
   std_strlcpy_test() +
   std_Xchr_test() +
   std_Xchrs_test() +
   std_memcmp_test() +
   std_memstr_test() +  
   std_memchr_test() + 
   std_memrchr_test() + 
   std_memrchrbegin_test() + 
   std_memchrend_test() +
   std_strspn_test() +
   std_strcspn_test() +
   std_swap_test() +
   std_SwapBytes_test() +
   std_wstrncmp_test() +
   std_wstrcmp_test() +
   std_wstrchr_test() +
   std_wstrrchr_test();
}

