/*
=======================================================================

FILE:         std_scanul.c

SERVICES:     ASCII -> unsigned long conversion

=======================================================================
        Copyright (c) 2005 Qualcomm Technologies Incorporated.
               All Rights Reserved.
            QUALCOMM Proprietary and Confidential
=======================================================================
*/

#include <AEEStdDef.h>   // #define MAX_UINT32 0xFFffFFff
#include <AEEstd.h>      // #define STD_BETWEEN(x,a,b)    ( (unsigned) ((x)-(a)) <= (unsigned) ((b) - (a)) )

#define IsAsciiDigit(c)   STD_BETWEEN(c, '0', '9'+1)
#define IsAsciiAlpha(c)   STD_BETWEEN( (c)|32, 'a', 'z'+1)
#define IsAsciiHexDigit(c) (STD_BETWEEN(c, '0', '9'+1) || STD_BETWEEN( (c)|32, 'a', 'f'+1))
#define AsciiToLower(c)   ((c)|32)

uint64 std_scanux( const char *  pchBuf,
                   int           nRadix,
                   const char ** ppchEnd,
                   int *         pnError )
{
   int nError = 0;   // SUCCESS
   const unsigned char *pch = (const unsigned char*)pchBuf;
   uint64 ullVal = 0;
   uint64 ullOverflow = 0;
   int nLastDigMax = 0;
   unsigned char c;

   if (nRadix < 0 || nRadix > 36 || nRadix == 1) {
      nError = STD_BADPARAM;
      goto done;
   }

   // Skip whitespace
   
   while ( (c = *pch) == ' ' || c == '\t' ) {
      ++pch;
   }

   // Scan sign
   
   if (c == '-') {
      nError = STD_NEGATIVE;
      ++pch;
   } else if (c == '+') {
      ++pch;
   }

   // Read optional prefix
   if ((0 == nRadix || 16 == nRadix) &&
       '0' == pch[0] && 'x' == AsciiToLower(pch[1]) && 
       IsAsciiHexDigit(pch[2])) {
      pch += 2;
      nRadix = 16;
   }
   
   if (0 == nRadix && '0' == pch[0]) {
      nRadix = 8;
   }

   if (0 == nRadix) {
      nRadix = 10;
   }

   /*-------------------------------------------------------------------------- 
      Compute the overflow threshold, which is the value of ullVal that would
      lead to overflow if another digit were added to it.  Normally, overflow
      would occur if ullVal * nRadix + nDigit > ullMaxVal, but that can't be
      done with 64-bit arithmetic.  Instead we transform this to
            ullVal > (ullMaxVal - nDigit) / nRadix
        OR  ullVal > (ullMaxVal / nRadix) - (nDigit / nRadix))
        OR  ullVal > A - Bn
      where A = (ullMaxVal / nRadix) is a constant, and
            Bn = (nDigit / nRadix) depends on the current scanned digit.
    
      This means that we might overflow whenever ullVal is greater than A and
      we have scanned another digit. However, if ullVal is equal to A, then
      we may or may not overflow based on the digit scanned.
    
      Checking for overflow this way, we can avoid doing the 64-bit math for
      each digit by using this one-time computed value.
   --------------------------------------------------------------------------*/

   // Use the C preprocessor to compute the overflow values for common
   //  radices. Do (relatively expensive) division in code for the rest.
   if (10 == nRadix) {
      ullOverflow = MAX_UINT64/10;
      nLastDigMax = (int) (MAX_UINT64 - ((MAX_UINT64/10) * 10));
   } else if (16 == nRadix) {
      ullOverflow = MAX_UINT64/16;
      nLastDigMax = (int) (MAX_UINT64 - ((MAX_UINT64/16) * 16));
   } else if (8 == nRadix) {
      ullOverflow = MAX_UINT64/8;
      nLastDigMax = (int) (MAX_UINT64 - ((MAX_UINT64/8) * 8));
   } else {
      ullOverflow = MAX_UINT64/nRadix;
      nLastDigMax = (int) (MAX_UINT64 - (ullOverflow * nRadix));
   }


   // Read digits
   {
      const unsigned char* pchStartOfDigits = pch;

      for (;;) {
         int nDigit;

         c = *pch;

         if (IsAsciiDigit(c)) {
            nDigit = c - '0';
         } else if (IsAsciiAlpha(c)) {
            nDigit = (AsciiToLower(c) - 'a') + 10;
         } else {
            break;
         }

         if (nDigit >= nRadix) {
            break;
         }

         ++pch;

         if ((ullVal > ullOverflow) ||
             ((ullVal == ullOverflow) && (nDigit > nLastDigMax))) {
            ullVal = MAX_UINT64;
            nError = STD_OVERFLOW;
            break;
         }

         ullVal = ullVal * nRadix + nDigit;
      }

      if (pch == pchStartOfDigits) {
         pch = (const unsigned char*)pchBuf;
         nError = STD_NODIGITS;
      }
   }

 done:
   if (pnError) {
      *pnError = nError;
   }

   if (ppchEnd) {
      *ppchEnd = (const char*)pch;
   }

   return ullVal;
}

uint32 std_scanul( const char *  pchBuf,
                   int           nRadix,
                   const char ** ppchEnd,
                   int *         pnError )
{
   uint64 ullVal = 0;
   uint32 ulVal = 0;
   int nError = 0;

   ullVal = std_scanux(pchBuf, nRadix, ppchEnd, &nError);

   ulVal = (unsigned long) ullVal;
   
   if (ullVal > MAX_UINT32) {
      nError = STD_OVERFLOW;
      ulVal = MAX_UINT32;
   } 
   else if (nError == STD_NEGATIVE) {
      ulVal = (uint32) -(int32)ulVal;
      if (ulVal == 0) {
         nError = 0; // SUCCESS
      }
   }

   if (pnError) {
      *pnError = nError;
   }

   return ulVal;
}

uint64 std_scanull( const char *  pchBuf,
                    int           nRadix,
                    const char ** ppchEnd,
                    int *         pnError )
{
   uint64 ullVal = 0;
   int nError = 0;

   ullVal = std_scanux(pchBuf, nRadix, ppchEnd, &nError);

   if (nError == STD_NEGATIVE) {
      ullVal = (uint64) -(int64)ullVal;
      if (ullVal == 0) {
         nError = 0; // SUCCESS
      }
   }

   if (pnError) {
      *pnError = nError;
   }

   return ullVal;
}


/* =======================================================================

   Sample code:

   =======================================================================
 
   std_scanlong()

   This function is similar to scanul(), but it returns only valid (signed)
   long values: (MINLONG ... MAXLONG).   Values outside that ranges result
   in STD_OVERFLOW error codes.  STD_NEGATIVE is not returned.
   
   
   long std_scanlong( const char *psz, int nRadix, char **ppszEnd, int *pnResult)
   {
      int nVal;
   
      int err = std_scanul(psz, nRadix, ppszEnd, (unsigned long*)&nVal);
   
      if (STD_NEGATIVE == err) {
         err = (nVal < 0 ? AEE_SUCCESS : STD_OVERFLOW);
      } else if (AEE_SUCCESS == err && nVal < 0) {
         err = STD_OVERFLOW;
      }
   
      *pnResult = nVal;
      return err;
   }

   =======================================================================

   strtoul()

   unsigned long strtoul( const char *pch, const char **ppchEnd, int nRadix)
   {
      unsigned long ul;
      
      int err = std_scanul(pch, nRadix, ppchEnd, &ul);
      
      if (err == STD_NEGATIVE && ((int)ul) < 0) {
         //
         // strtoul() range limits are ill-defined for signed values
         //
         err = AEE_SUCCESS;
      }
      
      if (err == STD_NEGATIVE || err == STD_OVERFLOW) {
         ul = ULONG_MAX;
         errno = ERANGE;
      } else if (err == STD_BADPARAM) {
         ul = 0;
         errno = EINVAL;
      } else {
         // http://www.opengroup.org/onlinepubs/007908799/xsh/strtoul.html
         // "Because 0 and ULONG_MAX are returned on error and are also valid
         //  returns on success, an application wishing to check for error
         //  situations should set errno to 0, then call strtoul(), then check
         //  errno."
      }
      return ul;
   }
     
   =======================================================================
*/
