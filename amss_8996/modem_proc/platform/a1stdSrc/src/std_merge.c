#include "AEEstd.h"

void std_merge(void* vpDst, int nDst, 
               const void* vpA, int nA,
               const void* vpB, int nB,
               int nElemWidth,
               int (*pfnCompare)(void*, const void*, const void*),
               void* pCompareCx)
{
   byte* pDst = vpDst;
   const byte* pA = vpA;
   const byte* pB = vpB;

   while (nDst > 0 && nA > 0 && nB > 0) {
      if (pfnCompare(pCompareCx, pA, pB) <= 0) {
         std_memmove(pDst, pA, nElemWidth);
         pA += nElemWidth;
         nA--;
      } else {
         std_memmove(pDst, pB, nElemWidth);
         pB += nElemWidth;
         nB--;
      }
      pDst += nElemWidth;
      nDst--;
   }

   if (nDst > 0) {
      if (nA > 0) {
         nA = STD_MIN(nA, nDst);
         std_memmove(pDst, pA, nA * nElemWidth);
      } else {
         nB = STD_MIN(nB, nDst);
         std_memmove(pDst, pB, nB * nElemWidth);
      }
   }
}

