#include <stdio.h>
#include <stdlib.h>
#include "AEEstd.h"
#include "AEEStdErr.h"

//
// Useful Macros
// 
#define FAILED(b)    ( (b) != AEE_SUCCESS ? TRUE : FALSE )
#define VALIDATE_TEST_RESULT(p, n, l) \
   if (FAILED((n))) { \
      printf(#p " Failed!\n" ); \
      goto l; \
   }

#define RAND_SIZE  10
#define RAND_SIZE2 (256*2)

static int TestRand1(int numtries)
{
   unsigned mask;

   if (0 == std_rand_next(0)) {
      fprintf(stderr, "std_rand_next(0) = 0\n");
      return 1;
   }

   for (mask = 0x01; mask != 0; mask <<= 1) {
      unsigned clear = 0, set = 0, next = 0;
      int i;

      for (i = 0; i < numtries; i++) {
         next = std_rand_next(next);
         if (mask & next) {
            set++;
         } else {
            clear++;
         }
      }
      if (0 == set) {
         fprintf(stderr, "zero set bits with mask 0x%x\n", mask);
         return 1;
      }
      if (0 == clear) {
         fprintf(stderr, "zero clear bits with mask 0x%x\n", mask);
         return 1;
      }
      if (set/clear != 1 && clear/set != 1) {
         fprintf(stderr, "set/clear == %d, clear/set == %d\n", set/clear, clear/set);
         return 1;
      }
   }

   return 0;
}

static int TestRand2(void)
//
// Test for the std_rand() function.
// Verifies that random numbers are returned.  In addition verifies that the
// distribution of the "random" number is mostly even.
// 
{
   int nError = AEE_SUCCESS;
   byte ValueList[RAND_SIZE] = {0};
   int nI = 0;
   byte RanList[RAND_SIZE2] = {0};
   byte RanCount[256] = {0};
   int nZeros = 0;
   int nMax = 0;
   int nMaxVal = 0;

   // 
   // Testing Unique Consecutive Numbers
   // 

   std_rand(0, ValueList, RAND_SIZE);

   for (nI = 0; nI < RAND_SIZE; nI++) {
      if (ValueList[0] != ValueList[nI]) {
         break;
      }
   }

   if (nI >= RAND_SIZE) {
      printf("Numbers didn't vary from %d\n", ValueList[0]);
      nError = AEE_EFAILED;
      goto Cleanup;
   }


   //
   // Testing Randomness
   // 
   
   //do simple statistical histogram
   std_rand(1024, RanList, RAND_SIZE2);
   for (nI = 0; nI < RAND_SIZE2; nI++) {
      ++RanCount[RanList[nI]];
   }

   // Two checks here:
   //    - check that one number doesn't occur too often
   //    - check if too many numbers don't occur
   for (nI = 0; nI < STD_ARRAY_SIZE(RanCount); nI++) {
      if (RanCount[nI] > nMax) {
         nMax = RanCount[nI];
         nMaxVal = nI;
      }
      if (RanCount[nI] == 0) {
         ++nZeros;
      }
   }

   // if a single number occurs more than approx 10% of the time...
   if (nMax > (STD_ARRAY_SIZE(RanCount) / 10)) {
      printf("%d occured %d times!", nMaxVal, nMax);
      nError = AEE_EFAILED;
      goto Cleanup;
   }

   // check if significant number of values never occured
   if (nZeros > 50) {
      printf("Uneven distribution %d numbers never occured", nZeros);
      nError = AEE_EFAILED;
      goto Cleanup;
   }

Cleanup:
   return nError;
}


int main(int argc, const char**argv)
{
   int nError = AEE_SUCCESS;
   int nNumTries = 512;

   if (argc > 1) {
      nNumTries = atoi(argv[1]);
   }

   nError = TestRand1(nNumTries);
   VALIDATE_TEST_RESULT( TestRand1, nError, Cleanup );

   nError = TestRand2();
   VALIDATE_TEST_RESULT( TestRand2, nError, Cleanup );

Cleanup:
   return nError;
}

