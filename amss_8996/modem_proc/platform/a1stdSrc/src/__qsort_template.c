
#ifndef __QSORT_WIDTH
#define __QSORT_WIDTH args->nElemWidth
#endif

#ifndef __QSORT_VALSWAP

# ifdef __QSORT_INPLACE

# define __VALSWAP __QSORT##_valswap

/* TODO: optimize */
static void __VALSWAP(byte* p1, byte* p2, int nWidth)
{
   while (nWidth--) {
      byte t = *p1;
      *p1++ = *p2;
      *p2++ = t;
   }
}

#  define __QSORT_VALSWAP(_dst, _src) __QSORT##_valswap((_dst), (_src), __QSORT_WIDTH)

# else

#  define __QSORT_VALSWAP(_dst, _src) \
   do {                                                                 \
      __QSORT_VALMOVE(args->pPivot, (_dst));                            \
      __QSORT_VALMOVE((_dst), (_src));                                  \
      __QSORT_VALMOVE((_src), (args->pPivot));                          \
   } while (0); 

# endif

#endif

#define __GE(x, y)     (args->pfnCompare(args->pCompareCx, (x), (y)) >= 0)
#define __LE(x, y)     (args->pfnCompare(args->pCompareCx, (x), (y)) <= 0)
#define __PDIFF(x, y)  (((x)-(y))/__QSORT_WIDTH)
#define __PMATH(x, v)  ((x)+((v)*(int)__QSORT_WIDTH))
#define __PDEC(x)      (x)=__PMATH((x),-1)
#define __PINC(x)      (x)=__PMATH((x),1)

static void __QSORT(byte* pElems, int nElems, const __qsort_args* args)
{
   byte* pLeft = pElems;
   byte* pPivot;
   
   /* Choose a pivot, pseudo randomly (all our sizes are going to 
      be relatively prime with Marsenne 31).
      Once we've chosen a pivot, move it to pLeft. */
   pPivot = __PMATH(pElems, 0x7fffffff%nElems);
   __QSORT_VALSWAP(pLeft, pPivot);

#ifndef __QSORT_INPLACE
      /* save pivot value, make a hole */
      __QSORT_VALMOVE(args->pPivot, pLeft);
      pPivot = args->pPivot;
#else
      /* we're gonna do it in place */
      pPivot = pLeft;
#endif
 
   {
      byte* pRight = __PMATH(pElems, nElems - 1);

      for (;;) {
         /* find the next thing from the right that needs to be on 
            the left of the pivot value */
         for (;;) {

            if (pLeft == pRight) {
               goto done;
            }
            if (__GE(pRight, pPivot)) {
               __PDEC(pRight);
            } else {
               break;
            }

         }

         /* if we haven't met left, swap with left, 
            this moves the pivot value to pRight */
#ifdef __QSORT_INPLACE
         __QSORT_VALSWAP(pLeft, pRight);
         pPivot = pRight;
#else
         __QSORT_VALMOVE(pLeft, pRight);
#endif
         __PINC(pLeft);
      
         /* find the next thing from the left that needs to be on the
            right of the pivot value */
         for (;;) {

            if (pLeft == pRight) {
               goto done;
            }
            if (__LE(pLeft, pPivot)) {
               __PINC(pLeft);
            } else {
               break;
            }

         }

         /* swap left and right, moves pivot back to pLeft */
#ifdef __QSORT_INPLACE
         __QSORT_VALSWAP(pRight, pLeft);
         pPivot = pLeft;
#else
         __QSORT_VALMOVE(pRight, pLeft);
#endif
         __PDEC(pRight);
      }
   }
 done:

#ifndef __QSORT_INPLACE
   /* restore pivot value to middle */
   __QSORT_VALMOVE(pLeft, args->pPivot);
#endif
   
   {
      int nLeft = __PDIFF(pLeft, pElems);

      if (nLeft > 1) {
         __QSORT(pElems, nLeft, args);
      }
   
      if (nElems - nLeft > 2) {
         __QSORT(__PMATH(pElems, nLeft + 1), nElems - nLeft - 1, args);
      }
   }

}

#undef __GE
#undef __LE
#undef __PDIFF
#undef __PMATH
#undef __PDEC
#undef __PINC

