#include <stdio.h>
#include <limits.h>
#include "AEEsmath.h"

#ifndef __FILENAME__
#define __FILENAME__ __FILE__
#endif

#define TEST(x) \
   do { if (!(x)) { \
      printf(__FILENAME__ ":%d: test failed: %s\n", __LINE__, #x); \
      errs++; \
   } } while (0)


int main(int argc, char **argv)
{
   int errs = 0;

   // Add
   
   TEST( smath_Add(3,4) == 7 );
   TEST( smath_Add(INT_MIN, INT_MIN) == INT_MIN );
   TEST( smath_Add(INT_MIN, -1)      == INT_MIN );
   TEST( smath_Add(INT_MIN, 1)       == INT_MIN+1 );

   TEST( smath_Add(INT_MAX, INT_MAX) == INT_MAX );
   TEST( smath_Add(INT_MAX, 1)       == INT_MAX );
   TEST( smath_Add(INT_MAX, -1)      == INT_MAX-1 );

   TEST( smath_Add(INT_MAX, INT_MIN) == INT_MAX+INT_MIN );

   // Sub
   
   TEST( smath_Sub(0, INT_MIN)       == INT_MAX );   // -INT_MIN > INT_MAX
   TEST( smath_Sub(0, INT_MAX)       == -INT_MAX );
   TEST( smath_Sub(INT_MIN, INT_MIN) == 0 );
   TEST( smath_Sub(INT_MAX, INT_MAX) == 0 );

   // Mul
   
   TEST( smath_Mul(0, INT_MIN)       == 0 );
   TEST( smath_Mul(0, INT_MAX)       == 0 );
   TEST( smath_Mul(INT_MAX, INT_MAX) == INT_MAX );
   TEST( smath_Mul(INT_MIN, INT_MIN) == INT_MAX );
   TEST( smath_Mul(INT_MAX, INT_MIN) == INT_MIN );

   TEST( smath_Mul(INT_MAX/2, 3)     == INT_MAX );
   TEST( smath_Mul(INT_MIN/2, 3)     == INT_MIN );

#if INT_MAX == 0x7fffffff
   TEST( smath_Mul(0xf000, 0xf000)   == INT_MAX );
   TEST( smath_Mul(-0xf000, 0xf000)  == INT_MIN );
#endif

#if INT_MAX >= 0x7fffffff
   TEST( smath_Mul(46340, 46340)     == 2147395600 );
#endif
   
   return 0;
}
