#define STD_QSORT_BIG_PIVOT 3 /* I have unit tests for arbitrary 3 and 5 */
#include "std_qsort.c"

#include <stdio.h> /* for printf() */


#undef __TEST_PRINT
#define __TEST_PRINT test_print_byte
#undef  __TEST_PRINT_FORMAT
#define __TEST_PRINT_FORMAT "%u, "
#undef __TEST_TYPE
#define __TEST_TYPE byte
#undef __TEST_COMPARE
#define __TEST_COMPARE test_compare_byte
#undef __TESTS
#define __TESTS tests_byte

#include "__sort_q_template.c"

#undef __TEST_PRINT
#define __TEST_PRINT test_print_uint16
#undef  __TEST_PRINT_FORMAT
#define __TEST_PRINT_FORMAT "%u, "
#undef __TEST_TYPE
#define __TEST_TYPE uint16
#undef __TEST_COMPARE
#define __TEST_COMPARE test_compare_uint16
#undef __TESTS
#define __TESTS tests_uint16
#include "__sort_q_template.c"

#undef __TEST_PRINT
#define __TEST_PRINT test_print_uint32
#undef  __TEST_PRINT_FORMAT
#define __TEST_PRINT_FORMAT "%lu, "
#undef __TEST_TYPE
#define __TEST_TYPE uint32
#undef __TEST_COMPARE
#define __TEST_COMPARE test_compare_uint32
#undef __TESTS
#define __TESTS tests_uint32
#include "__sort_q_template.c"


#undef __TEST_PRINT
#define __TEST_PRINT test_print_uint64
#undef  __TEST_PRINT_FORMAT
#define __TEST_PRINT_FORMAT "%llu, "
#undef __TEST_TYPE
#define __TEST_TYPE uint64
#undef __TEST_COMPARE
#define __TEST_COMPARE test_compare_uint64
#undef __TESTS
#define __TESTS tests_uint64
#include "__sort_q_template.c"

static void test_print_5(void* p, int num)
{
   int i;
   byte* vals = (byte*)p;
   for (i = 0; i < num; i++) {
      printf("{%u,%u,%u,%u,%u,},", 
             vals[0],
             vals[1],
             vals[2],
             vals[3],
             vals[4]);
   }
   printf("\n");
}

static int test_compare_5(void* p, const void* u1, const void* u2)
{
   return std_memcmp(u1, u2, 5);
}

static struct {
   char   test[10][5];
   char   expect[10][5];
   int    num;
} tests_5[] = {
   {
      {
         {0,0,0,0,0,}, {0,0,0,0,3,}, {0,0,0,0,0,}, {0,0,0,0,1,}, {0,0,0,0,2,},
      },
      {
         {0,0,0,0,0,}, {0,0,0,0,0,}, {0,0,0,0,1,}, {0,0,0,0,2,}, {0,0,0,0,3,},
      },
      5,
   },
   {
      {
         {0,0,0,0,4,}, {0,0,0,0,3,}, {0,0,0,0,2,}, {0,0,0,0,1,}, {0,0,0,0,0,},
      },
      {
         {0,0,0,0,0,}, {0,0,0,0,1,}, {0,0,0,0,2,}, {0,0,0,0,3,}, {0,0,0,0,4,},
      },
      5,
   },
   {
      {
         {0,0,0,0,0,}, {0,0,0,0,1,},
      },
      {
         {0,0,0,0,0,}, {0,0,0,0,1,},
      },
      2,
   },
   {
      {
         {0,0,0,0,1,}, {0,0,0,0,0,},
      },
      {
         {0,0,0,0,0,}, {0,0,0,0,1,},
      },
      2,
   },
};

static int test_compare_3(void* p, const void* u1, const void* u2)
{
   return std_memcmp(u1, u2, 3);
}

static void test_print_3(void* p, int num)
{
   int i;
   byte* vals = (byte*)p;
   for (i = 0; i < num; i++) {
      printf("{%u,%u,%u,},", 
             vals[(i*3)+0],
             vals[(i*3)+1],
             vals[(i*3)+2]);
   }
   printf("\n");
}

static struct {
   char   test[10][3];
   char   expect[10][3];
   int    num;
} tests_3[] = {
   {
      {
         {0,0,0,}, {0,0,3,}, {0,0,0,}, {0,0,1,}, {0,0,2,},
      },
      {
         {0,0,0,}, {0,0,0,}, {0,0,1,}, {0,0,2,}, {0,0,3,},
      },
      5,
   },
   {
      {
         {0,0,4,}, {0,0,3,}, {0,0,2,}, {0,0,1,}, {0,0,0,},
      },
      {
         {0,0,0,}, {0,0,1,}, {0,0,2,}, {0,0,3,}, {0,0,4,},
      },
      5,
   },
   {
      {
         {0,0,0,}, {0,0,1,},
      },
      {
         {0,0,0,}, {0,0,1,},
      },
      2,
   },
   {
      {
         {0,0,1,}, {0,0,0,},
      },
      {
         {0,0,0,}, {0,0,1,},
      },
      2,
   },
};


static int test_compare_2(void* p, const void* u1, const void* u2)
{
   return std_memcmp(u1, u2, 2);
}

static void test_print_2(void* p, int num)
{
   int i;
   byte* vals = (byte*)p;
   for (i = 0; i < num; i++) {
      printf("{%u,%u,},", 
             vals[(i*2)],
             vals[(i*2)+1]);
   }
   printf("\n");
}

static struct {
   char   unused;
   char   test[15];
   char   expect[15];
   int    num;
} tests_2[] = {
   {
      0,
      {0, 0, 0, 3, 0, 1, 0, 2, 0, 4},
      {0, 0, 0, 1, 0, 2, 0, 3, 0, 4},
      5,
   },
};

int main(void)
{
   int nErrs = 0;
   int i;

   for (i = 0; i < STD_ARRAY_SIZE(tests_byte); i++) {

      std_qsort(tests_byte[i].test, tests_byte[i].num, 
                sizeof(byte), test_compare_byte, 0);

      if (std_memcmp(tests_byte[i].test, tests_byte[i].expect, 
                     tests_byte[i].num*sizeof(byte))) {
         printf("test_2 %d:\nexpected ", i);
         test_print_byte(tests_byte[i].expect, tests_byte[i].num);
         printf("got ");
         test_print_byte(tests_byte[i].test, tests_byte[i].num);
         nErrs++;
      }
   }

   for (i = 0; i < STD_ARRAY_SIZE(tests_uint16); i++) {

      std_qsort(tests_uint16[i].test, tests_uint16[i].num, 
                sizeof(uint16), test_compare_uint16, 0);

      if (std_memcmp(tests_uint16[i].test, tests_uint16[i].expect, 
                     tests_uint16[i].num*sizeof(uint16))) {
         printf("test_uint16 %d:\nexpected ", i);
         test_print_uint16(tests_uint16[i].expect, tests_uint16[i].num);
         printf("got ");
         test_print_uint16(tests_uint16[i].test, tests_uint16[i].num);
         nErrs++;
      }
   }

   for (i = 0; i < STD_ARRAY_SIZE(tests_uint32); i++) {

      std_qsort(tests_uint32[i].test, tests_uint32[i].num, 
                sizeof(uint32), test_compare_uint32, 0);

      if (std_memcmp(tests_uint32[i].test, tests_uint32[i].expect, 
                     tests_uint32[i].num*sizeof(uint32))) {
         printf("test_uint32 %d:\nexpected ", i);
         test_print_uint32(tests_uint32[i].expect, tests_uint32[i].num);
         printf("got ");
         test_print_uint32(tests_uint32[i].test, tests_uint32[i].num);
         nErrs++;
      }
   }

   for (i = 0; i < STD_ARRAY_SIZE(tests_uint64); i++) {

      std_qsort(tests_uint64[i].test, tests_uint64[i].num, 
                sizeof(uint64), test_compare_uint64, 0);

      if (std_memcmp(tests_uint64[i].test, tests_uint64[i].expect, 
                     tests_uint64[i].num*sizeof(uint64))) {
         printf("test_uint64 %d:\nexpected ", i);
         test_print_uint64(tests_uint64[i].expect, tests_uint64[i].num);
         printf("got ");
         test_print_uint64(tests_uint64[i].test, tests_uint64[i].num);
         nErrs++;
      }
   }

   for (i = 0; i < STD_ARRAY_SIZE(tests_3); i++) {
      
      std_qsort(tests_3[i].test, tests_3[i].num, 
                3, test_compare_3, 0);

      if (std_memcmp(tests_3[i].test, tests_3[i].expect, 
                     tests_3[i].num*3)) {
         printf("test_3 %d:\nexpected ", i);
         test_print_3(tests_3[i].expect, tests_3[i].num);
         printf("got ");
         test_print_3(tests_3[i].test, tests_3[i].num);
         nErrs++;
      }
   }

   for (i = 0; i < STD_ARRAY_SIZE(tests_5); i++) {
      
      std_qsort(tests_5[i].test, tests_5[i].num, 
                5, test_compare_5, 0);

      if (std_memcmp(tests_5[i].test, tests_5[i].expect, 
                     tests_5[i].num*5)) {
         printf("test_5 %d:\nexpected ", i);
         test_print_5(tests_5[i].expect, tests_5[i].num);
         printf("got ");
         test_print_5(tests_5[i].test, tests_5[i].num);
         nErrs++;
      }
   }


   for (i = 0; i < STD_ARRAY_SIZE(tests_2); i++) {

      std_qsort(tests_2[i].test, tests_2[i].num, 
                2, test_compare_2, 0);

      if (std_memcmp(tests_2[i].test, tests_2[i].expect, 
                     tests_2[i].num*sizeof(byte))) {
         printf("test_byte %d:\nexpected ", i);
         test_print_2(tests_2[i].expect, tests_2[i].num);
         printf("got ");
         test_print_2(tests_2[i].test, tests_2[i].num);
         nErrs++;
      }
   }

   {
      static int bigun[2048];

      for (i = 0; i < STD_ARRAY_SIZE(bigun); i++) {
         bigun[i] = 0x7ffff%(i+1);
      }
      std_qsort(bigun, STD_ARRAY_SIZE(bigun), 
                sizeof(bigun[0]), test_compare_uint32, 0);
      {
         for (i = 1; i < STD_ARRAY_SIZE(bigun); i++) {
            if (bigun[i] < bigun[i-1]) {
               printf("bigun out of order bigun[%d] == %d\n", i, bigun[i]);
               nErrs++;
               break;
            }
         }
      }
   }

   return nErrs;
}



