/*
=======================================================================
        Copyright (c) 2009 Qualcomm Technologies Incorporated.
               All Rights Reserved.
            QUALCOMM Proprietary and Confidential
=======================================================================
*/

#include <stdio.h>
#include <stdlib.h>
#include "AEEstd.h"
#include "AEEStdErr.h"
#include "std_dtoa.h"

#ifndef __FILENAME__
#define __FILENAME__ __FILE__
#endif

//
// Constants
// 
#define DOUBLES_EQ_THRESHOLD     1e-14

//
// Useful Macros
// 
#define IS_NULL(p)               ( (p) == NULL )
#define NOT_NULL(p)              !IS_NULL(p)
#define SCANDFUNC                std_scand
#define ASSERT(cond)  \
   if( !(cond) ) \
   { \
      printf( __FILENAME__ ":%d: ASSERT failed: %s\n", __LINE__, #cond ); \
      nFailed++; \
   }
#define RUNTEST(args)               ASSERT( (RunTest args) == AEE_SUCCESS )
#define DOUBLES_EQ(d1, d2)          ((d1) == (d2))
#define DOUBLES_NZ_EQ_APPROX(d1, d2) \
   ( (d1) > (d2) ? (((d1) - (d2))/d1 <= DOUBLES_EQ_THRESHOLD) : \
   (((d2) - (d1))/d2 <= DOUBLES_EQ_THRESHOLD) )

static __inline boolean fp_is_nan( double dNumber )
//
// This function checks if a given floating point number equals NaN
// 
{
   int nError = AEE_SUCCESS;
   FloatingPointType NumberType = FP_TYPE_UNKOWN;

   nError = fp_check_special_cases( dNumber, &NumberType );
   if( AEE_SUCCESS == nError )
   {
      if( FP_TYPE_NAN == NumberType )
      {
         return TRUE;
      }
   }

   return FALSE;
}

static __inline boolean fp_is_inf( double dNumber )
//
// This function checks if a given floating point number equals INIFINITY
// 
{
   int nError = AEE_SUCCESS;
   FloatingPointType NumberType = FP_TYPE_UNKOWN;

   nError = fp_check_special_cases( dNumber, &NumberType );
   if( AEE_SUCCESS == nError )
   {
      if( ( FP_TYPE_NEGATIVE_INF == NumberType ) ||
          ( FP_TYPE_POSITIVE_INF == NumberType ) )
      {
         return TRUE;
      }
   }

   return FALSE;
}

static __inline boolean CompareDoubles( double d1, double d2 )
{
   if( fp_is_nan(d1) )
   {
      return fp_is_nan(d2);
   }
   else if( fp_is_inf(d2) )
   {
      return DOUBLES_EQ(d1, d2);
   }
   else if( 0.0 == d1 )
   {
      return ( 0.0 == d2 );
   }
   else if( 0.0 == d2 )
   {
      return ( 0.0 == d1 );
   }
   else
   {
      return DOUBLES_NZ_EQ_APPROX(d1, d2);
   }
}

static int RunTest( const char* cpszStr, double dExp, const char* cpszExpStrEnd )
{
   int nError = AEE_SUCCESS;
   double dVal = 0;
   char* pszStrEnd = NULL;

   dVal = SCANDFUNC( cpszStr, (const char**)&pszStrEnd );
   if( !CompareDoubles( dVal, dExp ) )
   {
      printf( "Expected Value = %g, Actual Value = %g\n", dExp, dVal );
      nError = AEE_EFAILED;
      goto Cleanup;
   }
   if( NOT_NULL( cpszExpStrEnd ) && 
       ( std_strcmp( pszStrEnd, cpszExpStrEnd ) ) )
   {
      printf( "Expected end of string = %s, Actual = %s\n", cpszExpStrEnd, 
              pszStrEnd );
      nError = AEE_EFAILED;
      goto Cleanup;
   }

Cleanup:

   return nError;
}

int main(int argc, char **argv)
{
   int nFailed = 0;
   uint64 ulInf = STD_DTOA_FP_POSITIVE_INF;
   uint64 ulNan = STD_DTOA_FP_QNAN;
   double dInf = UINT64_TO_DOUBLE( ulInf );

   // Denormalized numbers
   RUNTEST( ("0x1p-1023",
             1.11253692925360069154511635866620203210960799023116591527666e-308,
             "\0") );
   RUNTEST( ("0x0.8p-1022",
             1.11253692925360069154511635866620203210960799023116591527666e-308,
             "\0") );

   // Overflow/Underflow tests
   RUNTEST( ("0x1.0p1033", dInf, "\0") );
   RUNTEST( ("132.223e308", dInf, "\0") );
   RUNTEST( ("-1.223e309", -dInf, "\0") );
   RUNTEST( ("0x1.0p-1099", 0.0, "\0") );

   // Mixing Radix 10 and 2
   RUNTEST( ("12.213p23", 12.213, "p23") );
   RUNTEST( ("12.213abc", 12.213, "abc") );
   RUNTEST( ("0x1.8e2", 1.55517578125, "\0") );

   // Check for infinity and nans
   RUNTEST( ("INFINITY", dInf, "\0") );
   RUNTEST( ("iNFinItY Value", dInf, " Value") );
   RUNTEST( ("INF", dInf, "\0") );
   RUNTEST( ("Inf", dInf, "\0") );
   RUNTEST( ("-Inf", -dInf, "\0") );
   RUNTEST( ("+INFINItY VaLUe", dInf, " VaLUe") );
   RUNTEST( ("-INFINItY", -dInf, "\0") );
   RUNTEST( ("NAN", UINT64_TO_DOUBLE( ulNan ), "\0") );

   // Misc Tests
   RUNTEST( ("1000.25", 1000.25, "\0") );
   RUNTEST( ("10001.25", 10001.25, "\0") );
   RUNTEST( ("12.345", 12.345, "\0") );
   RUNTEST( ("123.", 123.0, "\0") );
   RUNTEST( ("+123.", 123.0, "\0") );
   RUNTEST( ("-123.", -123.0, "\0") );
   RUNTEST( ("-00000123.", -123.0, "\0") );
   RUNTEST( ("-00000123.0000", -123.0, "\0") );
   RUNTEST( ("-+123.", 0, "+123.") );
   RUNTEST( ("000.00034 Rest of the String", 0.00034, " Rest of the String") );
   RUNTEST( ("001.00034 Rest of the String", 1.00034, " Rest of the String") );
   RUNTEST( ("0x1.8", 1.5, "\0") );
   RUNTEST( ("12e2", 1200, "\0") );
   RUNTEST( ("12.32e-2", 0.1232, "\0") );
   RUNTEST( ("000012", 12, "\0") );
   RUNTEST( ("0001123.99", 1123.99, "\0") );
   RUNTEST( ("12", 12, "\0") );
   RUNTEST( ("123.123", 123.123, "\0") );
   RUNTEST( ("72394.94094", 72394.94094, "\0") );
   RUNTEST( ("72394.940944982374", 72394.940944982374, "\0") );
   RUNTEST( ("972394.940944982374", 972394.940944982374, "\0") );
   RUNTEST( ("0x1.0p-1040", 8.4879831638610892604455288594528e-314, "\0") );
   RUNTEST( ("0x8.0p-1043", 8.4879831638610892604455288594528e-314, "\0") );

   RUNTEST( ( "12.345", 12.345, "\0" ) );
   RUNTEST( ( "12.345e19", 12.345e19, "\0" ) );
   RUNTEST( ( "-.1e+9", -.1e+9, "\0" ) );
   RUNTEST( ( ".125", .125, "\0" ) );
   RUNTEST( ( "1e20", 1e20, "\0" ) );
   RUNTEST( ( "0e-19", 0, "\0" ) );
   RUNTEST( ( "4\00012", 4.0, "\0" ) );
   RUNTEST( ( "5.9e-76", 5.9e-76, "\0" ) );
   RUNTEST( ( "0x1.4p+3", 10.0, "\0" ) );
   RUNTEST( ( "0xAp0", 10.0, "\0" ) );
   RUNTEST( ( "0x0Ap0", 10.0, "\0" ) );
   RUNTEST( ( "0x0A", 10.0, "\0" ) );
   RUNTEST( ( "0xA0", 160.0, "\0" ) );
   RUNTEST( ( "0x0.A0p8", 160.0, "\0" ) );
   RUNTEST( ( "0xA.0p4", 160.0, "\0" ) );
   RUNTEST( ( "0x0.50p9", 160.0, "\0" ) );
   RUNTEST( ( "0x0.28p10", 160.0, "\0" ) );
   RUNTEST( ( "0x0.14p11", 160.0, "\0" ) );
   RUNTEST( ( "0x0.0A0p12", 160.0, "\0" ) );
   RUNTEST( ( "0x0.050p13", 160.0, "\0" ) );
   RUNTEST( ( "0x0.028p14", 160.0, "\0" ) );
   RUNTEST( ( "0x0.014p15", 160.0, "\0" ) );
   RUNTEST( ( "0x00.00A0p16", 160.0, "\0" ) );
   RUNTEST( ( "0x00.0050p17", 160.0, "\0" ) );
   RUNTEST( ( "0x00.0028p18", 160.0, "\0" ) );
   RUNTEST( ( "0x00.0014p19", 160.0, "\0" ) );

   {
      // Test to see if numbers that cannot be accurately represented
      // are handled consistently.
      double dVal1 = SCANDFUNC("5.3", 0);
      double dVal2 = SCANDFUNC("5.0", 0);
      dVal2 += SCANDFUNC("0.3", 0);
      ASSERT(dVal1 == 5.3);
      ASSERT(dVal2 == 5.3);
      ASSERT(dVal1 == dVal2);
   }

   return nFailed;
}
