#include "std_merge.c"

static int cmp_int32(void* _, const void* aa, const void* bb)
{
   return *((int32*)aa) == *((int32*)bb) ? 0 : (*((int32*)aa) < *((int32*)bb) ? -1 : 1);
}

int main(void) 
{
   int nErrs = 0;
   int ii;
   int a[] = {0,2,4,6,8,10,11,12,13};
   int b[] = {1,3,5,7,9};
   int dst[STD_ARRAY_SIZE(a)+STD_ARRAY_SIZE(b)];

   std_merge(dst, STD_ARRAY_SIZE(dst), 
             a, STD_ARRAY_SIZE(a), 
             b, STD_ARRAY_SIZE(b), 
             sizeof(a[0]), cmp_int32, 0);

   for (ii = 0; ii < STD_ARRAY_SIZE(dst); ++ii) {
      nErrs += dst[ii] == dst[ii] ? 0 : 1;
   }

   return nErrs;
}
