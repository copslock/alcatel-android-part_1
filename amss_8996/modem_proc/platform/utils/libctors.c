#include <stdio.h>

static int count = -1;
__attribute__((constructor)) static void ctor(void) {
      printf("ctor\n");
      count++;
}
static int* pcountext;

__attribute__((destructor)) static void dtor(void) {
      printf("dtor\n");
      (*pcountext)++;
}

int get_count(int* pc) {
   pcountext = pc;
   return count;
}
