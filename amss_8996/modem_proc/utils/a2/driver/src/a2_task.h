#ifndef A2_TASK_H
#define A2_TASK_H
/*!
  @file a2_task.h

  @brief
   Header for the A2 task
  
*/

/*==============================================================================

  Copyright (c) 2015 QUALCOMM Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  QUALCOMM Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of QUALCOMM Technologies Incorporated.

==============================================================================*/

/*==============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/utils/a2/driver/src/a2_task.h#1 $

when       who     what, where, why
--------   ---     -------------------------------------------------------------
01/30/15   ca      CR: 781257, Feature decob offload initial check in
04/17/12   yjz     Move to a2_timetick_t
06/12/12   rp      Added NV EFS read/write a2_dbg profiling.
02/13/12   ar      added support for A2 PC + SPS RESET feature
10/06/11   ar      added a2 power collapse support
10/30/09   ar      removed A2_STACK_SIZ definition
07/15/09   sah     Remove dependancy of a2_util.h header file in a2_task.h
03/20/09   sah     Add continue signal.
03/09/09   sm      Extern a2_task_init()
02/25/09   sah     Switch to Rex/A2 OS layer.
10/10/08   sah     Initial Checkin

==============================================================================*/

/*==============================================================================

                           INCLUDE FILES

==============================================================================*/

#include <rex.h>
#include <a2_common.h>
#include <ccs_a2_decob_offload.h>
#include <fws.h>
#include <modem_fw_ipc.h>
#include <modem_fw_init.h>
#include <modem_fw_memmap.h>


/*==============================================================================

                   EXTERNAL DEFINITIONS AND TYPES

==============================================================================*/

/*! @brief A2 task rex control block
 * a2 task stack and tcb are declared in hw sim for off-target environment.
 * On target these declarations reside in service/task/task.c */
extern rex_tcb_type a2_tcb;

/*! @ This macro indicates the number of indices in the A2 - CCS FIFO.
      - defined from FW. Value is 19. */ 
#define A2_TASK_TB_FIFO_IDX_MAX  CCS_A2_DECOB_OFFLOAD_BUF_INST

/* Max entries in A2 task stats - specify power of 2 */
#define A2_TASK_MAX_STATS_POW 6
/*! Max entries in A2 task stats */
#define A2_TASK_MAX_STATS_CNT (1 << A2_TASK_MAX_STATS_POW)
/*! mask used to compute the modulo operation. */
#define A2_TASK_MAX_STATS_CNT_MASK (0xffffffff >> (32 - A2_TASK_MAX_STATS_POW))

/*! @This macro represents the max array size for TB FIFO stats.  */
#define A2_TASK_TB_FIFO_STATS_IDX_MAX  64

#ifdef FEATURE_A2_DECOB_OFFLOAD_DBG

/*! @brief structure to hold a2 dl phy fifo profiling information */
typedef struct
{
  /*! to store the tb meta info from A2 - CCS FIFO */
  ccs_a2_decob_offload_info  tb_meta_info;
  /*! tb read time from fifo by A2 TASK */
  uint32                    tb_rcvd_time;
  /*CRC pass indication */
  boolean                   tb_crc_pass;
  
}a2_task_tb_dbg_info_s;

/*! @brief structure to hold a2 dl phy fifo profiling information */
typedef struct
{
  /*number of TBs rcvd */
  uint32                        num_tb_rcvd;
  /*number of  TBs with CRC failed */
  uint32                        num_crc_failed_tb_rcvd;
  /*number of times rd idx is updated both in mac context and a2 dl phy context*/
  uint32                        num_rd_idx_updated;
  /* array of all the rcvd TBs info */  
  a2_task_tb_dbg_info_s  tb_dbg_info[A2_TASK_MAX_STATS_CNT];
  /* current index into tb_info */
  uint16                        rcvd_tb_info_idx;
  uint16                        proc_tb_info_idx;
}a2_task_tb_dbg_info_log_s;
#endif /*FEATURE_A2_DECOB_OFFLOAD_DBG*/

/*! @brief structure for collecting a2 dl phy boot up time profiling 
     information */
typedef struct
{ 
  /*the time at which the signal is rcvd */
  a2_timetick_t tb_rcvd_sig_time;
  /*fifo rd idx when the signal is rcvd*/
  uint32        fifo_rd_idx;
  /*fifo wr idx when the signal is rcvd*/
  uint32        fifo_wr_idx;

}a2_task_tb_rcvd_sig_info_s;

/*! @brief structure for collecting a2 dl phy rcvd tb signal 
    information */
typedef struct
{
  /* current signal idx into tb_rcvd_sig_time */
  uint32          tb_rcvd_sig_idx;
  /* total num of signals rcvd */
  uint32          num_sig_rcvd;
  /*number of  signals processed */
  uint32          num_sig_proc;
  /* rcvd signal time log */
  a2_task_tb_rcvd_sig_info_s   tb_rcvd_sig_info[A2_TASK_MAX_STATS_CNT];
} a2_task_tb_rcvd_signal_stats_s;

#ifdef FEATURE_A2_DECOB_OFFLOAD_DBG
/*! @brief structure to maintain last shut down state of a2 task */
typedef struct
{
  /*! @ fifo internal rd idx at the time of last shut down */
  uint32  tb_fifo_ird_idx;
  /*! @ fifo internal wr idx at the time of last shut down */
  uint32  tb_fifo_iwr_idx;
  /*! @ fifo rd idx at the time of last shut down */
  uint32  tb_fifo_rd_idx;
  /*! @ fifo internal wr idx at the time of last shut down */  
  uint32  tb_fifo_wr_idx;
  /*! @ value of proc_tb_fifo_rd_idx at the time of last shut down */
  uint32  proc_tb_fifo_rd_idx[A2_CACHELINE_SIZE >> 2];
  /*! @ last dl phy shut down time */
  a2_timetick_t                 shut_down_time;
  

}a2_task_shut_down_state_s;
#endif /* FEATURE_A2_DECOB_OFFLOAD_DBG */

/*! @brief structure to maintain the stats for A2 dl phy tb signal and TBs */
typedef struct
{
  /*! @ stats for received tb signal from A2 - CCS call back  */
  a2_task_tb_rcvd_signal_stats_s tb_rcvd_signal_stats;
  
  /*! Stats for all the TBs received */
  a2_task_tb_dbg_info_log_s  tb_info_log;
  
  /*! Number of times proc_tb_fifo_rd_idx is reset */
  uint32  num_proc_tb_fifo_rd_idx_reset;

  /*! last time proc_tb_fifo_rd_idx was reset */
  a2_timetick_t  proc_tb_fifo_rd_idx_reset_time;
  
#ifdef FEATURE_A2_DECOB_OFFLOAD_DBG  
  /*! Shut down state of a2 task */
  a2_task_shut_down_state_s shut_down_state;
#endif /* FEATURE_A2_DECOB_OFFLOAD_DBG */

  /*! number of TBs dropped by A2 task because dl phy tech is not LTE */
  uint32 num_tbs_dropped;

}a2_task_stats_s;

/*!
@brief
 A structure to hold the current TB req which will be programmed to A2 HW. 

*/
typedef struct
{
  /* latest tb programmed to A2 HW meta info sw word 0 */
  uint32  tb_meta_info_sw_word0;
  /* latest tb programmed to A2 HW meta info sw word 1 */  
  uint32  tb_meta_info_sw_word1;
  /* latest tb programmed to A2 HW src addr */  
  uint32  tb_src_phys_addr;
  /* latest tb programmed to A2 HW dma len*/  
  uint32  tb_dma_len;
  
}a2_task_curr_tb_info_s;


/*!
   @brief
    Data structure to store fifo data used internally to program dma tasks.
*/
typedef struct
{
  /*! Pointer declaration to the shared FIFO mem between CCS and A2 */  
  volatile ccs_a2_decob_offload_buf* ccs_a2_tb_fifo_ptr;

  /*! this index ranges from 0 - 18. This represents the actual TB RD index to be 
     processed. */
  volatile uint32 tb_fifo_ird_idx;
  /*! this index ranges from 0 - 18. This represents the actual TB WR index after 
     tb rcvd signal. This will be used to determine the number of indices 
     to be processed for each signal rcvd. */
  volatile uint32 tb_fifo_iwr_idx;

  /*! An array which is used to commit the A2 - CCS rd idx.
			 This variable is updated using A2 HW for every TB. 
			 This variable size should be cache line size because A2 SW invalidates the 
			 cache before reading it. 
			 Indices 1,2 and 3 are never used */
  volatile uint32										proc_tb_fifo_rd_idx[A2_CACHELINE_SIZE >> 2] 
																		__attribute__((aligned(A2_CACHELINE_SIZE)));

 /*!  A structure to hold current tb info which is to be programmed to A2 HW*/
 a2_task_curr_tb_info_s   curr_tb_info;
 
 /*! dl phy curr tech ptr */
  uint32* dl_phy_curr_tech_ptr;
}a2_task_tb_fifo_info_s;


/*==============================================================================

                    EXTERNAL FUNCTION PROTOTYPES

==============================================================================*/

/*===========================================================================

  FUNCTION:  a2_task_main

===========================================================================*/
/*!
    @brief
    Rex main function for a2 task

    @return
    None
*/
/*=========================================================================*/
void a2_task_main
(
  dword dummy
);

/*===========================================================================

  FUNCTION:  a2_task_init

===========================================================================*/
/*!
    @brief
    call inits of general modules needed for a2 task and also sets the
    cache line registers

    @return
    None
*/
/*=========================================================================*/
void a2_task_init(void);

/*===========================================================================

  FUNCTION:  a2_task_signal_continue

===========================================================================*/
/*!
    @brief
    Signals the a2 task to continue processing.

    @return
    None
*/
/*=========================================================================*/
void a2_task_signal_continue(void);
/*===========================================================================

  FUNCTION:  a2_task_profiling_efs_start_set_time

===========================================================================*/
/*!
    @brief
    Set the a2 efs start time in task profiling structure
*/
/*=========================================================================*/
extern void a2_task_profiling_efs_start_set_time
(
  a2_timetick_t value
);

/*===========================================================================

  FUNCTION:  a2_task_profiling_efs_conf_write_set_time

===========================================================================*/
/*!
    @brief
    Set the a2 efs conf file write time in task profiling structure
*/
/*=========================================================================*/
extern void a2_task_profiling_efs_conf_write_set_time
(
  a2_timetick_t value
);

/*===========================================================================

  FUNCTION:  a2_task_profiling_efs_get_set_time

===========================================================================*/
/*!
    @brief
    Set the a2 efs get time in task profiling structure
*/
/*=========================================================================*/
extern void a2_task_profiling_efs_get_set_time
(
  a2_timetick_t value
);

/*==============================================================================

  FUNCTION:  a2_task_commit_tb_fifo_rd_idx
==============================================================================*/
/*!
  @brief
  To commit the rd index of the A2 -  CCS fifo.
  This would allow CCS to write more TBs. This is done in the interrupt context
  to let the CCS know abt the availability of the FIFO as soon as possible 
  @detail
  Note that this function is used in IRQ context.
*/
/*============================================================================*/

void a2_task_commit_tb_fifo_rd_idx( void );

/*===========================================================================

  FUNCTION:  a2_task_save_tb_dbg_info

===========================================================================*/
/*!
    @brief
    store the tb info data after reading from the FIFO . 
    This is used for debugging purpose.

*/
/*=========================================================================*/
void a2_task_save_tb_dbg_info
(
  boolean tb_crc, /*! This indicates if the TB crc is passed */
  uint32  rd_idx /*! fifo rd idx */
);

/*===========================================================================

  FUNCTION:  a2_task_tb_fifo_deinit

===========================================================================*/
/*!
    @brief
    This function blocks a2 task untill all the outstanding tbs for the current 
    subframe are programmed to a2 dl phy sw taskq.
*/
/*=========================================================================*/
void a2_task_tb_fifo_deinit( void );

/*===========================================================================

  FUNCTION:  a2_task_get_proc_tb_fifo_rd_idx_ptr

===========================================================================*/
/*!
    @brief Get the address of the proc tb fifo rd idx array

*/
/*=========================================================================*/
uint32* a2_task_get_proc_tb_fifo_rd_idx_ptr( void );

/*===========================================================================

  FUNCTION:  a2_task_tb_fifo_init

===========================================================================*/
/*!
    @brief A function to set up a2 task. CCS init is done prior to 
     A2 wake up and hence this can be called in A2 set tech.

*/
/*=========================================================================*/
void a2_task_tb_fifo_init( void );

/*===========================================================================

  FUNCTION:  a2_memcpy_word

===========================================================================*/
/*!
    @brief A function to read from AHB bus word by word.
*/
/*=========================================================================*/
INLINE void a2_memcpy_word
(
  void *dest_ptr,             /*!< Destination buffer, in DSP memory */
  volatile void *ahb_src_ptr, /*!< Source buffer, in AHB memory */
  uint32 num_bytes            /*!< Size of buffer to copy, in bytes */
)
{

  /* We use void * as arguments to this function for type convenience, but
     they're really uint32 aligned. 
     Ensure that the source buffer is unioned with uint32 to avoid strict-aliasing
     warnings/bugs. */
  uint32 *dest = (uint32 *) dest_ptr;
  volatile uint32 *src = (volatile uint32 *) ahb_src_ptr;
  uint32 len = num_bytes / sizeof(uint32); 
  uint32 i;

  /* Copy word-by-word. */
  for (i=0; i < len; ++i)
  {
    *dest++ = *src++;
  }

} /* fw_memss_ahb_read_uc_align32_small() */

#endif /* A2_TASK_H */

