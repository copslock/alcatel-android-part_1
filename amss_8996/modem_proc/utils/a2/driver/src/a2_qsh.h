#ifndef _A2_QSH_H_
#define _A2_QSH_H_
/*!
  @file a2_qsh.h

  @brief
  Interface for implementation of QSH client for A2.
*/

/*==============================================================================

  Copyright (c) 2015 Qualcomm Technologies, Inc. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies, Inc. and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies, Inc.

==============================================================================*/

/*==============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/utils/a2/driver/src/a2_qsh.h#1 $

when       who     what, where, why
--------   ---     -------------------------------------------------------------
10/13/14   mm      Added stats reset
05/23/14   mm      Removed obsolete macros
03/28/14   mm      Initial check-in

==============================================================================*/

/*==============================================================================

                           INCLUDE FILES

==============================================================================*/
#include <a2_util.h>
#include <a2_common.h>
#include <qsh.h>

/*==============================================================================

                   EXTERNAL DEFINITIONS AND TYPES

==============================================================================*/

/*! QSH major version */
#define A2_QSH_VER_MAJOR 1

/*! QSH minor version */
#define A2_QSH_VER_MINOR 1

/*! the minimum # of abnormal transmissions in order to report an error */
#define A2_QSH_UL_PHY_SW_ABNORMAL_PROCESSING_CNT_THRESH   0

/*! the minimum value of late processing cnt in order to report an error */
#define A2_QSH_UL_PHY_HW_LATE_PROCESSING_CNT_THRESH       20

/*! the maximum ratio of new transmissions to retransmissions in order to 
    log a HIGH message */
#define A2_QSH_UL_PHY_NUM_RETX_THRESH_HIGH                1000

typedef struct
{
  /*! whether or not the hw registers were saved */
  boolean       hw_regs_avail;
  
  /* how many times stats have been reset */
  uint32        stats_reset_count;
} a2_qsh_s;

/*==============================================================================

                    EXTERNAL FUNCTION PROTOTYPES

==============================================================================*/

/*==============================================================================

  FUNCTION:  a2_qsh_analysis_cb

==============================================================================*/
/*!
  @brief
  Callback registered with QSH.
*/
/*============================================================================*/
boolean a2_qsh_analysis_cb
(
  qsh_cb_params_s *   qsh_params_ptr
);

/*==============================================================================

  FUNCTION:  a2_qsh_run_action

==============================================================================*/
/*!
  @brief
  Performs top-level QSH action.
*/
/*============================================================================*/
void a2_qsh_run_action
(
  qsh_cb_params_s *   qsh_params_ptr
);

#endif /* _A2_QSH_H_ */
