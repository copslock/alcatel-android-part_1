#ifndef A2_UL_PHYI_H
#define A2_UL_PHYI_H
/*!
  @file a2_ul_phyi.h

  A2 ul phy internal interface file

  @ingroup a2_int_interface
*/

/*==============================================================================

  Copyright (c) 2015 Qualcomm Technologies, Inc. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies, Inc. and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies, Inc.

==============================================================================*/

/*==============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/utils/a2/driver/src/a2_ul_phyi.h#1 $

when       who     what, where, why
--------   ---     -------------------------------------------------------------
11/15/15   ca      CR: 938178, Debug code for delayed DTX
01/27/15   ag      UL CA Support
11/25/14   ca      CR:762863: UL CA API Interface changes
                   required for RLC and MAC
01/27/15   vd      CR735568: Requirement to corrupt CRC on UL PHY for a harq id
09/24/14   mm      CR 729757: Fix for PLT crash
09/24/14   ca      CR: 729845, ERAM base address for THOR is 
                   changed to 0. FW will pass Absolute addr and
                   ML1 gives the same to A2 to program A2 HW.
09/05/14   mm      Added signal to shutdown procedure
08/15/14   ca      CR:710338, Added new ERAM address for THOR
08/05/14   mm      Added a2_ul_phy_shut_down()
05/09/14   mm      Naming cleanup
05/06/14   ca      Added FEATURE_THOR_MODEM featurization to resolve 
                   compilatin errors for THOR.
04/07/14   ca      Replaced DMA task and ciph set up task with 
                   DMA_ciph task to save taskq memory                   
04/01/14   ca      Added stats per tti.
03/28/14   mm      Moved structs from a2_ul_phy.c for QSH
01/13/14   ca      DSDA feature is implemented.
10/09/13   ar      Initial Checkin

==============================================================================*/


/*==============================================================================

                           INCLUDE FILES

==============================================================================*/
#include <comdef.h>
#include <a2_dbg.h>
#include <a2_ul_phy.h>
#include <a2_taskq.h>
#include <a2_hw_constants.h>
#include <a2_hw_task_defi.h>
#include <dsm.h>
#include <qurt.h>

/*==============================================================================

                   EXTERNAL DEFINITIONS AND TYPES

==============================================================================*/
/* Max entries in The tx type histroy for each harq id - specify power of 2 */
#define A2_UL_PHY_MAX_TX_TYPE_HIST 3
#define A2_UL_PHY_MAX_TX_TYPE_CNT (1 << A2_UL_PHY_MAX_TX_TYPE_HIST)
#define A2_UL_PHY_MAX_TX_TYPE_CNT_MASK (0xffffffff >> (32 - A2_UL_PHY_MAX_TX_TYPE_HIST))


/*NOP task */
#define A2_UL_PHY_NOP                0x50000001 

/* CRC init task */
#define A2_UL_PHY_CRC_INIT           0x40000012 

#define A2_UL_PHY_CIPHER_INIT_NO_PARAM  0xFFFF

#define A2_UL_PHY_MAX_SRC_IOVEC         A2_MAX_SRC_IOVEC_COUNT_PER_DMA_CHAIN

/*! @brief Maximun number of bytes that can be dmaed out of ERAM in one DMA 
  operation on nikel */
#define A2_UL_PHY_NIKEL_DMA_FROM_ERAM_LIMIT   15

#define A2_UL_PHY_EXTRACT_BITS_11_to_17_USTMR(val)    \
  ( (val >> 0xB) & 0x7F )

#define A2_UL_PHY_DEBUG_INFO_MAX_SIZE   256

#define A2_UL_PHY_TAG_TIME_BMSK         0x3FFFFF

#define A2_UL_PHY_TAG_TIME_MAX_VAL      (A2_UL_PHY_TAG_TIME_BMSK + 1)

/*!
 *  DMA Tag field 
 *  USTMR   : [0:3]
 *  harq_id : [4:7]
 *  car_id  : [8:10]
 */
/*! Offset value for harq_id field in DMA_Tag */
#define A2_UL_PHY_TAG_HARQ_ID_OFFSET      0x4
/*! Offset value for car_id field in DMA_Tag */
#define A2_UL_PHY_TAG_CAR_ID_OFFSET       0x8
/*! Bitmask for harq_id field in DMA_Tag */
#define A2_UL_PHY_TAG_HARQ_ID_MASK        0xF0
/*! Bitmask for car_id field in DMA_Tag */
#define A2_UL_PHY_TAG_CAR_ID_MASK         0x700
/*! Bitmask for USTMR value */
#define A2_UL_PHY_TAG_USTMR_11BIT_MASK    0xB

#ifdef FEATURE_A2_ON_TARGET
#define A2_UL_PHY_STMR_MAX_VAL  (HWIO_MSS_STMR_TIME_RD_UNIV_TIME_BMSK + 1)
#endif

/*! @brief ERAM is placed in VPE0_LMEM memory. VPE0_LMEM starts at 
MODEM_LMEM_RAM_START. The address of this memory is 33bit 0x100C00000. A2 
driver needs to program just the lower 32bits i.e. 0x00C00000 and HW will 
add the 0x1 on the MSB to complete the address */

#ifdef FEATURE_A2_ON_TARGET 
  #ifdef FEATURE_THOR_MODEM
    #define A2_UL_PHY_VPE0_LMEM_START_ADDR 0x0
  #else /* FEATURE_THOR_MODEM */
    #define A2_UL_PHY_VPE0_LMEM_START_ADDR  HWIO_MODEM_LMEM_RAM_START_PHYS
  #endif /* FEATURE_THOR_MODEM */
#else  /* FEATURE_A2_ON_TARGET */
  #define A2_UL_PHY_VPE0_LMEM_START_ADDR  0x0
#endif  /* FEATURE_A2_ON_TARGET */

/*! Macro used to extract harq_id field from tag_ext */
#define A2_UL_PHY_TAG_GET_HARQ_ID(tag) \
  ((tag & A2_UL_PHY_TAG_HARQ_ID_MASK) >> A2_UL_PHY_TAG_HARQ_ID_OFFSET)

/*! Macro used to extract car_id field from tag_ext */
#define A2_UL_PHY_TAG_GET_CAR_ID(tag) \
  ((tag & A2_UL_PHY_TAG_CAR_ID_MASK) >> A2_UL_PHY_TAG_CAR_ID_OFFSET)

/*! @brief Enum for signal masks */
typedef enum
{
  A2_UL_PHY_SIGNAL_MASK_SHUTDOWN = 0x1
} a2_ul_phy_signal_mask_e;

/*!
   @brief Structure to hold a2 sub id information such as,
   the inst bf and registered call backs.
*/
typedef struct
{
  /*! variable to store handle to the instance bf for a given sub id */
  a2_hw_inst_bf_t hw_inst_bf;
  
}a2_ul_phy_sub_id_info_s;

typedef enum
{
  /*! Tag used for notifying that all tasks in the queue have been
    processed
    */
  A2_UL_PHY_TAG_ID_FINISHED = 0x0,
  A2_UL_PHY_TAG_HW_TAG_OTA,
  A2_UL_PHY_TAG_HW_PROCESS_START_TIME,
  A2_UL_PHY_TAG_HW_PROCESS_DONE_TIME,
  A2_UL_PHY_TAG_ID_SHUTDOWN_SIGNAL,
  A2_UL_PHY_TAG_ID_MAX
} a2_ul_phy_tag_id_e;

/*! enum to represent transmission type  */
typedef enum
{
  A2_UL_PHY_TX_TYPE_INVALID = 0x0,
  A2_UL_PHY_TX_TYPE_NEW,
  A2_UL_PHY_TX_TYPE_RTX,
  A2_UL_PHY_TX_TYPE_DTX,  
} a2_ul_phy_tx_type_e;

/*! @brief Structure storing the tasks to be filled
*/
typedef struct
{
  hwtask_a2_ts_dma_t                dma_to_specified_addr;
  hwtask_a2_ts_dma_ciph_t           dma_ciph_for_dsm;
  hwtask_a2_ts_dma_t                dma_for_dsm;
  hwtask_a2_ts_dma_t                first_dma;
  hwtask_a2_ts_dma_t                dma_from_eram;
  /* Dummy dma task to read 1 word out of ENCIB and discard the data. This is
  needed to ensure the AXI\AHB buffers are flushed for previous dma into eram */
  hwtask_a2_ts_dma_t                dummy_dma_from_eram;
  hwtask_a2_ts_dma_header_t         dma_header;
  hwtask_a2_ts_dma_fill_t           dma_fill;
  hwtask_a2_ts_dma_fill_t           first_dma_fill;
  hwtask_a2_ts_dma_fill_t           dma_fill_ext;
  hwtask_a2_ts_dma_fill_t           dma_fill_harq_done;
  hwtask_a2_ts_crc_init_t           crc_init;
  hwtask_a2_ts_crc_report_t         crc_report;
  hwtask_a2_ts_umts_ciph_setup_t    ciph_setup;
  hwtask_a2_ts_umts_ciph_init_t     ciph_init; 
  hwtask_a2_ts_dma_tag_t            tag;
  hwtask_a2_ts_dma_tag_t            tag_time;
  hwtask_a2_ts_dma_irq_t            dma_irq;
} a2_ul_phy_task_cache_s;

/*! @brief Structure storing taskq programming info for harq retransmissions
*/

typedef struct
{
  uint32   ota_sched_time;
  uint32   delta_between_ota;
  uint32   hw_start_time;
  uint32   hw_done_time;
  uint32   hw_delta_from_sched;
  uint32   harq_id;
  uint32   car_id;
} a2_ul_phy_time_tag_s;

/*! @brief Structure storing stats for current tti */
typedef struct
{
  /*packet build start time for current tti */
  a2_timetick_t  tti_start_time;
  /*packet build process time for current tti */
  a2_timetick_t  tti_process_time_us;
  /*current tti harqid*/
  uint8          curr_harq_id;
  /*transmission type */
  a2_ul_phy_tx_type_e tx_type;
  /*number of words written inside taskq for current tti */
  uint32         num_taskq_words_written;
} a2_ul_phy_tti_stats_s;

typedef struct
{ 
  /*tx type for each harq */
  a2_ul_phy_tx_type_e tx_type;
  /* time at which the tx type is recvd */
  a2_timetick_t       tx_time;
}a2_ul_phy_tx_type_hist_s;

typedef struct
{
  /* addr of the memory inside UL PHY taskq from where the taskq programming for
   * this harq_id starts */
  uint32*         harq_taskq_start_addr;

  /* addr of the memory inside UL PHY taskq from where the taskq programming for
   * this harq_id ends */
  uint32*         harq_taskq_end_addr;

  /* dsm pointer pointing to the back up memory where taskq programming for this
   * harq is saved in case of discontinuous transmission (DTX) */
  dsm_item_type*  task_mem_dsm_ptr;

  /* The Universal stmr timer at which the current harq has to be transmitted */
  uint32          ota_sched_time;

  /* The Universal stmr timer at which SW commits the task to HW  */
  uint32          sw_commit_time;

  /* 11- 17 bits of ota_sched_time including the offset needed for A2 process */
  uint32          a2_sched_time;
  /* SW wrkaround min and max delta */
  uint32          swrk_min_delta;
  uint32          swrk_max_delta;
  
  /* offset to CRC address */
  uint32          crc_offset;
  
  /* current idx of tx type histroy */
  uint8           tx_type_hist_idx;

  /*Histroy of tx type for each harq */
  a2_ul_phy_tx_type_hist_s tx_type_hist[A2_UL_PHY_MAX_TX_TYPE_CNT];
  
  /* whether or not this taskq programming will generate an interrupt */
  boolean         interrupt_reqd;

  
  /* flag to indicate if CRC needs to be corrupted or not for this harq id */
  boolean         corrupt_crc;  

  /* harq sysfn,subfn and cellid info*/
  a2_ul_phy_harq_id_fn_info_s harq_fn_info;
  /* End stats from Status Q*/
}a2_ul_phy_harq_taskq_info_s;

/*! @brief A2 UL PHY Harq related stats */
typedef struct
{
  /* number of new harq transmissions since power up */
  uint32 num_new_harq_tx;
  /* number of new harq re-transmissions since power up */
  uint32 num_harq_retx;
  /* number of new harq discontinuous transmissions (DTX) since power up */
  uint32 num_harq_dtx;
  /* number of forced CRC corrupted new transmissions */
  uint32 num_corrupt_crc_tx;
  /* number of forced CRC corrupted new transmissions */
  uint32 num_corrupt_crc_retx;
    /*number of error case where A2 picks up HARQ retx request after conn release*/
  uint32 num_harq_retx_ignore;
  /*number of addr is NULL during harq re-transmissions since power up*/
  uint32 num_harq_retx_addr_null;
  /*number of addr is NULL during harq discontinuous trans since power up*/
  uint32 num_harq_dtx_addr_null;
} a2_ul_phy_harq_stats_s;

/*! @brief A2 UL PHY statistics structure
*/
typedef struct
{
  /* harq related stats per carrier_id */
  a2_ul_phy_harq_stats_s harq_stats[A2_UL_PHY_CAR_ID_MAX];
  uint32 num_dma_irq_rcvd;
  /* no of time context switch happens after kernal protection*/
  uint32 num_cntx_switch_at_p0;
  /* sw delta scheduled slot histogram */
  uint32 delta_in_sched_histo[A2_UL_PHY_MAX_U_STMR_VALUE];
  /* no of times A2 HW notified the actual task process start time*/
  uint32 num_process_done_tag_time;
  /* counter to keep track UL PHY HW late processing */
  uint32 hw_late_processing_cnt;
  /* counter to keep track any abnormal in SW UL PHY time line */
  uint32 sw_abnormal_processing_cnt;
  /* debug UL PHY time tag */
  uint32 time_index;
  uint32 last_ota;
  a2_ul_phy_time_tag_s  time_tag[A2_UL_PHY_DEBUG_INFO_MAX_SIZE];
  /*stats for current tti per carrier_id */
  a2_ul_phy_tti_stats_s  curr_tti_stats[A2_UL_PHY_CAR_ID_MAX];
  /* Max number of words written to taskq */
  uint32  max_num_taskq_words_written;
  
  /*!< num of times taskq/statusq rd/wr ptr mismatched at unset_technology() */
  uint32    num_taskq_rd_wr_mismatch;
  uint32    num_statusq_rd_wr_mismatch;
  
  /* num of times shutdown blocked to wait for rd/wr ptrs */
  uint32    num_shutdown_blocked;
  
  /* max latency in us waiting for rd/wr ptrs */
  uint32    max_shutdown_latency_us;

#ifdef FEATURE_A2_SW_WORKAROUND_UL_PHY_UNEXPECTED_STATUS
  uint32                      bogus_frag_dst_cnt;
  uint32                      bogus_frag_addr_cnt;
  uint32                      bogus_dst_dsm_free_cnt;
  uint32                      bogus_addr_dsm_free_cnt;
#endif
}a2_ul_phy_stats_s;

/*! @brief Structure storing information on the ul phy
*/
typedef struct
{
  a2_taskq_s                  taskq;

  a2_taskq_status_s           statusq;
  
  /* Cached tasks where tasks are being prepared before copying to the
     uncached task queue */
  a2_ul_phy_task_cache_s      task;

  /*! technology for which a2 ul phy has been configured to */
  a2_technology_e             curr_technology;

  uint32                      num_rlc_iovecs;
  a2_iovec_t                  rlc_iovec_list[A2_UL_PHY_MAX_SRC_IOVEC];

  /*! harq taskq programming related info */
  a2_ul_phy_harq_taskq_info_s 
      harq_info[A2_UL_PHY_CAR_ID_MAX][A2_UL_PHY_MAX_LTE_UL_HARQS];

  /*! a2 ul phy statistics */
  a2_ul_phy_stats_s           stats;

  /*! a2 power collapse related debug info */
  a2_pc_dbg_s                 pc_dbg;
#ifdef FEATURE_A2_SW_WORKAROUND_UL_PHY_UNEXPECTED_STATUS
  dsm_item_type               *bogus_dsm_ptr;
#endif
  /*! ENC_IB start address of the tb currently under processing */
  uint32                       curr_tb_encib_start_addr;
  /*! sub id for a particular inst id */
  a2_sub_id_t                  sub_id;

} a2_ul_phy_inst_s;

/*! @brief Structure storing instamnces of ul phy */
typedef struct
{
  /*! A table to map hw instance bit field to hw instance id 
      The mapping for 4 valid bit fields looks like follows
       inst bf - inst id
             0 - A2_HW_INST_ID_INVALID
             1 - 0
             2 - 1
             3 - A2_HW_INST_ID_INVALID*/
  a2_hw_inst_id_e         hw_inst_bf_to_inst_id_tbl[A2_HW_INST_BF_MAX+1];

  /*! Dummy 1 word dest buffer for dummy dma readback from encib */
  /* todo:  we should think about reducing the size for dummy dma destination 
  memory by maybe allocating something from the uncached memory region.*/ 
  dsm_item_type *              dummy_dma_dest_dsm_ptr;
  
  /*! a2 ul phy sub id info to store call backs and a pointer to 
      a2_power.sub_id[].inst_bf*/
  a2_ul_phy_sub_id_info_s      sub_id_info_tbl[A2_SUB_ID_MAX];

    /*! The callback to be called when the notification is triggered. */
  a2_ul_phy_notify_cb_t         notify_cb;
  
  /* signal for shutdown */
  qurt_signal_t                 shutdown_signal;
} a2_ul_phy_common_s;

/*! @brief Structure storing instamnces of ul phy */
typedef struct
{ 
  /* common data structure for ul phy instances. */
  a2_ul_phy_common_s common;
  
  /*! instnces of ul phy taskq's*/
  a2_ul_phy_inst_s  inst[A2_HW_INST_ID_MAX];
  
} a2_ul_phy_s;

/*==============================================================================

                             MACROS

==============================================================================*/

/*! @brief This macro used to get the inst id from the sub id.
    This is a two step process. one is to get the inst bf from sub id
    and then get the inst id from inst bf */

#define A2_UL_PHY_GET_HW_INST_ID(sub_id)                      \
    a2_ul_phy.common.hw_inst_bf_to_inst_id_tbl                \
    [a2_ul_phy.common.sub_id_info_tbl[sub_id].hw_inst_bf]

/*!
    @brief
    macro validates the sub id to check if it is in the proper limits 
*/
#define A2_UL_PHY_VALIDATE_CAR_ID(car_id) \
        ((car_id >= A2_UL_PHY_CAR_ID_0)&&(car_id < A2_UL_PHY_CAR_ID_MAX))


/*==============================================================================

                    EXTERNAL FUNCTION PROTOTYPES

==============================================================================*/

/*==============================================================================

  FUNCTION:  a2_ul_phy_init

==============================================================================*/
/*!
  @brief
  Initialize the SW structures for ul phy block

*/
/*============================================================================*/
void a2_ul_phy_init
(
  void
);

/*==============================================================================

  FUNCTION:  a2_ul_phy_get_top_lvl_ptr

==============================================================================*/
/*!
  @brief
  Returns a pointer to the top-level data structure.
*/
/*============================================================================*/
a2_ul_phy_s * a2_ul_phy_get_top_lvl_ptr
(
  void
);

/*==============================================================================

  FUNCTION:  a2_ul_phy_ist_cb

==============================================================================*/
/*!
  @brief
  Interrupt handler for A2 UL PHY module.

*/
/*============================================================================*/
int a2_ul_phy_ist_cb ( void* arg );


/*==============================================================================

  FUNCTION:  a2_ul_phy_set_technology

==============================================================================*/
/*!
  @brief
  Set the technology that is going to use A2 UL PHY HW and perform necessary
  HW initialization.

  @caller
  HSPA: whenever handover to HSPA is initiated and/or whenever WCDMA cell search
        starts
  LTE: whenever UE starts LTE cell search

  @todo - need to see from when and where to call this function. Critical from
   HSPA<->LTE hand-off. Need to make sure that task bring up doesn't happen in
   parallel. If yes then there is a need to sync up between LTE and HSPA.
   Probable mutex to guarantee mutual exclusion.
*/
/*============================================================================*/
void a2_ul_phy_set_technology
(
  a2_sub_id_t     sub_id, /* subscriber id */
  a2_technology_e technology,
  a2_hw_inst_bf_t inst_bf
);

/*==============================================================================

  FUNCTION:  a2_ul_phy_unset_technology

==============================================================================*/
/*!
  @brief
  Set the technology to default.
*/
/*============================================================================*/
void a2_ul_phy_unset_technology
(
  a2_sub_id_t     sub_id /* subscriber id */
);

/*==============================================================================

  FUNCTION:  a2_ul_phy_shut_down

==============================================================================*/
/*!
  @brief
  Verifies that taskq/statusq have been processed.
*/
/*============================================================================*/
void a2_ul_phy_shut_down
(
  void
);

/*==============================================================================

  FUNCTION:  a2_ul_phy_hw_init

==============================================================================*/
/*!
    @brief
    initializes the HW related registers for UL PHY block

    @return
    None
*/
/*============================================================================*/
void a2_ul_phy_hw_init
(
  a2_sub_id_t     sub_id, /* subscriber id */
  a2_technology_e tech
);
/* a2_ul_phy_hw_init() */

/*==============================================================================

  FUNCTION:  a2_ul_phy_hspa_init

==============================================================================*/
/*!
    @brief
    Initializes variables associated with hspa init block

    @caller
    A2 Init (during A2 driver initialization)

    @return
    None
*/
/*============================================================================*/
void a2_ul_phy_hspa_init ( void );

/*==============================================================================

  FUNCTION:  a2_ul_phy_add_sub_id

==============================================================================*/
/*!
    @brief
    updates the sub id to inst id and inst id to sub id look up tables.
    This function is called everytime a new power request has been revieved.

    @return
    nothing
*/
/*============================================================================*/

void a2_ul_phy_add_sub_id
(
  a2_sub_id_t          sub_id,  /*!< sub id in the system*/
  a2_hw_inst_bf_t      inst_bf  /*!< instance bf for the coresponding sub id*/
);

/*==============================================================================

  FUNCTION:  a2_ul_phy_remove_sub_id

==============================================================================*/
/*!
  @brief
  removes the sub id info from sub id to inst id table.
*/
/*============================================================================*/
void a2_ul_phy_remove_sub_id
(
  a2_sub_id_t          sub_id  /*!< sub id in the system*/
);

/*==============================================================================

  FUNCTION:  a2_ul_phy_hspa_unset_technology

==============================================================================*/
/*!
    @brief
    cleans up UL PHY HSPA state during a2 power collapse

    @return
    TRUE if shut down was successful
    FALSE otherwise
*/
/*============================================================================*/
void a2_ul_phy_hspa_unset_technology
(
  a2_hw_inst_id_e inst_id

);
/* a2_ul_phy_hspa_shut_down() */

/*==============================================================================

  FUNCTION:  a2_ul_phy_get_sub_id_to_inst_bf_ptr
  
==============================================================================*/
/*!
  @brief
  To get the handle to the sub_id_to_inst_bf_tbl table
  */
/*============================================================================*/
a2_ul_phy_sub_id_info_s* a2_ul_phy_get_sub_id_to_inst_bf_tbl_ptr
(
  void
);

/*==============================================================================

  FUNCTION:  a2_ul_phy_get_inst_id_to_sub_id_ptr
  
==============================================================================*/
/*!
  @brief
  To get the handle to the a2_ul_phy sub_id variable for a given instance id
  */
/*============================================================================*/
a2_sub_id_t* a2_ul_phy_get_inst_id_to_sub_id_ptr
(
  a2_hw_inst_id_t inst_id /*a2 instance id */ 
);

/*==============================================================================

  FUNCTION:  a2_ul_phy_get_hw_inst_bf_to_inst_id_tbl_ptr
  
==============================================================================*/
/*!
  @brief
  To get the handle to the hw_inst_bf_to_inst_id_tbl table
  */
/*============================================================================*/

a2_hw_inst_id_e* a2_ul_phy_get_hw_inst_bf_to_inst_id_tbl_ptr
(
  void
);

/*==============================================================================

  FUNCTION:  a2_ul_phy_set_boot_up_state
  
==============================================================================*/
/*!
  @brief
  To set the boot up state for taskqs
  */
/*============================================================================*/
void a2_ul_phy_set_boot_up_state
(
  boolean boot_up_state
);

/*==============================================================================

  FUNCTION:  a2_ul_phy_update_crc

==============================================================================*
/
/*!
  @brief
  Corrupt/init CRC address 
 
  @param
  crc_addr    : The addr that needs to be updated 
  corrupt_crc : If true crc is corrupted by writing a no-op else will be
                initialized with the crc init value
*/
/*============================================================================*/
INLINE void a2_ul_phy_update_crc(uint32* crc_addr, boolean corrupt_crc)
{

  /* Ensure the address contains crc init or no-op as a sanity check */
  A2_ASSERT((*crc_addr == A2_UL_PHY_CRC_INIT) || 
            (*crc_addr == A2_UL_PHY_NOP));

  if(A2_LIKELY(corrupt_crc == FALSE))
  {
    /* Write the CRC init task - No need to corrupt the CRC */
    *crc_addr  = A2_UL_PHY_CRC_INIT;
    A2_ASSERT(*crc_addr == A2_UL_PHY_CRC_INIT);
  }
  else
  {
    /* Corrupt the CRC by writing a no-op here. Important
       we read it back to ensure the DDR is updated */
    *crc_addr = A2_UL_PHY_NOP;
    A2_ASSERT(*crc_addr == A2_UL_PHY_NOP);
  }
}

/*==============================================================================

  FUNCTION:  a2_ul_phy_curr_tti_update

==============================================================================*/
/*!
  @brief
  Update the curr_tti_stats struct.
*/
/*============================================================================*/
INLINE void a2_ul_phy_curr_tti_update
(
  /*! subscriber id */
  a2_hw_inst_id_t     inst_id,
  /*! Carrier ID for UL CA */
  a2_ul_phy_car_id_e  car_id, 
  /*! Harq_id for current TB */
  uint8               harq_id
);

/*==============================================================================

  FUNCTION:  a2_ul_phy_curr_tti_set

==============================================================================*/
/*!
  @brief
  Set the curr_tti_stats struct.
*/
/*============================================================================*/
INLINE void a2_ul_phy_curr_tti_set
(
  /*! subscriber id */
  a2_hw_inst_id_t     inst_id,
  /*! Carrier ID for UL CA */
  a2_ul_phy_car_id_e  car_id, 
  /*! Harq_id for current TB */
  uint8               harq_id,
  /*! tx_type */
  a2_ul_phy_tx_type_e tx_type
);

#endif /* A2_UL_PHYI_H */
