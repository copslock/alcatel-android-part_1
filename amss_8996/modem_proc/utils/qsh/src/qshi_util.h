/*!
  @file
  qshi_util.h

  @brief
  QSH internal header file for util functionality.
*/

/*==============================================================================

  Copyright (c) 2014 QUALCOMM Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  QUALCOMM Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of QUALCOMM Technologies Incorporated.

==============================================================================*/

/*==============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/utils/qsh/src/qshi_util.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------- 
07/01/15   mm      CR 864729: Adding API to allocate modem heap mem
04/14/15   mm      CR 806553: Fixing duplicate definition
03/30/15   mm      CR 814959: Fixing MOB compilation
02/13/15   mm      Initial checkin, renamed qshi.h -> qshi_util.h               
==============================================================================*/

#ifndef QSHI_UTIL_H
#define QSHI_UTIL_H

/*==============================================================================

                           INCLUDE FILES

==============================================================================*/

#include "utils_variation.h"
#include <assert.h>
#include "qsh_cfg.h"
#ifdef FEATURE_POSIX
#include <pthread.h>
#endif
#include <rex.h>
#include <qurt.h>
#include <modem_mem.h>
#include <ULog.h>
#include <ULogFront.h>

/*==============================================================================

                   EXTERNAL DEFINITIONS AND TYPES

==============================================================================*/

#ifdef FEATURE_POSIX
  typedef pthread_mutex_t qsh_crit_sect_s;
#else
  typedef rex_crit_sect_type qsh_crit_sect_s;
#endif

/*!
  @brief
  time type 
*/
typedef uint32 qsh_timetick_t;

/*!
  @brief
  16-bit time type 
*/
typedef uint16 qsh_timetick_16_t;

#ifdef __KLOCWORK__
  #define QSH_ASSERT(exp) if (!(exp)) abort();
#else
  #define QSH_ASSERT(exp)  \
  /*lint -save -e506 -e774 */ \
  if(QSH_UNLIKELY(!(exp)))  \
  /*lint -restore */ \
  { \
    ERR_FATAL( "QSH assertion failed: " #exp, 0, 0, 0 ); \
  }
#endif

/*==============================================================================

MACRO QSH_ASSERT_LOG

DESCRIPTION
  Given a boolean expression, verify that the input expression is TRUE.
  If the input expression is FALSE, flags an error and resets SW stack.
  This is to be used if the developer has determined that this error is 
  critical in nature if assertion fails, No easy recovery mechanism is possible, 
  Only action to be taken is to reset.

DEPENDENCIES
  None.

RETURN VALUE
  None

SIDE EFFECTS
  This WILL call error fatal

==============================================================================*/
#define QSH_ASSERT_LOG(exp, fmt, arg1, arg2, arg3 ) \
    /*lint -save -e506 -e774 */ \
    if(QSH_UNLIKELY(!(exp))) \
    { \
      /*lint --e{527} */ \
      ERR_FATAL( "Assert failed: " fmt ": " #exp, arg1, arg2, arg3 ); \
    } \
    /*lint -restore */ \

#define QSH_MAX(x,y) (((x)>(y)) ? (x) : (y))

#ifdef QSH_CFG_DEBUG_MSG
  #define QSH_DEBUG_MSG(xx_fmt)                                      \
    MSG(MSG_SSID_A2, MSG_LEGACY_HIGH, xx_fmt)

  #define QSH_DEBUG_MSG_1(xx_fmt, a)                                   \
    MSG_1(MSG_SSID_A2, MSG_LEGACY_HIGH, xx_fmt, a)

  #define QSH_DEBUG_MSG_2(xx_fmt, a, b)                                \
    MSG_2(MSG_SSID_A2, MSG_LEGACY_HIGH, xx_fmt, a, b)

  #define QSH_DEBUG_MSG_3(xx_fmt, a, b, c)                             \
    MSG_3(MSG_SSID_A2, MSG_LEGACY_HIGH, xx_fmt, a, b, c)

  #define QSH_DEBUG_MSG_4(xx_fmt, a, b, c, d)                          \
    MSG_4(MSG_SSID_A2, MSG_LEGACY_HIGH, xx_fmt, a, b, c, d)

  #define QSH_DEBUG_MSG_5(xx_fmt, a, b, c, d, e)                       \
    MSG_5(MSG_SSID_A2, MSG_LEGACY_HIGH, xx_fmt, a, b, c, d, e)

  #define QSH_DEBUG_MSG_6(xx_fmt, a, b, c, d, e, f)                    \
    MSG_6(MSG_SSID_A2, MSG_LEGACY_HIGH, xx_fmt, a, b, c, d, e, f)
#else
  #define QSH_DEBUG_MSG(xx_fmt)

  #define QSH_DEBUG_MSG_1(xx_fmt, a)

  #define QSH_DEBUG_MSG_2(xx_fmt, a, b)

  #define QSH_DEBUG_MSG_3(xx_fmt, a, b, c)

  #define QSH_DEBUG_MSG_4(xx_fmt, a, b, c, d)

  #define QSH_DEBUG_MSG_5(xx_fmt, a, b, c, d, e)

  #define QSH_DEBUG_MSG_6(xx_fmt, a, b, c, d, e, f)

#endif

#define QSH_MSG(xx_fmt)                                      \
  MSG(MSG_SSID_A2, MSG_LEGACY_HIGH, xx_fmt)

#define QSH_MSG_1(xx_fmt, a)                                   \
  MSG_1(MSG_SSID_A2, MSG_LEGACY_HIGH, xx_fmt, a)

#define QSH_MSG_2(xx_fmt, a, b)                                \
  MSG_2(MSG_SSID_A2, MSG_LEGACY_HIGH, xx_fmt, a, b)

#define QSH_MSG_3(xx_fmt, a, b, c)                             \
  MSG_3(MSG_SSID_A2, MSG_LEGACY_HIGH, xx_fmt, a, b, c)

#define QSH_MSG_4(xx_fmt, a, b, c, d)                          \
  MSG_4(MSG_SSID_A2, MSG_LEGACY_HIGH, xx_fmt, a, b, c, d)

#define QSH_MSG_5(xx_fmt, a, b, c, d, e)                       \
  MSG_5(MSG_SSID_A2, MSG_LEGACY_HIGH, xx_fmt, a, b, c, d, e)

#define QSH_ERR(xx_fmt)                                      \
  MSG(MSG_SSID_A2, MSG_LEGACY_ERROR, xx_fmt)

#define QSH_ERR_1(xx_fmt, a)                                   \
  MSG_1(MSG_SSID_A2, MSG_LEGACY_ERROR, xx_fmt, a)

#define QSH_ERR_2(xx_fmt, a, b)                                \
  MSG_2(MSG_SSID_A2, MSG_LEGACY_ERROR, xx_fmt, a, b)

#define QSH_ERR_3(xx_fmt, a, b, c)                             \
  MSG_3(MSG_SSID_A2, MSG_LEGACY_ERROR, xx_fmt, a, b, c)

#define QSH_ERR_4(xx_fmt, a, b, c, d)                          \
  MSG_4(MSG_SSID_A2, MSG_LEGACY_ERROR, xx_fmt, a, b, c, d)

#define QSH_ERR_5(xx_fmt, a, b, c, d, e)                       \
  MSG_5(MSG_SSID_A2, MSG_LEGACY_ERROR, xx_fmt, a, b, c, d, e)

/*! Macro for subtracting two timestamps with a given modulus */
#define SUB_MOD( a, b, m ) \
      ( ( (a) >= (b) ) ? ( (a) - (b) ) : ( (m) + (a) - (b) ) )

/*==============================================================================

                    EXTERNAL FUNCTION PROTOTYPES

==============================================================================*/
static inline void qsh_crit_sect_init( qsh_crit_sect_s * lock_ptr )
{
#ifdef FEATURE_POSIX
   pthread_mutexattr_t attr;
   QSH_ASSERT( lock_ptr != NULL );

   if( pthread_mutexattr_init(&attr) !=0 )
   {
     ERR_FATAL("failed on pthread_mutexattr_init",0,0,0);
   }
   if( pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE) !=0 )
   {
     ERR_FATAL("failed on pthread_mutexattr_settype",0,0,0);
   }
   if( pthread_mutex_init(lock_ptr, &attr) != 0 )
   {
     ERR_FATAL("failed on thread_mutex_init",0,0,0);
   }
   if(pthread_mutexattr_destroy(&attr)!=0)
   {
     MSG_HIGH(
      "WARNING: Potential memory leak. Could not destroy pthread_mutex_attr",
      0,0,0);
   }

#else
  rex_init_crit_sect( (rex_crit_sect_type*) lock_ptr );
#endif
}

static inline void qsh_crit_sect_deinit( qsh_crit_sect_s * lock_ptr )
{
#ifdef FEATURE_POSIX

  QSH_ASSERT( lock_ptr != NULL );

  if( pthread_mutex_destroy(lock_ptr) !=0 )
  {
    ERR_FATAL("Couldn't destroy pthread mutex",0,0,0);
  }

#endif /* FEATURE_POSIX */
}

/*==============================================================================

  FUNCTION:  qsh_mem_alloc

==============================================================================*/
/*!
  @brief
  Allocates memory from modem heap. Guarantees that pointer is not NULL.
*/
/*============================================================================*/
static inline void * qsh_mem_alloc
(
  size_t  size
)
{
  void * mem_ptr;

  /* allocate memory from modem heap (using _CRIT because failure results
    in a crash) */
  mem_ptr = modem_mem_alloc(size, MODEM_MEM_CLIENT_UTILS_CRIT);

  /* verify memory was allocated */
  QSH_ASSERT(mem_ptr != NULL);
  
  return mem_ptr;
}

/*==============================================================================

  FUNCTION:  qsh_ulog_create_manual

==============================================================================*/
/*!
  @brief
  Creates a ULog with the required buffer size.
*/
/*============================================================================*/
static inline void qsh_ulog_create_manual
(
  /* pointer to ulog handle */
  ULogHandle *    handle_ptr,

  /* name of ulog */
  const char *    name_str,

  /* lock type */
  ULOG_LOCK_TYPE  lock_type,

  /* pointer to static buffer */
  void *          buffer_ptr,

  /* size of buffer */
  uint32          buffer_size_bytes
)
{
  ULogResult result;

  /* initialize ulog */
  result = ULogCore_LogCreate(
    handle_ptr,
    name_str,
    0, /* bufferSize = 0 to leave buffer unallocated */
    ULOG_MEMORY_LOCAL,
    lock_type,
    ULOG_INTERFACE_REALTIME);

  /* verify init was incomplete (still need to set buffer addr) */
  QSH_ASSERT_LOG(result == ULOG_ERR_INITINCOMPLETE, 
    "result = %d", result, 0, 0);

  /* assign memory for buffer */
  result = ULogCore_MemoryAssign(
    *handle_ptr,
    ULOG_VALUE_BUFFER,
    buffer_ptr,
    buffer_size_bytes);

  QSH_ASSERT_LOG(result == DAL_SUCCESS, "result = %d", result, 0, 0);

  /* enable ulog */
  result = ULogCore_Enable(*handle_ptr);

  QSH_ASSERT_LOG(result == DAL_SUCCESS, "result = %d", result, 0, 0);
}

/*==============================================================================

  FUNCTION:  qsh_cache_flush

==============================================================================*/
/*!
  @brief
  Flushes data cache at specified addr/size.
*/
/*============================================================================*/
static inline void qsh_cache_flush
(
  void *  addr,
  size_t  size
)
{
#ifdef FEATURE_QSH_ON_TARGET
  qurt_mem_cache_clean((qurt_addr_t) addr, size, 
    QURT_MEM_CACHE_FLUSH_ALL, QURT_MEM_DCACHE);
#else
  (void) addr;
  (void) size;
#endif /* FEATURE_QSH_ON_TARGET */
}

/*==============================================================================

  FUNCTION:  qsh_cache_flush_ulog

==============================================================================*/
/*!
  @brief
  Flushes cache at all data associated with the ulog.
*/
/*============================================================================*/
static inline void qsh_cache_flush_ulog
(
  ULogHandle  ulog_handle
)
{
  /* sizeof(ULOG_TYPE) */
#define QSH_CACHE_FLUSH_ULOG_HANDLE_SIZE_BYTES          124

  /* offsetof(ULOG_TYPE, buffer) */
#define QSH_CACHE_FLUSH_ULOG_BUFFER_PTR_OFFSET_BYTES    36

  /* offsetof(ULOG_TYPE, bufSize) */
#define QSH_CACHE_FLUSH_ULOG_BUFFER_SIZE_OFFSET_BYTES   40

  /* pointer to ULOG_TYPE */
  uint8 * ulog_ptr = (uint8 *) ulog_handle;

  /* flush data at handle */
  qsh_cache_flush(ulog_ptr, QSH_CACHE_FLUSH_ULOG_HANDLE_SIZE_BYTES);

  /* flush data at buffer */
  qsh_cache_flush(
    *((char **) (ulog_ptr + QSH_CACHE_FLUSH_ULOG_BUFFER_PTR_OFFSET_BYTES)),
    *((uint32 *) (ulog_ptr + QSH_CACHE_FLUSH_ULOG_BUFFER_SIZE_OFFSET_BYTES)));
}

/*==============================================================================

  FUNCTION:  qsh_sleep_ms

==============================================================================*/
/*!
  @brief
  Sleeps for the given time in ms.
*/
/*============================================================================*/
static inline void qsh_sleep_ms
(
  uint32  time_ms
)
{
  /* qurt api takes microseconds */
  QSH_ASSERT(
    qurt_timer_sleep((qurt_timer_duration_t) (time_ms * 1000)) == QURT_EOK);
}

/*==============================================================================

  FUNCTION:  qsh_atomic_inc

==============================================================================*/
/*!
  @brief
  Atomic increment.
*/
/*============================================================================*/
static inline void qsh_atomic_inc
(
  uint32 *  target
)
{
#ifdef FEATURE_QSH_ON_TARGET
  qurt_atomic_inc((unsigned int *) target);
#else
  (*target) ++;
#endif /* FEATURE_QSH_ON_TARGET */
}

/*==============================================================================

  FUNCTION:  qsh_atomic_inc_return

==============================================================================*/
/*!
  @brief
  Atomic increment and return.
*/
/*============================================================================*/
static inline uint32 qsh_atomic_inc_return
(
  uint32 *  target
)
{
#ifdef FEATURE_QSH_ON_TARGET
  return (uint32) qurt_atomic_inc_return((unsigned int *) target);
#else
  (*target) ++;
  return *target;
#endif /* FEATURE_QSH_ON_TARGET */
}

/*==============================================================================

  FUNCTION:  qsh_atomic_dec

==============================================================================*/
/*!
  @brief
  Atomic decrement.
*/
/*============================================================================*/
static inline void qsh_atomic_dec
(
  uint32 *  target
)
{
#ifdef FEATURE_QSH_ON_TARGET
  qurt_atomic_dec((unsigned int *) target);
#else
  (*target) --;
#endif /* FEATURE_QSH_ON_TARGET */
}

/*==============================================================================

  FUNCTION:  qsh_atomic_or

==============================================================================*/
/*!
  @brief
  Atomic OR.
*/
/*============================================================================*/
static inline void qsh_atomic_or
(
  uint32 *  target,
  uint32    mask
)
{
#ifdef FEATURE_QSH_ON_TARGET
  qurt_atomic_or((unsigned int *) target, (unsigned int) mask);
#else
  (*target) |= mask;
#endif /* FEATURE_QSH_ON_TARGET */
}

/*==============================================================================

  FUNCTION:  qsh_atomic_and

==============================================================================*/
/*!
  @brief
  Atomic AND.
*/
/*============================================================================*/
static inline void qsh_atomic_and
(
  uint32 *  target,
  uint32    mask
)
{
#ifdef FEATURE_QSH_ON_TARGET
  qurt_atomic_and((unsigned int *) target, (unsigned int) mask);
#else
  (*target) &= mask;
#endif /* FEATURE_QSH_ON_TARGET */
}

/*==============================================================================

  FUNCTION:  qsh_timetick_get

==============================================================================*/
/*!
  @brief
  Returns the time_tick
*/
/*============================================================================*/
qsh_timetick_t qsh_timetick_get
(
  void
);

/*==============================================================================

  FUNCTION:  qsh_timetick_16_get

==============================================================================*/
/*!
  @brief
  Returns the 16-bit time_tick
*/
/*============================================================================*/
qsh_timetick_16_t qsh_timetick_16_get
(
  void
);

/*==============================================================================

  FUNCTION:  qsh_timetick_diff

==============================================================================*/
/*!
  @brief
  Returns the time difference in ns, this is for 32 bit timetick
*/
/*============================================================================*/
qsh_timetick_t qsh_timetick_diff
(
  qsh_timetick_t first_tick,
  qsh_timetick_t second_tick
);

/*==============================================================================

  FUNCTION:  qsh_timetick_diff_us

==============================================================================*/
/*!
  @brief
  Returns the time difference in us
*/
/*============================================================================*/
static inline qsh_timetick_t qsh_timetick_diff_us
(
  qsh_timetick_t first_tick,
  qsh_timetick_t second_tick
)
{
  return qsh_timetick_diff(first_tick, second_tick) / 1000;
}

/*==============================================================================

  FUNCTION:  qsh_timetick_diff_ms

==============================================================================*/
/*!
  @brief
  Returns the time difference in ms
*/
/*============================================================================*/
static inline qsh_timetick_t qsh_timetick_diff_ms
(
  qsh_timetick_t first_tick,
  qsh_timetick_t second_tick
)
{
  return qsh_timetick_diff_us(first_tick, second_tick) / 1000;
}

/*==============================================================================

  FUNCTION:  qsh_metric_fifo_advance

==============================================================================*/
/*!
  @brief
  Advances current write pointer in metric FIFO and handles wraparound.
*/
/*============================================================================*/
static inline void qsh_metric_fifo_advance
(
  qsh_ext_metric_cfg_s *  metric_cfg_ptr
)
{
  /* verify current index */
  QSH_ASSERT(metric_cfg_ptr->fifo.element_cur_idx <
    metric_cfg_ptr->fifo.element_count_total);
      
  /* increment index */
  metric_cfg_ptr->fifo.element_cur_idx ++;
  
  /* handle wraparound */
  if (metric_cfg_ptr->fifo.element_cur_idx == 
    metric_cfg_ptr->fifo.element_count_total)
  {
    metric_cfg_ptr->fifo.element_cur_idx = 0;
    metric_cfg_ptr->wrap_around_flag = TRUE;
  }
}

#endif /* QSHI_UTIL_H */
