#ifndef FTM_RF_TEST_INTERFACE_H
#define FTM_RF_TEST_INTERFACE_H
/*!
  @file
  ftm_rf_test_interface.h

  @brief
  Common framework to perform radio test in FTM mode
*/

/*======================================================================================================================

  Copyright (c) 2015 - 2016 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document are confidential 
  and proprietary information of Qualcomm Technologies Incorporated and all rights therein are 
  expressly reserved. By accepting this material the recipient agrees that this material and the 
  information contained therein are held in confidence and in trust and will not be used, copied, 
  reproduced in whole or in part, nor its contents revealed in any manner to others without the 
  express written permission of Qualcomm Technologies Incorporated.

======================================================================================================================*/

/*======================================================================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/rffactory/intf/ftm_rf_test_interface.h#1 $

when       who     what, where, why
--------   ---     -----------------------------------------------------------------------------------------------------
03/22/16   zhw     Added support for MSIM CONFIG in radio config
03/21/16   Saul    [FTM_TRM] Antenna Path support.
03/18/16   zhw     Added support for Ant path in radio config
03/08/16   zhw     Added support for Sensitivty, CtoN, Peak Freq measurement in Rx Measure
02/24/16   vv      [FTM_RF_TEST] - Adding enum ftm_trm_rra_property_type  
02/24/16   jfc     Add IQ capture subcommand code and enums
02/19/16   br      Added support for Mod_type change in FTM mode
02/18/16   jfc     [FTM RF TEST] Move subscriber and tech out of RF test header
02/16/16   zc      Adding Number of Rx Bursts for GSM
01/18/16   Saul    [FTM_RF_TEST] Recommended Radio Allocations API
01/04/16   zhw     Remove LTE PATH IDX property
12/14/15   zhw     Added Tx slot & Tx waveform support for GSM
11/20/15   zhw     Added support for LTE Path Index in Tx control
11/15/15   zhw     Added support for LTE Path Index in Rx Measure
10/27/15   zhw     Added support for LTE Path Index & Tx Control Properties
10/22/15   zhw     Added support for sig path property in rx measure and Tx control
09/16/15   zhw     Added support for sig path property in radio config
08/11/15   zhw     Added support for Tx Control Command
08/07/15   zhw     Update Rx Measure Property List. Add Rx Mode support in Radio Config
08/04/15   zhw     Added Tech Override support for Rx Measure Command
07/29/15   zhw     Moved REQ & RSP payload type to interface
                   Added support for Rx Measure command
04/15/15   aro     Added subcriber info in radio config unpack data structure
04/15/15   aro     Doxygen fix
04/15/15   aro     Added error codes to detect uncompression failures
04/15/15   aro     Added support for compressed packet
04/15/15   aro     PACK fix for OFT
04/14/15   aro     Extern C
04/14/15   aro     Added Radio Congig test case
04/14/15   aro     Added generic dispatch mechanism
04/13/15   aro     [1] Added support for radio config handler function
                   [2] Added error reporting
04/13/15   aro     Added common response infrastructure
04/13/15   aro     Restructuring/Renaming
04/13/15   aro     Renamed filenames
04/13/15   aro     Added radio config unpack function
04/08/15   aro     [1] Added error code
                   [2] Rename type/fuunc/variable name
                   [3] Initial support for radio config handler
04/08/15   aro     Fixed subscriber ID enum name
04/08/15   aro     Added Subscriber ID
04/08/15   aro     Added command ID
04/08/15   aro     Added initial type definition
04/08/15   aro     Initial Release

======================================================================================================================*/

#include "comdef.h"

#ifdef __cplusplus
extern "C"
{
#endif

#ifdef T_WINNT
#error code not present
#endif

/*--------------------------------------------------------------------------------------------------------------------*/
/*! Type definition to define the size of the reserved field in FTM test command packet.  */
typedef uint16 ftm_rf_test_comamnd_type;

/*--------------------------------------------------------------------------------------------------------------------*/
/*! Type definition to define the size of the reserved field in FTM test command packet.  */
typedef uint32 ftm_rf_test_field_reserved_type;

/*--------------------------------------------------------------------------------------------------------------------*/
/*! Type definition to define the size of the version field in FTM test command packet.  */
typedef uint32 ftm_rf_test_field_version_type;

/*--------------------------------------------------------------------------------------------------------------------*/
/*! Type definition to define the size of the subscription field in FTM test command packet.  */
typedef uint32 ftm_rf_test_field_subscriber_type;

/*--------------------------------------------------------------------------------------------------------------------*/
/*! Type definition to define the size of the technology field in FTM test command packet.  */
typedef uint32 ftm_rf_test_field_technology_type;

/*--------------------------------------------------------------------------------------------------------------------*/
/*! Type definition to define the error code mask for response packet. Each bit indicates a type of error. */ 
typedef uint32 ftm_rf_test_field_error_code_mask_type;

/*--------------------------------------------------------------------------------------------------------------------*/
/*! Defintion of command header for FTM command request packet */
typedef PACK(struct)
{
  uint8 diag_cmd;
  /*!< Command Identification */

  uint8 diag_subsys_id;
  /*!< Sub-system Identification. For instance - 11 for FTM */

  uint16 rf_subsys_id;
  /*!< RF Mode Identification. 101 for FTM_RF_TEST_C */

  ftm_rf_test_comamnd_type test_command;
  /*!< FTM test command defined by #ftm_rf_test_command_enum_type */

  uint16 req_len;
  /*!< Defines the request length of the FTM diag packet, <b>when the packet is uncompressed</b>. This includes 
  the summation of the size of ftm_rf_test_diag_header_type and size of remaning paylaod (when it is uncompressed) */

  uint16 rsp_len;
  /*!< Defines the response length of the FTM diag packet, <b>when the packet is uncompressed</b>. This includes 
  the summation of the size of ftm_rf_test_diag_header_type and size of remaning paylaod (when it is uncompressed) */

} ftm_rf_test_diag_header_type;

/*--------------------------------------------------------------------------------------------------------------------*/
/*! Defintion of command header for FTM RF Test command request packet */
typedef PACK(struct)
{
  ftm_rf_test_field_reserved_type reserved;
  /*!< Reserved Field */

  ftm_rf_test_field_version_type version;
  /*!< Version of packet */

  ftm_rf_test_field_subscriber_type subscriber;
  /*!< Indicates the subscription ID defined by #ftm_rf_test_command_enum_type */

  ftm_rf_test_field_technology_type technology;
  /*!< Indicates the technology (defined by ftm_rf_technology_type) for which the command is sent */

} ftm_rf_test_command_header_type;

/*--------------------------------------------------------------------------------------------------------------------*/
/*! Defintion of command header for FTM RF Test command request packet 
  V2 moves subscriber and technology fields out of test command header and into payload */
typedef PACK(struct)
{
  ftm_rf_test_field_reserved_type reserved;
  /*!< Reserved Field */

  ftm_rf_test_field_version_type version;
  /*!< Version of packet */

} ftm_rf_test_command_header_type_v2;

/*--------------------------------------------------------------------------------------------------------------------*/
/*! Defintion of command header for FTM RF Test command request packet */
typedef PACK(struct)
{
  ftm_rf_test_diag_header_type diag_header;
  /*!<  Structure defining the header for FTM request packet */

  ftm_rf_test_command_header_type command_header;
  /*!<  Structure defining the command header for RF Test command packet */

} ftm_rf_test_req_header_type;

/*--------------------------------------------------------------------------------------------------------------------*/
/*! Defintion of command header for FTM RF Test command request packet 
  V2 moves subscriber and technology fields out of test command header and into payload */
typedef PACK(struct)
{
  ftm_rf_test_diag_header_type diag_header;
  /*!<  Structure defining the header for FTM request packet */

  ftm_rf_test_command_header_type_v2 command_header;
  /*!<  Structure defining the command header for RF Test command packet */

} ftm_rf_test_req_header_type_v2;

/*--------------------------------------------------------------------------------------------------------------------*/
/*! Defintion of command header for FTM RF Test command response packet */
typedef PACK(struct)
{
  ftm_rf_test_field_reserved_type reserved;
  /*!< Reserved Field */

  ftm_rf_test_field_version_type version;
  /*!< Version of packet */

  ftm_rf_test_field_error_code_mask_type common_error_code;
  /*!< Error code from the common framework */

} ftm_rf_test_command_rsp_header_type;

/*--------------------------------------------------------------------------------------------------------------------*/
/*! Defintion of command header for FTM RF Test command response packet */
typedef PACK(struct)
{
  ftm_rf_test_diag_header_type diag_header;
  /*!<  Structure defining the header for FTM response packet */

  ftm_rf_test_command_rsp_header_type command_header;
  /*!<  Structure defining the command header for RF Test response packet */

} ftm_rf_test_header_rsp_type;

/*====================================================================================================================*/
/*!
  @addtogroup FTM_TEST_TOOLS_CID
  @{
*/

/*--------------------------------------------------------------------------------------------------------------------*/
/*! Enumeration to indicate the type of commands available for FTM test command */
typedef enum
{
  FTM_RF_TEST_CMD_UNASSIGNED = 0, /*!< 0 : Unassigned command */

  FTM_RF_TEST_CMD_RADIO_CONFIGURE = 1, /*!< 1 : Radio Configure command */

  FTM_RF_TEST_CMD_RX_MEASURE = 2, /*!< 2 : Rx Measure command */

  FTM_RF_TEST_CMD_TX_CONTROL = 3, /*!< 3 : Tx Control command */

  FTM_RF_TEST_CMD_QUERY_RECOMMENDED_RADIO_ALLOCATION = 4, /*!< 4 : RF Radio Allocation command */

  FTM_RF_TEST_CMD_RESERVED_5 = 5, /*!< 5 : Reserved  */

  FTM_RF_TEST_CMD_IQ_CAPTURE = 6, /*!< 6 : IQ Capture command */

  FTM_RF_TEST_CMD_MSIM_CONFIG = 7, /*!< 6 : MSIM Config command */

  FTM_RF_TEST_CMD_NUM  /*!< Max : Defines maximum number of command IDs */

} ftm_rf_test_command_enum_type;

/*! @} */ /* FTM_TEST_TOOLS_CID */

/*====================================================================================================================*/
/*!
  @addtogroup FTM_TEST_TOOLS_SUBS
  @{
*/

/*--------------------------------------------------------------------------------------------------------------------*/
/*! Enumeration to indicate the type of subscribers */
typedef enum
{
  FTM_RF_TEST_SUBSCRIBER_0 = 0, /*!< 0 : First subscriber */

  FTM_RF_TEST_SUBSCRIBER_1 = 1, /*!< 1 : Second subscriber */

  FTM_RF_TEST_SUBSCRIBER_NUM,  /*!< Max : Defines maximum number of subscriber IDs */

  FTM_RF_TEST_SUBSCRIBER_NA = 0xFF  /*!< Not Applicable */

} ftm_rf_test_subscriber_enum_type;

/*! @} */ /* FTM_TEST_TOOLS_SUBS */

/*====================================================================================================================*/
/*!
  @addtogroup FTM_TEST_TOOLS_ERROR_CODE
  @{
*/

/*--------------------------------------------------------------------------------------------------------------------*/
/*! Enumeration to indicate the type of subscribers */
typedef enum
{
  FTM_RF_TEST_EC_SUB_ALLOC = 0, /*!< 0 : Indicates subscription allocation failure */

  FTM_RF_TEST_EC_SUB_RELEASE = 1, /*!< 1 : Indicates subscription release failure */

  FTM_RF_TEST_CMD_HANDLER_FAIL = 2, /*!< 2 : Indicates command handlder failure */

  FTM_RF_TEST_EC_NULL_PARAM = 3, /*!< 3 : Indicates NULL parameter */

  FTM_RF_TEST_EC_RSP_CREATION = 4, /*!< 4 : Failure during response creation */

  FTM_RF_TEST_EC_BAD_CMD = 5, /*!< 5 : Bad Command */

  FTM_RF_TEST_EC_BAD_CMD_HANDLER = 6, /*!< 6 : Indicates bad command hander */

  FTM_RF_TEST_EC_UMCOMPRESS_FAIL = 7, /*!< 7 : Uncompress failure */

  FTM_RF_TEST_EC_MALLOC_FAILRE = 8, /*!< 8 : Malloc failure */

  FTM_RF_TEST_EC_NUM  /*!< Max : Defines maximum number of error codes */

} ftm_rf_test_error_code_enum_type;

/*! @} */ /* FTM_TEST_TOOLS_ERROR_CODE */

/*====================================================================================================================*/
/*!
  @addtogroup FTM_TEST_TOOLS_COMMAND Property IDs
  @{
*/

/*====================================================================================================================*/
/*!
  @name Radio Config Property ID

  @brief
  Radio Config Property ID list
*/
/*! @{ */

/*--------------------------------------------------------------------------------------------------------------------*/
/*! Enumeration to indicate the type of properties for FTM Radio Config command. These enumeration are used to define 
the content of #ftm_rf_test_field_property_type in FTM test command packet. 
radio_config_property_names Must be updated when this list is updated.*/ 
typedef enum
{
  FTM_RF_TEST_RADIO_CFG_PROP_UNASSIGNED = 0, /*!< 0 : Unassigned property */

  FTM_RF_TEST_RADIO_CFG_PROP_RX_CARRIER = 1, /*!< 1 : Receiver Carrier Number */

  FTM_RF_TEST_RADIO_CFG_PROP_TX_CARRIER = 2, /*!< 2 : Transmit Carrier Number */

  FTM_RF_TEST_RADIO_CFG_PROP_RFM_DEVICE_PRI = 3, /*!< 3 : Primary RFM device */

  FTM_RF_TEST_RADIO_CFG_PROP_RFM_DEVICE_DIV = 4, /*!< 4 : Diversity RFM device */

  FTM_RF_TEST_RADIO_CFG_PROP_BAND = 5, /*!< 5 : Operating Tech Band */

  FTM_RF_TEST_RADIO_CFG_PROP_CHANNEL = 6, /*!< 6 : Operating Tech channel */

  FTM_RF_TEST_RADIO_CFG_PROP_BANDWIDTH = 7,  /*!< 7 : Radio Bandwidth */

  FTM_RF_TEST_RADIO_CFG_PROP_RX_MODE= 8,  /*!< 8 : Rx Mode: Burst or Continous*/

  FTM_RF_TEST_RADIO_CFG_PROP_SIG_PATH = 9,  /*!< 9 : Signal Path for AT forward */

  FTM_RF_TEST_RADIO_CFG_PROP_ANT_PATH = 10,  /*!< 10 : Antenna Path */

  FTM_RF_TEST_RADIO_CFG_PROP_NUM  /*!< Max : Defines maximum number of properties */

} ftm_rf_test_radio_config_property_type;
      
/*! @} */
      
/*====================================================================================================================*/
/*!
  @name Rx measure Property ID

  @brief
  Rx measure Property ID list
*/
/*! @{ */
      
/*--------------------------------------------------------------------------------------------------------------------*/
/*! Enumeration to indicate the type of properties for FTM Rx Measure Command. 
  These enumeration are used to define the content of #ftm_rf_test_field_property_type in FTM test command packet. 
rx_measure_property_names Must be updated when this list is updated.*/
typedef enum
{
  FTM_RF_MEASURE_PROP_UNASSIGNED = 0, /*!< 0 : Unassigned property */

  FTM_RX_MEASURE_PROP_RX_CARRIER = 1, /*!< 1 : Receiver Carrier Number */

  FTM_RX_MEASURE_PROP_RFM_DEVICE = 2, /*!< 2 : RFM device for this measurement*/

  FTM_RX_MEASURE_PROP_EXPECTED_AGC = 3, /*!< 3 : Expected AGC Value */

  FTM_RX_MEASURE_PROP_AGC = 4, /*!< 4 : RxAGC value */

  FTM_RX_MEASURE_PROP_LNA_GAIN_STATE = 5, /*!< 5 : LNA Gain State */

  FTM_RX_MEASURE_PROP_SIG_PATH = 6,  /*!< 6 : Signal Path for AT forward */  

  FTM_RX_MEASURE_PROP_RX_MODE = 7,  /*!< 7 : GSM Rx Mode: Burst or Continous*/

  FTM_RX_MEASURE_PROP_RX_SLOT = 8,  /*!< 8 : GSM Rx Slot Number */

  FTM_RX_MEASURE_PROP_NUM_OF_BURST = 9,  /*!< 9 : GSM Number of Burst */

  FTM_RX_MEASURE_PROP_SENSITIVITY = 10,  /*!< 10 : Sensitivity*/

  FTM_RX_MEASURE_PROP_CTON = 11,  /*!< 11 : CtoN Measurement */

  FTM_RX_MEASURE_PROP_PEAK_FREQ = 12,  /*!< 12 : Peak Frequency Measurement*/

  FTM_RX_MEASURE_PROP_NUM  /*!< Max : Defines maximum number of properties */

} ftm_rf_test_rx_measure_property_type;

/*! @} */

/*====================================================================================================================*/
/*!
  @name Tx Control Property ID

  @brief
  Tx Control Property ID list
*/
/*! @{ */

/*--------------------------------------------------------------------------------------------------------------------*/
/*! Enumeration to indicate the type of properties for FTM TX CONTROL Command. 
  These enumeration are used to define the content of #ftm_rf_test_field_property_type in FTM test command packet
tx_control_property_names Must be updated when this list is updated.*/
typedef enum
{
  FTM_TX_CONTROL_PROP_UNASSIGNED = 0, /*!< 0 : Unassigned property */

  FTM_TX_CONTROL_PROP_TX_CARRIER = 1, /*!< 1 : Receiver Carrier Number */

  FTM_TX_CONTROL_PROP_RFM_DEVICE = 2, /*!< 2 : RFM device for this measurement*/

  FTM_TX_CONTROL_PROP_TX_ACTION = 3, /*!< 3 : TX ACTION ENABLE/DISABLE */

  FTM_TX_CONTROL_PROP_TX_POWER = 4, /*!< 4 : Transimit Power */

  FTM_TX_CONTROL_PROP_RB_CONFIG = 5, /*!< 5 : LTE Start RB */

  FTM_TX_CONTROL_PROP_SIG_PATH = 6,  /*!< 6 : Signal Path for AT forward */

  FTM_TX_CONTROL_PROP_NUM_OF_RB = 7,  /*!< 7 : LTE Number of RBs */

  FTM_TX_CONTROL_PROP_TX_WAVEFORM = 8,  /*!< 8 : LTE & GSM Tx waveform */

  FTM_TX_CONTROL_PROP_NETWORK_SIGNAL = 9,  /*!< 9 : LTE Network signalling value */

  FTM_TX_CONTROL_PROP_TX_SLOT = 10,  /*!< 10 : GSM TX SLOT Number */

  FTM_TX_CONTROL_PROP_MODULATION_TYPE = 11,  /*!< 11 : LTE Modulation Type */

  FTM_TX_CONTROL_PROP_NUM  /*!< Max : Defines maximum number of properties */

} ftm_rf_test_tx_control_property_type;

/*! @} */

/*====================================================================================================================*/
/*!
  @name Recommended Radio Allocations Property ID

  @brief
  Recommended Radio Allocations Property ID list
*/
/*! @{ */
      
/*--------------------------------------------------------------------------------------------------------------------*/
/*! Enumeration to indicate the type of properties for Recommended Radio Allocations Command. 
  These enumeration are used to define the content of test command packet. 
  property_names[] Must be updated when this list is updated.*/
typedef enum
{
  FTM_TRM_RRA_PROP_UNASSIGNED          = 0,  /*!< Unassigned property */
  FTM_TRM_RRA_PROP_SUB_IDX             = 1,  /*!< Subscription Index from ftm_rf_test_subscriber_enum_type */
  FTM_TRM_RRA_PROP_TECH                = 2,  /*!< Tech from ftm_rf_technology_type values */
  FTM_TRM_RRA_PROP_BAND                = 3,  /*!< Band from sys_band_class_e_type values */
  FTM_TRM_RRA_PROP_CHAN                = 4,  /*!< Chan */
  FTM_TRM_RRA_PROP_BANDWIDTH           = 5,  /*!< Bandwidth in Hz */
  FTM_TRM_RRA_PROP_RXTX                = 6,  /*!< Indicates if request is for Rx or Tx path from ftm_rf_test_device_radio_trx_t*/
  FTM_TRM_RRA_PROP_CHAIN               = 7,  /*!< Chain 0/1/2/3/n */
  FTM_TRM_RRA_PROP_CARRIER_IDX         = 8,  /*!< Carrier Index 0/1/2/n */
  FTM_TRM_RRA_PROP_RESOURCE            = 9,  /*!< Resource from ftm_trm_resource_type values */
  FTM_TRM_RRA_PROP_REASON              = 10, /*!< Resource Reason from ftm_trm_reason_type values */
  FTM_TRM_RRA_PROP_PATH_FUNCTIONALITY  = 11, /*!< Path Functionality from ftm_trm_path_functionality_type values */
  FTM_TRM_RRA_PROP_DEVICE              = 12, /*!< Device from rfm_device_enum_type values */
  FTM_TRM_RRA_PROP_SIGNAL_PATH         = 13, /*!< Signal */
  FTM_TRM_RRA_PROP_ANTENNA_PATH        = 14, /*!< Antenna Path */

  /* ADD MORE ITEMS ABOVE THIS LINE */
  FTM_TRM_RRA_PROP_NUM  /*!< Max : Defines maximum number of properties */

} ftm_trm_rra_property_type;

/*--------------------------------------------------------------------------------------------------------------------*/
/*! Enumeration to indicate the type of properties for FTM MSIN CONFIG Command. 
  These enumeration are used to define the content of #ftm_rf_test_field_property_type in FTM test command packet. 
rx_measure_property_names Must be updated when this list is updated.*/
typedef enum
{
  FTM_MSIM_CONFIG_PROP_UNASSIGNED = 0, /*!< 0 : Unassigned property */

  FTM_MSIM_CONFIG_PROP_SUBSCRIBER = 1, /*!< 1 : FTM Subscriber */

  FTM_MSIM_CONFIG_PROP_TECH = 2,       /*!< 2 : SUB FTM RF TECHNOLOGY */

  FTM_MSIM_CONFIG_PROP_SCENARIO = 3,   /*!< 3 : SUB SCENARIO */

  FTM_MSIM_CONFIG_PROP_NUM             /*!< Max : Defines maximum number of properties */

} ftm_rf_test_msim_config_property_type;

/*! @} */



/*====================================================================================================================*/
/*!
  @name IQ Capture Property ID

  @brief
  IQ Capture Property ID list
*/
/*! @{ */

/*--------------------------------------------------------------------------------------------------------------------*/
/*! Enumeration to indicate the type of properties for FTM IQ Capture Command. 
  These enumeration are used to define the content of #ftm_rf_test_field_property_type in FTM test command packet
tx_control_property_names Must be updated when this list is updated.*/
typedef enum
{
  FTM_IQ_CAPTURE_PROP_UNASSIGNED = 0, /*!< 0 : Unassigned property */

  FTM_IQ_CAPTURE_PROP_SUBSCRIBER = 1, /*!< 1 : Subscriber Number */

  FTM_IQ_CAPTURE_PROP_TECHNOLOGY = 2, /*!< 2 : Technology */

  FTM_IQ_CAPTURE_PROP_TX_CARRIER = 3, /*!< 3 : Transmit Carrier Number */

  FTM_IQ_CAPTURE_PROP_RX_CARRIER = 4, /*!< 4 : Receiver Carrier Number */

  FTM_IQ_CAPTURE_PROP_RFM_DEVICE = 5, /*!< 5 : RFM device for this measurement*/

  FTM_IQ_CAPTURE_PROP_SIG_PATH = 6,  /*!< 6 : Signal Path */

  FTM_IQ_CAPTURE_PROP_ACTION_GET_CFG = 7,   /*!< 7 : IQ capture action, get config */
  FTM_IQ_CAPTURE_PROP_ACTION_ACQUIRE = 8,   /*!< 8 : IQ capture action, acquire samples */
  FTM_IQ_CAPTURE_PROP_ACTION_FETCH = 9,     /*!< 9 : IQ capture action, retrieve samples */
  FTM_IQ_CAPTURE_PROP_ACTION_EST_SENS = 10, /*!< 10 : IQ capture action, retrieve estimated sensitivity */
  FTM_IQ_CAPTURE_PROP_ACTION_CTON = 11,     /*!< 11 : IQ capture action, retrieve CtoN */
  FTM_IQ_CAPTURE_PROP_ACTION_PEAK_FREQ = 12,/*!< 12 : IQ capture action, retrieve CtoN */

  FTM_IQ_CAPTURE_PROP_IQ_CAPTURE_SOURCE = 13, /*!< 13 : IQ capture source */

  FTM_IQ_CAPTURE_PROP_SAMPLE_SIZE = 14, /*!< 14 : Sample size */

  FTM_IQ_CAPTURE_PROP_IQ_DATA_FORMAT = 15, /*!< 15 : IQ data format */

  FTM_IQ_CAPTURE_PROP_SAMP_FREQ = 16, /*!< 16 : Sampling frequency */

  FTM_IQ_CAPTURE_PROP_MAX_DIAG_SIZE = 17, /*!< 17 : Maximum support diag size  */

  FTM_IQ_CAPTURE_PROP_SAMPLE_OFFSET = 18, /*!< 18 : Offset of bytes returned */

  FTM_IQ_CAPTURE_PROP_NUM_SAMPLE_BYTES = 19, /*!< 19 : Number of sample bytes returned */

  FTM_IQ_CAPTURE_PROP_EST_SENSITIVITY = 20, /*!< 20 : Estimated sensitivity */

  FTM_IQ_CAPTURE_PROP_CTON = 21, /*!< 21 : Calculated C/N */

  FTM_IQ_CAPTURE_PROP_CW_PEAK_FREQ = 22, /*!< 22 : CW peak frequency */

  FTM_IQ_CAPTURE_PROP_NUM,  /*!< Max : Defines maximum number of properties */

} ftm_rf_test_iq_capture_property_type;

/*! @} */



/*! @} */ /* FTM_TEST_TOOLS_CMD_IQ_CAPTURE */

#ifdef T_WINNT
#error code not present
#endif

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* FTM_RF_TEST_INTERFACE_H */

