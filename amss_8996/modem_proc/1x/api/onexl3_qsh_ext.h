#ifndef __ONEXL3_QSH_EXT_H__
#define __ONEXL3_QSH_EXT_H__
/*===========================================================================

         I N T E R F A C E   W I T H   Q S H    M O D U L E

DESCRIPTION
  This contains all the data and API declarations for the interface with 
  QSH (Qualcomm Sherlock Holmes) module.

  Copyright (c) 2015 Qualcomm Technologies, Inc. 
  All Rights Reserved Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies, Inc. and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies, Inc.


===========================================================================*/
/*===========================================================================

                      EDIT HISTORY FOR FILE

$PVCSPath: L:/src/asw/MSM5100/CP_REL_A/vcs/onex_voice_adapt_if.h   1.8   10 Mar 2015 13:34:04   azafer  $
$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/1x/api/onexl3_qsh_ext.h#1 $ $DateTime: 2016/03/28 23:03:55 $ $Author: mplcsds1 $


when       who     what, where, why
--------   ---     ------------------------------------------------------------
10/26/15   agh     Initial Revision

  ============================================================================*/

/*----------------------------------------------------------------------------
  Include files for Module
----------------------------------------------------------------------------*/
#include "comdef.h"    /* Definition for basic types and macros */
#include "qsh.h"


/* MACRO DEFINITIONS */

#define ONEXL3_QSH_MAJOR_VER 1
#define ONEXL3_QSH_MINOR_VER 1

/* Enumeration for the type of Metrics reported by 1X_CP */
typedef enum {
  ONEXL3_QSH_METRIC_NONE = -1,
    
  ONEXL3_QSH_METRIC_CELL_INFO = 0,  
  /* This metric is reported on 1X cell change and is not periodic */
  
  ONEXL3_QSH_METRIC_RF_PARAMS = 1,
  /* This metric is reported periodically every 200 ms */
  
  ONEXL3_QSH_METRIC_MAX
}onexl3_qsh_metric_e;

/* Structures to hold metrics to be reported to QSH module */

/* CDMA cell info to be reported to QSH on cell change */
typedef struct
{    
  qsh_metric_hdr_s hdr;  /* This header has the timestamp (uint16) */
  uint8  cfg_msg_seq;    /* Configuration message sequence number */
  uint8  band_class;     /* Band class of current cell */
  uint16 freq;           /* Downlink Frequency */
  uint16 pilot_pn;       /* Pilot PN of current channel */
  uint16 base_id;        /* Base station identification */
  uint16 reg_zone;       /* Registration zone */
  uint16 sid;            /* System identification */
  uint16 nid;            /* Network identification */
  
} onexl3_qsh_metric_cell_info_s;


/* CDMA RF parameters to be reported periodically */
typedef struct
{
  qsh_metric_hdr_s  hdr; /* This header has the timestamp (uint16) */
  uint16 rssi0; /* RSSI on primary antenna (in dBm) */ 
  uint16 rssi1; /* RSSI on diversity antenna (in dBm) */
  uint16 ecio0; /* EcIo on primary antenna (in dB) */
  uint16 ecio1; /* EcIo on diversity antenna (in dB) */
  uint16 rssi_comb; /* Combined RSSI value */
  uint16 ecio_comb; /* Combined EcIo value */
  int16  tx_pwr;    /* Tx AGC value (in dB) equivalent to 0x119D log packet */
  
} onexl3_qsh_metric_rf_params_s;

#endif /* __ONEXL3_QSH_EXT_H__ */
