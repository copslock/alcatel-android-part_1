/*
   Copyright (C) 2015-2016 QUALCOMM Technologies, Inc.
   All Rights Reserved.
   Confidential and Proprietary - Qualcomm Technologies, Inc.

   $Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/avs/vsd/vagent/src/vagent.c#5 $
   $Author: rajatm $
*/

/****************************************************************************
 * INCLUDE HEADER FILES                                                     *
 ****************************************************************************/

/* CORE APIs. */
#include <stddef.h>
#include <string.h>
#include "err.h"
#include "msg.h"
#include "err.h"
#include "rcinit.h"
#include "mmstd.h"

/* APR APIs. */
#include "apr_errcodes.h"
#include "apr_objmgr.h"
#include "apr_lock.h"
#include "apr_memmgr.h"
#include "apr_event.h"
#include "apr_thread.h"

/* CM APIs. */
#include "cm.h"
#include "cm_dualsim.h"

/* VSD APIs. */
#include "drv_api.h"
#include "vs_task.h"

/* VADAPTER APIs. */
#include "iva_if.h"
#include "iva_iresource_if.h"
#include "gva_if.h"
#include "gva_iresource_if.h"
#include "wva_if.h"
#include "wva_iresource_if.h"
#include "tva_if.h"
#include "tva_iresource_if.h"
#include "cva_if.h"
#include "cva_iresource_if.h"

/* ECALL APIs*/
#include "ecall_voice_if.h"

/* SELF APIs. */
#include "vagent_if.h"
#include "vagent_i.h"


/*****************************************************************************
 * Defines                                                                   *
 ****************************************************************************/

/* Currently voice agent supports two subscriptions.
 * One VSID per subscription
 */
#define VAGENT_MAX_NUM_OF_SESSIONS_V ( 2 )

/*
 * Represent that number of voice sessions(CVD) that can exist concurrently 
 * at a time in the system.
 */
#define VAGENT_MAX_NUM_OF_RESOURCE_V ( 1 )

/*
 * Represents ID's associated to modules among which voice agent sequences
 * the voice resource usage.
 */
#define VAGENT_MODULE_ID_CVA   ( 1 )
#define VAGENT_MODULE_ID_GVA   ( 2 )
#define VAGENT_MODULE_ID_WVA   ( 3 )
#define VAGENT_MODULE_ID_TVA   ( 4 )
#define VAGENT_MODULE_ID_IVA   ( 5 )
#define VAGENT_MODULE_ID_ECALL ( 6 )

/*****************************************************************************
 * Definitions                                                               *
 ****************************************************************************/

/*****************************************************************************
 * Global Variables                                                          *
 ****************************************************************************/

static apr_lock_t vagent_int_lock;
static apr_event_t vagent_control_event;

static apr_memmgr_type vagent_heapmgr;
static uint8_t vagent_heap_pool[ VAGENT_HEAP_SIZE_V ];

static apr_objmgr_t vagent_objmgr;
static apr_objmgr_object_t vagent_object_table[ VAGENT_MAX_OBJECTS_V ];

static apr_list_t vagent_nongating_work_pkt_q;
static apr_list_t vagent_free_work_pkt_q;
static vagent_work_item_t vagent_work_pkts[ VAGENT_NUM_WORK_PKTS_V ];

static apr_event_t vagent_work_event;
static apr_thread_t vagent_thread;
static uint8_t vagent_task_stack[ VAGENT_TASK_STACK_SIZE ];

static vagent_modem_subs_object_t* vagent_modem_subs_obj_list[VAGENT_MAX_NUM_OF_SESSIONS_V];
static vagent_session_object_t* vagent_session_obj_list[VAGENT_MAX_NUM_OF_SESSIONS_V];
static apr_lock_t vagent_global_lock;

static bool_t vagent_is_initialized = FALSE;

static uint32_t vagent_num_available_resource = VAGENT_MAX_NUM_OF_RESOURCE_V;
  /**< Indicates total number of voice resource available for grant. */


/****************************************************************************
 * COMMON INTERNAL ROUTINES                                                 *
 ****************************************************************************/

VAGENT_INTERNAL void vagent_int_lock_fn ( 
  void
)
{
  apr_lock_enter( vagent_int_lock );
}

VAGENT_INTERNAL void vagent_int_unlock_fn (
  void
)
{
  apr_lock_leave( vagent_int_lock );
}


/****************************************************************************
 * CMDs/EVENTs PACKET PREPARE/DISPATCHER/FREE ROUTINES                      *
*****************************************************************************/

VAGENT_INTERNAL uint32_t vagent_free_event_packet (
  vagent_event_packet_t* packet
)
{

  uint32_t rc = APR_EOK;

  if ( packet != NULL )
  {
    if( packet->params != NULL )
    {
      /* Free the memory - p_cmd_packet->params. */
      apr_memmgr_free( &vagent_heapmgr, packet->params );
      packet->params = NULL;
    }
 
    /* Free the memeory - p_cmd_packet. */
    apr_memmgr_free( &vagent_heapmgr, packet );
    packet= NULL;
  }
  return rc;

 }


/**
 * This is a common routine that prepares the incoming events and dispatches 
 * (signals) to voice agent. Actual processing of the events would happen in 
 * voice agent context. 
 * This function executes in calling task (server) context.
 */
VAGENT_INTERNAL uint32_t vagent_prepare_and_dispatch_event_packet (
 void* session_context,
 uint32_t event_id,
 void* params,
 uint32_t size
)
{

  uint32_t rc = APR_EOK;
  vagent_event_packet_t* packet = NULL;
  vagent_work_item_t* work_item = NULL;

  for ( ;; )
  {
    packet = ( ( vagent_event_packet_t* ) apr_memmgr_malloc( &vagent_heapmgr,
                                           sizeof( vagent_event_packet_t ) ) );
    if ( packet == NULL )
    {
      VAGENT_REPORT_FATAL_ON_ERROR( APR_ENORESOURCE );
      rc = APR_ENORESOURCE;
      break;
    }

    packet->session_context = session_context;
    packet->event_id = event_id;
    packet->params = NULL;

    if ( ( size > 0 ) && ( params != NULL ) )
    {
      packet->params = apr_memmgr_malloc(  &vagent_heapmgr, size );

      if ( packet->params == NULL )
      {
        rc = APR_ENORESOURCE;
        VAGENT_REPORT_FATAL_ON_NO_MEMORY( rc, size );
        ( void ) vagent_free_event_packet( packet );
        break;
      }
      mmstd_memcpy( packet->params, size, params, size );
    }

    /* Get a free command structure. */
    rc = apr_list_remove_head( &vagent_free_work_pkt_q,
                               ( ( apr_list_node_t** ) &work_item ) );
    if ( rc )
    {
      rc = APR_ENORESOURCE;
      VAGENT_REPORT_FATAL_ON_ERROR( rc );
      break;
    }

    work_item->packet = packet;
    /* Add to incoming request work queue */
    rc = apr_list_add_tail( &vagent_nongating_work_pkt_q, &work_item->link );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "VAGENT: Adding to packet queue failed. rc=(0x%08x)", rc );
      /* Add back to vagent_free_work_pkt_q */
      work_item->packet = NULL;
      ( void ) apr_list_add_tail( &vagent_free_work_pkt_q, &work_item->link );
    }
    else
    {
      /**
       * Signal voice agent thread to run
       */
      vagent_signal_run();
    }
    break;
  } /* for ( ;; ) */

  return rc;

}

/****************************************************************************
 * VAGENT OBJECT CREATION, DESTRUCTION AND INITIALISATION ROUTINES             *
 ****************************************************************************/

VAGENT_INTERNAL uint32_t vagent_mem_alloc_object (
  uint32_t size,
  vagent_object_t** ret_object
)
{

  uint32_t rc = APR_EOK;
  vagent_object_t* vagent_obj;
  apr_objmgr_object_t* objmgr_obj;

  for ( ;; )
  {
  if ( ret_object == NULL )
  {
      rc = APR_EBADPARAM;
      break;
  }

    /* Allocate memory for the VAGENT object. */
    vagent_obj = apr_memmgr_malloc( &vagent_heapmgr, size );
    if ( vagent_obj == NULL )
    {
      rc = APR_ENORESOURCE;
      VAGENT_REPORT_FATAL_ON_NO_MEMORY( rc, size );
      break;
    }

    /* Allocate a new handle for the MVS object. */
    rc = apr_objmgr_alloc_object( &vagent_objmgr, &objmgr_obj );
    if ( rc )
    {
      rc = APR_ENORESOURCE;
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, 
             "VAGENT: ran out of objects, rc=(0x%08x)", rc );
      apr_memmgr_free( &vagent_heapmgr, vagent_obj );
      break;
    }

    /* Use the custom object type. */
    objmgr_obj->any.ptr = vagent_obj;

    /* Initialize the base MVS object header. */
    vagent_obj->header.handle = objmgr_obj->handle;
    vagent_obj->header.type = VAGENT_OBJECT_TYPE_ENUM_UNINITIALIZED;

  *ret_object = vagent_obj;
    break;
  } /* for ( ;; ) */

  return rc;

}


VAGENT_INTERNAL uint32_t vagent_mem_free_object (
  vagent_object_t* object
)
{

  if ( object == NULL )
  {
    return APR_EBADPARAM;
  }

  /* Free the object memory and object handle. */
  ( void ) apr_objmgr_free_object( &vagent_objmgr, object->header.handle );
  apr_memmgr_free( &vagent_heapmgr, object );

  return APR_EOK;

}


VAGENT_INTERNAL uint32_t vagent_create_modem_subs_object ( 
  vagent_modem_subs_object_t** ret_subs_obj
)
{

  uint32_t rc = APR_EOK;
  vagent_modem_subs_object_t* subs_obj = NULL;

  for ( ;; ) 
  {
  if ( ret_subs_obj == NULL )
  {
      rc = APR_EBADPARAM;
      break;
  }

  rc = vagent_mem_alloc_object( sizeof( vagent_modem_subs_object_t ),
                                ( ( vagent_object_t** ) &subs_obj ) );
  if ( rc )
  {
      /* send the error code returned by vagent_mem_alloc_object to
         the caller */
      break;
  }

    /* Initialize the modem subscription object. */
    subs_obj->header.type = VAGENT_OBJECT_TYPE_ENUM_MODEM_SUBSCRIPTION;
    subs_obj->asid = SYS_MODEM_AS_ID_NONE;
    subs_obj->vsid = VAGENT_VSID_UNDEFINED_V;
    subs_obj->session_obj = NULL;
    subs_obj->call_refcount = 0;
    subs_obj->event_call_orig_dbg_count = 0;
    subs_obj->event_call_incom_dbg_count = 0;
    subs_obj->event_call_end_dbg_count = 0;

  *ret_subs_obj = subs_obj;
    break;
  } /* for ( ;; ) */

  return rc;

}


VAGENT_INTERNAL uint32_t vagent_create_session_object ( 
  vagent_session_object_t** ret_session_obj
)
{

  uint32_t rc = APR_EOK;
  vagent_session_object_t* session_obj = NULL;

  for ( ;; )
  {
  if ( ret_session_obj == NULL )
  {
      rc = APR_EBADPARAM;
      break;
  }

  rc = vagent_mem_alloc_object( sizeof( vagent_session_object_t ),
                             ( ( vagent_object_t** ) &session_obj ) );
  if ( rc )
  {
      /* send the error code returned by vagent_mem_alloc_object to
         the caller */
      break;
  }

    /* Initialize the simple job object. */
    session_obj->header.type = VAGENT_OBJECT_TYPE_ENUM_SESSION;
    session_obj->vsid = VAGENT_VSID_UNDEFINED_V;
    session_obj->modem_subs_obj = NULL;
    session_obj->state = VAGENT_SUBS_STATE_INACTIVE;
    session_obj->current_module = VAGENT_MODULE_ID_UNDEFINED;
    session_obj->pending_module = VAGENT_MODULE_ID_UNDEFINED;
    session_obj->iva_handle = APR_NULL_V;
    session_obj->gva_handle = APR_NULL_V;
    session_obj->wva_handle = APR_NULL_V;
    session_obj->cva_handle = APR_NULL_V;
    session_obj->tva_handle = APR_NULL_V;

    *ret_session_obj = session_obj;
    break;
  }

  return rc;

}


VAGENT_INTERNAL void vagent_log_voice_adapter_event_info(
  void* session_context,
  uint32_t event_id
)
{

  vagent_session_object_t* session_obj = NULL;

  if ( session_context == NULL )
  {
    return;
  }

  session_obj = (vagent_session_object_t*)session_context;

  switch( event_id )
  {
   case IVA_IRESOURCE_EVENT_REQUEST:
    {
      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "VAGENT: V_ADAPTER_CB:"
             "VSID=(0x%08x): ASID=(%d): IVA_IRESOURCE_EVENT_REQUEST: "
             "RefCount=(%d)", session_obj->vsid, 
             session_obj->modem_subs_obj->asid, 
             session_obj->modem_subs_obj->call_refcount );
    }
    break;

   case IVA_IRESOURCE_EVENT_RELEASED:
    {
      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "VAGENT: V_ADAPTER_CB:"
             "VSID=(0x%08x): ASID=(%d): IVA_IRESOURCE_EVENT_RELEASED: "
             "RefCount=(%d)", session_obj->vsid, 
             session_obj->modem_subs_obj->asid, 
             session_obj->modem_subs_obj->call_refcount );
    }
    break;

   case GVA_IRESOURCE_EVENT_REQUEST:
    {
      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "VAGENT: V_ADAPTER_CB:"
             "VSID=(0x%08x): ASID=(%d): GVA_IRESOURCE_EVENT_REQUEST: "
             "RefCount=(%d)", session_obj->vsid, 
             session_obj->modem_subs_obj->asid, 
             session_obj->modem_subs_obj->call_refcount );
    }
    break;

   case GVA_IRESOURCE_EVENT_RELEASED:
    {
      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "VAGENT: V_ADAPTER_CB:"
             "VSID=(0x%08x): ASID=(%d): GVA_IRESOURCE_EVENT_RELEASED: "
             "RefCount=(%d)", session_obj->vsid, 
             session_obj->modem_subs_obj->asid, 
             session_obj->modem_subs_obj->call_refcount );
    }
    break;

   case WVA_IRESOURCE_EVENT_REQUEST:
    {
      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "VAGENT: V_ADAPTER_CB:"
             "VSID=(0x%08x): ASID=(%d): WVA_IRESOURCE_EVENT_REQUEST: "
             "RefCount=(%d)", session_obj->vsid, 
             session_obj->modem_subs_obj->asid, 
             session_obj->modem_subs_obj->call_refcount );
    }
    break;

   case WVA_IRESOURCE_EVENT_RELEASED:
    {
      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "VAGENT: V_ADAPTER_CB:"
             "VSID=(0x%08x): ASID=(%d): WVA_IRESOURCE_EVENT_RELEASED: "
             "RefCount=(%d)", session_obj->vsid, 
             session_obj->modem_subs_obj->asid, 
             session_obj->modem_subs_obj->call_refcount );
    }
    break;

   case TVA_IRESOURCE_EVENT_REQUEST:
    {
      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "VAGENT: V_ADAPTER_CB:"
             "VSID=(0x%08x): ASID=(%d): TVA_IRESOURCE_EVENT_REQUEST: "
             "RefCount=(%d)", session_obj->vsid, 
             session_obj->modem_subs_obj->asid, 
             session_obj->modem_subs_obj->call_refcount );
    }
    break;

   case TVA_IRESOURCE_EVENT_RELEASED:
    {
      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "VAGENT: V_ADAPTER_CB:"
             "VSID=(0x%08x): ASID=(%d): TVA_IRESOURCE_EVENT_RELEASED: "
             "RefCount=(%d)", session_obj->vsid, 
             session_obj->modem_subs_obj->asid, 
             session_obj->modem_subs_obj->call_refcount );
    }
    break;

   case CVA_IRESOURCE_EVENT_REQUEST:
    {
      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "VAGENT: V_ADAPTER_CB:"
             "VSID=(0x%08x): ASID=(%d): CVA_IRESOURCE_EVENT_REQUEST: "
             "RefCount=(%d)", session_obj->vsid, 
             session_obj->modem_subs_obj->asid, 
             session_obj->modem_subs_obj->call_refcount );
    }
    break;

   case CVA_IRESOURCE_EVENT_RELEASED:
    {
      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "VAGENT: V_ADAPTER_CB:"
             "VSID=(0x%08x): ASID=(%d): CVA_IRESOURCE_EVENT_RELEASED: "
             "RefCount=(%d)", session_obj->vsid, 
             session_obj->modem_subs_obj->asid, 
             session_obj->modem_subs_obj->call_refcount );
    }
    break;

   case ECALL_VOICE_IRESOURCE_EVENT_REQUEST:
    {
      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "VAGENT: V_ADAPTER_CB:"
             "VSID=(0x%08x): ASID=(%d): ECALL_VOICE_IRESOURCE_EVENT_REQUEST: "
             "RefCount=(%d)", session_obj->vsid, 
             session_obj->modem_subs_obj->asid, 
             session_obj->modem_subs_obj->call_refcount );
    }
    break;

   case ECALL_VOICE_IRESOURCE_EVENT_RELEASED:
    {
      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED, "VAGENT: V_ADAPTER_CB:"
             "VSID=(0x%08x): ASID=(%d): ECALL_VOICE_IRESOURCE_EVENT_RELEASED: "
             "RefCount=(%d)", session_obj->vsid, 
             session_obj->modem_subs_obj->asid, 
             session_obj->modem_subs_obj->call_refcount );
    }
    break;

    default:
    break;

  }

  return;

}


/****************************************************************************
 *  VOICE AGENT - CALLBACK FUNCTIONS                                        * 
 *  These function executes in server (CM, voice adapter) context           * 
 *  1. vagent_cm_call_event_cb                                              * 
 *  2. vagent_cm_subs_info_cb                                               * 
 *  3. vagent_voice_adapter_event_cb                                        * 
*****************************************************************************/

VAGENT_INTERNAL void vagent_cm_call_event_cb (
  cm_call_event_e_type call_event,
  const cm_mm_call_info_s_type *call_info_ptr
)
{
   
  uint32_t rc = APR_EOK;
  vagent_modem_subs_object_t* subs_obj = NULL;
  vagent_eventi_cm_call_event_t call_status;

  if ( NULL == call_info_ptr )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
         "VAGENT: CM_CALL_EVENT_CB: call_info_ptr is NULL" );
    return;
  }

  if( ( call_info_ptr->asubs_id < SYS_MODEM_AS_ID_1 )||
      ( call_info_ptr->asubs_id > SYS_MODEM_AS_ID_2 ) )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "VAGENT: CM_CALL_EVENT_CB: ASID=(%d) not supported", 
           call_info_ptr->asubs_id );
    return;
  }

  MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,
         "VAGENT: CM_CALL_EVENT_CB: event=(%d), ASID=(%d)",
          call_event, call_info_ptr->asubs_id );

  subs_obj = vagent_modem_subs_obj_list[ call_info_ptr->asubs_id ];
  call_status.event_id = call_event;
  call_status.asid  = call_info_ptr->asubs_id;
  
  /* Only following CM events needs to be queued and handled.*/
  switch( call_event )
  {
    case CM_CALL_EVENT_ORIG:
    case CM_CALL_EVENT_INCOM:
    case CM_CALL_EVENT_ANSWER:
    case CM_CALL_EVENT_CONNECT:
    case CM_CALL_EVENT_END:
    case CM_CALL_EVENT_PROGRESS_INFO_IND:
    {
      if( ( CM_CALL_TYPE_VOICE == call_info_ptr->call_type ) ||
          ( CM_CALL_TYPE_EMERGENCY == call_info_ptr->call_type ) ||
          ( CM_CALL_TYPE_VT == call_info_ptr->call_type ) )
      {
        /* Queue only if inband ring back tones is notified while 
         * alerting/progress info indication.
         * This is applicable only to GSM, WCDMA and TDS-CDMA RATs.
         */
        if( ( CM_CALL_EVENT_PROGRESS_INFO_IND == call_event ) &&
            ( ( CM_CALL_MODE_INFO_GW_CS != call_info_ptr->mode_info.info_type ) ||
              ( USER_RESUMED != call_info_ptr->mode_info.info.gw_cs_call.call_progress ) ) )
        {
          MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
                 "VAGENT: CM_CALL_EVENT_CB: event=(56): ASID=(%d): VSID=(0x%08x): "
                 "call_mode=(%d): Ringback tone is NOT in-band: IGNORED ", 
                 subs_obj->asid, subs_obj->vsid, call_info_ptr->mode_info.info_type );
          break;
        }

        MSG_4( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
               "VAGENT: CM_CALL_EVENT_CB: event=(%d): ASID=(%d): VSID=(0x%08x): "
               "call_type=(%d)", call_event, subs_obj->asid, subs_obj->vsid, 
               call_info_ptr->call_type );

        rc = vagent_prepare_and_dispatch_event_packet( subs_obj,
                VAGENT_EVENTI_CM_CALL_EVENT, &call_status, sizeof( call_status) );
        if( rc )
        {
          VAGENT_REPORT_FATAL_ON_ERROR( rc );
        }
      }
      else
      {
        MSG_4( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
               "VAGENT: CM_CALL_EVENT_CB: event=(%d): ASID=(%d): VSID=(0x%08x): "
               "UNHANDLED call_type=(%d)", call_event, subs_obj->asid, 
               subs_obj->vsid, call_info_ptr->call_type );
      }
    }
    break;

    /* call_type is not applicable for handover event and during data call 
     * handover events are not expected from call manager.
     */
    case CM_CALL_EVENT_HO_START:
    {
      if( SYS_VOICE_HO_NONE != call_info_ptr->voice_ho_type )
      {
        MSG_4( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
               "VAGENT: CM_CALL_EVENT_CB: event=(%d): ASID=(%d): VSID=(0x%08x): "
               "HO_type=(%d)", call_event, subs_obj->asid, subs_obj->vsid, 
               call_info_ptr->voice_ho_type );

        rc = vagent_prepare_and_dispatch_event_packet( subs_obj,
                VAGENT_EVENTI_CM_CALL_EVENT, &call_status, sizeof( call_status) );
        if( rc )
        {
          VAGENT_REPORT_FATAL_ON_ERROR( rc );
        }
      }
      else
      {
        MSG_4( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
               "VAGENT: CM_CALL_EVENT_CB: event=(%d): ASID=(%d): VSID=(0x%08x): "
               "Unhandled handover type=(%d)", call_event, subs_obj->asid,
               subs_obj->vsid, call_info_ptr->voice_ho_type );
      }
    }
    break;

    case CM_CALL_EVENT_HO_COMPLETE:
    {
      if ( ( SYS_VOICE_HO_SRVCC_L_2_G == call_info_ptr->voice_ho_type ) ||
           ( SYS_VOICE_HO_SRVCC_L_2_W == call_info_ptr->voice_ho_type ) )
      {
        MSG_4(MSG_SSID_DFLT, MSG_LEGACY_HIGH,
              "VAGENT: CM_CALL_EVENT_CB: event=(%d): ASID=(%d): VSID=(0x%08x) "
              "HO_type=(%d)", call_event, subs_obj->asid, subs_obj->vsid, 
              call_info_ptr->voice_ho_type );

        rc = vagent_prepare_and_dispatch_event_packet( subs_obj,
               VAGENT_EVENTI_CM_CALL_EVENT, &call_status,
               sizeof( call_status) );
      }
      else
      {
        MSG_4( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
               "VAGENT: CM_CALL_EVENT_CB: event=(%d): ASID=(%d): VSID=(0x%08x) "
               "UNHANDLED handover type=(%d)", call_event, subs_obj->asid,
               subs_obj->vsid, call_info_ptr->voice_ho_type );
      }
    }
    break;

    default:
      break;

  } /* end of switch( call_event ) */

  return;
}


VAGENT_INTERNAL void vagent_cm_subs_info_cb (
  cm_ph_event_e_type event,
  const cm_subs_info_s_type* params 
)
{

  uint32_t rc = APR_EOK;
  vagent_modem_subs_object_t* subs_obj = NULL;
  vagent_eventi_cm_subs_info vagent_subs_event;

  if ( params == NULL )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, 
         "VAGENT: CM_SUBS_INFO_CB: Invalid event params received." );
    return;
  }

  switch ( event )
  {
     case CM_PH_EVENT_SUBSCRIPTION_PREF_INFO:
     {
      /* Currently only 2 subscriptions are supported */
      if ( ( params->subscription_id == SYS_MODEM_AS_ID_1 ) ||
           ( params->subscription_id == SYS_MODEM_AS_ID_2 ) )
      {
        MSG_4( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
               "VAGENT: CM_SUBS_INFO_CB: VSID=(0x%08x): ASID(%d): event=(%d):"
               "SUB_ACTIVE=(%d)", params->hw_id[SYS_VSID_APP_CS_VOICE], 
               params->subscription_id, event, params->is_subphone_active );
         
        subs_obj = vagent_modem_subs_obj_list[params->subscription_id];
        vagent_subs_event.asid = params->subscription_id;
        vagent_subs_event.vsid = params->hw_id[SYS_VSID_APP_CS_VOICE];
        vagent_subs_event.is_sub_active = params->is_subphone_active;
        rc = vagent_prepare_and_dispatch_event_packet( subs_obj,
               VAGENT_EVENTI_CM_SUBS_INFO, &vagent_subs_event,
               sizeof( vagent_subs_event) );
        if ( rc )
        {
          MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, 
                 "VAGENT: CM_SUBS_INFO_CB: event=(%d) handling error: "
                 "rc=(0x%08x)", event, rc );
        }
       }
       else
       {
        MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "VAGENT: CM_SUBS_INFO_CB: ASID(%d) not supported: VSID=(0x%08x): "
               "event=(%d)", params->subscription_id,
               params->hw_id[SYS_VSID_APP_CS_VOICE], event );
      }
       }
       break;

   default:
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH, 
             "VAGENT(): CM_SUBS_INFO_CB: Unsupported event=(%d)", event );
    }
    break;
  }

  return;

}


VAGENT_INTERNAL uint32_t vagent_voice_adapter_event_cb (
 void* session_context,
 uint32_t event_id,
 void* params,
 uint32_t size
)
{

  uint32_t rc = APR_EOK;

  switch ( event_id )
  {
   case IVA_IRESOURCE_EVENT_REQUEST:
   case IVA_IRESOURCE_EVENT_RELEASED:
   case GVA_IRESOURCE_EVENT_REQUEST:
   case GVA_IRESOURCE_EVENT_RELEASED:
   case WVA_IRESOURCE_EVENT_REQUEST:
   case WVA_IRESOURCE_EVENT_RELEASED:
   case TVA_IRESOURCE_EVENT_REQUEST:
   case TVA_IRESOURCE_EVENT_RELEASED:
   case CVA_IRESOURCE_EVENT_REQUEST:
   case CVA_IRESOURCE_EVENT_RELEASED:
   case ECALL_VOICE_IRESOURCE_EVENT_REQUEST:
   case ECALL_VOICE_IRESOURCE_EVENT_RELEASED:
    {
      vagent_log_voice_adapter_event_info( session_context, event_id );
      rc = vagent_prepare_and_dispatch_event_packet( session_context,
                                             event_id, params, size );
    }
    break;

   default:
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW, 
             "VAGENT: V_ADAPTER_CB: Unsupported event=(%d)",
             event_id );
     rc = APR_EFAILED;
  }
    break;
  }

  return rc;

}
/****************************************************************************
 *  End of VOICE AGENT - CALLBACK FUNCTIONS                                 * 
 *  These function executes in server (CM, voice adapter) context           * 
 *  1. vagent_cm_call_event_cb                                              * 
 *  2. vagent_cm_subs_info_cb                                               * 
 *  3. vagent_voice_adapter_event_cb                                        * 
*****************************************************************************/



/****************************************************************************
 *  VOICE AGENT -> VOICE ADAPTER(s) FUNCTIONS                               * 
 *  1. vagent_broadcast_subscription_info                                   * 
 *  2. vagent_resource_register                                             * 
 *  3. vagent_resource_deregister                                           * 
 *  4. vagent_resource_grant                                                * 
 *  5. vagent_resource_revoke                                               * 
*****************************************************************************/

VAGENT_INTERNAL void vagent_broadcast_subscription_info (
  vagent_modem_subs_object_t* subs_obj
)
{

  uint32_t rc = APR_EOK;
  cva_icommon_cmd_set_asid_vsid_mapping_t cva_map_cmd;
  gva_icommon_cmd_set_asid_vsid_mapping_t gva_map_cmd;
  wva_icommon_cmd_set_asid_vsid_mapping_t wva_map_cmd;
  tva_icommon_cmd_set_asid_vsid_mapping_t tva_map_cmd;

  for( ;; )
  {
    /* Send mapping to CVA. */
    cva_map_cmd.asid = subs_obj->asid;
    cva_map_cmd.vsid = subs_obj->vsid;
    rc = cva_call( CVA_ICOMMON_CMD_SET_ASID_VSID_MAPPING, (void*)&cva_map_cmd,
                   sizeof( cva_map_cmd ) );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "VAGENT: ASID-VSID mapping info send(to CVA) failed. rc=(0x%08x)",
             rc );
    }

    /* Send mapping to GVA. */
    gva_map_cmd.asid = subs_obj->asid;
    gva_map_cmd.vsid = subs_obj->vsid;
    rc = gva_call( GVA_ICOMMON_CMD_SET_ASID_VSID_MAPPING, (void*)&gva_map_cmd,
                   sizeof( gva_map_cmd ) );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "VAGENT: ASID-VSID mapping info send(to GVA) failed. rc=(0x%08x)",
             rc );
    }

    /* Send mapping to WVA. */
    wva_map_cmd.asid = subs_obj->asid;
    wva_map_cmd.vsid = subs_obj->vsid;
    rc = wva_call( WVA_ICOMMON_CMD_SET_ASID_VSID_MAPPING, (void*)&wva_map_cmd,
                   sizeof( wva_map_cmd ) );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "VAGENT: ASID-VSID mapping info send(to WVA) failed. rc=(0x%08x)",
             rc );
    }

    /* Send mapping to TVA. */
    tva_map_cmd.asid = subs_obj->asid;
    tva_map_cmd.vsid = subs_obj->vsid;
    rc = tva_call( TVA_ICOMMON_CMD_SET_ASID_VSID_MAPPING, (void*)&tva_map_cmd,
                   sizeof( tva_map_cmd ) );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "VAGENT: ASID-VSID mapping info send(to TVA) failed. rc=(0x%08x)",
             rc );
    }

    if ( APR_EOK == rc )
    {
      MSG(MSG_SSID_DFLT, MSG_LEGACY_HIGH,
           "VAGENT: ASID-VSID mapping info send to voice adapter(s) successfully" );
    }
    break;

  } /* for ( ;; ) */

  return;

}


VAGENT_INTERNAL uint32_t vagent_resource_register (
  vagent_session_object_t* session_obj
)
{

  uint32_t rc = APR_EOK;
  cva_iresource_cmd_register_t cva_reg_cmd;
  gva_iresource_cmd_register_t gva_reg_cmd;
  wva_iresource_cmd_register_t wva_reg_cmd;
  tva_iresource_cmd_register_t tva_reg_cmd;
  iva_iresource_cmd_register_t iva_reg_cmd;
  ecall_voice_iresource_cmd_register_t ecall_reg_cmd;

  for ( ;; )
  {
    /* Register for CVA resource control. */
    cva_reg_cmd.ret_handle = &session_obj->cva_handle;
    cva_reg_cmd.vsid = session_obj->vsid;
    cva_reg_cmd.session_context = ( void* )session_obj;
    cva_reg_cmd.event_cb = vagent_voice_adapter_event_cb;

    rc = cva_call( CVA_IRESOURCE_CMD_REGISTER, (void*)&cva_reg_cmd,
                   sizeof( cva_reg_cmd ) );
    if ( rc )
    {
      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "VAGENT: VSID=(0x%08x): ASID=(%d): CVA_IRESOURCE_CMD_REGISTER "
             "failed. rc=(0x%08x) ", session_obj->vsid, 
             session_obj->modem_subs_obj->asid, rc );
    }
    else
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,
             "VAGENT: VSID=(0x%08x): ASID=(%d): CVA_IRESOURCE_CMD_REGISTER "
             "success.", session_obj->vsid, session_obj->modem_subs_obj->asid );
    }

    /* Register for GVA resource control. */
    gva_reg_cmd.ret_handle = &session_obj->gva_handle;
    gva_reg_cmd.vsid = session_obj->vsid;
    gva_reg_cmd.session_context = ( void* )session_obj;
    gva_reg_cmd.event_cb = vagent_voice_adapter_event_cb;

    rc = gva_call( GVA_IRESOURCE_CMD_REGISTER, (void*)&gva_reg_cmd,
                   sizeof( gva_reg_cmd ) );
    if ( rc )
    {
      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "VAGENT: VSID=(0x%08x): ASID=(%d): GVA_IRESOURCE_CMD_REGISTER "
             "failed. rc=(0x%08x) ", session_obj->vsid, 
             session_obj->modem_subs_obj->asid, rc );
    }
    else
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,
             "VAGENT: VSID=(0x%08x): ASID=(%d): GVA_IRESOURCE_CMD_REGISTER "
             "success.", session_obj->vsid, session_obj->modem_subs_obj->asid );
    }

    /* Register for WVA resource control. */
    wva_reg_cmd.ret_handle = &session_obj->wva_handle;
    wva_reg_cmd.vsid = session_obj->vsid;
    wva_reg_cmd.session_context = ( void* )session_obj;
    wva_reg_cmd.event_cb = vagent_voice_adapter_event_cb;

    rc = wva_call( WVA_IRESOURCE_CMD_REGISTER, (void*)&wva_reg_cmd,
                   sizeof( wva_reg_cmd ) );
    if ( rc )
    {
      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "VAGENT: VSID=(0x%08x): ASID=(%d): WVA_IRESOURCE_CMD_REGISTER "
             "failed. rc=(0x%08x) ", session_obj->vsid, 
             session_obj->modem_subs_obj->asid, rc );
    }
    else
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,
             "VAGENT: VSID=(0x%08x): ASID=(%d): WVA_IRESOURCE_CMD_REGISTER "
             "success.", session_obj->vsid, session_obj->modem_subs_obj->asid );
    }

    /* Register for TVA resource control. */
    tva_reg_cmd.ret_handle = &session_obj->tva_handle;
    tva_reg_cmd.vsid = session_obj->vsid;
    tva_reg_cmd.session_context = ( void* )session_obj;
    tva_reg_cmd.event_cb = vagent_voice_adapter_event_cb;

    rc = tva_call( TVA_IRESOURCE_CMD_REGISTER, (void*)&tva_reg_cmd,
                   sizeof( tva_reg_cmd ) );
    if ( rc )
    {
      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "VAGENT: VSID=(0x%08x): ASID=(%d): TVA_IRESOURCE_CMD_REGISTER "
             "failed. rc=(0x%08x) ", session_obj->vsid, 
             session_obj->modem_subs_obj->asid, rc );
    }
    else
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,
             "VAGENT: VSID=(0x%08x): ASID=(%d): TVA_IRESOURCE_CMD_REGISTER "
             "success.", session_obj->vsid, session_obj->modem_subs_obj->asid );
    }

    /* Register for IVA resource control. */
    iva_reg_cmd.ret_handle = &session_obj->iva_handle;
    iva_reg_cmd.vsid = session_obj->vsid;
    iva_reg_cmd.session_context = ( void* )session_obj;
    iva_reg_cmd.event_cb = vagent_voice_adapter_event_cb;

    rc = iva_call( IVA_IRESOURCE_CMD_REGISTER, (void*)&iva_reg_cmd,
                   sizeof( iva_reg_cmd ) );
    if ( rc )
    {
      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "VAGENT: VSID=(0x%08x): ASID=(%d): IVA_IRESOURCE_CMD_REGISTER "
             "failed. rc=(0x%08x) ", session_obj->vsid, 
             session_obj->modem_subs_obj->asid, rc );
    }
    else
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,
             "VAGENT: VSID=(0x%08x): ASID=(%d): IVA_IRESOURCE_CMD_REGISTER "
             "success.", session_obj->vsid, session_obj->modem_subs_obj->asid );
    }

    /* Register for ECALL resource control. */
    ecall_reg_cmd.ret_handle_ptr = &session_obj->ecall_handle;
    ecall_reg_cmd.vsid = session_obj->vsid;
    ecall_reg_cmd.session_context_ptr = ( void* )session_obj;
    ecall_reg_cmd.event_cb = vagent_voice_adapter_event_cb;

    rc = ecall_voice_api( ECALL_VOICE_IRESOURCE_CMD_REGISTER, &ecall_reg_cmd,
                          sizeof( ecall_reg_cmd ) );
    if ( rc )
    {
      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "VAGENT: VSID=(0x%08x): ASID=(%d): ECALL_VOICE_IRESOURCE_CMD_REGISTER "
             "failed. rc=(0x%08x) ", session_obj->vsid, 
             session_obj->modem_subs_obj->asid, rc );
    }
    else
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,
             "VAGENT: VSID=(0x%08x): ASID=(%d): ECALL_VOICE_IRESOURCE_CMD_REGISTER "
             "success.", session_obj->vsid, session_obj->modem_subs_obj->asid );
    }

    break;
  } /* for ( ;; ) */

  return rc;

}


VAGENT_INTERNAL uint32_t vagent_resource_deregister (
  vagent_session_object_t* session_obj
)
{

  uint32_t rc = APR_EOK;
  cva_iresource_cmd_deregister_t cva_dereg_cmd;
  gva_iresource_cmd_deregister_t gva_dereg_cmd;
  wva_iresource_cmd_deregister_t wva_dereg_cmd;
  tva_iresource_cmd_deregister_t tva_dereg_cmd;
  iva_iresource_cmd_deregister_t iva_dereg_cmd;
  ecall_voice_iresource_cmd_deregister_t ecall_dereg_cmd;

  for ( ;; )
  {
    if ( session_obj->cva_handle != APR_NULL_V )
    {
      /* de-Register for TVA resource control. */
      cva_dereg_cmd.handle = session_obj->cva_handle;

      rc = cva_call( CVA_IRESOURCE_CMD_DEREGISTER, (void*)&cva_dereg_cmd,
                     sizeof( cva_dereg_cmd ) );
      if ( rc )
      {
        MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "VAGENT: VSID=(0x%08x): ASID=(%d): CVA_IRESOURCE_CMD_DEREGISTER "
               "failed. rc=(0x%08x) ", session_obj->vsid, 
               session_obj->modem_subs_obj->asid, rc );
      }
      else
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,
               "VAGENT: VSID=(0x%08x): ASID=(%d): CVA_IRESOURCE_CMD_DEREGISTER "
               "success.", session_obj->vsid, session_obj->modem_subs_obj->asid );
      }
      session_obj->cva_handle =  APR_NULL_V;
    }

    if ( session_obj->gva_handle != APR_NULL_V )
    {
      /* de-Register for GVA resource control. */
      gva_dereg_cmd.handle = session_obj->gva_handle;
  
      rc = gva_call( GVA_IRESOURCE_CMD_DEREGISTER, (void*)&gva_dereg_cmd,
                     sizeof( gva_dereg_cmd ) );
      if ( rc )
      {
        MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "VAGENT: VSID=(0x%08x): ASID=(%d): GVA_IRESOURCE_CMD_DEREGISTER "
               "failed. rc=(0x%08x) ", session_obj->vsid, 
               session_obj->modem_subs_obj->asid, rc );
      }
      else
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,
               "VAGENT: VSID=(0x%08x): ASID=(%d): GVA_IRESOURCE_CMD_DEREGISTER "
               "success.", session_obj->vsid, session_obj->modem_subs_obj->asid );
      }
      session_obj->gva_handle =  APR_NULL_V;
    }

    if ( session_obj->wva_handle != APR_NULL_V )
    {
      /* de-Register for WVA resource control. */
      wva_dereg_cmd.handle = session_obj->wva_handle;
  
      rc = wva_call( WVA_IRESOURCE_CMD_DEREGISTER, (void*)&wva_dereg_cmd,
                     sizeof( wva_dereg_cmd ) );
      if ( rc )
      {
        MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "VAGENT: VSID=(0x%08x): ASID=(%d): WVA_IRESOURCE_CMD_DEREGISTER "
               "failed. rc=(0x%08x) ", session_obj->vsid, 
               session_obj->modem_subs_obj->asid, rc );
      }
      else
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,
               "VAGENT: VSID=(0x%08x): ASID=(%d): WVA_IRESOURCE_CMD_DEREGISTER "
               "success.", session_obj->vsid, session_obj->modem_subs_obj->asid );
      }
      session_obj->wva_handle =  APR_NULL_V;
    }

    if ( session_obj->tva_handle != APR_NULL_V )
    {
      /* de-Register for TVA resource control. */
      tva_dereg_cmd.handle = session_obj->tva_handle;
  
      rc = tva_call( TVA_IRESOURCE_CMD_DEREGISTER, (void*)&tva_dereg_cmd,
                     sizeof( tva_dereg_cmd ) );
      if ( rc )
      {
        MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "VAGENT: VSID=(0x%08x): ASID=(%d): TVA_IRESOURCE_CMD_DEREGISTER "
               "failed. rc=(0x%08x) ", session_obj->vsid, 
               session_obj->modem_subs_obj->asid, rc );
      }
      else
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,
               "VAGENT: VSID=(0x%08x): ASID=(%d): TVA_IRESOURCE_CMD_DEREGISTER "
               "success.", session_obj->vsid, session_obj->modem_subs_obj->asid );
      }
      session_obj->tva_handle =  APR_NULL_V;
    }

    if ( session_obj->iva_handle != APR_NULL_V )
    {
      /* De-register for IVA resource control. */
      iva_dereg_cmd.handle = session_obj->iva_handle;
  
      rc = iva_call( IVA_IRESOURCE_CMD_DEREGISTER, (void*)&iva_dereg_cmd,
                     sizeof( iva_dereg_cmd ) );
      if ( rc )
      {
        MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "VAGENT: VSID=(0x%08x): ASID=(%d): IVA_IRESOURCE_CMD_DEREGISTER "
               "failed. rc=(0x%08x) ", session_obj->vsid, 
               session_obj->modem_subs_obj->asid, rc );
      }
      else
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,
               "VAGENT: VSID=(0x%08x): ASID=(%d): IVA_IRESOURCE_CMD_DEREGISTER "
               "success.", session_obj->vsid, session_obj->modem_subs_obj->asid );
      }
      session_obj->iva_handle =  APR_NULL_V;
    }

    if ( session_obj->ecall_handle != APR_NULL_V )
    {
      /* de-Register for TVA resource control. */
      ecall_dereg_cmd.handle = session_obj->ecall_handle;

      rc = ecall_voice_api( ECALL_VOICE_IRESOURCE_CMD_DEREGISTER, &ecall_dereg_cmd,
                            sizeof( ecall_dereg_cmd ) );
      if ( rc )
      {
        MSG_3( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "VAGENT: VSID=(0x%08x): ASID=(%d): ECALL_VOICE_IRESOURCE_CMD_DEREGISTER "
               "failed. rc=(0x%08x) ", session_obj->vsid, 
               session_obj->modem_subs_obj->asid, rc );
      }
      else
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,
               "VAGENT: VSID=(0x%08x): ASID=(%d): ECALL_VOICE_IRESOURCE_CMD_DEREGISTER "
               "success.", session_obj->vsid, session_obj->modem_subs_obj->asid );
      }
      session_obj->ecall_handle =  APR_NULL_V;
    }

    break;
  } /* for ( ;; ) */

  return rc;

}


VAGENT_INTERNAL uint32_t vagent_resource_grant (
  vagent_session_object_t* session_obj,
  uint32_t module_id
)
{

  uint32_t rc = APR_EOK;
  cva_iresource_cmd_grant_t cva_cmd_grant;
  gva_iresource_cmd_grant_t gva_cmd_grant;
  wva_iresource_cmd_grant_t wva_cmd_grant;
  tva_iresource_cmd_grant_t tva_cmd_grant;
  iva_iresource_cmd_grant_t iva_cmd_grant;
  ecall_voice_iresource_cmd_grant_t ecall_cmd_grant;

  switch( module_id )
  {
    case VAGENT_MODULE_ID_CVA:
    {
      cva_cmd_grant.handle = session_obj->cva_handle;
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,
             "VAGENT: VSID=(0x%08x): ASID=(%d): CVA_IRESOURCE_CMD_GRANT issued",
             session_obj->vsid, session_obj->modem_subs_obj->asid  );
      rc = cva_call ( CVA_IRESOURCE_CMD_GRANT, &cva_cmd_grant,
                      sizeof( cva_cmd_grant ) );
    }
    break;

    case VAGENT_MODULE_ID_GVA:
    {
      gva_cmd_grant.handle = session_obj->gva_handle;
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,
             "VAGENT: VSID=(0x%08x): ASID=(%d): GVA_IRESOURCE_CMD_GRANT issued",
             session_obj->vsid, session_obj->modem_subs_obj->asid  );
      rc = gva_call ( GVA_IRESOURCE_CMD_GRANT, &gva_cmd_grant,
                      sizeof( gva_cmd_grant) );
    }
    break;
 
    case VAGENT_MODULE_ID_WVA:
    {
      wva_cmd_grant.handle = session_obj->wva_handle;
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,
             "VAGENT: VSID=(0x%08x): ASID=(%d): WVA_IRESOURCE_CMD_GRANT issued",
             session_obj->vsid, session_obj->modem_subs_obj->asid  );
      rc = wva_call ( WVA_IRESOURCE_CMD_GRANT, &wva_cmd_grant,
                      sizeof( wva_cmd_grant ) );
    }
    break;
 
    case VAGENT_MODULE_ID_TVA:
    {
      tva_cmd_grant.handle = session_obj->tva_handle;
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,
             "VAGENT: VSID=(0x%08x): ASID=(%d): TVA_IRESOURCE_CMD_GRANT issued",
             session_obj->vsid, session_obj->modem_subs_obj->asid  );
      rc = tva_call ( TVA_IRESOURCE_CMD_GRANT, &tva_cmd_grant,
                      sizeof( tva_cmd_grant ) );
    }
    break;
 
    case VAGENT_MODULE_ID_IVA:
    {
      iva_cmd_grant.handle = session_obj->iva_handle;
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,
             "VAGENT: VSID=(0x%08x): ASID=(%d): IVA_IRESOURCE_CMD_GRANT issued",
             session_obj->vsid, session_obj->modem_subs_obj->asid  );
      rc = iva_call ( IVA_IRESOURCE_CMD_GRANT, &iva_cmd_grant,
                      sizeof( iva_cmd_grant) );
    }
    break;

    case VAGENT_MODULE_ID_ECALL:
    {
      ecall_cmd_grant.handle = session_obj->ecall_handle;
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,
             "VAGENT: VSID=(0x%08x): ASID=(%d): ECALL_VOICE_IRESOURCE_CMD_GRANT issued",
             session_obj->vsid, session_obj->modem_subs_obj->asid  );
      rc = ecall_voice_api( ECALL_VOICE_IRESOURCE_CMD_GRANT, &ecall_cmd_grant,
                            sizeof( ecall_cmd_grant ) );
    }
    break;

    case VAGENT_MODULE_ID_UNDEFINED:
    default:
      break;
  }

  return rc;

}


/**  
 * No revoke for ECALL is required. ECALL being the highest 
 * priority usecase hence all other voice use cases are blocked 
 * UE. ECALL would relased the reosurce automatically once the 
 * ECALL is ended. 
 *  
 * NOTE: ECALL only supported on single sim, hence no DSDA use 
 * case with it. 
 */
VAGENT_INTERNAL uint32_t vagent_resource_revoke (
  vagent_session_object_t* session_obj
)
{

  uint32_t rc = APR_EOK;
  cva_iresource_cmd_revoke_t cva_cmd_revoke;
  gva_iresource_cmd_revoke_t gva_cmd_revoke;
  wva_iresource_cmd_revoke_t wva_cmd_revoke;
  tva_iresource_cmd_revoke_t tva_cmd_revoke;
  iva_iresource_cmd_revoke_t iva_cmd_revoke;

  uint32_t module_id = session_obj->current_module;

  switch( module_id )
  {

   case VAGENT_MODULE_ID_CVA:
    {
      cva_cmd_revoke.handle = session_obj->cva_handle;
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "VAGENT: VSID=(0x%08x): ASID=(%d): CVA_IRESOURCE_CMD_REVOKE issued",
             session_obj->vsid, session_obj->modem_subs_obj->asid  );
      rc = cva_call( CVA_IRESOURCE_CMD_REVOKE, &cva_cmd_revoke,
                     sizeof( cva_cmd_revoke ) );
    }
    break;

   case VAGENT_MODULE_ID_GVA:
     {
       gva_cmd_revoke.handle = session_obj->gva_handle;
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "VAGENT: VSID=(0x%08x): ASID=(%d): GVA_IRESOURCE_CMD_REVOKE issued",
             session_obj->vsid, session_obj->modem_subs_obj->asid  );
       rc = gva_call( GVA_IRESOURCE_CMD_REVOKE, &gva_cmd_revoke,
                      sizeof( gva_cmd_revoke) );
     }
     break;

   case VAGENT_MODULE_ID_WVA:
     {
       wva_cmd_revoke.handle = session_obj->wva_handle;
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "VAGENT: VSID=(0x%08x): ASID=(%d): WVA_IRESOURCE_CMD_REVOKE issued",
             session_obj->vsid, session_obj->modem_subs_obj->asid  );
       rc = wva_call( WVA_IRESOURCE_CMD_REVOKE, &wva_cmd_revoke,
                      sizeof( wva_cmd_revoke ) );
     }
     break;

   case VAGENT_MODULE_ID_TVA:
     {
       tva_cmd_revoke.handle = session_obj->tva_handle;
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "VAGENT: VSID=(0x%08x): ASID=(%d): TVA_IRESOURCE_CMD_REVOKE issued",
             session_obj->vsid, session_obj->modem_subs_obj->asid  );
       rc = tva_call( TVA_IRESOURCE_CMD_REVOKE, &tva_cmd_revoke,
                      sizeof( tva_cmd_revoke ) );
     }
     break;

   case VAGENT_MODULE_ID_IVA:
     {
       iva_cmd_revoke.handle = session_obj->iva_handle;
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "VAGENT: VSID=(0x%08x): ASID=(%d): IVA_IRESOURCE_CMD_REVOKE issued",
             session_obj->vsid, session_obj->modem_subs_obj->asid  );
       rc = iva_call( IVA_IRESOURCE_CMD_REVOKE, &iva_cmd_revoke,
                      sizeof( iva_cmd_revoke) );
     }
     break;

   case VAGENT_MODULE_ID_UNDEFINED:
   default:
     break;
  }

  return rc;

}
/****************************************************************************
 *  END of VOICE AGENT -> VOICE ADAPTER(s) FUNCTIONS                        * 
 *  1. vagent_broadcast_subscription_info                                   * 
 *  2. vagent_resource_register                                             * 
 *  3. vagent_resource_deregister                                           * 
 *  4. vagent_resource_grant                                                * 
 *  5. vagent_resource_revoke                                               * 
*****************************************************************************/



/****************************************************************************
 *  VOICE AGENT - EVENT CALLBACK PROCESSING FUNCTIONS                       * 
 *  These function executes in voice agent context                          * 
 *  1. vagent_process_cm_call_event                                         * 
 *  2. vagent_process_cm_subs_info_event                                    * 
 *  3. vagent_process_resource_request_event                                * 
 *  4. vagent_process_ecall_resource_request_event                          * 
 *  5. vagent_process_resource_released_event                               * 
 *  6. vagent_process_ecall_resource_released_event                         * 
*****************************************************************************/

VAGENT_INTERNAL uint32_t vagent_process_cm_call_event ( 
  vagent_event_packet_t* packet
)
{

  uint32_t rc = APR_EOK;
  vagent_modem_subs_object_t* subs_obj = NULL;
  vagent_session_object_t* session_obj = NULL;
  vagent_eventi_cm_call_event_t* evt_params = NULL;

  for ( ;; )
  {
    evt_params = ( vagent_eventi_cm_call_event_t*) packet->params;
    if ( NULL == evt_params )
    {
      VAGENT_REPORT_FATAL_ON_ERROR( APR_EBADPARAM );
      break;
    }

    subs_obj = vagent_modem_subs_obj_list[evt_params->asid];
    if ( NULL == subs_obj )
    {
      VAGENT_REPORT_FATAL_ON_ERROR( APR_EBADPARAM );
      break;
    }

    /* Extract the mapped VSID session object. */
    session_obj = subs_obj->session_obj;
    if ( NULL == session_obj )
    {
      VAGENT_REPORT_FATAL_ON_ERROR( APR_EBADPARAM );
      break;
    }

    MSG_6( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
           "VAGENT: CM_CALL_EVENT(PROCESS): event=(%d): VSID=(0x%08x): "
           "ASID=(%d): STATE=(%d): RefCount=(%d): no_of_resources=(%d) ",
           evt_params->event_id, subs_obj->vsid, subs_obj->asid, 
           session_obj->state, subs_obj->call_refcount, vagent_num_available_resource );

    switch( evt_params->event_id )
    {
      case CM_CALL_EVENT_ORIG:
      {
        /* - Increment the reference count of acitve calls.
         * - Once in ACTIVE state reference count is incremented for every 
         *   ORIG event. */
        subs_obj->call_refcount++;
        subs_obj->event_call_orig_dbg_count++;

        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
               "VAGENT: CM_CALL_EVENT(PROCESS): ORIG: RefCount++(%d), "
               "Total_OrigCount=(%d)", subs_obj->call_refcount,
               subs_obj->event_call_orig_dbg_count );
      }
      break;
 
      case CM_CALL_EVENT_INCOM:
      {
        /* - Increment the reference count of acitve calls
         * - Once in ACTIVE state reference count is incremented for every 
         *   INCOMING event. */
        subs_obj->call_refcount++;
        subs_obj->event_call_incom_dbg_count++;

        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
               "VAGENT: CM_CALL_EVENT(PROCESS): INCOM: RefCount++(%d),"
               "Total_IncomCount=(%d)", subs_obj->call_refcount, 
               subs_obj->event_call_incom_dbg_count );
      }
      break;

      case CM_CALL_EVENT_HO_START:
      {
        /* SRVCC for IMS conference ends up having voice agent in INACTIVE STATE
         * Henceforth audio mute is observed in circuit switched domain if
         * inter-rat/intra-rat handover happens since voice agent do not grants
         * the vocoder once released.
         */
        if ( 0 == subs_obj->call_refcount )
        {
          subs_obj->call_refcount++;
          session_obj->state = VAGENT_SUBS_STATE_ACTIVE;

          MSG_4( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
                 "VAGENT: CM_CALL_EVENT(PROCESS): HO_START: state(->ACTIVE): "
                 "RefCount++(%d), current_module=(%d), pending_module=(%d)"
                 "no_of_resources=(%d)", subs_obj->call_refcount,
                 session_obj->current_module, session_obj->pending_module,
                 vagent_num_available_resource );
        }
        else
        {
          MSG_5( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
                 "VAGENT: CM_CALL_EVENT(PROCESS): HO_START IGNORED: STATE=(%d): "
                 "RefCount=(%d), current_module=(%d), pending_module=(%d), "
                 "no_of_resources=(%d)", session_obj->state, subs_obj->call_refcount, 
                 session_obj->current_module, session_obj->pending_module,
                 vagent_num_available_resource );
        }
      }
      break;

      case CM_CALL_EVENT_HO_COMPLETE:
      {
        /* Special Handling for OEM's with 3rd Party IMS solutions:
         * - If guidelines required by SVS design are not followed.
         * - SRVCC Failure, which may result in voice agent state to INACTICE 
         *   due to decrement of reference count because of additional call
         *   end event from CM. */
        if ( 0 == subs_obj->call_refcount )
        {
          subs_obj->call_refcount++;
          session_obj->state = VAGENT_SUBS_STATE_ACTIVE;

          MSG_4( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
                 "VAGENT: CM_CALL_EVENT(PROCESS): HO_COMPLETE: state(->ACTIVE): "
                 "RefCount++(%d), current_module=(%d), pending_module=(%d)"
                 "no_of_resources=(%d)", subs_obj->call_refcount,
                 session_obj->current_module, session_obj->pending_module,
                 vagent_num_available_resource );

          /* Grant if there is any pending request */
          if ( ( vagent_num_available_resource > 0 ) &&
               ( VAGENT_MODULE_ID_UNDEFINED != session_obj->pending_module ) )
          {
            ( void ) vagent_resource_grant ( session_obj, session_obj->pending_module );

             /* Update the current module to which the resource is granted. */
             session_obj->current_module = session_obj->pending_module;
             session_obj->pending_module = VAGENT_MODULE_ID_UNDEFINED;
             vagent_num_available_resource--;

             MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
                    "VAGENT: CM_CALL_EVENT(PROCESS): HO_COMPLETE: "
                    "granted to module=(%d): no_of_resources--(%d)",  
                    session_obj->current_module, vagent_num_available_resource );
          }
          else
          {
            if ( VAGENT_MODULE_ID_UNDEFINED != session_obj->pending_module )
            {
              MSG_3(MSG_SSID_DFLT, MSG_LEGACY_HIGH,
                     "VAGENT: CM_CALL_EVENT(PROCESS): HO_COMPLETE: cannot "
                     "grant resource to module=(%d): no_of_resources=(%d) "
                     "current_module=(%d)", session_obj->pending_module,
                     vagent_num_available_resource, session_obj->current_module );
            }

            /* Revoke the resource from current module if already granted to someone.
             * Grant to pending module once release notification received from current
             * module. */
            if ( session_obj->current_module != VAGENT_MODULE_ID_UNDEFINED )
            {
              MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
                     "VAGENT: CM_CALL_EVENT(PROCESS): HO_COMPLETE: STATE(ACTIVE) "
                     "revoking resource from current_module=(%d)",
                     session_obj->current_module );

              rc = vagent_resource_revoke( session_obj );
              if ( rc )
              {
                VAGENT_REPORT_FATAL_ON_ERROR( rc );
                break;
              }
            }
          }
        }
        else
        {
          MSG_5( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
                 "VAGENT: CM_CALL_EVENT(PROCESS): HO_COMPLETE IGNORED: STATE=(%d): "
                 "RefCount=(%d), current_module=(%d), pending_module=(%d), "
                 "no_of_resources=(%d)", session_obj->state, subs_obj->call_refcount, 
                 session_obj->current_module, session_obj->pending_module,
                 vagent_num_available_resource );
        }
      }
      break;

      case CM_CALL_EVENT_PROGRESS_INFO_IND:
      case CM_CALL_EVENT_CONNECT:
      case CM_CALL_EVENT_ANSWER:
      {
        /* We may not need this handling since we have similar logic to
           increment reference count during SRVCC START/COMPLETE for 3rd
           party IMS - Need evaluation <TBD>*/
        if ( 0 == subs_obj->call_refcount )
        {
          /* Third Party OEM's IMS Solution do not provide CM Event, hence we
           * need to handle "SRVCC during ring tone on MT" scenario which needs
           * voice agent to move to ACTIVE state. */
          subs_obj->call_refcount++;
          MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
                 "VAGENT: CM_CALL_EVENT(PROCESS): event=(%d): RefCount++(%d): "
                 "ANSWER/CONNECT/PROGRESS_INFO without ORIG/INCOM event",
                 evt_params->event_id, subs_obj->call_refcount  );
        }

        /* Voice agent state transition from INACTIVE to ACTIVE. */
        if ( VAGENT_SUBS_STATE_INACTIVE == session_obj->state )
        {
          session_obj->state = VAGENT_SUBS_STATE_ACTIVE;

          MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
                 "VAGENT: CM_CALL_EVENT(PROCESS): event=(%d): STATE=(->ACTIVE): "
                 "RefCount=(%d)", evt_params->event_id , subs_obj->call_refcount );

          /* Grant happens only in Active state provided resource request
           * is pending with Voice agent. IMS Voice Adapter is an exception
           * as IMS does not depend on CM call events to grant vocoder. */
          if ( VAGENT_MODULE_ID_UNDEFINED != session_obj->pending_module )
          {

            if ( vagent_num_available_resource > 0 )
            {
              rc = vagent_resource_grant ( session_obj, session_obj->pending_module );
              if ( rc )
              {
                VAGENT_REPORT_FATAL_ON_ERROR( rc );
                break;
              }

              /* Update the current module to which the resource is granted. */
              session_obj->current_module = session_obj->pending_module;
              session_obj->pending_module = VAGENT_MODULE_ID_UNDEFINED;
              vagent_num_available_resource--; 

              MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
                     "VAGENT: CM_CALL_EVENT(PROCESS): STATE=(ACTIVE): granted to"
                     " module=(%d): pending_module=(%d): no_of_resources--(%d)",
                     session_obj->current_module, session_obj->pending_module,
                     vagent_num_available_resource );

              /* ECALL run concurrently with other module. 
               * Hence resource needs to be granted to ecall & along with either of the
               * voice adapter together. 
               */ 
              if( session_obj->ecall_resource_request == TRUE )
              {
                rc = vagent_resource_grant( session_obj, VAGENT_MODULE_ID_ECALL );
                if ( rc )
                {
                  VAGENT_REPORT_FATAL_ON_ERROR( rc );
                }
                MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
                       "VAGENT: CM_CALL_EVENT(PROCESS): STATE=(ACTIVE): "
                       "granted to eCall. current_module=(%d)", 
                       session_obj->current_module );
              } /* if( session_obj->ecall_resource_request == TRUE )*/
            }
            /* ( vagent_num_available_resource == 0 ) */
            else
            {
              MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
                     "VAGENT: CM_CALL_EVENT(PROCESS): state=(ACTIVE) "
                     "RefCount=(%d): no. of resources(0): pending_module=(%d): "
                     "INCONSISTENT state", subs_obj->call_refcount,
                     session_obj->pending_module );
            } /* if ( vagent_num_available_resource > 0 ) */

          }  
          /* (VAGENT_MODULE_ID_UNDEFINED == session_obj->pending_module ) */
          else
          {
            MSG_4( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
                   "VAGENT: CM_CALL_EVENT(PROCESS): state=(ACTIVE) "
                   "RefCount=(%d): NO Open vocoder request,  no_of_resources=(%d):"
                   " pending_module=(%d): current_module=(%d):",
                   subs_obj->call_refcount, vagent_num_available_resource,
                   session_obj->pending_module, session_obj->current_module  );
          }
        } 
        /* ( VAGENT_SUBS_STATE_ACTIVE == session_obj->state ) */
        else
        {
          MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
               "VAGENT: CM_CALL_EVENT(PROCESS): ANSWER/CONNECT/PROGRESS_INFO "
               "in STATE_ACTIVE - IGNORED");
        } /* if ( VAGENT_SUBS_STATE_INACTIVE == session_obj->state ) */
      }
      break;

      case CM_CALL_EVENT_END:
      {
        subs_obj->event_call_end_dbg_count++;

        /* Decrement the reference count of acitve calls. */
        if( subs_obj->call_refcount > 0 )
        {
          subs_obj->call_refcount--;

          MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
                 "VAGENT: CM_CALL_EVENT(PROCESS): END: RefCount--(%d), "
                 "Total_EndCount=(%d) ", 
                 subs_obj->call_refcount, subs_obj->event_call_end_dbg_count );
 
          /* Voice agent state transition from ACTIVE to INACTIVE. */
          if( 0 == subs_obj->call_refcount )
          {
            if ( VAGENT_SUBS_STATE_ACTIVE == session_obj->state )
            {
              session_obj->state = VAGENT_SUBS_STATE_INACTIVE;
              MSG_4( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
                     "VAGENT: CM_CALL_EVENT(PROCESS): END: STATE=(->INACTIVE): "
                     "RefCount=(0): no_of_resources=(%d): OrigCount=(%d), "
                     "IncomCount=(%d), EndCount=(%d)", vagent_num_available_resource,
                     subs_obj->event_call_orig_dbg_count, 
                     subs_obj->event_call_incom_dbg_count, 
                     subs_obj->event_call_end_dbg_count );
            }
          } /* if( 0 == subs_obj->call_refcount ) */
        }
        /* ( subs_obj->call_refcount == 0 ) */
        else
        {
          /*  Active call count cannot go negative.
           *  This can happen during following use cases:
           *  1. SRVCC of conference calls
           *  2. Using 3rd party IMS that does not go through Call Manager.
           *  So, do not decrement the active call count in these scenarios. 
           */
          MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
                 "VAGENT: CM_CALL_EVENT(PROCESS): END: Reference count cannot go "
                 "negative. OrigCount=(%d), IncomCount=(%d), EndCount=(%d)",
                 subs_obj->event_call_orig_dbg_count, 
                 subs_obj->event_call_incom_dbg_count,
                 subs_obj->event_call_end_dbg_count );
        }
      }
      break;

    default:
        VAGENT_REPORT_FATAL_ON_ERROR( APR_EUNEXPECTED );
        break;
    }

      break;
  }

  ( void ) vagent_free_event_packet( packet );

  return rc;

}


VAGENT_INTERNAL uint32_t vagent_process_cm_subs_info_event ( 
  vagent_event_packet_t* packet
)
{

  uint32_t rc = APR_EOK;
  uint32_t index = 0;
  vagent_modem_subs_object_t* subs_obj = NULL;
  vagent_session_object_t* session_obj = NULL;
  vagent_eventi_cm_subs_info* evt_params = NULL;

  for ( ;; )
  {
    evt_params = ( vagent_eventi_cm_subs_info*) packet->params;
    if( evt_params == NULL )
    {
      VAGENT_REPORT_FATAL_ON_ERROR( APR_EBADPARAM );
      break;
    }

    subs_obj = vagent_modem_subs_obj_list[evt_params->asid];

    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
           "VAGENT: CM_SUBS_INFO(PROCESS): ASID=(%d), new_VSID=(0x%08x),"
           "current_VSID=(0x%08x)", evt_params->asid, evt_params->vsid, 
           subs_obj->vsid );

    /* This condition would be TRUE when there is no change in
     * ASID-VSID mapping (i.e post boot-up) 
     * No action required - just return from this function.
     */
    if( subs_obj->vsid == evt_params->vsid )
    {
      break;
    }

    /* Find session object with vsid.
     * Assign a session object if it does not exist. */
    for ( index = 0; index < VAGENT_MAX_NUM_OF_SESSIONS_V; ++index )
    {
      session_obj = vagent_session_obj_list[index];
      /* This condition would be TRUE when ASID-VSID mapping is received
       * for the first time (boot-up time) 
       */
      if( session_obj->vsid == VAGENT_VSID_UNDEFINED_V )
      {
        session_obj->vsid = evt_params->vsid;
        subs_obj->vsid = session_obj->vsid;
        subs_obj->session_obj = session_obj;
        session_obj->modem_subs_obj = subs_obj;
        ( void ) vagent_resource_register( session_obj );
        break;
      }
      /* This condition would be TRUE when there is a change in
       * ASID-VSID mapping.
       */
      if( session_obj->vsid == evt_params->vsid )
      {
        subs_obj->vsid = session_obj->vsid;
        subs_obj->session_obj = session_obj;
        session_obj->modem_subs_obj = subs_obj;
        break;
      }
    }

    ( void ) vagent_broadcast_subscription_info( subs_obj );
    break;
  }

  ( void ) vagent_free_event_packet( packet );

  return rc;

}


VAGENT_INTERNAL uint32_t vagent_process_resource_request_event ( 
  vagent_event_packet_t* packet,
  uint32_t module_id
)
{

  uint32_t rc = APR_EOK;
  vagent_session_object_t* session_obj = NULL;
  vagent_modem_subs_object_t* subs_obj = NULL;

  for ( ;; )
  {
    session_obj = packet->session_context;
    if( session_obj == NULL )
    {
      VAGENT_REPORT_FATAL_ON_ERROR( APR_EBADPARAM );
      break;
    }

    subs_obj = session_obj->modem_subs_obj;
    if ( subs_obj == NULL )
    {
      VAGENT_REPORT_FATAL_ON_ERROR( APR_EBADPARAM );
      break;
    }

    if ( session_obj->current_module == module_id )
    {
      MSG_4( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
            "VAGENT: IRESOURCE_EVENT_REQUEST(PROCESS): resource already granted "
            "to module=(%d): VSID=(0x%08x): ASID=(%d): STATE=(%d)", 
             module_id, subs_obj->vsid, subs_obj->asid, session_obj->state );
      break;
    }

    /* Third party IMS solution does not go through Call Manager entirely.
     * So, we may not get events such as ORIG, INCOM, ANSWER, CONNECT, 
     * EARLY_MEDIA, PROGRESS_INFO, END, etc.
     * So, updating the reference count based off REQUEST event 
     * is required so that SRVCC to CS happens fine.
     */
    if ( VAGENT_MODULE_ID_IVA == module_id )
    {
      subs_obj->call_refcount++;
      MSG_4( MSG_SSID_DFLT, MSG_LEGACY_MED,
             "VAGENT: IRESOURCE_EVENT_REQUEST(PROCESS): VSID=(0x%08x): "
             "ASID=(%d): STATE=(%d) RefCount++(%d) for IVA", subs_obj->vsid, 
             subs_obj->asid , session_obj->state, subs_obj->call_refcount );
    }

    /* Grant the resource if available and state is ACTIVE and resource is not
       granted to any other module. */
    if ( VAGENT_SUBS_STATE_ACTIVE == session_obj->state )
    {
      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "VAGENT: IRESOURCE_EVENT_REQUEST(PROCESS): VSID=(0x%08x): "
             "ASID=(%d): STATE(ACTIVE): for module=(%d)", 
             subs_obj->vsid, subs_obj->asid , module_id );

      if ( ( vagent_num_available_resource > 0 ) &&
           ( VAGENT_MODULE_ID_UNDEFINED == session_obj->current_module ) )
      {
        rc = vagent_resource_grant ( session_obj, module_id );
        if ( rc )
        {
          MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "VAGENT: IRESOURCE_EVENT_REQUEST(PROCESS): "
               "resource grant error: module=(%d): rc=(0x%08x)", 
               module_id, rc );
          break;
        }

        /* Update the current module to which the resource is granted. */
        session_obj->current_module = module_id;
        /* Clear any stale pending request */
        session_obj->pending_module = VAGENT_MODULE_ID_UNDEFINED;
        vagent_num_available_resource--;
        MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
               "VAGENT: IRESOURCE_EVENT_REQUEST(PROCESS): "
               "granted_to_module=(%d): RefCount=(%d): no_of_resources--(%d)", 
               module_id, subs_obj->call_refcount, vagent_num_available_resource  );

        /* ECALL run concurrently with other module. 
         * Hence resource needs to be granted to ecall & along with either of the
         * voice adapter together. 
         */ 
        if( session_obj->ecall_resource_request == TRUE )
        {
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
                 "VAGENT: IRESOURCE_EVENT_REQUEST(PROCESS): "
                 "Granting to eCall along with module=(%d)", module_id );
          rc = vagent_resource_grant( session_obj, VAGENT_MODULE_ID_ECALL );
          if ( rc )
          {
            MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
                 "VAGENT: IRESOURCE_EVENT_REQUEST(PROCESS): "
                 "resource grant error: eCall/module=(%d): rc=(0x%08x)", 
                 module_id, rc );
            break;
          }
        }
      }
      else
      {
        MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
               "VAGENT: IRESOURCE_EVENT_REQUEST(PROCESS): CANNOT grant to "
               "module=(%d): no_of_resources=(%d), current_module=(0x%x)",
               module_id, vagent_num_available_resource, 
               session_obj->current_module );

        /* Revoke the resource from current module if already granted to someone.
         * Grant to pending module once release notification recived from current
         * module.
         */
        if ( session_obj->current_module != VAGENT_MODULE_ID_UNDEFINED )
        {
          MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
                 "VAGENT: IRESOURCE_EVENT_REQUEST(PROCESS): "
                 "revoking resource from current_module=(%d) for the module=(%d)",
                 session_obj->current_module, module_id );
          rc = vagent_resource_revoke( session_obj );
          if ( rc )
          {
            MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
                 "VAGENT: IRESOURCE_EVENT_REQUEST(PROCESS): "
                 "resource revoke error: from module=(%d): rc=(0x%08x)", 
                 session_obj->current_module, rc );
            break;
          }
          session_obj->pending_module = module_id;
        }
      }  /* if ( ( vagent_num_available_resource > 0 ) && */
    }
    /* state = VAGENT_SUBS_STATE_INACTIVE*/
    else  
    {
      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "VAGENT: IRESOURCE_EVENT_REQUEST(PROCESS): VSID=(0x%08x): "
             "ASID=(%d): STATE(INACTIVE): module=(%d)", 
             subs_obj->vsid, subs_obj->asid , module_id );

      if( ( VAGENT_MODULE_ID_IVA == module_id ) &&
          ( vagent_num_available_resource > 0 ) )
      {
        /* Unlike CS voice call clients, for IMS we do not depend on CM call 
         * events to grant vocoder resource. Instead it is based off when IMS 
         * request for resource. Reason being that not all IMS call events are 
         * routed to CM when 3rd party IMS is used (ex: EARLY MEDIA event). */

        /* Update the state to ACTIVE. */
        session_obj->state = VAGENT_SUBS_STATE_ACTIVE;
        rc = vagent_resource_grant ( session_obj, module_id );
        if ( rc )
        {
          MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "VAGENT: IRESOURCE_EVENT_REQUEST(PROCESS): STATE(->ACTIVE): "
               "resource grant error for IVA: module=(%d): rc=(0x%08x)", 
               module_id, rc );
          break;
        }
        /* Update the current module to which the resource is granted. */
        session_obj->current_module = module_id;
        /* Clear any stale pending request */
        session_obj->pending_module = VAGENT_MODULE_ID_UNDEFINED;
        vagent_num_available_resource--;
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
               "VAGENT: IRESOURCE_EVENT_REQUEST(PROCESS): STATE(->ACTIVE): "
               "RefCount=(%d), granted resource to IVA: no_of_resources--(%d):", 
               subs_obj->call_refcount, vagent_num_available_resource );

      }
      else
      {
        /* For IVA only, revoke the vocoder from the current module and grant
           to IVA.
           We could hit this scenario in case of SRVCC conference call
           failure, and IVA request for resource again.
           In this scenario, Voice Agent state would be INACTIVE and reference
           count would be 0 and the resource is available with CS protocol. */
        if ( VAGENT_MODULE_ID_IVA == module_id )
        {
          /* Update the state to ACTIVE. */
          session_obj->state = VAGENT_SUBS_STATE_ACTIVE;
          session_obj->pending_module = module_id;
          MSG_4( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
                 "VAGENT: IRESOURCE_EVENT_REQUEST(PROCESS): "
                 "STATE(->ACTIVE): revoking resource from current_module=(%d)"
                 " for module=(%d); RefCount=(%d): no_of_resources=(%d)", 
                 session_obj->current_module, session_obj->pending_module,
                 subs_obj->call_refcount, vagent_num_available_resource );
          rc = vagent_resource_revoke( session_obj );
          if ( rc )
          {
            MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
                 "VAGENT: IRESOURCE_EVENT_REQUEST(PROCESS): "
                 "STATE(->ACTIVE): resource revoke error: from module=(%d): rc=(0x%08x)", 
                 session_obj->current_module, rc );
            break;
          }
        }
        /* ( VAGENT_MODULE_ID_IVA != module_id ) */
        else
        {
          /* Cache the request now. Grant only after session moves to ACTIVE state.
           * Update the cache with new module_id ( handovers before call connect.)
           */
          MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
                 "VAGENT: IRESOURCE_EVENT_REQUEST(PROCESS): caching request for "
                 "module=(%d): RefCount=(%d): no_of_resources=(%d)",
                 module_id, subs_obj->call_refcount, vagent_num_available_resource );
          session_obj->pending_module = module_id;
        } /* end of if ( VAGENT_MODULE_ID_IVA == module_id ) */
      }
    }  /* end of if ( VAGENT_SUBS_STATE_ACTIVE == session_obj->state ) */

    break;
  } /* for ( ;; ) */

  ( void ) vagent_free_event_packet( packet );

  return rc;

}


VAGENT_INTERNAL uint32_t vagent_process_ecall_resource_request_event ( 
  vagent_event_packet_t* packet,
  uint32_t module_id
)
{

  uint32_t rc = APR_EOK;
  vagent_session_object_t* session_obj = NULL;
  vagent_modem_subs_object_t* subs_obj = NULL;

  for ( ;; )
  {
    session_obj = packet->session_context;
    if( session_obj == NULL )
    {
      VAGENT_REPORT_FATAL_ON_ERROR( APR_EBADPARAM );
      break;
    }

    subs_obj = session_obj->modem_subs_obj;
    if( subs_obj == NULL )
    {
      VAGENT_REPORT_FATAL_ON_ERROR( APR_EBADPARAM );
      break;
    }

    /* Caching the resource request from ECALL.*/
    session_obj->ecall_resource_request = TRUE;

    /*
     * ECALL run concurrently with other module. 
     * Hence resource needs to be granted to ecall & along with either of the
     * voice adapter together. 
     */ 
    if ( ( session_obj->current_module != VAGENT_MODULE_ID_UNDEFINED ) &&
         ( session_obj->current_module !=  VAGENT_MODULE_ID_ECALL ) )
    {
      MSG_6( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "VAGENT: ECALL_IRESOURCE_EVENT_REQUEST(PROCESS): STATE(%d): "
             "VSID=(0x%08x): ASID=(%d): current_module=(%d), no_of_resources=(%d),"
             "RefCount=(%d), granting to eCall", session_obj->state,
             subs_obj->vsid, subs_obj->asid, session_obj->current_module,
             vagent_num_available_resource, subs_obj->call_refcount );
      rc = vagent_resource_grant ( session_obj, VAGENT_MODULE_ID_ECALL );
      if ( rc )
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "VAGENT: ECALL_IRESOURCE_EVENT_REQUEST(PROCESS): "
             "resource grant error for eCall: module=(%d): rc=(0x%08x)", 
             module_id, rc );
      }
    }
    else
    {
      MSG_6( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "VAGENT: ECALL_IRESOURCE_EVENT_REQUEST(PROCESS): STATE(%d): "
             "VSID=(0x%08x): ASID=(%d): current_module=(%d), no_of_resources=(%d),"
             "RefCount=(%d), SOMETHING WENT WRONG", session_obj->state,
             subs_obj->vsid, subs_obj->asid, session_obj->current_module, 
             vagent_num_available_resource, subs_obj->call_refcount );
    }
    break;
  } /* end of for ( ;; ) */

  ( void ) vagent_free_event_packet( packet );

  return rc;

}


VAGENT_INTERNAL uint32_t vagent_process_resource_released_event ( 
  vagent_event_packet_t* packet,
  uint32_t module_id
)
{

  uint32_t rc = APR_EOK;
  vagent_session_object_t* session_obj = NULL;
  vagent_modem_subs_object_t* subs_obj = NULL;

  for ( ;; )
  {
    session_obj = packet->session_context;
    if( session_obj == NULL )
    {
      VAGENT_REPORT_FATAL_ON_ERROR( APR_EBADPARAM );
      break;
    }

    subs_obj = session_obj->modem_subs_obj;
    if( subs_obj == NULL )
    {
      VAGENT_REPORT_FATAL_ON_ERROR( APR_EBADPARAM );
      break;
    }

    if ( session_obj->pending_module == module_id )
    {
      MSG_6( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "VAGENT: resource_released_event(PROCESS): event from "
             "pending module=(%d); VSID=(0x%08x): ASID=(%d): STATE=(%d):"
             "no_of_resources=(%d): current_module=(%d)", 
             session_obj->pending_module, subs_obj->vsid, subs_obj->asid, 
             session_obj->state, vagent_num_available_resource, 
             session_obj->current_module );
      /* Clearing cached request(if any). */
      session_obj->pending_module = VAGENT_MODULE_ID_UNDEFINED;

      break;
    }

    if ( session_obj->current_module !=  module_id )
    {
      MSG_6( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "VAGENT: resource_released_event(PROCESS): event from "
             "different module=(%d); VSID=(0x%08x): ASID=(%d): STATE=(%d):"
             "no_of_resources=(%d): current_module=(%d)", 
             module_id, subs_obj->vsid, subs_obj->asid, session_obj->state,
             vagent_num_available_resource, session_obj->current_module );
      break;
    }

    vagent_num_available_resource++;
    session_obj->current_module = VAGENT_MODULE_ID_UNDEFINED;

    MSG_5( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
           "VAGENT(): resource_released_event(PROCESS): module=(%d): "
           "VSID=(0x%08x): ASID=(%d): STATE=(%d): no_of_resources++(%d)",
           module_id, subs_obj->vsid, subs_obj->asid,
           session_obj->state, vagent_num_available_resource );

    /* Evaluate if any pending request for resource and no. of available
     * resource is more than 0.
     */
    if ( ( vagent_num_available_resource > 0 ) &&
         ( session_obj->pending_module != VAGENT_MODULE_ID_UNDEFINED ) )
    {
      /* During SRVCC failure of conference calls or when using 3rd party IMS,
       * the reference count would be 1. 
       * When 1, do not decrement the reference count for the 
       * vocoder resource to be granted to CS clients. 
       * Call reference count would be decremented to 0 (to INACTIVE) state 
       * when the CS call ends (after successful SRVCC) 
       */
      if ( VAGENT_MODULE_ID_IVA == module_id )
      {
        if ( 1 == subs_obj->call_refcount  )
        {
          /* do nothing in case of 3rd party IMS */
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
                 "VAGENT(): resource_released_event(PROCESS): from IVA module(%d): "
                 "DO NOT DECREMENT since RefCount=(1).", module_id );
        }
        else
        {
          subs_obj->call_refcount--;
          MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
                 "VAGENT(): resource_released_event(PROCESS): from IVA module(%d): "
                 "RefCount--(%d)", module_id, subs_obj->call_refcount );
        }
      }

      rc = vagent_resource_grant ( session_obj, session_obj->pending_module );
      if ( rc )
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "VAGENT(): resource_released_event(PROCESS): resource grant error for "
             "module=(%d): rc=(0x%08x)", session_obj->pending_module, rc );
        break;
      }

      /* Update the current module to which the resource is granted. */
      session_obj->current_module = session_obj->pending_module;
      session_obj->pending_module = VAGENT_MODULE_ID_UNDEFINED;
      vagent_num_available_resource--;
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "VAGENT(): resource_released_event(PROCESS): "
             "granted resource to module=(%d): no_of_resources--(%d)",
             session_obj->current_module, vagent_num_available_resource );

      break;
    }
    else
    {
      /* This is required for handling 3rd party IMS, where CM CALL events
       * are not available for maintaing call reference count and voice
       * agent state machine control comes here for VoLTE CALL END. 
       */
      if( ( subs_obj->call_refcount > 0 ) &&
          ( VAGENT_MODULE_ID_IVA == module_id ) )
      {
        subs_obj->call_refcount--;
        MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
               "VAGENT(): resource_released_event(PROCESS): from IVA "
               "module=(%d): RefCount--(%d): no_of_resources=(%d)",
               module_id, subs_obj->call_refcount, vagent_num_available_resource );

        if( 0 == subs_obj->call_refcount )
        {
          session_obj->state = VAGENT_SUBS_STATE_INACTIVE;
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
               "VAGENT(): resource_released_event(PROCESS): from IVA module=(%d)"
               "RefCount=(0): state=(ACTIVE->INACTIVE)", module_id );
        }
      }
    }
    break;
  }

  ( void ) vagent_free_event_packet( packet );

  return rc;
}


VAGENT_INTERNAL uint32_t vagent_process_ecall_resource_released_event ( 
  vagent_event_packet_t* packet,
  uint32_t module_id
)
{

  uint32_t rc = APR_EOK;
  vagent_session_object_t* session_obj = NULL;
  vagent_modem_subs_object_t* subs_obj = NULL;

  for ( ;; )
  {
    session_obj = packet->session_context;
    if( session_obj == NULL )
    {
      VAGENT_REPORT_FATAL_ON_ERROR( APR_EBADPARAM );
      break;
    }

    subs_obj = session_obj->modem_subs_obj;
    if( subs_obj == NULL )
    {
      VAGENT_REPORT_FATAL_ON_ERROR( APR_EBADPARAM );
      break;
    }

    /* Caching the resource release status from ECALL.*/
    session_obj->ecall_resource_request = FALSE;

    MSG_6( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
           "VAGENT: ECALL_IRESOURCE_EVENT_RELEASED(PROCESS): STATE(%d): "
           "VSID=(0x%08x): ASID=(%d): current_module=(%d), no_of_resources=(%d),"
           "RefCount=(%d)", session_obj->state, subs_obj->vsid, subs_obj->asid,
           session_obj->current_module, vagent_num_available_resource,
           subs_obj->call_refcount );

    break;
  }

  ( void ) vagent_free_event_packet( packet );

  return rc;

}


VAGENT_INTERNAL void vagent_process_nongating_work_items (
  void
)
{

  uint32_t rc = APR_EOK;
  uint32_t event_id = 0;
  vagent_work_item_t* work_item = NULL;
  vagent_event_packet_t* packet = NULL;

  while( apr_list_remove_head( &vagent_nongating_work_pkt_q,
                               ( ( apr_list_node_t** ) &work_item ) ) == APR_EOK )
  {
    packet = work_item->packet;

    /* Add back to vs_free_work_pkt_q. */
    work_item->packet = NULL;
    ( void ) apr_list_add_tail( &vagent_free_work_pkt_q, &work_item->link );

    if ( packet == NULL )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "vagent_task_process_nongating_work_items(): work packet NULL!! " );
      continue;
    }
    else
    {
      event_id = packet->event_id;
    }

    switch( event_id )
    {
     /**
      * Handling routine for nongating work-items should take of release the
      * memory allocated for the CMD/EVENT packets.
      */
     case IVA_IRESOURCE_EVENT_REQUEST:
       MSG( MSG_SSID_DFLT, MSG_LEGACY_MED,
              "VAGENT(): Processing IVA_IRESOURCE_EVENT_REQUEST" );
       rc = vagent_process_resource_request_event( packet, VAGENT_MODULE_ID_IVA );
       break;

     case IVA_IRESOURCE_EVENT_RELEASED:
       MSG( MSG_SSID_DFLT, MSG_LEGACY_MED,
              "VAGENT(): Processing IVA_IRESOURCE_EVENT_RELEASED" );
       rc = vagent_process_resource_released_event( packet, VAGENT_MODULE_ID_IVA );
       break;

     case GVA_IRESOURCE_EVENT_REQUEST:
       MSG( MSG_SSID_DFLT, MSG_LEGACY_MED,
              "VAGENT(): Processing GVA_IRESOURCE_EVENT_REQUEST" );
       rc = vagent_process_resource_request_event( packet, VAGENT_MODULE_ID_GVA );
       break;

     case GVA_IRESOURCE_EVENT_RELEASED:
       MSG( MSG_SSID_DFLT, MSG_LEGACY_MED,
              "VAGENT(): Processing GVA_IRESOURCE_EVENT_RELEASED" );
       rc = vagent_process_resource_released_event( packet, VAGENT_MODULE_ID_GVA );
       break;

     case WVA_IRESOURCE_EVENT_REQUEST:
       MSG( MSG_SSID_DFLT, MSG_LEGACY_MED,
              "VAGENT(): Processing WVA_IRESOURCE_EVENT_REQUEST" );
       rc = vagent_process_resource_request_event( packet, VAGENT_MODULE_ID_WVA );
       break;

     case WVA_IRESOURCE_EVENT_RELEASED:
       MSG( MSG_SSID_DFLT, MSG_LEGACY_MED,
              "VAGENT(): Processing WVA_IRESOURCE_EVENT_RELEASED" );
       rc = vagent_process_resource_released_event( packet, VAGENT_MODULE_ID_WVA );
       break;

     case TVA_IRESOURCE_EVENT_REQUEST:
       MSG( MSG_SSID_DFLT, MSG_LEGACY_MED,
              "VAGENT(): Processing TVA_IRESOURCE_EVENT_REQUEST" );
       rc = vagent_process_resource_request_event( packet, VAGENT_MODULE_ID_TVA );
       break;

     case TVA_IRESOURCE_EVENT_RELEASED:
       MSG( MSG_SSID_DFLT, MSG_LEGACY_MED,
              "VAGENT(): Processing TVA_IRESOURCE_EVENT_RELEASED" );
       rc = vagent_process_resource_released_event( packet, VAGENT_MODULE_ID_TVA );
       break;

     case CVA_IRESOURCE_EVENT_REQUEST:
       MSG( MSG_SSID_DFLT, MSG_LEGACY_MED,
              "VAGENT(): Processing CVA_IRESOURCE_EVENT_REQUEST" );
       rc = vagent_process_resource_request_event( packet, VAGENT_MODULE_ID_CVA );
       break;

     case CVA_IRESOURCE_EVENT_RELEASED:
       MSG( MSG_SSID_DFLT, MSG_LEGACY_MED,
              "VAGENT(): Processing CVA_IRESOURCE_EVENT_RELEASED" );
       rc = vagent_process_resource_released_event( packet, VAGENT_MODULE_ID_CVA );
       break;

     case ECALL_VOICE_IRESOURCE_EVENT_REQUEST:
       MSG( MSG_SSID_DFLT, MSG_LEGACY_MED,
              "VAGENT(): Processing ECALL_VOICE_IRESOURCE_EVENT_REQUEST" );
       rc = vagent_process_ecall_resource_request_event( packet, VAGENT_MODULE_ID_ECALL );
       break;

     case ECALL_VOICE_IRESOURCE_EVENT_RELEASED:
       MSG( MSG_SSID_DFLT, MSG_LEGACY_MED,
              "VAGENT(): Processing ECALL_VOICE_IRESOURCE_EVENT_RELEASED" );
       rc = vagent_process_ecall_resource_released_event( packet, VAGENT_MODULE_ID_ECALL );
       break;

     case VAGENT_EVENTI_CM_SUBS_INFO:
       MSG( MSG_SSID_DFLT, MSG_LEGACY_MED,
              "VAGENT(): Processing VAGENT_EVENTI_CM_SUBS_INFO" );
       rc = vagent_process_cm_subs_info_event( packet );
       break;

     case VAGENT_EVENTI_CM_CALL_EVENT:
       MSG( MSG_SSID_DFLT, MSG_LEGACY_MED,
              "VAGENT(): Processing VAGENT_EVENTI_CM_CALL_EVENT" );
       rc = vagent_process_cm_call_event( packet );
       break;

     default:
       {
         /* Handle unsupported request. */
         MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
                "VAGENT(): Unsupported event=(%d)", event_id );
         vagent_free_event_packet( packet );
       }
       break;
    } /* switch case ends. */

  } /* while loop ends. */

  return;

}
/****************************************************************************
 *  END of VOICE AGENT - EVENT CALLBACK PROCESSING FUNCTIONS                * 
 *  These function executes in voice agent context                          * 
 *  1. vagent_process_cm_call_event                                         * 
 *  2. vagent_process_cm_subs_info_event                                    * 
 *  3. vagent_process_resource_request_event                                * 
 *  4. vagent_process_ecall_resource_request_event                          * 
 *  5. vagent_process_resource_released_event                               * 
 *  6. vagent_process_ecall_resource_released_event                         * 
 *  7. vagent_process_nongating_work_items                                  * 
*****************************************************************************/



/****************************************************************************
 * TASK ROUTINES                                                        *
 ****************************************************************************/

/* This function executes in calling task (server) context */
VAGENT_INTERNAL void vagent_signal_run (
  void
)
{

  apr_event_signal( vagent_work_event );

}


VAGENT_INTERNAL int32_t vagent_run (
  void
)
{

  vagent_process_nongating_work_items( );
  return APR_EOK;

}

/* Voice agent's main worker thread function */
VAGENT_INTERNAL int32_t vagent_worker_thread_fn (
  void* param
)
{

  int32_t rc = APR_EOK;

  apr_event_create( &vagent_work_event );
  apr_event_signal( vagent_control_event );

  for ( ;; )
  {
    rc = apr_event_wait( vagent_work_event );
    if ( rc )
    {
      break;
    }
    vagent_run( );
  }

  apr_event_destroy( vagent_work_event );
  apr_event_signal( vagent_control_event );

  return rc;

}

/****************************************************************************
 * POWER UP/DOWN ROUTINES                                                   *
 ****************************************************************************/

VAGENT_INTERNAL int32_t vagent_init (
  void 
)
{

  uint32_t rc = APR_EOK;
  uint32_t index = 0;
  RCINIT_INFO info_handle = NULL;
  RCINIT_PRIO priority = 0;
  unsigned long stack_size = 0;

  MSG_2( MSG_SSID_DFLT, MSG_LEGACY_LOW,
         "vagent_init(): Date %s Time %s", __DATE__, __TIME__ );

  {  /* Initialize the locks. */
    rc = apr_lock_create( APR_LOCK_TYPE_INTERRUPT, &vagent_int_lock );
    apr_event_create( &vagent_control_event );
  }

  { /* Initialize the custom heap. */
    apr_memmgr_init_heap( &vagent_heapmgr, ( ( void* ) &vagent_heap_pool ),
                          sizeof( vagent_heap_pool ), NULL, NULL );
  }

  { /* Initialize the object manager. */
    apr_objmgr_setup_params_t params;
  
    params.table = vagent_object_table;
    params.total_bits = VAGENT_HANDLE_TOTAL_BITS_V;
    params.index_bits = VAGENT_HANDLE_INDEX_BITS_V;
    params.lock_fn = vagent_int_lock_fn;
    params.unlock_fn = vagent_int_unlock_fn;
    rc = apr_objmgr_construct( &vagent_objmgr, &params );
    VAGENT_PANIC_ON_ERROR( rc );
  }

  { /* Initialize free and nongating work pkt queues. */
    rc = apr_list_init_v2( &vagent_free_work_pkt_q, 
                           vagent_int_lock_fn, vagent_int_unlock_fn );
    for ( index = 0; index < VAGENT_NUM_WORK_PKTS_V; ++index )
    {
      ( void ) apr_list_init_node( ( apr_list_node_t* ) &vagent_work_pkts[index] );
      vagent_work_pkts[index].packet = NULL;
      rc = apr_list_add_tail( &vagent_free_work_pkt_q,
                              ( ( apr_list_node_t* ) &vagent_work_pkts[index] ) );
    }

    rc = apr_list_init_v2( &vagent_nongating_work_pkt_q,
                           vagent_int_lock_fn, vagent_int_unlock_fn );
  }

  { /* Initialize the global session lock. */
    rc = apr_lock_create( APR_LOCK_TYPE_MUTEX, &vagent_global_lock );
    VAGENT_PANIC_ON_ERROR( rc );
  }

  { /* Create the GVA task worker thread. */
    info_handle = rcinit_lookup( VAGENT_TASK_NAME );
    if ( info_handle == NULL ) 
    {
      /* Use the default priority & stack_size*/
      MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,
           "vagent_init(): VAGENT task not registered with RCINIT" );
      priority = VAGENT_TASK_PRIORITY;
      stack_size = VAGENT_TASK_STACK_SIZE;
    }
    else
    {
      priority = rcinit_lookup_prio_info( info_handle );
      stack_size = rcinit_lookup_stksz_info( info_handle );
    }

    if ( ( priority > 255 ) || ( stack_size == 0 ) ) 
    {
      ERR_FATAL( "vagent_init(): Invalid priority: %d or stack size: %d",
                 priority, stack_size, 0 );
    }

    rc = apr_thread_create( &vagent_thread, VAGENT_TASK_NAME, TASK_PRIORITY( priority ),
                            vagent_task_stack, stack_size, 
                            vagent_worker_thread_fn , NULL );
    VAGENT_PANIC_ON_ERROR( rc );

    apr_event_wait( vagent_control_event );
  }

  vagent_is_initialized = TRUE;

  return rc;

}


VAGENT_INTERNAL int32_t vagent_postinit ( 
  void
)
{

  uint32_t rc = APR_EOK;
  uint32_t index = 0;
  bool_t ret_val = FALSE;
  cm_client_id_type vagent_client_id = CM_CLIENT_TYPE_NONE;

  /* Initialize the subscription/session object list. */
  for( index = 0; index < VAGENT_MAX_NUM_OF_SESSIONS_V; ++index )
  {
    rc = vagent_create_modem_subs_object( &vagent_modem_subs_obj_list[index] );
    VAGENT_PANIC_ON_ERROR(rc);

    vagent_modem_subs_obj_list[index]->asid = ( sys_modem_as_id_e_type )index;
    rc = vagent_create_session_object( &vagent_session_obj_list[index] );
    VAGENT_PANIC_ON_ERROR(rc);

  }

  /* CM registration for CM Phone event for asid->vsid mapping. */
  rc = cm_client_init( CM_CLIENT_TYPE_VA, &vagent_client_id );
  if ( rc == CM_CLIENT_OK )
  {
    /* Resigter with CM for subscription information events. This event is
     * generated when subscription information is changed. 
     * Voice Agent is interested in knowing when subscription available/active
     * status changes, and ASID-VSID mapping information changes.
     */
    cm_client_subs_reg( vagent_client_id, vagent_cm_subs_info_cb );

    /* Regsiter for CM call events in the range [CM_CALL_EVENT_ORIG -
     * CM_CALL_EVENT_CONNECT]. */
    cm_mm_client_call_reg( vagent_client_id, vagent_cm_call_event_cb,
                           CM_CLIENT_EVENT_REG, CM_CALL_EVENT_ORIG,
                           CM_CALL_EVENT_CONNECT, NULL );

    /* Register for CM call event CM_CALL_EVENT_PROGRESS_INFO_IND
     * This event is applicable to GSM, WCDMA, TDSCDMA and IMS. */
    cm_mm_client_call_reg( vagent_client_id, vagent_cm_call_event_cb,
                           CM_CLIENT_EVENT_REG, CM_CALL_EVENT_PROGRESS_INFO_IND,
                           CM_CALL_EVENT_PROGRESS_INFO_IND, NULL );

    /* CM registration for CM call events for SRVCC - for handling 3rd party 
     * IMS that does not go through Voice Agent */
    cm_mm_client_call_reg( vagent_client_id, vagent_cm_call_event_cb,
                           CM_CLIENT_EVENT_REG, CM_CALL_EVENT_HO_START,
                           CM_CALL_EVENT_HO_START, NULL );

    /* CM registration for CM call events for SRVCC - for handling 3rd party IMS
       that does not go through Voice Agent */
    cm_mm_client_call_reg( vagent_client_id, vagent_cm_call_event_cb,
                           CM_CLIENT_EVENT_REG, CM_CALL_EVENT_HO_COMPLETE,
                           CM_CALL_EVENT_HO_COMPLETE, NULL );
  }
  else
  {
    VAGENT_REPORT_FATAL_ON_ERROR( rc );
  }

  /* TODO: CM regitration for LCH/ACTIVE events per subscription for DSDA. */

  /* Required, if CM has already broadcasted CM phone event before vagent
   * boot-up initialization. */
  index = 0;
  while ( index < SYS_MODEM_AS_ID_MAX )
  {
    ret_val = cm_ph_cmd_get_subs_pref_info( NULL, NULL, vagent_client_id,
                                            ( sys_modem_as_id_e_type ) index );
    if ( ret_val == FALSE ) 
    {
      VAGENT_REPORT_FATAL_ON_ERROR( APR_ENORESOURCE );
    }
    index++;
  }

  return rc;

}


VAGENT_INTERNAL int32_t vagent_predeinit (
  void
)
{

  uint32_t rc = APR_EOK;
  uint32_t index = 0;

  /* Destroy Session/Subscription object list. */
  for( index = 0; index < VAGENT_MAX_NUM_OF_SESSIONS_V; ++index )
  {
    ( void )vagent_mem_free_object( 
                   ( vagent_object_t* )vagent_modem_subs_obj_list[index] );

   ( void) vagent_resource_deregister( vagent_session_obj_list[index] );

    ( void )vagent_mem_free_object(
                   ( vagent_object_t* )vagent_session_obj_list[index] );
  }

  return rc;

}


VAGENT_INTERNAL int32_t vagent_deinit (
  void 
)
{

  uint32_t rc = APR_EOK;

  vagent_is_initialized = FALSE;

  apr_event_signal_abortall( vagent_work_event );
  apr_event_wait( vagent_control_event );

  /* Release work queue */
  ( void ) apr_list_destroy( &vagent_free_work_pkt_q );
  ( void ) apr_list_destroy( &vagent_nongating_work_pkt_q );

  /* Deinitialize the object handle table. */
  ( void ) apr_objmgr_destruct( &vagent_objmgr );

  /* Deinitialize basic OS resources for staging the setup. */
  ( void ) apr_event_destroy( vagent_control_event );
  ( void ) apr_lock_destroy( vagent_int_lock );
  ( void ) apr_lock_destroy( vagent_global_lock );

  return rc;

}


/****************************************************************************
 * SINGLE ENTRY POINT                                                       *
 ****************************************************************************/

VAGENT_EXTERNAL uint32_t vagent_call (
  uint32_t cmd_id,
  void* params,
  uint32_t size
)
{

  uint32_t rc = APR_EOK;

  switch ( cmd_id )
  {
  case DRV_CMDID_INIT:
    rc = vagent_init( );
    break;

  case DRV_CMDID_POSTINIT:
    rc = vagent_postinit( );
    break;

  case DRV_CMDID_PREDEINIT:
    rc = vagent_predeinit( );
    break;

  case DRV_CMDID_DEINIT:
    rc = vagent_deinit( );
    break;

  default:
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "vagent_call(): Unsupported cmd ID (0x%08x)", cmd_id );
    rc = APR_EUNSUPPORTED;
    break;
  }

  return rc;

}

