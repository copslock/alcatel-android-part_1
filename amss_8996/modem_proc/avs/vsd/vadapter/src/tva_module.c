/*
   Copyright (C) 2015-2016 Qualcomm Technologies, Inc.
   All Rights Reserved.
   Confidential and Proprietary - Qualcomm Technologies, Inc.

   $Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/avs/vsd/vadapter/src/tva_module.c#5 $
   $Author: rajatm $
*/

/****************************************************************************
 * INCLUDE HEADER FILES                                                     *
 ****************************************************************************/

#include <stddef.h>
#include <string.h>
#include "err.h"
#include "msg.h"
#include "rcinit.h"
#include "mmstd.h"

/* APR APIs. */
#include "apr_errcodes.h"
#include "apr_list.h"
#include "apr_objmgr.h"
#include "apr_lock.h"
#include "apr_timer.h"
#include "apr_event.h"
#include "apr_thread.h"
#include "apr_memmgr.h"

/* TDSCDMA APIs. */
#include "tds_ext_api.h"

#ifdef FEATURE_SEGMENT_LOADING
#include "mcfg_seg_load.h"
#endif

/* VSD APIs. */
#include "drv_api.h"
#include "vs_task.h"
#include "voice_util_api.h"
#include "vs.h"

/* SELF APIs. */
#include "tva_if.h"
#include "tva_iresource_if.h"
#include "tva_i.h"

/*****************************************************************************
 * Defines                                                                   *
 ****************************************************************************/

#define TVA_MAX_NUM_OF_SESSIONS_V ( 2 )
#define TVA_MAX_VOC_FRAME_LENGTH ( 70 )

/* TDSCDMA picks UL packet from DSM queue at 1000 micro sec from VFR. 
 */
#define TVA_TDSCDMA_READS_UL_PKT_FRM_DSM_AT_TIME_V ( 1000 )

/* Buffer time needed to put vocoder packet in DSM Queue.
 * Time in micro second. 
 */
#define TVA_BUF_TIME_FOR_PUTTING_UL_PACKET_IN_DSM_V ( 1000 )

/* The frame size in microseconds that each voice processing threads 
 * (vptx, vprx, encoder, decoder, decoder pp) operates on.
 */
#define TVA_VOICE_FRAME_SIZE_US_V ( 20000 ) 

#define TVA_VOICE_SAMPLE_RATE_UNDEFINED_V ( 0 )

#define TVA_VOICE_SAMPLE_RATE_NB_V ( 8000 )

#define TVA_VOICE_SAMPLE_RATE_WB_V ( 16000 )


/*****************************************************************************
 * Global Variables                                                          *
 ****************************************************************************/

static apr_lock_t tva_int_lock;
static apr_lock_t tva_thread_lock;
static apr_event_t tva_control_event;

static apr_memmgr_type tva_heapmgr;
static uint8_t tva_heap_pool[ TVA_HEAP_SIZE_V ];

static apr_objmgr_t tva_objmgr;
static apr_objmgr_object_t tva_object_table[ TVA_MAX_OBJECTS_V ];

static tva_gating_control_t tva_gating_work_pkt_q;
static apr_list_t tva_nongating_work_pkt_q;
static apr_list_t tva_free_work_pkt_q;
static tva_work_item_t tva_work_pkts[ TVA_NUM_WORK_PKTS_V ];


static apr_event_t tva_work_event;
static apr_thread_t tva_thread;
static uint8_t tva_task_stack[ TVA_TASK_STACK_SIZE ];

static tva_modem_subs_object_t* tva_subs_obj_list[TVA_MAX_NUM_OF_SESSIONS_V];
static tva_session_object_t* tva_session_obj_list[TVA_MAX_NUM_OF_SESSIONS_V];
static apr_lock_t tva_global_lock;

static bool_t tva_is_initialized = FALSE; 

#ifdef FEATURE_SEGMENT_LOADING
  interface_t *tva_ptr_Td =  NULL;
#endif

/****************************************************************************
 * COMMON INTERNAL ROUTINES                                                 *
 ****************************************************************************/

static void tva_int_lock_fn ( void )
{
  apr_lock_enter( tva_int_lock );
}

static void tva_int_unlock_fn ( void )
{
  apr_lock_leave( tva_int_lock );
}

static void tva_thread_lock_fn ( void )
{
  apr_lock_enter( tva_thread_lock );
}

static void tva_thread_unlock_fn ( void )
{
  apr_lock_leave( tva_thread_lock );
}


static void tva_sfg_timer_cb (
  void* session_context
)
{
  uint32_t rc = APR_EOK;

  MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,
         "TVA_INTERNAL_EVENT_SEND_SILENCE_FRAME -  session_obj=(0x%08x), "
         "event_id=(0x%08x)", session_context, TVA_INTERNAL_EVENT_SEND_SILENCE_FRAME );

  rc = tva_prepare_and_dispatch_event_packet( session_context,
         TVA_INTERNAL_EVENT_SEND_SILENCE_FRAME, NULL, 0 );

  return;
}

/****************************************************************************
 * TVA CMDs & EVENTs PACKET QUEUING FUNCTIONS                               *
 ****************************************************************************/

/**
 * Queues the tva_cmd_packet_t and tva_event_packet_t. In
 * case of failure to queue a apr packet, packet shall be
 * freed by the caller.
 */
TVA_INTERNAL uint32_t tva_queue_work_packet (
  tva_work_item_queue_type_t queue_type,
  tva_work_item_packet_type_t pkt_type,
  void* packet
)
{
  uint32_t rc = APR_EOK;
  tva_work_item_t* work_item = NULL;
  apr_list_t* work_queue = NULL;

  switch ( queue_type )
  {
   case TVA_WORK_ITEM_QUEUE_TYPE_NONGATING:
     work_queue = &tva_nongating_work_pkt_q;
     break;

   case TVA_WORK_ITEM_QUEUE_TYPE_GATING:
     work_queue = &tva_gating_work_pkt_q.cmd_q;
     break;

   default:
     rc = APR_EUNSUPPORTED;
     break;
  }

  for ( ;; )
  {
    /* Get a free command structure. */
    rc = apr_list_remove_head( &tva_free_work_pkt_q,
                               ( ( apr_list_node_t** ) &work_item ) );
    if ( rc )
    {
      rc = APR_ENORESOURCE;
      /* No free WORK packet structure is available. */
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_FATAL,
             "tva_queue_work_packet(): Ran out of WORK packets, rc=0x%08x, "
             "tva_state=%d",   rc, tva_is_initialized );
      break;
    }

    if ( pkt_type == TVA_WORK_ITEM_PKT_TYPE_CMD )
    {
      work_item->pkt_type = TVA_WORK_ITEM_PKT_TYPE_CMD;
    }
    else if ( pkt_type == TVA_WORK_ITEM_PKT_TYPE_EVENT )
    {
     work_item->pkt_type = TVA_WORK_ITEM_PKT_TYPE_EVENT;
    }
    else
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_FATAL,
           "tva_queue_work_packet(): Invalid packet type!!!" );
      TVA_PANIC_ON_ERROR ( APR_ENOTEXIST );
    }

    work_item->packet = packet;


    /* Add to incoming request work queue */
    rc = apr_list_add_tail( work_queue, &work_item->link );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "tva_queue_work_packet() - ERROR: rc=0x%08x", rc );
      /* Add back to tva_free_work_pkt_q */
      work_item->pkt_type = TVA_WORK_ITEM_PKT_TYPE_NONE;
      work_item->packet = NULL;
      ( void ) apr_list_add_tail( &tva_free_work_pkt_q, &work_item->link );
    }
    else
    {
      /**
       * Signal appropriate thread.
       */
      tva_signal_run();
    }

    break;
  } /* for loop ends. */

  return rc;
}  /* tva_queue_work_packet() ends. */


/****************************************************************************
 * TVA CMDs/EVENTs PACKET PREPARE/DISPATCHER/FREE ROUTINES                  *
 ****************************************************************************/

TVA_INTERNAL uint32_t tva_free_cmd_packet (
  tva_cmd_packet_t* packet
)
{
  uint32_t rc = VS_EOK;

  if ( packet != NULL )
  {
    if( packet->params != NULL )
    {
      /* Free the memory - p_cmd_packet->params. */
      apr_memmgr_free( &tva_heapmgr, packet->params );
      packet->params = NULL;
    }

    /* Free the memeory - p_cmd_packet. */
    apr_memmgr_free( &tva_heapmgr, packet );
    packet = NULL;
  }

  return rc;
}

/**
 * This is a common routine facilitating to prepare and
 * dispatches a CMD PKT.
 */
TVA_INTERNAL uint32_t tva_prepare_and_dispatch_cmd_packet (
  uint32_t cmd_id,
  void* params,
  uint32_t size
)
{
  uint32_t rc = APR_EOK;
  tva_cmd_packet_t* packet = NULL;

  for ( ;; )
  {
    packet = ( ( tva_cmd_packet_t* ) apr_memmgr_malloc( &tva_heapmgr,
                                           sizeof( tva_cmd_packet_t ) ) );
    if ( packet == NULL )
    {
      TVA_REPORT_FATAL_ON_ERROR( APR_ENORESOURCE );
      rc = APR_ENORESOURCE;
      break;
    }

    packet->cmd_id = cmd_id;
    packet->params = NULL;

    if ( ( size > 0 ) && ( params != NULL ) )
    {
      packet->params = apr_memmgr_malloc(  &tva_heapmgr, size );

      if ( packet->params == NULL )
      {
        rc = APR_ENORESOURCE;
        TVA_REPORT_FATAL_ON_ERROR( rc );
        ( void ) tva_free_cmd_packet( packet );
        break;
      }
      mmstd_memcpy( packet->params, size, params, size );
    }

    /* Queue the command packet for processing. */
    rc = tva_queue_work_packet( TVA_WORK_ITEM_QUEUE_TYPE_NONGATING,
                                TVA_WORK_ITEM_PKT_TYPE_CMD, ( void*) packet );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "tva_prepare_and_dispatch_cmd_packet() - cmd pkt queuing failed. "
             "rc=(0x%08x)", rc );
      ( void ) tva_free_cmd_packet( packet );
    }
    else
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,
             "tva_prepare_and_dispatch_cmd_packet() cmd pkt queued with "
             "cmd_id=(0x%08x)", cmd_id );
    }

    break;
  }

  return rc;
}


TVA_INTERNAL uint32_t tva_free_event_packet (
  tva_event_packet_t* packet
)
{
  uint32_t rc = VS_EOK;

  if ( packet != NULL )
  {
    if( packet->params != NULL )
    {
      /* Free the memory - p_cmd_packet->params. */
      apr_memmgr_free( &tva_heapmgr, packet->params );
      packet->params = NULL;
    }

    /* Free the memeory - p_cmd_packet. */
    apr_memmgr_free( &tva_heapmgr, packet );
    packet= NULL;
  }

  return rc;
}

/**
 * This is a common routine facilitating to prepare and
 * dispatches a CMD PKT.
 */
TVA_INTERNAL uint32_t tva_prepare_and_dispatch_event_packet (
  void* session_context,
  uint32_t event_id,
  void* params,
  uint32_t size
)
{
  uint32_t rc = APR_EOK;
  tva_event_packet_t* packet = NULL;

  for ( ;; )
  {
    packet = ( ( tva_event_packet_t* ) apr_memmgr_malloc( &tva_heapmgr,
                                         sizeof( tva_event_packet_t ) ) );
    if ( packet == NULL )
    {
      TVA_REPORT_FATAL_ON_ERROR( APR_ENORESOURCE );
      rc = APR_ENORESOURCE;
      break;
    }

    packet->session_context = session_context;
    packet->event_id = event_id;
    packet->params = NULL;

    if ( ( size > 0 ) && ( params != NULL ) )
    {
      packet->params = apr_memmgr_malloc(  &tva_heapmgr, size );

      if ( packet->params == NULL )
      {
        rc = APR_ENORESOURCE;
        TVA_REPORT_FATAL_ON_ERROR( rc );
        ( void ) tva_free_event_packet( packet );
        break;
      }
      mmstd_memcpy( packet->params, size, params, size );
    }

    /* Queue the command packet for processing. */
    rc = tva_queue_work_packet( TVA_WORK_ITEM_QUEUE_TYPE_NONGATING,
                                TVA_WORK_ITEM_PKT_TYPE_EVENT, ( void*) packet );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "tva_prepare_and_dispatch_event_packet()-event pkt queuing failed "
             "rc=(0x%08x)", rc );
      ( void ) tva_free_event_packet( packet );
    }
    else
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,
             "tva_prepare_and_dispatch_event_packet()-event pkt queued with "
             "event_id=(0x%08x)", event_id );
    }

    break;
  }

  return rc;
}


/****************************************************************************
 * TVA OBJECT CREATION, DESTRUCTION AND INITIALISATION ROUTINES             *
 ****************************************************************************/

static int32_t tva_get_object (
  uint32_t handle,
  tva_object_t** ret_obj
)
{
  int32_t rc;
  apr_objmgr_object_t* objmgr_obj;

  if ( ret_obj == NULL )
  {
    return APR_EBADPARAM;
  }

  rc = apr_objmgr_find_object( &tva_objmgr, handle, &objmgr_obj );
  if ( rc )
  {
    return APR_EFAILED;
  }

  *ret_obj = ( ( tva_object_t* ) objmgr_obj->any.ptr );

  return APR_EOK;
}

static uint32_t tva_mem_alloc_object (
  uint32_t size,
  tva_object_t** ret_object
)
{
  int32_t rc;
  tva_object_t* tva_obj;
  apr_objmgr_object_t* objmgr_obj;

  if ( ret_object == NULL )
  {
    return APR_EBADPARAM;
  }

  { /* Allocate memory for the TVA object. */
    tva_obj = apr_memmgr_malloc( &tva_heapmgr, size );
    if ( tva_obj == NULL )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "tva_mem_alloc_object(): Out of memory, requested size (%d)", size );
      return APR_ENORESOURCE;
    }

    /* Allocate a new handle for the MVS object. */
    rc = apr_objmgr_alloc_object( &tva_objmgr, &objmgr_obj );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, 
             "tva_mem_alloc_object(): Out of objects, rc = (0x%08X)", rc );
      apr_memmgr_free( &tva_heapmgr, tva_obj );
      return APR_ENORESOURCE;
    }

    /* Use the custom object type. */
    objmgr_obj->any.ptr = tva_obj;

    /* Initialize the base MVS object header. */
    tva_obj->header.handle = objmgr_obj->handle;
    tva_obj->header.type = TVA_OBJECT_TYPE_ENUM_UNINITIALIZED;
  }

  *ret_object = tva_obj;

  return APR_EOK;
}

static uint32_t tva_mem_free_object (
  tva_object_t* object
)
{
  if ( object == NULL )
  {
    return APR_EBADPARAM;
  }

  /* Free the object memory and object handle. */
  ( void ) apr_objmgr_free_object( &tva_objmgr, object->header.handle );
  apr_memmgr_free( &tva_heapmgr, object );

  return APR_EOK;
}


static uint32_t tva_create_modem_subs_object ( 
  tva_modem_subs_object_t** ret_subs_obj )
{
  uint32_t rc = APR_EOK;
  tva_modem_subs_object_t* subs_obj = NULL;

  if ( ret_subs_obj == NULL )
  {
    return APR_EBADPARAM;
  }

  rc = tva_mem_alloc_object( sizeof( tva_modem_subs_object_t ),
                             ( ( tva_object_t** ) &subs_obj ) );
  if ( rc )
  {
    return APR_ENORESOURCE;
  }

  { /* Initialize the Object. */
    subs_obj->header.type = TVA_OBJECT_TYPE_ENUM_MODEM_SUBSCRIPTION;

    subs_obj->asid = SYS_MODEM_AS_ID_NONE;
    subs_obj->vsid = TVA_VSID_UNDEFINED_V;
    subs_obj->pending_vsid = TVA_VSID_UNDEFINED_V;

    subs_obj->tds_handle = APR_NULL_V;
    subs_obj->is_tds_ready = FALSE;
    subs_obj->session_obj = NULL;;
  }

  *ret_subs_obj = subs_obj;

  return APR_EOK;
}


static uint32_t tva_create_session_object ( 
  tva_session_object_t** ret_session_obj )
{
  uint32_t rc = APR_EOK;
  tva_session_object_t* session_obj = NULL;

  if ( ret_session_obj == NULL )
  {
    return APR_EBADPARAM;
  }

  rc = tva_mem_alloc_object( sizeof( tva_session_object_t ),
                             ( ( tva_object_t** ) &session_obj ) );
  if ( rc )
  {
    *ret_session_obj = NULL;
    return APR_ENORESOURCE;
  }

  { /* Initialize the simple job object. */
    session_obj->header.type = TVA_OBJECT_TYPE_ENUM_SESSION;

    session_obj->vsid = TVA_VSID_UNDEFINED_V;
    session_obj->active_subs_obj = NULL;
    rc = apr_lock_create( APR_LOCK_TYPE_MUTEX, &session_obj->data_lock );
    TVA_PANIC_ON_ERROR(rc);

    ( void ) voice_dsm_amr_q_init ( &session_obj->ul_queue, TVA_AMR_DSM_Q_LEN );
    ( void ) voice_dsm_amr_q_init ( &session_obj->dl_queue, TVA_AMR_DSM_Q_LEN );
    mmstd_memset ( &session_obj->ul_chan_state, 0, 
                   sizeof( session_obj->ul_chan_state ) );
    mmstd_memset ( &session_obj->dl_chan_state, 0, 
                   sizeof( session_obj->dl_chan_state ) );

    rc = apr_timer_create( &session_obj->sfg_timer, tva_sfg_timer_cb, session_obj );
    TVA_PANIC_ON_ERROR( rc );

    session_obj->va_tva_event_cb = NULL;
    session_obj->va_session_context =  NULL;
    session_obj->is_resource_granted = FALSE;

    session_obj->vocoder_type = TVA_VOCODER_ID_UNDEFINED_V;
    session_obj->codec_mode = TVA_CODEC_MODE_UNDEFINED;
    session_obj->dtx_mode = FALSE;

    session_obj->vs_handle = APR_NULL_V;
    session_obj->vs_read_buf = NULL;
    session_obj->primed_read_buf = NULL;
    session_obj->vs_write_buf = NULL;
    session_obj->is_vs_ready = FALSE;
  }

  *ret_session_obj = session_obj;

  return APR_EOK;
}

static uint32_t tva_create_simple_job_object (
  uint32_t parentjob_handle,
  tva_simple_job_object_t** ret_job_obj
)
{
  int32_t rc;
  tva_simple_job_object_t* tva_obj = NULL;

  if ( ret_job_obj == NULL )
  {
    return APR_EBADPARAM;
  }

  rc = tva_mem_alloc_object( sizeof( tva_simple_job_object_t ),
                             ( ( tva_object_t** ) &tva_obj ) );
  if ( rc )
  {
    *ret_job_obj = NULL;
    return APR_ENORESOURCE;
  }

  { /* Initialize the simple job object. */
    tva_obj->header.type = TVA_OBJECT_TYPE_ENUM_SIMPLE_JOB;
    tva_obj->context_handle = parentjob_handle;
    tva_obj->is_completed = 0;
  }

  *ret_job_obj = tva_obj;

  return APR_EOK;
}

TVA_INTERNAL int32_t tva_create_sequencer_job_object (
  tva_sequencer_job_object_t** ret_job_obj
)
{
  int32_t rc;
  tva_sequencer_job_object_t* job_obj = NULL;

  if ( ret_job_obj == NULL )
  {
    return APR_EBADPARAM;
  }

  rc = tva_mem_alloc_object( sizeof( tva_sequencer_job_object_t ),
                             ( ( tva_object_t** ) &job_obj ) );
  if ( rc )
  {
    *ret_job_obj = NULL;
    return APR_ENORESOURCE;
  }

  { /* Initialize the pending job object. */
    job_obj->header.type = TVA_OBJECT_TYPE_ENUM_SEQUENCER_JOB;

    job_obj->state = APR_NULL_V;
    job_obj->subjob_obj = NULL;
    job_obj->status = APR_UNDEFINED_ID_V;
  }

  *ret_job_obj = job_obj;

  return APR_EOK;
}


/****************************************************************************
 * TVA TDSCDMA <> VS MAPPING  ROUTINES                                          *
 ****************************************************************************/

static uint32_t tva_map_vocamr_codec_mode_tdscdma_to_vs( 
  uint32_t tdscdma_codec_mode,
  uint32_t* vs_codec_mode
)
{
  uint32_t rc = APR_EOK;

  switch ( tdscdma_codec_mode )
  {
   case TDSCDMA_IVOCAMR_CODEC_MODE_0475:
     *vs_codec_mode = VS_VOCAMR_CODEC_MODE_0475;
     break;

   case TDSCDMA_IVOCAMR_CODEC_MODE_0515:
     *vs_codec_mode = VS_VOCAMR_CODEC_MODE_0515;
     break;

   case TDSCDMA_IVOCAMR_CODEC_MODE_0590:
     *vs_codec_mode = VS_VOCAMR_CODEC_MODE_0590;
     break;

   case TDSCDMA_IVOCAMR_CODEC_MODE_0670:
     *vs_codec_mode = VS_VOCAMR_CODEC_MODE_0670;
     break;

   case TDSCDMA_IVOCAMR_CODEC_MODE_0740:
     *vs_codec_mode = VS_VOCAMR_CODEC_MODE_0740;
     break;

   case TDSCDMA_IVOCAMR_CODEC_MODE_0795:
     *vs_codec_mode = VS_VOCAMR_CODEC_MODE_0795;
     break;

   case TDSCDMA_IVOCAMR_CODEC_MODE_1020:
     *vs_codec_mode = VS_VOCAMR_CODEC_MODE_1020;
     break;

   case TDSCDMA_IVOCAMR_CODEC_MODE_1220:
     *vs_codec_mode = VS_VOCAMR_CODEC_MODE_1220;
     break;
  
   default:
     rc = APR_EBADPARAM;
     *vs_codec_mode = TVA_CODEC_MODE_UNDEFINED;
     break;
  }

  return rc;
}

static uint32_t tva_map_vocamrwb_codec_mode_tdscdma_to_vs( 
  uint32_t tdscdma_codec_mode,
  uint32_t* vs_codec_mode
)
{
  uint32_t rc = APR_EOK;

  switch ( tdscdma_codec_mode )
  {
   case TDSCDMA_IVOCAMRWB_CODEC_MODE_0660:
     *vs_codec_mode = VS_VOCAMRWB_CODEC_MODE_0660;
     break;
  
   case TDSCDMA_IVOCAMRWB_CODEC_MODE_0885:
     *vs_codec_mode = VS_VOCAMRWB_CODEC_MODE_0885;
     break;
  
   case TDSCDMA_IVOCAMRWB_CODEC_MODE_1265:
     *vs_codec_mode = VS_VOCAMRWB_CODEC_MODE_1265;
     break;
  
   case TDSCDMA_IVOCAMRWB_CODEC_MODE_1425:
     *vs_codec_mode = VS_VOCAMRWB_CODEC_MODE_1425;
     break;
    
   case TDSCDMA_IVOCAMRWB_CODEC_MODE_1585:
     *vs_codec_mode = VS_VOCAMRWB_CODEC_MODE_1585;
     break;
    
   case TDSCDMA_IVOCAMRWB_CODEC_MODE_1825:
     *vs_codec_mode = VS_VOCAMRWB_CODEC_MODE_1825;
     break;
    
   case TDSCDMA_IVOCAMRWB_CODEC_MODE_1985:
     *vs_codec_mode = VS_VOCAMRWB_CODEC_MODE_1985;
     break;
   
   case TDSCDMA_IVOCAMRWB_CODEC_MODE_2305:
     *vs_codec_mode = VS_VOCAMRWB_CODEC_MODE_2305;
     break;
  
   case TDSCDMA_IVOCAMRWB_CODEC_MODE_2385:
     *vs_codec_mode = VS_VOCAMRWB_CODEC_MODE_2385;
     break;
  
   default:
     rc = APR_EBADPARAM;
     *vs_codec_mode = TVA_CODEC_MODE_UNDEFINED;
     break;
  }

  return rc;
}

static uint32_t tva_map_vocoder_type_tdscdma_to_vs(
  uint32_t tdscdma_vocoder_type
)
{
  uint32_t vs_media_id = 0xFFFFFFFF;

  switch ( tdscdma_vocoder_type )
  {
   case TDSCDMA_IVOCODER_ID_AMR:
     vs_media_id = VS_COMMON_MEDIA_ID_AMR;
     break;

   case TDSCDMA_IVOCODER_ID_AMRWB:
     vs_media_id = VS_COMMON_MEDIA_ID_AMRWB;
     break;
  }

  return vs_media_id;
}


static uint32_t tva_update_chan_state (
  voice_amr_chan_state_t* chan_state,
  uint32_t chan_class
)
{
  uint32_t rc = APR_EOK;

  switch ( chan_class )
  {
   case TDSCDMA_ICOMMON_CHAN_CLASS_TYPE_ABC:
     chan_state->has_chan_c = TRUE;
     /* fall through */

   case TDSCDMA_ICOMMON_CHAN_CLASS_TYPE_AB:
     chan_state->has_chan_b = TRUE;
     /* fall through */

   case TDSCDMA_ICOMMON_CHAN_CLASS_TYPE_A:
     chan_state->has_chan_a = TRUE;
     break;

   case TDSCDMA_ICOMMON_CHAN_CLASS_TYPE_NONE:
    {
      chan_state->has_chan_a = FALSE;
      chan_state->has_chan_b = FALSE;
      chan_state->has_chan_c = FALSE;
    }
    break;

   default:
     rc = APR_EBADPARAM;
     break;
  }

  if ( rc )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "tva_update_chan_state(): Invalid channel class=(0x%08x)", 
           chan_class );
  }

  return rc;
}

static void tva_downlink_channel_data_available (
  uint8_t lc_id,
  uint8_t n_unit,
  void* session_context 
)
{
  uint32_t rc = APR_EOK;
  tva_session_object_t* session_obj = NULL;
  session_obj = ( tva_session_object_t* ) session_context;

  for ( ;; )
  {
    if( lc_id == session_obj->dl_chan_state.lcc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "tva_downlink_channel_data_available(): logical channel C data "
             "available lc_id =(%d)", lc_id );
      break;
    }

    if( lc_id == session_obj->dl_chan_state.lcb )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "tva_downlink_channel_data_available(): logical channel B data "
             "available lc_id =(%d)", lc_id );
      break;
    }

    if( lc_id == session_obj->dl_chan_state.lca )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "tva_downlink_channel_data_available(): logical channel A data "
             "available lc_id =(%d)", lc_id );
      /* Post an internal event for DL processing. */
      rc = tva_prepare_and_dispatch_event_packet ( 
             session_context, TVA_INTERNAL_EVENT_DL_BUFFER_AVAILABLE, NULL, 0 );
      break;
    }

    break;
  }

  return;
}

/****************************************************************************
 * TVA COMMON ROUTINES                                                      *
 ****************************************************************************/

static uint32_t tva_set_voc_codec_mode (
  tva_session_object_t* session_obj
)
{

  uint32_t rc = APR_EOK;
  vs_vocamr_cmd_set_codec_mode_t vocamr_codec_cmd;
  vs_vocamrwb_cmd_set_codec_mode_t vocamrwb_codec_cmd;
  uint32_t codec_mode;

  switch ( session_obj->vocoder_type )
  {
   case TDSCDMA_IVOCODER_ID_AMR:
     {
       rc = tva_map_vocamr_codec_mode_tdscdma_to_vs( session_obj->codec_mode,
                                                     &codec_mode );
       if ( ( rc ) || ( session_obj->vs_handle == APR_NULL_V ) )
       {
         MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
                "TVA: Unsupported codec_mode = (0x%08x) for AMR",
                session_obj->codec_mode );
       }
       else
       {
         MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,
                "TVA: AMR codec_mode=(%d) sent to VS", codec_mode );
         vocamr_codec_cmd.handle = session_obj->vs_handle;
         vocamr_codec_cmd.codec_mode  = (vs_vocamr_codec_mode_t) codec_mode;
         rc  = vs_call( VS_VOCAMR_CMD_SET_CODEC_MODE, &vocamr_codec_cmd,
                        sizeof( vocamr_codec_cmd ) );
       }
     }
     break;

   case TDSCDMA_IVOCODER_ID_AMRWB:
     {
       rc = tva_map_vocamrwb_codec_mode_tdscdma_to_vs ( session_obj->codec_mode,
                                                        &codec_mode );
       if ( ( rc ) || ( session_obj->vs_handle == APR_NULL_V ) )
       {
         MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
                "TVA: Unsupported codec_mode = (0x%08x) for AMR-WB",
                session_obj->codec_mode );
       }
       else
       {
         MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,
                "TVA: AMR-WB codec_mode=(%d) sent to VS", codec_mode );
         vocamrwb_codec_cmd.handle = session_obj->vs_handle;
         vocamrwb_codec_cmd.codec_mode  = (vs_vocamrwb_codec_mode_t) codec_mode;
         rc  = vs_call( VS_VOCAMRWB_CMD_SET_CODEC_MODE, &vocamrwb_codec_cmd,
                        sizeof( vocamrwb_codec_cmd ) );
       }
     }
     break;

   default:
     rc = APR_EUNSUPPORTED;
     MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
            "tva_set_voc_codec_mode(): Unsupported vocoder=(0x%08x), ",
            session_obj->vocoder_type );
     break;
  }

  if ( rc != APR_EOK )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "TVA: failed to set codec_mode=(0x%08x), vocoder=(0x%08x)",
           session_obj->codec_mode, session_obj->vocoder_type );
  }
  return rc;

}


static uint32_t tva_set_voc_dtx_mode (
  tva_session_object_t* session_obj
)
{

  uint32_t rc = APR_EOK;
  vs_vocamr_cmd_set_dtx_mode_t vocamr_dtx_cmd;
  vs_vocamrwb_cmd_set_dtx_mode_t vocamrwb_dtx_cmd;

  switch ( session_obj->vocoder_type )
  {
   case TDSCDMA_IVOCODER_ID_AMR:
     {
       vocamr_dtx_cmd.handle = session_obj->vs_handle;
       vocamr_dtx_cmd.enable_flag  = session_obj->dtx_mode;
       MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
              "TVA: AMR dtx_mode=(%d) sent to VS", session_obj->dtx_mode );
       rc  = vs_call( VS_VOCAMR_CMD_SET_DTX_MODE, &vocamr_dtx_cmd,
                      sizeof( vocamr_dtx_cmd ) );
     }
     break;

   case TDSCDMA_IVOCODER_ID_AMRWB:
     {
       vocamrwb_dtx_cmd.handle = session_obj->vs_handle;
       vocamrwb_dtx_cmd.enable_flag  = session_obj->dtx_mode;
       MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
              "TVA: AMR-WB dtx_mode=(%d) sent to VS", session_obj->dtx_mode );
       rc  = vs_call( VS_VOCAMRWB_CMD_SET_DTX_MODE, &vocamrwb_dtx_cmd,
                      sizeof( vocamrwb_dtx_cmd ) );
     }
     break;

   default:
     MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
            "tva_set_voc_dtx_mode(): Unsupported vocoder=(0x%08x), ",
            session_obj->vocoder_type );
     break;
  }

  if ( rc != APR_EOK )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "TVA: failed to set dtx_mode=(%d), vocoder=(0x%08x)",
           session_obj->dtx_mode, session_obj->vocoder_type );
  }
  return rc;

}


static uint32_t tva_get_dl_vocoder_packet ( 
  tva_session_object_t* session_obj
)
{
  uint32_t rc = APR_EOK;
  vs_voc_buffer_t* vs_buffer = NULL;

  vs_buffer = session_obj->vs_write_buf;

  vs_buffer->media_id = tva_map_vocoder_type_tdscdma_to_vs( session_obj->vocoder_type );

  rc = voice_amr_dl_processing( vs_buffer, &session_obj->dl_queue );
  if ( rc ) return rc;

  vs_buffer->flags = TRUE;

  return rc;
}


static uint32_t tva_deliver_ul_vocoder_packet ( 
  tva_session_object_t* session_obj
)
{
  uint32_t rc = APR_EOK;
  vs_voc_buffer_t* vs_buffer = NULL;

  vs_buffer = session_obj->vs_read_buf;

  rc = voice_amr_ul_processing( vs_buffer, &session_obj->ul_chan_state,
                                &session_obj->ul_queue );

  return rc;
}


/****************************************************************************
 * TVA VS SESSION ROUTINES                                                  *
 ****************************************************************************/

static void tva_log_event_info(
  void* session_context,
  uint32_t event_id
)
{
  tva_modem_subs_object_t* subs_obj = ( tva_modem_subs_object_t* ) session_context;
  tva_session_object_t* session_obj = ( tva_session_object_t* ) session_context;

  if ( session_context == NULL ) return;

  switch( event_id )
  {
    case VS_COMMON_EVENT_CMD_RESPONSE:
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "TVA: VSID=(0x%08x): VS_COMMON_EVENT_CMD_RESPONSE recieved",
             session_obj->vsid );
    }
    break;

    case VS_COMMON_EVENT_NOT_READY:
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "TVA: VSID=(0x%08x): VS_COMMON_EVENT_NOT_READY recieved",
             session_obj->vsid );
    }
    break;

    case VS_COMMON_EVENT_READY:
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "TVA: VSID=(0x%08x): VS_COMMON_EVENT_READY recieved",
             session_obj->vsid );
    }
    break;

    case VS_VOC_EVENT_READ_AVAILABLE:
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,
             "TVA: VSID=(0x%08x): VS_VOC_EVENT_READ_AVAILABLE recieved",
             session_obj->vsid );
    }
    break;

    case VS_VOC_EVENT_WRITE_BUFFER_RETURNED:
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,
             "TVA: VSID=(0x%08x): VS_VOC_EVENT_WRITE_BUFFER_RETURNED recieved",
             session_obj->vsid );
    }
    break;

    case VS_COMMOM_EVENT_EAMR_MODE_CHANGE:
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "TVA: VSID=(0x%08x): VS_COMMOM_EVENT_EAMR_MODE_CHANGE recieved",
             session_obj->vsid );
    }
    break;

    case TVA_INTERNAL_EVENT_DL_BUFFER_AVAILABLE:
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,
             "TVA: VSID=(0x%08x): TVA_INTERNAL_EVENT_DL_BUFFER_AVAILABLE recieved",
             session_obj->vsid );
    }
    break;

    case TVA_INTERNAL_EVENT_SEND_SILENCE_FRAME:
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,
             "TVA: VSID=(0x%08x): TVA_INTERNAL_EVENT_SEND_SILENCE_FRAME recieved",
             session_obj->vsid );
    }
    break;

    case TDSCDMA_IVOICE_EVENT_SET_LOGICAL_CHANNELS:
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "TVA: ASID=(%d): VSID=(0x%08x): TDSCDMA_IVOICE_EVENT_SET_LOGICAL_CHANNELS recieved",
             subs_obj->asid, subs_obj->vsid );
    }
    break;

    case TDSCDMA_IVOICE_EVENT_REQUEST_START:
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "TVA: ASID=(%d): VSID=(0x%08x): TDSCDMA_IVOICE_EVENT_REQUEST_START recieved",
             subs_obj->asid, subs_obj->vsid );
    }
    break;

    case TDSCDMA_IVOICE_EVENT_REQUEST_STOP:
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "TVA: ASID=(%d): VSID=(0x%08x): TDSCDMA_IVOICE_EVENT_REQUEST_STOP recieved",
             subs_obj->asid, subs_obj->vsid );
    }
    break;

    case TDSCDMA_IVOICE_EVENT_SELECT_OWNER:
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "TVA: ASID=(%d): VSID=(0x%08x): TDSCDMA_IVOICE_EVENT_SELECT_OWNER recieved",
             subs_obj->asid, subs_obj->vsid );
    }
    break;

    case TDSCDMA_IVOICE_EVENT_REQUEST_CODEC_MODE:
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,
             "TVA: ASID=(%d): VSID=(0x%08x): TDSCDMA_IVOICE_EVENT_REQUEST_CODEC_MODE recieved",
             subs_obj->asid, subs_obj->vsid );
    }
    break;

    case TDSCDMA_IVOICE_EVENT_REQUEST_SCR_MODE:
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,
             "TVA: ASID=(%d): VSID=(0x%08x): TDSCDMA_IVOICE_EVENT_REQUEST_SCR_MODE recieved",
             subs_obj->asid, subs_obj->vsid );
    }
    break;

    case TDSCDMA_IVOICEL2_EVENT_VFR_NOTIFICATION:
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_LOW,
             "TVA: ASID=(%d): VSID=(0x%08x): TDSCDMA_IVOICEL2_EVENT_VFR_NOTIFICATION recieved",
             subs_obj->asid, subs_obj->vsid );
    }
    break;

   default:
     break;
  }

  return;
}


static uint32_t tva_vs_event_cb(
 uint32_t event_id,
 void* params,
 uint32_t size,
 void* session_context
)
{
  uint32_t rc = APR_EOK;

  if ( tva_is_initialized == FALSE ) return APR_EOK;

  switch ( event_id )
  {
   case VS_COMMON_EVENT_CMD_RESPONSE:
   case VS_COMMON_EVENT_NOT_READY:
   case VS_COMMON_EVENT_READY:
   case VS_VOC_EVENT_READ_AVAILABLE:
   case VS_VOC_EVENT_WRITE_BUFFER_RETURNED:
   case VS_COMMOM_EVENT_EAMR_MODE_CHANGE:
    {
      (void) tva_log_event_info( ( tva_session_object_t* ) session_context,
                                  event_id );
      rc = tva_prepare_and_dispatch_event_packet( session_context, event_id,
                                                  params, size );
    }
    break;

   default:
     MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, 
            "tva_vs_event_cb(): Unsupported event (%d)", event_id );
     rc = APR_EFAILED;
  }


  return rc;
}


static uint32_t tva_vs_prime_read_buffer (
  tva_session_object_t* session_obj
)
{
  uint32_t rc = APR_EOK;
  vs_voc_cmd_prime_read_buffer_t prime_read_buf_cmd;

  prime_read_buf_cmd.handle = session_obj->vs_handle;
  prime_read_buf_cmd.buffer = session_obj->vs_read_buf;

  rc = vs_call( VS_VOC_CMD_PRIME_READ_BUFFER, ( void* ) &prime_read_buf_cmd,
                sizeof( prime_read_buf_cmd ) );
  if ( rc )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "tva_vs_prime_read_buffer(): Failed to prime vs_read_buf, "
           "vs_read_buf=(0x%08x), rc=(0x%08x)", session_obj->vs_read_buf, rc );
  }
  else
  {
    session_obj->vs_read_buf = NULL;
  }

  return APR_EOK;
}


static uint32_t tva_vs_read_buffer (
  tva_session_object_t* session_obj
)
{
  uint32_t rc = APR_EOK;
  vs_voc_cmd_read_buffer_t read_buf_cmd;

  read_buf_cmd.handle = session_obj->vs_handle;
  read_buf_cmd.ret_buffer = &session_obj->vs_read_buf;

  rc = vs_call( VS_VOC_CMD_READ_BUFFER, ( void* )&read_buf_cmd,
                sizeof( read_buf_cmd ) );
  if ( rc )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "tva_vs_read_buffer(): Failed to read vs_buffer, "
           "vs_read_buf=(0x%08x), rc=(0x%08x)", session_obj->vs_read_buf, rc );
  }
  else
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,
           "tva_vs_read_buffer(): read buffer available vs_read_buf=(0x%08x) ",
           session_obj->vs_read_buf );
  }

  return rc;
}


static uint32_t tva_vs_write_buffer (
  tva_session_object_t* session_obj
)
{
  uint32_t rc = APR_EOK;
  vs_voc_cmd_write_buffer_t write_buf_cmd;

  write_buf_cmd.handle = session_obj->vs_handle;
  write_buf_cmd.buffer = session_obj->vs_write_buf;

  rc = vs_call( VS_VOC_CMD_WRITE_BUFFER, ( void* )&write_buf_cmd,
                sizeof( write_buf_cmd ) );
  if ( rc )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "tva_vs_write_buffer(): Failed to pass vs_write_buf  = (0x%08x), "
           "rc = (0x%08x)", session_obj->vs_write_buf, rc );
  }
  else
  {
    session_obj->vs_write_buf = NULL;
  }

  return APR_EOK;
}

static uint32_t tva_vs_free_buffer (
  tva_session_object_t* session_obj
)
{
  uint32_t rc = APR_EOK;
  vs_voc_cmd_free_buffer_t free_buf_cmd;

  if ( session_obj->vs_read_buf != NULL )
  {
    free_buf_cmd.handle = session_obj->vs_handle;
    free_buf_cmd.buffer = session_obj->vs_read_buf;

    rc = vs_call( VS_VOC_CMD_FREE_BUFFER, ( void* )&free_buf_cmd,
                  sizeof( free_buf_cmd ) );
    if ( rc )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "tva_vs_free_buffer(): Failed to free vs_read_buf = (0x%08x), " 
             "rc = (0x%08x)", session_obj->vs_read_buf, rc );
    }
    session_obj->vs_read_buf = NULL;
    session_obj->primed_read_buf = NULL;
  }

  if ( session_obj->vs_write_buf != NULL )
  {
    free_buf_cmd.handle = session_obj->vs_handle;
    free_buf_cmd.buffer = session_obj->vs_write_buf;

    rc = vs_call( VS_VOC_CMD_FREE_BUFFER, ( void* )&free_buf_cmd,
                  sizeof( free_buf_cmd ) );
    if ( rc )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "tva_vs_free_buffer(): Failed to free vs_write_buf = (0x%08x), " 
             "rc = (0x%08x)", session_obj->vs_write_buf, rc );
    }
    session_obj->vs_write_buf = NULL;
  }

  return rc;
}

static uint32_t tva_vs_alloc_buffer (
  tva_session_object_t* session_obj
)
{
  uint32_t rc;
  vs_voc_cmd_alloc_buffer_t alloc_buf_cmd;

  for ( ;; )
  {
    /* Allocate read buffer. */
    session_obj->vs_read_buf = NULL;
    alloc_buf_cmd.handle = session_obj->vs_handle;
    alloc_buf_cmd.ret_buffer = &session_obj->vs_read_buf;
    alloc_buf_cmd.req_max_frame_size = TVA_MAX_VOC_FRAME_LENGTH;//322; 
    rc = vs_call( VS_VOC_CMD_ALLOC_BUFFER, ( void* )&alloc_buf_cmd,
                  sizeof( alloc_buf_cmd ) );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "tva_vs_alloc_buffer(): Failed to allocate read buffer, "
             "rc = (0x%08x)", rc );
      break;
    }
    session_obj->primed_read_buf = session_obj->vs_read_buf;

    /* Allocate write buffer. */
    session_obj->vs_write_buf = NULL;
    alloc_buf_cmd.handle = session_obj->vs_handle;
    alloc_buf_cmd.ret_buffer = &session_obj->vs_write_buf;
    alloc_buf_cmd.req_max_frame_size = TVA_MAX_VOC_FRAME_LENGTH;//322; 
    rc = vs_call( VS_VOC_CMD_ALLOC_BUFFER, ( void* )&alloc_buf_cmd,
                  sizeof( alloc_buf_cmd ) );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "tva_vs_alloc_buffer(): Failed to allocate write buffer, "
             "rc = (0x%08x)", rc );
      ( void ) tva_vs_free_buffer ( session_obj );
      break;
    }

    break;
  }

  return rc;
}

static uint32_t tva_vs_close_session (
  tva_session_object_t* session_obj,
  void* client_context
)
{
  uint32_t rc = APR_EOK;
  vs_voc_cmd_close_t close_cmd;
  
  for ( ;; )
  {
    if ( session_obj->vs_handle == APR_NULL_V )
    {
      rc = APR_EOK;
      break;
    }
  
    /* Free read and write buffers. */
    ( void ) tva_vs_free_buffer ( session_obj );
  
    close_cmd.handle = session_obj->vs_handle;
    close_cmd.client_context = client_context;
  
    rc = vs_call( VS_VOC_CMD_CLOSE, (void*)&close_cmd, sizeof( close_cmd ) );
    if ( rc )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "tva_vs_close_session(): Failed to close VS session, " 
             "handle = (0x%08x), rc = (0x%08x)", session_obj->vs_handle, rc );
    }
    else
    {
      session_obj->vs_handle = APR_NULL_V;
      rc = APR_EPENDING;
    }
  
    break;
  }
 
  return rc;
}


static uint32_t tva_vs_open_session (
 tva_session_object_t* session_obj
)
{
  uint32_t rc;
  vs_voc_cmd_open_t open_cmd;

  for ( ;; )
  {
    /* Open VS session. */
    session_obj->is_vs_ready = FALSE;

    open_cmd.ret_handle = &session_obj->vs_handle;
    open_cmd.vsid = session_obj->vsid;
    open_cmd.client_id = VS_VOC_CLIENT_TDSCDMA;
    open_cmd.session_context = ( void* )session_obj;
    open_cmd.event_cb = tva_vs_event_cb;

    rc = vs_call( VS_VOC_CMD_OPEN, (void*)&open_cmd, sizeof( open_cmd ) );
    if ( rc )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "tva_vs_open_session(): failed to open VS session, "
             "client = (0x%08x), rc = (0x%08x)", open_cmd.client_id, rc );
      break;
    }

    rc = tva_vs_alloc_buffer( session_obj );
    if ( rc )
    {
      ( void ) tva_vs_close_session ( session_obj, NULL );
      
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "tva_vs_open_session(): failed to alloc VS buffers, "
             "client = (0x%08x), rc = (0x%08x)", open_cmd.client_id, rc );
      break;
    }

    break;
  }

  return rc;
}


static uint32_t tva_vs_flush_buffers (
  tva_session_object_t* session_obj,
  void* client_context
)
{
  uint32_t rc = APR_EOK;
  vs_voc_cmd_flush_buffers_t vs_flush_cmd;

  for ( ;; )
  {
    if ( session_obj->vs_handle == APR_NULL_V )
    {
      rc = APR_EOK;
      break;
    }
  
    vs_flush_cmd.handle = session_obj->vs_handle;
    vs_flush_cmd.client_context = client_context;
    rc = vs_call( VS_VOC_CMD_FLUSH_BUFFERS, &vs_flush_cmd,
                  sizeof( vs_flush_cmd ) );
    if ( rc )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "tva_vs_flush_buffers(): Failed to flush VS buffers, " 
             "handle = (0x%08x), rc = (0x%08x)", session_obj->vs_handle, rc );
    }
    else
    {
      rc = APR_EPENDING;
    }
  
    break;
  } /* For loop ends here. */
  
  return rc;
}


static uint32_t tva_vs_disable_vocoder (
  tva_session_object_t* session_obj,
  void* client_context
)
{
  uint32_t rc = APR_EOK;
  vs_voc_cmd_disable_t vs_disable_cmd;

  for ( ;; )
  {
    if ( session_obj->vs_handle == APR_NULL_V )
    {
      rc = APR_EOK;
      break;
    }

    vs_disable_cmd.handle = session_obj->vs_handle;
    vs_disable_cmd.client_context = client_context;
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
           "TVA: VS_VOC_CMD_DISABLE(): vocoder=(0x%08x)",
           tva_map_vocoder_type_tdscdma_to_vs( session_obj->vocoder_type ) );
    rc = vs_call( VS_VOC_CMD_DISABLE, &vs_disable_cmd,
                  sizeof( vs_disable_cmd ) );
    if ( rc )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "tva_vs_disable_vocoder(): Failed to disable VS session, " 
             "handle = (0x%08x), rc = (0x%08x)", session_obj->vs_handle, rc );
    }
    else
    {
      rc = APR_EPENDING;
    }
   
    break;
  }/* For loop ends here. */

  return rc;
}


static uint32_t tva_vs_enable_vocoder (
  tva_session_object_t* session_obj,
  void* client_context
)
{
  uint32_t rc = APR_EOK;
  uint32_t media_id;
  vs_voc_cmd_enable_t vs_enable_cmd;

  for ( ;; )
  {
    if ( session_obj->vs_handle == APR_NULL_V )
    {
      rc = APR_EOK;
      break;
    }

    media_id = tva_map_vocoder_type_tdscdma_to_vs ( session_obj->vocoder_type );
    vs_enable_cmd.handle = session_obj->vs_handle;
    vs_enable_cmd.media_id = media_id;
    vs_enable_cmd.client_context = ( void* ) client_context;
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
           "TVA: VS_VOC_CMD_ENABLE(): vocoder=(0x%08x)", vs_enable_cmd.media_id );
    rc = vs_call( VS_VOC_CMD_ENABLE, &vs_enable_cmd,
                  sizeof( vs_enable_cmd ) );
    if ( rc )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "tva_vs_enable_vocoder(): Failed to enable VS session, " 
             "handle = (0x%08x), rc = (0x%08x)", session_obj->vs_handle, rc );
    }
    else
    {
      rc = APR_EPENDING;
    }

    break;
  }/* For loop ends here. */

  return rc;
}


/****************************************************************************
 * TVA TDSCDMA SESSION ROUTINES                                                  *
 ****************************************************************************/

static uint32_t tva_tdscdma_event_cb(
 sys_modem_as_id_e_type asid,
 uint32_t event_id,
 void* params,
 uint32_t size
)
{
  uint32_t rc = APR_EOK;
  void* session_context = NULL;

  if ( ( asid < SYS_MODEM_AS_ID_1 ) || ( asid > SYS_MODEM_AS_ID_2 ) )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "tva_tdscdma_event_cb(): ASID=(%d) not supported", asid );
    return APR_EBADPARAM;
  }

  session_context = ( void* ) tva_subs_obj_list[ asid ];

  switch ( event_id )
  {
   case TDSCDMA_IVOICE_EVENT_REQUEST_START:
   case TDSCDMA_IVOICE_EVENT_REQUEST_STOP:
   case TDSCDMA_IVOICE_EVENT_SELECT_OWNER:
   case TDSCDMA_IVOICE_EVENT_SET_LOGICAL_CHANNELS:
   case TDSCDMA_IVOICE_EVENT_REQUEST_CODEC_MODE:
   case TDSCDMA_IVOICE_EVENT_REQUEST_SCR_MODE:
   case TDSCDMA_IVOICEL2_EVENT_VFR_NOTIFICATION:
    {
      ( void ) tva_log_event_info( session_context, event_id );
      rc = tva_prepare_and_dispatch_event_packet( session_context, event_id,
                                                  params, size );
    }
    break;

   default:
     MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, 
            "tva_tdscdma_event_cb(): Unsupported event (%d)", event_id );
     rc = APR_EFAILED;
  }

  return rc;
}

static uint32_t tva_tdscdma_open_session (
 tva_modem_subs_object_t* subs_obj
)
{
  uint32_t rc = APR_EOK;
  tdscdma_ivoice_cmd_open_t open_cmd;

  if( NULL == subs_obj )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
         "tva_tdscdma_open_session(): subs_obj is NULL" );
    return APR_EBADPARAM;
  }

  open_cmd.ret_handle = &subs_obj->tds_handle;
  open_cmd.as_id = subs_obj->asid;
  open_cmd.event_cb = tva_tdscdma_event_cb;

#ifndef WINSIM
#ifdef FEATURE_SEGMENT_LOADING
  tva_ptr_Td = get_tdscdma_interface();
  if ( tva_ptr_Td != NULL )
  {
    tds_ext_audio_api( TDSCDMA_IVOICE_CMD_OPEN, &open_cmd, sizeof( open_cmd ) );
  }
  else
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
         "tva_tdscdma_open_session(): TDSCDMA SEGMENT NOT LOADED!!");
  }
#else
  rc = tds_ext_audio_api( TDSCDMA_IVOICE_CMD_OPEN, &open_cmd, sizeof( open_cmd ) );
#endif

  if ( APR_EUNSUPPORTED == rc )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "tva_tdscdma_open_session(): TDSCDMA is UNSUPPORTED asid = (0x%08x), "
           "rc = (0x%08x)", open_cmd.as_id, rc );
    rc = APR_EOK;
  }
  else if ( APR_EOK != rc)
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "tva_tdscdma_open_session(): Failed to open TDSCDMA session, "
           "asid = (0x%08x), rc = (0x%08x)", open_cmd.as_id, rc );
  }
  else
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
           "tva_tdscdma_open_session(): TDSCDMA session successfully opened "
           " asid = (0x%08x), rc = (0x%08x)", open_cmd.as_id, rc );
  }
#endif

  return rc;
}

static uint32_t tva_tdscdma_close_session (
  tva_modem_subs_object_t* subs_obj
)
{
  uint32_t rc = APR_EOK;
  tdscdma_ivoice_cmd_close_t close_cmd;

  if( NULL == subs_obj )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
         "tva_tdscdma_close_session(): subs_obj is NULL" );
    return APR_EBADPARAM;
  }

  close_cmd.handle = subs_obj->tds_handle;

#ifndef WINSIM

#ifdef FEATURE_SEGMENT_LOADING
  tva_ptr_Td = get_tdscdma_interface();
  if ( tva_ptr_Td != NULL )
  {
    rc = tds_ext_audio_api( TDSCDMA_IVOICE_CMD_CLOSE,
                            &close_cmd, sizeof( close_cmd ) );
  }
  else
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
         "tva_tdscdma_close_session(): TDSCDMA SEGMENT NOT LOADED!!");
  }
#else
  rc = tds_ext_audio_api( TDSCDMA_IVOICE_CMD_CLOSE,
                          &close_cmd, sizeof( close_cmd ) );
#endif

  if ( rc )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "tva_tdscdma_close_session(): failed to close TDSCDMA session, "
           "asid = (0x%08x), rc = (0x%08x)", subs_obj->asid, rc );
  }
#endif

  return rc;
}

static uint32_t tva_tdscdma_start_session (
  tva_modem_subs_object_t* subs_obj
)
{
  uint32_t rc = APR_EOK;
  tdscdma_ivoice_cmd_start_t start_cmd;

  if( NULL == subs_obj )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
         "tva_tdscdma_start_session(): subs_obj is NULL" );
    return APR_EBADPARAM;
  }

  start_cmd.handle = subs_obj->tds_handle;

#ifndef WINSIM
  rc = tds_ext_audio_api ( TDSCDMA_IVOICE_CMD_START, &start_cmd,
                           sizeof( start_cmd ) );
  if ( rc )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "tva_tdscdma_start_session(): failed to close TDSCDMA session, "
           "asid = (0x%08x), rc = (0x%08x)", subs_obj->asid, rc );
  }
#endif

  return rc;
}

static uint32_t tva_tdscdma_stop_session (
  tva_modem_subs_object_t* subs_obj
)
{
  uint32_t rc = APR_EOK;
  tdscdma_ivoice_cmd_stop_t stop_cmd;

  if( NULL == subs_obj )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
         "tva_tdscdma_stop_session(): subs_obj is NULL" );
    return APR_EBADPARAM;
  }

  stop_cmd.handle = subs_obj->tds_handle;

#ifndef WINSIM
  rc = tds_ext_audio_api ( TDSCDMA_IVOICE_CMD_STOP, &stop_cmd,
                           sizeof( stop_cmd ) );
  if ( rc )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "tva_tdscdma_stop_session(): failed to close TDSCDMA session, "
           "asid = (0x%08x), rc = (0x%08x)", subs_obj->asid, rc );
  }
#endif

  return rc;
}

static uint32_t tva_tdscdma_set_vfr_notification (
  tva_modem_subs_object_t* subs_obj
)
{
  uint32_t rc = APR_EOK;
  tdscdma_ivoicel2_cmd_set_vfr_notification_t set_vfr_cmd;

  if( NULL == subs_obj )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
         "tva_tdscdma_set_vfr_notification(): subs_obj is NULL" );
    return APR_EBADPARAM;
  }

  set_vfr_cmd.as_id = subs_obj->asid;
  set_vfr_cmd.enable_flag = subs_obj->is_tds_ready;

#ifndef WINSIM
  rc = tds_ext_audio_api( TDSCDMA_IVOICEL2_CMD_SET_VFR_NOTIFICATION,
                          &set_vfr_cmd,  sizeof( set_vfr_cmd ) );
  if ( rc )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "tva_tdscdma_stop_session(): failed to close TDSCDMA session, "
           "asid = (0x%08x), rc = (0x%08x)", subs_obj->asid, rc );
  }
#endif

  return rc;
}



static uint32_t tva_tdscdma_deregister_ul_logical_channels (
  tdscdma_ivoice_event_set_logical_channels_t* chan_info,
  tva_modem_subs_object_t* subs_obj,
  tva_session_object_t* session_obj
)
{
  uint32_t rc = APR_EOK;
  uint8_t nchan = 0;
  voice_amr_chan_state_t* chan_state;
  voice_amr_dsm_queue_t* ul_queue = NULL;
  tdsl2_ul_service_register_type ul_service;
  tdscdma_ivoicel2_cmd_register_ul_service_t ul_srv_cmd;

  for( ;; )
  {
    chan_state = &session_obj->ul_chan_state;
    ul_queue = &session_obj->ul_queue;

#ifndef WINSIM
    /**
     * De-register logical channels if necessary.
     */
    ul_service.service = FALSE;

    /* deregister class A channel if necessary */
    if ( ( chan_state->has_chan_a == FALSE ) || 
         ( chan_state->lca != chan_info->lc_class_a ) )
    {
      if ( chan_state->lca != 0 )
      {
        ul_service.rlc_id[nchan] = chan_state->lca;
        nchan++;
        chan_state->lca = 0;
      }
    }

    /* deregister class B channel if necessary */
    if ( ( chan_state->has_chan_b == FALSE ) || 
         ( chan_state->lcb != chan_info->lc_class_b ) )
    {
      if ( chan_state->lcb != 0 )
      {
        ul_service.rlc_id[nchan] = chan_state->lcb;
        nchan++;
        chan_state->lcb = 0;
      }
    }

    /* deregister class C channel if necessary */
    if ( ( chan_state->has_chan_c == FALSE ) || 
         ( chan_state->lcc != chan_info->lc_class_c ) )
    {
      if ( chan_state->lcc != 0 )
      {
        ul_service.rlc_id[nchan] = chan_state->lcc;
        nchan++;
        chan_state->lcc = 0;
      }
    }

    if( nchan > 0 )
    {
      ( void ) voice_dsm_amr_q_empty( &session_obj->ul_queue );
       MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
            "DSM UL queue empty during de-registration" );
    }

    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH, 
           "tva_tdscdma_deregister_ul_logical_channels(): AFTER: lca=(%d), lcb=(%d), "
           "lcc=(%d)", chan_state->lca, chan_state->lcb, chan_state->lcc );

    ul_service.nchan = nchan;
    ul_srv_cmd.as_id = subs_obj->asid;
    ul_srv_cmd.service = &ul_service;

    rc  = tds_ext_audio_api( TDSCDMA_IVOICEL2_CMD_REGISTER_UL_SERVICE,
                             &ul_srv_cmd, sizeof( ul_srv_cmd ) );
    if( rc == TDSCDMA_EOK )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "TDSCDMA L2 UL De-registration successful. Number of TDSCDMA L2 "
             "uplink logical channels, nchan = (%d).", nchan );
      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH, 
             "tva_tdscdma_deregister_ul_logical_channels(): AFTER: lca=(%d), lcb=(%d), "
             "lcc=(%d)", chan_state->lca, chan_state->lcb, chan_state->lcc );
    }
    else
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, 
             "TDSCDMA L2 UL De-registration failed, rc = (0x%08X). Number of "
             "TDSCDMA L2 uplink logical channels, nchan = (%d).", rc, nchan );
      break;
    }

#endif

   break;
  }

  return rc;
}

static uint32_t tva_tdscdma_register_ul_logical_channels (
  tdscdma_ivoice_event_set_logical_channels_t* chan_info,
  tva_modem_subs_object_t* subs_obj,
  tva_session_object_t* session_obj
)
{
  uint32_t rc = APR_EOK;
  uint8_t nchan = 0;
  voice_amr_chan_state_t* chan_state;
  voice_amr_dsm_queue_t* ul_queue = NULL;
  tdsl2_ul_service_register_type ul_service;
  tdscdma_ivoicel2_cmd_register_ul_service_t ul_srv_cmd;

  for( ;; )
  {
    chan_state = &session_obj->ul_chan_state;
    ul_queue = &session_obj->ul_queue;
  
#ifndef WINSIM
    /**
     * De-register logical channels if necessary.
     */
    ul_service.service = TRUE;

    /* Register class A channel if necessary */
    if ( ( chan_state->has_chan_a == TRUE ) && ( chan_info->lc_class_a > 0 ) )
    {
      chan_state->lca = chan_info->lc_class_a;
      ul_service.rlc_id[nchan] = chan_state->lca;
      ul_service.ul_wm_ptr[nchan] = &ul_queue->wm_a;
      nchan++;
    }
    
    /* Register class B channel if necessary */
    if ( ( chan_state->has_chan_b == TRUE ) && ( chan_info->lc_class_b > 0 ) )
    {
      chan_state->lcb = chan_info->lc_class_b;
      ul_service.rlc_id[nchan] = chan_state->lcb;
      ul_service.ul_wm_ptr[nchan] = &ul_queue->wm_b;
      nchan++;
    }
    
    /* Register class C channel if necessary */
    if ( ( chan_state->has_chan_c == TRUE ) && ( chan_info->lc_class_c > 0 ) )
    {
      chan_state->lcc = chan_info->lc_class_c;
      ul_service.rlc_id[nchan] = chan_state->lcc;
      ul_service.ul_wm_ptr[nchan] = &ul_queue->wm_c;
      nchan++;
    }
    
    if( nchan > 0 )
    {
      ( void ) voice_dsm_amr_q_empty( &session_obj->ul_queue );
       MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
            "DSM UL queue empty during registration" );
    }

    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH, 
           "tva_tdscdma_register_ul_logical_channels(): BEFORE: lca=(%d), lcb=(%d), "
           "lcc=(%d)", chan_state->lca, chan_state->lcb, chan_state->lcc );

    ul_service.nchan = nchan;
    ul_srv_cmd.as_id = subs_obj->asid;
    ul_srv_cmd.service = &ul_service;

    rc  = tds_ext_audio_api( TDSCDMA_IVOICEL2_CMD_REGISTER_UL_SERVICE,
                             &ul_srv_cmd, sizeof( ul_srv_cmd ) );
    if( rc == TDSCDMA_EOK )
    {
      MSG_4( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "TDSCDMA L2 UL Registration successful. Number of TDSCDMA L2 "
             "uplink logical channels, nchan = (%d), la=(%d), lb=(%d), "
             "lc=(%d)", nchan, chan_info->lc_class_a, chan_info->lc_class_b,
             chan_info->lc_class_c );    
      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH, 
             "tva_tdscdma_register_ul_logical_channels(): AFTER: lca=(%d), lcb=(%d), "
             "lcc=(%d)", chan_state->lca, chan_state->lcb, chan_state->lcc );
    }
    else
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, 
             "TDSCDMA L2 UL Registration failed, rc = (0x%08X). Number of "
             "TDSCDMA L2 uplink logical channels, nchan = (%d).", rc, nchan );
      break;
    }

#endif

    break;
  }

  return rc;
}

static uint32_t tva_tdscdma_deregister_dl_logical_channels (
  tdscdma_ivoice_event_set_logical_channels_t* chan_info,
  tva_modem_subs_object_t* subs_obj,
  tva_session_object_t* session_obj
)
{
  uint32_t rc = APR_EOK;
  uint8_t nchan = 0;
  voice_amr_chan_state_t* chan_state;
  voice_amr_dsm_queue_t* dl_queue = NULL;
  tdsl2_dl_service_register_type dl_service;
  tdscdma_ivoicel2_cmd_register_dl_service_t dl_srv_cmd;

  for( ;; )
  {
    chan_state = &session_obj->dl_chan_state;
    dl_queue = &session_obj->dl_queue;

#ifndef WINSIM
    /**
     * De-register logical channels if necessary
     */
    dl_service.service = FALSE;

    /* deregister class A channel if necessary */
    if ( ( chan_state->has_chan_a == FALSE ) || 
         ( chan_state->lca != chan_info->lc_class_a ) )
    {
      if ( chan_state->lca != 0 )
      {
        dl_service.rlc_id[nchan] = chan_state->lca;
        nchan++;
        chan_state->lca = 0;
      }
    }

    /* deregister class B channel if necessary */
    if ( ( chan_state->has_chan_b == FALSE ) || 
         ( chan_state->lcb != chan_info->lc_class_b ) )
    {
      if ( chan_state->lcb != 0 )
      {
       dl_service.rlc_id[nchan] = chan_state->lcb;
       nchan++;
       chan_state->lcb = 0;
      }
    }

    /* deregister class C channel if necessary */
    if ( ( chan_state->has_chan_c == FALSE ) || 
         ( chan_state->lcc != chan_info->lc_class_c ) )
    {
      if ( chan_state->lcc != 0 )
      {
        dl_service.rlc_id[nchan] = chan_state->lcc;
        nchan++;
        chan_state->lcc = 0;
      }
    }

    if( nchan > 0 )
    {
      ( void ) voice_dsm_amr_q_empty( &session_obj->dl_queue );
        MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "DSM DL queue empty during de-registration" );
    }

    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH, 
           "tva_tdscdma_deregister_dl_logical_channels(): BEFORE: lca=(%d), lcb=(%d), "
           "lcc=(%d)", chan_state->lca, chan_state->lcb, chan_state->lcc );

    dl_service.nchan = nchan;
    dl_srv_cmd.as_id = subs_obj->asid;
    dl_srv_cmd.service = &dl_service;

    rc  = tds_ext_audio_api( TDSCDMA_IVOICEL2_CMD_REGISTER_DL_SERVICE,
                             &dl_srv_cmd, sizeof( dl_srv_cmd ) );
    if( rc == TDSCDMA_EOK )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "TDSCDMA L2 DL De-registration successful. Number of TDSCDMA L2 "
             "downlink logical channels, nchan = (%d).", nchan );
      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH, 
             "tva_tdscdma_deregister_dl_logical_channels(): AFTER: lca=(%d), lcb=(%d), "
             "lcc=(%d)", chan_state->lca, chan_state->lcb, chan_state->lcc );
    }
    else
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, 
             "TDSCDMA L2 DL De-registration failed, rc = (0x%08X). Number of "
             "TDSCDMA L2 downlink logical channels, nchan = (%d).", rc, nchan );
    }
#endif

    break;
  }

  return rc;
}

static uint32_t tva_tdscdma_register_dl_logical_channels (
  tdscdma_ivoice_event_set_logical_channels_t* chan_info,
  tva_modem_subs_object_t* subs_obj,
  tva_session_object_t* session_obj
)
{
  uint32_t rc = APR_EOK;
  uint8_t nchan = 0;
  voice_amr_chan_state_t* chan_state;
  voice_amr_dsm_queue_t* dl_queue = NULL;
  tdsl2_dl_service_register_type dl_service;
  tdscdma_ivoicel2_cmd_register_dl_service_t dl_srv_cmd;

  for( ;; )
  {
    chan_state = &session_obj->dl_chan_state;
    dl_queue = &session_obj->dl_queue;

#ifndef WINSIM
    /**
     * Register logical channels if necessary,
     */
    dl_service.service = TRUE;

    /* Register class A channel if necessary */
    if ( ( chan_state->has_chan_a == TRUE ) && ( chan_info->lc_class_a > 0 ) )
    {
      chan_state->lca = chan_info->lc_class_a;
      dl_service.rlc_id[nchan] = chan_state->lca;
      dl_service.dl_wm_ptr[nchan] = &dl_queue->wm_a;
      dl_service.context[nchan] = TRUE;
      dl_service.rlc_post_rx_func_ptr_para[nchan] = (void*)session_obj;
      dl_service.rlc_post_rx_proc_func_ptr[nchan] = tva_downlink_channel_data_available;
      nchan++;
    }

    /* Register class B channel if necessary */
    if ( ( chan_state->has_chan_b == TRUE ) && ( chan_info->lc_class_b > 0 ) )
    {
      chan_state->lcb = chan_info->lc_class_b;
      dl_service.rlc_id[nchan] = chan_state->lcb;
      dl_service.dl_wm_ptr[nchan] = &dl_queue->wm_b;
      dl_service.context[nchan] = TRUE;
      dl_service.rlc_post_rx_func_ptr_para[nchan] = (void*)session_obj;
      dl_service.rlc_post_rx_proc_func_ptr[nchan] = tva_downlink_channel_data_available;
      nchan++;
    }

    /* Register class C channel if necessary */
    if ( ( chan_state->has_chan_c == TRUE ) && ( chan_info->lc_class_c > 0 ) )
    {
      chan_state->lcc = chan_info->lc_class_c;
      dl_service.rlc_id[nchan] = chan_state->lcc;
      dl_service.dl_wm_ptr[nchan] = &dl_queue->wm_c;
      dl_service.context[nchan] = TRUE;
      dl_service.rlc_post_rx_func_ptr_para[nchan] = (void*)session_obj;
      dl_service.rlc_post_rx_proc_func_ptr[nchan] = tva_downlink_channel_data_available;
      nchan++;
    }

    if( nchan > 0 )
    {
      ( void ) voice_dsm_amr_q_empty( &session_obj->dl_queue );
         MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
              "DSM DL queue empty during registration" );
    }

    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH, 
           "tva_tdscdma_register_dl_logical_channels(): BEFORE: lca=(%d), lcb=(%d), "
           "lcc=(%d)", chan_state->lca, chan_state->lcb, chan_state->lcc );

    dl_service.nchan = nchan;
    dl_srv_cmd.as_id = subs_obj->asid;
    dl_srv_cmd.service = &dl_service;
    rc  = tds_ext_audio_api( TDSCDMA_IVOICEL2_CMD_REGISTER_DL_SERVICE,
                             &dl_srv_cmd, sizeof( dl_srv_cmd ) );
    if( rc == TDSCDMA_EOK )
    {
      MSG_4( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "TDSCDMA L2 DL Registration successful. Number of TDSCDMA L2 "
             "downlink logical channels, nchan = (%d), la=(%d), lb=(%d), "
             "lc=(%d)", nchan, chan_info->lc_class_a, chan_info->lc_class_b,
             chan_info->lc_class_c );

      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH, 
             "tva_tdscdma_register_dl_logical_channels(): AFTER: lca=(%d), lcb=(%d), "
             "lcc=(%d)", chan_state->lca, chan_state->lcb, chan_state->lcc );
    }
    else
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, 
             "TDSCDMA L2 DL Registration failed, rc = (0x%08X). Number of "
             "TDSCDMA L2 downlink logical channels, nchan = (%d).", rc, nchan );
    }
#endif

    break;
  }

  return rc;
}


/****************************************************************************
 * TVA CMDs/EVENTs HANDLING ROUTINES                                        *
 ****************************************************************************/

static uint32_t tva_process_vs_cmd_response_event( 
 tva_event_packet_t* event_pkt
)
{
  uint32_t rc = APR_EOK;
  tva_session_object_t* session_obj = NULL;
  tva_simple_job_object_t* simple_obj = NULL;
  vs_common_event_cmd_response_t* evt_params = NULL;

  for ( ;; )
  {
    session_obj = ( tva_session_object_t* ) event_pkt->session_context;
    if( session_obj == NULL )
    {
      TVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    evt_params = ( ( vs_common_event_cmd_response_t* ) event_pkt->params );
    if( evt_params == NULL )
    {
      TVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED,
           "tva_process_vs_cmd_response_event(): cmd response event recieved "
           "for VS cmd_id=(0x%08x), client_context=(0x%08x), status=(0x%08X)",
           evt_params->cmd_id, evt_params->client_context, evt_params->status_id );

    simple_obj = ( tva_simple_job_object_t* ) evt_params->client_context;
    if ( simple_obj == NULL ) break;

    simple_obj->is_completed = TRUE;
    simple_obj->status = evt_params->status_id;

    break;
  }

  tva_free_event_packet ( event_pkt );

  return rc;
}


static uint32_t tva_process_vs_ready_event( 
 tva_gating_control_t* ctrl
)
{
  uint32_t rc = APR_EOK;
  tva_event_packet_t* event_pkt = NULL;
  tva_session_object_t* session_obj = NULL;

  for ( ;; )
  {
    event_pkt = ( tva_event_packet_t* ) ctrl->packet;
    if( event_pkt == NULL )
    {
      TVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }
    
    session_obj = ( tva_session_object_t* ) event_pkt->session_context;
    if( session_obj == NULL )
    {
      TVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
         "tva_process_vs_ready_event(): vocoder session ready for packet "
         "exchnage" );
 
    TVA_ACQUIRE_LOCK( session_obj->data_lock );
 
    session_obj->is_vs_ready = TRUE;

    /* Prime the Ready buffer with VS for UL packets. */
    rc = tva_vs_prime_read_buffer( session_obj );
    
    TVA_RELEASE_LOCK( session_obj->data_lock );

    break;
  }

  tva_free_event_packet ( event_pkt );

  return rc;
}


static uint32_t tva_process_vs_not_ready_event( 
 tva_gating_control_t* ctrl 
)
{
  uint32_t rc = APR_EOK;
  tva_event_packet_t* event_pkt = NULL;
  tva_session_object_t* session_obj = NULL;
  tva_simple_job_object_t* simple_obj = NULL;

  for ( ;; )
  {
    event_pkt = ( tva_event_packet_t* ) ctrl->packet;
    if( event_pkt == NULL )
    {
      TVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    session_obj = ( tva_session_object_t* ) event_pkt->session_context;
    if( session_obj == NULL )
    {
      TVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    TVA_ACQUIRE_LOCK( session_obj->data_lock );
    
    if ( ctrl->state == TVA_GATING_CMD_STATE_EXECUTE )
    {
      MSG_1 ( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "tva_process_vs_not_ready_event(): for Session with vsid=(0x%08x)", 
             session_obj->vsid );

      session_obj->is_vs_ready = FALSE;

      /* FLUSH BUFFERS when voice services are not ready for vocoder 
         packet exchnage. */
      rc = tva_create_simple_job_object( session_obj->header.handle,
             ( tva_simple_job_object_t** ) &ctrl->rootjob_obj );
      simple_obj= &ctrl->rootjob_obj->simple_job;

      rc = tva_vs_flush_buffers( session_obj, (void*)simple_obj );
    }
    else
    {
      simple_obj = &ctrl->rootjob_obj->simple_job;
      if( simple_obj->is_completed != TRUE )
      {
        rc = APR_EPENDING;
        break;
      }

      /* Restore the read buffer reference if not returned from VS. */
      session_obj->vs_read_buf = session_obj->primed_read_buf;
    }

    break;
  }

  if( session_obj != NULL )
  {
   TVA_RELEASE_LOCK( session_obj->data_lock );
  }

  if ( rc == APR_EOK )
  {
   ( void ) tva_mem_free_object ( ( tva_object_t*) simple_obj );
   ( void ) tva_free_event_packet ( event_pkt );
  }

  return rc;
}


static uint32_t tva_process_vs_open_event ( 
 tva_gating_control_t* ctrl 
)
{
  uint32_t rc = APR_EOK;
  tva_event_packet_t* event_pkt = NULL;
  tva_session_object_t* session_obj = NULL;

  for ( ;; )
  {
    event_pkt = ( tva_event_packet_t* ) ctrl->packet;
    if( event_pkt == NULL )
    {
      TVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }
    
    session_obj = ( tva_session_object_t* ) event_pkt->session_context;
    if( session_obj == NULL )
    {
      TVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    TVA_ACQUIRE_LOCK( session_obj->data_lock );

    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
           "tva_process_vs_open_event(): for Session with vsid=(0x%08x)", 
           session_obj->vsid );

    ( void ) tva_vs_open_session( session_obj );

    TVA_RELEASE_LOCK( session_obj->data_lock );

    break;
  }

  tva_free_event_packet ( event_pkt );

  return rc;
}

static uint32_t tva_process_vs_close_event ( 
 tva_gating_control_t* ctrl 
)
{
  uint32_t rc = APR_EOK;
  tva_event_packet_t* event_pkt = NULL;
  tva_session_object_t* session_obj = NULL;
  tva_simple_job_object_t* simple_obj = NULL;

  for ( ;; )
  {
    event_pkt = ( tva_event_packet_t* ) ctrl->packet;
    if( event_pkt == NULL )
    {
      TVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }
    
    session_obj = ( tva_session_object_t* ) event_pkt->session_context;
    if( session_obj == NULL )
    {
      TVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    TVA_ACQUIRE_LOCK( session_obj->data_lock );

    if ( ctrl->state == TVA_GATING_CMD_STATE_EXECUTE )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "tva_process_vs_close_event(): for Session with vsid=(0x%08x)", 
             session_obj->vsid );;

      rc = tva_create_simple_job_object( session_obj->header.handle,
             ( tva_simple_job_object_t** ) &ctrl->rootjob_obj );
      simple_obj = &ctrl->rootjob_obj->simple_job;

      rc = tva_vs_close_session( session_obj, (void*) simple_obj );
    }
    else
    {
      simple_obj = &ctrl->rootjob_obj->simple_job;
      if( simple_obj->is_completed != TRUE )
      {
        rc = APR_EPENDING;
      }
      else
      {
        session_obj->vs_handle = APR_NULL_V;
      }
    }

    break;
  }

  if( session_obj != NULL )
  {
   TVA_RELEASE_LOCK( session_obj->data_lock );
  }

  if ( rc != APR_EPENDING )
  {
    ( void ) tva_mem_free_object ( ( tva_object_t*) simple_obj );
    ( void ) tva_free_event_packet ( event_pkt );
    rc = APR_EOK;
  }

  return rc;
}


static uint32_t tva_process_vs_eamr_rate_change_event( 
 tva_event_packet_t* event_pkt 
)
{
  uint32_t rc = APR_EOK;
  tva_session_object_t* session_obj = NULL;
  tva_modem_subs_object_t* subs_obj = NULL;
  vs_common_event_eamr_mode_t* evt_params = NULL;
  tdscdma_ivoice_cmd_send_sample_rate_t sample_rate_cmd;

  for ( ;; )
  {
    session_obj = ( tva_session_object_t* ) event_pkt->session_context;
    if( session_obj == NULL )
    {
      TVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    evt_params = ( ( vs_common_event_eamr_mode_t* ) event_pkt->params );
    if( evt_params == NULL )
    {
      TVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    if( session_obj->vocoder_type != TDSCDMA_IVOCODER_ID_AMR )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "tva_process_vs_eamr_rate_change_event(): Not Applicable for "
             "vocoder_type=0x%08x", session_obj->vocoder_type );
      break;
    }

    switch ( evt_params->mode )
    {
      case VS_COMMON_EAMR_MODE_NARROWBAND:
        sample_rate_cmd.sample_rate = TVA_VOICE_SAMPLE_RATE_NB_V;
        break;

      case VS_COMMON_EAMR_MODE_WIDEBAND:
        sample_rate_cmd.sample_rate = TVA_VOICE_SAMPLE_RATE_WB_V;
        break;

      default:
        sample_rate_cmd.sample_rate = TVA_VOICE_SAMPLE_RATE_UNDEFINED_V;
        break;
    }

    if( ( evt_params->mode != VS_COMMON_EAMR_MODE_NARROWBAND ) ||
        ( evt_params->mode != VS_COMMON_EAMR_MODE_WIDEBAND ) )
    {
      subs_obj = session_obj->active_subs_obj;
      sample_rate_cmd.handle = subs_obj->tds_handle;
      sample_rate_cmd.vocoder_id = session_obj->vocoder_type;

#ifndef WINSIM
    rc = tds_ext_audio_api( TDSCDMA_IVOICE_CMD_SEND_SAMPLE_RATE,
                            &sample_rate_cmd, sizeof( sample_rate_cmd ) );
#endif

    }

    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
           "wta_process_vs_eamr_rate_change_event(): for subs_obj asid=%d "
           "eamr mode=%d, sample_rate=%d", subs_obj->asid, evt_params->mode,
           sample_rate_cmd.sample_rate );
    break;
  }

  tva_free_event_packet ( event_pkt );

  return rc;
}



static uint32_t tva_process_vs_read_buf_available_event( 
 tva_event_packet_t* event_pkt 
)
{
  uint32_t rc = APR_EOK;
  tva_session_object_t* session_obj = NULL;
  
  for ( ;; )
  {
    session_obj = ( tva_session_object_t* ) event_pkt->session_context;
    if( session_obj == NULL )
    {
      TVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    /* Read the vocoder buffer from VS has returned.
     * check if vs_buffer->flags is true
     */
    rc = tva_vs_read_buffer ( session_obj );
    if ( session_obj->vs_read_buf == NULL ) break;

    /* Deliver the vocoder data to W-RLC via DSM queue. */
    rc = tva_deliver_ul_vocoder_packet( session_obj );
    if ( rc )
    {
     //Uplink packet discarded.
    }
  
    /* Prime the read buffer again to recieve next UL vs buffer. */
    rc = tva_vs_prime_read_buffer( session_obj );

    break;
  }

  ( void ) tva_free_event_packet ( event_pkt );

  return rc;
}


static uint32_t tva_process_vs_write_buf_returned_event( 
 tva_event_packet_t* event_pkt 
)
{
  uint32_t rc = APR_EOK;
  tva_session_object_t* session_obj = NULL;
  vs_voc_event_write_buffer_returned_t* evt_params = NULL;

  for ( ;; )
  {
    session_obj = ( tva_session_object_t* ) event_pkt->session_context;

    evt_params = ( vs_voc_event_write_buffer_returned_t* ) event_pkt->params;
    if ( evt_params == NULL )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "tva_process_vs_write_buf_returned_event(): evt_paramsr is NULL" );
      break;
    }

    if ( evt_params->buffer == NULL )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "tva_process_vs_write_buf_returned_event(): ret_buffer is NULL" );
      break;
    }

    TVA_ACQUIRE_LOCK( session_obj->data_lock );
    
    /* Indicates that the vocoder buffer written to voice services has been 
     * successfully rendered to DSP.
     * Write buffer returned for next downlink vocoder packet.
     */
    session_obj->vs_write_buf = evt_params->buffer;

    TVA_RELEASE_LOCK( session_obj->data_lock );

    break;
  }

  tva_free_event_packet ( event_pkt );

  return rc;
}


static uint32_t tva_process_tdscdma_scr_mode_event( 
 tva_event_packet_t* event_pkt 
)
{
  uint32_t rc = APR_EOK;
  tva_session_object_t* session_obj = NULL;
  tva_modem_subs_object_t* subs_obj = NULL;
  tdscdma_ivoice_event_request_scr_mode_t* evt_params = NULL;

  for ( ;; )
  {
    subs_obj = ( tva_modem_subs_object_t* ) event_pkt->session_context;
    if ( subs_obj == NULL )
    {
      TVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    evt_params = ( tdscdma_ivoice_event_request_scr_mode_t* ) event_pkt->params;
    if ( evt_params == NULL )
    {
      TVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    session_obj = subs_obj->session_obj;
    if ( session_obj == NULL )
    {
      TVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    TVA_ACQUIRE_LOCK( session_obj->data_lock );

    /* cache the new scr/dtx mode. */
    session_obj->dtx_mode = evt_params->enable_flag;  
    ( void ) tva_set_voc_dtx_mode ( session_obj );

    TVA_RELEASE_LOCK( session_obj->data_lock );

    break;
  }

  ( void ) tva_free_event_packet ( event_pkt );

  return rc;
}


static uint32_t tva_process_tdscdma_select_owner_event( 
 tva_event_packet_t* event_pkt 
)
{
  uint32_t rc = APR_EOK;
  tva_session_object_t* session_obj = NULL;
  tva_modem_subs_object_t* subs_obj = NULL;

  for ( ;; )
  {
    subs_obj = ( tva_modem_subs_object_t* ) event_pkt->session_context;
    if( subs_obj == NULL )
    {
      TVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    session_obj = subs_obj->session_obj;
    if( session_obj == NULL )
    {
      TVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    TVA_ACQUIRE_LOCK( session_obj->data_lock );

    /* FOR IRAT-HO Scenrios,request voice agent for resource grant. */
    if ( ( session_obj->is_resource_granted == FALSE ) &&
         ( session_obj->va_tva_event_cb != NULL ) )
    {
      session_obj->va_tva_event_cb( session_obj->va_session_context,
                                    TVA_IRESOURCE_EVENT_REQUEST, NULL, 0 );
      MSG( MSG_SSID_DFLT, MSG_LEGACY_MED,
           "tva_process_tdscdma_select_owner_event() - Resource requested " );
    }

    TVA_RELEASE_LOCK( session_obj->data_lock );

    break;
  }

  tva_free_event_packet ( event_pkt );

  return rc;
}

static uint32_t tva_process_tdscdma_codec_mode_event( 
 tva_event_packet_t* event_pkt 
)
{
  uint32_t rc = APR_EOK;
  tva_session_object_t* session_obj = NULL;
  tva_modem_subs_object_t* subs_obj = NULL;
  tdscdma_ivoice_event_request_codec_mode_t* evt_params = NULL;

  for ( ;; )
  {
    subs_obj = ( tva_modem_subs_object_t* ) event_pkt->session_context;
    if ( subs_obj == NULL )
    {
      TVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    evt_params = ( tdscdma_ivoice_event_request_codec_mode_t* ) event_pkt->params;
    if ( evt_params == NULL )
    {
      TVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    session_obj = subs_obj->session_obj;
    if ( session_obj == NULL )
    {
      TVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    TVA_ACQUIRE_LOCK( session_obj->data_lock );

    /* cache the new codec mode. */
    session_obj->codec_mode = evt_params->codec_mode;
    ( void ) tva_set_voc_codec_mode ( session_obj );

    TVA_RELEASE_LOCK( session_obj->data_lock );

    break;
  }

  ( void ) tva_free_event_packet ( event_pkt );

  return rc;
}


static uint32_t tva_process_tdscdma_l2_vfr_event( 
 tva_event_packet_t* event_pkt 
)
{
  uint32_t rc = APR_EOK;
  tva_session_object_t* session_obj = NULL;
  tva_modem_subs_object_t* subs_obj = NULL;
  int64_t tx_timing_offset_us = 0;
  uint32_t tds_sub_frame_count = 0;
  uint32_t tds_stmr_tick_count = 0;
  uint32_t tds_vfr_latency_in_us = 0;
  tdscdma_ivoicel2_event_vfr_notification_t* evt_params = NULL;

  for ( ;; )
  {
    subs_obj = ( tva_modem_subs_object_t* ) event_pkt->session_context;
    if ( subs_obj == NULL )
    {
      TVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    evt_params = ( tdscdma_ivoicel2_event_vfr_notification_t* ) event_pkt->params;
    if ( evt_params == NULL )
    {
      TVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    session_obj = subs_obj->session_obj;
    if( session_obj == NULL )
    {
      TVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    /* bits [28:16] represents the sub frame count from TDS. */
    tds_sub_frame_count = ( ( evt_params->tds_system_time & 0x1FFF0000 ) >> 16 );

    /* bits [15:0] represents the system tick for current TDS sub frame.
     * (512) ticks are equivalent to (50) usec.
     */
    tds_stmr_tick_count = ( evt_params->tds_system_time & 0x0000FFFF );
    tds_vfr_latency_in_us = ( uint32_t )( ( 1.0 * tds_stmr_tick_count ) / 10.24 ); 

    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
           "tva_process_tdscdma_l2_vfr_event(): tds_sub_frame_count = (%d),"
           "tds_stmr_tick_count = (%d), tds_vfr_latency_in_micro_sec = (%d)",
           tds_sub_frame_count, tds_stmr_tick_count, tds_vfr_latency_in_us );

    tx_timing_offset_us = ( ( int64_t ) TVA_TDSCDMA_READS_UL_PKT_FRM_DSM_AT_TIME_V -
                                    	 tds_vfr_latency_in_us );
  
    tx_timing_offset_us = ( tx_timing_offset_us - 
                            ( int64_t )( TVA_BUF_TIME_FOR_PUTTING_UL_PACKET_IN_DSM_V ) );

    if ( tx_timing_offset_us <= ( int64_t ) 0 )
    {
      if ( ( ( -1 ) * tx_timing_offset_us ) >= ( int64_t )( TVA_VOICE_FRAME_SIZE_US_V ) )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "tva_process_tdscdma_l2_vfr_event(): tds_vfr_latency_us=(0x%08X) "
               "is invalid", tds_vfr_latency_in_us );
        return APR_EFAILED;
      }

      tx_timing_offset_us  = tx_timing_offset_us + ( ( int64_t ) TVA_VOICE_FRAME_SIZE_US_V );
    }

    rc = apr_timer_start( session_obj->sfg_timer, ( tx_timing_offset_us*1000 ) );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "tva_process_tdscdma_l2_vfr_event(): Failed to start timer, rc = (0x%08x)", rc );
    }
    else
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "tva_process_tdscdma_l2_vfr_event(): Start timer, to be fired in (%d)us", tx_timing_offset_us );
    }

    break;
  }

  ( void ) tva_free_event_packet ( event_pkt );

  return rc;
}


static uint32_t tva_process_tdscdma_set_logical_channels_event( 
 tva_event_packet_t* event_pkt 
)
{
  uint32_t rc = APR_EOK;
  uint32_t chan_class;
  voice_amr_chan_state_t* chan_state = NULL;
  tva_session_object_t* session_obj = NULL;
  tva_modem_subs_object_t* subs_obj = NULL;
  tdscdma_ivoice_event_set_logical_channels_t* evt_params = NULL;

  for ( ;; )
  {
    subs_obj = ( tva_modem_subs_object_t* ) event_pkt->session_context;
    if ( subs_obj == NULL )
    {
      TVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    evt_params = ( tdscdma_ivoice_event_set_logical_channels_t* ) event_pkt->params;
    if ( evt_params == NULL )
    {
      TVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    session_obj = subs_obj->session_obj;
    if ( session_obj == NULL )
    {
      TVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    switch ( evt_params->direction )
    {
     /* Downlink logical channels. */
     case FALSE:
      {
        /* Update the channle state for DSM queue. */
        chan_class = evt_params->class_type;
        chan_state = &session_obj->dl_chan_state;
        rc = tva_update_chan_state( chan_state, chan_class );
        if( rc ) break;

        ( void ) tva_tdscdma_deregister_dl_logical_channels( evt_params, subs_obj,
                                                             session_obj );
        ( void ) tva_tdscdma_register_dl_logical_channels( evt_params, subs_obj,
                                                           session_obj );

      }
      break;
     
     /* Uplink logical channels. */
     case TRUE:
      {
        /* Update the channle state for DSM queue. */
        chan_class = evt_params->class_type;
        chan_state = &session_obj->ul_chan_state;
        rc = tva_update_chan_state( chan_state, chan_class );
        if( rc ) break;
        
        ( void ) tva_tdscdma_deregister_ul_logical_channels( evt_params, subs_obj,
                                                             session_obj );
        ( void ) tva_tdscdma_register_ul_logical_channels( evt_params, subs_obj,
                                                           session_obj );
      }
      break;
    }

    break;
  }

  ( void ) tva_free_event_packet ( event_pkt );

  return rc;
}

static uint32_t tva_process_tdscdma_dl_buf_available_event( 
 tva_event_packet_t* event_pkt 
)
{
  uint32_t rc = APR_EOK;
  tva_session_object_t* session_obj = NULL;

  for ( ;; )
  {
    session_obj = ( tva_session_object_t* ) event_pkt->session_context;
    if( session_obj == NULL )
    {
      TVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    if( session_obj->vs_write_buf == NULL )
    {
      TVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    if( session_obj->is_vs_ready == FALSE )
    {
      ( void ) voice_dsm_amr_q_empty( &session_obj->dl_queue );
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "tva_process_tdscdma_dl_buf_available_event(): VS not ready "
           "discarding DL vocoder channel data" );
      break;
    }

    /* Get the vocoder data from the DSM queue. */
    rc = tva_get_dl_vocoder_packet( session_obj );
    if ( rc ) break;
    
    /* Pass the vocoder buffer to VS from rendering. */
    ( void ) tva_vs_write_buffer( session_obj );

    break;
  }

  ( void ) tva_free_event_packet ( event_pkt );

  return rc;
}


static uint32_t tva_process_send_silence_frame_event ( 
 tva_event_packet_t* event_pkt 
)
{
  uint32_t rc = APR_EOK;
  tva_session_object_t* session_obj = NULL;
  vs_voc_buffer_t* vs_buffer = NULL;
  vs_vocamr_frame_info_t* vocamr_info = NULL;
  vs_vocamrwb_frame_info_t* vocamrwb_info = NULL;
  uint32_t vs_media_id = TVA_VOCODER_ID_UNDEFINED_V;
  uint32_t codec_mode = TVA_CODEC_MODE_UNDEFINED;


  for ( ;; )
  {
    session_obj = ( tva_session_object_t* ) event_pkt->session_context;
    if( session_obj == NULL )
    {
      TVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    TVA_ACQUIRE_LOCK( session_obj->data_lock );

    if( ( session_obj->is_vs_ready != FALSE ) ||
        ( session_obj->vs_read_buf == NULL ) )
    {
      break;
    }

    vs_media_id = tva_map_vocoder_type_tdscdma_to_vs( session_obj->vocoder_type );
    vs_buffer = session_obj->vs_read_buf;
    vs_buffer->media_id = vs_media_id;

    if( vs_media_id == VS_COMMON_MEDIA_ID_AMR )
    {
      ( void ) tva_map_vocamr_codec_mode_tdscdma_to_vs( session_obj->codec_mode,
                                                      &codec_mode );
       vocamr_info = ( vs_vocamr_frame_info_t* ) vs_buffer->frame_info;
       vocamr_info->frame_type = VS_VOCAMR_FRAME_TYPE_SPEECH_GOOD;
       vocamr_info->codec_mode = ( vs_vocamr_codec_mode_t ) codec_mode;
    }
    else if( vs_media_id == VS_COMMON_MEDIA_ID_AMRWB )
    {
      ( void ) tva_map_vocamrwb_codec_mode_tdscdma_to_vs( session_obj->codec_mode,
                                                        &codec_mode );
      vocamrwb_info = ( vs_vocamrwb_frame_info_t* ) vs_buffer->frame_info;
      vocamrwb_info->frame_type = VS_VOCAMRWB_FRAME_TYPE_SPEECH_GOOD;
      vocamrwb_info->codec_mode = ( vs_vocamrwb_codec_mode_t ) codec_mode;
    }
    else
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "tva_process_send_silence_frame_event(): Invalid vocoder type!!!" );
      break;
    }

    /* Get the silence frame vectors from voice utils. */
    rc = voice_util_get_homing_frame( vs_media_id, vs_buffer->frame_info,
                                      vs_buffer->frame, &vs_buffer->size );
    if ( rc ) break;

    /* Deliver the vocoder data to Td-RLC via DSM queue. */
    ( void ) tva_deliver_ul_vocoder_packet( session_obj );

    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
         "Tva_process_send_silence_frame_event(): Sending silence frame!!!" );


    break;
  }

  if( session_obj != NULL )
  {
    TVA_RELEASE_LOCK( session_obj->data_lock );
  }

  ( void ) tva_free_event_packet ( event_pkt );

  return rc;
}


static uint32_t tva_process_tdscdma_vocoder_start_event( 
 tva_gating_control_t* ctrl 
)
{
  uint32_t rc = APR_EOK;
  tva_event_packet_t* event_pkt = NULL;
  tva_session_object_t* session_obj = NULL;
  tva_modem_subs_object_t* subs_obj = NULL;
  tdscdma_ivoice_event_request_start_t* evt_params = NULL;
  tva_simple_job_object_t* simple_obj = NULL;

  for ( ;; )
  {
    event_pkt = ( tva_event_packet_t* ) ctrl->packet;
    if( event_pkt == NULL )
    {
      rc = APR_EUNEXPECTED;
      TVA_REPORT_FATAL_ON_ERROR ( rc );
      break;
    }
    
    evt_params = ( tdscdma_ivoice_event_request_start_t* ) event_pkt->params;
    if( evt_params == NULL )
    {
      rc = APR_EUNEXPECTED;
      TVA_REPORT_FATAL_ON_ERROR ( rc );
      break;
    }
    
    subs_obj= ( tva_modem_subs_object_t* ) event_pkt->session_context;
    if( subs_obj == NULL )
    {
      rc = APR_EUNEXPECTED;
      TVA_REPORT_FATAL_ON_ERROR ( rc );
      break;
    }
    
    session_obj = subs_obj->session_obj;
    if( session_obj == NULL )
    {
      rc = APR_EUNEXPECTED;
      TVA_REPORT_FATAL_ON_ERROR ( rc );
      break;
    }

    TVA_ACQUIRE_LOCK( session_obj->data_lock );

    if ( ctrl->state == TVA_GATING_CMD_STATE_EXECUTE )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,
             "tva_process_tdscdma_vocoder_start_event() - configured vocoder "
             "id = (0x%08x), requested vocoder id = (0x%08x)",
             session_obj->vocoder_type, evt_params->vocoder_id );

      if ( subs_obj->is_tds_ready == TRUE )
      {
        MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "tva_process_tdscdma_vocoder_start_event() - TDSCDMA already requested");
        break;
      }

      if ( evt_params->vocoder_id != TDSCDMA_IVOCODER_ID_NONE )
      {
        /* During Inter-frequency HHO scenrios, reuse the vocoder_type 
           set initially by WRRC. */
        session_obj->vocoder_type = evt_params->vocoder_id;
      }

      /* Vaildate the vocoder_type and check if the Logical channel are 
         configured to adapter by WRRC, this is to avoid multiple start/stop 
         from WL1 during add/drop state machine. */
      if ( ( session_obj->vocoder_type == TVA_VOCODER_ID_UNDEFINED_V ) ||
           ( session_obj->ul_chan_state.has_chan_a == FALSE ) ||
           ( session_obj->dl_chan_state.has_chan_a == FALSE ) )
      {
         MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
                "tva_process_tdscdma_vocoder_start_event() - Invalid vocoder "
                "type = (0x%08x) configurred, ul_dsm_state=%d, dl_dsm_state=%d",
                session_obj->vocoder_type, session_obj->ul_chan_state.has_chan_a,
                session_obj->dl_chan_state.has_chan_a );
         break;
      }

      /* Update the TDSCDMA state. */
      subs_obj->is_tds_ready = TRUE;
      session_obj->active_subs_obj = subs_obj;

      if ( ( session_obj->is_resource_granted == FALSE ) &&
           ( session_obj->va_tva_event_cb != NULL ) )
      {
        session_obj->va_tva_event_cb( session_obj->va_session_context,
                                      TVA_IRESOURCE_EVENT_REQUEST, NULL, 0 );
        MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "tva_process_tdscdma_vocoder_start_event() - Resource requested" );
        rc = APR_EOK;
        break;
      }

      /* Set cached vocoder properties. */
      ( void ) tva_set_voc_dtx_mode ( session_obj );
      ( void ) tva_set_voc_codec_mode ( session_obj ); 

      /* Create the Simple job object to track VS_ENABLE. */
      rc = tva_create_simple_job_object( session_obj->header.handle,
             ( tva_simple_job_object_t** ) &ctrl->rootjob_obj );
      simple_obj = &ctrl->rootjob_obj->simple_job;

      rc = tva_vs_enable_vocoder( session_obj, simple_obj );
    }
    else
    {
     simple_obj = &ctrl->rootjob_obj->simple_job;
     if( simple_obj->is_completed != TRUE )
     {
       rc = APR_EPENDING;
       break;
     }

     ( void ) tva_tdscdma_start_session( subs_obj );
     ( void ) tva_tdscdma_set_vfr_notification( subs_obj );
    }

    break;
  }

  /* If the return code is not APR_EPENDING it confirm that nothing is left for
   * hence memory associated to CMD packet, EVENT packet and JOB object created
   * shall be relesed. Control shall not return to this handler for a perticular
   * Insance of event. */
  if ( APR_EPENDING != rc ) 
  {
    rc = APR_EOK;
    /* Free CMD/EVT packet memory. */
    ( void ) tva_mem_free_object ( ( tva_object_t*) simple_obj );
    ( void ) tva_free_event_packet ( event_pkt );
  }

  if( session_obj != NULL )
  {
    TVA_RELEASE_LOCK( session_obj->data_lock );
  }

  return rc;
}


static uint32_t tva_process_tdscdma_vocoder_stop_event( 
 tva_gating_control_t* ctrl 
)
{
  uint32_t rc = APR_EOK;
  tva_event_packet_t* event_pkt = NULL;
  tva_session_object_t* session_obj = NULL;
  tva_modem_subs_object_t* subs_obj = NULL;
  tva_simple_job_object_t* simple_obj = NULL;
  tva_icommon_cmd_set_asid_vsid_mapping_t tva_map_cmd;

  for ( ;; )
  {
    event_pkt = ( tva_event_packet_t* ) ctrl->packet;
    if( event_pkt == NULL )
    {
      rc = APR_EUNEXPECTED;
      TVA_REPORT_FATAL_ON_ERROR ( rc );
      break;
    }
    
    subs_obj = ( tva_modem_subs_object_t* ) event_pkt->session_context;
    if( subs_obj == NULL )
    {
      rc = APR_EUNEXPECTED;
      TVA_REPORT_FATAL_ON_ERROR ( rc );
      break;
    }
    
    session_obj = subs_obj->session_obj;
    if( session_obj == NULL )
    {
      rc = APR_EUNEXPECTED;
      TVA_REPORT_FATAL_ON_ERROR ( rc );
      break;
    }

    TVA_ACQUIRE_LOCK( session_obj->data_lock );

    if ( ctrl->state == TVA_GATING_CMD_STATE_EXECUTE )
    {
      /* If the TRRC de-registeres the logical channles, update the session
         vocoder type as undefined, to ignore multiple start/stopn from TL1
         during add/drop state machine. */
      if ( ( session_obj->ul_chan_state.has_chan_a == FALSE ) &&
           ( session_obj->dl_chan_state.has_chan_a == FALSE ) )
      {
        session_obj->vocoder_type = TVA_VOCODER_ID_UNDEFINED_V;
        MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
               "tva_process_tdscdma_vocoder_stop_event(): Invalidate configured "
               "type = (0x%08x), ul_dsm_state=%d, dl_dsm_state=%d",
               session_obj->vocoder_type, session_obj->ul_chan_state.has_chan_a,
               session_obj->dl_chan_state.has_chan_a );
      }

      if ( FALSE == subs_obj->is_tds_ready )
      {
        MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "tdscdma_vocoder_stop_event(PROCESS): Vocoder not started yet" );
        break;
      }

      /* Update the TDSCDMA state. */
      subs_obj->is_tds_ready = FALSE;
      /* Invalidate the active subscrition object. */
      session_obj->active_subs_obj = NULL;

      if ( TRUE == session_obj->is_resource_granted )
      {
        /* Create the Simple job object to track CVD setup. */
        rc = tva_create_simple_job_object( session_obj->header.handle,
               ( tva_simple_job_object_t** ) &ctrl->rootjob_obj );
        simple_obj = &ctrl->rootjob_obj->simple_job;
  
        rc = tva_vs_disable_vocoder( session_obj, simple_obj );
      }
    }
    else
    {
      simple_obj = &ctrl->rootjob_obj->simple_job;
      if( simple_obj->is_completed != TRUE )
      {
        rc = APR_EPENDING;
        break;
      }
    }

    break;
  }

  if ( APR_EOK == rc ) 
  {
    /* Send voice resource released event to voice agent.
     * "REQUEST_STOP sent by T-RRC without sending REQUEST_START" 
     * This can happen if T-RRC called SELECT_OWNER during interRAT 
     * handover start. However the handover failed afterwards. So, T-RRC 
     * did not call REQUEST_START, instead directly called REQUEST_STOP 
     * to indicate TVA that it no longer required vocoder. 
     */
    if ( session_obj->va_tva_event_cb != NULL ) 
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "tdscdma_vocoder_stop_event(PROCESS) - RELEASED event sent to "
             "VA. grant_status=(%d)", session_obj->is_resource_granted );
      session_obj->is_resource_granted = FALSE;
      session_obj->va_tva_event_cb( session_obj->va_session_context, 
                                    TVA_IRESOURCE_EVENT_RELEASED, NULL, 0 );
    }

     /* Queue a cmd if a ASID-VSID mapping is waiting to be processed. */
    if ( TVA_VSID_UNDEFINED_V != subs_obj->pending_vsid )
    {
      tva_map_cmd.asid = subs_obj->asid;
      tva_map_cmd.vsid = subs_obj->pending_vsid;
      subs_obj->pending_vsid = TVA_VSID_UNDEFINED_V;
      tva_prepare_and_dispatch_cmd_packet ( TVA_ICOMMON_CMD_SET_ASID_VSID_MAPPING, 
                                            (void*)&tva_map_cmd,  sizeof( tva_map_cmd ) );
    }
  }

  /* If the return code is not APR_EPENDING it confirm that nothing is left for
   * hence memory associated to CMD packet, EVENT packet and JOB object created
   * shall be relesed. Control shall not return to this handler for a perticular
   * Insance of event. */
  if ( APR_EPENDING != rc ) 
  {
    rc = APR_EOK;
    /* Send STOP and VFR Disable notfication to TDSCDMA. */
    ( void ) tva_tdscdma_stop_session ( subs_obj );
    ( void ) tva_tdscdma_set_vfr_notification ( subs_obj );

    /* Free CMD/EVT packet memory. */
    ( void ) tva_mem_free_object ( ( tva_object_t*) simple_obj );
    ( void ) tva_free_event_packet ( event_pkt );
  }

  if( session_obj != NULL )
  {
    TVA_RELEASE_LOCK( session_obj->data_lock );
  }

  return rc;
}


static uint32_t tva_process_resource_grant_cmd( 
 tva_gating_control_t* ctrl 
)
{
  uint32_t rc = APR_EOK;
  tva_cmd_packet_t* cmd_pkt = NULL;
  tva_session_object_t* session_obj = NULL;
  tva_modem_subs_object_t* subs_obj = NULL;
  tva_iresource_cmd_grant_t* cmd_params = NULL;
  tva_simple_job_object_t* simple_obj = NULL;

  for ( ;; )
  {
    cmd_pkt = ( tva_cmd_packet_t* ) ctrl->packet;
    if( cmd_pkt == NULL )
    {
      TVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    cmd_params = ( tva_iresource_cmd_grant_t* ) cmd_pkt->params;
    if( cmd_params == NULL )
    {
      TVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    rc = tva_get_object ( cmd_params->handle, ( tva_object_t** )&session_obj );
    if( session_obj == NULL )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "tva_process_resource_grant_cmd(): Invalid handle=0X%08X, "
             "rc=0X%08X", cmd_params->handle, rc );
      rc = APR_EOK;
      break;
    }

    TVA_ACQUIRE_LOCK( session_obj->data_lock );

    if ( ctrl->state == TVA_GATING_CMD_STATE_EXECUTE )
    {
      session_obj->is_resource_granted = TRUE;
      subs_obj = session_obj->active_subs_obj;
      if ( ( subs_obj == NULL ) || ( subs_obj->is_tds_ready == FALSE ) )
      {
        MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "tva_process_resource_grant_cmd() - Traffic start request not "
             "available from TDSCDMA" );
        break;
      }

      MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
           "tva_process_resource_grant_cmd() - ALl SET, calling VS_ENABLE" );

      /* Set cached vocoder properties. */
      ( void ) tva_set_voc_dtx_mode ( session_obj );
      ( void ) tva_set_voc_codec_mode ( session_obj );

      /* Create the Simple job object to track VS_ENABLE. */
      rc = tva_create_simple_job_object( session_obj->header.handle,
             ( tva_simple_job_object_t** ) &ctrl->rootjob_obj );
      simple_obj = &ctrl->rootjob_obj->simple_job;

      rc = tva_vs_enable_vocoder( session_obj, simple_obj );
    }
    else
    {
      simple_obj = &ctrl->rootjob_obj->simple_job;
      if( simple_obj->is_completed != TRUE )
      {
        rc = APR_EPENDING;
        break;
      }
      
      ( void ) tva_tdscdma_start_session( session_obj->active_subs_obj );
      ( void ) tva_tdscdma_set_vfr_notification( session_obj->active_subs_obj );
    }

    break;
  }

  if( session_obj != NULL )
  {
    TVA_RELEASE_LOCK( session_obj->data_lock );
  }

  if ( rc == APR_EOK )
  {
    ( void ) tva_mem_free_object ( ( tva_object_t*) simple_obj );
    ( void ) tva_free_cmd_packet ( cmd_pkt );
  }

  return rc;
}


static uint32_t tva_process_resource_revoke_cmd( 
 tva_gating_control_t* ctrl 
)
{
  uint32_t rc = APR_EOK;
  tva_cmd_packet_t* cmd_pkt = NULL;
  tva_session_object_t* session_obj = NULL;
  tva_iresource_cmd_grant_t* cmd_params = NULL;
  tva_simple_job_object_t* simple_obj = NULL;

  for ( ;; )
  {
    cmd_pkt = ( tva_cmd_packet_t* ) ctrl->packet;
    if( cmd_pkt == NULL )
    {
      TVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    cmd_params = ( tva_iresource_cmd_grant_t* ) cmd_pkt->params;
    if( cmd_params == NULL )
    {
      TVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    rc = tva_get_object ( cmd_params->handle, ( tva_object_t** )&session_obj );
    if( session_obj == NULL )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "tva_process_resource_revoke_cmd(): Invalid handle=0X%08X, "
             "rc=0X%08X", cmd_params->handle, rc );
      rc = APR_EOK;
      break;
    }

    TVA_ACQUIRE_LOCK( session_obj->data_lock );

    if ( ctrl->state == TVA_GATING_CMD_STATE_EXECUTE )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
           "tva_process_resource_revoke_cmd() - vocoder revoked" );
    
      /* Create the Simple job object to track VS_DISABLE. */
      rc = tva_create_simple_job_object( session_obj->header.handle,
             ( tva_simple_job_object_t** ) &ctrl->rootjob_obj );
      simple_obj= &ctrl->rootjob_obj->simple_job;

      rc = tva_vs_disable_vocoder( session_obj, simple_obj );
    }
    else
    {
      simple_obj= &ctrl->rootjob_obj->simple_job;
      if( simple_obj->is_completed != TRUE )
      {
        rc = APR_EPENDING;
        break;
      }

      /* Send a vocoder release event to voice agent. */
      if( session_obj->va_tva_event_cb!= NULL )
      {
        session_obj->is_resource_granted = FALSE;
        session_obj->va_tva_event_cb( session_obj->va_session_context,
                                      TVA_IRESOURCE_EVENT_RELEASED, NULL, 0 );
      }
    }

    break;
  }

  if ( session_obj != NULL )
  {
    TVA_RELEASE_LOCK( session_obj->data_lock );
  }

  if ( rc == APR_EOK ) 
  {
    ( void ) tva_mem_free_object ( ( tva_object_t*) simple_obj );
    ( void ) tva_free_cmd_packet ( cmd_pkt );
  }

  return rc;
}

static uint32_t tva_process_set_asid_vsid_mapping_cmd ( 
  tva_cmd_packet_t* cmd_pkt 
)
{
  uint32_t rc = APR_EOK;
  uint32_t index = 0;
  tva_modem_subs_object_t* subs_obj = NULL;
  tva_session_object_t* session_obj = NULL;
  tva_icommon_cmd_set_asid_vsid_mapping_t* cmd_params = NULL;

  for ( ;; )
  {
    if( cmd_pkt == NULL )
    {
      TVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    cmd_params = ( tva_icommon_cmd_set_asid_vsid_mapping_t* ) cmd_pkt->params;
    if( cmd_params == NULL )
    {
      TVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    TVA_ACQUIRE_LOCK( tva_global_lock );

    subs_obj = tva_subs_obj_list[cmd_params->asid];
    if( subs_obj->vsid == cmd_params->vsid )
    {
      subs_obj->pending_vsid = TVA_VSID_UNDEFINED_V;
      MSG( MSG_SSID_DFLT, MSG_LEGACY_MED,
           "tva_process_set_asid_vsid_mapping_cmd(): no change to mapping" );
      TVA_RELEASE_LOCK ( tva_global_lock );
      break;
    }

    if( TRUE == subs_obj->is_tds_ready ) 
    {
      /* Store the VSID and apply the mapping at the end of the current voice call. */
      subs_obj->pending_vsid = cmd_params->vsid;
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,
           "tva_process_set_asid_vsid_mapping_cmd(): call in-progress"
           "new mapping is stored in pending-set: ASID=(%d) VSID=(0X%08X)",
           cmd_params->asid, cmd_params->vsid );
      TVA_RELEASE_LOCK ( tva_global_lock );
      break;
    }

    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,
           "tva_process_set_asid_vsid_mapping_cmd(): ASID=(%d) VSID=(0X%08X)",
           cmd_params->asid, cmd_params->vsid );
  
    /* Update the ASID-VSID mapping and the session object as per the VSID. */
    subs_obj->vsid = cmd_params->vsid;
    subs_obj->session_obj = NULL;

    for( index = 0; index < TVA_MAX_NUM_OF_SESSIONS_V; ++index )
    {
      session_obj = tva_session_obj_list[index];

      if( session_obj->vsid == subs_obj->vsid )
      {
        subs_obj->session_obj = session_obj;
        break;
      }
      else
      {
        session_obj = NULL;
      }
    }

    if( session_obj != NULL )
    {
      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "tva_process_set_asid_vsid_mapping_cmd() - asid=%d mapped to "
             "session_obj=(0x%08x) with vsid=(0x%08x)", subs_obj->asid,
             subs_obj->session_obj, subs_obj->vsid );
    }
    else
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "tva_process_set_asid_vsid_mapping_cmd(): Cannot find TVA session" );
    }

    TVA_RELEASE_LOCK ( tva_global_lock );

    break;
  }

  ( void ) tva_free_cmd_packet ( cmd_pkt );

  return rc;
}

/****************************************************************************
 * NONGATING REQUEST( CMDs/EVENTs ) PROCESSING FUNCTIONS
 ****************************************************************************/

static void tva_task_process_nongating_work_items ( void )
{
  uint32_t rc = APR_EOK;
  uint32_t request_id = 0;
  tva_work_item_packet_type_t pkt_type;
  tva_work_item_t* work_item = NULL;
  void* packet = NULL;

  while( apr_list_remove_head( &tva_nongating_work_pkt_q,
                               ( ( apr_list_node_t** ) &work_item ) ) == APR_EOK )
  {
    pkt_type = work_item->pkt_type;
    packet = work_item->packet;
    
    if ( pkt_type == TVA_WORK_ITEM_PKT_TYPE_EVENT )
    {
      request_id = ( ( tva_event_packet_t* ) packet )->event_id ;
    }
    else if ( pkt_type ==  TVA_WORK_ITEM_PKT_TYPE_CMD )
    {
      request_id = ( ( tva_cmd_packet_t* ) packet )->cmd_id;
    }
    else
    {
      TVA_PANIC_ON_ERROR ( APR_EUNEXPECTED );
    }

    /* Add back to vs_free_work_pkt_q. */
    work_item->pkt_type = TVA_WORK_ITEM_PKT_TYPE_NONE;
    work_item->packet = NULL;
    ( void ) apr_list_add_tail( &tva_free_work_pkt_q, &work_item->link );

    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,
           "tva_task_process_nongating_work_items(): Processing "
           "request_id=(0X%08X)", request_id );

    switch( request_id )
    {
     /**
      * Handling routine for nongating work-items should take of release the
      * memory allocated for the CMD/EVENT packets.
      */
     case TDSCDMA_IVOICE_EVENT_REQUEST_CODEC_MODE:
       rc = tva_process_tdscdma_codec_mode_event( ( tva_event_packet_t* ) packet );
       break;

     case TDSCDMA_IVOICE_EVENT_REQUEST_SCR_MODE:
       rc = tva_process_tdscdma_scr_mode_event( ( tva_event_packet_t* ) packet );
       break;

     case TDSCDMA_IVOICE_EVENT_SELECT_OWNER:
       rc = tva_process_tdscdma_select_owner_event( ( tva_event_packet_t* ) packet );
       break;

     case TDSCDMA_IVOICE_EVENT_SET_LOGICAL_CHANNELS:
       rc = tva_process_tdscdma_set_logical_channels_event( ( tva_event_packet_t* ) packet );
       break;

     case TDSCDMA_IVOICEL2_EVENT_VFR_NOTIFICATION:
       rc = tva_process_tdscdma_l2_vfr_event( ( tva_event_packet_t* ) packet );
       break;

     case TVA_INTERNAL_EVENT_DL_BUFFER_AVAILABLE:
       rc = tva_process_tdscdma_dl_buf_available_event( ( tva_event_packet_t* ) packet );
       break;

     case TVA_INTERNAL_EVENT_SEND_SILENCE_FRAME:
       rc = tva_process_send_silence_frame_event( ( tva_event_packet_t* ) packet );
       break;

     case TVA_ICOMMON_CMD_SET_ASID_VSID_MAPPING:
       rc = tva_process_set_asid_vsid_mapping_cmd( ( tva_cmd_packet_t* ) packet  );
       break;

     case VS_COMMON_EVENT_CMD_RESPONSE:
       rc = tva_process_vs_cmd_response_event( ( tva_event_packet_t* ) packet );
       break;

     case VS_COMMOM_EVENT_EAMR_MODE_CHANGE:
       rc = tva_process_vs_eamr_rate_change_event( ( tva_event_packet_t* ) packet );
       break;

     case VS_VOC_EVENT_READ_AVAILABLE:
       rc = tva_process_vs_read_buf_available_event( ( tva_event_packet_t* ) packet );
       break;

     case VS_VOC_EVENT_WRITE_BUFFER_RETURNED:
       rc = tva_process_vs_write_buf_returned_event( ( tva_event_packet_t* ) packet );
       break;

     default:
       /* Add remaining work items to the gating work queue. */
       rc = tva_queue_work_packet ( TVA_WORK_ITEM_QUEUE_TYPE_GATING,
                                    pkt_type, packet );
       if ( rc )
       {
         if ( pkt_type == TVA_WORK_ITEM_PKT_TYPE_CMD )
         {
           tva_free_cmd_packet( ( tva_cmd_packet_t* ) packet );
         }
         else
         {
          tva_free_event_packet( ( tva_event_packet_t* ) packet );
         }

       }
       break;
    }
  }

  return;
}

/****************************************************************************
 * GATING REQUEST( CMDs/EVENTs ) PROCESSING FUNCTIONS
 ****************************************************************************/
 
static void tva_task_process_gating_work_items ( void )
{
  uint32_t rc = APR_EOK;
  uint32_t request_id = 0;
  tva_work_item_t* work_item;
  tva_cmd_packet_t* cmd_pkt;
  tva_event_packet_t* event_pkt;
  tva_gating_control_t* ctrl = &tva_gating_work_pkt_q;

  for ( ;; )
  {
    switch ( ctrl->state )
    {
     case TVA_GATING_CMD_STATE_FETCH:
       {
          /* Fetch the next gating command to execute. */
          rc = apr_list_remove_head( &ctrl->cmd_q,
                                     ( ( apr_list_node_t** ) &work_item ) );
          if ( rc ) return;

          if ( ( work_item->packet == NULL ) ||
               ( work_item->pkt_type == TVA_WORK_ITEM_PKT_TYPE_NONE ) )
          {
            TVA_PANIC_ON_ERROR ( APR_EUNEXPECTED );
          }

          MSG( MSG_SSID_DFLT, MSG_LEGACY_MED,
               "tva_task_process_gating_work_items(): "
               "VS_GATING_CMD_STATE_ENUM_FETCH" );

          ctrl->packet = work_item->packet;
          ctrl->pkt_type = work_item->pkt_type;
          ctrl->state = TVA_GATING_CMD_STATE_EXECUTE;

          /* Add the vs_work_item_t back to vs_free_work_pkt_q. */
          work_item->packet = NULL;
          work_item->pkt_type = TVA_WORK_ITEM_PKT_TYPE_NONE;
          ( void ) apr_list_add_tail( &tva_free_work_pkt_q, &work_item->link );
       }
       break;

     case TVA_GATING_CMD_STATE_EXECUTE:
     case TVA_GATING_CMD_STATE_CONTINUE:
       {
         if ( ctrl->pkt_type == TVA_WORK_ITEM_PKT_TYPE_CMD )
         {
           cmd_pkt = ( ( tva_cmd_packet_t* ) ctrl->packet );
           request_id = cmd_pkt->cmd_id;
         }
         else if ( ctrl->pkt_type == TVA_WORK_ITEM_PKT_TYPE_EVENT )
         {
           event_pkt = ( ( tva_event_packet_t* ) ctrl->packet );
           request_id = event_pkt->event_id;
         }
         else
         {
           TVA_PANIC_ON_ERROR ( APR_EUNEXPECTED );
         }

         /**
          * For Supported request_id, handler should take care of releasing 
          * memory allocated for packets.
          */
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,
                 "tva_task_process_gating_work_items(): request_id=(0x%08x)",
                 request_id );

         switch ( request_id )
         {
          case TDSCDMA_IVOICE_EVENT_REQUEST_START:
            rc = tva_process_tdscdma_vocoder_start_event( ctrl );
            break;

          case TDSCDMA_IVOICE_EVENT_REQUEST_STOP:
            rc = tva_process_tdscdma_vocoder_stop_event( ctrl );
            break;

          case VS_COMMON_EVENT_READY:
            rc = tva_process_vs_ready_event( ctrl );
            break;

          case VS_COMMON_EVENT_NOT_READY:
            rc = tva_process_vs_not_ready_event( ctrl );
            break;
          /* TODO: Add handling of resource grant and revoke feom voice agent. */

          case TVA_INTERNAL_EVENT_VS_OPEN:
            rc = tva_process_vs_open_event( ctrl );
            break;

          case TVA_INTERNAL_EVENT_VS_CLOSE:
            rc = tva_process_vs_close_event( ctrl );
            break;

          case TVA_IRESOURCE_CMD_GRANT:
            rc = tva_process_resource_grant_cmd( ctrl );
            break;

          case TVA_IRESOURCE_CMD_REVOKE:
            rc = tva_process_resource_revoke_cmd( ctrl );
            break;

          default:
            {
              MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
                     "tva_task_process_gating_work_items(): Unsupported "
                     "request_id=(0X%08X)", request_id );

              /** For unsupported request_id, memory cleanup required for
               *  CMD/EVENT packets. */
              if( ctrl->pkt_type == TVA_WORK_ITEM_PKT_TYPE_CMD )
              {
                ( void ) tva_free_cmd_packet ( cmd_pkt );
              }

              if( ctrl->pkt_type == TVA_WORK_ITEM_PKT_TYPE_EVENT )
              {
                ( void ) tva_free_event_packet ( event_pkt );
              }

              /* set to VS_EOK to fetch the next command in queue. */
              rc = APR_EOK;
            }
           break;
         }

         /* Evaluate the pending command completion status. */
         switch ( rc )
         {
          case APR_EOK:
            {
              MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
                     "tva_task_process_gating_work_items(): request_id = "
                     "(0X%08X) processed successfully ", request_id );
              ctrl->packet = NULL;
              ctrl->pkt_type = TVA_WORK_ITEM_PKT_TYPE_NONE;
              /* The current command is finished so fetch the next command. */
              ctrl->state = TVA_GATING_CMD_STATE_FETCH;
            }
            break;
         
          case APR_EPENDING:
            /**
             * Assuming the current pending command control routine returns
             * APR_EPENDING the overall progress stalls until one or more
             * external events or responses are received.
             */
            ctrl->state = TVA_GATING_CMD_STATE_CONTINUE;
            /**
             * Return from here so as to avoid unecessary looping till reponse
             * is recived.
             */
            return;
         
          default:
            TVA_PANIC_ON_ERROR( APR_EUNEXPECTED );
            break;
         }
       }
       break;

     default:
      TVA_PANIC_ON_ERROR( rc );
      break;
    }/* switch case ends. */
  }/* for loop ends. */

  return;
}

/****************************************************************************
 * TVA TASK ROUTINES                                                        *
 ****************************************************************************/

TVA_INTERNAL void tva_signal_run ( void )
{
  apr_event_signal( tva_work_event );
}

static int32_t tva_run ( void )
{
  tva_task_process_nongating_work_items( );
  tva_task_process_gating_work_items( );

  return APR_EOK;
}

static int32_t tva_worker_thread_fn (
  void* param
)
{
  int32_t rc;

  apr_event_create( &tva_work_event );
  apr_event_signal( tva_control_event );

  for ( ;; )
  {
    rc = apr_event_wait( tva_work_event );
    if ( rc ) break;

    tva_run( );
  }

  apr_event_destroy( tva_work_event );
  apr_event_signal( tva_control_event );

  return APR_EOK;
}


/****************************************************************************
 * TVA BOOT-UP and POWER-DOWN ROUTINES                                      *
 ****************************************************************************/

static uint32_t tva_gating_control_init (
  tva_gating_control_t* p_ctrl
)
{
  uint32_t rc = APR_EOK;

  if ( p_ctrl == NULL )
  {
    return APR_EBADPARAM;
  }

  rc = apr_list_init_v2( &p_ctrl->cmd_q,
                         tva_thread_lock_fn, tva_thread_unlock_fn );
  if ( rc )
  {
    return APR_EFAILED;
  }

  p_ctrl->state = TVA_GATING_CMD_STATE_FETCH;
  p_ctrl->pkt_type = TVA_WORK_ITEM_PKT_TYPE_NONE;
  p_ctrl->packet = NULL;
  p_ctrl->rootjob_obj = NULL;

  return APR_EOK;
}  /* end of tva_gating_control_init () */

static uint32_t tva_gating_control_destroy (
  tva_gating_control_t* p_ctrl
)
{
  if ( p_ctrl == NULL )
  {
    return APR_EBADPARAM;
  }

  ( void ) apr_list_destroy( &p_ctrl->cmd_q );

  return APR_EOK;
}  /* end of vs_gating_control_destroy () */


/****************************************************************************
 * EXTERNAL API ROUTINES                                                    *
 ****************************************************************************/

static uint32_t tva_resource_cmd_register_proc ( 
  void* params,
  uint32_t size
)
{
  uint32_t rc = APR_EOK;
  uint32_t index = 0;
  tva_session_object_t* session_obj = NULL;
  tva_iresource_cmd_register_t* cmd_params = NULL;
  
  for ( ;; )
  {
    if ( size != sizeof ( tva_iresource_cmd_register_t ) )
    {
      TVA_REPORT_FATAL_ON_ERROR ( APR_EBADPARAM );
      rc = APR_EBADPARAM;
      break;
    }
  
    cmd_params = ( tva_iresource_cmd_register_t* ) params;
    if( cmd_params == NULL )
    {
      TVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      rc = APR_EBADPARAM;
      break;
    }
  
    TVA_ACQUIRE_LOCK ( tva_global_lock );
  
    /* Find a session with TVA_VSID_UNDEFINED_V vsid. */
    for( index = 0; index < TVA_MAX_NUM_OF_SESSIONS_V; ++index )
    {
      session_obj = tva_session_obj_list[index];
  
      if( ( session_obj->vsid == cmd_params->vsid ) ||
          ( session_obj->vsid == TVA_VSID_UNDEFINED_V ) )
      {
        break;
      }
      else
      {
        session_obj = NULL;
      }
    }
  
    if( session_obj != NULL )
    {
      session_obj->va_session_context = cmd_params->session_context;
      session_obj->va_tva_event_cb = cmd_params->event_cb;
      session_obj->vsid = cmd_params->vsid;
      ( void ) tva_vs_open_session( session_obj );
  
      *(cmd_params->ret_handle) = session_obj->header.handle;
    }
    else
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "tva_resource_cmd_register_proc(): Cannot find  TVA session instance" );
    }
  
    TVA_RELEASE_LOCK ( tva_global_lock );
  
    break;
  }

  return rc;
}


static uint32_t tva_resource_cmd_deregister_proc ( 
  void* params,
  uint32_t size
)
{
  uint32_t rc = APR_EOK;
  tva_session_object_t* session_obj = NULL;
  tva_iresource_cmd_deregister_t* cmd_params = NULL;
  
  for ( ;; )
  {
    if ( size != sizeof ( tva_iresource_cmd_deregister_t ) )
    {
      TVA_REPORT_FATAL_ON_ERROR ( APR_EBADPARAM );
      rc = APR_EBADPARAM;
      break;
    }
  
    cmd_params = ( tva_iresource_cmd_deregister_t* ) params;
    if( cmd_params == NULL )
    {
      TVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      rc = APR_EBADPARAM;
      break;
    }
  
    rc = tva_get_object ( cmd_params->handle, ( tva_object_t** )&session_obj );
    if( session_obj == NULL )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "tva_resource_cmd_deregister_proc(): Invalid handle=0X%08X, "
             "rc=0X%08X", cmd_params->handle, rc );
      break;
    }
  
    TVA_ACQUIRE_LOCK ( tva_global_lock );
  
    session_obj->vsid = TVA_VSID_UNDEFINED_V;
    session_obj->va_session_context = NULL;
    session_obj->va_tva_event_cb = NULL;
    ( void ) tva_vs_close_session( session_obj, NULL );
  
    TVA_RELEASE_LOCK ( tva_global_lock );
  
    break;
  }

  return rc;
}

/****************************************************************************
 * POWER UP/DOWN ROUTINES                                                    *
 ****************************************************************************/

static int32_t tva_init ( void )
{
  uint32_t rc = APR_EOK;
  uint32_t index;
  RCINIT_INFO info_handle = NULL;
  RCINIT_PRIO priority = 0;
  unsigned long stack_size = 0;

  MSG_2( MSG_SSID_DFLT, MSG_LEGACY_LOW,
         "tva_init(): Date %s Time %s", __DATE__, __TIME__ );

  {  /* Initialize the locks. */
    rc = apr_lock_create( APR_LOCK_TYPE_INTERRUPT, &tva_int_lock );
    rc = apr_lock_create( APR_LOCK_TYPE_MUTEX, &tva_thread_lock );
    apr_event_create( &tva_control_event );
  }

  { /* Initialize the custom heap. */
    apr_memmgr_init_heap( &tva_heapmgr, ( ( void* ) &tva_heap_pool ),
                          sizeof( tva_heap_pool ), NULL, NULL );
  }

  { /* Initialize the object manager. */
    apr_objmgr_setup_params_t params;
  
    params.table = tva_object_table;
    params.total_bits = TVA_HANDLE_TOTAL_BITS_V;
    params.index_bits = TVA_HANDLE_INDEX_BITS_V;
    params.lock_fn = tva_int_lock_fn;
    params.unlock_fn = tva_int_unlock_fn;
    rc = apr_objmgr_construct( &tva_objmgr, &params );
  }

  { /* Initialize free and nongating work pkt queues. */
    rc = apr_list_init_v2( &tva_free_work_pkt_q, 
                           tva_int_lock_fn, tva_int_unlock_fn );
    for ( index = 0; index < TVA_NUM_WORK_PKTS_V; ++index )
    {
      ( void ) apr_list_init_node( ( apr_list_node_t* ) &tva_work_pkts[index] );
      tva_work_pkts[index].pkt_type = TVA_WORK_ITEM_PKT_TYPE_NONE;
      tva_work_pkts[index].packet = NULL;
      rc = apr_list_add_tail( &tva_free_work_pkt_q,
                              ( ( apr_list_node_t* ) &tva_work_pkts[index] ) );
    }

    rc = apr_list_init_v2( &tva_nongating_work_pkt_q,
                           tva_int_lock_fn, tva_int_unlock_fn );
  }

  { /* Initialize gating work pkt queue. */
    rc = tva_gating_control_init( &tva_gating_work_pkt_q );
  }

  { /* Initialize the global session lock. */
    rc = apr_lock_create( APR_LOCK_TYPE_MUTEX, &tva_global_lock );
    TVA_PANIC_ON_ERROR( rc );
  }

  { /* Create the GVA task worker thread. */
    info_handle = rcinit_lookup( TVA_TASK_NAME );
    if ( info_handle == NULL ) 
    {
      /* Use the default priority & stack_size*/
      MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,
           "tva_init(): TVA task not registered with RCINIT" );
      priority = TVA_TASK_PRIORITY;
      stack_size = TVA_TASK_STACK_SIZE;
    }
    else
    {
      priority = rcinit_lookup_prio_info( info_handle );
      stack_size = rcinit_lookup_stksz_info( info_handle );
    }

    if ( ( priority > 255 ) || ( stack_size == 0 ) ) 
    {
      ERR_FATAL( "tva_init(): Invalid priority: %d or stack size: %d",
                 priority, stack_size, 0 );
    }

    rc = apr_thread_create( &tva_thread, TVA_TASK_NAME, TASK_PRIORITY(priority),
                            tva_task_stack, stack_size, 
                            tva_worker_thread_fn , NULL );
    TVA_PANIC_ON_ERROR( rc );

    apr_event_wait( tva_control_event );
  }

  tva_is_initialized = TRUE;

  return rc;
}


static int32_t tva_postinit ( void )
{
  uint32_t rc = APR_EOK;
  uint32_t index =0;
  
  /* Initialize the mapping info and open onex voice instance. */
  for( index = 0; index < TVA_MAX_NUM_OF_SESSIONS_V; ++index )
  {
    /* Create and Initialize modem subscription object. */
    rc = tva_create_modem_subs_object( &tva_subs_obj_list[index] );;
    TVA_PANIC_ON_ERROR (rc);
  
    /* Open CDMA voice session instance. */
    tva_subs_obj_list[index]->asid = ( sys_modem_as_id_e_type ) index;
    rc = tva_tdscdma_open_session( tva_subs_obj_list[index] );
    TVA_PANIC_ON_ERROR (rc);
  
    /* Create and initialize TVA session object. */
    rc =  tva_create_session_object ( &tva_session_obj_list[index] );
    TVA_PANIC_ON_ERROR( rc );
  }

  return rc;
}


static int32_t tva_predeinit ( void )
{
  uint32_t rc = APR_EOK;
  uint32_t index =0;
  
  /* Close onex Session instance. */
  for( index = 0; index < TVA_MAX_NUM_OF_SESSIONS_V; ++index )
  {
    /* TVA CLOSE's VS session instance for all available VSID. */
    ( void ) tva_vs_close_session( tva_session_obj_list[index], NULL );
  
    /* TVA CLOSE's CDMA session instance for all available ASID. */
    ( void ) tva_tdscdma_close_session( tva_subs_obj_list[index] );
  
    /* Free TVA session object for all VSID. */
    ( void ) apr_timer_destroy ( tva_session_obj_list[index]->sfg_timer );
    ( void ) apr_lock_destroy( tva_session_obj_list[index]->data_lock );
    ( void ) tva_mem_free_object( (tva_object_t*)tva_session_obj_list[index] );
    
    /* Free TVA subscription object for all ASID. */
    ( void ) tva_mem_free_object( (tva_object_t*)tva_subs_obj_list[index] );
  }


  return rc;
}


static int32_t tva_deinit ( void )
{
  uint32_t rc = APR_EOK;

  tva_is_initialized = FALSE;

  apr_event_signal_abortall( tva_work_event );
  apr_event_wait( tva_control_event );


  /* Release gating control structures */
  ( void ) tva_gating_control_destroy( &tva_gating_work_pkt_q );

  /* Release work queue */
  ( void ) apr_list_destroy( &tva_free_work_pkt_q );
  ( void ) apr_list_destroy( &tva_nongating_work_pkt_q );


  /* Deinitialize the object handle table. */
  ( void ) apr_objmgr_destruct( &tva_objmgr );

  /* Deinitialize basic OS resources for staging the setup. */
  ( void ) apr_event_destroy( tva_control_event );
  ( void ) apr_lock_destroy( tva_int_lock );
  ( void ) apr_lock_destroy( tva_thread_lock );
  ( void ) apr_lock_destroy( tva_global_lock );

  return rc;
}


TVA_EXTERNAL uint32_t tva_call (
  uint32_t cmd_id,
  void* params,
  uint32_t size
)
{
  uint32_t rc;

  switch ( cmd_id )
  {
  case DRV_CMDID_INIT:
    rc = tva_init( );
    break;

  case DRV_CMDID_POSTINIT:
    rc = tva_postinit( );
    break;

  case DRV_CMDID_PREDEINIT:
    rc = tva_predeinit( );
    break;

  case DRV_CMDID_DEINIT:
    rc = tva_deinit( );
    break;

  case TVA_IRESOURCE_CMD_REGISTER:
    rc =  tva_resource_cmd_register_proc( params, size );
    break;

  case TVA_IRESOURCE_CMD_DEREGISTER:
    rc =  tva_resource_cmd_deregister_proc( params, size );
    break;

  case TVA_IRESOURCE_CMD_GRANT:
    rc = tva_prepare_and_dispatch_cmd_packet ( cmd_id, params,  size );
    break;

  case TVA_IRESOURCE_CMD_REVOKE:
    rc = tva_prepare_and_dispatch_cmd_packet ( cmd_id, params,  size );
    break;

  case TVA_ICOMMON_CMD_SET_ASID_VSID_MAPPING:
    rc = tva_prepare_and_dispatch_cmd_packet ( cmd_id, params,  size );
    break;

  default:
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "tva_call(): Unsupported cmd ID (0x%08x)", cmd_id );
    rc = APR_EUNSUPPORTED;
    break;
  }

  return rc;
}

