#ifndef __IVA_I_H__
#define __IVA_I_H__

/**
  @file  iva_i.h
  @brief This file contains internal definitions of IMS voice adapter.
*/

/*
  ============================================================================

   Copyright (C) 2015 Qualcomm Technologies, Inc.
   All Rights Reserved.
   Confidential and Proprietary - Qualcomm Technologies, Inc.

  ============================================================================

                             Edit History

  $Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/avs/vsd/vadapter/inc/private/iva_i.h#1 $
  $Author: mplcsds1 $

  when      who   what, where, why
  --------  ---   ------------------------------------------------------------

  ============================================================================
*/

/****************************************************************************
  Include files for Module
****************************************************************************/

#include "sys.h"
#include "mmdefs.h"
#include "apr_list.h"


/****************************************************************************
  IVA DEFINES
****************************************************************************/

/* Defined 500 bytes Considering max 3 session/subscriptions .
 *
 * Note: one session consistes of one iva_session_object_t
 */
#define IVA_HEAP_SIZE_V ( 500 )

/* Size of VS work packet queue. */
#define IVA_NUM_WORK_PKTS_V ( 10 )

#define IVA_HANDLE_TOTAL_BITS_V ( 16 )
#define IVA_HANDLE_INDEX_BITS_V ( 3 ) /**< 3 bits = 8 handles. */
#define IVA_MAX_OBJECTS_V ( 1 << IVA_HANDLE_INDEX_BITS_V )

#define IVA_PANIC_ON_ERROR( rc ) \
  { if ( rc ) { ERR_FATAL( "Error[0x%08x], iva_state=%d", \
                            rc, iva_is_initialized, 0 ); } }

#define IVA_REPORT_FATAL_ON_ERROR( rc ) \
  { if ( rc ) { MSG_2( MSG_SSID_DFLT, MSG_LEGACY_FATAL, "Error[0x%08x], "\
                       "iva_state=%d", rc, iva_is_initialized ); } }

#define IVA_ACQUIRE_LOCK( lock ) \
  apr_lock_enter( lock );

#define IVA_RELEASE_LOCK( lock ) \
  apr_lock_leave( lock );


/* Defined for Error hanlding. */
#define IVA_VSID_UNDEFINED_V (0xFFFFFFFF)


/****************************************************************************
  IVA thread states
****************************************************************************/

typedef enum iva_thread_state_enum_t
{
  IVA_THREAD_STATE_ENUM_INIT,
  IVA_THREAD_STATE_ENUM_READY,
  IVA_THREAD_STATE_ENUM_EXIT
}
  iva_thread_state_enum_t;

/***************************************************************************
  IVA WORK REQUEST ITEM DEFINITIONS                                        *
****************************************************************************/

typedef struct iva_cmd_packet_t {
  
  uint32_t cmd_id;
    /**< Command id issued from client. */
  void* params;
    /**< Structure associated to each cmd_id. */
} iva_cmd_packet_t;


typedef struct iva_work_item_t {

  apr_list_node_t link;

  iva_cmd_packet_t* packet;

} iva_work_item_t;

/****************************************************************************
   THE COMMON OBJECT DEFINITIONS
****************************************************************************/

typedef enum iva_object_type_enum_t
{
   IVA_OBJECT_TYPE_ENUM_UNINITIALIZED,
   IVA_OBJECT_TYPE_ENUM_SESSION,
   IVA_OBJECT_TYPE_ENUM_INVALID

} iva_object_type_enum_t;

typedef struct iva_object_header_t
{
  uint32_t handle;
   /**< The handle to the associated apr_objmgr_object_t instance. */
  iva_object_type_enum_t type;
   /**<
    * The object type defines the actual derived object.
    *
    * The derived object can be any custom object type. A session or a
    * command are two such custom object types. A free object entry is set
    * to VS_OBJECT_TYPE_ENUM_FREE.
    */
} iva_object_header_t;


/****************************************************************************
  THE SESSION OBJECT
****************************************************************************/

typedef struct iva_session_object_t iva_session_object_t;

struct iva_session_object_t {

  iva_object_header_t header;

  uint32_t vsid;
  /**< System level published/documented Voice System ID. */

  bool_t is_resource_granted;
    /**< Indicates that vocoder resource access is granted. */

  iva_icommon_event_callback_fn_t va_event_cb;
    /**<
      * Callback function registered by voice agent to recieve voice resource
      * events.
      */

  void* va_session_context;
    /**< Session context provided by voice agent during 
      *  IVA_IRESOURCE_CMD_REGISTER.
      */

  bool_t is_ims_closed;
   /**< Indicates whether IMS has closed the opened session or not. */

  iva_icommon_event_callback_fn_t ims_event_cb;
    /**<
      * Callback function registered by IMS to recieve voice resource
      * events.
      */

  void* ims_session_context;
    /**< Session context provided by IMS during IVA_IVOICE_CMD_OPEN. */

};

/****************************************************************************
  THE GENERIC IVA OBJECT
****************************************************************************/

typedef union iva_object_t {

  iva_object_header_t header;
  iva_session_object_t session_obj;

} iva_object_t;


/****************************************************************************
 * GVA INTERNAL ROUTINES                                                    *
 ****************************************************************************/

IVA_INTERNAL uint32_t iva_free_cmd_packet (
  iva_cmd_packet_t* cmd_packet
);

IVA_INTERNAL uint32_t iva_prepare_and_dispatch_cmd_packet (
  uint32_t cmd_id,
  void* params,
  uint32_t size
);

IVA_INTERNAL void iva_signal_run ( 
  void
);

#endif  /* __IVA_I_H__ */
