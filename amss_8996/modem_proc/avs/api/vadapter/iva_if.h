#ifndef __IVA_IF_H__
#define __IVA_IF_H__

/**
  @file  iva_if.h
  @brief This is the public header file that clients of IVA should include.
         This file includes all other IVA public header files and contains
         single entry point into the IVA.
*/

/*
  ============================================================================

   Copyright (C) 2015 Qualcomm Technologies, Inc.
   All Rights Reserved.
   Confidential and Proprietary - Qualcomm Technologies, Inc.

  ============================================================================

                             Edit History

  $Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/avs/api/vadapter/iva_if.h#1 $
  $Author: mplcsds1 $

  when      who   what, where, why
  --------  ---   ------------------------------------------------------------


  ============================================================================
*/


/*----------------------------------------------------------------------------
  Include files for Module
----------------------------------------------------------------------------*/

#include "iva_icommon_if.h"
#include "iva_ivoice_if.h"


/*----------------------------------------------------------------------------
  IVA Call function.
----------------------------------------------------------------------------*/

/**
 * Provides a single API entry point into the IMS Voice Adapter.
 *
 * @param[in] event_id event identifier to execute.
 * @param[in] params event parameters.
 * @param[in] size Size of the event parameters in bytes.V
 *
 * @return
 * None
 */
IVA_EXTERNAL uint32_t iva_call (

  uint32_t cmd_id,
  void* params,
  uint32_t size

);

#endif /* __IVA_IF_H__ */

