#===============================================================================
#
# MPLM Common 
#
# GENERAL DESCRIPTION
#    Build scirpt to generate MPLM common/shared library.
#
# Copyright (c) 2014 by Qualcomm Technologies, Incorporated.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#
#-------------------------------------------------------------------------------
#
#
#
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
# 03/13/15    sz      Changed MPLM priority to the dedicated priority alias.
# 02/27/15    sz      Removed ECS LPR. 
# 12/02/14    sz      Compile based on feature flag.
# 05/14/14    sz      Ported from Bolt MCPM.
#===============================================================================

Import('env')
env = env.Clone()

from glob import glob
from os.path import join, basename

#-------------------------------------------------------------------------------
# Setup source PATH
#-------------------------------------------------------------------------------
SRCPATH = "../src"

env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0)


#-------------------------------------------------------------------------------
# QDSS Tracer definitions
#-------------------------------------------------------------------------------
if 'QDSS_TRACER_SWE' in env:
  env.SWEBuilder(['${BUILDPATH}/mplm_tracer_evt_ids.h'], None)
  env.Append(CPPPATH = ['${BUILD_ROOT}/mpower/mplm/common/build/$BUILDPATH'])

#-------------------------------------------------------------------------------
# Necessary Public and Restricted API's
#-------------------------------------------------------------------------------
env.RequirePublicApi([
    'HWENGINES',
    'DEBUGTOOLS',
    'SERVICES',
    'SYSTEMDRIVERS',
    'DAL',
    'POWER',
    'BUSES',
    'MPROC',
    'KERNEL',                             # needs to be last 
    ], area='core')

# Need to get access to Modem Public headers
env.RequirePublicApi([
    'ONEX',
    'GPS',      
    'HDR',      
    'MCS',
    'MMODE',    
    'UTILS',
    'RFA',      
    'UIM',      
    'GERAN',
    'FW',
    ])

# Need get access to Modem Restricted headers
env.RequireRestrictedApi([
    'MMODE',    
    'NAS',      
    'RFA',      
    'MDSP',     
    'MDSPSVC',  
    'GERAN',    
    'GPS',      
    'ONEX',     
    'HDR',      
    'UTILS',
    'MCS',
    'FW',
    ])


#-------------------------------------------------------------------------------
# Generate the library and add to an image
#-------------------------------------------------------------------------------

# Construct the list of source files by looking for *.c based on feature flag.
if 'USES_CUST_1' in env:
  MPLM_CMN_SOURCES = [
    '${BUILDPATH}/mplm_custom.c',
    '${BUILDPATH}/mplm_ecs.c',
    '${BUILDPATH}/mplm_devinfo.c',
    '${BUILDPATH}/mplm_mcpm.c',
    '${BUILDPATH}/mplm_nv_cfg.c',
    '${BUILDPATH}/mplm_utils.c',
    ]
else:
  MPLM_CMN_SOURCES = [
    '${BUILDPATH}/mplm.c',
    '${BUILDPATH}/mplm_devinfo.c',
    '${BUILDPATH}/mplm_mcpm.c',
    '${BUILDPATH}/mplm_nv_cfg.c',	
    '${BUILDPATH}/mplm_utils.c',
    ]

# Compile sources and convert to a binary library
env.AddBinaryLibrary(['MODEM_MODEM'], '${BUILDPATH}/mpower_mplm_cmn', MPLM_CMN_SOURCES)

#-------------------------------------------------------------------------------
# Set MSG_BT_SSID_DFLT for MPOWER MSG macros
#-------------------------------------------------------------------------------
env.Append(CPPDEFINES = [
   "MSG_BT_SSID_DFLT=MSG_SSID_MPOWER_MPLM",
])


#-------------------------------------------------------------------------------
#  RC INIT
#-------------------------------------------------------------------------------
RCINIT_IMG = ['MODEM_MODEM', 'CORE_QDSP6_SW']

# RC Init Function Dictionary
RCINIT_INIT_MPLM = {
    'sequence_group'             : 'RCINIT_GROUP_3',    # required
    'init_name'                  : 'mplm',              # required
    'init_function'              : 'MPLM_Init',         # required
    }
  
# RC Init Task Dictionary
RCINIT_TASK_MPLM = {
    'sequence_group'             : 'RCINIT_GROUP_3',
    'thread_name'                : 'mplm_task',
    'thread_entry'               : 'MPLM_Task',
    'thread_type'                : 'RCINIT_TASK_QURTTASK',
    'priority_amss_order'        : 'MPLM_TASK_PRI_ORDER',
    'stack_size_bytes'           : '4096',
    }
    
if 'USES_RCINIT' in env:
      env.AddRCInitFunc(RCINIT_IMG, RCINIT_INIT_MPLM)
      env.AddRCInitTask(RCINIT_IMG, RCINIT_TASK_MPLM)
