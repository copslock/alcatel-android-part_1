/*!
  @file
  rf_cdma_auto_pin.c

  @brief
  Provides CDMA Auto Pin functionality

*/

/*===========================================================================

Copyright (c) 2008 -2014 by Qualcomm Technologies, Inc.  All Rights Reserved.

                           EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$DateTime: 2016/03/28 23:06:37 $ $Author: mplcsds1 $
$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/rftech_cdma/common/rf/src/rf_cdma_auto_pin.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
08/31/15   wwl     Add autopin c2k state machine support
06/26/15   wwl     Initial version.

============================================================================*/
#include "rflm_c2k_msg.h"
#include "rf_cdma_data.h"
#include "rf_task.h"
#include "rf_cdma_tx_agc.h"
/*
typedef struct
{
  boolean tx_enabled;
  boolean pin_data_valid;
  uint32  wakeup_flag;
} rfcdma_autopin_state_type;

static rfcdma_autopin_state_type autopin_st[RFM_MAX_DEVICES];*/

/*----------------------------------------------------------------------------*/
/*!
  @brief
  Auto Pin update Handler function

  @details
  This function will be called by RF APPS task dispatcher when auto pin
  command is posted to the command Q beloging to RF APPS task. This function
  will perform Linearizer update and applies Pin value

  @param cmd_ptr 
  pointer to RFLM command buffer

  @param cid_info
  command ID info

  @param cb_data
  data buffer that carries command content
*/
void
rf_cdma_auto_pin_update_handler
(
  void *cmd_ptr,
  rf_dispatch_cid_info_type *cid_info,
  void *cb_data
)
{
  rfm_device_enum_type device = RFM_INVALID_DEVICE;
  rflm_c2k_trigger_autopin *auto_pin_data = NULL;
  int16 pin_comp_offset;
  rfm_device_enum_type dev;
  rf_cdma_data_status_type *dev_status = NULL;

  /* Sanity - check if data ptr from MSGR is valid */
  if(cmd_ptr != NULL)
  {
    /* Get the payload from MSGR cmd_ptr */
    auto_pin_data = (rflm_c2k_trigger_autopin *)
                      ((rf_cmd_type *)cmd_ptr)->payload;
    pin_comp_offset = auto_pin_data->pin;

    /* find the right device */
    for(dev = RFM_DEVICE_0; dev < RFM_MAX_WAN_DEVICES; dev++)
    {
      dev_status = rf_cdma_get_mutable_device_status( dev );

      if(NULL != dev_status)
      {
        if(auto_pin_data->tx_handle == dev_status->txlm_handle)
        {
          device = dev;
          break;
        }
      }
    }

    RF_MSG_5( RF_HIGH, "rf_cdma_auto_pin_update_handler: "
              "rflm handle on tx_handle = %d, pa_state %d, pin %d"
              " device %d calstate %d",
              auto_pin_data->tx_handle,
              auto_pin_data->pa_state,
              auto_pin_data->pin, device, rfm_get_calibration_state());

    if(NULL != dev_status)
    {
      if(dev_status->autopin_st.tx_enabled)
      {
        dev_status->autopin_st.pin_data_valid = TRUE;
      }
      rf_cdma_tx_agc_autopin_update(device, pin_comp_offset,
                                    auto_pin_data->pa_state );
    }
  }
  else
  {
    RF_MSG(RF_ERROR, "rf_cdma_auto_pin_update_handler: NULL MSGR data ptr!");
    return;
  }
} /* rf_cdma_auto_pin_update_handler */

/*----------------------------------------------------------------------------*/
/*!
  @brief
  Init C2K autopin module
*/
void
rf_cdma_auto_pin_cmd_proc_init
(
  void
)
{
  rfm_device_enum_type dev;
  rf_cdma_data_status_type *dev_status = NULL;

  for(dev = RFM_DEVICE_0; dev < RFM_MAX_DEVICES; dev++)
  {
    dev_status = rf_cdma_get_mutable_device_status( dev );
    if(NULL != dev_status)
    {
      dev_status->autopin_st.tx_enabled = FALSE;
      dev_status->autopin_st.pin_data_valid = FALSE;
      dev_status->autopin_st.wakeup_flag = 0;
    }
  }
}

/*----------------------------------------------------------------------------*/
/*!
  @brief
  Update internal C2K autopin state when TX is enabled

  @param tx_dev
  TX device
*/
void
rf_cdma_auto_pin_tx_wakeup
(
  rfm_device_enum_type tx_dev,
  rfm_mode_enum_type rfm_tech
)
{
  rf_cdma_data_status_type *dev_status = NULL;
  dev_status = rf_cdma_get_mutable_device_status( tx_dev );

  if(NULL != dev_status)
  {
    if(0 == dev_status->autopin_st.wakeup_flag)
    {
      /* if this is the first wakeup on the device */
      dev_status->autopin_st.tx_enabled = TRUE;
      dev_status->autopin_st.pin_data_valid = FALSE;
    }
    dev_status->autopin_st.wakeup_flag |= (1 << rfm_tech);
  }
}

/*----------------------------------------------------------------------------*/
/*!
  @brief
  Update internal C2K autopin state when TX is being put into sleep

  @param tx_dev
  TX device
*/
void
rf_cdma_auto_pin_tx_sleep
(
  rfm_device_enum_type tx_dev,
  rfm_mode_enum_type rfm_tech
)
{
  rf_cdma_data_status_type *dev_status = NULL;
  dev_status = rf_cdma_get_mutable_device_status( tx_dev );

  if(NULL != dev_status)
  {
    dev_status->autopin_st.wakeup_flag &= ~(1 << rfm_tech);

    if(0 == dev_status->autopin_st.wakeup_flag)
    {
      /* no tx for both 1X and HDR */
      dev_status->autopin_st.tx_enabled = FALSE;
      dev_status->autopin_st.pin_data_valid = FALSE;
    }
  }
}

/*----------------------------------------------------------------------------*/
/*!
  @brief
  Check to see if there is valid Pin value from autopin module

  @param tx_dev
  Auto Pin TX device
*/
boolean
rf_cdma_auto_pin_data_valid
(
  rfm_device_enum_type tx_dev
)
{
  boolean ret = FALSE;
  rf_cdma_data_status_type *dev_status = NULL;
  dev_status = rf_cdma_get_mutable_device_status( tx_dev );

  if(NULL != dev_status)
  {
    ret = dev_status->autopin_st.pin_data_valid;
  }
  return ret;
}

/*----------------------------------------------------------------------------*/
/*!
  @brief
  Get current autopin tx state for given TX dev

  @param tx_dev
  Autopin tx device
*/
boolean
rf_cdma_auto_pin_tx_state
(
  rfm_device_enum_type tx_dev
)
{
  boolean ret = FALSE;
  rf_cdma_data_status_type *dev_status = NULL;
  dev_status = rf_cdma_get_mutable_device_status( tx_dev );

  if(NULL != dev_status)
  {
    ret = dev_status->autopin_st.tx_enabled;
  }
 
  return ret;
}
