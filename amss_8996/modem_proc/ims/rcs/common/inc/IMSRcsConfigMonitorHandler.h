/******************************************************************************************
File: ImsRcsConfigMonitorHandler.h
Description: Declaration of ImsRcsConfigMonitorHandler class that handles 
 
$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/ims/rcs/common/inc/IMSRcsConfigMonitorHandler.h#1 $
$DateTime: 2016/03/28 23:03:22 $
$Author: mplcsds1 $

 Revision History
==========================================================================================
   Date    |   Author's Name    |  CR#   |  Review ID  |      Change Description
==========================================================================================
07-11-2012    chanchal              -          -         initial version
********************************************************************************************/
#ifndef __IMSRCSCONFIG_MONITOR_HANDLER_H__
#define __IMSRCSCONFIG_MONITOR_HANDLER_H__

#include <qpdpl.h>
#include <qpdcm.h>
#include <qpnet.h>
#include <networkBasehandler.h>

#define QP_RCS_CONFIG_IPC_DELIMETER "##"

typedef enum 
{
  QRCS_HTTPS_REQUEST_SENT = 1,
  QRCS_HTTPS_SUCCESSRESPONSE_RECEIVED,
  QRCS_HTTPS_FAILURE_RESPONSE_RECEIVED,
  QRCS_NVGROUP_DATA,
  QRCS_OMADAM_DATA
} QRCS_CONFIG_IPC_TYPE;

typedef struct 
{
  QPUINT16 iIPCType;
  QPUINT16 iNVGroup_ResponseCode;
  QPUINT16 iDataLength;
  QPCHAR  *pDataBuffer;
}QRCS_CONFIG_IPC ;

#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus

/************************************************************************************
  ImsRcsConfigMonitorHandler class
 ************************************************************************************/
class ImsRcsConfigMonitorHandler
{
  public:
    /* Constructor        
    */
    ImsRcsConfigMonitorHandler();
    
    /* Destructor */
    ~ImsRcsConfigMonitorHandler();

   // Instance of a class
    static ImsRcsConfigMonitorHandler* getInstance();
    static QPVOID delInstance();

    
    /* Method to init the ImsRcsConfigMonitorHandler module. Must be called before using the module 
       Paramaters: None
    */
    QPBOOL Init();
    
    /* Method to reset the ImsRcsConfigMonitorHandler module. This API is callaed so that the same instance of the module
       can be reused instead of deleting and creating a new instance
       Paramaters: None
    */
    QPBOOL DeInit();
    
    /* Method to send updates to the registered clients. This is achieved by sending IPC
       Paramaters: pIpcMessage[IN]: IPC Message to be sent
    */    

#if 1
    QPBOOL UpdateRegisteredClients(QRCS_CONFIG_IPC *pConfigIpc);
    QPBOOL SendIpcToAllRegisteredClients(const QPCHAR* n_pIpcMessage, QPUINT16 n_iMsgLength);
#else
    QPBOOL UpdateRegisteredClients(const QPCHAR* n_pIpcMessage, QPUINT16 n_iMsgLength);
#endif
    
   QPVOID handleMessage(QPNET_CONN_PROFILE* pNetConnProfile, QPNET_REMOTE_ADDR* pAddr, QPVOID* pBuff);
    /*
      QPNET_CONN_PROFILE* pNetConnProfile, 
      QPNET_REMOTE_ADDR* pAddr, 
      QPVOID* pBuff, 
      QPVOID* pUserData
    */ 
   static QPVOID handleIPCMessage(QPNET_CONN_PROFILE* pNetConnProfile, QPNET_REMOTE_ADDR* pAddr, QPVOID* pBuff, QPVOID* pUserData);
   static QPVOID  RcsRecoveryCB(QPDPL_RECOVERY_CB_STRUCT* n_pRecoveryStruct, QPVOID* pUserData)  ;
   QPVOID  HandleRecoveryCase(const QPUINT16& n_iPortToBeCleaned);

  private:
    /* member funtions*/
    QPVOID handleSubRcsConfig(QPCHAR* n_pPacketBody, QPNET_REMOTE_ADDR* n_pRemoteAddress);
    QPVOID handleUnsubRcsConfig(QPCHAR* n_pPacketBody, QPNET_REMOTE_ADDR* n_pRemoteAddress);
    QPVOID handleRegistrationFailureStatus(QPCHAR* n_pPacketBody, QPNET_REMOTE_ADDR* n_pRemoteAddress);
   QPVOID handleRejectConfigStatus(QPCHAR* n_pPacketBody, QPNET_REMOTE_ADDR* n_pRemoteAddress) ;

    QPVOID processNewSubscription();
  
    /* Data Members */
    // Structure of IPC subscribed listeners
    struct RcsConfigSubscribedNode
    {
      QPUINT16 iRemotePort;
      RcsConfigSubscribedNode* pNext;
    };

    RcsConfigSubscribedNode*    m_pRcsConfigSubscribeList; //List of subscribed listeners
    QPNET_CONN_PROFILE*         m_pConnProfile; // Connection profile (for IPC messages)
    //QPVOID*                   m_pHttpsEnabler; //Pointer to HttpsEnabler
    //ImsRcsConfigMgr*            m_pImsRcsConfigMgr;
    QPCHAR*                     m_pIpcMessage;
    QPUINT16                    m_iIpcMessageLength ;
    static ImsRcsConfigMonitorHandler*         m_pImsRcsConfigMonitorHandler;

    //copy constructor
    ImsRcsConfigMonitorHandler(const ImsRcsConfigMonitorHandler&); 
    //operator =
   ImsRcsConfigMonitorHandler& operator=(const ImsRcsConfigMonitorHandler &);
};

#ifdef __cplusplus
}
#endif // __cplusplus

#endif //__IMSRCSCONFIG_MONITOR_HANDLER_H__
