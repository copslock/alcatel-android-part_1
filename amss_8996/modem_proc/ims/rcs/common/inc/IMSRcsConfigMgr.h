/************************************************************************
 Copyright (C) 2013 Qualcomm Technologies Incorporation .All Rights Reserved.

 File Name      : IMSRcsConfigMgr.h
 Description    : Rcs Configuration Manager functions that transfers 
                  NV data
 Revision History
 ========================================================================
   Date      |   Author's Name    |  BugID  |        Change Description
 ========================================================================
 11-Nov-2013      Ramalakshmi             -                Initial Draft
************************************************************************/

#ifndef __IMSRCS_CONFIGMGR_H__
#define __IMSRCS_CONFIGMGR_H__

#include <qpPlatformConfig.h>
#include <qpdefines.h>
#include <qpdefinesCpp.h>
#include <qpConfigNVItem.h>
#include <IMSRcsConfigMonitorHandler.h>

#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus
    
class ImsRcsConfigMgr
{
  public:
    ImsRcsConfigMgr();
    virtual ~ImsRcsConfigMgr();
    // Instance of a class
    static ImsRcsConfigMgr* getInstance();
    static QPVOID delInstance();
    QPVOID handleNewSubscription();
   

  private:
    QPVOID ReadConfig();
    QPVOID SendNvGroupIPCMsg( QPUINT32 dwNvGroupType, QPCHAR* pMessage, QPUINT32 dwMessageLength);

    static ImsRcsConfigMgr*		     m_pImsRcsConfigMgr;
    QP_IMS_RCS_AUTO_CONFIG_ITEM*     m_pRCSAutoConfig;
    ImsRcsConfigMonitorHandler*      m_pImsRcsConfigMonitorHandler;

  
};

#ifdef __cplusplus
}
#endif // __cplusplus

/************************************************************************
Function rcs_configmgr_initialize()

Description
Initialize the session

Dependencies
None

Return Value
AEE_IMS_SUCCESS if the service has been initialized successfully for
update, otherwise AEE_IMS_EFAILED is returned

Side Effects
None
************************************************************************/
QPINT rcs_configmgr_initialize(QPVOID);

/************************************************************************
Function rcs_configmgr_uninitialize()

Description
Initialize the session

Dependencies
None

Return Value
AEE_IMS_SUCCESS if the service has been Un-initialized successfully ,
otherwise AEE_IMS_EFAILED is returned

Side Effects
None
************************************************************************/
QPVOID rcs_configmgr_uninitialize(QPVOID);


#endif //__IMSRCS_CONFIGMGR_H__
