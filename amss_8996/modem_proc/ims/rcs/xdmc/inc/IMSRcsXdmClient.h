/************************************************************************
 Copyright (C) 2011 Qualcomm Technologies Incorporation .All Rights Reserved.

 File Name      : IMSRcsXdmClient.h
 Description    : IMS XDM Client Enabler

 Revision History
 ========================================================================
   Date      |   Author's Name    |  BugID  |        Change Description
 ========================================================================

************************************************************************/

#ifndef __IMSRCSXDMCLIENT_H__
#define __IMSRCSXDMCLIENT_H__

/*----------------------------------------------------------------------------
 Header Includes
----------------------------------------------------------------------------*/
#include <qpPlatformConfig.h>
#include <EventListener.h>
#include <qpConfigNVItem.h>
#include <qpDplIPSecAKA.h>
#include <qpRegistrationMonitor.h>
#include <qpSipHeader.h>
#include <imsserviceconfig.h>


#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus

#define QP_RCS_XDMC_PUBLIC_ID_LEN   100
#define QP_RCS_XDMC_URL_LEN         512
#define QP_RCS_XDMC_FWDSLASH_STR    (QPCHAR*)"/"

#define QP_RCS_XDMC_COLON_STR       (QPCHAR*)":"
#define QP_RCS_XDMC_PERIOD_STR      (QPCHAR*)"."
#define QP_RCS_XDMC_ADDR_STR        (QPCHAR*)"&"
#define QP_RCS_XDMC_EQUAL_STR       (QPCHAR*)"="
#define QP_RCS_XDMC_URL_PRE         (QPCHAR*)"config"
#define QP_RCS_XDMC_URL_END         (QPCHAR*)"rcse"
#define QP_RCS_XDMC_URI_VERS        (QPCHAR*)"vers"

#define QP_RCS_XDMC_PRES_XDMS_AUID       (QPCHAR*)"org.openmobilealliance.pres-rules"
#define QP_RCS_XDMC_PRES_XDMS_DOC_NAME   (QPCHAR*)"pres-rules"
#define QP_RCS_XDMC_PRES_XDMS_DOC_TYPE   (QPCHAR*)"application/auth-policy+xml"

#define QP_RCS_XDMC_SHARED_XDMS_AUID     (QPCHAR*)"resource-lists"
#define QP_RCS_XDMC_SHARED_XDMS_DOC_NAME (QPCHAR*)"index"
#define QP_RCS_XDMC_SHARED_XDMS_DOC_TYPE (QPCHAR*)"application/resource-lists+xml"

#define QP_RCS_XDMC_RLS_XDMS_AUID        (QPCHAR*)"rls-services"
#define QP_RCS_XDMC_RLS_XDMS_DOC_NAME    (QPCHAR*)"index"
#define QP_RCS_XDMC_RLS_XDMS_DOC_TYPE    (QPCHAR*)"application/rls-services+xml"

#define QP_RCS_XDMC_TREE_ACCESS          (QPCHAR*)"users"

#define QP_RCS_XDMC_XCAP_DIRECTORY_AUID       (QPCHAR*)"org.openmobilealliance.xcap-directory"
#define QP_RCS_XDMC_XCAP_DIRECTORY_DOC_NAME   (QPCHAR*)"directory.xml"
#define QP_RCS_XDMC_XCAP_DIRECTORY_DOC_TYPE   (QPCHAR*)"application/vnd.oma.xcap-directory+xml"


#define QP_RCS_XDMC_ALWAYS_ON_PDN         0
#define QP_RCS_XDMC_ON_DEMAND_PDN         1

//5 mins
#define QP_RCS_XDMC_PDP_RELEASE_TIMER_VALUE    (5*60)

// 30 seconds
#define QP_RCS_XDMC_RESPONSE_WAIT_TIMER_VALUE    (30)

#define QP_RCS_XDMC_MAX_STR_LEN                  50
#define QP_RCS_XDMC_AT                          (QPCHAR*)"@"
#define QP_RCS_XDMC_DOMAIN_LABEL                 (QPCHAR*)"3gppnetwork.org"
#define QP_RCS_XDMC_XCAP_LABEL                   (QPCHAR*)"xcap"
#define QP_RCS_XDMC_INSERT_PUB                   (QPCHAR*)"pub"
#define QP_RCS_XDMC_BSF_LABEL                    (QPCHAR*)"bsf"
#define QP_RCS_XDMC_MNC                               (QPCHAR*)"mnc"
#define QP_RCS_XDMC_MCC                               (QPCHAR*)"mcc"
#define QP_RCS_XDMC_IMS_LABEL                         (QPCHAR*)"ims"

#define QP_RCS_XDMC_COOKIE_FILENAME (QPCHAR*)"RCSXdmcCookie.txt"



#define QP_RCS_XDMC_HYSTERESIS_TIMER_VALUE 120

typedef enum qpRcsXcapRetryTimerValueId
{
  QP_RCS_XDMC_RETRY_TIMER_FIRST_TRY = 0,
  QP_RCS_XDMC_RETRY_TIMER_30_SECONDS, 
  QP_RCS_XDMC_RETRY_TIMER_2_MINS,
  QP_RCS_XDMC_RETRY_TIMER_5_MINS,
  QP_RCS_XDMC_RETRY_TIMER_15_MINS,
  QP_RCS_XDMC_RETRY_TIMER_30_MINS,
  QP_RCS_XDMC_RETRY_TIMER_1_HOUR,
  QP_RCS_XDMC_RETRY_TIMER_2_HOURS,
  QP_RCS_XDMC_RETRY_TIMER_4_HOURS,
  QP_RCS_XDMC_RETRY_TIMER_8_HOURS,
  QP_RCS_XDMC_RETRY_TIMER_16_HOURS,
  QP_RCS_XDMC_RETRY_TIMER_24_HOURS,
  QP_RCS_XDMC_RETRY_TIMER_DAYS,
  QP_RCS_XDMC_RETRY_TIMER_MAX
}QPE_RCS_XCAP_RETRY_TIMER_VALUE_ID;

  const static struct
{
  QPE_RCS_XCAP_RETRY_TIMER_VALUE_ID RetryValueId;  
  QPUINT32                      iRetryValueInSeconds;
}gRcsXcapRetryValues[QP_RCS_XDMC_RETRY_TIMER_MAX] =
{
  {QP_RCS_XDMC_RETRY_TIMER_FIRST_TRY,   0},  
  {QP_RCS_XDMC_RETRY_TIMER_30_SECONDS,  30}, 
  {QP_RCS_XDMC_RETRY_TIMER_2_MINS,      2*60},
  {QP_RCS_XDMC_RETRY_TIMER_5_MINS,      5*60},
  {QP_RCS_XDMC_RETRY_TIMER_15_MINS,     15*60},
  {QP_RCS_XDMC_RETRY_TIMER_30_MINS,     30*60},
  {QP_RCS_XDMC_RETRY_TIMER_1_HOUR,      1*60*60},
  {QP_RCS_XDMC_RETRY_TIMER_2_HOURS,     2*60*60},
  {QP_RCS_XDMC_RETRY_TIMER_4_HOURS,     4*60*60},
  {QP_RCS_XDMC_RETRY_TIMER_8_HOURS,     8*60*60},
  {QP_RCS_XDMC_RETRY_TIMER_16_HOURS,    16*60*60},
  {QP_RCS_XDMC_RETRY_TIMER_24_HOURS,    24*60*60},
  {QP_RCS_XDMC_RETRY_TIMER_DAYS,        24*60*60}
};



typedef enum qpRcsXcapRequestType
{
  QP_RCS_XCAP_REQUEST_TYPE_NONE = 0,
  QP_RCS_XCAP_REQUEST_TYPE_PRESENCE_RULES_GET,
  QP_RCS_XCAP_REQUEST_TYPE_PRESENCE_RULES_PUT,
  QP_RCS_XCAP_REQUEST_TYPE_PRESENCE_RULES_DELETE,
  QP_RCS_XCAP_REQUEST_TYPE_DIRECTORY_GET,
  QP_RCS_XCAP_REQUEST_TYPE_MAX
}QPE_RCS_XCAP_REQUEST_TYPE;


/*----------------------------------------------------------------------------
 Forward Declarations
----------------------------------------------------------------------------*/
class IMSServiceConfig;


 // Timer Types
typedef enum qpTimerTypes
{     
  QPE_RCS_XDMC_RETRY_TIMER,
  QPE_RCS_XDMC_PDN_HYSTERESISI_TIMER,
  QPE_RCS_XDMC_PDP_RELEASE_TIMER,
  QPE_RCS_XDMC_RESPONSE_WAIT_TIMER
}QPE_RCS_XDMC_TIMERS_TYPES;


class XdmcEnabler : public EventListener,public RegisterServiceMonitor
{
public:
  // Instance of a class
  static XdmcEnabler* getInstance();
  static QPVOID delInstance();

  QPUINT32 XdmcEnablerInit();

  // from EventListener
  QPVOID update(EventObject& n_Ev);

  //RegMonitor APIs to notify Module about the SIM Status
  virtual QPVOID RegistrationStatus(eQpRegistrationStatus iRegistrationStatus);
  virtual QPVOID SipQosStatus(QPE_SIPQOS_STATUS iSipQosStatus);
  virtual QPVOID RegistrationCardStatus(QPBOOL n_iCardStatus);
  virtual QPVOID GenericUpdate(QPE_GENERIC_TASK_EVENT iGenericUpdate);
  virtual QPVOID ServiceAvailable( QPE_SERVICE_STATUS eServiceStatus, 
                                   QPCHAR* n_pFeatureTags, QPUINT16 iFeatureTagsLen);
  virtual QPVOID ServiceUnAvailable( QPE_REG_FAILURE_REASON iFailureReason, 
                                     QPUINT16 iStatusCode, QPCHAR* ReasonStr, QPUINT16 ReasonStrLen);
  virtual QPVOID ServiceAvailableWLAN(QPE_SERVICE_STATUS eServiceStatus, QPCHAR* n_pFeatureTags, QPUINT16 iFeatureTagsLen);
  virtual QPVOID ServiceUnAvailableWLAN( QPE_REG_FAILURE_REASON iFailureReason, 
                                         QPUINT16 iStatusCode, QPCHAR* ReasonStr, QPUINT16 ReasonStrLen);
  
  QPVOID ReadXdmcConfigNV();
  QPBOOL SetPresRulesXML( QPCHAR *pPresRulesXML, QPUINT32 iPresRulesXMLLength);
  QPBOOL ModifyPresRules();
  QPBOOL DeletePresRules();
  QPBOOL GetPresRules();
  static QPVOID OnModemNVChanged(QPE_IMS_CONFIG_ITEMS eNVGroup, QPVOID* pUserData);

private:
  //ctor
  XdmcEnabler();
  //dtor
  virtual ~XdmcEnabler();
  //copy constructor
  XdmcEnabler(const XdmcEnabler&); 
  //operator =
  XdmcEnabler& operator=(const XdmcEnabler &);

  QPVOID  triggerRequestPresRules(QPVOID);
  QPBOOL  sendRequestPresRules();    
  QPVOID  processTimerExpy(QPUINT32,QPINT);  
  QPVOID  handleHttpResponse(QPUINT32);
  QPVOID  constructURI();
  QPVOID  constructURL();    
  static  QPVOID xdmcTimerCallBack(QPINT, QPUINT32 ,QPVOID*);
  QPBOOL  setMyPresRulesXML();
  QPVOID  readSipPublicUri();
  QPBOOL  getXcapRootUri(QPCHAR *pXcapRootUri );
  
  QPVOID  startRetryMechanism();
  QPVOID  processHttpResponse();
  QPBOOL  validatePresRulesXML(QPCHAR* pXmlMsgBody, QPCHAR *pContentType);
  QPBOOL  setRequestParameters(QPE_RCS_XCAP_REQUEST_TYPE eXcapReqType, QPCHAR* pContent);
  QPBOOL  checkForCardChange();
  QPVOID  startPDNHysteresisTimer();
  QPVOID  startRetryTimer(QPUINT32 iTimerValue);
  QPVOID  deleteRetryTimer();
  QPVOID  deletePDNHysTimer();
  QPVOID  stopRetryTimer();
  QPVOID  stopPDNHysTimer();
  QPVOID  handleRATTechChange();
  QPVOID  xdmcCleanUp();
  QPVOID  deletePDPReleaseTimer();
  QPVOID  stopPDPReleaseTimer();
  QPVOID  startPDPReleaseTimer();
  QPBOOL  checkAndSendRequestPresRules();
  QPBOOL  qpXdmcConstructXCAPRoot(QPCHAR*, QPBOOL n_bURIType = QP_FALSE);
  QPBOOL  qpXdmcConstructDNfromIMSI(QPCHAR* pDomainName, QPUINT32 n_iLen, QPBOOL n_bURIType);
  QPVOID  updateDisableXdmcConfigNV(QPUINT8 iDisableXdmc);
  QPBOOL  validateXcapDirXML(QPCHAR* pXmlMsgBody, QPCHAR *pContentType);
  QPBOOL  findAUIDInXcapDirXML(QPCHAR* pXmlMsgBody, QPCHAR *pContentType);
  QPVOID  deleteResponseWaitTimer();
  QPVOID  stopResponseWaitTimer();
  QPVOID  startResponseWaitTimer();
  QPVOID  handleXdmcFailure();
  QPVOID  xdmcServiceConfigCleanUp();
  QPVOID  xdmcSimSwapCleanUp();


  QPCHAR*                     m_pUri;  
  QPDPL_IMSI_STRUCT*          m_pImsi;  
  IMSServiceConfig*           m_pIMSServiceConfig;  
  MafTimer*                   m_pXdmcRetryTimer;
  MafTimer*                   m_pXdmcPDNHysTimer;  
  MafTimer*                   m_pXdmcPDPReleaseTimer;  
  QPBOOL                      m_terminalReset;
  QPBOOL                      m_isCardReady;
  QPBOOL                      m_isCardChange;
  QPBOOL                      m_isPAssociatedURIChange;
  QPBOOL                      m_bPresRulesReqOngoingOnRATChange;
  QPBOOL                      m_bPendingPresRulesReq;
  static XdmcEnabler*         m_pXdmcEnabler;
  QPCHAR*                     m_pTxPresRulesXML;
  QPUINT32                    m_iPresRulesXMLLength;
  QPCHAR*                     m_pAssociatedUri;  
  QPCHAR*                     m_pPresRulesURL;
  QPUINT32                    m_currentTimerId;
  QPUINT32                    m_iCurrentXcapServerRetryCount;                  /* Retry counter after sending HTTP message */  
  QPE_RCS_XCAP_REQUEST_TYPE   m_eTxXcapReqType;
  QPCHAR*                     m_pTxXmlMsgBody;  
  QPCHAR*                     m_pRxXmlMsgBody;  
  QPCHAR*                     m_pRxContentType;
  QPUINT32                    m_ihttpStatusCode ;
  QP_IMS_SERVICE_CONFIG_NV    m_sImsSvcConfig;  
  QPUINT8                     m_iDisableXdmc;                                                   
  QPCHAR                      m_strUserAgent[QP_RCS_XDMC_USER_AGENT_LEN];
  QPCHAR                      m_strXCAPRootURI[QP_RCS_XDMC_CONFIG_ROOT_URI_LEN];  
  QPCHAR                      m_strXCAPAuthUserName[QP_RCS_XDMC_CONFIG_AUTH_USER_NAME_LEN];  
  QPCHAR                      m_strXCAPAuthSecret[QP_RCS_XDMC_CONFIG_AUTH_SECRET_LEN];  
  QPE_XCAP_AUTH_TYPE_CONFIG   m_eXCAPAuthType;
  QPUINT32                    m_iRevokeTimer;
  QPUINT8                     m_iPNBEnabled;
  QPUINT8                     m_iXDMDocChangesSubsEnabled;
  QPUINT16                    m_iXdmcPDNHysTimerValue;
  MafTimer*                   m_pResponseWaitTimer;
  QPBOOL                      m_iRegStatus; 
  QPUINT32                    m_iXdmcRatMaskValue;
  QPUINT8                     m_iBootConfigDisableXdmc;
};

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // __IMSRCSXDMCLIENT_H__