/************************************************************************
 Copyright (C) 2011 Qualcomm Technologies Incorporation .All Rights Reserved.

 File Name      : AutoConfigMgr.cpp
 Description    :

 Revision History
 ========================================================================
   Date      |   Author's Name    |  BugID  |        Change Description
 ========================================================================
 20-Jan-2013   Murali Anand                    Initial Version
 -------------------------------------------------------------------------------
 29-Apr-2013   Priyank              479646     Version -1 across the different 
                                               sim cards is not handled properly
 -------------------------------------------------------------------------------
 09-May-2013   Priyank              478986     Auto-config folder not being created in 
                                               EFS and file not getting stored
 -------------------------------------------------------------------------------
 20-05-13      Priyank              488364    When we insert a new sim card in the UE, UE is not able to differentiate 
                                              this as new sim card and sending the HTTP request with the version of 
                                              the old sim card instead of 0.0.0.0
-------------------------------------------------------------------------------
 31-05-13      Priyank              493893    After a successful APCS for SIM1, if APCS for SIM2 fails 
                                              then UE is using SIM1 specific config for SIM2 instead of 
                                              throwing "no valid config exist" error for SIM2
-------------------------------------------------------------------------------
08-07-13       Priyank              509611    Replacing DPL Memcpy with Memscpy (Related to FR 15386)                                                
-------------------------------------------------------------------------------
25-07-13      Priyank              494087    Incorrect behavior in "UE receives Validity -1 with SIM1 
                                              and then insert SIM2 and then insert back SIM1" usecase
************************************************************************/
#include <qpdpl.h>
#include "AutoConfigMgr.h"
#include <qpdefines.h>
#include <qpdefinesCpp.h>
#include <qplog.h>
#include <qpIO.h>
#include <qpConfigNVItem.h>
#include <ims_task.h>

// Singleton Instance of a class
AutoConfigMgr* AutoConfigMgr::m_pAutoConfigMgr = QP_NULL;

#define QP_AUTO_CONFIG_FILE            (QPCHAR*)"volte_auto_config.bin"
#define QP_KT_AUTO_CONFIG_FILE         (QPCHAR*)"kt_auto_config.bin"

/************************************************************************
Function AutoConfigMgr::GetInstance()

Description
Access method to get the singleton instance of the class

Dependencies
None

Return Value
Singleton Object

Side Effects
None
************************************************************************/
AutoConfigMgr* AutoConfigMgr::GetInstance() 
{
  if (m_pAutoConfigMgr == QP_NULL) 
  {
    IMS_NEW(AutoConfigMgr, MEM_IMS_HTTPS,m_pAutoConfigMgr, ());
    if ( QP_NULL == m_pAutoConfigMgr )
    {
      IMS_MSG_FATAL_0(LOG_IMS_FW,"AutoConfigMgr::GetInstance m_pAutoConfigMgr malloc failed");
      return QP_NULL;
    }
    m_pAutoConfigMgr->Init();
  }
  return m_pAutoConfigMgr;
}

/************************************************************************
Function AutoConfigMgr::DelInstance()

Description
Access method to delete the singleton instance of the class

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID AutoConfigMgr::DelInstance() 
{
  if (m_pAutoConfigMgr != QP_NULL) 
  {
    IMS_DELETE(m_pAutoConfigMgr,MEM_IMS_HTTPS);
    m_pAutoConfigMgr = QP_NULL;
  }
}
/************************************************************************
Function AutoConfigMgr::AutoConfigMgr()

Description
Constructor

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
AutoConfigMgr::AutoConfigMgr()
{
  m_pAutoConfigStruct = QP_NULL;
  m_pImsi = QP_NULL;
  m_iOprtMode = (QPINT8) QP_CONFIG_IMS_OPRT_UNKNOWN_MODE;
}

/************************************************************************
Function AutoConfigMgr::~AutoConfigMgr()

Description
Destructor

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
AutoConfigMgr::~AutoConfigMgr()
{
  if ( m_pAutoConfigStruct)
  {
    qpDplFree( MEM_IMS_HTTPS,m_pAutoConfigStruct);
  }
  if (m_pImsi )
  {
    qpDplFree( MEM_IMS_HTTPS,m_pImsi);
    m_pImsi = QP_NULL;
  }
}

/************************************************************************
Function AutoConfigMgr::Init()

Description
Initialization Function, read the configuration from EFS and update
the internal cache structure

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID AutoConfigMgr::Init()
{
  IMS_MSG_MED_0(LOG_IMS_FW,"AutoConfigMgr::Init | Enter");
  QPE_IO_ERROR fileIoError = QP_IO_ERROR_UNKNOWN;

  if ( !m_pAutoConfigStruct )
    m_pAutoConfigStruct = (QP_AUTO_CONFIG_STRUCT*)qpDplMalloc(MEM_IMS_HTTPS, sizeof(QP_AUTO_CONFIG_STRUCT));

  if(QP_NULL == m_pAutoConfigStruct)
  {
    IMS_MSG_FATAL_0(LOG_IMS_FW,"AutoConfigMgr::Init | Failure in m_pAutoConfigStruct malloc");
    return;
  }
  qpDplMemset(m_pAutoConfigStruct, 0, sizeof(QP_AUTO_CONFIG_STRUCT));

  /* Reading and storing the oprt mode */
  if(QP_IO_ERROR_OK != qpDplIODeviceGetItem(IMS_OPRT_MODE, (QPVOID*) &m_iOprtMode, sizeof(m_iOprtMode)))
  {
    IMS_MSG_LOW_0(LOG_IMS_FW,"AutoConfigMgr::Init -no oprt mode defined");
  }
  else
  {
    IMS_MSG_LOW_1(LOG_IMS_FW,"AutoConfigMgr::Init -oprt mode = %d", m_iOprtMode);
  }

  /* Check if the ims directory already exists; if not create it */
  fileIoError = qpDplIODeviceMKDIR(QIMF_AMSS_CONFIG_DIR) ;
  if ( QP_IO_ERROR_OK != fileIoError && QP_IO_ERROR_FILE_EXISTS != fileIoError )
  {
    IMS_MSG_FATAL_1(LOG_IMS_RCS_AUTO_CONFIG,"AutoConfigMgr::Init | Failure - creating /ims dir %d", fileIoError);
  }
  else
  {
    IMS_MSG_MED_1(LOG_IMS_RCS_AUTO_CONFIG,"AutoConfigMgr::Init | created or Already Exist /ims dir %d", fileIoError);
  }
  return;
}
/************************************************************************
Function AutoConfigMgr::readConfig()

Description
Read the Configuration Present In the file for KT

Dependencies
None

Return Value
QP_TRUE/QP_FALSE

Side Effects
None
************************************************************************/
QPBOOL AutoConfigMgr::readConfig( QPVOID )
{
  IMS_MSG_ERR_0(LOG_IMS_FW,"AutoConfigMgr::readConfig | Entered ");
  QPIO_CONN_PROFILE iFileProfile = {0, QP_IO_FFS, QP_NULL, QP_NULL, QP_NULL, QP_NULL};
  QPE_IO_ERROR iFileOpen         = QP_IO_ERROR_UNKNOWN; 
  QPINT32 iDataRead              = 0;
  QPBOOL  iResult                = QP_FALSE;
  QPCHAR fileName[50];

  (void)qpDplStrlcpy(fileName, QIMF_AMSS_CONFIG_DIR, sizeof(fileName));
  (void)qpDplStrlcat(fileName, "/", sizeof(fileName));
  (void)qpDplStrlcat(fileName,QP_AUTO_CONFIG_FILE , sizeof(fileName));

  /* After Card ready, read the values of IMSI */
  if(QP_FALSE == readIMSIValue())
  {
    return QP_FALSE;
  }

  while (1)
  {
    iFileOpen = qpDplIODeviceOpen(&iFileProfile, fileName, QP_IO_OPEN_READ_BINARY, QP_NULL);
    if (QP_IO_ERROR_OK != iFileOpen)
    {
      IMS_MSG_ERR_1_STR(LOG_IMS_FW,"AutoConfigMgr::readConfig - Unable to open file:%s", fileName);
      break;
    }
    if (QP_IO_ERROR_OK != qpDplIODeviceRead(&iFileProfile, m_pAutoConfigStruct,sizeof(QP_AUTO_CONFIG_STRUCT) , &iDataRead))
    {
      IMS_MSG_ERR_1_STR(LOG_IMS_FW,"AutoConfigMgr::readConfig - Unable to read file:%s", fileName);
      break;
    }
    IMS_MSG_LOW_1(LOG_IMS_FW,"AutoConfigMgr::readConfig - read Bytes-#%d from file ", iDataRead);
    iResult = QP_TRUE;
    break;
  }

  if (QP_IO_ERROR_OK  == iFileOpen)
  {
    if (QP_IO_ERROR_OK != qpDplIODeviceClose(&iFileProfile))
    {
      IMS_MSG_ERR_0(LOG_IMS_FW,"AutoConfigMgr::readConfig | Device close failed");
    }
  }

  if(qpDplStrcmp(m_pAutoConfigStruct->iImsi.pIMSIValue, m_pImsi->pIMSIValue) != 0 )
  {
    /* New ISIM has been detected, ignore the existing values */
    qpDplMemset(m_pAutoConfigStruct, 0, sizeof(QP_AUTO_CONFIG_STRUCT));
  }
  return iResult;
}

/************************************************************************
Function AutoConfigMgr::writeConfig()

Description
Write the configuration to the EFS from the internal cache

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPBOOL AutoConfigMgr::writeConfig( QPVOID )
{
  IMS_MSG_MED_0(LOG_IMS_FW,"AutoConfigMgr::writeConfig | Enter");

  QPIO_CONN_PROFILE iFileProfile = {0, QP_IO_FFS, QP_NULL, QP_NULL, QP_NULL, QP_NULL};
  QPE_IO_ERROR iFileOpen         = QP_IO_ERROR_UNKNOWN; 
  QPINT32 iDataWritten           = 0;
  QPBOOL  iResult                = QP_FALSE;
  QPCHAR  fileName[50];

  if ( !m_pAutoConfigStruct )
  {
    IMS_MSG_ERR_0_STR(LOG_IMS_FW,"AutoConfigMgr::writeConfig - m_pAutoConfigStruct is NULL");
    return QP_FALSE;
  }

  (void)qpDplStrlcpy(fileName, QIMF_AMSS_CONFIG_DIR, sizeof(fileName));
  (void)qpDplStrlcat(fileName, "/", sizeof(fileName));
  (void)qpDplStrlcat(fileName,QP_AUTO_CONFIG_FILE , sizeof(fileName));

  while (1)
  {
    iFileOpen = qpDplIODeviceOpen(&iFileProfile, fileName, QP_IO_OPEN_WRITE_BINARY, QP_NULL);
    if (QP_IO_ERROR_OK != iFileOpen)
    {
      IMS_MSG_ERR_1_STR(LOG_IMS_FW,"AutoConfigMgr::writeConfig - Unable to open file:%s", fileName);
      break;
    }
    if (QP_IO_ERROR_OK != qpDplIODeviceWrite(&iFileProfile, m_pAutoConfigStruct,sizeof(QP_AUTO_CONFIG_STRUCT), &iDataWritten, QP_TRUE))
    {
      IMS_MSG_ERR_1_STR(LOG_IMS_FW,"AutoConfigMgr::writeConfig - Unable to write file:%s", fileName);
      break;
    }
    IMS_MSG_LOW_1(LOG_IMS_FW,"AutoConfigMgr::writeConfig - Written Bytes-#%d into file ", iDataWritten);
    iResult = QP_TRUE;
    break;
  }

  if (QP_IO_ERROR_OK  == iFileOpen)
  {
    if (QP_IO_ERROR_OK != qpDplIODeviceClose(&iFileProfile))
    {
      IMS_MSG_ERR_0(LOG_IMS_FW,"AutoConfigMgr::writeConfig | Device close failed");
      iResult = QP_FALSE;
    }
  }
  return iResult;
}

/************************************************************************
Function AutoConfigMgr::updateConfig()

Description
Copy the input configuration to the internal cache. 

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPBOOL AutoConfigMgr::UpdateConfig( const QP_AUTO_CONFIG_STRUCT* n_pAutoConfigStructIn)
{
  IMS_MSG_MED_0(LOG_IMS_FW,"AutoConfigMgr::updateConfig | Enter");
  QPBOOL iResult = QP_FALSE;

  if ( QP_NULL == n_pAutoConfigStructIn || QP_NULL == m_pAutoConfigStruct )
  {
    IMS_MSG_ERR_0(LOG_IMS_FW,"AutoConfigMgr::updateConfig | pAutoConfigStructIn,m_pAutoConfigStruct is NULL");
    return QP_FALSE;
  }
  //Reset the existing internal cache
  qpDplMemset(m_pAutoConfigStruct, 0, sizeof(QP_AUTO_CONFIG_STRUCT));
  //Copy the passed structure to the internal cache
  qpDplMemscpy(m_pAutoConfigStruct,sizeof(QP_AUTO_CONFIG_STRUCT),n_pAutoConfigStructIn,sizeof(QP_AUTO_CONFIG_STRUCT));

  if (QP_CONFIG_IMS_OPRT_KT_MODE == m_iOprtMode)
  {
    iResult = writeConfigFileInfo();
  }
  else if (QP_CONFIG_IMS_OPRT_SKT_MODE == m_iOprtMode)
  {
    iResult = writeConfig();
  }
  return iResult;
}

/************************************************************************
Function AutoConfigMgr::GetConfigByType()

Description
Return the Configuration structure based on the structure type
pStructIn - memory buffer to be passed by the caller

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPBOOL AutoConfigMgr::GetConfigByType(QPVOID* n_pStructIn, QPE_AUTO_CONFIG_STRUCT_TYPES n_structType)
{
  IMS_MSG_MED_1(LOG_IMS_FW,"AutoConfigMgr::GetConfigByType | Enter Structure Type %d", n_structType);

  if ( QP_NULL == n_pStructIn || QP_NULL == m_pAutoConfigStruct)
  {
    IMS_MSG_ERR_0(LOG_IMS_FW,"AutoConfigMgr::GetConfigByType | pStructIn,m_pAutoConfigStruct is NULL");
    return QP_FALSE;
  }

  switch( n_structType )
  {
  case QP_AUTO_CONFIG_SIP:
    {
      QP_AUTO_CONFIG_SIP_STRUCT* pStruct = (QP_AUTO_CONFIG_SIP_STRUCT*)n_pStructIn;
      qpDplMemscpy((QPVOID*)pStruct,sizeof(QP_AUTO_CONFIG_SIP_STRUCT),(QPVOID*)&(m_pAutoConfigStruct->sip_struct),sizeof(QP_AUTO_CONFIG_SIP_STRUCT));
    }
    break;
  case QP_AUTO_CONFIG_SERVICE:
    {
      QP_AUTO_CONFIG_SERVICE_STRUCT* pStruct = (QP_AUTO_CONFIG_SERVICE_STRUCT*)n_pStructIn;
      qpDplMemscpy((QPVOID*)pStruct,sizeof(QP_AUTO_CONFIG_SERVICE_STRUCT),(QPVOID*)&(m_pAutoConfigStruct->service_struct),sizeof(QP_AUTO_CONFIG_SERVICE_STRUCT));
    }
    break;
  case QP_AUTO_CONFIG_REG:
    {
      QP_AUTO_CONFIG_REG_STRUCT* pStruct = (QP_AUTO_CONFIG_REG_STRUCT*)n_pStructIn;
      qpDplMemscpy((QPVOID*)pStruct,sizeof(QP_AUTO_CONFIG_REG_STRUCT),(QPVOID*)&(m_pAutoConfigStruct->reg_struct),sizeof(QP_AUTO_CONFIG_REG_STRUCT));
    }
    break;
  default:
    IMS_MSG_LOW_1(LOG_IMS_FW,"AutoConfigMgr::GetConfigByType | Unhandled structure Type  %d", n_structType);
    break;
  }

  return QP_TRUE;
}

/************************************************************************
Function AutoConfigMgr::InitConfigFileInfo()

Description
Initialize Config File After Card Read Success

Dependencies
None

Return Value
QP_TRUE/QP_FALSE

Side Effects
None
************************************************************************/
QPBOOL AutoConfigMgr::InitConfigFileInfo()
{
  IMS_MSG_MED_0(LOG_IMS_FW,"AutoConfigMgr::InitConfigFileInfo | Card Ready, Read Info Called");
  QPBOOL iResult = QP_FALSE;
  qpDplMemset(m_pAutoConfigStruct, 0, sizeof(QP_AUTO_CONFIG_STRUCT));

  if(QP_CONFIG_IMS_OPRT_KT_MODE == m_iOprtMode)
  {
    iResult = readConfigFileInfo();
  }
  else if(QP_CONFIG_IMS_OPRT_SKT_MODE == m_iOprtMode)
  {
    iResult = readConfig();
  }
  return iResult;
}

/************************************************************************
Function AutoConfigMgr::readConfigFileInfo()

Description
Read the Configuration Present In the file for KT

Dependencies
None

Return Value
QP_TRUE/QP_FALSE

Side Effects
None
************************************************************************/
QPBOOL AutoConfigMgr::readConfigFileInfo()
{
  QPIO_CONN_PROFILE iFileProfile = {0, QP_IO_FFS, QP_NULL, QP_NULL, QP_NULL, QP_NULL};
  QPE_IO_ERROR iFileOpen         = QP_IO_ERROR_UNKNOWN; 
  QPUINT   index                 = -1;
  QPINT32 iDataRead              = 0;
  QPBOOL  iResult                = QP_FALSE;
  QPCHAR  fileName[50];
  QPUINT32 iSeekOffset = 0;

  qpDplMemset(&m_SimConfigInfo, 0, sizeof(QP_KT_SIM_CONFIG_INFO));
  m_currentSimIndex = -1 ;
  m_SimConfigInfo.prevIndex = -1 ;
  /* After Card ready, read the values of IMSI */
  if(QP_FALSE == readIMSIValue())
  {
    return QP_FALSE;
  }

  (void)qpDplStrlcpy(fileName, QIMF_AMSS_CONFIG_DIR, sizeof(fileName));
  (void)qpDplStrlcat(fileName, "/", sizeof(fileName));
  (void)qpDplStrlcat(fileName,QP_KT_AUTO_CONFIG_FILE , sizeof(fileName));

  while (1)
  {
    iFileOpen = qpDplIODeviceOpen(&iFileProfile, fileName, QP_IO_OPEN_READ_BINARY, QP_NULL);
    if (QP_IO_ERROR_OK != iFileOpen)
    {
      IMS_MSG_ERR_1_STR(LOG_IMS_FW,"AutoConfigMgr::readConfigFileInfo - Unable to open file:%s", fileName);
      break;
    }
    if (QP_IO_ERROR_OK != qpDplIODeviceRead(&iFileProfile, &m_SimConfigInfo, sizeof(QP_KT_SIM_CONFIG_INFO), &iDataRead))
    {
      IMS_MSG_ERR_1_STR(LOG_IMS_FW,"AutoConfigMgr::readConfigFileInfo - Unable to read file:%s", fileName);
      break;
    }
    IMS_MSG_LOW_1(LOG_IMS_FW,"AutoConfigMgr::readConfigFileInfo - Read Bytes-#%d from file ", iDataRead);
    for ( index = 0; index < QP_KT_MAX_EFS_CONFIG; index++ )
    {
      if (qpDplStrcmp(m_SimConfigInfo.ImsiValue[index], m_pImsi->pIMSIValue ) == 0 )
      {
        m_currentSimIndex = index ;
        iSeekOffset = sizeof(QP_KT_SIM_CONFIG_INFO) + ( index * sizeof(QP_AUTO_CONFIG_STRUCT)) ;
        if (QP_IO_ERROR_OK != qpDplIODeviceSeek(&iFileProfile, iSeekOffset, QP_IO_SEEK_SET))
        {
          IMS_MSG_ERR_1_STR(LOG_IMS_FW,"AutoConfigMgr::readConfigFileInfo - Unable to read file:%s", fileName);
          break;
        }
        if (QP_IO_ERROR_OK != qpDplIODeviceRead(&iFileProfile, m_pAutoConfigStruct,sizeof(QP_AUTO_CONFIG_STRUCT) , &iDataRead))
        {
          IMS_MSG_ERR_1_STR(LOG_IMS_FW,"AutoConfigMgr::readConfigFileInfo - Unable to read file:%s", fileName);
          break;
        }
        IMS_MSG_LOW_1(LOG_IMS_FW,"AutoConfigMgr::readConfigFileInfo - read Bytes-#%d from file ", iDataRead);
        iResult = QP_TRUE;
        break;
      }             
    }
    break; 
  }

  if (QP_IO_ERROR_OK  == iFileOpen)
  {
    if (QP_IO_ERROR_OK != qpDplIODeviceClose(&iFileProfile))
    {
      IMS_MSG_ERR_0(LOG_IMS_FW,"AutoConfigMgr::readConfigFileInfo | Device close failed");
      iResult = QP_FALSE;
    }
  }
  return iResult;
}

/************************************************************************
Function AutoConfigMgr::writeConfigFileInfo()

Description
Writes the Configuration received to the EFS for KT

Dependencies
None

Return Value
QP_TRUE/QP_FALSE

Side Effects
None
************************************************************************/
QPBOOL AutoConfigMgr::writeConfigFileInfo()
{
  QPIO_CONN_PROFILE iFileProfile = {0, QP_IO_FFS, QP_NULL, QP_NULL, QP_NULL, QP_NULL};
  QPE_IO_ERROR iFileOpen         = QP_IO_ERROR_UNKNOWN; 
  QPINT32 iDataWritten           = 0;
  QPBOOL  iResult                = QP_FALSE;
  QPCHAR  fileName[50];
  QPUINT32 iSeekOffset = 0;
  QPINT8 index = m_currentSimIndex ;

  if ( index == -1 )
    index = ((m_SimConfigInfo.prevIndex + 1) % QP_KT_MAX_EFS_CONFIG );

  m_currentSimIndex = index ;
  m_SimConfigInfo.prevIndex = index ;

  qpDplStrlcpy(&(m_SimConfigInfo.ImsiValue[index][0]), m_pImsi->pIMSIValue, sizeof(m_pImsi->pIMSIValue));
  qpDplStrlcpy(&(m_SimConfigInfo.xmlVersion[index][0]), m_pAutoConfigStruct->xmlVersion,QP_AUTO_CONFIG_VER_LEN);
  m_SimConfigInfo.validityTime[index] = m_pAutoConfigStruct->validityTime ;

  IMS_MSG_HIGH_1_STR(LOG_IMS_FW,"AutoConfigMgr::writeConfigFileInfo -XML Version %s", m_pAutoConfigStruct->xmlVersion);
  IMS_MSG_HIGH_1(LOG_IMS_FW,"AutoConfigMgr::writeConfigFileInfo - Validity Time %d", m_pAutoConfigStruct->validityTime);

  (void)qpDplStrlcpy(fileName, QIMF_AMSS_CONFIG_DIR, sizeof(fileName));
  (void)qpDplStrlcat(fileName, "/", sizeof(fileName));
  (void)qpDplStrlcat(fileName,QP_KT_AUTO_CONFIG_FILE , sizeof(fileName));

  while (1)
  {
    iFileOpen = qpDplIODeviceOpen(&iFileProfile, fileName, QP_IO_OPEN_READ_WRITE_BINARY, QP_NULL);
    if (QP_IO_ERROR_OK != iFileOpen)
    {
      iFileOpen = qpDplIODeviceOpen(&iFileProfile, fileName, QP_IO_OPEN_WRITE_BINARY, QP_NULL);
      if (QP_IO_ERROR_OK != iFileOpen)
      {
        IMS_MSG_ERR_1_STR(LOG_IMS_FW,"AutoConfigMgr::writeConfigFileInfo - Unable to open file:%s", fileName);
        break;
      }
    }
    if (QP_IO_ERROR_OK != qpDplIODeviceWrite(&iFileProfile, &m_SimConfigInfo, sizeof(QP_KT_SIM_CONFIG_INFO), &iDataWritten, QP_TRUE))
    {
      IMS_MSG_ERR_1_STR(LOG_IMS_FW,"AutoConfigMgr::writeConfigFileInfo - Unable to write m_SimConfigInfo to file:%s", fileName);
      break;
    }
    iSeekOffset = sizeof(QP_KT_SIM_CONFIG_INFO) + ( index * sizeof(QP_AUTO_CONFIG_STRUCT)) ;
    if (QP_IO_ERROR_OK != qpDplIODeviceSeek(&iFileProfile, iSeekOffset, QP_IO_SEEK_SET))
    {
      IMS_MSG_ERR_2_STR(LOG_IMS_FW,"AutoConfigMgr::writeConfigFileInfo - Unable to seek offset %d file:%s", iSeekOffset,fileName); 
      break;
    }    
    if (QP_IO_ERROR_OK != qpDplIODeviceWrite(&iFileProfile, m_pAutoConfigStruct,sizeof(QP_AUTO_CONFIG_STRUCT), &iDataWritten, QP_TRUE))
    {
      IMS_MSG_ERR_1_STR(LOG_IMS_FW,"AutoConfigMgr::writeConfigFileInfo - Unable to write m_pAutoConfigStruct to file:%s", fileName);
      break;
    }    
    IMS_MSG_LOW_1(LOG_IMS_FW,"AutoConfigMgr::writeConfigFileInfo - Written Bytes-#%d into file ", iDataWritten);
    iResult = QP_TRUE;
    break;
  }

  if (QP_IO_ERROR_OK  == iFileOpen)
  {
    if (QP_IO_ERROR_OK != qpDplIODeviceClose(&iFileProfile))
    {
      IMS_MSG_ERR_0(LOG_IMS_FW,"AutoConfigMgr::writeConfig | Device close failed");
      iResult = QP_FALSE;
    }
  } 
  return iResult;
}

/************************************************************************
Function AutoConfigMgr::readIMSIValue()

Description
Reads the ISIM Value after card ready indication

Dependencies
None

Return Value
QP_TRUE/QP_FALSE

Side Effects
None
************************************************************************/
QPBOOL AutoConfigMgr::readIMSIValue()
{
  IMS_MSG_MED_0(LOG_IMS_FW,"AutoConfigMgr::readIMSIValue | Entered ");
  if(QP_NULL == m_pImsi)
  {
    m_pImsi = (QPDPL_IMSI_STRUCT*)qpDplMalloc(MEM_IMS_HTTPS, sizeof(QPDPL_IMSI_STRUCT));

    if(QP_NULL == m_pImsi)
    {
      IMS_MSG_FATAL_0(LOG_IMS_FW,"AutoConfigMgr::readIMSIValue | Failure in m_pImsi malloc");
      return QP_FALSE;
    }
  }
  qpDplMemset(m_pImsi, 0, sizeof(QPDPL_IMSI_STRUCT));
  qpDplGetIMSI(m_pImsi);

#ifdef FEATURE_IMS_PC_TESTBENCH
  QC_IMS_IGNORE_RETURN_VAL(qpDplStrlcpy(m_pImsi->pIMSIValue, "123456789",sizeof(m_pImsi->pIMSIValue)));
#endif // FEATURE_IMS_PC_TESTBENCH

  return QP_TRUE;
}

#define qp_hexToAsciii(c) (((c) < 10) ? (c) + '0' : ((c) - 10 + 'a'))
void ConvertHexToString( QPCHAR *Buf, QPUINT8 len, QPCHAR *OutBuf )
{
  QPUINT8 i =0 , j= 0;
  if ( ( Buf == NULL) || (len == 0 ) || ( OutBuf == NULL ) )
    return ;
  for ( i = 0; i < len ; i++)
  {
    QPCHAR lc = Buf[i]&0x0F;
    QPCHAR hc =  ((Buf[i]&0xF0) >> 4);
    OutBuf[j] = qp_hexToAsciii(hc);
    OutBuf[j+1] = qp_hexToAsciii(lc);
    j += 2;
  }
  OutBuf[j]='\0' ;
}
