
/************************************************************************
 Copyright (c) 2004-2006 Qualphone, Inc. All rights reserved.

 File Name      : qpdplsec.h
 Description    : Device Portability Layer security interface file. Public 
                  API for terminal devices
 
 Revision History
 ========================================================================
    Date    |   Author's Name    |  BugID  |        Change Description
 ========================================================================
 27-May-2013    Radvajesh          484719     Addition of crypto and hash functions
 ------------------------------------------------------------------------------------
 27-Aug-2013    Radvajesh          530216     Addition of keylength for hash functions
**********************************************************************************************************/ 
/**
 * \file qpdplsec.h
 * \brief Device Portability Layer security interface file. Public 
 *        API for terminal devices
 * \ingroup IMS
 *
 * qpdpl - Device Portability Layer security interface file. Public 
 *         API for terminal devices.
 *
 */ 
  
 /** @defgroup DPL_SEC The Device Portability Layer for security functions
 *  This is the DPL SEC Module group
 * \ingroup IMS
 */
 
 /** @defgroup DPL_SEC The Device Portability Layer security Interface file
 *  This is the DPL Module security interface file . Public API for terminal devices
 * \ingroup DPL_SEC
 *  @{
 */

#ifndef __QPDPL_SEC_H
#define __QPDPL_SEC_H

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus
/**
 * \enum eSecAlgo
 * Security algo Type enumeration
 */
 /**
 * \var typedef eSecAlgo QPE_DPL_SEC_ALGO
 * Type name for sec algo
 */
typedef enum eSecAlgo
{
  QP_DPL_SECAPI_NULL =0,
  QP_DPL_SECAPI_SHA             = 1,      /* SHA-1 Hash function                  */
  QP_DPL_SECAPI_SHA256          = 2,      /* SHA256 Hash function                 */ 
  QP_DPL_SECAPI_MD5             = 3,      /* MD5 Hash function                    */
  QP_DPL_SECAPI_HSHA1           = 4,      /* HMAC SHA1 Hash function              */
  QP_DPL_SECAPI_HSHA256          =5,      /* HMACH SHA2 function                  */
  QP_SECAPI_DES             = 6,      /* DES Encryption-Decryption function   */
  QP_SECAPI_3DES            = 7,     /* 3DES Encryption-Decryption function  */
  QP_SECAPI_AES128          = 8,   /* AES CTR cipher, 128-bit key           */
  QP_DPL_SECAPI_ALGO_MAX,                 /* Must be last in list, to size array  */  
} QPE_DPL_SEC_ALGO;


/**
 * \enum eSecPad
 * Security algo padding Type enumeration
 */
 /**
 * \var typedef eSecPad QPE_DPL_SEC_PADDING_type
 * Type name for sec padding
 */
typedef enum eSecPad
{
  QP_DPL_SECAPI_ENC_NO_PADDING = 0, /* No padding*/
  QP_DPL_SECAPI_ENC_ENABLE_PADDING=1, /* Enable padding*/
} QPE_DPL_SEC_PADDING_type;


/**
 * \enum eSecAlgoMode
 * Security algo mode Type enumeration
 */
 /**
 * \var typedef eSecAlgoMode QPE_DPL_SEC_ALGO_MODE
 * Type name for sec padding
 */
typedef enum eSecAlgoMode
{
  QP_DPL_SECAPI_ENC_OP_MODE_CBC   = 0, /* CBC Mode*/
  QP_DPL_SECAPI_ENC_OP_MODE_ECB   = 0x1, /* ECB Mode*/
  QP_DPL_SECAPI_ENC_OP_MODE_CTR   = 0x2, /* CTR Mode */
  QP_DPL_SECAPI_HSH_INPUT_MODE_ATOMIC =0x3,
} QPE_DPL_SEC_ALGO_MODE;

/**
 * \enum eSecPlatform
 * Security execution platform Type enumeration
 */
 /**
 * \var typedef eSecPlatform QP_DPL_CIPHER_EXEC_MODE
 * Type name for sec execution mode
 */
typedef enum eSecPlatform
{
  QP_DPL_SECAPI_EXEC_PLATFORM_SW   = 0, /* Software based execution*/
  QP_DPL_SECAPI_EXEC_PLATFORM_HW   = 0x1, /* Hardware based  execution*/
} QP_DPL_CIPHER_EXEC_MODE;




/**
 * \fn QP_IMPORT_C QPUINT32  qpDplGetCipherHandle(QPE_DPL_SEC_ALGO  eSecAlgo);
 * 
 *
 * This function will create a cipher handle for the specifed cipher algo
 * eSecAlgo         - cipher algorithm specifier
 * emode            - cipher algorithm mode specifier
 * epadtype         - epadding type
 * \return QPUINT32    -   cipher handle
 * \remarks : The cipher handle to be used for subsequent cipher operations.
 */
QP_IMPORT_C QPUINT32  qpDplGetCipherHandle(QPE_DPL_SEC_ALGO  eSecAlgo,
                                           QPE_DPL_SEC_ALGO_MODE emode,
										   QPE_DPL_SEC_PADDING_type epadtype);

/**
 * \fn QP_IMPORT_C QPUINT32  qpDplFreeCipherHandle(QPUINT32  uiCipherHandle);
 * 
 *
 * This function will free a cipher handle 
 * uiCipherHandle      - The cipher handle
 
* \return QPUINT32    -   0 on success and error code on failure
 * \remarks : The cipher handle cannot be reused anymore.
 */
QP_IMPORT_C QPINT32  qpDplFreeCipherHandle(QPUINT32  uiCipherHandle);



/**
 * \fn QP_IMPORT_C QPINT32  qpDplSetExecMode(QPUINT32 uiCipherHandle,
								     QPE_DPL_SEC_ALGO emode);
 * 
 *
 * This function will configure set the execution mode for cipher process
 * uiCipherHandle      - The cipher handle
 * emode               - Cipher Alogrithm execution mode specifier h/w or software
 
 * \return QPUINT32    -   0 on success and error code on failure
 * \remarks : The execution mode can be changed on the fly based on performence analysis
 */
QP_IMPORT_C QPINT32  qpDplSetExecMode(QPUINT32 uiCipherHandle,
								     QP_DPL_CIPHER_EXEC_MODE emode);


/**
 * \fn QP_IMPORT_C QPINT32  qpDplEncryptData(QPUINT32 uiCipherHandle,
								     QPUINT8* indata,
									 QPUINT8* outdata,
									 QPUINT32  inDatalenbytes,
									 QPUINT32* pOutDatalenbytes,
									 const QPCHAR*  Key,
									 const QPCHAR* iv);										   
 * 
 *
 * This function will encrypt the data specified in indata and placed in out data
 * uiCipherHandle    - The cipher handle
 * indata            - data to be encyrpyted
 * outdata           - storage for output of encyrpyted data can be same as indata
 * inDatalenbytes    - input data length in bytes
 * pOutDatalenbytes  - reference to output data length
 * Key               - Key null terminated
 * iv                - initilization vector null terminated
 * \return QPUINT32    -   0 on success and error code on failure
 * \remarks : The indata and outdata can be same.
 */
QP_IMPORT_C QPINT32  qpDplEncryptData(QPUINT32 uiCipherHandle,
								     QPUINT8* indata,
									 QPUINT8* outdata,
									 QPUINT32  inDatalenbytes,
									 QPUINT32* pOutDatalenbytes,
									 const QPCHAR*  Key,
									 const QPCHAR* iv);		

/**
 * \fn QP_IMPORT_C QPINT32  qpDplDecryptData(QPUINT32 uiCipherHandle,
								     QPUINT8* indata,
									 QPUINT8* outdata,
									 QPUINT32  inDatalenbytes,
									 QPUINT32* pOutDatalenbytes,
									 const QPCHAR* Key,
									 const QPCHAR*  iv);								   
 * 
 *
 * This function will Decrypt the data specified in indata and placed in out data
 * uiCipherHandle      - The cipher handle
 * indata            - data to be decrypted
 * outdata           - storage for output of decrypted data can be same as indata
 * inDatalenbytes     -  input data length in bytes
 * pOutDatalenbytes  - reference to output data length
 * Key               - Key null terminated
 * iv                - initilization vector null terminated
 * \return QPUINT32    -   0 on success and error code on failure
 * \remarks : The indata and outdata can be same.
 */
QP_IMPORT_C QPINT32  qpDplDecryptData(QPUINT32 uiCipherHandle,
								     QPUINT8* indata,
									 QPUINT8* outdata,
									 QPUINT32  inDatalenbytes,
									 QPUINT32* pOutDatalenbytes,
									 const QPCHAR* Key,
									 const QPCHAR*  iv);										 
									   
/**
 * \fn QP_IMPORT_C QPINT32  qpDplHashData(QPUINT32 uiCipherHandle,
								     QPUINT8* indata,
									 QPUINT32 inDatalenbytes,
									 QPUINT8* outdata,
									 QPUINT32 outDatalenbytes,
									 const QPCHAR* Key);
 * 
 *
 * This function will hash the given data with the configured algo and mode
 * This function will Decrypt the data specified in indata and placed in out data
 * uiCipherHandle      - The cipher handle
 * indata            - data to be hashed
 * inDatalenbytes     - in data length in bytes
 * outdata           - storage for output of hash data 
 * OutDatalenbytes     - out data length in bytes
 * Keylenbytes       - Keylength  for HMAC otherwice set to NULL
 * \return QPUINT32    -   0 on success and error code on failure
 */
QP_IMPORT_C QPINT32  qpDplHashData(QPUINT32 uiCipherHandle,
								     QPUINT8* indata,
									 QPUINT32 inDatalenbytes,
									 QPUINT8* outdata,
									 QPUINT32 outDatalenbytes,
									 const QPCHAR* Key,
									 QPUINT32 Keylenbytes);		

/**
 * \fn QP_IMPORT_C QPBOOL qpDplHashCreateDigest( 
                                          QPE_DPL_SEC_ALGO    eSecAlgo,
                                          QPUINT8*            msg_data_ptr,
                                          QPUINT16            msg_byte_len,
                                          QPUINT8*            msg_digest_ptr);
 * 
 *
 * This function will create a message digest using the algorithm specified.
 * pHashHandle         - HashHandle to be provided on delete
 * eSecAlgo            - Hashing algorithm specifier
 * msg_data_ptr        - Pointer to message to be authenticated
 * msg_byte_len        - Length of message in bytes
 * msg_digest_ptr      - Pointer to message digest (memory provided by caller)
 *
 *
 *
 * \return QPBOOL    -   QP_TRUE or QP_FALSE
 * \remarks : Once the HashCreateDigest is successful it will internally delete the Crypto instance created.
 */
QP_IMPORT_C QPBOOL  qpDplHashCreateDigest(QPE_DPL_SEC_ALGO          eSecAlgo,
                                          QPUINT8*                  pMsg_data_ptr,
                                          QPUINT16                  pMsg_byte_len,
                                          QPUINT8*                  pMsg_digest_ptr);


#ifdef __LINUX__
/**
 * \fn QP_IMPORT_C QPUINT8  qpDplGetPmemBuffer(QPUINT32 uiCipherHandle,QPUINT16  iDatalenbytes);
 * 
 *
 * This function will create a pmem handle  and the buffer pointer shall be returned
 * uiCipherHandle      - The cipher handle
 * iDatalenbytes     - in data length in bytes
  
 * \return QPUINT8    -   Pointer to pmembuffer
 * \remarks : Pmem buffer is used to avoid user to kernel copies and should be used of inplace encryption.
 */
QP_IMPORT_C QPUINT8*  qpDplGetPmemBuffer(QPUINT16  iDatalenbytes);


/**
 * \fn QP_IMPORT_C QPINT32  qpDplSetPmemBufferOffset(QPUINT32 uiCipherHandle,QPUINT32 uiOffset);
 * 
 *
 * This function will set the pemem buffer pointer to be used for ciphering
 * uiCipherHandle      - The cipher handle
 * uiOffset     - offset to be used
  
 * \return QPUINT32    -   0 on success and error code on failure
 * \remarks : Pmem buffer can hold multiple packets and pass the offset for each cipher operation.
 */
QP_IMPORT_C QPINT32  qpDplSetPmemBufferOffset(QPUINT32 uiCipherHandle,QPUINT32 uiOffset);




/**
 * \fn QP_IMPORT_C QPINT32  qpDplFreeHWCryptoResource();
 * 
 *
 * This function will free the pmem handle and close the qcedriver

 * \return QPINT32    -   0 success
 * \remarks : Pmem buffer is used to avoid user to kernel copies and should be used of inplace encryption.
 */
QP_IMPORT_C QPINT32  qpDplFreeHWCryptoResource();

#endif	//__LINUX__
#ifdef __cplusplus
}
#endif // __cplusplus

#endif // __DPL_SEC_H
/** @} */ // end of group DPL_SEC
