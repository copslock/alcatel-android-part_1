﻿//
// Copyright (c) 2014- Qualcomm Technologies, Inc. All rights reserved.
//
//
// qpDplhandOver.h
//

/**
 * \file qpDplhandOver.h
 * \brief DPL handover engine header
 *
 * \ingroup DPL
 *
 *
 */ 

 /************************************************************************
 Copyright (C) 2015 Qualcomm Technologies, Inc. All Rights Reserved.

 File Name      : qpDplhandOver.h
 Description    : DPL Handover Engine Header
 
 Revision History
 ========================================================================
    Date    |   Author's Name    |  BugID  |        Change Description
 ========================================================================
  06/03/2014    Vikas                            Initial Draft
-------------------------------------------------------------------------
29-Aug-2014     Devrath            716266        FR 18799: iWLAN Emergency Calling over WiFi
-------------------------------------------------------------------------
11-Feb-2015     Dipak              766106        FR 24984: SIM Swap without RESET
-----------------------------------------------------------------------------
1-Jul-2015     Devrath        819069     [IMS Requirement - DPL]: FR 25455: Internal - Improvements to WiFi 
                                          Interface selection criteria, Media metrics
----------------------------------------------------------------------------------------------------
10-Aug-2015    Devrathc           850502   FR 28055: Use modem data api for wifi quality
----------------------------------------------------------------------------------
1-Sep-2015     Devrathc        898036     1x ecio is not converted to proper units before comparing 
                                          against threshold 
----------------------------------------------------------------------------------
18-Nov-2015    Devrath          937637    Enhancements and Decision Table modifications for Media Metrics
---------------------------------------------------------------------------------
30-Nov-2015    Vikas            903058    FR 30244 - IMS DPL changes
************************************************************************/

#ifndef _QPDPL_HO_H
#define _QPDPL_HO_H

#include <qplog.h>
#include <qpdcm.h>
#include <qpdpl.h>
#include <lte_pdcp_msg.h>
#include <cmapi.h>
#include <qpdefines.h>

// LTE_DL
#define LTE_DL_SAMPLING_TIMER 500

// MAX UINT32 value
#define _UI32_MAX     0xffffffff /* maximum unsigned 32 bit value */

// Thresholds 

#define LTE_DL_RSRQThresholdSrcLow -70
#define LTE_DL_RSRQThresholdSrcHigh -60
#define LTE_DL_RSRQThresholdTargetLow -70
#define LTE_DL_RSRQThresholdTargetHigh -60
#define LTE_DL_SNRThresholdSrcLow 100
#define LTE_DL_SNRThresholdSrcHigh 150
#define LTE_DL_SNRThresholdTargetLow 100
#define LTE_DL_SNRThresholdTargetHigh 150
#define LTE_DL_MonitorSTRATTimeInSec 5
#define LTE_DL_MonitorTRATTimeInSec 5

#define WLAN_SamplingTimerValue 2
#define WLAN_MonitorTimerValue 6
#define WLAN_RssiThresholdHigh -75
#define WLAN_RssiThresholdLow -85

//LTE_UL default values
#define LTE_UL_SAMPLING_TIMER 1000
#define LTE_UL_MONITORING_TIME 6000
#define LTE_UL_ALPHA 0.75
#define LTE_UL_THRESHOLD_BAD_DELAY 70.0
#define LTE_UL_THRESHOLD_GOOD_DELAY 60.0
#define WLAN_RssiThresholdRepoint -80
//LTE_UL default values

// 1x CP values
#define TH_1x_VALUE -12 //db
#define MAX_1x_SAMPLES 5
#define IMS_CMSS_RSSI_NO_SIGNAL 125
#define IMS_CMSS_ECIO_VALUE_NO_SIGNAL 5
#define IMS_CMSS_RSSI_NO_SIGNAL_2 63

#define HO_TestMode 0

//HO algo flavor
#define HO_AlgoFlavor 1 //leagcy (set to 1 for vzw vowifi)

#define QUAL_MASK_INVALID_64 0xF000000000000000
#define QUAL_MASK_INVALID 0xF0000000
#define QUAL_MASK_ALL_LOW 0x00000000
#define QUAL_MASK_A_HIGH 0x00000001
#define QUAL_MASK_B_HIGH 0x00000002
#define QUAL_MASK_C_HIGH 0x00000004
#define QUAL_MASK_D_HIGH 0x00000008
#define QUAL_MASK_E_HIGH 0x00000010
#define QUAL_MASK_F_HIGH 0x00000020
#define QUAL_MASK_G_HIGH 0x00000040
#define QUAL_MASK_H_HIGH 0x00000080

#define LTE_QUAL_TH_LTE1_HIGH -117
#define LTE_QUAL_TH_LTE2_HIGH -115
#define LTE_QUAL_TH_LTE3_HIGH -110

#define LTE_QUAL_TH1_HIGH -117
#define LTE_QUAL_TH2_HIGH -115
#define LTE_MONITOR_TIME 5


#define WIFI_QUAL_TH_A_HIGH -85
#define WIFI_QUAL_TH_B_HIGH -95
#define WIFI_QUAL_TH_C_HIGH -85
#define WIFI_QUAL_TH_D_HIGH -95

//HO algo tuning Param
#define HO_AlgoTuningParam 1

//Media Related
#define Media_UL_Jitter_Good 10
#define Media_UL_Jitter_Acceptable 10
#define Media_UL_Jitter_Fair 10
#define Media_UL_Jitter_Bad 10
#define Media_UL_FrameLoss_Good 10
#define Media_UL_FrameLoss_Acceptable 10
#define Media_UL_FrameLoss_Fair 10
#define Media_UL_FrameLoss_Bad 10
#define Media_DL95_Jitter_Good 10
#define Media_DL95_Jitter_Acceptable 10
#define Media_DL95_Jitter_Fair 10
#define Media_DL95_Jitter_Bad 10
#define Media_DL_Jitter_Good 10
#define Media_DL_Jitter_Acceptable 10
#define Media_DL_Jitter_Fair 10
#define Media_DL_Jitter_Bad 10
#define Media_DL_FrameLoss_Good 10
#define Media_DL_FrameLoss_Acceptable 10
#define Media_DL_FrameLoss_Fair 10
#define Media_DL_FrameLoss_Bad 10
#define Media_DL_QDJ_Speech 10
#define Media_DL_QDJ_Silence 10
#define Media_Monitor_Time 5
#define Media_Metrics_Disabled 1

#define DCM_MAX_WIFI_PROFILES DS_SYS_MAX_WQE_PROFILES

typedef struct ThresholdData
{
  float LTEDLRSRQThresholdSrcLow;
  float LTEDLRSRQThresholdSrcHigh;
  float LTEDLRSRQThresholdTargetLow;
  float LTEDLRSRQThresholdTargetHigh;
  float LTEDLSNRThresholdSrcLow;
  float LTEDLSNRThresholdSrcHigh;
  float LTEDLSNRThresholdTargetLow;
  float LTEDLSNRThresholdTargetHigh;
  QPUINT32 LTEDLMonitorSTRATTimeInSec;
  QPUINT32 LTEDLMonitorTRATTimeInSec;

  QPUINT32 MediaMonitorTime;

  QPUINT32 WLANSamplingTimerValue; 
  QPUINT32 WLANMonitorTimerValue; 
  QPUINT32 WLANRssiThresholdHigh; 
  QPUINT32 WLANRssiThresholdLow; 
  QPUINT32 WLANRssiThresholdRepoint; 

  QPUINT32 LTEMonitorTimeInSec;
  QPINT32  LTE_QUAL_TH_LTE1;
  QPINT32  LTE_QUAL_TH_LTE2;
  QPINT32  LTE_QUAL_TH_LTE3;

  QPINT32  LTE_QUAL_TH1;
  QPINT32  LTE_QUAL_TH2;

  QPINT32 THWlanA, THWlanB,THWlanC,THWlanD;

  //LTEUL
  QPUINT32 iLTE_UL_Sampling_Timer;
  QPUINT32 iLTE_UL_Monitoring_Time;
  float    fLTE_UL_Alpha;
  float    fLTE_UL_Threashold_Bad_Delay;
  float    fLTE_UL_Threashold_Good_Delay;	
  //LTEUL

  //Media Metrics
  QPUINT32 iMediaULJitterGood;
  QPUINT32 iMediaULJitterAcceptable;
  QPUINT32 iMediaULJitterFair;
  QPUINT32 iMediaULJitterBad;
  QPUINT32 iMediaULFrameLossGood;
  QPUINT32 iMediaULFrameLossAcceptable;
  QPUINT32 iMediaULFrameLossFair;
  QPUINT32 iMediaULFrameLossBad;
  QPUINT32 iMediaDL95JitterGood;
  QPUINT32 iMediaDL95JitterAcceptable;
  QPUINT32 iMediaDL95JitterFair;
  QPUINT32 iMediaDL95JitterBad;
  QPUINT32 iMediaDLJitterGood;
  QPUINT32 iMediaDLJitterAcceptable;
  QPUINT32 iMediaDLJitterFair;
  QPUINT32 iMediaDLJitterBad;
  QPUINT32 iMediaDLFrameLossGood;
  QPUINT32 iMediaDLFrameLossAcceptable;
  QPUINT32 iMediaDLFrameLossFair;
  QPUINT32 iMediaDLFrameLossBad;
  QPUINT8  iMediaDLQDJSpeech;
  QPUINT8  iMediaDLQDJSilence;
  QPUINT32 iMediaTimerInterval;
  
  //HO Algo tuning
  QPUINT32 HOAlgoTuningParam;

  QPUINT32 HOAlgoFlavor;

  QPINT32  WIFI_QUAL_TH_A;
  QPINT32  WIFI_QUAL_TH_B;
  QPINT32  WIFI_QUAL_TH_C;
  QPINT32  WIFI_QUAL_TH_D;

  QPBOOL   bIsWiFiThresholdsUpdated;

  QPUINT32 HOTestMode;
  //Media Metrics Enable
  QPUINT32 MediaMetricsDisabledFlag; 
 // 1xcp
  QPINT32 th_1x_ThresholdValue;
  QPINT32 max_1x_samples;
  
  QPINT32 iMediaAlpha;
}Threshold_Data;

typedef enum qpDplThresholdStatus
{
  THRESH_INVALID = -1,
  THRESH_MET = 0,
  THRESH_NOT_MET = 1
}QP_DPL_THRESHOLD_STATUS;

typedef enum qpDplLastCommand
{
  INVALID = -1,
  INIT = 0,
  START = 1,  
  STOP = 2
}QP_DPL_LAST_COMMAND;

typedef struct WLAN_Config_Data
{
  QP_DPL_LAST_COMMAND  LAST_COMMAND;
  QPE_DCM_RAT eSRAT;
  QPE_DCM_RAT eTRAT;
  QPBOOL bInCall; 
  QPBOOL bIsQMIInitSent;

}WLAN_ConfigData;

typedef enum qpDplAppAction
{
  NO_ADDL_CRITERIA = 0,
  CS_REPOINT_CRITERIA_MET,
  HO_INVALID_CRITERIA
}QP_DPL_ADDL_CRITERIA;

typedef struct LTEDLConfigData
{
  QPUINT32 startTime;
  float RSRQ_old;
  float RSRP_old;
  float SNR_old;
  QP_DPL_RAT_QUALITY  SNR_STATE;
  QP_DPL_RAT_QUALITY  RSRQ_STATE;
  QP_DPL_RAT_QUALITY DL_OVERALL_STATE;
  QP_DPL_RAT_QUALITY DL_PREV_STATE;
  QP_DPL_LAST_COMMAND  LAST_COMMAND;
  QPE_DCM_RAT eSRAT;
  QPE_DCM_RAT eTRAT;
  QPUINT32 PREV_RAT_QUALITY_MASK;
}LTE_DL_ConfigData;

typedef enum qpDplCallType
{
  QPE_CALL_TYPE_INVALID = 0,
  QPE_CALL_TYPE_VOICE,
  QPE_CALL_TYPE_VIDEO
}QPE_CALL_TYPE;

struct RatQualityMatrix
{
  QP_DPL_RAT_QUALITY qTable[QPE_METRIC_MODULE_MAX];
  QPUINT32 wifi_quality_mask; //wifi mask for new algorithm
  QPUINT64 wifi_quality_mask_64bit; //Raw DS mask
  QPUINT32 lte_dl_quality_mask; //wifi mask for new algorithm
  QP_DPL_RAT_QUALITY media_quality;
};

typedef struct RatQualityMatrix RAT_QUALITY_MATRIX;

struct qpHandOverStatusStruct
{
  QPBOOL bIsHOPossible;
  QPE_DCM_RAT eSRAT;
  QPE_DCM_RAT eTRAT;
  QP_DPL_ADDL_CRITERIA action;
  QP_DPL_RAT_QUALITY srcRATQuality;
  QP_DPL_RAT_QUALITY targetRATQuality; 
  QP_DPL_RAT_QUALITY qTable[QPE_METRIC_MODULE_MAX];
  QPE_CALL_TYPE call_type;
  QPBOOL bInCall;
};

typedef struct qpHandOverStatusStruct QP_HANDOVER_STATUS_TYPE;

typedef QPVOID (*QP_DPL_HO_STATUS_CB)(QP_HANDOVER_STATUS_TYPE *pHOStatus, QPVOID* pAppCallBackData);

typedef enum qpDplHoServiceMask
{
  HO_SERVICE_MASK_NONE = 0x0,
  /* mask for SMS */
  HO_SERVICE_MASK_SMS = 0x1,
  /* mask for VOICE */
  HO_SERVICE_MASK_VOICE = 0x2,
  /* mask for VIDEO */
  HO_SERVICE_MASK_VIDEO = 0x4,
  /* mask for RCS */
  HO_SERVICE_MASK_RCS = 0x8,
  /* mask for VOICE+VIDEO */
  HO_SERVICE_MASK_VOICE_OR_VIDEO = HO_SERVICE_MASK_VOICE | HO_SERVICE_MASK_VIDEO,
  /* mask for ALL */
  HO_SERVICE_MASK_ALL   = 0xF
}QP_DPL_HO_SERVICE_MASK;

typedef enum qpDplHoAppType
{
  HO_APP_TYPE_RM = 0,
  HO_APPTYPE_QIPCALL
}QP_DPL_HO_APP_TYPE;

struct qpHandOverParamStruct
{
  QPE_DCM_RAT eSRAT;
  QP_DPL_HO_SERVICE_MASK eSRAT_MASK;
  QPE_DCM_RAT eTRAT;
  QP_DPL_HO_SERVICE_MASK eTRAT_MASK;
  QPE_DCM_RAT ePrefRAT;
  QPE_DCM_RAT eUnProcessedSRAT;
  QPE_DCM_RAT eUnProcessedTRAT;
  QPBOOL bInCall;
  QP_DPL_HO_APP_TYPE app_type;
  QPBOOL bIsCallOnHold;
  QPE_CALL_TYPE call_type;
  QPINT32 wifi_quality_mask; //wifi mask for new algorithm
  QPINT32 lte_dl_quality_mask; //wifi mask for new algorithm
};

typedef struct qpHandOverParamStruct QP_HANDOVER_PARAMS_TYPE;

typedef struct HOConfigData
{
  /* Source, target rat values provided by APP before handover init */
  QP_HANDOVER_PARAMS_TYPE CurrentConfig;
  /* Matrix table received from each module when Metric Report is called */
  RAT_QUALITY_MATRIX RawMatrix;
  /* Matrix table processed based on raw table and other implicit rules based on priority, invalid measurements */
  RAT_QUALITY_MATRIX PreprocessedMatrix;
  /* TRAT Matrix table received from each module when Metric Report is called */
  //RAT_QUALITY_MATRIX TRATRawMatrix;
  /* TRAT Matrix table processed based on raw table and other implicit rules based on priority, invalid measurements */
  //RAT_QUALITY_MATRIX TRATPreprocessedMatrix;
  /* Decision Matrix table as per the algo document for SRAT */
  QPINT   iSRATMeasurementQualityTable[16];
  /* Decision Matrix table as per the algo document for TRAT */
  QPINT   iTRATMeasurementQualityTable[4];
  /* Current Quality of SRAT */
  QP_DPL_RAT_QUALITY eCurrSRATQual;
  /* Current Quality of SRAT */
  QP_DPL_RAT_QUALITY eCurrTRATQual;
  /* APP callback to notify Hand Over status */
  QP_DPL_HO_STATUS_CB  pAppCallback;
  /* APP Data to be passed as part of HO clalback */
  QPVOID               *pAppData;
  /* measurement module states */
  QPBOOL isLTEStarted;
  QPBOOL isWLANStarted;
  QPBOOL isMEDIAStarted;

  //HO Algo tuning
  QPUINT32 HOAlgoTuningParam;

  //HO Algo mode)
  // 1 --- vzw vowifi FR, new values will be defined going fwd if required.
  QPUINT32 HOAlgoFlavor;

  //vzw specific for convinience
  QP_DPL_THRESHOLD_STATUS wlanA,wlanB,wlanC,wlanD, lte1, lte2, lte3, lteth1, lteth2;
  QPUINT32 HOTestMode;

}HO_ConfigData;

typedef struct qpLTEDLReportStruct QP_LTE_DL_ReportStruct;



typedef enum qpDplHOModuleCmd 
{
  QPE_HO_CMD_INVALID = -1,  
  QPE_HO_CMD_INIT = 0,
  QPE_HO_CMD_START,
  QPE_HO_CMD_STOP,
  QPE_HO_CMD_MAX
}QPE_HO_MODULE_CMD;

typedef enum qpDplRATSource
{
  SRAT = 0,  
  TRAT = 1
}QP_DPL_RAT_SOURCE;



//LTE UL data structures
typedef struct{
	QPUINT32 eps_id;
	QPUINT32 iSampleTime;
}PerBearerTimerInfo;

typedef struct ULConfigData
{
  QPUINT32 iStartTimeForLTEUL;
  QPBOOL iIsFirstMeasurement;
  QP_DPL_MEASUREMENT_REPORT_STATUS   sLastReportedState;
  QP_DPL_MEASUREMENT_REPORT_STATUS   sLastComputedState;
  float iPrevLTETransDelay;
  PerBearerTimerInfo iRBMeasurementTime[LTE_MAX_EPS];
  QPBOOL       bIsLTEULStarted;
}UL_ConfigData;
//LTE UL data structures

struct Qp1xConfigData
{
  QPUINT32            numberOfSamples;
  QP_DPL_RAT_QUALITY_1x  OVERALL_STATE;

  QP_DPL_RAT_QUALITY_1x  REPORT_STATE;
  QPUINT8             raw_ecio_1x;
};

typedef struct Qp1xConfigData QPC_1x_ConfigData;

struct HO1xConfig
{
  /* This flag needs to be set if the below 1x threshold is set to a 
  valid value and if app wants DPL to update the threshold */
  QPBOOL    bImsHO_1x_Config_Valid;
  QPINT32   iIMSHO_1x_QualTH;
};

typedef struct HO1xConfig IMS_HO_1x_CONFIG;

struct HOLTEConfig
{
  /* This flag needs to be set if at least one of the below LTE thresholds are set to a 
  valid value and if app wants DPL to update the threshold */
  QPBOOL    bIMSHO_LTE_Config_Valid;

  /* Flag to indicate if this LTE threshold is valid */
  QPBOOL    bImsHO_LTE_TH1_Valid;
  QPINT32   iIMSHO_LTE_Qual_TH1; 
  QPBOOL    bImsHO_LTE_TH2_Valid;
  QPINT32   iIMSHO_LTE_Qual_TH2;
  QPBOOL    bImsHO_LTE_TH3_Valid;
  QPINT32   iIMSHO_LTE_Qual_TH3;
};

typedef struct HOLTEConfig IMS_HO_LTE_CONFIG;

struct HOWiFiConfig
{
  /* This flag needs to be set if at least one of the below WiFi thresholds are set to a 
  valid value and if app wants DPL to update the threshold */
  QPBOOL    bIMSHO_WiFi_Config_Valid;

  /* Flag to indicate if this WiFi threshold is valid */
  QPBOOL    bImsHO_WIFI_THA_Valid;
  QPINT32   iIMSHO_WIFI_Qual_THA;
  QPBOOL    bImsHO_WIFI_THB_Valid;
  QPINT32   iIMSHO_WIFI_Qual_THB;
};

typedef struct HOWiFiConfig IMS_HO_WIFI_CONFIG;

struct IMSHandOverConfigItem
{
  /* 1x Thresholds */
  IMS_HO_1x_CONFIG       s1xConfig;

  /* LTE Thresholds */
  IMS_HO_LTE_CONFIG      sLTEConfig;

  /* Wifi Thresholds */
  IMS_HO_WIFI_CONFIG     sWiFiConfig;
};

typedef struct IMSHandOverConfigItem IMS_HO_CONFIG_ITEM;

QPBOOL qpDplLTEULMeasurementsSetParam(QPE_HO_MODULE_CMD eHOModuleCmd);
QPVOID qpDplLTEDLMeasurementsSetParam(QPE_HO_MODULE_CMD eHOModuleCmd);
QPBOOL qpDplWiFiMeasurementsSetParam(QPE_HO_MODULE_CMD eHOModuleCmd);

QP_DPL_RAT_QUALITY qpDplLTEULMeasurementCalculateStats(QPUINT32 iRetReports, QPUINT32 iCurrTime);

/**
 * \ qpDplHandOverParamsSet
 * Main entry point from qipcall to HO engine
 *
 *only LTE, IWLAN or NONE RAT values allowed in HOParams 
 */
QPBOOL qpDplHandOverParamsSet(QP_HANDOVER_PARAMS_TYPE *pHOParams, QP_DPL_HO_STATUS_CB pHandOverCallBack, QPVOID *pAppData);

QPVOID qpDplHOEngineInitialize();

QPVOID qpDplHOConfigDataInit();
QPVOID qpDplHODecisionMeasurementTableInit(QPINT *pMeasurementQualityTable);
QPBOOL qpDplHandOverMsgrEventSend(QPUINT32 iMsgrMessageID, QPVOID* pMSGRBuf);
QPVOID qpDplHandOverMsgrEventRecv(QPUINT32 iMsgrMessageID, QPVOID* pMSGRBuf);

QPVOID qpDplHOFillAppCallbackData(QPBOOL isHOPosible, QP_DPL_ADDL_CRITERIA iCriteria);

QPVOID qpDplSetIsQmiInitSent(QPBOOL bVal);

QPVOID qpDplHandOverDeInit();

QPBOOL qpDplHandoverIsInCall();

QPVOID qpdplUpdateMediaMetricQualityWithSRATQuality();
QPVOID qpDplUpdateRATQualityOnRATChange(QPE_DCM_RAT eRAT);
QPVOID qpDplPrintCurrentRATQual();

/**
 * \ DPLUpdateHOConfig
 *   API for app to update handover configuration parameters 
 *
 */

QPBOOL DPLUpdateHOConfig(IMS_HO_CONFIG_ITEM *pHOConfigBuf);

#endif //_QPDPL_HO_H
