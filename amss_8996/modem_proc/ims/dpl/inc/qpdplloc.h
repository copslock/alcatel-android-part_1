/* $Author: mplcsds1 $ */
/* $DateTime: 2016/03/28 23:03:22 $ */
/* $Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/ims/dpl/inc/qpdplloc.h#1 $ */


/************************************************************************
 Copyright (c) 2004-2006 Qualphone, Inc. All rights reserved.

 File Name      : qpdpl_qmi_handler.h
 Description    : Device Portability Layer qmi interface file.
 
 Revision History
 ========================================================================
    Date    |   Author's Name    |  BugID  |        Change Description
 ========================================================================
 20-May-2015     Shashank           826013    Initial version
***********************************************************************/ 

#ifndef __QPDPL_LOC_H
#define __QPDPL_LOC_H

#ifdef FEATURE_IMS_QMI_LOC_QCCI_CLIENT

#include "qpdpl_qmi_handler.h"
#include <qpdpl_qmi_defs.h>

#define MAX_NUMBER_OF_LOC_MODULES 3

typedef enum eDplQmiLocReqModule
{
  QCE_QMI_LOC_REQ_MODULE_MIN        = -1,
  QCE_QMI_LOC_REQ_MODULE_NORMAL_REG = 0,
  QCE_QMI_LOC_REQ_MODULE_EMER_REG   = 1,
  QCE_QMI_LOC_REQ_MODULE_MAX
}QCE_QMI_LOC_REQ_MODULE;

typedef enum eDplQmiLocReqResponse
{
  QCE_QMI_LOC_REQ_RESPONSE_INVALID_ARG,
  QCE_QMI_LOC_REQ_RESPONSE_ALREAY_USED_ARG,
  QCE_QMI_LOC_REQ_RESPONSE_PREV_IN_PROGRESS,
  QCE_QMI_LOC_REQ_RESPONSE_FAIL,
  QCE_QMI_LOC_REQ_RESPONSE_SUCCESS
}QCE_QMI_LOC_REQ_RESPONSE;

typedef enum eDplQmiLocReqIndication
{
  QCE_QMI_LOC_REQ_INDICATION_READY   = 0,
  QCE_QMI_LOC_REQ_INDICATION_FAIL,
  QCE_QMI_LOC_REQ_INDICATION_SUCCESS
}QCE_QMI_LOC_REQ_INDICATION;

QPINT qpDplInitLocQmi(QPVOID);
QPINT qpDplDeInitLocQmi(QPVOID);

/**
 * \fn typedef QPVOID (*QPLOC_POS_CB)(ims_qmi_loc_get_best_available_position_ind* tLocPosCbData, QPVOID* pUserData);
 * \brief Geo Location Callback Function Pointer
 *
 * \param tLocPosCbData is the returned Data with callback function
 * \param pUserData is passed by the APP and it returned to APP, APP is responsible to interpret it
 * \return QPVOID
 * \remarks None.
 */
typedef QPVOID (*QPLOC_POS_CB)(QCE_QMI_LOC_REQ_INDICATION eLocReqIndication, ims_qmi_loc_get_best_available_position_ind* tLocPosCbData, QPVOID* pUserData);

typedef struct
{
  QCE_QMI_LOC_REQ_MODULE eRequestId;
  QPLOC_POS_CB           tLocPosCb;
  QPVOID*                pUserData;
}QPDPL_LOC_CALLER_INFO;

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

/**
 * \fn QP_IMPORT_C QCE_QMI_LOC_REQ_RESPONSE qpDplGetGeoLocation(QPLOC_POS_CB tLocPosCb, QPVOID* pUserData);
 * \brief Register callback function to Get Geo Location
 *
 * Register callback function to Get Geo Location
 *
 * \param tLocPosCb - pointer to QPLOC_POS_CB
 * \param pUserData - pointer to user data
 * \return QCE_QMI_LOC_REQ_RESPONSE
 * \remarks None
 */
QP_IMPORT_C QCE_QMI_LOC_REQ_RESPONSE qpDplGetGeoLocation(QCE_QMI_LOC_REQ_MODULE eLocReqModule, QPLOC_POS_CB tLocPosCb, QPVOID* pUserData);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif//ifdef FEATURE_IMS_QMI_LOC_QCCI_CLIENT

#endif // __QPDPL_LOC_H