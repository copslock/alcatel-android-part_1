﻿/* $Author: */
/* $DateTime:*/
/* $Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/ims/dpl/inc/qpDplGba.h#1 $ */

/******************************************************************************
 Copyright (C) 2008 Qualcomm. All Rights Reserved.

 File Name      : qpDplGba.h
 Description    : Implements BootStrap Gba procedure.
 
    Revision History
 ==============================================================================
    Date    |   Author's Name    |  BugID  |        Change Description
 ==============================================================================
 5-May-2014    nishantk     -     789668      FR25901: DPL changes for migrating IMS over to UIM GBA solution
-------------------------------------------------------------------------------

************************************************************************/
 
#ifndef __QPDPLGBA_H_
#define __QPDPLGBA_H_

#include "ims_variation.h"
#include <qpPlatformConfig.h>
#include <gba_lib.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus


#define QP_GBA_NAF_SECURITY_PROTOCOL_LEN      5
#define QP_GBA_KS_NAF_LEN                     32
#define QP_GBA_MAX_BTID_LEN                   255
#define QP_GBA_MAX_LIFETIME_LEN               25
#define QP_GBA_MAX_NAF_FQDN_LEN               255
#define QP_GBA_MAX_IMPI_LEN                   255


/***************** Enums **********************/

/** 
 * \enum QP_GBA_RESULT_ENUM_TYPE
 * \brief GBA result type
 *  
 * Specifies the results of GBA procedure.
 */

typedef enum {
  /* Successful GBA command processed */
  QP_GBA_SUCCESS,
  /* Generic internal error */
  QP_GBA_GENERIC_ERROR,
  /* Invalid input parameter(s) */
  QP_GBA_INCORRECT_PARAMS,
  /* Not enough memory available */
  QP_GBA_MEMORY_ERROR_HEAP_EXHAUSTED,
  /* Command/parameter not supported */
  QP_GBA_UNSUPPORTED,
  /* Authentication failure */
  QP_GBA_AUTH_FAILURE,
  /* Card error causing GBA failure */
  QP_GBA_SIM_ERROR,
  /* Card is not ready. GBA unable to proceed */
  QP_GBA_SIM_NOT_READY,
  /* Network failure causing GBA failure */
  QP_GBA_NETWORK_ERROR,
  /* Network is not yet ready */
  QP_GBA_NETWORK_NOT_READY,
  /* Command process timed out */
  QP_GBA_TIMEOUT,
  /* Command was cancelled */
  QP_GBA_CANCELLED,
  /* Server error from BSF */
  QP_GBA_SERVER_ERROR
} QP_GBA_RESULT_ENUM_TYPE;



/** 
 * \enum qpe_card_seesion_type
 * \brief card application session present on the card
 *  
 * Specifies the card application session 
 */
typedef enum qpe_card_seesion_type 
{
  QC_UNKNOWN_GBA  = -1,
  QC_GBA_3GPP_PROV_SESSION_PRI,
  QC_GBA_3GPP_PROV_SESSION_SEC,
  QC_GBA_NON_PROV_SESSION_SLOT_1,
  QC_GBA_NON_PROV_SESSION_SLOT_2,
  QC_GBA_SESSION_MAX
} QPE_GBA_SESSION_TYPE;


/* ====== Structures ======= **/

/**
 * \struct QpBTIDData
 *
 * Structure holds to hold BTid data and length.
 */
typedef struct QpBTIDData
{
  QPCHAR    sData[QP_GBA_MAX_BTID_LEN];
  QPUINT16  iLen;

}QP_BT_ID_DATA;

/**
 * \struct QpFQDNTYPEData
 *
 * Structure holds to hold FQDN data and length.
 */
typedef struct QpFQDNTYPEData
{
  QPCHAR    sData[QP_GBA_MAX_NAF_FQDN_LEN];
  QPUINT16  iLen;

}QP_FQDN_TYPE_DATA;

/**
 * \struct QpLifeTimeData
 *
 * Structure holds to hold Life time data and length.
 */
typedef struct QpLifeTimeData
{
  QPCHAR    sData[QP_GBA_MAX_LIFETIME_LEN];
  QPUINT16  iLen;

}QP_LIFE_TIME_DATA;

/**
 * \struct QpImpiData
 *
 * Structure holds to hold IMPI data and length.
 */
typedef struct QpImpiData
{
  QPCHAR    sData[QP_GBA_MAX_IMPI_LEN];
  QPUINT16  iLen;

}QP_IMPI_DATA;

/**
 * \struct QpNafIdType
 *
 * Structure holds QP_FQDN_TYPE_DATA and security protocol type provide by Application to start bootstrap.
 */
typedef struct QpNafIdType
{
  QP_FQDN_TYPE_DATA   sFQDNType;
  QPUINT8             sNAFSecurityProtocolType[QP_GBA_NAF_SECURITY_PROTOCOL_LEN];
}QP_NAF_ID_TYPE;

/**
 * \struct QpBootStrapesult
 *
 * Structure holds key data for Aplication in response to the BootStrap procedure.
 */
typedef struct QpBootStrapesult
{
  QPUINT8                 sKsNaf[QP_GBA_KS_NAF_LEN];
  QP_BT_ID_DATA           sBtid;
  QP_LIFE_TIME_DATA       sLifetime;
  QP_IMPI_DATA            sImpi;
  QPUINT16                error_code;
}QP_BOOTSTRAP_RESULT;

/**
 * \var typedef QPVOID (*QP_BOOTSTRAP_CALLBACK)(QP_BOOTSTRAP_RESULT* pParam1, 
 *                                          QPVOID* pUserData);
 *                                          
 * QP_BOOTSTRAP_CALLBACK is function pointer to the response callback function.
 */
typedef QPVOID (*QP_BOOTSTRAP_CALLBACK)(QP_GBA_RESULT_ENUM_TYPE sGbaResultType,QP_BOOTSTRAP_RESULT* n_pGBAResult, 
																		QPVOID* pUserData);

/**
 * \struct QpBootStrapUserData
 * Callback parameter for BootStrap APIs
 *
 * Structure holds UserData and a User Callback to be called by DPL for BootStrap operations.
 */
typedef struct QpBootStrapUserData
{

  QPVOID*                   pUserData;
  QP_BOOTSTRAP_CALLBACK     BootStrapCallBack;

}QP_BOOTSTRAP_USERDATA;

/**
 * \struct qp_bootstrap_param
 *
 * Structure holds sNafIdPtr provided by Application to start BootStrap procedure.
  * Structure holds BootStrapUserData where Application userdata and callback will be stored.
 */
typedef struct qp_bootstrap_param
{
    QP_NAF_ID_TYPE            sNafIdPtr;
    QPCHAR                    *SApnData;
    QP_BOOTSTRAP_USERDATA     BootStrapUserData;
}QP_BOOTSTRAP_PARAM;

/**
 * \struct qp_gba_key_param
 *
 * Structure holds QP_BT_ID_DATA and QP_LIFE_TIME_DATA to to validate the GBA key.
 */
typedef struct qp_gba_key_param
{
    QP_BT_ID_DATA          sBtidPtr;
    QP_LIFE_TIME_DATA      sLifeTimePtr;
}QP_GBA_KEY_PARAM;

typedef struct
{
  QP_GBA_RESULT_ENUM_TYPE sGbaResultType;
  QPUINT32               sRequestHandle;
  QP_BOOTSTRAP_RESULT   sGbaResponse;
} QP_GBA_INFO_EVENT_CB_STRUCT;

/********************* APIS ***********************/

/**
 * \fn QP_IMPORT_C QPBOOL qpDplStartBootstrapProcedure(QP_BOOTSTRAP_PARAM ,QPE_CARD_DATA,eForceBootstrap);
 * \brief gives the Application to start gba_bootstrap 
 * \brief bSuccess QP_TRUE will successfully initiated
 * \brief bSuccess QP_FALSE unable to initaite
 * \remarks None.
 */

QP_IMPORT_C QPBOOL qpDplStartBootstrapProcedure(QP_BOOTSTRAP_PARAM *n_pGBAParam,QPE_CARD_DATA eCardType,QPBOOL eForceBootstrap);

/**
 * \fn QP_IMPORT_C QPBOOL qpDplGBAKeyValid(QP_GBA_KEY_PARAM,QPE_CARD_DATA);
 * \brief gives the Application to check valid key for GBA
 * \brief bSuccess QP_TRUE is valid Key
 * \brief bSuccess QP_FALSE is not valid
 * \remarks None.
 */

QP_IMPORT_C QPBOOL qpDplGBAKeyValid(QP_GBA_KEY_PARAM *n_pCheckGBAParam,QPE_CARD_DATA eCardType);

/**
 * \fn QP_IMPORT_C QPBOOL qpDplGBACancel(QP_GBA_KEY_PARAM,QPE_CARD_DATA);
 * \brief gives the Application to check valid key for GBA
 * \brief bSuccess QP_TRUE is valid Key
 * \brief bSuccess QP_FALSE is not valid
 * \remarks None.
 */

QP_IMPORT_C QPBOOL qpDplGBACancel(QPE_CARD_DATA eCardType);


#ifdef __cplusplus
}
#endif // __cplusplus
#endif
/** @} */ // end of group DPL_SEC