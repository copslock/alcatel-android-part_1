//
// Copyright (c) 2004-2006 Qualphone, Inc. All rights reserved.
//
//
/**
 * \file qpdefinesCpp.h
 * \brief DPL main macro definitions.
 *
 * \ingroup DPL
 * DPL - Device Portability Layer's macro definitions. 
 * This module incorporates some frequently used ANSI C stdlib functions
 * as defined by the ANSI standard. If the ANSI implementation is unavailable
 * for a specific target, an equivalent implementaion has to be done and 
 * the function prototypes to be included in qpPlatformConfig.h header file.
 */
/************************************************************************
 Revision History
 ========================================================================
    Date    |   Author's Name    |  BugID  |        Change Description
 ========================================================================
21-04-15        Shashank                    Moved new() and delete()
************************************************************************/

 /** @defgroup DPL_MACROS The Device Portability Layer's macro definitions
 * \ingroup DPL
 *  @{
 */
#ifndef __QPDEFINESCPP_H
#define __QPDEFINESCPP_H

#include "ims_variation.h"
#include "qpPlatformConfig.h"
#include <memheap.h>
#include <modem_mem.h>

#ifdef __cplusplus
inline void* operator new     ( size_t size, int id ) { return modem_mem_alloc(size, MODEM_MEM_CLIENT_IMS_CRIT); }
inline void* operator new[]   ( size_t size, int id ) { return modem_mem_alloc(size, MODEM_MEM_CLIENT_IMS_CRIT); }
#ifndef FEATURE_IMS_PC_TESTBENCH
inline void  operator delete  ( void* ptr) { modem_mem_free( ptr, MODEM_MEM_CLIENT_IMS_CRIT); }
inline void  operator delete[]( void* ptr) { modem_mem_free( ptr, MODEM_MEM_CLIENT_IMS_CRIT); }
#endif

/**
 * \def IMS_NEW(X, id, ptr, p)
 * creates object of class x with parameters to constructor specified by p
 * id - Module Id
 * \usage
 * IMS_NEW(X, RTP_MODULE_ID, ptr, ()) -> creates object of class X with default constructor. Similar to (ptr = new X();)
 * IMS_NEW(X, RTP_MODULE_ID, ptr, (port, ip)) -> creates object of class X with constructor accepting port and IP. 
 * Similar to (ptr = new X(port, IP);)
 * \remarks IMS_NEW(x, id, ptr, p)
  */
#define IMS_NEW(x, id, ptr, p)\
  {\
    ptr = new (id) x p;\
    qcmemlog_add(id,__FILE__, __LINE__, (void*)ptr, sizeof(x));\
  }

/**
 * \def IMS_DELETE(ptr, id)
 * deletes object ptr. Similar to (delete ptr;)
 * id - Module Id
 * 
 * \remarks IMS_DELETE(ptr, id);
 */
#define IMS_DELETE(ptr, id)\
  if (NULL != ptr)\
  {\
    qcmemlog_remove(id, (void*)ptr);\
    ::delete ptr;\
  }

/**
 * \def IMS_NEW_ARRAY(x, id, ptr, n)
 * creates an array of objects of class x with default constructor only
 * id - Module Id, n - size of array
 * \usage
 * IMS_NEW_ARRAY(X, RTP_MODULE_ID, ptr,2) - creates array of object of type class X with default constructor. 
 * Similar to (ptr = new X[2];)
 * \remarks IMS_NEW_ARRAY(x, id, ptr, n)
 */
#define IMS_NEW_ARRAY(x, id, ptr, n)\
  {\
    ptr = new (id) x [ n ];\
    qcmemlog_add(id,__FILE__, __LINE__, ptr, (sizeof(x)*n));\
  }

/**
 * \def IMS_DELETE_ARRAY(ptr, id)
 * deletes object ptr. Similar to (delete ptr;)
 * id - Module Id
 * 
 * \remarks IMS_DELETE_ARRAY(ptr, id);
 */
#define IMS_DELETE_ARRAY(id, ptr)\
  {\
    qcmemlog_remove(id, ptr);\
    ::delete [] ptr;\
  }
#endif

#endif // __QPDEFINES_H
/** @} */ // end of group DPL_MACROS