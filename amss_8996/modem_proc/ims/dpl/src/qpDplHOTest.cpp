﻿#ifdef FEATURE_IMS_PC_TESTBENCH 
#define QPCMSG_DPL_HO_TEST_EV_MSG 2
#include <windows.h>
#include <tchar.h>
#include <strsafe.h>
#include <iostream>
#include <fstream>
#include<string>
extern "C"
{
#include "qpDplHandOver.h"
}
#include <qpdpl.h>

#ifdef FEATURE_IMS_PC_TESTBENCH 
QPC_GLOBAL_DATA* pDplHOTESTGlobalData = NULL;
int ltedl_rsrq;
int ltedl_snr;
#endif

static QPBOOL once = QP_TRUE;
static QPBOOL cmdblock = QP_FALSE; 
//extern int ltedl_rsrq;
//extern int ltedl_snr;
QPVOID hoTestCallback(QP_HANDOVER_STATUS_TYPE *pHOStatus, QPVOID *pAppData)
{
  printf("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
  printf("TESTCALLBACK pHOSatus->isPossible:%d   from SRAT:%d to TRAT:%d \n", pHOStatus->bIsHOPossible, pHOStatus->eSRAT, pHOStatus->eTRAT);
  printf("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
	
}
 QP_DPL_RAT_QUALITY convertQualityToEnum (std::string quality)
 {
	 printf("quality is %s\n", quality.c_str());
	 if (quality.compare("good")==0) return RAT_QUAL_GOOD;
	 if (quality.compare("bad")==0) return RAT_QUAL_BAD;
	 if (quality.compare("medium")==0) return RAT_QUAL_MEDIUM;
	 if (quality.compare("invalid")==0) return RAT_QUAL_INVALID;
	 if (quality.compare("medium_high")==0) return RAT_QUAL_MEDIUM_HIGH;
	 if (quality.compare("medium_low")==0) return RAT_QUAL_MEDIUM_LOW;
	 return RAT_QUAL_INVALID;
}

 QP_DPL_RAT_QUALITY convertMaskToEnum (std::string quality)
 {   
	 printf("quality is %s\n", quality.c_str());
	 //Possible srat combos
	 //profile1|profile3 ==> 1notmet, 1met|3met, 1met|3notmet
	 if (quality.compare("1nm")==0) return RAT_QUAL_BAD;
	 if (quality.compare("1m3m")==0) return RAT_QUAL_GOOD;
     if (quality.compare("1m3nm")==0) return RAT_QUAL_MEDIUM_LOW;

	 //Possible trat combos
     //profilew ==> 2notmet, 2met
	 if (quality.compare("2nm")==0) return RAT_QUAL_BAD;
     if (quality.compare("2m")==0) return RAT_QUAL_GOOD;

	 return RAT_QUAL_INVALID;
}

 QPUINT64 convertMaskTo64bitMask(std::string quality)
 {
	 //Possible srat combos
	 //profile1|profile3 ==> 1notmet, 1met|3met, 1met|3notmet
	 if (quality.compare("1nm")==0) return DS_SYS_WQE_PROFILE_INACTIVE;
	 if (quality.compare("1m3m")==0) return DS_SYS_WQE_PROFILE_IMS_TYPE_1|DS_SYS_WQE_PROFILE_IMS_TYPE_3;
     if (quality.compare("1m3nm")==0) return DS_SYS_WQE_PROFILE_IMS_TYPE_1;
	 	 
	 //Possible trat combos
     //profilew ==> 2notmet, 2met
	 if (quality.compare("2nm")==0) return DS_SYS_WQE_PROFILE_INACTIVE;
     if (quality.compare("2m")==0) return DS_SYS_WQE_PROFILE_IMS_TYPE_1|DS_SYS_WQE_PROFILE_IMS_TYPE_2|DS_SYS_WQE_PROFILE_IMS_TYPE_3;

	 return QUAL_MASK_INVALID_64;
 }

QPE_DPL_METRIC_MODULE convertModuleToEnum(std::string module)
{
	printf("module is %s\n", module.c_str());
	if(module.compare("ltedl") == 0) return QPE_METRIC_LTE_DL_MODULE;
	if(module.compare("lteul") == 0) return QPE_METRIC_LTE_UL_MODULE;
	if(module.compare("mediaul") == 0) return QPE_METRIC_MEDIA_UL_MODULE;
	if(module.compare("mediadl") == 0) return QPE_METRIC_MEDIA_DL_MODULE;
	if(module.compare("wlandl") == 0) return QPE_METRIC_WiFi_DL_MODULE;
	if(module.compare("wlanul") == 0) return QPE_METRIC_WiFi_UL_MODULE;
	if(module.compare("wlan") == 0) return QPE_METRIC_WiFi_MODULE;
    
	return QPE_METRIC_MODULE_NONE;
}

QPE_DCM_RAT convertRatToEnum(std::string rat)
{
	printf("module is %s\n", rat.c_str());
	if(rat.compare("lte") == 0) return DCM_RAT_LTE;
	if(rat.compare("iwlan") == 0) return DCM_RAT_IWLAN;
	if(rat.compare("none") == 0) return DCM_RAT_NONE;
	if(rat.compare("cs") == 0) return DCM_RAT_CS;
	return DCM_RAT_NONE;
}

static std::string eEventcmd = "";

extern "C"
{
extern QPBOOL qpDplPostEventToEventQueue(QPUINT16 iMsgId, QPINT32 iParam, QPVOID* pParam, QPC_GLOBAL_DATA* pGlobalData);
}

void executeCommand(std::string cmdline)
{

	 if(cmdline.c_str()[0] == '#' || cmdline.c_str()[0] == ' ')
		return; //comment line
	 //handle sleep command in this thread
	 std::string params[6];
	 std::string delim = " ";
     int counter = 0;
	 size_t iterbegin = 0;
	 size_t iterend = cmdline.find(delim);
	 while (iterend != std::string::npos)
	 {
		 params[counter] =  cmdline.substr(iterbegin, iterend-iterbegin);
		 iterbegin = iterend+1;
		 iterend = cmdline.find(delim, iterbegin);
		 counter++;
	 }

	if(params[0].compare("sleep") == 0)
	{
		printf("sleeping for %s seconds before next command\n", params[1].c_str());
		int sec = atoi(params[1].c_str());
		Sleep(sec * 1000);
		return;
	}
	 if(1)
	     //IMS_MSG_LOW(LOG_IMS_DPL, "Inside callback function DplProxyCb() with ecmd[%d]\n", ecmd, 0, 0);

		eEventcmd = cmdline;
		if (QP_FALSE == qpDplPostEventToEventQueue(QPCMSG_DPL_HO_TEST_EV_MSG, 0, 0,pDplHOTESTGlobalData))
		{
		 //MSG_ERROR("qpDplDSSysEvCB - FAILURE in posting event", 0, 0, 0);
		}
		if(cmdblock == QP_FALSE)
			{
				cmdblock = QP_TRUE;
				
			}
			while(cmdblock == QP_TRUE)
			{
				Sleep(100); //crappy logic for now
			}
		 return;
}

void executeCommandInternal(std::string cmdline)
{
	std::string params[6];
	 std::string delim = " ";
     int counter = 0;
	 size_t iterbegin = 0;
	 size_t iterend = cmdline.find(delim);
	 while (iterend != std::string::npos)
	 {
		 params[counter] =  cmdline.substr(iterbegin, iterend-iterbegin);
		 iterbegin = iterend+1;
		 iterend = cmdline.find(delim, iterbegin);
		 counter++;
	 }

	//if(params[0].compare("sleep") == 0)
	//{
	//	printf("sleeping for %s seconds before next command\n", params[1].c_str());
	//	int sec = atoi(params[1].c_str());
	//	Sleep(sec * 1000);
	//	return;
	//}

	if(params[0].compare("report") == 0)
	{
		printf("report state %s for module %s\n", params[2].c_str(), params[1].c_str());
		QP_DPL_RAT_QUALITY quality = convertQualityToEnum(params[2]);
		QPE_DPL_METRIC_MODULE module = convertModuleToEnum(params[1]);
		QP_DPL_MEASUREMENT_REPORT_STATUS report;
		report.stat = quality;
		qpDplMetricReport(module, report);
		return;
	}

	if(params[0].compare("reportmask") == 0)
	{
		printf("report mask %s for module %s\n", params[2].c_str(), params[1].c_str());
		//QP_DPL_RAT_QUALITY quality = convertQualityToEnum(params[2]);
		QPE_DPL_METRIC_MODULE module = convertModuleToEnum(params[1]);
		QP_DPL_MEASUREMENT_REPORT_STATUS report;
		if(module == QPE_METRIC_WiFi_MODULE)
		{
			if(0 == params[3].compare("tmo") || 0 == params[3].compare("ee") || 0 == params[3].compare("ir92") )//tmo
			{
				report.stat = convertMaskToEnum(params[2]);
				report.wifi_quality_mask_64bit = convertMaskTo64bitMask(params[2]);
			}
			else //vzw/att
				report.wifi_quality_mask = (QPUINT32) atoi (params[2].c_str());
		}
		else //assume lte dl
			report.lte_dl_quality_mask =(QPUINT32) atoi (params[2].c_str());
		qpDplMetricReport(module, report);
		return;
	}

  if(params[0].compare("UpdateHOConfig") == 0)
  {
    // UpdateHOConfig LTE WiFi 1x 1
    IMS_HO_CONFIG_ITEM pHOConfigBuf;

		printf("setting algo flavour=%d\n", params[4].c_str());

    pHOConfigBuf.s1xConfig.bImsHO_1x_Config_Valid = QP_TRUE;
    pHOConfigBuf.s1xConfig.iIMSHO_1x_QualTH = -10;

    pHOConfigBuf.sLTEConfig.bIMSHO_LTE_Config_Valid = QP_TRUE;
    pHOConfigBuf.sLTEConfig.bImsHO_LTE_TH1_Valid = QP_TRUE;
    pHOConfigBuf.sLTEConfig.bImsHO_LTE_TH2_Valid = QP_TRUE;
    pHOConfigBuf.sLTEConfig.bImsHO_LTE_TH3_Valid = QP_TRUE;

    pHOConfigBuf.sLTEConfig.iIMSHO_LTE_Qual_TH1 = -125;
    pHOConfigBuf.sLTEConfig.iIMSHO_LTE_Qual_TH2 = -120;
    pHOConfigBuf.sLTEConfig.iIMSHO_LTE_Qual_TH3 = -115;

    pHOConfigBuf.sWiFiConfig.bIMSHO_WiFi_Config_Valid = QP_TRUE;
    pHOConfigBuf.sWiFiConfig.bImsHO_WIFI_THA_Valid = QP_TRUE;
    pHOConfigBuf.sWiFiConfig.bImsHO_WIFI_THB_Valid = QP_TRUE;

    pHOConfigBuf.sWiFiConfig.iIMSHO_WIFI_Qual_THA = -75;
    pHOConfigBuf.sWiFiConfig.iIMSHO_WIFI_Qual_THB = -85;

    DPLUpdateHOConfig(&pHOConfigBuf);
  }

	if(params[0].compare("setparam") == 0)
	{
		printf("setting SRAT=%s, TRAT=%s, prefRAT=%s\n", params[1].c_str(), params[2].c_str(), params[3].c_str());
		QP_HANDOVER_PARAMS_TYPE p;
		    p.eSRAT = convertRatToEnum(params[1]); 
      if(p.eSRAT == DCM_RAT_CS)
      {
        p.eSRAT_MASK = HO_SERVICE_MASK_NONE;
      }
			p.eTRAT = convertRatToEnum(params[2]);
      if(p.eTRAT == DCM_RAT_CS)
      {
        p.eTRAT_MASK = HO_SERVICE_MASK_NONE;
      }
      else
      {
        p.eTRAT_MASK = HO_SERVICE_MASK_VOICE_OR_VIDEO;
      }
			p.ePrefRAT = convertRatToEnum(params[3]);
			if(params[4].compare("true") == 0) {
				p.bInCall = QP_TRUE;
			}
			else
			{
				p.bInCall = QP_FALSE;
			}

			if(params[5].compare("0") == 0)
			{
				p.call_type = QPE_CALL_TYPE_VOICE;
			}
			else
			{
				p.call_type = QPE_CALL_TYPE_VOICE;
			}


		  qpDplHandOverParamsSet(&p, (QP_DPL_HO_STATUS_CB)hoTestCallback, QP_NULL);
		return;
	}
	if(params[0].compare("setmockltedlmetrics") == 0)
	{
		ltedl_rsrq = atoi(params[1].c_str());
		ltedl_snr = atoi(params[2].c_str());
		printf("setmockltedlmetrics ltedl_rsrq :: %p  ltedl_snr :: %p\n", params[1].c_str(), params[2].c_str());
		return;
	}

	printf("skipping unknown command %s\n", params[0].c_str());
	
	return;
}



DWORD WINAPI MyThreadFunction( LPVOID lpParam ) 
{
	std::string line;
	std::ifstream text ("c:\\Dropbox\\hocmds.txt");
    
	if (text.is_open())
	{
		while (text.good())
		{
			getline(text,line);
			
			executeCommand(line+" ");
			
		}

		text.close();
	}

    while(1)
	{
		Sleep(100000); //sleep foreever
	}
	return 0;
}

#ifdef __cplusplus
     extern "C" {
#endif
         void startHoTest(void);
		 int DplHOTestCb();
#ifdef __cplusplus
     }
#endif

int DplHOTestCb()
{
	executeCommandInternal(eEventcmd);
	cmdblock = QP_FALSE;
	return 0;
}


void startHoTest(void)
{

	//start a thread to parse and execute commands
	int* pData;
	DWORD   dwThreadId;
	HANDLE  hThread;
		if(once == QP_FALSE)
		return;
	once = QP_FALSE;
	pData = (int*) HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY,
                sizeof(int));
	hThread = CreateThread( 
            NULL,                   // default security attributes
            0,                      // use default stack size  
            MyThreadFunction,       // thread function name
            pData,          // argument to thread function 
            0,                      // use default creation flags 
            &dwThreadId);   // returns the thread identifier 

    if(pData)
	{
		HeapFree(GetProcessHeap(),HEAP_NO_SERIALIZE ,(LPVOID)pData);
		pData = NULL;
	}

	CloseHandle(hThread);
}

#endif