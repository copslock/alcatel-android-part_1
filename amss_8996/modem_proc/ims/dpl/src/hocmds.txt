#test scenarios for ho
#valid commands are:
#
# *** cmds ***
#sleep <seconds>
#report <srcmodule> <state>
#reportmask <srcmodule> <mask> <algo_type>
#setparam <srat> <trat> <prefrat> <bInCall> <calltype>
#setparamformodule <module_name> <cmd>
#setmockltedlmetrics <RSRQ> <SNR>
#setmocklteulmetrics <rb_id> <20 values> <rb_id> <20 values> ..... <rb_id> <20 values>
#
# *** cmd params ***
#<calltype> 0(voice) | 1 (vt)
#<bInCall = true | false
#srcmodule/module_name = lteul | ltedl | wlanul | wlandl | mediaul | mediadl
#state = inavlid | good | bad | medium
#srat/trat/prefrat = lte | iwlan | invalid
#cmd = init | start | stop
#rb_id = 0 ... 7
# 
#NOTE: masks
#for vzw wifi mask bits correspond to A, B, C, D  (A is least significant bit)
#for vzw lte mask bits correspond to LTE1, LTE2, LTE3, LTETH1, LTETH2 (LTE1 is least significant bit)
#for tmo/ee wifi mask bits correspond to SRAT, TRAT, REPOINT (SRAT is least significant bit)
# <------ start script ------------>
sleep 5

#srat=iwlan, trat=lte, pref=iwlan incall=true, call_type=audio
setparam iwlan lte iwlan true 0


sleep 2


#wifi reports good
reportmask wlan 1m3m

#wifi good+preferred --> stop lte

sleep 2

#wifi reports medium_low
reportmask wlan 1m3nm 0

#restart lte measurements

#lteul reports good
report lteul good

#ltedl reports good
report ltedl good

#report repoint after lte measurements are valid
##Based on above, we should report HO not possible + repoint.

#wifi reports bad
reportmask wlan 1nm 0

##Based on above, we should report HO possible.

