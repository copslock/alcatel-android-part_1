/*=*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

cvd_undef_hdr. h

GENERAL DESCRIPTION

This file contains undefined CVD header section .

Copyright (c) 2015 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is
regulated by the U.S. Government. Diversion contrary to U.S. law prohibited.


                            EDIT HISTORY FOR FILE


when        who    what, where, why
--------    ---    ----------------------------------------------------------
12/08/15   manjunat  initial creation
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*==*=*/

#ifndef __CVD_UNDEF_HDR_H
#define __CVD_UNDEF_HDR_H

#include "ims_variation.h"

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

#define TEST_CODE 1

/** G.711 a-law V2 vocoder format; contains 20 ms vocoder frame. */
#ifndef VSS_MEDIA_ID_G711_ALAW_V2
#define VSS_MEDIA_ID_G711_ALAW_V2 ( 0x00010FCD )
#endif

/** G.711 mu-law V2 vocoder format; contains 20 ms vocoder frame. */
#ifndef VSS_MEDIA_ID_G711_MULAW_V2
#define VSS_MEDIA_ID_G711_MULAW_V2 ( 0x00010FCE )
#endif

/** Enhanced Voice Services (EVS) vocoder format. */
#ifndef VSS_MEDIA_ID_EVS
#define VSS_MEDIA_ID_EVS ( 0x00010FD8 )
#endif

#ifndef VSS_ISTREAM_CMD_SET_EVS_VOC_ENC_OPERATING_MODE
#define VSS_ISTREAM_CMD_SET_EVS_VOC_ENC_OPERATING_MODE ( 0x00013166 )

/** Bandwidth is Narrowband for EVS. */
#define VSS_ISTREAM_EVS_VOC_BANDWIDTH_NB ( 0 )

/** Bandwidth is Wideband for EVS. */
#define VSS_ISTREAM_EVS_VOC_BANDWIDTH_WB ( 1 )

/** Bandwidth is Super-wideband for EVS. */
#define VSS_ISTREAM_EVS_VOC_BANDWIDTH_SWB ( 2 )

/** Bandwidth is Fullband for EVS. */
#define VSS_ISTREAM_EVS_VOC_BANDWIDTH_FB ( 3 )

/* Type definition for vss_istream_cmd_set_evs_voc_enc_operating_mode_t. */
typedef struct vss_istream_cmd_set_evs_voc_enc_operating_mode_t vss_istream_cmd_set_evs_voc_enc_operating_mode_t;

/** @weakgroup weak_vss_istream_cmd_set_evs_voc_enc_operating_mode_t
@{ */
/* Payload structure for the VSS_ISTREAM_CMD_SET_EVS_VOC_ENC_OPERATING_MODE
    command.
*/
struct vss_istream_cmd_set_evs_voc_enc_operating_mode_t
{
   uint8_t mode;
   /**< Sets the operating bit-rate on the vocoder encoder.
      
        @values
        - 0x00000000 --  6.60 kbps, AMR-WB IO (WB)
        - 0x00000001 --  8.85 kbps, AMR-WB IO (WB)
        - 0x00000002 -- 12.65 kbps, AMR-WB IO (WB)
        - 0x00000003 -- 14.25 kbps, AMR-WB IO (WB)
        - 0x00000004 -- 15.85 kbps, AMR-WB IO (WB)
        - 0x00000005 -- 18.25 kbps, AMR-WB IO (WB)
        - 0x00000006 -- 19.85 kbps, AMR-WB IO (WB)
        - 0x00000007 -- 23.05 kbps, AMR-WB IO (WB)
        - 0x00000008 -- 23.85 kbps, AMR-WB IO (WB)
        - 0x00000009 -- 5.90 kbps, EVS (NB, WB)
        - 0x0000000A -- 7.20 kbps, EVS (NB, WB)
        - 0x0000000B -- 8.00 kbps, EVS (NB, WB)
        - 0x0000000C -- 9.60 kbps, EVS (NB, WB, SWB)
        - 0x0000000D -- 13.20 kbps, EVS (NB, WB, SWB)
        - 0x0000000E -- 16.40 kbps, EVS (NB, WB, SWB, FB)
        - 0x0000000F -- 24.40 kbps, EVS (NB, WB, SWB, FB)
        - 0x00000010 -- 32.00 kbps, EVS (WB, SWB, FB)
        - 0x00000011 -- 48.00 kbps, EVS (WB, SWB, FB)
        - 0x00000012 -- 64.00 kbps, EVS (WB, SWB, FB)
        - 0x00000013 -- 96.00 kbps, EVS (WB, SWB, FB)
        - 0x00000014 -- 128.00 kbps, EVS (WB, SWB, FB) @tablebulletend */

  uint8_t bandwidth;
  /**< Sets the operating audio bandwidth on the vocoder encoder.
    
        @values
        - #VSS_ISTREAM_EVS_VOC_BANDWIDTH_NB
        - #VSS_ISTREAM_EVS_VOC_BANDWIDTH_WB 
        - #VSS_ISTREAM_EVS_VOC_BANDWIDTH_SWB 
        - #VSS_ISTREAM_EVS_VOC_BANDWIDTH_FB @tbend2 */

};
/** @} */ /* end_weakgroup weak_vss_istream_cmd_set_evs_voc_enc_operating_mode_t */
#endif

#ifndef VSS_ISTREAM_CMD_SET_EVS_ENC_CHANNEL_AWARE_MODE_ENABLE
/** @ingroup cvd_cvs_cmd_set_evs_enc_channel_aware_mode_enable
  Enables the encoder channel aware mode for EVS

  @apr_msgpayload{vss_istream_cmd_set_evs_enc_channel_aware_mode_enable_t}
  @wktable{weak__vss__istream__cmd__set__evs_enc__channel__aware__mode__enable__t}

@detdesc
  This command will have no effect and will return an error when either:
  - The EVS vocoder is not being used by the stream
  - The payload parameters are invalid

  Channel aware mode operates only at 13.2kbps on WB or SWB bandwidths.

  It is recommended to enable channel aware mode before #VSS_ISTREAM_CMD_SET_EVS_VOC_ENC_OPERATING_MODE
  to start the feature before the bit rate changes to 13.2kbps.

  Valid payload parameters are specified in the payload descriptor.

  Please refer to 3GPP document TS 26.445 Version 12.0.0 Release 12 for more information
  about channel-aware mode.

  @return
  APRV2_IBASIC_RSP_RESULT (refer to @xhyperref{Q2,[Q2]}).

  @dependencies
  None.
*/
#define VSS_ISTREAM_CMD_SET_EVS_ENC_CHANNEL_AWARE_MODE_ENABLE ( 0x00013168 )

/* Type definition for vss_istream_cmd_set_evs_enc_channel_aware_mode_enable_t. */
typedef struct vss_istream_cmd_set_evs_enc_channel_aware_mode_enable_t vss_istream_cmd_set_evs_enc_channel_aware_mode_enable_t;


/** @weakgroup weak_vss_istream_cmd_set_evs_enc_channel_aware_mode_enable_t
@{ */
/* Payload structure for the VSS_ISTREAM_CMD_SET_EVS_ENC_CHANNEL_AWARE_MODE_ENABLE command.
*/
struct vss_istream_cmd_set_evs_enc_channel_aware_mode_enable_t
{
   uint8_t fec_offset;
   /**< Specifies the forward-error correction offset.

       @values 2, 3, 5, 7 */

   uint8_t fer_rate;
   /**< Specifies FER rate threshold to LOW (0) or HIGH (1). */

};

/** @} */ /* end_weakgroup weak_vss_istream_cmd_set_evs_enc_channel_aware_mode_enable_t */

/** @ingroup cvd_cvs_cmd_set_evs_enc_channel_aware_mode_disable
  Disables the encoder channel aware mode for EVS

@detdesc
  This command will have no effect and will return an error when either the EVS vocoder is 
  not being used or if Channel Aware mode is already disabled.

  @return
  APRV2_IBASIC_RSP_RESULT (refer to @xhyperref{Q2,[Q2]}).

  @dependencies
  None.
*/
#define VSS_ISTREAM_CMD_SET_EVS_ENC_CHANNEL_AWARE_MODE_DISABLE ( 0x0001316C )

/** @endcond */
#endif

#ifndef VSS_IMVM_CMD_SET_MAX_VAR_VOC_SAMPLING_RATE
/** @ingroup cvd_mvm_cmd_set_max_var_voc_sampling_rate
  Sets the max sampling rates to be used for vocoders that support variable
  sampling rates.

  @apr_msgpayload{vss_imvm_cmd_set_max_var_voc_sampling_rate_t}
  @wktable{weak__vss__imvm__cmd__set__max__var__voc__sampling__rate__t} 

  @detdesc
  This command will not have an effect when:
  - The following media types are not used via #VSS_IMVM_CMD_SET_CAL_MEDIA_TYPE:
    - #VSS_MEDIA_ID_EVS
  - The payload parameters are invalid

  When this command's effect is taken:
  - If the vocoder supports variable sampling rates and no maximum is
    provided to the MVM (i.e., this command is not called), the default
    maximum sampling rate for the media type is used.

  - If the sampling rate specified for the payload is 0, it is a default
    maximum sampling rate for the media type.

  Reconfiguration of PP sampling rates may occur after this command is sent as 
  the actual sampling rate setup on the system is an interal policy of CVD.

  @par
  Any combination of the valid sampling rates for Rx and Tx are supported.

  The valid sampling rates are:
  0 Hz – Default
  8000 Hz – Narrowband (NB)
  16000 Hz – Wideband (WB)
  32000 Hz – Super-wideband (SWB)
  48000 Hz – Fullband (FB)

  @par
 
  @return
  APRV2_IBASIC_RSP_RESULT (refer to @xhyperref{Q2,[Q2]}).

*/
#define VSS_IMVM_CMD_SET_MAX_VAR_VOC_SAMPLING_RATE ( 0x0001137C )

/* Type definition for vss_imvm_cmd_set_max_var_voc_sampling_rate_t. */
typedef struct vss_imvm_cmd_set_max_var_voc_sampling_rate_t vss_imvm_cmd_set_max_var_voc_sampling_rate_t;

/** @weakgroup weak_vss_imvm_cmd_set_max_var_voc_sampling_rate_t 
@{ */
/* Payload structure of the VSS_IMVM_CMD_SET_MAX_VAR_VOC_SAMPLING_RATE command.
*/
struct vss_imvm_cmd_set_max_var_voc_sampling_rate_t
{
  uint32_t rx;
  /**< Rx sampling rate. 
    
       @values 0 (Default), 8000 (NB), 16000 (WB), 32000 (SWB), 48000 (FB) Hz */

  uint32_t tx;
  /**< Tx sampling rate. 
    
       @values 0 (Default), 8000 (NB), 16000 (WB), 32000 (SWB), 48000 (FB) Hz */
};
/** @} */ /* end_weakgroup weak_vss_imvm_cmd_set_max_var_voc_sampling_rate_t */
#endif

#ifndef VOICECFG_CFG_IS_VOLTE_EAMR_ENABLED
#define VOICECFG_CFG_IS_VOLTE_EAMR_ENABLED ( 0x0001309F )
#endif
#ifdef __cplusplus
}
#endif // __cplusplus


#endif//__CVD_UNDEF_HDR_H

