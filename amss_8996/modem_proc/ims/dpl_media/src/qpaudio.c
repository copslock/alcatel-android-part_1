﻿/* $Author: mplcsds1 $ */
/* $DateTime: 2016/03/28 23:03:22 $ */
/* $Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/ims/dpl_media/src/qpaudio.c#1 $ */

/************************************************************************
Copyright (C) 2006  Qualphone, Inc. All Rights Reserved.

File Name      : qpaudio.c
Description    : Implements all the audio functionality for BREW environment.
Uses the IVocoder functionality
Revision History
========================================================================
Date    |   Author's Name    |  BugID  |        Change Description
========================================================================
02-Mar-2006    Amarnath             387    - Changes done in callback function
to post an event to the main Q.
- In ReadyCB function copying the 
framebundle to a copy frame bundle
- Adding FillAudioDplData & 
qpDplPostAudioEventToEventQueue 
utility functions.
- Removed all INFO and ERROR and 
------------------------------------------------------------------------------
21-Aprl-2006   Manjunath            384    - Added null checks on userdata to
avoid crash on simulator in most of
the Apis.
------------------------------------------------------------------------------
28-Apr-2006    Amarnath             384    - Added VocConfigure in RecordStart
and PlayStart do initialize the userPtr
appropriately.

-------------------------------------------------------------------------------
19-Jun-2006    Amarnath             856    - Changes in qpDplInitialize 
PlayeDataCB, HaveDataCB according to 
defect reported by Klokwork.
-------------------------------------------------------------------------------
22-Jun-2006    Amarnath              -     - Review comments incorporated. Changes in 
qpDplAudioInitialize
========================================================================
05-Jul-2006    Chanchal            --        Merged code from QCPhase1.0
-------------------------------------------------------------------------------
29-Nov-2006    Michele               -     - qpDplPostAudioEventToEventQueue:
using qpDplPostEventToEventQueue.
--------------------------------------------------------------------------
06-Apr-2006    Bhavana               -     - Klocwork report fixes.
-------------------------------------------------------------------------------
28-May-2007    Michele               -     - Fixed lint warnings.
-------------------------------------------------------------------------------
09-Aug-2007    Manju                 -     - Fixed lint warnings
-------------------------------------------------------------------------------
17-Sep-2007    Amar, Manju           -     - Crash fixed on calling the API
qpDplPostEventToEventQueue with different
parameter set.
-------------------------------------------------------------------------------
10-Oct-2007    Swapneel              -     - commenting FEATURE_AMSS_DPL for inclusion of 
qpmvs. And qpaudio_ is prefixed before mvs
related function calls/definitions.
-------------------------------------------------------------------------------
30-Apr-2009    Antonio           180337      Added Brew compilation flag
---------------------------------------------------------------------- 
02-Mar-09      sameer                        Featurization introduced
---------------------------------------------------------------------- 
17-Jan-12      Shankar                       Time Warping Feature
---------------------------------------------------------------------- 
17-Jan-12      Shankar                       Checking in For Comform Noise CR
---------------------------------------------------------------------- 
11-may-12      Shankar                       Checking in For DTX
13/08/13       vailani                       CR#: 523903 - JINGALA-43381 - 9x15 LE4.7 crashed while making a Volte call
16/08/13       vailani                       CR#: 521035 - RTP timestamp is increased by 60ms instead of 20ms when CVD don't give callback for more than 40 ms
---------------------------------------------------------------------- 
11/06/15       Manju            811109       IMS Need to send VSS_IMVM_CMD_SET_CAL_MEDIA_TYPE to MVM per CVD design
---------------------------------------------------------------------- 
11/06/15       Pradeepg         789244      FR 25455: Internal - Improvements to WiFi Interface selection criteria, Media metrics
---------------------------------------------------------------------- 
24/06/15       vailani          844680      G711 Interface changes which was bought in 
---------------------------------------------------------------------- 
30/06/15       vailani          847737      Add G711 entry for the new API change for codec Calibiration  API for CVD
---------------------------------------------------------------------- 
07/07/15       pradeepg         642698      Add check for cmd req/ack/resp for cvd commands only
---------------------------------------------------------------------- 
05/08/15       Manju            881932      Not able to receive incoming alerting call after SRVCC failure followed by success case.
---------------------------------------------------------------------- 
20/08/15       Manju            892522      IMS RTP SN and Payload disabled/ Wrong RTP time stamp on first packet
************************************************************************/ 

#include "ims_variation.h"


#include <qpAudio.h>
#include <qpVideo.h>
#include <qplog.h>
#include "qpdplAmssMisc.h"
#include "apr_errcodes.h"
#include "apr_list.h"
#include "apr_objmgr.h"
#include "apr_lock.h"
#include "apr_event.h"
#include "apr_misc.h"
#include "aprv2_api_inline.h"
#include "aprv2_msg_if.h"
#include "vss_public_if.h"
#include "voicemem.h"
#include "qvp_rtp_codec.h"
#include <qpdplCommonMisc.h>
#include "qpDplQdj.h"
#include "qurt.h"
#include"qw.h"
#include "voicecfg_api.h"
#include "apr_errcodes.h"
#include "qpEvsPreviewFrame.h"
#include "cvd_undef_hdr.h"

#define DPL_AUD_MAX_BUNDLE_SIZE       16
#define DPL_AUD_MAX_TX_DATA_SIZE      323

#define DPL_AUD_AV_TIME_MIN_WRAP_ARND 29000
#define DPL_AUD_AV_TIME_MAX_WRAP_ARND 4294867296LL

#define DPLAUDIO_PANIC_ON_ERROR( rc ) \
  { if ( rc ) { IMS_MSG_FATAL_1(LOG_IMS_DPL, "Fatal Error[%d]", rc);} }

#define DPLAUDIO_CMD_QUEUE_SIZE ( 10 )
#define DPLAUDIO_RSP_QUEUE_SIZE ( 10 )

#define DPLAUDIO_HANDLE_TOTAL_BITS ( 16 )
#define DPLAUDIO_HANDLE_INDEX_BITS ( 5 )
#define DPLAUDIO_NUM_HANDLES ( 1 << DPLAUDIO_HANDLE_INDEX_BITS )

#define DPLAUDIO_MAX_SESSION_NAME_SIZE ( 31 )
#define DPLAUDIO_CVD_CMD_TIMEOUT 2000


#define VOICE_PACKET_LENGTH 162 

#define VOICE_PACKET_HDR_LENGTH 1 // Length of voice packet header in bytes


#define QVP_RTP_T140_LEN 2
#define PKT_DROP_INTERVAL 20
#define QPDL_AUDIO_ENC_OFFSET 4096
#define QPDL_AUDIO_DEC_OFFSET 6144

#define QVP_RTP_PKT_INTERVAL 20

#define QVP_RTP_JBA_ADAPTIVE         3  /*JBA rfc 3611*/
#define QVP_RTP_JBA_NON_ADAPTIVE     2  /*JBA rfc 3611*/
#define QVP_RTP_JBA_UNKNOWN          0  /*JBA rfc 3611*/

//EVS
#define EVS_SID_PRIM_FTYPE 28
#define EVS_NODATA_FTYPE 15
#define EVS_PRIM_SID_LEN 6

uint64  g_Dec_RequestTime = 0;
extern boolean g_isNetworkCommandPending;
extern void qvp_rtp_time_get_ms(uint64 * time);
extern boolean srvcc_tear_down_req;
typedef struct dplaudio_work_item_t
{
  apr_list_node_t link;
  aprv2_packet_t* packet;
  QPUINT64		systime;
} dplaudio_work_item_t;

int32_t dpl_send_dec_pkt (
                          void* buffer,
                          uint32 buf_size,
                          aprv2_packet_t* in_packet,
                          QPE_AUDIO_CODEC      eCodec
                          );

typedef void ( *dplaudio_rsp_handler_cb_t ) (
  uint16_t my_port,
  uint16_t server_addr,
  uint16_t server_port,
  uint32_t token,
  uint32_t opcode,
  void* payload,
  uint32_t payload_size
  );


boolean	dpl_cvd_state = FALSE;	
uint16	g_cmd_sent = 0;
uint16	g_cmd_rsp = 0;	
uint16  g_cmd_ack = 0;
uint8   g_apr_assert = 0;
	
boolean g_TimelineToSend = TRUE;
boolean g_firstdequeue = TRUE;
boolean g_updateAVdelay = TRUE;	
boolean g_is_warp_around = FALSE;
uint32  g_iAVSyncTxDelay  = 0;
QPUINT32  last_sent_timestamp = 0;	
uint16		 g_uplink_pkt_count;
uint16		 g_downlink_pkt_count;
uint16		 g_uplink_speech_pkt_count;
uint16		 g_uplink_no_data_pkt_count;
uint16		 g_uplink_silence_pkt_count;
uint16		 g_downlink_speech_pkt_count;
uint16		 g_downlink_no_data_pkt_count;
uint16		 g_downlink_silence_pkt_count;
uint8      g_PrevRxMode; 
/*****************************************************************************
* Variables                                                                 *
****************************************************************************/
//VSID for WLAN/IWLAN
  static char_t dplaudio_cvs_session_name_wlan[] = "10002000";//"default volte voice"; //"default modem voice";
/* "default modem voice" is the name of the CS voice call CVS instance. */
  static char_t dplaudio_mvm_session_name_wlan[] = "10002000";// "default volte voice"; //
//VSID for LTE
static char_t dplaudio_cvs_session_name[] = "default volte voice";//"default mod           em voice";
/* "default modem voice" is the name of the CS voice call CVS instance. */
static char_t dplaudio_mvm_session_name[] =  "default volte voice";//
/* "default modem voice" is the name of the CS voice call MVM instance. */

static apr_lock_t dplaudio_lock;

static apr_objmgr_t dplaudio_objmgr;
static apr_objmgr_object_t dplaudio_objects[ DPLAUDIO_NUM_HANDLES ];

static apr_list_t dplaudio_used_rsp_q;
static apr_list_t dplaudio_free_rsp_q;
static dplaudio_work_item_t dplaudio_rsps[ DPLAUDIO_RSP_QUEUE_SIZE ];

static apr_list_t dplaudio_used_cmd_q;
static apr_list_t dplaudio_free_cmd_q;
static dplaudio_work_item_t dplaudio_cmds[ DPLAUDIO_CMD_QUEUE_SIZE ];
static void dplaudio_evnt_CB_temp(uint32 param);
void dplaudio_evnt_CB(uint32 param);

static char_t dplaudio_my_dns[] = "qcom.funcarea.dplaudio";
static char_t dplaudio_cvs_dns[] = "qcom.audio.cvs";
static char_t dplaudio_cvp_dns[] = "qcom.audio.cvp";
static char_t dplaudio_mvm_dns[] = "qcom.audio.mvm";

static uint32_t dplaudio_apr_handle;
static uint16_t dplaudio_my_addr;
static uint16_t dplaudio_my_port = APR_NULL_V;

static uint16_t dplaudio_cvs_addr;
static uint16_t dplaudio_cvp_addr;
static uint16_t dplaudio_mvm_addr;

static uint16_t dplaudio_app_cvs_port = APR_NULL_V;
static uint16_t dplaudio_app_mvm_port = APR_NULL_V;

#define IMS_RTP_LOOP_BACK           "/nv/item_files/ims/ims_rtp_loop_back_enabled"
#define IMS_AMR_DTX_ENABLED         "/nv/item_files/ims/ims_scr_amr_nb_enabled"             
#define IMS_AMRWB_DTX_ENABLED       "/nv/item_files/ims/ims_scr_amr_wb_enabled"
#define IMS_RTP_ASSERT_ENABLED      "/nv/item_files/ims/ims_assert_enable"

/* TCX partial copy frame types */
#define RF_NO_DATA                            0
#define RF_TCXFD                              1
#define RF_TCXTD1                             2
#define RF_TCXTD2                             3

extern void qvp_rtp_service_last_played_audio_ts(uint32 n_pTimestamp);
extern void qvp_rtp_service_send_av_sync_feed(uint64 param);
extern void qvp_rtp_service_send_av_sync_feed_first(uint64 param);
/* Parameters added for device switch*/
int32 event_counter = -1;    
uint8 cvd_event_not_ready = 1;

QPE_AUDIO_RAT_PVT g_rat_type;


/* Getting a handle for rex timer */
rex_timer_type rex_timer_handle;  
rex_timer_type cvd_timer_handle;

/*Timestamp from GET_TIME API*/
uint64 av_timestamp64 = 0;
uint64 av_timestamp32 = 0;

QPUINT32         g_voicemem_handle;
void*            g_base_virt_addr;

typedef enum 
{
  eAudioStateNULL,
  eAudioStateIniting,
  eAudioStateInited,
  eAudioStateConfiguring,  
  eAudioStateReConfiguring,
  eAudioStateReConfiguringCodeMode,
  eAudioStateConfigured,
  eAudioStateStarting,  
  eAudioStateStarted,
  eAudioStateStopping,
  eAudioStateReleasing
}eAudioStateT;

typedef enum 
{
  eAudioInvalidMode = -1,
  eAudioULMode,
  eAudioDLMode,
  eAudioULDLMode
}eAudioOPMode;

typedef enum 
{
  eCvdStateInitMVMCtrlSession,
  eCvdStateInitStreamSession,
  eCvdStateInitAttachStream,
  eCvdStateStandby,
  eCvdStateInited,
  eCvdStateConfigEvsMedia,
  eCvdStateEvsMaxRate,
  eCvdStateConfigMedia,
  eCvdStateConfigNetwork,
  eCvdStateConfigMVMMedia,  
  eCvdStateConfigTiming,
  eCvdStateConfigCHAwareMode,
  eCvdStateConfigCodecProp,
  eCvdStateConfigDtx,
  eCvdStateConfigured,
  eCvdStateStartStream,
  eCvdStateRun,
  eCvdStateStopStream,
  eCvdStateDeInitDetachStream,
  eCvdStateDeInitCVSDestroy,
  eCvdStateDeInitMVMDestroy,
  eCvdStateNULL
}eDplCvdState;

typedef struct PlayerRecevDesc
{
  QP_AUDIO_DESC pPlayerDesp;
  QP_AUDIO_DESC pRecorderDesp;
}qpDplPlayerRecorderDesc;

typedef struct Desc
{
  QP_AUDIO_DESC pDesc;
}qpDplDesc;


typedef union Desccriptors
{
  qpDplPlayerRecorderDesc PlayRecDesc;
  qpDplDesc               Descriptor;  
}qpDplDescriptors;


typedef struct DescriptorData
{
  eDplCvdState     eCvdState;
  eAudioOPMode     eMode;
  QPBOOL           bConfigPending;
  QPBOOL           bIsPlayerRecorderMode;
  QPBOOL           bIssink_active;
  QPBOOL           bIsaudio_started;
  QP_CODEC_CONFIG  tCodecConfig;
  QP_CODEC_CONFIG  tNewCodecConfig;
  qpDplDescriptors uDecriptors; 
  QPUINT8          iPrevFrameType;
  QPUINT32         iTimeStamp; 
  QPUINT32         iPrevTimeStamp;
  QPUINT32         voicemem_handle;
  void*            base_virt_addr;
  QPUINT32         mapped_mem_handle;
  QPUINT64         mapped_phys_addr;
  QPBOOL           is_eamr_enabled;
  QPUINT32		   iRxDelay; //in us
} qpDplDecriptorData;


typedef struct PlayerRecorder 
{
  QP_AUDIO_DESC pPlayerDesp;
  QP_AUDIO_DESC pRecorderDesp;
} qpDplPlayRecData;


typedef struct AMSSdplAudioData
{
  QPE_AUDIO_DEV                   eADev;
  eAudioStateT                    iCurrentState;
  QP_AUDIO_CALLBACK               tAudioCallBack;
  QPVOID*                         pUserData;
  QPUINT8                         iBundleSize;
  QP_MULTIMEDIA_FRAME*            pAudioBundle[DPL_AUD_MAX_BUNDLE_SIZE];
  QPBOOL                          bIsPlayerRecorderMode;
  QPDPL_QDJ_HANDLE_TYPE           hQdjHandle;
  QPBOOL                          bIsLoopBackMode;
  QPVOID*			  ttyUserData;
}QP_AUDIO_DATA_AMSS;

void qpDplSetEVSEncMode(qpDplDecriptorData *pDescp, 
                            QPE_EVS_PRIMARY_MODE pMode, 
                            QPE_EVS_BW pBandWidth);

static void dplaudio_custom_app_full_cvs_rsp_handler_cb (
  uint16_t my_port,
  uint16_t server_addr,
  uint16_t server_port,
  uint32_t token,
  uint32_t opcode,
  void* payload,
  uint32_t payload_size
  );

static void dpl_aud_cleanup(qpDplDecriptorData *pDplDescData);

static int dpl_cvd_reconfigure(qpDplDecriptorData *pDplDescData);

static int dpl_cvd_standby_before_reconfig(qpDplDecriptorData *pDplDescData);

static int dpl_cvd_configure(qpDplDecriptorData *pDplDescData);

static int dpl_cvd_set_codec(qpDplDecriptorData *pDplDescData);

static void dplaudio_generic_basic_rsp_handler_cb (
  uint16_t my_port,
  uint16_t server_addr,
  uint16_t server_port,
  uint32_t token,
  uint32_t opcode,
  void* payload,
  uint32_t payload_size
  );

  static void dplaudio_process_tty_tx (
  aprv2_packet_t* packet
  );
  
 static void dplaudio_update_tty_state(boolean state);
 static void dplaudio_map_outofband_memory(qpDplDecriptorData* pDplDescData);
QPE_AUDIO_ERROR qvp_rtp_send_rx_char(
 uint16 ttychar,
 QP_AUDIO_DESC tAudioDescriptor,
 uint32 token
 );
 QPVOID dplaudio_G711_codec_change_response(void);

 extern void qvp_rtp_execute_pending_handoff(void);
 void dplaudio_cvd_timeout_cb(uint32 param);

 eDplCvdState g_eCVDState = eCvdStateNULL;

/*---------------------------------------------------------------------------
  QVP_CODECINIT_CODECRATE_CHANGE_EVENT_INFO_TYPE

  This struct defines the codec initialization and rate change event parameters.
---------------------------------------------------------------------------*/
typedef struct
{  
  uint8        ChannelID;       /* stream id */
  uint8         Direction;       /* 0 = Rx 1 = Tx */
  uint8         Codec;           /* 0 = AMR 1 = AMR-WB */
  uint8         Codecrate;      /* see table 1.0 */  
} qvp_codecinit_codecrate_change_event_info_type;

/*---------------------------------------------------------------------------
  QVP_AUDIOPATH_DELAY_CHANGE_EVENT_INFO_TYPE
  This struct defines the delay in audio path event parameters.
---------------------------------------------------------------------------*/
typedef struct
{  
  uint8        TxDelay[3];       /* Audio transmit path delay in micro seconds */
  uint8        RxDelay[3];       /* Audio receive path delay in micro seconds */
} qvp_audiopath_delay_change_event_info_type;


const uint32 sCVDEvsCodecRates[]=
{
/* assigning the value of the mode start in cvd ingterface*/
  0x09, /** EVS 5.9 kbps */  
  0x0A,/** EVS 7.2 kbps */  
  0x0B,/** EVS 8.0 kbps */    
  0x0C,/** EVS 9.6 kbps */  
  0x0D,/** EVS 13.2 kbps */    
  0x0E,/** EVS 16.4 kbps */        
  0x0F,/** EVS 24.4 kbps */          
  0x10,/** EVS 32.0 kbps */            
  0x11,/** EVS 48.0 kbps */              
  0x12,/** EVS 64.0 kbps */                
  0x13,/** EVS 96.0 kbps */                  
  0x14,/** EVS 128.0 kbps */   
  0x0D,/** EVS 13.2 CH AW kbps */      
};//QPE_CVD_EVS_PRIMARY_MODE;

/* audio path delay event */
qvp_audiopath_delay_change_event_info_type	audiopath_delay_event_info_type;

/* Table 1.0

For AMR:                        For AMR-WB: 
0 = 4.75     kbps               0 = 6.6        kbps
1 = 5.15     kbps               1 = 8.855    kbps
2 = 5.9      kbps               2 = 12.65    kbps
3 = 6.7      kbps               3 = 14.25    kbps
4 = 7.4      kbps               4 = 15.85    kbps
5 = 7.95     kbps               5 = 18.25    kbps
6 = 10.2     kbps               6 = 19.85    kbps
7 = 12.2     kbps               7 = 23.05    kbps
8 = Silence frame               8 = 23.85    kbps
9 = Silence frame 
*/

LOCAL QPVOID qpDplLogCodecEvents(uint8 Direction, uint8 Codec, uint8 Codecrate, QPUINT16 evt);
LOCAL QPVOID qpDplLogAudioDelayEvents(uint64 TxDelay, uint32 RxDelay);

QP_EXPORT_C QPVOID qvp_rtp_log_qdj_loss ( QPUINT8 lost_pkt_cnt,
								  QPUINT16 last_rtp_seq,
										  QDJ_LOG_LOSS_TYPE loss_type,
								  QPUINT8 lost_frame_cnt);
QP_IMPORT_C QPE_AUDIO_ERROR dplaudio_set_pktExchng_Mode(QP_AUDIO_DESC tAudioDescriptor, QPBOOL oobMode);
static int32_t dplaudio_set_enc_dec_buffer_config(vss_istream_cmd_set_oob_packet_exchange_config_t *config);

uint64		g_iAVsyncTime= 0; // Latest AV Sync time received
uint64		g_iAVmSysTime; // corresponding modem system time
uint64		g_iTimeInTxPkt= 0; // Latest AV Sync time received

/*****************************************************************************
* Implementations                                                           *
****************************************************************************/
void qpAudioGetDecoderTimeline()
{
	  uint32 iLastPLayedAudioTimeStamp = 0;
	  QPINT iResult = 0;
	  QPC_GLOBAL_DATA* pGlobalData  = QP_NULL;
	  QP_AUDIO_DATA_AMSS* pAudioData = QP_NULL;
	  qpDplDecriptorData *pDplDescData = QP_NULL;

	  pGlobalData = qpDplGetGlobalData();
	  if (pGlobalData)
	  {
	    pDplDescData = (qpDplDecriptorData *)pGlobalData->pAudioData;
	    if(pDplDescData)
	    {
	      pAudioData = (QP_AUDIO_DATA_AMSS*) pDplDescData->uDecriptors.Descriptor.pDesc;
	    }
	    else
	    {
	      IMS_MSG_ERR_0(LOG_IMS_DPL, "qpAudioGetDecoderTimeline ...pDplDescData  is NULL");
	    }
	  }
	  else
	  {
	    IMS_MSG_ERR_0(LOG_IMS_DPL, "qpAudioGetDecoderTimeline ...pGlobalData  is NULL");
	    return;
	  } 

	  if(QP_NULL == pAudioData)
	  {
	    IMS_MSG_ERR_0(LOG_IMS_DPL, "qpAudioGetDecoderTimeline ...pDplDescData  is NULL");
	    return;
	  }

	  if( pAudioData->hQdjHandle == NULL )
	  {
	    IMS_MSG_ERR_0(LOG_IMS_DPL, "qpAudioGetDecoderTimeline ... QDJ handle is NULL");
	    return;
	  }	

	  iResult = qpDplQdjGetCurrentRTPPlayoutTS(pAudioData->hQdjHandle,&iLastPLayedAudioTimeStamp);
	  
	  if(iResult == 1)
	  {
	  IMS_MSG_ERR_1(LOG_IMS_DPL, "qpAudioGetDecoderTimeline ... Sending last audio TS = %u",iLastPLayedAudioTimeStamp);
      (QPVOID)qvp_rtp_service_last_played_audio_ts(iLastPLayedAudioTimeStamp);
	  }

	  return;
}

QP_IMPORT_C void qpAudioAVTimeGetMs(uint64* time)
{
	uint64	current_avtime = 0;
	uint64	diff = 0;		
		uint64  avsync_time_ms = 0;

	if(g_iAVsyncTime)
		{
			avsync_time_ms = g_iAVsyncTime/1000; // AV sync time in ms
		
		qvp_rtp_time_get_ms(&current_avtime);
		diff = current_avtime - g_iAVmSysTime; //finding delta from the time we received last AV time
		current_avtime = avsync_time_ms + diff; //adding the delta to get current time
	}
 
  (*time) = current_avtime;
  IMS_MSG_ERR_2(LOG_IMS_DPL,"qpAudioAVTimeGetMs diff: %lu current_avtime = %Lu ",diff, current_avtime);
  
  return;
}


//log the loss frames from QDJ
QPVOID qpAudioQDJlogLossFrame(QPUINT8 lost_pkt_cnt,
								  QPUINT16 last_rtp_seq,
								  QDJ_LOG_LOSS_TYPE loss_type,
								  QPUINT8 lost_frame_cnt)
{
	IMS_MSG_HIGH_3(LOG_IMS_DPL,"qpAudioQDJlogLossFrame last_rtp_seq = %d, lost_frame_cnt = %d and loss_type = %d",
 last_rtp_seq, lost_frame_cnt, loss_type);
	qvp_rtp_log_qdj_loss ( lost_pkt_cnt, last_rtp_seq, loss_type, lost_frame_cnt);
	return;

//		return AUDIO_ERROR_UNKNOWN;
}


/**
 * \fn QP_EXPORT_C QPE_AUDIO_ERROR qpAudioGetQDJVoiceCallStat(
									QPVOID* voice_call_stat_params)
 * \brief Get parameters of Voice call statistics log packet from QDJ
 *
 * \param voice_call_stat_params pointer to structure  Voice call stat QDJ
 * \return QPE_AUDIO_ERROR 
 * \remarks None
 */
QP_EXPORT_C QPE_AUDIO_ERROR qpAudioGetQDJVoiceCallStat(QPVOID* voice_call_stat_params)
{
	QPDPL_QDJ_VOICE_CALL_STAT_PARAMS* pAudio_voice_call_stat_params;
	QPC_GLOBAL_DATA* pGlobalData  = QP_NULL;
	pGlobalData = qpDplGetGlobalData();

	if (pGlobalData)
    {
      QP_AUDIO_DATA_AMSS* pAudioData = QP_NULL;
      qpDplDecriptorData *pDplDescData = QP_NULL;

      pDplDescData = (qpDplDecriptorData *)pGlobalData->pAudioData;
      if (pDplDescData)
      {
          pAudioData = (QP_AUDIO_DATA_AMSS*) pDplDescData->uDecriptors.Descriptor.pDesc;
	pAudio_voice_call_stat_params = ( QPDPL_QDJ_VOICE_CALL_STAT_PARAMS* )voice_call_stat_params;
		  
	  qpDplQdjGetVoiceCallStat( pAudioData->hQdjHandle,pAudio_voice_call_stat_params);
	  }
	}

	return AUDIO_ERROR_OK;
}


static void dplaudio_isr_lock_fn ( void  )
{
  apr_lock_enter( dplaudio_lock );
}

static void dplaudio_isr_unlock_fn ( void )
{
  apr_lock_leave( dplaudio_lock );
}

static void dplaudio_send_command (
                                   uint16_t to_addr,
                                   uint16_t to_port,
                                   uint32_t token,
                                   uint32_t opcode,
                                   void* payload,
                                   uint32_t payload_size,
                                   dplaudio_rsp_handler_cb_t callback
                                   )
{
  int32_t rc;
  apr_objmgr_object_t* obj = QP_NULL;
  IMS_MSG_LOW_0(LOG_IMS_DPL,"Entering dplaudio_send_command..");
  rc = apr_objmgr_alloc_object( &dplaudio_objmgr, &obj );
  DPLAUDIO_PANIC_ON_ERROR( rc );

  if(obj != QP_NULL)
  {
  obj->type = token;
  obj->any.ptr = callback;

    if(obj->handle != QP_NULL)
    {
  IMS_MSG_HIGH_1(LOG_IMS_DPL,"Posting CVD Command  %x", opcode);

  rc = __aprv2_cmd_alloc_send( dplaudio_apr_handle,
    APRV2_PKT_MSGTYPE_SEQCMD_V,
    dplaudio_my_addr, dplaudio_my_port,
    to_addr, to_port,
    obj->handle,
    opcode,
    payload, payload_size );
  DPLAUDIO_PANIC_ON_ERROR( rc );
      if ( rc )
      {
        IMS_MSG_ERR_0(LOG_IMS_DPL,"__aprv2_cmd_alloc_send failed");
        apr_objmgr_free_object( &dplaudio_objmgr, obj->handle );
        ASSERT(0);
      }
	    g_cmd_sent++;
      (void)rex_set_timer(&cvd_timer_handle, DPLAUDIO_CVD_CMD_TIMEOUT);
	    IMS_MSG_HIGH_3(LOG_IMS_DPL, "dplaudio_cmd_send g_cmd_sent = %d g_cmd_rsp = %d g_cmd_ack = %d", g_cmd_sent, g_cmd_rsp, g_cmd_ack); 
    }
    else
    {
      IMS_MSG_ERR_0(LOG_IMS_DPL,"obj->handle is null, so cant send APR command");
	ASSERT(0);
    }

  }
  else
  {
    IMS_MSG_ERR_0(LOG_IMS_DPL,"apr_objmgr_alloc_object allocate fail, ran out of buffers");
	ASSERT(0);
  }
  /**< An error robust implementation would retry instead of panicking. */
}


static void dplaudio_process_send_enc_buffer_evt_msg (
  aprv2_packet_t* packet
  )
{
  QPUINT8 iReqBundleSize        = 1;
  QPUINT8* payload              = QP_NULL;
  QPUINT32 payload_size         = 0;
  QPC_GLOBAL_DATA* pGlobalData  = QP_NULL;
  //int mode = 0 ;
  //int frameType = 0 ;
  QP_AUDIO_DATA_AMSS* pAudioData = QP_NULL;
  qpDplDecriptorData *pDplDescData = QP_NULL;
  pGlobalData = qpDplGetGlobalData();

  do
  {
    if (NULL == pGlobalData)
    {
      IMS_MSG_ERR_0(LOG_IMS_DPL,"GlobalData is NULL");
      break;
    }
    pDplDescData = (qpDplDecriptorData *)pGlobalData->pAudioData;
    if (NULL == pDplDescData)
    {
      IMS_MSG_ERR_0(LOG_IMS_DPL,"Audio Desc in GlobalData is NULL");
      break;
    }
    if (eCvdStateRun != pDplDescData->eCvdState)
    {
      IMS_MSG_ERR_1(LOG_IMS_DPL,"Wrong CVD State: %x", pDplDescData->eCvdState);
      break;
    }
    //TBD: Assuming eDev is RecPlayer later there should be just one descriptor
    pAudioData = (QP_AUDIO_DATA_AMSS*) pDplDescData->uDecriptors.Descriptor.pDesc;
    if (QP_NULL == pAudioData)
    {
        IMS_MSG_ERR_0(LOG_IMS_DPL,"AudioData is NULL");
        break;
     }
    if (eAudioStateStarted != pAudioData->iCurrentState)
    {
      IMS_MSG_ERR_1(LOG_IMS_DPL,"Wrong Audio State: %x", pAudioData->iCurrentState);
      break;
    }
    if(pAudioData->bIsLoopBackMode)
    {
      IMS_MSG_LOW_0(LOG_IMS_DPL,"LoopBack Mode  Dropping UL Frames  ");
      break;
    }

    payload_size =(APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( packet->header ) - sizeof( vss_istream_evt_send_enc_buffer_t ));

    if (pAudioData->iBundleSize < DPL_AUD_MAX_BUNDLE_SIZE && payload_size < DPL_AUD_MAX_TX_DATA_SIZE)
    {
      QP_MULTIMEDIA_FRAME iAudioDPLFrame;
      uint64              aud_timestamp;
      uint32              iFrameTS;

      qvp_rtp_time_get_ms(&aud_timestamp);

      iFrameTS = (uint32)(aud_timestamp);

      payload = APR_PTR_END_OF( APRV2_PKT_GET_PAYLOAD( void, packet ),
        sizeof( vss_istream_evt_send_enc_buffer_t ) );

      qpDplMemset(&iAudioDPLFrame, 0, sizeof(QP_MULTIMEDIA_FRAME));

      //Parse the mode and frame type
      //frameType = (int)( ((*payload) & 0xF0) >> 4 );
      //mode = (int)( (*payload) & 0x0F );


      iAudioDPLFrame.iDataLen = payload_size - VOICE_PACKET_HDR_LENGTH;
      iAudioDPLFrame.pData    = (uint8*) payload + VOICE_PACKET_HDR_LENGTH;

      if(pDplDescData->iPrevTimeStamp && ((iFrameTS - pDplDescData->iPrevTimeStamp) > 40))
      {
        IMS_MSG_LOW_1(LOG_IMS_DPL,"Time difference greater than 40 ms %d ", (iFrameTS - pDplDescData->iPrevTimeStamp));

        if(pDplDescData->tCodecConfig.eCodec == AUDIO_CODEC_AMR_WB)
          pDplDescData->iTimeStamp = pDplDescData->iTimeStamp + (iFrameTS - pDplDescData->iPrevTimeStamp)/20 * 320;
          else
          pDplDescData->iTimeStamp = pDplDescData->iTimeStamp + (iFrameTS - pDplDescData->iPrevTimeStamp)/20 * 160;
        }
        else
        {

          if(pDplDescData->tCodecConfig.eCodec == AUDIO_CODEC_AMR_WB)
            pDplDescData->iTimeStamp = pDplDescData->iTimeStamp + 320;
          else
            pDplDescData->iTimeStamp = pDplDescData->iTimeStamp + 160;
        }

        pDplDescData->iPrevTimeStamp = iFrameTS;

        if ((AUDIO_CODEC_AMR == pDplDescData->tCodecConfig.eCodec) || (AUDIO_CODEC_AMR_WB == pDplDescData->tCodecConfig.eCodec))
        {
          iAudioDPLFrame.sFrameInfo.sAMRInfo.iAMRModeRequest = pDplDescData->tCodecConfig.sCodecInfo.sAMRInfo.eCodecMode;
        }

        iAudioDPLFrame.sMediaPacketInfo.sMediaPktInfoTx.iTimeStamp = pDplDescData->iTimeStamp;

        qpDplMemscpy(pAudioData->pAudioBundle[pAudioData->iBundleSize], sizeof(QP_MULTIMEDIA_FRAME), &iAudioDPLFrame, sizeof(QP_MULTIMEDIA_FRAME));
        // Avoiding the copy just assigning the pointer
        pAudioData->pAudioBundle[pAudioData->iBundleSize]->pData = iAudioDPLFrame.pData;


        if (pAudioData->tAudioCallBack)
        {
          pAudioData->tAudioCallBack(AUDIO_MSG_RECORDED_DATA, pAudioData->pAudioBundle,
            iReqBundleSize, 
            pAudioData->pUserData);

          IMS_MSG_LOW_1(LOG_IMS_DPL,"Posting Recorded Data of size %d ", pAudioData->pAudioBundle[pAudioData->iBundleSize]->iDataLen);
        }
        pAudioData->iBundleSize = 0;
    }
    else
    {
      IMS_MSG_ERR_2(LOG_IMS_DPL,"Wrong Bundle Sz: %x or Payload Sz: %x", pAudioData->iBundleSize, payload_size );
      break;
    }
  }while(0);

  __aprv2_cmd_free( dplaudio_apr_handle, packet );
}
  
static void dplaudio_process_send_enc_buffer_oob_evt_msg (
  aprv2_packet_t* packet
  )
{
  QPUINT8* Enc_data = NULL;
  QPUINT8 iReqBundleSize        = 1;
  QPUINT32 payload_size         = 0;
  QPUINT32 av_tx_tstamp         = 0;
  QPUINT32 media_type           = 0;
  int32_t rc;  
  QPC_GLOBAL_DATA* pGlobalData  = QP_NULL;
  QP_AUDIO_DATA_AMSS* pAudioData = QP_NULL;
  qpDplDecriptorData *pDplDescData = QP_NULL;

  pGlobalData = qpDplGetGlobalData();

  do
  {
    if (NULL == pGlobalData)
    {
      IMS_MSG_ERR_0(LOG_IMS_DPL,"GlobalData is NULL");
      break;
    }
    pDplDescData = (qpDplDecriptorData *)pGlobalData->pAudioData;
    if (NULL == pDplDescData)
    {
      IMS_MSG_ERR_0(LOG_IMS_DPL,"Audio Desc in GlobalData is NULL");
      break;
    }
    if (eCvdStateRun != pDplDescData->eCvdState)
    {
      IMS_MSG_ERR_1(LOG_IMS_DPL,"Wrong CVD State: %x", pDplDescData->eCvdState);
      break;
    }
    //TBD: Assuming eDev is RecPlayer later there should be just one descriptor
    pAudioData = (QP_AUDIO_DATA_AMSS*) pDplDescData->uDecriptors.Descriptor.pDesc;
    if (QP_NULL == pAudioData)
    {
        IMS_MSG_ERR_0(LOG_IMS_DPL,"AudioData is NULL");
        break;
     }
    if (eAudioStateStarted != pAudioData->iCurrentState)
    {
      IMS_MSG_ERR_1(LOG_IMS_DPL,"Wrong Audio State: %x", pAudioData->iCurrentState);
      break;
    }
    if(pAudioData->bIsLoopBackMode)
    {
      IMS_MSG_LOW_0(LOG_IMS_DPL,"LoopBack Mode  Dropping UL Frames  ");
      break;
    }

    /** Invalidate the cache **/
    /*{
     vsd_status_t status;
     voicemem_cmd_cache_invalidate_t voice_mem_config;
     voice_mem_config.voicemem_handle = pDplDescData->voicemem_handle;
     voice_mem_config.virt_addr = pDplDescData->base_virt_addr;
     voice_mem_config.size = 8192;
     status = voicemem_call(VOICEMEM_CMD_CACHE_INVALIDATE, &voice_mem_config, sizeof(voicemem_cmd_cache_invalidate_t));
     if(VSD_EOK != status)
     {
       IMS_MSG_ERR_0(LOG_IMS_DPL,"VOICEMEM_CMD_CACHE_INVALIDATE failed");
     }
     
    }*/

    /** Read Payload from virtual memory **/
    {
      QPUINT32 * temp_var = NULL;
      QPUINT8* temp_read = (QPUINT8*)pDplDescData->base_virt_addr;
      Enc_data = (QPUINT8*)pDplDescData->base_virt_addr;
      temp_read += QPDL_AUDIO_ENC_OFFSET;
      temp_var = (QPUINT32*)temp_read;
      av_tx_tstamp = *temp_var;
      temp_var++;
      media_type = *temp_var;
      temp_var++;
      payload_size = *temp_var;
      if(payload_size)
         payload_size -= VOICE_PACKET_HDR_LENGTH;
      IMS_MSG_MED_3(LOG_IMS_DPL,"dplaudio_process_send_enc_buffer_oob_evt_msg av_tx_tstamp = %lu  media_type = %lu payload_size = %lu ", av_tx_tstamp, media_type, payload_size);
      Enc_data += (QPDL_AUDIO_ENC_OFFSET + 12 + VOICE_PACKET_HDR_LENGTH);

    }

    if (pAudioData->iBundleSize < DPL_AUD_MAX_BUNDLE_SIZE && payload_size < DPL_AUD_MAX_TX_DATA_SIZE)
    {
      QP_MULTIMEDIA_FRAME iAudioDPLFrame;
      uint64               aud_timestamp;
      uint32              iFrameTS;
      uint64			  iTimeStamp64 = 0;
      uint16        clkrate = 0;

      if(av_timestamp32 > av_tx_tstamp)
      {
        if( !(( av_tx_tstamp < DPL_AUD_AV_TIME_MIN_WRAP_ARND ) && ( av_timestamp32 > DPL_AUD_AV_TIME_MAX_WRAP_ARND )) )
        {
          IMS_MSG_FATAL_2(LOG_IMS_DPL,"AV timestamp out of range..looks wrong: previous Rx TS:%lu new TS:%lu",av_timestamp32,av_tx_tstamp);
        }
          /*lower 32 bits have rolled up so increment the upper 32 bits by 1 
        --REVIEW what happens if initially it is 0 ?*/
        IMS_MSG_MED_1(LOG_IMS_DPL,"Incrementing upper 32 bits of AV timer by 1 %Lu",*((uint32*)&av_tx_tstamp));
          av_timestamp64 += 0x0000000100000000LL;
        g_is_warp_around = TRUE;
      }

      iTimeStamp64 = (av_timestamp64 & 0xFFFFFFFF00000000LL) | (av_tx_tstamp & 0x00000000FFFFFFFFLL);
      g_iTimeInTxPkt = iTimeStamp64;
      av_timestamp32 = av_tx_tstamp;

      if(g_updateAVdelay || g_is_warp_around)
      {
        dplaudio_send_command( dplaudio_mvm_addr, dplaudio_app_mvm_port,
          0x12345678,
          VSS_IAVTIMER_CMD_GET_TIME ,
          QP_NULL, 0,
          dplaudio_generic_basic_rsp_handler_cb );
        IMS_MSG_MED_0(LOG_IMS_DPL,"sending VSS_IAVTIMER_CMD_GET_TIME ");
      }			  
      /*convert from Microseconds to Milliseconds*/
      //IMS_MSG_MED_2(LOG_IMS_DPL,"iTimeStamp64 in microsec = %u %u",*((uint32*)&iTimeStamp64),*((uint32*)&iTimeStamp64 + 1));
      iTimeStamp64 /= 1000;
      IMS_MSG_MED_2(LOG_IMS_DPL,"iTimeStamp64 in millisec = %u %u",*((uint32*)&iTimeStamp64),*((uint32*)&iTimeStamp64 + 1));

      qvp_rtp_time_get_ms(&aud_timestamp);

      iFrameTS = (uint32)(aud_timestamp);

      qpDplMemset(&iAudioDPLFrame, 0, sizeof(QP_MULTIMEDIA_FRAME));

      //Parse the mode and frame type
      //frameType = (int)( ((*payload) & 0xF0) >> 4 );
      //mode = (int)( (*payload) & 0x0F );

      if ((AUDIO_CODEC_AMR == pDplDescData->tCodecConfig.eCodec) || 
          (AUDIO_CODEC_AMR_WB == pDplDescData->tCodecConfig.eCodec)||
          (AUDIO_CODEC_EVS == pDplDescData->tCodecConfig.eCodec))
      {
        iAudioDPLFrame.iDataLen = payload_size;
        iAudioDPLFrame.pData    = Enc_data;
        if(AUDIO_CODEC_EVS == pDplDescData->tCodecConfig.eCodec)
        {
          iAudioDPLFrame.sFrameInfo.sEVSInfo.iEVSModeRequest = pDplDescData->tCodecConfig.sCodecInfo.sEVSInfo.eCodecMode.ePrmyMode;
        }
        else
        {
          iAudioDPLFrame.sFrameInfo.sAMRInfo.iAMRModeRequest = pDplDescData->tCodecConfig.sCodecInfo.sAMRInfo.eCodecMode;
        }
      }
      else if ((AUDIO_CODEC_G711U== pDplDescData->tCodecConfig.eCodec) || (AUDIO_CODEC_G711A== pDplDescData->tCodecConfig.eCodec))
      {
         uint8	header = 0;				 
         header = *(Enc_data - VOICE_PACKET_HDR_LENGTH);		

         IMS_MSG_MED_2(LOG_IMS_DPL,"AUDIO_CODEC_G711 frametype = %x Companding Type = %x",( header & 0x03 ),(( header & 0x0C ) >> 2));

         iAudioDPLFrame.pData	  = Enc_data;	
         iAudioDPLFrame.sFrameInfo.sG711Info.iFrameType = ( header & 0x03 );
         IMS_MSG_ERR_2(LOG_IMS_DPL,"avsync time = %x or %X",iTimeStamp64,iTimeStamp64);
         
         if(iAudioDPLFrame.sFrameInfo.sG711Info.iFrameType == AUDIO_G711_SPEECH)
         {
           iAudioDPLFrame.iDataLen = 160;
         }
         else if(iAudioDPLFrame.sFrameInfo.sG711Info.iFrameType == AUDIO_G711_SID)
         {
           iAudioDPLFrame.iDataLen = 11;
         }
         else
         {
           IMS_MSG_MED_0(LOG_IMS_DPL,"AUDIO_CODEC_G711 NTX packet");
         }			 
      }
      /* the clockrate param is either 8/16
         packets are generted every 20ms (QVP_RTP_PKT_INTERVAL)
         the duration of clock corresponding to the 20
         calculation = (clktrate/1000)*QVP_RTP_PKT_INTERVAL
      */
      clkrate = (pDplDescData->tCodecConfig.iClockRate * QVP_RTP_PKT_INTERVAL);
      /* pDplDescData->iTimeStamp = pDplDescData->iTimeStamp + clkrate 
                                 + (((iFrameTS - pDplDescData->iPrevTimeStamp)/QVP_RTP_PKT_INTERVAL)-1)*clkrate; */
      
      if(pDplDescData->iPrevTimeStamp && ((iFrameTS - pDplDescData->iPrevTimeStamp) > 40))
      {
        IMS_MSG_LOW_1(LOG_IMS_DPL,"Time difference greater than 40 ms %d ", (iFrameTS - pDplDescData->iPrevTimeStamp));
          pDplDescData->iTimeStamp = pDplDescData->iTimeStamp + (iFrameTS - pDplDescData->iPrevTimeStamp)/20 * clkrate;
      }
      else
      {
          pDplDescData->iTimeStamp = pDplDescData->iTimeStamp + clkrate;
      }
      
      pDplDescData->iPrevTimeStamp = iFrameTS;

      if(  iFrameTS >= ( last_sent_timestamp + 200 ) )
      {

        uint64 iFrameTS_l = 0;
        uint64 current_time = 0;
        last_sent_timestamp = iFrameTS;

        qpAudioAVTimeGetMs(&current_time);
  
        iFrameTS_l =  current_time;
  
        IMS_MSG_LOW_2(LOG_IMS_DPL,"avsync time in ms = %Lu, current_time = %Lu ", iFrameTS_l, current_time );
        qvp_rtp_service_send_av_sync_feed(iFrameTS_l);
      }	
      iAudioDPLFrame.sMediaPacketInfo.sMediaPktInfoTx.iTimeStamp = pDplDescData->iTimeStamp;
      /*set the 64 bit timestamp*/
      iAudioDPLFrame.sMediaPacketInfo.sMediaPktInfoTx.iAVTimeStamp64 = iTimeStamp64;

      qpDplMemscpy(pAudioData->pAudioBundle[pAudioData->iBundleSize], sizeof(QP_MULTIMEDIA_FRAME), &iAudioDPLFrame, sizeof(QP_MULTIMEDIA_FRAME));
      // Avoiding the copy just assigning the pointer
      pAudioData->pAudioBundle[pAudioData->iBundleSize]->pData = iAudioDPLFrame.pData;


      if (pAudioData->tAudioCallBack)
      {
        uint16 iDataLen = pAudioData->pAudioBundle[pAudioData->iBundleSize]->iDataLen;
        if (iDataLen == 0)
        {
      	  ++g_uplink_no_data_pkt_count;
        }
        else if ( iDataLen == 5)
        {
      	  ++g_uplink_silence_pkt_count;
        }
        else if (iDataLen > 5)
        {
      	  ++g_uplink_speech_pkt_count;
        }
        IMS_MSG_LOW_3(LOG_IMS_DPL,"g_uplink_no_data_pkt_count %d g_uplink_silence_pkt_count %d g_uplink_speech_pkt_count %d", \
  			   g_uplink_no_data_pkt_count,g_uplink_silence_pkt_count,g_uplink_speech_pkt_count);
            pAudioData->tAudioCallBack(AUDIO_MSG_RECORDED_DATA, pAudioData->pAudioBundle,
              iReqBundleSize, 
              pAudioData->pUserData);

            IMS_MSG_LOW_1(LOG_IMS_DPL,"Posting Recorded Data of size %d ", pAudioData->pAudioBundle[pAudioData->iBundleSize]->iDataLen);
      }
      pAudioData->iBundleSize = 0;

    }
    else
    {
      IMS_MSG_ERR_2(LOG_IMS_DPL,"Wrong Bundle Sz: %x or Payload Sz: %x", pAudioData->iBundleSize, payload_size );
      break;
    }
  }while(0);

  __aprv2_cmd_free( dplaudio_apr_handle, packet );
  

  IMS_MSG_HIGH_0(LOG_IMS_DPL,"Posting CVD Command  VSS_ISTREAM_EVT_OOB_NOTIFY_ENC_BUFFER_CONSUMED");
  /** Send Encoded buffer consumed **/
  rc = __aprv2_cmd_alloc_send( dplaudio_apr_handle,
                              APRV2_PKT_MSGTYPE_SEQCMD_V,
                              dplaudio_my_addr, dplaudio_my_port,
                              dplaudio_cvs_addr, dplaudio_app_cvs_port,
                              0,
                              VSS_ISTREAM_EVT_OOB_NOTIFY_ENC_BUFFER_CONSUMED,
                              NULL, 0 );
                              DPLAUDIO_PANIC_ON_ERROR( rc );
  if ( rc )
  {
    IMS_MSG_ERR_0(LOG_IMS_DPL,"__aprv2_cmd_alloc_send failed");
    ASSERT(0);
  }

}


static void dplaudio_process_rx_path_delay_evt_msg (
  aprv2_packet_t* packet
  )
{
  QPUINT8* payload              = QP_NULL;
  QPUINT32 payload_size         = 0;
  QPC_GLOBAL_DATA* pGlobalData  = QP_NULL;

  pGlobalData = qpDplGetGlobalData();

  do
  {
    if (pGlobalData)
    {
      QP_AUDIO_DATA_AMSS* pAudioData = QP_NULL;
      qpDplDecriptorData *pDplDescData = QP_NULL;

      pDplDescData = (qpDplDecriptorData *)pGlobalData->pAudioData;
      if (pDplDescData)
      {

          pAudioData = (QP_AUDIO_DATA_AMSS*) pDplDescData->uDecriptors.Descriptor.pDesc;
          if (pAudioData )
          {

            if(pAudioData->bIsLoopBackMode)
            {
              IMS_MSG_LOW_0(LOG_IMS_DPL,"LoopBack Mode  Dropping UL Frames  ");
              break;
			  
            }

            payload_size =(APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( packet->header ));// - sizeof( vss_iavsync_evt_tx_timestamp_t ));
		  
			if (payload_size == sizeof(QPUINT32))
			{
				QPUINT32              *iFrameDelay = 0;
				QPUINT32				delay = 0;
				payload =  APRV2_PKT_GET_PAYLOAD( void, packet );

				iFrameDelay = (QPUINT32*)payload;
				delay = *iFrameDelay;
				IMS_MSG_LOW_2(LOG_IMS_DPL,"RX PATH avsync delay in us = %lX or %lu", delay, delay);
				pDplDescData->iRxDelay = delay;							
				/*	If there is change in AV Rx Delay Then log the Event	*/								
				//if ( (uint8)delay != audiopath_delay_event_info_type.RxDelay[0])
				qpDplLogAudioDelayEvents(0, (uint32)(delay));	
				delay=delay/1000;
				qvp_rtp_handle_audio_rx_delay_cb((uint16)(delay));
            }
            
          }
          else
          {
              IMS_MSG_ERR_0(LOG_IMS_DPL,"AudioData is NULL");
              break;
          }

      }
      else
      {
        IMS_MSG_ERR_0(LOG_IMS_DPL,"Audio Desc in GlobalData is NULL");
        break;
      }
    }
    else
    {
      IMS_MSG_ERR_0(LOG_IMS_DPL,"GlobalData is NULL");
      break;
    }
  }while(0);

  __aprv2_cmd_free( dplaudio_apr_handle, packet );
}

int32_t dpl_send_dec_pkt (
                          void* buffer,
                          uint32 buf_size,
                          aprv2_packet_t* in_packet,
                          QPE_AUDIO_CODEC      eCodec
                          )
{
  //int32_t rc;
  aprv2_packet_t* packet = QP_NULL;
  vss_istream_evt_send_dec_buffer_t* dec_buffer;
  uint8_t* content;

  /*rc = */__aprv2_cmd_alloc_ext(
    dplaudio_apr_handle, APRV2_PKT_MSGTYPE_EVENT_V,
    in_packet->dst_addr, in_packet->dst_port,
    in_packet->src_addr, in_packet->src_port,
    in_packet->token, VSS_ISTREAM_EVT_SEND_DEC_BUFFER,
    ( sizeof( vss_istream_evt_send_dec_buffer_t ) + buf_size ),
    &packet );

  //Log rc  here
  if(packet == QP_NULL)
  {
    return APR_EFAILED;
  }
  g_downlink_pkt_count++;
  IMS_MSG_MED_1(LOG_IMS_DPL,"g_downlink_pkt_count %d",g_downlink_pkt_count);
  dec_buffer = APRV2_PKT_GET_PAYLOAD( vss_istream_evt_send_dec_buffer_t, packet );
  if(eCodec == AUDIO_CODEC_AMR)
    dec_buffer->media_id = VSS_MEDIA_ID_AMR_NB_MODEM; //Need to set to AMR or WB based on the codec config
  else if(eCodec == AUDIO_CODEC_AMR_WB)
    dec_buffer->media_id = VSS_MEDIA_ID_AMR_WB_MODEM; //Need to set to AMR or WB based on the codec config
  else if(eCodec == AUDIO_CODEC_G711U)
  {
  	dec_buffer->media_id = VSS_MEDIA_ID_G711_MULAW_V2;
  }
  else if(eCodec == AUDIO_CODEC_G711A)
  {
  	dec_buffer->media_id = VSS_MEDIA_ID_G711_ALAW_V2;
  }

  IMS_MSG_LOW_2(LOG_IMS_DPL,"dpl_send_dec_pkt len %d  Mode %d", buf_size, dec_buffer->media_id);

  content = APR_PTR_END_OF( dec_buffer, sizeof( vss_istream_evt_send_dec_buffer_t ) );
  qpDplMemscpy( content, buf_size, buffer, buf_size );

  ( void ) __aprv2_cmd_forward( dplaudio_apr_handle, packet );

  return APR_EOK;
}

/*******************************************************************************
Function    :   dplaudio_set_enc_dec_buffer_config
Arguments   :   config - memory map structure
Return Type :   error value
Purpose     :   This function post the PACKET_EXCHANGE_CONFIG to CVS
********************************************************************************/
static int32_t dplaudio_set_enc_dec_buffer_config(vss_istream_cmd_set_oob_packet_exchange_config_t *config)
{
  int32_t status = APR_EFAILED;
  if (NULL != config)
  {
    dplaudio_send_command( dplaudio_cvs_addr, dplaudio_app_cvs_port,
                0x12345678,
                VSS_ISTREAM_CMD_SET_OOB_PACKET_EXCHANGE_CONFIG,
                config, sizeof( vss_istream_cmd_set_oob_packet_exchange_config_t),
                dplaudio_generic_basic_rsp_handler_cb );
	status = APR_EOK;
  }
  return status;
}
/*******************************************************************************
Function    :   dplaudio_evnt_cb
Arguments   :   Nil
Return Type :   Void
Purpose     :   This is the callback when we receive 
                cvd event not ready. This callback will be called every 
				20 ms until cvd event becomes cvd event ready. Here we'll dequeue
				a packet and simply getting dropped without being sent to CVD 
				for playout.
********************************************************************************/

void dplaudio_evnt_CB(uint32 param)
{
  QPC_GLOBAL_DATA* pGlobalData  = QP_NULL;
  QP_AUDIO_DATA_AMSS* pAudioData = QP_NULL;
  qpDplDecriptorData *pDplDescData = QP_NULL;
  QPDPL_QDJ_FRAME_TYPE      qdj_frame;
  //int frameType; 
  uint8 vocoderPacket[VOICE_PACKET_LENGTH];
  //IMS_MSG_ERR_0(LOG_IMS_DPL,"Entered 20 MS Not ready dplaudio_evnt_CB dequue function");
  pGlobalData = qpDplGetGlobalData();
	//pGlobalData = (QPC_GLOBAL_DATA*)param; //qpDplGetGlobalData();
		if (pGlobalData == QP_NULL )
		{
			IMS_MSG_ERR_0(LOG_IMS_DPL, "dplaudio_evnt_CB ..pGlobalData  is NULL");
                        return;
		}

	pDplDescData = (qpDplDecriptorData *)pGlobalData->pAudioData;
		if( pDplDescData == QP_NULL )
		{
		   IMS_MSG_ERR_0(LOG_IMS_DPL, "dplaudio_evnt_CB ...pDplDescdata  is NULL");
                   return ;
		}
	    
	pAudioData = (QP_AUDIO_DATA_AMSS*) pDplDescData->uDecriptors.Descriptor.pDesc;
	if(QP_NULL == pAudioData)
	{
		IMS_MSG_ERR_0(LOG_IMS_DPL, "dplaudio_evnt_CB ...pAudioData is NULL");
		return;
	}
    

  if( pAudioData->hQdjHandle == NULL )
  {
    IMS_MSG_ERR_0(LOG_IMS_DPL, "dplaudio_evnt_CB ... QDJ handle is NULL");
	return;
  }
    
	/*Get a dummy qdj frame and pass it for dequeue */
 	qpDplMemset(&qdj_frame, 0, sizeof(QPDPL_QDJ_FRAME_TYPE));
	qdj_frame.data = (uint8*) &vocoderPacket + VOICE_PACKET_HDR_LENGTH;
    qdj_frame.len = VOICE_PACKET_LENGTH; 

        IMS_MSG_HIGH_0(LOG_IMS_DPL,"CVD is not ready to get the packet,so packets are dequeued and  dropped");

  /*---------------------------------------------------------------------
		Dequeue an element here
	---------------------------------------------------------------------*/

  if( qpDplQdjDequeue(pAudioData->hQdjHandle, &qdj_frame ) == 0 )
  {
    (void)qpDplQdjService(pAudioData->hQdjHandle);
  }
  /* Dequeued the packet and  doing nothing here*/
  IMS_MSG_MED_0(LOG_IMS_DPL,"CVD event callback");

  	(void)rex_set_timer(&rex_timer_handle,PKT_DROP_INTERVAL);

}  /* End of dplaudio_evnt_CB */


static void dplaudio_process_request_dec_buffer_oob_evt_msg (
  aprv2_packet_t* packet
  )
{
  uint8* vocoderPacket = NULL;
  uint32 packetLength ;
  int32_t rc;
  QPUINT32 iLastPLayedAudioTimeStamp = 0;
  QPINT iResult = 0;
  QPC_GLOBAL_DATA* pGlobalData  = QP_NULL;
  QP_AUDIO_DATA_AMSS* pAudioData = QP_NULL;
  qpDplDecriptorData *pDplDescData = QP_NULL;
  QPDPL_QDJ_FRAME_TYPE      qdj_frame;
  uint8 temp=0;
  //uint16 timewarp_factor;
  int mode = 0;
  int frameType = 0;
  uint8* temp_store_data = NULL;

  pGlobalData = qpDplGetGlobalData();
  if (pGlobalData)
  {
    pDplDescData = (qpDplDecriptorData *)pGlobalData->pAudioData;
    if(pDplDescData)
    {
      pAudioData = (QP_AUDIO_DATA_AMSS*) pDplDescData->uDecriptors.Descriptor.pDesc;
    }
    else
    {
      IMS_MSG_ERR_0(LOG_IMS_DPL, "dplaudio_process_request_dec_buffer_oob_evt_msg ...pDplDescData  is NULL");
    }
  }
  else
  {
    IMS_MSG_ERR_0(LOG_IMS_DPL, "dplaudio_process_request_dec_buffer_oob_evt_msg ...pGlobalData  is NULL");
    __aprv2_cmd_free( dplaudio_apr_handle, packet );
    return;
  } 


  if(pDplDescData && eCvdStateRun != pDplDescData->eCvdState)
  {
    IMS_MSG_ERR_1(LOG_IMS_DPL, "dplaudio_process_request_dec_buffer_oob_evt_msg ... CVD in wrong  state %d", pDplDescData->eCvdState);
    __aprv2_cmd_free( dplaudio_apr_handle, packet );
    return;
  }

  if(QP_NULL == pAudioData)
  {
    IMS_MSG_ERR_0(LOG_IMS_DPL, "dplaudio_process_request_dec_buffer_oob_evt_msg ...pDplDescData  is NULL");
    __aprv2_cmd_free( dplaudio_apr_handle, packet );
    return;
  }

  if( pAudioData->hQdjHandle == NULL )
  {
    IMS_MSG_ERR_0(LOG_IMS_DPL, "dplaudio_process_request_dec_buffer_oob_evt_msg ... QDJ handle is NULL");
    __aprv2_cmd_free( dplaudio_apr_handle, packet );
    return;
  }



  /*------------------------------------------------------------------------
  If we got here that means audio codec is up and running let us flag 
  this thing. packets will get queued only when this is detected.
  ------------------------------------------------------------------------*/
  if( !pDplDescData->bIssink_active   )
  {
    pDplDescData->bIssink_active = QP_TRUE;
  }

  /*---------------------------------------------------------------------
  Dequeue an element here
  ---------------------------------------------------------------------*/
  qpDplMemset(&qdj_frame, 0, sizeof(QPDPL_QDJ_FRAME_TYPE));
  vocoderPacket = (QPUINT8*)pDplDescData->base_virt_addr;
  qdj_frame.data = vocoderPacket;
  qdj_frame.data += (QPDL_AUDIO_DEC_OFFSET + 12 + VOICE_PACKET_HDR_LENGTH);

  /*---------------------------------------------------------------------
  this is max length of an audio RTP pkt
  ---------------------------------------------------------------------*/
  qdj_frame.len = VOICE_PACKET_LENGTH;

  if( qpDplQdjDequeue(pAudioData->hQdjHandle, &qdj_frame ) == 0 )
  {
    (void)qpDplQdjService(pAudioData->hQdjHandle);
	
    //To handle qdj unawareness of device switch
    if( event_counter>=0 && event_counter < 3 ) 
    {
      IMS_MSG_MED_0(LOG_IMS_DPL,"Feeding silent frames since the cvd event reeturned to ready state from nonready state");
      //check for silence if not make it as a silence
      qdj_frame.len  = 0;
      event_counter += 1;
    }

    vocoderPacket += QPDL_AUDIO_DEC_OFFSET;
    vocoderPacket += 12;
  	if(pDplDescData->tCodecConfig.eCodec == AUDIO_CODEC_AMR ||
         pDplDescData->tCodecConfig.eCodec == AUDIO_CODEC_AMR_WB)
  	{	
      frameType = 0;//MVS_AMR_SPEECH_GOOD;

      if(qdj_frame.len == 5)
      {
        g_TimelineToSend = TRUE;
        temp=qdj_frame.data[4]&16;
        if(temp==0)
        {
          frameType = 4;
        }
        else
        {
          frameType = 5;
        }
      }
      if(qdj_frame.len == 0)
      {
        frameType = 7;
      }
      if(qdj_frame.len == 5 &&  (qdj_frame.profile_info.amr_info.amr_fqi==0))
        frameType= 6;
      if(qdj_frame.len > 5 &&  (qdj_frame.profile_info.amr_info.amr_fqi==0))
        frameType= 3;

      mode = qdj_frame.profile_info.amr_info.amr_mode;
      
	  /* PrevRxMode is set to the current codec when the value is the default value*/
	  if((pDplDescData->tCodecConfig.eCodec == AUDIO_CODEC_AMR && g_PrevRxMode >= 8) ||
	   (pDplDescData->tCodecConfig.eCodec == AUDIO_CODEC_AMR_WB  && g_PrevRxMode >= 9)) 
	  { 
		g_PrevRxMode = pDplDescData->tCodecConfig.sCodecInfo.sAMRInfo.eCodecMode;
	  }

	  /* If its a speech frame then assing the PrevRxMode from the speech packet*/
      if(qdj_frame.len != 0 && qdj_frame.len != 5)
      {
        g_PrevRxMode = mode;
      }
      else
      {
        mode = g_PrevRxMode;
      }

      pDplDescData->iPrevFrameType = frameType;
      //Populate the packet header
      *vocoderPacket = ((frameType & 0x0F) << 4) | (mode & 0x0F);		
  	}
    else if (pDplDescData->tCodecConfig.eCodec == AUDIO_CODEC_EVS)
    {
         //whole thing should be in primary check: TBD
         mode = qdj_frame.profile_info.evs_info.evs_mode;
         if(!qdj_frame.len)
         {
            //where is primary or IO check?
            mode = EVS_NODATA_FTYPE;
         }
         else if(qdj_frame.len == EVS_PRIM_SID_LEN)
         {
            //where is primary or IO check?
            mode = EVS_SID_PRIM_FTYPE;
         }
         else
         {
            mode += EVS_NODATA_FTYPE + 1/* add 1 since mode starts with zero */;
         }
         *vocoderPacket = ((qdj_frame.partialCopy & 0x01) << 5) | (mode & 0x1F);		
         IMS_MSG_LOW_3(LOG_IMS_DPL,"dplaudio_process_request_dec_buffer_oob_evt_msg evs hdr %d  Mode %d", *vocoderPacket, mode, 0);  
    }
  	else if(((pDplDescData->tCodecConfig.eCodec == AUDIO_CODEC_G711U) || 
                (pDplDescData->tCodecConfig.eCodec == AUDIO_CODEC_G711A)) )
  	{
  		uint8 header = 0;
  	    //mode = qdj_frame.profile_info.amr_info.amr_mode;

  	    frameType = 0;//MVS_AMR_SPEECH_GOOD;

  		/*Selecting prame type*/
  	    if(qdj_frame.len == 0) //erasure
  	    {
	      frameType = AUDIO_G711_NTX;		
          qdj_frame.len = 0;		
  	    }
  	    else if(qdj_frame.silence) //silence
  	    {	 
		  frameType = AUDIO_G711_SID;
          qdj_frame.len = 11;
  	    }
  	    else //speech
  	    {
	      frameType = AUDIO_G711_SPEECH;
          qdj_frame.len = 160;// always send the G711 packet of 160 bytes to CVD
  	    }

  		/* Header for G711.1 */
  		if(pDplDescData->tCodecConfig.eCodec == AUDIO_CODEC_G711U)
  		{
  			header = (frameType &  0x03); /*Companding type for U Law is 0*/
  		}
  		else if(pDplDescData->tCodecConfig.eCodec == AUDIO_CODEC_G711A)
  		{
  			header = ((0x04) | (frameType &  0x03)); /*Companding type for A Law is 1*/
  		}


         IMS_MSG_HIGH_3(LOG_IMS_DPL,"temp_store_data G711 qdj_frame.len = %d header=%d frameType:%d", qdj_frame.len,header,frameType);
         *vocoderPacket = header;

  	 }	

  }

  iResult = qpDplQdjGetCurrentRTPPlayoutTS(pAudioData->hQdjHandle, &iLastPLayedAudioTimeStamp);

  if(iResult == 1)
  {
		if( (g_TimelineToSend && (qdj_frame.len > 0 && !qdj_frame.silence) ) || g_firstdequeue)      
		{   
			IMS_MSG_LOW_1(LOG_IMS_DPL,"sending last played audio time stamp %u ", iLastPLayedAudioTimeStamp);
			(QPVOID)qvp_rtp_service_last_played_audio_ts(iLastPLayedAudioTimeStamp);
			g_TimelineToSend = FALSE;
			g_firstdequeue = FALSE;
		}
  }


  packetLength = qdj_frame.len + VOICE_PACKET_HDR_LENGTH;
  /** Add Packet Length and Media type **/
  {
    QPUINT32 * temp_var = NULL;
    QPUINT8* temp_read = (QPUINT8*)pDplDescData->base_virt_addr;
    temp_read += QPDL_AUDIO_DEC_OFFSET;
    temp_var = (QPUINT32*)temp_read;
    *temp_var = 0; /** OPtional AV timer on RX side**/
    temp_var++;
    if(pDplDescData->tCodecConfig.eCodec == AUDIO_CODEC_AMR)
    {
      *temp_var = VSS_MEDIA_ID_AMR_NB_MODEM; //Need to set to AMR or WB based on the codec config
		  if(pDplDescData->is_eamr_enabled)
		    *temp_var = VSS_MEDIA_ID_EAMR;		
	  }
    else if(pDplDescData->tCodecConfig.eCodec == AUDIO_CODEC_AMR_WB)
      *temp_var = VSS_MEDIA_ID_AMR_WB_MODEM; //Need to set to AMR or WB based on the codec config
    else if(pDplDescData->tCodecConfig.eCodec == AUDIO_CODEC_G711A)
      *temp_var = VSS_MEDIA_ID_G711_ALAW_V2; //Need to set to based on the codec config
    else if(pDplDescData->tCodecConfig.eCodec == AUDIO_CODEC_G711U)
      *temp_var = VSS_MEDIA_ID_G711_MULAW_V2; //Need to set to based on the codec config
    else if(pDplDescData->tCodecConfig.eCodec == AUDIO_CODEC_EVS)
      *temp_var = VSS_MEDIA_ID_EVS;
      
    temp_var++;
    *temp_var = packetLength;
  }
#if 0
  /*---------------------------------------------------------------------
  Send Time Warping Info if there is time warping factor
  ---------------------------------------------------------------------*/

  /*send time warping. */

  cvd_cmd_timewarp.enable_time_warp = 1;
  timewarp_factor = qdj_frame.tw_factor;
  if ( timewarp_factor < 50 ) /* VCP does not accept valus less than 1/2 of the non-compressed/non-expanded PCM frame length */
  {
    timewarp_factor = 50;
  }
  if(pDplDescData->tCodecConfig.eCodec == AUDIO_CODEC_AMR_WB)
  {
    cvd_cmd_timewarp.exp_length = (320 * timewarp_factor)/100;
  }
  else
  {
    cvd_cmd_timewarp.exp_length = (160 * timewarp_factor)/100;
  }
  cvd_cmd_timewarp.enable_phase_match = 0;
  cvd_cmd_timewarp.run_length = 0;
  cvd_cmd_timewarp.phase_offset = 0;


  dplaudio_send_command(dplaudio_cvs_addr, dplaudio_app_cvs_port,
    0x12345678,
    VSS_ISTREAM_CMD_SET_DEC_TIMEWARP,
    &cvd_cmd_timewarp, sizeof( cvd_cmd_timewarp ),
    dplaudio_generic_basic_rsp_handler_cb );
#endif
  if(pAudioData->bIsLoopBackMode)
  {
    /* Loop back the received near-end Tx packet back to Rx. */

    QP_MULTIMEDIA_FRAME iAudioDPLFrame;
    uint64               aud_timestamp;
    QPUINT8 iReqBundleSize        = 1;

    qvp_rtp_time_get_ms(&aud_timestamp);

    qpDplMemset(&iAudioDPLFrame, 0, sizeof(QP_MULTIMEDIA_FRAME));

    iAudioDPLFrame.iDataLen = qdj_frame.len;
    iAudioDPLFrame.pData    = qdj_frame.data;


    if ((AUDIO_CODEC_AMR == pDplDescData->tCodecConfig.eCodec) || (AUDIO_CODEC_AMR_WB == pDplDescData->tCodecConfig.eCodec))
    {
      iAudioDPLFrame.sFrameInfo.sAMRInfo.iAMRModeRequest = pDplDescData->tCodecConfig.sCodecInfo.sAMRInfo.eCodecMode;
    }

    iAudioDPLFrame.sMediaPacketInfo.sMediaPktInfoTx.iTimeStamp = qdj_frame.rtp_tstamp;

    qpDplMemscpy(pAudioData->pAudioBundle[pAudioData->iBundleSize], sizeof(QP_MULTIMEDIA_FRAME), &iAudioDPLFrame, sizeof(QP_MULTIMEDIA_FRAME));
    // Avoiding the copy just assigning the pointer
    pAudioData->pAudioBundle[pAudioData->iBundleSize]->pData = iAudioDPLFrame.pData;


    if (pAudioData->tAudioCallBack)
    {
      pAudioData->tAudioCallBack(AUDIO_MSG_RECORDED_DATA, pAudioData->pAudioBundle,
        iReqBundleSize, 
        pAudioData->pUserData);

      IMS_MSG_LOW_1(LOG_IMS_DPL,"Posting Recorded Data of size %d ", pAudioData->pAudioBundle[pAudioData->iBundleSize]->iDataLen);
    }
    pAudioData->iBundleSize = 0;

  }

  IMS_MSG_LOW_3(LOG_IMS_DPL,"dplaudio_process_request_dec_buffer_oob_evt_msg len %d  Mode %d frameType %d", packetLength, mode, frameType);  

  /** Invalidate the cache **/
  {
    vsd_status_t status;
    voicemem_cmd_cache_flush_t voice_mem_config;
    QPUINT8* temp_dec_ptr = (QPUINT8*)pDplDescData->base_virt_addr;
    temp_dec_ptr += QPDL_AUDIO_DEC_OFFSET;	
    voice_mem_config.voicemem_handle = pDplDescData->voicemem_handle;
    voice_mem_config.virt_addr = temp_dec_ptr;
    voice_mem_config.size = (8192-QPDL_AUDIO_DEC_OFFSET);
    status = voicemem_call(VOICEMEM_CMD_CACHE_FLUSH, &voice_mem_config, sizeof(voicemem_cmd_cache_flush_t));
    if(VSD_EOK != status)
    {
      IMS_MSG_ERR_0(LOG_IMS_DPL,"VOICEMEM_CMD_CACHE_FLUSH failed");
    }

  }

  __aprv2_cmd_free( dplaudio_apr_handle, packet );
  
  /*rc = *///dpl_send_dec_pkt((void*)vocoderPacket,packetLength,packet,pDplDescData->tCodecConfig.eCodec);
  if(temp_store_data)
  {
  	qpDplFree(MEM_IMS_DPL,temp_store_data);
  }  
  /** Send Decoded buffer Ready **/
  
  IMS_MSG_HIGH_0(LOG_IMS_DPL,"Posting CVD Command  VSS_ISTREAM_EVT_OOB_NOTIFY_DEC_BUFFER_READY");
  
  rc = __aprv2_cmd_alloc_send( dplaudio_apr_handle,
    APRV2_PKT_MSGTYPE_SEQCMD_V,
    dplaudio_my_addr, dplaudio_my_port,
    dplaudio_cvs_addr, dplaudio_app_cvs_port,
    0,
    VSS_ISTREAM_EVT_OOB_NOTIFY_DEC_BUFFER_READY,
    NULL, 0 );
    DPLAUDIO_PANIC_ON_ERROR( rc );
    if ( rc )
    {
      IMS_MSG_ERR_0(LOG_IMS_DPL,"__aprv2_cmd_alloc_send failed");
      ASSERT(0);
    }
    else
    {
		if (frameType == 4 || frameType == 5)
		{
			/*silence frame*/
			++g_downlink_silence_pkt_count;
		}
		else if (frameType == 7)
		{
			/*0 size packet*/
			++g_downlink_no_data_pkt_count;
		}
		else if (frameType == 0)
		{
			/*regular speech packet*/
			++g_downlink_speech_pkt_count;
		}
      IMS_MSG_LOW_3(LOG_IMS_DPL,"g_downlink_silence_pkt_count %d g_downlink_no_data_pkt_count %d g_downlink_speech_pkt_count %d",\
				   g_downlink_silence_pkt_count,g_downlink_no_data_pkt_count,g_downlink_speech_pkt_count);
      g_downlink_pkt_count++;
      IMS_MSG_MED_1(LOG_IMS_DPL,"g_downlink_pkt_count %d",g_downlink_pkt_count);
    }
	if((qdj_frame.len == 0 || qdj_frame.silence)&& srvcc_tear_down_req)
	{
		cvd_event_not_ready = 1;
		if(qpAudioStop(pDplDescData))
		{
			//do some thing
		}
	}
}

static void dplaudio_process_request_dec_buffer_evt_msg (
  aprv2_packet_t* packet
  )
{
  uint8 vocoderPacket[VOICE_PACKET_LENGTH];
  uint32 packetLength ;
  QPUINT32 iLastPLayedAudioTimeStamp = 0;
  QPINT iResult = 0;
  QPC_GLOBAL_DATA* pGlobalData  = QP_NULL;
  QP_AUDIO_DATA_AMSS* pAudioData = QP_NULL;
  qpDplDecriptorData *pDplDescData = QP_NULL;
  QPDPL_QDJ_FRAME_TYPE      qdj_frame;
  
  //uint16 timewarp_factor;
  int mode = 0;
  int frameType = 0;
  //int32_t rc = 0;
  int8 dequeueReturnType =0;

  if(cvd_event_not_ready)
  	{
  		(void)rex_clr_timer(&rex_timer_handle);
  		cvd_event_not_ready = 0;
  	}
  pGlobalData = qpDplGetGlobalData();
  if (pGlobalData)
  {
    pDplDescData = (qpDplDecriptorData *)pGlobalData->pAudioData;
    if(pDplDescData)
    {
      pAudioData = (QP_AUDIO_DATA_AMSS*) pDplDescData->uDecriptors.Descriptor.pDesc;
    }
    else
    {
      IMS_MSG_ERR_0(LOG_IMS_DPL, "dplaudio_process_request_dec_buffer_evt_msg ...pDplDescData  is NULL");
    }
  }
  else
  {
    IMS_MSG_ERR_0(LOG_IMS_DPL, "dplaudio_process_request_dec_buffer_evt_msg ...pGlobalData  is NULL");
    __aprv2_cmd_free( dplaudio_apr_handle, packet );
    return;
  } 


  if(pDplDescData && eCvdStateRun != pDplDescData->eCvdState)
  {
    IMS_MSG_ERR_1(LOG_IMS_DPL, "dplaudio_process_request_dec_buffer_evt_msg ... CVD in wrong  state %d", pDplDescData->eCvdState);
    __aprv2_cmd_free( dplaudio_apr_handle, packet );
    return;
  }

  if(QP_NULL == pAudioData)
  {
    IMS_MSG_ERR_0(LOG_IMS_DPL, "dplaudio_process_request_dec_buffer_evt_msg ...pDplDescData  is NULL");
    __aprv2_cmd_free( dplaudio_apr_handle, packet );
    return;
  }

  if( pAudioData->hQdjHandle == NULL )
  {
    IMS_MSG_ERR_0(LOG_IMS_DPL, "dplaudio_process_request_dec_buffer_evt_msg ... QDJ handle is NULL");
	__aprv2_cmd_free( dplaudio_apr_handle, packet );
    return;
  }

  /*------------------------------------------------------------------------
    If we got here that means audio codec is up and running let us flag 
    this thing. packets will get queued only when this is detected.
  ------------------------------------------------------------------------*/
  if( !pDplDescData->bIssink_active   )
  {
    pDplDescData->bIssink_active = QP_TRUE;
  }

  /*---------------------------------------------------------------------
  Dequeue an element here
  ---------------------------------------------------------------------*/
  qpDplMemset(&qdj_frame, 0, sizeof(QPDPL_QDJ_FRAME_TYPE));
  qdj_frame.data = (uint8*) &vocoderPacket + VOICE_PACKET_HDR_LENGTH;

  /*---------------------------------------------------------------------
  this is max length of an audio RTP pkt
  ---------------------------------------------------------------------*/
  qdj_frame.len = VOICE_PACKET_LENGTH; 

  
  dequeueReturnType = qpDplQdjDequeue(pAudioData->hQdjHandle, &qdj_frame );
 if(  dequeueReturnType == 0 )
  {
    mode = qdj_frame.profile_info.amr_info.amr_mode;

    frameType = 0;//MVS_AMR_SPEECH_GOOD;

    if(qdj_frame.len == 5)
    {
  	  g_TimelineToSend = TRUE;	 
	  
      if(pDplDescData->iPrevFrameType == 0)
      {
        frameType = 4;
      }
      else
      {
        frameType = 5;
      }
	  if(pDplDescData->tCodecConfig.eCodec == AUDIO_CODEC_AMR && g_PrevRxMode < 8)
			mode = g_PrevRxMode;
	  else if(pDplDescData->tCodecConfig.eCodec == AUDIO_CODEC_AMR_WB  && g_PrevRxMode < 9)
			mode = g_PrevRxMode;
	  else 
      mode = pDplDescData->tCodecConfig.sCodecInfo.sAMRInfo.eCodecMode;

    }

    if(qdj_frame.len == 0)
	{
      frameType = 7;
	  g_TimelineToSend = TRUE;
	}

  if(qdj_frame.len != 0 && qdj_frame.len != 5)
  {
    g_PrevRxMode = mode;
  }

    pDplDescData->iPrevFrameType = frameType;

	//To handle qdj unawareness of device switch
	if( event_counter>=0 && event_counter < 3 ) 
	{
		 IMS_MSG_MED_0(LOG_IMS_DPL,"Feeding silent frames since the cvd event reeturned to ready state from nonready state");
		//check for silence if not make it as a silence
	    qdj_frame.len  = 0;
		event_counter += 1;
	}
    //TODO Populate this by MVS_AMR_SID_UPDATE 

    //Populate the packet header
    *vocoderPacket = ((frameType & 0x0F) << 4) | (mode & 0x0F);
  }

	iResult = qpDplQdjGetCurrentRTPPlayoutTS(pAudioData->hQdjHandle, &iLastPLayedAudioTimeStamp);

	if(iResult == 1)
	{
     if( (g_TimelineToSend && (qdj_frame.len > 5) ) || g_firstdequeue)      
		{   
		  IMS_MSG_LOW_1(LOG_IMS_DPL,"sending last played audio time stamp %u ", iLastPLayedAudioTimeStamp);
		  (QPVOID)qvp_rtp_service_last_played_audio_ts(iLastPLayedAudioTimeStamp);
           g_TimelineToSend = FALSE;
		  g_firstdequeue = FALSE;
		}
	}


  packetLength = qdj_frame.len + VOICE_PACKET_HDR_LENGTH;
#if 0
  /*---------------------------------------------------------------------
  Send Time Warping Info if there is time warping factor
  ---------------------------------------------------------------------*/

  /*send time warping. */

  cvd_cmd_timewarp.enable_time_warp = 1;
  timewarp_factor = qdj_frame.tw_factor;
  if ( timewarp_factor < 50 ) /* VCP does not accept valus less than 1/2 of the non-compressed/non-expanded PCM frame length */
  {
    timewarp_factor = 50;
  }
  if(pDplDescData->tCodecConfig.eCodec == AUDIO_CODEC_AMR_WB)
  {
    cvd_cmd_timewarp.exp_length = (320 * timewarp_factor)/100;
  }
  else
  {
    cvd_cmd_timewarp.exp_length = (160 * timewarp_factor)/100;
  }
  cvd_cmd_timewarp.enable_phase_match = 0;
  cvd_cmd_timewarp.run_length = 0;
  cvd_cmd_timewarp.phase_offset = 0;


  dplaudio_send_command(dplaudio_cvs_addr, dplaudio_app_cvs_port,
    0x12345678,
    VSS_ISTREAM_CMD_SET_DEC_TIMEWARP,
    &cvd_cmd_timewarp, sizeof( cvd_cmd_timewarp ),
    dplaudio_generic_basic_rsp_handler_cb );
#endif
  if(pAudioData->bIsLoopBackMode)
  {
    /* Loop back the received near-end Tx packet back to Rx. */

    QP_MULTIMEDIA_FRAME iAudioDPLFrame;
    uint64               aud_timestamp;
    QPUINT8 iReqBundleSize        = 1;

    qvp_rtp_time_get_ms(&aud_timestamp);

    qpDplMemset(&iAudioDPLFrame, 0, sizeof(QP_MULTIMEDIA_FRAME));

    iAudioDPLFrame.iDataLen = qdj_frame.len;
    iAudioDPLFrame.pData    = qdj_frame.data;


    if ((AUDIO_CODEC_AMR == pDplDescData->tCodecConfig.eCodec) || (AUDIO_CODEC_AMR_WB == pDplDescData->tCodecConfig.eCodec))
    {
      iAudioDPLFrame.sFrameInfo.sAMRInfo.iAMRModeRequest = pDplDescData->tCodecConfig.sCodecInfo.sAMRInfo.eCodecMode;
    }

    iAudioDPLFrame.sMediaPacketInfo.sMediaPktInfoTx.iTimeStamp = qdj_frame.rtp_tstamp;

    qpDplMemscpy(pAudioData->pAudioBundle[pAudioData->iBundleSize], sizeof(QP_MULTIMEDIA_FRAME), &iAudioDPLFrame, sizeof(QP_MULTIMEDIA_FRAME));
    // Avoiding the copy just assigning the pointer
    pAudioData->pAudioBundle[pAudioData->iBundleSize]->pData = iAudioDPLFrame.pData;


    if (pAudioData->tAudioCallBack)
    {
      pAudioData->tAudioCallBack(AUDIO_MSG_RECORDED_DATA, pAudioData->pAudioBundle,
        iReqBundleSize, 
        pAudioData->pUserData);

      IMS_MSG_LOW_1(LOG_IMS_DPL,"Posting Recorded Data of size %d ", pAudioData->pAudioBundle[pAudioData->iBundleSize]->iDataLen);
    }
    pAudioData->iBundleSize = 0;

  }

  IMS_MSG_LOW_3(LOG_IMS_DPL,"dplaudio_process_request_dec_buffer_evt_msg len %d  Mode %d frameType %d", packetLength, mode, frameType);  
  if (frameType == 4 || frameType == 5)
  {
	  ++g_downlink_silence_pkt_count;
  }
  else if (frameType == 7)
  {
	  ++g_downlink_no_data_pkt_count;
  }
  else if (frameType == 0)
  {
	  ++g_downlink_speech_pkt_count;
  }
  IMS_MSG_LOW_3(LOG_IMS_DPL,"g_downlink_silence_pkt_count %d g_downlink_no_data_pkt_count %d g_downlink_speech_pkt_count %d",\
			   g_downlink_silence_pkt_count,g_downlink_no_data_pkt_count,g_downlink_speech_pkt_count);
  /*rc = */dpl_send_dec_pkt((void*)vocoderPacket,packetLength,packet,pDplDescData->tCodecConfig.eCodec);

 
  if(  dequeueReturnType == 0 )
     (void)qpDplQdjService(pAudioData->hQdjHandle);

  //IMS_MSG_LOW_0(LOG_IMS_DPL, " packet sent to CVD ");

  __aprv2_cmd_free( dplaudio_apr_handle, packet );
}

static void dplaudio_generic_basic_rsp_handler_cb (
  uint16_t my_port,
  uint16_t server_addr,
  uint16_t server_port,
  uint32_t token,
  uint32_t opcode,
  void* payload,
  uint32_t payload_size
  )
{
  aprv2_ibasic_rsp_result_t* rsp = ( ( aprv2_ibasic_rsp_result_t* ) payload );

  QPUINT64              *iFrameTS = NULL;
  uint64				  current_time =0;
  QPC_GLOBAL_DATA* pGlobalData = QP_NULL;
  qpDplDecriptorData *pDplDescData = QP_NULL;
  QP_AUDIO_DATA_AMSS *pAudioData = QP_NULL; 
  //int32_t rc = 0;

  if(QP_NULL == rsp)
  {
    IMS_MSG_ERR_0(LOG_IMS_DPL,"Response is NULL ");
    return ;// Check with VIkram   
  }

  IMS_MSG_MED_2(LOG_IMS_DPL,"Processing the response %x, status %d ", rsp->opcode, rsp->status );

  pGlobalData = qpDplGetGlobalData();

  if (NULL ==pGlobalData)
  {
    IMS_MSG_ERR_0(LOG_IMS_DPL,"GlobalData is NULL");
    return;
  }
  pDplDescData = (qpDplDecriptorData *)pGlobalData->pAudioData;
  if (NULL ==pDplDescData)
  {
    IMS_MSG_ERR_0(LOG_IMS_DPL,"AudioDesc in GlobalData is NULL");
    return;
  }

  /* Verify that the opcode is expected. */
  if ( opcode != APRV2_IBASIC_RSP_RESULT )
  {
    if(  opcode == VSS_IAVTIMER_RSP_GET_TIME)
    {
      QP_AUDIO_DATA_AMSS* pAudioData = QP_NULL;
      if (payload_size != sizeof(QPUINT64))
      {
        IMS_MSG_LOW_1(LOG_IMS_DPL,"unmatched payload size = %d ", payload_size);
        return;
      }
      //TBD: Assuming eDev is RecPlayer later there should be just one descriptor
      pAudioData = (QP_AUDIO_DATA_AMSS*) pDplDescData->uDecriptors.Descriptor.pDesc;
      if (NULL == pAudioData /*&& (eAudioStateStarted == pAudioData->iCurrentState)*/)
      {
        IMS_MSG_ERR_0(LOG_IMS_DPL,"AudioData is NULL");
        return;
      }

      iFrameTS = (QPUINT64*)payload;
      current_time = *iFrameTS;
      av_timestamp64 = (av_timestamp64 & 0xFFFFFFFF00000000LL);
      IMS_MSG_ERR_2(LOG_IMS_DPL,"VSS_IAVTIMER_RSP_GET_TIME avsync current time in microsecond = %Lu av_timestamp64 = %Lu", current_time, av_timestamp64>>32);
      if(g_iAVsyncTime && !g_is_warp_around)
      {
        if(g_updateAVdelay)
        {
        /*	If there is change in AV Tx Delay Then log the Event	*/								
          g_iAVSyncTxDelay = (current_time - g_iTimeInTxPkt);
          qpDplLogAudioDelayEvents(g_iAVSyncTxDelay,0);
          IMS_MSG_ERR_3(LOG_IMS_DPL,"VSS_IAVTIMER_RSP_GET_TIME avsync current time in microsecond = %Lu last TimeInTxPkt = %Lu and g_iAVSyncTxDelay %lu ", current_time, g_iTimeInTxPkt, g_iAVSyncTxDelay);							
          g_updateAVdelay = FALSE;
        }
        else
        {
          g_updateAVdelay = TRUE;
        }
      }
      else
      {
        if( current_time > g_iAVsyncTime )
        {
          g_iAVsyncTime = current_time;
          av_timestamp64 = current_time;
          qvp_rtp_time_get_ms(&g_iAVmSysTime);

          //g_iAVSyncTxDelay = 0;
          g_is_warp_around = FALSE;
          current_time /= 1000; //in ms
          IMS_MSG_ERR_2(LOG_IMS_DPL,"VSS_IAVTIMER_RSP_GET_TIME avsyncfeed current time in microsecond = %Lx = %Lu", current_time, current_time);
          qvp_rtp_service_send_av_sync_feed_first(current_time);
          /*
          if(!g_updateAVdelay)
          {
          g_updateAVdelay = TRUE;
          }*/
        }
        else
        {
          IMS_MSG_ERR_3(LOG_IMS_DPL,"VSS_IAVTIMER_RSP_GET_TIME Alarm:current time in us = %Lx = %Lu < prev time: %LX. Not updating the time", current_time, current_time, g_iAVsyncTime);
        }
      }
    }
	  else if(VSS_IMEMORY_RSP_MAP == opcode)
    {

      IMS_MSG_MED_0(LOG_IMS_DPL,"Received VSS_IMEMORY_RSP_MAP");
      pAudioData = (QP_AUDIO_DATA_AMSS*) pDplDescData->uDecriptors.Descriptor.pDesc;
      if (QP_NULL == pAudioData)
      {
        IMS_MSG_ERR_0(LOG_IMS_DPL,"pAudioData is NULL");
        return;
      }
      pDplDescData->mapped_mem_handle = ( ( vss_imemory_rsp_map_t* ) payload )->mem_handle;
      IMS_MSG_ERR_1(LOG_IMS_DPL,"mapped_mem_handle : %d",pDplDescData->mapped_mem_handle);
      pDplDescData->eCvdState = eCvdStateInited;
      pAudioData->iCurrentState = eAudioStateInited;
      if (pAudioData->tAudioCallBack)
      {
        pAudioData->tAudioCallBack(AUDIO_MSG_DEV_INITIALISED, QP_NULL, pAudioData->eADev, pAudioData->pUserData);
        IMS_MSG_HIGH_0(LOG_IMS_DPL,"Posting Audio Initialized");
        if (QP_TRUE == pDplDescData->bConfigPending)
        {
          IMS_MSG_LOW_0(LOG_IMS_DPL,"CodecSet is Pending Initiating the configuration now");
          dpl_cvd_configure(pDplDescData);
          pDplDescData->bConfigPending = QP_FALSE;
        }
      }
      else
      {
        IMS_MSG_ERR_0(LOG_IMS_DPL,"Audio is INITIALIZED but failed to raise CB");
      }			
    }
    else
    {
      IMS_MSG_ERR_1(LOG_IMS_DPL,"Unexpected command response type %x", opcode);
      return;
    }
  }

  /* Verify that the payload size is expected. */
  if ( payload_size != sizeof( aprv2_ibasic_rsp_result_t ) )
  {
    IMS_MSG_ERR_0(LOG_IMS_DPL,"Unexpected command response payload size");
  }

  switch ( rsp->opcode )
  {

  case VSS_IMEMORY_CMD_UNMAP:
    {

      voicemem_cmd_free_t voicemem_cmd_config;
      vsd_status_t status;
      voicemem_cmd_config.voicemem_handle = pDplDescData->voicemem_handle;
      status = voicemem_call(VOICEMEM_CMD_FREE,&voicemem_cmd_config,sizeof(voicemem_cmd_free_t));	

      if(VSD_EOK != status)
      {
        IMS_MSG_ERR_0(LOG_IMS_DPL,"VOICEMEM_CMD_FREE failed");
      }
      status = voicemem_call(VOICEMEM_CMD_DEINIT,NULL,0);	
      if(VSD_EOK != status)
      {
        IMS_MSG_ERR_0(LOG_IMS_DPL,"VOICEMEM_CMD_DEINIT failed");
      }			
      pDplDescData->eCvdState = eCvdStateDeInitDetachStream;
      /* Detach the CVS instance from the MVM instance. */
      dplaudio_send_command( dplaudio_mvm_addr, dplaudio_app_mvm_port,
        0x12345678,
        VSS_IMVM_CMD_DETACH_STREAM,
        &dplaudio_app_cvs_port, sizeof( dplaudio_app_cvs_port ),
        dplaudio_generic_basic_rsp_handler_cb );

    }
    break;  
  case VSS_IMVM_CMD_ATTACH_STREAM:
    {
      QP_AUDIO_DATA_AMSS* pAudioData = QP_NULL;
      vss_inotify_cmd_listen_for_event_class_t listen_evt_cmd;
      IMS_MSG_HIGH_0(LOG_IMS_DPL,"received APR_EOK in response to VSS_IMVM_CMD_ATTACH_STREAM");
      if (eCvdStateInitAttachStream != pDplDescData->eCvdState)
      {
        IMS_MSG_ERR_2(LOG_IMS_DPL,"Unexpected CMD message: %x in State: %x", rsp->opcode, pDplDescData->eCvdState );
        break;
      }
      //TBD: Assuming eDev is RecPlayer later there should be just one descriptor
      pAudioData = (QP_AUDIO_DATA_AMSS*) pDplDescData->uDecriptors.Descriptor.pDesc;
      if (NULL == pAudioData)
      {
          IMS_MSG_ERR_0(LOG_IMS_DPL,"AudioData is NULL");
          break;
      }
      listen_evt_cmd.class_id = VSS_IAVSYNC_EVENT_CLASS_RX;

      dplaudio_send_command( dplaudio_cvs_addr, dplaudio_app_cvs_port,
      	0x12345678, VSS_INOTIFY_CMD_LISTEN_FOR_EVENT_CLASS,
      	&listen_evt_cmd, sizeof(vss_inotify_cmd_listen_for_event_class_t),
      	dplaudio_generic_basic_rsp_handler_cb );

      IMS_MSG_ERR_0(LOG_IMS_DPL,"Registering for VSS_IAVSYNC_EVENT_CLASS_RX event ");
    }
    break;
	
  case VSS_INOTIFY_CMD_LISTEN_FOR_EVENT_CLASS:
    {
      if (rsp->status != APR_EOK)
      {
        /*log error message */
       IMS_MSG_ERR_1(LOG_IMS_DPL,"ERROR: response code for VSS_INOTIFY_CMD_LISTEN_FOR_EVENT_CLASS was not APR_EOK but %d",rsp->status);
       break;
      }
      else
      {
        QP_AUDIO_DATA_AMSS* pAudioData = QP_NULL;
        IMS_MSG_HIGH_0(LOG_IMS_DPL,"received APR_EOK in response to VSS_INOTIFY_CMD_LISTEN_FOR_EVENT_CLASS");
        if (eCvdStateInitAttachStream == pDplDescData->eCvdState)
        {
          //TBD: Assuming eDev is RecPlayer later there should be just one descriptor
          pAudioData = (QP_AUDIO_DATA_AMSS*) pDplDescData->uDecriptors.Descriptor.pDesc;

          if (pAudioData && (eAudioStateIniting == pAudioData->iCurrentState))
          {

            dplaudio_send_command( dplaudio_mvm_addr, dplaudio_app_mvm_port,
              0x12345678,
              VSS_IMVM_CMD_MODEM_STANDBY_VOICE,
              QP_NULL, 0,
              dplaudio_generic_basic_rsp_handler_cb );

            pDplDescData->eCvdState = eCvdStateStandby;

          }
          else
          {
            IMS_MSG_ERR_0(LOG_IMS_DPL,"AudioData is NULL");
          }
        }
				else if (eCvdStateStandby == pDplDescData->eCvdState)
				{
					IMS_MSG_ERR_0(LOG_IMS_DPL,"Response for VSS_IAVSYNC_EVENT_CLASS_TX / VSS_IAVSYNC_EVENT_CLASS_RX : CVD in standby state ");
				}
        else
        {
        IMS_MSG_ERR_2(LOG_IMS_DPL,"Unexpected CMD message: %x in State: %x", rsp->opcode, pDplDescData->eCvdState );          
        }
      }
    }
    break;

   case VSS_ISTREAM_CMD_SET_MEDIA_TYPE:
    { /* Set the voice calibration profile. */
      vss_imvm_cmd_set_cal_network_t network_args;


      if (eCvdStateConfigMedia != pDplDescData->eCvdState)
      {
        IMS_MSG_ERR_2(LOG_IMS_DPL,"Unexpected CMD message: %x in State: %x", rsp->opcode, pDplDescData->eCvdState );
        break;
      }
      network_args.network_id = VSS_ICOMMON_CAL_NETWORK_ID_LTE;
      dplaudio_send_command( dplaudio_mvm_addr, dplaudio_app_mvm_port,
          0x12345678,
          VSS_IMVM_CMD_SET_CAL_NETWORK,
          &network_args, sizeof( network_args ),
          dplaudio_generic_basic_rsp_handler_cb );
      IMS_MSG_HIGH_1(LOG_IMS_DPL,"Setting network_id to  %d", network_args.network_id );

      pDplDescData->eCvdState = eCvdStateConfigNetwork;
      
    }
    break;
  case VSS_IMVM_CMD_SET_CAL_NETWORK:
  case VSS_ICOMMON_CMD_SET_NETWORK:
    {
      /** Changes were made as requested by Audio team to set Media type for MVM after getting response for SET network command **/
      vss_imvm_cmd_set_cal_media_type_t set_mvm_media_type;
      
      if (eCvdStateConfigNetwork != pDplDescData->eCvdState)
      {
        IMS_MSG_ERR_2(LOG_IMS_DPL,"Unexpected CMD message: %x in State: %x", rsp->opcode, pDplDescData->eCvdState );
        break;
      }
      switch(pDplDescData->tCodecConfig.eCodec)
      {
        case AUDIO_CODEC_AMR_WB:
          set_mvm_media_type.media_id = VSS_MEDIA_ID_AMR_WB_MODEM;
          break;
        case AUDIO_CODEC_G711U:
          set_mvm_media_type.media_id = VSS_MEDIA_ID_G711_MULAW_V2;
          break;
        case AUDIO_CODEC_G711A:
          set_mvm_media_type.media_id = VSS_MEDIA_ID_G711_ALAW_V2;
          break;
        case AUDIO_CODEC_EVS:
          set_mvm_media_type.media_id = VSS_MEDIA_ID_EVS;
          break;
        default:
          set_mvm_media_type.media_id = VSS_MEDIA_ID_AMR_NB_MODEM;
          break;
      }
      IMS_MSG_HIGH_0(LOG_IMS_DPL,"posting command VSS_IMVM_CMD_SET_CAL_MEDIA_TYPE for MVM");
        dplaudio_send_command( dplaudio_mvm_addr, dplaudio_app_mvm_port,
          0x12345678,
          VSS_IMVM_CMD_SET_CAL_MEDIA_TYPE,
          &set_mvm_media_type, sizeof( set_mvm_media_type ),
          dplaudio_generic_basic_rsp_handler_cb );

      pDplDescData->eCvdState = eCvdStateConfigMVMMedia;
    }
    break;
  case VSS_IMVM_CMD_SET_CAL_MEDIA_TYPE:
    {
      
      QP_CODEC_CONFIG* pCC = &pDplDescData->tCodecConfig;
      if(eCvdStateConfigMVMMedia!= pDplDescData->eCvdState)
      {  
        /* there has to be some action taken in unexpected message since
           they will leave the state machine in hanging state */
        IMS_MSG_ERR_2(LOG_IMS_DPL,"Unexpected CMD message: %x in State: %x", rsp->opcode, pDplDescData->eCvdState );
        break;
      }
      if (AUDIO_CODEC_EVS == pCC->eCodec) 
      {
        vss_imvm_cmd_set_max_var_voc_sampling_rate_t args;
        uint16 mxclk[] = {8000,16000,32000,48000};
        QP_EVS_CODEC_INFO *evs = NULL;
        evs = &pDplDescData->tCodecConfig.sCodecInfo.sEVSInfo;
        evs->tx_max_clk = mxclk[evs->eTxBW];
        evs->rx_max_clk = mxclk[evs->eRxBW];
        IMS_MSG_HIGH_2(LOG_IMS_DPL,"setting Max BW - tx=%d rx=%d",evs->tx_max_clk, evs->rx_max_clk);
        args.tx = pCC->sCodecInfo.sEVSInfo.tx_max_clk;
        args.rx = pCC->sCodecInfo.sEVSInfo.rx_max_clk;;
        IMS_MSG_ERR_2(LOG_IMS_DPL,"EVS:REQ- VSS_IMVM_CMD_SET_MAX_VAR_VOC_SAMPLING_RATE \
                      sampling rate; tx - %d rx - %d",args.tx,args.rx);
        dplaudio_send_command( dplaudio_mvm_addr, dplaudio_app_mvm_port,
            0x12345678,
            VSS_IMVM_CMD_SET_MAX_VAR_VOC_SAMPLING_RATE,
            &args, sizeof( vss_imvm_cmd_set_max_var_voc_sampling_rate_t ),
            dplaudio_generic_basic_rsp_handler_cb );
         pDplDescData->eCvdState = eCvdStateEvsMaxRate;
      }
      else
      {
        vss_icommon_cmd_set_voice_timing_t timing_args;
        timing_args.mode           = 0; /* No VFR. */
        timing_args.enc_offset     = 8600;
        timing_args.dec_offset     = 8000;
        timing_args.dec_req_offset = 1000;			  

        dplaudio_send_command( dplaudio_mvm_addr, dplaudio_app_mvm_port,
          0x12345678,
          VSS_ICOMMON_CMD_SET_VOICE_TIMING,
          &timing_args, sizeof( timing_args ),
          dplaudio_generic_basic_rsp_handler_cb );

        pDplDescData->eCvdState = eCvdStateConfigTiming;
      }

    }
    break;
  case VSS_IMVM_CMD_SET_MAX_VAR_VOC_SAMPLING_RATE:
   {
     vss_icommon_cmd_set_voice_timing_t timing_args;
     
     IMS_MSG_ERR_0(LOG_IMS_DPL,"EVS:RSP- VSS_IMVM_CMD_SET_MAX_VAR_VOC_SAMPLING_RATE");
     if(eCvdStateEvsMaxRate != pDplDescData->eCvdState)
     {  
       /* there has to be some action taken in unexpected message since
          they will leave the state machine in hanging state */
       IMS_MSG_ERR_2(LOG_IMS_DPL,"Unexpected CMD message: %x in State: %x", rsp->opcode, pDplDescData->eCvdState );
       break;
     }

     timing_args.mode           = 0; /* No VFR. */
     timing_args.enc_offset     = 8600;
     timing_args.dec_offset     = 8000;
     timing_args.dec_req_offset = 1000;        
     
     dplaudio_send_command( dplaudio_mvm_addr, dplaudio_app_mvm_port,
       0x12345678,
       VSS_ICOMMON_CMD_SET_VOICE_TIMING,
       &timing_args, sizeof( timing_args ),
       dplaudio_generic_basic_rsp_handler_cb );
     
     pDplDescData->eCvdState = eCvdStateConfigTiming;

   }
   break;
  case VSS_ICOMMON_CMD_SET_VOICE_TIMING:
    {
	    QP_AUDIO_DATA_AMSS* pAudioData = QP_NULL;
      if (eCvdStateConfigTiming != pDplDescData->eCvdState)
      {
        IMS_MSG_ERR_2(LOG_IMS_DPL,"Unexpected CMD message: %x in State: %x", rsp->opcode, pDplDescData->eCvdState );
        break;
      }
      if( (AUDIO_CODEC_G711A == pDplDescData->tCodecConfig.eCodec) || 
               (AUDIO_CODEC_G711U== pDplDescData->tCodecConfig.eCodec) )
      {
        pAudioData = (QP_AUDIO_DATA_AMSS*) pDplDescData->uDecriptors.Descriptor.pDesc;
	
        if (pAudioData && (eAudioStateConfiguring == pAudioData->iCurrentState))
        {
          vss_istream_cmd_set_enc_dtx_mode_t set_dtx;
          pDplDescData->eCvdState = eCvdStateConfigDtx;

          set_dtx.enable = 0;
          if(pDplDescData->tNewCodecConfig.bDTXEnable)
          {
          	set_dtx.enable = 1; /*Disable DTX*/
          }
          IMS_MSG_HIGH_1(LOG_IMS_DPL,"Setting DTX %d", set_dtx.enable );

          dplaudio_send_command( dplaudio_cvs_addr, dplaudio_app_cvs_port,
            0x12345678,
            VSS_ISTREAM_CMD_SET_ENC_DTX_MODE,
            &set_dtx, sizeof( set_dtx ),
            dplaudio_generic_basic_rsp_handler_cb );

        }
        else if(pAudioData && (eAudioStateReConfiguring == pAudioData->iCurrentState))
        {        

          dplaudio_send_command( dplaudio_mvm_addr, dplaudio_app_mvm_port,
            0x12345678,
            VSS_IMVM_CMD_MODEM_START_VOICE,
            QP_NULL, 0,
            dplaudio_generic_basic_rsp_handler_cb );

          pDplDescData->eCvdState   = eCvdStateStartStream;

        }
        else if(pAudioData && (eAudioStateReConfiguringCodeMode == pAudioData->iCurrentState))
        {
          IMS_MSG_MED_0(LOG_IMS_DPL,"Posting codec changed  started");
          pAudioData->tAudioCallBack(AUDIO_MSG_CODEC_CHANGED, QP_NULL, pAudioData->eADev, pAudioData->pUserData);
          if(pDplDescData->bIsaudio_started)
          {
            pDplDescData->eCvdState   = eCvdStateRun;
            pAudioData->iCurrentState = eAudioStateStarted;
          }
          else
          {
            pDplDescData->eCvdState = eCvdStateConfigured;
            pAudioData->iCurrentState = eAudioStateConfigured;
          }
          if(QP_TRUE == pDplDescData->bConfigPending)
          {
            IMS_MSG_LOW_0(LOG_IMS_DPL,"CodecSet is Pending Initiating the configuration now");
            /*rc =*/ dpl_cvd_standby_before_reconfig(pDplDescData);
            pDplDescData->bConfigPending = QP_FALSE;
          }
        }
      }
      else
      {
        if(dpl_cvd_set_codec(pDplDescData))
        {
          IMS_MSG_ERR_0(LOG_IMS_DPL, "failed to set the codec");
        }
      }
    }
    break;
  case VSS_ISTREAM_CMD_SET_EVS_ENC_CHANNEL_AWARE_MODE_ENABLE:
    {
      QP_EVS_CODEC_INFO *evs = &pDplDescData->tCodecConfig.sCodecInfo.sEVSInfo;
      IMS_MSG_ERR_0(LOG_IMS_DPL,"evs:rsp - VSS_ISTREAM_CMD_SET_EVS_ENC_CHANNEL_AWARE_MODE_ENABLE");
      if(eCvdStateConfigCHAwareMode != pDplDescData->eCvdState)
      {
        IMS_MSG_ERR_2(LOG_IMS_DPL,"Unexpected CMD message: %x in State: %x", rsp->opcode, pDplDescData->eCvdState );
        break;
      }
      qpDplSetEVSEncMode(pDplDescData,evs->eCodecMode.ePrmyMode,evs->eTxBW);
    }
    break;
  case VSS_ISTREAM_CMD_SET_EVS_VOC_ENC_OPERATING_MODE:
  case VSS_ISTREAM_CMD_VOC_AMRWB_SET_ENC_RATE:
  case VSS_ISTREAM_CMD_VOC_AMR_SET_ENC_RATE:
    {
      QP_AUDIO_DATA_AMSS* pAudioData = QP_NULL;
      if (eCvdStateConfigCodecProp != pDplDescData->eCvdState)
      {
        IMS_MSG_ERR_2(LOG_IMS_DPL,"Unexpected CMD message: %x in State: %x", rsp->opcode, pDplDescData->eCvdState );
        break;
      }
      pAudioData = (QP_AUDIO_DATA_AMSS*) pDplDescData->uDecriptors.Descriptor.pDesc;
      if (pAudioData && (eAudioStateConfiguring == pAudioData->iCurrentState ||
          eAudioStateReConfiguringCodeMode == pAudioData->iCurrentState)) 
      {
        vss_istream_cmd_set_enc_dtx_mode_t set_dtx;
        pDplDescData->eCvdState = eCvdStateConfigDtx;
        set_dtx.enable = 0;
        pDplDescData->tCodecConfig.bDTXEnable = pDplDescData->tNewCodecConfig.bDTXEnable;
        if (pDplDescData->tCodecConfig.bDTXEnable)
        {
          set_dtx.enable = 1;
          IMS_MSG_MED_0(LOG_IMS_DPL, "dplaudio_generic_basic_rsp_handler_cb | AMR DTX is Enabled");
        }
        IMS_MSG_HIGH_1(LOG_IMS_DPL,"Setting DTX %d", set_dtx.enable );
        dplaudio_send_command( dplaudio_cvs_addr, dplaudio_app_cvs_port,
          0x12345678,
          VSS_ISTREAM_CMD_SET_ENC_DTX_MODE,
          &set_dtx, sizeof( set_dtx ),
          dplaudio_generic_basic_rsp_handler_cb );

      }
      else if(pAudioData && (eAudioStateReConfiguring == pAudioData->iCurrentState))
      {        
        dplaudio_set_pktExchng_Mode(pDplDescData,TRUE);
      }
      else if(pAudioData && (eAudioStateReConfiguringCodeMode == pAudioData->iCurrentState))
      {
        IMS_MSG_MED_0(LOG_IMS_DPL,"Posting codec changed  started");
        if(pDplDescData->bIsaudio_started)
        {
          pDplDescData->eCvdState   = eCvdStateRun;
          pAudioData->iCurrentState = eAudioStateStarted;
        }
        else
        {
          pDplDescData->eCvdState = eCvdStateConfigured;
          pAudioData->iCurrentState = eAudioStateConfigured;
        }
        pAudioData->tAudioCallBack(AUDIO_MSG_CODEC_CHANGED, QP_NULL, pAudioData->eADev, pAudioData->pUserData);

        if(QP_TRUE == pDplDescData->bConfigPending)//CHECK WITH VIKRAM 
        {
          IMS_MSG_LOW_0(LOG_IMS_DPL,"CodecSet is Pending Initiating the configuration now");
          /*rc =*/ dpl_cvd_standby_before_reconfig(pDplDescData);
          pDplDescData->bConfigPending = QP_FALSE;
        }			  
      }
      else
      {
        IMS_MSG_ERR_1(LOG_IMS_DPL,"AudioData is NULL or wrong state %x", pAudioData);
      }
    }
    break;
  case VSS_ISTREAM_CMD_SET_ENC_DTX_MODE:
    {
      QP_AUDIO_DATA_AMSS* pAudioData = QP_NULL;
      if (eCvdStateConfigDtx != pDplDescData->eCvdState)
      {
        IMS_MSG_ERR_2(LOG_IMS_DPL,"Unexpected CMD message: %x in State: %x", rsp->opcode, pDplDescData->eCvdState );
        break;
      }

      pAudioData = (QP_AUDIO_DATA_AMSS*) pDplDescData->uDecriptors.Descriptor.pDesc;
      if (pAudioData && (eAudioStateConfiguring == pAudioData->iCurrentState))
      {
        pDplDescData->eCvdState = eCvdStateConfigured;
        pAudioData->iCurrentState = eAudioStateConfigured;

        if (pAudioData->tAudioCallBack)
        {
          IMS_MSG_HIGH_0(LOG_IMS_DPL,"Posting Codec Changed");
          pAudioData->tAudioCallBack(AUDIO_MSG_CODEC_CHANGED, QP_NULL, pAudioData->eADev, pAudioData->pUserData);
          if(QP_TRUE == pDplDescData->bConfigPending)//CHECK WITH VIKRAM 
          {
            IMS_MSG_LOW_0(LOG_IMS_DPL,"CodecSet is Pending Initiating the configuration now");
            /*rc = */dpl_cvd_standby_before_reconfig(pDplDescData);
            pDplDescData->bConfigPending = QP_FALSE;
          }				
        }
        else
        {
          IMS_MSG_ERR_0(LOG_IMS_DPL,"Audio is CONFIGURED but failed to raise CB");
        }
      }
      else if(pAudioData && (eAudioStateReConfiguring == pAudioData->iCurrentState))
      {        

        dplaudio_set_pktExchng_Mode(pDplDescData,TRUE);

      }
      else if (pAudioData && (eAudioStateReConfiguringCodeMode == pAudioData->iCurrentState))
      {
        IMS_MSG_HIGH_0(LOG_IMS_DPL, "dtx changed in reconfiguring mode started");
        if (pDplDescData->bIsaudio_started)
        {
          pDplDescData->eCvdState   = eCvdStateRun;
          pAudioData->iCurrentState = eAudioStateStarted;
        }
        else
        {
          pDplDescData->eCvdState = eCvdStateConfigured;
          pAudioData->iCurrentState = eAudioStateConfigured;
        }
        pAudioData->tAudioCallBack(AUDIO_MSG_CODEC_CHANGED, QP_NULL, pAudioData->eADev, pAudioData->pUserData);

        if (QP_TRUE == pDplDescData->bConfigPending) 
        {
          IMS_MSG_LOW_0(LOG_IMS_DPL, "CodecSet is Pending Initiating the configuration now");
          dpl_cvd_standby_before_reconfig(pDplDescData);
          pDplDescData->bConfigPending = QP_FALSE;
        }	
      }
      else
      {
        IMS_MSG_ERR_1(LOG_IMS_DPL,"AudioData is NULL or wrong state %x", pAudioData);
      }

    }
    break;
  case VSS_IMVM_CMD_MODEM_STANDBY_VOICE:
    {
      QP_AUDIO_DATA_AMSS* pAudioData = QP_NULL;
      pAudioData = (QP_AUDIO_DATA_AMSS*) pDplDescData->uDecriptors.Descriptor.pDesc;
      if(NULL == pAudioData)
      {
        IMS_MSG_ERR_0(LOG_IMS_DPL,"audio data is null");
        break;
      }
      if((eAudioStateIniting == pAudioData->iCurrentState) && (eCvdStateStandby == pDplDescData->eCvdState))
      {
         dplaudio_map_outofband_memory(pDplDescData);
      }
      else if ((eCvdStateStandby == pDplDescData->eCvdState)&& (eAudioStateReConfiguring == pAudioData->iCurrentState)) 
      {
        dpl_cvd_reconfigure(pDplDescData); 
      }

      else
      {
        IMS_MSG_ERR_2(LOG_IMS_DPL,"Wrong Sate: CVD State: %x Aud State: %x", pDplDescData->eCvdState, pAudioData->iCurrentState );
      }
    }
    break;
  case VSS_IMVM_CMD_MODEM_START_VOICE:
    {
      QP_AUDIO_DATA_AMSS* pAudioData = QP_NULL;
      if (eCvdStateStartStream != pDplDescData->eCvdState)
      {
        IMS_MSG_ERR_2(LOG_IMS_DPL,"Unexpected CMD message: %x in State: %x", rsp->opcode, pDplDescData->eCvdState );
        break;
      }
      pAudioData = (QP_AUDIO_DATA_AMSS*) pDplDescData->uDecriptors.Descriptor.pDesc;
      if (pAudioData && ((eAudioStateStarting == pAudioData->iCurrentState) || (eAudioStateReConfiguring == pAudioData->iCurrentState)))
      {

        if (pAudioData->tAudioCallBack && eAudioULDLMode == pDplDescData->eMode)
        {
          QPE_AUDIO_MSG msg = AUDIO_MSG_CODEC_CHANGED;

          if(eAudioStateStarting == pAudioData->iCurrentState)
          {
            msg = AUDIO_MSG_STREAMING_STARTED;
          }
          pDplDescData->eCvdState = eCvdStateRun;
          pAudioData->iCurrentState = eAudioStateStarted;
          pDplDescData->bIsaudio_started = QP_TRUE;                  
          pAudioData->tAudioCallBack(msg, QP_NULL, pAudioData->eADev, pAudioData->pUserData);

          //TBD: We need to add code for other mode like UL or DL

          if(QP_TRUE == pDplDescData->bConfigPending)//CHECK WITH VIKRAM 
          {
            IMS_MSG_LOW_0(LOG_IMS_DPL,"CodecSet is Pending Initiating the configuration now");
            /*rc =*/ dpl_cvd_standby_before_reconfig(pDplDescData);
            pDplDescData->bConfigPending = QP_FALSE;
          }
        }
        else
        {
          IMS_MSG_ERR_0(LOG_IMS_DPL,"Streaming started but CB is NULL");
        }
      }
      else
      { 
        if (QP_NULL == pAudioData)
        {
          IMS_MSG_ERR_0(LOG_IMS_DPL,"AudioData is NULL");
        }
        else
        {
          IMS_MSG_ERR_2(LOG_IMS_DPL,"Wrong State: CVD State: %x  Aud State: %x", pDplDescData->eCvdState, pAudioData->iCurrentState );
        }
      }
    }
    break;
  case VSS_IMVM_CMD_MODEM_STOP_VOICE:
    {
      QP_AUDIO_DATA_AMSS* pAudioData = QP_NULL;
      qpAudio_tty_deregister(pDplDescData);
      if (eCvdStateStopStream != pDplDescData->eCvdState)
      {
        IMS_MSG_ERR_2(LOG_IMS_DPL,"Unexpected CMD message: %x in State: %x", rsp->opcode, pDplDescData->eCvdState );
        break;
      }
      pAudioData = (QP_AUDIO_DATA_AMSS*) pDplDescData->uDecriptors.Descriptor.pDesc;
      if(NULL == pAudioData)
      {
        IMS_MSG_ERR_0(LOG_IMS_DPL,"AudioData is NULL");
        break;
      }
      if (eAudioStateStopping == pAudioData->iCurrentState)
      {
        pDplDescData->eCvdState = eCvdStateConfigured;
        pAudioData->iCurrentState = eAudioStateConfigured;
        pDplDescData->bIsaudio_started = QP_FALSE;
        //TBD: Handle the case where qpDplAudioCodecSet() is called once again and new configure has to happen

        if (pAudioData->tAudioCallBack)
        {
          IMS_MSG_HIGH_0(LOG_IMS_DPL,"Posting streaming stopped");
          pAudioData->tAudioCallBack(AUDIO_MSG_STREAMING_STOPPED, QP_NULL, pAudioData->eADev, pAudioData->pUserData);
        }
        else
        {
          IMS_MSG_ERR_0(LOG_IMS_DPL,"Audio is STOPPED but failed to raise CB");
        }
      }
    }
    break;

  case VSS_INOTIFY_CMD_CANCEL_EVENT_CLASS :
   {
	  
	  if (rsp->status == APR_EOK)
	  {
        IMS_MSG_HIGH_0(LOG_IMS_DPL,"received APR_EOK in response to VSS_INOTIFY_CMD_CANCEL_EVENT_CLASS");

	  }
	  else
	  {
		  /*log error message */
      IMS_MSG_ERR_1(LOG_IMS_DPL,"ERROR: response code for VSS_INOTIFY_CMD_CANCEL_EVENT_CLASS was not APR_EOK but %d",rsp->status);
	  }

    }
    break;  

  case VSS_IMVM_CMD_DETACH_STREAM:
    {
      QP_AUDIO_DATA_AMSS* pAudioData = QP_NULL;

      pAudioData = (QP_AUDIO_DATA_AMSS*) pDplDescData->uDecriptors.Descriptor.pDesc;
      if (QP_NULL == pAudioData)
      {
        IMS_MSG_ERR_0(LOG_IMS_DPL,"pAudioData is NULL");
        break;
      }
      if ((eCvdStateDeInitDetachStream == pDplDescData->eCvdState) && (eAudioStateReleasing == pAudioData->iCurrentState))
      {
        /* Release the CVS instance. */
        dplaudio_send_command( dplaudio_cvs_addr, dplaudio_app_cvs_port,
          0x12345678,
          APRV2_IBASIC_CMD_DESTROY_SESSION,
          QP_NULL, 0,
          dplaudio_generic_basic_rsp_handler_cb );

        pDplDescData->eCvdState   = eCvdStateDeInitCVSDestroy;
      }
      else
      {
        IMS_MSG_ERR_2(LOG_IMS_DPL,"Wrong Sate: CVD State: %x Aud State: %x", pDplDescData->eCvdState, pAudioData->iCurrentState );
      }
    }
    break;
  case APRV2_IBASIC_CMD_DESTROY_SESSION:
    {
      QP_AUDIO_DATA_AMSS* pAudioData = QP_NULL;
      pAudioData = (QP_AUDIO_DATA_AMSS*) pDplDescData->uDecriptors.Descriptor.pDesc;
      if (QP_NULL == pAudioData)
      {
        IMS_MSG_ERR_0(LOG_IMS_DPL,"pAudioData is NULL");
        break;
      }
      if ((eCvdStateDeInitCVSDestroy == pDplDescData->eCvdState) && (eAudioStateReleasing == pAudioData->iCurrentState))
      {
        dplaudio_app_cvs_port = APR_NULL_V;

        /* Release the CVS instance. */
        dplaudio_send_command( dplaudio_mvm_addr, dplaudio_app_mvm_port,
          0x12345678,
          APRV2_IBASIC_CMD_DESTROY_SESSION,
          QP_NULL, 0,
          dplaudio_generic_basic_rsp_handler_cb );

        pDplDescData->eCvdState   = eCvdStateDeInitMVMDestroy;
      }
      else if ((eCvdStateDeInitMVMDestroy == pDplDescData->eCvdState) && (eAudioStateReleasing == pAudioData->iCurrentState))
      {
        dplaudio_app_mvm_port = APR_NULL_V;
      }
      else
      {
        IMS_MSG_ERR_2(LOG_IMS_DPL,"Wrong Sate: CVD State: %x Aud State: %x", pDplDescData->eCvdState, pAudioData->iCurrentState );
      }
    }
    break;
  case VSS_ISTREAM_CMD_SET_DEC_TIMEWARP:
    {
      if(APRV2_IBASIC_RSP_RESULT  != opcode)
        IMS_MSG_HIGH_0(LOG_IMS_DPL,"Time warping failure");
    }
    break;
  case VSS_ITTYOOB_CMD_REGISTER:
	  {
      IMS_MSG_MED_0(LOG_IMS_DPL,"Received response for VSS_ITTYOOB_CMD_REGISTER");
      dplaudio_set_pktExchng_Mode(pDplDescData,TRUE);
	  }
	break;
  case VSS_ITTYOOB_CMD_DEREGISTER:
	  {
      QP_AUDIO_DATA_AMSS* pAudioData = QP_NULL;
      IMS_MSG_MED_0(LOG_IMS_DPL,"Received response for VSS_ITTYOOB_CMD_DEREGISTER");
      
      if (eCvdStateStopStream != pDplDescData->eCvdState)
      {
        IMS_MSG_ERR_2(LOG_IMS_DPL,"Unexpected CMD message: %x in State: %x", rsp->opcode, pDplDescData->eCvdState );
      }
  
      pAudioData = (QP_AUDIO_DATA_AMSS*) pDplDescData->uDecriptors.Descriptor.pDesc;
      if (pAudioData && (eAudioStateStopping == pAudioData->iCurrentState))
      {
        pDplDescData->eCvdState = eCvdStateConfigured;
        pAudioData->iCurrentState = eAudioStateConfigured;

        pDplDescData->bIsaudio_started = QP_FALSE;

        //TBD: Handle the case where qpDplAudioCodecSet() is called once again and new configure has to happen

        if (pAudioData->tAudioCallBack)
        {
          IMS_MSG_MED_0(LOG_IMS_DPL,"Posting streaming stopped");
          pAudioData->tAudioCallBack(AUDIO_MSG_STREAMING_STOPPED, QP_NULL, pAudioData->eADev, pAudioData->pUserData);
        }
        else
        {
          IMS_MSG_ERR_0(LOG_IMS_DPL,"Audio is STOPPED but failed to raise CB");
        }
      }
      else
      {
        IMS_MSG_ERR_0(LOG_IMS_DPL,"AudioData is NULL");
      }
    }
    break;
  case VSS_ITTYOOB_CMD_SEND_RX_CHAR:
  {
    IMS_MSG_MED_0(LOG_IMS_DPL,"Received response for VSS_ITTYOOB_CMD_SEND_RX_CHAR");
	  if (rsp->status == APR_EOK || rsp->status == APR_ENOTREADY)
	  {
	    dpl_cvd_state = TRUE;
	    dplaudio_update_tty_state(dpl_cvd_state);
        IMS_MSG_HIGH_1(LOG_IMS_DPL,"received %d in response to VSS_ITTYOOB_CMD_SEND_RX_CHAR",rsp->status);
	  }
	  else
	  {
		  /*log error message */
      IMS_MSG_ERR_1(LOG_IMS_DPL,"ERROR: response code for VSS_ITTYOOB_CMD_SEND_RX_CHAR was not APR_EOK but %d",rsp->status);
	  }
  }
	  break;
  case VSS_ISTREAM_CMD_SET_PACKET_EXCHANGE_MODE:
   {
     QP_AUDIO_DATA_AMSS* pAudioData = (QP_AUDIO_DATA_AMSS*) pDplDescData->uDecriptors.Descriptor.pDesc;		
     if(pAudioData && pAudioData->iCurrentState == eAudioStateReleasing)
     {
       vss_imemory_cmd_unmap_t physical_unmapping_mem_args;
       physical_unmapping_mem_args.mem_handle = pDplDescData->mapped_mem_handle;

       dplaudio_send_command( dplaudio_mvm_addr, dplaudio_app_mvm_port,
        0x12345678,
      	VSS_IMEMORY_CMD_UNMAP,
      	&physical_unmapping_mem_args, sizeof( vss_imemory_cmd_unmap_t ),
        dplaudio_generic_basic_rsp_handler_cb );		  

     }
     else
     {
       vss_istream_cmd_set_oob_packet_exchange_config_t config;
       IMS_MSG_MED_0(LOG_IMS_DPL,"Received response for VSS_ISTREAM_CMD_SET_PACKET_EXCHANGE_MODE");
       
       config.mem_handle = pDplDescData->mapped_mem_handle;
       config.dec_buf_size = 2048;
       config.enc_buf_size = 2048;
       config.enc_buf_addr = pDplDescData->mapped_phys_addr + QPDL_AUDIO_ENC_OFFSET; 		  
       config.dec_buf_addr = pDplDescData->mapped_phys_addr + QPDL_AUDIO_DEC_OFFSET;		  
       if (APR_EOK == rsp->status)
       {
         dplaudio_set_enc_dec_buffer_config(&config);
       }
     }
   }
   break;
  case VSS_ISTREAM_CMD_SET_OOB_PACKET_EXCHANGE_CONFIG:
    {
      QP_AUDIO_DATA_AMSS* pAudioData = QP_NULL;
      IMS_MSG_MED_0(LOG_IMS_DPL,"Received response for VSS_ISTREAM_CMD_SET_OOB_PACKET_EXCHANGE_CONFIG");

      pAudioData = (QP_AUDIO_DATA_AMSS*)pDplDescData->uDecriptors.Descriptor.pDesc;
      if(pAudioData && (eAudioStateReConfiguring == pAudioData->iCurrentState))
      {        

        dplaudio_send_command( dplaudio_mvm_addr, dplaudio_app_mvm_port,
          0x12345678,
          VSS_IMVM_CMD_MODEM_START_VOICE,
          QP_NULL, 0,
          dplaudio_generic_basic_rsp_handler_cb );

        pDplDescData->eCvdState   = eCvdStateStartStream;

        IMS_MSG_ERR_0(LOG_IMS_DPL,"posting command VSS_IMVM_CMD_MODEM_START_VOICE");
      }
      else
      {
        qpAudioStart_voice(pDplDescData);
      }
    }
    break;
  case VSS_IAVTIMER_CMD_GET_TIME:
	  {
		  IMS_MSG_HIGH_0(LOG_IMS_DPL,"received response to VSS_IAVTIMER_CMD_GET_TIME");
	  }
	  break;
  default:
    IMS_MSG_LOW_1(LOG_IMS_DPL,"Unexpected CMD message: %x", rsp->opcode);
    break;
  }
}

static void dplaudio_custom_app_full_mvm_rsp_handler_cb (
  uint16_t my_port,
  uint16_t server_addr,
  uint16_t server_port,
  uint32_t token,
  uint32_t opcode,
  void* payload,
  uint32_t payload_size
  )
{
  aprv2_ibasic_rsp_result_t* rsp = ( ( aprv2_ibasic_rsp_result_t* ) payload );

  if(QP_NULL == rsp)
  {
    IMS_MSG_ERR_0(LOG_IMS_DPL,"Response is NULL ");
    return; // check with vikram 
  }

  IMS_MSG_MED_1(LOG_IMS_DPL,"Processing the response %x ", rsp->opcode);

  for ( ;; )
  {
    /* Verify that the opcode is expected. */
    if ( opcode != APRV2_IBASIC_RSP_RESULT )
    {
      IMS_MSG_ERR_0(LOG_IMS_DPL,"Unexpected command response type");
      break;
    }

    /* Verify that the payload size is expected. */
    if ( payload_size != sizeof( aprv2_ibasic_rsp_result_t ) )
    {
      IMS_MSG_ERR_0(LOG_IMS_DPL,"Unexpected command response payload size");
      break;
    }

    switch ( rsp->opcode )
    {
    case VSS_IMVM_CMD_CREATE_FULL_CONTROL_SESSION:
      {
        QPC_GLOBAL_DATA* pGlobalData = QP_NULL;
        uint8  apr_msg_len = 0;
        dplaudio_app_mvm_port = server_port;
        pGlobalData = qpDplGetGlobalData();

        if (pGlobalData)
        {
          qpDplDecriptorData *pDplDescData = QP_NULL;

          pDplDescData = (qpDplDecriptorData *)pGlobalData->pAudioData;
          if (pDplDescData)
          {
            if (eCvdStateInitMVMCtrlSession == pDplDescData->eCvdState)
            {
              
              vss_istream_cmd_create_full_control_session_t session;
			  uint8* stream_args = QP_NULL;

			  
			  stream_args = qpDplMalloc(MEM_IMS_DPL, (sizeof(vss_istream_cmd_create_full_control_session_t) + ( sizeof( char_t ) * DPLAUDIO_MAX_SESSION_NAME_SIZE )));

			  if(QP_NULL == stream_args)
			  {
			    IMS_MSG_ERR_0(LOG_IMS_DPL,"Not able to allocate stream_args");
			    return; 
			  }		  

              pDplDescData->eCvdState = eCvdStateInitStreamSession;

              { /* Enable modem voice control. */
                session.direction = 2; /* Select Rx + Tx. */
                session.enc_media_type = VSS_MEDIA_ID_AMR_NB_MODEM;
                session.dec_media_type = VSS_MEDIA_ID_AMR_NB_MODEM;
                session.network_id = VSS_NETWORK_ID_DEFAULT;

				qpDplMemscpy( stream_args, (sizeof(vss_istream_cmd_create_full_control_session_t) + ( sizeof( char_t ) * DPLAUDIO_MAX_SESSION_NAME_SIZE )), &session, sizeof( vss_istream_cmd_create_full_control_session_t) );
				
				if(g_rat_type == AUDIO_RAT_IWLAN)
				{
					qpDplMemscpy( stream_args + sizeof(vss_istream_cmd_create_full_control_session_t), ( sizeof( char_t ) *DPLAUDIO_MAX_SESSION_NAME_SIZE ), dplaudio_cvs_session_name_wlan, sizeof( dplaudio_cvs_session_name_wlan) );
					apr_msg_len = sizeof(vss_istream_cmd_create_full_control_session_t) +  sizeof(dplaudio_cvs_session_name_wlan);
				}
				else
				{
                	qpDplMemscpy( stream_args + sizeof(vss_istream_cmd_create_full_control_session_t), ( sizeof( char_t ) *DPLAUDIO_MAX_SESSION_NAME_SIZE ), dplaudio_cvs_session_name, sizeof( dplaudio_cvs_session_name ) );
                	apr_msg_len = sizeof(vss_istream_cmd_create_full_control_session_t) + sizeof(dplaudio_cvs_session_name);
				}

                IMS_MSG_MED_1(LOG_IMS_DPL,"apr_msg_len %u ", apr_msg_len);

                dplaudio_send_command( dplaudio_cvs_addr, APR_NULL_V,
                  0x12345678,
                  VSS_ISTREAM_CMD_CREATE_FULL_CONTROL_SESSION,
                  stream_args, apr_msg_len,
                  dplaudio_custom_app_full_cvs_rsp_handler_cb );
                qpDplFree(MEM_IMS_DPL, stream_args);
              }
            }
            else
            {
              IMS_MSG_ERR_2(LOG_IMS_DPL,"Unexpected MVM CMD message: %x in State: %x", rsp->opcode, pDplDescData->eCvdState );
            }
          }
          else
          {
            IMS_MSG_ERR_0(LOG_IMS_DPL,"AudioDesc in GlobalData is NULL");
          }
        }
        else
        {
          IMS_MSG_ERR_0(LOG_IMS_DPL,"GlobalData is NULL");
        }
      }
      break;

    default:
      IMS_MSG_ERR_1(LOG_IMS_DPL,"Unexpected MVM CMD message: %x", rsp->opcode);
      break;
    }

    break;
  }
}

static void dplaudio_custom_app_full_cvs_rsp_handler_cb (
  uint16_t my_port,
  uint16_t server_addr,
  uint16_t server_port,
  uint32_t token,
  uint32_t opcode,
  void* payload,
  uint32_t payload_size
  )
{
  aprv2_ibasic_rsp_result_t* rsp = ( ( aprv2_ibasic_rsp_result_t* ) payload );

  if(QP_NULL == rsp)
  {
    IMS_MSG_ERR_0(LOG_IMS_DPL,"Response is NULL ");
    return ;// Check with vikram  
  }

  IMS_MSG_MED_1(LOG_IMS_DPL,"Processing the response %x ", rsp->opcode);

  for ( ;; )
  {
    /* Verify that the opcode is expected. */
    if ( opcode != APRV2_IBASIC_RSP_RESULT )
    {
      IMS_MSG_ERR_1(LOG_IMS_DPL,"Unexpected command response type %x", opcode);
      break;
    }

    /* Verify that the payload size is expected. */
    if ( payload_size != sizeof( aprv2_ibasic_rsp_result_t ) )
    {
      IMS_MSG_ERR_0(LOG_IMS_DPL,"Unexpected command response payload size");
      break;
    }

    switch ( rsp->opcode )
    {
    case VSS_ISTREAM_CMD_CREATE_FULL_CONTROL_SESSION:
      { 
        QPC_GLOBAL_DATA* pGlobalData = QP_NULL;
        pGlobalData = qpDplGetGlobalData();

        if (pGlobalData)
        {
          qpDplDecriptorData *pDplDescData = QP_NULL;

          pDplDescData = (qpDplDecriptorData *)pGlobalData->pAudioData;
          if (pDplDescData)
          {
            if (eCvdStateInitStreamSession == pDplDescData->eCvdState)
            {
              dplaudio_app_cvs_port = server_port;
              pDplDescData->eCvdState = eCvdStateInitAttachStream;

              { /* Enable modem voice control. */
                dplaudio_send_command( dplaudio_mvm_addr, dplaudio_app_mvm_port,
                  0x12345678,
                  VSS_IMVM_CMD_ATTACH_STREAM,
                  &dplaudio_app_cvs_port, sizeof( dplaudio_app_cvs_port ),
                  dplaudio_generic_basic_rsp_handler_cb );
              }
            }
            else
            {
              IMS_MSG_ERR_2(LOG_IMS_DPL,"Unexpected CVS CMD message: %x in State: %x", rsp->opcode, pDplDescData->eCvdState );
            }
          }
          else
          {
            IMS_MSG_ERR_0(LOG_IMS_DPL,"AudioDesc in GlobalData is NULL");
          }
        }
        else
        {
          IMS_MSG_ERR_0(LOG_IMS_DPL,"GlobalData is NULL");
        }
      }
      break;

    default:
      IMS_MSG_ERR_1(LOG_IMS_DPL,"Unexpected APR message: %x", rsp->opcode);
      break;
    }

    break;
  }
}

static void dplaudio_process_rsp_msg (
                                      aprv2_packet_t* packet
                                      )
{
  int32_t rc;
  apr_objmgr_object_t* obj = NULL;
  aprv2_ibasic_rsp_result_t* rsp;
  void* payload = QP_NULL;
  QPBOOL bis_clean_up = QP_FALSE;
  uint16_t tmp_app_mvm_port = dplaudio_app_mvm_port;

  rc = apr_objmgr_find_object( &dplaudio_objmgr, packet->token, &obj );
  DPLAUDIO_PANIC_ON_ERROR( rc );

  payload = APRV2_PKT_GET_PAYLOAD( void, packet );

  if ( obj != QP_NULL && obj->any.ptr != QP_NULL )
  {
    ( ( dplaudio_rsp_handler_cb_t ) obj->any.ptr )(
      packet->dst_port,
      packet->src_addr,
      packet->src_port,
      obj->type,
      packet->opcode,
      payload,
      APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( packet->header ) );
  }

  if(obj != QP_NULL && obj->handle != QP_NULL)
  apr_objmgr_free_object( &dplaudio_objmgr, obj->handle );

  rsp = ( ( aprv2_ibasic_rsp_result_t* ) payload );


  /* Verify that the opcode is expected. */
  if ( packet->opcode == APRV2_IBASIC_RSP_RESULT )
  {
    if ((APRV2_IBASIC_CMD_DESTROY_SESSION == rsp->opcode) && (packet->src_port == tmp_app_mvm_port))
    {
      if (dplaudio_app_mvm_port == APR_NULL_V)
      {
        IMS_MSG_LOW_0(LOG_IMS_DPL,"clean up time");
        bis_clean_up = QP_TRUE;
      }	
    }
  }

  __aprv2_cmd_free( dplaudio_apr_handle, packet );

  if(bis_clean_up)
  {
    QPC_GLOBAL_DATA* pGlobalData  = QP_NULL;
    pGlobalData = qpDplGetGlobalData();
    if (pGlobalData)
    {
      QP_AUDIO_DATA_AMSS* pAudioData = QP_NULL;
      qpDplDecriptorData *pDplDescData = QP_NULL;

      pDplDescData = (qpDplDecriptorData *)pGlobalData->pAudioData;
      if (pDplDescData)
      {	
        pAudioData = (QP_AUDIO_DATA_AMSS*) pDplDescData->uDecriptors.Descriptor.pDesc;
        if (pAudioData && (eCvdStateDeInitMVMDestroy == pDplDescData->eCvdState) && (eAudioStateReleasing == pAudioData->iCurrentState))
        {
          if (pAudioData->tAudioCallBack)
          { 
            pAudioData->tAudioCallBack(AUDIO_MSG_DEV_UNINITIALISED, QP_NULL, pAudioData->eADev, pAudioData->pUserData);
            IMS_MSG_HIGH_0(LOG_IMS_DPL,"Posting Audio Un-Initialized");

            //dpl_aud_cleanup(pDplDescData);

          }
          else
          {
            IMS_MSG_FATAL_0(LOG_IMS_DPL,"Audio is UNINITIALISED but failed to raise CB");
          }
		      IMS_MSG_HIGH_3(LOG_IMS_DPL, "Final count g_cmd_sent=%d g_cmd_rsp=%d g_cmd_ack=%d", g_cmd_sent, g_cmd_rsp, g_cmd_ack);
			    if(g_apr_assert)
			    {
			      IMS_MSG_HIGH_0(LOG_IMS_DPL, " Asserting is enabled check and assert");
		        ASSERT(g_cmd_sent == g_cmd_rsp);
            ASSERT(g_cmd_sent == g_cmd_ack);
			    }
          IMS_MSG_LOW_0(LOG_IMS_DPL,"calling dpl_aud_cleanup");
          dpl_aud_cleanup(pDplDescData);
        }
      }
    }
        /* Deinitialize the handle table. */
        apr_objmgr_destruct( &dplaudio_objmgr );

        apr_lock_destroy( dplaudio_lock );

		qpDplPostEventToEventQueue(QPCMSG_DPL_AUDIO_MSG, QP_NULL,
			  (QPVOID*)qvp_rtp_execute_pending_handoff, pGlobalData);		
      }
    }

static void dplaudio_process_work_queue (
  apr_list_t* used_q,
  apr_list_t* free_q
  )
{
  int32_t rc;
  dplaudio_work_item_t* item = QP_NULL;
  aprv2_packet_t* packet;
  IMS_MSG_LOW_0(LOG_IMS_DPL,"Entering dplaudio_process_work_queue");
  for ( ;; )
  {
    rc = apr_list_remove_head( used_q, ( ( apr_list_node_t** ) &item ) );
    if ( rc ) break;

    if(item == QP_NULL)
      break; 

    packet = item->packet;

    if ( APR_GET_FIELD( APRV2_PKT_MSGTYPE, packet->header ) == APRV2_PKT_MSGTYPE_CMDRSP_V )
    {
	    g_cmd_rsp++; 
	    IMS_MSG_HIGH_3(LOG_IMS_DPL, "triggering3 ? : g_cmd_sent = %d g_cmd_rsp = %d g_cmd_ack =%d", g_cmd_sent, g_cmd_rsp, g_cmd_ack); 
		  if(g_cmd_sent == g_cmd_rsp)
		  {
        (void)rex_clr_timer(&cvd_timer_handle);
        g_eCVDState = eCvdStateNULL;
  	  }
      dplaudio_process_rsp_msg( packet );
    }
    else
    {
      switch ( packet->opcode )
      {
      case APRV2_IBASIC_EVT_ACCEPTED:
        __aprv2_cmd_free( dplaudio_apr_handle, packet );
        break;

      case VSS_ISTREAM_EVT_OOB_NOTIFY_ENC_BUFFER_READY:
        g_uplink_pkt_count++;
        IMS_MSG_MED_1(LOG_IMS_DPL,"g_uplink_pkt_count %d",g_uplink_pkt_count);
        if(used_q->dummy.next != &used_q->dummy)
        {
          dplaudio_work_item_t* item_2 = QP_NULL;

          rc = apr_list_remove_head( used_q, ( ( apr_list_node_t** ) &item_2 ) );
          if(rc)
          {
            IMS_MSG_FATAL_0(LOG_IMS_DPL,"Queue is non zero and Empty..looks wrong");
          }
          else
          {
            //IMS_MSG_HIGH_0(LOG_IMS_DPL,"One More Command in the Queue");
            aprv2_packet_t* packet_2;
            packet_2 = item_2->packet;
            if(packet_2->opcode == VSS_ISTREAM_EVT_OOB_NOTIFY_DEC_BUFFER_REQUEST)
            {
			
              IMS_MSG_HIGH_0(LOG_IMS_DPL,"Prioritizing EVT_REQUEST_DEC");
              dplaudio_process_request_dec_buffer_oob_evt_msg( packet_2);
              apr_list_add_tail( free_q, &item_2->link );
			  IMS_MSG_HIGH_0(LOG_IMS_DPL,"free_q  adding");
			  
            }
            else
            {      
			  //IMS_MSG_HIGH_1(LOG_IMS_DPL,"packet_2->opcode %d",packet_2->opcode);
              apr_list_add_tail( used_q, &item_2->link );
            } /*if(packet_2->opcode == VSS_ISTREAM_EVT_OOB_NOTIFY_DEC_BUFFER_REQUEST)*/
          }/*if(rc)*/
        }/*if(used_q->dummy.next != &used_q->dummy)*/
		
        dplaudio_process_send_enc_buffer_oob_evt_msg(packet);			  
		if(g_iAVSyncTxDelay)
		{
			g_iAVsyncTime = g_iTimeInTxPkt + g_iAVSyncTxDelay;
			g_iAVmSysTime = item->systime;
			IMS_MSG_MED_1(LOG_IMS_DPL,"debug: current AV time:%lu",g_iAVsyncTime);
		}
        break;
      case VSS_ISTREAM_EVT_OOB_NOTIFY_DEC_BUFFER_REQUEST:
        dplaudio_process_request_dec_buffer_oob_evt_msg(packet);
        break;
      case VSS_ISTREAM_EVT_SEND_ENC_BUFFER:
                if(used_q->dummy.next != &used_q->dummy)
        {
		
          dplaudio_work_item_t* item_2 = QP_NULL;
          rc = apr_list_remove_head( used_q, ( ( apr_list_node_t** ) &item_2 ) );
          if(rc)
          {
            IMS_MSG_FATAL_0(LOG_IMS_DPL,"Queue is non zero and Empty..looks wrong");
          }
          else
          {
            //IMS_MSG_HIGH_0(LOG_IMS_DPL,"One More Command in the Queue");
            aprv2_packet_t* packet_2;
            packet_2 = item_2->packet;
            if(packet_2->opcode == VSS_ISTREAM_EVT_REQUEST_DEC_BUFFER)
            {
			
              IMS_MSG_HIGH_0(LOG_IMS_DPL,"Prioritizing EVT_REQUEST_DEC");
              dplaudio_process_request_dec_buffer_evt_msg( packet_2);
              apr_list_add_tail( free_q, &item_2->link );
			  //IMS_MSG_HIGH_0(LOG_IMS_DPL,"free_q  adding");
			  
            }
            else
            {      
			  //IMS_MSG_HIGH_1(LOG_IMS_DPL,"packet_2->opcode %d",packet_2->opcode);
              apr_list_add_tail( used_q, &item_2->link );
            } 
          }
		}
        dplaudio_process_send_enc_buffer_evt_msg( packet );
        break;

      case VSS_ISTREAM_EVT_REQUEST_DEC_BUFFER:
        dplaudio_process_request_dec_buffer_evt_msg( packet );
        break;

      case VSS_ITTYOOB_EVT_SEND_TX_CHAR:
        IMS_MSG_MED_0(LOG_IMS_DPL,"Received VSS_ITTYOOB_EVT_SEND_TX_CHAR");
	    dplaudio_process_tty_tx( packet);
		break;
	  case VSS_IAVSYNC_EVT_RX_PATH_DELAY: 
		IMS_MSG_HIGH_0(LOG_IMS_DPL,"Received VSS_IAVSYNC_EVT_RX_PATH_DELAY");
	  	dplaudio_process_rx_path_delay_evt_msg( packet );
	    break;

      case VSS_ISTREAM_EVT_READY:
        IMS_MSG_HIGH_0(LOG_IMS_DPL,"Received VSS_ISTREAM_EVT_READY setting qvp_rtp_cvd_state to READY");

			if(cvd_event_not_ready)
			{
			 IMS_MSG_MED_0(LOG_IMS_DPL,"cvd event is VSS_ISTREAM_EVT_READY before clearing the timer");
   		         (void)rex_clr_timer(&rex_timer_handle);
				cvd_event_not_ready =0;
			}
    	g_updateAVdelay = TRUE;
		
        dpl_cvd_state = TRUE;
	    dplaudio_update_tty_state(dpl_cvd_state);
		__aprv2_cmd_free( dplaudio_apr_handle, packet );
    		break;

      case VSS_ISTREAM_EVT_NOT_READY:
        {
          QPC_GLOBAL_DATA* pGlobalData = QP_NULL;
          IMS_MSG_HIGH_0(LOG_IMS_DPL,"Received VSS_ISTREAM_EVT_NOT_READY setting qvp_rtp_cvd_state to NOT_READY");

          /*reset the g_Dec_requestTime */
          g_Dec_RequestTime = 0;

          /*Also set g_isNetworkCommandPending to false*/
          //g_isNetworkCommandPending = FALSE;
          qpDplPostEventToEventQueue(QPCMSG_DPL_AUDIO_MSG, QP_NULL,QP_NULL, QP_NULL);

          /* Starting the already initialized timer */ 

          pGlobalData = qpDplGetGlobalData();

          if (pGlobalData)
          {
            QP_AUDIO_DATA_AMSS* pAudioData = QP_NULL;
            qpDplDecriptorData *pDplDescData = QP_NULL;

            pDplDescData = (qpDplDecriptorData *)pGlobalData->pAudioData;
            if (pDplDescData)
            {
              pAudioData = (QP_AUDIO_DATA_AMSS*) pDplDescData->uDecriptors.Descriptor.pDesc;
              if (QP_NULL == pAudioData)
              {	
                 IMS_MSG_ERR_0(LOG_IMS_DPL,"pAudioData is NULL");
                 break;
              }
              if(pAudioData->iCurrentState == eAudioStateStarted && !cvd_event_not_ready)
              {
                IMS_MSG_HIGH_0(LOG_IMS_DPL,"cvd event is VSS_ISTREAM_EVT_NOT_READY before setting the timer");
                (void)rex_set_timer(&rex_timer_handle,PKT_DROP_INTERVAL);
                event_counter = 0;
                cvd_event_not_ready = 1;
                if(srvcc_tear_down_req)
                {
                  if(qpAudioStop(pDplDescData))
                  {
                    //do some thing
                  }
                }
              }
            } 
            else
            {
                   IMS_MSG_ERR_0(LOG_IMS_DPL,"pDplDescData is NULL");
            }  
          }
          else
          {
            IMS_MSG_ERR_0(LOG_IMS_DPL,"GlobalData is NULL");
          } 

          IMS_MSG_MED_0(LOG_IMS_DPL,"Received VSS_ISTREAM_EVT_NOT_READY setting qvp_rtp_cvd_state to NOT_READY");
          dpl_cvd_state = FALSE;
          dplaudio_update_tty_state(dpl_cvd_state);
          __aprv2_cmd_free( dplaudio_apr_handle, packet );			
        }
    		break;

      default:
        IMS_MSG_ERR_0(LOG_IMS_DPL,"Received an unsupported command");
        __aprv2_cmd_end_command( dplaudio_apr_handle, packet, APR_EUNSUPPORTED );
        break;
      }
    }
    apr_list_add_tail( free_q, &item->link );
  }
}

static void dplaudio_process_apr_cb (void* param)
{
  IMS_MSG_LOW_0(LOG_IMS_DPL,"dplaudio_process_apr_cb: Waking up to do work");
  dplaudio_process_work_queue( &dplaudio_used_cmd_q, &dplaudio_free_cmd_q );
  dplaudio_process_work_queue( &dplaudio_used_rsp_q, &dplaudio_free_rsp_q );
}

static int32_t dplaudio_isr_dispatch_cb (
  aprv2_packet_t* packet,
  void* dispatch_data
  )
{
  int32_t rc;
  apr_list_t* used_q;
  apr_list_t* free_q;
  dplaudio_work_item_t* item = QP_NULL;
  QPC_GLOBAL_DATA* pGlobalData = (QPC_GLOBAL_DATA*) dispatch_data;

  MSG_LOW( "dplaudio_isr_dispatch_cb: Received a message", 0, 0, 0 );

  if(packet == NULL || dispatch_data == NULL)
  {
    if(packet)
	{
      __aprv2_cmd_accept_command( dplaudio_apr_handle, packet );	
	  __aprv2_cmd_free( dplaudio_apr_handle, packet );
	  IMS_MSG_ERR_0(LOG_IMS_DPL, "dplaudio_isr_dispatch_cb IMS user data NULL");
	}
	IMS_MSG_ERR_0(LOG_IMS_DPL, "dplaudio_isr_dispatch_cb Dropping the APR packet");
    return APR_EOK;
  }
  
  if(packet->opcode == APRV2_IBASIC_EVT_ACCEPTED)
  {
    uint32_t opcode = 0;
    void *payload = NULL;

    payload = APRV2_PKT_GET_PAYLOAD(void, packet); 
    opcode = ((aprv2_ibasic_evt_accepted_t *)payload)->opcode;
    if (opcode != VSS_ISTREAM_EVT_OOB_NOTIFY_ENC_BUFFER_CONSUMED  &&
        opcode != VSS_ISTREAM_EVT_OOB_NOTIFY_DEC_BUFFER_READY) 
    {
     
      g_cmd_ack++;
      IMS_MSG_HIGH_3(LOG_IMS_DPL, "g_cmd_sent=%d g_cmd_rsp=%d g_cmd_ack=%d", g_cmd_sent, g_cmd_rsp, g_cmd_ack);
    }
    __aprv2_cmd_accept_command( dplaudio_apr_handle, packet );  
    __aprv2_cmd_free( dplaudio_apr_handle, packet );
    return APR_EOK;	
  }
  //TBD: Check GlobalData being NULL

  /* Avoid adding additional system delays by processing incoming messages in
  * thread context only.
  */

  /* Separate command messages and response messages to different queues. The
  * client can always re-send commands when the command queue overflows.
  * However, response messages comes only once so it is imperative that they
  * are not lost due to queue overflows.
  */
  if ( APR_GET_FIELD( APRV2_PKT_MSGTYPE, packet->header ) == APRV2_PKT_MSGTYPE_CMDRSP_V )
  {
    used_q = &dplaudio_used_rsp_q;
    free_q = &dplaudio_free_rsp_q;
  }
  else
  {
  #if 0
    if(packet->opcode == VSS_ISTREAM_EVT_OOB_NOTIFY_ENC_BUFFER_READY)
	{
		QPUINT32 av_tx_tstamp         = 0;
		{
             vsd_status_t status;
             voicemem_cmd_cache_invalidate_t voice_mem_config;
             voice_mem_config.voicemem_handle = g_voicemem_handle;
             voice_mem_config.virt_addr = g_base_virt_addr;
             voice_mem_config.size = 8192;
             status = voicemem_call(VOICEMEM_CMD_CACHE_INVALIDATE, &voice_mem_config, sizeof(voicemem_cmd_cache_invalidate_t));
             if(VSD_EOK != status)
             {
               IMS_MSG_ERR_0(LOG_IMS_DPL,"VOICEMEM_CMD_CACHE_INVALIDATE failed");
             }
             
        }
            /** Read Payload from virtual memory **/
        {
              QPUINT32 * temp_var = NULL;
              QPUINT8* temp_read = (QPUINT8*)g_base_virt_addr;
              temp_read += QPDL_AUDIO_ENC_OFFSET;
              temp_var = (QPUINT32*)temp_read;
			  av_tx_tstamp = *temp_var;

		
				/*Should not update AV time while wrap around case as we already have updated time using get time*/				
				if(g_iAVSyncTxDelay)
				{
					av_tx_tstamp += g_iAVSyncTxDelay;
					
					if((uint32)g_iAVsyncTime > av_tx_tstamp )
					{
						/*lower 32 bits have rolled up so increment the upper 32 bits by 1 
							--REVIEW what happens if initially it is 0 ?*/
						
						IMS_MSG_MED_3(LOG_IMS_DPL,"Incrementing upper 32 bits of AV timer by 1 av_tx_tstamp:%LX g_iAVsyncTime: %LX or %LX",*((uint32*)&av_tx_tstamp),g_iAVsyncTime,av_tx_tstamp);
						
						if( !( av_tx_tstamp < DPL_AUD_AV_TIME_MIN_WRAP_ARND ) && ( (uint32)g_iAVsyncTime > DPL_AUD_AV_TIME_MAX_WRAP_ARND ))
						{
							IMS_MSG_FATAL_2(LOG_IMS_DPL,"AV timestamp out of range..looks wrong: previous Rx TS:%X new TS:%X",g_iAVsyncTime,av_tx_tstamp);
						}
						
						g_iAVsyncTime += 0x0000000100000000LL;
						
						g_is_warp_around = TRUE;
						dplaudio_send_command( dplaudio_mvm_addr, dplaudio_app_mvm_port,
						0x12345678,
						VSS_IAVTIMER_CMD_GET_TIME ,
						QP_NULL, 0,
						dplaudio_generic_basic_rsp_handler_cb );
						IMS_MSG_ERR_0(LOG_IMS_DPL,"sending VSS_IAVTIMER_CMD_GET_TIME ");//To remove
					}			    
			    
					g_iAVsyncTime = (g_iAVsyncTime & 0xFFFFFFFF00000000LL) | (av_tx_tstamp & 0x00000000FFFFFFFFLL);  
					qvp_rtp_time_get_ms(&g_iAVmSysTime);
			}
	  
        }
	}
	#endif
    if (packet->opcode == VSS_ISTREAM_EVT_REQUEST_DEC_BUFFER
|| packet->opcode == VSS_ISTREAM_EVT_OOB_NOTIFY_DEC_BUFFER_REQUEST)
    {
      qvp_rtp_time_get_ms(&g_Dec_RequestTime);
    }
    
    used_q = &dplaudio_used_cmd_q;
    free_q = &dplaudio_free_cmd_q;
  }

  rc = apr_list_remove_head( free_q, ( ( apr_list_node_t** ) &item ) );
  if ( rc == APR_EOK )
  {
    __aprv2_cmd_accept_command( dplaudio_apr_handle, packet );

    if(item == QP_NULL)
      return APR_EFAILED; 

    item->packet = packet;
	qvp_rtp_time_get_ms(&item->systime);	
    apr_list_add_tail( used_q, &item->link );

    if ( (g_Dec_RequestTime != 0) && (packet->opcode == VSS_ISTREAM_EVT_SEND_ENC_BUFFER || packet->opcode == VSS_ISTREAM_EVT_OOB_NOTIFY_ENC_BUFFER_READY))
    {
      uint64 current_time = 0;
      uint64 diff = 0;
      qvp_rtp_time_get_ms(&current_time);
      diff = current_time - g_Dec_RequestTime;

      if( diff <=18 || diff > 25)
      {
        qpDplPostEventToEventQueue(QPCMSG_DPL_AUDIO_MSG, QP_NULL,(QPVOID*)dplaudio_process_apr_cb, pGlobalData);
      }

      else
      {
        IMS_MSG_ERR_0(LOG_IMS_DPL, "dplaudio_isr_dispatch_cb:skipping signal posting");
      }
    }
    else
    {
    qpDplPostEventToEventQueue(QPCMSG_DPL_AUDIO_MSG, QP_NULL,
      (QPVOID*)dplaudio_process_apr_cb, pGlobalData);
  }
  }
  else
  {
    IMS_MSG_ERR_0(LOG_IMS_DPL, "dplaudio_isr_dispatch_cb: Ran out of free commands");
    __aprv2_cmd_end_command( dplaudio_apr_handle, packet, APR_EBUSY );
  }

  return APR_EOK;
}

QPVOID FillAudioDplData(QP_AUDIO_DPL_DATA* pAudioDplData, QP_AUDIO_CALLBACK callBackPtr,
                        QPE_AUDIO_MSG audioMsg, QPVOID* pParam, QPUINT32 iParam, 
                        QPVOID* pUserData)
{
  if (pAudioDplData == QP_NULL)
  {
    IMS_MSG_MED_0(LOG_IMS_DPL,"FillAudioDplData|pAudioDplData is NULL");
    return;
  }
  pAudioDplData->audioMsg = audioMsg;
  pAudioDplData->callBackPtr = callBackPtr;
  pAudioDplData->pParam1 = pParam;
  pAudioDplData->iParam2 = iParam;
  pAudioDplData->pUserData = pUserData;
}


QPBOOL qpDplPostEventToEventQueue(QPUINT16 iMsgId, QPINT32 iParam, QPVOID* pParam, QPC_GLOBAL_DATA* pGlobalData);

QPVOID qpDplPostAudioEventToEventQueue(QPC_GLOBAL_DATA* pGlobalData, 
                                       QP_AUDIO_DPL_DATA* pAudioDplData)
{
  (QPVOID)qpDplPostEventToEventQueue(AUDIO_MSG_VAL, 0, pAudioDplData, QP_NULL);
}

static void dplaudio_create_work_queues (
  apr_list_t* used_q,
  apr_list_t* free_q,
  dplaudio_work_item_t* store,
  uint32_t store_size
  )
{
  uint32_t index;

  apr_list_init( used_q, dplaudio_isr_lock_fn, dplaudio_isr_unlock_fn );
  apr_list_init( free_q, dplaudio_isr_lock_fn, dplaudio_isr_unlock_fn );

  for ( index = 0; index < store_size; ++index )
  {
    apr_list_add_tail( free_q, &store[ index ].link );
  }
}

static int dplaudio_apr_init ( void )
{
  int32_t rc = 0;
  QPC_GLOBAL_DATA* pGlobalData = QP_NULL;

  pGlobalData = qpDplGetGlobalData();

  apr_lock_create( APR_LOCK_TYPE_INTERRUPT, &dplaudio_lock );

  { /* Initialize the handle table. */
    apr_objmgr_setup_params_t setup_args;

    setup_args.table = dplaudio_objects;
    setup_args.total_bits = DPLAUDIO_HANDLE_TOTAL_BITS;
    setup_args.index_bits = DPLAUDIO_HANDLE_INDEX_BITS;
    setup_args.lock_fn = dplaudio_isr_lock_fn;
    setup_args.unlock_fn = dplaudio_isr_unlock_fn;

    rc = apr_objmgr_construct( &dplaudio_objmgr, &setup_args );
  }
  { /* Initialize work queues. */
    dplaudio_create_work_queues( &dplaudio_used_rsp_q, &dplaudio_free_rsp_q,
      dplaudio_rsps, DPLAUDIO_RSP_QUEUE_SIZE );
    dplaudio_create_work_queues( &dplaudio_used_cmd_q, &dplaudio_free_cmd_q,
      dplaudio_cmds, DPLAUDIO_CMD_QUEUE_SIZE );
  }

  rc = __aprv2_cmd_register2( &dplaudio_apr_handle, dplaudio_my_dns,
    sizeof( dplaudio_my_dns ), 0,
    dplaudio_isr_dispatch_cb, pGlobalData,
    &dplaudio_my_addr );

  return rc;
}

static int dplaudio_cvd_setup ( void )
{
  int32_t rc = 0;

  rc = __aprv2_cmd_local_dns_lookup( dplaudio_cvs_dns,
    sizeof( dplaudio_cvs_dns ),
    &dplaudio_cvs_addr );


  rc = __aprv2_cmd_local_dns_lookup( dplaudio_cvp_dns,
    sizeof( dplaudio_cvp_dns ),
    &dplaudio_cvp_addr );


  rc = __aprv2_cmd_local_dns_lookup( dplaudio_mvm_dns,
    sizeof( dplaudio_mvm_dns ),
    &dplaudio_mvm_addr );

  return rc;
}

static int dplaudio_cvd_init ( void )
{
  int rc = 0;
  if(g_rat_type == AUDIO_RAT_IWLAN)
  { /* Acquire a full control MVM session. Use ADB to brinp CS voice device. */
    dplaudio_send_command( dplaudio_mvm_addr, APR_NULL_V,
      0x12345678,
      VSS_IMVM_CMD_CREATE_FULL_CONTROL_SESSION,
      &dplaudio_mvm_session_name_wlan,
      sizeof( dplaudio_mvm_session_name_wlan),
      dplaudio_custom_app_full_mvm_rsp_handler_cb );
  }
  else
  {
    dplaudio_send_command( dplaudio_mvm_addr, APR_NULL_V,
      0x12345678,
      VSS_IMVM_CMD_CREATE_FULL_CONTROL_SESSION,
      &dplaudio_mvm_session_name,
      sizeof( dplaudio_mvm_session_name ),
      dplaudio_custom_app_full_mvm_rsp_handler_cb );
  }

  return rc;
}

static int dpl_cvd_set_codec(qpDplDecriptorData *pDplDescData)
{
    boolean codec_mode_change = FALSE;
    int rc = 0;
    QP_AUDIO_DATA_AMSS* pAudioData = QP_NULL;
    
    if(NULL == pDplDescData)
    {
      return 1;
    }
    
    pAudioData = (QP_AUDIO_DATA_AMSS*)pDplDescData->uDecriptors.Descriptor.pDesc;

    if (QP_NULL == pAudioData)
    {
      IMS_MSG_ERR_0(LOG_IMS_DPL,"dpl_cvd_config_codec -  AudioData is NULL");
      return 1;
    }
    
    if(AUDIO_CODEC_AMR == pDplDescData->tCodecConfig.eCodec ||
       AUDIO_CODEC_AMR_WB == pDplDescData->tCodecConfig.eCodec)
    {
      uint32 size = 0;
      uint32 msgtype = 0;
      void *msg = NULL;
      uint32 mode = 0xFF;

      if(pDplDescData->tCodecConfig.sCodecInfo.sAMRInfo.eCodecMode != pDplDescData->tNewCodecConfig.sCodecInfo.sAMRInfo.eCodecMode)
      {
        codec_mode_change = TRUE;
      }
      pDplDescData->tCodecConfig.sCodecInfo.sAMRInfo.eCodecMode = pDplDescData->tNewCodecConfig.sCodecInfo.sAMRInfo.eCodecMode;

      // Different mode so initiate mode change command
      if (AUDIO_CODEC_AMR == pDplDescData->tCodecConfig.eCodec)
      {
        vss_istream_cmd_voc_amr_set_enc_rate_t set_amr_mode;
        if ((AUDIO_CODEC_MODE_7 < pDplDescData->tCodecConfig.sCodecInfo.sAMRInfo.eCodecMode))
        {
          set_amr_mode.mode = AUDIO_CODEC_MODE_7;
        }
        else
        {
          set_amr_mode.mode = ( ( uint32_t ) pDplDescData->tCodecConfig.sCodecInfo.sAMRInfo.eCodecMode);
        }
        msg = (void*)&set_amr_mode;
        size = sizeof(vss_istream_cmd_voc_amr_set_enc_rate_t);
        mode = set_amr_mode.mode; 
        msgtype = VSS_ISTREAM_CMD_VOC_AMR_SET_ENC_RATE;
      }
      else
      {
        vss_istream_cmd_voc_amrwb_set_enc_rate_t set_awb_mode;
        QPE_AUDIO_AMR_CODEC_MODE amr_wb_mode;

        amr_wb_mode = pDplDescData->tCodecConfig.sCodecInfo.sAMRInfo.eCodecMode;

        if (AUDIO_CODEC_MODE_8 < amr_wb_mode)
        {
          set_awb_mode.mode = AUDIO_CODEC_MODE_8;
        }
        else
        {
          set_awb_mode.mode = ( uint32_t ) amr_wb_mode;
        }
        size = sizeof(vss_istream_cmd_voc_amrwb_set_enc_rate_t);
        msg = (void*)&set_awb_mode;
        mode = set_awb_mode.mode;
        msgtype = VSS_ISTREAM_CMD_VOC_AMRWB_SET_ENC_RATE;
      }

      IMS_MSG_HIGH_2(LOG_IMS_DPL,"Setting %d TX mode to %d", pDplDescData->tCodecConfig.eCodec,mode );

      dplaudio_send_command( dplaudio_cvs_addr, dplaudio_app_cvs_port,
        0x12345678, msgtype, msg, size,
        dplaudio_generic_basic_rsp_handler_cb );
      
      g_PrevRxMode = 100;//Resetting Mode

      pDplDescData->eCvdState = eCvdStateConfigCodecProp;
      if(pAudioData->iCurrentState == eAudioStateConfiguring)
      {  
        qpDplLogCodecEvents(0 /* 0 - rx direction */, 0 /* 0 - AMR */, mode, EVENT_DPL_CODEC_INIT);
        qpDplLogCodecEvents(1 /* 1 - Tx direction */, 0 /* 0 - AMR */, mode, EVENT_DPL_CODEC_INIT);
      }
      else if(pAudioData->iCurrentState == eAudioStateReConfiguringCodeMode || codec_mode_change)
      {
        qpDplLogCodecEvents(1 /* 1 - Tx direction */, 0 /* 0 - AMR */, mode, EVENT_AMR_CODEC_RATE);
      }
    }
    else if (AUDIO_CODEC_EVS == pDplDescData->tCodecConfig.eCodec)
    {
           
      QP_EVS_CODEC_INFO *evs = &pDplDescData->tCodecConfig.sCodecInfo.sEVSInfo; 
      evs->eCodecMode.ePrmyMode = pDplDescData->tNewCodecConfig.sCodecInfo.sEVSInfo.eCodecMode.ePrmyMode;

      //evs_mode.mode = evs->eCodecMode.ePrmyMode;
      if(AUDIO_EVS_CODEC_MODE_4 == evs->eCodecMode.ePrmyMode && TRUE == evs->ch_aw_enabled)
      {
        /* this is channel aware mode set the channel aware enable first*/
        vss_istream_cmd_set_evs_enc_channel_aware_mode_enable_t args;
        args.fec_offset = evs->tx_evs_red_offset;
        args.fer_rate = evs->tx_fer;
        IMS_MSG_ERR_2(LOG_IMS_DPL,"EVS config for mode 4 - fec_offset = %d \
                                   fer = %d ",args.fec_offset,args.fer_rate);
        dplaudio_send_command(dplaudio_cvs_addr, dplaudio_app_cvs_port,
               0x12345678,
               VSS_ISTREAM_CMD_SET_EVS_ENC_CHANNEL_AWARE_MODE_ENABLE,
               &args, sizeof(vss_istream_cmd_set_evs_enc_channel_aware_mode_enable_t),
               dplaudio_generic_basic_rsp_handler_cb);
         pDplDescData->eCvdState = eCvdStateConfigCHAwareMode;
      }
      else
      {
        IMS_MSG_ERR_2(LOG_IMS_DPL,"EVS config for mode = %d bandwidth = %d ",
                      evs->eCodecMode.ePrmyMode,evs->eTxBW);
         /* call to set the encocder mode */
         qpDplSetEVSEncMode(pDplDescData,evs->eCodecMode.ePrmyMode,evs->eTxBW);
      }
    }
    else
    {
      IMS_MSG_ERR_1(LOG_IMS_DPL,"dpl_cvd_reconfigure - Codec is not supported: %x", pDplDescData->tCodecConfig.eCodec);
      rc = 1;
    }
    return rc;
}

int dpl_cvd_set_media(qpDplDecriptorData *pDplDescData)
{
  int rc = 0;
  vss_istream_cmd_set_media_type_t set_media_type;
  voicecfg_cmd_get_bool_item_t item;

  void *payload = NULL;
  uint32 payloadsize = 0;
  QP_CODEC_CONFIG* pCC = &pDplDescData->tCodecConfig;

  item.id = VOICECFG_CFG_IS_VOLTE_EAMR_ENABLED;
  item.ret_value = FALSE;

  // Copy the new configuration to permanent config
  qpDplMemscpy(&pDplDescData->tCodecConfig, sizeof(QP_CODEC_CONFIG), &pDplDescData->tNewCodecConfig, sizeof(QP_CODEC_CONFIG));
  switch (pCC->eCodec)
  {
    case AUDIO_CODEC_AMR :
    {
      set_media_type.rx_media_id = VSS_MEDIA_ID_AMR_NB_MODEM;
      set_media_type.tx_media_id = VSS_MEDIA_ID_AMR_NB_MODEM;
      rc =  voicecfg_call( VOICECFG_CMD_GET_BOOL_ITEM, &item,
                                 sizeof( item ) );
      if ( ! rc  && item.ret_value == TRUE )
      {
        // Enable eAMR for VOLTE call.
        IMS_MSG_ERR_0(LOG_IMS_DPL,"dpl_cvd_reconfigure -  VSS_MEDIA_ID_EAMR enabled");
        set_media_type.rx_media_id = VSS_MEDIA_ID_EAMR;
        set_media_type.tx_media_id = VSS_MEDIA_ID_EAMR;
        pDplDescData->is_eamr_enabled = QP_TRUE;
      }
      rc = 0;
    }
    break;
    case AUDIO_CODEC_AMR_WB:
    {
      set_media_type.rx_media_id = VSS_MEDIA_ID_AMR_WB_MODEM;
      set_media_type.tx_media_id = VSS_MEDIA_ID_AMR_WB_MODEM;
    }
    break;
    case AUDIO_CODEC_G711U:
    {
      set_media_type.rx_media_id = VSS_MEDIA_ID_G711_MULAW_V2;
      set_media_type.tx_media_id = VSS_MEDIA_ID_G711_MULAW_V2;
    }
    break;
    case AUDIO_CODEC_G711A:
    {
      set_media_type.rx_media_id = VSS_MEDIA_ID_G711_ALAW_V2;
      set_media_type.tx_media_id = VSS_MEDIA_ID_G711_ALAW_V2;
    }
    break;
    case AUDIO_CODEC_EVS:
     set_media_type.rx_media_id = VSS_MEDIA_ID_EVS;
     set_media_type.tx_media_id = VSS_MEDIA_ID_EVS;
    break;
    default:
    {
     IMS_MSG_ERR_1(LOG_IMS_DPL,"dpl_cvd_reconfigure -  Codec Not Supported: %d", pCC->eCodec);
      set_media_type.rx_media_id = 0;
     rc = 1;
     break;
    }
  }
  IMS_MSG_HIGH_1(LOG_IMS_DPL,"Setting Media to  %d", set_media_type.rx_media_id );
  if(!rc)
  {
    payload = (void*)&set_media_type;
    payloadsize = sizeof(vss_istream_cmd_set_media_type_t);
    pDplDescData->eCvdState = eCvdStateConfigMedia;
    dplaudio_send_command( dplaudio_cvs_addr, dplaudio_app_cvs_port,
      0x12345678,
      VSS_ISTREAM_CMD_SET_MEDIA_TYPE,payload,payloadsize,
      dplaudio_generic_basic_rsp_handler_cb );
  }
  return rc;
}

static int dpl_cvd_reconfigure(qpDplDecriptorData *pDplDescData)
{
  int rc = 0;
  //uint8 amr_wb_mode = 0;
  QPC_GLOBAL_DATA* pGlobalData = QP_NULL;
  uint8 lflag = 0;
  pGlobalData = qpDplGetGlobalData();

  do
  {
    QP_AUDIO_DATA_AMSS* pAudioData = QP_NULL;
    QP_EVS_CODEC_INFO *evs = NULL;
    QP_EVS_CODEC_INFO *evsNew = NULL;
    if (QP_NULL == pDplDescData)
    {
      IMS_MSG_ERR_0(LOG_IMS_DPL,"dpl_cvd_reconfigure -  Invalid Param");
      rc = 1;
      break;
    }

    pAudioData = (QP_AUDIO_DATA_AMSS*)pDplDescData->uDecriptors.Descriptor.pDesc;

    if (QP_NULL == pAudioData)
    {
      IMS_MSG_ERR_0(LOG_IMS_DPL,"dpl_cvd_reconfigure -  AudioData is NULL");
      rc = 1;
      break;
    }
    if(AUDIO_CODEC_EVS == pDplDescData->tCodecConfig.eCodec && 
       AUDIO_CODEC_EVS == pDplDescData->tNewCodecConfig.eCodec)
    {
      /* this means there is a change in either the evs codec rate or
         band width, redundacy, channelawaremode disabled */
      evs = &pDplDescData->tCodecConfig.sCodecInfo.sEVSInfo;
      evsNew = &pDplDescData->tNewCodecConfig.sCodecInfo.sEVSInfo;
      if(evs->eTxBW != evsNew->eTxBW || 
         evsNew->ch_aw_enabled == TRUE ||
           evs->ch_aw_enabled == TRUE ||
           (evsNew->eCodecMode.ePrmyMode == AUDIO_EVS_CODEC_MODE_4 && 
			evsNew->ch_aw_enabled == TRUE && 
            evsNew->tx_evs_red_offset!= evs->tx_evs_red_offset))
            {
              /*set flag that this has to change all settings*/
              lflag = 1;
            }
    }
    // Check if Codec is changed then begin with MediaType command
    if (pDplDescData->tCodecConfig.eCodec != pDplDescData->tNewCodecConfig.eCodec ||
        lflag)
    {
      // Copy the new configuration to permanent config
      qpDplMemscpy(&pDplDescData->tCodecConfig, sizeof(QP_CODEC_CONFIG),&pDplDescData->tNewCodecConfig, sizeof(QP_CODEC_CONFIG));
      if((rc = dpl_cvd_set_media(pDplDescData)))
      {
        IMS_MSG_ERR_0(LOG_IMS_DPL,"dpl_cvd_reconfigure: call to set media failed");
        break;
      }
      pAudioData->iCurrentState = eAudioStateReConfiguring;
    }
    else 
    {
      if( (AUDIO_CODEC_G711A== pDplDescData->tCodecConfig.eCodec) || (AUDIO_CODEC_G711U == pDplDescData->tCodecConfig.eCodec) )
      {
        IMS_MSG_HIGH_0(LOG_IMS_DPL,"G711 codec. No Need to reconfigure CVD.");
        qpDplPostEventToEventQueue(QPCMSG_DPL_AUDIO_MSG, QP_NULL,(QPVOID*)dplaudio_G711_codec_change_response, pGlobalData);
      }
      else
      {
        pAudioData->iCurrentState = eAudioStateReConfiguringCodeMode;
        rc = dpl_cvd_set_codec(pDplDescData);
        
      }
    }
  }while(0);

  return rc;
}

static int dpl_cvd_standby_before_reconfig(qpDplDecriptorData *pDplDescData)
{
  int rc = 0;
  QP_EVS_CODEC_INFO  *evs = NULL;
  QP_EVS_CODEC_INFO  *evsNew = NULL;
  
  do{

    QP_AUDIO_DATA_AMSS* pAudioData = QP_NULL;
    if (QP_NULL == pDplDescData)
    {
      IMS_MSG_ERR_0(LOG_IMS_DPL,"dpl_cvd_standby_before_reconfig -  Invalid Param");
      rc = 1;
      break;
    }

    pAudioData = (QP_AUDIO_DATA_AMSS*)pDplDescData->uDecriptors.Descriptor.pDesc;

    if (QP_NULL == pAudioData)
    {
      IMS_MSG_ERR_0(LOG_IMS_DPL,"dpl_cvd_standby_before_reconfig -  AudioData is NULL");
      rc = 1;
      break;
    }  
    if(AUDIO_CODEC_EVS == pDplDescData->tCodecConfig.eCodec)
    {
      evs = &pDplDescData->tCodecConfig.sCodecInfo.sEVSInfo;
      evsNew = &pDplDescData->tNewCodecConfig.sCodecInfo.sEVSInfo;
    }
    // Check if Codec is changed
    if(pDplDescData->tCodecConfig.eCodec != pDplDescData->tNewCodecConfig.eCodec ||
       (AUDIO_CODEC_EVS == pDplDescData->tCodecConfig.eCodec && 
        ((evs->ch_aw_enabled == TRUE || 
         evsNew->ch_aw_enabled == TRUE) ||
         (evsNew->eCodecMode.ePrmyMode == AUDIO_EVS_CODEC_MODE_4 && 
		  evsNew->ch_aw_enabled == TRUE && 
          (evs->eTxBW != evsNew->eTxBW || evs->tx_fer != evsNew->tx_fer ||
           evs->tx_evs_red_offset != evsNew->tx_evs_red_offset))))
        )
    {
      dplaudio_send_command( dplaudio_mvm_addr, dplaudio_app_mvm_port,
        0x12345678,
        VSS_IMVM_CMD_MODEM_STANDBY_VOICE,
        QP_NULL, 0,
        dplaudio_generic_basic_rsp_handler_cb );

      pAudioData->iCurrentState = eAudioStateReConfiguring;
      pDplDescData->eCvdState = eCvdStateStandby;
    }
    else
    {
      //No need to go to stadby. we can reconfigure directly 
      dpl_cvd_reconfigure(pDplDescData);
    }

  }while(0);

  return rc;
}


int dpl_cvd_configure(qpDplDecriptorData *pDplDescData)
{
  int rc = 0;
  do{
    QP_AUDIO_DATA_AMSS* pAudioData = QP_NULL;
    if (QP_NULL == pDplDescData)
    {
      IMS_MSG_ERR_0(LOG_IMS_DPL,"dpl_cvd_configure -  Invalid Param");
      rc = 1;
      break;
    }

    pAudioData = (QP_AUDIO_DATA_AMSS*)pDplDescData->uDecriptors.Descriptor.pDesc;

    if (QP_NULL == pAudioData)
    {
      IMS_MSG_ERR_0(LOG_IMS_DPL,"dpl_cvd_configure -  AudioData is NULL");
      rc = 1;
      break;
    }

    // First Configuration
    if (eAudioStateInited == pAudioData->iCurrentState || eAudioStateConfigured == pAudioData->iCurrentState)
    {
      dpl_cvd_set_media(pDplDescData);
      pAudioData->iCurrentState = eAudioStateConfiguring;
    }
  }while(0);

  return rc;
}

QP_EXPORT_C QP_AUDIO_DESC qpAudioInitialize(QP_AUDIO_CALLBACK tAudioCallBack,
                                            QPVOID* pUserData,
                                            QPE_AUDIO_DEV eADev,
                                            QPE_AUDIO_RAT_PVT rat_type)
{
  QP_AUDIO_DATA_AMSS* pAudioData = QP_NULL;
  qpDplDecriptorData *pDplDescData = QP_NULL; 
  QPC_GLOBAL_DATA* pGlobalData     = QP_NULL;
  QPUINT8 loopback=0;
  QPE_IO_ERROR error;

  g_cmd_rsp = 0;
  g_cmd_ack = 0;
  g_cmd_sent = 0;

  IMS_MSG_MED_0(LOG_IMS_DPL,"qpAudioInitialize Start ");
  error = qpDplIODeviceGetItem(IMS_RTP_ASSERT_ENABLED, &g_apr_assert, sizeof(QPUINT8)); 
  if(error != QP_IO_ERROR_OK)
  {
     IMS_MSG_ERR_0(LOG_IMS_DPL,"qpAudioInitialize: NV read for assert enable failed set to 0");
	   g_apr_assert = 0;
  }

  do{
    pGlobalData = qpDplGetGlobalData();
    if (QP_NULL == pGlobalData)
    {
      IMS_MSG_FATAL_0(LOG_IMS_DPL,"qpAudioInitialize - GlobalData is NULL");
      return QP_NULL;
    }

    pDplDescData = (qpDplDecriptorData*)qpDplMalloc(MEM_IMS_DPL, sizeof(qpDplDecriptorData));

    if(QP_NULL == pDplDescData)
    {
      IMS_MSG_FATAL_0(LOG_IMS_DPL,"qpAudioInitialize - Malloc Failed");
      return QP_NULL;
    }

    qpDplMemset(&audiopath_delay_event_info_type,0,sizeof(qvp_audiopath_delay_change_event_info_type));

    qpDplMemset(pDplDescData,0,sizeof(qpDplDecriptorData));
    av_timestamp32 = 0;
    g_PrevRxMode = 100;
    pGlobalData->pAudioData = (QPVOID*)pDplDescData;

    g_rat_type = rat_type;

    if(AUDIO_PLAYER_RECORDER == eADev)
    {
      int iCounter = 0;
      int rc = -1;

      pAudioData = (QP_AUDIO_DATA_AMSS*) qpDplMalloc(MEM_IMS_DPL, sizeof(QP_AUDIO_DATA_AMSS));

      if (QP_NULL == pAudioData)
      {
        IMS_MSG_FATAL_0(LOG_IMS_DPL,"qpAudioInitialize - Malloc Failed");
        break;
      }

      qpDplMemset(pAudioData, 0, sizeof(QP_AUDIO_DATA_AMSS));

      /*************************************************************
      Check If Loopback mode is enabled
      *************************************************************/
      error = qpDplIODeviceGetItem(IMS_RTP_LOOP_BACK, &loopback, sizeof(QPUINT8));
      IMS_MSG_MED_3(LOG_IMS_DPL, "qpAudioInitialize | IMS_RTP_LOOP_BACK  %d for %s, loopback value is %d", error, IMS_RTP_LOOP_BACK, loopback);
      pAudioData->bIsLoopBackMode = (QPBOOL)loopback;

      for (iCounter = 0; iCounter < DPL_AUD_MAX_BUNDLE_SIZE; iCounter++)
      {
        pAudioData->pAudioBundle[iCounter] = (QP_MULTIMEDIA_FRAME* )qpDplMalloc(MEM_IMS_DPL, sizeof(QP_MULTIMEDIA_FRAME));
        if (pAudioData->pAudioBundle[iCounter])
        {
          qpDplMemset(pAudioData->pAudioBundle[iCounter], 0, sizeof(QP_MULTIMEDIA_FRAME));
        }
      }

      rc = dplaudio_apr_init();
      if (rc)
      {
        IMS_MSG_FATAL_0(LOG_IMS_DPL,"qpAudioInitialize - APR Init Failed");
        break;
      }

      rc = dplaudio_cvd_setup();
      if (rc)
      {
        IMS_MSG_FATAL_0(LOG_IMS_DPL,"qpAudioInitialize - CVD Setup Failed");
        break;
      }
      qpDplMemset(&cvd_timer_handle, 0, sizeof(rex_timer_type));
      rex_def_timer_ex(&cvd_timer_handle, dplaudio_cvd_timeout_cb, NULL);

      pDplDescData->eCvdState = eCvdStateInitMVMCtrlSession;
      rc = dplaudio_cvd_init();

      if (rc)
      {
        IMS_MSG_FATAL_0(LOG_IMS_DPL,"qpAudioInitialize - CVD Init Failed");
        break;
      }

      g_iAVsyncTime = 0;
      g_iAVmSysTime = 0;
      g_firstdequeue = TRUE;
      g_updateAVdelay = TRUE;
      g_is_warp_around = FALSE;
      g_iAVSyncTxDelay  = 0;

      pDplDescData->eMode = eAudioULDLMode;
      pDplDescData->bIssink_active = QP_FALSE;
      pDplDescData->is_eamr_enabled = QP_FALSE;
      pDplDescData->bIsaudio_started = QP_FALSE;
      pAudioData->eADev = AUDIO_PLAYER_RECORDER;
      pAudioData->bIsPlayerRecorderMode = QP_TRUE;
      pAudioData->iCurrentState = eAudioStateIniting;
      pAudioData->tAudioCallBack = tAudioCallBack;
      pAudioData->pUserData = pUserData;
      last_sent_timestamp = 0;
      pDplDescData->uDecriptors.Descriptor.pDesc = pAudioData;

      pAudioData->hQdjHandle = qpDplQdjCreateHandle();

      if(!pAudioData->hQdjHandle)
      {
        IMS_MSG_FATAL_0(LOG_IMS_DPL,"qpAudioInitialize - QDJ Init Failed");
        break;
      }

		/* Timer allocation for cvd events */
		//IMS_MSG_HIGH_0(LOG_IMS_DPL,"Timer definition for cvd events");
		qpDplMemset(&rex_timer_handle,0,sizeof(rex_timer_type));
		rex_def_timer_ex(&rex_timer_handle, dplaudio_evnt_CB_temp,(uint32 )pGlobalData );

      return pDplDescData;
    }
    else
    {
      IMS_MSG_FATAL_0(LOG_IMS_DPL,"qpAudioInitialize - Only AUDIO_PLAYER_RECORDER is Supported");
      break;
    } 

  }while(0);

  /*************************************************************
  If the init has failed on either the player or the recorder 
  free the allocated structure and return 
  *************************************************************/

  if(!pDplDescData->uDecriptors.PlayRecDesc.pPlayerDesp|| !pDplDescData->uDecriptors.PlayRecDesc.pRecorderDesp)
  {
    qpDplFree(MEM_IMS_DPL, pDplDescData);
    pDplDescData = QP_NULL;
    IMS_MSG_ERR_0(LOG_IMS_DPL,"qpAudioInitialize -  Failed");
  }

    
  
  IMS_MSG_MED_0(LOG_IMS_DPL,"qpAudioInitialize Done");

  return pDplDescData;
}

void dplaudio_cvd_timeout_cb(uint32 param)
{
  IMS_MSG_ERR_1(LOG_IMS_DPL,"dplaudio_cvd_timeout_cb expired cvd_state %d", g_eCVDState);
  ASSERT(0);
}

void dplaudio_evnt_CB_temp(uint32 param)
{
    IMS_MSG_ERR_0(LOG_IMS_DPL,"Received EVENT_NOT_READY 20 MS dequeue time and posting signal to RTP task for Dequeue");
	if(cvd_event_not_ready)
	{
		qpDplPostEventToEventQueue(QPCMSG_DPL_AUDIO_MSG, QP_NULL,
		(QPVOID*)dplaudio_evnt_CB, (void*)param);
	}
}

	  
void dpl_aud_cleanup(qpDplDecriptorData *pDplDescData)
{
  QPUINT8 iCounter = 0;
  QP_AUDIO_DATA_AMSS* pAudioData = QP_NULL;
  QPC_GLOBAL_DATA* pGlobalData = QP_NULL;

  pGlobalData = qpDplGetGlobalData();


  if (QP_NULL == pDplDescData || QP_NULL == pGlobalData)
  {
    IMS_MSG_FATAL_0(LOG_IMS_DPL,"dpl_aud_cleanup -  Failed: Descriptor is NULL");
    return;
  }

  pAudioData = (QP_AUDIO_DATA_AMSS*) pDplDescData->uDecriptors.Descriptor.pDesc;
  if (QP_NULL == pAudioData)
  {
    IMS_MSG_ERR_0(LOG_IMS_DPL,"dpl_aud_cleanup: pAudioData is NULL");
    qpDplFree(MEM_IMS_DPL, pDplDescData);
    pDplDescData = QP_NULL;
    return;
  }

  __aprv2_cmd_deregister( dplaudio_apr_handle );
  pDplDescData->eCvdState   = eCvdStateNULL;
  pAudioData->iCurrentState = eAudioStateNULL;

  for (iCounter = 0; iCounter < DPL_AUD_MAX_BUNDLE_SIZE; iCounter++)
  {
    if (pAudioData->pAudioBundle[iCounter])
    {
      qpDplFree(MEM_IMS_DPL, pAudioData->pAudioBundle[iCounter]);
      pAudioData->pAudioBundle[iCounter] = 0;
    }
  }

  qpDplFree(MEM_IMS_DPL, pAudioData);
  qpDplFree(MEM_IMS_DPL, pDplDescData);
  pDplDescData = QP_NULL;
  pGlobalData->pAudioData = QP_NULL;

  // Clean up the APR Q and all
  /* Deinitialize work queues. */
  apr_list_destroy( &dplaudio_free_cmd_q );
  apr_list_destroy( &dplaudio_used_cmd_q );
  apr_list_destroy( &dplaudio_free_rsp_q );
  apr_list_destroy( &dplaudio_used_rsp_q );
}

QP_EXPORT_C QPVOID qpAudioUninitialize(QP_AUDIO_DESC tAudioDescriptor,
                                       QPE_AUDIO_DEV eADev)
{
  QP_AUDIO_DATA_AMSS* pAudioData = QP_NULL;

  do
  {
    qpDplDecriptorData *pDplDescData = QP_NULL;

    if (QP_NULL == tAudioDescriptor)
    {
      IMS_MSG_FATAL_0(LOG_IMS_DPL,"qpAudioUnInitialize -  Failed: Descriptor is NULL");
      break;
    }

    pDplDescData = (qpDplDecriptorData*)tAudioDescriptor;
    pAudioData = (QP_AUDIO_DATA_AMSS*)pDplDescData->uDecriptors.Descriptor.pDesc;


    if(pAudioData->hQdjHandle)
    {
      if(0 != qpDplQdjDestroyHandle(pAudioData->hQdjHandle))
        IMS_MSG_ERR_0(LOG_IMS_DPL,"qpAudioUnInitialize - QDJ De init failed:");
	  pAudioData->hQdjHandle = NULL;
    }

    /* Cancle the inotify events in the CVS instance */
    {
      vss_inotify_cmd_cancel_event_class_t cancel_evt_cmd;
      cancel_evt_cmd.class_id = VSS_IAVSYNC_EVENT_CLASS_RX;

      dplaudio_send_command( dplaudio_cvs_addr, dplaudio_app_cvs_port,
        0x12345678, VSS_INOTIFY_CMD_CANCEL_EVENT_CLASS,
        &cancel_evt_cmd, sizeof(vss_inotify_cmd_cancel_event_class_t),
        dplaudio_generic_basic_rsp_handler_cb );
      IMS_MSG_ERR_0(LOG_IMS_DPL,"cancel - VSS_IAVSYNC_EVENT_CLASS_RX");
    }

    pAudioData->iCurrentState = eAudioStateReleasing;
    pDplDescData->bIsaudio_started = QP_FALSE;
    g_Dec_RequestTime = 0;
	
    //g_isNetworkCommandPending = FALSE;
    g_iAVsyncTime = 0;
    g_iAVmSysTime = 0;
    g_firstdequeue = TRUE;	
    g_updateAVdelay = TRUE;
    g_iAVSyncTxDelay  = 0;
	
    /** Client should set the packet exchange mode to default(inband) before unpamming the memory **/
    dplaudio_set_pktExchng_Mode(pDplDescData,FALSE);

  }while(0);
  //IMS_MSG_LOW_0(LOG_IMS_DPL,"qpAudioUninitialize : clearing the cvd event timer");
  timer_undef(&rex_timer_handle);
 	 timer_undef(&cvd_timer_handle);
}


QP_EXPORT_C QPE_AUDIO_ERROR qpAudioCodecSet(QP_AUDIO_DESC tAudioDescriptor, 
                                            QP_CODEC_CONFIG tCodecConfig)
{
  QPE_AUDIO_ERROR eRetValue = AUDIO_ERROR_UNKNOWN;
  qpDplDecriptorData *pDplDescData = QP_NULL;
  QPBOOL reset_qdj = TRUE;
  QPDPL_QDJ_PARAMS_TYPE  qdj_params;

  if (QP_NULL == tAudioDescriptor)
  {
    IMS_MSG_ERR_0(LOG_IMS_DPL,"qpAudioCodecSet -  Invalid Param");
    return eRetValue;
  }
  pDplDescData = (qpDplDecriptorData*)tAudioDescriptor;

  IMS_MSG_HIGH_2(LOG_IMS_DPL,"codec selected = %d, codec mode selected = %d", tCodecConfig.eCodec, tCodecConfig.sCodecInfo.sAMRInfo.eCodecMode);

  do
  {
    int32_t rc = 0;
    QP_AUDIO_DATA_AMSS* pAudioData = (QP_AUDIO_DATA_AMSS*)pDplDescData->uDecriptors.Descriptor.pDesc;

    if ((eAudioStateNULL == pAudioData->iCurrentState) || (eAudioStateReleasing == pAudioData->iCurrentState))
    {
      IMS_MSG_ERR_1(LOG_IMS_DPL,"qpAudioCodecSet - Failed: Invoked in wrong state: %d", pAudioData->iCurrentState);
      break;
    }
    else if ((eAudioStateIniting == pAudioData->iCurrentState)|| 
      (eAudioStateStarting == pAudioData->iCurrentState)||
      (eAudioStateStopping == pAudioData->iCurrentState)||
      (eAudioStateConfiguring == pAudioData->iCurrentState)||
	  (eAudioStateReConfiguring == pAudioData->iCurrentState)||
      (eAudioStateReConfiguringCodeMode == pAudioData->iCurrentState))
    {
      IMS_MSG_LOW_1(LOG_IMS_DPL, "qpAudioCodecSet, delaying the configuration, Transitional State: %x", pAudioData->iCurrentState);
      qpDplMemscpy(&pDplDescData->tNewCodecConfig, sizeof(QP_CODEC_CONFIG), &tCodecConfig, sizeof(QP_CODEC_CONFIG));
      pDplDescData->bConfigPending = QP_TRUE;
      eRetValue = AUDIO_ERROR_OK;
      break;
    }
    else if (eAudioStateInited == pAudioData->iCurrentState || eAudioStateConfigured  == pAudioData->iCurrentState)
    {
      qpDplMemscpy(&pDplDescData->tNewCodecConfig, sizeof(QP_CODEC_CONFIG), &tCodecConfig, sizeof(QP_CODEC_CONFIG));

      rc = dpl_cvd_configure(pDplDescData);
    }
    else // Codec Re configure case
    {
      qpDplMemscpy(&pDplDescData->tNewCodecConfig, sizeof(QP_CODEC_CONFIG), &tCodecConfig, sizeof(QP_CODEC_CONFIG));
	  if(pDplDescData->tCodecConfig.eCodec == pDplDescData->tNewCodecConfig.eCodec)
			reset_qdj = FALSE;

      if(pAudioData->hQdjHandle && reset_qdj)
      {
        if(0 != qpDplQdjDestroyHandle(pAudioData->hQdjHandle))
        {
          IMS_MSG_ERR_0(LOG_IMS_DPL,"qpAudioCodecSet - QDJ De init failed:");
          break;
        }

        pAudioData->hQdjHandle = NULL;
		
        //Re create the QDJ //TODO make a new API in QDJ to reset
        pAudioData->hQdjHandle = qpDplQdjCreateHandle();
      }
      rc = dpl_cvd_standby_before_reconfig(pDplDescData);
    }

    if (rc)
    {
      IMS_MSG_ERR_0(LOG_IMS_DPL,"qpAudioCodecSet -  CVD config Failed");
      break;
    }

    eRetValue = AUDIO_ERROR_OK;

    if(pAudioData->hQdjHandle && reset_qdj)
    {
      //Get QDJ default parameters

      if ( qpDplQdjGetParams(pAudioData->hQdjHandle, &qdj_params ) != 0 )
      {
        IMS_MSG_ERR_0(LOG_IMS_DPL,"rtp_jb: Could not get QDJ default params");
        break;
      }


      /*------------------------------------------------------------------------
      Set the clock rate in hertz and packetization interval in milliseconds
      ------------------------------------------------------------------------*/
      qdj_params.frame_time = tCodecConfig.iFrameDuration;
      qdj_params.sampling_frequency = tCodecConfig.iClockRate * 1000;

      /*------------------------------------------------------------------------
      Set the modified values
      ------------------------------------------------------------------------*/
      if ( qpDplQdjSetParams( pAudioData->hQdjHandle, &qdj_params ) != 0 )
      {
        IMS_MSG_ERR_0(LOG_IMS_DPL,"rtp_jb: error setting QDJ params.QDJ will use its defaults");
      }  
    }
    else
    {
      IMS_MSG_ERR_0(LOG_IMS_DPL,"qpAudioCodecSet -  CVD config Failed");
      break;
    }
  }while(0);
  return eRetValue;
}


QP_IMPORT_C QPE_AUDIO_ERROR dplaudio_set_pktExchng_Mode(QP_AUDIO_DESC tAudioDescriptor, QPBOOL oobMode)
{
  QPE_AUDIO_ERROR eErr = AUDIO_ERROR_OK;
  //QP_AUDIO_DATA_AMSS* pAudioData = QP_NULL;
   /* By default the mode is set to IN Band*/
  vss_istream_cmd_set_packet_exchange_mode_t pktExchng;

  do
  {
    qpDplDecriptorData *pDplDescData = QP_NULL;

    if (QP_NULL == tAudioDescriptor)
    {
      IMS_MSG_ERR_0(LOG_IMS_DPL,"dplaudio_set_pktExchng_Mode - Invalid Param");
      eErr = AUDIO_ERROR_UNKNOWN;
      break;
    }

    pDplDescData = (qpDplDecriptorData*)tAudioDescriptor;
    //pAudioData = (QP_AUDIO_DATA_AMSS*)pDplDescData->uDecriptors.Descriptor.pDesc;

    if (eAudioULDLMode != pDplDescData->eMode)
    {
      IMS_MSG_ERR_1(LOG_IMS_DPL,"dplaudio_set_pktExchng_Mode - Invalid operation in Mode: %x", pDplDescData->eMode);
      eErr = AUDIO_ERROR_UNKNOWN;
      break;
    }

	if (TRUE == oobMode)
	{
	  pktExchng.mode = VSS_ISTREAM_PACKET_EXCHANGE_MODE_OUT_OF_BAND;
	  IMS_MSG_ERR_0(LOG_IMS_DPL,"dplaudio_set_pktExchng_Mode - Posting VSS_ISTREAM_PACKET_EXCHANGE_MODE_OUT_OF_BAND");
	}
	else
	{
	  pktExchng.mode = VSS_ISTREAM_PACKET_EXCHANGE_MODE_INBAND;
	  IMS_MSG_ERR_0(LOG_IMS_DPL,"dplaudio_set_pktExchng_Mode - Posting VSS_ISTREAM_PACKET_EXCHANGE_MODE_INBAND");	  
	}

    dplaudio_send_command( dplaudio_cvs_addr, dplaudio_app_cvs_port,
      0x12345678,
      VSS_ISTREAM_CMD_SET_PACKET_EXCHANGE_MODE,
      &pktExchng, sizeof(vss_istream_cmd_set_packet_exchange_mode_t),
      dplaudio_generic_basic_rsp_handler_cb );

    //IMS_MSG_ERR_0(LOG_IMS_DPL,"Posting VSS_ISTREAM_CMD_SET_PACKET_EXCHANGE_MODE ");

  }while(0);

  return eErr;
}


QP_IMPORT_C QPE_AUDIO_ERROR qpAudioStart_voice(QP_AUDIO_DESC tAudioDescriptor)
{

  QPE_AUDIO_ERROR eErr = AUDIO_ERROR_OK;
  QP_AUDIO_DATA_AMSS* pAudioData = QP_NULL;

  do
  {
    qpDplDecriptorData *pDplDescData = QP_NULL;

    if (QP_NULL == tAudioDescriptor)
    {
      IMS_MSG_ERR_0(LOG_IMS_DPL,"qpAudioStart - Invalid Param");
      eErr = AUDIO_ERROR_UNKNOWN;
      break;
    }

    pDplDescData = (qpDplDecriptorData*)tAudioDescriptor;
    pAudioData = (QP_AUDIO_DATA_AMSS*)pDplDescData->uDecriptors.Descriptor.pDesc;

    if (eAudioULDLMode != pDplDescData->eMode)
    {
      IMS_MSG_ERR_1(LOG_IMS_DPL,"qpAudioStart - Invalid operation in Mode: %x", pDplDescData->eMode);
      eErr = AUDIO_ERROR_UNKNOWN;
      break;
    }

    if ((eCvdStateConfigured != pDplDescData->eCvdState) || (eAudioStateConfigured != pAudioData->iCurrentState))
    {
      IMS_MSG_ERR_2(LOG_IMS_DPL,"qpAudioStart - Wrong state CS: %x  AS: %x", pDplDescData->eCvdState, pAudioData->iCurrentState);
      eErr = AUDIO_ERROR_UNKNOWN;
      break;
    }
	 av_timestamp64 = 0;

    dplaudio_send_command( dplaudio_mvm_addr, dplaudio_app_mvm_port,
      0x12345678,
			  VSS_IMVM_CMD_MODEM_START_VOICE,
      QP_NULL, 0,
      dplaudio_generic_basic_rsp_handler_cb );
			  IMS_MSG_ERR_0(LOG_IMS_DPL,"posting command VSS_IMVM_CMD_MODEM_START_VOICE");
    pDplDescData->eCvdState   = eCvdStateStartStream;
    pAudioData->iCurrentState = eAudioStateStarting;

	dplaudio_send_command( dplaudio_mvm_addr, dplaudio_app_mvm_port,
				  0x12345678,
				  VSS_IAVTIMER_CMD_GET_TIME ,
				  QP_NULL, 0,
				  dplaudio_generic_basic_rsp_handler_cb );
    IMS_MSG_ERR_0(LOG_IMS_DPL,"Posting VSS_IAVTIMER_CMD_GET_TIME ");

  }while(0);

  return eErr;
}

QP_IMPORT_C QPE_AUDIO_ERROR qpAudioStop(QP_AUDIO_DESC tAudioDescriptor)
{
  QPE_AUDIO_ERROR eErr = AUDIO_ERROR_OK;
  QP_AUDIO_DATA_AMSS* pAudioData = QP_NULL;

  do
  {
    qpDplDecriptorData *pDplDescData = QP_NULL;

    if (QP_NULL == tAudioDescriptor)
    {
      IMS_MSG_ERR_0(LOG_IMS_DPL,"qpAudioStop - Invalid Param");
      eErr = AUDIO_ERROR_UNKNOWN;
      break;
    }
    if(!cvd_event_not_ready && srvcc_tear_down_req)
	{
		IMS_MSG_ERR_0(LOG_IMS_DPL,"qpAudioStop - SRVCC tear down hold on for playout");
		break;
	}
    pDplDescData = (qpDplDecriptorData*)tAudioDescriptor;
    pAudioData = (QP_AUDIO_DATA_AMSS*)pDplDescData->uDecriptors.Descriptor.pDesc;

    if (eAudioULDLMode != pDplDescData->eMode)
    {
      IMS_MSG_ERR_1(LOG_IMS_DPL,"qpAudioStop - Invalid operation in Mode: %x", pDplDescData->eMode);
      eErr = AUDIO_ERROR_UNKNOWN;
      break;
    }

    if ((eCvdStateRun != pDplDescData->eCvdState) || (eAudioStateStarted != pAudioData->iCurrentState))
    {
      IMS_MSG_ERR_2(LOG_IMS_DPL,"qpAudioStop - Wrong state CS: %x  AS: %x", pDplDescData->eCvdState, pAudioData->iCurrentState);
      eErr = AUDIO_ERROR_UNKNOWN;
      break;
    }

    pDplDescData->eCvdState = eCvdStateStopStream;
    pAudioData->iCurrentState = eAudioStateStopping;

    dplaudio_send_command( dplaudio_mvm_addr, dplaudio_app_mvm_port,
      0x12345678,
      VSS_IMVM_CMD_MODEM_STOP_VOICE, QP_NULL, 0,
      dplaudio_generic_basic_rsp_handler_cb );

  }while(0);

       IMS_MSG_LOW_0(LOG_IMS_DPL,"qpAudioStop : clearing the event timer");
       (void)rex_clr_timer(&rex_timer_handle);
  return eErr;
}


QP_IMPORT_C QPE_AUDIO_ERROR qpAudio_tty_deregister(QP_AUDIO_DESC tAudioDescriptor)
{
	QPE_AUDIO_ERROR eErr = AUDIO_ERROR_OK;

    if (QP_NULL == tAudioDescriptor)
    {
      IMS_MSG_ERR_0(LOG_IMS_DPL,"tty_deregister - Invalid Param");
      eErr = AUDIO_ERROR_UNKNOWN;
      return eErr;
    }

//pavan - changed from mvm to cvs
    dplaudio_send_command( dplaudio_cvs_addr, dplaudio_app_cvs_port,
      0x12345678,
      VSS_ITTYOOB_CMD_DEREGISTER, QP_NULL, 0,
      dplaudio_generic_basic_rsp_handler_cb );


  return eErr;

}
QP_EXPORT_C QPE_AUDIO_ERROR qpAudioRecordStart(QP_AUDIO_DESC tAudioDescriptor)
{
  QPE_AUDIO_ERROR eRetValue = AUDIO_ERROR_UNKNOWN;

  return eRetValue;
}

QP_EXPORT_C QPE_AUDIO_ERROR qpAudioRecordStop(QP_AUDIO_DESC tAudioDescriptor)
{
  QPE_AUDIO_ERROR eRetValue = AUDIO_ERROR_UNKNOWN;

  return eRetValue;
}

QP_EXPORT_C QPE_AUDIO_ERROR qpAudioPlayStart(QP_AUDIO_DESC tAudioDescriptor)
{
  QPE_AUDIO_ERROR eRetValue = AUDIO_ERROR_UNKNOWN;

  return eRetValue;
}

QP_EXPORT_C QPE_AUDIO_ERROR qpAudioPlayStop(QP_AUDIO_DESC tAudioDescriptor, 
                                            QPBOOL bPurge)
{
  QPE_AUDIO_ERROR eRetValue = AUDIO_ERROR_UNKNOWN;

  return eRetValue;
}

QP_EXPORT_C QPE_AUDIO_ERROR qpAudioPlayFrame(QP_AUDIO_DESC tAudioDescriptor, 
                                             QP_MULTIMEDIA_FRAME* pFrameData)
{
  QPE_AUDIO_ERROR eRetValue = AUDIO_ERROR_UNKNOWN;
  QPDPL_QDJ_FRAME_TYPE qdj_frame;
  qpDplDecriptorData *pDplDescData = QP_NULL;
  QP_AUDIO_DATA_AMSS* pAudioData   = QP_NULL;
  int16 partialCopyFrameType=0, partialCopyOffset=0, rf_mode=0;
  uint32 last_tstamp = 0;

  if (QP_NULL == tAudioDescriptor)
  {
    IMS_MSG_ERR_1(LOG_IMS_DPL,"qpAudioPlayFrame - failed: %x", 0);
    return eRetValue;
  }

  if(QP_NULL == pFrameData)
  {
    IMS_MSG_ERR_1(LOG_IMS_DPL,"qpAudioPlayFrame - failed: %x", 0);
    return eRetValue;
  }

  pDplDescData = (qpDplDecriptorData*)tAudioDescriptor;  
  pAudioData = (QP_AUDIO_DATA_AMSS*)pDplDescData->uDecriptors.Descriptor.pDesc;

  if( !pDplDescData->bIssink_active )
  {
    IMS_MSG_ERR_0(LOG_IMS_DPL," Trying to queue packet when decoder is not running");
    return eRetValue;
  }
  if ((eAudioStateStarted != pAudioData->iCurrentState))
  {
    IMS_MSG_ERR_1(LOG_IMS_DPL,"qpAudioPlayFrame - failed: wrong state %x", pAudioData->iCurrentState);
    return eRetValue;
  } 

  if (eCvdStateRun != pDplDescData->eCvdState)      
  {
    IMS_MSG_ERR_1(LOG_IMS_DPL,"qpAudioPlayFrame - failed: CVD state %x", pDplDescData->eCvdState);
    return eRetValue;
  }     

  /*--------------------------------------------------------------------
  Now construct QDJ frame.
  --------------------------------------------------------------------*/
  qpDplMemset(&qdj_frame,0,sizeof(QPDPL_QDJ_FRAME_TYPE));

  qdj_frame.seq_num = (uint16) pFrameData->sMediaPacketInfo.sMediaPktInfoRx.iSeqNum;
  qdj_frame.rtp_tstamp = pFrameData->sMediaPacketInfo.sMediaPktInfoRx.iTimeStamp;
  qdj_frame.silence = pFrameData->sMediaPacketInfo.sMediaPktInfoRx.bSilence;
  qdj_frame.tw_factor = (uint16) QDJ_TW_NONE;
  qdj_frame.data = pFrameData->pData;
  qdj_frame.len = pFrameData->iDataLen;
  switch(pDplDescData->tCodecConfig.eCodec)
  {
    case AUDIO_CODEC_AMR:
    case AUDIO_CODEC_AMR_WB:
      qdj_frame.profile_info.amr_info.amr_fqi = pFrameData->amr_fqi;/*fqi bit for amr*/
      qdj_frame.profile_info.amr_info.amr_mode_request = 15;
      qdj_frame.profile_info.amr_info.amr_mode = pFrameData->sFrameInfo.sAMRInfo.iAMRModeRequest;
    break;
    case AUDIO_CODEC_EVS:
      qdj_frame.profile_info.evs_info.evs_mode = pFrameData->sFrameInfo.sEVSInfo.iEVSModeRequest;
      qdj_frame.profile_info.evs_info.evs_mode_request = pFrameData->sFrameInfo.sEVSInfo.iEVSModeRequest;  
    break;
    default:
      IMS_MSG_HIGH_1(LOG_IMS_DPL,"qpAudioPlayframe: codec = %d",pDplDescData->tCodecConfig.eCodec);
    break;
  }
  qdj_frame.frame_present = 1;
  qdj_frame.frame_info = QDJ_FT_SPEECH;

   if(qdj_frame.len < 7)
    qdj_frame.frame_info = QDJ_FT_SID;

	qpAudioAVTimeGetMs(&qdj_frame.rx_tstamp);

   if(pDplDescData->tCodecConfig.eCodec == AUDIO_CODEC_EVS && (QPE_EVS_PRIMARY_MODE)pFrameData->sFrameInfo.sEVSInfo.iEVSModeRequest == AUDIO_EVS_CODEC_MODE_4)
   {
	 /* Get the partial frame information for the 13.2kbps RF frames */
	  evs_dec_previewFrame_QDJ(qdj_frame.data, (qdj_frame.len * 8), &partialCopyFrameType, &partialCopyOffset, &rf_mode);
	  IMS_MSG_HIGH_3(LOG_IMS_DPL, "qpAudioPlayFrame: partialCopyFrameType=%d, partialCopyOffset=%d, rf_mode=%d", 
		  partialCopyFrameType, partialCopyOffset, rf_mode);  
	  if ( rf_mode == 1)
	  {
		  qdj_frame.fec_offset = partialCopyOffset;
	  }
	  else
	  {
		  qdj_frame.fec_offset = 0;
	  }
   }

  if ( !qpDplQdjEnqueue(pAudioData->hQdjHandle, 
    &qdj_frame ) )
  {

    IMS_MSG_LOW_3(LOG_IMS_DPL, "qpAudioPlayFrame: Enqueued pkt to QDJ ts=%lu, seq=%d, len=%d", 
      qdj_frame.rtp_tstamp, qdj_frame.seq_num, qdj_frame.len);    
    eRetValue = AUDIO_ERROR_OK;                 
  }
  else
  {
    eRetValue = AUDIO_ERROR_UNKNOWN;
    IMS_MSG_ERR_0(LOG_IMS_DPL,"qpAudioPlayFrame: Enqueue pkt to QDJ failed ");

  }
   if(pDplDescData->tCodecConfig.eCodec == AUDIO_CODEC_EVS && (QPE_EVS_PRIMARY_MODE)pFrameData->sFrameInfo.sEVSInfo.iEVSModeRequest == AUDIO_EVS_CODEC_MODE_4)
   {

 last_tstamp = qpDplQdjGetLastTstamp(pAudioData->hQdjHandle);
 	  IMS_MSG_HIGH_3(LOG_IMS_DPL, "qpAudioPlayFrame: last_tstamp=%d, timestamp offset=%d, rf rtp_tstamp=%d", 
		  last_tstamp, (partialCopyOffset * 20 * pDplDescData->tCodecConfig.iClockRate), (qdj_frame.rtp_tstamp - (partialCopyOffset * 20 * pDplDescData->tCodecConfig.iClockRate)));  
  if( partialCopyFrameType != RF_NO_DATA && 
	 partialCopyOffset != 0 && rf_mode == 1 && 
	 (last_tstamp < (qdj_frame.rtp_tstamp - (partialCopyOffset * 20 * pDplDescData->tCodecConfig.iClockRate))))
  {
	  qdj_frame.partialCopy = 1;
	  qdj_frame.seq_num = qdj_frame.seq_num  - partialCopyOffset;
	  qdj_frame.rtp_tstamp = qdj_frame.rtp_tstamp - partialCopyOffset * 20 * pDplDescData->tCodecConfig.iClockRate ;
	  qdj_frame.fec_offset = partialCopyOffset;
	  qdj_frame.rx_tstamp = qdj_frame.rx_tstamp - partialCopyOffset*20 - 1 ; /* set the received time stamp as the current time - delay due to offset*/ 
  
  if ( !qpDplQdjEnqueue(pAudioData->hQdjHandle, 
    &qdj_frame ) )
  {

    IMS_MSG_LOW_3(LOG_IMS_DPL, "qpAudioPlayFrame: Enqueued pkt to QDJ ts=%lu, seq=%d, len=%d", 
      qdj_frame.rtp_tstamp, qdj_frame.seq_num, qdj_frame.len);    
    eRetValue = AUDIO_ERROR_OK;                 
  }
  else
  {
    eRetValue = AUDIO_ERROR_UNKNOWN;
    IMS_MSG_ERR_0(LOG_IMS_DPL,"qpAudioPlayFrame: Enqueue pkt to QDJ failed ");

  }
  }    
   }

  return eRetValue;
}



QP_EXPORT_C QPUINT32 qpAudioCodecGetFrameSizeInBytes(QP_CODEC_CONFIG tCodecConfig)
{
  QPUINT32 iRetValue = 0;

  return iRetValue;
}

QP_EXPORT_C QPUINT32 qpAudioCodecGetFrameSizeInBits(QP_CODEC_CONFIG tCodecConfig)
{
  QPUINT32 iRetValue = 0;

  return iRetValue;
}

QP_EXPORT_C QPVOID qpAudioReleaseBuffer(QPVOID* n_pCodecFrame, 
                                        QPUINT32 n_iEncodeFramesAvailable)
{

}

QPE_AUDIO_ERROR qpDplAudioReport(QP_AUDIO_DESC tAudioDescriptor, QPUINT32 n_iMSNtpTime, QPUINT32 n_iLSNtpTime, QPUINT32 n_iRtpTimeStamp)
{

  qpVideoSendAudioReport(n_iMSNtpTime,n_iLSNtpTime,n_iRtpTimeStamp);
  return AUDIO_ERROR_OK;
}

 
QP_EXPORT_C QPE_AUDIO_ERROR qp_get_xr_qdjparam(QP_AUDIO_DESC tAudioDescriptor,QPVOID* data)
{
  qpDplDecriptorData *pDplDescData = QP_NULL;
  QPDPL_QDJ_PARAMS_TYPE  qdj_params;
  QP_AUDIO_DATA_AMSS* pAudioData = QP_NULL;
  QPDPL_QDJ_VOIP_XR_PARAMS* voip_xr_param = (QPDPL_QDJ_VOIP_XR_PARAMS*)data;
  if (QP_NULL == tAudioDescriptor)
  {
    IMS_MSG_ERR_0(LOG_IMS_DPL,"qp_get_xr_qdjparam -  Invalid Param");
    return AUDIO_ERROR_UNKNOWN;
  }
  pDplDescData = (qpDplDecriptorData*)tAudioDescriptor;
  pAudioData = (QP_AUDIO_DATA_AMSS*)pDplDescData->uDecriptors.Descriptor.pDesc;

  if (QP_NULL == pAudioData)
  {
    IMS_MSG_ERR_0(LOG_IMS_DPL,"qp_get_xr_qdjparam -  Invalid Param");
    return AUDIO_ERROR_UNKNOWN;
  }

  memset(&qdj_params, 0, sizeof(QPDPL_QDJ_PARAMS_TYPE));
 	
  if ( qpDplQdjGetParams( pAudioData->hQdjHandle, &qdj_params ) != 0 )
  {
    IMS_MSG_ERR_0(LOG_IMS_DPL,"qp_get_xr_qdjparam: Could not get QDJ default params");
       
  }  

  //populate the delay values
  voip_xr_param->tx_delay = g_iAVSyncTxDelay/1000; // in ms
  voip_xr_param->rx_delay = pDplDescData->iRxDelay/1000; // in ms
  IMS_MSG_ERR_2(LOG_IMS_DPL,"voip_xr_param tx_delay: %lu ms rx_delay = %Lu us",voip_xr_param->tx_delay, pDplDescData->iRxDelay);
  
  voip_xr_param->JB_maximum = qdj_params.max_dejitter_delay;
  voip_xr_param->JB_abs_max = qdj_params.max_dejitter_delay;

  if( !qpDplQdjGetXRParams( pAudioData->hQdjHandle,voip_xr_param))
  {
    IMS_MSG_ERR_0(LOG_IMS_DPL,"qp_get_xr_qdjparam -  Failed to get parameters");
  }
  voip_xr_param->plc = 0; /*more investigation needed*/
  voip_xr_param->jba = QVP_RTP_JBA_ADAPTIVE;
  voip_xr_param->jb_rate = 0;/*more investigation needed*/
  return AUDIO_ERROR_OK;
}

QP_IMPORT_C QPE_AUDIO_ERROR qpAudioChangeTaskOnFrameDelivery()
{
  return AUDIO_ERROR_OK;
}

QPE_AUDIO_ERROR qpDplAudioEmptyJitterBuffer(QP_AUDIO_DESC tAudioDescriptor)
{
  qpDplDecriptorData *pDplDescData = QP_NULL;
  QP_AUDIO_DATA_AMSS* pAudioData = QP_NULL;
  QPDPL_QDJ_PARAMS_TYPE  qdj_params;
  QPE_AUDIO_ERROR eRetValue = AUDIO_ERROR_UNKNOWN;

  do{
    if (QP_NULL == tAudioDescriptor)
    {
      IMS_MSG_ERR_0(LOG_IMS_DPL,"qpDplAudioEmptyJitterBuffer - failed: wrong parameter");
      break;
    }

    pDplDescData = (qpDplDecriptorData*)tAudioDescriptor;
    pAudioData = (QP_AUDIO_DATA_AMSS*)pDplDescData->uDecriptors.Descriptor.pDesc;

    if (QP_NULL == pAudioData)
    {
      IMS_MSG_ERR_0(LOG_IMS_DPL,"qpDplAudioEmptyJitterBuffer - failed: wrong parameter");
      break;
    }

    if (eAudioULDLMode != pDplDescData->eMode)
    {
      IMS_MSG_ERR_1(LOG_IMS_DPL,"qpDplAudioEmptyJitterBuffer - Invalid operation in Mode: %x", pDplDescData->eMode);
      break;
    }

    if(pAudioData->hQdjHandle)
    {
      IMS_MSG_LOW_0(LOG_IMS_DPL, "qpDplAudioEmptyJitterBuffer: Deleting the old QDJ");
      qpDplQdjDestroyHandle(pAudioData->hQdjHandle);
	  pAudioData->hQdjHandle = NULL;
    }

    pAudioData->hQdjHandle = qpDplQdjCreateHandle();

    if(!pAudioData->hQdjHandle)
    {
      IMS_MSG_FATAL_0(LOG_IMS_DPL,"qpDplAudioEmptyJitterBuffer - QDJ Init Failed");
      break;
    }

    //Get QDJ default parameters
    if ( qpDplQdjGetParams(pAudioData->hQdjHandle, &qdj_params ) != 0 )
    {
      IMS_MSG_ERR_0(LOG_IMS_DPL,"qpDplAudioEmptyJitterBuffer: Could not get QDJ default params");
      break;
    }  

    /*------------------------------------------------------------------------
    Set the clock rate in hertz and packetization interval in milliseconds
    ------------------------------------------------------------------------*/
    qdj_params.frame_time = pDplDescData->tCodecConfig.iFrameDuration;
    qdj_params.sampling_frequency = pDplDescData->tCodecConfig.iClockRate * 1000;

    /*------------------------------------------------------------------------
    Set the modified values
    ------------------------------------------------------------------------*/
    if ( qpDplQdjSetParams( pAudioData->hQdjHandle, &qdj_params ) != 0 )
    {
      IMS_MSG_ERR_0(LOG_IMS_DPL,"qpDplAudioEmptyJitterBuffer: error setting QDJ params.QDJ will use its defaults");
    }  
    eRetValue = AUDIO_ERROR_OK;
  }while(0);
  return eRetValue;
}

QPVOID qpDplLogCodecEvents(uint8 Direction, uint8 Codec, uint8 Codecrate, QPUINT16 evt)
{
  qvp_codecinit_codecrate_change_event_info_type  event_info_type;
  event_info_type.ChannelID = 0;//we have decided to go with zero as of now
  event_info_type.Direction = Direction;
  event_info_type.Codec = Codec;
  event_info_type.Codecrate = Codecrate;
  qpLogEventPayloadRecord(evt, (const QPVOID*)(&event_info_type), sizeof(qvp_codecinit_codecrate_change_event_info_type));
} 


QPVOID qpDplLogAudioDelayEvents(uint64 TxDelay, uint32 RxDelay)
{
  QPUINT16 evt = EVENT_AUDIO_DELAY;

  if(TxDelay)
  {
  	  IMS_MSG_HIGH_2(LOG_IMS_DPL,"qpDplLogAudioDelayEvents TxDelay = 0x%.8x%.8x ",(uint32)(TxDelay>>32),(uint32)TxDelay);
	  qpDplMemscpy(&audiopath_delay_event_info_type.TxDelay, sizeof(audiopath_delay_event_info_type.TxDelay), &TxDelay, 3 * sizeof(uint8));
  }
  if(RxDelay)
  {
  	  IMS_MSG_HIGH_1(LOG_IMS_DPL,"qpDplLogAudioDelayEvents RxDelay = 0x%.8x ",RxDelay);
  	  qpDplMemscpy(&audiopath_delay_event_info_type.RxDelay, sizeof(audiopath_delay_event_info_type.RxDelay), &RxDelay, 3 * sizeof(uint8));
  }

  if( (audiopath_delay_event_info_type.TxDelay[0] || audiopath_delay_event_info_type.TxDelay[1] || audiopath_delay_event_info_type.TxDelay[2]) &&
  	(audiopath_delay_event_info_type.RxDelay[0] || audiopath_delay_event_info_type.RxDelay[1] || audiopath_delay_event_info_type.RxDelay[2] ))
  {
  	//IMS_MSG_HIGH_3(LOG_IMS_DPL,"qpDplLogAudioDelayEvents Log The event. TxDelay= 0x%.2x%.2x%.2x ",audiopath_delay_event_info_type.TxDelay[2],audiopath_delay_event_info_type.TxDelay[1],audiopath_delay_event_info_type.TxDelay[0]);  
  	//IMS_MSG_HIGH_3(LOG_IMS_DPL,"qpDplLogAudioDelayEvents Log The event. RxDelay= 0x%.2x%.2x%.2x ",audiopath_delay_event_info_type.RxDelay[2],audiopath_delay_event_info_type.RxDelay[1],audiopath_delay_event_info_type.RxDelay[0]);    	
  	qpLogEventPayloadRecord(evt, (const QPVOID*)(&audiopath_delay_event_info_type), sizeof(qvp_audiopath_delay_change_event_info_type));
  }

  return;
} 

QPE_AUDIO_ERROR qpAudioStart(QP_AUDIO_DESC tAudioDescriptor)
{ 
  if (NULL == tAudioDescriptor)
  {
	return AUDIO_ERROR_UNKNOWN;
  }
	g_uplink_pkt_count = 0;
	g_uplink_silence_pkt_count = 0;
	g_uplink_speech_pkt_count = 0;
	g_uplink_no_data_pkt_count = 0;
	g_downlink_pkt_count = 0;
	g_downlink_silence_pkt_count = 0;
	g_downlink_speech_pkt_count = 0;
	g_downlink_no_data_pkt_count = 0;
  //pavan- changed mvm to cvs
  dplaudio_send_command( dplaudio_cvs_addr, dplaudio_app_cvs_port,
	0x12345678,
	VSS_ITTYOOB_CMD_REGISTER, NULL,
	0,
	dplaudio_generic_basic_rsp_handler_cb );

  return AUDIO_ERROR_OK;

}

QPE_AUDIO_ERROR qvp_rtp_send_rx_char(uint16 ttychar, QP_AUDIO_DESC tAudioDescriptor, uint32 token)
{
  vss_ittyoob_cmd_rx_char_t payload = {ttychar}; 
  //IMS_MSG_LOW_0(LOG_IMS_DPL,"Entered qvp_rtp_send_rx_char");
  if (NULL == tAudioDescriptor)
  {
	return AUDIO_ERROR_UNKNOWN;
  }
  IMS_MSG_HIGH_1(LOG_IMS_DPL,"sendng VSS_ITTYOOB_CMD_SEND_RX_CHAR cmd to cvd. ttychar:%u",ttychar);
  //pavan - changed mvm to cvs
  dplaudio_send_command( dplaudio_cvs_addr, dplaudio_app_cvs_port,
	token,
	VSS_ITTYOOB_CMD_SEND_RX_CHAR, &payload, 
	sizeof(vss_ittyoob_cmd_rx_char_t),
	dplaudio_generic_basic_rsp_handler_cb );

  return AUDIO_ERROR_OK;
}

static void dplaudio_update_tty_state(boolean state)
{
  qpDplDecriptorData *pDplDescData = QP_NULL;
  QPC_GLOBAL_DATA* pGlobalData = QP_NULL;
  QP_AUDIO_DATA_AMSS* pAudioData = QP_NULL;
  pGlobalData = qpDplGetGlobalData();
  if (pGlobalData) 
  {
    pDplDescData = (qpDplDecriptorData*)pGlobalData->pAudioData;
    if (pDplDescData) 
	{
      pAudioData = (QP_AUDIO_DATA_AMSS*)pDplDescData->uDecriptors.Descriptor.pDesc;
      if (QP_NULL != pAudioData) 
	  {
        if (QP_NULL != pAudioData->tAudioCallBack) 
		{
          pAudioData->tAudioCallBack(AUDIO_MSG_TTY_ENABLED, &state, 0, pAudioData->pUserData);
        }
      }
    }
  }
  else 
  {
    IMS_MSG_ERR_0(LOG_IMS_DPL, "dplaudio_update_tty_state ...pDplDescData  is NULL");
  }
}



static void dplaudio_process_tty_tx (
  aprv2_packet_t* packet
  )
{
  uint16 payload_size = 0;
  QPC_GLOBAL_DATA* pGlobalData  = QP_NULL;
  QP_AUDIO_DATA_AMSS* pAudioData = QP_NULL;
  qpDplDecriptorData *pDplDescData = QP_NULL;
  qvp_rtp_codec_t140 *pkt = NULL;
  uint64 ts;
  uint8 *payload = NULL;

  //IMS_MSG_LOW_0(LOG_IMS_DPL," entering -> dplaudio_process_tty_tx()");

  if (NULL == packet)
  {
    IMS_MSG_ERR_0(LOG_IMS_DPL,"dplaudio_process_tty_tx:packet is NULL");
    return;
  }

  pGlobalData = qpDplGetGlobalData();
  if (pGlobalData)
  {
    pDplDescData = (qpDplDecriptorData *)pGlobalData->pAudioData;
    if(pDplDescData)
    {
      pAudioData = (QP_AUDIO_DATA_AMSS*) pDplDescData->uDecriptors.Descriptor.pDesc;
	  if(QP_NULL == pAudioData)
	  {
		IMS_MSG_ERR_0(LOG_IMS_DPL, "dplaudio_process_tty_tx ...pDplDescData  is NULL");
		return;
	  }
    }
    else
    {
      IMS_MSG_ERR_0(LOG_IMS_DPL, "dplaudio_process_tty_tx ...pDplDescData  is NULL");
    }
  }
  else
  {
    IMS_MSG_ERR_0(LOG_IMS_DPL, "dplaudio_process_tty_tx ...pGlobalData  is NULL");
  } 
  payload_size = APRV2_PKT_GET_PAYLOAD_BYTE_SIZE( packet->header );
  //IMS_MSG_HIGH_1(LOG_IMS_DPL,"payload size : %d ",payload_size);
  //payload_size -= sizeof( vss_ittyoob_evt_tx_char_t );

  IMS_MSG_MED_1(LOG_IMS_DPL,"dplaudio_process_tty_tx -> payload size = %d", payload_size);
  if (payload_size == QVP_RTP_T140_LEN)
  {
	payload =  APRV2_PKT_GET_PAYLOAD( void, packet );
	if (payload == NULL)
	{
		IMS_MSG_ERR_0(LOG_IMS_DPL,"payload is NULL");
	}
	else
	{
	qvp_rtp_time_get_ms(&ts);
	pkt = (qvp_rtp_codec_t140*)qpDplMalloc(MEM_IMS_DPL, sizeof(qvp_rtp_codec_t140));

	if (pkt == NULL)
	{
		IMS_MSG_ERR_0(LOG_IMS_DPL,"pkt is NULL");
	}
	else
  {
	pkt->dataLen = QVP_RTP_T140_LEN;
	pkt->data = ((vss_ittyoob_evt_tx_char_t*)payload)->tty_char;
	pkt->timeStamp = (uint32)ts;
	IMS_MSG_HIGH_1(LOG_IMS_DPL,"dplaudio_process_tty_tx: received character -> 0x%x ",pkt->data);
  if (NULL != pAudioData && NULL != pAudioData->tAudioCallBack)
  {
      IMS_MSG_MED_0(LOG_IMS_DPL,"dplaudio_process_tty_tx ->calling callback ");
		  pAudioData->tAudioCallBack(AUDIO_MSG_TTY_TX_DATA, &pkt->data,0, pAudioData->pUserData);
  }
  else
  {
    IMS_MSG_HIGH_0(LOG_IMS_DPL,"pAudioData is NULL");
  }
  }
	}
  }
  qpDplFree(MEM_IMS_DPL,pkt);
  __aprv2_cmd_free( dplaudio_apr_handle, packet );
	}

static void dplaudio_map_outofband_memory(qpDplDecriptorData* pDplDescData)
{

  voicemem_cmd_alloc_default_t voicemem_local_alloc_args;
  vss_imemory_cmd_map_physical_t physical_mapping_mem_args;
  vss_imemory_table_descriptor_t physical_table_descriptor_args;
  vsd_status_t status;
  QPUINT8 * enc_dec_buf_virt_addr = NULL;
  vss_imemory_block_t data_mem_block;
  vss_imemory_table_t mem_block_table;
  qurt_addr_t phys_addr;
  
  if(!pDplDescData)
    return;
   
   status = voicemem_call(VOICEMEM_CMD_INIT,NULL,0);
   
   if(VSD_EOK != status)
   {
	 IMS_MSG_ERR_1(LOG_IMS_DPL,"voicemem_call Failed VOICEMEM_CMD_INIT %d", status);
	 /*if (pAudioData->tAudioCallBack)
	 {
		pAudioData->tAudioCallBack(AUDIO_MSG_ERROR, QP_NULL, pAudioData->eADev, pAudioData->pUserData);
	 }
	 else
	 {
	   ASSERT(0)
	 }*/
	 //TODO
   }
   else
   {
     memset(&voicemem_local_alloc_args, 0, sizeof(voicemem_cmd_alloc_default_t));
     /* Allocate 8KB, 4KB for descriptors and 4 KB for Data, Since Modem Page
     is 4k Aligned we needed it to be 4KB evenif we consume Less **/
	   voicemem_local_alloc_args.size = 	8192; 
    /** it seems we only need 24 bytes for Table descriptor, so we can try only
    allocating 4kB -- TODO after first cut **/
    /* Return handle of the allocated memory region */
    voicemem_local_alloc_args.ret_voicemem_handle = &pDplDescData->voicemem_handle;  
    /* Returned virtual start address of the allocated memory region */
    voicemem_local_alloc_args.ret_virt_addr = &pDplDescData->base_virt_addr;  

  	if(g_rat_type == AUDIO_RAT_IWLAN)
    {
      qpDplMemscpy(voicemem_local_alloc_args.client_name, 31, dplaudio_cvs_session_name_wlan, sizeof( dplaudio_cvs_session_name_wlan) );
    }
    else
    {
      qpDplMemscpy(voicemem_local_alloc_args.client_name, 31, dplaudio_cvs_session_name, sizeof( dplaudio_cvs_session_name ) );
    }
  
	 status = voicemem_call(VOICEMEM_CMD_ALLOC_DEFAULT, &voicemem_local_alloc_args, sizeof(voicemem_local_alloc_args));
		
	 if(VSD_EOK != status)
	 {
	   IMS_MSG_ERR_1(LOG_IMS_DPL,"voicemem_call Failed VOICEMEM_CMD_INIT %d", status);
	   /*if (pAudioData->tAudioCallBack)
	   {
		  pAudioData->tAudioCallBack(AUDIO_MSG_ERROR, QP_NULL, pAudioData->eADev, pAudioData->pUserData);
	   }
	   else
	   {
		 ASSERT(0)
	   }*/
	   //TODO
	 }
    {

      enc_dec_buf_virt_addr = (QPUINT8 *)pDplDescData->base_virt_addr;
      g_base_virt_addr = pDplDescData->base_virt_addr;
      g_voicemem_handle = pDplDescData->voicemem_handle;
      enc_dec_buf_virt_addr += 4096;
      phys_addr = qurt_lookup_physaddr( ( ( qurt_addr_t ) enc_dec_buf_virt_addr ) ); /* Do a lookup for the phy addr. */
      if ( phys_addr == 0 )
      {
        IMS_MSG_ERR_1( LOG_IMS_DPL, "qurt_lookup_physaddr(): Unable to get the phys_addr for enc_dec_buf_virt_addr=0x%016X",
                                    enc_dec_buf_virt_addr);
      }
      data_mem_block.mem_address = phys_addr; /* Init memory block. */
      data_mem_block.mem_size = 4096;
      
      mem_block_table.next_table_descriptor.mem_size = 0;
      qpDplMemscpy( ( QPUINT8*) pDplDescData->base_virt_addr, sizeof(vss_imemory_table_t),
                     &mem_block_table, sizeof(vss_imemory_table_t) );
      qpDplMemscpy( ( (QPUINT8*)pDplDescData->base_virt_addr+sizeof(vss_imemory_table_descriptor_t) ), sizeof(vss_imemory_block_t),
                      &data_mem_block,
                      sizeof(vss_imemory_block_t));
					  

      {
        IMS_MSG_ERR_1( LOG_IMS_DPL, "voicemem_call(): pDplDescData->base_virt_addr=0x%X",pDplDescData->base_virt_addr);
        phys_addr = qurt_lookup_physaddr( ( ( qurt_addr_t ) pDplDescData->base_virt_addr ) );
        if ( phys_addr == 0 )
        {
          IMS_MSG_ERR_1( LOG_IMS_DPL, "qurt_lookup_physaddr(): Unable to get the phys_addr for base_virt_addr=0x%016X",
            pDplDescData->base_virt_addr);
        }
        physical_table_descriptor_args.mem_address = pDplDescData->mapped_phys_addr = phys_addr;
        physical_table_descriptor_args.mem_size = ( sizeof(vss_imemory_table_t) + sizeof(vss_imemory_block_t) );
        /* check with audio team on this */
      }
      physical_mapping_mem_args.table_descriptor = physical_table_descriptor_args;
      physical_mapping_mem_args.is_cached = TRUE;
      physical_mapping_mem_args.cache_line_size = 128;
      physical_mapping_mem_args.access_mask = 1;
      physical_mapping_mem_args.page_align = 4096;
      physical_mapping_mem_args.min_data_width = 8;
      physical_mapping_mem_args.max_data_width = 64;	 
      dplaudio_send_command( dplaudio_mvm_addr, dplaudio_app_mvm_port,
        0x12345678,
        VSS_IMEMORY_CMD_MAP_PHYSICAL,
        &physical_mapping_mem_args, sizeof( vss_imemory_cmd_map_physical_t ),
        dplaudio_generic_basic_rsp_handler_cb );	  
    }
   }
 }

QPE_AUDIO_ERROR qpAudioClearAudioResource(QP_AUDIO_DESC tAudioDescriptor)
{
    QPC_GLOBAL_DATA* pGlobalData  = QP_NULL;
    pGlobalData = qpDplGetGlobalData();
    if (pGlobalData)
    {
      QP_AUDIO_DATA_AMSS* pAudioData = QP_NULL;
      qpDplDecriptorData *pDplDescData = QP_NULL;
      //dplaudio_process_apr_cb(pGlobalData);
      pDplDescData = (qpDplDecriptorData *)pGlobalData->pAudioData;
      if (pDplDescData)
      {	
        pAudioData = (QP_AUDIO_DATA_AMSS*) pDplDescData->uDecriptors.Descriptor.pDesc;
        if (pAudioData)
        {
					dplaudio_process_apr_cb(pGlobalData);
					if(pAudioData->hQdjHandle)
					{
					  if(0 != qpDplQdjDestroyHandle(pAudioData->hQdjHandle))
						IMS_MSG_ERR_0(LOG_IMS_DPL,"qpAudioClearAudioResource - QDJ De init failed:");
					  pAudioData->hQdjHandle = NULL;
					}		
			          IMS_MSG_LOW_0(LOG_IMS_DPL,"calling dpl_aud_cleanup");
			  
			  {
          vsd_status_t status;
				  voicemem_cmd_free_t voicemem_cmd_config;
		          voicemem_cmd_config.voicemem_handle = pDplDescData->voicemem_handle;
		           status = voicemem_call(VOICEMEM_CMD_FREE,&voicemem_cmd_config,sizeof(voicemem_cmd_free_t));	
		          if(VSD_EOK != status)
		          {
		            IMS_MSG_ERR_0(LOG_IMS_DPL,"VOICEMEM_CMD_FREE failed");
		          }
		          status = voicemem_call(VOICEMEM_CMD_DEINIT,NULL,0);
				  if(VSD_EOK != status)
		          {
		            IMS_MSG_ERR_0(LOG_IMS_DPL,"VOICEMEM_CMD_DEINIT failed");		
				  }
	          }
			          dpl_aud_cleanup(pDplDescData);    
				        /* Deinitialize the handle table. */
				      apr_objmgr_destruct( &dplaudio_objmgr );
				      apr_lock_destroy( dplaudio_lock );		  
        }
      }
    }
  return AUDIO_ERROR_OK;
}


 QPVOID dplaudio_G711_codec_change_response(void)
{
  QPC_GLOBAL_DATA* pGlobalData = QP_NULL;
  
  QP_AUDIO_DATA_AMSS* pAudioData = QP_NULL;
  qpDplDecriptorData *pDplDescData = QP_NULL;

  pGlobalData = qpDplGetGlobalData();
  do
  {  
    if (NULL == pGlobalData)
    {
      IMS_MSG_ERR_0( LOG_IMS_DPL, "dplaudio_G711_codec_change_response: pGlobalData is NULL");
      break;
    }

    pDplDescData = (qpDplDecriptorData *)pGlobalData->pAudioData;
    if (NULL == pDplDescData)
    {
    
      IMS_MSG_ERR_0( LOG_IMS_DPL, "dplaudio_G711_codec_change_response: pDplDescData is NULL");
      break;
    }
        pAudioData = (QP_AUDIO_DATA_AMSS*) pDplDescData->uDecriptors.Descriptor.pDesc;

    if(NULL == pAudioData)
    {
      IMS_MSG_ERR_0( LOG_IMS_DPL, "dplaudio_G711_codec_change_response: pAudioData is NULL");
      break;
    }	
    if (pAudioData->tAudioCallBack)
    {
      IMS_MSG_HIGH_0(LOG_IMS_DPL,"Posting Codec Changed");
      pAudioData->tAudioCallBack(AUDIO_MSG_CODEC_CHANGED, QP_NULL, pAudioData->eADev, pAudioData->pUserData);
    }

  }
while(0);

  return;
}
QPBOOL qpAudioSetQdjThresh(uint8 pSilence, uint8 pSpeech, qp_audio_metirc_cb pCbfptr)
{
	QPC_GLOBAL_DATA* pGlobalData = QP_NULL;
  QP_AUDIO_DATA_AMSS* pAudioData = QP_NULL;
  qpDplDecriptorData *pDplDescData = QP_NULL;
 
  pGlobalData = qpDplGetGlobalData();
  
  if (NULL == pGlobalData)
  {
    return FALSE;
  }
	  
  pDplDescData = (qpDplDecriptorData *)pGlobalData->pAudioData;
  if (NULL == pDplDescData)
	{
    return FALSE;
  }
	pAudioData = (QP_AUDIO_DATA_AMSS*) pDplDescData->uDecriptors.Descriptor.pDesc;

	if(NULL == pAudioData || NULL == pAudioData->hQdjHandle)
  {
    return FALSE;
  }

  qpDplQdjSetMetricTh(pAudioData->hQdjHandle,pSilence, pSpeech, pCbfptr);

  return TRUE;

}

  /**
  * \fn QP_IMPORT_C QPE_AUDIO_ERROR qpAudioupdateRedoffset(QP_AUDIO_DESC tAudioDescriptor, uint32 fec_offset, uint32 fer_rate);
  * \brief updates redundancy offset in channel aware mode
  *
  * This function updates FEC offset 
   * in channel aware mode.   
   * \param tAudioDescriptor Audio Descriptor.\n
  * \param fec_offset @values 2, 3, 5, 7.\n
  * \param fer_rate Specifies FER rate threshold to LOW (0) or HIGH.\n
  * \param ebw specifies bw.\n
  * See \ref QP_AUDIO_DESC.
  * \return QPE_AUDIO_ERROR 
   * \retval AUDIO_ERROR_OK
  * \retval AUDIO_ERROR_UNKNOWN
  * \remarks None
  */
  QP_EXPORT_C QPE_AUDIO_ERROR qpAudioUpdateRedOffset(QP_AUDIO_DESC tAudioDescriptor,
                                  uint32 fec_offset, uint32 fer_rate, QPE_EVS_BW ebw)
  {
    QP_EVS_CODEC_INFO *evs = NULL;
    QPC_GLOBAL_DATA* pGlobalData  = QP_NULL;
    QP_AUDIO_DATA_AMSS* pAudioData = QP_NULL;
    qpDplDecriptorData *pDplDescData = QP_NULL;
    pGlobalData = qpDplGetGlobalData();

    IMS_MSG_ERR_0(LOG_IMS_DPL,"EVS:qpAudioUpdateRedOffset-entered");
    if (NULL ==pGlobalData)
    {
      IMS_MSG_ERR_0(LOG_IMS_DPL,"EVS:GlobalData is NULL");
	  return AUDIO_ERROR_UNKNOWN; 
    }
    pDplDescData = (qpDplDecriptorData *)pGlobalData->pAudioData;
    if (NULL ==pDplDescData)
    {
      IMS_MSG_ERR_0(LOG_IMS_DPL,"EVS:AudioDesc in GlobalData is NULL");
	  return AUDIO_ERROR_UNKNOWN; 
    }
    pAudioData = (QP_AUDIO_DATA_AMSS*) pDplDescData->uDecriptors.Descriptor.pDesc;
    if (NULL == pAudioData)
    {
       IMS_MSG_ERR_0(LOG_IMS_DPL,"EVS:AudioData is NULL");
       return AUDIO_ERROR_UNKNOWN;
    }
    /* check if bandwidth has changed - this will be the trigger to start*/
    if(NULL == tAudioDescriptor)
    {
       IMS_MSG_ERR_0(LOG_IMS_DPL,"EVS:qpAudioUpdateRedOffset - invalid params");
       return AUDIO_ERROR_UNKNOWN;
    }
    if(AUDIO_CODEC_EVS != pDplDescData->tCodecConfig.eCodec)
    {
      IMS_MSG_ERR_0(LOG_IMS_DPL,"EVS:qpAudioUpdateRedOffset - not evs mode ");
    }
    evs = &pDplDescData->tCodecConfig.sCodecInfo.sEVSInfo;
    if(evs->tx_fer == fer_rate && evs->tx_evs_red_offset == fec_offset &&
       evs->eTxBW == ebw)
    {
      IMS_MSG_ERR_0(LOG_IMS_DPL,"EVS:same values nothing to be changed");
      return AUDIO_ERROR_OK;
    }
    /*this is the case where we need to reconfigure so reset the current
      codec to somehing else and new codec to the evs trick it into
      reconfigure everything */
    //pDplDescData->tCodecConfig.sCodecInfo.eCodec = AUDIO_CODEC_AMR_WB;
    evs = &pDplDescData->tCodecConfig.sCodecInfo.sEVSInfo;
    evs->eTxBW = ebw;
    evs->tx_evs_red_offset = fec_offset;
    evs->tx_fer = fer_rate;
    dpl_cvd_standby_before_reconfig(pDplDescData);
    return AUDIO_ERROR_OK; 
  }

  void qpDplSetEVSEncMode(qpDplDecriptorData *pDescp, 
                            QPE_EVS_PRIMARY_MODE pMode, 
                            QPE_EVS_BW pBandWidth)
  {
    vss_istream_cmd_set_evs_voc_enc_operating_mode_t evs_mode;
    
    IMS_MSG_HIGH_3(LOG_IMS_DPL,"qpDplSetEvSEncode - mode= %x, badwidth = %d, cvdMode = %d",
                             pMode, pBandWidth, sCVDEvsCodecRates[pMode]);

    evs_mode.mode = sCVDEvsCodecRates[pMode];/* mapping from table*/
    switch(pBandWidth)
    {
      case AUDIO_BW_NB:
        evs_mode.bandwidth = VSS_ISTREAM_EVS_VOC_BANDWIDTH_NB;
        break;
      case AUDIO_BW_SWB:
        evs_mode.bandwidth = VSS_ISTREAM_EVS_VOC_BANDWIDTH_SWB;
        break;
      case AUDIO_BW_FB:
        evs_mode.bandwidth = VSS_ISTREAM_EVS_VOC_BANDWIDTH_FB;
        break;
      default:
        pBandWidth = AUDIO_BW_WB;
      case AUDIO_BW_WB:
        evs_mode.bandwidth = VSS_ISTREAM_EVS_VOC_BANDWIDTH_WB;
        break;
    }
    IMS_MSG_HIGH_0(LOG_IMS_DPL,"Posting event - VSS_ISTREAM_CMD_SET_EVS_VOC_ENC_OPERATING_MODE");
    dplaudio_send_command( dplaudio_cvs_addr, dplaudio_app_cvs_port,
      0x12345678,
      VSS_ISTREAM_CMD_SET_EVS_VOC_ENC_OPERATING_MODE,
      &evs_mode, sizeof( vss_istream_cmd_set_evs_voc_enc_operating_mode_t ),
      dplaudio_generic_basic_rsp_handler_cb );
      pDescp->eCvdState = eCvdStateConfigCodecProp;
      g_PrevRxMode = 100;

      qpDplLogCodecEvents(0 /* 0 - rx direction */, 4 /* 4 - EVS */, pMode, EVENT_DPL_CODEC_INIT);
      qpDplLogCodecEvents(1 /* 1 - Tx direction */, 4 /* 4 - EVS */, pMode, EVENT_DPL_CODEC_INIT);
  }