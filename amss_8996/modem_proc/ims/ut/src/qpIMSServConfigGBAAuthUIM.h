/************************************************************************
 Copyright (C) 2013 Qualcomm Technologies Incorporation .All Rights Reserved.

 File Name      : qpIMSServConfigGBAAuthUIM.h
 Description    : IMS Sevice Configuration 

 Revision History
 ========================================================================
   Date      |   Author's Name    |  BugID  |        Change Description
 ========================================================================
-------------------------------------------------------------------------------
12-May-2015   Sreenidhi     789667      FR25901: Ut changes for migrating IMS over to UIM GBA solution
************************************************************************/

#ifndef __QPIMSSERVCONFIGGBAAUTHUIM_H__
#define __QPIMSSERVCONFIGGBAAUTHUIM_H__

#include <qpPlatformConfig.h>
#include "qpIMSServConfigDefines.h"
#include "qpIMSServConfigGBAUIMDefines.h"
#include "QimfHttp.h"

class IMSServConfigGBAAuthUIM
{
public:
  IMSServConfigGBAAuthUIM();
  ~IMSServConfigGBAAuthUIM();

  QPBOOL GenerateUaAuthHeader(QP_IMS_SERV_CONFIG_GBA_UA_AUTH_PARAMS*);
  QPBOOL ValidateGBA2XXResponse(QPCHAR*, QPCHAR*);
  QPBOOL ValidateAuthenticationInterface(QP_IMS_SERV_CONFIG_UA_ON_REQ_CHALLENGE_PARAMS&, const QPCHAR*, QPTLS_SESS_PROFILE*);
  QPBOOL IsQopTypeAuthInt();
  QPBOOL IsNextNoncePresent();
  QPVOID ResetPreviousAuthParams();
  QPVOID SetGBATLSMode(QPE_IMS_SERV_CONFIG_GBA_TLS_MODE);
  QPVOID GetUaAuthParams(QP_AUTH_PARAMS*);
  

private:
  //Copy Constructor and Operator = 
  IMSServConfigGBAAuthUIM(const IMSServConfigGBAAuthUIM&);
  IMSServConfigGBAAuthUIM& operator=(const IMSServConfigGBAAuthUIM&);

  QPBOOL  ParseAuthHeaderAndFillChallengeStruct(const QPCHAR*, QP_AUTH_PARAMS&);
  QPVOID  ResetChallengeStruct(QP_AUTH_PARAMS&);
  QPVOID  ParseRspAuthParam(QPCHAR*, QPCHAR*, QP_AUTH_PARAMS&);
  QPVOID  UpdateChallengeParams(QPCHAR*, QP_AUTH_PARAMS&, QP_SIP_DIGEST_AUTH_PARAM);
  QPCHAR* ExtractParamsFromHeader(QPCHAR*);
  QPCHAR* RemoveQoutes(const SipString*);
  QPVOID  ResetNextChallengeStruct(QP_IMS_SERV_CONFIG_NEXT_CHALLENGE_PARAMS&);
  QPVOID  UpdateNextChallengeParams(QPCHAR* n_pParam, QPCHAR** n_pInput);
  QPVOID  CopyReceivedChallengeParams();
  QPBOOL  ValidateServersRealm(QPCHAR*, QPTLS_SESS_PROFILE*);
  QPCHAR* ExpandInIPv6Format(QPCHAR*);
  QPUINT  QpIsAlphaNum(QPUINT c);

private:
  QPBOOL                                    m_bUaOnReqNoncePresent;
  QP_IMS_SERV_CONFIG_NEXT_CHALLENGE_PARAMS  m_stUaNNonceParams;
  QP_AUTH_PARAMS                            m_stGBAUaAuthParams;
  QP_AUTH_PARAMS                            m_stOnReqUaAuthParams;
  QPBOOL                                    m_bGAAGBAAuthCalcInProgress;
  QPE_IMS_SERV_CONFIG_GBA_TLS_MODE          m_eGBATLSMode;
  QPINT8                                    m_iOprtMode ;
};

#endif //__QPIMSSERVCONFIGGBAAUTH_H__
