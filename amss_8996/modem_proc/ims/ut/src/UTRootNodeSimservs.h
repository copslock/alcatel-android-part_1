/************************************************************************
 Copyright (C) 2013 Qualcomm Technologies Incorporation .All Rights Reserved.

 File Name      : UTRootNodeSimservs.h
 Description    : Simservs Root Node for SS
 

 ========================================================================
    Date    |   Author's Name    |  BugID  |        Change Description
 ========================================================================
 02-12-2013     Priyank            539082     Perform a GET operationof the entire server document initially to avoid rule id mismatch
 --------------------------------------------------------------------------------------------------
 29-01-2014     Priyank            594699     Number specific Incoming Call Barring Related XCAP Configuration
 -------------------------------------------------------------------------------
 29-01-2014     Sreenidhi          605560     At boot-up perform GET at simservs node for all the Requests
 --------------------------------------------------------------------------------------------------
 05-02-2014     Priyank            605477     Ut marshal/unmarshal support of namespace at service level
  ------------------------------------------------------------------------------- 
 15-05-2014     Priyank             663757     UE crashes if CDIV has CFU in activated state and media element present
---------------------------------------------------------------------------------------------
20-July-2014   asharm             684864    FR 21910: CMCC Media and Timer based requirement for call forwarding supplementary servcies
---------------------------------------------------------------------------------------------
04-Aug-2014   asharm              699306    CFC register is erasing cdiv node and adding only CFB rule id
---------------------------------------------------------------------------------------------
30-Sept-2014   asharm             710919   Global operator mode is ATT but still audio media tag is added; which should not be
--------------------------------------------------------------------------------------------------
10-Aug-2015     Sreenidhi      878829  FR 29171: Support <media> Element in Ut/XCAP XML Document of Call Diversion
************************************************************************/

#ifndef __UTROOTNODESIMSERVS_H__
#define __UTROOTNODESIMSERVS_H__

#include "UTXMLSSHandler.h"
#include "UTBaseCommunicationBarring.h"
#include "UTBaseCommunicationDiversion.h"
#include <qpPlatformConfig.h>

class qp_Simservs;
class qp_communication_diversion;
class qp_ns_common_policy_conditionsType;
class qp_ns_common_policy_extensibleType;
class qp_incoming_communication_barring;
class qp_outgoing_communication_barring;
class QpSingleElementList;
class qp_ns_common_policy_ruleType;

class UTRootNodeSimservs : public UTXMLSSHandler
{
public:
  UTRootNodeSimservs();
  ~UTRootNodeSimservs();
  /* Procees the SS request. Returns object of class QP_SS_REQPARAMS*/
  QP_SS_REQPARAMS* ProcessSSRequest(QP_SUPP_SRV_CB_DATA*, QP_CF_REQUEST_INFO*);
  /* Procees the SS response received from the N/w.
  ** Returns bool for response parsing and indication data in QP_SUPS_IND_DATA
  */
  QPBOOL ProcessSSResponse(QPCHAR*, QP_SUPS_IND_DATA*, QPBOOL& n_bCompleteXML){return QP_FALSE;}
  /* Returns class name for the Class*/
  QPCHAR* ClassName();
  QP_SS_REQPARAMS* CompleteSimservsXML();
  /* Procees the SS response received from the N/w
  ** And fill details of service supported and their status
  */
  QPBOOL ProcessSSResponse(QPCHAR*, QP_UT_ALL_SERV_ATTR&);

private:

  //Copy Constructor and Operator = 
  UTRootNodeSimservs(const UTRootNodeSimservs&);
  UTRootNodeSimservs& operator=(const UTRootNodeSimservs &);

  QPVOID RegistrationRequest(QP_SUPP_SRV_CB_DATA*, QP_CF_REQUEST_INFO*){}
  QPVOID ActivationRequest(QP_SUPP_SRV_CB_DATA*, QP_CF_REQUEST_INFO*);
  QPVOID DeActivationRequest(QP_SUPP_SRV_CB_DATA*, QP_CF_REQUEST_INFO*);
  QPVOID InterrogationRequest(QP_SUPP_SRV_CB_DATA*);
  QPVOID ErasureRequest(QP_SUPP_SRV_CB_DATA*, QP_CF_REQUEST_INFO*){}
  QPVOID Init();

  QPVOID CompleteCDIVNode(QpSingleElementList*);
  QPVOID CompleteCBIncomingNode(QpSingleElementList*);
  QPVOID CompleteCBOutgoingNode(QpSingleElementList*);

  /* Extracts the XML contents for All the services*/
  QPBOOL GetContents(QP_UT_ALL_SERV_ATTR&);
  /* Extracts the XML contents and stores in m_Rule_Parameter */
  QPBOOL GetCDIVContents(qp_communication_diversion*, QP_UT_SERV_ATTR&);
  /* Extracts the XML condition values for a given service and stores in m_Rule_Parameter */
  QPBOOL GetCDIVConditionValues(qp_ns_common_policy_conditionsType*, QPINT16&, QPCHAR*);
  /* Extracts the XML action values for a given service and stores in m_Rule_Parameter */
  QPVOID GetCDIVActionValues(qp_ns_common_policy_extensibleType*, QPINT16, QPCHAR*);
  /* Extracts the XML contents and stores in m_CB_IN_Rule_Parameter */
  QPBOOL GetCBINContents(qp_incoming_communication_barring*, QP_UT_CB_SERV_ATTR&);
  /* Extracts the XML condition values for a given service and stores in m_CB_IN_Rule_Parameter */
  QPBOOL GetCBINConditionValues(qp_ns_common_policy_conditionsType*, QP_ICB_RULE_PARAMETERS*, QP_UT_CB_SERV_ATTR&);
   /* Extracts the XML action values for a given service and stores in m_CB_IN_Rule_Parameter */
  QPVOID GetCBINActionValues(qp_ns_common_policy_extensibleType*, QP_ICB_RULE_PARAMETERS*);
  /* Extracts the XML contents and stores in m_CB_OUT_Rule_Parameter */
  QPBOOL GetCBOUTContents(qp_outgoing_communication_barring*, QP_UT_CB_SERV_ATTR&);
  /* Extracts the XML condition values for a given service and stores in m_CB_OUT_Rule_Parameter */
  QPBOOL GetCBOUTConditionValues(qp_ns_common_policy_conditionsType*, QPINT16&, QPCHAR*, QP_UT_CB_SERV_ATTR&);
   /* Extracts the XML action values for a given service and stores in m_CB_OUT_Rule_Parameter */
  QPVOID GetCBOUTActionValues(qp_ns_common_policy_extensibleType*, QPINT16, QPCHAR*);
  /* Generates Rule Type Node for ICB */
  qp_ns_common_policy_ruleType* GenerateRuleTypeNode(QPE_CB_IN_CONDITION, QP_ICB_RULE_PARAMETERS*);
  /*Store Media/Timer values*/
  QPBOOL GetMediaTimerValues(XMLElement*);
  QP_SS_OPERATION_CODE_T GetCodeCorrespondingToCDIVCondition(QPINT16);

private:
  qp_Simservs*                      m_pRequestElement;
  qp_Simservs*                      m_pResponseElement;
  QPUINT                            m_iNoReplyTimerValue;
  QP_CDIV_RULE_PARAMETERS           m_CDIV_Rule_Parameter[MAX_CDIV_RULE_LIMIT];
  QP_CB_RULE_PARAMETERS             m_CB_OUT_Rule_Parameter[MAX_CB_OUT_CONDITION_LIMIT];
  QPBOOL                            m_bCommWaitingStatus;
  QPBOOL                            m_bOIPStatus;
  QPBOOL                            m_bTIPStatus;
  QPBOOL                            m_bCDIVNode;
  QPBOOL                            m_bCWNode;
  QPBOOL                            m_bCBIncomingNode;
  QPBOOL                            m_bCBOutgoingNode;
  QPBOOL                            m_bOIPNode;
  QPBOOL                            m_bTIPNode;
  QP_MEDIA_TAG_USAGE                m_iMediaTagUsage;
  QP_EMPTY_SIB_USAGE                m_iEmptySIBUsage;
};

#endif // __UTROOTNODESIMSERVS_H__