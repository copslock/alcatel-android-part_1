/************************************************************************
 Copyright (C) 2013 Qualcomm Technologies Incorporation .All Rights Reserved.

 File Name      : UTOriginatingIdentityPresentationRestriction.h
 Description    : OIR class for UT operations
 

 ========================================================================
    Date    |   Author's Name    |  BugID  |        Change Description
 ========================================================================
02-Sept-2014     asharm        716797     FR 23312: Originating Identity restriction (OIR) Interogation Support 
************************************************************************/

#ifndef __UTORIGINATINGIDENTITYPRESENTATIONRESTRICTION_H__
#define __UTORIGINATINGIDENTITYPRESENTATIONRESTRICTION_H__

#include "UTXMLSSHandler.h"
#include "XMLOIR.h"
#include <qpPlatformConfig.h>

class qp_oir;
class UTOriginatingIdentityPresentationRestriction : public UTXMLSSHandler
{
public:
  UTOriginatingIdentityPresentationRestriction();
  ~UTOriginatingIdentityPresentationRestriction();
  /* Procees the SS request. Returns object of class QP_SS_REQPARAMS*/
  QP_SS_REQPARAMS* ProcessSSRequest(QP_SUPP_SRV_CB_DATA*, QP_CF_REQUEST_INFO*);
  /* Procees the SS response received from the N/w.
  Returns bool for response parsing and indication data in QP_SUPS_IND_DATA
  */
  QPBOOL ProcessSSResponse(QPCHAR*, QP_SUPS_IND_DATA*, QPBOOL& n_bCompleteXML);
  /* Returns class name for the Class*/
  QPCHAR* ClassName();

private:

  //Copy Constructor and Operator = 
  UTOriginatingIdentityPresentationRestriction(const UTOriginatingIdentityPresentationRestriction&);
  UTOriginatingIdentityPresentationRestriction& operator=(const UTOriginatingIdentityPresentationRestriction &);

  
  QPVOID RegistrationRequest(QP_SUPP_SRV_CB_DATA*, QP_CF_REQUEST_INFO*){}
  QPVOID ActivationRequest(QP_SUPP_SRV_CB_DATA*, QP_CF_REQUEST_INFO*);
  QPVOID DeActivationRequest(QP_SUPP_SRV_CB_DATA*, QP_CF_REQUEST_INFO*);
  QPVOID InterrogationRequest(QP_SUPP_SRV_CB_DATA*);
  QPVOID ErasureRequest(QP_SUPP_SRV_CB_DATA*, QP_CF_REQUEST_INFO*){}
  QPVOID Init();
  

private:
  qp_oir*      m_pRequestElement;
  qp_oir*      m_pResponseElement; 
};

#endif // __UTORIGINATINGIDENTITYPRESENTATIONRESTRICTION_H__