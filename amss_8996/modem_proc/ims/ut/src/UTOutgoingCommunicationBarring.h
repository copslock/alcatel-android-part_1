/************************************************************************
 Copyright (C) 2013 Qualcomm Technologies Incorporation .All Rights Reserved.

 File Name      : UTOutgoingCommunicationBarring.h
 Description    : Outgoing Communication Barring class for UT operations
 

 ========================================================================
    Date        |   Author's Name    |  BugID  |        Change Description
 ========================================================================
 27-09-2013     Sreenidhi          552121     Support for Communication Barring over UT interface
--------------------------------------------------------------------------------------------------
 02-12-2013     Priyank            539082     Perform a GET operationof the entire server document initially to avoid rule id mismatch
----------------------------------------------------------------------------------------
 26-12-2013     Sreenidhi          581666     Interrogating CF HTTP GET query will be implemented based on last possible node level
                                              (rule type such as CFU, CFB, CFNRC, CFNRY)
--------------------------------------------------------------------------------------------------
 05-02-2014     Priyank            605477     Ut marshal/unmarshal support of namespace at service level
 --------------------------------------------------------------------------------------------------
 16-04-2014     Sreenidhi          645056     Adding <conditions> element if not present for CFU/CB unconditional cases
  ------------------------------------------------------------------------------- 
 15-05-2014     Priyank             663757     UE crashes if CDIV has CFU in activated state and media element present
************************************************************************/

#ifndef __UTOUTGOINGCOMMUNICATIONBARRING_H__
#define __UTOUTGOINGCOMMUNICATIONBARRING_H__

#include "UTBaseCommunicationBarring.h"
#include "UTXMLSSHandler.h"
#include "XMLCommunicationBarring.h"

//Forward Declaration
class qp_outgoing_communication_barring;
class qp_ns_common_policy_ruleType;

class UTOutgoingCommunicationBarring : public UTXMLSSHandler
{
public:
  UTOutgoingCommunicationBarring();
  ~UTOutgoingCommunicationBarring();
  /* Procees the SS request. Returns object of class QP_SS_REQPARAMS*/
  QP_SS_REQPARAMS* ProcessSSRequest(QP_SUPP_SRV_CB_DATA*, QP_CF_REQUEST_INFO*);
  /* Procees the SS response received from the N/w.
  Returns bool for response parsing and indication data in QP_SUPS_IND_DATA
  */
  QPBOOL ProcessSSResponse(QPCHAR*, QP_SUPS_IND_DATA*, QPBOOL& n_bCompleteXML);
  /* Returns class name for the Class*/
  QPCHAR* ClassName();

private:

  //Copy Constructor and Operator = 
  UTOutgoingCommunicationBarring(const UTOutgoingCommunicationBarring&);
  UTOutgoingCommunicationBarring& operator=(const UTOutgoingCommunicationBarring &);

  QPVOID RegistrationRequest(QP_SUPP_SRV_CB_DATA*, QP_CF_REQUEST_INFO*);
  QPVOID ActivationRequest(QP_SUPP_SRV_CB_DATA*, QP_CF_REQUEST_INFO*);
  QPVOID DeActivationRequest(QP_SUPP_SRV_CB_DATA*, QP_CF_REQUEST_INFO*);
  QPVOID InterrogationRequest(QP_SUPP_SRV_CB_DATA*);
  QPVOID ErasureRequest(QP_SUPP_SRV_CB_DATA*, QP_CF_REQUEST_INFO*);
  QPVOID Init();
  /* Extracts the XML contents and stores in m_Rule_Parameter */
  QPBOOL GetContents();
  /* Extracts the XML condition values for a given service and stores in m_Rule_Parameter */
  QPBOOL GetConditionValues(qp_ns_common_policy_conditionsType*);
  /* Extracts the XML action values for a given service and stores in m_Rule_Parameter */
  QPVOID GetActionValues(qp_ns_common_policy_extensibleType*);
  /* Creates XML for Register request to be sent to server from m_Rule_Parameter*/
  QPVOID CreateRegisterRequestElements(QP_SUPP_SRV_CB_DATA*, QPE_CB_OUT_CONDITION);
  /* Creates XML for Activation/DeActivation request to be sent to server from m_Rule_Parameter*/
  QPVOID CreateActOrDeActRequestElements(QP_CF_REQUEST_INFO*, QPE_CB_OUT_CONDITION, QPBOOL, QPBOOL n_bIsConditionAbsentForUnconditional = QP_FALSE);
  /* Creates XML for Erase request to be sent to server from m_Rule_Parameter*/
  QPVOID CreateEraseRequestElements(QPE_CB_OUT_CONDITION);
  /* Sets the CB contents to be sent to CM for the request received*/
  QPVOID SetCBContents(QP_SUPS_IND_DATA*);
  /* Creates Register Element at Service Node level */
  QPVOID CreateRequestElementsAtServNode(QPE_CB_OUT_CONDITION, QPBOOL);
  /* Creates Register Element at Rule Node level */
  qp_ns_common_policy_ruleType* CreateRequestElementsAtRule(QPE_CB_OUT_CONDITION, QPBOOL);

private:
  qp_outgoing_communication_barring*                  m_pOCBRequestElement;
  qp_ns_common_policy_ruleType*                       m_pOCBRuleType;
   // Temporary Storing all values individually
  QP_CB_RULE_PARAMETERS                               m_Rule_Parameter;
  QPBOOL                                              m_bRequestAtServNode;
};

#endif // __UTCOMMUNICATIONBARRING_H__
