/************************************************************************
 Copyright (C) 2013 Qualcomm Technologies Incorporation .All Rights Reserved.

 File Name      : qpIMSServConfigGBAAuthUIM.cpp
 Description    : IMS Sevice Configuration 

 Revision History
 ========================================================================
   Date      |   Author's Name    |  BugID  |        Change Description
 ========================================================================
-------------------------------------------------------------------------------
12-May-2015   Sreenidhi     789667      FR25901: Ut changes for migrating IMS over to UIM GBA solution
-------------------------------------------------------------------------------
16-Jun-2015   Sreenidhi     848610      Memory leak issue due to establishing XCAP PDN connection repeatedly
-------------------------------------------------------------------------------
26-Jan-2016      huawenc           967115      TLS Mode variable is not initialized

************************************************************************/

#include "qpIMSServConfigGBAAuthUIM.h"
#include <XMLFactory.h>
#include <XMLBootstrappingInfo.h>
#include <ims_task.h>
#include "singoConfig.h"

#define QP_UB_AKAv1_INTERFACE_QOP                      (QPCHAR*)"auth-int"
#define QP_UA_INTERFACE_QOP                            (QPCHAR*)"auth,auth-int"
#define QP_UB_AKAv1_INTERFACE_ALGORITHM                (QPCHAR*)"AKAv1-MD5"
#define QP_UA_INTERFACE_ALGORITHM                      (QPCHAR*)"MD5"
#define QP_IMS_SERV_IMS_LABEL                          (QPCHAR*)"ims."
#define QP_IMS_SERV_MNC                                (QPCHAR*)"mnc"
#define QP_IMS_SERV_MCC                                (QPCHAR*)".mcc"
#define QP_IMS_SERV_DOMAIN_LABEL                       (QPCHAR*)".3gppnetwork.org"
#define QP_IMS_SERV_ATT                                (QPCHAR*)"@"
#define QP_IMS_SERV_COLON                              (QPCHAR*)":"
#define QP_UA_INTERFACE_INITIAL_NC                     (QPCHAR*)"00000000"
#define QP_IMS_SERV_DOMAIN_LEN                         50
#define	QP_IMS_SERV_IMPI_LEN                           256
#define QP_IMS_SERV_IPv6_LEN                           40


/************************************************************************
Function IMSServConfigGBAAuthUIM::IMSServConfigGBAAuthUIM()

Description
Constructor

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
IMSServConfigGBAAuthUIM::IMSServConfigGBAAuthUIM()
{
  m_eGBATLSMode = QPE_IMS_SERV_CONFIG_GBA_TLS_MODE_NONE;		
  m_bUaOnReqNoncePresent = QP_FALSE;
  qpDplMemset(&m_stGBAUaAuthParams, 0, sizeof(QP_AUTH_PARAMS));
  qpDplMemset(&m_stOnReqUaAuthParams, 0, sizeof(QP_AUTH_PARAMS));
  qpDplMemset(&m_stUaNNonceParams, 0, sizeof(QP_IMS_SERV_CONFIG_NEXT_CHALLENGE_PARAMS));
  m_iOprtMode = QP_CONFIG_IMS_OPRT_UNKNOWN_MODE;
  /* Reading and storing the oprt mode */
  if(QP_IO_ERROR_OK != qpDplIODeviceGetItem(IMS_OPRT_MODE, (QPVOID*) &m_iOprtMode, sizeof(m_iOprtMode)))
  {
    IMS_MSG_LOW_0(LOG_IMS_UT,"IMSServConfigGBAAuthUIM::ValidateAuthenticationInterface -no oprt mode defined");
  }
  else
  {
    IMS_MSG_LOW_1(LOG_IMS_UT,"IMSServConfigGBAAuthUIM::ValidateAuthenticationInterface -oprt mode = %d", m_iOprtMode);
  }
}

/************************************************************************
Function IMSServConfigGBAAuthUIM::~IMSServConfigGBAAuthUIM()

Description
Destructor

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
IMSServConfigGBAAuthUIM::~IMSServConfigGBAAuthUIM()
{
  ResetChallengeStruct(m_stGBAUaAuthParams);
  ResetChallengeStruct(m_stOnReqUaAuthParams);
  ResetNextChallengeStruct(m_stUaNNonceParams);
}
/************************************************************************
Function IMSServConfigGBAAuthUIM::GenerateUaAuthHeader()

Description
This function is to Generate Authorization header for
Ua Interface and fill the respective structure

Dependencies
None

Return Value
QP_TRUE/QP_FALSE

Side Effects
None
************************************************************************/
QPBOOL IMSServConfigGBAAuthUIM::GenerateUaAuthHeader(QP_IMS_SERV_CONFIG_GBA_UA_AUTH_PARAMS* n_pUaAuthHeaderParams)
{
  IMS_MSG_LOW_1(LOG_IMS_UT,"IMSServConfigGBAAuthUIM::GenerateUaAuthHeader Entered - m_bUaOnReqNoncePresent = %d",m_bUaOnReqNoncePresent);

  if( QP_NULL == n_pUaAuthHeaderParams) //|| QP_NULL == n_pUaAuthHeaderParams->pAuthorizationHeader )
  {
    IMS_MSG_FATAL_0(LOG_IMS_UT,"IMSServConfigGBAAuthUIM::GenerateUaAuthHeader n_pUaAuthHeaderParams/pAuthorizationHeader is NULL");
    return QP_FALSE;
  }

  /* If m_bUaOnReqNoncePresent is true, then we already have relevant data 
  ** and it has been reset in ValidateAuthenticationInterface function call.
  ** Do not do here, else stored data will be lost.
  */
  if(QP_FALSE == m_bUaOnReqNoncePresent)
  {
    ResetChallengeStruct(m_stGBAUaAuthParams);
    /* Store the Realm Value */
    if((QP_FALSE == IsNextNoncePresent()) && QP_NULL != n_pUaAuthHeaderParams->pRealm &&
      QP_NULL != (m_stGBAUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_REALM].pDigestRespose = qpDplStrdup(MEM_IMS_UT, n_pUaAuthHeaderParams->pRealm)))
    {
      m_stGBAUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_REALM].iDigestResLen = qpDplStrlen(n_pUaAuthHeaderParams->pRealm);
      m_stGBAUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_REALM].m_DlgChlType = QP_SIP_AUTH_REALM;
    }

    /* If next nonce is present, store it in the struct along with other values,
    ** so that next time before sending the message out we use this next nonce as nonce and compute response.
    ** We will come here when On Req mode is set and we haven't received nonce value in the challenge,
    ** Or, when Always On mode is set and it is a new request.
    */
    if(QP_TRUE == IsNextNoncePresent())
    {
      /* This will be true when Always On mode is set and it is a new request 
      ** and in the previous request's 200 OK we received next nonce.
      */
      if(QP_NULL != m_stUaNNonceParams.pNextNonce && QP_NULL != (m_stGBAUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_NONCE].pDigestRespose = qpDplStrdup(MEM_IMS_UT, m_stUaNNonceParams.pNextNonce)))
      {
        m_stGBAUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_NONCE].iDigestResLen = qpDplStrlen(m_stUaNNonceParams.pNextNonce);
        m_stGBAUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_NONCE].m_DlgChlType = QP_SIP_AUTH_NONCE;
      }
      else
        IMS_MSG_ERR_0(LOG_IMS_FW,"IMSServConfigGBAAuthUIM::GenerateUaAuthHeader | Nonce qpDplStrdup Failed ");

      /* When Next Nonce is present, then fill Algorithm and Qop as well */
      if( QP_NULL != m_stUaNNonceParams.pAlgorithm && QP_NULL != (m_stGBAUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_ALGORITHM].pDigestRespose = qpDplStrdup(MEM_IMS_UT, m_stUaNNonceParams.pAlgorithm)))
      {
        m_stGBAUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_ALGORITHM].iDigestResLen = qpDplStrlen(m_stUaNNonceParams.pAlgorithm);
        m_stGBAUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_ALGORITHM].m_DlgChlType = QP_SIP_AUTH_ALGORITHM;
      }
      else if(QP_NULL == m_stUaNNonceParams.pAlgorithm && QP_NULL != (m_stGBAUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_ALGORITHM].pDigestRespose = qpDplStrdup(MEM_IMS_UT, QP_UA_INTERFACE_ALGORITHM)))
      {
        IMS_MSG_MED_0(LOG_IMS_FW,"IMSServConfigGBAAuthUIM::GenerateUaAuthHeader | ALGORITHM not present, taking default value");
        m_stGBAUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_ALGORITHM].iDigestResLen = qpDplStrlen(QP_UA_INTERFACE_ALGORITHM);
        m_stGBAUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_ALGORITHM].m_DlgChlType = QP_SIP_AUTH_ALGORITHM;
      }
      else
        IMS_MSG_ERR_0(LOG_IMS_FW,"IMSServConfigGBAAuthUIM::GenerateUaAuthHeader | ALGORITHM qpDplStrdup Failed ");

      if(QP_NULL != m_stUaNNonceParams.pQoP && QP_NULL != (m_stGBAUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_QOP].pDigestRespose = qpDplStrdup(MEM_IMS_UT, m_stUaNNonceParams.pQoP)))
      {
        m_stGBAUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_QOP].iDigestResLen = qpDplStrlen(m_stUaNNonceParams.pQoP);
        m_stGBAUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_QOP].m_DlgChlType = QP_SIP_AUTH_QOP;
      }
      else if(QP_NULL == m_stUaNNonceParams.pQoP && QP_NULL != (m_stGBAUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_QOP].pDigestRespose = qpDplStrdup(MEM_IMS_UT, QP_UA_INTERFACE_QOP)))
      {
        IMS_MSG_MED_0(LOG_IMS_FW,"IMSServConfigGBAAuthUIM::GenerateUaAuthHeader | AUTH QOP not present, taking default value");
        m_stGBAUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_QOP].iDigestResLen = qpDplStrlen(QP_UA_INTERFACE_QOP);
        m_stGBAUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_QOP].m_DlgChlType = QP_SIP_AUTH_QOP;
      }
      else
        IMS_MSG_ERR_0(LOG_IMS_FW,"IMSServConfigGBAAuthUIM::GenerateUaAuthHeader | AUTH QOP qpDplStrdup Failed or NULL");

      if(QP_NULL != m_stUaNNonceParams.pOpaque && QP_NULL != (m_stGBAUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_OPAQUE].pDigestRespose = qpDplStrdup(MEM_IMS_UT, m_stUaNNonceParams.pOpaque)))
      {
        m_stGBAUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_OPAQUE].iDigestResLen = qpDplStrlen(m_stUaNNonceParams.pOpaque);
        m_stGBAUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_OPAQUE].m_DlgChlType = QP_SIP_AUTH_OPAQUE;
      }
      else
        IMS_MSG_ERR_0(LOG_IMS_FW,"IMSServConfigGBAAuthUIM::GenerateUaAuthHeader | AUTH Opaque qpDplStrdup Failed or NULL");

      if(QP_NULL != m_stUaNNonceParams.pRealm && QP_NULL != (m_stGBAUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_REALM].pDigestRespose = qpDplStrdup(MEM_IMS_UT, m_stUaNNonceParams.pRealm)))
      {
        m_stGBAUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_REALM].iDigestResLen = qpDplStrlen(m_stUaNNonceParams.pRealm);
        m_stGBAUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_REALM].m_DlgChlType = QP_SIP_AUTH_REALM;
      }
      else
        IMS_MSG_ERR_0(LOG_IMS_FW,"IMSServConfigGBAAuthUIM::GenerateUaAuthHeader | Next nonce is present and Realm qpDplStrdup Failed Or NULL");

	  if(QP_NULL != m_stUaNNonceParams.pNC && QP_NULL != (m_stGBAUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_NC].pDigestRespose = qpDplStrdup(MEM_IMS_UT, m_stUaNNonceParams.pNC)))
      {
        m_stGBAUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_NC].iDigestResLen = qpDplStrlen(m_stUaNNonceParams.pNC);
        m_stGBAUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_NC].m_DlgChlType = QP_SIP_AUTH_NC;
      }
	  else if(QP_NULL == m_stUaNNonceParams.pNC && QP_NULL != (m_stGBAUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_NC].pDigestRespose = qpDplStrdup(MEM_IMS_UT,QP_UA_INTERFACE_INITIAL_NC)))
	  {
	    m_stGBAUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_NC].iDigestResLen = qpDplStrlen(QP_UA_INTERFACE_INITIAL_NC);
        m_stGBAUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_NC].m_DlgChlType = QP_SIP_AUTH_NC;
	  }
      else
      {
        IMS_MSG_ERR_0(LOG_IMS_FW,"IMSServConfigGBAAuthUIM::GenerateUaAuthHeader | Next nonce is present and NC qpDplStrdup Failed Or NULL");
      }
    }
  }

  /* Store the Username Value */
  if(QP_NULL != n_pUaAuthHeaderParams->pUsername)
  {
    if(QP_NULL != m_stGBAUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_USERNAME].pDigestRespose)
    {
      qpDplFree(MEM_IMS_UT,m_stGBAUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_USERNAME].pDigestRespose);
	}
	if(QP_NULL != (m_stGBAUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_USERNAME].pDigestRespose = qpDplStrdup(MEM_IMS_UT, n_pUaAuthHeaderParams->pUsername)))
	{
	  m_stGBAUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_USERNAME].iDigestResLen = qpDplStrlen(n_pUaAuthHeaderParams->pUsername);
	  m_stGBAUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_USERNAME].m_DlgChlType = QP_SIP_AUTH_USERNAME;
	}
  }

  /* Store the URI Value */
  if(QP_NULL != n_pUaAuthHeaderParams->pURI)
  {
    if(QP_NULL != m_stGBAUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_URI].pDigestRespose)
    {
	  qpDplFree(MEM_IMS_UT,m_stGBAUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_URI].pDigestRespose);
	}
	if(QP_NULL != (m_stGBAUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_URI].pDigestRespose = qpDplStrdup(MEM_IMS_UT, n_pUaAuthHeaderParams->pURI)))
	{
	  m_stGBAUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_URI].iDigestResLen = qpDplStrlen(n_pUaAuthHeaderParams->pURI);
	  m_stGBAUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_URI].m_DlgChlType = QP_SIP_AUTH_URI;
	}
  }

  /* Store the Entity Body Value */
  if(QP_NULL != n_pUaAuthHeaderParams->pEntityBody)
  {
    if(m_stGBAUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_ENTITY_BODY].pDigestRespose)
	{
      qpDplFree(MEM_IMS_UT,m_stGBAUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_ENTITY_BODY].pDigestRespose);
	}
	if(QP_NULL != (m_stGBAUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_ENTITY_BODY].pDigestRespose = qpDplStrdup(MEM_IMS_UT, n_pUaAuthHeaderParams->pEntityBody)))
	{
	  m_stGBAUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_ENTITY_BODY].iDigestResLen = qpDplStrlen(n_pUaAuthHeaderParams->pEntityBody);
	  m_stGBAUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_ENTITY_BODY].m_DlgChlType = QP_SIP_AUTH_ENTITY_BODY;
	}
  }

  /* Store the Method Name Value */
  if(QP_NULL != n_pUaAuthHeaderParams->pMethodName)
  {
    if(m_stGBAUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_METHOD].pDigestRespose)
    {
	  qpDplFree(MEM_IMS_UT,m_stGBAUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_METHOD].pDigestRespose);
	}
	if(QP_NULL != (m_stGBAUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_METHOD].pDigestRespose = qpDplStrdup(MEM_IMS_UT, n_pUaAuthHeaderParams->pMethodName)))
	{
	  m_stGBAUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_METHOD].iDigestResLen = qpDplStrlen(n_pUaAuthHeaderParams->pMethodName);
	  m_stGBAUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_METHOD].m_DlgChlType = QP_SIP_AUTH_METHOD;
	}
  }
  //GenerateAuthorizationHeader(m_stGBAUaAuthParams, n_pUaAuthHeaderParams->pAuthorizationHeader, QP_MAX_HEADER_LEN * sizeof(QPCHAR));
  return QP_TRUE;
}

/************************************************************************
Function IMSServConfigGBAAuthUIM::ParseAuthHeaderAndFillChallengeStruct()

Description
This is a generic function to parse the Authorization header received
and fill the challenge structure and respective parameters

Dependencies
None

Return Value
QP_TRUE/QP_FALSE

Side Effects
None
************************************************************************/
QPBOOL IMSServConfigGBAAuthUIM::ParseAuthHeaderAndFillChallengeStruct(const QPCHAR* n_pInAuthenticateHeader,
                                                                   QP_AUTH_PARAMS& n_stChallengeParams)
{
  IMS_MSG_LOW_0(LOG_IMS_UT,"IMSServConfigGBAAuthUIM::ParseAuthHeaderAndFillChallengeStruct Entered");
  SipHeader* pSipHeader          = QP_NULL;
  QPCHAR*    pNonce              = QP_NULL;
  QPCHAR*    pRealm              = QP_NULL;
  QPCHAR*    pAlgorithm          = QP_NULL;
  QPCHAR*    pQop                = QP_NULL;
  QPCHAR*    pOpaque             = QP_NULL;
  QPCHAR*    pStale              = QP_NULL;
  QPCHAR*    pNC                 = QP_NULL;

  SipString strName((QPCHAR*)HttpHeadersStrings[QP_AUTHENTICATE]);
  SipString strValue(n_pInAuthenticateHeader);
  IMS_NEW(SipHeader, MEM_IMS_UT, pSipHeader, (strName, strValue));

  if(pSipHeader == QP_NULL)
  {
    IMS_MSG_FATAL_0(LOG_IMS_UT,"IMSServConfigGBAAuthUIM::ParseAuthHeaderAndFillChallengeStruct | new SipHeader failed");
    return QP_FALSE;
  }

  /* Free the old nonce value if present and store the value from new Challenge
  ** This will happen when previously sent Authorization header has been re-challenged by the server.
  */
  pNonce = RemoveQoutes(pSipHeader->getParameter((SipString)QP_SIP_NONCE_STR));
  UpdateChallengeParams(pNonce, n_stChallengeParams, QP_SIP_AUTH_NONCE);
  /* Store the value of this Nonce that is being used to send the response for this challenge as,
  ** if Next Nonce doesn't come in the 200 OK, the same Nonce value should be used.
  */
  UpdateNextChallengeParams(pNonce , &m_stUaNNonceParams.pNextNonce);

  /* update the NC to initila value as a new nonce is received here */
  pNC = qpDplStrdup(MEM_IMS_UT,QP_UA_INTERFACE_INITIAL_NC);
  UpdateChallengeParams(pNC, n_stChallengeParams, QP_SIP_AUTH_NC);
  UpdateNextChallengeParams(pNC , &m_stUaNNonceParams.pNC);

  /* Delete the previous Realm as in the response it can change and store the Realm value */
  pRealm = RemoveQoutes(pSipHeader->getParameter((SipString)QP_SIP_REALM_STR));
  UpdateChallengeParams(pRealm, n_stChallengeParams, QP_SIP_AUTH_REALM);

  /* Store the value of Realm that is being used to send the response for the challenge as,
  ** if Next Nonce comes in the 200 OK, the same Realm value should be used.
  */  

  UpdateNextChallengeParams(pRealm , &m_stUaNNonceParams.pRealm);

  
  /* Free the Old ALGORITHM value if present and store the value from new Challenge */
  pAlgorithm = RemoveQoutes(pSipHeader->getParameter((SipString)QP_SIP_ALGORITHM_STR));
  UpdateChallengeParams(pAlgorithm, n_stChallengeParams, QP_SIP_AUTH_ALGORITHM);

  UpdateNextChallengeParams(pAlgorithm , &m_stUaNNonceParams.pAlgorithm);

  
  /* Free the old value and store the new Qop value */
  pQop = RemoveQoutes(pSipHeader->getParameter((SipString)QP_SIP_QOP_STR));
  UpdateChallengeParams(pQop, n_stChallengeParams, QP_SIP_AUTH_QOP);


  UpdateNextChallengeParams(pQop , &m_stUaNNonceParams.pQoP);

  
  /* Free the old value and store the new Opaque value */
  pOpaque = RemoveQoutes(pSipHeader->getParameter((SipString)QP_SIP_OPAQUE_STR));
  UpdateChallengeParams(pOpaque, n_stChallengeParams, QP_SIP_AUTH_OPAQUE);


  UpdateNextChallengeParams(pOpaque , &m_stUaNNonceParams.pOpaque);

  
  /* Free the old value and store the new Stale value */
  pStale = RemoveQoutes(pSipHeader->getParameter((SipString)QP_SIP_STALE_STR));
  if(QP_NULL == pStale)
  {
    /* Stale has to be case insensitive */
    pStale = RemoveQoutes(pSipHeader->getParameter((SipString)QP_SIP_STALE_STR_CAPS));
  }
  UpdateChallengeParams(pStale, n_stChallengeParams, QP_SIP_AUTH_STALE);

  IMS_DELETE(pSipHeader,MEM_IMS_UT);
  return QP_TRUE;
}
/************************************************************************
Function IMSServConfigGBAAuthUIM::ValidateGBA2XXResponse()

Description
This function is called from the application to validate the 200 OK
received from the server and check if integrity is protected. 

Dependencies
None

Return Value
QP_TRUE/QP_FALSE

Side Effects
None
************************************************************************/
QPBOOL IMSServConfigGBAAuthUIM::ValidateGBA2XXResponse(QPCHAR* n_pRspAuthHeader, QPCHAR* n_pHttpContent)
{
  IMS_MSG_LOW_0(LOG_IMS_UT,"IMSServConfigGBAAuthUIM::ProcessGBAResponse Entered");
  if(QP_NULL == n_pRspAuthHeader)
  {
    IMS_MSG_ERR_0(LOG_IMS_UT,"IMSServConfigGBAAuthUIM::ProcessGBAResponse Response Auth Header is NULL ");
    return QP_FALSE;
  }
  ParseRspAuthParam(n_pRspAuthHeader, n_pHttpContent, m_stGBAUaAuthParams);
  /* Once the request is completed on Ua Interface, next request should use fresh set of values */
  m_bUaOnReqNoncePresent = QP_FALSE;
  return QP_TRUE;
}

/************************************************************************
Function IMSServConfigGBAAuthUIM::ParseRspAuthParam()

Description
This function parses the "rspauth" parameter.

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID IMSServConfigGBAAuthUIM::ParseRspAuthParam(QPCHAR* n_pRspAuthHeader, QPCHAR* n_pHttpContent, 
                                               QP_AUTH_PARAMS& n_stChallengeParams)
{
  IMS_MSG_LOW_0(LOG_IMS_UT,"IMSServConfigGBAAuthUIM::ParseRspAuthParam Entered");
  QPCHAR* pNextNonce             = QP_NULL;
  QPCHAR* pQop                   = QP_NULL;
  QPCHAR* pRspAuth               = QP_NULL;
  QPCHAR* pHttpContent           = QP_NULL;
  QPCHAR* pTemp                  = QP_NULL;
  QPCHAR* pParam                 = QP_NULL;
  QPCHAR* pNC                    = QP_NULL;

  pTemp = qpDplStrstr(n_pRspAuthHeader, QP_SIP_RSPAUTH_STR);
  pRspAuth = ExtractParamsFromHeader(pTemp);
  /* Free the old rspauth and store the new Qop value, ideally this should be nevre be filled before. */
  UpdateChallengeParams(pRspAuth, n_stChallengeParams, QP_SIP_AUTH_RSPAUTH);

  /* In Authentication-Info header qop may not be in "" 
  ** So handling 4 use case 
  ** 1) qop ="auth-int"
  ** 2) qop =auth-int
  ** 3) qop = "auth-int"
  ** 4) qop = auth-int
  */
  pTemp = QP_NULL;
  pTemp = qpDplStrstr(n_pRspAuthHeader, QP_SIP_QOP_STR);
  if(QP_NULL != pTemp)
  {
    pParam = qpDplStrstr(pTemp, QP_SIP_EQUALS_STR);
    if(QP_NULL != pParam)
    {
      pParam++;
      if (pParam[0] == ' ')
        pParam++;
      if(pParam[0] == '\"')
        pParam++;
      pTemp = pParam;
      while(*pParam != '\"' && *pParam != ',' && *pParam != '\0')
        ++pParam;

      pQop = (QPCHAR *)qpDplMalloc(MEM_IMS_SIP, (pParam - pTemp + 1) * sizeof(QPCHAR));
      if(pQop == QP_NULL)
      {
        IMS_MSG_ERR_0(LOG_IMS_UT,"IMSServConfigGBAAuthUIM::ParseRspAuthParam | Malloc Failed");
        return;
      }
      qpDplMemset(pQop, 0, (pParam - pTemp + 1) * sizeof(QPCHAR));
      (QPVOID)qpDplStrlncpy(pQop, pTemp, (pParam - pTemp + 1) * sizeof(QPCHAR), pParam - pTemp);
      IMS_MSG_MED_1_STR(LOG_IMS_UT,"IMSServConfigGBAAuthUIM::ParseRspAuthParam | Qop Value %s", pQop);
      /* Free the old value and store the new Qop value */
      UpdateChallengeParams(pQop, n_stChallengeParams, QP_SIP_AUTH_QOP);
    }
  }
  /* Free the old Entity Body and store the new Entity body coming from 200 OK */
  if(QP_NULL != n_pHttpContent)
  {
    pHttpContent= qpDplStrdup(MEM_IMS_UT, n_pHttpContent);
  }
  UpdateChallengeParams(pHttpContent, n_stChallengeParams, QP_SIP_AUTH_ENTITY_BODY);

  pTemp = QP_NULL;
  pTemp = qpDplStrstr(n_pRspAuthHeader, QP_SIP_NEXT_NONCE_STR);
  pNextNonce = ExtractParamsFromHeader(pTemp);

  /* 200 Ok has been received for the request sent.
  ** If next nonce is present in the header, save it in the respective member variable,
  ** else if it not present then delete the previous one so that it doesn't get used in the next request,
  ** otherwise server might send challenge with STALE true.
  */

  {
    if(QP_NULL != pNextNonce)
    {
      if(QP_NULL != m_stUaNNonceParams.pNextNonce)
      {
        qpDplFree(MEM_IMS_UT, m_stUaNNonceParams.pNextNonce);
        m_stUaNNonceParams.pNextNonce = QP_NULL;
      }
      m_stUaNNonceParams.pNextNonce = pNextNonce;
	  // Reset NC value as next nonce is present
	  qpDplFree(MEM_IMS_UT, m_stUaNNonceParams.pNC);
	  m_stUaNNonceParams.pNC = qpDplStrdup(MEM_IMS_UT,QP_UA_INTERFACE_INITIAL_NC);
    }
	else
	{
	  // Do nothing as the value will be incremented by protocols
	  pTemp = QP_NULL;
	  pParam = QP_NULL;
      pTemp = qpDplStrstr(n_pRspAuthHeader, QP_SIP_NC_STR);
	  if(QP_NULL != pTemp)
      {
        pParam = qpDplStrstr(pTemp, QP_SIP_EQUALS_STR);
	  }
	  else
	  {
	    pTemp = qpDplStrstr(n_pRspAuthHeader, QP_SIP_NC_STR_CAPS);
	    if(QP_NULL != pTemp)
        {
          pParam = qpDplStrstr(pTemp, QP_SIP_EQUALS_STR);
	    }
	  }
	  if(pParam)
	  {
        pParam++;
	  }
	  if(pParam)
	  {
	    pNC = qpDplStrdup(MEM_IMS_UT,pParam);
	    UpdateChallengeParams(pNC, m_stGBAUaAuthParams, QP_SIP_AUTH_NC);
        UpdateNextChallengeParams(pNC , &m_stUaNNonceParams.pNC);
	  }
	  IMS_MSG_HIGH_0(LOG_IMS_UT,"IMSServConfigGBAAuthUIM::ParseRspAuthParam | No Nextnonce");
	}
  }
}

/************************************************************************
Function IMSServConfigGBAAuthUIM::UpdateChallengeParams()

Description
Updates the challenge parameter's particular instance.

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID IMSServConfigGBAAuthUIM::UpdateChallengeParams(QPCHAR* n_pParam, QP_AUTH_PARAMS& n_stChallengeParams,
                                                   QP_SIP_DIGEST_AUTH_PARAM n_eAuthParam)
{
  if(QP_NULL != n_stChallengeParams.m_pDigestChallenge[n_eAuthParam].pDigestRespose)
  {
    qpDplFree(MEM_IMS_UT, n_stChallengeParams.m_pDigestChallenge[n_eAuthParam].pDigestRespose);
    n_stChallengeParams.m_pDigestChallenge[n_eAuthParam].pDigestRespose = QP_NULL;
  }
  if(QP_NULL != n_pParam)
  {
    n_stChallengeParams.m_pDigestChallenge[n_eAuthParam].pDigestRespose = n_pParam;
    n_stChallengeParams.m_pDigestChallenge[n_eAuthParam].iDigestResLen = qpDplStrlen(n_pParam);
    n_stChallengeParams.m_pDigestChallenge[n_eAuthParam].m_DlgChlType = n_eAuthParam;
  }
}

/************************************************************************
Function IMSServConfigGBAAuthUIM::UpdateNextChallengeParams()

Description
Updates the next challenge parameter's particular instance.

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID IMSServConfigGBAAuthUIM::UpdateNextChallengeParams(QPCHAR* n_pParam, QPCHAR** n_pChallengeParam)
{
  if(QP_NULL != n_pChallengeParam && QP_NULL != *n_pChallengeParam)
  {
    qpDplFree(MEM_IMS_UT, *n_pChallengeParam);
    *n_pChallengeParam = QP_NULL;
  }
  if(QP_NULL != n_pParam && QP_NULL != n_pChallengeParam)
  {
    if(QP_NULL == ( *n_pChallengeParam = qpDplStrdup(MEM_IMS_UT, n_pParam)))
    {
      IMS_MSG_ERR_1_STR(LOG_IMS_FW,"IMSServConfigGBAAuthUIM::UpdateNextChallengeParams | n_pParam qpDplStrdup Failed % s", n_pParam);
    } 
  }
}

/************************************************************************
Function IMSServConfigGBAAuthUIM::ValidateAuthenticationInterface()

Description
This function is called from the application when On-Request
Authentication is enabled and we need to the type of Challenge.

Dependencies
None

Return Value
QP_TRUE/QP_FALSE

Side Effects
None
************************************************************************/
QPBOOL IMSServConfigGBAAuthUIM::ValidateAuthenticationInterface(QP_IMS_SERV_CONFIG_UA_ON_REQ_CHALLENGE_PARAMS& n_stOnReqAuthParams, const QPCHAR* n_pInAuthHeader,
                                                             QPTLS_SESS_PROFILE* n_pTLSSessProfile)
{
  IMS_MSG_LOW_0(LOG_IMS_UT,"IMSServConfigGBAAuthUIM::ValidateAuthenticationInterface Entered");
  QPBOOL  bRetValue                 = QP_FALSE;
  QPBOOL  bIsRealmFieldValid        = QP_TRUE;
  QPCHAR* pRealm                    = QP_NULL;
  QPCHAR* pBootStrap                = QP_NULL;
  QPCHAR* pRealmWithUICC            = QP_NULL;
  QPCHAR* pSemiColonRealm            = QP_NULL;

  if(QP_NULL == n_pInAuthHeader)
  {
    IMS_MSG_ERR_0(LOG_IMS_UT,"IMSServConfigGBAAuthUIM::ValidateAuthenticationInterface Auth Header is NULL");
    return bRetValue;
  }

  /* Reset the value as it will called only when there is a new request */
  m_bUaOnReqNoncePresent = QP_FALSE;
  ResetChallengeStruct(m_stOnReqUaAuthParams);
  bRetValue = ParseAuthHeaderAndFillChallengeStruct(n_pInAuthHeader, m_stOnReqUaAuthParams);

  if(QP_TRUE == bRetValue)
  {
    if (QP_NULL != m_stOnReqUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_NONCE].pDigestRespose &&
      0 != qpDplStrlen(m_stOnReqUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_NONCE].pDigestRespose))
    {
      IMS_MSG_LOW_0(LOG_IMS_UT,"IMSServConfigGBAAuthUIM::ValidateAuthenticationInterface Nonce present");
      n_stOnReqAuthParams.bNoncePresent = QP_TRUE;
    }
    else
    {
      IMS_MSG_LOW_0(LOG_IMS_UT,"IMSServConfigGBAAuthUIM::ValidateAuthenticationInterface Nonce absent");
      n_stOnReqAuthParams.bNoncePresent = QP_FALSE;
    }

    if (QP_NULL != m_stOnReqUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_REALM].pDigestRespose &&
      0 != qpDplStrlen(m_stOnReqUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_REALM].pDigestRespose))
    {
      pRealm = m_stOnReqUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_REALM].pDigestRespose;
      pBootStrap = qpDplStrstr(pRealm, IMS_SERV_CONFIG_UA_REALM);

      if(QP_NULL != pBootStrap)
      {
        n_stOnReqAuthParams.bPerformUbAuth = QP_TRUE;
      }

      /* Check if UICC is present in the realm */
      pRealm = m_stOnReqUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_REALM].pDigestRespose;
      pRealmWithUICC = qpDplStrstr(pRealm, IMS_SERV_CONFIG_UA_UICC_REALM);

      if(QP_NULL != pRealmWithUICC && m_iOprtMode == QP_CONFIG_IMS_OPRT_TMO_MODE) 
      {
        pSemiColonRealm = qpDplStrstr(pRealm, IMS_SERV_CONFIG_SEMI_COLON);   
		if(pSemiColonRealm != QP_NULL && pSemiColonRealm > pRealmWithUICC ) // uicc realm found before semicolon part of realm
		{
          pRealm = pSemiColonRealm + 1;
         (QPVOID)qpDplStrlcpy(m_stOnReqUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_REALM].pDigestRespose, pRealm, qpDplStrlen(pRealm) + 1);
          m_stOnReqUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_REALM].iDigestResLen =  qpDplStrlen(pRealm);	
		  pRealmWithUICC = QP_NULL;
		}
		else if(pSemiColonRealm != QP_NULL )
		{
         pRealm[pSemiColonRealm - pRealm]= '\0';
         (QPVOID)qpDplStrlcpy(m_stOnReqUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_REALM].pDigestRespose, pRealm, qpDplStrlen(pRealm) + 1);
		 m_stOnReqUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_REALM].iDigestResLen =  qpDplStrlen(pRealm);	
		 pRealmWithUICC = QP_NULL;
		}
		else
		{          
		  IMS_MSG_MED_1(LOG_IMS_UT,"IMSServConfigGBAAuthUIM::ValidateAuthenticationInterface | Invalid realm format %s", pRealm);
		}        	 
       }

      /* Check if TLS mode is enabled; realm parameter matches */
      if(QPE_IMS_SERV_CONFIG_GBA_TLS_SharedKey_CertBased == m_eGBATLSMode && QP_NULL != pBootStrap)
      {
        //We need to compare the NAF ID part in the validated realm
        pRealm = pRealm + qpDplStrlen(IMS_SERV_CONFIG_UA_REALM);
        bIsRealmFieldValid = ValidateServersRealm(pRealm, n_pTLSSessProfile);
        IMS_MSG_MED_1(LOG_IMS_UT,"IMSServConfigGBAAuthUIM::ValidateAuthenticationInterface | bIsRealmFieldValid %d", bIsRealmFieldValid);
      }

      if(QP_NULL != pRealmWithUICC || QP_NULL == pBootStrap || QP_FALSE == bIsRealmFieldValid)
      {
        IMS_MSG_MED_0(LOG_IMS_UT,"IMSServConfigGBAAuthUIM | UICC present/Incorrect Digest/3GPP-bootstrapping@ Absent/ bIsRealmFieldValid");
        n_stOnReqAuthParams.bIsParamIncorrect = QP_TRUE;
      }
    }

    /* This code is required in the scenario when On Request is enabled 
    ** and on the very first challenge server is sending nonce also
    */
    if(QP_TRUE == n_stOnReqAuthParams.bNoncePresent && QP_TRUE != n_stOnReqAuthParams.bIsParamIncorrect) 
    {
        m_bUaOnReqNoncePresent = QP_TRUE;
        CopyReceivedChallengeParams();
        /* Don't delete, just memset as the ownership is passed in above statement, and in future if delete
        ** is called on m_stOnReqUaAuthParams, pointers will not deleted and will just set to QP_NULL */
        qpDplMemset(&m_stOnReqUaAuthParams, 0, sizeof(QP_AUTH_PARAMS));
    }
  }
  return bRetValue;
}

/************************************************************************
Function IMSServConfigGBAAuthUIM::ResetPreviousAuthParams()

Description
This function is called when a new challenged is received even when
in previous request contained Authorization header with response.

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID IMSServConfigGBAAuthUIM::ResetPreviousAuthParams()
{

  IMS_MSG_LOW_0(LOG_IMS_UT,"IMSServConfigGBAAuthUIM::ResetPreviousAuthParams Entered");

    m_bUaOnReqNoncePresent = QP_FALSE;
    ResetNextChallengeStruct(m_stUaNNonceParams);
}

/************************************************************************
Function IMSServConfigGBAAuthUIM::IsQopTypeAuthInt()

Description
This function is called from the application to check if
Qop Type is "auth-int"

Dependencies
None

Return Value
QP_TRUE/QP_FALSE

Side Effects
None
************************************************************************/
QPBOOL IMSServConfigGBAAuthUIM::IsQopTypeAuthInt()
{
  IMS_MSG_LOW_0(LOG_IMS_UT,"IMSServConfigGBAAuthUIM::IsQopTypeAuthInt Entered");

  if(QP_NULL != m_stGBAUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_QOP].pDigestRespose &&
       QP_NULL != qpDplStrstr(m_stGBAUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_QOP].pDigestRespose, "auth-int"))
    {
      IMS_MSG_LOW_0(LOG_IMS_UT,"IMSServConfigGBAAuthUIM::IsQopTypeAuthInt Auth-Int on Ua Present");
      return QP_TRUE;
    }
  return QP_FALSE;
}

/************************************************************************
Function IMSServConfigGBAAuthUIM::IsNextNoncePresent()

Description
This function is to check if the Next Nonce is present.

Dependencies
None

Return Value
QP_TRUE/QP_FALSE

Side Effects
None
************************************************************************/
QPBOOL IMSServConfigGBAAuthUIM::IsNextNoncePresent()
{
  IMS_MSG_LOW_0(LOG_IMS_UT,"IMSServConfigGBAAuthUIM::IsNextNoncePresent Entered");
    if(QP_NULL != m_stUaNNonceParams.pNextNonce && 0 != qpDplStrlen(m_stUaNNonceParams.pNextNonce))
    {
      IMS_MSG_LOW_0(LOG_IMS_UT,"IMSServConfigGBAAuthUIM::IsNextNoncePresent Ua Next Nonce Present ");
      return QP_TRUE;
    }
  return QP_FALSE;
}
/************************************************************************
Function IMSServConfigGBAAuthUIM::ExtractParamsFromHeader()

Description
This function extracts parameters based on Input

Dependencies
None

Return Value
QPCHAR*

Side Effects
None
************************************************************************/
QPCHAR* IMSServConfigGBAAuthUIM::ExtractParamsFromHeader(QPCHAR* n_pInStr)
{
  QPCHAR* pParam                 = QP_NULL;
  QPCHAR* pTemp                  = QP_NULL;

  if(n_pInStr != QP_NULL)
  {
    pParam = qpDplStrstr(n_pInStr, QP_SIP_DQUOTE_STR);
    if(pParam == QP_NULL)
    {
      IMS_MSG_ERR_0(LOG_IMS_UT,"IMSServConfigGBAAuthUIM::ExtractParamsFromHeader | pParam is NULL");
      return QP_NULL;
    }
    ++pParam;
    pTemp = pParam; 

    while(*pParam != '\"' && *pParam != ',' && *pParam != '\0')
      ++pParam;

    if(*pParam != '\"')
    {
      IMS_MSG_ERR_0(LOG_IMS_UT,"IMSServConfigGBAAuthUIM::ExtractParamsFromHeader | Param is malformed");
      return QP_NULL;
    }

    QPCHAR *pParamVal = (QPCHAR *)qpDplMalloc(MEM_IMS_SIP, (pParam - pTemp + 1) * sizeof(QPCHAR));
    if(pParamVal == QP_NULL)
    {
      IMS_MSG_ERR_0(LOG_IMS_UT,"IMSServConfigGBAAuthUIM::ExtractParamsFromHeader | Malloc Failed");
      return QP_NULL;
    }
    qpDplMemset(pParamVal, 0, (pParam - pTemp + 1) * sizeof(QPCHAR));
    (QPVOID)qpDplStrlncpy(pParamVal, pTemp, (pParam - pTemp + 1) * sizeof(QPCHAR), pParam - pTemp);
    return pParamVal;
  }
  else
    return QP_NULL;
}

/************************************************************************
Function IMSServConfigGBAAuthUIM::RemoveQoutes()

Description
This function removes quotes and mallocs that value and
returns a pointer for the same.

Dependencies
None

Return Value
QPCHAR*

Side Effects
None
************************************************************************/
QPCHAR* IMSServConfigGBAAuthUIM::RemoveQoutes(const SipString *n_pStrParam)
{
  QPCHAR* pParam              = QP_NULL;

  if(n_pStrParam)
  {
    QPCHAR *pTemp = (QPCHAR*)n_pStrParam->c_str();
    //Remove Quote if present at beginning
    if(pTemp[0] == '\"')
      pTemp++;
    QPINT i = 0;
    while(pTemp[i] != '\0')
    {
      i++;
    }
    //Remove Quote if present at end
    if(pTemp[--i] == '\"')
      pTemp[i] = '\0';

    pParam = (QPCHAR *)qpDplMalloc(MEM_IMS_UT, n_pStrParam->length() + 1);
    if(pParam == QP_NULL)
    {
      IMS_MSG_FATAL_0(LOG_IMS_UT,"IMSServConfigGBAAuthUIM::RemoveQoutes | new failed");
      return QP_NULL;
    }  
    (QPVOID)qpDplStrlcpy(pParam, pTemp, (n_pStrParam->length() + 1));
    return pParam;
  }
  return QP_NULL;
}

/************************************************************************
Function IMSServConfigGBAAuthUIM::ResetChallengeStruct()

Description
This function Reset and deletes the pointers held by challenge structure

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID IMSServConfigGBAAuthUIM::ResetChallengeStruct(QP_AUTH_PARAMS& n_stChallengeParams)
{
  for(QPUINT32 i = 0; i < QP_SIP_MAX_DIGEST_CLN_CNT; i++)
  {
    if(QP_NULL != n_stChallengeParams.m_pDigestChallenge[i].pDigestRespose)
    {
      qpDplFree(MEM_IMS_UT, n_stChallengeParams.m_pDigestChallenge[i].pDigestRespose);
      n_stChallengeParams.m_pDigestChallenge[i].pDigestRespose = QP_NULL;
    }
  }
  qpDplMemset(&n_stChallengeParams, 0, sizeof(QP_AUTH_PARAMS));
}

/************************************************************************
Function IMSServConfigGBAAuthUIM::ResetNextChallengeStruct()

Description
This function Reset and deletes the pointers held by next challenge structure

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID IMSServConfigGBAAuthUIM::ResetNextChallengeStruct(QP_IMS_SERV_CONFIG_NEXT_CHALLENGE_PARAMS& n_stNextChallengeParams)
{
  if(QP_NULL != n_stNextChallengeParams.pNextNonce)
  {
    qpDplFree(MEM_IMS_UT, n_stNextChallengeParams.pNextNonce);
    n_stNextChallengeParams.pNextNonce = QP_NULL;
  }
  if(QP_NULL != n_stNextChallengeParams.pAlgorithm)
  {
    qpDplFree(MEM_IMS_UT, n_stNextChallengeParams.pAlgorithm);
    n_stNextChallengeParams.pAlgorithm = QP_NULL;
  }
  if(QP_NULL != n_stNextChallengeParams.pQoP)
  {
    qpDplFree(MEM_IMS_UT, n_stNextChallengeParams.pQoP);
    n_stNextChallengeParams.pQoP = QP_NULL;
  }
  if(QP_NULL != n_stNextChallengeParams.pOpaque)
  {
    qpDplFree(MEM_IMS_UT, n_stNextChallengeParams.pOpaque);
    n_stNextChallengeParams.pOpaque = QP_NULL;
  }
  if(QP_NULL != n_stNextChallengeParams.pRealm)
  {
    qpDplFree(MEM_IMS_UT, n_stNextChallengeParams.pRealm);
    n_stNextChallengeParams.pRealm = QP_NULL;
  }
  if(QP_NULL != n_stNextChallengeParams.pNC)
  {
    qpDplFree(MEM_IMS_UT, n_stNextChallengeParams.pNC);
    n_stNextChallengeParams.pNC = QP_NULL;
  }
  qpDplMemset(&n_stNextChallengeParams, 0, sizeof(QP_IMS_SERV_CONFIG_NEXT_CHALLENGE_PARAMS));
}
/************************************************************************
Function IMSServConfigGBAAuthUIM::CopyReceivedChallengeParams()

Description
This function copies the challenge parameters received in
m_stGBAUaAuthParams data member from m_stOnReqUaAuthParams

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID IMSServConfigGBAAuthUIM::CopyReceivedChallengeParams()
{
  for(QPUINT32 i = 0; i < QP_SIP_MAX_DIGEST_CLN_CNT; i++)
  {
    /* If On Req Params has some value, then these are all related to new challenge received */
    if(QP_NULL != m_stOnReqUaAuthParams.m_pDigestChallenge[i].pDigestRespose)
    {
      /* If Ua Params has some value against this "i", delete and store the newly received ones */
      if(QP_NULL != m_stGBAUaAuthParams.m_pDigestChallenge[i].pDigestRespose)
      {
        qpDplFree(MEM_IMS_UT, m_stGBAUaAuthParams.m_pDigestChallenge[i].pDigestRespose);
        qpDplMemset(&m_stGBAUaAuthParams.m_pDigestChallenge[i], 0, sizeof(DigestChallenge));
      }
      qpDplMemscpy(&m_stGBAUaAuthParams.m_pDigestChallenge[i], sizeof(DigestChallenge), &m_stOnReqUaAuthParams.m_pDigestChallenge[i], sizeof(DigestChallenge));
    }
  }
}
/************************************************************************
Function IMSServConfigGBAAuthUIM::SetGBATLSMode(QPE_IMS_SERV_CONFIG_GBA_TLS_MODE)

Description
This function sets UE's TLS Mode

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID IMSServConfigGBAAuthUIM::SetGBATLSMode(QPE_IMS_SERV_CONFIG_GBA_TLS_MODE n_eGBATLSMode)
{
  IMS_MSG_MED_1(LOG_IMS_UT,"IMSServConfigGBAAuthUIM::SetGBATLSMode to %d",n_eGBATLSMode);
  m_eGBATLSMode = n_eGBATLSMode;
}

/************************************************************************
Function IMSServConfigGBAAuthUIM::ValidateServersRealm(QPCHAR*, const QPTLS_SESS_PROFILE*)

Description
This function validates the Realm ID received in challenge against the
server certificate's domain name

Dependencies
None

Return Value
QP_TRUE/QP_FALSE

Side Effects
None
************************************************************************/
QPBOOL IMSServConfigGBAAuthUIM::ValidateServersRealm(QPCHAR* n_pRealm, QPTLS_SESS_PROFILE* n_pTLSSessProfile)
{
  IMS_MSG_MED_0(LOG_IMS_UT,"IMSServConfigGBAAuthUIM::ValidateServersRealm");
  if(QP_NULL == n_pTLSSessProfile || QP_NULL == n_pRealm)
  {
    IMS_MSG_MED_0(LOG_IMS_UT,"IMSServConfigGBAAuthUIM::ValidateServersRealm | Session Profile/Realm is NULL");
    return QP_FALSE;
  }
  QPTLS_DOMAIN_NAME* pTLSDomainName = qpDplTlsGetDomainName(n_pTLSSessProfile);
  if(QP_NULL == pTLSDomainName || 0 == pTLSDomainName->nNumItems )
  {
    IMS_MSG_MED_0(LOG_IMS_UT,"IMSServConfigGBAAuthUIM::ValidateServersRealm | Domain Name List is NULL/empty ");
    return QP_FALSE;
  }
  IMS_MSG_HIGH_1_STR(LOG_IMS_UT,"IMSServConfigGBAAuthUIM::ValidateServersRealm, Realm received = %s", n_pRealm);
  QPUINT16 nNumItems = pTLSDomainName->nNumItems;
  QPCHAR* pExpandedRealm = QP_NULL;
  
  IMS_MSG_HIGH_1(LOG_IMS_UT,"IMSServConfigGBAAuthUIM::ValidateServersRealm, Number of Items = %d", nNumItems);
  for(QPUINT16 i = 0; i < nNumItems; i++)
  {
    QPCHAR* pDomainName = pTLSDomainName->pList[i];
    QPCHAR* pDomainName2 = pTLSDomainName->pList[i];
    if(QP_NULL != pDomainName)
    {
      IMS_MSG_HIGH_1_STR(LOG_IMS_REGMGR,"IMSServConfigGBAAuthUIM::ValidateServersRealm, DNS = %s", pDomainName);
      QPUINT16 iLen = qpDplStrlen(pDomainName);
      QPUINT16 iLenCount = 0;
      while ('\0' != pDomainName && iLenCount < iLen)
      {
        if(QP_TRUE == QpIsAlphaNum(*pDomainName))
        {
          break;
        }
        pDomainName++;
        iLenCount++;
      }
      if('\0' != pDomainName && iLenCount < iLen)
      {
        IMS_MSG_HIGH_1_STR(LOG_IMS_REGMGR,"IMSServConfigGBAAuthUIM::ValidateServersRealm, while comparing = %s", pDomainName);
        //IPv6 requires special handling and in a special case even FQDN can be present with length 39
        if((iLen == QP_IMS_SERV_IPv6_LEN - 1) && QP_TRUE == QpIsAlphaNum(*pDomainName2) &&
          (QP_TRUE == QpIsAlphaNum(*n_pRealm) || ':' == *n_pRealm))
          /* Special case where IPv6 address starts with ":" */
        {
          if(QP_NULL == pExpandedRealm)
          {
            pExpandedRealm = ExpandInIPv6Format(n_pRealm);
          }
          if(QP_NULL != pExpandedRealm && 0 == qpDplStrnicmp(pDomainName2, pExpandedRealm, (QP_IMS_SERV_IPv6_LEN -1)))
          {
            qpDplFree(MEM_IMS_UT, pExpandedRealm);
            return QP_TRUE;
          }
        }
        else
        {
          QPCHAR* pPtr = qpDplStrstr(n_pRealm, pDomainName);
          if(QP_NULL != pPtr)
          {
            if(QP_NULL != pExpandedRealm)
            {
              qpDplFree(MEM_IMS_UT, pExpandedRealm);
            }
            return QP_TRUE;
          }
        }
      }
    }
  }
  if(QP_NULL != pExpandedRealm)
  {
    qpDplFree(MEM_IMS_UT, pExpandedRealm);
  }
  return QP_FALSE;
}

QPCHAR* IMSServConfigGBAAuthUIM::ExpandInIPv6Format(QPCHAR* n_pRealm)
{
  IMS_MSG_MED_0(LOG_IMS_UT,"IMSServConfigGBAAuthUIM::ExpandInIPv6Format");
  QPCHAR* pExpandedRealm      = QP_NULL;
  QPCHAR* pOctet              = QP_NULL;
  QPCHAR* pPtr                = n_pRealm;
  QPUINT8 totalNumofColon     = 0;
  QPUINT8 remainingNumofColon = 0;
  QPBOOL  bDoubleColonProcessed = QP_FALSE;
  
  if(!n_pRealm)
  {
    return QP_NULL;
  }
  while (QP_NULL != pPtr)
  {
    QPCHAR* pColon = qpDplStrstr(pPtr, QP_IMS_SERV_COLON);
    if(QP_NULL != pColon)
    {
      pPtr = pColon + 1;
      totalNumofColon++;
    }
    else if(QP_NULL == pColon)
      break;
  }
  if(totalNumofColon == 0 || totalNumofColon > 7)
  {
    /* This is a special case where FQDN has a length of QP_IMS_SERV_IPv6_LEN or if something is wrong */
    pExpandedRealm = qpDplStrdup(MEM_IMS_UT, n_pRealm);
    return pExpandedRealm;
  }
  //Proceed with IPv6 expansion
  pExpandedRealm = (QPCHAR *)qpDplMalloc(MEM_IMS_UT, QP_IMS_SERV_IPv6_LEN * sizeof(QPCHAR));
  if(QP_NULL == pExpandedRealm)
  {
    return QP_NULL;
  }
  pOctet = (QPCHAR*)qpDplMalloc(MEM_IMS_UT, 5 * sizeof(QPCHAR));
  if(QP_NULL == pOctet)
  {
    qpDplFree(MEM_IMS_UT, pExpandedRealm);
    return QP_NULL;
  }
  qpDplMemset(pExpandedRealm, 0, QP_IMS_SERV_IPv6_LEN * sizeof(QPCHAR));
  qpDplMemset(pOctet, 0, 5 * sizeof(QPCHAR));
  // Start processing in octets
  QPCHAR* pLastNonColon = QP_NULL;
  if(':' != *n_pRealm)
  {
    pPtr = n_pRealm;
    pLastNonColon = n_pRealm;
  }
  /* Special case handling when IPv6 starts with ":" like "::192:168:0:1" */
  else
  {
    pPtr = n_pRealm + 1;
    pLastNonColon = n_pRealm + 1;
    totalNumofColon--;
  }

  remainingNumofColon = totalNumofColon;
  while (QP_NULL != pPtr)
  {
    QPCHAR* pCurrColon = qpDplStrstr(pPtr, QP_IMS_SERV_COLON);
    if(QP_NULL != pCurrColon && QP_NULL != pLastNonColon)
    {
      pPtr = pCurrColon + 1;
      switch (pCurrColon - pLastNonColon)
      {
      case 4 :
        qpDplMemset(pOctet, 0, 5 * sizeof(QPCHAR));
        (QPVOID)qpDplStrlncpy(pOctet, pLastNonColon, 5, 4);
        (QPVOID)qpDplStrlcat(pExpandedRealm, pOctet, QP_IMS_SERV_IPv6_LEN);
        (QPVOID)qpDplStrlcat(pExpandedRealm, QP_IMS_SERV_COLON, QP_IMS_SERV_IPv6_LEN);
        remainingNumofColon--;
        break;
      case 3 :
        qpDplMemset(pOctet, 0, 5 * sizeof(QPCHAR));
        (QPVOID)qpDplStrlncpy(pOctet, pLastNonColon, 5, 3);
        (QPVOID)qpDplStrlcat(pExpandedRealm, (QPCHAR*)"0", QP_IMS_SERV_IPv6_LEN);
        (QPVOID)qpDplStrlcat(pExpandedRealm, pOctet, QP_IMS_SERV_IPv6_LEN);
        (QPVOID)qpDplStrlcat(pExpandedRealm, QP_IMS_SERV_COLON, QP_IMS_SERV_IPv6_LEN);
        remainingNumofColon--;
        break;
      case 2 :
        qpDplMemset(pOctet, 0, 5 * sizeof(QPCHAR));
        (QPVOID)qpDplStrlncpy(pOctet, pLastNonColon, 5, 2);
        (QPVOID)qpDplStrlcat(pExpandedRealm, (QPCHAR*)"00", QP_IMS_SERV_IPv6_LEN);
        (QPVOID)qpDplStrlcat(pExpandedRealm, pOctet, QP_IMS_SERV_IPv6_LEN);
        (QPVOID)qpDplStrlcat(pExpandedRealm, QP_IMS_SERV_COLON, QP_IMS_SERV_IPv6_LEN);
        remainingNumofColon--;
        break;
      case 1 :
        qpDplMemset(pOctet, 0, 5 * sizeof(QPCHAR));
        (QPVOID)qpDplStrlncpy(pOctet, pLastNonColon, 5, 1);
        (QPVOID)qpDplStrlcat(pExpandedRealm, (QPCHAR*)"000", QP_IMS_SERV_IPv6_LEN);
        (QPVOID)qpDplStrlcat(pExpandedRealm, pOctet, QP_IMS_SERV_IPv6_LEN);
        (QPVOID)qpDplStrlcat(pExpandedRealm, QP_IMS_SERV_COLON, QP_IMS_SERV_IPv6_LEN);
        remainingNumofColon--;
        break;
      case 0 :
        for(QPUINT8 i = 0; i < 8 - totalNumofColon && !bDoubleColonProcessed; i++)
        {
          (QPVOID)qpDplStrlcat(pExpandedRealm, (QPCHAR*)"0000:", QP_IMS_SERV_IPv6_LEN);
        }
        bDoubleColonProcessed = QP_TRUE;
        remainingNumofColon--;
        break;
      default:
        IMS_MSG_HIGH_0(LOG_IMS_REGMGR,"IMSServConfigGBAAuthUIM::ExpandInIPv6Format | Shouldn't come here");
        break;
      }
      pLastNonColon = pCurrColon+1;
      pCurrColon = QP_NULL;
    }
    else if(QP_NULL == pCurrColon && remainingNumofColon == 0)
    {
      /* Copy the last octet */
      if(4 == qpDplStrlen(pPtr))
      {
        (QPVOID)qpDplStrlcat(pExpandedRealm, pPtr, QP_IMS_SERV_IPv6_LEN);
      }
      else if (3 == qpDplStrlen(pPtr))
      {
        (QPVOID)qpDplStrlcat(pExpandedRealm, (QPCHAR*)"0", QP_IMS_SERV_IPv6_LEN);
        (QPVOID)qpDplStrlcat(pExpandedRealm, pPtr, QP_IMS_SERV_IPv6_LEN);
      }
      else if(2 == qpDplStrlen(pPtr))
      {
        (QPVOID)qpDplStrlcat(pExpandedRealm, (QPCHAR*)"00", QP_IMS_SERV_IPv6_LEN);
        (QPVOID)qpDplStrlcat(pExpandedRealm, pPtr, QP_IMS_SERV_IPv6_LEN);
      }
      else if(1 == qpDplStrlen(pPtr))
      {
        (QPVOID)qpDplStrlcat(pExpandedRealm, (QPCHAR*)"000", QP_IMS_SERV_IPv6_LEN);
        (QPVOID)qpDplStrlcat(pExpandedRealm, pPtr, QP_IMS_SERV_IPv6_LEN);
      }
      // Spl case like "2000::"
      else if(0 == qpDplStrlen(pPtr))
      {
        (QPVOID)qpDplStrlcat(pExpandedRealm, (QPCHAR*)"0000", QP_IMS_SERV_IPv6_LEN);
        (QPVOID)qpDplStrlcat(pExpandedRealm, pPtr, QP_IMS_SERV_IPv6_LEN);
      }
      break;
    }
    else
    {
      IMS_MSG_HIGH_0(LOG_IMS_REGMGR,"IMSServConfigGBAAuthUIM::ExpandInIPv6Format | Unexpected state");
      qpDplFree(MEM_IMS_UT, pExpandedRealm);
      pExpandedRealm = qpDplStrdup(MEM_IMS_UT, n_pRealm);
      break;
    }
  }
  qpDplFree(MEM_IMS_UT, pOctet);
  if(QP_NULL != pExpandedRealm)
  {
    IMS_MSG_HIGH_1_STR(LOG_IMS_REGMGR,"IMSServConfigGBAAuthUIM::ExpandInIPv6Format = %s", pExpandedRealm);
  }
  return pExpandedRealm;
}


QPVOID IMSServConfigGBAAuthUIM::GetUaAuthParams(QP_AUTH_PARAMS* n_pAuthParams)
{
  IMS_MSG_HIGH_0(LOG_IMS_REGMGR,"IMSServConfigGBAAuthUIM::GetUaAuthParams");
  if(n_pAuthParams)
  {
	for(QPUINT32 i = 0; i < QP_SIP_MAX_DIGEST_CLN_CNT; i++)
    {
      if(QP_NULL != m_stGBAUaAuthParams.m_pDigestChallenge[i].pDigestRespose)
      {
        if(QP_NULL != n_pAuthParams->m_pDigestChallenge[i].pDigestRespose)
        {
          qpDplFree(MEM_IMS_UT, n_pAuthParams->m_pDigestChallenge[i].pDigestRespose);
		  n_pAuthParams->m_pDigestChallenge[i].pDigestRespose = QP_NULL;
        }
		qpDplMemset(&n_pAuthParams->m_pDigestChallenge[i],0,sizeof(DigestChallenge));
		n_pAuthParams->m_pDigestChallenge[i].pDigestRespose = qpDplStrdup(MEM_IMS_UT,m_stGBAUaAuthParams.m_pDigestChallenge[i].pDigestRespose);
		n_pAuthParams->m_pDigestChallenge[i].iDigestResLen = m_stGBAUaAuthParams.m_pDigestChallenge[i].iDigestResLen;
		n_pAuthParams->m_pDigestChallenge[i].m_DlgChlType = m_stGBAUaAuthParams.m_pDigestChallenge[i].m_DlgChlType;
//		qpDplMemscpy(&n_pAuthParams->m_pDigestChallenge[i],sizeof(DigestChallenge),&m_stGBAUaAuthParams.m_pDigestChallenge[i],sizeof(DigestChallenge));
      }      
  	}
  }
}

QPUINT IMSServConfigGBAAuthUIM::QpIsAlphaNum(QPUINT c)
{
  return ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c >= '0' && c <= '9')) ? QP_TRUE : QP_FALSE;
}

