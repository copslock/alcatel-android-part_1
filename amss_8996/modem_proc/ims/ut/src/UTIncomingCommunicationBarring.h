/************************************************************************
 Copyright (C) 2013 Qualcomm Technologies Incorporation .All Rights Reserved.

 File Name      : UTIncomingCommunicationBarring.h
 Description    : Incoming Communication Barring class for UT operations
 

 ========================================================================
    Date       |   Author's Name    |  BugID  |        Change Description
 ========================================================================
 27-09-2013     Sreenidhi          552121     Support for Communication Barring over UT interface
 --------------------------------------------------------------------------------------------------
 02-12-2013     Priyank            539082     Perform a GET operation of the entire server document initially to avoid rule id mismatch
 ----------------------------------------------------------------------------------------
 26-12-2013     Sreenidhi          581666     Interrogating CF HTTP GET query will be implemented based on last possible node level
                                              (rule type such as CFU, CFB, CFNRC, CFNRY)
 ----------------------------------------------------------------------------------------
 13-01-2014     Sreenidhi          596796     CB query after deactivation is failing, In QMI API there is no deactivated indication param											  
--------------------------------------------------------------------------------------------------
 29-01-2014     Priyank            594699     Number specific Incoming Call Barring Related XCAP Configuration
 --------------------------------------------------------------------------------------------------
 05-02-2014     Priyank            605477     Ut marshal/unmarshal support of namespace at service level
 --------------------------------------------------------------------------------------------------
 10-02-2014     Sreenidhi          609314     [9x15 LE5.0 KDDI] ICB Num registration for previous failed number is failing 
  --------------------------------------------------------------------------------------------------
 16-04-2014     Sreenidhi          645056     Adding <conditions> element if not present for CFU/CB unconditional cases
 ------------------------------------------------------------------------------- 
 15-05-2014     Priyank             663757     UE crashes if CDIV has CFU in activated state and media element present
--------------------------------------------------------------------------------------------------------
 06-06-2014     Sreenidhi           676530     Address Compiler Warnings in RM and UT modules 
  -------------------------------------------------------------------------------
 11-07-2014     Sreenidhi           685419     FR 21497: No phone context is present for local target number for Call forwarding on UT/XCAP 
---------------------------------------------------------------------------------------------
 30-10-2014     Sreenidhi          744344     Deletion failure of registered SIP URI for ICB 
************************************************************************/

#ifndef __UTINCOMINGCOMMUNICATIONBARRING_H__
#define __UTINCOMINGCOMMUNICATIONBARRING_H__

#include "UTXMLSSHandler.h"
#include "XMLCommunicationBarring.h"

#include "UTBaseCommunicationBarring.h"

//Forward Declaration
class qp_incoming_communication_barring;
class qp_ns_common_policy_ruleType;

class UTIncomingCommunicationBarring : public UTXMLSSHandler
{
public:
  UTIncomingCommunicationBarring();
  ~UTIncomingCommunicationBarring();
  /* Procees the SS request. Returns object of class QP_SS_REQPARAMS*/
  QP_SS_REQPARAMS* ProcessSSRequest(QP_SUPP_SRV_CB_DATA*, QP_CF_REQUEST_INFO*);
  /* Procees the SS response received from the N/w.
  ** Returns bool for response parsing and indication data in QP_SUPS_IND_DATA
  */
  QPBOOL ProcessSSResponse(QPCHAR*, QP_SUPS_IND_DATA*, QPBOOL& n_bCompleteXML);
  /* Returns class name for the Class*/
  QPCHAR* ClassName();

  QPVOID HandleICBErrorScenario();
  /* Serach function to see if the ICB number is already present in the list */
  static int qpUTSearchICBNUmber(QPVOID* pItem, QPVOID* pCompareVal);
  static QPCHAR* qpICBExtractNumber(QPCHAR* pURI);

private:
  //Copy Constructor and Operator = 
  UTIncomingCommunicationBarring(const UTIncomingCommunicationBarring&);
  UTIncomingCommunicationBarring& operator=(const UTIncomingCommunicationBarring &);

  QPVOID RegistrationRequest(QP_SUPP_SRV_CB_DATA*, QP_CF_REQUEST_INFO*);
  QPVOID ActivationRequest(QP_SUPP_SRV_CB_DATA*, QP_CF_REQUEST_INFO*);
  QPVOID DeActivationRequest(QP_SUPP_SRV_CB_DATA*, QP_CF_REQUEST_INFO*);
  QPVOID InterrogationRequest(QP_SUPP_SRV_CB_DATA*);
  QPVOID ErasureRequest(QP_SUPP_SRV_CB_DATA*, QP_CF_REQUEST_INFO*);
  QPVOID Init();
  /* Extracts the XML contents and stores in m_Rule_Parameter */
  QPBOOL GetContents();
  /* Extracts the XML condition values for a given service and stores in m_Rule_Parameter */
  QPBOOL GetConditionValues(qp_ns_common_policy_conditionsType*, QP_ICB_RULE_PARAMETERS*);
  /* Extracts the XML action values for a given service and stores in m_Rule_Parameter */
  QPVOID GetActionValues(qp_ns_common_policy_extensibleType*, QP_ICB_RULE_PARAMETERS*);
  /* Creates XML for Register request to be sent to server from m_Rule_Parameter*/
  QPVOID CreateRegisterRequestElements(QPE_CB_IN_CONDITION);
  /* Creates XML for Activation/DeActivation request to be sent to server from m_Rule_Parameter*/
  QPVOID CreateActOrDeActRequestElements(QPE_CB_IN_CONDITION, QPBOOL, QP_CF_REQUEST_INFO*, QP_ICB_RULE_PARAMETERS* n_pRuleParam = QP_NULL, QPBOOL n_bIsConditionAbsentForUnconditional = QP_FALSE);
  /* Creates XML for Erase request to be sent to server from m_Rule_Parameter*/
  QPVOID CreateEraseRequestElements(QPE_CB_IN_CONDITION);
  /* Sets the CB contents to be sent to CM for the request received*/
  QPVOID SetCBContents(QP_SUPS_IND_DATA*);
  QPVOID HandleICBRegistration(QP_SUPP_SRV_CB_DATA*);
  QPVOID HandleICBErasure(QP_SUPP_SRV_CB_DATA*);
  QPVOID HandleICBAcOrDeActRequest(QP_SUPP_SRV_CB_DATA*, QPBOOL, QP_CF_REQUEST_INFO*);
  QPCHAR* GenerateRuleId();
  /* Creates XML for Request to be sent to server at Service Node */
  QPVOID CreateRequestElementsAtServNode();
  qp_ns_common_policy_ruleType* GenerateRuleTypeNode(QPE_CB_IN_CONDITION, QP_ICB_RULE_PARAMETERS*);
  QPVOID FormulateTargetURI(QP_CALLED_PARTY_NO*);
  QPVOID SetICBCBContents(QP_SUPS_IND_DATA*);
  QPVOID FillICBNumber(QP_ICB_RULE_PARAMETERS*, QP_SUPS_CB_NUM_INFO*, QPUINT8&);
  QPVOID GeneratePhoneContextURI(QPCHAR* n_pPhoneContextURI);

private:
  qp_incoming_communication_barring*                   m_pICBRequestElement;
  qp_incoming_communication_barring*                   m_pICBResponseElement;
  qp_ns_common_policy_ruleType*                        m_pICBRuleType;
  QP_SUPS_CMD_E_TYPE                                   m_eTypeOfOperation;
  QPBOOL                                               m_bRequestAtServNode;
  QPCHAR*                                              m_aFwdURI;
  QPBOOL                                               m_bIsRuleDeactivated;
  QPUINT8                                              m_iRegListSize;
  QP_DPL_LIST_TYPE*                                    m_pICBEraseNumberList;
};

#endif // __UTCOMMUNICATIONBARRING_H__
