/************************************************************************
 Copyright (C) 2013 Qualcomm Technologies Incorporation .All Rights Reserved.

 File Name      : UTBaseCommunicationBarring.h
 Description    : Communication Barring Base Header File
 

 ========================================================================
    Date         |   Author's Name    |  BugID  |        Change Description
 ========================================================================
  27-09-2013      Sreenidhi           552121       Support for Communication Barring over UT interface
 --------------------------------------------------------------------------------------------------
  02-12-2013      Priyank             539082       Perform a GET operationof the entire server document initially to avoid rule id mismatch
  --------------------------------------------------------------------------------------------------
  29-01-2014      Priyank             594699       Number specific Incoming Call Barring Related XCAP Configuration
  --------------------------------------------------------------------------------------------------
  05-02-2014      Priyank             605477       Ut marshal/unmarshal support of namespace at service level
 --------------------------------------------------------------------------------------------------
  20-06-2014      Sreenidhi          681985     limitation of 40 bytes for rule id 
  -------------------------------------------------------------------------------
  11-07-2014      Sreenidhi          685419     FR 21497: No phone context is present for local target number for Call forwarding on UT/XCAP 
************************************************************************/

#ifndef __UTBASECOMMUNICATIONBARRING_H__
#define __UTBASECOMMUNICATIONBARRING_H__

#include <qpPlatformConfig.h>
#include <qpDplCallCtrl.h>
#include "qpDplList.h"

#define MAX_ID_LENGTH  64
#define MAX_CONDITION_STR_LEN 40
#define MAX_FORWARD_STR_LEN  40
#define MAX_CB_IN_CONDITION_LIMIT 4
#define MAX_CB_OUT_CONDITION_LIMIT 3
#define MAX_CB_IN_DN_OPRT1_LIMIT 30
#define MAX_TIME_UNIT_LEN 5

// Incoming Communication Barring related string defines
typedef enum qpCBInCondition
{
  QP_IN_BARRING_NONE = -1,
  QP_BARRING_ALL_INCOMING_CALLS,
  QP_BARRING_INCOMING_CALLS_IN_ROAMING,
  QP_BARRING_INCOMING_CALLS_ANONYMOUS,
  QP_INCOMING_NUMBER_SPECIFIC_BARRING
}QPE_CB_IN_CONDITION;

// Outgoing Communication Barring related string defines
typedef enum qpCBOutCondition
{
  QP_OUT_BARRING_NONE = -1,
  QP_BARRING_ALL_OUTGOING_CALLS,
  QP_BARRING_OUTGOING_INTERNATIONAL_CALLS,
  QP_BARRING_OUTGOING_INTERNATIONAL_CALLS_EXHC
}QPE_CB_OUT_CONDITION;

/* static const buffer to all the SS condition*/
static const QPCHAR CB_IN_CONDITION[][MAX_CONDITION_STR_LEN] =
{
  "icb-unconditional",
  "roaming",
  "anonymous",
  ""
};

static const QPCHAR CB_OUT_CONDITION[][MAX_CONDITION_STR_LEN] =
{
  "ocb-unconditional",
  "international",
  "international-exHC"
};

typedef struct qp_cb_rule_parameters
{
  QPCHAR                    m_RuleId[MAX_ID_LENGTH];
  QPCHAR                    m_Condition[MAX_CONDITION_STR_LEN];
  QPBOOL                    m_RuleDeActivated;
  QPBOOL                    m_AllowValue;
  QP_SS_OPERATION_CODE_T    code;
}QP_CB_RULE_PARAMETERS;

typedef struct qp_icb_rule_parameters
{
  QP_DPL_LIST_LINK_TYPE       link;                               /* link to be used with linked list */
  QPCHAR                      m_RuleId[MAX_ID_LENGTH];
  QPBOOL                      bIsRuleDeActivated;
  QPBOOL                      bAllowValue;
  QPCHAR*                     m_BlockURI;
  QPCHAR*                     m_SipURI;
  QPBOOL                      bURIPresent;
  QP_SS_OPERATION_CODE_T      code;
  QPE_CB_IN_CONDITION         eConditionType;
}QP_ICB_RULE_PARAMETERS;

#endif
