/************************************************************************
 Copyright (C) 2013 Qualcomm Technologies Incorporation .All Rights Reserved.

 File Name      : UTTerminatingIdentityPresentation.h
 Description    : TIP class for UT operations
 

 ========================================================================
    Date    |   Author's Name    |  BugID  |        Change Description
 ========================================================================
 24-03-2014     asharm             626051       FR 19676:Terminating Identification Restriction (TIR)
************************************************************************/

#ifndef __UTTERMINATINGIDENTITYPRESENTATIONRESTRICTION_H__
#define __UTTERMINATINGIDENTITYPRESENTATIONRESTRICTION_H__

#include "UTXMLSSHandler.h"
#include "XMLTIR.h"
#include <qpPlatformConfig.h>

class qp_tir;
class UTTerminatingIdentityPresentationRestriction : public UTXMLSSHandler
{
public:
  UTTerminatingIdentityPresentationRestriction();
  ~UTTerminatingIdentityPresentationRestriction();
  /* Procees the SS request. Returns object of class QP_SS_REQPARAMS*/
  QP_SS_REQPARAMS* ProcessSSRequest(QP_SUPP_SRV_CB_DATA*, QP_CF_REQUEST_INFO*);
  /* Procees the SS response received from the N/w.
  Returns bool for response parsing and indication data in QP_SUPS_IND_DATA
  */
  QPBOOL ProcessSSResponse(QPCHAR*, QP_SUPS_IND_DATA*, QPBOOL& n_bCompleteXML);
  /* Returns class name for the Class*/
  QPCHAR* ClassName();

private:

  //Copy Constructor and Operator = 
  UTTerminatingIdentityPresentationRestriction(const UTTerminatingIdentityPresentationRestriction&);
  UTTerminatingIdentityPresentationRestriction& operator=(const UTTerminatingIdentityPresentationRestriction &);

  
  QPVOID RegistrationRequest(QP_SUPP_SRV_CB_DATA*, QP_CF_REQUEST_INFO*){}
  QPVOID ActivationRequest(QP_SUPP_SRV_CB_DATA*, QP_CF_REQUEST_INFO*);
  QPVOID DeActivationRequest(QP_SUPP_SRV_CB_DATA*, QP_CF_REQUEST_INFO*);
  QPVOID InterrogationRequest(QP_SUPP_SRV_CB_DATA*);
  QPVOID ErasureRequest(QP_SUPP_SRV_CB_DATA*, QP_CF_REQUEST_INFO*){}
  QPVOID Init();
  

private:
  qp_tir*      m_pRequestElement;
  qp_tir*      m_pResponseElement; 
};

#endif // __UTTERMINATINGIDENTITYPRESENTATION_H__