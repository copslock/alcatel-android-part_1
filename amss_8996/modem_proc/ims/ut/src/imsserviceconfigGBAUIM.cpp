/************************************************************************
 Copyright (C) 2012 Qualcomm Technologies Incorporation .All Rights Reserved.

 File Name      : IMSServiceConfigGBAUIM.cpp
 Description    :

 Revision History
 ========================================================================
   Date      |   Author's Name    |  BugID  |        Change Description
=========================================================================
-------------------------------------------------------------------------------
12-May-2015   Sreenidhi     789667      FR25901: Ut changes for migrating IMS over to UIM GBA solution
-------------------------------------------------------------------------------
11-Jun-15        asharm      827203   FR 27504: support Ub over HTTPS and redirection
************************************************************************/

#include "imsserviceconfigGBAUIM.h"
#include "SingoIMSServConfigEvManager.h"
#include "qpIMSServConfigGBAAuthUIM.h"
#include <qpdefines.h>


#define QP_GBA_MAX_FAILURE_COUNT     3
#define QP_STALE_TRUE                (QPCHAR*)"TRUE"
#define QP_GBA_404_FAILURE_ON_UB     998

/************************************************************************
Function IMSServiceConfigGBAUIM::IMSServiceConfigGBAUIM()

Description
Constructor of the class

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
IMSServiceConfigGBAUIM::IMSServiceConfigGBAUIM():IMSServiceConfig()
{
  m_pHttpContent = QP_NULL;
  m_pRealm = QP_NULL;
  m_pNafFqdn = QP_NULL;
  m_pImsi = QP_NULL;
  m_pGBAAuth = QP_NULL;
  m_iRspAuthFailureCount = 0;
  m_iPerformGBAUbCount = 0;
  m_eHttpConnectionType = QP_HTTP_CONNECTION;
  m_bUaAuthInProgress = QP_FALSE;
  m_bUbAuthInProgress = QP_FALSE;
  m_bUaAuthRespIncluded = QP_FALSE;
  m_eGBAUbMode = QPE_IMS_SERV_CONFIG_GBA_UB_MODE_NONE;
  m_eGBATLSMode = QPE_IMS_SERV_CONFIG_GBA_TLS_MODE_NONE;
  m_eGBAUbTLSMode = QPE_IMS_SERV_CONFIG_GBA_TLS_MODE_OVER_UB_NONE;
  qpDplMemset(&m_stHttpMessageContent, 0, sizeof(QP_HTTP_MESSAGE_CONTENT));
  qpDplMemset(&m_strNVConfigUserName, 0, sizeof(QP_REG_CONFIG_USER_NAME_LEN));
  qpDplMemset(&m_strNVConfigPassword, 0, sizeof(QP_REG_CONFIG_PASSWORD_LEN));
  qpDplMemset(&m_stGBAUbAttributes, 0, sizeof(QP_IMS_SERV_CONFIG_GBA_UIM_UB_ATTRIBURES));
  qpDplMemset(&m_stUaOnReqAuthParams, 0, sizeof(QP_IMS_SERV_CONFIG_UA_ON_REQ_CHALLENGE_PARAMS));
  qpDplMemset(&m_stUserData, 0, sizeof(QP_IMS_SERV_CONFIG_USER_DATA));
  qpDplMemset(&m_strApnName,0, sizeof(QP_APN_NAME_LEN));
  qpDplMemset(&m_strApn2Name,0, sizeof(QP_APN_NAME_LEN));
  m_stUserData.pthis = this;
  m_stUserData.pHandleUbAuthCallBack = qpHandleUBAuthCallBack;
  m_iUaSecProtocolId = 0;
  m_iCSList = 0;
  qpDplMemset(&m_stUaAuthParams,0,sizeof(QP_AUTH_PARAMS));
}

/************************************************************************
Function IMSServiceConfigGBAUIM::~IMSServiceConfigGBAUIM()

Description
Destructor of the class

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
IMSServiceConfigGBAUIM::~IMSServiceConfigGBAUIM()
{
  if(QP_NULL != m_pGBAAuth)
  {
    IMS_DELETE(m_pGBAAuth,MEM_IMS_UT);
    m_pGBAAuth = QP_NULL;
  }
  if(QP_NULL != m_pHttpContent)
  {
    qpDplFree(MEM_IMS_UT,m_pHttpContent);
    m_pHttpContent = QP_NULL;
  }
  if(QP_NULL != m_pRealm)
  {
    qpDplFree(MEM_IMS_UT,m_pRealm);
    m_pRealm = QP_NULL;
  }
  if(QP_NULL != m_pNafFqdn)
  {
    qpDplFree(MEM_IMS_UT,m_pNafFqdn);
	m_pNafFqdn = QP_NULL;
  }
  if(QP_NULL != m_pImsi)
  {
    qpDplFree(MEM_IMS_UT,m_pImsi);
    m_pImsi = QP_NULL;
  }
  ResetGBAUbAttributes();
  ResetUaAuthParams(m_stUaAuthParams);
}

/************************************************************************
Function IMSServiceConfigGBAUIM::Init()

Description
Initialize the class the class

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID IMSServiceConfigGBAUIM::Init(QP_IMS_SERVICE_CONFIG_NV * n_pConfig, PDNBRINGUPTYPE ePDNType)
{
  IMS_MSG_MED_0(LOG_IMS_UT,"IMSServiceConfigGBAUIM::Init Entered");
  if(n_pConfig == QP_NULL)
  {
    IMS_MSG_FATAL_0(LOG_IMS_UT,"IMSServiceConfigGBAUIM::Init | n_pConfig is NULL");
    return;
  }

  m_pPDPHandler->Init(n_pConfig, ePDNType);
  m_eGBAUbMode = n_pConfig->eGBAUbMode;
  m_eGBATLSMode = n_pConfig->eGBATLSMode;
  m_eGBAUbTLSMode = n_pConfig->eGBAUbTLSMode;

  qpDplStrlncpy(m_strApnName,n_pConfig->imsServConfigProfileInfo.strApnName,
  	QP_APN_NAME_LEN,qpDplStrlen(n_pConfig->imsServConfigProfileInfo.strApnName));
  qpDplStrlncpy(m_strApn2Name,n_pConfig->imsServConfigProfileInfo_APN2.strApnName,
  	QP_APN_NAME_LEN,qpDplStrlen(n_pConfig->imsServConfigProfileInfo_APN2.strApnName));
  
  SingoIMSServConfigEvManager* pEvMgr = QP_NULL;
  pEvMgr = SINGLETON_OBJECT(SingoIMSServConfigEvManager);
  InitGBASupport();
  if(pEvMgr != QP_NULL)
  {
    pEvMgr->addEventListener(IMS_SERV_CONFIG_EVENT_LISTENER, this, (QPVOID*)m_pPDPHandler);
  }
}

/************************************************************************
Function IMSServiceConfigGBAUIM::InitGBASupport()

Description
Initialize the data members related to GBA Support

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID IMSServiceConfigGBAUIM::InitGBASupport()
{
  IMS_MSG_LOW_0(LOG_IMS_UT,"IMSServiceConfigGBAUIM::InitGBASupport Entered");

  IMS_NEW(IMSServConfigGBAAuthUIM, MEM_IMS_UT, m_pGBAAuth, ());

  m_pRealm = (QPCHAR*)qpDplMalloc(MEM_IMS_UT, QP_MAX_SERVER_URI_SIZE * sizeof(QPCHAR));
  if(QP_NULL == m_pRealm)
  {
    IMS_MSG_FATAL_0(LOG_IMS_UT,"IMSServConfigGBAAuthUIM::InitGBASupport | Malloc Failure in m_pRealm");
    return;
  }
  qpDplMemset(m_pRealm, 0 , QP_MAX_SERVER_URI_SIZE * sizeof(QPCHAR));

  m_pNafFqdn = (QPCHAR*)qpDplMalloc(MEM_IMS_UT, QP_MAX_SERVER_URI_SIZE * sizeof(QPCHAR));
  if(QP_NULL == m_pNafFqdn)
  {
    IMS_MSG_FATAL_0(LOG_IMS_UT,"IMSServConfigGBAAuthUIM::InitGBASupport | Malloc Failure in m_pNafFqdn");
    return;
  }
  qpDplMemset(m_pNafFqdn, 0 , QP_MAX_SERVER_URI_SIZE * sizeof(QPCHAR));
  m_pImsi = (QPDPL_IMSI_STRUCT*)qpDplMalloc(MEM_IMS_UT, sizeof(QPDPL_IMSI_STRUCT));
  if(QP_NULL == m_pImsi)
  {
    IMS_MSG_FATAL_0(LOG_IMS_UT,"IMSServConfigGBAAuthUIM::InitGBASupport | Malloc Failure in m_pImsi");
    return;
  }
  qpDplMemset(m_pImsi, 0, sizeof(QPDPL_IMSI_STRUCT));
  if( QPE_IMS_SERV_CONFIG_GBA_TLS_MODE_NONE != m_eGBATLSMode)
  {
    m_pGBAAuth->SetGBATLSMode(m_eGBATLSMode);
  }
}

/************************************************************************
Function IMSServiceConfigGBAUIM::DeInitGBASupport()

Description
De-Initialize the data members related to GBA Support

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID IMSServiceConfigGBAUIM::DeInitGBASupport()
{
  IMS_MSG_LOW_0(LOG_IMS_UT,"IMSServiceConfigGBAUIM::DeInitGBASupport Entered");
  if(QP_NULL != m_pGBAAuth)
  {
    IMS_DELETE(m_pGBAAuth,MEM_IMS_UT);
    m_pGBAAuth = QP_NULL;
  }
  if(QP_NULL != m_pRealm)
  {
    qpDplFree(MEM_IMS_UT,m_pRealm);
    m_pRealm = QP_NULL;
  }
  if(QP_NULL != m_pNafFqdn)
  {
    qpDplFree(MEM_IMS_UT,m_pNafFqdn);
	m_pNafFqdn = QP_NULL;
  }
  if(QP_NULL != m_pImsi)
  {
    qpDplFree(MEM_IMS_UT,m_pImsi);
    m_pImsi = QP_NULL;
  }
  ResetGBAUbAttributes();
}

/************************************************************************
Function IMSServiceConfigGBAUIM::NotifyHttpResponse()

Description
Call Back handler for all the response from the HTTP module

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID IMSServiceConfigGBAUIM::NotifyHttpResponse(HttpConnection* pHttpConnection)
{
  IMS_MSG_MED_0(LOG_IMS_UT,"IMSServiceConfigGBAUIM::NotifyHttpResponse");
  if(pHttpConnection == m_pHttpConnection)
  {
    m_iStatusCode = pHttpConnection->GetStatusCode();
    IMS_MSG_MED_2(LOG_IMS_UT,"IMSServiceConfigGBAUIM::NotifyHttpResponse m_bUaAuthInProgress = %d, m_eGBAUbMode = %d ",
      m_bUaAuthInProgress, m_eGBAUbMode);
    HandleGBAResponse();
  }
}
/************************************************************************
Function IMSServiceConfigGBAUIM::HandleGBAResponse()

Description
This function handles all the responses when GBA is enabled

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID IMSServiceConfigGBAUIM::HandleGBAResponse()
{
  IMS_MSG_MED_0(LOG_IMS_UT,"IMSServiceConfigGBAUIM::HandleGBAResponse");
  QPCHAR* pInAuthenticateHeader         = QP_NULL;
  QPBOOL  bRetValue                     = QP_FALSE;
  QPBOOL  bStale                        = QP_FALSE;
  bStale = IsStalePresent();
  
  if((m_iStatusCode == 401 || m_iStatusCode == 407))
  {
    /* Extract the WWW-Authenticate header and send it to 
    ** IMSServConfigGBAAuth to calculate the response */
    pInAuthenticateHeader = GetHeaderValue( (QPCHAR*)HttpHeadersStrings[QP_AUTHENTICATE] );

    if(QP_TRUE == m_bUaAuthInProgress)
    {
      IMS_MSG_LOW_0(LOG_IMS_UT,"IMSServiceConfigGBAUIM::HandleGBAResponse - m_bUaAuthInProgress");
      m_iUaSecProtocolId = 0;
	  m_iCSList = 0;
      /* If a challenge is received, then it means reset the previous Nonce if present as a new nonce would have come in this challenge */
      m_pGBAAuth->ResetPreviousAuthParams();      

      /* This scenario will arise when UE has sent the request with valid credentials but server wants UE to revalidate its 
      ** credentials on the BSF server.
      */
      qpDplMemset(&m_stUaOnReqAuthParams, 0, sizeof(QP_IMS_SERV_CONFIG_UA_ON_REQ_CHALLENGE_PARAMS));
      QPTLS_SESS_PROFILE* pTLSSessProfile = m_pHttpConnection->GetHttpTLSSessionProfile();
	  QPTLS_PARAM_TYPE *pTlsParamData = (QPTLS_PARAM_TYPE*)qpDplMalloc(MEM_IMS_UT,sizeof(QPTLS_PARAM_TYPE));
	  if(!pTlsParamData)
	  {
	    IMS_MSG_FATAL_0(LOG_IMS_UT,"IMSServiceConfigGBAUIM::HandleGBAResponse - Malloc failure for pTlsParamData");
        return;
	  }
	  qpDplMemset(pTlsParamData,0,sizeof(QPTLS_PARAM_TYPE));
      bRetValue = m_pGBAAuth->ValidateAuthenticationInterface(m_stUaOnReqAuthParams, pInAuthenticateHeader, pTLSSessProfile);

	  if((QPE_IMS_SERV_CONFIG_GBA_TLS_SharedKey_CertBased == m_eGBATLSMode ||
       QPE_IMS_SERV_CONFIG_GBA_TLS_On_Demand == m_eGBATLSMode)&&
	   (TLS_ERROR_OK == qpDplTlsGetParam(pTLSSessProfile,TLS_SES_CIPHER_SUITE,pTlsParamData)))
      {
        m_iUaSecProtocolId = (QPUINT16)pTlsParamData->CSid;
		m_iCSList = GetCSList();
	    IMS_MSG_LOW_2(LOG_IMS_UT,"IMSServiceConfigGBAUIM::HandleGBAResponse - Setting seprotocol ID - %d cslist - %d", m_iUaSecProtocolId, m_iCSList);
      }
	  qpDplFree(MEM_IMS_UT,pTlsParamData);
      if(QP_TRUE == bRetValue)
      {
        if(QP_TRUE == m_stUaOnReqAuthParams.bPerformUbAuth && QP_TRUE != m_stUaOnReqAuthParams.bIsParamIncorrect && 
          (QP_TRUE == m_bUaAuthRespIncluded || QP_FALSE == m_stUaOnReqAuthParams.bNoncePresent) && QP_TRUE != bStale)
        {
          /* Increase the count, as NAF server is redirecting UE to go to BSF server 
          ** When the count reaches 3 w/o getting successful response from NAF server, break the loop
          */
          m_iPerformGBAUbCount++;
		  QPUINT8 iRetVal = QP_SUCCESS;
          IMS_MSG_MED_1(LOG_IMS_UT,"IMSServiceConfigGBAUIM::HandleGBAResponse, Perform GBA Ub Authentication Trying again count = %d ", m_iPerformGBAUbCount);
          if(m_iPerformGBAUbCount <= QP_GBA_MAX_FAILURE_COUNT)
          {
            m_bUaAuthInProgress = QP_FALSE;
            iRetVal = InitiateGBAUbProcedure(QP_TRUE);
          }
		  if(QP_DNS_FAILURE == iRetVal)
	      {
	        IMS_MSG_MED_0(LOG_IMS_UT,"IMSServiceConfigGBAUIM::ProcessGBAUBAuthCallBack DNS failure, raise event to try on CS ");
	        m_iStatusCode = 0;
            Cleanup();
            RaiseEvent(QPE_IMS_SERV_CONFIG_HTTP_RESPONSE);
	      }
          else if(QP_SUCCESS != iRetVal || m_iPerformGBAUbCount > QP_GBA_MAX_FAILURE_COUNT)
          {
            IMS_MSG_MED_0(LOG_IMS_UT,"IMSServiceConfigGBAUIM::HandleGBAResponse Synchronous Ub Auth Failure or Ub Loop Count Reached ");
            Cleanup();
            RaiseEvent(QPE_IMS_SERV_CONFIG_HTTP_AUTH_FAILURE_EV);
            return;
          }
          return;
        }
        else if(QP_TRUE == m_stUaOnReqAuthParams.bIsParamIncorrect)
        {
          IMS_MSG_MED_0(LOG_IMS_UT,"IMSServiceConfigGBAUIM::HandleGBAResponse| Realm passed has UICC in it, Raise Event ");
          Cleanup();
          RaiseEvent(QPE_IMS_SERV_CONFIG_HTTP_AUTH_FAILURE_EV);
          return;
        }
      	  
	    ResetUaAuthParams(m_stUaAuthParams);
        RemoveAuthorizationHeader();
	    m_pGBAAuth->GetUaAuthParams(&m_stUaAuthParams);
	    if(m_iStatusCode == 401)
	    {
	      m_stUaAuthParams.m_iAuthenticateType = QP_AUTHORIZATION;
	    }
	    else if(m_iStatusCode == 407)
	    {
	      m_stUaAuthParams.m_iAuthenticateType = QP_PROXY_AUTHORIZATION;
	    }
	    bRetValue = SetUpHttpConnection(m_stHttpMessageContent);
        if(bRetValue)
        {
          QPCHAR* pEncodedKey = m_pHttpConnection->qpEncodeKeyToBase64(m_stGBAUbAttributes.iKsNAF,QP_GBA_KS_NAF_LEN);
          if(pEncodedKey != QP_NULL)
          {
	        m_stUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_PASSWORD].m_DlgChlType = QP_SIP_AUTH_PASSWORD;
	        m_stUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_PASSWORD].pDigestRespose = qpDplStrdup(MEM_IMS_UT,pEncodedKey);
		    m_stUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_PASSWORD].iDigestResLen = qpDplStrlen(pEncodedKey);
		    qpDplFree(MEM_IMS_FW,pEncodedKey);
	      }
          if(m_pHttpConnection->qpFillAuthorizationCredentials(&m_stUaAuthParams))
	      {
	        m_bUaAuthRespIncluded = QP_TRUE;
			QPUINT8 iRetVal = Send(m_stHttpMessageContent);
	        if(QP_SUCCESS == iRetVal)
	        {
	          bRetValue = QP_TRUE;
	        }
			else
			{
			  bRetValue = QP_FALSE;
			}
			
			if(QP_DNS_FAILURE == iRetVal)
			{
			  IMS_MSG_MED_0(LOG_IMS_UT,"IMSServiceConfigGBAUIM::HandleGBAResponse DNS failure, raise event to try on CS ");
	          m_iStatusCode = 0;
              Cleanup();
              RaiseEvent(QPE_IMS_SERV_CONFIG_HTTP_RESPONSE); 
			  return;
			}
	      }
		  else
		  {
		    bRetValue = QP_FALSE;
		  }
        }
      }
    }
    else if(QPE_IMS_SERV_CONFIG_GBA_UB_ON_REQ == m_eGBAUbMode)
    {
      m_iUaSecProtocolId = 0;
	  m_iCSList = 0;
      QPTLS_SESS_PROFILE* pTLSSessProfile = m_pHttpConnection->GetHttpTLSSessionProfile();
	  QPTLS_PARAM_TYPE *pTlsParamData = (QPTLS_PARAM_TYPE*)qpDplMalloc(MEM_IMS_UT,sizeof(QPTLS_PARAM_TYPE));
	  if(!pTlsParamData)
	  {
	    IMS_MSG_FATAL_0(LOG_IMS_UT,"IMSServiceConfigGBAUIM::HandleGBAResponse - Malloc failure for pTlsParamData");
        return;
	  }
	  qpDplMemset(pTlsParamData,0,sizeof(QPTLS_PARAM_TYPE));
      qpDplMemset(&m_stUaOnReqAuthParams, 0, sizeof(QP_IMS_SERV_CONFIG_UA_ON_REQ_CHALLENGE_PARAMS));
      bRetValue = m_pGBAAuth->ValidateAuthenticationInterface(m_stUaOnReqAuthParams, pInAuthenticateHeader, pTLSSessProfile);

      SipString strName((QPCHAR*)HttpHeadersStrings[QP_AUTHENTICATE]);
      SipString strValue(pInAuthenticateHeader);
	  SipHeader* pSipHeader          = QP_NULL;
      IMS_NEW(SipHeader, MEM_IMS_UT, pSipHeader, (strName, strValue));

      if(pSipHeader != QP_NULL)
      {
        QPCHAR* pRealm = RemoveQoutes(pSipHeader->getParameter((SipString)QP_SIP_REALM_STR));
		if(pRealm && qpDplStrlen(pRealm))
		{
		  qpDplMemset(m_pNafFqdn,0,QP_MAX_SERVER_URI_SIZE * sizeof(QPCHAR));
		  qpDplStrlncpy(m_pNafFqdn,pRealm,QP_MAX_SERVER_URI_SIZE * sizeof(QPCHAR), qpDplStrlen(pRealm));
        }
      }
	  if((QPE_IMS_SERV_CONFIG_GBA_TLS_SharedKey_CertBased == m_eGBATLSMode ||  QPE_IMS_SERV_CONFIG_GBA_TLS_On_Demand == m_eGBATLSMode)&&
           TLS_ERROR_OK == qpDplTlsGetParam(pTLSSessProfile,TLS_SES_CIPHER_SUITE,pTlsParamData))
      {
        m_iUaSecProtocolId = (QPUINT16)pTlsParamData->CSid;
		m_iCSList = GetCSList();
	    IMS_MSG_LOW_2(LOG_IMS_UT,"IMSServiceConfigGBAUIM::HandleGBAResponse - Setting seprotocol ID - %d cs list = %d", m_iUaSecProtocolId, m_iCSList);
      }
	  qpDplFree(MEM_IMS_UT,pTlsParamData);
      if(QP_TRUE == bRetValue)
      {
        if(QP_TRUE == m_stUaOnReqAuthParams.bPerformUbAuth && QP_TRUE != m_stUaOnReqAuthParams.bIsParamIncorrect)
        {
          ResetGBAUbAttributes();
          QPUINT8 iRetVal = InitiateGBAUbProcedure();
		  if(QP_DNS_FAILURE == iRetVal)
	      {
	        IMS_MSG_MED_0(LOG_IMS_UT,"IMSServiceConfigGBAUIM::ProcessGBAUBAuthCallBack DNS failure, raise event to try on CS ");
	        m_iStatusCode = 0;
            Cleanup();
            RaiseEvent(QPE_IMS_SERV_CONFIG_HTTP_RESPONSE);
	      }
          else if(QP_SUCCESS != iRetVal)
          {
            IMS_MSG_MED_0(LOG_IMS_UT,"IMSServiceConfigGBAUIM::HandleGBAResponse Synchronous Ub Auth Failure, Raise Event ");
            Cleanup();
            RaiseEvent(QPE_IMS_SERV_CONFIG_HTTP_AUTH_FAILURE_EV);
            return;
          }
        }
      }
    }
    if(QP_FALSE == bRetValue)
    {
      IMS_MSG_MED_0(LOG_IMS_UT,"IMSServiceConfigGBAUIM::HandleGBAResponse Synchronous Auth Failure, Raise Event ");
      Cleanup();
      RaiseEvent(QPE_IMS_SERV_CONFIG_HTTP_AUTH_FAILURE_EV);
      return;
    }
  }
  else if (200 == m_iStatusCode || 201 == m_iStatusCode)
  {
    if(QP_TRUE == m_bUaAuthInProgress)
    {
      ProcessUa2XXResponse();
    }
    else if(QPE_IMS_SERV_CONFIG_GBA_UB_ON_REQ == m_eGBAUbMode)
    {
      qpDplMemset(&m_stUaOnReqAuthParams, 0, sizeof(QP_IMS_SERV_CONFIG_UA_ON_REQ_CHALLENGE_PARAMS));
      RaiseEvent(QPE_IMS_SERV_CONFIG_HTTP_RESPONSE);
      return;
    }
  }
  /*Handle HTTP request redirection for On Demand TLS*/
  else if ((300 == m_iStatusCode || 301 == m_iStatusCode || 302 == m_iStatusCode || 303 == m_iStatusCode ||
           305 == m_iStatusCode || 307 == m_iStatusCode) && QPE_IMS_SERV_CONFIG_GBA_TLS_On_Demand == m_eGBATLSMode)
  {    
	QPUINT8 iRetVal = ProcessUa3XXResponse();
	if(QP_DNS_FAILURE == iRetVal)
	{
	  IMS_MSG_MED_0(LOG_IMS_UT,"IMSServiceConfigGBAUIM::HandleGBAResponse DNS failure, raise event to try on CS ");
	  m_iStatusCode = 0;
      Cleanup();
      RaiseEvent(QPE_IMS_SERV_CONFIG_HTTP_RESPONSE);
	}
    else if(QP_SUCCESS != iRetVal)
    {
      IMS_MSG_MED_0(LOG_IMS_UT,"IMSServiceConfig::ProcessUb3XXResponse Redirection failed, Raise Event ");
      Cleanup();
      RaiseEvent(QPE_IMS_SERV_CONFIG_HTTP_AUTH_FAILURE_EV);
    }
  }   
  else if(999 == m_iStatusCode && QP_TRUE == m_bUaAuthInProgress && 
  	      QPE_IMS_SERV_CONFIG_GBA_TLS_SharedKey_CertBased == m_eGBATLSMode &&
  	      m_iUaSecProtocolId != 0 && m_iCSList != 0)
  {
    IMS_MSG_MED_1(LOG_IMS_UT,"IMSServiceConfigGBAUIM::HandleGBAResponse - Non-none CS list(%d), attempt Ua again with default cs list", m_iCSList);
	m_iCSList = 0;
	m_iUaSecProtocolId = 0;
    /* Reset the previous Nonce if present as a new nonce would have come in this challenge */
    m_pGBAAuth->ResetPreviousAuthParams();
	/* Start Ua procedure afresh. */
    qpDplMemset(&m_stUaOnReqAuthParams, 0, sizeof(QP_IMS_SERV_CONFIG_UA_ON_REQ_CHALLENGE_PARAMS));
	RemoveAuthorizationHeader();
	QPUINT8 iRetVal = InitiateGBAUaProcedure();
	if(QP_DNS_FAILURE == iRetVal)
	{
	  IMS_MSG_MED_0(LOG_IMS_UT,"IMSServiceConfigGBAUIM::HandleGBAResponse DNS failure, raise event to try on CS ");
	  m_iStatusCode = 0;
      Cleanup();
      RaiseEvent(QPE_IMS_SERV_CONFIG_HTTP_RESPONSE);
	}
    else if(QP_SUCCESS != iRetVal)
    {
      IMS_MSG_MED_0(LOG_IMS_UT,"IMSServiceConfigGBAUIM::HandleGBAResponse Synchronous Send failure on Ua Interface, Raise Event ");
      Cleanup();
      RaiseEvent(QPE_IMS_SERV_CONFIG_HTTP_AUTH_FAILURE_EV);
    }
  }
  else
  {
    Cleanup();
    RaiseEvent(QPE_IMS_SERV_CONFIG_HTTP_RESPONSE);
  }
}

/************************************************************************
Function IMSServiceConfigGBAUIM::ProcessUa2XXResponse()

Description
This function handles success 200 OK on Ua Interface and
validates the rspauth parameter.

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID IMSServiceConfigGBAUIM::ProcessUa2XXResponse()
{
  IMS_MSG_LOW_0(LOG_IMS_UT,"IMSServiceConfigGBAUIM::ProcessUa2XXResponse Entered");
  QPCHAR*  pInRspAuthHeader              = QP_NULL;
  QPCHAR*  pEncodedKey                   = QP_NULL;
  QPBOOL   bRspAuthValidation            = QP_FALSE;
  QPUINT32 iLength                       = 0;

  if(m_pHttpContent)
  {
    qpDplFree(MEM_IMS_UT,m_pHttpContent);
    m_pHttpContent = QP_NULL;
  }
  /* Get the Content Body, to validate the rspauth parameter */
  m_pHttpContent = qpDplStrdup(MEM_IMS_UT, GetContent(iLength));
  
  pEncodedKey = m_pHttpConnection->qpEncodeKeyToBase64(m_stGBAUbAttributes.iKsNAF,QP_GBA_KS_NAF_LEN);
  /* Get the Aunthentication-Info header and check the validity */
  pInRspAuthHeader = GetHeaderValue((QPCHAR*)HttpHeadersStrings[QP_AUTHENTICATION_INFO]);
  if(QP_FALSE == m_pGBAAuth->ValidateGBA2XXResponse(pInRspAuthHeader, m_pHttpContent) || 
  	 QP_FALSE == m_pHttpConnection->qpValidateRspAuth(pEncodedKey))
  {
    /* Validate Response has failed, check if we can continue */
	m_pGBAAuth->ResetPreviousAuthParams();
    bRspAuthValidation = ContinueAfterRspAuthFailure();
  }
  else
  {
    bRspAuthValidation = QP_TRUE;
  }
  qpDplFree(MEM_IMS_FW,pEncodedKey);
  if(QP_FALSE == bRspAuthValidation)
  {
    /* Validation has failed and Integrity check is ON. Repeat the same action */
    m_iRspAuthFailureCount++;
    if(m_iRspAuthFailureCount <= QP_GBA_MAX_FAILURE_COUNT)
    {
      IMS_MSG_MED_1(LOG_IMS_UT,"IMSServiceConfigGBAUIM::ProcessUa2XXResponse RspAuth Failure, Trying again count = %d ", m_iRspAuthFailureCount);
	  /* Start Ua procedure afresh. */
	  qpDplMemset(&m_stUaOnReqAuthParams, 0, sizeof(QP_IMS_SERV_CONFIG_UA_ON_REQ_CHALLENGE_PARAMS));
      RemoveAuthorizationHeader();
      QPUINT8 iRetVal = InitiateGBAUaProcedure();
	  if(QP_DNS_FAILURE == iRetVal)
	  {
	    IMS_MSG_MED_0(LOG_IMS_UT,"IMSServiceConfigGBAUIM::ProcessUa2XXResponse DNS failure, raise event to try on CS ");
	    m_iStatusCode = 0;
        Cleanup();
        RaiseEvent(QPE_IMS_SERV_CONFIG_HTTP_RESPONSE);
	  }
      else if(QP_SUCCESS != iRetVal)
      {
        IMS_MSG_MED_0(LOG_IMS_UT,"IMSServiceConfigGBAUIM::ProcessUa2XXResponse Synchronous Send failure on Ua Interface, Raise Event ");
        Cleanup();
        RaiseEvent(QPE_IMS_SERV_CONFIG_HTTP_AUTH_FAILURE_EV);
      }
    }
    else
    {
      IMS_MSG_FATAL_0(LOG_IMS_UT,"IMSServiceConfigGBAUIM::ProcessUb2XXResponse Synchronous RspAuth Failure happened for the third time, aborting request, Raise Event ");
      Cleanup();
      RaiseEvent(QPE_IMS_SERV_CONFIG_HTTP_AUTH_FAILURE_EV);
    }
    return;
  }
  else
  {
    IMS_MSG_MED_0(LOG_IMS_UT,"IMSServiceConfigGBAUIM::ProcessUa2XXResponse Validation succeeded ");
    m_iRspAuthFailureCount = 0;
    m_iPerformGBAUbCount = 0;
    m_bUaAuthInProgress = QP_FALSE;
    qpDplMemset(&m_stUaOnReqAuthParams, 0, sizeof(QP_IMS_SERV_CONFIG_UA_ON_REQ_CHALLENGE_PARAMS));
    RaiseEvent(QPE_IMS_SERV_CONFIG_HTTP_RESPONSE);
    return;
  }
}

/************************************************************************
Function IMSServiceConfigGBAUIM::SendMessage()

Description
This is the main functin which Applications call to send the 
HTTP message out. It returns synchronous failure if any.

Dependencies
None

Return Value
QP_TRUE/QP_FALSE

Side Effects
None
************************************************************************/
QPUINT8 IMSServiceConfigGBAUIM::SendMessage(QP_HTTP_MESSAGE_CONTENT* n_pHttpMessageContent, QPE_HTTP_CONNECTION_TYPE n_eHttpConType )
{
  IMS_MSG_MED_0(LOG_IMS_UT,"IMSServiceConfigGBAUIM::SendMessage Entered");
  QPUINT8 iRetVal = QP_FAILURE;

  if(n_pHttpMessageContent == QP_NULL || 0 == qpDplStrlen(n_pHttpMessageContent->n_Uri))
  {
    IMS_MSG_FATAL_0(LOG_IMS_UT,"IMSServiceConfigGBAUIM::SendMessage n_pHttpMessageContent/URI is NULL");
    return QP_FAILURE;
  }

  /* Store the Http Content in the data member,
  ** Clear the previous request's values.
  ** This function is called only from Applications, so it is a new Request.
  */
  qpDplMemset(&m_stHttpMessageContent, 0, sizeof(QP_HTTP_MESSAGE_CONTENT));
  qpDplMemscpy(&m_stHttpMessageContent, sizeof(QP_HTTP_MESSAGE_CONTENT), n_pHttpMessageContent, sizeof(QP_HTTP_MESSAGE_CONTENT));
  m_eHttpConnectionType = n_eHttpConType;

  /* Don't raise events from this function as on failure it 
  ** gives synchronous failure to the caller.
  */

  /* Check if Ub procedure is ongoing. If there is an ongoing Auth response calculation,
  cancel it before proceeding with the current message. */
 if(QPE_IMS_SERV_CONFIG_GBA_UB_MODE_NONE != m_eGBAUbMode && QP_TRUE == m_bUbAuthInProgress)
 {
   QPE_CARD_DATA eCardType = (qpDplIsISIMPresent() == QP_TRUE)?QP_CARD_ISIM:QP_CARD_USIM;
   qpDplGBACancel(eCardType);
   IMS_MSG_LOW_1(LOG_IMS_UT,"IMSServiceConfigGBAUIM::SendMessage Ub procedure is ongoing - m_bUbAuthInProgress = %d", m_bUbAuthInProgress);
   Cleanup();
 }

 /* When request sent to HTTP Server for On Deamnd TLS, Remove Authorization header from request,
     Do not clean up Ub Interface paramters*/
  if(QPE_IMS_SERV_CONFIG_GBA_TLS_On_Demand  ==  m_eGBATLSMode && 
     m_eHttpConnectionType == QP_HTTP_CONNECTION )
  {
    /* Reset the previous Nonce if present as a new nonce would have come in this challenge */
    ResetUaAuthParams(m_stUaAuthParams);
    m_pGBAAuth->ResetPreviousAuthParams();
  }

  /* If GBA Ub mode is Always-On then before a HTTP Request is sent on the Ua Interface,
  ** authentication has to be performed on Ub Interface first.
  ** Hold the HTTP Request till Authentication on Ub is completed.
  */

  
  /* This is to store the current card's IMSI value
	  ** and compare it with the next request to see if 
	  ** the Card has been changed without reboot
	  */
  qpDplMemset(m_pImsi, 0, sizeof(QPDPL_IMSI_STRUCT));
  qpDplGetIMSI(m_pImsi);
  /*Do not add User agent for Ua Interface, will be added by Application*/
  if(QPE_IMS_SERV_CONFIG_GBA_UB_ALWAYS == m_eGBAUbMode)
  {
    iRetVal = InitiateGBAUbProcedure()
  }
  else if(QPE_IMS_SERV_CONFIG_GBA_UB_ON_REQ == m_eGBAUbMode)
  {
    if(QP_NULL != m_stGBAUbAttributes.pBTIDValue && 0 != qpDplStrlen(m_stGBAUbAttributes.pBTIDValue))
    {
      QPE_CARD_DATA eCardType = (qpDplIsISIMPresent() == QP_TRUE)?QP_CARD_ISIM:QP_CARD_USIM;
      if (QP_TRUE == IsBTIDValid(eCardType))
      {
        iRetVal = InitiateGBAUaProcedure();		
      }
      else
      {
        iRetVal = InitiateGBAUbProcedure();
      }
    }
    else
    {
      /* If TLS is enabled,then on Ua Interface HTTPS connection should be established */
      /*For On Demand TLS setting, initially HTTP connection should be established with NAF server which is default Connection Type*/
      if( QPE_IMS_SERV_CONFIG_GBA_TLS_SharedKey_CertBased == m_eGBATLSMode)
      {
        m_eHttpConnectionType = QP_HTTPS_CONNECTION;
      }
	  if(SetUpHttpConnection(m_stHttpMessageContent))
	  {
        iRetVal = Send(m_stHttpMessageContent);
	  }
	  else
	  {
	    iRetVal = QP_FAILURE;
	  }
    }
  }
  return iRetVal;
}

QPBOOL IMSServiceConfigGBAUIM::SetUpHttpConnection(QP_HTTP_MESSAGE_CONTENT& n_stHttpMessageContent)
{
  IMS_MSG_MED_0(LOG_IMS_UT,"IMSServiceConfigGBAUIM::SetUpHttpConnection Entered ");
  if(m_pHttpConnection)
  {
    IMS_DELETE(m_pHttpConnection,MEM_IMS_UT);
    m_pHttpConnection = QP_NULL;
  }

  if (m_eHttpConnectionType == QP_HTTPS_CONNECTION )
  {
    IMS_MSG_MED_0(LOG_IMS_UT,"IMSServiceConfigGBAUIM::SetUpHttpConnection HTTPS");
    IMS_NEW(HttpConnection, MEM_IMS_UT,m_pHttpConnection, (n_stHttpMessageContent.n_Uri, QP_MAX_BODY_SIZE, QP_HTTPS_CONNECTION));
  }
  else
  {
    IMS_MSG_MED_0(LOG_IMS_UT,"IMSServiceConfigGBAUIM::SetUpHttpConnection HTTP");
    IMS_NEW(HttpConnection, MEM_IMS_UT,m_pHttpConnection, (n_stHttpMessageContent.n_Uri));
  }

  m_pHttpConnection->SetListener(this);
  if(m_pPDPHandler)
  {
    m_pHttpConnection->SetPDPID(m_pPDPHandler->GetPDPID());
    m_pHttpConnection->SetDcmConnProfile(m_pPDPHandler->GetDcmConnProfile());
  }
  
  if( (QPE_IMS_SERV_CONFIG_GBA_TLS_SharedKey_CertBased == m_eGBATLSMode ||
       QPE_IMS_SERV_CONFIG_GBA_TLS_On_Demand == m_eGBATLSMode) && QP_TRUE == m_bUaAuthInProgress)
  {
    QPTLS_PARAM_TYPE sTlsParamData;
    qpDplMemset(&sTlsParamData,0,sizeof(QPTLS_PARAM_TYPE));
    sTlsParamData.CSList = m_iCSList;
    m_pHttpConnection->SetCipherSuitList(TLS_PREFERRED_CIPHER_LIST,sTlsParamData);
  }
  QPE_BASE_MESG_CODES retCode = m_pHttpConnection->InitRequest(n_stHttpMessageContent.n_MethodName, n_stHttpMessageContent.n_Url);
  if(QP_SUCCESS == retCode)
  {
    if(0 != qpDplStrlen(n_stHttpMessageContent.n_XmlContent) && 0 != qpDplStrlen(n_stHttpMessageContent.n_XmlContentType))
    {		
      m_pHttpConnection->AddContent(n_stHttpMessageContent.n_XmlContent,qpDplStrlen(n_stHttpMessageContent.n_XmlContent));
      m_pHttpConnection->AddHeader((QPCHAR*)HttpHeadersStrings[QP_CONTENT_TYPE],n_stHttpMessageContent.n_XmlContentType);
    }

    /*Add Etag value in IF_match field for HTTP PUT operation in IR92 mode*/
	/*Oprt mode check not required, as m_pCurrentEntityTag will be Null oustide IR92 mode*/
	if((m_pCurrentEntityTag != QP_NULL && qpDplStrlen(m_pCurrentEntityTag) > 0) &&
        qpDplStricmp("PUT", m_pHttpConnection->GetMethod()) == 0)        
    {
      m_pHttpConnection->AddHeader((QPCHAR*)HttpHeadersStrings[QP_IF_MATCH], m_pCurrentEntityTag);
    }
	
    if(0 != qpDplStrlen(n_stHttpMessageContent.n_HttpIntendedIdentity))
    {
      m_pHttpConnection->AddHeader((QPCHAR*)HttpHeadersStrings[QP_3GPP_INTENDED_IDENTITY],n_stHttpMessageContent.n_HttpIntendedIdentity);
    }

    QPUINT32 iAddHeaderVar  =  0;
    if (n_stHttpMessageContent.n_AdditionalHdrsCount > QP_MAX_HEADERS)
    {
      IMS_MSG_MED_0(LOG_IMS_UT,"IMSServiceConfigGBAUIM::SetUpHttpConnection n_AdditionalHdrsCount is greater than QP_MAX_HEADERS");
      return QP_FALSE;
    }
    for (iAddHeaderVar = 0; iAddHeaderVar < n_stHttpMessageContent.n_AdditionalHdrsCount; iAddHeaderVar++)
    {
      if(QP_NULL != n_stHttpMessageContent.n_AdditionalHeaders[iAddHeaderVar].hName && 
         QP_NULL != n_stHttpMessageContent.n_AdditionalHeaders[iAddHeaderVar].hValue &&
         0 != qpDplStrlen(n_stHttpMessageContent.n_AdditionalHeaders[iAddHeaderVar].hValue))
      {
        m_pHttpConnection->AddHeader(n_stHttpMessageContent.n_AdditionalHeaders[iAddHeaderVar].hName,
                                     n_stHttpMessageContent.n_AdditionalHeaders[iAddHeaderVar].hValue);
      }
      else
      {
        IMS_MSG_MED_0(LOG_IMS_UT,"IMSServiceConfigGBAUIM::SetUpHttpConnection Not a valid Header Name Value Pair");
      }
    }
     /* If Cert is true, it means Ut Application is using it and SSL AUTO Mode Session Needs to be set */    
     /*If Ub Authentication process on-going for TMO, Set TLS Auto mode for Ub Interface*/  
    if((QPE_IMS_SERV_CONFIG_GBA_TLS_SharedKey_CertBased == m_eGBATLSMode) ||
		(QPE_IMS_SERV_CONFIG_GBA_TLS_MODE_OVER_UB_ON   == m_eGBAUbTLSMode  && m_bUbAuthInProgress) ||
		(QPE_IMS_SERV_CONFIG_GBA_TLS_On_Demand       == m_eGBATLSMode && m_bUaAuthInProgress))
    {
      m_pHttpConnection->SetSSLSessionMode(QPTLS_SECSSL_SES_ALERT_MODE_AUTO);
    }
	return QP_TRUE;
  }
  return QP_FALSE;
}
  
QPUINT8 IMSServiceConfigGBAUIM::Send(QP_HTTP_MESSAGE_CONTENT& n_stHttpMessageContent)
{
  QPUINT8 iRetVal = QP_FAILURE;
  IMS_MSG_MED_0(LOG_IMS_UT,"IMSServiceConfigGBAUIM::Send");
  iRetVal = m_pHttpConnection->Send();
  if(QP_SUCCESS != iRetVal)
  {
    IMS_MSG_MED_0(LOG_IMS_FW,"IMSServiceConfigGBAUIM::Send | Send() failed");
  }
  return iRetVal;
}

/************************************************************************
Function IMSServiceConfigGBAUIM::IsBTIDValid()

Description
To check if BTID is valid

Dependencies
None

Return Value
QP_TRUE/QP_FALSE

Side Effects
None
************************************************************************/
QPBOOL IMSServiceConfigGBAUIM::IsBTIDValid(QPE_CARD_DATA n_eCardType)
{
  IMS_MSG_MED_0(LOG_IMS_UT,"IMSServiceConfigGBAUIM::IsBTIDValid");
  QPBOOL bRetVal = QP_FALSE;
  QP_GBA_KEY_PARAM sCheckGBAParam;
  qpDplMemset(&sCheckGBAParam,0,sizeof(QP_GBA_KEY_PARAM));
  sCheckGBAParam.sBtidPtr.iLen = qpDplStrlen(m_stGBAUbAttributes.pBTIDValue);
  qpDplStrlncpy(sCheckGBAParam.sBtidPtr.sData,m_stGBAUbAttributes.pBTIDValue,QP_GBA_MAX_BTID_LEN, qpDplStrlen(m_stGBAUbAttributes.pBTIDValue));
  
  bRetVal = qpDplGBAKeyValid(&sCheckGBAParam,n_eCardType);
  return bRetVal;
}
/************************************************************************
Function IMSServiceConfigGBAUIM::ReInitGBASupport()

Description
GBA related data will get De-initialized and then re-initialized

Dependencies
None

Return Value
QP_VOID

Side Effects
None
************************************************************************/
QPVOID IMSServiceConfigGBAUIM::ReInitGBASupport()
{
  IMS_MSG_LOW_0(LOG_IMS_UT,"IMSServiceConfigGBAUIM::ReInitGBASupport - DeInit and Init GBA parameters");
  if( QPE_IMS_SERV_CONFIG_GBA_UB_MODE_NONE != m_eGBAUbMode )
  {
    DeInitGBASupport();
    InitGBASupport();
  }
}

/************************************************************************
Function IMSServiceConfigGBAUIM::InitiateGBAUbProcedure()

Description
In this function we get the HTTP message on Ub
Interface generated

Dependencies
None

Return Value
QP_TRUE/QP_FALSE

Side Effects
None
************************************************************************/
QPUINT8 IMSServiceConfigGBAUIM::InitiateGBAUbProcedure(QPBOOL n_bForceUb)
{
  IMS_MSG_LOW_1(LOG_IMS_UT,"IMSServiceConfigGBAUIM::InitiateGBAUbProcedure Entered n_bForceUb %d", n_bForceUb);
  QPUINT8 iRetVal              = QP_FAILURE;
  QPBOOL bBTIDValid            = QP_FALSE;
  QP_BOOTSTRAP_PARAM sGBAParam;
  QPE_CARD_DATA eCardType;
  QPCHAR* pNafFqdn = QP_NULL;
  
  if(m_eGBAUbTLSMode == QPE_IMS_SERV_CONFIG_GBA_TLS_MODE_OVER_UB_ON)
  {
    m_eHttpConnectionType        = QP_HTTPS_CONNECTION; 
  }
  else
  {
    m_eHttpConnectionType        = QP_HTTP_CONNECTION;
  }

  eCardType = (qpDplIsISIMPresent() == QP_TRUE)?QP_CARD_ISIM:QP_CARD_USIM;
  if(QP_NULL != m_stGBAUbAttributes.pBTIDValue && QP_NULL != m_stGBAUbAttributes.pKsLifeTime)
  {
    bBTIDValid = IsBTIDValid(eCardType);
  }
  /* BTID has expired, reset the TMPI calculated. Protocols will not be aware of this */
  if(QP_FALSE == bBTIDValid)
  {
    ResetGBAUbAttributes();
  }
  /* When forceful Ub INterface Authentication is called don't reset TMPI value */
  if(QP_FALSE == bBTIDValid || QP_TRUE == n_bForceUb)
  {
    IMS_MSG_HIGH_1(LOG_IMS_UT,"IMSServiceConfigGBAUIM::InitiateGBAUbProcedure Valid %d ", bBTIDValid);
	QPCHAR* pRealm = QP_NULL;
    qpDplMemset(&sGBAParam,0,sizeof(QP_BOOTSTRAP_PARAM));
	if(QP_NULL != m_stUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_REALM].pDigestRespose && m_stUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_REALM].iDigestResLen > 0)
	{
	  pRealm = qpDplStrdup(MEM_IMS_UT,m_stUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_REALM].pDigestRespose);
	}
	else
	{
	  if(qpDplStrlen(m_pNafFqdn) > 0)
	  {
		pRealm = qpDplStrdup(MEM_IMS_UT,m_pNafFqdn);
	  }	  
	}
    if(pRealm)
    {
      pNafFqdn = qpDplStrstr(pRealm,"@");
	  if(pNafFqdn)
	  {
	    pNafFqdn++;
	  }
    }
	if(!pNafFqdn)
	{
	  IMS_MSG_HIGH_0(LOG_IMS_UT,"IMSServiceConfigGBAUIM::InitiateGBAUbProcedure invalid naf id ");
	  qpDplMemset(sGBAParam.sNafIdPtr.sFQDNType.sData,0,QP_GBA_MAX_NAF_FQDN_LEN);
	  sGBAParam.sNafIdPtr.sFQDNType.iLen = 0;
	}
	else
	{
	  IMS_MSG_HIGH_1_STR(LOG_IMS_UT,"IMSServiceConfigGBAUIM::InitiateGBAUbProcedure - naf id: %s", pNafFqdn);
	  qpDplStrlncpy(sGBAParam.sNafIdPtr.sFQDNType.sData,pNafFqdn,QP_GBA_MAX_NAF_FQDN_LEN, qpDplStrlen(pNafFqdn));
      sGBAParam.sNafIdPtr.sFQDNType.iLen = qpDplStrlen(pNafFqdn); 
	}
	FillNAFSecProtocolType(sGBAParam.sNafIdPtr.sNAFSecurityProtocolType);
	if(pRealm)
	{
	  qpDplFree(MEM_IMS_UT,pRealm);
	}
	if(GetCurrentRAT() == DCM_RAT_IWLAN)
    {
      sGBAParam.SApnData = m_strApn2Name;      
    }
    else
    {
      sGBAParam.SApnData = m_strApnName;
    }
	sGBAParam.BootStrapUserData.pUserData = this;
	sGBAParam.BootStrapUserData.BootStrapCallBack = qpHandleUBAuthCallBack;
	if(QP_TRUE == qpDplStartBootstrapProcedure(&sGBAParam,eCardType,n_bForceUb))
	{
	  m_bUbAuthInProgress = QP_TRUE;
	  iRetVal = QP_SUCCESS;
	}
	else
	{
	  m_bUbAuthInProgress = bRetValue;
	  iRetVal = QP_FAILURE;
	}
  }
  else
  {
    /* Valid BTID value is present, no need to initaite a Ub Interface request,
    ** just use the existing value as username.
    */
    IMS_MSG_HIGH_1(LOG_IMS_UT,"IMSServiceConfigGBAUIM::InitiateGBAUbProcedure Valid %d ", bBTIDValid);
    iRetVal = InitiateGBAUaProcedure();
  }
  return iRetVal;
}

/************************************************************************
Function IMSServiceConfigGBAUIM::InitiateGBAUaProcedure()

Description
In this function we get the Authorization message on Ua
Interface generated after Ub Interface authentication and sent with 
Authorization header if nonce is received from the previous 
successful request or received when On Req Mode is enabled.

Dependencies
None

Return Value
Qp_TRUE/QP_FALSE

Side Effects
None
************************************************************************/
QPUINT8 IMSServiceConfigGBAUIM::InitiateGBAUaProcedure()
{
  IMS_MSG_LOW_0(LOG_IMS_UT,"IMSServiceConfigGBAUIM::InitiateGBAUaProcedure Entered");
  QPUINT8 iRetVal = QP_FAILURE;
  QP_IMS_SERV_CONFIG_GBA_UA_AUTH_PARAMS    sUaAuthHeaderParams;
  qpDplMemset(&sUaAuthHeaderParams, 0 ,sizeof(QP_IMS_SERV_CONFIG_GBA_UA_AUTH_PARAMS));
  qpDplMemset(m_pRealm, 0 , QP_MAX_SERVER_URI_SIZE * sizeof(QPCHAR));

  /* If TLS is enabled,then on Ua Interface HTTPS connection should be established */
  /*For On Demand TLS, m_eHttpConnectionType will be set as HTTPS during redirection */
  if(QPE_IMS_SERV_CONFIG_GBA_TLS_SharedKey_CertBased == m_eGBATLSMode ||
     QPE_IMS_SERV_CONFIG_GBA_TLS_On_Demand           == m_eGBATLSMode)
  {
    m_eHttpConnectionType = QP_HTTPS_CONNECTION;
  }
  else /*Except for Cert based TLS, On Demand TLS, HTTP connection should be used*/
  {
    m_eHttpConnectionType = QP_HTTP_CONNECTION; 
  }

  if(QP_NULL != m_stGBAUbAttributes.pBTIDValue && 0 != qpDplStrlen(m_stGBAUbAttributes.pBTIDValue))
  {
    sUaAuthHeaderParams.pUsername = m_stGBAUbAttributes.pBTIDValue;
    IMS_MSG_MED_1_STR(LOG_IMS_UT,"IMSServiceConfigGBAUIM::InitiateGBAUaProcedure BTID value %s", m_stGBAUbAttributes.pBTIDValue);
  }
  else
  {
    IMS_MSG_HIGH_0(LOG_IMS_UT,"IMSServiceConfigGBAUIM::InitiateGBAUaProcedure BTID value not present, should NEVER happen");
    sUaAuthHeaderParams.pUsername = qpDplGetPrivateUID();
  }

  /* Here the URI for Auth Header is the URL from the HTTP Message */
  sUaAuthHeaderParams.pURI = &m_stHttpMessageContent.n_Url[0];
  /* Here the Realm for Auth Header is the URI from the HTTP Message */
  ParseServerAddress(m_pRealm, m_stHttpMessageContent.n_Uri);
  sUaAuthHeaderParams.pRealm = m_pRealm;
  sUaAuthHeaderParams.pEntityBody = &m_stHttpMessageContent.n_XmlContent[0];
  sUaAuthHeaderParams.pMethodName = &m_stHttpMessageContent.n_MethodName[0];

  /* This call passes all the relevant dat which is present in Authorization header.
    ** Don't move it downwords else, while calculating Auth response some data will be missing.
    */
  if(QP_TRUE == m_pGBAAuth->GenerateUaAuthHeader(&sUaAuthHeaderParams))
  {
    iRetVal = QP_SUCCESS;
  }
	
  /* If Nonce is present from the first On Req challenge then use it,
  ** generate the resp parameter and send it in the Auth header.
  ** Else send a new message with Auth header having username = BTID value and get nonce in next challenge.
  */
  if((QP_TRUE == m_stUaOnReqAuthParams.bNoncePresent && QPE_IMS_SERV_CONFIG_GBA_UB_ON_REQ == m_eGBAUbMode) || QP_TRUE == m_pGBAAuth->IsNextNoncePresent())
  {
    /* Already Authorization header with nonce has been received 
    ** or next nonce from the previous successful request is stored,
    ** and corresponding structure of the utility is filled, don't pass InAuth Header.
    ** Get the response from utility and then send request out with response in Authorization header.
    */
    m_bUaAuthInProgress = QP_TRUE;
    /* Clean the existing HTTP connection; the object might be pointing to some other server address */
    ResetUaAuthParams(m_stUaAuthParams);
	m_pGBAAuth->GetUaAuthParams(&m_stUaAuthParams);
	RemoveAuthorizationHeader();

	/* No need to re-create HTTP object if it is the case of first time 401 on Ua */
	if(!(QP_TRUE == m_stUaOnReqAuthParams.bNoncePresent && QPE_IMS_SERV_CONFIG_GBA_UB_ON_REQ == m_eGBAUbMode))
	{
	  if(SetUpHttpConnection(m_stHttpMessageContent))
	  {
	    iRetVal = QP_SUCCESS;
	  }
	}
    if(iRetVal == QP_SUCCESS)
    {      
	  QPCHAR* pEncodedKey = m_pHttpConnection->qpEncodeKeyToBase64(m_stGBAUbAttributes.iKsNAF,QP_GBA_KS_NAF_LEN);
      if(pEncodedKey != QP_NULL)
	  {
	    m_stUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_PASSWORD].m_DlgChlType = QP_SIP_AUTH_PASSWORD;
	    m_stUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_PASSWORD].pDigestRespose = qpDplStrdup(MEM_IMS_UT,pEncodedKey);
		m_stUaAuthParams.m_pDigestChallenge[QP_SIP_AUTH_PASSWORD].iDigestResLen = qpDplStrlen(pEncodedKey);
		qpDplFree(MEM_IMS_FW,pEncodedKey);
	  }
	  if(m_iStatusCode == 407)
	  {
		m_stUaAuthParams.m_iAuthenticateType = QP_PROXY_AUTHORIZATION;
	  }
	  else /* Do not check for specific status code here as can come here even after receiving 200/201 for next Ut request */
	  {
		m_stUaAuthParams.m_iAuthenticateType = QP_AUTHORIZATION;
	  }
      if(m_pHttpConnection->qpFillAuthorizationCredentials(&m_stUaAuthParams))
	  {
	    m_bUaAuthRespIncluded = QP_TRUE;
	    iRetVal = Send(m_stHttpMessageContent);
	  }
	  else
	  {
	    iRetVal = QP_FAILURE;
	  }
    }
  }
  else
  {
    m_bUaAuthInProgress = QP_TRUE;
    m_bUaAuthRespIncluded = QP_FALSE;
	if(SetUpHttpConnection(m_stHttpMessageContent))
    {
      iRetVal = Send(m_stHttpMessageContent);
	}
	else
	{
	  iRetVal = QP_FAILURE;
	}
  }
  return iRetVal;
}

/************************************************************************
Function IMSServiceConfigGBAUIM::qpHandleGBAAuthCallBack()

Description
Static function to receive the call back from the IMS Service Config
GBA helper class.

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID IMSServiceConfigGBAUIM::qpHandleUBAuthCallBack(QP_GBA_RESULT_ENUM_TYPE n_eGbaResultType,QP_BOOTSTRAP_RESULT* n_pGBAResult, 
																		QPVOID* pUserData)
{
  IMS_MSG_LOW_0(LOG_IMS_UT,"IMSServiceConfigGBAUIM::qpHandleUBAuthCallBack ");
  if( QP_NULL == pUserData || QP_NULL == n_pGBAResult)
  {
    IMS_MSG_ERR_0(LOG_IMS_UT,"IMSServiceConfigGBAUIM::qpHandleUBAuthCallBack CB Pointers are NULL");
    return;
  }
  IMSServiceConfigGBAUIM* pThis = (IMSServiceConfigGBAUIM*)pUserData;
  pThis->ProcessGBAUBAuthCallBack(n_eGbaResultType, n_pGBAResult);
}
QPVOID IMSServiceConfigGBAUIM::ProcessGBAUBAuthCallBack(QP_GBA_RESULT_ENUM_TYPE n_eGbaResultType,QP_BOOTSTRAP_RESULT* n_pGBAResult)
{
  IMS_MSG_HIGH_0(LOG_IMS_UT,"IMSServiceConfigGBAUIM::ProcessGBAUBAuthCallBack");
  m_bUbAuthInProgress = QP_FALSE;
  switch(n_eGbaResultType)
  {
    case QP_GBA_SUCCESS:
    {
	  ResetGBAUbAttributes();
	  qpDplMemscpy(&m_stGBAUbAttributes.iKsNAF,QP_GBA_KS_NAF_LEN,n_pGBAResult->sKsNaf,QP_GBA_KS_NAF_LEN);
	  m_stGBAUbAttributes.pBTIDValue = qpDplStrdup(MEM_IMS_UT,n_pGBAResult->sBtid.sData);
	  m_stGBAUbAttributes.pKsLifeTime = qpDplStrdup(MEM_IMS_UT,n_pGBAResult->sLifetime.sData);
	  QPE_CARD_DATA eCardType = (qpDplIsISIMPresent() == QP_TRUE)?QP_CARD_ISIM:QP_CARD_USIM;
	  if(QP_FALSE == IsBTIDValid(eCardType))
      {
        Cleanup();
        RaiseEvent(QPE_IMS_SERV_CONFIG_HTTP_AUTH_FAILURE_EV);
      }
	  else
	  {
	    QPUINT8 iRetVal = InitiateGBAUaProcedure();
	    if(QP_DNS_FAILURE == iRetVal)
	    {
	      IMS_MSG_MED_0(LOG_IMS_UT,"IMSServiceConfigGBAUIM::ProcessGBAUBAuthCallBack DNS failure, raise event to try on CS ");
	      m_iStatusCode = 0;
          Cleanup();
          RaiseEvent(QPE_IMS_SERV_CONFIG_HTTP_RESPONSE);
	    }
        else if(QP_SUCCESS != iRetVal)
        {
          IMS_MSG_MED_0(LOG_IMS_UT,"IMSServiceConfigGBAUIM::ProcessGBAUBAuthCallBack Synchronous Send failure on Ua Interface, Raise Event ");
          Cleanup();
          RaiseEvent(QPE_IMS_SERV_CONFIG_HTTP_AUTH_FAILURE_EV);
        }
	  }
      break;
    }
	case QP_GBA_SERVER_ERROR:
	{
	  IMS_MSG_HIGH_1(LOG_IMS_UT,"IMSServiceConfigGBAUIM::ProcessGBAUBAuthCallBack - n_pGBAResult->error_code = %d", n_pGBAResult->error_code);
	  if(404 == n_pGBAResult->error_code)
      {
	    /* If 404 is received on Ub, it should not be considered as element not found. 
	      To differentiate from 404 received on Ua, using internal error mapping. */
        m_iStatusCode = QP_GBA_404_FAILURE_ON_UB;
        IMS_MSG_MED_1(LOG_IMS_UT,"IMSServiceConfigGBAUIM::ProcessGBAUBAuthCallBack - Received 404 on Ub, Setting m_iStatusCode to %d",m_iStatusCode);
      }
	  else
	  {
        m_iStatusCode = n_pGBAResult->error_code;
	  }
      Cleanup();
      RaiseEvent(QPE_IMS_SERV_CONFIG_HTTP_RESPONSE);
      break;
	}
    default:
    {
      Cleanup();
      RaiseEvent(QPE_IMS_SERV_CONFIG_HTTP_AUTH_FAILURE_EV);
      break;
    }
  }
}

/************************************************************************
Function IMSServiceConfigGBAUIM::ContinueAfterRspAuthFailure()

Description
When "rspauth" failure has occureed, we check if Intergrity check is enabled
or disabled. If enabled we need to re-do the operation just performed else 
just ignore and continue.

Dependencies
None

Return Value
QP_TRUE/QP_FALSE

Side Effects
None
************************************************************************/
QPBOOL IMSServiceConfigGBAUIM::ContinueAfterRspAuthFailure()
{
  IMS_MSG_LOW_0(LOG_IMS_UT,"IMSServiceConfigGBAUIM::ContinueAfterRspAuthFailure Entered");
  QPBOOL bRetValue        = QP_FALSE;

  bRetValue = m_pGBAAuth->IsQopTypeAuthInt();
  if(QP_TRUE == bRetValue)
  {
    /* Integrity check is enabled, perform the same action again */
    IMS_MSG_LOW_0(LOG_IMS_UT,"IMSServiceConfigGBAUIM::ContinueAfterRspAuthFailure Failed");
    return QP_FALSE;
  }
  IMS_MSG_LOW_0(LOG_IMS_UT,"IMSServiceConfigGBAUIM::ContinueAfterRspAuthFailure Passed");
  return QP_TRUE;
}

/************************************************************************
Function IMSServiceConfigGBAUIM::ResetGBAUbAttributes()

Description
This function resets the GBA attributes like BTID and Ks values.

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID IMSServiceConfigGBAUIM::ResetGBAUbAttributes()
{
  IMS_MSG_LOW_0(LOG_IMS_UT,"IMSServiceConfigGBAUIM::ResetGBAUbAttributes Entered");
  if(QP_NULL != m_stGBAUbAttributes.pBTIDValue)
  {
    qpDplFree(MEM_IMS_UT, m_stGBAUbAttributes.pBTIDValue);
    m_stGBAUbAttributes.pBTIDValue= QP_NULL;
  }
  if(QP_NULL != m_stGBAUbAttributes.pKsLifeTime)
  {
    qpDplFree(MEM_IMS_UT, m_stGBAUbAttributes.pKsLifeTime);
    m_stGBAUbAttributes.pKsLifeTime= QP_NULL;
  }
  qpDplMemset(m_stGBAUbAttributes.iKsNAF,0,QP_GBA_KS_NAF_LEN);
}

/************************************************************************
Function IMSServiceConfigGBAUIM::ParseServerAddress()

Description
This is a utility function to parse the server address and remove
the port if present

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID IMSServiceConfigGBAUIM::ParseServerAddress(QPCHAR* n_pOutAddr, QPCHAR* n_pInAddr)
{
	if(QP_NULL == n_pInAddr || QP_NULL == n_pOutAddr )
	{
		IMS_MSG_MED_0(LOG_IMS_FW,"IMSServiceConfigGBAUIM::ParseServerAddress n_pInAddr/n_pOutAddr is null");
		return;
	}
	//Parse the given server address for FQDN/IPv6/IPv4 : Port
	if(n_pInAddr[0] == '[')
	{
		QPCHAR* pCloseBracket = qpDplStrchr(n_pInAddr, ']');
		if(pCloseBracket && pCloseBracket > n_pInAddr + 1)
		{
			if(n_pOutAddr)
			{
			    qpDplMemset(n_pOutAddr,0,QP_MAX_SERVER_URI_SIZE * sizeof(QPCHAR));
				(QPVOID)qpDplStrlncpy(n_pOutAddr, n_pInAddr + 1, QP_MAX_SERVER_URI_SIZE, pCloseBracket - n_pInAddr - 1);
				n_pOutAddr[pCloseBracket - n_pInAddr - 1] = '\0';
			}
		}
		else
		{
			IMS_MSG_ERR_0(LOG_IMS_FW,"IMSServiceConfigGBAUIM::ParseServerAddress Open/Close Braces mismatch");
			return;
		}
	}
	else
	{
		QPCHAR* pColon = qpDplStrchr(n_pInAddr, ':');
		if (pColon)
		{
			if(n_pOutAddr)
			{
			    qpDplMemset(n_pOutAddr,0,QP_MAX_SERVER_URI_SIZE * sizeof(QPCHAR));
				(QPVOID)qpDplStrlncpy(n_pOutAddr, n_pInAddr, QP_MAX_SERVER_URI_SIZE, pColon - n_pInAddr);
				n_pOutAddr[pColon - n_pInAddr] = '\0';
			}
		}
		else
		{
		    qpDplMemset(n_pOutAddr,0,QP_MAX_SERVER_URI_SIZE * sizeof(QPCHAR));
			qpDplStrlcpy(n_pOutAddr, n_pInAddr,QP_MAX_SERVER_URI_SIZE);
		}
	}
}

QPVOID IMSServiceConfigGBAUIM::Cleanup()
{
  /* Clean up all the states */
  qpDplMemset(&m_stUaOnReqAuthParams, 0, sizeof(QP_IMS_SERV_CONFIG_UA_ON_REQ_CHALLENGE_PARAMS));

  ResetGBAUbAttributes();
  ResetUaAuthParams(m_stUaAuthParams);
  m_pGBAAuth->ResetPreviousAuthParams();
  m_bUaAuthInProgress = QP_FALSE;
  m_bUbAuthInProgress = QP_FALSE;
  m_iPerformGBAUbCount = 0;
  m_iRspAuthFailureCount = 0;
  m_bUaAuthRespIncluded = QP_FALSE;
  m_iUaSecProtocolId = 0;
  m_iCSList = 0;
}
/************************************************************************
Function IMSServiceConfigGBAUIM::IsStalePresent()

Description
Checks if STALE is present in the challenge.

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPBOOL IMSServiceConfigGBAUIM::IsStalePresent()
{
  IMS_MSG_MED_0(LOG_IMS_UT,"IMSServiceConfigGBAUIM::IsStalePresent");
  QPCHAR* pInAuthenticateHeader         = QP_NULL;
  QPBOOL  bRetValue                     = QP_FALSE;
  if(401 == m_iStatusCode)
  {
    pInAuthenticateHeader = GetHeaderValue( (QPCHAR*)HttpHeadersStrings[QP_AUTHENTICATE] );
    SipHeader* pSipHeader          = QP_NULL;
    QPCHAR*    pStale              = QP_NULL;
    SipString strName((QPCHAR*)HttpHeadersStrings[QP_AUTHENTICATE]);
    SipString strValue(pInAuthenticateHeader);
    IMS_NEW(SipHeader, MEM_IMS_UT, pSipHeader, (strName, strValue));
    if(pSipHeader == QP_NULL)
    {
      IMS_MSG_FATAL_0(LOG_IMS_UT,"IMSServiceConfigGBAUIM::IsStalePresent | new SipHeader failed");
      return bRetValue;
    }
    pStale = RemoveQoutes(pSipHeader->getParameter((SipString)QP_SIP_STALE_STR));
    if(QP_NULL == pStale)
    {
      /* Stale has to be case insensitive */
      pStale = RemoveQoutes(pSipHeader->getParameter((SipString)QP_SIP_STALE_STR_CAPS));
    }
    if(QP_NULL!= pStale)
    {
      if(qpDplStrnicmp(pStale, QP_STALE_TRUE, qpDplStrlen(QP_STALE_TRUE)) == 0)
        bRetValue = QP_TRUE;
      else
        bRetValue = QP_FALSE;
      qpDplFree(MEM_IMS_UT, pStale);
    }
    IMS_DELETE(pSipHeader,MEM_IMS_UT);
  }
  return bRetValue;
}
/************************************************************************
Function IMSServiceConfigGBAUIM::RemoveQoutes()

Description
This function removes quotes and mallocs that value and
returns a pointer for the same.

Dependencies
None

Return Value
QPCHAR*

Side Effects
None
************************************************************************/
QPCHAR* IMSServiceConfigGBAUIM::RemoveQoutes(const SipString *n_pStrParam)
{
  QPCHAR* pParam              = QP_NULL;
  if(n_pStrParam)
  {
    QPCHAR *pTemp = (QPCHAR*)n_pStrParam->c_str();
    //Remove Quote if present at beginning
    if(pTemp[0] == '\"')
      pTemp++;
    QPINT i = 0;
    while(pTemp[i] != '\0')
    {
      i++;
    }
    //Remove Quote if present at end
    if(pTemp[--i] == '\"')
      pTemp[i] = '\0';
    pParam = (QPCHAR *)qpDplMalloc(MEM_IMS_UT, n_pStrParam->length() + 1);
    if(pParam == QP_NULL)
    {
      IMS_MSG_FATAL_0(LOG_IMS_UT,"IMSServiceConfigGBAUIM::RemoveQoutes | new failed");
      return QP_NULL;
    }  
    (QPVOID)qpDplStrlcpy(pParam, pTemp, (n_pStrParam->length() + 1));
    return pParam;
  }
  return QP_NULL;
}
/************************************************************************
Function IMSServiceConfigGBAUIM::GetCSList()

Description
This API returns CS corresponding to Ua sec protocol id

Dependencies
None

Return Value
QPUINT32

Side Effects
None
************************************************************************/
QPUINT32 IMSServiceConfigGBAUIM::GetCSList()
{
  QPUINT32 iCsList = 0;
  switch(m_iUaSecProtocolId)
  {
    case QP_CS_ID_RSA_WITH_3DES_EDE_CBC_SHA:
	{
      iCsList = (QPUINT32)QP_TLS_RSA_WITH_3DES_EDE_CBC_SHA;
	  break;
	}
	case QP_CS_ID_DHE_RSA_WITH_3DES_EDE_CBC_SHA:
	{
	  iCsList = (QPUINT32)QP_TLS_DHE_RSA_WITH_3DES_CBC_SHA;
	  break;
	}
	case QP_CS_ID_RSA_WITH_AES_128_CBC_SHA:
	{
	  iCsList = (QPUINT32)QP_TLS_RSA_WITH_AES_128_CBC_SHA;
	  break;
	}
	case QP_CS_ID_DHE_RSA_WITH_AES_128_CBC_SHA:
	{
	  iCsList = (QPUINT32)QP_TLS_DHE_RSA_WITH_AES_128_CBC_SHA;
	  break;
	}
	default:
	{
	  iCsList = (QPUINT32)QP_TLS_CIPHER_NONE;
	}
  }
  return iCsList;
}

/************************************************************************
Function IMSServiceConfigGBAUIM::RemoveAuthorizationHeader()

Description
This API removes Authorization header added in the previous iteration

Dependencies
None

Return Value
QPBOOL

Side Effects
None
************************************************************************/

QPVOID IMSServiceConfigGBAUIM::RemoveAuthorizationHeader()
{
  IMS_MSG_MED_0(LOG_IMS_UT,"IMSServiceConfigGBAUIM::RemoveAuthorizationHeader");
  for ( QPUINT32 iAddHeaderVar = 0;
    iAddHeaderVar < m_stHttpMessageContent.n_AdditionalHdrsCount && m_stHttpMessageContent.n_AdditionalHdrsCount < QP_MAX_HEADERS;
    iAddHeaderVar++)
  {
    if(QP_NULL != m_stHttpMessageContent.n_AdditionalHeaders[iAddHeaderVar].hName && 0 == qpDplStrcmp(m_stHttpMessageContent.n_AdditionalHeaders[iAddHeaderVar].hName, (QPCHAR*)(HttpHeadersStrings[QP_AUTHORIZATION])))
    {
      // The pointer [points to a static memory location. So no need to free it.
      m_stHttpMessageContent.n_AdditionalHeaders[iAddHeaderVar].hName = QP_NULL;
      //Don't free any data as the pointer is already as part of member variable and will be used in the next iteration.
      m_stHttpMessageContent.n_AdditionalHeaders[iAddHeaderVar].hValue = QP_NULL;
      m_stHttpMessageContent.n_AdditionalHdrsCount--;
      IMS_MSG_HIGH_1(LOG_IMS_REGMGR,"IMSServiceConfigGBAUIM::RemoveAuthorizationHeader, removing Authorization header. Header Count %d", m_stHttpMessageContent.n_AdditionalHdrsCount);
      break; //for loop
    }
  }
  return;
}


/************************************************************************
Function IMSServiceConfigGBAUIM::FillNAFSecProtocolType()

Description
This API returns CS corresponding to Ua sec protocol id

Dependencies
None

Return Value
QPVOID

Side Effects
None
************************************************************************/
QPVOID IMSServiceConfigGBAUIM::FillNAFSecProtocolType(QPUINT8* n_pUaSecProtocolType)
{
  IMS_MSG_MED_0(LOG_IMS_UT,"IMSServiceConfigGBAUIM::FillNAFSecProtocolType");
  switch(m_iUaSecProtocolId)
  {
    case QP_CS_ID_RSA_WITH_3DES_EDE_CBC_SHA:
	{
      n_pUaSecProtocolType[0] = 0x01;
	  n_pUaSecProtocolType[1] = 0x00;
      n_pUaSecProtocolType[2] = 0x01;
      n_pUaSecProtocolType[3] = 0x00;
      n_pUaSecProtocolType[4] = 0x0a;
	  break;
	}
	case QP_CS_ID_DHE_RSA_WITH_3DES_EDE_CBC_SHA:
	{
	  n_pUaSecProtocolType[0] = 0x01;
	  n_pUaSecProtocolType[1] = 0x00;
      n_pUaSecProtocolType[2] = 0x01;
      n_pUaSecProtocolType[3] = 0x00;
      n_pUaSecProtocolType[4] = 0x16;
	  break;
	}
	case QP_CS_ID_RSA_WITH_AES_128_CBC_SHA:
	{
	  n_pUaSecProtocolType[0] = 0x01;
	  n_pUaSecProtocolType[1] = 0x00;
      n_pUaSecProtocolType[2] = 0x01;
      n_pUaSecProtocolType[3] = 0x00;
      n_pUaSecProtocolType[4] = 0x2f;
	  break;
	}
	case QP_CS_ID_DHE_RSA_WITH_AES_128_CBC_SHA:
	{
	  n_pUaSecProtocolType[0] = 0x01;
	  n_pUaSecProtocolType[1] = 0x00;
      n_pUaSecProtocolType[2] = 0x01;
      n_pUaSecProtocolType[3] = 0x00;
      n_pUaSecProtocolType[4] = 0x33;
	  break;
	}
	default:
	{
	  // HTTP digest
	  n_pUaSecProtocolType[0] = 0x01;
	  n_pUaSecProtocolType[1] = 0x00;
      n_pUaSecProtocolType[2] = 0x00;
      n_pUaSecProtocolType[3] = 0x00;
      n_pUaSecProtocolType[4] = 0x02;
	}
  }
  return;
}
/************************************************************************
Function IMSServiceConfigGBAUIM::ResetChallengeStruct()

Description
This function Reset and deletes the pointers held by challenge structure

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPVOID IMSServiceConfigGBAUIM::ResetUaAuthParams(QP_AUTH_PARAMS& n_stChallengeParams)
{
  IMS_MSG_MED_0(LOG_IMS_UT,"IMSServiceConfigGBAUIM::ResetUaAuthParams");
  for(QPUINT32 i = 0; i < QP_SIP_MAX_DIGEST_CLN_CNT; i++)
  {
    if(QP_NULL != n_stChallengeParams.m_pDigestChallenge[i].pDigestRespose)
    {
      qpDplFree(MEM_IMS_UT, n_stChallengeParams.m_pDigestChallenge[i].pDigestRespose);
      n_stChallengeParams.m_pDigestChallenge[i].pDigestRespose = QP_NULL;
    }
  }
  qpDplMemset(&n_stChallengeParams, 0, sizeof(QP_AUTH_PARAMS));
}

/************************************************************************
Function IMSServiceConfigGBAUIM::ProcessUa3XXResponse()

Description

Dependencies
None

Return Value
None

Side Effects
None
************************************************************************/
QPUINT8 IMSServiceConfigGBAUIM::ProcessUa3XXResponse()
{  
   IMS_MSG_MED_0(LOG_IMS_UT,"IMSServiceConfig::ProcessUa3XXResponse - Entry");
   QPUINT8 iRetValue  = QP_FAILURE;
   /*Retrieve redirected protocol type and new URI from protocol*/
   QPCHAR* n_Uri = (QPCHAR*) qpDplMalloc(MEM_IMS_UT, QP_MAX_SERVER_URI_SIZE *sizeof(QPCHAR*));
   if(n_Uri)
   {
     qpDplMemset(n_Uri, 0, QP_MAX_SERVER_URI_SIZE *sizeof(QPCHAR*));
   }
   m_eHttpConnectionType  =  m_pHttpConnection->GetRedirectedServerDetails(&n_Uri);

   if (n_Uri != QP_NULL  && qpDplStrlen(n_Uri)>0)
   {     
     (QPVOID)qpDplStrlncpy(m_stHttpMessageContent.n_Uri, n_Uri, QP_MAX_SERVER_URI_SIZE, QP_MAX_SERVER_URI_SIZE);	  
   }
   if (n_Uri != QP_NULL)
   {
     qpDplFree(MEM_IMS_UT, n_Uri);
     n_Uri = QP_NULL;  
   }
   IMS_MSG_MED_1(LOG_IMS_UT,"IMSServiceConfig::ProcessUa3XXResponse -m_eHttpConnectionType[%d]", m_eHttpConnectionType);

   /* Reset the previous Nonce if present as a new nonce would have come in this challenge */
   ResetUaAuthParams(m_stUaAuthParams);
   m_pGBAAuth->ResetPreviousAuthParams();	
   if(m_eHttpConnectionType ==  QP_HTTPS_CONNECTION)
   {    
     iRetValue =  InitiateGBAUaProcedure();     
   }   
   else
   {    
     if(SetUpHttpConnection(m_stHttpMessageContent))
     {
       iRetValue = Send(m_stHttpMessageContent);
	 }
   }
   IMS_MSG_MED_1(LOG_IMS_UT,"IMSServiceConfig::ProcessUa3XXResponse - Exit bRetValue[%d]", bRetValue);
   return iRetValue;
}
