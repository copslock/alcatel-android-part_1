/************************************************************************
 Copyright (C) 2013 Qualcomm Technologies Incorporation .All Rights Reserved.

 File Name      : qpIMSServConfigGBADefines.h
 Description    : IMS Service Config realted all the define/struct/enum
 

 ========================================================================
    Date    |   Author's Name    |  BugID  |        Change Description
 ========================================================================
 -------------------------------------------------------------------------------
 12-May-2015   Sreenidhi     789667      FR25901: Ut changes for migrating IMS over to UIM GBA solution 
************************************************************************/ 

#ifndef __QPIMSSERVCONFIGGBADEFINES__
#define __QPIMSSERVCONFIGGBADEFINES__

#include "ims_variation.h"
#include "qpConfigNVItem.h"
#include "qpGAAGBAAuth.h"


/*
** Function Pointer for the callback from IMS Service Config GBA utility to 
** IMS Service Config. This is needed as Challenege calculation is asynchronous from DPL
** typedef QPVOID (*QP_HANDLE_GBA_AUTH_CALLBACK)(QPVOID* pUserData, QPVOID* pAuthHeader);
**                                        
** QP_HANDLE_GBA_AUTH_CALLBACK is function pointer to the AUTH callback function.
** pUserData is the this pointer
** pChallengeParams is pointer to QP_IMS_SERV_CONFIG_CHALLENGE_PARAMS sent while calling the function.
*/
typedef QPVOID (*QP_HANDLE_GBA_AUTH_CALLBACK)(QPVOID* pUserData, QPVOID* pChallengeParams);

/**
** This is the user data infromation that IMS Service config passes
** to the internal utility to receive the CB.
**/
typedef struct qpIMSServConfigUserData
{
  QPVOID*                               pthis;
  QP_HANDLE_GBA_AUTH_CALLBACK           pHandleGBAAuthCallBack;
}QP_IMS_SERV_CONFIG_USER_DATA;
/**
** qpIMSServConfigChallengeParams is the structure used by the 
** IMS Service Config to the challenge received on Ua/Ub Interface
** to the internal utility.
**/
typedef struct qpIMSServConfigChallengeParams
{
  QPCHAR*                               pInAuthenticateHeader;
  QPCHAR*                               pOutAuthorizationHeader;
  QPE_GBA_AUTH                          eGBAAuthInterface;
  QPE_AUTH_MSG                          eGBAAuthMsg;
  QPE_GBA_UB_TYPE                       eGBAUbType;
}QP_IMS_SERV_CONFIG_CHALLENGE_PARAMS;

/**
** qpIMSServConfigUaOnReqParams is the structure used
** when Mode is set as On Req and challenge is received 
** for the first time. Authorization header is parsed and 
** relevant data is extracted and filled in this structure.
**/
typedef struct qpIMSServConfigUaOnReqParams
{
  QPBOOL                                bNoncePresent;
  QPBOOL                                bPerformUbAuth;
  QPBOOL                                bPerformAKAv1;
  QPBOOL                                bIsParamIncorrect;
  QPE_GBA_AUTH                          eAuthInterface;
}QP_IMS_SERV_CONFIG_UA_ON_REQ_AUTH_PARAMS;
/**
** qpIMSServConfigBSFHeaderAuthParams is a structure used
** to store the details like: whether tmpi is used, if resp is included
** if auts is included.
**/
typedef struct qpIMSServConfigBSFHeaderAuthParams
{
  QPBOOL                                bTempIMPIUsed;
  QPBOOL                                bAuthRespIncluded;
  QPBOOL                                bAutsIncluded;
}QP_IMS_SERV_CONFIG_BSF_HEAD_AUTH_PARAMS;

/**
** qpIMSServConfigGBAUbAttributes is the structure used 
** to store the values of BTID, Ks Life Time and storing the 
** Life time value in Julian Format
**/
typedef struct qpIMSServConfigGBAUbAttributes
{
  QPCHAR*                               pBTIDValue;
  QPCHAR*                               pKsLifeTime;
  QPTM                                  stValidityTime;
}QP_IMS_SERV_CONFIG_GBA_UB_ATTRIBURES;


/**
** qpIMSServConfigNextChallengeParams is a structure used
** to store the value of Next Nonce and other parameters like Algorithm and 
** Qop associated with it.
**/
typedef struct qpIMSServConfigNextChallengeParams
{
  QPCHAR*                               pAlgorithm;
  QPCHAR*                               pNextNonce;
  QPCHAR*                               pQoP;
  QPCHAR*                               pRealm;
  QPCHAR*                               pOpaque;
}QP_IMS_SERV_CONFIG_NEXT_CHALLENGE_PARAMS;

#endif