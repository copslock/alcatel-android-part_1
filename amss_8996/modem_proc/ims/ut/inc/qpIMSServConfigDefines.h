/************************************************************************
 Copyright (C) 2013 Qualcomm Technologies Incorporation .All Rights Reserved.

 File Name      : qpIMSServConfigDefines.h
 Description    : IMS Service Config realted all the define/struct/enum
 

 ========================================================================
    Date    |   Author's Name    |  BugID  |        Change Description
 ========================================================================
 21-Jun-2013    Priyank             494609     Application ( UT ) changes for FR 3236: GBA/GAA Support
 ----------------------------------------------------------------------------
 26-08-2013     Sankalp             494761     Set Ut apn as sharable so that if any other
                                               application at app side using the same apn
                                               should not have any issue
-------------------------------------------------------------------------------
 04-Oct-2013     Priyank              541854     On receiving 401 with invalid BTID; UE doesn't reinitiate Ub procedure
-------------------------------------------------------------------------------
 14-Nov-2013    Sreenidhi           566148     X-3GPP-Intended-Identity extension-header not surrounded by quotation marks 
 -------------------------------------------------------------------------------
 10-Dec-2013    Sreenidhi           582449     Realm value in the new request to be used from previous 401 challenge
                                               when device has validated next nonce available                                               
 -------------------------------------------------------------------------------
 23-Dec-2013    Sreenidhi           584663     In ON_REQUEST mode, if nonce is received during the bootup inital request;
                                               UE doesn't do Ub procedure
 -------------------------------------------------------------------------------											                                                  
 23-Dec-2013    Sreenidhi           589936     When TMPI is returned with 403 on Ub procedure; UE does not fall back to IMPI based retry
 ------------------------------------------------------------------------------- 
 20-Feb-2014    Sreenidhi           614122     UE to use TMPI only when server announces its support for the same
 ------------------------------------------------------------------------------- 
 29-Apr-2014    Priyank             625112     FR 20013: TLS for XCAP
 -------------------------------------------------------------------------------
24-June-2014      asharm               660626     Authorization's QOP header should not be sent with DQUOTE
-----------------------------------------------------------------------------------------------------------------------
10-Aug-2014      asharm             696761     UE should handle two realm values in WWW Authenticate header in GBA for TMO 
--------------------------------------------------------------------------------------------------
04-Dec-2014      Sreenidhi          738799     For GBA over TLS, Ut Interface doesn't use correct Security Identifier Protocol
--------------------------------------------------------------------------------------------------
11-Feb-2015      asharm             775514     Ut Interface changes for IWLAN
-------------------------------------------------------------------------------
12-May-2015      Sreenidhi          789667     FR25901: Ut changes for migrating IMS over to UIM GBA solution
-------------------------------------------------------------------------------
11-Jun-15        asharm      827203   FR 27504: support Ub over HTTPS and redirection
--------------------------------------------------------------------------------------------------
29-Jun-2014      asharm           855226     TMO-UCE:FR:24894/27504: Aggregate TMO UCE customization in GBA module
--------------------------------------------------------------------------------------------------
15-Apr-16      Sreenidhi          996194      FR 34850: RJIL Ut IP fallback support
************************************************************************/ 

#ifndef __QPIMSSERVCONFIGDEFINES__
#define __QPIMSSERVCONFIGDEFINES__

#include "ims_variation.h"
#include "qpConfigNVItem.h"
#include <QimfHttp.h>


#define QP_IMS_SERV_CONFIG_PDP_PROFILE_NAME_LEN       64
#define QP_IMS_SERV_CONFIG_MAX_3GPP_IDENTITY_LEN      258

#define IMS_SERV_CONFIG_DOUBLE_QUOTE             (QPCHAR*)"\""
#define IMS_SERV_CONFIG_FRWD_SLASH               (QPCHAR*)"/"
#define IMS_SERV_CONFIG_SEMI_COLON               (QPCHAR*)";"


#define QP_IMS_SERV_CONFIG_BSF_URI_LEN                256
#define QP_IMS_SERV_CONFIG_UTC_FORMAT_LEN             25

#define IMS_SERV_CONFIG_UB_USER_AGENT            (QPCHAR*)"3gpp-gba-tmpi"
#define IMS_SERV_CONFIG_UB_UICC_USER_AGENT       (QPCHAR*)"3gpp-gba-uicc"
#define IMS_SERV_CONFIG_UA_USER_AGENT            (QPCHAR*)"3gpp-gba"
#define IMS_SERV_CONFIG_UA_REALM                 (QPCHAR*)"3GPP-bootstrapping@"
#define IMS_SERV_CONFIG_UA_UICC_REALM            (QPCHAR*)"3GPP-bootstrapping-uicc@"
#define IMS_SERV_CONFIG_ALGO_AKAV1               (QPCHAR*)"AKAv1-MD5"
#define IMS_SERV_CONFIG_TMPI_PRODUCT_TOKEN       (QPCHAR*)"3gpp-gba-tmpi"

/**
** Ua security protocol identifier obtained from tls session
** (in line with values in sec layer, after accounting for network byte order)
**/
typedef enum eQP_TLSUaSecProtocolId { 
  QP_CS_ID_RSA_WITH_3DES_EDE_CBC_SHA     = 2560,     /* 0x000a */
  QP_CS_ID_DHE_RSA_WITH_3DES_EDE_CBC_SHA = 5632,     /* 0x0016 */
  QP_CS_ID_RSA_WITH_AES_128_CBC_SHA      = 12032,    /* 0x002f */
  QP_CS_ID_DHE_RSA_WITH_AES_128_CBC_SHA  = 13056     /* 0x0033 */
}QP_TLS_UA_SEC_PROTOCOL_ID;

/**
** qpIMSServConfigGBAUaAuthParams is this structure used
** by IMS Service Config to get the Authorization header 
** generated on Ua Interface
**/
typedef struct qpIMSServConfigGBAUaAuthParams
{
  QPCHAR*                               pUsername;
  QPCHAR*                               pRealm;
  QPCHAR*                               pURI;
  QPCHAR*                               pEntityBody;
  QPCHAR*                               pMethodName;
  QPCHAR*                               pAuthorizationHeader;
}QP_IMS_SERV_CONFIG_GBA_UA_AUTH_PARAMS;
/**
** qp_http_message_content is the structure which 
** Application use to pass the HTTP content to
** IMS Service Config layer to send the data onto N/w
**/
typedef struct qp_http_message_content
{
  QPCHAR                        n_Uri[QP_MAX_SERVER_URI_SIZE];
  QPCHAR                        n_Url[QP_MAX_SERVER_URL_SIZE];
  QPCHAR                        n_XmlContent[QP_MAX_BODY_SIZE];
  QPCHAR                        n_MethodName[MAX_METHOD_STR_LEN];
  QPCHAR                        n_XmlContentType[MAX_CONTENT_TYPE_STR_LEN];
  QPCHAR                        n_HttpIntendedIdentity[QP_IMS_SERV_CONFIG_MAX_3GPP_IDENTITY_LEN];
  QP_ADDITIONAL_HEADER_INFO     n_AdditionalHeaders[QP_MAX_HEADERS];
  QPUINT32                      n_AdditionalHdrsCount;	
}QP_HTTP_MESSAGE_CONTENT;


/**
 * \enum eIPADDR_TYPE
 * \brief IPAddress type enumeration
 *
 * IPAddress type enumeration
 */
typedef enum eIMS_SERV_IPADDR_TYPE {
/** UNKNOWN      */
 IMS_SERV_UNKNOWN_IP_TYPE = 0,
/** IPV4 Address */
  IMS_SERV_IPV4 = 1,
/** IPV6 Address */
  IMS_SERV_IPV6   = 2,
/**  3 - Not supported, IPV4V6 Address*/
/** IPV4V6 Address, Primary IPv4, Fallabck IPv6*/
  IMS_SERV_IPV4V6 = 4,
  /** IPV6V4 Address, Primary IPv6, Fallabck IPv4*/
  IMS_SERV_IPV6V4 = 5,
  /** IPV4V6 Address, Primary IPv4, Fallabck IPv6, Use Primary IP Type after LPM/Online*/
  IMS_SERV_IPV4V6PIP = 6,
  /** IPV6V4 Address, Primary IPv6, Fallabck IPv4, Use Primary IP Type after LPM/Online*/
  IMS_SERV_IPV6V4PIP = 7
}QPE_IMS_SERV_CONFIG_IPADDR_TYPE;


struct QpImsServDcmProfileInfo
{
  /** RAT associated with this profile */
  QPE_DCM_RAT     eDcmRat;
  /** APN Name associated with this Profile */
  QPCHAR      strApnName[QP_APN_NAME_LEN];
  /** APN type */
  QPE_APN_TYPE    eAPNType;
   /** Address Type used during PDN bring up*/
  QPE_IMS_SERV_CONFIG_IPADDR_TYPE    eIPAddrType;
  /** Where APN is shareable with AP **/
  QPBOOL bIsShareable;
  /** If resolved APN is passed(for LTE only), set it to 1*/
  QPBOOL bIsResolvedApnSet;
  /** Null terminated resolved APN name*/
  QPCHAR      strResolvedApnName[QP_APN_NAME_LEN];
};

typedef struct QpImsServDcmProfileInfo QPE_IMS_SERV_DCM_PROFILE_INFO;
/**
** QP_IMS_SERVICE_CONFIG_NV is the structure where Applications pass their respective 
** data to IMS Service Config while creating its instance.
**/
typedef PACKED struct PACKED_POST
{
  QPE_IMS_SERV_DCM_PROFILE_INFO                    imsServConfigProfileInfo;
  QPE_IMS_SERV_DCM_PROFILE_INFO                   imsServConfigProfileInfo_APN2;  /*APN info APN2*/
  QPE_IMS_SERV_CONFIG_GBA_UB_MODE       eGBAUbMode;                   /* GBA Ub Auth Mode : Always On, When Needed */
  QPE_IMS_SERV_CONFIG_GBA_UB_TYPE       eGBAUbType;                   /* GBA Ub Type : GBA_ME, GBA_U, Both */
  QPBOOL                                bAPNIsShareable;
  QPE_IMS_SERV_CONFIG_GBA_TLS_MODE      eGBATLSMode;                  /* GBA TLS Mode */
  QPE_IMS_SERV_CONFIG_GBA_TLS_MODE_OVER_UB      eGBAUbTLSMode;       /* GBA TLS Mode over Ub */
}QP_IMS_SERVICE_CONFIG_NV;

#endif