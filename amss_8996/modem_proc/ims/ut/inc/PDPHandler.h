/*****************************************************************************
 Copyright (C) 2012 Qualcomm Technologies Incorporation .All Rights Reserved.

 File: PDPHandler.h
 Description:This file moniters PDP

 Revision History
===============================================================================
   Date    |   Author's Name    |  BugID  |        Change Description
===============================================================================
 30-11-2012     Priyank             395900            Initial Check-In
 ----------------------------------------------------------------------------
 20-02-2013     Priyank             453967       Device is trying to bring PDP without 
                                                 checking whether there is valid rat or not
-------------------------------------------------------------------------------------------------------
 21-Jun-2013    Priyank             494609       Application ( UT ) changes for FR 3236: GBA/GAA Support
-------------------------------------------------------------------------------
 07-08-13       Sankalp             481175       Policy Mananger FR 3126
 -------------------------------------------------------------------------
 03-Apr-2014    Priyank             525151    FR 19971: XCAP/Ut PDN Connection ON/Off Dynamically
------------------------------------------------------------------------------- 
25-Apr-14       asharm             634535     FR 20207: QMI Interface to configure Ut APN dynamically - IMS
-------------------------------------------------------------------------------
08-May-14       asharm             649672     FR 20509: XCAP PDN, an On demand PDN.
-------------------------------------------------------------------------------
23-May-14       asharm             664862     Ut first request is failed after device boots up and no error message to user
-------------------------------------------------------------------------------
13-June-14      asharm              607605     [8974 LA4.0 DI4.0] Hysteresis timer is not started when Ut 
                                                recovery timer fired before PDP bring up
-----------------------------------------------------------------------------------------------------------
25-June-14      asharm              677437     Configurable UT RAT mask 
-----------------------------------------------------------------------------------------------------------
23-July-14      asharm              695873    FR 20509-Hysteresis timer is not cleaned in case of SIM refresh PDP failure
-----------------------------------------------------------------------------------------------------------
06-08-14      asharm              699860   UE is bringing up Ut PDP for Unsupported SSD request when no request sent to server
--------------------------------------------------------------------------------------------------
11-Feb-2015      asharm             775514     Ut Interface changes for IWLAN
-------------------------------------------------------------------------------
01-Jul-15       asharm          868934      EE- ePDG support for Ut
--------------------------------------------------------------------------------------------------
11-Sept-2015      asharm            899840     FR 27504: TMO UCE - Add support for local breakout for ACD and XDMC
--------------------------------------------------------------------------------------------------
15-Apr-16      Sreenidhi          996194      FR 34850: RJIL Ut IP fallback support
******************************************************************************/

#ifndef __PDPHANDLER_H__
#define __PDPHANDLER_H__

#include "IMSServConfigEvent.h"
#include "qpIMSServConfigDefines.h"
#include <qpPlatformConfig.h>
#include <qpdcm.h>

typedef enum PDPState
{
  ePDPStateInit,
  ePDPStateActivatingPdp,
  ePDPStatepdpActivated,
  ePDPStateDeactivatingPdp,
  ePDPForceDeactivate,
}PDPHANDLERSTATE;

typedef enum PDNTYPE
{
  AlwaysOnPDN, 
  OnDemandPDN
}PDNBRINGUPTYPE ;

typedef struct WifiPrefs
{
  QPE_WFC_STATUS                       wFCStatus;
  QPE_CALL_MODE_PREFERENCE             callModePreference;
}QP_WIFI_PREF;

// Forward Declaration
class CPDPRecoveryTimer;

class CPDPHandler
{
public:
  CPDPHandler();
  ~CPDPHandler();
  QPVOID Init(QP_IMS_SERVICE_CONFIG_NV *, PDNBRINGUPTYPE = AlwaysOnPDN);
  QPVOID DeInit();
  QPVOID PDNRecoveryTimerFired(QPINT n_iID);
  static QPVOID DcmCallback(QPE_DCM_MSG tDcmMsg, QPDCM_CONN_PROFILE* pDcmProfile, QPVOID* pMsgData);
  QPE_DCM_RAT GetRAT(QPBOOL bQueryServingSystem =QP_FALSE);
  QPUINT32 GetPDPID(){return m_PDPID;}
  QPDCM_CONN_PROFILE* GetDcmConnProfile(){return m_pDcmProfile;}
  /*Register call back with lower layer to get the generic info e.g rat, t3402 , tty mode*/
  static QPVOID GenericDcmCallback(QPE_DCM_MSG  tDcmMsg, QPVOID*  pUserData, QPVOID*    pMsgData);
  QPVOID HandleSrvStatus(QPBOOL n_bSrvSatatus);
   /*Enable Ut PDN*/
  QPVOID EnablePDP();
  /*Disable Ut PDN*/
  QPVOID DisablePDP(QPBOOL n_bBootUpStatus = QP_FALSE);
  /*Delete PDN Profile, New profile will be created in Init function*/
  QPVOID DeleteProfile();
  QPBOOL IsPDPActive() { return ((m_ePDPState == ePDPStatepdpActivated)? QP_TRUE:QP_FALSE );}  
  QPVOID SetPDPAppStatus(QPBOOL n_bPDPAppStatus) {m_bPDPAPPStatus = n_bPDPAppStatus;}
  PDNBRINGUPTYPE GetPDNType(){return m_ePDNType;}
  QPUINT32 DcmFailureRecoveryType();
  QPVOID SetRATMask(QPUINT32 uRATMask) {m_iRatMaskValue = uRATMask;}
  QPBOOL IsValidRAT() {return m_bIsRatValid;}
  QPVOID CreateProfile();
  
  /*Get Previous RAT to compare if profile needs to be deleted/created*/
  QPE_DCM_RAT GetPreviousRAT() {return m_ePreviousRAT;}
  
  /*Handle wifi  preference change, recalculate in RAT*/
  QPVOID HandleWifiPreferencesChange();

  QPVOID SetAPN2PrefSys() { m_bIsAPN2PrefSysNotIWLAN = QP_TRUE;}
  QPBOOL GetAPN2PrefSys() { return m_bIsAPN2PrefSysNotIWLAN;}
  QPVOID SingoConfigUpdatedInd();
 /*Pass LPM-Online status to PDPHandler class to decide IP Addrtype for PDN bring up*/
  QPVOID SetPowerDownStatus(QPBOOL n_bPowerDownStatus);
  QPBOOL IsIPFallBackInitiated();
  QPVOID PerformIPFallback();
  QPVOID ResetIPFallbackStatusForThisRequest() {m_bIsIPFallBackDoneForThisRequest = QP_FALSE;}
private :

  //Copy Constructor and Operator = 
  CPDPHandler(const CPDPHandler&);
  CPDPHandler& operator=(const CPDPHandler &);
  QPVOID PdpActivate();
  QPVOID PdpDeactivate();
  QPVOID EventPdpActivated();
  QPVOID EventPdpDeActivated();
  QPVOID EventPdpFailure(QPE_DCM_FAILURE_MSG iFailureReason = DCM_FAILURE_UNKNOWN);
  QPVOID RaiseEvent( IMS_SERV_CONFIG_EVENT n_eUTEvent);
  QPVOID EventChangeRat(QPE_DCM_RAT iNewRat);
  QPVOID CanPDPActivated();  
  QPBOOL IsValidRat(const QPE_DCM_RAT&);

   /*When call back received from lower layer call this public api to handle it*/
  QPVOID HandleRATNotification(QPE_DCM_RAT n_eDcmRat, QPE_DCM_RAT_MASK n_eRATMask);

  /*Finds the preferred RAT based on the RAT mask and user preferences*/
  QPE_DCM_RAT GetPreferredRAT(QPE_DCM_RAT_MASK n_eRATMask) ;
  
  /*Retrieve GWL/WWAN RAT from RATmask provided from DPL*/
  QPE_DCM_RAT RetrieveGWLRAT(const QPE_DCM_RAT_MASK ratMask);

  /*Retrieve IWLAN RAT from RATmask provided from DPL*/
  QPE_DCM_RAT RetrieveIWLANRAT(const QPE_DCM_RAT_MASK ratMask);
  
  /* Read wifi preference settings from NV*/
  QPVOID ReadWifiPreferences();

  /*Retrieve WLAN RAT from RATmask provided from DPL*/
  QPE_DCM_RAT RetrieveWLANRAT(const QPE_DCM_RAT_MASK ratMask);

private:
  QPDCM_CONN_PROFILE*    m_pDcmProfile;                    // Pointer to DCM Connection Profile
  PDPHANDLERSTATE        m_ePDPState;                      // Holds the PDP state information
  QPE_DCM_FAILURE_MSG    m_eFailureReason;                 // PDP failure reason
  CPDPRecoveryTimer*     m_pPDPRetryTimer;                 // PDP retry timer value
  QPE_DCM_RAT            m_eCurrentRAT;                    // Holds current RAT information
  QPBOOL                 m_bIsRatValid;                    // Whether the current RAT is valid
  QPUINT32               m_PDPID;                          // PDP id 
  QPDCM_PROFILE_INFO     m_DcmProfileInfo;                 // DCM Profile Info
  QPBOOL                 m_bRecoveryInProgress;            //This will be set to true if app side service is closed
  QPBOOL                 m_bPDPAPPStatus;
  QPUINT32               m_pPDNRetryTimerValLong;
  QPUINT32               m_pPDNRetryTimerValShort;
  PDNBRINGUPTYPE         m_ePDNType;
  QPUINT32               m_iRatMaskValue;    
  QPE_DCM_RAT_MASK       m_eCurrentRATmask;               /*Store current RatMask info received from DPL when Serving System Changed*/          
  QP_WIFI_PREF          m_sWifiPreferences;               /*Current Wifi preference settings*/ 
  QPE_DCM_RAT           m_ePreviousRAT;                     // Holds current RAT information               
  QPBOOL                 m_bIsAPN2PrefSysNotIWLAN;                   // var determines if UE needs to skip RAT calculation for IWLAN using wifi preference, and not set DS pref system
  QPBOOL                 m_bIPFallbackEnabled;
  QPE_IPADDR_TYPE        m_ePreferredIPType;
  QPBOOL                 m_bProfileDeletionPending;
  QPBOOL                 m_bIsIPFallBackInitiated;
  QPBOOL                 m_bIsIPFallBackDoneForThisRequest;
};

//--------------------------------------

class CPDPRecoveryTimer: public MafTimer
{
public:
	CPDPRecoveryTimer(CPDPHandler* n_pPDPHandler);
	virtual ~CPDPRecoveryTimer();
	// Callback for MafTimer
	virtual QPVOID TimerEventCallback();
	QPBOOL StartTimer(QPUINT32 iDelayTime, QPINT n_iID);
	QPVOID StopTimer();
private:
	CPDPHandler* m_pPDPHandler;
};
#endif //__PDPHANDLER_H__