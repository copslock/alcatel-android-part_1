﻿#ifndef QIPCALLDTMF_H
#define QIPCALLDTMF_H
/*===========================================================================

Q I P C A L L    DTMF


DESCRIPTION
This file contains the implementation of DTMF related functions

EXTERNALIZED FUNCTIONS


INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2006 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government. Diversion contrary to U.S. law prohibited.
===========================================================================*/

/*===========================================================================

EDIT HISTORY FOR FILE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/ims/qipcall/inc/qipcalldtmf.h#2 $
$DateTime: 2016/04/05 15:30:38 $
$Author: 
$Change: 10208881 $

when       who     what, where, why
--------   ---    ----------------------------------------------------------
03/26/15   lw     First Version
===========================================================================*/
#include <qipcall.h>



/*==========================================================================

                          FUNCTION DECLARATIONS

===========================================================================*/

/*===========================================================================
FUNCTION  qipcall_cleanup_dtmf_state

DESCRIPTION
Clean up call_id related DTMF event
and stop dtmf_timer if the timer owner has the same call_id

DEPENDENCIES
None

RETURN VALUE
None

SIDE EFFECTS
None
===========================================================================*/
QPVOID qipcall_cleanup_dtmf_state(QP_CALL_ID call_id);

/*===========================================================================
FUNCTION   qipcall_process_stop_dtmf_event
DESCRIPTION
Processes a stop dtmf event msg
DEPENDENCIES
None
RETURN VALUE
None
SIDE EFFECTS
None
===========================================================================*/
QPVOID qipcall_process_stop_dtmf_event(const qipcall_msg_type *msg);

/*===========================================================================
FUNCTION   qipcall_process_start_dtmf_event
DESCRIPTION
Processes a start dtmf event msg
DEPENDENCIES
None
RETURN VALUE
None
SIDE EFFECTS
None
===========================================================================*/
QPVOID qipcall_process_start_dtmf_event(const qipcall_msg_type *msg);

#endif