#ifndef QIPCALLSRVCC_H
#define QIPCALLSRVCC_H
/*===========================================================================

Q I P C A L L    DTMF


DESCRIPTION
This file contains the implementation of DTMF related functions

EXTERNALIZED FUNCTIONS


INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2006 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government. Diversion contrary to U.S. law prohibited.
===========================================================================*/

/*===========================================================================

EDIT HISTORY FOR FILE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/ims/qipcall/inc/qipcall_srvcc.h#1 $
$DateTime: 2016/05/20 18:36:27 $
$Author: 
$Change: 10523071 $

when       who     what, where, why
--------   ---    ----------------------------------------------------------
05/04/16   lw     CR 1009866: Initial Version
===========================================================================*/
#include <qipcallh.h>

/*=========================================================================*/
/**
*   Description: Populates required SRVCC structures and writes this information to DPL Msg Router interface
*   
*   @param  void
*   @return void
*
*/
QPVOID qipcall_srvcc_send_call_information_list(QPVOID);

/*=========================================================================*/
/**
*   Description: qipcall_is_call_alerting
*   check if call is in alerting state before connection, 180 RINGING is sent or received
*
*/
QPBOOL qipcall_is_call_alerting( qipcallh_obj_type* call_ptr );

/*=========================================================================*/
/**
*   Description: qipcall_is_call_pre_alerting
*   check if call is in pre-alerting state, before 180 RINGING is sent or received
*
*/
QPBOOL qipcall_is_call_pre_alerting( qipcallh_obj_type* call_ptr );

#endif