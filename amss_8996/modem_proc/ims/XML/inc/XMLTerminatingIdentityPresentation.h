/*****************************************************************************
  Copyright (c) 2013 Qualcomm Technologies, Inc. All rights reserved.
  File  : XMLTerminatingIdentityPresentation.h
  Desc  : 

  Revision History 
  Rohit Initial Revision
===============================================================================
  Date      |   Author's Name       |  BugID  |      Change Description
===============================================================================
 19-11-2013     Sreenidhi          549221     FR 17157: XML Configuration Access Protocol (XCAP) / Ut support for KDDI    
*****************************************************************************/
#ifndef __XMLTERMINATINGIDENTITYPRESENTATION_H__
#define __XMLTERMINATINGIDENTITYPRESENTATION_H__

#include <qpXMLDocGenerator.h>
#include <qplog.h>

#include "XMLCommonDef.h"
#include "XMLSimservsXcap.h"

#include <XMLContext.h>


class qp_terminating_identity_presentation : public qp_ns_xcap_simservType
{
private:
  XML_NAMESPACES_CB_CD								uriPrefix;
  qp_terminating_identity_presentation(const qp_terminating_identity_presentation&){};
  qp_terminating_identity_presentation& operator=(const qp_terminating_identity_presentation&){return *this;}

public:
  qp_terminating_identity_presentation(){ uriPrefix = (XML_NAMESPACES_CB_CD)XMLNS_SS; };
  ~qp_terminating_identity_presentation(){};
		

  QPBOOL		 			ProcessUnMarshall(QPCHAR* n_Element,QPCHAR* n_Value, XMLElement* n_Object);
  QPBOOL					ProcessUnMarshallAttribute(QPCHAR* n_Element,QPCHAR* n_Value,XMLAttribute* n_Object);

  QPBOOL IsRootElement(){
  return QP_TRUE;
  }
  XML_NAMESPACES_CB_CD		getUriPrefix()
  {
	  return uriPrefix;
  }
  QPBOOL					toggleUriPrefix();

  QPCHAR* ClassName();
  QPCHAR* ClassType(){ return (QPCHAR*)"XMLObject"; }
  QPCHAR* TagName(){ return (QPCHAR*)"TerminatingIdentityPresentation"; }
  QPUINT8 Valiadate(){ return 0; }
};

class TIPMarshaller : public XMLMarshaller, QpXMLDocGenerator
{

private:
  QPCHAR*		            		mTIPXml;
  qp_xml_common_array_definition*   m_pCommonArray;

public:
  TIPMarshaller();  
  ~TIPMarshaller();

  const QPCHAR* Marshall(XMLElement* pObj);


  QPVOID  Marshall(XMLObject* pObj, XMLFile* pfile){}

  QPBOOL AddTIPElementToXml(qp_terminating_identity_presentation* terminatingIdentityPresentation);
};

class TIPContext : public XMLContext
{
private:

	TIPContext(const TIPContext&){};
    TIPContext& operator=(const TIPContext&){return *this;}
	
	static QPCHAR *initNameSpacePrefix[(XML_NAMESPACES_CB_CD)XMLNS_MAX];
	static QPCHAR *NamespaceUri[3];
	static QPUINT uriListSize;
	QPCHAR** NSPrefix;
	QPVOID initNamespacePrefix();

public:
	XMLObject*		m_pNameSpaceinfo;
  TIPContext();
  ~TIPContext();

  using XMLContext::setNamespacePrefix;

  XMLMarshaller* createMarshaller();
  XMLUnMarshaller* createUnMarshaller();
  XMLObjectFactory* createBinderFactory();
  
  QPBOOL setNamespacePrefix(XML_NAMESPACES_CB_CD ns, QPCHAR* prefix);
  QPBOOL setNamespacePrefix(QPUINT ns, QPCHAR* prefix);
  QPCHAR* getNamespacePrefix(XML_NAMESPACES_CB_CD ns);
  QPCHAR* getNamespaceUri(QPUINT iIndex);
  QPUINT  getUriListSize();
};

class TIPDataBinder : public XMLObjectFactory
{

public:
	TIPDataBinder(){};  
	~TIPDataBinder(){};

  XMLObject* CreateRootElement();
};

class TIPUnMarshaller : public XMLUnMarshaller
{
public:
	TIPUnMarshaller();  
	~TIPUnMarshaller();

  XMLObject* UnMarshall(const QPCHAR* pstring);
  XMLObject* UnMarshall(XMLFile* pfile);
};

  QPVOID TestTIPMarshaller(QPVOID);

#endif // end of __XMLTERMINATINGIDENTITYPRESENTATION_H__