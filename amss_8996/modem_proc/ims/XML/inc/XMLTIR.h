/*****************************************************************************
  Copyright (c) 2011-2012 Qualcomm Technologies, Inc. All rights reserved.
  File  : XMLCommunicationDiversion.h
  Desc  : 
  Author: -----
  Date  : -----

  Revision History 
  Rohit Initial Revision
===============================================================================
  Date      |   Author's Name       |  BugID  |      Change Description
===============================================================================
 ---------   ----------------------    ----      Defining the methods.
-------------------------------------------------------------------------------
*****************************************************************************/

#ifndef __XMLTIR_H__
#define __XMLTIR_H__

#include <qpXMLDocGenerator.h>
#include <qplog.h>

#include "XMLCommonDef.h"
#include "XMLSimservsXcap.h"

typedef enum qp_Xml_TIR_DEFAULT_BEHAVIOUR
{
	TIR_DEFAULT_PRESENTATION_NOT_RESTRICTED,
	TIR_DEFAULT_PRESENTATION_RESTRICTED,
	TIR_DEFAULT_MAX
}e_qp_Xml_TIR_DEFAULT_BEHAVIOUR;


class qp_tir_DefaultBehaviour : public XMLElement
{
private:
  qp_tir_DefaultBehaviour(const qp_tir_DefaultBehaviour&){};
  qp_tir_DefaultBehaviour& operator=(const qp_tir_DefaultBehaviour&){return *this;}

  e_qp_Xml_TIR_DEFAULT_BEHAVIOUR					mDefaultBehaviourValue;
  QPBOOL											mDefaultBehaviourPresent;

public:
	qp_tir_DefaultBehaviour(){
	  mDefaultBehaviourValue = TIR_DEFAULT_PRESENTATION_RESTRICTED;
	  mDefaultBehaviourPresent = QP_FALSE;
	}
	virtual ~qp_tir_DefaultBehaviour(){
		mDefaultBehaviourValue = TIR_DEFAULT_PRESENTATION_RESTRICTED;
		mDefaultBehaviourPresent = QP_FALSE;
	}

	QPBOOL									setDefaultBehaviourValue(e_qp_Xml_TIR_DEFAULT_BEHAVIOUR default_behaviour);
  e_qp_Xml_TIR_DEFAULT_BEHAVIOUR			getDefaultBehaviourValue() {
	  return mDefaultBehaviourValue;
  }

  QPBOOL									setIsDefaultBehaviourElementPresent(QPBOOL	present);
  QPBOOL									getIsDefaultBehaviourElementPresent(){return mDefaultBehaviourPresent;};

  QPBOOL		 			ProcessUnMarshall(QPCHAR* n_Element,QPCHAR* n_Value, XMLElement* n_Object){return QP_TRUE;};
  QPBOOL					ProcessUnMarshallAttribute(QPCHAR* n_Element,QPCHAR* n_Value,XMLAttribute* n_Object){return QP_TRUE;};
  QPCHAR* ClassName(){ return (QPCHAR*)"qp_DefaultBehaviour";};
  QPCHAR* ClassType(){ return (QPCHAR*)"XMLObject"; }
  QPCHAR* TagName(){ return (QPCHAR*)"default-behaviour"; }
  QPUINT8 Valiadate(){ return 0; }

};

class qp_tir : public qp_ns_xcap_simservType
{
private:
  XML_NAMESPACES_CB_CD								uriPrefix;
  qp_tir_DefaultBehaviour*							mDefaultBehaviour;
  
  qp_tir(const qp_tir&){};
  qp_tir& operator=(const qp_tir&){return *this;}

public:
  qp_tir();
  ~qp_tir();

  QPBOOL									setDefaultBehaviour(qp_tir_DefaultBehaviour* default_behaviuor);
  qp_tir_DefaultBehaviour*					getDefaultBehaviour() {
	  return mDefaultBehaviour;
  }

  XML_NAMESPACES_CB_CD		getUriPrefix()
  {
	  return uriPrefix;
  }
  QPBOOL					toggleUriPrefix();

  QPBOOL		 			ProcessUnMarshall(QPCHAR* n_Element,QPCHAR* n_Value, XMLElement* n_Object);
  QPBOOL					ProcessUnMarshallAttribute(QPCHAR* n_Element,QPCHAR* n_Value,XMLAttribute* n_Object);
  QPBOOL IsRootElement(){
	  return QP_TRUE;
  }
  QPCHAR* ClassName();
  QPCHAR* ClassType(){ return (QPCHAR*)"XMLObject"; }
  QPCHAR* TagName(){ return (QPCHAR*)"terminating-identity-presentation-restriction"; }
  QPUINT8 Valiadate(){ return 0; }

};

class TIRMarshaller : public XMLMarshaller, QpXMLDocGenerator
{

private:
  QPCHAR*		            		mTIRXml;
  qp_xml_common_array_definition*   m_pCommonArray;
public:
  TIRMarshaller();  
  ~TIRMarshaller();

  const QPCHAR* Marshall(XMLElement* pObj);

  QPVOID  Marshall(XMLObject* pObj, XMLFile* pfile){}

  QPBOOL AddTIRElementToXml(qp_tir* 	tir);
  QPBOOL AddTIRDefaultBehaviourElementToXml(qp_tir_DefaultBehaviour* 	default_behaviour);
};

class TIRDataBinder : public XMLObjectFactory
{
public:
	TIRDataBinder(){};
	~TIRDataBinder(){};

  XMLObject* CreateRootElement();
  XMLElement* CreateTIRDefaultBehaviourElement();
};

class TIRContext : public XMLContext
{
private:
	TIRContext(const TIRContext&){};
    TIRContext& operator=(const TIRContext&){return *this;}
	
	static QPCHAR *initNameSpacePrefix[(XML_NAMESPACES_CB_CD)XMLNS_MAX];
	static QPCHAR *NamespaceUri[3];
	static QPUINT uriListSize;
	QPCHAR** NSPrefix;
	QPVOID initNamespacePrefix();
public:
	TIRContext();
	~TIRContext();

	using XMLContext::setNamespacePrefix;

  XMLMarshaller* createMarshaller();
  XMLUnMarshaller* createUnMarshaller();
  XMLObjectFactory* createBinderFactory();
  QPBOOL setNamespacePrefix(XML_NAMESPACES_CB_CD ns, QPCHAR* prefix);
  QPBOOL setNamespacePrefix(QPUINT ns, QPCHAR* prefix);
  QPCHAR* getNamespacePrefix(XML_NAMESPACES_CB_CD ns);
  QPCHAR* getNamespaceUri(QPUINT iIndex);
  QPUINT  getUriListSize();

};

class TIRUnMarshaller : public XMLUnMarshaller
{

public:
	TIRUnMarshaller();
	~TIRUnMarshaller();

  XMLObject* UnMarshall(const QPCHAR* pstring);
  XMLObject* UnMarshall(XMLFile* pfile);
};

#endif
