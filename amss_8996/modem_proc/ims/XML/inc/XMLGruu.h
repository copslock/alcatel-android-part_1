/*****************************************************************************
  Copyright (c) 2011-2012 Qualcomm Technologies, Inc. All rights reserved.
  File  : XMLGruu.h
  Desc  : 
  Author: Suraj Peddi	
  Date  : 24.01.2013

  Revision History 
  Rohit Initial Revision
===============================================================================
  Date      |   Author's Name       |  BugID  |      Change Description
===============================================================================
 24.01.2013   Suraj Peddi     ----      Defining the methods.
-------------------------------------------------------------------------------
*****************************************************************************/

#ifndef __XMLGRUU_H__
#define __XMLGRUU_H__

#include <qpXMLDocGenerator.h>
#include <qplog.h>
#include "XMLCommonDef.h"

class qp_ns_gruuinfo_pubGruu : public XMLObject
{
  private:
  xs_anyURI			m_pUri;

  qp_ns_gruuinfo_pubGruu(const qp_ns_gruuinfo_pubGruu&){};
  qp_ns_gruuinfo_pubGruu& operator=(const qp_ns_gruuinfo_pubGruu&){return *this;}

  public:
	  qp_ns_gruuinfo_pubGruu(){
		  m_pUri = QP_NULL;
	  }

	  virtual ~qp_ns_gruuinfo_pubGruu(){
		  if(m_pUri != QP_NULL)
		  {
			qpDplFree(MEM_IMS_XML,m_pUri);
			m_pUri = QP_NULL;
		  }
	  }

  QPBOOL				   setUriValue(xs_anyURI uri){
	  if(uri != QP_NULL)
	  {
		m_pUri = qpDplStrdup(MEM_IMS_XML,uri);
	  }
	  else
	  {
		m_pUri = QP_NULL;
	  }
	  return QP_TRUE;
  }
  xs_anyURI					getUriValue() {
	  return m_pUri;
  }

  QPBOOL				ProcessUnMarshall(QPCHAR* n_strElement,QPCHAR* n_strVal,XMLElement* n_Object);
  QPBOOL				ProcessUnMarshallAttribute(QPCHAR* n_Attribute,QPCHAR* n_Value, XMLAttribute* n_Object);

  QPBOOL IsRootElement(){
  return QP_FALSE;
  }
  QPCHAR* ClassName(){ return (QPCHAR*)"qp_ns_gruuinfo_pubGruu"; }
  QPCHAR* ClassType(){ return (QPCHAR*)"XMLElement"; }
  QPCHAR* TagName(){ return (QPCHAR*)"pubGruu"; }
  QPUINT8 Valiadate(){ return 0; }
};

class qp_ns_gruuinfo_tempGruu : public qp_ns_gruuinfo_pubGruu
{
  private:
  xs_unsignedLong			m_pFirstCseq;

  qp_ns_gruuinfo_tempGruu(const qp_ns_gruuinfo_tempGruu&){};
  qp_ns_gruuinfo_tempGruu& operator=(const qp_ns_gruuinfo_tempGruu&){return *this;}

  public:
	qp_ns_gruuinfo_tempGruu()
	{
	  m_pFirstCseq = 0;
	}

	virtual ~qp_ns_gruuinfo_tempGruu()
	{
	  m_pFirstCseq = 0;
	}

	QPBOOL					setFirstCseqValue(xs_unsignedLong firstcseq)
	{
	  m_pFirstCseq = firstcseq;
	  return QP_TRUE;
	}
	xs_unsignedLong			getFirstCseqValue() 
	{
	  return m_pFirstCseq;
    }

	QPBOOL					ProcessUnMarshall(QPCHAR* n_strElement,QPCHAR* n_strVal,XMLElement* n_Object);
	QPBOOL					ProcessUnMarshallAttribute(QPCHAR* n_Attribute,QPCHAR* n_Value, XMLAttribute* n_Object);

	QPBOOL IsRootElement()
	{
	  return QP_FALSE;
	}
	QPCHAR* ClassName(){ return (QPCHAR*)"qp_ns_gruuinfo_tempGruu"; }
	QPCHAR* ClassType(){ return (QPCHAR*)"XMLElement"; }
	QPCHAR* TagName(){ return (QPCHAR*)"tempGruu"; }
	QPUINT8 Valiadate(){ return 0; }
};


class qp_pub_gruu : public qp_ns_gruuinfo_pubGruu
{
  private:
    qp_pub_gruu(const qp_pub_gruu&){};
    qp_pub_gruu& operator=(const qp_pub_gruu&){return *this;}

  public:
	qp_pub_gruu(){};
	~qp_pub_gruu(){};
  
	QPBOOL IsRootElement()
	{
	  return QP_TRUE;
	}

	QPBOOL				ProcessUnMarshall(QPCHAR* n_strElement,QPCHAR* n_strVal,XMLElement* n_Object);
	QPBOOL				ProcessUnMarshallAttribute(QPCHAR* n_Attribute,QPCHAR* n_Value, XMLAttribute* n_Object);

	QPCHAR* ClassName();
	QPCHAR* ClassType(){ return (QPCHAR*)"XMLObject"; }
	QPCHAR* TagName(){ return (QPCHAR*)"pub-gruu"; }
	QPUINT8 Valiadate(){ return 0; }
};

class qp_temp_gruu : public qp_ns_gruuinfo_tempGruu
{
  private:
    qp_temp_gruu(const qp_temp_gruu&){};
    qp_temp_gruu& operator=(const qp_temp_gruu&){return *this;}

  public:
	qp_temp_gruu(){};
	~qp_temp_gruu(){};
  
	QPBOOL IsRootElement()
	{
	  return QP_TRUE;
	}

	QPBOOL				ProcessUnMarshall(QPCHAR* n_strElement,QPCHAR* n_strVal,XMLElement* n_Object);
	QPBOOL				ProcessUnMarshallAttribute(QPCHAR* n_Attribute,QPCHAR* n_Value, XMLAttribute* n_Object);

	QPCHAR* ClassName();
	QPCHAR* ClassType(){ return (QPCHAR*)"XMLObject"; }
	QPCHAR* TagName(){ return (QPCHAR*)"temp-gruu"; }
	QPUINT8 Valiadate(){ return 0; }
};


class GruuMarshaller : public XMLMarshaller, QpXMLDocGenerator
{

private:
  QPCHAR*							mGruuXml;
  qp_xml_common_array_definition*   m_pCommonArray;
public:

  GruuMarshaller();  
  ~GruuMarshaller();

  const QPCHAR* Marshall(XMLElement* pObj);
  QPVOID  Marshall(XMLObject* pObj, XMLFile* pfile){};

  QPBOOL AddPubGruuElementToXml(qp_pub_gruu* pubgruu);
  QPBOOL AddTempGruuElementToXml(qp_temp_gruu* tempgruu);
};

class GruuContext : public XMLContext
{
public:
  XMLMarshaller* createMarshaller();
  XMLUnMarshaller* createUnMarshaller();
  XMLObjectFactory* createBinderFactory();
};

class GruuDataBinder : public XMLObjectFactory
{
private:
	QPCHAR*		m_pGruutype;

	GruuDataBinder(const GruuDataBinder&){};
    GruuDataBinder& operator=(const GruuDataBinder&){return *this;}
public:
	GruuDataBinder(){
	  m_pGruutype = QP_NULL;
	}  
	virtual ~GruuDataBinder(){
	  if(m_pGruutype != QP_NULL)
	  {
		qpDplFree(MEM_IMS_XML, m_pGruutype);
		m_pGruutype = QP_NULL;
	  }
	}

QPBOOL	setGruuTypeValue(QPCHAR* gruutype){
  if(m_pGruutype != QP_NULL)
  {
	qpDplFree(MEM_IMS_XML, m_pGruutype);
	m_pGruutype = QP_NULL;
  }
  if(gruutype != QP_NULL)
  {
	m_pGruutype = qpDplStrdup(MEM_IMS_XML,gruutype);
  }
  return QP_TRUE;
}
QPCHAR*  getGruuTypeValue(){
  return m_pGruutype;
}

  XMLObject* CreateRootElement();
  XMLObject* CreatePubGruuElement();
  XMLObject* CreateTempGruuElement();
};

class GruuUnMarshaller : public XMLUnMarshaller
{
public:

	GruuUnMarshaller(){};  
	~GruuUnMarshaller(){};

  XMLObject* UnMarshall(const QPCHAR* pstring);
  XMLObject* UnMarshall(XMLFile* pfile);
};

#endif // end of __XMLGruu_H__
