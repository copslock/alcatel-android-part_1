/*****************************************************************************
Copyright (c) 2011-2012 Qualcomm Technologies, Inc. All rights reserved.
File  : XMLCallWaitingIndication.h
Desc  :
Author: Suraj Peddi
Date  : 09.01.2013

Revision History
Rohit Initial Revision
===============================================================================
Date      |   Author's Name       |  BugID  |      Change Description
===============================================================================
09.01.2013   Suraj Peddi     ----      Defining the methods.
-------------------------------------------------------------------------------
*****************************************************************************/

#ifndef __XMLCWINDICATION_H__
#define __XMLCWINDICATION_H__

#include <qpXMLDocGenerator.h>
#include <qplog.h>
#include "XMLCommonDef.h"

class qp_alternative_service : public XMLElement
{
private:
	xs_string			m_pAction;
	xs_string			m_pType;
	xs_string			m_pReason;

	qp_alternative_service(const qp_alternative_service&){};
	qp_alternative_service& operator=(const qp_alternative_service&) { return *this; }

public:
	qp_alternative_service(){
		m_pAction = QP_NULL;
		m_pType = QP_NULL;
		m_pReason = QP_NULL;
	}

	virtual ~qp_alternative_service();

	QPBOOL							 setActionValue(xs_string action){
		if (action != QP_NULL)
		{
			m_pAction = qpDplStrdup(MEM_IMS_XML,action);
		}
		return QP_TRUE;
	}
	xs_string			 getActionValue() {
		return m_pAction;
	}

	QPBOOL							 setTypeValue(xs_string type){
		if (type != QP_NULL)
		{
			m_pType = qpDplStrdup(MEM_IMS_XML,type);
		}
		return QP_TRUE;
	}
	xs_string			 getTypeValue() {
		return m_pType;
	}

	QPBOOL							 setReasonValue(xs_string reason){
		if (reason != QP_NULL)
		{
			m_pReason = qpDplStrdup(MEM_IMS_XML, reason);
		}
		return QP_TRUE;
	}
	xs_string			 getReasonValue() {
		return m_pReason;
	}

	QPBOOL				ProcessUnMarshall(QPCHAR* n_strElement, QPCHAR* n_strVal, XMLElement* n_Object);
	QPBOOL				ProcessUnMarshallAttribute(QPCHAR* n_Element, QPCHAR* n_Value, XMLAttribute* n_Object);

	QPCHAR* ClassName(){ return (QPCHAR*) "qp_alternative_service"; }
	QPCHAR* ClassType(){ return (QPCHAR*) "XMLElement"; }
	QPCHAR* TagName(){ return (QPCHAR*) "alternative-service"; }
	QPUINT8 Valiadate(){ return 0; }
};


class qp_ims_3gpp : public XMLObject
{
private:
	qp_alternative_service*		m_pAlternativeService;
	xs_Int						m_iVersion;
	xs_string					m_pServiceInfo;

	qp_ims_3gpp(const qp_ims_3gpp&){};
	qp_ims_3gpp& operator=(const qp_ims_3gpp&) { return *this; }

public:
	qp_ims_3gpp();
	~qp_ims_3gpp();

	QPBOOL								setAlternativeServiceValue(qp_alternative_service* alternativeservice);
	qp_alternative_service*   getAlternativeServiceValue() {
		return m_pAlternativeService;
	}

	QPBOOL								setServiceInfoValue(xs_string service_info);
	xs_string   getServiceInfoValue() {
		return m_pServiceInfo;
	}

	QPBOOL							 setVersionValue(xs_Int version){
		if (version != -1)
		{
			m_iVersion = version;
		}
		return QP_TRUE;
	}
	xs_Int							 getVersionValue() {
		return m_iVersion;
	}

	QPBOOL				ProcessUnMarshall(QPCHAR* n_strElement, QPCHAR* n_strVal, XMLElement* n_Object);
	QPBOOL				ProcessUnMarshallAttribute(QPCHAR* n_Element, QPCHAR* n_Value, XMLAttribute* n_Object);

	QPBOOL IsRootElement();

	QPCHAR* ClassName();
	QPCHAR* ClassType(){ return (QPCHAR*) "XMLObject"; }
	QPCHAR* TagName(){ return (QPCHAR*) "ims-3gpp"; }
	QPUINT8 Valiadate(){ return 0; }
};


class CWIndicationMarshaller : public XMLMarshaller, QpXMLDocGenerator
{

private:
	QPCHAR*		            		mCWIndicationXml;
	qp_xml_common_array_definition*   m_pCommonArray;
public:

	CWIndicationMarshaller();
	~CWIndicationMarshaller();

	const QPCHAR* Marshall(XMLElement* pObj);
	QPVOID  Marshall(XMLObject* pObj, XMLFile* pfile){};

	QPBOOL AddIms3gppElementToXml(qp_ims_3gpp* ims_3gpp);
	QPBOOL AddAlternativeServiceElementToXml(qp_alternative_service* alternative_service);
};

class CWIndicationContext : public XMLContext
{
public:
	XMLMarshaller* createMarshaller();
	XMLUnMarshaller* createUnMarshaller();
	XMLObjectFactory* createBinderFactory();
};

class CWIndicationDataBinder : public XMLObjectFactory
{

public:
	CWIndicationDataBinder(){};
	~CWIndicationDataBinder(){};

	XMLObject* CreateRootElement();
	XMLElement* CreateAlternativeServiceElement();
	XMLElement* CreateActionElement();
	XMLElement* CreateCallWaitingIndicationElement();
};

class CWIndicationUnMarshaller : public XMLUnMarshaller
{

public:

	CWIndicationUnMarshaller(){};
	~CWIndicationUnMarshaller(){};

	XMLObject* UnMarshall(const QPCHAR* pstring);
	XMLObject* UnMarshall(XMLFile* pfile);
};

#endif // end of __XMLCWINDICATION_H__
