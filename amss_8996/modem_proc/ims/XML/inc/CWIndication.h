#ifndef __CWINDICATION_H__
#define __CWINDICATION_H__

#include<XMLCWIndication.h>
#include<XMLList.h>
#include<XMLStack.h>
#include <XmlDecoderSchema.h>

class XMLCWIndicationDecoder : public XmlDecoderSchema
{
public:
	XMLCWIndicationDecoder();
	~XMLCWIndicationDecoder();
	XmlCodeGenStack m_codeGenStack;
	qp_ims_3gpp *m_pIms3gpp;

	QPINT ProcessElement(QPCHAR* n_strElement, QPCHAR* n_strVal);
	QPINT ProcessAttribute(QPCHAR* n_strElement, QPCHAR* n_strAttr, QPCHAR* n_strVal);
	QPINT Validate() { return 0; };
	XMLElement* ProcessAnyElement(QPCHAR* n_strElement, QPCHAR* n_strVal){ return QP_NULL; };
	QPINT ProcessState(QPCHAR *n_strElement) { return 0; };
	QPVOID* GetSessionData() { return m_pIms3gpp; };
};
#endif

