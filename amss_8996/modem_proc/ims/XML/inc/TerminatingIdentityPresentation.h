/*****************************************************************************
  Copyright (c) 2013 Qualcomm Technologies, Inc. All rights reserved.
  File  : TerminatingIdentityPresentation.h
  Desc  : 
  Author: 
  Date  : 

  Revision History 
===============================================================================
  Date      |   Author's Name       |  BugID  |      Change Description
===============================================================================
 19-11-2013     Sreenidhi          549221     FR 17157: XML Configuration Access Protocol (XCAP) / Ut support for KDDI    
*****************************************************************************/
#ifndef __TERMINATINGIDENTITYPRESENTATION_H__
#define __TERMINATINGIDENTITYPRESENTATION_H__

#include<XMLTerminatingIdentityPresentation.h>
#include<XMLSimservsXcap.h>
#include<XMLList.h>
#include<XMLStack.h>
#include <XmlDecoderSchema.h>
#include <XMLSchemaNameSpaceRetrieval.h>
#include <XMLCommonDef.h>
#include <XMLContext.h>

class XMLTIPDecoder : public XmlDecoderSchema
{

public:
	XMLTIPDecoder();
	~XMLTIPDecoder();

XmlCodeGenStack					            m_codeGenStack; 
qp_terminating_identity_presentation*		m_eTerminatingIdentityPresentation;
sXmlNameSpaces*					            m_NameSpaces;
QPCHAR*							            m_strDup;
QPCHAR*							            m_PassName;
XMLContext*									mContext;

QPINT ProcessElement(QPCHAR* n_strElement,QPCHAR* n_strVal);
QPINT ProcessAttribute(QPCHAR* n_strElement,QPCHAR* n_strAttr,QPCHAR* n_strVal);
QPINT Validate() {return 0;};
XMLElement* ProcessAnyElement(QPCHAR* n_strElement,QPCHAR* n_strVal){return QP_NULL;};
QPINT ProcessState(QPCHAR *n_strElement) {return 0;}; 
QPVOID* GetSessionData() {return m_eTerminatingIdentityPresentation;}; 
QPBOOL XMLRemoveNameSpaceTag(QPCHAR* n_strElement);

}; 

#endif
  
  