#ifndef __DIALOGINFO_H__
#define __DIALOGINFO_H__

#include<XMLDialogInfo.h>
#include<XMLList.h>
#include<XMLStack.h>
#include<XmlDecoderSchema.h>

class XMLDialogInfoDecoder : public XmlDecoderSchema
{
public:
  XMLDialogInfoDecoder();
  ~XMLDialogInfoDecoder();
  XmlCodeGenStack m_codeGenStack; 
  XMLObject*      m_pDialogInfoObject;
  QPCHAR*         m_PassName;
                    
  QPINT ProcessElement(QPCHAR* n_strElement,QPCHAR* n_strVal);
  QPINT ProcessAttribute(QPCHAR* n_strElement,QPCHAR* n_strAttr,QPCHAR* n_strVal);
  QPINT Validate() {return 0;};
  XMLElement* ProcessAnyElement(QPCHAR* n_strElement,QPCHAR* n_strVal);
  QPINT ProcessState(QPCHAR *n_strElement) {return 0;};
  QPBOOL  XMLRemoveNameSpaceTag(QPCHAR* pElementName);
  QPVOID* GetSessionData() { return m_pDialogInfoObject; };
}; 

#endif

