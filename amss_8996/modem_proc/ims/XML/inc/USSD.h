#ifndef __USSD_H__
#define __USSD_H__

#include<XMLUSSD.h>
#include<XMLList.h>
#include<XMLStack.h>
#include <XmlDecoderSchema.h>
class XMLUSSDDecoder : public XmlDecoderSchema
{

public:
	XMLUSSDDecoder();
	~XMLUSSDDecoder();

XmlCodeGenStack m_codeGenStack; 
qp_ussdData* m_pUssdData;

QPINT ProcessElement(QPCHAR* n_strElement,QPCHAR* n_strVal);
QPINT ProcessAttribute(QPCHAR* n_strElement,QPCHAR* n_strAttr,QPCHAR* n_strVal);
QPINT Validate() {return 0;};
XMLElement* ProcessAnyElement(QPCHAR* n_strElement,QPCHAR* n_strVal){return QP_NULL;};
QPINT ProcessState(QPCHAR *n_strElement) {return 0;}; 
QPVOID* GetSessionData() {return m_pUssdData;}; 
   
}; 


#endif
  
  