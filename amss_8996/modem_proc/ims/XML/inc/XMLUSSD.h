/*****************************************************************************
  Copyright (c) 2011-2012 Qualcomm Technologies, Inc. All rights reserved.
  File  : XMLUSSD.h
  Desc  : 
  Author: Manasi J
  Date  : 05.25.2013

  Revision History 
  Rohit Initial Revision
===============================================================================
  Date      |   Author's Name       |  BugID  |      Change Description
===============================================================================
 06.09.2013   Manasi J                                     Defining USSD methods.
-------------------------------------------------------------------------------
*****************************************************************************/
#ifndef __XMLUSSD_H__
#define __XMLUSSD_H__

#include <qpXMLDocGenerator.h>
#include <qplog.h>
#include "XMLCommonDef.h"

class qp_ussdData : public XMLObject
{
  private:
  xs_string 	mLanguage;
  xs_string    mUssdString;
  xs_Int          mErrorCode;
  
  qp_ussdData(const qp_ussdData&){};
  qp_ussdData& operator=(const qp_ussdData&){return *this;}

  public:
  qp_ussdData();
	virtual ~qp_ussdData();

  QPBOOL				  setLanguageValue(xs_string  Language);
  xs_string  getLanguageValue() {
	  return mLanguage;
  }

  QPBOOL				  setUssdStringValue(xs_string UssdString);
  xs_string  getUssdStringValue() {
	  return mUssdString;
  }

  QPBOOL				  setErrorCodeValue(xs_Int ErrorCode);
  xs_Int getErrorCodeValue() {
	  return mErrorCode;
  }

  QPBOOL				ProcessUnMarshall(QPCHAR* n_strElement,QPCHAR* n_strVal,XMLElement* n_Object);
  
  QPBOOL IsRootElement();

  QPCHAR* ClassName();
  QPCHAR* ClassType(){ return (QPCHAR*)"XMLObject"; }
  QPCHAR* TagName(){ return (QPCHAR*)"ussd-data"; }
  QPUINT8 Valiadate(){ return 0; }

};

class USSDMarshaller : public XMLMarshaller, public QpXMLDocGenerator
{

private:
  QPCHAR*		            		mUSSDXml;
  qp_xml_common_array_definition*   m_pCommonArray;
public:

  USSDMarshaller();
  ~USSDMarshaller();

  const QPCHAR* Marshall(XMLElement* pObj);
  QPVOID  Marshall(XMLObject* pObj, XMLFile* pfile){}

  QPBOOL AddUSSDDataElementToXml(qp_ussdData* ussdData);
  QPVOID AddLanguageElementToXml(xs_string 	language);
  QPVOID AddUSSDStringElementToXml(xs_string ussdString);
  QPVOID AddErrorCodeElementToXml(xs_Int errorCode);

};

class USSDContext : public XMLContext
{
public:
  XMLMarshaller* createMarshaller();
  XMLUnMarshaller* createUnMarshaller();
  XMLObjectFactory* createBinderFactory();
};

class USSDDataBinder : public XMLObjectFactory
{
public:

	USSDDataBinder(){};
	~USSDDataBinder(){};

  XMLObject* CreateRootElement();
  XMLElement* CreateLanguage();
  XMLElement* CreateUSSDstring();
  XMLElement* CreateErrorCode();
};

class USSDUnMarshaller : public XMLUnMarshaller
{

public:

	USSDUnMarshaller(){};
	~USSDUnMarshaller(){};

  XMLObject* UnMarshall(const QPCHAR* pstring);
  XMLObject* UnMarshall(XMLFile* pfile);
};

QPVOID TestUSSDMarshall(QPVOID);

#endif // end of __XMLUSSD_H__

