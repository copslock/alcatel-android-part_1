#ifndef __SIMSERVS_H__
#define __SIMSERVS_H__

#include<XMLSimservs.h>
#include<XMLList.h>
#include<XMLStack.h>
#include <XmlDecoderSchema.h>
#include <XMLCommonDef.h>
#include <XMLSchemaNameSpaceRetrieval.h>

class XMLSimservsDecoder : public XmlDecoderSchema
{

public:
  XMLSimservsDecoder();
  ~XMLSimservsDecoder();
  XmlCodeGenStack m_codeGenStack; 
  qp_Simservs *m_eSimservs;

  XMLElement*								m_pIntermediateElement;
  QPCHAR*									m_strDup;
  QPCHAR*									m_PassName;

  QPINT ProcessElement(QPCHAR* n_strElement,QPCHAR* n_strVal);
  QPINT ProcessAttribute(QPCHAR* n_strElement,QPCHAR* n_strAttr,QPCHAR* n_strVal);
  QPINT Validate() {return 0;};
  XMLElement* ProcessAnyElement(QPCHAR* n_strElement,QPCHAR* n_strVal){return QP_NULL;};
  QPINT ProcessState(QPCHAR *n_strElement) {return 0;}; 
  QPVOID* GetSessionData() { return m_eSimservs; };

  QPBOOL XMLRemoveNameSpaceTag(QPCHAR* n_strElement);

}; 

#endif