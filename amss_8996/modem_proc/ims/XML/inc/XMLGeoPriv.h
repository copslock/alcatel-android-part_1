/*****************************************************************************
Copyright (c) 2011-2012 Qualcomm Technologies, Inc. All rights reserved.
File  : XMLGeoLocation.h
Desc  :
Author: Vinaya Kumar D
Date  : 20.05.2015

Revision History
Rohit Initial Revision
===============================================================================
Date      |   Author's Name       |  BugID  |      Change Description
===============================================================================
20.05.2015   Vinaya Kumar D     ----      Defining the methods.
*****************************************************************************/
#ifndef __XMLGEOPRIV_H__
#define __XMLGEOPRIV_H__
#include <qpXMLDocGenerator.h>
#include <qplog.h>
#include "XMLCommonDef.h"
#include "XMLOmaPres.h"
#include "XMLCaps.h"

typedef enum
{
	XMLNS_GP_DEFAULT,
	XMLNS_CON,
	XMLNS_DM,
	XMLNS_GP,
	XMLNS_GML,
	XMLNS_GS,
	XMLNS_GBP,
	XMLNS_GP_MAX
}XML_NAMESPACES_GEOLOCATION;

class Presence :public XMLObject {
private: 
	QpSingleElementList*	mDevice;
	QpSingleElementList*	mAnyAttribute;
	xs_string				mEntity;

	Presence(const Presence&);
	Presence& operator=(const Presence&){ return *this; }

public:
	Presence();
	virtual ~Presence();

	QPBOOL			setDeviceValue(QpSingleElementList* Device) {
		mDevice = Device;
		return QP_TRUE;
	}

	QpSingleElementList*		getDeviceValue() {
		return mDevice;
	}

	QPBOOL			setAnyAttributeValue(QpSingleElementList* AnyAttribute) {
		mAnyAttribute = AnyAttribute;
		return QP_TRUE;
	}

	QpSingleElementList*		getAnyAttributeValue() {
		return mAnyAttribute;
	}

	QPBOOL			setEntityValue(xs_string Entity) {
		if (Entity != QP_NULL)
		{
			mEntity = qpDplStrdup(MEM_IMS_XML, Entity);
		}
		return QP_TRUE;
	}

	xs_string		getEntityValue() {
		return mEntity;
	}

	QPBOOL IsRootElement(){
		return QP_TRUE;
	}

	
	QPBOOL		 			ProcessUnMarshall(QPCHAR* n_Element, QPCHAR* n_Value, XMLElement* n_Object){ return QP_NULL;}
	QPBOOL					ProcessUnMarshallAttribute(QPCHAR* n_Element, QPCHAR* n_Value, XMLAttribute* n_Object){ return QP_NULL;}

	QPCHAR* ClassName();
	QPCHAR* ClassType(){ return (QPCHAR*)"XMLObject"; }
	QPCHAR* TagName(){ return (QPCHAR*)"presence"; }
	QPUINT8 Valiadate(){ return 0; }
};




class Note :public XMLElement {
private:
	xs_string mNoteValue;
	xs_string mXMLLang;
	Note(const Note&);
	Note& operator=(const Note&){ return *this; }

public:
	Note();
	virtual ~Note();

	QPBOOL			setNoteValue(xs_string NoteValue) {
		if (NoteValue != QP_NULL)
		{
			mNoteValue = qpDplStrdup(MEM_IMS_XML, NoteValue);
		}
		return QP_TRUE;
	}

	xs_string		getNoteValue() {
		return mNoteValue;
	}

	QPBOOL			setXMLLangValue(xs_string XMLLang) {
		if (XMLLang != QP_NULL)
		{
			mXMLLang = qpDplStrdup(MEM_IMS_XML, XMLLang);
		}
		return QP_TRUE;
	}

	xs_string		getXMLLangValue() {
		return mXMLLang;
	}

	
	QPBOOL		 			ProcessUnMarshall(QPCHAR* n_Element, QPCHAR* n_Value, XMLElement* n_Object){ return QP_NULL;}
	QPBOOL					ProcessUnMarshallAttribute(QPCHAR* n_Element, QPCHAR* n_Value, XMLAttribute* n_Object){ return QP_NULL;}

	QPCHAR* ClassName();
	QPCHAR* ClassType(){ return (QPCHAR*)"XMLElement"; }
	QPCHAR* TagName(){ return (QPCHAR*)"note"; }
	QPUINT8 Valiadate(){ return 0; }
};

class Notewell :public XMLElement {
private:
	xs_string mNoteWellValue;
	//xs_string mXMLLang;
	Notewell(const Notewell&);
	Notewell& operator=(const Notewell&){ return *this; }

public:
	Notewell();
	virtual ~Notewell();

	QPBOOL			setNoteWellValue(xs_string NoteWellValue) {
		if (NoteWellValue != QP_NULL)
		{
			mNoteWellValue = qpDplStrdup(MEM_IMS_XML, NoteWellValue);
		}
		return QP_TRUE;
	}

	xs_string		getNoteWellValue() {
		return mNoteWellValue;
	}

	/*QPBOOL			setXMLLangValue(xs_string XMLLang) {
		mXMLLang = XMLLang;
		return QP_TRUE;
	}

	xs_string		getXMLLangValue() {
		return mXMLLang;
	}*/

	
	QPBOOL		 			ProcessUnMarshall(QPCHAR* n_Element, QPCHAR* n_Value, XMLElement* n_Object){ return QP_NULL;}
	QPBOOL					ProcessUnMarshallAttribute(QPCHAR* n_Element, QPCHAR* n_Value, XMLAttribute* n_Object){ return QP_NULL;}

	QPCHAR* ClassName();
	QPCHAR* ClassType(){ return (QPCHAR*)"XMLElement"; }
	QPCHAR* TagName(){ return (QPCHAR*)"notewell"; }
	QPUINT8 Valiadate(){ return 0; }
};

class Radius :public XMLElement {
private:
	xs_string		mUOM;
	QPREAL64		mRadiusValue;
	Radius(const Radius&);
	Radius& operator=(const Radius&){ return *this; }

public:
	Radius();
	virtual ~Radius();

	QPBOOL			setUOMValue(xs_string UOM) {
		if (UOM != QP_NULL)
		{
			mUOM = qpDplStrdup(MEM_IMS_XML, UOM);
		}
		return QP_TRUE;
	}

	xs_string		getUOMValue() {
		return mUOM;
	}


	QPBOOL			setRadiusValue(QPREAL64 RadiusValue) {
		mRadiusValue = RadiusValue;
		return QP_TRUE;
	}

	QPREAL64		getRadiusValue() {
		return mRadiusValue;
	}

	
	QPBOOL		 			ProcessUnMarshall(QPCHAR* n_Element, QPCHAR* n_Value, XMLElement* n_Object){ return QP_NULL; }
	QPBOOL					ProcessUnMarshallAttribute(QPCHAR* n_Element, QPCHAR* n_Value, XMLAttribute* n_Object){ return QP_NULL; }

	QPCHAR* ClassName();
	QPCHAR* ClassType(){ return (QPCHAR*)"XMLElement"; }
	QPCHAR* TagName(){ return (QPCHAR*)"radius"; }
	QPUINT8 Valiadate(){ return 0; }
};

class Circle :public XMLElement {
private:
	xs_string		mSRSname;
	xs_string		mPos;
	Radius*			mRadius;
	Circle(const Circle&);
	Circle& operator=(const Circle&){ return *this; }

public:
	Circle();
	virtual ~Circle();

	QPBOOL			setSRSnameValue(xs_string SRSname) {
		if (SRSname != QP_NULL)
		{
			mSRSname = qpDplStrdup(MEM_IMS_XML, SRSname);
		}
		return QP_TRUE;
	}

	xs_string		getSRSnameValue() {
		return mSRSname;
	}


	QPBOOL			setPosValue(xs_string Pos) {
		if (Pos != QP_NULL)
		{
			mPos = qpDplStrdup(MEM_IMS_XML, Pos);
		}
		return QP_TRUE;
	}

	xs_string		getPosValue() {
		return mPos;
	}


	QPBOOL			setRadiusValue(Radius* Radius) {
		mRadius = Radius;
		return QP_TRUE;
	}

	Radius*		getRadiusValue() {
		return mRadius;
	}

	
	QPBOOL		 			ProcessUnMarshall(QPCHAR* n_Element, QPCHAR* n_Value, XMLElement* n_Object){ return QP_NULL;}
	QPBOOL					ProcessUnMarshallAttribute(QPCHAR* n_Element, QPCHAR* n_Value, XMLAttribute* n_Object){ return QP_NULL;}

	QPCHAR* ClassName();
	QPCHAR* ClassType(){ return (QPCHAR*)"XMLElement"; }
	QPCHAR* TagName(){ return (QPCHAR*)"Circle"; }
	QPUINT8 Valiadate(){ return 0; }
};

class Point :public XMLElement {
private:
	xs_string		mSRSname;
	xs_string		mPos;
	Point(const Point&);
	Point& operator=(const Point&){ return *this; }

public:
	Point();
	virtual ~Point();

	QPBOOL			setSRSnameValue(xs_string SRSname) {
		if (SRSname != QP_NULL)
		{
			mSRSname = qpDplStrdup(MEM_IMS_XML, SRSname);
		}
		return QP_TRUE;
	}

	xs_string		getSRSnameValue() {
		return mSRSname;
	}


	QPBOOL			setPosValue(xs_string Pos) {
		if (Pos != QP_NULL)
		{
			mPos = qpDplStrdup(MEM_IMS_XML, Pos);
		}
		return QP_TRUE;
	}

	xs_string		getPosValue() {
		return mPos;
	}

	
	QPBOOL		 			ProcessUnMarshall(QPCHAR* n_Element, QPCHAR* n_Value, XMLElement* n_Object){ return QP_NULL;}
	QPBOOL					ProcessUnMarshallAttribute(QPCHAR* n_Element, QPCHAR* n_Value, XMLAttribute* n_Object){ return QP_NULL;}

	QPCHAR* ClassName();
	QPCHAR* ClassType(){ return (QPCHAR*)"XMLElement"; }
	QPCHAR* TagName(){ return (QPCHAR*)"Point"; }
	QPUINT8 Valiadate(){ return 0; }
};

class Sphere :public XMLElement {
private:
	xs_string		mSRSname;
	xs_string		mPos;
	Radius*			mRadius;
	Sphere(const Sphere&);
	Sphere& operator=(const Sphere&){ return *this; }

public:
	Sphere();
	virtual ~Sphere();

	QPBOOL			setSRSnameValue(xs_string SRSname) {
		if (SRSname != QP_NULL)
		{
			mSRSname = qpDplStrdup(MEM_IMS_XML, SRSname);
		}
		return QP_TRUE;
	}

	xs_string		getSRSnameValue() {
		return mSRSname;
	}


	QPBOOL			setPosValue(xs_string Pos) {
		if (Pos != QP_NULL)
		{
			mPos = qpDplStrdup(MEM_IMS_XML, Pos);
		}
		return QP_TRUE;
	}

	xs_string		getPosValue() {
		return mPos;
	}


	QPBOOL			setRadiusValue(Radius* Radius) {
		mRadius = Radius;
		return QP_TRUE;
	}

	Radius*		getRadiusValue() {
		return mRadius;
	}

	
	QPBOOL		 			ProcessUnMarshall(QPCHAR* n_Element, QPCHAR* n_Value, XMLElement* n_Object){ return QP_NULL;}
	QPBOOL					ProcessUnMarshallAttribute(QPCHAR* n_Element, QPCHAR* n_Value, XMLAttribute* n_Object){ return QP_NULL;}

	QPCHAR* ClassName();
	QPCHAR* ClassType(){ return (QPCHAR*)"XMLElement"; }
	QPCHAR* TagName(){ return (QPCHAR*)"Sphere"; }
	QPUINT8 Valiadate(){ return 0; }
};



class Confidence :public XMLElement {
private:
	QPREAL64		mConfidenceValue;
	xs_string		mPDFValue;
	Confidence(const Confidence&);
	Confidence& operator=(const Confidence&){ return *this; }

public:
	Confidence();
	virtual ~Confidence();

	QPBOOL			setConfidenceValue(QPREAL64 Conf) {
			mConfidenceValue = Conf;
		return QP_TRUE;
	}

	QPREAL64		getConfidenceValue() {
		return mConfidenceValue;
	}


	QPBOOL			setPDFValue(xs_string PDFValue) {
		if (PDFValue != QP_NULL)
		{
			mPDFValue = qpDplStrdup(MEM_IMS_XML, PDFValue);
		}
		return QP_TRUE;
	}

	xs_string		getPDFValue() {
		return mPDFValue;
	}

	
	QPBOOL		 			ProcessUnMarshall(QPCHAR* n_Element, QPCHAR* n_Value, XMLElement* n_Object){ return QP_NULL;}
	QPBOOL					ProcessUnMarshallAttribute(QPCHAR* n_Element, QPCHAR* n_Value, XMLAttribute* n_Object){ return QP_NULL;}

	QPCHAR* ClassName();
	QPCHAR* ClassType(){ return (QPCHAR*)"XMLElement"; }
	QPCHAR* TagName(){ return (QPCHAR*)"confidence"; }
	QPUINT8 Valiadate(){ return 0; }
};




class LocInfoType :public XMLElement {
private:
	QpSingleElementList *	mAny;

	LocInfoType(const LocInfoType&);
	LocInfoType& operator=(const LocInfoType&){ return *this; }

public:
	LocInfoType();
	virtual ~LocInfoType();

	QPBOOL			setAnyValue(QpSingleElementList* Any) {
		mAny = Any;
		return QP_TRUE;
	}
	QpSingleElementList*		getAnyValue() {
		return mAny;
	}
	
	QPBOOL		 			ProcessUnMarshall(QPCHAR* n_Element, QPCHAR* n_Value, XMLElement* n_Object){ return QP_NULL; }
	QPBOOL					ProcessUnMarshallAttribute(QPCHAR* n_Element, QPCHAR* n_Value, XMLAttribute* n_Object){ return QP_NULL; }

	QPCHAR* ClassName();
	QPCHAR* ClassType(){ return (QPCHAR*)"XMLElement"; }
	QPCHAR* TagName(){ return (QPCHAR*)"location-info"; }
	QPUINT8 Valiadate(){ return 0; }
};

class LocMethod :public XMLElement {
private:
	xs_string mlocMethodValue;
	//xs_string mXmlLangValue;
	LocMethod(const LocMethod&);
	LocMethod& operator=(const LocMethod&){ return *this; }

public:
	LocMethod();
	virtual ~LocMethod();

	QPBOOL			setlocMethodValue(xs_string locMethodValue) {
		if (locMethodValue != QP_NULL)
		{
			mlocMethodValue = qpDplStrdup(MEM_IMS_XML, locMethodValue);
		}
		return QP_TRUE;
	}

	xs_string		getlocMethodValue() {
		return mlocMethodValue;
	}


	/*QPBOOL			setXmlLangValueValue(xs_string XmlLangValue) {
	mXmlLangValue = XmlLangValue;
	return QP_TRUE;
	}

	xs_string		getXmlLangValueValue() {
	return mXmlLangValue;
	}*/

	
	QPBOOL		 			ProcessUnMarshall(QPCHAR* n_Element, QPCHAR* n_Value, XMLElement* n_Object){ return QP_NULL; }
	QPBOOL					ProcessUnMarshallAttribute(QPCHAR* n_Element, QPCHAR* n_Value, XMLAttribute* n_Object){ return QP_NULL; }

	QPCHAR* ClassName();
	QPCHAR* ClassType(){ return (QPCHAR*)"XMLElement"; }
	QPCHAR* TagName(){ return (QPCHAR*)"method"; }
	QPUINT8 Valiadate(){ return 0; }
};

class LocPolicyType :public XMLElement {
private:
	xs_boolean				mRetransmission_allowed;
	xs_boolean              mIsRetransmissionAllowedPresent;
	xs_dateTime				mRetention_expiry;
	xs_anyURI				mExternal_ruleset;
	QpSingleElementList*	mAny;
	Notewell *				mNotewell;

	LocPolicyType(const LocPolicyType&);
	LocPolicyType& operator=(const LocPolicyType&){ return *this; }

public:
	LocPolicyType();
	virtual ~LocPolicyType();



	QPBOOL			setRetransmission_allowedValue(xs_boolean Retransmission_allowed) {
		mRetransmission_allowed = Retransmission_allowed;
		return QP_TRUE;
	}

	xs_boolean		getRetransmission_allowedValue() {
		return mRetransmission_allowed;
	}

	QPBOOL			setIsRetransmissionAllowedPresent(xs_boolean RetransmissionAllowedPresent) {
		mIsRetransmissionAllowedPresent = RetransmissionAllowedPresent;
		return QP_TRUE;
	}

	xs_boolean		getIsRetransmissionAllowedPresent() {
		return mIsRetransmissionAllowedPresent;
	}
	
	QPBOOL			setRetention_expiryValue(xs_dateTime Retention_expiry) {
		if (Retention_expiry != QP_NULL)
		{
			mRetention_expiry = qpDplStrdup(MEM_IMS_XML, Retention_expiry);
		}
		return QP_TRUE;
	}

	xs_dateTime		getRetention_expiryValue() {
		return mRetention_expiry;
	}


	QPBOOL			setExternal_rulesetValue(xs_anyURI External_ruleset) {
		if (External_ruleset != QP_NULL)
		{
			mExternal_ruleset = qpDplStrdup(MEM_IMS_XML, External_ruleset);
		}
		return QP_TRUE;
	}

	xs_anyURI		getExternal_rulesetValue() {
		return mExternal_ruleset;
	}


	QPBOOL			setAnyValue(QpSingleElementList* Any) {
		mAny = Any;
		return QP_TRUE;
	}

	QpSingleElementList*		getAnyValue() {
		return mAny;
	}


	QPBOOL			setNotewellValue(Notewell* Notewell) {
		mNotewell = Notewell;
		return QP_TRUE;
	}

	Notewell*		getNotewellValue() {
		return mNotewell;
	}

	
	QPBOOL		 			ProcessUnMarshall(QPCHAR* n_Element, QPCHAR* n_Value, XMLElement* n_Object){ return QP_NULL; }
	QPBOOL					ProcessUnMarshallAttribute(QPCHAR* n_Element, QPCHAR* n_Value, XMLAttribute* n_Object){ return QP_NULL; }

	QPCHAR* ClassName();
	QPCHAR* ClassType(){ return (QPCHAR*)"XMLElement"; }
	QPCHAR* TagName(){ return (QPCHAR*)"usage-rules"; }
	QPUINT8 Valiadate(){ return 0; }
};

class GeoPriv :public XMLElement {
private:
	LocInfoType*			mLocInfoType;
	LocPolicyType*			mUsageRules;
	LocMethod*				mLocMethod;
	QpSingleElementList*	mAny;
	GeoPriv(const GeoPriv&);
	GeoPriv& operator=(const GeoPriv&){ return *this; }

public:
	GeoPriv();
	virtual ~GeoPriv();

	QPBOOL			setLocInfoTypeValue(LocInfoType* LocInfoType) {
		mLocInfoType = LocInfoType;
		return QP_TRUE;
	}
	LocInfoType*		getLocInfoTypeValue() {
		return mLocInfoType;
	}

	QPBOOL			setUsageRulesValue(LocPolicyType* UsageRules) {
		mUsageRules = UsageRules;
		return QP_TRUE;
	}
	LocPolicyType*		getUsageRulesValue() {
		return mUsageRules;
	}

	QPBOOL			setLocMethodValue(LocMethod* LocMethod) {
		mLocMethod = LocMethod;
		return QP_TRUE;
	}
	LocMethod*		getLocMethodValue() {
		return mLocMethod;
	}

	QPBOOL			setAnyValue(QpSingleElementList* Any) {
		mAny = Any;
		return QP_TRUE;
	}
	QpSingleElementList*		getAnyValue() {
		return mAny;
	}

	
	QPBOOL		 			ProcessUnMarshall(QPCHAR* n_Element, QPCHAR* n_Value, XMLElement* n_Object){ return QP_NULL; }
	QPBOOL					ProcessUnMarshallAttribute(QPCHAR* n_Element, QPCHAR* n_Value, XMLAttribute* n_Object){ return QP_NULL; }

	QPCHAR* ClassName();
	QPCHAR* ClassType(){ return (QPCHAR*)"XMLElement"; }
	QPCHAR* TagName(){ return (QPCHAR*)"geopriv"; }
	QPUINT8 Valiadate(){ return 0; }
};

class Device :public XMLElement {
private:
	xs_ID					mId;
	GeoPriv*				mGeoPriv;
	xs_dateTime				mTimeStamp;
	xs_string				mDeviceID;
	QpSingleElementList*	mNote;

	Device(const Device&);
	Device& operator=(const Device&){ return *this; }

public:
	Device();
	virtual ~Device();

	QPBOOL			setIdValue(xs_ID Id) {
		if (Id != QP_NULL)
		{
			mId = qpDplStrdup(MEM_IMS_XML, Id);
		}
		return QP_TRUE;
	}

	xs_ID		getIdValue() {
		return mId;
	}

	QPBOOL			setGeoPrivValue(GeoPriv* GeoPriv) {
		mGeoPriv = GeoPriv;
		return QP_TRUE;
	}

	GeoPriv*		getGeoPrivValue() {
		return mGeoPriv;
	}

	QPBOOL			setTimeStampValue(xs_dateTime TimeStamp) {
		if (TimeStamp != QP_NULL)
		{
			mTimeStamp = qpDplStrdup(MEM_IMS_XML, TimeStamp);
		}
		return QP_TRUE;
	}

	xs_dateTime		getTimeStampValue() {
		return mTimeStamp;
	}

	QPBOOL			setDeviceIDValue(xs_string DeviceID) {
		if (DeviceID != QP_NULL)
		{
			mDeviceID = qpDplStrdup(MEM_IMS_XML, DeviceID);
		}
		return QP_TRUE;
	}

	xs_string		getDeviceIDValue() {
		return mDeviceID;
	}

	QPBOOL			setNoteValue(QpSingleElementList* Note) {
		mNote = Note;
		return QP_TRUE;
	}

	QpSingleElementList*		getNoteValue() {
		return mNote;
	}
	
	QPBOOL		 			ProcessUnMarshall(QPCHAR* n_Element, QPCHAR* n_Value, XMLElement* n_Object){ return QP_NULL; }
	QPBOOL					ProcessUnMarshallAttribute(QPCHAR* n_Element, QPCHAR* n_Value, XMLAttribute* n_Object){ return QP_NULL; }

	QPCHAR* ClassName();
	QPCHAR* ClassType(){ return (QPCHAR*)"XMLElement"; }
	QPCHAR* TagName(){ return (QPCHAR*)"device"; }
	QPUINT8 Valiadate(){ return 0; }
};

class GeoLocationMarshaller : public XMLMarshaller, QpXMLDocGenerator
{
private:
	QPCHAR*		            			mXml;
	qp_xml_common_array_definition*   m_pCommonArray;

	QPBOOL AddPresenceElementToXml(Presence* presence);
	QPBOOL AddDeviceElementToXml(Device* device);
	QPBOOL AddGeoPrivElementToXml(GeoPriv* geoPriv);
	QPBOOL AddLocInfoTypeElementToXml(LocInfoType* locInfoType);
	QPBOOL AddLocMethodElementToXml(LocMethod* LocMethod);
	QPBOOL AddLocPolicyTypeElementToXml(LocPolicyType* locPolicyType);
	QPBOOL AddNotewellElementToXml(Notewell* notewell);
	QPBOOL AddCircleElementToXml(Circle* circle);
	//QPBOOL AddPointElementToXml(Point* point);
	QPBOOL AddSphereElementToXml(Sphere* sphere);
	QPBOOL AddConfidenceElementToXml(Confidence* confidence);
	QPBOOL AddRadiusElementToXml(Radius* radius);
	QPBOOL AddNoteElementToXml(Note* note);
public:

	GeoLocationMarshaller();
	~GeoLocationMarshaller();

	const QPCHAR* Marshall(XMLElement* pObj);

	QPVOID  Marshall(XMLObject* pObj, XMLFile* pfile){}
};

class GeoLocationContext : public XMLContext
{
	static QPCHAR *defaultNameSpacePrefix[(XML_NAMESPACES_GEOLOCATION)XMLNS_GP_MAX];
	
public:
	static QPCHAR *NamespaceUri[(XML_NAMESPACES_GEOLOCATION)XMLNS_GP_MAX];
	QPCHAR* getNamespacePrefix(XML_NAMESPACES_GEOLOCATION ns);
	XMLMarshaller* createMarshaller();
	XMLUnMarshaller* createUnMarshaller();
	XMLObjectFactory* createBinderFactory();
};

class GeoLocationDataBinder : public XMLObjectFactory
{

public:

	GeoLocationDataBinder(){};
	~GeoLocationDataBinder(){};

	XMLObject*  CreateRootElement();
	XMLElement* CreateDeviceElement();
	XMLElement* CreateGeoPrivElement();
	XMLElement* CreateLocInfoTypeElement();
	XMLElement* CreateLocMethodElement();
	XMLElement* CreateLocPolicyTypeElement();
	XMLElement* CreateNotewellElement();
	XMLElement* CreateCircleElement();
	XMLElement* CreatePointElement();
	XMLElement* CreateSphereElement();
	XMLElement* CreateConfidenceElement();
	XMLElement* CreateRadiusElement();
	XMLElement* CreateNoteElement();
};

class GeoLocationUnMarshaller : public XMLUnMarshaller
{

public:

	GeoLocationUnMarshaller(){};
	~GeoLocationUnMarshaller(){};

	XMLObject* UnMarshall(const QPCHAR* pstring);
	XMLObject* UnMarshall(XMLFile* pfile);
};
#endif