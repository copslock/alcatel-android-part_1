/*****************************************************************************
  Copyright (c) 2011-2012 Qualcomm Technologies, Inc. All rights reserved.
  File  : XMLReginfo.h
  Desc  : 
  Author: Suraj Peddi
  Date  : 05.04.2013

  Revision History 
===============================================================================
  Date      |   Author's Name       |  BugID  |      Change Description
===============================================================================
 05.04.2013      Suraj Peddi           ----          Defining the methods.
-------------------------------------------------------------------------------
*****************************************************************************/

#ifndef __XMLREGINFO_H__
#define __XMLREGINFO_H__

#include <qpXMLDocGenerator.h>
#include <qplog.h>
#include "XMLCommonDef.h"
#include "XMLGruu.h"

#define XML_MAX_ARR_REGINFO_STR_LEN 32

typedef enum qp_reginfo_state_type_enum
{
  QP_REGINFO_STATE_TYPE_FULL,
  QP_REGINFO_STATE_TYPE_PARTIAL, 
  QP_REGINFO_STATE_TYPE_MAX
}e_qp_reginfo_state_type_enum;

static const char qp_reginfo_state_type[QP_REGINFO_STATE_TYPE_MAX][XML_MAX_ARR_REGINFO_STR_LEN] = 
{ 
  "full",
  "partial"
};

typedef enum qp_registration_state_type_enum
{
  QP_REGISTRATION_STATE_TYPE_INIT,
  QP_REGISTRATION_STATE_TYPE_ACTIVE, 
  QP_REGISTRATION_STATE_TYPE_TERMINATED,
  QP_REGISTRATION_STATE_TYPE_MAX
}e_qp_registration_state_type_enum;

static const char qp_registration_state_type[QP_REGISTRATION_STATE_TYPE_MAX][XML_MAX_ARR_REGINFO_STR_LEN] = 
{ 
  "init",
  "active",
  "terminated"
};

typedef enum qp_contact_state_type_enum
{
  QP_CONTACT_STATE_TYPE_ACTIVE, 
  QP_CONTACT_STATE_TYPE_TERMINATED,
  QP_CONTACT_STATE_TYPE_MAX
}e_qp_contact_state_type_enum;

static const char qp_contact_state_type[QP_CONTACT_STATE_TYPE_MAX][XML_MAX_ARR_REGINFO_STR_LEN] = 
{ 
  "active",
  "terminated"
};

typedef enum qp_contact_event_type_enum
{
  QP_CONTACT_EVENT_TYPE_REGISTERED, 
  QP_CONTACT_EVENT_TYPE_CREATED,
  QP_CONTACT_EVENT_TYPE_REFRESHED,
  QP_CONTACT_EVENT_TYPE_SHORTENED, 
  QP_CONTACT_EVENT_TYPE_EXPIRED,
  QP_CONTACT_EVENT_TYPE_DEACTIVATED,
  QP_CONTACT_EVENT_TYPE_PROBATION, 
  QP_CONTACT_EVENT_TYPE_UNREGISTERED,
  QP_CONTACT_EVENT_TYPE_REJECTED,
  QP_CONTACT_EVENT_TYPE_MAX
}e_qp_contact_event_type_enum;

static const char qp_contact_event_type[QP_CONTACT_EVENT_TYPE_MAX][XML_MAX_ARR_REGINFO_STR_LEN] = 
{ 
  "registered",
  "created",
  "refreshed",
  "shortened",
  "expired",
  "deactivated",
  "probation",
  "unregistered",
  "rejected"
};

class qp_reginfo : public XMLObject
{
  private:
  QpSingleElementList*               m_pRegistration;
  e_qp_reginfo_state_type_enum       m_eState;
  xs_nonNegativeInt                  m_iVersion;
  QpSingleElementList*               m_pAny;

  public:
	  qp_reginfo();
	  ~qp_reginfo();

  QPBOOL				            setRegistrationListValue(QpSingleElementList* pRegistration);
  QpSingleElementList*			getRegistrationListValue() {
	  return m_pRegistration;
  }

  QPBOOL				            setStateValue(e_qp_reginfo_state_type_enum state);
  e_qp_reginfo_state_type_enum getStateValue() {
	  return m_eState;
  }

  QPBOOL			  	          setVersionValue(xs_nonNegativeInt version);
  xs_nonNegativeInt			  	getVersionValue() {
	  return m_iVersion;
  }

  QPBOOL				            setAnyElementValue(QpSingleElementList* any);
  QpSingleElementList*      getAnyElementValue() {
	  return m_pAny;
  }

  QPBOOL				            ProcessUnMarshall(QPCHAR* n_strElement,QPCHAR* n_strVal,XMLElement* n_Object);
  QPBOOL                    ProcessUnMarshallAttribute(QPCHAR* n_strAttribute,QPCHAR* n_strVal, XMLAttribute* n_Object);
  
  QPBOOL IsRootElement();

  QPCHAR* ClassName();
  QPCHAR* ClassType(){ return (QPCHAR*)"XMLObject"; }
  QPCHAR* TagName(){ return (QPCHAR*)"reginfo"; }
  QPUINT8 Valiadate(){ return 0; }
};

class qp_registration : public XMLElement
{
  private:
  QpSingleElementList*               m_pContactList;
  xs_anyURI                          m_Aor;
  xs_string                          m_Id;
  e_qp_registration_state_type_enum  m_eState;
  QpSingleElementList*               m_pAny;

  public:
    qp_registration(){
      m_pContactList  = QP_NULL;
      m_Aor           = QP_NULL;
      m_Id            = QP_NULL;
      m_eState        = QP_REGISTRATION_STATE_TYPE_MAX;
      m_pAny          = QP_NULL;
    }
    ~qp_registration(){
      if (m_pContactList != QP_NULL)
      {
        qp_xml_common_methods :: DeleteElementList(m_pContactList);
		    m_pContactList = QP_NULL;
      }
      if(m_Aor != QP_NULL)
      {
        qpDplFree(MEM_IMS_XML,m_Aor);
        m_Aor = QP_NULL;
      }
      if(m_Id != QP_NULL)
      {
        qpDplFree(MEM_IMS_XML,m_Id);
        m_Id = QP_NULL;
      }
      m_eState   = QP_REGISTRATION_STATE_TYPE_MAX;
      if (m_pAny != QP_NULL)
      {
		    qp_xml_common_methods :: DeleteElementList(m_pAny);
		    m_pAny = QP_NULL;
      }
    }

  QPBOOL				            setContactListValue(QpSingleElementList* pContact) {
    m_pContactList = pContact;
    return QP_TRUE;
  }
  QpSingleElementList*			getContactListValue() {
	  return m_pContactList;
  }

  QPBOOL			  	          setAorValue(xs_anyURI aor) {
    if(aor != QP_NULL)
    {
      m_Aor = qpDplStrdup(MEM_IMS_XML,aor);
    }
    else
    {
      m_Aor = QP_NULL;
    }
    return QP_TRUE;
  }
  xs_anyURI			  	        getAorValue() {
	  return m_Aor;
  }

  QPBOOL			  	          setIdValue(xs_string id) {
    if(id != QP_NULL)
    {
      m_Id = qpDplStrdup(MEM_IMS_XML,id);
    }
    return QP_TRUE;
  }
  xs_string			  	        getIdValue() {
	  return m_Id;
  }

  QPBOOL				            setStateValue(e_qp_registration_state_type_enum state) {
    if (state < QP_REGISTRATION_STATE_TYPE_MAX)
      m_eState = state;
    else
  	  m_eState = QP_REGISTRATION_STATE_TYPE_MAX;
  
    return QP_TRUE;
  }
  e_qp_registration_state_type_enum getStateValue() {
	  return m_eState;
  }

  QPBOOL				            setAnyElementValue(QpSingleElementList* any) {
    m_pAny = any;
    return QP_TRUE;
  }
  QpSingleElementList*      getAnyElementValue() {
	  return m_pAny;
  }

  QPBOOL				            ProcessUnMarshall(QPCHAR* n_strElement,QPCHAR* n_strVal,XMLElement* n_Object);
  QPBOOL                    ProcessUnMarshallAttribute(QPCHAR* n_strAttribute,QPCHAR* n_strVal, XMLAttribute* n_Object);
  
  QPCHAR* ClassName(){ return (QPCHAR*)"qp_registration"; }
  QPCHAR* ClassType(){ return (QPCHAR*)"XMLElement"; }
  QPCHAR* TagName(){ return (QPCHAR*)"registration"; }
  QPUINT8 Valiadate(){ return 0; }
};

class qp_ns_reginfo_contact : public XMLElement
{
  private:
  xs_anyURI                          m_Uri;
  xs_string                          m_DisplayName;
  QpSingleElementList*               m_pUnknownParam;
  e_qp_contact_state_type_enum       m_eState;                // required attribute
  e_qp_contact_event_type_enum       m_eEvent;                // required attribute
  xs_unsignedLong                    m_DurationRegistered;
  xs_unsignedLong                    m_Expires;
  xs_unsignedLong                    m_RetryAfter;
  xs_string                          m_Id;                    // required attribute
  xs_string                          m_Q;
  xs_string                          m_Callid;
  xs_unsignedLong                    m_Cseq;
  QpSingleElementList*               m_pAny;

  public:
    qp_ns_reginfo_contact(){
      m_Uri                 = QP_NULL;
      m_DisplayName         = QP_NULL;
      m_pUnknownParam       = QP_NULL;
      m_eState              = QP_CONTACT_STATE_TYPE_MAX;
      m_eEvent              = QP_CONTACT_EVENT_TYPE_MAX;
      m_DurationRegistered  = 0;
      m_Expires             = 0;
      m_RetryAfter          = 0;
      m_Id                  = QP_NULL;                
      m_Q                   = QP_NULL;
      m_Callid              = QP_NULL;
      m_Cseq                = 0;
      m_pAny                = QP_NULL;
    }
    ~qp_ns_reginfo_contact(){
      if(m_Uri != QP_NULL)
      {
        qpDplFree(MEM_IMS_XML,m_Uri);
        m_Uri = QP_NULL;
      }
      if(m_DisplayName != QP_NULL)
      {
        qpDplFree(MEM_IMS_XML,m_DisplayName);
        m_DisplayName = QP_NULL;
      }
      if (m_pUnknownParam != QP_NULL)
      {
        qp_xml_common_methods :: DeleteElementList(m_pUnknownParam);
		    m_pUnknownParam = QP_NULL;
      }
      m_eState              = QP_CONTACT_STATE_TYPE_MAX;
      m_eEvent              = QP_CONTACT_EVENT_TYPE_MAX;
      m_DurationRegistered  = 0;
      m_Expires             = 0;
      m_RetryAfter          = 0;
      if(m_Id != QP_NULL)
      {
        qpDplFree(MEM_IMS_XML,m_Id);
        m_Id = QP_NULL;
      }
      if(m_Q != QP_NULL)
      {
        qpDplFree(MEM_IMS_XML,m_Q);
        m_Q = QP_NULL;
      }
      if(m_Callid != QP_NULL)
      {
        qpDplFree(MEM_IMS_XML,m_Callid);
        m_Callid = QP_NULL;
      }
      m_Cseq                = 0;
      if (m_pAny != QP_NULL)
      {
        qp_xml_common_methods :: DeleteElementList(m_pAny);
		    m_pAny = QP_NULL;
      }
    }

  QPBOOL			  	          setUriValue(xs_anyURI uri) {
    if(uri != QP_NULL)
    {
      m_Uri = qpDplStrdup(MEM_IMS_XML,uri);
    }
    else
    {
      m_Uri = QP_NULL;
    }
    return QP_TRUE;
  }
  xs_anyURI			  	        getUriValue() {
	  return m_Uri;
  }

  QPBOOL			  	          setDisplayNameValue(xs_string displayName){
    if(displayName != QP_NULL)
    {
      m_DisplayName = qpDplStrdup(MEM_IMS_XML,displayName);
    }
    else
    {
      m_DisplayName = QP_NULL;
    }
    return QP_TRUE;
  }
  xs_string			  	        getDisplayNameValue() {
	  return m_DisplayName;
  }

  QPBOOL				            setUnknownParamListValue(QpSingleElementList* pUnknownParam) {
    m_pUnknownParam = pUnknownParam;
    return QP_TRUE;
  }
  QpSingleElementList*			getUnknownParamListValue() {
	  return m_pUnknownParam;
  }

  QPBOOL				            setStateValue(e_qp_contact_state_type_enum state) {
    if (state < QP_CONTACT_STATE_TYPE_MAX)
      m_eState = state;
    else
  	  m_eState = QP_CONTACT_STATE_TYPE_MAX;
  
    return QP_TRUE;
  }
  e_qp_contact_state_type_enum getStateValue() {
	  return m_eState;
  }

  QPBOOL				            setEventValue(e_qp_contact_event_type_enum pEvent) {
    if (pEvent < QP_CONTACT_EVENT_TYPE_MAX)
      m_eEvent = pEvent;
    else
  	  m_eEvent = QP_CONTACT_EVENT_TYPE_MAX;
  
    return QP_TRUE;
  }
  e_qp_contact_event_type_enum getEventValue() {
	  return m_eEvent;
  }

  QPBOOL			  	          setDurationRegisteredValue(xs_unsignedLong durationregistered) {
    m_DurationRegistered = durationregistered;
    return QP_TRUE;
  }
  xs_unsignedLong			  	  getDurationRegisteredValue() {
	  return m_DurationRegistered;
  }

  QPBOOL			  	          setExpiresValue(xs_unsignedLong expires) {
    m_Expires = expires;
    return QP_TRUE;
  }
  xs_unsignedLong			  	  getExpiresValue() {
	  return m_Expires;
  }

  QPBOOL			  	          setRetryAfterValue(xs_unsignedLong retryafter) {
    m_RetryAfter = retryafter;
    return QP_TRUE;
  }
  xs_unsignedLong			  	  getRetryAfterValue() {
	  return m_RetryAfter;
  }

  QPBOOL			  	          setIdValue(xs_string id){
    if(id != QP_NULL)
    {
      m_Id = qpDplStrdup(MEM_IMS_XML,id);
    }
    else
    {
      m_Id = QP_NULL;
    }
    return QP_TRUE;
  }
  xs_string			  	        getIdValue() {
	  return m_Id;
  }

  QPBOOL			  	          setQValue(xs_string q){
    if(q != QP_NULL)
    {
      m_Q = qpDplStrdup(MEM_IMS_XML,q);
    }
    else
    {
      m_Q = QP_NULL;
    }
    return QP_TRUE;
  }
  xs_string			  	        getQValue() {
	  return m_Q;
  }

  QPBOOL			  	          setCallidValue(xs_string callid){
    if(callid != QP_NULL)
    {
      m_Callid = qpDplStrdup(MEM_IMS_XML,callid);
    }
    else
    {
      m_Callid = QP_NULL;
    }
    return QP_TRUE;
  }
  xs_string			  	        getCallidValue() {
	  return m_Callid;
  }

  QPBOOL			  	          setCseqValue(xs_unsignedLong cseq) {
    m_Cseq = cseq;
    return QP_TRUE;
  }
  xs_unsignedLong			  	  getCseqValue() {
	  return m_Cseq;
  }

  QPBOOL				            setAnyElementValue(QpSingleElementList* any){
    m_pAny = any;
    return QP_TRUE;
  }
  QpSingleElementList*      getAnyElementValue() {
	  return m_pAny;
  }

  QPBOOL				            ProcessUnMarshall(QPCHAR* n_strElement,QPCHAR* n_strVal,XMLElement* n_Object);
  QPBOOL                    ProcessUnMarshallAttribute(QPCHAR* n_strAttribute,QPCHAR* n_strVal, XMLAttribute* n_Object);
  
  QPCHAR* ClassName(){ return (QPCHAR*)"qp_ns_reginfo_contact"; }
  QPCHAR* ClassType(){ return (QPCHAR*)"XMLElement"; }
  QPCHAR* TagName(){ return (QPCHAR*)"contact"; }
  QPUINT8 Valiadate(){ return 0; }
};

class qp_unknown_param : public qp_string
{
  private:
  xs_string                          m_Name;

  public:
    qp_unknown_param(){
      m_Name = QP_NULL;
    }
    ~qp_unknown_param(){
      if(m_Name != QP_NULL)
      {
        qpDplFree(MEM_IMS_XML,m_Name);
        m_Name = QP_NULL;
      }
    }

  QPBOOL			  	          setNameValue(xs_string name){
    if(name != QP_NULL)
    {
      m_Name = qpDplStrdup(MEM_IMS_XML,name);
    }
    else
    {
      m_Name = QP_NULL;
    }
    return QP_TRUE;
  }
  xs_string			  	        getNameValue() {
	  return m_Name;
  }

  QPBOOL				            ProcessUnMarshall(QPCHAR* n_strElement,QPCHAR* n_strVal,XMLElement* n_Object);
  QPBOOL                    ProcessUnMarshallAttribute(QPCHAR* n_strAttribute,QPCHAR* n_strVal, XMLAttribute* n_Object);
  
  QPCHAR* ClassName(){ return (QPCHAR*)"qp_unknown_param"; }
  QPCHAR* ClassType(){ return (QPCHAR*)"XMLObject"; }
  QPCHAR* TagName(){ return (QPCHAR*)"unknown-param"; }
  QPUINT8 Valiadate(){ return 0; }
};

class ReginfoMarshaller : public XMLMarshaller, QpXMLDocGenerator
{

private:
  QPCHAR*		            		        mReginfoXml;
  qp_xml_common_array_definition*   m_pCommonArray;

public:
  ReginfoMarshaller();  
  ~ReginfoMarshaller();

  const QPCHAR* Marshall(XMLElement* pObj);

  QPVOID  Marshall(XMLObject* pObj, XMLFile* pfile){}

  QPBOOL AddReginfoElementToXml(qp_reginfo* pReginfo);
  QPBOOL AddRegistrationElementToXml(qp_registration* pRegistration);
  QPBOOL AddReginfoContactElementToXml(qp_ns_reginfo_contact* pContact);
  QPBOOL AddUnknownParamElementToXml(qp_unknown_param* pUnknownParam);
  QPBOOL AddPubGruuElementToXml(qp_pub_gruu* pubgruu);
  QPBOOL AddTempGruuElementToXml(qp_temp_gruu* tempgruu);
};

class ReginfoContext : public XMLContext
{
public:
  XMLMarshaller* createMarshaller();
  XMLUnMarshaller* createUnMarshaller();
  XMLObjectFactory* createBinderFactory();
};

class ReginfoDataBinder : public XMLObjectFactory
{
public:
	ReginfoDataBinder(){};  
	~ReginfoDataBinder(){};

  XMLObject* CreateRootElement();
  XMLElement* CreateRegistrationElement();
  XMLElement* CreateContactElement();
  XMLElement* CreateUnknownParamElement();
  XMLElement* CreatePubGruuElement();
  XMLElement* CreateTempGruuElement();
};

class ReginfoUnMarshaller : public XMLUnMarshaller
{
public:
	ReginfoUnMarshaller(){};  
	~ReginfoUnMarshaller(){};

  XMLObject* UnMarshall(const QPCHAR* pstring);
  XMLObject* UnMarshall(XMLFile* pfile);
};

#endif // end of __XMLREGINFO_H__
