/*****************************************************************************
  Copyright (c) 2011-2012 Qualcomm Technologies, Inc. All rights reserved.
  File  : XMLCommunicationDiversion.h
  Desc  : 
  Author: -----
  Date  : -----

  Revision History 
  Rohit Initial Revision
===============================================================================
  Date      |   Author's Name       |  BugID  |      Change Description
===============================================================================
 ---------   ----------------------    ----      Defining the methods.
-------------------------------------------------------------------------------
*****************************************************************************/

#ifndef __XMLOIR_H__
#define __XMLOIR_H__

#include <qpXMLDocGenerator.h>
#include <qplog.h>

#include "XMLCommonDef.h"
#include "XMLSimservsXcap.h"

typedef enum qp_Xml_OIR_DEFAULT_BEHAVIOUR
{
	OIR_DEFAULT_PRESENTATION_NOT_RESTRICTED,
	OIR_DEFAULT_PRESENTATION_RESTRICTED,
	OIR_DEFAULT_MAX
}e_qp_Xml_OIR_DEFAULT_BEHAVIOUR;

class qp_oir_DefaultBehaviour : public XMLElement
{
private:
  e_qp_Xml_OIR_DEFAULT_BEHAVIOUR					mDefaultBehaviourValue;
  QPBOOL											mDefaultBehaviourPresent;

  qp_oir_DefaultBehaviour(const qp_oir_DefaultBehaviour&){};
  qp_oir_DefaultBehaviour& operator=(const qp_oir_DefaultBehaviour&){return *this;}

public:
	qp_oir_DefaultBehaviour(){
	  mDefaultBehaviourValue = OIR_DEFAULT_PRESENTATION_RESTRICTED;
	  mDefaultBehaviourPresent = QP_FALSE;
	}
	virtual ~qp_oir_DefaultBehaviour(){
		mDefaultBehaviourValue = OIR_DEFAULT_PRESENTATION_RESTRICTED;
		mDefaultBehaviourPresent = QP_FALSE;
	}

	QPBOOL									setDefaultBehaviourValue(e_qp_Xml_OIR_DEFAULT_BEHAVIOUR default_behaviour);
  e_qp_Xml_OIR_DEFAULT_BEHAVIOUR			getDefaultBehaviourValue() {
	  return mDefaultBehaviourValue;
  }

  QPBOOL									setIsDefaultBehaviourElementPresent(QPBOOL	present);
  QPBOOL									getIsDefaultBehaviourElementPresent(){return mDefaultBehaviourPresent;};

  QPBOOL		 			ProcessUnMarshall(QPCHAR* n_Element,QPCHAR* n_Value, XMLElement* n_Object){return QP_TRUE;};
  QPBOOL					ProcessUnMarshallAttribute(QPCHAR* n_Element,QPCHAR* n_Value,XMLAttribute* n_Object){return QP_TRUE;};
  QPCHAR* ClassName(){ return (QPCHAR*)"qp_oir_DefaultBehaviour";};
  QPCHAR* ClassType(){ return (QPCHAR*)"XMLObject"; }
  QPCHAR* TagName(){ return (QPCHAR*)"default-behaviour"; }
  QPUINT8 Valiadate(){ return 0; }

};

class qp_oir : public qp_ns_xcap_simservType
{
private:
  XML_NAMESPACES_CB_CD								uriPrefix;
  qp_oir_DefaultBehaviour*								mDefaultBehaviour;

  qp_oir(const qp_oir&){};
  qp_oir& operator=(const qp_oir&){return *this;}

public:
  qp_oir();
  ~qp_oir();

  QPBOOL									setDefaultBehaviour(qp_oir_DefaultBehaviour* default_behaviuor);
  qp_oir_DefaultBehaviour*						getDefaultBehaviour() {
	  return mDefaultBehaviour;
  }

  XML_NAMESPACES_CB_CD		getUriPrefix()
  {
	  return uriPrefix;
  }
  QPBOOL					toggleUriPrefix();

  QPBOOL		 			ProcessUnMarshall(QPCHAR* n_Element,QPCHAR* n_Value, XMLElement* n_Object);
  QPBOOL					ProcessUnMarshallAttribute(QPCHAR* n_Element,QPCHAR* n_Value,XMLAttribute* n_Object);
  QPBOOL IsRootElement(){
	  return QP_TRUE;
  }
  QPCHAR* ClassName();
  QPCHAR* ClassType(){ return (QPCHAR*)"XMLObject"; }
  QPCHAR* TagName(){ return (QPCHAR*)"originating-identity-presentation-restriction"; }
  QPUINT8 Valiadate(){ return 0; }

};

class OIRMarshaller : public XMLMarshaller, QpXMLDocGenerator
{

private:
  QPCHAR*		            		mOIRXml;
  qp_xml_common_array_definition*   m_pCommonArray;
public:
  OIRMarshaller();  
  ~OIRMarshaller();

  const QPCHAR* Marshall(XMLElement* pObj);

  QPVOID  Marshall(XMLObject* pObj, XMLFile* pfile){}

  QPBOOL AddOIRElementToXml(qp_oir* 	oir);
  QPBOOL AddOIRDefaultBehaviourElementToXml(qp_oir_DefaultBehaviour* 	default_behaviour);
};

class OIRDataBinder : public XMLObjectFactory
{
public:
	OIRDataBinder(){};
	~OIRDataBinder(){};

  XMLObject* CreateRootElement();
  XMLElement* CreateOIRDefaultBehaviourElement();
};

class OIRContext : public XMLContext
{
private:
	OIRContext(const OIRContext&){};
    OIRContext& operator=(const OIRContext&){return *this;}
	
	static QPCHAR *initNameSpacePrefix[(XML_NAMESPACES_CB_CD)XMLNS_MAX];
	static QPCHAR *NamespaceUri[3];
	static QPUINT uriListSize;
	QPCHAR** NSPrefix;
	QPVOID initNamespacePrefix();
public:
	OIRContext();
	~OIRContext();

	using XMLContext::setNamespacePrefix;

  XMLMarshaller* createMarshaller();
  XMLUnMarshaller* createUnMarshaller();
  XMLObjectFactory* createBinderFactory();
  QPBOOL setNamespacePrefix(XML_NAMESPACES_CB_CD ns, QPCHAR* prefix);
  QPBOOL setNamespacePrefix(QPUINT ns, QPCHAR* prefix);
  QPCHAR* getNamespacePrefix(XML_NAMESPACES_CB_CD ns);
  QPCHAR* getNamespaceUri(QPUINT iIndex);
  QPUINT  getUriListSize();

};

class OIRUnMarshaller : public XMLUnMarshaller
{

public:
	OIRUnMarshaller();
	~OIRUnMarshaller();

  XMLObject* UnMarshall(const QPCHAR* pstring);
  XMLObject* UnMarshall(XMLFile* pfile);
};

#endif 
