#include "ims_variation.h"
#include "customer.h"
#include "comdef.h"
#include <qpdpl.h>
#include "qpPlatformConfig.h"
#include "qplog.h"

#ifdef __cplusplus
extern "C"
{
#endif

extern QPC_GLOBAL_DATA *rtptestGlobalData;
void rtp_test_main(void);


typedef struct
{
    QPUINT8    diag_num;
    QPUINT8    subsystem;
	QPUINT8    subcommand1;
    QPUINT8    subcommand2;
    QPINT      data;
} test_rtp_diag_packet;


#ifdef __cplusplus
}
#endif
