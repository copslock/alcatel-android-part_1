#ifndef _IMS_VT_MEDIA_TEST_FRAMEWORK_CB
#define _IMS_VT_MEDIA_TEST_FRAMEWORK_CB

#include "ims_variation.h"
#include "customer.h"

#ifdef FEATURE_VOIP

/*===========================================================================

INCLUDE FILES FOR MODULE

===========================================================================*/
#include "comdef.h"

#include "qvp_rtp_api.h"


void qvp_rtp_get_qipcall_cb(qvp_rtp_app_call_backs_type *call_backs);

/*===========================================================================
FUNCTION test_rtp_register_cb

DESCRIPTION
This function handles RTP Registration callback status.
DEPENDENCIES
None

RETURN VALUE
None

SIDE EFFECTS
None
===========================================================================*/
extern QPVOID test_rtp_register_cb
(
 qvp_rtp_status_type      status,
 qvp_rtp_app_id_type      app_id
 );

/*===========================================================================
FUNCTION test_rtp_reset_cb

DESCRIPTION
This function handles RTP Reset callback status.
DEPENDENCIES
None

RETURN VALUE
None

SIDE EFFECTS
None
===========================================================================*/
extern QPVOID test_rtp_reset_cb
(
 qvp_rtp_app_id_type      app_id,      /* RTP Application ID*/
 qvp_rtp_app_data_type    app_data,
 qvp_rtp_status_type      status,
 qvp_rtp_stream_id_type   stream
 );

/*===========================================================================
FUNCTION test_rtp_stream_open_cb

DESCRIPTION
This function Handles the RTP Stream Open callback.

DEPENDENCIES
None

RETURN VALUE
None

SIDE EFFECTS
None
===========================================================================*/
extern QPVOID test_rtp_stream_open_cb
(
 qvp_rtp_app_id_type    app_id,      /* RTP Application ID*/
 qvp_rtp_app_data_type  app_data,    /* app handle into the request */
 qvp_rtp_status_type    status,      /* status of requested operation */
 qvp_rtp_stream_id_type stream      /* identifier of the stream being
                                    * opened
                                    */
                                    );

/*===========================================================================
FUNCTION test_rtp_stream_close_cb

DESCRIPTION
This function handles RTP Stream close callback.

DEPENDENCIES
None

RETURN VALUE
None

SIDE EFFECTS
None
===========================================================================*/
extern QPVOID test_rtp_stream_close_cb
(
 qvp_rtp_app_id_type      app_id,      /* RTP Application ID*/
 qvp_rtp_app_data_type    app_data,  /* app handle into the request */
 qvp_rtp_status_type      status,    /* satatus of operation        */
 qvp_rtp_stream_id_type   stream     /* stream which got closed */
 );
/*===========================================================================
FUNCTION test_rtp_flow_cb

DESCRIPTION
This function handles the flow control feedback from RTP.

DEPENDENCIES
None

RETURN VALUE
None

SIDE EFFECTS
None
===========================================================================*/
//extern QPVOID test_rtp_flow_cb
//(
//qvp_rtp_app_data_type    app_data,  /* app handle into the request */
//qvp_rtp_feed_back_type   *feedback  /* flow feed back to the app */
//);


/*===========================================================================
FUNCTION test_rtp_stream_config_cb

DESCRIPTION
This function handles the callback status of RTP configuration.

DEPENDENCIES
None

RETURN VALUE
None

SIDE EFFECTS
None
===========================================================================*/
extern QPVOID test_rtp_stream_config_cb
(
 qvp_rtp_app_id_type      app_id,      /* RTP Application ID*/
 qvp_rtp_app_data_type  app_data,    /* app handle into the request */
 qvp_rtp_status_type    status,      /* status of requested operation */
 qvp_rtp_stream_id_type stream       /* identifier of the stream being
                                     * opened
                                     */
                                     );
/*===========================================================================
FUNCTION test_rtp_stream_timeout_cb

DESCRIPTION
This function handles RTP Stream timeout callback.

DEPENDENCIES
None

RETURN VALUE
None

SIDE EFFECTS
None
===========================================================================*/
extern QPVOID test_rtp_stream_timeout_cb
(
 qvp_rtp_app_id_type      app_id,      /* RTP Application ID*/
 qvp_rtp_app_data_type    app_data    /* app handle into the request */
 );

/*===========================================================================
FUNCTION test_rtcp_stream_timeout_cb

DESCRIPTION
This function handles RTCP Stream timeout callback.

DEPENDENCIES
None

RETURN VALUE
None

SIDE EFFECTS
None
===========================================================================*/
extern QPVOID test_rtcp_stream_timeout_cb
(
  qvp_rtp_app_id_type      app_id,    /* RTP Application ID*/
 qvp_rtp_app_data_type    app_data  /* app handle into the request */
                                      );

/*===========================================================================
FUNCTION test_rtp_config_session_cb

DESCRIPTION
This function handles RTP session configure callback.

DEPENDENCIES
None

RETURN VALUE
None

SIDE EFFECTS
None
===========================================================================*/
QPVOID test_rtp_config_session_cb
(
 qvp_rtp_app_id_type    app_id,
 qvp_rtp_status_type    status 
);

/*===========================================================================
FUNCTION test_rtp_media_evt_cb

DESCRIPTION
This function handles media related event callback.

DEPENDENCIES
None

RETURN VALUE
None

SIDE EFFECTS
None
===========================================================================*/
extern QPVOID test_rtp_media_evt_cb
(
 qvp_rtp_app_id_type      app_id,    /* RTP Application ID*/
 qvp_rtp_stream_id_type   stream_id,    /* stream id to which the rtp info belongs */
 qvp_rtp_media_events     evnt,      /* media events */
 qvp_rtp_status_type      status,     /* status */
 qvp_rtp_stream_direction direction     /* direction */
 );

/*===========================================================================
FUNCTION test_generate_nal_cb

DESCRIPTION
This function handles generate nal related callback.

DEPENDENCIES
None

RETURN VALUE
None

SIDE EFFECTS
None
===========================================================================*/
QPVOID test_generate_nal_cb
(
  QPCHAR*                    evnt_data,
  qvp_rtp_status_type        status     /* status */,
  QPVOID                    *puserdata

);

/*===========================================================================
FUNCTION  test_generate_idr_cb

DESCRIPTION
This function handles generate iframe from peer callback

DEPENDENCIES
None

RETURN VALUE
None

SIDE EFFECTS
None
===========================================================================*/
QPVOID test_generate_idr_cb
 (
  qvp_rtp_app_id_type      app_id,
  qvp_rtp_stream_id_type   stream_id 
 );
#endif /* FEATURE_VOIP */

#endif
