/*=*====*====*====*====*====*====*===*====*====*====*====*====*====*====*====*


                         QVP_RTP_G729_PROFILE . C

GENERAL DESCRIPTION

  This file contains the implementation of g729 profile. g729 profile acts 
  as conduite inside RTP layer. The RFC which is based on is RFC3551.

EXTERNALIZED FUNCTIONS
  None.


INITIALIZATION AND SEQUENCING REQUIREMENTS

  Need to init and configure the profile before it becomes usable.


  Copyright (c) 3004 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
  QUALCOMM Proprietary.  Export of this technology or software is
  regulated by the U.S. Government. Diversion contrary to U.S. law prohibited.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*=*/

/*===========================================================================

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/ims/ps_media/rtp_profile/src/qvp_rtp_g729_profile.c#1 $ $DateTime: 2016/03/28 23:03:22 $ $Author: mplcsds1 $

                            EDIT HISTORY FOR FILE


when        who    what, where, why
--------    ---    ----------------------------------------------------------
14/03/10    sha    supporting bundling 

26/06/09    sha    Changed the validate function to check pyld and Chdir only when valid
                   Turning off silent suppr in init
03/04/09    srk    Initial Creation.
===========================================================================*/
#include "ims_variation.h"
#include "customer.h"
#ifdef FEATURE_QVPHONE_RTP
#ifdef  RTP_FILE_NUMBER 
  #undef RTP_FILE_NUMBER 
#endif 
#define RTP_FILE_NUMBER 27

/*===========================================================================
                     INCLUDE FILES FOR MODULE
===========================================================================*/
#include <comdef.h>               /* common target/c-sim defines */
#include <string.h>               /* memory routines */
#include <stdlib.h>               /* for malloc */
#include "qvp_rtp_api.h"          /* return type propagation */
#include "qvp_rtp_profile.h"      /* profile template data type */
#include "qvp_rtp_msg.h"
#include "qvp_rtp_log.h"
#include "qvp_rtp_g729_profile.h" /* profile template data type */
#include <bit.h>                  /* for bit parsing/manipulation */


/*===========================================================================
                     LOCAL STATIC FUNCTIONS 
===========================================================================*/


LOCAL qvp_rtp_status_type qvp_rtp_g729_profile_init
(
  qvp_rtp_profile_config_type *config
);

LOCAL qvp_rtp_status_type qvp_rtp_g729_profile_open
(
  qvp_rtp_profile_hdl_type     *hdl,         /* handle which will get 
                                              * populated with a new profile
                                              * stream handle
                                              */
  qvp_rtp_profile_usr_hdl_type  usr_hdl     /* user handle for any cb */
);

LOCAL qvp_rtp_status_type qvp_rtp_g729_profile_read_deflt_config
(
  
  qvp_rtp_payload_type        payld, /* not used */ 
  qvp_rtp_payload_config_type *payld_config 
);

LOCAL qvp_rtp_status_type qvp_rtp_g729_profile_validate_config_rx
(
  qvp_rtp_payload_config_type  *payld_config,
  qvp_rtp_payload_config_type  *counter_config 
);

LOCAL qvp_rtp_status_type qvp_rtp_g729_profile_validate_config_tx
(
  qvp_rtp_payload_config_type  *payld_config,
  qvp_rtp_payload_config_type  *counter_config 
);

LOCAL qvp_rtp_status_type qvp_rtp_g729_profile_configure
(
  qvp_rtp_profile_hdl_type      hdl,     
  qvp_rtp_payload_config_type   *payld_config 
);

LOCAL qvp_rtp_status_type qvp_rtp_g729_profile_send
(
  qvp_rtp_profile_hdl_type     hdl,          /* handle created while open */
  qvp_rtp_profile_usr_hdl_type usr_hdl,      /* user handle for tx cb */
  qvp_rtp_buf_type             *pkt          /* packet to be formatted 
                                              * through the profile
                                              */
);

LOCAL qvp_rtp_status_type qvp_rtp_g729_profile_recv
(

  qvp_rtp_profile_hdl_type     hdl,          /* handle created while open */
  qvp_rtp_profile_usr_hdl_type usr_hdl,      /* user handle for rx cb */
  qvp_rtp_buf_type             *pkt          /* packet parsed and removed
                                              * header 
                                              */

);

LOCAL qvp_rtp_status_type qvp_rtp_g729_profile_close
(

  qvp_rtp_profile_hdl_type     hdl           /* handle the profile */

);

LOCAL void qvp_rtp_g729_profile_shutdown( void );

/*--------------------------------------------------------------------------
      FUNCTIONS APART FROM PROFILE TEMPLATE 
--------------------------------------------------------------------------*/
LOCAL void qvp_rtp_g729_reset_tx_ctx
( 
  qvp_rtp_g729_ctx_type *stream 
);

LOCAL void qvp_rtp_g729_reset_stream
( 
  qvp_rtp_g729_ctx_type *stream 
);

LOCAL qvp_rtp_status_type qvp_rtp_g729_profile_copy_config
(
  qvp_rtp_payload_config_type *payld_config,
  qvp_rtp_g729_config_type    *g729_config
);

LOCAL qvp_rtp_status_type qvp_rtp_g729_profile_config_tx_param
(
  qvp_rtp_g729_ctx_type       *stream,
  qvp_rtp_payload_config_type *payld_config 
);

LOCAL qvp_rtp_status_type qvp_rtp_g729_profile_config_rx_param
(
  qvp_rtp_g729_ctx_type       *stream,
  qvp_rtp_payload_config_type *payld_config 
);

LOCAL int16  qvp_rtp_g729_find_num_frames 
(
  qvp_rtp_buf_type *buf
);

/*--------------------------------------------------------------------------
    Table of accessors for the profile. This need to be populated in
    the table entry at qvp_rtp_profole.c. All these functions will 
    automatically link to the RTP stack by doing so.
--------------------------------------------------------------------------*/
qvp_rtp_profile_type qvp_rtp_g729_profile_table = 
{
  qvp_rtp_g729_profile_init,      /* LOACAL initialization routine  */
  qvp_rtp_g729_profile_open,      /* LOCAL function to open channel */
  qvp_rtp_g729_profile_send,      /* LOCAL function to send a pkt   */
  qvp_rtp_g729_profile_recv,      /* LOCAL function to rx a nw pkt  */
  qvp_rtp_g729_profile_close,     /* LOCAL function to close channel*/
  qvp_rtp_g729_profile_read_deflt_config,
  NULL,
  qvp_rtp_g729_profile_validate_config_rx,
  qvp_rtp_g729_profile_validate_config_tx,
  qvp_rtp_g729_profile_configure,
  qvp_rtp_g729_profile_shutdown   /* LOCAL function to shut down */

};

/*===========================================================================
                     LOCAL STATIC DATA 
===========================================================================*/

LOCAL boolean g729_initialized = FALSE;

/*--------------------------------------------------------------------------
     Stores the configuration requested by the app.                        
--------------------------------------------------------------------------*/
LOCAL  qvp_rtp_profile_config_type g729_profile_config;

/*--------------------------------------------------------------------------
        Stream context array.  Responsible for each bidirectional 
        connections
--------------------------------------------------------------------------*/
LOCAL qvp_rtp_g729_ctx_type *qvp_rtp_g729_array = NULL;

/*--------------------------------------------------------------------------
     Configuration pertaining payload formatting for g729 
--------------------------------------------------------------------------*/
LOCAL qvp_rtp_g729_config_type g729_stream_config;


/*===========================================================================

FUNCTION QVP_RTP_g729_PROFILE_INIT 


DESCRIPTION
  Initializes data structures and resources used by the g729 profile.

DEPENDENCIES
  None

ARGUMENTS IN
  config - pointer to profile configuration requested.

RETURN VALUE
  QVP_RTP_SUCCESS  - if we could initialize the profile. or an error code


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_g729_profile_init
(
  qvp_rtp_profile_config_type *config
)
{
/*------------------------------------------------------------------------*/


  if( g729_initialized )
  {
    return( QVP_RTP_SUCCESS );
  }
   
  /*------------------------------------------------------------------------
    Init if we get a valid config  and appropriate call backs 
    for the basic operation of the profile.
  ------------------------------------------------------------------------*/
  if( config && config->rx_cb && config->tx_cb )
  {
    qvp_rtp_memscpy( &g729_profile_config, sizeof( g729_profile_config ), config, sizeof( g729_profile_config ) );
  }
  else
  {
    return( QVP_RTP_WRONG_PARAM );
  }

  /*------------------------------------------------------------------------
    Allocate the number of streams first
  ------------------------------------------------------------------------*/
  qvp_rtp_g729_array  = qvp_rtp_malloc( config->num_streams * 
                                sizeof( qvp_rtp_g729_ctx_type ) ); 

  /*------------------------------------------------------------------------
      See if we could get the memory
  ------------------------------------------------------------------------*/
  if( !qvp_rtp_g729_array )
  {
    return( QVP_RTP_ERR_FATAL );
  }
  else
  {
    memset( qvp_rtp_g729_array, 0, config->num_streams * 
                    sizeof( qvp_rtp_g729_ctx_type ) );
    
    /*----------------------------------------------------------------------
        Set up the default configuration
    ----------------------------------------------------------------------*/
    g729_stream_config.rx_ptime = QVP_RTP_G729_DFLT_PKT_INTERVAL;
    g729_stream_config.rx_max_ptime = QVP_RTP_G729_DFLT_BUNDLE_SIZE *
                                      QVP_RTP_G729_DFLT_PKT_INTERVAL;
    
    /*----------------------------------------------------------------------
      We do not support interleaving yet...
      So easy
    ----------------------------------------------------------------------*/
    g729_stream_config.tx_ptime = QVP_RTP_G729_DFLT_PKT_INTERVAL;
    g729_stream_config.tx_max_ptime = QVP_RTP_G729_DFLT_BUNDLE_SIZE *
                                      QVP_RTP_G729_DFLT_PKT_INTERVAL;
    g729_stream_config.tx_bundle_size = QVP_RTP_G729_DFLT_BUNDLE_SIZE;

  }

  /*------------------------------------------------------------------------
    Flag initialization
  ------------------------------------------------------------------------*/
  g729_initialized  = TRUE;
  
  

  return( QVP_RTP_SUCCESS );
 
}/* end of function qvp_rtp_g729_profile_init */

/*===========================================================================

FUNCTION  QVP_RTP_g729_PROFILE_SEND


DESCRIPTION
  request to send specified buffer through this profile.

DEPENDENCIES
  None

ARGUMENTS IN
  hdl     - handle into the profile. 
  usr_hdl - handle to use when we call send function using the registered
            tx callback
  pkt     - packet of data to be sent.

RETURN VALUE
  QVP_RTP_SUCCESS  - if we could send data. or an error code


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_g729_profile_send
(
  qvp_rtp_profile_hdl_type     hdl,          /* handle created while open */
  qvp_rtp_profile_usr_hdl_type usr_hdl,      /* user handle for tx cb */
  qvp_rtp_buf_type              *pkt          /* packet to be formatted 
                                              * through the profile
                                              */
)
{
  qvp_rtp_g729_ctx_type *stream = ( qvp_rtp_g729_ctx_type *) hdl;
  uint16 offset = 0;
  qvp_rtp_status_type status = QVP_RTP_SUCCESS;
  qvp_rtp_buf_type       *aud_buf;
  uint8                  *dataptr;
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
                    Are we sane here ..?
  ------------------------------------------------------------------------*/
  if( !g729_initialized || !stream || !stream->valid || 
      !g729_profile_config.tx_cb  || !pkt ) 
  {
    
    /*----------------------------------------------------------------------
      Free the packet before we bail out
    ----------------------------------------------------------------------*/
    if( pkt )
    {
      qvp_rtp_free_buf( pkt );
    }
    return( QVP_RTP_ERR_FATAL );
  }

 
  switch( pkt->frm_info.info.aud_info.frame_type )
  {
    /*----------------------------------------------------------------------
      handle comfort noise frame 
    ----------------------------------------------------------------------*/
    case QVP_RTP_AUDIO_SID:
    {
      /*--------------------------------------------------------------------
        If bundle size is greater than zero than we can assume that we are waiting 
        on for a another speech frame to ship.Instead we have a SID in between ,
        this would mean that the speech frame we were waiting for will only be 
        delived after another 20 miliseconds.
        Since we cannot wait for that long a period send out the speech packet too 
      --------------------------------------------------------------------*/
      if(stream->tx_ctx.frame_cnt > 0)
      {
        QVP_RTP_MSG_LOW_0( "SID packet ,so send the remaining speech out");
        aud_buf = qvp_rtp_alloc_buf_by_len( QVP_RTP_HEAD_ROOM + (QVP_RTP_G729_DFLT_SPEECH_FRAME_SIZE * stream->tx_ctx.frame_cnt));
        if ( !aud_buf )
        {
          
          QVP_RTP_ERR( " Could not get an audio buffer \r\n", 0, 0, 0 );
          
          /*------------------------------------------------------------------
          Free if needed
          ------------------------------------------------------------------*/
          if ( pkt->need_tofree )
          {
            qvp_rtp_free_buf( pkt );
          }
          return( QVP_RTP_ERR_FATAL );

        }


        aud_buf->len  = QVP_RTP_G729_DFLT_SPEECH_FRAME_SIZE * stream->tx_ctx.frame_cnt;
        
        //copy the data after head room on the packet
        aud_buf->head_room = QVP_RTP_HEAD_ROOM;
        qvp_rtp_memscpy(aud_buf->data + aud_buf->head_room,aud_buf->len,stream->tx_ctx.frame_buffer,aud_buf->len);
        
        //setting other parameters
        aud_buf->codec_hdr.valid = FALSE;
        aud_buf->marker_bit = FALSE;
        aud_buf->tstamp = stream->tx_ctx.first_time_stamp;

        //Set the payload of g729 
        aud_buf->rtp_pyld_type    = QVP_RTP_G729_DFLT_PAYLOAD_TYPE;
        aud_buf->need_tofree = TRUE;
        
        //send out the speech packet
        status = g729_profile_config.tx_cb(aud_buf, usr_hdl);
        if(status != QVP_RTP_SUCCESS)
        {
          QVP_RTP_ERR( "Sending speech packet failed",0, 0, 0 );
        }
        
        //Reset bundle size and temp memory
        stream->tx_ctx.frame_cnt = 0;
        memset(stream->tx_ctx.frame_buffer,0,QVP_RTP_G729_DFLT_SPEECH_FRAME_SIZE * stream->stream_config.tx_bundle_size);
      }

      /*--------------------------------------------------------------------
        Check if voice activiity detection is enabled 
        then pack the comfort noise payload number. 
      --------------------------------------------------------------------*/
      if( !stream->tx_ctx.vad_on )
      {
        QVP_RTP_ERR( " VAD is not on, drop this CN frame %d \r\n",
          pkt->tstamp, 0, 0);
        qvp_rtp_free_buf( pkt );
        return( QVP_RTP_NOT_SUPPORTED );
      }


      /*--------------------------------------------------------------------
         Set the marker bit to zero always for CN packets
      ----------------------------------------------------------------------*/
      pkt->marker_bit = 0;
      b_packb( pkt->marker_bit, pkt->data + pkt->head_room , 8, 1 );
      offset += 1;

      /*--------------------------------------------------------------------
         Pack the new payload number            
      --------------------------------------------------------------------*/
      pkt->rtp_pyld_type = stream->tx_ctx.cn_pyld_num;
      b_packb( pkt->rtp_pyld_type, pkt->data + pkt->head_room , 9, 7 );
       
      return ( g729_profile_config.tx_cb( pkt, usr_hdl ) );
    }

    /*----------------------------------------------------------------------
      handle silence frame
    ----------------------------------------------------------------------*/
    case QVP_RTP_AUDIO_NO_DATA:
    {
      /*--------------------------------------------------------------------
         Check if silence suppression is enabled, then drop the frame
      --------------------------------------------------------------------*/   
      if( stream->tx_ctx.enable_silence_suppr )
      {
        QVP_RTP_MSG_MED_1( " Suppress this silence frame %d \r\n", 
                          pkt->tstamp, 0, 0);
        qvp_rtp_free_buf( pkt );
        return( QVP_RTP_SUCCESS );
      }
      else
      {
        return ( g729_profile_config.tx_cb( pkt, usr_hdl ) );
      }
    }

    /*----------------------------------------------------------------------
      send the speech frame as it is
    ----------------------------------------------------------------------*/
    case QVP_RTP_AUDIO_SPEECH:
    {
      /*----------------------------------------------------------------
        if there is no need to bundle just ship the frame   
      -----------------------------------------------------------------*/
     
      if(stream->stream_config.tx_bundle_size == 1)
      {
        return g729_profile_config.tx_cb(pkt, usr_hdl);
      }

      QVP_RTP_MSG_LOW_1( "qvp_rtp_g729_profile_send %d bundle size",stream->tx_ctx.frame_cnt);
     
      /*----------------------------------------------------------------
        If we need to bundle then copy the frame onto the frame buffer and 
        then decide if we need to ship the packet
      -----------------------------------------------------------------*/
       dataptr = stream->tx_ctx.frame_buffer + (stream->tx_ctx.frame_cnt * QVP_RTP_G729_DFLT_SPEECH_FRAME_SIZE);

       qvp_rtp_memscpy((void*)dataptr,QVP_RTP_G729_DFLT_SPEECH_FRAME_SIZE,pkt->data + pkt->head_room,QVP_RTP_G729_DFLT_SPEECH_FRAME_SIZE);

       /*------------------------------------------------
         Copy the timestamp of the first Packet
       -------------------------------------------------*/
       if(stream->tx_ctx.frame_cnt == 0)
          stream->tx_ctx.first_time_stamp = pkt->tstamp; 
  
       //Increment the bundle size;
       stream->tx_ctx.frame_cnt++;
      /*----------------------------------------------------------------
        We reached the maximum bundle size ,hence ship it to the network 
      -----------------------------------------------------------------*/
      if(stream->tx_ctx.frame_cnt == stream->stream_config.tx_bundle_size)
      {
        //copy the stored timestamp 
        pkt->tstamp = stream->tx_ctx.first_time_stamp;
        pkt->len    = QVP_RTP_G729_DFLT_SPEECH_FRAME_SIZE * stream->tx_ctx.frame_cnt;
        qvp_rtp_memscpy(pkt->data + pkt->head_room,pkt->len,stream->tx_ctx.frame_buffer,pkt->len);

        //send out the speech packet
        status = g729_profile_config.tx_cb(pkt, usr_hdl);

        if(status != QVP_RTP_SUCCESS)
        {
          QVP_RTP_ERR( "Sending speech packet failed",0, 0, 0 );
        }

        //Reset bundle size and memset the frame buffer
        
        memset(stream->tx_ctx.frame_buffer,0,QVP_RTP_G729_DFLT_SPEECH_FRAME_SIZE * stream->stream_config.tx_bundle_size);
        stream->tx_ctx.frame_cnt = 0;
      }
      else
      {
      
      /*------------------------------------------------------------------
        Free if needed since we are not going to send this now
       ------------------------------------------------------------------*/
        QVP_RTP_MSG_LOW_0("Freeing packets  _DEBUG_ ");
        if ( pkt->need_tofree )
        {
          qvp_rtp_free_buf( pkt );
        }
      }
      return QVP_RTP_SUCCESS;
    }

    default:
    {
      QVP_RTP_ERR( " unknown frame type, discard this frame %d \r\n", 
                     pkt->tstamp, 0, 0);
      qvp_rtp_free_buf( pkt );
      return QVP_RTP_ERR_FATAL;
    }

  } /* end of switch frame type */


} /* end of function qvp_rtp_g729_profile_send */

/*===========================================================================

FUNCTION  QVP_RTP_g729_PROFILE_OPEN 


DESCRIPTION
    Requests to open a bi directional channel within the profile.

DEPENDENCIES
  None

ARGUMENTS IN
  usr_hdl       - handle to use when we call send function using the 
                  registered tx callback

ARGUMENTS OUT
  hdl           - on success we write the handle to the profiile into 
                  the  passed double pointer
  
RETURN VALUE
  QVP_RTP_SUCCESS  - if we could send data. or an error code


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_g729_profile_open
(
  qvp_rtp_profile_hdl_type     *hdl,         /* handle which will get 
                                              * populated with a new profile
                                              * stream handle
                                              */
  qvp_rtp_profile_usr_hdl_type  usr_hdl      /* user handle for any cb */
)
{
  uint32 i;   /* index variable */
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
    Bail out if we are not initialized yet
  ------------------------------------------------------------------------*/
  if( !g729_initialized )
  {
    return( QVP_RTP_ERR_FATAL );
  }

  /*------------------------------------------------------------------------
              look at an idle stream 
  ------------------------------------------------------------------------*/
  for( i = 0;  i < g729_profile_config.num_streams; i++ )
  {
    
    
    /*----------------------------------------------------------------------
              Fish for free entries
    ----------------------------------------------------------------------*/
    if( !qvp_rtp_g729_array[ i ].valid )
    {
      qvp_rtp_g729_array[ i ].valid = TRUE;
      *hdl = &qvp_rtp_g729_array[ i ]; 
      
      /*--------------------------------------------------------------------
            copy the g729 configuration to the  stream_ctx 
      --------------------------------------------------------------------*/
      qvp_rtp_memscpy( &qvp_rtp_g729_array[i].stream_config, sizeof(qvp_rtp_g729_config_type),&g729_stream_config,
               sizeof( qvp_rtp_g729_array[i].stream_config ));

      /*--------------------------------------------------------------------
          Reset the stream context - if any stale values in there.
      --------------------------------------------------------------------*/
      qvp_rtp_g729_reset_stream( &qvp_rtp_g729_array[i] );
      
      
      
      return( QVP_RTP_SUCCESS );

    }
    
  } /* maximum no of streams */

  return( QVP_RTP_NORESOURCES );


} /* end of function qvp_rtp_g729_profile_open */


/*==================================================================

FUNCTION QVP_RTP_g729_PROFILE_VALIDATE_CONFIG 


DESCRIPTION

  This function checks the configuration of g729 specified in the input 
  against the valid fmtp values for g729

DEPENDENCIES
  None

ARGUMENTS IN
    payld_config - configuration which needs to be validated

ARGUMENTS OUT
  counter_config - If the attribute  is valid, counter_config is not modified
                   If the attribute  is invalid,
                   counter_config is updated with proposed value

RETURN VALUE
  QVP_RTP_SUCCESS  - The given Configuration is  valid for the given payload,
                     RTP configure will succeed for these settings
                     
  QVP_RTP_ERR_FATAL   - The given Configuration is not valid for the given 
                        payload. RTP configure will fail for these settings

SIDE EFFECTS
  None
===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_g729_profile_validate_config_rx
(
  qvp_rtp_payload_config_type  *payld_config,
  qvp_rtp_payload_config_type  *counter_config 
)
{
  qvp_rtp_status_type status = QVP_RTP_SUCCESS;
  /*----------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
    If profile config is NULL return error
  ------------------------------------------------------------------------*/
  if ( ( !payld_config ) || ( !counter_config ) )
  {
    return( QVP_RTP_ERR_FATAL );
  }

  /*------------------------------------------------------------------------
    Validating rx configuration
   -----------------------------------------------------------------------*/
  if ( ( payld_config->config_rx_params.valid ) && 
       ( payld_config->config_rx_params.
             config_rx_payld_params.g729_rx_config.valid ) )
  {
    if ( ( payld_config->ch_dir == QVP_RTP_CHANNEL_INBOUND ) || 
              ( payld_config->ch_dir == QVP_RTP_CHANNEL_FULL_DUPLEX ) )
    {
      /*--------------------------------------------------------------------
        Validate ptime. Throw an error if we cant receive packets with 
        the ptime proposed.
      --------------------------------------------------------------------*/
      if ( ( payld_config->config_rx_params.rx_rtp_param.ptime_valid ) && 
           ( payld_config->config_rx_params.rx_rtp_param.ptime  != 
                       QVP_RTP_G729_DFLT_PKT_INTERVAL ) )
      {
        counter_config->config_rx_params.rx_rtp_param.ptime = 
                        QVP_RTP_G729_DFLT_PKT_INTERVAL ; 
        status = QVP_RTP_ERR_FATAL;
      }
      /*--------------------------------------------------------------------
        Validate maxptime. Throw an error if we cant receive packets with 
        the maxptime proposed.
      --------------------------------------------------------------------*/
      if ( ( payld_config->config_rx_params.rx_rtp_param.maxptime_valid ) && 
           ( ( payld_config->config_rx_params.rx_rtp_param.maxptime  < 
                       QVP_RTP_G729_DFLT_PKT_INTERVAL ) ||
             ( payld_config->config_rx_params.rx_rtp_param.maxptime > 
                    QVP_RTP_G729_DFLT_PKT_INTERVAL *
                                              QVP_RTP_G729_DFLT_BUNDLE_SIZE ) )  )
      {
        counter_config->config_rx_params.rx_rtp_param.maxptime = 
                  QVP_RTP_G729_DFLT_PKT_INTERVAL *
                                                   QVP_RTP_G729_DFLT_BUNDLE_SIZE ; 
        status = QVP_RTP_ERR_FATAL;
      }

    }/* end of if ( ( payld_config->ch_dir */
    else
    {
      QVP_RTP_ERR ( " Attempting to config rx for an o/b only ch", 0, 0, 0);
      status = QVP_RTP_ERR_FATAL ;
    }
  } /* end of   if ( ( payld_config->config_rx_params.valid )*/

  
  return ( status ); 


}/* end of qvp_rtp_g729_profile_validate_config_rx */


/*===========================================================================

FUNCTION QVP_RTP_g729_PROFILE_VALIDATE_CONFIG_TX 


DESCRIPTION

  This function checks the configuration of g729 specified in the input 
  against the valid fmtp values for g729

DEPENDENCIES
  None

ARGUMENTS IN
    payld_config - configuration which needs to be validated

ARGUMENTS OUT
  counter_config - If the attribute  is valid, counter_config is not modified
                   If the attribute  is invalid, 
                   counter_config is updated with proposed value


RETURN VALUE
  QVP_RTP_SUCCESS  - The given Configuration is  valid for the given payload,
                     RTP configure will succeed for these settings
  QVP_RTP_ERR_FATAL   - The given Configuration is not valid for the given 
                        payload. RTP configure will fail for these settings

SIDE EFFECTS
  None
===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_g729_profile_validate_config_tx
(
  qvp_rtp_payload_config_type*  payld_config,
  qvp_rtp_payload_config_type*  counter_config 
)
{
  qvp_rtp_status_type status = QVP_RTP_SUCCESS;
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
    If profile config is NULL return error
  ------------------------------------------------------------------------*/
  if( !payld_config )
  {
    return( QVP_RTP_ERR_FATAL );
  }

  /*------------------------------------------------------------------------
    Validating tx configuration
   -----------------------------------------------------------------------*/
  if ( ( payld_config->config_tx_params.valid ) && 
       ( payld_config->config_tx_params.
             config_tx_payld_params.g729_tx_config.valid ) )
  {
    if ( ( payld_config->ch_dir == QVP_RTP_CHANNEL_OUTBOUND ) || 
         ( payld_config->ch_dir == QVP_RTP_CHANNEL_FULL_DUPLEX ) )
    {
      /*--------------------------------------------------------------------
        Validate ptime. Throw an error if peer wants us to send 
        packets with ptime lesser than our minimum supported ptime.
      --------------------------------------------------------------------*/
      if ( ( payld_config->config_tx_params.tx_rtp_param.ptime_valid ) && 
           ( payld_config->config_tx_params.tx_rtp_param.ptime < 
                       QVP_RTP_G729_DFLT_PKT_INTERVAL ) )
      {
        counter_config->config_tx_params.tx_rtp_param.ptime = 
                        QVP_RTP_G729_DFLT_PKT_INTERVAL ; 
        status = QVP_RTP_ERR_FATAL;
      }
      /*--------------------------------------------------------------------
        Validate maxptime. Throw an error if peer wants us to send 
        packets with maxptime lesser than our minimum supported maxptime.
      --------------------------------------------------------------------*/
      if ( ( payld_config->config_tx_params.tx_rtp_param.maxptime_valid ) && 
           ( payld_config->config_tx_params.tx_rtp_param.maxptime  < 
                       QVP_RTP_G729_DFLT_PKT_INTERVAL )  )
      {
        counter_config->config_tx_params.tx_rtp_param.maxptime = 
                  QVP_RTP_G729_DFLT_PKT_INTERVAL *
                                                   QVP_RTP_G729_DFLT_BUNDLE_SIZE ; 
        status = QVP_RTP_ERR_FATAL;
      }

    }/* end of if ( ( payld_config->ch_dir */
    else
    {
      QVP_RTP_ERR ( " Attempting to config tx for an i/b only ch", 0, 0, 0);
      status = QVP_RTP_ERR_FATAL ;
    }
  } /* end of if ( ( payld_config->config_tx_params.valid )*/
  
  return ( status ); 

}/* end of qvp_rtp_g729_profile_validate_config_tx */


/*===========================================================================

FUNCTION QVP_RTP_g729_PROFILE_READ_DEFLT_CONFIG 


DESCRIPTION

  This function reads out the default configuration of g729
  payload format. The result is conveyed by populating the passed in 
  structure on return.

DEPENDENCIES
  None

ARGUMENTS IN
    payload        - payload type for which the default configuration 
                     is being read out. This is not used by g729. Some 
                     other profiles uses it as they support multiple 
                     payload formats.
    
ARGUMENTS OUT
    payld_config   - pointer to configuration structure. On return this 
                     structure will be populated with the default values
                     for the profile.
    



RETURN VALUE
  QVP_RTP_SUCCESS  - operation was succesful.
                    appropriate error code otherwise


SIDE EFFECTS
  payld_config structure will be populated with the default values 
  for the particular profile.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_g729_profile_read_deflt_config
(
  
  qvp_rtp_payload_type        payld, /* not used */ 
  qvp_rtp_payload_config_type *payld_config 
)
{

  /*------------------------------------------------------------------------
    If profile config is NULL return error
  ------------------------------------------------------------------------*/
  if( !payld_config )
  {

    return( QVP_RTP_ERR_FATAL );
  }
  

  return( qvp_rtp_g729_profile_copy_config( payld_config, 
                                            &g729_stream_config  ) );

    
  
} /* end of function qvp_rtp_g729_profile_read_deflt_config */ 

/*===========================================================================

FUNCTION QVP_RTP_g729_PROFILE_CONFIGURE

DESCRIPTION

  This function payload configuration of a previously opened g729  
  channel.

DEPENDENCIES
  None

ARGUMENTS IN
    hdl            - handle to the opend channel
    payld_config   - pointer to configuration structure
    



RETURN VALUE
  QVP_RTP_SUCCESS  - operation was succesfull. 
  else - Attempted a configuration  which is not currently supported by 
         the implementation.


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_g729_profile_configure
(
  qvp_rtp_profile_hdl_type      hdl,     
  qvp_rtp_payload_config_type   *payld_config 
)
{
  qvp_rtp_g729_ctx_type *stream = ( qvp_rtp_g729_ctx_type *) hdl; 
  qvp_rtp_g729_config_type prev_config;
  qvp_rtp_status_type      status = QVP_RTP_SUCCESS;
/*------------------------------------------------------------------------*/


  
  /*------------------------------------------------------------------------
                Are we sane here ..?
  ------------------------------------------------------------------------*/
  if( !g729_initialized || !stream || !stream->valid || 
      !payld_config ) 
  {
    return( QVP_RTP_ERR_FATAL );
  }

  /*------------------------------------------------------------------------
    Backup the current configuration before we proceed from here.
  ------------------------------------------------------------------------*/
  prev_config = stream->stream_config;
  
  /*------------------------------------------------------------------------
      Configuration steps.

      1) Try and configure RX.
         If RX fails.
          Reset it to previous state and return error.
          This way application can easily implement OFFER/ANSWER model as 
          given in RFC3264.
      2) Try and configure TX
         If TX  configure fails.
          Reset it to previous state and return error.
          This way application can easily implement OFFER/ANSWER model 
          as given in RFC3264.
  ------------------------------------------------------------------------*/

  if( payld_config->config_rx_params.valid )
  {
    status = qvp_rtp_g729_profile_config_rx_param( stream, payld_config );
  }


  /*------------------------------------------------------------------------
      Try and configure Tx part now....
  ------------------------------------------------------------------------*/
  if( ( status == QVP_RTP_SUCCESS )&&
      payld_config->config_tx_params.valid )
  {
    status = qvp_rtp_g729_profile_config_tx_param(stream, payld_config );
    
  }

  /*------------------------------------------------------------------------
    If we did not succeed to a struct to reset to preivous value and 
    return failure
  ------------------------------------------------------------------------*/
  if( status != QVP_RTP_SUCCESS )
  {
    stream->stream_config = prev_config ;
  }
  
  return( status );

} /* end of function qvp_rtp_g729_profile_configure */

/*===========================================================================

FUNCTION QVP_RTP_G729_PROFILE_CONFIG_RX_PARAM

DESCRIPTION

  This function payload will configuration of Rx part of a previously
  opened stream.  

DEPENDENCIES
  None

ARGUMENTS IN
    stream         - g729 stream which needs to be configured.
    payld_config   - pointer to configuration structure
    



RETURN VALUE
  QVP_RTP_SUCCESS  - operation was succesfull. 
  else - Attempted a configuration  which is not currently supported by 
         the implementation.


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_g729_profile_config_rx_param
(
  
  qvp_rtp_g729_ctx_type       *stream,
  qvp_rtp_payload_config_type *payld_config 
)
{
  qvp_rtp_g729_sdp_config_type  *g729_rx_config = 
   &payld_config->config_rx_params.config_rx_payld_params.g729_rx_config;

  if( !payld_config || !stream ) 
  {
    QVP_RTP_ERR ( " Invalid params", 0, 0, 0);
    return( QVP_RTP_ERR_FATAL );
  }
  /*------------------------------------------------------------------------
    See if the pttime matches with what we have
  ------------------------------------------------------------------------*/
  if( payld_config->config_rx_params.rx_rtp_param.ptime_valid && 
      ( payld_config->config_rx_params.rx_rtp_param.ptime != 
                                 QVP_RTP_G729_DFLT_PKT_INTERVAL ) )
  {
    QVP_RTP_MSG_HIGH_1( "g729 Invalid Rx ptime %d ",
        payld_config->config_rx_params.rx_rtp_param.ptime, 0, 0 );
    return( QVP_RTP_ERR_FATAL );
  }

  /*------------------------------------------------------------------------
    We will cache the max ptime ... If this exceeds too much this will
    be bad for our jitter buffer. But then the other end is asking 
    for trouble.
  ------------------------------------------------------------------------*/
  if( payld_config->config_rx_params.rx_rtp_param.maxptime_valid ) 
  {

    /*----------------------------------------------------------------------
      if maxptime is LT ptime or greater than maximum maxptime we can 
      support bail out with error.
    ----------------------------------------------------------------------*/
    if( ( payld_config->config_rx_params.rx_rtp_param.maxptime < 
        QVP_RTP_G729_DFLT_PKT_INTERVAL ) || 
        ( payld_config->config_rx_params.rx_rtp_param.maxptime > 
           QVP_RTP_G729_MAX_BUNDLE_SIZE * QVP_RTP_G729_DFLT_PKT_INTERVAL ) )
    {
      QVP_RTP_MSG_MED_1( "g729 Invalid Rx maxptime %d ",
        payld_config->config_rx_params.rx_rtp_param.maxptime, 0, 0 );
      return( QVP_RTP_ERR_FATAL );
    }
    stream->stream_config.rx_max_ptime = 
          payld_config->config_rx_params.rx_rtp_param.maxptime; 
  }
  QVP_RTP_MSG_MED_2( "g729 Rx maxptime %d Rx ptime %d",
        stream->stream_config.rx_max_ptime,
         stream->stream_config.rx_ptime, 0 );

  if( g729_rx_config->annexb_valid )
  {
    stream->rx_ctx.vad_on = g729_rx_config->annexb;
  }

  if(g729_rx_config->cn_pyld_num != 0 )
  {
    stream->rx_ctx.cn_pyld_num = g729_rx_config->cn_pyld_num;
  }
  else
  {
    stream->rx_ctx.cn_pyld_num = 13;
  }
  
  QVP_RTP_MSG_HIGH_2( "g729 Rx VAD %d CN pyld num %d",
        stream->rx_ctx.vad_on,
        stream->rx_ctx.cn_pyld_num, 0 );
  return( QVP_RTP_SUCCESS );
  
} /* end of function qvp_rtp_g729_profile_config_rx_param */ 

/*===========================================================================

FUNCTION QVP_RTP_g729_PROFILE_CONFIG_TX_PARAM

DESCRIPTION

  This function payload will configuration of Tx part of a previously
  opened stream.  

DEPENDENCIES
  None

ARGUMENTS IN
    stream         - g729 stream which needs to be configured.
    payld_config   - pointer to configuration structure
    



RETURN VALUE
  QVP_RTP_SUCCESS  - operation was succesfull. 
  else - Attempted a configuration  which is not currently supported by 
         the implementation.


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_g729_profile_config_tx_param
(
  
  qvp_rtp_g729_ctx_type       *stream,
  qvp_rtp_payload_config_type *payld_config 
)
{
  qvp_rtp_g729_sdp_config_type  *g729_tx_config = 
    &payld_config->config_tx_params.config_tx_payld_params.g729_tx_config;

  
  if( !payld_config || !stream ) 
  {
    QVP_RTP_ERR ( " Invalid params", 0, 0, 0);
    return( QVP_RTP_ERR_FATAL );
  }

  if( payld_config->config_tx_params.tx_rtp_param.ptime_valid )
  {
    /*----------------------------------------------------------------------
      This means peer can receive pkts with ptime lesser 
      than our minimum ptime pkts we can send. Throw an error.
    ----------------------------------------------------------------------*/
    if ( payld_config->config_tx_params.tx_rtp_param.ptime < 
                QVP_RTP_G729_DFLT_PKT_INTERVAL )
    {
      QVP_RTP_MSG_MED_1( "g729 Invalid Tx ptime %d ",
        payld_config->config_tx_params.tx_rtp_param.ptime, 0, 0 );
      return( QVP_RTP_ERR_FATAL );
    }
    /*----------------------------------------------------------------------
      This means peer can receive pkts with ptime greater 
      than our maximum ptime. Just set the transmit ptime to maximum 
      ptime we can send.
    ----------------------------------------------------------------------*/
    else if ( payld_config->config_tx_params.tx_rtp_param.ptime > 
                QVP_RTP_G729_DFLT_PKT_INTERVAL )
    {
      stream->stream_config.tx_ptime = QVP_RTP_G729_DFLT_PKT_INTERVAL;
    }
    /*----------------------------------------------------------------------
      Proposed ptime is within the limits, we can send packets with the 
      ptime proposed.
    ----------------------------------------------------------------------*/
    else
    {
      stream->stream_config.tx_ptime = 
          payld_config->config_tx_params.tx_rtp_param.ptime;
    }
  }

  if( payld_config->config_tx_params.tx_rtp_param.maxptime_valid ) 
  {
    /*----------------------------------------------------------------------
      if max ptime is less than ptime then we have a problem. 
    ----------------------------------------------------------------------*/
    if( payld_config->config_tx_params.tx_rtp_param.maxptime < 
        QVP_RTP_G729_DFLT_PKT_INTERVAL )
    {
      QVP_RTP_MSG_MED_1( "g729 Invalid Tx maxptime %d ",
        payld_config->config_tx_params.tx_rtp_param.maxptime, 0, 0 );
      return( QVP_RTP_ERR_FATAL );
    }
    /*----------------------------------------------------------------------
      This means peer can receive pkts with maxptime greater 
      than our maximum maxptime. Just set the transmit maxptime to 
      maximum maxptime we can send.
    ----------------------------------------------------------------------*/
    if( payld_config->config_tx_params.tx_rtp_param.maxptime > 
        QVP_RTP_G729_DFLT_PKT_INTERVAL * QVP_RTP_G729_MAX_BUNDLE_SIZE)
    {
      stream->stream_config.tx_max_ptime   = 
        QVP_RTP_G729_DFLT_PKT_INTERVAL * QVP_RTP_G729_MAX_BUNDLE_SIZE;
    }
    /*----------------------------------------------------------------------
      Proposed maxptime is within the limits, we can send packets with the 
      maxptime proposed.
    ----------------------------------------------------------------------*/
    else
    {
      stream->stream_config.tx_max_ptime = 
            payld_config->config_tx_params.tx_rtp_param.maxptime;
    }

  }
  
   
  stream->stream_config.tx_bundle_size = 
     stream->stream_config.tx_max_ptime/stream->stream_config.tx_ptime;


  QVP_RTP_MSG_HIGH("Configured maxptime %d ptime %d bundle value %d ",stream->stream_config.tx_max_ptime ,
    stream->stream_config.tx_ptime, stream->stream_config.tx_bundle_size);
  

 /*-------------------------------------------------------------------
   Allocate a frame buffer to hold intermediate frames for bundling
  ----------------------------------------------------------------*/
  //Free any previosly configured value
  if(stream->tx_ctx.frame_buffer)
    {
      qvp_rtp_free(stream->tx_ctx.frame_buffer);
      stream->tx_ctx.frame_buffer = NULL;
    }

  stream->tx_ctx.frame_cnt     = 0;
  
  stream->tx_ctx.first_time_stamp  = 0;
  
  //QVP_RTP_MSG_HIGH_1("Configured bundling size  _DEBUG_ %d",stream->stream_config.tx_bundle_size);
  
  stream->tx_ctx.frame_buffer = qvp_rtp_malloc(QVP_RTP_G729_DFLT_SPEECH_FRAME_SIZE * stream->stream_config.tx_bundle_size);

  //QVP_RTP_MSG_HIGH_1("qvp_rtp_g729_profile_config_tx_param %d",QVP_RTP_G729_DFLT_SPEECH_FRAME_SIZE * stream->stream_config.tx_bundle_size );

  if(!stream->tx_ctx.frame_buffer)
  {
    //send an alarm if malloc failed
    return( QVP_RTP_ERR_FATAL );
  }

  if( g729_tx_config->annexb_valid )
  {
    stream->tx_ctx.vad_on = g729_tx_config->annexb;
  }

  if(g729_tx_config->cn_pyld_num != 0)
  {
    stream->tx_ctx.cn_pyld_num = g729_tx_config->cn_pyld_num;
  }
  else
  {
    stream->tx_ctx.cn_pyld_num = 13;
  }
  
  QVP_RTP_MSG_HIGH_2( "g729 Tx VAD %d CN pyld num %d",
        stream->tx_ctx.vad_on,
        stream->tx_ctx.cn_pyld_num, 0 );

  return( QVP_RTP_SUCCESS );
        
} /* end of function qvp_rtp_g729_profile_config_tx_param */
  
/*===========================================================================

FUNCTION  QVP_RTP_g729_PROFILE_RECV 


DESCRIPTION
   The function which the RTP framework will call upon arrival of data from 
   NW. g729 header is stripped but parameters are passed.

DEPENDENCIES
  None

ARGUMENTS IN
  hdl           - hdl into the profile.
  usr_hdl       - handle to use when we call send function using the 
                  registered rx callback
  pkt           - actual data packet to be parsed.

  
RETURN VALUE
  QVP_RTP_SUCCESS  - if we could send data. or an error code


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_g729_profile_recv
(

  qvp_rtp_profile_hdl_type     hdl,          /* handle created while open */
  qvp_rtp_profile_usr_hdl_type usr_hdl,      /* user handle for rx cb */
  qvp_rtp_buf_type             *pkt          /* packet parsed and removed*/

)
{
  qvp_rtp_g729_ctx_type *stream = ( qvp_rtp_g729_ctx_type *) hdl; 
  int16   inof                  = 0;
  uint32  tstamp                = 0;
  int16   counter               = 0;
  qvp_rtp_buf_type    *aud_buf  = NULL;
  uint8               *aud_data = NULL;


   
  /*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
                Are we sane here ..?
  ------------------------------------------------------------------------*/
  if( !g729_initialized || !stream || !stream->valid || 
      !g729_profile_config.rx_cb  ) 
  {
    return( QVP_RTP_ERR_FATAL );
  }

  QVP_RTP_MSG_MED( "RX g729 packet tstamp =  %d, Seq = %d, Len %d", 
               pkt->tstamp , pkt->seq, pkt->len );
 
  /*------------------------------------------------------------------------
    Find the frame type
  ------------------------------------------------------------------------*/
  if( pkt->rtp_pyld_type == stream->rx_ctx.cn_pyld_num ||
       pkt->rtp_pyld_type == QVP_RTP_DFLT_CN_PYLD_NUM ||
       pkt->len == QVP_RTP_CN_PYLD_SIZE )
  {
    pkt->frm_info.info.aud_info.frame_type = 
                     QVP_RTP_AUDIO_SID;
  }
  else if( pkt->len == 0)
  {
    pkt->frm_info.info.aud_info.frame_type = 
                         QVP_RTP_AUDIO_NO_DATA;
  }
  else if( pkt->len > 0 )
  {
    //number of frames
    inof   = qvp_rtp_g729_find_num_frames(pkt);
    tstamp = pkt->tstamp;
    
    

    QVP_RTP_MSG_LOW_1("qvp_rtp_g729_profile_recv number of frames %d",inof);

    for(counter = 1;counter <= inof;counter++)
    {
      /*--------------------------------------------------------------------
        try and alloc an audio buffer
      --------------------------------------------------------------------*/
       aud_buf = qvp_rtp_alloc_buf( QVP_RTP_POOL_AUDIO );
      
      /*--------------------------------------------------------------------
        See if we got a buffer
      --------------------------------------------------------------------*/
      if ( !aud_buf )
      { 
        QVP_RTP_ERR( " Could not get an audio buffer \r\n", 0, 0, 0 );
        
        /*------------------------------------------------------------------
        Free if needed
        ------------------------------------------------------------------*/
        if ( pkt->need_tofree )
        {
          qvp_rtp_free_buf( pkt );
        }
        return( QVP_RTP_ERR_FATAL );
        
      }

       aud_buf->head_room = 0;
       aud_buf->len = QVP_RTP_G729_DFLT_SPEECH_FRAME_SIZE; 
       aud_buf->need_tofree = TRUE;

       /*--------------------------------------------------------
         Each packet contains the timestamp of the 1st frame for the other 
         frames incrment by 160 samples
       ---------------------------------------------------------*/
       aud_buf->tstamp = tstamp + (QVP_RTP_G729_DFLT_PKT_INCR  * (counter -1));
       aud_buf->seq   = pkt->seq;

       //set the frame type
       aud_buf->frm_info.info.aud_info.frame_type = 
                         QVP_RTP_AUDIO_SPEECH;
       aud_data = pkt->data + pkt->head_room;

       //Move the data to point to the next frame
       aud_data = aud_data + (QVP_RTP_G729_DFLT_SPEECH_FRAME_SIZE * (counter -1));

       QVP_RTP_MSG_LOW_2(" qvp_rtp_g729_profile_recv counter %d ts %x",counter,aud_buf->tstamp);

       qvp_rtp_memscpy( aud_buf->data,QVP_RTP_G729_DFLT_SPEECH_FRAME_SIZE,(void*)(aud_data),QVP_RTP_G729_DFLT_SPEECH_FRAME_SIZE);
       
       aud_buf->silence = FALSE;
       aud_buf->frm_info.info.aud_info.frm_present = TRUE;

       //ship the packet 
       (g729_profile_config.rx_cb( aud_buf,usr_hdl));
    }

      /*----------------------------------------------------------------------
            Free the buffer if needed
      ----------------------------------------------------------------------*/
      if( pkt->need_tofree )
      {
        qvp_rtp_free_buf( pkt );
      }
     return QVP_RTP_SUCCESS;
  }
  else 
  {
    pkt->frm_info.info.aud_info.frame_type = 
                           QVP_RTP_AUDIO_INVALID_FRAME;
    return( g729_profile_config.rx_cb( pkt, usr_hdl ) );
  }

  QVP_RTP_MSG_MED_1( "RX g729 frame type %d", 
               pkt->frm_info.info.aud_info.frame_type ,0, 0 );
  
  return( g729_profile_config.rx_cb( pkt, usr_hdl ) );
   
} /* end of function qvp_rtp_g729_profile_recv */

/*===========================================================================

FUNCTION  QVP_RTP_g729_PROFILE_CLOSE


DESCRIPTION
  Closes an already open bi directional channel inside the profile.

DEPENDENCIES
  None

ARGUMENTS IN
  hdl - handle of channel to close.

RETURN VALUE
  QVP_RTP_SUCCESS  - if we could send data. or an error code


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_g729_profile_close
(

  qvp_rtp_profile_hdl_type     hdl           /* handle the profile */

)
{
  qvp_rtp_g729_ctx_type *stream = ( qvp_rtp_g729_ctx_type *) hdl; 
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
              If we get  a valid handle invalidate the index 

              Also check are we initialized yet ..?
  ------------------------------------------------------------------------*/
  if( hdl && g729_initialized )
  {
    
    /*----------------------------------------------------------------------
        We cannot afford to loose buffers so when we are in the middle of
        reassmbling we need to free the whole chain
    ----------------------------------------------------------------------*/
    stream->valid = FALSE;
    
    /*----------------------------------------------------------------------
        Reset the stream context - if any stale values in there.
    ----------------------------------------------------------------------*/
    qvp_rtp_g729_reset_stream( stream );

    if(stream->tx_ctx.frame_buffer)
    {
      qvp_rtp_free(  stream->tx_ctx.frame_buffer);
      stream->tx_ctx.frame_buffer = NULL;
    }
    
    return( QVP_RTP_SUCCESS );
  }
  else
  {
    return( QVP_RTP_ERR_FATAL );
  }


} /* end of function qvp_rtp_g729_profile_close */ 

/*===========================================================================

FUNCTION  QVP_RTP_g729_RESET_TX_CTX


DESCRIPTION
  Reset the trasmitter context inside the stream

DEPENDENCIES
  None

ARGUMENTS IN
  stream to be reset

RETURN VALUE
  None


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL void qvp_rtp_g729_reset_tx_ctx
( 
  qvp_rtp_g729_ctx_type *stream 
)
{
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
      Reset the packet to packet to packet context
  ------------------------------------------------------------------------*/
  stream->tx_ctx.enable_silence_suppr = FALSE;
  stream->tx_ctx.vad_on = TRUE;
  stream->tx_ctx.cn_pyld_num = 13;

  stream->tx_ctx.frame_buffer = NULL;
  stream->tx_ctx.frame_cnt = 0;
  stream->tx_ctx.first_time_stamp = 0;
  
} /* end of function qvp_rtp_g729_reset_tx_ctx */
    
/*===========================================================================

FUNCTION  QVP_RTP_g729_RESET_STREAM 


DESCRIPTION
  Reset the context inside the stream

DEPENDENCIES
  None

ARGUMENTS IN
  stream to be reset

RETURN VALUE
  QVP_RTP_SUCCESS  - if we could send data. or an error code


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL void qvp_rtp_g729_reset_stream
( 
  qvp_rtp_g729_ctx_type *stream 
)
{
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
          Reset the transmit side
  ------------------------------------------------------------------------*/
  stream->tx_ctx.enable_silence_suppr = FALSE;
  stream->tx_ctx.vad_on = TRUE;
  stream->tx_ctx.cn_pyld_num = 13;
  stream->tx_ctx.frame_buffer = NULL;
  stream->tx_ctx.frame_cnt = 0;
  stream->tx_ctx.first_time_stamp = 0;
  stream->tx_ctx.valid = TRUE;

  stream->rx_ctx.valid = TRUE;
  stream->rx_ctx.vad_on = TRUE;
  stream->rx_ctx.cn_pyld_num = 13;

  
} /* end of function qvp_rtp_g729_reset_stream */

/*===========================================================================

FUNCTION QVP_RTP_g729_PROFILE_COPY_CONFIG 


DESCRIPTION

  This function reads out the default configuration of g729
  payload format. The result is conveyed by populating the passed in 
  structure on return.

DEPENDENCIES
  None

ARGUMENTS IN
    payload        - payload type for which the default configuration 
                     is being read out. This is not used by g729. Some 
                     other profiles uses it as they support multiple 
                     payload formats.
    
ARGUMENTS OUT
    payld_config   - pointer to configuration structure. On return this 
                     structure will be populated with the default values
                     for the profile.
    



RETURN VALUE
  QVP_RTP_SUCCESS  - operation was succesful.
                    appropriate error code otherwise


SIDE EFFECTS
  payld_config structure will be populated with the default values 
  for the particular profile.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_g729_profile_copy_config
(
  qvp_rtp_payload_config_type *payld_config,
  qvp_rtp_g729_config_type    *g729_config
)
{
  qvp_rtp_g729_sdp_config_type  *g729_tx_config = 
      &payld_config->config_tx_params.config_tx_payld_params.g729_tx_config;
  qvp_rtp_g729_sdp_config_type  *g729_rx_config = 
      &payld_config->config_rx_params.config_rx_payld_params.g729_rx_config;
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
    Memset the whole configuration before we proceed
  ------------------------------------------------------------------------*/
  memset( g729_rx_config, 0, 
          sizeof( qvp_rtp_g729_sdp_config_type ) ); 
  memset( g729_tx_config, 0, 
          sizeof( qvp_rtp_g729_sdp_config_type ) );
  
  /*------------------------------------------------------------------------
      Copy each and every FMTP and flag them as TRUE
  ------------------------------------------------------------------------*/
  
  payld_config->config_tx_params.tx_rtp_param.maxptime_valid = 
  payld_config->config_rx_params.rx_rtp_param.maxptime_valid = FALSE;

  payld_config->config_tx_params.tx_rtp_param.ptime_valid = 
  payld_config->config_rx_params.rx_rtp_param.ptime_valid = TRUE;

  payld_config->config_tx_params.tx_rtp_param.ptime = 
                                  g729_config->tx_ptime;
  payld_config->config_rx_params.rx_rtp_param.ptime = 
                                  g729_config->rx_ptime;
  /*------------------------------------------------------------------------
    Flag the entire rx  and tx config as valid

    Just make sure we stick in our paylod format in the pointer.

    If you got here by accident you get what you get.
  ------------------------------------------------------------------------*/
  payld_config->config_rx_params.valid = TRUE;
  payld_config->config_tx_params.valid = TRUE;
  
  g729_tx_config->valid = TRUE;
  g729_rx_config->valid = TRUE; 
  
  g729_tx_config->annexb_valid = FALSE;
     g729_rx_config->annexb_valid = FALSE;   /* Take the default */
  
  payld_config->payload  = QVP_RTP_PYLD_G729;
  payld_config->payload_valid = TRUE;
  payld_config->chdir_valid = TRUE;
  payld_config->ch_dir = QVP_RTP_CHANNEL_FULL_DUPLEX;
  payld_config->valid = TRUE;
  
  return( QVP_RTP_SUCCESS );
  
} /* end of function qvp_rtp_g729_profile_copy_config */ 

/*===========================================================================

FUNCTION  QVP_RTP_g729_PROFILE_SHUTDOWN


DESCRIPTION
  Shuts this module down and flag intialization as false

DEPENDENCIES
  None

ARGUMENTS IN
  len - length of the parload

RETURN VALUE
  toc type


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL void qvp_rtp_g729_profile_shutdown( void )
{
  uint32 i;
/*------------------------------------------------------------------------*/


  /*------------------------------------------------------------------------
    Lets not do multiple shutdown
  ------------------------------------------------------------------------*/
  if( !g729_initialized )
  {
    return;
  }
  
  /*------------------------------------------------------------------------
      Walk through the stream array and close all channels
  ------------------------------------------------------------------------*/
  for( i = 0; i < g729_profile_config.num_streams; i ++ )
  {

    
    /*----------------------------------------------------------------------
        If this stream is open close it now
    ----------------------------------------------------------------------*/
    if ( qvp_rtp_g729_array[ i ].valid )
    {
      qvp_rtp_g729_profile_close( ( qvp_rtp_profile_hdl_type ) i );
    }
    
  } /* end of for i = 0  */
  
  /*------------------------------------------------------------------------
    free the array of streams
  ------------------------------------------------------------------------*/
  qvp_rtp_free( qvp_rtp_g729_array   );

  /*------------------------------------------------------------------------
    Flag array as NULL and init as FALSE
  ------------------------------------------------------------------------*/
  qvp_rtp_g729_array = NULL; 
  g729_initialized  = FALSE;
  
  
} /* end of function qvp_rtp_g729_profile_shutdown  */


LOCAL int16  qvp_rtp_g729_find_num_frames ( qvp_rtp_buf_type *buf)
{
 
  if(buf->len >= QVP_RTP_G729_DFLT_SPEECH_FRAME_SIZE)
     return buf->len /QVP_RTP_G729_DFLT_SPEECH_FRAME_SIZE;
  else 
    return 1;
}


#endif /* end of FEATURE_QVPHONE_RTP */
