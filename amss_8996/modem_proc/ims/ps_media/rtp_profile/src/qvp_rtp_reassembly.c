/*=*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                         QVP_RTP_REASSEMBLY . C

GENERAL DESCRIPTION

  This file contains data types & APIs needed for the implementation of 
  reassebmling packets belonging to same frame
  The reassebly context maintains the list of fragments
  of a frame and regroups them on reaching a indication of end of
  frame( marker bit )
  
APIs

  qvp_rtp_create_reassem_ctx()
     Creates a single reassembly context used for regrouping fragments
     belonging to a single frame. The context holds a array of fragments
     ordered list of frags, timestamp, first & last seq of frame, 
     number of bytes of data and few other params used in regrouping 
     frame.

  qvp_rtp_close_reassem_ctx()
     Closes a reassembly context by clearing all buffers & 
     freeing the memory for fragments

  qvp_rtp_input_frag_in_reassembly()
     This function adds a RTP packet, a fragment of a frame to the 
     reassembly chain. 

  qvp_rtp_get_reassembled_frame()
     This function regroups all the fragments in the reassembly chain  
     to form a complete frame.

  qvp_rtp_reset_reassem_ctx()
     Any time this function can be called to reset the context 
     of reassmbly.This function does not free the memory associated.

  qvp_rtp_abort_reassembly() 
     Walks through the reassembly chain and removes all buffers from 
     the ordered list of fragments

  qvp_rtp_get_reasm_frame_length()
     Obtains the frame length of the currrent reassembly    

  qvp_rtp_get_reasm_timestamp()
     Obtains the RTP timestamp of the frame being reassembled

  qvp_rtp_get_reasm_drop_cnt()
     Obtains number of RTP fragments dropped in the reassembly  

  qvp_rtp_get_reasm_last_seq()
     Obtains seq number of last pkt in reassembly/frame
  
  qvp_rtp_get_reasm_first_seq()
     Obtains seq number of first pkt in reassembly/frame
  
  qvp_rtp_get_reasm_missng_cnt()
     Obtains number of missing pkts in a reassembly/frame 
  
  qvp_rtp_get_reasm_marker_bit()
     Indicates if the pkt with marker bit has been received for this
     frame
  

INITIALIZATION AND SEQUENCING REQUIREMENTS

  1. Create a reassembly context and use the context handle to use 
     all the above APIs


  Copyright (c) 2004 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
  QUALCOMM Proprietary.  Export of this technology or software is
  regulated by the U.S. Government. Diversion contrary to U.S. law prohibited.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*=*/

/*===========================================================================


                            EDIT HISTORY FOR FILE


when        who    what, where, why
--------    ---    ----------------------------------------------------------
07/09/07    apr    Including RTP profile header file for errconcl macros
11/01/06    apr    Initial Creation.
===========================================================================*/
#include "ims_variation.h"
#include "customer.h"
#ifdef FEATURE_QVPHONE_RTP


#ifdef  RTP_FILE_NUMBER 
  #undef RTP_FILE_NUMBER 
#endif 
#define RTP_FILE_NUMBER 23

#include "qvp_rtp_reassembly.h"
#include "qvp_rtp_log.h"
#include "qvp_rtp_profile.h"


LOCAL int qvp_rtp_compare_fgmt
( 
  void *item, 
  void *seq_no
);

LOCAL qvp_rtp_status_type qvp_rtp_regroup_segments 
( 
  qvp_rtp_reassem_ctx_type* reassem_ctxt,
  ordered_list_type  *reassem_chain,
  uint8              *reassem_data,
  uint16             *reassem_len,
  uint16             len,
  uint16             last_seq_no
);

/*===========================================================================

FUNCTION qvp_rtp_init_reassem_frag_list 


DESCRIPTION
  Creates a single reassembly context used for regrouping fragments
  belonging to a single frame. The context holds the list of fragments
  timestamp, first & last seq of frame, number of bytes of data
  and few other params used in regrouping frame.

DEPENDENCIES
  None

ARGUMENTS IN
 None 

RETURN VALUE
  qvp_rtp_reassem_hdl

SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_reassem_hdl qvp_rtp_create_reassem_ctx( void )
{
  qvp_rtp_reassem_ctx_type* reassem_ctxt = NULL;
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
    Allocate memory
  ------------------------------------------------------------------------*/
  reassem_ctxt = (qvp_rtp_reassem_ctx_type*)
                      qvp_rtp_malloc( sizeof( qvp_rtp_reassem_ctx_type ) );
  
  if( !reassem_ctxt )
  {
    QVP_RTP_ERR( " Could not allocate reassembly ctxt ", 0, 0, 0 );
    return( NULL );
  }

  reassem_ctxt->frag_list = NULL;
  /*------------------------------------------------------------------------
    Allocate memory for fragments list
    This memory will be freed at close reassem_ctxt,
    no_of_frags_used keeps track of number fragments used at any istant
  ------------------------------------------------------------------------*/
  reassem_ctxt->frag_list = (qvp_rtp_fragmt_type*)qvp_rtp_malloc( 
             sizeof( qvp_rtp_fragmt_type) * QVP_RTP_MAX_NO_OF_VIDEO_FRAGS );

  if( !reassem_ctxt->frag_list )
  {
    qvp_rtp_free( reassem_ctxt );
    reassem_ctxt = NULL;

    return( NULL );

  }

  /*------------------------------------------------------------------------
    Initialise ordered list & other members of ctxt
  ------------------------------------------------------------------------*/
  ordered_list_init( 
                  &reassem_ctxt->reassem_chain , 
                  ORDERED_LIST_ASCENDING,
                  ORDERED_LIST_PUSH_LTE 
                  );

  reassem_ctxt->reassem_cnt = 0;
  reassem_ctxt->wr_cnt = 0;
  reassem_ctxt->frag_cnt = 0;
  reassem_ctxt->last_seq = 0;
  reassem_ctxt->tstamp = 0;
  reassem_ctxt->drop_cnt = 0;
  reassem_ctxt->first_seq = 0;
  reassem_ctxt->marker_bit = 0;
  reassem_ctxt->no_frags_used = 0;
  reassem_ctxt->max_frags_length = QVP_RTP_MAX_NO_OF_VIDEO_FRAGS;

  return( (qvp_rtp_reassem_hdl)reassem_ctxt );

} /* end of qvp_rtp_create_reassem_ctx */

/*===========================================================================

FUNCTION QVP_RTP_CLOSE_REASSEM_CTX 


DESCRIPTION
  Closes a reassembly context, deallocates memory for the context

DEPENDENCIES
  None

ARGUMENTS IN
 reassem_hdl - to be closed 

RETURN VALUE
  none

SIDE EFFECTS
  Deallocates memory for reassem_hdl

===========================================================================*/
LOCAL void qvp_rtp_close_reassem_ctx
(
  qvp_rtp_reassem_hdl        reassem_hdl
)
{
  qvp_rtp_reassem_ctx_type* reassem_ctxt = 
                           (qvp_rtp_reassem_ctx_type*)reassem_hdl;
/*------------------------------------------------------------------------*/
  

  /*------------------------------------------------------------------------
    Free memory
  ------------------------------------------------------------------------*/
  if( reassem_ctxt )
  {
    qvp_rtp_abort_reassembly( reassem_hdl );

    if( reassem_ctxt->frag_list != NULL )
    {
      qvp_rtp_free( reassem_ctxt->frag_list );
      reassem_ctxt->frag_list = NULL;
    }

    qvp_rtp_free( reassem_ctxt );
    reassem_ctxt = NULL;
  }

  QVP_RTP_MSG_LOW_0( "Closing reassembly context");
  return;

} /* end of qvp_rtp_close_reassem_ctx */

/*===========================================================================

FUNCTION QVP_RTP_INPUT_FRAG_IN_REASSEMBLY 


DESCRIPTION
  This function adds a RTP packet, a fragment of a frame to the 
  reassembly chain. Packets with the same timestamp belong to 
  same frame. The reassebly context maintains the list of fragments
  of a frame and regroups them on reaching a indication of end of
  frame( marker bit )

DEPENDENCIES
  1. Reassembly context should be initialised

  2. While reassembling a frame, a packet with different timestamp 
     not equal to timestamp in Reassem Ctxt should not be inserted.

ARGUMENTS IN
  reassem_hdl  - Reassembly context to which the fragment belongs
  rtp_frag     - Fragment to be input

RETURN VALUE
  status       - success/ or appr error code


SIDE EFFECTS
  None.

===========================================================================*/
qvp_rtp_status_type qvp_rtp_input_frag_in_reassembly
(
  qvp_rtp_reassem_hdl        reassem_hdl,
  qvp_rtp_buf_type*          rtp_frag
)
{
  qvp_rtp_reassem_ctx_type* reassem_ctx = 
                           (qvp_rtp_reassem_ctx_type*)reassem_hdl;
  qvp_rtp_fragmt_type *frag_to_add = NULL; 
/*------------------------------------------------------------------------*/
  if( !reassem_ctx || !rtp_frag )
  {
    QVP_RTP_ERR( "qvp_rtp_input_frag_in_reassembly : Null params ", 0, 0, 0 );
    return( QVP_RTP_WRONG_PARAM );
  }

  /*------------------------------------------------------------------------
    Bail out if the fragment does not belong to current reassembly context
  ------------------------------------------------------------------------*/
  if( reassem_ctx->reassem_cnt && 
      rtp_frag->tstamp != reassem_ctx->tstamp )
  {
    QVP_RTP_ERR( "Inserting frag %d in a different frame tstamp:%d, ", \
                  rtp_frag->tstamp, reassem_ctx->tstamp, 0 );

    if( rtp_frag->need_tofree )
    {
      qvp_rtp_free_buf( rtp_frag );
    }

    return( QVP_RTP_ERR_FATAL );

  }

  QVP_RTP_MSG_LOW( "Insert pkt seq %d,ts %d,marker bit %d",\
                  rtp_frag->seq, rtp_frag->tstamp, rtp_frag->marker_bit );

  QVP_RTP_MSG_LOW( "In reassembly len %d, last seq %d, drop_cnt %d",\
   reassem_ctx->reassem_cnt, reassem_ctx->last_seq, reassem_ctx->drop_cnt );

  /*------------------------------------------------------------------------
    adjust last sequence number 
  ------------------------------------------------------------------------*/
  if( rtp_frag->seq > reassem_ctx->last_seq )
  {
    reassem_ctx->last_seq = rtp_frag->seq; 
  }

  /*------------------------------------------------------------------------
    Set marker bit
  ------------------------------------------------------------------------*/
  if( rtp_frag->marker_bit )
  {
    reassem_ctx->marker_bit = TRUE;
  }

  /*------------------------------------------------------------------------
     check for duplicate fragments 
  ------------------------------------------------------------------------*/
  if( ordered_list_linear_search( &reassem_ctx->reassem_chain,
                                  qvp_rtp_compare_fgmt, (void *)rtp_frag->seq 
                                                                ) != NULL )
  {
    QVP_RTP_MSG_HIGH_2( "Duplicate pkt recvd pkt SEQ: %d, TS: %d",\
                       rtp_frag->seq, rtp_frag->tstamp, 0 );

    if( rtp_frag->need_tofree )
    {
      qvp_rtp_free_buf( rtp_frag );
      return( QVP_RTP_SUCCESS );
    }
    
    /*----------------------------------------------------------------------
      update the statistics counter
    ----------------------------------------------------------------------*/
    reassem_ctx->drop_cnt++;
    
    return( QVP_RTP_SUCCESS );
  }
  
  /*------------------------------------------------------------------------
     We are talking a real fragment here so we need to len check 
  ------------------------------------------------------------------------*/
  if( ( rtp_frag->len + reassem_ctx->reassem_cnt > 
         QVP_RTP_PKT_VIDEO_SIZE ) ||
      ( reassem_ctx->reassem_chain.size + 1  >= 
        QVP_RTP_MAX_NO_OF_VIDEO_FRAGS ) ||
      ( reassem_ctx->no_frags_used + 1 >= 
                        reassem_ctx->max_frags_length ) )
  {

    QVP_RTP_MSG_HIGH( "Frame overflow reasmlen: %d, chain_size: %d,\
                       frag_cnt: %d ", reassem_ctx->reassem_cnt, \
                        reassem_ctx->reassem_chain.size, \
                        reassem_ctx->no_frags_used );

    /*----------------------------------------------------------------------
      update the statistics counter
    ----------------------------------------------------------------------*/
    reassem_ctx->drop_cnt++;

    /*----------------------------------------------------------------------
        Free the packet and return
    ----------------------------------------------------------------------*/
    if( rtp_frag->need_tofree )
    {
      qvp_rtp_free_buf( rtp_frag );
    }

    return( QVP_RTP_NORESOURCES );
  }

  /*------------------------------------------------------------------------
     get a free fragment container structure 
  ------------------------------------------------------------------------*/
  frag_to_add = &reassem_ctx->frag_list[reassem_ctx->no_frags_used++];

  if( !frag_to_add )
  {
    QVP_RTP_ERR( " Could not get new fragment container \r\n", 0, 0, 0 );

    /*----------------------------------------------------------------------
        Free the packet and return
    ----------------------------------------------------------------------*/
    if( rtp_frag->need_tofree )
    {
      qvp_rtp_free_buf( rtp_frag );
    }

    /*----------------------------------------------------------------------
      update the statistics counter
    ----------------------------------------------------------------------*/
    reassem_ctx->drop_cnt++;

    return( QVP_RTP_NORESOURCES );

  }

  /*------------------------------------------------------------------------
     This fragment should be chained to packet 
  ------------------------------------------------------------------------*/
  frag_to_add->fragment = rtp_frag;  


  /*------------------------------------------------------------------------
       push the current segment to list 
  ------------------------------------------------------------------------*/
  ordered_list_push( &reassem_ctx->reassem_chain, 
                      (ordered_list_link_type * ) frag_to_add, 
                     rtp_frag->seq );

  /*------------------------------------------------------------------------
     increment the reassembly count. 
  ------------------------------------------------------------------------*/
  reassem_ctx->reassem_cnt += rtp_frag->len;
  reassem_ctx->tstamp = rtp_frag->tstamp;
  reassem_ctx->frag_cnt++;

  /*------------------------------------------------------------------------
    Once in a blue moon RTP Seq no will wrap around. what if we happen
    to be reassembling on that moment. ..?

    We need to split list gather. 


    The logic is a bit convoluted.

    If this sequence no is less than previous one then there is a chance 
    that we are wrapping around.

    Now when you wrap around the difference is a huge number. 
    If you hit hat kind of difference We go into wrap around mode.

    Second else case if after wrap around your seq will be low. Any thing
    more than the possible chain size is a pretty good estimate.
    
  ------------------------------------------------------------------------*/
  if( ( reassem_ctx->last_seq  ) && 
      ( !reassem_ctx->wr_cnt ) &&
      ( ( ( rtp_frag->seq < reassem_ctx->last_seq ) &&
          ( ( reassem_ctx->last_seq - rtp_frag->seq ) >
                           QVP_RTP_POOL_NW_SIZE * 10 )  )  ) )
  {
    /*----------------------------------------------------------------------
         We just wrapped around so start adding code for wrap around 
         counter 
    ----------------------------------------------------------------------*/
    QVP_RTP_MSG_HIGH_2( "SeqNo new wrap around pkt SEQ: %d, TS: %d", 
                       rtp_frag->seq, rtp_frag->tstamp, 0 );


    /*----------------------------------------------------------------------
       We use wr_cnt only when wrap around occurs in between fragments
       of a frame. 
       Ex: A Frame [65534, 65535, 0, 1, 2] 

       Check if this is the first packet of a new frame reassembly 
       in which case we dont need to treat this reassembly as a special
       case, it can be reassembled normally. 
       Ex: Frame 1 - [65534, 65535];  Frame 2 - [0, 1, 2]
       
    ----------------------------------------------------------------------*/
    if( reassem_ctx->reassem_cnt != rtp_frag->len )
    {
      reassem_ctx->wr_cnt = rtp_frag->len;
    }

    reassem_ctx->last_seq = rtp_frag->seq;

  }
  else if(  reassem_ctx->wr_cnt && 
            ( rtp_frag->seq < QVP_RTP_POOL_NW_SIZE  ) )
  {
    QVP_RTP_MSG_HIGH_2( "SeqNo new wrap around pkt SEQ: %d, TS: %d",\
                       rtp_frag->seq, rtp_frag->tstamp, 0 );

    reassem_ctx->wr_cnt += rtp_frag->len;

  }
  
  /*------------------------------------------------------------------------
    Update least sequence number - seqno of the first packet in frame
  ------------------------------------------------------------------------*/
  if( ( !reassem_ctx->first_seq ) ||
      ( ( !reassem_ctx->wr_cnt ) && 
       ( rtp_frag->seq < reassem_ctx->first_seq ) ) )
  {
    reassem_ctx->first_seq = rtp_frag->seq;
  }

  
  QVP_RTP_MSG_LOW( "In reassembly len %d, last seq %d, drop_cnt %d",\
        reassem_ctx->reassem_cnt, reassem_ctx->last_seq, \
        reassem_ctx->drop_cnt );

  return( QVP_RTP_SUCCESS );

} /* end of function qvp_rtp_input_frag_in_reassembly */

/*===========================================================================

FUNCTION QVP_RTP_GET_REASSEMBLED_FRAME


DESCRIPTION
  This function regroups all the fragments in the reassembly chain to form 
  a complete frame. Packets with the same timestamp belong to 
  same frame. The reassebly context maintains the list of fragments
  of a frame and regroups them on reaching a indication of end of
  frame( marker bit ). 
  It also checks for any missing pkts in between a frame and inserts 
  a error concealment pattern if supported. 

DEPENDENCIES
  None

ARGUMENTS IN
  reassem_ctxt - Reassembly context to be regrouped
  rtp_frame    - RTP buffer given by app
  last_seq_sent - Seq num of the last RTP packet sent in prev frame
                  used to check if there are missing pkts in starting
  

RETURN VALUE
  status       - success/ or appr error code


SIDE EFFECTS
  rtp_frame    - RTP buffer which will be filled with frame data

===========================================================================*/
qvp_rtp_status_type qvp_rtp_get_reassembled_frame
(
  qvp_rtp_reassem_hdl        reassem_hdl,
  qvp_rtp_buf_type*          rtp_frame,
  uint32                     max_frame_length,
  uint16                     last_seq_sent
)
{
  qvp_rtp_reassem_ctx_type* reassem_ctxt = 
                           (qvp_rtp_reassem_ctx_type*)reassem_hdl;
  qvp_rtp_status_type     status = QVP_RTP_SUCCESS;
/*------------------------------------------------------------------------*/

  if( !reassem_ctxt || !rtp_frame || 
      ( max_frame_length < reassem_ctxt->reassem_cnt ) )
  {
    QVP_RTP_ERR( "qvp_rtp_get_reassembled_frame : Null params ", 0, 0, 0 );
    return( QVP_RTP_WRONG_PARAM );
  }

  QVP_RTP_MSG_LOW( "Get Frame in reassembly ts %d, len %d, last seq %d ",\
        reassem_ctxt->tstamp, reassem_ctxt->reassem_cnt, \
        reassem_ctxt->last_seq );

  QVP_RTP_MSG_LOW( "Reassembly drop cnt %d, marker bit %d, first seq %d ",\
        reassem_ctxt->drop_cnt, reassem_ctxt->marker_bit, \
        reassem_ctxt->first_seq );

  rtp_frame->head_room = 0;
  rtp_frame->seq = reassem_ctxt->last_seq;
  rtp_frame->tstamp = reassem_ctxt->tstamp;
  rtp_frame->marker_bit = reassem_ctxt->marker_bit;
  rtp_frame->len = reassem_ctxt->reassem_cnt;

  if( reassem_ctxt->wr_cnt )
  {

    /*----------------------------------------------------------------------
        we need to do split reassembly 

        Assemble the wrapped around portion
        
        Then assemble the starting  portion

        ----------------------------------------------------
        |          |                                        |
        | wr_cnt   |   Starting portion                     |
        ----------------------------------------------------
    ----------------------------------------------------------------------*/
    status = qvp_rtp_regroup_segments ( reassem_ctxt,
                  &reassem_ctxt->reassem_chain,
                  rtp_frame->data + reassem_ctxt->reassem_cnt 
                  - reassem_ctxt->wr_cnt , 
                  &rtp_frame->len,
                  reassem_ctxt->wr_cnt, 
                  last_seq_sent );

    /*----------------------------------------------------------------------
       Bail out if error or if there is no starting portion left
    ----------------------------------------------------------------------*/
    if( ( status != QVP_RTP_SUCCESS ) || 
        ( ( reassem_ctxt->reassem_cnt - 
                  reassem_ctxt->wr_cnt ) == 0 ) )
    {
      status = QVP_RTP_ERR_FATAL;
    }

    /*----------------------------------------------------------------------
        assemble the starting portion
    ----------------------------------------------------------------------*/
    if( status == QVP_RTP_SUCCESS )
    {
      status = qvp_rtp_regroup_segments ( reassem_ctxt,
                  &reassem_ctxt->reassem_chain,
                  rtp_frame->data,
                  &rtp_frame->len,  
                  reassem_ctxt->reassem_cnt - 
                  reassem_ctxt->wr_cnt,
                  last_seq_sent );
    } 

  } /* end of if wrap around occured  */
  else
  {
    status = qvp_rtp_regroup_segments ( reassem_ctxt,
                &reassem_ctxt->reassem_chain,
                rtp_frame->data, &rtp_frame->len,
                reassem_ctxt->reassem_cnt,
                last_seq_sent );
  }

  if( status != QVP_RTP_SUCCESS )
  {
    QVP_RTP_ERR( " COuld not reassemble the fragments ", 0, 0, 0 );
    return( status );
  }
  
  QVP_RTP_MSG_HIGH( "Reassembled Frame len = %d, tstamp = %d, seq = %d", 
                      rtp_frame->len, rtp_frame->tstamp, rtp_frame->seq );

  return( QVP_RTP_SUCCESS );

} /* end of function qvp_rtp_get_reassembled_frame */


/*===========================================================================

FUNCTION  QVP_RTP_REGROUP_SEGMENTS 


DESCRIPTION
   This function will reassmeble the length of reasembly passed and by
   gathering the fragments from the reassembly list.

DEPENDENCIES
  None

ARGUMENTS IN
  reassem_chain - the chain from which the reassembly should occure
  len           - length of data which needs to be reassembled
  last_seq_no   - last seq number sent 
  
ARGUMENTS OUT
  reassem_data - The data which will get reassembled
  reassem_len - length of reassembled data


RETURN VALUE
  True on success FALSE on failure.

SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_regroup_segments 
( 
  qvp_rtp_reassem_ctx_type* reassem_ctxt,
  ordered_list_type        *reassem_chain,
  uint8                    *reassem_data,
  uint16                   *reassem_len,
  uint16                   len,
  uint16                   last_seq_no
)
{
  qvp_rtp_fragmt_type *buf = NULL;
/*------------------------------------------------------------------------*/

  if( !reassem_chain || !reassem_data || len == 0 )
  {
    QVP_RTP_ERR( "qvp_rtp_regroup_segments: Invalid params", 
                       0, 0, 0 );
    return( QVP_RTP_ERR_FATAL );
  }

  /*------------------------------------------------------------------------
    Iteratively pop items out and copy it to the larger buffer 
  ------------------------------------------------------------------------*/
  while( ( len && 
         ( buf = ordered_list_pop_front( reassem_chain ) ) 
                  != NULL ) 
         ) 
  {
    
    if ( len > buf->fragment->len )
    {
      len -=   buf->fragment->len;
    }
    else
    {
      len = 0;
    }


    qvp_rtp_memscpy( reassem_data, buf->fragment->len,
            buf->fragment->data + buf->fragment->head_room, 
            buf->fragment->len );

    /*----------------------------------------------------------------------
       increment the data pointer 
    ----------------------------------------------------------------------*/
    reassem_data += buf->fragment->len;

    /*----------------------------------------------------------------------
       If any missing pkt insert bitpattern
    ----------------------------------------------------------------------*/
    /*----------------------------------------------------------------------
      Limitation using this logic for inserting bit pattern - 
      A bit pattern is inserted even for a complete frame if previous
      frame's last pkts are missing.
    ----------------------------------------------------------------------*/
    if((  buf->fragment->seq != last_seq_no ) &&  /* !(very first seq no) */
         ( buf->fragment->seq != ((uint16)((last_seq_no) + 1) ) ) ) 
    {
      QVP_RTP_ERR( "Missing pkt:errcncl at tstamp = %d, seq = %d",
                            buf->fragment->tstamp, last_seq_no, 0 );

#ifdef QVP_RTP_ERROR_CONCEAL_PATTERN
      /*------------------------------------------------------------------
        Insert bit pattern and increment the length of the data buffer
      ------------------------------------------------------------------*/
      reassem_data[0] = QVP_RTP_ERR_CONCEAL_BYTE_1;
      reassem_data[1] = QVP_RTP_ERR_CONCEAL_BYTE_2;
      reassem_data[2] = QVP_RTP_ERR_CONCEAL_BYTE_3;
      reassem_data[3] = QVP_RTP_ERR_CONCEAL_BYTE_4;

      reassem_data += QVP_RTP_ERR_CONCL_PATTERN_LENGTH;
      *reassem_len += QVP_RTP_ERR_CONCL_PATTERN_LENGTH;
#endif

    }
      
    last_seq_no = buf->fragment->seq;

    /*----------------------------------------------------------------------
      put the memory back in the pool 
    ----------------------------------------------------------------------*/
    qvp_rtp_free_buf( buf->fragment );

    /*----------------------------------------------------------------------
       flag bad fragment 
    ----------------------------------------------------------------------*/
    buf->fragment = NULL;

    /*----------------------------------------------------------------------
      put the container strucutre back to the free list 
    ----------------------------------------------------------------------*/
    reassem_ctxt->no_frags_used--;

  } /* end of while */

  reassem_ctxt->last_seq = last_seq_no;

  if( !reassem_ctxt->marker_bit )
  {
     QVP_RTP_ERR( "Last pkt missing in frame TS = %d, seq = %d",
                            reassem_ctxt->tstamp, last_seq_no, 0 );
  }

   /*-----------------------------------------------------------------------
             We should not have any remaining len here
   -----------------------------------------------------------------------*/
   if( len )
   {
     QVP_RTP_ERR( " Couldn't reassemble fragments:remaining len %d ",
                            len, 0, 0 );
  
     return( QVP_RTP_ERR_FATAL );
   }
   else
   {
     return( QVP_RTP_SUCCESS );
   }

} /* end of function qvp_rtp_regroup_segments */

/*===========================================================================

FUNCTION QVP_RTP_RESET_REASSEM_CTX 


DESCRIPTION
  Any time this function can be called to reset the context of reassmbly.
  This function does not free the memory associated.

DEPENDENCIES
  None

ARGUMENTS IN
 stream - the stream for which the reassmbly context should be reset 
 
ARGUMENTS OUT
  None
  

RETURN VALUE
  True on success FALSE on failure.

SIDE EFFECTS
  None.

===========================================================================*/
LOCAL void qvp_rtp_reset_reassem_ctx
( 
  qvp_rtp_reassem_hdl        reassem_hdl
)
{
  qvp_rtp_reassem_ctx_type* reassem_ctxt = 
                           (qvp_rtp_reassem_ctx_type*)reassem_hdl;
/*------------------------------------------------------------------------*/
  if( !reassem_ctxt )
  {
    QVP_RTP_ERR( "qvp_rtp_reset_reassem_ctx: Invalid params", 
                       0, 0, 0 );
    return;
  }

  /*------------------------------------------------------------------------
     reset counts and state. Someone should free the chain. 
  ------------------------------------------------------------------------*/
  reassem_ctxt->reassem_cnt = 0;
  reassem_ctxt->wr_cnt = 0;
  reassem_ctxt->drop_cnt = 0;
  reassem_ctxt->first_seq = 0;
  reassem_ctxt->last_seq = 0;
  reassem_ctxt->marker_bit = 0;
  reassem_ctxt->tstamp = 0;
  reassem_ctxt->no_frags_used = 0;
  reassem_ctxt->frag_cnt = 0;

}/* end of function qvp_rtp_reset_reassem_ctx */


/*===========================================================================

FUNCTION  QVP_RTP_ABORT_REASSEMBLY 


DESCRIPTION
  Bail out function in the middle of a reassmbly. Walks through the
  reassembly chain and frees the buffers one at a time.

DEPENDENCIES
  None

ARGUMENTS IN
  The stream for which the reassembly should be aborted
  
ARGUMENTS OUT
  None
  

RETURN VALUE
  True on success FALSE on failure.

SIDE EFFECTS
  None.

===========================================================================*/
LOCAL void qvp_rtp_abort_reassembly
( 
  qvp_rtp_reassem_hdl        reassem_hdl 
)
{
  qvp_rtp_fragmt_type *buf = NULL;
  qvp_rtp_reassem_ctx_type* reassem_ctxt = 
                           (qvp_rtp_reassem_ctx_type*)reassem_hdl;
/*------------------------------------------------------------------------*/

  if( !reassem_ctxt )
  {
    QVP_RTP_ERR( "qvp_rtp_abort_reassembly: Invalid params", 
                       0, 0, 0 );
    return;
  }

  QVP_RTP_MSG_LOW_0( "Abort reassembly context");

  /*------------------------------------------------------------------------
    Iteratively pop items and put it back to free list 
  -----------------------------------------------------------------------*/
  while( ( buf = ( qvp_rtp_fragmt_type * ) ordered_list_pop_front( 
                    &reassem_ctxt->reassem_chain ) )!= NULL ) 
  {
    

    /*----------------------------------------------------------------------
      put the memory back in the pool 
    ----------------------------------------------------------------------*/
    qvp_rtp_free_buf( buf->fragment );

    /*----------------------------------------------------------------------
       flag bad memory 
    ----------------------------------------------------------------------*/
    buf->fragment = NULL;

    reassem_ctxt->no_frags_used--;

  } /* end of while */

  if( reassem_ctxt->no_frags_used )
  {
    QVP_RTP_ERR( "qvp_rtp_abort_reassembly: Error in clearning bufs ", 
                       0, 0, 0 );
    reassem_ctxt->no_frags_used = 0;
  }

   /*------------------------------------------------------------------------
      reset counts and state. 
   ------------------------------------------------------------------------*/
   qvp_rtp_reset_reassem_ctx( reassem_hdl);

  return;

} /* end of function qvp_rtp_abort_reassembly */

/*===========================================================================

FUNCTION  QVP_RTP_COMPARE_FGMT 


DESCRIPTION
  This function will be passed to the list manipulation APIs to detect 
  duplicate segments. The sequence no of the each stored 
  segment is compared against the sequence number of the incoming packet.

DEPENDENCIES
  None

ARGUMENTS IN
  item   - pointer to list item
  seq_no - sequence number of incoming fragment
  
ARGUMENTS OUT
  None
  

RETURN VALUE
  True on success FALSE on failure.

SIDE EFFECTS
  None.

===========================================================================*/
LOCAL int qvp_rtp_compare_fgmt
( 
  void *item, 
  void *seq_no
)
{
  qvp_rtp_fragmt_type *fragment = ( qvp_rtp_fragmt_type *) item;
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
     see if we have a duplicate fragment here  
  ------------------------------------------------------------------------*/
  if( ( fragment->fragment ) &&
      ( fragment->fragment->seq == ( uint32 ) seq_no ) )
  {
    return( TRUE );
  }
  else
  {
    return( FALSE );
  }

} /* end of function qvp_rtp_compare_fgmt */

/*===========================================================================

FUNCTION  QVP_RTP_GET_REASM_MISSING_CNT


DESCRIPTION
  Obtains number of missing pkts in a reassembly/frame  

DEPENDENCIES
  None

ARGUMENTS IN
  reassem_hdl - reassembly context handle ( frame )
  

RETURN VALUE
  uint32

SIDE EFFECTS
  None.

===========================================================================*/
LOCAL uint32 qvp_rtp_get_reasm_missng_cnt
( 
  qvp_rtp_reassem_hdl        reassem_hdl
)
{
  qvp_rtp_fragmt_type *buf = NULL;
  qvp_rtp_fragmt_type *buf_next = NULL;
  qvp_rtp_reassem_ctx_type* reassem_ctxt = 
                           (qvp_rtp_reassem_ctx_type*)reassem_hdl;
  uint32 miss_cnt = 0;
/*------------------------------------------------------------------------*/

  if( !reassem_ctxt )
  {
    QVP_RTP_ERR( "qvp_rtp_get_missing_cnt_in_reassem: Invalid params", 
                       0, 0, 0 );
    return 0;
  }
  /*------------------------------------------------------------------------
    get first fragment/rtp pkt of the reassembly/frame
  ------------------------------------------------------------------------*/
  buf = ordered_list_peek_front( &reassem_ctxt->reassem_chain );

  /*------------------------------------------------------------------------
    Traverse the list of fragments to check for seq nums missing
  ------------------------------------------------------------------------*/
  while( buf )
  {
    buf_next = ordered_list_peek_next( &reassem_ctxt->reassem_chain, 
                                       (ordered_list_link_type*)buf );
    if( buf_next )
    {
      if( buf_next->fragment->seq != ((uint16)(buf->fragment->seq + 1) ) )
      {
        miss_cnt += 
          ( (uint16)( buf_next->fragment->seq - buf->fragment->seq ) );
      }
    }
    buf = buf_next;
  }

  QVP_RTP_MSG_LOW_1( "Reassembly context missing pkt cnt %d ",\
                            miss_cnt, 0, 0 );
  if( miss_cnt )
  {
      QVP_RTP_ERR( "Video pkts %d missing at seq %d at tstamp %d",\
               miss_cnt, reassem_ctxt->first_seq, reassem_ctxt->tstamp );
  }

  return( miss_cnt );

} /* end of function qvp_rtp_get_reasm_missng_cnt */



/*===========================================================================

FUNCTION  QVP_RTP_GET_REASM_MARKER_BIT


DESCRIPTION
  Indicates if the pkt with marker bit has been received for this
  frame

DEPENDENCIES
  None

ARGUMENTS IN
  reassem_hdl - reassembly context handle ( frame )
  

RETURN VALUE
  uint32

SIDE EFFECTS
  None.

===========================================================================*/
LOCAL boolean qvp_rtp_get_reasm_marker_bit
( 
  qvp_rtp_reassem_hdl        reassem_hdl 
)
{
  qvp_rtp_reassem_ctx_type* reassem_ctxt = 
                           (qvp_rtp_reassem_ctx_type*)reassem_hdl;
/*------------------------------------------------------------------------*/
  if( !reassem_ctxt )
  {
    QVP_RTP_ERR( "qvp_rtp_get_reasm_marker_bit: Invalid params", 
                       0, 0, 0 );
    return FALSE;
  }

  QVP_RTP_MSG_LOW_1( "Reassembly context marker bit %d ",\
                            reassem_ctxt->marker_bit, 0, 0 );

  return( reassem_ctxt->marker_bit );

} /* end of function qvp_rtp_get_reasm_marker_bit */

/*===========================================================================

FUNCTION  QVP_RTP_GET_REASM_FIRST_SEQ


DESCRIPTION
  Obtains seq number of first pkt in reassembly/frame

DEPENDENCIES
  None

ARGUMENTS IN
  reassem_hdl - reassembly context handle ( frame )
  

RETURN VALUE
  uint16

SIDE EFFECTS
  None.

===========================================================================*/
LOCAL uint16 qvp_rtp_get_reasm_first_seq
( 
  qvp_rtp_reassem_hdl        reassem_hdl 
)
{
  qvp_rtp_reassem_ctx_type* reassem_ctxt = 
                           (qvp_rtp_reassem_ctx_type*)reassem_hdl;
/*------------------------------------------------------------------------*/
  if( !reassem_ctxt )
  {
    QVP_RTP_ERR( "qvp_rtp_get_reasm_first_seq: Invalid params", 
                       0, 0, 0 );
    return 0;
  }
  
  QVP_RTP_MSG_LOW_1( "Reassembly context first seq no %d ",\
                            reassem_ctxt->first_seq, 0, 0 );

  return( reassem_ctxt->first_seq );

} /* end of func qvp_rtp_get_reasm_first_seq */

/*===========================================================================

FUNCTION  QVP_RTP_GET_REASM_LAST_SEQ


DESCRIPTION
  Obtains seq number of last pkt in reassembly/frame

DEPENDENCIES
  None

ARGUMENTS IN
  reassem_hdl - reassembly context handle ( frame )
  

RETURN VALUE
  uint16

SIDE EFFECTS
  None.

===========================================================================*/
LOCAL uint16 qvp_rtp_get_reasm_last_seq
( 
  qvp_rtp_reassem_hdl        reassem_hdl 
)
{
  qvp_rtp_reassem_ctx_type* reassem_ctxt = 
                           (qvp_rtp_reassem_ctx_type*)reassem_hdl;
/*------------------------------------------------------------------------*/
  if( !reassem_ctxt )
  {
    QVP_RTP_ERR( "qvp_rtp_get_reasm_last_seq: Invalid params", 
                       0, 0, 0 );
    return 0;
  }

  QVP_RTP_MSG_LOW_1( "Reassembly context last seq no %d ",\
                            reassem_ctxt->last_seq, 0, 0 );

  return( reassem_ctxt->last_seq );

} /* end of qvp_rtp_get_reasm_last_seq */

/*===========================================================================

FUNCTION  QVP_RTP_GET_REASM_DROP_CNT


DESCRIPTION
  Obtains number of missing pkts in the frame  

DEPENDENCIES
  None

ARGUMENTS IN
  reassem_hdl - reassembly context handle ( frame )
  

RETURN VALUE
  uint32

SIDE EFFECTS
  None.

===========================================================================*/
LOCAL uint32 qvp_rtp_get_reasm_drop_cnt
( 
  qvp_rtp_reassem_hdl        reassem_hdl 
)
{
  qvp_rtp_reassem_ctx_type* reassem_ctxt = 
                           (qvp_rtp_reassem_ctx_type*)reassem_hdl;
/*------------------------------------------------------------------------*/
  if( !reassem_ctxt )
  {
    QVP_RTP_ERR( "qvp_rtp_get_reasm_drop_cnt: Invalid params", 
                       0, 0, 0 );
    return 0;
  }

  QVP_RTP_MSG_LOW_1( "Reassembly context drop cnt %d ",\
                            reassem_ctxt->drop_cnt, 0, 0 );

  return( reassem_ctxt->drop_cnt );

} /* end of func qvp_rtp_get_reasm_drop_cnt */


/*===========================================================================

FUNCTION  QVP_RTP_GET_REASM_TIMESTAMP


DESCRIPTION
  Obtains the RTP timestamp of the frame being reassembled

DEPENDENCIES
  None

ARGUMENTS IN
  reassem_hdl - reassembly context handle
  

RETURN VALUE
  uint32

SIDE EFFECTS
  None.

===========================================================================*/
LOCAL uint32 qvp_rtp_get_reasm_timestamp
( 
  qvp_rtp_reassem_hdl        reassem_hdl 
)
{
  qvp_rtp_reassem_ctx_type* reassem_ctxt = 
                           (qvp_rtp_reassem_ctx_type*)reassem_hdl;
/*------------------------------------------------------------------------*/
  if( !reassem_ctxt )
  {
    QVP_RTP_ERR( "qvp_rtp_get_reasm_timestamp: Invalid params", 
                       0, 0, 0 );
    return 0;
  }

  QVP_RTP_MSG_LOW_1( "Reassembly context timestamp %d ",\
                            reassem_ctxt->tstamp, 0, 0 );

  return( reassem_ctxt->tstamp );

} /* end of func qvp_rtp_get_reasm_timestamp */

/*===========================================================================

FUNCTION  QVP_RTP_GET_REASM_FRAG_CNT


DESCRIPTION
  Obtains the number of fragments in the reassembly/frame

DEPENDENCIES
  None

ARGUMENTS IN
  reassem_hdl - reassembly context handle
  

RETURN VALUE
  uint32

SIDE EFFECTS
  None.

===========================================================================*/
LOCAL uint32 qvp_rtp_get_reasm_frag_cnt
( 
  qvp_rtp_reassem_hdl        reassem_hdl 
)
{
    qvp_rtp_reassem_ctx_type* reassem_ctxt = 
                           (qvp_rtp_reassem_ctx_type*)reassem_hdl;
/*------------------------------------------------------------------------*/
  if( !reassem_ctxt )
  {
    QVP_RTP_ERR( "qvp_rtp_get_reasm_frag_cnt: Invalid params", 
                       0, 0, 0 );
    return 0;
  }

  QVP_RTP_MSG_LOW_1( "Reassembly context fragment count %d ",\
                  reassem_ctxt->frag_cnt, 0, 0 );

  return( reassem_ctxt->frag_cnt );

} /* end of func qvp_rtp_get_reasm_frag_cnt */

/*===========================================================================

FUNCTION  QVP_RTP_GET_REASM_FRAME_LENGTH


DESCRIPTION
  Obtains the frame length of the currrent reassembly

DEPENDENCIES
  None

ARGUMENTS IN
  reassem_hdl - reassembly context handle
  

RETURN VALUE
  uint32

SIDE EFFECTS
  None.

===========================================================================*/
LOCAL uint32 qvp_rtp_get_reasm_frame_length
( 
  qvp_rtp_reassem_hdl        reassem_hdl 
)
{
    qvp_rtp_reassem_ctx_type* reassem_ctxt = 
                           (qvp_rtp_reassem_ctx_type*)reassem_hdl;
/*------------------------------------------------------------------------*/
  if( !reassem_ctxt )
  {
    QVP_RTP_ERR( "qvp_rtp_get_reasm_frame_length: Invalid params", 
                       0, 0, 0 );
    return 0;
  }

  QVP_RTP_MSG_LOW_1( "Reassembly context frame length %d ",\
                            reassem_ctxt->reassem_cnt, 0, 0 );

  return( reassem_ctxt->reassem_cnt );

} /* end of func qvp_rtp_get_reasm_frame_length */


#endif /* For FEATURE_QVPHONE_RTP */
