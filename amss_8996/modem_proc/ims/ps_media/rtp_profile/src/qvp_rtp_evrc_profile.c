/*=*====*====*====*====*====*====*===*====*====*====*====*====*====*====*====*


                         QVP_RTP_EVRC_PROFILE . C

GENERAL DESCRIPTION

  This file contains the implementation of EVRC profile. EVRC profile acts 
  as conduite inside RTP layer. The RFC which is based on is RFC3558.

EXTERNALIZED FUNCTIONS
  None.


INITIALIZATION AND SEQUENCING REQUIREMENTS

  Need to init and configure the profile before it becomes usable.


  Copyright (c) 2004 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
  QUALCOMM Proprietary.  Export of this technology or software is
  regulated by the U.S. Government. Diversion contrary to U.S. law prohibited.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*=*/

/*===========================================================================

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/ims/ps_media/rtp_profile/src/qvp_rtp_evrc_profile.c#1 $ $DateTime: 2016/03/28 23:03:22 $ $Author: mplcsds1 $

                            EDIT HISTORY FOR FILE


when        who    what, where, why
--------    ---    ----------------------------------------------------------
26/06/09    sha    Changed the validate function to check pyld and Chdir only when valid
12/04/08    mc     Fixing RVCT compile warnings for 7K
08/01/08    grk    Updated frm_present flag for received frames.
04/10/08    apr    Fixed compiler warnings
01/14/08    apr    Fixes in Configure EVRCB, separate EVRC and EVRCB
12/24/07    apr    Support for EVRC-WB
12/31/07    grk    Moving maxptime/ptime to media level.
10/18/07    rk     QDJ integration. Marking silence frames
09/14/06    apr    Added support for DTX, 
                   Fix in packing blank frame TOC, 
                   Fix in config maxptime 20
07/13/06    uma    Fix for maxinterleave_valid in
                   qvp_rtp_evrc_profile_copy_config
07/13/06    uma    Added new functions for validating payload configuration
06/21/06    uma    Modified prototype of qvp_rtp_evrc_profile_copy_config
                   and name of qvp_rtp_evrc_sdp_config_type
                   to support EVRCB
06/09/06    apr    RTP payload type number is copied into the network
                   buffer, as part of fix for CR 95499
05/05/06    apr    Including RTP code in feature FEATURE_QVPHONE_RTP
03/20/06    uma    Replaced all malloc and free with wrappers 
                   qvp_rtp_malloc & qvp_rtp_free 
09/09/05    srk    Started adjusting our bundle size with the maxptime in 
                   config. Error check on maxptime config
08/24/05    srk    Started using generic buffer pool in tx path as well.
06/14/05    srk    Started failing when interleave len 
                   is not specified.
05/20/05    srk    Added EVRC configure API
03/18/05    srk    Removed RV type header. Added 
                   stdlib.
11/17/04    srk    Initial Creation.
===========================================================================*/
#include "ims_variation.h"
#include "customer.h"
#ifdef FEATURE_QVPHONE_RTP

#ifdef  RTP_FILE_NUMBER 
  #undef RTP_FILE_NUMBER 
#endif 
#define RTP_FILE_NUMBER 22

/*===========================================================================
                     INCLUDE FILES FOR MODULE
===========================================================================*/
#include <comdef.h>               /* common target/c-sim defines */
#include <string.h>               /* memory routines */
#include <stdlib.h>               /* for malloc */
#include "qvp_rtp_api.h"          /* return type propagation */
#include "qvp_rtp_profile.h"      /* profile template data type */
#include "qvp_rtp_msg.h"
#include "qvp_rtp_log.h"
#include "qvp_rtp_evrc_profile.h" /* profile template data type */
#include <bit.h>                  /* for bit parsing/manipulation */


/*===========================================================================
                     LOCAL STATIC FUNCTIONS 
===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_evrc_profile_init
(
  qvp_rtp_profile_config_type *config
);

LOCAL qvp_rtp_status_type qvp_rtp_evrc_profile_open
(
  qvp_rtp_profile_hdl_type     *hdl,         /* handle which will get 
                                              * populated with a new profile
                                              * stream handle
                                              */
  qvp_rtp_profile_usr_hdl_type  usr_hdl     /* user handle for any cb */
);

LOCAL qvp_rtp_status_type qvp_rtp_evrcwb_profile_open
(
  qvp_rtp_profile_hdl_type     *hdl,         /* handle which will get 
                                              * populated with a new profile
                                              * stream handle
                                              */
  qvp_rtp_profile_usr_hdl_type  usr_hdl     /* user handle for any cb */
);

LOCAL qvp_rtp_status_type qvp_rtp_evrc_profile_read_deflt_config
(
  
  qvp_rtp_payload_type        payld, /* not used */ 
  qvp_rtp_payload_config_type *payld_config 
);

LOCAL qvp_rtp_status_type qvp_rtp_evrc_profile_validate_config_rx
(
  qvp_rtp_payload_config_type*  payld_config,
  qvp_rtp_payload_config_type*  counter_config 
);

LOCAL qvp_rtp_status_type qvp_rtp_evrc_profile_validate_config_tx
(
  qvp_rtp_payload_config_type*  payld_config,
  qvp_rtp_payload_config_type*  counter_config 
);


LOCAL qvp_rtp_status_type qvp_rtp_evrc_profile_configure
(
  qvp_rtp_profile_hdl_type      hdl,     
  qvp_rtp_payload_config_type   *payld_config 
);

LOCAL qvp_rtp_status_type qvp_rtp_evrcwb_profile_configure
(
  qvp_rtp_profile_hdl_type      hdl,     
  qvp_rtp_payload_config_type   *payld_config 
);

LOCAL qvp_rtp_status_type qvp_rtp_evrc_profile_send
(
  qvp_rtp_profile_hdl_type     hdl,          /* handle created while open */
  qvp_rtp_profile_usr_hdl_type usr_hdl,      /* user handle for tx cb */
  qvp_rtp_buf_type             *pkt          /* packet to be formatted 
                                              * through the profile
                                              */
);

LOCAL qvp_rtp_status_type qvp_rtp_evrc_profile_recv
(

  qvp_rtp_profile_hdl_type     hdl,          /* handle created while open */
  qvp_rtp_profile_usr_hdl_type usr_hdl,      /* user handle for rx cb */
  qvp_rtp_buf_type             *pkt          /* packet parsed and removed
                                              * header 
                                              */

);

LOCAL qvp_rtp_status_type qvp_rtp_evrc_profile_close
(

  qvp_rtp_profile_hdl_type     hdl           /* handle the profile */

);

LOCAL void qvp_rtp_evrc_profile_shutdown( void );

/*--------------------------------------------------------------------------
      FUNCTIONS APART FROM PROFILE TEMPLATE 
--------------------------------------------------------------------------*/
LOCAL void qvp_rtp_evrc_reset_tx_ctx
( 
  qvp_rtp_evrc_ctx_type *stream 
);

LOCAL void qvp_rtp_evrc_reset_stream
( 
  qvp_rtp_evrc_ctx_type *stream 
);
 
LOCAL uint8 qvp_rtp_evrc_form_header
( 
  qvp_rtp_evrc_ctx_type *stream 
);

LOCAL uint16 qvp_rtp_parse_evrc_fixed_hdr
(
  
  uint8 *data,
  uint16 len,
  qvp_rtp_evrc_hdr_param_type *hdr
  
);

LOCAL qvp_rtp_evrc_toc_type qvp_rtp_evrc_find_toc
( 
  qvp_rtp_evrc_ctx_type       *stream,  
  uint16 len
);

LOCAL qvp_rtp_status_type qvp_rtp_evrc_profile_validate_fmtp
(
  qvp_rtp_channel_type                    ch_dir,
  qvp_rtp_evrc_intl_bund_sdp_config_type*  evrc_config,
  qvp_rtp_evrc_intl_bund_sdp_config_type  *counter_evrc_config
);

LOCAL qvp_rtp_status_type qvp_rtp_evrc_profile_copy_config
(
  qvp_rtp_payload_type         payld,
  qvp_rtp_evrc_intl_bund_sdp_config_type  *evrc_tx_config,
  qvp_rtp_evrc_intl_bund_sdp_config_type  *evrc_rx_config,
  qvp_rtp_evrc_config_type    *evrc_config
);

LOCAL qvp_rtp_status_type qvp_rtp_evrc_profile_config_tx_param
(
  qvp_rtp_evrc_ctx_type       *stream,
  qvp_rtp_evrc_intl_bund_sdp_config_type  *evrc_tx_config
);

LOCAL qvp_rtp_status_type qvp_rtp_evrc_profile_config_rx_param
(
  qvp_rtp_evrc_ctx_type       *stream,
  qvp_rtp_evrc_intl_bund_sdp_config_type  *evrc_rx_config
);

LOCAL qvp_rtp_status_type qvp_rtp_evrc_config_pkt_itvl
(
  qvp_rtp_evrc_ctx_type *stream,     
  qvp_rtp_payload_config_type   *payld_config 
);

/*--------------------------------------------------------------------------
    Table of accessors for the profile. This need to be populated in
    the table entry at qvp_rtp_profole.c. All these functions will 
    automatically link to the RTP stack by doing so.
--------------------------------------------------------------------------*/
qvp_rtp_profile_type qvp_rtp_evrc_profile_table = 
{
  qvp_rtp_evrc_profile_init,              /* LOCAL initialization routine */
  qvp_rtp_evrc_profile_open,              /* LOCAL func to open channel   */
  qvp_rtp_evrc_profile_send,              /* LOCAL func to send a pkt     */
  qvp_rtp_evrc_profile_recv,              /* LOCAL func to rx a nw pkt    */
  qvp_rtp_evrc_profile_close,             /* LOCAL func to close channel  */
  qvp_rtp_evrc_profile_read_deflt_config, /* LOCAL func to read dflt confg*/
  NULL,
  qvp_rtp_evrc_profile_validate_config_rx,/* LOCAL func-validate rx conf  */
  qvp_rtp_evrc_profile_validate_config_tx,/* LOCAL func-validate tx conf  */
  qvp_rtp_evrc_profile_configure,         /* LOCAL func to config profile */
  qvp_rtp_evrc_profile_shutdown           /* LOCAL function to shut down  */
};

/*--------------------------------------------------------------------------
    Table of accessors for the profile. This need to be populated in
    the table entry at qvp_rtp_profole.c. All these functions will 
    automatically link to the RTP stack by doing so.

     This is for EVRC-WB
--------------------------------------------------------------------------*/
qvp_rtp_profile_type qvp_rtp_evrcwb_profile_table = 
{
  qvp_rtp_evrc_profile_init,              /* LOCAL initialization routine */
  qvp_rtp_evrcwb_profile_open,            /* LOCAL func to open channel   */
  qvp_rtp_evrc_profile_send,              /* LOCAL func to send a pkt     */
  qvp_rtp_evrc_profile_recv,              /* LOCAL func to rx a nw pkt    */
  qvp_rtp_evrc_profile_close,             /* LOCAL func to close channel  */
  qvp_rtp_evrc_profile_read_deflt_config, /* LOCAL func to read dflt confg*/
  NULL,
  qvp_rtp_evrc_profile_validate_config_rx,/* LOCAL func-validate rx conf  */
  qvp_rtp_evrc_profile_validate_config_tx,/* LOCAL func-validate tx conf  */
  qvp_rtp_evrcwb_profile_configure,       /* LOCAL func to config profile */
  qvp_rtp_evrc_profile_shutdown           /* LOCAL function to shut down  */
};

/*===========================================================================
                     LOCAL STATIC DATA 
===========================================================================*/

LOCAL boolean evrc_initialized = FALSE;

/*--------------------------------------------------------------------------
     Stores the configuration requested by the app.                        
--------------------------------------------------------------------------*/
LOCAL  qvp_rtp_profile_config_type evrc_profile_config;

/*--------------------------------------------------------------------------
        Stream context array.  Responsible for each bidirectional 
        connections
--------------------------------------------------------------------------*/
LOCAL qvp_rtp_evrc_ctx_type *qvp_rtp_evrc_array = NULL;

/*--------------------------------------------------------------------------
     Configuration pertaining payload formatting for EVRC 
--------------------------------------------------------------------------*/
LOCAL qvp_rtp_evrc_config_type evrc_stream_config;

/*--------------------------------------------------------------------------
    LOCAL table to parse the length
     0     Blank      0    (0 bit)
     1     1/8        2    (16 bits)
     2     1/4        5    (40 bits; not valid for EVRC)
     3     1/2       10    (80 bits)
     4     1         22    (171 bits; 5 padded at end with zeros)
--------------------------------------------------------------------------*/
LOCAL int16 qvp_rtp__evrc_toc_len_table[ QVP_RTP_EVRC_TOC_RATE_1 + 1 ] =
{
  
  0,  /* Blank */
  2,  /* 1/8 rate */
  -1, /* 1/4 rate  ( not valid for EVRC ) */
  10, /* 1/2 rate */
  22, /* full rate */
  
};


/*===========================================================================

FUNCTION QVP_RTP_EVRC_PROFILE_INIT 


DESCRIPTION
  Initializes data structures and resources used by the EVRC profile.

DEPENDENCIES
  None

ARGUMENTS IN
  config - pointer to profile configuration requested.

RETURN VALUE
  QVP_RTP_SUCCESS  - if we could initialize the profile. or an error code


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_evrc_profile_init
(
  qvp_rtp_profile_config_type *config
)
{
/*------------------------------------------------------------------------*/


  if( evrc_initialized )
  {
    return( QVP_RTP_SUCCESS );
  }
   
  /*------------------------------------------------------------------------
    Init if we get a valid config  and appropriate call backs 
    for the basic operation of the profile.
  ------------------------------------------------------------------------*/
  if( config && config->rx_cb && config->tx_cb )
  {
    qvp_rtp_memscpy( &evrc_profile_config,sizeof( evrc_profile_config ), config, sizeof( evrc_profile_config ) );
  }
  else
  {
    return( QVP_RTP_WRONG_PARAM );
  }

  /*------------------------------------------------------------------------
    Allocate the number of streams first
  ------------------------------------------------------------------------*/
  qvp_rtp_evrc_array  = qvp_rtp_malloc( config->num_streams * 
                                sizeof( qvp_rtp_evrc_ctx_type ) ); 

  /*------------------------------------------------------------------------
      See if we could get the memory
  ------------------------------------------------------------------------*/
  if( !qvp_rtp_evrc_array )
  {
    return( QVP_RTP_ERR_FATAL );
  }
  else
  {
    memset( qvp_rtp_evrc_array, 0, config->num_streams * 
                    sizeof( qvp_rtp_evrc_ctx_type ) );
    
    /*----------------------------------------------------------------------
        Set up the default configuration
    ----------------------------------------------------------------------*/
    evrc_stream_config.rx_interleave_on = FALSE;
    evrc_stream_config.rx_toc_mode      = QVP_RTP_EVRC_TOC_RATE_1;
    evrc_stream_config.rx_evrc_pkt_interval = QVP_RTP_EVRC_DFLT_TSTAMP_INCR;
    evrc_stream_config.rx_ptime = QVP_RTP_EVRC_DFLT_PKT_INTERVAL;
    evrc_stream_config.rx_max_ptime = QVP_RTP_DFLT_BUNDLE_SIZE *
                                      QVP_RTP_EVRC_DFLT_PKT_INTERVAL;
    
    
    /*----------------------------------------------------------------------
      We do not support interleaving yet...
      So easy
    ----------------------------------------------------------------------*/
    evrc_stream_config.rx_maxinterleave = 0;

    
    
    evrc_stream_config.tx_interleave_on = FALSE;
    evrc_stream_config.tx_toc_mode      = QVP_RTP_EVRC_TOC_RATE_1;
    evrc_stream_config.tx_bundle_size = QVP_RTP_DFLT_BUNDLE_SIZE;
    evrc_stream_config.tx_ptime = QVP_RTP_EVRC_DFLT_PKT_INTERVAL;
    evrc_stream_config.tx_max_ptime = QVP_RTP_DFLT_BUNDLE_SIZE *
                                      QVP_RTP_EVRC_DFLT_PKT_INTERVAL;

  }
    
  /*------------------------------------------------------------------------
    Flag initialization
  ------------------------------------------------------------------------*/
  evrc_initialized  = TRUE;
  
  return( QVP_RTP_SUCCESS );

}/* end of function qvp_rtp_evrc_profile_init */

/*===========================================================================

FUNCTION  QVP_RTP_EVRC_PROFILE_SEND


DESCRIPTION
  request to send specified buffer through this profile.

DEPENDENCIES
  None

ARGUMENTS IN
  hdl     - handle into the profile. 
  usr_hdl - handle to use when we call send function using the registered
            tx callback
  pkt     - packet of data to be sent.

RETURN VALUE
  QVP_RTP_SUCCESS  - if we could send data. or an error code


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_evrc_profile_send
(
  qvp_rtp_profile_hdl_type     hdl,          /* handle created while open */
  qvp_rtp_profile_usr_hdl_type usr_hdl,      /* user handle for tx cb */
  qvp_rtp_buf_type             *pkt          /* packet to be formatted 
                                              * through the profile
                                              */
)
{
  qvp_rtp_evrc_ctx_type *stream = ( qvp_rtp_evrc_ctx_type *) hdl; 
  qvp_rtp_evrc_toc_type toc;
  qvp_rtp_buf_type   *nw_buf = NULL;
  uint32 cur_tstamp;
  uint16 pkt_len;
  uint8  rtp_pyld_type;
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
                    Are we sane here ..?
  ------------------------------------------------------------------------*/
  if( !evrc_initialized || !stream || !stream->valid || 
      !evrc_profile_config.tx_cb || !pkt  ) 
  {
    
    
    /*----------------------------------------------------------------------
      If the packet is there just free it
    ----------------------------------------------------------------------*/
    if( pkt )
    {
      qvp_rtp_free_buf( pkt );
    }
    return( QVP_RTP_ERR_FATAL );
  }

  /*------------------------------------------------------------------------
    Store the payload type number
  ------------------------------------------------------------------------*/
  rtp_pyld_type = pkt->rtp_pyld_type;

  /*------------------------------------------------------------------------
               If packet len is zero this is a silent packet 
  ------------------------------------------------------------------------*/
  if( !pkt->len )
  {
    
    /*----------------------------------------------------------------------
        Flag it if it is a silent frame
    ----------------------------------------------------------------------*/
    stream->tx_ctx.prev_silence_frame = TRUE;
    
    /*----------------------------------------------------------------------
                Len is zero then mark it with blank frame
    ----------------------------------------------------------------------*/
    b_packb( QVP_RTP_EVRC_TOC_BLANK , stream->tx_ctx.op_packet + 2, 
             stream->tx_ctx.frame_cnt * 4, 4 );
    
  }
  else
  {
    
    /*--------------------------------------------------------------------
        check if it is a speech frame other than NODATA
    --------------------------------------------------------------------*/
    stream->tx_ctx.all_silence_frames = FALSE;

    /*------------------------------------------------------------------
          for a speech frame check if it is the first frame in the RTP 
          packet 
    ------------------------------------------------------------------*/
    if( stream->tx_ctx.frame_cnt == 0 && 
        stream->tx_ctx.prev_silence_frame )
    {
      stream->tx_ctx.marker = TRUE;
    }

    stream->tx_ctx.prev_silence_frame = FALSE;

    /*----------------------------------------------------------------------
       See if the length matches any of the TOCs
    ----------------------------------------------------------------------*/
    toc =  qvp_rtp_evrc_find_toc( stream, pkt->len );

    
    /*----------------------------------------------------------------------
      check for valid toc
    ----------------------------------------------------------------------*/
    if( toc ==  QVP_RTP_EVRC_TOC_INVALID )
    {
      
      QVP_RTP_ERR( " wrong pkt len in EVRC send \r\n", 0, 0, 0);
      
      /*--------------------------------------------------------------------
        Free the buffer and return
      --------------------------------------------------------------------*/
      qvp_rtp_free_buf( pkt );
      return( QVP_RTP_ERR_FATAL );
    }
    else
    {
      
      /*--------------------------------------------------------------------
                      Len is non zero 
      --------------------------------------------------------------------*/
      b_packb( toc, /* 4 bit toc mode */ 
                 stream->tx_ctx.op_packet + 2 /* EVRC fixed header */, 
                 stream->tx_ctx.frame_cnt * 4 /* already packed tocs */, 
                 4 /* len of one toc */ );
      
      /*--------------------------------------------------------------------
              concatenate the frame into our n/w buffer
      --------------------------------------------------------------------*/
      qvp_rtp_memscpy( stream->tx_ctx.op_packet + 2 + 
              ( ( stream->stream_config.tx_bundle_size * 4 + 4 ) /8 )
                + stream->tx_ctx.data_len,QVP_RTP_MTU_DLFT - (2 + ( stream->stream_config.tx_bundle_size * 4 + 4 ) /8 ) - stream->tx_ctx.data_len,
                pkt->data  + pkt->head_room, pkt->len );
      
      /*--------------------------------------------------------------------
            Add to the data len for the newly copied frame
      --------------------------------------------------------------------*/
      stream->tx_ctx.data_len += pkt->len;
              
    }
  }

  /*------------------------------------------------------------------------
    Store the tstamp of the packet
  ------------------------------------------------------------------------*/
  cur_tstamp = pkt->tstamp;

  /*------------------------------------------------------------------------
    Free the buffer
  ------------------------------------------------------------------------*/
  qvp_rtp_free_buf( pkt );
  
  
  /*------------------------------------------------------------------------
      Frame count should be incrementd on each frame

      We will ship the bundle when frame count equals bundle size
  ------------------------------------------------------------------------*/
  stream->tx_ctx.frame_cnt++;


  /*------------------------------------------------------------------------
              If we reached the bundle size lets ship it out
      Send RTP packet only if it has speech frames
  ------------------------------------------------------------------------*/
  if( stream->tx_ctx.frame_cnt == stream->stream_config.tx_bundle_size )
  {
    QVP_RTP_MSG_MED_1( " frame cnt %d \r\n", stream->tx_ctx.frame_cnt);
    if( !stream->tx_ctx.all_silence_frames ) 
    {
      pkt_len  = 2 +  /* EVRC fixed header len */
       ( ( stream->stream_config.tx_bundle_size * 4 + 4 ) /8 ) /* toc len */
                      + stream->tx_ctx.data_len;  /* data already copied */
    
      /*--------------------------------------------------------------------
        Allocate a suiteable len buffer
      --------------------------------------------------------------------*/
      nw_buf = qvp_rtp_alloc_buf_by_len( QVP_RTP_HEAD_ROOM + pkt_len );

    
      /*--------------------------------------------------------------------
        Bail out and cry if we could not allocate.
      --------------------------------------------------------------------*/
      if( !nw_buf )
      {
        QVP_RTP_ERR(" Cannot allocate buffer to send EVRC pkt arggg.. ",
                    0, 0, 0 );
        qvp_rtp_evrc_reset_tx_ctx( stream );
        return( QVP_RTP_ERR_FATAL );
      }
    
      nw_buf->head_room = QVP_RTP_HEAD_ROOM;
      qvp_rtp_memscpy( nw_buf->data + nw_buf->head_room,pkt_len,stream->tx_ctx.op_packet,pkt_len);
      nw_buf->len = pkt_len;

      nw_buf->marker_bit = stream->tx_ctx.marker;
      nw_buf->tstamp = cur_tstamp;
      nw_buf->rtp_pyld_type = rtp_pyld_type;
    
      QVP_RTP_MSG_MED_2( " Sending EVRC Bundle Len = %d, timestamp = %d, \
                         marker = %d", nw_buf->len, nw_buf->tstamp, 
                         nw_buf->marker_bit );

      /*--------------------------------------------------------------------
                      Ship the packet out 
      --------------------------------------------------------------------*/
      evrc_profile_config.tx_cb( nw_buf, usr_hdl );

    }
    
    /*----------------------------------------------------------------------
                We have sent the packet so lets reset it for next bundle
    ----------------------------------------------------------------------*/
    qvp_rtp_evrc_reset_tx_ctx( stream );
    
  }

  return( QVP_RTP_SUCCESS );
  
} /* end of function qvp_rtp_evrc_profile_send */

/*===========================================================================

FUNCTION  QVP_RTP_EVRC_PROFILE_OPEN 


DESCRIPTION
    Requests to open a bi directional channel within the profile.

DEPENDENCIES
  None

ARGUMENTS IN
  usr_hdl       - handle to use when we call send function using the 
                  registered tx callback

ARGUMENTS OUT
  hdl           - on success we write the handle to the profiile into 
                  the  passed double pointer
  
RETURN VALUE
  QVP_RTP_SUCCESS  - if we could send data. or an error code


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_evrc_profile_open
(
  qvp_rtp_profile_hdl_type     *hdl,         /* handle which will get 
                                              * populated with a new profile
                                              * stream handle
                                              */
  qvp_rtp_profile_usr_hdl_type  usr_hdl      /* user handle for any cb */
)
{
  uint32 i, j;   /* index variable */
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
    Bail out if we are not initialized yet
  ------------------------------------------------------------------------*/
  if( !evrc_initialized )
  {
    return( QVP_RTP_ERR_FATAL );
  }

  /*------------------------------------------------------------------------
              look at an idle stream 
  ------------------------------------------------------------------------*/
  for( i = 0;  i < evrc_profile_config.num_streams; i++ )
  {
    
    
    /*----------------------------------------------------------------------
              Fish for free entries
    ----------------------------------------------------------------------*/
    if( !qvp_rtp_evrc_array[ i ].valid )
    {
      qvp_rtp_evrc_array[ i ].valid = TRUE;
      *hdl = &qvp_rtp_evrc_array[ i ]; 
      
      /*--------------------------------------------------------------------
            copy the EVRC configuration to the  stream_ctx 
      --------------------------------------------------------------------*/
      qvp_rtp_memscpy( &qvp_rtp_evrc_array[i].stream_config,sizeof(qvp_rtp_evrc_config_type), &evrc_stream_config,
               sizeof( qvp_rtp_evrc_array[i].stream_config ));

      /*--------------------------------------------------------------------
          Reset the stream context - if any stale values in there.
      --------------------------------------------------------------------*/
      qvp_rtp_evrc_reset_stream( &qvp_rtp_evrc_array[i] );
      
      
      /*--------------------------------------------------------------------
        Form a  predetermined header. There is nothing which will change 
        from bundle to bundle
      --------------------------------------------------------------------*/
      qvp_rtp_evrc_array[i].tx_ctx.tx_hdr_len = 
                      qvp_rtp_evrc_form_header( &qvp_rtp_evrc_array[i] ); 

      qvp_rtp_evrc_array[i].rx_ctx.pkt_interval = 
	     	           evrc_stream_config.rx_evrc_pkt_interval;
	  
      /*--------------------------------------------------------------------
          Copy the default TOC table
      --------------------------------------------------------------------*/
      for( j = 0; j < QVP_RTP_EVRC_TOC_RATE_1 + 1; j++ )
      {
        qvp_rtp_evrc_array[i].toc_len_table[j] = 
			            qvp_rtp__evrc_toc_len_table[j];
      }
	  
      
      return( QVP_RTP_SUCCESS );

    }
    
  } /* maximum no of streams */

  return( QVP_RTP_NORESOURCES );


} /* end of function qvp_rtp_evrc_profile_open */

/*===========================================================================

FUNCTION  QVP_RTP_EVRCWB_PROFILE_OPEN 


DESCRIPTION
    Requests to open a bi directional channel within the profile.

DEPENDENCIES
  None

ARGUMENTS IN
  usr_hdl       - handle to use when we call send function using the 
                  registered tx callback

ARGUMENTS OUT
  hdl           - on success we write the handle to the profiile into 
                  the  passed double pointer
  
RETURN VALUE
  QVP_RTP_SUCCESS  - if we could send data. or an error code


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_evrcwb_profile_open
(
  qvp_rtp_profile_hdl_type     *hdl,         /* handle which will get 
                                              * populated with a new profile
                                              * stream handle
                                              */
  qvp_rtp_profile_usr_hdl_type  usr_hdl      /* user handle for any cb */
)
{
  uint32 i, j;   /* index variable */
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
    Bail out if we are not initialized yet
  ------------------------------------------------------------------------*/
  if( !evrc_initialized )
  {
    return( QVP_RTP_ERR_FATAL );
  }

  /*------------------------------------------------------------------------
              look at an idle stream 
  ------------------------------------------------------------------------*/
  for( i = 0;  i < evrc_profile_config.num_streams; i++ )
  {
    
    
    /*----------------------------------------------------------------------
              Fish for free entries
    ----------------------------------------------------------------------*/
    if( !qvp_rtp_evrc_array[ i ].valid )
    {
      qvp_rtp_evrc_array[ i ].valid = TRUE;
      *hdl = &qvp_rtp_evrc_array[ i ]; 
      
      /*--------------------------------------------------------------------
            copy the EVRC configuration to the  stream_ctx 
      --------------------------------------------------------------------*/
      qvp_rtp_memscpy( &qvp_rtp_evrc_array[i].stream_config,sizeof(qvp_rtp_evrc_config_type), &evrc_stream_config,
               sizeof( qvp_rtp_evrc_array[i].stream_config ));

      /*--------------------------------------------------------------------
          Reset the stream context - if any stale values in there.
      --------------------------------------------------------------------*/
      qvp_rtp_evrc_reset_stream( &qvp_rtp_evrc_array[i] );
      
      
      /*--------------------------------------------------------------------
        Form a  predetermined header. There is nothing which will change 
        from bundle to bundle
      --------------------------------------------------------------------*/
      qvp_rtp_evrc_array[i].tx_ctx.tx_hdr_len = 
                      qvp_rtp_evrc_form_header( &qvp_rtp_evrc_array[i] ); 
	  
      qvp_rtp_evrc_array[i].rx_ctx.pkt_interval = 
	     	           evrc_stream_config.rx_evrcwb_pkt_interval;

      /*--------------------------------------------------------------------
          Copy the default TOC table & update it for EVRCWB
      --------------------------------------------------------------------*/
      for( j = 0; j < QVP_RTP_EVRC_TOC_RATE_1 + 1; j++ )
      {
        qvp_rtp_evrc_array[i].toc_len_table[j] = 
			               qvp_rtp__evrc_toc_len_table[j];
      }

      qvp_rtp_evrc_array[i].toc_len_table[QVP_RTP_EVRC_TOC_RATE_1_4] = 5;
      
      return( QVP_RTP_SUCCESS );

    }
    
  } /* maximum no of streams */

  return( QVP_RTP_NORESOURCES );


} /* end of function qvp_rtp_evrc_profile_open */
/*===========================================================================

FUNCTION QVP_RTP_EVRC_PROFILE_READ_DEFLT_CONFIG 


DESCRIPTION

  This function reads out the default configuration of EVRC
  payload format. The result is conveyed by populating the passed in 
  structure on return.

DEPENDENCIES
  None

ARGUMENTS IN
    payload        - payload type for which the default configuration 
                     is being read out. This is used as
                     EVRC & EVRCB have the same profile tables. 
    
ARGUMENTS OUT
    payld_config   - pointer to configuration structure. On return this 
                     structure will be populated with the default values
                     for the profile.
    



RETURN VALUE
  QVP_RTP_SUCCESS  - operation was succesful.
                    appropriate error code otherwise


SIDE EFFECTS
  payld_config structure will be populated with the default values 
  for the particular profile.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_evrc_profile_read_deflt_config
(
  
  qvp_rtp_payload_type        payld, /* not used */ 
  qvp_rtp_payload_config_type *payld_config 
)
{
  qvp_rtp_evrc_intl_bund_sdp_config_type  *evrc_tx_config = 
      &payld_config->config_tx_params.config_tx_payld_params.evrc_tx_config;
  qvp_rtp_evrc_intl_bund_sdp_config_type  *evrc_rx_config = 
      &payld_config->config_rx_params.config_rx_payld_params.evrc_rx_config;
/*------------------------------------------------------------------------*/


  /*------------------------------------------------------------------------
    If profile config is NULL return error
  ------------------------------------------------------------------------*/
  if( !payld_config )
  {

    return( QVP_RTP_ERR_FATAL );
  }
  

  switch( payld ) 
  {
    case QVP_RTP_PYLD_EVRC:
    {
      evrc_tx_config = 
      &payld_config->config_tx_params.config_tx_payld_params.evrc_tx_config;
      evrc_rx_config = 
      &payld_config->config_rx_params.config_rx_payld_params.evrc_rx_config;
      break;
    }
    case QVP_RTP_PYLD_EVRCB:
    {
      evrc_tx_config = 
      &payld_config->config_tx_params.config_tx_payld_params.evrcb_tx_config;
      evrc_rx_config = 
      &payld_config->config_rx_params.config_rx_payld_params.evrcb_rx_config;
      break;
    }	
    case QVP_RTP_PYLD_EVRCWB:
    {
      evrc_tx_config = 
      &payld_config->config_tx_params.config_tx_payld_params.evrcwb_tx_config;
      evrc_rx_config = 
      &payld_config->config_rx_params.config_rx_payld_params.evrcwb_rx_config;
	break;
    }
    default:
    {
      QVP_RTP_ERR( "Not a EVRC payload %d ", payld, 0, 0 );
      return( QVP_RTP_ERR_FATAL );
    }
  }/* end of switch pyld */

   /*------------------------------------------------------------------------
     Bundling interval
  ------------------------------------------------------------------------*/
  payld_config->config_rx_params.rx_rtp_param.maxptime_valid = TRUE;
  payld_config->config_tx_params.tx_rtp_param.maxptime_valid = TRUE;

  payld_config->config_rx_params.rx_rtp_param.maxptime = 
                                                evrc_stream_config.rx_max_ptime;
  payld_config->config_tx_params.tx_rtp_param.maxptime =
                                                evrc_stream_config.tx_max_ptime;
  
  /*------------------------------------------------------------------------
      Framing interval 20 ms for EVRC
  ------------------------------------------------------------------------*/
  payld_config->config_rx_params.rx_rtp_param.ptime_valid = TRUE;
  payld_config->config_tx_params.tx_rtp_param.ptime_valid = TRUE;

  payld_config->config_rx_params.rx_rtp_param.ptime = 
                                               evrc_stream_config.rx_ptime;
  payld_config->config_tx_params.tx_rtp_param.ptime = 
                                               evrc_stream_config.tx_ptime;

  /*------------------------------------------------------------------------
    Flag the entire rx  and tx config as valid

    Just make sure we stick in our paylod format in the pointer.

    If you got here by accident you get what you get.
  ------------------------------------------------------------------------*/
  payld_config->config_rx_params.valid = TRUE;
  payld_config->config_tx_params.valid = TRUE;

  payld_config->payload  = payld;
  payld_config->valid = TRUE;

  evrc_tx_config->valid = TRUE;
  evrc_rx_config->valid = TRUE; 

  payld_config->chdir_valid = TRUE;
  payld_config->ch_dir = QVP_RTP_CHANNEL_FULL_DUPLEX;
  payld_config->valid = TRUE;


  return( qvp_rtp_evrc_profile_copy_config( payld, evrc_tx_config, 
                                            evrc_rx_config, 
                                            &evrc_stream_config  ) );

    
  
} /* end of function qvp_rtp_evrc_profile_read_deflt_config */ 

/*===========================================================================

FUNCTION QVP_RTP_EVRC_PROFILE_CONFIGURE

DESCRIPTION

  This function payload configuration of a previously opened EVRC  
  channel.

DEPENDENCIES
  None

ARGUMENTS IN
    hdl            - handle to the opend channel
    payld_config   - pointer to configuration structure
    



RETURN VALUE
  QVP_RTP_SUCCESS  - operation was succesfull. 
  else - Attempted a configuration  which is not currently supported by 
         the implementation.


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_evrc_profile_configure
(
  qvp_rtp_profile_hdl_type      hdl,     
  qvp_rtp_payload_config_type   *payld_config 
)
{
  qvp_rtp_evrc_ctx_type *stream = ( qvp_rtp_evrc_ctx_type *) hdl; 
  qvp_rtp_evrc_config_type prev_config;
  qvp_rtp_status_type      status = QVP_RTP_SUCCESS;
  qvp_rtp_evrc_intl_bund_sdp_config_type  *evrc_rx_config = NULL;
  qvp_rtp_evrc_intl_bund_sdp_config_type *evrc_tx_config = NULL;
/*------------------------------------------------------------------------*/


  
  /*------------------------------------------------------------------------
                Are we sane here ..?
  ------------------------------------------------------------------------*/
  if( !evrc_initialized || !stream || !stream->valid || 
      !payld_config ) 
  {
    return( QVP_RTP_ERR_FATAL );
  }

  /*------------------------------------------------------------------------
      Update the EVRC TOC table
      Since we are using the same code for EVRC & EVRCB we need to get the
      right pyld config structure here
  ------------------------------------------------------------------------*/
  if( payld_config->payload == QVP_RTP_PYLD_EVRCB )
  {
    stream->toc_len_table[QVP_RTP_EVRC_TOC_RATE_1_4] = 5;
    evrc_rx_config = 
      &payld_config->config_rx_params.config_rx_payld_params.evrcb_rx_config;
    evrc_tx_config = 
      &payld_config->config_tx_params.config_tx_payld_params.evrcb_tx_config;
  }
  else if( payld_config->payload == QVP_RTP_PYLD_EVRC )
  {    
    stream->toc_len_table[QVP_RTP_EVRC_TOC_RATE_1_4] = -1;
    evrc_rx_config = 
      &payld_config->config_rx_params.config_rx_payld_params.evrc_rx_config;
    evrc_tx_config = 
      &payld_config->config_tx_params.config_tx_payld_params.evrc_tx_config;	
  }
  else
  {
    QVP_RTP_ERR( "Not a EVRC Payload %d ", payld_config->payload ,0,0 );
    status =  QVP_RTP_ERR_FATAL;
  }

   
  /*------------------------------------------------------------------------
    Backup the current configuration before we proceed from here.
  ------------------------------------------------------------------------*/
  prev_config = stream->stream_config;

  /*------------------------------------------------------------------------
    Configure ptime, maxptime here for both RX & TX
  ------------------------------------------------------------------------*/
  status = qvp_rtp_evrc_config_pkt_itvl( stream, payld_config );

  
  /*------------------------------------------------------------------------
      Configuration steps.

      1) Try and configure RX.
         If RX fails.
          Reset it to previous state and return error.
          This way application can easily implement OFFER/ANSWER model as 
          given in RFC3264.
      2) Try and configure TX
         If TX  configure fails.
          Reset it to previous state and return error.
          This way application can easily implement OFFER/ANSWER model 
          as given in RFC3264.
  ------------------------------------------------------------------------*/

  if( ( status == QVP_RTP_SUCCESS ) &&
      payld_config->config_rx_params.valid )
  {
    status = qvp_rtp_evrc_profile_config_rx_param( stream, evrc_rx_config );
  }


  /*------------------------------------------------------------------------
      Try and configure Tx part now....
  ------------------------------------------------------------------------*/
  if( ( status == QVP_RTP_SUCCESS )&&
      payld_config->config_tx_params.valid )
  {
    status = qvp_rtp_evrc_profile_config_tx_param(stream, evrc_tx_config );
    
  }

  /*------------------------------------------------------------------------
    If we did not succeed to a struct to reset to preivous value and 
    return failure
  ------------------------------------------------------------------------*/
  if( status != QVP_RTP_SUCCESS )
  {
    stream->stream_config = prev_config ;
  }

  /*------------------------------------------------------------------------
      In case any of the TX params are changed form the header again, 
      no harm even if the config above was not successful
  ------------------------------------------------------------------------*/
  if( stream->tx_ctx.valid )
  {
    stream->tx_ctx.tx_hdr_len = qvp_rtp_evrc_form_header( stream );
  }

  /*------------------------------------------------------------------------
      Set the timestamp interval for every configure, since this will 
      not change for all evrc & evrcb streams
  ------------------------------------------------------------------------*/  
  stream->rx_ctx.pkt_interval =  evrc_stream_config.rx_evrc_pkt_interval;
  
  return( status );

} /* end of function qvp_rtp_evrc_profile_configure */

/*===========================================================================

FUNCTION QVP_RTP_EVRCWB_PROFILE_CONFIGURE

DESCRIPTION

  This function payload configuration of a previously opened EVRC  
  channel.

DEPENDENCIES
  None

ARGUMENTS IN
    hdl            - handle to the opend channel
    payld_config   - pointer to configuration structure
    



RETURN VALUE
  QVP_RTP_SUCCESS  - operation was succesfull. 
  else - Attempted a configuration  which is not currently supported by 
         the implementation.


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_evrcwb_profile_configure
(
  qvp_rtp_profile_hdl_type      hdl,     
  qvp_rtp_payload_config_type   *payld_config 
)
{
  qvp_rtp_status_type status = QVP_RTP_SUCCESS;
  qvp_rtp_evrc_ctx_type *stream = ( qvp_rtp_evrc_ctx_type *) hdl; 
  qvp_rtp_evrc_config_type prev_config;
/*------------------------------------------------------------------------*/


  
  /*------------------------------------------------------------------------
                Are we sane here ..?
  ------------------------------------------------------------------------*/
  if( !evrc_initialized || !stream || !stream->valid || 
      !payld_config ) 
  {
    return( QVP_RTP_ERR_FATAL );
  }

  stream->toc_len_table[QVP_RTP_EVRC_TOC_RATE_1_4] = 5;

  /*------------------------------------------------------------------------
    Backup the current configuration before we proceed from here.
  ------------------------------------------------------------------------*/
  prev_config = stream->stream_config;

  /*------------------------------------------------------------------------
    Configure ptime, maxptime here
  ------------------------------------------------------------------------*/
  status = qvp_rtp_evrc_config_pkt_itvl( stream, payld_config );

  
  /*------------------------------------------------------------------------
      Configuration steps.

      1) Try and configure RX.
         If RX fails.
          Reset it to previous state and return error.
          This way application can easily implement OFFER/ANSWER model as 
          given in RFC3264.
      2) Try and configure TX
         If TX  configure fails.
          Reset it to previous state and return error.
          This way application can easily implement OFFER/ANSWER model 
          as given in RFC3264.
  ------------------------------------------------------------------------*/

  if( status == QVP_RTP_SUCCESS &&
      payld_config->config_rx_params.valid )
  {
    status = qvp_rtp_evrc_profile_config_rx_param( stream, 
		&payld_config->config_rx_params.config_rx_payld_params.evrcwb_rx_config  );
  }


  /*------------------------------------------------------------------------
      Try and configure Tx part now....
  ------------------------------------------------------------------------*/
  if( ( status == QVP_RTP_SUCCESS )&&
      payld_config->config_tx_params.valid )
  {
    status = qvp_rtp_evrc_profile_config_tx_param(stream, 
		&payld_config->config_tx_params.config_tx_payld_params.evrcwb_tx_config );
    
  }

  /*------------------------------------------------------------------------
    If we did not succeed to a struct to reset to preivous value and 
    return failure
  ------------------------------------------------------------------------*/
  if( status != QVP_RTP_SUCCESS )
  {
    stream->stream_config = prev_config ;
  }

  /*------------------------------------------------------------------------
      In case any of the TX params are changed form the header again, 
      no harm even if the config above was not successful
  ------------------------------------------------------------------------*/
  if( stream->tx_ctx.valid )
  {
    stream->tx_ctx.tx_hdr_len = qvp_rtp_evrc_form_header( stream );
  }

  /*------------------------------------------------------------------------
      Set the timestamp interval for every configure, since this will 
      not change for all evrcwb streams
  ------------------------------------------------------------------------*/
  stream->rx_ctx.pkt_interval = evrc_stream_config.rx_evrcwb_pkt_interval;
  
  return( status );
  
} /* end of qvp_rtp_evrcwb_profile_configure */

/*===========================================================================

FUNCTION qvp_rtp_evrc_config_pkt_itvl

DESCRIPTION

  To configure All TX & RX params which are common for both 
  EVRC, EVRCWB

DEPENDENCIES
  None

ARGUMENTS IN
    hdl            - handle to the opend channel
    payld_config   - pointer to configuration structure
    



RETURN VALUE
  QVP_RTP_SUCCESS  - operation was succesfull. 
  else - Attempted a configuration  which is not currently supported by 
         the implementation.


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_evrc_config_pkt_itvl
(
  qvp_rtp_evrc_ctx_type *stream,     
  qvp_rtp_payload_config_type   *payld_config 
)
{
  if( !stream || !payld_config )
  {
    QVP_RTP_ERR( "Invalid params ", 0, 0, 0 );
    return( QVP_RTP_ERR_FATAL );
  }

  /*------------------------------------------------------------------------
    See if the pttime matches with what we have
  ------------------------------------------------------------------------*/
  if( payld_config->config_rx_params.rx_rtp_param.ptime_valid && 
      ( payld_config->config_rx_params.rx_rtp_param.ptime != 
                   QVP_RTP_EVRC_DFLT_PKT_INTERVAL ) )
  {
    QVP_RTP_MSG_MED_1( "EVRC Invalid Rx ptime %d ",
        payld_config->config_rx_params.rx_rtp_param.ptime, 0, 0 );
    return( QVP_RTP_ERR_FATAL );
  }

  if( payld_config->config_rx_params.rx_rtp_param.maxptime_valid ) 
  {

    /*----------------------------------------------------------------------
      if max ptime is less than ptime then we have a problem.
      If this exceeds too much this will
      be bad for our jitter buffer. But then the other end is asking 
      for trouble.
    ----------------------------------------------------------------------*/
    if( ( payld_config->config_rx_params.rx_rtp_param.maxptime < 
                  QVP_RTP_EVRC_DFLT_PKT_INTERVAL ) ||
       ( payld_config->config_tx_params.tx_rtp_param.maxptime > 
          QVP_RTP_EVRC_DFLT_PKT_INTERVAL*QVP_RTP_DFLT_BUNDLE_SIZE ) )
    {
      QVP_RTP_MSG_MED_1( "EVRC Invalid Rx maxptime %d ",
        payld_config->config_rx_params.rx_rtp_param.maxptime, 0, 0 );
      return( QVP_RTP_ERR_FATAL );
    }

    
  }

  /*----------------------------------------------------------------------
     Store this value.
  ----------------------------------------------------------------------*/
  stream->stream_config.rx_max_ptime = 
                 payld_config->config_rx_params.rx_rtp_param.maxptime; 

  QVP_RTP_MSG_HIGH_2( "EVRC Rx maxptime %d Rx ptime %d",
      stream->stream_config.rx_max_ptime,
        stream->stream_config.tx_ptime, 0 );  

  if( payld_config->config_tx_params.tx_rtp_param.ptime_valid == TRUE )
  {

    /*----------------------------------------------------------------------
      This means peer can receive pkts with ptime lesser 
      than our minimum ptime pkts we can send. Throw an error.
    ----------------------------------------------------------------------*/
    if ( payld_config->config_tx_params.tx_rtp_param.ptime < 
                       QVP_RTP_EVRC_DFLT_PKT_INTERVAL )
    {
      QVP_RTP_MSG_MED_1( "EVRC Invalid Tx ptime %d ",
        payld_config->config_tx_params.tx_rtp_param.ptime, 0, 0 );
      return( QVP_RTP_ERR_FATAL );
    }

    /*----------------------------------------------------------------------
      This means peer can receive pkts with ptime greater 
      than our maximum ptime. Just set the transmit ptime to maximum 
      ptime we can send.
    ----------------------------------------------------------------------*/
    else if ( payld_config->config_tx_params.tx_rtp_param.ptime > 
                       QVP_RTP_EVRC_DFLT_PKT_INTERVAL )
    {
      stream->stream_config.tx_ptime = QVP_RTP_EVRC_DFLT_PKT_INTERVAL;
    }
    /*----------------------------------------------------------------------
      Proposed ptime is within the limits, we can send packets with the 
      ptime proposed.
    ----------------------------------------------------------------------*/
    else
    {
      stream->stream_config.tx_ptime = 
              payld_config->config_tx_params.tx_rtp_param.ptime;
    }

  }/*end of if (ptime_valid == TRUE )*/

  if( payld_config->config_tx_params.tx_rtp_param.maxptime_valid == TRUE ) 
  {

    /*----------------------------------------------------------------------
      if max ptime is less than ptime then we have a problem. 
    ----------------------------------------------------------------------*/
    if( payld_config->config_tx_params.tx_rtp_param.maxptime < 
                         QVP_RTP_EVRC_DFLT_PKT_INTERVAL )
    {
      QVP_RTP_MSG_MED_1( "EVRC Invalid Tx maxptime %d ",
        payld_config->config_tx_params.tx_rtp_param.maxptime, 0, 0 );
      return( QVP_RTP_ERR_FATAL );
    }
    /*----------------------------------------------------------------------
      This means peer can receive pkts with maxptime greater 
      than our maximum maxptime. Just set the transmit maxptime to 
      maximum maxptime we can send.
    ----------------------------------------------------------------------*/
    if( payld_config->config_tx_params.tx_rtp_param.maxptime > 
         ( QVP_RTP_EVRC_DFLT_PKT_INTERVAL*QVP_RTP_DFLT_BUNDLE_SIZE ) )
    {
      stream->stream_config.tx_max_ptime = QVP_RTP_EVRC_DFLT_PKT_INTERVAL*
                              QVP_RTP_DFLT_BUNDLE_SIZE;
    }
    /*----------------------------------------------------------------------
      Proposed maxptime is within the limits, we can send packets with the 
      maxptime proposed.
    ----------------------------------------------------------------------*/
    else
    {
      stream->stream_config.tx_max_ptime = 
               payld_config->config_tx_params.tx_rtp_param.maxptime;
    }
    
  }/*end of if (maxptim_valid == TRUE )*/

  /*------------------------------------------------------------------------
    configure the transmit bundle size with this maxptime
  ------------------------------------------------------------------------*/
  stream->stream_config.tx_bundle_size = 
      stream->stream_config.tx_max_ptime/stream->stream_config.tx_ptime;

  QVP_RTP_MSG_HIGH( "EVRC Tx maxptime %d Tx ptime %d bundle size %d",
        stream->stream_config.tx_max_ptime,
    stream->stream_config.tx_ptime, stream->stream_config.tx_bundle_size );

  return( QVP_RTP_SUCCESS );

} /* end of qvp_rtp_evrc_config_pkt_itvl */

/*===========================================================================

FUNCTION QVP_RTP_EVRC_PROFILE_CONFIG_RX_PARAM

DESCRIPTION

  This function payload will configuration of Rx part of a previously
  opened stream.  

DEPENDENCIES
  None

ARGUMENTS IN
  stream        - EVRC stream which needs to be configured.
  payld_config  - pointer to configuration structure

RETURN VALUE
  QVP_RTP_SUCCESS  - operation was succesfull. 
  else - Attempted a configuration  which is not currently supported by 
         the implementation.


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_evrc_profile_config_rx_param
(
  
  qvp_rtp_evrc_ctx_type       *stream,
  qvp_rtp_evrc_intl_bund_sdp_config_type  *evrc_rx_config 
)
{
      
/*------------------------------------------------------------------------*/
  if( !evrc_rx_config || !stream ) 
  {
    QVP_RTP_ERR ( " Invalid params", 0, 0, 0);
    return( QVP_RTP_ERR_FATAL );
  }

  /*------------------------------------------------------------------------
      If someone is asking for interleaving sorry no.

      Or using the default 5 say no
  ------------------------------------------------------------------------*/
  if( ( !evrc_rx_config->maxinterleave_valid ) ||
      ( ( evrc_rx_config->maxinterleave_valid )
      && ( evrc_rx_config->maxinterleave > 0 ) ) )
  {
    QVP_RTP_ERR( "Interleaving not supported", 0, 0, 0 );
    return( QVP_RTP_ERR_FATAL );
    
  }
  else
  {

    
    stream->stream_config.rx_interleave_on  = 0;
    stream->stream_config.rx_maxinterleave = 0;
    
  }


  return( QVP_RTP_SUCCESS );
  
} /* end of function qvp_rtp_evrc_profile_config_rx_param */ 

/*===========================================================================

FUNCTION QVP_RTP_EVRC_PROFILE_CONFIG_TX_PARAM

DESCRIPTION

  This function payload will configuration of Tx part of a previously
  opened stream.  

DEPENDENCIES
  None

ARGUMENTS IN
    stream         - EVRC stream which needs to be configured.
    payld_config   - pointer to configuration structure
    



RETURN VALUE
  QVP_RTP_SUCCESS  - operation was succesfull. 
  else - Attempted a configuration  which is not currently supported by 
         the implementation.


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_evrc_profile_config_tx_param
(
  
  qvp_rtp_evrc_ctx_type       *stream,
  qvp_rtp_evrc_intl_bund_sdp_config_type  *evrc_tx_config
)
{
      
/*------------------------------------------------------------------------*/
  if( !evrc_tx_config || !stream ) 
  {
    QVP_RTP_ERR ( " Invalid params", 0, 0, 0);
    return( QVP_RTP_ERR_FATAL );
  }

  /*------------------------------------------------------------------------
    If we are getting interleaving signalled we need NOT say NO
    We will just will not transmit interleaved frames. Consec frames
    are a special case of interleaved frames.
  ------------------------------------------------------------------------*/
  if( evrc_tx_config->maxinterleave_valid ) 
  {

    stream->stream_config.tx_maxinterleave = evrc_tx_config->maxinterleave; 
  }

  /*------------------------------------------------------------------------
    Just to retiterate we dont plan on transmitting interleaved frames yet.
  ------------------------------------------------------------------------*/
  stream->stream_config.tx_interleave_on = 0;

  /*------------------------------------------------------------------------
      Form a  predetermined header. There is nothing which will change 
      from bundle to bundle
  ------------------------------------------------------------------------*/
  stream->tx_ctx.tx_hdr_len = qvp_rtp_evrc_form_header( stream ); 

  QVP_RTP_MSG_HIGH_2( "After EVRC tx config bundlesize %d, header len %d ", 
                          stream->stream_config.tx_bundle_size,
                             stream->tx_ctx.tx_hdr_len, 0 );

  return( QVP_RTP_SUCCESS );
        
} /* end of function qvp_rtp_evrc_profile_config_tx_param */

/*==========================================================================

FUNCTION QVP_RTP_EVRC_PROFILE_VALIDATE_CONFIG 


DESCRIPTION

  This function checks the configuration of EVRC specified in the input 
  against the valid fmtp values for EVRC

DEPENDENCIES
  None

ARGUMENTS IN
  payld_config - configuration which needs to be validated

ARGUMENTS OUT
  counter_config - If the attribute  is valid, counter_config is not modified
                   If the attribute  is invalid,
                   counter_config is updated with proposed value

RETURN VALUE
  QVP_RTP_SUCCESS  - The given Configuration is  valid for the given payload,
                     RTP configure will succeed for these settings
                     
  QVP_RTP_ERR_FATAL  - The given Configuration is not valid for the given 
                        payload. RTP configure will fail for these settings

SIDE EFFECTS
  None
===========================================================================*/
LOCAL qvp_rtp_status_type  qvp_rtp_evrc_profile_validate_config_rx
(
  qvp_rtp_payload_config_type  *payld_config,
  qvp_rtp_payload_config_type  *counter_config 
)
{
  qvp_rtp_status_type status = QVP_RTP_SUCCESS;
  qvp_rtp_evrc_intl_bund_sdp_config_type*  evrc_config = NULL;
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
    If profile config is NULL return error
  ------------------------------------------------------------------------*/
  if ( ( !payld_config ) || ( !counter_config ) )
  {
    return( QVP_RTP_ERR_FATAL );
  }

  /*------------------------------------------------------------------------
    Validating rx configuration
   -----------------------------------------------------------------------*/
  if ( ( payld_config->config_rx_params.valid ) && 
       ( payld_config->config_rx_params.
             config_rx_payld_params.evrc_rx_config.valid ) )
  {
    if ( ( payld_config->ch_dir == QVP_RTP_CHANNEL_INBOUND ) || 
         ( payld_config->ch_dir == QVP_RTP_CHANNEL_FULL_DUPLEX ) )
    {
      /*--------------------------------------------------------------------
        Validate ptime. Throw an error if we cant receive packets with 
        the ptime proposed.
      --------------------------------------------------------------------*/
      if ( ( payld_config->config_rx_params.rx_rtp_param.ptime_valid ) && 
           ( payld_config->config_rx_params.rx_rtp_param.ptime != 
                    QVP_RTP_EVRC_DFLT_PKT_INTERVAL ) )
      {
        counter_config->config_rx_params.rx_rtp_param.ptime = 
                    QVP_RTP_EVRC_DFLT_PKT_INTERVAL ; 
        status = QVP_RTP_ERR_FATAL;
      }
      /*--------------------------------------------------------------------
        Validate maxptime. Throw an error if we cant receive packets with 
        the maxptime proposed.
      --------------------------------------------------------------------*/
      if ( ( payld_config->config_rx_params.rx_rtp_param.maxptime_valid ) && 
           ( ( payld_config->config_rx_params.rx_rtp_param.maxptime  < 
                       QVP_RTP_EVRC_DFLT_PKT_INTERVAL ) ||
             ( payld_config->config_rx_params.rx_rtp_param.maxptime  >  
                       QVP_RTP_EVRC_DFLT_PKT_INTERVAL *
                                                QVP_RTP_DFLT_BUNDLE_SIZE ) ) )
      {
        counter_config->config_rx_params.rx_rtp_param.maxptime = 
                       QVP_RTP_EVRC_DFLT_PKT_INTERVAL *
                                                    QVP_RTP_DFLT_BUNDLE_SIZE ; 
        status = QVP_RTP_ERR_FATAL;
      }

      switch( payld_config->payload )
       {
         case QVP_RTP_PYLD_EVRC:
         {
           evrc_config = 
           &payld_config->config_rx_params.config_rx_payld_params.
                                                     evrc_rx_config;
           break;
         }
         case QVP_RTP_PYLD_EVRCB:
         {
           evrc_config = 
           &payld_config->config_rx_params.config_rx_payld_params.
                                                  evrcb_rx_config;
           break;
         }
         case QVP_RTP_PYLD_EVRCWB:
         {
           evrc_config = 
           &payld_config->config_rx_params.config_rx_payld_params.
                                                   evrcwb_rx_config;
           break;
         }
        default:
        {
         QVP_RTP_ERR ( " Not a EVRC payload %d ", payld_config->payload, 0, 0);
         status = QVP_RTP_ERR_FATAL ;
        }

      }/* end of switch payload */
	
      /*--------------------------------------------------------------------
        Attempting to configure rx for an inbound/ duplex channel
      --------------------------------------------------------------------*/
      if( status == QVP_RTP_SUCCESS )
      {
        status = qvp_rtp_evrc_profile_validate_fmtp (
                payld_config->ch_dir, evrc_config ,
                & (counter_config->config_rx_params.config_rx_payld_params.
                                                          evrc_rx_config) );
      }
    }/* end of if ( ( payld_config->ch_dir */
    else
    {
      QVP_RTP_ERR ( " Attempting to config rx for an o/b only ch", 0, 0, 0);
      status = QVP_RTP_ERR_FATAL ;
    }
  } /* end of if ( ( payld_config->config_rx_params.valid )*/
  
  return ( status ); 

}/* end of qvp_rtp_evrc_profile_validate_config_rx */


/*===========================================================================

FUNCTION QVP_RTP_EVRC_PROFILE_VALIDATE_CONFIG_TX 


DESCRIPTION

  This function checks the configuration of EVRC specified in the input 
  against the valid fmtp values for EVRC

DEPENDENCIES
  None

ARGUMENTS IN
  payld_config - configuration which needs to be validated

ARGUMENTS OUT
  counter_config - If the attribute  is valid, counter_config is not modified
                   If the attribute  is invalid, 
                   counter_config is updated with proposed value


RETURN VALUE
  QVP_RTP_SUCCESS  - The given Configuration is  valid for the given payload,
                     RTP configure will succeed for these settings
  QVP_RTP_ERR_FATAL   - The given Configuration is not valid for the given 
                        payload. RTP configure will fail for these settings

SIDE EFFECTS
  None
===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_evrc_profile_validate_config_tx
(
  qvp_rtp_payload_config_type*  payld_config,
  qvp_rtp_payload_config_type*  counter_config 
)
{
  qvp_rtp_status_type status = QVP_RTP_SUCCESS;
  qvp_rtp_evrc_intl_bund_sdp_config_type*  evrc_config = NULL;
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
    If profile config is NULL return error
  ------------------------------------------------------------------------*/
  if( !payld_config )
  {
    return( QVP_RTP_ERR_FATAL );
  }

  /*------------------------------------------------------------------------
    Validating tx configuration
   -----------------------------------------------------------------------*/
  if ( ( payld_config->config_tx_params.valid ) && 
       ( payld_config->config_tx_params.
             config_tx_payld_params.evrc_tx_config.valid ) )
  {
    if ( ( payld_config->ch_dir == QVP_RTP_CHANNEL_OUTBOUND ) || 
         ( payld_config->ch_dir == QVP_RTP_CHANNEL_FULL_DUPLEX ) )
    {
      /*--------------------------------------------------------------------
        Validate ptime. Throw an error if peer wants us to send 
        packets with ptime lesser than our minimum supported ptime.
      --------------------------------------------------------------------*/
      if ( ( payld_config->config_tx_params.tx_rtp_param.ptime_valid ) && 
           ( payld_config->config_tx_params.tx_rtp_param.ptime < 
                    QVP_RTP_EVRC_DFLT_PKT_INTERVAL ) )
      {
        counter_config->config_tx_params.tx_rtp_param.ptime = 
                    QVP_RTP_EVRC_DFLT_PKT_INTERVAL ; 
        status = QVP_RTP_ERR_FATAL;
      }
      /*--------------------------------------------------------------------
        Validate maxptime. Throw an error if peer wants us to send 
        packets with maxptime lesser than our minimum supported maxptime.
      --------------------------------------------------------------------*/
      if ( ( payld_config->config_tx_params.tx_rtp_param.maxptime_valid ) && 
           ( payld_config->config_tx_params.tx_rtp_param.maxptime < 
                       QVP_RTP_EVRC_DFLT_PKT_INTERVAL ) )
      {
        counter_config->config_tx_params.tx_rtp_param.maxptime = 
                         QVP_RTP_EVRC_DFLT_PKT_INTERVAL *
                                    QVP_RTP_DFLT_BUNDLE_SIZE ; 
        status = QVP_RTP_ERR_FATAL;
      }

       switch( payld_config->payload )
       {
         case QVP_RTP_PYLD_EVRC:
         {
           evrc_config = 
           &payld_config->config_tx_params.config_tx_payld_params.
                                                     evrc_tx_config;
           break;
         }
         case QVP_RTP_PYLD_EVRCB:
         {
           evrc_config = 
           &payld_config->config_tx_params.config_tx_payld_params.
                                                  evrcb_tx_config;
           break;
         }
         case QVP_RTP_PYLD_EVRCWB:
         {
           evrc_config = 
           &payld_config->config_tx_params.config_tx_payld_params.
                                                   evrcwb_tx_config;
           break;
         }
        default:
        {
         QVP_RTP_ERR ( " Not a EVRC payload %d ", payld_config->payload, 0, 0);
         status = QVP_RTP_ERR_FATAL ;
        }
      }/* end of switch payload */

      /*--------------------------------------------------------------------
        Attempting to configure tx for an outbound/ duplex channel
      --------------------------------------------------------------------*/
      if( status == QVP_RTP_SUCCESS )
      {
        status = qvp_rtp_evrc_profile_validate_fmtp (
                 payld_config->ch_dir, evrc_config,
                 & (counter_config->config_tx_params.config_tx_payld_params.
                                                          evrc_tx_config) );
      }
    }/* end of if ( ( payld_config->ch_dir */
    else
    {
      QVP_RTP_ERR ( " Attempting to config tx for an i/b only ch", 0, 0, 0);
      status = QVP_RTP_ERR_FATAL ;
    }
  }/* end of if ( ( payld_config->config_tx_params.valid )*/
  
  return ( status ); 

}/* end of qvp_rtp_evrc_profile_validate_config_tx */


/*===========================================================================

FUNCTION QVP_RTP_EVRC_PROFILE_VALIDATE_FMTP


DESCRIPTION

  This function checks the configuration of EVRC specified in the input
  against the valid fmtp values for EVRC
  
  This is the generic function used for both tx & rx params since most of 
  the parameters have the same valid range in both the directions.
  
  In case of any assymetry,the differences have to be accomodated 
  using channel direction 
  or this can be split into separate functions on tx&rx plane


DEPENDENCIES
  None

ARGUMENTS IN
    payld_config - configuration which needs to be validated
    ch_dir       - This gives the information whether the Tx or Rx plane 
                   have to be configured
                   Currently not used since Tx & Rx plane are symmetrical

ARGUMENTS OUT
  counter_config - If the attribute is valid, counter_config is not modified
                   If the attribute  is invalid, 
                   counter_config is updated with proposed value
                   
                   counter_config has the basic capabilities.
                   In future additional support can be added to come up with
                   an enhanced configuration


RETURN VALUE
  QVP_RTP_SUCCESS   - The given Configuration is valid for the given payload,
                      RTP configure will succeed for these settings
  QVP_RTP_ERR_FATAL - The given Configuration is not valid for the given 
                      payload. RTP configure will fail for these settings

SIDE EFFECTS
  None
  
===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_evrc_profile_validate_fmtp
(
  qvp_rtp_channel_type                    ch_dir,  /* not used for now*/
  qvp_rtp_evrc_intl_bund_sdp_config_type*  evrc_config,
  qvp_rtp_evrc_intl_bund_sdp_config_type  *counter_evrc_config 
)
{
  qvp_rtp_status_type status = QVP_RTP_SUCCESS;
/*------------------------------------------------------------------------*/

  if ( !counter_evrc_config ) 
  {
    return( QVP_RTP_ERR_FATAL );
  }

  if ( !evrc_config || evrc_config->valid )
  {
    return (status );
  }

  /*------------------------------------------------------------------------
    Maxinterleave is optional but if not mentioned default of 5 is assumed 
    Since we do not support interleaving we cannot handle this.
    So the only valid scenario is maxinterleave set with value 0
  ------------------------------------------------------------------------*/
  if( ( !evrc_config->maxinterleave_valid ) ||
      ( ( evrc_config->maxinterleave )
      && (  evrc_config->maxinterleave > 0 ) ) ) /* interleaving not supp */
  {
    counter_evrc_config->maxinterleave = 0; /* bundled format*/
    status = QVP_RTP_ERR_FATAL;
  }

  /*------------------------------------------------------------------------
    Fmtp related to DTX parameters are not used by RTP ,validation not done
  ------------------------------------------------------------------------*/
  
  return ( status );
  
} /* end of function qvp_rtp_evrc_profile_validate_fmtp */ 

  
/*===========================================================================

FUNCTION  QVP_RTP_EVRC_PROFILE_RECV 


DESCRIPTION
   The function which the RTP framework will call upon arrival of data from 
   NW. EVRC header is stripped but parameters are passed.

DEPENDENCIES
  None

ARGUMENTS IN
  hdl           - hdl into the profile.
  usr_hdl       - handle to use when we call send function using the 
                  registered rx callback
  pkt           - actual data packet to be parsed.

  
RETURN VALUE
  QVP_RTP_SUCCESS  - if we could send data. or an error code


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_evrc_profile_recv
(

  qvp_rtp_profile_hdl_type     hdl,          /* handle created while open */
  qvp_rtp_profile_usr_hdl_type usr_hdl,      /* user handle for rx cb */
  qvp_rtp_buf_type             *pkt          /* packet parsed and removed*/

)
{
  qvp_rtp_evrc_hdr_param_type hdr;     /* parsed header storage */
  qvp_rtp_evrc_ctx_type *stream = ( qvp_rtp_evrc_ctx_type *) hdl; 
  uint16                  toc;        /* to parse toc field */
  qvp_rtp_buf_type       *aud_buf;
  uint16                  offset = 0; /* for parsing tocs */
  uint16                 len;
  uint8                  *aud_data;
  uint16                 aud_len;
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
                Are we sane here ..?
  ------------------------------------------------------------------------*/
  if( !evrc_initialized || !stream || !stream->valid || 
      !evrc_profile_config.rx_cb  ) 
  {
    return( QVP_RTP_ERR_FATAL );
  }

  QVP_RTP_MSG_MED_0( " Got an EVRC packet ");
  
  /*------------------------------------------------------------------------
              Parse the header 
  ------------------------------------------------------------------------*/
  if( ( qvp_rtp_parse_evrc_fixed_hdr( pkt->data + pkt->head_room, 
                              pkt->len, &hdr )  == 0 /* parse error */) || 
     (  ( hdr.lll != 0 /* we are getting interleaved frames */ ) 
        && !stream->stream_config.rx_interleave_on ) )
  {
    QVP_RTP_ERR( " Error in evrc pkt hdr ", 0, 0, 0 ); 
    /*----------------------------------------------------------------------
                free and exit
    ----------------------------------------------------------------------*/
    if( pkt->need_tofree )
    {
      qvp_rtp_free_buf( pkt );
    }
    return( QVP_RTP_ERR_FATAL );
    
  }

 /*
   From RFC 3558
   Frame Count (Count): 5 bits
      The number of ToC fields (and vocoder frames) present in the
      packet is the value of the frame count field plus one.

      hence we need to increment by one
 */
  hdr.count++;

  if( ( stream->rx_ctx.last_seq + 1 ) != pkt->seq ) 
  {
    QVP_RTP_ERR( " Missisg Seq inside RTP", 0, 0, 0 ); 
  }
 
  stream->rx_ctx.last_seq = pkt->seq;

  /*------------------------------------------------------------------------
      If we have a smaller buffer free and exit
  ------------------------------------------------------------------------*/
  if( pkt->len < ( 2 /* fixed header */ + ( ( hdr.count * 4 + 4 ) / 8 ) ) )
  {
    QVP_RTP_ERR( " Pkt len insufficient %d ", pkt->len, 0, 0 ); 
    /*----------------------------------------------------------------------
                free and exit
    ----------------------------------------------------------------------*/
    if( pkt->need_tofree )
    {
      qvp_rtp_free_buf( pkt );
    }
    return( QVP_RTP_ERR_FATAL );
  }
  

  /*------------------------------------------------------------------------
      Advance to audio data inside the packet 
  ------------------------------------------------------------------------*/
  aud_data = pkt->data + pkt->head_room + 2 /* fixed header length */ + 
            ( ( hdr.count * 4 + 4 ) / 8 );
  
  /*------------------------------------------------------------------------
      Preacalculate the audio data length inside the packet
  ------------------------------------------------------------------------*/
  aud_len = pkt->len - 2 /* fixed hdr */ 
            - ( ( hdr.count * 4 + 4 ) / 8 ) /* dynamic header */ ; 
  
  /*------------------------------------------------------------------------
      iterate through the packet until we have parsed all the frames...

      Or we get erraneous PDU.
  ------------------------------------------------------------------------*/
  while( hdr.count )
  {

    /*----------------------------------------------------------------------
                    read the toc
    ----------------------------------------------------------------------*/
    toc = b_unpackb( pkt->data + pkt->head_room + 2, offset, 4 );
    offset += 4;

    
    /*----------------------------------------------------------------------
                validate the toc first
    ----------------------------------------------------------------------*/
    if( ( toc >= QVP_RTP_EVRC_TOC_ERASURE ) ||
        ( stream->toc_len_table[ toc ] < 0 )  )
    {
      
      /*--------------------------------------------------------------------
                  Free the buffer if needed
      --------------------------------------------------------------------*/
      if( pkt->need_tofree )
      {
        qvp_rtp_free_buf( pkt );
      }
      
      QVP_RTP_ERR( " got unknown toc  support this \r\n", 0, 0, 0 ); 
      return( QVP_RTP_ERR_FATAL );
      
    }

    /*----------------------------------------------------------------------
                find the pkt len
    ----------------------------------------------------------------------*/
    len = stream->toc_len_table[ toc ];

    
    /*----------------------------------------------------------------------
       Double check for the len remaining
    ----------------------------------------------------------------------*/
    if( aud_len < len )
    {
      
      
      /*--------------------------------------------------------------------
        Malformed or corrupt packet
      --------------------------------------------------------------------*/
      QVP_RTP_ERR( " Insufficient data len inside the EVRC %d %d \r\n", \
                    aud_len, len, 0 ); 
      
      /*--------------------------------------------------------------------
                  Free the buffer if needed
      --------------------------------------------------------------------*/
      if( pkt->need_tofree )
      {
        qvp_rtp_free_buf( pkt );
      }
      
      return( QVP_RTP_ERR_FATAL );
    }
    

    /*--------------------------------------------------------------------
              try and alloc an audio buffer
    --------------------------------------------------------------------*/
    aud_buf = qvp_rtp_alloc_buf( QVP_RTP_POOL_AUDIO );
  
  
    /*--------------------------------------------------------------------
              See if we got a buffer
    --------------------------------------------------------------------*/
    if ( !aud_buf )
    {
  
      QVP_RTP_ERR( " Could not get an audio buffer \r\n", 0, 0, 0 );
  
      /*------------------------------------------------------------------
              Free if needed
      ------------------------------------------------------------------*/
      if ( pkt->need_tofree )
      {
        qvp_rtp_free_buf( pkt );
      }
      return( QVP_RTP_ERR_FATAL );
  
    }
    else
    {
      aud_buf->head_room = 0;
      aud_buf->len = len;
      
      /*--------------------------------------------------------------------
        Taken from RFC3558

        "
        The RTP timestamp is in 1/8000 of a second units
        for EVRC and SMV.  For any other vocoders that use this packet
        format, the timestamp unit needs to be defined explicitly. 

        "
        
        "
        When multiple codec data frames are present in a single RTP packet,
        the timestamp is that of the oldest data represented in the RTP
        packet. 
        "

        Remaining time stamps are calculated using packetization 
        interval ( 20 ms fixed for EVRC ) in this implementation

      --------------------------------------------------------------------*/
      
      aud_buf->tstamp = pkt->tstamp - ( ( hdr.count - 1 ) * 
                       stream->rx_ctx.pkt_interval );
      aud_buf->need_tofree = TRUE;
      aud_buf->seq = pkt->seq;
  
      qvp_rtp_memscpy( aud_buf->data,len, aud_data, len ); 
  
      
      /*--------------------------------------------------------------------
        If this is a silence frame or 1/8th frame mark it 
        After DTX we are considering only blank frames as silence frames. 
        -Marking 1/8th frame too since CRD is doing it

        We now indicate silence frames via the silence flag
      --------------------------------------------------------------------*/
      if( toc ==  QVP_RTP_EVRC_TOC_BLANK || 
          toc == QVP_RTP_EVRC_TOC_RATE_1_8 )
      {
        aud_buf->silence = TRUE;
      }
      else
      {
        aud_buf->silence = FALSE;
      }

      /*--------------------------------------------------------------------
           Mark frm_present bit
      --------------------------------------------------------------------*/
      aud_buf->frm_info.info.aud_info.frm_present = TRUE;

      QVP_RTP_MSG_MED( " RX EVRC Bundle Len = %d, timestamp = %d, \
                       marker = %d", aud_buf->len, aud_buf->tstamp, 
                       aud_buf->marker_bit );
                       
      QVP_RTP_MSG_MED_1( " RX EVRC Bundle Seq = %d", pkt->seq);

      
  
      /*------------------------------------------------------------------
                Ship the packet up

                If we could not ship it break;
      ------------------------------------------------------------------*/
      if( evrc_profile_config.rx_cb(aud_buf, usr_hdl ) != QVP_RTP_SUCCESS )
      {
        break;
      }
    }


    /*----------------------------------------------------------------------
              Decrement the frame count next no
    ----------------------------------------------------------------------*/
    hdr.count--;

    
    /*----------------------------------------------------------------------
            decrement the remaining audion len accordingly
    ----------------------------------------------------------------------*/
    aud_len -= len;
    
    /*----------------------------------------------------------------------
          If there is still life in this loop advance data pointer
    ----------------------------------------------------------------------*/
    if( hdr.count )
    {
      
      /*--------------------------------------------------------------------
            Advance the audio buffer
      --------------------------------------------------------------------*/
      aud_data += len;
     }
    
    
  } /* end of while hdr.count */

  
  /*----------------------------------------------------------------------
            Free the buffer if needed
  ----------------------------------------------------------------------*/
  if( pkt->need_tofree )
  {
    qvp_rtp_free_buf( pkt );
  }

  /*------------------------------------------------------------------------
     If at this moment hdr.count is not zero there is something wrong
  ------------------------------------------------------------------------*/
  if( hdr.count )
  {
    return( QVP_RTP_ERR_FATAL );
  }
  else
  {
    return( QVP_RTP_SUCCESS );
  }
  
} /* end of function qvp_rtp_evrc_profile_recv */

/*===========================================================================

FUNCTION  QVP_RTP_EVRC_PROFILE_CLOSE


DESCRIPTION
  Closes an already open bi directional channel inside the profile.

DEPENDENCIES
  None

ARGUMENTS IN
  hdl - handle of channel to close.

RETURN VALUE
  QVP_RTP_SUCCESS  - if we could send data. or an error code


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_evrc_profile_close
(

  qvp_rtp_profile_hdl_type     hdl           /* handle the profile */

)
{
  qvp_rtp_evrc_ctx_type *stream = ( qvp_rtp_evrc_ctx_type *) hdl; 
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
              If we get  a valid handle invalidate the index 

              Also check are we initialized yet ..?
  ------------------------------------------------------------------------*/
  if( hdl && evrc_initialized )
  {
    
    /*----------------------------------------------------------------------
        We cannot afford to loose buffers so when we are in the middle of
        reassmbling we need to free the whole chain
    ----------------------------------------------------------------------*/
    stream->valid = FALSE;
    
    /*----------------------------------------------------------------------
        Reset the stream context - if any stale values in there.
    ----------------------------------------------------------------------*/
    qvp_rtp_evrc_reset_stream( stream );
    
    return( QVP_RTP_SUCCESS );
  }
  else
  {
    return( QVP_RTP_ERR_FATAL );
  }


} /* end of function qvp_rtp_evrc_profile_close */ 

/*===========================================================================

FUNCTION  QVP_RTP_EVRC_RESET_TX_CTX


DESCRIPTION
  Reset the trasmitter context inside the stream

DEPENDENCIES
  None

ARGUMENTS IN
  stream to be reset

RETURN VALUE
  None


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL void qvp_rtp_evrc_reset_tx_ctx
( 
  qvp_rtp_evrc_ctx_type *stream 
)
{
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
      Reset the packet to packet to packet context
  ------------------------------------------------------------------------*/
  stream->tx_ctx.data_len = 0;
  stream->tx_ctx.frame_cnt = 0;
  stream->tx_ctx.marker = 0;
  stream->tx_ctx.all_silence_frames = TRUE;
  
} /* end of function qvp_rtp_evrc_reset_tx_ctx */
    
/*===========================================================================

FUNCTION  QVP_RTP_EVRC_RESET_STREAM 


DESCRIPTION
  Reset the context inside the stream

DEPENDENCIES
  None

ARGUMENTS IN
  stream to be reset

RETURN VALUE
  QVP_RTP_SUCCESS  - if we could send data. or an error code


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL void qvp_rtp_evrc_reset_stream
( 
  qvp_rtp_evrc_ctx_type *stream 
)
{
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
          Reset the transmit side
  ------------------------------------------------------------------------*/
  qvp_rtp_evrc_reset_tx_ctx( stream );

  /*------------------------------------------------------------------------
    prev_silence_frame is reset only when the whole stream is reset
    SO IT DOES NOT STRICTLY BELONG TO PREVIOUS FUNCTION.
  ------------------------------------------------------------------------*/
  stream->tx_ctx.prev_silence_frame = FALSE;

  /*------------------------------------------------------------------------
      add code to reset rx_ctx when needed if and when we do 
      interleaving 
  ------------------------------------------------------------------------*/
  
} /* end of function qvp_rtp_evrc_reset_stream */

/*===========================================================================

FUNCTION  QVP_RTP_PARSE_EVRC_FIXED_HDR 


DESCRIPTION
  Parse the fixed part of EVRC header inside trasmit buffer. This is 
  typically done for each bundled ( pkt received ).

DEPENDENCIES
  None

ARGUMENTS IN
  data - data to  be parsed
  len  - len of data contained

ARGUMENTS IN
  hdr - container for parsed values.

RETURN VALUE
  length of bytes chewed up. Zero on parse errors.


SIDE EFFECTS
  None.

===========================================================================*/

LOCAL uint16 qvp_rtp_parse_evrc_fixed_hdr
(
  
  uint8 *data,
  uint16 len,
  qvp_rtp_evrc_hdr_param_type *hdr
  
)
{
  uint16 parse_offset = 2;  /* 2 resrved bits on header */
/*------------------------------------------------------------------------*/
  
  /*------------------------------------------------------------------------
      If length is than the fixed header return 0
  ------------------------------------------------------------------------*/
  if( len < 2 )
  {
    return( 0 );
  }
  
  /*------------------------------------------------------------------------
  
    +=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
    |R|R| LLL | NNN | MMM |  Count  |  TOC  |  ...  |  TOC  |padding|
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  
    From RFC3558
  ------------------------------------------------------------------------*/
  
  /*------------------------------------------------------------------------
          parse the LLL (interleave count ); 
  ------------------------------------------------------------------------*/
  hdr->lll = b_unpackb( data, parse_offset, 3 );
  parse_offset += 3;

  /*------------------------------------------------------------------------
          Parse the  interleave index
  ------------------------------------------------------------------------*/
  hdr->nnn = b_unpackb( data, parse_offset, 3 );
  parse_offset += 3;
  
  /*------------------------------------------------------------------------
          Parse the mode 
  ------------------------------------------------------------------------*/
  hdr->mode = b_unpackb( data, parse_offset, 3 );
  parse_offset += 3;
     
  /*------------------------------------------------------------------------
          Parse the Count
  ------------------------------------------------------------------------*/
  hdr->count = b_unpackb( data, parse_offset, 5 );
  parse_offset += 5;

  /*------------------------------------------------------------------------
        return the fixed header len
  ------------------------------------------------------------------------*/
  return( 2 );
  
} /* end of  function qvp_rtp_parse_evrc_fixed_hdr */

/*===========================================================================

FUNCTION  QVP_RTP_EVRC_FORM_HEADER 


DESCRIPTION
  Preloads the trasmit buffer with the fixed EVRC paylod header

DEPENDENCIES
  None

ARGUMENTS IN
  stream to be stuffed with fixed header 

RETURN VALUE
   length of bytes loaded 


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL uint8 qvp_rtp_evrc_form_header
( 
  qvp_rtp_evrc_ctx_type *stream 
)
{
  uint16 offset = 0;
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
    Reset the maximum len of a EVRC pyld header
  ------------------------------------------------------------------------*/
  memset( stream->tx_ctx.op_packet, 0, QVP_RTP_EVRC_MAX_HDR_SZ );  
  
  /*------------------------------------------------------------------------ 
  
  +=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
  |R|R| LLL | NNN | MMM |  Count  |  TOC  |  ...  |  TOC  |padding|
  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

  From RFC3558

  ------------------------------------------------------------------------*/
  
  /*------------------------------------------------------------------------ 
        Add reserved bits
  ------------------------------------------------------------------------*/
  b_packb( 0, stream->tx_ctx.op_packet, offset, 2 ); 
  offset += 2;

  /*------------------------------------------------------------------------
      If we do not have interleave this header is static
  ------------------------------------------------------------------------*/
 if( !stream->stream_config.tx_interleave_on ) 
 {
   /*----------------------------------------------------------------------- 
     Add interleave len 
     LLL
   -----------------------------------------------------------------------*/
   b_packb( 0, stream->tx_ctx.op_packet, offset, 3 ); 
   offset += 3;
   /*----------------------------------------------------------------------- 
     Add interleave len 
     LLL
   -----------------------------------------------------------------------*/
   b_packb( 0, stream->tx_ctx.op_packet, offset, 3 ); 
   offset += 3;
   
 }
 else
 {
   
   /*-----------------------------------------------------------------------
        If interleave is asked cry and bail out
   -----------------------------------------------------------------------*/
   QVP_RTP_ERR( \
	  " qvp_rtp_evrc_form_header:: interleaving asked but not supported\r\n", \
                 0, 0, 0 ); 
   return(  0 );
 }

 /*-------------------------------------------------------------------------
    Add the mode feild .. we are not going to switch dynamically 

    So put invalid mode here
 -------------------------------------------------------------------------*/
 b_packb( QVP_RTP_EVRC_INVALID_MODE, stream->tx_ctx.op_packet, offset, 3 ); 
 offset += 3;
 
 
 /*-------------------------------------------------------------------------
      Add count
 -------------------------------------------------------------------------*/
 /*
 From RFC 3558
   Frame Count (Count): 5 bits
      The number of ToC fields (and vocoder frames) present in the
      packet is the value of the frame count field plus one,
      hence we need to reduce 1 while packing
 */
 b_packb((stream->stream_config.tx_bundle_size - 1), stream->tx_ctx.op_packet, 
          offset, 5 ); 
 offset += 5;
 
 
 return( 2 /* total header size */ );
 

} /* end of function qvp_rtp_evrc_form_header */

/*===========================================================================

FUNCTION  QVP_RTP_EVRC_FIND_TOC


DESCRIPTION
   Finds the matching toc for a particular frame length.

DEPENDENCIES
  None

ARGUMENTS IN
  len - length of the parload

RETURN VALUE
  toc type


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_evrc_toc_type qvp_rtp_evrc_find_toc
( 
  qvp_rtp_evrc_ctx_type       *stream,
  uint16 len
)
{
  int8 i; /* index variable */
/*------------------------------------------------------------------------*/

  if( !stream )
  {
    return( QVP_RTP_EVRC_TOC_INVALID );
  }
  
  /*------------------------------------------------------------------------
      Lookup the len table for a matching TOC
  ------------------------------------------------------------------------*/
  for( i = 0; i < QVP_RTP_EVRC_TOC_ERASURE; i ++ )
  {
    /* return the index of the table when we get a match */ 
    if( stream->toc_len_table[i]  == len  )
    {
      return( ( qvp_rtp_evrc_toc_type ) i );
    }
    
  } /* end of for i = 0 */

  return( QVP_RTP_EVRC_TOC_INVALID ); 

} /* end of function qvp_rtp_evrc_find_toc */

/*===========================================================================

FUNCTION QVP_RTP_EVRC_PROFILE_COPY_CONFIG 


DESCRIPTION

  This function reads out the default configuration of EVRC
  payload format. The result is conveyed by populating the passed in 
  structure on return.

DEPENDENCIES
  None

ARGUMENTS IN
    payload        - payload type for which the default configuration 
                     is being read out. This is not used by EVRC. Some 
                     other profiles uses it as they support multiple 
                     payload formats.
    
ARGUMENTS OUT
    payld_config   - pointer to configuration structure. On return this 
                     structure will be populated with the default values
                     for the profile.
    



RETURN VALUE
  QVP_RTP_SUCCESS  - operation was succesful.
                    appropriate error code otherwise


SIDE EFFECTS
  payld_config structure will be populated with the default values 
  for the particular profile.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_evrc_profile_copy_config
(
  qvp_rtp_payload_type        payld,
  qvp_rtp_evrc_intl_bund_sdp_config_type  *evrc_tx_config,
  qvp_rtp_evrc_intl_bund_sdp_config_type  *evrc_rx_config,
  qvp_rtp_evrc_config_type    *evrc_config
)
{

  /*------------------------------------------------------------------------
    Memset the whole configuration before we proceed
  ------------------------------------------------------------------------*/
  memset( evrc_rx_config, 0, 
          sizeof( qvp_rtp_evrc_intl_bund_sdp_config_type ) ); 
  memset( evrc_tx_config, 0, 
          sizeof( qvp_rtp_evrc_intl_bund_sdp_config_type ) );
  
  /*------------------------------------------------------------------------
      Copy each and every FMTP and flag them as TRUE
  ------------------------------------------------------------------------*/

  
  /*------------------------------------------------------------------------
           RX   Intrleave len 
  ------------------------------------------------------------------------*/
  evrc_rx_config->maxinterleave = evrc_config->rx_maxinterleave;
  evrc_rx_config->maxinterleave_valid = TRUE;

  evrc_rx_config->valid = TRUE;

  /*------------------------------------------------------------------------
           TX   Intrleave len 
  ------------------------------------------------------------------------*/
  evrc_tx_config->maxinterleave = evrc_config->rx_maxinterleave;
  evrc_tx_config->maxinterleave_valid = TRUE;

  evrc_tx_config->valid = TRUE;


  return( QVP_RTP_SUCCESS );
  
} /* end of function qvp_rtp_evrc_profile_copy_config */ 

/*===========================================================================

FUNCTION  QVP_RTP_EVRC_PROFILE_SHUTDOWN


DESCRIPTION
  Shuts this module down and flag intialization as false

DEPENDENCIES
  None

ARGUMENTS IN
  len - length of the parload

RETURN VALUE
  toc type


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL void qvp_rtp_evrc_profile_shutdown( void )
{
  uint32 i;
/*------------------------------------------------------------------------*/


  /*------------------------------------------------------------------------
    Lets not do multiple shutdown
  ------------------------------------------------------------------------*/
  if( !evrc_initialized )
  {
    return;
  }
  
  /*------------------------------------------------------------------------
      Walk through the stream array and close all channels
  ------------------------------------------------------------------------*/
  for( i = 0; i < evrc_profile_config.num_streams; i ++ )
  {

    
    /*----------------------------------------------------------------------
        If this stream is open close it now
    ----------------------------------------------------------------------*/
    if ( qvp_rtp_evrc_array[ i ].valid )
    {
      qvp_rtp_evrc_profile_close( ( qvp_rtp_profile_hdl_type ) i );
    }
    
  } /* end of for i = 0  */
  
  /*------------------------------------------------------------------------
    free the array of streams
  ------------------------------------------------------------------------*/
  qvp_rtp_free( qvp_rtp_evrc_array   );

  /*------------------------------------------------------------------------
    Flag array as NULL and init as FALSE
  ------------------------------------------------------------------------*/
  qvp_rtp_evrc_array = NULL; 
  evrc_initialized  = FALSE;
  
  
} /* end of function qvp_rtp_evrc_profile_shutdown  */

#endif /* end of FEATURE_QVPHONE_RTP */
