/*=*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                         QVP_RTP_H263_PROFILE . C

GENERAL DESCRIPTION

  This file contains the implementation of H263 profile. This file takes 
  care of payload fragmentation, reassebmly etc.

EXTERNALIZED FUNCTIONS
  None. Exposed through a profile table. All the functions are static
  but exposed through function pointers.


INITIALIZATION AND SEQUENCING REQUIREMENTS

  None of the externalized functions will function before the task is 
  spawned.


  Copyright (c) 2004 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
  QUALCOMM Proprietary.  Export of this technology or software is
  regulated by the U.S. Government. Diversion contrary to U.S. law prohibited.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*=*/

/*===========================================================================

$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/ims/ps_media/rtp_profile/src/qvp_rtp_h263_profile.c#1 $ $DateTime: 2016/03/28 23:03:22 $ $Author: mplcsds1 $

                            EDIT HISTORY FOR FILE


when        who    what, where, why
--------    ---    ----------------------------------------------------------
16/04/08    apr    KW critical warning fixes
01/03/08    grk    Set profile reassembly handle for stream only if rx 
                   configuration is requested.
07/05/07    apr    Modified to fix 7200 warnings
06/14/07    apr    Removed reset reassembly in open profile
01/11/07    apr    Move reassembly part into a separate module
05/21/06    apr    Modified H263+(2429) send func to remove pic start code
                   of 16 zero-valued bits
12/20/06    apr    Fix for wrap around reassembling 
07/13/06    uma    Updated qvp_rtp_h263_profile_table for config validate 
06/14/06    apr    Added code to support H263+ RFC 2429 - 
                   1) profile table qvp_rtp_h263_2429_profile_table
                   2) function to receive & send H263+ RTP packets 
                      qvp_rtp_h263_2429_profile_recv
                      qvp_rtp_h263_2429_profile_send
                   Modifying send for H263(2190) to copy payload header
05/05/06    apr    Including RTP code in feature FEATURE_QVPHONE_RTP
04/17/06    apr    Added support for handling partial frames based on 
                   timestamp, and inserting error concealment bit pattern
                   for missing pkts
03/20/06    uma    Replaced all malloc and free with wrappers 
                   qvp_rtp_malloc & qvp_rtp_free 
08/13/05    srk    Adding support ModA, ModB and Mod C parsing
06/04/05    srk    Initial Creation.
===========================================================================*/
#include "ims_variation.h"
#include "customer.h"
#ifdef FEATURE_QVPHONE_RTP


#ifdef  RTP_FILE_NUMBER 
  #undef RTP_FILE_NUMBER 
#endif 
#define RTP_FILE_NUMBER 32
/*===========================================================================
                     INCLUDE FILES FOR MODULE
===========================================================================*/
#include <comdef.h>               /* common target/c-sim defines */
#include <string.h>               /* memory routines */
#include <stdlib.h>               /* for malloc */
#include <comdef.h>               /* common target/c-sim defines */
#include "qvp_rtp_api.h"          /* return type propagation */
#include "qvp_rtp_buf.h"
#include "qvp_rtp_profile.h"      /* profile template data type */
#include "qvp_rtp_h263_profile.h"
#include "qvp_rtp_buf.h"          /* for memory management */
#include "qvp_rtp_log.h"
#include "stdio.h"
#include <bit.h>
#include <list.h>


/*===========================================================================
                    LOCAL STATIC FUNCTIONS 
===========================================================================*/

LOCAL qvp_rtp_status_type qvp_rtp_h263_profile_init
(
  qvp_rtp_profile_config_type *config
);

LOCAL qvp_rtp_status_type qvp_rtp_h263_profile_open
(
  qvp_rtp_profile_hdl_type     *hdl,         /* handle which will get 
                                              * populated with a new profile
                                              * stream handle
                                              */
  qvp_rtp_profile_usr_hdl_type  usr_hdl     /* user handle for any cb */
  
);

LOCAL qvp_rtp_status_type qvp_rtp_h263_profile_read_deflt_config
(
  qvp_rtp_payload_type        payld, /* not used */ 
  qvp_rtp_payload_config_type *payld_config 
);

LOCAL qvp_rtp_status_type qvp_rtp_h263_profile_configure
(
  qvp_rtp_profile_hdl_type      hdl,     
  qvp_rtp_payload_config_type   *payld_config 
);

LOCAL qvp_rtp_status_type qvp_rtp_h263_profile_send
(
  qvp_rtp_profile_hdl_type     hdl,          /* handle created while open */
  qvp_rtp_profile_usr_hdl_type usr_hdl,      /* user handle for tx cb */
  qvp_rtp_buf_type             *pkt          /* packet to be formatted 
                                              * through the profile
                                              */
);

LOCAL qvp_rtp_status_type qvp_rtp_h263_2429_profile_send
(

  qvp_rtp_profile_hdl_type     hdl,          /* handle created while open */
  qvp_rtp_profile_usr_hdl_type usr_hdl,      /* user handle for rx cb */
  qvp_rtp_buf_type             *pkt          /* packet to be formatted 
                                              * through the profile
                                              */

);

LOCAL qvp_rtp_status_type qvp_rtp_h263_profile_recv
(

  qvp_rtp_profile_hdl_type     hdl,          /* handle created while open */
  qvp_rtp_profile_usr_hdl_type usr_hdl,      /* user handle for rx cb */
  qvp_rtp_buf_type             *pkt          /* packet parsed and removed
                                              * the profile formatting
                                              */

);

LOCAL qvp_rtp_status_type qvp_rtp_h263_2429_profile_recv
(

  qvp_rtp_profile_hdl_type     hdl,          /* handle created while open */
  qvp_rtp_profile_usr_hdl_type usr_hdl,      /* user handle for rx cb */
  qvp_rtp_buf_type             *pkt          /* packet parsed and removed
                                              * the profile formatting
                                              */

);
LOCAL qvp_rtp_status_type qvp_rtp_h263_profile_close
(

  qvp_rtp_profile_hdl_type     hdl           /* handle the profile */

);

LOCAL void qvp_rtp_h263_profile_shutdown( void );

LOCAL qvp_rtp_status_type qvp_rtp_h263_fragment_and_send
(
  qvp_rtp_profile_hdl_type     hdl,          /* handle created while open */
  qvp_rtp_profile_usr_hdl_type usr_hdl,      /* user handle for tx cb */
  qvp_rtp_buf_type             *pkt          /* packet to be formatted 
                                              * through the profile
                                              */
);


LOCAL qvp_rtp_status_type  qvp_rtp_h263_stuff_single_au_header
( 
  qvp_rtp_profile_hdl_type    hdl,          /* handle created while open */
  qvp_rtp_buf_type            *pkt, 
  uint16                      au_len,
  boolean                     marker
);


LOCAL void qvp_rtp_h263_reset_stream
(
  qvp_rtp_h263_ctx_type *stream
);

LOCAL qvp_rtp_status_type qvp_rtp_h263_profile_copy_config
(
  qvp_rtp_payload_config_type *payld_config,
  qvp_rtp_h263_config_type    *h263_config
);

LOCAL qvp_rtp_status_type qvp_rtp_h263_profile_config_rx_param
(
  
  qvp_rtp_h263_ctx_type       *stream,
  qvp_rtp_payload_config_type *payld_config 
);

LOCAL qvp_rtp_status_type qvp_rtp_h263_profile_config_tx_param
(
  
  qvp_rtp_h263_ctx_type       *stream,
  qvp_rtp_payload_config_type *payld_config 
);


/*===========================================================================
                    LOCAL STATIC DATA 
===========================================================================*/
/*--------------------------------------------------------------------------
    Table of accessors for the profile. This need to be populated in
    the table entry at qvp_rtp_profole.c. All these functions will 
    automatically link to the RTP stack by doing so.
--------------------------------------------------------------------------*/
qvp_rtp_profile_type qvp_rtp_h263_profile_table = 
{
  qvp_rtp_h263_profile_init,             /* LOACAL initialization routine  */
  qvp_rtp_h263_profile_open,             /* LOCAL function to open channel */
  qvp_rtp_h263_profile_send,             /* LOCAL function to send a pkt   */
  qvp_rtp_h263_profile_recv,             /* LOCAL function to rx a nw pkt  */
  qvp_rtp_h263_profile_close,            /* LOCAL function to close channel*/
  qvp_rtp_h263_profile_read_deflt_config,/* read the defaults              */
  NULL,                                  /* read config not supported      */
  NULL,                                  /* validate_config_rx not present */
  NULL,                                  /* validate_config_tx not present */
  qvp_rtp_h263_profile_configure,        /* configure the payld format     */ 
  qvp_rtp_h263_profile_shutdown          /* LOCAL function to shut down    */

};

/*--------------------------------------------------------------------------
    Table of accessors for the profile H263+ of RFC2429. 
    This need to be populated in the table entry at qvp_rtp_profole.c. 
    All these functions will automatically link to the RTP stack by 
    doing so.
--------------------------------------------------------------------------*/
qvp_rtp_profile_type qvp_rtp_h263_2429_profile_table = 
{
  qvp_rtp_h263_profile_init,             /* LOACAL initialization routine  */
  qvp_rtp_h263_profile_open,             /* LOCAL function to open channel */
  qvp_rtp_h263_2429_profile_send,        /* LOCAL function to send a pkt   */
  qvp_rtp_h263_2429_profile_recv,        /* LOCAL function to rx a nw pkt  */
  qvp_rtp_h263_profile_close,            /* LOCAL function to close channel*/
  qvp_rtp_h263_profile_read_deflt_config,/* read the defaults              */
  NULL,                                  /* read config not supported      */
  NULL,                                  /* validate_config_rx not present */
  NULL,                                  /* validate_config_tx not present */
  qvp_rtp_h263_profile_configure,        /* configure the payld format     */ 
  qvp_rtp_h263_profile_shutdown          /* LOCAL function to shut down    */

};

/*--------------------------------------------------------------------------
  Flags module initialization  
--------------------------------------------------------------------------*/ 
LOCAL boolean h263_initialized;

/*--------------------------------------------------------------------------
        Stores the configuration requested by the app. 

        This is generic fconfiguration which hooks the profile to some
        user (RTP stack context ). Created off a template for all
        profiles.
--------------------------------------------------------------------------*/
LOCAL  qvp_rtp_profile_config_type h263_profile_config;

/*--------------------------------------------------------------------------
        Stream context array.  Responsible for each bidirectional 
        connections
--------------------------------------------------------------------------*/
LOCAL qvp_rtp_h263_ctx_type *qvp_rtp_h263_array = NULL;

/*--------------------------------------------------------------------------
     Configuration pertaining payload formatting for H263 
--------------------------------------------------------------------------*/
LOCAL qvp_rtp_h263_config_type h263_stream_config;

/*===========================================================================

FUNCTION QVP_RTP_H263_PROFILE_INIT 


DESCRIPTION
  Initializes data structures and resources used by the H263 profile.

DEPENDENCIES
  None

ARGUMENTS IN
  config - pointer to profile configuration requested.

RETURN VALUE
  QVP_RTP_SUCCESS  - if we could initialize the profile. or an error code


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_h263_profile_init
(
  qvp_rtp_profile_config_type *config
)
{

/*------------------------------------------------------------------------*/


  
  /*------------------------------------------------------------------------
    If this module is already inited then just return success
  ------------------------------------------------------------------------*/
  if( h263_initialized )
  {
    return( QVP_RTP_SUCCESS );
  }

  /*------------------------------------------------------------------------
    Init if we get a valid config  and appropriate call backs 
    for the basic operation of the profile.
  ------------------------------------------------------------------------*/
  if( config && config->rx_cb && config->tx_cb )
  {
    qvp_rtp_memscpy( &h263_profile_config, sizeof( h263_profile_config ), config, sizeof( h263_profile_config ) );
  }
  else
  {
    QVP_RTP_ERR( "qvp_rtp_h263_profile_init:: wrong parameters", 0, 0, 0 );
    return( QVP_RTP_WRONG_PARAM );
  }

  qvp_rtp_h263_array = qvp_rtp_malloc(  sizeof( qvp_rtp_h263_ctx_type  )
                                  * config->num_streams );

  if( qvp_rtp_h263_array == NULL )
  {
   // QVP_RTP_ERR( "Could not allocate streams qvp_rtp_h263_array", 0, 0, 0 );
    return( QVP_RTP_ERR_FATAL );
  }
  else
  {
    memset( qvp_rtp_h263_array, 0, sizeof( qvp_rtp_h263_ctx_type  )
                              * config->num_streams );
  }


  /*------------------------------------------------------------------------
    Initial default configuration  
  ------------------------------------------------------------------------*/



  h263_stream_config.rx_err_concl_support = FALSE; /* Error concealment 
                                                    * support is not present
                                                    * by default 
                                                    */

  /*------------------------------------------------------------------------
    Flag the module initialization
  ------------------------------------------------------------------------*/
  h263_initialized = TRUE;

  return( QVP_RTP_SUCCESS );

}/* end of function qvp_rtp_h263_profile_init */

/*===========================================================================

FUNCTION  QVP_RTP_H263_PROFILE_SEND


DESCRIPTION
  request to send specified buffer through this profile.

DEPENDENCIES
  None

ARGUMENTS IN
  hdl     - handle into the profile. 
  usr_hdl - handle to use when we call send function using the registered
            tx callback
  pkt     - packet of data to be sent.

RETURN VALUE
  QVP_RTP_SUCCESS  - if we could send data. or an error code


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_h263_profile_send
(
  qvp_rtp_profile_hdl_type     hdl,          /* handle created while open */
  qvp_rtp_profile_usr_hdl_type usr_hdl,      /* user handle for tx cb */
  qvp_rtp_buf_type             *pkt          /* packet to be formatted 
                                              * through the profile
                                              */
)
{
/*------------------------------------------------------------------------*/


  /*------------------------------------------------------------------------
    If no tx cb or uninitialized bail out
  ------------------------------------------------------------------------*/
  if( !h263_profile_config.tx_cb || !h263_initialized )
  {
    return( QVP_RTP_ERR_FATAL );
  }
  else
  {
    
    /*----------------------------------------------------------------------
       Copy payload header
    ----------------------------------------------------------------------*/
    if( pkt->codec_hdr.valid && pkt->codec_hdr.hdr_len > 0 )
    {
      if( pkt->head_room < pkt->codec_hdr.hdr_len || 
           pkt->codec_hdr.hdr_len > sizeof( pkt->codec_hdr.codec_hdr) )
      {
        QVP_RTP_ERR( " qvp_rtp_h263_profile_send : \
                        not enough head room \r\n", 0, 0, 0 );
        return( QVP_RTP_ERR_FATAL );
      }
      pkt->len += pkt->codec_hdr.hdr_len;      
      pkt->head_room -= pkt->codec_hdr.hdr_len;
      qvp_rtp_memscpy( pkt->data + pkt->head_room , pkt->codec_hdr.hdr_len,
              pkt->codec_hdr.codec_hdr, pkt->codec_hdr.hdr_len );
    }
    
    /*----------------------------------------------------------------------
       reurn the return value of the tx function 
    ----------------------------------------------------------------------*/
    return( h263_profile_config.tx_cb( pkt, usr_hdl  ) );
  }
  
} /* end of function qvp_rtp_h263_profile_send */

/*===========================================================================

FUNCTION  QVP_RTP_H263_PROFILE_OPEN 


DESCRIPTION
    Requests to open a bi directional channel within the profile.

DEPENDENCIES
  None

ARGUMENTS IN
  usr_hdl       - handle to use when we call send function using the 
                  registered tx callback
  stream_param  - stream parameters applies to profiles with transport 
                  integrated. Its not planning to use the RTP.

ARGUMENTS OUT
  hdl - on success we write the handle to the profiile into the ** passed
  
RETURN VALUE
  QVP_RTP_SUCCESS  - if we could send data. or an error code


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_h263_profile_open
(
  qvp_rtp_profile_hdl_type     *hdl,         /* handle which will get 
                                              * populated with a new profile
                                              * stream handle
                                              */
  qvp_rtp_profile_usr_hdl_type  usr_hdl     /* user handle for any cb */
)
{
  uint32 i;   /* index variable */
/*------------------------------------------------------------------------*/


  /*------------------------------------------------------------------------
    look at an idle stream 
  ------------------------------------------------------------------------*/
  for( i = 0;  i < h263_profile_config.num_streams; i++ )
  {
    if( !qvp_rtp_h263_array[ i ].valid )
    {
      qvp_rtp_h263_array[ i ].valid = TRUE;
      *hdl = &qvp_rtp_h263_array[ i ]; 
      
      /*--------------------------------------------------------------------
        copy the H263 configuration to the  stream_ctx 
      --------------------------------------------------------------------*/
      qvp_rtp_memscpy( &qvp_rtp_h263_array[i].stream_config,sizeof(qvp_rtp_h263_config_type), &h263_stream_config,
               sizeof( qvp_rtp_h263_array[i].stream_config ));

      /*--------------------------------------------------------------------
        Initialize reassembly handle, 
        create it when required during config
      --------------------------------------------------------------------*/
      qvp_rtp_h263_array[i].rx_ctx.reassem_hdl = NULL;
      
      return( QVP_RTP_SUCCESS );

    }
  } /* maximum no of streams */

  return( QVP_RTP_NORESOURCES );

} /* end of function qvp_rtp_h263_profile_open */

/*===========================================================================

FUNCTION  QVP_RTP_H263_PROFILE_RECV 


DESCRIPTION
   The function which the RTP framework will call upon arrival of data from 
   NW. RTP header is stripped but parameters are passed.

DEPENDENCIES
  None

ARGUMENTS IN
  hdl           - hdl into the profile.
  usr_hdl       - handle to use when we call send function using the 
                  registered rx callback
  stream_param  - stream parameters applies to profiles with transport 
                  integrated. Its not planning to use the RTP.

  
RETURN VALUE
  QVP_RTP_SUCCESS  - if we could send data. or an error code


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_h263_profile_recv
(

  qvp_rtp_profile_hdl_type     hdl,          /* handle created while open */
  qvp_rtp_profile_usr_hdl_type usr_hdl,      /* user handle for rx cb */
  qvp_rtp_buf_type             *pkt          /* packet parsed and removed
                                              * the profile formatting
                                              */

)
{
  qvp_rtp_h263_ctx_type *stream = ( qvp_rtp_h263_ctx_type *) hdl; 
  uint16                          header_len = 0;
  uint8*                          hdr = pkt->data + pkt->head_room;
  qvp_rtp_status_type status          = QVP_RTP_SUCCESS;
  qvp_rtp_buf_type        *app_buf    = NULL;
/*------------------------------------------------------------------------*/
 
  /*------------------------------------------------------------------------
    If the module is not initialized bail out
  ------------------------------------------------------------------------*/
  if ( !h263_initialized || !stream ) 
  {
      return( QVP_RTP_ERR_FATAL );
    
  }
  
  QVP_RTP_MSG_MED( "qvp_rtp_h263_profile_recv: SEQ=%d, TS=%lu, Mark=%d",
                     pkt->seq, pkt->tstamp, pkt->marker_bit );

  /*----------------------------------------------------------------------
        Skip over the frame headers
  ----------------------------------------------------------------------*/
  if( !(*hdr & 0x80) )
  {
    /*----------------------------------------------------------------------
      This is a MOD A header
    ----------------------------------------------------------------------*/
    header_len  = QVP_RTP_H263_PYLD_HDR_LEN_MODA;
  }
  else
  {
    /*----------------------------------------------------------------------
     F BIT is one see for P BIT
    ----------------------------------------------------------------------*/
    if( !(*hdr & 0x40) )
    {
      /*--------------------------------------------------------------------
        F = 1 , P = 0; Mod B
      --------------------------------------------------------------------*/
      header_len  = QVP_RTP_H263_PYLD_HDR_LEN_MODB;
    }
    else
    {

      /*--------------------------------------------------------------------
        F  = 1 , p = 1 mode C
      --------------------------------------------------------------------*/
      header_len  = QVP_RTP_H263_PYLD_HDR_LEN_MODC;
    }
  }

  if( pkt->len < header_len )
  {
    qvp_rtp_free_buf( pkt );
    return( QVP_RTP_ERR_FATAL );
  }

  pkt->head_room += header_len ;
  pkt->len -= header_len;

  /*------------------------------------------------------------------------
    Pass the packet to next level if the stream is in fragment mode
  ------------------------------------------------------------------------*/
  if( stream->rx_ctx.output_mode == QVP_RTP_OUTPUT_FRAG_MODE )
  {
    stream->rx_ctx.last_ts = pkt->tstamp;
    stream->rx_ctx.last_seq = stream->rx_ctx.last_seq_sent = pkt->seq;

    return( h263_profile_config.rx_cb( pkt, usr_hdl ) );

  }

  /*------------------------------------------------------------------------
    Check for timestamp wrap around and Seq no wrap around
    If it doesnt fall in either of these cases its a delayed packet.
  ------------------------------------------------------------------------*/
  if( ( ( pkt->tstamp < stream->rx_ctx.last_ts ) && 
        ( stream->rx_ctx.last_ts - pkt->tstamp < 
                 QVP_RTP_H263_TS_WRAP_LIMIT )  /* !( TS wrap around )*/
      )  ||
      ( ( pkt->seq < stream->rx_ctx.last_seq_sent ) &&
        ( stream->rx_ctx.last_seq_sent - pkt->seq <
               ( QVP_RTP_POOL_NW_SIZE * 10 ) ) /* !( Seqno wrap around )*/
      ) 
    )
  {
    /*----------------------------------------------------------------------
      update the statistics counter
    ----------------------------------------------------------------------*/
    stream->rx_ctx.drop_cnt++;

    QVP_RTP_ERR( "Delayed packet dropped SEQ: %d, TS: %d", \
                stream->rx_ctx.last_seq_sent ,\
                stream->rx_ctx.last_ts, 0 );

    /*----------------------------------------------------------------------
       Drop the packet for now, Cleanup and return success
    ----------------------------------------------------------------------*/
    if( pkt->need_tofree )
    {
      qvp_rtp_free_buf( pkt );
    }
    
    return( QVP_RTP_SUCCESS );

  }
  /*------------------------------------------------------------------------
      the marker bit will flag a complete frame or part of the 
      frame or fragment

      1) is this a fragment ..?
      
      2) is this really an end of frame ( most possible while we are
      reassembling.
      
      3) We need to try and confirm this by try and parsing the first few 
      bytes. A layer violation but this is the only way we can get 
      a robust 2190 receiver.
  ------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
      Check if it belongs to the same frame or
       if we are starting a new reassembly
          Insert the packet received in the reassembly chain, 
  ------------------------------------------------------------------------*/
  if( ( pkt->tstamp == stream->rx_ctx.last_ts ) ||
      ( stream->rx_ctx.rx_state == QVP_RTP_H263_RX_IDLE ) )
  {
    QVP_RTP_MSG_LOW_1( "Insert H263 pkt in reassembly TS %d", pkt->tstamp);
    status = qvp_rtp_input_frag_in_reassembly( stream->rx_ctx.reassem_hdl, 
                                               pkt );

    stream->rx_ctx.rx_state = QVP_RTP_H263_RX_REASSMBLING;
    stream->rx_ctx.last_ts = pkt->tstamp;

  }
  
  /*------------------------------------------------------------------------
    Check if we reached end of frame - 
    If the marker bit of the packet is set. 
    If packet timestamp changes from currently reassembly timestamp
    Then Regroup the current reassembly 
  ------------------------------------------------------------------------*/
  if( ( status == QVP_RTP_SUCCESS || status == QVP_RTP_NORESOURCES ) &&
      ( ( pkt->marker_bit ) ||
        ( ( stream->rx_ctx.rx_state == QVP_RTP_H263_RX_REASSMBLING ) && 
          ( pkt->tstamp != stream->rx_ctx.last_ts ) ) ) )
  {

    /*----------------------------------------------------------------------
       Update all statistcs before aborting the reassembly
    ----------------------------------------------------------------------*/
    stream->rx_ctx.drop_cnt += 
           qvp_rtp_get_reasm_drop_cnt( stream->rx_ctx.reassem_hdl );

    stream->rx_ctx.missing_pkt_cnt += 
           qvp_rtp_get_reasm_missng_cnt( stream->rx_ctx.reassem_hdl );

    /*----------------------------------------------------------------------
        regroup the chain in one VIDEO buffer 
        allocate a buffer to the list
    ----------------------------------------------------------------------*/
    app_buf = qvp_rtp_alloc_buf( QVP_RTP_POOL_VIDEO );

    if( app_buf )
    {
      QVP_RTP_MSG_LOW_1( "Get H263 frame TS %d", stream->rx_ctx.last_ts);
      status = qvp_rtp_get_reassembled_frame( stream->rx_ctx.reassem_hdl,
                                            app_buf, QVP_RTP_PKT_VIDEO_SIZE,
                                            stream->rx_ctx.last_seq_sent );

      if( ( status != QVP_RTP_SUCCESS ) && ( app_buf->need_tofree ) )
      {
         qvp_rtp_free_buf( app_buf );
         app_buf = NULL;
      }
    }
    else
    {
      status = QVP_RTP_NORESOURCES;

    } /* if no app_buf */

    stream->rx_ctx.last_seq_sent = 
             qvp_rtp_get_reasm_last_seq( stream->rx_ctx.reassem_hdl );

    
    /*--------------------------------------------------------------------
         Do not abort the reassembly here since there is no need to start a new 
         reassembly chain. 
         Hoping that we can recover from previous errors we will continue here 
         by sending a NULL frame to application
    --------------------------------------------------------------------*/
    if( ( status != QVP_RTP_SUCCESS ) && 
         ( pkt->tstamp == stream->rx_ctx.last_ts ) )
    {
      QVP_RTP_ERR( "Error ignored, sending NULL frame at TS %d", \
                                      stream->rx_ctx.last_ts, 0, 0 );
      return( h263_profile_config.rx_cb( app_buf, usr_hdl ) );
    }

    /*----------------------------------------------------------------------
       If we reach here, ignore other errors and move to next frame 
    ----------------------------------------------------------------------*/
    status = QVP_RTP_SUCCESS;

    /*----------------------------------------------------------------------
      Check if we have to start a new reassembly for next frame
      Then input the packet in a new reassembly chain
    ----------------------------------------------------------------------*/
    if( pkt->tstamp != stream->rx_ctx.last_ts )
    {
      /*--------------------------------------------------------------------
         Stop any previous reassembly here
      --------------------------------------------------------------------*/
      qvp_rtp_abort_reassembly( stream->rx_ctx.reassem_hdl );

      QVP_RTP_MSG_LOW_1( "Insert pkt in reassembly TS %d", pkt->tstamp);
      stream->rx_ctx.last_ts = pkt->tstamp;
      stream->rx_ctx.rx_state = QVP_RTP_H263_RX_REASSMBLING;
      status = qvp_rtp_input_frag_in_reassembly( stream->rx_ctx.reassem_hdl, 
                                                 pkt );
    }

  }  

  /*------------------------------------------------------------------------
    Update receiver context 
  ------------------------------------------------------------------------*/  
  stream->rx_ctx.last_ts = pkt->tstamp;

  /*------------------------------------------------------------------------
    Return for any fatal errors here, other errors like NORESOURCES can be skipped
  ------------------------------------------------------------------------*/
  if( status != QVP_RTP_SUCCESS && 
      status != QVP_RTP_NORESOURCES )
  {
    return( status );
  }
  
  /*------------------------------------------------------------------------
    If we formed a frame send it to application
  ------------------------------------------------------------------------*/
  if( app_buf )
  {
    QVP_RTP_MSG_MED_1( "H263 frame TS %d", stream->rx_ctx.last_ts);

    /*----------------------------------------------------------------------
       Update the state so that new frame can be started with next pkt
    ----------------------------------------------------------------------*/
    stream->rx_ctx.rx_state = QVP_RTP_H263_RX_IDLE;

    qvp_rtp_abort_reassembly( stream->rx_ctx.reassem_hdl );

    return( h263_profile_config.rx_cb( app_buf, usr_hdl ) );
  }

  return( QVP_RTP_SUCCESS );

} /* qvp_rtp_h263_profile_recv */

/*===========================================================================

FUNCTION  QVP_RTP_H263_PROFILE_CLOSE


DESCRIPTION
  Closes an already open bi directional channel inside the profile.

DEPENDENCIES
  None

ARGUMENTS IN
  hdl - handle of channel to close.

RETURN VALUE
  QVP_RTP_SUCCESS  - if we could send data. or an error code


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_h263_profile_close
(

  qvp_rtp_profile_hdl_type     hdl           /* handle the profile */

)
{
  qvp_rtp_h263_ctx_type *stream = ( qvp_rtp_h263_ctx_type *) hdl; 
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
    If the module is not initialized bail out
  ------------------------------------------------------------------------*/
  if ( !h263_initialized ) 
  {
      return( QVP_RTP_ERR_FATAL );
    
  }

  /*------------------------------------------------------------------------
    If we get  a valid hadle invalidate the index 
  ------------------------------------------------------------------------*/
  if( hdl )
  {
    /*----------------------------------------------------------------------
        We cannot afford to loose buffers so when we are in the middle of
        reassmbling we need to free the whole chain
    ----------------------------------------------------------------------*/
    qvp_rtp_close_reassem_ctx( stream->rx_ctx.reassem_hdl );
    stream->valid = FALSE;
  }
  return( QVP_RTP_SUCCESS );

} /* end of function qvp_rtp_h263_profile_close */ 

/*===========================================================================

FUNCTION QVP_RTP_H263_PROFILE_READ_DEFLT_CONFIG 


DESCRIPTION

  This function reads out the default configuration of H263
  payload format. The result is conveyed by populating the passed in 
  structure on return.

DEPENDENCIES
  None

ARGUMENTS IN
    payload        - payload type for which the default configuration 
                     is being read out. This is not used by H263. Some 
                     other profiles uses it as they support multiple 
                     payload formats.
    
ARGUMENTS OUT
    payld_config   - pointer to configuration structure. On return this 
                     structure will be populated with the default values
                     for the profile.
    



RETURN VALUE
  QVP_RTP_SUCCESS  - operation was succesful.
                    appropriate error code otherwise


SIDE EFFECTS
  payld_config structure will be populated with the default values 
  for the particular profile.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_h263_profile_read_deflt_config
(
  
  qvp_rtp_payload_type        payld, /* not used */ 
  qvp_rtp_payload_config_type *payld_config 
)
{

  /*------------------------------------------------------------------------
    If profile config is NULL return error
  ------------------------------------------------------------------------*/
  if( !payld_config )
  {

    return( QVP_RTP_ERR_FATAL );
  }
  

  payld_config->payload = payld;
  
  return( qvp_rtp_h263_profile_copy_config( payld_config, 
                                            &h263_stream_config  ) );



} /* end of function qvp_rtp_h263_profile_read_deflt_config */ 

/*===========================================================================

FUNCTION QVP_RTP_H263_PROFILE_CONFIGURE

DESCRIPTION

  This function payload configuration of a previously opened H263  
  channel.

DEPENDENCIES
  None

ARGUMENTS IN
    hdl            - handle to the opend channel
    payld_config   - pointer to configuration structure
    



RETURN VALUE
  QVP_RTP_SUCCESS  - operation was succesfull. 
  else - Attempted a configuration  which is not currently supported by 
         the implementation.


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_h263_profile_configure
(
  qvp_rtp_profile_hdl_type      hdl,     
  qvp_rtp_payload_config_type   *payld_config 
)
{
  qvp_rtp_h263_ctx_type *stream = ( qvp_rtp_h263_ctx_type *) hdl; 
  qvp_rtp_status_type status = QVP_RTP_SUCCESS;
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
    If profile config is NULL return error
  ------------------------------------------------------------------------*/
  if( !payld_config || !stream )
  {

    return( QVP_RTP_ERR_FATAL );
  }

  if( payld_config->config_rx_params.valid == TRUE )
  {
    status = qvp_rtp_h263_profile_config_rx_param( stream, payld_config );
  }
  /*------------------------------------------------------------------------
    Reassembly ctx not required for outbound stream.
  ------------------------------------------------------------------------*/
  if ( ( status == QVP_RTP_SUCCESS ) &&
      ( payld_config->chdir_valid == TRUE ) && 
      ( payld_config->ch_dir == QVP_RTP_CHANNEL_OUTBOUND ) &&
      ( stream->rx_ctx.reassem_hdl ) )
  {

      QVP_RTP_MSG_LOW_0( "Closing reassembly contxt not reqd");
      qvp_rtp_close_reassem_ctx( stream->rx_ctx.reassem_hdl ); 

  }

  return( QVP_RTP_SUCCESS );

} /* end of function qvp_rtp_h263_profile_configure */

/*===========================================================================

FUNCTION  QVP_RTP_H263_STUFF_SINGLE_AU_HEADER 


DESCRIPTION
      This function will form a single single AU header for the  packaging
      of generic H263 profile.

DEPENDENCIES
  None

ARGUMENTS IN
  hdl - handle of channel to close.
  pkt - packet strucutre where this header need to be stuffed in
  au_len - length of AU which will be added in the pay load
  marker - I-Frame/ silence on/off etc.

RETURN VALUE
  QVP_RTP_SUCCESS  - if we could send data. or an error code


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type  qvp_rtp_h263_stuff_single_au_header
( 
  qvp_rtp_profile_hdl_type   hdl,      /* handle created while open */
  qvp_rtp_buf_type          *pkt,     /* packet with head room */
  uint16                     au_len,   /* length of AU */
  boolean                    marker    /* marker bit */
)
{
/*------------------------------------------------------------------------*/


  return( QVP_RTP_SUCCESS );

} /* end of function qvp_rtp_h263_stuff_single_au_header */


/*===========================================================================

FUNCTION  QVP_RTP_H263_FRAGMENT_AND_SEND 


DESCRIPTION
  This function will fragment a large AU into small pieces for 
  MTU requirement of the N/W stack. Also IP fragmentation is not a 
  good thing. We need to be smarted here to align it to encoder slices
  and so on.

DEPENDENCIES
  None

ARGUMENTS IN
  hdl - handle of channel to close.
  usr_hdl - user context for calling txmit function
  pkt - packet strucutre where this header need to be stuffed in

RETURN VALUE
  QVP_RTP_SUCCESS  - if we could send data. or an error code


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_h263_fragment_and_send
(
  qvp_rtp_profile_hdl_type     hdl,          /* handle created while open */
  qvp_rtp_profile_usr_hdl_type usr_hdl,      /* user handle for tx cb */
  qvp_rtp_buf_type             *pkt          /* packet to be formatted 
                                              * through the profile
                                              */
)
{
  return( QVP_RTP_SUCCESS );

} /* end of function qvp_rtp_h263_fragment_and_send */

/*===========================================================================

FUNCTION qvp_rtp_h263_reset_stream 


DESCRIPTION
  Any time this function can be called to reset the context of an H263 
  stream. This function does not free the memory associated.

DEPENDENCIES
  None

ARGUMENTS IN
 stream - the stream for which the context should be reset 
 
ARGUMENTS OUT
  None
  

RETURN VALUE
  True on success FALSE on failure.

SIDE EFFECTS
  None.

===========================================================================*/
LOCAL void qvp_rtp_h263_reset_stream
(
  qvp_rtp_h263_ctx_type *stream
)
{

  /*------------------------------------------------------------------------
       reset the reassmbly context
  ------------------------------------------------------------------------*/
  if( stream->rx_ctx.reassem_hdl )
  {
    qvp_rtp_reset_reassem_ctx( stream->rx_ctx.reassem_hdl );
  }

  /*------------------------------------------------------------------------
    Will need to add interleaving and other things as well later
  ------------------------------------------------------------------------*/
    
  /*------------------------------------------------------------------------
      reset statstics counters
  ------------------------------------------------------------------------*/
  stream->rx_ctx.drop_cnt = 0;
  stream->rx_ctx.missing_pkt_cnt = 0;
  stream->rx_ctx.last_seq_sent = 0;
  stream->rx_ctx.last_seq = 0;
  stream->rx_ctx.last_ts = 0;
  stream->rx_ctx.rx_state = QVP_RTP_H263_RX_IDLE;
  stream->rx_ctx.output_mode = QVP_RTP_OUTPUT_FRAME_MODE;
    
  
} /* end of function qvp_rtp_h263_reset_stream */


/*===========================================================================

FUNCTION QVP_RTP_H263_PROFILE_CONFIG_RX_PARAM

DESCRIPTION

  This function payload will configuration of Rx part of a previously
  opened stream.  

DEPENDENCIES
  None

ARGUMENTS IN
    stream         - H263 stream which needs to be configured.
    payld_config   - pointer to configuration structure
    



RETURN VALUE
  QVP_RTP_SUCCESS  - operation was succesfull. 
  else - Attempted a configuration  which is not currently supported by 
         the implementation.


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_h263_profile_config_rx_param
(
  
  qvp_rtp_h263_ctx_type       *stream,
  qvp_rtp_payload_config_type *payld_config 
)
{
/*------------------------------------------------------------------------*/

  if( payld_config->config_rx_params.rx_rtp_param.jitter_valid )
  {
    stream->rx_ctx.output_mode = qvp_rtp_get_profile_output_mode( 
                payld_config->config_rx_params.rx_rtp_param.jitter_size );
  }

  /*------------------------------------------------------------------------
    Create a ressembly context if in Frame mode
  ------------------------------------------------------------------------*/
  if( stream->rx_ctx.output_mode == QVP_RTP_OUTPUT_FRAME_MODE )
  {
    if( !stream->rx_ctx.reassem_hdl )
    {
      if( ( stream->rx_ctx.reassem_hdl = qvp_rtp_create_reassem_ctx() ) 
                                          == NULL )
      {
        QVP_RTP_ERR( "Unable to allocate reassembly context",0,0,0 );
        return( QVP_RTP_NORESOURCES );
      }
    }
  }
  /*------------------------------------------------------------------------
    In fragment mode profile does not require a reassembly context, so 
    close it if it is already present
  ------------------------------------------------------------------------*/
  else if( stream->rx_ctx.output_mode == QVP_RTP_OUTPUT_FRAG_MODE )
  {
    if( stream->rx_ctx.reassem_hdl )
    {
      QVP_RTP_MSG_LOW_0( "Closing reassembly contxt not reqd");
      qvp_rtp_close_reassem_ctx( stream->rx_ctx.reassem_hdl ); 
    }
  }
  else
  {
    QVP_RTP_ERR( "Error in output mode ", 0, 0, 0 );
    return( QVP_RTP_ERR_FATAL );
  }

  return( QVP_RTP_SUCCESS );



} /* end of function qvp_rtp_h263_profile_config_rx_param */ 

/*===========================================================================

FUNCTION QVP_RTP_H263_PROFILE_CONFIG_TX_PARAM

DESCRIPTION

  This function payload will configuration of Tx part of a previously
  opened stream.  

DEPENDENCIES
  None

ARGUMENTS IN
    stream         - H263 stream which needs to be configured.
    payld_config   - pointer to configuration structure
    



RETURN VALUE
  QVP_RTP_SUCCESS  - operation was succesfull. 
  else - Attempted a configuration  which is not currently supported by 
         the implementation.


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_h263_profile_config_tx_param
(
  
  qvp_rtp_h263_ctx_type       *stream,
  qvp_rtp_payload_config_type *payld_config 
)
{
/*------------------------------------------------------------------------*/


  return( QVP_RTP_SUCCESS );

} /* end of function qvp_rtp_h263_profile_config_tx_param */

/*===========================================================================

FUNCTION QVP_RTP_H263_PROFILE_COPY_CONFIG 


DESCRIPTION

  This function reads out the default configuration of H263
  payload format. The result is conveyed by populating the passed in 
  structure on return.

DEPENDENCIES
  None

ARGUMENTS IN
    payload        - payload type for which the default configuration 
                     is being read out. This is used by H263. This is 
                     because if someone specifies the ES we will just 
                     configure the GENERIC to operate in header less 
                     mode.
                    
    
ARGUMENTS OUT
    payld_config   - pointer to configuration structure. On return this 
                     structure will be populated with the default values
                     for the profile.
    



RETURN VALUE
  QVP_RTP_SUCCESS  - operation was succesful.
                    appropriate error code otherwise


SIDE EFFECTS
  payld_config structure will be populated with the default values 
  for the particular profile.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_h263_profile_copy_config
(
  qvp_rtp_payload_config_type *payld_config,
  qvp_rtp_h263_config_type    *h263_config
)
{
/*------------------------------------------------------------------------*/


 
  return( QVP_RTP_SUCCESS );
} /* end of function qvp_rtp_h263_profile_copy_config */

/*===========================================================================

FUNCTION  QVP_RTP_H263_PROFILE_SHUTDOWN


DESCRIPTION
  Shuts this module down and flag intialization as false

DEPENDENCIES
  None

ARGUMENTS IN
  len - length of the parload

RETURN VALUE
  toc type


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL void qvp_rtp_h263_profile_shutdown( void )
{
  uint32 i;
/*------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
     If not inited or already shut down bail out
  ------------------------------------------------------------------------*/
  if( !h263_initialized )
  {
    return;
  }
  
  /*------------------------------------------------------------------------
      Walk through the stream array and close all channels
  ------------------------------------------------------------------------*/
  for( i = 0; i < h263_profile_config.num_streams; i ++ )
  {

    
    /*----------------------------------------------------------------------
        If this stream is open close it now
    ----------------------------------------------------------------------*/
    if ( qvp_rtp_h263_array[ i ].valid )
    {
      qvp_rtp_h263_profile_close( ( qvp_rtp_profile_hdl_type  ) i );
    }
    
  } /* end of for i = 0  */
  
  /*------------------------------------------------------------------------
    free the array of streams
  ------------------------------------------------------------------------*/
  qvp_rtp_free( qvp_rtp_h263_array   );

  /*------------------------------------------------------------------------
    Flag array as NULL and init as FALSE
  ------------------------------------------------------------------------*/
  qvp_rtp_h263_array = NULL; 
  h263_initialized  = FALSE;
  
} /* end of function qvp_rtp_h263_profile_shutdown  */

/*===========================================================================

FUNCTION  QVP_RTP_H263_PROFILE_RECV_2429 


DESCRIPTION
   The function which the RTP framework will call upon arrival of data from 
   NW. RTP header is stripped but parameters are passed.

DEPENDENCIES
  None

ARGUMENTS IN
  hdl           - hdl into the profile.
  usr_hdl       - handle to use when we call send function using the 
                  registered rx callback
  stream_param  - stream parameters applies to profiles with transport 
                  integrated. Its not planning to use the RTP.

  
RETURN VALUE
  QVP_RTP_SUCCESS  - if we could send data. or an error code


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_h263_2429_profile_recv
(

  qvp_rtp_profile_hdl_type     hdl,          /* handle created while open */
  qvp_rtp_profile_usr_hdl_type usr_hdl,      /* user handle for rx cb */
  qvp_rtp_buf_type             *pkt          /* packet parsed and removed
                                              * the profile formatting
                                              */

)
{
  qvp_rtp_h263_ctx_type *stream = ( qvp_rtp_h263_ctx_type *) hdl; 
  uint16                          header_len = 0;
  uint8*                          hdr = pkt->data + pkt->head_room;
  boolean                         pic_start_bit = FALSE;
  uint16                          extra_pic_header = 0;
  qvp_rtp_status_type status          = QVP_RTP_SUCCESS;
  qvp_rtp_buf_type        *app_buf    = NULL; 
/*------------------------------------------------------------------------*/
 
  /*------------------------------------------------------------------------
    If the module is not initialized bail out
  ------------------------------------------------------------------------*/
  if ( !h263_initialized || !stream ) 
  {
      return( QVP_RTP_ERR_FATAL );
    
  }
  
  /*----------------------------------------------------------------------
  16 bit header
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     |   RR    |P|V|   PLEN    |PEBIT|
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  ----------------------------------------------------------------------*/
 
  /*----------------------------------------------------------------------
     Payload header of length 2 bytes preceded payload data
  ----------------------------------------------------------------------*/
  header_len = QVP_RTP_H263_2429_PYLD_HDR_LEN;

  /*----------------------------------------------------------------------
        Check if p bit is set 
  ----------------------------------------------------------------------*/
  if( *hdr & QVP_RTP_H263_2429_PIC_START_MASK )
  {
    pic_start_bit = TRUE;
  }
  
  /*----------------------------------------------------------------------
    Check if v bit is set,and ignore the VRC byte since we are not
    supporting VRC       
  ----------------------------------------------------------------------*/
  if( *hdr & QVP_RTP_H263_2429_VRC_MASK )
  {
    header_len++;
  }
  
  /*----------------------------------------------------------------------
    Find extra picture header length p_len and increment the header 
    length 
  ----------------------------------------------------------------------*/
  extra_pic_header = ( (*(hdr + 1)) >> 3 ) || (*hdr & 0x01 << 5);  
  header_len += extra_pic_header;
  
  /*----------------------------------------------------------------------
    We are ignoring the PEBIT( number of bits to be ignored in pic hdr ),
    since we are not processing the pcture header, this needs to be 
    taken care of when we want to use the picture header for any decode
    functions.
  ----------------------------------------------------------------------*/
  if( extra_pic_header )
  { 
    /* process PEBIT */
  }

  if( pkt->len < header_len )
  {
    QVP_RTP_ERR( " qvp_rtp_h263_2429_profile_recv : pkt length %d not \
                   enough for header %d \r\n", pkt->len, header_len, 0 );
    qvp_rtp_free_buf( pkt );
    return( QVP_RTP_ERR_FATAL );
  }

  pkt->head_room += header_len;
  pkt->len -= header_len;

  /*----------------------------------------------------------------------
     2 bytes of zero bits must be added to payload starting if picture
     start bit is set, reusing the head room bytes for 2 bytes
  ----------------------------------------------------------------------*/
  if( pic_start_bit )
  {
    pkt->data[pkt->head_room - 2] = 0x00;
    pkt->data[pkt->head_room - 1] = 0x00;
    pkt->head_room -= 2;
    pkt->len += 2;
  }
  
  /*------------------------------------------------------------------------
    Pass the packet to next level if the stream is in fragment mode
  ------------------------------------------------------------------------*/
  if( stream->rx_ctx.output_mode == 
                                        QVP_RTP_OUTPUT_FRAG_MODE )
  {
    return( h263_profile_config.rx_cb( pkt, usr_hdl ) );
  }

  /*------------------------------------------------------------------------
    Check for timestamp wrap around and Seq no wrap around
    If it doesnt fall in either of these cases its a delayed packet.
  ------------------------------------------------------------------------*/
  if( ( ( pkt->tstamp < stream->rx_ctx.last_ts ) && 
        ( stream->rx_ctx.last_ts - pkt->tstamp < 
                 QVP_RTP_H263_TS_WRAP_LIMIT )  /* !( TS wrap around )*/
      )  ||
      ( ( pkt->seq < stream->rx_ctx.last_seq_sent ) &&
        ( stream->rx_ctx.last_seq_sent - pkt->seq <
               ( QVP_RTP_POOL_NW_SIZE * 10 ) ) /* !( Seqno wrap around )*/
      ) 
    )
  {
    /*----------------------------------------------------------------------
      update the statistics counter
    ----------------------------------------------------------------------*/
    stream->rx_ctx.drop_cnt++;

    QVP_RTP_ERR( "Delayed packet dropped SEQ: %d, TS: %d", \
                stream->rx_ctx.last_seq_sent ,\
                stream->rx_ctx.last_ts, 0 );

    /*----------------------------------------------------------------------
       Drop the packet for now, Cleanup and return success
    ----------------------------------------------------------------------*/
    if( pkt->need_tofree )
    {
      qvp_rtp_free_buf( pkt );
    }
    
    return( QVP_RTP_SUCCESS );

  }
  /*------------------------------------------------------------------------
      the marker bit will flag a complete frame or part of the 
      frame or fragment

      1) is this a fragment ..?
      
      2) is this really an end of frame ( most possible while we are
      reassembling.
      
      3) We need to try and confirm this by try and parsing the first few 
      bytes. A layer violation but this is the only way we can get 
      a robust 2190 receiver.
  ------------------------------------------------------------------------*/

  /*------------------------------------------------------------------------
      Check if it belongs to the same frame or
       if we are starting a new reassembly
          Insert the packet received in the reassembly chain, 
  ------------------------------------------------------------------------*/
  if( ( pkt->tstamp == stream->rx_ctx.last_ts ) ||
      ( stream->rx_ctx.rx_state == QVP_RTP_H263_RX_IDLE ) )
  {
    QVP_RTP_MSG_LOW_1( "Insert H263 pkt in reassembly TS %d", pkt->tstamp);
    status = qvp_rtp_input_frag_in_reassembly( stream->rx_ctx.reassem_hdl, 
                                               pkt );

    stream->rx_ctx.rx_state = QVP_RTP_H263_RX_REASSMBLING;
    stream->rx_ctx.last_ts = pkt->tstamp;

  }
  
  /*------------------------------------------------------------------------
    Check if we reached end of frame - 
    If the marker bit of the packet is set. 
    If packet timestamp changes from currently reassembly timestamp
    Then Regroup the current reassembly 
  ------------------------------------------------------------------------*/
  if( ( status == QVP_RTP_SUCCESS || status == QVP_RTP_NORESOURCES ) &&
      ( ( pkt->marker_bit ) ||
        ( ( stream->rx_ctx.rx_state == QVP_RTP_H263_RX_REASSMBLING ) && 
          ( pkt->tstamp != stream->rx_ctx.last_ts ) ) ) )
  {

    /*----------------------------------------------------------------------
       Update all statistcs before aborting the reassembly
    ----------------------------------------------------------------------*/
    stream->rx_ctx.drop_cnt += 
           qvp_rtp_get_reasm_drop_cnt( stream->rx_ctx.reassem_hdl );

    stream->rx_ctx.missing_pkt_cnt += 
           qvp_rtp_get_reasm_missng_cnt( stream->rx_ctx.reassem_hdl );

    stream->rx_ctx.last_seq_sent = 
             qvp_rtp_get_reasm_last_seq( stream->rx_ctx.reassem_hdl );

    /*----------------------------------------------------------------------
        regroup the chain in one VIDEO buffer 
        allocate a buffer to the list
    ----------------------------------------------------------------------*/
    app_buf = qvp_rtp_alloc_buf( QVP_RTP_POOL_VIDEO );

    if( app_buf )
    {
      QVP_RTP_MSG_LOW_1( "Get H263 frame TS %d", stream->rx_ctx.last_ts);
      status = qvp_rtp_get_reassembled_frame( stream->rx_ctx.reassem_hdl,
                                            app_buf, QVP_RTP_PKT_VIDEO_SIZE,
                                            stream->rx_ctx.last_seq_sent );

      if( ( status != QVP_RTP_SUCCESS ) && ( app_buf->need_tofree ) )
      {
         qvp_rtp_free_buf( app_buf );
         app_buf = NULL;
      }
    }
    else
    {
      status = QVP_RTP_NORESOURCES;

    } /* if no app_buf */

    
    /*--------------------------------------------------------------------
         Do not abort the reassembly here since there is no need to start a new 
         reassembly chain. 
         Hoping that we can recover from previous errors we will continue here 
         by sending a NULL frame to application
    --------------------------------------------------------------------*/
    if( ( status != QVP_RTP_SUCCESS ) && 
         ( pkt->tstamp == stream->rx_ctx.last_ts ) )
    {
      QVP_RTP_ERR( "Error ignored, sending NULL frame at TS %d", \
                                      stream->rx_ctx.last_ts, 0, 0 );
      return( h263_profile_config.rx_cb( app_buf, usr_hdl ) );
    }

    /*----------------------------------------------------------------------
       If we reach here, ignore other errors and move to next frame 
    ----------------------------------------------------------------------*/
    status = QVP_RTP_SUCCESS;

    /*----------------------------------------------------------------------
      Check if we have to start a new reassembly for next frame
      Then input the packet in a new reassembly chain
    ----------------------------------------------------------------------*/
    if( pkt->tstamp != stream->rx_ctx.last_ts )
    {
      /*--------------------------------------------------------------------
         Stop any previous reassembly here
      --------------------------------------------------------------------*/
      qvp_rtp_abort_reassembly( stream->rx_ctx.reassem_hdl );

      QVP_RTP_MSG_LOW_1( "Insert pkt in reassembly TS %d", pkt->tstamp);
      stream->rx_ctx.last_ts = pkt->tstamp;
      stream->rx_ctx.rx_state = QVP_RTP_H263_RX_REASSMBLING;
      status = qvp_rtp_input_frag_in_reassembly( stream->rx_ctx.reassem_hdl, 
                                                 pkt );
    }

  }  

  /*------------------------------------------------------------------------
    Update receiver context 
  ------------------------------------------------------------------------*/  
  stream->rx_ctx.last_ts = pkt->tstamp;

  /*------------------------------------------------------------------------
    Return for any fatal errors here, other errors like NORESOURCES can be skipped
  ------------------------------------------------------------------------*/
  if( status != QVP_RTP_SUCCESS && 
      status != QVP_RTP_NORESOURCES )
  {
    return( status );
  }
  
  /*------------------------------------------------------------------------
    If we formed a frame send it to application
  ------------------------------------------------------------------------*/
  if( app_buf )
  {
    QVP_RTP_MSG_MED_1( "H263 frame TS %d", stream->rx_ctx.last_ts);

    /*----------------------------------------------------------------------
       Update the state so that new frame can be started with next pkt
    ----------------------------------------------------------------------*/
    stream->rx_ctx.rx_state = QVP_RTP_H263_RX_IDLE;

    qvp_rtp_abort_reassembly( stream->rx_ctx.reassem_hdl );

    return( h263_profile_config.rx_cb( app_buf, usr_hdl ) );
  }

  return( QVP_RTP_SUCCESS );

} /* qvp_rtp_h263_2429_profile_recv */



/*===========================================================================

FUNCTION  QVP_RTP_H263_2429_PROFILE_SEND


DESCRIPTION
  request to send specified buffer through this profile.

DEPENDENCIES
  None

ARGUMENTS IN
  hdl     - handle into the profile. 
  usr_hdl - handle to use when we call send function using the registered
            tx callback
  pkt     - packet of data to be sent.

RETURN VALUE
  QVP_RTP_SUCCESS  - if we could send data. or an error code


SIDE EFFECTS
  None.

===========================================================================*/
LOCAL qvp_rtp_status_type qvp_rtp_h263_2429_profile_send
(
  qvp_rtp_profile_hdl_type     hdl,          /* handle created while open */
  qvp_rtp_profile_usr_hdl_type usr_hdl,      /* user handle for tx cb */
  qvp_rtp_buf_type             *pkt          /* packet to be formatted 
                                              * through the profile
                                              */
)
{
  /*------------------------------------------------------------------------
    If no tx cb or uninitialized bail out
  ------------------------------------------------------------------------*/
  if( !h263_profile_config.tx_cb || !h263_initialized )
  {
    return( QVP_RTP_ERR_FATAL );
  }
  else
  {
    if( pkt->head_room < QVP_RTP_H263_2429_PYLD_HDR_LEN )
    {
      QVP_RTP_ERR( " qvp_rtp_h263_2429_profile_send : \
                     no head room for header \r\n", 0, 0, 0 );
      return( QVP_RTP_ERR_FATAL );
    }

    /*----------------------------------------------------------------------
      0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     |   RR    |P|V|   PLEN    |PEBIT|
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    ----------------------------------------------------------------------*/

    /*----------------------------------------------------------------------
      Only P bit is set, it is used to complete picture/GOB/slice/EOS/EOSBS 
      start code. It is set to 1 by default.
      By default all other params are not set, V bit indicates presence of
      VRC video redundancy coding,  PLEN is used for extra picture header, 
      and PEBIT is set only when extra pic header is on. 
    ----------------------------------------------------------------------*/

    /*-----------------------------------------------------------------------
      Removing 16 zero-valued bits of pic start code  ( 2 byte long )
    -----------------------------------------------------------------------*/
    pkt->head_room += 2; 

    /*----------------------------------------------------------------------
      We need to reduce head room 
    ----------------------------------------------------------------------*/
    pkt->head_room -= QVP_RTP_H263_2429_PYLD_HDR_LEN;

    /*----------------------------------------------------------------------
      Then copy the 2 byte header  
      In the First byte( above header ) set the P bit to give 0x04
      In the second byte none of the bits are set to give 0x00
    ----------------------------------------------------------------------*/  
    pkt->data[pkt->head_room] = QVP_RTP_H263_2429_PIC_START_MASK; /* 0x04 */
    pkt->data[pkt->head_room + 1] = QVP_RTP_H263_2429_ZERO_BYTE; /* 0x00 */
    
    /*---------------------------------------------------------------------
      Need to update the packet length 
    ----------------------------------------------------------------------*/
    pkt->len += QVP_RTP_H263_2429_PYLD_HDR_LEN;

    /*----------------------------------------------------------------------
       reurn the return value of the tx function 
    ----------------------------------------------------------------------*/
    return( h263_profile_config.tx_cb( pkt, usr_hdl  ) );
  }
  
} /* end of function qvp_rtp_h263_profile_send */

#endif /* end of FEATURE_QVPHONE_RTP */
