
#ifndef _QVP_RTP_CODEC_TXT_H_
#define _QVP_RTP_CODEC_TXT_H_

/*=*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

qvp_rtp_codec. h

GENERAL DESCRIPTION

This file contains DPL state machine and codec global context for DPL CVD for various codec operations.

Copyright (c) 2004 by QUALCOMM Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is
regulated by the U.S. Government. Diversion contrary to U.S. law prohibited.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*==*=*/

#include "customer.h"
#ifdef FEATURE_QVPHONE_RTP
#include "qvp_rtp_codec.h"    /* for API prototypes           */
//#include "qvp_rtp.h"
#include "qvp_rtp_text_profile.h"
/* Pradeep : Support for tty - Start*/
#define	BEL			0x0007
#define	BS			0x0008
#define TTY_QMARK	0x003F
#define	SOS			0x0098
#define	ST			0x009C
#define	ESC			0x001B
#define	READY		1
#define	NOT_READY	0

extern boolean	qvp_rtp_cvd_state;



struct cvdq_node
{
	uint16 t140Char;	//The t140 to be sent to the CVD interface
	struct cvdq_node* next;

};
typedef struct cvdq_node rtp_txt_cvdq_node;

qvp_rtp_status_type qvp_rtp_text_codec_initialize_pvt(
  qvp_rtp_app_id_type    app_id,      /* application which opened
                                       * the stream
                                       */
  qvp_rtp_stream_id_type  stream_id,    /* stream which was opened */
  qvp_rtp_media_config   *config,
  qvp_rtp_media_handle_response  handle_media_res,
  qvp_rtp_profile_hdl_type *handle,
  qvp_rtp_stream_direction       direction
);
typedef struct 
{
	boolean skip_char;
	rtp_txt_cvdq_node* cvd_head;
	rtp_txt_cvdq_node* cvd_tail;
}qvp_rtp_codec_txt_rx_ctx;

typedef struct 
{	
	qvp_rtp_app_id_type					app_id;
	qvp_rtp_stream_id_type				stream_id;
	qvp_rtp_codec_txt_rx_ctx*			rx_ctx;
}qvp_rtp_codec_txt_ctx;

struct qvp_rtp_txt_supported_char_range
{
	uint8	min_val;
	uint8	max_val;
};
void qvp_rtp_text_cvdq_add(rtp_txt_queue_node* pToAdd);
rtp_txt_cvdq_node* qvp_rtp_text_cvdq_getpkt(void);
void qvp_rtp_text_process_cvdq(void);

qvp_rtp_status_type qvp_rtp_text_codec_configure_pvt(
  qvp_rtp_app_id_type    app_id,      /* application which opened
                                      * the stream
                                      */
  qvp_rtp_stream_id_type  stream_id,   /* stream which was opened */
  qvp_rtp_media_config   *config,
  qvp_rtp_media_handle_response  handle_media_res,
  qvp_rtp_profile_hdl_type *handle,
  qvp_rtp_stream_direction  direction   
  );

qvp_rtp_status_type qvp_rtp_text_codec_release_pvt(
  qvp_rtp_app_id_type    app_id,      /* application which opened
                                     * the stream
                                     */
  qvp_rtp_media_handle_response  handle_media_res
  );

qvp_rtp_status_type qvp_rtp_text_stream_start_pvt
(
  qvp_rtp_app_id_type    app_id,      /* application which opened
                                       * the stream
                                       */
  qvp_rtp_stream_id_type  stream_id,    /* stream which was opened */
  qvp_rtp_stream_direction   direction
);
qvp_rtp_status_type qvp_rtp_text_stream_stop_pvt
(
  qvp_rtp_app_id_type    app_id,      /* application which opened
                                       * the stream
                                       */
  qvp_rtp_stream_id_type  stream_id,    /* stream which was opened */
  qvp_rtp_stream_direction   direction
);
void qvp_rtp_handle_text_incoming_pkt_cb_pvt
(
 qvp_rtp_app_id_type     app_id,      /* RTP Application ID*/
 qvp_rtp_stream_id_type   stream,    /* stream id through which the
                                     * packet came
                                     */
 qvp_rtp_recv_pkt_type    *pkt       /* packet which is being delivered */
);



void qvp_rtp_tty_process_tx_data(QPE_AUDIO_MSG tAudioMsg, void* pParam1, QPUINT32 iParam2, void *pUserdata);

qvp_rtp_status_type qvp_rtp_media_text_init_config(
                    qvp_rtp_media_config *config,
                    qvp_rtp_profile_hdl_type *hdl );

boolean qvp_rtp_txt_is_supported_tty_char(uint16 ttyChar);
/* Pradeep: Support for tty - End*/

#endif /* end of FEATURE_QVPHONE_RTP */
#endif /*  end of function _QVP_RTP_CODEC_TXT_H_ */
