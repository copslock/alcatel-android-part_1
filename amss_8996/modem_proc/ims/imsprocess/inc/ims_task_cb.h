#ifndef _IMS_TASK_CB_H_
#define _IMS_TASK_CB_H_
/*===========================================================================

                    I M S  C A L L B A C K  T A S K
                    H E A D E R   F I L E

DESCRIPTION
  This file contains the definition of the public interfaces that are needed
  for the IMS task

EXTERNALIZED FUNCTIONS

INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2008 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government. Diversion contrary to U.S. law prohibited.
===========================================================================*/

/*===========================================================================
Revision History
========================================================================
    Date    |   Author's Name    |  BugID  |        Change Description
========================================================================
26-Feb-2010   sameer                 -      - Moving the task code out of fwapi and
                                               into process directory
                                             - Featurizing code
===========================================================================*/
#include "customer.h"
#include "comdef.h"

#ifdef __cplusplus
extern "C"
{
#endif

/*===========================================================================
 
                     INCLUDE FILES FOR MODULE
 
===========================================================================*/
#include "customer.h"

#include "rex.h"

typedef enum
{
  IMSTASK_CB_STATE_INITIAL          = 0, /* initial task creation */
  IMSTASK_CB_STATE_STARTED          = 1, /* TASK_START_SIG recieved */
  IMSTASK_CB_STATE_STOPPED          = 2, /* TASK_STOP_SIG recieved */
  IMSTASK_CB_STATE_OFFLINE          = 3, /* TASK_OFFLINE_SIG received */
  IMSTASK_CB_STATE_FAILED           = 4, /* task encountered unrecoverable error*/
  IMSTASK_CB_STATE_MAX
} imstask_cb_state_enum_type;

typedef int (*imstask_cb_cmd_handler_fn_type)(void *);

/*===========================================================================
FUNCTION: ims_task_cb

DESCRIPTION
  Rex entry point for the IMS Task.

DEPENDENCIES
  None.

PARAMETERS
  dword                       [In]- unused

RETURN VALUE
  None.

COMMENTS
  None

SIDE EFFECTS
  None
===========================================================================*/
extern void ims_task_cb( dword dummy );

/*===========================================================================
FUNCTION: imstask_cb_cmd

DESCRIPTION
  Execute an IMS Task command.

DEPENDENCIES
  None.

PARAMETERS
  cmd_fn                   [In] - function to execute in IMS task.
  data_ptr                 [In/Out] - argument passed to cmd_fn
  response_sig             [In] - signal sent to calling task when cmd_fn
                                  completes.

RETURN VALUE
  AEE_IMS_SUCCESS - operation successful
  AEE_IMS_ENOMEMORY- out of memory
  AEE_IMS_ENOTREADY- task not ready

COMMENTS
  None

SIDE EFFECTS
  None
===========================================================================*/
extern int imstask_cb_cmd
(
  imstask_cb_cmd_handler_fn_type cmd_fn,
  void*                         data_ptr,
  rex_sigs_type                 response_sig
);

/*===========================================================================
FUNCTION: imstask_cb_post_callback

DESCRIPTION:
  posts a callback to imstask_cb.
  - queues up callback function and data
  - sets cmd_sig on ims_task_cb, so it can handle it later

DEPENDENCIES
  None.

PARAMETERS
  cmd_fn                   [In] - function to execute in IMS task.
  data_ptr                 [In/Out] - argument passed to cmd_fn

RETURN VALUE
  None.

COMMENTS
  There is no response sig associated with this function

SIDE EFFECTS
  None
===========================================================================*/
extern int imstask_cb_post_callback
(
  imstask_cb_cmd_handler_fn_type  cmd_fn,
  void*                           data_ptr
);

/*===========================================================================
FUNCTION: imstask_cb_start

DESCRIPTION
  Test function to force IMS task into IMSTASK_CB_STATE_STARTED.

DEPENDENCIES

PARAMETERS
  None

RETURN VALUE
  AEE_IMS_SUCCESS - operation successful

COMMENTS
  None

SIDE EFFECTS
  None
===========================================================================*/
extern int imstask_cb_start(void);

/*===========================================================================
FUNCTION: imstask_cb_stop

DESCRIPTION
  Test function to force IMS task into IMSTASK_CB_STATE_STOPPED.

DEPENDENCIES

PARAMETERS
  None

RETURN VALUE
  AEE_IMS_SUCCESS - operation successful

COMMENTS
  None

SIDE EFFECTS
  None
===========================================================================*/
extern int imstask_cb_stop(void);

/*===========================================================================
FUNCTION: imstask_cb_fail

DESCRIPTION
  Test function to force IMS task into IMSTASK_CB_STATE_FAILED.

DEPENDENCIES

PARAMETERS
  None

RETURN VALUE
  None

COMMENTS
  None

SIDE EFFECTS
  None
===========================================================================*/
extern void imstask_cb_fail(void);

/*===========================================================================
FUNCTION: imstask_cb_get_state

DESCRIPTION
  Retrieve current IMS task state.

DEPENDENCIES

PARAMETERS
  state_ptr                [Out] - returns current state

RETURN VALUE
  AEE_IMS_SUCCESS - operation successful
  AEE_IMS_ENOMEMORY - out of memory

COMMENTS
  None

SIDE EFFECTS
  None
===========================================================================*/
extern int imstask_cb_get_state(imstask_cb_state_enum_type* state_ptr);

#ifdef __cplusplus
}
#endif

#endif /* _IMS_TASK_CB_H_ */

