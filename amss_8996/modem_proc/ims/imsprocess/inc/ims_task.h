#ifndef _IMS_TASK_H_
#define _IMS_TASK_H_
/*===========================================================================

                    IMS  T A S K
                    H E A D E R   F I L E

DESCRIPTION
  This file contains the definition of the public interfaces that are needed
  for the IMS task

EXTERNALIZED FUNCTIONS

INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2008 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government. Diversion contrary to U.S. law prohibited.
===========================================================================*/

/*===========================================================================
Revision History
========================================================================
  Date    |   Author's Name    |  BugID  |        Change Description
========================================================================
 26-Feb-2010   sameer                 -      - Moving the task code out of fwapi and
                                               into process directory
                                             - Featurizing code
--------------------------------------------------------------------------------------
14-Dec-2011    Ramananda          325680     - Creation if QIPCALL Path for NV item to be created 
                                               under ims.conf
--------------------------------------------------------------------------------------
25-Jan-2012    Preeti                       - Added NVs for ringing timer, ringback timer and rtp
                                              link aliveness
--------------------------------------------------------------------------------------
12-Mar-2012    Ramananda           342875    - Added NVs for ims_scr_amr_wb_enabled,ims_scr_amr_nb_enabled 
-------------------------------------------------------------------------
12-6-2012     Ramanand              369706 -   Adding new NV IMS_USER_AGENT
-------------------------------------------------------------------------
08-29-2012    Manasi                 388079    Adding new NV DAN retry count/duration
-------------------------------------------------------------------------
01-03-2012    Ankith Agarwal         419055    Adding new NV DAN channel prefernce
--------------------------------------------------------------------------------
01-10-2013    Ankith                  514930   FR 16577: ATT specific changes for VT Preconditions
--------------------------------------------------------------------------------
11-22-2013    Vinay                  556679    FR 17973: INVITE Error Retry Counters & Timers and Configurability - IMS Changes
--------------------------------------------------------------------------------
02-20-2014    Malavika              594672    Renamed DTX NV to SESSION BW NV 
--------------------------------------------------------------------------------
01-07-2015    Sarat                 843579    FR 28149: Emergency over WIFI Disable NV
--------------------------------------------------------------------------------------------------------
07-31-15      manasij     883034     VZW Add CDMALess FT if device is CDMAless type
===========================================================================*/
#include "customer.h"
#include "comdef.h"
#include "stddef.h" /* for size_t */
#include "ims_common_defs.h"

#ifdef __cplusplus
extern "C"
{
#endif

/*===========================================================================
 
                     INCLUDE FILES FOR MODULE
 
===========================================================================*/
#include "ims_task_def.h"

#define QIPCALL_CODEC_MODE_SET_AMR_WB                 "/nv/item_files/ims/qipcall_codec_mode_set_amr_wb"
#define QIPCALL_OCTET_ALIGNED_MODE_AMR_WB             "/nv/item_files/ims/qipcall_octet_aligned_mode_amr_wb"
#define QIPCALL_OCTET_ALIGNED_MODE_AMR_NB             "/nv/item_files/ims/qipcall_octet_aligned_mode_amr_nb"
#define VOIP_PRFRD_CODEC                              "/nv/item_files/ims/voip_prfrd_codec"
#define QIPCALL_SESSION_LEVEL_BANDWIDTH_ENABLED       "/nv/item_files/ims/qipcall_session_level_media_bw_enabled"
#define QIPCALL_QOS_ENABLED                           "/nv/item_files/ims/qipcall_qos_enabled"
#define QIPCALL_CONFRD_URI                            "/nv/item_files/ims/qipcall_confrd_uri"
#define QIPCALL_DAN_ENABLE                            "/nv/item_files/ims/qipcall_dan_enable"
#define QIPCALL_CALLERID_MODE                         "/nv/item_files/ims/qipcall_callerid_mode"
#define QIPCALL_REFER_SUBSCRIPTION_EXPIRES_DURATION   "/nv/item_files/ims/qipcall_refer_subscription_expires_duration"
#define QIPCALL_NOTIFY_REFER_RESPONSE_DURATION        "/nv/item_files/ims/qipcall_notify_refer_response_duration"
#define QIPCALL_PRECONDITION_ENABLE                   "/nv/item_files/ims/qipcall_precondition_enable"
#define QIPCALL_ENABLE_HD_VOICE                       "/nv/item_files/ims/qipcall_enable_hd_voice"
#define QIPCALL_CODEC_MODE_SET                        "/nv/item_files/ims/qipcall_codec_mode_set"
#define QIPCALL_DOMAIN_SELECTION_ENABLED              "/nv/item_files/ims/qipcall_domain_selection_enable"
#define QIPCALL_IS_CONF_SERVER_REFER_RECEIPIENT       "/nv/item_files/ims/qipcall_is_conf_server_refer_recipient"
#define QIPCALL_RTCP_LINK_ALIVENESS_TIMER             "/nv/item_files/ims/qipcall_rtcp_link_aliveness_timer"
#define QIPCALL_RTCP_REPORTING_INTERVAL               "/nv/item_files/ims/qipcall_rtcp_reporting_interval"
#define QIPCALL_DAN_HYSTERISIS_TIMER_VALUE 	          "/nv/item_files/ims/qipcall_dan_hysterisis_timer_duration"
#define QIPCALL_DAN_NEEDED			                      "/nv/item_files/ims/qipcall_dan_needed"
#define QIPCALL_1x_SMS_AND_VOICE		                  "/nv/item_files/ims/qipcall_1xsmsandvoice"
#define QIPCALL_RINGBACK_TIMER                        "/nv/item_files/ims/qipcall_ringback_timer"
#define QIPCALL_RINGING_TIMER                         "/nv/item_files/ims/qipcall_ringing_timer"
#define QIPCALL_RTP_LINK_ALIVENESS_TIMER              "/nv/item_files/ims/qipcall_rtp_link_aliveness_timer"
#define IMS_SCR_AMR_WB_ENABLED                        "/nv/item_files/ims/ims_scr_amr_wb_enabled"
#define IMS_SCR_AMR_NB_ENABLED                        "/nv/item_files/ims/ims_scr_amr_nb_enabled"
#define IMS_OPRT_MODE                                 "/nv/item_files/ims/ims_operation_mode"
#define IMS_USER_AGENT                                "/nv/item_files/ims/ims_user_agent"
#define QIPCALL_DAN_RETRY_TIMER_DURATION              "/nv/item_files/ims/qipcall_dan_retry_timer_duration"
#define QIPCALL_DAN_MAX_RETRY_COUNT                   "/nv/item_files/ims/qipcall_dan_max_retry_count"
#define QIPCALL_DAN_CHANNEL_PREFERENCE                "/nv/item_files/ims/qipcall_dan_channel_preference"
#define QIPCALL_SIGNAL_STRENGTH_THRESHOLD               "/nv/item_files/ims/qipcall_signal_strength_threshold"
#define QIPCALL_CONFERENCE_AWARE                      "/nv/item_files/ims/qipcall_is_conference_aware"
#define IMSTASK_NV_HYBRID_ENABLE_FLAG                 "/nv/item_files/ims/ims_hybrid_enable"
#define QIPCALL_QOS_RESERVATION_TIMER                 "/nv/item_files/ims/qipcall_qos_reservation_timer"
#define QIPCALL_INVITE_RETRY_COUNTER                  "/nv/item_files/ims/qipcall_invite_retry_counter"
#define QIPCALL_VT_QUALITY_SELECTOR                   "/nv/item_files/ims/qipcall_vt_quality_selector"
#define PRI_UITTY_FILE_NAME   	                      "/nv/item_files/modem/mmode/qmi/ui_tty_setting_pri"
#define QIPCALL_SMS_IMS_PSI_STRING                    "/nv/item_files/ims/qipcall_ims_sms_psi"
#define QIPCALL_AUDIO_CODEC_LIST_STRING               "/nv/item_files/ims/qipcall_audio_codec_list"
#define QP_IMS_EMERG_WIFI_DISABLE                     "/nv/item_files/ims/qp_ims_emerg_wifi_disable"
#define CM_NV_CDMALESS				      "/nv/item_files/modem/mmode/cdma_less"
/*===========================================================================
FUNCTION: ims_task

DESCRIPTION
  Rex entry point for the IMS Task.

DEPENDENCIES
  None.

PARAMETERS
  dword                       [In]- unused

RETURN VALUE
  None.

COMMENTS
  None

SIDE EFFECTS
  None
===========================================================================*/
extern void ims_task( dword dummy );

/*===========================================================================
Utility functions
=============================================================================*/

/*===========================================================================
IMSAtomic_Add

Description:
   Adds the given value to *puDest in atomic fashion.

Parameters:
   puDest : pointer to unsigned int that needs to be incremented
   nVal   : value that *puDest needs to be incremented with

Return value:
   int - IMS FW error return code defined in AEEIMSCommon.h
===========================================================================*/
uint32 IMSAtomic_Add
(
   uint32 * puDest,
   int nVal
);

void * ims_malloc(size_t size);
void ims_free(void * ptr);

/*===========================================================================
FUNCTION: imstask_dpl_id_alloc

DESCRIPTION
  Free a DPL instance. qpDplInitialize() should be called from an imstask_cmd()
  function before using the DPL instance.

DEPENDENCIES

PARAMETERS
  dpl_id_ptr              [Out] - handle to the dpl instance

RETURN VALUE
  AEE_IMS_SUCCESS - operation successful
  AEE_IMS_ENOMEMORY - out of memory
  AEE_IMS_EBADPARAM - 

COMMENTS
  None

SIDE EFFECTS
  None
===========================================================================*/
extern int imstask_dpl_id_alloc(imstask_dpl_id_type* dpl_id_ptr);

/*===========================================================================
FUNCTION: imstask_dpl_id_free

DESCRIPTION
  Free a DPL instance. qpDplUninitialize() should be called from an
  imstask_cmd() function before freeing the DPL instance.

DEPENDENCIES

PARAMETERS
  dpl_id_ptr              [In] - handle to the dpl instance

RETURN VALUE
  AEE_IMS_SUCCESS - operation successful
  AEE_IMS_EBADPARAM - 

COMMENTS
  None

SIDE EFFECTS
  None
===========================================================================*/
extern int imstask_dpl_id_free(imstask_dpl_id_type dpl_id);

/*===========================================================================
FUNCTION: imstask_Osih_mmi

DESCRIPTION:
  Executes the IMS API.

DEPENDENCIES
  None.

PARAMETERS
  msg_id                   [In] - core message id.
  event_id                 [In] - event id.
  data_ptr                 [In/Out] - argument passed to cmd_fn

RETURN VALUE
  None.

COMMENTS
  None

SIDE EFFECTS
  None
===========================================================================*/
extern void imstask_osih_mmi
(
   int       msg_id,
   int       event_id,
   void*     data_ptr
);

/*===========================================================================
FUNCTION: imstask_cmd

DESCRIPTION
  Execute an IMS Task command.

DEPENDENCIES
  None.

PARAMETERS
  cmd_fn                   [In] - function to execute in IMS task.
  data_ptr                 [In/Out] - argument passed to cmd_fn
  response_sig             [In] - signal sent to calling task when cmd_fn
                                  completes.
  dpl_id                   [In] - the DPL instance to use for this command.

RETURN VALUE
  AEE_IMS_SUCCESS - operation successful
  AEE_IMS_ENOMEMORY- out of memory
  AEE_IMS_ENOTREADY- DPL initialization is not complete
  other errors returned by cmd_fn.


COMMENTS
  None

SIDE EFFECTS
  None
===========================================================================*/
extern int imstask_cmd
(
  imstask_cmd_handler_fn_type   cmd_fn,
  void*                         data_ptr,
  rex_sigs_type                 response_sig,
  imstask_dpl_id_type           dpl_id
);

/*===========================================================================
FUNCTION: imstask_post_event_to_session

DESCRIPTION
  Post media event to active session in IMS Task context.

DEPENDENCIES
  None.

PARAMETERS
  session_ptr              [In] - pointer to IMSSession.
  stream_ptr               [In] - pointer to IMSStreamMedia
  event_id                 [In] - media event id.


RETURN VALUE
  AEE_IMS_SUCCESS - operation successful
  AEE_IMS_ENOMEMORY- out of memory
  AEE_IMS_ENOTREADY- DPL initialization is not complete
  other errors returned by cmd_fn.


COMMENTS
  None

SIDE EFFECTS
  None
===========================================================================*/
extern int imstask_post_event_to_session
(
  void*    session_ptr,
  void*    stream_ptr,
  int      event_id
);

/*===========================================================================
FUNCTION: imstask_post_callback

DESCRIPTION:
  posts a callback to imstask.
  - queues up callback function and data
  - sets cmd_sig on ims_task, so it can handle it later

DEPENDENCIES
  None.

PARAMETERS
  cmd_fn                   [In] - function to execute in IMS task.
  data_ptr                 [In/Out] - argument passed to cmd_fn
  cmd_type                 [In] - does cmd require DPL to be initialized before
                                  it runs.

RETURN VALUE
  None.

COMMENTS
  There is no response sig associated with this function

SIDE EFFECTS
  None
===========================================================================*/
extern int imstask_post_callback
(
  imstask_cmd_handler_fn_type  cmd_fn,
  void*                        data_ptr,
  imstask_dpl_id_type          dpl_id
);

/*===========================================================================
FUNCTION: imstask_register_state_cb

DESCRIPTION
  Register a function to be called when IMS task state changes.

DEPENDENCIES

PARAMETERS
  cb_handle_ptr            [Out] - handle to the callback entry.
  cb_fn                    [In] - state change callback function.
  user_data                [In] - data passed to callback function
                                  completes.

RETURN VALUE
  AEE_IMS_SUCCESS - operation successful
  AEE_IMS_ENOMEMORY- out of memory

COMMENTS
  None

SIDE EFFECTS
  None
===========================================================================*/
extern int imstask_register_state_cb
(
  imstask_state_cb_handle_type* cb_handle_ptr,  
  imstask_state_cb              cb_fn,
  uint32                        user_data
);

/*===========================================================================
FUNCTION: imstask_deregister_state_cb

DESCRIPTION
  Deregister a previously registered state change callback.

DEPENDENCIES

PARAMETERS
  cb_handle                [In] - handle to the callback entry.

RETURN VALUE
  AEE_IMS_SUCCESS - operation successful

COMMENTS
  None

SIDE EFFECTS
  None
===========================================================================*/
extern int imstask_deregister_state_cb
(
  imstask_state_cb_handle_type cb_handle
);


/*===========================================================================
FUNCTION: imstask_register_session

DESCRIPTION
  Register a active session.

DEPENDENCIES

PARAMETERS
  session_ptr              [In] - pointer to session.
  ev_handle_fn_type        [In] - media event handler function pointer
  dpl_id                   [In] - The DPL instance to use


RETURN VALUE
  AEE_IMS_SUCCESS - operation successful
  AEE_IMS_ENOMEMORY- out of memory

COMMENTS
  None

SIDE EFFECTS
  None
===========================================================================*/
extern int imstask_register_session
(
  void*                                session_ptr,
  imstask_media_event_handler_fn_type  ev_handle_fn_type,  
  imstask_dpl_id_type                  dpl_id
);

/*===========================================================================
FUNCTION: imstask_deregister_session

DESCRIPTION
  De Register a active session.

DEPENDENCIES

PARAMETERS
  session_ptr              [In] - pointer to session.
  dpl_id                   [In] - The DPL instance to use


RETURN VALUE
  AEE_IMS_SUCCESS - operation successful

COMMENTS
  None

SIDE EFFECTS
  None
===========================================================================*/
extern int imstask_deregister_session
(
 void*                        session_ptr,
  imstask_dpl_id_type         dpl_id
);

/*===========================================================================
FUNCTION: imstask_notify_dpl_init_status

DESCRIPTION
  Notify ims task regarding DPL initialization status. Mainly used by Regmanager task

DEPENDENCIES

PARAMETERS
  iParam                   [In] - parameter to indicate if DPL initialization was successfull
                                  or not.
RETURN VALUE
  None

COMMENTS
  None

SIDE EFFECTS
  None
===========================================================================*/
  extern void imstask_notify_dpl_init_status (int iParam);

/*===========================================================================
FUNCTION: imstask_start

DESCRIPTION
  Test function to force IMS task into IMSTASK_STATE_STARTED.

DEPENDENCIES

PARAMETERS
  None

RETURN VALUE
  AEE_IMS_SUCCESS - operation successful

COMMENTS
  None

SIDE EFFECTS
  None
===========================================================================*/
extern int imstask_start(void);

/*===========================================================================
FUNCTION: imstask_stop

DESCRIPTION
  Test function to force IMS task into IMSTASK_STATE_STOPPED.

DEPENDENCIES

PARAMETERS
  None

RETURN VALUE
  AEE_IMS_SUCCESS - operation successful

COMMENTS
  None

SIDE EFFECTS
  None
===========================================================================*/
extern int imstask_stop(void);

/*===========================================================================
FUNCTION: imstask_fail

DESCRIPTION
  Test function to force IMS task into IMSTASK_STATE_FAILED.

DEPENDENCIES

PARAMETERS
  None

RETURN VALUE
  None

COMMENTS
  None

SIDE EFFECTS
  None
===========================================================================*/
extern void imstask_fail(void);

/*===========================================================================
FUNCTION: imstask_get_state

DESCRIPTION
  Retrieve current IMS task state.

DEPENDENCIES

PARAMETERS
  state_ptr                [Out] - returns current state

RETURN VALUE
  AEE_IMS_SUCCESS - operation successful
  AEE_IMS_ENOMEMORY - out of memory

COMMENTS
  None

SIDE EFFECTS
  None
===========================================================================*/
extern int imstask_get_state(imstask_state_enum_type* state_ptr);

#ifdef __cplusplus
}
#endif

#endif /* _IMS_TASK_H_ */

