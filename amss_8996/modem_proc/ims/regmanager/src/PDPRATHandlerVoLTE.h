﻿/******************************************************************************************

 File: PDPRATHandlerVoLTE.h
 Description: 
 
$Header: //commercial/MPSS.TH.2.0.c1.9/Main/modem_proc/ims/regmanager/src/PDPRATHandlerVoLTE.h#3 $
$DateTime: 2016/09/26 01:20:01 $
$Author: huawenc $

 Revision History
==========================================================================================
   Date    |   Author's Name    |  CR#   |  Review ID  |      Change Description
==========================================================================================
13-09-13  Priyank        543915     FR 3104: IMS on Wifi
--------------------------------------------------------------------------------------------------
12-12-13   Manasi        589093       When powered up on eHRPD device takes about 58 seconds to perform VSNCP for 
                                      IMS after internet PDN was attached - change back off timer to 5 seconds 
-------------------------------------------------------------------------------
14-03-14   Ahmet       631224      Handle emergency call if emergency PDP is deactivating
-------------------------------------------------------------------------------
15-09-14   c_snaras    708477      FR 22825: VZW IMS PDN retry failure handling for SRLTE device.
-------------------------------------------------------------------------------------------------
03-11-14   c_snaras    749436     Dime 4.0.1 L hVoLTE FR 22825: Mode switch triggered after PDN connectivity reject
-------------------------------------------------------------------------------------------------------------
04-Feb-14   Priyank    722987       FR 20991: Retry E911 call in IMS when in Operator's Whitelist MCC
----------------------------------------------------------------------------------------------------
12-30-14   c_snaras   781524   VzW eHRPD TC 2.5 failure due to incorrect recovery timer on eHRPD RAT
-------------------------------------------------------------------------------
02-14-15   c_snaras        767931     FR 24213: VzW - Handling APN related parameter change when connected to IMS APN
--------------------------------------------------------------------------------------------------------
23-02-15  c_snaras    798800       RE: [LTEFTBOLT-603][8994 BOLT TMO SD Field Issue#33][VoLTE Field]Call dropped while Hand-in from LTE to WIFI
-------------------------------------------------------------------------------
06-04-15  Sreenidhi   811524       FR 26778: IMS service disable in 3G on PDP failure with any error code
--------------------------------------------------------------------------------------------------
15-06-15    Sreenidhi     851807   FR 28509: Sub Oprt 7 � IMS APN requirements
--------------------------------------------------------------------------------------------------
13-07-15    Sreenidhi     865526   Sub Oprt 7 - DUT is getting eregistered services = 0 after completion of registration in IPv6
-------------------------------------------------------------------------------
06-05-15       Priyank        844055   While releasing the PDP, RM to Notify all its registered clients about PDP Deactivating
--------------------------------------------------------------------------------------------------
07-10-15      Priyank    841648      Support of P-CSCF V6 to V4 (vice versa) fallback, when there is no P-CSCF IPv6 from network for IMS Registration
-------------------------------------------------------------------------------
17-Jul-15      Priyank  849956  UE does not SIP registration in IPv4 after SIP registration in IPv6.
-------------------------------------------------------------------------------
27-Aug-15      Priyank  895112  In LGU mode, RM shall not trigger Force PDN release in case of LPM
-------------------------------------------------------------------------------
28-Aug-15      Priyank  894265  After LPM/Online if there is a change in resolved APN Name then IMS reg is failing
--------------------------------------------------------------------------------------------------
21-Sep-15     Priyank    907270       After IP fall back, UE is registering with the previously registering IP type during LPM on/off
********************************************************************************************/

#ifndef __PDPRAT_HANDLER_VOLTE_H__
#define __PDPRAT_HANDLER_VOLTE_H__

#include "PDPRATHandler.h"
extern "C"
{
#include "qpDplHandOver.h"
}

typedef enum WifiMeasurementState
{
  eWifiMsrmntInit,
  eWifiMsrmntStarted,
  eWifiMsrmntStopped,
  eWifiMsrmntHoPossble,
  eWifiMsrmntHoNotPossble
}WIFIMSRMNTSTATE;

class CPDPRATHandlerVoLTE: public CPDPRATHandler
{
public:
  CPDPRATHandlerVoLTE(QP_PDP_TYPE);
  virtual ~CPDPRATHandlerVoLTE();
  QPVOID Init();
  QPVOID DeInit();
  QPVOID Reset(QPBOOL n_bRatToReset=QP_TRUE);

  static QPVOID DcmCallback(QPE_DCM_MSG tDcmMsg, QPDCM_CONN_PROFILE* pDcmProfile, QPVOID* pMsgData);
  static QPVOID WifiMeasurementCallBack(QP_HANDOVER_STATUS_TYPE *pHOStatus, QPVOID* n_pUserData);
  QPBOOL PdpActivate();
  QPVOID UpdateHOSysPrefInfo(QPUINT32 numAvailAPNs ,QPDCM_PREF_SYS_INFO* sysPrefInfos);
  QPVOID PdpDeactivate();
  QPVOID EventChangeRat(QPE_DCM_RAT iNewRat, QPE_DCM_RAT_MASK iRATMask);
  QPVOID EventChangeRatMask(QPE_DCM_RAT iNewRat, QPE_DCM_RAT_MASK iRATMask);
  QPVOID UpdateRatFromRatMask();
  QPVOID EventHOInit();
  QPVOID EventHOSuccess(QpDcmHOStatus* pHOStatus);
  QPVOID EventHOFail(QpDcmHOStatus* pHOStatus);
  QPUINT8 GetErrorBasedOnDCMFailure();
  QP_RM_RECOVERY_TYPE DoPDNRecovery();
  QPVOID StopPDNRetrytimers();
  QPVOID UpdatePDNRetrytimers();
  QPBOOL IsTimerRunning();
  QPBOOL CheckRegistrationNeeded();
  QPBOOL IsPDPActive() { return ((m_ePDPState == ePDPStatepdpActivated)? QP_TRUE:QP_FALSE );}
  PDPHANDLERSTATE GetPDPState() { return m_ePDPState;}
  QPVOID ResetCounts();
  QPVOID SetPermBlock();
  QPVOID UnSetPermBlock();
  QPVOID EventExtendedIPConfig();
  QPCHAR *GetClientIndication();
  /* This method will notify the CM regarding the registration status */
  QPVOID NotifyRegStatusToCM();
  QPVOID SetPermBlockDueToRejEvt();
 /*Handle the APCS notification*/
  QPVOID HandleAPCSNotification();
  QPVOID HandleRatNotification(QPE_DCM_RAT n_eRatParams);

  QPBOOL InitPDP(const QP_APN_RAT &n_sAPNRat,const QP_APN_ATTR_INFO & n_sApnAttrInfo);
  /*QPE_SECURITY_TYPE GetSecurityType();*/
  QPE_DCM_RAT GetHORATInfo();
  QP_RM_EMER_PDN_RECOVERY_TYPE GetEmerRetryStatus();
  /*This method will returns weather PDN recovery is required or not in single mode*/
  QPBOOL SRLTEPDNRecoveryNeeded();
  QPUINT8 GetOperatorReservedPCO();
  QPVOID StopWifiMeasurementIfStarted();
  QPVOID SetBlockPDNOnUMTS(QPBOOL n_bBlockPDNOnUMTS);
  QPVOID CreateProfileAfterRefCnt0(QPUINT32*);
  QPVOID SetPowerDownStatus(QPBOOL n_bPowerDownStatus);
  QPVOID DisconnectPDN();

private:
  CPDPRATHandlerVoLTE(const CPDPRATHandlerVoLTE&);
  CPDPRATHandlerVoLTE& operator= (const CPDPRATHandlerVoLTE&);

  QPUINT32 DcmFailureRecoveryType();                              //To determine recovery wait type for failure cause
  QPUINT8 CheckPremFailureType(); 
  QPBOOL isIPFallBackRequire();
  QPVOID EventPdpActivated();
  QPVOID EventPdpDeActivated();
  QPVOID EventPdpFailure(QPE_DCM_FAILURE_MSG iFailureReason = DCM_FAILURE_UNKNOWN);
  QPVOID PDNRecoveryTimerFired(QPINT n_iID);
  QPVOID HandleWifiMeasurement(QP_HANDOVER_STATUS_TYPE *pHOStatus);
  QPVOID StartWifiMeasurement();
  QPVOID StopWifiMeasurement();
  QPBOOL IWLANBringupCriteriaMet();
  QPVOID EventOperatorReservedPCOChange();
  QPVOID UpdatePCOApps(QPBOOL forcePush);
  QPUINT8 CheckPremFailureTypeForEHPRD();
  QPBOOL UpdateAPNParamsChangeToPM();
  QPVOID EventAPNParamsChange(QP_DCM_CHANGED_APN_PARAMS_TYPE* pApnParamsType);
  QPVOID EventDSProfileChange(QPBOOL n_bIsProfileChangeEv,QP_DCM_PROFILE_CHANGE_PARAMS *sIMSProfileBuf = QP_NULL);
  QPBOOL CheckAPNInfoChangeAndCopy(QPUINT32 n_iAPNMask, QP_DCM_APN_PARAMS_TYPE *pNewAPNParams);
  QPBOOL IsDeleteProfileNeeded(QPBOOL n_bIsProfileChangeEv);
  QPVOID StartRetainFirstPCSCFAddressTimer();
  QPBOOL IsProfileToBeRecreated(const QP_APN_ATTR_INFO& n_sApnAttrInfo);
  QPVOID UpdateAPNNameAndIPTypeInPM();
  QPBOOL IsPCSCFBasedIPFBRequired();
  QPVOID PerformPCSCFBasedIPFB();
  QPVOID SetHoSysPref();
  QPE_DCM_APN_PREF_SYS GetAPNSysPreference();
  QPVOID BringupPdp();
  QPE_DCM_RAT QueryPreferredforAPN();
  QPE_DCM_RAT GetPreferredRATForAPN(QPUINT32 numAvailAPNs , QPDCM_PREF_SYS_INFO* sysPrefInfos);
  QPBOOL CheckIfPreferencesMatch(QPE_DCM_RAT preferredRAT);
  QPVOID SetPreferenceForApn(QPE_DCM_APN_PREF_SYS currentPreference);
  QPBOOL ShouldRMWaitForSystemStatusIndication();


private:
  QPE_DCM_RAT             m_ePDPActivatedRAT; //Track the RAT on which PDP was activated.
  CPDNRecoveryTimer*      m_pPDNTempBlockTimer;
  CPDNRecoveryTimer*      m_pPDNRetryTimer;
  CPDNRecoveryTimer*      m_pRetainFirstPCSCFAddressTimer;
  QPE_DCM_RAT             m_pPDNRetryTimerRAT;                    //RAT for which PDP retry timer has started.
  QPUINT32                m_pPDNRetryTimerValQuick;
  QPUINT32                m_pPDNRetryTimerValShort;
  QPUINT32                m_pPDNRetryTimerValLong;
  QPUINT32                m_iNoServiceRetryTimer;
  QPUINT32                m_iEHRPDRecoveryTimer;
  QPUINT32                m_iTempBlockTimer;
  QPUINT32                m_pPDNRetryTimerValInternalErr;
  QPUINT8                 m_iMaxIntermediatePDPRetries;           //Maximum number PDP activation retries done before informing clients
  QPUINT8                 m_iNumIntermediatePDPFailureCnt;        //Count of intermediate retries
  QPBOOL                  m_bBlockIMSPDNConnPermanently;         //this flag will be set to true in case of max 400/02/403 attempts
  QPBOOL                  m_iBlockRegPermDueToRejEvt;
  QPBOOL                  m_iBlockRegTemporarily;
  QPUINT8                 m_iOprtMode;                           //Hold the customer mode
  QPE_DCM_RAT             m_eHORatInfo;
  WIFIMSRMNTSTATE         m_eWifiMsrmntState;
  QPUINT8                 m_iOperatorRsrvdPCO;          /* OPerator resreved PCO */
  QPBOOL                  m_bBlockIMSOnUMTS;
  QPBOOL                  m_bIsPCSCFBasedIPFBRequired;

  QPBOOL                  m_bWaitForHoSysPref;
  QPE_DCM_APN_PREF_SYS    m_eLastSetHoPreference;
  QPBOOL                  m_bProfileDeletionPending;
  QPBOOL                  m_bIsPriorityIPTypeToBeUsed;   /*After LPM Priority IP Type to be used */
  QPE_IPADDR_TYPE         m_ePreferredIPType;
  
  QPUINT8                 m_iIPFallbackCounter;
  
};

#endif //__PDPRAT_HANDLER_VOLTE_H__
