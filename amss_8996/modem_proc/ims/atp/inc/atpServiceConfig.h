#ifndef __ATPSERVICECONFIG_H__
#define __ATPSERVICECONFIG_H__

/*----------------------------------------------------------------------------
 Header Includes
----------------------------------------------------------------------------*/
#include "qpIMSServConfigDefines.h"
#include "IMSServConfigEvent.h"
#include <qpPlatformConfig.h>

typedef enum
{
  PDP_DOWN = 0,
  PDP_UP
}PDP_STATUS;


//Forward Declaration
class CPDPHandler;

typedef QPVOID (*PDP_HANDLER_CALLBACK)(PDP_STATUS pdpStatus, QPVOID* pUserData);

class ATPServiceConfig : public EventListener
{
public:
  ATPServiceConfig();

  ~ATPServiceConfig();

  QPVOID InitAndBringUpPDN(QPCHAR* apnName, PDP_HANDLER_CALLBACK pCallBack, QPVOID* pUserData, QPE_DCM_RAT rat = DCM_RAT_LTE, QPE_APN_TYPE apnType = DCM_APN_INTERNET, QPE_IPADDR_TYPE addrType = IPV4, QPBOOL isPDNShareable = TRUE);

  QPVOID update(EventObject& n_Ev);

  CPDPHandler* getPDPHandler() { return m_pPDPHandler; };

private:
  CPDPHandler*                              m_pPDPHandler;

  PDP_HANDLER_CALLBACK                      m_pCallBack;

  QPVOID*									m_pUserData;

  QPVOID initMafFramework();
};

#endif //__ATPSERVICECONFIG_H__