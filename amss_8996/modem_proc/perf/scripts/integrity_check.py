##  @file       integrity_check.py.
#   @author     raholr.
#   @brief      This script calculates the checksums for the RO_Compressed Region.
#   @version    1.0.
#===============================================================================
# Copyright (c) 2014 by Qualcomm Technologies, Incorporated.
# All Rights Reserved.
# Qualcomm Technologies Proprietary/GTDR
#
#-------------------------------------------------------------------------------
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when      who     ver     what, where, why
# --------  ------  ----    ----------------------------------------------------
# 06/30/15  raholr  1.0     Created the script
#-------------------------------------------------------------------------------

import sys
import re
import os
import inspect

file_path = inspect.getframeinfo(inspect.currentframe()).filename
file_dir, file_name = os.path.split(file_path)

sz_bit = 14
block_size = 1 << sz_bit
max_checksum_blocks = 1 << (sz_bit -2)
max_size = max_checksum_blocks << sz_bit
checksum_size = 4


# import elfManipualtor {{{
elfManipulator_path = os.path.abspath(
  os.path.join(
    file_dir,
    'elfManipulator',
    'include'))

sys.path.insert(0, elfManipulator_path)

import elfUtils as utils
import elfConstants as const
import elfFileClass as elfFileClass
import dynRec_manip as editor

##
# @brief    calculate xor checksums for specified address range and store in specified location
# @param    elf          The elfFileClass object to read from
# @param    start      The starting point to read from
# @param    end        end point
# @pre      returns pointer to next location in checksum array
def checksum_xor(elf_handle, start, end, write_ptr):
  count = 0
  temp = editor.readDataByAddress(elf_handle, start, 4)
  print temp
  while start < end:
    step_end = start + block_size
    if step_end > end:
	  step_end = end;
    block_xor = 0
    while start < step_end:
      temp = editor.readDataByAddress(elf_handle, start, 4)
      block_xor = block_xor ^ temp
      start = start + 4
    editor.setDataByAddress(elf_handle, write_ptr, 4, block_xor)
    write_ptr = write_ptr + 4
    count = count + 1
  return write_ptr
  

##
# @brief    calculate fletcher's checksums for specified address range and store in specified location
# @param    elf          The elfFileClass object to read from
# @param    start      The starting point to read from
# @param    end        end point
# @pre      returns pointer to next location in checksum array  
def checksum_fletcher(elf_handle, start, end, write_ptr):
  count = 0
  temp = editor.readDataByAddress(elf_handle, start, 4)
  while start < end:
   step_end = start + block_size
   if step_end > end:
     step_end = end
   sa = 0
   sb = 0
   while start < step_end:
     temp = editor.readDataByAddress(elf_handle, start, 4)
     sa = sa + temp
     sb = sb + sa
     start = start + 4
   sf = (sb ^ sa) % 4294967296
   editor.setDataByAddress(elf_handle, write_ptr, 4, sf)
   write_ptr = write_ptr + 4
   count = count + 1 
  return write_ptr  
  


def main(): 
  Dict = {}
  elf_handle = elfFileClass.elfFile(sys.argv[1])
  q6_roSection = ".candidate_compress_section"
  retcode = 0
  compress_text_begin = "perf_integritycheck_compress_section_begin" 
  compress_text_size =  "perf_integritycheck_compress_section_size"
  
  checksum_array = elf_handle.getSymbolByName("perf_integritycheck_checksum_buf")
  if checksum_array == const.RC_ERROR :
    print " "*3 + "-> ! Cannot find \'perf_integritycheck_checksum_buf'\, integritycheck disabled"
    sys.exit(1)
  
  sh = elf_handle.getSectionByName(q6_roSection)
  if sh == const.RC_ERROR:
    print " "*3 + "-> ! No candidate compress section found"
    sys.exit(1)
  if(sh.sh_size > max_size):
    size = max_size
    print " "*3 + "-> ! Warning: candidate_compress size is greater than max size! Running Integrity Check for first %d bytes" %(max_size)
    retcode = 2
  size = sh.sh_size
  start = sh.sh_addr
  end = start + size
  
  Dict[compress_text_begin] = start
  Dict[compress_text_size] = size
  
  
  start_ptr = checksum_array.st_value
  write_ptr = checksum_fletcher(elf_handle, start, end, start_ptr)
  

 
  elf_handle.updateSymbolValuesByDict(Dict)
  elf_handle.writeOutELF(sys.argv[2])
  sys.exit(retcode)
  
  
  
  
if __name__ == '__main__':
  main()

