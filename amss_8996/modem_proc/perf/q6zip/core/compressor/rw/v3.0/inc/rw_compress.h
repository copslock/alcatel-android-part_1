#ifndef RW_COMPRESS_H
#define RW_COMPRESS_H

#define RW_v3_0

extern unsigned int deltaCompress(unsigned int *,unsigned int *,unsigned int);
extern unsigned int deltaUncompress(unsigned int *compressed,unsigned int in_len,unsigned int *uncompressed,unsigned int out_len);
#endif /* RW_COMPRESS_H */

