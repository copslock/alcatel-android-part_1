import os
import inspect

RO_VERSION = "v3-d2"
RW_VERSION = "v1.1"
#RO_VERSION = "v5"
#RW_VERSION = "v2"
#RO_VERSION = "v7"
ENABLE_Q6ZIP_IPA = True

curr_dir = os.path.split(inspect.getframeinfo(inspect.currentframe()).filename)[0]


RO_BUILDTIME_PATH          = "%s/ro/%s/bin"  % (curr_dir, RO_VERSION)
RO_RUNTIME_FULL_SRCPATH    = "%s/ro/%s/src" % (curr_dir, RO_VERSION)
RO_RUNTIME_FULL_INCPATH    = "%s/ro/%s/inc" % (curr_dir, RO_VERSION)
RO_RUNTIME_PARTIAL_SRCPATH = "ro/%s"        % (RO_VERSION)

RW_BUILDTIME_PATH          = "%s/rw/%s/bin"  % (curr_dir, RW_VERSION)
RW_RUNTIME_FULL_SRCPATH    = "%s/rw/%s/src" % (curr_dir, RW_VERSION) 
RW_RUNTIME_FULL_INCPATH    = "%s/rw/%s/inc" % (curr_dir, RW_VERSION)
RW_RUNTIME_PARTIAL_SRCPATH = "rw/%s"        % (RW_VERSION)


RO_DECOMPRESSOR_SRC = "q6zip_uncompress.c"
if "q6zip_uncompress.s" in os.listdir(RO_RUNTIME_FULL_SRCPATH):
        RO_DECOMPRESSOR_SRC = "q6zip_uncompress.s"

RW_COMPRESSOR_SRC = "rw_compress.c"
if "rw_compress.s" in os.listdir(RW_RUNTIME_FULL_SRCPATH):
        RW_COMPRESSOR_SRC = "rw_compress.s"

RW_DECOMPRESSOR_SRC = "rw_compress.c"
if "rw_uncompress.c" in os.listdir(RW_RUNTIME_FULL_SRCPATH):
        RW_DECOMPRESSOR_SRC = "rw_uncompress.c"
if "rw_uncompress.s" in os.listdir(RW_RUNTIME_FULL_SRCPATH):
        RW_DECOMPRESSOR_SRC = "rw_uncompress.s"




CCFLAGS = '-DQ6ZIP_RO_ALG_VERS=\\\"'+RO_VERSION+'\\\"'
CCFLAGS = CCFLAGS + ' -DQ6ZIP_RW_ALG_VERS=\\\"'+RW_VERSION+'\\\"'
if ENABLE_Q6ZIP_IPA:
        CCFLAGS = CCFLAGS + ' -DENABLE_Q6ZIP_IPA'
        

SRCLIST = []
SRCLIST.append('ro/%s/src/%s' % (RO_VERSION, RO_DECOMPRESSOR_SRC))
SRCLIST.append('rw/%s/src/%s' % (RW_VERSION, RW_COMPRESSOR_SRC))
if RW_DECOMPRESSOR_SRC != RW_COMPRESSOR_SRC:
        SRCLIST.append('rw/%s/src/%s' % (RW_VERSION, RW_DECOMPRESSOR_SRC))
