	.text
.Ltext0:

/***************************************************************************
* jump_len_table is the jump_table,  it is 32Byte aligned to fit in a cache line
*  In the main decode algo
***************************************************************************/
.section	.rodata
.p2align 5
.jump_len_table:
/* 0*/	.word	.MATCH_FFFFFF00_SQ0
		.word	3
/* 1*/	.word	.MATCH_FFFFFFFF_SQ0
		.word	3
/* 2*/	.word	.MATCH_FFFFF000_SQ0
		.word	4
/* 3*/	.word	.NO_MATCH
		.word	3
/* 4*/	.word	.DICT1_MATCH
		.word	3
/* 5*/	.word	.DICT2_MATCH
		.word	4
/* 6*/	.word	.MATCH_FFFFFF00_SQ1
		.word	3
/* 7*/	.word	.MATCH_FFFFFFFF_SQ1
		.word	3
/* 8*/	.word	.MATCH_FFFFFF00_SQ0
		.word	3
/* 9*/	.word	.MATCH_FFFFFFFF_SQ0
		.word	3
/*10*/	.word	.LEVEL_A
		.word	4
/*11*/	.word	.NO_MATCH
		.word	3
/*12*/	.word	.DICT1_MATCH
		.word	3
/*13*/	.word	.LEVEL_B1
		.word	4
/*14*/	.word	.MATCH_FFFFFF00_SQ1
		.word	3
/*15*/	.word	.MATCH_FFFFFFFF_SQ1
		.word	3

#define HOLD_LS      r18     /* Global  */
#define HOLD_MS      r19     /* Global  */
#define HOLD_LOW     r19:18  /* Global  */
#define HOLD_TOP     r21:20  /* Global  */
#define HOLD_TMP     r13:12  /* Global  */
#define COMP_PTR     r16     /* Global  Pointer to start of the compressed data */
#define OFFSET       r7     /* Global  Offset in bits from start of compressed data */
#define NEG_OFFSET   r11     /* Temp    Offset in bits from top of HOLD_LOW*/

#define LEN_TGT_TABLE r10   /* Global Points to the jump targets for each code */
#define HDR_IDX       r27   /* Global valid on entry to each block - ls 4 bits of each code */
#define HDR_LEN       r9    /* Global valid on entry to each block - length of each code 3 or 4(or more) bits */
#define HDR_TGT       r8    /* Global valid on entry to each block - Next jump target */
#define HDR_LEN_TGT   r9:8  /* Global valid on entry to each block - */
#define LB_IDX        r25   /* Global Note this is set to -1 and ls 9 bits are updated */
#define OUT_PTR       r0    /* Global Points to the 4KB output buffer */
#define OUT_SIZE_PTR  r1    /* Global Points to final size of the decode output buffer */
#define START_OUT_PTR r26   /* Global Points to the 4KB output buffer */
#define RAW_PTR       r14   /* Global Points to the raw 32bit words*/
#define HOLD_CPY      r3:2  /* Local 64b copy of the hold register*/
#define HOLD_CPY_LS   r2    /* Local 32b copy of the hold register*/
#define LB_WORD       r5    /* Local bits of the Lookback word*/
#define RAW_WORD      r5    /* Local bits of the Lookback word*/
#define NLB_WORD      r4    /* Local bits from NOT the Lookback word*/
#define TEMP          r4

#define DICT1_PTR     r15
#define DICT2_PTR     r17

//TEMP regs r22, r23 , r24, r28
#define DCT1_OFF_SZ  10
#define DCT2_OFF_SZ  12

#define LB_BITS          9
#define PL_MASK8         ( 8 + LB_BITS) /* payload length with 8bit mask */
#define PL_MASK12        (12 + LB_BITS) /* payload length with 12bit mask */
#define PL_MASK16        (16 + LB_BITS) /* payload length with 16bit mask */

#define PREF_DIST    128
/***************************************************************************
* q6zip_uncompress function entry point
*   r0=char* out_buf
*   r1=int* out_buf_size
*   r2=char* in_buf
*   r3=int in_buf_size
*   r4=char* dict
***************************************************************************/
.text
.p2align 2
.p2align 4,,15
.globl q6zip_uncompress
.type	q6zip_uncompress, @function
q6zip_uncompress:
.file 1 "q6zip_uncompress.c"
    //r22 = #0
    {
      allocframe(#112)
      call __save_r16_through_r27
    }
    {
      r3 = add(r3, #31)
    }
    {
      r16   = lsr(r3, #5)         /* Height - Number of 32B cache lines to read*/
      r17   = #32                 /* Width */
    }
    r17   =combine(r17.l,r16.l)
    r17:16=combine(##32,r17)    
    l2fetch(r4,r17:16)

    /*OUT_PTR   = r0*/
    /*OUT_SIZE  = r1*/
    {
      START_OUT_PTR = OUT_PTR
      DICT1_PTR = r4
    }
    {
      DICT2_PTR = add(DICT1_PTR,#4096)    /* derive dictionary2 pointer*/
      LEN_TGT_TABLE = ##.jump_len_table
    }
    dcfetch(LEN_TGT_TABLE+#0)
    dcfetch(LEN_TGT_TABLE+#32)
    dcfetch(LEN_TGT_TABLE+#64)
    dcfetch(LEN_TGT_TABLE+#96)
    {
      loop0(.DCZERO_OUT_BUF,#128)
      RAW_PTR       = add(r2,#4)          /* R2 points to the compressed input */ 
    }
    dcfetch(RAW_PTR+#0)
    dcfetch(RAW_PTR+#32)
    dcfetch(RAW_PTR+#64)
    dcfetch(RAW_PTR+#96)
    r2 = memw(r2)                         /* first word==raw length*/
    r2 = asl(r2,#2)                       /* Convert from word offset to bytes offset */
    COMP_PTR = add(RAW_PTR, r2)           /* Compressed data starts here */
    {
      COMP_PTR = add(COMP_PTR, #8)        /* Point to the second double word */
      dcfetch(COMP_PTR+#0)
      LB_IDX = #-1                        /* used for last offset */
    }

    dcfetch(COMP_PTR+#0)
    dcfetch(COMP_PTR+#32)
    dcfetch(COMP_PTR+#64)
    dcfetch(COMP_PTR+#96)    
    /***************************************************************************
    * Allocate and zero the 4KB output buffer using dczeroa
    ***************************************************************************/
    .falign
    .DCZERO_OUT_BUF:
    {
        dczeroa(r0)
        OUT_PTR = add(OUT_PTR,#32)
    }:endloop0
    OUT_PTR=add(OUT_PTR,#-(32*128))

    /***************************************************************************
    * Setup the endloop counter 
    ***************************************************************************/
    TEMP     = #1025               /* used to load LC0 */
    LC0      = TEMP
    /***************************************************************************************/
    /* Startup                                                                             */
    /* Holding Register    | N1 HDR | N0_HDR     |                                        */
    /* Holding Register    |   4bit | 4 or 3 bit |                                        */
    /***************************************************************************************/
	{ OFFSET     =#0 ;HOLD_LOW=memd(COMP_PTR + #-8)}
    /***************************************************************************************/
    { HOLD_CPY    = lsr(HOLD_LOW, #0)               }  /* Remove the N+1 HDR, everything fits into LS 32bits */
    { HDR_IDX     = extractu(HOLD_CPY_LS, #4, #0)    }
    { HDR_LEN_TGT = memd(LEN_TGT_TABLE+HDR_IDX<<#3)  }
    /***************************************************************************************/
    /* Now we start with a near regular decode and jump*/ 
    /***************************************************************************************/
    { OFFSET     = add(OFFSET,add(HDR_LEN,#0)); HOLD_LOW=memd(COMP_PTR + #-8)              ;SA0 = HDR_TGT                              }
    { OFFSET     = and( OFFSET,#63)           ; p0=cmp.gt(OFFSET,#63)                      ;HOLD_CPY = lsr(HOLD_LOW, HDR_LEN)          }  /* Remove the N0 HDR, everything fits into LS 32bits */
    { NEG_OFFSET = sub(#64,OFFSET)            ; if (p0) HOLD_LOW=memd(COMP_PTR++#8)        ;HDR_IDX = extractu(HOLD_CPY_LS, #4, #0)    }
    { HOLD_TMP=lsr(HOLD_LOW,OFFSET)           ; HOLD_TOP=memd(COMP_PTR)                    ;HDR_LEN_TGT = memd(LEN_TGT_TABLE+HDR_IDX<<#3)  }
    { HOLD_TMP|=lsl(HOLD_TOP,NEG_OFFSET)      ; OFFSET     = add(OFFSET,HDR_LEN);          }:endloop0
    /***************************************************************************************/


/***************************************************************************************/
/* DICT2_MATCH  uses 15 or 16 bits                                              */
/* Holding Register    | N+2 HDR |PAYLOAD mask  | N+1_HDR    |              */
/* Holding Register    |    4bit |    12bit     | 4 or 3 bit |              */
/***************************************************************************************/
.falign
.DICT2_MATCH:
{ OFFSET = add(OFFSET,#12)           ; dcfetch(COMP_PTR+#PREF_DIST)                  ; SA0 = HDR_TGT                                  ; HOLD_CPY = lsr(HOLD_TMP, HDR_LEN)        }
{ OFFSET = and( OFFSET,#63)          ; p0=cmp.gt(OFFSET,#63)                         ; TEMP = extractu(HOLD_CPY_LS, #12, #0)          ; HDR_IDX = extractu(HOLD_CPY_LS, #4, #12) }
{ NEG_OFFSET = sub(#64,OFFSET)       ; if (p0) HOLD_LOW=memd(COMP_PTR++#8)           ; TEMP = memw(DICT2_PTR+TEMP<<#2);               }
{ HOLD_TMP=lsr(HOLD_LOW,OFFSET)      ; HOLD_TOP=memd(COMP_PTR)                       ; HDR_LEN_TGT = memd(LEN_TGT_TABLE+HDR_IDX<<#3)  }
{ HOLD_TMP|=lsl(HOLD_TOP,NEG_OFFSET) ; OFFSET = add(OFFSET,HDR_LEN)                  ; memw(OUT_PTR++#4) = TEMP                       } :endloop0
jump .EXIT

/***************************************************************************************/
/* DICT1_MATCH  uses 13 or 14 bits                                              */
/* Holding Register    | N+2 HDR |PAYLOAD mask  | N+1_HDR    |              */
/* Holding Register    |    4bit |    10bit     | 4 or 3 bit |              */
/***************************************************************************************/
.falign
.DICT1_MATCH:
{ OFFSET = add(OFFSET,#10)           ; dcfetch(COMP_PTR+#PREF_DIST)                  ; SA0 = HDR_TGT                                  ; HOLD_CPY = lsr(HOLD_TMP, HDR_LEN)        }
{ OFFSET = and( OFFSET,#63)          ; p0=cmp.gt(OFFSET,#63)                         ; TEMP = extractu(HOLD_CPY_LS, #10, #0)          ; HDR_IDX = extractu(HOLD_CPY_LS, #4, #10) }
{ NEG_OFFSET = sub(#64,OFFSET)       ; if (p0) HOLD_LOW=memd(COMP_PTR++#8)           ; TEMP = memw(DICT1_PTR+TEMP<<#2);               }
{ HOLD_TMP=lsr(HOLD_LOW,OFFSET)      ; HOLD_TOP=memd(COMP_PTR)                       ; HDR_LEN_TGT = memd(LEN_TGT_TABLE+HDR_IDX<<#3)  }
{ HOLD_TMP|=lsl(HOLD_TOP,NEG_OFFSET) ; OFFSET = add(OFFSET,HDR_LEN)                  ; memw(OUT_PTR++#4) = TEMP                       } :endloop0
jump .EXIT

/***************************************************************************************/
/* NO_MATCH  uses 7 or 8 bits                                              */
/* Holding Register    | N+2 HDR |PAYLOAD mask | N+1_HDR    |              */
/* Holding Register    |    4bit |    0bit     | 4 or 3 bit |              */
/***************************************************************************************/
.falign
.NO_MATCH:
{                                                                                    ; SA0 = HDR_TGT                                  ; HOLD_CPY = lsr(HOLD_TMP, HDR_LEN)       }
{ OFFSET = and( OFFSET,#63)          ; p0=cmp.gt(OFFSET,#63)                         ; RAW_WORD = memw(RAW_PTR++#4)                   ; dcfetch(COMP_PTR+#PREF_DIST)}
{ NEG_OFFSET = sub(#64,OFFSET)       ; if (p0) HOLD_LOW=memd(COMP_PTR++#8)           ; HDR_IDX = extractu(HOLD_CPY_LS, #4, #0)        ; dcfetch(RAW_PTR+#PREF_DIST)}
{ HOLD_TMP=lsr(HOLD_LOW,OFFSET)      ; HOLD_TOP=memd(COMP_PTR)                       ; HDR_LEN_TGT = memd(LEN_TGT_TABLE+HDR_IDX<<#3)  }
{ HOLD_TMP|=lsl(HOLD_TOP,NEG_OFFSET) ; OFFSET = add(OFFSET,HDR_LEN)                  ; memw(OUT_PTR++#4) = RAW_WORD                   } :endloop0
jump .EXIT

/***************************************************************************************/
/* MATCH_FFFFFFFF_SQ0  uses 14 or 15 bits                                              */
/* Holding Register    | N+2 HDR |PAYLOAD  | N+1_HDR    |                              */
/* Holding Register    |    4bit |    9bit | 4 or 3 bit |                              */
/***************************************************************************************/
.falign
.MATCH_FFFFFFFF_SQ0:
{ OFFSET = add(OFFSET,#LB_BITS)      ; dcfetch(COMP_PTR+#PREF_DIST)                  ; SA0 = HDR_TGT                                  ; HOLD_CPY = lsr(HOLD_TMP, HDR_LEN)        }
{ OFFSET = and( OFFSET,#63)          ; p0=cmp.gt(OFFSET,#63)                         ; LB_IDX = insert(HOLD_CPY_LS, #LB_BITS, #0)     ; HDR_IDX = extractu(HOLD_CPY_LS, #4, #LB_BITS) }
{ NEG_OFFSET = sub(#64,OFFSET)       ; if (p0) HOLD_LOW=memd(COMP_PTR++#8)           ; LB_WORD = memw(OUT_PTR+LB_IDX<<#2)             }
{ HOLD_TMP=lsr(HOLD_LOW,OFFSET)      ; HOLD_TOP=memd(COMP_PTR)                       ; HDR_LEN_TGT = memd(LEN_TGT_TABLE+HDR_IDX<<#3)  }
{ HOLD_TMP|=lsl(HOLD_TOP,NEG_OFFSET) ; OFFSET = add(OFFSET,HDR_LEN)                  ; memw(OUT_PTR++#4) = LB_WORD                    } :endloop0
jump .EXIT
/***************************************************************************************/
/* MATCH_FFFFFFFF_SQ1  uses 7 or 8 bits                                              */
/* Holding Register    | N+2 HDR |PAYLOAD  | N+1_HDR    |                              */
/* Holding Register    |    4bit |    0bit | 4 or 3 bit |                              */
/***************************************************************************************/
.falign
.MATCH_FFFFFFFF_SQ1:
{ OFFSET = add(OFFSET,#0)            ; dcfetch(COMP_PTR+#PREF_DIST)                  ; SA0 = HDR_TGT                                  ; HOLD_CPY = lsr(HOLD_TMP, HDR_LEN)        }
{ OFFSET = and( OFFSET,#63)          ; p0=cmp.gt(OFFSET,#63)                         ; HDR_IDX = extractu(HOLD_CPY_LS, #4, #0)        ; dcfetch(COMP_PTR+#64)}
{ NEG_OFFSET = sub(#64,OFFSET)       ; if (p0) HOLD_LOW=memd(COMP_PTR++#8)           ; LB_WORD = memw(OUT_PTR+LB_IDX<<#2)             }
{ HOLD_TMP=lsr(HOLD_LOW,OFFSET)      ; HOLD_TOP=memd(COMP_PTR)                       ; HDR_LEN_TGT = memd(LEN_TGT_TABLE+HDR_IDX<<#3)  }
{ HOLD_TMP|=lsl(HOLD_TOP,NEG_OFFSET) ; OFFSET = add(OFFSET,HDR_LEN)                  ; memw(OUT_PTR++#4) = LB_WORD                    } :endloop0
jump .EXIT

/***************************************************************************************/
/* MATCH_FFFFFF00_SQ0  uses 24 or 25 bits                                              */
/* Holding Register    | N+2 HDR |PAYLOAD mask  + lb | N+1_HDR    |                    */
/* Holding Register    |    4bit |    8bit + 9bit   | 4 or 3 bit |                    */
/***************************************************************************************/
.falign

.MATCH_FFFFFF00_SQ0:
{ OFFSET     = add(OFFSET,#PL_MASK8) ; dcfetch(COMP_PTR+#PREF_DIST)                       ; SA0 = HDR_TGT                               ; HOLD_CPY = lsr(HOLD_TMP, HDR_LEN)          }
{ OFFSET = and( OFFSET,#63)          ; p0=cmp.gt(OFFSET,#63)                              ; LB_IDX = insert(HOLD_CPY_LS, #LB_BITS, #0)  ; HDR_IDX = extractu(HOLD_CPY_LS, #4, #PL_MASK8) }
{ NEG_OFFSET = sub(#64,OFFSET)       ; if (p0)  HOLD_LOW=memd(COMP_PTR++#8)               ; LB_WORD = memw(OUT_PTR+LB_IDX<<#2)          ; NLB_WORD = extractu(HOLD_CPY_LS, #8, #LB_BITS) } 
{ HOLD_TMP=lsr(HOLD_LOW,OFFSET)      ; HOLD_TOP=memd(COMP_PTR)                            ; LB_WORD = insert(NLB_WORD, #8, #0)          ; HDR_LEN_TGT = memd(LEN_TGT_TABLE+HDR_IDX<<#3) }
{ HOLD_TMP|=lsl(HOLD_TOP,NEG_OFFSET) ; OFFSET     = add(OFFSET,HDR_LEN)                   ; memw(OUT_PTR++#4) = LB_WORD             }:endloop0 
jump .EXIT

/***************************************************************************************/
/* MATCH_FFFFF000_SQ0  uses 28 or 29 bits                                              */
/* Holding Register    | N+2 HDR |PAYLOAD mask  + lb | N+1_HDR    |                    */
/* Holding Register    |    4bit |    12bit + 9bit   | 4 or 3 bit |                    */
/***************************************************************************************/
.falign
.MATCH_FFFFF000_SQ0:
{ OFFSET     = add(OFFSET,#PL_MASK12) ; dcfetch(COMP_PTR+#PREF_DIST)                       ; SA0 = HDR_TGT                              ; HOLD_CPY = lsr(HOLD_TMP, HDR_LEN)          }
{ OFFSET = and( OFFSET,#63)           ; p0=cmp.gt(OFFSET,#63)                              ; LB_IDX = insert(HOLD_CPY_LS, #LB_BITS, #0) ; HDR_IDX = extractu(HOLD_CPY_LS, #4, #PL_MASK12) }
{ NEG_OFFSET = sub(#64,OFFSET)        ; if (p0)  HOLD_LOW=memd(COMP_PTR++#8)               ; LB_WORD = memw(OUT_PTR+LB_IDX<<#2)         ; NLB_WORD = extractu(HOLD_CPY_LS, #12, #9) } 
{ HOLD_TMP=lsr(HOLD_LOW,OFFSET)       ; HOLD_TOP=memd(COMP_PTR)                            ; LB_WORD = insert(NLB_WORD, #12, #0)        ; HDR_LEN_TGT = memd(LEN_TGT_TABLE+HDR_IDX<<#3) }
{ HOLD_TMP|=lsl(HOLD_TOP,NEG_OFFSET)  ; OFFSET     = add(OFFSET,HDR_LEN)                   ; memw(OUT_PTR++#4) = LB_WORD                ; }:endloop0 
jump .EXIT


/***************************************************************************************/
/* MATCH_FFFFFF00_SQ1  uses 15 or 16 bits                                              */
/* Holding Register    | N+2 HDR |PAYLOAD mask | N+1_HDR    |                    */
/* Holding Register    |    4bit |    8bit     | 4 or 3 bit |                    */
/***************************************************************************************/
.falign
.MATCH_FFFFFF00_SQ1:
{ OFFSET     = add(OFFSET,#8)          ; dcfetch(COMP_PTR+#PREF_DIST)                 ; SA0 = HDR_TGT                            ; HOLD_CPY = lsr(HOLD_TMP, HDR_LEN)        }
{ OFFSET = and( OFFSET,#63)            ; p0=cmp.gt(OFFSET,#63)                        ; NLB_WORD = extractu(HOLD_CPY_LS, #8, #0) ; LB_WORD = memw(OUT_PTR+LB_IDX<<#2)       } 
{ NEG_OFFSET = sub(#64,OFFSET)         ; if (p0)  HOLD_LOW=memd(COMP_PTR++#8)         ; LB_WORD = insert(NLB_WORD, #8, #0)       ; HDR_IDX = extractu(HOLD_CPY_LS, #4, #8)  } 
{ HOLD_TMP=lsr(HOLD_LOW,OFFSET)        ; HOLD_TOP=memd(COMP_PTR)                      ; HDR_LEN_TGT = memd(LEN_TGT_TABLE+HDR_IDX<<#3); }
{ HOLD_TMP|=lsl(HOLD_TOP,NEG_OFFSET)   ; OFFSET = add(OFFSET,HDR_LEN)                 ; memw(OUT_PTR++#4) = LB_WORD              }:endloop0 
jump .EXIT


.falign
.LEVEL_B1:
/***************************************************************************************/
/* LEVEL_B includes                                                                    */
/* MATCH_FFFF0000_SQ0 - uses 26 bits or                                                */
/* MATCH_FFFFF000_SQ1 - uses 13 bits                                                   */
/***************************************************************************************/
{                                                                                    ; SA0 = HDR_TGT                              ; HOLD_CPY = lsr(HOLD_TMP, HDR_LEN)     }
{ TEMP = and(HOLD_CPY_LS,#1)          ; if (cmp.eq(TEMP.new,#1)) jump:t .MATCH_FFFFF000_SQ1                                       ; HOLD_CPY = lsr(HOLD_CPY, #1)          }

/***************************************************************************************/
/* MATCH_FFFF0000_SQ0  uses 32 or 33 bits                                              */
/* Holding Register    | N+2 HDR |PAYLOAD mask  + lb      | N+1_HDR    |                    */
/* Holding Register    |    4bit |16bit + 9bit +1bit hdr  | 4 or 3 bit |                    */
/***************************************************************************************/
.falign
.MATCH_FFFF0000_SQ0:
{ OFFSET = add(OFFSET,#(PL_MASK16 + 1)) ; dcfetch(COMP_PTR+#PREF_DIST)}
{ OFFSET = and( OFFSET,#63)             ; p0=cmp.gt(OFFSET,#63)                        ; LB_IDX = insert(HOLD_CPY_LS, #LB_BITS, #0) ; HDR_IDX = extractu(HOLD_CPY_LS, #4, #PL_MASK16)  }  
{ NEG_OFFSET = sub(#64,OFFSET)          ; if (p0)  HOLD_LOW=memd(COMP_PTR++#8)         ; LB_WORD = memw(OUT_PTR+LB_IDX<<#2)         ; NLB_WORD = extractu(HOLD_CPY_LS, #16, #LB_BITS)      } 
{ HOLD_TMP=lsr(HOLD_LOW,OFFSET)         ; HOLD_TOP=memd(COMP_PTR)                      ; LB_WORD = insert(NLB_WORD, #16, #0)        ; HDR_LEN_TGT = memd(LEN_TGT_TABLE+HDR_IDX<<#3) }
{ HOLD_TMP|=lsl(HOLD_TOP,NEG_OFFSET)    ; OFFSET = add(OFFSET,HDR_LEN)                 ; memw(OUT_PTR++#4) = LB_WORD                }:endloop0
jump .EXIT

/***************************************************************************************/
/* MATCH_FFFFF000_SQ1  uses 16 or 17 bits                                              */
/* Holding Register    | N+2 HDR |PAYLOAD mask  + lb      | N+1_HDR    |               */
/* Holding Register    |    4bit |12bit + 1bit hdr        | 4 or 3 bit |               */
/***************************************************************************************/
.falign
.MATCH_FFFFF000_SQ1:
{ OFFSET = add(OFFSET,#13)             ; dcfetch(COMP_PTR+#PREF_DIST) }
{ OFFSET = and( OFFSET,#63)            ; p0=cmp.gt(OFFSET,#63)                        ; NLB_WORD = extractu(HOLD_CPY_LS, #12, #0) ; LB_WORD = memw(OUT_PTR+LB_IDX<<#2)       } 
{ NEG_OFFSET = sub(#64,OFFSET)         ; if (p0)  HOLD_LOW=memd(COMP_PTR++#8)         ; LB_WORD = insert(NLB_WORD, #12, #0)       ; HDR_IDX = extractu(HOLD_CPY_LS, #4, #12) } 
{ HOLD_TMP=lsr(HOLD_LOW,OFFSET)        ; HOLD_TOP=memd(COMP_PTR)                      ; HDR_LEN_TGT = memd(LEN_TGT_TABLE+HDR_IDX<<#3); }
{ HOLD_TMP|=lsl(HOLD_TOP,NEG_OFFSET)   ; OFFSET = add(OFFSET,HDR_LEN)                 ; memw(OUT_PTR++#4) = LB_WORD              }:endloop0 
jump .EXIT

.falign
.LEVEL_A:
/***************************************************************************************/
/* LEVEL_A includes                                                                    
 * MATCH_FFFF0000_SQ1: HDR =  0b001010, PAYLOAD = 16bit mask + 2bit hdr                                              
 * MATCH_FFFF00FF_SQ0: HRD =  0b111010, PAYLOAD = 9bit LB + 8bit mask +2bit hdr  
 * MATCH_FF00FFFF_SQ0: HDR =  0b101010, PAYLOAD = 9bit LB + 8bit mask +2bit hdr  
 * MATCH_FFFF00FF_SQ1: HDR = 0b1011010, PAYLOAD = 8bit mask + 3bit hdr 
 * MATCH_FF00FFFF_SQ1: HDR = 0b0011010, PAYLOAD = 8bit mask + 3bit hdr                                               
/***************************************************************************************/
{                                     ;                                                   ; SA0 = HDR_TGT                         ; HOLD_CPY = lsr(HOLD_TMP, HDR_LEN)     }
{ TEMP = and(HOLD_CPY_LS,#3)          ; if (cmp.eq(TEMP.new,#0)) jump:t .MATCH_FFFF0000_SQ1                                       ; HOLD_CPY = lsr(HOLD_CPY, #2)          }
{ p1 =  cmp.eq(TEMP,#3)               ; if (p1.new) jump:t .MATCH_FFFF00FF_SQ0             }
{ p1 =  cmp.eq(TEMP,#2)               ; if (p1.new) jump:t .MATCH_FF00FFFF_SQ0             }
{ TEMP = and(HOLD_CPY_LS,#1)          ; if (cmp.eq(TEMP.new,#1)) jump:t .MATCH_FFFF00FF_SQ1                                       ; HOLD_CPY = lsr(HOLD_CPY, #1)          } 

/* fall through */
/***************************************************************************************/
/* MATCH_FF00FFFF_SQ1  uses 14 or 15 bits                                              */
/* Holding Register    | N+2 HDR |PAYLOAD mask + lb        | N+1_HDR    |                    */
/* Holding Register    |    4bit | 8bit + 3bit hdr         | 4 or 3 bit |                    */
/***************************************************************************************/
.falign
.MATCH_FF00FFFF_SQ1:
{ OFFSET = add(OFFSET,#11)             ; dcfetch(COMP_PTR+#PREF_DIST) }
{ OFFSET = and( OFFSET,#63)            ; p0=cmp.gt(OFFSET,#63)                        ; NLB_WORD = extractu(HOLD_CPY_LS, #8, #0) ; LB_WORD = memw(OUT_PTR+LB_IDX<<#2)       } 
{ NEG_OFFSET = sub(#64,OFFSET)         ; if (p0)  HOLD_LOW=memd(COMP_PTR++#8)         ; LB_WORD = insert(NLB_WORD, #8, #16)       ; HDR_IDX = extractu(HOLD_CPY_LS, #4, #8) } 
{ HOLD_TMP=lsr(HOLD_LOW,OFFSET)        ; HOLD_TOP=memd(COMP_PTR)                      ; HDR_LEN_TGT = memd(LEN_TGT_TABLE+HDR_IDX<<#3); }
{ HOLD_TMP|=lsl(HOLD_TOP,NEG_OFFSET)   ; OFFSET = add(OFFSET,HDR_LEN)                 ; memw(OUT_PTR++#4) = LB_WORD              }:endloop0 
jump .EXIT


/***************************************************************************************/
/* MATCH_FFFF0000_SQ1  uses 21 or 22 bits                                              */
/* Holding Register    | N+2 HDR |PAYLOAD mask + lb        | N+1_HDR    |                    */
/* Holding Register    |    4bit | 16bit + 2bit hdr        | 4 or 3 bit |                    */
/***************************************************************************************/
.falign
.MATCH_FFFF0000_SQ1:
{ OFFSET = add(OFFSET,#18)             ; dcfetch(COMP_PTR+#PREF_DIST) }
{ OFFSET = and( OFFSET,#63)            ; p0=cmp.gt(OFFSET,#63)                        ; NLB_WORD = extractu(HOLD_CPY_LS, #16, #0) ; LB_WORD = memw(OUT_PTR+LB_IDX<<#2)       } 
{ NEG_OFFSET = sub(#64,OFFSET)         ; if (p0)  HOLD_LOW=memd(COMP_PTR++#8)         ; LB_WORD = insert(NLB_WORD, #16, #0)       ; HDR_IDX = extractu(HOLD_CPY_LS, #4, #16) } 
{ HOLD_TMP=lsr(HOLD_LOW,OFFSET)        ; HOLD_TOP=memd(COMP_PTR)                      ; HDR_LEN_TGT = memd(LEN_TGT_TABLE+HDR_IDX<<#3); }
{ HOLD_TMP|=lsl(HOLD_TOP,NEG_OFFSET)   ; OFFSET = add(OFFSET,HDR_LEN)                 ; memw(OUT_PTR++#4) = LB_WORD              }:endloop0 
jump .EXIT

/***************************************************************************************/
/* MATCH_FFFF00FF_SQ0  uses 21 or 22 bits                                              */
/* Holding Register    | N+2 HDR |PAYLOAD mask                          | N+1_HDR    |                    */
/* Holding Register    |    4bit | 9bit LB + 8bit mask +2bit hdr        | 4 or 3 bit |                    */
/***************************************************************************************/
.falign
.MATCH_FFFF00FF_SQ0:
{ OFFSET = add(OFFSET,#(PL_MASK8 + 2))  ; dcfetch(COMP_PTR+#PREF_DIST)}
{ OFFSET = and( OFFSET,#63)             ; p0=cmp.gt(OFFSET,#63)                        ; LB_IDX = insert(HOLD_CPY_LS, #LB_BITS, #0) ; HDR_IDX = extractu(HOLD_CPY_LS, #4, #PL_MASK8)  }  
{ NEG_OFFSET = sub(#64,OFFSET)          ; if (p0)  HOLD_LOW=memd(COMP_PTR++#8)         ; LB_WORD = memw(OUT_PTR+LB_IDX<<#2)         ; NLB_WORD = extractu(HOLD_CPY_LS, #8, #LB_BITS)      } 
{ HOLD_TMP=lsr(HOLD_LOW,OFFSET)         ; HOLD_TOP=memd(COMP_PTR)                      ; LB_WORD = insert(NLB_WORD, #8, #8)         ; HDR_LEN_TGT = memd(LEN_TGT_TABLE+HDR_IDX<<#3) }
{ HOLD_TMP|=lsl(HOLD_TOP,NEG_OFFSET)    ; OFFSET = add(OFFSET,HDR_LEN)                 ; memw(OUT_PTR++#4) = LB_WORD                }:endloop0
jump .EXIT

/***************************************************************************************/
/* MATCH_FF00FFFF_SQ0  uses 21 or 22 bits                                              */
/* Holding Register    | N+2 HDR |PAYLOAD mask                          | N+1_HDR    |                    */
/* Holding Register    |    4bit | 9bit LB + 8bit mask +2bit hdr        | 4 or 3 bit |                    */
/***************************************************************************************/
.falign
.MATCH_FF00FFFF_SQ0:
{ OFFSET = add(OFFSET,#(PL_MASK8 + 2))  ; dcfetch(COMP_PTR+#PREF_DIST)}
{ OFFSET = and( OFFSET,#63)             ; p0=cmp.gt(OFFSET,#63)                        ; LB_IDX = insert(HOLD_CPY_LS, #LB_BITS, #0)       ; HDR_IDX = extractu(HOLD_CPY_LS, #4, #PL_MASK8)  }  
{ NEG_OFFSET = sub(#64,OFFSET)          ; if (p0)  HOLD_LOW=memd(COMP_PTR++#8)         ; LB_WORD = memw(OUT_PTR+LB_IDX<<#2)         ; NLB_WORD = extractu(HOLD_CPY_LS, #8, #LB_BITS)      } 
{ HOLD_TMP=lsr(HOLD_LOW,OFFSET)         ; HOLD_TOP=memd(COMP_PTR)                      ; LB_WORD = insert(NLB_WORD, #8, #16)        ; HDR_LEN_TGT = memd(LEN_TGT_TABLE+HDR_IDX<<#3) }
{ HOLD_TMP|=lsl(HOLD_TOP,NEG_OFFSET)    ; OFFSET = add(OFFSET,HDR_LEN)                 ; memw(OUT_PTR++#4) = LB_WORD                }:endloop0
jump .EXIT


/***************************************************************************************/
/* MATCH_FFFF00FF_SQ1  uses 14 or 15 bits                                              */
/* Holding Register    | N+2 HDR |PAYLOAD mask + lb        | N+1_HDR    |                    */
/* Holding Register    |    4bit | 8bit + 3bit hdr         | 4 or 3 bit |                    */
/***************************************************************************************/
.falign
.MATCH_FFFF00FF_SQ1:
{ OFFSET = add(OFFSET,#11)             ; dcfetch(COMP_PTR+#PREF_DIST) }
{ OFFSET = and( OFFSET,#63)            ; p0=cmp.gt(OFFSET,#63)                        ; NLB_WORD = extractu(HOLD_CPY_LS, #8, #0) ; LB_WORD = memw(OUT_PTR+LB_IDX<<#2)       } 
{ NEG_OFFSET = sub(#64,OFFSET)         ; if (p0)  HOLD_LOW=memd(COMP_PTR++#8)         ; LB_WORD = insert(NLB_WORD, #8, #8)       ; HDR_IDX = extractu(HOLD_CPY_LS, #4, #8) } 
{ HOLD_TMP=lsr(HOLD_LOW,OFFSET)        ; HOLD_TOP=memd(COMP_PTR)                      ; HDR_LEN_TGT = memd(LEN_TGT_TABLE+HDR_IDX<<#3); }
{ HOLD_TMP|=lsl(HOLD_TOP,NEG_OFFSET)   ; OFFSET = add(OFFSET,HDR_LEN)                 ; memw(OUT_PTR++#4) = LB_WORD              }:endloop0 
jump .EXIT


.falign
.EXIT:
	{
		r3 = sub(OUT_PTR,START_OUT_PTR)
	}
	{
		r0 = #0
		memw(OUT_SIZE_PTR+#0) = r3
		jump __restore_r16_through_r27_and_deallocframe
	}

.falign
.size	q6zip_uncompress, .-q6zip_uncompress
.section	.bss,"aw",@nobits
