#include "assert.h"
#include <stdint.h>
#include <stdlib.h>
#include "q6zip_uncompress.h"
#if __hexagon__
#include <hexagon_protos.h>
#if !defined( DC_ZERO )
#define DC_ZERO
#endif
#if !defined( L2_FETCH )
#define L2_FETCH
#endif
#endif



#define debug(...) info
#define info

#ifdef debug
#include <stdio.h>
#endif

static unsigned int* input;
static int* q6zip_out_buf_size;

#define GET_WORD_FROM(ci)           (*((unsigned int*)ci))
#define GET_WORD()                  ((unsigned int)(hold & 0xFFFFFFFF))
#define GET_DWORD()                 hold
#define GET_WORD_SKIPPING(skip)     ((unsigned int)(hold >> skip))
#define GET_DWORD_SKIPPING(skip)    (hold >> skip)  
#define SKIP_HOLD(n);               hold >>= n
#define SKIP_HOLD_N_BITS_W_CHECK(n) SKIP_HOLD(n); SKIP_BITS_W_CHECK(n)
#define GET_BITS(n)                 (unsigned int)(hold & ((1UL << n) - 1))
#define GET_BITS_N_SKIP_HOLD(n)     GET_BITS(n); hold >>= n
  
#define SKIP_BITS_W_CHECK(n)						\
  bits -= n;								\
  if (UNLIKELY(bits < 32)) {	 				        \
    hold |= (uint64_t)(*(in++)) << bits;				\
    Q6_dcfetch_A(in);\
    bits += 32;								\
  }


#define CACHE_LINE_SHIFT (5)
#define CACHE_LINE_SZ  (1 << CACHE_LINE_SHIFT)

#define LIKELY(x) __builtin_expect((x), 1)
#define UNLIKELY(x) __builtin_expect((x), 0)

#ifdef L2_FETCH

//#define JUMP_NEXT_DECODE  {void *jumpPtr=jump_table[op1]; if (LIKELY(op1==1)) goto MATCH_8N_SQ0;if (LIKELY(op1==9)) goto MATCH_8N_SQ0;goto *jumpPtr;}
#define JUMP_NEXT_DECODE  {void *jumpPtr=jump_table[op1]; if (LIKELY(op1==1)) goto MATCH_8N_SQ0;goto *jumpPtr;}


static inline void l2fetch_buffer
(
  void *addr,
  unsigned int len
)
{
  /* Cache-align starting address and length. */
  unsigned int ofs = ((unsigned int) addr) & (CACHE_LINE_SZ-1);
  addr = (void *) ((unsigned int) addr - ofs);
  len  = (len+ofs+CACHE_LINE_SZ-1) / CACHE_LINE_SZ;

  /* Width=cache line, height=# cache lines, stride=cache line */
  asm volatile ("l2fetch(%[addr],%[dim])" : : 
     [addr] "r" (addr), 
     [dim] "r" ( Q6_P_combine_IR(CACHE_LINE_SZ, Q6_R_combine_RlRl(CACHE_LINE_SZ, len)) )
     : "memory");

}
#endif

#ifdef DC_ZERO

/* linked in from dczero assembly version in dlpager/src */
void dczero(unsigned int addr, unsigned int size);

#endif

#define OP1_BITS 3
#define OP2_BITS 3
#define OP3_BITS 2
#define DICT1_BITS 10
#define DICT2_BITS 12
#define LB_BITS 9




int q6zip_uncompress(char* out_buf, int* out_buf_size,
		     char* in_buf,  int in_buf_size,
		     char* dict)
{
  input = (unsigned int*)in_buf;
  /* VB: set local static ptr to point to passed in value   */
  q6zip_out_buf_size = out_buf_size;
  unsigned int* dictionary1 = (unsigned int*)dict;
  unsigned int* dictionary2 = &dictionary1[1<<DICT1_BITS];
  unsigned int* out = (unsigned int*)out_buf;
  /* @todo rraghave unsigned int* raw = (unsigned int*) input;*/
  int lastOut=0xFFFFFFFF;
  unsigned int* in = input;
  uint64_t hold = (uint64_t)(*in++);
  int bits = 32;
  /* @todo rraghave int count=0;*/
  /* @todo rraghave int prefetched_dictionary=0;*/
#ifdef L2_FETCH
  l2fetch_buffer((void *)in, in_buf_size);
#endif

#ifdef DC_ZERO
  /* VB: Why not use out_buf_size?  */
  /* dczero((unsigned int)out_buf, 4096); */
  dczero((unsigned int)out_buf, *out_buf_size);
#endif

  debug("  offset  p hold             added    raw              next"
	"     codes out      notes\n");
  /* @todo rraghave Remove this
  start:*/
/*  if (((char*)out-(char*)out_buf)>=(4096))
  {
     *q6zip_out_buf_size = (out - (unsigned int*)out_buf) * sizeof(unsigned int) ;
     goto exit_inflate;
  }*/
  //while(1)
  {

#define DEBUG_START debug("%8lu %2d %016llX %08X %016llX %08X %u", \
	  (in - input) * 4, bits, GET_DWORD(), \
	  (unsigned int)(GET_DWORD() >> (bits - 32)), GET_DWORD_SKIPPING(2), \
	  GET_WORD_FROM(in + 1), GET_BITS(2));
/*
    MATCH_6N_2x0_SQ0    389698       000 0
        MATCH_8N_SQ0    658700       001 1
    MATCH_5N_3x0_SQ0    239840      0010 2
            NO_MATCH    730198       011 3
         DICT1_MATCH    468710       100 4
         DICT2_MATCH    308708      0101 5
    MATCH_6N_2x0_SQ1    526647       110 6
        MATCH_8N_SQ1    758299       111 7
    MATCH_4N_4x0_SQ1     46241    001010 10
    MATCH_4N_4x0_SQ0    173634     01101 13
    MATCH_5N_3x0_SQ1    196326     11101 29
    MATCH_6N_2x2_SQ0     84638    111010 58
    MATCH_6N_2x4_SQ0     52167    101010 42
    MATCH_6N_2x2_SQ1     38398   1011010 90
    MATCH_6N_2x4_SQ1     25603   0011010 26
*/
#define MATCH_6N_2x0_SQ0_BITS 3
#define MATCH_8N_SQ0_BITS     3
#define MATCH_5N_3x0_SQ0_BITS 4
#define NO_MATCH_BITS         3
#define DICT1_MATCH_BITS      3
#define DICT2_MATCH_BITS      4
#define MATCH_6N_2x0_SQ1_BITS 3
#define MATCH_8N_SQ1_BITS     3
#define MATCH_4N_4x0_SQ1_BITS 6
#define MATCH_4N_4x0_SQ0_BITS 5
#define MATCH_5N_3x0_SQ1_BITS 5
#define MATCH_6N_2x2_SQ0_BITS 6
#define MATCH_6N_2x4_SQ0_BITS 6
#define MATCH_6N_2x2_SQ1_BITS 7
#define MATCH_6N_2x4_SQ1_BITS 7

     void* jump_table[] =  {
            &&MATCH_6N_2x0_SQ0,  //0
            &&MATCH_8N_SQ0,      //1
            &&MATCH_5N_3x0_SQ0,  //2
            &&NO_MATCH,          //3
            &&DICT1_MATCH,       //4
            &&DICT2_MATCH,       //5
            &&MATCH_6N_2x0_SQ1,  //6
            &&MATCH_8N_SQ1,      //7
            &&MATCH_6N_2x0_SQ0,  //8
            &&MATCH_8N_SQ0,      //9
            &&LEVEL_A,           //10 =0xA==0x1010 
            &&NO_MATCH,          //11
            &&DICT1_MATCH,       //12
            &&LEVEL_B,           //13 0xD==1101 MATCH_6N_2x2_SQ1  = ( 0b1011010, 7, 0xFFFF00FF ,"MATCH_6N_2x2_SQ1"),     #MATCH_6Nx1
            &&MATCH_6N_2x0_SQ1,  //14
            &&MATCH_8N_SQ1,      //15
                   };               
    //asm("hintjr(%0)" : : "r" (&&MATCH_8N_SQ1));

    {
       unsigned int op1 = Q6_R_extractu_RII((unsigned int)hold,OP1_BITS+1,0);
       JUMP_NEXT_DECODE
    }
    MATCH_6N_2x0_SQ0:
    {
       DEBUG_START
          unsigned int op1 = Q6_R_extractu_RII((unsigned int)hold,OP1_BITS+1,8+MATCH_6N_2x0_SQ0_BITS+LB_BITS);
            lastOut=Q6_R_tableidxb_RII(lastOut,(unsigned int)hold,LB_BITS,MATCH_6N_2x0_SQ0_BITS);
       unsigned int masked = Q6_R_extractu_RII((unsigned int)hold,8,MATCH_6N_2x0_SQ0_BITS+LB_BITS);
       *out = Q6_R_insert_RII(*(out + lastOut), masked, 8,0); out++
       SKIP_HOLD_N_BITS_W_CHECK(MATCH_6N_2x0_SQ0_BITS+LB_BITS+8);
       debug("   %08X %u mask byte 3 %08X\n", *(out-1), lastOut, masked);
       //unsigned int op1 = Q6_R_extractu_RII((unsigned int)hold,OP1_BITS+1,0);

       JUMP_NEXT_DECODE
    }
    MATCH_6N_2x0_SQ1:
    {
       DEBUG_START
       unsigned int op1 = Q6_R_extractu_RII((unsigned int)hold,OP1_BITS+1,MATCH_6N_2x0_SQ1_BITS+8);
       unsigned int masked = Q6_R_extractu_RII((unsigned int)hold,8,MATCH_6N_2x0_SQ1_BITS);
       *out = Q6_R_insert_RII(*(out + lastOut), masked, 8,0); out++
       SKIP_HOLD_N_BITS_W_CHECK(MATCH_6N_2x0_SQ1_BITS+8);
       debug("   %08X %u mask byte 3 %08X\n", *(out-1), lastOut, masked);
       //unsigned int op1 = Q6_R_extractu_RII((unsigned int)hold,OP1_BITS+1,0);
       JUMP_NEXT_DECODE
    }
    LEVEL_B:
    {
       //printf("op1=0x%x LEVEL_B hold=0x%llx\n",op1,hold);
       if ((unsigned int)hold&(1<<4)) goto MATCH_5N_3x0_SQ1;
       goto MATCH_4N_4x0_SQ0;
    }
    LEVEL_A:
    {
       //MATCH_6N_2x2_SQ0     84638    11 1010 58
       //MATCH_6N_2x4_SQ0     52167    10 1010 42
       //MATCH_4N_4x0_SQ1     46241    00 1010 10
       //MATCH_6N_2x2_SQ1     38398   101 1010 90
       //MATCH_6N_2x4_SQ1     25603   001 1010 26
       unsigned int temp=(((unsigned int)hold)>>4)&0x3;
       if (temp==3)goto MATCH_6N_2x2_SQ0;
       if (temp==2)goto MATCH_6N_2x4_SQ0;
       if (temp==0)goto MATCH_4N_4x0_SQ1;
       if ((unsigned int)hold&(1<<6)) goto MATCH_6N_2x2_SQ1;
       goto MATCH_6N_2x4_SQ1;
    }    
#if 1
    NO_MATCH:
    {
       /* @todo rraghave Remove this
       static int no_match_seq=0;*/

       DEBUG_START
       SKIP_HOLD_N_BITS_W_CHECK(NO_MATCH_BITS);
       *out++ = GET_WORD();
       hold = ((uint64_t)(*(in++)) << (bits-32))|(hold>>32);
       //Q6_dcfetch_A(in);
       debug("     %08X uncompressed %u\n", *(out-1), bits);
       unsigned int op1 = Q6_R_extractu_RII((unsigned int)hold,OP1_BITS+1,0);
       JUMP_NEXT_DECODE
    }    
 #else
    NO_MATCH:
    {
       DEBUG_START
       unsigned int op1 = Q6_R_extractu_RII((unsigned int)hold,OP1_BITS+1,NO_MATCH_BITS);
       void *jumpPtr=jump_table[op1];
       SKIP_HOLD_N_BITS_W_CHECK(NO_MATCH_BITS);
       *out++ = *raw++;
       //Q6_dcfetch_A(in);
       debug("     %08X uncompressed %u\n", *(out-1), bits);
       goto *jumpPtr;
    }    
 #endif
    MATCH_8N_SQ1:
    {
       DEBUG_START
       unsigned int op1 = Q6_R_extractu_RII((unsigned int)hold,OP1_BITS+1,MATCH_8N_SQ1_BITS);
       SKIP_HOLD_N_BITS_W_CHECK(MATCH_8N_SQ1_BITS);
       void *jumpPtr=jump_table[op1];
       *out = *(out + lastOut); out++;
       goto *jumpPtr;
       debug("    out=0x%x sequential  \n", *(out-1));
    }    
    MATCH_8N_SQ0:
    {
       DEBUG_START
       unsigned int op1 = Q6_R_extractu_RII((unsigned int)hold,OP1_BITS+1,MATCH_8N_SQ0_BITS+LB_BITS);
       lastOut=Q6_R_tableidxb_RII(lastOut,(unsigned int)hold,LB_BITS,MATCH_8N_SQ0_BITS);
       SKIP_HOLD_N_BITS_W_CHECK(MATCH_8N_SQ0_BITS+LB_BITS);
       void *jumpPtr=jump_table[op1];
       *out = *(out + lastOut); out++
       debug("    out=0x%08x lookback \n", *(out-1));
       goto *jumpPtr;
    }    
    DICT1_MATCH:
    {
       DEBUG_START
       unsigned int op1 = Q6_R_extractu_RII((unsigned int)hold,OP1_BITS+1,DICT1_MATCH_BITS+DICT1_BITS);
       void *jumpPtr=jump_table[op1];
       unsigned int entry=Q6_R_extractu_RII((unsigned int)hold,DICT1_BITS,DICT1_MATCH_BITS);
       SKIP_HOLD_N_BITS_W_CHECK(DICT1_MATCH_BITS+DICT1_BITS);
       *out = GET_WORD_FROM(dictionary1 + entry); out++
       debug("   out=0x%08x dictionary1 \n",*(out-1));
       goto *jumpPtr;
       //unsigned int op1 = Q6_R_extractu_RII((unsigned int)hold,OP1_BITS+1,0);
       //JUMP_NEXT_DECODE
    }    
    DICT2_MATCH:
    {
       DEBUG_START
       *out++ = GET_WORD_FROM(dictionary2 + Q6_R_extractu_RII((unsigned int)hold,DICT2_BITS,DICT2_MATCH_BITS));
       SKIP_HOLD_N_BITS_W_CHECK(DICT2_MATCH_BITS+DICT2_BITS);
       debug("   out=0x%08x dictionary2 \n",*(out-1));
       unsigned int op1 = Q6_R_extractu_RII((unsigned int)hold,OP1_BITS+1,0);
       JUMP_NEXT_DECODE
    }    
    MATCH_5N_3x0_SQ0:
    {
       DEBUG_START
            lastOut=Q6_R_tableidxb_RII(lastOut,(unsigned int)hold,LB_BITS,MATCH_5N_3x0_SQ0_BITS);
       unsigned int masked = Q6_R_extractu_RII((unsigned int)hold,12,MATCH_5N_3x0_SQ0_BITS+LB_BITS);
       *out = Q6_R_insert_RII(*(out + lastOut), masked, 12,0); out++
       SKIP_HOLD_N_BITS_W_CHECK(MATCH_5N_3x0_SQ0_BITS+LB_BITS+12);
       debug("   %08X %u mask 12 bit 20 %012X\n", *(out-1), lastOut, masked);
       unsigned int op1 = Q6_R_extractu_RII((unsigned int)hold,OP1_BITS+1,0);
       JUMP_NEXT_DECODE
    }    
    MATCH_5N_3x0_SQ1:
    {
       DEBUG_START
       unsigned int masked = Q6_R_extractu_RII((unsigned int)hold,12,MATCH_5N_3x0_SQ1_BITS);
       *out = Q6_R_insert_RII(*(out + lastOut), masked, 12,0); out++
       SKIP_HOLD_N_BITS_W_CHECK(MATCH_5N_3x0_SQ1_BITS+12);
       debug("   %08X %u mask 12 bit 20 %012X\n", *(out-1), lastOut, masked);
       unsigned int op1 = Q6_R_extractu_RII((unsigned int)hold,OP1_BITS+1,0);
       JUMP_NEXT_DECODE
    }    
    MATCH_4N_4x0_SQ0:
    {
       DEBUG_START
            lastOut=Q6_R_tableidxb_RII(lastOut,(unsigned int)hold,LB_BITS,MATCH_4N_4x0_SQ0_BITS);
       unsigned int masked = Q6_R_extractu_RII((unsigned int)hold,16,MATCH_4N_4x0_SQ0_BITS+LB_BITS);
       *out = Q6_R_insert_RII(*(out + lastOut), masked, 16,0); out++;
       SKIP_HOLD_N_BITS_W_CHECK(MATCH_4N_4x0_SQ0_BITS+LB_BITS+16);
       debug("   %08X %u mask 16 bit 16 %2X\n", *(out-1), lastOut, masked);
       unsigned int op1 = Q6_R_extractu_RII((unsigned int)hold,OP1_BITS+1,0);
       JUMP_NEXT_DECODE
    }    
    MATCH_4N_4x0_SQ1:
    {
       DEBUG_START
       unsigned int masked = Q6_R_extractu_RII((unsigned int)hold,16,MATCH_4N_4x0_SQ1_BITS);
       *out = Q6_R_insert_RII(*(out + lastOut), masked, 16,0); out++
       SKIP_HOLD_N_BITS_W_CHECK(MATCH_4N_4x0_SQ1_BITS+16);
       debug("   %08X %u mask 16 bit 16 %2X\n", *(out-1), lastOut, masked);
       unsigned int op1 = Q6_R_extractu_RII((unsigned int)hold,OP1_BITS+1,0);
       JUMP_NEXT_DECODE
    }    
    MATCH_6N_2x2_SQ0:
    {
       DEBUG_START
            lastOut=Q6_R_tableidxb_RII(lastOut,(unsigned int)hold,LB_BITS,MATCH_6N_2x2_SQ0_BITS);
       unsigned int masked = Q6_R_extractu_RII((unsigned int)hold,8,MATCH_6N_2x2_SQ0_BITS+LB_BITS);
       *out = Q6_R_insert_RII(*(out + lastOut), masked,8,8); out++;
       SKIP_HOLD_N_BITS_W_CHECK(MATCH_6N_2x2_SQ0_BITS+LB_BITS+8);
       debug("   %08X %u mask byte 2 %2X\n", *(out-1), lastOut, masked);
       unsigned int op1 = Q6_R_extractu_RII((unsigned int)hold,OP1_BITS+1,0);
       JUMP_NEXT_DECODE
    }    
    MATCH_6N_2x2_SQ1:
    {
       DEBUG_START
       unsigned int masked = Q6_R_extractu_RII((unsigned int)hold,8,MATCH_6N_2x2_SQ1_BITS);
       *out = Q6_R_insert_RII(*(out + lastOut), masked,8,8); out++;
       SKIP_HOLD_N_BITS_W_CHECK(MATCH_6N_2x2_SQ1_BITS+8);
       debug("   %08X %u mask byte 2 %2X\n", *(out-1), lastOut, masked);
       unsigned int op1 = Q6_R_extractu_RII((unsigned int)hold,OP1_BITS+1,0);
       JUMP_NEXT_DECODE
    }
    MATCH_6N_2x4_SQ0:
    {
       DEBUG_START
            lastOut=Q6_R_tableidxb_RII(lastOut,(unsigned int)hold,LB_BITS,MATCH_6N_2x4_SQ0_BITS);
       unsigned int masked = Q6_R_extractu_RII((unsigned int)hold,8,MATCH_6N_2x4_SQ0_BITS+LB_BITS);
       *out = Q6_R_insert_RII(*(out + lastOut), masked,8,16); out++;
       SKIP_HOLD_N_BITS_W_CHECK(MATCH_6N_2x4_SQ0_BITS+LB_BITS+8);
       debug("   %08X %u mask 16 bit 16 %2X\n", *(out-1), lastOut, masked);
       unsigned int op1 = Q6_R_extractu_RII((unsigned int)hold,OP1_BITS+1,0);
       JUMP_NEXT_DECODE
    }    
    MATCH_6N_2x4_SQ1:
    {
		/* printf("out: %u, out_buf: %u, out_buf_size: %d\n", out, out_buf, *out_buf_size); */
		/* VB: logic was hard-coded  to stop at 4096. Modified to use page size */
       DEBUG_START
       /* if (((char*)out-(char*)out_buf)>=(4096)) */
       if (((char*)out-(char*)out_buf)>=(*out_buf_size))
       {
		   /* overwrite the value passed in as page size with the actual size of the decompressed data */
          *q6zip_out_buf_size = (out - (unsigned int*)out_buf) * sizeof(unsigned int) ;

		  /* bail out when a page's worth of decompressed data has been written */
          return 0;
       }

       unsigned int masked = Q6_R_extractu_RII((unsigned int)hold,8,MATCH_6N_2x4_SQ1_BITS);
       *out = Q6_R_insert_RII(*(out + lastOut), masked, 8,16); out++;
       SKIP_HOLD_N_BITS_W_CHECK(MATCH_6N_2x4_SQ1_BITS+8);
       debug("   %08X %u mask 16 bit 16 %2X\n", *(out-1), lastOut, masked);
       unsigned int op1 = Q6_R_extractu_RII((unsigned int)hold,OP1_BITS+1,0);
       JUMP_NEXT_DECODE
    }    
  }  
}
