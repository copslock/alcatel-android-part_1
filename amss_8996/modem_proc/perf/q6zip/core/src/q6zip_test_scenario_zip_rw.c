#include "err.h"
#include "msg_diag_service.h"
#if !defined( Q6ZIP_ENABLE_ASSERTS )
#define Q6ZIP_ENABLE_ASSERTS
#endif
#include "q6zip_assert.h"
#include "q6zip_test_scenario_zip_rw.h"
#include "q6zip_test_vectors_rw.h"
#include "q6zip_waiter.h"

typedef PACK( struct )
{
    uint8 control;

    /** @brief The IPA queue to use for requests. 0 = low, 1 = high */
    uint8 queue_select;

    /** @brief Index (one-based) of the vector to run. 0 = all vectors */
    uint8 vector_num;

} q6zip_test_scenario_zip_rw_conf_t;

static uint8 queue_select;

/** @brief Destination/compressed page
    @warning *MUST* be aligned on cache line */
static uint8 zipped_page[ 4352 ] __attribute__(( aligned( 64 ) ));

/** @brief Waiter to wait for completion of compression/decompression request */
static q6zip_waiter_t waiter;

static void initialize (q6zip_test_scenario_t * scenario)
{
    q6zip_waiter_init( &waiter );
}

static void configure (
    q6zip_test_scenario_t * scenario,
    q6zip_test_copyd_knobs_t const * knobs,
    uint8 const * msg,
    uint16 len
)
{
    q6zip_test_scenario_zip_rw_conf_t const * conf = (q6zip_test_scenario_zip_rw_conf_t const *)msg;
    queue_select = conf->queue_select;
}

static void start (q6zip_test_scenario_t * scenario)
{
    q6zip_iface_t const * q6zip = q6zip_test_get_implementation();
    q6zip_request_t * request;
    q6zip_error_t error;
    unsigned int i;
    boolean bit_exact_match = FALSE;

    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "ENTER: RW zip tests. queue_select=%u", queue_select, 0, 0 );

    for ( i = 0; i < Q6ZIP_TEST_VECTORS_RW_MAX; i++ )
    {
        /* Paint destination buffer with a pattern (number of the vector) */
        memset( zipped_page, i, sizeof( zipped_page ) );

        request = q6zip->get_request();
        Q6ZIP_ASSERT( request );

        request->src.ptr  = (void *)RW_VECTORS_INDEX[ i ].unzipped;
        request->src.len  = RW_VECTORS_INDEX[ i ].unzipped_size;
        request->dst.ptr  = zipped_page;
        request->dst.len  = sizeof( zipped_page );
        request->algo     = Q6ZIP_ALGO_ZIP_RW;
        request->priority = queue_select ? Q6ZIP_FIXED_PRIORITY_HIGH : Q6ZIP_FIXED_PRIORITY_LOW;

        error = q6zip->submit( &request );
        Q6ZIP_ASSERT( !error );

        q6zip_waiter_wait( &waiter );

        if ( memcmp( RW_VECTORS_INDEX[ i ].zipped, zipped_page, RW_VECTORS_INDEX[ i ].zipped_size ) == 0 )
        {
            bit_exact_match = TRUE;
        }
        else
        {
            bit_exact_match = FALSE;
            break;
        }
    }

    if ( bit_exact_match )
    {
        MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "PASS: %u RW zip vectors", i, 0, 0 );
    }
    else
    {
        ERR_FATAL( "FAIL: RW zip of vector %u dst_ptr=0x%x", i, zipped_page, 0 );
    }

    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "LEAVE: RW zip tests", 0, 0, 0 );
}

static void response (
    q6zip_test_scenario_t * scenario,
    q6zip_request_t const * request,
    q6zip_response_t const * response
)
{
    Q6ZIP_ASSERT( !response->error );
    q6zip_waiter_wake( &waiter );
}

static void stop (q6zip_test_scenario_t * scenario)
{
}

q6zip_test_scenario_t q6zip_test_scenario_zip_rw =
{
    .ops = 
    {
        initialize,
        configure,
        start,
        response,
        stop
    }
};
