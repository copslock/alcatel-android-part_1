#include "q6zip_context.h"
#include "q6zip_params.h"
#include "assert.h"

#define Q6ZIP_CONTEXT_MAX_ITEMS Q6ZIP_PARAM_CONTEXT_MAX_ITEMS

q6zip_context_module_t q6zip_context_module;

/** @brief Pool of Q6ZIP contexts */
static q6zip_context_t pool[ Q6ZIP_CONTEXT_MAX_ITEMS ];

void q6zip_context_module_init (void)
{
    uint32 i;

    if ( q6zip_context_module.initialized )
    {
        return;
    }

    q_init( &q6zip_context_module.pool.free );

    for ( i = 0; i < Q6ZIP_ARRAY_SIZE( pool ); i++ )
    {
        pool[ i ].magic = (unsigned char *)&pool[ i ];
        pool[ i ].magic += 1;
        q_put( &q6zip_context_module.pool.free, ( q_link_type * )&pool[ i ] );
    }

    q6zip_context_module.initialized = TRUE;
}

void q6zip_context_module_fini (void)
{
    if ( !q6zip_context_module.initialized )
    {
        return;
    }

    ASSERT( q_cnt( &q6zip_context_module.pool.free ) == Q6ZIP_CONTEXT_MAX_ITEMS );
    q6zip_context_module.initialized = FALSE;
}
