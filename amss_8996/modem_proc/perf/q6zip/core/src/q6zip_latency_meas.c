#include "customer.h"
#include "fs_lib.h"
#include "msg_diag_service.h"
#include "q6zip_latency_meas.h"
#include "qurt.h"

/** @brief IPA HW version register. Obtained from pre-processed output of
           ipa_hal.c:ipa_hal_get_comp_hw_version */
#define IPA_COMP_HW_VERSION ((0xe4100000 + 0x00040000) + 0x00000030)

/** @brief Data structure for this module. Internal usage */
static q6zip_latency_meas_module_t q6zip_latency_meas_module;

/** @brief Pointer to module for external usage */
q6zip_latency_meas_module_t const * Q6ZIP_LATENCY_MEAS_MODULE = &q6zip_latency_meas_module;

static void * q6zip_latency_meas_worker_main (void *argument)
{
    while ( TRUE )
    {
        qurt_timer_sleep( q6zip_latency_meas_module.conf.interval * 1000 );
        q6zip_latency_meas_record( FALSE );
    }

    return NULL;
}

void q6zip_latency_meas_conf_init (void)
{
    int bytes_read;

    bytes_read = efs_get(
        "/nv/item_files/modem/q6zip/q6zip_latency_meas",
        (void *)&q6zip_latency_meas_module.conf.is_enabled,
        1U );
    if ( bytes_read != 1U )
    {
        q6zip_latency_meas_module.conf.is_enabled = FALSE;
    }

    bytes_read = efs_get(
        "/nv/item_files/modem/q6zip/q6zip_latency_meas_interval",
        (void *)&q6zip_latency_meas_module.conf.interval,
        4U );

    if ( ( bytes_read != 4U ) ||
         ( q6zip_latency_meas_module.conf.interval == 0 ) )
    {
        /* File is either absent or interval is incorrect. Default it */
        q6zip_latency_meas_module.conf.interval = 1000;
    }

    bytes_read = efs_get(
        "/nv/item_files/modem/q6zip/q6zip_latency_meas_order",
        (void *)&q6zip_latency_meas_module.conf.order,
        4U );
    if ( ( bytes_read != 4U ) ||
         ( q6zip_latency_meas_module.conf.order > 4 ) )
    {
        /* File absent or order out-of-range. Default it */
        q6zip_latency_meas_module.conf.order = 1;
    }
}

void q6zip_latency_meas_append (
    uint64  tick, 
    uint16  pcnoc_read_latency_ticks, 
    boolean is_dlpager
)
{
    uint32 idx = atomic_inc_return( &q6zip_latency_meas_module.history.idx ) & Q6ZIP_LATENCY_MEAS_MASK;

    q6zip_latency_meas_module.history.samples[ idx ].tick = tick;
    q6zip_latency_meas_module.history.samples[ idx ].pcnoc_read_latency_ticks = pcnoc_read_latency_ticks;
    q6zip_latency_meas_module.history.samples[ idx ].is_dlpager = is_dlpager;
}

void q6zip_latency_meas_record (boolean is_dlpager)
{
    uint64 time_start;
    uint32 i;
    uint16 time_elapsed, time_elapsed_avg;
    volatile uint32 * ipa_register;

    ipa_register = (uint32 *)IPA_COMP_HW_VERSION;

    time_start = qurt_sysclock_get_hw_ticks();

    for ( i = 0; i < ( 1 << q6zip_latency_meas_module.conf.order ); i++ )
    {
        q6zip_latency_meas_module.pcnoc_register_value = *ipa_register;
    }

    time_elapsed = qurt_sysclock_get_hw_ticks() - time_start;

    time_elapsed_avg = time_elapsed >> q6zip_latency_meas_module.conf.order;

    q6zip_latency_meas_append( time_start, time_elapsed_avg, is_dlpager );
}

void q6zip_latency_meas_rcinit (void)
{
    /*memset( &q6zip_latency_meas_module, 0, sizeof( q6zip_latency_meas_module ) );*/

    /* Initialize configuration */
    q6zip_latency_meas_conf_init();

    atomic_init( &q6zip_latency_meas_module.history.idx, 0 );
}

void q6zip_latency_meas_run (void)
{
    if ( !q6zip_latency_meas_module.conf.is_enabled )
    {
        /* Latency measurement is disabled, bail! */
        return;
    }

    (void) q6zip_latency_meas_worker_main( NULL );
}
