#===============================================================================
#                    Copyright 2009 QUALCOMM Technologies Incorporated.
#                           All Rights Reserved.
#                         QUALCOMM Proprietary/GTDR
#-------------------------------------------------------------------------------
#
#  $Header: //components/rel/dspbuild.adsp/2.8.2/utils.py#3 $
#  $DateTime: 2016/02/02 15:09:42 $
#  $Change: 9831797 $
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
#   when       who            what, where, why
# --------   ---        ---------------------------------------------------------
#   06/12/13   corinc    Script to call the QC-SCons build System for Badger    

#===============================================================================
import os
import re
import sys
import datetime
import subprocess
import socket
import shutil
import platform
import fileinput
from time import sleep
from datetime import timedelta


def sign_a_module(hostname, portnumber, module, outfolder_on_server, timestamp):
    '''This function interacts with server script running on a host and uses  Host's port to
    connect to that machine. This function will also send and receive data, and that data is used by 
    the server script to sign the module. After successful signing, server script sends 
    the path of file to be copied over
    '''
    s = socket.socket()         # Create a socket object
    host = hostname #host to be connected for signing
    port = int(portnumber)               # Reserve a port for your service.
    print host
    print port
    try:
        s.connect((host, port))
    except socket.error, v:
        errorcode=v[0]
        return "DO_STATIC_HASH"
    print module # This module name is being 
    s.send(module)
    sleep(5)
    s.send(timestamp)
    sleep(5)
    s.send(outfolder_on_server)
    print s.recv(1024)
    s.settimeout(600)
    try:
        signed_message = s.recv(1024)
    except socket.timeout:
        return "DO_STATIC_HASH"
    print signed_message
    output_file = s.recv(1024)
    print output_file
    if not output_file:
        print "asking to static hash"
        return "DO_STATIC_HASH"
    s.close
    return output_file

def get_all_files_with_path(dirname):
    ''' lists out all the files in a given directory path
    '''
    files =[]
    for root, dirs, files in os.walk(dirname):
        for file in files:
            filename = os.path.join(root, file)
            files.append(filename)
    return files
def date_based_folder(): #Time stamp based string generation
    '''
    This function generates time stamp based string which will be used in copying the
    unsigned file to a dropbox location on signing server 
    '''
    now = datetime.datetime.now()
    date_str = str(now.date())
    date_str = re.sub('-','_',date_str)
    time_str =str(now.time())
    time_str = re.sub(':','_',time_str)
    date_time_str = date_str + "_"+ time_str
    return date_time_str[0:19]

def sign_all_modules_from_folder(input, output, host, port):
    '''
    This function will take care of copying files from build server using copy_files_to_signing_Server function
    and after that, it gets all the unsigned files and get them signed one by one using sign_a_module function call
    the output of this would be a flag "YES/NO" and array of signed files
    '''
    input_folder = os.path.abspath(input)
    print input_folder
    output_folder_client = os.path.abspath(output)
    if not os.path.isdir(output_folder_client):
        os.makedirs(output_folder_client)
    time_stamp = date_based_folder()
    Copy_folder_linux = os.path.join("/prj/vocoder/appdsp5/users/achintal/test/input", time_stamp)
    Copy_folder_windows = os.path.join("\\\\vanquish\\vocoder_appdsp5\\users\\achintal\\test\\input",time_stamp)
    if platform.system() == 'Linux':
        print "This is linux machine"
        os.makedirs(Copy_folder_linux)
        Copy_folder = Copy_folder_linux
        output_folder_in_server_native = os.path.join(os.path.dirname(os.path.dirname(Copy_folder)),"output")
    elif platform.system() ==  'Windows':
        print "This is windows machine"
        os.makedirs(Copy_folder_windows)
        Copy_folder = Copy_folder_windows
        output_folder_in_server_native = os.path.join(os.path.dirname(os.path.dirname(Copy_folder)),"output")
    output_folder_in_server_windows = ("\\\\vanquish\\vocoder_appdsp5\\users\\achintal\\test\\output")
    src_files = os.listdir(input_folder)
    for file_name in src_files:
        full_file_name = os.path.join(input_folder, file_name)
        if not full_file_name.endswith("txt"):
            if (os.path.isfile(full_file_name)):
                shutil.copy(full_file_name, Copy_folder)
    all_modules = os.listdir(Copy_folder)
    number_of_unsigned_files = len(all_modules)
    signed_files =[]
    for module in all_modules:
        module_full_path = os.path.join(Copy_folder_windows, module)
        out_filename_on_server = sign_a_module(host, port, module_full_path, output_folder_in_server_windows, time_stamp)
        if out_filename_on_server != "DO_STATIC_HASH":
            outfile_name = os.path.join(output_folder_client, out_filename_on_server) #output_folder + os.path.basename(out_file)
            outfile_on_server = os.path.join(output_folder_in_server_native, time_stamp, out_filename_on_server)
            shutil.copy2(outfile_on_server, outfile_name)
            print outfile_name + " is signed with dongle"
            signed_files.append(outfile_name)
    number_of_signed_files = len(signed_files)
    if number_of_unsigned_files == number_of_signed_files:
        print "All the files in "+ input +" folder are signed by dongle"
        print signed_files
        return 1
    else:
        print "Some or more files in " + input + " folder are not signed using dongle"
        print signed_files
        return 0

def sign_check(signhost,signport):
        cwd_dir = os.getcwd()  
        dynamic_mods = ''.join([cwd_dir + "/build/dynamic_modules"])
        output_16_folder = './build/dynamic_signed/'
        #output_bin_folder = ''.join([cwd_dir + "/build/dynamic_signed"])
        result = 0
        host = signhost
        port = signport
        print '\n\nCRM Build Signing Attempted'
        print '\n****************'
        print '\nSigning Dynamic SOs'
        print '\n****************'
        if platform.system() == 'Windows':
              print "This is windows machine"
              input_folder = 'build\dynamic_modules'
              output_folder = 'build\dynamic_signed\shared_obj'
              print input_folder
              print output_folder
        elif platform.system() == 'Linux':
              print "This is linux machine"
              input_folder = './build/dynamic_modules'
              output_folder = './build/dynamic_signed/shared_obj'
              print input_folder
              print output_folder
        result = sign_all_modules_from_folder(input_folder,output_folder,host,port)              
        print '\n****************'
        print '\n Signing Dynamic SOs DONE'
        print '\n****************'
        print '\n Sign Check Exited'
        return result

def Not_in_adspsobin():
    cwd_dir = os.getcwd()
    check_txt = ''.join([cwd_dir + "/build/dynamic_modules/map_SHARED_LIBS_AAAAAAAAQ.txt"])
    file_list = []

    for line in fileinput.input([check_txt]):
        print line
        print '\n'
        if re.search('/CV/',line) is not None:
            list=re.split('/',line)
            element = list[len(list)-1]
            file_element = re.split('/',element)
            print file_element
            file_list.append(file_element[0].rstrip('\n'))
        if re.search('/camera/',line) is not None:
            list=re.split('/',line)
            element = list[len(list)-1]
            file_element = re.split('/',element)
            print file_element
            file_list.append(file_element[0].rstrip('\n'))
        if re.search('/video_processing/',line) is not None:
            list=re.split('/',line)
            element = list[len(list)-1]
            file_element = re.split('/',element)
            print file_element
            file_list.append(file_element[0].rstrip('\n'))
    for i in range(0,len(file_list)):
        removal_file = ''.join([cwd_dir + "/build/dynamic_modules/" + file_list[i]])
        os.remove(removal_file)
            
def avs_check():
    cwd_dir = os.getcwd()
    avs_check_txt = ''.join([cwd_dir + "/build/dynamic_modules/map_avs_shared_libs.txt"])
    file_list = []
    for line in fileinput.input([avs_check_txt]):
        if re.search('/avs/',line) is not None:        
           print line
           print '\n'
           list=re.split('/',line)
           element = list[len(list)-1]
           file_element = re.split('/',element)
           print file_element
           file_list.append(file_element[0].rstrip('\n'))
	if not os.path.exists("build/dynamic_modules_avs"):
		os.mkdir("build/dynamic_modules_avs")
    for i in range(0,len(file_list)):
        cp_file = ''.join([cwd_dir + "/build/dynamic_modules/" + file_list[i]])
        dst_dir = ''.join([cwd_dir + "/build/dynamic_modules_avs/" + file_list[i]])
        print cp_file
        print dst_dir
        shutil.copy(cp_file,dst_dir)

def nonavs_check():
    cwd_dir = os.getcwd()
    nonavs_check_txt = ''.join([cwd_dir + "/build/dynamic_modules/map_SHARED_LIBS_AAAAAAAAQ.txt"])
    file_list = []
    for line in fileinput.input([nonavs_check_txt]):
        if re.search('/avs/',line) is None and re.search('../',line) is not None:        
           print line
           print '\n'
           list=re.split('/',line)
           element = list[len(list)-1]
           file_element = re.split('/',element)
           print file_element
           file_list.append(file_element[0].rstrip('\n'))
	if not os.path.exists("build/dynamic_modules_nonavs"):
		os.mkdir("build/dynamic_modules_nonavs")
    for i in range(0,len(file_list)):
        cp_file = ''.join([cwd_dir + "/build/dynamic_modules/" + file_list[i]])
        dst_dir = ''.join([cwd_dir + "/build/dynamic_modules_nonavs/" + file_list[i]])
        print cp_file
        print dst_dir
        if os.path.exists(cp_file):
            shutil.copy(cp_file,dst_dir)
                
            
def static_hash_prepare(build_flags): 
   cwd_dir = os.getcwd()
   print "Preparing the image for Static Hash\n"
   if 'GEN_SHARED_LIBS' in build_flags:
          avs_check()
   nonavs_check()
   dynamic_shared = ''.join([cwd_dir + "/build/dynamic_signed/shared_obj"])
   if platform.system() == 'Windows':
       #os.system("mkdir build\dynamic_modules_hash")
       if not os.path.exists(dynamic_shared):
           os.system("mkdir build\dynamic_signed\shared_obj")
       #os.system("copy build\dynamic_modules build\dynamic_modules_hash")
       os.system("copy build\dynamic_modules build\dynamic_signed\shared_obj")
   elif platform.system() == 'Linux':
       #os.system("mkdir build/dynamic_modules_hash")
       shared_object_folder = ''.join([cwd_dir + "/build/dynamic_signed/shared_obj"])
       shared_object_folder_client = os.path.abspath(shared_object_folder)
       if not os.path.exists(shared_object_folder_client):
           os.makedirs(shared_object_folder_client)
       #os.system("cp -r build/dynamic_modules/* build/dynamic_modules_hash")
       os.system("cp -r build/dynamic_modules/* build/dynamic_signed/shared_obj")
         

def adspsobin_create():
   cwd_dir = os.getcwd()      
   os.system('chmod +x ./build/ext4fs_tools/ubuntu/make_ext4fs')
   os.system('chmod +x ./build/ext4fs_tools/ubuntu/simg2img')
   output_16_folder = './build/dynamic_signed/'
   output_bin_folder = ''.join([cwd_dir + "/build/dynamic_signed"])
   output_bin_folder_client = os.path.abspath(output_bin_folder)
   if not os.path.exists(output_bin_folder_client):
      os.makedirs(output_bin_folder_client)
   print '\nCreating the binary out of the Dynamic Shared Objects'
   if platform.system() == 'Linux':
      proc = subprocess.Popen("./build/ext4fs_tools/ubuntu/make_ext4fs -s -T -1 -S ./build/ext4fs_tools/ubuntu/file_contexts -L dsp -l 16777216 -a dsp "+output_16_folder+"/adspso_sparse.bin ./build/dynamic_signed/shared_obj", shell=True)
      (out, err) = proc.communicate()
      if err:
         print err
      proc = subprocess.Popen("./build/ext4fs_tools/ubuntu/simg2img "+output_16_folder+"/adspso_sparse.bin "+output_16_folder+"/adspso.bin", shell=True)
      (out, err) = proc.communicate()
      if err:
         print err


def oem_root_shared_libs_copy():
   print '\nCopying shared libraries from OEM_ROOT to adsp_proc/build/dynamic_signed/shared_obj'
   if platform.system() == 'Linux':
      if os.environ.get('OEM_ROOT')!=None:
        proc = subprocess.Popen("cp -r "+os.environ.get('OEM_ROOT')+"build/dynamic_modules_hash/* ./build/dynamic_signed/shared_obj", shell=True)
        (out, err) = proc.communicate()
        if err:
            print err
   else: 
      print platform.system()
      if os.environ.get('OEM_ROOT')!=None:
        proc = subprocess.Popen("xcopy "+os.environ.get('OEM_ROOT')+"\\build\\dynamic_modules_hash .\\build\\dynamic_signed\\shared_obj /Y", shell=True)
        (out, err) = proc.communicate()
        if err:
             print err

def create_timestamp(env):
   print "==================Adding Domain Name to TimeStamp=================="
   domain=socket.getfqdn()
   Dname = domain.partition('.')[2]
   if Dname:
      DOMAIN_NAME=Dname
   else:
      DOMAIN_NAME="NODOMAIN"
   print DOMAIN_NAME
   
   print "==================Creating timestamp and q6_build_version header files=================="
   print env['BUILD_VERSION']
   str_build_version = "#define QDSP6_BUILD_VERSION " + env.subst('$BUILD_VERSION') 
   file = open('./q6_build_version.h', 'w')
   file.write(str_build_version)
   file.close()
   
   timestamp_dir = os.getcwd()
   print "\n\n Time Stamp working directory is %s" % timestamp_dir

   current_time = datetime.datetime.now()
   timestamp_value = current_time.strftime("Q6_BUILD_TS_%a_%b_%d_%H:%M:%S_PST_%Y")
   if os.path.exists('./qcom_timestamp.h'):
      print "qcom_timestamp header file already generated from CRM build, check the build version from existing header file"
      ReadFile = open('./qcom_timestamp.h', 'r')
      for line in ReadFile:
         match = re.split('QCOM time:', line)[1]
         if match: 
            os.environ['OS_ENV_QCOM_TIME_STAMP'] = match[:-2]
      ReadFile.close()  
   else:
      QTime= "QCOM time:" + timestamp_value + "_" + env.subst('$BUILD_VERSION') 
      os.environ['OS_ENV_QCOM_TIME_STAMP'] = timestamp_value + "_" + env.subst('$BUILD_VERSION')
      str_timestamp_qcom = "const char qcom_timestampstring[] = \"" + QTime + "\";"
      file = open('./qcom_timestamp.h', 'w')
      file.write(str_timestamp_qcom)
      file.close()
   
   target_name =os.environ.get('CHIPSET')
   EnggTime= "ENGG time:" + timestamp_value + "_" + target_name + "_" + DOMAIN_NAME
   os.environ['OS_ENV_ENGG_TIME_STAMP'] = timestamp_value
   str_timestamp_engg = "const char engg_timestampstring[] = \"" + EnggTime + "\";"   
   filepath = './engg_timestamp.h'
   if os.path.exists(filepath):
      os.system('chmod 777 '+filepath+'')
      os.remove(filepath)
   file = open(filepath, 'w')
   file.write(str_timestamp_engg)
   file.close()
   
#   current_time = date | sed 's/ /_/g'
#   file.write(current_time)

def init_chipset(env):
  
  #print env.Dump()
  print "current PLATFORM is " + env['PLATFORM']
  chipset=env.subst('$CHIPSET')
  print chipset
  userargs = os.environ.get('BUILD_USERARGS',None)
  #userargs=env.subst('$BUILD_USERARGS')
  if userargs:
      env.Replace(SIGN_HOST = userargs[0:13])
      env.Replace(SIGN_PORT = userargs[14:19])   
  else:
      env.Replace(SIGN_HOST = "IT.IS.THEHASH")
      env.Replace(SIGN_PORT = "00000")   
  print userargs
  build_flavor=env.subst('$BUILD_FLAVOR')
  print build_flavor
  build_act=env.subst('$BUILD_ACT')
  print build_act
  build_flags=env.subst('$BUILD_FLAGS')
  print "Printing the build Flags information \n"
  print build_flags
  build_verbose=env.subst('$BUILD_VERBOSE')
  print build_verbose
  build_numcore=env.subst('$BUILD_NUMCORE')
  print build_numcore
  build_component=env.subst('$BUILD_COMPONENT')
  print build_component
  build_filter=env.subst('$BUILD_FILTER')
  print build_filter
  build_sconsargs=env.subst('$BUILD_SCONSARGS')
  print build_sconsargs
 
  

  if env['PLATFORM'] == 'posix':
     env.Replace(SCONS_CMD_SCRIPT = "python build.py")
  else:
     env.Replace(SCONS_CMD_SCRIPT = "python build.py")

  if build_flavor=='spd':
     if chipset=='msm8x26':
        env.Replace(IMAGE_ALIAS = "msm8974_SPD")
     elif chipset=='msm8962':
        env.Replace(IMAGE_ALIAS = "apq8084_SPD")
     else:   
        env.Replace(IMAGE_ALIAS = "${CHIPSET}_SPD")
  elif build_flavor=='mpd':
     if chipset=='msm8x26':
        env.Replace(IMAGE_ALIAS = "msm8974_MPD")
     elif chipset=='msm8962':
        env.Replace(IMAGE_ALIAS = "apq8084_MPD")    
     else:
        env.Replace(IMAGE_ALIAS = "${CHIPSET}_MPD")   
  else:
     print "Unknown BUILD_FLAVOR!!!"
     sys.exit(0)

  env.Replace(SCONS_CMD = "${SCONS_CMD_SCRIPT} ${IMAGE_ALIAS} BUILD_ID=AAAAAAAA BUILD_VER=1234")
  if build_component:
     build_component = build_component.replace(',', ' ')
     env.Replace(IMAGE_ALIAS = build_component)
  if build_filter:
     env.Append(SCONS_CMD = " --filter="+build_filter)
  
  if build_numcore:
     env.Replace(SCONS_CMD_NUMCORE = "cd build/ms && ${SCONS_CMD} -j "+build_numcore)
  else:
     env.Replace(SCONS_CMD_NUMCORE = "cd build/ms && ${SCONS_CMD} -j 8")

  if build_verbose:
     env.Replace(SCONS_FLAVOR_CMD = "${SCONS_CMD_NUMCORE} --verbose="+build_verbose)
  else:
     env.Replace(SCONS_FLAVOR_CMD = "${SCONS_CMD_NUMCORE} --verbose=1")
     
  if build_sconsargs:
     opts_sconsargs = build_sconsargs.replace(',', ' ')
     env.Append(SCONS_FLAVOR_CMD = " "+opts_sconsargs)
  if os.path.exists('./obj'):
      splitelf_dir = './obj'
      shutil.rmtree(splitelf_dir)
  if build_act=='clean':
     env.Replace(SCONS_FLAVOR_ACT_CMD = "${SCONS_FLAVOR_CMD} -c")
     env.Replace(CLEAN_CMD = "clean")
     print "clean engg_timestamp"
     engg_timestamp_file = './engg_timestamp.h'
     os.remove(engg_timestamp_file)
     print "clean split images"
  elif build_act=='SIM':
     print "for build_act is SIM, pass SIM=1 to SCons build cmd"
     env.Replace(SCONS_FLAVOR_ACT_CMD = "${SCONS_FLAVOR_CMD} SIM=1")

     print "generating osam cfg file"
     #str_osam_path = "../core/kernel/qurt/install_again/ADSPv5MP/debugger/lnx64/qurt_model.so"
     #import pdb; pdb.set_trace() 
     if build_flags != 'CHECKSIMBOOT':
        if env.get('PLATFORM') in ["Windows_NT","windows","win32","cygwin"] :
           #This is a hack as pw works on windows and testing is done on linux
           str_osam_path = "../core/kernel/qurt/install_again/ADSPv5MP/debugger/lnx64/qurt_model.so"       
        else:
           print "sds"
           str_osam_path = "../core/kernel/qurt/install_again/ADSPv5MP/debugger/lnx64/qurt_model.so"
     else:
        if env.get('PLATFORM') in ["Windows_NT","windows","win32","cygwin"] :
           str_osam_path = "..\core\kernel\qurt\install_again\ADSPv5MP\debugger\cygwin\qurt_model.dll"       
        else:
           str_osam_path = "../core/kernel/qurt/install_again/ADSPv5MP/debugger/lnx64/qurt_model.so"
     print str_osam_path      
     #import pdb; pdb.set_trace()       
     dir_obj_ReleaseG = './obj/qdsp6v5_ReleaseG/'
     if not os.path.exists(dir_obj_ReleaseG):
        os.makedirs(dir_obj_ReleaseG)
     file = open(dir_obj_ReleaseG + 'osam.cfg', 'w')
     file.write(str_osam_path)
     file.close()
  elif build_act=='klocwork':
     env.Replace(SCONS_FLAVOR_ACT_CMD = "${SCONS_FLAVOR_CMD}")
  else:
     env.Replace(SCONS_FLAVOR_ACT_CMD = "${SCONS_FLAVOR_CMD}")
  
  # if build_flags:
     # env.Append(SCONS_FLAVOR_ACT_CMD = " USES_FLAGS="+build_flags)
       
def execute_buildcmd(env):
   print "==================delete the elfs from previous build if exist=================="
   host1 = env.subst('$SIGN_HOST')
   port1 = env.subst('$SIGN_PORT')
   print "############################\n"
   print host1
   print port1
   print "############################\n"
   port1 = int(port1)
   dspelf_file = './dsp.elf'
   if os.path.exists(dspelf_file):
      os.remove(dspelf_file)
   adsp_link_dir = './build/bsp/adsp_link/build/AAAAAAAA/'
   if os.path.exists(adsp_link_dir):
      adsp_link_files = os.listdir(adsp_link_dir)
      print adsp_link_files
      os.chdir(adsp_link_dir)
      for file in adsp_link_files:
         os.remove(file)
      os.chdir('../../../../..')

   print "==================execute the build startup command=================="
   cwd_dir = os.getcwd()
   print "\n\nBuild working directory is %s" % cwd_dir
   chipset = env.subst('$CHIPSET')
   # Check if this is Internal or External Package
   # If not external only then try to run shared libs
   #output_16_folder = './build/dynamic_signed/'
   #output_bin_folder = ''.join([cwd_dir + "/build/dynamic_signed"])
   #output_bin_folder = ''.join([cwd_dir + "/build/dynamic_signed"])
   #output_bin_folder_client = os.path.abspath(output_bin_folder)
   #external = 0
   external_pack = ''.join([cwd_dir + "/build/ms/tbc_cleanpack.py"])
   if not os.path.exists(external_pack) and not 'cleanpack' in env['BUILD_SCONSARGS']:
       if not env.subst('$BUILD_COMPONENT'):
           if chipset=='msm8996' or chipset=='msm8998':
               dynamic_mods1 = ''.join([cwd_dir + '/build/dynamic_modules/map_avs_shared_libs.txt'])
               dynamic_mods2 = ''.join([cwd_dir + '/build/dynamic_modules/map_SHARED_LIBS_AAAAAAAAQ.txt'])
               for file in (dynamic_mods1,dynamic_mods2):
                    if os.path.exists(file):
                        os.remove(file)
                        print "Delete ", file
               print "Creating shared libs at the start"
               saved_alias = env.subst('$IMAGE_ALIAS')
               build_flags=env.subst('$BUILD_FLAGS')
               if 'GEN_SHARED_LIBS' in build_flags:
                    env.Replace(IMAGE_ALIAS = "fastrpc_shell_img shared_libs avs_shared_libs")
               else:
                    env.Replace(IMAGE_ALIAS = "fastrpc_shell_img shared_libs")  		
               print "Alias used is ", env.subst('$IMAGE_ALIAS')
               scons_flavor_act_cmd=env.subst('$SCONS_FLAVOR_ACT_CMD')
               os.system(scons_flavor_act_cmd)
               build_act=env.subst('$BUILD_ACT')
               if build_act != 'clean':
                   if os.path.exists(dynamic_mods1) and os.path.exists(dynamic_mods2):
                       Not_in_adspsobin()
                       env_bld_ver_CRM = os.environ.get('CRM_BUILDID', None)
                       if env_bld_ver_CRM:
                           signcheck = sign_check(host1,port1)
                           print "Printing the Signing Result\n"
                           print signcheck
                       else:
                           signcheck = 0
                       if signcheck == 0:
                           static_hash_prepare(build_flags)
                           fo = open("statichash.txt", "wb")
                           print "Static_Hash Performed for the Dynamic SOs in this build \n"
                           fo.write( "StaticHash of the Dynamic Modules being done\n");
                           # Close opend file
                           fo.close()
                       adspsobin_create()
                       env.Replace(IMAGE_ALIAS = saved_alias)
                       print "Building again with Alias ", env.subst('$IMAGE_ALIAS')                    
                       scons_flavor_act_cmd=env.subst('$SCONS_FLAVOR_ACT_CMD')
                       os.system(scons_flavor_act_cmd)    
                   else:
                       print "Dynamic Modules Not Created\n"
               else:
                   env.Replace(IMAGE_ALIAS = saved_alias)
                   print "Cleaning again with Alias ", env.subst('$IMAGE_ALIAS')                    
                   scons_flavor_act_cmd=env.subst('$SCONS_FLAVOR_ACT_CMD')
                   os.system(scons_flavor_act_cmd)
           else:
               scons_flavor_act_cmd=env.subst('$SCONS_FLAVOR_ACT_CMD')
               os.system(scons_flavor_act_cmd)
       else:
           print "Alias given from the top Build Command\n"
           scons_flavor_act_cmd=env.subst('$SCONS_FLAVOR_ACT_CMD')
           os.system(scons_flavor_act_cmd)
   else: #This is External Build Just perform Static Image Generation
       print "External Build Being Triggered\n" 
       if (os.environ.get('OEM_ROOT', None)) and not env.subst('$BUILD_COMPONENT') and ('GEN_SHARED_LIBS' in env.subst('$BUILD_FLAGS')):
            print "Compiling for shared libs"
            saved_alias = env.subst('$IMAGE_ALIAS')
            env.Replace(IMAGE_ALIAS = 'custom_shared_libs')
            scons_flavor_act_cmd=env.subst('$SCONS_FLAVOR_ACT_CMD')
            os.system(scons_flavor_act_cmd)
            env.Replace(IMAGE_ALIAS = saved_alias)
       scons_flavor_act_cmd=env.subst('$SCONS_FLAVOR_ACT_CMD')
       os.system(scons_flavor_act_cmd)
       oem_root_shared_libs_copy()
       adspsobin_create()
   cwd_dir = os.getcwd()
   print "\n\nCurrent working directory is %s" % cwd_dir

def split_proc(env):
   print "================== Running splitter ==================="
   if os.path.exists('./dsp.elf'):
     split_dir = os.path.dirname('./obj/qdsp6v5_ReleaseG/LA/system/etc/firmware/')

     split_dir_unsigned = os.path.dirname('./obj/qdsp6v5_ReleaseG/unsigned/LA/system/etc/firmware/') 
     split_dir_signed = os.path.dirname('./obj/qdsp6v5_ReleaseG/signed/LA/system/etc/firmware/')
     split_dir_signed_encrypted = os.path.dirname('./obj/qdsp6v5_ReleaseG/signed_encrypted/LA/system/etc/firmware/')

     obj_dir_unsigned = os.path.dirname('./obj/qdsp6v5_ReleaseG/unsigned/') 
     obj_dir_signed = os.path.dirname('./obj/qdsp6v5_ReleaseG/signed/')
     obj_dir_signed_encrypted = os.path.dirname('./obj/qdsp6v5_ReleaseG/signed_encrypted/')


     adsp_proc_root = os.getcwd()

     if not os.path.exists(split_dir):
         os.makedirs(split_dir)
     print "================== Running splitter on adsp.mbn at default locationn (/obj/qdsp6v5_ReleaseG/)==================="
     SPLIT_CMD = "cd "+adsp_proc_root+"/obj/qdsp6v5_ReleaseG/LA/system/etc/firmware && python "+adsp_proc_root+"/qdsp6/scripts/pil-splitter.py "+adsp_proc_root+"/obj/qdsp6v5_ReleaseG/adsp.mbn adsp"
     os.system(SPLIT_CMD)
     print "================== Running splitter on non-reloc==================="
     os.system(SPLIT_CMD)

     if os.path.exists(obj_dir_unsigned):
        if not os.path.exists(split_dir_unsigned):
           os.makedirs(split_dir_unsigned)
        print "================== Running splitter on unsigned==================="
        SPLIT_CMD = "cd "+adsp_proc_root+"/obj/qdsp6v5_ReleaseG/unsigned/LA/system/etc/firmware && python "+adsp_proc_root+"/qdsp6/scripts/pil-splitter.py "+adsp_proc_root+"/obj/qdsp6v5_ReleaseG/unsigned/adsp.mbn adsp"
        os.system(SPLIT_CMD)
     if os.path.exists(obj_dir_signed):
        if not os.path.exists(split_dir_signed):
           os.makedirs(split_dir_signed)
        print "================== Running splitter on signed==================="
        SPLIT_CMD = "cd "+adsp_proc_root+"/obj/qdsp6v5_ReleaseG/signed/LA/system/etc/firmware && python "+adsp_proc_root+"/qdsp6/scripts/pil-splitter.py "+adsp_proc_root+"/obj/qdsp6v5_ReleaseG/signed/adsp.mbn adsp"
        os.system(SPLIT_CMD)
     if os.path.exists(obj_dir_signed_encrypted):
        if not os.path.exists(split_dir_signed_encrypted):
           os.makedirs(split_dir_signed_encrypted)
        print "================== Running splitter on signed_encryted==================="   
        SPLIT_CMD = "cd "+adsp_proc_root+"/obj/qdsp6v5_ReleaseG/signed_encrypted/LA/system/etc/firmware && python "+adsp_proc_root+"/qdsp6/scripts/pil-splitter.py "+adsp_proc_root+"/obj/qdsp6v5_ReleaseG/signed_encrypted/adsp.mbn adsp"
        os.system(SPLIT_CMD)
   else:
     print "dsp.elf is not generated!!!"
     sys.exit(0)
	 
def elf_extractor_proc(env):
   if os.path.exists('./build/bsp/adsp_link/build/AAAAAAAA/ADSP_PROC_IMG_AAAAAAAAQ.elf.map'): 
      print "================== Running hexagon elf extractor script ==================="
      env.Replace(ELF_EXTRACTOR_PROC_CMD = "python ./qdsp6/scripts/hexagon_elf_extractor.py --target=adsp --elf=./build/bsp/adsp_link/build/AAAAAAAA/ADSP_PROC_IMG_AAAAAAAAQ.elf")
      elf_extractor_proc_cmd=env.subst('$ELF_EXTRACTOR_PROC_CMD')
      os.system(elf_extractor_proc_cmd)

def memory_proc(env):

   build_flavor=env.subst('$BUILD_FLAVOR')
   chipset_name=env.subst('$CHIPSET')
   target_name=chipset_name[3:]
   tools_version_used = os.environ.get('HEXAGON_RTOS_RELEASE', None)   
   tools_major_version_used = int(tools_version_used[0])      
   elf_name_normal="".join(['M',target_name,'AAAAAAAAQ1234.elf'])
   elf_name_normal=os.path.join("./build/ms/",elf_name_normal)
   
   elf_name_audio="".join(['M',target_name,'AAAAAAAAQ1234_AUDIO.elf'])
   elf_name_audio=os.path.join("./build/ms/",elf_name_audio)   


   print "================== Running memory profile script ==================="
   if build_flavor=='spd':
          env.Replace(MEMORY_PROC_CMD2 = "python ./qdsp6/scripts/show_memory_qurt.py ./build/bsp/adsp_link/build/AAAAAAAA/ADSP_PROC_IMG_AAAAAAAAQ.elf ${CHIPSET}")   
          memory_proc_cmd2=env.subst('$MEMORY_PROC_CMD2')
          show_memory_qurt_flag = os.system(memory_proc_cmd2)     
   elif build_flavor=='mpd':
          env.Replace(MEMORY_PROC_CMD2 = "python ./qdsp6/scripts/show_memory_qurt.py ./build/bsp/multi_pd_img/build/AAAAAAAA/bootimage.pbn ${CHIPSET} %s"%(tools_major_version_used))       
          memory_proc_cmd2=env.subst('$MEMORY_PROC_CMD2')
          os.system(memory_proc_cmd2)
   if os.path.exists('./build/bsp/adsp_link/build/AAAAAAAA/ADSP_PROC_IMG_AAAAAAAAQ.elf.map'):
          env.Replace(MEMORY_PROC_CMD1 = "python ./qdsp6/scripts/Image_Break_Down.py ${CHIPSET} ./build/bsp/adsp_link/build/AAAAAAAA/ADSP_PROC_IMG_AAAAAAAAQ.elf.map CommonPD %s"%(tools_major_version_used))   
          memory_proc_cmd1=env.subst('$MEMORY_PROC_CMD1')
          os.system(memory_proc_cmd1)
   if os.path.exists('./build/bsp/sensor_img/build/AAAAAAAA/SENSOR_IMG_AAAAAAAAQ.elf.map'):
          env.Replace(MEMORY_PROC_CMD1 = "python ./qdsp6/scripts/Image_Break_Down.py ${CHIPSET} ./build/bsp/sensor_img/build/AAAAAAAA/SENSOR_IMG_AAAAAAAAQ.elf.map SensorsPD %s"%(tools_major_version_used))   
          memory_proc_cmd2=env.subst('$MEMORY_PROC_CMD1')
          os.system(memory_proc_cmd2)
   if os.path.exists('./build/bsp/avs_adsp_user/build/AAAAAAAA/AUDIO_IMG_AAAAAAAAQ.elf.map'):
          env.Replace(MEMORY_PROC_CMD1 = "python ./qdsp6/scripts/Image_Break_Down.py ${CHIPSET} ./build/bsp/avs_adsp_user/build/AAAAAAAA/AUDIO_IMG_AAAAAAAAQ.elf.map AudioPD %s"%(tools_major_version_used))   
          memory_proc_cmd3=env.subst('$MEMORY_PROC_CMD1')
          os.system(memory_proc_cmd3)  
   if os.path.exists('./build/dynamic_modules/'):
          env.Replace(MEMORY_PROC_CMD1 = "python ./qdsp6/scripts/DynamicModuleSize.py ./build/dynamic_modules/")   
          memory_proc_cmd4=env.subst('$MEMORY_PROC_CMD1')
          os.system(memory_proc_cmd4)

   if env['PLATFORM'] == 'posix':
          env.Replace(HEXAGON_ANALYZER_BACKEND_CMD = "hexagon-analyzer-backend")
   else:
          env.Replace(HEXAGON_ANALYZER_BACKEND_CMD = "hexagon-analyzer-backend.exe")
    
   if os.path.exists('./build/bsp/adsp_link/build/AAAAAAAA/ADSP_PROC_IMG_AAAAAAAAQ.elf.map'):
          env.Replace(MEMORY_PROC_CMD3 = ("$HEXAGON_ANALYZER_BACKEND_CMD --aidfile ./qdsp6/tools/input/libcAidFile.txt --elffile %s -o ./build/ms > ./build/ms/hexagon_StackAnalyzer_Log.txt "%elf_name_normal))
          memory_proc_cmd3=env.subst('$MEMORY_PROC_CMD3')
          os.system(memory_proc_cmd3)
             
          if os.path.exists('./build/ms/RA_FunctionStackSizes.csv'):
    		if os.path.exists('./build/ms/RA_FunctionStackSizes_normal.csv'):
    				os.remove('./build/ms/RA_FunctionStackSizes_normal.csv')
    		os.renames('./build/ms/RA_FunctionStackSizes.csv','./build/ms/RA_FunctionStackSizes_normal.csv')
    		  
          if os.path.exists('%s'%elf_name_audio):
    		env.Replace(MEMORY_PROC_CMD4 = ("$HEXAGON_ANALYZER_BACKEND_CMD --aidfile ./qdsp6/tools/input/libcAidFile.txt --elffile %s -o ./build/ms > ./build/ms/hexagon_StackAnalyzer_Log.txt "%elf_name_audio))
    		memory_proc_cmd4=env.subst('$MEMORY_PROC_CMD4')
    		os.system(memory_proc_cmd4)
    		
          os.system("python ./qdsp6/tools/stack_size.py %s %s"%(build_flavor,elf_name_audio))	


#Initialize environmentn 
def Init(env):

   os.chdir('../')
   env_init_dir = os.getcwd()
   print "\n\n Env init working directory is %s" % env_init_dir

   #InitChipset method
   env.AddMethod(init_chipset,"InitChipset")

   #create timestamp
   env.AddMethod(create_timestamp, "CreateTimestamp")

   #execute the build startup command
   env.AddMethod(execute_buildcmd, "ExecuteBuildCmd")

   #execute the split_proc command
   env.AddMethod(split_proc, "SplitProc")

   #execute the memory_proc command
   env.AddMethod(memory_proc, "MemoryProc")

   #execute the hexagon_elf_extractor command
   #env.AddMethod(elf_extractor_proc, "ElfExtractorProc")
    
   env.AddMethod(check_sim_boot, "CheckSimBoot")
def check_sim():
   print "Starting sim command"
   sim_cmd="cd avs && qdsp6-sim --simulated_returnval --mv5c_256 ../dsp.elf --simulated_returnval --rtos ../obj/qdsp6v5_ReleaseG/osam.cfg --symfile ../build/ms/M8974AAAAAAAAQ1234_reloc.elf --symfile ../build/ms/M8974AAAAAAAAQ1234_SENSOR_reloc.elf --cosim_file ./q6ss.cfg"
   os.system(sim_cmd)
   sys.exit(0)


def check_sim_boot(env):
   path =".."
   name="check_sim_boot.txt"
   full_path = os.path.join(path, name)
   print "Starting sim command"
   sim_cmd='cd avs && qdsp6-sim --simulated_returnval --mv5c_256 ../dsp.elf --simulated_returnval --rtos ../obj/qdsp6v5_ReleaseG/osam.cfg --symfile ../build/ms/M8974AAAAAAAAQ1234_reloc.elf --symfile ../build/ms/M8974AAAAAAAAQ1234_SENSOR_reloc.elf --cosim_file ./q6ss.cfg  > %s 2>&1' % full_path
   os.system(sim_cmd)
   if os.path.isfile('check_sim_boot.txt'):
       f=open('check_sim_boot.txt', 'r')
       check_msgs=['CreateEliteStaticService Begin','CreateEliteStaticService End','StartEliteStaticService start','StartEliteStaticService end','Ratio to Real Time']
       count=0
       for msg in check_msgs:
          f.seek(0, 0)
          count =0
          for line in f:
             if msg in line:
                count=count+1
          if count == 0:
             print msg+"not created"
             print "Failed in testing the sim image, check check_sim_boot.txt for more info"
             sys.exit(-1)
       print "======================= Simulation Tests Passed  =========================="
   else:
       print "======== check_sim_boot.txt is not found ================"
       sys.exit(-1)
  
