//===============================================================================
// devcfg_main.c
//
// GENERAL DESCRIPTION
//    main entry point
//
// Copyright (c) 2009-2010,2015 Qualcomm Technologies Inc.
// All Rights Reserved.
// Qualcomm Confidential and Proprietary
//
//-------------------------------------------------------------------------------
//                      EDIT HISTORY FOR FILE
//                      
//  This section contains comments describing changes made to the module.
//  Notice that changes are listed in reverse chronological order.
//  
// when       who     what, where, why
// --------   ---     ---------------------------------------------------------
// 1/4/15   Asmit     Added this file
//===============================================================================
int main(int argc, char *argv[])
{
   return 0;
}
