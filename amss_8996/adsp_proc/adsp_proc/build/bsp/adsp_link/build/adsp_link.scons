#===============================================================================
#
# Basic skeleton image scrip
#
# GENERAL DESCRIPTION
#    build script
#
# Copyright (c) 2009-2012 by Qualcomm Technologies, Incorporated.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#
#-------------------------------------------------------------------------------
#
#  $Header: //components/rel/dspbuild.adsp/2.8.2/bsp/adsp_link/build/adsp_link.scons#1 $
#  $DateTime: 2016/01/21 21:54:46 $
#  $Change: 9768170 $
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
#
#===============================================================================

import os
import re
Import('env')

#------------------------------------------------------------------------------
# Init image vars 
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# Init aliases array.
# first alias (0th elemten in array) is always unique name, there should 
# never be two images with the same unique name
aliases = ['adsp_link', 'adsp_proc', 'adsp_images', 'adsp_mpd_images', 'adsp_core_images', 'adsp_mpd_core_images',
           'msm8994_MPD', 'msm8996_MPD', 'msm8998_MPD', 'msm8992_MPD', 'msm8952_MPD'
          ]

build_tools = ['buildspec_builder.py',
         '${BUILD_ROOT}/core/bsp/build/scripts/mbn_builder.py',
         '${BUILD_ROOT}/core/bsp/build/scripts/dnt_builder.py',
         '${BUILD_ROOT}/core/bsp/build/scripts/devcfg_builder.py',
         '${BUILD_ROOT}/core/bsp/build/scripts/tms_builder_servreg.py',
         '${BUILD_ROOT}/core/bsp/build/scripts/cmm_builder.py',
         '${BUILD_ROOT}/core/bsp/build/scripts/swe_builder.py',
         '${BUILD_ROOT}/core/bsp/build/scripts/sleep_lpr_builder.py']
				
# For the 8992 or 8994 chipset, we want to include island builder support.
if env['CHIPSET'] in [ 'msm8994', 'msm8992', 'msm8952']:
   build_tools += [
            '${BUILD_ROOT}/core/bsp/build/scripts/island_builder.py',
            '${BUILD_ROOT}/core/kernel/qurt/scripts/island_analysis.py']

# For 8994 and 8996, we want to autogenerate the LCS file.
if env['CHIPSET'] in [ 'msm8994', 'msm8996', 'msm8998', 'msm8992', 'msm8952']:
   build_tools += [
            '${BUILD_ROOT}/core/bsp/build/scripts/lcs_autogen.py']

# Define the SCons image build tags.
build_tags = ['CORE_QDSP6_SW', 'AVS_ADSP', 'AVS_ADSP_ROOT', 'VIDEO_ADSP', 'QMIMSGS_ADSP',
              'IMAGE_TREE_VERSION_AUTO_GENERATE',
              'IMAGE_TREE_UUID_AUTO_GENERATE',
              'CORE_ADSP_ROOT'
             ]
#------------------------------------------------------------------------------
# Init environment variables
env.InitImageVars(
   alias_list=aliases,  # aliases list
   proc='qdsp6',      # proc (depending on tool chain arm, hexago, etc)
   config='adsp',
   plat='qurt',           # platform (l4, blast, foo, bar, etc)
   target='ADSP_PROC_IMG_${BUILD_ID}',# target (elf, image file name)
#build_tags = ['BASIC_IMAGE_EXAMPLE'],
   build_tags = build_tags,
   tools = build_tools
)

#------------------------------------------------------------------------------
# Check if we need to load this script or just bail-out
#------------------------------------------------------------------------------
if not env.CheckAlias():
   Return()

#---------------------------------------------------------------------------
# Now that we know we need to build something, the first thing we need
# to do is add our image to BuildProducts.txt, so that tools can verify
# when our build is successful.  Make sure we append, so that we don't
# overwrite other images that have written to this file.
#---------------------------------------------------------------------------
build_products_fn = env['TARGET_ROOT'] + "/BuildProducts.txt"
build_cleanpack_file = env['TARGET_ROOT'] + "/adsp_proc/build/ms/tbc_cleanpack.py"
if os.path.exists(build_cleanpack_file):
   fh = open(build_products_fn, "a")
   fh.write ("./adsp_proc/success\n")
   fh.close()
if not env.GetOption('clean'):
   fh = open(build_products_fn, "a")
   fh.write ("./adsp_proc/dsp.elf\n")
   fh.close()
else:
   if os.path.exists(build_products_fn):
      os.remove(build_products_fn)

#---------------------------------------------------------------------------
# Load in CBSP uses and path variables
#---------------------------------------------------------------------------
env.InitBuildConfig()

env.AddUsesFlags('USES_RCINIT_PLAYBOOK')

env.Replace(USES_DEVCFG = 'yes')
env.Replace(DEVCONFIG_ASSOC_FLAG = 'DAL_DEVCFG_IMG')
env.Decider('timestamp-newer')
env.AddUsesFlags('USES_SERVICE_REGISTRY_PLAYBOOK')
if ('USES_FEATURE_DYNAMIC_LOADING_GLOBAL' in env) and 'BUILD_BAREBONE' not in env:
   env.Replace(USES_FEATURE_DYNAMIC_LOADING = 'yes')
   env.LoadToolScript('sharedlib_symbols', toolpath = ['${BUILD_ROOT}/core/bsp/build/scripts'])
   env.LoadToolScript('dlinker_symbols', toolpath = ['${BUILD_ROOT}/core/bsp/build/scripts'])
      

if 'USES_MULTI_PD' in env:
   env.PrintInfo("This adsp core image is being built for Multi PD version")
   env.AddUsesFlags('USES_MPD')
   env.AddUsesFlags('USES_QURTOS_IMG') #this is the flag to be used for mpd builds to compile for guest os
   env.Append(CPPDEFINES="SENSOR_IMG_NAME=\\\"M${BUILD_ASIC}${BUILD_ID}${BUILD_VER}_SENSOR.pbn\\\"")
   env.Append(CPPDEFINES="AUDIO_IMG_NAME=\\\"M${BUILD_ASIC}${BUILD_ID}${BUILD_VER}_AUDIO.pbn\\\"")
   env.Append(CPPDEFINES=["MULTI_PD_BUILD"])
else:
   env.PrintInfo("This adsp core image is being built for Single PD version")

#if ARGUMENTS.get("$SIM") == "1" or ARGUMENTS.get("$SIM") == "TRUE":
if ARGUMENTS.get('SIM') == "1" or ARGUMENTS.get('SIM') == "TRUE":
   env.PrintInfo("SIM is in the argument, build qdsp6 for SIM image")
   env.AddUsesFlags(['USES_AVS_TEST'])
else:
   env.PrintInfo("SIM is not in the argument, build qdsp6 for target image")

#---------------------------------------------------------------------------
# Add ADSP Image specific linker inputs here
#---------------------------------------------------------------------------
env.LoadToolScript('hexagon_adsp', toolpath = ['${BUILD_ROOT}/build'])

env.Replace(HEXAGON_IMAGE_ENTRY='0xF0000000')

if 'BUILD_BAREBONE' in env:
   env.Replace(LINKFLAGS = "-m${Q6VERSION} --gc-sections -g -T${BUILD_ROOT}/build/chipset/${CHIPSET_DIR}/${LINKER_FILE} -G8 -nostdlib --section-start .start=${HEXAGON_IMAGE_ENTRY} ${HEXAGONLD_MAP_CMD} ${TARGET.posix}.map ${LFLAGS}")
else:
   env.Replace(LINKFLAGS = "-m${Q6VERSION} --gc-sections --wrap malloc --wrap calloc --wrap free --wrap realloc --wrap memalign -g -T${BUILD_ROOT}/build/chipset/${CHIPSET_DIR}/${LINKER_FILE} -G8 -nostdlib --section-start .start=${HEXAGON_IMAGE_ENTRY} ${HEXAGONLD_MAP_CMD} ${TARGET.posix}.map ${LFLAGS}")

# if feature flag USES_FEATURE_DYNAMIC_LOADING not enabled, just export base symbols into dynamic sections
if  env.GetUsesFlag('USES_FEATURE_DYNAMIC_LOADING') not in env and 'BUILD_BAREBONE' not in env  :
   env.Replace(DYNLINKFLAGS = "--force-dynamic --dynamic-linker= --dynamic-list=${BUILD_ROOT}/platform/exports/dynsymbols.lst -T${BUILD_ROOT}/platform/exports/externs.lst")

env.Replace(LINKOBJGRP = "${QDSP6OBJS1} " \
   "--start-group $_LIBFLAGS_POSIX ${QDSP6OBJS2} $SOURCES.posix $_LISTFILES ${QDSP6OBJS3} --end-group " \
   "${QDSP6OBJS4} "
)  

#env.Replace(LINKCOM = "${TEMPFILE('$LINK $LINKFLAGS $LINK_DIRS $LINKOBJGRP $HEXAGON_OUTPUT_CMD $TARGET.posix $LINKFLAGS_END')}")
env.Replace(LINKCOM = "${TEMPFILE('$LINK $LINKFLAGS $LINK_DIRS $LINKOBJGRP $DYNLINKFLAGS $HEXAGON_OUTPUT_CMD $TARGET.posix $LINKFLAGS_END')}")

#---------------------------------------------------------------------------
# Check Multi PD and change variables here
#---------------------------------------------------------------------------
if 'AUDIO_IN_USERPD' in env:
   env.Replace(AVS_LIB = 'avs_root_libs')
   env.Replace(AVS_LIBS_TXT = 'AVS_ROOT_LIBS_AAAAAAAAQ.txt')
else:
   env.Replace(AVS_LIB = 'avs_libs')
   env.Replace(AVS_LIBS_TXT = 'AVS_LIBS_AAAAAAAAQ.txt')
   
#---------------------------------------------------------------------------
# Libs/Objs
#---------------------------------------------------------------------------
image_libs = []
image_objs = []

#---------------------------------------------------------------------------
# Libraries Section
#---------------------------------------------------------------------------
core_qurt_env = env.Clone()
#hexagon_adsp.py defined the CHIPSET_DIR based on CHIPSET info and whether it is the MEMOPT case
cust_config_xml = core_qurt_env.RealPath('${BUILD_ROOT}/build/chipset/${CHIPSET_DIR}'+'/qdsp6.xml')
core_qurt_env.PrintInfo("the chipset directory where the XML file called from is: ${CHIPSET_DIR} for cust_config_xml")

core_qurt_env.Replace(CONFIG_XML=cust_config_xml)

if core_qurt_env['MSM_ID'] in ['8994', '8992', '8952']:
   core_qurt_env.Replace(QURT_BUILD_CONFIG="ADSPv56MP")
elif core_qurt_env['MSM_ID'] in ['8996', '8998']:
   core_qurt_env.Replace(QURT_BUILD_CONFIG="ADSPv60MP")
else:
   env.PrintError("QURT_BUILD_CONFIG is not defined for this chipset, exit compilation!!!")
   Exit(1)

core_qurt_items = core_qurt_env.LoadAreaSoftwareUnits('core', ["kernel/qurt"])
image_libs.extend(core_qurt_items['LIBS'])
image_objs.extend(core_qurt_items['OBJS'])

# for image version reporting
build_items = env.LoadAreaSoftwareUnits('build')
image_libs.extend(build_items['LIBS'])
image_objs.extend(build_items['OBJS'])

image_units = [image_objs, image_libs]


# Create UUID  file
img_uuid_tag = 'IMAGE_TREE_UUID_AUTO_GENERATE'
if env.IsKeyEnable(img_uuid_tag):
    env.LoadToolScript('version_builder', toolpath = ['${BUILD_SCRIPTS_ROOT}'])
    oem_uuid_c = env.OEM_UUID_Builder(img_uuid_tag,    
    '${SHORT_BUILDPATH}/oem_uuid.c', [image_objs, image_libs],
    TARGET_IMG_PATH = env.RealPath("${SHORT_BUILDPATH}/${TARGET_NAME}"))
    #oem_uuid_o = env.Object(oem_uuid_c)
    oem_uuid_o = env.AddObject(img_uuid_tag, oem_uuid_c)
    #image_units += oem_uuid_o
    image_objs.append(oem_uuid_o)

#--- RCINIT Playbook Extension, Library Specific Details -------------------

core_rcinit_pl=env.RealPath('${BUILD_ROOT}/build/bsp/core_libs/build/AAAAAAAA/rcinit_playlist.rcpl')
wlan_rcinit_pl=env.RealPath('${BUILD_ROOT}/build/bsp/wlan_libs/build/AAAAAAAA/rcinit_playlist.rcpl')
loc_rcinit_pl=env.RealPath('${BUILD_ROOT}/build/bsp/loc_libs/build/AAAAAAAA/rcinit_playlist.rcpl')
platform_rcinit_pl=env.RealPath('${BUILD_ROOT}/build/bsp/platform_libs/build/AAAAAAAA/rcinit_playlist.rcpl')
apr_rcinit_pl=env.RealPath('${BUILD_ROOT}/build/bsp/apr_libs/build/AAAAAAAA/rcinit_playlist.rcpl')
qdsp6_rcinit_pl=env.RealPath('${BUILD_ROOT}/build/bsp/qdsp6_libs/build/AAAAAAAA/rcinit_playlist.rcpl')
performance_rcinit_pl=env.RealPath('${BUILD_ROOT}/build/bsp/performance_libs/build/AAAAAAAA/rcinit_playlist.rcpl')


PLAYLISTS = [core_rcinit_pl]

if 'BUILD_BAREBONE' not in env:
   PLAYLISTS.append(platform_rcinit_pl)
   PLAYLISTS.append(apr_rcinit_pl)
   PLAYLISTS.append(qdsp6_rcinit_pl)
   PLAYLISTS.append(performance_rcinit_pl)
   PLAYLISTS.append(wlan_rcinit_pl)
   PLAYLISTS.append(loc_rcinit_pl)

   if 'USES_DISABLE_WLAN' in env:
      PLAYLISTS.remove(wlan_rcinit_pl)
   if 'USES_DISABLE_LOC' in env:
      PLAYLISTS.remove(loc_rcinit_pl)
   if 'USES_DISABLE_PERFORMANCE' in env:
      PLAYLISTS.remove(performance_rcinit_pl)

print 'playlist Files: \n', PLAYLISTS

# Follows all LoadAreaSoftwareUnits(). Precedes Link Step Details.
# Image Owner supplies PLAYLISTS. Avoid other customization this step.

# FOR SCONS TOOL EMITTERS TO PLACE OUTPUT PROPERLY
if not os.path.exists(env.RealPath('${SHORT_BUILDPATH}')):
   if Execute(Mkdir(env.RealPath('${SHORT_BUILDPATH}'))):
      raise

# ONLY WHEN DNT_BUILDER SCONS TOOL LOADED
if 'USES_RCINIT' in env and 'USES_RCINIT_PLAYBOOK' in env:

   # NEVER POLLUTE ENV CONSTRUCTION ENVIRONMENT WHICH GETS INHERITED
   playbook_env = env.Clone()

   # PLAYBOOK C OUTPUT THIS IMAGE (REQUIRED)
   rcinit_out_c = playbook_env.RealPath('${SHORT_BUILDPATH}/rcinit_autogen.c')
   playbook_env.AddRCInitPlaybook(build_tags, rcinit_out_c, PLAYLISTS)
   rcinit_obj = playbook_env.AddObject(build_tags, rcinit_out_c)
   playbook_env.Depends(build_tags, rcinit_out_c)     # Manage explicit detail outside of AU
   image_objs.append(rcinit_obj)                      # Manage explicit detail outside of AU

   # PLAYBOOK TXT OUTPUT THIS IMAGE (REQUIRED)
   rcinit_out_txt = playbook_env.RealPath('${SHORT_BUILDPATH}/${TARGET_NAME}_rcinit_output_log.txt')
   playbook_env.AddRCInitPlaybook(build_tags, rcinit_out_txt, None)
   playbook_env.AddArtifact(build_tags, rcinit_out_txt)
   playbook_env.Depends(rcinit_out_txt, rcinit_out_c) # Manage explicit detail outside of AU
   image_units.append(rcinit_out_txt)                 # Manage explicit detail outside of AU

#--- RCINIT Playbook Extension, Library Specific Details -------------------

#--- SERVICE REGISTRY Playbook Extension, Image Specific Details -----------BEGIN

build_tags = ['CORE_QDSP6_SW', 'AVS_ADSP', 'AVS_ADSP_ROOT', 'VIDEO_ADSP', 'QMIMSGS_ADSP',
              'IMAGE_TREE_VERSION_AUTO_GENERATE',
              'IMAGE_TREE_UUID_AUTO_GENERATE',
             ]

# Follows all LoadAreaSoftwareUnits(). Precedes Link Step Details.

# FOR SCONS TOOL EMITTERS TO PLACE OUTPUT PROPERLY
if not os.path.exists(env.RealPath('${SHORT_BUILDPATH}')):
   if Execute(Mkdir(env.RealPath('${SHORT_BUILDPATH}'))):
      raise

# ONLY WHEN tms_builder_servreg SCONS TOOL LOADED
if 'USES_SERVICE_REGISTRY' in env and 'USES_SERVICE_REGISTRY_PLAYBOOK' in env:

   # NEVER POLLUTE ENV CONSTRUCTION ENVIRONMENT WHICH GETS INHERITED
   playbook_servreg_env = env.Clone()

   # EVERY IMAGE OWNER HAS TO DEFINE THE DOMAIN SCOPES USING AddServRegDomain() API
   playbook_servreg_env.AddServRegDomain(
      build_tags,
      {
         'soc'                  : 'msm',
         'domain'               : 'adsp',
         'subdomain'            : 'root_pd',
      })

   # PLAYLIST TO STORE THE SERVICE REGISTRY - DOMAIN INFORMATION
   servreg_out_pl = playbook_servreg_env.RealPath('${SHORT_BUILDPATH}/servreg_playlist.pl')
   playbook_servreg_env.AddServRegPlaylist(build_tags, servreg_out_pl)
   playbook_servreg_env.Depends(build_tags, servreg_out_pl)
   image_units.append(servreg_out_pl)

   # LIST OF ALL PLAYLISTS THAT WILL CONTRIBUTE TO MAKE THE OUTPUT JSON FILE

   adsplink_servreg_pl=env.RealPath('${BUILD_ROOT}/build/bsp/adsp_link/build/AAAAAAAA/servreg_playlist.pl')
   core_servreg_pl=env.RealPath('${BUILD_ROOT}/build/bsp/core_libs/build/AAAAAAAA/servreg_playlist.pl')
   qdsp6_servreg_pl=env.RealPath('${BUILD_ROOT}/build/bsp/qdsp6_libs/build/AAAAAAAA/servreg_playlist.pl')
   qmimsgs_servreg_pl=env.RealPath('${BUILD_ROOT}/build/bsp/qmimsgs_libs/build/AAAAAAAA/servreg_playlist.pl')
   apr_servreg_pl=env.RealPath('${BUILD_ROOT}/build/bsp/apr_libs/build/AAAAAAAA/servreg_playlist.pl')
   avsroot_servreg_pl=env.RealPath('${BUILD_ROOT}/build/bsp/avs_root_libs/build/AAAAAAAA/servreg_playlist.pl')
   platform_servreg_pl=env.RealPath('${BUILD_ROOT}/build/bsp/platform_libs/build/AAAAAAAA/servreg_playlist.pl')
   wlan_servreg_pl=env.RealPath('${BUILD_ROOT}/build/bsp/wlan_libs/build/AAAAAAAA/servreg_playlist.pl')
   loc_servreg_pl=env.RealPath('${BUILD_ROOT}/build/bsp/loc_libs/build/AAAAAAAA/servreg_playlist.pl')
   performance_servreg_pl=env.RealPath('${BUILD_ROOT}/build/bsp/performance_libs/build/AAAAAAAA/servreg_playlist.pl')

   SERVREG_PLAYLISTS = [adsplink_servreg_pl, core_servreg_pl, qdsp6_servreg_pl, qmimsgs_servreg_pl]
   
   if 'BUILD_BAREBONE' not in env:
      SERVREG_PLAYLISTS.append(apr_servreg_pl)
      SERVREG_PLAYLISTS.append(avsroot_servreg_pl)
      SERVREG_PLAYLISTS.append(platform_servreg_pl)
      SERVREG_PLAYLISTS.append(wlan_servreg_pl)
      SERVREG_PLAYLISTS.append(loc_servreg_pl)
      SERVREG_PLAYLISTS.append(performance_servreg_pl)

      if 'USES_DISABLE_WLAN' in env: 
         SERVREG_PLAYLISTS.remove(wlan_servreg_pl)
      if 'USES_DISABLE_LOC' in env:
         SERVREG_PLAYLISTS.remove(loc_servreg_pl)
      if 'USES_DISABLE_PERFORMANCE' in env:
         SERVREG_PLAYLISTS.remove(performance_servreg_pl)

   print 'servreg playlist Files: \n', SERVREG_PLAYLISTS

   # CREATION OF OUTPUT JSON FILE
   servreg_out_json = playbook_servreg_env.RealPath('${SHORT_BUILDPATH}/${TARGET_NAME}_servreg.json')
   playbook_servreg_env.AddServRegPlaybook(build_tags, servreg_out_json, SERVREG_PLAYLISTS)
   playbook_servreg_env.Depends(build_tags, servreg_out_json)
   image_units.append(servreg_out_json)

   # CREATEION OF OUTPUT C FILE i.e THE SERVICE REGISTRY LOCAL DATABASE
   servreg_out_c = playbook_servreg_env.RealPath('${SHORT_BUILDPATH}/servreg_local_db_augtogen.c')
   playbook_servreg_env.AddServRegPlaybook(build_tags, servreg_out_c, SERVREG_PLAYLISTS)
   servreg_obj = playbook_servreg_env.AddObject(build_tags, servreg_out_c)
   playbook_servreg_env.Depends(build_tags, servreg_out_c)
   image_objs.append(servreg_obj)

 # INSTALL JSON FILE IN LOCAL DIRECTORY
   servreg_out_json_install = InstallAs(env.RealPath("${BUILD_MS_ROOT}/servreg/${BUILD_ID}/adspr.jsn"),servreg_out_json)
   image_units += [servreg_out_json_install]
   
#--- SERVICE REGISTRY Playbook Extension, Image Specific Details ----------- END

#------------------------------------------------------------------------------
# Putting the image toghther
#------------------------------------------------------------------------------

if 'IMAGE_BUILD_LOCAL_FILES' in env:
   #-------------------------------------------------------------------------
   # Local Files
   #-------------------------------------------------------------------------
   
   # this is where local files are created, for example link scripts (lcs)
   # for qdsp6 like images, or scatter load files (scl) for amr like images.
   local_itmes= []
   
   if 'USES_LCS_FILE' in env:
      # Dynamically load lcs_builder
      env.LoadToolScript("${BUILD_SCRIPTS_ROOT}/lcs_builder.py")
         
      # Location and name of lcs file
      lcs_file = env.LcsBuilder("${SHORT_BUILDPATH}/${TARGET_NAME}.elf.lcs", 
         "${BUILD_MS_ROOT}/${BUILD_ASIC}_QuRT_${TARGET_IMAGE}.lcs")
      local_itmes = lcs_file

   image_units += local_itmes
      
if 'IMAGE_BUILD_LINK' in env:
   #Dynamic symbol list files required from other dependent components
   core_sym_lst = env.RealPath('${BUILD_ROOT}/build/bsp/core_libs/build/AAAAAAAA/CORE_SYMS_AAAAAAAAQ.txt')
   platform_sym_lst=env.RealPath('${BUILD_ROOT}/build/bsp/platform_libs/build/AAAAAAAA/PLATFORM_SYMS_AAAAAAAAQ.txt')
   avs_sym_lst=env.RealPath('${BUILD_ROOT}/build/bsp/${AVS_LIB}/build/AAAAAAAA/AVS_SYMS_AAAAAAAAQ.txt')

   # libs txt file containing the path and partial libs from each tech area unit
   core_libs_txt=env.RealPath('${BUILD_ROOT}/build/bsp/core_libs/build/AAAAAAAA/CORE_LIBS_AAAAAAAAQ.txt')
   avs_libs_txt=env.RealPath('${BUILD_ROOT}/build/bsp/${AVS_LIB}/build/AAAAAAAA/${AVS_LIBS_TXT}')
   qdsp6_libs_txt=env.RealPath('${BUILD_ROOT}/build/bsp/qdsp6_libs/build/AAAAAAAA/QDSP6_LIBS_AAAAAAAAQ.txt')
   platform_libs_txt=env.RealPath('${BUILD_ROOT}/build/bsp/platform_libs/build/AAAAAAAA/PLATFORM_LIBS_AAAAAAAAQ.txt')
   sensors_libs_txt=env.RealPath('${BUILD_ROOT}/build/bsp/sensors_libs/build/AAAAAAAA/SENSORS_LIBS_AAAAAAAAQ.txt')
   qmimsgs_libs_txt=env.RealPath('${BUILD_ROOT}/build/bsp/qmimsgs_libs/build/AAAAAAAA/QMIMSGS_LIBS_AAAAAAAAQ.txt')
   video_libs_txt=env.RealPath('${BUILD_ROOT}/build/bsp/video_libs/build/AAAAAAAA/VIDEO_LIBS_AAAAAAAAQ.txt')
   loc_libs_txt=env.RealPath('${BUILD_ROOT}/build/bsp/loc_libs/build/AAAAAAAA/LOC_LIBS_AAAAAAAAQ.txt')
   wlan_libs_txt=env.RealPath('${BUILD_ROOT}/build/bsp/wlan_libs/build/AAAAAAAA/WLAN_LIBS_AAAAAAAAQ.txt')
   apr_libs_txt=env.RealPath('${BUILD_ROOT}/build/bsp/apr_libs/build/AAAAAAAA/APR_LIBS_AAAAAAAAQ.txt')
   performance_libs_txt=env.RealPath('${BUILD_ROOT}/build/bsp/performance_libs/build/AAAAAAAA/PERFORMANCE_LIBS_AAAAAAAAQ.txt')
   
   #-------------------------------------------------------------------------
   # Link image
   #-------------------------------------------------------------------------
   # base link library files
   listfile = [qdsp6_libs_txt, core_libs_txt, apr_libs_txt, avs_libs_txt, platform_libs_txt, qmimsgs_libs_txt]

   if env.get('CHIPSET') == "msm8992" or env.get('CHIPSET') == "msm8994" or env.get('CHIPSET') == "msm8952":
      listfile.append(sensors_libs_txt)
      listfile.append(loc_libs_txt)
      listfile.append(wlan_libs_txt)
      
   if env.get('CHIPSET') == "msm8996" or env.get('CHIPSET') == "msm8998" or env.get('CHIPSET') == "msm8952":	  
      listfile.append(performance_libs_txt)
      
   if env.get('CHIPSET') == "msm8996":
       listfile.append(loc_libs_txt)
       listfile.append(wlan_libs_txt)
#   if 'USES_AVS_TEST' in env:  Nothing needs to be done here

   if 'BUILD_BAREBONE' in env:
      listfile = [qdsp6_libs_txt, core_libs_txt, qmimsgs_libs_txt, performance_libs_txt]

   if 'BUILD_SLPI' in env:
      listfile.remove(apr_libs_txt)
      
   if 'USES_DISABLE_SENSORS' in env or not env.PathExists("${BUILD_ROOT}/Sensors"):
      if sensors_libs_txt in listfile: 
         listfile.remove(sensors_libs_txt)   

   if 'USES_DISABLE_WLAN' in env or not env.PathExists("${BUILD_ROOT}/wlan"):
      if wlan_libs_txt in listfile: 
         listfile.remove(wlan_libs_txt)
      
   if 'USES_DISABLE_LOC' in env or not env.PathExists("${BUILD_ROOT}/loc"):
      if loc_libs_txt in listfile:
         listfile.remove(loc_libs_txt) 

   print 'Library List Files: \n', listfile
   #-------------------------------------------------------------------------
   # Dynamic Loading Support
   #-------------------------------------------------------------------------
   if env.GetUsesFlag('USES_FEATURE_DYNAMIC_LOADING') and 'BUILD_BAREBONE' not in env :
      env.Replace(DYNLINKFLAGS = "--force-dynamic --dynamic-linker= ")
      
      #Generate dynamic symbols list files for exporting symbols for shared library
      dynsym_export_lst = '${SHORT_BUILDPATH}/dynsymbols.lst'
      dynsym_extern_lst = '${SHORT_BUILDPATH}/externs.lst'
      sym_list = [platform_sym_lst , core_sym_lst , avs_sym_lst]
      sym_lst_all = env.DLExposeLSTBuilder([dynsym_export_lst, dynsym_extern_lst], sym_list)
      #Add dynamic symbols list files as dependency on final image
      image_units += sym_lst_all
      
   # This is only executed if we are compiling for a chipset using the uImage island mode.
   if 'USES_ISLAND' in env:
      # Provide locations of linker template, island specification, and kernel script.      
      core_libs_island_txt=env.RealPath('${BUILD_ROOT}/build/bsp/core_libs/build/AAAAAAAA/CORE_LIBS_AAAAAAAAQ_island.txt')
      
      qurt_island_txt=env.GenerateIslandList()
      image_units += [qurt_island_txt]      
      island_analysis_filename=env.RealPath('${BUILD_ROOT}/build/bsp/core_libs/build/AAAAAAAA/island_analysis.txt')
      libc_island = env.RealPath('${BUILD_ROOT}/build/chipset/${CHIPSET_DIR}/hex.ispec')
      island_analysis_file = env.IslandAnalysis(island_analysis_filename,[core_libs_island_txt,libc_island, qurt_island_txt],['${BUILD_ROOT}','${QDSP6_RELEASE_LIB_DIR}'],[])
      image_units += [island_analysis_file]
      
   # This will be run if we are autogenerating the LCS file.
   if 'USES_LCS_AUTOGEN' in env:
      linker_template = "${BUILD_ROOT}/build/chipset/${CHIPSET_DIR}/adsp_root.lcs.template"
      generated_linker = "${SHORT_BUILDPATH}/adsp_root"
      generated_linker_path = "${BUILD_ROOT}/build/bsp/adsp_link/build/AAAAAAAA/adsp_root.lcs"
      
      # Generate the LCS from the template, and add it to the environment.
      if 'USES_ISLAND' in env:
         lcs_autogen = env.GenerateLCSFileFromTemplate(generated_linker, linker_template, [core_libs_island_txt,libc_island,qurt_island_txt])
         env.Depends(lcs_autogen, island_analysis_file)
      # If this is not an island build, do not pass in I-SPEC files.
      else:
         lcs_autogen = env.GenerateLCSFileFromTemplate(generated_linker, linker_template)
      image_units += [lcs_autogen]

      # Store the LCS file in the image environment, if we need to refer to it later.
      rootenv = env.get('IMAGE_ENV')
      rootenv['lcs_file'] = lcs_autogen
      
      # Get the LINKFLAGS.
      linkflags = env.get('LINKFLAGS')
      # Search to see if a template file is being used.
      if re.match(r'.* -{1,2}T.*',linkflags):
         # Modify the linkflags to use the new template file.
         modified_linkflags = re.sub(r'-{1,2}T ?([^ ]*)','-T ' + generated_linker_path, linkflags)
         env.Replace(LINKFLAGS = modified_linkflags)
      else:
         # Append the linker script to the linkflags.
         env.Append(LINKFLAGS = "-T " + generated_linker_path)

      # Temporary requirement to replace '.start' with '.qurtstart'
      # Get the LINKFLAGS.
      linkflags = env.get('LINKFLAGS')
      # Search to see if '.start' is being used.
      if re.match(r'.* .start*',linkflags):
         # Modify the linkflags to use '.qurtstart'.
         modified_linkflags = re.sub(r'\.start','.qurtstart', linkflags)
         env.Replace(LINKFLAGS = modified_linkflags)
      else:
         env.PrintError("Cannot find .start in LINKFLAGS.  This is expected.")
         env.PrintError("In Island linking, we need to replace .start with .qurtstart")
         exit(1)

   # this is where the rule to "link" is done.
   if env.GetUsesFlag('USES_FEATURE_DYNAMIC_LOADING') and 'BUILD_BAREBONE' not in env :
      image_elf = env.AddProgram("${SHORT_BUILDPATH}/${TARGET_NAME}", image_objs, LIBS=image_libs, LISTFILES=listfile, 
      SHARED_LIBS_DYNSYM=sym_lst_all[0], SHARED_LIBS_EXTSYM=sym_lst_all[1])
   else :
      image_elf = env.AddProgram("${SHORT_BUILDPATH}/${TARGET_NAME}", image_objs, LIBS=image_libs, LISTFILES=listfile)
   
   # The image linking depends on whether the LCS file was generated.
   if 'USES_LCS_AUTOGEN' in env:
      env.Depends(image_elf, lcs_autogen)

   # this is just to create something for testing
   #image_elf = env.ListFileBuilder("${SHORT_BUILDPATH}/${TARGET_NAME}.txt", [image_objs, image_libs], 
   #      add_header=True)
   
if 'IMAGE_BUILD_POST_LINK' in env:
   #-------------------------------------------------------------------------
   # Post process image
   #-------------------------------------------------------------------------
   env.Replace(BUILD_ELF_EXTN = '_NODEVCFG')
   # copy elf and reloc to needed locations for AMSS tools to load on target
   install_target_elf = env.InstallAs(
      "${BUILD_MS_ROOT}/M${BUILD_ASIC}${BUILD_ID}${BUILD_VER}${BUILD_ELF_EXTN}.elf", image_elf)

   # this is where any aditional rules after linking are done.

   #=========================================================================
   # Define targets needed 
   #
   if 'USES_CMMBUILDER' in env:
      if 'USES_CMMBUILDER_V2' in env:
         cmmscripts = env.CMMBuilderGenerateFiles()
         image_units += [cmmscripts]
    
      else:
         cmm_image_list_file=env.RealPath('${BUILD_ROOT}/build/bsp/adsp_link/build/AAAAAAAA/cmmoutputlist.txt')
          
         cmmfiles = env.CreateCMMImageFile(cmm_image_list_file,[])
         image_units += [cmmfiles]
          
         image_cmm_lst = env.RealPath('${BUILD_ROOT}/build/bsp/adsp_link/build/AAAAAAAA/cmmimage_adsp_link.txt')
          
         sourcefiles = [   env.RealPath('${BUILD_ROOT}/build/bsp/adsp_link/build/AAAAAAAA/cmmoutputlist.txt'),
                            env.RealPath('${BUILD_ROOT}/build/bsp/core_libs/build/AAAAAAAA/cmmoutputlist.txt')
                       ]   
         cmmtarget=image_cmm_lst
         env.Depends(cmmtarget,cmmfiles)
         #make final scripts out of all the individual image enviro's
          
         cmmscripts = env.CMMGenerateFiles(cmmtarget,sourcefiles)
         image_units += [cmmscripts]
          
   image_units += [
      image_elf,
      install_target_elf
   ]

#=========================================================================
# Finish up...
env.BindAliasesToTargets(image_units)

