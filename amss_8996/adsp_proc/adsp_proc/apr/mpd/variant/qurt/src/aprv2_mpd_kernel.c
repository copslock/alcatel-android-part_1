/*
  Copyright (C) 2014 Qualcomm Technologies Incorporated.
  All rights reserved.
  Qualcomm Confidential and Proprietary

  $Header: //components/rel/apr.adsp/2.3/mpd/variant/qurt/src/aprv2_mpd_kernel.c#3 $
  $Author: pwbldsvc $
*/
#include <qurt.h>
#include "qurt_qdi_driver.h"
#include "apr_errcodes.h"
#include "aprv2_api_inline.h"
#include "apr_lock.h"
#include "apr_list.h"
#include "aprv2_packet.h"
#include "aprv2_mpd_i.h"
#include "aprv2_mpd_qurt_i.h"

/*****************************************************************************
 * Defines                                                                   *
 ****************************************************************************/

/* Max. no. of uAPRs allowed */
#define APRV2_MPD_MAX_UAPRS                   10

/* Total number of packets */
#define APRV2_MPD_NUM_PACKETS                 5 * APRV2_MPD_MAX_UAPRS

/*****************************************************************************
 * Data Structures                                                           *
 ****************************************************************************/

typedef struct aprv2_mpd_packet_item_s {
   apr_list_node_t link;
   aprv2_packet_t *packet;
} aprv2_mpd_packet_item_t;

struct aprv2_mpd_uapr_item_s;

typedef struct aprv2_mpd_uapr_s {
   qurt_qdi_obj_t qdiobj;

   /* Driver defined fields */
   struct aprv2_mpd_uapr_item_s *nodeptr;       /* Ptr to its container item */
   qurt_signal_t * rx_signal;     /* To signal uAPR about incoming rx packet */
   uint8_t domain_id;                /* Assuming only one domain id per uAPR */
 //aprv2_mpd_packet_item_t *rx_packet;             /* Rx packet for the uAPR */
   apr_list_t rx_packet_q;                   /* Rx packet queue for the uAPR */

} aprv2_mpd_uapr_t;

typedef struct aprv2_mpd_uapr_item_s {
   apr_list_node_t link;
   aprv2_mpd_uapr_t uapr;
} aprv2_mpd_uapr_item_t;

typedef struct aprv2_mpd_qdi_opener_s {
   qurt_qdi_obj_t qdiobj;
} aprv2_mpd_qdi_opener;


/*****************************************************************************
 * Declarations                                                              *
 ****************************************************************************/

static int aprv2_mpd_qdi_invocation
(
   int client_handle,
   qurt_qdi_obj_t *obj,
   int method,
   qurt_qdi_arg_t a1,
   qurt_qdi_arg_t a2,
   qurt_qdi_arg_t a3,
   qurt_qdi_arg_t a4,
   qurt_qdi_arg_t a5,
   qurt_qdi_arg_t a6,
   qurt_qdi_arg_t a7,
   qurt_qdi_arg_t a8,
   qurt_qdi_arg_t a9
);

static void aprv2_mpd_qdi_release(qurt_qdi_obj_t *qdiobj);
static int32_t aprv2_mpd_allocate_uapr(aprv2_mpd_uapr_t **ppuapr);
static int32_t aprv2_mpd_free_uapr(aprv2_mpd_uapr_t *puapr);
static int32_t aprv2_mpd_free_packet_item(aprv2_mpd_packet_item_t *ppacket_item);
static int32_t aprv2_mpd_allocate_packet_item(aprv2_mpd_packet_item_t **pppacket_item);

/*****************************************************************************
 * Variables                                                                 *
 ****************************************************************************/

/*
 * Data Definitions
*/
static aprv2_mpd_qdi_opener aprv2_mpd_opener = {
   { aprv2_mpd_qdi_invocation,
     QDI_REFCNT_PERM,
     NULL }
};

static apr_lock_t aprv2_mpd_packets_lock;
static aprv2_mpd_packet_item_t aprv2_mpd_packets[APRV2_MPD_NUM_PACKETS];
//static apr_list_t aprv2_mpd_used_packet_q;
static apr_list_t aprv2_mpd_free_packet_q;

static aprv2_mpd_uapr_item_t aprv2_mpd_uaprs[APRV2_MPD_MAX_UAPRS];
static apr_list_t aprv2_mpd_used_uapr_q;
static apr_list_t aprv2_mpd_free_uapr_q;

/*****************************************************************************
 * Implementations                                                           *
 ****************************************************************************/

static void aprv2_mpd_isr_lock_fn (void)
{
   apr_lock_enter(aprv2_mpd_packets_lock);
}


static void aprv2_mpd_isr_unlock_fn (void)
{
   apr_lock_leave(aprv2_mpd_packets_lock);
}


APR_INTERNAL int32_t aprv2_mpd_init ( void )
{
    int32_t ret = APR_EOK;
    uint32_t index;

   /* 
    *  Initializing the pre-allocated pool of packets
   */
   ret = apr_lock_create(APR_LOCK_TYPE_MUTEX, &aprv2_mpd_packets_lock);

 //apr_list_init(aprv2_mpd_used_packet_q,
 //                         aprv2_mpd_isr_lock_fn, aprv2_mpd_isr_unlock_fn);
   apr_list_init(&aprv2_mpd_free_packet_q,
                            aprv2_mpd_isr_lock_fn, aprv2_mpd_isr_unlock_fn);

   for (index = 0; index < APRV2_MPD_NUM_PACKETS; index++)
   {
      apr_list_add_tail(&aprv2_mpd_free_packet_q,
                             &aprv2_mpd_packets[index].link);
   }

   /* 
    *  Initializing the pre-allocated array of aprv2_mpd_uapr_t objects.
   */
   apr_list_init(&aprv2_mpd_used_uapr_q, NULL, NULL);
   apr_list_init(&aprv2_mpd_free_uapr_q, NULL, NULL);

   for (index = 0; index < APRV2_MPD_MAX_UAPRS; index++)
   {
      apr_list_init(&aprv2_mpd_uaprs[index].uapr.rx_packet_q, 
                             aprv2_mpd_isr_lock_fn, aprv2_mpd_isr_unlock_fn);
      apr_list_add_tail(&aprv2_mpd_free_uapr_q, &aprv2_mpd_uaprs[index].link);
   }

   /* 
    * Registering APR MPD DeviceName with QuRT.
    *
   */
   if(qurt_qdi_devname_register(
               APRV2_MPD_DEVICE_NAME, &aprv2_mpd_opener.qdiobj))
   {
      ret = APR_EFAILED;
   }

   return ret;

}

APR_INTERNAL int32_t aprv2_mpd_deinit ( void )
{
    int32_t ret = APR_EOK;

   /* TODO:
    *
    * 1. Do the required clean-up as kAPR is going down.
    *
   */

   return ret;
}

/*
  Register a service with Kernel APR.
 
  @param addr address of service being registered.
*/
APR_INTERNAL int32_t aprv2_mpd_register(uint16_t addr )
{

   /* This is no-op on kAPR. Returning APR_EOK
   */
   return APR_EOK;
}

/*
  Deregister a service with Kernel APR.
 
  @param addr address of service being registered.
*/
APR_INTERNAL int32_t aprv2_mpd_deregister(uint16_t addr )
{

   /* This is a no-op on kAPR. Returning APR_EOK
   */
   return APR_EOK;
}

/*
 * Send a packet to the dst address specified in the packet. In case
 * of UAPR the packet is sent to Kernel. In-case of KAPR the packet gets
 * sent to the correct destination based on dst ID. 
 *
 * Assumption: Ownership of the memory allocated for packet is transferred to
 *             APR MPD.
 *
 * @param packet apr packet to send
*/
APR_INTERNAL int32_t aprv2_mpd_send(aprv2_packet_t* packet )
{
   /*
    * Based on the book keeping information maintained by kAPR determine
    * which uAPR hosts this service and send the message.
    *
    * Although uAPR informs kAPR on each service registration. Routing of
    * messages would be made solely based on the domain id.
    *
    * Per design, each uAPR shall be assigned one or more domain ids. But, no
    * two uAPRs shall have the same domain id.
    *
   */

   uint16_t dst_domain_id;
   apr_list_node_t *cur_node, *dummy_node;
   aprv2_mpd_uapr_item_t *cur_uapr_item;
   aprv2_mpd_packet_item_t *ppacket_item;
   int32_t ret;

   if (NULL == packet)
      return APR_EBADPARAM;

   dst_domain_id = APR_GET_FIELD(APRV2_PKT_DOMAIN_ID, packet->dst_addr);
   /* Find if there exists an uAPR with a matching domain ID
   */
   dummy_node = &aprv2_mpd_used_uapr_q.dummy;
   cur_node = dummy_node->next;

   while (cur_node != dummy_node)
   {
      cur_uapr_item = (aprv2_mpd_uapr_item_t *)cur_node;
      if (dst_domain_id == cur_uapr_item->uapr.domain_id)
         break;

      cur_node = cur_node->next;
   }

   if (cur_node == dummy_node)
      return APR_ENOTEXIST;

   /* Queue the packet to this uAPR and issue a signal to the same.
   */
   ret = aprv2_mpd_allocate_packet_item(&ppacket_item);
   if (APR_EOK != ret)
      return APR_ENOMEMORY;

   ppacket_item->packet = packet;

   /* Adding to the uAPR Rx packet queue
   */
   //cur_uapr_item->uapr.rx_packet = ppacket_item;
   apr_list_add_tail(&cur_uapr_item->uapr.rx_packet_q, (apr_list_node_t *)ppacket_item);

   /* Signal uAPR */
   qurt_signal_set(cur_uapr_item->uapr.rx_signal, APRV2_MPD_SIG_RX_PACKET);

   return APR_EOK;
}

/*
  Register callback with mpd service. This call-back will get invoked
  for every received message.
 
  @param pfn_rx_cb callback function
*/
APR_INTERNAL int32_t aprv2_mpd_set_rx_cb(aprv2_mpd_cb_fn_t pfn_rx_cb)
{

   /* TODO: Add code to store the callback function
   */

   return APR_EOK;
}

static int aprv2_mpd_qdi_invocation
(
   int client_handle,
   qurt_qdi_obj_t *obj,
   int method,
   qurt_qdi_arg_t a1,
   qurt_qdi_arg_t a2,
   qurt_qdi_arg_t a3,
   qurt_qdi_arg_t a4,
   qurt_qdi_arg_t a5,
   qurt_qdi_arg_t a6,
   qurt_qdi_arg_t a7,
   qurt_qdi_arg_t a8,
   qurt_qdi_arg_t a9
)
{
   aprv2_mpd_uapr_t *puapr = (aprv2_mpd_uapr_t *)obj;
   int ret = APR_EOK;

   switch (method)
   {
      case QDI_OPEN:

         ret = aprv2_mpd_allocate_uapr(&puapr);
         if (APR_EOK != ret)
         {
             return -1;
         }

         puapr->qdiobj.invoke = aprv2_mpd_qdi_invocation;
         puapr->qdiobj.refcnt = QDI_REFCNT_INIT;
         puapr->qdiobj.release = aprv2_mpd_qdi_release;

         puapr->domain_id = 0x4; //TODO: Temporary fix. Should be a1.num;

         /* Creating signal to inform uAPR on Rx packets
         */
         puapr->rx_signal =
                 (qurt_signal_t *) qurt_qdi_user_malloc(client_handle,
                                                sizeof(*puapr->rx_signal));
         if (NULL == puapr->rx_signal)
         {
            aprv2_mpd_free_uapr(puapr);
            return -2;
         }

         qurt_signal_init(puapr->rx_signal);

         /* Create a handle for the object and return
         */
         ret = qurt_qdi_handle_create_from_obj_t(client_handle,
                                               (qurt_qdi_obj_t *)puapr);
         if (ret < 0)
         {
            qurt_signal_destroy(puapr->rx_signal);
            aprv2_mpd_free_uapr(puapr);
         }

         return ret;  /* Returning the handle obtained */

      case APRV2_MPD_CMDID_GET_RX_SIGNAL:
      {
         qurt_signal_t ** ret_signal;
         
         ret = qurt_qdi_buffer_lock(client_handle, a1.ptr,
                 sizeof(qurt_signal_t **), QDI_PERM_W, (void **)&ret_signal);
         if (0 == ret)
         {
             *ret_signal = puapr->rx_signal;
             ret = APR_EOK;
         }
         else
         {
             ret = APR_EFAILED;
         }

         return ret;
      }

      case APRV2_MPD_CMDID_REGISTER:
      case APRV2_MPD_CMDID_DEREGISTER:

         /*
          * Register/De-register are no-op for now as each uAPR is assigned one
          * or more unique domaind Ids. No two uAPRs shall have the same domain
          * Id.
          * In future, this information shall be maintained and used
          * for service id level up/down notifications.
          *
         */

         return ret;

      case APRV2_MPD_CMDID_ASYNC_SEND:
      {
         /*
          * Check if the destination belongs to one of the uAPRs.
          * If so, try to send it to the uAPR. Otherwise, give it to kAPR core.
         */
         aprv2_packet_t *packet, *packet_copy;
         uint32_t size, packet_size, alloc_type;

         size = a2.num;

         ret = qurt_qdi_buffer_lock(client_handle,
                               a1.ptr, size, QDI_PERM_R, (void **)&packet);
         if (0 == ret)
         {
            /* Allocate a packet, copy and send
            */
            packet_size = APRV2_PKT_GET_PACKET_BYTE_SIZE( packet->header );

            alloc_type =
                ((APR_GET_FIELD(APRV2_PKT_MSGTYPE, packet->header)
                == APRV2_PKT_MSGTYPE_CMDRSP_V) ? APRV2_ALLOC_TYPE_RESPONSE_RAW:
                                                APRV2_ALLOC_TYPE_COMMAND_RAW );

            ret = __aprv2_cmd_alloc(packet->src_addr,
                                        alloc_type, packet_size, &packet_copy);
            if (APR_EOK != ret)
               return ret;

            memcpy(packet_copy, packet, packet_size);
            ret = __aprv2_cmd_async_send(packet_copy->src_addr, packet_copy);
         }
         else
         {
             ret = APR_EFAILED;
         }

         return ret;
      }

      case APRV2_MPD_CMDID_PEEK_RX_PACKET:
      {
         aprv2_packet_t *ret_packet_header;
         aprv2_mpd_packet_item_t *packet_item;

       //if (apr_list_raw_is_empty(&puapr->rx_packet_q))
         if (puapr->rx_packet_q.dummy.next == &puapr->rx_packet_q.dummy)
            return APR_ENOTEXIST;

         ret = qurt_qdi_buffer_lock(client_handle, a1.ptr,
                 sizeof(aprv2_packet_t), QDI_PERM_W, (void **)&ret_packet_header);

         if (0 == ret)
         {
            packet_item = (aprv2_mpd_packet_item_t *)puapr->rx_packet_q.dummy.next;
            memcpy(ret_packet_header, packet_item->packet, sizeof(aprv2_packet_t));
            ret = APR_EOK;
         }
         else
         {
             ret = APR_EFAILED;
         }

         return ret;
      }

      case APRV2_MPD_CMDID_READ_RX_PACKET:
      {
         aprv2_packet_t *packet, *ret_packet;
         uint32_t size, packet_size, *ret_bytes_copied;
         aprv2_mpd_packet_item_t *ppacket_item;

         if (puapr->rx_packet_q.dummy.next == &puapr->rx_packet_q.dummy)
            return APR_ENOTEXIST;

         size = a2.num;
         ret = qurt_qdi_buffer_lock(client_handle,
                               a1.ptr, size, QDI_PERM_W, (void **)&ret_packet);

         if (0 == ret)
         {
            apr_list_remove_head(&puapr->rx_packet_q, (apr_list_node_t **)&ppacket_item);
            packet = ppacket_item->packet;
            packet_size = APRV2_PKT_GET_PACKET_BYTE_SIZE(packet->header);

            memcpy(ret_packet, packet, packet_size);

            __aprv2_cmd_free(packet->dst_addr, packet);
            ppacket_item->packet = NULL;
            aprv2_mpd_free_packet_item(ppacket_item);

            ret = qurt_qdi_buffer_lock(client_handle,
                               a3.ptr, sizeof(uint32_t), QDI_PERM_W, (void **)&ret_bytes_copied);
            if (0 == ret)
               *ret_bytes_copied = packet_size;

            ret = APR_EOK;
         }
         else
         {
             ret = APR_EFAILED;
         }

         return ret;
      }

      default:
         return -1;
   }
}

static void aprv2_mpd_qdi_release(qurt_qdi_obj_t *qdiobj)
{
   aprv2_mpd_uapr_t *puapr = (aprv2_mpd_uapr_t *) qdiobj;

   if (NULL != puapr)
   {
      qurt_signal_destroy(puapr->rx_signal);
      aprv2_mpd_free_uapr(puapr);
   }

   return;
}

static int32_t aprv2_mpd_allocate_uapr(aprv2_mpd_uapr_t **ppuapr)
{
   int ret = APR_EOK;
   aprv2_mpd_uapr_item_t *puapr_item = NULL;

   if (NULL == ppuapr)
       return APR_EBADPARAM;

   ret = apr_list_remove_head(&aprv2_mpd_free_uapr_q, (apr_list_node_t**) &puapr_item);
   if (APR_EOK == ret)
   {
       apr_list_add_tail(&aprv2_mpd_used_uapr_q, &puapr_item->link);
       puapr_item->uapr.nodeptr = (struct aprv2_mpd_uapr_item_s *)puapr_item;
       *ppuapr = &puapr_item->uapr;
   }

   return ret;
}

static int32_t aprv2_mpd_free_uapr(aprv2_mpd_uapr_t *puapr)
{
   int ret = APR_EOK;
   aprv2_mpd_uapr_item_t *puapr_item = NULL;

   if (NULL == puapr)
       return APR_EBADPARAM;

   puapr_item = (aprv2_mpd_uapr_item_t *)puapr->nodeptr;

   ret = apr_list_delete(&aprv2_mpd_used_uapr_q, (apr_list_node_t *)puapr_item);
   if (APR_EOK == ret)
   {
       apr_list_add_tail(&aprv2_mpd_free_uapr_q, &puapr_item->link);
   }

   return ret;
}

static int32_t aprv2_mpd_allocate_packet_item(aprv2_mpd_packet_item_t **pppacket_item)
{
   int ret = APR_EOK;
   aprv2_mpd_packet_item_t *ppacket_item = NULL;

   if (NULL == pppacket_item)
       return APR_EBADPARAM;

   ret = apr_list_remove_head(&aprv2_mpd_free_packet_q, (apr_list_node_t**) &ppacket_item);
   if (APR_EOK == ret)
   {
     //apr_list_add_tail(aprv2_mpd_used_packet_q, &ppacket_item->link);
       *pppacket_item = ppacket_item;
   }

   return ret;
}

static int32_t aprv2_mpd_free_packet_item(aprv2_mpd_packet_item_t *ppacket_item)
{
   int ret = APR_EOK;

   if (NULL == ppacket_item)
       return APR_EBADPARAM;

 //ret = apr_list_delete(aprv2_mpd_used_packet_q, (apr_list_node_t *)ppacket_item);
 //if (APR_EOK == ret)
 //{
       apr_list_add_tail(&aprv2_mpd_free_packet_q, &ppacket_item->link);
 //}

   return ret;
}
