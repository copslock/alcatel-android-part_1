#include "DALStdDef.h" 
#include "DALReg.h" 

#include"DALSysTypes.h"
extern DALREG_DriverInfo DALSlimBus_DriverInfo;
extern DALREG_DriverInfo DALSTMTrace_DriverInfo;
extern DALREG_DriverInfo DALSTMCfg_DriverInfo;
extern DALREG_DriverInfo DALGPIOInt_DriverInfo;
extern DALREG_DriverInfo DALInterruptController_DriverInfo;

static DALREG_DriverInfo * DALDriverInfoArr[] = {
	& DALSlimBus_DriverInfo,
	& DALSTMTrace_DriverInfo,
	& DALSTMCfg_DriverInfo,
	& DALGPIOInt_DriverInfo,
	& DALInterruptController_DriverInfo,
};

DALREG_DriverInfoList gDALModDriverInfoList = {5, DALDriverInfoArr}; 



StringDevice DAL_Mod_driver_list[] = {
			{NULL}

};

DALProps DAL_Mod_Info = {NULL, 0 ,0, DAL_Mod_driver_list};
