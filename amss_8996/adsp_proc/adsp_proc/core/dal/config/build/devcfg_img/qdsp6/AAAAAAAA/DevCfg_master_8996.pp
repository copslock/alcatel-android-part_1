# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/dal/config/build/devcfg_img/qdsp6/AAAAAAAA/DevCfg_master_8996.xml"
# 1 "<built-in>" 1
# 1 "<built-in>" 3
# 140 "<built-in>" 3
# 1 "<command line>" 1
# 1 "<built-in>" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/dal/config/build/devcfg_img/qdsp6/AAAAAAAA/DevCfg_master_8996.xml" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALPropDef.h" 1
# 2 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/dal/config/build/devcfg_img/qdsp6/AAAAAAAA/DevCfg_master_8996.xml" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALDeviceId.h" 1
# 3 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/dal/config/build/devcfg_img/qdsp6/AAAAAAAA/DevCfg_master_8996.xml" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/dal/config/dalconfig.h" 1
# 4 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/dal/config/build/devcfg_img/qdsp6/AAAAAAAA/DevCfg_master_8996.xml" 2

<?xml version="1.0"?>
<dal>
<module name="modem">
<driver name="I2C">
   <device id=0x02000025>
   </device>
   <device id=0x020000C6>
   </device>
   <device id=0x020000CA>
   </device>
</driver>

<driver name="NULL">
   <global_def>
   </global_def>
   <device id="/dev/ABTimeout">
     <props name="ABT_Propdata" type=0x00000012>
        ABT_propdata
     </props>
   </device>
</driver>

<!--
FILE: icbcfg.xml
DESCRIPTION: This file contains the DAL XML properties for the upper layer
              ICB Configuration Driver
                       Edit History
$Header:
$DateTime: 2014/09/29 10:50:00 $
$Author: pwbldsvc $
$Change: 6677711 $
when who what, where, why
==============================================================================
2013/07/16 sds Update copyright to reflect QTI ownership.
2012/04/25 dj Checked in
Copyright (c) 2013 Qualcomm Technologies, Inc.
All Rights Reserved.
Qualcomm Technologies, Inc. Confidential and Proprietary.
-->
<driver name="AxiCfg">
<device id="/core/buses/icb/arb">
<props name="icb_cfg_bus_count" type=0x00000012>
u32cfg_bus_list_size
</props>
<props name="icb_cfg_master_count" type=0x00000012>
u32cfg_master_list_size
</props>
<props name="icb_cfg_slave_count" type=0x00000012>
u32cfg_slave_list_size
</props>
<props name="icb_cfg_bus_list" type=0x00000012>
cfg_bus_list
</props>
<props name="icb_cfg_master_list" type=0x00000012>
cfg_master_list
</props>
<props name="icb_cfg_slave_list" type=0x00000012>
cfg_slave_list
</props>
<props name="icb_arb_clk_count" type=0x00000012>
u32ul_arb_clk_node_list_size
</props>
<props name="icb_arb_master_count" type=0x00000012>
u32ul_arb_master_list_size
</props>
<props name="icb_arb_slave_count" type=0x00000012>
u32ul_arb_slave_list_size
</props>
<props name="icb_arb_master_list" type=0x00000012>
ul_arb_master_list
</props>
<props name="icb_arb_slave_list" type=0x00000012>
ul_arb_slave_list
</props>
<props name="icb_arb_route_tree" type=0x00000012>
route_tree_root
</props>
<props name="icb_arb_clock_node_list" type=0x00000012>
ul_arb_clock_node_list
</props>
<props name="icb_cfg_clk_count" type=0x00000012>
u32ul_cfg_clk_node_list_size
</props>
<props name="icb_cfg_clock_node_list" type=0x00000012>
ul_cfg_clock_node_list
</props>
<props name="icb_cfg_tcsr_info" type=0x00000012>
cfg_tcsr_info
</props>
</device>
</driver>

enum_header_path "DDITlmm.h"
enum_header_path "DalDevice.h"
enum_header_path "DALStdDef.h"
enum_header_path "DALDeviceId.h"
enum_header_path "DALStdErr.h"
enum_header_path "com_dtypes.h"
enum_header_path "TlmmDefs.h"
enum_header_path "DDIUart.h"
enum_header_path "icbarb.h"
enum_header_path "npa_resource.h"
enum_header_path "npa.h"
enum_header_path "DALSys.h"
enum_header_path "DALSysTypes.h"
enum_header_path "DALReg.h"
enum_header_path "icbid.h"
typedef unsigned long int uint32;
typedef unsigned short uint16;
typedef unsigned char uint8;
typedef signed long int int32;
typedef signed short int16;
typedef signed char int8;
typedef unsigned long long uint64;
typedef long long int64;
typedef unsigned char byte;
typedef uint32 DALBOOL;
typedef uint32 DALDEVICEID;
typedef uint32 DalPowerCmd;
typedef uint32 DalPowerDomain;
typedef uint32 DalSysReq;
typedef uint32 DALHandle;
typedef int DALResult;
typedef void * DALEnvHandle;
typedef void * DALSYSEventHandle;
typedef uint32 DALMemAddr;
typedef uint32 DALSYSMemAddr;
typedef uint64 DALSYSPhyAddr;
typedef uint32 DALInterfaceVersion;
typedef unsigned char * DALDDIParamPtr;
typedef struct DALEventObject DALEventObject;
struct DALEventObject
{
    uint32 obj[8];
};
typedef DALEventObject * DALEventHandle;
typedef struct _DALMemObject
{
   uint32 memAttributes;
   uint32 sysObjInfo[2];
   uint32 dwLen;
   uint32 ownerVirtAddr;
   uint32 virtAddr;
   uint32 physAddr;
}
DALMemObject;
typedef struct _DALDDIMemBufDesc
{
   uint32 dwOffset;
   uint32 dwLen;
   uint32 dwUser;
}
DALDDIMemBufDesc;
typedef struct _DALDDIMemDescList
{
   uint32 dwFlags;
   uint32 dwNumBufs;
   DALDDIMemBufDesc bufList[1];
}
DALDDIMemDescList;
typedef struct DALSysMemDescBuf DALSysMemDescBuf;
struct DALSysMemDescBuf
{
   DALSYSMemAddr VirtualAddr;
   DALSYSMemAddr PhysicalAddr;
   uint32 size;
   uint32 user;
};
typedef struct { uint32 dwObjInfo; DALSYSMemAddr thisVirtualAddr; DALSYSMemAddr PhysicalAddr; DALSYSMemAddr VirtualAddr; uint32 hOwnerProc; uint32 dwCurBufIdx; uint32 dwNumDescBufs; DALSysMemDescBuf BufInfo[1];} DALSysMemDescList;
typedef struct DalDeviceInfo DalDeviceInfo;
struct DalDeviceInfo
{
   uint32 sizeOfActual;
   uint32 Version;
   char Name[32];
};
typedef struct DalDeviceStatus DalDeviceStatus;
struct DalDeviceStatus
{
   uint32 sizeOfActual;
   uint32 Status;
};
typedef struct DalDeviceHandle DalDeviceHandle;
typedef struct DalDevice DalDevice;
struct DalDevice
{
   DALResult (*Attach)(const char*,DALDEVICEID,DalDeviceHandle **);
   uint32 (*Detach)(DalDeviceHandle *);
   DALResult (*Init)(DalDeviceHandle *);
   DALResult (*DeInit)(DalDeviceHandle *);
   DALResult (*Open)(DalDeviceHandle *, uint32);
   DALResult (*Close)(DalDeviceHandle *);
   DALResult (*Info)(DalDeviceHandle *, DalDeviceInfo *, uint32);
   DALResult (*PowerEvent)(DalDeviceHandle *, DalPowerCmd, DalPowerDomain);
   DALResult (*SysRequest)(DalDeviceHandle *, DalSysReq, const void *, uint32,
                           void *,uint32, uint32*);
};
typedef struct DalInterface DalInterface;
struct DalInterface
{
   struct DalDevice DalDevice;
};
struct DalDeviceHandle
{
   uint32 dwDalHandleId;
   const DalInterface *pVtbl;
   void *pClientCtxt;
   uint32 dwVtblLen;
};
typedef struct DalDeviceHandle * DALDEVICEHANDLE;
typedef struct DalRemoteHandle DalRemoteHandle;
typedef struct DalRemote DalRemote;
struct DalRemote
{
   struct DalDevice DalDevice;
   DALResult (* FCN_0) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1 );
   DALResult (* FCN_1) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32 u2 );
   DALResult (* FCN_2) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32* p_u2 );
   DALResult (* FCN_3) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32 u2, uint32 u3 );
   DALResult (* FCN_4) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32 u2, uint32* p_u3 );
   DALResult (* FCN_5) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen);
   DALResult (* FCN_6) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * ibuf, uint32 ilen);
   DALResult (* FCN_7) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * obuf, uint32 olen, uint32 *oalen);
   DALResult (* FCN_8) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * obuf, uint32 olen);
   DALResult (* FCN_9) ( uint32 ddi_idx, DalDeviceHandle *h, void * obuf, uint32 olen );
   DALResult (* FCN_10)( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * ibuf, uint32 ilen, void * obuf, uint32 olen, uint32 * oalen);
   DALResult (* FCN_11) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * obuf, uint32 olen);
   DALResult (* FCN_12) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * obuf, uint32 olen, uint32 *oalen);
   DALResult (* FCN_13) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * ibuf2, uint32 ilen2, void * obuf, uint32 olen);
   DALResult (* FCN_14) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * obuf1, uint32 olen, void * obuf2, uint32 olen2, uint32 * oalen);
   DALResult (* FCN_15) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * ibuf2, uint32 ilen2, void * obuf2, uint32 olen2, uint32 *oalen, void * obuf, uint32 olen );
};
struct DalRemoteHandle
{
   uint32 dwDalHandleId;
   const DalRemote *pVtbl;
   void *pClientCtxt;
};
static __inline uint32
DalDevice_Detach(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Detach(_h);
}
static __inline DALResult
DalDevice_Init(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Init(_h);
}
static __inline DALResult
DalDevice_DeInit(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.DeInit(_h);
}
static __inline DALResult
DalDevice_PowerEvent(DalDeviceHandle *_h, DalPowerCmd PowerCmd,
                     DalPowerDomain PowerDomain)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.PowerEvent(_h,PowerCmd,PowerDomain);
}
static __inline DALResult
DalDevice_Open(DalDeviceHandle *_h, uint32 mode)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Open(_h,mode);
}
static __inline DALResult
DalDevice_Close(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Close(_h);
}
static __inline DALResult
DalDevice_Info(DalDeviceHandle *_h, DalDeviceInfo* info, uint32 infoSize)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Info(_h,info,infoSize);
}
static __inline DALResult
DalDevice_SysRequest(DalDeviceHandle *_h, DalSysReq ReqIdx,
                     const unsigned char* SrcBuf, int SrcBufLen,
                     unsigned char* DestBuf, uint32 DestBufLen, uint32* DestBufLenReq)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.SysRequest(_h,ReqIdx,SrcBuf,SrcBufLen,
                                          DestBuf,DestBufLen,DestBufLenReq);
}
DALResult
DAL_DeviceAttach(DALDEVICEID DeviceId,
                 DalDeviceHandle **phDevice);
DALResult
DAL_DeviceAttachLocal(const char *pszArg,DALDEVICEID DeviceId,
                      DalDeviceHandle **phDevice);
DALResult
DAL_DeviceAttachEx(const char *pszArg, DALDEVICEID DeviceId,
               DALInterfaceVersion ClientVersion,DalDeviceHandle **phDevice);
DALResult
DAL_StringDeviceAttachEx(const char *pszArg,
                   const char *pszDevName,
                   DALInterfaceVersion ClientVersion,
                   DalDeviceHandle **phDalDevice);
DALResult
DAL_DeviceAttachRemote(const char *pszArg,
                   DALDEVICEID DevId,
                   DALInterfaceVersion ClientVersion,
                   DalDeviceHandle **phDALDevice);
DALResult
DAL_DeviceDetach(DalDeviceHandle *hDevice);
DALResult
DAL_StringDeviceAttach(const char *pszDevName,DalDeviceHandle **phDalDevice);
typedef unsigned char boolean;
typedef unsigned short word;
typedef unsigned long dword;
typedef unsigned char uint1;
typedef unsigned short uint2;
typedef unsigned long uint4;
typedef signed char int1;
typedef signed short int2;
typedef long int int4;
typedef signed long sint31;
typedef signed short sint15;
typedef signed char sint7;
typedef uint16 UWord16 ;
typedef uint32 UWord32 ;
typedef int32 Word32 ;
typedef int16 Word16 ;
typedef uint8 UWord8 ;
typedef int8 Word8 ;
typedef int32 Vect32 ;
typedef enum
{
   DAL_GPIO_MODE_PRIMARY,
   DAL_GPIO_MODE_IO,
   DAL_PLACEHOLDER_DALGpioModeType = 0x7fffffff
}DalGpioModeType;
typedef enum
{
  DAL_GPIO_INACTIVE = 0,
  DAL_GPIO_ACTIVE = 1,
  DAL_PLACEHOLDER_DALGpioStatusType = 0x7fffffff
}DALGpioStatusType;
typedef enum
{
  DAL_GPIO_OWNER_MASTER,
  DAL_GPIO_OWNER_APPS1,
  DAL_PLACEHOLDER_DALGpioOwnerType = 0x7fffffff
}DALGpioOwnerType;
typedef enum
{
  DAL_GPIO_UNLOCKED = 0,
  DAL_GPIO_LOCKED = 1,
  DAL_PLACEHOLDER_DALGpioLockType = 0x7fffffff
}DALGpioLockType;
typedef enum
{
  DAL_GPIO_CLIENT_PWRDB = 0,
  DAL_PLACEHOLDER_DALGpioClientType = 0x7fffffff
}DALGpioClientType;
typedef enum
{
  DAL_GPIO_INPUT = 0,
  DAL_GPIO_OUTPUT = 1,
  DAL_PLACEHOLDER_DALGpioDirectionType = 0x7fffffff
}DALGpioDirectionType;
typedef enum
{
  DAL_GPIO_NO_PULL = 0,
  DAL_GPIO_PULL_DOWN = 0x1,
  DAL_GPIO_KEEPER = 0x2,
  DAL_GPIO_PULL_UP = 0x3,
  DAL_PLACEHOLDER_DALGpioPullType = 0x7fffffff
}DALGpioPullType;
typedef enum
{
  DAL_GPIO_2MA = 0,
  DAL_GPIO_4MA = 0x1,
  DAL_GPIO_6MA = 0x2,
  DAL_GPIO_8MA = 0x3,
  DAL_GPIO_10MA = 0x4,
  DAL_GPIO_12MA = 0x5,
  DAL_GPIO_14MA = 0x6,
  DAL_GPIO_16MA = 0x7,
  DAL_PLACEHOLDER_DALGpioDriveType = 0x7fffffff
}DALGpioDriveType;
typedef enum
{
  DAL_GPIO_LOW_VALUE,
  DAL_GPIO_HIGH_VALUE,
  DAL_PLACEHOLDER_DALGpioValueType = 0x7fffffff
}DALGpioValueType;
typedef uint32 DALGpioSignalType;
typedef uint32 DALGpioIdType;
typedef enum
{
  DAL_GPIO_SLEEP_CONFIG_ALL,
  DAL_GPIO_SLEEP_CONFIG_APPS,
  DAL_GPIO_SLEEP_CONFIG_NUM_MODES,
  DAL_PLACEHOLDER_DALGpioSleepConfigType = 0x7fffffff
}DALGpioSleepConfigType;
typedef enum
{
  DAL_TLMM_GPIO_DISABLE,
  DAL_TLMM_GPIO_ENABLE,
  DAL_PLACEHOLDER_DALGpioEnableType = 0x7fffffff
}DALGpioEnableType;
typedef enum
{
  DAL_TLMM_USB_PORT_SEL,
  DAL_TLMM_PORT_CONFIG_USB,
  DAL_TLMM_PORT_TSIF,
  DAL_TLMM_AUD_PCM,
  DAL_TLMM_UART1,
  DAL_TLMM_UART3,
  DAL_TLMM_LCDC_CFG,
  DAL_TLMM_KEYSENSE_SC_IRQ,
  DAL_TLMM_KEYSENSE_A9_IRQ,
  DAL_TLMM_TCXO_EN,
  DAL_TLMM_SSBI_PMIC,
  DAL_TLMM_PA_RANGE1,
  DAL_TLMM_SPARE_WR_UART_SEL,
  DAL_TLMM_PAD_ALT_FUNC_SDIO_EN,
  DAL_TLMM_SPARE_WR_TCXO_EN,
  DAL_TLMM_SPARE_WR_PA_ON,
  DAL_TLMM_SDC3_CTL,
  DAL_TLMM_SDC4_CTL,
  DAL_TLMM_UIM1_PAD_CTL,
  DAL_TLMM_UIM2_PAD_CTL,
  DAL_TLMM_NUM_PORTS,
  DAL_PLACEHOLDER_DALGpioPortType = 0x7fffffff
}DALGpioPortType;
typedef struct
{
  DALGpioDirectionType eDirection;
  DALGpioPullType ePull;
  DALGpioDriveType eDriveStrength;
}DalTlmm_GpioConfigIdType;
typedef struct
{
  uint32 nGpioNumber;
  uint32 nFunctionSelect;
  DalTlmm_GpioConfigIdType Settings;
  DALGpioValueType eOutputDrive;
}DalTlmm_GpioIdSettingsType;
typedef struct DalTlmm DalTlmm;
struct DalTlmm
{
   struct DalDevice DalDevice;
   DALResult (*ConfigGpio)(DalDeviceHandle * _h, DALGpioSignalType gpio_config, DALGpioEnableType enable);
   DALResult (*ConfigGpioGroup)(DalDeviceHandle * _h, DALGpioEnableType enable, DALGpioSignalType* gpio_group, uint32 size);
   DALResult (*TristateGpio)(DalDeviceHandle * _h, DALGpioSignalType gpio_config);
   DALResult (*TristateGpioGroup)(DalDeviceHandle * _h, DALGpioSignalType* gpio_group, uint32 size);
   DALResult (*ConfigGpiosForSleep)(DalDeviceHandle * _h, DALGpioSleepConfigType gpio_sleep_config);
   DALResult (*RestoreGpiosFromSleep)(DalDeviceHandle * _h, DALGpioSleepConfigType gpio_sleep_config);
   DALResult (*GetGpioNumber)(DalDeviceHandle * _h, DALGpioSignalType gpio_config, uint32 * number);
   DALResult (*GpioIn)(DalDeviceHandle * _h, DALGpioSignalType gpio_config, DALGpioValueType* value);
   DALResult (*GpioOut)(DalDeviceHandle * _h, DALGpioSignalType gpio_config, DALGpioValueType value);
   DALResult (*GpioOutGroup)(DalDeviceHandle * _h, DALGpioSignalType* gpio_config, uint32 size, DALGpioValueType value);
   DALResult (*SwitchGpioIntOwnership)(DalDeviceHandle * _h, uint32 owner);
   DALResult (*SetTlmmPort)(DalDeviceHandle * _h, DALGpioPortType Port, uint32 value);
   DALResult (*ExternalGpioConfig)(DalDeviceHandle * _h, DALGpioSignalType gpio_config, DALGpioEnableType enable);
   DALResult (*RequestGpio)(DalDeviceHandle * _h, DALGpioSignalType gpio_config);
   DALResult (*ReleaseGpio)(DalDeviceHandle * _h, DALGpioSignalType gpio_config);
   DALResult (*GetGpioOwner)(DalDeviceHandle * _h, uint32 gpio_number, DALGpioOwnerType* owner);
   DALResult (*GetCurrentConfig)(DalDeviceHandle * _h, uint32 gpio_number, DALGpioSignalType* config);
   DALResult (*GetGpioStatus)(DalDeviceHandle * _h, uint32 gpio_number, DALGpioStatusType* status);
   DALResult (*SetInactiveConfig)(DalDeviceHandle * _h, uint32 gpio_number, DALGpioSignalType config);
   DALResult (*GetInactiveConfig)(DalDeviceHandle *h, uint32 gpio_number, DALGpioSignalType* config);
   DALResult (*LockGpio)(DalDeviceHandle * _h, DALGpioClientType client, uint32 gpio_number);
   DALResult (*UnlockGpio)(DalDeviceHandle * _h, DALGpioClientType client, uint32 gpio_number);
   DALResult (*IsGpioLocked)(DalDeviceHandle * _h, uint32 gpio_number, DALGpioLockType* lock);
   DALResult (*GpioBypassSleep)(DalDeviceHandle * _h, uint32 gpio_number, DALGpioEnableType enable);
   DALResult (*AttachRemote)(DalDeviceHandle * _h, uint32 processor);
   DALResult (*GetOutput)(DalDeviceHandle * _h, uint32 gpio_number, DALGpioValueType* value);
   DALResult (*GetGpioId)(DalDeviceHandle * _h, const char* gpio_str, DALGpioIdType* nGpioId);
   DALResult (*ConfigGpioId)(DalDeviceHandle * _h, DALGpioIdType nGpioId, DalTlmm_GpioConfigIdType* tGpioSettings);
   DALResult (*ConfigGpioIdInactive)(DalDeviceHandle * _h, DALGpioIdType nGpioId);
   DALResult (*GpioIdOut)(DalDeviceHandle * _h, DALGpioIdType nGpioId, DALGpioValueType value);
   DALResult (*GpioIdIn)(DalDeviceHandle * _h, DALGpioIdType nGpioId, DALGpioValueType *value);
   DALResult (*ReleaseGpioId)(DalDeviceHandle * h, DALGpioIdType nGpioId);
   DALResult (*SelectGpioIdMode)(DalDeviceHandle * h, DALGpioIdType nGpioId, DalGpioModeType eMode, DalTlmm_GpioConfigIdType* pUserSettings);
   DALResult (*GetGpioIdSettings)(DalDeviceHandle * h, DALGpioIdType nGpioId, DalTlmm_GpioIdSettingsType* pGpioSettings);
   DALResult (*ConfigGpioIdModeIndexed)(DalDeviceHandle * _h, DALGpioIdType nGpioId, uint32 nIndex);
};
typedef struct DalTlmmHandle DalTlmmHandle;
struct DalTlmmHandle
{
   uint32 dwDalHandleId;
   const DalTlmm * pVtbl;
   void * pClientCtxt;
   uint32 dwVtblen;
};
static __inline DALResult
DalTlmm_ConfigGpio(DalDeviceHandle * _h, DALGpioSignalType gpio_config, DALGpioEnableType enable)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_1(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->ConfigGpio)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
        _h, gpio_config, enable);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->ConfigGpio( _h, gpio_config, enable);
}
static __inline DALResult
DalTlmm_ConfigGpioGroup
(
  DalDeviceHandle * _h,
  DALGpioEnableType enable,
  DALGpioSignalType* gpio_group,
  uint32 size
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return
        hRemote->pVtbl->FCN_6(
          ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->ConfigGpioGroup)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
          _h, enable, gpio_group, size*(sizeof(uint32)));
   }
   return
     ((DalTlmmHandle *)_h)->pVtbl->ConfigGpioGroup( _h, enable, gpio_group, size);
}
static __inline DALResult
DalTlmm_TristateGpio
(
  DalDeviceHandle* _h,
  DALGpioSignalType gpio_config
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return
        hRemote->pVtbl->FCN_0(
          ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->TristateGpio)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
          _h, gpio_config);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->TristateGpio( _h,
                                                   (DALGpioSignalType)gpio_config);
}
static __inline DALResult
DalTlmm_TristateGpioGroup
(
  DalDeviceHandle *_h,
  DALGpioSignalType *gpio_group,
  uint32 size
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_1(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->TristateGpioGroup)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
        _h, (uint32)gpio_group, size);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->TristateGpioGroup( _h, gpio_group, size);
}
static __inline DALResult
DalTlmm_ConfigGpiosForSleep
(
  DalDeviceHandle *_h,
  DALGpioSleepConfigType gpio_sleep_config
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_0(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->ConfigGpiosForSleep)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
        _h, gpio_sleep_config);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->ConfigGpiosForSleep( _h,
                                                            gpio_sleep_config);
}
static __inline DALResult
DalTlmm_RestoreGpiosFromSleep
(
  DalDeviceHandle *_h,
  DALGpioSleepConfigType gpio_sleep_config
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_0(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->RestoreGpiosFromSleep)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
        _h,
        gpio_sleep_config);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->RestoreGpiosFromSleep( _h,
                                                      gpio_sleep_config);
}
static __inline DALResult
DalTlmm_GetGpioNumber
(
  DalDeviceHandle *_h,
  DALGpioSignalType gpio_config,
  uint32 *gpio_number
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_2(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->GetGpioNumber)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
        _h, gpio_config, gpio_number);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->GetGpioNumber( _h,
                                                     (DALGpioSignalType)gpio_config,
                                                     gpio_number);
}
static __inline DALResult
DalTlmm_GpioIn
(
  DalDeviceHandle *_h,
  DALGpioSignalType gpio_config,
  DALGpioValueType*value
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_2(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->GpioIn)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
        _h, gpio_config, (uint32*)value);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->GpioIn( _h, gpio_config, value);
}
static __inline DALResult
DalTlmm_GpioOut
(
  DalDeviceHandle *_h,
  DALGpioSignalType gpio_config,
  DALGpioValueType value
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_1(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->GpioOut)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)), _h, gpio_config, value);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->GpioOut( _h, gpio_config, value);
}
static __inline DALResult
DalTlmm_GpioOutGroup
(
  DalDeviceHandle *_h,
  DALGpioSignalType *gpio_group,
  uint32 size,
  DALGpioValueType value
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_1(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->GpioOutGroup)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
        _h, (uint32)gpio_group, value);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->GpioOutGroup( _h,
                                                      gpio_group,
                                                      size, value);
}
static __inline DALResult
DalTlmm_SwitchGpioIntOwnership
(
  DalDeviceHandle *_h,
  uint32 owner
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_0(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->SwitchGpioIntOwnership)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
        _h, owner);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->SwitchGpioIntOwnership( _h, owner);
}
static __inline DALResult
DalTlmm_SetPort
(
  DalDeviceHandle *_h,
  DALGpioPortType port,
  uint32 value
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_1(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->SetTlmmPort)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
        _h, port, value);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->SetTlmmPort( _h, port, value);
}
static __inline DALResult
DalTlmm_ExternalGpioConfig
(
  DalDeviceHandle *_h,
  DALGpioSignalType gpio_config,
  DALGpioEnableType enable
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_1(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->ExternalGpioConfig)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
        _h, gpio_config, enable);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->ExternalGpioConfig( _h,
                                    gpio_config, enable);
}
static __inline DALResult
DalTlmm_RequestGpio
(
  DalDeviceHandle *_h,
  DALGpioSignalType gpio_config
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_0(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->RequestGpio)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)), _h, gpio_config);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->RequestGpio( _h, gpio_config);
}
static __inline DALResult
DalTlmm_ReleaseGpio
(
  DalDeviceHandle *_h,
  DALGpioSignalType gpio_config
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_0(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->ReleaseGpio)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)), _h, gpio_config);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->ReleaseGpio( _h, gpio_config);
}
static __inline DALResult
DalTlmm_GetGpioOwner
(
  DalDeviceHandle *_h,
  uint32 gpio_number,
  DALGpioOwnerType* owner
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_2(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->GetGpioOwner)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
        _h, gpio_number, (uint32*)owner);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->GetGpioOwner( _h, gpio_number, owner);
}
static __inline DALResult
DalTlmm_GetCurrentConfig
(
  DalDeviceHandle *_h,
  uint32 gpio_number,
  DALGpioSignalType *gpio_config
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_2(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->GetCurrentConfig)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
        _h, gpio_number, (uint32*)gpio_config);
   }
   return
     ((DalTlmmHandle *)_h)->pVtbl->GetCurrentConfig( _h, gpio_number, gpio_config);
}
static __inline DALResult
DalTlmm_GetGpioStatus
(
  DalDeviceHandle *_h,
  uint32 gpio_number,
  DALGpioStatusType *status
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_2(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->GetGpioStatus)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
        _h, gpio_number, (uint32*)status);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->GetGpioStatus( _h, gpio_number, status);
}
static __inline DALResult
DalTlmm_SetInactiveConfig
(
  DalDeviceHandle *_h,
  uint32 gpio_number,
  DALGpioSignalType gpio_config
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_1(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->SetInactiveConfig)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
        _h, gpio_number, gpio_config);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->SetInactiveConfig( _h,
                                                           gpio_number,
                                                           gpio_config);
}
static __inline DALResult
DalTlmm_GetInactiveConfig
(
  DalDeviceHandle *_h,
  uint32 gpio_number,
  DALGpioSignalType *gpio_config
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_2(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->GetInactiveConfig)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
        _h, gpio_number, (uint32*)gpio_config);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->GetInactiveConfig( _h,
                                                           gpio_number,
                                                           gpio_config);
}
static __inline DALResult
DalTlmm_LockGpio
(
  DalDeviceHandle *_h,
  DALGpioClientType client,
  uint32 gpio_number
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_1(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->LockGpio)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
        _h, client, gpio_number);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->LockGpio( _h, client, gpio_number);
}
static __inline DALResult
DalTlmm_UnlockGpio
(
  DalDeviceHandle *_h,
  DALGpioClientType client,
  uint32 gpio_number
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_1(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->UnlockGpio)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
        _h, client, gpio_number);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->UnlockGpio( _h, client, gpio_number);
}
static __inline DALResult
DalTlmm_IsGpioLocked(DalDeviceHandle * _h, uint32 gpio_number, DALGpioLockType* lock)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_2(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->IsGpioLocked)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
        _h, gpio_number, (uint32*)lock);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->IsGpioLocked( _h, gpio_number, lock);
}
static __inline DALResult
DalTlmm_GpioBypassSleep
(
  DalDeviceHandle *_h,
  uint32 gpio_number,
  DALGpioEnableType enable
)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_1(
        ((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->GpioBypassSleep)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)),
        _h, gpio_number, enable);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->GpioBypassSleep( _h, gpio_number, enable);
}
static __inline DALResult
DalTlmm_AttachRemote(DalDeviceHandle * _h, uint32 processor)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_0(((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->AttachRemote)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)), _h, processor);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->AttachRemote( _h, processor);
}
static __inline DALResult
DalTlmm_GetOutput(DalDeviceHandle * _h, uint32 gpio_number, DALGpioValueType* value)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_2(((uint32 *)&((((DalTlmmHandle *)_h)->pVtbl)->GetOutput)-(uint32 *)(((DalTlmmHandle *)_h)->pVtbl)), _h, gpio_number, (uint32*)value);
   }
   return ((DalTlmmHandle *)_h)->pVtbl->GetOutput( _h, gpio_number, value);
}
static __inline DALResult
DalTlmm_GetGpioId(DalDeviceHandle * _h, const char* pszGpio, DALGpioIdType* pnGpioId)
{
   return ((DalTlmmHandle *)_h)->pVtbl->GetGpioId( _h, pszGpio, pnGpioId);
}
static __inline DALResult
DalTlmm_ConfigGpioId(DalDeviceHandle * _h, DALGpioIdType nGpioId, DalTlmm_GpioConfigIdType* pUserSettings)
{
   return ((DalTlmmHandle *)_h)->pVtbl->ConfigGpioId( _h, nGpioId, pUserSettings);
}
static __inline DALResult
DalTlmm_ConfigGpioIdInactive(DalDeviceHandle * _h, DALGpioIdType nGpioId)
{
   return ((DalTlmmHandle *)_h)->pVtbl->ConfigGpioIdInactive( _h, nGpioId);
}
static __inline DALResult
DalTlmm_GpioIdOut(DalDeviceHandle * _h, DALGpioIdType nGpioId, DALGpioValueType eValue)
{
   return ((DalTlmmHandle *)_h)->pVtbl->GpioIdOut( _h, nGpioId, eValue);
}
static __inline DALResult
DalTlmm_GpioIdIn(DalDeviceHandle * _h, DALGpioIdType nGpioId, DALGpioValueType *eValue)
{
   return ((DalTlmmHandle *)_h)->pVtbl->GpioIdIn( _h, nGpioId, eValue);
}
static __inline DALResult
DalTlmm_ReleaseGpioId( DalDeviceHandle * _h, DALGpioIdType nGpioId)
{
   return ((DalTlmmHandle *)_h)->pVtbl->ReleaseGpioId(_h, nGpioId);
}
static __inline DALResult
DalTlmm_SelectGpioIdMode( DalDeviceHandle * _h, DALGpioIdType nGpioId, DalGpioModeType eMode, DalTlmm_GpioConfigIdType* pUserSettings)
{
   return ((DalTlmmHandle *)_h)->pVtbl->SelectGpioIdMode(_h, nGpioId, eMode, pUserSettings);
}
static __inline DALResult
DalTlmm_GetGpioIdSettings( DalDeviceHandle * _h, DALGpioIdType nGpioId, DalTlmm_GpioIdSettingsType* pGpioSettings)
{
   return ((DalTlmmHandle *)_h)->pVtbl->GetGpioIdSettings(_h, nGpioId, pGpioSettings);
}
static __inline DALResult
DalTlmm_ConfigGpioIdModeIndexed( DalDeviceHandle * _h, DALGpioIdType nGpioId, uint32 nIndex )
{
   return ((DalTlmmHandle *)_h)->pVtbl->ConfigGpioIdModeIndexed(_h, nGpioId, nIndex);
}
typedef enum
{
    POWER_INVALID = 0,
    POWER_FULL = (1<<0),
    POWER_075 = (1<<1),
    POWER_050 = (1<<2),
    POWER_025 = (1<<3),
    POWER_OFF = (1<<4),
    _QIDL_PLACEHOLDER_UartPowerType = 0x12345678
} UartPowerType;
typedef enum
{
    EVENT_NONE = 0,
    EVENT_BREAK = (1<<0),
    EVENT_CTS = (1<<1),
    EVENT_DSR = (1<<3),
    EVENT_ERROR = (1<<4),
    EVENT_RX_WAKEUP = (1<<5),
    EVENT_RING = (1<<7),
    EVENT_RLSD = (1<<8),
    EVENT_RX_CHAR = (1<<9),
    EVENT_RX_HUNTCHAR = (1<<10),
    EVENT_TXEMPTY = (1<<11),
    EVENT_BREAK_END = (1<<12),
    _QIDL_PLACEHOLDER_UartLineEventType = 0x12345678
} UartLineEventType;
typedef enum
{
    STATUS_RX_PARITY_ERROR = (1<<0),
    STATUS_RX_FRAME_ERROR = (1<<1),
    STATUS_RX_OVERRUN_ERROR = (1<<2),
    STATUS_RX_BREAK = (1<<3),
    STATUS_RX_HUNT_CHAR_DETECTED = (1<<4),
    STATUS_RX_FULL = (1<<5),
    STATUS_RX_READY = (1<<6),
    STATUS_TX_EMPTY = (1<<7),
    STATUS_TX_READY = (1<<8),
    STATUS_RING_ASSERTED = (1<<16),
    STATUS_RLSD_ASSERTED = (1<<17),
    STATUS_DSR_ASSERTED = (1<<18),
    STATUS_CTS_ASSERTED = (1<<19),
    _QIDL_PLACEHOLDER_UartStatusType = 0x12345678
} UartStatusType;
typedef enum
{
    RTS_DEASSERT,
    RTS_ASSERT,
    RTS_AUTO,
    _QIDL_PLACEHOLDER_UartRtsControlType = 0x12345678
} UartRtsControlType;
typedef enum
{
    CTS_DISABLE,
    CTS_ENABLE,
    _QIDL_PLACEHOLDER_UartCtsControlType = 0x12345678
} UartCtsControlType;
typedef enum
{
  UART_5_BITS_PER_CHAR = (1<<0),
  UART_6_BITS_PER_CHAR = (1<<1),
  UART_7_BITS_PER_CHAR = (1<<2),
  UART_8_BITS_PER_CHAR = (1<<3),
  UART_INVALID_BITS_PER_CHAR = 0x7FFFFFFF
} UartBitsPerCharType;
typedef enum
{
  UART_0_5_STOP_BITS = (1<<0),
  UART_1_0_STOP_BITS = (1<<1),
  UART_1_5_STOP_BITS = (1<<2),
  UART_2_0_STOP_BITS = (1<<3),
  UART_INVALID_STOP_BITS = 0x7FFFFFFF
} UartNumStopBitsType;
typedef enum
{
  UART_NO_PARITY = (1<<0),
  UART_ODD_PARITY = (1<<1),
  UART_EVEN_PARITY = (1<<2),
  UART_SPACE_PARITY = (1<<3),
  UART_INVALID_PARITY = 0x7FFFFFFF
} UartParityModeType;
typedef struct
{
   UartParityModeType ParityMode;
   UartBitsPerCharType BitsPerChar;
   UartNumStopBitsType NumStopBits;
} UartCharFormat;
typedef struct
{
   void *uart_irq;
   void *rx_irq;
   uint32 enable_dma;
   void *Ctxt;
   void (*LineEventNotif)(void *Ctxt, uint32 LineEvents);
   void (*TransmitReadyNotif)(void *Ctxt);
   void (*ReceiveReadyNotif)(void *Ctxt);
} UartInitConfig;
typedef struct
{
   uint32 baud_rate;
   UartCharFormat char_format;
   UartRtsControlType rts_control;
   UartCtsControlType cts_control;
} UartOpenConfig;
typedef struct
{
   DalDevice DalDevice;
   DALResult (*PostInit) (DalDeviceHandle *h, UartInitConfig *init_cfg);
   DALResult (*OpenEx) (DalDeviceHandle *h, UartOpenConfig *open_cfg);
   DALResult (*Read) (DalDeviceHandle *h, uint8 *read_buffer, uint32 buffer_len,
                                                        uint32 *bytes_read, uint32 *overrun_events);
   DALResult (*Write) (DalDeviceHandle *h, const uint8 *write_buffer,
                                                        uint32 buffer_len, uint32 *bytes_written);
   DALResult (*SetRate) (DalDeviceHandle *h, uint32 baud_rate);
   DALResult (*SetCharFormat) (DalDeviceHandle *h, UartCharFormat *char_format);
   DALResult (*PurgeTx) (DalDeviceHandle *h);
   DALResult (*PurgeRx) (DalDeviceHandle *h);
   DALResult (*SetBreak) (DalDeviceHandle *h);
   DALResult (*ClearBreak) (DalDeviceHandle *h);
   DALResult (*TxSingleChar) (DalDeviceHandle *h, uint8 character);
   DALResult (*GetStatus) (DalDeviceHandle *h, uint32 *status);
   DALResult (*TxComplete) (DalDeviceHandle *h, uint32 *tx_complete);
   DALResult (*SetRtsControl) (DalDeviceHandle *h, UartRtsControlType rts_control);
   DALResult (*SetCtsControl) (DalDeviceHandle *h, UartCtsControlType cts_control);
   DALResult (*GetCTS) (DalDeviceHandle *h, uint32 *cts_state);
   DALResult (*PowerCapabilities) (DalDeviceHandle *h, uint32 *power_mask);
   DALResult (*PowerGet) (DalDeviceHandle *h, UartPowerType *power_state);
   DALResult (*PowerSet) (DalDeviceHandle *h, UartPowerType power_state);
   DALResult (*LoopbackSet) (DalDeviceHandle *h, uint32 enabled);
   DALResult (*SetCxm) (DalDeviceHandle *h, uint32 enabled, uint32 sam);
   DALResult (*DumpRegs) (DalDeviceHandle *h);
   DALResult (*GetCxmTxSticky) (DalDeviceHandle *h, uint32 *tx_sticky, uint32 clear);
   DALResult (*CxmTxDirectChar) (DalDeviceHandle *h, uint8 character);
   DALResult (*RxActive) (DalDeviceHandle *h, uint32 *rx_active);
} UartInterface;
static __inline DALResult
DalUart_PostInit(DalDeviceHandle *h, UartInitConfig *init_cfg)
{
   return ((UartInterface *)h->pVtbl)->PostInit(h, init_cfg);
}
static __inline DALResult
DalUart_OpenEx(DalDeviceHandle *h, UartOpenConfig *open_cfg)
{
   return ((UartInterface *)h->pVtbl)->OpenEx(h, open_cfg);
}
static __inline DALResult
DalUart_Read(DalDeviceHandle *h, uint8 *read_buffer, uint32 buffer_len,
             uint32 *bytes_read, uint32 *overrun_events)
{
   return ((UartInterface *)h->pVtbl)->Read(h, read_buffer, buffer_len, bytes_read, overrun_events);
}
static __inline DALResult
DalUart_Write(DalDeviceHandle *h, const uint8 *write_buffer, uint32 buffer_len,
              uint32 *bytes_written)
{
   return ((UartInterface *)h->pVtbl)->Write(h, write_buffer, buffer_len, bytes_written);
}
static __inline DALResult
DalUart_SetRate(DalDeviceHandle *h, uint32 baud_rate)
{
   return ((UartInterface *)h->pVtbl)->SetRate(h, baud_rate);
}
static __inline DALResult
DalUart_SetCharFormat(DalDeviceHandle *h, UartCharFormat *char_format)
{
   return ((UartInterface *)h->pVtbl)->SetCharFormat(h, char_format);
}
static __inline DALResult
DalUart_PurgeTx(DalDeviceHandle *h)
{
   return ((UartInterface *)h->pVtbl)->PurgeTx(h);
}
static __inline DALResult
DalUart_PurgeRx(DalDeviceHandle *h)
{
   return ((UartInterface *)h->pVtbl)->PurgeRx(h);
}
static __inline DALResult
DalUart_SetBreak(DalDeviceHandle *h)
{
   return ((UartInterface *)h->pVtbl)->SetBreak(h);
}
static __inline DALResult
DalUart_ClearBreak(DalDeviceHandle *h)
{
   return ((UartInterface *)h->pVtbl)->ClearBreak(h);
}
static __inline DALResult
DalUart_TxSingleChar(DalDeviceHandle *h, uint8 character)
{
   return ((UartInterface *)h->pVtbl)->TxSingleChar(h, character);
}
static __inline DALResult
DalUart_GetStatus(DalDeviceHandle *h, uint32 *status)
{
   return ((UartInterface *)h->pVtbl)->GetStatus(h, status);
}
static __inline DALResult
DalUart_TxComplete(DalDeviceHandle *h, uint32 *tx_complete)
{
   return ((UartInterface *)h->pVtbl)->TxComplete(h, tx_complete);
}
static __inline DALResult
DalUart_SetRtsControl(DalDeviceHandle *h, UartRtsControlType rts_control)
{
   return ((UartInterface *)h->pVtbl)->SetRtsControl(h, rts_control);
}
static __inline DALResult
DalUart_SetCtsControl(DalDeviceHandle *h, UartCtsControlType cts_control)
{
   return ((UartInterface *)h->pVtbl)->SetCtsControl(h, cts_control);
}
static __inline DALResult
DalUart_GetCTS(DalDeviceHandle *h, uint32 *cts_state)
{
   return ((UartInterface *)h->pVtbl)->GetCTS(h, cts_state);
}
static __inline DALResult
DalUart_PowerCapabilities(DalDeviceHandle *h, uint32 *power_mask)
{
   return ((UartInterface *)h->pVtbl)->PowerCapabilities(h, power_mask);
}
static __inline DALResult
DalUart_PowerGet(DalDeviceHandle *h, UartPowerType *power_state)
{
   return ((UartInterface *)h->pVtbl)->PowerGet(h, power_state);
}
static __inline DALResult
DalUart_PowerSet(DalDeviceHandle *h, UartPowerType power_state)
{
   return ((UartInterface *)h->pVtbl)->PowerSet(h, power_state);
}
static __inline DALResult
DalUart_LoopbackSet(DalDeviceHandle *h, uint32 enabled)
{
   return ((UartInterface *)h->pVtbl)->LoopbackSet(h, enabled);
}
static __inline DALResult
DalUart_SetCxm(DalDeviceHandle *h, uint32 enabled, uint32 sam)
{
   return ((UartInterface *)h->pVtbl)->SetCxm(h, enabled, sam);
}
static __inline DALResult
DalUart_DumpRegs(DalDeviceHandle *h)
{
   return ((UartInterface *)h->pVtbl)->DumpRegs(h);
}
static __inline DALResult
DalUart_GetCxmTxSticky(DalDeviceHandle *h, uint32 *tx_sticky, uint32 clear)
{
   return ((UartInterface *)h->pVtbl)->GetCxmTxSticky(h, tx_sticky, clear);
}
static __inline DALResult
DalUart_CxmTxDirectChar(DalDeviceHandle *h, uint8 character)
{
   return ((UartInterface *)h->pVtbl)->CxmTxDirectChar(h, character);
}
static __inline DALResult
DalUart_RxActive(DalDeviceHandle *h, uint32 *rx_active)
{
   return ((UartInterface *)h->pVtbl)->RxActive(h, rx_active);
}
typedef struct DALREG_DriverInfo DALREG_DriverInfo;
struct DALREG_DriverInfo
{
 DALResult (*pfnDALNewFunc)(const char * , DALDEVICEID, DalDeviceHandle**);
 uint32 dwNumDevices;
 DALDEVICEID *pDeviceId;
};
typedef struct DALREG_DriverInfoList DALREG_DriverInfoList;
struct DALREG_DriverInfoList
{
 uint32 dwLen;
 DALREG_DriverInfo ** pDriverInfo;
};
typedef void * DALSYSObjHandle;
typedef void * DALSYSSyncHandle;
typedef void * DALSYSMemHandle;
typedef void * DALSYSWorkLoopHandle;
typedef uint32 *DALSYSPropertyHandle;
typedef struct DALSYSPropertyVar DALSYSPropertyVar;
struct DALSYSPropertyVar
{
    uint32 dwType;
    uint32 dwLen;
    union
    {
        byte *pbVal;
        char *pszVal;
        uint32 dwVal;
        uint32 *pdwVal;
        const void *pStruct;
    }Val;
};
typedef struct DALSYSPropStructTblType DALSYSPropStructTblType ;
struct DALSYSPropStructTblType{
   uint32 dwSize;
   const void *pStruct;
};
typedef struct StringDevice StringDevice;
 struct StringDevice{
   char *pszName;
   uint32 dwHash;
   uint32 dwOffset;
   DALREG_DriverInfo *pFunctionName;
   uint32 dwNumCollision;
   uint32 *pdwCollisions;
};
typedef struct DALProps DALProps;
struct DALProps
{
   const byte *pDALPROP_PropBin;
   const DALSYSPropStructTblType *pDALPROP_StructPtrs;
   const uint32 dwDeviceSize;
   const StringDevice *pDevices;
};
typedef struct DALSYSBaseObj DALSYSBaseObj;
struct DALSYSBaseObj
{
   uint32 dwObjInfo;
   uint32 hOwnerProc;
   DALSYSMemAddr thisVirtualAddr;
};
typedef struct DALSYSEventObj DALSYSEventObj;
struct DALSYSEventObj
{
   unsigned long long _bSpace[80/sizeof(unsigned long long)];
};
typedef struct DALSYSSyncObj DALSYSSyncObj;
struct DALSYSSyncObj
{
   unsigned long long _bSpace[40/sizeof(unsigned long long)];
};
typedef struct DALSYSMemObj DALSYSMemObj;
struct DALSYSMemObj
{
   unsigned long long _bSpace[40/sizeof(unsigned long long)];
};
typedef struct DALSYSWorkLoopEventObj DALSYSWorkLoopEventObj;
struct DALSYSWorkLoopEventObj
{
   unsigned long long _bSpace[32/sizeof(unsigned long long)];
};
typedef struct DALSYSWorkLoopObj DALSYSWorkLoopObj;
struct DALSYSWorkLoopObj
{
   unsigned long long _bSpace[64/sizeof(unsigned long long)];
};
typedef void * DALSYSCallbackFuncCtx;
typedef void * (*DALSYSCallbackFunc)(void*,uint32,void*,uint32);
typedef struct DALSYSMemInfo DALSYSMemInfo;
struct DALSYSMemInfo
{
    DALSYSMemAddr VirtualAddr;
    DALSYSMemAddr PhysicalAddr;
    uint32 dwLen;
    uint32 dwMappedLen;
    uint32 dwProps;
};
typedef enum
{
   DALSYS_MEM_CACHE_DEVICE,
   DALSYS_MEM_CACHE_NONE,
   DALSYS_MEM_CACHE_WRITEBACK,
   DALSYS_MEM_CACHE_WRITETHROUGH,
   DALSYS_MEM_CACHE_INVALID
}
DALSYSMemCacheType;
typedef enum
{
   DALSYS_MEM_PERM_NONE=0,
   DALSYS_MEM_PERM_R=0x1,
   DALSYS_MEM_PERM_W=0x2,
   DALSYS_MEM_PERM_RW=0x3,
   DALSYS_MEM_PERM_X=0x4,
   DALSYS_MEM_PERM_RX=0x5,
   DALSYS_MEM_PERM_WX=0x6,
   DALSYS_MEM_PERM_RWX=0x7,
   DALSYS_MEM_PERM_INVALID=0x8
}
DALSYSMemPermType;
typedef enum
{
   DALSYS_MEM_SHARE_ALLOWED,
   DALSYS_MEM_SHARE_NOT_ALLOWED,
   DALSYS_MEM_SHARE_INVALID
}
DALSYSMemShareType;
typedef struct DALSYSMemInfoEx DALSYSMemInfoEx;
struct DALSYSMemInfoEx
{
    DALSYSPhyAddr physicalAddr;
    DALSYSMemAddr virtualAddr;
    DALSYSMemAddr size;
    DALSYSMemCacheType cacheType;
    DALSYSMemPermType permission;
    DALSYSMemShareType shareType;
};
typedef struct DALSYSMemReq DALSYSMemReq;
struct DALSYSMemReq
{
    DALSYSMemInfoEx memInfo;
    DALSYSMemObj *pObj;
    DALBOOL prealloc;
};
typedef enum
{
   DEVCFG_TARGET_INFO_SOC,
   DEVCFG_TARGET_INFO_PLATFORM
}DEVCFG_TARGET_INFO_TYPE;
typedef DALResult
(*DALSYSWorkLoopExecute)
(
    DALSYSEventHandle,
    void *
);
typedef void
(*DALSYS_InitSystemHandleFncPtr)
(
    DalDeviceHandle * hDalDevice
);
typedef DALResult
(*DALSYS_DestroyObjectFncPtr)
(
    DALSYSObjHandle
);
typedef DALResult
(*DALSYS_CopyObjectFncPtr)
(
    DALSYSObjHandle,
    DALSYSObjHandle *
);
typedef DALResult
(*DALSYS_RegisterWorkLoopFncPtr)
(
    uint32,
    uint32,
    DALSYSWorkLoopHandle *,
    DALSYSWorkLoopObj *
);
typedef DALResult
(*DALSYS_RegisterWorkLoopExFncPtr)
(
    char *,
    uint32,
    uint32,
    uint32,
    DALSYSWorkLoopHandle *,
    DALSYSWorkLoopObj *
);
typedef DALResult
(*DALSYS_AddEventToWorkLoopFncPtr)
(
    DALSYSWorkLoopHandle,
    DALSYSWorkLoopExecute,
    void *,
    DALSYSEventHandle,
    DALSYSSyncHandle
);
typedef DALResult
(*DALSYS_DeleteEventFromWorkLoopFncPtr)
(
    DALSYSWorkLoopHandle,
    DALSYSEventHandle
);
typedef DALResult
(*DALSYS_EventCreateFncPtr)
(
    uint32 ,
    DALSYSEventHandle *,
    DALSYSEventObj *
);
typedef DALResult
(*DALSYS_EventCtrlFncPtr)
(
    DALSYSEventHandle,
    uint32,
   uint32,
   void *,
   uint32
);
typedef DALResult
(*DALSYS_EventWaitFncPtr)
(
    DALSYSEventHandle
);
typedef DALResult
(*DALSYS_EventMultipleWaitFncPtr)
(
    DALSYSEventHandle*,
    int,
    uint32,
    uint32 *
);
typedef DALResult
(*DALSYS_SetupCallbackEventFncPtr)
(
    DALSYSEventHandle,
    DALSYSCallbackFunc,
    DALSYSCallbackFuncCtx
);
typedef DALResult
(*DALSYS_SyncCreateFncPtr)
(
    uint32,
    DALSYSSyncHandle *,
    DALSYSSyncObj *
);
typedef void
(*DALSYS_SyncEnterFncPtr)
(
    DALSYSSyncHandle
);
typedef DALResult
(*DALSYS_SyncTryEnterFncPtr)
(
    DALSYSSyncHandle
);
typedef void
(*DALSYS_SyncLeaveFncPtr)
(
    DALSYSSyncHandle
);
typedef DALResult
(*DALSYS_MemRegionAllocFncPtr)
(
    uint32,
    DALSYSMemAddr,
    DALSYSMemAddr,
    uint32,
    DALSYSMemHandle *,
    DALSYSMemObj *
);
typedef DALResult
(*DALSYS_MemRegionMapPhysFncPtr)
(
    DALSYSMemHandle,
    uint32,
    DALSYSMemAddr,
    uint32
);
typedef DALResult
(*DALSYS_MemInfoFncPtr)
(
    DALSYSMemHandle,
    DALSYSMemInfo *
);
typedef DALResult
(*DALSYS_CacheCommandFncPtr)
(
    uint32 ,
    DALSYSMemAddr,
    uint32
);
typedef DALResult
(*DALSYS_MallocFncPtr)
(
    uint32 ,
    void **
);
typedef DALResult
(*DALSYS_FreeFncPtr)
(
    void *
);
typedef void
(*DALSYS_BusyWaitFncPtr)
(
   uint32
);
typedef DALResult
(*DALSYS_MemDescAddBufFncPtr)
(
    DALSysMemDescList *,
    uint32,
    uint32,
    uint32
);
typedef DALResult
(*DALSYS_MemDescPrepareFncPtr)
(
    DALSysMemDescList *,
    uint32
);
typedef DALResult
(*DALSYS_MemDescValidateFncPtr)
(
    DALSysMemDescList *,
    uint32
);
typedef DALResult
(*DALSYS_GetDALPropertyHandleFncPtr)
(
    DALDEVICEID,
    DALSYSPropertyHandle
);
typedef DALResult
(*DAL_DeviceAttachFncPtr)
(
    const char *pszArg,
    DALDEVICEID,
    DalDeviceHandle **
);
typedef DALResult
(*DAL_DeviceDetachFncPtr)
(
    DalDeviceHandle *
);
typedef DALResult
(*DAL_DeviceAttachExFncPtr)
(
 const char *pszArg,
    DALDEVICEID DevId,
    DALInterfaceVersion ClientVersion,
    DalDeviceHandle **phDalDevice
);
typedef DALResult
(*DALSYS_GetPropertyValueFncPtr)
(
    DALSYSPropertyHandle,
    const char *,
    uint32,
    DALSYSPropertyVar *
);
typedef void
(*DALSYS_LogEventFncPtr)
(
    DALDEVICEID,
    uint32,
    const char *
);
typedef struct DALSYSFncPtrTbl DALSYSFncPtrTbl;
struct DALSYSFncPtrTbl
{
    DALSYS_InitSystemHandleFncPtr DALSYS_InitSystemHandleFnc;
    DALSYS_DestroyObjectFncPtr DALSYS_DestroyObjectFnc;
    DALSYS_CopyObjectFncPtr DALSYS_CopyObjectFnc;
    DALSYS_RegisterWorkLoopFncPtr DALSYS_RegisterWorkLoopFnc;
    DALSYS_RegisterWorkLoopExFncPtr DALSYS_RegisterWorkLoopExFnc;
    DALSYS_AddEventToWorkLoopFncPtr DALSYS_AddEventToWorkLoopFnc;
    DALSYS_DeleteEventFromWorkLoopFncPtr DALSYS_DeleteEventFromWorkLoopFnc;
    DALSYS_EventCreateFncPtr DALSYS_EventCreateFnc;
    DALSYS_EventCtrlFncPtr DALSYS_EventCtrlFnc;
    DALSYS_EventWaitFncPtr DALSYS_EventWaitFnc;
    DALSYS_EventMultipleWaitFncPtr DALSYS_EventMultipleWaitFnc;
    DALSYS_SetupCallbackEventFncPtr DALSYS_SetupCallbackEventFnc;
    DALSYS_SyncCreateFncPtr DALSYS_SyncCreateFnc;
    DALSYS_SyncEnterFncPtr DALSYS_SyncEnterFnc;
    DALSYS_SyncTryEnterFncPtr DALSYS_SyncTryEnterFnc;
    DALSYS_SyncLeaveFncPtr DALSYS_SyncLeaveFnc;
    DALSYS_MemRegionAllocFncPtr DALSYS_MemRegionAllocFnc;
    DALSYS_MemRegionMapPhysFncPtr DALSYS_MemRegionMapPhysFnc;
    DALSYS_MemInfoFncPtr DALSYS_MemInfoFnc;
    DALSYS_CacheCommandFncPtr DALSYS_CacheCommandFnc;
    DALSYS_MallocFncPtr DALSYS_MallocFnc;
    DALSYS_FreeFncPtr DALSYS_FreeFnc;
    DALSYS_BusyWaitFncPtr DALSYS_BusyWaitFnc;
    DALSYS_MemDescAddBufFncPtr DALSYS_MemDescAddBufFnc;
    DALSYS_MemDescPrepareFncPtr DALSYS_MemDescPrepareFnc;
    DALSYS_MemDescValidateFncPtr DALSYS_MemDescValidateFnc;
    DALSYS_GetDALPropertyHandleFncPtr DALSYS_GetDALPropertyHandleFnc;
    DAL_DeviceAttachFncPtr DALSYS_DeviceAttachFnc;
    DAL_DeviceAttachExFncPtr DALSYS_DeviceAttachExFnc;
    DAL_DeviceAttachExFncPtr DALSYS_DeviceAttachRemoteFnc;
    DAL_DeviceDetachFncPtr DALSYS_DeviceDetachFnc;
    DALSYS_GetPropertyValueFncPtr DALSYS_GetPropertyValueFnc;
    DALSYS_LogEventFncPtr DALSYS_LogEventFnc;
};
typedef DALResult
(*DALRemote_NewFncPtr)(const char *, DALDEVICEID, DalDeviceHandle **);
typedef int
(*DALRemote_CommonInitFncPtr)(void);
typedef void
(*DALRemote_IPCInitFcnPtr)(uint32);
typedef int
(*DALRemote_InitFncPtr)(void);
typedef DALSYSEventHandle
(*DALRemote_CreateEventFncPtr)(void *pClientCtxt, DALSYSEventHandle hRemote);
typedef uint32
(*DALRemoteInterProcessCallFncPtr)(void *in_buf, void *out_buf);
typedef int
(*DALRemote_DeinitFncPtr)(void);
typedef struct DALRemoteVtbl DALRemoteVtbl;
struct DALRemoteVtbl
{
   DALRemote_NewFncPtr DALRemote_NewFnc;
   DALRemote_CommonInitFncPtr DALRemote_CommonInitFnc;
   DALRemote_InitFncPtr DALRemote_InitFnc;
   DALRemote_DeinitFncPtr DALRemote_DeinitFnc;
   DALRemote_CreateEventFncPtr DALRemote_CreateEventFnc;
   DALRemote_CreateEventFncPtr DALRemote_CreatePayloadEventFnc;
   DALRemoteInterProcessCallFncPtr DALRemoteInterProcessCallFnc;
   DALRemote_IPCInitFcnPtr DALRemote_IPCInitFcn;
};
typedef struct DALSYSConfig DALSYSConfig;
struct DALSYSConfig
{
    void *pNativeEnv;
    uint32 dwConfig;
   DALRemoteVtbl *pRemoteVtbl;
};
typedef long _Int32t;
typedef unsigned long _Uint32t;
typedef int _Ptrdifft;
typedef unsigned int _Sizet;
typedef __builtin_va_list va_list;
typedef long long _Longlong;
typedef unsigned long long _ULonglong;
typedef int _Wchart;
typedef int _Wintt;
typedef va_list _Va_list;
void _Atexit(void (*)(void));
typedef char _Sysch_t;
void _Locksyslock(int);
void _Unlocksyslock(int);
typedef unsigned short __attribute__((__may_alias__)) alias_short;
static alias_short *strict_aliasing_workaround(unsigned short *ptr) __attribute__((always_inline,unused));
static alias_short *strict_aliasing_workaround(unsigned short *ptr)
{
  alias_short *aliasptr = (alias_short *)ptr;
  return aliasptr;
}
typedef _Sizet size_t;
int memcmp(const void *, const void *, size_t) __attribute__((__nothrow__));
void *memcpy(void *, const void *, size_t) __attribute__((__nothrow__));
void *memcpy_v(volatile void *, const volatile void *, size_t) __attribute__((__nothrow__));
void *memset(void *, int, size_t) __attribute__((__nothrow__));
char *strcat(char *, const char *) __attribute__((__nothrow__));
int strcmp(const char *, const char *) __attribute__((__nothrow__));
char *strcpy(char *, const char *) __attribute__((__nothrow__));
size_t strlen(const char *) __attribute__((__nothrow__));
void *memmove(void *, const void *, size_t) __attribute__((__nothrow__));
void *memmove_v(volatile void *, const volatile void *, size_t) __attribute__((__nothrow__));
int strcoll(const char *, const char *) __attribute__((__nothrow__));
size_t strcspn(const char *, const char *) __attribute__((__nothrow__));
char *strerror(int) __attribute__((__nothrow__));
size_t strlcat(char *, const char *, size_t) __attribute__((__nothrow__));
char *strncat(char *, const char *, size_t) __attribute__((__nothrow__));
int strncmp(const char *, const char *, size_t) __attribute__((__nothrow__));
size_t strlcpy(char *, const char *, size_t) __attribute__((__nothrow__));
char *strncpy(char *, const char *, size_t) __attribute__((__nothrow__));
size_t strspn(const char *, const char *) __attribute__((__nothrow__));
char *strtok(char *, const char *) __attribute__((__nothrow__));
char *strsep(char **, const char *) __attribute__((__nothrow__));
size_t strxfrm(char *, const char *, size_t) __attribute__((__nothrow__));
char *strdup(const char *) __attribute__((__nothrow__));
int strcasecmp(const char *, const char *) __attribute__((__nothrow__));
int strncasecmp(const char *, const char *, size_t) __attribute__((__nothrow__));
char *strtok_r(char *, const char *, char **) __attribute__((__nothrow__));
void *memccpy (void *, const void *, int, size_t) __attribute__((__nothrow__));
int strerror_r (int, char *, size_t) __attribute__((__nothrow__));
char *strchr(const char *, int) __attribute__((__nothrow__));
char *strpbrk(const char *, const char *) __attribute__((__nothrow__));
char *strrchr(const char *, int) __attribute__((__nothrow__));
char *strstr(const char *, const char *) __attribute__((__nothrow__));
void *memchr(const void *, int, size_t) __attribute__((__nothrow__));
void
DALSYS_InitMod(DALSYSConfig * pCfg);
void
DALSYS_DeInitMod(void);
void
DALSYS_InitSystemHandle(DalDeviceHandle *hDalDevice);
DALSYS_LogEventFncPtr
DALSYS_SetLogCfg(uint32 dwMaxLogLevel, DALSYS_LogEventFncPtr DALSysLogFcn);
DALResult
DALSYS_DestroyObject(DALSYSObjHandle hObj);
DALResult
DALSYS_CopyObject(DALSYSObjHandle hObjOrig, DALSYSObjHandle *phObjCopy );
DALResult
DALSYS_RegisterWorkLoop(uint32 dwPriority,
                  uint32 dwMaxNumEvents,
                  DALSYSWorkLoopHandle *phWorkLoop,
                  DALSYSWorkLoopObj *pWorkLoopObj);
DALResult
DALSYS_RegisterWorkLoopEx(
                  char * pszname,
                  uint32 dwStackSize,
                  uint32 dwPriority,
                  uint32 dwMaxNumEvents,
                  DALSYSWorkLoopHandle *phWorkLoop,
                  DALSYSWorkLoopObj *pWorkLoopObj);
DALResult
DALSYS_AddEventToWorkLoop(DALSYSWorkLoopHandle hWorkLoop,
                    DALSYSWorkLoopExecute pfnWorkLoopExecute,
                    void * pArg,
                    DALSYSEventHandle hEvent,
                    DALSYSSyncHandle hSync);
DALResult
DALSYS_DeleteEventFromWorkLoop(DALSYSWorkLoopHandle hWorkLoop,
                         DALSYSEventHandle hEvent);
DALResult
DALSYS_EventCreate(uint32 dwAttribs, DALSYSEventHandle *phEvent,
               DALSYSEventObj *pEventObj);
DALResult
DALSYS_EventCopy(DALSYSEventHandle hEvent,
             DALSYSEventHandle *phEventCopy,DALSYSEventObj *pEventObjCopy,
                 uint32 dwMarshalFlags);
DALResult
DALSYS_EventCtrlEx(DALSYSEventHandle hEvent, uint32 dwCtrl, uint32 dwParam,
                   void *pPayload, uint32 dwPayloadSize);
DALResult
DALSYS_EventWait(DALSYSEventHandle hEvent);
DALResult
DALSYS_EventMultipleWait(DALSYSEventHandle* phEvent, int nEvents,
                         uint32 dwTimeoutUs,uint32 *pdwEventIdx);
DALResult
DALSYS_SetupCallbackEvent(DALSYSEventHandle hEvent, DALSYSCallbackFunc cbFunc,
                          DALSYSCallbackFuncCtx cbFuncCtx);
DALResult DALSYS_TimerStart( DALSYSEventHandle hEvent, uint32 time);
DALResult DALSYS_TimerStop( DALSYSEventHandle hEvent );
DALResult
DALSYS_SyncCreate(uint32 dwAttribs,
                  DALSYSSyncHandle *phSync,
                  DALSYSSyncObj *pSyncObj);
void
DALSYS_SyncEnter(DALSYSSyncHandle hSync);
DALResult
DALSYS_SyncTryEnter(DALSYSSyncHandle hSync);
void
DALSYS_SyncLeave(DALSYSSyncHandle hSync);
DALResult
DALSYS_MemRegionAlloc(uint32 dwAttribs, DALSYSMemAddr VirtualAddr,
                      DALSYSMemAddr PhysicalAddr, uint32 dwLen,
                      DALSYSMemHandle *phMem, DALSYSMemObj *pMemObj);
DALResult
DALSYS_MemRegionAllocEx( DALSYSMemHandle *phMem, const DALSYSMemReq *pMemReq,
      DALSYSMemInfoEx *pMemInfo );
DALResult
DALSYS_MemRegionMapPhys(DALSYSMemHandle hMem, uint32 dwVirtualBaseOffset,
                        DALSYSMemAddr PhysicalAddr, uint32 dwLen);
DALResult
DALSYS_MemInfo(DALSYSMemHandle hMem, DALSYSMemInfo *pMemInfo);
DALResult
DALSYS_MemInfoEx(DALSYSMemHandle hMem, DALSYSMemInfoEx *pMemInfo);
DALResult
DALSYS_CacheCommand(uint32 CacheCmd, DALSYSMemAddr VirtualAddr, uint32 dwLen);
DALResult
DALSYS_Malloc(uint32 dwSize, void **ppMem);
DALResult
DALSYS_Free(void *pmem);
void
DALSYS_BusyWait(uint32 pause_time_us);
DALResult
DALSYS_GetDALPropertyHandle(DALDEVICEID DeviceId, DALSYSPropertyHandle hDALProps);
DALResult
DALSYS_GetDALPropertyHandleEx(DALDEVICEID DeviceId,DALSYSPropertyHandle hDALProps,
                              DEVCFG_TARGET_INFO_TYPE target_info_type);
DALResult
DALSYS_GetDALPropertyHandleDyn(DALDEVICEID DeviceId, DALSYSPropertyHandle hDALProps);
DALResult
DALSYS_GetDALPropertyHandleStr(const char *pszDevName, DALSYSPropertyHandle hDALProps);
DALResult
DALSYS_GetDALPropertyHandleStrEx(const char *pszDevName, DALSYSPropertyHandle hDALProps,
                                 DEVCFG_TARGET_INFO_TYPE target_info_type);
DALResult
DALSYS_GetDALPropertyHandleStrDyn(const char *pszDevName, DALSYSPropertyHandle hDALProps);
DALResult
DALSYS_GetPropertyValue(DALSYSPropertyHandle hDALProps, const char *pszName,
                  uint32 dwId,
                   DALSYSPropertyVar *pDALPropVar);
void
DALSYS_LogEvent(DALDEVICEID DeviceId, uint32 dwLogEventType,
      const char * pszFmt, ...);
DALRemoteVtbl *
DALSYS_GetRemoteInterfaceVtbl(void);
uint32 DALSYS_SetThreadPriority(uint32 priority);
uint32 _DALSYS_memscpy(void * pDest, uint32 iDestSz,
      const void * pSrc, uint32 iSrcSize);
typedef enum
{
  NPA_SUCCESS = 0,
  NPA_ERROR = -1,
} npa_status;
typedef enum
{
  NPA_NO_CLIENT = 0x7fffffff,
  NPA_CLIENT_RESERVED1 = (1 << 0),
  NPA_CLIENT_RESERVED2 = (1 << 1),
  NPA_CLIENT_CUSTOM1 = (1 << 2),
  NPA_CLIENT_CUSTOM2 = (1 << 3),
  NPA_CLIENT_CUSTOM3 = (1 << 4),
  NPA_CLIENT_CUSTOM4 = (1 << 5),
  NPA_CLIENT_REQUIRED = (1 << 6),
  NPA_CLIENT_ISOCHRONOUS = (1 << 7),
  NPA_CLIENT_IMPULSE = (1 << 8),
  NPA_CLIENT_LIMIT_MAX = (1 << 9),
  NPA_CLIENT_VECTOR = (1 << 10),
  NPA_CLIENT_SUPPRESSIBLE = (1 << 11),
  NPA_CLIENT_SUPPRESSIBLE_VECTOR = (1 << 12),
  NPA_CLIENT_CUSTOM5 = (1 << 13),
  NPA_CLIENT_CUSTOM6 = (1 << 14),
  NPA_CLIENT_SUPPRESSIBLE2 = (1 << 15),
  NPA_CLIENT_SUPPRESSIBLE2_VECTOR = (1 << 16),
} npa_client_type;
typedef enum
{
  NPA_TRIGGER_RESERVED_EVENT_0,
  NPA_TRIGGER_RESERVED_EVENT_1,
  NPA_TRIGGER_RESERVED_EVENT_2,
  NPA_TRIGGER_CHANGE_EVENT,
  NPA_TRIGGER_WATERMARK_EVENT,
  NPA_TRIGGER_THRESHOLD_EVENT,
  NPA_TRIGGER_CUSTOM_EVENT1 = 0x010,
  NPA_TRIGGER_CUSTOM_EVENT2 = 0x020,
  NPA_TRIGGER_CUSTOM_EVENT3 = 0x040,
  NPA_TRIGGER_CUSTOM_EVENT4 = 0x080,
  NPA_TRIGGER_PRE_CHANGE_EVENT = 0x0100,
  NPA_TRIGGER_POST_CHANGE_EVENT = 0x0200,
  NPA_TRIGGER_NAS_REQUEST_ACTIVE_EVENT = 0x0400,
  NPA_TRIGGER_MAX_EVENT = 0x0000ffff,
  NPA_TRIGGER_RESERVED_BIT_30 = 0x40000000,
  NPA_TRIGGER_RESERVED_BIT_31 = ( int )0x80000000
} npa_event_trigger_type;
typedef enum
{
  NPA_EVENT_RESERVED,
  NPA_EVENT_RESERVED_1,
  NPA_EVENT_RESERVED_2,
  NPA_EVENT_HI_WATERMARK,
  NPA_EVENT_LO_WATERMARK,
  NPA_EVENT_CHANGE,
  NPA_EVENT_THRESHOLD_LO,
  NPA_EVENT_THRESHOLD_NOMINAL,
  NPA_EVENT_THRESHOLD_HI,
  NPA_EVENT_CUSTOM1 = 0x010,
  NPA_EVENT_CUSTOM2 = 0x020,
  NPA_EVENT_CUSTOM3 = 0x040,
  NPA_EVENT_CUSTOM4 = 0x080,
  NPA_EVENT_PRE_CHANGE = 0x0100,
  NPA_EVENT_POST_CHANGE = 0x0200,
  NPA_EVENT_NAS_REQUEST_ACTIVE = 0x0400,
  NPA_NUM_EVENT_TYPES
} npa_event_type;
typedef enum
{
  NPA_REQUEST_DEFAULT = 0,
  NPA_REQUEST_FIRE_AND_FORGET = 0x00000001,
  NPA_REQUEST_NEXT_AWAKE = 0x00000002,
  NPA_REQUEST_CHANGED_TYPE = 0x00000004,
  NPA_REQUEST_BEST_EFFORT = 0x00000008,
  NPA_REQUEST_RESERVED_ATTRIBUTE1 = 0x00000010,
  NPA_REQUEST_MULTIPLE_NEXT_AWAKE = 0x00000020,
} npa_request_attribute;
enum {
  NPA_QUERY_CURRENT_STATE,
  NPA_QUERY_CLIENT_ACTIVE_REQUEST,
  NPA_QUERY_ACTIVE_MAX,
  NPA_QUERY_RESOURCE_MAX,
  NPA_QUERY_RESOURCE_DISABLED,
  NPA_QUERY_RESOURCE_LATENCY,
  NPA_QUERY_CURRENT_AGGREGATION,
  NPA_MAX_PUBLIC_QUERY = 1023,
  NPA_QUERY_RESERVED_BEGIN = 1024,
  NPA_QUERY_REMOTE_RESOURCE_AVAILABLE = 1025,
  NPA_QUERY_RESERVED_END = 4095
};
typedef enum {
  NPA_QUERY_SUCCESS = 0,
  NPA_QUERY_UNSUPPORTED_QUERY_ID,
  NPA_QUERY_UNKNOWN_RESOURCE,
  NPA_QUERY_NULL_POINTER,
  NPA_QUERY_NO_VALUE,
} npa_query_status;
typedef unsigned int npa_resource_state;
typedef int npa_resource_state_delta;
typedef uint32 npa_time_sclk;
typedef npa_time_sclk npa_resource_time;
typedef void ( *npa_callback )( void *context,
                                unsigned int event_type,
                                void *data,
                                unsigned int data_size );
typedef void ( *npa_simple_callback )( void *context );
typedef struct npa_client * npa_client_handle;
typedef struct npa_event * npa_event_handle;
typedef struct npa_custom_event * npa_custom_event_handle;
typedef struct npa_event_data
{
  const char *resource_name;
  npa_resource_state state;
  npa_resource_state_delta headroom;
} npa_event_data;
typedef struct npa_prepost_change_data
{
  const char *resource_name;
  npa_resource_state from_state;
  npa_resource_state to_state;
  void *data;
} npa_prepost_change_data;
typedef struct npa_link * npa_query_handle;
typedef enum
{
   NPA_QUERY_TYPE_STATE,
   NPA_QUERY_TYPE_VALUE,
   NPA_QUERY_TYPE_NAME,
   NPA_QUERY_TYPE_REFERENCE,
   NPA_QUERY_TYPE_VALUE64,
   NPA_QUERY_TYPE_STATE_VECTOR,
   NPA_QUERY_NUM_TYPES
} npa_query_type_enum;
typedef struct npa_query_type
{
  npa_query_type_enum type;
  union
  {
    npa_resource_state state;
    unsigned int value;
    unsigned long long value64;
    char *name;
    void *reference;
    npa_resource_state *state_vector;
  } data;
} npa_query_type;
npa_client_handle npa_create_sync_client_ex( const char *resource_name,
                                             const char *client_name,
                                             npa_client_type client_type,
                                             unsigned int client_value,
                                             void *client_ref );
npa_client_handle
npa_create_async_client_cb_ex( const char *resource_name,
                               const char *client_name,
                               npa_client_type client_type,
                               npa_callback callback,
                               void *context,
                               unsigned int client_value,
                               void *client_ref );
void npa_destroy_client( npa_client_handle client );
void npa_issue_scalar_request( npa_client_handle client,
                               npa_resource_state state );
void npa_modify_required_request( npa_client_handle client,
                                  npa_resource_state_delta delta );
void npa_issue_scalar_request_unconditional( npa_client_handle client,
                                             npa_resource_state state );
void npa_issue_impulse_request( npa_client_handle client );
void npa_issue_vector_request( npa_client_handle client,
                               unsigned int num_elems,
                               npa_resource_state *vector );
void npa_issue_isoc_request( npa_client_handle client,
                             npa_resource_time deadline,
                             npa_resource_state level_hint );
void npa_issue_limit_max_request( npa_client_handle client,
                                  npa_resource_state max );
void npa_complete_request( npa_client_handle client );
void npa_cancel_request( npa_client_handle client );
void npa_set_request_attribute( npa_client_handle client,
                                npa_request_attribute attr );
void npa_set_client_type_required( npa_client_handle client );
void npa_set_client_type_suppressible( npa_client_handle client );
npa_event_handle
npa_create_event_cb( const char *resource_name,
                     const char *event_name,
                     npa_event_trigger_type trigger_type,
                     npa_callback callback,
                     void *context );
void npa_set_event_watermarks( npa_event_handle event,
                               npa_resource_state_delta hi_watermark,
                               npa_resource_state_delta lo_watermark );
void npa_set_event_thresholds( npa_event_handle event,
                               npa_resource_state lo_threshold,
                               npa_resource_state hi_threshold );
npa_event_handle npa_create_custom_event( const char *resource_name,
                                          const char *event_name,
                                          unsigned int trigger_type,
                                          void *reg_data,
                                          npa_callback callback,
                                          void *context );
void npa_destroy_custom_event( npa_event_handle event );
void npa_destroy_event_handle( npa_event_handle event );
npa_query_handle npa_create_query_handle( const char * resource_name );
void npa_destroy_query_handle( npa_query_handle query );
npa_query_status npa_query( npa_query_handle query,
                            uint32 query_id,
                            npa_query_type *query_result );
npa_query_status npa_query_by_name( const char *resource_name,
                                    uint32 query_id,
                                    npa_query_type *query_result );
npa_query_status npa_query_by_client( npa_client_handle client,
                                      uint32 query_id,
                                      npa_query_type *query_result );
npa_query_status npa_query_by_event( npa_event_handle event,
                                     uint32 query_id,
                                     npa_query_type *query_result );
npa_query_status npa_query_resource_available( const char *resource_name );
void npa_resources_available_cb( unsigned int num_resources,
                                 const char *resources[],
                                 npa_callback callback,
                                 void *context );
void npa_resource_available_cb( const char *resource_name,
                                npa_callback callback,
                                void *context );
void npa_dal_event_callback( void *dal_event_handle,
                             unsigned int event_type,
                             void *data,
                             unsigned int data_size );
typedef enum
{
  NPA_FORK_DEFAULT = 0,
  NPA_FORK_DISALLOWED,
  NPA_FORK_ALLOWED,
} npa_fork_pref;
typedef unsigned int (*npa_join_function) ( void *, void * );
void npa_set_client_fork_pref( npa_client_handle client,
                               npa_fork_pref pref );
npa_fork_pref npa_get_client_fork_pref( npa_client_handle client );
void npa_join_request( npa_client_handle client );
typedef enum
{
  NPA_RESOURCE_DEFAULT = 0,
  NPA_RESOURCE_DRIVER_UNCONDITIONAL = 0x00000001,
  NPA_RESOURCE_SINGLE_CLIENT = 0x00000002,
  NPA_RESOURCE_VECTOR_STATE = 0x00000004,
  NPA_RESOURCE_REMOTE_ACCESS_ALLOWED = 0x00000008,
  NPA_RESOURCE_INTERPROCESS_ACCESS_ALLOWED = 0x00000008,
  NPA_RESOURCE_REMOTE = 0x00000010,
  NPA_RESOURCE_REMOTE_PROXY = 0x00000020,
  NPA_RESOURCE_REMOTE_NO_INIT = 0x00000040,
  NPA_RESOURCE_SUPPORTS_SUPPRESSIBLE = 0x00000080,
  NPA_RESOURCE_DRIVER_UNCONDITIONAL_FIRST = 0x00000200,
  NPA_RESOURCE_LPR_ISSUABLE = 0x00000400,
  NPA_RESOURCE_ALLOW_CLIENT_TYPE_CHANGE = 0x00000800,
  NPA_RESOURCE_ENABLE_PREPOST_CHANGE_EVENTS = 0x00001000,
} npa_resource_attribute;
typedef enum
{
  NPA_NODE_DEFAULT = 0,
  NPA_NODE_NO_LOCK = 0x00000001,
  NPA_NODE_DISABLEABLE = 0x00000002,
  NPA_NODE_FORKABLE = 0x00000004,
} npa_node_attribute;
enum
{
  NPA_RESOURCE_AUTHOR_QUERY_START = NPA_MAX_PUBLIC_QUERY,
  NPA_QUERY_RESOURCE_ATTRIBUTES,
  NPA_QUERY_NODE_ATTRIBUTES,
  NPA_MAX_RESOURCE_AUTHOR_QUERY = 2047
};
typedef void* npa_user_data;
typedef struct npa_event_callback
{
  npa_callback callback;
  npa_user_data context;
} npa_event_callback;
typedef struct npa_work_request
{
  npa_resource_state state;
  union
  {
    npa_resource_state *vector;
    char *string;
    void *reference;
  } pointer;
} npa_work_request;
typedef struct npa_client
{
  struct npa_client *prev, *next;
  const char *name;
  const char *resource_name;
  struct npa_resource *resource;
  npa_user_data resource_data;
  npa_client_type type;
  npa_work_request work[3];
  unsigned int index;
  unsigned int sequence;
  struct npa_async_client_data *async;
  void *log_handle;
  unsigned int request_attr;
  void (*issue_request)( npa_client_handle client, int new_request );
  struct npa_scheduler_data *request_ptr;
} npa_client;
typedef struct npa_event
{
  struct npa_event *prev, *next;
  unsigned int trigger_type;
  const char *name;
  struct npa_resource *resource;
  union
  {
    npa_resource_state_delta watermark;
    npa_resource_state threshold;
  } lo;
  union
  {
    npa_resource_state_delta watermark;
    npa_resource_state threshold;
  } hi;
  npa_event_callback callback;
  void *reg_data;
  struct npa_event_action *action;
} npa_event;
typedef struct npa_link
{
  struct npa_link *next, *prev;
  const char *name;
  struct npa_resource *resource;
} npa_link;
typedef npa_resource_state (*npa_resource_driver_fcn) (
  struct npa_resource *resource,
  npa_client_handle client,
  npa_resource_state state
  );
typedef npa_resource_state (*npa_resource_update_fcn)(
  struct npa_resource *resource,
  npa_client_handle client
  );
typedef struct npa_resource_latency {
  uint32 request;
  uint32 fork;
  uint32 notify;
} npa_resource_latency;
typedef npa_query_status (*npa_resource_query_fcn)(
  struct npa_resource *resource,
  unsigned int query_id,
  npa_query_type *query_result );
typedef npa_query_status (*npa_link_query_fcn)(
  struct npa_link *resource_link,
  unsigned int query_id,
  npa_query_type *query_result );
typedef struct npa_resource_plugin
{
  npa_resource_update_fcn update_fcn;
  unsigned int supported_clients;
  void (*create_client_fcn) ( npa_client * );
  void (*destroy_client_fcn)( npa_client * );
  unsigned int (*create_client_ex_fcn)( npa_client *, unsigned int, void * );
  void (*cancel_client_fcn) ( npa_client *);
} npa_resource_plugin;
typedef struct npa_node_dependency
{
  const char *name;
  npa_client_type client_type;
  npa_client_handle handle;
} npa_node_dependency;
typedef struct npa_resource_definition
{
  const char *name;
  const char *units;
  npa_resource_state max;
  const npa_resource_plugin *plugin;
  unsigned int attributes;
  npa_user_data data;
  npa_resource_query_fcn query_fcn;
  npa_link_query_fcn link_query_fcn;
  struct npa_resource *handle;
} npa_resource_definition;
typedef struct npa_node_definition
{
  const char *name;
  npa_resource_driver_fcn driver_fcn;
  unsigned int attributes;
  npa_user_data data;
  unsigned int dependency_count;
  npa_node_dependency *dependencies;
  unsigned int resource_count;
  npa_resource_definition *resources;
} npa_node_definition;
typedef struct npa_resource
{
  npa_resource_definition *definition;
  npa_node_definition *node;
  unsigned int index;
  npa_client *clients;
  union
  {
    npa_event *creation_events;
    struct npa_event_list *list;
  } events;
  const npa_resource_plugin *active_plugin;
  npa_resource_state request_state;
  npa_resource_state active_state;
  npa_resource_state internal_state[8];
  npa_resource_state *state_vector;
  npa_resource_state *required_state_vector;
  npa_resource_state *suppressible_state_vector;
  npa_resource_state *suppressible2_state_vector;
  npa_resource_state *semiactive_state_vector;
  npa_resource_state active_max;
  npa_resource_state_delta active_headroom;
  struct CoreMutex *node_lock;
  struct CoreMutex *event_lock;
  struct npa_resource_internal_data *_internal;
  unsigned int sequence;
  void *log_handle;
  npa_resource_latency *latency;
} npa_resource;
typedef void* npa_join_token;
void npa_define_node_cb( npa_node_definition *node,
                         npa_resource_state initial_state[],
                         npa_callback node_cb,
                         void *context);
void npa_alias_resource_cb( const char *resource_name,
                            const char *alias_name,
                            npa_callback alias_cb,
                            void *context);
void npa_define_marker( const char *marker_name );
void npa_define_marker_with_attributes( const char *marker_name,
                                        npa_resource_attribute attributes );
void npa_issue_dependency_request( npa_client_handle cur_client,
                                   npa_client_handle req_client,
                                   npa_resource_state req_state,
                                   npa_client_handle sup_client,
                                   npa_resource_state sup_state );
void npa_issue_dependency_vector_request( npa_client_handle cur_client,
                                          npa_client_handle req_client,
                                          unsigned int req_num_elems,
                                          npa_resource_state *req_vector,
                                          npa_client_handle sup_client,
                                          unsigned int sup_num_elems,
                                          npa_resource_state *sup_vector );
void npa_assign_resource_state( npa_resource *resource,
                                npa_resource_state state );
npa_resource *npa_query_get_resource( npa_query_handle query_handle );
void npa_enable_node( npa_node_definition *node, npa_resource_state default_state[] );
void npa_disable_node( npa_node_definition *node );
npa_join_token npa_fork_resource( npa_resource *resource,
                                  npa_join_function join_func,
                                  void *join_data );
void npa_resource_lock( npa_resource *resource );
int npa_resource_trylock( npa_resource *resource );
void npa_resource_unlock( npa_resource *resource );
unsigned int npa_request_has_attribute( npa_client_handle client,
                                        npa_request_attribute attr );
unsigned int npa_get_request_attributes( npa_client_handle client );
npa_client_handle npa_pass_request_attributes( npa_client_handle current,
                                               npa_client_handle dependency );
void npa_change_resource_plugin( const char *resource_name,
                                 const npa_resource_plugin *plugin );
extern const npa_resource_plugin npa_binary_plugin;
extern const npa_resource_plugin npa_max_plugin;
extern const npa_resource_plugin npa_min_plugin;
extern const npa_resource_plugin npa_sum_plugin;
extern const npa_resource_plugin npa_identity_plugin;
extern const npa_resource_plugin npa_always_on_plugin;
extern const npa_resource_plugin npa_impulse_plugin;
extern const npa_resource_plugin npa_or_plugin;
extern const npa_resource_plugin npa_binary_and_plugin;
extern const npa_resource_plugin npa_no_client_plugin;
npa_resource_state npa_min_update_fcn( npa_resource *resource,
                                       npa_client_handle client);
npa_resource_state npa_max_update_fcn( npa_resource *resource,
                                       npa_client_handle client );
npa_resource_state npa_sum_update_fcn( npa_resource *resource,
                                       npa_client_handle client );
npa_resource_state npa_binary_update_fcn( npa_resource *resource,
                                          npa_client_handle client );
npa_resource_state npa_or_update_fcn( npa_resource *resource,
                                      npa_client_handle client);
npa_resource_state npa_binary_and_update_fcn( npa_resource *resource,
                                              npa_client_handle client);
npa_resource_state npa_identity_update_fcn( npa_resource *resource,
                                            npa_client_handle client );
npa_resource_state npa_always_on_update_fcn( npa_resource *resource,
                                             npa_client_handle client );
npa_resource_state npa_impulse_update_fcn( npa_resource *resource,
                                           npa_client_handle client );
void npa_issue_internal_request( npa_client_handle client );
npa_status npa_resource_add_system_event_callback( const char *resource_name,
                                                   npa_callback callback,
                                                   void *context );
npa_status npa_resource_remove_system_event_callback( const char *resource_name,
                                                      npa_callback callback,
                                                      void *context );
void npa_post_custom_event( npa_event_handle event,
                            npa_event_type type, void *event_data );
void npa_post_custom_event_nodups( npa_event_handle event,
                                   npa_event_type type, void *data );
void npa_post_custom_events( npa_resource *resource,
                             npa_event_type type, void *event_data );
void npa_dispatch_custom_event( npa_event_handle event,
                                npa_event_type type, void *data );
void npa_dispatch_custom_events( npa_resource *resource,
                                 npa_event_type type, void *data );
npa_event_handle
npa_get_first_event_of_trigger_type( npa_resource *resource,
                                     unsigned int trigger_type );
npa_event_handle
npa_get_next_event_of_trigger_type( npa_event_handle event,
                                    unsigned int trigger_type );
void npa_dispatch_pre_change_events( npa_resource *resource,
                                     npa_resource_state from_state,
                                     npa_resource_state to_state,
                                     void *data );
void npa_dispatch_post_change_events( npa_resource *resource,
                                      npa_resource_state from_state,
                                      npa_resource_state to_state,
                                      void *data );
typedef enum {
  ICBID_MASTER_APPSS_PROC = 0,
  ICBID_MASTER_MSS_PROC,
  ICBID_MASTER_MNOC_BIMC,
  ICBID_MASTER_SNOC_BIMC,
  ICBID_MASTER_SNOC_BIMC_0 = ICBID_MASTER_SNOC_BIMC,
  ICBID_MASTER_CNOC_MNOC_MMSS_CFG,
  ICBID_MASTER_CNOC_MNOC_CFG,
  ICBID_MASTER_GFX3D,
  ICBID_MASTER_JPEG,
  ICBID_MASTER_MDP,
  ICBID_MASTER_MDP0 = ICBID_MASTER_MDP,
  ICBID_MASTER_MDPS = ICBID_MASTER_MDP,
  ICBID_MASTER_VIDEO,
  ICBID_MASTER_VIDEO_P0 = ICBID_MASTER_VIDEO,
  ICBID_MASTER_VIDEO_P1,
  ICBID_MASTER_VFE,
  ICBID_MASTER_VFE0 = ICBID_MASTER_VFE,
  ICBID_MASTER_CNOC_ONOC_CFG,
  ICBID_MASTER_JPEG_OCMEM,
  ICBID_MASTER_MDP_OCMEM,
  ICBID_MASTER_VIDEO_P0_OCMEM,
  ICBID_MASTER_VIDEO_P1_OCMEM,
  ICBID_MASTER_VFE_OCMEM,
  ICBID_MASTER_LPASS_AHB,
  ICBID_MASTER_QDSS_BAM,
  ICBID_MASTER_SNOC_CFG,
  ICBID_MASTER_BIMC_SNOC,
  ICBID_MASTER_BIMC_SNOC_0 = ICBID_MASTER_BIMC_SNOC,
  ICBID_MASTER_CNOC_SNOC,
  ICBID_MASTER_CRYPTO,
  ICBID_MASTER_CRYPTO_CORE0 = ICBID_MASTER_CRYPTO,
  ICBID_MASTER_CRYPTO_CORE1,
  ICBID_MASTER_LPASS_PROC,
  ICBID_MASTER_MSS,
  ICBID_MASTER_MSS_NAV,
  ICBID_MASTER_OCMEM_DMA,
  ICBID_MASTER_PNOC_SNOC,
  ICBID_MASTER_WCSS,
  ICBID_MASTER_QDSS_ETR,
  ICBID_MASTER_USB3,
  ICBID_MASTER_USB3_0 = ICBID_MASTER_USB3,
  ICBID_MASTER_SDCC_1,
  ICBID_MASTER_SDCC_3,
  ICBID_MASTER_SDCC_2,
  ICBID_MASTER_SDCC_4,
  ICBID_MASTER_TSIF,
  ICBID_MASTER_BAM_DMA,
  ICBID_MASTER_BLSP_2,
  ICBID_MASTER_USB_HSIC,
  ICBID_MASTER_BLSP_1,
  ICBID_MASTER_USB_HS,
  ICBID_MASTER_USB_HS1 = ICBID_MASTER_USB_HS,
  ICBID_MASTER_PNOC_CFG,
  ICBID_MASTER_SNOC_PNOC,
  ICBID_MASTER_RPM_INST,
  ICBID_MASTER_RPM_DATA,
  ICBID_MASTER_RPM_SYS,
  ICBID_MASTER_DEHR,
  ICBID_MASTER_QDSS_DAP,
  ICBID_MASTER_SPDM,
  ICBID_MASTER_TIC,
  ICBID_MASTER_SNOC_CNOC,
  ICBID_MASTER_GFX3D_OCMEM,
  ICBID_MASTER_GFX3D_GMEM = ICBID_MASTER_GFX3D_OCMEM,
  ICBID_MASTER_OVIRT_SNOC,
  ICBID_MASTER_SNOC_OVIRT,
  ICBID_MASTER_SNOC_GVIRT = ICBID_MASTER_SNOC_OVIRT,
  ICBID_MASTER_ONOC_OVIRT,
  ICBID_MASTER_USB_HS2,
  ICBID_MASTER_QPIC,
  ICBID_MASTER_IPA,
  ICBID_MASTER_DSI,
  ICBID_MASTER_MDP1,
  ICBID_MASTER_MDPE = ICBID_MASTER_MDP1,
  ICBID_MASTER_VPU_PROC,
  ICBID_MASTER_VPU,
  ICBID_MASTER_VPU0 = ICBID_MASTER_VPU,
  ICBID_MASTER_CRYPTO_CORE2,
  ICBID_MASTER_PCIE_0,
  ICBID_MASTER_PCIE_1,
  ICBID_MASTER_SATA,
  ICBID_MASTER_UFS,
  ICBID_MASTER_USB3_1,
  ICBID_MASTER_VIDEO_OCMEM,
  ICBID_MASTER_VPU1,
  ICBID_MASTER_VCAP,
  ICBID_MASTER_EMAC,
  ICBID_MASTER_BCAST,
  ICBID_MASTER_MMSS_PROC,
  ICBID_MASTER_SNOC_BIMC_1,
  ICBID_MASTER_SNOC_PCNOC,
  ICBID_MASTER_AUDIO,
  ICBID_MASTER_MM_INT_0,
  ICBID_MASTER_MM_INT_1,
  ICBID_MASTER_MM_INT_2,
  ICBID_MASTER_MM_INT_BIMC,
  ICBID_MASTER_MSS_INT,
  ICBID_MASTER_PCNOC_CFG,
  ICBID_MASTER_PCNOC_INT_0,
  ICBID_MASTER_PCNOC_INT_1,
  ICBID_MASTER_PCNOC_M_0,
  ICBID_MASTER_PCNOC_M_1,
  ICBID_MASTER_PCNOC_S_0,
  ICBID_MASTER_PCNOC_S_1,
  ICBID_MASTER_PCNOC_S_2,
  ICBID_MASTER_PCNOC_S_3,
  ICBID_MASTER_PCNOC_S_4,
  ICBID_MASTER_PCNOC_S_6,
  ICBID_MASTER_PCNOC_S_7,
  ICBID_MASTER_PCNOC_S_8,
  ICBID_MASTER_PCNOC_S_9,
  ICBID_MASTER_QDSS_INT,
  ICBID_MASTER_SNOC_INT_0,
  ICBID_MASTER_SNOC_INT_1,
  ICBID_MASTER_SNOC_INT_BIMC,
  ICBID_MASTER_TCU_0,
  ICBID_MASTER_TCU_1,
  ICBID_MASTER_BIMC_INT_0,
  ICBID_MASTER_BIMC_INT_1,
  ICBID_MASTER_CAMERA,
  ICBID_MASTER_RICA,
  ICBID_MASTER_SNOC_BIMC_2,
  ICBID_MASTER_BIMC_SNOC_1,
  ICBID_MASTER_A0NOC_SNOC,
  ICBID_MASTER_A1NOC_SNOC,
  ICBID_MASTER_A2NOC_SNOC,
  ICBID_MASTER_PIMEM,
  ICBID_MASTER_SNOC_VMEM,
  ICBID_MASTER_CPP,
  ICBID_MASTER_CNOC_A1NOC,
  ICBID_MASTER_PNOC_A1NOC,
  ICBID_MASTER_HMSS,
  ICBID_MASTER_PCIE_2,
  ICBID_MASTER_ROTATOR,
  ICBID_MASTER_VENUS_VMEM,
  ICBID_MASTER_DCC,
  ICBID_MASTER_MCDMA,
  ICBID_MASTER_PCNOC_INT_2,
  ICBID_MASTER_PCNOC_INT_3,
  ICBID_MASTER_PCNOC_INT_4,
  ICBID_MASTER_PCNOC_INT_5,
  ICBID_MASTER_PCNOC_INT_6,
  ICBID_MASTER_PCNOC_S_5,
  ICBID_MASTER_SENSORS_AHB,
  ICBID_MASTER_SENSORS_PROC,
  ICBID_MASTER_QSPI,
  ICBID_MASTER_VFE1,
  ICBID_MASTER_SNOC_INT_2,
  ICBID_MASTER_SMMNOC_BIMC,
  ICBID_MASTER_CRVIRT_A1NOC,
  ICBID_MASTER_XM_USB_HS1,
  ICBID_MASTER_XI_USB_HS1,
  ICBID_MASTER_COUNT,
  ICBID_MASTER_SIZE = 0x7FFFFFFF
} ICBId_MasterType;
typedef enum {
  ICBID_SLAVE_EBI1 = 0,
  ICBID_SLAVE_APPSS_L2,
  ICBID_SLAVE_BIMC_SNOC,
  ICBID_SLAVE_BIMC_SNOC_0 = ICBID_SLAVE_BIMC_SNOC,
  ICBID_SLAVE_CAMERA_CFG,
  ICBID_SLAVE_DISPLAY_CFG,
  ICBID_SLAVE_OCMEM_CFG,
  ICBID_SLAVE_CPR_CFG,
  ICBID_SLAVE_CPR_XPU_CFG,
  ICBID_SLAVE_MISC_CFG,
  ICBID_SLAVE_MISC_XPU_CFG,
  ICBID_SLAVE_VENUS_CFG,
  ICBID_SLAVE_GFX3D_CFG,
  ICBID_SLAVE_MMSS_CLK_CFG,
  ICBID_SLAVE_MMSS_CLK_XPU_CFG,
  ICBID_SLAVE_MNOC_MPU_CFG,
  ICBID_SLAVE_ONOC_MPU_CFG,
  ICBID_SLAVE_MNOC_BIMC,
  ICBID_SLAVE_SERVICE_MNOC,
  ICBID_SLAVE_OCMEM,
  ICBID_SLAVE_GMEM = ICBID_SLAVE_OCMEM,
  ICBID_SLAVE_SERVICE_ONOC,
  ICBID_SLAVE_APPSS,
  ICBID_SLAVE_LPASS,
  ICBID_SLAVE_USB3,
  ICBID_SLAVE_USB3_0 = ICBID_SLAVE_USB3,
  ICBID_SLAVE_WCSS,
  ICBID_SLAVE_SNOC_BIMC,
  ICBID_SLAVE_SNOC_BIMC_0 = ICBID_SLAVE_SNOC_BIMC,
  ICBID_SLAVE_SNOC_CNOC,
  ICBID_SLAVE_IMEM,
  ICBID_SLAVE_OCIMEM = ICBID_SLAVE_IMEM,
  ICBID_SLAVE_SNOC_OVIRT,
  ICBID_SLAVE_SNOC_GVIRT = ICBID_SLAVE_SNOC_OVIRT,
  ICBID_SLAVE_SNOC_PNOC,
  ICBID_SLAVE_SNOC_PCNOC = ICBID_SLAVE_SNOC_PNOC,
  ICBID_SLAVE_SERVICE_SNOC,
  ICBID_SLAVE_QDSS_STM,
  ICBID_SLAVE_SDCC_1,
  ICBID_SLAVE_SDCC_3,
  ICBID_SLAVE_SDCC_2,
  ICBID_SLAVE_SDCC_4,
  ICBID_SLAVE_TSIF,
  ICBID_SLAVE_BAM_DMA,
  ICBID_SLAVE_BLSP_2,
  ICBID_SLAVE_USB_HSIC,
  ICBID_SLAVE_BLSP_1,
  ICBID_SLAVE_USB_HS,
  ICBID_SLAVE_USB_HS1 = ICBID_SLAVE_USB_HS,
  ICBID_SLAVE_PDM,
  ICBID_SLAVE_PERIPH_APU_CFG,
  ICBID_SLAVE_PNOC_MPU_CFG,
  ICBID_SLAVE_PRNG,
  ICBID_SLAVE_PNOC_SNOC,
  ICBID_SLAVE_PCNOC_SNOC = ICBID_SLAVE_PNOC_SNOC,
  ICBID_SLAVE_SERVICE_PNOC,
  ICBID_SLAVE_CLK_CTL,
  ICBID_SLAVE_CNOC_MSS,
  ICBID_SLAVE_PCNOC_MSS = ICBID_SLAVE_CNOC_MSS,
  ICBID_SLAVE_SECURITY,
  ICBID_SLAVE_TCSR,
  ICBID_SLAVE_TLMM,
  ICBID_SLAVE_CRYPTO_0_CFG,
  ICBID_SLAVE_CRYPTO_1_CFG,
  ICBID_SLAVE_IMEM_CFG,
  ICBID_SLAVE_MESSAGE_RAM,
  ICBID_SLAVE_BIMC_CFG,
  ICBID_SLAVE_BOOT_ROM,
  ICBID_SLAVE_CNOC_MNOC_MMSS_CFG,
  ICBID_SLAVE_PMIC_ARB,
  ICBID_SLAVE_SPDM_WRAPPER,
  ICBID_SLAVE_DEHR_CFG,
  ICBID_SLAVE_MPM,
  ICBID_SLAVE_QDSS_CFG,
  ICBID_SLAVE_RBCPR_CFG,
  ICBID_SLAVE_RBCPR_CX_CFG = ICBID_SLAVE_RBCPR_CFG,
  ICBID_SLAVE_RBCPR_QDSS_APU_CFG,
  ICBID_SLAVE_CNOC_MNOC_CFG,
  ICBID_SLAVE_SNOC_MPU_CFG,
  ICBID_SLAVE_CNOC_ONOC_CFG,
  ICBID_SLAVE_PNOC_CFG,
  ICBID_SLAVE_SNOC_CFG,
  ICBID_SLAVE_EBI1_DLL_CFG,
  ICBID_SLAVE_PHY_APU_CFG,
  ICBID_SLAVE_EBI1_PHY_CFG,
  ICBID_SLAVE_RPM,
  ICBID_SLAVE_CNOC_SNOC,
  ICBID_SLAVE_SERVICE_CNOC,
  ICBID_SLAVE_OVIRT_SNOC,
  ICBID_SLAVE_OVIRT_OCMEM,
  ICBID_SLAVE_USB_HS2,
  ICBID_SLAVE_QPIC,
  ICBID_SLAVE_IPS_CFG,
  ICBID_SLAVE_DSI_CFG,
  ICBID_SLAVE_USB3_1,
  ICBID_SLAVE_PCIE_0,
  ICBID_SLAVE_PCIE_1,
  ICBID_SLAVE_PSS_SMMU_CFG,
  ICBID_SLAVE_CRYPTO_2_CFG,
  ICBID_SLAVE_PCIE_0_CFG,
  ICBID_SLAVE_PCIE_1_CFG,
  ICBID_SLAVE_SATA_CFG,
  ICBID_SLAVE_SPSS_GENI_IR,
  ICBID_SLAVE_UFS_CFG,
  ICBID_SLAVE_AVSYNC_CFG,
  ICBID_SLAVE_VPU_CFG,
  ICBID_SLAVE_USB_PHY_CFG,
  ICBID_SLAVE_RBCPR_MX_CFG,
  ICBID_SLAVE_PCIE_PARF,
  ICBID_SLAVE_VCAP_CFG,
  ICBID_SLAVE_EMAC_CFG,
  ICBID_SLAVE_BCAST_CFG,
  ICBID_SLAVE_KLM_CFG,
  ICBID_SLAVE_DISPLAY_PWM,
  ICBID_SLAVE_GENI,
  ICBID_SLAVE_SNOC_BIMC_1,
  ICBID_SLAVE_AUDIO,
  ICBID_SLAVE_CATS_0,
  ICBID_SLAVE_CATS_1,
  ICBID_SLAVE_MM_INT_0,
  ICBID_SLAVE_MM_INT_1,
  ICBID_SLAVE_MM_INT_2,
  ICBID_SLAVE_MM_INT_BIMC,
  ICBID_SLAVE_MMU_MODEM_XPU_CFG,
  ICBID_SLAVE_MSS_INT,
  ICBID_SLAVE_PCNOC_INT_0,
  ICBID_SLAVE_PCNOC_INT_1,
  ICBID_SLAVE_PCNOC_M_0,
  ICBID_SLAVE_PCNOC_M_1,
  ICBID_SLAVE_PCNOC_S_0,
  ICBID_SLAVE_PCNOC_S_1,
  ICBID_SLAVE_PCNOC_S_2,
  ICBID_SLAVE_PCNOC_S_3,
  ICBID_SLAVE_PCNOC_S_4,
  ICBID_SLAVE_PCNOC_S_6,
  ICBID_SLAVE_PCNOC_S_7,
  ICBID_SLAVE_PCNOC_S_8,
  ICBID_SLAVE_PCNOC_S_9,
  ICBID_SLAVE_PRNG_XPU_CFG,
  ICBID_SLAVE_QDSS_INT,
  ICBID_SLAVE_RPM_XPU_CFG,
  ICBID_SLAVE_SNOC_INT_0,
  ICBID_SLAVE_SNOC_INT_1,
  ICBID_SLAVE_SNOC_INT_BIMC,
  ICBID_SLAVE_TCU,
  ICBID_SLAVE_BIMC_INT_0,
  ICBID_SLAVE_BIMC_INT_1,
  ICBID_SLAVE_RICA_CFG,
  ICBID_SLAVE_SNOC_BIMC_2,
  ICBID_SLAVE_BIMC_SNOC_1,
  ICBID_SLAVE_PNOC_A1NOC,
  ICBID_SLAVE_SNOC_VMEM,
  ICBID_SLAVE_A0NOC_SNOC,
  ICBID_SLAVE_A1NOC_SNOC,
  ICBID_SLAVE_A2NOC_SNOC,
  ICBID_SLAVE_A0NOC_CFG,
  ICBID_SLAVE_A0NOC_MPU_CFG,
  ICBID_SLAVE_A0NOC_SMMU_CFG,
  ICBID_SLAVE_A1NOC_CFG,
  ICBID_SLAVE_A1NOC_MPU_CFG,
  ICBID_SLAVE_A1NOC_SMMU_CFG,
  ICBID_SLAVE_A2NOC_CFG,
  ICBID_SLAVE_A2NOC_MPU_CFG,
  ICBID_SLAVE_A2NOC_SMMU_CFG,
  ICBID_SLAVE_AHB2PHY,
  ICBID_SLAVE_CAMERA_THROTTLE_CFG,
  ICBID_SLAVE_DCC_CFG,
  ICBID_SLAVE_DISPLAY_THROTTLE_CFG,
  ICBID_SLAVE_DSA_CFG,
  ICBID_SLAVE_DSA_MPU_CFG,
  ICBID_SLAVE_SSC_MPU_CFG,
  ICBID_SLAVE_HMSS_L3,
  ICBID_SLAVE_LPASS_SMMU_CFG,
  ICBID_SLAVE_MMAGIC_CFG,
  ICBID_SLAVE_PCIE20_AHB2PHY,
  ICBID_SLAVE_PCIE_2,
  ICBID_SLAVE_PCIE_2_CFG,
  ICBID_SLAVE_PIMEM,
  ICBID_SLAVE_PIMEM_CFG,
  ICBID_SLAVE_QDSS_RBCPR_APU_CFG,
  ICBID_SLAVE_RBCPR_CX,
  ICBID_SLAVE_RBCPR_MX,
  ICBID_SLAVE_SMMU_CPP_CFG,
  ICBID_SLAVE_SMMU_JPEG_CFG,
  ICBID_SLAVE_SMMU_MDP_CFG,
  ICBID_SLAVE_SMMU_ROTATOR_CFG,
  ICBID_SLAVE_SMMU_VENUS_CFG,
  ICBID_SLAVE_SMMU_VFE_CFG,
  ICBID_SLAVE_SSC_CFG,
  ICBID_SLAVE_VENUS_THROTTLE_CFG,
  ICBID_SLAVE_VMEM,
  ICBID_SLAVE_VMEM_CFG,
  ICBID_SLAVE_QDSS_MPU_CFG,
  ICBID_SLAVE_USB3_PHY_CFG,
  ICBID_SLAVE_IPA_CFG,
  ICBID_SLAVE_PCNOC_INT_2,
  ICBID_SLAVE_PCNOC_INT_3,
  ICBID_SLAVE_PCNOC_INT_4,
  ICBID_SLAVE_PCNOC_INT_5,
  ICBID_SLAVE_PCNOC_INT_6,
  ICBID_SLAVE_PCNOC_S_5,
  ICBID_SLAVE_QSPI,
  ICBID_SLAVE_A1NOC_MS_MPU_CFG,
  ICBID_SLAVE_A2NOC_MS_MPU_CFG,
  ICBID_SLAVE_MODEM_Q6_SMMU_CFG,
  ICBID_SLAVE_MSS_MPU_CFG,
  ICBID_SLAVE_MSS_PROC_MS_MPU_CFG,
  ICBID_SLAVE_SKL,
  ICBID_SLAVE_SNOC_INT_2,
  ICBID_SLAVE_SMMNOC_BIMC,
  ICBID_SLAVE_CRVIRT_A1NOC,
  ICBID_SLAVE_COUNT,
  ICBID_SLAVE_SIZE = 0x7FFFFFFF
} ICBId_SlaveType;
typedef enum
{
   ICBARB_ERROR_SUCCESS = 0,
   ICBARB_ERROR_UNSUPPORTED,
   ICBARB_ERROR_INVALID_ARG,
   ICBARB_ERROR_INVALID_MASTER,
   ICBARB_ERROR_NO_ROUTE_TO_SLAVE,
   ICBARB_ERROR_VECTOR_LENGTH_MISMATCH,
   ICBARB_ERROR_OUT_OF_MEMORY,
   ICBARB_ERROR_UNKNOWN,
   ICBARB_ERROR_REQUEST_REJECTED,
   ICBARB_ERROR_MAX,
   ICBARB_ERROR_PLACEHOLDER = 0x7FFFFFFF
} ICBArb_ErrorType;
typedef enum
{
   ICBARB_STATE_NORMAL = 0,
   ICBARB_STATE_OVERSUBSCRIBED,
   ICBARB_STATE_MAX,
   ICBARB_STATE_PLACEHOLDER = 0x7FFFFFFF
} ICBArb_StateType;
typedef struct
{
   ICBId_MasterType eMaster;
   ICBId_SlaveType eSlave;
} ICBArb_MasterSlaveType;
typedef struct
{
   npa_callback callback;
   ICBArb_MasterSlaveType *aMasterSlave;
} ICBArb_CreateClientVectorType;
typedef enum
{
   ICBARB_REQUEST_TYPE_1,
   ICBARB_REQUEST_TYPE_2,
   ICBARB_REQUEST_TYPE_3,
   ICBARB_REQUEST_TYPE_1_TIER_3,
   ICBARB_REQUEST_TYPE_2_TIER_3,
   ICBARB_REQUEST_TYPE_3_TIER_3,
   ICBARB_REQUEST_TYPE_1_LAT,
   ICBARB_REQUEST_TYPE_2_LAT,
   ICBARB_REQUEST_TYPE_3_LAT,
   ICBARB_REQUEST_TYPE_MAX,
   ICBARB_REQUEST_TYPE_PLACEHOLDER = 0x7FFFFFFF
} ICBArb_RequestTypeType;
typedef struct
{
   uint64 uDataBurst;
   uint32 uTransferTimeUs;
   uint32 uPeriodUs;
   uint32 uLatencyNs;
} ICBArb_Request1Type;
typedef struct
{
   uint64 uThroughPut;
   uint32 uUsagePercentage;
   uint32 uLatencyNs;
} ICBArb_Request2Type;
typedef struct
{
   uint64 uIb;
   uint64 uAb;
   uint32 uLatencyNs;
} ICBArb_Request3Type;
typedef union
{
   ICBArb_Request1Type type1;
   ICBArb_Request2Type type2;
   ICBArb_Request3Type type3;
} ICBArb_RequestUnionType;
typedef struct
{
   ICBArb_RequestTypeType arbType;
   ICBArb_RequestUnionType arbData;
} ICBArb_RequestType;
typedef enum
{
   ICBARB_QUERY_RESOURCE_COUNT = 0,
   ICBARB_QUERY_RESOURCE_NAMES,
   ICBARB_QUERY_SLAVE_PORT_COUNT,
   ICBARB_QUERY_MAX,
   ICBARB_QUERY_PLACEHOLDER = 0x7FFFFFFF
} ICBArb_QueryTypeType;
typedef struct
{
   npa_client_handle client;
   uint32 uNumResource;
} ICBArb_QueryResourceCountType;
typedef struct
{
   npa_client_handle client;
   const char **aResourceNames;
   uint32 uNumResource;
} ICBArb_QueryResourceNamesType;
typedef struct
{
   ICBId_SlaveType eSlaveId;
   uint32 uNumPorts;
} ICBArb_QuerySlavePortCountType;
typedef union
{
   ICBArb_QueryResourceCountType resourceCount;
   ICBArb_QueryResourceNamesType resourceNames;
   ICBArb_QuerySlavePortCountType slavePortCount;
} ICBArb_QueryUnionType;
typedef struct
{
   ICBArb_QueryTypeType queryType;
   ICBArb_QueryUnionType queryData;
} ICBArb_QueryType;
void icbarb_init( void );
ICBArb_CreateClientVectorType *icbarb_fill_client_vector(
  ICBArb_CreateClientVectorType *psVector,
  ICBArb_MasterSlaveType *aMasterSlave,
  npa_callback callback);
npa_client_handle icbarb_create_client(
   const char *pszClientName,
   ICBArb_MasterSlaveType *aMasterSlave,
   uint32 uNumMasterSlave);
npa_client_handle icbarb_create_client_ex(
   const char *pszClientName,
   ICBArb_MasterSlaveType *aMasterSlave,
   uint32 uNumMasterSlave,
   npa_callback callback);
npa_client_handle icbarb_create_suppressible_client_ex(
   const char *pszClientName,
   ICBArb_MasterSlaveType *aMasterSlave,
   uint32 uNumMasterSlave,
   npa_callback callback);
ICBArb_ErrorType icbarb_issue_request(
   npa_client_handle client,
   ICBArb_RequestType *aRequest,
   uint32 uNumRequest);
void icbarb_complete_request( npa_client_handle client );
void icbarb_destroy_client( npa_client_handle client );
ICBArb_ErrorType icbarb_query( ICBArb_QueryType *pQuery );
<!-- ================================================================================================== -->
<!-- GPIO configs. -->
<!-- -->
<!-- Source: IP Catalog -->
<!-- ================================================================================================== -->
<!--
     BLSP_UART1_TX_DATA (((0) & 0x3FF)<< 4 | ((2) & 0xF)| ((DAL_GPIO_OUTPUT) & 0x1) << 14| ((DAL_GPIO_PULL_UP) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART1_RX_DATA (((1) & 0x3FF)<< 4 | ((2) & 0xF)| ((DAL_GPIO_INPUT) & 0x1) << 14| ((DAL_GPIO_PULL_DOWN) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART1_CTS_N (((2) & 0x3FF)<< 4 | ((2) & 0xF)| ((DAL_GPIO_INPUT) & 0x1) << 14| ((DAL_GPIO_PULL_DOWN) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART1_RFR_N (((3) & 0x3FF)<< 4 | ((2) & 0xF)| ((DAL_GPIO_OUTPUT) & 0x1) << 14| ((DAL_GPIO_PULL_UP) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART2_TX_DATA (((4) & 0x3FF)<< 4 | ((2) & 0xF)| ((DAL_GPIO_OUTPUT) & 0x1) << 14| ((DAL_GPIO_PULL_UP) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART2_RX_DATA (((5) & 0x3FF)<< 4 | ((2) & 0xF)| ((DAL_GPIO_INPUT) & 0x1) << 14| ((DAL_GPIO_PULL_DOWN) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART2_CTS_N (((6) & 0x3FF)<< 4 | ((2) & 0xF)| ((DAL_GPIO_INPUT) & 0x1) << 14| ((DAL_GPIO_PULL_DOWN) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART2_RFR_N (((7) & 0x3FF)<< 4 | ((2) & 0xF)| ((DAL_GPIO_OUTPUT) & 0x1) << 14| ((DAL_GPIO_PULL_UP) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART3_TX_DATA (((8) & 0x3FF)<< 4 | ((2) & 0xF)| ((DAL_GPIO_OUTPUT) & 0x1) << 14| ((DAL_GPIO_PULL_UP) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART3_RX_DATA (((9) & 0x3FF)<< 4 | ((2) & 0xF)| ((DAL_GPIO_INPUT) & 0x1) << 14| ((DAL_GPIO_PULL_UP) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART3_CTS_N (((10) & 0x3FF)<< 4 | ((3) & 0xF)| ((DAL_GPIO_INPUT) & 0x1) << 14| ((DAL_GPIO_PULL_DOWN) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART3_RFR_N (((11) & 0x3FF)<< 4 | ((3) & 0xF)| ((DAL_GPIO_OUTPUT) & 0x1) << 14| ((DAL_GPIO_PULL_UP) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART4_TX_DATA (((17) & 0x3FF)<< 4 | ((3) & 0xF)| ((DAL_GPIO_OUTPUT) & 0x1) << 14| ((DAL_GPIO_PULL_UP) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART4_RX_DATA (((18) & 0x3FF)<< 4 | ((3) & 0xF)| ((DAL_GPIO_INPUT) & 0x1) << 14| ((DAL_GPIO_PULL_DOWN) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART4_CTS_N (((19) & 0x3FF)<< 4 | ((3) & 0xF)| ((DAL_GPIO_INPUT) & 0x1) << 14| ((DAL_GPIO_PULL_DOWN) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART4_RFR_N (((20) & 0x3FF)<< 4 | ((3) & 0xF)| ((DAL_GPIO_OUTPUT) & 0x1) << 14| ((DAL_GPIO_PULL_UP) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART5_TX_DATA (((21) & 0x3FF)<< 4 | ((3) & 0xF)| ((DAL_GPIO_OUTPUT) & 0x1) << 14| ((DAL_GPIO_PULL_UP) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART5_RX_DATA (((22) & 0x3FF)<< 4 | ((3) & 0xF)| ((DAL_GPIO_INPUT) & 0x1) << 14| ((DAL_GPIO_PULL_DOWN) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART5_CTS_N (((23) & 0x3FF)<< 4 | ((3) & 0xF)| ((DAL_GPIO_INPUT) & 0x1) << 14| ((DAL_GPIO_PULL_DOWN) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART5_RFR_N (((24) & 0x3FF)<< 4 | ((4) & 0xF)| ((DAL_GPIO_OUTPUT) & 0x1) << 14| ((DAL_GPIO_PULL_UP) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART6_TX_DATA (((25) & 0x3FF)<< 4 | ((4) & 0xF)| ((DAL_GPIO_OUTPUT) & 0x1) << 14| ((DAL_GPIO_PULL_UP) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART6_RX_DATA (((26) & 0x3FF)<< 4 | ((3) & 0xF)| ((DAL_GPIO_INPUT) & 0x1) << 14| ((DAL_GPIO_PULL_DOWN) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART6_CTS_N (((27) & 0x3FF)<< 4 | ((2) & 0xF)| ((DAL_GPIO_INPUT) & 0x1) << 14| ((DAL_GPIO_PULL_DOWN) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART6_RFR_N (((28) & 0x3FF)<< 4 | ((2) & 0xF)| ((DAL_GPIO_OUTPUT) & 0x1) << 14| ((DAL_GPIO_PULL_UP) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART7_TX_DATA (((41) & 0x3FF)<< 4 | ((2) & 0xF)| ((DAL_GPIO_OUTPUT) & 0x1) << 14| ((DAL_GPIO_PULL_UP) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART7_RX_DATA (((42) & 0x3FF)<< 4 | ((2) & 0xF)| ((DAL_GPIO_INPUT) & 0x1) << 14| ((DAL_GPIO_PULL_DOWN) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART7_CTS_N (((43) & 0x3FF)<< 4 | ((2) & 0xF)| ((DAL_GPIO_INPUT) & 0x1) << 14| ((DAL_GPIO_PULL_DOWN) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART7_RFR_N (((44) & 0x3FF)<< 4 | ((2) & 0xF)| ((DAL_GPIO_OUTPUT) & 0x1) << 14| ((DAL_GPIO_PULL_UP) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART8_TX_DATA (((45) & 0x3FF)<< 4 | ((2) & 0xF)| ((DAL_GPIO_OUTPUT) & 0x1) << 14| ((DAL_GPIO_PULL_UP) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART8_RX_DATA (((46) & 0x3FF)<< 4 | ((2) & 0xF)| ((DAL_GPIO_INPUT) & 0x1) << 14| ((DAL_GPIO_PULL_DOWN) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART8_CTS_N (((47) & 0x3FF)<< 4 | ((2) & 0xF)| ((DAL_GPIO_INPUT) & 0x1) << 14| ((DAL_GPIO_PULL_DOWN) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART8_RFR_N (((48) & 0x3FF)<< 4 | ((2) & 0xF)| ((DAL_GPIO_OUTPUT) & 0x1) << 14| ((DAL_GPIO_PULL_UP) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART9_TX_DATA (((49) & 0x3FF)<< 4 | ((3) & 0xF)| ((DAL_GPIO_OUTPUT) & 0x1) << 14| ((DAL_GPIO_PULL_UP) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART9_RX_DATA (((50) & 0x3FF)<< 4 | ((3) & 0xF)| ((DAL_GPIO_INPUT) & 0x1) << 14| ((DAL_GPIO_PULL_DOWN) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART9_CTS_N (((51) & 0x3FF)<< 4 | ((3) & 0xF)| ((DAL_GPIO_INPUT) & 0x1) << 14| ((DAL_GPIO_PULL_DOWN) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART9_RFR_N (((52) & 0x3FF)<< 4 | ((3) & 0xF)| ((DAL_GPIO_OUTPUT) & 0x1) << 14| ((DAL_GPIO_PULL_UP) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART10_TX_DATA (((53) & 0x3FF)<< 4 | ((4) & 0xF)| ((DAL_GPIO_OUTPUT) & 0x1) << 14| ((DAL_GPIO_PULL_UP) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART10_RX_DATA (((54) & 0x3FF)<< 4 | ((4) & 0xF)| ((DAL_GPIO_INPUT) & 0x1) << 14| ((DAL_GPIO_PULL_DOWN) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART10_CTS_N (((55) & 0x3FF)<< 4 | ((3) & 0xF)| ((DAL_GPIO_INPUT) & 0x1) << 14| ((DAL_GPIO_PULL_DOWN) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART10_RFR_N (((56) & 0x3FF)<< 4 | ((3) & 0xF)| ((DAL_GPIO_OUTPUT) & 0x1) << 14| ((DAL_GPIO_PULL_UP) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART11_TX_DATA (((81) & 0x3FF)<< 4 | ((3) & 0xF)| ((DAL_GPIO_OUTPUT) & 0x1) << 14| ((DAL_GPIO_PULL_UP) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART11_RX_DATA (((82) & 0x3FF)<< 4 | ((3) & 0xF)| ((DAL_GPIO_INPUT) & 0x1) << 14| ((DAL_GPIO_PULL_DOWN) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART11_CTS_N (((83) & 0x3FF)<< 4 | ((2) & 0xF)| ((DAL_GPIO_INPUT) & 0x1) << 14| ((DAL_GPIO_PULL_DOWN) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART11_RFR_N (((84) & 0x3FF)<< 4 | ((2) & 0xF)| ((DAL_GPIO_OUTPUT) & 0x1) << 14| ((DAL_GPIO_PULL_UP) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART12_TX_DATA (((85) & 0x3FF)<< 4 | ((2) & 0xF)| ((DAL_GPIO_OUTPUT) & 0x1) << 14| ((DAL_GPIO_PULL_UP) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART12_RX_DATA (((86) & 0x3FF)<< 4 | ((2) & 0xF)| ((DAL_GPIO_INPUT) & 0x1) << 14| ((DAL_GPIO_PULL_DOWN) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART12_CTS_N (((87) & 0x3FF)<< 4 | ((2) & 0xF)| ((DAL_GPIO_INPUT) & 0x1) << 14| ((DAL_GPIO_PULL_DOWN) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     BLSP_UART12_RFR_N (((88) & 0x3FF)<< 4 | ((2) & 0xF)| ((DAL_GPIO_OUTPUT) & 0x1) << 14| ((DAL_GPIO_PULL_UP) & 0x3) << 15| ((DAL_GPIO_2MA)& 0xF) << 17| 0x20000000)
     PnocIBval or PnocABval value = required bandwidth(in terms of bytes)
                                  = required bandwidth/8(in terms of bits).
     In case of UART we can support the maximum baudrate 4Mbps, so we are voting for the max
     bandwidth(500000).
     The BAM IRQ for BLSP1 is the only BAM IRQ that is routed to MSS. You must use a UART on BLSP1
     if you want to use it with BAM.
======================================================================================================= -->
<driver name="Uart">
  <global_def>
    <string name="blsp1_ahb_clock_name" type=0x00000001> gcc_blsp1_ahb_clk </string>
    <string name="blsp2_ahb_clock_name" type=0x00000001> gcc_blsp2_ahb_clk </string>
    <string name="uartbam_1_clock_name" type=0x00000001> gcc_blsp1_uart1_apps_clk </string>
    <string name="uartbam_2_clock_name" type=0x00000001> gcc_blsp1_uart2_apps_clk </string>
    <string name="uartbam_3_clock_name" type=0x00000001> gcc_blsp1_uart3_apps_clk </string>
    <string name="uartbam_4_clock_name" type=0x00000001> gcc_blsp1_uart4_apps_clk </string>
    <string name="uartbam_5_clock_name" type=0x00000001> gcc_blsp1_uart5_apps_clk </string>
    <string name="uartbam_6_clock_name" type=0x00000001> gcc_blsp1_uart6_apps_clk </string>
    <string name="uartbam_7_clock_name" type=0x00000001> gcc_blsp2_uart1_apps_clk </string>
    <string name="uartbam_8_clock_name" type=0x00000001> gcc_blsp2_uart2_apps_clk </string>
    <string name="uartbam_9_clock_name" type=0x00000001> gcc_blsp2_uart3_apps_clk </string>
    <string name="uartbam_10_clock_name" type=0x00000001> gcc_blsp2_uart4_apps_clk </string>
    <string name="uartbam_11_clock_name" type=0x00000001> gcc_blsp2_uart5_apps_clk </string>
    <string name="uartbam_12_clock_name" type=0x00000001> gcc_blsp2_uart6_apps_clk </string>
    <string name="uartbam_1_tx_name" type=0x00000001> blsp_uart_tx[1] </string>
    <string name="uartbam_1_rx_name" type=0x00000001> blsp_uart_rx[1] </string>
    <string name="uartbam_1_cts_name" type=0x00000001> blsp_uart_cts_n[1] </string>
    <string name="uartbam_1_rfr_name" type=0x00000001> blsp_uart_rfr_n[1] </string>
    <string name="uartbam_2_tx_name" type=0x00000001> blsp_uart_tx[2] </string>
    <string name="uartbam_2_rx_name" type=0x00000001> blsp_uart_rx[2] </string>
    <string name="uartbam_2_cts_name" type=0x00000001> blsp_uart_cts_n[2] </string>
    <string name="uartbam_2_rfr_name" type=0x00000001> blsp_uart_rfr_n[2] </string>
    <string name="uartbam_3_tx_name" type=0x00000001> blsp_uart_tx[3] </string>
    <string name="uartbam_3_rx_name" type=0x00000001> blsp_uart_rx[3] </string>
    <string name="uartbam_3_cts_name" type=0x00000001> blsp_uart_cts_n[3] </string>
    <string name="uartbam_3_rfr_name" type=0x00000001> blsp_uart_rfr_n[3] </string>
    <string name="uartbam_4_tx_name" type=0x00000001> blsp_uart_tx[4] </string>
    <string name="uartbam_4_rx_name" type=0x00000001> blsp_uart_rx[4] </string>
    <string name="uartbam_4_cts_name" type=0x00000001> blsp_uart_cts_n[4] </string>
    <string name="uartbam_4_rfr_name" type=0x00000001> blsp_uart_rfr_n[4] </string>
    <string name="uartbam_5_tx_name" type=0x00000001> blsp_uart_tx[5] </string>
    <string name="uartbam_5_rx_name" type=0x00000001> blsp_uart_rx[5] </string>
    <string name="uartbam_5_cts_name" type=0x00000001> blsp_uart_cts_n[5] </string>
    <string name="uartbam_5_rfr_name" type=0x00000001> blsp_uart_rfr_n[5] </string>
    <string name="uartbam_6_tx_name" type=0x00000001> blsp_uart_tx[6] </string>
    <string name="uartbam_6_rx_name" type=0x00000001> blsp_uart_rx[6] </string>
    <string name="uartbam_6_cts_name" type=0x00000001> blsp_uart_cts_n[6] </string>
    <string name="uartbam_6_rfr_name" type=0x00000001> blsp_uart_rfr_n[6] </string>
    <string name="uartbam_7_tx_name" type=0x00000001> blsp_uart_tx[7] </string>
    <string name="uartbam_7_rx_name" type=0x00000001> blsp_uart_rx[7] </string>
    <string name="uartbam_7_cts_name" type=0x00000001> blsp_uart_cts_n[7] </string>
    <string name="uartbam_7_rfr_name" type=0x00000001> blsp_uart_rfr_n[7] </string>
    <string name="uartbam_8_tx_name" type=0x00000001> blsp_uart_tx[8] </string>
    <string name="uartbam_8_rx_name" type=0x00000001> blsp_uart_rx[8] </string>
    <string name="uartbam_8_cts_name" type=0x00000001> blsp_uart_cts_n[8] </string>
    <string name="uartbam_8_rfr_name" type=0x00000001> blsp_uart_rfr_n[8] </string>
    <string name="uartbam_9_tx_name" type=0x00000001> blsp_uart_tx[9] </string>
    <string name="uartbam_9_rx_name" type=0x00000001> blsp_uart_rx[9] </string>
    <string name="uartbam_9_cts_name" type=0x00000001> blsp_uart_cts_n[9] </string>
    <string name="uartbam_9_rfr_name" type=0x00000001> blsp_uart_rfr_n[9] </string>
    <string name="uartbam_10_tx_name" type=0x00000001> blsp_uart_tx[10] </string>
    <string name="uartbam_10_rx_name" type=0x00000001> blsp_uart_rx[10] </string>
    <string name="uartbam_10_cts_name" type=0x00000001> blsp_uart_cts_n[10] </string>
    <string name="uartbam_10_rfr_name" type=0x00000001> blsp_uart_rfr_n[10] </string>
    <string name="uartbam_11_tx_name" type=0x00000001> blsp_uart_tx[11] </string>
    <string name="uartbam_11_rx_name" type=0x00000001> blsp_uart_rx[11] </string>
    <string name="uartbam_11_cts_name" type=0x00000001> blsp_uart_cts_n[11] </string>
    <string name="uartbam_11_rfr_name" type=0x00000001> blsp_uart_rfr_n[11] </string>
    <string name="uartbam_12_tx_name" type=0x00000001> blsp_uart_tx[12] </string>
    <string name="uartbam_12_rx_name" type=0x00000001> blsp_uart_rx[12] </string>
    <string name="uartbam_12_cts_name" type=0x00000001> blsp_uart_cts_n[12] </string>
    <string name="uartbam_12_rfr_name" type=0x00000001> blsp_uart_rfr_n[12] </string>
  </global_def>
  <!-- =================================================================== -->
  <!-- UARTBAM1 -->
  <!-- =================================================================== -->
  <device id=0x02001000>
    <props name="Irq" type=0x00000002> 104 </props>
    <props name="UartBase" type=0x00000002> 0x756f000 </props>
    <props name="UartClockName" type=0x00000011> uartbam_1_clock_name </props>
    <props name="PClockName" type=0x00000011> blsp1_ahb_clock_name </props>
    <props name="ManagePCLK" type=0x00000002> 1 </props>
    <props name="DetectBrk" type=0x00000002> 0 </props>
    <props name="UartIntSelBase" type=0x00000002> 0x7ab040 </props>
    <props name="UartIntSelVal" type=0x00000002> 0x00000001 </props>
    <props name="PnocVoteEnable" type=0x00000002> 1 </props>
    <props name="PnocIBval" type=0x00000002> 500000 </props>
    <props name="PnocABval" type=0x00000002> 500000 </props>
    <props name="PnocArbMaster" type=0x00000002> ICBID_MASTER_BLSP_1 </props>
    <props name="PnocArbSlave" type=0x00000002> ICBID_SLAVE_EBI1 </props>
    <props name="GpioTxDataName" type=0x00000011> uartbam_1_tx_name </props>
    <props name="GpioRxDataName" type=0x00000011> uartbam_1_rx_name </props>
    <props name="GpioCtsDataName" type=0x00000011> uartbam_1_cts_name </props>
    <props name="GpioRfrDataName" type=0x00000011> uartbam_1_rfr_name </props>
    <props name="GpioTxConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA} </props>
    <props name="GpioRxConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA} </props>
    <props name="GpioCtsConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA} </props>
    <props name="GpioRfrConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA} </props>
  </device>
  <!-- =================================================================== -->
  <!-- UARTBAM2 -->
  <!-- =================================================================== -->
  <device id=0x02001001>
    <props name="Irq" type=0x00000002> 104 </props>
    <props name="UartBase" type=0x00000002> 0x7570000 </props>
    <props name="UartClockName" type=0x00000011> uartbam_2_clock_name </props>
    <props name="PClockName" type=0x00000011> blsp1_ahb_clock_name </props>
    <props name="ManagePCLK" type=0x00000002> 1 </props>
    <props name="DetectBrk" type=0x00000002> 0 </props>
    <props name="UartIntSelBase" type=0x00000002> 0x7ab040 </props>
    <props name="UartIntSelVal" type=0x00000002> 0x00000002 </props>
    <props name="PnocVoteEnable" type=0x00000002> 1 </props>
    <props name="PnocIBval" type=0x00000002> 500000 </props>
    <props name="PnocABval" type=0x00000002> 500000 </props>
    <props name="PnocArbMaster" type=0x00000002> ICBID_MASTER_BLSP_1 </props>
    <props name="PnocArbSlave" type=0x00000002> ICBID_SLAVE_EBI1 </props>
    <props name="GpioTxDataName" type=0x00000011> uartbam_2_tx_name </props>
    <props name="GpioRxDataName" type=0x00000011> uartbam_2_rx_name </props>
    <props name="GpioCtsDataName" type=0x00000011> uartbam_2_cts_name </props>
    <props name="GpioRfrDataName" type=0x00000011> uartbam_2_rfr_name </props>
    <props name="GpioTxConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA} </props>
    <props name="GpioRxConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA} </props>
    <props name="GpioCtsConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA} </props>
    <props name="GpioRfrConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA} </props>
  </device>
  <!-- =================================================================== -->
  <!-- UARTBAM3 -->
  <!-- =================================================================== -->
  <device id=0x02001002>
    <props name="Irq" type=0x00000002> 104 </props>
    <props name="UartBase" type=0x00000002> 0x7571000 </props>
    <props name="UartClockName" type=0x00000011> uartbam_3_clock_name </props>
    <props name="PClockName" type=0x00000011> blsp1_ahb_clock_name </props>
    <props name="ManagePCLK" type=0x00000002> 1 </props>
    <props name="DetectBrk" type=0x00000002> 0 </props>
    <props name="UartIntSelBase" type=0x00000002> 0x7ab040 </props>
    <props name="UartIntSelVal" type=0x00000002> 0x00000004 </props>
    <props name="PnocVoteEnable" type=0x00000002> 1 </props>
    <props name="PnocIBval" type=0x00000002> 500000 </props>
    <props name="PnocABval" type=0x00000002> 500000 </props>
    <props name="PnocArbMaster" type=0x00000002> ICBID_MASTER_BLSP_1 </props>
    <props name="PnocArbSlave" type=0x00000002> ICBID_SLAVE_EBI1 </props>
    <props name="GpioTxDataName" type=0x00000011> uartbam_3_tx_name </props>
    <props name="GpioRxDataName" type=0x00000011> uartbam_3_rx_name </props>
    <!-- Flow control lines are not used for the ADSP UART
    <props name="GpioCtsDataName" type=0x00000011> uartbam_3_cts_name </props>
    <props name="GpioRfrDataName" type=0x00000011> uartbam_3_rfr_name </props>
    -->
    <props name="GpioTxConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA} </props>
    <props name="GpioRxConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_INPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA} </props>
    <!-- Flow control lines are not used for the ADSP UART
    <props name="GpioCtsConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA} </props>
    <props name="GpioRfrConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA} </props>
    -->
  </device>
  <!-- =================================================================== -->
  <!-- UARTBAM4 -->
  <!-- =================================================================== -->
  <device id=0x02001003>
    <props name="Irq" type=0x00000002> 104 </props>
    <props name="UartBase" type=0x00000002> 0x7572000 </props>
    <props name="UartClockName" type=0x00000011> uartbam_4_clock_name </props>
    <props name="PClockName" type=0x00000011> blsp1_ahb_clock_name </props>
    <props name="ManagePCLK" type=0x00000002> 1 </props>
    <props name="DetectBrk" type=0x00000002> 0 </props>
    <props name="UartIntSelBase" type=0x00000002> 0x7ab040 </props>
    <props name="UartIntSelVal" type=0x00000002> 0x00000008 </props>
    <props name="PnocVoteEnable" type=0x00000002> 1 </props>
    <props name="PnocIBval" type=0x00000002> 500000 </props>
    <props name="PnocABval" type=0x00000002> 500000 </props>
    <props name="PnocArbMaster" type=0x00000002> ICBID_MASTER_BLSP_1 </props>
    <props name="PnocArbSlave" type=0x00000002> ICBID_SLAVE_EBI1 </props>
    <props name="GpioTxDataName" type=0x00000011> uartbam_4_tx_name </props>
    <props name="GpioRxDataName" type=0x00000011> uartbam_4_rx_name </props>
    <props name="GpioCtsDataName" type=0x00000011> uartbam_4_cts_name </props>
    <props name="GpioRfrDataName" type=0x00000011> uartbam_4_rfr_name </props>
    <props name="GpioTxConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA} </props>
    <props name="GpioRxConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA} </props>
    <props name="GpioCtsConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA} </props>
    <props name="GpioRfrConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA} </props>
</device>
  <!-- =================================================================== -->
  <!-- UARTBAM5 -->
  <!-- =================================================================== -->
  <device id=0x02001004>
    <props name="Irq" type=0x00000002> 104 </props>
    <props name="UartBase" type=0x00000002> 0x7573000 </props>
    <props name="UartClockName" type=0x00000011> uartbam_5_clock_name </props>
    <props name="PClockName" type=0x00000011> blsp1_ahb_clock_name </props>
    <props name="ManagePCLK" type=0x00000002> 1 </props>
    <props name="DetectBrk" type=0x00000002> 0 </props>
    <props name="UartIntSelBase" type=0x00000002> 0x7ab040 </props>
    <props name="UartIntSelVal" type=0x00000002> 0x00000010 </props>
    <props name="PnocVoteEnable" type=0x00000002> 1 </props>
    <props name="PnocIBval" type=0x00000002> 500000 </props>
    <props name="PnocABval" type=0x00000002> 500000 </props>
    <props name="PnocArbMaster" type=0x00000002> ICBID_MASTER_BLSP_1 </props>
    <props name="PnocArbSlave" type=0x00000002> ICBID_SLAVE_EBI1 </props>
    <props name="GpioTxDataName" type=0x00000011> uartbam_5_tx_name </props>
    <props name="GpioRxDataName" type=0x00000011> uartbam_5_rx_name </props>
    <props name="GpioCtsDataName" type=0x00000011> uartbam_5_cts_name </props>
    <props name="GpioRfrDataName" type=0x00000011> uartbam_5_rfr_name </props>
    <props name="GpioTxConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA} </props>
    <props name="GpioRxConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA} </props>
    <props name="GpioCtsConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA} </props>
    <props name="GpioRfrConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA} </props>
  </device>
  <!-- =================================================================== -->
  <!-- UARTBAM6 -->
  <!-- =================================================================== -->
  <device id=0x02001005>
    <props name="Irq" type=0x00000002> 104 </props>
    <props name="UartBase" type=0x00000002> 0x7574000 </props>
    <props name="UartClockName" type=0x00000011> uartbam_6_clock_name </props>
    <props name="PClockName" type=0x00000011> blsp1_ahb_clock_name </props>
    <props name="ManagePCLK" type=0x00000002> 1 </props>
    <props name="DetectBrk" type=0x00000002> 0 </props>
    <props name="UartIntSelBase" type=0x00000002> 0x7ab040 </props>
    <props name="UartIntSelVal" type=0x00000002> 0x00000020 </props>
    <props name="PnocVoteEnable" type=0x00000002> 1 </props>
    <props name="PnocIBval" type=0x00000002> 500000 </props>
    <props name="PnocABval" type=0x00000002> 500000 </props>
    <props name="PnocArbMaster" type=0x00000002> ICBID_MASTER_BLSP_1 </props>
    <props name="PnocArbSlave" type=0x00000002> ICBID_SLAVE_EBI1 </props>
    <props name="GpioTxDataName" type=0x00000011> uartbam_6_tx_name </props>
    <props name="GpioRxDataName" type=0x00000011> uartbam_6_rx_name </props>
    <props name="GpioCtsDataName" type=0x00000011> uartbam_6_cts_name </props>
    <props name="GpioRfrDataName" type=0x00000011> uartbam_6_rfr_name </props>
    <props name="GpioTxConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA} </props>
    <props name="GpioRxConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA} </props>
    <props name="GpioCtsConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA} </props>
    <props name="GpioRfrConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA} </props>
  </device>
  <!-- =================================================================== -->
  <!-- UARTBAM7 -->
  <!-- =================================================================== -->
  <device id=0x02001006>
    <props name="Irq" type=0x00000002> 104 </props>
    <props name="UartBase" type=0x00000002> 0x75af000 </props>
    <props name="UartClockName" type=0x00000011> uartbam_7_clock_name </props>
    <props name="PClockName" type=0x00000011> blsp2_ahb_clock_name </props>
    <props name="ManagePCLK" type=0x00000002> 1 </props>
    <props name="DetectBrk" type=0x00000002> 0 </props>
    <props name="UartIntSelBase" type=0x00000002> 0x7ab040 </props>
    <props name="UartIntSelVal" type=0x00000002> 0x00000040 </props>
    <props name="PnocVoteEnable" type=0x00000002> 1 </props>
    <props name="PnocIBval" type=0x00000002> 500000 </props>
    <props name="PnocABval" type=0x00000002> 500000 </props>
    <props name="PnocArbMaster" type=0x00000002> ICBID_MASTER_BLSP_2 </props>
    <props name="PnocArbSlave" type=0x00000002> ICBID_SLAVE_EBI1 </props>
    <props name="GpioTxDataName" type=0x00000011> uartbam_7_tx_name </props>
    <props name="GpioRxDataName" type=0x00000011> uartbam_7_rx_name </props>
    <props name="GpioCtsDataName" type=0x00000011> uartbam_7_cts_name </props>
    <props name="GpioRfrDataName" type=0x00000011> uartbam_7_rfr_name </props>
    <props name="GpioTxConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA} </props>
    <props name="GpioRxConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA} </props>
    <props name="GpioCtsConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA} </props>
    <props name="GpioRfrConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA} </props>
  </device>
  <!-- =================================================================== -->
  <!-- UARTBAM8 -->
  <!-- =================================================================== -->
  <device id=0x02001007>
    <props name="Irq" type=0x00000002> 104 </props>
    <props name="UartBase" type=0x00000002> 0x75b0000 </props>
    <props name="UartClockName" type=0x00000011> uartbam_8_clock_name </props>
    <props name="PClockName" type=0x00000011> blsp2_ahb_clock_name </props>
    <props name="ManagePCLK" type=0x00000002> 1 </props>
    <props name="DetectBrk" type=0x00000002> 0 </props>
    <props name="UartIntSelBase" type=0x00000002> 0x7ab040 </props>
    <props name="UartIntSelVal" type=0x00000002> 0x00000080 </props>
    <props name="PnocVoteEnable" type=0x00000002> 1 </props>
    <props name="PnocIBval" type=0x00000002> 500000 </props>
    <props name="PnocABval" type=0x00000002> 500000 </props>
    <props name="PnocArbMaster" type=0x00000002> ICBID_MASTER_BLSP_2 </props>
    <props name="PnocArbSlave" type=0x00000002> ICBID_SLAVE_EBI1 </props>
    <props name="GpioTxDataName" type=0x00000011> uartbam_8_tx_name </props>
    <props name="GpioRxDataName" type=0x00000011> uartbam_8_rx_name </props>
    <props name="GpioCtsDataName" type=0x00000011> uartbam_8_cts_name </props>
    <props name="GpioRfrDataName" type=0x00000011> uartbam_8_rfr_name </props>
    <props name="GpioTxConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA} </props>
    <props name="GpioRxConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA} </props>
    <props name="GpioCtsConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA} </props>
    <props name="GpioRfrConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA} </props>
  </device>
  <!-- =================================================================== -->
  <!-- UARTBAM9 -->
  <!-- =================================================================== -->
  <device id=0x02001008>
    <props name="Irq" type=0x00000002> 104 </props>
    <props name="UartBase" type=0x00000002> 0x75b1000 </props>
    <props name="UartClockName" type=0x00000011> uartbam_9_clock_name </props>
    <props name="PClockName" type=0x00000011> blsp2_ahb_clock_name </props>
    <props name="ManagePCLK" type=0x00000002> 1 </props>
    <props name="DetectBrk" type=0x00000002> 0 </props>
    <props name="UartIntSelBase" type=0x00000002> 0x7ab040 </props>
    <props name="UartIntSelVal" type=0x00000002> 0x00000100 </props>
    <props name="PnocVoteEnable" type=0x00000002> 1 </props>
    <props name="PnocIBval" type=0x00000002> 500000 </props>
    <props name="PnocABval" type=0x00000002> 500000 </props>
    <props name="PnocArbMaster" type=0x00000002> ICBID_MASTER_BLSP_2 </props>
    <props name="PnocArbSlave" type=0x00000002> ICBID_SLAVE_EBI1 </props>
    <props name="GpioTxDataName" type=0x00000011> uartbam_9_tx_name </props>
    <props name="GpioRxDataName" type=0x00000011> uartbam_9_rx_name </props>
    <props name="GpioCtsDataName" type=0x00000011> uartbam_9_cts_name </props>
    <props name="GpioRfrDataName" type=0x00000011> uartbam_9_rfr_name </props>
    <props name="GpioTxConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA} </props>
    <props name="GpioRxConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA} </props>
    <props name="GpioCtsConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA} </props>
    <props name="GpioRfrConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA} </props>
  </device>
  <!-- =================================================================== -->
  <!-- UARTBAM10 -->
  <!-- =================================================================== -->
  <device id=0x02001009>
    <props name="Irq" type=0x00000002> 104 </props>
    <props name="UartBase" type=0x00000002> 0x75b2000 </props>
    <props name="UartClockName" type=0x00000011> uartbam_10_clock_name </props>
    <props name="PClockName" type=0x00000011> blsp2_ahb_clock_name </props>
    <props name="ManagePCLK" type=0x00000002> 1 </props>
    <props name="DetectBrk" type=0x00000002> 0 </props>
    <props name="UartIntSelBase" type=0x00000002> 0x7ab040 </props>
    <props name="UartIntSelVal" type=0x00000002> 0x00000200 </props>
    <props name="PnocVoteEnable" type=0x00000002> 1 </props>
    <props name="PnocIBval" type=0x00000002> 500000 </props>
    <props name="PnocABval" type=0x00000002> 500000 </props>
    <props name="PnocArbMaster" type=0x00000002> ICBID_MASTER_BLSP_2 </props>
    <props name="PnocArbSlave" type=0x00000002> ICBID_SLAVE_EBI1 </props>
    <props name="GpioTxDataName" type=0x00000011> uartbam_10_tx_name </props>
    <props name="GpioRxDataName" type=0x00000011> uartbam_10_rx_name </props>
    <props name="GpioCtsDataName" type=0x00000011> uartbam_10_cts_name </props>
    <props name="GpioRfrDataName" type=0x00000011> uartbam_10_rfr_name </props>
    <props name="GpioTxConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA} </props>
    <props name="GpioRxConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA} </props>
    <props name="GpioCtsConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA} </props>
    <props name="GpioRfrConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA} </props>
  </device>
  <!-- =================================================================== -->
  <!-- UARTBAM11 -->
  <!-- =================================================================== -->
  <device id=0x0200100A>
    <props name="Irq" type=0x00000002> 104 </props>
    <props name="UartBase" type=0x00000002> 0x75b3000 </props>
    <props name="UartClockName" type=0x00000011> uartbam_11_clock_name </props>
    <props name="PClockName" type=0x00000011> blsp2_ahb_clock_name </props>
    <props name="ManagePCLK" type=0x00000002> 1 </props>
    <props name="DetectBrk" type=0x00000002> 0 </props>
    <props name="UartIntSelBase" type=0x00000002> 0x7ab040 </props>
    <props name="UartIntSelVal" type=0x00000002> 0x00000400 </props>
    <props name="PnocVoteEnable" type=0x00000002> 1 </props>
    <props name="PnocIBval" type=0x00000002> 500000 </props>
    <props name="PnocABval" type=0x00000002> 500000 </props>
    <props name="PnocArbMaster" type=0x00000002> ICBID_MASTER_BLSP_2 </props>
    <props name="PnocArbSlave" type=0x00000002> ICBID_SLAVE_EBI1 </props>
    <props name="GpioTxDataName" type=0x00000011> uartbam_11_tx_name </props>
    <props name="GpioRxDataName" type=0x00000011> uartbam_11_rx_name </props>
    <props name="GpioCtsDataName" type=0x00000011> uartbam_11_cts_name </props>
    <props name="GpioRfrDataName" type=0x00000011> uartbam_11_rfr_name </props>
    <props name="GpioTxConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA} </props>
    <props name="GpioRxConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA} </props>
    <props name="GpioCtsConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA} </props>
    <props name="GpioRfrConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA} </props>
  </device>
  <!-- =================================================================== -->
  <!-- UARTBAM12 -->
  <!-- =================================================================== -->
  <device id=0x0200100B>
    <props name="Irq" type=0x00000002> 104 </props>
    <props name="UartBase" type=0x00000002> 0x75b4000 </props>
    <props name="UartClockName" type=0x00000011> uartbam_12_clock_name </props>
    <props name="PClockName" type=0x00000011> blsp2_ahb_clock_name </props>
    <props name="ManagePCLK" type=0x00000002> 1 </props>
    <props name="DetectBrk" type=0x00000002> 0 </props>
    <props name="UartIntSelBase" type=0x00000002> 0x7ab040 </props>
    <props name="UartIntSelVal" type=0x00000002> 0x00000800 </props>
    <props name="PnocVoteEnable" type=0x00000002> 1 </props>
    <props name="PnocIBval" type=0x00000002> 500000 </props>
    <props name="PnocABval" type=0x00000002> 500000 </props>
    <props name="PnocArbMaster" type=0x00000002> ICBID_MASTER_BLSP_2 </props>
    <props name="PnocArbSlave" type=0x00000002> ICBID_SLAVE_EBI1 </props>
    <props name="GpioTxDataName" type=0x00000011> uartbam_12_tx_name </props>
    <props name="GpioRxDataName" type=0x00000011> uartbam_12_rx_name </props>
    <props name="GpioCtsDataName" type=0x00000011> uartbam_12_cts_name </props>
    <props name="GpioRfrDataName" type=0x00000011> uartbam_12_rfr_name </props>
    <props name="GpioTxConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA} </props>
    <props name="GpioRxConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA} </props>
    <props name="GpioCtsConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA} </props>
    <props name="GpioRfrConfig" type="DalTlmm_GpioConfigIdType"> {DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA} </props>
  </device>
</driver>

<!-- ================================================================================================== -->
<!-- GPIO configs. -->
<!-- -->
<!-- Source: IP Catalog -->
<!-- ================================================================================================== -->
<!--
DAL_GPIO_OUTPUT = 1
DAL_GPIO_INPUT = 0
DAL_GPIO_PULL_UP = 3
DAL_GPIO_PULL_DOWN = 1
     BLSP_UART1_TX_DATA DAL_GPIO_CFG( 0, 2, DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA )
     BLSP_UART1_RX_DATA DAL_GPIO_CFG( 1, 2, DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA )
     BLSP_UART1_CTS_N DAL_GPIO_CFG( 2, 2, DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA )
     BLSP_UART1_RFR_N DAL_GPIO_CFG( 3, 2, DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA )
     BLSP_UART2_TX_DATA DAL_GPIO_CFG( 41, 2, DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA )
     BLSP_UART2_RX_DATA DAL_GPIO_CFG( 42, 2, DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA )
     BLSP_UART2_CTS_N DAL_GPIO_CFG( 43, 2, DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA )
     BLSP_UART2_RFR_N DAL_GPIO_CFG( 44, 2, DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA )
     BLSP_UART3_TX_DATA DAL_GPIO_CFG( 45, 2, DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA )
     BLSP_UART3_RX_DATA DAL_GPIO_CFG( 46, 2, DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA )
     BLSP_UART3_CTS_N DAL_GPIO_CFG( 47, 2, DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA )
     BLSP_UART3_RFR_N DAL_GPIO_CFG( 48, 2, DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA )
     BLSP_UART4_TX_DATA DAL_GPIO_CFG( 65, 3, DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA )
     BLSP_UART4_RX_DATA DAL_GPIO_CFG( 66, 3, DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA )
     BLSP_UART4_CTS_N DAL_GPIO_CFG( 67, 3, DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA )
     BLSP_UART4_RFR_N DAL_GPIO_CFG( 68, 3, DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA )
     BLSP_UART5_TX_DATA DAL_GPIO_CFG( 81, 3, DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA )
     BLSP_UART5_RX_DATA DAL_GPIO_CFG( 82, 3, DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA )
     BLSP_UART5_CTS_N DAL_GPIO_CFG( 83, 3, DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA )
     BLSP_UART5_RFR_N DAL_GPIO_CFG( 84, 2, DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA )
     BLSP_UART6_TX_DATA DAL_GPIO_CFG( 25, 4, DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA )
     BLSP_UART6_RX_DATA DAL_GPIO_CFG( 26, 3, DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA )
     BLSP_UART6_CTS_N DAL_GPIO_CFG( 27, 2, DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA )
     BLSP_UART6_RFR_N DAL_GPIO_CFG( 28, 2, DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA )
     BLSP_UART7_TX_DATA DAL_GPIO_CFG( 53, 2, DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA )
     BLSP_UART7_RX_DATA DAL_GPIO_CFG( 54, 2, DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA )
     BLSP_UART7_CTS_N DAL_GPIO_CFG( 55, 2, DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA )
     BLSP_UART7_RFR_N DAL_GPIO_CFG( 56, 2, DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA )
     BLSP_UART8_TX_DATA DAL_GPIO_CFG( 4, 2, DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA )
     BLSP_UART8_RX_DATA DAL_GPIO_CFG( 5, 2, DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA )
     BLSP_UART8_CTS_N DAL_GPIO_CFG( 6, 2, DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA )
     BLSP_UART8_RFR_N DAL_GPIO_CFG( 7, 2, DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA )
     BLSP_UART9_TX_DATA DAL_GPIO_CFG( 49, 2, DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA )
     BLSP_UART9_RX_DATA DAL_GPIO_CFG( 50, 2, DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA )
     BLSP_UART9_CTS_N DAL_GPIO_CFG( 51, 2, DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA )
     BLSP_UART9_RFR_N DAL_GPIO_CFG( 52, 2, DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA )
     BLSP_UART10_TX_DATA DAL_GPIO_CFG( 8, 2, DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA )
     BLSP_UART10_RX_DATA DAL_GPIO_CFG( 9, 2, DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA )
     BLSP_UART10_CTS_N DAL_GPIO_CFG( 10, 4, DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA )
     BLSP_UART10_RFR_N DAL_GPIO_CFG( 11, 4, DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA )
     BLSP_UART11_TX_DATA DAL_GPIO_CFG( 58, 3, DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA )
     BLSP_UART11_RX_DATA DAL_GPIO_CFG( 59, 3, DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA )
     BLSP_UART11_CTS_N DAL_GPIO_CFG( 60, 3, DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA )
     BLSP_UART11_RFR_N DAL_GPIO_CFG( 61, 3, DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA )
     BLSP_UART12_TX_DATA DAL_GPIO_CFG( 85, 2, DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA )
     BLSP_UART12_RX_DATA DAL_GPIO_CFG( 86, 2, DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA )
     BLSP_UART12_CTS_N DAL_GPIO_CFG( 87, 2, DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA )
     BLSP_UART12_RFR_N DAL_GPIO_CFG( 88, 2, DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA )
======================================================================================================= -->
<driver name="NULL">
  <global_def>
    <string name="UART_PHY_DEVICE_1" type=0x00000001> /core/buses/uart/1 </string>
    <string name="UART_PHY_DEVICE_2" type=0x00000001> /core/buses/uart/2 </string>
    <string name="UART_PHY_DEVICE_3" type=0x00000001> /core/buses/uart/3 </string>
    <string name="UART_PHY_DEVICE_4" type=0x00000001> /core/buses/uart/4 </string>
    <string name="UART_PHY_DEVICE_5" type=0x00000001> /core/buses/uart/5 </string>
    <string name="UART_PHY_DEVICE_6" type=0x00000001> /core/buses/uart/6 </string>
    <string name="UART_PHY_DEVICE_7" type=0x00000001> /core/buses/uart/7 </string>
    <string name="UART_PHY_DEVICE_8" type=0x00000001> /core/buses/uart/8 </string>
    <string name="UART_PHY_DEVICE_9" type=0x00000001> /core/buses/uart/9 </string>
    <string name="UART_PHY_DEVICE_10" type=0x00000001> /core/buses/uart/10 </string>
    <string name="UART_PHY_DEVICE_11" type=0x00000001> /core/buses/uart/11 </string>
    <string name="UART_PHY_DEVICE_12" type=0x00000001> /core/buses/uart/12 </string>
    <string name="blsp1_ahb_clock_name" type=0x00000001> gcc_blsp1_ahb_clk </string>
    <string name="blsp2_ahb_clock_name" type=0x00000001> gcc_blsp2_ahb_clk </string>
    <string name="uartbam_1_clock_name" type=0x00000001> gcc_blsp1_uart1_apps_clk </string>
    <string name="uartbam_2_clock_name" type=0x00000001> gcc_blsp1_uart2_apps_clk </string>
    <string name="uartbam_3_clock_name" type=0x00000001> gcc_blsp1_uart3_apps_clk </string>
    <string name="uartbam_4_clock_name" type=0x00000001> gcc_blsp1_uart4_apps_clk </string>
    <string name="uartbam_5_clock_name" type=0x00000001> gcc_blsp1_uart5_apps_clk </string>
    <string name="uartbam_6_clock_name" type=0x00000001> gcc_blsp1_uart6_apps_clk </string>
    <string name="uartbam_7_clock_name" type=0x00000001> gcc_blsp2_uart1_apps_clk </string>
    <string name="uartbam_8_clock_name" type=0x00000001> gcc_blsp2_uart2_apps_clk </string>
    <string name="uartbam_9_clock_name" type=0x00000001> gcc_blsp2_uart3_apps_clk </string>
    <string name="uartbam_10_clock_name" type=0x00000001> gcc_blsp2_uart4_apps_clk </string>
    <string name="uartbam_11_clock_name" type=0x00000001> gcc_blsp2_uart5_apps_clk </string>
    <string name="uartbam_12_clock_name" type=0x00000001> gcc_blsp2_uart6_apps_clk </string>
  </global_def>
  <!-- =================================================================== -->
  <!-- TARGET SPECIFIC UART PROPERTIES -->
  <!-- =================================================================== -->
  <device id="/core/buses/uart">
      <props name="UartMainPortPhy" type=0x00000011> UART_PHY_DEVICE_8 </props>
  </device>
  <!-- =================================================================== -->
  <!-- UART1 -->
  <!-- =================================================================== -->
  <device id="/core/buses/uart/1">
    <props name="GpioRxData" type=0x00000002> 0x20008012 </props>
    <props name="GpioTxData" type=0x00000002> 0x2001C002 </props>
    <props name="GpioRfrN" type=0x00000002> 0x2001C032 </props>
    <props name="GpioCtsN" type=0x00000002> 0x20008022 </props>
    <props name="UartBase" type=0x00000002> 0x756F000 </props>
    <props name="IsLoopback" type=0x00000002> 0 </props>
    <props name="BitRate" type=0x00000002> 115200 </props>
    <props name="UartClockName" type=0x00000011> uartbam_1_clock_name </props>
    <props name="PClockName" type=0x00000011> blsp1_ahb_clock_name </props>
    <props name="Irq" type=0x00000002> 104 </props>
    <props name="UartIntSelOffset" type=0x00000002> 0x0000B040 </props>
    <props name="UartIntSelVal" type=0x00000002> 0x001 </props>
  </device>
  <!-- =================================================================== -->
  <!-- UART2 -->
  <!-- =================================================================== -->
  <device id="/core/buses/uart/2">
    <props name="GpioRxData" type=0x00000002> 0x200082A2 </props>
    <props name="GpioTxData" type=0x00000002> 0x2001C292 </props>
    <props name="GpioRfrN" type=0x00000002> 0x2001C2C2 </props>
    <props name="GpioCtsN" type=0x00000002> 0x200082B2 </props>
    <props name="UartBase" type=0x00000002> 0x7570000 </props>
    <props name="IsLoopback" type=0x00000002> 0 </props>
    <props name="BitRate" type=0x00000002> 115200 </props>
    <props name="UartClockName" type=0x00000011> uartbam_2_clock_name </props>
    <props name="PClockName" type=0x00000011> blsp1_ahb_clock_name </props>
    <props name="Irq" type=0x00000002> 104 </props>
    <props name="UartIntSelOffset" type=0x00000002> 0x0000B040 </props>
    <props name="UartIntSelVal" type=0x00000002> 0x002 </props>
  </device>
  <!-- =================================================================== -->
  <!-- UART3 -->
  <!-- =================================================================== -->
  <device id="/core/buses/uart/3">
    <props name="GpioRxData" type=0x00000002> 0x200082E2 </props>
    <props name="GpioTxData" type=0x00000002> 0x2001C2D2 </props>
    <props name="GpioRfrN" type=0x00000002> 0x2001C302 </props>
    <props name="GpioCtsN" type=0x00000002> 0x200082F2 </props>
    <props name="UartBase" type=0x00000002> 0x7571000 </props>
    <props name="IsLoopback" type=0x00000002> 0 </props>
    <props name="BitRate" type=0x00000002> 115200 </props>
    <props name="UartClockName" type=0x00000011> uartbam_3_clock_name </props>
    <props name="PClockName" type=0x00000011> blsp1_ahb_clock_name </props>
    <props name="Irq" type=0x00000002> 104 </props>
    <props name="UartIntSelOffset" type=0x00000002> 0x0000B040 </props>
    <props name="UartIntSelVal" type=0x00000002> 0x004 </props>
  </device>
  <!-- =================================================================== -->
  <!-- UART4 -->
  <!-- =================================================================== -->
  <device id="/core/buses/uart/4">
    <props name="GpioRxData" type=0x00000002> 0x20008423 </props>
    <props name="GpioTxData" type=0x00000002> 0x2001C413 </props>
    <props name="GpioRfrN" type=0x00000002> 0x2001C443 </props>
    <props name="GpioCtsN" type=0x00000002> 0x20008433 </props>
    <props name="UartBase" type=0x00000002> 0x7572000 </props>
    <props name="IsLoopback" type=0x00000002> 0 </props>
    <props name="BitRate" type=0x00000002> 115200 </props>
    <props name="UartClockName" type=0x00000011> uartbam_4_clock_name </props>
    <props name="PClockName" type=0x00000011> blsp1_ahb_clock_name </props>
    <props name="Irq" type=0x00000002> 104 </props>
    <props name="UartIntSelOffset" type=0x00000002> 0x0000B040 </props>
    <props name="UartIntSelVal" type=0x00000002> 0x008 </props>
  </device>
  <!-- =================================================================== -->
  <!-- UART5 -->
  <!-- =================================================================== -->
  <device id="/core/buses/uart/5">
    <props name="GpioRxData" type=0x00000002> 0x20008523 </props>
    <props name="GpioTxData" type=0x00000002> 0x2001C513 </props>
    <props name="GpioRfrN" type=0x00000002> 0x2001C542 </props>
    <props name="GpioCtsN" type=0x00000002> 0x20008533 </props>
    <props name="UartBase" type=0x00000002> 0x7573000 </props>
    <props name="IsLoopback" type=0x00000002> 0 </props>
    <props name="BitRate" type=0x00000002> 115200 </props>
    <props name="UartClockName" type=0x00000011> uartbam_5_clock_name </props>
    <props name="PClockName" type=0x00000011> blsp1_ahb_clock_name </props>
    <props name="Irq" type=0x00000002> 104 </props>
    <props name="UartIntSelOffset" type=0x00000002> 0x0000B040 </props>
    <props name="UartIntSelVal" type=0x00000002> 0x010 </props>
  </device>
  <!-- =================================================================== -->
  <!-- UART6 -->
  <!-- =================================================================== -->
  <device id="/core/buses/uart/6">
    <props name="GpioRxData" type=0x00000002> 0x200081A3 </props>
    <props name="GpioTxData" type=0x00000002> 0x2001C194 </props>
    <props name="GpioRfrN" type=0x00000002> 0x2001C1C2 </props>
    <props name="GpioCtsN" type=0x00000002> 0x200081B2 </props>
    <props name="UartBase" type=0x00000002> 0x7574000 </props>
    <props name="IsLoopback" type=0x00000002> 0 </props>
    <props name="BitRate" type=0x00000002> 115200 </props>
    <props name="UartClockName" type=0x00000011> uartbam_6_clock_name </props>
    <props name="PClockName" type=0x00000011> blsp1_ahb_clock_name </props>
    <props name="Irq" type=0x00000002> 104 </props>
    <props name="UartIntSelOffset" type=0x00000002> 0x0000B040 </props>
    <props name="UartIntSelVal" type=0x00000002> 0x020 </props>
  </device>
  <!-- =================================================================== -->
  <!-- UART7 -->
  <!-- =================================================================== -->
  <device id="/core/buses/uart/7">
    <props name="GpioRxData" type=0x00000002> 0x20008362 </props>
    <props name="GpioTxData" type=0x00000002> 0x2001C352 </props>
    <props name="GpioRfrN" type=0x00000002> 0x2001C382 </props>
    <props name="GpioCtsN" type=0x00000002> 0x20008372 </props>
    <props name="UartBase" type=0x00000002> 0x75AF000 </props>
    <props name="IsLoopback" type=0x00000002> 0 </props>
    <props name="BitRate" type=0x00000002> 115200 </props>
    <props name="UartClockName" type=0x00000011> uartbam_7_clock_name </props>
    <props name="PClockName" type=0x00000011> blsp2_ahb_clock_name </props>
    <props name="Irq" type=0x00000002> 104 </props>
    <props name="UartIntSelOffset" type=0x00000002> 0x0000B040 </props>
    <props name="UartIntSelVal" type=0x00000002> 0x040 </props>
  </device>
  <!-- =================================================================== -->
  <!-- UART8 -->
  <!-- =================================================================== -->
  <device id="/core/buses/uart/8">
    <props name="GpioRxData" type=0x00000002> 0x20008052 </props>
    <props name="GpioTxData" type=0x00000002> 0x2001C042 </props>
    <!-- On CDPs, the flow control lines are not present and UART driver does not support flow control.
    <props name="GpioRfrN" type=0x00000002> 0x2001C072 </props>
    <props name="GpioCtsN" type=0x00000002> 0x20008062 </props>
    -->
    <props name="UartBase" type=0x00000002> 0x75B0000 </props>
    <props name="IsLoopback" type=0x00000002> 0 </props>
    <props name="BitRate" type=0x00000002> 115200 </props>
    <props name="UartClockName" type=0x00000011> uartbam_8_clock_name </props>
    <props name="PClockName" type=0x00000011> blsp2_ahb_clock_name </props>
    <props name="Irq" type=0x00000002> 104 </props>
    <props name="UartIntSelOffset" type=0x00000002> 0x0000B040 </props>
    <props name="UartIntSelVal" type=0x00000002> 0x080 </props>
  </device>
  <!-- =================================================================== -->
  <!-- UART9 -->
  <!-- =================================================================== -->
  <device id="/core/buses/uart/9">
    <props name="GpioRxData" type=0x00000002> 0x20008322 </props>
    <props name="GpioTxData" type=0x00000002> 0x2001C312 </props>
    <props name="GpioRfrN" type=0x00000002> 0x2001C342 </props>
    <props name="GpioCtsN" type=0x00000002> 0x20008332 </props>
    <props name="UartBase" type=0x00000002> 0x75B1000 </props>
    <props name="IsLoopback" type=0x00000002> 0 </props>
    <props name="BitRate" type=0x00000002> 115200 </props>
    <props name="UartClockName" type=0x00000011> uartbam_9_clock_name</props>
    <props name="PClockName" type=0x00000011> blsp2_ahb_clock_name </props>
    <props name="Irq" type=0x00000002> 104 </props>
    <props name="UartIntSelOffset" type=0x00000002> 0x0000B040 </props>
    <props name="UartIntSelVal" type=0x00000002> 0x100 </props>
  </device>
  <!-- =================================================================== -->
  <!-- UART10 -->
  <!-- =================================================================== -->
  <device id="/core/buses/uart/10">
    <props name="GpioRxData" type=0x00000002> 0x20008092 </props>
    <props name="GpioTxData" type=0x00000002> 0x2001C082 </props>
    <props name="GpioRfrN" type=0x00000002> 0x2001C0B4 </props>
    <props name="GpioCtsN" type=0x00000002> 0x200080A4 </props>
    <props name="UartBase" type=0x00000002> 0x75B2000 </props>
    <props name="IsLoopback" type=0x00000002> 0 </props>
    <props name="BitRate" type=0x00000002> 115200 </props>
    <props name="UartClockName" type=0x00000011> uartbam_10_clock_name</props>
    <props name="PClockName" type=0x00000011> blsp2_ahb_clock_name </props>
    <props name="Irq" type=0x00000002> 104 </props>
    <props name="UartIntSelOffset" type=0x00000002> 0x0000B040 </props>
    <props name="UartIntSelVal" type=0x00000002> 0x200 </props>
  </device>
  <!-- =================================================================== -->
  <!-- UART11 -->
  <!-- =================================================================== -->
  <device id="/core/buses/uart/11">
    <props name="GpioRxData" type=0x00000002> 0x200083B3 </props>
    <props name="GpioTxData" type=0x00000002> 0x2001C3A3 </props>
    <props name="GpioRfrN" type=0x00000002> 0x2001C3D3 </props>
    <props name="GpioCtsN" type=0x00000002> 0x200083C3 </props>
    <props name="UartBase" type=0x00000002> 0x75B3000 </props>
    <props name="IsLoopback" type=0x00000002> 0 </props>
    <props name="BitRate" type=0x00000002> 115200 </props>
    <props name="UartClockName" type=0x00000011> uartbam_11_clock_name</props>
    <props name="PClockName" type=0x00000011> blsp2_ahb_clock_name </props>
    <props name="Irq" type=0x00000002> 104 </props>
    <props name="UartIntSelOffset" type=0x00000002> 0x0000B040 </props>
    <props name="UartIntSelVal" type=0x00000002> 0x400 </props>
  </device>
  <!-- =================================================================== -->
  <!-- UART12 -->
  <!-- =================================================================== -->
  <device id="/core/buses/uart/12">
    <props name="GpioRxData" type=0x00000002> 0x20008562 </props>
    <props name="GpioTxData" type=0x00000002> 0x2001C552 </props>
    <props name="GpioRfrN" type=0x00000002> 0x2001C582 </props>
    <props name="GpioCtsN" type=0x00000002> 0x20008572 </props>
    <props name="UartBase" type=0x00000002> 0x75B4000 </props>
    <props name="IsLoopback" type=0x00000002> 0 </props>
    <props name="BitRate" type=0x00000002> 115200 </props>
    <props name="UartClockName" type=0x00000011> uartbam_12_clock_name</props>
    <props name="PClockName" type=0x00000011> blsp2_ahb_clock_name </props>
    <props name="Irq" type=0x00000002> 104 </props>
    <props name="UartIntSelOffset" type=0x00000002> 0x0000B040 </props>
    <props name="UartIntSelVal" type=0x00000002> 0x800 </props>
  </device>
</driver>

<!-- ================================================================================================== -->
<!-- GPIO configs. -->
<!-- -->
<!-- Source: IP Catalog -->
<!-- ================================================================================================== -->
<!--
======================================================================================================= -->
<driver name="UartLite">
  <!-- =================================================================== -->
  <!-- TARGET SPECIFIC UART PROPERTIES -->
  <!-- =================================================================== -->
  <device id="UartMainPort">
      <props name="IsRemotable" type=0x00000002> 0x1 </props>
  </device>
  <device id="UartSecondPort">
      <props name="IsRemotable" type=0x00000002> 0x1 </props>
  </device>
  <device id="UartThirdPort">
      <props name="IsRemotable" type=0x00000002> 0x1 </props>
  </device>
</driver>

<driver name="System">
   <global_def>
      <var_seq name="g_target_cfg" type=0x00000003>
         8996, 0x4050, end
      </var_seq>
   </global_def>
   <device id="0x0">
      <props name="NumSysWorkLoops" type=0x00000002>
         0x0
      </props>
      <props name="DalEnv" type=0x00000002>
         0x20000001
      </props>
      <props name="TargetCfg" type=0x00000014>
         g_target_cfg
      </props>
      <props name="GlbCtxtHWMutexNumber" type=0x00000002>
         0x9
      </props>
   </device>
</driver>
<driver name="Timer">
   <device id=0x02000003>
      <props name="IsRemotable" type=0x00000002>
         0x1
      </props>
   </device>
</driver>
<driver name="SystemCall">
   <device id=0x02000005>
   </device>
</driver>

enum_header_path "err_inject_crash.h"
typedef enum
{
  ERR_INJECT_ERR_FATAL,
  ERR_INJECT_WDOG_TIMEOUT,
  ERR_INJECT_NULLPTR,
  ERR_INJECT_DIV0,
  ERR_INJECT_DYNAMIC_ERR_FATAL,
  SIZEOF_ERR_INJECT_CRASH_TYPE
} err_inject_crash_type;
void err_inject_crash_init(void);
<driver name="NULL">
   <device id="tms_eic">
    <!--
    Enable automatic crashing.
      0x00 = disable
      0x01 = enable)
    Note: QXDM crash injection is independent of this configuration
    -->
    <props name="eic_crash_enable" type=0x00000002>
      0x00
    </props>
    <!--
    Type of crash to execute.
      ERR_INJECT_ERR_FATAL
      ERR_INJECT_WDOG_TIMEOUT
      ERR_INJECT_NULLPTR
      ERR_INJECT_DIV0
    Note: above list may not accurately reflect all options
          see err_inject_crash.h for full list
    -->
    <props name="eic_crash_type" type=0x00000002>
      ERR_INJECT_ERR_FATAL
    </props>
    <!--
    Delay to wait before initiating the crash
    Values below ERR_INJECT_CRASH_DELAY_DEFAULT will be serviced as default
    -->
    <props name="eic_crash_delay" type=0x00000002>
      90
    </props>
  </device>
</driver>

<driver name="NULL">
   <device id="pd_mon_restart_dalcfg">
      <!--
      Enable PD restart.
      0x00 = disable
      0x01 = enable)
      -->
      <props name="pd_mon_restart_enable" type=0x00000002>
         0x00
      </props>
   </device>
</driver>

<driver name="NULL">
  <device id="tms_diag">
    <props name="image_id" type=0x00000002>
      48
    </props>
  </device>
</driver>

<driver name="HWEvent">
<device id="DALDEVICEID_HWEVENT">
    <props name="hwevent_config" type=0x00000012>
     hwevent_config_table
    </props>
    <props name="hwevent_config_size" type=0x00000012>
     table_size_array
    </props>
    <props name="hwevent_addr_check" type=0x00000012>
     hwevent_addr_table
    </props>
    <props name="hwevent_addr_check_size" type=0x00000012>
     addr_range_table_size
    </props>
</device>
</driver>

<driver name="STMCfg">
  <device id=0x0200014D>
    <props name="stm_claim_tag" type=0x00000002>
      0x1
    </props>
 <props name="stm_phys_addr" type=0x00000002>
      0x03002000
    </props>
  </device>
</driver>

<driver name="STMTrace">
  <device id=0x0200014E>
 <props name="stm_sp_base_addr" type=0x00000002>
      0x08000000
    </props>
 <props name="stm_base_port" type=0x00000002>
      3072
    </props>
 <props name="stm_num_ports" type=0x00000002>
      768
    </props>
  </device>
</driver>

<driver name="TFunnel">
   <global_def>
      <var_seq name="tfunnel_phys_addr_arr" type=0x00000003>
        0x03021000,0x03022000,0x03023000,end
      </var_seq>
   </global_def>
  <device id=0x0200014F>
    <props name="num_tfunnels" type=0x00000002>
     0x3
    </props>
    <props name="tfunnel_phys_addrs" type=0x00000014>
     tfunnel_phys_addr_arr
    </props>
    <props name="port_rpm" type=0x00000012>
      tfunnel_port_rpm
    </props>
    <props name="port_mpss" type=0x00000012>
      tfunnel_port_mpss
    </props>
    <props name="port_adsp" type=0x00000012>
      tfunnel_port_adsp
    </props>
    <props name="port_system_noc" type=0x00000012>
      tfunnel_port_system_noc
    </props>
    <props name="port_apps_etm" type=0x00000012>
      tfunnel_port_apps_etm
    </props>
    <props name="port_mmss_noc" type=0x00000012>
      tfunnel_port_mmss_noc
    </props>
    <props name="port_peripheral_noc" type=0x00000012>
      tfunnel_port_peripheral_noc
    </props>
    <props name="port_rpm_itm" type=0x00000012>
      tfunnel_port_rpm_itm
    </props>
    <props name="port_mmss" type=0x00000012>
      tfunnel_port_mmss
    </props>
    <props name="port_pronto" type=0x00000012>
      tfunnel_port_pronto
    </props>
    <props name="port_bimc" type=0x00000012>
      tfunnel_port_bimc
    </props>
    <props name="port_modem" type=0x00000012>
      tfunnel_port_modem
    </props>
    <props name="port_ocmem_noc" type=0x00000012>
      tfunnel_port_ocmem_noc
    </props>
    <props name="port_stm" type=0x00000012>
      tfunnel_port_stm
    </props>
  </device>
</driver>

<driver name="TMC">
  <device id="DALDEVICEID_TMC">
  </device>
</driver>

<driver name="NULL">
  <global_def>
  </global_def>
  <device id="/core/hwengines/bam">
    <props name="bam_tgt_config" type=0x00000012>
         bam_tgt_config
    </props>
  </device>
 </driver>

<!--
  This XML file contains target specific information for IPC Router
  driver for targets using device config feature.
  Copyright (c) 2013 Qualcomm Technologies, Inc. All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.
-->
<driver name="NULL">
  <device id="/dev/core/mproc/ipc_router">
    <props name="local_processor_id" type=0x00000002>
      5
    </props>
  </device>
</driver>

<driver name="NULL">
  <global_def></global_def>
  <device id="/core/mproc/smd">
    <props name="smd_intr_enabled" type= 0x00000002>0x4f</props>
  </device>
</driver>
<!-- Disable interrupts on edges where remote endpoint is not present.
     Corresponding bit is 0 if processor is not supported
  0100 1111 = 0x4f (configuration for msm)
  Below is bit position for each processor
  APPS = 0x1 (0000 0001)
  MODEM = 0x2 (0000 0010)
  ADSP = 0x4 (0000 0100)
  SSC = 0x8 (0000 1000)
  WCNSS = 0x10 (0001 0000)
  MdmFW = 0x20 (0010 0000)
  RPM = 0x40 (0100 0000)
-->

<driver name="NULL">
  <global_def></global_def>
  <device id="/core/mproc/smp2p">
    <props name="smp2p_intr_enabled" type= 0x00000008>
      0x1,
      0x1,
      0x1,
      0x1,
      0x0,
      0x0,
      0x1,
      end
    </props>
  </device>
</driver>

enum_header_path "adsppm_utils.h"
enum_header_path "adsppm.h"
enum_header_path "adsppm_types.h"
enum_header_path "adsppm_defs.h"
enum_header_path "coreUtils.h"
enum_header_path "DALFramework.h"
enum_header_path "stringl.h"
enum_header_path "core_internal.h"
enum_header_path "asic.h"
enum_header_path "DDIIPCInt.h"
enum_header_path "mmpm.h"
enum_header_path "qurt_memory.h"
enum_header_path "qurt_error.h"
enum_header_path "qurt_types.h"
enum_header_path "qurt_consts.h"
enum_header_path "ULog.h"
enum_header_path "ULogFront.h"
enum_header_path "msg.h"
enum_header_path "msg_diag_service.h"
enum_header_path "customer.h"
enum_header_path "custtarget.h"
enum_header_path "comdef.h"
enum_header_path "target.h"
enum_header_path "armasm.h"
enum_header_path "qw.h"
enum_header_path "msg_qsr.h"
enum_header_path "msg_pkt_defs.h"
enum_header_path "msgcfg.h"
enum_header_path "msgtgt.h"
enum_header_path "msg_mask.h"
enum_header_path "qurt.h"
enum_header_path "qurt_alloc.h"
enum_header_path "qurt_futex.h"
enum_header_path "qurt_mutex.h"
enum_header_path "qurt_pipe.h"
enum_header_path "qurt_sem.h"
enum_header_path "qurt_printf.h"
enum_header_path "qurt_assert.h"
enum_header_path "qurt_thread.h"
enum_header_path "qurt_trace.h"
enum_header_path "qurt_cycles.h"
enum_header_path "qurt_cond.h"
enum_header_path "qurt_rmutex2.h"
enum_header_path "qurt_barrier.h"
enum_header_path "qurt_fastint.h"
enum_header_path "qurt_allsignal.h"
enum_header_path "qurt_anysignal.h"
enum_header_path "qurt_signal.h"
enum_header_path "qurt_rmutex.h"
enum_header_path "qurt_pimutex.h"
enum_header_path "qurt_signal2.h"
enum_header_path "qurt_pimutex2.h"
enum_header_path "qurt_int.h"
enum_header_path "qurt_lifo.h"
enum_header_path "qurt_power.h"
enum_header_path "qurt_event.h"
enum_header_path "qurt_pmu.h"
enum_header_path "qurt_tlb.h"
enum_header_path "qurt_qdi.h"
enum_header_path "qurt_qdi_constants.h"
enum_header_path "qurt_qdi_imacros.h"
enum_header_path "qurt_sclk.h"
enum_header_path "qurt_space.h"
enum_header_path "qurt_process.h"
enum_header_path "qurt_shmem.h"
enum_header_path "qurt_timer.h"
enum_header_path "qurt_tls.h"
enum_header_path "qurt_thread_context.h"
enum_header_path "qurt_hvx.h"
enum_header_path "qurt_mailbox.h"
enum_header_path "qurt_island.h"
typedef unsigned long int uint32;
typedef unsigned short uint16;
typedef unsigned char uint8;
typedef signed long int int32;
typedef signed short int16;
typedef signed char int8;
typedef unsigned long long uint64;
typedef long long int64;
typedef unsigned char byte;
typedef uint32 DALBOOL;
typedef uint32 DALDEVICEID;
typedef uint32 DalPowerCmd;
typedef uint32 DalPowerDomain;
typedef uint32 DalSysReq;
typedef uint32 DALHandle;
typedef int DALResult;
typedef void * DALEnvHandle;
typedef void * DALSYSEventHandle;
typedef uint32 DALMemAddr;
typedef uint32 DALSYSMemAddr;
typedef uint64 DALSYSPhyAddr;
typedef uint32 DALInterfaceVersion;
typedef unsigned char * DALDDIParamPtr;
typedef struct DALEventObject DALEventObject;
struct DALEventObject
{
    uint32 obj[8];
};
typedef DALEventObject * DALEventHandle;
typedef struct _DALMemObject
{
   uint32 memAttributes;
   uint32 sysObjInfo[2];
   uint32 dwLen;
   uint32 ownerVirtAddr;
   uint32 virtAddr;
   uint32 physAddr;
}
DALMemObject;
typedef struct _DALDDIMemBufDesc
{
   uint32 dwOffset;
   uint32 dwLen;
   uint32 dwUser;
}
DALDDIMemBufDesc;
typedef struct _DALDDIMemDescList
{
   uint32 dwFlags;
   uint32 dwNumBufs;
   DALDDIMemBufDesc bufList[1];
}
DALDDIMemDescList;
typedef struct DALSysMemDescBuf DALSysMemDescBuf;
struct DALSysMemDescBuf
{
   DALSYSMemAddr VirtualAddr;
   DALSYSMemAddr PhysicalAddr;
   uint32 size;
   uint32 user;
};
typedef struct { uint32 dwObjInfo; DALSYSMemAddr thisVirtualAddr; DALSYSMemAddr PhysicalAddr; DALSYSMemAddr VirtualAddr; uint32 hOwnerProc; uint32 dwCurBufIdx; uint32 dwNumDescBufs; DALSysMemDescBuf BufInfo[1];} DALSysMemDescList;
typedef struct DalDeviceInfo DalDeviceInfo;
struct DalDeviceInfo
{
   uint32 sizeOfActual;
   uint32 Version;
   char Name[32];
};
typedef struct DalDeviceStatus DalDeviceStatus;
struct DalDeviceStatus
{
   uint32 sizeOfActual;
   uint32 Status;
};
typedef struct DalDeviceHandle DalDeviceHandle;
typedef struct DalDevice DalDevice;
struct DalDevice
{
   DALResult (*Attach)(const char*,DALDEVICEID,DalDeviceHandle **);
   uint32 (*Detach)(DalDeviceHandle *);
   DALResult (*Init)(DalDeviceHandle *);
   DALResult (*DeInit)(DalDeviceHandle *);
   DALResult (*Open)(DalDeviceHandle *, uint32);
   DALResult (*Close)(DalDeviceHandle *);
   DALResult (*Info)(DalDeviceHandle *, DalDeviceInfo *, uint32);
   DALResult (*PowerEvent)(DalDeviceHandle *, DalPowerCmd, DalPowerDomain);
   DALResult (*SysRequest)(DalDeviceHandle *, DalSysReq, const void *, uint32,
                           void *,uint32, uint32*);
};
typedef struct DalInterface DalInterface;
struct DalInterface
{
   struct DalDevice DalDevice;
};
struct DalDeviceHandle
{
   uint32 dwDalHandleId;
   const DalInterface *pVtbl;
   void *pClientCtxt;
   uint32 dwVtblLen;
};
typedef struct DalDeviceHandle * DALDEVICEHANDLE;
typedef struct DalRemoteHandle DalRemoteHandle;
typedef struct DalRemote DalRemote;
struct DalRemote
{
   struct DalDevice DalDevice;
   DALResult (* FCN_0) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1 );
   DALResult (* FCN_1) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32 u2 );
   DALResult (* FCN_2) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32* p_u2 );
   DALResult (* FCN_3) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32 u2, uint32 u3 );
   DALResult (* FCN_4) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32 u2, uint32* p_u3 );
   DALResult (* FCN_5) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen);
   DALResult (* FCN_6) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * ibuf, uint32 ilen);
   DALResult (* FCN_7) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * obuf, uint32 olen, uint32 *oalen);
   DALResult (* FCN_8) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * obuf, uint32 olen);
   DALResult (* FCN_9) ( uint32 ddi_idx, DalDeviceHandle *h, void * obuf, uint32 olen );
   DALResult (* FCN_10)( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * ibuf, uint32 ilen, void * obuf, uint32 olen, uint32 * oalen);
   DALResult (* FCN_11) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * obuf, uint32 olen);
   DALResult (* FCN_12) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * obuf, uint32 olen, uint32 *oalen);
   DALResult (* FCN_13) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * ibuf2, uint32 ilen2, void * obuf, uint32 olen);
   DALResult (* FCN_14) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * obuf1, uint32 olen, void * obuf2, uint32 olen2, uint32 * oalen);
   DALResult (* FCN_15) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * ibuf2, uint32 ilen2, void * obuf2, uint32 olen2, uint32 *oalen, void * obuf, uint32 olen );
};
struct DalRemoteHandle
{
   uint32 dwDalHandleId;
   const DalRemote *pVtbl;
   void *pClientCtxt;
};
static __inline uint32
DalDevice_Detach(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Detach(_h);
}
static __inline DALResult
DalDevice_Init(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Init(_h);
}
static __inline DALResult
DalDevice_DeInit(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.DeInit(_h);
}
static __inline DALResult
DalDevice_PowerEvent(DalDeviceHandle *_h, DalPowerCmd PowerCmd,
                     DalPowerDomain PowerDomain)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.PowerEvent(_h,PowerCmd,PowerDomain);
}
static __inline DALResult
DalDevice_Open(DalDeviceHandle *_h, uint32 mode)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Open(_h,mode);
}
static __inline DALResult
DalDevice_Close(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Close(_h);
}
static __inline DALResult
DalDevice_Info(DalDeviceHandle *_h, DalDeviceInfo* info, uint32 infoSize)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Info(_h,info,infoSize);
}
static __inline DALResult
DalDevice_SysRequest(DalDeviceHandle *_h, DalSysReq ReqIdx,
                     const unsigned char* SrcBuf, int SrcBufLen,
                     unsigned char* DestBuf, uint32 DestBufLen, uint32* DestBufLenReq)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.SysRequest(_h,ReqIdx,SrcBuf,SrcBufLen,
                                          DestBuf,DestBufLen,DestBufLenReq);
}
DALResult
DAL_DeviceAttach(DALDEVICEID DeviceId,
                 DalDeviceHandle **phDevice);
DALResult
DAL_DeviceAttachLocal(const char *pszArg,DALDEVICEID DeviceId,
                      DalDeviceHandle **phDevice);
DALResult
DAL_DeviceAttachEx(const char *pszArg, DALDEVICEID DeviceId,
               DALInterfaceVersion ClientVersion,DalDeviceHandle **phDevice);
DALResult
DAL_StringDeviceAttachEx(const char *pszArg,
                   const char *pszDevName,
                   DALInterfaceVersion ClientVersion,
                   DalDeviceHandle **phDalDevice);
DALResult
DAL_DeviceAttachRemote(const char *pszArg,
                   DALDEVICEID DevId,
                   DALInterfaceVersion ClientVersion,
                   DalDeviceHandle **phDALDevice);
DALResult
DAL_DeviceDetach(DalDeviceHandle *hDevice);
DALResult
DAL_StringDeviceAttach(const char *pszDevName,DalDeviceHandle **phDalDevice);
typedef struct DALREG_DriverInfo DALREG_DriverInfo;
struct DALREG_DriverInfo
{
 DALResult (*pfnDALNewFunc)(const char * , DALDEVICEID, DalDeviceHandle**);
 uint32 dwNumDevices;
 DALDEVICEID *pDeviceId;
};
typedef struct DALREG_DriverInfoList DALREG_DriverInfoList;
struct DALREG_DriverInfoList
{
 uint32 dwLen;
 DALREG_DriverInfo ** pDriverInfo;
};
typedef void * DALSYSObjHandle;
typedef void * DALSYSSyncHandle;
typedef void * DALSYSMemHandle;
typedef void * DALSYSWorkLoopHandle;
typedef uint32 *DALSYSPropertyHandle;
typedef struct DALSYSPropertyVar DALSYSPropertyVar;
struct DALSYSPropertyVar
{
    uint32 dwType;
    uint32 dwLen;
    union
    {
        byte *pbVal;
        char *pszVal;
        uint32 dwVal;
        uint32 *pdwVal;
        const void *pStruct;
    }Val;
};
typedef struct DALSYSPropStructTblType DALSYSPropStructTblType ;
struct DALSYSPropStructTblType{
   uint32 dwSize;
   const void *pStruct;
};
typedef struct StringDevice StringDevice;
 struct StringDevice{
   char *pszName;
   uint32 dwHash;
   uint32 dwOffset;
   DALREG_DriverInfo *pFunctionName;
   uint32 dwNumCollision;
   uint32 *pdwCollisions;
};
typedef struct DALProps DALProps;
struct DALProps
{
   const byte *pDALPROP_PropBin;
   const DALSYSPropStructTblType *pDALPROP_StructPtrs;
   const uint32 dwDeviceSize;
   const StringDevice *pDevices;
};
typedef struct DALSYSBaseObj DALSYSBaseObj;
struct DALSYSBaseObj
{
   uint32 dwObjInfo;
   uint32 hOwnerProc;
   DALSYSMemAddr thisVirtualAddr;
};
typedef struct DALSYSEventObj DALSYSEventObj;
struct DALSYSEventObj
{
   unsigned long long _bSpace[80/sizeof(unsigned long long)];
};
typedef struct DALSYSSyncObj DALSYSSyncObj;
struct DALSYSSyncObj
{
   unsigned long long _bSpace[40/sizeof(unsigned long long)];
};
typedef struct DALSYSMemObj DALSYSMemObj;
struct DALSYSMemObj
{
   unsigned long long _bSpace[40/sizeof(unsigned long long)];
};
typedef struct DALSYSWorkLoopEventObj DALSYSWorkLoopEventObj;
struct DALSYSWorkLoopEventObj
{
   unsigned long long _bSpace[32/sizeof(unsigned long long)];
};
typedef struct DALSYSWorkLoopObj DALSYSWorkLoopObj;
struct DALSYSWorkLoopObj
{
   unsigned long long _bSpace[64/sizeof(unsigned long long)];
};
typedef void * DALSYSCallbackFuncCtx;
typedef void * (*DALSYSCallbackFunc)(void*,uint32,void*,uint32);
typedef struct DALSYSMemInfo DALSYSMemInfo;
struct DALSYSMemInfo
{
    DALSYSMemAddr VirtualAddr;
    DALSYSMemAddr PhysicalAddr;
    uint32 dwLen;
    uint32 dwMappedLen;
    uint32 dwProps;
};
typedef enum
{
   DALSYS_MEM_CACHE_DEVICE,
   DALSYS_MEM_CACHE_NONE,
   DALSYS_MEM_CACHE_WRITEBACK,
   DALSYS_MEM_CACHE_WRITETHROUGH,
   DALSYS_MEM_CACHE_INVALID
}
DALSYSMemCacheType;
typedef enum
{
   DALSYS_MEM_PERM_NONE=0,
   DALSYS_MEM_PERM_R=0x1,
   DALSYS_MEM_PERM_W=0x2,
   DALSYS_MEM_PERM_RW=0x3,
   DALSYS_MEM_PERM_X=0x4,
   DALSYS_MEM_PERM_RX=0x5,
   DALSYS_MEM_PERM_WX=0x6,
   DALSYS_MEM_PERM_RWX=0x7,
   DALSYS_MEM_PERM_INVALID=0x8
}
DALSYSMemPermType;
typedef enum
{
   DALSYS_MEM_SHARE_ALLOWED,
   DALSYS_MEM_SHARE_NOT_ALLOWED,
   DALSYS_MEM_SHARE_INVALID
}
DALSYSMemShareType;
typedef struct DALSYSMemInfoEx DALSYSMemInfoEx;
struct DALSYSMemInfoEx
{
    DALSYSPhyAddr physicalAddr;
    DALSYSMemAddr virtualAddr;
    DALSYSMemAddr size;
    DALSYSMemCacheType cacheType;
    DALSYSMemPermType permission;
    DALSYSMemShareType shareType;
};
typedef struct DALSYSMemReq DALSYSMemReq;
struct DALSYSMemReq
{
    DALSYSMemInfoEx memInfo;
    DALSYSMemObj *pObj;
    DALBOOL prealloc;
};
typedef enum
{
   DEVCFG_TARGET_INFO_SOC,
   DEVCFG_TARGET_INFO_PLATFORM
}DEVCFG_TARGET_INFO_TYPE;
typedef DALResult
(*DALSYSWorkLoopExecute)
(
    DALSYSEventHandle,
    void *
);
typedef void
(*DALSYS_InitSystemHandleFncPtr)
(
    DalDeviceHandle * hDalDevice
);
typedef DALResult
(*DALSYS_DestroyObjectFncPtr)
(
    DALSYSObjHandle
);
typedef DALResult
(*DALSYS_CopyObjectFncPtr)
(
    DALSYSObjHandle,
    DALSYSObjHandle *
);
typedef DALResult
(*DALSYS_RegisterWorkLoopFncPtr)
(
    uint32,
    uint32,
    DALSYSWorkLoopHandle *,
    DALSYSWorkLoopObj *
);
typedef DALResult
(*DALSYS_RegisterWorkLoopExFncPtr)
(
    char *,
    uint32,
    uint32,
    uint32,
    DALSYSWorkLoopHandle *,
    DALSYSWorkLoopObj *
);
typedef DALResult
(*DALSYS_AddEventToWorkLoopFncPtr)
(
    DALSYSWorkLoopHandle,
    DALSYSWorkLoopExecute,
    void *,
    DALSYSEventHandle,
    DALSYSSyncHandle
);
typedef DALResult
(*DALSYS_DeleteEventFromWorkLoopFncPtr)
(
    DALSYSWorkLoopHandle,
    DALSYSEventHandle
);
typedef DALResult
(*DALSYS_EventCreateFncPtr)
(
    uint32 ,
    DALSYSEventHandle *,
    DALSYSEventObj *
);
typedef DALResult
(*DALSYS_EventCtrlFncPtr)
(
    DALSYSEventHandle,
    uint32,
   uint32,
   void *,
   uint32
);
typedef DALResult
(*DALSYS_EventWaitFncPtr)
(
    DALSYSEventHandle
);
typedef DALResult
(*DALSYS_EventMultipleWaitFncPtr)
(
    DALSYSEventHandle*,
    int,
    uint32,
    uint32 *
);
typedef DALResult
(*DALSYS_SetupCallbackEventFncPtr)
(
    DALSYSEventHandle,
    DALSYSCallbackFunc,
    DALSYSCallbackFuncCtx
);
typedef DALResult
(*DALSYS_SyncCreateFncPtr)
(
    uint32,
    DALSYSSyncHandle *,
    DALSYSSyncObj *
);
typedef void
(*DALSYS_SyncEnterFncPtr)
(
    DALSYSSyncHandle
);
typedef DALResult
(*DALSYS_SyncTryEnterFncPtr)
(
    DALSYSSyncHandle
);
typedef void
(*DALSYS_SyncLeaveFncPtr)
(
    DALSYSSyncHandle
);
typedef DALResult
(*DALSYS_MemRegionAllocFncPtr)
(
    uint32,
    DALSYSMemAddr,
    DALSYSMemAddr,
    uint32,
    DALSYSMemHandle *,
    DALSYSMemObj *
);
typedef DALResult
(*DALSYS_MemRegionMapPhysFncPtr)
(
    DALSYSMemHandle,
    uint32,
    DALSYSMemAddr,
    uint32
);
typedef DALResult
(*DALSYS_MemInfoFncPtr)
(
    DALSYSMemHandle,
    DALSYSMemInfo *
);
typedef DALResult
(*DALSYS_CacheCommandFncPtr)
(
    uint32 ,
    DALSYSMemAddr,
    uint32
);
typedef DALResult
(*DALSYS_MallocFncPtr)
(
    uint32 ,
    void **
);
typedef DALResult
(*DALSYS_FreeFncPtr)
(
    void *
);
typedef void
(*DALSYS_BusyWaitFncPtr)
(
   uint32
);
typedef DALResult
(*DALSYS_MemDescAddBufFncPtr)
(
    DALSysMemDescList *,
    uint32,
    uint32,
    uint32
);
typedef DALResult
(*DALSYS_MemDescPrepareFncPtr)
(
    DALSysMemDescList *,
    uint32
);
typedef DALResult
(*DALSYS_MemDescValidateFncPtr)
(
    DALSysMemDescList *,
    uint32
);
typedef DALResult
(*DALSYS_GetDALPropertyHandleFncPtr)
(
    DALDEVICEID,
    DALSYSPropertyHandle
);
typedef DALResult
(*DAL_DeviceAttachFncPtr)
(
    const char *pszArg,
    DALDEVICEID,
    DalDeviceHandle **
);
typedef DALResult
(*DAL_DeviceDetachFncPtr)
(
    DalDeviceHandle *
);
typedef DALResult
(*DAL_DeviceAttachExFncPtr)
(
 const char *pszArg,
    DALDEVICEID DevId,
    DALInterfaceVersion ClientVersion,
    DalDeviceHandle **phDalDevice
);
typedef DALResult
(*DALSYS_GetPropertyValueFncPtr)
(
    DALSYSPropertyHandle,
    const char *,
    uint32,
    DALSYSPropertyVar *
);
typedef void
(*DALSYS_LogEventFncPtr)
(
    DALDEVICEID,
    uint32,
    const char *
);
typedef struct DALSYSFncPtrTbl DALSYSFncPtrTbl;
struct DALSYSFncPtrTbl
{
    DALSYS_InitSystemHandleFncPtr DALSYS_InitSystemHandleFnc;
    DALSYS_DestroyObjectFncPtr DALSYS_DestroyObjectFnc;
    DALSYS_CopyObjectFncPtr DALSYS_CopyObjectFnc;
    DALSYS_RegisterWorkLoopFncPtr DALSYS_RegisterWorkLoopFnc;
    DALSYS_RegisterWorkLoopExFncPtr DALSYS_RegisterWorkLoopExFnc;
    DALSYS_AddEventToWorkLoopFncPtr DALSYS_AddEventToWorkLoopFnc;
    DALSYS_DeleteEventFromWorkLoopFncPtr DALSYS_DeleteEventFromWorkLoopFnc;
    DALSYS_EventCreateFncPtr DALSYS_EventCreateFnc;
    DALSYS_EventCtrlFncPtr DALSYS_EventCtrlFnc;
    DALSYS_EventWaitFncPtr DALSYS_EventWaitFnc;
    DALSYS_EventMultipleWaitFncPtr DALSYS_EventMultipleWaitFnc;
    DALSYS_SetupCallbackEventFncPtr DALSYS_SetupCallbackEventFnc;
    DALSYS_SyncCreateFncPtr DALSYS_SyncCreateFnc;
    DALSYS_SyncEnterFncPtr DALSYS_SyncEnterFnc;
    DALSYS_SyncTryEnterFncPtr DALSYS_SyncTryEnterFnc;
    DALSYS_SyncLeaveFncPtr DALSYS_SyncLeaveFnc;
    DALSYS_MemRegionAllocFncPtr DALSYS_MemRegionAllocFnc;
    DALSYS_MemRegionMapPhysFncPtr DALSYS_MemRegionMapPhysFnc;
    DALSYS_MemInfoFncPtr DALSYS_MemInfoFnc;
    DALSYS_CacheCommandFncPtr DALSYS_CacheCommandFnc;
    DALSYS_MallocFncPtr DALSYS_MallocFnc;
    DALSYS_FreeFncPtr DALSYS_FreeFnc;
    DALSYS_BusyWaitFncPtr DALSYS_BusyWaitFnc;
    DALSYS_MemDescAddBufFncPtr DALSYS_MemDescAddBufFnc;
    DALSYS_MemDescPrepareFncPtr DALSYS_MemDescPrepareFnc;
    DALSYS_MemDescValidateFncPtr DALSYS_MemDescValidateFnc;
    DALSYS_GetDALPropertyHandleFncPtr DALSYS_GetDALPropertyHandleFnc;
    DAL_DeviceAttachFncPtr DALSYS_DeviceAttachFnc;
    DAL_DeviceAttachExFncPtr DALSYS_DeviceAttachExFnc;
    DAL_DeviceAttachExFncPtr DALSYS_DeviceAttachRemoteFnc;
    DAL_DeviceDetachFncPtr DALSYS_DeviceDetachFnc;
    DALSYS_GetPropertyValueFncPtr DALSYS_GetPropertyValueFnc;
    DALSYS_LogEventFncPtr DALSYS_LogEventFnc;
};
typedef DALResult
(*DALRemote_NewFncPtr)(const char *, DALDEVICEID, DalDeviceHandle **);
typedef int
(*DALRemote_CommonInitFncPtr)(void);
typedef void
(*DALRemote_IPCInitFcnPtr)(uint32);
typedef int
(*DALRemote_InitFncPtr)(void);
typedef DALSYSEventHandle
(*DALRemote_CreateEventFncPtr)(void *pClientCtxt, DALSYSEventHandle hRemote);
typedef uint32
(*DALRemoteInterProcessCallFncPtr)(void *in_buf, void *out_buf);
typedef int
(*DALRemote_DeinitFncPtr)(void);
typedef struct DALRemoteVtbl DALRemoteVtbl;
struct DALRemoteVtbl
{
   DALRemote_NewFncPtr DALRemote_NewFnc;
   DALRemote_CommonInitFncPtr DALRemote_CommonInitFnc;
   DALRemote_InitFncPtr DALRemote_InitFnc;
   DALRemote_DeinitFncPtr DALRemote_DeinitFnc;
   DALRemote_CreateEventFncPtr DALRemote_CreateEventFnc;
   DALRemote_CreateEventFncPtr DALRemote_CreatePayloadEventFnc;
   DALRemoteInterProcessCallFncPtr DALRemoteInterProcessCallFnc;
   DALRemote_IPCInitFcnPtr DALRemote_IPCInitFcn;
};
typedef struct DALSYSConfig DALSYSConfig;
struct DALSYSConfig
{
    void *pNativeEnv;
    uint32 dwConfig;
   DALRemoteVtbl *pRemoteVtbl;
};
typedef long _Int32t;
typedef unsigned long _Uint32t;
typedef int _Ptrdifft;
typedef unsigned int _Sizet;
typedef __builtin_va_list va_list;
typedef long long _Longlong;
typedef unsigned long long _ULonglong;
typedef int _Wchart;
typedef int _Wintt;
typedef va_list _Va_list;
void _Atexit(void (*)(void));
typedef char _Sysch_t;
void _Locksyslock(int);
void _Unlocksyslock(int);
typedef unsigned short __attribute__((__may_alias__)) alias_short;
static alias_short *strict_aliasing_workaround(unsigned short *ptr) __attribute__((always_inline,unused));
static alias_short *strict_aliasing_workaround(unsigned short *ptr)
{
  alias_short *aliasptr = (alias_short *)ptr;
  return aliasptr;
}
typedef _Sizet size_t;
int memcmp(const void *, const void *, size_t) __attribute__((__nothrow__));
void *memcpy(void *, const void *, size_t) __attribute__((__nothrow__));
void *memcpy_v(volatile void *, const volatile void *, size_t) __attribute__((__nothrow__));
void *memset(void *, int, size_t) __attribute__((__nothrow__));
char *strcat(char *, const char *) __attribute__((__nothrow__));
int strcmp(const char *, const char *) __attribute__((__nothrow__));
char *strcpy(char *, const char *) __attribute__((__nothrow__));
size_t strlen(const char *) __attribute__((__nothrow__));
void *memmove(void *, const void *, size_t) __attribute__((__nothrow__));
void *memmove_v(volatile void *, const volatile void *, size_t) __attribute__((__nothrow__));
int strcoll(const char *, const char *) __attribute__((__nothrow__));
size_t strcspn(const char *, const char *) __attribute__((__nothrow__));
char *strerror(int) __attribute__((__nothrow__));
size_t strlcat(char *, const char *, size_t) __attribute__((__nothrow__));
char *strncat(char *, const char *, size_t) __attribute__((__nothrow__));
int strncmp(const char *, const char *, size_t) __attribute__((__nothrow__));
size_t strlcpy(char *, const char *, size_t) __attribute__((__nothrow__));
char *strncpy(char *, const char *, size_t) __attribute__((__nothrow__));
size_t strspn(const char *, const char *) __attribute__((__nothrow__));
char *strtok(char *, const char *) __attribute__((__nothrow__));
char *strsep(char **, const char *) __attribute__((__nothrow__));
size_t strxfrm(char *, const char *, size_t) __attribute__((__nothrow__));
char *strdup(const char *) __attribute__((__nothrow__));
int strcasecmp(const char *, const char *) __attribute__((__nothrow__));
int strncasecmp(const char *, const char *, size_t) __attribute__((__nothrow__));
char *strtok_r(char *, const char *, char **) __attribute__((__nothrow__));
void *memccpy (void *, const void *, int, size_t) __attribute__((__nothrow__));
int strerror_r (int, char *, size_t) __attribute__((__nothrow__));
char *strchr(const char *, int) __attribute__((__nothrow__));
char *strpbrk(const char *, const char *) __attribute__((__nothrow__));
char *strrchr(const char *, int) __attribute__((__nothrow__));
char *strstr(const char *, const char *) __attribute__((__nothrow__));
void *memchr(const void *, int, size_t) __attribute__((__nothrow__));
void
DALSYS_InitMod(DALSYSConfig * pCfg);
void
DALSYS_DeInitMod(void);
void
DALSYS_InitSystemHandle(DalDeviceHandle *hDalDevice);
DALSYS_LogEventFncPtr
DALSYS_SetLogCfg(uint32 dwMaxLogLevel, DALSYS_LogEventFncPtr DALSysLogFcn);
DALResult
DALSYS_DestroyObject(DALSYSObjHandle hObj);
DALResult
DALSYS_CopyObject(DALSYSObjHandle hObjOrig, DALSYSObjHandle *phObjCopy );
DALResult
DALSYS_RegisterWorkLoop(uint32 dwPriority,
                  uint32 dwMaxNumEvents,
                  DALSYSWorkLoopHandle *phWorkLoop,
                  DALSYSWorkLoopObj *pWorkLoopObj);
DALResult
DALSYS_RegisterWorkLoopEx(
                  char * pszname,
                  uint32 dwStackSize,
                  uint32 dwPriority,
                  uint32 dwMaxNumEvents,
                  DALSYSWorkLoopHandle *phWorkLoop,
                  DALSYSWorkLoopObj *pWorkLoopObj);
DALResult
DALSYS_AddEventToWorkLoop(DALSYSWorkLoopHandle hWorkLoop,
                    DALSYSWorkLoopExecute pfnWorkLoopExecute,
                    void * pArg,
                    DALSYSEventHandle hEvent,
                    DALSYSSyncHandle hSync);
DALResult
DALSYS_DeleteEventFromWorkLoop(DALSYSWorkLoopHandle hWorkLoop,
                         DALSYSEventHandle hEvent);
DALResult
DALSYS_EventCreate(uint32 dwAttribs, DALSYSEventHandle *phEvent,
               DALSYSEventObj *pEventObj);
DALResult
DALSYS_EventCopy(DALSYSEventHandle hEvent,
             DALSYSEventHandle *phEventCopy,DALSYSEventObj *pEventObjCopy,
                 uint32 dwMarshalFlags);
DALResult
DALSYS_EventCtrlEx(DALSYSEventHandle hEvent, uint32 dwCtrl, uint32 dwParam,
                   void *pPayload, uint32 dwPayloadSize);
DALResult
DALSYS_EventWait(DALSYSEventHandle hEvent);
DALResult
DALSYS_EventMultipleWait(DALSYSEventHandle* phEvent, int nEvents,
                         uint32 dwTimeoutUs,uint32 *pdwEventIdx);
DALResult
DALSYS_SetupCallbackEvent(DALSYSEventHandle hEvent, DALSYSCallbackFunc cbFunc,
                          DALSYSCallbackFuncCtx cbFuncCtx);
DALResult DALSYS_TimerStart( DALSYSEventHandle hEvent, uint32 time);
DALResult DALSYS_TimerStop( DALSYSEventHandle hEvent );
DALResult
DALSYS_SyncCreate(uint32 dwAttribs,
                  DALSYSSyncHandle *phSync,
                  DALSYSSyncObj *pSyncObj);
void
DALSYS_SyncEnter(DALSYSSyncHandle hSync);
DALResult
DALSYS_SyncTryEnter(DALSYSSyncHandle hSync);
void
DALSYS_SyncLeave(DALSYSSyncHandle hSync);
DALResult
DALSYS_MemRegionAlloc(uint32 dwAttribs, DALSYSMemAddr VirtualAddr,
                      DALSYSMemAddr PhysicalAddr, uint32 dwLen,
                      DALSYSMemHandle *phMem, DALSYSMemObj *pMemObj);
DALResult
DALSYS_MemRegionAllocEx( DALSYSMemHandle *phMem, const DALSYSMemReq *pMemReq,
      DALSYSMemInfoEx *pMemInfo );
DALResult
DALSYS_MemRegionMapPhys(DALSYSMemHandle hMem, uint32 dwVirtualBaseOffset,
                        DALSYSMemAddr PhysicalAddr, uint32 dwLen);
DALResult
DALSYS_MemInfo(DALSYSMemHandle hMem, DALSYSMemInfo *pMemInfo);
DALResult
DALSYS_MemInfoEx(DALSYSMemHandle hMem, DALSYSMemInfoEx *pMemInfo);
DALResult
DALSYS_CacheCommand(uint32 CacheCmd, DALSYSMemAddr VirtualAddr, uint32 dwLen);
DALResult
DALSYS_Malloc(uint32 dwSize, void **ppMem);
DALResult
DALSYS_Free(void *pmem);
void
DALSYS_BusyWait(uint32 pause_time_us);
DALResult
DALSYS_GetDALPropertyHandle(DALDEVICEID DeviceId, DALSYSPropertyHandle hDALProps);
DALResult
DALSYS_GetDALPropertyHandleEx(DALDEVICEID DeviceId,DALSYSPropertyHandle hDALProps,
                              DEVCFG_TARGET_INFO_TYPE target_info_type);
DALResult
DALSYS_GetDALPropertyHandleDyn(DALDEVICEID DeviceId, DALSYSPropertyHandle hDALProps);
DALResult
DALSYS_GetDALPropertyHandleStr(const char *pszDevName, DALSYSPropertyHandle hDALProps);
DALResult
DALSYS_GetDALPropertyHandleStrEx(const char *pszDevName, DALSYSPropertyHandle hDALProps,
                                 DEVCFG_TARGET_INFO_TYPE target_info_type);
DALResult
DALSYS_GetDALPropertyHandleStrDyn(const char *pszDevName, DALSYSPropertyHandle hDALProps);
DALResult
DALSYS_GetPropertyValue(DALSYSPropertyHandle hDALProps, const char *pszName,
                  uint32 dwId,
                   DALSYSPropertyVar *pDALPropVar);
void
DALSYS_LogEvent(DALDEVICEID DeviceId, uint32 dwLogEventType,
      const char * pszFmt, ...);
DALRemoteVtbl *
DALSYS_GetRemoteInterfaceVtbl(void);
uint32 DALSYS_SetThreadPriority(uint32 priority);
uint32 _DALSYS_memscpy(void * pDest, uint32 iDestSz,
      const void * pSrc, uint32 iSrcSize);
typedef unsigned char boolean;
typedef unsigned long int bool32;
typedef struct DALDrvCtxt DALDrvCtxt;
typedef struct DALDevCtxt DALDevCtxt;
typedef struct DALClientCtxt DALClientCtxt;
typedef struct DALDrvVtbl DALDrvVtbl;
struct DALDrvVtbl
{
   int (*DAL_DriverInit)(DALDrvCtxt *);
   int (*DAL_DriverDeInit)(DALDrvCtxt *);
};
struct DALDevCtxt
{
    uint32 dwRefs;
    DALDEVICEID DevId;
    uint32 dwDevCtxtRefIdx;
    DALDrvCtxt *pDALDrvCtxt;
    uint32 hProp[2];
    DalDeviceHandle *hDALSystem;
    DalDeviceHandle *hDALTimer;
    DalDeviceHandle *hDALInterrupt;
    uint32 * pSystemTbl;
    DALSYSWorkLoopHandle * pDefaultWorkLoop;
    const char *strDeviceName;
    uint32 reserve[16-6];
};
struct DALDrvCtxt
{
   DALDrvVtbl DALDrvVtbl;
   uint32 dwNumDev;
   uint32 dwSizeDevCtxt;
   uint32 bInit;
   uint32 dwRefs;
   DALDevCtxt DALDevCtxt[1];
};
struct DALClientCtxt
{
    uint32 dwRefs;
    uint32 dwAccessMode;
    void * pPortCtxt;
    DALDevCtxt *pDALDevCtxt;
};
typedef struct DALInheritSrcPram DALInheritSrcPram;
struct DALInheritSrcPram
{
   const byte * pBuf;
   int dwBufLen;
   uint32 dwSeqIdx;
};
typedef struct DALInheritDestPram DALInheritDestPram;
struct DALInheritDestPram
{
   byte * pBuf;
   uint32 dwBufLen;
   int *pdwObjLen;
   uint32 *dwSeqIdx;
};
typedef struct DALInterface DALInterface;
struct DALInterface
{
    uint32 dwDalHandleId;
    void *pVtbl;
    DALClientCtxt *pclientCtxt;
};
DALResult
DALFW_AttachToStringDevice(const char *pszDeviceName, DALDrvCtxt *pdalDrvCtxt,
                           DALClientCtxt *pClientCtxt);
DALResult
DALFW_AttachToDevice(DALDEVICEID devId,DALDrvCtxt *pdalDrvCtxt,
                     DALClientCtxt *pdalClientCtxt);
void
DALFW_MarkDeviceStatic(DALDevCtxt *pdalDevCtxt);
uint32
DALFW_AddRef(DALClientCtxt *pdalClientCtxt);
uint32
DALFW_Release(DALClientCtxt *pdalClientCtxt);
void
DALFW_SystemAttach(DALDevCtxt *pDevCtxt);
void
DALFW_SystemDetach(DALDevCtxt *pDevCtxt);
DALSYSWorkLoopHandle *
DALFW_GetWorkLoop(DALDevCtxt *pDevCtxt, uint32 dwWorkLoopPriority);
DALMemObject *
DALFW_CopyMemObjectToDDI(DALSYSMemHandle hMem, DALMemObject * pDestMemObject);
DALSYSMemHandle
DALFW_CreateMemObjectFromDDI(uint32 dwMarshalFlags, DALMemObject * pInMemObject);
DALSYSEventHandle
DALFW_CreateEventFromDDI(DALClientCtxt * pClientCtxt, uint32 dwMarshalFlags,
                DALSYSEventHandle inHandle, DALEventObject * pPrealloc);
DALSYSEventHandle
DALFW_CreatePayloadEventFromDDI(DALClientCtxt * pClientCtxt, uint32 dwMarshalFlags,
                DALSYSEventHandle inHandle, DALEventObject * pPrealloc);
uint32
DALFW_InitDDIMemDescListFromSys(uint32 dwFlags,
                             DALSysMemDescList *pInMemDescList,
                             DALDDIMemDescList * pDestDDIMemDescList);
DALSysMemDescList *
DALFW_InitSysMemDescListFromDDI(DALSYSMemHandle hMemHandle,
                             DALDDIMemDescList *pInDDIMemDescList,
                             DALSysMemDescList *pDestMemDescList);
void
DALFW_RegisterSystemNotificationEvent(DalDeviceHandle * h, DALSYSEventHandle hEvent);
void memory_barrier(void);
DALSysMemDescBuf *
DALFW_MemDescBufPtr(DALSysMemDescList * pMemDescList, uint32 idx);
DALResult
DALFW_MemDescInit(DALSYSMemHandle hMem, DALSysMemDescList * pMemDescList,
                  uint32 dwNumBufs);
DALResult
DALFW_MemDescAddBuf(DALSysMemDescList * pMemDescList, uint32 bufIdx,
                    uint32 offset, uint32 size, uint32 userInfo);
DALSYSMemHandle
DALFW_AllocPhysForMemDescBufs(DALSYSMemHandle hMem,
                              uint32 operation,
                              DALSysMemDescList *pInDescList,
                              DALSysMemDescList ** pOutDescList);
DALResult
DALFW_MemDescListCopyBufs(DALSysMemDescList * pDestDescList,
                          DALSysMemDescList * pSrcDescList);
uint32 DALFW_LockedExchangeW(volatile uint32 *pTarget, uint32 value);
uint32 DALFW_LockedIncrementW(volatile uint32 *pTarget);
uint32 DALFW_LockedDecrementW(volatile uint32 *pTarget);
uint32 DALFW_LockedGetModifySetW(volatile uint32 *pTarget, uint32 AND_value, uint32 OR_value);
uint32 DALFW_LockedCompareExchangeW(volatile uint32 *pTarget, uint32 comparand, uint32 value);
void DALFW_LockSpinLock(volatile uint32 *spinLock, uint32 pid);
void DALFW_UnLockSpinLock(volatile uint32 *spinLock);
void DALFW_LockSpinLockExt(volatile uint32 *spinLock, uint32 pid);
void DALFW_UnLockSpinLockExt(volatile uint32 *spinLock);
DALResult DALFW_TryLockSpinLockExt(volatile uint32 *spinLock, uint32 pid);
typedef struct _DAFFW_MPLOCK
{
  volatile uint32 spin_lock;
  volatile uint32 owner;
  volatile uint32 waiting;
   volatile uint32 size;
}
DALFW_MPLOCK;
void DALFW_MPLock(DALFW_MPLOCK *pLock, uint32 pid, uint32 delayParam);
void DALFW_MPUnLock(DALFW_MPLOCK *pLock);
uint32 DALFW_TrySpinLockEx(DALFW_MPLOCK *pLock, uint32 pid);
typedef struct coreUtils_Q_LinkStruct
{
  struct coreUtils_Q_LinkStruct *pNext;
  struct coreUtils_Q_LinkStruct *pPrev;
} coreUtils_Q_LinkType;
typedef struct coreUtils_Q_HeadLinkStruct
{
  struct coreUtils_Q_LinkStruct *pNext;
  struct coreUtils_Q_LinkStruct *pPrev;
} coreUtils_Q_HeadLinkType;
typedef struct coreUtils_Q_Struct
{
  coreUtils_Q_LinkType link;
  int nCnt;
} coreUtils_Q_Type;
typedef struct {
   coreUtils_Q_LinkType link;
} coreUtils_Q_GenericItemType;
typedef int (*coreUtils_Q_CompareFuncType)(void* pItem, void *pCompareVal);
typedef void (*coreUtils_Q_ActionFuncType)(void *pItem, void *pParam);
coreUtils_Q_Type* coreUtils_Q_Init ( coreUtils_Q_Type *q_ptr );
coreUtils_Q_LinkType* coreUtils_Q_Link ( void *pItem, coreUtils_Q_LinkType *pLink );
void coreUtils_Q_Put ( coreUtils_Q_Type *pQ, coreUtils_Q_LinkType *pLink );
void* coreUtils_Q_Get ( coreUtils_Q_Type *pQ );
void* coreUtils_Q_LastGet ( coreUtils_Q_Type *pQ );
int coreUtils_Q_Cnt ( coreUtils_Q_Type *pQ );
void* coreUtils_Q_Check ( coreUtils_Q_Type *pQ );
void* coreUtils_Q_LastCheck ( coreUtils_Q_Type *pQ );
void* coreUtils_Q_Next(
   coreUtils_Q_Type *pQ,
   coreUtils_Q_LinkType *pLink
);
void coreUtils_Q_Insert ( coreUtils_Q_Type *pQ, coreUtils_Q_LinkType *pLinkToInsert, coreUtils_Q_LinkType *pLinkPrev);
void coreUtils_Q_Delete ( coreUtils_Q_Type *pQ, coreUtils_Q_LinkType *pLink );
boolean coreUtils_Q_DeleteExt ( coreUtils_Q_Type *pQ, coreUtils_Q_LinkType *pLink );
void* coreUtils_Q_LinearSearch(
  coreUtils_Q_Type *pQ,
  coreUtils_Q_CompareFuncType compareFunc,
  void *pCompareVal
);
void coreUtils_Q_LinearDelete(
  coreUtils_Q_Type *pQ,
  coreUtils_Q_CompareFuncType compareFunc,
  void *pParam,
  coreUtils_Q_ActionFuncType actionFunc
);
typedef struct _Mbstatet
 {
 unsigned long _Wchar;
 unsigned short _Byte, _State;
 } _Mbstatet;
typedef struct fpos_t
 {
 _Longlong _Off;
 _Mbstatet _Wstate;
 } fpos_t;
struct _Dnk_filet
 {
 unsigned short _Mode;
 unsigned char _Idx;
 int _Handle;
 unsigned char *_Buf, *_Bend, *_Next;
 unsigned char *_Rend, *_Wend, *_Rback;
 _Wchart *_WRback, _WBack[2];
 unsigned char *_Rsave, *_WRend, *_WWend;
 _Mbstatet _Wstate;
 char *_Tmpnam;
 unsigned char _Back[8], _Cbuf;
 };
typedef struct _Dnk_filet _Filet;
typedef _Filet FILE;
extern FILE _Stdin, _Stdout, _Stderr;
void clearerr(FILE *) __attribute__((__nothrow__));
int fclose(FILE *) __attribute__((__nothrow__));
int feof(FILE *) __attribute__((__nothrow__));
int ferror(FILE *) __attribute__((__nothrow__));
int fflush(FILE *) __attribute__((__nothrow__));
int fgetc(FILE *) __attribute__((__nothrow__));
int fgetpos(FILE *, fpos_t *) __attribute__((__nothrow__));
char *fgets(char *, int, FILE *) __attribute__((__nothrow__));
FILE *fopen(const char *, const char *) __attribute__((__nothrow__));
int fprintf(FILE *, const char *, ...) __attribute__((__nothrow__));
int fputc(int, FILE *) __attribute__((__nothrow__));
int fputs(const char *, FILE *) __attribute__((__nothrow__));
size_t fread(void *, size_t, size_t, FILE *) __attribute__((__nothrow__));
FILE *freopen(const char *, const char *,
 FILE *) __attribute__((__nothrow__));
int fscanf(FILE * , const char *, ...) __attribute__((__nothrow__));
int fseek(FILE *, long, int) __attribute__((__nothrow__));
int fsetpos(FILE *, const fpos_t *) __attribute__((__nothrow__));
long ftell(FILE *) __attribute__((__nothrow__));
size_t fwrite(const void *, size_t, size_t,
 FILE *) __attribute__((__nothrow__));
char *gets(char *) __attribute__((__nothrow__));
void perror(const char *) __attribute__((__nothrow__));
int fseeko (FILE *, long, int) __attribute__((__nothrow__));
long ftello (FILE *) __attribute__((__nothrow__));
int getchar_unlocked (void) __attribute__((__nothrow__));
int getc_unlocked (FILE *) __attribute__((__nothrow__));
int putchar_unlocked (int) __attribute__((__nothrow__));
int putc_unlocked (int, FILE *) __attribute__((__nothrow__));
int printf(const char *, ...) __attribute__((__nothrow__));
int puts(const char *) __attribute__((__nothrow__));
int remove(const char *) __attribute__((__nothrow__));
int rename(const char *, const char *) __attribute__((__nothrow__));
void rewind(FILE *) __attribute__((__nothrow__));
int scanf(const char *, ...) __attribute__((__nothrow__));
void setbuf(FILE * , char *) __attribute__((__nothrow__));
int setvbuf(FILE * , char *, int, size_t) __attribute__((__nothrow__));
int sprintf(char *, const char *, ...) __attribute__((__nothrow__));
int sscanf(const char *, const char *, ...) __attribute__((__nothrow__));
FILE *tmpfile(void) __attribute__((__nothrow__));
char *tmpnam(char *) __attribute__((__nothrow__));
int ungetc(int, FILE *) __attribute__((__nothrow__));
int vfprintf(FILE *, const char *, _Va_list) __attribute__((__nothrow__));
int vprintf(const char *, _Va_list) __attribute__((__nothrow__));
int vsprintf(char *, const char *, _Va_list) __attribute__((__nothrow__));
FILE *fdopen(int, const char *) __attribute__((__nothrow__));
int fileno(FILE *) __attribute__((__nothrow__));
int getw(FILE *) __attribute__((__nothrow__));
int putw(int, FILE *) __attribute__((__nothrow__));
long _Fgpos(FILE *, fpos_t *) __attribute__((__nothrow__));
int _Flocale(FILE *, const char *, int) __attribute__((__nothrow__));
void _Fsetlocale(FILE *, int) __attribute__((__nothrow__));
int _Fspos(FILE *, const fpos_t *, long, int) __attribute__((__nothrow__));
void _Lockfilelock(_Filet *) __attribute__((__nothrow__));
void _Unlockfilelock(_Filet *) __attribute__((__nothrow__));
extern FILE *_Files[20];
int snprintf(char *, size_t,
 const char *, ...) __attribute__((__nothrow__));
int vsnprintf(char *, size_t,
 const char *, _Va_list) __attribute__((__nothrow__));
int vfscanf(FILE *,
 const char *, _Va_list) __attribute__((__nothrow__));
int vscanf(const char *, _Va_list) __attribute__((__nothrow__));
int vsscanf(const char *,
 const char *, _Va_list) __attribute__((__nothrow__));
int getc(FILE *) __attribute__((__nothrow__));
int getchar(void) __attribute__((__nothrow__));
int putc(int, FILE *) __attribute__((__nothrow__));
int putchar(int) __attribute__((__nothrow__));
typedef _Mbstatet mbstate_t;
struct tm;
struct _Dnk_filet;
typedef _Wchart wchar_t;
typedef _Wintt wint_t;
wint_t fgetwc(_Filet *) __attribute__((__nothrow__));
wchar_t *fgetws(wchar_t *, int,
 _Filet *) __attribute__((__nothrow__));
wint_t fputwc(wchar_t, _Filet *) __attribute__((__nothrow__));
int fputws(const wchar_t *,
 _Filet *) __attribute__((__nothrow__));
int fwide(_Filet *, int) __attribute__((__nothrow__));
int fwprintf(_Filet *,
 const wchar_t *, ...) __attribute__((__nothrow__));
int fwscanf(_Filet *,
 const wchar_t *, ...) __attribute__((__nothrow__));
wint_t getwc(_Filet *) __attribute__((__nothrow__));
wint_t getwchar(void) __attribute__((__nothrow__));
wint_t putwc(wchar_t, _Filet *) __attribute__((__nothrow__));
wint_t putwchar(wchar_t) __attribute__((__nothrow__));
int swprintf(wchar_t *, size_t,
 const wchar_t *, ...) __attribute__((__nothrow__));
int swscanf(const wchar_t *,
 const wchar_t *, ...) __attribute__((__nothrow__));
wint_t ungetwc(wint_t, _Filet *) __attribute__((__nothrow__));
int vfwprintf(_Filet *,
 const wchar_t *, _Va_list) __attribute__((__nothrow__));
int vswprintf(wchar_t *, size_t,
 const wchar_t *, _Va_list) __attribute__((__nothrow__));
int vwprintf(const wchar_t *, _Va_list) __attribute__((__nothrow__));
int wprintf(const wchar_t *, ...) __attribute__((__nothrow__));
int wscanf(const wchar_t *, ...) __attribute__((__nothrow__));
int vfwscanf(_Filet *,
 const wchar_t *, _Va_list) __attribute__((__nothrow__));
int vswscanf(const wchar_t *,
 const wchar_t *, _Va_list) __attribute__((__nothrow__));
int vwscanf(const wchar_t *, _Va_list) __attribute__((__nothrow__));
size_t mbrlen(const char *,
 size_t, mbstate_t *) __attribute__((__nothrow__));
size_t mbrtowc(wchar_t *, const char *,
 size_t, mbstate_t *) __attribute__((__nothrow__));
size_t mbsrtowcs(wchar_t *,
 const char **, size_t, mbstate_t *) __attribute__((__nothrow__));
int mbsinit(const mbstate_t *) __attribute__((__nothrow__));
size_t wcrtomb(char *,
 wchar_t, mbstate_t *) __attribute__((__nothrow__));
size_t wcsrtombs(char *,
 const wchar_t **, size_t, mbstate_t *) __attribute__((__nothrow__));
long wcstol(const wchar_t *,
 wchar_t **, int) __attribute__((__nothrow__));
_Longlong wcstoll(const wchar_t *,
 wchar_t **, int) __attribute__((__nothrow__));
_ULonglong wcstoull(const wchar_t *,
 wchar_t **, int) __attribute__((__nothrow__));
wchar_t *wcscat(wchar_t *, const wchar_t *) __attribute__((__nothrow__));
int wcscmp(const wchar_t *, const wchar_t *) __attribute__((__nothrow__));
wchar_t *wcscpy(wchar_t *, const wchar_t *) __attribute__((__nothrow__));
size_t wcslen(const wchar_t *) __attribute__((__nothrow__));
int wcsncmp(const wchar_t *, const wchar_t *, size_t) __attribute__((__nothrow__));
wchar_t *wcsncpy(wchar_t *,
 const wchar_t *, size_t) __attribute__((__nothrow__));
int wcscoll(const wchar_t *, const wchar_t *) __attribute__((__nothrow__));
size_t wcscspn(const wchar_t *, const wchar_t *) __attribute__((__nothrow__));
wchar_t *wcsncat(wchar_t *,
 const wchar_t *, size_t) __attribute__((__nothrow__));
size_t wcsspn(const wchar_t *, const wchar_t *) __attribute__((__nothrow__));
wchar_t *wcstok(wchar_t *, const wchar_t *,
 wchar_t **) __attribute__((__nothrow__));
size_t wcsxfrm(wchar_t *,
 const wchar_t *, size_t) __attribute__((__nothrow__));
int wmemcmp(const wchar_t *, const wchar_t *, size_t) __attribute__((__nothrow__));
wchar_t *wmemcpy(wchar_t *,
 const wchar_t *, size_t) __attribute__((__nothrow__));
wchar_t *wmemmove(wchar_t *, const wchar_t *, size_t) __attribute__((__nothrow__));
wchar_t *wmemset(wchar_t *, wchar_t, size_t) __attribute__((__nothrow__));
size_t wcsftime(wchar_t *, size_t,
 const wchar_t *, const struct tm *) __attribute__((__nothrow__));
wint_t _Btowc(int) __attribute__((__nothrow__));
int _Wctob(wint_t) __attribute__((__nothrow__));
double _WStod(const wchar_t *, wchar_t **, long) __attribute__((__nothrow__));
float _WStof(const wchar_t *, wchar_t **, long) __attribute__((__nothrow__));
long double _WStold(const wchar_t *, wchar_t **, long) __attribute__((__nothrow__));
unsigned long _WStoul(const wchar_t *, wchar_t **, int) __attribute__((__nothrow__));
wchar_t *wmemchr(const wchar_t *, wchar_t, size_t);
double wcstod(const wchar_t *, wchar_t **);
unsigned long wcstoul(const wchar_t *, wchar_t **, int);
wchar_t *wcschr(const wchar_t *, wchar_t);
wchar_t *wcspbrk(const wchar_t *, const wchar_t *);
wchar_t *wcsrchr(const wchar_t *, wchar_t);
wchar_t *wcsstr(const wchar_t *, const wchar_t *);
wint_t btowc(int);
int wctob(wint_t);
float wcstof(const wchar_t *,
 wchar_t **);
long double wcstold(const wchar_t *,
 wchar_t **);
typedef unsigned short wchar;
size_t strlcat(char *dst, const char *src, size_t siz);
size_t wcslcat(wchar_t *dst, const wchar_t *src, size_t siz);
size_t wstrlcat(wchar* dst, const wchar* src, size_t siz);
size_t strlcpy(char *dst, const char *src, size_t siz);
size_t wcslcpy(wchar_t *dst, const wchar_t *src, size_t siz);
size_t wstrlcpy(wchar* dst, const wchar* src, size_t siz);
size_t wstrlen(const wchar *src);
int wstrcmp(const wchar *s1, const wchar *s2);
int wstrncmp(const wchar *s1, const wchar *s2, size_t n);
int strcasecmp(const char * s1, const char * s2);
int strncasecmp(const char * s1, const char * s2, size_t n);
unsigned int std_scanul(const char * pchBuf, int nRadix, const char ** ppchEnd, int *pnError);
size_t memscpy(void *dst, size_t dst_size, const void *src, size_t src_size);
static __inline size_t memscpy_i
(
  void *dst,
  size_t dst_size,
  const void *src,
  size_t src_size
)
{
  size_t copy_size = (dst_size <= src_size)? dst_size : src_size;
  memcpy(dst, src, copy_size);
  return copy_size;
}
size_t memsmove(void *dst, size_t dst_size, const void *src, size_t src_size);
static __inline size_t memsmove_i
(
  void *dst,
  size_t dst_size,
  const void *src,
  size_t src_size
)
{
  size_t copy_size = (dst_size <= src_size)? dst_size : src_size;
  memmove(dst, src, copy_size);
  return copy_size;
}
void* secure_memset(void* ptr, int value, size_t len);
int timesafe_memcmp(const void* ptr1, const void* ptr2, size_t len);
int timesafe_strncmp(const char* ptr1, const char* ptr2, size_t len);
size_t strnlen(const char *str, size_t maxlen);
typedef enum
{
     Adsppm_Api_Type_None,
     Adsppm_Api_Type_Sync,
     Adsppm_Api_Type_Async,
     Adsppm_Ape_Type_Enum_Max,
     Adsppm_Api_Type_Force8bits = 0x7F
} AdsppmApiType;
typedef enum
{
    Adsppm_Status_Success,
    Adsppm_Status_Failed,
    Adsppm_Status_NoMemory,
    Adsppm_Status_VersionNotSupport,
    Adsppm_Status_BadClass,
    Adsppm_Status_BadState,
    Adsppm_Status_BadParm,
    Adsppm_Status_InvalidFormat,
    Adsppm_Status_UnSupported,
    Adsppm_Status_ResourceNotFound,
    Adsppm_Status_BadMemPtr,
    Adsppm_Status_BadHandle,
    Adsppm_Status_ResourceInUse,
    Adsppm_Status_NoBandwidth,
    Adsppm_Status_NullPointer,
    Adsppm_Status_NotInitialized,
    Adsppm_Status_ResourceNotRequested,
    Adsppm_Status_CoreResourceNotAvailable,
    Adsppm_Status_Max,
    Adsppm_Status_Force32Bits = 0x7FFFFFFF
} AdsppmStatusType;
typedef AdsppmStatusType Adsppm_Status;
typedef enum
{
    Adsppm_Core_Id_None = 0,
    Adsppm_Core_Id_ADSP = 1,
    Adsppm_Core_Id_LPASS_Core = 2,
    Adsppm_Core_Id_LPM = 3,
    Adsppm_Core_Id_DML = 4,
    Adsppm_Core_Id_AIF = 5,
    Adsppm_Core_Id_SlimBus = 6,
    Adsppm_Core_Id_Midi = 7,
    Adsppm_Core_Id_AVsync = 8,
    Adsppm_Core_Id_HWRSMP = 9,
    Adsppm_Core_Id_SRam = 10,
    Adsppm_Core_Id_DCodec = 11,
    Adsppm_Core_Id_Spdif = 12,
    Adsppm_Core_Id_Hdmirx = 13,
    Adsppm_Core_Id_Hdmitx = 14,
    Adsppm_Core_Id_Sif = 15,
    Adsppm_Core_Id_BSTC = 16,
    Adsppm_Core_Id_HVX = 17,
    Adsppm_Core_Id_Max,
    Adsppm_Core_Id_Force8Bits = 0x7F
} AdsppmCoreIdType;
typedef enum
{
    Adsppm_Instance_Id_None = 0,
    Adsppm_Instance_Id_0 = 1,
    Adsppm_Instance_Id_1 = 2,
    Adsppm_Instance_Id_2 = 3,
    Adsppm_Instance_Id_Max,
    Adsppm_Instance_Id_Force8Bits = 0x7F
} AdsppmInstanceIdType;
typedef enum
{
    Adsppm_Rsc_Id_None = 0,
    Adsppm_Rsc_Id_Power = 1,
    Adsppm_Rsc_Id_Core_Clk = 2,
    Adsppm_Rsc_Id_Sleep_Latency = 3,
    Adsppm_Rsc_Id_Mips = 4,
    Adsppm_Rsc_Id_BW = 5,
    Adsppm_Rsc_Id_Thermal = 6,
    Adsppm_Rsc_Id_MemPower = 7,
    Adsppm_Rsc_Id_Core_Clk_Domain = 8,
    Adsppm_Rsc_Id_Max,
    Adsppm_Rsc_Id_Force8Bits = 0x7F
} AdsppmRscIdType;
typedef enum
{
    Adsppm_State_ACMInit = 0x1,
    Adspmm_State_HalIntrInit = 0x2,
    Adsppm_State_HalHwIoInit = 0x4,
    Adsppm_State_HalClkRgmInit = 0x8,
    Adsppm_State_HalBusInit = 0x10,
    Adsppm_State_HalSlpInit = 0x20,
    Adsppm_State_CoreCtxLockInit = 0x80,
    Adsppm_State_CoreRMInit = 0x100,
    Adsppm_State_CoreAMAsyncInit = 0x200,
    Adsppm_State_CoreMIPSInit = 0x400,
    Adsppm_State_CoreBUSInit = 0x800,
    Adsppm_State_CoreAHBMInit = 0x1000,
    Adsppm_State_CorePWRInit = 0x2000,
    Adsppm_State_CoreCLKInit = 0x4000,
    Adsppm_State_CoreSLEEPInit = 0x8000,
    Adsppm_State_CoreTHERMALInit = 0x10000,
    Adsppm_State_CoreMEMPWRInit = 0x20000,
    Adsppm_State_CoreCMInit = 0x40000,
    Adsppm_State_CoreCPMInit = 0x80000,
    Adsppm_State_CoreDCVSInit = 0x100000,
    Adsppm_State_CoreEXTBWInit = 0x200000,
    Adsppm_State_CoreADSPCLKInit = 0x400000,
    Adsppm_State_CoreQCMInit = 0x800000,
    Adsppm_State_CoreBMRegisterEvent = 0x1000000,
    Adsppm_State_Force32bit = 0x7fffffff
} AdsppmInitStateType;
typedef enum
{
    Adsppm_Callback_Event_Id_None,
    Adsppm_Callback_Event_Id_Thermal = 0x0002,
    Adsppm_Callback_Event_Id_Async_Complete = 0x0004,
    Adsppm_Callback_Event_Id_Idle,
    Adsppm_Callback_Event_Id_Busy,
    Adsppm_Callback_Event_Id_Max,
    Adsppm_Callback_Event_Id_Force32Bits = 0x7FFFFFFF
} AdsppmCallbackEventIdType;
typedef struct
{
    AdsppmCallbackEventIdType eventId;
    uint32 clientId;
    uint32 callbackDataSize;
    void *callbackData;
} AdsppmCallbackParamType;
typedef enum
{
    Adsppm_Thermal_NONE,
    Adsppm_Thermal_LOW,
    Adsppm_Thermal_NORM,
    Adsppm_Thermal_High_L1,
    Adsppm_Thermal_High_L2,
    Adsppm_Thermal_High_L3,
    Adsppm_Thermal_High_L4,
    Adsppm_Thermal_High_L5,
    Adsppm_Thermal_High_L6,
    Adsppm_Thermal_High_L7,
    Adsppm_Thermal_High_L8,
    Adsppm_Thermal_High_L9,
    Adsppm_Thermal_High_L10,
    Adsppm_Thermal_High_L11,
    Adsppm_Thermal_High_L12,
    Adsppm_Thermal_High_L13,
    Adsppm_Thermal_High_L14,
    Adsppm_Thermal_High_L15,
    Adsppm_Thermal_High_L16,
    Adsppm_Thermal_Max,
    Adsppm_Thermal_Force32Bits = 0x7FFFFFFF
} AdsppmThermalType;
typedef enum
{
    AdsppmClk_None = 0,
    AdsppmClk_Adsp_Core,
    AdsppmClk_Ahb_Root,
    AdsppmClk_AhbI_Hclk,
    AdsppmClk_AhbX_Hclk,
    AdsppmClk_HwRsp_Hclk,
    AdsppmClk_Dml_Hclk,
    AdsppmClk_Aif_Hclk,
    AdsppmClk_Aif_Csr_Hclk,
    AdsppmClk_Slimbus_Hclk,
    AdsppmClk_Slimbus_cbc,
    AdsppmClk_Slimbus2_Hclk,
    AdsppmClk_Slimbus2_cbc,
    AdsppmClk_Midi_Hclk,
    AdsppmClk_AvSync_Hclk,
    AdsppmClk_Atimer_Hclk,
    AdsppmClk_Lpm_Hclk,
    AdsppmClk_Lpm_cbc,
    AdsppmClk_Csr_Hclk,
    AdsppmClk_Dcodec_Hclk,
    AdsppmClk_Spdif_Hmclk,
    AdsppmClk_Spdif_Hsclk,
    AdsppmClk_Hdmirx_Hclk,
    AdsppmClk_Hdmitx_Hclk,
    AdsppmClk_Sif_Hclk,
    AdsppmClk_Bstc_Hclk,
    AdsppmClk_Smmu_Adsp_Hclk,
    AdsppmClk_Smmu_Lpass_Hclk,
    AdsppmClk_Sysnoc_Hclk,
    AdsppmClk_Sysnoc_cbc,
    AdsppmClk_Bus_Timeout_Hclk,
    AdsppmClk_Tlb_Preload_Hclk,
    AdsppmClk_Qos_Hclk,
    AdsppmClk_Qdsp_Sway_Hclk,
    AdsppmClk_AhbE_Hclk,
    AdsppmClk_Adsp_Hmclk,
    AdsppmClk_Adsp_Hsclk,
    AdsppmClk_Sram_Hclk,
    AdsppmClk_Lcc_Hclk,
    AdsppmClk_Security_Hclk,
    AdsppmClk_Wrapper_Security_Hclk,
    AdsppmClk_Wrapper_Br_Hclk,
    AdsppmClk_Audio_Core_AON,
    AdsppmClk_Audio_Wrapper_AON,
    AdsppmClk_HwRsp_Core,
    AdsppmClk_Midi_Core,
    AdsppmClk_AvSync_Xo,
    AdsppmClk_AvSync_Bt,
    AdsppmClk_AvSync_Fm,
    AdsppmClk_Slimbus_Core,
    AdsppmClk_Slimbus2_Core,
    AdsppmClk_Avtimer_core,
    AdsppmClk_Atime_core,
    AdsppmClk_Atime2_core,
    AdsppmClk_EnumMax,
    AdsppmClk_EnumForce32Bit = 0x7fffffff
} AdsppmClkIdType;
typedef enum
{
    AdsppmBusPort_None = 0,
    AdsppmBusPort_Adsp_Master,
    AdsppmBusPort_Dml_Master,
    AdsppmBusPort_Aif_Master,
    AdsppmBusPort_Slimbus_Master,
    AdsppmBusPort_Slimbus2_Master,
    AdsppmBusPort_Midi_Master,
    AdsppmBusPort_HwRsmp_Master,
    AdsppmBusPort_Ext_Ahb_Master,
    AdsppmBusPort_Spdif_Master,
    AdsppmBusPort_Hdmirx_Master,
    AdsppmBusPort_Hdmitx_Master,
    AdsppmBusPort_Sif_Master,
    AdsppmBusPort_Dml_Slave,
    AdsppmBusPort_Aif_Slave,
    AdsppmBusPort_Slimbus_Slave,
    AdsppmBusPort_Slimbus2_Slave,
    AdsppmBusPort_Midi_Slave,
    AdsppmBusPort_HwRsmp_Slave,
    AdsppmBusPort_AvSync_Slave,
    AdsppmBusPort_Lpm_Slave,
    AdsppmBusPort_Sram_Slave,
    AdsppmBusPort_Ext_Ahb_Slave,
    AdsppmBusPort_Ddr_Slave,
    AdsppmBusPort_Ocmem_Slave,
    AdsppmBusPort_PerifNoc_Slave,
    AdsppmBusPort_Spdif_Slave,
    AdsppmBusPort_Hdmirx_Slave,
    AdsppmBusPort_Hdmitx_Slave,
    AdsppmBusPort_Sif_Slave,
    AdsppmBusPort_Bstc_Slave,
    AdsppmBusPort_Dcodec_Slave,
    AdsppmBusPort_Core,
    AdsppmBusPort_Adsp_Slave,
    AdsppmBusPort_EnumMax,
    AdsppmBusPort_EnumForce32Bit = 0x7fffffff
} AdsppmBusPortIdType;
typedef struct
{
    AdsppmBusPortIdType masterPort;
    AdsppmBusPortIdType slavePort;
} AdsppmBusRouteType;
typedef struct
{
    uint64 Ab;
    uint64 Ib;
    uint32 latencyNs;
} AdsppmBusBWDataIbAbType;
typedef enum
{
    Adsppm_BwUsage_None,
    Adsppm_BwUsage_DSP,
    Adsppm_BwUsage_DMA,
    Adsppm_BwUsage_EXT_CPU,
    Adsppm_BwUsage_Enum_Max,
    Adsppm_BwUsage_Force32Bbits = 0x7FFFFFFF
} AdsppmBwUsageLpassType;
typedef struct
{
    uint64 bwBytePerSec;
    uint32 usagePercentage;
    AdsppmBwUsageLpassType usageType;
} AdsppmBusBWDataUsageType;
typedef union
{
    AdsppmBusBWDataIbAbType busBwAbIb;
    AdsppmBusBWDataUsageType busBwValue;
} AdsppmBusBWDataType;
typedef enum
{
    AdsppmBusBWOperation_BW = 1,
    AdsppmBusBWOperation_RegAccess
}AdsppmBusBWOperationType;
typedef struct
{
    AdsppmBusRouteType busRoute;
    AdsppmBusBWDataType bwValue;
    AdsppmBusBWOperationType bwOperation;
} AdsppmBusBWRequestValueType;
typedef enum{
    Adsppm_BwReqClass_Generic,
    Adsppm_BwReqClass_Compensated,
    Adsppm_BwReqClass_Force32Bbits = 0x7FFFFFFF
} AdsppmBwRequestClass;
typedef struct
{
    uint32 numOfBw;
    AdsppmBusBWRequestValueType pBwArray[8];
    AdsppmBwRequestClass requestClass;
} AdsppmBwReqType;
typedef struct
{
    uint64 adspDdrBwAb;
    uint64 adspDdrBwIb;
    uint64 extAhbDdrBwAb;
    uint64 extAhbDdrBwIb;
    uint64 intAhbBwAb;
    uint64 intAhbBwIb;
} AdsppmInfoAggregatedBwType;
typedef struct
{
    uint32 ahbeFreqHz;
} AdsppmInfoAhbType;
typedef struct
{
    AdsppmBusBWDataIbAbType dcvsVote;
    AdsppmBusBWDataIbAbType clientsFloorVote;
    AdsppmBusBWDataIbAbType clientsFinalVote;
    AdsppmBusBWDataIbAbType finalVoteToNpa;
} AdsppmInfoDcvsAdspDdrBwType;
typedef struct
{
    uint32 dcvsVote;
    uint32 clientsFloorVote;
    uint32 clientsFinalVote;
    uint32 finalVoteToNpa;
} AdsppmInfoDcvsAdspClockType;
typedef enum
{
    ADSPPM_PRIVATE_INFO_AGGREGATED_BW = 0,
    ADSPPM_PRIVATE_INFO_AHB = 1,
    ADSPPM_PRIVATE_INFO_ADSPDDR_BW = 2,
    ADSPPM_PRIVATE_INFO_ADSPCLOCK = 3,
} AdsppmInfoPrivateTypeType;
typedef struct
{
    AdsppmInfoPrivateTypeType type;
    union
    {
        AdsppmInfoAggregatedBwType aggregatedBw;
        AdsppmInfoAhbType ahb;
        AdsppmInfoDcvsAdspDdrBwType adspDdrBw;
        AdsppmInfoDcvsAdspClockType adspClock;
    };
} AdsppmInfoPrivateType;
typedef enum
{
    MipsRequestOperation_None = 0,
    MipsRequestOperation_MIPS = 1,
    MipsRequestOperation_BWandMIPS = 2,
    MipsRequestOperation_MAX = 3,
    MipsRequestOperation_EnumForce8Bit = 0x7f
} AdsppmMipsOperationType;
typedef enum
{
    Adsppm_Q6ClockRequestUsageType_Mips,
    Adsppm_Q6ClockRequestUsageType_Mpps,
    Adsppm_Q6ClockRequestUsageType_Force32Bbits = 0x7FFFFFFF
} AdsppmQ6ClockRequestUsageType;
typedef struct
{
    uint32 mipsTotal;
    uint32 mipsPerThread;
    AdsppmBusPortIdType codeLocation;
    AdsppmMipsOperationType reqOperation;
} AdsppmMipsRequestType;
typedef struct
{
    uint32 mppsTotal;
    uint32 adspFloorClock;
} AdsppmMppsRequestType;
typedef struct
{
    union
    {
        AdsppmMipsRequestType mipsRequestData;
        AdsppmMppsRequestType mppsRequestData;
    } AdsppmQ6ClockRequestType;
    AdsppmQ6ClockRequestUsageType usageType;
} AdsppmQ6ClockRequestInfoType;
typedef struct
{
    uint32 mipsTotal;
    uint32 mipsPerThread;
} AdsppmMipsDataType;
typedef struct
{
    AdsppmMipsDataType mipsData;
    AdsppmBusBWRequestValueType mipsToBW;
} AdsppmMIPSToBWAggregateType;
typedef struct
{
    AdsppmMipsDataType mipsData;
    uint32 qDSP6Clock;
} AdsppmMIPSToClockAggregateType;
typedef enum
{
    Adsppm_RegProg_None,
    Adsppm_RegProg_Norm,
    Adsppm_RegProg_Fast,
    Adsppm_RegProg_Enum_Max,
    Adsppm_RegProg_EnumForce32Bit = 0x7fffffff
} AdsppmRegProgMatchType;
typedef enum
{
    Adsppm_Freq_At_Least,
    Adsppm_Freq_At_Most,
    Adsppm_Freq_Closest,
    Adsppm_Freq_Exact,
    Adsppm_Freq_Max,
    Adsppm_Freq_Force8Bits = 0x7F
} AdsppmFreqMatchType;
typedef enum
{
    Adsppm_Info_Type_None,
    Adsppm_Info_Type_Max_Value,
    Adsppm_Info_Type_Min_Value,
    Adsppm_Info_Type_Current_Value,
    Adsppm_Info_Type_Max,
    Adsppm_Info_Type_Force8Bits = 0x7F
} AdsppmInfoType;
typedef struct
{
    AdsppmClkIdType clkId;
    uint32 clkFreqHz;
    AdsppmFreqMatchType freqMatch;
} AdsppmClkValType;
typedef struct
{
    uint32 clkId;
    uint32 clkFreqHz;
    uint32 forceMeasure;
} AdsppmInfoClkFreqType;
typedef struct
{
    uint32 numOfClk;
    AdsppmClkValType pClkArray[8];
} AdsppmClkRequestType;
typedef enum
{
    Adsppm_Clk_Domain_Src_Id_Lpass_None,
    Adsppm_Clk_Domain_Src_Id_Primary_PLL,
    Adsppm_Clk_Domain_Src_Id_Secondary_PLL,
    Adsppm_Clk_Domain_Src_Id_Ternery_PLL,
    Adsppm_Clk_Domain_Src_Id_Lpass_Max,
    Adsppm_Clk_Domain_Src_Id_Lpass_Force32bits = 0x7FFFFFFF
} AdsppmClkDomainSrcIdLpassType;
typedef union
{
    AdsppmClkDomainSrcIdLpassType clkDomainSrcIdLpass;
} AdsppmClkDomainSrcIdType;
typedef struct
{
    AdsppmClkIdType clkId;
    uint32 clkFreqHz;
    AdsppmClkDomainSrcIdType clkDomainSrc;
} AdsppmClkDomainType;
typedef struct
{
    uint32 numOfClk;
    AdsppmClkDomainType pClkDomainArray[8];
} AdsppmClkDomainReqType;
typedef enum
{
    Adsppm_Mem_Power_None,
    Adsppm_Mem_Power_Off,
    Adsppm_Mem_Power_Retention,
    Adsppm_Mem_Power_Active,
    Adsppm_Mem_Power_Max,
    Adsppm_Mem_Power_Force32Bits = 0x7FFFFFFF
} AdsppmMemPowerStateType;
typedef enum
{
    Adsppm_Mem_None,
    Adsppm_Mem_Ocmem,
    Adsppm_Mem_Lpass_LPM,
    Adsppm_Mem_Sram,
    Adsppm_Mem_Max,
    Adsppm_MEM_FORCE32BITS = 0x7FFFFFFF
} AdsppmMemIdType;
typedef struct
{
    AdsppmMemIdType memory;
    AdsppmMemPowerStateType powerState;
} AdsppmMemPowerReqParamType;
typedef enum
{
    Adsppm_Param_Id_None = 0,
    Adsppm_Param_Id_Resource_Limit,
    Adsppm_Param_Id_Client_Ocmem_Map,
    Adsppm_Param_Id_Memory_Map,
    Adsppm_Param_Id_Enum_Max,
    Adsppm_Param_Id_Force32bit = 0x7fffffff
} AdsppmParameterIdType;
typedef struct
{
    AdsppmParameterIdType paramId;
    void *pParamConfig;
} AdsppmParameterConfigType;
Adsppm_Status ADSPPM_Init(void);
uint32 ADSPPM_IsInitialized(void);
void ADSPPM_EnterSleep(void);
void ADSPPM_ExitSleep(void);
typedef struct{
    DALSYSSyncHandle resourceLock[Adsppm_Rsc_Id_Max - 1];
    coreUtils_Q_Type rscReqQ[Adsppm_Rsc_Id_Max - 1];
    uint32 periodicUseCase;
} AdsppmCoreCtxType;
AdsppmCoreCtxType *GetAdsppmCoreContext(void);
uint32 IsPeriodicUseCase(void);
void SetPeriodicUseCase(uint32 periodic);
typedef enum
{
  NPA_SUCCESS = 0,
  NPA_ERROR = -1,
} npa_status;
typedef enum
{
  NPA_NO_CLIENT = 0x7fffffff,
  NPA_CLIENT_RESERVED1 = (1 << 0),
  NPA_CLIENT_RESERVED2 = (1 << 1),
  NPA_CLIENT_CUSTOM1 = (1 << 2),
  NPA_CLIENT_CUSTOM2 = (1 << 3),
  NPA_CLIENT_CUSTOM3 = (1 << 4),
  NPA_CLIENT_CUSTOM4 = (1 << 5),
  NPA_CLIENT_REQUIRED = (1 << 6),
  NPA_CLIENT_ISOCHRONOUS = (1 << 7),
  NPA_CLIENT_IMPULSE = (1 << 8),
  NPA_CLIENT_LIMIT_MAX = (1 << 9),
  NPA_CLIENT_VECTOR = (1 << 10),
  NPA_CLIENT_SUPPRESSIBLE = (1 << 11),
  NPA_CLIENT_SUPPRESSIBLE_VECTOR = (1 << 12),
  NPA_CLIENT_CUSTOM5 = (1 << 13),
  NPA_CLIENT_CUSTOM6 = (1 << 14),
  NPA_CLIENT_SUPPRESSIBLE2 = (1 << 15),
  NPA_CLIENT_SUPPRESSIBLE2_VECTOR = (1 << 16),
} npa_client_type;
typedef enum
{
  NPA_TRIGGER_RESERVED_EVENT_0,
  NPA_TRIGGER_RESERVED_EVENT_1,
  NPA_TRIGGER_RESERVED_EVENT_2,
  NPA_TRIGGER_CHANGE_EVENT,
  NPA_TRIGGER_WATERMARK_EVENT,
  NPA_TRIGGER_THRESHOLD_EVENT,
  NPA_TRIGGER_CUSTOM_EVENT1 = 0x010,
  NPA_TRIGGER_CUSTOM_EVENT2 = 0x020,
  NPA_TRIGGER_CUSTOM_EVENT3 = 0x040,
  NPA_TRIGGER_CUSTOM_EVENT4 = 0x080,
  NPA_TRIGGER_PRE_CHANGE_EVENT = 0x0100,
  NPA_TRIGGER_POST_CHANGE_EVENT = 0x0200,
  NPA_TRIGGER_NAS_REQUEST_ACTIVE_EVENT = 0x0400,
  NPA_TRIGGER_MAX_EVENT = 0x0000ffff,
  NPA_TRIGGER_RESERVED_BIT_30 = 0x40000000,
  NPA_TRIGGER_RESERVED_BIT_31 = ( int )0x80000000
} npa_event_trigger_type;
typedef enum
{
  NPA_EVENT_RESERVED,
  NPA_EVENT_RESERVED_1,
  NPA_EVENT_RESERVED_2,
  NPA_EVENT_HI_WATERMARK,
  NPA_EVENT_LO_WATERMARK,
  NPA_EVENT_CHANGE,
  NPA_EVENT_THRESHOLD_LO,
  NPA_EVENT_THRESHOLD_NOMINAL,
  NPA_EVENT_THRESHOLD_HI,
  NPA_EVENT_CUSTOM1 = 0x010,
  NPA_EVENT_CUSTOM2 = 0x020,
  NPA_EVENT_CUSTOM3 = 0x040,
  NPA_EVENT_CUSTOM4 = 0x080,
  NPA_EVENT_PRE_CHANGE = 0x0100,
  NPA_EVENT_POST_CHANGE = 0x0200,
  NPA_EVENT_NAS_REQUEST_ACTIVE = 0x0400,
  NPA_NUM_EVENT_TYPES
} npa_event_type;
typedef enum
{
  NPA_REQUEST_DEFAULT = 0,
  NPA_REQUEST_FIRE_AND_FORGET = 0x00000001,
  NPA_REQUEST_NEXT_AWAKE = 0x00000002,
  NPA_REQUEST_CHANGED_TYPE = 0x00000004,
  NPA_REQUEST_BEST_EFFORT = 0x00000008,
  NPA_REQUEST_RESERVED_ATTRIBUTE1 = 0x00000010,
  NPA_REQUEST_MULTIPLE_NEXT_AWAKE = 0x00000020,
} npa_request_attribute;
enum {
  NPA_QUERY_CURRENT_STATE,
  NPA_QUERY_CLIENT_ACTIVE_REQUEST,
  NPA_QUERY_ACTIVE_MAX,
  NPA_QUERY_RESOURCE_MAX,
  NPA_QUERY_RESOURCE_DISABLED,
  NPA_QUERY_RESOURCE_LATENCY,
  NPA_QUERY_CURRENT_AGGREGATION,
  NPA_MAX_PUBLIC_QUERY = 1023,
  NPA_QUERY_RESERVED_BEGIN = 1024,
  NPA_QUERY_REMOTE_RESOURCE_AVAILABLE = 1025,
  NPA_QUERY_RESERVED_END = 4095
};
typedef enum {
  NPA_QUERY_SUCCESS = 0,
  NPA_QUERY_UNSUPPORTED_QUERY_ID,
  NPA_QUERY_UNKNOWN_RESOURCE,
  NPA_QUERY_NULL_POINTER,
  NPA_QUERY_NO_VALUE,
} npa_query_status;
typedef unsigned int npa_resource_state;
typedef int npa_resource_state_delta;
typedef uint32 npa_time_sclk;
typedef npa_time_sclk npa_resource_time;
typedef void ( *npa_callback )( void *context,
                                unsigned int event_type,
                                void *data,
                                unsigned int data_size );
typedef void ( *npa_simple_callback )( void *context );
typedef struct npa_client * npa_client_handle;
typedef struct npa_event * npa_event_handle;
typedef struct npa_custom_event * npa_custom_event_handle;
typedef struct npa_event_data
{
  const char *resource_name;
  npa_resource_state state;
  npa_resource_state_delta headroom;
} npa_event_data;
typedef struct npa_prepost_change_data
{
  const char *resource_name;
  npa_resource_state from_state;
  npa_resource_state to_state;
  void *data;
} npa_prepost_change_data;
typedef struct npa_link * npa_query_handle;
typedef enum
{
   NPA_QUERY_TYPE_STATE,
   NPA_QUERY_TYPE_VALUE,
   NPA_QUERY_TYPE_NAME,
   NPA_QUERY_TYPE_REFERENCE,
   NPA_QUERY_TYPE_VALUE64,
   NPA_QUERY_TYPE_STATE_VECTOR,
   NPA_QUERY_NUM_TYPES
} npa_query_type_enum;
typedef struct npa_query_type
{
  npa_query_type_enum type;
  union
  {
    npa_resource_state state;
    unsigned int value;
    unsigned long long value64;
    char *name;
    void *reference;
    npa_resource_state *state_vector;
  } data;
} npa_query_type;
npa_client_handle npa_create_sync_client_ex( const char *resource_name,
                                             const char *client_name,
                                             npa_client_type client_type,
                                             unsigned int client_value,
                                             void *client_ref );
npa_client_handle
npa_create_async_client_cb_ex( const char *resource_name,
                               const char *client_name,
                               npa_client_type client_type,
                               npa_callback callback,
                               void *context,
                               unsigned int client_value,
                               void *client_ref );
void npa_destroy_client( npa_client_handle client );
void npa_issue_scalar_request( npa_client_handle client,
                               npa_resource_state state );
void npa_modify_required_request( npa_client_handle client,
                                  npa_resource_state_delta delta );
void npa_issue_scalar_request_unconditional( npa_client_handle client,
                                             npa_resource_state state );
void npa_issue_impulse_request( npa_client_handle client );
void npa_issue_vector_request( npa_client_handle client,
                               unsigned int num_elems,
                               npa_resource_state *vector );
void npa_issue_isoc_request( npa_client_handle client,
                             npa_resource_time deadline,
                             npa_resource_state level_hint );
void npa_issue_limit_max_request( npa_client_handle client,
                                  npa_resource_state max );
void npa_complete_request( npa_client_handle client );
void npa_cancel_request( npa_client_handle client );
void npa_set_request_attribute( npa_client_handle client,
                                npa_request_attribute attr );
void npa_set_client_type_required( npa_client_handle client );
void npa_set_client_type_suppressible( npa_client_handle client );
npa_event_handle
npa_create_event_cb( const char *resource_name,
                     const char *event_name,
                     npa_event_trigger_type trigger_type,
                     npa_callback callback,
                     void *context );
void npa_set_event_watermarks( npa_event_handle event,
                               npa_resource_state_delta hi_watermark,
                               npa_resource_state_delta lo_watermark );
void npa_set_event_thresholds( npa_event_handle event,
                               npa_resource_state lo_threshold,
                               npa_resource_state hi_threshold );
npa_event_handle npa_create_custom_event( const char *resource_name,
                                          const char *event_name,
                                          unsigned int trigger_type,
                                          void *reg_data,
                                          npa_callback callback,
                                          void *context );
void npa_destroy_custom_event( npa_event_handle event );
void npa_destroy_event_handle( npa_event_handle event );
npa_query_handle npa_create_query_handle( const char * resource_name );
void npa_destroy_query_handle( npa_query_handle query );
npa_query_status npa_query( npa_query_handle query,
                            uint32 query_id,
                            npa_query_type *query_result );
npa_query_status npa_query_by_name( const char *resource_name,
                                    uint32 query_id,
                                    npa_query_type *query_result );
npa_query_status npa_query_by_client( npa_client_handle client,
                                      uint32 query_id,
                                      npa_query_type *query_result );
npa_query_status npa_query_by_event( npa_event_handle event,
                                     uint32 query_id,
                                     npa_query_type *query_result );
npa_query_status npa_query_resource_available( const char *resource_name );
void npa_resources_available_cb( unsigned int num_resources,
                                 const char *resources[],
                                 npa_callback callback,
                                 void *context );
void npa_resource_available_cb( const char *resource_name,
                                npa_callback callback,
                                void *context );
void npa_dal_event_callback( void *dal_event_handle,
                             unsigned int event_type,
                             void *data,
                             unsigned int data_size );
typedef enum
{
  NPA_FORK_DEFAULT = 0,
  NPA_FORK_DISALLOWED,
  NPA_FORK_ALLOWED,
} npa_fork_pref;
typedef unsigned int (*npa_join_function) ( void *, void * );
void npa_set_client_fork_pref( npa_client_handle client,
                               npa_fork_pref pref );
npa_fork_pref npa_get_client_fork_pref( npa_client_handle client );
void npa_join_request( npa_client_handle client );
typedef enum
{
  NPA_RESOURCE_DEFAULT = 0,
  NPA_RESOURCE_DRIVER_UNCONDITIONAL = 0x00000001,
  NPA_RESOURCE_SINGLE_CLIENT = 0x00000002,
  NPA_RESOURCE_VECTOR_STATE = 0x00000004,
  NPA_RESOURCE_REMOTE_ACCESS_ALLOWED = 0x00000008,
  NPA_RESOURCE_INTERPROCESS_ACCESS_ALLOWED = 0x00000008,
  NPA_RESOURCE_REMOTE = 0x00000010,
  NPA_RESOURCE_REMOTE_PROXY = 0x00000020,
  NPA_RESOURCE_REMOTE_NO_INIT = 0x00000040,
  NPA_RESOURCE_SUPPORTS_SUPPRESSIBLE = 0x00000080,
  NPA_RESOURCE_DRIVER_UNCONDITIONAL_FIRST = 0x00000200,
  NPA_RESOURCE_LPR_ISSUABLE = 0x00000400,
  NPA_RESOURCE_ALLOW_CLIENT_TYPE_CHANGE = 0x00000800,
  NPA_RESOURCE_ENABLE_PREPOST_CHANGE_EVENTS = 0x00001000,
} npa_resource_attribute;
typedef enum
{
  NPA_NODE_DEFAULT = 0,
  NPA_NODE_NO_LOCK = 0x00000001,
  NPA_NODE_DISABLEABLE = 0x00000002,
  NPA_NODE_FORKABLE = 0x00000004,
} npa_node_attribute;
enum
{
  NPA_RESOURCE_AUTHOR_QUERY_START = NPA_MAX_PUBLIC_QUERY,
  NPA_QUERY_RESOURCE_ATTRIBUTES,
  NPA_QUERY_NODE_ATTRIBUTES,
  NPA_MAX_RESOURCE_AUTHOR_QUERY = 2047
};
typedef void* npa_user_data;
typedef struct npa_event_callback
{
  npa_callback callback;
  npa_user_data context;
} npa_event_callback;
typedef struct npa_work_request
{
  npa_resource_state state;
  union
  {
    npa_resource_state *vector;
    char *string;
    void *reference;
  } pointer;
} npa_work_request;
typedef struct npa_client
{
  struct npa_client *prev, *next;
  const char *name;
  const char *resource_name;
  struct npa_resource *resource;
  npa_user_data resource_data;
  npa_client_type type;
  npa_work_request work[3];
  unsigned int index;
  unsigned int sequence;
  struct npa_async_client_data *async;
  void *log_handle;
  unsigned int request_attr;
  void (*issue_request)( npa_client_handle client, int new_request );
  struct npa_scheduler_data *request_ptr;
} npa_client;
typedef struct npa_event
{
  struct npa_event *prev, *next;
  unsigned int trigger_type;
  const char *name;
  struct npa_resource *resource;
  union
  {
    npa_resource_state_delta watermark;
    npa_resource_state threshold;
  } lo;
  union
  {
    npa_resource_state_delta watermark;
    npa_resource_state threshold;
  } hi;
  npa_event_callback callback;
  void *reg_data;
  struct npa_event_action *action;
} npa_event;
typedef struct npa_link
{
  struct npa_link *next, *prev;
  const char *name;
  struct npa_resource *resource;
} npa_link;
typedef npa_resource_state (*npa_resource_driver_fcn) (
  struct npa_resource *resource,
  npa_client_handle client,
  npa_resource_state state
  );
typedef npa_resource_state (*npa_resource_update_fcn)(
  struct npa_resource *resource,
  npa_client_handle client
  );
typedef struct npa_resource_latency {
  uint32 request;
  uint32 fork;
  uint32 notify;
} npa_resource_latency;
typedef npa_query_status (*npa_resource_query_fcn)(
  struct npa_resource *resource,
  unsigned int query_id,
  npa_query_type *query_result );
typedef npa_query_status (*npa_link_query_fcn)(
  struct npa_link *resource_link,
  unsigned int query_id,
  npa_query_type *query_result );
typedef struct npa_resource_plugin
{
  npa_resource_update_fcn update_fcn;
  unsigned int supported_clients;
  void (*create_client_fcn) ( npa_client * );
  void (*destroy_client_fcn)( npa_client * );
  unsigned int (*create_client_ex_fcn)( npa_client *, unsigned int, void * );
  void (*cancel_client_fcn) ( npa_client *);
} npa_resource_plugin;
typedef struct npa_node_dependency
{
  const char *name;
  npa_client_type client_type;
  npa_client_handle handle;
} npa_node_dependency;
typedef struct npa_resource_definition
{
  const char *name;
  const char *units;
  npa_resource_state max;
  const npa_resource_plugin *plugin;
  unsigned int attributes;
  npa_user_data data;
  npa_resource_query_fcn query_fcn;
  npa_link_query_fcn link_query_fcn;
  struct npa_resource *handle;
} npa_resource_definition;
typedef struct npa_node_definition
{
  const char *name;
  npa_resource_driver_fcn driver_fcn;
  unsigned int attributes;
  npa_user_data data;
  unsigned int dependency_count;
  npa_node_dependency *dependencies;
  unsigned int resource_count;
  npa_resource_definition *resources;
} npa_node_definition;
typedef struct npa_resource
{
  npa_resource_definition *definition;
  npa_node_definition *node;
  unsigned int index;
  npa_client *clients;
  union
  {
    npa_event *creation_events;
    struct npa_event_list *list;
  } events;
  const npa_resource_plugin *active_plugin;
  npa_resource_state request_state;
  npa_resource_state active_state;
  npa_resource_state internal_state[8];
  npa_resource_state *state_vector;
  npa_resource_state *required_state_vector;
  npa_resource_state *suppressible_state_vector;
  npa_resource_state *suppressible2_state_vector;
  npa_resource_state *semiactive_state_vector;
  npa_resource_state active_max;
  npa_resource_state_delta active_headroom;
  struct CoreMutex *node_lock;
  struct CoreMutex *event_lock;
  struct npa_resource_internal_data *_internal;
  unsigned int sequence;
  void *log_handle;
  npa_resource_latency *latency;
} npa_resource;
typedef void* npa_join_token;
void npa_define_node_cb( npa_node_definition *node,
                         npa_resource_state initial_state[],
                         npa_callback node_cb,
                         void *context);
void npa_alias_resource_cb( const char *resource_name,
                            const char *alias_name,
                            npa_callback alias_cb,
                            void *context);
void npa_define_marker( const char *marker_name );
void npa_define_marker_with_attributes( const char *marker_name,
                                        npa_resource_attribute attributes );
void npa_issue_dependency_request( npa_client_handle cur_client,
                                   npa_client_handle req_client,
                                   npa_resource_state req_state,
                                   npa_client_handle sup_client,
                                   npa_resource_state sup_state );
void npa_issue_dependency_vector_request( npa_client_handle cur_client,
                                          npa_client_handle req_client,
                                          unsigned int req_num_elems,
                                          npa_resource_state *req_vector,
                                          npa_client_handle sup_client,
                                          unsigned int sup_num_elems,
                                          npa_resource_state *sup_vector );
void npa_assign_resource_state( npa_resource *resource,
                                npa_resource_state state );
npa_resource *npa_query_get_resource( npa_query_handle query_handle );
void npa_enable_node( npa_node_definition *node, npa_resource_state default_state[] );
void npa_disable_node( npa_node_definition *node );
npa_join_token npa_fork_resource( npa_resource *resource,
                                  npa_join_function join_func,
                                  void *join_data );
void npa_resource_lock( npa_resource *resource );
int npa_resource_trylock( npa_resource *resource );
void npa_resource_unlock( npa_resource *resource );
unsigned int npa_request_has_attribute( npa_client_handle client,
                                        npa_request_attribute attr );
unsigned int npa_get_request_attributes( npa_client_handle client );
npa_client_handle npa_pass_request_attributes( npa_client_handle current,
                                               npa_client_handle dependency );
void npa_change_resource_plugin( const char *resource_name,
                                 const npa_resource_plugin *plugin );
extern const npa_resource_plugin npa_binary_plugin;
extern const npa_resource_plugin npa_max_plugin;
extern const npa_resource_plugin npa_min_plugin;
extern const npa_resource_plugin npa_sum_plugin;
extern const npa_resource_plugin npa_identity_plugin;
extern const npa_resource_plugin npa_always_on_plugin;
extern const npa_resource_plugin npa_impulse_plugin;
extern const npa_resource_plugin npa_or_plugin;
extern const npa_resource_plugin npa_binary_and_plugin;
extern const npa_resource_plugin npa_no_client_plugin;
npa_resource_state npa_min_update_fcn( npa_resource *resource,
                                       npa_client_handle client);
npa_resource_state npa_max_update_fcn( npa_resource *resource,
                                       npa_client_handle client );
npa_resource_state npa_sum_update_fcn( npa_resource *resource,
                                       npa_client_handle client );
npa_resource_state npa_binary_update_fcn( npa_resource *resource,
                                          npa_client_handle client );
npa_resource_state npa_or_update_fcn( npa_resource *resource,
                                      npa_client_handle client);
npa_resource_state npa_binary_and_update_fcn( npa_resource *resource,
                                              npa_client_handle client);
npa_resource_state npa_identity_update_fcn( npa_resource *resource,
                                            npa_client_handle client );
npa_resource_state npa_always_on_update_fcn( npa_resource *resource,
                                             npa_client_handle client );
npa_resource_state npa_impulse_update_fcn( npa_resource *resource,
                                           npa_client_handle client );
void npa_issue_internal_request( npa_client_handle client );
npa_status npa_resource_add_system_event_callback( const char *resource_name,
                                                   npa_callback callback,
                                                   void *context );
npa_status npa_resource_remove_system_event_callback( const char *resource_name,
                                                      npa_callback callback,
                                                      void *context );
void npa_post_custom_event( npa_event_handle event,
                            npa_event_type type, void *event_data );
void npa_post_custom_event_nodups( npa_event_handle event,
                                   npa_event_type type, void *data );
void npa_post_custom_events( npa_resource *resource,
                             npa_event_type type, void *event_data );
void npa_dispatch_custom_event( npa_event_handle event,
                                npa_event_type type, void *data );
void npa_dispatch_custom_events( npa_resource *resource,
                                 npa_event_type type, void *data );
npa_event_handle
npa_get_first_event_of_trigger_type( npa_resource *resource,
                                     unsigned int trigger_type );
npa_event_handle
npa_get_next_event_of_trigger_type( npa_event_handle event,
                                    unsigned int trigger_type );
void npa_dispatch_pre_change_events( npa_resource *resource,
                                     npa_resource_state from_state,
                                     npa_resource_state to_state,
                                     void *data );
void npa_dispatch_post_change_events( npa_resource *resource,
                                      npa_resource_state from_state,
                                      npa_resource_state to_state,
                                      void *data );
typedef enum {
  ICBID_MASTER_APPSS_PROC = 0,
  ICBID_MASTER_MSS_PROC,
  ICBID_MASTER_MNOC_BIMC,
  ICBID_MASTER_SNOC_BIMC,
  ICBID_MASTER_SNOC_BIMC_0 = ICBID_MASTER_SNOC_BIMC,
  ICBID_MASTER_CNOC_MNOC_MMSS_CFG,
  ICBID_MASTER_CNOC_MNOC_CFG,
  ICBID_MASTER_GFX3D,
  ICBID_MASTER_JPEG,
  ICBID_MASTER_MDP,
  ICBID_MASTER_MDP0 = ICBID_MASTER_MDP,
  ICBID_MASTER_MDPS = ICBID_MASTER_MDP,
  ICBID_MASTER_VIDEO,
  ICBID_MASTER_VIDEO_P0 = ICBID_MASTER_VIDEO,
  ICBID_MASTER_VIDEO_P1,
  ICBID_MASTER_VFE,
  ICBID_MASTER_VFE0 = ICBID_MASTER_VFE,
  ICBID_MASTER_CNOC_ONOC_CFG,
  ICBID_MASTER_JPEG_OCMEM,
  ICBID_MASTER_MDP_OCMEM,
  ICBID_MASTER_VIDEO_P0_OCMEM,
  ICBID_MASTER_VIDEO_P1_OCMEM,
  ICBID_MASTER_VFE_OCMEM,
  ICBID_MASTER_LPASS_AHB,
  ICBID_MASTER_QDSS_BAM,
  ICBID_MASTER_SNOC_CFG,
  ICBID_MASTER_BIMC_SNOC,
  ICBID_MASTER_BIMC_SNOC_0 = ICBID_MASTER_BIMC_SNOC,
  ICBID_MASTER_CNOC_SNOC,
  ICBID_MASTER_CRYPTO,
  ICBID_MASTER_CRYPTO_CORE0 = ICBID_MASTER_CRYPTO,
  ICBID_MASTER_CRYPTO_CORE1,
  ICBID_MASTER_LPASS_PROC,
  ICBID_MASTER_MSS,
  ICBID_MASTER_MSS_NAV,
  ICBID_MASTER_OCMEM_DMA,
  ICBID_MASTER_PNOC_SNOC,
  ICBID_MASTER_WCSS,
  ICBID_MASTER_QDSS_ETR,
  ICBID_MASTER_USB3,
  ICBID_MASTER_USB3_0 = ICBID_MASTER_USB3,
  ICBID_MASTER_SDCC_1,
  ICBID_MASTER_SDCC_3,
  ICBID_MASTER_SDCC_2,
  ICBID_MASTER_SDCC_4,
  ICBID_MASTER_TSIF,
  ICBID_MASTER_BAM_DMA,
  ICBID_MASTER_BLSP_2,
  ICBID_MASTER_USB_HSIC,
  ICBID_MASTER_BLSP_1,
  ICBID_MASTER_USB_HS,
  ICBID_MASTER_USB_HS1 = ICBID_MASTER_USB_HS,
  ICBID_MASTER_PNOC_CFG,
  ICBID_MASTER_SNOC_PNOC,
  ICBID_MASTER_RPM_INST,
  ICBID_MASTER_RPM_DATA,
  ICBID_MASTER_RPM_SYS,
  ICBID_MASTER_DEHR,
  ICBID_MASTER_QDSS_DAP,
  ICBID_MASTER_SPDM,
  ICBID_MASTER_TIC,
  ICBID_MASTER_SNOC_CNOC,
  ICBID_MASTER_GFX3D_OCMEM,
  ICBID_MASTER_GFX3D_GMEM = ICBID_MASTER_GFX3D_OCMEM,
  ICBID_MASTER_OVIRT_SNOC,
  ICBID_MASTER_SNOC_OVIRT,
  ICBID_MASTER_SNOC_GVIRT = ICBID_MASTER_SNOC_OVIRT,
  ICBID_MASTER_ONOC_OVIRT,
  ICBID_MASTER_USB_HS2,
  ICBID_MASTER_QPIC,
  ICBID_MASTER_IPA,
  ICBID_MASTER_DSI,
  ICBID_MASTER_MDP1,
  ICBID_MASTER_MDPE = ICBID_MASTER_MDP1,
  ICBID_MASTER_VPU_PROC,
  ICBID_MASTER_VPU,
  ICBID_MASTER_VPU0 = ICBID_MASTER_VPU,
  ICBID_MASTER_CRYPTO_CORE2,
  ICBID_MASTER_PCIE_0,
  ICBID_MASTER_PCIE_1,
  ICBID_MASTER_SATA,
  ICBID_MASTER_UFS,
  ICBID_MASTER_USB3_1,
  ICBID_MASTER_VIDEO_OCMEM,
  ICBID_MASTER_VPU1,
  ICBID_MASTER_VCAP,
  ICBID_MASTER_EMAC,
  ICBID_MASTER_BCAST,
  ICBID_MASTER_MMSS_PROC,
  ICBID_MASTER_SNOC_BIMC_1,
  ICBID_MASTER_SNOC_PCNOC,
  ICBID_MASTER_AUDIO,
  ICBID_MASTER_MM_INT_0,
  ICBID_MASTER_MM_INT_1,
  ICBID_MASTER_MM_INT_2,
  ICBID_MASTER_MM_INT_BIMC,
  ICBID_MASTER_MSS_INT,
  ICBID_MASTER_PCNOC_CFG,
  ICBID_MASTER_PCNOC_INT_0,
  ICBID_MASTER_PCNOC_INT_1,
  ICBID_MASTER_PCNOC_M_0,
  ICBID_MASTER_PCNOC_M_1,
  ICBID_MASTER_PCNOC_S_0,
  ICBID_MASTER_PCNOC_S_1,
  ICBID_MASTER_PCNOC_S_2,
  ICBID_MASTER_PCNOC_S_3,
  ICBID_MASTER_PCNOC_S_4,
  ICBID_MASTER_PCNOC_S_6,
  ICBID_MASTER_PCNOC_S_7,
  ICBID_MASTER_PCNOC_S_8,
  ICBID_MASTER_PCNOC_S_9,
  ICBID_MASTER_QDSS_INT,
  ICBID_MASTER_SNOC_INT_0,
  ICBID_MASTER_SNOC_INT_1,
  ICBID_MASTER_SNOC_INT_BIMC,
  ICBID_MASTER_TCU_0,
  ICBID_MASTER_TCU_1,
  ICBID_MASTER_BIMC_INT_0,
  ICBID_MASTER_BIMC_INT_1,
  ICBID_MASTER_CAMERA,
  ICBID_MASTER_RICA,
  ICBID_MASTER_SNOC_BIMC_2,
  ICBID_MASTER_BIMC_SNOC_1,
  ICBID_MASTER_A0NOC_SNOC,
  ICBID_MASTER_A1NOC_SNOC,
  ICBID_MASTER_A2NOC_SNOC,
  ICBID_MASTER_PIMEM,
  ICBID_MASTER_SNOC_VMEM,
  ICBID_MASTER_CPP,
  ICBID_MASTER_CNOC_A1NOC,
  ICBID_MASTER_PNOC_A1NOC,
  ICBID_MASTER_HMSS,
  ICBID_MASTER_PCIE_2,
  ICBID_MASTER_ROTATOR,
  ICBID_MASTER_VENUS_VMEM,
  ICBID_MASTER_DCC,
  ICBID_MASTER_MCDMA,
  ICBID_MASTER_PCNOC_INT_2,
  ICBID_MASTER_PCNOC_INT_3,
  ICBID_MASTER_PCNOC_INT_4,
  ICBID_MASTER_PCNOC_INT_5,
  ICBID_MASTER_PCNOC_INT_6,
  ICBID_MASTER_PCNOC_S_5,
  ICBID_MASTER_SENSORS_AHB,
  ICBID_MASTER_SENSORS_PROC,
  ICBID_MASTER_QSPI,
  ICBID_MASTER_VFE1,
  ICBID_MASTER_SNOC_INT_2,
  ICBID_MASTER_SMMNOC_BIMC,
  ICBID_MASTER_CRVIRT_A1NOC,
  ICBID_MASTER_XM_USB_HS1,
  ICBID_MASTER_XI_USB_HS1,
  ICBID_MASTER_COUNT,
  ICBID_MASTER_SIZE = 0x7FFFFFFF
} ICBId_MasterType;
typedef enum {
  ICBID_SLAVE_EBI1 = 0,
  ICBID_SLAVE_APPSS_L2,
  ICBID_SLAVE_BIMC_SNOC,
  ICBID_SLAVE_BIMC_SNOC_0 = ICBID_SLAVE_BIMC_SNOC,
  ICBID_SLAVE_CAMERA_CFG,
  ICBID_SLAVE_DISPLAY_CFG,
  ICBID_SLAVE_OCMEM_CFG,
  ICBID_SLAVE_CPR_CFG,
  ICBID_SLAVE_CPR_XPU_CFG,
  ICBID_SLAVE_MISC_CFG,
  ICBID_SLAVE_MISC_XPU_CFG,
  ICBID_SLAVE_VENUS_CFG,
  ICBID_SLAVE_GFX3D_CFG,
  ICBID_SLAVE_MMSS_CLK_CFG,
  ICBID_SLAVE_MMSS_CLK_XPU_CFG,
  ICBID_SLAVE_MNOC_MPU_CFG,
  ICBID_SLAVE_ONOC_MPU_CFG,
  ICBID_SLAVE_MNOC_BIMC,
  ICBID_SLAVE_SERVICE_MNOC,
  ICBID_SLAVE_OCMEM,
  ICBID_SLAVE_GMEM = ICBID_SLAVE_OCMEM,
  ICBID_SLAVE_SERVICE_ONOC,
  ICBID_SLAVE_APPSS,
  ICBID_SLAVE_LPASS,
  ICBID_SLAVE_USB3,
  ICBID_SLAVE_USB3_0 = ICBID_SLAVE_USB3,
  ICBID_SLAVE_WCSS,
  ICBID_SLAVE_SNOC_BIMC,
  ICBID_SLAVE_SNOC_BIMC_0 = ICBID_SLAVE_SNOC_BIMC,
  ICBID_SLAVE_SNOC_CNOC,
  ICBID_SLAVE_IMEM,
  ICBID_SLAVE_OCIMEM = ICBID_SLAVE_IMEM,
  ICBID_SLAVE_SNOC_OVIRT,
  ICBID_SLAVE_SNOC_GVIRT = ICBID_SLAVE_SNOC_OVIRT,
  ICBID_SLAVE_SNOC_PNOC,
  ICBID_SLAVE_SNOC_PCNOC = ICBID_SLAVE_SNOC_PNOC,
  ICBID_SLAVE_SERVICE_SNOC,
  ICBID_SLAVE_QDSS_STM,
  ICBID_SLAVE_SDCC_1,
  ICBID_SLAVE_SDCC_3,
  ICBID_SLAVE_SDCC_2,
  ICBID_SLAVE_SDCC_4,
  ICBID_SLAVE_TSIF,
  ICBID_SLAVE_BAM_DMA,
  ICBID_SLAVE_BLSP_2,
  ICBID_SLAVE_USB_HSIC,
  ICBID_SLAVE_BLSP_1,
  ICBID_SLAVE_USB_HS,
  ICBID_SLAVE_USB_HS1 = ICBID_SLAVE_USB_HS,
  ICBID_SLAVE_PDM,
  ICBID_SLAVE_PERIPH_APU_CFG,
  ICBID_SLAVE_PNOC_MPU_CFG,
  ICBID_SLAVE_PRNG,
  ICBID_SLAVE_PNOC_SNOC,
  ICBID_SLAVE_PCNOC_SNOC = ICBID_SLAVE_PNOC_SNOC,
  ICBID_SLAVE_SERVICE_PNOC,
  ICBID_SLAVE_CLK_CTL,
  ICBID_SLAVE_CNOC_MSS,
  ICBID_SLAVE_PCNOC_MSS = ICBID_SLAVE_CNOC_MSS,
  ICBID_SLAVE_SECURITY,
  ICBID_SLAVE_TCSR,
  ICBID_SLAVE_TLMM,
  ICBID_SLAVE_CRYPTO_0_CFG,
  ICBID_SLAVE_CRYPTO_1_CFG,
  ICBID_SLAVE_IMEM_CFG,
  ICBID_SLAVE_MESSAGE_RAM,
  ICBID_SLAVE_BIMC_CFG,
  ICBID_SLAVE_BOOT_ROM,
  ICBID_SLAVE_CNOC_MNOC_MMSS_CFG,
  ICBID_SLAVE_PMIC_ARB,
  ICBID_SLAVE_SPDM_WRAPPER,
  ICBID_SLAVE_DEHR_CFG,
  ICBID_SLAVE_MPM,
  ICBID_SLAVE_QDSS_CFG,
  ICBID_SLAVE_RBCPR_CFG,
  ICBID_SLAVE_RBCPR_CX_CFG = ICBID_SLAVE_RBCPR_CFG,
  ICBID_SLAVE_RBCPR_QDSS_APU_CFG,
  ICBID_SLAVE_CNOC_MNOC_CFG,
  ICBID_SLAVE_SNOC_MPU_CFG,
  ICBID_SLAVE_CNOC_ONOC_CFG,
  ICBID_SLAVE_PNOC_CFG,
  ICBID_SLAVE_SNOC_CFG,
  ICBID_SLAVE_EBI1_DLL_CFG,
  ICBID_SLAVE_PHY_APU_CFG,
  ICBID_SLAVE_EBI1_PHY_CFG,
  ICBID_SLAVE_RPM,
  ICBID_SLAVE_CNOC_SNOC,
  ICBID_SLAVE_SERVICE_CNOC,
  ICBID_SLAVE_OVIRT_SNOC,
  ICBID_SLAVE_OVIRT_OCMEM,
  ICBID_SLAVE_USB_HS2,
  ICBID_SLAVE_QPIC,
  ICBID_SLAVE_IPS_CFG,
  ICBID_SLAVE_DSI_CFG,
  ICBID_SLAVE_USB3_1,
  ICBID_SLAVE_PCIE_0,
  ICBID_SLAVE_PCIE_1,
  ICBID_SLAVE_PSS_SMMU_CFG,
  ICBID_SLAVE_CRYPTO_2_CFG,
  ICBID_SLAVE_PCIE_0_CFG,
  ICBID_SLAVE_PCIE_1_CFG,
  ICBID_SLAVE_SATA_CFG,
  ICBID_SLAVE_SPSS_GENI_IR,
  ICBID_SLAVE_UFS_CFG,
  ICBID_SLAVE_AVSYNC_CFG,
  ICBID_SLAVE_VPU_CFG,
  ICBID_SLAVE_USB_PHY_CFG,
  ICBID_SLAVE_RBCPR_MX_CFG,
  ICBID_SLAVE_PCIE_PARF,
  ICBID_SLAVE_VCAP_CFG,
  ICBID_SLAVE_EMAC_CFG,
  ICBID_SLAVE_BCAST_CFG,
  ICBID_SLAVE_KLM_CFG,
  ICBID_SLAVE_DISPLAY_PWM,
  ICBID_SLAVE_GENI,
  ICBID_SLAVE_SNOC_BIMC_1,
  ICBID_SLAVE_AUDIO,
  ICBID_SLAVE_CATS_0,
  ICBID_SLAVE_CATS_1,
  ICBID_SLAVE_MM_INT_0,
  ICBID_SLAVE_MM_INT_1,
  ICBID_SLAVE_MM_INT_2,
  ICBID_SLAVE_MM_INT_BIMC,
  ICBID_SLAVE_MMU_MODEM_XPU_CFG,
  ICBID_SLAVE_MSS_INT,
  ICBID_SLAVE_PCNOC_INT_0,
  ICBID_SLAVE_PCNOC_INT_1,
  ICBID_SLAVE_PCNOC_M_0,
  ICBID_SLAVE_PCNOC_M_1,
  ICBID_SLAVE_PCNOC_S_0,
  ICBID_SLAVE_PCNOC_S_1,
  ICBID_SLAVE_PCNOC_S_2,
  ICBID_SLAVE_PCNOC_S_3,
  ICBID_SLAVE_PCNOC_S_4,
  ICBID_SLAVE_PCNOC_S_6,
  ICBID_SLAVE_PCNOC_S_7,
  ICBID_SLAVE_PCNOC_S_8,
  ICBID_SLAVE_PCNOC_S_9,
  ICBID_SLAVE_PRNG_XPU_CFG,
  ICBID_SLAVE_QDSS_INT,
  ICBID_SLAVE_RPM_XPU_CFG,
  ICBID_SLAVE_SNOC_INT_0,
  ICBID_SLAVE_SNOC_INT_1,
  ICBID_SLAVE_SNOC_INT_BIMC,
  ICBID_SLAVE_TCU,
  ICBID_SLAVE_BIMC_INT_0,
  ICBID_SLAVE_BIMC_INT_1,
  ICBID_SLAVE_RICA_CFG,
  ICBID_SLAVE_SNOC_BIMC_2,
  ICBID_SLAVE_BIMC_SNOC_1,
  ICBID_SLAVE_PNOC_A1NOC,
  ICBID_SLAVE_SNOC_VMEM,
  ICBID_SLAVE_A0NOC_SNOC,
  ICBID_SLAVE_A1NOC_SNOC,
  ICBID_SLAVE_A2NOC_SNOC,
  ICBID_SLAVE_A0NOC_CFG,
  ICBID_SLAVE_A0NOC_MPU_CFG,
  ICBID_SLAVE_A0NOC_SMMU_CFG,
  ICBID_SLAVE_A1NOC_CFG,
  ICBID_SLAVE_A1NOC_MPU_CFG,
  ICBID_SLAVE_A1NOC_SMMU_CFG,
  ICBID_SLAVE_A2NOC_CFG,
  ICBID_SLAVE_A2NOC_MPU_CFG,
  ICBID_SLAVE_A2NOC_SMMU_CFG,
  ICBID_SLAVE_AHB2PHY,
  ICBID_SLAVE_CAMERA_THROTTLE_CFG,
  ICBID_SLAVE_DCC_CFG,
  ICBID_SLAVE_DISPLAY_THROTTLE_CFG,
  ICBID_SLAVE_DSA_CFG,
  ICBID_SLAVE_DSA_MPU_CFG,
  ICBID_SLAVE_SSC_MPU_CFG,
  ICBID_SLAVE_HMSS_L3,
  ICBID_SLAVE_LPASS_SMMU_CFG,
  ICBID_SLAVE_MMAGIC_CFG,
  ICBID_SLAVE_PCIE20_AHB2PHY,
  ICBID_SLAVE_PCIE_2,
  ICBID_SLAVE_PCIE_2_CFG,
  ICBID_SLAVE_PIMEM,
  ICBID_SLAVE_PIMEM_CFG,
  ICBID_SLAVE_QDSS_RBCPR_APU_CFG,
  ICBID_SLAVE_RBCPR_CX,
  ICBID_SLAVE_RBCPR_MX,
  ICBID_SLAVE_SMMU_CPP_CFG,
  ICBID_SLAVE_SMMU_JPEG_CFG,
  ICBID_SLAVE_SMMU_MDP_CFG,
  ICBID_SLAVE_SMMU_ROTATOR_CFG,
  ICBID_SLAVE_SMMU_VENUS_CFG,
  ICBID_SLAVE_SMMU_VFE_CFG,
  ICBID_SLAVE_SSC_CFG,
  ICBID_SLAVE_VENUS_THROTTLE_CFG,
  ICBID_SLAVE_VMEM,
  ICBID_SLAVE_VMEM_CFG,
  ICBID_SLAVE_QDSS_MPU_CFG,
  ICBID_SLAVE_USB3_PHY_CFG,
  ICBID_SLAVE_IPA_CFG,
  ICBID_SLAVE_PCNOC_INT_2,
  ICBID_SLAVE_PCNOC_INT_3,
  ICBID_SLAVE_PCNOC_INT_4,
  ICBID_SLAVE_PCNOC_INT_5,
  ICBID_SLAVE_PCNOC_INT_6,
  ICBID_SLAVE_PCNOC_S_5,
  ICBID_SLAVE_QSPI,
  ICBID_SLAVE_A1NOC_MS_MPU_CFG,
  ICBID_SLAVE_A2NOC_MS_MPU_CFG,
  ICBID_SLAVE_MODEM_Q6_SMMU_CFG,
  ICBID_SLAVE_MSS_MPU_CFG,
  ICBID_SLAVE_MSS_PROC_MS_MPU_CFG,
  ICBID_SLAVE_SKL,
  ICBID_SLAVE_SNOC_INT_2,
  ICBID_SLAVE_SMMNOC_BIMC,
  ICBID_SLAVE_CRVIRT_A1NOC,
  ICBID_SLAVE_COUNT,
  ICBID_SLAVE_SIZE = 0x7FFFFFFF
} ICBId_SlaveType;
typedef enum
{
   ICBARB_ERROR_SUCCESS = 0,
   ICBARB_ERROR_UNSUPPORTED,
   ICBARB_ERROR_INVALID_ARG,
   ICBARB_ERROR_INVALID_MASTER,
   ICBARB_ERROR_NO_ROUTE_TO_SLAVE,
   ICBARB_ERROR_VECTOR_LENGTH_MISMATCH,
   ICBARB_ERROR_OUT_OF_MEMORY,
   ICBARB_ERROR_UNKNOWN,
   ICBARB_ERROR_REQUEST_REJECTED,
   ICBARB_ERROR_MAX,
   ICBARB_ERROR_PLACEHOLDER = 0x7FFFFFFF
} ICBArb_ErrorType;
typedef enum
{
   ICBARB_STATE_NORMAL = 0,
   ICBARB_STATE_OVERSUBSCRIBED,
   ICBARB_STATE_MAX,
   ICBARB_STATE_PLACEHOLDER = 0x7FFFFFFF
} ICBArb_StateType;
typedef struct
{
   ICBId_MasterType eMaster;
   ICBId_SlaveType eSlave;
} ICBArb_MasterSlaveType;
typedef struct
{
   npa_callback callback;
   ICBArb_MasterSlaveType *aMasterSlave;
} ICBArb_CreateClientVectorType;
typedef enum
{
   ICBARB_REQUEST_TYPE_1,
   ICBARB_REQUEST_TYPE_2,
   ICBARB_REQUEST_TYPE_3,
   ICBARB_REQUEST_TYPE_1_TIER_3,
   ICBARB_REQUEST_TYPE_2_TIER_3,
   ICBARB_REQUEST_TYPE_3_TIER_3,
   ICBARB_REQUEST_TYPE_1_LAT,
   ICBARB_REQUEST_TYPE_2_LAT,
   ICBARB_REQUEST_TYPE_3_LAT,
   ICBARB_REQUEST_TYPE_MAX,
   ICBARB_REQUEST_TYPE_PLACEHOLDER = 0x7FFFFFFF
} ICBArb_RequestTypeType;
typedef struct
{
   uint64 uDataBurst;
   uint32 uTransferTimeUs;
   uint32 uPeriodUs;
   uint32 uLatencyNs;
} ICBArb_Request1Type;
typedef struct
{
   uint64 uThroughPut;
   uint32 uUsagePercentage;
   uint32 uLatencyNs;
} ICBArb_Request2Type;
typedef struct
{
   uint64 uIb;
   uint64 uAb;
   uint32 uLatencyNs;
} ICBArb_Request3Type;
typedef union
{
   ICBArb_Request1Type type1;
   ICBArb_Request2Type type2;
   ICBArb_Request3Type type3;
} ICBArb_RequestUnionType;
typedef struct
{
   ICBArb_RequestTypeType arbType;
   ICBArb_RequestUnionType arbData;
} ICBArb_RequestType;
typedef enum
{
   ICBARB_QUERY_RESOURCE_COUNT = 0,
   ICBARB_QUERY_RESOURCE_NAMES,
   ICBARB_QUERY_SLAVE_PORT_COUNT,
   ICBARB_QUERY_MAX,
   ICBARB_QUERY_PLACEHOLDER = 0x7FFFFFFF
} ICBArb_QueryTypeType;
typedef struct
{
   npa_client_handle client;
   uint32 uNumResource;
} ICBArb_QueryResourceCountType;
typedef struct
{
   npa_client_handle client;
   const char **aResourceNames;
   uint32 uNumResource;
} ICBArb_QueryResourceNamesType;
typedef struct
{
   ICBId_SlaveType eSlaveId;
   uint32 uNumPorts;
} ICBArb_QuerySlavePortCountType;
typedef union
{
   ICBArb_QueryResourceCountType resourceCount;
   ICBArb_QueryResourceNamesType resourceNames;
   ICBArb_QuerySlavePortCountType slavePortCount;
} ICBArb_QueryUnionType;
typedef struct
{
   ICBArb_QueryTypeType queryType;
   ICBArb_QueryUnionType queryData;
} ICBArb_QueryType;
void icbarb_init( void );
ICBArb_CreateClientVectorType *icbarb_fill_client_vector(
  ICBArb_CreateClientVectorType *psVector,
  ICBArb_MasterSlaveType *aMasterSlave,
  npa_callback callback);
npa_client_handle icbarb_create_client(
   const char *pszClientName,
   ICBArb_MasterSlaveType *aMasterSlave,
   uint32 uNumMasterSlave);
npa_client_handle icbarb_create_client_ex(
   const char *pszClientName,
   ICBArb_MasterSlaveType *aMasterSlave,
   uint32 uNumMasterSlave,
   npa_callback callback);
npa_client_handle icbarb_create_suppressible_client_ex(
   const char *pszClientName,
   ICBArb_MasterSlaveType *aMasterSlave,
   uint32 uNumMasterSlave,
   npa_callback callback);
ICBArb_ErrorType icbarb_issue_request(
   npa_client_handle client,
   ICBArb_RequestType *aRequest,
   uint32 uNumRequest);
void icbarb_complete_request( npa_client_handle client );
void icbarb_destroy_client( npa_client_handle client );
ICBArb_ErrorType icbarb_query( ICBArb_QueryType *pQuery );
typedef enum
{
  DALIPCINT_PROC_NULL = 0,
  DALIPCINT_PROC_MCPU = 1,
  DALIPCINT_PROC_MDSPSW = 2,
  DALIPCINT_PROC_MDSPFW = 3,
  DALIPCINT_PROC_ACPU = 4,
  DALIPCINT_PROC_ACPU1 = 5,
  DALIPCINT_PROC_ADSP = 6,
  DALIPCINT_PROC_SPS = 7,
  DALIPCINT_PROC_RPM = 8,
  DALIPCINT_PROC_RIVA = 9,
  DALIPCINT_PROC_WCN = DALIPCINT_PROC_RIVA,
  DALIPCINT_PROC_GSS = 10,
  DALIPCINT_PROC_LPASS = 11,
  DALIPCINT_PROC_TZ = 12,
  DALIPCINT_NUM_PROCS = 13,
  DALIPCINT_PROC_32BITS = 0x7FFFFFF
} DalIPCIntProcessorType;
typedef enum
{
  DALIPCINT_GP_0 = 0,
  DALIPCINT_GP_1 = 1,
  DALIPCINT_GP_2 = 2,
  DALIPCINT_GP_3 = 3,
  DALIPCINT_GP_4 = 4,
  DALIPCINT_GP_5 = 5,
  DALIPCINT_GP_6 = 6,
  DALIPCINT_GP_7 = 7,
  DALIPCINT_GP_8 = 8,
  DALIPCINT_GP_9 = 9,
  DALIPCINT_GP_LOW = DALIPCINT_GP_0,
  DALIPCINT_GP_MED = DALIPCINT_GP_1,
  DALIPCINT_GP_HIGH = DALIPCINT_GP_2,
  DALIPCINT_SOFTRESET = 10,
  DALIPCINT_WAKEUP = 11,
  DALIPCINT_NUM_INTS = 12,
  DALIPCINT_INT_32BITS = 0x7FFFFFF
} DalIPCIntInterruptType;
typedef struct DalIPCInt DalIPCInt;
struct DalIPCInt
{
  struct DalDevice DalDevice;
  DALResult (*Trigger)(DalDeviceHandle * _h, DalIPCIntProcessorType eTarget, DalIPCIntInterruptType eInterrupt);
  DALResult (*TriggerBySource)(DalDeviceHandle * _h, DalIPCIntProcessorType eSource, DalIPCIntProcessorType eTarget, DalIPCIntInterruptType eInterrupt);
  DALResult (*IsSupported)(DalDeviceHandle * _h, DalIPCIntProcessorType eTarget, DalIPCIntInterruptType eInterrupt);
  DALResult (*IsSupportedBySource)(DalDeviceHandle * _h, DalIPCIntProcessorType eSource, DalIPCIntProcessorType eTarget, DalIPCIntInterruptType eInterrupt);
};
typedef struct DalIPCIntHandle DalIPCIntHandle;
struct DalIPCIntHandle
{
  uint32 dwDalHandleId;
  const DalIPCInt * pVtbl;
  void * pClientCtxt;
};
static __inline DALResult DalIPCInt_Trigger
(
  DalDeviceHandle *_h,
  DalIPCIntProcessorType eTarget,
  DalIPCIntInterruptType eInterrupt
)
{
   return ((DalIPCIntHandle *)_h)->pVtbl->Trigger( _h, eTarget, eInterrupt);
}
static __inline DALResult DalIPCInt_TriggerBySource
(
  DalDeviceHandle *_h,
  DalIPCIntProcessorType eSource,
  DalIPCIntProcessorType eTarget,
  DalIPCIntInterruptType eInterrupt)
{
   return ((DalIPCIntHandle *)_h)->pVtbl->TriggerBySource(
     _h, eSource, eTarget, eInterrupt);
}
static __inline DALResult DalIPCInt_IsSupported
(
  DalDeviceHandle *_h,
  DalIPCIntProcessorType eTarget,
  DalIPCIntInterruptType eInterrupt)
{
   return ((DalIPCIntHandle *)_h)->pVtbl->IsSupported(
     _h, eTarget, eInterrupt);
}
static __inline DALResult DalIPCInt_IsSupportedBySource
(
  DalDeviceHandle *_h,
  DalIPCIntProcessorType eSource,
  DalIPCIntProcessorType eTarget,
  DalIPCIntInterruptType eInterrupt
)
{
   return ((DalIPCIntHandle *)_h)->pVtbl->IsSupportedBySource(
     _h, eSource, eTarget, eInterrupt);
}
void IPCInt_Init(void);
typedef enum {
    MMPM_STATUS_SUCCESS,
    MMPM_STATUS_FAILED,
    MMPM_STATUS_NOMEMORY,
    MMPM_STATUS_VERSIONNOTSUPPORT,
    MMPM_STATUS_BADCLASS,
    MMPM_STATUS_BADSTATE,
    MMPM_STATUS_BADPARM,
    MMPM_STATUS_INVALIDFORMAT,
    MMPM_STATUS_UNSUPPORTED,
    MMPM_STATUS_RESOURCENOTFOUND,
    MMPM_STATUS_BADMEMPTR,
    MMPM_STATUS_BADHANDLE,
    MMPM_STATUS_RESOURCEINUSE,
    MMPM_STATUS_NOBANDWIDTH,
    MMPM_STATUS_NULLPOINTER,
    MMPM_STATUS_NOTINITIALIZED,
    MMPM_STATUS_RESOURCENOTREQUESTED,
    MMPM_STATUS_CORERESOURCENOTAVAILABLE,
    MMPM_STATUS_MAX,
    MMPM_STATUS_FORCE32BITS = 0x7FFFFFFF
} MmpmStatusType;
typedef MmpmStatusType MMPM_STATUS;
typedef enum {
    MMPM_CORE_ID_NONE = 0,
    MMPM_CORE_ID_2D_GRP = 1,
    MMPM_CORE_ID_3D_GRP = 2,
    MMPM_CORE_ID_MDP = 3,
    MMPM_CORE_ID_VCODEC = 4,
    MMPM_CORE_ID_VPE = 5,
    MMPM_CORE_ID_VFE = 6,
    MMPM_CORE_ID_MIPICSI = 7,
    MMPM_CORE_ID_SENSOR = 8,
    MMPM_CORE_ID_JPEGD = 9,
    MMPM_CORE_ID_JPEGE = 10,
    MMPM_CORE_ID_FABRIC = 11,
    MMPM_CORE_ID_IMEM = 12,
    MMPM_CORE_ID_SMMU = 13,
    MMPM_CORE_ID_ROTATOR = 14,
    MMPM_CORE_ID_TV = 15,
    MMPM_CORE_ID_DSI = 16,
    MMPM_CORE_ID_AUDIOIF = 17,
    MMPM_CORE_ID_GMEM = 18,
    MMPM_CORE_ID_LPASS_START = 100,
    MMPM_CORE_ID_LPASS_ADSP = 101,
    MMPM_CORE_ID_LPASS_CORE = 102,
    MMPM_CORE_ID_LPASS_LPM = 103,
    MMPM_CORE_ID_LPASS_DML = 104,
    MMPM_CORE_ID_LPASS_AIF = 105,
    MMPM_CORE_ID_LPASS_SLIMBUS = 106,
    MMPM_CORE_ID_LPASS_MIDI = 107,
    MMPM_CORE_ID_LPASS_AVSYNC = 108,
    MMPM_CORE_ID_LPASS_HWRSMP = 109,
    MMPM_CORE_ID_LPASS_SRAM = 110,
    MMPM_CORE_ID_LPASS_DCODEC = 111,
    MMPM_CORE_ID_LPASS_SPDIF = 112,
    MMPM_CORE_ID_LPASS_HDMIRX = 113,
    MMPM_CORE_ID_LPASS_HDMITX = 114,
    MMPM_CORE_ID_LPASS_SIF = 115,
    MMPM_CORE_ID_LPASS_BSTC = 116,
    MMPM_CORE_ID_LPASS_ADSP_HVX = 117,
    MMPM_CORE_ID_LPASS_END,
    MMPM_CORE_ID_MAX,
    MMPM_CORE_ID_FORCE32BITS = 0x7FFFFFFF
} MmpmCoreIdType;
typedef enum {
    MMPM_CALLBACK_EVENT_ID_NONE,
    MMPM_CALLBACK_EVENT_ID_IDLE,
    MMPM_CALLBACK_EVENT_ID_BUSY,
    MMPM_CALLBACK_EVENT_ID_THERMAL,
    MMPM_CALLBACK_EVENT_ID_COMPLETE,
    MMPM_CALLBACK_EVENT_ID_MAX,
    MMPM_CALLBACK_EVENT_ID_FORCE32BITS = 0x7FFFFFFF
} MmpmCallbackEventIdType;
typedef struct {
    MmpmCallbackEventIdType eventId;
    uint32 clientId;
    uint32 callbackDataSize;
    void* callbackData;
} MmpmCallbackParamType;
typedef struct {
    uint32 reqTag;
    MMPM_STATUS result;
}MmpmCompletionCallbackDataType;
typedef enum{
    MMPM_CORE_INSTANCE_NONE,
    MMPM_CORE_INSTANCE_0,
    MMPM_CORE_INSTANCE_1,
    MMPM_CORE_INSTANCE_2,
    MMPM_CORE_INSTANCE_MAX,
    MMPM_CORE_INSTANCE_FORCE32BITS = 0x7FFFFFFF
} MmpmCoreInstanceIdType;
typedef struct {
    uint32 rev;
    MmpmCoreIdType coreId;
    MmpmCoreInstanceIdType instanceId;
    char *pClientName;
    uint32 pwrCtrlFlag;
    uint32 callBackFlag;
    uint32 (*MMPM_Callback)(MmpmCallbackParamType *pCbParam);
    uint32 cbFcnStackSize;
} MmpmRegParamType;
typedef enum {
    MMPM_RSC_ID_NONE = 0,
    MMPM_RSC_ID_POWER = 1,
    MMPM_RSC_ID_VREG = 2,
    MMPM_RSC_ID_REG_PROG = 3,
    MMPM_RSC_ID_CORE_CLK = 4,
    MMPM_RSC_ID_CORE_CLK_DOMAIN = 5,
    MMPM_RSC_ID_MEM_BW = 6,
    MMPM_RSC_ID_AXI_EN = 7,
    MMPM_RSC_ID_MIPS = 8,
    MMPM_RSC_ID_SLEEP_LATENCY = 9,
    MMPM_RSC_ID_ACTIVE_STATE = 10,
    MMPM_RSC_ID_PMIC_GPIO = 11,
    MMPM_RSC_ID_RESET = 12,
    MMPM_RSC_ID_MIPS_EXT = 13,
    MMPM_RSC_ID_GENERIC_BW = 14,
    MMPM_RSC_ID_THERMAL = 15,
    MMPM_RSC_ID_MEM_POWER = 16,
    MMPM_RSC_ID_GENERIC_BW_EXT = 17,
    MMPM_RSC_ID_MPPS = 18,
    MMPM_RSC_ID_MAX ,
    MMPM_RSC_ID_FORCE32BITS = 0x7FFFFFFF
} MmpmRscIdType;
typedef enum {
    MMPM_REG_PROG_NONE,
    MMPM_REG_PROG_NORM,
    MMPM_REG_PROG_FAST,
    MMPM_REG_PROG_MAX,
    MMPM_REG_PROG_FORCE32BITS = 0x7FFFFFFF
} MmpmRegProgMatchType;
typedef enum {
    MMPM_CLK_ID_2D_GRP_NONE,
    MMPM_CLK_ID_2D_GRP,
    MMPM_CLK_ID_2D_GRP_MAX,
    MMPM_CLK_ID_2D_GRP_FORCE32BITS = 0x7FFFFFFF
} MmpmClkId2dGrpType;
typedef enum {
    MMPM_CLK_ID_3D_GRP_NONE,
    MMPM_CLK_ID_3D_GRP,
    MMPM_CLK_ID_3D_GRP_MAX,
    MMPM_CLK_ID_3D_GRP_FORCE32BITS = 0x7FFFFFFF
} MmpmClkId3dGrpType;
typedef enum {
    MMPM_CLK_ID_MDP_NONE,
    MMPM_CLK_ID_MDP,
    MMPM_CLK_ID_MDP_VSYNC,
    MMPM_CLK_ID_MDP_LUT,
    MMPM_CLK_ID_MDP_MAX,
    MMPM_CLK_ID_MDP_FORCE32BITS = 0x7FFFFFFF
} MmpmClkIdMdpType;
typedef enum {
    MMPM_CLK_ID_VCODEC_NONE,
    MMPM_CLK_ID_VCODEC,
    MMPM_CLK_ID_VCODEC_MAX,
    MMPM_CLK_ID_VCODEC_FORCE32BITS = 0x7FFFFFFF
} MmpmClkIdVcodecType;
typedef enum {
    MMPM_CLK_ID_VPE_NONE,
    MMPM_CLK_ID_VPE,
    MMPM_CLK_ID_VPE_MAX,
    MMPM_CLK_ID_VPE_FORCE32BITS = 0x7FFFFFFF
} MmpmClkIdVpeType;
typedef enum {
    MMPM_CLK_ID_VFE_NONE,
    MMPM_CLK_ID_VFE,
    MMPM_CLK_ID_VFE_MAX,
    MMPM_CLK_ID_VFE_FORCE32BITS = 0x7FFFFFFF
} MmpmClkIdVfeType;
typedef enum {
    MMPM_CLK_ID_CSI_NONE,
    MMPM_CLK_ID_CSI,
    MMPM_CLK_ID_CSI_VFE,
    MMPM_CLK_ID_CSI_PHY,
    MMPM_CLK_ID_CSI_PHY_TIMER,
    MMPM_CLK_ID_CSI_PIX0,
    MMPM_CLK_ID_CSI_PIX1,
    MMPM_CLK_ID_CSI_RDI0,
    MMPM_CLK_ID_CSI_RDI1,
    MMPM_CLK_ID_CSI_RDI2,
    MMPM_CLK_ID_CSI_MAX,
    MMPM_CLK_ID_CSI_FORCE32BITS = 0x7FFFFFFF
} MmpmClkIdCsiType;
typedef enum {
    MMPM_CLK_ID_SENSOR_NONE,
    MMPM_CLK_ID_SENSOR,
    MMPM_CLK_ID_SENSOR_MAX,
    MMPM_CLK_ID_SENSOR_FORCE32BITS = 0x7FFFFFFF
} MmpmClkIdSensorType;
typedef enum {
    MMPM_CLK_ID_JPEGD_NONE,
    MMPM_CLK_ID_JPEGD,
    MMPM_CLK_ID_JPEGD_MAX,
    MMPM_CLK_ID_JPEGD_FORCE32BITS = 0x7FFFFFFF
} MmpmClkIdJpegdType;
typedef enum {
    MMPM_CLK_ID_JPEGE_NONE,
    MMPM_CLK_ID_JPEGE,
    MMPM_CLK_ID_JPEGE_MAX,
    MMPM_CLK_ID_JPEGE_FORCE32BITS = 0x7FFFFFFF
} MmpmClkIdJpegeType;
typedef enum {
    MMPM_CLK_ID_ROTATOR_NONE,
    MMPM_CLK_ID_ROTATOR,
    MMPM_CLK_ID_ROTATOR_MAX,
    MMPM_CLK_ID_ROTATOR_FORCE32BITS = 0x7FFFFFFF
} MmpmClkIdRotatorType;
typedef enum {
    MMPM_CLK_ID_TV_NONE,
    MMPM_CLK_ID_TV_ENC,
    MMPM_CLK_ID_TV_DAC,
    MMPM_CLK_ID_TV_MDP,
    MMPM_CLK_ID_TV_HDMI_APP,
    MMPM_CLK_ID_TV_HDMI,
    MMPM_CLK_ID_TV_RGB,
    MMPM_CLK_ID_TV_NPL,
    MMPM_CLK_ID_TV_MAX,
    MMPM_CLK_ID_TV_FORCE32BITS = 0x7FFFFFFF
} MmpmClkIdTvType;
typedef enum {
    MMPM_CLK_ID_DSI_NONE,
    MMPM_CLK_ID_DSI,
    MMPM_CLK_ID_DSI_ESC,
    MMPM_CLK_ID_DSI_PIX,
    MMPM_CLK_ID_DSI_BYTE,
    MMPM_CLK_ID_DSI_LVDS,
    MMPM_CLK_ID_DSI_MDP_P2,
    MMPM_CLK_ID_DSI_MAX,
    MMPM_CLK_ID_DSI_FORCE32BITS = 0x7FFFFFFF
} MmpmClkIdDsiType;
typedef enum {
    MMPM_CLK_ID_AUDIOIF_NONE,
    MMPM_CLK_ID_AUDIOIF_PCM,
    MMPM_CLK_ID_AUDIOIF_I2S_CODEC_SPKR_MCLK,
    MMPM_CLK_ID_AUDIOIF_I2S_CODEC_SPKR_SCLK,
    MMPM_CLK_ID_AUDIOIF_I2S_SPARE_SPKR_MCLK,
    MMPM_CLK_ID_AUDIOIF_I2S_SPARE_SPKR_SCLK,
    MMPM_CLK_ID_AUDIOIF_I2S_CODEC_MIC_MCLK,
    MMPM_CLK_ID_AUDIOIF_I2S_CODEC_MIC_SCLK,
    MMPM_CLK_ID_AUDIOIF_I2S_SPARE_MIC_MCLK,
    MMPM_CLK_ID_AUDIOIF_I2S_SPARE_MIC_SCLK,
    MMPM_CLK_ID_AUDIOIF_MI2S_CODEC_MCLK,
    MMPM_CLK_ID_AUDIOIF_MI2S_CODEC_SCLK,
    MMPM_CLK_ID_AUDIOIF_MI2S_CODEC_TX_MCLK,
    MMPM_CLK_ID_AUDIOIF_MI2S_CODEC_TX_SCLK,
    MMPM_CLK_ID_AUDIOIF_LPASS_SB_REF_CLK,
    MMPM_CLK_ID_AUDIOIF_SPS_SB_REF_CLK,
    MMPM_CLK_ID_AUDIOIF_MAX,
    MMPM_CLK_ID_AUDIOIF_FORCE32BITS = 0x7FFFFFFF
} MmpmClkIdAudioIfType;
typedef enum {
    MMPM_CLK_ID_LPASS_NONE = 0,
    MMPM_CLK_ID_LPASS_HWRSP_CORE,
    MMPM_CLK_ID_LPASS_MIDI_CORE,
    MMPM_CLK_ID_LPASS_AVSYNC_XO,
    MMPM_CLK_ID_LPASS_AVSYNC_BT,
    MMPM_CLK_ID_LPASS_AVSYNC_FM,
    MMPM_CLK_ID_LPASS_SLIMBUS_CORE,
    MMPM_CLK_ID_LPASS_AVTIMER_CORE,
    MMPM_CLK_ID_LPASS_ATIME_CORE,
    MMPM_CLK_ID_LPASS_ATIME2_CORE,
    MMPM_CLK_ID_LPASS_ADSP_CORE,
    MMPM_CLK_ID_LPASS_AHB_ROOT,
    MMPM_CLK_ID_LPASS_ENUM_MAX,
    MMPM_CLK_ID_LPASS_FORCE32BITS = 0x7fffffff
} MmpmClkIdLpassType;
typedef enum {
    MMPM_CLK_ID_VCAP_NONE,
    MMPM_CLK_ID_VCAP,
    MMPM_CLK_ID_VCAP_NPL,
    MMPM_CLK_ID_VCAP_MAX,
    MMPM_CLK_ID_VCAP_FORCE32BITS = 0x7FFFFFFF
} MmpmClkIdVcapType;
typedef union {
    MmpmClkId2dGrpType clkId2dGrp;
    MmpmClkId3dGrpType clkId3dGrp;
    MmpmClkIdMdpType clkIdMdp;
    MmpmClkIdVcodecType clkIdVcodec;
    MmpmClkIdVpeType clkIdVpe;
    MmpmClkIdVfeType clkIdVfe;
    MmpmClkIdCsiType clkIdCsi;
    MmpmClkIdSensorType clkIdSensor;
    MmpmClkIdJpegdType clkIdJpegd;
    MmpmClkIdJpegeType clkIdJpege;
    MmpmClkIdRotatorType clkIdRotator;
    MmpmClkIdTvType clkIdTv;
    MmpmClkIdDsiType clkIdDsi;
    MmpmClkIdAudioIfType clkIdAudioIf;
    MmpmClkIdVcapType clkIdVcap;
    MmpmClkIdLpassType clkIdLpass;
} MmpmCoreClkIdType;
typedef enum {
    MMPM_FREQ_AT_LEAST,
    MMPM_FREQ_AT_MOST,
    MMPM_FREQ_CLOSEST,
    MMPM_FREQ_EXACT,
    MMPM_FREQ_MAX,
    MMPM_FREQ_FORCE32BITS = 0x7FFFFFFF
} MmpmFreqMatchType;
typedef struct {
    MmpmCoreClkIdType clkId;
    uint32 clkFreqHz;
    MmpmFreqMatchType freqMatch;
} MmpmClkValType;
typedef struct {
    uint32 numOfClk;
    MmpmClkValType *pClkArray;
} MmpmClkReqType;
typedef enum {
    MMPM_CLK_DOMAIN_SRC_ID_HDMI_NONE,
    MMPM_CLK_DOMAIN_SRC_ID_HDMI0,
    MMPM_CLK_DOMAIN_SRC_ID_HDMI_MAX,
    MMPM_CLK_DOMAIN_SRC_ID_HDMI_FORCE32BITS = 0x7FFFFFFF
} MmpmClkDomainSrcIdHdmiType;
typedef enum {
    MMPM_CLK_DOMAIN_SRC_ID_DSI_NONE,
    MMPM_CLK_DOMAIN_SRC_ID_DSI0,
    MMPM_CLK_DOMAIN_SRC_ID_DSI1,
    MMPM_CLK_DOMAIN_SRC_ID_LVDS,
    MMPM_CLK_DOMAIN_SRC_ID_DSI_MAX,
    MMPM_CLK_DOMAIN_SRC_ID_DSI_FORCE32BITS = 0x7FFFFFFF
} MmpmClkDomainSrcIdDsiType;
typedef enum {
    MMPM_CLK_DOMAIN_SRC_ID_LPASS_NONE,
    MMPM_CLK_DOMAIN_SRC_ID_Q6PLL,
    MMPM_CLK_DOMAIN_SRC_ID_AUDIOPLL,
    MMPM_CLK_DOMAIN_SRC_ID_PRIUSPLL,
    MMPM_CLK_DOMAIN_SRC_ID_LPASS_MAX,
    MMPM_CLK_DOMAIN_SRC_ID_LPASS_FORCE32BITS = 0x7FFFFFFF
} MmpmClkDomainSrcIdLpassType;
typedef union {
    MmpmClkDomainSrcIdHdmiType clkDomainSrcIdHdmi;
    MmpmClkDomainSrcIdDsiType clkDomainSrcIdDsi;
    MmpmClkDomainSrcIdLpassType clkDomainSrcIdLpass;
} MmpmClkDomainSrcIdType;
typedef struct {
    MmpmCoreClkIdType clkId;
    uint32 M_val;
    uint32 N_val;
    uint32 n2D_val;
    uint32 div_val;
    uint32 clkFreqHz;
    MmpmClkDomainSrcIdType clkDomainSrc;
} MmpmClkDomainType;
typedef struct {
    uint32 numOfClk;
    MmpmClkDomainType *pClkDomainArray;
} MmpmClkDomainReqType;
typedef enum {
    MMPM_THERMAL_NONE,
    MMPM_THERMAL_LOW,
    MMPM_THERMAL_NORM,
    MMPM_THERMAL_HIGH_L1,
    MMPM_THERMAL_HIGH_L2,
    MMPM_THERMAL_HIGH_L3,
    MMPM_THERMAL_HIGH_L4,
    MMPM_THERMAL_HIGH_L5,
    MMPM_THERMAL_HIGH_L6,
    MMPM_THERMAL_HIGH_L7,
    MMPM_THERMAL_HIGH_L8,
    MMPM_THERMAL_HIGH_L9,
    MMPM_THERMAL_HIGH_L10,
    MMPM_THERMAL_HIGH_L11,
    MMPM_THERMAL_HIGH_L12,
    MMPM_THERMAL_HIGH_L13,
    MMPM_THERMAL_HIGH_L14,
    MMPM_THERMAL_HIGH_L15,
    MMPM_THERMAL_HIGH_L16,
    MMPM_THERMAL_MAX,
    MMPM_THERMAL_FORCE32BITS = 0x7FFFFFFF
} MmpmThermalType;
typedef enum {
    MMPM_BW_USAGE_2D_GRP_NONE,
    MMPM_BW_USAGE_2D_GRP,
    MMPM_BW_USAGE_2D_GRP_MAX,
    MMPM_BW_USAGE_2D_GRP_FORCE32BITS = 0x7FFFFFFF
} MmpmBwUsage2dGrpType;
typedef enum {
    MMPM_BW_USAGE_3D_GRP_NONE,
    MMPM_BW_USAGE_3D_GRP,
    MMPM_BW_USAGE_3D_GRP_MAX,
    MMPM_BW_USAGE_3D_GRP_FORCE32BITS = 0x7FFFFFFF
} MmpmBwUsage3dGrpType;
typedef enum {
    MMPM_BW_USAGE_MDP_NONE,
    MMPM_BW_USAGE_MDP,
    MMPM_BW_USAGE_MDP_HRES,
    MMPM_BW_USAGE_MDP_MAX,
    MMPM_BW_USAGE_MDP_FORCE32BITS = 0x7FFFFFFF
} MmpmBwUsageMdpType;
typedef enum {
    MMPM_BW_USAGE_VCODEC_NONE,
    MMPM_BW_USAGE_VCODEC_ENC,
    MMPM_BW_USAGE_VCODEC_DEC,
    MMPM_BW_USAGE_VCODEC_ENC_DEC,
    MMPM_BW_USAGE_VCODEC_MAX,
    MMPM_BW_USAGE_VCODEC_FORCE32BITS = 0x7FFFFFFF
} MmpmBwUsageVcodecType;
typedef enum {
    MMPM_BW_USAGE_VPE_NONE,
    MMPM_BW_USAGE_VPE,
    MMPM_BW_USAGE_VPE_MAX,
    MMPM_BW_USAGE_VPE_FORCE32BITS = 0x7FFFFFFF
} MmpmBwUsageVpeType;
typedef enum {
    MMPM_BW_USAGE_VFE_NONE,
    MMPM_BW_USAGE_VFE,
    MMPM_BW_USAGE_VFE_MAX,
    MMPM_BW_USAGE_VFE_FORCE32BITS = 0x7FFFFFFF
} MmpmBwUsageVfeType;
typedef enum {
    MMPM_BW_USAGE_JPEGD_NONE,
    MMPM_BW_USAGE_JPEGD,
    MMPM_BW_USAGE_JPEGD_MAX,
    MMPM_BW_USAGE_JPEGD_FORCE32BITS = 0x7FFFFFFF
} MmpmBwUsageJpegdType;
typedef enum {
    MMPM_BW_USAGE_JPEGE_NONE,
    MMPM_BW_USAGE_JPEGE,
    MMPM_BW_USAGE_JPEGE_MAX,
    MMPM_BW_USAGE_JPEGE_FORCE32BITS = 0x7FFFFFFF
} MmpmBwUsageJpegeType;
typedef enum {
    MMPM_BW_USAGE_ROTATOR_NONE,
    MMPM_BW_USAGE_ROTATOR,
    MMPM_BW_USAGE_ROTATOR_MAX,
    MMPM_BW_USAGE_ROTATOR_FORCE32BITS = 0x7FFFFFFF
} MmpmBwUsageRotatorType;
typedef enum {
    MMPM_BW_USAGE_LPASS_NONE,
    MMPM_BW_USAGE_LPASS_DSP,
    MMPM_BW_USAGE_LPASS_DMA,
    MMPM_BW_USAGE_LPASS_EXT_CPU,
    MMPM_BW_USAGE_LPASS_ENUM_MAX,
    MMPM_BW_USAGE_LPASS_FORCE32BITS = 0x7FFFFFFF
} MmpmBwUsageLpassType;
typedef union {
    MmpmBwUsage2dGrpType bwUsage2dGrp;
    MmpmBwUsage3dGrpType bwUsage3dGrp;
    MmpmBwUsageMdpType bwUsageMdp;
    MmpmBwUsageVcodecType bwUsageVcodec;
    MmpmBwUsageVpeType bwUsageVpe;
    MmpmBwUsageVfeType bwUsageVfe;
    MmpmBwUsageJpegdType bwUsageJpegd;
    MmpmBwUsageJpegeType bwUsageJpege;
    MmpmBwUsageRotatorType bwUsageRotator;
    MmpmBwUsageLpassType bwUsageLpass;
} MmpmCoreBwUsageType;
typedef enum {
    MMPM_BW_PORT_ID_NONE = 0,
    MMPM_BW_PORT_ID_ADSP_MASTER,
    MMPM_BW_PORT_ID_DML_MASTER,
    MMPM_BW_PORT_ID_AIF_MASTER,
    MMPM_BW_PORT_ID_SLIMBUS_MASTER,
    MMPM_BW_PORT_ID_MIDI_MASTER,
    MMPM_BW_PORT_ID_HWRSMP_MASTER,
    MMPM_BW_PORT_ID_EXT_AHB_MASTER,
    MMPM_BW_PORT_ID_SPDIF_MASTER,
    MMPM_BW_PORT_ID_HDMIRX_MASTER,
    MMPM_BW_PORT_ID_HDMITX_MASTER,
    MMPM_BW_PORT_ID_SIF_MASTER,
    MMPM_BW_PORT_ID_DML_SLAVE,
    MMPM_BW_PORT_ID_AIF_SLAVE,
    MMPM_BW_PORT_ID_SLIMBUS_SLAVE,
    MMPM_BW_PORT_ID_MIDI_SLAVE,
    MMPM_BW_PORT_ID_HWRSMP_SLAVE,
    MMPM_BW_PORT_ID_AVSYNC_SLAVE,
    MMPM_BW_PORT_ID_LPM_SLAVE,
    MMPM_BW_PORT_ID_SRAM_SLAVE,
    MMPM_BW_PORT_ID_EXT_AHB_SLAVE,
    MMPM_BW_PORT_ID_DDR_SLAVE,
    MMPM_BW_PORT_ID_OCMEM_SLAVE,
    MMPM_BW_PORT_ID_PERIFNOC_SLAVE,
    MMPM_BW_PORT_ID_SPDIF_SLAVE,
    MMPM_BW_PORT_ID_HDMIRX_SLAVE,
    MMPM_BW_PORT_ID_HDMITX_SLAVE,
    MMPM_BW_PORT_ID_SIF_SLAVE,
    MMPM_BW_PORT_ID_BSTC_SLAVE,
    MMPM_BW_PORT_ID_DCODEC_SLAVE,
    MMPM_BW_PORT_ID_CORE,
    MMPM_BW_PORT_ID_MAX,
    MMPM_BW_PORT_ID_FORCE32BITS = 0x7FFFFFFF
} MmpmBwPortIdType;
typedef struct {
    uint32 memPhyAddr;
    uint32 axiPort;
    uint32 bwBytePerSec;
    uint32 usagePercentage;
    MmpmCoreBwUsageType bwUsageType;
} MmpmBwValType;
typedef struct {
    uint32 numOfBw;
    MmpmBwValType *pBandWidthArray;
} MmpmBwReqType;
typedef struct{
    MmpmBwPortIdType masterPort;
    MmpmBwPortIdType slavePort;
} MmpmBusRouteType;
typedef struct{
    uint64 Ab;
    uint64 Ib;
} MmpmBusBWDataIbAbType;
typedef struct{
    uint64 bwBytePerSec;
    uint32 usagePercentage;
    MmpmBwUsageLpassType usageType;
} MmpmBusBWDataUsageType;
typedef union{
    MmpmBusBWDataIbAbType busBwAbIb;
    MmpmBusBWDataUsageType busBwValue;
} MmpmBusBWDataType;
typedef struct{
    MmpmBusRouteType busRoute;
    MmpmBusBWDataType bwValue;
} MmpmGenBwValType;
typedef struct {
    uint32 numOfBw;
    MmpmGenBwValType *pBandWidthArray;
} MmpmGenBwReqType;
typedef enum {
    MMPM_MEM_POWER_NONE,
    MMPM_MEM_POWER_OFF,
    MMPM_MEM_POWER_RETENTION,
    MMPM_MEM_POWER_ACTIVE,
    MMPM_MEM_POWER_MAX,
    MMPM_MEM_POWER_FORCE32BITS = 0x7FFFFFFF
} MmpmMemPowerStateType;
typedef enum{
    MMPM_MEM_NONE,
    MMPM_MEM_OCMEM,
    MMPM_MEM_LPASS_LPM,
    MMPM_MEM_SRAM,
    MMPM_MEM_MAX,
    MMPM_MEM_FORCE32BITS = 0x7FFFFFFF
} MmpmMemIdType;
typedef struct{
    MmpmMemIdType memory;
    MmpmMemPowerStateType powerState;
}MmpmMemPowerReqParamType;
typedef struct {
    uint32 gpioId;
    boolean configGpio;
    uint32 gpioVoltageSource;
    boolean gpioModeOnOff;
    uint32 gpioModeSelect;
    uint32 gpioOutBufferConfig;
    boolean invertExtPin;
    uint32 gpioCurrentSourcePulls;
    uint32 gpioOutBufferDriveStrength;
    uint32 gpioDtestBufferOnOff;
    uint32 gpioExtPinConfig;
    uint32 gpioSourceConfig;
    boolean interrupPolarity;
    uint32 uartPath;
} MmpmPmicGpioParamType;
typedef enum {
     MMPM_PMIC_CONFIG,
     MMPM_PMIC_CONFIG_BIAS_VOLTAGE,
     MMPM_PMIC_CONFIG_DIGITAL_INPUT,
     MMPM_PMIC_CONFIG_DIGITAL_OUTPUT,
     MMPM_PMIC_CONFIG_SET_VOLTAGE_SOURCE,
     MMPM_PMIC_CONFIG_MODE_SELECTION,
     MMPM_PMIC_CONFIG_SET_OUTPUT_BUFFER,
     MMPM_PMIC_CONFIG_SET_INVERSION,
     MMPM_PMIC_CONFIG_SET_CURRENT_SOURCE_PULLS,
     MMPM_PMIC_CONFIG_SET_EXT_PIN,
     MMPM_PMIC_CONFIG_SET_OUTPUT_BUF_DRIVE_STRG,
     MMPM_PMIC_CONFIG_SET_SOURCE,
     MMPM_PMIC_CONFIG_SET_INT_POLARITY,
     MMPM_PMIC_CONFIG_SET_MUX_CTRL,
} MmpmPmicGpioConfigType;
typedef struct {
    MmpmPmicGpioConfigType configId;
    MmpmPmicGpioParamType gpioParam;
} MmpmPmicGpioReqType;
typedef enum {
     MMPM_RESET_NONE,
     MMPM_RESET_DEASSERT,
     MMPM_RESET_ASSERT,
     MMPM_RESET_PULSE,
     MMPM_RESET_ENUM_MAX,
     MMPM_RESET_ENUM_FORCE32BITS = 0x7FFFFFFF
} MmpmResetType;
typedef struct {
    MmpmCoreClkIdType clkId;
    MmpmResetType resetType;
} MmpmResetParamType;
typedef struct {
    uint32 numOfReset;
    MmpmResetParamType *pResetParamArray;
} MmpmResetReqType;
typedef enum {
    MMPM_MIPS_REQUEST_NONE = 0,
    MMPM_MIPS_REQUEST_CPU_CLOCK_ONLY,
    MMPM_MIPS_REQUEST_CPU_CLOCK_AND_BW,
    MMPM_MIPS_REQUEST_ENUM_MAX,
    MMPM_MIPS_REQUEST_FORCE32BITS = 0x7FFFFFFF
} MmpmMipsRequestFnType;
typedef struct {
    uint32 mipsTotal;
    uint32 mipsPerThread;
    MmpmBwPortIdType codeLocation;
    MmpmMipsRequestFnType reqOperation;
} MmpmMipsReqType;
typedef struct {
    uint32 mppsTotal;
    uint32 adspFloorClock;
} MmpmMppsReqType;
typedef struct {
    uint32 numOfClk;
    MmpmCoreClkIdType *pClkIdArray;
} MmpmClkIdArrayParamType;
typedef union {
    uint32 vregMilliVolt;
    MmpmRegProgMatchType regProgMatch;
    MmpmPmicGpioReqType *pPmicGpio;
    MmpmClkReqType *pCoreClk;
    MmpmClkDomainReqType *pCoreClkDomain;
    MmpmBwReqType *pBwReq;
    MmpmGenBwReqType *pGenBwReq;
    uint32 sleepMicroSec;
    uint32 mips;
    MmpmResetReqType *pResetReq;
    MmpmMipsReqType *pMipsExt;
    MmpmMppsReqType *pMppsReq;
    MmpmThermalType thermalMitigation;
    MmpmMemPowerReqParamType *pMemPowerState;
    MmpmClkIdArrayParamType *pRelClkIdArray;
    MmpmCoreClkIdType gateClkId;
}MmpmRscParamStructType;
typedef struct {
    MmpmRscIdType rscId;
    MmpmRscParamStructType rscParam;
} MmpmRscParamType;
typedef enum{
     MMPM_API_TYPE_NONE,
     MMPM_API_TYPE_SYNC,
     MMPM_API_TYPE_ASYNC,
     MMPM_API_TYPE_ENUM_MAX,
     MMPM_API_TYPE_FORCE32BITS = 0x7FFFFFFF
} MmpmApiType;
typedef struct {
    MmpmApiType apiType;
    uint32 numOfReq;
    MmpmRscParamType *pReqArray;
    MMPM_STATUS *pStsArray;
    uint32 reqTag;
    void *pExt;
} MmpmRscExtParamType;
typedef enum {
    MMPM_INFO_ID_NONE,
    MMPM_INFO_ID_CORE_CLK,
    MMPM_INFO_ID_CORE_CLK_MAX,
    MMPM_INFO_ID_CORE_CLK_MAX_SVS,
    MMPM_INFO_ID_CORE_CLK_MAX_NOMINAL,
    MMPM_INFO_ID_CORE_CLK_MAX_TURBO,
    MMPM_INFO_ID_MIPS_MAX,
    MMPM_INFO_ID_BW_MAX,
    MMPM_INFO_ID_CRASH_DUMP,
    MMPM_INFO_ID_POWER_SUPPORT,
    MMPM_INFO_ID_CLK_FREQ,
    MMPM_INFO_ID_PERFMON,
    MMPM_INFO_ID_PMIC_GPIO,
    MMPM_INFO_ID_SET_DEBUG_LEVEL,
    MMPM_INFO_ID_MMSS_BUS,
    MMPM_INFO_ID_LPASS_BUS,
    MMPM_INFO_ID_AGGREGATE_CLIENT_CLASS,
    MMPM_INFO_ID_MPPS,
    MMPM_INFO_ID_BW_EXT,
    MMPM_INFO_ID_DCVS_STATE,
    MMPM_INFO_ID_EXT_REQ,
    MMPM_INFO_ID_MAX,
    MMPM_INFO_ID_FORCE32BITS = 0x7FFFFFFF
} MmpmInfoIdType;
typedef struct {
    MmpmCoreClkIdType clkId;
    uint32 clkFreqHz;
} MmpmInfoClkType;
typedef struct {
    uint32 clkId;
    uint32 clkFreqHz;
    uint32 forceMeasure;
} MmpmInfoClkFreqType;
typedef struct
{
    uint32 clientClasses;
    uint32 aggregateMpps;
} MmpmInfoMppsType;
typedef struct
{
    MmpmBusRouteType busRoute;
    uint64 totalBw;
} MmpmInfoBwExtType;
typedef struct {
    uint32 masterMeasureArray[7];
    uint32 measurementConfig[7];
    uint32 latencyMasterPort;
    uint32 holdoffTime;
    uint32 triggerMode;
    void *pDataMsg;
    char pFilename[64];
    uint32 axiClockFreq;
    uint32 clockInfo[10];
    uint32 isClockFreqCalc[10];
} MmpmInfoPerfmonType;
typedef struct {
    uint32 gpioModeSelect;
    uint32 gpioVoltageSource;
    uint32 gpioModeOnOff;
    uint32 gpioOutBufferConfig;
    uint32 gpioOutBufferDriveStrength;
    uint32 gpioCurrentSourcePulls;
    uint32 gpioSourceConfig;
    uint32 gpioDtestBufferOnOff;
    uint32 gpioExtPinConfig;
} MmpmPmicGpioStatusType;
typedef struct {
    uint32 gpioId;
    MmpmPmicGpioStatusType gpioSts;
} MmpmPmicGpioInfoType;
typedef struct {
    uint32 clientId;
    char clientName[32];
    char fabClientName[32];
    uint32 uIbBytePerSec;
    uint32 uAbBytePerSec;
} MmpmBwClientType;
typedef struct {
    uint32 coreId;
    uint32 instanceId;
    uint32 axiPort;
    uint32 slaveId;
    uint32 uIbBytePerSec;
    uint32 uAbBytePerSec;
    uint32 numOfClients;
    MmpmBwClientType client[16];
} MmpmMasterSlaveType;
typedef struct {
    uint32 mmFabClkFreq;
    uint32 appsFabClkFreq;
    uint32 sysFabClkFreq;
    uint32 lpassFabClkFreq;
    uint32 ebiClkFreq;
    uint32 mmImemClkFreq;
    uint32 numOfMasterSlave;
    MmpmMasterSlaveType masterSlave[16];
} MmpmFabStatusInfoType;
typedef union {
    MmpmInfoClkType *pInfoClk;
    boolean bInfoPower;
    MmpmInfoPerfmonType *pInfoPerfmon;
    MmpmInfoClkFreqType *pInfoClkFreqType;
    MmpmPmicGpioInfoType *pPmicGpioStatus;
    uint32 infoSetLotLevel;
    MmpmFabStatusInfoType *pFabStatus;
    uint32 mipsValue;
    uint64 bwValue;
    uint32 aggregateClientClass;
    MmpmInfoMppsType *pMppsInfo;
    MmpmInfoBwExtType *pBwExtInfo;
    boolean dcvsState;
    void *pExtInfo;
} MmpmInfoDataStructType;
typedef struct {
    MmpmInfoIdType infoId;
    MmpmCoreIdType coreId;
    MmpmCoreInstanceIdType instanceId;
    MmpmInfoDataStructType info;
} MmpmInfoDataType;
typedef enum{
    MMPM_PARAM_ID_NONE = 0,
    MMPM_PARAM_ID_RESOURCE_LIMIT,
    MMPM_PARAM_ID_CLIENT_OCMEM_MAP,
    MMPM_PARAM_ID_MEMORY_MAP,
    MMPM_PARAM_ID_CLIENT_CLASS,
    MMPM_PARAM_ID_L2_CACHE_LINE_LOCK,
    MMPM_PARAM_ID_DCVS_PARTICIPATION,
    MMPM_PARAM_ID_ENUM_MAX,
    MMPM_PARAM_ID_Force32bit = 0x7fffffff
} MmpmParameterIdType;
typedef struct
{
    MmpmMemIdType memory;
    uint64 startAddress;
    uint32 size;
} MmpmMemoryMapType;
typedef struct
{
    void* startAddress;
    uint32 size;
    uint32 throttleBlockSize;
    uint32 throttlePauseUs;
} MmpmL2CacheLineLockParameterType;
typedef enum
{
    MMPM_UNKNOWN_CLIENT_CLASS = 0x00,
    MMPM_AUDIO_CLIENT_CLASS = 0x01,
    MMPM_VOICE_CLIENT_CLASS = 0x02,
    MMPM_COMPUTE_CLIENT_CLASS = 0x04,
    MMPM_STREAMING_1HVX_CLIENT_CLASS = 0x08,
    MMPM_STREAMING_2HVX_CLIENT_CLASS = 0x10,
} MmpmClientClassType;
typedef enum
{
    MMPM_DCVS_ADJUST_UP_DOWN,
    MMPM_DCVS_ADJUST_ONLY_UP
} MmpmDcvsEnableOptionsType;
typedef struct
{
    boolean enable;
    MmpmDcvsEnableOptionsType enableOpt;
} MmpmDcvsParticipationType;
typedef struct{
    MmpmParameterIdType paramId;
    void* pParamConfig;
} MmpmParameterConfigType;
typedef struct{
    uint32 regionId;
    uint32 numKeys;
    uint32 *pKey;
} MmpmOcmemMapRegionDescType;
typedef struct{
    uint32 numRegions;
    MmpmOcmemMapRegionDescType *pRegions;
} MmpmOcmemMapType;
uint32 MMPM_Init(char * cmd_line);
uint32 MMPM_Register_Ext(MmpmRegParamType *pRegParam);
MMPM_STATUS MMPM_Deregister_Ext(uint32 clientId);
MMPM_STATUS MMPM_Request(uint32 clientId,
                             MmpmRscParamType *pMmpmRscParam);
MMPM_STATUS MMPM_Release(uint32 clientId,
                             MmpmRscParamType *pMmpmRscParam);
MMPM_STATUS MMPM_Pause(uint32 clientId,
                           MmpmRscParamType *pMmpmRscParam);
MMPM_STATUS MMPM_Resume(uint32 clientId,
                            MmpmRscParamType *pMmpmRscParam);
MMPM_STATUS MMPM_Request_Ext(uint32 clientId,
                                 MmpmRscExtParamType *pRscExtParam);
MMPM_STATUS MMPM_Release_Ext(uint32 clientId,
                                 MmpmRscExtParamType *pRscExtParam);
MMPM_STATUS MMPM_Pause_Ext(uint32 clientId,
                               MmpmRscExtParamType *pRscExtParam);
MMPM_STATUS MMPM_Resume_Ext(uint32 clientId,
                                MmpmRscExtParamType *pRscExtParam);
MMPM_STATUS MMPM_GetInfo(MmpmInfoDataType *pInfoData);
MMPM_STATUS MMPM_SetParameter(uint32 clientId, MmpmParameterConfigType *pParamConfig);
typedef unsigned int qurt_addr_t;
typedef unsigned int qurt_paddr_t;
typedef unsigned long long qurt_paddr_64_t;
typedef unsigned int qurt_mem_region_t;
typedef unsigned int qurt_mem_fs_region_t;
typedef unsigned int qurt_mem_pool_t;
typedef unsigned int qurt_size_t;
typedef enum {
        QURT_MEM_MAPPING_VIRTUAL=0,
        QURT_MEM_MAPPING_PHYS_CONTIGUOUS = 1,
        QURT_MEM_MAPPING_IDEMPOTENT=2,
        QURT_MEM_MAPPING_VIRTUAL_FIXED=3,
        QURT_MEM_MAPPING_NONE=4,
        QURT_MEM_MAPPING_VIRTUAL_RANDOM=7,
        QURT_MEM_MAPPING_INVALID=10,
} qurt_mem_mapping_t;
typedef enum {
        QURT_MEM_CACHE_WRITEBACK=7,
        QURT_MEM_CACHE_NONE_SHARED=6,
        QURT_MEM_CACHE_WRITETHROUGH=5,
        QURT_MEM_CACHE_WRITEBACK_NONL2CACHEABLE=0,
        QURT_MEM_CACHE_WRITETHROUGH_NONL2CACHEABLE=1,
        QURT_MEM_CACHE_WRITEBACK_L2CACHEABLE=QURT_MEM_CACHE_WRITEBACK,
        QURT_MEM_CACHE_WRITETHROUGH_L2CACHEABLE=QURT_MEM_CACHE_WRITETHROUGH,
        QURT_MEM_CACHE_DEVICE = 4,
        QURT_MEM_CACHE_NONE = 4,
        QURT_MEM_CACHE_INVALID=10,
} qurt_mem_cache_mode_t;
typedef enum {
        QURT_PERM_READ=0x1,
        QURT_PERM_WRITE=0x2,
        QURT_PERM_EXECUTE=0x4,
        QURT_PERM_FULL=QURT_PERM_READ|QURT_PERM_WRITE|QURT_PERM_EXECUTE,
} qurt_perm_t;
typedef enum {
        QURT_MEM_ICACHE,
        QURT_MEM_DCACHE
} qurt_mem_cache_type_t;
typedef enum {
    QURT_MEM_CACHE_FLUSH,
    QURT_MEM_CACHE_INVALIDATE,
    QURT_MEM_CACHE_FLUSH_INVALIDATE,
    QURT_MEM_CACHE_FLUSH_ALL,
    QURT_MEM_CACHE_FLUSH_INVALIDATE_ALL,
    QURT_MEM_CACHE_TABLE_FLUSH_INVALIDATE,
} qurt_mem_cache_op_t;
typedef enum {
        QURT_MEM_REGION_LOCAL=0,
        QURT_MEM_REGION_SHARED=1,
        QURT_MEM_REGION_USER_ACCESS=2,
        QURT_MEM_REGION_FS=4,
        QURT_MEM_REGION_INVALID=10,
} qurt_mem_region_type_t;
struct qurt_pgattr {
   unsigned pga_value;
};
typedef struct qurt_pgattr qurt_pgattr_t;
typedef struct {
    qurt_mem_mapping_t mapping_type;
    unsigned char perms;
    unsigned short owner;
    qurt_pgattr_t pga;
    unsigned ppn;
    qurt_addr_t virtaddr;
    qurt_mem_region_type_t type;
    qurt_size_t size;
} qurt_mem_region_attr_t;
typedef struct {
    char name[32];
    struct ranges{
        unsigned int start;
        unsigned int size;
    } ranges[16];
} qurt_mem_pool_attr_t;
typedef enum {
    HEXAGON_L1_I_CACHE = 0,
    HEXAGON_L1_D_CACHE = 1,
    HEXAGON_L2_CACHE = 2
} qurt_cache_type_t;
typedef enum {
    FULL_SIZE = 0,
    HALF_SIZE = 1,
    THREE_QUARTER_SIZE = 2,
    SEVEN_EIGHTHS_SIZE = 3
} qurt_cache_partition_size_t;
extern qurt_mem_pool_t qurt_mem_default_pool;
int qurt_mem_cache_clean(qurt_addr_t addr, qurt_size_t size, qurt_mem_cache_op_t opcode, qurt_mem_cache_type_t type);
int qurt_mem_l2cache_line_lock (qurt_addr_t addr, qurt_size_t size);
int qurt_mem_l2cache_line_unlock(qurt_addr_t addr, qurt_size_t size);
void qurt_mem_region_attr_init(qurt_mem_region_attr_t *attr);
int qurt_mem_pool_attach(char *name, qurt_mem_pool_t *pool);
int qurt_mem_pool_create(char *name, unsigned base, unsigned size, qurt_mem_pool_t *pool);
int qurt_mem_pool_add_pages(qurt_mem_pool_t pool,
                            unsigned first_pageno,
                            unsigned size_in_pages);
int qurt_mem_pool_remove_pages(qurt_mem_pool_t pool,
                               unsigned first_pageno,
                               unsigned size_in_pages,
                               unsigned flags,
                               void (*callback)(void *),
                               void *arg);
int qurt_mem_pool_attr_get (qurt_mem_pool_t pool, qurt_mem_pool_attr_t *attr);
static inline int qurt_mem_pool_attr_get_size (qurt_mem_pool_attr_t *attr, int range_id, qurt_size_t *size){
    if ((range_id >= 16) || (range_id < 0)){
        (*size) = 0;
        return 4;
    }
    else {
        (*size) = attr->ranges[range_id].size;
    }
    return 0;
}
static inline int qurt_mem_pool_attr_get_addr (qurt_mem_pool_attr_t *attr, int range_id, qurt_addr_t *addr){
    if ((range_id >= 16) || (range_id < 0)){
        (*addr) = 0;
        return 4;
    }
    else {
        (*addr) = (attr->ranges[range_id].start)<<12;
   }
   return 0;
}
int qurt_mem_region_create(qurt_mem_region_t *region, qurt_size_t size, qurt_mem_pool_t pool, qurt_mem_region_attr_t *attr);
int qurt_mem_region_delete(qurt_mem_region_t region);
int qurt_mem_region_attr_get(qurt_mem_region_t region, qurt_mem_region_attr_t *attr);
static inline void qurt_mem_region_attr_set_type(qurt_mem_region_attr_t *attr, qurt_mem_region_type_t type){
    attr->type = type;
}
static inline void qurt_mem_region_attr_get_size(qurt_mem_region_attr_t *attr, qurt_size_t *size){
    (*size) = attr->size;
}
static inline void qurt_mem_region_attr_get_type(qurt_mem_region_attr_t *attr, qurt_mem_region_type_t *type){
    (*type) = attr->type;
}
static inline void qurt_mem_region_attr_set_physaddr(qurt_mem_region_attr_t *attr, qurt_paddr_t addr){
    attr->ppn = (unsigned)(((unsigned)(addr))>>12);
}
static inline void qurt_mem_region_attr_get_physaddr(qurt_mem_region_attr_t *attr, unsigned int *addr){
    (*addr) = (unsigned)(((unsigned) (attr->ppn))<<12);
}
static inline void qurt_mem_region_attr_set_virtaddr(qurt_mem_region_attr_t *attr, qurt_addr_t addr){
    attr->virtaddr = addr;
}
static inline void qurt_mem_region_attr_get_virtaddr(qurt_mem_region_attr_t *attr, unsigned int *addr){
    (*addr) = (unsigned int)(attr->virtaddr);
}
static inline void qurt_mem_region_attr_set_mapping(qurt_mem_region_attr_t *attr, qurt_mem_mapping_t mapping){
    attr->mapping_type = mapping;
}
static inline void qurt_mem_region_attr_get_mapping(qurt_mem_region_attr_t *attr, qurt_mem_mapping_t *mapping){
    (*mapping) = attr->mapping_type;
}
static inline void qurt_mem_region_attr_set_cache_mode(qurt_mem_region_attr_t *attr, qurt_mem_cache_mode_t mode){
    (((attr->pga).pga_value)=(((attr->pga).pga_value)&~(((~0u)>>(31-((3)-(0))))<<(0)))|((((unsigned)mode)<<(0))&(((~0u)>>(31-((3)-(0))))<<(0))));
}
static inline void qurt_mem_region_attr_get_cache_mode(qurt_mem_region_attr_t *attr, qurt_mem_cache_mode_t *mode){
    (*mode) = (qurt_mem_cache_mode_t)((((attr->pga).pga_value)&(((~0u)>>(31-((3)-(0))))<<(0)))>>(0));
}
static inline void qurt_mem_region_attr_set_bus_attr(qurt_mem_region_attr_t *attr, unsigned abits){
    (((attr->pga).pga_value)=(((attr->pga).pga_value)&~(((~0u)>>(31-((5)-(4))))<<(4)))|(((abits)<<(4))&(((~0u)>>(31-((5)-(4))))<<(4))));
}
static inline void qurt_mem_region_attr_get_bus_attr(qurt_mem_region_attr_t *attr, unsigned *pbits){
    (*pbits) = ((((attr->pga).pga_value)&(((~0u)>>(31-((5)-(4))))<<(4)))>>(4));
}
void qurt_mem_region_attr_set_owner(qurt_mem_region_attr_t *attr, int handle);
void qurt_mem_region_attr_get_owner(qurt_mem_region_attr_t *attr, int *p_handle);
void qurt_mem_region_attr_set_perms(qurt_mem_region_attr_t *attr, unsigned perms);
void qurt_mem_region_attr_get_perms(qurt_mem_region_attr_t *attr, unsigned *p_perms);
int qurt_mem_map_static_query(qurt_addr_t *vaddr, qurt_addr_t paddr, unsigned int page_size, qurt_mem_cache_mode_t cache_attribs, qurt_perm_t perm);
int qurt_mem_region_query(qurt_mem_region_t *region_handle, qurt_addr_t vaddr, qurt_paddr_t paddr);
int qurt_mapping_create(qurt_addr_t vaddr, qurt_addr_t paddr, qurt_size_t size,
                         qurt_mem_cache_mode_t cache_attribs, qurt_perm_t perm);
int qurt_mapping_remove(qurt_addr_t vaddr, qurt_addr_t paddr, qurt_size_t size);
qurt_paddr_t qurt_lookup_physaddr (qurt_addr_t vaddr);
static inline void qurt_mem_region_attr_set_physaddr_64(qurt_mem_region_attr_t *attr, qurt_paddr_64_t addr_64){
    attr->ppn = (unsigned)(((unsigned long long)(addr_64))>>12);
}
static inline void qurt_mem_region_attr_get_physaddr_64(qurt_mem_region_attr_t *attr, qurt_paddr_64_t *addr_64){
    (*addr_64) = (unsigned long long)(((unsigned long long)(attr->ppn))<<12);
}
int qurt_mem_map_static_query_64(qurt_addr_t *vaddr, qurt_paddr_64_t paddr_64, unsigned int page_size, qurt_mem_cache_mode_t cache_attribs, qurt_perm_t perm);
int qurt_mem_region_query_64(qurt_mem_region_t *region_handle, qurt_addr_t vaddr, qurt_paddr_64_t paddr_64);
int qurt_mapping_create_64(qurt_addr_t vaddr, qurt_paddr_64_t paddr_64, qurt_size_t size,
                         qurt_mem_cache_mode_t cache_attribs, qurt_perm_t perm);
int qurt_mapping_remove_64(qurt_addr_t vaddr, qurt_paddr_64_t paddr_64, qurt_size_t size);
qurt_paddr_64_t qurt_lookup_physaddr_64 (qurt_addr_t vaddr);
int qurt_mapping_reclaim(qurt_addr_t vaddr, qurt_size_t vsize, qurt_mem_pool_t pool);
int qurt_mem_configure_cache_partition(qurt_cache_type_t cache_type, qurt_cache_partition_size_t partition_size);
void qurt_l2fetch_disable(void);
static inline void qurt_mem_syncht(void){
    __asm__ __volatile__ (" SYNCHT \n");
}
static inline void qurt_mem_barrier(void){
    __asm__ __volatile__ (" BARRIER \n");
}
typedef enum
{
    AsicRsc_None = 0x0,
    AsicRsc_Power_Mem = 0x1,
    AsicRsc_Power_ADSP = 0x2,
    AsicRsc_Power_Core = 0x4,
    AsicRsc_MIPS_Clk = 0x8,
    AsicRsc_MIPS_BW = 0x10,
    AsicRsc_BW_Internal = 0x20,
    AsicRsc_BW_External = 0x40,
    AsicRsc_Core_Clk = 0x80,
    AsicRsc_Latency = 0x100,
    AsicRsc_Thermal = 0x200,
    AsicRsc_Power_Ocmem = 0x400,
    AsicRsc_Power_Hvx = 0x800,
    AsicRsc_EnumForce32bit = 0x7fffffff
} AsicHwRscIdType;
typedef void (*Asic_Rsc_AggregationFnType)(void *pInputData, void *pOutputData);
typedef enum
{
    AsicClk_TypeNone = 0,
    AsicClk_TypeNpa,
    AsicClk_TypeDalFreqSet,
    AsicClk_TypeDalEnable,
    AsicClk_TypeInternalCGC,
    AsicClk_TypeCBC,
    AsicClk_TypeDalDomainSrc
} AsicClkTypeType;
typedef enum
{
    AsicClk_CntlNone = 0,
    AsicClk_CntlOff,
    AsicClk_CntlAlwaysON,
    AsicClk_CntlAlwaysON_DCG,
    AsicClk_CntlSW,
    AsicClk_CntlSW_DCG
} AsicClkCntlTypeType;
typedef enum
{
    AsicClk_MemCntlNone = 0,
    AsicClk_MemCntlAlwaysRetain,
} AsicClkMemRetentionType;
typedef struct
{
    uint32 baseAddr;
    uint32 physAddr;
    uint32 size;
} AsicHwioRegRangeType;
typedef struct
{
    uint32 regOffset;
    uint32 enableMask;
    uint32 hwctlMask;
    uint32 stateMask;
} AsicClkHwIoInfoType;
typedef struct
{
    AdsppmClkIdType clkId;
    AsicClkTypeType clkType;
    AsicClkCntlTypeType clkCntlType;
    AsicClkMemRetentionType clkMemCntlType;
    union
    {
        AsicClkHwIoInfoType hwioInfo;
        char clkName[64];
    } clkInfo;
    AdsppmClkIdType clkSrcId;
    AdsppmMemIdType memoryId;
} AsicClkDescriptorType;
typedef struct
{
    const uint32 numClocks;
    const AdsppmClkIdType *pClocks;
} AsicClockArrayType;
typedef struct
{
    AdsppmBusPortIdType adsppmMaster;
    ICBId_MasterType icbarbMaster;
} AsicBusExtMasterType;
typedef struct
{
    AdsppmBusPortIdType adsppmSlave;
    ICBId_SlaveType icbarbSlave;
} AsicBusExtSlaveType;
typedef struct
{
    AsicBusExtMasterType masterPort;
    AsicBusExtSlaveType slavePort;
} AsicBusExtRouteType;
typedef enum
{
    AsicBusPort_None = 0,
    AsicBusPort_AhbE_M = 0x1,
    AsicBusPort_AhbE_S = 0x2,
    AsicBusPort_AhbI_M = 0x4,
    AsicBusPort_AhbI_S = 0x8,
    AsicBusPort_AhbX_M = 0x10,
    AsicBusPort_AhbX_S = 0x20,
    AsicBusPort_Ahb_Any = 0x3F,
    AsicBusPort_Ext_M = 0x40,
    AsicBusPort_Ext_S = 0x80,
    AsicBusPort_Ext_Any = 0xC0,
    AsicBusPort_Any = 0xFF
} AsicBusPortConnectionType;
typedef struct
{
    AdsppmBusPortIdType port;
    AsicBusPortConnectionType portConnection;
    AdsppmClkIdType busClk;
    AsicClockArrayType regProgClocks;
    union
    {
        ICBId_MasterType icbarbMaster;
        ICBId_SlaveType icbarbSlave;
    } icbarbId;
    AdsppmBusPortIdType accessPort;
} AsicBusPortDescriptorType;
typedef struct
{
    const uint32 numBusPorts;
    const AdsppmBusPortIdType *pBusPorts;
} AsicBusPortArrayType;
typedef struct
{
    uint64 bwThreshold;
    uint64 bwVote;
    uint32 latencyVote;
    uint32 snocFloorVoteMhz;
    uint32 honestFlag;
} AsicCompensatedDdrBwTableEntryType;
typedef enum
{
    AsicPowerDomain_AON = 0,
    AsicPowerDomain_Adsp = 1,
    AsicPowerDomain_LpassCore = 2,
    AsicPowerDomain_Lpm = 3,
    AsicPowerDomain_SRam = 4,
    AsicPowerDomain_Ocmem = 5,
    AsicPowerDomain_Hvx = 6,
    AsicPowerDomain_EnumMax
} AsicPowerDomainType;
typedef struct
{
    AsicPowerDomainType pwrDomain;
    char pwrDomainName[64];
    AsicHwRscIdType pwrDomainType;
    AdsppmClkIdType clkId;
    DalIPCIntInterruptType intrReinitTrigger;
    uint32 intrReinitDone;
    AsicClockArrayType securityClocks;
} AsicPowerDomainDescriptorType;
typedef struct
{
    AdsppmCoreIdType coreId;
    AsicHwRscIdType hwResourceId[Adsppm_Rsc_Id_Max];
    AsicPowerDomainType pwrDomain;
    AsicClockArrayType coreClockInstances;
    AsicBusPortArrayType masterBusPortInstances;
    AsicBusPortArrayType slaveBusPortInstances;
    AdsppmInstanceIdType numInstances;
} AsicCoreDescType;
typedef struct
{
    uint64 startAddr;
    uint32 size;
} AsicAddressRangeType;
typedef struct
{
    AdsppmMemIdType memId;
    AsicPowerDomainType pwrDomain;
} AsicMemDescriptorType;
typedef struct
{
    const AsicMemDescriptorType *pDescriptor;
    AsicAddressRangeType virtAddr;
} AsicMemDescriptorFullType;
typedef struct
{
    MmpmClientClassType aggregateClass;
    qurt_cache_partition_size_t mainPartSize;
} AsicCachePartitionConfigType;
typedef struct
{
    const uint32 numEntries;
    const AsicCachePartitionConfigType* pConfigEntries;
} AsicCachePartitionConfigTableType;
typedef enum
{
    AsicFeatureId_Adsp_Clock_Scaling,
    AsicFeatureId_Adsp_LowTemp_Voltage_Restriction,
    AsicFeatureId_Adsp_PC,
    AsicFeatureId_Ahb_Scaling,
    AsicFeatureId_Ahb_Sw_CG,
    AsicFeatureId_Ahb_DCG,
    AsicFeatureId_LpassCore_PC,
    AsicFeatureId_LpassCore_PC_TZ_Handshake,
    AsicFeatureId_Bus_Scaling,
    AsicFeatureId_CoreClk_Scaling,
    AsicFeatureId_Min_Adsp_BW_Vote,
    AsicFeatureId_InitialState,
    AsicFeatureId_TimelineOptimisationMips,
    AsicFeatureId_TimelineOptimisationBw,
    AsicFeatureId_TimelineOptimisationAhb,
    AsicFeatureId_LpassClkSleepOptimization,
    AsicFeatureId_LPMRetention,
    AsicFeatureId_DomainCoreClocks,
    AsicFeatureId_CachePartitionControl,
    AsicFeatureId_DcvsControl,
    AsicFeatureId_AhbeScaling,
    AsicFeatureId_CacheSizeBWScaling,
    AsicFeatureId_enum_max,
    AsicFeatureId_force_32bit = 0x7FFFFFFF
} AsicFeatureIdType;
typedef enum
{
    AsicFeatureState_Disabled,
    AsicFeatureState_Enabled,
    AsicFeatureState_Limited
} AsicFeatureStateType;
typedef struct
{
    AsicFeatureStateType state;
    uint64 min;
    uint64 max;
} AsicFeatureDescType;
Adsppm_Status ACM_Init(void);
uint32 ACM_GetDebugLevel(void);
AsicHwRscIdType ACM_GetHwRscId(AdsppmCoreIdType, AdsppmRscIdType);
Adsppm_Status ACM_BusBWAggregate(AdsppmBusBWDataIbAbType *pAggregateBwIbAbValue, AdsppmBusBWDataType *pBwValue);
Adsppm_Status ACM_GetBWFromMips(AdsppmMIPSToBWAggregateType *pMipsAggregateData);
Adsppm_Status ACM_GetClockFromMips(AdsppmMIPSToClockAggregateType *pMipsAggregateData);
Adsppm_Status ACM_GetClockFromBW(uint32 *pClock, AdsppmBusBWDataIbAbType *pAHBBwData);
void ACM_GetCompensatedDdrBwTableEntry( uint64 vote, AsicCompensatedDdrBwTableEntryType *pTableEntry);
void ACM_ApplyAdspDdrBwConcurrencyFactor(uint32 numVotes, uint64 maxBw, uint64* pAb, uint64* pIb);
uint32 ACM_GetMipsFromMpps(uint32 mppsTotal, uint32 numDominantThreads);
Adsppm_Status ACM_ApplyAhbBwCompensation(uint64 bwVoteIn, uint64* pBwVoteOut);
Adsppm_Status ACM_GetAhbeFromAdspFreq(uint32 adspClockFreq, uint32* ahbClockFreq);
Adsppm_Status ACM_GetBWScalingFactorFromCacheSize(uint32 cachesize, uint32* scalingfactor);
int ACMBus_GetNumberOfExtRoutes(void);
void ACMBus_GetExtRoutes(AsicBusExtRouteType *pExtRoutes);
AdsppmClkIdType ACMClk_GetInstanceCoreClockId(AdsppmCoreIdType coreId, AdsppmInstanceIdType instanceId, AdsppmClkIdType coreClock);
AdsppmBusPortIdType ACMBus_GetInstanceMasterBusPortId(AdsppmCoreIdType coreId, AdsppmInstanceIdType instanceId, AdsppmBusPortIdType masterBusPort);
AdsppmBusPortIdType ACMBus_GetInstanceSlaveBusPortId(AdsppmCoreIdType coreId, AdsppmInstanceIdType instanceId, AdsppmBusPortIdType slaveBusPort);
AdsppmBusPortIdType ACMBus_GetCoreSlavePort(AdsppmCoreIdType coreId, AdsppmInstanceIdType instanceId);
const AsicBusPortDescriptorType *ACMBus_GetPortDescriptor(AdsppmBusPortIdType);
AsicBusPortConnectionType ACMBus_GetPortConnections(AdsppmBusPortIdType port);
AsicClkTypeType ACMClk_GetClockType(AdsppmClkIdType);
const AsicClkDescriptorType *ACMClk_GetClockDescriptor(AdsppmClkIdType);
const AsicPowerDomainDescriptorType *ACMClk_GetPwrDomainDescriptor(AsicPowerDomainType);
AsicPowerDomainType ACMPwr_GetPowerDomain(AdsppmCoreIdType);
AsicPowerDomainType ACMPwr_GetMemPowerDomain(AdsppmMemIdType mem);
AsicHwRscIdType ACMPwr_GetMemPowerType(AdsppmMemIdType mem);
const AsicCoreDescType *ACM_GetCoreDescriptor(AdsppmCoreIdType);
uint32 ACM_GetTZSecureInterrupt(void);
AsicFeatureStateType ACM_GetFeatureStatus(AsicFeatureIdType);
AsicFeatureDescType *ACM_GetFeatureDescriptor(AsicFeatureIdType featureId);
uint64 ACM_AdjustParamValue(AsicFeatureIdType featureId, uint64 value);
uint64 ACM_ms_to_sclk(uint64 ms);
uint64 ACM_us_to_qclk(uint64 us);
const int ACMBus_GetNumberOfMipsBwRoutes(void);
const AdsppmBusRouteType *ACMBus_GetMipsBwRoutes(void);
uint32 ACM_GetHwThreadNumber(void);
uint64 ACM_GetLprVoteTimeoutValue(void);
AsicHwioRegRangeType *ACM_GetLpassRegRange(void);
AsicHwioRegRangeType *ACM_GetL2ConfigRegRange(void);
AsicMemDescriptorFullType *ACM_GetMemoryDescriptor(AdsppmMemIdType mem);
AsicAddressRangeType *ACM_GetVirtMemAddressRange(AdsppmMemIdType mem);
AdsppmStatusType ACM_SetVirtMemAddressRange(AdsppmMemIdType mem, uint64 addr, uint32 size);
AsicCachePartitionConfigTableType* ACM_GetCachePartitionConfig();
typedef void * ULogHandle;
typedef int32 ULogResult;
typedef enum
{
  ULOG_ERR_INVALIDNAME = 1,
  ULOG_ERR_INVALIDPARAMETER,
  ULOG_ERR_ALREADYCONNECTED,
  ULOG_ERR_ALREADYCREATED,
  ULOG_ERR_NOTCONNECTED,
  ULOG_ERR_ALREADYENABLED,
  ULOG_ERR_INITINCOMPLETE,
  ULOG_ERR_READONLY,
  ULOG_ERR_INVALIDHANDLE,
  ULOG_ERR_MALLOC,
  ULOG_ERR_ASSIGN,
  ULOG_ERR_INSUFFICIENT_BUFFER,
  ULOG_ERR_NAMENOTFOUND,
  ULOG_ERR_MISC,
  ULOG_ERR_OVERRUN,
  ULOG_ERR_NOTSUPPORTED,
} ULOG_ERRORS;
typedef enum
{
  ULOG_VALUE_BUFFER,
  ULOG_VALUE_SHARED_HEADER,
  ULOG_VALUE_READCORE,
  ULOG_VALUE_WRITECORE,
  ULOG_VALUE_LOCKCORE,
  ULOG_VALUE_LOG_BASE = 0x100,
  ULOG_VALUE_LOG_READFLAGS = ULOG_VALUE_LOG_BASE,
} ULOG_VALUE_ID;
typedef enum
{
  ULOG_LOCK_NONE = 0,
  ULOG_LOCK_OS,
  ULOG_LOCK_SPIN,
  ULOG_LOCK_INT,
  ULOG_LOCK_UINT32 = 0x7FFFFFFF
} ULOG_LOCK_TYPE;
typedef enum
{
  ULOG_ALLOC_LOCAL = 0x01,
  ULOG_ALLOC_MANUAL = 0x04,
  ULOG_ALLOC_TYPE_MASK = 0xFF,
  ULOG_ALLOC_UINT32 = 0x7FFFFFFF
} ULOG_ALLOC_TYPE;
typedef enum
{
  ULOG_MEMORY_USAGE_FULLACCESS = 0x0300,
  ULOG_MEMORY_USAGE_READABLE = 0x0100,
  ULOG_MEMORY_USAGE_WRITEABLE = 0x0200,
  ULOG_MEMORY_USAGE_UINT32 = 0x7FFFFFFF
} ULOG_MEMORY_USAGE_TYPE;
typedef enum
{
  ULOG_MEMORY_CONFIG_LOCAL = 0x010000,
  ULOG_MEMORY_CONFIG_SHARED = 0x020000,
  ULOG_MEMORY_CONFIG_UINT32 = 0x7FFFFFFF
} ULOG_MEMORY_CONFIG_TYPE;
typedef uint64(*ULOG_ALT_TS_SRC)(void);
typedef enum
{
  ULOG_INTERFACE_INVALID = 0,
  ULOG_INTERFACE_RAW,
  ULOG_INTERFACE_REALTIME,
  ULOG_INTERFACE_UINT32 = 0x7FFFFFFF
} ULOG_INTERFACE_TYPE;
typedef enum
{
  ULOG_REALTIME_SUBTYPE_RESERVED_FOR_RAW = 0,
  ULOG_REALTIME_SUBTYPE_PRINTF,
  ULOG_REALTIME_SUBTYPE_BYTEDATA,
  ULOG_REALTIME_SUBTYPE_STRINGDATA,
  ULOG_REALTIME_SUBTYPE_WORDDATA,
  ULOG_REALTIME_SUBTYPE_CSVDATA,
  ULOG_REALTIME_SUBTYPE_VECTOR,
  ULOG_REALTIME_SUBTYPE_MULTIPART,
  ULOG_REALTIME_SUBTYPE_MULTIPART_STREAM_END,
  ULOG_SUBTYPE_REALTIME_TOKENIZED_STRING,
  ULOG_SUBTYPE_RESERVED1,
  ULOG_SUBTYPE_RESERVED2,
  ULOG_SUBTYPE_RESERVED3,
  ULOG_SUBTYPE_RAW8,
  ULOG_SUBTYPE_RAW16,
  ULOG_SUBTYPE_RAW32,
  ULOG_REALTIME_SUBTYPE_UINT32 = 0x7FFFFFFF
} ULOG_REALTIME_SUBTYPES;
typedef enum
{
  ULOG_LOG_WRAPPED = 0x0001,
  ULOG_ERR_LARGEMSG = 0x0002,
  ULOG_ERR_LARGEMSGOUT = 0x0004,
  ULOG_ERR_RESET = 0x0008,
  ULOG_ERR_UINT32 = 0x7FFFFFFF
} ULOG_WRITER_STATUS_TYPE;
typedef enum
{
  ULOG_RD_FLAG_REWIND = 0x0001,
  ULOG_RD_FLAG_UINT32 = 0x7FFFFFFF
} ULOG_RD_FLAG_TYPE;
typedef enum
{
  ULOG_ATTRIBUTE_READ_AUTOREWIND = 0x1,
  ULOG_ATTRIBUTE_UINT32 = 0x7FFFFFFF
} ULOG_ATTRIBUTE_TYPE;
typedef struct
{
  ULogResult (*write) (ULogHandle h, uint32 firstMsgCount, const char * firstMsgContent, uint32 secondMsgCount, const char * secondMsgContent, ULOG_INTERFACE_TYPE interfaceType);
  DALBOOL (*multipartMsgBegin) (ULogHandle h);
  void (*multipartMsgEnd) (ULogHandle h);
} ULOG_CORE_VTABLE;
ULogResult ULogCore_Init(void);
ULogResult ULogCore_LogCreate(ULogHandle * h,
                              const char * logName,
                              uint32 bufferSize,
                              uint32 memoryType,
                              ULOG_LOCK_TYPE lockType,
                              ULOG_INTERFACE_TYPE interfaceType);
void ULogCore_SetLockType( ULogHandle h, ULOG_LOCK_TYPE lockType );
ULogResult ULogCore_HeaderSet(ULogHandle h, char * headerText);
void ULogCore_AttributeSet(ULogHandle h, uint32 attribute);
ULogResult ULogCore_MemoryAssign(ULogHandle h,
                                 ULOG_VALUE_ID id,
                                 void * bufferPtr,
                                 uint32 bufferSize);
ULogResult ULogCore_Enable(ULogHandle h);
DALBOOL ULogCore_IsEnabled(ULogHandle h, ULOG_CORE_VTABLE ** core);
ULogResult ULogCore_Query(ULogHandle h, ULOG_VALUE_ID id, uint32 * value);
ULogResult ULogCore_Read(ULogHandle h,
                         uint32 outputSize,
                         char * outputMem,
                         uint32 * outputCount);
ULogResult ULogCore_ReadEx(ULogHandle h,
                           uint32 outputSize,
                           char * outputMem,
                           uint32 * outputCount,
                           uint32 outputMessageLimit);
ULogResult ULogCore_ReadSessionComplete(ULogHandle h);
ULogResult ULogCore_Allocate(ULogHandle h, uint32 bufferSize);
ULogResult ULogCore_Disable(ULogHandle h);
ULogResult ULogCore_List(uint32 * registeredCount,
                         uint32 nameArraySize,
                         uint32 * namesReadCount,
                         char * nameArray);
ULogResult ULogCore_ListEx(uint32 offsetCount,
                           uint32 * registeredCount,
                           uint32 nameArraySize,
                           uint32 * namesReadCount,
                           char * nameArray);
ULogResult ULogCore_MsgFormat(ULogHandle h,
                              char * msg,
                              char * msgString,
                              uint32 msgStringSize,
                              uint32 * msgConsumed);
ULogResult ULogCore_HeaderRead(ULogHandle h,
                               uint32 headerReadOffset,
                               char * headerString,
                               uint32 headerStringSize,
                               uint32 * headerActualLength);
ULogResult ULogCore_Connect(ULogHandle * h, const char * logName);
void ULogCore_LogLevelSet(ULogHandle h, uint32 level);
uint32 ULogCore_LogLevelGet(ULogHandle h);
ULogHandle ULogCore_TeeAdd(ULogHandle h1, ULogHandle h2);
ULogHandle ULogCore_TeeRemove(ULogHandle h1, ULogHandle h2);
ULogHandle ULogCore_HandleGet(char *logName);
DALBOOL ULogCore_IsLogPresent(ULogHandle h, char *logName);
ULogResult ULogCore_SetTimestampSrcFn(ULogHandle h, ULOG_ALT_TS_SRC altULogTimeStampFn);
ULogResult ULogCore_SetTransportToRAM(ULogHandle h);
ULogResult ULogCore_SetTransportToStm(ULogHandle h, unsigned char protocol_num);
ULogResult ULogCore_SetTransportToStmAscii(ULogHandle h, unsigned char protocol_num);
ULogResult ULogCore_SetTransportToAlt(ULogHandle h, ULOG_CORE_VTABLE* newTansportVTable);
ULogResult ULogDiagAddPlugin(int(*new_pluginfcn)(uint32), uint32 new_plugin_id);
ULogResult ULogDiagAddPluginExt
(
  int(*new_pluginfcnext)(void*, unsigned long int),
  uint32 new_plugin_id
);
ULogResult ULogFront_RawInit(ULogHandle * h,
                             const char * name,
                             uint32 logBufSize,
                             uint32 logBufMemType,
                             ULOG_LOCK_TYPE logLockType);
ULogResult ULogFront_RawLog(ULogHandle h,
                            const char * dataArray,
                            uint32 dataCount);
ULogResult ULogFront_RealTimeInit(ULogHandle * h,
                             const char * name,
                             uint32 logBufSize,
                             uint32 logBufMemType,
                             ULOG_LOCK_TYPE logLockType);
ULogResult ULogFront_RealTimeStaticInit( ULogHandle * h,
                                         const char * name,
                                         uint32 logMessageSlots,
                                         uint32 logBufMemType,
                                         ULOG_LOCK_TYPE logLockType);
ULogResult ULogFront_RealTimePrintf(ULogHandle h,
                                    uint32 dataCount,
                                    const char * formatStr,
                                    ...);
ULogResult ULogFront_RealTimeStaticPrintf(ULogHandle h, uint32 msgSlot, uint32 dataCount, const char * formatStr, ...);
ULogResult ULogFront_RealTimeVprintf(ULogHandle h,
                                     uint32 dataCount,
                                     const char * formatStr,
                                     va_list ap);
ULogResult ULogFront_RealTimeData(ULogHandle h, uint32 dataCount, ...);
ULogResult ULogFront_RealTimeCharArray(ULogHandle h,
                                       uint32 byteCount,
                                       char * byteData);
ULogResult ULogFront_RealTimeString(ULogHandle h, char * cStr);
ULogResult ULogFront_RealTimeWordArray(ULogHandle h,
                                       uint32 wordCount,
                                       const uint32 * wordData);
ULogResult ULogFront_RealTimeCsv(ULogHandle h,
                                 uint32 wordCount,
                                 const uint32 * wordData);
ULogResult ULogFront_RealTimeVector(ULogHandle h,
                                    const char * formatStr,
                                    unsigned short entryByteCount,
                                    unsigned short vectorLength,
                                    const void * vector);
DALBOOL ULogFront_RealTimeMultipartMsgBegin (ULogHandle h);
void ULogFront_RealTimeMultipartMsgEnd (ULogHandle h);
typedef unsigned short word;
typedef unsigned long dword;
typedef unsigned char uint1;
typedef unsigned short uint2;
typedef unsigned long uint4;
typedef signed char int1;
typedef signed short int2;
typedef long int int4;
typedef signed long sint31;
typedef signed short sint15;
typedef signed char sint7;
typedef uint16 UWord16 ;
typedef uint32 UWord32 ;
typedef int32 Word32 ;
typedef int16 Word16 ;
typedef uint8 UWord8 ;
typedef int8 Word8 ;
typedef int32 Vect32 ;
typedef void * addr_t;
        typedef struct __attribute__((packed))
        { uint16 x; }
        unaligned_uint16;
        typedef struct __attribute__((packed))
        { uint32 x; }
        unaligned_uint32;
        typedef struct __attribute__((packed))
        { uint64 x; }
        unaligned_uint64;
        typedef struct __attribute__((packed))
        { int16 x; }
        unaligned_int16;
        typedef struct __attribute__((packed))
        { int32 x; }
        unaligned_int32;
        typedef struct __attribute__((packed))
        { int64 x; }
        unaligned_int64;
  extern dword rex_int_lock(void);
  extern dword rex_int_free(void);
   extern void rex_task_lock( void);
   extern void rex_task_free( void);
typedef unsigned long qword[ 2 ];
  typedef unsigned long qc_qword[ 2 ];
void qw_set
(
  qc_qword qw,
  uint32 hi,
  uint32 lo
);
void qw_equ
(
  qc_qword qw1,
  qc_qword qw2
);
uint32 qw_hi
(
  qc_qword qw
);
uint32 qw_lo
(
  qc_qword qw
);
void qw_inc
(
  qc_qword qw1,
  uint32 val
);
void qw_dec
(
  qc_qword qw1,
  uint32 val
);
void qw_add
(
  qc_qword sum,
  qc_qword addend,
  qc_qword adder
);
void qw_sub
(
  qc_qword difference,
  qc_qword subtrahend,
  qc_qword subtractor
);
void qw_mul
(
  qc_qword product,
  qc_qword multiplicand,
  uint32 multiplier
);
word qw_div
(
  qc_qword quotient,
  qc_qword dividend,
  word divisor
);
word qw_div_by_power_of_2
(
  qc_qword quotient,
  qc_qword dividend,
  unsigned short num_bits
);
void qw_shift
(
  qc_qword shifticand,
  int shiftidend
);
int qw_cmp
(
  qc_qword qw1,
  qc_qword qw2
);
typedef struct
{
  uint8 cmd_code;
  uint8 sub_cmd;
}
msg_get_ssid_ranges_req_type;
typedef struct
{
  uint8 cmd_code;
  uint8 sub_cmd;
  uint8 status;
  uint8 rsvd;
  uint32 range_cnt;
  struct
  {
    uint16 ssid_first;
    uint16 ssid_last;
  }
  ssid_ranges[1];
}
msg_get_ssid_ranges_rsp_type;
typedef struct
{
  uint8 cmd_code;
  uint8 sub_cmd;
  uint16 ssid_start;
  uint16 ssid_end;
}
msg_get_mask_req_type;
typedef struct
{
  uint8 cmd_code;
  uint8 sub_cmd;
  uint16 ssid_start;
  uint16 ssid_end;
  uint8 status;
  uint8 pad;
  uint32 bld_mask[1];
}
msg_get_mask_rsp_type;
typedef struct
{
  uint8 cmd_code;
  uint8 sub_cmd;
  uint16 ssid_start;
  uint16 ssid_end;
  uint16 pad;
  uint32 rt_mask[1];
}
msg_set_rt_mask_req_type;
typedef struct
{
  uint8 cmd_code;
  uint8 sub_cmd;
  uint16 ssid_start;
  uint16 ssid_end;
  uint8 status;
  uint8 pad;
  uint32 rt_mask[1];
}
msg_set_rt_mask_rsp_type;
typedef struct
{
  uint8 cmd_code;
  uint8 sub_cmd;
  uint16 rsvd;
  uint32 rt_mask;
}
msg_set_all_masks_req_type;
typedef struct
{
  uint8 cmd_code;
  uint8 sub_cmd;
  uint8 status;
  uint8 rsvd;
  uint32 rt_mask;
}
msg_set_all_masks_rsp_type;
typedef struct
{
  uint16 line;
  uint16 ss_id;
  uint32 ss_mask;
}
msg_desc_type;
typedef struct
{
  uint8 cmd_code;
  uint8 ts_type;
  uint8 num_args;
  uint8 drop_cnt;
  qword ts;
}
msg_hdr_type;
typedef enum
{
  MSG_TS_TYPE_CDMA_FULL = 0,
  MSG_TS_TYPE_WIN32,
  MSG_TS_TYPE_GW
}
msg_ts_type;
typedef struct
{
  msg_hdr_type hdr;
  msg_desc_type desc;
  uint32 args[1];
}
msg_ext_type;
typedef struct __attribute__((__packed__))
{
  byte cmd_code;
  word msg_level;
}
msg_legacy_req_type;
typedef struct __attribute__((__packed__))
{
  byte cmd_code;
  word qty;
  dword drop_cnt;
  dword total_msgs;
  byte level;
  char file[(12+1)];
  word line;
  char fmt[40];
  dword code1;
  dword code2;
  dword code3;
  qword time;
}
msg_legacy_rsp_type;
typedef struct
{
  msg_desc_type desc;
  uint32 msg_hash;
}
msg_qsr_const_type;
typedef struct
{
  msg_qsr_const_type qsr_const_blk;
  const char *fname;
} err_msg_qsr_const_type;
typedef struct
{
  msg_hdr_type hdr;
  const msg_qsr_const_type* qsr_const_data_ptr;
  uint32 qsr_flag;
  uint32 args[1];
}
msg_qsr_store_type;
typedef struct
{
  msg_desc_type desc;
  const char * msg;
}
msg_v2_const_type;
typedef struct
{
  msg_v2_const_type msg_v2_const_blk;
  const char *fname;
} err_msg_v2_const_type;
  void qsr_msg_send ( const msg_qsr_const_type * xx_msg_const_ptr);
  void msg_v2_send ( const msg_v2_const_type * xx_msg_const_ptr);
  void qsr_msg_send_1 (const msg_qsr_const_type * xx_msg_const_ptr, uint32 xx_arg1);
  void msg_v2_send_1 (const msg_v2_const_type * xx_msg_const_ptr, uint32 xx_arg1);
  void qsr_msg_send_2 ( const msg_qsr_const_type * xx_msg_const_ptr,uint32 xx_arg1,
    uint32 xx_arg2);
  void msg_v2_send_2 ( const msg_v2_const_type * xx_msg_const_ptr,uint32 xx_arg1,
    uint32 xx_arg2);
  void qsr_msg_send_3 ( const msg_qsr_const_type * xx_msg_const_ptr, uint32 xx_arg1,
    uint32 xx_arg2, uint32 xx_arg3);
  void msg_v2_send_3 ( const msg_v2_const_type * xx_msg_const_ptr, uint32 xx_arg1,
    uint32 xx_arg2, uint32 xx_arg3);
  void qsr_msg_send_var ( const msg_qsr_const_type * xx_msg_const_ptr, uint32 num_args, ...);
  void msg_v2_send_var ( const msg_v2_const_type * xx_msg_const_ptr, uint32 num_args, ...);
void msg_qsrerrlog_3 (const err_msg_qsr_const_type* const_blk, uint32 code1, uint32 code2, uint32 code3);
void msg_qsrerrlog_2 (const err_msg_qsr_const_type* const_blk, uint32 code1, uint32 code2);
void msg_qsrerrlog_1 (const err_msg_qsr_const_type* const_blk, uint32 code1);
void msg_qsrerrlog_0 (const err_msg_qsr_const_type* const_blk);
void msg_v2_errlog_3 (const err_msg_v2_const_type* const_blk, uint32 code1, uint32 code2, uint32 code3);
void msg_v2_errlog_2 (const err_msg_v2_const_type* const_blk, uint32 code1, uint32 code2);
void msg_v2_errlog_1 (const err_msg_v2_const_type* const_blk, uint32 code1);
void msg_v2_errlog_0 (const err_msg_v2_const_type* const_blk);
void *qurt_malloc( unsigned int size);
void *qurt_calloc(unsigned int elsize, unsigned int num);
void *qurt_realloc(void *ptr, int newsize);
void qurt_free( void *ptr);
int qurt_futex_wait(void *lock, int val);
int qurt_futex_wait_cancellable(void *lock, int val);
int qurt_futex_wait64(void *lock, long long val);
int qurt_futex_wake(void *lock, int n_to_wake);
typedef union qurt_mutex_aligned8{
    struct {
        unsigned int holder;
        unsigned int count;
        unsigned int queue;
        unsigned int wait_count;
    };
    unsigned long long int raw;
} qurt_mutex_t;
void qurt_mutex_init(qurt_mutex_t *lock);
void qurt_mutex_destroy(qurt_mutex_t *lock);
void qurt_mutex_lock(qurt_mutex_t *lock);
void qurt_mutex_unlock(qurt_mutex_t *lock);
int qurt_mutex_try_lock(qurt_mutex_t *lock);
typedef union {
 unsigned int raw[2] __attribute__((aligned(8)));
 struct {
  unsigned short val;
  unsigned short n_waiting;
        unsigned int reserved1;
        unsigned int queue;
        unsigned int reserved2;
 }X;
} qurt_sem_t;
int qurt_sem_add(qurt_sem_t *sem, unsigned int amt);
static inline int qurt_sem_up(qurt_sem_t *sem) { return qurt_sem_add(sem,1); };
int qurt_sem_down(qurt_sem_t *sem);
int qurt_sem_try_down(qurt_sem_t *sem);
void qurt_sem_init(qurt_sem_t *sem);
void qurt_sem_destroy(qurt_sem_t *sem);
void qurt_sem_init_val(qurt_sem_t *sem, unsigned short val);
static inline unsigned short qurt_sem_get_val(qurt_sem_t *sem ){return sem->X.val;}
int qurt_sem_down_cancellable(qurt_sem_t *sem);
typedef unsigned long long int qurt_pipe_data_t;
typedef struct {
    qurt_mutex_t pipe_lock;
    qurt_sem_t senders;
    qurt_sem_t receiver;
    unsigned int size;
    unsigned int sendidx;
    unsigned int recvidx;
    void (*lock_func)(qurt_mutex_t *);
    void (*unlock_func)(qurt_mutex_t *);
    int (*try_lock_func)(qurt_mutex_t *);
    void (*destroy_lock_func)(qurt_mutex_t *);
    unsigned int magic;
    qurt_pipe_data_t *data;
} qurt_pipe_t;
typedef struct {
  qurt_pipe_data_t *buffer;
  unsigned int elements;
  unsigned char mem_partition;
} qurt_pipe_attr_t;
static inline void qurt_pipe_attr_init(qurt_pipe_attr_t *attr)
{
  attr->buffer = 0;
  attr->elements = 0;
  attr->mem_partition = 0;
}
static inline void qurt_pipe_attr_set_buffer(qurt_pipe_attr_t *attr, qurt_pipe_data_t *buffer)
{
  attr->buffer = buffer;
}
static inline void qurt_pipe_attr_set_elements(qurt_pipe_attr_t *attr, unsigned int elements)
{
  attr->elements = elements;
}
static inline void qurt_pipe_attr_set_buffer_partition(qurt_pipe_attr_t *attr, unsigned char mem_partition)
{
  attr->mem_partition = mem_partition;
}
int qurt_pipe_create(qurt_pipe_t **pipe, qurt_pipe_attr_t *attr);
int qurt_pipe_init(qurt_pipe_t *pipe, qurt_pipe_attr_t *attr);
void qurt_pipe_destroy(qurt_pipe_t *pipe);
void qurt_pipe_delete(qurt_pipe_t *pipe);
void qurt_pipe_send(qurt_pipe_t *pipe, qurt_pipe_data_t data);
qurt_pipe_data_t qurt_pipe_receive(qurt_pipe_t *pipe);
int qurt_pipe_try_send(qurt_pipe_t *pipe, qurt_pipe_data_t data);
qurt_pipe_data_t qurt_pipe_try_receive(qurt_pipe_t *pipe, int *success);
int qurt_pipe_receive_cancellable(qurt_pipe_t *pipe, qurt_pipe_data_t *result);
int qurt_pipe_send_cancellable(qurt_pipe_t *pipe, qurt_pipe_data_t data);
int qurt_pipe_is_empty(qurt_pipe_t *pipe);
int qurt_printf(const char* format, ...);
void qurt_assert_error(const char *filename, int lineno) __attribute__((noreturn));
typedef enum {
    CCCC_PARTITION = 0,
    MAIN_PARTITION = 1,
    AUX_PARTITION = 2,
    MINIMUM_PARTITION = 3
} qurt_cache_partition_t;
typedef unsigned int qurt_thread_t;
typedef struct _qurt_thread_attr {
    char name[16];
    unsigned char tcb_partition;
    unsigned char affinity;
    unsigned short priority;
    unsigned char asid;
    unsigned char bus_priority;
    unsigned short timetest_id;
    unsigned int stack_size;
    void *stack_addr;
} qurt_thread_attr_t;
static inline void qurt_thread_attr_init (qurt_thread_attr_t *attr)
{
    attr->name[0] = 0;
    attr->tcb_partition = 0;
    attr->priority = 255;
    attr->asid = 0;
    attr->affinity = (-1);
    attr->bus_priority = 255;
    attr->timetest_id = (-2);
    attr->stack_size = 0;
    attr->stack_addr = 0;
}
static inline void qurt_thread_attr_set_name (qurt_thread_attr_t *attr, char *name)
{
    strlcpy (attr->name, name, 16);
    attr->name[16 - 1] = 0;
}
static inline void qurt_thread_attr_set_tcb_partition (qurt_thread_attr_t *attr, unsigned char tcb_partition)
{
    attr->tcb_partition = tcb_partition;
}
static inline void qurt_thread_attr_set_priority (qurt_thread_attr_t *attr, unsigned short priority)
{
    attr->priority = priority;
}
static inline void qurt_thread_attr_set_affinity (qurt_thread_attr_t *attr, unsigned char affinity)
{
    attr->affinity = affinity;
}
static inline void qurt_thread_attr_set_timetest_id (qurt_thread_attr_t *attr, unsigned short timetest_id)
{
    attr->timetest_id = timetest_id;
}
static inline void qurt_thread_attr_set_stack_size (qurt_thread_attr_t *attr, unsigned int stack_size)
{
    attr->stack_size = stack_size;
}
static inline void qurt_thread_attr_set_stack_addr (qurt_thread_attr_t *attr, void *stack_addr)
{
    attr->stack_addr = stack_addr;
}
static inline void qurt_thread_attr_set_bus_priority ( qurt_thread_attr_t *attr, unsigned short bus_priority)
{
    attr->bus_priority = bus_priority;
}
void qurt_thread_get_name (char *name, unsigned char max_len);
int qurt_thread_create (qurt_thread_t *thread_id, qurt_thread_attr_t *attr, void (*entrypoint) (void *), void *arg);
void qurt_thread_stop(void);
int qurt_thread_resume(unsigned int thread_id);
qurt_thread_t qurt_thread_get_id (void);
qurt_cache_partition_t qurt_thread_get_l2cache_partition (void);
void qurt_thread_set_timetest_id (unsigned short tid);
void qurt_thread_set_cache_partition(qurt_cache_partition_t l1_icache, qurt_cache_partition_t l1_dcache, qurt_cache_partition_t l2_cache);
void qurt_thread_set_coprocessor(unsigned int enable, unsigned int coproc_id);
unsigned short qurt_thread_get_timetest_id (void);
void qurt_thread_exit(int status);
int qurt_thread_join(unsigned int tid, int *status);
unsigned int qurt_thread_get_anysignal(void);
int qurt_thread_get_priority (qurt_thread_t threadid);
int qurt_thread_set_priority (qurt_thread_t threadid, unsigned short newprio);
unsigned int qurt_api_version(void);
int qurt_thread_attr_get (qurt_thread_t thread_id, qurt_thread_attr_t *attr);
unsigned int qurt_trace_get_marker(void);
int qurt_trace_changed(unsigned int prev_trace_marker, unsigned int trace_mask);
unsigned int qurt_etm_set_config(unsigned int type, unsigned int route, unsigned int filter);
unsigned int qurt_etm_enable(unsigned int enable_flag);
unsigned int qurt_etm_testbus_set_config(unsigned int cfg_data);
unsigned int qurt_etm_set_breakpoint(unsigned int type, unsigned int address, unsigned int data, unsigned int mask);
unsigned int qurt_etm_set_breakarea(unsigned int type, unsigned int start_address, unsigned int end_address, unsigned int count);
void qurt_profile_reset_idle_pcycles (void);
unsigned long long int qurt_profile_get_thread_pcycles(void);
unsigned long long int qurt_profile_get_thread_tcycles(void);
unsigned long long int qurt_get_core_pcycles(void);
void qurt_profile_get_idle_pcycles (unsigned long long *pcycles);
void qurt_profile_get_threadid_pcycles (int thread_id, unsigned long long *pcycles);
void qurt_profile_reset_threadid_pcycles (int thread_id);
void qurt_profile_enable (int enable);
typedef struct {
   unsigned int holder __attribute__((aligned(8)));
   unsigned short waiters;
   unsigned short refs;
   unsigned int queue;
   unsigned int excess_locks;
} qurt_rmutex2_t;
void qurt_rmutex2_init(qurt_rmutex2_t *lock);
void qurt_rmutex2_destroy(qurt_rmutex2_t *lock);
void qurt_rmutex2_lock(qurt_rmutex2_t *lock);
void qurt_rmutex2_unlock(qurt_rmutex2_t *lock);
int qurt_rmutex2_try_lock(qurt_rmutex2_t *lock);
typedef union {
    unsigned long long raw;
    struct {
        unsigned int count;
        unsigned int n_waiting;
        unsigned int queue;
        unsigned int reserved;
    }X;
} qurt_cond_t;
void qurt_cond_init(qurt_cond_t *cond);
void qurt_cond_destroy(qurt_cond_t *cond);
void qurt_cond_signal(qurt_cond_t *cond);
void qurt_cond_broadcast(qurt_cond_t *cond);
void qurt_cond_wait(qurt_cond_t *cond, qurt_mutex_t *mutex);
void qurt_cond_wait2(qurt_cond_t *cond, qurt_rmutex2_t *mutex);
typedef union {
 struct {
        unsigned short threads_left;
  unsigned short count;
  unsigned int threads_total;
        unsigned int queue;
        unsigned int reserved;
 };
 unsigned long long int raw;
} qurt_barrier_t;
int qurt_barrier_init(qurt_barrier_t *barrier, unsigned int threads_total);
int qurt_barrier_destroy(qurt_barrier_t *barrier);
int qurt_barrier_wait(qurt_barrier_t *barrier);
unsigned int qurt_fastint_register(int intno, void (*fn)(int));
unsigned int qurt_fastint_deregister(int intno);
unsigned int qurt_isr_register(int intno, void (*fn)(int));
unsigned int qurt_isr_deregister(int intno);
typedef union {
 unsigned long long int raw;
 struct {
  unsigned int waiting;
  unsigned int signals_in;
  unsigned int queue;
  unsigned int reserved;
 }X;
} qurt_allsignal_t;
void qurt_allsignal_init(qurt_allsignal_t *signal);
void qurt_allsignal_destroy(qurt_allsignal_t *signal);
static inline unsigned int qurt_allsignal_get(qurt_allsignal_t *signal)
{ return signal->X.signals_in; };
void qurt_allsignal_wait(qurt_allsignal_t *signal, unsigned int mask);
void qurt_allsignal_set(qurt_allsignal_t *signal, unsigned int mask);
typedef union {
    unsigned long long int raw;
    struct {
        unsigned int signals;
        unsigned int waiting;
        unsigned int queue;
        unsigned int attribute;
    }X;
} qurt_signal_t;
void qurt_signal_init(qurt_signal_t *signal);
void qurt_signal_destroy(qurt_signal_t *signal);
unsigned int qurt_signal_wait(qurt_signal_t *signal, unsigned int mask,
                unsigned int attribute);
static inline unsigned int qurt_signal_wait_any(qurt_signal_t *signal, unsigned int mask)
{
  return qurt_signal_wait(signal, mask, 0x00000000);
}
static inline unsigned int qurt_signal_wait_all(qurt_signal_t *signal, unsigned int mask)
{
  return qurt_signal_wait(signal, mask, 0x00000001);
}
void qurt_signal_set(qurt_signal_t *signal, unsigned int mask);
unsigned int qurt_signal_get(qurt_signal_t *signal);
void qurt_signal_clear(qurt_signal_t *signal, unsigned int mask);
int qurt_signal_wait_cancellable(qurt_signal_t *signal, unsigned int mask,
                                 unsigned int attribute,
                                 unsigned int *return_mask);
typedef qurt_signal_t qurt_anysignal_t;
static inline void qurt_anysignal_init(qurt_anysignal_t *signal)
{
  qurt_signal_init(signal);
}
static inline void qurt_anysignal_destroy(qurt_anysignal_t *signal)
{
  qurt_signal_destroy(signal);
}
static inline unsigned int qurt_anysignal_wait(qurt_anysignal_t *signal, unsigned int mask)
{
  return qurt_signal_wait(signal, mask, 0x00000000);
}
unsigned int qurt_anysignal_set(qurt_anysignal_t *signal, unsigned int mask);
static inline unsigned int qurt_anysignal_get(qurt_anysignal_t *signal)
{
  return qurt_signal_get(signal);
}
unsigned int qurt_anysignal_clear(qurt_anysignal_t *signal, unsigned int mask);
void qurt_rmutex_init(qurt_mutex_t *lock);
void qurt_rmutex_destroy(qurt_mutex_t *lock);
void qurt_rmutex_lock(qurt_mutex_t *lock);
void qurt_rmutex_unlock(qurt_mutex_t *lock);
int qurt_rmutex_try_lock(qurt_mutex_t *lock);
int qurt_rmutex_try_lock_block_once(qurt_mutex_t *lock);
void qurt_pimutex_init(qurt_mutex_t *lock);
void qurt_pimutex_destroy(qurt_mutex_t *lock);
void qurt_pimutex_lock(qurt_mutex_t *lock);
void qurt_pimutex_unlock(qurt_mutex_t *lock);
int qurt_pimutex_try_lock(qurt_mutex_t *lock);
typedef struct {
   unsigned int cur_mask __attribute__((aligned(8)));
   unsigned int sig_state;
   unsigned int queue;
   unsigned int wait_mask;
} qurt_signal2_t;
void qurt_signal2_init(qurt_signal2_t *signal);
void qurt_signal2_destroy(qurt_signal2_t *signal);
unsigned int qurt_signal2_wait(qurt_signal2_t *signal, unsigned int mask,
                unsigned int attribute);
static inline unsigned int qurt_signal2_wait_any(qurt_signal2_t *signal, unsigned int mask)
{
  return qurt_signal2_wait(signal, mask, 0x00000000);
}
static inline unsigned int qurt_signal2_wait_all(qurt_signal2_t *signal, unsigned int mask)
{
  return qurt_signal2_wait(signal, mask, 0x00000001);
}
void qurt_signal2_set(qurt_signal2_t *signal, unsigned int mask);
unsigned int qurt_signal2_get(qurt_signal2_t *signal);
void qurt_signal2_clear(qurt_signal2_t *signal, unsigned int mask);
int qurt_signal2_wait_cancellable(qurt_signal2_t *signal,
                                  unsigned int mask,
                                  unsigned int attribute,
                                  unsigned int *p_returnmask);
void qurt_pimutex2_init(qurt_rmutex2_t *lock);
void qurt_pimutex2_destroy(qurt_rmutex2_t *lock);
void qurt_pimutex2_lock(qurt_rmutex2_t *lock);
void qurt_pimutex2_unlock(qurt_rmutex2_t *lock);
int qurt_rmutex2_try_lock(qurt_rmutex2_t *lock);
 unsigned int qurt_interrupt_register(int int_num, qurt_anysignal_t *int_signal, int signal_mask);
int qurt_interrupt_acknowledge(int int_num);
unsigned int qurt_interrupt_deregister(int int_num);
 unsigned int qurt_interrupt_enable(int int_num);
 unsigned int qurt_interrupt_disable(int int_num);
unsigned int qurt_interrupt_status(int int_num, int *status);
unsigned int qurt_interrupt_clear(int int_num);
unsigned int qurt_interrupt_get_registered(void);
unsigned int qurt_interrupt_get_config(unsigned int int_num, unsigned int *int_type, unsigned int *int_polarity);
unsigned int qurt_interrupt_set_config(unsigned int int_num, unsigned int int_type, unsigned int int_polarity);
int qurt_interrupt_raise(unsigned int interrupt_num);
void qurt_interrupt_disable_all(void);
int qurt_isr_subcall(void);
void * qurt_lifo_pop(void *freelist);
void qurt_lifo_push(void *freelist, void *buf);
void qurt_lifo_remove(void *freelist, void *buf);
static inline int qurt_power_shutdown_prepare(void){ return 0;}
int qurt_power_shutdown_enter (int type);
int qurt_power_exit(void);
int qurt_power_apcr_enter (void);
int qurt_power_tcxo_prepare (void);
int qurt_power_tcxo_fail_exit (void);
int qurt_power_tcxo_enter (void);
int qurt_power_tcxo_exit (void);
void qurt_power_override_wait_for_idle(int enable);
void qurt_power_wait_for_idle (void);
void qurt_power_wait_for_active (void);
unsigned int qurt_system_ipend_get (void);
void qurt_system_avscfg_set(unsigned int avscfg_value);
unsigned int qurt_system_avscfg_get(void);
unsigned int qurt_system_vid_get(void);
int qurt_power_shutdown_get_pcycles( unsigned long long *enter_pcycles, unsigned long long *exit_pcycles );
int qurt_system_tcm_set_size(unsigned int new_size);
int qurt_power_shutdown_get_hw_ticks( unsigned long long *before_pc_ticks, unsigned long long *after_wb_ticks );
typedef struct qurt_sysenv_swap_pools {
   unsigned int spoolsize;
   unsigned int spooladdr;
}qurt_sysenv_swap_pools_t;
typedef struct qurt_sysenv_app_heap {
   unsigned int heap_base;
   unsigned int heap_limit;
} qurt_sysenv_app_heap_t ;
typedef struct qurt_sysenv_arch_version {
    unsigned int arch_version;
}qurt_arch_version_t;
typedef struct qurt_sysenv_max_hthreads {
   unsigned int max_hthreads;
}qurt_sysenv_max_hthreads_t;
typedef struct qurt_sysenv_max_pi_prio {
    unsigned int max_pi_prio;
}qurt_sysenv_max_pi_prio_t;
typedef struct qurt_sysenv_timer_hw {
   unsigned int base;
   unsigned int int_num;
}qurt_sysenv_hw_timer_t;
typedef struct qurt_sysenv_procname {
   unsigned int asid;
   char name[64];
}qurt_sysenv_procname_t;
typedef struct qurt_sysenv_stack_profile_count {
   unsigned int count;
}qurt_sysenv_stack_profile_count_t;
typedef struct _qurt_sysevent_error_t
{
    unsigned int thread_id;
    unsigned int fault_pc;
    unsigned int sp;
    unsigned int badva;
    unsigned int cause;
    unsigned int ssr;
    unsigned int fp;
    unsigned int lr;
} qurt_sysevent_error_t ;
typedef struct qurt_sysevent_pagefault {
    qurt_thread_t thread_id;
    unsigned int fault_addr;
    unsigned int ssr_cause;
} qurt_sysevent_pagefault_t ;
int qurt_sysenv_get_swap_spool0 (qurt_sysenv_swap_pools_t *pools );
int qurt_sysenv_get_swap_spool1(qurt_sysenv_swap_pools_t *pools );
int qurt_sysenv_get_app_heap(qurt_sysenv_app_heap_t *aheap );
int qurt_sysenv_get_hw_timer(qurt_sysenv_hw_timer_t *timer );
int qurt_sysenv_get_arch_version(qurt_arch_version_t *vers);
int qurt_sysenv_get_max_hw_threads(qurt_sysenv_max_hthreads_t *mhwt );
int qurt_sysenv_get_max_pi_prio(qurt_sysenv_max_pi_prio_t *mpip );
int qurt_sysenv_get_process_name(qurt_sysenv_procname_t *pname );
int qurt_sysenv_get_stack_profile_count(qurt_sysenv_stack_profile_count_t *count );
unsigned int qurt_exception_wait (unsigned int *ip, unsigned int *sp,
                                  unsigned int *badva, unsigned int *cause);
unsigned int qurt_exception_wait_ext (qurt_sysevent_error_t * sys_err);
static inline unsigned int qurt_exception_wait2(qurt_sysevent_error_t * sys_err)
{
   return qurt_exception_wait_ext(sys_err);
}
int qurt_exception_raise_nonfatal (int error) __attribute__((noreturn));
void qurt_exception_raise_fatal (void);
void qurt_exception_shutdown_fatal(void) __attribute__((noreturn));
void qurt_exception_shutdown_fatal2(void);
unsigned int qurt_exception_register_fatal_notification ( void(*entryfuncpoint)(void *), void *argp);
unsigned int qurt_enable_floating_point_exception(unsigned int mask);
static inline unsigned int qurt_exception_enable_fp_exceptions(unsigned int mask)
{
   return qurt_enable_floating_point_exception(mask);
}
unsigned int qurt_exception_wait_pagefault (qurt_sysevent_pagefault_t *sys_pagefault);
void qurt_pmu_set (int reg_id, unsigned int reg_value);
unsigned int qurt_pmu_get (int red_id);
void qurt_pmu_enable (int enable);
int qurt_tlb_entry_create (unsigned int *entry_id, qurt_addr_t vaddr, qurt_paddr_t paddr, qurt_size_t size, qurt_mem_cache_mode_t cache_attribs, qurt_perm_t perms, int asid);
int qurt_tlb_entry_create_64 (unsigned int *entry_id, qurt_addr_t vaddr, qurt_paddr_64_t paddr_64, qurt_size_t size, qurt_mem_cache_mode_t cache_attribs, qurt_perm_t perms, int asid);
int qurt_tlb_entry_delete (unsigned int entry_id);
int qurt_tlb_entry_query (unsigned int *entry_id, qurt_addr_t vaddr, int asid);
int qurt_tlb_entry_set (unsigned int entry_id, unsigned long long int entry);
int qurt_tlb_entry_get (unsigned int entry_id, unsigned long long int *entry);
unsigned short qurt_tlb_entry_get_available(void);
unsigned int qurt_tlb_get_pager_physaddr(unsigned int** pager_phys_addrs);
int qurt_qdi_qhi3(int,int,int);
int qurt_qdi_qhi4(int,int,int,int);
int qurt_qdi_qhi5(int,int,int,int,int);
int qurt_qdi_qhi6(int,int,int,int,int,int);
int qurt_qdi_qhi7(int,int,int,int,int,int,int);
int qurt_qdi_qhi8(int,int,int,int,int,int,int,int);
int qurt_qdi_qhi9(int,int,int,int,int,int,int,int,int);
int qurt_qdi_qhi10(int,int,int,int,int,int,int,int,int,int);
int qurt_qdi_qhi11(int,int,int,int,int,int,int,int,int,int,int);
int qurt_qdi_qhi12(int,int,int,int,int,int,int,int,int,int,int,int);
int qurt_qdi_write(int handle, const void *buf, unsigned len);
int qurt_qdi_read(int handle, void *buf, unsigned len);
int qurt_qdi_close(int handle);
extern int qurt_sysclock_register (qurt_anysignal_t *signal, unsigned int signal_mask);
extern unsigned long long qurt_sysclock_alarm_create (int id, unsigned long long ref_count, unsigned long long match_value);
extern int qurt_sysclock_timer_create (int id, unsigned long long duration);
extern unsigned long long qurt_sysclock_get_expiry (void);
unsigned long long qurt_sysclock_get_hw_ticks (void);
extern int qurt_timer_base __attribute__((section(".data.qurt_timer_base")));
static inline unsigned long qurt_sysclock_get_hw_ticks_32 (void)
{
    return (volatile unsigned long)(*((unsigned long *)((unsigned int)qurt_timer_base+0x1000)));
}
static inline unsigned short qurt_sysclock_get_hw_ticks_16 (void)
{
    unsigned long ticks;
    ticks = (volatile unsigned long)(*((unsigned long *)((unsigned int)qurt_timer_base+0x1000)));
    __asm__ __volatile__ ( "%0 = lsr(%0, #16) \n" :"+r"(ticks));
    return (unsigned short)ticks;
}
unsigned long long qurt_timer_timetick_to_us(unsigned long long ticks);
int qurt_spawn_flags(const char * name, int flags);
int qurt_space_switch(int asid);
int qurt_wait(int *status);
int qurt_event_register(int type, int value, qurt_signal_t *signal, unsigned int mask, void *data, unsigned int data_size);
typedef struct _qurt_process_attr {
    char name[64];
    int flags;
} qurt_process_attr_t;
int qurt_process_create (qurt_process_attr_t *attr);
int qurt_process_get_id (void);
static inline void qurt_process_attr_init (qurt_process_attr_t *attr)
{
    attr->name[0] = 0;
    attr->flags = 0;
}
static inline void qurt_process_attr_set_executable (qurt_process_attr_t *attr, char *name)
{
    strlcpy (attr->name, name, 64);
}
static inline void qurt_process_attr_set_flags (qurt_process_attr_t *attr, int flags)
{
    attr->flags = flags;
}
void qurt_process_cmdline_get(char *buf, unsigned buf_siz);
typedef unsigned int mode_t;
int shm_open(const char * name, int oflag, mode_t mode);
void *shm_mmap(void *addr, unsigned int len, int prot, int flags, int fd, unsigned int offset);
int shm_close(int fd);
typedef enum
{
  QURT_TIMER_ONESHOT = 0,
  QURT_TIMER_PERIODIC
} qurt_timer_type_t;
typedef unsigned int qurt_timer_t;
typedef unsigned long long qurt_timer_duration_t;
typedef unsigned long long qurt_timer_time_t;
typedef struct
{
    unsigned int magic;
    qurt_timer_duration_t duration;
    qurt_timer_time_t expiry;
    qurt_timer_duration_t remaining;
    qurt_timer_type_t type;
    unsigned int group;
}
qurt_timer_attr_t;
int qurt_timer_stop (qurt_timer_t timer);
int qurt_timer_restart (qurt_timer_t timer, qurt_timer_duration_t duration);
int qurt_timer_create (qurt_timer_t *timer, const qurt_timer_attr_t *attr,
                  const qurt_anysignal_t *signal, unsigned int mask);
int qurt_timer_create_sig2 (qurt_timer_t *timer, const qurt_timer_attr_t *attr,
                  const qurt_signal2_t *signal, unsigned int mask);
void qurt_timer_attr_init(qurt_timer_attr_t *attr);
void qurt_timer_attr_set_duration(qurt_timer_attr_t *attr, qurt_timer_duration_t duration);
void qurt_timer_attr_set_expiry(qurt_timer_attr_t *attr, qurt_timer_time_t time);
void qurt_timer_attr_get_duration(qurt_timer_attr_t *attr, qurt_timer_duration_t *duration);
void qurt_timer_attr_get_remaining(qurt_timer_attr_t *attr, qurt_timer_duration_t *remaining);
void qurt_timer_attr_set_type(qurt_timer_attr_t *attr, qurt_timer_type_t type);
void qurt_timer_attr_get_type(qurt_timer_attr_t *attr, qurt_timer_type_t *type);
void qurt_timer_attr_set_group(qurt_timer_attr_t *attr, unsigned int group);
void qurt_timer_attr_get_group(qurt_timer_attr_t *attr, unsigned int *group);
int qurt_timer_get_attr(qurt_timer_t timer, qurt_timer_attr_t *attr);
int qurt_timer_delete(qurt_timer_t timer);
int qurt_timer_sleep(qurt_timer_duration_t duration);
int qurt_timer_group_disable (unsigned int group);
int qurt_timer_group_enable (unsigned int group);
void qurt_timer_recover_pc (void);
static inline int qurt_timer_is_init (void) {return 1;};
unsigned long long qurt_timer_get_ticks (void);
int qurt_tls_create_key (int *key, void (*destructor)(void *));
int qurt_tls_set_specific (int key, const void *value);
void *qurt_tls_get_specific (int key);
int qurt_tls_delete_key (int key);
static inline int qurt_thread_iterator_create(void)
{
   return qurt_qdi_qhi3(0,4,68);
}
static inline qurt_thread_t qurt_thread_iterator_next(int iter)
{
   return qurt_qdi_qhi3(0,iter,69);
}
static inline int qurt_thread_iterator_destroy(int iter)
{
   return qurt_qdi_close(iter);
}
int qurt_thread_context_get_tname(unsigned int thread_id, char *name, unsigned char max_len);
int qurt_thread_context_get_prio(unsigned int thread_id, unsigned char *prio);
int qurt_thread_context_get_pcycles(unsigned int thread_id, unsigned long long int *pcycles);
int qurt_thread_context_get_stack_base(unsigned int thread_id, unsigned int *sbase);
int qurt_thread_context_get_stack_size(unsigned int thread_id, unsigned int *ssize);
int qurt_thread_context_get_pid(unsigned int thread_id, unsigned int *pid);
int qurt_thread_context_get_pname(unsigned int thread_id, char *name, unsigned int len);
typedef enum {
    QURT_HVX_MODE_64B = 0,
    QURT_HVX_MODE_128B = 1
} qurt_hvx_mode_t;
int qurt_hvx_lock(qurt_hvx_mode_t lock_mode);
int qurt_hvx_unlock(void);
int qurt_hvx_try_lock(qurt_hvx_mode_t lock_mode);
int qurt_hvx_get_mode(void);
int qurt_hvx_get_units(void);
int qurt_hvx_reserve(int num_units);
int qurt_hvx_cancel_reserve(void);
int qurt_hvx_get_lock_val(void);
typedef enum {
        QURT_MAILBOX_AT_QURTOS=0,
        QURT_MAILBOX_AT_ROOTPD=1,
        QURT_MAILBOX_AT_USERPD=2,
        QURT_MAILBOX_AT_SECUREPD=3,
} qurt_mailbox_receiver_cfg_t;
typedef enum {
        QURT_MAILBOX_SEND_OVERWRITE=0,
        QURT_MAILBOX_SEND_NON_OVERWRITE=1,
} qurt_mailbox_send_option_t;
typedef enum {
        QURT_MAILBOX_RECV_WAITING=0,
        QURT_MAILBOX_RECV_NON_WAITING=1,
        QURT_MAILBOX_RECV_PEEK_NON_WAITING=2,
} qurt_mailbox_recv_option_t;
unsigned long long qurt_mailbox_create(char *name, qurt_mailbox_receiver_cfg_t recv_opt);
unsigned long long qurt_mailbox_get_id(char *name);
int qurt_mailbox_send(unsigned long long mailbox_id, qurt_mailbox_send_option_t send_opt, unsigned long long data);
int qurt_mailbox_receive(unsigned long long mailbox_id, qurt_mailbox_recv_option_t recv_opt, unsigned long long *data);
int qurt_mailbox_delete(unsigned long long mailbox_id);
int qurt_mailbox_receive_halt(unsigned long long mailbox_id);
enum qurt_island_attr_resource_type {
    QURT_ISLAND_ATTR_INVALID,
    QURT_ISLAND_ATTR_END_OF_LIST = QURT_ISLAND_ATTR_INVALID,
    QURT_ISLAND_ATTR_INT,
    QURT_ISLAND_ATTR_THREAD,
    QURT_ISLAND_ATTR_MEMORY
};
typedef struct qurt_island_attr_resource {
    enum qurt_island_attr_resource_type type;
    union {
        struct {
            qurt_addr_t base_addr;
            qurt_size_t size;
        } memory;
        unsigned int interrupt;
        qurt_thread_t thread_id;
    };
} qurt_island_attr_resource_t;
typedef struct qurt_island_attr {
    int max_attrs;
    struct qurt_island_attr_resource attrs[1];
} qurt_island_attr_t;
typedef struct {
   int qdi_handle;
} qurt_island_t;
int qurt_island_attr_create (qurt_island_attr_t **attr, int max_attrs);
void qurt_island_attr_delete (qurt_island_attr_t *attr);
int qurt_island_attr_add (qurt_island_attr_t *attr, qurt_island_attr_resource_t *resources);
int qurt_island_attr_add_interrupt (qurt_island_attr_t *attr, unsigned int interrupt);
int qurt_island_attr_add_mem (qurt_island_attr_t *attr, qurt_addr_t base_addr, qurt_size_t size);
int qurt_island_attr_add_thread (qurt_island_attr_t *attr, qurt_thread_t thread_id);
int qurt_island_spec_create (qurt_island_t *spec_id, qurt_island_attr_t *attr);
int qurt_island_spec_delete (qurt_island_t spec_id);
int qurt_island_enter (qurt_island_t spec_id);
int qurt_island_exit (void);
unsigned int qurt_island_exception_wait (unsigned int *ip, unsigned int *sp,
                                         unsigned int *badva, unsigned int *cause);
unsigned int qurt_island_get_status (void);
typedef struct
{
  msg_desc_type desc;
  const char* fmt;
  const char* fname;
}
msg_const_type;
typedef struct
{
  msg_hdr_type hdr;
  const msg_const_type* const_data_ptr;
  uint32 args[1];
}
msg_ext_store_type;
void msg_init(void);
void msg_send(const msg_const_type* xx_msg_const_ptr);
void msg_send_1(const msg_const_type* xx_msg_const_ptr, uint32 xx_arg1);
void msg_send_2(const msg_const_type* xx_msg_const_ptr, uint32 xx_arg1,
                uint32 xx_arg2);
void msg_send_3(const msg_const_type* xx_msg_const_ptr, uint32 xx_arg1,
                uint32 xx_arg2, uint32 xx_arg3);
void msg_send_var(const msg_const_type* xx_msg_const_ptr, uint32 num_args,
                  ...);
void msg_sprintf(const msg_const_type* const_blk, ...);
void msg_send_ts(const msg_const_type* const_blk, uint64 timestamp);
void msg_save_3(const msg_const_type* const_blk,
                uint32 xx_arg1, uint32 xx_arg2, uint32 xx_arg3,
                msg_ext_store_type* msg);
void msg_errlog_3(const msg_const_type* const_blk, uint32 code1, uint32 code2, uint32 code3);
void msg_errlog_2(const msg_const_type* const_blk, uint32 code1, uint32 code2);
void msg_errlog_1(const msg_const_type* const_blk, uint32 code1);
void msg_errlog_0(const msg_const_type* const_blk);
boolean msg_status(uint16 ss_id, uint32 ss_mask);
void adsppm_lock(DALSYSSyncHandle lock);
void adsppm_unlock(DALSYSSyncHandle lock);
ULogHandle GetUlogHandle(void);
const char *adsppm_getBusPortName(AdsppmBusPortIdType busPort);
<driver name="NULL">
   <device id="/core/power/adsppm">
     <props name="DEBUG_LEVEL" type=0x00000002>
           3
     </props>
     <props name="THREAD_NUMBER" type=0x00000002>
           2
     </props>
     <props name="OVERHANG_VOTE_TIMEOUT_MS" type=0x00000002>
           5
     </props>
     <props name="LPASS_REG_RANGE" type=0x00000012>
           lpassRegRange_8996
     </props>
     <props name="L2_CONFIG_REG_RANGE" type=0x00000012>
           l2ConfigRegRange_8996
     </props>
     <props name="CORE_DESCRIPTORS" type=0x00000012>
           cores_8996
     </props>
     <props name="MEMORY_DESCRIPTORS" type=0x00000012>
           memories_8996
     </props>
     <props name="CLOCK_DESCRIPTORS" type=0x00000012>
           clocks_8996
     </props>
     <props name="BUS_PORT_DESCRIPTORS" type=0x00000012>
           busPorts_8996
     </props>
     <props name="EXTERNAL_BUS_ROUTES" type=0x00000012>
           extBusRoutes_8996
     </props>
     <props name="MIPS_BUS_ROUTES" type=0x00000012>
           mipsBwRoutes_8996
     </props>
     <props name="REGISTER_PROGRAMMING_SPEEDS" type=0x00000012>
           regProgSpeeds_8996
     </props>
     <props name="POWER_DOMAIN_DESCRIPTORS" type=0x00000012>
           pwrDomains_8996
     </props>
     <props name="FEATURE_DESCRIPTORS" type=0x00000012>
           features_8996
     </props>
     <props name="COMPENSATED_DDR_BW_TABLE" type=0x00000012>
          compensatedDdrBwTable_8996
     </props>
     <props name="COMPENSATED_AHB_BW_TABLE" type=0x00000012>
          compensatedAhbBwTable_8996
     </props>
     <props name="CACHE_PARTITION_CONFIG_TABLE" type=0x00000012>
          cachePartitionConfigTable_8996
     </props>
     <props name="BW_CONCURRENCY_SETTINGS" type=0x00000012>
            bwConcurrencySettings_8996
     </props>
     <props name="THREAD_LOADING_DATA" type=0x00000012>
            threadLoadingData_8996
     </props>
     <props name="CPP_FACTORS" type=0x00000012>
            audioVoiceCppFactors_8996
     </props>
     <props name="ADSP_TO_AHBE_FREQ_TABLE_V1" type=0x00000012>
            adspToAhbeFreqTable_8996v1
     </props>
     <props name="ADSP_TO_AHBE_FREQ_TABLE_V2" type=0x00000012>
            adspToAhbeFreqTable_8996v2
     </props>
     <props name="ADSP_TO_AHBE_FREQ_TABLE_V3" type=0x00000012>
            adspToAhbeFreqTable_8996v3
     </props>
     <props name="ADSP_CACHE_SIZE_BW_SCALING_TABLE" type=0x00000012>
            adspcachesizebwscaling_8996
     </props>
   </device>
</driver>

<!--
  This XML file contains the vMPM device ID
  Copyright (c) 2014 QUALCOMM Technologies Inc. (QTI)
  All Rights Reserved.
  Qualcomm Confidential and Proprietary
-->
<driver name="mpm">
  <device id="/dev/mpm">
    <props name="IsRemotable" type=0x00000002> 0x1 </props>
  </device>
</driver>

enum_header_path "DDIInterruptController.h"
<!--
  This XML file contains target specific information for MPM
  driver for targets using device config feature.
  Copyright (c) 2013-2014 QUALCOMM Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary
-->
typedef unsigned long int uint32;
typedef unsigned short uint16;
typedef unsigned char uint8;
typedef signed long int int32;
typedef signed short int16;
typedef signed char int8;
typedef unsigned long long uint64;
typedef long long int64;
typedef unsigned char byte;
typedef uint32 DALBOOL;
typedef uint32 DALDEVICEID;
typedef uint32 DalPowerCmd;
typedef uint32 DalPowerDomain;
typedef uint32 DalSysReq;
typedef uint32 DALHandle;
typedef int DALResult;
typedef void * DALEnvHandle;
typedef void * DALSYSEventHandle;
typedef uint32 DALMemAddr;
typedef uint32 DALSYSMemAddr;
typedef uint64 DALSYSPhyAddr;
typedef uint32 DALInterfaceVersion;
typedef unsigned char * DALDDIParamPtr;
typedef struct DALEventObject DALEventObject;
struct DALEventObject
{
    uint32 obj[8];
};
typedef DALEventObject * DALEventHandle;
typedef struct _DALMemObject
{
   uint32 memAttributes;
   uint32 sysObjInfo[2];
   uint32 dwLen;
   uint32 ownerVirtAddr;
   uint32 virtAddr;
   uint32 physAddr;
}
DALMemObject;
typedef struct _DALDDIMemBufDesc
{
   uint32 dwOffset;
   uint32 dwLen;
   uint32 dwUser;
}
DALDDIMemBufDesc;
typedef struct _DALDDIMemDescList
{
   uint32 dwFlags;
   uint32 dwNumBufs;
   DALDDIMemBufDesc bufList[1];
}
DALDDIMemDescList;
typedef struct DALSysMemDescBuf DALSysMemDescBuf;
struct DALSysMemDescBuf
{
   DALSYSMemAddr VirtualAddr;
   DALSYSMemAddr PhysicalAddr;
   uint32 size;
   uint32 user;
};
typedef struct { uint32 dwObjInfo; DALSYSMemAddr thisVirtualAddr; DALSYSMemAddr PhysicalAddr; DALSYSMemAddr VirtualAddr; uint32 hOwnerProc; uint32 dwCurBufIdx; uint32 dwNumDescBufs; DALSysMemDescBuf BufInfo[1];} DALSysMemDescList;
typedef struct DalDeviceInfo DalDeviceInfo;
struct DalDeviceInfo
{
   uint32 sizeOfActual;
   uint32 Version;
   char Name[32];
};
typedef struct DalDeviceStatus DalDeviceStatus;
struct DalDeviceStatus
{
   uint32 sizeOfActual;
   uint32 Status;
};
typedef struct DalDeviceHandle DalDeviceHandle;
typedef struct DalDevice DalDevice;
struct DalDevice
{
   DALResult (*Attach)(const char*,DALDEVICEID,DalDeviceHandle **);
   uint32 (*Detach)(DalDeviceHandle *);
   DALResult (*Init)(DalDeviceHandle *);
   DALResult (*DeInit)(DalDeviceHandle *);
   DALResult (*Open)(DalDeviceHandle *, uint32);
   DALResult (*Close)(DalDeviceHandle *);
   DALResult (*Info)(DalDeviceHandle *, DalDeviceInfo *, uint32);
   DALResult (*PowerEvent)(DalDeviceHandle *, DalPowerCmd, DalPowerDomain);
   DALResult (*SysRequest)(DalDeviceHandle *, DalSysReq, const void *, uint32,
                           void *,uint32, uint32*);
};
typedef struct DalInterface DalInterface;
struct DalInterface
{
   struct DalDevice DalDevice;
};
struct DalDeviceHandle
{
   uint32 dwDalHandleId;
   const DalInterface *pVtbl;
   void *pClientCtxt;
   uint32 dwVtblLen;
};
typedef struct DalDeviceHandle * DALDEVICEHANDLE;
typedef struct DalRemoteHandle DalRemoteHandle;
typedef struct DalRemote DalRemote;
struct DalRemote
{
   struct DalDevice DalDevice;
   DALResult (* FCN_0) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1 );
   DALResult (* FCN_1) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32 u2 );
   DALResult (* FCN_2) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32* p_u2 );
   DALResult (* FCN_3) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32 u2, uint32 u3 );
   DALResult (* FCN_4) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32 u2, uint32* p_u3 );
   DALResult (* FCN_5) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen);
   DALResult (* FCN_6) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * ibuf, uint32 ilen);
   DALResult (* FCN_7) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * obuf, uint32 olen, uint32 *oalen);
   DALResult (* FCN_8) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * obuf, uint32 olen);
   DALResult (* FCN_9) ( uint32 ddi_idx, DalDeviceHandle *h, void * obuf, uint32 olen );
   DALResult (* FCN_10)( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * ibuf, uint32 ilen, void * obuf, uint32 olen, uint32 * oalen);
   DALResult (* FCN_11) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * obuf, uint32 olen);
   DALResult (* FCN_12) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * obuf, uint32 olen, uint32 *oalen);
   DALResult (* FCN_13) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * ibuf2, uint32 ilen2, void * obuf, uint32 olen);
   DALResult (* FCN_14) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * obuf1, uint32 olen, void * obuf2, uint32 olen2, uint32 * oalen);
   DALResult (* FCN_15) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * ibuf2, uint32 ilen2, void * obuf2, uint32 olen2, uint32 *oalen, void * obuf, uint32 olen );
};
struct DalRemoteHandle
{
   uint32 dwDalHandleId;
   const DalRemote *pVtbl;
   void *pClientCtxt;
};
static __inline uint32
DalDevice_Detach(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Detach(_h);
}
static __inline DALResult
DalDevice_Init(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Init(_h);
}
static __inline DALResult
DalDevice_DeInit(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.DeInit(_h);
}
static __inline DALResult
DalDevice_PowerEvent(DalDeviceHandle *_h, DalPowerCmd PowerCmd,
                     DalPowerDomain PowerDomain)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.PowerEvent(_h,PowerCmd,PowerDomain);
}
static __inline DALResult
DalDevice_Open(DalDeviceHandle *_h, uint32 mode)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Open(_h,mode);
}
static __inline DALResult
DalDevice_Close(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Close(_h);
}
static __inline DALResult
DalDevice_Info(DalDeviceHandle *_h, DalDeviceInfo* info, uint32 infoSize)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Info(_h,info,infoSize);
}
static __inline DALResult
DalDevice_SysRequest(DalDeviceHandle *_h, DalSysReq ReqIdx,
                     const unsigned char* SrcBuf, int SrcBufLen,
                     unsigned char* DestBuf, uint32 DestBufLen, uint32* DestBufLenReq)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.SysRequest(_h,ReqIdx,SrcBuf,SrcBufLen,
                                          DestBuf,DestBufLen,DestBufLenReq);
}
DALResult
DAL_DeviceAttach(DALDEVICEID DeviceId,
                 DalDeviceHandle **phDevice);
DALResult
DAL_DeviceAttachLocal(const char *pszArg,DALDEVICEID DeviceId,
                      DalDeviceHandle **phDevice);
DALResult
DAL_DeviceAttachEx(const char *pszArg, DALDEVICEID DeviceId,
               DALInterfaceVersion ClientVersion,DalDeviceHandle **phDevice);
DALResult
DAL_StringDeviceAttachEx(const char *pszArg,
                   const char *pszDevName,
                   DALInterfaceVersion ClientVersion,
                   DalDeviceHandle **phDalDevice);
DALResult
DAL_DeviceAttachRemote(const char *pszArg,
                   DALDEVICEID DevId,
                   DALInterfaceVersion ClientVersion,
                   DalDeviceHandle **phDALDevice);
DALResult
DAL_DeviceDetach(DalDeviceHandle *hDevice);
DALResult
DAL_StringDeviceAttach(const char *pszDevName,DalDeviceHandle **phDalDevice);
typedef uint32 DALInterruptID;
typedef void * DALISRCtx;
typedef void * (*DALISR)(DALISRCtx);
typedef void * DALIRQCtx;
typedef void * (*DALIRQ)(DALIRQCtx);
typedef enum
{
  DALINTCNTLR_NO_POWER_COLLAPSE,
  DALINTCNTLR_POWER_COLLAPSE,
  PLACEHOLDER_InterruptControllerSleepType = 0x7fffffff
} InterruptControllerSleepType;
typedef struct DalInterruptController DalInterruptController;
struct DalInterruptController
{
   struct DalDevice DalDevice;
   DALResult (*RegisterISR)(DalDeviceHandle * _h, DALInterruptID intrID,
                           const DALISR isr, const DALISRCtx ctx, uint32 bEnable);
   DALResult (*RegisterIST)(DalDeviceHandle * _h, DALInterruptID intrID,
                           const DALISR isr, const DALISRCtx ctx, uint32 bEnable,char* pISTName);
   DALResult (*RegisterEvent)(DalDeviceHandle * _h, DALInterruptID intrID,
                             const DALSYSEventHandle hEvent, uint32 bEnable);
   DALResult (*Unregister)(DalDeviceHandle * _h, DALInterruptID intrID);
   DALResult (*InterruptDone)(DalDeviceHandle * _h, DALInterruptID intrID);
   DALResult (*InterruptEnable)(DalDeviceHandle * _h, DALInterruptID intrID);
   DALResult (*InterruptDisable)(DalDeviceHandle * _h, DALInterruptID intrID);
   DALResult (*InterruptTrigger)(DalDeviceHandle * _h, DALInterruptID intrID);
   DALResult (*InterruptClear)(DalDeviceHandle * _h, DALInterruptID intrID);
   DALResult (*InterruptStatus)(DalDeviceHandle * _h, DALInterruptID intrID);
   DALResult (*RegisterIRQHandler)(DalDeviceHandle * _h, DALInterruptID intrID,
                           const DALIRQ irq, const DALIRQCtx ctx, uint32 bEnable);
  DALResult (*SetInterruptTrigger)(DalDeviceHandle * _h, DALInterruptID intrID, uint32 nTrigger);
  DALResult (*IsInterruptPending)(DalDeviceHandle * _h, DALInterruptID intrID,void* bState,uint32 size);
  DALResult (*IsInterruptEnabled)(DalDeviceHandle * _h, DALInterruptID intrID,void* bState,uint32 size);
  DALResult (*MapWakeupInterrupt)(DalDeviceHandle * _h, DALInterruptID intrID,uint32 nWakeupIntID);
  DALResult (*IsAnyInterruptPending)(DalDeviceHandle * _h, uint32* bState,uint32 size);
  DALResult (*Sleep)(DalDeviceHandle * _h, InterruptControllerSleepType sleep);
  DALResult (*Wakeup)(DalDeviceHandle * _h, InterruptControllerSleepType sleep);
  DALResult (*GetInterruptTrigger)(DalDeviceHandle * _h, DALInterruptID intrID,uint32* eTrigger, uint32 size);
  DALResult (*GetInterruptID)(DalDeviceHandle * _h, const char *szIntrName, uint32* pnIntrID, uint32 size);
  DALResult (*LogState)(DalDeviceHandle * _h, void *pULog);
};
typedef struct DalInterruptControllerHandle DalInterruptControllerHandle;
struct DalInterruptControllerHandle
{
   uint32 dwDalHandleId;
   const DalInterruptController * pVtbl;
   void * pClientCtxt;
};
static __inline DALResult
DalInterruptController_RegisterISR(DalDeviceHandle * _h, DALInterruptID intrID,
                           const DALISR isr, const DALISRCtx ctx, uint32 IntrFlags)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->RegisterISR( _h, intrID,
                                                                   isr, ctx, IntrFlags);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
DalInterruptController_RegisterIST(DalDeviceHandle * _h, DALInterruptID intrID,
                           const DALISR isr, const DALISRCtx ctx, uint32 IntrFlags, char* pISTName)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->RegisterIST( _h, intrID,
                                                                   isr, ctx, IntrFlags,pISTName);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
DalInterruptController_RegisterEvent(DalDeviceHandle * _h, DALInterruptID intrID,
                            const DALSYSEventHandle hEvent, uint32 IntrFlags)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->RegisterEvent( _h, intrID,
                                                                        hEvent, IntrFlags);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
DalInterruptController_Unregister(DalDeviceHandle * _h, DALInterruptID intrID)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->Unregister( _h, intrID);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
DalInterruptController_InterruptDone(DalDeviceHandle * _h, DALInterruptID intrID)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->InterruptDone( _h, intrID);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
DalInterruptController_InterruptEnable(DalDeviceHandle * _h, DALInterruptID intrID)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->InterruptEnable( _h, intrID);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
DalInterruptController_InterruptDisable(DalDeviceHandle * _h, DALInterruptID intrID)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->InterruptDisable( _h, intrID);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
DalInterruptController_InterruptTrigger(DalDeviceHandle * _h, DALInterruptID intrID)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->InterruptTrigger( _h, intrID);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
DalInterruptController_InterruptClear(DalDeviceHandle * _h, DALInterruptID intrID)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->InterruptClear( _h, intrID);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
DalInterruptController_InterruptStatus
(
  DalDeviceHandle * _h,
  DALInterruptID intrID
)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->InterruptStatus( _h, intrID);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
DalInterruptController_RegisterIRQHandler(DalDeviceHandle * _h, DALInterruptID intrID,const DALIRQ irq, const DALIRQCtx ctx, uint32 bEnable)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->RegisterIRQHandler( _h, intrID,
                                                                   irq, ctx, bEnable);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
DalInterruptController_SetTrigger(DalDeviceHandle * _h, DALInterruptID intrID,uint32 nTrigger)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->SetInterruptTrigger( _h,
            intrID, nTrigger);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
DalInterruptController_IsInterruptPending(DalDeviceHandle * _h, DALInterruptID intrID,uint32* bState)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->IsInterruptPending( _h,
            intrID, (void*)bState,1);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
DalInterruptController_IsInterruptEnabled(DalDeviceHandle * _h, DALInterruptID intrID,uint32* bState)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->IsInterruptEnabled( _h,
           intrID, (void*)bState,1);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
DalInterruptController_MapWakeupInterrupt(DalDeviceHandle * _h, DALInterruptID intrID, uint32 WakeupIntID)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->MapWakeupInterrupt( _h,
           intrID, WakeupIntID);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
DalInterruptController_IsAnyInterruptPending(DalDeviceHandle * _h, uint32* bState)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->IsAnyInterruptPending( _h,
           (uint32*)bState,1);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
DalInterruptController_Sleep(DalDeviceHandle * _h,InterruptControllerSleepType sleep)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->Sleep( _h, sleep);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
DalInterruptController_Wakeup(DalDeviceHandle * _h, InterruptControllerSleepType sleep)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->Wakeup( _h, sleep);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
DalInterruptController_GetInterruptTrigger(DalDeviceHandle * _h, DALInterruptID intrID,uint32* eTrigger)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->GetInterruptTrigger( _h,
             intrID,eTrigger,1);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
DalInterruptController_GetInterruptID(DalDeviceHandle * _h, const char * szIntrName,DALInterruptID* pnIntrID)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->GetInterruptID( _h, szIntrName, pnIntrID,1);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
DalInterruptController_LogState(DalDeviceHandle * _h, void *pULog)
{
   return ((DalInterruptControllerHandle *)_h)->pVtbl->LogState( _h, pULog);
}
<!--
 * The following physical address for the
 * processor as defined by the RPM. The start address is
 * defined to be at the wakeup_time - not enable request.
 * (see rpm_proc\core\power\mpm\src\vmpm_*.c)
-->
<driver name="NULL">
  <device id="/dev/core/power/mpm">
    <props name="vmpm_pinNumMappingTable" type=0x00000012>
      devcfg_MpmInterruptPinNum_Mapping
    </props>
    <props name="vmpm_procRegionStartPA" type=0x00000002>
      0x0006A1B8
    </props>
  </device>
</driver>

<!--
  This XML file contains target specific information for SPM
  driver for targets using device config feature.
  Copyright (c) 2012 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary
-->
<driver name="NULL">
  <device id="/dev/core/power/spm">
    <props name="spm_num_cores" type=0x00000002>
      1
    </props>
    <props name="spm_bsp_data" type=0x00000012>
      devcfgSpmBspData
    </props>
    <props name="spm_cmd_seq_info_array" type=0x00000012>
      devcfgSpmCmdSeqArray
    </props>
  </device>
</driver>

enum_header_path "diag_f3_trace_devcfg.h"
typedef enum
{
  DIAG_F3_TRACE_DISABLE = 0x0,
  DIAG_F3_TRACE_ENABLE = 0x1,
  DIAG_F3_TRACE_DIAG_MASK_DEBUG_MODE = 0x9,
  SIZEOF_DIAG_F3_TRACE_CONTROL_TYPE
} diag_f3_trace_control_type;
<driver name="NULL">
   <device id="diag_f3_trace">
    <!-- f3trace_control (equivalent to NV 1892 Diag Debug Control)
              DIAG_F3_TRACE_DISABLE (0x0) - logging off
              DIAG_F3_TRACE_ENABLE (0x1) - log filtered by f3trace_detail below
              DIAG_F3_TRACE_DIAG_MASK_DEBUG_MODE (0x9) - log based on diag-QXDM filters
    -->
    <props name="diag_f3_trace_control" type=0x00000002>
      DIAG_F3_TRACE_ENABLE
    </props>
    <!-- f3trace_detail bitmask (equivalent to NV 1895 Diag Debug Detail)
              BIT Enables
               7 MSG_FATAL
               6 MSG_ERROR
               5 MSG_HIGH
               4 MSG_MED
               3 MSG_LOW
               2 <reserved>
               1 timestamp
               0 msg args
    -->
    <props name="diag_f3_trace_detail" type=0x00000002>
      0xFF
    </props>
    <!-- DO NOT MODIFY -->
    <props name="f3trace_devcfg_version" type=0x00000002>
      0x01
    </props>
  </device>
</driver>

enum_header_path "DDIGPIOInt.h"
typedef unsigned long int uint32;
typedef unsigned short uint16;
typedef unsigned char uint8;
typedef signed long int int32;
typedef signed short int16;
typedef signed char int8;
typedef unsigned long long uint64;
typedef long long int64;
typedef unsigned char byte;
typedef uint32 DALBOOL;
typedef uint32 DALDEVICEID;
typedef uint32 DalPowerCmd;
typedef uint32 DalPowerDomain;
typedef uint32 DalSysReq;
typedef uint32 DALHandle;
typedef int DALResult;
typedef void * DALEnvHandle;
typedef void * DALSYSEventHandle;
typedef uint32 DALMemAddr;
typedef uint32 DALSYSMemAddr;
typedef uint64 DALSYSPhyAddr;
typedef uint32 DALInterfaceVersion;
typedef unsigned char * DALDDIParamPtr;
typedef struct DALEventObject DALEventObject;
struct DALEventObject
{
    uint32 obj[8];
};
typedef DALEventObject * DALEventHandle;
typedef struct _DALMemObject
{
   uint32 memAttributes;
   uint32 sysObjInfo[2];
   uint32 dwLen;
   uint32 ownerVirtAddr;
   uint32 virtAddr;
   uint32 physAddr;
}
DALMemObject;
typedef struct _DALDDIMemBufDesc
{
   uint32 dwOffset;
   uint32 dwLen;
   uint32 dwUser;
}
DALDDIMemBufDesc;
typedef struct _DALDDIMemDescList
{
   uint32 dwFlags;
   uint32 dwNumBufs;
   DALDDIMemBufDesc bufList[1];
}
DALDDIMemDescList;
typedef struct DALSysMemDescBuf DALSysMemDescBuf;
struct DALSysMemDescBuf
{
   DALSYSMemAddr VirtualAddr;
   DALSYSMemAddr PhysicalAddr;
   uint32 size;
   uint32 user;
};
typedef struct { uint32 dwObjInfo; DALSYSMemAddr thisVirtualAddr; DALSYSMemAddr PhysicalAddr; DALSYSMemAddr VirtualAddr; uint32 hOwnerProc; uint32 dwCurBufIdx; uint32 dwNumDescBufs; DALSysMemDescBuf BufInfo[1];} DALSysMemDescList;
typedef struct DalDeviceInfo DalDeviceInfo;
struct DalDeviceInfo
{
   uint32 sizeOfActual;
   uint32 Version;
   char Name[32];
};
typedef struct DalDeviceStatus DalDeviceStatus;
struct DalDeviceStatus
{
   uint32 sizeOfActual;
   uint32 Status;
};
typedef struct DalDeviceHandle DalDeviceHandle;
typedef struct DalDevice DalDevice;
struct DalDevice
{
   DALResult (*Attach)(const char*,DALDEVICEID,DalDeviceHandle **);
   uint32 (*Detach)(DalDeviceHandle *);
   DALResult (*Init)(DalDeviceHandle *);
   DALResult (*DeInit)(DalDeviceHandle *);
   DALResult (*Open)(DalDeviceHandle *, uint32);
   DALResult (*Close)(DalDeviceHandle *);
   DALResult (*Info)(DalDeviceHandle *, DalDeviceInfo *, uint32);
   DALResult (*PowerEvent)(DalDeviceHandle *, DalPowerCmd, DalPowerDomain);
   DALResult (*SysRequest)(DalDeviceHandle *, DalSysReq, const void *, uint32,
                           void *,uint32, uint32*);
};
typedef struct DalInterface DalInterface;
struct DalInterface
{
   struct DalDevice DalDevice;
};
struct DalDeviceHandle
{
   uint32 dwDalHandleId;
   const DalInterface *pVtbl;
   void *pClientCtxt;
   uint32 dwVtblLen;
};
typedef struct DalDeviceHandle * DALDEVICEHANDLE;
typedef struct DalRemoteHandle DalRemoteHandle;
typedef struct DalRemote DalRemote;
struct DalRemote
{
   struct DalDevice DalDevice;
   DALResult (* FCN_0) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1 );
   DALResult (* FCN_1) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32 u2 );
   DALResult (* FCN_2) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32* p_u2 );
   DALResult (* FCN_3) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32 u2, uint32 u3 );
   DALResult (* FCN_4) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32 u2, uint32* p_u3 );
   DALResult (* FCN_5) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen);
   DALResult (* FCN_6) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * ibuf, uint32 ilen);
   DALResult (* FCN_7) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * obuf, uint32 olen, uint32 *oalen);
   DALResult (* FCN_8) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * obuf, uint32 olen);
   DALResult (* FCN_9) ( uint32 ddi_idx, DalDeviceHandle *h, void * obuf, uint32 olen );
   DALResult (* FCN_10)( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * ibuf, uint32 ilen, void * obuf, uint32 olen, uint32 * oalen);
   DALResult (* FCN_11) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * obuf, uint32 olen);
   DALResult (* FCN_12) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * obuf, uint32 olen, uint32 *oalen);
   DALResult (* FCN_13) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * ibuf2, uint32 ilen2, void * obuf, uint32 olen);
   DALResult (* FCN_14) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * obuf1, uint32 olen, void * obuf2, uint32 olen2, uint32 * oalen);
   DALResult (* FCN_15) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * ibuf2, uint32 ilen2, void * obuf2, uint32 olen2, uint32 *oalen, void * obuf, uint32 olen );
};
struct DalRemoteHandle
{
   uint32 dwDalHandleId;
   const DalRemote *pVtbl;
   void *pClientCtxt;
};
static __inline uint32
DalDevice_Detach(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Detach(_h);
}
static __inline DALResult
DalDevice_Init(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Init(_h);
}
static __inline DALResult
DalDevice_DeInit(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.DeInit(_h);
}
static __inline DALResult
DalDevice_PowerEvent(DalDeviceHandle *_h, DalPowerCmd PowerCmd,
                     DalPowerDomain PowerDomain)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.PowerEvent(_h,PowerCmd,PowerDomain);
}
static __inline DALResult
DalDevice_Open(DalDeviceHandle *_h, uint32 mode)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Open(_h,mode);
}
static __inline DALResult
DalDevice_Close(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Close(_h);
}
static __inline DALResult
DalDevice_Info(DalDeviceHandle *_h, DalDeviceInfo* info, uint32 infoSize)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Info(_h,info,infoSize);
}
static __inline DALResult
DalDevice_SysRequest(DalDeviceHandle *_h, DalSysReq ReqIdx,
                     const unsigned char* SrcBuf, int SrcBufLen,
                     unsigned char* DestBuf, uint32 DestBufLen, uint32* DestBufLenReq)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.SysRequest(_h,ReqIdx,SrcBuf,SrcBufLen,
                                          DestBuf,DestBufLen,DestBufLenReq);
}
DALResult
DAL_DeviceAttach(DALDEVICEID DeviceId,
                 DalDeviceHandle **phDevice);
DALResult
DAL_DeviceAttachLocal(const char *pszArg,DALDEVICEID DeviceId,
                      DalDeviceHandle **phDevice);
DALResult
DAL_DeviceAttachEx(const char *pszArg, DALDEVICEID DeviceId,
               DALInterfaceVersion ClientVersion,DalDeviceHandle **phDevice);
DALResult
DAL_StringDeviceAttachEx(const char *pszArg,
                   const char *pszDevName,
                   DALInterfaceVersion ClientVersion,
                   DalDeviceHandle **phDalDevice);
DALResult
DAL_DeviceAttachRemote(const char *pszArg,
                   DALDEVICEID DevId,
                   DALInterfaceVersion ClientVersion,
                   DalDeviceHandle **phDALDevice);
DALResult
DAL_DeviceDetach(DalDeviceHandle *hDevice);
DALResult
DAL_StringDeviceAttach(const char *pszDevName,DalDeviceHandle **phDalDevice);
typedef struct DALREG_DriverInfo DALREG_DriverInfo;
struct DALREG_DriverInfo
{
 DALResult (*pfnDALNewFunc)(const char * , DALDEVICEID, DalDeviceHandle**);
 uint32 dwNumDevices;
 DALDEVICEID *pDeviceId;
};
typedef struct DALREG_DriverInfoList DALREG_DriverInfoList;
struct DALREG_DriverInfoList
{
 uint32 dwLen;
 DALREG_DriverInfo ** pDriverInfo;
};
typedef void * DALSYSObjHandle;
typedef void * DALSYSSyncHandle;
typedef void * DALSYSMemHandle;
typedef void * DALSYSWorkLoopHandle;
typedef uint32 *DALSYSPropertyHandle;
typedef struct DALSYSPropertyVar DALSYSPropertyVar;
struct DALSYSPropertyVar
{
    uint32 dwType;
    uint32 dwLen;
    union
    {
        byte *pbVal;
        char *pszVal;
        uint32 dwVal;
        uint32 *pdwVal;
        const void *pStruct;
    }Val;
};
typedef struct DALSYSPropStructTblType DALSYSPropStructTblType ;
struct DALSYSPropStructTblType{
   uint32 dwSize;
   const void *pStruct;
};
typedef struct StringDevice StringDevice;
 struct StringDevice{
   char *pszName;
   uint32 dwHash;
   uint32 dwOffset;
   DALREG_DriverInfo *pFunctionName;
   uint32 dwNumCollision;
   uint32 *pdwCollisions;
};
typedef struct DALProps DALProps;
struct DALProps
{
   const byte *pDALPROP_PropBin;
   const DALSYSPropStructTblType *pDALPROP_StructPtrs;
   const uint32 dwDeviceSize;
   const StringDevice *pDevices;
};
typedef struct DALSYSBaseObj DALSYSBaseObj;
struct DALSYSBaseObj
{
   uint32 dwObjInfo;
   uint32 hOwnerProc;
   DALSYSMemAddr thisVirtualAddr;
};
typedef struct DALSYSEventObj DALSYSEventObj;
struct DALSYSEventObj
{
   unsigned long long _bSpace[80/sizeof(unsigned long long)];
};
typedef struct DALSYSSyncObj DALSYSSyncObj;
struct DALSYSSyncObj
{
   unsigned long long _bSpace[40/sizeof(unsigned long long)];
};
typedef struct DALSYSMemObj DALSYSMemObj;
struct DALSYSMemObj
{
   unsigned long long _bSpace[40/sizeof(unsigned long long)];
};
typedef struct DALSYSWorkLoopEventObj DALSYSWorkLoopEventObj;
struct DALSYSWorkLoopEventObj
{
   unsigned long long _bSpace[32/sizeof(unsigned long long)];
};
typedef struct DALSYSWorkLoopObj DALSYSWorkLoopObj;
struct DALSYSWorkLoopObj
{
   unsigned long long _bSpace[64/sizeof(unsigned long long)];
};
typedef void * DALSYSCallbackFuncCtx;
typedef void * (*DALSYSCallbackFunc)(void*,uint32,void*,uint32);
typedef struct DALSYSMemInfo DALSYSMemInfo;
struct DALSYSMemInfo
{
    DALSYSMemAddr VirtualAddr;
    DALSYSMemAddr PhysicalAddr;
    uint32 dwLen;
    uint32 dwMappedLen;
    uint32 dwProps;
};
typedef enum
{
   DALSYS_MEM_CACHE_DEVICE,
   DALSYS_MEM_CACHE_NONE,
   DALSYS_MEM_CACHE_WRITEBACK,
   DALSYS_MEM_CACHE_WRITETHROUGH,
   DALSYS_MEM_CACHE_INVALID
}
DALSYSMemCacheType;
typedef enum
{
   DALSYS_MEM_PERM_NONE=0,
   DALSYS_MEM_PERM_R=0x1,
   DALSYS_MEM_PERM_W=0x2,
   DALSYS_MEM_PERM_RW=0x3,
   DALSYS_MEM_PERM_X=0x4,
   DALSYS_MEM_PERM_RX=0x5,
   DALSYS_MEM_PERM_WX=0x6,
   DALSYS_MEM_PERM_RWX=0x7,
   DALSYS_MEM_PERM_INVALID=0x8
}
DALSYSMemPermType;
typedef enum
{
   DALSYS_MEM_SHARE_ALLOWED,
   DALSYS_MEM_SHARE_NOT_ALLOWED,
   DALSYS_MEM_SHARE_INVALID
}
DALSYSMemShareType;
typedef struct DALSYSMemInfoEx DALSYSMemInfoEx;
struct DALSYSMemInfoEx
{
    DALSYSPhyAddr physicalAddr;
    DALSYSMemAddr virtualAddr;
    DALSYSMemAddr size;
    DALSYSMemCacheType cacheType;
    DALSYSMemPermType permission;
    DALSYSMemShareType shareType;
};
typedef struct DALSYSMemReq DALSYSMemReq;
struct DALSYSMemReq
{
    DALSYSMemInfoEx memInfo;
    DALSYSMemObj *pObj;
    DALBOOL prealloc;
};
typedef enum
{
   DEVCFG_TARGET_INFO_SOC,
   DEVCFG_TARGET_INFO_PLATFORM
}DEVCFG_TARGET_INFO_TYPE;
typedef DALResult
(*DALSYSWorkLoopExecute)
(
    DALSYSEventHandle,
    void *
);
typedef void
(*DALSYS_InitSystemHandleFncPtr)
(
    DalDeviceHandle * hDalDevice
);
typedef DALResult
(*DALSYS_DestroyObjectFncPtr)
(
    DALSYSObjHandle
);
typedef DALResult
(*DALSYS_CopyObjectFncPtr)
(
    DALSYSObjHandle,
    DALSYSObjHandle *
);
typedef DALResult
(*DALSYS_RegisterWorkLoopFncPtr)
(
    uint32,
    uint32,
    DALSYSWorkLoopHandle *,
    DALSYSWorkLoopObj *
);
typedef DALResult
(*DALSYS_RegisterWorkLoopExFncPtr)
(
    char *,
    uint32,
    uint32,
    uint32,
    DALSYSWorkLoopHandle *,
    DALSYSWorkLoopObj *
);
typedef DALResult
(*DALSYS_AddEventToWorkLoopFncPtr)
(
    DALSYSWorkLoopHandle,
    DALSYSWorkLoopExecute,
    void *,
    DALSYSEventHandle,
    DALSYSSyncHandle
);
typedef DALResult
(*DALSYS_DeleteEventFromWorkLoopFncPtr)
(
    DALSYSWorkLoopHandle,
    DALSYSEventHandle
);
typedef DALResult
(*DALSYS_EventCreateFncPtr)
(
    uint32 ,
    DALSYSEventHandle *,
    DALSYSEventObj *
);
typedef DALResult
(*DALSYS_EventCtrlFncPtr)
(
    DALSYSEventHandle,
    uint32,
   uint32,
   void *,
   uint32
);
typedef DALResult
(*DALSYS_EventWaitFncPtr)
(
    DALSYSEventHandle
);
typedef DALResult
(*DALSYS_EventMultipleWaitFncPtr)
(
    DALSYSEventHandle*,
    int,
    uint32,
    uint32 *
);
typedef DALResult
(*DALSYS_SetupCallbackEventFncPtr)
(
    DALSYSEventHandle,
    DALSYSCallbackFunc,
    DALSYSCallbackFuncCtx
);
typedef DALResult
(*DALSYS_SyncCreateFncPtr)
(
    uint32,
    DALSYSSyncHandle *,
    DALSYSSyncObj *
);
typedef void
(*DALSYS_SyncEnterFncPtr)
(
    DALSYSSyncHandle
);
typedef DALResult
(*DALSYS_SyncTryEnterFncPtr)
(
    DALSYSSyncHandle
);
typedef void
(*DALSYS_SyncLeaveFncPtr)
(
    DALSYSSyncHandle
);
typedef DALResult
(*DALSYS_MemRegionAllocFncPtr)
(
    uint32,
    DALSYSMemAddr,
    DALSYSMemAddr,
    uint32,
    DALSYSMemHandle *,
    DALSYSMemObj *
);
typedef DALResult
(*DALSYS_MemRegionMapPhysFncPtr)
(
    DALSYSMemHandle,
    uint32,
    DALSYSMemAddr,
    uint32
);
typedef DALResult
(*DALSYS_MemInfoFncPtr)
(
    DALSYSMemHandle,
    DALSYSMemInfo *
);
typedef DALResult
(*DALSYS_CacheCommandFncPtr)
(
    uint32 ,
    DALSYSMemAddr,
    uint32
);
typedef DALResult
(*DALSYS_MallocFncPtr)
(
    uint32 ,
    void **
);
typedef DALResult
(*DALSYS_FreeFncPtr)
(
    void *
);
typedef void
(*DALSYS_BusyWaitFncPtr)
(
   uint32
);
typedef DALResult
(*DALSYS_MemDescAddBufFncPtr)
(
    DALSysMemDescList *,
    uint32,
    uint32,
    uint32
);
typedef DALResult
(*DALSYS_MemDescPrepareFncPtr)
(
    DALSysMemDescList *,
    uint32
);
typedef DALResult
(*DALSYS_MemDescValidateFncPtr)
(
    DALSysMemDescList *,
    uint32
);
typedef DALResult
(*DALSYS_GetDALPropertyHandleFncPtr)
(
    DALDEVICEID,
    DALSYSPropertyHandle
);
typedef DALResult
(*DAL_DeviceAttachFncPtr)
(
    const char *pszArg,
    DALDEVICEID,
    DalDeviceHandle **
);
typedef DALResult
(*DAL_DeviceDetachFncPtr)
(
    DalDeviceHandle *
);
typedef DALResult
(*DAL_DeviceAttachExFncPtr)
(
 const char *pszArg,
    DALDEVICEID DevId,
    DALInterfaceVersion ClientVersion,
    DalDeviceHandle **phDalDevice
);
typedef DALResult
(*DALSYS_GetPropertyValueFncPtr)
(
    DALSYSPropertyHandle,
    const char *,
    uint32,
    DALSYSPropertyVar *
);
typedef void
(*DALSYS_LogEventFncPtr)
(
    DALDEVICEID,
    uint32,
    const char *
);
typedef struct DALSYSFncPtrTbl DALSYSFncPtrTbl;
struct DALSYSFncPtrTbl
{
    DALSYS_InitSystemHandleFncPtr DALSYS_InitSystemHandleFnc;
    DALSYS_DestroyObjectFncPtr DALSYS_DestroyObjectFnc;
    DALSYS_CopyObjectFncPtr DALSYS_CopyObjectFnc;
    DALSYS_RegisterWorkLoopFncPtr DALSYS_RegisterWorkLoopFnc;
    DALSYS_RegisterWorkLoopExFncPtr DALSYS_RegisterWorkLoopExFnc;
    DALSYS_AddEventToWorkLoopFncPtr DALSYS_AddEventToWorkLoopFnc;
    DALSYS_DeleteEventFromWorkLoopFncPtr DALSYS_DeleteEventFromWorkLoopFnc;
    DALSYS_EventCreateFncPtr DALSYS_EventCreateFnc;
    DALSYS_EventCtrlFncPtr DALSYS_EventCtrlFnc;
    DALSYS_EventWaitFncPtr DALSYS_EventWaitFnc;
    DALSYS_EventMultipleWaitFncPtr DALSYS_EventMultipleWaitFnc;
    DALSYS_SetupCallbackEventFncPtr DALSYS_SetupCallbackEventFnc;
    DALSYS_SyncCreateFncPtr DALSYS_SyncCreateFnc;
    DALSYS_SyncEnterFncPtr DALSYS_SyncEnterFnc;
    DALSYS_SyncTryEnterFncPtr DALSYS_SyncTryEnterFnc;
    DALSYS_SyncLeaveFncPtr DALSYS_SyncLeaveFnc;
    DALSYS_MemRegionAllocFncPtr DALSYS_MemRegionAllocFnc;
    DALSYS_MemRegionMapPhysFncPtr DALSYS_MemRegionMapPhysFnc;
    DALSYS_MemInfoFncPtr DALSYS_MemInfoFnc;
    DALSYS_CacheCommandFncPtr DALSYS_CacheCommandFnc;
    DALSYS_MallocFncPtr DALSYS_MallocFnc;
    DALSYS_FreeFncPtr DALSYS_FreeFnc;
    DALSYS_BusyWaitFncPtr DALSYS_BusyWaitFnc;
    DALSYS_MemDescAddBufFncPtr DALSYS_MemDescAddBufFnc;
    DALSYS_MemDescPrepareFncPtr DALSYS_MemDescPrepareFnc;
    DALSYS_MemDescValidateFncPtr DALSYS_MemDescValidateFnc;
    DALSYS_GetDALPropertyHandleFncPtr DALSYS_GetDALPropertyHandleFnc;
    DAL_DeviceAttachFncPtr DALSYS_DeviceAttachFnc;
    DAL_DeviceAttachExFncPtr DALSYS_DeviceAttachExFnc;
    DAL_DeviceAttachExFncPtr DALSYS_DeviceAttachRemoteFnc;
    DAL_DeviceDetachFncPtr DALSYS_DeviceDetachFnc;
    DALSYS_GetPropertyValueFncPtr DALSYS_GetPropertyValueFnc;
    DALSYS_LogEventFncPtr DALSYS_LogEventFnc;
};
typedef DALResult
(*DALRemote_NewFncPtr)(const char *, DALDEVICEID, DalDeviceHandle **);
typedef int
(*DALRemote_CommonInitFncPtr)(void);
typedef void
(*DALRemote_IPCInitFcnPtr)(uint32);
typedef int
(*DALRemote_InitFncPtr)(void);
typedef DALSYSEventHandle
(*DALRemote_CreateEventFncPtr)(void *pClientCtxt, DALSYSEventHandle hRemote);
typedef uint32
(*DALRemoteInterProcessCallFncPtr)(void *in_buf, void *out_buf);
typedef int
(*DALRemote_DeinitFncPtr)(void);
typedef struct DALRemoteVtbl DALRemoteVtbl;
struct DALRemoteVtbl
{
   DALRemote_NewFncPtr DALRemote_NewFnc;
   DALRemote_CommonInitFncPtr DALRemote_CommonInitFnc;
   DALRemote_InitFncPtr DALRemote_InitFnc;
   DALRemote_DeinitFncPtr DALRemote_DeinitFnc;
   DALRemote_CreateEventFncPtr DALRemote_CreateEventFnc;
   DALRemote_CreateEventFncPtr DALRemote_CreatePayloadEventFnc;
   DALRemoteInterProcessCallFncPtr DALRemoteInterProcessCallFnc;
   DALRemote_IPCInitFcnPtr DALRemote_IPCInitFcn;
};
typedef struct DALSYSConfig DALSYSConfig;
struct DALSYSConfig
{
    void *pNativeEnv;
    uint32 dwConfig;
   DALRemoteVtbl *pRemoteVtbl;
};
typedef long _Int32t;
typedef unsigned long _Uint32t;
typedef int _Ptrdifft;
typedef unsigned int _Sizet;
typedef __builtin_va_list va_list;
typedef long long _Longlong;
typedef unsigned long long _ULonglong;
typedef int _Wchart;
typedef int _Wintt;
typedef va_list _Va_list;
void _Atexit(void (*)(void));
typedef char _Sysch_t;
void _Locksyslock(int);
void _Unlocksyslock(int);
typedef unsigned short __attribute__((__may_alias__)) alias_short;
static alias_short *strict_aliasing_workaround(unsigned short *ptr) __attribute__((always_inline,unused));
static alias_short *strict_aliasing_workaround(unsigned short *ptr)
{
  alias_short *aliasptr = (alias_short *)ptr;
  return aliasptr;
}
typedef _Sizet size_t;
int memcmp(const void *, const void *, size_t) __attribute__((__nothrow__));
void *memcpy(void *, const void *, size_t) __attribute__((__nothrow__));
void *memcpy_v(volatile void *, const volatile void *, size_t) __attribute__((__nothrow__));
void *memset(void *, int, size_t) __attribute__((__nothrow__));
char *strcat(char *, const char *) __attribute__((__nothrow__));
int strcmp(const char *, const char *) __attribute__((__nothrow__));
char *strcpy(char *, const char *) __attribute__((__nothrow__));
size_t strlen(const char *) __attribute__((__nothrow__));
void *memmove(void *, const void *, size_t) __attribute__((__nothrow__));
void *memmove_v(volatile void *, const volatile void *, size_t) __attribute__((__nothrow__));
int strcoll(const char *, const char *) __attribute__((__nothrow__));
size_t strcspn(const char *, const char *) __attribute__((__nothrow__));
char *strerror(int) __attribute__((__nothrow__));
size_t strlcat(char *, const char *, size_t) __attribute__((__nothrow__));
char *strncat(char *, const char *, size_t) __attribute__((__nothrow__));
int strncmp(const char *, const char *, size_t) __attribute__((__nothrow__));
size_t strlcpy(char *, const char *, size_t) __attribute__((__nothrow__));
char *strncpy(char *, const char *, size_t) __attribute__((__nothrow__));
size_t strspn(const char *, const char *) __attribute__((__nothrow__));
char *strtok(char *, const char *) __attribute__((__nothrow__));
char *strsep(char **, const char *) __attribute__((__nothrow__));
size_t strxfrm(char *, const char *, size_t) __attribute__((__nothrow__));
char *strdup(const char *) __attribute__((__nothrow__));
int strcasecmp(const char *, const char *) __attribute__((__nothrow__));
int strncasecmp(const char *, const char *, size_t) __attribute__((__nothrow__));
char *strtok_r(char *, const char *, char **) __attribute__((__nothrow__));
void *memccpy (void *, const void *, int, size_t) __attribute__((__nothrow__));
int strerror_r (int, char *, size_t) __attribute__((__nothrow__));
char *strchr(const char *, int) __attribute__((__nothrow__));
char *strpbrk(const char *, const char *) __attribute__((__nothrow__));
char *strrchr(const char *, int) __attribute__((__nothrow__));
char *strstr(const char *, const char *) __attribute__((__nothrow__));
void *memchr(const void *, int, size_t) __attribute__((__nothrow__));
void
DALSYS_InitMod(DALSYSConfig * pCfg);
void
DALSYS_DeInitMod(void);
void
DALSYS_InitSystemHandle(DalDeviceHandle *hDalDevice);
DALSYS_LogEventFncPtr
DALSYS_SetLogCfg(uint32 dwMaxLogLevel, DALSYS_LogEventFncPtr DALSysLogFcn);
DALResult
DALSYS_DestroyObject(DALSYSObjHandle hObj);
DALResult
DALSYS_CopyObject(DALSYSObjHandle hObjOrig, DALSYSObjHandle *phObjCopy );
DALResult
DALSYS_RegisterWorkLoop(uint32 dwPriority,
                  uint32 dwMaxNumEvents,
                  DALSYSWorkLoopHandle *phWorkLoop,
                  DALSYSWorkLoopObj *pWorkLoopObj);
DALResult
DALSYS_RegisterWorkLoopEx(
                  char * pszname,
                  uint32 dwStackSize,
                  uint32 dwPriority,
                  uint32 dwMaxNumEvents,
                  DALSYSWorkLoopHandle *phWorkLoop,
                  DALSYSWorkLoopObj *pWorkLoopObj);
DALResult
DALSYS_AddEventToWorkLoop(DALSYSWorkLoopHandle hWorkLoop,
                    DALSYSWorkLoopExecute pfnWorkLoopExecute,
                    void * pArg,
                    DALSYSEventHandle hEvent,
                    DALSYSSyncHandle hSync);
DALResult
DALSYS_DeleteEventFromWorkLoop(DALSYSWorkLoopHandle hWorkLoop,
                         DALSYSEventHandle hEvent);
DALResult
DALSYS_EventCreate(uint32 dwAttribs, DALSYSEventHandle *phEvent,
               DALSYSEventObj *pEventObj);
DALResult
DALSYS_EventCopy(DALSYSEventHandle hEvent,
             DALSYSEventHandle *phEventCopy,DALSYSEventObj *pEventObjCopy,
                 uint32 dwMarshalFlags);
DALResult
DALSYS_EventCtrlEx(DALSYSEventHandle hEvent, uint32 dwCtrl, uint32 dwParam,
                   void *pPayload, uint32 dwPayloadSize);
DALResult
DALSYS_EventWait(DALSYSEventHandle hEvent);
DALResult
DALSYS_EventMultipleWait(DALSYSEventHandle* phEvent, int nEvents,
                         uint32 dwTimeoutUs,uint32 *pdwEventIdx);
DALResult
DALSYS_SetupCallbackEvent(DALSYSEventHandle hEvent, DALSYSCallbackFunc cbFunc,
                          DALSYSCallbackFuncCtx cbFuncCtx);
DALResult DALSYS_TimerStart( DALSYSEventHandle hEvent, uint32 time);
DALResult DALSYS_TimerStop( DALSYSEventHandle hEvent );
DALResult
DALSYS_SyncCreate(uint32 dwAttribs,
                  DALSYSSyncHandle *phSync,
                  DALSYSSyncObj *pSyncObj);
void
DALSYS_SyncEnter(DALSYSSyncHandle hSync);
DALResult
DALSYS_SyncTryEnter(DALSYSSyncHandle hSync);
void
DALSYS_SyncLeave(DALSYSSyncHandle hSync);
DALResult
DALSYS_MemRegionAlloc(uint32 dwAttribs, DALSYSMemAddr VirtualAddr,
                      DALSYSMemAddr PhysicalAddr, uint32 dwLen,
                      DALSYSMemHandle *phMem, DALSYSMemObj *pMemObj);
DALResult
DALSYS_MemRegionAllocEx( DALSYSMemHandle *phMem, const DALSYSMemReq *pMemReq,
      DALSYSMemInfoEx *pMemInfo );
DALResult
DALSYS_MemRegionMapPhys(DALSYSMemHandle hMem, uint32 dwVirtualBaseOffset,
                        DALSYSMemAddr PhysicalAddr, uint32 dwLen);
DALResult
DALSYS_MemInfo(DALSYSMemHandle hMem, DALSYSMemInfo *pMemInfo);
DALResult
DALSYS_MemInfoEx(DALSYSMemHandle hMem, DALSYSMemInfoEx *pMemInfo);
DALResult
DALSYS_CacheCommand(uint32 CacheCmd, DALSYSMemAddr VirtualAddr, uint32 dwLen);
DALResult
DALSYS_Malloc(uint32 dwSize, void **ppMem);
DALResult
DALSYS_Free(void *pmem);
void
DALSYS_BusyWait(uint32 pause_time_us);
DALResult
DALSYS_GetDALPropertyHandle(DALDEVICEID DeviceId, DALSYSPropertyHandle hDALProps);
DALResult
DALSYS_GetDALPropertyHandleEx(DALDEVICEID DeviceId,DALSYSPropertyHandle hDALProps,
                              DEVCFG_TARGET_INFO_TYPE target_info_type);
DALResult
DALSYS_GetDALPropertyHandleDyn(DALDEVICEID DeviceId, DALSYSPropertyHandle hDALProps);
DALResult
DALSYS_GetDALPropertyHandleStr(const char *pszDevName, DALSYSPropertyHandle hDALProps);
DALResult
DALSYS_GetDALPropertyHandleStrEx(const char *pszDevName, DALSYSPropertyHandle hDALProps,
                                 DEVCFG_TARGET_INFO_TYPE target_info_type);
DALResult
DALSYS_GetDALPropertyHandleStrDyn(const char *pszDevName, DALSYSPropertyHandle hDALProps);
DALResult
DALSYS_GetPropertyValue(DALSYSPropertyHandle hDALProps, const char *pszName,
                  uint32 dwId,
                   DALSYSPropertyVar *pDALPropVar);
void
DALSYS_LogEvent(DALDEVICEID DeviceId, uint32 dwLogEventType,
      const char * pszFmt, ...);
DALRemoteVtbl *
DALSYS_GetRemoteInterfaceVtbl(void);
uint32 DALSYS_SetThreadPriority(uint32 priority);
uint32 _DALSYS_memscpy(void * pDest, uint32 iDestSz,
      const void * pSrc, uint32 iSrcSize);
typedef uint32 GPIOINTISRCtx;
typedef void * (*GPIOINTISR)(GPIOINTISRCtx);
typedef enum{
  GPIOINT_DEVICE_MODEM,
  GPIOINT_DEVICE_SPS,
  GPIOINT_DEVICE_LPA_DSP,
  GPIOINT_DEVICE_RPM,
  GPIOINT_DEVICE_APPS,
  GPIOINT_DEVICE_WCN,
  GPIOINT_DEVICE_DSP,
  GPIOINT_DEVICE_NONE,
  PLACEHOLDER_GPIOIntProcessorType = 0x7fffffff
}GPIOIntProcessorType;
typedef enum{
  GPIOINT_TRIGGER_HIGH,
  GPIOINT_TRIGGER_LOW,
  GPIOINT_TRIGGER_RISING,
  GPIOINT_TRIGGER_FALLING,
  GPIOINT_TRIGGER_DUAL_EDGE,
  PLACEHOLDER_GPIOIntTriggerType = 0x7fffffff
}GPIOIntTriggerType;
typedef struct GPIOInt GPIOInt;
struct GPIOInt
{
   struct DalDevice DalDevice;
   DALResult (*SetTrigger)(DalDeviceHandle * _h, uint32 gpio, GPIOIntTriggerType trigger);
   DALResult (*RegisterIsr)(DalDeviceHandle * _h, uint32 gpio,GPIOIntTriggerType trigger,
                            GPIOINTISR isr,GPIOINTISRCtx param);
   DALResult (*RegisterEvent)(DalDeviceHandle * _h, uint32 gpio, GPIOIntTriggerType trigger,
                              DALSYSEventHandle event);
   DALResult (*DeRegisterEvent)(DalDeviceHandle * _h, uint32 gpio,DALSYSEventHandle event);
   DALResult (*DeregisterIsr)(DalDeviceHandle * _h, uint32 gpio, GPIOINTISR isr);
   DALResult (*IsInterruptEnabled)(DalDeviceHandle * _h, uint32 gpio, void * state, uint32 size);
   DALResult (*IsInterruptPending)(DalDeviceHandle * _h, uint32 gpio, void * state, uint32 size);
   DALResult (*Save)(DalDeviceHandle * _h);
   DALResult (*Restore)(DalDeviceHandle * _h);
   DALResult (*DisableInterrupt)(DalDeviceHandle * _h, uint32 gpio);
   DALResult (*EnableInterrupt)(DalDeviceHandle * _h, uint32 gpio);
   DALResult (*InterruptNotify)(DalDeviceHandle * _h, uint32 gpio);
   DALResult (*MonitorInterrupts)(DalDeviceHandle * _h, GPIOINTISR isr,uint32 enable);
   DALResult (*MapMPMInterrupt)(DalDeviceHandle * h, uint32 gpio, uint32 mpm_interrupt_id);
   DALResult (*AttachRemote)(DalDeviceHandle * h, uint32 processor);
   DALResult (*TriggerInterrupt)(DalDeviceHandle * h, uint32 gpio);
   DALResult (*ClearInterrupt)(DalDeviceHandle * h, uint32 gpio);
   DALResult (*IsInterruptSet)(DalDeviceHandle * _h, uint32 gpio, void * state, uint32 size);
   DALResult (*SetDirectConnectGPIOMapping)(DalDeviceHandle * _h, uint32 gpio, uint32 direct_connect_line);
   DALResult (*IsInterruptRegistered)(DalDeviceHandle * _h, uint32 gpio, void * state, uint32 size);
   DALResult (*DisableGPIOInterrupt)(DalDeviceHandle * _h, uint32 gpio);
   DALResult (*EnableGPIOInterrupt)(DalDeviceHandle * _h, uint32 gpio);
   DALResult (*GPIOInt_RegisterIsrEx)(DalDeviceHandle * _h, uint32 gpio,GPIOIntTriggerType trigger,
                            GPIOINTISR isr,GPIOINTISRCtx param, uint32 nFlags);
};
typedef struct GPIOIntHandle GPIOIntHandle;
struct GPIOIntHandle
{
   uint32 dwDalHandleId;
   const GPIOInt * pVtbl;
   void * pClientCtxt;
};
static __inline DALResult
GPIOInt_SetTrigger(DalDeviceHandle * _h, uint32 gpio, GPIOIntTriggerType trigger)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_1(((uint32 *)&((((GPIOIntHandle *)_h)->pVtbl)->SetTrigger)-(uint32 *)(((GPIOIntHandle *)_h)->pVtbl)), _h, gpio, trigger);
   }
   return ((GPIOIntHandle *)_h)->pVtbl->SetTrigger( _h, gpio, trigger);
}
static __inline DALResult
GPIOInt_RegisterIsr(DalDeviceHandle * _h, uint32 gpio,GPIOIntTriggerType trigger,
GPIOINTISR isr,GPIOINTISRCtx param)
{
   if(!(((DALHandle)_h) & 0x00000001))
   {
     return ((GPIOIntHandle *)_h)->pVtbl->RegisterIsr( _h, gpio, trigger,isr,param);
   }
   else
       return -1;
}
static __inline DALResult
GPIOInt_RegisterIsrEx(DalDeviceHandle * _h, uint32 gpio,GPIOIntTriggerType trigger,
GPIOINTISR isr,GPIOINTISRCtx param, uint32 nFlags)
{
   if(!(((DALHandle)_h) & 0x00000001))
   {
     return ((GPIOIntHandle *)_h)->pVtbl->GPIOInt_RegisterIsrEx( _h, gpio, trigger,isr,param, nFlags);
   }
   else
     return -1;
}
static __inline DALResult
GPIOInt_RegisterEvent(DalDeviceHandle * _h, uint32 gpio,
                                         GPIOIntTriggerType trigger,DALSYSEventHandle event)
{
   if(!(((DALHandle)_h) & 0x00000001))
   {
     return ((GPIOIntHandle *)_h)->pVtbl->RegisterEvent( _h, gpio, trigger, event);
   }
   else
       return -1;
}
static __inline DALResult
GPIOInt_DeRegisterEvent(DalDeviceHandle * _h, uint32 gpio,DALSYSEventHandle event)
{
   if(!(((DALHandle)_h) & 0x00000001))
   {
     return ((GPIOIntHandle *)_h)->pVtbl->DeRegisterEvent( _h, gpio,event);
   }
   else
     return -1;
}
static __inline DALResult
GPIOInt_DeregisterIsr(DalDeviceHandle * _h, uint32 gpio, GPIOINTISR isr)
{
  if(!(((DALHandle)_h) & 0x00000001))
  {
    return ((GPIOIntHandle *)_h)->pVtbl->DeregisterIsr( _h, gpio, isr);
  }
  else
    return -1;
}
static __inline DALResult
GPIOInt_IsInterruptEnabled(DalDeviceHandle * _h, uint32 gpio, uint32* state)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_6(((uint32 *)&((((GPIOIntHandle *)_h)->pVtbl)->IsInterruptEnabled)-(uint32 *)(((GPIOIntHandle *)_h)->pVtbl)), _h, gpio, (void *)state, 1);
   }
   return ((GPIOIntHandle *)_h)->pVtbl->IsInterruptEnabled( _h, gpio, (void *)state, 1);
}
static __inline DALResult
GPIOInt_IsInterruptPending(DalDeviceHandle * _h, uint32 gpio, uint32 * state)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_6(((uint32 *)&((((GPIOIntHandle *)_h)->pVtbl)->IsInterruptPending)-(uint32 *)(((GPIOIntHandle *)_h)->pVtbl)), _h, gpio, (void *)state, 1);
   }
   return ((GPIOIntHandle *)_h)->pVtbl->IsInterruptPending( _h, gpio, (void *)state, 1);
}
static __inline DALResult
GPIOInt_IsInterruptSet(DalDeviceHandle * _h, uint32 gpio, uint32 * state)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_6(((uint32 *)&((((GPIOIntHandle *)_h)->pVtbl)->IsInterruptSet)-(uint32 *)(((GPIOIntHandle *)_h)->pVtbl)), _h, gpio, (void *)state, 1);
   }
   return ((GPIOIntHandle *)_h)->pVtbl->IsInterruptSet( _h, gpio, (void *)state, 1);
}
static __inline DALResult
GPIOInt_Save(DalDeviceHandle * _h)
{
  if(!(((DALHandle)_h) & 0x00000001))
  {
    return ((GPIOIntHandle *)_h)->pVtbl->Save( _h);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
GPIOInt_Restore(DalDeviceHandle * _h)
{
  if(!(((DALHandle)_h) & 0x00000001))
  {
    return ((GPIOIntHandle *)_h)->pVtbl->Restore( _h);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
GPIOInt_DisableInterrupt(DalDeviceHandle * _h, uint32 gpio)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_0(((uint32 *)&((((GPIOIntHandle *)_h)->pVtbl)->DisableInterrupt)-(uint32 *)(((GPIOIntHandle *)_h)->pVtbl)), _h, gpio);
   }
   return ((GPIOIntHandle *)_h)->pVtbl->DisableInterrupt( _h, gpio);
}
static __inline DALResult
GPIOInt_EnableInterrupt(DalDeviceHandle * _h, uint32 gpio)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_0(((uint32 *)&((((GPIOIntHandle *)_h)->pVtbl)->EnableInterrupt)-(uint32 *)(((GPIOIntHandle *)_h)->pVtbl)), _h, gpio);
   }
   return ((GPIOIntHandle *)_h)->pVtbl->EnableInterrupt( _h, gpio);
}
static __inline DALResult
GPIOInt_InterruptNotify(DalDeviceHandle * _h, uint32 gpio)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_0(((uint32 *)&((((GPIOIntHandle *)_h)->pVtbl)->InterruptNotify)-(uint32 *)(((GPIOIntHandle *)_h)->pVtbl)), _h, gpio);
   }
   return ((GPIOIntHandle *)_h)->pVtbl->InterruptNotify( _h, gpio);
}
static __inline DALResult
GPIOInt_MonitorInterrupts(DalDeviceHandle * _h, GPIOINTISR isr,uint32 enable)
{
  if(!(((DALHandle)_h) & 0x00000001))
   {
   return ((GPIOIntHandle *)_h)->pVtbl->MonitorInterrupts( _h,isr,enable);
}
  else
    return -1;
}
static __inline DALResult
GPIOInt_MapMPMInterrupt(DalDeviceHandle * _h, uint32 gpio, uint32 mpm_interrupt_id)
{
  if(!(((DALHandle)_h) & 0x00000001))
  {
    return ((GPIOIntHandle *)_h)->pVtbl->MapMPMInterrupt( _h, gpio,mpm_interrupt_id);
  }
  else
    return -1;
}
static __inline DALResult
GPIOInt_AttachRemote(DalDeviceHandle * _h, uint32 processor)
{
  if(!(((DALHandle)_h) & 0x00000001))
  {
    return ((GPIOIntHandle *)_h)->pVtbl->AttachRemote( _h, processor);
  }
  else
    return -1;
}
static __inline DALResult
GPIOInt_TriggerInterrupt(DalDeviceHandle * _h, uint32 gpio)
{
  if(!(((DALHandle)_h) & 0x00000001))
  {
    return ((GPIOIntHandle *)_h)->pVtbl->TriggerInterrupt( _h, gpio);
  }
  else
  {
    return -1;
  }
}
void GPIOInt_Init(void);
static __inline DALResult
GPIOInt_ClearInterrupt(DalDeviceHandle * _h, uint32 gpio)
{
  if(!(((DALHandle)_h) & 0x00000001))
  {
    return ((GPIOIntHandle *)_h)->pVtbl->ClearInterrupt( _h, gpio);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
GPIOInt_SetDirectConnectGPIOMapping(DalDeviceHandle * _h, uint32 gpio, uint32 direct_connect_line)
{
  if(!(((DALHandle)_h) & 0x00000001))
  {
    return ((GPIOIntHandle *)_h)->pVtbl->SetDirectConnectGPIOMapping( _h, gpio, direct_connect_line);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
GPIOInt_IsInterruptRegistered(DalDeviceHandle * _h, uint32 gpio, uint32 * state)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_6(((uint32 *)&((((GPIOIntHandle *)_h)->pVtbl)->IsInterruptRegistered)-(uint32 *)(((GPIOIntHandle *)_h)->pVtbl)), _h, gpio, (void *)state, 1);
   }
   return ((GPIOIntHandle *)_h)->pVtbl->IsInterruptRegistered( _h, gpio, (void *)state, 1);
}
<driver name="GPIOInt">
<global_def>
<var_seq name="xo_shutdown" type=0x00000001>/xo/cxo</var_seq>
</global_def>
<device id=0x020000AB>
<props name="GPIOINT_TARGET_PROC_VERSION" type=0x00000002>
1
</props>
<props name="NUMBER_OF_DIRECT_CONNECT_INTERRUPTS" type=0x00000002>
6
</props>
<props name="GPIOINT_PHYSICAL_ADDRESS" type=0x00000002>
0x01000000
</props>
<props name="PROCESSOR" type=0x00000002>
GPIOINT_DEVICE_LPA_DSP
</props>
<props name="SUMMARY_INTR_ID" type=0x00000002>
38
</props>
<props name="AUD_CODEC_INT_GPIO_MAP" type=0x00000008>
25,53,end
</props>
<props name="AUD_EXT_VFR_INT_GPIO_MAP_0" type=0x00000008>
24,34,end
</props>
<props name="DIRECT_CONNECT_CONFIG_MAP" type=0x00000012>
interrupt_config_map
</props>
<props name="INTERRUPT_PLATFORM" type=0x00000002>
2
</props>
<props name="XO_SHUTDOWN_RSRC" type=0x00000011>
xo_shutdown
</props>
</device>
</driver>

<driver name="GPIOMgr">
  <device id="GPIOManager">
    <props name="GPIOMGR_NUM_GPIOS" type=0x00000002>
      150
    </props>
    <props name="GPIOMGR_DIRCONN_CONFIG_MAP" type=0x00000012>
      GPIOMgrConfigMap
    </props>
    <props name="GPIOMGR_PD_CONFIG_MAP" type=0x00000012>
      GPIOMgrConfigPD
    </props>
    <props name="IsRemotable" type=0x00000002>
      0x1
    </props>
  </device>
</driver>

enum_header_path "DALInterruptControllerConfig.h"
enum_header_path "DALInterruptController.h"
enum_header_path "atomic_ops.h"
enum_header_path "qurt_atomic_ops.h"
enum_header_path "atomic_ops_plat.h"
enum_header_path "DALInterruptController_utils.h"
typedef unsigned long int uint32;
typedef unsigned short uint16;
typedef unsigned char uint8;
typedef signed long int int32;
typedef signed short int16;
typedef signed char int8;
typedef unsigned long long uint64;
typedef long long int64;
typedef unsigned char byte;
typedef uint32 DALBOOL;
typedef uint32 DALDEVICEID;
typedef uint32 DalPowerCmd;
typedef uint32 DalPowerDomain;
typedef uint32 DalSysReq;
typedef uint32 DALHandle;
typedef int DALResult;
typedef void * DALEnvHandle;
typedef void * DALSYSEventHandle;
typedef uint32 DALMemAddr;
typedef uint32 DALSYSMemAddr;
typedef uint64 DALSYSPhyAddr;
typedef uint32 DALInterfaceVersion;
typedef unsigned char * DALDDIParamPtr;
typedef struct DALEventObject DALEventObject;
struct DALEventObject
{
    uint32 obj[8];
};
typedef DALEventObject * DALEventHandle;
typedef struct _DALMemObject
{
   uint32 memAttributes;
   uint32 sysObjInfo[2];
   uint32 dwLen;
   uint32 ownerVirtAddr;
   uint32 virtAddr;
   uint32 physAddr;
}
DALMemObject;
typedef struct _DALDDIMemBufDesc
{
   uint32 dwOffset;
   uint32 dwLen;
   uint32 dwUser;
}
DALDDIMemBufDesc;
typedef struct _DALDDIMemDescList
{
   uint32 dwFlags;
   uint32 dwNumBufs;
   DALDDIMemBufDesc bufList[1];
}
DALDDIMemDescList;
typedef struct DALSysMemDescBuf DALSysMemDescBuf;
struct DALSysMemDescBuf
{
   DALSYSMemAddr VirtualAddr;
   DALSYSMemAddr PhysicalAddr;
   uint32 size;
   uint32 user;
};
typedef struct { uint32 dwObjInfo; DALSYSMemAddr thisVirtualAddr; DALSYSMemAddr PhysicalAddr; DALSYSMemAddr VirtualAddr; uint32 hOwnerProc; uint32 dwCurBufIdx; uint32 dwNumDescBufs; DALSysMemDescBuf BufInfo[1];} DALSysMemDescList;
typedef struct DalDeviceInfo DalDeviceInfo;
struct DalDeviceInfo
{
   uint32 sizeOfActual;
   uint32 Version;
   char Name[32];
};
typedef struct DalDeviceStatus DalDeviceStatus;
struct DalDeviceStatus
{
   uint32 sizeOfActual;
   uint32 Status;
};
typedef struct DalDeviceHandle DalDeviceHandle;
typedef struct DalDevice DalDevice;
struct DalDevice
{
   DALResult (*Attach)(const char*,DALDEVICEID,DalDeviceHandle **);
   uint32 (*Detach)(DalDeviceHandle *);
   DALResult (*Init)(DalDeviceHandle *);
   DALResult (*DeInit)(DalDeviceHandle *);
   DALResult (*Open)(DalDeviceHandle *, uint32);
   DALResult (*Close)(DalDeviceHandle *);
   DALResult (*Info)(DalDeviceHandle *, DalDeviceInfo *, uint32);
   DALResult (*PowerEvent)(DalDeviceHandle *, DalPowerCmd, DalPowerDomain);
   DALResult (*SysRequest)(DalDeviceHandle *, DalSysReq, const void *, uint32,
                           void *,uint32, uint32*);
};
typedef struct DalInterface DalInterface;
struct DalInterface
{
   struct DalDevice DalDevice;
};
struct DalDeviceHandle
{
   uint32 dwDalHandleId;
   const DalInterface *pVtbl;
   void *pClientCtxt;
   uint32 dwVtblLen;
};
typedef struct DalDeviceHandle * DALDEVICEHANDLE;
typedef struct DalRemoteHandle DalRemoteHandle;
typedef struct DalRemote DalRemote;
struct DalRemote
{
   struct DalDevice DalDevice;
   DALResult (* FCN_0) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1 );
   DALResult (* FCN_1) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32 u2 );
   DALResult (* FCN_2) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32* p_u2 );
   DALResult (* FCN_3) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32 u2, uint32 u3 );
   DALResult (* FCN_4) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32 u2, uint32* p_u3 );
   DALResult (* FCN_5) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen);
   DALResult (* FCN_6) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * ibuf, uint32 ilen);
   DALResult (* FCN_7) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * obuf, uint32 olen, uint32 *oalen);
   DALResult (* FCN_8) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * obuf, uint32 olen);
   DALResult (* FCN_9) ( uint32 ddi_idx, DalDeviceHandle *h, void * obuf, uint32 olen );
   DALResult (* FCN_10)( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * ibuf, uint32 ilen, void * obuf, uint32 olen, uint32 * oalen);
   DALResult (* FCN_11) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * obuf, uint32 olen);
   DALResult (* FCN_12) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * obuf, uint32 olen, uint32 *oalen);
   DALResult (* FCN_13) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * ibuf2, uint32 ilen2, void * obuf, uint32 olen);
   DALResult (* FCN_14) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * obuf1, uint32 olen, void * obuf2, uint32 olen2, uint32 * oalen);
   DALResult (* FCN_15) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * ibuf2, uint32 ilen2, void * obuf2, uint32 olen2, uint32 *oalen, void * obuf, uint32 olen );
};
struct DalRemoteHandle
{
   uint32 dwDalHandleId;
   const DalRemote *pVtbl;
   void *pClientCtxt;
};
static __inline uint32
DalDevice_Detach(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Detach(_h);
}
static __inline DALResult
DalDevice_Init(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Init(_h);
}
static __inline DALResult
DalDevice_DeInit(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.DeInit(_h);
}
static __inline DALResult
DalDevice_PowerEvent(DalDeviceHandle *_h, DalPowerCmd PowerCmd,
                     DalPowerDomain PowerDomain)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.PowerEvent(_h,PowerCmd,PowerDomain);
}
static __inline DALResult
DalDevice_Open(DalDeviceHandle *_h, uint32 mode)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Open(_h,mode);
}
static __inline DALResult
DalDevice_Close(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Close(_h);
}
static __inline DALResult
DalDevice_Info(DalDeviceHandle *_h, DalDeviceInfo* info, uint32 infoSize)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Info(_h,info,infoSize);
}
static __inline DALResult
DalDevice_SysRequest(DalDeviceHandle *_h, DalSysReq ReqIdx,
                     const unsigned char* SrcBuf, int SrcBufLen,
                     unsigned char* DestBuf, uint32 DestBufLen, uint32* DestBufLenReq)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.SysRequest(_h,ReqIdx,SrcBuf,SrcBufLen,
                                          DestBuf,DestBufLen,DestBufLenReq);
}
DALResult
DAL_DeviceAttach(DALDEVICEID DeviceId,
                 DalDeviceHandle **phDevice);
DALResult
DAL_DeviceAttachLocal(const char *pszArg,DALDEVICEID DeviceId,
                      DalDeviceHandle **phDevice);
DALResult
DAL_DeviceAttachEx(const char *pszArg, DALDEVICEID DeviceId,
               DALInterfaceVersion ClientVersion,DalDeviceHandle **phDevice);
DALResult
DAL_StringDeviceAttachEx(const char *pszArg,
                   const char *pszDevName,
                   DALInterfaceVersion ClientVersion,
                   DalDeviceHandle **phDalDevice);
DALResult
DAL_DeviceAttachRemote(const char *pszArg,
                   DALDEVICEID DevId,
                   DALInterfaceVersion ClientVersion,
                   DalDeviceHandle **phDALDevice);
DALResult
DAL_DeviceDetach(DalDeviceHandle *hDevice);
DALResult
DAL_StringDeviceAttach(const char *pszDevName,DalDeviceHandle **phDalDevice);
typedef uint32 DALInterruptID;
typedef void * DALISRCtx;
typedef void * (*DALISR)(DALISRCtx);
typedef void * DALIRQCtx;
typedef void * (*DALIRQ)(DALIRQCtx);
typedef enum
{
  DALINTCNTLR_NO_POWER_COLLAPSE,
  DALINTCNTLR_POWER_COLLAPSE,
  PLACEHOLDER_InterruptControllerSleepType = 0x7fffffff
} InterruptControllerSleepType;
typedef struct DalInterruptController DalInterruptController;
struct DalInterruptController
{
   struct DalDevice DalDevice;
   DALResult (*RegisterISR)(DalDeviceHandle * _h, DALInterruptID intrID,
                           const DALISR isr, const DALISRCtx ctx, uint32 bEnable);
   DALResult (*RegisterIST)(DalDeviceHandle * _h, DALInterruptID intrID,
                           const DALISR isr, const DALISRCtx ctx, uint32 bEnable,char* pISTName);
   DALResult (*RegisterEvent)(DalDeviceHandle * _h, DALInterruptID intrID,
                             const DALSYSEventHandle hEvent, uint32 bEnable);
   DALResult (*Unregister)(DalDeviceHandle * _h, DALInterruptID intrID);
   DALResult (*InterruptDone)(DalDeviceHandle * _h, DALInterruptID intrID);
   DALResult (*InterruptEnable)(DalDeviceHandle * _h, DALInterruptID intrID);
   DALResult (*InterruptDisable)(DalDeviceHandle * _h, DALInterruptID intrID);
   DALResult (*InterruptTrigger)(DalDeviceHandle * _h, DALInterruptID intrID);
   DALResult (*InterruptClear)(DalDeviceHandle * _h, DALInterruptID intrID);
   DALResult (*InterruptStatus)(DalDeviceHandle * _h, DALInterruptID intrID);
   DALResult (*RegisterIRQHandler)(DalDeviceHandle * _h, DALInterruptID intrID,
                           const DALIRQ irq, const DALIRQCtx ctx, uint32 bEnable);
  DALResult (*SetInterruptTrigger)(DalDeviceHandle * _h, DALInterruptID intrID, uint32 nTrigger);
  DALResult (*IsInterruptPending)(DalDeviceHandle * _h, DALInterruptID intrID,void* bState,uint32 size);
  DALResult (*IsInterruptEnabled)(DalDeviceHandle * _h, DALInterruptID intrID,void* bState,uint32 size);
  DALResult (*MapWakeupInterrupt)(DalDeviceHandle * _h, DALInterruptID intrID,uint32 nWakeupIntID);
  DALResult (*IsAnyInterruptPending)(DalDeviceHandle * _h, uint32* bState,uint32 size);
  DALResult (*Sleep)(DalDeviceHandle * _h, InterruptControllerSleepType sleep);
  DALResult (*Wakeup)(DalDeviceHandle * _h, InterruptControllerSleepType sleep);
  DALResult (*GetInterruptTrigger)(DalDeviceHandle * _h, DALInterruptID intrID,uint32* eTrigger, uint32 size);
  DALResult (*GetInterruptID)(DalDeviceHandle * _h, const char *szIntrName, uint32* pnIntrID, uint32 size);
  DALResult (*LogState)(DalDeviceHandle * _h, void *pULog);
};
typedef struct DalInterruptControllerHandle DalInterruptControllerHandle;
struct DalInterruptControllerHandle
{
   uint32 dwDalHandleId;
   const DalInterruptController * pVtbl;
   void * pClientCtxt;
};
static __inline DALResult
DalInterruptController_RegisterISR(DalDeviceHandle * _h, DALInterruptID intrID,
                           const DALISR isr, const DALISRCtx ctx, uint32 IntrFlags)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->RegisterISR( _h, intrID,
                                                                   isr, ctx, IntrFlags);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
DalInterruptController_RegisterIST(DalDeviceHandle * _h, DALInterruptID intrID,
                           const DALISR isr, const DALISRCtx ctx, uint32 IntrFlags, char* pISTName)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->RegisterIST( _h, intrID,
                                                                   isr, ctx, IntrFlags,pISTName);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
DalInterruptController_RegisterEvent(DalDeviceHandle * _h, DALInterruptID intrID,
                            const DALSYSEventHandle hEvent, uint32 IntrFlags)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->RegisterEvent( _h, intrID,
                                                                        hEvent, IntrFlags);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
DalInterruptController_Unregister(DalDeviceHandle * _h, DALInterruptID intrID)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->Unregister( _h, intrID);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
DalInterruptController_InterruptDone(DalDeviceHandle * _h, DALInterruptID intrID)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->InterruptDone( _h, intrID);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
DalInterruptController_InterruptEnable(DalDeviceHandle * _h, DALInterruptID intrID)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->InterruptEnable( _h, intrID);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
DalInterruptController_InterruptDisable(DalDeviceHandle * _h, DALInterruptID intrID)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->InterruptDisable( _h, intrID);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
DalInterruptController_InterruptTrigger(DalDeviceHandle * _h, DALInterruptID intrID)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->InterruptTrigger( _h, intrID);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
DalInterruptController_InterruptClear(DalDeviceHandle * _h, DALInterruptID intrID)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->InterruptClear( _h, intrID);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
DalInterruptController_InterruptStatus
(
  DalDeviceHandle * _h,
  DALInterruptID intrID
)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->InterruptStatus( _h, intrID);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
DalInterruptController_RegisterIRQHandler(DalDeviceHandle * _h, DALInterruptID intrID,const DALIRQ irq, const DALIRQCtx ctx, uint32 bEnable)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->RegisterIRQHandler( _h, intrID,
                                                                   irq, ctx, bEnable);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
DalInterruptController_SetTrigger(DalDeviceHandle * _h, DALInterruptID intrID,uint32 nTrigger)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->SetInterruptTrigger( _h,
            intrID, nTrigger);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
DalInterruptController_IsInterruptPending(DalDeviceHandle * _h, DALInterruptID intrID,uint32* bState)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->IsInterruptPending( _h,
            intrID, (void*)bState,1);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
DalInterruptController_IsInterruptEnabled(DalDeviceHandle * _h, DALInterruptID intrID,uint32* bState)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->IsInterruptEnabled( _h,
           intrID, (void*)bState,1);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
DalInterruptController_MapWakeupInterrupt(DalDeviceHandle * _h, DALInterruptID intrID, uint32 WakeupIntID)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->MapWakeupInterrupt( _h,
           intrID, WakeupIntID);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
DalInterruptController_IsAnyInterruptPending(DalDeviceHandle * _h, uint32* bState)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->IsAnyInterruptPending( _h,
           (uint32*)bState,1);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
DalInterruptController_Sleep(DalDeviceHandle * _h,InterruptControllerSleepType sleep)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->Sleep( _h, sleep);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
DalInterruptController_Wakeup(DalDeviceHandle * _h, InterruptControllerSleepType sleep)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->Wakeup( _h, sleep);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
DalInterruptController_GetInterruptTrigger(DalDeviceHandle * _h, DALInterruptID intrID,uint32* eTrigger)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->GetInterruptTrigger( _h,
             intrID,eTrigger,1);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
DalInterruptController_GetInterruptID(DalDeviceHandle * _h, const char * szIntrName,DALInterruptID* pnIntrID)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->GetInterruptID( _h, szIntrName, pnIntrID,1);
  }
  else
  {
    return -1;
  }
}
static __inline DALResult
DalInterruptController_LogState(DalDeviceHandle * _h, void *pULog)
{
   return ((DalInterruptControllerHandle *)_h)->pVtbl->LogState( _h, pULog);
}
typedef struct DALREG_DriverInfo DALREG_DriverInfo;
struct DALREG_DriverInfo
{
 DALResult (*pfnDALNewFunc)(const char * , DALDEVICEID, DalDeviceHandle**);
 uint32 dwNumDevices;
 DALDEVICEID *pDeviceId;
};
typedef struct DALREG_DriverInfoList DALREG_DriverInfoList;
struct DALREG_DriverInfoList
{
 uint32 dwLen;
 DALREG_DriverInfo ** pDriverInfo;
};
typedef void * DALSYSObjHandle;
typedef void * DALSYSSyncHandle;
typedef void * DALSYSMemHandle;
typedef void * DALSYSWorkLoopHandle;
typedef uint32 *DALSYSPropertyHandle;
typedef struct DALSYSPropertyVar DALSYSPropertyVar;
struct DALSYSPropertyVar
{
    uint32 dwType;
    uint32 dwLen;
    union
    {
        byte *pbVal;
        char *pszVal;
        uint32 dwVal;
        uint32 *pdwVal;
        const void *pStruct;
    }Val;
};
typedef struct DALSYSPropStructTblType DALSYSPropStructTblType ;
struct DALSYSPropStructTblType{
   uint32 dwSize;
   const void *pStruct;
};
typedef struct StringDevice StringDevice;
 struct StringDevice{
   char *pszName;
   uint32 dwHash;
   uint32 dwOffset;
   DALREG_DriverInfo *pFunctionName;
   uint32 dwNumCollision;
   uint32 *pdwCollisions;
};
typedef struct DALProps DALProps;
struct DALProps
{
   const byte *pDALPROP_PropBin;
   const DALSYSPropStructTblType *pDALPROP_StructPtrs;
   const uint32 dwDeviceSize;
   const StringDevice *pDevices;
};
typedef struct DALSYSBaseObj DALSYSBaseObj;
struct DALSYSBaseObj
{
   uint32 dwObjInfo;
   uint32 hOwnerProc;
   DALSYSMemAddr thisVirtualAddr;
};
typedef struct DALSYSEventObj DALSYSEventObj;
struct DALSYSEventObj
{
   unsigned long long _bSpace[80/sizeof(unsigned long long)];
};
typedef struct DALSYSSyncObj DALSYSSyncObj;
struct DALSYSSyncObj
{
   unsigned long long _bSpace[40/sizeof(unsigned long long)];
};
typedef struct DALSYSMemObj DALSYSMemObj;
struct DALSYSMemObj
{
   unsigned long long _bSpace[40/sizeof(unsigned long long)];
};
typedef struct DALSYSWorkLoopEventObj DALSYSWorkLoopEventObj;
struct DALSYSWorkLoopEventObj
{
   unsigned long long _bSpace[32/sizeof(unsigned long long)];
};
typedef struct DALSYSWorkLoopObj DALSYSWorkLoopObj;
struct DALSYSWorkLoopObj
{
   unsigned long long _bSpace[64/sizeof(unsigned long long)];
};
typedef void * DALSYSCallbackFuncCtx;
typedef void * (*DALSYSCallbackFunc)(void*,uint32,void*,uint32);
typedef struct DALSYSMemInfo DALSYSMemInfo;
struct DALSYSMemInfo
{
    DALSYSMemAddr VirtualAddr;
    DALSYSMemAddr PhysicalAddr;
    uint32 dwLen;
    uint32 dwMappedLen;
    uint32 dwProps;
};
typedef enum
{
   DALSYS_MEM_CACHE_DEVICE,
   DALSYS_MEM_CACHE_NONE,
   DALSYS_MEM_CACHE_WRITEBACK,
   DALSYS_MEM_CACHE_WRITETHROUGH,
   DALSYS_MEM_CACHE_INVALID
}
DALSYSMemCacheType;
typedef enum
{
   DALSYS_MEM_PERM_NONE=0,
   DALSYS_MEM_PERM_R=0x1,
   DALSYS_MEM_PERM_W=0x2,
   DALSYS_MEM_PERM_RW=0x3,
   DALSYS_MEM_PERM_X=0x4,
   DALSYS_MEM_PERM_RX=0x5,
   DALSYS_MEM_PERM_WX=0x6,
   DALSYS_MEM_PERM_RWX=0x7,
   DALSYS_MEM_PERM_INVALID=0x8
}
DALSYSMemPermType;
typedef enum
{
   DALSYS_MEM_SHARE_ALLOWED,
   DALSYS_MEM_SHARE_NOT_ALLOWED,
   DALSYS_MEM_SHARE_INVALID
}
DALSYSMemShareType;
typedef struct DALSYSMemInfoEx DALSYSMemInfoEx;
struct DALSYSMemInfoEx
{
    DALSYSPhyAddr physicalAddr;
    DALSYSMemAddr virtualAddr;
    DALSYSMemAddr size;
    DALSYSMemCacheType cacheType;
    DALSYSMemPermType permission;
    DALSYSMemShareType shareType;
};
typedef struct DALSYSMemReq DALSYSMemReq;
struct DALSYSMemReq
{
    DALSYSMemInfoEx memInfo;
    DALSYSMemObj *pObj;
    DALBOOL prealloc;
};
typedef enum
{
   DEVCFG_TARGET_INFO_SOC,
   DEVCFG_TARGET_INFO_PLATFORM
}DEVCFG_TARGET_INFO_TYPE;
typedef DALResult
(*DALSYSWorkLoopExecute)
(
    DALSYSEventHandle,
    void *
);
typedef void
(*DALSYS_InitSystemHandleFncPtr)
(
    DalDeviceHandle * hDalDevice
);
typedef DALResult
(*DALSYS_DestroyObjectFncPtr)
(
    DALSYSObjHandle
);
typedef DALResult
(*DALSYS_CopyObjectFncPtr)
(
    DALSYSObjHandle,
    DALSYSObjHandle *
);
typedef DALResult
(*DALSYS_RegisterWorkLoopFncPtr)
(
    uint32,
    uint32,
    DALSYSWorkLoopHandle *,
    DALSYSWorkLoopObj *
);
typedef DALResult
(*DALSYS_RegisterWorkLoopExFncPtr)
(
    char *,
    uint32,
    uint32,
    uint32,
    DALSYSWorkLoopHandle *,
    DALSYSWorkLoopObj *
);
typedef DALResult
(*DALSYS_AddEventToWorkLoopFncPtr)
(
    DALSYSWorkLoopHandle,
    DALSYSWorkLoopExecute,
    void *,
    DALSYSEventHandle,
    DALSYSSyncHandle
);
typedef DALResult
(*DALSYS_DeleteEventFromWorkLoopFncPtr)
(
    DALSYSWorkLoopHandle,
    DALSYSEventHandle
);
typedef DALResult
(*DALSYS_EventCreateFncPtr)
(
    uint32 ,
    DALSYSEventHandle *,
    DALSYSEventObj *
);
typedef DALResult
(*DALSYS_EventCtrlFncPtr)
(
    DALSYSEventHandle,
    uint32,
   uint32,
   void *,
   uint32
);
typedef DALResult
(*DALSYS_EventWaitFncPtr)
(
    DALSYSEventHandle
);
typedef DALResult
(*DALSYS_EventMultipleWaitFncPtr)
(
    DALSYSEventHandle*,
    int,
    uint32,
    uint32 *
);
typedef DALResult
(*DALSYS_SetupCallbackEventFncPtr)
(
    DALSYSEventHandle,
    DALSYSCallbackFunc,
    DALSYSCallbackFuncCtx
);
typedef DALResult
(*DALSYS_SyncCreateFncPtr)
(
    uint32,
    DALSYSSyncHandle *,
    DALSYSSyncObj *
);
typedef void
(*DALSYS_SyncEnterFncPtr)
(
    DALSYSSyncHandle
);
typedef DALResult
(*DALSYS_SyncTryEnterFncPtr)
(
    DALSYSSyncHandle
);
typedef void
(*DALSYS_SyncLeaveFncPtr)
(
    DALSYSSyncHandle
);
typedef DALResult
(*DALSYS_MemRegionAllocFncPtr)
(
    uint32,
    DALSYSMemAddr,
    DALSYSMemAddr,
    uint32,
    DALSYSMemHandle *,
    DALSYSMemObj *
);
typedef DALResult
(*DALSYS_MemRegionMapPhysFncPtr)
(
    DALSYSMemHandle,
    uint32,
    DALSYSMemAddr,
    uint32
);
typedef DALResult
(*DALSYS_MemInfoFncPtr)
(
    DALSYSMemHandle,
    DALSYSMemInfo *
);
typedef DALResult
(*DALSYS_CacheCommandFncPtr)
(
    uint32 ,
    DALSYSMemAddr,
    uint32
);
typedef DALResult
(*DALSYS_MallocFncPtr)
(
    uint32 ,
    void **
);
typedef DALResult
(*DALSYS_FreeFncPtr)
(
    void *
);
typedef void
(*DALSYS_BusyWaitFncPtr)
(
   uint32
);
typedef DALResult
(*DALSYS_MemDescAddBufFncPtr)
(
    DALSysMemDescList *,
    uint32,
    uint32,
    uint32
);
typedef DALResult
(*DALSYS_MemDescPrepareFncPtr)
(
    DALSysMemDescList *,
    uint32
);
typedef DALResult
(*DALSYS_MemDescValidateFncPtr)
(
    DALSysMemDescList *,
    uint32
);
typedef DALResult
(*DALSYS_GetDALPropertyHandleFncPtr)
(
    DALDEVICEID,
    DALSYSPropertyHandle
);
typedef DALResult
(*DAL_DeviceAttachFncPtr)
(
    const char *pszArg,
    DALDEVICEID,
    DalDeviceHandle **
);
typedef DALResult
(*DAL_DeviceDetachFncPtr)
(
    DalDeviceHandle *
);
typedef DALResult
(*DAL_DeviceAttachExFncPtr)
(
 const char *pszArg,
    DALDEVICEID DevId,
    DALInterfaceVersion ClientVersion,
    DalDeviceHandle **phDalDevice
);
typedef DALResult
(*DALSYS_GetPropertyValueFncPtr)
(
    DALSYSPropertyHandle,
    const char *,
    uint32,
    DALSYSPropertyVar *
);
typedef void
(*DALSYS_LogEventFncPtr)
(
    DALDEVICEID,
    uint32,
    const char *
);
typedef struct DALSYSFncPtrTbl DALSYSFncPtrTbl;
struct DALSYSFncPtrTbl
{
    DALSYS_InitSystemHandleFncPtr DALSYS_InitSystemHandleFnc;
    DALSYS_DestroyObjectFncPtr DALSYS_DestroyObjectFnc;
    DALSYS_CopyObjectFncPtr DALSYS_CopyObjectFnc;
    DALSYS_RegisterWorkLoopFncPtr DALSYS_RegisterWorkLoopFnc;
    DALSYS_RegisterWorkLoopExFncPtr DALSYS_RegisterWorkLoopExFnc;
    DALSYS_AddEventToWorkLoopFncPtr DALSYS_AddEventToWorkLoopFnc;
    DALSYS_DeleteEventFromWorkLoopFncPtr DALSYS_DeleteEventFromWorkLoopFnc;
    DALSYS_EventCreateFncPtr DALSYS_EventCreateFnc;
    DALSYS_EventCtrlFncPtr DALSYS_EventCtrlFnc;
    DALSYS_EventWaitFncPtr DALSYS_EventWaitFnc;
    DALSYS_EventMultipleWaitFncPtr DALSYS_EventMultipleWaitFnc;
    DALSYS_SetupCallbackEventFncPtr DALSYS_SetupCallbackEventFnc;
    DALSYS_SyncCreateFncPtr DALSYS_SyncCreateFnc;
    DALSYS_SyncEnterFncPtr DALSYS_SyncEnterFnc;
    DALSYS_SyncTryEnterFncPtr DALSYS_SyncTryEnterFnc;
    DALSYS_SyncLeaveFncPtr DALSYS_SyncLeaveFnc;
    DALSYS_MemRegionAllocFncPtr DALSYS_MemRegionAllocFnc;
    DALSYS_MemRegionMapPhysFncPtr DALSYS_MemRegionMapPhysFnc;
    DALSYS_MemInfoFncPtr DALSYS_MemInfoFnc;
    DALSYS_CacheCommandFncPtr DALSYS_CacheCommandFnc;
    DALSYS_MallocFncPtr DALSYS_MallocFnc;
    DALSYS_FreeFncPtr DALSYS_FreeFnc;
    DALSYS_BusyWaitFncPtr DALSYS_BusyWaitFnc;
    DALSYS_MemDescAddBufFncPtr DALSYS_MemDescAddBufFnc;
    DALSYS_MemDescPrepareFncPtr DALSYS_MemDescPrepareFnc;
    DALSYS_MemDescValidateFncPtr DALSYS_MemDescValidateFnc;
    DALSYS_GetDALPropertyHandleFncPtr DALSYS_GetDALPropertyHandleFnc;
    DAL_DeviceAttachFncPtr DALSYS_DeviceAttachFnc;
    DAL_DeviceAttachExFncPtr DALSYS_DeviceAttachExFnc;
    DAL_DeviceAttachExFncPtr DALSYS_DeviceAttachRemoteFnc;
    DAL_DeviceDetachFncPtr DALSYS_DeviceDetachFnc;
    DALSYS_GetPropertyValueFncPtr DALSYS_GetPropertyValueFnc;
    DALSYS_LogEventFncPtr DALSYS_LogEventFnc;
};
typedef DALResult
(*DALRemote_NewFncPtr)(const char *, DALDEVICEID, DalDeviceHandle **);
typedef int
(*DALRemote_CommonInitFncPtr)(void);
typedef void
(*DALRemote_IPCInitFcnPtr)(uint32);
typedef int
(*DALRemote_InitFncPtr)(void);
typedef DALSYSEventHandle
(*DALRemote_CreateEventFncPtr)(void *pClientCtxt, DALSYSEventHandle hRemote);
typedef uint32
(*DALRemoteInterProcessCallFncPtr)(void *in_buf, void *out_buf);
typedef int
(*DALRemote_DeinitFncPtr)(void);
typedef struct DALRemoteVtbl DALRemoteVtbl;
struct DALRemoteVtbl
{
   DALRemote_NewFncPtr DALRemote_NewFnc;
   DALRemote_CommonInitFncPtr DALRemote_CommonInitFnc;
   DALRemote_InitFncPtr DALRemote_InitFnc;
   DALRemote_DeinitFncPtr DALRemote_DeinitFnc;
   DALRemote_CreateEventFncPtr DALRemote_CreateEventFnc;
   DALRemote_CreateEventFncPtr DALRemote_CreatePayloadEventFnc;
   DALRemoteInterProcessCallFncPtr DALRemoteInterProcessCallFnc;
   DALRemote_IPCInitFcnPtr DALRemote_IPCInitFcn;
};
typedef struct DALSYSConfig DALSYSConfig;
struct DALSYSConfig
{
    void *pNativeEnv;
    uint32 dwConfig;
   DALRemoteVtbl *pRemoteVtbl;
};
typedef long _Int32t;
typedef unsigned long _Uint32t;
typedef int _Ptrdifft;
typedef unsigned int _Sizet;
typedef __builtin_va_list va_list;
typedef long long _Longlong;
typedef unsigned long long _ULonglong;
typedef int _Wchart;
typedef int _Wintt;
typedef va_list _Va_list;
void _Atexit(void (*)(void));
typedef char _Sysch_t;
void _Locksyslock(int);
void _Unlocksyslock(int);
typedef unsigned short __attribute__((__may_alias__)) alias_short;
static alias_short *strict_aliasing_workaround(unsigned short *ptr) __attribute__((always_inline,unused));
static alias_short *strict_aliasing_workaround(unsigned short *ptr)
{
  alias_short *aliasptr = (alias_short *)ptr;
  return aliasptr;
}
typedef _Sizet size_t;
int memcmp(const void *, const void *, size_t) __attribute__((__nothrow__));
void *memcpy(void *, const void *, size_t) __attribute__((__nothrow__));
void *memcpy_v(volatile void *, const volatile void *, size_t) __attribute__((__nothrow__));
void *memset(void *, int, size_t) __attribute__((__nothrow__));
char *strcat(char *, const char *) __attribute__((__nothrow__));
int strcmp(const char *, const char *) __attribute__((__nothrow__));
char *strcpy(char *, const char *) __attribute__((__nothrow__));
size_t strlen(const char *) __attribute__((__nothrow__));
void *memmove(void *, const void *, size_t) __attribute__((__nothrow__));
void *memmove_v(volatile void *, const volatile void *, size_t) __attribute__((__nothrow__));
int strcoll(const char *, const char *) __attribute__((__nothrow__));
size_t strcspn(const char *, const char *) __attribute__((__nothrow__));
char *strerror(int) __attribute__((__nothrow__));
size_t strlcat(char *, const char *, size_t) __attribute__((__nothrow__));
char *strncat(char *, const char *, size_t) __attribute__((__nothrow__));
int strncmp(const char *, const char *, size_t) __attribute__((__nothrow__));
size_t strlcpy(char *, const char *, size_t) __attribute__((__nothrow__));
char *strncpy(char *, const char *, size_t) __attribute__((__nothrow__));
size_t strspn(const char *, const char *) __attribute__((__nothrow__));
char *strtok(char *, const char *) __attribute__((__nothrow__));
char *strsep(char **, const char *) __attribute__((__nothrow__));
size_t strxfrm(char *, const char *, size_t) __attribute__((__nothrow__));
char *strdup(const char *) __attribute__((__nothrow__));
int strcasecmp(const char *, const char *) __attribute__((__nothrow__));
int strncasecmp(const char *, const char *, size_t) __attribute__((__nothrow__));
char *strtok_r(char *, const char *, char **) __attribute__((__nothrow__));
void *memccpy (void *, const void *, int, size_t) __attribute__((__nothrow__));
int strerror_r (int, char *, size_t) __attribute__((__nothrow__));
char *strchr(const char *, int) __attribute__((__nothrow__));
char *strpbrk(const char *, const char *) __attribute__((__nothrow__));
char *strrchr(const char *, int) __attribute__((__nothrow__));
char *strstr(const char *, const char *) __attribute__((__nothrow__));
void *memchr(const void *, int, size_t) __attribute__((__nothrow__));
void
DALSYS_InitMod(DALSYSConfig * pCfg);
void
DALSYS_DeInitMod(void);
void
DALSYS_InitSystemHandle(DalDeviceHandle *hDalDevice);
DALSYS_LogEventFncPtr
DALSYS_SetLogCfg(uint32 dwMaxLogLevel, DALSYS_LogEventFncPtr DALSysLogFcn);
DALResult
DALSYS_DestroyObject(DALSYSObjHandle hObj);
DALResult
DALSYS_CopyObject(DALSYSObjHandle hObjOrig, DALSYSObjHandle *phObjCopy );
DALResult
DALSYS_RegisterWorkLoop(uint32 dwPriority,
                  uint32 dwMaxNumEvents,
                  DALSYSWorkLoopHandle *phWorkLoop,
                  DALSYSWorkLoopObj *pWorkLoopObj);
DALResult
DALSYS_RegisterWorkLoopEx(
                  char * pszname,
                  uint32 dwStackSize,
                  uint32 dwPriority,
                  uint32 dwMaxNumEvents,
                  DALSYSWorkLoopHandle *phWorkLoop,
                  DALSYSWorkLoopObj *pWorkLoopObj);
DALResult
DALSYS_AddEventToWorkLoop(DALSYSWorkLoopHandle hWorkLoop,
                    DALSYSWorkLoopExecute pfnWorkLoopExecute,
                    void * pArg,
                    DALSYSEventHandle hEvent,
                    DALSYSSyncHandle hSync);
DALResult
DALSYS_DeleteEventFromWorkLoop(DALSYSWorkLoopHandle hWorkLoop,
                         DALSYSEventHandle hEvent);
DALResult
DALSYS_EventCreate(uint32 dwAttribs, DALSYSEventHandle *phEvent,
               DALSYSEventObj *pEventObj);
DALResult
DALSYS_EventCopy(DALSYSEventHandle hEvent,
             DALSYSEventHandle *phEventCopy,DALSYSEventObj *pEventObjCopy,
                 uint32 dwMarshalFlags);
DALResult
DALSYS_EventCtrlEx(DALSYSEventHandle hEvent, uint32 dwCtrl, uint32 dwParam,
                   void *pPayload, uint32 dwPayloadSize);
DALResult
DALSYS_EventWait(DALSYSEventHandle hEvent);
DALResult
DALSYS_EventMultipleWait(DALSYSEventHandle* phEvent, int nEvents,
                         uint32 dwTimeoutUs,uint32 *pdwEventIdx);
DALResult
DALSYS_SetupCallbackEvent(DALSYSEventHandle hEvent, DALSYSCallbackFunc cbFunc,
                          DALSYSCallbackFuncCtx cbFuncCtx);
DALResult DALSYS_TimerStart( DALSYSEventHandle hEvent, uint32 time);
DALResult DALSYS_TimerStop( DALSYSEventHandle hEvent );
DALResult
DALSYS_SyncCreate(uint32 dwAttribs,
                  DALSYSSyncHandle *phSync,
                  DALSYSSyncObj *pSyncObj);
void
DALSYS_SyncEnter(DALSYSSyncHandle hSync);
DALResult
DALSYS_SyncTryEnter(DALSYSSyncHandle hSync);
void
DALSYS_SyncLeave(DALSYSSyncHandle hSync);
DALResult
DALSYS_MemRegionAlloc(uint32 dwAttribs, DALSYSMemAddr VirtualAddr,
                      DALSYSMemAddr PhysicalAddr, uint32 dwLen,
                      DALSYSMemHandle *phMem, DALSYSMemObj *pMemObj);
DALResult
DALSYS_MemRegionAllocEx( DALSYSMemHandle *phMem, const DALSYSMemReq *pMemReq,
      DALSYSMemInfoEx *pMemInfo );
DALResult
DALSYS_MemRegionMapPhys(DALSYSMemHandle hMem, uint32 dwVirtualBaseOffset,
                        DALSYSMemAddr PhysicalAddr, uint32 dwLen);
DALResult
DALSYS_MemInfo(DALSYSMemHandle hMem, DALSYSMemInfo *pMemInfo);
DALResult
DALSYS_MemInfoEx(DALSYSMemHandle hMem, DALSYSMemInfoEx *pMemInfo);
DALResult
DALSYS_CacheCommand(uint32 CacheCmd, DALSYSMemAddr VirtualAddr, uint32 dwLen);
DALResult
DALSYS_Malloc(uint32 dwSize, void **ppMem);
DALResult
DALSYS_Free(void *pmem);
void
DALSYS_BusyWait(uint32 pause_time_us);
DALResult
DALSYS_GetDALPropertyHandle(DALDEVICEID DeviceId, DALSYSPropertyHandle hDALProps);
DALResult
DALSYS_GetDALPropertyHandleEx(DALDEVICEID DeviceId,DALSYSPropertyHandle hDALProps,
                              DEVCFG_TARGET_INFO_TYPE target_info_type);
DALResult
DALSYS_GetDALPropertyHandleDyn(DALDEVICEID DeviceId, DALSYSPropertyHandle hDALProps);
DALResult
DALSYS_GetDALPropertyHandleStr(const char *pszDevName, DALSYSPropertyHandle hDALProps);
DALResult
DALSYS_GetDALPropertyHandleStrEx(const char *pszDevName, DALSYSPropertyHandle hDALProps,
                                 DEVCFG_TARGET_INFO_TYPE target_info_type);
DALResult
DALSYS_GetDALPropertyHandleStrDyn(const char *pszDevName, DALSYSPropertyHandle hDALProps);
DALResult
DALSYS_GetPropertyValue(DALSYSPropertyHandle hDALProps, const char *pszName,
                  uint32 dwId,
                   DALSYSPropertyVar *pDALPropVar);
void
DALSYS_LogEvent(DALDEVICEID DeviceId, uint32 dwLogEventType,
      const char * pszFmt, ...);
DALRemoteVtbl *
DALSYS_GetRemoteInterfaceVtbl(void);
uint32 DALSYS_SetThreadPriority(uint32 priority);
uint32 _DALSYS_memscpy(void * pDest, uint32 iDestSz,
      const void * pSrc, uint32 iSrcSize);
typedef struct DALDrvCtxt DALDrvCtxt;
typedef struct DALDevCtxt DALDevCtxt;
typedef struct DALClientCtxt DALClientCtxt;
typedef struct DALDrvVtbl DALDrvVtbl;
struct DALDrvVtbl
{
   int (*DAL_DriverInit)(DALDrvCtxt *);
   int (*DAL_DriverDeInit)(DALDrvCtxt *);
};
struct DALDevCtxt
{
    uint32 dwRefs;
    DALDEVICEID DevId;
    uint32 dwDevCtxtRefIdx;
    DALDrvCtxt *pDALDrvCtxt;
    uint32 hProp[2];
    DalDeviceHandle *hDALSystem;
    DalDeviceHandle *hDALTimer;
    DalDeviceHandle *hDALInterrupt;
    uint32 * pSystemTbl;
    DALSYSWorkLoopHandle * pDefaultWorkLoop;
    const char *strDeviceName;
    uint32 reserve[16-6];
};
struct DALDrvCtxt
{
   DALDrvVtbl DALDrvVtbl;
   uint32 dwNumDev;
   uint32 dwSizeDevCtxt;
   uint32 bInit;
   uint32 dwRefs;
   DALDevCtxt DALDevCtxt[1];
};
struct DALClientCtxt
{
    uint32 dwRefs;
    uint32 dwAccessMode;
    void * pPortCtxt;
    DALDevCtxt *pDALDevCtxt;
};
typedef struct DALInheritSrcPram DALInheritSrcPram;
struct DALInheritSrcPram
{
   const byte * pBuf;
   int dwBufLen;
   uint32 dwSeqIdx;
};
typedef struct DALInheritDestPram DALInheritDestPram;
struct DALInheritDestPram
{
   byte * pBuf;
   uint32 dwBufLen;
   int *pdwObjLen;
   uint32 *dwSeqIdx;
};
typedef struct DALInterface DALInterface;
struct DALInterface
{
    uint32 dwDalHandleId;
    void *pVtbl;
    DALClientCtxt *pclientCtxt;
};
DALResult
DALFW_AttachToStringDevice(const char *pszDeviceName, DALDrvCtxt *pdalDrvCtxt,
                           DALClientCtxt *pClientCtxt);
DALResult
DALFW_AttachToDevice(DALDEVICEID devId,DALDrvCtxt *pdalDrvCtxt,
                     DALClientCtxt *pdalClientCtxt);
void
DALFW_MarkDeviceStatic(DALDevCtxt *pdalDevCtxt);
uint32
DALFW_AddRef(DALClientCtxt *pdalClientCtxt);
uint32
DALFW_Release(DALClientCtxt *pdalClientCtxt);
void
DALFW_SystemAttach(DALDevCtxt *pDevCtxt);
void
DALFW_SystemDetach(DALDevCtxt *pDevCtxt);
DALSYSWorkLoopHandle *
DALFW_GetWorkLoop(DALDevCtxt *pDevCtxt, uint32 dwWorkLoopPriority);
DALMemObject *
DALFW_CopyMemObjectToDDI(DALSYSMemHandle hMem, DALMemObject * pDestMemObject);
DALSYSMemHandle
DALFW_CreateMemObjectFromDDI(uint32 dwMarshalFlags, DALMemObject * pInMemObject);
DALSYSEventHandle
DALFW_CreateEventFromDDI(DALClientCtxt * pClientCtxt, uint32 dwMarshalFlags,
                DALSYSEventHandle inHandle, DALEventObject * pPrealloc);
DALSYSEventHandle
DALFW_CreatePayloadEventFromDDI(DALClientCtxt * pClientCtxt, uint32 dwMarshalFlags,
                DALSYSEventHandle inHandle, DALEventObject * pPrealloc);
uint32
DALFW_InitDDIMemDescListFromSys(uint32 dwFlags,
                             DALSysMemDescList *pInMemDescList,
                             DALDDIMemDescList * pDestDDIMemDescList);
DALSysMemDescList *
DALFW_InitSysMemDescListFromDDI(DALSYSMemHandle hMemHandle,
                             DALDDIMemDescList *pInDDIMemDescList,
                             DALSysMemDescList *pDestMemDescList);
void
DALFW_RegisterSystemNotificationEvent(DalDeviceHandle * h, DALSYSEventHandle hEvent);
void memory_barrier(void);
DALSysMemDescBuf *
DALFW_MemDescBufPtr(DALSysMemDescList * pMemDescList, uint32 idx);
DALResult
DALFW_MemDescInit(DALSYSMemHandle hMem, DALSysMemDescList * pMemDescList,
                  uint32 dwNumBufs);
DALResult
DALFW_MemDescAddBuf(DALSysMemDescList * pMemDescList, uint32 bufIdx,
                    uint32 offset, uint32 size, uint32 userInfo);
DALSYSMemHandle
DALFW_AllocPhysForMemDescBufs(DALSYSMemHandle hMem,
                              uint32 operation,
                              DALSysMemDescList *pInDescList,
                              DALSysMemDescList ** pOutDescList);
DALResult
DALFW_MemDescListCopyBufs(DALSysMemDescList * pDestDescList,
                          DALSysMemDescList * pSrcDescList);
uint32 DALFW_LockedExchangeW(volatile uint32 *pTarget, uint32 value);
uint32 DALFW_LockedIncrementW(volatile uint32 *pTarget);
uint32 DALFW_LockedDecrementW(volatile uint32 *pTarget);
uint32 DALFW_LockedGetModifySetW(volatile uint32 *pTarget, uint32 AND_value, uint32 OR_value);
uint32 DALFW_LockedCompareExchangeW(volatile uint32 *pTarget, uint32 comparand, uint32 value);
void DALFW_LockSpinLock(volatile uint32 *spinLock, uint32 pid);
void DALFW_UnLockSpinLock(volatile uint32 *spinLock);
void DALFW_LockSpinLockExt(volatile uint32 *spinLock, uint32 pid);
void DALFW_UnLockSpinLockExt(volatile uint32 *spinLock);
DALResult DALFW_TryLockSpinLockExt(volatile uint32 *spinLock, uint32 pid);
typedef struct _DAFFW_MPLOCK
{
  volatile uint32 spin_lock;
  volatile uint32 owner;
  volatile uint32 waiting;
   volatile uint32 size;
}
DALFW_MPLOCK;
void DALFW_MPLock(DALFW_MPLOCK *pLock, uint32 pid, uint32 delayParam);
void DALFW_MPUnLock(DALFW_MPLOCK *pLock);
uint32 DALFW_TrySpinLockEx(DALFW_MPLOCK *pLock, uint32 pid);
typedef struct InterruptControllerDrvCtxt InterruptControllerDrvCtxt;
typedef struct InterruptControllerDevCtxt InterruptControllerDevCtxt;
typedef struct InterruptControllerClientCtxt InterruptControllerClientCtxt;
typedef struct InterruptControllerDALVtbl InterruptControllerDALVtbl;
struct InterruptControllerDALVtbl
{
   int (*InterruptController_DriverInit)(InterruptControllerDrvCtxt *);
   int (*InterruptController_DriverDeInit)(InterruptControllerDrvCtxt *);
};
struct InterruptControllerDevCtxt
{
   uint32 dwRefs;
    DALDEVICEID DevId;
    uint32 dwDevCtxtRefIdx;
    InterruptControllerDrvCtxt *pInterruptControllerDrvCtxt;
   uint32 hProp[2];
    uint32 Reserved[16];
};
struct InterruptControllerDrvCtxt
{
   InterruptControllerDALVtbl InterruptControllerDALVtbl;
   uint32 dwNumDev;
   uint32 dwSizeDevCtxt;
   uint32 bInit;
    uint32 dwRefs;
   InterruptControllerDevCtxt InterruptControllerDevCtxt[1];
};
struct InterruptControllerClientCtxt
{
   uint32 dwRefs;
    uint32 dwAccessMode;
    void *pPortCtxt;
    InterruptControllerDevCtxt *pInterruptControllerDevCtxt;
    DalInterruptControllerHandle DalInterruptControllerHandle;
   DALSYSSyncHandle hSyncIntrCtrlTbl;
   uint32 IntrCtrlTblNextSlot;
   void * IntrCtrlTbl[512];
};
DALResult InterruptController_DriverInit(InterruptControllerDrvCtxt *);
DALResult InterruptController_DriverDeInit(InterruptControllerDrvCtxt *);
DALResult InterruptController_DeviceInit(InterruptControllerClientCtxt *);
DALResult InterruptController_DeviceDeInit(InterruptControllerClientCtxt *);
DALResult InterruptController_Reset(InterruptControllerClientCtxt *);
DALResult InterruptController_PowerEvent(InterruptControllerClientCtxt *, DalPowerCmd, DalPowerDomain);
DALResult InterruptController_Open(InterruptControllerClientCtxt *, uint32);
DALResult InterruptController_Close(InterruptControllerClientCtxt *);
DALResult InterruptController_Info(InterruptControllerClientCtxt *,DalDeviceInfo *, uint32);
DALResult InterruptController_InheritObjects(InterruptControllerClientCtxt *,DALInheritSrcPram *,DALInheritDestPram *);
DALResult InterruptController_RegisterISR( InterruptControllerClientCtxt *, DALInterruptID , const DALISR , const DALISRCtx , uint32 );
DALResult InterruptController_RegisterIST( InterruptControllerClientCtxt *,DALInterruptID , const DALISR , const DALISRCtx , uint32, char * );
DALResult InterruptController_RegisterEvent( InterruptControllerClientCtxt *, DALInterruptID , const DALSYSEventHandle , uint32 );
DALResult InterruptController_Unregister( InterruptControllerClientCtxt *, DALInterruptID );
DALResult InterruptController_InterruptDone( InterruptControllerClientCtxt *, DALInterruptID );
DALResult InterruptController_InterruptEnable( InterruptControllerClientCtxt *, DALInterruptID );
DALResult InterruptController_InterruptDisable( InterruptControllerClientCtxt *, DALInterruptID );
DALResult InterruptController_InterruptTrigger( InterruptControllerClientCtxt *, DALInterruptID );
DALResult InterruptController_InterruptClear( InterruptControllerClientCtxt *, DALInterruptID );
DALResult InterruptController_InterruptStatus( InterruptControllerClientCtxt *, DALInterruptID );
DALResult InterruptController_SetTrigger(InterruptControllerClientCtxt *, DALInterruptID ,uint32 );
DALResult InterruptController_IsInterruptPending(InterruptControllerClientCtxt *, DALInterruptID ,uint32*);
DALResult InterruptController_IsInterruptEnabled(InterruptControllerClientCtxt *, DALInterruptID,uint32*);
DALResult InterruptController_IsAnyInterruptPending(InterruptControllerClientCtxt *,uint32* bState);
DALResult InterruptController_MapWakeupInterrupt(InterruptControllerClientCtxt * , DALInterruptID ,uint32 );
DALResult InterruptController_Sleep(InterruptControllerClientCtxt * , InterruptControllerSleepType sleep);
DALResult InterruptController_Wakeup(InterruptControllerClientCtxt * , InterruptControllerSleepType sleep);
DALResult InterruptController_GetInterruptTrigger(InterruptControllerClientCtxt *, DALInterruptID ,uint32* );
DALResult InterruptController_LogState(InterruptControllerClientCtxt *,void *);
DALResult InterruptController_GetInterruptID(InterruptControllerClientCtxt *pclientCtxt,const char* szIntrName,uint32* pnIntrID);
static inline unsigned int
qurt_atomic_set(unsigned int* target, unsigned int value)
{
    unsigned long tmp;
    __asm__ __volatile__(
        "1:     %0 = memw_locked(%2)\n"
        "       memw_locked(%2, p0) = %3\n"
        "       if !p0 jump 1b\n"
        : "=&r" (tmp),"+m" (*target)
        : "r" (target), "r" (value)
        : "p0");
    return value;
}
static inline void
qurt_atomic_and(unsigned int* target, unsigned int mask)
{
    unsigned int result;
    __asm__ __volatile__(
        "1:     %0 = memw_locked(%2)\n"
        "       %0 = and(%0, %3)\n"
        "       memw_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*target)
        : "r" (target),"r" (mask)
        : "p0");
}
static inline unsigned int
qurt_atomic_and_return(unsigned int* target, unsigned int mask)
{
    unsigned int result;
    __asm__ __volatile__(
        "1:     %0 = memw_locked(%2)\n"
        "       %0 = and(%0, %3)\n"
        "       memw_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*target)
        : "r" (target), "r" (mask)
        : "p0");
    return result;
}
static inline void
qurt_atomic_or(unsigned int* target, unsigned int mask)
{
    unsigned int result;
    __asm__ __volatile__(
        "1:     %0 = memw_locked(%2)\n"
        "       %0 = or(%0, %3)\n"
        "       memw_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*target)
        : "r" (target), "r" (mask)
        : "p0");
}
static inline unsigned int
qurt_atomic_or_return(unsigned int* target, unsigned int mask)
{
    unsigned int result;
    __asm__ __volatile__(
        "1:     %0 = memw_locked(%2)\n"
        "       %0 = or(%0, %3)\n"
        "       memw_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*target)
        : "r" (target), "r" (mask)
        : "p0");
    return result;
}
static inline void
qurt_atomic_xor(unsigned int* target, unsigned int mask)
{
    unsigned int result;
    __asm__ __volatile__(
        "1:     %0 = memw_locked(%2)\n"
        "       %0 = xor(%0, %3)\n"
        "       memw_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*target)
        : "r" (target), "r" (mask)
        : "p0");
}
static inline unsigned int
qurt_atomic_xor_return(unsigned int* target, unsigned int mask)
{
    unsigned int result;
    __asm__ __volatile__(
        "1:     %0 = memw_locked(%2)\n"
        "       %0 = xor(%0, %3)\n"
        "       memw_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*target)
        : "r" (target), "r" (mask)
        : "p0");
    return result;
}
static inline void
qurt_atomic_set_bit(unsigned int *target, unsigned int bit)
{
    unsigned int result;
  int aword = bit / (sizeof(unsigned int) * 8);
    unsigned int sbit = bit % (sizeof(unsigned int) * 8);
  unsigned int *wtarget= (unsigned int *)&target[aword];
    __asm__ __volatile__(
        "1:     %0 = memw_locked(%2)\n"
        "       %0 = setbit(%0, %3)\n"
        "       memw_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*wtarget)
        : "r" (wtarget), "r" (sbit)
        : "p0");
}
static inline void
qurt_atomic_clear_bit(unsigned int *target, unsigned int bit)
{
    unsigned int result;
  int aword = bit / (sizeof(unsigned int) * 8);
    unsigned int sbit = bit % (sizeof(unsigned int) * 8);
  unsigned int *wtarget= (unsigned int *)&target[aword];
    __asm__ __volatile__(
        "1:     %0 = memw_locked(%2)\n"
        "       %0 = clrbit(%0, %3)\n"
        "       memw_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*wtarget)
        : "r" (wtarget), "r" (sbit)
        : "p0");
}
static inline void
qurt_atomic_change_bit(unsigned int *target, unsigned int bit)
{
    unsigned int result;
  int aword = bit / (sizeof(unsigned int) * 8);
    unsigned int sbit = bit & 0x1f;
  unsigned int *wtarget= (unsigned int *)&target[aword];
    __asm__ __volatile__(
        "1:     %0 = memw_locked(%2)\n"
        "       %0 = togglebit(%0, %3)\n"
        "       memw_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*wtarget)
        : "r" (wtarget),"r" (sbit)
        : "p0");
}
static inline void
qurt_atomic_add(unsigned int *target, unsigned int v)
{
    unsigned int result;
    __asm__ __volatile__(
        "1:     %0 = memw_locked(%2)\n"
        "       %0 = add(%0, %3)\n"
        "       memw_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*target)
        : "r" (target), "r" (v)
        : "p0");
}
static inline unsigned int
qurt_atomic_add_return(unsigned int *target, unsigned int v)
{
    unsigned int result;
    __asm__ __volatile__(
        "1:     %0 = memw_locked(%2)\n"
        "       %0 = add(%0, %3)\n"
        "       memw_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*target)
        : "r" (target), "r" (v)
        : "p0");
    return result;
}
static inline void
qurt_atomic_sub(unsigned int *target, unsigned int v)
{
    unsigned int result;
    __asm__ __volatile__(
        "1:     %0 = memw_locked(%2)\n"
        "       %0 = sub(%0, %3)\n"
        "       memw_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*target)
        : "r" (target), "r" (v)
        : "p0");
}
static inline unsigned int
qurt_atomic_sub_return(unsigned int *target, unsigned int v)
{
    unsigned int result;
    __asm__ __volatile__(
        "1:     %0 = memw_locked(%2)\n"
        "       %0 = sub(%0, %3)\n"
        "       memw_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*target)
        : "r" (target), "r" (v)
        : "p0");
    return result;
}
static inline void
qurt_atomic_inc(unsigned int *target)
{
    unsigned int result;
    __asm__ __volatile__(
        "1:     %0 = memw_locked(%2)\n"
        "       %0 = add(%0, #1)\n"
        "       memw_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*target)
        : "r" (target)
        : "p0");
}
static inline unsigned int
qurt_atomic_inc_return(unsigned int *target)
{
    unsigned int result;
    __asm__ __volatile__(
        "1:     %0 = memw_locked(%2)\n"
        "       %0 = add(%0, #1)\n"
        "       memw_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*target)
        : "r" (target)
        : "p0");
    return result;
}
static inline void
qurt_atomic_dec(unsigned int *target)
{
    unsigned int result;
    __asm__ __volatile__(
        "1:     %0 = memw_locked(%2)\n"
        "       %0 = add(%0, #-1)\n"
        "       memw_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*target)
        : "r" (target)
        : "p0");
}
static inline unsigned int
qurt_atomic_dec_return(unsigned int *target)
{
    unsigned int result;
    __asm__ __volatile__(
        "1:     %0 = memw_locked(%2)\n"
        "       %0 = add(%0, #-1)\n"
        "       memw_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*target)
        : "r" (target)
        : "p0");
    return result;
}
static inline unsigned int
qurt_atomic_compare_and_set(unsigned int* target,
                       unsigned int old_val,
                       unsigned int new_val)
{
    unsigned int current_val;
    __asm__ __volatile__(
        "1:     %0 = memw_locked(%2)\n"
        "       p0 = cmp.eq(%0, %3)\n"
        "       if !p0 jump 2f\n"
        "       memw_locked(%2, p0) = %4\n"
        "       if !p0 jump 1b\n"
        "2:\n"
        : "=&r" (current_val),"+m" (*target)
        : "r" (target), "r" (old_val), "r" (new_val)
        : "p0");
    return current_val == old_val;
}
static inline void
qurt_atomic_barrier(void)
{
    __asm__ __volatile__ (
        ""
        :
        :
        :
        "memory");
}
static inline void
qurt_atomic_barrier_write(void)
{
    qurt_atomic_barrier();
}
static inline void
qurt_atomic_barrier_write_smp(void)
{
    qurt_atomic_barrier();
}
static inline void
qurt_atomic_barrier_read(void)
{
    qurt_atomic_barrier();
}
static inline void
qurt_atomic_barrier_read_smp(void)
{
    qurt_atomic_barrier();
}
static inline void
qurt_atomic_barrier_smp(void)
{
    qurt_atomic_barrier();
}
static inline unsigned long long
qurt_atomic64_set(unsigned long long* target, unsigned long long value)
{
    unsigned long long tmp;
    __asm__ __volatile__(
        "1:     %0 = memd_locked(%2)\n"
        "       memd_locked(%2, p0) = %3\n"
        "       if !p0 jump 1b\n"
        : "=&r" (tmp),"+m" (*target)
        : "r" (target), "r" (value)
        : "p0");
    return value;
}
static inline void
qurt_atomic64_and(unsigned long long* target, unsigned long long mask)
{
    unsigned long long result;
    __asm__ __volatile__(
        "1:     %0 = memd_locked(%2)\n"
        "       %0 = and(%0, %3)\n"
        "       memd_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*target)
        : "r" (target),"r" (mask)
        : "p0");
}
static inline unsigned long long
qurt_atomic64_and_return(unsigned long long* target, unsigned long long mask)
{
    unsigned long long result;
    __asm__ __volatile__(
        "1:     %0 = memd_locked(%2)\n"
        "       %0 = and(%0, %3)\n"
        "       memd_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*target)
        : "r" (target), "r" (mask)
        : "p0");
    return result;
}
static inline void
qurt_atomic64_or(unsigned long long* target, unsigned long long mask)
{
    unsigned long long result;
    __asm__ __volatile__(
        "1:     %0 = memd_locked(%2)\n"
        "       %0 = or(%0, %3)\n"
        "       memd_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*target)
        : "r" (target), "r" (mask)
        : "p0");
}
static inline unsigned long long
qurt_atomic64_or_return(unsigned long long* target, unsigned long long mask)
{
    unsigned long long result;
    __asm__ __volatile__(
        "1:     %0 = memd_locked(%2)\n"
        "       %0 = or(%0, %3)\n"
        "       memd_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*target)
        : "r" (target), "r" (mask)
        : "p0");
    return result;
}
static inline void
qurt_atomic64_xor(unsigned long long* target, unsigned long long mask)
{
    unsigned long long result;
    __asm__ __volatile__(
        "1:     %0 = memd_locked(%2)\n"
        "       %0 = xor(%0, %3)\n"
        "       memd_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*target)
        : "r" (target), "r" (mask)
        : "p0");
}
static inline unsigned long long
qurt_atomic64_xor_return(unsigned long long* target, unsigned long long mask)
{
    unsigned long long result;
    __asm__ __volatile__(
        "1:     %0 = memd_locked(%2)\n"
        "       %0 = xor(%0, %3)\n"
        "       memd_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*target)
        : "r" (target), "r" (mask)
        : "p0");
    return result;
}
static inline void
qurt_atomic64_set_bit(unsigned long long *target, unsigned int bit)
{
    unsigned int result;
  unsigned int *wtarget;
  unsigned int *pwtarget = (unsigned int *)target;
  int aword = bit / (sizeof(unsigned int) * 8);
    unsigned int sbit = bit & 0x1F;
  wtarget = (unsigned int *)&pwtarget[aword];
    __asm__ __volatile__(
        "1:     %0 = memw_locked(%2)\n"
        "       %0 = setbit(%0, %3)\n"
        "       memw_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*wtarget)
        : "r" (wtarget), "r" (sbit)
        : "p0");
}
static inline void
qurt_atomic64_clear_bit(unsigned long long *target, unsigned int bit)
{
    unsigned int result;
  unsigned int *wtarget;
  unsigned int *pwtarget = (unsigned int *)target;
  int aword = bit / (sizeof(unsigned int) * 8);
    unsigned int sbit = bit & 0x1F;
  wtarget = (unsigned int *)&pwtarget[aword];
    __asm__ __volatile__(
        "1:     %0 = memw_locked(%2)\n"
        "       %0 = clrbit(%0, %3)\n"
        "       memw_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*wtarget)
        : "r" (wtarget), "r" (sbit)
        : "p0");
}
static inline void
qurt_atomic64_change_bit(unsigned long long *target, unsigned int bit)
{
    unsigned int result;
  unsigned int *wtarget;
  unsigned int *pwtarget = (unsigned int *)target;
  int aword = bit / (sizeof(unsigned int) * 8);
    unsigned int sbit = bit & 0x1F;
  wtarget = (unsigned int *)&pwtarget[aword];
    __asm__ __volatile__(
        "1:     %0 = memw_locked(%2)\n"
        "       %0 = togglebit(%0, %3)\n"
        "       memw_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*wtarget)
        : "r" (wtarget),"r" (sbit)
        : "p0");
}
static inline void
qurt_atomic64_add(unsigned long long *target, unsigned long long v)
{
    unsigned long long result;
    __asm__ __volatile__(
        "1:     %0 = memd_locked(%2)\n"
        "       %0 = add(%0, %3)\n"
        "       memd_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*target)
        : "r" (target), "r" (v)
        : "p0");
}
static inline unsigned long long
qurt_atomic64_add_return(unsigned long long *target, unsigned long long v)
{
    unsigned long long result;
    __asm__ __volatile__(
        "1:     %0 = memd_locked(%2)\n"
        "       %0 = add(%0, %3)\n"
        "       memd_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*target)
        : "r" (target), "r" (v)
        : "p0");
    return result;
}
static inline void
qurt_atomic64_sub(unsigned long long *target, unsigned long long v)
{
    unsigned long long result;
    __asm__ __volatile__(
        "1:     %0 = memd_locked(%2)\n"
        "       %0 = sub(%0, %3)\n"
        "       memd_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*target)
        : "r" (target), "r" (v)
        : "p0");
}
static inline unsigned long long
qurt_atomic64_sub_return(unsigned long long *target, unsigned long long v)
{
    unsigned long long result;
    __asm__ __volatile__(
        "1:     %0 = memd_locked(%2)\n"
        "       %0 = sub(%0, %3)\n"
        "       memd_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*target)
        : "r" (target), "r" (v)
        : "p0");
    return result;
}
static inline void
qurt_atomic64_inc(unsigned long long *target)
{
    unsigned long long result;
  unsigned long long inc =1;
    __asm__ __volatile__(
        "1:     %0 = memd_locked(%2)\n"
        "       %0 = add(%0, %3)\n"
        "       memd_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*target)
        : "r" (target),"r" (inc)
        : "p0");
}
static inline unsigned long long
qurt_atomic64_inc_return(unsigned long long *target)
{
    unsigned long long result;
  unsigned long long inc =1;
    __asm__ __volatile__(
        "1:     %0 = memd_locked(%2)\n"
        "       %0 = add(%0, %3)\n"
        "       memd_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*target)
        : "r" (target),"r" (inc)
        : "p0");
    return result;
}
static inline void
qurt_atomic64_dec(unsigned long long *target)
{
    unsigned long long result;
  unsigned long long minus1 = -1;
    __asm__ __volatile__(
        "1:     %0 = memd_locked(%2)\n"
        "       %0 = add(%0, %3)\n"
        "       memd_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*target)
        : "r" (target),"r" (minus1)
        : "p0");
}
static inline unsigned long long
qurt_atomic64_dec_return(unsigned long long *target)
{
    unsigned long long result;
  unsigned long long minus1 = -1;
    __asm__ __volatile__(
        "1:     %0 = memd_locked(%2)\n"
        "       %0 = add(%0, %3)\n"
        "       memd_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*target)
        : "r" (target),"r" (minus1)
        : "p0");
    return result;
}
static inline int
qurt_atomic64_compare_and_set(unsigned long long *target,
                       unsigned long long old_val,
                       unsigned long long new_val)
{
    unsigned long long current_val;
    __asm__ __volatile__(
        "1:     %0 = memd_locked(%2)\n"
        "       p0 = cmp.eq(%0, %3)\n"
        "       if !p0 jump 2f\n"
        "       memd_locked(%2, p0) = %4\n"
        "       if !p0 jump 1b\n"
        "2:\n"
        : "=&r" (current_val),"+m" (*target)
        : "r" (target), "r" (old_val), "r" (new_val)
        : "p0");
    return current_val ==old_val;
}
static inline void
qurt_atomic64_barrier(void)
{
    __asm__ __volatile__ (
        ""
        :
        :
        :
        "memory");
}
static inline void
qurt_atomic64_barrier_write(void)
{
    qurt_atomic64_barrier();
}
static inline void
qurt_atomic64_barrier_write_smp(void)
{
    qurt_atomic64_barrier();
}
static inline void
qurt_atomic64_barrier_read(void)
{
    qurt_atomic64_barrier();
}
static inline void
qurt_atomic64_barrier_read_smp(void)
{
    qurt_atomic64_barrier();
}
static inline void
qurt_atomic64_barrier_smp(void)
{
    qurt_atomic64_barrier();
}
typedef unsigned int atomic_plain_word_t;
typedef struct {
    volatile atomic_plain_word_t value;
} atomic_word_t;
static inline void
atomic_init(atomic_word_t *a, atomic_plain_word_t v)
{
    a->value = v;
}
atomic_plain_word_t atomic_set(atomic_word_t* target,
                                      atomic_plain_word_t value);
void atomic_and(atomic_word_t* target,
                       atomic_plain_word_t mask);
atomic_plain_word_t atomic_and_return(atomic_word_t* target,
                                             atomic_plain_word_t mask);
void atomic_or(atomic_word_t* target,
                      atomic_plain_word_t mask);
atomic_plain_word_t atomic_or_return(atomic_word_t* target,
                                            atomic_plain_word_t mask);
void atomic_xor(atomic_word_t* target,
                       atomic_plain_word_t mask);
atomic_plain_word_t atomic_xor_return(atomic_word_t* target,
                                             atomic_plain_word_t mask);
void atomic_set_bit(atomic_word_t *target, unsigned int bit);
void atomic_clear_bit(atomic_word_t *target, unsigned int bit);
void atomic_change_bit(atomic_word_t *target, unsigned int bit);
void atomic_add(atomic_word_t *target, atomic_plain_word_t v);
atomic_plain_word_t atomic_add_return(atomic_word_t *target,
                                             atomic_plain_word_t v);
void atomic_sub(atomic_word_t *target, atomic_plain_word_t v);
atomic_plain_word_t atomic_sub_return(atomic_word_t *target,
                                             atomic_plain_word_t v);
void atomic_inc(atomic_word_t *target);
atomic_plain_word_t atomic_inc_return(atomic_word_t *target);
void atomic_dec(atomic_word_t *target);
atomic_plain_word_t atomic_dec_return(atomic_word_t *target);
int atomic_compare_and_set(atomic_word_t *target,
                                  atomic_plain_word_t old_val,
                                  atomic_plain_word_t new_val);
void atomic_barrier_write(void);
void atomic_barrier_write_smp(void);
void atomic_barrier_read(void);
void atomic_barrier_read_smp(void);
void atomic_barrier(void);
void atomic_barrier_smp(void);
static inline atomic_plain_word_t atomic_read(atomic_word_t *target)
{
    return target->value;
}
static inline void atomic_compiler_barrier(void)
{
    asm volatile (""::: "memory");
}
typedef unsigned long long atomic64_plain_word_t;
typedef struct {
    volatile atomic64_plain_word_t value;
} atomic64_word_t;
static inline void
atomic64_init(atomic64_word_t *a, atomic64_plain_word_t v)
{
    a->value = v;
}
atomic64_plain_word_t atomic64_set(atomic64_word_t* target,
                                      atomic64_plain_word_t value);
void atomic64_and(atomic64_word_t* target,
                       atomic64_plain_word_t mask);
atomic64_plain_word_t atomic64_and_return(atomic64_word_t* target,
                                             atomic64_plain_word_t mask);
void atomic64_or(atomic64_word_t* target,
                      atomic64_plain_word_t mask);
atomic64_plain_word_t atomic64_or_return(atomic64_word_t* target,
                                            atomic64_plain_word_t mask);
void atomic64_xor(atomic64_word_t* target,
                       atomic64_plain_word_t mask);
atomic64_plain_word_t atomic64_xor_return(atomic64_word_t* target,
                                             atomic64_plain_word_t mask);
void atomic64_set_bit(atomic64_word_t *target, unsigned int bit);
void atomic64_clear_bit(atomic64_word_t *target, unsigned int bit);
void atomic64_change_bit(atomic64_word_t *target, unsigned int bit);
void atomic64_add(atomic64_word_t *target, atomic64_plain_word_t v);
atomic64_plain_word_t atomic64_add_return(atomic64_word_t *target,
                                             atomic64_plain_word_t v);
void atomic64_sub(atomic64_word_t *target, atomic64_plain_word_t v);
atomic64_plain_word_t atomic64_sub_return(atomic64_word_t *target,
                                             atomic64_plain_word_t v);
void atomic64_inc(atomic64_word_t *target);
atomic64_plain_word_t atomic64_inc_return(atomic64_word_t *target);
void atomic64_dec(atomic64_word_t *target);
atomic64_plain_word_t atomic64_dec_return(atomic64_word_t *target);
int atomic64_compare_and_set(atomic64_word_t *target,
                                  atomic64_plain_word_t old_val,
                                  atomic64_plain_word_t new_val);
void atomic64_barrier_write(void);
void atomic64_barrier_write_smp(void);
void atomic64_barrier_read(void);
void atomic64_barrier_read_smp(void);
void atomic64_barrier(void);
void atomic64_barrier_smp(void);
static inline atomic64_plain_word_t atomic64_read(atomic64_word_t *target)
{
    return target->value;
}
static inline void atomic64_compiler_barrier(void)
{
    asm volatile (""::: "memory");
}
typedef union {
    unsigned long long int raw;
    struct {
        unsigned int signals;
        unsigned int waiting;
        unsigned int queue;
        unsigned int attribute;
    }X;
} qurt_signal_t;
void qurt_signal_init(qurt_signal_t *signal);
void qurt_signal_destroy(qurt_signal_t *signal);
unsigned int qurt_signal_wait(qurt_signal_t *signal, unsigned int mask,
                unsigned int attribute);
static inline unsigned int qurt_signal_wait_any(qurt_signal_t *signal, unsigned int mask)
{
  return qurt_signal_wait(signal, mask, 0x00000000);
}
static inline unsigned int qurt_signal_wait_all(qurt_signal_t *signal, unsigned int mask)
{
  return qurt_signal_wait(signal, mask, 0x00000001);
}
void qurt_signal_set(qurt_signal_t *signal, unsigned int mask);
unsigned int qurt_signal_get(qurt_signal_t *signal);
void qurt_signal_clear(qurt_signal_t *signal, unsigned int mask);
int qurt_signal_wait_cancellable(qurt_signal_t *signal, unsigned int mask,
                                 unsigned int attribute,
                                 unsigned int *return_mask);
typedef qurt_signal_t qurt_anysignal_t;
static inline void qurt_anysignal_init(qurt_anysignal_t *signal)
{
  qurt_signal_init(signal);
}
static inline void qurt_anysignal_destroy(qurt_anysignal_t *signal)
{
  qurt_signal_destroy(signal);
}
static inline unsigned int qurt_anysignal_wait(qurt_anysignal_t *signal, unsigned int mask)
{
  return qurt_signal_wait(signal, mask, 0x00000000);
}
unsigned int qurt_anysignal_set(qurt_anysignal_t *signal, unsigned int mask);
static inline unsigned int qurt_anysignal_get(qurt_anysignal_t *signal)
{
  return qurt_signal_get(signal);
}
unsigned int qurt_anysignal_clear(qurt_anysignal_t *signal, unsigned int mask);
typedef enum {
    CCCC_PARTITION = 0,
    MAIN_PARTITION = 1,
    AUX_PARTITION = 2,
    MINIMUM_PARTITION = 3
} qurt_cache_partition_t;
typedef unsigned int qurt_thread_t;
typedef struct _qurt_thread_attr {
    char name[16];
    unsigned char tcb_partition;
    unsigned char affinity;
    unsigned short priority;
    unsigned char asid;
    unsigned char bus_priority;
    unsigned short timetest_id;
    unsigned int stack_size;
    void *stack_addr;
} qurt_thread_attr_t;
static inline void qurt_thread_attr_init (qurt_thread_attr_t *attr)
{
    attr->name[0] = 0;
    attr->tcb_partition = 0;
    attr->priority = 255;
    attr->asid = 0;
    attr->affinity = (-1);
    attr->bus_priority = 255;
    attr->timetest_id = (-2);
    attr->stack_size = 0;
    attr->stack_addr = 0;
}
static inline void qurt_thread_attr_set_name (qurt_thread_attr_t *attr, char *name)
{
    strlcpy (attr->name, name, 16);
    attr->name[16 - 1] = 0;
}
static inline void qurt_thread_attr_set_tcb_partition (qurt_thread_attr_t *attr, unsigned char tcb_partition)
{
    attr->tcb_partition = tcb_partition;
}
static inline void qurt_thread_attr_set_priority (qurt_thread_attr_t *attr, unsigned short priority)
{
    attr->priority = priority;
}
static inline void qurt_thread_attr_set_affinity (qurt_thread_attr_t *attr, unsigned char affinity)
{
    attr->affinity = affinity;
}
static inline void qurt_thread_attr_set_timetest_id (qurt_thread_attr_t *attr, unsigned short timetest_id)
{
    attr->timetest_id = timetest_id;
}
static inline void qurt_thread_attr_set_stack_size (qurt_thread_attr_t *attr, unsigned int stack_size)
{
    attr->stack_size = stack_size;
}
static inline void qurt_thread_attr_set_stack_addr (qurt_thread_attr_t *attr, void *stack_addr)
{
    attr->stack_addr = stack_addr;
}
static inline void qurt_thread_attr_set_bus_priority ( qurt_thread_attr_t *attr, unsigned short bus_priority)
{
    attr->bus_priority = bus_priority;
}
void qurt_thread_get_name (char *name, unsigned char max_len);
int qurt_thread_create (qurt_thread_t *thread_id, qurt_thread_attr_t *attr, void (*entrypoint) (void *), void *arg);
void qurt_thread_stop(void);
int qurt_thread_resume(unsigned int thread_id);
qurt_thread_t qurt_thread_get_id (void);
qurt_cache_partition_t qurt_thread_get_l2cache_partition (void);
void qurt_thread_set_timetest_id (unsigned short tid);
void qurt_thread_set_cache_partition(qurt_cache_partition_t l1_icache, qurt_cache_partition_t l1_dcache, qurt_cache_partition_t l2_cache);
void qurt_thread_set_coprocessor(unsigned int enable, unsigned int coproc_id);
unsigned short qurt_thread_get_timetest_id (void);
void qurt_thread_exit(int status);
int qurt_thread_join(unsigned int tid, int *status);
unsigned int qurt_thread_get_anysignal(void);
int qurt_thread_get_priority (qurt_thread_t threadid);
int qurt_thread_set_priority (qurt_thread_t threadid, unsigned short newprio);
unsigned int qurt_api_version(void);
int qurt_thread_attr_get (qurt_thread_t thread_id, qurt_thread_attr_t *attr);
void *qurt_malloc( unsigned int size);
void *qurt_calloc(unsigned int elsize, unsigned int num);
void *qurt_realloc(void *ptr, int newsize);
void qurt_free( void *ptr);
int qurt_futex_wait(void *lock, int val);
int qurt_futex_wait_cancellable(void *lock, int val);
int qurt_futex_wait64(void *lock, long long val);
int qurt_futex_wake(void *lock, int n_to_wake);
typedef union qurt_mutex_aligned8{
    struct {
        unsigned int holder;
        unsigned int count;
        unsigned int queue;
        unsigned int wait_count;
    };
    unsigned long long int raw;
} qurt_mutex_t;
void qurt_mutex_init(qurt_mutex_t *lock);
void qurt_mutex_destroy(qurt_mutex_t *lock);
void qurt_mutex_lock(qurt_mutex_t *lock);
void qurt_mutex_unlock(qurt_mutex_t *lock);
int qurt_mutex_try_lock(qurt_mutex_t *lock);
typedef union {
 unsigned int raw[2] __attribute__((aligned(8)));
 struct {
  unsigned short val;
  unsigned short n_waiting;
        unsigned int reserved1;
        unsigned int queue;
        unsigned int reserved2;
 }X;
} qurt_sem_t;
int qurt_sem_add(qurt_sem_t *sem, unsigned int amt);
static inline int qurt_sem_up(qurt_sem_t *sem) { return qurt_sem_add(sem,1); };
int qurt_sem_down(qurt_sem_t *sem);
int qurt_sem_try_down(qurt_sem_t *sem);
void qurt_sem_init(qurt_sem_t *sem);
void qurt_sem_destroy(qurt_sem_t *sem);
void qurt_sem_init_val(qurt_sem_t *sem, unsigned short val);
static inline unsigned short qurt_sem_get_val(qurt_sem_t *sem ){return sem->X.val;}
int qurt_sem_down_cancellable(qurt_sem_t *sem);
typedef unsigned long long int qurt_pipe_data_t;
typedef struct {
    qurt_mutex_t pipe_lock;
    qurt_sem_t senders;
    qurt_sem_t receiver;
    unsigned int size;
    unsigned int sendidx;
    unsigned int recvidx;
    void (*lock_func)(qurt_mutex_t *);
    void (*unlock_func)(qurt_mutex_t *);
    int (*try_lock_func)(qurt_mutex_t *);
    void (*destroy_lock_func)(qurt_mutex_t *);
    unsigned int magic;
    qurt_pipe_data_t *data;
} qurt_pipe_t;
typedef struct {
  qurt_pipe_data_t *buffer;
  unsigned int elements;
  unsigned char mem_partition;
} qurt_pipe_attr_t;
static inline void qurt_pipe_attr_init(qurt_pipe_attr_t *attr)
{
  attr->buffer = 0;
  attr->elements = 0;
  attr->mem_partition = 0;
}
static inline void qurt_pipe_attr_set_buffer(qurt_pipe_attr_t *attr, qurt_pipe_data_t *buffer)
{
  attr->buffer = buffer;
}
static inline void qurt_pipe_attr_set_elements(qurt_pipe_attr_t *attr, unsigned int elements)
{
  attr->elements = elements;
}
static inline void qurt_pipe_attr_set_buffer_partition(qurt_pipe_attr_t *attr, unsigned char mem_partition)
{
  attr->mem_partition = mem_partition;
}
int qurt_pipe_create(qurt_pipe_t **pipe, qurt_pipe_attr_t *attr);
int qurt_pipe_init(qurt_pipe_t *pipe, qurt_pipe_attr_t *attr);
void qurt_pipe_destroy(qurt_pipe_t *pipe);
void qurt_pipe_delete(qurt_pipe_t *pipe);
void qurt_pipe_send(qurt_pipe_t *pipe, qurt_pipe_data_t data);
qurt_pipe_data_t qurt_pipe_receive(qurt_pipe_t *pipe);
int qurt_pipe_try_send(qurt_pipe_t *pipe, qurt_pipe_data_t data);
qurt_pipe_data_t qurt_pipe_try_receive(qurt_pipe_t *pipe, int *success);
int qurt_pipe_receive_cancellable(qurt_pipe_t *pipe, qurt_pipe_data_t *result);
int qurt_pipe_send_cancellable(qurt_pipe_t *pipe, qurt_pipe_data_t data);
int qurt_pipe_is_empty(qurt_pipe_t *pipe);
int qurt_printf(const char* format, ...);
void qurt_assert_error(const char *filename, int lineno) __attribute__((noreturn));
unsigned int qurt_trace_get_marker(void);
int qurt_trace_changed(unsigned int prev_trace_marker, unsigned int trace_mask);
unsigned int qurt_etm_set_config(unsigned int type, unsigned int route, unsigned int filter);
unsigned int qurt_etm_enable(unsigned int enable_flag);
unsigned int qurt_etm_testbus_set_config(unsigned int cfg_data);
unsigned int qurt_etm_set_breakpoint(unsigned int type, unsigned int address, unsigned int data, unsigned int mask);
unsigned int qurt_etm_set_breakarea(unsigned int type, unsigned int start_address, unsigned int end_address, unsigned int count);
void qurt_profile_reset_idle_pcycles (void);
unsigned long long int qurt_profile_get_thread_pcycles(void);
unsigned long long int qurt_profile_get_thread_tcycles(void);
unsigned long long int qurt_get_core_pcycles(void);
void qurt_profile_get_idle_pcycles (unsigned long long *pcycles);
void qurt_profile_get_threadid_pcycles (int thread_id, unsigned long long *pcycles);
void qurt_profile_reset_threadid_pcycles (int thread_id);
void qurt_profile_enable (int enable);
typedef struct {
   unsigned int holder __attribute__((aligned(8)));
   unsigned short waiters;
   unsigned short refs;
   unsigned int queue;
   unsigned int excess_locks;
} qurt_rmutex2_t;
void qurt_rmutex2_init(qurt_rmutex2_t *lock);
void qurt_rmutex2_destroy(qurt_rmutex2_t *lock);
void qurt_rmutex2_lock(qurt_rmutex2_t *lock);
void qurt_rmutex2_unlock(qurt_rmutex2_t *lock);
int qurt_rmutex2_try_lock(qurt_rmutex2_t *lock);
typedef union {
    unsigned long long raw;
    struct {
        unsigned int count;
        unsigned int n_waiting;
        unsigned int queue;
        unsigned int reserved;
    }X;
} qurt_cond_t;
void qurt_cond_init(qurt_cond_t *cond);
void qurt_cond_destroy(qurt_cond_t *cond);
void qurt_cond_signal(qurt_cond_t *cond);
void qurt_cond_broadcast(qurt_cond_t *cond);
void qurt_cond_wait(qurt_cond_t *cond, qurt_mutex_t *mutex);
void qurt_cond_wait2(qurt_cond_t *cond, qurt_rmutex2_t *mutex);
typedef union {
 struct {
        unsigned short threads_left;
  unsigned short count;
  unsigned int threads_total;
        unsigned int queue;
        unsigned int reserved;
 };
 unsigned long long int raw;
} qurt_barrier_t;
int qurt_barrier_init(qurt_barrier_t *barrier, unsigned int threads_total);
int qurt_barrier_destroy(qurt_barrier_t *barrier);
int qurt_barrier_wait(qurt_barrier_t *barrier);
unsigned int qurt_fastint_register(int intno, void (*fn)(int));
unsigned int qurt_fastint_deregister(int intno);
unsigned int qurt_isr_register(int intno, void (*fn)(int));
unsigned int qurt_isr_deregister(int intno);
typedef union {
 unsigned long long int raw;
 struct {
  unsigned int waiting;
  unsigned int signals_in;
  unsigned int queue;
  unsigned int reserved;
 }X;
} qurt_allsignal_t;
void qurt_allsignal_init(qurt_allsignal_t *signal);
void qurt_allsignal_destroy(qurt_allsignal_t *signal);
static inline unsigned int qurt_allsignal_get(qurt_allsignal_t *signal)
{ return signal->X.signals_in; };
void qurt_allsignal_wait(qurt_allsignal_t *signal, unsigned int mask);
void qurt_allsignal_set(qurt_allsignal_t *signal, unsigned int mask);
void qurt_rmutex_init(qurt_mutex_t *lock);
void qurt_rmutex_destroy(qurt_mutex_t *lock);
void qurt_rmutex_lock(qurt_mutex_t *lock);
void qurt_rmutex_unlock(qurt_mutex_t *lock);
int qurt_rmutex_try_lock(qurt_mutex_t *lock);
int qurt_rmutex_try_lock_block_once(qurt_mutex_t *lock);
void qurt_pimutex_init(qurt_mutex_t *lock);
void qurt_pimutex_destroy(qurt_mutex_t *lock);
void qurt_pimutex_lock(qurt_mutex_t *lock);
void qurt_pimutex_unlock(qurt_mutex_t *lock);
int qurt_pimutex_try_lock(qurt_mutex_t *lock);
typedef struct {
   unsigned int cur_mask __attribute__((aligned(8)));
   unsigned int sig_state;
   unsigned int queue;
   unsigned int wait_mask;
} qurt_signal2_t;
void qurt_signal2_init(qurt_signal2_t *signal);
void qurt_signal2_destroy(qurt_signal2_t *signal);
unsigned int qurt_signal2_wait(qurt_signal2_t *signal, unsigned int mask,
                unsigned int attribute);
static inline unsigned int qurt_signal2_wait_any(qurt_signal2_t *signal, unsigned int mask)
{
  return qurt_signal2_wait(signal, mask, 0x00000000);
}
static inline unsigned int qurt_signal2_wait_all(qurt_signal2_t *signal, unsigned int mask)
{
  return qurt_signal2_wait(signal, mask, 0x00000001);
}
void qurt_signal2_set(qurt_signal2_t *signal, unsigned int mask);
unsigned int qurt_signal2_get(qurt_signal2_t *signal);
void qurt_signal2_clear(qurt_signal2_t *signal, unsigned int mask);
int qurt_signal2_wait_cancellable(qurt_signal2_t *signal,
                                  unsigned int mask,
                                  unsigned int attribute,
                                  unsigned int *p_returnmask);
void qurt_pimutex2_init(qurt_rmutex2_t *lock);
void qurt_pimutex2_destroy(qurt_rmutex2_t *lock);
void qurt_pimutex2_lock(qurt_rmutex2_t *lock);
void qurt_pimutex2_unlock(qurt_rmutex2_t *lock);
int qurt_rmutex2_try_lock(qurt_rmutex2_t *lock);
 unsigned int qurt_interrupt_register(int int_num, qurt_anysignal_t *int_signal, int signal_mask);
int qurt_interrupt_acknowledge(int int_num);
unsigned int qurt_interrupt_deregister(int int_num);
 unsigned int qurt_interrupt_enable(int int_num);
 unsigned int qurt_interrupt_disable(int int_num);
unsigned int qurt_interrupt_status(int int_num, int *status);
unsigned int qurt_interrupt_clear(int int_num);
unsigned int qurt_interrupt_get_registered(void);
unsigned int qurt_interrupt_get_config(unsigned int int_num, unsigned int *int_type, unsigned int *int_polarity);
unsigned int qurt_interrupt_set_config(unsigned int int_num, unsigned int int_type, unsigned int int_polarity);
int qurt_interrupt_raise(unsigned int interrupt_num);
void qurt_interrupt_disable_all(void);
int qurt_isr_subcall(void);
void * qurt_lifo_pop(void *freelist);
void qurt_lifo_push(void *freelist, void *buf);
void qurt_lifo_remove(void *freelist, void *buf);
static inline int qurt_power_shutdown_prepare(void){ return 0;}
int qurt_power_shutdown_enter (int type);
int qurt_power_exit(void);
int qurt_power_apcr_enter (void);
int qurt_power_tcxo_prepare (void);
int qurt_power_tcxo_fail_exit (void);
int qurt_power_tcxo_enter (void);
int qurt_power_tcxo_exit (void);
void qurt_power_override_wait_for_idle(int enable);
void qurt_power_wait_for_idle (void);
void qurt_power_wait_for_active (void);
unsigned int qurt_system_ipend_get (void);
void qurt_system_avscfg_set(unsigned int avscfg_value);
unsigned int qurt_system_avscfg_get(void);
unsigned int qurt_system_vid_get(void);
int qurt_power_shutdown_get_pcycles( unsigned long long *enter_pcycles, unsigned long long *exit_pcycles );
int qurt_system_tcm_set_size(unsigned int new_size);
int qurt_power_shutdown_get_hw_ticks( unsigned long long *before_pc_ticks, unsigned long long *after_wb_ticks );
typedef struct qurt_sysenv_swap_pools {
   unsigned int spoolsize;
   unsigned int spooladdr;
}qurt_sysenv_swap_pools_t;
typedef struct qurt_sysenv_app_heap {
   unsigned int heap_base;
   unsigned int heap_limit;
} qurt_sysenv_app_heap_t ;
typedef struct qurt_sysenv_arch_version {
    unsigned int arch_version;
}qurt_arch_version_t;
typedef struct qurt_sysenv_max_hthreads {
   unsigned int max_hthreads;
}qurt_sysenv_max_hthreads_t;
typedef struct qurt_sysenv_max_pi_prio {
    unsigned int max_pi_prio;
}qurt_sysenv_max_pi_prio_t;
typedef struct qurt_sysenv_timer_hw {
   unsigned int base;
   unsigned int int_num;
}qurt_sysenv_hw_timer_t;
typedef struct qurt_sysenv_procname {
   unsigned int asid;
   char name[64];
}qurt_sysenv_procname_t;
typedef struct qurt_sysenv_stack_profile_count {
   unsigned int count;
}qurt_sysenv_stack_profile_count_t;
typedef struct _qurt_sysevent_error_t
{
    unsigned int thread_id;
    unsigned int fault_pc;
    unsigned int sp;
    unsigned int badva;
    unsigned int cause;
    unsigned int ssr;
    unsigned int fp;
    unsigned int lr;
} qurt_sysevent_error_t ;
typedef struct qurt_sysevent_pagefault {
    qurt_thread_t thread_id;
    unsigned int fault_addr;
    unsigned int ssr_cause;
} qurt_sysevent_pagefault_t ;
int qurt_sysenv_get_swap_spool0 (qurt_sysenv_swap_pools_t *pools );
int qurt_sysenv_get_swap_spool1(qurt_sysenv_swap_pools_t *pools );
int qurt_sysenv_get_app_heap(qurt_sysenv_app_heap_t *aheap );
int qurt_sysenv_get_hw_timer(qurt_sysenv_hw_timer_t *timer );
int qurt_sysenv_get_arch_version(qurt_arch_version_t *vers);
int qurt_sysenv_get_max_hw_threads(qurt_sysenv_max_hthreads_t *mhwt );
int qurt_sysenv_get_max_pi_prio(qurt_sysenv_max_pi_prio_t *mpip );
int qurt_sysenv_get_process_name(qurt_sysenv_procname_t *pname );
int qurt_sysenv_get_stack_profile_count(qurt_sysenv_stack_profile_count_t *count );
unsigned int qurt_exception_wait (unsigned int *ip, unsigned int *sp,
                                  unsigned int *badva, unsigned int *cause);
unsigned int qurt_exception_wait_ext (qurt_sysevent_error_t * sys_err);
static inline unsigned int qurt_exception_wait2(qurt_sysevent_error_t * sys_err)
{
   return qurt_exception_wait_ext(sys_err);
}
int qurt_exception_raise_nonfatal (int error) __attribute__((noreturn));
void qurt_exception_raise_fatal (void);
void qurt_exception_shutdown_fatal(void) __attribute__((noreturn));
void qurt_exception_shutdown_fatal2(void);
unsigned int qurt_exception_register_fatal_notification ( void(*entryfuncpoint)(void *), void *argp);
unsigned int qurt_enable_floating_point_exception(unsigned int mask);
static inline unsigned int qurt_exception_enable_fp_exceptions(unsigned int mask)
{
   return qurt_enable_floating_point_exception(mask);
}
unsigned int qurt_exception_wait_pagefault (qurt_sysevent_pagefault_t *sys_pagefault);
void qurt_pmu_set (int reg_id, unsigned int reg_value);
unsigned int qurt_pmu_get (int red_id);
void qurt_pmu_enable (int enable);
typedef unsigned int qurt_addr_t;
typedef unsigned int qurt_paddr_t;
typedef unsigned long long qurt_paddr_64_t;
typedef unsigned int qurt_mem_region_t;
typedef unsigned int qurt_mem_fs_region_t;
typedef unsigned int qurt_mem_pool_t;
typedef unsigned int qurt_size_t;
typedef enum {
        QURT_MEM_MAPPING_VIRTUAL=0,
        QURT_MEM_MAPPING_PHYS_CONTIGUOUS = 1,
        QURT_MEM_MAPPING_IDEMPOTENT=2,
        QURT_MEM_MAPPING_VIRTUAL_FIXED=3,
        QURT_MEM_MAPPING_NONE=4,
        QURT_MEM_MAPPING_VIRTUAL_RANDOM=7,
        QURT_MEM_MAPPING_INVALID=10,
} qurt_mem_mapping_t;
typedef enum {
        QURT_MEM_CACHE_WRITEBACK=7,
        QURT_MEM_CACHE_NONE_SHARED=6,
        QURT_MEM_CACHE_WRITETHROUGH=5,
        QURT_MEM_CACHE_WRITEBACK_NONL2CACHEABLE=0,
        QURT_MEM_CACHE_WRITETHROUGH_NONL2CACHEABLE=1,
        QURT_MEM_CACHE_WRITEBACK_L2CACHEABLE=QURT_MEM_CACHE_WRITEBACK,
        QURT_MEM_CACHE_WRITETHROUGH_L2CACHEABLE=QURT_MEM_CACHE_WRITETHROUGH,
        QURT_MEM_CACHE_DEVICE = 4,
        QURT_MEM_CACHE_NONE = 4,
        QURT_MEM_CACHE_INVALID=10,
} qurt_mem_cache_mode_t;
typedef enum {
        QURT_PERM_READ=0x1,
        QURT_PERM_WRITE=0x2,
        QURT_PERM_EXECUTE=0x4,
        QURT_PERM_FULL=QURT_PERM_READ|QURT_PERM_WRITE|QURT_PERM_EXECUTE,
} qurt_perm_t;
typedef enum {
        QURT_MEM_ICACHE,
        QURT_MEM_DCACHE
} qurt_mem_cache_type_t;
typedef enum {
    QURT_MEM_CACHE_FLUSH,
    QURT_MEM_CACHE_INVALIDATE,
    QURT_MEM_CACHE_FLUSH_INVALIDATE,
    QURT_MEM_CACHE_FLUSH_ALL,
    QURT_MEM_CACHE_FLUSH_INVALIDATE_ALL,
    QURT_MEM_CACHE_TABLE_FLUSH_INVALIDATE,
} qurt_mem_cache_op_t;
typedef enum {
        QURT_MEM_REGION_LOCAL=0,
        QURT_MEM_REGION_SHARED=1,
        QURT_MEM_REGION_USER_ACCESS=2,
        QURT_MEM_REGION_FS=4,
        QURT_MEM_REGION_INVALID=10,
} qurt_mem_region_type_t;
struct qurt_pgattr {
   unsigned pga_value;
};
typedef struct qurt_pgattr qurt_pgattr_t;
typedef struct {
    qurt_mem_mapping_t mapping_type;
    unsigned char perms;
    unsigned short owner;
    qurt_pgattr_t pga;
    unsigned ppn;
    qurt_addr_t virtaddr;
    qurt_mem_region_type_t type;
    qurt_size_t size;
} qurt_mem_region_attr_t;
typedef struct {
    char name[32];
    struct ranges{
        unsigned int start;
        unsigned int size;
    } ranges[16];
} qurt_mem_pool_attr_t;
typedef enum {
    HEXAGON_L1_I_CACHE = 0,
    HEXAGON_L1_D_CACHE = 1,
    HEXAGON_L2_CACHE = 2
} qurt_cache_type_t;
typedef enum {
    FULL_SIZE = 0,
    HALF_SIZE = 1,
    THREE_QUARTER_SIZE = 2,
    SEVEN_EIGHTHS_SIZE = 3
} qurt_cache_partition_size_t;
int qurt_tlb_entry_create (unsigned int *entry_id, qurt_addr_t vaddr, qurt_paddr_t paddr, qurt_size_t size, qurt_mem_cache_mode_t cache_attribs, qurt_perm_t perms, int asid);
int qurt_tlb_entry_create_64 (unsigned int *entry_id, qurt_addr_t vaddr, qurt_paddr_64_t paddr_64, qurt_size_t size, qurt_mem_cache_mode_t cache_attribs, qurt_perm_t perms, int asid);
int qurt_tlb_entry_delete (unsigned int entry_id);
int qurt_tlb_entry_query (unsigned int *entry_id, qurt_addr_t vaddr, int asid);
int qurt_tlb_entry_set (unsigned int entry_id, unsigned long long int entry);
int qurt_tlb_entry_get (unsigned int entry_id, unsigned long long int *entry);
unsigned short qurt_tlb_entry_get_available(void);
unsigned int qurt_tlb_get_pager_physaddr(unsigned int** pager_phys_addrs);
extern qurt_mem_pool_t qurt_mem_default_pool;
int qurt_mem_cache_clean(qurt_addr_t addr, qurt_size_t size, qurt_mem_cache_op_t opcode, qurt_mem_cache_type_t type);
int qurt_mem_l2cache_line_lock (qurt_addr_t addr, qurt_size_t size);
int qurt_mem_l2cache_line_unlock(qurt_addr_t addr, qurt_size_t size);
void qurt_mem_region_attr_init(qurt_mem_region_attr_t *attr);
int qurt_mem_pool_attach(char *name, qurt_mem_pool_t *pool);
int qurt_mem_pool_create(char *name, unsigned base, unsigned size, qurt_mem_pool_t *pool);
int qurt_mem_pool_add_pages(qurt_mem_pool_t pool,
                            unsigned first_pageno,
                            unsigned size_in_pages);
int qurt_mem_pool_remove_pages(qurt_mem_pool_t pool,
                               unsigned first_pageno,
                               unsigned size_in_pages,
                               unsigned flags,
                               void (*callback)(void *),
                               void *arg);
int qurt_mem_pool_attr_get (qurt_mem_pool_t pool, qurt_mem_pool_attr_t *attr);
static inline int qurt_mem_pool_attr_get_size (qurt_mem_pool_attr_t *attr, int range_id, qurt_size_t *size){
    if ((range_id >= 16) || (range_id < 0)){
        (*size) = 0;
        return 4;
    }
    else {
        (*size) = attr->ranges[range_id].size;
    }
    return 0;
}
static inline int qurt_mem_pool_attr_get_addr (qurt_mem_pool_attr_t *attr, int range_id, qurt_addr_t *addr){
    if ((range_id >= 16) || (range_id < 0)){
        (*addr) = 0;
        return 4;
    }
    else {
        (*addr) = (attr->ranges[range_id].start)<<12;
   }
   return 0;
}
int qurt_mem_region_create(qurt_mem_region_t *region, qurt_size_t size, qurt_mem_pool_t pool, qurt_mem_region_attr_t *attr);
int qurt_mem_region_delete(qurt_mem_region_t region);
int qurt_mem_region_attr_get(qurt_mem_region_t region, qurt_mem_region_attr_t *attr);
static inline void qurt_mem_region_attr_set_type(qurt_mem_region_attr_t *attr, qurt_mem_region_type_t type){
    attr->type = type;
}
static inline void qurt_mem_region_attr_get_size(qurt_mem_region_attr_t *attr, qurt_size_t *size){
    (*size) = attr->size;
}
static inline void qurt_mem_region_attr_get_type(qurt_mem_region_attr_t *attr, qurt_mem_region_type_t *type){
    (*type) = attr->type;
}
static inline void qurt_mem_region_attr_set_physaddr(qurt_mem_region_attr_t *attr, qurt_paddr_t addr){
    attr->ppn = (unsigned)(((unsigned)(addr))>>12);
}
static inline void qurt_mem_region_attr_get_physaddr(qurt_mem_region_attr_t *attr, unsigned int *addr){
    (*addr) = (unsigned)(((unsigned) (attr->ppn))<<12);
}
static inline void qurt_mem_region_attr_set_virtaddr(qurt_mem_region_attr_t *attr, qurt_addr_t addr){
    attr->virtaddr = addr;
}
static inline void qurt_mem_region_attr_get_virtaddr(qurt_mem_region_attr_t *attr, unsigned int *addr){
    (*addr) = (unsigned int)(attr->virtaddr);
}
static inline void qurt_mem_region_attr_set_mapping(qurt_mem_region_attr_t *attr, qurt_mem_mapping_t mapping){
    attr->mapping_type = mapping;
}
static inline void qurt_mem_region_attr_get_mapping(qurt_mem_region_attr_t *attr, qurt_mem_mapping_t *mapping){
    (*mapping) = attr->mapping_type;
}
static inline void qurt_mem_region_attr_set_cache_mode(qurt_mem_region_attr_t *attr, qurt_mem_cache_mode_t mode){
    (((attr->pga).pga_value)=(((attr->pga).pga_value)&~(((~0u)>>(31-((3)-(0))))<<(0)))|((((unsigned)mode)<<(0))&(((~0u)>>(31-((3)-(0))))<<(0))));
}
static inline void qurt_mem_region_attr_get_cache_mode(qurt_mem_region_attr_t *attr, qurt_mem_cache_mode_t *mode){
    (*mode) = (qurt_mem_cache_mode_t)((((attr->pga).pga_value)&(((~0u)>>(31-((3)-(0))))<<(0)))>>(0));
}
static inline void qurt_mem_region_attr_set_bus_attr(qurt_mem_region_attr_t *attr, unsigned abits){
    (((attr->pga).pga_value)=(((attr->pga).pga_value)&~(((~0u)>>(31-((5)-(4))))<<(4)))|(((abits)<<(4))&(((~0u)>>(31-((5)-(4))))<<(4))));
}
static inline void qurt_mem_region_attr_get_bus_attr(qurt_mem_region_attr_t *attr, unsigned *pbits){
    (*pbits) = ((((attr->pga).pga_value)&(((~0u)>>(31-((5)-(4))))<<(4)))>>(4));
}
void qurt_mem_region_attr_set_owner(qurt_mem_region_attr_t *attr, int handle);
void qurt_mem_region_attr_get_owner(qurt_mem_region_attr_t *attr, int *p_handle);
void qurt_mem_region_attr_set_perms(qurt_mem_region_attr_t *attr, unsigned perms);
void qurt_mem_region_attr_get_perms(qurt_mem_region_attr_t *attr, unsigned *p_perms);
int qurt_mem_map_static_query(qurt_addr_t *vaddr, qurt_addr_t paddr, unsigned int page_size, qurt_mem_cache_mode_t cache_attribs, qurt_perm_t perm);
int qurt_mem_region_query(qurt_mem_region_t *region_handle, qurt_addr_t vaddr, qurt_paddr_t paddr);
int qurt_mapping_create(qurt_addr_t vaddr, qurt_addr_t paddr, qurt_size_t size,
                         qurt_mem_cache_mode_t cache_attribs, qurt_perm_t perm);
int qurt_mapping_remove(qurt_addr_t vaddr, qurt_addr_t paddr, qurt_size_t size);
qurt_paddr_t qurt_lookup_physaddr (qurt_addr_t vaddr);
static inline void qurt_mem_region_attr_set_physaddr_64(qurt_mem_region_attr_t *attr, qurt_paddr_64_t addr_64){
    attr->ppn = (unsigned)(((unsigned long long)(addr_64))>>12);
}
static inline void qurt_mem_region_attr_get_physaddr_64(qurt_mem_region_attr_t *attr, qurt_paddr_64_t *addr_64){
    (*addr_64) = (unsigned long long)(((unsigned long long)(attr->ppn))<<12);
}
int qurt_mem_map_static_query_64(qurt_addr_t *vaddr, qurt_paddr_64_t paddr_64, unsigned int page_size, qurt_mem_cache_mode_t cache_attribs, qurt_perm_t perm);
int qurt_mem_region_query_64(qurt_mem_region_t *region_handle, qurt_addr_t vaddr, qurt_paddr_64_t paddr_64);
int qurt_mapping_create_64(qurt_addr_t vaddr, qurt_paddr_64_t paddr_64, qurt_size_t size,
                         qurt_mem_cache_mode_t cache_attribs, qurt_perm_t perm);
int qurt_mapping_remove_64(qurt_addr_t vaddr, qurt_paddr_64_t paddr_64, qurt_size_t size);
qurt_paddr_64_t qurt_lookup_physaddr_64 (qurt_addr_t vaddr);
int qurt_mapping_reclaim(qurt_addr_t vaddr, qurt_size_t vsize, qurt_mem_pool_t pool);
int qurt_mem_configure_cache_partition(qurt_cache_type_t cache_type, qurt_cache_partition_size_t partition_size);
void qurt_l2fetch_disable(void);
static inline void qurt_mem_syncht(void){
    __asm__ __volatile__ (" SYNCHT \n");
}
static inline void qurt_mem_barrier(void){
    __asm__ __volatile__ (" BARRIER \n");
}
int qurt_qdi_qhi3(int,int,int);
int qurt_qdi_qhi4(int,int,int,int);
int qurt_qdi_qhi5(int,int,int,int,int);
int qurt_qdi_qhi6(int,int,int,int,int,int);
int qurt_qdi_qhi7(int,int,int,int,int,int,int);
int qurt_qdi_qhi8(int,int,int,int,int,int,int,int);
int qurt_qdi_qhi9(int,int,int,int,int,int,int,int,int);
int qurt_qdi_qhi10(int,int,int,int,int,int,int,int,int,int);
int qurt_qdi_qhi11(int,int,int,int,int,int,int,int,int,int,int);
int qurt_qdi_qhi12(int,int,int,int,int,int,int,int,int,int,int,int);
int qurt_qdi_write(int handle, const void *buf, unsigned len);
int qurt_qdi_read(int handle, void *buf, unsigned len);
int qurt_qdi_close(int handle);
extern int qurt_sysclock_register (qurt_anysignal_t *signal, unsigned int signal_mask);
extern unsigned long long qurt_sysclock_alarm_create (int id, unsigned long long ref_count, unsigned long long match_value);
extern int qurt_sysclock_timer_create (int id, unsigned long long duration);
extern unsigned long long qurt_sysclock_get_expiry (void);
unsigned long long qurt_sysclock_get_hw_ticks (void);
extern int qurt_timer_base __attribute__((section(".data.qurt_timer_base")));
static inline unsigned long qurt_sysclock_get_hw_ticks_32 (void)
{
    return (volatile unsigned long)(*((unsigned long *)((unsigned int)qurt_timer_base+0x1000)));
}
static inline unsigned short qurt_sysclock_get_hw_ticks_16 (void)
{
    unsigned long ticks;
    ticks = (volatile unsigned long)(*((unsigned long *)((unsigned int)qurt_timer_base+0x1000)));
    __asm__ __volatile__ ( "%0 = lsr(%0, #16) \n" :"+r"(ticks));
    return (unsigned short)ticks;
}
unsigned long long qurt_timer_timetick_to_us(unsigned long long ticks);
int qurt_spawn_flags(const char * name, int flags);
int qurt_space_switch(int asid);
int qurt_wait(int *status);
int qurt_event_register(int type, int value, qurt_signal_t *signal, unsigned int mask, void *data, unsigned int data_size);
typedef struct _qurt_process_attr {
    char name[64];
    int flags;
} qurt_process_attr_t;
int qurt_process_create (qurt_process_attr_t *attr);
int qurt_process_get_id (void);
static inline void qurt_process_attr_init (qurt_process_attr_t *attr)
{
    attr->name[0] = 0;
    attr->flags = 0;
}
static inline void qurt_process_attr_set_executable (qurt_process_attr_t *attr, char *name)
{
    strlcpy (attr->name, name, 64);
}
static inline void qurt_process_attr_set_flags (qurt_process_attr_t *attr, int flags)
{
    attr->flags = flags;
}
void qurt_process_cmdline_get(char *buf, unsigned buf_siz);
typedef unsigned int mode_t;
int shm_open(const char * name, int oflag, mode_t mode);
void *shm_mmap(void *addr, unsigned int len, int prot, int flags, int fd, unsigned int offset);
int shm_close(int fd);
typedef enum
{
  QURT_TIMER_ONESHOT = 0,
  QURT_TIMER_PERIODIC
} qurt_timer_type_t;
typedef unsigned int qurt_timer_t;
typedef unsigned long long qurt_timer_duration_t;
typedef unsigned long long qurt_timer_time_t;
typedef struct
{
    unsigned int magic;
    qurt_timer_duration_t duration;
    qurt_timer_time_t expiry;
    qurt_timer_duration_t remaining;
    qurt_timer_type_t type;
    unsigned int group;
}
qurt_timer_attr_t;
int qurt_timer_stop (qurt_timer_t timer);
int qurt_timer_restart (qurt_timer_t timer, qurt_timer_duration_t duration);
int qurt_timer_create (qurt_timer_t *timer, const qurt_timer_attr_t *attr,
                  const qurt_anysignal_t *signal, unsigned int mask);
int qurt_timer_create_sig2 (qurt_timer_t *timer, const qurt_timer_attr_t *attr,
                  const qurt_signal2_t *signal, unsigned int mask);
void qurt_timer_attr_init(qurt_timer_attr_t *attr);
void qurt_timer_attr_set_duration(qurt_timer_attr_t *attr, qurt_timer_duration_t duration);
void qurt_timer_attr_set_expiry(qurt_timer_attr_t *attr, qurt_timer_time_t time);
void qurt_timer_attr_get_duration(qurt_timer_attr_t *attr, qurt_timer_duration_t *duration);
void qurt_timer_attr_get_remaining(qurt_timer_attr_t *attr, qurt_timer_duration_t *remaining);
void qurt_timer_attr_set_type(qurt_timer_attr_t *attr, qurt_timer_type_t type);
void qurt_timer_attr_get_type(qurt_timer_attr_t *attr, qurt_timer_type_t *type);
void qurt_timer_attr_set_group(qurt_timer_attr_t *attr, unsigned int group);
void qurt_timer_attr_get_group(qurt_timer_attr_t *attr, unsigned int *group);
int qurt_timer_get_attr(qurt_timer_t timer, qurt_timer_attr_t *attr);
int qurt_timer_delete(qurt_timer_t timer);
int qurt_timer_sleep(qurt_timer_duration_t duration);
int qurt_timer_group_disable (unsigned int group);
int qurt_timer_group_enable (unsigned int group);
void qurt_timer_recover_pc (void);
static inline int qurt_timer_is_init (void) {return 1;};
unsigned long long qurt_timer_get_ticks (void);
int qurt_tls_create_key (int *key, void (*destructor)(void *));
int qurt_tls_set_specific (int key, const void *value);
void *qurt_tls_get_specific (int key);
int qurt_tls_delete_key (int key);
static inline int qurt_thread_iterator_create(void)
{
   return qurt_qdi_qhi3(0,4,68);
}
static inline qurt_thread_t qurt_thread_iterator_next(int iter)
{
   return qurt_qdi_qhi3(0,iter,69);
}
static inline int qurt_thread_iterator_destroy(int iter)
{
   return qurt_qdi_close(iter);
}
int qurt_thread_context_get_tname(unsigned int thread_id, char *name, unsigned char max_len);
int qurt_thread_context_get_prio(unsigned int thread_id, unsigned char *prio);
int qurt_thread_context_get_pcycles(unsigned int thread_id, unsigned long long int *pcycles);
int qurt_thread_context_get_stack_base(unsigned int thread_id, unsigned int *sbase);
int qurt_thread_context_get_stack_size(unsigned int thread_id, unsigned int *ssize);
int qurt_thread_context_get_pid(unsigned int thread_id, unsigned int *pid);
int qurt_thread_context_get_pname(unsigned int thread_id, char *name, unsigned int len);
typedef enum {
    QURT_HVX_MODE_64B = 0,
    QURT_HVX_MODE_128B = 1
} qurt_hvx_mode_t;
int qurt_hvx_lock(qurt_hvx_mode_t lock_mode);
int qurt_hvx_unlock(void);
int qurt_hvx_try_lock(qurt_hvx_mode_t lock_mode);
int qurt_hvx_get_mode(void);
int qurt_hvx_get_units(void);
int qurt_hvx_reserve(int num_units);
int qurt_hvx_cancel_reserve(void);
int qurt_hvx_get_lock_val(void);
typedef enum {
        QURT_MAILBOX_AT_QURTOS=0,
        QURT_MAILBOX_AT_ROOTPD=1,
        QURT_MAILBOX_AT_USERPD=2,
        QURT_MAILBOX_AT_SECUREPD=3,
} qurt_mailbox_receiver_cfg_t;
typedef enum {
        QURT_MAILBOX_SEND_OVERWRITE=0,
        QURT_MAILBOX_SEND_NON_OVERWRITE=1,
} qurt_mailbox_send_option_t;
typedef enum {
        QURT_MAILBOX_RECV_WAITING=0,
        QURT_MAILBOX_RECV_NON_WAITING=1,
        QURT_MAILBOX_RECV_PEEK_NON_WAITING=2,
} qurt_mailbox_recv_option_t;
unsigned long long qurt_mailbox_create(char *name, qurt_mailbox_receiver_cfg_t recv_opt);
unsigned long long qurt_mailbox_get_id(char *name);
int qurt_mailbox_send(unsigned long long mailbox_id, qurt_mailbox_send_option_t send_opt, unsigned long long data);
int qurt_mailbox_receive(unsigned long long mailbox_id, qurt_mailbox_recv_option_t recv_opt, unsigned long long *data);
int qurt_mailbox_delete(unsigned long long mailbox_id);
int qurt_mailbox_receive_halt(unsigned long long mailbox_id);
enum qurt_island_attr_resource_type {
    QURT_ISLAND_ATTR_INVALID,
    QURT_ISLAND_ATTR_END_OF_LIST = QURT_ISLAND_ATTR_INVALID,
    QURT_ISLAND_ATTR_INT,
    QURT_ISLAND_ATTR_THREAD,
    QURT_ISLAND_ATTR_MEMORY
};
typedef struct qurt_island_attr_resource {
    enum qurt_island_attr_resource_type type;
    union {
        struct {
            qurt_addr_t base_addr;
            qurt_size_t size;
        } memory;
        unsigned int interrupt;
        qurt_thread_t thread_id;
    };
} qurt_island_attr_resource_t;
typedef struct qurt_island_attr {
    int max_attrs;
    struct qurt_island_attr_resource attrs[1];
} qurt_island_attr_t;
typedef struct {
   int qdi_handle;
} qurt_island_t;
int qurt_island_attr_create (qurt_island_attr_t **attr, int max_attrs);
void qurt_island_attr_delete (qurt_island_attr_t *attr);
int qurt_island_attr_add (qurt_island_attr_t *attr, qurt_island_attr_resource_t *resources);
int qurt_island_attr_add_interrupt (qurt_island_attr_t *attr, unsigned int interrupt);
int qurt_island_attr_add_mem (qurt_island_attr_t *attr, qurt_addr_t base_addr, qurt_size_t size);
int qurt_island_attr_add_thread (qurt_island_attr_t *attr, qurt_thread_t thread_id);
int qurt_island_spec_create (qurt_island_t *spec_id, qurt_island_attr_t *attr);
int qurt_island_spec_delete (qurt_island_t spec_id);
int qurt_island_enter (qurt_island_t spec_id);
int qurt_island_exit (void);
unsigned int qurt_island_exception_wait (unsigned int *ip, unsigned int *sp,
                                         unsigned int *badva, unsigned int *cause);
unsigned int qurt_island_get_status (void);
typedef struct InterruptArgsStruct
{
  uint32 nInterruptVector;
  qurt_anysignal_t pRetSig;
  struct InterruptArgsStruct *pNext;
} InterruptArgsType;
void InterruptControllerPushArg( InterruptArgsType* pArgs, InterruptArgsType** ppList );
InterruptArgsType* InterruptControllerPopArg( InterruptArgsType** ppList );
InterruptArgsType* InterruptControllerSearchArg( uint32 nInterruptVector, InterruptArgsType** ppList );
typedef enum
{
  INTERRUPT_LOG_EVENT_NULL,
  INTERRUPT_LOG_EVENT_ISR_START,
  INTERRUPT_LOG_EVENT_ISR_FINISH,
  INTERRUPT_LOG_EVENT_SUBISR_START,
  INTERRUPT_LOG_EVENT_SUBISR_FINISH,
  INTERRUPT_LOG_EVENT_TRIGGERED,
  INTERRUPT_LOG_EVENT_SENT,
  INTERRUPT_LOG_EVENT_UNHANDLED,
  INTERRUPT_LOG_EVENT_SUSPENDED,
  INTERRUPT_LOG_EVENT_UNSUPPORTED,
  INTERRUPT_LOG_EVENT_SLEEP,
  INTERRUPT_LOG_EVENT_WAKEUP,
  INTERRUPT_LOG_EVENT_SHUTDOWN,
  INTERRUPT_NUM_LOG_EVENTS,
  PLACEHOLDER_InterruptLogEventType = 0x7fffffff
} InterruptLogEventType;
typedef struct
{
  InterruptLogEventType eEvent;
  uint32 nInterrupt;
  char* pInterruptName;
  uint64 nTimeStamp;
  unsigned long long int pcycles;
} InterruptLogEntryType;
typedef struct
{
  atomic_word_t AtomicIdx;
  uint32 nIdx;
  uint32 nLogSize;
  InterruptLogEntryType* pEntries;
} InterruptLogType;
typedef void (*DALISR_HandlerType) (uint32 param);
typedef struct
{
  DALISR Isr;
  DALISRCtx nParam;
  uint8 nTrigger;
  uint8 nFlags;
  uint32 nCount;
  uint64 nLastFired;
  qurt_thread_t nThreadID;
  unsigned char * pISTStack;
  uint32 nInterruptVector;
  char aISTName[16];
  qurt_anysignal_t ISTSignal;
  qurt_thread_attr_t ThreadAttr;
  uint16 nTimeTestProfileID;
  uint32 nPriority;
  uint32 nISTStackSize;
  char* pInterruptName;
} InterruptStateType;
typedef struct
{
  uint32 nInterruptVector;
  char *pInterruptName;
} InterruptConfigType;
typedef struct
{
  InterruptConfigType *pIRQConfigs;
  uint32 nMaxIRQ;
} InterruptPlatformDataType;
typedef struct
{
  DalDeviceHandle *pTimetickHandle;
  InterruptControllerClientCtxt *pClientCtxt;
  InterruptStateType *pInterruptState;
  InterruptLogType pLog;
  InterruptPlatformDataType *pPlatformConfig;
  uint32 uInterruptController;
  InterruptArgsType *pArgsList;
  InterruptArgsType *pEmptyList;
} InterruptDataType;
<driver name="InterruptController">
<device id=0x02000004>
<props name=INTERRUPT_CONFIG_DATA type=0x00000012>
pInterruptControllerConfigData
</props>
<props name="UINTERRUPT_CONTROLLER" type=0x00000002>
1
</props>
</device>
</driver>

<driver name="NULL">
  <device id="Busywait_QTimer">
        <props name="TIMER_BASE" type=0x00000002>
          0x09000000
        </props>
       <props name="TIMER_OFFSET" type=0x00000002>
          0x003A2000
        </props>
  </device>
</driver>

<driver name="ChipInfo">
    <device id=0x0200006F>
     <props name="ChipIdOverride" type=0x00000002>
       246
     </props>
     <props name="HWREVNUM_PHYS_ADDR" type=0x00000002>
       0x01010000
     </props>
     <props name="HWREVNUM_OFFSET" type=0x00000002>
       0x00141010
     </props>
     <props name="SOC_HW_VERSION_PHYS_ADDR" type=0x00000002>
       0x007A0000
     </props>
     <props name="SOC_HW_VERSION_OFFSET" type=0x00000002>
       0x00008000
     </props>
     <props name="PARTNUM_BMSK" type=0x00000002>
       0xffff000
     </props>
     <props name="PARTNUM_SHFT" type=0x00000002>
       0xc
     </props>
     <props name="VERSION_ID_BMSK" type=0x00000002>
       0xf0000000
     </props>
     <props name="VERSION_ID_SHFT" type=0x00000002>
       0x1c
     </props>
     <props name="QUALCOMM_MFG_ID_BMSK" type=0x00000002>
       0xffe
     </props>
     <props name="QUALCOMM_MFG_ID_SHFT" type=0x00000002>
       0x1
     </props>
     <props name="MAJOR_VERSION_BMSK" type=0x00000002>
       0x0000FF00
     </props>
     <props name="MAJOR_VERSION_SHFT" type=0x00000002>
       0x8
     </props>
     <props name="MINOR_VERSION_BMSK" type=0x00000002>
       0x000000FF
     </props>
     <props name="MINOR_VERSION_SHFT" type=0x00000002>
       0x0
     </props>
     <props name="IsRemotable" type=0x00000002>
       0x1
     </props>
  </device>
</driver>

<driver name="Clock">
  <device id=0x02000145>
    <!-- Clock sources -->
    <props name="ClockSources" type=0x00000012>
      SourceConfig
    </props>
    <props name="IsRemotable" type=0x00000002>
      0x1
    </props>
    <props name="ClockCPUCalCfgV2" type=0x00000012>
      CPUCalibrationFreqV2
    </props>
    <props name="ClockCPUCalCfgV3" type=0x00000012>
      CPUCalibrationFreqV3
    </props>
    <!-- GCC Clock Configurations -->
    <props name="gcc_blsp1_qup1_i2c_apps_clk" type=0x00000012>
      BLSP1QUP1I2CAPPSClockConfig
    </props>
    <props name="gcc_blsp1_qup1_spi_apps_clk" type=0x00000012>
      BLSP1QUP1SPIAPPSClockConfig
    </props>
    <props name="gcc_blsp1_qup2_i2c_apps_clk" type=0x00000012>
      BLSP1QUP1I2CAPPSClockConfig
    </props>
    <props name="gcc_blsp1_qup2_spi_apps_clk" type=0x00000012>
      BLSP1QUP1SPIAPPSClockConfig
    </props>
    <props name="gcc_blsp1_qup3_i2c_apps_clk" type=0x00000012>
      BLSP1QUP1I2CAPPSClockConfig
    </props>
    <props name="gcc_blsp1_qup3_spi_apps_clk" type=0x00000012>
      BLSP1QUP1SPIAPPSClockConfig
    </props>
    <props name="gcc_blsp1_qup4_i2c_apps_clk" type=0x00000012>
      BLSP1QUP1I2CAPPSClockConfig
    </props>
    <props name="gcc_blsp1_qup4_spi_apps_clk" type=0x00000012>
      BLSP1QUP1SPIAPPSClockConfig
    </props>
    <props name="gcc_blsp1_qup5_i2c_apps_clk" type=0x00000012>
      BLSP1QUP1I2CAPPSClockConfig
    </props>
    <props name="gcc_blsp1_qup5_spi_apps_clk" type=0x00000012>
      BLSP1QUP1SPIAPPSClockConfig
    </props>
    <props name="gcc_blsp1_qup6_i2c_apps_clk" type=0x00000012>
      BLSP1QUP1I2CAPPSClockConfig
    </props>
    <props name="gcc_blsp1_qup6_spi_apps_clk" type=0x00000012>
      BLSP1QUP1SPIAPPSClockConfig
    </props>
    <props name="gcc_blsp1_uart1_apps_clk" type=0x00000012>
      BLSP1UART1APPSClockConfig
    </props>
    <props name="gcc_blsp1_uart2_apps_clk" type=0x00000012>
      BLSP1UART1APPSClockConfig
    </props>
    <props name="gcc_blsp1_uart3_apps_clk" type=0x00000012>
      BLSP1UART1APPSClockConfig
    </props>
    <props name="gcc_blsp1_uart4_apps_clk" type=0x00000012>
      BLSP1UART1APPSClockConfig
    </props>
    <props name="gcc_blsp1_uart5_apps_clk" type=0x00000012>
      BLSP1UART1APPSClockConfig
    </props>
    <props name="gcc_blsp1_uart6_apps_clk" type=0x00000012>
      BLSP1UART1APPSClockConfig
    </props>
    <props name="gcc_blsp2_qup1_i2c_apps_clk" type=0x00000012>
      BLSP1QUP1I2CAPPSClockConfig
    </props>
    <props name="gcc_blsp2_qup1_spi_apps_clk" type=0x00000012>
      BLSP1QUP1SPIAPPSClockConfig
    </props>
    <props name="gcc_blsp2_qup2_i2c_apps_clk" type=0x00000012>
      BLSP1QUP1I2CAPPSClockConfig
    </props>
    <props name="gcc_blsp2_qup2_spi_apps_clk" type=0x00000012>
      BLSP1QUP1SPIAPPSClockConfig
    </props>
    <props name="gcc_blsp2_qup3_i2c_apps_clk" type=0x00000012>
      BLSP1QUP1I2CAPPSClockConfig
    </props>
    <props name="gcc_blsp2_qup3_spi_apps_clk" type=0x00000012>
      BLSP1QUP1SPIAPPSClockConfig
    </props>
    <props name="gcc_blsp2_qup4_i2c_apps_clk" type=0x00000012>
      BLSP1QUP1I2CAPPSClockConfig
    </props>
    <props name="gcc_blsp2_qup4_spi_apps_clk" type=0x00000012>
      BLSP1QUP1SPIAPPSClockConfig
    </props>
    <props name="gcc_blsp2_qup5_i2c_apps_clk" type=0x00000012>
      BLSP1QUP1I2CAPPSClockConfig
    </props>
    <props name="gcc_blsp2_qup5_spi_apps_clk" type=0x00000012>
      BLSP1QUP1SPIAPPSClockConfig
    </props>
    <props name="gcc_blsp2_qup6_i2c_apps_clk" type=0x00000012>
      BLSP1QUP1I2CAPPSClockConfig
    </props>
    <props name="gcc_blsp2_qup6_spi_apps_clk" type=0x00000012>
      BLSP1QUP1SPIAPPSClockConfig
    </props>
    <props name="gcc_blsp2_uart1_apps_clk" type=0x00000012>
      BLSP1UART1APPSClockConfig
    </props>
    <props name="gcc_blsp2_uart2_apps_clk" type=0x00000012>
      BLSP1UART1APPSClockConfig
    </props>
    <props name="gcc_blsp2_uart3_apps_clk" type=0x00000012>
      BLSP1UART1APPSClockConfig
    </props>
    <props name="gcc_blsp2_uart4_apps_clk" type=0x00000012>
      BLSP1UART1APPSClockConfig
    </props>
    <props name="gcc_blsp2_uart5_apps_clk" type=0x00000012>
      BLSP1UART1APPSClockConfig
    </props>
    <props name="gcc_blsp2_uart6_apps_clk" type=0x00000012>
      BLSP1UART1APPSClockConfig
    </props>
    <!-- LPASS Clock Configurations -->
    <props name="audio_core_aud_slimbus_clk" type=0x00000012>
      AUDSLIMBUSClockConfig
    </props>
    <props name="audio_core_aud_slimbus_core_clk" type=0x00000012>
      COREClockConfig
    </props>
    <props name="audio_core_avsync_stc_xo_clk" type=0x00000012>
      XOClockConfig
    </props>
    <props name="audio_core_lpaif_codec_spkr_ibit_clk" type=0x00000012>
      LPAIFPCMOEClockConfig
    </props>
    <props name="audio_core_lpaif_codec_spkr_osr_clk" type=0x00000012>
      LPAIFPCMOEClockConfig
    </props>
    <props name="audio_core_lpaif_pcm_data_oe_clk" type=0x00000012>
      LPAIFPCMOEClockConfig
    </props>
    <props name="audio_core_lpaif_pri_ibit_clk" type=0x00000012>
      LPAIFPCMOEClockConfig
    </props>
    <props name="audio_core_lpaif_quad_ibit_clk" type=0x00000012>
      LPAIFPCMOEClockConfig
    </props>
    <props name="audio_core_lpaif_sec_ibit_clk" type=0x00000012>
      LPAIFPCMOEClockConfig
    </props>
    <props name="audio_core_lpaif_ter_ibit_clk" type=0x00000012>
      LPAIFPCMOEClockConfig
    </props>
    <props name="audio_core_qca_slimbus_clk" type=0x00000012>
      AUDSLIMBUSClockConfig
    </props>
    <props name="audio_core_qdsp_sway_aon_clk" type=0x00000012>
      AONClockConfig
    </props>
    <props name="audio_core_resampler_core_clk" type=0x00000012>
      RESAMPLERClockConfig
    </props>
    <props name="audio_wrapper_ext_mclk0_clk" type=0x00000012>
      EXTMCLK0ClockConfig
    </props>
    <props name="audio_wrapper_ext_mclk1_clk" type=0x00000012>
      EXTMCLK0ClockConfig
    </props>
    <props name="audio_wrapper_ext_mclk2_clk" type=0x00000012>
      EXTMCLK0ClockConfig
    </props>
    <props name="lpass_core_avsync_atime_clk" type=0x00000012>
      ATIMEClockConfig
    </props>
    <!-- Clock Log Defaults -->
    <props name="ClockLogDefaults" type=0x00000012>
      ClockLogDefaultConfig
    </props>
    <!-- Clock Init Flags -->
    <props name="ClockInitFlags" type=0x00000012>
      ClockFlagInitConfig
    </props>
    <!-- CX Init VRegLevel -->
    <props name="CXVRegInitLevel" type=0x00000012>
      CXVRegInitLevelConfig
    </props>
    <props name="ClockLPASSBase" type=0x00000012>
      ClockLPASSHWIOBases
    </props>
    <!-- CPU Initialization -->
    <props name="ClockImageConfig" type=0x00000012>
      ClockImageBSPConfig
    </props>
    <props name="ClockImageConfigV2" type=0x00000012>
      ClockImageBSPConfigV2
    </props>
    <props name="ClockImageConfigV3" type=0x00000012>
      ClockImageBSPConfigV3
    </props>
    <!-- Resource Publishing Requirements -->
    <props name="ClockResourcePub" type=0x00000012>
      ClockResourcePub
    </props>
    <props name="ClockSourcesToInit" type=0x00000012>
      ClockSourcesToInit
    </props>
  </device>
</driver>

<driver name="HWIO">
  <device id=0x02000070>
    <props name="BaseMap" type=0x00000012>
      HWIOBaseMap
    </props>
     <props name="IsRemotable" type=0x00000002>
       0x1
     </props>
  </device>
</driver>

<!-- See DDIIPCInt.h for the enum these values come from. -->
<driver name="IPCInt">
  <device id=0x02000099>
    <props name="SourceProc" type=0x00000002>
      6
    </props>
    <props name="IPCINT_PHYSICAL_ADDRESS" type=0x00000002>
      0x9000000
    </props>
    <props name="IPCINT_OFFSET" type=0x00000002>
      0x385000
    </props>
  </device>
</driver>

<driver name="PlatformInfo">
  <device id=0x02000139>
  </device>
</driver>

<driver name="NULL">
  <device id="/lpass/pmic">
    <props name="PAM_Node_Rsr" type=0x00000012>
      pm_npa_lpass_pam_node_rsrcs
    </props>
    <props name="PAM_Num" type=0x00000012>
      num_of_pm_lpass_nodes
    </props>
    <props name="MX_Info" type=0x00000012>
      MX_Info
    </props>
    <props name="CX_Info" type=0x00000012>
      CX_Info
    </props>
    <props name="RemoteLDORsrc" type=0x00000012>
      pmic_npa_remote_ldo
    </props>
    <props name="RemoteVSRsrc" type=0x00000012>
      pmic_npa_remote_vs
    </props>
    <props name="RemoteSMPSRsrc" type=0x00000012>
      pmic_npa_remote_smps
    </props>
  </device>
</driver>

<driver name="Timetick">
  <device id="SystemTimer">
  <props name="DEFAULT_FREQUENCY" type=0x00000002>
   19200000
  </props>
  <props name="IsRemotable" type=0x00000002>
   0x1
  </props>
  <props name="QTIMER_FRAME" type=0x00000002>
   1
  </props>
  <props name="QTIMER_BASE" type=0x00000002>
    0x09000000
  </props>
  <props name="QTIMER_OFFSET" type=0x00000002>
   0x003A2000
  </props>
  <props name="QTIMER_AC_OFFSET" type=0x00000002>
   0x003A0000
  </props>
  <props name="QTIMER_INTVECTNUM" type=0x00000002>
   3
  </props>
  </device>
  <device id="WakeUpTimer">
  <props name="DEFAULT_FREQUENCY" type=0x00000002>
   19200000
  </props>
  <props name="QTIMER_FRAME" type=0x00000002>
   2
  </props>
  <props name="QTIMER_BASE" type=0x00000002>
   0x09000000
  </props>
 <props name="QTIMER_OFFSET" type=0x00000002>
   0x003A3000
  </props>
  <props name="QTIMER_AC_OFFSET" type=0x00000002>
   0x003A0000
  </props>
  <props name="QTIMER_INTVECTNUM" type=0x00000002>
   30
  </props>
  </device>
</driver>

  <device id="/tlmm/dragonboard">
    <props name="blsp_uart_tx[8]" type="TLMMGpioIdType">{4, 2}</props>
    <props name="blsp_uart_rx[8]" type="TLMMGpioIdType">{5, 2}</props>
    <props name="blsp_i2c_sda[8]" type="TLMMGpioIdType">{6, 3}</props>
    <props name="blsp_i2c_scl[8]" type="TLMMGpioIdType">{7, 3}</props>
    <props name="lcd0_reset_n" type="TLMMGpioIdType">{8, 0}</props>
    <props name="disp_gpio3" type="TLMMGpioIdType">{9, 255}</props>
    <props name="mdp_vsync_p" type="TLMMGpioIdType">{10, 1}</props>
    <props name="mdp_vsync_s" type="TLMMGpioIdType">{11, 1}</props>
    <props name="cam_mclk0" type="TLMMGpioIdType">{13, 1}</props>
    <props name="cam_mclk1" type="TLMMGpioIdType">{14, 1}</props>
    <props name="cam_mclk2" type="TLMMGpioIdType">{15, 1}</props>
    <props name="cam_mclk3" type="TLMMGpioIdType">{16, 1}</props>
    <props name="cci_i2c_sda0" type="TLMMGpioIdType">{17, 1}</props>
    <props name="cci_i2c_scl0" type="TLMMGpioIdType">{18, 1}</props>
    <props name="cci_i2c_sda1" type="TLMMGpioIdType">{19, 1}</props>
    <props name="cci_i2c_scl1" type="TLMMGpioIdType">{20, 1}</props>
    <props name="cci_timer0" type="TLMMGpioIdType">{21, 1}</props>
    <props name="cci_timer1" type="TLMMGpioIdType">{22, 1}</props>
    <props name="cam2_rst_n" type="TLMMGpioIdType">{23, 0}</props>
    <props name="cam_irq" type="TLMMGpioIdType">{24, 255}</props>
    <props name="blsp_i2c_sda[6]" type="TLMMGpioIdType">{27, 3}</props>
    <props name="blsp_i2c_scl[6]" type="TLMMGpioIdType">{28, 3}</props>
    <props name="cam0_standby_n" type="TLMMGpioIdType">{29, 0}</props>
    <props name="cam0_rst_n" type="TLMMGpioIdType">{30, 0}</props>
    <props name="hdmi_cec" type="TLMMGpioIdType">{31, 1}</props>
    <props name="hdmi_ddc_clock" type="TLMMGpioIdType">{32, 1}</props>
    <props name="hdmi_ddc_data" type="TLMMGpioIdType">{33, 1}</props>
    <props name="hdmi_hot_plug_detect" type="TLMMGpioIdType">{34, 1}</props>
    <props name="pci_e0_rst_n" type="TLMMGpioIdType">{35, 1}</props>
    <props name="pci_e0_clkreqn" type="TLMMGpioIdType">{36, 1}</props>
    <props name="pci_e0_wake" type="TLMMGpioIdType">{37, 0}</props>
    <props name="sd_card_det_n" type="TLMMGpioIdType">{38, 0}</props>
    <props name="edu_gp" type="TLMMGpioIdType">{39, 255}</props>
    <props name="edu_irq" type="TLMMGpioIdType">{40, 255}</props>
    <props name="blsp_uart_tx[2]" type="TLMMGpioIdType">{41, 2}</props>
    <props name="blsp_uart_rx[2]" type="TLMMGpioIdType">{42, 2}</props>
    <props name="blsp_uart_cts_n[2]" type="TLMMGpioIdType">{43, 2}</props>
    <props name="blsp_uart_rfr_n[2]" type="TLMMGpioIdType">{44, 2}</props>
    <props name="blsp_spi_mosi[3]" type="TLMMGpioIdType">{45, 1}</props>
    <props name="blsp_spi_miso[3]" type="TLMMGpioIdType">{46, 1}</props>
    <props name="blsp_spi_cs_n[3]" type="TLMMGpioIdType">{47, 1}</props>
    <props name="blsp_spi_clk[3]" type="TLMMGpioIdType">{48, 1}</props>
    <props name="blsp_spi_mosi[1]" type="TLMMGpioIdType">{49, 255}</props>
    <props name="blsp_spi_miso[1]" type="TLMMGpioIdType">{50, 255}</props>
    <props name="blsp_spi_cs_n[1]" type="TLMMGpioIdType">{51, 255}</props>
    <props name="blsp_spi_clk[1]" type="TLMMGpioIdType">{52, 255}</props>
    <props name="codec_int2" type="TLMMGpioIdType">{53, 0}</props>
    <props name="codec_int1" type="TLMMGpioIdType">{54, 0}</props>
    <props name="qua_mi2s_mclk" type="TLMMGpioIdType">{57, 1}</props>
    <props name="qua_mi2s_sck" type="TLMMGpioIdType">{58, 1}</props>
    <props name="qua_mi2s_ws" type="TLMMGpioIdType">{59, 1}</props>
    <props name="qua_mi2s_data0" type="TLMMGpioIdType">{60, 1}</props>
    <props name="qua_mi2s_data1" type="TLMMGpioIdType">{61, 1}</props>
    <props name="qua_mi2s_data2" type="TLMMGpioIdType">{62, 1}</props>
    <props name="qua_mi2s_data3" type="TLMMGpioIdType">{63, 1}</props>
    <props name="codec_rst_n" type="TLMMGpioIdType">{64, 0}</props>
    <props name="pcm_clk" type="TLMMGpioIdType">{65, 0}</props>
    <props name="pcm_sync" type="TLMMGpioIdType">{66, 0}</props>
    <props name="pcm_din" type="TLMMGpioIdType">{67, 0}</props>
    <props name="pcm_dout" type="TLMMGpioIdType">{68, 0}</props>
    <props name="audio_ref_clk" type="TLMMGpioIdType">{69, 2}</props>
    <props name="lpass_slimbus_clk" type="TLMMGpioIdType">{70, 1}</props>
    <props name="lpass_slimbus_data0" type="TLMMGpioIdType">{71, 1}</props>
    <props name="lpass_slimbus_data1" type="TLMMGpioIdType">{72, 1}</props>
    <props name="ts1_reset_n" type="TLMMGpioIdType">{79, 0}</props>
    <props name="mems_reset_n" type="TLMMGpioIdType">{80, 0}</props>
    <props name="disp_gpio2" type="TLMMGpioIdType">{81, 255}</props>
    <props name="blsp_spi_mosi[12]" type="TLMMGpioIdType">{85, 1}</props>
    <props name="blsp_spi_miso[12]" type="TLMMGpioIdType">{86, 1}</props>
    <props name="blsp_i2c_sda[12]" type="TLMMGpioIdType">{87, 3}</props>
    <props name="blsp_i2c_scl[12]" type="TLMMGpioIdType">{88, 3}</props>
    <props name="ts0_reset_n" type="TLMMGpioIdType">{89, 0}</props>
    <props name="cam1_standby_n" type="TLMMGpioIdType">{98, 0}</props>
    <props name="lcd1_reset_n" type="TLMMGpioIdType">{101, 0}</props>
    <props name="pcie_slt_prsnt_n" type="TLMMGpioIdType">{102, 255}</props>
    <props name="usb_hub_reset_n" type="TLMMGpioIdType">{103, 255}</props>
    <props name="cam1_rst_n" type="TLMMGpioIdType">{104, 0}</props>
    <props name="nfc_disable" type="TLMMGpioIdType">{105, 0}</props>
    <props name="nfc_irq" type="TLMMGpioIdType">{106, 0}</props>
    <props name="disp_gpio1" type="TLMMGpioIdType">{107, 255}</props>
    <props name="ts_int_1_n" type="TLMMGpioIdType">{108, 255}</props>
    <props name="uim1_data" type="TLMMGpioIdType">{109, 1}</props>
    <props name="uim1_clk" type="TLMMGpioIdType">{110, 1}</props>
    <props name="uim1_reset" type="TLMMGpioIdType">{111, 1}</props>
    <props name="uim1_present" type="TLMMGpioIdType">{112, 1}</props>
    <props name="pci_e2_rst_n" type="TLMMGpioIdType">{114, 2}</props>
    <props name="pci_e2_clkreq_n" type="TLMMGpioIdType">{115, 255}</props>
    <props name="pci_e2_wake" type="TLMMGpioIdType">{116, 0}</props>
    <props name="ssc_irq_0" type="TLMMGpioIdType">{117, 255}</props>
    <props name="ssc_irq_1" type="TLMMGpioIdType">{118, 255}</props>
    <props name="ssc_irq_2" type="TLMMGpioIdType">{119, 255}</props>
    <props name="ssc_irq_3" type="TLMMGpioIdType">{120, 255}</props>
    <props name="ssc_irq_4" type="TLMMGpioIdType">{121, 255}</props>
    <props name="ssc_irq_5" type="TLMMGpioIdType">{122, 255}</props>
    <props name="ssc_irq_6" type="TLMMGpioIdType">{123, 255}</props>
    <props name="ssc_irq_7" type="TLMMGpioIdType">{124, 255}</props>
    <props name="ssc_irq_8" type="TLMMGpioIdType">{125, 255}</props>
    <props name="edu_gpio1" type="TLMMGpioIdType">{127, 255}</props>
    <props name="pci_e1_rst_n" type="TLMMGpioIdType">{130, 1}</props>
    <props name="pci_e1_clkreqn" type="TLMMGpioIdType">{131, 1}</props>
    <props name="pci_e1_wake" type="TLMMGpioIdType">{132, 0}</props>
    <props name="cam2_standby_n" type="TLMMGpioIdType">{133, 0}</props>
    <props name="edu_gpio3" type="TLMMGpioIdType">{134, 255}</props>
    <props name="bl0_en" type="TLMMGpioIdType">{135, 0}</props>
    <props name="qspi_cs_n_1" type="TLMMGpioIdType">{138, 2}</props>
    <props name="ssbi2" type="TLMMGpioIdType">{139, 2}</props>
    <props name="ssbi1" type="TLMMGpioIdType">{140, 2}</props>
    <props name="qspi_cs_n_0" type="TLMMGpioIdType">{141, 2}</props>
    <props name="qspi_reset_n" type="TLMMGpioIdType">{142, 0}</props>
    <props name="qspi_clk" type="TLMMGpioIdType">{145, 2}</props>
    <props name="qspi_data[0]" type="TLMMGpioIdType">{146, 2}</props>
    <props name="qspi_data[1]" type="TLMMGpioIdType">{147, 2}</props>
    <props name="qspi_data[2]" type="TLMMGpioIdType">{148, 2}</props>
    <props name="qspi_data[3]" type="TLMMGpioIdType">{149, 2}</props>
  </device>

  <device id="/tlmm/liquid">
    <props name="blsp_spi_mosi[1]" type="TLMMGpioIdType">{0, 1}</props>
    <props name="blsp_spi_miso[1]" type="TLMMGpioIdType">{1, 1}</props>
    <props name="blsp_spi_cs_n[1]" type="TLMMGpioIdType">{2, 1}</props>
    <props name="blsp_spi_clk[1]" type="TLMMGpioIdType">{3, 1}</props>
    <props name="blsp_uart_tx[8]" type="TLMMGpioIdType">{4, 2}</props>
    <props name="blsp_uart_rx[8]" type="TLMMGpioIdType">{5, 2}</props>
    <props name="blsp_i2c_sda[8]" type="TLMMGpioIdType">{6, 3}</props>
    <props name="blsp_i2c_scl[8]" type="TLMMGpioIdType">{7, 3}</props>
    <props name="lcd0_reset_n" type="TLMMGpioIdType">{8, 0}</props>
    <props name="nfc_irq" type="TLMMGpioIdType">{9, 0}</props>
    <props name="mdp_vsync_p" type="TLMMGpioIdType">{10, 1}</props>
    <props name="epm_int_n" type="TLMMGpioIdType">{11, 0}</props>
    <props name="nfc_disable" type="TLMMGpioIdType">{12, 0}</props>
    <props name="cam_mclk0" type="TLMMGpioIdType">{13, 1}</props>
    <props name="cam_mclk1" type="TLMMGpioIdType">{14, 1}</props>
    <props name="cam_mclk2" type="TLMMGpioIdType">{15, 1}</props>
    <props name="webcam2_reset_n" type="TLMMGpioIdType">{16, 0}</props>
    <props name="cci_i2c_sda0" type="TLMMGpioIdType">{17, 1}</props>
    <props name="cci_i2c_scl0" type="TLMMGpioIdType">{18, 1}</props>
    <props name="cci_i2c_sda1" type="TLMMGpioIdType">{19, 1}</props>
    <props name="cci_i2c_scl1" type="TLMMGpioIdType">{20, 1}</props>
    <props name="cci_timer0" type="TLMMGpioIdType">{21, 1}</props>
    <props name="cci_timer1" type="TLMMGpioIdType">{22, 1}</props>
    <props name="webcam1_reset_n" type="TLMMGpioIdType">{23, 0}</props>
    <props name="hdmi_mux_sel" type="TLMMGpioIdType">{25, 0}</props>
    <props name="webcam1_standby" type="TLMMGpioIdType">{26, 0}</props>
    <props name="blsp_i2c_sda[6]" type="TLMMGpioIdType">{27, 3}</props>
    <props name="blsp_i2c_scl[6]" type="TLMMGpioIdType">{28, 3}</props>
    <props name="cam1_standby_n" type="TLMMGpioIdType">{29, 0}</props>
    <props name="cam1_rst_n" type="TLMMGpioIdType">{30, 0}</props>
    <props name="hdmi_cec" type="TLMMGpioIdType">{31, 1}</props>
    <props name="hdmi_ddc_clock" type="TLMMGpioIdType">{32, 1}</props>
    <props name="hdmi_ddc_data" type="TLMMGpioIdType">{33, 1}</props>
    <props name="hdmi_hot_plug_detect" type="TLMMGpioIdType">{34, 1}</props>
    <props name="pci_e0_rst_n" type="TLMMGpioIdType">{35, 1}</props>
    <props name="pci_e0_clkreqn" type="TLMMGpioIdType">{36, 1}</props>
    <props name="pci_e0_wake" type="TLMMGpioIdType">{37, 0}</props>
    <props name="fm_rds_int_n" type="TLMMGpioIdType">{38, 0}</props>
    <props name="fm_rst_n" type="TLMMGpioIdType">{39, 0}</props>
    <props name="sd_write_protect" type="TLMMGpioIdType">{40, 1}</props>
    <props name="blsp_uart_tx[2]" type="TLMMGpioIdType">{41, 2}</props>
    <props name="blsp_uart_rx[2]" type="TLMMGpioIdType">{42, 2}</props>
    <props name="blsp_uart_cts_n[2]" type="TLMMGpioIdType">{43, 2}</props>
    <props name="blsp_uart_rfr_n[2]" type="TLMMGpioIdType">{44, 2}</props>
    <props name="blsp_uart_tx[3]" type="TLMMGpioIdType">{45, 2}</props>
    <props name="blsp_uart_rx[3]" type="TLMMGpioIdType">{46, 2}</props>
    <props name="blsp_uart_cts_n[3]" type="TLMMGpioIdType">{47, 2}</props>
    <props name="blsp_uart_rfr_n[3]" type="TLMMGpioIdType">{48, 2}</props>
    <props name="uim3_data" type="TLMMGpioIdType">{49, 1}</props>
    <props name="uim3_clk" type="TLMMGpioIdType">{50, 1}</props>
    <props name="uim3_reset" type="TLMMGpioIdType">{51, 1}</props>
    <props name="uim3_present" type="TLMMGpioIdType">{52, 1}</props>
    <props name="codec_int2" type="TLMMGpioIdType">{53, 0}</props>
    <props name="codec_int1" type="TLMMGpioIdType">{54, 0}</props>
    <props name="blsp_i2c_sda[7]" type="TLMMGpioIdType">{55, 3}</props>
    <props name="blsp_i2c_scl[7]" type="TLMMGpioIdType">{56, 3}</props>
    <props name="forced_usb_boot" type="TLMMGpioIdType">{57, 6}</props>
    <props name="uim4_data" type="TLMMGpioIdType">{58, 2}</props>
    <props name="uim4_clk" type="TLMMGpioIdType">{59, 2}</props>
    <props name="uim4_reset" type="TLMMGpioIdType">{60, 2}</props>
    <props name="uim4_present" type="TLMMGpioIdType">{61, 2}</props>
    <props name="cam2_standby_n" type="TLMMGpioIdType">{62, 0}</props>
    <props name="gesture_reset_n" type="TLMMGpioIdType">{63, 0}</props>
    <props name="codec_rst_n" type="TLMMGpioIdType">{64, 0}</props>
    <props name="pcm_clk" type="TLMMGpioIdType">{65, 1}</props>
    <props name="pcm_sync" type="TLMMGpioIdType">{66, 1}</props>
    <props name="pcm_din" type="TLMMGpioIdType">{67, 1}</props>
    <props name="pcm_dout" type="TLMMGpioIdType">{68, 1}</props>
    <props name="audio_ref_clk" type="TLMMGpioIdType">{69, 2}</props>
    <props name="lpass_slimbus_clk" type="TLMMGpioIdType">{70, 1}</props>
    <props name="lpass_slimbus_data0" type="TLMMGpioIdType">{71, 1}</props>
    <props name="lpass_slimbus_data1" type="TLMMGpioIdType">{72, 1}</props>
    <props name="btfm_slimbus_data" type="TLMMGpioIdType">{73, 1}</props>
    <props name="btfm_slimbus_clk" type="TLMMGpioIdType">{74, 1}</props>
    <props name="ter_mi2s_sck" type="TLMMGpioIdType">{75, 1}</props>
    <props name="ter_mi2s_ws" type="TLMMGpioIdType">{76, 1}</props>
    <props name="ter_mi2s_data0" type="TLMMGpioIdType">{77, 1}</props>
    <props name="gp_pdm_2a" type="TLMMGpioIdType">{79, 0}</props>
    <props name="mems_reset_n" type="TLMMGpioIdType">{80, 0}</props>
    <props name="blsp_spi_mosi[5]" type="TLMMGpioIdType">{81, 2}</props>
    <props name="blsp_spi_miso[5]" type="TLMMGpioIdType">{82, 2}</props>
    <props name="blsp_spi_cs_n[5]" type="TLMMGpioIdType">{83, 2}</props>
    <props name="blsp_spi_clk[5]" type="TLMMGpioIdType">{84, 1}</props>
    <props name="blsp_spi_mosi[12]" type="TLMMGpioIdType">{85, 1}</props>
    <props name="ts_aux" type="TLMMGpioIdType">{86, 0}</props>
    <props name="blsp_i2c_sda[12]" type="TLMMGpioIdType">{87, 3}</props>
    <props name="blsp_i2c_scl[12]" type="TLMMGpioIdType">{88, 3}</props>
    <props name="ts_reset_n" type="TLMMGpioIdType">{89, 0}</props>
    <props name="blsp1_spi_cs1_n" type="TLMMGpioIdType">{90, 2}</props>
    <props name="epm_marker11" type="TLMMGpioIdType">{91, 0}</props>
    <props name="epm_marker2" type="TLMMGpioIdType">{92, 0}</props>
    <props name="epm_global_en" type="TLMMGpioIdType">{93, 0}</props>
    <props name="wigg_en" type="TLMMGpioIdType">{94, 0}</props>
    <props name="sd_card_det_n" type="TLMMGpioIdType">{95, 0}</props>
    <props name="hdmi_mux_en" type="TLMMGpioIdType">{96, 0}</props>
    <props name="grfc[0]" type="TLMMGpioIdType">{97, 1}</props>
    <props name="grfc[1]" type="TLMMGpioIdType">{98, 1}</props>
    <props name="grfc[2]" type="TLMMGpioIdType">{99, 1}</props>
    <props name="grfc[3]" type="TLMMGpioIdType">{100, 1}</props>
    <props name="grfc[4]" type="TLMMGpioIdType">{101, 1}</props>
    <props name="grfc[5]" type="TLMMGpioIdType">{102, 1}</props>
    <props name="grfc[6]" type="TLMMGpioIdType">{103, 1}</props>
    <props name="grfc[7]" type="TLMMGpioIdType">{104, 1}</props>
    <props name="uim2_data" type="TLMMGpioIdType">{105, 1}</props>
    <props name="uim2_clk" type="TLMMGpioIdType">{106, 1}</props>
    <props name="uim2_reset" type="TLMMGpioIdType">{107, 1}</props>
    <props name="uim2_present" type="TLMMGpioIdType">{108, 1}</props>
    <props name="uim1_data" type="TLMMGpioIdType">{109, 1}</props>
    <props name="uim1_clk" type="TLMMGpioIdType">{110, 1}</props>
    <props name="uim1_reset" type="TLMMGpioIdType">{111, 1}</props>
    <props name="uim1_present" type="TLMMGpioIdType">{112, 1}</props>
    <props name="uim_batt_alarm" type="TLMMGpioIdType">{113, 1}</props>
    <props name="grfc[8]" type="TLMMGpioIdType">{114, 1}</props>
    <props name="grfc[9]" type="TLMMGpioIdType">{115, 1}</props>
    <props name="grfc[10]" type="TLMMGpioIdType">{116, 1}</props>
    <props name="ssc_irq_0" type="TLMMGpioIdType">{117, 0}</props>
    <props name="ssc_irq_1" type="TLMMGpioIdType">{118, 0}</props>
    <props name="ssc_irq_2" type="TLMMGpioIdType">{119, 0}</props>
    <props name="ssc_irq_3" type="TLMMGpioIdType">{120, 0}</props>
    <props name="ssc_irq_4" type="TLMMGpioIdType">{121, 0}</props>
    <props name="ssc_irq_5" type="TLMMGpioIdType">{122, 0}</props>
    <props name="ssc_irq_6" type="TLMMGpioIdType">{123, 0}</props>
    <props name="ssc_irq_7" type="TLMMGpioIdType">{124, 0}</props>
    <props name="ssc_irq_8" type="TLMMGpioIdType">{125, 0}</props>
    <props name="grfc[11]" type="TLMMGpioIdType">{127, 1}</props>
    <props name="grfc[12]" type="TLMMGpioIdType">{128, 1}</props>
    <props name="grfc[13]" type="TLMMGpioIdType">{129, 1}</props>
    <props name="pci_e1_rst_n" type="TLMMGpioIdType">{130, 1}</props>
    <props name="pci_e1_clkreqn" type="TLMMGpioIdType">{131, 1}</props>
    <props name="pci_e1_wake" type="TLMMGpioIdType">{132, 0}</props>
    <props name="grfc[14]" type="TLMMGpioIdType">{133, 1}</props>
    <props name="gsm_tx_phase_d0" type="TLMMGpioIdType">{134, 1}</props>
    <props name="gsm_tx_phase_d1" type="TLMMGpioIdType">{135, 1}</props>
    <props name="grfc[15]" type="TLMMGpioIdType">{136, 1}</props>
    <props name="rffe3_data" type="TLMMGpioIdType">{137, 1}</props>
    <props name="rffe3_clk" type="TLMMGpioIdType">{138, 1}</props>
    <props name="rffe4_data" type="TLMMGpioIdType">{139, 1}</props>
    <props name="rffe4_clk" type="TLMMGpioIdType">{140, 1}</props>
    <props name="rffe5_data" type="TLMMGpioIdType">{141, 1}</props>
    <props name="rffe5_clk" type="TLMMGpioIdType">{142, 1}</props>
    <props name="gps_tx_aggressor" type="TLMMGpioIdType">{143, 1}</props>
    <props name="mss_lte_coxm_txd" type="TLMMGpioIdType">{144, 1}</props>
    <props name="mss_lte_coxm_rxd" type="TLMMGpioIdType">{145, 1}</props>
    <props name="rffe2_data" type="TLMMGpioIdType">{146, 1}</props>
    <props name="rffe2_clk" type="TLMMGpioIdType">{147, 1}</props>
    <props name="rffe1_data" type="TLMMGpioIdType">{148, 1}</props>
    <props name="rffe1_clk" type="TLMMGpioIdType">{149, 1}</props>
  </device>

enum_header_path "msmhwiobase.h"
enum_header_path "DALTLMMPropDef.h"
enum_header_path "PlatformInfoDefs.h"
<driver name="TLMM">
typedef enum
{
   DAL_GPIO_MODE_PRIMARY,
   DAL_GPIO_MODE_IO,
   DAL_PLACEHOLDER_DALGpioModeType = 0x7fffffff
}DalGpioModeType;
typedef enum
{
  DAL_GPIO_INACTIVE = 0,
  DAL_GPIO_ACTIVE = 1,
  DAL_PLACEHOLDER_DALGpioStatusType = 0x7fffffff
}DALGpioStatusType;
typedef enum
{
  DAL_GPIO_OWNER_MASTER,
  DAL_GPIO_OWNER_APPS1,
  DAL_PLACEHOLDER_DALGpioOwnerType = 0x7fffffff
}DALGpioOwnerType;
typedef enum
{
  DAL_GPIO_UNLOCKED = 0,
  DAL_GPIO_LOCKED = 1,
  DAL_PLACEHOLDER_DALGpioLockType = 0x7fffffff
}DALGpioLockType;
typedef enum
{
  DAL_GPIO_CLIENT_PWRDB = 0,
  DAL_PLACEHOLDER_DALGpioClientType = 0x7fffffff
}DALGpioClientType;
typedef enum
{
  DAL_GPIO_INPUT = 0,
  DAL_GPIO_OUTPUT = 1,
  DAL_PLACEHOLDER_DALGpioDirectionType = 0x7fffffff
}DALGpioDirectionType;
typedef enum
{
  DAL_GPIO_NO_PULL = 0,
  DAL_GPIO_PULL_DOWN = 0x1,
  DAL_GPIO_KEEPER = 0x2,
  DAL_GPIO_PULL_UP = 0x3,
  DAL_PLACEHOLDER_DALGpioPullType = 0x7fffffff
}DALGpioPullType;
typedef enum
{
  DAL_GPIO_2MA = 0,
  DAL_GPIO_4MA = 0x1,
  DAL_GPIO_6MA = 0x2,
  DAL_GPIO_8MA = 0x3,
  DAL_GPIO_10MA = 0x4,
  DAL_GPIO_12MA = 0x5,
  DAL_GPIO_14MA = 0x6,
  DAL_GPIO_16MA = 0x7,
  DAL_PLACEHOLDER_DALGpioDriveType = 0x7fffffff
}DALGpioDriveType;
typedef enum
{
  DAL_GPIO_LOW_VALUE,
  DAL_GPIO_HIGH_VALUE,
  DAL_PLACEHOLDER_DALGpioValueType = 0x7fffffff
}DALGpioValueType;
typedef uint32 DALGpioSignalType;
typedef uint32 DALGpioIdType;
typedef enum
{
  DAL_GPIO_SLEEP_CONFIG_ALL,
  DAL_GPIO_SLEEP_CONFIG_APPS,
  DAL_GPIO_SLEEP_CONFIG_NUM_MODES,
  DAL_PLACEHOLDER_DALGpioSleepConfigType = 0x7fffffff
}DALGpioSleepConfigType;
typedef enum
{
  DAL_TLMM_GPIO_DISABLE,
  DAL_TLMM_GPIO_ENABLE,
  DAL_PLACEHOLDER_DALGpioEnableType = 0x7fffffff
}DALGpioEnableType;
typedef enum
{
  DAL_TLMM_USB_PORT_SEL,
  DAL_TLMM_PORT_CONFIG_USB,
  DAL_TLMM_PORT_TSIF,
  DAL_TLMM_AUD_PCM,
  DAL_TLMM_UART1,
  DAL_TLMM_UART3,
  DAL_TLMM_LCDC_CFG,
  DAL_TLMM_KEYSENSE_SC_IRQ,
  DAL_TLMM_KEYSENSE_A9_IRQ,
  DAL_TLMM_TCXO_EN,
  DAL_TLMM_SSBI_PMIC,
  DAL_TLMM_PA_RANGE1,
  DAL_TLMM_SPARE_WR_UART_SEL,
  DAL_TLMM_PAD_ALT_FUNC_SDIO_EN,
  DAL_TLMM_SPARE_WR_TCXO_EN,
  DAL_TLMM_SPARE_WR_PA_ON,
  DAL_TLMM_SDC3_CTL,
  DAL_TLMM_SDC4_CTL,
  DAL_TLMM_UIM1_PAD_CTL,
  DAL_TLMM_UIM2_PAD_CTL,
  DAL_TLMM_NUM_PORTS,
  DAL_PLACEHOLDER_DALGpioPortType = 0x7fffffff
}DALGpioPortType;
typedef struct
{
  DALGpioDirectionType eDirection;
  DALGpioPullType ePull;
  DALGpioDriveType eDriveStrength;
}DalTlmm_GpioConfigIdType;
typedef struct
{
  uint32 nGpioNumber;
  uint32 nFunctionSelect;
  DalTlmm_GpioConfigIdType Settings;
  DALGpioValueType eOutputDrive;
}DalTlmm_GpioIdSettingsType;
typedef unsigned long int uint32;
typedef unsigned short uint16;
typedef unsigned char uint8;
typedef signed long int int32;
typedef signed short int16;
typedef signed char int8;
typedef unsigned long long uint64;
typedef long long int64;
typedef unsigned char byte;
typedef uint32 DALBOOL;
typedef uint32 DALDEVICEID;
typedef uint32 DalPowerCmd;
typedef uint32 DalPowerDomain;
typedef uint32 DalSysReq;
typedef uint32 DALHandle;
typedef int DALResult;
typedef void * DALEnvHandle;
typedef void * DALSYSEventHandle;
typedef uint32 DALMemAddr;
typedef uint32 DALSYSMemAddr;
typedef uint64 DALSYSPhyAddr;
typedef uint32 DALInterfaceVersion;
typedef unsigned char * DALDDIParamPtr;
typedef struct DALEventObject DALEventObject;
struct DALEventObject
{
    uint32 obj[8];
};
typedef DALEventObject * DALEventHandle;
typedef struct _DALMemObject
{
   uint32 memAttributes;
   uint32 sysObjInfo[2];
   uint32 dwLen;
   uint32 ownerVirtAddr;
   uint32 virtAddr;
   uint32 physAddr;
}
DALMemObject;
typedef struct _DALDDIMemBufDesc
{
   uint32 dwOffset;
   uint32 dwLen;
   uint32 dwUser;
}
DALDDIMemBufDesc;
typedef struct _DALDDIMemDescList
{
   uint32 dwFlags;
   uint32 dwNumBufs;
   DALDDIMemBufDesc bufList[1];
}
DALDDIMemDescList;
typedef struct DALSysMemDescBuf DALSysMemDescBuf;
struct DALSysMemDescBuf
{
   DALSYSMemAddr VirtualAddr;
   DALSYSMemAddr PhysicalAddr;
   uint32 size;
   uint32 user;
};
typedef struct { uint32 dwObjInfo; DALSYSMemAddr thisVirtualAddr; DALSYSMemAddr PhysicalAddr; DALSYSMemAddr VirtualAddr; uint32 hOwnerProc; uint32 dwCurBufIdx; uint32 dwNumDescBufs; DALSysMemDescBuf BufInfo[1];} DALSysMemDescList;
typedef unsigned char boolean;
typedef unsigned short word;
typedef unsigned long dword;
typedef unsigned char uint1;
typedef unsigned short uint2;
typedef unsigned long uint4;
typedef signed char int1;
typedef signed short int2;
typedef long int int4;
typedef signed long sint31;
typedef signed short sint15;
typedef signed char sint7;
typedef uint16 UWord16 ;
typedef uint32 UWord32 ;
typedef int32 Word32 ;
typedef int16 Word16 ;
typedef uint8 UWord8 ;
typedef int8 Word8 ;
typedef int32 Vect32 ;
typedef enum
{
  DALPLATFORMINFO_TYPE_UNKNOWN = 0x00,
  DALPLATFORMINFO_TYPE_SURF = 0x01,
  DALPLATFORMINFO_TYPE_FFA = 0x02,
  DALPLATFORMINFO_TYPE_FLUID = 0x03,
  DALPLATFORMINFO_TYPE_FUSION = 0x04,
  DALPLATFORMINFO_TYPE_OEM = 0x05,
  DALPLATFORMINFO_TYPE_QT = 0x06,
  DALPLATFORMINFO_TYPE_CDP = DALPLATFORMINFO_TYPE_SURF,
  DALPLATFORMINFO_TYPE_MTP_MDM = 0x07,
  DALPLATFORMINFO_TYPE_MTP_MSM = 0x08,
  DALPLATFORMINFO_TYPE_MTP = DALPLATFORMINFO_TYPE_MTP_MSM,
  DALPLATFORMINFO_TYPE_LIQUID = 0x09,
  DALPLATFORMINFO_TYPE_DRAGONBOARD = 0x0A,
  DALPLATFORMINFO_TYPE_QRD = 0x0B,
  DALPLATFORMINFO_TYPE_EVB = 0x0C,
  DALPLATFORMINFO_TYPE_HRD = 0x0D,
  DALPLATFORMINFO_TYPE_DTV = 0x0E,
  DALPLATFORMINFO_TYPE_RUMI = 0x0F,
  DALPLATFORMINFO_TYPE_VIRTIO = 0x10,
  DALPLATFORMINFO_TYPE_GOBI = 0x11,
  DALPLATFORMINFO_TYPE_CBH = 0x12,
  DALPLATFORMINFO_TYPE_BTS = 0x13,
  DALPLATFORMINFO_TYPE_XPM = 0x14,
  DALPLATFORMINFO_TYPE_RCM = 0x15,
  DALPLATFORMINFO_TYPE_DMA = 0x16,
  DALPLATFORMINFO_TYPE_STP = 0x17,
  DALPLATFORMINFO_TYPE_SBC = 0x18,
  DALPLATFORMINFO_TYPE_ADP = 0x19,
  DALPLATFORMINFO_NUM_TYPES = 0x20,
  DALPLATFORMINFO_TYPE_32BITS = 0x7FFFFFFF
} DalPlatformInfoPlatformType;
typedef enum
{
  DALPLATFORMINFO_KEY_UNKNOWN = 0x00,
  DALPLATFORMINFO_KEY_DDR_FREQ = 0x01,
  DALPLATFORMINFO_KEY_GFX_FREQ = 0x02,
  DALPLATFORMINFO_KEY_CAMERA_FREQ = 0x03,
  DALPLATFORMINFO_KEY_FUSION = 0x04,
  DALPLATFORMINFO_KEY_CUST = 0x05,
  DALPLATFORMINFO_NUM_KEYS = 0x06,
  DALPLATFORMINFO_KEY_32BITS = 0x7FFFFFFF
} DalPlatformInfoKeyType;
typedef enum
{
  DALPLATFORMINFO_SUBTYPE_UNKNOWN = 0x00,
  DALPLATFORMINFO_SUBTYPE_CSFB = 0x01,
  DALPLATFORMINFO_SUBTYPE_SVLTE1 = 0x02,
  DALPLATFORMINFO_SUBTYPE_SVLTE2A = 0x03,
  DALPLATFORMINFO_SUBTYPE_SVLTE2B = 0x04,
  DALPLATFORMINFO_SUBTYPE_SVLTE2C = 0x05,
  DALPLATFORMINFO_SUBTYPE_SGLTE = 0x06,
  DALPLATFORMINFO_NUM_SUBTYPES = 0x07,
  DALPLATFORMINFO_SUBTYPE_32BITS = 0x7FFFFFFF
} DalPlatformInfoPlatformSubtype;
typedef struct
{
  DalPlatformInfoPlatformType platform;
  uint32 version;
  uint32 subtype;
  boolean fusion;
} DalPlatformInfoPlatformInfoType;
typedef struct DalPlatformInfoSMemPMICType
{
  uint32 nPMICModel;
  uint32 nPMICVersion;
} DalPlatformInfoSMemPMICType;
typedef struct
{
  uint32 nFormat;
  uint32 eChipId;
  uint32 nChipVersion;
  char aBuildId[32];
  uint32 nRawChipId;
  uint32 nRawChipVersion;
  DalPlatformInfoPlatformType ePlatformType;
  uint32 nPlatformVersion;
  uint32 bFusion;
  uint32 nPlatformSubtype;
  DalPlatformInfoSMemPMICType aPMICInfo[3];
} DalPlatformInfoSMemType;
typedef struct
{
   uint32 nGpioNumber;
   uint32 nFunctionSelect;
}TLMMGpioIdType;
typedef struct
{
  DalPlatformInfoPlatformInfoType Platform;
  uint32 nKVPS;
  uint32 nKVPSType;
  const char* pszPlatform;
  const char* pszPlatConfigs;
}TLMMPlatformMapType;
typedef struct {
  uint32 nDir;
  uint32 nPull;
  uint32 nDrive;
  uint32 nOutput;
}TLMMGpioIdCfgType;
  <device id="/tlmm/cdp001">
    <props name="blsp_spi_mosi[1]" type="TLMMGpioIdType">{0, 1}</props>
    <props name="blsp_spi_miso[1]" type="TLMMGpioIdType">{1, 1}</props>
    <props name="blsp_spi_cs_n[1]" type="TLMMGpioIdType">{2, 1}</props>
    <props name="blsp_spi_clk[1]" type="TLMMGpioIdType">{3, 1}</props>
    <props name="blsp_uart_tx[8]" type="TLMMGpioIdType">{4, 2}</props>
    <props name="blsp_uart_rx[8]" type="TLMMGpioIdType">{5, 2}</props>
    <props name="blsp_i2c_sda[8]" type="TLMMGpioIdType">{6, 3}</props>
    <props name="blsp_i2c_scl[8]" type="TLMMGpioIdType">{7, 3}</props>
    <props name="lcd0_reset_n" type="TLMMGpioIdType">{8, 0}</props>
    <props name="nfc_irq" type="TLMMGpioIdType">{9, 0}</props>
    <props name="mdp_vsync_p" type="TLMMGpioIdType">{10, 1}</props>
    <props name="mdp_vsync_s" type="TLMMGpioIdType">{11, 1}</props>
    <props name="ethernet_int" type="TLMMGpioIdType">{11, 0}</props>
    <props name="nfc_disable" type="TLMMGpioIdType">{12, 0}</props>
    <props name="cam_mclk0" type="TLMMGpioIdType">{13, 1}</props>
    <props name="cam_mclk1" type="TLMMGpioIdType">{14, 1}</props>
    <props name="cam_mclk2" type="TLMMGpioIdType">{15, 1}</props>
    <props name="webcam2_reset_n" type="TLMMGpioIdType">{16, 0}</props>
    <props name="cci_i2c_sda0" type="TLMMGpioIdType">{17, 1}</props>
    <props name="cci_i2c_scl0" type="TLMMGpioIdType">{18, 1}</props>
    <props name="cci_i2c_sda1" type="TLMMGpioIdType">{19, 1}</props>
    <props name="cci_i2c_scl1" type="TLMMGpioIdType">{20, 1}</props>
    <props name="cci_timer0" type="TLMMGpioIdType">{21, 1}</props>
    <props name="cci_timer1" type="TLMMGpioIdType">{22, 1}</props>
    <props name="webcam1_reset_n" type="TLMMGpioIdType">{23, 0}</props>
    <props name="qdss_cti_trig_in_a" type="TLMMGpioIdType">{24, 5}</props>
    <props name="qdss_cti_trig_out_a" type="TLMMGpioIdType">{25, 8}</props>
    <props name="webcam1_standby" type="TLMMGpioIdType">{26, 0}</props>
    <props name="cam1_standby_n" type="TLMMGpioIdType">{29, 0}</props>
    <props name="cam1_rst_n" type="TLMMGpioIdType">{30, 0}</props>
    <props name="hdmi_cec" type="TLMMGpioIdType">{31, 1}</props>
    <props name="hdmi_ddc_clock" type="TLMMGpioIdType">{32, 1}</props>
    <props name="hdmi_ddc_data" type="TLMMGpioIdType">{33, 1}</props>
    <props name="hdmi_hot_plug_detect" type="TLMMGpioIdType">{34, 1}</props>
    <props name="pci_e0_rst_n" type="TLMMGpioIdType">{35, 1}</props>
    <props name="pci_e0_clkreqn" type="TLMMGpioIdType">{36, 1}</props>
    <props name="pci_e0_wake" type="TLMMGpioIdType">{37, 0}</props>
    <props name="fm_rds_int_n" type="TLMMGpioIdType">{38, 0}</props>
    <props name="fm_rst_n" type="TLMMGpioIdType">{39, 0}</props>
    <props name="sd_write_protect" type="TLMMGpioIdType">{40, 1}</props>
    <props name="blsp_uart_tx[2]" type="TLMMGpioIdType">{41, 2}</props>
    <props name="blsp_uart_rx[2]" type="TLMMGpioIdType">{42, 2}</props>
    <props name="blsp_uart_cts_n[2]" type="TLMMGpioIdType">{43, 2}</props>
    <props name="blsp_uart_rfr_n[2]" type="TLMMGpioIdType">{44, 2}</props>
    <props name="blsp_uart_tx[3]" type="TLMMGpioIdType">{45, 2}</props>
    <props name="blsp_uart_rx[3]" type="TLMMGpioIdType">{46, 2}</props>
    <props name="blsp_uart_cts_n[3]" type="TLMMGpioIdType">{47, 2}</props>
    <props name="blsp_uart_rfr_n[3]" type="TLMMGpioIdType">{48, 2}</props>
    <props name="blsp_uart_tx[9]" type="TLMMGpioIdType">{49, 3}</props>
    <props name="blsp_uart_rx[9]" type="TLMMGpioIdType">{50, 3}</props>
    <props name="gnss_reset_n" type="TLMMGpioIdType">{52, 0}</props>
    <props name="codec_int2" type="TLMMGpioIdType">{53, 0}</props>
    <props name="codec_int1" type="TLMMGpioIdType">{54, 0}</props>
    <props name="blsp_i2c_sda[7]" type="TLMMGpioIdType">{55, 3}</props>
    <props name="blsp_i2c_scl[7]" type="TLMMGpioIdType">{56, 3}</props>
    <props name="forced_usb_boot" type="TLMMGpioIdType">{57, 6}</props>
    <props name="uim4_data" type="TLMMGpioIdType">{58, 2}</props>
    <props name="uim4_clk" type="TLMMGpioIdType">{59, 2}</props>
    <props name="uim4_reset" type="TLMMGpioIdType">{60, 2}</props>
    <props name="uim4_present" type="TLMMGpioIdType">{61, 2}</props>
    <props name="cam2_standby_n" type="TLMMGpioIdType">{62, 0}</props>
    <props name="cam2_rst_n" type="TLMMGpioIdType">{63, 0}</props>
    <props name="codec_rst_n" type="TLMMGpioIdType">{64, 0}</props>
    <props name="pcm_clk" type="TLMMGpioIdType">{65, 1}</props>
    <props name="pcm_sync" type="TLMMGpioIdType">{66, 1}</props>
    <props name="pcm_din" type="TLMMGpioIdType">{67, 1}</props>
    <props name="pcm_dout" type="TLMMGpioIdType">{68, 1}</props>
    <props name="audio_ref_clk" type="TLMMGpioIdType">{69, 2}</props>
    <props name="lpass_slimbus_clk" type="TLMMGpioIdType">{70, 1}</props>
    <props name="lpass_slimbus_data0" type="TLMMGpioIdType">{71, 1}</props>
    <props name="lpass_slimbus_data1" type="TLMMGpioIdType">{72, 1}</props>
    <props name="btfm_slimbus_data" type="TLMMGpioIdType">{73, 1}</props>
    <props name="btfm_slimbus_clk" type="TLMMGpioIdType">{74, 1}</props>
    <props name="ter_mi2s_sck" type="TLMMGpioIdType">{75, 1}</props>
    <props name="ter_mi2s_ws" type="TLMMGpioIdType">{76, 1}</props>
    <props name="ter_mi2s_data0" type="TLMMGpioIdType">{77, 1}</props>
    <props name="fm_status_gpio" type="TLMMGpioIdType">{78, 0}</props>
    <props name="ethernet_rst" type="TLMMGpioIdType">{79, 0}</props>
    <props name="mems_reset_n" type="TLMMGpioIdType">{80, 0}</props>
    <props name="blsp_spi_mosi[5]" type="TLMMGpioIdType">{81, 2}</props>
    <props name="blsp_spi_miso[5]" type="TLMMGpioIdType">{82, 2}</props>
    <props name="blsp_spi_cs_n[5]" type="TLMMGpioIdType">{83, 2}</props>
    <props name="blsp_spi_clk[5]" type="TLMMGpioIdType">{84, 1}</props>
    <props name="blsp_spi_mosi[12]" type="TLMMGpioIdType">{85, 1}</props>
    <props name="ts_aux" type="TLMMGpioIdType">{86, 0}</props>
    <props name="blsp_i2c_sda[12]" type="TLMMGpioIdType">{87, 3}</props>
    <props name="blsp_i2c_scl[12]" type="TLMMGpioIdType">{88, 3}</props>
    <props name="ts_reset_n" type="TLMMGpioIdType">{89, 0}</props>
    <props name="blsp1_spi_cs1_n" type="TLMMGpioIdType">{90, 2}</props>
    <props name="rcm_trigger1" type="TLMMGpioIdType">{91, 0}</props>
    <props name="rcm_trigger2" type="TLMMGpioIdType">{92, 0}</props>
    <props name="rcm_trigger3" type="TLMMGpioIdType">{93, 0}</props>
    <props name="wigg_en" type="TLMMGpioIdType">{94, 0}</props>
    <props name="sd_card_det_n" type="TLMMGpioIdType">{95, 0}</props>
    <props name="lcd1_reset_n" type="TLMMGpioIdType">{96, 0}</props>
    <props name="qdss_cti_trig_out_d" type="TLMMGpioIdType">{100, 4}</props>
    <props name="qdss_cti_trig_in_d" type="TLMMGpioIdType">{101, 3}</props>
    <props name="apq_div_clk1_en" type="TLMMGpioIdType">{102, 0}</props>
    <props name="ap2mdm_chnl_rdy" type="TLMMGpioIdType">{103, 0}</props>
    <props name="mdm2ap_wakeup" type="TLMMGpioIdType">{104, 0}</props>
    <props name="ap2mdm_wakeup" type="TLMMGpioIdType">{105, 0}</props>
    <props name="mdm2ap_status" type="TLMMGpioIdType">{106, 0}</props>
    <props name="ap2mdm_status" type="TLMMGpioIdType">{107, 0}</props>
    <props name="mdm2ap_err_fatal" type="TLMMGpioIdType">{108, 0}</props>
    <props name="ap2mdm_err_fatal" type="TLMMGpioIdType">{109, 0}</props>
    <props name="mdm2ap_chnl_rdy" type="TLMMGpioIdType">{110, 0}</props>
    <props name="ap2mdm_vdd_min" type="TLMMGpioIdType">{111, 0}</props>
    <props name="mdm2ap_vdd_min" type="TLMMGpioIdType">{112, 0}</props>
    <props name="ap2mdm_pon_reset" type="TLMMGpioIdType">{113, 0}</props>
    <props name="pci_e2_rst_n" type="TLMMGpioIdType">{114, 2}</props>
    <props name="pci_e2_clkreq_n" type="TLMMGpioIdType">{115, 255}</props>
    <props name="pci_e2_wake" type="TLMMGpioIdType">{116, 0}</props>
    <props name="ssc_irq_0" type="TLMMGpioIdType">{117, 0}</props>
    <props name="ssc_irq_1" type="TLMMGpioIdType">{118, 0}</props>
    <props name="ssc_irq_2" type="TLMMGpioIdType">{119, 0}</props>
    <props name="ssc_irq_3" type="TLMMGpioIdType">{120, 0}</props>
    <props name="ssc_irq_4" type="TLMMGpioIdType">{121, 0}</props>
    <props name="ssc_irq_5" type="TLMMGpioIdType">{122, 0}</props>
    <props name="ssc_irq_6" type="TLMMGpioIdType">{123, 0}</props>
    <props name="ssc_irq_7" type="TLMMGpioIdType">{124, 0}</props>
    <props name="ssc_irq_8" type="TLMMGpioIdType">{125, 0}</props>
    <props name="pci_e1_rst_n" type="TLMMGpioIdType">{130, 1}</props>
    <props name="pci_e1_clkreqn" type="TLMMGpioIdType">{131, 1}</props>
    <props name="pci_e1_wake" type="TLMMGpioIdType">{132, 0}</props>
  </device>
  <device id="/tlmm/mtp001">
    <props name="blsp_spi_mosi[1]" type="TLMMGpioIdType">{0, 1}</props>
    <props name="blsp_spi_miso[1]" type="TLMMGpioIdType">{1, 1}</props>
    <props name="blsp_spi_clk[1]" type="TLMMGpioIdType">{3, 1}</props>
    <props name="blsp_uart_tx[8]" type="TLMMGpioIdType">{4, 2}</props>
    <props name="blsp_uart_rx[8]" type="TLMMGpioIdType">{5, 2}</props>
    <props name="blsp_i2c_sda[8]" type="TLMMGpioIdType">{6, 3}</props>
    <props name="blsp_i2c_scl[8]" type="TLMMGpioIdType">{7, 3}</props>
    <props name="lcd0_reset_n" type="TLMMGpioIdType">{8, 0}</props>
    <props name="nfc_irq" type="TLMMGpioIdType">{9, 0}</props>
    <props name="mdp_vsync_p" type="TLMMGpioIdType">{10, 1}</props>
    <props name="ethernet_int" type="TLMMGpioIdType">{11, 0}</props>
    <props name="nfc_disable" type="TLMMGpioIdType">{12, 0}</props>
    <props name="cam_mclk0" type="TLMMGpioIdType">{13, 1}</props>
    <props name="cam_mclk1" type="TLMMGpioIdType">{14, 1}</props>
    <props name="cam_mclk2" type="TLMMGpioIdType">{15, 1}</props>
    <props name="webcam2_reset_n" type="TLMMGpioIdType">{16, 0}</props>
    <props name="cci_i2c_sda0" type="TLMMGpioIdType">{17, 1}</props>
    <props name="cci_i2c_scl0" type="TLMMGpioIdType">{18, 1}</props>
    <props name="cci_i2c_sda1" type="TLMMGpioIdType">{19, 1}</props>
    <props name="cci_i2c_scl1" type="TLMMGpioIdType">{20, 1}</props>
    <props name="cci_timer0" type="TLMMGpioIdType">{21, 1}</props>
    <props name="cci_timer1" type="TLMMGpioIdType">{22, 1}</props>
    <props name="webcam1_reset_n" type="TLMMGpioIdType">{23, 0}</props>
    <props name="blsp1_spi_cs2a_n" type="TLMMGpioIdType">{24, 3}</props>
    <props name="webcam1_standby" type="TLMMGpioIdType">{26, 0}</props>
    <props name="blsp_i2c_sda[6]" type="TLMMGpioIdType">{27, 3}</props>
    <props name="blsp_i2c_scl[6]" type="TLMMGpioIdType">{28, 3}</props>
    <props name="cam1_standby_n" type="TLMMGpioIdType">{29, 0}</props>
    <props name="cam1_rst_n" type="TLMMGpioIdType">{30, 0}</props>
    <props name="hdmi_cec" type="TLMMGpioIdType">{31, 1}</props>
    <props name="hdmi_ddc_clock" type="TLMMGpioIdType">{32, 1}</props>
    <props name="hdmi_ddc_data" type="TLMMGpioIdType">{33, 1}</props>
    <props name="hdmi_hot_plug_detect" type="TLMMGpioIdType">{34, 1}</props>
    <props name="pci_e0_rst_n" type="TLMMGpioIdType">{35, 1}</props>
    <props name="pci_e0_clkreqn" type="TLMMGpioIdType">{36, 1}</props>
    <props name="pci_e0_wake" type="TLMMGpioIdType">{37, 0}</props>
    <props name="fm_rds_int_n" type="TLMMGpioIdType">{38, 0}</props>
    <props name="fm_rst_n" type="TLMMGpioIdType">{39, 0}</props>
    <props name="sd_write_protect" type="TLMMGpioIdType">{40, 1}</props>
    <props name="blsp_uart_tx[2]" type="TLMMGpioIdType">{41, 2}</props>
    <props name="blsp_uart_rx[2]" type="TLMMGpioIdType">{42, 2}</props>
    <props name="blsp_uart_cts_n[2]" type="TLMMGpioIdType">{43, 2}</props>
    <props name="blsp_uart_rfr_n[2]" type="TLMMGpioIdType">{44, 2}</props>
    <props name="blsp_uart_tx[3]" type="TLMMGpioIdType">{45, 2}</props>
    <props name="blsp_uart_rx[3]" type="TLMMGpioIdType">{46, 2}</props>
    <props name="blsp_uart_cts_n[3]" type="TLMMGpioIdType">{47, 2}</props>
    <props name="blsp_uart_rfr_n[3]" type="TLMMGpioIdType">{48, 2}</props>
    <props name="uim3_data" type="TLMMGpioIdType">{49, 1}</props>
    <props name="uim3_clk" type="TLMMGpioIdType">{50, 1}</props>
    <props name="uim3_reset" type="TLMMGpioIdType">{51, 1}</props>
    <props name="uim3_present" type="TLMMGpioIdType">{52, 1}</props>
    <props name="codec_int2" type="TLMMGpioIdType">{53, 0}</props>
    <props name="codec_int1" type="TLMMGpioIdType">{54, 0}</props>
    <props name="blsp_i2c_sda[7]" type="TLMMGpioIdType">{55, 3}</props>
    <props name="blsp_i2c_scl[7]" type="TLMMGpioIdType">{56, 3}</props>
    <props name="forced_usb_boot" type="TLMMGpioIdType">{57, 6}</props>
    <props name="uim4_data" type="TLMMGpioIdType">{58, 2}</props>
    <props name="uim4_clk" type="TLMMGpioIdType">{59, 2}</props>
    <props name="uim4_reset" type="TLMMGpioIdType">{60, 2}</props>
    <props name="uim4_present" type="TLMMGpioIdType">{61, 2}</props>
    <props name="cam2_standby_n" type="TLMMGpioIdType">{62, 0}</props>
    <props name="cam2_rst_n" type="TLMMGpioIdType">{63, 0}</props>
    <props name="codec_rst_n" type="TLMMGpioIdType">{64, 0}</props>
    <props name="pcm_clk" type="TLMMGpioIdType">{65, 1}</props>
    <props name="pcm_sync" type="TLMMGpioIdType">{66, 1}</props>
    <props name="pcm_din" type="TLMMGpioIdType">{67, 1}</props>
    <props name="pcm_dout" type="TLMMGpioIdType">{68, 1}</props>
    <props name="audio_ref_clk" type="TLMMGpioIdType">{69, 2}</props>
    <props name="lpass_slimbus_clk" type="TLMMGpioIdType">{70, 1}</props>
    <props name="lpass_slimbus_data0" type="TLMMGpioIdType">{71, 1}</props>
    <props name="lpass_slimbus_data1" type="TLMMGpioIdType">{72, 1}</props>
    <props name="btfm_slimbus_data" type="TLMMGpioIdType">{73, 1}</props>
    <props name="btfm_slimbus_clk" type="TLMMGpioIdType">{74, 1}</props>
    <props name="ter_mi2s_sck" type="TLMMGpioIdType">{75, 1}</props>
    <props name="ter_mi2s_ws" type="TLMMGpioIdType">{76, 1}</props>
    <props name="ter_mi2s_data0" type="TLMMGpioIdType">{77, 1}</props>
    <props name="fm_status_gpio" type="TLMMGpioIdType">{78, 0}</props>
    <props name="ethernet_rst" type="TLMMGpioIdType">{79, 0}</props>
    <props name="mems_reset_n" type="TLMMGpioIdType">{80, 0}</props>
    <props name="blsp_spi_mosi[5]" type="TLMMGpioIdType">{81, 2}</props>
    <props name="blsp_spi_miso[5]" type="TLMMGpioIdType">{82, 2}</props>
    <props name="blsp_spi_cs_n[5]" type="TLMMGpioIdType">{83, 2}</props>
    <props name="blsp_spi_clk[5]" type="TLMMGpioIdType">{84, 1}</props>
    <props name="blsp_spi_mosi[12]" type="TLMMGpioIdType">{85, 1}</props>
    <props name="blsp_i2c_sda[12]" type="TLMMGpioIdType">{87, 3}</props>
    <props name="blsp_i2c_scl[12]" type="TLMMGpioIdType">{88, 3}</props>
    <props name="ts_reset_n" type="TLMMGpioIdType">{89, 0}</props>
    <props name="blsp1_spi_cs1_n" type="TLMMGpioIdType">{90, 2}</props>
    <props name="rcm_trigger1" type="TLMMGpioIdType">{91, 0}</props>
    <props name="rcm_trigger2" type="TLMMGpioIdType">{92, 0}</props>
    <props name="rcm_trigger3" type="TLMMGpioIdType">{93, 0}</props>
    <props name="wigg_en" type="TLMMGpioIdType">{94, 0}</props>
    <props name="sd_card_det_n" type="TLMMGpioIdType">{95, 0}</props>
    <props name="grfc[0]" type="TLMMGpioIdType">{97, 1}</props>
    <props name="grfc[1]" type="TLMMGpioIdType">{98, 1}</props>
    <props name="grfc[2]" type="TLMMGpioIdType">{99, 1}</props>
    <props name="grfc[3]" type="TLMMGpioIdType">{100, 1}</props>
    <props name="grfc[4]" type="TLMMGpioIdType">{101, 1}</props>
    <props name="grfc[5]" type="TLMMGpioIdType">{102, 1}</props>
    <props name="grfc[6]" type="TLMMGpioIdType">{103, 1}</props>
    <props name="grfc[7]" type="TLMMGpioIdType">{104, 1}</props>
    <props name="uim2_data" type="TLMMGpioIdType">{105, 1}</props>
    <props name="uim2_clk" type="TLMMGpioIdType">{106, 1}</props>
    <props name="uim2_reset" type="TLMMGpioIdType">{107, 1}</props>
    <props name="uim2_present" type="TLMMGpioIdType">{108, 1}</props>
    <props name="uim1_data" type="TLMMGpioIdType">{109, 1}</props>
    <props name="uim1_clk" type="TLMMGpioIdType">{110, 1}</props>
    <props name="uim1_reset" type="TLMMGpioIdType">{111, 1}</props>
    <props name="uim1_present" type="TLMMGpioIdType">{112, 1}</props>
    <props name="uim_batt_alarm" type="TLMMGpioIdType">{113, 1}</props>
    <props name="grfc[8]" type="TLMMGpioIdType">{114, 1}</props>
    <props name="grfc[9]" type="TLMMGpioIdType">{115, 1}</props>
    <props name="grfc[10]" type="TLMMGpioIdType">{116, 1}</props>
    <props name="ssc_irq_0" type="TLMMGpioIdType">{117, 0}</props>
    <props name="ssc_irq_1" type="TLMMGpioIdType">{118, 0}</props>
    <props name="ssc_irq_2" type="TLMMGpioIdType">{119, 0}</props>
    <props name="ssc_irq_3" type="TLMMGpioIdType">{120, 0}</props>
    <props name="ssc_irq_4" type="TLMMGpioIdType">{121, 0}</props>
    <props name="ssc_irq_5" type="TLMMGpioIdType">{122, 0}</props>
    <props name="ssc_irq_7" type="TLMMGpioIdType">{124, 0}</props>
    <props name="ssc_irq_8" type="TLMMGpioIdType">{125, 0}</props>
    <props name="grfc[11]" type="TLMMGpioIdType">{127, 1}</props>
    <props name="grfc[12]" type="TLMMGpioIdType">{128, 1}</props>
    <props name="grfc[13]" type="TLMMGpioIdType">{129, 1}</props>
    <props name="pci_e1_rst_n" type="TLMMGpioIdType">{130, 1}</props>
    <props name="pci_e1_clkreqn" type="TLMMGpioIdType">{131, 1}</props>
    <props name="pci_e1_wake" type="TLMMGpioIdType">{132, 0}</props>
    <props name="grfc[14]" type="TLMMGpioIdType">{133, 1}</props>
    <props name="gsm_tx_phase_d0" type="TLMMGpioIdType">{134, 1}</props>
    <props name="gsm_tx_phase_d1" type="TLMMGpioIdType">{135, 1}</props>
    <props name="grfc[15]" type="TLMMGpioIdType">{136, 1}</props>
    <props name="rffe3_data" type="TLMMGpioIdType">{137, 1}</props>
    <props name="rffe3_clk" type="TLMMGpioIdType">{138, 1}</props>
    <props name="rffe4_data" type="TLMMGpioIdType">{139, 1}</props>
    <props name="rffe4_clk" type="TLMMGpioIdType">{140, 1}</props>
    <props name="rffe5_data" type="TLMMGpioIdType">{141, 1}</props>
    <props name="rffe5_clk" type="TLMMGpioIdType">{142, 1}</props>
    <props name="gps_tx_aggressor" type="TLMMGpioIdType">{143, 1}</props>
    <props name="mss_lte_coxm_txd" type="TLMMGpioIdType">{144, 1}</props>
    <props name="mss_lte_coxm_rxd" type="TLMMGpioIdType">{145, 1}</props>
    <props name="rffe2_data" type="TLMMGpioIdType">{146, 1}</props>
    <props name="rffe2_clk" type="TLMMGpioIdType">{147, 1}</props>
    <props name="rffe1_data" type="TLMMGpioIdType">{148, 1}</props>
    <props name="rffe1_clk" type="TLMMGpioIdType">{149, 1}</props>
  </device>
  <device id="/tlmm/fluid1">
    <props name="blsp_spi_mosi[1]" type="TLMMGpioIdType">{0, 1}</props>
    <props name="blsp_spi_miso[1]" type="TLMMGpioIdType">{1, 1}</props>
    <props name="blsp_spi_clk[1]" type="TLMMGpioIdType">{3, 1}</props>
    <props name="blsp_uart_tx[8]" type="TLMMGpioIdType">{4, 2}</props>
    <props name="blsp_uart_rx[8]" type="TLMMGpioIdType">{5, 2}</props>
    <props name="blsp_i2c_sda[8]" type="TLMMGpioIdType">{6, 3}</props>
    <props name="blsp_i2c_scl[8]" type="TLMMGpioIdType">{7, 3}</props>
    <props name="lcd0_reset_n" type="TLMMGpioIdType">{8, 0}</props>
    <props name="nfc_irq" type="TLMMGpioIdType">{9, 0}</props>
    <props name="mdp_vsync_p" type="TLMMGpioIdType">{10, 1}</props>
    <props name="ethernet_int" type="TLMMGpioIdType">{11, 0}</props>
    <props name="nfc_disable" type="TLMMGpioIdType">{12, 0}</props>
    <props name="cam_mclk0" type="TLMMGpioIdType">{13, 1}</props>
    <props name="cam_mclk1" type="TLMMGpioIdType">{14, 1}</props>
    <props name="cam_mclk2" type="TLMMGpioIdType">{15, 1}</props>
    <props name="webcam2_reset_n" type="TLMMGpioIdType">{16, 0}</props>
    <props name="cci_i2c_sda0" type="TLMMGpioIdType">{17, 1}</props>
    <props name="cci_i2c_scl0" type="TLMMGpioIdType">{18, 1}</props>
    <props name="cci_i2c_sda1" type="TLMMGpioIdType">{19, 1}</props>
    <props name="cci_i2c_scl1" type="TLMMGpioIdType">{20, 1}</props>
    <props name="cci_timer0" type="TLMMGpioIdType">{21, 1}</props>
    <props name="cci_timer1" type="TLMMGpioIdType">{22, 1}</props>
    <props name="webcam1_reset_n" type="TLMMGpioIdType">{23, 0}</props>
    <props name="blsp1_spi_cs2a_n" type="TLMMGpioIdType">{24, 3}</props>
    <props name="webcam1_standby" type="TLMMGpioIdType">{26, 0}</props>
    <props name="blsp_i2c_sda[6]" type="TLMMGpioIdType">{27, 3}</props>
    <props name="blsp_i2c_scl[6]" type="TLMMGpioIdType">{28, 3}</props>
    <props name="cam1_standby_n" type="TLMMGpioIdType">{29, 0}</props>
    <props name="cam1_rst_n" type="TLMMGpioIdType">{30, 0}</props>
    <props name="hdmi_cec" type="TLMMGpioIdType">{31, 1}</props>
    <props name="hdmi_ddc_clock" type="TLMMGpioIdType">{32, 1}</props>
    <props name="hdmi_ddc_data" type="TLMMGpioIdType">{33, 1}</props>
    <props name="hdmi_hot_plug_detect" type="TLMMGpioIdType">{34, 1}</props>
    <props name="pci_e0_rst_n" type="TLMMGpioIdType">{35, 1}</props>
    <props name="pci_e0_clkreqn" type="TLMMGpioIdType">{36, 1}</props>
    <props name="pci_e0_wake" type="TLMMGpioIdType">{37, 0}</props>
    <props name="fm_rds_int_n" type="TLMMGpioIdType">{38, 0}</props>
    <props name="fm_rst_n" type="TLMMGpioIdType">{39, 0}</props>
    <props name="sd_write_protect" type="TLMMGpioIdType">{40, 1}</props>
    <props name="blsp_uart_tx[2]" type="TLMMGpioIdType">{41, 2}</props>
    <props name="blsp_uart_rx[2]" type="TLMMGpioIdType">{42, 2}</props>
    <props name="blsp_uart_cts_n[2]" type="TLMMGpioIdType">{43, 2}</props>
    <props name="blsp_uart_rfr_n[2]" type="TLMMGpioIdType">{44, 2}</props>
    <props name="blsp_uart_tx[3]" type="TLMMGpioIdType">{45, 2}</props>
    <props name="blsp_uart_rx[3]" type="TLMMGpioIdType">{46, 2}</props>
    <props name="blsp_uart_cts_n[3]" type="TLMMGpioIdType">{47, 2}</props>
    <props name="blsp_uart_rfr_n[3]" type="TLMMGpioIdType">{48, 2}</props>
    <props name="uim3_data" type="TLMMGpioIdType">{49, 1}</props>
    <props name="uim3_clk" type="TLMMGpioIdType">{50, 1}</props>
    <props name="uim3_reset" type="TLMMGpioIdType">{51, 1}</props>
    <props name="uim3_present" type="TLMMGpioIdType">{52, 1}</props>
    <props name="codec_int2" type="TLMMGpioIdType">{53, 0}</props>
    <props name="codec_int1" type="TLMMGpioIdType">{54, 0}</props>
    <props name="blsp_i2c_sda[7]" type="TLMMGpioIdType">{55, 3}</props>
    <props name="blsp_i2c_scl[7]" type="TLMMGpioIdType">{56, 3}</props>
    <props name="forced_usb_boot" type="TLMMGpioIdType">{57, 6}</props>
    <props name="uim4_data" type="TLMMGpioIdType">{58, 2}</props>
    <props name="uim4_clk" type="TLMMGpioIdType">{59, 2}</props>
    <props name="uim4_reset" type="TLMMGpioIdType">{60, 2}</props>
    <props name="uim4_present" type="TLMMGpioIdType">{61, 2}</props>
    <props name="cam2_standby_n" type="TLMMGpioIdType">{62, 0}</props>
    <props name="cam2_rst_n" type="TLMMGpioIdType">{63, 0}</props>
    <props name="codec_rst_n" type="TLMMGpioIdType">{64, 0}</props>
    <props name="pcm_clk" type="TLMMGpioIdType">{65, 1}</props>
    <props name="pcm_sync" type="TLMMGpioIdType">{66, 1}</props>
    <props name="pcm_din" type="TLMMGpioIdType">{67, 1}</props>
    <props name="pcm_dout" type="TLMMGpioIdType">{68, 1}</props>
    <props name="audio_ref_clk" type="TLMMGpioIdType">{69, 2}</props>
    <props name="lpass_slimbus_clk" type="TLMMGpioIdType">{70, 1}</props>
    <props name="lpass_slimbus_data0" type="TLMMGpioIdType">{71, 1}</props>
    <props name="lpass_slimbus_data1" type="TLMMGpioIdType">{72, 1}</props>
    <props name="btfm_slimbus_data" type="TLMMGpioIdType">{73, 1}</props>
    <props name="btfm_slimbus_clk" type="TLMMGpioIdType">{74, 1}</props>
    <props name="ter_mi2s_sck" type="TLMMGpioIdType">{75, 1}</props>
    <props name="ter_mi2s_ws" type="TLMMGpioIdType">{76, 1}</props>
    <props name="ter_mi2s_data0" type="TLMMGpioIdType">{77, 1}</props>
    <props name="fm_status_gpio" type="TLMMGpioIdType">{78, 0}</props>
    <props name="ethernet_rst" type="TLMMGpioIdType">{79, 0}</props>
    <props name="mems_reset_n" type="TLMMGpioIdType">{80, 0}</props>
    <props name="blsp_spi_mosi[5]" type="TLMMGpioIdType">{81, 2}</props>
    <props name="blsp_spi_miso[5]" type="TLMMGpioIdType">{82, 2}</props>
    <props name="blsp_spi_cs_n[5]" type="TLMMGpioIdType">{83, 2}</props>
    <props name="blsp_spi_clk[5]" type="TLMMGpioIdType">{84, 1}</props>
    <props name="blsp_spi_mosi[12]" type="TLMMGpioIdType">{85, 1}</props>
    <props name="blsp_i2c_sda[12]" type="TLMMGpioIdType">{87, 3}</props>
    <props name="blsp_i2c_scl[12]" type="TLMMGpioIdType">{88, 3}</props>
    <props name="ts_reset_n" type="TLMMGpioIdType">{89, 0}</props>
    <props name="blsp1_spi_cs1_n" type="TLMMGpioIdType">{90, 2}</props>
    <props name="rcm_trigger1" type="TLMMGpioIdType">{91, 0}</props>
    <props name="rcm_trigger2" type="TLMMGpioIdType">{92, 0}</props>
    <props name="rcm_trigger3" type="TLMMGpioIdType">{93, 0}</props>
    <props name="wigg_en" type="TLMMGpioIdType">{94, 0}</props>
    <props name="sd_card_det_n" type="TLMMGpioIdType">{95, 0}</props>
    <props name="grfc[0]" type="TLMMGpioIdType">{97, 1}</props>
    <props name="grfc[1]" type="TLMMGpioIdType">{98, 1}</props>
    <props name="grfc[2]" type="TLMMGpioIdType">{99, 1}</props>
    <props name="grfc[3]" type="TLMMGpioIdType">{100, 1}</props>
    <props name="grfc[4]" type="TLMMGpioIdType">{101, 1}</props>
    <props name="grfc[5]" type="TLMMGpioIdType">{102, 1}</props>
    <props name="grfc[6]" type="TLMMGpioIdType">{103, 1}</props>
    <props name="grfc[7]" type="TLMMGpioIdType">{104, 1}</props>
    <props name="uim2_data" type="TLMMGpioIdType">{105, 1}</props>
    <props name="uim2_clk" type="TLMMGpioIdType">{106, 1}</props>
    <props name="uim2_reset" type="TLMMGpioIdType">{107, 1}</props>
    <props name="uim2_present" type="TLMMGpioIdType">{108, 1}</props>
    <props name="uim1_data" type="TLMMGpioIdType">{109, 1}</props>
    <props name="uim1_clk" type="TLMMGpioIdType">{110, 1}</props>
    <props name="uim1_reset" type="TLMMGpioIdType">{111, 1}</props>
    <props name="uim1_present" type="TLMMGpioIdType">{112, 1}</props>
    <props name="uim_batt_alarm" type="TLMMGpioIdType">{113, 1}</props>
    <props name="grfc[8]" type="TLMMGpioIdType">{114, 1}</props>
    <props name="grfc[9]" type="TLMMGpioIdType">{115, 1}</props>
    <props name="grfc[10]" type="TLMMGpioIdType">{116, 1}</props>
    <props name="ssc_irq_0" type="TLMMGpioIdType">{117, 0}</props>
    <props name="ssc_irq_1" type="TLMMGpioIdType">{118, 0}</props>
    <props name="ssc_irq_2" type="TLMMGpioIdType">{119, 0}</props>
    <props name="ssc_irq_3" type="TLMMGpioIdType">{120, 0}</props>
    <props name="ssc_irq_4" type="TLMMGpioIdType">{121, 0}</props>
    <props name="ssc_irq_5" type="TLMMGpioIdType">{122, 0}</props>
    <props name="ssc_irq_7" type="TLMMGpioIdType">{124, 0}</props>
    <props name="ssc_irq_8" type="TLMMGpioIdType">{125, 0}</props>
    <props name="grfc[11]" type="TLMMGpioIdType">{127, 1}</props>
    <props name="grfc[12]" type="TLMMGpioIdType">{128, 1}</props>
    <props name="grfc[13]" type="TLMMGpioIdType">{129, 1}</props>
    <props name="pci_e1_rst_n" type="TLMMGpioIdType">{130, 1}</props>
    <props name="pci_e1_clkreqn" type="TLMMGpioIdType">{131, 1}</props>
    <props name="pci_e1_wake" type="TLMMGpioIdType">{132, 0}</props>
    <props name="grfc[14]" type="TLMMGpioIdType">{133, 1}</props>
    <props name="gsm_tx_phase_d0" type="TLMMGpioIdType">{134, 1}</props>
    <props name="gsm_tx_phase_d1" type="TLMMGpioIdType">{135, 1}</props>
    <props name="grfc[15]" type="TLMMGpioIdType">{136, 1}</props>
    <props name="rffe3_data" type="TLMMGpioIdType">{137, 1}</props>
    <props name="rffe3_clk" type="TLMMGpioIdType">{138, 1}</props>
    <props name="rffe4_data" type="TLMMGpioIdType">{139, 1}</props>
    <props name="rffe4_clk" type="TLMMGpioIdType">{140, 1}</props>
    <props name="rffe5_data" type="TLMMGpioIdType">{141, 1}</props>
    <props name="rffe5_clk" type="TLMMGpioIdType">{142, 1}</props>
    <props name="gps_tx_aggressor" type="TLMMGpioIdType">{143, 1}</props>
    <props name="mss_lte_coxm_txd" type="TLMMGpioIdType">{144, 1}</props>
    <props name="mss_lte_coxm_rxd" type="TLMMGpioIdType">{145, 1}</props>
    <props name="rffe2_data" type="TLMMGpioIdType">{146, 1}</props>
    <props name="rffe2_clk" type="TLMMGpioIdType">{147, 1}</props>
    <props name="rffe1_data" type="TLMMGpioIdType">{148, 1}</props>
    <props name="rffe1_clk" type="TLMMGpioIdType">{149, 1}</props>
  </device>
  <device id="/tlmm/liquid">
    <props name="blsp_spi_mosi[1]" type="TLMMGpioIdType">{0, 1}</props>
    <props name="blsp_spi_miso[1]" type="TLMMGpioIdType">{1, 1}</props>
    <props name="blsp_spi_cs_n[1]" type="TLMMGpioIdType">{2, 1}</props>
    <props name="blsp_spi_clk[1]" type="TLMMGpioIdType">{3, 1}</props>
    <props name="blsp_uart_tx[8]" type="TLMMGpioIdType">{4, 2}</props>
    <props name="blsp_uart_rx[8]" type="TLMMGpioIdType">{5, 2}</props>
    <props name="blsp_i2c_sda[8]" type="TLMMGpioIdType">{6, 3}</props>
    <props name="blsp_i2c_scl[8]" type="TLMMGpioIdType">{7, 3}</props>
    <props name="lcd0_reset_n" type="TLMMGpioIdType">{8, 0}</props>
    <props name="nfc_irq" type="TLMMGpioIdType">{9, 0}</props>
    <props name="mdp_vsync_p" type="TLMMGpioIdType">{10, 1}</props>
    <props name="epm_int_n" type="TLMMGpioIdType">{11, 0}</props>
    <props name="nfc_disable" type="TLMMGpioIdType">{12, 0}</props>
    <props name="cam_mclk0" type="TLMMGpioIdType">{13, 1}</props>
    <props name="cam_mclk1" type="TLMMGpioIdType">{14, 1}</props>
    <props name="cam_mclk2" type="TLMMGpioIdType">{15, 1}</props>
    <props name="webcam2_reset_n" type="TLMMGpioIdType">{16, 0}</props>
    <props name="cci_i2c_sda0" type="TLMMGpioIdType">{17, 1}</props>
    <props name="cci_i2c_scl0" type="TLMMGpioIdType">{18, 1}</props>
    <props name="cci_i2c_sda1" type="TLMMGpioIdType">{19, 1}</props>
    <props name="cci_i2c_scl1" type="TLMMGpioIdType">{20, 1}</props>
    <props name="cci_timer0" type="TLMMGpioIdType">{21, 1}</props>
    <props name="cci_timer1" type="TLMMGpioIdType">{22, 1}</props>
    <props name="webcam1_reset_n" type="TLMMGpioIdType">{23, 0}</props>
    <props name="hdmi_mux_sel" type="TLMMGpioIdType">{25, 0}</props>
    <props name="webcam1_standby" type="TLMMGpioIdType">{26, 0}</props>
    <props name="blsp_i2c_sda[6]" type="TLMMGpioIdType">{27, 3}</props>
    <props name="blsp_i2c_scl[6]" type="TLMMGpioIdType">{28, 3}</props>
    <props name="cam1_standby_n" type="TLMMGpioIdType">{29, 0}</props>
    <props name="cam1_rst_n" type="TLMMGpioIdType">{30, 0}</props>
    <props name="hdmi_cec" type="TLMMGpioIdType">{31, 1}</props>
    <props name="hdmi_ddc_clock" type="TLMMGpioIdType">{32, 1}</props>
    <props name="hdmi_ddc_data" type="TLMMGpioIdType">{33, 1}</props>
    <props name="hdmi_hot_plug_detect" type="TLMMGpioIdType">{34, 1}</props>
    <props name="pci_e0_rst_n" type="TLMMGpioIdType">{35, 1}</props>
    <props name="pci_e0_clkreqn" type="TLMMGpioIdType">{36, 1}</props>
    <props name="pci_e0_wake" type="TLMMGpioIdType">{37, 0}</props>
    <props name="fm_rds_int_n" type="TLMMGpioIdType">{38, 0}</props>
    <props name="fm_rst_n" type="TLMMGpioIdType">{39, 0}</props>
    <props name="sd_write_protect" type="TLMMGpioIdType">{40, 1}</props>
    <props name="blsp_uart_tx[2]" type="TLMMGpioIdType">{41, 2}</props>
    <props name="blsp_uart_rx[2]" type="TLMMGpioIdType">{42, 2}</props>
    <props name="blsp_uart_cts_n[2]" type="TLMMGpioIdType">{43, 2}</props>
    <props name="blsp_uart_rfr_n[2]" type="TLMMGpioIdType">{44, 2}</props>
    <props name="blsp_uart_tx[3]" type="TLMMGpioIdType">{45, 2}</props>
    <props name="blsp_uart_rx[3]" type="TLMMGpioIdType">{46, 2}</props>
    <props name="blsp_uart_cts_n[3]" type="TLMMGpioIdType">{47, 2}</props>
    <props name="blsp_uart_rfr_n[3]" type="TLMMGpioIdType">{48, 2}</props>
    <props name="uim3_data" type="TLMMGpioIdType">{49, 1}</props>
    <props name="uim3_clk" type="TLMMGpioIdType">{50, 1}</props>
    <props name="uim3_reset" type="TLMMGpioIdType">{51, 1}</props>
    <props name="uim3_present" type="TLMMGpioIdType">{52, 1}</props>
    <props name="codec_int2" type="TLMMGpioIdType">{53, 0}</props>
    <props name="codec_int1" type="TLMMGpioIdType">{54, 0}</props>
    <props name="blsp_i2c_sda[7]" type="TLMMGpioIdType">{55, 3}</props>
    <props name="blsp_i2c_scl[7]" type="TLMMGpioIdType">{56, 3}</props>
    <props name="forced_usb_boot" type="TLMMGpioIdType">{57, 6}</props>
    <props name="uim4_data" type="TLMMGpioIdType">{58, 2}</props>
    <props name="uim4_clk" type="TLMMGpioIdType">{59, 2}</props>
    <props name="uim4_reset" type="TLMMGpioIdType">{60, 2}</props>
    <props name="uim4_present" type="TLMMGpioIdType">{61, 2}</props>
    <props name="cam2_standby_n" type="TLMMGpioIdType">{62, 0}</props>
    <props name="gesture_reset_n" type="TLMMGpioIdType">{63, 0}</props>
    <props name="codec_rst_n" type="TLMMGpioIdType">{64, 0}</props>
    <props name="pcm_clk" type="TLMMGpioIdType">{65, 1}</props>
    <props name="pcm_sync" type="TLMMGpioIdType">{66, 1}</props>
    <props name="pcm_din" type="TLMMGpioIdType">{67, 1}</props>
    <props name="pcm_dout" type="TLMMGpioIdType">{68, 1}</props>
    <props name="audio_ref_clk" type="TLMMGpioIdType">{69, 2}</props>
    <props name="lpass_slimbus_clk" type="TLMMGpioIdType">{70, 1}</props>
    <props name="lpass_slimbus_data0" type="TLMMGpioIdType">{71, 1}</props>
    <props name="lpass_slimbus_data1" type="TLMMGpioIdType">{72, 1}</props>
    <props name="btfm_slimbus_data" type="TLMMGpioIdType">{73, 1}</props>
    <props name="btfm_slimbus_clk" type="TLMMGpioIdType">{74, 1}</props>
    <props name="ter_mi2s_sck" type="TLMMGpioIdType">{75, 1}</props>
    <props name="ter_mi2s_ws" type="TLMMGpioIdType">{76, 1}</props>
    <props name="ter_mi2s_data0" type="TLMMGpioIdType">{77, 1}</props>
    <props name="gp_pdm_2a" type="TLMMGpioIdType">{79, 0}</props>
    <props name="mems_reset_n" type="TLMMGpioIdType">{80, 0}</props>
    <props name="blsp_spi_mosi[5]" type="TLMMGpioIdType">{81, 2}</props>
    <props name="blsp_spi_miso[5]" type="TLMMGpioIdType">{82, 2}</props>
    <props name="blsp_spi_cs_n[5]" type="TLMMGpioIdType">{83, 2}</props>
    <props name="blsp_spi_clk[5]" type="TLMMGpioIdType">{84, 1}</props>
    <props name="blsp_spi_mosi[12]" type="TLMMGpioIdType">{85, 1}</props>
    <props name="ts_aux" type="TLMMGpioIdType">{86, 0}</props>
    <props name="blsp_i2c_sda[12]" type="TLMMGpioIdType">{87, 3}</props>
    <props name="blsp_i2c_scl[12]" type="TLMMGpioIdType">{88, 3}</props>
    <props name="ts_reset_n" type="TLMMGpioIdType">{89, 0}</props>
    <props name="blsp1_spi_cs1_n" type="TLMMGpioIdType">{90, 2}</props>
    <props name="epm_marker11" type="TLMMGpioIdType">{91, 0}</props>
    <props name="epm_marker2" type="TLMMGpioIdType">{92, 0}</props>
    <props name="epm_global_en" type="TLMMGpioIdType">{93, 0}</props>
    <props name="wigg_en" type="TLMMGpioIdType">{94, 0}</props>
    <props name="sd_card_det_n" type="TLMMGpioIdType">{95, 0}</props>
    <props name="hdmi_mux_en" type="TLMMGpioIdType">{96, 0}</props>
    <props name="grfc[0]" type="TLMMGpioIdType">{97, 1}</props>
    <props name="grfc[1]" type="TLMMGpioIdType">{98, 1}</props>
    <props name="grfc[2]" type="TLMMGpioIdType">{99, 1}</props>
    <props name="grfc[3]" type="TLMMGpioIdType">{100, 1}</props>
    <props name="grfc[4]" type="TLMMGpioIdType">{101, 1}</props>
    <props name="grfc[5]" type="TLMMGpioIdType">{102, 1}</props>
    <props name="grfc[6]" type="TLMMGpioIdType">{103, 1}</props>
    <props name="grfc[7]" type="TLMMGpioIdType">{104, 1}</props>
    <props name="uim2_data" type="TLMMGpioIdType">{105, 1}</props>
    <props name="uim2_clk" type="TLMMGpioIdType">{106, 1}</props>
    <props name="uim2_reset" type="TLMMGpioIdType">{107, 1}</props>
    <props name="uim2_present" type="TLMMGpioIdType">{108, 1}</props>
    <props name="uim1_data" type="TLMMGpioIdType">{109, 1}</props>
    <props name="uim1_clk" type="TLMMGpioIdType">{110, 1}</props>
    <props name="uim1_reset" type="TLMMGpioIdType">{111, 1}</props>
    <props name="uim1_present" type="TLMMGpioIdType">{112, 1}</props>
    <props name="uim_batt_alarm" type="TLMMGpioIdType">{113, 1}</props>
    <props name="grfc[8]" type="TLMMGpioIdType">{114, 1}</props>
    <props name="grfc[9]" type="TLMMGpioIdType">{115, 1}</props>
    <props name="grfc[10]" type="TLMMGpioIdType">{116, 1}</props>
    <props name="ssc_irq_0" type="TLMMGpioIdType">{117, 0}</props>
    <props name="ssc_irq_1" type="TLMMGpioIdType">{118, 0}</props>
    <props name="ssc_irq_2" type="TLMMGpioIdType">{119, 0}</props>
    <props name="ssc_irq_3" type="TLMMGpioIdType">{120, 0}</props>
    <props name="ssc_irq_4" type="TLMMGpioIdType">{121, 0}</props>
    <props name="ssc_irq_5" type="TLMMGpioIdType">{122, 0}</props>
    <props name="ssc_irq_6" type="TLMMGpioIdType">{123, 0}</props>
    <props name="ssc_irq_7" type="TLMMGpioIdType">{124, 0}</props>
    <props name="ssc_irq_8" type="TLMMGpioIdType">{125, 0}</props>
    <props name="grfc[11]" type="TLMMGpioIdType">{127, 1}</props>
    <props name="grfc[12]" type="TLMMGpioIdType">{128, 1}</props>
    <props name="grfc[13]" type="TLMMGpioIdType">{129, 1}</props>
    <props name="pci_e1_rst_n" type="TLMMGpioIdType">{130, 1}</props>
    <props name="pci_e1_clkreqn" type="TLMMGpioIdType">{131, 1}</props>
    <props name="pci_e1_wake" type="TLMMGpioIdType">{132, 0}</props>
    <props name="grfc[14]" type="TLMMGpioIdType">{133, 1}</props>
    <props name="gsm_tx_phase_d0" type="TLMMGpioIdType">{134, 1}</props>
    <props name="gsm_tx_phase_d1" type="TLMMGpioIdType">{135, 1}</props>
    <props name="grfc[15]" type="TLMMGpioIdType">{136, 1}</props>
    <props name="rffe3_data" type="TLMMGpioIdType">{137, 1}</props>
    <props name="rffe3_clk" type="TLMMGpioIdType">{138, 1}</props>
    <props name="rffe4_data" type="TLMMGpioIdType">{139, 1}</props>
    <props name="rffe4_clk" type="TLMMGpioIdType">{140, 1}</props>
    <props name="rffe5_data" type="TLMMGpioIdType">{141, 1}</props>
    <props name="rffe5_clk" type="TLMMGpioIdType">{142, 1}</props>
    <props name="gps_tx_aggressor" type="TLMMGpioIdType">{143, 1}</props>
    <props name="mss_lte_coxm_txd" type="TLMMGpioIdType">{144, 1}</props>
    <props name="mss_lte_coxm_rxd" type="TLMMGpioIdType">{145, 1}</props>
    <props name="rffe2_data" type="TLMMGpioIdType">{146, 1}</props>
    <props name="rffe2_clk" type="TLMMGpioIdType">{147, 1}</props>
    <props name="rffe1_data" type="TLMMGpioIdType">{148, 1}</props>
    <props name="rffe1_clk" type="TLMMGpioIdType">{149, 1}</props>
  </device>
  <device id="/tlmm/fusion">
    <props name="blsp_spi_mosi[1]" type="TLMMGpioIdType">{0, 1}</props>
    <props name="blsp_spi_miso[1]" type="TLMMGpioIdType">{1, 1}</props>
    <props name="blsp_spi_cs_n[1]" type="TLMMGpioIdType">{2, 1}</props>
    <props name="blsp_spi_clk[1]" type="TLMMGpioIdType">{3, 1}</props>
    <props name="blsp_uart_tx[8]" type="TLMMGpioIdType">{4, 2}</props>
    <props name="blsp_uart_rx[8]" type="TLMMGpioIdType">{5, 2}</props>
    <props name="blsp_i2c_sda[8]" type="TLMMGpioIdType">{6, 3}</props>
    <props name="blsp_i2c_scl[8]" type="TLMMGpioIdType">{7, 3}</props>
    <props name="lcd0_reset_n" type="TLMMGpioIdType">{8, 0}</props>
    <props name="nfc_irq" type="TLMMGpioIdType">{9, 0}</props>
    <props name="mdp_vsync_p" type="TLMMGpioIdType">{10, 1}</props>
    <props name="mdp_vsync_s" type="TLMMGpioIdType">{11, 1}</props>
    <props name="ethernet_int" type="TLMMGpioIdType">{11, 0}</props>
    <props name="nfc_disable" type="TLMMGpioIdType">{12, 0}</props>
    <props name="cam_mclk0" type="TLMMGpioIdType">{13, 1}</props>
    <props name="cam_mclk1" type="TLMMGpioIdType">{14, 1}</props>
    <props name="cam_mclk2" type="TLMMGpioIdType">{15, 1}</props>
    <props name="webcam2_reset_n" type="TLMMGpioIdType">{16, 0}</props>
    <props name="cci_i2c_sda0" type="TLMMGpioIdType">{17, 1}</props>
    <props name="cci_i2c_scl0" type="TLMMGpioIdType">{18, 1}</props>
    <props name="cci_i2c_sda1" type="TLMMGpioIdType">{19, 1}</props>
    <props name="cci_i2c_scl1" type="TLMMGpioIdType">{20, 1}</props>
    <props name="cci_timer0" type="TLMMGpioIdType">{21, 1}</props>
    <props name="cci_timer1" type="TLMMGpioIdType">{22, 1}</props>
    <props name="webcam1_reset_n" type="TLMMGpioIdType">{23, 0}</props>
    <props name="qdss_cti_trig_in_a" type="TLMMGpioIdType">{24, 5}</props>
    <props name="qdss_cti_trig_out_a" type="TLMMGpioIdType">{25, 8}</props>
    <props name="webcam1_standby" type="TLMMGpioIdType">{26, 0}</props>
    <props name="cam1_standby_n" type="TLMMGpioIdType">{29, 0}</props>
    <props name="cam1_rst_n" type="TLMMGpioIdType">{30, 0}</props>
    <props name="hdmi_cec" type="TLMMGpioIdType">{31, 1}</props>
    <props name="hdmi_ddc_clk" type="TLMMGpioIdType">{32, 255}</props>
    <props name="hdmi_ddc_data" type="TLMMGpioIdType">{33, 1}</props>
    <props name="hdmi_hot_plug_detect" type="TLMMGpioIdType">{34, 1}</props>
    <props name="pci_e0_rst_n" type="TLMMGpioIdType">{35, 1}</props>
    <props name="pci_e0_clkreq_n" type="TLMMGpioIdType">{36, 255}</props>
    <props name="pci_e0_wake" type="TLMMGpioIdType">{37, 0}</props>
    <props name="fm_rds_int_n" type="TLMMGpioIdType">{38, 0}</props>
    <props name="fm_rst_n" type="TLMMGpioIdType">{39, 0}</props>
    <props name="sd_write_protect" type="TLMMGpioIdType">{40, 1}</props>
    <props name="blsp_uart_tx[2]" type="TLMMGpioIdType">{41, 2}</props>
    <props name="blsp_uart_rx[2]" type="TLMMGpioIdType">{42, 2}</props>
    <props name="blsp_uart_cts_n[2]" type="TLMMGpioIdType">{43, 2}</props>
    <props name="blsp_uart_rfr_n[2]" type="TLMMGpioIdType">{44, 2}</props>
    <props name="blsp_uart_tx[3]" type="TLMMGpioIdType">{45, 2}</props>
    <props name="blsp_uart_rx[3]" type="TLMMGpioIdType">{46, 2}</props>
    <props name="blsp_uart_cts_n[3]" type="TLMMGpioIdType">{47, 2}</props>
    <props name="blsp_uart_rfr_n[3]" type="TLMMGpioIdType">{48, 2}</props>
    <props name="blsp_uart_tx[9]" type="TLMMGpioIdType">{49, 3}</props>
    <props name="blsp_uart_rx[9]" type="TLMMGpioIdType">{50, 3}</props>
    <props name="gnss_reset_n" type="TLMMGpioIdType">{52, 0}</props>
    <props name="codec_int2" type="TLMMGpioIdType">{53, 0}</props>
    <props name="codec_int1" type="TLMMGpioIdType">{54, 0}</props>
    <props name="blsp_i2c_sda[7]" type="TLMMGpioIdType">{55, 3}</props>
    <props name="blsp_i2c_scl[7]" type="TLMMGpioIdType">{56, 3}</props>
    <props name="forced_usb_boot" type="TLMMGpioIdType">{57, 6}</props>
    <props name="uim4_data" type="TLMMGpioIdType">{58, 2}</props>
    <props name="uim4_clk" type="TLMMGpioIdType">{59, 2}</props>
    <props name="uim4_reset" type="TLMMGpioIdType">{60, 2}</props>
    <props name="uim4_present" type="TLMMGpioIdType">{61, 2}</props>
    <props name="cam2_standby_n" type="TLMMGpioIdType">{62, 0}</props>
    <props name="cam2_rst_n" type="TLMMGpioIdType">{63, 0}</props>
    <props name="codec_rst_n" type="TLMMGpioIdType">{64, 0}</props>
    <props name="pcm_clk" type="TLMMGpioIdType">{65, 1}</props>
    <props name="pcm_sync" type="TLMMGpioIdType">{66, 1}</props>
    <props name="pcm_din" type="TLMMGpioIdType">{67, 1}</props>
    <props name="pcm_dout" type="TLMMGpioIdType">{68, 1}</props>
    <props name="audio_ref_clk" type="TLMMGpioIdType">{69, 2}</props>
    <props name="slimbus_clk" type="TLMMGpioIdType">{70, 255}</props>
    <props name="slimbus_data0" type="TLMMGpioIdType">{71, 255}</props>
    <props name="slimbus_data1" type="TLMMGpioIdType">{72, 255}</props>
    <props name="btfm_slimbus_data" type="TLMMGpioIdType">{73, 1}</props>
    <props name="btfm_slimbus_clk" type="TLMMGpioIdType">{74, 1}</props>
    <props name="ter_mi2s_sck" type="TLMMGpioIdType">{75, 1}</props>
    <props name="ter_mi2s_ws" type="TLMMGpioIdType">{76, 1}</props>
    <props name="ter_mi2s_data0" type="TLMMGpioIdType">{77, 1}</props>
    <props name="fm_status_gpio" type="TLMMGpioIdType">{78, 0}</props>
    <props name="ethernet_rst" type="TLMMGpioIdType">{79, 0}</props>
    <props name="wlan_en" type="TLMMGpioIdType">{80, 0}</props>
    <props name="blsp_spi_mosi[5]" type="TLMMGpioIdType">{81, 2}</props>
    <props name="blsp_spi_miso[5]" type="TLMMGpioIdType">{82, 2}</props>
    <props name="blsp_spi_cs_n[5]" type="TLMMGpioIdType">{83, 2}</props>
    <props name="blsp_spi_clk[5]" type="TLMMGpioIdType">{84, 1}</props>
    <props name="blsp_spi_mosi[12]" type="TLMMGpioIdType">{85, 1}</props>
    <props name="ts_aux" type="TLMMGpioIdType">{86, 0}</props>
    <props name="blsp_i2c_sda[12]" type="TLMMGpioIdType">{87, 3}</props>
    <props name="blsp_i2c_scl[12]" type="TLMMGpioIdType">{88, 3}</props>
    <props name="ts_reset_n" type="TLMMGpioIdType">{89, 0}</props>
    <props name="blsp1_spi_cs1_n" type="TLMMGpioIdType">{90, 2}</props>
    <props name="rcm_trigger1" type="TLMMGpioIdType">{91, 0}</props>
    <props name="rcm_trigger2" type="TLMMGpioIdType">{92, 0}</props>
    <props name="rcm_trigger3" type="TLMMGpioIdType">{93, 0}</props>
    <props name="wigg_en" type="TLMMGpioIdType">{94, 0}</props>
    <props name="sd_card_det_n" type="TLMMGpioIdType">{95, 0}</props>
    <props name="lcd1_reset_n" type="TLMMGpioIdType">{96, 0}</props>
    <props name="qdss_cti_trig_out_d" type="TLMMGpioIdType">{100, 4}</props>
    <props name="qdss_cti_trig_in_d" type="TLMMGpioIdType">{101, 3}</props>
    <props name="apq_div_clk1_en" type="TLMMGpioIdType">{102, 0}</props>
    <props name="ap2mdm_chnl_rdy" type="TLMMGpioIdType">{103, 0}</props>
    <props name="mdm2ap_wakeup" type="TLMMGpioIdType">{104, 0}</props>
    <props name="ap2mdm_wakeup" type="TLMMGpioIdType">{105, 0}</props>
    <props name="mdm2ap_status" type="TLMMGpioIdType">{106, 0}</props>
    <props name="ap2mdm_status" type="TLMMGpioIdType">{107, 0}</props>
    <props name="mdm2ap_err_fatal" type="TLMMGpioIdType">{108, 0}</props>
    <props name="ap2mdm_err_fatal" type="TLMMGpioIdType">{109, 0}</props>
    <props name="mdm2ap_chnl_rdy" type="TLMMGpioIdType">{110, 0}</props>
    <props name="ap2mdm_vdd_min" type="TLMMGpioIdType">{111, 0}</props>
    <props name="mdm2ap_vdd_min" type="TLMMGpioIdType">{112, 0}</props>
    <props name="ap2mdm_pon_reset" type="TLMMGpioIdType">{113, 0}</props>
    <props name="pci_e2_rst_n" type="TLMMGpioIdType">{114, 2}</props>
    <props name="pci_e2_clkreq_n" type="TLMMGpioIdType">{115, 255}</props>
    <props name="pci_e2_wake" type="TLMMGpioIdType">{116, 0}</props>
    <props name="ssc_irq_0" type="TLMMGpioIdType">{117, 0}</props>
    <props name="ssc_irq_1" type="TLMMGpioIdType">{118, 0}</props>
    <props name="ssc_ir1_2" type="TLMMGpioIdType">{119, 0}</props>
    <props name="ssc_irq_3" type="TLMMGpioIdType">{120, 0}</props>
    <props name="ssc_irq_4" type="TLMMGpioIdType">{121, 0}</props>
    <props name="ssc_irq_5" type="TLMMGpioIdType">{122, 0}</props>
    <props name="ssc_irq_6" type="TLMMGpioIdType">{123, 0}</props>
    <props name="ssc_irq_7" type="TLMMGpioIdType">{124, 0}</props>
    <props name="ssc_irq_8" type="TLMMGpioIdType">{125, 0}</props>
    <props name="mems_reset_n" type="TLMMGpioIdType">{126, 0}</props>
    <props name="pci_e1_rst_n" type="TLMMGpioIdType">{130, 1}</props>
    <props name="pci_e1_clkreq_n" type="TLMMGpioIdType">{131, 255}</props>
    <props name="pci_e1_wake" type="TLMMGpioIdType">{132, 0}</props>
  </device>
  <device id="/tlmm/dragonboard">
    <props name="blsp_uart_tx[8]" type="TLMMGpioIdType">{4, 2}</props>
    <props name="blsp_uart_rx[8]" type="TLMMGpioIdType">{5, 2}</props>
    <props name="blsp_i2c_sda[8]" type="TLMMGpioIdType">{6, 3}</props>
    <props name="blsp_i2c_scl[8]" type="TLMMGpioIdType">{7, 3}</props>
    <props name="lcd0_reset_n" type="TLMMGpioIdType">{8, 0}</props>
    <props name="disp_gpio3" type="TLMMGpioIdType">{9, 255}</props>
    <props name="mdp_vsync_p" type="TLMMGpioIdType">{10, 1}</props>
    <props name="mdp_vsync_s" type="TLMMGpioIdType">{11, 1}</props>
    <props name="cam_mclk0" type="TLMMGpioIdType">{13, 1}</props>
    <props name="cam_mclk1" type="TLMMGpioIdType">{14, 1}</props>
    <props name="cam_mclk2" type="TLMMGpioIdType">{15, 1}</props>
    <props name="cam_mclk3" type="TLMMGpioIdType">{16, 1}</props>
    <props name="cci_i2c_sda0" type="TLMMGpioIdType">{17, 1}</props>
    <props name="cci_i2c_scl0" type="TLMMGpioIdType">{18, 1}</props>
    <props name="cci_i2c_sda1" type="TLMMGpioIdType">{19, 1}</props>
    <props name="cci_i2c_scl1" type="TLMMGpioIdType">{20, 1}</props>
    <props name="cci_timer0" type="TLMMGpioIdType">{21, 1}</props>
    <props name="cci_timer1" type="TLMMGpioIdType">{22, 1}</props>
    <props name="cam2_rst_n" type="TLMMGpioIdType">{23, 0}</props>
    <props name="cam_irq" type="TLMMGpioIdType">{24, 255}</props>
    <props name="blsp_i2c_sda[6]" type="TLMMGpioIdType">{27, 3}</props>
    <props name="blsp_i2c_scl[6]" type="TLMMGpioIdType">{28, 3}</props>
    <props name="cam0_standby_n" type="TLMMGpioIdType">{29, 0}</props>
    <props name="cam0_rst_n" type="TLMMGpioIdType">{30, 0}</props>
    <props name="hdmi_cec" type="TLMMGpioIdType">{31, 1}</props>
    <props name="hdmi_ddc_clock" type="TLMMGpioIdType">{32, 1}</props>
    <props name="hdmi_ddc_data" type="TLMMGpioIdType">{33, 1}</props>
    <props name="hdmi_hot_plug_detect" type="TLMMGpioIdType">{34, 1}</props>
    <props name="pci_e0_rst_n" type="TLMMGpioIdType">{35, 1}</props>
    <props name="pci_e0_clkreqn" type="TLMMGpioIdType">{36, 1}</props>
    <props name="pci_e0_wake" type="TLMMGpioIdType">{37, 0}</props>
    <props name="sd_card_det_n" type="TLMMGpioIdType">{38, 0}</props>
    <props name="edu_gp" type="TLMMGpioIdType">{39, 255}</props>
    <props name="edu_irq" type="TLMMGpioIdType">{40, 255}</props>
    <props name="blsp_uart_tx[2]" type="TLMMGpioIdType">{41, 2}</props>
    <props name="blsp_uart_rx[2]" type="TLMMGpioIdType">{42, 2}</props>
    <props name="blsp_uart_cts_n[2]" type="TLMMGpioIdType">{43, 2}</props>
    <props name="blsp_uart_rfr_n[2]" type="TLMMGpioIdType">{44, 2}</props>
    <props name="blsp_spi_mosi[3]" type="TLMMGpioIdType">{45, 1}</props>
    <props name="blsp_spi_miso[3]" type="TLMMGpioIdType">{46, 1}</props>
    <props name="blsp_spi_cs_n[3]" type="TLMMGpioIdType">{47, 1}</props>
    <props name="blsp_spi_clk[3]" type="TLMMGpioIdType">{48, 1}</props>
    <props name="blsp_spi_mosi[1]" type="TLMMGpioIdType">{49, 255}</props>
    <props name="blsp_spi_miso[1]" type="TLMMGpioIdType">{50, 255}</props>
    <props name="blsp_spi_cs_n[1]" type="TLMMGpioIdType">{51, 255}</props>
    <props name="blsp_spi_clk[1]" type="TLMMGpioIdType">{52, 255}</props>
    <props name="codec_int2" type="TLMMGpioIdType">{53, 0}</props>
    <props name="codec_int1" type="TLMMGpioIdType">{54, 0}</props>
    <props name="qua_mi2s_mclk" type="TLMMGpioIdType">{57, 1}</props>
    <props name="qua_mi2s_sck" type="TLMMGpioIdType">{58, 1}</props>
    <props name="qua_mi2s_ws" type="TLMMGpioIdType">{59, 1}</props>
    <props name="qua_mi2s_data0" type="TLMMGpioIdType">{60, 1}</props>
    <props name="qua_mi2s_data1" type="TLMMGpioIdType">{61, 1}</props>
    <props name="qua_mi2s_data2" type="TLMMGpioIdType">{62, 1}</props>
    <props name="qua_mi2s_data3" type="TLMMGpioIdType">{63, 1}</props>
    <props name="codec_rst_n" type="TLMMGpioIdType">{64, 0}</props>
    <props name="pcm_clk" type="TLMMGpioIdType">{65, 0}</props>
    <props name="pcm_sync" type="TLMMGpioIdType">{66, 0}</props>
    <props name="pcm_din" type="TLMMGpioIdType">{67, 0}</props>
    <props name="pcm_dout" type="TLMMGpioIdType">{68, 0}</props>
    <props name="audio_ref_clk" type="TLMMGpioIdType">{69, 2}</props>
    <props name="lpass_slimbus_clk" type="TLMMGpioIdType">{70, 1}</props>
    <props name="lpass_slimbus_data0" type="TLMMGpioIdType">{71, 1}</props>
    <props name="lpass_slimbus_data1" type="TLMMGpioIdType">{72, 1}</props>
    <props name="ts1_reset_n" type="TLMMGpioIdType">{79, 0}</props>
    <props name="mems_reset_n" type="TLMMGpioIdType">{80, 0}</props>
    <props name="disp_gpio2" type="TLMMGpioIdType">{81, 255}</props>
    <props name="blsp_spi_mosi[12]" type="TLMMGpioIdType">{85, 1}</props>
    <props name="blsp_spi_miso[12]" type="TLMMGpioIdType">{86, 1}</props>
    <props name="blsp_i2c_sda[12]" type="TLMMGpioIdType">{87, 3}</props>
    <props name="blsp_i2c_scl[12]" type="TLMMGpioIdType">{88, 3}</props>
    <props name="ts0_reset_n" type="TLMMGpioIdType">{89, 0}</props>
    <props name="cam1_standby_n" type="TLMMGpioIdType">{98, 0}</props>
    <props name="lcd1_reset_n" type="TLMMGpioIdType">{101, 0}</props>
    <props name="pcie_slt_prsnt_n" type="TLMMGpioIdType">{102, 255}</props>
    <props name="usb_hub_reset_n" type="TLMMGpioIdType">{103, 255}</props>
    <props name="cam1_rst_n" type="TLMMGpioIdType">{104, 0}</props>
    <props name="nfc_disable" type="TLMMGpioIdType">{105, 0}</props>
    <props name="nfc_irq" type="TLMMGpioIdType">{106, 0}</props>
    <props name="disp_gpio1" type="TLMMGpioIdType">{107, 255}</props>
    <props name="ts_int_1_n" type="TLMMGpioIdType">{108, 255}</props>
    <props name="uim1_data" type="TLMMGpioIdType">{109, 1}</props>
    <props name="uim1_clk" type="TLMMGpioIdType">{110, 1}</props>
    <props name="uim1_reset" type="TLMMGpioIdType">{111, 1}</props>
    <props name="uim1_present" type="TLMMGpioIdType">{112, 1}</props>
    <props name="pci_e2_rst_n" type="TLMMGpioIdType">{114, 2}</props>
    <props name="pci_e2_clkreq_n" type="TLMMGpioIdType">{115, 255}</props>
    <props name="pci_e2_wake" type="TLMMGpioIdType">{116, 0}</props>
    <props name="ssc_irq_0" type="TLMMGpioIdType">{117, 255}</props>
    <props name="ssc_irq_1" type="TLMMGpioIdType">{118, 255}</props>
    <props name="ssc_irq_2" type="TLMMGpioIdType">{119, 255}</props>
    <props name="ssc_irq_3" type="TLMMGpioIdType">{120, 255}</props>
    <props name="ssc_irq_4" type="TLMMGpioIdType">{121, 255}</props>
    <props name="ssc_irq_5" type="TLMMGpioIdType">{122, 255}</props>
    <props name="ssc_irq_6" type="TLMMGpioIdType">{123, 255}</props>
    <props name="ssc_irq_7" type="TLMMGpioIdType">{124, 255}</props>
    <props name="ssc_irq_8" type="TLMMGpioIdType">{125, 255}</props>
    <props name="edu_gpio1" type="TLMMGpioIdType">{127, 255}</props>
    <props name="pci_e1_rst_n" type="TLMMGpioIdType">{130, 1}</props>
    <props name="pci_e1_clkreqn" type="TLMMGpioIdType">{131, 1}</props>
    <props name="pci_e1_wake" type="TLMMGpioIdType">{132, 0}</props>
    <props name="cam2_standby_n" type="TLMMGpioIdType">{133, 0}</props>
    <props name="edu_gpio3" type="TLMMGpioIdType">{134, 255}</props>
    <props name="bl0_en" type="TLMMGpioIdType">{135, 0}</props>
    <props name="qspi_cs_n_1" type="TLMMGpioIdType">{138, 2}</props>
    <props name="ssbi2" type="TLMMGpioIdType">{139, 2}</props>
    <props name="ssbi1" type="TLMMGpioIdType">{140, 2}</props>
    <props name="qspi_cs_n_0" type="TLMMGpioIdType">{141, 2}</props>
    <props name="qspi_reset_n" type="TLMMGpioIdType">{142, 0}</props>
    <props name="qspi_clk" type="TLMMGpioIdType">{145, 2}</props>
    <props name="qspi_data[0]" type="TLMMGpioIdType">{146, 2}</props>
    <props name="qspi_data[1]" type="TLMMGpioIdType">{147, 2}</props>
    <props name="qspi_data[2]" type="TLMMGpioIdType">{148, 2}</props>
    <props name="qspi_data[3]" type="TLMMGpioIdType">{149, 2}</props>
  </device>
 <global_def>
     <var_seq name="tlmm_port_cfg" type=0x00000003>
      0xFFFFFFFF, 0, 0, 0, 0,
      end
     </var_seq>
   </global_def>
   <device id=0x02000020>
     <props name="tlmm_base" type=0x00000002>
      0x01000000
     </props>
     <props name="tlmm_offset" type=0x00000002>
      0x00010000
     </props>
     <props name="tlmm_total_gpio" type=0x00000002>
      150
     </props>
     <props name="tlmm_ports" type=0x00000014>
      tlmm_port_cfg
     </props>
     <props name="IsRemotable" type=0x00000002>
      0x1
     </props>
    <props name="TLMMDefaultPlatformGroup" type="TLMMPlatformMapType">
      {{DALPLATFORMINFO_TYPE_MTP_MSM, 1, 0, 0}, 0, 0, "/tlmm/mtp001", "/tlmm/mtpcfgs"}
    </props>
    <props name="TlmmPlatformGroups" type="TLMMPlatformMapType" array="True">
      {{{DALPLATFORMINFO_TYPE_LIQUID, 1, 0, 0}, 0, 0, "/tlmm/liquid", "/tlmm/liquidcfgs"},
       {{DALPLATFORMINFO_TYPE_CDP, 1, 0, 0}, 0, 0, "/tlmm/cdp001", "/tlmm/cdpcfgs"},
       {{DALPLATFORMINFO_TYPE_MTP_MSM, 1, 0, 0}, 0, 0, "/tlmm/mtp001", "/tlmm/mtpcfgs"},
       {{DALPLATFORMINFO_TYPE_FLUID, 1, 0, 0}, 0, 0, "/tlmm/fluid1", "/tlmm/fluid1cfgs"},
       {{DALPLATFORMINFO_TYPE_MTP_MSM, 1, 0, 1}, 0, 0, "/tlmm/fusion", "/tlmm/fusioncfgs"},
       {{DALPLATFORMINFO_TYPE_DRAGONBOARD, 1, 0, 0}, 0, 0, "/tlmm/dragonboard", "/tlmm/dbcgs"}}
    </props>
   </device>
</driver>

  <device id="/tlmm/mtp001">
    <props name="blsp_spi_mosi[1]" type="TLMMGpioIdType">{0, 1}</props>
    <props name="blsp_spi_miso[1]" type="TLMMGpioIdType">{1, 1}</props>
    <props name="blsp_spi_clk[1]" type="TLMMGpioIdType">{3, 1}</props>
    <props name="blsp_uart_tx[8]" type="TLMMGpioIdType">{4, 2}</props>
    <props name="blsp_uart_rx[8]" type="TLMMGpioIdType">{5, 2}</props>
    <props name="blsp_i2c_sda[8]" type="TLMMGpioIdType">{6, 3}</props>
    <props name="blsp_i2c_scl[8]" type="TLMMGpioIdType">{7, 3}</props>
    <props name="lcd0_reset_n" type="TLMMGpioIdType">{8, 0}</props>
    <props name="nfc_irq" type="TLMMGpioIdType">{9, 0}</props>
    <props name="mdp_vsync_p" type="TLMMGpioIdType">{10, 1}</props>
    <props name="ethernet_int" type="TLMMGpioIdType">{11, 0}</props>
    <props name="nfc_disable" type="TLMMGpioIdType">{12, 0}</props>
    <props name="cam_mclk0" type="TLMMGpioIdType">{13, 1}</props>
    <props name="cam_mclk1" type="TLMMGpioIdType">{14, 1}</props>
    <props name="cam_mclk2" type="TLMMGpioIdType">{15, 1}</props>
    <props name="webcam2_reset_n" type="TLMMGpioIdType">{16, 0}</props>
    <props name="cci_i2c_sda0" type="TLMMGpioIdType">{17, 1}</props>
    <props name="cci_i2c_scl0" type="TLMMGpioIdType">{18, 1}</props>
    <props name="cci_i2c_sda1" type="TLMMGpioIdType">{19, 1}</props>
    <props name="cci_i2c_scl1" type="TLMMGpioIdType">{20, 1}</props>
    <props name="cci_timer0" type="TLMMGpioIdType">{21, 1}</props>
    <props name="cci_timer1" type="TLMMGpioIdType">{22, 1}</props>
    <props name="webcam1_reset_n" type="TLMMGpioIdType">{23, 0}</props>
    <props name="blsp1_spi_cs2a_n" type="TLMMGpioIdType">{24, 3}</props>
    <props name="webcam1_standby" type="TLMMGpioIdType">{26, 0}</props>
    <props name="blsp_i2c_sda[6]" type="TLMMGpioIdType">{27, 3}</props>
    <props name="blsp_i2c_scl[6]" type="TLMMGpioIdType">{28, 3}</props>
    <props name="cam1_standby_n" type="TLMMGpioIdType">{29, 0}</props>
    <props name="cam1_rst_n" type="TLMMGpioIdType">{30, 0}</props>
    <props name="hdmi_cec" type="TLMMGpioIdType">{31, 1}</props>
    <props name="hdmi_ddc_clock" type="TLMMGpioIdType">{32, 1}</props>
    <props name="hdmi_ddc_data" type="TLMMGpioIdType">{33, 1}</props>
    <props name="hdmi_hot_plug_detect" type="TLMMGpioIdType">{34, 1}</props>
    <props name="pci_e0_rst_n" type="TLMMGpioIdType">{35, 1}</props>
    <props name="pci_e0_clkreqn" type="TLMMGpioIdType">{36, 1}</props>
    <props name="pci_e0_wake" type="TLMMGpioIdType">{37, 0}</props>
    <props name="fm_rds_int_n" type="TLMMGpioIdType">{38, 0}</props>
    <props name="fm_rst_n" type="TLMMGpioIdType">{39, 0}</props>
    <props name="sd_write_protect" type="TLMMGpioIdType">{40, 1}</props>
    <props name="blsp_uart_tx[2]" type="TLMMGpioIdType">{41, 2}</props>
    <props name="blsp_uart_rx[2]" type="TLMMGpioIdType">{42, 2}</props>
    <props name="blsp_uart_cts_n[2]" type="TLMMGpioIdType">{43, 2}</props>
    <props name="blsp_uart_rfr_n[2]" type="TLMMGpioIdType">{44, 2}</props>
    <props name="blsp_uart_tx[3]" type="TLMMGpioIdType">{45, 2}</props>
    <props name="blsp_uart_rx[3]" type="TLMMGpioIdType">{46, 2}</props>
    <props name="blsp_uart_cts_n[3]" type="TLMMGpioIdType">{47, 2}</props>
    <props name="blsp_uart_rfr_n[3]" type="TLMMGpioIdType">{48, 2}</props>
    <props name="uim3_data" type="TLMMGpioIdType">{49, 1}</props>
    <props name="uim3_clk" type="TLMMGpioIdType">{50, 1}</props>
    <props name="uim3_reset" type="TLMMGpioIdType">{51, 1}</props>
    <props name="uim3_present" type="TLMMGpioIdType">{52, 1}</props>
    <props name="codec_int2" type="TLMMGpioIdType">{53, 0}</props>
    <props name="codec_int1" type="TLMMGpioIdType">{54, 0}</props>
    <props name="blsp_i2c_sda[7]" type="TLMMGpioIdType">{55, 3}</props>
    <props name="blsp_i2c_scl[7]" type="TLMMGpioIdType">{56, 3}</props>
    <props name="forced_usb_boot" type="TLMMGpioIdType">{57, 6}</props>
    <props name="uim4_data" type="TLMMGpioIdType">{58, 2}</props>
    <props name="uim4_clk" type="TLMMGpioIdType">{59, 2}</props>
    <props name="uim4_reset" type="TLMMGpioIdType">{60, 2}</props>
    <props name="uim4_present" type="TLMMGpioIdType">{61, 2}</props>
    <props name="cam2_standby_n" type="TLMMGpioIdType">{62, 0}</props>
    <props name="cam2_rst_n" type="TLMMGpioIdType">{63, 0}</props>
    <props name="codec_rst_n" type="TLMMGpioIdType">{64, 0}</props>
    <props name="pcm_clk" type="TLMMGpioIdType">{65, 1}</props>
    <props name="pcm_sync" type="TLMMGpioIdType">{66, 1}</props>
    <props name="pcm_din" type="TLMMGpioIdType">{67, 1}</props>
    <props name="pcm_dout" type="TLMMGpioIdType">{68, 1}</props>
    <props name="audio_ref_clk" type="TLMMGpioIdType">{69, 2}</props>
    <props name="lpass_slimbus_clk" type="TLMMGpioIdType">{70, 1}</props>
    <props name="lpass_slimbus_data0" type="TLMMGpioIdType">{71, 1}</props>
    <props name="lpass_slimbus_data1" type="TLMMGpioIdType">{72, 1}</props>
    <props name="btfm_slimbus_data" type="TLMMGpioIdType">{73, 1}</props>
    <props name="btfm_slimbus_clk" type="TLMMGpioIdType">{74, 1}</props>
    <props name="ter_mi2s_sck" type="TLMMGpioIdType">{75, 1}</props>
    <props name="ter_mi2s_ws" type="TLMMGpioIdType">{76, 1}</props>
    <props name="ter_mi2s_data0" type="TLMMGpioIdType">{77, 1}</props>
    <props name="fm_status_gpio" type="TLMMGpioIdType">{78, 0}</props>
    <props name="ethernet_rst" type="TLMMGpioIdType">{79, 0}</props>
    <props name="mems_reset_n" type="TLMMGpioIdType">{80, 0}</props>
    <props name="blsp_spi_mosi[5]" type="TLMMGpioIdType">{81, 2}</props>
    <props name="blsp_spi_miso[5]" type="TLMMGpioIdType">{82, 2}</props>
    <props name="blsp_spi_cs_n[5]" type="TLMMGpioIdType">{83, 2}</props>
    <props name="blsp_spi_clk[5]" type="TLMMGpioIdType">{84, 1}</props>
    <props name="blsp_spi_mosi[12]" type="TLMMGpioIdType">{85, 1}</props>
    <props name="blsp_i2c_sda[12]" type="TLMMGpioIdType">{87, 3}</props>
    <props name="blsp_i2c_scl[12]" type="TLMMGpioIdType">{88, 3}</props>
    <props name="ts_reset_n" type="TLMMGpioIdType">{89, 0}</props>
    <props name="blsp1_spi_cs1_n" type="TLMMGpioIdType">{90, 2}</props>
    <props name="rcm_trigger1" type="TLMMGpioIdType">{91, 0}</props>
    <props name="rcm_trigger2" type="TLMMGpioIdType">{92, 0}</props>
    <props name="rcm_trigger3" type="TLMMGpioIdType">{93, 0}</props>
    <props name="wigg_en" type="TLMMGpioIdType">{94, 0}</props>
    <props name="sd_card_det_n" type="TLMMGpioIdType">{95, 0}</props>
    <props name="grfc[0]" type="TLMMGpioIdType">{97, 1}</props>
    <props name="grfc[1]" type="TLMMGpioIdType">{98, 1}</props>
    <props name="grfc[2]" type="TLMMGpioIdType">{99, 1}</props>
    <props name="grfc[3]" type="TLMMGpioIdType">{100, 1}</props>
    <props name="grfc[4]" type="TLMMGpioIdType">{101, 1}</props>
    <props name="grfc[5]" type="TLMMGpioIdType">{102, 1}</props>
    <props name="grfc[6]" type="TLMMGpioIdType">{103, 1}</props>
    <props name="grfc[7]" type="TLMMGpioIdType">{104, 1}</props>
    <props name="uim2_data" type="TLMMGpioIdType">{105, 1}</props>
    <props name="uim2_clk" type="TLMMGpioIdType">{106, 1}</props>
    <props name="uim2_reset" type="TLMMGpioIdType">{107, 1}</props>
    <props name="uim2_present" type="TLMMGpioIdType">{108, 1}</props>
    <props name="uim1_data" type="TLMMGpioIdType">{109, 1}</props>
    <props name="uim1_clk" type="TLMMGpioIdType">{110, 1}</props>
    <props name="uim1_reset" type="TLMMGpioIdType">{111, 1}</props>
    <props name="uim1_present" type="TLMMGpioIdType">{112, 1}</props>
    <props name="uim_batt_alarm" type="TLMMGpioIdType">{113, 1}</props>
    <props name="grfc[8]" type="TLMMGpioIdType">{114, 1}</props>
    <props name="grfc[9]" type="TLMMGpioIdType">{115, 1}</props>
    <props name="grfc[10]" type="TLMMGpioIdType">{116, 1}</props>
    <props name="ssc_irq_0" type="TLMMGpioIdType">{117, 0}</props>
    <props name="ssc_irq_1" type="TLMMGpioIdType">{118, 0}</props>
    <props name="ssc_irq_2" type="TLMMGpioIdType">{119, 0}</props>
    <props name="ssc_irq_3" type="TLMMGpioIdType">{120, 0}</props>
    <props name="ssc_irq_4" type="TLMMGpioIdType">{121, 0}</props>
    <props name="ssc_irq_5" type="TLMMGpioIdType">{122, 0}</props>
    <props name="ssc_irq_7" type="TLMMGpioIdType">{124, 0}</props>
    <props name="ssc_irq_8" type="TLMMGpioIdType">{125, 0}</props>
    <props name="grfc[11]" type="TLMMGpioIdType">{127, 1}</props>
    <props name="grfc[12]" type="TLMMGpioIdType">{128, 1}</props>
    <props name="grfc[13]" type="TLMMGpioIdType">{129, 1}</props>
    <props name="pci_e1_rst_n" type="TLMMGpioIdType">{130, 1}</props>
    <props name="pci_e1_clkreqn" type="TLMMGpioIdType">{131, 1}</props>
    <props name="pci_e1_wake" type="TLMMGpioIdType">{132, 0}</props>
    <props name="grfc[14]" type="TLMMGpioIdType">{133, 1}</props>
    <props name="gsm_tx_phase_d0" type="TLMMGpioIdType">{134, 1}</props>
    <props name="gsm_tx_phase_d1" type="TLMMGpioIdType">{135, 1}</props>
    <props name="grfc[15]" type="TLMMGpioIdType">{136, 1}</props>
    <props name="rffe3_data" type="TLMMGpioIdType">{137, 1}</props>
    <props name="rffe3_clk" type="TLMMGpioIdType">{138, 1}</props>
    <props name="rffe4_data" type="TLMMGpioIdType">{139, 1}</props>
    <props name="rffe4_clk" type="TLMMGpioIdType">{140, 1}</props>
    <props name="rffe5_data" type="TLMMGpioIdType">{141, 1}</props>
    <props name="rffe5_clk" type="TLMMGpioIdType">{142, 1}</props>
    <props name="gps_tx_aggressor" type="TLMMGpioIdType">{143, 1}</props>
    <props name="mss_lte_coxm_txd" type="TLMMGpioIdType">{144, 1}</props>
    <props name="mss_lte_coxm_rxd" type="TLMMGpioIdType">{145, 1}</props>
    <props name="rffe2_data" type="TLMMGpioIdType">{146, 1}</props>
    <props name="rffe2_clk" type="TLMMGpioIdType">{147, 1}</props>
    <props name="rffe1_data" type="TLMMGpioIdType">{148, 1}</props>
    <props name="rffe1_clk" type="TLMMGpioIdType">{149, 1}</props>
  </device>

  <device id="/tlmm/fluid1">
    <props name="blsp_spi_mosi[1]" type="TLMMGpioIdType">{0, 1}</props>
    <props name="blsp_spi_miso[1]" type="TLMMGpioIdType">{1, 1}</props>
    <props name="blsp_spi_clk[1]" type="TLMMGpioIdType">{3, 1}</props>
    <props name="blsp_uart_tx[8]" type="TLMMGpioIdType">{4, 2}</props>
    <props name="blsp_uart_rx[8]" type="TLMMGpioIdType">{5, 2}</props>
    <props name="blsp_i2c_sda[8]" type="TLMMGpioIdType">{6, 3}</props>
    <props name="blsp_i2c_scl[8]" type="TLMMGpioIdType">{7, 3}</props>
    <props name="lcd0_reset_n" type="TLMMGpioIdType">{8, 0}</props>
    <props name="nfc_irq" type="TLMMGpioIdType">{9, 0}</props>
    <props name="mdp_vsync_p" type="TLMMGpioIdType">{10, 1}</props>
    <props name="ethernet_int" type="TLMMGpioIdType">{11, 0}</props>
    <props name="nfc_disable" type="TLMMGpioIdType">{12, 0}</props>
    <props name="cam_mclk0" type="TLMMGpioIdType">{13, 1}</props>
    <props name="cam_mclk1" type="TLMMGpioIdType">{14, 1}</props>
    <props name="cam_mclk2" type="TLMMGpioIdType">{15, 1}</props>
    <props name="webcam2_reset_n" type="TLMMGpioIdType">{16, 0}</props>
    <props name="cci_i2c_sda0" type="TLMMGpioIdType">{17, 1}</props>
    <props name="cci_i2c_scl0" type="TLMMGpioIdType">{18, 1}</props>
    <props name="cci_i2c_sda1" type="TLMMGpioIdType">{19, 1}</props>
    <props name="cci_i2c_scl1" type="TLMMGpioIdType">{20, 1}</props>
    <props name="cci_timer0" type="TLMMGpioIdType">{21, 1}</props>
    <props name="cci_timer1" type="TLMMGpioIdType">{22, 1}</props>
    <props name="webcam1_reset_n" type="TLMMGpioIdType">{23, 0}</props>
    <props name="blsp1_spi_cs2a_n" type="TLMMGpioIdType">{24, 3}</props>
    <props name="webcam1_standby" type="TLMMGpioIdType">{26, 0}</props>
    <props name="blsp_i2c_sda[6]" type="TLMMGpioIdType">{27, 3}</props>
    <props name="blsp_i2c_scl[6]" type="TLMMGpioIdType">{28, 3}</props>
    <props name="cam1_standby_n" type="TLMMGpioIdType">{29, 0}</props>
    <props name="cam1_rst_n" type="TLMMGpioIdType">{30, 0}</props>
    <props name="hdmi_cec" type="TLMMGpioIdType">{31, 1}</props>
    <props name="hdmi_ddc_clock" type="TLMMGpioIdType">{32, 1}</props>
    <props name="hdmi_ddc_data" type="TLMMGpioIdType">{33, 1}</props>
    <props name="hdmi_hot_plug_detect" type="TLMMGpioIdType">{34, 1}</props>
    <props name="pci_e0_rst_n" type="TLMMGpioIdType">{35, 1}</props>
    <props name="pci_e0_clkreqn" type="TLMMGpioIdType">{36, 1}</props>
    <props name="pci_e0_wake" type="TLMMGpioIdType">{37, 0}</props>
    <props name="fm_rds_int_n" type="TLMMGpioIdType">{38, 0}</props>
    <props name="fm_rst_n" type="TLMMGpioIdType">{39, 0}</props>
    <props name="sd_write_protect" type="TLMMGpioIdType">{40, 1}</props>
    <props name="blsp_uart_tx[2]" type="TLMMGpioIdType">{41, 2}</props>
    <props name="blsp_uart_rx[2]" type="TLMMGpioIdType">{42, 2}</props>
    <props name="blsp_uart_cts_n[2]" type="TLMMGpioIdType">{43, 2}</props>
    <props name="blsp_uart_rfr_n[2]" type="TLMMGpioIdType">{44, 2}</props>
    <props name="blsp_uart_tx[3]" type="TLMMGpioIdType">{45, 2}</props>
    <props name="blsp_uart_rx[3]" type="TLMMGpioIdType">{46, 2}</props>
    <props name="blsp_uart_cts_n[3]" type="TLMMGpioIdType">{47, 2}</props>
    <props name="blsp_uart_rfr_n[3]" type="TLMMGpioIdType">{48, 2}</props>
    <props name="uim3_data" type="TLMMGpioIdType">{49, 1}</props>
    <props name="uim3_clk" type="TLMMGpioIdType">{50, 1}</props>
    <props name="uim3_reset" type="TLMMGpioIdType">{51, 1}</props>
    <props name="uim3_present" type="TLMMGpioIdType">{52, 1}</props>
    <props name="codec_int2" type="TLMMGpioIdType">{53, 0}</props>
    <props name="codec_int1" type="TLMMGpioIdType">{54, 0}</props>
    <props name="blsp_i2c_sda[7]" type="TLMMGpioIdType">{55, 3}</props>
    <props name="blsp_i2c_scl[7]" type="TLMMGpioIdType">{56, 3}</props>
    <props name="forced_usb_boot" type="TLMMGpioIdType">{57, 6}</props>
    <props name="uim4_data" type="TLMMGpioIdType">{58, 2}</props>
    <props name="uim4_clk" type="TLMMGpioIdType">{59, 2}</props>
    <props name="uim4_reset" type="TLMMGpioIdType">{60, 2}</props>
    <props name="uim4_present" type="TLMMGpioIdType">{61, 2}</props>
    <props name="cam2_standby_n" type="TLMMGpioIdType">{62, 0}</props>
    <props name="cam2_rst_n" type="TLMMGpioIdType">{63, 0}</props>
    <props name="codec_rst_n" type="TLMMGpioIdType">{64, 0}</props>
    <props name="pcm_clk" type="TLMMGpioIdType">{65, 1}</props>
    <props name="pcm_sync" type="TLMMGpioIdType">{66, 1}</props>
    <props name="pcm_din" type="TLMMGpioIdType">{67, 1}</props>
    <props name="pcm_dout" type="TLMMGpioIdType">{68, 1}</props>
    <props name="audio_ref_clk" type="TLMMGpioIdType">{69, 2}</props>
    <props name="lpass_slimbus_clk" type="TLMMGpioIdType">{70, 1}</props>
    <props name="lpass_slimbus_data0" type="TLMMGpioIdType">{71, 1}</props>
    <props name="lpass_slimbus_data1" type="TLMMGpioIdType">{72, 1}</props>
    <props name="btfm_slimbus_data" type="TLMMGpioIdType">{73, 1}</props>
    <props name="btfm_slimbus_clk" type="TLMMGpioIdType">{74, 1}</props>
    <props name="ter_mi2s_sck" type="TLMMGpioIdType">{75, 1}</props>
    <props name="ter_mi2s_ws" type="TLMMGpioIdType">{76, 1}</props>
    <props name="ter_mi2s_data0" type="TLMMGpioIdType">{77, 1}</props>
    <props name="fm_status_gpio" type="TLMMGpioIdType">{78, 0}</props>
    <props name="ethernet_rst" type="TLMMGpioIdType">{79, 0}</props>
    <props name="mems_reset_n" type="TLMMGpioIdType">{80, 0}</props>
    <props name="blsp_spi_mosi[5]" type="TLMMGpioIdType">{81, 2}</props>
    <props name="blsp_spi_miso[5]" type="TLMMGpioIdType">{82, 2}</props>
    <props name="blsp_spi_cs_n[5]" type="TLMMGpioIdType">{83, 2}</props>
    <props name="blsp_spi_clk[5]" type="TLMMGpioIdType">{84, 1}</props>
    <props name="blsp_spi_mosi[12]" type="TLMMGpioIdType">{85, 1}</props>
    <props name="blsp_i2c_sda[12]" type="TLMMGpioIdType">{87, 3}</props>
    <props name="blsp_i2c_scl[12]" type="TLMMGpioIdType">{88, 3}</props>
    <props name="ts_reset_n" type="TLMMGpioIdType">{89, 0}</props>
    <props name="blsp1_spi_cs1_n" type="TLMMGpioIdType">{90, 2}</props>
    <props name="rcm_trigger1" type="TLMMGpioIdType">{91, 0}</props>
    <props name="rcm_trigger2" type="TLMMGpioIdType">{92, 0}</props>
    <props name="rcm_trigger3" type="TLMMGpioIdType">{93, 0}</props>
    <props name="wigg_en" type="TLMMGpioIdType">{94, 0}</props>
    <props name="sd_card_det_n" type="TLMMGpioIdType">{95, 0}</props>
    <props name="grfc[0]" type="TLMMGpioIdType">{97, 1}</props>
    <props name="grfc[1]" type="TLMMGpioIdType">{98, 1}</props>
    <props name="grfc[2]" type="TLMMGpioIdType">{99, 1}</props>
    <props name="grfc[3]" type="TLMMGpioIdType">{100, 1}</props>
    <props name="grfc[4]" type="TLMMGpioIdType">{101, 1}</props>
    <props name="grfc[5]" type="TLMMGpioIdType">{102, 1}</props>
    <props name="grfc[6]" type="TLMMGpioIdType">{103, 1}</props>
    <props name="grfc[7]" type="TLMMGpioIdType">{104, 1}</props>
    <props name="uim2_data" type="TLMMGpioIdType">{105, 1}</props>
    <props name="uim2_clk" type="TLMMGpioIdType">{106, 1}</props>
    <props name="uim2_reset" type="TLMMGpioIdType">{107, 1}</props>
    <props name="uim2_present" type="TLMMGpioIdType">{108, 1}</props>
    <props name="uim1_data" type="TLMMGpioIdType">{109, 1}</props>
    <props name="uim1_clk" type="TLMMGpioIdType">{110, 1}</props>
    <props name="uim1_reset" type="TLMMGpioIdType">{111, 1}</props>
    <props name="uim1_present" type="TLMMGpioIdType">{112, 1}</props>
    <props name="uim_batt_alarm" type="TLMMGpioIdType">{113, 1}</props>
    <props name="grfc[8]" type="TLMMGpioIdType">{114, 1}</props>
    <props name="grfc[9]" type="TLMMGpioIdType">{115, 1}</props>
    <props name="grfc[10]" type="TLMMGpioIdType">{116, 1}</props>
    <props name="ssc_irq_0" type="TLMMGpioIdType">{117, 0}</props>
    <props name="ssc_irq_1" type="TLMMGpioIdType">{118, 0}</props>
    <props name="ssc_irq_2" type="TLMMGpioIdType">{119, 0}</props>
    <props name="ssc_irq_3" type="TLMMGpioIdType">{120, 0}</props>
    <props name="ssc_irq_4" type="TLMMGpioIdType">{121, 0}</props>
    <props name="ssc_irq_5" type="TLMMGpioIdType">{122, 0}</props>
    <props name="ssc_irq_7" type="TLMMGpioIdType">{124, 0}</props>
    <props name="ssc_irq_8" type="TLMMGpioIdType">{125, 0}</props>
    <props name="grfc[11]" type="TLMMGpioIdType">{127, 1}</props>
    <props name="grfc[12]" type="TLMMGpioIdType">{128, 1}</props>
    <props name="grfc[13]" type="TLMMGpioIdType">{129, 1}</props>
    <props name="pci_e1_rst_n" type="TLMMGpioIdType">{130, 1}</props>
    <props name="pci_e1_clkreqn" type="TLMMGpioIdType">{131, 1}</props>
    <props name="pci_e1_wake" type="TLMMGpioIdType">{132, 0}</props>
    <props name="grfc[14]" type="TLMMGpioIdType">{133, 1}</props>
    <props name="gsm_tx_phase_d0" type="TLMMGpioIdType">{134, 1}</props>
    <props name="gsm_tx_phase_d1" type="TLMMGpioIdType">{135, 1}</props>
    <props name="grfc[15]" type="TLMMGpioIdType">{136, 1}</props>
    <props name="rffe3_data" type="TLMMGpioIdType">{137, 1}</props>
    <props name="rffe3_clk" type="TLMMGpioIdType">{138, 1}</props>
    <props name="rffe4_data" type="TLMMGpioIdType">{139, 1}</props>
    <props name="rffe4_clk" type="TLMMGpioIdType">{140, 1}</props>
    <props name="rffe5_data" type="TLMMGpioIdType">{141, 1}</props>
    <props name="rffe5_clk" type="TLMMGpioIdType">{142, 1}</props>
    <props name="gps_tx_aggressor" type="TLMMGpioIdType">{143, 1}</props>
    <props name="mss_lte_coxm_txd" type="TLMMGpioIdType">{144, 1}</props>
    <props name="mss_lte_coxm_rxd" type="TLMMGpioIdType">{145, 1}</props>
    <props name="rffe2_data" type="TLMMGpioIdType">{146, 1}</props>
    <props name="rffe2_clk" type="TLMMGpioIdType">{147, 1}</props>
    <props name="rffe1_data" type="TLMMGpioIdType">{148, 1}</props>
    <props name="rffe1_clk" type="TLMMGpioIdType">{149, 1}</props>
  </device>

  <device id="/tlmm/fusion">
    <props name="blsp_spi_mosi[1]" type="TLMMGpioIdType">{0, 1}</props>
    <props name="blsp_spi_miso[1]" type="TLMMGpioIdType">{1, 1}</props>
    <props name="blsp_spi_cs_n[1]" type="TLMMGpioIdType">{2, 1}</props>
    <props name="blsp_spi_clk[1]" type="TLMMGpioIdType">{3, 1}</props>
    <props name="blsp_uart_tx[8]" type="TLMMGpioIdType">{4, 2}</props>
    <props name="blsp_uart_rx[8]" type="TLMMGpioIdType">{5, 2}</props>
    <props name="blsp_i2c_sda[8]" type="TLMMGpioIdType">{6, 3}</props>
    <props name="blsp_i2c_scl[8]" type="TLMMGpioIdType">{7, 3}</props>
    <props name="lcd0_reset_n" type="TLMMGpioIdType">{8, 0}</props>
    <props name="nfc_irq" type="TLMMGpioIdType">{9, 0}</props>
    <props name="mdp_vsync_p" type="TLMMGpioIdType">{10, 1}</props>
    <props name="mdp_vsync_s" type="TLMMGpioIdType">{11, 1}</props>
    <props name="ethernet_int" type="TLMMGpioIdType">{11, 0}</props>
    <props name="nfc_disable" type="TLMMGpioIdType">{12, 0}</props>
    <props name="cam_mclk0" type="TLMMGpioIdType">{13, 1}</props>
    <props name="cam_mclk1" type="TLMMGpioIdType">{14, 1}</props>
    <props name="cam_mclk2" type="TLMMGpioIdType">{15, 1}</props>
    <props name="webcam2_reset_n" type="TLMMGpioIdType">{16, 0}</props>
    <props name="cci_i2c_sda0" type="TLMMGpioIdType">{17, 1}</props>
    <props name="cci_i2c_scl0" type="TLMMGpioIdType">{18, 1}</props>
    <props name="cci_i2c_sda1" type="TLMMGpioIdType">{19, 1}</props>
    <props name="cci_i2c_scl1" type="TLMMGpioIdType">{20, 1}</props>
    <props name="cci_timer0" type="TLMMGpioIdType">{21, 1}</props>
    <props name="cci_timer1" type="TLMMGpioIdType">{22, 1}</props>
    <props name="webcam1_reset_n" type="TLMMGpioIdType">{23, 0}</props>
    <props name="qdss_cti_trig_in_a" type="TLMMGpioIdType">{24, 5}</props>
    <props name="qdss_cti_trig_out_a" type="TLMMGpioIdType">{25, 8}</props>
    <props name="webcam1_standby" type="TLMMGpioIdType">{26, 0}</props>
    <props name="cam1_standby_n" type="TLMMGpioIdType">{29, 0}</props>
    <props name="cam1_rst_n" type="TLMMGpioIdType">{30, 0}</props>
    <props name="hdmi_cec" type="TLMMGpioIdType">{31, 1}</props>
    <props name="hdmi_ddc_clk" type="TLMMGpioIdType">{32, 255}</props>
    <props name="hdmi_ddc_data" type="TLMMGpioIdType">{33, 1}</props>
    <props name="hdmi_hot_plug_detect" type="TLMMGpioIdType">{34, 1}</props>
    <props name="pci_e0_rst_n" type="TLMMGpioIdType">{35, 1}</props>
    <props name="pci_e0_clkreq_n" type="TLMMGpioIdType">{36, 255}</props>
    <props name="pci_e0_wake" type="TLMMGpioIdType">{37, 0}</props>
    <props name="fm_rds_int_n" type="TLMMGpioIdType">{38, 0}</props>
    <props name="fm_rst_n" type="TLMMGpioIdType">{39, 0}</props>
    <props name="sd_write_protect" type="TLMMGpioIdType">{40, 1}</props>
    <props name="blsp_uart_tx[2]" type="TLMMGpioIdType">{41, 2}</props>
    <props name="blsp_uart_rx[2]" type="TLMMGpioIdType">{42, 2}</props>
    <props name="blsp_uart_cts_n[2]" type="TLMMGpioIdType">{43, 2}</props>
    <props name="blsp_uart_rfr_n[2]" type="TLMMGpioIdType">{44, 2}</props>
    <props name="blsp_uart_tx[3]" type="TLMMGpioIdType">{45, 2}</props>
    <props name="blsp_uart_rx[3]" type="TLMMGpioIdType">{46, 2}</props>
    <props name="blsp_uart_cts_n[3]" type="TLMMGpioIdType">{47, 2}</props>
    <props name="blsp_uart_rfr_n[3]" type="TLMMGpioIdType">{48, 2}</props>
    <props name="blsp_uart_tx[9]" type="TLMMGpioIdType">{49, 3}</props>
    <props name="blsp_uart_rx[9]" type="TLMMGpioIdType">{50, 3}</props>
    <props name="gnss_reset_n" type="TLMMGpioIdType">{52, 0}</props>
    <props name="codec_int2" type="TLMMGpioIdType">{53, 0}</props>
    <props name="codec_int1" type="TLMMGpioIdType">{54, 0}</props>
    <props name="blsp_i2c_sda[7]" type="TLMMGpioIdType">{55, 3}</props>
    <props name="blsp_i2c_scl[7]" type="TLMMGpioIdType">{56, 3}</props>
    <props name="forced_usb_boot" type="TLMMGpioIdType">{57, 6}</props>
    <props name="uim4_data" type="TLMMGpioIdType">{58, 2}</props>
    <props name="uim4_clk" type="TLMMGpioIdType">{59, 2}</props>
    <props name="uim4_reset" type="TLMMGpioIdType">{60, 2}</props>
    <props name="uim4_present" type="TLMMGpioIdType">{61, 2}</props>
    <props name="cam2_standby_n" type="TLMMGpioIdType">{62, 0}</props>
    <props name="cam2_rst_n" type="TLMMGpioIdType">{63, 0}</props>
    <props name="codec_rst_n" type="TLMMGpioIdType">{64, 0}</props>
    <props name="pcm_clk" type="TLMMGpioIdType">{65, 1}</props>
    <props name="pcm_sync" type="TLMMGpioIdType">{66, 1}</props>
    <props name="pcm_din" type="TLMMGpioIdType">{67, 1}</props>
    <props name="pcm_dout" type="TLMMGpioIdType">{68, 1}</props>
    <props name="audio_ref_clk" type="TLMMGpioIdType">{69, 2}</props>
    <props name="slimbus_clk" type="TLMMGpioIdType">{70, 255}</props>
    <props name="slimbus_data0" type="TLMMGpioIdType">{71, 255}</props>
    <props name="slimbus_data1" type="TLMMGpioIdType">{72, 255}</props>
    <props name="btfm_slimbus_data" type="TLMMGpioIdType">{73, 1}</props>
    <props name="btfm_slimbus_clk" type="TLMMGpioIdType">{74, 1}</props>
    <props name="ter_mi2s_sck" type="TLMMGpioIdType">{75, 1}</props>
    <props name="ter_mi2s_ws" type="TLMMGpioIdType">{76, 1}</props>
    <props name="ter_mi2s_data0" type="TLMMGpioIdType">{77, 1}</props>
    <props name="fm_status_gpio" type="TLMMGpioIdType">{78, 0}</props>
    <props name="ethernet_rst" type="TLMMGpioIdType">{79, 0}</props>
    <props name="wlan_en" type="TLMMGpioIdType">{80, 0}</props>
    <props name="blsp_spi_mosi[5]" type="TLMMGpioIdType">{81, 2}</props>
    <props name="blsp_spi_miso[5]" type="TLMMGpioIdType">{82, 2}</props>
    <props name="blsp_spi_cs_n[5]" type="TLMMGpioIdType">{83, 2}</props>
    <props name="blsp_spi_clk[5]" type="TLMMGpioIdType">{84, 1}</props>
    <props name="blsp_spi_mosi[12]" type="TLMMGpioIdType">{85, 1}</props>
    <props name="ts_aux" type="TLMMGpioIdType">{86, 0}</props>
    <props name="blsp_i2c_sda[12]" type="TLMMGpioIdType">{87, 3}</props>
    <props name="blsp_i2c_scl[12]" type="TLMMGpioIdType">{88, 3}</props>
    <props name="ts_reset_n" type="TLMMGpioIdType">{89, 0}</props>
    <props name="blsp1_spi_cs1_n" type="TLMMGpioIdType">{90, 2}</props>
    <props name="rcm_trigger1" type="TLMMGpioIdType">{91, 0}</props>
    <props name="rcm_trigger2" type="TLMMGpioIdType">{92, 0}</props>
    <props name="rcm_trigger3" type="TLMMGpioIdType">{93, 0}</props>
    <props name="wigg_en" type="TLMMGpioIdType">{94, 0}</props>
    <props name="sd_card_det_n" type="TLMMGpioIdType">{95, 0}</props>
    <props name="lcd1_reset_n" type="TLMMGpioIdType">{96, 0}</props>
    <props name="qdss_cti_trig_out_d" type="TLMMGpioIdType">{100, 4}</props>
    <props name="qdss_cti_trig_in_d" type="TLMMGpioIdType">{101, 3}</props>
    <props name="apq_div_clk1_en" type="TLMMGpioIdType">{102, 0}</props>
    <props name="ap2mdm_chnl_rdy" type="TLMMGpioIdType">{103, 0}</props>
    <props name="mdm2ap_wakeup" type="TLMMGpioIdType">{104, 0}</props>
    <props name="ap2mdm_wakeup" type="TLMMGpioIdType">{105, 0}</props>
    <props name="mdm2ap_status" type="TLMMGpioIdType">{106, 0}</props>
    <props name="ap2mdm_status" type="TLMMGpioIdType">{107, 0}</props>
    <props name="mdm2ap_err_fatal" type="TLMMGpioIdType">{108, 0}</props>
    <props name="ap2mdm_err_fatal" type="TLMMGpioIdType">{109, 0}</props>
    <props name="mdm2ap_chnl_rdy" type="TLMMGpioIdType">{110, 0}</props>
    <props name="ap2mdm_vdd_min" type="TLMMGpioIdType">{111, 0}</props>
    <props name="mdm2ap_vdd_min" type="TLMMGpioIdType">{112, 0}</props>
    <props name="ap2mdm_pon_reset" type="TLMMGpioIdType">{113, 0}</props>
    <props name="pci_e2_rst_n" type="TLMMGpioIdType">{114, 2}</props>
    <props name="pci_e2_clkreq_n" type="TLMMGpioIdType">{115, 255}</props>
    <props name="pci_e2_wake" type="TLMMGpioIdType">{116, 0}</props>
    <props name="ssc_irq_0" type="TLMMGpioIdType">{117, 0}</props>
    <props name="ssc_irq_1" type="TLMMGpioIdType">{118, 0}</props>
    <props name="ssc_ir1_2" type="TLMMGpioIdType">{119, 0}</props>
    <props name="ssc_irq_3" type="TLMMGpioIdType">{120, 0}</props>
    <props name="ssc_irq_4" type="TLMMGpioIdType">{121, 0}</props>
    <props name="ssc_irq_5" type="TLMMGpioIdType">{122, 0}</props>
    <props name="ssc_irq_6" type="TLMMGpioIdType">{123, 0}</props>
    <props name="ssc_irq_7" type="TLMMGpioIdType">{124, 0}</props>
    <props name="ssc_irq_8" type="TLMMGpioIdType">{125, 0}</props>
    <props name="mems_reset_n" type="TLMMGpioIdType">{126, 0}</props>
    <props name="pci_e1_rst_n" type="TLMMGpioIdType">{130, 1}</props>
    <props name="pci_e1_clkreq_n" type="TLMMGpioIdType">{131, 255}</props>
    <props name="pci_e1_wake" type="TLMMGpioIdType">{132, 0}</props>
  </device>

  <device id="/tlmm/cdp001">
    <props name="blsp_spi_mosi[1]" type="TLMMGpioIdType">{0, 1}</props>
    <props name="blsp_spi_miso[1]" type="TLMMGpioIdType">{1, 1}</props>
    <props name="blsp_spi_cs_n[1]" type="TLMMGpioIdType">{2, 1}</props>
    <props name="blsp_spi_clk[1]" type="TLMMGpioIdType">{3, 1}</props>
    <props name="blsp_uart_tx[8]" type="TLMMGpioIdType">{4, 2}</props>
    <props name="blsp_uart_rx[8]" type="TLMMGpioIdType">{5, 2}</props>
    <props name="blsp_i2c_sda[8]" type="TLMMGpioIdType">{6, 3}</props>
    <props name="blsp_i2c_scl[8]" type="TLMMGpioIdType">{7, 3}</props>
    <props name="lcd0_reset_n" type="TLMMGpioIdType">{8, 0}</props>
    <props name="nfc_irq" type="TLMMGpioIdType">{9, 0}</props>
    <props name="mdp_vsync_p" type="TLMMGpioIdType">{10, 1}</props>
    <props name="mdp_vsync_s" type="TLMMGpioIdType">{11, 1}</props>
    <props name="ethernet_int" type="TLMMGpioIdType">{11, 0}</props>
    <props name="nfc_disable" type="TLMMGpioIdType">{12, 0}</props>
    <props name="cam_mclk0" type="TLMMGpioIdType">{13, 1}</props>
    <props name="cam_mclk1" type="TLMMGpioIdType">{14, 1}</props>
    <props name="cam_mclk2" type="TLMMGpioIdType">{15, 1}</props>
    <props name="webcam2_reset_n" type="TLMMGpioIdType">{16, 0}</props>
    <props name="cci_i2c_sda0" type="TLMMGpioIdType">{17, 1}</props>
    <props name="cci_i2c_scl0" type="TLMMGpioIdType">{18, 1}</props>
    <props name="cci_i2c_sda1" type="TLMMGpioIdType">{19, 1}</props>
    <props name="cci_i2c_scl1" type="TLMMGpioIdType">{20, 1}</props>
    <props name="cci_timer0" type="TLMMGpioIdType">{21, 1}</props>
    <props name="cci_timer1" type="TLMMGpioIdType">{22, 1}</props>
    <props name="webcam1_reset_n" type="TLMMGpioIdType">{23, 0}</props>
    <props name="qdss_cti_trig_in_a" type="TLMMGpioIdType">{24, 5}</props>
    <props name="qdss_cti_trig_out_a" type="TLMMGpioIdType">{25, 8}</props>
    <props name="webcam1_standby" type="TLMMGpioIdType">{26, 0}</props>
    <props name="cam1_standby_n" type="TLMMGpioIdType">{29, 0}</props>
    <props name="cam1_rst_n" type="TLMMGpioIdType">{30, 0}</props>
    <props name="hdmi_cec" type="TLMMGpioIdType">{31, 1}</props>
    <props name="hdmi_ddc_clock" type="TLMMGpioIdType">{32, 1}</props>
    <props name="hdmi_ddc_data" type="TLMMGpioIdType">{33, 1}</props>
    <props name="hdmi_hot_plug_detect" type="TLMMGpioIdType">{34, 1}</props>
    <props name="pci_e0_rst_n" type="TLMMGpioIdType">{35, 1}</props>
    <props name="pci_e0_clkreqn" type="TLMMGpioIdType">{36, 1}</props>
    <props name="pci_e0_wake" type="TLMMGpioIdType">{37, 0}</props>
    <props name="fm_rds_int_n" type="TLMMGpioIdType">{38, 0}</props>
    <props name="fm_rst_n" type="TLMMGpioIdType">{39, 0}</props>
    <props name="sd_write_protect" type="TLMMGpioIdType">{40, 1}</props>
    <props name="blsp_uart_tx[2]" type="TLMMGpioIdType">{41, 2}</props>
    <props name="blsp_uart_rx[2]" type="TLMMGpioIdType">{42, 2}</props>
    <props name="blsp_uart_cts_n[2]" type="TLMMGpioIdType">{43, 2}</props>
    <props name="blsp_uart_rfr_n[2]" type="TLMMGpioIdType">{44, 2}</props>
    <props name="blsp_uart_tx[3]" type="TLMMGpioIdType">{45, 2}</props>
    <props name="blsp_uart_rx[3]" type="TLMMGpioIdType">{46, 2}</props>
    <props name="blsp_uart_cts_n[3]" type="TLMMGpioIdType">{47, 2}</props>
    <props name="blsp_uart_rfr_n[3]" type="TLMMGpioIdType">{48, 2}</props>
    <props name="blsp_uart_tx[9]" type="TLMMGpioIdType">{49, 3}</props>
    <props name="blsp_uart_rx[9]" type="TLMMGpioIdType">{50, 3}</props>
    <props name="gnss_reset_n" type="TLMMGpioIdType">{52, 0}</props>
    <props name="codec_int2" type="TLMMGpioIdType">{53, 0}</props>
    <props name="codec_int1" type="TLMMGpioIdType">{54, 0}</props>
    <props name="blsp_i2c_sda[7]" type="TLMMGpioIdType">{55, 3}</props>
    <props name="blsp_i2c_scl[7]" type="TLMMGpioIdType">{56, 3}</props>
    <props name="forced_usb_boot" type="TLMMGpioIdType">{57, 6}</props>
    <props name="uim4_data" type="TLMMGpioIdType">{58, 2}</props>
    <props name="uim4_clk" type="TLMMGpioIdType">{59, 2}</props>
    <props name="uim4_reset" type="TLMMGpioIdType">{60, 2}</props>
    <props name="uim4_present" type="TLMMGpioIdType">{61, 2}</props>
    <props name="cam2_standby_n" type="TLMMGpioIdType">{62, 0}</props>
    <props name="cam2_rst_n" type="TLMMGpioIdType">{63, 0}</props>
    <props name="codec_rst_n" type="TLMMGpioIdType">{64, 0}</props>
    <props name="pcm_clk" type="TLMMGpioIdType">{65, 1}</props>
    <props name="pcm_sync" type="TLMMGpioIdType">{66, 1}</props>
    <props name="pcm_din" type="TLMMGpioIdType">{67, 1}</props>
    <props name="pcm_dout" type="TLMMGpioIdType">{68, 1}</props>
    <props name="audio_ref_clk" type="TLMMGpioIdType">{69, 2}</props>
    <props name="lpass_slimbus_clk" type="TLMMGpioIdType">{70, 1}</props>
    <props name="lpass_slimbus_data0" type="TLMMGpioIdType">{71, 1}</props>
    <props name="lpass_slimbus_data1" type="TLMMGpioIdType">{72, 1}</props>
    <props name="btfm_slimbus_data" type="TLMMGpioIdType">{73, 1}</props>
    <props name="btfm_slimbus_clk" type="TLMMGpioIdType">{74, 1}</props>
    <props name="ter_mi2s_sck" type="TLMMGpioIdType">{75, 1}</props>
    <props name="ter_mi2s_ws" type="TLMMGpioIdType">{76, 1}</props>
    <props name="ter_mi2s_data0" type="TLMMGpioIdType">{77, 1}</props>
    <props name="fm_status_gpio" type="TLMMGpioIdType">{78, 0}</props>
    <props name="ethernet_rst" type="TLMMGpioIdType">{79, 0}</props>
    <props name="mems_reset_n" type="TLMMGpioIdType">{80, 0}</props>
    <props name="blsp_spi_mosi[5]" type="TLMMGpioIdType">{81, 2}</props>
    <props name="blsp_spi_miso[5]" type="TLMMGpioIdType">{82, 2}</props>
    <props name="blsp_spi_cs_n[5]" type="TLMMGpioIdType">{83, 2}</props>
    <props name="blsp_spi_clk[5]" type="TLMMGpioIdType">{84, 1}</props>
    <props name="blsp_spi_mosi[12]" type="TLMMGpioIdType">{85, 1}</props>
    <props name="ts_aux" type="TLMMGpioIdType">{86, 0}</props>
    <props name="blsp_i2c_sda[12]" type="TLMMGpioIdType">{87, 3}</props>
    <props name="blsp_i2c_scl[12]" type="TLMMGpioIdType">{88, 3}</props>
    <props name="ts_reset_n" type="TLMMGpioIdType">{89, 0}</props>
    <props name="blsp1_spi_cs1_n" type="TLMMGpioIdType">{90, 2}</props>
    <props name="rcm_trigger1" type="TLMMGpioIdType">{91, 0}</props>
    <props name="rcm_trigger2" type="TLMMGpioIdType">{92, 0}</props>
    <props name="rcm_trigger3" type="TLMMGpioIdType">{93, 0}</props>
    <props name="wigg_en" type="TLMMGpioIdType">{94, 0}</props>
    <props name="sd_card_det_n" type="TLMMGpioIdType">{95, 0}</props>
    <props name="lcd1_reset_n" type="TLMMGpioIdType">{96, 0}</props>
    <props name="qdss_cti_trig_out_d" type="TLMMGpioIdType">{100, 4}</props>
    <props name="qdss_cti_trig_in_d" type="TLMMGpioIdType">{101, 3}</props>
    <props name="apq_div_clk1_en" type="TLMMGpioIdType">{102, 0}</props>
    <props name="ap2mdm_chnl_rdy" type="TLMMGpioIdType">{103, 0}</props>
    <props name="mdm2ap_wakeup" type="TLMMGpioIdType">{104, 0}</props>
    <props name="ap2mdm_wakeup" type="TLMMGpioIdType">{105, 0}</props>
    <props name="mdm2ap_status" type="TLMMGpioIdType">{106, 0}</props>
    <props name="ap2mdm_status" type="TLMMGpioIdType">{107, 0}</props>
    <props name="mdm2ap_err_fatal" type="TLMMGpioIdType">{108, 0}</props>
    <props name="ap2mdm_err_fatal" type="TLMMGpioIdType">{109, 0}</props>
    <props name="mdm2ap_chnl_rdy" type="TLMMGpioIdType">{110, 0}</props>
    <props name="ap2mdm_vdd_min" type="TLMMGpioIdType">{111, 0}</props>
    <props name="mdm2ap_vdd_min" type="TLMMGpioIdType">{112, 0}</props>
    <props name="ap2mdm_pon_reset" type="TLMMGpioIdType">{113, 0}</props>
    <props name="pci_e2_rst_n" type="TLMMGpioIdType">{114, 2}</props>
    <props name="pci_e2_clkreq_n" type="TLMMGpioIdType">{115, 255}</props>
    <props name="pci_e2_wake" type="TLMMGpioIdType">{116, 0}</props>
    <props name="ssc_irq_0" type="TLMMGpioIdType">{117, 0}</props>
    <props name="ssc_irq_1" type="TLMMGpioIdType">{118, 0}</props>
    <props name="ssc_irq_2" type="TLMMGpioIdType">{119, 0}</props>
    <props name="ssc_irq_3" type="TLMMGpioIdType">{120, 0}</props>
    <props name="ssc_irq_4" type="TLMMGpioIdType">{121, 0}</props>
    <props name="ssc_irq_5" type="TLMMGpioIdType">{122, 0}</props>
    <props name="ssc_irq_6" type="TLMMGpioIdType">{123, 0}</props>
    <props name="ssc_irq_7" type="TLMMGpioIdType">{124, 0}</props>
    <props name="ssc_irq_8" type="TLMMGpioIdType">{125, 0}</props>
    <props name="pci_e1_rst_n" type="TLMMGpioIdType">{130, 1}</props>
    <props name="pci_e1_clkreqn" type="TLMMGpioIdType">{131, 1}</props>
    <props name="pci_e1_wake" type="TLMMGpioIdType">{132, 0}</props>
  </device>

<driver name="VCS">
  <device id="VCS">
     <!-- VCS Log Defaults -->
     <props name="VCSLogDefaults" type=0x00000012>
       VCS_LogDefaultConfig
     </props>
     <!-- Rails -->
     <props name="VCSBSPConfig" type=0x00000012>
       VCS_BSPConfig
     </props>
   </device>
</driver>

<driver name="NULL">
    <global_def>
       <var_seq name="g_pl_dtor_register_list" type=0x00000003>
            0x95c0080, 0x95c2080, end
       </var_seq>
    </global_def>
    <device id="/platform_reset_registers">
        <props name="PL_REGISTERS" type=0x00000014>
          g_pl_dtor_register_list
        </props>
    </device>
</driver>

<driver name="NULL">
  <global_def>
    <var_seq name="root_0_seq" type=0x00000002>
      0x0c, 0x09, 0x76, 0x0d, 0x72, 0xb2, 0xe9, 0x96,
      0x89, 0xf0, 0x04, 0xbf, 0x8e, 0xf0, 0xeb, 0xbb,
      0x83, 0xcf, 0x45, 0x11, 0xbd, 0xf6, 0xcc, 0xf8,
      0x38, 0x9b, 0x33, 0xa3, 0x5e, 0x42, 0xab, 0x09, end
    </var_seq>
  </global_def>
  <device id="/security">
    <props name="root_0" type=0x00000018>
      root_0_seq
    </props>
    <props name="root_0_testsig_only" type=0x00000002>
      00
    </props>
    <props name="no_default_root" type=0x00000002>
      00
    </props>
    <props name="tcg_codesig" type=0x00000002>
      123456
    </props>
    <props name="tcg_testsig" type=0x00000002>
      123456
    </props>
  </device>
</driver>

<driver name="NULL">
  <device id="/security_hwio">
    <props name="PL_SEC_HWIO" type=0x00000012>
      pl_sechwio
    </props>
  </device>
</driver>

<driver name="NULL">
    <device id="/qdsp6/sysmon">
        <props name="PREFETCH_BW_MULTIPLIER" type=0x00000002>
            14
        </props>
        <props name="DEMAND_BW_MULTIPLIER" type=0x00000002>
            25
        </props>
        <props name="WRITE_BW_MULTIPLIER" type=0x00000002>
            14
        </props>
        <props name="OVERALL_BW_MULTIPLIER" type=0x00000002>
            15
        </props>
        <props name="SNOC_BUS_WIDTH" type=0x00000002>
            16
        </props>
        <props name="NPA_SNOC_CLK_MULTIPLIER" type=0x00000002>
            2
        </props>
        <props name="HFW_WINDOW_SIZE_NON_AUDIO" type=0x00000002>
            50
        </props>
        <props name="HFW_WINDOW_SIZE_AUDIO" type=0x00000002>
            20
        </props>
        <props name="CORE_CLK_SAFE_LEVEL_AUDIO" type=0x00000002>
            70
        </props>
        <props name="CORE_CLK_DANGER_LEVEL_AUDIO" type=0x00000002>
            90
        </props>
        <props name="CORE_CLK_SAFE_LEVEL_NON_AUDIO" type=0x00000002>
            90
        </props>
        <props name="CORE_CLK_DANGER_LEVEL_NON_AUDIO" type=0x00000002>
            98
        </props>
        <props name="CORE_CLK_GO_LOW_LEVEL_AUDIO" type=0x00000002>
            68
        </props>
        <props name="CORE_CLK_GO_LOW_LEVEL_NON_AUDIO" type=0x00000002>
            88
        </props>
        <props name="CORE_CLK_GO_LOW_HYSTERESIS_ADJUST" type=0x00000002>
            10
        </props>
        <props name="CORE_CLK_GO_LOW_HYSTERESIS_MIN" type=0x00000002>
            68
        </props>
        <props name="CORE_CLK_1T_SAFE_LEVEL" type=0x00000002>
            10
        </props>
        <props name="MONITORING_DURATION_FOR_CORE_CLK_GO_DOWN" type=0x00000002>
            100
        </props>
        <props name="HFW_SAMPLES_FOR_CORE_CLK_GO_DOWN" type=0x00000002>
            0
        </props>
        <props name="HYSTERESIS_DURATION_FOR_CORE_CLK_GO_DOWN" type=0x00000002>
            100
        </props>
        <props name="HYSTERESIS_MAX_DURATION_FOR_CORE_CLK_GO_DOWN" type=0x00000002>
            400
        </props>
        <props name="MONITORING_DURATION_FOR_BUS_CLK_GO_DOWN" type=0x00000002>
            200
        </props>
        <props name="HFW_SAMPLES_FOR_BUS_CLK_GO_DOWN" type=0x00000002>
            50
        </props>
        <props name="DCVS_TRANSIENT_STATE_DURATION" type=0x00000002>
            200
        </props>
        <props name="DCVS_TRANSIENT_STATE_DURATION_MAX" type=0x00000002>
            500
        </props>
        <props name="PCPP_DANGER_LEVEL" type=0x00000002>
            10
        </props>
        <props name="PCPP_SAFE_LEVEL" type=0x00000002>
            5
        </props>
        <props name="CORE_STALLS_DANGER_LEVEL" type=0x00000002>
            35
        </props>
        <props name="BUS_PEAKS_TO_BUMPUP" type=0x00000002>
            5
        </props>
        <props name="BUS_PEAKS_OBSERVATION_WINDOW" type=0x00000002>
            20
        </props>
        <props name="BUS_PEAKS_TO_BUMPUP_COMPUTE" type=0x00000002>
            20
        </props>
        <props name="BUS_PEAKS_OBSERVATION_WINDOW_COMPUTE" type=0x00000002>
            50
        </props>
        <props name="SNOC_CLK_TABLE_DESCRIPTOR" type=0x00000012>
            bus_clk_descriptor
        </props>
        <props name="CORE_CLK_TABLE_DESCRIPTOR" type=0x00000012>
            core_clk_descriptor
        </props>
    </device>
</driver>

<driver name="NULL">
 <device id="/statichashes/libsysmon_skel.so">
  <props name="num_segments" type=0x00000002>3</props>
  <props name="hash" type=0x00000008>0xd7, 0xf8, 0x5f, 0x2, 0xd6, 0xcd, 0xa, 0x69, 0x86, 0x24, 0xb2, 0x7c, 0xef, 0x22, 0x47, 0x76, 0x6, 0x13, 0x46, 0xc6, 0x16, 0x87, 0x16, 0x47, 0xf9, 0x38, 0xa2, 0x31, 0x64, 0xef, 0x80, 0x3d, 0x79, 0x5b, 0x6e, 0x36, 0x61, 0x99, 0x30, 0xb4, 0xc1, 0x7f, 0xeb, 0x53, 0xc6, 0x6, 0x40, 0x2a, 0x93, 0xc5, 0xd6, 0xc2, 0x63, 0x41, 0x2e, 0x17, 0x41, 0x6e, 0xaa, 0xd5, 0x49, 0x4, 0xd3, 0xc3, 0xe4, 0x3c, 0x8e, 0x44, 0x73, 0x59, 0x1a, 0x29, 0xc6, 0x51, 0xf5, 0xfa, 0xac, 0xf3, 0xac, 0x3a, 0x2a, 0x1e, 0xb2, 0x65, 0x7, 0xdb, 0x50, 0xd4, 0x77, 0x7b, 0xf, 0xd7, 0xd2, 0x71, 0xc7, 0x45, end</props>
 </device>
</driver>

<driver name="NULL">
 <device id="/statichashes/fastrpc_shell_0">
  <props name="num_segments" type=0x00000002>4</props>
  <props name="hash" type=0x00000008>0x4b, 0xdb, 0x3c, 0x87, 0xb1, 0xb0, 0x71, 0x35, 0x60, 0x80, 0x47, 0xb6, 0x8a, 0xa7, 0x6b, 0x51, 0xef, 0x7f, 0xff, 0x10, 0xc0, 0xec, 0x96, 0xac, 0xff, 0x2c, 0xb8, 0x7, 0x45, 0x1a, 0xf7, 0x4c, 0xad, 0xdf, 0xbd, 0x1b, 0x7d, 0xc9, 0x73, 0xc9, 0x1e, 0xff, 0xb7, 0x21, 0xd4, 0xf6, 0xc1, 0x2b, 0xd0, 0x9c, 0x96, 0x97, 0x57, 0x69, 0xd1, 0x9, 0xd8, 0x7a, 0x98, 0x87, 0xba, 0x2, 0x64, 0x38, 0x7a, 0x39, 0xa6, 0xc7, 0xe8, 0xb5, 0xad, 0x9, 0x2b, 0x35, 0xa, 0xf0, 0x89, 0x7b, 0x39, 0x7b, 0xa4, 0xa0, 0x98, 0x8, 0x32, 0xeb, 0xd5, 0x86, 0x39, 0x2d, 0xd6, 0x2, 0x18, 0xdf, 0x1d, 0x53, 0x5f, 0x5, 0x2, 0x7c, 0x2f, 0xd4, 0x2a, 0xbb, 0x58, 0xe2, 0x5e, 0xf2, 0x27, 0x5d, 0x76, 0x9d, 0xea, 0xf4, 0x74, 0xa, 0xad, 0xbf, 0x74, 0xbe, 0x2b, 0x8b, 0x83, 0x96, 0x44, 0x9, 0x7a, 0x91, end</props>
 </device>
</driver>

<driver name="NULL">
 <device id="/statichashes/fastrpc_shell_0_debug">
  <props name="num_segments" type=0x00000002>4</props>
  <props name="hash" type=0x00000008>0x96, 0xbc, 0x31, 0xc0, 0x78, 0x97, 0x95, 0xcd, 0x6c, 0x94, 0x80, 0x9b, 0x94, 0x10, 0xaa, 0x7c, 0x7f, 0xd5, 0x69, 0xb6, 0x64, 0xfa, 0x9a, 0x3b, 0x5e, 0xb6, 0x5b, 0x49, 0x7, 0xc7, 0x74, 0x46, 0xac, 0x74, 0xe0, 0xdb, 0xe6, 0x22, 0xbc, 0xca, 0x88, 0xe1, 0x85, 0xdd, 0xf5, 0xb2, 0x2f, 0x15, 0x87, 0xda, 0xbd, 0x70, 0xac, 0x90, 0x43, 0x8a, 0x7, 0x26, 0x4e, 0xc2, 0xd3, 0x11, 0xce, 0xf6, 0x8a, 0x6d, 0xb5, 0x55, 0x56, 0xd0, 0xc, 0xb6, 0x7f, 0x69, 0x1d, 0xe4, 0xfc, 0xfe, 0xa2, 0xe4, 0xdb, 0x32, 0x8c, 0x2b, 0x9d, 0x66, 0xcf, 0x4, 0xa7, 0x8e, 0x8a, 0xf5, 0x2f, 0x44, 0x0, 0x8b, 0x97, 0x4a, 0x61, 0x40, 0x21, 0x5b, 0x53, 0x18, 0xcb, 0x92, 0xb8, 0xa5, 0x60, 0xed, 0x87, 0xb2, 0xf1, 0x54, 0xee, 0xc8, 0xe1, 0x83, 0xbb, 0x45, 0xc7, 0x1c, 0xa, 0xfa, 0x94, 0x2b, 0xa6, 0x30, end</props>
 </device>
</driver>

</module>
</dal>
