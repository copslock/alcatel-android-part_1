typedef unsigned long int uint32;
typedef unsigned short uint16;
typedef unsigned char uint8;
typedef signed long int int32;
typedef signed short int16;
typedef signed char int8;
typedef unsigned long long uint64;
typedef long long int64;
typedef unsigned char byte;
typedef uint32 DALBOOL;
typedef uint32 DALDEVICEID;
typedef uint32 DalPowerCmd;
typedef uint32 DalPowerDomain;
typedef uint32 DalSysReq;
typedef uint32 DALHandle;
typedef int DALResult;
typedef void * DALEnvHandle;
typedef void * DALSYSEventHandle;
typedef uint32 DALMemAddr;
typedef uint32 DALSYSMemAddr;
typedef uint64 DALSYSPhyAddr;
typedef uint32 DALInterfaceVersion;
typedef unsigned char * DALDDIParamPtr;
typedef struct DALEventObject DALEventObject;
struct DALEventObject
{
    uint32 obj[8];
};
typedef DALEventObject * DALEventHandle;
typedef struct _DALMemObject
{
   uint32 memAttributes;
   uint32 sysObjInfo[2];
   uint32 dwLen;
   uint32 ownerVirtAddr;
   uint32 virtAddr;
   uint32 physAddr;
}
DALMemObject;
typedef struct _DALDDIMemBufDesc
{
   uint32 dwOffset;
   uint32 dwLen;
   uint32 dwUser;
}
DALDDIMemBufDesc;
typedef struct _DALDDIMemDescList
{
   uint32 dwFlags;
   uint32 dwNumBufs;
   DALDDIMemBufDesc bufList[1];
}
DALDDIMemDescList;
typedef struct DALSysMemDescBuf DALSysMemDescBuf;
struct DALSysMemDescBuf
{
   DALSYSMemAddr VirtualAddr;
   DALSYSMemAddr PhysicalAddr;
   uint32 size;
   uint32 user;
};
typedef struct { uint32 dwObjInfo; DALSYSMemAddr thisVirtualAddr; DALSYSMemAddr PhysicalAddr; DALSYSMemAddr VirtualAddr; uint32 hOwnerProc; uint32 dwCurBufIdx; uint32 dwNumDescBufs; DALSysMemDescBuf BufInfo[1];} DALSysMemDescList;
typedef enum
{
    BAM_SUCCESS = 0x0,
    BAM_FAILED = 0x1,
    BAM_INVALID_CONFIG = 0x2,
    BAM_INVALID_BUFSIZE = 0x3,
    BAM_POLL_NO_RESULT = 0x4,
    BAM_PIPE_DISABLED = 0x5,
    BAM_PIPE_NOT_EMPTY = 0x6,
    BAM_NOT_ENOUGH_SPACE = 0x7
}bam_status_type;
typedef enum
{
   BAM_O_DESC_DONE = 0x00000001,
   BAM_O_INACTIVE = 0x00000002,
   BAM_O_WAKEUP = 0x00000004,
   BAM_O_OUT_OF_DESC = 0x00000008,
   BAM_O_ERROR = 0x00000010,
   BAM_O_EOT = 0x00000020,
   BAM_O_RST_ERROR = 0x00000040,
   BAM_O_HRESP_ERROR = 0x00000080,
   BAM_O_STREAMING = 0x00010000,
   BAM_O_POLL = 0x001000000,
   BAM_O_NO_Q = 0x002000000,
   BAM_O_WAKEUP_IS_ONESHOT = 0x004000000,
   BAM_O_ACK_TRANSFERS = 0x00800000,
   BAM_O_AUTO_ENABLE = 0x01000000,
   BAM_O_ACK_LAST_ONLY = 0x02000000,
   BAM_O_PIPE_NO_INIT = 0x04000000,
   BAM_O_IS_32_BIT = 0x7fffffff
} bam_options_type;
typedef enum
{
   BAM_EVENT_INVALID = 0,
   BAM_EVENT_EOT,
   BAM_EVENT_DESC_DONE,
   BAM_EVENT_OUT_OF_DESC,
   BAM_EVENT_WAKEUP,
   BAM_EVENT_FLOWOFF,
   BAM_EVENT_INACTIVE,
   BAM_EVENT_ERROR,
   BAM_EVENT_HRESP_ERROR,
   BAM_EVENT_RST_ERROR,
   BAM_EVENT_EMPTY,
   BAM_EVENT_IN_PROGRESS,
   BAM_EVENT_DESC_PROCESSED,
   BAM_EVENT_MAX,
   BAM_EVENT_IS_32_BIT = 0x7fffffff
} bam_event_type;
typedef enum
{
   BAM_IOVEC_FLAG_INT = 0x800,
   BAM_IOVEC_FLAG_EOT = 0x400,
   BAM_IOVEC_FLAG_EOB = 0x200,
   BAM_IOVEC_FLAG_NWD = 0x100,
   BAM_IOVEC_FLAG_CMD = 0x080,
   BAM_IOVEC_FLAG_LCK = 0x040,
   BAM_IOVEC_FLAG_UNLCK = 0x020,
   BAM_IOVEC_FLAG_IMM_CMD = 0x010,
   BAM_IOVEC_FLAG_NO_SUBMIT = 0x008,
   BAM_IOVEC_FLAG_DEFAULT = 0x001
}bam_iovec_options_type;
typedef enum
{
    BAM_DIR_PRODUCER = 0x1,
    BAM_DIR_CONSUMER = 0x0
}bam_dir_type;
typedef enum
{
  BAM_MODE_BAM2BAM = 0x0,
  BAM_MODE_SYSTEM = 0x1
}bam_mode_type;
typedef enum
{
    BAM_IRQ_HRESP_ERR_EN = 0x2,
    BAM_IRQ_ERR_EN = 0x4,
    BAM_IRQ_EMPTY_EN = 0x8,
    BAM_IRQ_TIMER_EN = 0x10
}bam_irq_options_type;
typedef enum
{
   BAM_TIMER_OP_CONFIG = 0,
   BAM_TIMER_OP_RESET,
   BAM_TIMER_OP_READ,
   BAM_TIMER_OP_IS_32_BIT = 0x7fffffff
} bam_timer_oper_type;
typedef enum
{
    BAM_TIMER_MODE_ONESHOT = 0,
    BAM_TIMER_MODE_IS_32_BIT = 0x7fffffff
} bam_timer_mode_type;
typedef enum
{
    BAM_CE_OP_WRITE = 0,
    BAM_CE_OP_READ = 1
}bam_ce_oper_type;
typedef enum
{
    BAM_DUMP_TOP = 0x1,
    BAM_DUMP_PIPE = 0x2
}bam_dump_type;
typedef enum
{
    BAM_INVOKE_CB_DEFAULT = 0,
    BAM_INVOKE_CB_IN_POLL = 1
}bam_cb_mode_type;
typedef DALSYSMemAddr bam_vaddr;
typedef DALSYSMemAddr bam_paddr;
typedef struct _bamconfig
{
  bam_paddr bam_pa;
  bam_vaddr bam_va;
  uint32 bam_irq;
  uint32 bam_irq_mask;
  uint32 sum_thresh;
  uint32 options;
  uint32 bam_mti_irq_pa;
} bam_config_type;
typedef struct _PipeConfig
{
      uint32 options ;
      bam_dir_type dir;
      bam_mode_type mode;
      bam_vaddr desc_base_va;
      bam_paddr desc_base_pa;
      uint32 desc_size;
      uint16 evt_thresh;
      uint32 lock_group;
      bam_paddr peer_base_pa;
      bam_paddr data_base_pa;
      uint32 peer_pipe_num;
      bam_vaddr data_base_va;
      uint32 data_size;
}bam_pipe_config_type;
typedef struct _coredumpconfig
{
    void *user_buffer;
    uint32 buf_size;
    uint32 pipe_mask;
    uint32 peer_pipe_mask;
    void *descfifo_buf;
    uint32 descfifo_size;
    void *datafifo_buf;
    uint32 datafifo_size;
    void *option;
} bam_coredump_config_type;
typedef struct
{
  uint32 buf_pa;
  uint32 buf_size : 16;
  uint32 buf_pa_msb :4;
  uint32 flags : 12;
} bam_iovec_type;
typedef struct
{
    uint32 reg_addr : 24;
    uint32 command : 8;
    uint32 data ;
    uint32 mask;
    uint32 reserved;
} bam_ce_type;
typedef struct _bam_result_type
{
    void *cb_data;
    bam_event_type event;
    union
    {
       struct
       {
           bam_iovec_type iovec;
           uint32 accum_size_bytes;
           void *xfer_cb_data;
       } xfer;
      struct
      {
         uint32 status;
         bam_paddr address;
         uint32 data;
         uint32 bus_ctrls;
      } error;
   } data;
   bam_cb_mode_type cb_mode;
} bam_result_type;
typedef DALSYSMemAddr bam_handle;
typedef void (*bam_callback_func_type)(bam_result_type bam_result);
typedef struct
{
    bam_callback_func_type func;
    void *data;
}bam_callback_type;
typedef struct
{
    bam_timer_oper_type op;
    bam_timer_mode_type mode;
    uint32 timeout_ms;
} bam_timer_ctrl_type;
typedef struct
{
    uint32 curr_timer;
} bam_timer_rslt_type;
typedef struct
{
    uint32 num_pipes;
    uint32 bam_header_buffer_size;
    uint32 bam_buffer_size;
    uint32 bam_pipe_buffer_size;
    uint32 bam_max_desc_size;
}bam_info_type;
void bam_fill_ce(bam_vaddr ce_base, uint32 index ,bam_paddr reg, uint32 cmd, uint64 data, uint32 mask);
bam_status_type bam_get_info(bam_handle bamhandle, bam_info_type *info);
bam_handle bam_init( bam_config_type *bam_cfg, bam_callback_type *bam_cb);
bam_status_type bam_secure_init(bam_config_type *bam_cfg);
bam_status_type bam_deinit(bam_handle handle, uint8 disable_bam);
bam_status_type bam_reset(bam_handle handle);
bam_status_type bam_setirqmode(bam_handle bamhandle, uint32 irq_en,uint32 irq_mask);
bam_status_type bam_pipe_isirqmode(bam_handle pipehandle, uint32* irq_en);
bam_handle bam_pipe_init(bam_handle bamhandle, uint32 pipe_num, bam_pipe_config_type *pipe_cfg, bam_callback_type *pipe_cb);
bam_status_type bam_pipe_deinit(bam_handle pipehandle);
bam_status_type bam_pipe_enable(bam_handle pipehandle);
bam_status_type bam_pipe_disable(bam_handle pipehandle);
bam_status_type bam_pipe_setirqmode(bam_handle pipehandle, uint32 irq_en, uint32 pipe_cfg_opts);
bam_status_type bam_pipe_transfer(bam_handle pipehandle, bam_paddr buf_pa, uint16 buf_size, uint16 xfer_opts, void *user_data);
bam_status_type bam_pipe_bulktransfer(bam_handle pipehandle, bam_paddr buf_pa, uint32 buf_size, uint16 xfer_opts, void *user_data);
uint32 bam_pipe_getfreecount(bam_handle pipehandle);
uint32 bam_pipe_isempty(bam_handle pipehandle);
bam_status_type bam_pipe_trigger_event(bam_handle pipehandle);
bam_status_type bam_pipe_trigger_zlt_event(bam_handle pipehandle);
bam_status_type bam_pipe_trigger_event_sys(bam_handle pipehandle);
bam_status_type bam_pipe_poll(bam_handle pipehandle, bam_result_type *result);
bam_status_type bam_pipe_get_current_descriptor_info(bam_handle pipehandle, bam_result_type *result);
bam_status_type bam_pipe_timerctrl(bam_handle pipehandle, bam_timer_ctrl_type *timer_ctrl, bam_timer_rslt_type *timer_rslt);
bam_status_type bam_core_dump(bam_handle bamhandle, bam_coredump_config_type *coredump_cfg);
typedef struct
{
   uint32 pipe_mask;
   uint32 vmid;
   uint32 pipe_mask_ac;
   uint32 vmid_ac;
} bam_ee_sec_config_type;
typedef struct
{
   uint32 pipe_mask;
} bam_sg_sec_config_type;
typedef struct
{
   bam_ee_sec_config_type ee[8];
   bam_sg_sec_config_type sg[4];
   uint32 ctrl_vmid;
   uint32 bam_irq_ee;
} bam_sec_config_type;
typedef struct _bamtgtcfg{
  uint32 bam_pa;
  uint32 options;
  uint32 cfg_bits;
  uint8 ee;
  bam_sec_config_type *sec_config;
  uint32 size;
}bam_target_config_type;
const bam_target_config_type* bam_tgt_getcfg(uint32 bam_pa);
uint32 bam_tgt_getnumdevs(void);
uint32 bam_tgt_getpipelockgroup(uint32 bam_pa, uint32 pipe_num);
void bam_tgt_initpipemem(void);
uint32 bam_tgt_getpipememstartaddr(void);
uint32 bam_tgt_getpipememendaddr(void);
void* bam_tgt_getpipememptaddr(void);
bam_sec_config_type bam_tgt_slimbus_aud_secconfig_8996 =
{
                    {
                                        {
                                            0x1fffffE7,
                                            0x0,
                                            0x0,
                                            0x0
                                        },
                                        {
                                            0x60000018,
                                            0x0,
                                            0x0,
                                            0x0
                                        },
                                        {
                                            0x00000000,
                                            0x0,
                                            0x0,
                                            0x0
                                         }
                    },
                    {{0x0}},
                    0x0,
                    0x0
};
bam_sec_config_type bam_tgt_slimbus_qca_secconfig_8996 =
{
                      {
                                            {
                                                0x0007ffE7,
                                                0x0,
                                                0x0,
                                                0x0
                                            },
                                            {
                                                0x00000018,
                                                0x0,
                                                0x0,
                                                0x0
                                            },
                                            {
                                                0x00000000,
                                                0x0,
                                                0x0,
                                                0x0
                                            }
                      },
                      {{0x0}},
                      0x0,
                      0x0
};
const bam_target_config_type bam_tgt_config[] = {
    {
                         0x7544000,
                         (0x2),
                         0xFFFFF004,
                         1,
                         0,
                         0x40000
    },
    {
                         0x7584000,
                         (0x2),
                         0xFFFFF004,
                         1,
                         0,
                         0x40000
    },
    {
                         0x9184000,
                         0x0,
                         0xFFFFF004,
                         0,
                         &bam_tgt_slimbus_aud_secconfig_8996,
                         0x40000
    },
    {
                         0x9204000,
                         0x0,
                         0xFFFFF004,
                         0,
                         &bam_tgt_slimbus_qca_secconfig_8996,
                         0x40000
    },
    {
                         0x644000,
                         0x0,
                         0xFFFFF004,
                         1,
                         0,
                         0x40000
    },
    {
                         0x0,
                         0,
                         0,
                         0,
                         0,
                         0
    },
};
