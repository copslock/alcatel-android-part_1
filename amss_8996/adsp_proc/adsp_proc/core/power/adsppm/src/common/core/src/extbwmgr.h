/*
* Copyright (c) 2015 Qualcomm Technologies, Inc.
* All Rights Reserved.
* Qualcomm Technologies, Inc. Confidential and Proprietary.
*/

/*
@file: extbwmgr.h
@brief: Header for ADSPPM External BW Manager.

$Header: //components/rel/core.adsp/2.7/power/adsppm/src/common/core/src/extbwmgr.h#2 $
*/

#ifndef _EXTBWMGR_H_
#define _EXTBWMGR_H_

#include "adsppm.h"

/**
 * @fn ExtBwMgr_Init
 * @brief Initialize the External BW Manager
 *        Requires that the DCVS Manager is already initialized
 */
Adsppm_Status ExtBwMgr_Init(void);

/**
 * @fn ExtBwMgr_IssueExtBwRequest
 * @brief Aggregates current external BW values with requests from SysMon DCVS
 */
Adsppm_Status ExtBwMgr_IssueExtBwRequest(uint32 numUpdatedExtBwVotes,
    AdsppmBusBWRequestValueType* pUpdatedExtBusBwValues,
    AdsppmBusBWDataIbAbType* dcvsFloorVote);

/**
 * @fn ExtBwMgr_GetInfo
 * @brief Get DCVS ADSP to DDR BW aggregation information for testing purposes
 */
Adsppm_Status ExtBwMgr_GetInfo(
    AdsppmInfoDcvsAdspDdrBwType* pDcvsAdspDdrBwInfo);


#endif /* _EXTBWMGR_H_ */

