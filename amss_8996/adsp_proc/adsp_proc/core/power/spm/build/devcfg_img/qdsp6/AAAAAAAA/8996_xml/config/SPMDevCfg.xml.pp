# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/spm/config/SPMDevCfg.xml"
# 1 "<built-in>" 1
# 1 "<built-in>" 3
# 140 "<built-in>" 3
# 1 "<command line>" 1
# 1 "<built-in>" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/power/spm/config/SPMDevCfg.xml" 2
<!--
  This XML file contains target specific information for SPM
  driver for targets using device config feature.

  Copyright (c) 2012 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary
-->

<driver name="NULL">
  <device id="/dev/core/power/spm">
    <props name="spm_num_cores" type=DALPROP_ATTR_TYPE_UINT32>
      1
    </props>

    <props name="spm_bsp_data" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
      devcfgSpmBspData
    </props>

    <props name="spm_cmd_seq_info_array" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
      devcfgSpmCmdSeqArray
    </props>
  </device>
</driver>
