#ifndef SLEEPACTIVE_H
#define SLEEPACTIVE_H
/*==============================================================================
  FILE:         sleepActive.h

  OVERVIEW:     This file contains public APIs exposed by the Sleep Active-time 
                Solver Thread.

  DEPENDENCIES: None

                Copyright (c) 2014-2015 Qualcomm Technologies, Inc. (QTI).
                All Rights Reserved.
                Qualcomm Technologies Confidential and Proprietary
================================================================================
$Header: //components/rel/core.adsp/2.7/power/sleep2.0/inc/sleepActive.h#2 $
$DateTime: 2015/07/16 09:52:19 $
==============================================================================*/
#include "sleep_solver.h"

/*==============================================================================
                                GLOBAL TYPE DECLARATIONS
  =============================================================================*/ 
/* List of Signals that cause the Active Time solver 
 * to re-evaluate its selection of low-power modes */
typedef enum
{
  SLEEP_SIGNAL_CONTROL         = ( 1 << 0 ),    /* Enable/Disable Active Time Solver  */
  SLEEP_SIGNAL_LATENCY         = ( 1 << 1 ),    /* Update in Latency Constraint       */
  SLEEP_SIGNAL_SOFT_DURATION   = ( 1 << 2 ),    /* Update Idle Soft Duration          */
  SLEEP_SIGNAL_REGISTRY        = ( 1 << 3 ),    /* Update List of Enabled Synth Modes */
  SLEEP_SIGNAL_CPU_FREQUENCY   = ( 1 << 4 ),    /* Cpu Frequency Change               */
  SLEEP_SIGNAL_HARD_DURATION   = ( 1 << 5 ),    /* Update idle hard duration          */
  SLEEP_SIGNAL_MASK_ALL        = ( 1 << 6 ) - 1 /* Mask for all signals               */
} sleepActive_SignalType;

/*==============================================================================
                              GLOBAL FUNCTION DECLARATIONS
  =============================================================================*/
/**
 * @brief Trigger signal(s) of type sig on Active Time solver thread
 *
 *        Signals set on the Active Time solver are used to determine 
 *        which execution conditions that have changed. 
 *        The Active Time solver thread executes the sleep solver to determine
 *        whether these modified inputs cause a change in the selection of 
 *        low-power mode.
 * 
 * @param signals: A single or bitwise OR of specific signals to be set
 *                 for Active Time solver to take action on
 */
void sleepActive_SetSignal(sleepActive_SignalType signals);

/**
 * @brief Returns the hard deadline times (in absolute ticks) that 
 *        were used by the solver to select the low power mode 
 *  
 * @return A pointer to the deadline data structure
 */
const sleep_solver_deadlines *sleepActive_GetSolverHardDeadlines(void);

#endif /* SLEEPACTIVE_H */

