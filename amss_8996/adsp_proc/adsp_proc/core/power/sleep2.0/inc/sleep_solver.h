#ifndef SLEEP_SOLVER_H
#define SLEEP_SOLVER_H
/*==============================================================================
  FILE:         sleep_solver.h
  
  OVERVIEW:     This file declares the public types and functions to access data
                from the main sleep solver.

  DEPENDENCIES: None

                Copyright (c) 2015 Qualcomm Technologies, Inc. (QTI).
                All Rights Reserved.
                Qualcomm Technologies Confidential and Proprietary
================================================================================
$Header: //components/rel/core.adsp/2.7/power/sleep2.0/inc/sleep_solver.h#1 $
$DateTime: 2015/07/16 09:52:19 $
==============================================================================*/
#include "DALStdDef.h"

/*==============================================================================
                          GLOBAL TYPE DEFINITIONS
 =============================================================================*/
 /**
 * @brief sleep_solver_deadlines
 * 
 * Data structure to hold the various hard deadlines that the solver will use to 
 * select a low power mode. 
 */
typedef struct sleep_solver_deadlines_s
{
  uint64  island;   /* Minimum of all island mode specific deadlines */
  uint64  normal;   /* Minimum of all normal mode specific deadlines */
  uint64  minimum;  /* Minimum of all island & normal mode deadlines */
} sleep_solver_deadlines;

#endif /* SLEEP_SOLVER_H */

