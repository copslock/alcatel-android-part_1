/*==============================================================================
  FILE:         sleep_npa.c
  
  OVERVIEW:     This file provides the NPA node definitions for latency and
                wakeup nodes.

  DEPENDENCIES: None

                Copyright (c) 2010-2015 Qualcomm Technologies, Inc. (QTI).
                All Rights Reserved.
                Qualcomm Technologies Confidential and Proprietary
================================================================================
$Header: //components/rel/core.adsp/2.7/power/sleep2.0/src/npa_nodes/sleep_npa.c#5 $
$DateTime: 2015/07/16 09:52:19 $
==============================================================================*/
#ifndef FEATURE_SLEEP_NO_WAKEUP_NODE
#include "CoreTime.h"
#include "sleep_log.h"
#include "sleep_utils.h"
#endif /* !FEATURE_SLEEP_NO_WAKEUP_NODE */

#if !defined( FEATURE_SLEEP_NO_WAKEUP_NODE )    || \
    !defined( FEATURE_SLEEP_NO_LATENCY_NODE )   || \
    !defined( FEATURE_SLEEP_NO_MAX_DURATION_NODE )
#include "CoreVerify.h"
#include "CoreString.h"
#include "sleep_lpr.h"
#include "npa_resource.h"
#include "npa_log.h"
#include "sleepi.h"
#include "sleep_os.h"
#include <stdio.h>
#include <stdlib.h>
#endif /* !FEATURE_SLEEP_NO_WAKEUP_NODE */
       /* || !FEATURE_SLEEP_NO_LATENCY_NODE */
       /* || !FEATURE_SLEEP_NO_MAX_DURATION_NODE */

#if !defined( FEATURE_SLEEP_NO_LATENCY_NODE )  || \
    !defined( FEATURE_SLEEP_NO_MAX_DURATION_NODE )
#include "sleep_idle_plugin.h"
#include "sleepActive.h"
#endif /* !FEATURE_SLEEP_NO_LATENCY_NODE */
       /* || !FEATURE_SLEEP_NO_MAX_DURATION_NODE */

#ifndef FEATURE_SLEEP_NO_MAX_DURATION_NODE
/*==============================================================================
                         SLEEP MAX DURATION NODE DEFINITION
 =============================================================================*/
#define MAX_DURATION_TICKS "/sleep/max_duration"
#define MAX_DURATION_USEC  "/sleep/max_duration/usec"

/**
 * maxDurationResourceData_createClientFcn
 *
 * @brief In order to specify to driver function the untis chosen 
 *        by the client to this resource
 *
 * Custom client function to determine units specified by client
 *
 * @param client: The handle to the client registered to the 
 *                resource.
 *
 * @return None
 */
void maxDurationResourceData_createClientFcn(npa_client *client)
{
  if(strncmp(client->resource_name, MAX_DURATION_USEC,
                   strlen(MAX_DURATION_USEC)) == 0)
  {
    /* client data in usec */
    client->resource_data = (void *)1;
  }
  else
  {
    /* client data in ticks */
    client->resource_data = (void *)0;
  }
}

/**
 * sleepNPA_maxDurationUpdate
 *
 * @brief This function is invoked by the /sleep/max_duration resource
 *        when a client request is made.
 *
 * Determines the new state of the resource by calculating max of the
 * clients' requests 
 *
 * @param resource: The NPA resource being requested (should be 
 *                  /sleep/max_duration).
 * @param client: The handle to the clients registered to the 
 *                resource.
 *
 * @return Returns the new state of the resource.
 *
 */
npa_resource_state sleepNPA_maxDurationUpdate(npa_resource *resource, 
                                              npa_client_handle client)
{
  static npa_resource_state req_state = 0;
  npa_resource_state request;

  CORE_VERIFY_PTR(resource);
  CORE_VERIFY_PTR(client);

  if (client->resource_data == (void *)1)
  {
    /* convert usec to ticks */
    request = NPA_PENDING_REQUEST(client).state;
    NPA_PENDING_REQUEST(client).state = US_TO_TICKS( request );
  }

  /* Client is requesting a specific point at which we should be awake.
   * Take the min of the client votes.  This will return the min of the
   * required and suppressible clients. */
  req_state = npa_min_plugin.update_fcn( resource, client );

  return req_state;
}

/**
 * sleepNPA_maxDurationDriver
 *
 * @brief Driver function for the /sleep/max_duration resource 
 *        that updates its state.
 *
 * @param resource: Pointer to the /sleep/max_duration resource
 * @param client: Client of the resource
 * @param state: State of the resource.
 *
 * @return state of the resource.
 */
static npa_resource_state sleepNPA_maxDurationDriver(npa_resource *resource,
                                                     npa_client *client,
                                                     npa_resource_state state)
{
  /* Record the state so it can be queried from the sleep task before we go
   * to sleep. */
  if(0 == state)
  {  
    state = NPA_MAX_STATE;
  }

  /* There is a race condition where ATS may finish the query before NPA 
   * framework can process the return vlaue from driver causing ATS to use 
   * stale values. Until NPA has support for synchronous POST_CHANGE 
   * callbacks, update the final calculated state in separate field 
   * as recommended by NPA team - use this field for query purpose */
  resource->definition->data = (npa_user_data)state;

  /* Trigger Active Timer Solver -- max_duration is updated */
  sleepActive_SetSignal( SLEEP_SIGNAL_SOFT_DURATION );
  
  return state;
}

/**
 * sleepNPA_maxDurationQuery
 *
 * @brief Query function for the /sleep/max_duration resource that returns
 *        soft wakeup time
 *
 * @param resource: Resource to which we are making request (max_duration node)
 * @param query_id: ID for the query being made.
 * @param query_result: Stores to the result of the query
 *
 * @return Returns the status for the query (i.e. successfully handled or 
 *         unsupported).
 */
npa_query_status sleepNPA_maxDurationQuery
(
  npa_link *resource_link,
  unsigned int query_id, 
  npa_query_type *query_result
)
{
  npa_query_status status = NPA_QUERY_SUCCESS;
  npa_resource_state retVal;

  CORE_VERIFY_PTR(resource_link);
  CORE_VERIFY_PTR(query_result);

  switch(query_id)
  {
    case NPA_QUERY_CURRENT_STATE:
    case SLEEP_QUERY_MAX_DURATION:
    {
      retVal = (npa_resource_state)resource_link->resource->definition->data;
      if(strncmp(resource_link->name, MAX_DURATION_USEC,
                   strlen(MAX_DURATION_USEC)) == 0)
      {
        /* client request in usec, so return in usec */
        retVal = TICKS_TO_US(retVal);
      }
      query_result->data.value = retVal;
      query_result->type = NPA_QUERY_TYPE_VALUE;
      break;
    }
    default:
    {
      status = NPA_QUERY_UNSUPPORTED_QUERY_ID;
      break;
    }
  }

  return status;
}

/**
 * @brief g_sleepMaxDurationPlugin
 *
 * The plugin for the /sleep/max_duration resource.
 */
const npa_resource_plugin g_sleepMaxDurationPlugin = 
{
  sleepNPA_maxDurationUpdate,                 /* Update function */
  NPA_CLIENT_REQUIRED,                        /* Supported client types */
  maxDurationResourceData_createClientFcn,    /* Create client function */
  NULL                                        /* Destroy client function */
};

/**
 * @brief g_sleepMaxDurationResource
 *
 * Definition of the max duration resource. 
 */
static npa_resource_definition g_sleepMaxDurationResource[] = 
{ 
  {  
    "/sleep/max_duration",                    /* Name */
    "ticks",                                  /* Units - usec for alias */
    NPA_MAX_STATE,                            /* Max State */
    &g_sleepMaxDurationPlugin,                /* Plugin */
    NPA_RESOURCE_INTERPROCESS_ACCESS_ALLOWED, /* Attributes */
    NULL,                                     /* User Data */
    NULL,                                     /* Query Function */
    sleepNPA_maxDurationQuery                 /* Query Link Function */
  }
};

/**
 * @brief g_sleepMaxDurationNode
 *
 * Definition of the max duration node. 
 */
static npa_node_definition g_sleepMaxDurationNode = 
{ 
  "/node/sleep/max_duration",                 /* Name */
  sleepNPA_maxDurationDriver,                 /* Driver_fcn */
  NPA_NODE_DEFAULT,                           /* Attributes */
  NULL,                                       /* Data */
  NPA_EMPTY_ARRAY,                            /* Dependencies */
  NPA_ARRAY(g_sleepMaxDurationResource)       /* Resource Array */
};

#endif /* FEATURE_SLEEP_NO_MAX_DURATION_NODE */

#ifndef FEATURE_SLEEP_NO_WAKEUP_NODE
/*==============================================================================
                         SLEEP WAKEUP NODE DEFINITION
 =============================================================================*/
/**
 * @brief g_wakeupResourceMin64
 *
 * Minimum absolute of the set of clients on wakeup node resource
 */
static uint64 g_wakeupResourceMin64;

/**
 * @brief g_wakeupResourceMinClientHandle
 *
 * Client handle whose request is the currently set minimum
 *
 */
static npa_client *g_wakeupResourceMinClientHandle;

/**
 * @brief wakeupUserData
 *
 * npa_user_data is defined as void* and wakeup node defines this 
 * struct to send as resource_data
 *
 */
typedef struct wakeupResourceData_s
{
  uint64 data;
}wakeupUserData;

/**
 * @brief sleepNPA_wakeupResource_aggregate
 *
 * Aggregates minimum of all client requests
 *
 * @param resource: The NPA resource being requested (should be 
 *                  /core/cpu/wakeup).
 * @param client: The handle to the client registered to the 
 *                resource.
 *
 * @return Returns the new minimum of the resource 
 */
static void sleepNPA_wakeupResource_aggregate( npa_resource *resource, 
                                               npa_client *client )
{
  npa_client_handle activeClient = resource->clients;
  wakeupUserData *dataPtr;

  /* First reset minimum and minimum client handle */
  g_wakeupResourceMin64           = UINT64_MAX;
  g_wakeupResourceMinClientHandle = NULL;

  /* Traverse through the list of clients to find the minimum */
  while (activeClient != NULL)
  {
    dataPtr = (wakeupUserData *)activeClient->resource_data;

    /* Get the minimum value from clients */
    if (dataPtr->data < g_wakeupResourceMin64)
    {
      g_wakeupResourceMin64           = dataPtr->data;
      g_wakeupResourceMinClientHandle = activeClient;
    }
    activeClient = activeClient->next;
  }

  if (g_wakeupResourceMin64 <= CoreTimetick_Get64())
  {
    /* Node clients are responsible to cancel requests placed on node
     * when use-case completes and cannot have requests that are in past */
    sleepLog_printf(SLEEP_LOG_LEVEL_INFO, 1, 
                    "Client (handle: 0x%08x) - request on "
                    "wakeup node is in the past",
                    g_wakeupResourceMinClientHandle);
  }

  return;
}

/**
 * wakeupResourceData_createClientFcn
 *
 * @brief In order to store values into the resource-data field, that will
 *        need to be assigned necessary memory
 *
 * Allocating memory to store address to 64-bit absolute time
 *
 * @param client: The handle to the client registered to the 
 *                resource.
 *
 * @return None
 */
void wakeupResourceData_createClientFcn( npa_client *client )
{
  wakeupUserData *clientReq;

  /* Allocate storage space because NPA discards the data otherwise */
  client->resource_data = (wakeupUserData *)
                                malloc(sizeof(wakeupUserData));
  
  CORE_VERIFY_PTR(client->resource_data);

  /* get the address where 64-bit address is/should be placed & set to max */
  clientReq = (wakeupUserData *)client->resource_data;
  clientReq->data = UINT64_MAX;
}

/**
 * wakeupResourceData_destroyClientFcn
 *
 * @brief Free memory allocated to resource-data if client requests
 *        itself to be destroyed via npa_destroy_client
 *
 * Calling into npa_destroy_client would make NPA FW to call into the
 * plugin's destroy client fcn
 *
 * @param client: The handle to the client registered to the 
 *                resource.
 *
 * @return None
 */
void wakeupResourceData_destroyClientFcn( npa_client *client )
{
  /* Free the memory allocated for the client's data */
  free(client->resource_data);
}

/**
 * sleepNPA_wakeupUpdate
 *
 * @brief This function is invoked by the /core/cpu/wakeup resource when a 
 *        client request is made.
 *
 * Its job is to compute the expected wakeup time based on input from clients
 * so that sleep can better estimate what modes it should enter. 
 *
 * @param resource: The NPA resource being requested (should be 
 *                  /core/cpu/wakeup).
 * @param client: The handle to the clients registered to the 
 *                resource.
 *
 * @return Returns the new state of the resource ('zero' until 64-bit NPA
 *         support becomes available).
 */
static npa_resource_state sleepNPA_wakeupUpdate( npa_resource       *resource, 
                                                 npa_client_handle  client)
{
  wakeupUserData      *clientReq;
  npa_resource_state  newReq;
  uint64              now = CoreTimetick_Get64();

  CORE_VERIFY_PTR(resource);
  CORE_VERIFY_PTR(client);

  /* Get the relative requested duration */
  newReq = NPA_PENDING_REQUEST(client).state;

  /* get the address where 64-bit address is/should be placed */
  clientReq = (wakeupUserData *)client->resource_data;

  /* Check if this is a cancel request or destroy client request
   * - in both of those cases, the requested value is sent as zero */
  if (newReq == 0)
  {
    /* Set MAX for the current client's data */
    clientReq->data = UINT64_MAX;
  }
  else
  {
    /* add current time and store into data field.
     * Since newReq is a offset from the client, use the time from the beginning
     * of this call rather than the current time at this point */
    clientReq->data = (uint64)(newReq + now);
  }

  if (clientReq->data < g_wakeupResourceMin64)
  {
    /* Find minimum duration at which wakeup needs to occur. If
     * first client or client's request is less than current minimum,
     * set client as new minimum */
    g_wakeupResourceMin64           = clientReq->data;
    g_wakeupResourceMinClientHandle = client;
  }
  else if (g_wakeupResourceMinClientHandle == client)
  {
    /* Re-aggregate the new minimum */
    sleepNPA_wakeupResource_aggregate(resource, client);
  }

  return 0;
}

/**
 * sleepNPA_wakeupDriver
 *
 * @brief Driver function for the /core/cpu/wakeup resource that updates its
 *        state.
 *
 * @param resource: Pointer to the /core/cpu/wakeup resource
 * @param client: Client of the resource
 * @param state: state of the resource.
 *
 * @return state of the resource.
 */
static npa_resource_state sleepNPA_wakeupDriver (npa_resource       *resource,
                                                 npa_client         *client,
                                                 npa_resource_state state)
{
  CORE_VERIFY_PTR(resource);
  CORE_VERIFY_PTR(client);

  if(client->type == NPA_CLIENT_INITIALIZE)
  {
    resource->internal_state[0]     = (npa_resource_state)NULL;
    g_wakeupResourceMin64           = UINT64_MAX;
    g_wakeupResourceMinClientHandle = NULL;
    return 0;
  }

  return state;
}

/**
 * sleepNPA_wakeupQuery
 *
 * @brief Query function for the /core/cpu/wakeup resource that returns
 *        soft wakeup time based on the query id
 *
 * @param resource: Resource to which we are making request (wakeup node here).
 * @param query_id: ID for the query being made.
 * @param query_result: Stores to the result of the query
 *
 * @return Returns the status for the query (i.e. successfully handled or 
 *         unsupported).
 */
npa_query_status sleepNPA_wakeupQuery
(
  struct npa_resource *resource, 
  unsigned int query_id, 
  npa_query_type *query_result
)
{
  npa_query_status status = NPA_QUERY_SUCCESS;

  CORE_VERIFY_PTR(resource);
  CORE_VERIFY_PTR(query_result);

  switch(query_id)
  {
    case SLEEP_QUERY_WAKEUP_TIME:
    {
      uint64 now = CoreTimetick_Get64();

      /* return 32-bit value as soft-duration */
      query_result->type = NPA_QUERY_TYPE_VALUE;

      if(g_wakeupResourceMin64 == UINT64_MAX)
      {
        /* return query result as UINT32_MAX */
        query_result->data.value = UINT32_MAX;
      }
      else if (g_wakeupResourceMin64 <= now)
      {
        /* Node clients are responsible to cancel requests placed on node
         * when use-case completes, and cannot have requests in past. */
        sleepLog_printf(SLEEP_LOG_LEVEL_INFO, 1, 
                        "Client (handle: 0x%08x) "
                        "- request on wakeup node is in the past",
                        g_wakeupResourceMinClientHandle);

        /* return zero as soft-duration */
        query_result->data.value = 0;
      }
      else
      {
        query_result->data.value = safe_unsigned_truncate(g_wakeupResourceMin64 - now); 
      }
      break;
    }

    case SLEEP_QUERY_ABS_WAKEUP_TIME:
    {
      /* Return 64-bit value as soft-duration */
      query_result->type = NPA_QUERY_TYPE_VALUE64;

      /* Set return value to minimum wakeup time */
      query_result->data.value64 = g_wakeupResourceMin64;

      /* Verify that the minimum is in the future */
      if (g_wakeupResourceMin64 <= CoreTimetick_Get64())
      {
        /* Node clients are responsible to cancel requests placed on node
         * when use-case completes, and cannot have requests in past. */
        sleepLog_printf(SLEEP_LOG_LEVEL_INFO, 1, 
                        "Client (handle: 0x%08x) " 
                        "- request on ABS wakeup node is in the past",
                         g_wakeupResourceMinClientHandle);
      }
      break;
    }

    default:
    {
      status = NPA_QUERY_UNSUPPORTED_QUERY_ID;
      break;
    }
  }
  return status;
}

/**
 * @brief g_sleepWakeupPlugin
 *
 * The plugin for the /core/cpu/wakeup resource.
 */
const npa_resource_plugin g_sleepWakeupPlugin = 
{
  sleepNPA_wakeupUpdate,                  /* Update function */
  NPA_CLIENT_REQUIRED,                    /* Supported client types */
  wakeupResourceData_createClientFcn,     /* Create client function */
  wakeupResourceData_destroyClientFcn     /* Destroy client function */
};

/**
 * @brief g_sleepWakeupResource
 *
 * Expected wakeup pseudo-resource. 
 */
static npa_resource_definition g_sleepWakeupResource[] = 
{ 
  {  
    "/core/cpu/wakeup",                        /* Name */
    "ticks",                                   /* Units */
    NPA_MAX_STATE,                             /* Max State */
    &g_sleepWakeupPlugin,                      /* Plugin */
    NPA_RESOURCE_INTERPROCESS_ACCESS_ALLOWED,  /* Attributes */
    NULL,                                      /* User Data */
    sleepNPA_wakeupQuery                       /* Query Function */
  }
};

/**
 * @brief g_sleepWakeupNode
 *
 * Definition of the expected wakeup node. 
 */
static npa_node_definition g_sleepWakeupNode = 
{ 
  "/node/core/cpu/wakeup", /* name */
  sleepNPA_wakeupDriver,   /* driver_fcn */
  NPA_NODE_DEFAULT,        /* attributes */
  NULL,                    /* data */
  NPA_EMPTY_ARRAY,
  NPA_ARRAY(g_sleepWakeupResource)
};
#endif        /* !FEATURE_SLEEP_NO_WAKEUP_NODE */

#ifndef FEATURE_SLEEP_NO_LATENCY_NODE
/*==============================================================================
                         SLEEP LATENCY NODE DEFINITION
 =============================================================================*/
/**
 * sleepNPA_latencyDriver
 *
 * @brief Driver function for the /core/cpu/latency resource that updates it.
 *
 * @param resource: Pointer to the resource (i.e. /core/cpu/latency) (unused).
 * @param client: Client of this resource (unused).
 * @param state: Resource state as aggregated by the Update function.
 *
 * @return The state of the resource if set else NPA_MAX_STATE
*/
static npa_resource_state sleepNPA_latencyDriver ( npa_resource       *resource,
                                                   npa_client         *client,
                                                   npa_resource_state state )
{
  /* 
   * Record the state so it can be queried from the sleep task before we go
   * to sleep. 
   */
  if(0 == state)
  {  
    state = NPA_MAX_STATE;
  }

  /* There is a race condition where ATS may finish the query before NPA 
   * framework can process the return vlaue from driver causing ATS to use 
   * stale values. Until NPA has support for synchronous POST_CHANGE 
   * callbacks, update the final calculated state in separate field 
   * as recommended by NPA team - use this field for query purpose */
  resource->definition->data = (npa_user_data)state;

  /* Trigger Active Timer Solver -- latency is updated */
  sleepActive_SetSignal( SLEEP_SIGNAL_LATENCY );
  
  return state;
}

/**
 * sleepNPA_latencyDriverusec
 *
 * @brief Driver function for the /core/cpu/latency resource that updates it.
 *
 * @param resource: Pointer to the resource (i.e. /core/cpu/latency) (unused).
 * @param client: Client of this resource (unused).
 * @param state: New(?) state for the resource (unused).
 *
 * @return The state of the resource if set else NPA_MAX_STATE
*/
static npa_resource_state sleepNPA_latencyDriverusec (npa_resource        *resource,
                                                      npa_client          *client,
                                                      npa_resource_state  state)
{
  npa_client_handle clientHandle = NPA_DEPENDENCY(resource, 0);

  /* See the comment in latency driver function */
  resource->definition->data = (npa_user_data)state;

  /* Convert the request to ticks and forward it to the corresponding
   * base latency resource */
  npa_issue_required_request(clientHandle, US_TO_TICKS(state));

  return state;
}

/*
 * sleepNPA_latencyQuery
 */
npa_query_status sleepNPA_latencyQuery( npa_resource    *resource,
                                        unsigned int    query_id, 
                                        npa_query_type  *query_result )
{
  npa_query_status status = NPA_QUERY_SUCCESS;
  
  CORE_VERIFY( resource );
  CORE_VERIFY( query_result );
  
  switch( query_id )
  {
    case NPA_QUERY_CURRENT_STATE:
      query_result->type = NPA_QUERY_TYPE_STATE;
      query_result->data.state = 
        (npa_resource_state)(resource->definition->data);
      break;
      
    default:
      status = NPA_QUERY_UNSUPPORTED_QUERY_ID;
      break;
  } 
  
  return status;
}

/* Definition of the (ticks) latency resource. */
static npa_resource_definition g_sleepLatencyDefinition[] = 
{ 
  {  
    "/core/cpu/latency",                      /* Name */
    "ticks",                                  /* Units */
    NPA_MAX_STATE,                            /* Max State */
    &npa_min_plugin,                          /* Plugin */
    NPA_RESOURCE_INTERPROCESS_ACCESS_ALLOWED, /* Attributes */
    NULL,                                     /* User Data */
    sleepNPA_latencyQuery,                    /* Query Function */
    NULL                                      /* Query Link Function */
  }
};

/* Definition of the (ticks) latency node. */
static npa_node_definition g_sleepLatencyNode = 
{ 
  "/node/core/cpu/latency",                 /* name */
  sleepNPA_latencyDriver,                   /* driver_fcn */
  NPA_NODE_DEFAULT,                         /* attributes */
  NULL,                                     /* data */
  NPA_EMPTY_ARRAY,                          /* dependencies */
  NPA_ARRAY(g_sleepLatencyDefinition)       /* resources */
};

/**
 * sleepNPA_defineLatencyNode
 *
 * @brief Creates the npa resources for latency node 
 *
 * This defines the base latency node.  This takes latency requests in ticks
 */
static void sleepNPA_defineLatencyNode(void)
{
  npa_resource_state initial_state  = 0;
  
  /* Registering the latency node with npa framework */
  npa_define_node(&g_sleepLatencyNode, &initial_state, NULL);

  /* Add "latency" to the list of resources logged in "Sleep Requests" */
  CORE_VERIFY(NPA_SUCCESS == (npa_add_resource_log_by_handle("Sleep Requests",
                                                             g_sleepLatencyDefinition->handle)));

  return;
}

/* Node dependencies for the uSec latency node */
static npa_node_dependency g_sleepUSecLatencyDependencies[1] =
{
  {
    "/core/cpu/latency",
    NPA_CLIENT_REQUIRED,
  },
};

/* Definition of the (uSec) latency resource. */
static npa_resource_definition g_sleepUSecLatencyDefinition[] = 
{ 
  {  
    "/core/cpu/latency/usec",                 /* Name */
    "uSec",                                   /* Units */
    NPA_MAX_STATE,                            /* Max State */
    &npa_min_plugin,                          /* Plugin */
    NPA_RESOURCE_INTERPROCESS_ACCESS_ALLOWED, /* Attributes */
    NULL,                                     /* User Data */
    sleepNPA_latencyQuery,                    /* Query Function */
    NULL                                      /* Query Link Function */
  }
};

/* Definition of the (uSec) latency node. */
static npa_node_definition g_sleepUSecLatencyNode = 
{ 
  "/node/core/cpu/latency/usec",            /* name */
  sleepNPA_latencyDriverusec,               /* driver_fcn */
  NPA_NODE_DEFAULT,                         /* attributes */
  NULL,                                     /* data */
  NPA_ARRAY(g_sleepUSecLatencyDependencies),/* dependencies */
  NPA_ARRAY(g_sleepUSecLatencyDefinition)   /* resources */
};

/**
 * sleepNPA_defineLatencyNodeusec
 *
 * @brief Creates the npa resources for the uSec latency node 
 *
 * This defines the usec latency node.  This takes latency requests in
 * usec, and then converts the minimum to ticks and sends it to the
 * base latency resource
 */
static void sleepNPA_defineLatencyNodeusec(void)
{
  /* Registering the latency node with npa framework */
  npa_define_node(&g_sleepUSecLatencyNode, NULL, NULL);

  return;
}

/**
 * sleepNPA_defineAllLatencyNodes
 *
 * @brief Constructs npa resources for latency node based on number of cores
 *        the processor has.
 *
 * If processor has just one core, it will create just a global latency 
 * resource. However, if there are more than one core on a processor, it
 * will create a latency resource for each core and a global resource.
 */
static void sleepNPA_defineAllLatencyNodes(void)
{
  sleepNPA_defineLatencyNode();
  sleepNPA_defineLatencyNodeusec();
  return;
}

#endif        /* !FEATURE_SLEEP_NO_LATENCY_NODE */


/*
 * sleepNPA_initialize
 */
void sleepNPA_initialize(void)
{
#if !defined(FEATURE_SLEEP_NO_WAKEUP_NODE)      || \
    !defined(FEATURE_SLEEP_NO_LATENCY_NODE)     || \
    !defined(FEATURE_SLEEP_NO_MAX_DURATION_NODE)
  npa_resource_state initial_state[] = {NPA_MAX_STATE};
#endif 

#ifndef FEATURE_SLEEP_NO_WAKEUP_NODE
  npa_define_node(&g_sleepWakeupNode, initial_state, NULL);
#endif /* !FEATURE_SLEEP_NO_WAKEUP_NODE */

#ifndef FEATURE_SLEEP_NO_MAX_DURATION_NODE
  npa_define_node(&g_sleepMaxDurationNode, initial_state, NULL);
  npa_alias_resource_cb(MAX_DURATION_TICKS, MAX_DURATION_USEC, NULL, NULL);

  /* Log requests to "max_duration" resource in "Sleep Requests" */
  CORE_VERIFY(NPA_SUCCESS == (npa_add_resource_log_by_handle(
                              "Sleep Requests", 
                              g_sleepMaxDurationResource[0].handle)));

#endif /* !FEATURE_SLEEP_NO_MAX_DURATION_NODE */

#ifndef FEATURE_SLEEP_NO_LATENCY_NODE
  sleepNPA_defineAllLatencyNodes();
#endif /* !FEATURE_SLEEP_NO_LATENCY_NODE */

  return;
}

