# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/InterruptController/config/8996/kernel/InterruptConfigData.c"
# 1 "<built-in>" 1
# 1 "<built-in>" 3
# 140 "<built-in>" 3
# 1 "<command line>" 1
# 1 "<built-in>" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/InterruptController/config/8996/kernel/InterruptConfigData.c" 2
# 30 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/InterruptController/config/8996/kernel/InterruptConfigData.c"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALReg.h" 1
# 19 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALReg.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DalDevice.h" 1
# 15 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DalDevice.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALStdDef.h" 1
# 28 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALStdDef.h"
typedef unsigned long int uint32;




typedef unsigned short uint16;




typedef unsigned char uint8;




typedef signed long int int32;




typedef signed short int16;




typedef signed char int8;
# 62 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALStdDef.h"
typedef unsigned long long uint64;




typedef long long int64;




typedef unsigned char byte;



typedef uint32 DALBOOL;
typedef uint32 DALDEVICEID;
typedef uint32 DalPowerCmd;
typedef uint32 DalPowerDomain;
typedef uint32 DalSysReq;
typedef uint32 DALHandle;
typedef int DALResult;
typedef void * DALEnvHandle;
typedef void * DALSYSEventHandle;
typedef uint32 DALMemAddr;
typedef uint32 DALSYSMemAddr;
typedef uint64 DALSYSPhyAddr;
typedef uint32 DALInterfaceVersion;







typedef unsigned char * DALDDIParamPtr;

typedef struct DALEventObject DALEventObject;
struct DALEventObject
{
    uint32 obj[8];
};
typedef DALEventObject * DALEventHandle;

typedef struct _DALMemObject
{
   uint32 memAttributes;
   uint32 sysObjInfo[2];
   uint32 dwLen;
   uint32 ownerVirtAddr;
   uint32 virtAddr;
   uint32 physAddr;
}
DALMemObject;

typedef struct _DALDDIMemBufDesc
{
   uint32 dwOffset;
   uint32 dwLen;
   uint32 dwUser;
}
DALDDIMemBufDesc;


typedef struct _DALDDIMemDescList
{
   uint32 dwFlags;
   uint32 dwNumBufs;
   DALDDIMemBufDesc bufList[1];
}
DALDDIMemDescList;





typedef struct DALSysMemDescBuf DALSysMemDescBuf;
struct DALSysMemDescBuf
{
   DALSYSMemAddr VirtualAddr;
   DALSYSMemAddr PhysicalAddr;
   uint32 size;
   uint32 user;
};
# 158 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALStdDef.h"
typedef struct { uint32 dwObjInfo; DALSYSMemAddr thisVirtualAddr; DALSYSMemAddr PhysicalAddr; DALSYSMemAddr VirtualAddr; uint32 hOwnerProc; uint32 dwCurBufIdx; uint32 dwNumDescBufs; DALSysMemDescBuf BufInfo[1];} DALSysMemDescList;
# 16 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DalDevice.h" 2
# 55 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DalDevice.h"
typedef struct DalDeviceInfo DalDeviceInfo;
struct DalDeviceInfo
{
   uint32 sizeOfActual;
   uint32 Version;
   char Name[32];
};

typedef struct DalDeviceStatus DalDeviceStatus;
struct DalDeviceStatus
{
   uint32 sizeOfActual;
   uint32 Status;
};







typedef struct DalDeviceHandle DalDeviceHandle;
typedef struct DalDevice DalDevice;
struct DalDevice
{
   DALResult (*Attach)(const char*,DALDEVICEID,DalDeviceHandle **);
   uint32 (*Detach)(DalDeviceHandle *);
   DALResult (*Init)(DalDeviceHandle *);
   DALResult (*DeInit)(DalDeviceHandle *);
   DALResult (*Open)(DalDeviceHandle *, uint32);
   DALResult (*Close)(DalDeviceHandle *);
   DALResult (*Info)(DalDeviceHandle *, DalDeviceInfo *, uint32);
   DALResult (*PowerEvent)(DalDeviceHandle *, DalPowerCmd, DalPowerDomain);
   DALResult (*SysRequest)(DalDeviceHandle *, DalSysReq, const void *, uint32,
                           void *,uint32, uint32*);
};

typedef struct DalInterface DalInterface;
struct DalInterface
{
   struct DalDevice DalDevice;

};

struct DalDeviceHandle
{
   uint32 dwDalHandleId;
   const DalInterface *pVtbl;
   void *pClientCtxt;
   uint32 dwVtblLen;
};

typedef struct DalDeviceHandle * DALDEVICEHANDLE;

typedef struct DalRemoteHandle DalRemoteHandle;
typedef struct DalRemote DalRemote;
struct DalRemote
{
   struct DalDevice DalDevice;
   DALResult (* FCN_0) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1 );
   DALResult (* FCN_1) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32 u2 );
   DALResult (* FCN_2) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32* p_u2 );
   DALResult (* FCN_3) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32 u2, uint32 u3 );
   DALResult (* FCN_4) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32 u2, uint32* p_u3 );
   DALResult (* FCN_5) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen);
   DALResult (* FCN_6) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * ibuf, uint32 ilen);
   DALResult (* FCN_7) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * obuf, uint32 olen, uint32 *oalen);
   DALResult (* FCN_8) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * obuf, uint32 olen);
   DALResult (* FCN_9) ( uint32 ddi_idx, DalDeviceHandle *h, void * obuf, uint32 olen );
   DALResult (* FCN_10)( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * ibuf, uint32 ilen, void * obuf, uint32 olen, uint32 * oalen);
   DALResult (* FCN_11) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * obuf, uint32 olen);
   DALResult (* FCN_12) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * obuf, uint32 olen, uint32 *oalen);
   DALResult (* FCN_13) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * ibuf2, uint32 ilen2, void * obuf, uint32 olen);
   DALResult (* FCN_14) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * obuf1, uint32 olen, void * obuf2, uint32 olen2, uint32 * oalen);
   DALResult (* FCN_15) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * ibuf2, uint32 ilen2, void * obuf2, uint32 olen2, uint32 *oalen, void * obuf, uint32 olen );
};

struct DalRemoteHandle
{
   uint32 dwDalHandleId;
   const DalRemote *pVtbl;
   void *pClientCtxt;
};






static __inline uint32
DalDevice_Detach(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Detach(_h);
}

static __inline DALResult
DalDevice_Init(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Init(_h);
}

static __inline DALResult
DalDevice_DeInit(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.DeInit(_h);
}

static __inline DALResult
DalDevice_PowerEvent(DalDeviceHandle *_h, DalPowerCmd PowerCmd,
                     DalPowerDomain PowerDomain)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.PowerEvent(_h,PowerCmd,PowerDomain);
}

static __inline DALResult
DalDevice_Open(DalDeviceHandle *_h, uint32 mode)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Open(_h,mode);
}

static __inline DALResult
DalDevice_Close(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Close(_h);
}

static __inline DALResult
DalDevice_Info(DalDeviceHandle *_h, DalDeviceInfo* info, uint32 infoSize)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Info(_h,info,infoSize);
}

static __inline DALResult
DalDevice_SysRequest(DalDeviceHandle *_h, DalSysReq ReqIdx,
                     const unsigned char* SrcBuf, int SrcBufLen,
                     unsigned char* DestBuf, uint32 DestBufLen, uint32* DestBufLenReq)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.SysRequest(_h,ReqIdx,SrcBuf,SrcBufLen,
                                          DestBuf,DestBufLen,DestBufLenReq);
}
# 218 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DalDevice.h"
DALResult
DAL_DeviceAttach(DALDEVICEID DeviceId,
                 DalDeviceHandle **phDevice);
# 239 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DalDevice.h"
DALResult
DAL_DeviceAttachLocal(const char *pszArg,DALDEVICEID DeviceId,
                      DalDeviceHandle **phDevice);
# 259 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DalDevice.h"
DALResult
DAL_DeviceAttachEx(const char *pszArg, DALDEVICEID DeviceId,
               DALInterfaceVersion ClientVersion,DalDeviceHandle **phDevice);






DALResult
DAL_StringDeviceAttachEx(const char *pszArg,
                   const char *pszDevName,
                   DALInterfaceVersion ClientVersion,
                   DalDeviceHandle **phDalDevice);





DALResult
DAL_DeviceAttachRemote(const char *pszArg,
                   DALDEVICEID DevId,
                   DALInterfaceVersion ClientVersion,
                   DalDeviceHandle **phDALDevice);
# 299 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DalDevice.h"
DALResult
DAL_DeviceDetach(DalDeviceHandle *hDevice);
# 314 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DalDevice.h"
DALResult
DAL_StringDeviceAttach(const char *pszDevName,DalDeviceHandle **phDalDevice);
# 20 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALReg.h" 2


typedef struct DALREG_DriverInfo DALREG_DriverInfo;
struct DALREG_DriverInfo
{
 DALResult (*pfnDALNewFunc)(const char * , DALDEVICEID, DalDeviceHandle**);
 uint32 dwNumDevices;
 DALDEVICEID *pDeviceId;
};


typedef struct DALREG_DriverInfoList DALREG_DriverInfoList;
struct DALREG_DriverInfoList
{
 uint32 dwLen;
 DALREG_DriverInfo ** pDriverInfo;
};
# 31 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/InterruptController/config/8996/kernel/InterruptConfigData.c" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALDeviceId.h" 1
# 32 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/InterruptController/config/8996/kernel/InterruptConfigData.c" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIInterruptController.h" 1
# 45 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIInterruptController.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALStdErr.h" 1
# 46 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIInterruptController.h" 2
# 111 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIInterruptController.h"
typedef uint32 DALInterruptID;
typedef void * DALISRCtx;
typedef void * (*DALISR)(DALISRCtx);



typedef void * DALIRQCtx;
typedef void * (*DALIRQ)(DALIRQCtx);
# 136 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIInterruptController.h"
typedef enum
{
  DALINTCNTLR_NO_POWER_COLLAPSE,

  DALINTCNTLR_POWER_COLLAPSE,

  PLACEHOLDER_InterruptControllerSleepType = 0x7fffffff
} InterruptControllerSleepType;





typedef struct DalInterruptController DalInterruptController;
struct DalInterruptController
{
   struct DalDevice DalDevice;
   DALResult (*RegisterISR)(DalDeviceHandle * _h, DALInterruptID intrID,
                           const DALISR isr, const DALISRCtx ctx, uint32 bEnable);
   DALResult (*RegisterIST)(DalDeviceHandle * _h, DALInterruptID intrID,
                           const DALISR isr, const DALISRCtx ctx, uint32 bEnable,char* pISTName);
   DALResult (*RegisterEvent)(DalDeviceHandle * _h, DALInterruptID intrID,
                             const DALSYSEventHandle hEvent, uint32 bEnable);
   DALResult (*Unregister)(DalDeviceHandle * _h, DALInterruptID intrID);
   DALResult (*InterruptDone)(DalDeviceHandle * _h, DALInterruptID intrID);
   DALResult (*InterruptEnable)(DalDeviceHandle * _h, DALInterruptID intrID);
   DALResult (*InterruptDisable)(DalDeviceHandle * _h, DALInterruptID intrID);
   DALResult (*InterruptTrigger)(DalDeviceHandle * _h, DALInterruptID intrID);
   DALResult (*InterruptClear)(DalDeviceHandle * _h, DALInterruptID intrID);
   DALResult (*InterruptStatus)(DalDeviceHandle * _h, DALInterruptID intrID);
   DALResult (*RegisterIRQHandler)(DalDeviceHandle * _h, DALInterruptID intrID,
                           const DALIRQ irq, const DALIRQCtx ctx, uint32 bEnable);
  DALResult (*SetInterruptTrigger)(DalDeviceHandle * _h, DALInterruptID intrID, uint32 nTrigger);
  DALResult (*IsInterruptPending)(DalDeviceHandle * _h, DALInterruptID intrID,void* bState,uint32 size);
  DALResult (*IsInterruptEnabled)(DalDeviceHandle * _h, DALInterruptID intrID,void* bState,uint32 size);
  DALResult (*MapWakeupInterrupt)(DalDeviceHandle * _h, DALInterruptID intrID,uint32 nWakeupIntID);
  DALResult (*IsAnyInterruptPending)(DalDeviceHandle * _h, uint32* bState,uint32 size);
  DALResult (*Sleep)(DalDeviceHandle * _h, InterruptControllerSleepType sleep);
  DALResult (*Wakeup)(DalDeviceHandle * _h, InterruptControllerSleepType sleep);
  DALResult (*GetInterruptTrigger)(DalDeviceHandle * _h, DALInterruptID intrID,uint32* eTrigger, uint32 size);
  DALResult (*GetInterruptID)(DalDeviceHandle * _h, const char *szIntrName, uint32* pnIntrID, uint32 size);
  DALResult (*LogState)(DalDeviceHandle * _h, void *pULog);
};

typedef struct DalInterruptControllerHandle DalInterruptControllerHandle;
struct DalInterruptControllerHandle
{
   uint32 dwDalHandleId;
   const DalInterruptController * pVtbl;
   void * pClientCtxt;
};
# 239 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIInterruptController.h"
static __inline DALResult
DalInterruptController_RegisterISR(DalDeviceHandle * _h, DALInterruptID intrID,
                           const DALISR isr, const DALISRCtx ctx, uint32 IntrFlags)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->RegisterISR( _h, intrID,
                                                                   isr, ctx, IntrFlags);
  }
  else
  {
    return -1;
  }
}
# 299 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIInterruptController.h"
static __inline DALResult
DalInterruptController_RegisterIST(DalDeviceHandle * _h, DALInterruptID intrID,
                           const DALISR isr, const DALISRCtx ctx, uint32 IntrFlags, char* pISTName)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->RegisterIST( _h, intrID,
                                                                   isr, ctx, IntrFlags,pISTName);
  }
  else
  {
    return -1;
  }
}
# 352 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIInterruptController.h"
static __inline DALResult
DalInterruptController_RegisterEvent(DalDeviceHandle * _h, DALInterruptID intrID,
                            const DALSYSEventHandle hEvent, uint32 IntrFlags)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->RegisterEvent( _h, intrID,
                                                                        hEvent, IntrFlags);
  }
  else
  {
    return -1;
  }
}
# 404 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIInterruptController.h"
static __inline DALResult
DalInterruptController_Unregister(DalDeviceHandle * _h, DALInterruptID intrID)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->Unregister( _h, intrID);
  }
  else
  {
    return -1;
  }
}
# 449 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIInterruptController.h"
static __inline DALResult
DalInterruptController_InterruptDone(DalDeviceHandle * _h, DALInterruptID intrID)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->InterruptDone( _h, intrID);
  }
  else
  {
    return -1;
  }
}
# 499 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIInterruptController.h"
static __inline DALResult
DalInterruptController_InterruptEnable(DalDeviceHandle * _h, DALInterruptID intrID)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->InterruptEnable( _h, intrID);
  }
  else
  {
    return -1;
  }
}
# 552 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIInterruptController.h"
static __inline DALResult
DalInterruptController_InterruptDisable(DalDeviceHandle * _h, DALInterruptID intrID)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->InterruptDisable( _h, intrID);
  }
  else
  {
    return -1;
  }
}
# 597 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIInterruptController.h"
static __inline DALResult
DalInterruptController_InterruptTrigger(DalDeviceHandle * _h, DALInterruptID intrID)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->InterruptTrigger( _h, intrID);
  }
  else
  {
    return -1;
  }

}
# 644 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIInterruptController.h"
static __inline DALResult
DalInterruptController_InterruptClear(DalDeviceHandle * _h, DALInterruptID intrID)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->InterruptClear( _h, intrID);
  }
  else
  {
    return -1;
  }
}
# 692 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIInterruptController.h"
static __inline DALResult
DalInterruptController_InterruptStatus
(
  DalDeviceHandle * _h,
  DALInterruptID intrID
)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->InterruptStatus( _h, intrID);
  }
  else
  {
    return -1;
  }
}
# 726 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIInterruptController.h"
static __inline DALResult
DalInterruptController_RegisterIRQHandler(DalDeviceHandle * _h, DALInterruptID intrID,const DALIRQ irq, const DALIRQCtx ctx, uint32 bEnable)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->RegisterIRQHandler( _h, intrID,
                                                                   irq, ctx, bEnable);
  }
  else
  {
    return -1;
  }
}
# 777 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIInterruptController.h"
static __inline DALResult
DalInterruptController_SetTrigger(DalDeviceHandle * _h, DALInterruptID intrID,uint32 nTrigger)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->SetInterruptTrigger( _h,
            intrID, nTrigger);
  }
  else
  {
    return -1;
  }
}
# 829 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIInterruptController.h"
static __inline DALResult
DalInterruptController_IsInterruptPending(DalDeviceHandle * _h, DALInterruptID intrID,uint32* bState)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->IsInterruptPending( _h,
            intrID, (void*)bState,1);
  }
  else
  {
    return -1;
  }
}
# 880 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIInterruptController.h"
static __inline DALResult
DalInterruptController_IsInterruptEnabled(DalDeviceHandle * _h, DALInterruptID intrID,uint32* bState)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->IsInterruptEnabled( _h,
           intrID, (void*)bState,1);
  }
  else
  {
    return -1;
  }
}
# 935 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIInterruptController.h"
static __inline DALResult
DalInterruptController_MapWakeupInterrupt(DalDeviceHandle * _h, DALInterruptID intrID, uint32 WakeupIntID)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->MapWakeupInterrupt( _h,
           intrID, WakeupIntID);
  }
  else
  {
    return -1;
  }
}
# 988 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIInterruptController.h"
static __inline DALResult
DalInterruptController_IsAnyInterruptPending(DalDeviceHandle * _h, uint32* bState)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->IsAnyInterruptPending( _h,
           (uint32*)bState,1);
  }
  else
  {
    return -1;
  }
}
# 1038 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIInterruptController.h"
static __inline DALResult
DalInterruptController_Sleep(DalDeviceHandle * _h,InterruptControllerSleepType sleep)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->Sleep( _h, sleep);
  }
  else
  {
    return -1;
  }
}
# 1087 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIInterruptController.h"
static __inline DALResult
DalInterruptController_Wakeup(DalDeviceHandle * _h, InterruptControllerSleepType sleep)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->Wakeup( _h, sleep);
  }
  else
  {
    return -1;
  }
}
# 1147 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIInterruptController.h"
static __inline DALResult
DalInterruptController_GetInterruptTrigger(DalDeviceHandle * _h, DALInterruptID intrID,uint32* eTrigger)
{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->GetInterruptTrigger( _h,
             intrID,eTrigger,1);
  }
  else
  {
    return -1;
  }
}
# 1200 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIInterruptController.h"
static __inline DALResult
DalInterruptController_GetInterruptID(DalDeviceHandle * _h, const char * szIntrName,DALInterruptID* pnIntrID)

{
  if(_h != 0)
  {
    return ((DalInterruptControllerHandle *)_h)->pVtbl->GetInterruptID( _h, szIntrName, pnIntrID,1);
  }
  else
  {
    return -1;
  }
}
# 1234 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIInterruptController.h"
static __inline DALResult
DalInterruptController_LogState(DalDeviceHandle * _h, void *pULog)
{
   return ((DalInterruptControllerHandle *)_h)->pVtbl->LogState( _h, pULog);
}
# 33 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/InterruptController/config/8996/kernel/InterruptConfigData.c" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/InterruptController/src/qurt/kernel/DALInterruptControllerConfig.h" 1
# 39 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/InterruptController/src/qurt/kernel/DALInterruptControllerConfig.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/InterruptController/inc/DALInterruptController.h" 1
# 17 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/InterruptController/inc/DALInterruptController.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALFramework.h" 1
# 27 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALFramework.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h" 1
# 18 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSysTypes.h" 1
# 37 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSysTypes.h"
typedef void * DALSYSObjHandle;
typedef void * DALSYSSyncHandle;
typedef void * DALSYSMemHandle;
typedef void * DALSYSWorkLoopHandle;
typedef uint32 *DALSYSPropertyHandle;

typedef struct DALSYSPropertyVar DALSYSPropertyVar;
struct DALSYSPropertyVar
{
    uint32 dwType;
    uint32 dwLen;
    union
    {
        byte *pbVal;
        char *pszVal;
        uint32 dwVal;
        uint32 *pdwVal;
        const void *pStruct;
    }Val;
};


typedef struct DALSYSPropStructTblType DALSYSPropStructTblType ;
struct DALSYSPropStructTblType{
   uint32 dwSize;
   const void *pStruct;
};


typedef struct StringDevice StringDevice;
 struct StringDevice{
   char *pszName;
   uint32 dwHash;
   uint32 dwOffset;
   DALREG_DriverInfo *pFunctionName;
   uint32 dwNumCollision;
   uint32 *pdwCollisions;
};

typedef struct DALProps DALProps;
struct DALProps
{
   const byte *pDALPROP_PropBin;
   const DALSYSPropStructTblType *pDALPROP_StructPtrs;
   const uint32 dwDeviceSize;
   const StringDevice *pDevices;
};

typedef struct DALSYSBaseObj DALSYSBaseObj;
struct DALSYSBaseObj
{
   uint32 dwObjInfo;
   uint32 hOwnerProc;
   DALSYSMemAddr thisVirtualAddr;
};







typedef struct DALSYSEventObj DALSYSEventObj;
struct DALSYSEventObj
{
   unsigned long long _bSpace[80/sizeof(unsigned long long)];
};

typedef struct DALSYSSyncObj DALSYSSyncObj;
struct DALSYSSyncObj
{
   unsigned long long _bSpace[40/sizeof(unsigned long long)];
};

typedef struct DALSYSMemObj DALSYSMemObj;
struct DALSYSMemObj
{
   unsigned long long _bSpace[40/sizeof(unsigned long long)];
};

typedef struct DALSYSWorkLoopEventObj DALSYSWorkLoopEventObj;
struct DALSYSWorkLoopEventObj
{
   unsigned long long _bSpace[32/sizeof(unsigned long long)];
};

typedef struct DALSYSWorkLoopObj DALSYSWorkLoopObj;
struct DALSYSWorkLoopObj
{
   unsigned long long _bSpace[64/sizeof(unsigned long long)];
};

typedef void * DALSYSCallbackFuncCtx;
typedef void * (*DALSYSCallbackFunc)(void*,uint32,void*,uint32);


typedef struct DALSYSMemInfo DALSYSMemInfo;
struct DALSYSMemInfo
{
    DALSYSMemAddr VirtualAddr;
    DALSYSMemAddr PhysicalAddr;
    uint32 dwLen;
    uint32 dwMappedLen;
    uint32 dwProps;
};

typedef enum
{
   DALSYS_MEM_CACHE_DEVICE,
   DALSYS_MEM_CACHE_NONE,
   DALSYS_MEM_CACHE_WRITEBACK,
   DALSYS_MEM_CACHE_WRITETHROUGH,
   DALSYS_MEM_CACHE_INVALID
}
DALSYSMemCacheType;

typedef enum
{
   DALSYS_MEM_PERM_NONE=0,
   DALSYS_MEM_PERM_R=0x1,
   DALSYS_MEM_PERM_W=0x2,
   DALSYS_MEM_PERM_RW=0x3,
   DALSYS_MEM_PERM_X=0x4,
   DALSYS_MEM_PERM_RX=0x5,
   DALSYS_MEM_PERM_WX=0x6,
   DALSYS_MEM_PERM_RWX=0x7,
   DALSYS_MEM_PERM_INVALID=0x8
}
DALSYSMemPermType;

typedef enum
{
   DALSYS_MEM_SHARE_ALLOWED,
   DALSYS_MEM_SHARE_NOT_ALLOWED,
   DALSYS_MEM_SHARE_INVALID
}
DALSYSMemShareType;


typedef struct DALSYSMemInfoEx DALSYSMemInfoEx;
struct DALSYSMemInfoEx
{
    DALSYSPhyAddr physicalAddr;
    DALSYSMemAddr virtualAddr;
    DALSYSMemAddr size;
    DALSYSMemCacheType cacheType;
    DALSYSMemPermType permission;
    DALSYSMemShareType shareType;
};






typedef struct DALSYSMemReq DALSYSMemReq;
struct DALSYSMemReq
{
    DALSYSMemInfoEx memInfo;

    DALSYSMemObj *pObj;

    DALBOOL prealloc;
};

typedef enum
{
   DEVCFG_TARGET_INFO_SOC,
   DEVCFG_TARGET_INFO_PLATFORM
}DEVCFG_TARGET_INFO_TYPE;
# 243 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSysTypes.h"
typedef DALResult
(*DALSYSWorkLoopExecute)
(
    DALSYSEventHandle,
    void *
);

typedef void
(*DALSYS_InitSystemHandleFncPtr)
(
    DalDeviceHandle * hDalDevice
);

typedef DALResult
(*DALSYS_DestroyObjectFncPtr)
(
    DALSYSObjHandle
);

typedef DALResult
(*DALSYS_CopyObjectFncPtr)
(
    DALSYSObjHandle,
    DALSYSObjHandle *
);

typedef DALResult
(*DALSYS_RegisterWorkLoopFncPtr)
(
    uint32,
    uint32,
    DALSYSWorkLoopHandle *,
    DALSYSWorkLoopObj *
);

typedef DALResult
(*DALSYS_RegisterWorkLoopExFncPtr)
(
    char *,
    uint32,
    uint32,
    uint32,
    DALSYSWorkLoopHandle *,
    DALSYSWorkLoopObj *
);

typedef DALResult
(*DALSYS_AddEventToWorkLoopFncPtr)
(
    DALSYSWorkLoopHandle,
    DALSYSWorkLoopExecute,
    void *,
    DALSYSEventHandle,
    DALSYSSyncHandle
);

typedef DALResult
(*DALSYS_DeleteEventFromWorkLoopFncPtr)
(
    DALSYSWorkLoopHandle,
    DALSYSEventHandle
);

typedef DALResult
(*DALSYS_EventCreateFncPtr)
(
    uint32 ,
    DALSYSEventHandle *,
    DALSYSEventObj *
);

typedef DALResult
(*DALSYS_EventCtrlFncPtr)
(
    DALSYSEventHandle,
    uint32,
   uint32,
   void *,
   uint32
);

typedef DALResult
(*DALSYS_EventWaitFncPtr)
(
    DALSYSEventHandle
);

typedef DALResult
(*DALSYS_EventMultipleWaitFncPtr)
(
    DALSYSEventHandle*,
    int,
    uint32,
    uint32 *
);
typedef DALResult
(*DALSYS_SetupCallbackEventFncPtr)
(
    DALSYSEventHandle,
    DALSYSCallbackFunc,
    DALSYSCallbackFuncCtx
);

typedef DALResult
(*DALSYS_SyncCreateFncPtr)
(
    uint32,
    DALSYSSyncHandle *,
    DALSYSSyncObj *
);

typedef void
(*DALSYS_SyncEnterFncPtr)
(
    DALSYSSyncHandle
);

typedef DALResult
(*DALSYS_SyncTryEnterFncPtr)
(
    DALSYSSyncHandle
);

typedef void
(*DALSYS_SyncLeaveFncPtr)
(
    DALSYSSyncHandle
);

typedef DALResult
(*DALSYS_MemRegionAllocFncPtr)
(
    uint32,
    DALSYSMemAddr,
    DALSYSMemAddr,
    uint32,
    DALSYSMemHandle *,
    DALSYSMemObj *
);

typedef DALResult
(*DALSYS_MemRegionMapPhysFncPtr)
(
    DALSYSMemHandle,
    uint32,
    DALSYSMemAddr,
    uint32
);

typedef DALResult
(*DALSYS_MemInfoFncPtr)
(
    DALSYSMemHandle,
    DALSYSMemInfo *
);

typedef DALResult
(*DALSYS_CacheCommandFncPtr)
(
    uint32 ,
    DALSYSMemAddr,
    uint32
);

typedef DALResult
(*DALSYS_MallocFncPtr)
(
    uint32 ,
    void **
);

typedef DALResult
(*DALSYS_FreeFncPtr)
(
    void *
);

typedef void
(*DALSYS_BusyWaitFncPtr)
(
   uint32
);

typedef DALResult
(*DALSYS_MemDescAddBufFncPtr)
(
    DALSysMemDescList *,
    uint32,
    uint32,
    uint32
);

typedef DALResult
(*DALSYS_MemDescPrepareFncPtr)
(
    DALSysMemDescList *,
    uint32
);

typedef DALResult
(*DALSYS_MemDescValidateFncPtr)
(
    DALSysMemDescList *,
    uint32
);

typedef DALResult
(*DALSYS_GetDALPropertyHandleFncPtr)
(
    DALDEVICEID,
    DALSYSPropertyHandle
);

typedef DALResult
(*DAL_DeviceAttachFncPtr)
(
    const char *pszArg,
    DALDEVICEID,
    DalDeviceHandle **
);

typedef DALResult
(*DAL_DeviceDetachFncPtr)
(
    DalDeviceHandle *
);

typedef DALResult
(*DAL_DeviceAttachExFncPtr)
(
 const char *pszArg,
    DALDEVICEID DevId,
    DALInterfaceVersion ClientVersion,
    DalDeviceHandle **phDalDevice
);

typedef DALResult
(*DALSYS_GetPropertyValueFncPtr)
(
    DALSYSPropertyHandle,
    const char *,
    uint32,
    DALSYSPropertyVar *
);

typedef void
(*DALSYS_LogEventFncPtr)
(
    DALDEVICEID,
    uint32,
    const char *
);

typedef struct DALSYSFncPtrTbl DALSYSFncPtrTbl;
struct DALSYSFncPtrTbl
{
    DALSYS_InitSystemHandleFncPtr DALSYS_InitSystemHandleFnc;
    DALSYS_DestroyObjectFncPtr DALSYS_DestroyObjectFnc;
    DALSYS_CopyObjectFncPtr DALSYS_CopyObjectFnc;
    DALSYS_RegisterWorkLoopFncPtr DALSYS_RegisterWorkLoopFnc;
    DALSYS_RegisterWorkLoopExFncPtr DALSYS_RegisterWorkLoopExFnc;
    DALSYS_AddEventToWorkLoopFncPtr DALSYS_AddEventToWorkLoopFnc;
    DALSYS_DeleteEventFromWorkLoopFncPtr DALSYS_DeleteEventFromWorkLoopFnc;
    DALSYS_EventCreateFncPtr DALSYS_EventCreateFnc;
    DALSYS_EventCtrlFncPtr DALSYS_EventCtrlFnc;
    DALSYS_EventWaitFncPtr DALSYS_EventWaitFnc;
    DALSYS_EventMultipleWaitFncPtr DALSYS_EventMultipleWaitFnc;
    DALSYS_SetupCallbackEventFncPtr DALSYS_SetupCallbackEventFnc;
    DALSYS_SyncCreateFncPtr DALSYS_SyncCreateFnc;
    DALSYS_SyncEnterFncPtr DALSYS_SyncEnterFnc;
    DALSYS_SyncTryEnterFncPtr DALSYS_SyncTryEnterFnc;
    DALSYS_SyncLeaveFncPtr DALSYS_SyncLeaveFnc;
    DALSYS_MemRegionAllocFncPtr DALSYS_MemRegionAllocFnc;
    DALSYS_MemRegionMapPhysFncPtr DALSYS_MemRegionMapPhysFnc;
    DALSYS_MemInfoFncPtr DALSYS_MemInfoFnc;
    DALSYS_CacheCommandFncPtr DALSYS_CacheCommandFnc;
    DALSYS_MallocFncPtr DALSYS_MallocFnc;
    DALSYS_FreeFncPtr DALSYS_FreeFnc;
    DALSYS_BusyWaitFncPtr DALSYS_BusyWaitFnc;
    DALSYS_MemDescAddBufFncPtr DALSYS_MemDescAddBufFnc;
    DALSYS_MemDescPrepareFncPtr DALSYS_MemDescPrepareFnc;
    DALSYS_MemDescValidateFncPtr DALSYS_MemDescValidateFnc;
    DALSYS_GetDALPropertyHandleFncPtr DALSYS_GetDALPropertyHandleFnc;
    DAL_DeviceAttachFncPtr DALSYS_DeviceAttachFnc;
    DAL_DeviceAttachExFncPtr DALSYS_DeviceAttachExFnc;
    DAL_DeviceAttachExFncPtr DALSYS_DeviceAttachRemoteFnc;
    DAL_DeviceDetachFncPtr DALSYS_DeviceDetachFnc;
    DALSYS_GetPropertyValueFncPtr DALSYS_GetPropertyValueFnc;
    DALSYS_LogEventFncPtr DALSYS_LogEventFnc;
};


typedef DALResult
(*DALRemote_NewFncPtr)(const char *, DALDEVICEID, DalDeviceHandle **);
typedef int
(*DALRemote_CommonInitFncPtr)(void);
typedef void
(*DALRemote_IPCInitFcnPtr)(uint32);
typedef int
(*DALRemote_InitFncPtr)(void);
typedef DALSYSEventHandle
(*DALRemote_CreateEventFncPtr)(void *pClientCtxt, DALSYSEventHandle hRemote);
typedef uint32
(*DALRemoteInterProcessCallFncPtr)(void *in_buf, void *out_buf);
typedef int
(*DALRemote_DeinitFncPtr)(void);

typedef struct DALRemoteVtbl DALRemoteVtbl;
struct DALRemoteVtbl
{
   DALRemote_NewFncPtr DALRemote_NewFnc;
   DALRemote_CommonInitFncPtr DALRemote_CommonInitFnc;
   DALRemote_InitFncPtr DALRemote_InitFnc;
   DALRemote_DeinitFncPtr DALRemote_DeinitFnc;
   DALRemote_CreateEventFncPtr DALRemote_CreateEventFnc;
   DALRemote_CreateEventFncPtr DALRemote_CreatePayloadEventFnc;
   DALRemoteInterProcessCallFncPtr DALRemoteInterProcessCallFnc;
   DALRemote_IPCInitFcnPtr DALRemote_IPCInitFcn;
};

typedef struct DALSYSConfig DALSYSConfig;
struct DALSYSConfig
{
    void *pNativeEnv;
    uint32 dwConfig;
   DALRemoteVtbl *pRemoteVtbl;
};
# 19 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h" 2
# 1 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/string.h" 1 3 4
# 10 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/string.h" 3 4
# 1 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 1 3 4
# 297 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
typedef long _Int32t;
typedef unsigned long _Uint32t;
# 308 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
typedef int _Ptrdifft;
# 318 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
typedef unsigned int _Sizet;
# 676 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
# 1 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/stdarg.h" 1 3 4
# 12 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/stdarg.h" 3 4
typedef __builtin_va_list va_list;
# 677 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 2 3 4
# 786 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
typedef long long _Longlong;
typedef unsigned long long _ULonglong;
# 850 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
typedef int _Wchart;
typedef int _Wintt;
# 880 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
typedef va_list _Va_list;
# 902 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
void _Atexit(void (*)(void));
# 915 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
typedef char _Sysch_t;
# 935 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
void _Locksyslock(int);
void _Unlocksyslock(int);
# 1066 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
typedef unsigned short __attribute__((__may_alias__)) alias_short;
static alias_short *strict_aliasing_workaround(unsigned short *ptr) __attribute__((always_inline,unused));

static alias_short *strict_aliasing_workaround(unsigned short *ptr)
{
  alias_short *aliasptr = (alias_short *)ptr;
  return aliasptr;
}
# 11 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/string.h" 2 3 4
# 29 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/string.h" 3 4
typedef _Sizet size_t;




int memcmp(const void *, const void *, size_t) __attribute__((__nothrow__));
void *memcpy(void *, const void *, size_t) __attribute__((__nothrow__));
void *memcpy_v(volatile void *, const volatile void *, size_t) __attribute__((__nothrow__));
void *memset(void *, int, size_t) __attribute__((__nothrow__));
char *strcat(char *, const char *) __attribute__((__nothrow__));
int strcmp(const char *, const char *) __attribute__((__nothrow__));
char *strcpy(char *, const char *) __attribute__((__nothrow__));
size_t strlen(const char *) __attribute__((__nothrow__));

void *memmove(void *, const void *, size_t) __attribute__((__nothrow__));
void *memmove_v(volatile void *, const volatile void *, size_t) __attribute__((__nothrow__));
int strcoll(const char *, const char *) __attribute__((__nothrow__));
size_t strcspn(const char *, const char *) __attribute__((__nothrow__));
char *strerror(int) __attribute__((__nothrow__));
size_t strlcat(char *, const char *, size_t) __attribute__((__nothrow__));
char *strncat(char *, const char *, size_t) __attribute__((__nothrow__));
int strncmp(const char *, const char *, size_t) __attribute__((__nothrow__));
size_t strlcpy(char *, const char *, size_t) __attribute__((__nothrow__));
char *strncpy(char *, const char *, size_t) __attribute__((__nothrow__));
size_t strspn(const char *, const char *) __attribute__((__nothrow__));
char *strtok(char *, const char *) __attribute__((__nothrow__));
char *strsep(char **, const char *) __attribute__((__nothrow__));
size_t strxfrm(char *, const char *, size_t) __attribute__((__nothrow__));


char *strdup(const char *) __attribute__((__nothrow__));
int strcasecmp(const char *, const char *) __attribute__((__nothrow__));
int strncasecmp(const char *, const char *, size_t) __attribute__((__nothrow__));
char *strtok_r(char *, const char *, char **) __attribute__((__nothrow__));

void *memccpy (void *, const void *, int, size_t) __attribute__((__nothrow__));
int strerror_r (int, char *, size_t) __attribute__((__nothrow__));
# 106 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/string.h" 3 4
char *strchr(const char *, int) __attribute__((__nothrow__));
char *strpbrk(const char *, const char *) __attribute__((__nothrow__));
char *strrchr(const char *, int) __attribute__((__nothrow__));
char *strstr(const char *, const char *) __attribute__((__nothrow__));
# 130 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/string.h" 3 4
void *memchr(const void *, int, size_t) __attribute__((__nothrow__));
# 20 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h" 2
# 227 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
void
DALSYS_InitMod(DALSYSConfig * pCfg);
# 238 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
void
DALSYS_DeInitMod(void);
# 248 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
void
DALSYS_InitSystemHandle(DalDeviceHandle *hDalDevice);
# 264 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALSYS_LogEventFncPtr
DALSYS_SetLogCfg(uint32 dwMaxLogLevel, DALSYS_LogEventFncPtr DALSysLogFcn);
# 279 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_DestroyObject(DALSYSObjHandle hObj);
# 292 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_CopyObject(DALSYSObjHandle hObjOrig, DALSYSObjHandle *phObjCopy );
# 310 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_RegisterWorkLoop(uint32 dwPriority,
                  uint32 dwMaxNumEvents,
                  DALSYSWorkLoopHandle *phWorkLoop,
                  DALSYSWorkLoopObj *pWorkLoopObj);
# 334 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_RegisterWorkLoopEx(
                  char * pszname,
                  uint32 dwStackSize,
                  uint32 dwPriority,
                  uint32 dwMaxNumEvents,
                  DALSYSWorkLoopHandle *phWorkLoop,
                  DALSYSWorkLoopObj *pWorkLoopObj);
# 358 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_AddEventToWorkLoop(DALSYSWorkLoopHandle hWorkLoop,
                    DALSYSWorkLoopExecute pfnWorkLoopExecute,
                    void * pArg,
                    DALSYSEventHandle hEvent,
                    DALSYSSyncHandle hSync);
# 375 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_DeleteEventFromWorkLoop(DALSYSWorkLoopHandle hWorkLoop,
                         DALSYSEventHandle hEvent);
# 394 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_EventCreate(uint32 dwAttribs, DALSYSEventHandle *phEvent,
               DALSYSEventObj *pEventObj);
# 415 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_EventCopy(DALSYSEventHandle hEvent,
             DALSYSEventHandle *phEventCopy,DALSYSEventObj *pEventObjCopy,
                 uint32 dwMarshalFlags);
# 434 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_EventCtrlEx(DALSYSEventHandle hEvent, uint32 dwCtrl, uint32 dwParam,
                   void *pPayload, uint32 dwPayloadSize);
# 458 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_EventWait(DALSYSEventHandle hEvent);
# 473 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_EventMultipleWait(DALSYSEventHandle* phEvent, int nEvents,
                         uint32 dwTimeoutUs,uint32 *pdwEventIdx);
# 492 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_SetupCallbackEvent(DALSYSEventHandle hEvent, DALSYSCallbackFunc cbFunc,
                          DALSYSCallbackFuncCtx cbFuncCtx);
# 508 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult DALSYS_TimerStart( DALSYSEventHandle hEvent, uint32 time);
# 519 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult DALSYS_TimerStop( DALSYSEventHandle hEvent );
# 555 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_SyncCreate(uint32 dwAttribs,
                  DALSYSSyncHandle *phSync,
                  DALSYSSyncObj *pSyncObj);
# 572 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
void
DALSYS_SyncEnter(DALSYSSyncHandle hSync);
# 589 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_SyncTryEnter(DALSYSSyncHandle hSync);
# 605 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
void
DALSYS_SyncLeave(DALSYSSyncHandle hSync);
# 634 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_MemRegionAlloc(uint32 dwAttribs, DALSYSMemAddr VirtualAddr,
                      DALSYSMemAddr PhysicalAddr, uint32 dwLen,
                      DALSYSMemHandle *phMem, DALSYSMemObj *pMemObj);
# 666 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_MemRegionAllocEx( DALSYSMemHandle *phMem, const DALSYSMemReq *pMemReq,
      DALSYSMemInfoEx *pMemInfo );
# 683 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_MemRegionMapPhys(DALSYSMemHandle hMem, uint32 dwVirtualBaseOffset,
                        DALSYSMemAddr PhysicalAddr, uint32 dwLen);
# 698 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_MemInfo(DALSYSMemHandle hMem, DALSYSMemInfo *pMemInfo);
# 712 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_MemInfoEx(DALSYSMemHandle hMem, DALSYSMemInfoEx *pMemInfo);
# 726 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_CacheCommand(uint32 CacheCmd, DALSYSMemAddr VirtualAddr, uint32 dwLen);
# 744 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_Malloc(uint32 dwSize, void **ppMem);
# 758 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_Free(void *pmem);
# 771 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
void
DALSYS_BusyWait(uint32 pause_time_us);
# 786 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_GetDALPropertyHandle(DALDEVICEID DeviceId, DALSYSPropertyHandle hDALProps);
# 802 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_GetDALPropertyHandleEx(DALDEVICEID DeviceId,DALSYSPropertyHandle hDALProps,
                              DEVCFG_TARGET_INFO_TYPE target_info_type);
# 818 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_GetDALPropertyHandleDyn(DALDEVICEID DeviceId, DALSYSPropertyHandle hDALProps);
# 833 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_GetDALPropertyHandleStr(const char *pszDevName, DALSYSPropertyHandle hDALProps);
# 849 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_GetDALPropertyHandleStrEx(const char *pszDevName, DALSYSPropertyHandle hDALProps,
                                 DEVCFG_TARGET_INFO_TYPE target_info_type);
# 865 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_GetDALPropertyHandleStrDyn(const char *pszDevName, DALSYSPropertyHandle hDALProps);
# 882 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_GetPropertyValue(DALSYSPropertyHandle hDALProps, const char *pszName,
                  uint32 dwId,
                   DALSYSPropertyVar *pDALPropVar);
# 900 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
void
DALSYS_LogEvent(DALDEVICEID DeviceId, uint32 dwLogEventType,
      const char * pszFmt, ...);
# 913 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALRemoteVtbl *
DALSYS_GetRemoteInterfaceVtbl(void);
# 927 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
uint32 DALSYS_SetThreadPriority(uint32 priority);
# 943 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
uint32 _DALSYS_memscpy(void * pDest, uint32 iDestSz,
      const void * pSrc, uint32 iSrcSize);
# 28 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALFramework.h" 2





typedef struct DALDrvCtxt DALDrvCtxt;
typedef struct DALDevCtxt DALDevCtxt;
typedef struct DALClientCtxt DALClientCtxt;
typedef struct DALDrvVtbl DALDrvVtbl;

struct DALDrvVtbl
{
   int (*DAL_DriverInit)(DALDrvCtxt *);
   int (*DAL_DriverDeInit)(DALDrvCtxt *);
};

struct DALDevCtxt
{
    uint32 dwRefs;
    DALDEVICEID DevId;
    uint32 dwDevCtxtRefIdx;
    DALDrvCtxt *pDALDrvCtxt;
    uint32 hProp[2];
    DalDeviceHandle *hDALSystem;
    DalDeviceHandle *hDALTimer;
    DalDeviceHandle *hDALInterrupt;
    uint32 * pSystemTbl;
    DALSYSWorkLoopHandle * pDefaultWorkLoop;
    const char *strDeviceName;
    uint32 reserve[16-6];
};

struct DALDrvCtxt
{
   DALDrvVtbl DALDrvVtbl;
   uint32 dwNumDev;
   uint32 dwSizeDevCtxt;
   uint32 bInit;
   uint32 dwRefs;
   DALDevCtxt DALDevCtxt[1];
};

struct DALClientCtxt
{
    uint32 dwRefs;
    uint32 dwAccessMode;
    void * pPortCtxt;
    DALDevCtxt *pDALDevCtxt;
};

typedef struct DALInheritSrcPram DALInheritSrcPram;
struct DALInheritSrcPram
{
   const byte * pBuf;
   int dwBufLen;
   uint32 dwSeqIdx;
};

typedef struct DALInheritDestPram DALInheritDestPram;
struct DALInheritDestPram
{
   byte * pBuf;
   uint32 dwBufLen;
   int *pdwObjLen;
   uint32 *dwSeqIdx;
};

typedef struct DALInterface DALInterface;
struct DALInterface
{
    uint32 dwDalHandleId;
    void *pVtbl;
    DALClientCtxt *pclientCtxt;
};
# 156 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALFramework.h"
DALResult
DALFW_AttachToStringDevice(const char *pszDeviceName, DALDrvCtxt *pdalDrvCtxt,
                           DALClientCtxt *pClientCtxt);

DALResult
DALFW_AttachToDevice(DALDEVICEID devId,DALDrvCtxt *pdalDrvCtxt,
                     DALClientCtxt *pdalClientCtxt);
# 171 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALFramework.h"
void
DALFW_MarkDeviceStatic(DALDevCtxt *pdalDevCtxt);

uint32
DALFW_AddRef(DALClientCtxt *pdalClientCtxt);

uint32
DALFW_Release(DALClientCtxt *pdalClientCtxt);


void
DALFW_SystemAttach(DALDevCtxt *pDevCtxt);

void
DALFW_SystemDetach(DALDevCtxt *pDevCtxt);

DALSYSWorkLoopHandle *
DALFW_GetWorkLoop(DALDevCtxt *pDevCtxt, uint32 dwWorkLoopPriority);






DALMemObject *
DALFW_CopyMemObjectToDDI(DALSYSMemHandle hMem, DALMemObject * pDestMemObject);

DALSYSMemHandle
DALFW_CreateMemObjectFromDDI(uint32 dwMarshalFlags, DALMemObject * pInMemObject);

DALSYSEventHandle
DALFW_CreateEventFromDDI(DALClientCtxt * pClientCtxt, uint32 dwMarshalFlags,
                DALSYSEventHandle inHandle, DALEventObject * pPrealloc);

DALSYSEventHandle
DALFW_CreatePayloadEventFromDDI(DALClientCtxt * pClientCtxt, uint32 dwMarshalFlags,
                DALSYSEventHandle inHandle, DALEventObject * pPrealloc);

uint32
DALFW_InitDDIMemDescListFromSys(uint32 dwFlags,
                             DALSysMemDescList *pInMemDescList,
                             DALDDIMemDescList * pDestDDIMemDescList);

DALSysMemDescList *
DALFW_InitSysMemDescListFromDDI(DALSYSMemHandle hMemHandle,
                             DALDDIMemDescList *pInDDIMemDescList,
                             DALSysMemDescList *pDestMemDescList);
void
DALFW_RegisterSystemNotificationEvent(DalDeviceHandle * h, DALSYSEventHandle hEvent);

void memory_barrier(void);
# 232 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALFramework.h"
DALSysMemDescBuf *
DALFW_MemDescBufPtr(DALSysMemDescList * pMemDescList, uint32 idx);
# 248 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALFramework.h"
DALResult
DALFW_MemDescInit(DALSYSMemHandle hMem, DALSysMemDescList * pMemDescList,
                  uint32 dwNumBufs);
# 266 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALFramework.h"
DALResult
DALFW_MemDescAddBuf(DALSysMemDescList * pMemDescList, uint32 bufIdx,
                    uint32 offset, uint32 size, uint32 userInfo);
# 284 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALFramework.h"
DALSYSMemHandle
DALFW_AllocPhysForMemDescBufs(DALSYSMemHandle hMem,
                              uint32 operation,
                              DALSysMemDescList *pInDescList,
                              DALSysMemDescList ** pOutDescList);






DALResult
DALFW_MemDescListCopyBufs(DALSysMemDescList * pDestDescList,
                          DALSysMemDescList * pSrcDescList);
# 322 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALFramework.h"
uint32 DALFW_LockedExchangeW(volatile uint32 *pTarget, uint32 value);






uint32 DALFW_LockedIncrementW(volatile uint32 *pTarget);






uint32 DALFW_LockedDecrementW(volatile uint32 *pTarget);







uint32 DALFW_LockedGetModifySetW(volatile uint32 *pTarget, uint32 AND_value, uint32 OR_value);
# 357 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALFramework.h"
uint32 DALFW_LockedCompareExchangeW(volatile uint32 *pTarget, uint32 comparand, uint32 value);
# 370 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALFramework.h"
void DALFW_LockSpinLock(volatile uint32 *spinLock, uint32 pid);
# 379 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALFramework.h"
void DALFW_UnLockSpinLock(volatile uint32 *spinLock);
# 392 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALFramework.h"
void DALFW_LockSpinLockExt(volatile uint32 *spinLock, uint32 pid);







void DALFW_UnLockSpinLockExt(volatile uint32 *spinLock);
# 409 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALFramework.h"
DALResult DALFW_TryLockSpinLockExt(volatile uint32 *spinLock, uint32 pid);






typedef struct _DAFFW_MPLOCK
{
  volatile uint32 spin_lock;
  volatile uint32 owner;
  volatile uint32 waiting;
   volatile uint32 size;
}
DALFW_MPLOCK;






void DALFW_MPLock(DALFW_MPLOCK *pLock, uint32 pid, uint32 delayParam);






void DALFW_MPUnLock(DALFW_MPLOCK *pLock);






uint32 DALFW_TrySpinLockEx(DALFW_MPLOCK *pLock, uint32 pid);
# 18 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/InterruptController/inc/DALInterruptController.h" 2
# 33 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/InterruptController/inc/DALInterruptController.h"
typedef struct InterruptControllerDrvCtxt InterruptControllerDrvCtxt;
typedef struct InterruptControllerDevCtxt InterruptControllerDevCtxt;
typedef struct InterruptControllerClientCtxt InterruptControllerClientCtxt;




typedef struct InterruptControllerDALVtbl InterruptControllerDALVtbl;
struct InterruptControllerDALVtbl
{
   int (*InterruptController_DriverInit)(InterruptControllerDrvCtxt *);
   int (*InterruptController_DriverDeInit)(InterruptControllerDrvCtxt *);
};

struct InterruptControllerDevCtxt
{

   uint32 dwRefs;
    DALDEVICEID DevId;
    uint32 dwDevCtxtRefIdx;
    InterruptControllerDrvCtxt *pInterruptControllerDrvCtxt;
   uint32 hProp[2];
    uint32 Reserved[16];

};

struct InterruptControllerDrvCtxt
{

   InterruptControllerDALVtbl InterruptControllerDALVtbl;
   uint32 dwNumDev;
   uint32 dwSizeDevCtxt;
   uint32 bInit;
    uint32 dwRefs;
   InterruptControllerDevCtxt InterruptControllerDevCtxt[1];

};




struct InterruptControllerClientCtxt
{

   uint32 dwRefs;
    uint32 dwAccessMode;
    void *pPortCtxt;
    InterruptControllerDevCtxt *pInterruptControllerDevCtxt;
    DalInterruptControllerHandle DalInterruptControllerHandle;




   DALSYSSyncHandle hSyncIntrCtrlTbl;
   uint32 IntrCtrlTblNextSlot;
   void * IntrCtrlTbl[512];
};

DALResult InterruptController_DriverInit(InterruptControllerDrvCtxt *);
DALResult InterruptController_DriverDeInit(InterruptControllerDrvCtxt *);
DALResult InterruptController_DeviceInit(InterruptControllerClientCtxt *);
DALResult InterruptController_DeviceDeInit(InterruptControllerClientCtxt *);
DALResult InterruptController_Reset(InterruptControllerClientCtxt *);
DALResult InterruptController_PowerEvent(InterruptControllerClientCtxt *, DalPowerCmd, DalPowerDomain);
DALResult InterruptController_Open(InterruptControllerClientCtxt *, uint32);
DALResult InterruptController_Close(InterruptControllerClientCtxt *);
DALResult InterruptController_Info(InterruptControllerClientCtxt *,DalDeviceInfo *, uint32);
DALResult InterruptController_InheritObjects(InterruptControllerClientCtxt *,DALInheritSrcPram *,DALInheritDestPram *);






DALResult InterruptController_RegisterISR( InterruptControllerClientCtxt *, DALInterruptID , const DALISR , const DALISRCtx , uint32 );
DALResult InterruptController_RegisterIST( InterruptControllerClientCtxt *,DALInterruptID , const DALISR , const DALISRCtx , uint32, char * );
DALResult InterruptController_RegisterEvent( InterruptControllerClientCtxt *, DALInterruptID , const DALSYSEventHandle , uint32 );
DALResult InterruptController_Unregister( InterruptControllerClientCtxt *, DALInterruptID );
DALResult InterruptController_InterruptDone( InterruptControllerClientCtxt *, DALInterruptID );
DALResult InterruptController_InterruptEnable( InterruptControllerClientCtxt *, DALInterruptID );
DALResult InterruptController_InterruptDisable( InterruptControllerClientCtxt *, DALInterruptID );
DALResult InterruptController_InterruptTrigger( InterruptControllerClientCtxt *, DALInterruptID );
DALResult InterruptController_InterruptClear( InterruptControllerClientCtxt *, DALInterruptID );
DALResult InterruptController_InterruptStatus( InterruptControllerClientCtxt *, DALInterruptID );
DALResult InterruptController_SetTrigger(InterruptControllerClientCtxt *, DALInterruptID ,uint32 );
DALResult InterruptController_IsInterruptPending(InterruptControllerClientCtxt *, DALInterruptID ,uint32*);
DALResult InterruptController_IsInterruptEnabled(InterruptControllerClientCtxt *, DALInterruptID,uint32*);
DALResult InterruptController_IsAnyInterruptPending(InterruptControllerClientCtxt *,uint32* bState);
DALResult InterruptController_MapWakeupInterrupt(InterruptControllerClientCtxt * , DALInterruptID ,uint32 );
DALResult InterruptController_Sleep(InterruptControllerClientCtxt * , InterruptControllerSleepType sleep);
DALResult InterruptController_Wakeup(InterruptControllerClientCtxt * , InterruptControllerSleepType sleep);
DALResult InterruptController_GetInterruptTrigger(InterruptControllerClientCtxt *, DALInterruptID ,uint32* );
DALResult InterruptController_LogState(InterruptControllerClientCtxt *,void *);
DALResult InterruptController_GetInterruptID(InterruptControllerClientCtxt *pclientCtxt,const char* szIntrName,uint32* pnIntrID);
# 40 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/InterruptController/src/qurt/kernel/DALInterruptControllerConfig.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/atomic_ops.h" 1
# 104 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/atomic_ops.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_atomic_ops.h" 1
# 113 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_atomic_ops.h"
static inline unsigned int
qurt_atomic_set(unsigned int* target, unsigned int value)
{
    unsigned long tmp;

    __asm__ __volatile__(
        "1:     %0 = memw_locked(%2)\n"
        "       memw_locked(%2, p0) = %3\n"
        "       if !p0 jump 1b\n"
        : "=&r" (tmp),"+m" (*target)
        : "r" (target), "r" (value)
        : "p0");
    return value;
}


static inline void
qurt_atomic_and(unsigned int* target, unsigned int mask)
{
    unsigned int result;

    __asm__ __volatile__(
        "1:     %0 = memw_locked(%2)\n"
        "       %0 = and(%0, %3)\n"
        "       memw_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*target)
        : "r" (target),"r" (mask)
        : "p0");
}

static inline unsigned int
qurt_atomic_and_return(unsigned int* target, unsigned int mask)
{
    unsigned int result;

    __asm__ __volatile__(
        "1:     %0 = memw_locked(%2)\n"
        "       %0 = and(%0, %3)\n"
        "       memw_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*target)
        : "r" (target), "r" (mask)
        : "p0");

    return result;
}

static inline void
qurt_atomic_or(unsigned int* target, unsigned int mask)
{
    unsigned int result;

    __asm__ __volatile__(
        "1:     %0 = memw_locked(%2)\n"
        "       %0 = or(%0, %3)\n"
        "       memw_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*target)
        : "r" (target), "r" (mask)
        : "p0");
}

static inline unsigned int
qurt_atomic_or_return(unsigned int* target, unsigned int mask)
{
    unsigned int result;

    __asm__ __volatile__(
        "1:     %0 = memw_locked(%2)\n"
        "       %0 = or(%0, %3)\n"
        "       memw_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*target)
        : "r" (target), "r" (mask)
        : "p0");

    return result;
}

static inline void
qurt_atomic_xor(unsigned int* target, unsigned int mask)
{
    unsigned int result;

    __asm__ __volatile__(
        "1:     %0 = memw_locked(%2)\n"
        "       %0 = xor(%0, %3)\n"
        "       memw_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*target)
        : "r" (target), "r" (mask)
        : "p0");
}

static inline unsigned int
qurt_atomic_xor_return(unsigned int* target, unsigned int mask)
{
    unsigned int result;

    __asm__ __volatile__(
        "1:     %0 = memw_locked(%2)\n"
        "       %0 = xor(%0, %3)\n"
        "       memw_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*target)
        : "r" (target), "r" (mask)
        : "p0");

    return result;
}



static inline void
qurt_atomic_set_bit(unsigned int *target, unsigned int bit)
{
    unsigned int result;
  int aword = bit / (sizeof(unsigned int) * 8);
    unsigned int sbit = bit % (sizeof(unsigned int) * 8);
  unsigned int *wtarget= (unsigned int *)&target[aword];

    __asm__ __volatile__(
        "1:     %0 = memw_locked(%2)\n"
        "       %0 = setbit(%0, %3)\n"
        "       memw_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*wtarget)
        : "r" (wtarget), "r" (sbit)
        : "p0");
}

static inline void
qurt_atomic_clear_bit(unsigned int *target, unsigned int bit)
{
    unsigned int result;
  int aword = bit / (sizeof(unsigned int) * 8);
    unsigned int sbit = bit % (sizeof(unsigned int) * 8);
  unsigned int *wtarget= (unsigned int *)&target[aword];

    __asm__ __volatile__(
        "1:     %0 = memw_locked(%2)\n"
        "       %0 = clrbit(%0, %3)\n"
        "       memw_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*wtarget)
        : "r" (wtarget), "r" (sbit)
        : "p0");
}

static inline void
qurt_atomic_change_bit(unsigned int *target, unsigned int bit)
{
    unsigned int result;
  int aword = bit / (sizeof(unsigned int) * 8);
    unsigned int sbit = bit & 0x1f;
  unsigned int *wtarget= (unsigned int *)&target[aword];

    __asm__ __volatile__(
        "1:     %0 = memw_locked(%2)\n"
        "       %0 = togglebit(%0, %3)\n"
        "       memw_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*wtarget)
        : "r" (wtarget),"r" (sbit)
        : "p0");
}



static inline void
qurt_atomic_add(unsigned int *target, unsigned int v)
{
    unsigned int result;

    __asm__ __volatile__(
        "1:     %0 = memw_locked(%2)\n"
        "       %0 = add(%0, %3)\n"
        "       memw_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*target)
        : "r" (target), "r" (v)
        : "p0");
}

static inline unsigned int
qurt_atomic_add_return(unsigned int *target, unsigned int v)
{
    unsigned int result;

    __asm__ __volatile__(
        "1:     %0 = memw_locked(%2)\n"
        "       %0 = add(%0, %3)\n"
        "       memw_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*target)
        : "r" (target), "r" (v)
        : "p0");

    return result;
}

static inline void
qurt_atomic_sub(unsigned int *target, unsigned int v)
{
    unsigned int result;

    __asm__ __volatile__(
        "1:     %0 = memw_locked(%2)\n"
        "       %0 = sub(%0, %3)\n"
        "       memw_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*target)
        : "r" (target), "r" (v)
        : "p0");
}

static inline unsigned int
qurt_atomic_sub_return(unsigned int *target, unsigned int v)
{
    unsigned int result;

    __asm__ __volatile__(
        "1:     %0 = memw_locked(%2)\n"
        "       %0 = sub(%0, %3)\n"
        "       memw_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*target)
        : "r" (target), "r" (v)
        : "p0");

    return result;
}

static inline void
qurt_atomic_inc(unsigned int *target)
{
    unsigned int result;

    __asm__ __volatile__(
        "1:     %0 = memw_locked(%2)\n"
        "       %0 = add(%0, #1)\n"
        "       memw_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*target)
        : "r" (target)
        : "p0");
}

static inline unsigned int
qurt_atomic_inc_return(unsigned int *target)
{
    unsigned int result;

    __asm__ __volatile__(
        "1:     %0 = memw_locked(%2)\n"
        "       %0 = add(%0, #1)\n"
        "       memw_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*target)
        : "r" (target)
        : "p0");

    return result;
}


static inline void
qurt_atomic_dec(unsigned int *target)
{
    unsigned int result;

    __asm__ __volatile__(
        "1:     %0 = memw_locked(%2)\n"
        "       %0 = add(%0, #-1)\n"
        "       memw_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*target)
        : "r" (target)
        : "p0");
}

static inline unsigned int
qurt_atomic_dec_return(unsigned int *target)
{
    unsigned int result;

    __asm__ __volatile__(
        "1:     %0 = memw_locked(%2)\n"
        "       %0 = add(%0, #-1)\n"
        "       memw_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*target)
        : "r" (target)
        : "p0");

    return result;
}



static inline unsigned int
qurt_atomic_compare_and_set(unsigned int* target,
                       unsigned int old_val,
                       unsigned int new_val)
{
    unsigned int current_val;

    __asm__ __volatile__(
        "1:     %0 = memw_locked(%2)\n"
        "       p0 = cmp.eq(%0, %3)\n"
        "       if !p0 jump 2f\n"
        "       memw_locked(%2, p0) = %4\n"
        "       if !p0 jump 1b\n"
        "2:\n"
        : "=&r" (current_val),"+m" (*target)
        : "r" (target), "r" (old_val), "r" (new_val)
        : "p0");

    return current_val == old_val;
}

static inline void
qurt_atomic_barrier(void)
{
    __asm__ __volatile__ (
        ""
        :
        :
        :
        "memory");
}

static inline void
qurt_atomic_barrier_write(void)
{
    qurt_atomic_barrier();
}

static inline void
qurt_atomic_barrier_write_smp(void)
{
    qurt_atomic_barrier();
}

static inline void
qurt_atomic_barrier_read(void)
{
    qurt_atomic_barrier();
}

static inline void
qurt_atomic_barrier_read_smp(void)
{
    qurt_atomic_barrier();
}

static inline void
qurt_atomic_barrier_smp(void)
{
    qurt_atomic_barrier();
}







static inline unsigned long long
qurt_atomic64_set(unsigned long long* target, unsigned long long value)
{
    unsigned long long tmp;

    __asm__ __volatile__(
        "1:     %0 = memd_locked(%2)\n"
        "       memd_locked(%2, p0) = %3\n"
        "       if !p0 jump 1b\n"
        : "=&r" (tmp),"+m" (*target)
        : "r" (target), "r" (value)
        : "p0");
    return value;
}


static inline void
qurt_atomic64_and(unsigned long long* target, unsigned long long mask)
{
    unsigned long long result;

    __asm__ __volatile__(
        "1:     %0 = memd_locked(%2)\n"
        "       %0 = and(%0, %3)\n"
        "       memd_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*target)
        : "r" (target),"r" (mask)
        : "p0");
}

static inline unsigned long long
qurt_atomic64_and_return(unsigned long long* target, unsigned long long mask)
{
    unsigned long long result;

    __asm__ __volatile__(
        "1:     %0 = memd_locked(%2)\n"
        "       %0 = and(%0, %3)\n"
        "       memd_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*target)
        : "r" (target), "r" (mask)
        : "p0");

    return result;
}

static inline void
qurt_atomic64_or(unsigned long long* target, unsigned long long mask)
{
    unsigned long long result;

    __asm__ __volatile__(
        "1:     %0 = memd_locked(%2)\n"
        "       %0 = or(%0, %3)\n"
        "       memd_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*target)
        : "r" (target), "r" (mask)
        : "p0");
}

static inline unsigned long long
qurt_atomic64_or_return(unsigned long long* target, unsigned long long mask)
{
    unsigned long long result;

    __asm__ __volatile__(
        "1:     %0 = memd_locked(%2)\n"
        "       %0 = or(%0, %3)\n"
        "       memd_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*target)
        : "r" (target), "r" (mask)
        : "p0");

    return result;
}

static inline void
qurt_atomic64_xor(unsigned long long* target, unsigned long long mask)
{
    unsigned long long result;

    __asm__ __volatile__(
        "1:     %0 = memd_locked(%2)\n"
        "       %0 = xor(%0, %3)\n"
        "       memd_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*target)
        : "r" (target), "r" (mask)
        : "p0");
}

static inline unsigned long long
qurt_atomic64_xor_return(unsigned long long* target, unsigned long long mask)
{
    unsigned long long result;

    __asm__ __volatile__(
        "1:     %0 = memd_locked(%2)\n"
        "       %0 = xor(%0, %3)\n"
        "       memd_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*target)
        : "r" (target), "r" (mask)
        : "p0");

    return result;
}



static inline void
qurt_atomic64_set_bit(unsigned long long *target, unsigned int bit)
{
    unsigned int result;
  unsigned int *wtarget;
  unsigned int *pwtarget = (unsigned int *)target;
  int aword = bit / (sizeof(unsigned int) * 8);
    unsigned int sbit = bit & 0x1F;
  wtarget = (unsigned int *)&pwtarget[aword];


    __asm__ __volatile__(
        "1:     %0 = memw_locked(%2)\n"
        "       %0 = setbit(%0, %3)\n"
        "       memw_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*wtarget)
        : "r" (wtarget), "r" (sbit)
        : "p0");
}

static inline void
qurt_atomic64_clear_bit(unsigned long long *target, unsigned int bit)
{
    unsigned int result;
  unsigned int *wtarget;
  unsigned int *pwtarget = (unsigned int *)target;
  int aword = bit / (sizeof(unsigned int) * 8);
    unsigned int sbit = bit & 0x1F;
  wtarget = (unsigned int *)&pwtarget[aword];

    __asm__ __volatile__(
        "1:     %0 = memw_locked(%2)\n"
        "       %0 = clrbit(%0, %3)\n"
        "       memw_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*wtarget)
        : "r" (wtarget), "r" (sbit)
        : "p0");
}

static inline void
qurt_atomic64_change_bit(unsigned long long *target, unsigned int bit)
{
    unsigned int result;
  unsigned int *wtarget;
  unsigned int *pwtarget = (unsigned int *)target;
  int aword = bit / (sizeof(unsigned int) * 8);
    unsigned int sbit = bit & 0x1F;
  wtarget = (unsigned int *)&pwtarget[aword];

    __asm__ __volatile__(
        "1:     %0 = memw_locked(%2)\n"
        "       %0 = togglebit(%0, %3)\n"
        "       memw_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*wtarget)
        : "r" (wtarget),"r" (sbit)
        : "p0");
}



static inline void
qurt_atomic64_add(unsigned long long *target, unsigned long long v)
{
    unsigned long long result;

    __asm__ __volatile__(
        "1:     %0 = memd_locked(%2)\n"
        "       %0 = add(%0, %3)\n"
        "       memd_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*target)
        : "r" (target), "r" (v)
        : "p0");
}

static inline unsigned long long
qurt_atomic64_add_return(unsigned long long *target, unsigned long long v)
{
    unsigned long long result;

    __asm__ __volatile__(
        "1:     %0 = memd_locked(%2)\n"
        "       %0 = add(%0, %3)\n"
        "       memd_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*target)
        : "r" (target), "r" (v)
        : "p0");

    return result;
}

static inline void
qurt_atomic64_sub(unsigned long long *target, unsigned long long v)
{
    unsigned long long result;

    __asm__ __volatile__(
        "1:     %0 = memd_locked(%2)\n"
        "       %0 = sub(%0, %3)\n"
        "       memd_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*target)
        : "r" (target), "r" (v)
        : "p0");
}

static inline unsigned long long
qurt_atomic64_sub_return(unsigned long long *target, unsigned long long v)
{
    unsigned long long result;

    __asm__ __volatile__(
        "1:     %0 = memd_locked(%2)\n"
        "       %0 = sub(%0, %3)\n"
        "       memd_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*target)
        : "r" (target), "r" (v)
        : "p0");

    return result;
}

static inline void
qurt_atomic64_inc(unsigned long long *target)
{
    unsigned long long result;
  unsigned long long inc =1;

    __asm__ __volatile__(
        "1:     %0 = memd_locked(%2)\n"
        "       %0 = add(%0, %3)\n"
        "       memd_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*target)
        : "r" (target),"r" (inc)
        : "p0");
}

static inline unsigned long long
qurt_atomic64_inc_return(unsigned long long *target)
{
    unsigned long long result;
  unsigned long long inc =1;

    __asm__ __volatile__(
        "1:     %0 = memd_locked(%2)\n"
        "       %0 = add(%0, %3)\n"
        "       memd_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*target)
        : "r" (target),"r" (inc)
        : "p0");

    return result;
}


static inline void
qurt_atomic64_dec(unsigned long long *target)
{
    unsigned long long result;
  unsigned long long minus1 = -1;

    __asm__ __volatile__(
        "1:     %0 = memd_locked(%2)\n"
        "       %0 = add(%0, %3)\n"
        "       memd_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*target)
        : "r" (target),"r" (minus1)
        : "p0");
}

static inline unsigned long long
qurt_atomic64_dec_return(unsigned long long *target)
{
    unsigned long long result;
  unsigned long long minus1 = -1;

    __asm__ __volatile__(
        "1:     %0 = memd_locked(%2)\n"
        "       %0 = add(%0, %3)\n"
        "       memd_locked(%2, p0) = %0\n"
        "       if !p0 jump 1b\n"
        : "=&r" (result),"+m" (*target)
        : "r" (target),"r" (minus1)
        : "p0");

    return result;
}



static inline int
qurt_atomic64_compare_and_set(unsigned long long *target,
                       unsigned long long old_val,
                       unsigned long long new_val)
{
    unsigned long long current_val;

    __asm__ __volatile__(
        "1:     %0 = memd_locked(%2)\n"
        "       p0 = cmp.eq(%0, %3)\n"
        "       if !p0 jump 2f\n"
        "       memd_locked(%2, p0) = %4\n"
        "       if !p0 jump 1b\n"
        "2:\n"
        : "=&r" (current_val),"+m" (*target)
        : "r" (target), "r" (old_val), "r" (new_val)
        : "p0");

    return current_val ==old_val;
}

static inline void
qurt_atomic64_barrier(void)
{
    __asm__ __volatile__ (
        ""
        :
        :
        :
        "memory");
}

static inline void
qurt_atomic64_barrier_write(void)
{
    qurt_atomic64_barrier();
}

static inline void
qurt_atomic64_barrier_write_smp(void)
{
    qurt_atomic64_barrier();
}

static inline void
qurt_atomic64_barrier_read(void)
{
    qurt_atomic64_barrier();
}

static inline void
qurt_atomic64_barrier_read_smp(void)
{
    qurt_atomic64_barrier();
}

static inline void
qurt_atomic64_barrier_smp(void)
{
    qurt_atomic64_barrier();
}
# 105 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/atomic_ops.h" 2
# 116 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/atomic_ops.h"
typedef unsigned int atomic_plain_word_t;
# 137 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/atomic_ops.h"
typedef struct {
    volatile atomic_plain_word_t value;
} atomic_word_t;



static inline void
atomic_init(atomic_word_t *a, atomic_plain_word_t v)
{
    a->value = v;
}
# 161 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/atomic_ops.h"
atomic_plain_word_t atomic_set(atomic_word_t* target,
                                      atomic_plain_word_t value);


void atomic_and(atomic_word_t* target,
                       atomic_plain_word_t mask);
atomic_plain_word_t atomic_and_return(atomic_word_t* target,
                                             atomic_plain_word_t mask);

void atomic_or(atomic_word_t* target,
                      atomic_plain_word_t mask);
atomic_plain_word_t atomic_or_return(atomic_word_t* target,
                                            atomic_plain_word_t mask);

void atomic_xor(atomic_word_t* target,
                       atomic_plain_word_t mask);
atomic_plain_word_t atomic_xor_return(atomic_word_t* target,
                                             atomic_plain_word_t mask);



void atomic_set_bit(atomic_word_t *target, unsigned int bit);
void atomic_clear_bit(atomic_word_t *target, unsigned int bit);
void atomic_change_bit(atomic_word_t *target, unsigned int bit);


void atomic_add(atomic_word_t *target, atomic_plain_word_t v);
atomic_plain_word_t atomic_add_return(atomic_word_t *target,
                                             atomic_plain_word_t v);

void atomic_sub(atomic_word_t *target, atomic_plain_word_t v);
atomic_plain_word_t atomic_sub_return(atomic_word_t *target,
                                             atomic_plain_word_t v);

void atomic_inc(atomic_word_t *target);
atomic_plain_word_t atomic_inc_return(atomic_word_t *target);

void atomic_dec(atomic_word_t *target);
atomic_plain_word_t atomic_dec_return(atomic_word_t *target);



int atomic_compare_and_set(atomic_word_t *target,
                                  atomic_plain_word_t old_val,
                                  atomic_plain_word_t new_val);



void atomic_barrier_write(void);
void atomic_barrier_write_smp(void);
void atomic_barrier_read(void);
void atomic_barrier_read_smp(void);
void atomic_barrier(void);
void atomic_barrier_smp(void);



static inline atomic_plain_word_t atomic_read(atomic_word_t *target)
{
    return target->value;
}

static inline void atomic_compiler_barrier(void)
{





    asm volatile (""::: "memory");

}


typedef unsigned long long atomic64_plain_word_t;

typedef struct {
    volatile atomic64_plain_word_t value;
} atomic64_word_t;

static inline void
atomic64_init(atomic64_word_t *a, atomic64_plain_word_t v)
{
    a->value = v;
}





atomic64_plain_word_t atomic64_set(atomic64_word_t* target,
                                      atomic64_plain_word_t value);


void atomic64_and(atomic64_word_t* target,
                       atomic64_plain_word_t mask);
atomic64_plain_word_t atomic64_and_return(atomic64_word_t* target,
                                             atomic64_plain_word_t mask);

void atomic64_or(atomic64_word_t* target,
                      atomic64_plain_word_t mask);
atomic64_plain_word_t atomic64_or_return(atomic64_word_t* target,
                                            atomic64_plain_word_t mask);

void atomic64_xor(atomic64_word_t* target,
                       atomic64_plain_word_t mask);
atomic64_plain_word_t atomic64_xor_return(atomic64_word_t* target,
                                             atomic64_plain_word_t mask);



void atomic64_set_bit(atomic64_word_t *target, unsigned int bit);
void atomic64_clear_bit(atomic64_word_t *target, unsigned int bit);
void atomic64_change_bit(atomic64_word_t *target, unsigned int bit);


void atomic64_add(atomic64_word_t *target, atomic64_plain_word_t v);
atomic64_plain_word_t atomic64_add_return(atomic64_word_t *target,
                                             atomic64_plain_word_t v);

void atomic64_sub(atomic64_word_t *target, atomic64_plain_word_t v);
atomic64_plain_word_t atomic64_sub_return(atomic64_word_t *target,
                                             atomic64_plain_word_t v);

void atomic64_inc(atomic64_word_t *target);
atomic64_plain_word_t atomic64_inc_return(atomic64_word_t *target);

void atomic64_dec(atomic64_word_t *target);
atomic64_plain_word_t atomic64_dec_return(atomic64_word_t *target);



int atomic64_compare_and_set(atomic64_word_t *target,
                                  atomic64_plain_word_t old_val,
                                  atomic64_plain_word_t new_val);



void atomic64_barrier_write(void);
void atomic64_barrier_write_smp(void);
void atomic64_barrier_read(void);
void atomic64_barrier_read_smp(void);
void atomic64_barrier(void);
void atomic64_barrier_smp(void);





static inline atomic64_plain_word_t atomic64_read(atomic64_word_t *target)
{
    return target->value;
}

static inline void atomic64_compiler_barrier(void)
{





    asm volatile (""::: "memory");

}






# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/atomic_ops_plat.h" 1
# 331 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/atomic_ops.h" 2
# 41 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/InterruptController/src/qurt/kernel/DALInterruptControllerConfig.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_anysignal.h" 1
# 17 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_anysignal.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_signal.h" 1
# 30 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_signal.h"
typedef union {

    unsigned long long int raw;
    struct {
        unsigned int signals;
        unsigned int waiting;
        unsigned int queue;
        unsigned int attribute;
    }X;

} qurt_signal_t;
# 68 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_signal.h"
void qurt_signal_init(qurt_signal_t *signal);
# 91 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_signal.h"
void qurt_signal_destroy(qurt_signal_t *signal);
# 128 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_signal.h"
unsigned int qurt_signal_wait(qurt_signal_t *signal, unsigned int mask,
                unsigned int attribute);
# 157 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_signal.h"
static inline unsigned int qurt_signal_wait_any(qurt_signal_t *signal, unsigned int mask)
{
  return qurt_signal_wait(signal, mask, 0x00000000);
}
# 188 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_signal.h"
static inline unsigned int qurt_signal_wait_all(qurt_signal_t *signal, unsigned int mask)
{
  return qurt_signal_wait(signal, mask, 0x00000001);
}
# 214 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_signal.h"
void qurt_signal_set(qurt_signal_t *signal, unsigned int mask);
# 234 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_signal.h"
unsigned int qurt_signal_get(qurt_signal_t *signal);
# 259 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_signal.h"
void qurt_signal_clear(qurt_signal_t *signal, unsigned int mask);
# 298 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_signal.h"
int qurt_signal_wait_cancellable(qurt_signal_t *signal, unsigned int mask,
                                 unsigned int attribute,
                                 unsigned int *return_mask);
# 18 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_anysignal.h" 2






typedef qurt_signal_t qurt_anysignal_t;
# 47 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_anysignal.h"
static inline void qurt_anysignal_init(qurt_anysignal_t *signal)
{
  qurt_signal_init(signal);
}
# 73 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_anysignal.h"
static inline void qurt_anysignal_destroy(qurt_anysignal_t *signal)
{
  qurt_signal_destroy(signal);
}
# 105 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_anysignal.h"
static inline unsigned int qurt_anysignal_wait(qurt_anysignal_t *signal, unsigned int mask)
{
  return qurt_signal_wait(signal, mask, 0x00000000);
}
# 130 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_anysignal.h"
unsigned int qurt_anysignal_set(qurt_anysignal_t *signal, unsigned int mask);
# 151 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_anysignal.h"
static inline unsigned int qurt_anysignal_get(qurt_anysignal_t *signal)
{
  return qurt_signal_get(signal);
}
# 178 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_anysignal.h"
unsigned int qurt_anysignal_clear(qurt_anysignal_t *signal, unsigned int mask);
# 42 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/InterruptController/src/qurt/kernel/DALInterruptControllerConfig.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread.h" 1
# 20 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread.h"
# 1 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/string.h" 1 3 4
# 21 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread.h" 2
# 83 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread.h"
typedef enum {
    CCCC_PARTITION = 0,
    MAIN_PARTITION = 1,
    AUX_PARTITION = 2,
    MINIMUM_PARTITION = 3
} qurt_cache_partition_t;


typedef unsigned int qurt_thread_t;


typedef struct _qurt_thread_attr {

    char name[16];
    unsigned char tcb_partition;

    unsigned char affinity;

    unsigned short priority;
    unsigned char asid;
    unsigned char bus_priority;
    unsigned short timetest_id;
    unsigned int stack_size;
    void *stack_addr;


} qurt_thread_attr_t;
# 142 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread.h"
static inline void qurt_thread_attr_init (qurt_thread_attr_t *attr)
{

    attr->name[0] = 0;
    attr->tcb_partition = 0;
    attr->priority = 255;
    attr->asid = 0;
    attr->affinity = (-1);
    attr->bus_priority = 255;
    attr->timetest_id = (-2);
    attr->stack_size = 0;
    attr->stack_addr = 0;
}
# 175 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread.h"
static inline void qurt_thread_attr_set_name (qurt_thread_attr_t *attr, char *name)
{
    strlcpy (attr->name, name, 16);
    attr->name[16 - 1] = 0;
}
# 201 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread.h"
static inline void qurt_thread_attr_set_tcb_partition (qurt_thread_attr_t *attr, unsigned char tcb_partition)
{
    attr->tcb_partition = tcb_partition;
}
# 223 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread.h"
static inline void qurt_thread_attr_set_priority (qurt_thread_attr_t *attr, unsigned short priority)
{
    attr->priority = priority;
}
# 255 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread.h"
static inline void qurt_thread_attr_set_affinity (qurt_thread_attr_t *attr, unsigned char affinity)
{
    attr->affinity = affinity;
}
# 280 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread.h"
static inline void qurt_thread_attr_set_timetest_id (qurt_thread_attr_t *attr, unsigned short timetest_id)
{
    attr->timetest_id = timetest_id;
}
# 307 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread.h"
static inline void qurt_thread_attr_set_stack_size (qurt_thread_attr_t *attr, unsigned int stack_size)
{
    attr->stack_size = stack_size;
}
# 337 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread.h"
static inline void qurt_thread_attr_set_stack_addr (qurt_thread_attr_t *attr, void *stack_addr)
{
    attr->stack_addr = stack_addr;
}
# 364 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread.h"
static inline void qurt_thread_attr_set_bus_priority ( qurt_thread_attr_t *attr, unsigned short bus_priority)
{
    attr->bus_priority = bus_priority;
}
# 384 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread.h"
void qurt_thread_get_name (char *name, unsigned char max_len);
# 411 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread.h"
int qurt_thread_create (qurt_thread_t *thread_id, qurt_thread_attr_t *attr, void (*entrypoint) (void *), void *arg);
# 422 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread.h"
void qurt_thread_stop(void);
# 437 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread.h"
int qurt_thread_resume(unsigned int thread_id);
# 449 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread.h"
qurt_thread_t qurt_thread_get_id (void);
# 465 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread.h"
qurt_cache_partition_t qurt_thread_get_l2cache_partition (void);
# 482 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread.h"
void qurt_thread_set_timetest_id (unsigned short tid);
# 501 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread.h"
void qurt_thread_set_cache_partition(qurt_cache_partition_t l1_icache, qurt_cache_partition_t l1_dcache, qurt_cache_partition_t l2_cache);
# 536 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread.h"
void qurt_thread_set_coprocessor(unsigned int enable, unsigned int coproc_id);
# 551 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread.h"
unsigned short qurt_thread_get_timetest_id (void);
# 572 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread.h"
void qurt_thread_exit(int status);
# 596 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread.h"
int qurt_thread_join(unsigned int tid, int *status);
# 611 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread.h"
unsigned int qurt_thread_get_anysignal(void);
# 632 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread.h"
int qurt_thread_get_priority (qurt_thread_t threadid);
# 655 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread.h"
int qurt_thread_set_priority (qurt_thread_t threadid, unsigned short newprio);
# 666 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread.h"
unsigned int qurt_api_version(void);
# 685 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread.h"
int qurt_thread_attr_get (qurt_thread_t thread_id, qurt_thread_attr_t *attr);
# 43 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/InterruptController/src/qurt/kernel/DALInterruptControllerConfig.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/InterruptController/inc/DALInterruptController_utils.h" 1
# 18 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/InterruptController/inc/DALInterruptController_utils.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 1
# 40 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_consts.h" 1
# 41 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_alloc.h" 1
# 54 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_alloc.h"
void *qurt_malloc( unsigned int size);
# 75 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_alloc.h"
void *qurt_calloc(unsigned int elsize, unsigned int num);
# 100 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_alloc.h"
void *qurt_realloc(void *ptr, int newsize);
# 119 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_alloc.h"
void qurt_free( void *ptr);
# 42 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_futex.h" 1
# 23 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_futex.h"
int qurt_futex_wait(void *lock, int val);
int qurt_futex_wait_cancellable(void *lock, int val);
int qurt_futex_wait64(void *lock, long long val);
int qurt_futex_wake(void *lock, int n_to_wake);
# 43 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_mutex.h" 1
# 34 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_mutex.h"
typedef union qurt_mutex_aligned8{

    struct {
        unsigned int holder;
        unsigned int count;
        unsigned int queue;
        unsigned int wait_count;
    };
    unsigned long long int raw;

} qurt_mutex_t;
# 81 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_mutex.h"
void qurt_mutex_init(qurt_mutex_t *lock);
# 103 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_mutex.h"
void qurt_mutex_destroy(qurt_mutex_t *lock);
# 129 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_mutex.h"
void qurt_mutex_lock(qurt_mutex_t *lock);
# 151 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_mutex.h"
void qurt_mutex_unlock(qurt_mutex_t *lock);
# 175 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_mutex.h"
int qurt_mutex_try_lock(qurt_mutex_t *lock);
# 44 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pipe.h" 1
# 24 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pipe.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_sem.h" 1
# 28 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_sem.h"
typedef union {

 unsigned int raw[2] __attribute__((aligned(8)));
 struct {
  unsigned short val;
  unsigned short n_waiting;
        unsigned int reserved1;
        unsigned int queue;
        unsigned int reserved2;
 }X;
} qurt_sem_t;
# 71 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_sem.h"
int qurt_sem_add(qurt_sem_t *sem, unsigned int amt);
# 94 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_sem.h"
static inline int qurt_sem_up(qurt_sem_t *sem) { return qurt_sem_add(sem,1); };
# 117 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_sem.h"
int qurt_sem_down(qurt_sem_t *sem);
# 142 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_sem.h"
int qurt_sem_try_down(qurt_sem_t *sem);
# 157 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_sem.h"
void qurt_sem_init(qurt_sem_t *sem);
# 177 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_sem.h"
void qurt_sem_destroy(qurt_sem_t *sem);
# 195 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_sem.h"
void qurt_sem_init_val(qurt_sem_t *sem, unsigned short val);
# 212 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_sem.h"
static inline unsigned short qurt_sem_get_val(qurt_sem_t *sem ){return sem->X.val;}
int qurt_sem_down_cancellable(qurt_sem_t *sem);
# 25 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pipe.h" 2
# 39 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pipe.h"
typedef unsigned long long int qurt_pipe_data_t;


typedef struct {

    qurt_mutex_t pipe_lock;
    qurt_sem_t senders;
    qurt_sem_t receiver;
    unsigned int size;
    unsigned int sendidx;
    unsigned int recvidx;
    void (*lock_func)(qurt_mutex_t *);
    void (*unlock_func)(qurt_mutex_t *);
    int (*try_lock_func)(qurt_mutex_t *);
    void (*destroy_lock_func)(qurt_mutex_t *);
    unsigned int magic;
    qurt_pipe_data_t *data;

} qurt_pipe_t;


typedef struct {

  qurt_pipe_data_t *buffer;
  unsigned int elements;
  unsigned char mem_partition;

} qurt_pipe_attr_t;
# 94 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pipe.h"
static inline void qurt_pipe_attr_init(qurt_pipe_attr_t *attr)
{
  attr->buffer = 0;
  attr->elements = 0;
  attr->mem_partition = 0;
}
# 123 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pipe.h"
static inline void qurt_pipe_attr_set_buffer(qurt_pipe_attr_t *attr, qurt_pipe_data_t *buffer)
{
  attr->buffer = buffer;
}
# 151 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pipe.h"
static inline void qurt_pipe_attr_set_elements(qurt_pipe_attr_t *attr, unsigned int elements)
{
  attr->elements = elements;
}
# 178 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pipe.h"
static inline void qurt_pipe_attr_set_buffer_partition(qurt_pipe_attr_t *attr, unsigned char mem_partition)
{
  attr->mem_partition = mem_partition;
}
# 208 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pipe.h"
int qurt_pipe_create(qurt_pipe_t **pipe, qurt_pipe_attr_t *attr);
# 230 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pipe.h"
int qurt_pipe_init(qurt_pipe_t *pipe, qurt_pipe_attr_t *attr);
# 252 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pipe.h"
void qurt_pipe_destroy(qurt_pipe_t *pipe);
# 277 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pipe.h"
void qurt_pipe_delete(qurt_pipe_t *pipe);
# 303 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pipe.h"
void qurt_pipe_send(qurt_pipe_t *pipe, qurt_pipe_data_t data);
# 328 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pipe.h"
qurt_pipe_data_t qurt_pipe_receive(qurt_pipe_t *pipe);
# 356 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pipe.h"
int qurt_pipe_try_send(qurt_pipe_t *pipe, qurt_pipe_data_t data);
# 382 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pipe.h"
qurt_pipe_data_t qurt_pipe_try_receive(qurt_pipe_t *pipe, int *success);
# 416 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pipe.h"
int qurt_pipe_receive_cancellable(qurt_pipe_t *pipe, qurt_pipe_data_t *result);
# 447 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pipe.h"
int qurt_pipe_send_cancellable(qurt_pipe_t *pipe, qurt_pipe_data_t data);
# 466 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pipe.h"
int qurt_pipe_is_empty(qurt_pipe_t *pipe);
# 45 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_printf.h" 1
# 26 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_printf.h"
int qurt_printf(const char* format, ...);
# 46 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_assert.h" 1
# 26 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_assert.h"
void qurt_assert_error(const char *filename, int lineno) __attribute__((noreturn));
# 47 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2

# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_trace.h" 1
# 85 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_trace.h"
unsigned int qurt_trace_get_marker(void);
# 117 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_trace.h"
int qurt_trace_changed(unsigned int prev_trace_marker, unsigned int trace_mask);
# 172 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_trace.h"
unsigned int qurt_etm_set_config(unsigned int type, unsigned int route, unsigned int filter);
# 194 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_trace.h"
unsigned int qurt_etm_enable(unsigned int enable_flag);
# 211 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_trace.h"
unsigned int qurt_etm_testbus_set_config(unsigned int cfg_data);
# 246 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_trace.h"
unsigned int qurt_etm_set_breakpoint(unsigned int type, unsigned int address, unsigned int data, unsigned int mask);
# 276 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_trace.h"
unsigned int qurt_etm_set_breakarea(unsigned int type, unsigned int start_address, unsigned int end_address, unsigned int count);
# 49 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_cycles.h" 1
# 34 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_cycles.h"
void qurt_profile_reset_idle_pcycles (void);
# 48 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_cycles.h"
unsigned long long int qurt_profile_get_thread_pcycles(void);
# 62 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_cycles.h"
unsigned long long int qurt_profile_get_thread_tcycles(void);
# 84 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_cycles.h"
unsigned long long int qurt_get_core_pcycles(void);
# 110 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_cycles.h"
void qurt_profile_get_idle_pcycles (unsigned long long *pcycles);
# 133 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_cycles.h"
void qurt_profile_get_threadid_pcycles (int thread_id, unsigned long long *pcycles);
# 148 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_cycles.h"
void qurt_profile_reset_threadid_pcycles (int thread_id);
# 170 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_cycles.h"
void qurt_profile_enable (int enable);
# 50 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2

# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_cond.h" 1
# 33 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_cond.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_rmutex2.h" 1
# 32 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_rmutex2.h"
typedef struct {

   unsigned int holder __attribute__((aligned(8)));
   unsigned short waiters;
   unsigned short refs;
   unsigned int queue;
   unsigned int excess_locks;

} qurt_rmutex2_t;
# 66 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_rmutex2.h"
void qurt_rmutex2_init(qurt_rmutex2_t *lock);
# 88 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_rmutex2.h"
void qurt_rmutex2_destroy(qurt_rmutex2_t *lock);
# 118 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_rmutex2.h"
void qurt_rmutex2_lock(qurt_rmutex2_t *lock);
# 138 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_rmutex2.h"
void qurt_rmutex2_unlock(qurt_rmutex2_t *lock);
# 158 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_rmutex2.h"
int qurt_rmutex2_try_lock(qurt_rmutex2_t *lock);
# 34 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_cond.h" 2








typedef union {

    unsigned long long raw;
    struct {
        unsigned int count;
        unsigned int n_waiting;
        unsigned int queue;
        unsigned int reserved;
    }X;

} qurt_cond_t;
# 76 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_cond.h"
void qurt_cond_init(qurt_cond_t *cond);
# 97 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_cond.h"
void qurt_cond_destroy(qurt_cond_t *cond);
# 127 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_cond.h"
void qurt_cond_signal(qurt_cond_t *cond);
# 156 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_cond.h"
void qurt_cond_broadcast(qurt_cond_t *cond);
# 187 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_cond.h"
void qurt_cond_wait(qurt_cond_t *cond, qurt_mutex_t *mutex);
# 220 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_cond.h"
void qurt_cond_wait2(qurt_cond_t *cond, qurt_rmutex2_t *mutex);
# 52 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_barrier.h" 1
# 38 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_barrier.h"
typedef union {

 struct {
        unsigned short threads_left;
  unsigned short count;
  unsigned int threads_total;
        unsigned int queue;
        unsigned int reserved;
 };
 unsigned long long int raw;

} qurt_barrier_t;
# 75 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_barrier.h"
int qurt_barrier_init(qurt_barrier_t *barrier, unsigned int threads_total);
# 98 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_barrier.h"
int qurt_barrier_destroy(qurt_barrier_t *barrier);
# 129 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_barrier.h"
int qurt_barrier_wait(qurt_barrier_t *barrier);
# 53 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_fastint.h" 1
# 94 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_fastint.h"
unsigned int qurt_fastint_register(int intno, void (*fn)(int));
# 113 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_fastint.h"
unsigned int qurt_fastint_deregister(int intno);
# 136 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_fastint.h"
unsigned int qurt_isr_register(int intno, void (*fn)(int));
# 156 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_fastint.h"
unsigned int qurt_isr_deregister(int intno);
# 54 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_allsignal.h" 1
# 30 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_allsignal.h"
typedef union {

 unsigned long long int raw;
 struct {
  unsigned int waiting;
  unsigned int signals_in;
  unsigned int queue;
  unsigned int reserved;
 }X;

} qurt_allsignal_t;
# 64 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_allsignal.h"
void qurt_allsignal_init(qurt_allsignal_t *signal);
# 86 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_allsignal.h"
void qurt_allsignal_destroy(qurt_allsignal_t *signal);
# 106 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_allsignal.h"
static inline unsigned int qurt_allsignal_get(qurt_allsignal_t *signal)
{ return signal->X.signals_in; };
# 142 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_allsignal.h"
void qurt_allsignal_wait(qurt_allsignal_t *signal, unsigned int mask);
# 165 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_allsignal.h"
void qurt_allsignal_set(qurt_allsignal_t *signal, unsigned int mask);
# 55 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2


# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_rmutex.h" 1
# 49 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_rmutex.h"
void qurt_rmutex_init(qurt_mutex_t *lock);
# 70 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_rmutex.h"
void qurt_rmutex_destroy(qurt_mutex_t *lock);
# 100 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_rmutex.h"
void qurt_rmutex_lock(qurt_mutex_t *lock);
# 120 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_rmutex.h"
void qurt_rmutex_unlock(qurt_mutex_t *lock);
# 145 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_rmutex.h"
int qurt_rmutex_try_lock(qurt_mutex_t *lock);
# 165 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_rmutex.h"
int qurt_rmutex_try_lock_block_once(qurt_mutex_t *lock);
# 58 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pimutex.h" 1
# 50 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pimutex.h"
void qurt_pimutex_init(qurt_mutex_t *lock);
# 72 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pimutex.h"
void qurt_pimutex_destroy(qurt_mutex_t *lock);
# 110 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pimutex.h"
void qurt_pimutex_lock(qurt_mutex_t *lock);
# 134 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pimutex.h"
void qurt_pimutex_unlock(qurt_mutex_t *lock);
# 157 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pimutex.h"
int qurt_pimutex_try_lock(qurt_mutex_t *lock);
# 59 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_signal2.h" 1
# 30 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_signal2.h"
typedef struct {

   unsigned int cur_mask __attribute__((aligned(8)));
   unsigned int sig_state;




   unsigned int queue;
   unsigned int wait_mask;

} qurt_signal2_t;
# 71 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_signal2.h"
void qurt_signal2_init(qurt_signal2_t *signal);
# 95 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_signal2.h"
void qurt_signal2_destroy(qurt_signal2_t *signal);
# 129 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_signal2.h"
unsigned int qurt_signal2_wait(qurt_signal2_t *signal, unsigned int mask,
                unsigned int attribute);
# 158 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_signal2.h"
static inline unsigned int qurt_signal2_wait_any(qurt_signal2_t *signal, unsigned int mask)
{
  return qurt_signal2_wait(signal, mask, 0x00000000);
}
# 189 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_signal2.h"
static inline unsigned int qurt_signal2_wait_all(qurt_signal2_t *signal, unsigned int mask)
{
  return qurt_signal2_wait(signal, mask, 0x00000001);
}
# 215 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_signal2.h"
void qurt_signal2_set(qurt_signal2_t *signal, unsigned int mask);
# 235 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_signal2.h"
unsigned int qurt_signal2_get(qurt_signal2_t *signal);
# 260 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_signal2.h"
void qurt_signal2_clear(qurt_signal2_t *signal, unsigned int mask);

int qurt_signal2_wait_cancellable(qurt_signal2_t *signal,
                                  unsigned int mask,
                                  unsigned int attribute,
                                  unsigned int *p_returnmask);
# 60 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2

# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pimutex2.h" 1
# 46 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pimutex2.h"
void qurt_pimutex2_init(qurt_rmutex2_t *lock);
# 68 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pimutex2.h"
void qurt_pimutex2_destroy(qurt_rmutex2_t *lock);
# 98 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pimutex2.h"
void qurt_pimutex2_lock(qurt_rmutex2_t *lock);
# 118 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pimutex2.h"
void qurt_pimutex2_unlock(qurt_rmutex2_t *lock);
# 138 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pimutex2.h"
int qurt_rmutex2_try_lock(qurt_rmutex2_t *lock);
# 62 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_int.h" 1
# 80 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_int.h"
 unsigned int qurt_interrupt_register(int int_num, qurt_anysignal_t *int_signal, int signal_mask);
# 123 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_int.h"
int qurt_interrupt_acknowledge(int int_num);
# 145 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_int.h"
unsigned int qurt_interrupt_deregister(int int_num);
# 165 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_int.h"
 unsigned int qurt_interrupt_enable(int int_num);
# 184 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_int.h"
 unsigned int qurt_interrupt_disable(int int_num);
# 201 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_int.h"
unsigned int qurt_interrupt_status(int int_num, int *status);
# 217 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_int.h"
unsigned int qurt_interrupt_clear(int int_num);
# 233 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_int.h"
unsigned int qurt_interrupt_get_registered(void);
# 252 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_int.h"
unsigned int qurt_interrupt_get_config(unsigned int int_num, unsigned int *int_type, unsigned int *int_polarity);
# 273 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_int.h"
unsigned int qurt_interrupt_set_config(unsigned int int_num, unsigned int int_type, unsigned int int_polarity);
# 289 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_int.h"
int qurt_interrupt_raise(unsigned int interrupt_num);
# 303 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_int.h"
void qurt_interrupt_disable_all(void);
# 319 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_int.h"
int qurt_isr_subcall(void);
# 63 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_lifo.h" 1
# 41 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_lifo.h"
void * qurt_lifo_pop(void *freelist);
# 58 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_lifo.h"
void qurt_lifo_push(void *freelist, void *buf);

void qurt_lifo_remove(void *freelist, void *buf);
# 64 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_power.h" 1
# 34 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_power.h"
static inline int qurt_power_shutdown_prepare(void){ return 0;}
# 74 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_power.h"
int qurt_power_shutdown_enter (int type);
# 98 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_power.h"
int qurt_power_exit(void);
# 121 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_power.h"
int qurt_power_apcr_enter (void);
# 138 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_power.h"
int qurt_power_tcxo_prepare (void);
# 151 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_power.h"
int qurt_power_tcxo_fail_exit (void);
# 170 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_power.h"
int qurt_power_tcxo_enter (void);
# 182 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_power.h"
int qurt_power_tcxo_exit (void);
# 209 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_power.h"
void qurt_power_override_wait_for_idle(int enable);
# 233 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_power.h"
void qurt_power_wait_for_idle (void);
# 249 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_power.h"
void qurt_power_wait_for_active (void);
# 269 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_power.h"
unsigned int qurt_system_ipend_get (void);
# 282 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_power.h"
void qurt_system_avscfg_set(unsigned int avscfg_value);
# 294 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_power.h"
unsigned int qurt_system_avscfg_get(void);
# 311 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_power.h"
unsigned int qurt_system_vid_get(void);
# 351 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_power.h"
int qurt_power_shutdown_get_pcycles( unsigned long long *enter_pcycles, unsigned long long *exit_pcycles );

int qurt_system_tcm_set_size(unsigned int new_size);
# 373 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_power.h"
int qurt_power_shutdown_get_hw_ticks( unsigned long long *before_pc_ticks, unsigned long long *after_wb_ticks );
# 65 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_event.h" 1
# 26 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_event.h"
typedef struct qurt_sysenv_swap_pools {

   unsigned int spoolsize;
   unsigned int spooladdr;

}qurt_sysenv_swap_pools_t;


typedef struct qurt_sysenv_app_heap {

   unsigned int heap_base;
   unsigned int heap_limit;

} qurt_sysenv_app_heap_t ;


typedef struct qurt_sysenv_arch_version {

    unsigned int arch_version;

}qurt_arch_version_t;


typedef struct qurt_sysenv_max_hthreads {

   unsigned int max_hthreads;

}qurt_sysenv_max_hthreads_t;


typedef struct qurt_sysenv_max_pi_prio {

    unsigned int max_pi_prio;

}qurt_sysenv_max_pi_prio_t;


typedef struct qurt_sysenv_timer_hw {

   unsigned int base;
   unsigned int int_num;

}qurt_sysenv_hw_timer_t;


typedef struct qurt_sysenv_procname {

   unsigned int asid;
   char name[64];

}qurt_sysenv_procname_t;


typedef struct qurt_sysenv_stack_profile_count {

   unsigned int count;

}qurt_sysenv_stack_profile_count_t;




typedef struct _qurt_sysevent_error_t
{

    unsigned int thread_id;
    unsigned int fault_pc;
    unsigned int sp;
    unsigned int badva;
    unsigned int cause;
    unsigned int ssr;
    unsigned int fp;
    unsigned int lr;

} qurt_sysevent_error_t ;


typedef struct qurt_sysevent_pagefault {
    qurt_thread_t thread_id;
    unsigned int fault_addr;
    unsigned int ssr_cause;
} qurt_sysevent_pagefault_t ;
# 128 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_event.h"
int qurt_sysenv_get_swap_spool0 (qurt_sysenv_swap_pools_t *pools );
# 144 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_event.h"
int qurt_sysenv_get_swap_spool1(qurt_sysenv_swap_pools_t *pools );
# 161 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_event.h"
int qurt_sysenv_get_app_heap(qurt_sysenv_app_heap_t *aheap );
# 178 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_event.h"
int qurt_sysenv_get_hw_timer(qurt_sysenv_hw_timer_t *timer );
# 195 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_event.h"
int qurt_sysenv_get_arch_version(qurt_arch_version_t *vers);
# 212 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_event.h"
int qurt_sysenv_get_max_hw_threads(qurt_sysenv_max_hthreads_t *mhwt );
# 229 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_event.h"
int qurt_sysenv_get_max_pi_prio(qurt_sysenv_max_pi_prio_t *mpip );
# 246 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_event.h"
int qurt_sysenv_get_process_name(qurt_sysenv_procname_t *pname );
# 259 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_event.h"
int qurt_sysenv_get_stack_profile_count(qurt_sysenv_stack_profile_count_t *count );
# 288 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_event.h"
unsigned int qurt_exception_wait (unsigned int *ip, unsigned int *sp,
                                  unsigned int *badva, unsigned int *cause);

unsigned int qurt_exception_wait_ext (qurt_sysevent_error_t * sys_err);
# 326 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_event.h"
static inline unsigned int qurt_exception_wait2(qurt_sysevent_error_t * sys_err)
{
   return qurt_exception_wait_ext(sys_err);
}
# 350 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_event.h"
int qurt_exception_raise_nonfatal (int error) __attribute__((noreturn));
# 372 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_event.h"
void qurt_exception_raise_fatal (void);
# 387 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_event.h"
void qurt_exception_shutdown_fatal(void) __attribute__((noreturn));
# 402 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_event.h"
void qurt_exception_shutdown_fatal2(void);
# 425 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_event.h"
unsigned int qurt_exception_register_fatal_notification ( void(*entryfuncpoint)(void *), void *argp);

unsigned int qurt_enable_floating_point_exception(unsigned int mask);
# 457 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_event.h"
static inline unsigned int qurt_exception_enable_fp_exceptions(unsigned int mask)
{
   return qurt_enable_floating_point_exception(mask);
}
# 483 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_event.h"
unsigned int qurt_exception_wait_pagefault (qurt_sysevent_pagefault_t *sys_pagefault);
# 66 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pmu.h" 1
# 53 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pmu.h"
void qurt_pmu_set (int reg_id, unsigned int reg_value);
# 78 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pmu.h"
unsigned int qurt_pmu_get (int red_id);
# 96 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_pmu.h"
void qurt_pmu_enable (int enable);
# 67 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2

# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_tlb.h" 1
# 38 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_tlb.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_types.h" 1
# 35 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_types.h"
typedef unsigned int qurt_addr_t;
typedef unsigned int qurt_paddr_t;
typedef unsigned long long qurt_paddr_64_t;
typedef unsigned int qurt_mem_region_t;
typedef unsigned int qurt_mem_fs_region_t;
typedef unsigned int qurt_mem_pool_t;
typedef unsigned int qurt_size_t;
# 96 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_types.h"
typedef enum {
        QURT_MEM_MAPPING_VIRTUAL=0,


        QURT_MEM_MAPPING_PHYS_CONTIGUOUS = 1,



        QURT_MEM_MAPPING_IDEMPOTENT=2,
        QURT_MEM_MAPPING_VIRTUAL_FIXED=3,



        QURT_MEM_MAPPING_NONE=4,
        QURT_MEM_MAPPING_VIRTUAL_RANDOM=7,

        QURT_MEM_MAPPING_INVALID=10,
} qurt_mem_mapping_t;


typedef enum {
        QURT_MEM_CACHE_WRITEBACK=7,
        QURT_MEM_CACHE_NONE_SHARED=6,
        QURT_MEM_CACHE_WRITETHROUGH=5,
        QURT_MEM_CACHE_WRITEBACK_NONL2CACHEABLE=0,
        QURT_MEM_CACHE_WRITETHROUGH_NONL2CACHEABLE=1,
        QURT_MEM_CACHE_WRITEBACK_L2CACHEABLE=QURT_MEM_CACHE_WRITEBACK,
        QURT_MEM_CACHE_WRITETHROUGH_L2CACHEABLE=QURT_MEM_CACHE_WRITETHROUGH,
        QURT_MEM_CACHE_DEVICE = 4,
        QURT_MEM_CACHE_NONE = 4,
        QURT_MEM_CACHE_INVALID=10,
} qurt_mem_cache_mode_t;



typedef enum {
        QURT_PERM_READ=0x1,
        QURT_PERM_WRITE=0x2,
        QURT_PERM_EXECUTE=0x4,
        QURT_PERM_FULL=QURT_PERM_READ|QURT_PERM_WRITE|QURT_PERM_EXECUTE,
} qurt_perm_t;


typedef enum {
        QURT_MEM_ICACHE,
        QURT_MEM_DCACHE
} qurt_mem_cache_type_t;


typedef enum {
    QURT_MEM_CACHE_FLUSH,
    QURT_MEM_CACHE_INVALIDATE,
    QURT_MEM_CACHE_FLUSH_INVALIDATE,
    QURT_MEM_CACHE_FLUSH_ALL,
    QURT_MEM_CACHE_FLUSH_INVALIDATE_ALL,
    QURT_MEM_CACHE_TABLE_FLUSH_INVALIDATE,
} qurt_mem_cache_op_t;


typedef enum {
        QURT_MEM_REGION_LOCAL=0,
        QURT_MEM_REGION_SHARED=1,
        QURT_MEM_REGION_USER_ACCESS=2,
        QURT_MEM_REGION_FS=4,
        QURT_MEM_REGION_INVALID=10,
} qurt_mem_region_type_t;



struct qurt_pgattr {
   unsigned pga_value;
};
typedef struct qurt_pgattr qurt_pgattr_t;
# 186 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_types.h"
typedef struct {

    qurt_mem_mapping_t mapping_type;
    unsigned char perms;
    unsigned short owner;
    qurt_pgattr_t pga;
    unsigned ppn;
    qurt_addr_t virtaddr;
    qurt_mem_region_type_t type;
    qurt_size_t size;

} qurt_mem_region_attr_t;



typedef struct {

    char name[32];
    struct ranges{
        unsigned int start;
        unsigned int size;
    } ranges[16];

} qurt_mem_pool_attr_t;



typedef enum {
    HEXAGON_L1_I_CACHE = 0,
    HEXAGON_L1_D_CACHE = 1,
    HEXAGON_L2_CACHE = 2
} qurt_cache_type_t;



typedef enum {
    FULL_SIZE = 0,
    HALF_SIZE = 1,
    THREE_QUARTER_SIZE = 2,
    SEVEN_EIGHTHS_SIZE = 3
} qurt_cache_partition_size_t;
# 39 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_tlb.h" 2
# 66 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_tlb.h"
int qurt_tlb_entry_create (unsigned int *entry_id, qurt_addr_t vaddr, qurt_paddr_t paddr, qurt_size_t size, qurt_mem_cache_mode_t cache_attribs, qurt_perm_t perms, int asid);


int qurt_tlb_entry_create_64 (unsigned int *entry_id, qurt_addr_t vaddr, qurt_paddr_64_t paddr_64, qurt_size_t size, qurt_mem_cache_mode_t cache_attribs, qurt_perm_t perms, int asid);
# 84 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_tlb.h"
int qurt_tlb_entry_delete (unsigned int entry_id);
# 101 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_tlb.h"
int qurt_tlb_entry_query (unsigned int *entry_id, qurt_addr_t vaddr, int asid);
# 117 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_tlb.h"
int qurt_tlb_entry_set (unsigned int entry_id, unsigned long long int entry);
# 133 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_tlb.h"
int qurt_tlb_entry_get (unsigned int entry_id, unsigned long long int *entry);
# 147 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_tlb.h"
unsigned short qurt_tlb_entry_get_available(void);
# 168 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_tlb.h"
unsigned int qurt_tlb_get_pager_physaddr(unsigned int** pager_phys_addrs);
# 69 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h" 1
# 18 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_error.h" 1
# 19 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h" 2
# 30 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
extern qurt_mem_pool_t qurt_mem_default_pool;
# 63 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
int qurt_mem_cache_clean(qurt_addr_t addr, qurt_size_t size, qurt_mem_cache_op_t opcode, qurt_mem_cache_type_t type);
# 84 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
int qurt_mem_l2cache_line_lock (qurt_addr_t addr, qurt_size_t size);
# 106 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
int qurt_mem_l2cache_line_unlock(qurt_addr_t addr, qurt_size_t size);
# 133 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
void qurt_mem_region_attr_init(qurt_mem_region_attr_t *attr);
# 160 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
int qurt_mem_pool_attach(char *name, qurt_mem_pool_t *pool);
# 202 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
int qurt_mem_pool_create(char *name, unsigned base, unsigned size, qurt_mem_pool_t *pool);
# 221 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
int qurt_mem_pool_add_pages(qurt_mem_pool_t pool,
                            unsigned first_pageno,
                            unsigned size_in_pages);
# 266 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
int qurt_mem_pool_remove_pages(qurt_mem_pool_t pool,
                               unsigned first_pageno,
                               unsigned size_in_pages,
                               unsigned flags,
                               void (*callback)(void *),
                               void *arg);
# 291 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
int qurt_mem_pool_attr_get (qurt_mem_pool_t pool, qurt_mem_pool_attr_t *attr);
# 311 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
static inline int qurt_mem_pool_attr_get_size (qurt_mem_pool_attr_t *attr, int range_id, qurt_size_t *size){
    if ((range_id >= 16) || (range_id < 0)){
        (*size) = 0;
        return 4;
    }
    else {
        (*size) = attr->ranges[range_id].size;
    }
    return 0;
}
# 340 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
static inline int qurt_mem_pool_attr_get_addr (qurt_mem_pool_attr_t *attr, int range_id, qurt_addr_t *addr){
    if ((range_id >= 16) || (range_id < 0)){
        (*addr) = 0;
        return 4;
    }
    else {
        (*addr) = (attr->ranges[range_id].start)<<12;
   }
   return 0;
}
# 397 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
int qurt_mem_region_create(qurt_mem_region_t *region, qurt_size_t size, qurt_mem_pool_t pool, qurt_mem_region_attr_t *attr);
# 420 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
int qurt_mem_region_delete(qurt_mem_region_t region);
# 442 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
int qurt_mem_region_attr_get(qurt_mem_region_t region, qurt_mem_region_attr_t *attr);
# 466 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
static inline void qurt_mem_region_attr_set_type(qurt_mem_region_attr_t *attr, qurt_mem_region_type_t type){
    attr->type = type;
}
# 486 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
static inline void qurt_mem_region_attr_get_size(qurt_mem_region_attr_t *attr, qurt_size_t *size){
    (*size) = attr->size;
}
# 506 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
static inline void qurt_mem_region_attr_get_type(qurt_mem_region_attr_t *attr, qurt_mem_region_type_t *type){
    (*type) = attr->type;
}
# 527 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
static inline void qurt_mem_region_attr_set_physaddr(qurt_mem_region_attr_t *attr, qurt_paddr_t addr){
    attr->ppn = (unsigned)(((unsigned)(addr))>>12);
}
# 546 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
static inline void qurt_mem_region_attr_get_physaddr(qurt_mem_region_attr_t *attr, unsigned int *addr){
    (*addr) = (unsigned)(((unsigned) (attr->ppn))<<12);
}
# 566 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
static inline void qurt_mem_region_attr_set_virtaddr(qurt_mem_region_attr_t *attr, qurt_addr_t addr){
    attr->virtaddr = addr;
}
# 585 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
static inline void qurt_mem_region_attr_get_virtaddr(qurt_mem_region_attr_t *attr, unsigned int *addr){
    (*addr) = (unsigned int)(attr->virtaddr);
}
# 635 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
static inline void qurt_mem_region_attr_set_mapping(qurt_mem_region_attr_t *attr, qurt_mem_mapping_t mapping){
    attr->mapping_type = mapping;
}
# 655 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
static inline void qurt_mem_region_attr_get_mapping(qurt_mem_region_attr_t *attr, qurt_mem_mapping_t *mapping){
    (*mapping) = attr->mapping_type;
}
# 683 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
static inline void qurt_mem_region_attr_set_cache_mode(qurt_mem_region_attr_t *attr, qurt_mem_cache_mode_t mode){
    (((attr->pga).pga_value)=(((attr->pga).pga_value)&~(((~0u)>>(31-((3)-(0))))<<(0)))|((((unsigned)mode)<<(0))&(((~0u)>>(31-((3)-(0))))<<(0))));
}
# 703 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
static inline void qurt_mem_region_attr_get_cache_mode(qurt_mem_region_attr_t *attr, qurt_mem_cache_mode_t *mode){
    (*mode) = (qurt_mem_cache_mode_t)((((attr->pga).pga_value)&(((~0u)>>(31-((3)-(0))))<<(0)))>>(0));
}
# 724 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
static inline void qurt_mem_region_attr_set_bus_attr(qurt_mem_region_attr_t *attr, unsigned abits){
    (((attr->pga).pga_value)=(((attr->pga).pga_value)&~(((~0u)>>(31-((5)-(4))))<<(4)))|(((abits)<<(4))&(((~0u)>>(31-((5)-(4))))<<(4))));
}
# 744 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
static inline void qurt_mem_region_attr_get_bus_attr(qurt_mem_region_attr_t *attr, unsigned *pbits){
    (*pbits) = ((((attr->pga).pga_value)&(((~0u)>>(31-((5)-(4))))<<(4)))>>(4));
}

void qurt_mem_region_attr_set_owner(qurt_mem_region_attr_t *attr, int handle);
void qurt_mem_region_attr_get_owner(qurt_mem_region_attr_t *attr, int *p_handle);
void qurt_mem_region_attr_set_perms(qurt_mem_region_attr_t *attr, unsigned perms);
void qurt_mem_region_attr_get_perms(qurt_mem_region_attr_t *attr, unsigned *p_perms);
# 782 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
int qurt_mem_map_static_query(qurt_addr_t *vaddr, qurt_addr_t paddr, unsigned int page_size, qurt_mem_cache_mode_t cache_attribs, qurt_perm_t perm);
# 813 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
int qurt_mem_region_query(qurt_mem_region_t *region_handle, qurt_addr_t vaddr, qurt_paddr_t paddr);
# 838 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
int qurt_mapping_create(qurt_addr_t vaddr, qurt_addr_t paddr, qurt_size_t size,
                         qurt_mem_cache_mode_t cache_attribs, qurt_perm_t perm);
# 859 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
int qurt_mapping_remove(qurt_addr_t vaddr, qurt_addr_t paddr, qurt_size_t size);
# 877 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
qurt_paddr_t qurt_lookup_physaddr (qurt_addr_t vaddr);
# 896 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
static inline void qurt_mem_region_attr_set_physaddr_64(qurt_mem_region_attr_t *attr, qurt_paddr_64_t addr_64){
    attr->ppn = (unsigned)(((unsigned long long)(addr_64))>>12);
}
# 916 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
static inline void qurt_mem_region_attr_get_physaddr_64(qurt_mem_region_attr_t *attr, qurt_paddr_64_t *addr_64){
    (*addr_64) = (unsigned long long)(((unsigned long long)(attr->ppn))<<12);
}
# 950 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
int qurt_mem_map_static_query_64(qurt_addr_t *vaddr, qurt_paddr_64_t paddr_64, unsigned int page_size, qurt_mem_cache_mode_t cache_attribs, qurt_perm_t perm);
# 979 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
int qurt_mem_region_query_64(qurt_mem_region_t *region_handle, qurt_addr_t vaddr, qurt_paddr_64_t paddr_64);
# 1004 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
int qurt_mapping_create_64(qurt_addr_t vaddr, qurt_paddr_64_t paddr_64, qurt_size_t size,
                         qurt_mem_cache_mode_t cache_attribs, qurt_perm_t perm);
# 1026 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
int qurt_mapping_remove_64(qurt_addr_t vaddr, qurt_paddr_64_t paddr_64, qurt_size_t size);
# 1044 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
qurt_paddr_64_t qurt_lookup_physaddr_64 (qurt_addr_t vaddr);
# 1081 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
int qurt_mapping_reclaim(qurt_addr_t vaddr, qurt_size_t vsize, qurt_mem_pool_t pool);
# 1111 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
int qurt_mem_configure_cache_partition(qurt_cache_type_t cache_type, qurt_cache_partition_size_t partition_size);
# 1123 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
void qurt_l2fetch_disable(void);
# 1138 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
static inline void qurt_mem_syncht(void){
    __asm__ __volatile__ (" SYNCHT \n");
}
# 1156 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_memory.h"
static inline void qurt_mem_barrier(void){
    __asm__ __volatile__ (" BARRIER \n");
}
# 70 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_qdi.h" 1
# 19 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_qdi.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_qdi_constants.h" 1
# 20 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_qdi.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_qdi_imacros.h" 1
# 21 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_qdi.h" 2
# 101 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_qdi.h"
int qurt_qdi_qhi3(int,int,int);
int qurt_qdi_qhi4(int,int,int,int);
int qurt_qdi_qhi5(int,int,int,int,int);
int qurt_qdi_qhi6(int,int,int,int,int,int);
int qurt_qdi_qhi7(int,int,int,int,int,int,int);
int qurt_qdi_qhi8(int,int,int,int,int,int,int,int);
int qurt_qdi_qhi9(int,int,int,int,int,int,int,int,int);
int qurt_qdi_qhi10(int,int,int,int,int,int,int,int,int,int);
int qurt_qdi_qhi11(int,int,int,int,int,int,int,int,int,int,int);
int qurt_qdi_qhi12(int,int,int,int,int,int,int,int,int,int,int,int);
# 130 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_qdi.h"
int qurt_qdi_write(int handle, const void *buf, unsigned len);
# 150 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_qdi.h"
int qurt_qdi_read(int handle, void *buf, unsigned len);
# 171 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_qdi.h"
int qurt_qdi_close(int handle);
# 71 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_sclk.h" 1
# 85 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_sclk.h"
extern int qurt_sysclock_register (qurt_anysignal_t *signal, unsigned int signal_mask);
# 111 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_sclk.h"
extern unsigned long long qurt_sysclock_alarm_create (int id, unsigned long long ref_count, unsigned long long match_value);
# 131 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_sclk.h"
extern int qurt_sysclock_timer_create (int id, unsigned long long duration);
# 147 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_sclk.h"
extern unsigned long long qurt_sysclock_get_expiry (void);
# 163 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_sclk.h"
unsigned long long qurt_sysclock_get_hw_ticks (void);
# 181 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_sclk.h"
extern int qurt_timer_base __attribute__((section(".data.qurt_timer_base")));
static inline unsigned long qurt_sysclock_get_hw_ticks_32 (void)
{
    return (volatile unsigned long)(*((unsigned long *)((unsigned int)qurt_timer_base+0x1000)));
}
# 207 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_sclk.h"
static inline unsigned short qurt_sysclock_get_hw_ticks_16 (void)
{
    unsigned long ticks;

    ticks = (volatile unsigned long)(*((unsigned long *)((unsigned int)qurt_timer_base+0x1000)));
    __asm__ __volatile__ ( "%0 = lsr(%0, #16) \n" :"+r"(ticks));

    return (unsigned short)ticks;
}


unsigned long long qurt_timer_timetick_to_us(unsigned long long ticks);
# 72 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_space.h" 1
# 35 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_space.h"
int qurt_spawn_flags(const char * name, int flags);
# 69 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_space.h"
int qurt_space_switch(int asid);
# 83 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_space.h"
int qurt_wait(int *status);






int qurt_event_register(int type, int value, qurt_signal_t *signal, unsigned int mask, void *data, unsigned int data_size);
# 73 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_process.h" 1
# 22 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_process.h"
typedef struct _qurt_process_attr {

    char name[64];
    int flags;

} qurt_process_attr_t;
# 49 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_process.h"
int qurt_process_create (qurt_process_attr_t *attr);
# 60 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_process.h"
int qurt_process_get_id (void);
# 84 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_process.h"
static inline void qurt_process_attr_init (qurt_process_attr_t *attr)
{
    attr->name[0] = 0;
    attr->flags = 0;
}
# 110 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_process.h"
static inline void qurt_process_attr_set_executable (qurt_process_attr_t *attr, char *name)
{
    strlcpy (attr->name, name, 64);
}
# 132 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_process.h"
static inline void qurt_process_attr_set_flags (qurt_process_attr_t *attr, int flags)
{
    attr->flags = flags;
}
# 159 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_process.h"
void qurt_process_cmdline_get(char *buf, unsigned buf_siz);
# 74 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_shmem.h" 1
# 23 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_shmem.h"
typedef unsigned int mode_t;
# 44 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_shmem.h"
int shm_open(const char * name, int oflag, mode_t mode);
# 63 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_shmem.h"
void *shm_mmap(void *addr, unsigned int len, int prot, int flags, int fd, unsigned int offset);
# 80 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_shmem.h"
int shm_close(int fd);
# 75 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_timer.h" 1
# 87 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_timer.h"
typedef enum
{
  QURT_TIMER_ONESHOT = 0,
  QURT_TIMER_PERIODIC
} qurt_timer_type_t;







typedef unsigned int qurt_timer_t;


typedef unsigned long long qurt_timer_duration_t;


typedef unsigned long long qurt_timer_time_t;


typedef struct
{

    unsigned int magic;

    qurt_timer_duration_t duration;

    qurt_timer_time_t expiry;

    qurt_timer_duration_t remaining;

    qurt_timer_type_t type;


    unsigned int group;


}
qurt_timer_attr_t;
# 155 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_timer.h"
int qurt_timer_stop (qurt_timer_t timer);
# 185 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_timer.h"
int qurt_timer_restart (qurt_timer_t timer, qurt_timer_duration_t duration);
# 213 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_timer.h"
int qurt_timer_create (qurt_timer_t *timer, const qurt_timer_attr_t *attr,
                  const qurt_anysignal_t *signal, unsigned int mask);

int qurt_timer_create_sig2 (qurt_timer_t *timer, const qurt_timer_attr_t *attr,
                  const qurt_signal2_t *signal, unsigned int mask);
# 236 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_timer.h"
void qurt_timer_attr_init(qurt_timer_attr_t *attr);
# 264 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_timer.h"
void qurt_timer_attr_set_duration(qurt_timer_attr_t *attr, qurt_timer_duration_t duration);
# 285 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_timer.h"
void qurt_timer_attr_set_expiry(qurt_timer_attr_t *attr, qurt_timer_time_t time);
# 306 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_timer.h"
void qurt_timer_attr_get_duration(qurt_timer_attr_t *attr, qurt_timer_duration_t *duration);
# 330 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_timer.h"
void qurt_timer_attr_get_remaining(qurt_timer_attr_t *attr, qurt_timer_duration_t *remaining);
# 357 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_timer.h"
void qurt_timer_attr_set_type(qurt_timer_attr_t *attr, qurt_timer_type_t type);
# 375 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_timer.h"
void qurt_timer_attr_get_type(qurt_timer_attr_t *attr, qurt_timer_type_t *type);
# 397 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_timer.h"
void qurt_timer_attr_set_group(qurt_timer_attr_t *attr, unsigned int group);
# 414 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_timer.h"
void qurt_timer_attr_get_group(qurt_timer_attr_t *attr, unsigned int *group);
# 435 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_timer.h"
int qurt_timer_get_attr(qurt_timer_t timer, qurt_timer_attr_t *attr);
# 453 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_timer.h"
int qurt_timer_delete(qurt_timer_t timer);
# 478 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_timer.h"
int qurt_timer_sleep(qurt_timer_duration_t duration);
# 497 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_timer.h"
int qurt_timer_group_disable (unsigned int group);
# 513 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_timer.h"
int qurt_timer_group_enable (unsigned int group);






void qurt_timer_recover_pc (void);
# 529 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_timer.h"
static inline int qurt_timer_is_init (void) {return 1;};
# 539 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_timer.h"
unsigned long long qurt_timer_get_ticks (void);
# 76 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_tls.h" 1
# 43 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_tls.h"
int qurt_tls_create_key (int *key, void (*destructor)(void *));
# 56 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_tls.h"
int qurt_tls_set_specific (int key, const void *value);
# 72 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_tls.h"
void *qurt_tls_get_specific (int key);
# 90 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_tls.h"
int qurt_tls_delete_key (int key);
# 77 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread_context.h" 1
# 21 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_thread_context.h"
static inline int qurt_thread_iterator_create(void)
{
   return qurt_qdi_qhi3(0,4,68);
}

static inline qurt_thread_t qurt_thread_iterator_next(int iter)
{
   return qurt_qdi_qhi3(0,iter,69);
}

static inline int qurt_thread_iterator_destroy(int iter)
{
   return qurt_qdi_close(iter);
}



int qurt_thread_context_get_tname(unsigned int thread_id, char *name, unsigned char max_len);
int qurt_thread_context_get_prio(unsigned int thread_id, unsigned char *prio);
int qurt_thread_context_get_pcycles(unsigned int thread_id, unsigned long long int *pcycles);
int qurt_thread_context_get_stack_base(unsigned int thread_id, unsigned int *sbase);
int qurt_thread_context_get_stack_size(unsigned int thread_id, unsigned int *ssize);

int qurt_thread_context_get_pid(unsigned int thread_id, unsigned int *pid);
int qurt_thread_context_get_pname(unsigned int thread_id, char *name, unsigned int len);
# 78 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_hvx.h" 1
# 23 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_hvx.h"
typedef enum {
    QURT_HVX_MODE_64B = 0,
    QURT_HVX_MODE_128B = 1
} qurt_hvx_mode_t;
# 82 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_hvx.h"
int qurt_hvx_lock(qurt_hvx_mode_t lock_mode);
# 103 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_hvx.h"
int qurt_hvx_unlock(void);
# 127 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_hvx.h"
int qurt_hvx_try_lock(qurt_hvx_mode_t lock_mode);
# 150 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_hvx.h"
int qurt_hvx_get_mode(void);
# 173 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_hvx.h"
int qurt_hvx_get_units(void);
# 204 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_hvx.h"
int qurt_hvx_reserve(int num_units);
# 229 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_hvx.h"
int qurt_hvx_cancel_reserve(void);
# 255 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_hvx.h"
int qurt_hvx_get_lock_val(void);
# 79 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_mailbox.h" 1
# 31 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_mailbox.h"
typedef enum {
        QURT_MAILBOX_AT_QURTOS=0,
        QURT_MAILBOX_AT_ROOTPD=1,
        QURT_MAILBOX_AT_USERPD=2,
        QURT_MAILBOX_AT_SECUREPD=3,
} qurt_mailbox_receiver_cfg_t;


typedef enum {
        QURT_MAILBOX_SEND_OVERWRITE=0,
        QURT_MAILBOX_SEND_NON_OVERWRITE=1,
} qurt_mailbox_send_option_t;


typedef enum {
        QURT_MAILBOX_RECV_WAITING=0,
        QURT_MAILBOX_RECV_NON_WAITING=1,
        QURT_MAILBOX_RECV_PEEK_NON_WAITING=2,
} qurt_mailbox_recv_option_t;
# 68 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_mailbox.h"
unsigned long long qurt_mailbox_create(char *name, qurt_mailbox_receiver_cfg_t recv_opt);
# 83 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_mailbox.h"
unsigned long long qurt_mailbox_get_id(char *name);
# 104 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_mailbox.h"
int qurt_mailbox_send(unsigned long long mailbox_id, qurt_mailbox_send_option_t send_opt, unsigned long long data);
# 125 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_mailbox.h"
int qurt_mailbox_receive(unsigned long long mailbox_id, qurt_mailbox_recv_option_t recv_opt, unsigned long long *data);
# 143 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_mailbox.h"
int qurt_mailbox_delete(unsigned long long mailbox_id);
# 159 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_mailbox.h"
int qurt_mailbox_receive_halt(unsigned long long mailbox_id);
# 80 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_island.h" 1
# 25 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_island.h"
enum qurt_island_attr_resource_type {
    QURT_ISLAND_ATTR_INVALID,
    QURT_ISLAND_ATTR_END_OF_LIST = QURT_ISLAND_ATTR_INVALID,
    QURT_ISLAND_ATTR_INT,
    QURT_ISLAND_ATTR_THREAD,
    QURT_ISLAND_ATTR_MEMORY
};

typedef struct qurt_island_attr_resource {
    enum qurt_island_attr_resource_type type;
    union {
        struct {
            qurt_addr_t base_addr;
            qurt_size_t size;
        } memory;
        unsigned int interrupt;
        qurt_thread_t thread_id;
    };
} qurt_island_attr_resource_t;



typedef struct qurt_island_attr {

    int max_attrs;
    struct qurt_island_attr_resource attrs[1];

} qurt_island_attr_t;


typedef struct {
   int qdi_handle;
} qurt_island_t;
# 72 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_island.h"
int qurt_island_attr_create (qurt_island_attr_t **attr, int max_attrs);
# 84 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_island.h"
void qurt_island_attr_delete (qurt_island_attr_t *attr);
# 109 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_island.h"
int qurt_island_attr_add (qurt_island_attr_t *attr, qurt_island_attr_resource_t *resources);
# 129 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_island.h"
int qurt_island_attr_add_interrupt (qurt_island_attr_t *attr, unsigned int interrupt);
# 152 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_island.h"
int qurt_island_attr_add_mem (qurt_island_attr_t *attr, qurt_addr_t base_addr, qurt_size_t size);
# 173 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_island.h"
int qurt_island_attr_add_thread (qurt_island_attr_t *attr, qurt_thread_t thread_id);
# 197 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_island.h"
int qurt_island_spec_create (qurt_island_t *spec_id, qurt_island_attr_t *attr);
# 219 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_island.h"
int qurt_island_spec_delete (qurt_island_t spec_id);
# 239 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_island.h"
int qurt_island_enter (qurt_island_t spec_id);
# 255 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_island.h"
int qurt_island_exit (void);
# 290 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_island.h"
unsigned int qurt_island_exception_wait (unsigned int *ip, unsigned int *sp,
                                         unsigned int *badva, unsigned int *cause);
# 305 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt_island.h"
unsigned int qurt_island_get_status (void);
# 81 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/kernel/qurt/qurt.h" 2
# 19 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/InterruptController/inc/DALInterruptController_utils.h" 2






typedef struct InterruptArgsStruct
{
  uint32 nInterruptVector;
  qurt_anysignal_t pRetSig;
  struct InterruptArgsStruct *pNext;
} InterruptArgsType;
# 51 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/InterruptController/inc/DALInterruptController_utils.h"
void InterruptControllerPushArg( InterruptArgsType* pArgs, InterruptArgsType** ppList );
# 71 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/InterruptController/inc/DALInterruptController_utils.h"
InterruptArgsType* InterruptControllerPopArg( InterruptArgsType** ppList );
# 92 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/InterruptController/inc/DALInterruptController_utils.h"
InterruptArgsType* InterruptControllerSearchArg( uint32 nInterruptVector, InterruptArgsType** ppList );
# 44 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/InterruptController/src/qurt/kernel/DALInterruptControllerConfig.h" 2
# 125 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/InterruptController/src/qurt/kernel/DALInterruptControllerConfig.h"
typedef enum
{
  INTERRUPT_LOG_EVENT_NULL,




  INTERRUPT_LOG_EVENT_ISR_START,
  INTERRUPT_LOG_EVENT_ISR_FINISH,
  INTERRUPT_LOG_EVENT_SUBISR_START,
  INTERRUPT_LOG_EVENT_SUBISR_FINISH,
  INTERRUPT_LOG_EVENT_TRIGGERED,
  INTERRUPT_LOG_EVENT_SENT,
  INTERRUPT_LOG_EVENT_UNHANDLED,
  INTERRUPT_LOG_EVENT_SUSPENDED,
  INTERRUPT_LOG_EVENT_UNSUPPORTED,
  INTERRUPT_LOG_EVENT_SLEEP,
  INTERRUPT_LOG_EVENT_WAKEUP,
  INTERRUPT_LOG_EVENT_SHUTDOWN,
  INTERRUPT_NUM_LOG_EVENTS,
  PLACEHOLDER_InterruptLogEventType = 0x7fffffff
} InterruptLogEventType;
# 158 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/InterruptController/src/qurt/kernel/DALInterruptControllerConfig.h"
typedef struct
{
  InterruptLogEventType eEvent;
  uint32 nInterrupt;
  char* pInterruptName;
  uint64 nTimeStamp;
  unsigned long long int pcycles;
} InterruptLogEntryType;







typedef struct
{
  atomic_word_t AtomicIdx;
  uint32 nIdx;
  uint32 nLogSize;
  InterruptLogEntryType* pEntries;
} InterruptLogType;

typedef void (*DALISR_HandlerType) (uint32 param);
# 204 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/InterruptController/src/qurt/kernel/DALInterruptControllerConfig.h"
typedef struct
{
  DALISR Isr;
  DALISRCtx nParam;
  uint8 nTrigger;
  uint8 nFlags;



  uint32 nCount;
  uint64 nLastFired;
  qurt_thread_t nThreadID;
  unsigned char * pISTStack;
  uint32 nInterruptVector;
  char aISTName[16];
  qurt_anysignal_t ISTSignal;
  qurt_thread_attr_t ThreadAttr;
  uint16 nTimeTestProfileID;
  uint32 nPriority;
  uint32 nISTStackSize;
  char* pInterruptName;
} InterruptStateType;




typedef struct
{
  uint32 nInterruptVector;
  char *pInterruptName;

} InterruptConfigType;







typedef struct
{
  InterruptConfigType *pIRQConfigs;
  uint32 nMaxIRQ;
} InterruptPlatformDataType;
# 259 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/InterruptController/src/qurt/kernel/DALInterruptControllerConfig.h"
typedef struct
{
  DalDeviceHandle *pTimetickHandle;
  InterruptControllerClientCtxt *pClientCtxt;
  InterruptStateType *pInterruptState;
  InterruptLogType pLog;
  InterruptPlatformDataType *pPlatformConfig;
  uint32 uInterruptController;
  InterruptArgsType *pArgsList;
  InterruptArgsType *pEmptyList;
} InterruptDataType;
# 34 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/InterruptController/config/8996/kernel/InterruptConfigData.c" 2
# 50 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/InterruptController/config/8996/kernel/InterruptConfigData.c"
static InterruptConfigType InterruptConfigs[] =
{
  {0 , "qdsp6_etm_irq" },
  {1 , "qdsp6_isdb_irq" },
  {2 , "qtmr_phy_irq[0]" },
  {3 , "qtmr_phy_irq[1]" },
  {5 , "rpm_ipc[9]" },
  {7 , "rpm_ipc[8]" },
  {8 , "APCS_adspHlosIPCInterrupt[1]" },
  {11 , "slimbus_core_ee0_irq" },
  {12 , "bam_irq[0]" },
  {13 , "rpm_ipc[10]" },
  {14 , "rpm_ipc[11]" },
  {18 , "APCS_adspHlosIPCInterrupt[0]" },
  {19 , "audio_out2_irq" },
  {20 , "bus_irq" },
  {23 , "dm_irq" },
  {24 , "dir_conn_irq_lpa_dsp[0]" },
  {25 , "dir_conn_irq_lpa_dsp[1]" },
  {26 , "dir_conn_irq_lpa_dsp[2]" },
  {27 , "dir_conn_irq_lpa_dsp[3]" },
  {28 , "dir_conn_irq_lpa_dsp[4]" },
  {29 , "dir_conn_irq_lpa_dsp[5]" },
  {30 , "q6ss_idle_timer_irq" },
  {30 , "qtmr_phy_irq[2]" },
  {31 , "q6ss_cti_irq" },
  {32 , "q6ss_wdog_irq" },
  {33 , "APCS_adspHlosIPCInterrupt[2]" },
  {37 , "adsp_ext_vfr_irq" },
  {38 , "summary_irq_lpa_dsp" },
  {41 , "q6ss_avs_irq" },
  {42 , "q6ss_avs_cpu_up_irq" },
  {42 , "usb20s_power_event_irq" },
  {43 , "q6ss_avs_cpu_dn_irq" },
  {44 , "usb30_power_event_irq" },
  {45 , "usb30_ee1_irq" },
  {46 , "APCS_adspHlosIPCInterrupt[3]" },
  {47 , "usb30_ee2_irq" },
  {48 , "ee3_lpass_spmi_periph_irq" },
  {49 , "channel3_lpass_trans_done_irq" },
  {50 , "avtimer_int_0" },
  {51 , "vfr_irq_mux_out[0]" },
  {53 , "resampler_irq[1]" },
  {54 , "usb20s_ee1_irq" },
  {55 , "blsp1_bam_irq[1]" },
  {56 , "blsp2_bam_irq[1]" },
  {57 , "o_hwe_sof_stb_vfe_0_lpass" },
  {58 , "o_hwe_eof_stb_vfe_0_lpass" },
  {60 , "ao_pen_irq" },
  {61 , "spdm_realtime_irq[2]" },
  {63 , "q6ss_avs_sw_done_irq" },
  {64 , "blsp1_qup_irq(0)" },
  {65 , "blsp1_qup_irq(1)" },
  {66 , "blsp1_qup_irq(2)" },
  {67 , "blsp1_qup_irq(3)" },
  {68 , "blsp1_qup_irq(4)" },
  {69 , "blsp1_qup_irq(5)" },
  {72 , "pcie20_2_int_msi_dev5" },
  {73 , "pcie20_2_int_msi_dev4" },
  {74 , "pcie20_1_int_msi_dev5" },
  {75 , "pcie20_1_int_msi_dev4" },
  {76 , "pcie20_0_int_msi_dev5" },
  {77 , "pcie20_0_int_msi_dev4" },
  {79 , "ssc_irq_out_lpass[0]" },
  {80 , "blsp2_qup_irq(0)" },
  {81 , "blsp2_qup_irq(1)" },
  {82 , "blsp2_qup_irq(2)" },
  {83 , "blsp2_qup_irq(3)" },
  {84 , "blsp2_qup_irq(4)" },
  {85 , "blsp2_qup_irq(5)" },
  {86 , "slimbus_core_ee0_irq" },
  {87 , "bam_irq[0]" },
  {90 , "o_timeout_slave_lpass_summary_intr" },
  {91 , "o_hwe_sof_stb_vfe_1_lpass" },
  {92 , "o_hwe_eof_stb_vfe_1_lpass" },
  {93 , "ssc_irq_out_lpass[3]" },
  {93 , "ssc_irq_out_lpass[3]" },
  {94 , "lpass_hdmitx_interrupt" },
  {96 , "crypto_core_irq[0]" },
  {96 , "crypto_core_irq[1]" },
  {97 , "crypto_bam_irq[0]" },
  {97 , "crypto_bam_irq[1]" },
  {104 , "phss_uart_lpass_int[0]" },
  {105 , "phss_uart_lpass_int[1]" },
  {112 , "lpass_core_qos_q6_interrupt" },
  {113 , "lpass_qos_q6_interrupt" },
  {114 , "APCS_adspHypIPCInterrupt[0]" },
  {115 , "APCS_adspHypIPCInterrupt[1]" },
  {116 , "APCS_adspHypIPCInterrupt[2]" },
  {117 , "APCS_adspHypIPCInterrupt[3]" },
  {118 , "ssc_irq_out_lpass[1]" },
  {118 , "ssc_irq_out_lpass[1]" },
  {119 , "ssc_irq_out_lpass[2]" },
  {119 , "ssc_irq_out_lpass[2]" },
  {124 , "vfr_irq_mux_out[1]" },
  {125 , "camss_irq0" },
  {126 , "camss_irq1" },
  {127 , "camss_irq2" },
  {128 , "camss_irq3" },
  {129 , "camss_irq4" },
  {130 , "camss_irq5" },
  {131 , "camss_irq6" },
  {132 , "camss_irq7" },
  {133 , "camss_irq8" },
  {134 , "camss_irq9" },
  {135 , "camss_irq10" },
  {137 , "camss_irq12" },
  {138 , "camss_irq13" },
  {139 , "camss_irq14" },
  {140 , "camss_irq15" },
  {141 , "camss_irq16" },
  {142 , "camss_irq17" },
  {143 , "camss_irq18" },
  {144 , "camss_dsp_streaming_0_irq" },
  {145 , "camss_dsp_streaming_1_irq" },
  {146 , "lcc_spkr_ext_clk_detect_inactive_irq" },
  {147 , "lcc_pri_ext_clk_detect_inactive_irq" },
  {148 , "lcc_sec_ext_clk_detect_inactive_irq" },
  {149 , "lcc_ter_ext_clk_detect_inactive_irq" },
  {150 , "lcc_quad_ext_clk_detect_inactive_irq" },
  {151 , "lcc_audio_core_smmu_gds_hw_ctrl_sequence_abort_irq" },
  {152 , "lcc_audio_wrapper_q6_smmu_gds_hw_ctrl_seq_abort" },





  {0x7FFFFFFF , ""}
};





InterruptPlatformDataType pInterruptControllerConfigData[] =
{
  {
    InterruptConfigs,
    154
  }
};
