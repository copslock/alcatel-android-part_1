# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/timetick/config/Timetick.xml"
# 1 "<built-in>" 1
# 1 "<built-in>" 3
# 140 "<built-in>" 3
# 1 "<command line>" 1
# 1 "<built-in>" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/timetick/config/Timetick.xml" 2
<driver name="Timetick">
  <device id="SystemTimer">
  <props name="DEFAULT_FREQUENCY" type=DALPROP_ATTR_TYPE_UINT32>
   19200000
  </props>
  <props name="IsRemotable" type=DALPROP_ATTR_TYPE_UINT32>
   0x1
  </props>
  <props name="QTIMER_FRAME" type=DALPROP_ATTR_TYPE_UINT32>
   1
  </props>
# 28 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/timetick/config/Timetick.xml"
  <props name="QTIMER_BASE" type=DALPROP_ATTR_TYPE_UINT32>
    0x09000000
  </props>
  <props name="QTIMER_OFFSET" type=DALPROP_ATTR_TYPE_UINT32>
   0x003A2000
  </props>
  <props name="QTIMER_AC_OFFSET" type=DALPROP_ATTR_TYPE_UINT32>
   0x003A0000
  </props>
  <props name="QTIMER_INTVECTNUM" type=DALPROP_ATTR_TYPE_UINT32>
   3
  </props>


  </device>
  <device id="WakeUpTimer">
  <props name="DEFAULT_FREQUENCY" type=DALPROP_ATTR_TYPE_UINT32>
   19200000
  </props>
  <props name="QTIMER_FRAME" type=DALPROP_ATTR_TYPE_UINT32>
   2
  </props>
# 66 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/timetick/config/Timetick.xml"
  <props name="QTIMER_BASE" type=DALPROP_ATTR_TYPE_UINT32>
   0x09000000
  </props>
 <props name="QTIMER_OFFSET" type=DALPROP_ATTR_TYPE_UINT32>
   0x003A3000
  </props>
  <props name="QTIMER_AC_OFFSET" type=DALPROP_ATTR_TYPE_UINT32>
   0x003A0000
  </props>
  <props name="QTIMER_INTVECTNUM" type=DALPROP_ATTR_TYPE_UINT32>
   30
  </props>


  </device>

</driver>
