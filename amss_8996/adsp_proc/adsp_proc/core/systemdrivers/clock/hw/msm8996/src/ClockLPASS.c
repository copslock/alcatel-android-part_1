/*
===========================================================================
*/
/**
  @file ClockLPASS.c 
  
  Main entry point for the MSM8996 LPASS clock driver.
*/
/*  
  ====================================================================

  Copyright (c) 2014 - 2015 QUALCOMM Technologies Incorporated.
  All Rights Reserved.  
  QUALCOMM Proprietary and Confidential. 

  ==================================================================== 
  $Header: //components/rel/core.adsp/2.7/systemdrivers/clock/hw/msm8996/src/ClockLPASS.c#14 $
  $DateTime: 2016/01/14 11:58:08 $
  $Author: pwbldsvc $
 
  when       who     what, where, why
  --------   ---     -------------------------------------------------
  09/07/11   dcf     Created.
 
  ====================================================================
*/ 


/*=========================================================================
      Include Files
==========================================================================*/

#include "DALDeviceId.h"
#include "ClockDriver.h"
#include "ClockLPASS.h"
#include "busywait.h"
#include "ClockSWEVT.h"
#include "ClockLPASSCPU.h"
#include "ClockLPASSHWIO.h"
#include "DDIChipInfo.h"

/*=========================================================================
      Macros
==========================================================================*/

#define CLOCK_ID_CPU   "lpass_q6core"

/*=========================================================================
      Type Definitions
==========================================================================*/

uint32 Clock_nHWIOBaseLPASS;

/*=========================================================================
      Data
==========================================================================*/

static ClockImageCtxtType Clock_ImageCtxt;

/* =========================================================================
      Functions
==========================================================================*/


/* =========================================================================
**  Function : Clock_DetectBSPVersion
** =========================================================================*/
/**
  Detects which BSP configuration to use for the current HW.

  @param *pDrvCtxt [in] -- Pointer to driver context.

  @return
  DAL_ERROR if a valid configuration was not found, other DAL_SUCCESS.

  @dependencies
  None.
*/

static DALResult Clock_DetectBSPVersion
(
  ClockDrvCtxt *pDrvCtxt
)
{
  ClockImageCtxtType     *pImageCtxt;
  ClockCPUPerfConfigType *pCPUPerfConfig;
  uint32                  i;
  uint32                  nSourceIndex;
  ClockPropertyValueType  PropVal = NULL;
  uint32                  nChipVersion = 0;
  uint32                  nChipFamily = 0;

  pImageCtxt = (ClockImageCtxtType *)pDrvCtxt->pImageCtxt;

  /*-----------------------------------------------------------------------*/
  /* Detect which CPU BSP data to use for this HW version.                 */
  /*-----------------------------------------------------------------------*/

  pCPUPerfConfig = pImageCtxt->pBSPConfig->pCPUPerfConfig;
  for (i = 0; i < pImageCtxt->pBSPConfig->nNumCPUPerfLevelConfigs; i++)
  {
    if (Clock_IsBSPSupported(&pCPUPerfConfig[i].HWVersion) == TRUE)
    {
      pImageCtxt->CPUCtxt.PerfConfig.HWVersion = pCPUPerfConfig[i].HWVersion;
      pImageCtxt->CPUCtxt.PerfConfig.anPerfLevel = pCPUPerfConfig[i].anPerfLevel;
      pImageCtxt->CPUCtxt.PerfConfig.nMaxPerfLevel = pCPUPerfConfig[i].nMaxPerfLevel;
      pImageCtxt->CPUCtxt.PerfConfig.nMinPerfLevel = pCPUPerfConfig[i].nMinPerfLevel;
      pImageCtxt->CPUCtxt.PerfConfig.nNumPerfLevels = pCPUPerfConfig[i].nNumPerfLevels;

      break;
    }
  }

  if (i == pImageCtxt->pBSPConfig->nNumCPUPerfLevelConfigs)
  {
    return DAL_ERROR;
  }

  /*
   * Need to change the Calibration configuration for the QDSP6SS PLL as it is at a
   * different frequency on v2 and v3 vs v1.
   */
  nChipVersion = DalChipInfo_ChipVersion();
  nChipFamily = DalChipInfo_ChipFamily();

  if((nChipVersion >= DALCHIPINFO_VERSION(2, 0)) || (nChipFamily == DALCHIPINFO_FAMILY_MSM8996SG))
  {
    if((nChipVersion < DALCHIPINFO_VERSION(3, 0)) && (nChipFamily != DALCHIPINFO_FAMILY_MSM8996SG))
    {
      if(DAL_SUCCESS != Clock_GetPropertyValue("ClockCPUCalCfgV2", &PropVal))
      {
        return(DAL_ERROR);
      }
    }
    else
    {
      if(DAL_SUCCESS != Clock_GetPropertyValue("ClockCPUCalCfgV3", &PropVal))
      {
        return(DAL_ERROR);
      }
    }

    nSourceIndex = pDrvCtxt->anSourceIndex[HAL_CLK_SOURCE_LPAPLL1];
    if((nSourceIndex != 0xFF) && (PropVal != NULL))
    {
      pDrvCtxt->aSources[nSourceIndex].pBSPConfig->pCalibrationFreqConfig =
        (ClockSourceFreqConfigType*)PropVal;
    }
    else
    {
      return DAL_ERROR;
    }
  }

  return DAL_SUCCESS;

} /* END of Clock_DetectBSPVersion */


/* =========================================================================
**  Function : ClockStub_InitImage
** =========================================================================*/
/*
  See ClockDriver.h
*/

DALResult ClockStub_InitImage
(
  ClockDrvCtxt *pDrvCtxt
)
{
  /*-----------------------------------------------------------------------*/
  /* Initialize the SW Events for Clocks.                                  */
  /*-----------------------------------------------------------------------*/

  Clock_SWEvent(CLOCK_EVENT_INIT, 0);

  /*-----------------------------------------------------------------------*/
  /* Good to go.                                                           */
  /*-----------------------------------------------------------------------*/

  return DAL_SUCCESS;

} /* END Clock_InitImage */

/* =========================================================================
**  Function : Clock_InitCPUConfig
** =========================================================================*/
/**
  Initializes current configuration of CPU clock
 
  This function is invoked at driver initialization to initialize the current
  CPU configuration.
 
  @param *pDrvCtxt [in] -- Pointer to driver context.

  @return
  DAL_ERROR if configuration was not valid, other DAL_SUCCESS.

  @dependencies
  None.
*/ 

static DALResult Clock_InitCPUConfig
(
  ClockDrvCtxt *pDrvCtxt
)
{
  DALResult            eRes;
  ClockNodeType       *pClock;
  ClockSourceNodeType *pSource;
  ClockImageCtxtType  *pImageCtxt;
  ClockCPUConfigType  *pConfig;
  uint32               nPL, nConfig;
  uint32               nSourceIndex;

  pImageCtxt = (ClockImageCtxtType *)pDrvCtxt->pImageCtxt;

  /*-----------------------------------------------------------------------*/
  /* Get CPU clock ID.                                                     */
  /*-----------------------------------------------------------------------*/

  eRes =
    Clock_GetClockId(
      pDrvCtxt, CLOCK_ID_CPU,
      (ClockIdType *)&pImageCtxt->CPUCtxt.pClock);
  if (eRes != DAL_SUCCESS)
  {
    return eRes;
  }

  pClock = pImageCtxt->CPUCtxt.pClock;

  /*-----------------------------------------------------------------------*/
  /* Find the max performance level.                                       */
  /*-----------------------------------------------------------------------*/

  if ((DalChipInfo_ChipVersion() < DALCHIPINFO_VERSION(2, 0)) &&
      (DalChipInfo_ChipFamily() != DALCHIPINFO_FAMILY_MSM8996SG))
  {
  nPL = pImageCtxt->CPUCtxt.PerfConfig.nMaxPerfLevel-1;
  }
  else
  {
    nPL = pImageCtxt->CPUCtxt.PerfConfig.nMaxPerfLevel-2;
  }
  nConfig = pImageCtxt->CPUCtxt.PerfConfig.anPerfLevel[nPL];
  pConfig = &pImageCtxt->pBSPConfig->pCPUConfig[nConfig];

  /*-----------------------------------------------------------------------*/
  /* Configure the CPU to the max performance level.                       */
  /*-----------------------------------------------------------------------*/

  HAL_clk_ConfigClockMux(pClock->pDomain->HALHandle, &pConfig->Mux.HALConfig);

  /*-----------------------------------------------------------------------*/
  /* Update state.                                                         */
  /*-----------------------------------------------------------------------*/

  nSourceIndex = pDrvCtxt->anSourceIndex[pConfig->Mux.HALConfig.eSource];
  if (nSourceIndex == 0xFF)
  {
    return DAL_ERROR;
  }

  pSource = &pDrvCtxt->aSources[nSourceIndex];
  if (pSource == NULL)
  {
    return DAL_ERROR;
  }

  pClock->pDomain->pSource = pSource;
  pImageCtxt->CPUCtxt.pConfig = pConfig;
  pClock->pDomain->pActiveMuxConfig = &pConfig->Mux;

  /*-----------------------------------------------------------------------*/
  /* Ensure that the CPU core clock/domain/source reference counts are 1.  */
  /*-----------------------------------------------------------------------*/

  Clock_EnableClock(pDrvCtxt, (ClockIdType)pImageCtxt->CPUCtxt.pClock);

  /*-----------------------------------------------------------------------*/
  /* Initialize the DCVS module.                                           */
  /*-----------------------------------------------------------------------*/

  eRes = Clock_InitDCVS(pDrvCtxt);
  if (eRes != DAL_SUCCESS)
  {
    return eRes;
  }

  return DAL_SUCCESS;

} /* END Clock_InitCPUConfig */


/* =========================================================================
**  Function : Clock_InitImage
** =========================================================================*/
/*
  See ClockDriver.h
*/ 

DALResult Clock_InitImage
(
  ClockDrvCtxt *pDrvCtxt
)
{
  DALResult eRes;
  uint32 nIdx;
  ClockPropertyValueType PropVal = NULL;
  ClockSourceInitType* pInitSources = NULL;
  HAL_clk_HWIOBaseType *pHWIOBases = NULL;
  ClockPowerDomainIdType nPowerDomain;
  ClockIdType nCoreClkId, nMportClkId, nAONBusClkId;

  /*-----------------------------------------------------------------------*/
  /* Get the CPU Configurations.                                           */
  /*-----------------------------------------------------------------------*/

  if (DalChipInfo_ChipFamily() == DALCHIPINFO_FAMILY_MSM8996SG)
  {
    if(DAL_SUCCESS != Clock_GetPropertyValue("ClockImageConfigV3", &PropVal))
    {
      return(DAL_ERROR);
    }
  }
  else if (DalChipInfo_ChipVersion() < DALCHIPINFO_VERSION(2, 0))
  {
    if(DAL_SUCCESS != Clock_GetPropertyValue("ClockImageConfig", &PropVal))
    {
      return(DAL_ERROR);
    }
  }
  else if (DalChipInfo_ChipVersion() < DALCHIPINFO_VERSION(3, 0))
  {
    if(DAL_SUCCESS != Clock_GetPropertyValue("ClockImageConfigV2", &PropVal))
    {
      return(DAL_ERROR);
    }
  }
  else
  {
    if(DAL_SUCCESS != Clock_GetPropertyValue("ClockImageConfigV3", &PropVal))
    {
      return(DAL_ERROR);
    }
  }

  if (PropVal == NULL)
  {
    return DAL_ERROR;
  }

  Clock_ImageCtxt.pBSPConfig = (ClockImageBSPConfigType*)PropVal;

  pDrvCtxt->pImageCtxt = &Clock_ImageCtxt;

  /*-----------------------------------------------------------------------*/
  /* Attach to the VCS DAL.                                                */
  /*-----------------------------------------------------------------------*/

  eRes = DAL_StringDeviceAttach("VCS", &Clock_ImageCtxt.hVCS);

  if (eRes != DAL_SUCCESS)
  {
    DALSYS_LogEvent(
      DALDEVICEID_CLOCK, DALSYS_LOGEVENT_FATAL_ERROR,
      "Unable to attach to VCS DAL: %d", eRes);
    return eRes;
  }

  /*-----------------------------------------------------------------------*/
  /* Initialize the Q6SS HWIO Base address.                                */
  /*-----------------------------------------------------------------------*/

  if(Clock_nHWIOBaseLPASS == 0)
  {
    eRes = Clock_GetPropertyValue("ClockLPASSBase", &PropVal);

    if (eRes != DAL_SUCCESS)
    {
      return(eRes);
    }

    pHWIOBases = (HAL_clk_HWIOBaseType*)PropVal;

    if (pHWIOBases != NULL)
    {
      Clock_MapHWIORegion(
        pHWIOBases->nPhysAddr, pHWIOBases->nSize, &Clock_nHWIOBaseLPASS);

      /*
       * If we are unable to map a virtual address, assume the physical one.
       */
      if(Clock_nHWIOBaseLPASS == NULL)
      {
        Clock_nHWIOBaseLPASS = pHWIOBases->nPhysAddr;
      }
    }
    else
    {
      /*
       * We were unable to get the base address.
       */
      return(DAL_ERROR);
    }
  }

  /*-----------------------------------------------------------------------*/
  /* Detect the BSP version to use.                                        */
  /*-----------------------------------------------------------------------*/

  eRes = Clock_DetectBSPVersion(pDrvCtxt);
  if (eRes != DAL_SUCCESS)
  {
    DALSYS_LogEvent (
      DALDEVICEID_CLOCK, DALSYS_LOGEVENT_FATAL_ERROR,
      "Failed to detect the BSP version.");
    return eRes;
  }  

  /*-----------------------------------------------------------------------*/
  /* Initialize the XO module.                                             */
  /*-----------------------------------------------------------------------*/

  Clock_InitXO(pDrvCtxt);

  /*-----------------------------------------------------------------------*/
  /* Initialize CPU core clock frequency configuration.                    */
  /*-----------------------------------------------------------------------*/

  eRes = Clock_InitCPUConfig(pDrvCtxt);
  if (eRes != DAL_SUCCESS)
  {
    DALSYS_LogEvent (
      DALDEVICEID_CLOCK, DALSYS_LOGEVENT_FATAL_ERROR,
      "Unable to detect CPU core clock configuration.");
    return eRes;
  }


  /*-----------------------------------------------------------------------*/
  /* Initialize the Q6PLL if applicable to this chipset.                   */
  /*-----------------------------------------------------------------------*/

  if(DAL_SUCCESS == Clock_GetPropertyValue("ClockSourcesToInit", &PropVal))
  {
    pInitSources = (ClockSourceInitType*)PropVal;

    if (pInitSources != NULL)
    {
      nIdx = 0;
      while (pInitSources[nIdx].eSource != HAL_CLK_SOURCE_NULL)
      {
        Clock_InitSource(pDrvCtxt,
                         pInitSources[nIdx].eSource,
                         pInitSources[nIdx].nFreqHz);
        nIdx++;
      }
    }
  }

  /*-----------------------------------------------------------------------*/
  /* Connect the active frequency configuration for the main PLL. This has */
  /* already been configured and enabled pre-main so this step ensures SW  */
  /* has correct data structures initialized.                              */
  /*-----------------------------------------------------------------------*/

  nIdx = pDrvCtxt->anSourceIndex[HAL_CLK_SOURCE_LPAPLL2];
  pDrvCtxt->aSources[nIdx].pActiveFreqConfig = 
    pDrvCtxt->aSources[nIdx].pBSPConfig->pSourceFreqConfig;


  /*----------------------------------------------------------------------*/
  /* Enable the AON bus clock and set the frequency based on what was     */
  /* programmed in the BSP in the early init.                             */
  /*----------------------------------------------------------------------*/

  if(DAL_SUCCESS == Clock_GetClockId(pDrvCtxt, "audio_wrapper_aon_clk", &nAONBusClkId))
  {
    eRes = Clock_EnableClock(pDrvCtxt, nAONBusClkId);

    if(DAL_SUCCESS != eRes)
    {
      DALSYS_LogEvent(
          DALDEVICEID_CLOCK, DALSYS_LOGEVENT_FATAL_ERROR,
          "Clock_InitImage:  Failed to enable the audio_wrapper_aon_clk clock.  Error code: %x", eRes);
      return eRes;
    }
  }

  /*-----------------------------------------------------------------------*/
  /* Now that NPA is initialized, allow Q6 scaling by the power manager.   */
  /*-----------------------------------------------------------------------*/

  eRes = Clock_EnableDCVS(pDrvCtxt);

  if(DAL_SUCCESS != eRes)
  {
    DALSYS_LogEvent(
      DALDEVICEID_CLOCK, DALSYS_LOGEVENT_FATAL_ERROR,
      "Clock_InitImage:  Failed to initialize DCVS %d", eRes);
    return eRes;
  }

  /*-----------------------------------------------------------------------*/
  /* Initialize the NPA Voltage NPA node.                                  */
  /*-----------------------------------------------------------------------*/

  eRes = Clock_InitVdd(pDrvCtxt);

  if(DAL_SUCCESS != eRes)
  {
    DALSYS_LogEvent(
      DALDEVICEID_CLOCK, DALSYS_LOGEVENT_FATAL_ERROR,
      "Clock_InitImage:  Failed to initialize Vdd node %d", eRes);
    return eRes;
  }

  /*-----------------------------------------------------------------------*/
  /* Initialize the HVX Coprocessor node.                                  */
  /*-----------------------------------------------------------------------*/

  eRes = Clock_InitHvx(pDrvCtxt);

  if(DAL_SUCCESS != eRes)
  {
    DALSYS_LogEvent(
      DALDEVICEID_CLOCK, DALSYS_LOGEVENT_FATAL_ERROR,
      "Clock_InitImage:  Failed to initialize HVX %d", eRes);
    return eRes;
  }

  /*
   * For v1 and v2, set a vote for the power domain.  This will have already
   * been enabled, but need to reference count this.  The ADSPPM will call
   * to disable this during their initialization.
   */
  if(DalChipInfo_ChipVersion() < DALCHIPINFO_VERSION(4, 0))
  {
    /*
     * Put in a vote for the core bus clock and the mport CBC.  These will
     * already be on at this point.  ADSPPM will later disable them.
     */
    if(DAL_SUCCESS == Clock_GetClockId(pDrvCtxt,
                                       "audio_core_core_clk",
                                       &nCoreClkId))
    {
      Clock_EnableClock(pDrvCtxt, nCoreClkId);

      if(DAL_SUCCESS == 
          Clock_GetClockId(pDrvCtxt,
                           "audio_core_sysnoc_mport_core_clk",
                           &nMportClkId))
      {
        Clock_EnableClock(pDrvCtxt, nMportClkId);

        if(DAL_SUCCESS == Clock_GetPowerDomainId(pDrvCtxt, "VDD_AUDIO_CORE", &nPowerDomain))
        {
          Clock_EnablePowerDomain(pDrvCtxt, nPowerDomain);
        }
      }
    }
  }

  /*-----------------------------------------------------------------------*/
  /* Set a indefinite vote for the LPAAUDIO_DIG PLL.  This PLL will remain */
  /* on until the SPM collapses it for power collapse in order to mitigate */
  /* extra delays caused by enabling the PLL via vote.                     */
  /*-----------------------------------------------------------------------*/

  HWIO_OUTF(LPASS_LPA_PLL_VOTE_LPASSQ6, LPAAUDIO_DIG_PLL, 0x1);

  /*-----------------------------------------------------------------------*/
  /* Good to go.                                                           */
  /*-----------------------------------------------------------------------*/

  return DAL_SUCCESS;

} /* END Clock_InitImage */


/* =========================================================================
**  Function : Clock_ProcessorSleep
** =========================================================================*/
/*
  See DDIClock.h
*/

DALResult Clock_ProcessorSleep
(
  ClockDrvCtxt       *pDrvCtxt,
  ClockSleepModeType  eMode,
  uint32              nFlags
)
{

  return DAL_SUCCESS;

} /* END Clock_ProcessorSleep */


/* =========================================================================
**  Function : Clock_ProcessorRestore
** =========================================================================*/
/*
  See DDIClock.h
*/

DALResult Clock_ProcessorRestore
(
  ClockDrvCtxt       *pDrvCtxt,
  ClockSleepModeType  eMode,
  uint32              nFlags
) 
{

  /*
   * Nothing to do here. HW_VOTE does SPMCTL override handling.
  */

  return DAL_SUCCESS;

} /* END Clock_ProcessorRestore */


/* =========================================================================
**  Function : Clock_AdjustSourceFrequency
** =========================================================================*/
/*
  See DDIClock.h
*/
DALResult Clock_AdjustSourceFrequency
(
   ClockDrvCtxt    *pDrvCtxt,
   ClockSourceType eSource,
   int32           nDeltaLAlpha
)
{
  return(DAL_ERROR);

} /* Clock_AdjustSourceFrequency */


DALResult Clock_SelectClockSource
(
   ClockDrvCtxt    *pDrvCtxt,
   ClockIdType     nClock,
   ClockSourceType eSource
)
{

  if(eSource == CLOCK_SOURCE_PRIMARY)
  {
    return(DAL_SUCCESS);
  }
  return(DAL_ERROR);

} /* Clock_SelectClockSource */


/* =========================================================================
**  Function : Clock_GetImageCtxt
** =========================================================================*/
/*
  See ClockLPASS.h
*/

ClockImageCtxtType* Clock_GetImageCtxt(void)
{
  return(&Clock_ImageCtxt);
}

/*
 * Unused APIs are stubbed out here.
 */

DALResult Clock_ImageBIST
(
  ClockDrvCtxt  *pDrvCtxt,
  boolean       *bBISTPassed,
  uint32        *nFailedTests
)
{
  return(DAL_ERROR);
}

DALResult Clock_LoadNV
(
  ClockDrvCtxt  *pDrvCtxt
)
{
  return(DAL_ERROR_NOT_SUPPORTED);
}


