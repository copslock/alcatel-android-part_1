# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/GPIOMgr/config/GPIOMgrConfigData.c"
# 1 "<built-in>" 1
# 1 "<built-in>" 3
# 140 "<built-in>" 3
# 1 "<command line>" 1
# 1 "<built-in>" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/GPIOMgr/config/GPIOMgrConfigData.c" 2
# 29 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/GPIOMgr/config/GPIOMgrConfigData.c"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/com_dtypes.h" 1
# 88 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/com_dtypes.h"
typedef unsigned char boolean;
# 107 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/com_dtypes.h"
typedef unsigned long int uint32;




typedef unsigned short uint16;




typedef unsigned char uint8;




typedef signed long int int32;




typedef signed short int16;




typedef signed char int8;







typedef unsigned char byte;



typedef unsigned short word;
typedef unsigned long dword;

typedef unsigned char uint1;
typedef unsigned short uint2;
typedef unsigned long uint4;

typedef signed char int1;
typedef signed short int2;
typedef long int int4;

typedef signed long sint31;
typedef signed short sint15;
typedef signed char sint7;

typedef uint16 UWord16 ;
typedef uint32 UWord32 ;
typedef int32 Word32 ;
typedef int16 Word16 ;
typedef uint8 UWord8 ;
typedef int8 Word8 ;
typedef int32 Vect32 ;
# 181 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/services/com_dtypes.h"
      typedef long long int64;



      typedef unsigned long long uint64;
# 30 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/GPIOMgr/config/GPIOMgrConfigData.c" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/GPIOMgr/inc/DDIGPIOMgr.h" 1
# 39 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/GPIOMgr/inc/DDIGPIOMgr.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DalDevice.h" 1
# 15 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DalDevice.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALStdDef.h" 1
# 76 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALStdDef.h"
typedef uint32 DALBOOL;
typedef uint32 DALDEVICEID;
typedef uint32 DalPowerCmd;
typedef uint32 DalPowerDomain;
typedef uint32 DalSysReq;
typedef uint32 DALHandle;
typedef int DALResult;
typedef void * DALEnvHandle;
typedef void * DALSYSEventHandle;
typedef uint32 DALMemAddr;
typedef uint32 DALSYSMemAddr;
typedef uint64 DALSYSPhyAddr;
typedef uint32 DALInterfaceVersion;







typedef unsigned char * DALDDIParamPtr;

typedef struct DALEventObject DALEventObject;
struct DALEventObject
{
    uint32 obj[8];
};
typedef DALEventObject * DALEventHandle;

typedef struct _DALMemObject
{
   uint32 memAttributes;
   uint32 sysObjInfo[2];
   uint32 dwLen;
   uint32 ownerVirtAddr;
   uint32 virtAddr;
   uint32 physAddr;
}
DALMemObject;

typedef struct _DALDDIMemBufDesc
{
   uint32 dwOffset;
   uint32 dwLen;
   uint32 dwUser;
}
DALDDIMemBufDesc;


typedef struct _DALDDIMemDescList
{
   uint32 dwFlags;
   uint32 dwNumBufs;
   DALDDIMemBufDesc bufList[1];
}
DALDDIMemDescList;





typedef struct DALSysMemDescBuf DALSysMemDescBuf;
struct DALSysMemDescBuf
{
   DALSYSMemAddr VirtualAddr;
   DALSYSMemAddr PhysicalAddr;
   uint32 size;
   uint32 user;
};
# 158 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALStdDef.h"
typedef struct { uint32 dwObjInfo; DALSYSMemAddr thisVirtualAddr; DALSYSMemAddr PhysicalAddr; DALSYSMemAddr VirtualAddr; uint32 hOwnerProc; uint32 dwCurBufIdx; uint32 dwNumDescBufs; DALSysMemDescBuf BufInfo[1];} DALSysMemDescList;
# 16 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DalDevice.h" 2
# 55 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DalDevice.h"
typedef struct DalDeviceInfo DalDeviceInfo;
struct DalDeviceInfo
{
   uint32 sizeOfActual;
   uint32 Version;
   char Name[32];
};

typedef struct DalDeviceStatus DalDeviceStatus;
struct DalDeviceStatus
{
   uint32 sizeOfActual;
   uint32 Status;
};







typedef struct DalDeviceHandle DalDeviceHandle;
typedef struct DalDevice DalDevice;
struct DalDevice
{
   DALResult (*Attach)(const char*,DALDEVICEID,DalDeviceHandle **);
   uint32 (*Detach)(DalDeviceHandle *);
   DALResult (*Init)(DalDeviceHandle *);
   DALResult (*DeInit)(DalDeviceHandle *);
   DALResult (*Open)(DalDeviceHandle *, uint32);
   DALResult (*Close)(DalDeviceHandle *);
   DALResult (*Info)(DalDeviceHandle *, DalDeviceInfo *, uint32);
   DALResult (*PowerEvent)(DalDeviceHandle *, DalPowerCmd, DalPowerDomain);
   DALResult (*SysRequest)(DalDeviceHandle *, DalSysReq, const void *, uint32,
                           void *,uint32, uint32*);
};

typedef struct DalInterface DalInterface;
struct DalInterface
{
   struct DalDevice DalDevice;

};

struct DalDeviceHandle
{
   uint32 dwDalHandleId;
   const DalInterface *pVtbl;
   void *pClientCtxt;
   uint32 dwVtblLen;
};

typedef struct DalDeviceHandle * DALDEVICEHANDLE;

typedef struct DalRemoteHandle DalRemoteHandle;
typedef struct DalRemote DalRemote;
struct DalRemote
{
   struct DalDevice DalDevice;
   DALResult (* FCN_0) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1 );
   DALResult (* FCN_1) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32 u2 );
   DALResult (* FCN_2) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32* p_u2 );
   DALResult (* FCN_3) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32 u2, uint32 u3 );
   DALResult (* FCN_4) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32 u2, uint32* p_u3 );
   DALResult (* FCN_5) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen);
   DALResult (* FCN_6) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * ibuf, uint32 ilen);
   DALResult (* FCN_7) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * obuf, uint32 olen, uint32 *oalen);
   DALResult (* FCN_8) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * obuf, uint32 olen);
   DALResult (* FCN_9) ( uint32 ddi_idx, DalDeviceHandle *h, void * obuf, uint32 olen );
   DALResult (* FCN_10)( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * ibuf, uint32 ilen, void * obuf, uint32 olen, uint32 * oalen);
   DALResult (* FCN_11) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * obuf, uint32 olen);
   DALResult (* FCN_12) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * obuf, uint32 olen, uint32 *oalen);
   DALResult (* FCN_13) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * ibuf2, uint32 ilen2, void * obuf, uint32 olen);
   DALResult (* FCN_14) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * obuf1, uint32 olen, void * obuf2, uint32 olen2, uint32 * oalen);
   DALResult (* FCN_15) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * ibuf2, uint32 ilen2, void * obuf2, uint32 olen2, uint32 *oalen, void * obuf, uint32 olen );
};

struct DalRemoteHandle
{
   uint32 dwDalHandleId;
   const DalRemote *pVtbl;
   void *pClientCtxt;
};






static __inline uint32
DalDevice_Detach(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Detach(_h);
}

static __inline DALResult
DalDevice_Init(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Init(_h);
}

static __inline DALResult
DalDevice_DeInit(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.DeInit(_h);
}

static __inline DALResult
DalDevice_PowerEvent(DalDeviceHandle *_h, DalPowerCmd PowerCmd,
                     DalPowerDomain PowerDomain)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.PowerEvent(_h,PowerCmd,PowerDomain);
}

static __inline DALResult
DalDevice_Open(DalDeviceHandle *_h, uint32 mode)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Open(_h,mode);
}

static __inline DALResult
DalDevice_Close(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Close(_h);
}

static __inline DALResult
DalDevice_Info(DalDeviceHandle *_h, DalDeviceInfo* info, uint32 infoSize)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Info(_h,info,infoSize);
}

static __inline DALResult
DalDevice_SysRequest(DalDeviceHandle *_h, DalSysReq ReqIdx,
                     const unsigned char* SrcBuf, int SrcBufLen,
                     unsigned char* DestBuf, uint32 DestBufLen, uint32* DestBufLenReq)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.SysRequest(_h,ReqIdx,SrcBuf,SrcBufLen,
                                          DestBuf,DestBufLen,DestBufLenReq);
}
# 218 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DalDevice.h"
DALResult
DAL_DeviceAttach(DALDEVICEID DeviceId,
                 DalDeviceHandle **phDevice);
# 239 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DalDevice.h"
DALResult
DAL_DeviceAttachLocal(const char *pszArg,DALDEVICEID DeviceId,
                      DalDeviceHandle **phDevice);
# 259 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DalDevice.h"
DALResult
DAL_DeviceAttachEx(const char *pszArg, DALDEVICEID DeviceId,
               DALInterfaceVersion ClientVersion,DalDeviceHandle **phDevice);






DALResult
DAL_StringDeviceAttachEx(const char *pszArg,
                   const char *pszDevName,
                   DALInterfaceVersion ClientVersion,
                   DalDeviceHandle **phDalDevice);





DALResult
DAL_DeviceAttachRemote(const char *pszArg,
                   DALDEVICEID DevId,
                   DALInterfaceVersion ClientVersion,
                   DalDeviceHandle **phDALDevice);
# 299 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DalDevice.h"
DALResult
DAL_DeviceDetach(DalDeviceHandle *hDevice);
# 314 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DalDevice.h"
DALResult
DAL_StringDeviceAttach(const char *pszDevName,DalDeviceHandle **phDalDevice);
# 40 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/GPIOMgr/inc/DDIGPIOMgr.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALDeviceId.h" 1
# 41 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/GPIOMgr/inc/DDIGPIOMgr.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h" 1
# 18 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSysTypes.h" 1
# 19 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSysTypes.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALStdErr.h" 1
# 20 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSysTypes.h" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALReg.h" 1
# 22 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALReg.h"
typedef struct DALREG_DriverInfo DALREG_DriverInfo;
struct DALREG_DriverInfo
{
 DALResult (*pfnDALNewFunc)(const char * , DALDEVICEID, DalDeviceHandle**);
 uint32 dwNumDevices;
 DALDEVICEID *pDeviceId;
};


typedef struct DALREG_DriverInfoList DALREG_DriverInfoList;
struct DALREG_DriverInfoList
{
 uint32 dwLen;
 DALREG_DriverInfo ** pDriverInfo;
};
# 21 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSysTypes.h" 2
# 37 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSysTypes.h"
typedef void * DALSYSObjHandle;
typedef void * DALSYSSyncHandle;
typedef void * DALSYSMemHandle;
typedef void * DALSYSWorkLoopHandle;
typedef uint32 *DALSYSPropertyHandle;

typedef struct DALSYSPropertyVar DALSYSPropertyVar;
struct DALSYSPropertyVar
{
    uint32 dwType;
    uint32 dwLen;
    union
    {
        byte *pbVal;
        char *pszVal;
        uint32 dwVal;
        uint32 *pdwVal;
        const void *pStruct;
    }Val;
};


typedef struct DALSYSPropStructTblType DALSYSPropStructTblType ;
struct DALSYSPropStructTblType{
   uint32 dwSize;
   const void *pStruct;
};


typedef struct StringDevice StringDevice;
 struct StringDevice{
   char *pszName;
   uint32 dwHash;
   uint32 dwOffset;
   DALREG_DriverInfo *pFunctionName;
   uint32 dwNumCollision;
   uint32 *pdwCollisions;
};

typedef struct DALProps DALProps;
struct DALProps
{
   const byte *pDALPROP_PropBin;
   const DALSYSPropStructTblType *pDALPROP_StructPtrs;
   const uint32 dwDeviceSize;
   const StringDevice *pDevices;
};

typedef struct DALSYSBaseObj DALSYSBaseObj;
struct DALSYSBaseObj
{
   uint32 dwObjInfo;
   uint32 hOwnerProc;
   DALSYSMemAddr thisVirtualAddr;
};







typedef struct DALSYSEventObj DALSYSEventObj;
struct DALSYSEventObj
{
   unsigned long long _bSpace[80/sizeof(unsigned long long)];
};

typedef struct DALSYSSyncObj DALSYSSyncObj;
struct DALSYSSyncObj
{
   unsigned long long _bSpace[40/sizeof(unsigned long long)];
};

typedef struct DALSYSMemObj DALSYSMemObj;
struct DALSYSMemObj
{
   unsigned long long _bSpace[40/sizeof(unsigned long long)];
};

typedef struct DALSYSWorkLoopEventObj DALSYSWorkLoopEventObj;
struct DALSYSWorkLoopEventObj
{
   unsigned long long _bSpace[32/sizeof(unsigned long long)];
};

typedef struct DALSYSWorkLoopObj DALSYSWorkLoopObj;
struct DALSYSWorkLoopObj
{
   unsigned long long _bSpace[64/sizeof(unsigned long long)];
};

typedef void * DALSYSCallbackFuncCtx;
typedef void * (*DALSYSCallbackFunc)(void*,uint32,void*,uint32);


typedef struct DALSYSMemInfo DALSYSMemInfo;
struct DALSYSMemInfo
{
    DALSYSMemAddr VirtualAddr;
    DALSYSMemAddr PhysicalAddr;
    uint32 dwLen;
    uint32 dwMappedLen;
    uint32 dwProps;
};

typedef enum
{
   DALSYS_MEM_CACHE_DEVICE,
   DALSYS_MEM_CACHE_NONE,
   DALSYS_MEM_CACHE_WRITEBACK,
   DALSYS_MEM_CACHE_WRITETHROUGH,
   DALSYS_MEM_CACHE_INVALID
}
DALSYSMemCacheType;

typedef enum
{
   DALSYS_MEM_PERM_NONE=0,
   DALSYS_MEM_PERM_R=0x1,
   DALSYS_MEM_PERM_W=0x2,
   DALSYS_MEM_PERM_RW=0x3,
   DALSYS_MEM_PERM_X=0x4,
   DALSYS_MEM_PERM_RX=0x5,
   DALSYS_MEM_PERM_WX=0x6,
   DALSYS_MEM_PERM_RWX=0x7,
   DALSYS_MEM_PERM_INVALID=0x8
}
DALSYSMemPermType;

typedef enum
{
   DALSYS_MEM_SHARE_ALLOWED,
   DALSYS_MEM_SHARE_NOT_ALLOWED,
   DALSYS_MEM_SHARE_INVALID
}
DALSYSMemShareType;


typedef struct DALSYSMemInfoEx DALSYSMemInfoEx;
struct DALSYSMemInfoEx
{
    DALSYSPhyAddr physicalAddr;
    DALSYSMemAddr virtualAddr;
    DALSYSMemAddr size;
    DALSYSMemCacheType cacheType;
    DALSYSMemPermType permission;
    DALSYSMemShareType shareType;
};






typedef struct DALSYSMemReq DALSYSMemReq;
struct DALSYSMemReq
{
    DALSYSMemInfoEx memInfo;

    DALSYSMemObj *pObj;

    DALBOOL prealloc;
};

typedef enum
{
   DEVCFG_TARGET_INFO_SOC,
   DEVCFG_TARGET_INFO_PLATFORM
}DEVCFG_TARGET_INFO_TYPE;
# 243 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSysTypes.h"
typedef DALResult
(*DALSYSWorkLoopExecute)
(
    DALSYSEventHandle,
    void *
);

typedef void
(*DALSYS_InitSystemHandleFncPtr)
(
    DalDeviceHandle * hDalDevice
);

typedef DALResult
(*DALSYS_DestroyObjectFncPtr)
(
    DALSYSObjHandle
);

typedef DALResult
(*DALSYS_CopyObjectFncPtr)
(
    DALSYSObjHandle,
    DALSYSObjHandle *
);

typedef DALResult
(*DALSYS_RegisterWorkLoopFncPtr)
(
    uint32,
    uint32,
    DALSYSWorkLoopHandle *,
    DALSYSWorkLoopObj *
);

typedef DALResult
(*DALSYS_RegisterWorkLoopExFncPtr)
(
    char *,
    uint32,
    uint32,
    uint32,
    DALSYSWorkLoopHandle *,
    DALSYSWorkLoopObj *
);

typedef DALResult
(*DALSYS_AddEventToWorkLoopFncPtr)
(
    DALSYSWorkLoopHandle,
    DALSYSWorkLoopExecute,
    void *,
    DALSYSEventHandle,
    DALSYSSyncHandle
);

typedef DALResult
(*DALSYS_DeleteEventFromWorkLoopFncPtr)
(
    DALSYSWorkLoopHandle,
    DALSYSEventHandle
);

typedef DALResult
(*DALSYS_EventCreateFncPtr)
(
    uint32 ,
    DALSYSEventHandle *,
    DALSYSEventObj *
);

typedef DALResult
(*DALSYS_EventCtrlFncPtr)
(
    DALSYSEventHandle,
    uint32,
   uint32,
   void *,
   uint32
);

typedef DALResult
(*DALSYS_EventWaitFncPtr)
(
    DALSYSEventHandle
);

typedef DALResult
(*DALSYS_EventMultipleWaitFncPtr)
(
    DALSYSEventHandle*,
    int,
    uint32,
    uint32 *
);
typedef DALResult
(*DALSYS_SetupCallbackEventFncPtr)
(
    DALSYSEventHandle,
    DALSYSCallbackFunc,
    DALSYSCallbackFuncCtx
);

typedef DALResult
(*DALSYS_SyncCreateFncPtr)
(
    uint32,
    DALSYSSyncHandle *,
    DALSYSSyncObj *
);

typedef void
(*DALSYS_SyncEnterFncPtr)
(
    DALSYSSyncHandle
);

typedef DALResult
(*DALSYS_SyncTryEnterFncPtr)
(
    DALSYSSyncHandle
);

typedef void
(*DALSYS_SyncLeaveFncPtr)
(
    DALSYSSyncHandle
);

typedef DALResult
(*DALSYS_MemRegionAllocFncPtr)
(
    uint32,
    DALSYSMemAddr,
    DALSYSMemAddr,
    uint32,
    DALSYSMemHandle *,
    DALSYSMemObj *
);

typedef DALResult
(*DALSYS_MemRegionMapPhysFncPtr)
(
    DALSYSMemHandle,
    uint32,
    DALSYSMemAddr,
    uint32
);

typedef DALResult
(*DALSYS_MemInfoFncPtr)
(
    DALSYSMemHandle,
    DALSYSMemInfo *
);

typedef DALResult
(*DALSYS_CacheCommandFncPtr)
(
    uint32 ,
    DALSYSMemAddr,
    uint32
);

typedef DALResult
(*DALSYS_MallocFncPtr)
(
    uint32 ,
    void **
);

typedef DALResult
(*DALSYS_FreeFncPtr)
(
    void *
);

typedef void
(*DALSYS_BusyWaitFncPtr)
(
   uint32
);

typedef DALResult
(*DALSYS_MemDescAddBufFncPtr)
(
    DALSysMemDescList *,
    uint32,
    uint32,
    uint32
);

typedef DALResult
(*DALSYS_MemDescPrepareFncPtr)
(
    DALSysMemDescList *,
    uint32
);

typedef DALResult
(*DALSYS_MemDescValidateFncPtr)
(
    DALSysMemDescList *,
    uint32
);

typedef DALResult
(*DALSYS_GetDALPropertyHandleFncPtr)
(
    DALDEVICEID,
    DALSYSPropertyHandle
);

typedef DALResult
(*DAL_DeviceAttachFncPtr)
(
    const char *pszArg,
    DALDEVICEID,
    DalDeviceHandle **
);

typedef DALResult
(*DAL_DeviceDetachFncPtr)
(
    DalDeviceHandle *
);

typedef DALResult
(*DAL_DeviceAttachExFncPtr)
(
 const char *pszArg,
    DALDEVICEID DevId,
    DALInterfaceVersion ClientVersion,
    DalDeviceHandle **phDalDevice
);

typedef DALResult
(*DALSYS_GetPropertyValueFncPtr)
(
    DALSYSPropertyHandle,
    const char *,
    uint32,
    DALSYSPropertyVar *
);

typedef void
(*DALSYS_LogEventFncPtr)
(
    DALDEVICEID,
    uint32,
    const char *
);

typedef struct DALSYSFncPtrTbl DALSYSFncPtrTbl;
struct DALSYSFncPtrTbl
{
    DALSYS_InitSystemHandleFncPtr DALSYS_InitSystemHandleFnc;
    DALSYS_DestroyObjectFncPtr DALSYS_DestroyObjectFnc;
    DALSYS_CopyObjectFncPtr DALSYS_CopyObjectFnc;
    DALSYS_RegisterWorkLoopFncPtr DALSYS_RegisterWorkLoopFnc;
    DALSYS_RegisterWorkLoopExFncPtr DALSYS_RegisterWorkLoopExFnc;
    DALSYS_AddEventToWorkLoopFncPtr DALSYS_AddEventToWorkLoopFnc;
    DALSYS_DeleteEventFromWorkLoopFncPtr DALSYS_DeleteEventFromWorkLoopFnc;
    DALSYS_EventCreateFncPtr DALSYS_EventCreateFnc;
    DALSYS_EventCtrlFncPtr DALSYS_EventCtrlFnc;
    DALSYS_EventWaitFncPtr DALSYS_EventWaitFnc;
    DALSYS_EventMultipleWaitFncPtr DALSYS_EventMultipleWaitFnc;
    DALSYS_SetupCallbackEventFncPtr DALSYS_SetupCallbackEventFnc;
    DALSYS_SyncCreateFncPtr DALSYS_SyncCreateFnc;
    DALSYS_SyncEnterFncPtr DALSYS_SyncEnterFnc;
    DALSYS_SyncTryEnterFncPtr DALSYS_SyncTryEnterFnc;
    DALSYS_SyncLeaveFncPtr DALSYS_SyncLeaveFnc;
    DALSYS_MemRegionAllocFncPtr DALSYS_MemRegionAllocFnc;
    DALSYS_MemRegionMapPhysFncPtr DALSYS_MemRegionMapPhysFnc;
    DALSYS_MemInfoFncPtr DALSYS_MemInfoFnc;
    DALSYS_CacheCommandFncPtr DALSYS_CacheCommandFnc;
    DALSYS_MallocFncPtr DALSYS_MallocFnc;
    DALSYS_FreeFncPtr DALSYS_FreeFnc;
    DALSYS_BusyWaitFncPtr DALSYS_BusyWaitFnc;
    DALSYS_MemDescAddBufFncPtr DALSYS_MemDescAddBufFnc;
    DALSYS_MemDescPrepareFncPtr DALSYS_MemDescPrepareFnc;
    DALSYS_MemDescValidateFncPtr DALSYS_MemDescValidateFnc;
    DALSYS_GetDALPropertyHandleFncPtr DALSYS_GetDALPropertyHandleFnc;
    DAL_DeviceAttachFncPtr DALSYS_DeviceAttachFnc;
    DAL_DeviceAttachExFncPtr DALSYS_DeviceAttachExFnc;
    DAL_DeviceAttachExFncPtr DALSYS_DeviceAttachRemoteFnc;
    DAL_DeviceDetachFncPtr DALSYS_DeviceDetachFnc;
    DALSYS_GetPropertyValueFncPtr DALSYS_GetPropertyValueFnc;
    DALSYS_LogEventFncPtr DALSYS_LogEventFnc;
};


typedef DALResult
(*DALRemote_NewFncPtr)(const char *, DALDEVICEID, DalDeviceHandle **);
typedef int
(*DALRemote_CommonInitFncPtr)(void);
typedef void
(*DALRemote_IPCInitFcnPtr)(uint32);
typedef int
(*DALRemote_InitFncPtr)(void);
typedef DALSYSEventHandle
(*DALRemote_CreateEventFncPtr)(void *pClientCtxt, DALSYSEventHandle hRemote);
typedef uint32
(*DALRemoteInterProcessCallFncPtr)(void *in_buf, void *out_buf);
typedef int
(*DALRemote_DeinitFncPtr)(void);

typedef struct DALRemoteVtbl DALRemoteVtbl;
struct DALRemoteVtbl
{
   DALRemote_NewFncPtr DALRemote_NewFnc;
   DALRemote_CommonInitFncPtr DALRemote_CommonInitFnc;
   DALRemote_InitFncPtr DALRemote_InitFnc;
   DALRemote_DeinitFncPtr DALRemote_DeinitFnc;
   DALRemote_CreateEventFncPtr DALRemote_CreateEventFnc;
   DALRemote_CreateEventFncPtr DALRemote_CreatePayloadEventFnc;
   DALRemoteInterProcessCallFncPtr DALRemoteInterProcessCallFnc;
   DALRemote_IPCInitFcnPtr DALRemote_IPCInitFcn;
};

typedef struct DALSYSConfig DALSYSConfig;
struct DALSYSConfig
{
    void *pNativeEnv;
    uint32 dwConfig;
   DALRemoteVtbl *pRemoteVtbl;
};
# 19 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h" 2
# 1 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/string.h" 1 3 4
# 10 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/string.h" 3 4
# 1 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 1 3 4
# 297 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
typedef long _Int32t;
typedef unsigned long _Uint32t;
# 308 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
typedef int _Ptrdifft;
# 318 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
typedef unsigned int _Sizet;
# 676 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
# 1 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/stdarg.h" 1 3 4
# 12 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/stdarg.h" 3 4
typedef __builtin_va_list va_list;
# 677 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 2 3 4
# 786 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
typedef long long _Longlong;
typedef unsigned long long _ULonglong;
# 850 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
typedef int _Wchart;
typedef int _Wintt;
# 880 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
typedef va_list _Va_list;
# 902 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
void _Atexit(void (*)(void));
# 915 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
typedef char _Sysch_t;
# 935 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
void _Locksyslock(int);
void _Unlocksyslock(int);
# 1066 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
typedef unsigned short __attribute__((__may_alias__)) alias_short;
static alias_short *strict_aliasing_workaround(unsigned short *ptr) __attribute__((always_inline,unused));

static alias_short *strict_aliasing_workaround(unsigned short *ptr)
{
  alias_short *aliasptr = (alias_short *)ptr;
  return aliasptr;
}
# 11 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/string.h" 2 3 4
# 29 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/string.h" 3 4
typedef _Sizet size_t;




int memcmp(const void *, const void *, size_t) __attribute__((__nothrow__));
void *memcpy(void *, const void *, size_t) __attribute__((__nothrow__));
void *memcpy_v(volatile void *, const volatile void *, size_t) __attribute__((__nothrow__));
void *memset(void *, int, size_t) __attribute__((__nothrow__));
char *strcat(char *, const char *) __attribute__((__nothrow__));
int strcmp(const char *, const char *) __attribute__((__nothrow__));
char *strcpy(char *, const char *) __attribute__((__nothrow__));
size_t strlen(const char *) __attribute__((__nothrow__));

void *memmove(void *, const void *, size_t) __attribute__((__nothrow__));
void *memmove_v(volatile void *, const volatile void *, size_t) __attribute__((__nothrow__));
int strcoll(const char *, const char *) __attribute__((__nothrow__));
size_t strcspn(const char *, const char *) __attribute__((__nothrow__));
char *strerror(int) __attribute__((__nothrow__));
size_t strlcat(char *, const char *, size_t) __attribute__((__nothrow__));
char *strncat(char *, const char *, size_t) __attribute__((__nothrow__));
int strncmp(const char *, const char *, size_t) __attribute__((__nothrow__));
size_t strlcpy(char *, const char *, size_t) __attribute__((__nothrow__));
char *strncpy(char *, const char *, size_t) __attribute__((__nothrow__));
size_t strspn(const char *, const char *) __attribute__((__nothrow__));
char *strtok(char *, const char *) __attribute__((__nothrow__));
char *strsep(char **, const char *) __attribute__((__nothrow__));
size_t strxfrm(char *, const char *, size_t) __attribute__((__nothrow__));


char *strdup(const char *) __attribute__((__nothrow__));
int strcasecmp(const char *, const char *) __attribute__((__nothrow__));
int strncasecmp(const char *, const char *, size_t) __attribute__((__nothrow__));
char *strtok_r(char *, const char *, char **) __attribute__((__nothrow__));

void *memccpy (void *, const void *, int, size_t) __attribute__((__nothrow__));
int strerror_r (int, char *, size_t) __attribute__((__nothrow__));
# 106 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/string.h" 3 4
char *strchr(const char *, int) __attribute__((__nothrow__));
char *strpbrk(const char *, const char *) __attribute__((__nothrow__));
char *strrchr(const char *, int) __attribute__((__nothrow__));
char *strstr(const char *, const char *) __attribute__((__nothrow__));
# 130 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/string.h" 3 4
void *memchr(const void *, int, size_t) __attribute__((__nothrow__));
# 20 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h" 2
# 227 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
void
DALSYS_InitMod(DALSYSConfig * pCfg);
# 238 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
void
DALSYS_DeInitMod(void);
# 248 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
void
DALSYS_InitSystemHandle(DalDeviceHandle *hDalDevice);
# 264 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALSYS_LogEventFncPtr
DALSYS_SetLogCfg(uint32 dwMaxLogLevel, DALSYS_LogEventFncPtr DALSysLogFcn);
# 279 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_DestroyObject(DALSYSObjHandle hObj);
# 292 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_CopyObject(DALSYSObjHandle hObjOrig, DALSYSObjHandle *phObjCopy );
# 310 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_RegisterWorkLoop(uint32 dwPriority,
                  uint32 dwMaxNumEvents,
                  DALSYSWorkLoopHandle *phWorkLoop,
                  DALSYSWorkLoopObj *pWorkLoopObj);
# 334 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_RegisterWorkLoopEx(
                  char * pszname,
                  uint32 dwStackSize,
                  uint32 dwPriority,
                  uint32 dwMaxNumEvents,
                  DALSYSWorkLoopHandle *phWorkLoop,
                  DALSYSWorkLoopObj *pWorkLoopObj);
# 358 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_AddEventToWorkLoop(DALSYSWorkLoopHandle hWorkLoop,
                    DALSYSWorkLoopExecute pfnWorkLoopExecute,
                    void * pArg,
                    DALSYSEventHandle hEvent,
                    DALSYSSyncHandle hSync);
# 375 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_DeleteEventFromWorkLoop(DALSYSWorkLoopHandle hWorkLoop,
                         DALSYSEventHandle hEvent);
# 394 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_EventCreate(uint32 dwAttribs, DALSYSEventHandle *phEvent,
               DALSYSEventObj *pEventObj);
# 415 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_EventCopy(DALSYSEventHandle hEvent,
             DALSYSEventHandle *phEventCopy,DALSYSEventObj *pEventObjCopy,
                 uint32 dwMarshalFlags);
# 434 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_EventCtrlEx(DALSYSEventHandle hEvent, uint32 dwCtrl, uint32 dwParam,
                   void *pPayload, uint32 dwPayloadSize);
# 458 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_EventWait(DALSYSEventHandle hEvent);
# 473 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_EventMultipleWait(DALSYSEventHandle* phEvent, int nEvents,
                         uint32 dwTimeoutUs,uint32 *pdwEventIdx);
# 492 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_SetupCallbackEvent(DALSYSEventHandle hEvent, DALSYSCallbackFunc cbFunc,
                          DALSYSCallbackFuncCtx cbFuncCtx);
# 508 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult DALSYS_TimerStart( DALSYSEventHandle hEvent, uint32 time);
# 519 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult DALSYS_TimerStop( DALSYSEventHandle hEvent );
# 555 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_SyncCreate(uint32 dwAttribs,
                  DALSYSSyncHandle *phSync,
                  DALSYSSyncObj *pSyncObj);
# 572 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
void
DALSYS_SyncEnter(DALSYSSyncHandle hSync);
# 589 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_SyncTryEnter(DALSYSSyncHandle hSync);
# 605 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
void
DALSYS_SyncLeave(DALSYSSyncHandle hSync);
# 634 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_MemRegionAlloc(uint32 dwAttribs, DALSYSMemAddr VirtualAddr,
                      DALSYSMemAddr PhysicalAddr, uint32 dwLen,
                      DALSYSMemHandle *phMem, DALSYSMemObj *pMemObj);
# 666 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_MemRegionAllocEx( DALSYSMemHandle *phMem, const DALSYSMemReq *pMemReq,
      DALSYSMemInfoEx *pMemInfo );
# 683 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_MemRegionMapPhys(DALSYSMemHandle hMem, uint32 dwVirtualBaseOffset,
                        DALSYSMemAddr PhysicalAddr, uint32 dwLen);
# 698 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_MemInfo(DALSYSMemHandle hMem, DALSYSMemInfo *pMemInfo);
# 712 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_MemInfoEx(DALSYSMemHandle hMem, DALSYSMemInfoEx *pMemInfo);
# 726 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_CacheCommand(uint32 CacheCmd, DALSYSMemAddr VirtualAddr, uint32 dwLen);
# 744 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_Malloc(uint32 dwSize, void **ppMem);
# 758 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_Free(void *pmem);
# 771 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
void
DALSYS_BusyWait(uint32 pause_time_us);
# 786 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_GetDALPropertyHandle(DALDEVICEID DeviceId, DALSYSPropertyHandle hDALProps);
# 802 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_GetDALPropertyHandleEx(DALDEVICEID DeviceId,DALSYSPropertyHandle hDALProps,
                              DEVCFG_TARGET_INFO_TYPE target_info_type);
# 818 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_GetDALPropertyHandleDyn(DALDEVICEID DeviceId, DALSYSPropertyHandle hDALProps);
# 833 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_GetDALPropertyHandleStr(const char *pszDevName, DALSYSPropertyHandle hDALProps);
# 849 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_GetDALPropertyHandleStrEx(const char *pszDevName, DALSYSPropertyHandle hDALProps,
                                 DEVCFG_TARGET_INFO_TYPE target_info_type);
# 865 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_GetDALPropertyHandleStrDyn(const char *pszDevName, DALSYSPropertyHandle hDALProps);
# 882 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALResult
DALSYS_GetPropertyValue(DALSYSPropertyHandle hDALProps, const char *pszName,
                  uint32 dwId,
                   DALSYSPropertyVar *pDALPropVar);
# 900 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
void
DALSYS_LogEvent(DALDEVICEID DeviceId, uint32 dwLogEventType,
      const char * pszFmt, ...);
# 913 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
DALRemoteVtbl *
DALSYS_GetRemoteInterfaceVtbl(void);
# 927 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
uint32 DALSYS_SetThreadPriority(uint32 priority);
# 943 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALSys.h"
uint32 _DALSYS_memscpy(void * pDest, uint32 iDestSz,
      const void * pSrc, uint32 iSrcSize);
# 42 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/GPIOMgr/inc/DDIGPIOMgr.h" 2

# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIGPIOInt.h" 1
# 92 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIGPIOInt.h"
typedef uint32 GPIOINTISRCtx;
typedef void * (*GPIOINTISR)(GPIOINTISRCtx);
# 105 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIGPIOInt.h"
typedef enum{
  GPIOINT_DEVICE_MODEM,
  GPIOINT_DEVICE_SPS,
  GPIOINT_DEVICE_LPA_DSP,
  GPIOINT_DEVICE_RPM,
  GPIOINT_DEVICE_APPS,
  GPIOINT_DEVICE_WCN,
  GPIOINT_DEVICE_DSP,
  GPIOINT_DEVICE_NONE,

  PLACEHOLDER_GPIOIntProcessorType = 0x7fffffff

}GPIOIntProcessorType;





typedef enum{
  GPIOINT_TRIGGER_HIGH,
  GPIOINT_TRIGGER_LOW,
  GPIOINT_TRIGGER_RISING,
  GPIOINT_TRIGGER_FALLING,
  GPIOINT_TRIGGER_DUAL_EDGE,

  PLACEHOLDER_GPIOIntTriggerType = 0x7fffffff

}GPIOIntTriggerType;





typedef struct GPIOInt GPIOInt;
struct GPIOInt
{
   struct DalDevice DalDevice;
   DALResult (*SetTrigger)(DalDeviceHandle * _h, uint32 gpio, GPIOIntTriggerType trigger);
   DALResult (*RegisterIsr)(DalDeviceHandle * _h, uint32 gpio,GPIOIntTriggerType trigger,
                            GPIOINTISR isr,GPIOINTISRCtx param);
   DALResult (*RegisterEvent)(DalDeviceHandle * _h, uint32 gpio, GPIOIntTriggerType trigger,
                              DALSYSEventHandle event);
   DALResult (*DeRegisterEvent)(DalDeviceHandle * _h, uint32 gpio,DALSYSEventHandle event);
   DALResult (*DeregisterIsr)(DalDeviceHandle * _h, uint32 gpio, GPIOINTISR isr);
   DALResult (*IsInterruptEnabled)(DalDeviceHandle * _h, uint32 gpio, void * state, uint32 size);
   DALResult (*IsInterruptPending)(DalDeviceHandle * _h, uint32 gpio, void * state, uint32 size);
   DALResult (*Save)(DalDeviceHandle * _h);
   DALResult (*Restore)(DalDeviceHandle * _h);
   DALResult (*DisableInterrupt)(DalDeviceHandle * _h, uint32 gpio);
   DALResult (*EnableInterrupt)(DalDeviceHandle * _h, uint32 gpio);
   DALResult (*InterruptNotify)(DalDeviceHandle * _h, uint32 gpio);
   DALResult (*MonitorInterrupts)(DalDeviceHandle * _h, GPIOINTISR isr,uint32 enable);
   DALResult (*MapMPMInterrupt)(DalDeviceHandle * h, uint32 gpio, uint32 mpm_interrupt_id);
   DALResult (*AttachRemote)(DalDeviceHandle * h, uint32 processor);
   DALResult (*TriggerInterrupt)(DalDeviceHandle * h, uint32 gpio);
   DALResult (*ClearInterrupt)(DalDeviceHandle * h, uint32 gpio);
   DALResult (*IsInterruptSet)(DalDeviceHandle * _h, uint32 gpio, void * state, uint32 size);
   DALResult (*SetDirectConnectGPIOMapping)(DalDeviceHandle * _h, uint32 gpio, uint32 direct_connect_line);
   DALResult (*IsInterruptRegistered)(DalDeviceHandle * _h, uint32 gpio, void * state, uint32 size);
   DALResult (*DisableGPIOInterrupt)(DalDeviceHandle * _h, uint32 gpio);
   DALResult (*EnableGPIOInterrupt)(DalDeviceHandle * _h, uint32 gpio);
   DALResult (*GPIOInt_RegisterIsrEx)(DalDeviceHandle * _h, uint32 gpio,GPIOIntTriggerType trigger,
                            GPIOINTISR isr,GPIOINTISRCtx param, uint32 nFlags);
};

typedef struct GPIOIntHandle GPIOIntHandle;
struct GPIOIntHandle
{
   uint32 dwDalHandleId;
   const GPIOInt * pVtbl;
   void * pClientCtxt;
};
# 204 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIGPIOInt.h"
static __inline DALResult
GPIOInt_SetTrigger(DalDeviceHandle * _h, uint32 gpio, GPIOIntTriggerType trigger)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_1(((uint32 *)&((((GPIOIntHandle *)_h)->pVtbl)->SetTrigger)-(uint32 *)(((GPIOIntHandle *)_h)->pVtbl)), _h, gpio, trigger);
   }
   return ((GPIOIntHandle *)_h)->pVtbl->SetTrigger( _h, gpio, trigger);
}
# 245 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIGPIOInt.h"
static __inline DALResult
GPIOInt_RegisterIsr(DalDeviceHandle * _h, uint32 gpio,GPIOIntTriggerType trigger,
GPIOINTISR isr,GPIOINTISRCtx param)
{
   if(!(((DALHandle)_h) & 0x00000001))
   {
     return ((GPIOIntHandle *)_h)->pVtbl->RegisterIsr( _h, gpio, trigger,isr,param);
   }
   else
       return -1;
}
# 290 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIGPIOInt.h"
static __inline DALResult
GPIOInt_RegisterIsrEx(DalDeviceHandle * _h, uint32 gpio,GPIOIntTriggerType trigger,
GPIOINTISR isr,GPIOINTISRCtx param, uint32 nFlags)
{
   if(!(((DALHandle)_h) & 0x00000001))
   {
     return ((GPIOIntHandle *)_h)->pVtbl->GPIOInt_RegisterIsrEx( _h, gpio, trigger,isr,param, nFlags);
   }
   else
     return -1;
}
# 330 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIGPIOInt.h"
static __inline DALResult
GPIOInt_RegisterEvent(DalDeviceHandle * _h, uint32 gpio,
                                         GPIOIntTriggerType trigger,DALSYSEventHandle event)
{
   if(!(((DALHandle)_h) & 0x00000001))
   {
     return ((GPIOIntHandle *)_h)->pVtbl->RegisterEvent( _h, gpio, trigger, event);
   }
   else
       return -1;
}
# 369 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIGPIOInt.h"
static __inline DALResult
GPIOInt_DeRegisterEvent(DalDeviceHandle * _h, uint32 gpio,DALSYSEventHandle event)
{
   if(!(((DALHandle)_h) & 0x00000001))
   {
     return ((GPIOIntHandle *)_h)->pVtbl->DeRegisterEvent( _h, gpio,event);
   }
   else
     return -1;
}
# 408 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIGPIOInt.h"
static __inline DALResult
GPIOInt_DeregisterIsr(DalDeviceHandle * _h, uint32 gpio, GPIOINTISR isr)
{
  if(!(((DALHandle)_h) & 0x00000001))
  {
    return ((GPIOIntHandle *)_h)->pVtbl->DeregisterIsr( _h, gpio, isr);
  }
  else
    return -1;
}
# 446 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIGPIOInt.h"
static __inline DALResult
GPIOInt_IsInterruptEnabled(DalDeviceHandle * _h, uint32 gpio, uint32* state)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_6(((uint32 *)&((((GPIOIntHandle *)_h)->pVtbl)->IsInterruptEnabled)-(uint32 *)(((GPIOIntHandle *)_h)->pVtbl)), _h, gpio, (void *)state, 1);
   }
   return ((GPIOIntHandle *)_h)->pVtbl->IsInterruptEnabled( _h, gpio, (void *)state, 1);
}
# 482 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIGPIOInt.h"
static __inline DALResult
GPIOInt_IsInterruptPending(DalDeviceHandle * _h, uint32 gpio, uint32 * state)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_6(((uint32 *)&((((GPIOIntHandle *)_h)->pVtbl)->IsInterruptPending)-(uint32 *)(((GPIOIntHandle *)_h)->pVtbl)), _h, gpio, (void *)state, 1);
   }
   return ((GPIOIntHandle *)_h)->pVtbl->IsInterruptPending( _h, gpio, (void *)state, 1);
}
# 518 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIGPIOInt.h"
static __inline DALResult
GPIOInt_IsInterruptSet(DalDeviceHandle * _h, uint32 gpio, uint32 * state)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_6(((uint32 *)&((((GPIOIntHandle *)_h)->pVtbl)->IsInterruptSet)-(uint32 *)(((GPIOIntHandle *)_h)->pVtbl)), _h, gpio, (void *)state, 1);
   }
   return ((GPIOIntHandle *)_h)->pVtbl->IsInterruptSet( _h, gpio, (void *)state, 1);
}
# 550 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIGPIOInt.h"
static __inline DALResult
GPIOInt_Save(DalDeviceHandle * _h)
{
  if(!(((DALHandle)_h) & 0x00000001))
  {
    return ((GPIOIntHandle *)_h)->pVtbl->Save( _h);
  }
  else
  {
    return -1;
  }
}
# 583 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIGPIOInt.h"
static __inline DALResult
GPIOInt_Restore(DalDeviceHandle * _h)
{
  if(!(((DALHandle)_h) & 0x00000001))
  {
    return ((GPIOIntHandle *)_h)->pVtbl->Restore( _h);
  }
  else
  {
    return -1;
  }
}
# 622 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIGPIOInt.h"
static __inline DALResult
GPIOInt_DisableInterrupt(DalDeviceHandle * _h, uint32 gpio)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_0(((uint32 *)&((((GPIOIntHandle *)_h)->pVtbl)->DisableInterrupt)-(uint32 *)(((GPIOIntHandle *)_h)->pVtbl)), _h, gpio);
   }
   return ((GPIOIntHandle *)_h)->pVtbl->DisableInterrupt( _h, gpio);
}
# 659 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIGPIOInt.h"
static __inline DALResult
GPIOInt_EnableInterrupt(DalDeviceHandle * _h, uint32 gpio)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_0(((uint32 *)&((((GPIOIntHandle *)_h)->pVtbl)->EnableInterrupt)-(uint32 *)(((GPIOIntHandle *)_h)->pVtbl)), _h, gpio);
   }
   return ((GPIOIntHandle *)_h)->pVtbl->EnableInterrupt( _h, gpio);
}
# 697 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIGPIOInt.h"
static __inline DALResult
GPIOInt_InterruptNotify(DalDeviceHandle * _h, uint32 gpio)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_0(((uint32 *)&((((GPIOIntHandle *)_h)->pVtbl)->InterruptNotify)-(uint32 *)(((GPIOIntHandle *)_h)->pVtbl)), _h, gpio);
   }
   return ((GPIOIntHandle *)_h)->pVtbl->InterruptNotify( _h, gpio);
}
# 735 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIGPIOInt.h"
static __inline DALResult
GPIOInt_MonitorInterrupts(DalDeviceHandle * _h, GPIOINTISR isr,uint32 enable)
{
  if(!(((DALHandle)_h) & 0x00000001))
   {
   return ((GPIOIntHandle *)_h)->pVtbl->MonitorInterrupts( _h,isr,enable);
}
  else
    return -1;

}
# 772 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIGPIOInt.h"
static __inline DALResult
GPIOInt_MapMPMInterrupt(DalDeviceHandle * _h, uint32 gpio, uint32 mpm_interrupt_id)
{
  if(!(((DALHandle)_h) & 0x00000001))
  {
    return ((GPIOIntHandle *)_h)->pVtbl->MapMPMInterrupt( _h, gpio,mpm_interrupt_id);
  }
  else
    return -1;
}
# 806 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIGPIOInt.h"
static __inline DALResult
GPIOInt_AttachRemote(DalDeviceHandle * _h, uint32 processor)
{
  if(!(((DALHandle)_h) & 0x00000001))
  {
    return ((GPIOIntHandle *)_h)->pVtbl->AttachRemote( _h, processor);
  }
  else
    return -1;
}
# 840 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIGPIOInt.h"
static __inline DALResult
GPIOInt_TriggerInterrupt(DalDeviceHandle * _h, uint32 gpio)
{

  if(!(((DALHandle)_h) & 0x00000001))
  {
    return ((GPIOIntHandle *)_h)->pVtbl->TriggerInterrupt( _h, gpio);
  }
  else
  {
    return -1;
  }

}
# 873 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIGPIOInt.h"
void GPIOInt_Init(void);
# 902 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIGPIOInt.h"
static __inline DALResult
GPIOInt_ClearInterrupt(DalDeviceHandle * _h, uint32 gpio)
{

  if(!(((DALHandle)_h) & 0x00000001))
  {
    return ((GPIOIntHandle *)_h)->pVtbl->ClearInterrupt( _h, gpio);
  }
  else
  {
    return -1;
  }

}
# 940 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIGPIOInt.h"
static __inline DALResult
GPIOInt_SetDirectConnectGPIOMapping(DalDeviceHandle * _h, uint32 gpio, uint32 direct_connect_line)
{

  if(!(((DALHandle)_h) & 0x00000001))
  {
    return ((GPIOIntHandle *)_h)->pVtbl->SetDirectConnectGPIOMapping( _h, gpio, direct_connect_line);
  }
  else
  {
    return -1;
  }

}
# 979 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/systemdrivers/DDIGPIOInt.h"
static __inline DALResult
GPIOInt_IsInterruptRegistered(DalDeviceHandle * _h, uint32 gpio, uint32 * state)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_6(((uint32 *)&((((GPIOIntHandle *)_h)->pVtbl)->IsInterruptRegistered)-(uint32 *)(((GPIOIntHandle *)_h)->pVtbl)), _h, gpio, (void *)state, 1);
   }
   return ((GPIOIntHandle *)_h)->pVtbl->IsInterruptRegistered( _h, gpio, (void *)state, 1);
}
# 44 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/GPIOMgr/inc/DDIGPIOMgr.h" 2
# 56 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/GPIOMgr/inc/DDIGPIOMgr.h"
typedef enum{
  GPIOMGR_DEVICE_MODEM = GPIOINT_DEVICE_MODEM,
  GPIOMGR_DEVICE_SPS = GPIOINT_DEVICE_SPS,
  GPIOMGR_DEVICE_LPA_DSP = GPIOINT_DEVICE_LPA_DSP,
  GPIOMGR_DEVICE_RPM = GPIOINT_DEVICE_RPM,
  GPIOMGR_DEVICE_APPS = GPIOINT_DEVICE_APPS,
  GPIOMGR_DEVICE_WCN = GPIOINT_DEVICE_WCN,
  GPIOMGR_DEVICE_DSP = GPIOINT_DEVICE_DSP,
  GPIOMGR_DEVICE_NONE = GPIOINT_DEVICE_NONE,

  PLACEHOLDER_GPIOMgrProcessorType = 0x7f

}GPIOMgrProcessorType;




typedef struct GPIOMgrIF GPIOMgrIF;
struct GPIOMgrIF
{
   struct DalDevice DalDevice;
   DALResult (*GPIOMgr_LockGPIO)(DalDeviceHandle * _h, uint32 nGPIO, uint32 nPD);
   DALResult (*GPIOMgr_ReleaseGPIO)(DalDeviceHandle * _h, uint32 nGPIO);
   DALResult (*GPIOMgr_GetGPIOCurrentPD)(DalDeviceHandle * _h, uint32 nGPIO, uint32* pnPD);
   DALResult (*GPIOMgr_GetDirectConnectGPIO)(DalDeviceHandle * _h, uint32 nGPIO, uint32* pDirConnID);
   DALResult (*GPIOMgr_ReleaseDirectConnectGPIO)(DalDeviceHandle * _h, uint32 nGPIO, uint32 nDirConnID);
   DALResult (*GPIOMgr_GetDirectConnectInterruptID)(DalDeviceHandle * _h, uint32 nGPIO, uint32* pInterruptID);
};

typedef struct GPIOMgrIFHandle GPIOMgrIFHandle;
struct GPIOMgrIFHandle
{
   uint32 dwDalHandleId;
   const GPIOMgrIF * pVtbl;
   void * pClientCtxt;
   uint32 dwVtblen;
};
# 118 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/GPIOMgr/inc/DDIGPIOMgr.h"
static __inline DALResult
DalGPIOMgr_LockGPIO(DalDeviceHandle * _h, uint32 nGPIO, uint32 nPD)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_1(((uint32 *)&((((GPIOMgrIFHandle *)_h)->pVtbl)->GPIOMgr_LockGPIO)-(uint32 *)(((GPIOMgrIFHandle *)_h)->pVtbl)), _h, nGPIO, nPD);
   }
   return ((GPIOMgrIFHandle *)_h)->pVtbl->GPIOMgr_LockGPIO( _h, nGPIO, nPD);
}
# 147 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/GPIOMgr/inc/DDIGPIOMgr.h"
static __inline DALResult
DalGPIOMgr_ReleaseGPIO(DalDeviceHandle * _h, uint32 nGPIO)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_0(((uint32 *)&((((GPIOMgrIFHandle *)_h)->pVtbl)->GPIOMgr_ReleaseGPIO)-(uint32 *)(((GPIOMgrIFHandle *)_h)->pVtbl)), _h, nGPIO);
   }
   return ((GPIOMgrIFHandle *)_h)->pVtbl->GPIOMgr_ReleaseGPIO( _h, nGPIO);
}
# 175 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/GPIOMgr/inc/DDIGPIOMgr.h"
static __inline DALResult
DalGPIOMgr_GetGPIOCurrentPD(DalDeviceHandle * _h, uint32 nGPIO, uint32* pnPD)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_2(((uint32 *)&((((GPIOMgrIFHandle *)_h)->pVtbl)->GPIOMgr_GetGPIOCurrentPD)-(uint32 *)(((GPIOMgrIFHandle *)_h)->pVtbl)), _h, nGPIO, pnPD);
   }
   return ((GPIOMgrIFHandle *)_h)->pVtbl->GPIOMgr_GetGPIOCurrentPD( _h, nGPIO, pnPD);
}
# 203 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/GPIOMgr/inc/DDIGPIOMgr.h"
static __inline DALResult
DalGPIOMgr_GetDirectConnectGPIO(DalDeviceHandle * _h, uint32 nGPIO, uint32* pDirConnID)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_2(((uint32 *)&((((GPIOMgrIFHandle *)_h)->pVtbl)->GPIOMgr_GetDirectConnectGPIO)-(uint32 *)(((GPIOMgrIFHandle *)_h)->pVtbl)), _h, nGPIO, pDirConnID);
   }
   return ((GPIOMgrIFHandle *)_h)->pVtbl->GPIOMgr_GetDirectConnectGPIO( _h, nGPIO, pDirConnID);
}
# 232 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/GPIOMgr/inc/DDIGPIOMgr.h"
static __inline DALResult
DalGPIOMgr_ReleaseDirectConnectGPIO(DalDeviceHandle * _h, uint32 nGPIO, uint32 nDirConnID)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_1(((uint32 *)&((((GPIOMgrIFHandle *)_h)->pVtbl)->GPIOMgr_ReleaseDirectConnectGPIO)-(uint32 *)(((GPIOMgrIFHandle *)_h)->pVtbl)), _h, nGPIO, nDirConnID);
   }
   return ((GPIOMgrIFHandle *)_h)->pVtbl->GPIOMgr_ReleaseDirectConnectGPIO( _h, nGPIO, nDirConnID);
}
# 260 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/GPIOMgr/inc/DDIGPIOMgr.h"
static __inline DALResult
DalGPIOMgr_GetDirectConnectInterruptID(DalDeviceHandle * _h, uint32 nGPIO, uint32* pInterruptID)
{
   if((((DALHandle)_h) & 0x00000001))
   {
      DalRemoteHandle *hRemote = (DalRemoteHandle *)(_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
      return hRemote->pVtbl->FCN_2(((uint32 *)&((((GPIOMgrIFHandle *)_h)->pVtbl)->GPIOMgr_GetDirectConnectInterruptID)-(uint32 *)(((GPIOMgrIFHandle *)_h)->pVtbl)), _h, nGPIO, pInterruptID);
   }
   return ((GPIOMgrIFHandle *)_h)->pVtbl->GPIOMgr_GetDirectConnectInterruptID( _h, nGPIO, pInterruptID);
}
# 286 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/GPIOMgr/inc/DDIGPIOMgr.h"
static __inline DALResult
DalGPIOMgr_Attach(DalDeviceHandle ** _h)
{
  return DAL_StringDeviceAttach( "GPIOManager", _h );
}
# 31 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/GPIOMgr/config/GPIOMgrConfigData.c" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/GPIOMgr/inc/GPIOMgr.h" 1
# 17 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/GPIOMgr/inc/GPIOMgr.h"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALFramework.h" 1
# 33 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALFramework.h"
typedef struct DALDrvCtxt DALDrvCtxt;
typedef struct DALDevCtxt DALDevCtxt;
typedef struct DALClientCtxt DALClientCtxt;
typedef struct DALDrvVtbl DALDrvVtbl;

struct DALDrvVtbl
{
   int (*DAL_DriverInit)(DALDrvCtxt *);
   int (*DAL_DriverDeInit)(DALDrvCtxt *);
};

struct DALDevCtxt
{
    uint32 dwRefs;
    DALDEVICEID DevId;
    uint32 dwDevCtxtRefIdx;
    DALDrvCtxt *pDALDrvCtxt;
    uint32 hProp[2];
    DalDeviceHandle *hDALSystem;
    DalDeviceHandle *hDALTimer;
    DalDeviceHandle *hDALInterrupt;
    uint32 * pSystemTbl;
    DALSYSWorkLoopHandle * pDefaultWorkLoop;
    const char *strDeviceName;
    uint32 reserve[16-6];
};

struct DALDrvCtxt
{
   DALDrvVtbl DALDrvVtbl;
   uint32 dwNumDev;
   uint32 dwSizeDevCtxt;
   uint32 bInit;
   uint32 dwRefs;
   DALDevCtxt DALDevCtxt[1];
};

struct DALClientCtxt
{
    uint32 dwRefs;
    uint32 dwAccessMode;
    void * pPortCtxt;
    DALDevCtxt *pDALDevCtxt;
};

typedef struct DALInheritSrcPram DALInheritSrcPram;
struct DALInheritSrcPram
{
   const byte * pBuf;
   int dwBufLen;
   uint32 dwSeqIdx;
};

typedef struct DALInheritDestPram DALInheritDestPram;
struct DALInheritDestPram
{
   byte * pBuf;
   uint32 dwBufLen;
   int *pdwObjLen;
   uint32 *dwSeqIdx;
};

typedef struct DALInterface DALInterface;
struct DALInterface
{
    uint32 dwDalHandleId;
    void *pVtbl;
    DALClientCtxt *pclientCtxt;
};
# 156 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALFramework.h"
DALResult
DALFW_AttachToStringDevice(const char *pszDeviceName, DALDrvCtxt *pdalDrvCtxt,
                           DALClientCtxt *pClientCtxt);

DALResult
DALFW_AttachToDevice(DALDEVICEID devId,DALDrvCtxt *pdalDrvCtxt,
                     DALClientCtxt *pdalClientCtxt);
# 171 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALFramework.h"
void
DALFW_MarkDeviceStatic(DALDevCtxt *pdalDevCtxt);

uint32
DALFW_AddRef(DALClientCtxt *pdalClientCtxt);

uint32
DALFW_Release(DALClientCtxt *pdalClientCtxt);


void
DALFW_SystemAttach(DALDevCtxt *pDevCtxt);

void
DALFW_SystemDetach(DALDevCtxt *pDevCtxt);

DALSYSWorkLoopHandle *
DALFW_GetWorkLoop(DALDevCtxt *pDevCtxt, uint32 dwWorkLoopPriority);






DALMemObject *
DALFW_CopyMemObjectToDDI(DALSYSMemHandle hMem, DALMemObject * pDestMemObject);

DALSYSMemHandle
DALFW_CreateMemObjectFromDDI(uint32 dwMarshalFlags, DALMemObject * pInMemObject);

DALSYSEventHandle
DALFW_CreateEventFromDDI(DALClientCtxt * pClientCtxt, uint32 dwMarshalFlags,
                DALSYSEventHandle inHandle, DALEventObject * pPrealloc);

DALSYSEventHandle
DALFW_CreatePayloadEventFromDDI(DALClientCtxt * pClientCtxt, uint32 dwMarshalFlags,
                DALSYSEventHandle inHandle, DALEventObject * pPrealloc);

uint32
DALFW_InitDDIMemDescListFromSys(uint32 dwFlags,
                             DALSysMemDescList *pInMemDescList,
                             DALDDIMemDescList * pDestDDIMemDescList);

DALSysMemDescList *
DALFW_InitSysMemDescListFromDDI(DALSYSMemHandle hMemHandle,
                             DALDDIMemDescList *pInDDIMemDescList,
                             DALSysMemDescList *pDestMemDescList);
void
DALFW_RegisterSystemNotificationEvent(DalDeviceHandle * h, DALSYSEventHandle hEvent);

void memory_barrier(void);
# 232 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALFramework.h"
DALSysMemDescBuf *
DALFW_MemDescBufPtr(DALSysMemDescList * pMemDescList, uint32 idx);
# 248 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALFramework.h"
DALResult
DALFW_MemDescInit(DALSYSMemHandle hMem, DALSysMemDescList * pMemDescList,
                  uint32 dwNumBufs);
# 266 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALFramework.h"
DALResult
DALFW_MemDescAddBuf(DALSysMemDescList * pMemDescList, uint32 bufIdx,
                    uint32 offset, uint32 size, uint32 userInfo);
# 284 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALFramework.h"
DALSYSMemHandle
DALFW_AllocPhysForMemDescBufs(DALSYSMemHandle hMem,
                              uint32 operation,
                              DALSysMemDescList *pInDescList,
                              DALSysMemDescList ** pOutDescList);






DALResult
DALFW_MemDescListCopyBufs(DALSysMemDescList * pDestDescList,
                          DALSysMemDescList * pSrcDescList);
# 322 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALFramework.h"
uint32 DALFW_LockedExchangeW(volatile uint32 *pTarget, uint32 value);






uint32 DALFW_LockedIncrementW(volatile uint32 *pTarget);






uint32 DALFW_LockedDecrementW(volatile uint32 *pTarget);







uint32 DALFW_LockedGetModifySetW(volatile uint32 *pTarget, uint32 AND_value, uint32 OR_value);
# 357 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALFramework.h"
uint32 DALFW_LockedCompareExchangeW(volatile uint32 *pTarget, uint32 comparand, uint32 value);
# 370 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALFramework.h"
void DALFW_LockSpinLock(volatile uint32 *spinLock, uint32 pid);
# 379 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALFramework.h"
void DALFW_UnLockSpinLock(volatile uint32 *spinLock);
# 392 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALFramework.h"
void DALFW_LockSpinLockExt(volatile uint32 *spinLock, uint32 pid);







void DALFW_UnLockSpinLockExt(volatile uint32 *spinLock);
# 409 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/dal/DALFramework.h"
DALResult DALFW_TryLockSpinLockExt(volatile uint32 *spinLock, uint32 pid);






typedef struct _DAFFW_MPLOCK
{
  volatile uint32 spin_lock;
  volatile uint32 owner;
  volatile uint32 waiting;
   volatile uint32 size;
}
DALFW_MPLOCK;






void DALFW_MPLock(DALFW_MPLOCK *pLock, uint32 pid, uint32 delayParam);






void DALFW_MPUnLock(DALFW_MPLOCK *pLock);






uint32 DALFW_TrySpinLockEx(DALFW_MPLOCK *pLock, uint32 pid);
# 18 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/GPIOMgr/inc/GPIOMgr.h" 2
# 29 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/GPIOMgr/inc/GPIOMgr.h"
typedef struct GPIOMgrDrvCtxt GPIOMgrDrvCtxt;
typedef struct GPIOMgrDevCtxt GPIOMgrDevCtxt;
typedef struct GPIOMgrClientCtxt GPIOMgrClientCtxt;





typedef struct
{
  uint8 nPD;
  uint8 nDirConnID;
  uint8 nTargetProc;
  uint8 nFlags;
}
GPIOMgr_StateType;

typedef struct
{
  uint16 nDirConnID;
  uint16 nInterruptID;
  uint16 nGPIO;
  uint16 nReservedPD;
}
GPIOMgrConfigMapType;

typedef struct
{
  uint32 nPD;
  uint32 nTargetProc;
}
GPIOMgrConfigPDType;




typedef struct GPIOMgrDALVtbl GPIOMgrDALVtbl;
struct GPIOMgrDALVtbl
{
  int (*GPIOMgr_DriverInit)(GPIOMgrDrvCtxt *);
  int (*GPIOMgr_DriverDeInit)(GPIOMgrDrvCtxt *);
};

struct GPIOMgrDevCtxt
{
  uint32 dwRefs;
  DALDEVICEID DevId;
  uint32 dwDevCtxtRefIdx;
  GPIOMgrDrvCtxt *pGPIOMgrDrvCtxt;
  uint32 hProp[2];
  uint32 Reserved[16];
  DALSYSSyncObj hSyncObj;
  DALSYSSyncHandle hMutex;
  uint32 nMaxGpios;
  GPIOMgr_StateType *pState;
  GPIOMgrConfigMapType *pDirConnMap;
  GPIOMgrConfigPDType *pPDMap;
  uint32 nDirConnMapSize;
  uint32 nPDMapSize;
};

struct GPIOMgrDrvCtxt
{
  GPIOMgrDALVtbl GPIOMgrDALVtbl;
  uint32 dwNumDev;
  uint32 dwSizeDevCtxt;
  uint32 bInit;
  uint32 dwRefs;
  GPIOMgrDevCtxt GPIOMgrDevCtxt[1];
};





struct GPIOMgrClientCtxt
{
  uint32 dwRefs;
  uint32 dwAccessMode;
  void *pPortCtxt;
  GPIOMgrDevCtxt *pGPIOMgrDevCtxt;
  GPIOMgrIFHandle GPIOMgrIFHandle;
};



DALResult GPIOMgr_DriverInit(GPIOMgrDrvCtxt *);
DALResult GPIOMgr_DriverDeInit(GPIOMgrDrvCtxt *);

DALResult GPIOMgr_LockGPIO(DalDeviceHandle *, uint32, uint32);
DALResult GPIOMgr_ReleaseGPIO(DalDeviceHandle *, uint32);
DALResult GPIOMgr_GetGPIOCurrentPD(DalDeviceHandle *, uint32, uint32*);
DALResult GPIOMgr_GetDirectConnectGPIO(DalDeviceHandle *, uint32, uint32*);
DALResult GPIOMgr_ReleaseDirectConnectGPIO(DalDeviceHandle *, uint32, uint32);
DALResult GPIOMgr_GetDirectConnectInterruptID(DalDeviceHandle *, uint32, uint32*);
# 32 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/GPIOMgr/config/GPIOMgrConfigData.c" 2

GPIOMgrConfigMapType GPIOMgrConfigMap[] =
{
  {0, 24, 0x10, 1},
  {1, 25, 0x10, 1},
  {2, 26, 0x7FFF, 0},
  {3, 27, 0x7FFF, 0},
  {4, 28, 0x7FFF, 1},
  {5, 29, 0x7FFF, 1},
  {0, 0, 0, 0},
};

GPIOMgrConfigPDType GPIOMgrConfigPD[] =
{
  {0, GPIOMGR_DEVICE_LPA_DSP},
  {1, GPIOMGR_DEVICE_LPA_DSP},
  {0, GPIOMGR_DEVICE_NONE}
};
