# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/pmic/config/msm8996/pm_lpass_settings.c"
# 1 "<built-in>" 1
# 1 "<built-in>" 3
# 140 "<built-in>" 3
# 1 "<command line>" 1
# 1 "<built-in>" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/pmic/config/msm8996/pm_lpass_settings.c" 2
# 38 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/pmic/config/msm8996/pm_lpass_settings.c"
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/rpm.h" 1
# 12 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/rpm.h"
# 1 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/stdbool.h" 1 3 4




# 1 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 1 3 4
# 297 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
typedef long _Int32t;
typedef unsigned long _Uint32t;
# 308 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
typedef int _Ptrdifft;
# 318 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
typedef unsigned int _Sizet;
# 676 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
# 1 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/stdarg.h" 1 3 4
# 12 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/stdarg.h" 3 4
typedef __builtin_va_list va_list;
# 677 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 2 3 4
# 786 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
typedef long long _Longlong;
typedef unsigned long long _ULonglong;
# 850 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
typedef int _Wchart;
typedef int _Wintt;
# 880 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
typedef va_list _Va_list;
# 902 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
void _Atexit(void (*)(void));
# 915 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
typedef char _Sysch_t;
# 935 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
void _Locksyslock(int);
void _Unlocksyslock(int);
# 1066 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/yvals.h" 3 4
typedef unsigned short __attribute__((__may_alias__)) alias_short;
static alias_short *strict_aliasing_workaround(unsigned short *ptr) __attribute__((always_inline,unused));

static alias_short *strict_aliasing_workaround(unsigned short *ptr)
{
  alias_short *aliasptr = (alias_short *)ptr;
  return aliasptr;
}
# 6 "/afs/qualcomm.com/cm/gv2.6/sysname/pkg.@sys/qct/software/hexagon/releases/tools/7.2.11/Tools/bin/../target/hexagon/include/stdbool.h" 2 3 4
# 13 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/api/power/rpm.h" 2

typedef enum
{
    RPM_ACTIVE_SET = 0,
    RPM_SLEEP_SET = 1,
    RPM_NEXT_ACTIVE_SET = 2,


    RPM_NUM_SETS,
    RPM_SEMI_ACTIVE_SET = RPM_NUM_SETS
} rpm_set_type;

typedef enum
{
    RPM_TEST_REQ = 0x74736574,

    RPM_CLOCK_0_REQ = 0x306b6c63,
    RPM_CLOCK_1_REQ = 0x316b6c63,
    RPM_CLOCK_2_REQ = 0x326b6c63,

    RPM_BUS_SLAVE_REQ = 0x766c7362,
    RPM_BUS_MASTER_REQ = 0x73616d62,
    RPM_BUS_SPDM_CLK_REQ = 0x63707362,
    RPM_BUS_MASTER_LATENCY_REQ = 0x74616c62,

    RPM_SMPS_A_REQ = 0x61706D73,
    RPM_LDO_A_REQ = 0x616F646C,
    RPM_NCP_A_REQ = 0x6170636E,
    RPM_VS_A_REQ = 0x617376,
    RPM_CLK_BUFFER_A_REQ = 0x616B6C63,

    RPM_SMPS_B_REQ = 0x62706D73,
    RPM_LDO_B_REQ = 0x626F646C,
    RPM_NCP_B_REQ = 0x6270636E,
    RPM_VS_B_REQ = 0x627376,
    RPM_CLK_BUFFER_B_REQ = 0x626B6C63,

    RPM_OCMEM_POWER_REQ = 0x706d636f,

    RPM_RBCPR_REQ = 0x727063,

    RPM_GPIO_TOGGLE_REQ = 0x6F697067,
} rpm_resource_type;

typedef enum
{
    RPM_REQUEST_SERVICE = 0x00716572,
    RPM_SYSTEMDB_SERVICE = 0x00626473,
} rpm_service_type;

_Bool rpm_is_up(void);
# 39 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/pmic/config/msm8996/pm_lpass_settings.c" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/pmic/npa/inc/pm_lpass_dal.h" 1
# 35 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/pmic/npa/inc/pm_lpass_dal.h"
typedef enum LUTInfoType
{
    VoltLevel,
    VoltRange,
    MX,
    CX,
    NUM_OF_RSRC,
    DEP,
    SAW,
    PAM,
    REMOTE_RSRC_LDO,
    REMOTE_RSRC_VS,
    REMOTE_RSRC_SMPS,
    RSRC_PUB,
}LUTInfoType;

typedef struct PwrResourceInfoType
{
    rpm_resource_type ResourceType;
    unsigned ResourceIndex;
    void* data1;
    void* data2;
    void* data3;
}PwrResourceInfoType;




void* pmiC_DalSettingsRegisters_GetModuleLUTInfo(LUTInfoType infoType, void* helper);




void pmiC_DalSettingsRegisters_Init(void);
# 40 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/pmic/config/msm8996/pm_lpass_settings.c" 2
# 52 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/pmic/config/msm8996/pm_lpass_settings.c"
static unsigned num_of_cx_corners = 6;
static unsigned pm_cx_corners[6] =
{ 500000, 725000, 812500, 900000, 987500, 1050000};
static char PM_MODEM_CX_VREG[50] = "/pmic/device/smps/A/smps1/vec";

static unsigned num_of_mx_corners = 6;
static unsigned pm_mx_corners[6] =
{675000, 950000, 950000, 950000, 1050000, 1050000};
static char PM_MODEM_MX_VREG[50] = "/pmic/device/smps/A/smps2/vec";

PwrResourceInfoType MX_Info[1] =
{
    {RPM_SMPS_A_REQ, 2, &num_of_mx_corners, pm_mx_corners, PM_MODEM_MX_VREG }
};
PwrResourceInfoType CX_Info[1] =
{
    {RPM_SMPS_A_REQ, 1, &num_of_cx_corners, pm_cx_corners, PM_MODEM_CX_VREG }
};
