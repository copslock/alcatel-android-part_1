# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/pmic/config/msm8996/pmic_config.xml"
# 1 "<built-in>" 1
# 1 "<built-in>" 3
# 140 "<built-in>" 3
# 1 "<command line>" 1
# 1 "<built-in>" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/pmic/config/msm8996/pmic_config.xml" 2
<driver name="NULL">
  <device id="/lpass/pmic">
    <props name="PAM_Node_Rsr" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
      pm_npa_lpass_pam_node_rsrcs
    </props>
    <props name="PAM_Num" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
      num_of_pm_lpass_nodes
    </props>
    <props name="MX_Info" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
      MX_Info
    </props>
    <props name="CX_Info" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
      CX_Info
    </props>
    <props name="RemoteLDORsrc" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
      pmic_npa_remote_ldo
    </props>
    <props name="RemoteVSRsrc" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
      pmic_npa_remote_vs
    </props>
    <props name="RemoteSMPSRsrc" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
      pmic_npa_remote_smps
    </props>
  </device>
</driver>
