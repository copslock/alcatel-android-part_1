# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/chipinfo/config/DalChipInfo.xml"
# 1 "<built-in>" 1
# 1 "<built-in>" 3
# 140 "<built-in>" 3
# 1 "<command line>" 1
# 1 "<built-in>" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/chipinfo/config/DalChipInfo.xml" 2
<driver name="ChipInfo">
    <device id=DALDEVICEID_CHIPINFO>
# 36 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/chipinfo/config/DalChipInfo.xml"
     <props name="ChipIdOverride" type=DALPROP_ATTR_TYPE_UINT32>
       246
     </props>
     <props name="HWREVNUM_PHYS_ADDR" type=DALPROP_ATTR_TYPE_UINT32>
       0x01010000
     </props>
     <props name="HWREVNUM_OFFSET" type=DALPROP_ATTR_TYPE_UINT32>
       0x00141010
     </props>
     <props name="SOC_HW_VERSION_PHYS_ADDR" type=DALPROP_ATTR_TYPE_UINT32>
       0x007A0000
     </props>
     <props name="SOC_HW_VERSION_OFFSET" type=DALPROP_ATTR_TYPE_UINT32>
       0x00008000
     </props>
# 68 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/systemdrivers/chipinfo/config/DalChipInfo.xml"
     <props name="PARTNUM_BMSK" type=DALPROP_ATTR_TYPE_UINT32>
       0xffff000
     </props>
     <props name="PARTNUM_SHFT" type=DALPROP_ATTR_TYPE_UINT32>
       0xc
     </props>
     <props name="VERSION_ID_BMSK" type=DALPROP_ATTR_TYPE_UINT32>
       0xf0000000
     </props>
     <props name="VERSION_ID_SHFT" type=DALPROP_ATTR_TYPE_UINT32>
       0x1c
     </props>
     <props name="QUALCOMM_MFG_ID_BMSK" type=DALPROP_ATTR_TYPE_UINT32>
       0xffe
     </props>
     <props name="QUALCOMM_MFG_ID_SHFT" type=DALPROP_ATTR_TYPE_UINT32>
       0x1
     </props>
     <props name="MAJOR_VERSION_BMSK" type=DALPROP_ATTR_TYPE_UINT32>
       0x0000FF00
     </props>
     <props name="MAJOR_VERSION_SHFT" type=DALPROP_ATTR_TYPE_UINT32>
       0x8
     </props>
     <props name="MINOR_VERSION_BMSK" type=DALPROP_ATTR_TYPE_UINT32>
       0x000000FF
     </props>
     <props name="MINOR_VERSION_SHFT" type=DALPROP_ATTR_TYPE_UINT32>
       0x0
     </props>
     <props name="IsRemotable" type=DALPROP_ATTR_TYPE_UINT32>
       0x1
     </props>
  </device>
</driver>
