/*
===========================================================================
*/
/**
  @file VCSLDOs.c 
  
  LDO-related functions for the VCS driver.
*/
/*  
  ====================================================================

  Copyright (c) 2014 QUALCOMM Technologies Incorporated. All Rights Reserved.  
  QUALCOMM Proprietary and Confidential. 

  ==================================================================== 
  $Header: //components/rel/core.adsp/2.7/systemdrivers/vcs/src/VCSCPU.c#3 $
  $DateTime: 2015/02/27 11:19:18 $
  $Author: pwbldsvc $
 
  when       who     what, where, why
  --------   ---     -------------------------------------------------
  01/22/14   lil     Created.
 
  ====================================================================
*/ 


/*=========================================================================
      Include Files
==========================================================================*/


#include <DALSys.h>
#include "VCSDriver.h"
#include "HALldo.h"
#include "DALDeviceId.h"
#include <npa_resource.h>
#include "VCSSWEVT.h"

/*=========================================================================
      Externs
==========================================================================*/


/*=========================================================================
      Prototypes
==========================================================================*/


/*=========================================================================
      Functions
==========================================================================*/


/* =========================================================================
**  Function : VCS_InitLDOVoltageTable
** =========================================================================*/
/**
  Initializes the voltage table for this HW version.

  @param pLDO [in]         -- Pointer rail node.
  @param pCornerConfig [in] -- Pointer rail corner config for this HW version.
  @return
  DAL_ERROR if a voltage table not initialized, other DAL_SUCCESS.

  @dependencies
  None.
*/

static DALResult VCS_InitLDOVoltageTable
(
  VCSLDONodeType   *pLDO,
  VCSLDOConfigType *pLDOConfig

)
{
  DALResult               eResult;
  uint32                  i, nSize;
  VCSCornerVoltageType   *pBSPVoltage, *pLDOVoltage;
  VCSLDOVoltageTableType *pVoltageTable;
//  VCSDrvCtxt             *pDrvCtxt;

  /*-----------------------------------------------------------------------*/
  /* Sanity.                                                               */
  /*-----------------------------------------------------------------------*/

  if (pLDO == NULL || pLDOConfig == NULL)
  {
    return DAL_ERROR_INVALID_PARAMETER;
  }


  /*-----------------------------------------------------------------------*/
  /* Validate the BSP config.                                              */
  /*-----------------------------------------------------------------------*/

  if (pLDOConfig->nNumCornerVoltages == 0 ||
      pLDOConfig->nNumCornerVoltages >= VCS_CORNER_NUM_OF_CORNERS ||
      pLDOConfig->pCornerVoltage == NULL)
  {
    return DAL_ERROR_INTERNAL;
  }

//  pDrvCtxt = VCS_GetDrvCtxt();

  /*-----------------------------------------------------------------------*/
  /* Allocate storage for LDO voltage table.                               */
  /*-----------------------------------------------------------------------*/

  nSize = sizeof (*pLDO->pVoltageTable);

  eResult = DALSYS_Malloc(nSize, (void **)&pLDO->pVoltageTable);
  if (eResult != DAL_SUCCESS)
  {
    DALSYS_LogEvent(
      0,
      DALSYS_LOGEVENT_FATAL_ERROR,
      "DALLOG Device VCS: Unable to allocate %lu bytes for LDO[%s].",
      nSize,
      pLDO->pBSPConfig->szName);

    return eResult;
  }

  memset((void *)pLDO->pVoltageTable, 0x0, nSize);

  /*-----------------------------------------------------------------------*/
  /* Allocate storage for corner voltages.                                 */
  /*-----------------------------------------------------------------------*/

  pVoltageTable = pLDO->pVoltageTable;
  pVoltageTable->nNumCornerVoltages = pLDOConfig->nNumCornerVoltages;

  nSize =
    pVoltageTable->nNumCornerVoltages * sizeof (*pVoltageTable->pCornerVoltage);

  eResult = DALSYS_Malloc(nSize, (void **)&pVoltageTable->pCornerVoltage);
  if (eResult != DAL_SUCCESS)
  {
    DALSYS_LogEvent(
      0,
      DALSYS_LOGEVENT_FATAL_ERROR,
      "DALLOG Device VCS: Unable to allocate %lu bytes for LDO[%s].",
      nSize,
      pLDO->pBSPConfig->szName);

    return eResult;
  }

  /*-----------------------------------------------------------------------*/
  /* Populate corner voltage table.                                        */
  /*-----------------------------------------------------------------------*/

  for (i = 0; i < pVoltageTable->nNumCornerVoltages; i++)
  {
    // Copy corner voltage table from BSP.
    pBSPVoltage = &pLDOConfig->pCornerVoltage[i];
    pLDOVoltage = &pVoltageTable->pCornerVoltage[i];

    if (pBSPVoltage->eCorner >= VCS_CORNER_NUM_OF_CORNERS)
    {
      DALSYS_LogEvent(
          0,
          DALSYS_LOGEVENT_FATAL_ERROR,
          "DALLOG Device VCS: Invalid corner for LDO[%s] specified in BSP",
          pLDO->pBSPConfig->szName);

      return DAL_ERROR_INTERNAL;
    }

    pLDOVoltage->eCorner    = pBSPVoltage->eCorner;
    pLDOVoltage->nVoltageUV = pBSPVoltage->nVoltageUV;

    /*
     * Hook up corner mapping.
     */
    pVoltageTable->apCornerMap[pLDOVoltage->eCorner] = pLDOVoltage;
  }

  return DAL_SUCCESS;

} /* END VCS_InitLDOVoltageTable */


/* =========================================================================
**  Function : VCS_InitLDO
** =========================================================================*/
/**
  Initializes the LDO structures for the specified CPU.

  @param pCPU [in] -- Pointer to CPU node.
  @return
  DAL_ERROR if LDO couldn't be initialized, other DAL_SUCCESS.

  @dependencies
  None.
*/
static DALResult VCS_InitLDO
(
  VCSCPUNodeType *pCPU
)
{
  DALResult               eResult;
  uint32                  nSize;
  VCSLDONodeType         *pLDO;
  VCSLDOConfigType       *pLDOConfig;

  /*-----------------------------------------------------------------------*/
  /* Sanity.                                                               */
  /*-----------------------------------------------------------------------*/

  if (pCPU == NULL ||
      pCPU->pBSPConfig == NULL)
  {
    return DAL_ERROR_INVALID_PARAMETER;
  }

  pLDOConfig = pCPU->pBSPConfig->pLDOConfig;

  /*-----------------------------------------------------------------------*/
  /* Nohting to do if no LDO present.                                      */
  /*-----------------------------------------------------------------------*/

  if (pLDOConfig == NULL)
  {
    return DAL_SUCCESS;
  }

  /*-----------------------------------------------------------------------*/
  /* Allocate memory for LDO node.                                         */
  /*-----------------------------------------------------------------------*/

  nSize = sizeof (*pCPU->pLDO);

  eResult = DALSYS_Malloc(nSize, (void **)&pCPU->pLDO);
  if (eResult != DAL_SUCCESS)
  {
    DALSYS_LogEvent(
      0,
      DALSYS_LOGEVENT_FATAL_ERROR,
      "DALLOG Device VCS: Unable to allocate %lu bytes for LDO[%s]",
      nSize,
      pLDOConfig->szName);

    return eResult;
  }

  memset((void *)pCPU->pLDO, 0x0, nSize);

  /*-----------------------------------------------------------------------*/
  /* Store LDO properties.                                                 */
  /*-----------------------------------------------------------------------*/

  pLDO = pCPU->pLDO;
  pLDO->eLDO             = pLDOConfig->eLDO;
  pLDO->eCorner          = VCS_CORNER_OFF;
  pLDO->nVoltageUV       = 0;
  pLDO->nNumRestrictions = 0;
  pLDO->pBSPConfig       = pLDOConfig;

  /*-----------------------------------------------------------------------*/
  /* Explicitly disable per the BSP configuration.                         */
  /*-----------------------------------------------------------------------*/

  if (!pLDOConfig->bEnable)
  {
    pLDO->nDisable |= VCS_FLAG_DISABLED_BY_BSP;
  }

  /*-----------------------------------------------------------------------*/
  /* Initialize the LDO voltage table.                                     */
  /*-----------------------------------------------------------------------*/

  eResult = VCS_InitLDOVoltageTable(pLDO, pLDOConfig);
  if (eResult != DAL_SUCCESS)
  {
    DALSYS_LogEvent(
      0,
      DALSYS_LOGEVENT_FATAL_ERROR,
      "DALLOG Device VCS: Failed to initialize voltage table for LDO[%s]",
      pLDO->pBSPConfig->szName);

    return DAL_ERROR_INTERNAL;
  }

  return DAL_SUCCESS;

} /* END VCS_InitLDO */


/* =========================================================================
**  Function : VCS_InitCPUs
** =========================================================================*/
/*
  See VCSDriver.h
*/

DALResult VCS_InitCPUs
(
  VCSDrvCtxt       *pDrvCtxt,
  VCSBSPConfigType *pBSPConfig
)
{
  DALResult         eResult;
  uint32            i, j, k, nSize;
  VCSCPUNodeType   *pCPU;
  VCSRailNodeType  *pRail;

  /*-----------------------------------------------------------------------*/
  /* Nothing to do if no CPUs present.                                     */
  /*-----------------------------------------------------------------------*/

  if (pBSPConfig->nNumCPUConfigs == 0)
  {
    return DAL_SUCCESS;
  }

  pDrvCtxt->nNumCPUs = pBSPConfig->nNumCPUConfigs;

  /*-----------------------------------------------------------------------*/
  /* Allocate memory for the CPU nodes.                                    */
  /*-----------------------------------------------------------------------*/

  nSize = pDrvCtxt->nNumCPUs * sizeof (*pDrvCtxt->aCPUs);

  eResult = DALSYS_Malloc(nSize, (void **)&pDrvCtxt->aCPUs);
  if (eResult != DAL_SUCCESS)
  {
    DALSYS_LogEvent(
      0,
      DALSYS_LOGEVENT_FATAL_ERROR,
      "DALLOG Device VCS: Unable to allocate %lu bytes for %lu CPUs.",
      nSize,
      pDrvCtxt->nNumCPUs);

    return eResult;
  }

  memset((void *)pDrvCtxt->aCPUs, 0x0, nSize);

  /*-----------------------------------------------------------------------*/
  /* Initialize the CPU nodes.                                             */
  /*-----------------------------------------------------------------------*/

  for (i = 0; i < pDrvCtxt->nNumCPUs; i++)
  {
    pCPU = &pDrvCtxt->aCPUs[i];

    /*
     * Link the BSP data.`
     */
    pCPU->pBSPConfig = &pBSPConfig->pCPUConfig[i];

    /*
     * Save CPU enum.
     */
    pCPU->eCPU = pCPU->pBSPConfig->eCPU;

    /*
     * Create entry in CPU enum to node map.
     */
    pDrvCtxt->apCPUMap[pCPU->eCPU] = pCPU;

    /*
     * Increment the count for the rail powering this CPU.
     */
    pRail = pDrvCtxt->apRailMap[pCPU->pBSPConfig->eRail];
    if (pRail == NULL)
    {
      return DAL_ERROR_INTERNAL;
    }

    pRail->nNumCPUs++;

    /*
     * Init the LDO if present for this CPU
     */
    eResult = VCS_InitLDO(pCPU);
    if (eResult != DAL_SUCCESS)
    {
      DALSYS_LogEvent(
        0,
        DALSYS_LOGEVENT_FATAL_ERROR,
        "DALLOG Device VCS: Unable to initialize LDO for CPU[%lu].",
        pCPU->eCPU);

      return eResult;
    }
  }

  /*-----------------------------------------------------------------------*/
  /* Connect the Rails to each of the CPUs which it powers.                */
  /*-----------------------------------------------------------------------*/

  for (i = 0; i < pDrvCtxt->nNumRails; i++)
  {
    pRail = &pDrvCtxt->aRails[i];

    /*
     * Skip this rail if there are no CPU's to map.
     */
    if (pRail->nNumCPUs == 0)
    {
      continue;
    }

    /*
     * Allocate storage for pointers to CPU nodes.
     */
    nSize = pRail->nNumCPUs * sizeof (*pRail->apCPU);

    eResult = DALSYS_Malloc(nSize, (void **)&pRail->apCPU);
    if (eResult != DAL_SUCCESS)
    {
      DALSYS_LogEvent(
        0,
        DALSYS_LOGEVENT_FATAL_ERROR,
        "DALLOG Device VCS: Unable to allocate %lu bytes for %lu CPUs.",
        nSize,
        pDrvCtxt->nNumCPUs);

      return eResult;
    }

    memset((void *)pRail->apCPU, 0x0, nSize);

    /*
     * Iterate through CPU list and store pointers to those on this rail.
     */
    for (j = 0, k = 0; j < pRail->nNumCPUs; j++, k++)
    {
      for ( ; k < pDrvCtxt->nNumCPUs; k++)
      {
        pCPU = &pDrvCtxt->aCPUs[j];
        if (pCPU->pBSPConfig->eRail == pRail->eRail)
        {
          pRail->apCPU[j] = pCPU;
          break;
        }
      }
    }
  }

  return DAL_SUCCESS;

} /* END VCS_InitCPUs */


/* =========================================================================
**  Function : VCS_SetLDOCorner
** =========================================================================*/
/*
  See VCSDriver.h
*/
  
DALResult VCS_SetLDOCorner
(
  VCSLDONodeType *pLDO,
  VCSCornerType   eLDOCorner,
  VCSCornerType   eRailCorner
)
{
  VCSCornerVoltageType   *pCornerVoltage;
  VCSNPALDOEventDataType  LDOEventData;

  /*-----------------------------------------------------------------------*/
  /* Validate arguments.                                                   */
  /*-----------------------------------------------------------------------*/

  if (pLDO == NULL ||
      eLDOCorner >= VCS_CORNER_NUM_OF_CORNERS ||
      eRailCorner >= VCS_CORNER_NUM_OF_CORNERS)
  {
    return DAL_ERROR_INVALID_PARAMETER;
  }

  /*-----------------------------------------------------------------------*/
  /* Short circuit if no change in LDO corner.                             */
  /*-----------------------------------------------------------------------*/

  if (pLDO->eCorner == eLDOCorner)
  {
    return DAL_SUCCESS;
  }

  /*-----------------------------------------------------------------------*/
  /* Sanity.                                                               */
  /*-----------------------------------------------------------------------*/

  if (pLDO->pVoltageTable == NULL)
  {
    return DAL_ERROR_INTERNAL;
  }

  /*-----------------------------------------------------------------------*/
  /* Switch to LDO if the corner is supported and enough headroom exists.  */
  /*-----------------------------------------------------------------------*/

  pCornerVoltage = pLDO->pVoltageTable->apCornerMap[eLDOCorner];
  if (pCornerVoltage != NULL &&
      eLDOCorner + pLDO->pBSPConfig->nHeadroom <= eRailCorner)
  {
    /*
     * Fill out event data for LDO state change.
     */
    LDOEventData.PreChange.eCorner  = pLDO->eCorner;
    LDOEventData.PreChange.nVoltageUV = pLDO->nVoltageUV;
    LDOEventData.PostChange.eCorner = eLDOCorner;
    LDOEventData.PostChange.nVoltageUV = pCornerVoltage->nVoltageUV;

    /*
     * Notify NPA clients of pre-switch event.
     */
    npa_dispatch_custom_events(
      pLDO->resource.handle,
      (npa_event_type)VCS_NPA_LDO_EVENT_PRE_CHANGE,
      &LDOEventData);

    /*
     * Update the LDO voltage.
     */
    HAL_ldo_SetLDOVoltage(pLDO->eLDO, pCornerVoltage->nVoltageUV);
    
    /*
     * Update the LDO Voltage SW Event
     */
    VCS_SWEvent(VCS_EVENT_LDO_VOLTAGE_SET, 3,
      pLDO->eLDO, eLDOCorner, pCornerVoltage->nVoltageUV);

    /*
     * Enable the LDO if it's not already on.
     */
    if (pLDO->eCorner == VCS_CORNER_OFF)
    {
      HAL_ldo_EnableLDO(pLDO->eLDO);

      VCS_SWEvent(VCS_EVENT_LDO_STATE_SET, 2,
        pLDO->eLDO, 1);
    }

    /*
     * Update the context.
     */
    pLDO->nVoltageUV = pCornerVoltage->nVoltageUV;
    pLDO->eCorner    = eLDOCorner;

    /*
     * Notify NPA clients of post-switch event.
     */
    npa_dispatch_custom_events(
      pLDO->resource.handle,
      (npa_event_type)VCS_NPA_LDO_EVENT_POST_CHANGE,
      &LDOEventData);
  }

  /*-----------------------------------------------------------------------*/
  /* Otherwise disable the LDO if it's not already off.                    */
  /*-----------------------------------------------------------------------*/

  else if (pLDO->eCorner != VCS_CORNER_OFF)
  {
    /*
     * Fill out event data for LDO state change.
     */
    LDOEventData.PreChange.eCorner  = pLDO->eCorner;
    LDOEventData.PreChange.nVoltageUV = pLDO->nVoltageUV;
    LDOEventData.PostChange.eCorner = VCS_CORNER_OFF;
    LDOEventData.PostChange.nVoltageUV = 0;

    /*
     * Notify NPA clients of pre-switch event.
     */
    npa_dispatch_custom_events(
      pLDO->resource.handle,
      (npa_event_type)VCS_NPA_LDO_EVENT_PRE_CHANGE,
      &LDOEventData);

    HAL_ldo_DisableLDO(pLDO->eLDO);

    /*
     * Update the LDO Disable SW Event
     */
    VCS_SWEvent(VCS_EVENT_LDO_STATE_SET, 2,
      pLDO->eLDO, 0);

    /*
     * Update the context.
     */
    pLDO->eCorner = VCS_CORNER_OFF;
    pLDO->nVoltageUV = 0;

    /*
     * Notify NPA clients of post-switch event.
     */
    npa_dispatch_custom_events(
      pLDO->resource.handle,
      (npa_event_type)VCS_NPA_LDO_EVENT_POST_CHANGE,
      &LDOEventData);
}

  return DAL_SUCCESS;

} /* END VCS_SetLDOCorner */


/* =========================================================================
**  Function : VCS_SetCPUCorner
** =========================================================================*/
/*
  See DDIVCS.h
*/

DALResult VCS_SetCPUCorner
(
  VCSDrvCtxt   *pDrvCtxt,
  ClockCPUType  eCPU,
  VCSCornerType eCornerRequest
)
{
  DALResult        eResult;
  VCSRailNodeType *pRail;
  VCSCPUNodeType  *pCPU;
  VCSCornerType    eCornerStart;


  /*-----------------------------------------------------------------------*/
  /* Sanity.                                                               */
  /*-----------------------------------------------------------------------*/

  if (eCPU >= CLOCK_CPU_NUM_OF_CPUS ||
      eCornerRequest >= VCS_CORNER_NUM_OF_CORNERS)
  {
    return DAL_ERROR_INVALID_PARAMETER;
  }

  /*-----------------------------------------------------------------------*/
  /* Verify the CPU is supported.                                          */
  /*-----------------------------------------------------------------------*/

  pCPU = pDrvCtxt->apCPUMap[eCPU];
  if (pCPU == NULL)
  {
    return DAL_ERROR_NOT_SUPPORTED;
  }

  /*-----------------------------------------------------------------------*/
  /* Short circuit if the new request is same as current.                  */
  /*-----------------------------------------------------------------------*/

  if (pCPU->eCorner == eCornerRequest)
  {
    return DAL_SUCCESS;
  }

  /*-----------------------------------------------------------------------*/
  /* Get handle to rail node.                                              */
  /*-----------------------------------------------------------------------*/

  pRail = pDrvCtxt->apRailMap[pCPU->pBSPConfig->eRail];
  if (pRail == NULL)
  {
    return DAL_ERROR_INTERNAL;
  }

  /*-----------------------------------------------------------------------*/
  /* Protect operation on rail with a critical section.                    */
  /*-----------------------------------------------------------------------*/

  npa_resource_lock(pRail->resource.handle);

  /*-----------------------------------------------------------------------*/
  /* Keep track of the rail corner pre-CPU vote.                           */
  /*-----------------------------------------------------------------------*/

  eCornerStart = pRail->eCorner;

  /*-----------------------------------------------------------------------*/
  /* Make a corner request with the provided NPA handle.                   */
  /*-----------------------------------------------------------------------*/

  npa_issue_scalar_request(pCPU->NPAHandle, eCornerRequest);

  /*-----------------------------------------------------------------------*/
  /* If the rail corner didn't change after the CPU vote then it means     */
  /* that the CPU is <= the aggregated rail vote.  In this case the driver */
  /* function for the rail would not have been invoked and we must take    */
  /* care to update the LDO state directly.                                */
  /*-----------------------------------------------------------------------*/

  if (pRail->eCorner == eCornerStart &&
      pCPU->pLDO && !pCPU->pLDO->nDisable)
  {
    eResult = VCS_SetLDOCorner(pCPU->pLDO, eCornerRequest, pRail->eCorner);
    if (eResult != DAL_SUCCESS)
    {
      npa_resource_unlock(pRail->resource.handle);

      DALSYS_LogEvent(
        0,
        DALSYS_LOGEVENT_FATAL_ERROR,
        "DALLOG Device VCS: Failed to update LDO corner.");

      return eResult;
    }
  }

  /*-----------------------------------------------------------------------*/
  /* Update context.                                                       */
  /*-----------------------------------------------------------------------*/

  pCPU->eCorner = eCornerRequest;

  /*-----------------------------------------------------------------------*/
  /* Release lock.                                                         */
  /*-----------------------------------------------------------------------*/

  npa_resource_unlock(pRail->resource.handle);

  return DAL_SUCCESS;

} /* END VCS_SetCPUCorner */


/* =========================================================================
**  Function : VCS_AddLDORestriction
** =========================================================================*/
/*
  See DDIVCS.h
*/

DALResult VCS_AddLDORestriction
(
  VCSDrvCtxt   *pDrvCtxt,
  ClockCPUType  eCPU
)
{
  VCSCPUNodeType  *pCPU;
  VCSRailNodeType *pRail;
  VCSLDONodeType  *pLDO;

  /*-----------------------------------------------------------------------*/
  /* Sanity.                                                               */
  /*-----------------------------------------------------------------------*/

  pCPU = pDrvCtxt->apCPUMap[eCPU];
  if (pCPU == NULL || pCPU->pLDO == NULL)
  {
    return DAL_ERROR_INVALID_PARAMETER;
  }

  pLDO = pCPU->pLDO;

  /*-----------------------------------------------------------------------*/
  /* Get pointer to the rail node.                                         */
  /*-----------------------------------------------------------------------*/

  pRail = pDrvCtxt->apRailMap[pCPU->pBSPConfig->eRail];
  if (pRail == NULL)
  {
    return DAL_ERROR_INTERNAL;
  }

  /*-----------------------------------------------------------------------*/
  /* Protect operation on rail/LDO with a critical section.                */
  /*-----------------------------------------------------------------------*/

  npa_resource_lock(pRail->resource.handle);

  /*-----------------------------------------------------------------------*/
  /* Increment the number of restrictions on the LDO.                      */
  /*-----------------------------------------------------------------------*/

  pLDO->nNumRestrictions += 1;

  /*-----------------------------------------------------------------------*/
  /* Log the SW Event LDO restrictions                                     */
  /*-----------------------------------------------------------------------*/

  VCS_SWEvent(VCS_EVENT_LDO_RESTRICTION, 2,
    pLDO->eLDO, pLDO->nNumRestrictions);

  /*-----------------------------------------------------------------------*/
  /* Disable the LDO if not already done.                                  */
  /*-----------------------------------------------------------------------*/

  if (!(pLDO->nDisable & VCS_FLAG_DISABLED_BY_SWCLIENT))
  {
    /*
     * Disable the LDO.
     */
    VCS_SetLDOCorner(pLDO, VCS_CORNER_OFF, pRail->eCorner);

    /*
     * Update the disable flag.
     */
    pLDO->nDisable |= VCS_FLAG_DISABLED_BY_SWCLIENT;
  }

  /*-----------------------------------------------------------------------*/
  /* Release lock.                                                         */
  /*-----------------------------------------------------------------------*/

  npa_resource_unlock(pRail->resource.handle);

  return DAL_SUCCESS;

} /* END VCS_AddLDORestriction */


/* =========================================================================
**  Function : VCS_RemoveLDORestriction
** =========================================================================*/
/*
  See DDIVCS.h
*/

DALResult VCS_RemoveLDORestriction
(
  VCSDrvCtxt   *pDrvCtxt,
  ClockCPUType  eCPU
)
{
  VCSCPUNodeType  *pCPU;
  VCSRailNodeType *pRail;
  VCSLDONodeType  *pLDO;
  DALResult        eResult = DAL_SUCCESS;

  /*-----------------------------------------------------------------------*/
  /* Sanity.                                                               */
  /*-----------------------------------------------------------------------*/

  pCPU = pDrvCtxt->apCPUMap[eCPU];
  if (pCPU == NULL || pCPU->pLDO == NULL)
  {
    return DAL_ERROR_INVALID_PARAMETER;
  }

  pLDO = pCPU->pLDO;

  /*-----------------------------------------------------------------------*/
  /* Get pointer to the rail node.                                         */
  /*-----------------------------------------------------------------------*/

  pRail = pDrvCtxt->apRailMap[pCPU->pBSPConfig->eRail];
  if (pRail == NULL)
  {
    return DAL_ERROR_INTERNAL;
  }

  /*-----------------------------------------------------------------------*/
  /* Protect operation on rail/ldo with a critical section.                */
  /*-----------------------------------------------------------------------*/

  npa_resource_lock(pRail->resource.handle);

  /*-----------------------------------------------------------------------*/
  /* Decrement the number of restrictions on the LDO.                      */
  /*-----------------------------------------------------------------------*/

  if (pLDO->nNumRestrictions > 0)
  {
    pLDO->nNumRestrictions -= 1;
  }

  /*-----------------------------------------------------------------------*/
  /* Log the SW Event LDO restrictions                                     */
  /*-----------------------------------------------------------------------*/

  VCS_SWEvent(VCS_EVENT_LDO_RESTRICTION, 2,
    pLDO->eLDO, pLDO->nNumRestrictions);

  /*-----------------------------------------------------------------------*/
  /* If the number of restrictions on the LDO has gone down to zero, clear */
  /* the relevant bit in the disable flag.                                 */
  /*-----------------------------------------------------------------------*/

  if (pLDO->nDisable == 0)
  {
    pLDO->nDisable &= ~VCS_FLAG_DISABLED_BY_SWCLIENT;
  }

  /*-----------------------------------------------------------------------*/
  /* Re-vote the CPU's vote for the LDO if there are no disable flags set. */
  /*-----------------------------------------------------------------------*/

  if (!pLDO->nDisable)
  {
    eResult = VCS_SetLDOCorner(pLDO, pCPU->eCorner, pRail->eCorner);
    if (eResult != DAL_SUCCESS)
    {
      DALSYS_LogEvent(
          0,
          DALSYS_LOGEVENT_FATAL_ERROR,
          "DALLOG Device VCS: Failed to update LDO corner.");
    }
  }

  /*-----------------------------------------------------------------------*/
  /* Release lock.                                                         */
  /*-----------------------------------------------------------------------*/

  npa_resource_unlock(pRail->resource.handle);

  return eResult;

} /* END VCS_RemoveLDORestriction */

