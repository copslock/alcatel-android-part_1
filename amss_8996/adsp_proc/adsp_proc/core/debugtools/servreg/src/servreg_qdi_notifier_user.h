#ifndef SERVREG_QDI_NOTIFIER_USER_H
#define SERVREG_QDI_NOTIFIER_USER_H
/*
#============================================================================
#  Name:
#    servreg_qdi_notifier_user.h
#
#  Description:
#    Common Service Registry notifier header file for user images only
#
# Copyright (c) 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
#============================================================================
*/
#include "stdlib.h"
#include "qurt.h"
#include "servreg_common.h"

#if defined(__cplusplus)
extern "C"
{
#endif

typedef uint32_t SERVREG_REMOTE_HANDLE;

/** =====================================================================
 * Function:
 *     servreg_get_qdi_notif_node
 *
 * Description:
 *     Checks if a notifier node already exists with the sr monitor handle. If it does 
 *     exists it returns a pointer to that notif node.
 *
 * Parameters:
 *     sr_mon_handle : Handle to an existing service state which is mapped by domain + service 
 *                     or just domain name
 *
 * Returns:
 *    SERVREG_QDI_NOTIF_HANDLE : handle to the notifier node
 * =====================================================================  */
#if (defined(__GNUC__) && __GNUC__ >= 4) || defined(__clang__)
__attribute__((nonnull(1)))
#endif
SERVREG_QDI_NOTIF_HANDLE servreg_get_qdi_notif_node(SERVREG_MON_HANDLE sr_mon_handle);

/** =====================================================================
 * Function:
 *     servreg_create_qdi_notif_node
 *
 * Description:
 *     Creates a sr notif node with the given sr_mon_handle and sr_remote_handle
 *
 * Parameters:
 *     sr_mon_handle    : Handle to an existing service state which is mapped by domain + service 
 *                        or just domain name
 *     sr_remote_handle : Remote (root PD) notifier node address
 *
 * Returns:
 *    SERVREG_QDI_NOTIF_HANDLE : handle to the notifier node
 * =====================================================================  */
#if (defined(__GNUC__) && __GNUC__ >= 4) || defined(__clang__)
__attribute__((nonnull(1)))
#endif
SERVREG_QDI_NOTIF_HANDLE servreg_create_qdi_notif_node(SERVREG_MON_HANDLE sr_mon_handle, SERVREG_REMOTE_HANDLE sr_notif_remote_node);

/** =====================================================================
 * Function:
 *     servreg_delete_qdi_notif_node
 *
 * Description:
 *     Deletes a sr notif node given the sr notif handle
 *
 * Parameters:
 *     sr_qdi_notif_handle  : Handle to the notifier node to be deleted
 *
 * Returns:
 *     Refer to the enum SERVREG_RESULT for list of possible results
 * =====================================================================  */
#if (defined(__GNUC__) && __GNUC__ >= 4) || defined(__clang__)
__attribute__((nonnull(1)))
#endif
SERVREG_RESULT servreg_delete_qdi_notif_node(SERVREG_QDI_NOTIF_HANDLE sr_qdi_notif_handle);

/** =====================================================================
 * Function:
 *     servreg_map_remote_handle
 *
 * Description:
 *     Given the remote notif handle i.e root pd notif node address, return
 *     the user pd's corresponding monitor handle
 *
 * Parameters:
 *     sr_remote_handle : Remote (root PD) notifier node address
 *
 * Returns:
 *     SERVREG_MON_HANDLE :  Handle to an existing service state which is mapped by domain + service 
 *                           or just domain name
 * =====================================================================  */
SERVREG_MON_HANDLE servreg_map_remote_handle(SERVREG_REMOTE_HANDLE sr_remote_handle);

#if defined(__cplusplus)
}
#endif

#endif