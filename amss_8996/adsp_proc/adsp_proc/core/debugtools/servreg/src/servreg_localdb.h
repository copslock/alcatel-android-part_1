#ifndef SERVREG_LOCALDB_H
#define SERVREG_LOCALDB_H
/*
#============================================================================
#  Name:                                                                     
#    servreg_localdb.h 
#
#  Description:                                                              
#    None 
#                                                                            
# Copyright (c) 2015 by Qualcomm Technologies, Incorporated.  All Rights Reserved.        
#============================================================================
*/
#include "stdlib.h"
#include "qurt.h"
#include "servreg_common.h"
#include "servreg_internal.h"

#if defined(__cplusplus)
extern "C"
{
#endif

#define SERVREG_PREFIX_NAME "sr:"

/** =====================================================================
 * Function:
 *     servreg_get_local_soc_scope
 *
 * Description:
 *     Function to get the local soc scope info
 *     Queries the local database
 *
 * Parameters:
 *     None
 *
 * Returns:
 *     "soc" string
 * =====================================================================  */
SERVREG_NAME servreg_get_local_soc_scope(void);

/** =====================================================================
 * Function:
 *     servreg_get_local_domain_scope
 *
 * Description:
 *     Function to get the local domain scope
 *     Queries the local database
 *
 * Parameters:
 *     None
 *
 * Returns:
 *     "domain" string
 * =====================================================================  */
SERVREG_NAME servreg_get_local_domain_scope(void);

/** =====================================================================
 * Function:
 *     servreg_get_local_subdomain_scope
 *
 * Description:
 *     Function to get the local subdomain scope
 *     Queries the local database
 *
 * Parameters:
 *     None
 *
 * Returns:
 *     "subdomain" string
 * =====================================================================  */
SERVREG_NAME servreg_get_local_subdomain_scope(void);

/** =====================================================================
 * Function:
 *     servreg_get_local_instance_id
 *
 * Description:
 *     Function to get the local qmi instance id
 *     Queries the local database
 *
 * Parameters:
 *     None
 *
 * Returns:
 *     qmi_instance_id for the notifier module
 * =====================================================================  */
uint32_t servreg_get_local_instance_id(void);

/** =====================================================================
 * Function:
 *     servreg_name_check
 *
 * Description:
 *     This function checks if the given name is a valid service name or not.
 *
 * Parameters:
 *     name : "soc/domain/subdomain/provider/service" or just "soc/domain/subdomain" info
 *
 * Returns:
 *     SERVREG_SUCCESS       : If name is valid
 *     SERVREG_INVALID_PARAM : If name is invalid
 * =====================================================================  */
SERVREG_RESULT servreg_name_check(SERVREG_NAME domain, SERVREG_NAME service);

/** =====================================================================
 * Function:
 *     servreg_get_ssr_name
 *
 * Description:
 *     This function takes in the service name and returns the before shutdown ssr message name
 *
 * Parameters:
 *     name : "soc/domain/subdomain/provider/service" or just "soc/domain/subdomain" info
 *
 * Returns:
 *     ssr name : "ssr:####:before_shutdown" #### will be the name extracted from the input param domain
 * =====================================================================  */
SERVREG_NAME servreg_get_ssr_name(SERVREG_NAME domain);

/** =====================================================================
 * Function:
 *     servreg_is_domain_local
 *
 * Description:
 *     This function checks if the domain is local or remote for root-pd vs user-pd
 *     if root-pd : name is local if "soc/domain" matches with that of the root-pd's "soc/domain"
 *     if user-pd : local if "soc/domian/subdomain" scopes matches with the local "soc/domian/subdomain" string
 *
 * Parameters:
 *     name : "soc/domain/subdomain/provider/service" or just "soc/domain/subdomain" info
 *
 * Returns:
 *     TRUE  : If domain is local
 *     FALSE : If domain is remote
 * =====================================================================  */
SERVREG_BOOL servreg_is_domain_local(SERVREG_NAME name);

/** =====================================================================
 * Function:
 *     servreg_concat
 *
 * Description:
 *     This functions concatenates "soc/domain/subdomain" and "provider/service"
 *     string
 *
 * Parameters:
 *     domain : "soc/domain/subdomain" info
 *     service : "provider/service" info
 *
 * Returns:
 *     concatenated name : "soc/domain/subdomain/provider/service"
 * =====================================================================  */
 #if (defined(__GNUC__) && __GNUC__ >= 4) || defined(__clang__)
__attribute__((nonnull(1)))
#endif
SERVREG_NAME servreg_concat(SERVREG_NAME domain, SERVREG_NAME service);

#if defined(__cplusplus)
}
#endif

#endif