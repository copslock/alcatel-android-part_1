/*
#============================================================================
#  Name:
#    servreg_internal.c 
#
#  Description:
#     Common header file for Service Registry feature
#
# Copyright (c) 2015 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
#============================================================================
*/

#include "stringl/stringl.h"
#include "servreg_internal.h"

/** =====================================================================
 * Function:
 *     servreg_mutex_init_dal
 *
 * Description:
 *     Initialize a DAL mutex
 *
 * Parameters:
 *     mutex_p : mutex to be initialized
 *
 * Returns:
 *     None
 * =====================================================================  */
void servreg_mutex_init_dal(servreg_mutex_p const mutex_p)
{
   if (SERVREG_NULL != mutex_p)
   {
      secure_memset((void*)mutex_p, 0, sizeof(servreg_mutex_t));
      if (DAL_SUCCESS == DALSYS_SyncCreate(DALSYS_SYNC_ATTR_RESOURCE, &(mutex_p->mutex), &(mutex_p->mutexObject)))
      {
      }
   }
}

/** =====================================================================
 * Function:
 *     servreg_mutex_lock_dal
 *
 * Description:
 *     Lock the mutex
 *
 * Parameters:
 *    mutex_p : mutex to be locked
 *
 * Returns:
 *    None
 * =====================================================================  */
void servreg_mutex_lock_dal(servreg_mutex_p const mutex_p)
{
   if (SERVREG_NULL != mutex_p)
   {
      DALSYS_SyncEnter(mutex_p->mutex);
   }
}

/** =====================================================================
 * Function:
 *     servreg_mutex_unlock_dal
 *
 * Description:
 *     Unlock the mutex
 *
 * Parameters:
 *     mutex_p : mutex to be unlocked
 *
 * Returns:
 *     None
 * =====================================================================  */
void servreg_mutex_unlock_dal(servreg_mutex_p const mutex_p)
{
   if (SERVREG_NULL != mutex_p)
   {
      DALSYS_SyncLeave(mutex_p->mutex);
   }
}

/** =====================================================================
 * Function:
 *     servreg_nmelen
 *
 * Description:
 *     Function to find out the string length
 *
 * Parameters:
 *     name : string
 *
 * Returns:
 *     int : string length
 * =====================================================================  */
int servreg_nmelen(SERVREG_NAME const name)
{
   SERVREG_NAME s;
   for (s = name; '\0' != *s; ++s)
   /* NULL */;
   return s - name;
}

/** =====================================================================
 * Function:
 *     servreg_nmecmp
 *
 * Description:
 *     Compares two strings
 *
 * Parameters:
 *     name_1 : string 1
 *     name_2 : string 2
 *     len    : length of the string
 *
 * Returns:
 *     -1, 0 or 1
 * =====================================================================  */
int servreg_nmecmp(SERVREG_NAME const name_1, SERVREG_NAME const name_2, int len)
{
   SERVREG_NAME s1 = name_1, s2 = name_2;

   if (0 == len)
   {
      return 0;
   }

   while (len-- > 0 && *s1 == *s2)
   {
      if (0 == len || '\0' == *s1)
      {
         return 0;
      }
      s1++, s2++;
   }
   /* lexical analysis and return result (-1, 0, 1) */
   return(*s1 < *s2) ? -1 : (*s1 > *s2);
}




