/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

         E R R O R     R E P O R T I N G    S E R V I C E S

GENERAL DESCRIPTION
  This module provides error reporting services for both fatal and
  non-fatal errors.  This module is not a task, but rather a set of
  callable procedures which run in the context of the calling task.

Copyright (c) 2014 - 2015 by Qualcomm Technologies Incorporated.  All Rights Reserved.

*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

                        Edit History

$Header: //components/rel/core.adsp/2.7/debugtools/err/src/err_pd.c#8 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/01/15   abh     Added changes for extended smem logging
08/27/14   din     Made changes to support Audio PD.
07/31/14   din     File created to support Error on User PD.

===========================================================================*/


/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/

#include "comdef.h"
#include "qurt.h"
#include "qurt_event.h"
#include "erri.h"
#include <stdlib.h>
#include <stringl.h>
#include "err_qdi_client.h"
#include "DDITimetick.h"
#include "err_smem_log.h"


/*===========================================================================

                      Prototypes for internal functions

===========================================================================*/
void err_emergency_error_recovery(void) UERR_CODE_SECTION ;
static void err_reentrancy_violated(void) UERR_CODE_SECTION;
static void err_raise_to_kernel(void)  UERR_CODE_SECTION;
void err_fatal_handler( void ) ;

void err_fatal_lock(void) UERR_CODE_SECTION;
static void err_minimal_logging (const err_const_type* const_blk, uint32 code1, 
		         uint32 code2, uint32 code3) UERR_CODE_SECTION;
void err_fatal_jettison_core (unsigned int line, const char *file_name,
  const char *format, uint32 param1, uint32 param2, uint32 param3);



/*===========================================================================

                      Prototypes for external functions

===========================================================================*/
extern void err_update_image_versioning_info (void);
/*===========================================================================

                 Defines and variable declarations for module

===========================================================================*/

static boolean err_services_init_complete = FALSE;

/* Struct used to hold coredump data */
coredump_type coredump;
uint32 coredump_count=0;
err_fatal_params_type err_fatal_params UERR_DATA_SECTION;

#define ERR_SET_AND_FLUSH_PTR(PTR, VAL) \
  do { \
     PTR = VAL; \
     asm volatile ("dccleana(%0)" : : "r" ((qurt_addr_t )( &PTR )));\
   } while (0)

#define ERR_FLUSH_ADDR(ADDR) \
  do { \
     asm volatile ("dccleana(%0)" : : "r" ((qurt_addr_t )( &ADDR )));\
   } while (0)



/* Ptr used by assembly routines to grab registers */
/*  (update this as needed if struct changes)      */
arch_coredump_type* UERR_DATA_SECTION arch_coredump_ptr = 
                          (arch_coredump_type*)(&err_fatal_params.array);

static ERR_MUTEX_TYPE err_fatal_mutex UERR_DATA_SECTION;
static boolean err_fatal_mutex_init UERR_DATA_SECTION;
static DalDeviceHandle* phTimetickHandle UERR_DATA_SECTION;

static const err_cb_ptr err_preflush_internal[] =
{
  /* Function pointer for extended smem logging */
  err_info_to_smem_buffer,
  /* NULL must be last in the array */
  NULL
};


/* The function tables below are processed by the error handler
 * in the following order:
 *   1. err_preflush_external - Part of coredump structure (one time)
 *   2. err_flush_internal (one time)
 */
 
/* Flush will be done by kernel */
static const err_cb_ptr err_flush_internal[] =
{
  err_raise_to_kernel,  /* Should be the last callback */
  /* NULL must be last in the array */  
  NULL
};

/*===========================================================================

                              Function definitions

===========================================================================*/

/*===========================================================================

FUNCTION ERR_INITIALIZE_COREDUMP

DESCRIPTION
  Initializes coredump

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None

===========================================================================*/
static void err_initialize_coredump (void)
{
  /* Set type and version values */
  coredump.version = ERR_COREDUMP_VERSION;
  coredump.arch.type = ERR_ARCH_COREDUMP_TYPE;
  coredump.arch.version = ERR_ARCH_COREDUMP_VER;  
  coredump.os.type = ERR_OS_COREDUMP_TYPE;
  coredump.os.version = ERR_OS_COREDUMP_VER;
  coredump.err.version = ERR_COREDUMP_VER;

} /* err_initialize_coredump */

/*===========================================================================

FUNCTION ERR_INIT

DESCRIPTION
  Initialize error related functionality.

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.

===========================================================================*/

void err_init (void)
{
  /* Init Timer */
  DalTimetick_Attach("SystemTimer", &phTimetickHandle);

  if(!err_fatal_mutex_init)
  {
    ERR_MUTEX_INIT(&err_fatal_mutex);
    err_fatal_mutex_init=TRUE;
  }

  /* initialize the extended smem logging buffer */
  err_smem_log_init();

  /* Calculate the offset from which smem log needs to be written.
     * (only for User PD)
     */
  err_smem_calculate_offset();

  //MSG(MSG_SSID_TMS, MSG_LEGACY_LOW,"Err service initialized.");
    
  err_initialize_coredump();
  
  err_update_image_versioning_info();

  err_services_init_complete = TRUE;
  
} /* err_init */

/*===========================================================================

FUNCTION ERR_GET_TIMETICK

DESCRIPTION
  Add timetick value to specified address if timetick handle is initialized

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None .

===========================================================================*/

static void err_get_timetick(DalTimetickTime64Type * tick)
{
  if (NULL != phTimetickHandle)
  {
    /* Best effort, no failure action */
    DalTimetick_GetTimetick64(phTimetickHandle, tick );
  }  
} /* err_get_timetick */

/*===========================================================================

FUNCTION ERR_UPDATE_COREDUMP_TID_AND_IS_EXCEPTION

DESCRIPTION
 Updates tid information to coredump only the tid is not updated earlier.

DEPENDENCIES

RETURN VALUE
  No return.

SIDE EFFECTS

===========================================================================*/

void err_update_coredump_tid_and_is_exception(uint32 tid, boolean is_exception)
{
  qurt_thread_attr_t attr;
  uint32 stack_base = 0;  /* lower start address of the stack */
  uint32 stack_end = 0;   /* higher adress, i.e. range of the stack */
	  
  if ( coredump.err.tid == 0 )
  {
    coredump.err.tid = tid;
  }
  coredump.err.is_exception = is_exception;

  /* Get tcb name from tid name. It uses k0lock which is internal to kernel. 
   * But that doesn�t preempt the caller task in any way */
  qurt_thread_attr_get (coredump.err.tid, &attr);

  if ( attr.name[0] != 0 )
  {
    strlcpy(coredump.err.tcb_name, attr.name, QURT_THREAD_ATTR_NAME_MAXLEN );
  }

  stack_base = (uint32)attr.stack_addr;      /* base address of the stack */
  stack_end = stack_base + attr.stack_size;  /* Range of the stack */

  coredump.err.stack_error = STACK_NO_ERROR;

  /* Only check for stack_error if we got the stack addresses */
  if ( stack_base!= 0 && stack_end != 0 )
  {
    /* Check for stack overflow */
    if ( (coredump.arch.regs.name.fp < stack_base) ||
    	   (coredump.arch.regs.name.sp < stack_base) )
    {
      /* stack overflow detected */
  	  coredump.err.stack_error |= STACK_OVERFLOW;
    }

    /* Check for stack underflow */
    if ( (coredump.arch.regs.name.fp > stack_end) ||
    	   (coredump.arch.regs.name.sp > stack_end) )
    {
      /* stack underflow detected */
  	  coredump.err.stack_error |= STACK_UNDERFLOW;
    }
  }
  else
  {
    coredump.err.stack_error |= STACK_ERR_OBTAINING;
  }

} /* err_update_coredump_tid_and_is_exception */

/*===========================================================================

FUNCTION ERR_CALL_NEXT_TO_STM_CB

DESCRIPTION
 Calls err next to STM cb

DEPENDENCIES

RETURN VALUE
  No return.

SIDE EFFECTS

===========================================================================*/

void err_call_next_to_STM_CB(void)
{
  /* Set the start time for postflush callbacks */
  err_get_timetick( &(coredump.err.err_next_to_STM.cb_start_tick) );
  
  /* err_preflush_external[] is of size ERR_MAX_PREFLUSH_CB + 1 */
  ERR_FLUSH_ADDR( 
       coredump.err.err_next_to_STM.cb_start_tick );

  if ( coredump.err.err_next_to_STM.err_cb )
  {
    coredump.err.err_current_cb = 
	      coredump.err.err_next_to_STM.err_cb;
    coredump.err.err_next_to_STM.cb_start = TRUE;

    ERR_FLUSH_ADDR(coredump.err.err_current_cb);
    ERR_FLUSH_ADDR(coredump.err.err_next_to_STM.cb_start);
 
    coredump.err.err_current_cb();
  }
} /* err_call_next_to_STM_CB */

/*===========================================================================

FUNCTION ERR_PD_EXIT

DESCRIPTION
  Initiates PD exit 

DEPENDENCIES

RETURN VALUE
  No return.

SIDE EFFECTS
  **************************************************************
  ************ THERE IS NO RETURN FROM THIS FUNCTION ***********
  **************************************************************

===========================================================================*/

static void err_pd_exit(void)
{
  qurt_qdi_handle_invoke(QDI_HANDLE_GENERIC, QDI_OS_PROCESS_EXIT, 0);

} /* err_pd_exit */


/*===========================================================================

FUNCTION ERROR_FATAL_HANDLER

DESCRIPTION
  This function is invoked from err_fatal_jettison_core. When using JTAG,
  default breakpoint for ERR_FATAL should be placed at this function.
  Will log error to SMEM, kill the PA, and copy the coredump data into
  the err_data structure in unintialized memory.
 

DEPENDENCIES

RETURN VALUE
  No return.

SIDE EFFECTS
  **************************************************************
  ************ THERE IS NO RETURN FROM THIS FUNCTION ***********
  **************************************************************

===========================================================================*/
void err_fatal_handler ( void )
{
  int fptr_index;
  static uint32 err_count=0;

  /* Clean Cache */
  qurt_mem_cache_clean(0,0, QURT_MEM_CACHE_FLUSH_ALL, QURT_MEM_DCACHE);

  err_count++;

  if((err_count>1) || (err_services_init_complete!=TRUE))
  {
    err_initialize_coredump();
  
    /* May not return */
    err_emergency_error_recovery();
  }

  fptr_index=0;
  while(err_preflush_internal[fptr_index] != NULL)
  {
    /* Cycle through internal functions */
    ERR_SET_AND_FLUSH_PTR(coredump.err.err_current_cb, err_preflush_internal[fptr_index]); 
    coredump.err.err_current_cb();
    fptr_index++;
  }

  ERR_CRUMB_TRAIL_BMSK(ERR_CRUMB_TRAIL_BMSK_LOG_EXTERNEL_CBS_PRE);
 
  for(fptr_index=0; fptr_index<ERR_MAX_PREFLUSH_CB; fptr_index++)
  {
    err_get_timetick( 
      &(coredump.err.err_preflush_external[fptr_index].cb_start_tick));

    ERR_FLUSH_ADDR(coredump.err.err_preflush_external[fptr_index].cb_start_tick);

     /* Cycle through external functions */
    if(coredump.err.err_preflush_external[fptr_index].err_cb!= 0)
    {
       coredump.err.err_current_cb = 
	      coredump.err.err_preflush_external[fptr_index].err_cb;
      coredump.err.err_preflush_external[fptr_index].cb_start = TRUE;

      ERR_FLUSH_ADDR(coredump.err.err_current_cb);
      ERR_FLUSH_ADDR(coredump.err.err_preflush_external[fptr_index].cb_start);
       
      coredump.err.err_current_cb();
    }
  }

  /* Set the start time for non existent external cb, Can be used to calculate 
   * total time spanned by callbacks */
  err_get_timetick( 
      &(coredump.err.err_preflush_external[ERR_MAX_PREFLUSH_CB].cb_start_tick));
  
  /* err_preflush_external[] is of size ERR_MAX_PREFLUSH_CB + 1 */
  ERR_FLUSH_ADDR( 
       coredump.err.err_preflush_external[ERR_MAX_PREFLUSH_CB].cb_start_tick );

  /* Set the start time for postflush callbacks */
  err_get_timetick( &(coredump.err.err_postflush_external.cb_start_tick));
  
  /* err_preflush_external[] is of size ERR_MAX_PREFLUSH_CB + 1 */
  ERR_FLUSH_ADDR( 
       coredump.err.err_postflush_external.cb_start_tick );

  if ( coredump.err.err_postflush_external.err_cb )
  {
    coredump.err.err_current_cb = 
	      coredump.err.err_postflush_external.err_cb;
    coredump.err.err_postflush_external.cb_start = TRUE;

    ERR_FLUSH_ADDR(coredump.err.err_current_cb);
    ERR_FLUSH_ADDR(coredump.err.err_postflush_external.cb_start);
 
    coredump.err.err_current_cb();
  }

  /* Main loop (cache flush happens here) */
  fptr_index=0;
  while(err_flush_internal[fptr_index] != NULL)
  {
	/* Cycle through internal functions */
    ERR_SET_AND_FLUSH_PTR(coredump.err.err_current_cb, err_flush_internal[fptr_index]); 
    coredump.err.err_current_cb();
	fptr_index++;
  }

  /* err_raise_to_kernel called from err_flush_internal() would end up with main PD.
   * If main PD returns, Call into PD_EXIT (leading to PD ramdump collection and PD restart )
   * otherwise main PD would continue with SSR based on some configuration  */
  err_pd_exit();

} /* err_fatal_handler */


/*===========================================================================

FUNCTION ERR_FATAL_TRY_LOCK
DESCRIPTION
  Updates Error functionality at the entry of error. This is intentionally 
  used from exception handler.
============================================================================*/
void err_fatal_try_lock(void)
{
  ERR_MUTEX_TRY_LOCK(&err_fatal_mutex);
  
  /* err_get_timetick is not island mode safe */
  err_get_timetick( &(coredump.err.err_handler_start_time));

} /* err_fatal_try_lock */

/*===========================================================================

FUNCTION ERR_FATAL_UNLOCK
DESCRIPTION
  Unlocks Error mutex lock.
============================================================================*/
void err_fatal_unlock(void)
{
  ERR_MUTEX_UNLOCK(&err_fatal_mutex);

} /* err_fatal_unlock */


/*===========================================================================

FUNCTION ERR_FATAL_LOCK
DESCRIPTION
  Gets mutex for err_fatal to prevent multiple and/or cascading errors
============================================================================*/
void err_fatal_lock(void) 
{
  static boolean err_reentrancy_flag UERR_DATA_SECTION ;
  if ( !err_qdi_client_is_initialized() )
  {
    ERR_FATAL_ENTER_SINGLE_THREADED_MODE();
    ERR_FATAL_FLUSH_CACHE_NO_RETURN();
  }
  
  if(err_fatal_mutex_init==TRUE)
  {
    ERR_MUTEX_LOCK(&err_fatal_mutex);

    //mutex does not prevent the same thread being routed back into err_fatal by a bad callback
    if(err_reentrancy_flag)
    {
      //does not return
      err_reentrancy_violated();
    }
    else
    {
      err_reentrancy_flag = TRUE;
    }
  }
  else
  {
    /* If not intialized then this is an early ERR_FATAL */
    /* Proceed anyway so it can be handled */
  }

} /* err_fatal_lock */


/*===========================================================================

FUNCTION ERR_FATAL_CORE_DUMP
DESCRIPTION
  Logs fatal error information, including a core dump.

  NOTE: There is no return from this function.
============================================================================*/
void err_fatal_core_dump (
  unsigned int line,      /* From __LINE__ */
  const char   *file_name, /* From __FILE__ */
  const char   *format   /* format string */
)
{
  err_fatal_lock();
  err_fatal_jettison_core(line, file_name, format, 0, 0, 0);
}

/*===========================================================================

FUNCTION ERR_GET_TCB_NAME
DESCRIPTION
  Returns TCB NAME of the faulting thread.
  Should be used only after err_update_coredump_tid_and_is_exception()

  NOTE: There is no return from this function.
============================================================================*/
char * err_get_tcb_name (void)
{
  return coredump.err.tcb_name;
}

  
/*===========================================================================

FUNCTION ERR_FATAL_JETTISON_CORE
DESCRIPTION
  Logs fatal error information, including a core dump.
  Not to be called directly by outside code -- for that, use the function
  err_fatal_core_dump().

  NOTE: There is no return from this function.
============================================================================*/
#define ERR_FATAL_EXCEPTION_REENTRANCY "ERR_FATAL_EXCEPTION_REENTRANCY"

void err_fatal_jettison_core (
  unsigned int line,       /* From __LINE__ */
  const char   *file_name, /* From __FILE__ */
  const char   *format,    /* format string */
  uint32 param1,
  uint32 param2,
  uint32 param3
)
{

  /* NOTE: register information should already be saved prior to
   * calling this function.
   */

  /* Get tcb_ptr (if not pre-filled by err_exception_handler) */
  if (!coredump.os.tcb_ptr) {
    coredump.os.tcb_ptr = err_fatal_params.tcb;
  }

  /* Store line number */
  coredump.err.linenum = line;

  /* Copy file name */
  if(file_name != 0)
  {
    (void) strlcpy((char *)coredump.err.filename,
                       (char *)file_name,    
                       ERR_LOG_MAX_FILE_LEN);
  }

  /* Copy message string */
  if(format != 0)
  {
    (void) strlcpy((char *)coredump.err.message,
                       (char *)format,
                       ERR_LOG_MAX_MSG_LEN);
  }
  
  coredump.err.param[0]=param1;
  coredump.err.param[1]=param2;
  coredump.err.param[2]=param3;
  
  /* Inform PFR reason to root PD */
  err_qdi_invoke_inform_pfr(strnlen(coredump.err.message, ERR_LOG_MAX_MSG_LEN),coredump.err.message);
  
  ERR_CRUMB_TRAIL_BMSK(ERR_CRUMB_TRAIL_BMSK_ERR_FATAL_JETTISION_CORE_POST);

  /* Update the coredump pid */
  qurt_thread_context_get_pid( coredump.err.tid, 
		               &coredump.err.pd_id ); 

  /* Check if an user exceptions and ERR_FATAL arrived simultaneously */
  if ( err_fatal_params.crumb_trail_bmsk != 0 && 
        coredump.err.is_exception == TRUE)
  {
    /* Record secondary failure to coredump */
    strlcpy(coredump.err.aux_msg, ERR_FATAL_EXCEPTION_REENTRANCY, 
	    sizeof(ERR_FATAL_EXCEPTION_REENTRANCY));   
  }

  /* Call ERR_FATAL handler (no return) */
  err_fatal_handler();

}


/*=========================================================================

FUNCTION err_emergency_error_recovery

DESCRIPTION
  Action to be taken when more than one error has occurred, or if an
  error occurs before err_init has completed.

DEPENDENCIES
  None
 
RETURN VALUE
  None
  
SIDE EFFECTS
  No return from this function

===========================================================================*/
void err_emergency_error_recovery( void )
{
  /* Define action to be taken when multiple crashes occur */
  ERR_CRUMB_TRAIL_BMSK(ERR_CRUMB_TRAIL_BMSK_EMERGENCY_RECOVERY_PRE);

  /* flush cache, etc - does not return*/
  err_raise_to_kernel();

  /* Exit User PD */
  err_pd_exit(); 
}

/*=========================================================================

FUNCTION err_crash_cb_register

DESCRIPTION
  Registers a function (ptr type err_cb_ptr) to be called after an ERR_FATAL
  Function should NOT rely on any messaging, task switching (or system calls
  that may invoke task switching), interrupts, etc.

  !!!These functions MUST NOT call ERR_FATAL/ASSERT under ANY circumstances!!!

DEPENDENCIES
  None
 
RETURN VALUE
  TRUE if function added to table successfully
  FALSE if function not added.
  
SIDE EFFECTS
  None

===========================================================================*/
boolean err_crash_cb_register(err_cb_ptr cb)
{
  int i;
  boolean rval = FALSE;

  for(i=0; i<ERR_MAX_PREFLUSH_CB; i++)
  {
	if(coredump.err.err_preflush_external[i].err_cb == NULL)
	{
	  coredump.err.err_preflush_external[i].err_cb = cb;
	  rval = TRUE;
	  break;
	}
  }

  return rval;
}


/*=========================================================================

FUNCTION err_crash_cb_dereg

DESCRIPTION
 Deregisters a function from the error callback table.

DEPENDENCIES
  None
 
RETURN VALUE
  TRUE if removed
  FALSE if function is not found in table
  
SIDE EFFECTS
  None

===========================================================================*/
boolean err_crash_cb_dereg(err_cb_ptr cb)
{
  int i;
  boolean rval = FALSE;

  for(i=0; i<ERR_MAX_PREFLUSH_CB; i++)
  {
	if(coredump.err.err_preflush_external[i].err_cb == cb)
	{
	  coredump.err.err_preflush_external[i].err_cb = NULL;
	  rval = TRUE;
	  break;
	}
  }
  
  return rval;
}
/*=========================================================================

FUNCTION err_crash_cb_reg_next_to_STM

DESCRIPTION
  Registers a function (ptr type err_cb_ptr) to be called immediately after
  STM API call 
  Function should NOT rely on any messaging, task switching (or system calls
  that may invoke task switching), interrupts, etc.
 
  !!!These functions MUST NOT call ERR_FATAL under ANY circumstances!!!

DEPENDENCIES
  None
 
RETURN VALUE
  TRUE if function added to table successfully
  FALSE if function not added.

SIDE EFFECTS
  Only one registration of such API is supported so if its used by more than one
  clients than it will overwrite the old registered callback,
  this API was provided only for special case handling to stop ULT Audio Core
  in DPM PL
===========================================================================*/
boolean err_crash_cb_reg_next_to_STM(err_cb_ptr cb)
{
  if(NULL == coredump.err.err_next_to_STM.err_cb )
  {
    /* Check if already a callback registered*/
    coredump.err.err_next_to_STM.err_cb = cb;
    return TRUE;
  }
  else{
     return FALSE;
  }
}
/*=========================================================================

FUNCTION err_crash_cb_dereg_next_to_STM

DESCRIPTION
 Deregisters a function from the error callback table.

DEPENDENCIES
  None

RETURN VALUE
  TRUE if removed
  FALSE if function is not found in table

SIDE EFFECTS
  None

===========================================================================*/
boolean err_crash_cb_dereg_next_to_STM(err_cb_ptr cb)
{
  if(coredump.err.err_next_to_STM.err_cb == cb)
  {
    coredump.err.err_next_to_STM.err_cb = NULL;
    return TRUE;
  }
  else{
    return FALSE;
  }
}

/*===========================================================================
FUNCTION err_crash_cb_postflush_register
DESCRIPTION
  Register for callback function. The Callback function will be called after
  external functions are already called.
  It will be upto the callback to resolve its cache issues. 
============================================================================*/


boolean err_crash_cb_postflush_register (err_cb_ptr cb)
{
  if(NULL == coredump.err.err_postflush_external.err_cb )
  {
    /* Check if already a callback registered*/
    coredump.err.err_postflush_external.err_cb = cb;
    return TRUE;
  }
  else{
     return FALSE;
  }

} /* err_crash_cb_postflush_register*/

boolean err_crash_cb_postflush_deregister (err_cb_ptr cb)
{
  if(coredump.err.err_postflush_external.err_cb == cb)
  {
    coredump.err.err_postflush_external.err_cb = NULL;
    return TRUE;
  }
  else{
    return FALSE;
  }
} /* err_crash_cb_postflush_deregister */


/*=========================================================================

FUNCTION err_raise_to_kernel

DESCRIPTION
  Function which will terminate user space/PD handling and raise to kernel

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
static void err_raise_to_kernel(void)
{
  ERR_CRUMB_TRAIL_BMSK(ERR_CRUMB_TRAIL_BMSK_RAISE_TO_KERNEL_PRE);
  
  err_get_timetick(&(coredump.err.err_handler_end_time));
  
  err_qdi_invoke_err_handling_done();
}

/*===========================================================================

FUNCTION       err_fatal_post_exception_processing

DESCRIPTION
  This is called from exception handler after error fatal raises an 
  exception

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.

===========================================================================*/

void err_fatal_post_exception_processing(void)
{
  /* Copy trace log from island mode data structure */
  coredump.err.crumb_trail_bmsk |= err_fatal_params.crumb_trail_bmsk; 
  
  /* Copy compressed prt to coredump */
  coredump.err.compressed_ptr = err_fatal_params.msg_const_ptr;

  /* Copy register content */
  memscpy( coredump.arch.regs.array, SIZEOF_ALLOCATED_COREDUMP_REG,
             err_fatal_params.array,  SIZEOF_ARCH_COREDUMP_REGISTERS * sizeof(uint32) );

  ERR_CRUMB_TRAIL_BMSK(ERR_CRUMB_TRAIL_BMSK_CACHCE_CLEAN_PRE);

  /* Clean Cache */
  qurt_mem_cache_clean(0,0, QURT_MEM_CACHE_FLUSH_ALL, QURT_MEM_DCACHE);

  err_call_next_to_STM_CB();
  
  ERR_CRUMB_TRAIL_BMSK(ERR_CRUMB_TRAIL_BMSK_COREDUMP_UPDATED_PRE);

  err_fatal_jettison_core (err_fatal_params.msg_const_ptr->line,
                           err_fatal_params.msg_const_ptr->fname,
                           err_fatal_params.msg_const_ptr->fmt,
                           err_fatal_params.param1, 
                           err_fatal_params.param2, 
                           err_fatal_params.param3);

} /* err_fatal_post_exception_processing */

	

/*===========================================================================

FUNCTION       err_minimal_logging

DESCRIPTION

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.

===========================================================================*/

static void err_minimal_logging 
(
  const err_const_type* const_blk, 
  uint32                code1, 
  uint32                code2, 
  uint32                code3
)
{
   err_fatal_params.param1 = code1;
   err_fatal_params.param2 = code2;
   err_fatal_params.param3 = code3;
   err_fatal_params.tcb    = NULL;
   err_fatal_params.msg_const_ptr = const_blk;

} /* err_minimal_logging*/


/*===========================================================================

FUNCTION       err_Fatal_internal

DESCRIPTION
  Do not call any of these functions directly.  They should be accessed by
  macros defined in err.h exclusively.
  Code is expected to on run in uImage as well.
  
DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.

===========================================================================*/
void err_Fatal_internal3 
(
  const err_const_type* const_blk, 
  uint32                code1, 
  uint32                code2, 
  uint32                code3
)
{
 /* Enter critical section */
 err_fatal_lock();
 
 /* Capture registers */
 jettison_core();
  
 ERR_CRUMB_TRAIL_BMSK_ISLAND(ERR_CRUMB_TRAIL_BMSK_ERR_FATAL_PRE);
 ERR_CRUMB_TRAIL_BMSK_ISLAND(ERR_CRUMB_TRAIL_BMSK_JETTISON_CORE_POST);

 /* Capture minimal log, Updates err_fatal_params */
 err_minimal_logging( const_blk, code1, code2, code3 );

 ERR_CRUMB_TRAIL_BMSK_ISLAND(ERR_CRUMB_TRAIL_BMSK_ERR_FATAL_RAISE_EXCEPTION_PRE);

 /*  Raise an exception to qurt */
 qurt_exception_raise_nonfatal( ERR_RAISE_EXCEPTION_ARG(ERR_TYPE_ERR_FATAL) );

 /* Must not reach here */
 while(1);

}

#ifdef __HEXMSGABI_SUPPORTED__
void err_Fatal_internal3_pcrelR0
(
  const err_const_type* const_blk,
  uint32                code1,
  uint32                code2,
  uint32                code3
)
{
 asm("{r0 = memw(r31++#4); jump err_Fatal_internal3}");
}
#endif

void err_Fatal_internal2 (const err_const_type* const_blk, uint32 code1, uint32 code2) 
{
 err_Fatal_internal3(const_blk, code1, code2, 0 );
}

#ifdef __HEXMSGABI_SUPPORTED__
void err_Fatal_internal2_pcrelR0
(
  const err_const_type* const_blk,
  uint32                code1,
  uint32                code2
)
{
 asm("{r0 = memw(r31++#4); jump err_Fatal_internal2}");
}
#endif

void err_Fatal_internal1 (const err_const_type* const_blk, uint32 code1) 
{
  err_Fatal_internal3(const_blk, code1, 0, 0 );
}

#ifdef __HEXMSGABI_SUPPORTED__
void err_Fatal_internal1_pcrelR0
(
  const err_const_type* const_blk,
  uint32                code1
)
{
 asm("{r0 = memw(r31++#4); jump err_Fatal_internal1}");
}
#endif

void err_Fatal_internal0 (const err_const_type* const_blk) 
{
  err_Fatal_internal3(const_blk, 0, 0, 0 );
}

#ifdef __HEXMSGABI_SUPPORTED__
void err_Fatal_internal0_pcrelR0
(
  const err_const_type* const_blk
)
{
 asm("{r0 = memw(r31++#4); jump err_Fatal_internal0}");
}
#endif

/*=========================================================================

FUNCTION err_reentrancy_violated

DESCRIPTION
  This will only be called when ERR_FATAL is called while processing an
  ERR_FATAL.  It usually means that somone has registered a non-compliant
  callback function using 

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

===========================================================================*/
#define ERR_REENTRANCY_STRING "ERR_FATAL reentrancy violation, remove cb until resolved"
static void err_reentrancy_violated(void)  
{
  if ( qurt_island_get_status() == FALSE )
  {
    /* Record secondary failure to coredump */
    strlcpy(coredump.err.aux_msg, ERR_REENTRANCY_STRING, sizeof(ERR_REENTRANCY_STRING));
  }

  err_emergency_error_recovery();

}

void err_put_log (word line, const char *file_ptr)
{
  //err logging is not supported in this build
}
