/** vi: tw=128 ts=3 sw=3 et
@file version_term.c
@brief This file contains the API for the IMAGE VERSION, API 1.0.1
*/
/*=============================================================================
NOTE: The @brief description above does not appear in the PDF.
The tms_mainpage.dox file contains the group/module descriptions that
are displayed in the output PDF generated using Doxygen and LaTeX. To
edit or update any of the group/module text in the PDF, edit the
tms_mainpage.dox file or contact Tech Pubs.
===============================================================================*/
/*=============================================================================
Copyright (c) 2014 Qualcomm Technologies Incorporated.
All rights reserved.
Qualcomm Confidential and Proprietary
=============================================================================*/
/*=============================================================================
Edit History
$Header: //components/rel/core.adsp/2.7/debugtools/version/src/version_term.c#2 $
$DateTime: 2014/12/03 19:35:00 $
$Change: 7073232 $
$Author: pwbldsvc $
===============================================================================*/

#include "version.h"

void version_term(void)
{
}
