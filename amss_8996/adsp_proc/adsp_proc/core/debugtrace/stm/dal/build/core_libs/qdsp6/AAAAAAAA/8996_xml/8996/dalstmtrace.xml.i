<driver name="STMTrace">
  <device id=DALDEVICEID_STM_TRACE>
 <props name="stm_sp_base_addr" type=DALPROP_ATTR_TYPE_UINT32>
      0x08000000
    </props>
 <props name="stm_base_port" type=DALPROP_ATTR_TYPE_UINT32>
      3072
    </props>
 <props name="stm_num_ports" type=DALPROP_ATTR_TYPE_UINT32>
      768
    </props>
  </device>
</driver>
