# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/debugtrace/tfunnel/config/8996/daltfunnel.xml"
# 1 "<built-in>" 1
# 1 "<built-in>" 3
# 140 "<built-in>" 3
# 1 "<command line>" 1
# 1 "<built-in>" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/debugtrace/tfunnel/config/8996/daltfunnel.xml" 2
# 23 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/debugtrace/tfunnel/config/8996/daltfunnel.xml"
<driver name="TFunnel">
   <global_def>
      <var_seq name="tfunnel_phys_addr_arr" type=DALPROP_DATA_TYPE_UINT32_SEQ>


        0x03021000,0x03022000,0x03023000,end
      </var_seq>
   </global_def>

  <device id=DALDEVICEID_TFUNNEL_0>
    <props name="num_tfunnels" type=DALPROP_ATTR_TYPE_UINT32>
     0x3
    </props>
    <props name="tfunnel_phys_addrs" type=DALPROP_ATTR_TYPE_UINT32_SEQ_PTR>
     tfunnel_phys_addr_arr
    </props>
    <props name="port_rpm" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
      tfunnel_port_rpm
    </props>
    <props name="port_mpss" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
      tfunnel_port_mpss
    </props>
    <props name="port_adsp" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
      tfunnel_port_adsp
    </props>
    <props name="port_system_noc" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
      tfunnel_port_system_noc
    </props>
    <props name="port_apps_etm" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
      tfunnel_port_apps_etm
    </props>
    <props name="port_mmss_noc" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
      tfunnel_port_mmss_noc
    </props>
    <props name="port_peripheral_noc" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
      tfunnel_port_peripheral_noc
    </props>
    <props name="port_rpm_itm" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
      tfunnel_port_rpm_itm
    </props>
    <props name="port_mmss" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
      tfunnel_port_mmss
    </props>
    <props name="port_pronto" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
      tfunnel_port_pronto
    </props>
    <props name="port_bimc" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
      tfunnel_port_bimc
    </props>
    <props name="port_modem" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
      tfunnel_port_modem
    </props>
    <props name="port_ocmem_noc" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
      tfunnel_port_ocmem_noc
    </props>
    <props name="port_stm" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
      tfunnel_port_stm
    </props>
  </device>
</driver>
