# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/services/diag/f3_trace/src/diag_f3_trace_devcfg.xml"
# 1 "<built-in>" 1
# 1 "<built-in>" 3
# 140 "<built-in>" 3
# 1 "<command line>" 1
# 1 "<built-in>" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/services/diag/f3_trace/src/diag_f3_trace_devcfg.xml" 2
# 1 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/services/diag/f3_trace/src/diag_f3_trace_devcfg.h" 1
# 41 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/services/diag/f3_trace/src/diag_f3_trace_devcfg.h"
typedef enum
{
  DIAG_F3_TRACE_DISABLE = 0x0,
  DIAG_F3_TRACE_ENABLE = 0x1,
  DIAG_F3_TRACE_DIAG_MASK_DEBUG_MODE = 0x9,

  SIZEOF_DIAG_F3_TRACE_CONTROL_TYPE
} diag_f3_trace_control_type;
# 2 "/local/mnt/workspace/CRMBuilds/ADSP.8996.2.7.1.c3-00009-00121-1_20161004_004432/b/adsp_proc/core/services/diag/f3_trace/src/diag_f3_trace_devcfg.xml" 2
<driver name="NULL">
   <device id="diag_f3_trace">
    <!-- f3trace_control (equivalent to NV 1892 Diag Debug Control)
              DIAG_F3_TRACE_DISABLE (0x0) - logging off
              DIAG_F3_TRACE_ENABLE (0x1) - log filtered by f3trace_detail below
              DIAG_F3_TRACE_DIAG_MASK_DEBUG_MODE (0x9) - log based on diag-QXDM filters
    -->
    <props name="diag_f3_trace_control" type=DALPROP_ATTR_TYPE_UINT32>
      DIAG_F3_TRACE_ENABLE
    </props>

    <!-- f3trace_detail bitmask (equivalent to NV 1895 Diag Debug Detail)
              BIT Enables
               7 MSG_FATAL
               6 MSG_ERROR
               5 MSG_HIGH
               4 MSG_MED
               3 MSG_LOW
               2 <reserved>
               1 timestamp
               0 msg args
    -->
    <props name="diag_f3_trace_detail" type=DALPROP_ATTR_TYPE_UINT32>
      0xFF
    </props>




    <!-- DO NOT MODIFY -->
    <props name="f3trace_devcfg_version" type=DALPROP_ATTR_TYPE_UINT32>
      0x01
    </props>
  </device>
</driver>
