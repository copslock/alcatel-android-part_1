#ifndef DIAGBUF_MPD_TYPE_H
#define DIAGBUF_MPD_TYPE_H
/*==========================================================================

               Diagnostics Buffer

Description
  Type definition for the diagmpd buffer.

Copyright (c) 2014 by QUALCOMM Technologies, Incorporated.
All Rights Reserved. Qualcomm Confidential and Proprietary 
===========================================================================*/

/*===========================================================================

                          Edit History

      $Header:
      //components/dev/core.adsp/2.6/vinayk.CORE.ADSP.2.6.multiPD_optimization_Sep19/services/diag/LSM/qurt/src/diagbuf_mpd_type.h#1
      $

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
11/14/14   jtm/vk  Created file 
===========================================================================*/

#include "queue.h"
#include "osal.h"

typedef struct
{
  q_link_type        link;                           // Link used so that the root PD can place this item in a linked list

  /* PD identifier group */                                        
  int32              pid;                            // The process ID associated with the diag buf 

  /* Variable directly affecting mpd buffer group */
  uint8             *diagbuf_mpd_buf;                // The allocated memory for the buffer. 
  int32              diagbuf_mpd_head;               // The head of the ring buffer
  int32              diagbuf_mpd_tail;               // The tail of the ring buffer_used
  uint32             diagbuf_size;                   // Size of diagbuf pointed by diagbuf_mpd_buf
  osal_mutex_arg_t   diagbuf_mpd_buf_cs;             // The mutex associated with the diagbuf.  Used for updating the head
  boolean            in_use;                         // Indicates whether the buffer can be freed

  /* mpd buffer drain algorithm driver group */
  uint32             diagbuf_mpd_commit_size;        // Number of bytes currently in the buffer
  uint32             diagbuf_mpd_commit_threshold;   // Commit threshold in bytes. This is a % of diagbuf_size member
  uint32             diagbuf_mpd_drain_threshold;    // Drain threshold in bytes. This is a  % of diagbuf_size member
  osal_timer_t       diagbuf_mpd_drain_timer;        // PD Drain timer
  osal_timer_cb_type diagbuf_mpd_timer_cb;           // Timer callback function 

  /* mpd buffer Statistic group */
  uint32             msg_alloc_count;                // Msg allocation counter
  uint32             msg_drop_count;                 // Number of msgs that could not be allocated because the buffer is full
  uint32             log_alloc_count;                // Log allocation counter
  uint32             log_drop_count;                 // Number of logs that could not be allocated because the buffer is full                                        

} diagbuf_mpd_type;                                 // Type to encapsulate the diagbuf used for multi-PD

#endif  /* DIAGBUF_MPD_TYPE_H */
