#! /usr/bin/python

from lib.qurt import *

class UniqueStringClass:
    #
    #  Return a synthesized unique string.
    #  Mainly designed to make sure each call returns a value
    #   which is distinct from all other valid strings.
    #
    #  With this and the below assignment after the class
    #   definition, UniqueString() will return a new value
    #   each time it's called.
    #
    def __init__(self):
        self.counter = 0
    def __call__(self):
        self.counter += 1
        return '$$UNIQUE$$%u' % self.counter

UniqueString = UniqueStringClass()

class QIBPhysAddr(object):
    #
    #  QIBPhysAddr is a class for representing physical addresses known
    #   to the image builder.  Primarily, this is to unify two different
    #   types of addresses known to the builder -- absolute addresses,
    #   and relative addresses (relative to the start of the load image).
    #
    #  If relocatable is set to False, then addr is an absolute physical
    #   address.  The value of align is ignored.
    #  If relocatable is set to True, then addr is an address relative
    #   to the start of the physical load image.  The value "align" is
    #   an indicator of how much we can rely on the loader to choose
    #   an aligned physical address.  The default for "align" is 1MB,
    #   indicating that the loader will choose a load address which
    #   differs from the requested address by a multiple of 1MB.
    #
    def __init__(self, addr, relocatable=False, base=0, align=None):
        addr = int(addr)
        relocatable = bool(relocatable)
        base = int(base)
        if align == None:
            align = (0,0x100000)[relocatable]
        self.iaddr = addr
        self.irelocatable = relocatable
        self.ibase = base
        self.ialign = int(align)
    def __add__(self, delta):
        if delta == 0:
            return self
        return QIBPhysAddr(self.iaddr + delta, self.irelocatable, self.ibase, self.ialign)
    def __sub__(self, other):
        if isinstance(other, QIBPhysAddr):
            if self.irelocatable != other.irelocatable:
                raise Exception('Internal error -- cannot take difference of relocatable and non-relocatable addresses')
            return self.iaddr - other.iaddr
        return self + (-other)
    def __int__(self):
        return self.iaddr
    def __cmp__(self,other):
        #
        #  Pretend that all relocatable addresses are greater than all absolute addresses
        #
        if self.irelocatable != other.irelocatable:
            return self.irelocatable - other.irelocatable
        else:
            return self.iaddr - other.iaddr
    def basevalue(self):
        return self.iaddr+self.ibase
    def align_up_to_vaddr(self, vaddr, alignment=1):
        #
        #  Return the result of aligning the current address up to the
        #   next address which is congruent to (vaddr) modulo (alignment).
        #
        #  Check if the requested alignment is more strict than we
        #   can guarantee due to relocation uncertainty.
        #
        if (alignment-1) & ~(self.ialign-1):
            raise Exception('Requested alignment of %u is more strict than loader alignment of %u' %
                            (alignment, self.ialign))
        return self + ((vaddr-self.iaddr) & (alignment-1))

class ConfigField(object):
    #
    #  ConfigField represents a base class for implementing things like memory
    #   mapping attributes.  A ConfigField object can have a value of DontCare,
    #   or it can have a defined value.
    #
    #  Two objects of ConfigField can be "anded" together using the standard
    #   "&" operator.  If the two objects are compatible, the result will be
    #   the merged value of the two input objects.  In general, we have:
    #
    #    value & value == value
    #    value & DontCare == value
    #    DontCare & DontCare == value
    #    value1 & value2 == raises a ValueError exception if value1 and value2 not equal.
    #
    #  This is used as a shorthand for determining whether two objects are
    #   compatible, by hiding the actual combination logic in the objects.
    #
    def __init__(self, *ls, **kw):
        if len(ls) > 1:
            raise ValueError()
        self.default = False
        self.ivalue = ls                        # ivalue is either an empty tuple or a one-valued tuple
        self.__dict__.update(kw)                # Allow overriding default
    def value(self):
        return (self.ivalue+(self.default,))[0]
    def combine(self, other):
        raise ValueError()                      # This is only called when we couldn't do a trivial combine
    def __str__(self):
        return str(self.value())
    def __nonzero__(self):
        return bool(self.value())
    def __and__(self, other):
        if not isinstance(other, ConfigField):
            return self.value() & other
        if not other.ivalue:
            return self
        if not self.ivalue:
            return other
        if self.ivalue == other.ivalue:
            return self
        if self.__class__ == other.__class__:
            return self.combine(other)
        raise ValueError()

DontCare = ConfigField()
ConfigTrue = ConfigField(True)
ConfigFalse = ConfigField(False)

class QIBSection(Elf32SectionHeader):
    SHF_INTERNAL_ONLY      = 0x100000000                # Section should not be pushed out to the final ELF
    SHF_MAP_CAN_BE_SKIPPED = 0x200000000                # Mapping the section is optional
    SHF_TLB_MAPPING        = 0x400000000                # Section represents an actual Hexagon TLB mapping

    def __init__(self, *ls, **kw):
        self.mapping_group = 'default'
        Elf32SectionHeader.__init__(self, *ls, **kw)

    def tlb_encoding(self, is_reloc):
        #
        #  If it's a TLB mapping of the appropriate type (either
        #   relocatable or absolute), return the 8 byte string
        #   representing the binary encoding of the TLB mapping;
        #   otherwise return an empty string.
        #
        if (self.sh_flags & self.SHF_TLB_MAPPING) == 0:
            return ''
        if bool(self.physaddr.irelocatable) != bool(is_reloc):
            return ''
        import struct
        physaddr = self.physaddr
        tlb_entry  = (1 << 63)                                          # Set the valid bit
        tlb_entry |= ((self.G & 1) << 62)                               # Set the global bit
        tlb_entry |= (((int(self.physaddr) >> 35) & 1) << 61)           # Set the EP bit
        tlb_entry |= ((self.A1A0 & 3) << 59)                            # Set the bus attribute bits
        tlb_entry |= ((self.ASID & 0x7F) << 52)                         # Set the ASID field
        tlb_entry |= (((self.sh_addr >> 12) & 0xFFFFF) << 32)           # Set the VADDR field
        tlb_entry |= ((self.X & 1) << 31)                               # Set the X bit
        tlb_entry |= ((self.W & 1) << 30)                               # Set the W bit
        tlb_entry |= ((self.R & 1) << 29)                               # Set the R bit
        tlb_entry |= ((self.U & 1) << 28)                               # Set the U bit
        tlb_entry |= ((self.C & 0x0F) << 24)                            # Set the C field
        tlb_entry |= (((int(self.physaddr) >> 12) & 0x7FFFFF) << 1)     # Set the PADDR field
        sz = 1
        while 0x1000*sz*sz < self.sh_size:
            sz *= 2
        tlb_entry |= sz                                                 # Set the TLB entry size
        print '%016X' % tlb_entry
        return struct.pack('<Q', tlb_entry)

    def set_mapping_attributes(self):
        #
        #  Called prior to the step where we compute mappings.
        #   This should set all attributes for the section which
        #   make a difference for mapping purposes.
        #
        if hasattr(self,'memsection'):
            self.p_minus_v = ConfigField(self.physaddr - self.sh_addr)
            self.msvbase = ConfigField(self.sh_addr)
            return
        if not (self.sh_flags & self.SHF_ALLOC):
            return
        self.G = DontCare
        self.ASID = DontCare
        self.X = DontCare
        self.W = DontCare
        self.R = ConfigTrue
        self.U = DontCare
        self.SECURE = ConfigFalse
        self.locked = ConfigField(self.root_elf)
        self.FILE = ConfigField(self.f)
        print '... %s' % self.name
        if self.sh_flags & self.SHF_EXECINSTR:
            #
            #  ELF header requests execution permission
            #  For security reasons, turn off write permission
            #
            self.X &= ConfigTrue
            self.W &= ConfigFalse
        if self.sh_flags & self.SHF_WRITE:
            #
            #  ELF header requests write permission
            #  For security reasons, turn off execution permission
            #
            self.W &= ConfigTrue
            self.X &= ConfigFalse
        if self.name.startswith('.qskernel_') or self.name == '.qskstart':
            #
            #  Sections whose name starts with .qskernel_ are mapped global
            #   with ASID 0.  They are also mapped user executable and
            #   with no write permission.
            #
            if self.sh_type == self.SHT_NOBITS:
                self.sh_flags &= ~self.SHF_EXECINSTR
            else:
                self.sh_flags |= self.SHF_EXECINSTR
            self.sh_flags &= ~self.SHF_WRITE
            self.G &= ConfigTrue
            self.ASID = ConfigField(0)
            self.X = ConfigTrue
            if self.name == '.qskernel_eip' or self.name == '.qskernel_exports':
                self.R = ConfigTrue
            else:
                self.R = ConfigFalse
            self.W = ConfigFalse
            self.U &= ConfigTrue
            self.SECURE = ConfigTrue
        self.p_minus_v = ConfigField(self.physaddr - self.sh_addr)

    def overlaps(self, other):
        if self.sh_addr + self.sh_size <= other.sh_addr:
            return False
        if other.sh_addr + other.sh_size <= self.sh_addr:
            return False
        return True

    def merge(self, other, gbounds=None):
        #
        #  Here, self is a provisional mapping in the system, and
        #   other is a defined section which might conflict or
        #   modify self.
        #
        #  If other is compatible with self, update self to
        #   reflect any changes due to other and return True.
        #  If other is not compatible with self, do not update
        #   self, and return False.
        #
        if not self.overlaps(other):
            return True
        if (other.sh_flags & other.SHF_MAP_CAN_BE_SKIPPED):
            return True
        #
        #  Find all attributes of "other" which are of
        #   type ConfigField and merge them into "self".
        #  If anything is incompatible, we will throw a
        #   ValueError; catch it and return False.
        #
        try:
            for k,v in other.__dict__.items():
                if isinstance(v,ConfigField):
                    self.__dict__[k] = self.__dict__.get(k,DontCare) & v
            if gbounds != None:
                if len(gbounds) == 0:
                    gbounds.append(other.sh_addr)
                    gbounds.append(other.sh_addr+other.sh_size-1)
                else:
                    gbounds[0] = min(gbounds[0], other.sh_addr)
                    gbounds[1] = max(gbounds[1], other.sh_addr+other.sh_size-1)
            return True
        except ValueError:
            return False

    def setname(self, strtab):
        self.name = strtab[self.sh_name:].split('\0')[0]
        if self.name == '.qstart':
            self.name = '.start'

    def fixup(self):
        #
        # For compatibility with older builds, handle
        #  certain names with special treatment.
        #
        if self.name.startswith('.ukernel.') or self.name == '.start':
            self.sh_flags &= ~self.SHF_WRITE

    def setroot(self, isroot):
        self.root_elf = isroot
        if not isroot:
            self.name = '%s.%s' % (self.name, self.appname)
        if self.isalloc() and self.sh_type != self.SHT_NOBITS and self.sh_type != self.SHT_PROGBITS:
            if isroot:
                self.name = '.x%s' % self.name
            self.sh_type = self.SHT_PROGBITS
            self.sh_link = self.SHN_UNDEF
            self.sh_info = 0
            self.sh_entsize = 0

    def isalloc(self):
        #
        # Return TRUE if the section occupies address space.
        # This means that SHF_ALLOC is set, and that the size is non-zero.
        #
        if self.name == '.qskernel_vspace' or self.name == '.qsvspace' or self.name == '.qskernel_eip_build':
            #
            # This is a special case.  We don't propagate this section through to
            #  the output file or allocate physical space for it.
            #
            return False
        return ((self.sh_flags & self.SHF_ALLOC) != 0) and (self.sh_size != 0)

    def __getitem__(self,key):
        #
        # key[0] will be an address and key[1] will be a length.
        # Return as much of the beginning of the described address range
        #  as possible, or return an empty string.
        #
        if self.isalloc() and key[1]:
            offset = key[0] - self.sh_addr
            if offset < self.sh_size:
                f = self.contents()
                f.seek(offset)
                return f.read(key[1])           # The subfile will truncate at the end of the section
        return ''

class Elf_File(object):
    def __init__(self, cfg, appname, filename, restartable):
        self.appname = appname
        f = open(filename, 'rb')
        self.f = f
        eh = Elf32Header(f)
        if not hasattr(cfg, 'ehdr'):
            cfg.ehdr = eh
        f.seek(eh.e_shoff)
        sections = [QIBSection(f, f=f, cfg=cfg, appname=appname, restartable=restartable) for _ in range(eh.e_shnum)]
        strtab = sections[eh.e_shstrndx].contents().read()
        [s.setname(strtab) for s in sections]
        [s.fixup() for s in sections]
        [s.setroot(not cfg.alloc_sections) for s in sections]
        cfg.section_list.extend(sections)
        cfg.alloc_sections.append([s for s in sections if s.isalloc()])

def parse_build_elements(cfg, els):
    #
    #  parse_build_elements() must be a top level function in this file;
    #   the qurt_config.py script finds it and imports it.
    #
    #  This function is called with a list of XML Element nodes and is
    #   expected to parse the nodes appropriately and update the provided
    #   cfg structure with a parsed representation of the nodes.  It is
    #   expected that other code elsewhere will know what to do with the
    #   information attached to the cfg structure.
    #
    #  This function is called once by qurt_config.py and again by
    #   qurt-image-build.py.  It should perform the same parsing and
    #   updates in both cases, although not all of the information
    #   will be needed in both passes.
    #
    def BadBuildElem(s):
        raise Exception('Failure in <build> configuration: %s' % s)

    class generic_xml:
        def __init__(self, cfg):
            setattr(cfg, self.__class__.__name__, self)
            for k,v in self.defaults:
                setattr(self, k, v)
        def add_attr(self, k, v):
            setattr(self, k, v)
        def finalize(self):
            pass
        force_init = True
        defaults = []

    # For each "simple" XML element, define a class with the same name
    #  as the XML element, have it inherit from generic_xml, and set
    #  the class variable "defaults" to a list of default attributes.

    class max_elf_tlb(generic_xml):
        defaults = [('value', 0x100000)]
        def finalize(self):
            self.value = int(self.value,0)
    class relocatable_image(generic_xml):
        defaults = [('value', True), ('align', 0x100)]                  # Default:  True, 1MB alignment
        def finalize(self):
            if self.value.lower() == 'false':
                (self.value, self.align) = (False, 0x1000)              # False, 16MB alignment
            elif self.value.lower() == 'true':
                (self.value, self.align) = (True, 0x100)                # True, 1MB alignment
            else:
                (self.value, tmp) = (True, convert_size(self.value))
                if tmp < 0x1000 or (tmp & (tmp-1)) != 0:
                    raise Exception('relocatable_image value must be a power of two >= 4K')
                # Convert self.align to a number of 4K pages which can be represented in a TLB entry
                self.align = 0x1000
                while (self.align << 12) > tmp:
                    self.align /= 4
    class tlb_limit(generic_xml):
        defaults = [('value', 10000)]
        def finalize(self):
            self.value = int(self.value)
    class gap_reclaim(generic_xml):
        defaults = [('value', 'normal')]
    class tlbdump_usage(generic_xml):
        defaults = [('value', 'normal')]
    class mapping_method(generic_xml):
        defaults = [('value', 'normal')]
    class allow_map_wx(generic_xml):
        defaults = [('value', 'false')]

    class section:
        def __init__(self,cfg):
            self.match = lambda x, sname: (sname==None)
            self.attrs = {}
            cfg.section_info.append(self)
        def finalize(self):
            # Allow all section elements and attributes for now.
            # In the future, check here for conflicting attributes,
            #  illegal attributes, etc.
            pass
        def add_attr(self,k,v):
            from re import compile
            if k == 'tlb_lock' and v == '1':
                self.attrs['TLBLOCK'] = lambda s: setattr(s,'locked',True)
                self.attrs['TLBLOCK'] = lambda s: setattr(s,'reclaim',False)
            elif k == 'tlb_lock' and v == '0':
                self.attrs['TLBLOCK'] = lambda s: setattr(s,'locked',False)
                self.attrs['TLBLOCK'] = lambda s: setattr(s,'reclaim',False)
            elif k == 'tlb_lock' and v.lower() == 'root':
                self.attrs['TLBLOCK'] = lambda s: setattr(s,'locked',s.root_elf)
                self.attrs['TLBLOCK'] = lambda s: setattr(s,'reclaim',False)
            elif k == 'tlb_lock' and v.lower() == 'boot':
                self.attrs['TLBLOCK'] = lambda s: setattr(s,'locked',s.root_elf)
                self.attrs['TLBLOCK'] = lambda s: setattr(s,'reclaim',s.root_elf)
            elif k == 'mapping' and v.lower() == 'none':
                self.attrs['MAPPING'] = lambda s: setattr(s,'mapping','none')
                self.attrs['PERMS'] = lambda s: setattr(s,'sh_flags',s.sh_flags & ~(s.SHF_WRITE | s.SHF_EXECINSTR))
                self.attrs['CACHE'] = lambda s: setattr(s,'cache_policy','-1')
            elif k == 'mapping' and v.lower() == 'rx':
                self.attrs['MAPPING'] = lambda s: setattr(s,'mapping','rx')
                self.attrs['PERMS'] = lambda s: setattr(s,'sh_flags',(s.sh_flags | s.SHF_EXECINSTR) & ~s.SHF_WRITE)
            elif k == 'mapping' and v.lower() == 'rw':
                self.attrs['MAPPING'] = lambda s: setattr(s,'mapping','rw')
                self.attrs['PERMS'] = lambda s: setattr(s,'sh_flags',(s.sh_flags | s.SHF_WRITE) & ~s.SHF_EXECINSTR)
            elif k == 'mapping' and v.lower() == 'overlay':
                self.attrs['MAPPING'] = lambda s: setattr(s,'mapping','overlay')
            elif k == 'user_mode' and v.lower() == 'false':
                self.attrs['UMODE'] = lambda s: setattr(s,'user_mode',not s.root_elf)
            elif k == 'user_mode' and v.lower() == 'true':
                self.attrs['UMODE'] = lambda s: setattr(s,'user_mode',True)
            elif k == 'cache_policy':
                self.attrs['CACHE'] = lambda s,v=v: setattr(s,'cache_policy',v)
            elif k == 'mapping_group':
                self.attrs['MAPGROUP'] = lambda s,v=v: setattr(s,'mapping_group',v)
            elif k == 'physpool':
                self.attrs['PHYSPOOL'] = lambda s,v=v: setattr(s,'physpool', v)
            elif k == 'name' and not '*' in v:
                self.name = v
                self.match = lambda x, sname: (sname== x.name)
            elif k == 'name' and '*' in v:
                tmp = v.replace('.','\\.')
                tmp = v.replace('*','.*')
                self.regex = compile(tmp + '$')
                self.match = lambda x, sname: (sname and x.regex.match(sname) != None)
            elif k == 'match':
                self.regex = compile(v + '$')
                self.match = lambda x, sname: (sname and x.regex.match(sname) != None)
            else:
                BadBuildElem('%s cannot set %s to %s' % (self,k,v))

    class MemsectionGap(object):
        def __init__(self, cfg,
                     vaddr_variable_index,              # Mandatory; filled in by qurt_config
                     size_variable_index,               # Mandatory; filled in by qurt_config
                     size,                              # Mandatory
                     vaddr_variable_name = None,        # Optional; ignored
                     size_variable_name = None,         # Optional; ignored
                     cache_policy = "6",                # Optional; uncached
                     min_size = None,                   # Optional; set to size later
                     attach = "R",                      # Optional; read-only
                     glob = "1",                        # Optional; global
                     gap_recovery = "!R",               # Optional; not readable
                     physpool = "DEFAULT_PHYSPOOL",     # Optional; default physpool
                     max_fragment = "0xFFFFF000",       # Optional; effectively infinite
                     **kw):
            if kw:
                raise Exception('Unknown attributes for gap memsection: %s' % kw.keys())
            self.vaddr_index = int(vaddr_variable_index)
            self.size_index = int(size_variable_index)
            if min_size == None:
                min_size = size
            self.max_size = convert_size(size)
            self.min_size = convert_size(min_size)
            if cache_policy in cfg.cachepolicydict:
                cache = cfg.cachepolicydict[cache_policy]
            else:
                cache = int(cache_policy.split(':')[0], 0)
            self.tlb_entry = 1<<63
            self.tlb_entry |= cache << 24
            pdict = {'R': 1<<29, 'W': 1<<30, 'X': 1<<31}
            for c in attach:
                self.tlb_entry |= pdict[c.upper()]
            if glob == '1':
                self.tlb_entry |= 1<<62
            if physpool != 'DEFAULT_PHYSPOOL':
                raise Exception('Gap recovery only supported from DEFAULT_PHYSPOOL')
            self.max_fragment = convert_size(max_fragment)
            self.gap_recovery = gap_recovery
            self.cfg = cfg
        def rangefits(self, range):
            if isinstance(range, AllocatedRange):
                return False
            for s in self.gap_recovery.split(','):
                if s[0:1] == '!':
                    if getattr(range,s[1:]):
                        return False
                else:
                    if not getattr(range,s):
                        return False
            # Must contain at least 4K of usable space
            usable_size = aligndown(range.right,0x1000) - alignup(range.left,0x1000)
            if usable_size <= 0:
                return False
            range.usable_size = usable_size
            return True
        def process(self):
            vaddr_loc = self.cfg.unpack_from('<L', self.cfg.qurtos_slots + 4*self.vaddr_index)[0]
            size_loc = self.cfg.unpack_from('<L', self.cfg.qurtos_slots + 4*self.size_index)[0]
            poss = [r for r in self.cfg.physpool_space.ranges if self.rangefits(r)]
            poss.sort(key=lambda r: (r.usable_size, r.left))
            need_bytes = aligndown(self.max_size,0x1000)
            ranges = []
            for r in poss:
                if need_bytes > 0:
                    if r.usable_size <= need_bytes:
                        ranges.append(self.cfg.physpool_space.alloc(addr=alignup(r.left,0x1000),
                                                                    size=r.usable_size,
                                                                    gap_recovery=self))
                        need_bytes -= r.usable_size
                    elif r.usable_size <= self.max_fragment:
                        ranges.append(self.cfg.physpool_space.alloc(addr=alignup(r.left,0x1000),
                                                                    size=need_bytes,
                                                                    gap_recovery=self))
                        need_bytes = 0
            for r in self.cfg.physpool_space.ranges:
                if 'usable_size' in r.__dict__:
                    del r.__dict__['usable_size']
            tlb_list = []
            tsize = 0
            for r in ranges:
                addr = r.addr
                size = r.size
                while size:
                    tlbsize = 0x100000                                  # 1MB
                    while tlbsize > size or (addr%tlbsize) != 0:
                        tlbsize /= 4
                    tlb_list.append((tlbsize,addr))
                    addr += tlbsize
                    size -= tlbsize
                    tsize += tlbsize
            if tsize < self.min_size:
                raise Exception('Gap recovery memsection cannot meet minimum size')
            if tsize == 0:
                return
            tlb_list.sort(key=lambda x:(-x[0],x[1]))
            vaddr = self.cfg.vaddr_space.find(tsize,tlb_list[0][0],gap_recovery=self).addr
            self.cfg[(vaddr_loc,4)] = ('<L', vaddr)
            self.cfg[(size_loc,4)] = ('<L', tsize)
            for t in tlb_list:
                e = HexagonMapping()
                e.set_from_TLB_entry(self.tlb_entry | 1, locked=False, reloc=self.cfg.relocatable_image.value)
                e.vpage = (vaddr >> 12)
                e.ppage = (t[1] >> 12)
                e.size = (t[0] >> 12)
                self.cfg.extra_mappings.append(e)
                vaddr += t[0]

    section_attributes = section

    cfg.section_info = []

    for k,v in locals().items():
        if hasattr(v,'force_init'):
            v(cfg)

    cfg.cachepolicydict = {}
    for oe in els:
        if oe.tagName == 'cache_policy':
            cfg.cachepolicydict[oe.getAttribute('name')] = int(oe.getAttribute('value'),0)

    cfg.program_maps = {}
    for oe in els:
        if oe.tagName == 'program_name_map':
            cfg.program_maps[oe.getAttribute('name')] = int(oe.getAttribute('number'),0)

    cfg.memsectiongaps = []
    for oe in els:
        if oe.tagName == 'memsection_gap':
            cfg.memsectiongaps.append(MemsectionGap(cfg, **dict(oe.attributes.items())))

    for oe in els:
        for ie in oe.childNodes:
            if ie.nodeType == ie.ELEMENT_NODE:
                this = locals()[ie.tagName](cfg)
                if ie.attributes:
                    for k,v in ie.attributes.items():
                        this.add_attr(k,v)
                this.finalize()

def seg_optimize(segs):
    #
    # Look through the list for segments which overlap or touch
    # Elements are sorted by the start address
    # If they overlap, combine them and combine permissions
    # If they touch without overlapping, only combine them if permissions are identical
    # Edit the list and return True if we were able to optimize
    # Leave the list and return False if we were not able to optimize
    #
    for n in range(len(segs)-1):
        if segs[n][1] > segs[n+1][0]:
            ''' n and n+1 overlap '''
            if segs[n][2] != segs[n+1][2]:
                raise Exception('Overlapping segments with different memory maps')
            ''' n and n+1 overlap with the same offset '''
            ''' Combine them, including permissions, and return '''
            combined = (segs[n][0],
                        max(segs[n][1], segs[n+1][1]),
                        segs[n][2],
                        segs[n][3] or segs[n+1][3],
                        segs[n][4] or segs[n+1][4],
                        segs[n][5] or segs[n+1][5])
            segs = segs[:n] + [combined] + segs[n+2:]
            return (False, segs)
        elif segs[n][1] == segs[n+1][0]:
            ''' n and n+1 touch '''
            if segs[n][2:] == segs[n+1][2:]:
                ''' n and n+1 touch and have the same offset and permissions '''
                ''' combine them and return '''
                segs = segs[:n] + [(segs[n][0], segs[n+1][1]) + segs[n][2:]] + segs[n+2:]
                return (False, segs)
        else:
            ''' n and n+1 have separation between them '''
            pass
    return (True, segs)

def splitname(s, restart):
    from os import path
    names = s.split('=')
    if len(names) == 1:
        return (path.basename(s).replace('elf','pbn'), s, restart)
    if len(names) == 2:
        return (names[0], names[1], restart)
    raise Exception("input file argument cannot contain multiple '=' characters")

class PhysPool:
    def __init__(self, tuple):
        self.ranges = []
        self.name = tuple[0].split('\0')[0]
        for base,size in zip(tuple[1::2],tuple[2::2]):
            if size == 0:
                break
            if (size & 0xFFF) != 0:
                raise Exception("Pool %s has size which is not 4K aligned" % self.name)
            self.ranges.append([base,size >> 12])
    def alloc_pages(self, size, align):
        # Allocate pages from this PhysPool
        # For now, don't get fancy.  We only allocate in order from large->small,
        #  and so we should always be able to take the first fit.
        # Returns the page number
        for r in self.ranges:
            if r[1] >= size:
                r[0] += size
                r[1] -= size
                return r[0]-size
        raise Exception("Unable to allocate %u pages from phys pool %s" % (size, self.name))

class HexagonMapping:
    """Class for representing a Hexagon TLB entry."""
    def __init__(self):
        self.sname = ''
        pass
    def get_TLB_entry(self):
        """Return the 64-bit encoded TLB entry."""
        # Get the size in place first
        ret = 1
        while ret*ret < self.size:
            ret *= 2
        if ret*ret != self.size:
            raise Exception('Internal error -- locked TLB entry of illegal size')
        ret |= (1L << 63)       # Set the valid bit
        ret |= self.G << 62
        if self.ppage & 0x800000:
            ret |= (1L << 61)
        ret |= self.A1A0 << 59
        ret |= self.ASID << 52
        ret |= self.vpage << 32
        ret |= self.X << 31
        ret |= self.W << 30
        ret |= self.R << 29
        ret |= self.U << 28
        ret |= self.C << 24
        ret |= (self.ppage & 0x7FFFFF) << 1
        if self.ASID == 0 and self.X == 1 and self.W == 0 and self.C != 4 and self.C != 6:
            ret |= (1 << 28)
        return ret
    def set_from_TLB_entry(self, v, locked=True, mapping_group=None, reloc=False):
        if mapping_group == None:
            mapping_group = UniqueString()
        """Set the fields of the mapping based on the 64-bit TLB entry (v)."""
        if (v & (1L << 63)) == 0:
            raise Exception('Tried to use a TLB entry not set to valid')
        tmp = (v & 0x7F)
        tmp = (tmp & -tmp)    # Sets to 0, 1, 2, 4, 8, 16, 32, or 64
        sz = tmp * tmp       # Sets to 0, 1, 4, 16, 64, 256, 1024, or 4096
        if sz == 0:
            raise Exception('Tried to use a TLB entry with invalid size')
        self.island = False
        self.reclaim = False
        self.mapping_group = mapping_group
        self.locked = locked
        self.reloc = reloc
        self.physpool = None
        self.overlay = 0
        self.G    = ((v >> 62) & 0x01)
        self.A1A0 = ((v >> 59) & 0x03)
        self.ASID = ((v >> 52) & 0x7F)
        self.X    = ((v >> 31) & 0x01)
        self.W    = ((v >> 30) & 0x01)
        self.R    = ((v >> 29) & 0x01)
        self.U    = ((v >> 28) & 0x01)
        self.C    = ((v >> 24) & 0x0F)
        self.vpage = ((v >> 32) & 0xFFFFF) & (~(sz-1))
        self.ppage = ((v >>  1) & 0x7FFFFF) & (~(sz-1))
        if ((v >> 61) & 0x01):
            # EP bit is set, ppage gets high bit set
            self.ppage += 0x800000
        self.size = sz
        return self
    def contains(self, other):
        """Return True if self is a proper superset of other."""
        my_attr = (self.mapping_group, self.locked, self.G, self.A1A0, self.ASID,
                   self.X, self.W, self.R, self.U, self.C, self.reloc)
        his_attr = (other.mapping_group, other.locked, other.G, other.A1A0, other.ASID,
                    other.X, other.W, other.R, other.U, other.C, other.reloc)
        if my_attr != his_attr:
            return False
        if self.vpage - self.ppage != other.vpage - other.ppage:
            return False
        if other.vpage < self.vpage:
            return False
        if other.vpage + other.size > self.vpage + self.size:
            return False
        return True
    def overlaps(self, other):
        """Return True if self and other overlap in virtual space."""
        if self.vpage + self.size <= other.vpage:
            return False
        if other.vpage + other.size <= self.vpage:
            return False
        return True
    def fix_to_TLB_entries(self, maxsize=0x1000):
        """Takes a mapping of any size and returns an equivalent list of mappings of legal sizes."""
        from copy import copy
        ret = []
        off = 0            # Offset from beginning
        sz = self.size     # Number of pages left
        while sz > 0:
            this_sz = 0x1000
            while this_sz > 1:
                if this_sz <= sz and this_sz <= maxsize:
                    if ((self.vpage+off) & (this_sz-1)) == 0:
                        if ((self.ppage+off) & (this_sz-1)) == 0:
                            break
                this_sz = this_sz >> 2
            tmp = copy(self)
            tmp.vpage += off
            tmp.ppage += off
            tmp.size = this_sz
            tmp.mapping_group = self.mapping_group
            ret.append(tmp)
            sz -= this_sz
            off += this_sz
        return ret
    def __str__(self):
        return '<%05X->%06X [%04X] G:%u A:%u ASID:%u X:%u W:%u R:%u U:%u C:%X %s %s %s %s %s>' % (self.vpage,
                                                                                               self.ppage,
                                                                                               self.size,
                                                                                               self.G,
                                                                                               self.A1A0,
                                                                                               self.ASID,
                                                                                               self.X,
                                                                                               self.W,
                                                                                               self.R,
                                                                                               self.U,
                                                                                               self.C,
                                                                                               ('unlocked','locked')[self.locked],
                                                                                               self.mapping_group,
                                                                                               ('non-island','island')[self.island],
                                                                                               ('non-reloc','reloc')[self.reloc],
                                                                                               self.sname)

class QurtElfPatch:
    """Class for doing ELF patch processing on the merged ELF file."""
    #
    # When we call start() on this class, the class becomes basically like
    #  a 4GB bytearray in the ELF's virtual address space.
    # Slices of the virtual address space can be accessed using standard
    #  slice notation.  For instance, to access the 4 bytes at address ADDR
    #  you can use self[ADDR:ADDR+4].
    # To read the same 4 bytes into X as an unsigned 32-bit quantity:
    #  X = struct.unpack('<L', self[ADDR:ADDR+4])
    # To write the same 4 bytes with X which is an unsigned 32-bit quantity:
    #  self[ADDR:ADDR+4] = struct.pack('<L', X)
    #
    # Note that accesses outside of the defined virtual image of the ELF cause
    #  an exception.  Portions of the virtual image which are not backed by
    #  file data (such as bss sections, etc.) can be read, but return zeroes;
    #  attempts to write non-zero data to those cause an exception.
    #
    def __init__(self):
        self.section_list = []
        self.alloc_sections = []
        self.all_mappings = []
        self.opt_mappings = []
        self.island_moves = []
        self.extra_mappings = []
        self.ramfs_info = []
        self.vaddr_space = QurtAddressSpace()
        self.vaddr_space.alloc(left= -QurtInfinity,
                               right= 0x1000, reserve_only=True)
        self.vaddr_space.alloc(left=0xFFFFF000,
                               right= QurtInfinity, reserve_only=True)
        self.physpool_space = QurtAddressSpace()
        self.deferred_writes = []
    def __getitem__(self, key):
        from struct import unpack, calcsize
        cvt = False
        if isinstance(key,slice):
            sz = key.stop - key.start
            addr = key.start
        elif isinstance(key[1], basestring):
            sz = calcsize(key[1])
            addr = key[0]
            cvt = True
        else:
            sz = key[1]
            addr = key[0]
        ret = []
        for s in self.alloc_sections_sorted:
            if sz:
                tmp = s[(addr,sz)]
                if tmp:
                    addr += len(tmp)
                    sz -= len(tmp)
                    ret.append(tmp)
        if sz:
            raise Exception('Cannot read %u bytes at vaddr %X' % (sz, addr))
        ret = ''.join(ret)
        if cvt:
            ret = unpack(key[1], ret)
        return ret
    def __setitem__(self, key, value):
        # If value is not a string, assume it's an iterable (most likely a tuple)
        #  appropriate for being passed as the argument list to struct.pack
        if not isinstance(value, basestring):
            from struct import pack
            value = pack(*value)
        sz = len(value)
        if isinstance(key,slice):
            addr = key.start
        else:
            addr = key[0]
        if not hasattr(self, 'file'):
            self.deferred_writes.append((addr,value))
            return
        while sz > 0:
            this_sz = 0
            for p in self.phdr:
                if addr >= p.p_vaddr:
                    if addr < p.p_vaddr + p.p_filesz:
                        # The next byte we need to read is in this segment
                        this_sz = min(sz, (p.p_vaddr + p.p_filesz) - addr)
                        this_offset = p.p_offset + (addr - p.p_vaddr)
                        self.file.seek(this_offset)
                        self.file.write(value[:this_sz])
                        self.file.flush()
                        addr += this_sz
                        sz -= this_sz
                        value = value[this_sz:]
                        break
            if this_sz == 0:
                raise Exception('Cannot read %u bytes at vaddr %X' % (sz, addr))
    def parse_args(self,argv):
        from optparse import OptionParser, OptionGroup
        parser = OptionParser(usage='%prog [parameters] [[flags...] input_file]...')

        def add_flag(parser, key):
            if len(parser.largs) == 0 or not isinstance(parser.largs[-1],list):
                parser.largs.append(list())
            parser.largs[-1].append(key)

        def opt_callback_align(option, opt_str, value, parser):
            try:
                value = convert_size(value.upper())
            except ValueError:
                parser.error('Invalid format for --align')
            add_flag(parser, 'align')
            add_flag(parser, value)

        def opt_callback_file(option, opt_str, value, parser):
            add_flag(parser, 'ramfs')

        def opt_callback_contig(option, opt_str, value, parser):
            add_flag(parser, 'contig')

        def opt_callback_restart(option, opt_str, value, parser):
            add_flag(parser, 'restartable')

        group = OptionGroup(parser, 'Required Parameters')
        group.add_option('-o', action='store', dest='outfilename',
                         help='write output to FILENAME', metavar='FILENAME')
        group.add_option('-p', action='store', dest='physaddr',
                         help='set base physical address of output file to PHYSADDR', metavar='PHYSADDR')
        parser.add_option_group(group)

        group = OptionGroup(parser, 'Optional Parameters')
        group.add_option('-n', action='store_true', dest='no_adjust',
                         help='skip writing _reloc versions of input files')
        group.add_option('--assert_free_microimage', action='store', dest='free_uimg', default='0',
                         help='fail the build if the amount of microimage free space falls below SIZE', metavar='SIZE')
        parser.add_option_group(group)

        group = OptionGroup(parser, 'Flags affecting the following input file')
        group.add_option('-R', action='callback', callback=opt_callback_restart,
                         help='mark the next input file as a restartable ELF')
        group.add_option('--file', action='callback', callback=opt_callback_file,
                         help='mark the next input file as a RAMFS file')
        group.add_option('--align', action='callback', callback=opt_callback_align, type='string',
                         help='mark the next input file as being aligned to BOUNDARY in physical memory', metavar='BOUNDARY')
        group.add_option('--contiguous', action='callback', callback=opt_callback_contig,
                         help='mark the next input file as being contiguous in physical memory')
        parser.add_option_group(group)

        group = OptionGroup(parser, 'Deprecated Parameters',
                            'These are allowed but ignored')
        group.add_option('-e', action='store_true',
                         help='request old EIP format')
        group.add_option('-t', action='store',
                         help='use Hexagon tools at TOOLSDIR', metavar='TOOLSDIR')
        parser.add_option_group(group)

        (opts, args) = parser.parse_args()

        # Put all of the arguments where they belong:

        self.outfilename = opts.outfilename
        if self.outfilename == None:
            parser.error('Must provide an output file name using -o')

        try:
            if opts.physaddr == None:
                raise ValueError()
            self.physaddr = convert_size(opts.physaddr.upper())
            if (self.physaddr & 0xFFFFF000) != self.physaddr:
                raise ValueError()
        except ValueError:
            parser.error('Must provide a valid 4K-aligned physical address using -p')

        try:
            self.free_uimg = convert_size(opts.free_uimg.upper())
        except ValueError:
            parser.error('Invalid format for --assert_free_microimage')

        self.write_reloc = not opts.no_adjust

        if len(args) == 0:
            parser.error('At least one input ELF must be provided')

        if isinstance(args[-1],list):
            parser.error('Flags found with no file name following')

        self.infiles = []
        self.ramfs = []
        flags = []
        for x in args:
            if isinstance(x,list):
                flags = x
            elif 'ramfs' in flags:
                self.ramfs.append((x,flags))
                flags = []
            else:
                self.infiles.append(Elf_File(self, *splitname(x, 'restartable' in flags)))
                flags = []

    def unpack_from(self, fmt, offset):
        return self[(offset,fmt)]
    def finish(self):
        #
        # Write the following in order:
        #  * ELF Header
        #  * Program Headers
        #  * Section Headers
        #  * String Table
        #  * Comment Section contents
        #  * Program Header contents
        #
        # After all have been written, rewrite the ELF Header,
        #  Program Headers, and Section Headers, as they've
        #  now been updated with their final file offsets.
        #
        import shutil

        nullsection = Elf32SectionHeader(None,
                                         name = '',
                                         sh_name = 0,
                                         sh_type = Elf32SectionHeader.SHT_NULL,
                                         sh_flags = 0,
                                         sh_addr = 0,
                                         sh_offset = 0,
                                         sh_size = 0,
                                         sh_link = Elf32SectionHeader.SHN_UNDEF,
                                         sh_info = 0,
                                         sh_addralign = 0,
                                         sh_entsize = 0)

        strtabsection = Elf32SectionHeader(None,
                                           name = '.shstrtab',
                                           sh_name = 0,
                                           sh_type = Elf32SectionHeader.SHT_STRTAB,
                                           sh_flags = 0,
                                           sh_addr = 0,
                                           sh_offset = 0,
                                           sh_size = 0,
                                           sh_link = Elf32SectionHeader.SHN_UNDEF,
                                           sh_info = 0,
                                           sh_addralign = 1,
                                           sh_entsize = 0)

        self.shdr = [s for s in self.shdr if not hasattr(s,'s_internal')]

        self.shdr.insert(0,nullsection)
        self.shdr.insert(1,strtabsection)
        self.phdr = sorted(self.phdr, key=lambda p: p.p_paddr)
        self.file = open(self.outfilename, 'wb')
        f = self.file
        e = self.ehdr

        BookmarkWrite("EHDR", f, e.output())
        (e.e_phoff, e.e_phnum) = BookmarkWrite("PHDR", f, ''.join([p.output() for p in self.phdr]))
        (e.e_shoff, e.e_shnum) = BookmarkWrite("SHDR", f, ''.join([s.output() for s in self.shdr]))
        e.e_phnum /= e.e_phentsize
        e.e_shnum /= e.e_shentsize
        e.e_shstrndx = 1
        (strtabsection.sh_offset, strtabsection.sh_size) = BookmarkWrite("_", f, strtabify(self.shdr))
        for s in self.shdr:
            if (s.sh_flags & s.SHF_ALLOC) == 0:
                if s.sh_type not in (s.SHT_STRTAB, s.SHT_NULL):
                    (s.sh_offset, _) = BookmarkWrite("_", f, s.contents().read())
        for p in self.phdr:
            if p.p_align > 1:
                f.write('\0' * ((p.p_paddr - f.tell()) % p.p_align))
            saveloc = f.tell()
            shutil.copyfileobj(p.contents(), f)
            p.p_offset = saveloc
            for s in p.section_list:
                s.sh_offset = p.p_offset + (s.sh_addr - p.p_vaddr)

        BookmarkWrite("@EHDR", f, e.output())
        BookmarkWrite("@PHDR", f, ''.join([p.output() for p in self.phdr]))
        BookmarkWrite("@SHDR", f, ''.join([s.output() for s in self.shdr]))

        f.flush()
        for t in self.deferred_writes:
            self[(t[0],len(t[1]))] = t[1]
        f.flush()
        f.close()
    def add_segment(self, vaddr, paddr, size, contents, sectionname=None, executable=False, writable=False):
        #
        # Add a new segment (and section) to the output ELF file.
        # The virtual address, physical address, and size are as specified.
        # The "contents" argument must be a file-like object.
        #
        # Optional arguments include:  sectionname (if omitted, a default is used)
        #                              executable (False by default)
        #                              writable (False by default)
        #
        # TBD:  Do we ever need to specify alignment different than 0x1000?
        #
        if sectionname == None:
            self.qibsectioncount = getattr(self,'qibsectioncount',0) + 1
            sectionname = '.qib.%u' % self.qibsectioncount

        p = self.phdr[0].copy()
        p.p_type = p.PT_LOAD
        p.p_offset = 0                  # Start at beginning of contents
        p.p_vaddr = vaddr
        p.p_paddr = paddr
        p.p_filesz = size
        p.p_memsz = size
        p.p_flags = 4                   # Readable
        if writable:
            p.p_flags |= 2              # Writable
        if executable:
            p.p_flags |= 1              # Executable
        p.p_align = 0x1000

        s = self.shdr[0].copy()
        s.name = sectionname
        s.sh_type = s.SHT_PROGBITS
        s.sh_flags = s.SHF_ALLOC
        s.sh_addr = vaddr
        s.sh_offset = 0                 # Start at beginning of contents
        s.sh_size = size
        s.sh_link = s.SHN_UNDEF
        s.sh_info = 0
        s.sh_addralign = 1
        s.sh_entsize = 0
        p.section_list = [s]

        self.phdr.append(p)
        self.shdr.append(s)
        p.f = contents

    def read_boot_mappings(self):
        # Get the boot mappings from the ELF file
        addr = self.eip_addr_boot_maps
        boot_maps = []
        while True:
            tmp = self.unpack_from('<QLL', addr)
            if tmp[0] == 0:
                break
            boot_maps.append(tmp)
            addr += 16
        self.boot_mappings = boot_maps
    def read_tlb_dump(self):
        self.compare_tlbs = []
        if self.tlbdump_usage.value == 'ignore':
            return None
        addr = self.eip_addr_tlb_dump
        tlb_dump = []
        for i in range(self.eip_size_tlb_dump):
            tmp = self.unpack_from('<Q', addr)[0]
            if (tmp & (1L << 63)):
                if self.tlbdump_usage.value == 'compare':
                    self.compare_tlbs.append(tmp)
                else:
                    tlb_dump.append(HexagonMapping().set_from_TLB_entry(tmp))
            addr += 8
        self.tlb_dump = tlb_dump
        self.all_mappings.extend(tlb_dump)
    def do_tlb_compare(self, v, vn):
        if not self.compare_tlbs:
            return
        original = self.compare_tlbs
        updated = v+vn
        total = sorted(original+updated)
        for t in total:
            if not t in updated:
                print 'Original only: %s' % HexagonMapping().set_from_TLB_entry(t)
            if not t in original:
                print 'Updated  only: %s' % HexagonMapping().set_from_TLB_entry(t)
    def write_tlb_dump(self, v, vn):
        addr = self.eip_addr_tlb_dump
        if len(v)+len(vn) > self.eip_size_tlb_dump:
            raise Exception('Build generates %u static TLB entries -- only %u are allowed' %
                            (len(v)+len(vn), self.eip_size_tlb_dump))
        if len(v)+len(vn) > self.tlb_limit.value:
            raise Exception('Build generates %u total TLB entries, exceeds XML configured limit of %u' %
                            (len(v)+len(vn), self.tlb_limit.value))
        for tmp in v:
            self[(addr,8)] = ('<Q', tmp)
            addr += 8
        for tmp in vn:
            self[(addr,8)] = ('<Q', tmp)
            addr += 8
        self[(self.eip_addr_tlb_relocs,4)] = ('<L', len(v))
    def write_tlb_reclaim(self, mask):
        if self.qurtos_tlb_reclaim:
            self[(self.qurtos_tlb_reclaim,16)] = ('<QQ',
                                                  (mask >>  0) & ((1L << 64) - 1),
                                                  (mask >> 64) & ((1L << 64) - 1))
    def write_quick_mmu(self, i):
        addr = self.eip_addr_tlb_dump
        pg = (addr >> 12)
        for t in self.opt_mappings:
            if pg >= t.vpage and pg < t.vpage + t.size and t.locked:
                val = (addr - (t.vpage << 12)) + (t.ppage << 12)
                if t.reloc:
                    val += 1
                self[(self.eip_addr_quick_mmu,4)] = ('<L', val)
    def read_mmap_table(self):
        lock_count = self.unpack_from('<L', self.eip_addr_tlblock)[0]
        addr = self.eip_addr_mmap_table
        mmap_table_locked = []
        mmap_table_unlocked = []
        while True:
            tmp = self.unpack_from('<Q', addr)[0]
            if tmp == 0:
                break
            addr += 8
            if len(mmap_table_locked) < lock_count:
                if self.tlbdump_usage.value == 'compare':
                    self.compare_tlbs.append(tmp)
                mmap_table_locked.append(HexagonMapping().set_from_TLB_entry(tmp, locked=True))
            else:
                mmap_table_unlocked.append(HexagonMapping().set_from_TLB_entry(tmp, locked=False, mapping_group='default'))
        self.mmap_table_locked = mmap_table_locked
        self.mmap_table_unlocked = mmap_table_unlocked
        self.all_mappings.extend(mmap_table_locked)
        self.all_mappings.extend(mmap_table_unlocked)
        for x in mmap_table_locked+mmap_table_unlocked:
            self.vaddr_space.alloc(size=x.size << 12,
                                   addr=x.vpage << 12,
                                   mmap=x)
    def read_pool_configs(self):
        self.pools = []
        addr = self.pool_configs
        if addr == 0:
            return
        while True:
            tmp = self.unpack_from('<32s32L', addr)
            if tmp[0].startswith('\0'):
                break
            addr += 160
            self.pools.append(PhysPool(tmp))

    def process_input_files(self):
        ''' Figure out where we put it in memory '''
        curloc = self.physaddr
        for f in self.infiles:
            mbot = 0
            mtop = 0
            malign = 0
            for p in f.phdr:
                if mtop == 0:
                    mbot = p.p_paddr
                    mtop = p.p_paddr+p.p_memsz
                    malign = p.p_align
                else:
                    mbot = min(mbot, p.p_paddr)
                    mtop = max(mtop, p.p_paddr+p.p_memsz)
                    malign = max(malign,p.p_align)
            if malign == 0:
                malign = 1
            ''' Need to increase curloc until it is congruent to mbot modulo malign '''
            curloc += ((mbot%malign)+malign-(curloc%malign))%malign
            f.output_physaddr = curloc
            f.vmem_bot = mbot
            f.vmem_top = mtop
            f.memalign = malign
            curloc += (mtop-mbot)
            ''' Round up to next 1MB boundary '''
            curloc = (curloc+0xFFFFF) & ~0xFFFFF

    def synthesize_elf_mappings(self, infiles):
        asid = 0
        for i in infiles:
            for s in i.shdr:
                if (s.sh_flags & s.SHF_ALLOC) != 0 and s.sh_size > 0:
                    sname = s.name
                    match = []
                    for p in self.phdr:
                        if s.sh_addr >= p.p_vaddr:
                            if s.sh_addr + (s.need_map * s.sh_size) <= p.p_vaddr + p.p_memsz:
                                match.append(p)
                    if len(match) != 1:
                        print 'PHDRS:'
                        for p in sorted(self.phdr, key=lambda p: p.p_vaddr):
                            print '    %s' % p.oneline()
                        print 'SHDRS:'
                        for s in sorted(i.shdr, key=lambda s: s.sh_addr):
                            if (s.sh_flags & s.SHF_ALLOC) != 0 and s.sh_size > 0:
                                print '    %s' % s.oneline()
                        raise Exception('Cannot find unique PHDR for section %s' % sname)
                    p = match[0]
                    paddr = s.sh_addr + p.p_paddr - p.p_vaddr
                    perm_write = ((s.sh_flags & s.SHF_WRITE) != 0)
                    perm_exec = ((s.sh_flags & s.SHF_EXECINSTR) != 0)
                    skip = False
                    for z in self.boot_mappings:
                        if s.sh_addr >= z[1] and s.sh_addr < z[2]:
                            skip = True
                    if not skip:
                        self.add_elf_mapping(s, s.sh_addr, s.sh_size, paddr, perm_write, perm_exec, asid, sname.endswith('.island'), i)
            asid += 1
    def add_elf_mapping(self, section, vaddr, size, paddr, perm_write, perm_exec, asid, island, infile):
        # Round beginning down to 4K boundary
        size += (vaddr & 0xFFF)
        paddr -= (vaddr & 0xFFF)
        vaddr -= (vaddr & 0xFFF)
        # Convert size to page count
        pgcnt = (size + 0xFFF) >> 12
        if (paddr & 0xFFF) != 0:
            raise Exception('Virtual/physical address mis-alignment')
        vpage = vaddr >> 12
        ppage = paddr >> 12
        tmp = HexagonMapping()
        tmp.G = int(asid == 0)
        tmp.A1A0 = 0
        tmp.ASID = asid
        tmp.X = int(perm_exec)
        tmp.W = int(perm_write)
        tmp.R = 1
        tmp.U = int(getattr(section,'user_mode',asid != 0))
        tmp.C = int(getattr(section,'cache_policy','7'))    # Default L2 cacheable
        tmp.vpage = vpage
        tmp.ppage = ppage
        tmp.size = pgcnt
        tmp.locked = getattr(section,'locked',(asid==0))
        tmp.reclaim = getattr(section,'reclaim',False)
        tmp.mapping_group = section.mapping_group
        tmp.island = island
        tmp.reloc = self.relocatable_image.value
        tmp.infile = infile
        tmp.overlay = section.overlay
        tmp.physpool = getattr(section,'physpool',None)
        tmp.sname = section.name
        tmp = tmp.fix_to_TLB_entries(maxsize = self.max_elf_tlb.value/0x1000)
        self.all_mappings.extend(tmp)
    def sort_mappings(self):
        """Sort the all_mappings array."""
        for t in self.all_mappings:
            t.sortkey = (t.vpage << 32) + (t.size)
        self.all_mappings.sort(key=lambda x: x.sortkey)
    def get_ranges(self):
        """Compute the list of virtual address ranges to be mapped."""
        current_range = None
        current_group = None
        all_ranges = []
        for t in self.all_mappings:
            if t.mapping_group != current_group:
                if current_range:
                    all_ranges.append(current_range)
                current_range = [t.vpage, t.vpage+t.size, [t]]
                current_group = t.mapping_group
            elif t.vpage <= current_range[1]:
                current_range[1] = max(current_range[1], t.vpage+t.size)
                current_range[2].append(t)
            else:
                all_ranges.append(current_range)
                current_range = [t.vpage, t.vpage+t.size, [t]]
        if current_range:
            all_ranges.append(current_range)
        self.all_ranges = all_ranges
    def get_map(self, vaddr, size, mappings, verbose=False):
        # Get the list of mappings that overlap the proposed (vaddr,size)
        overlap = []
        for t in mappings:
            if t.vpage + t.size <= vaddr:
                continue
            if t.vpage >= vaddr + size:
                continue
            overlap.append(t)
        if not overlap:
            raise Exception('Internal error in image builder -- hole found in range')
        if verbose:
            print 'XML info:'
            print self.xml.toxml('utf-8')
            print 'Conflicting mapping requests:'
            for t in overlap:
                print '*** %s' % t
        from copy import copy
        proposed = copy(overlap[0])
        proposed.ppage += (vaddr - proposed.vpage)
        proposed.vpage = vaddr
        proposed.size = size
        for t in overlap[1:]:
            overs = sorted([proposed.overlay, t.overlay])
            if overs[1] == 1 and overs[0] < 1:
                return False
            if max(overs) == 1:
                continue                 # All overlays are compatible with each other
            if proposed.vpage - proposed.ppage != t.vpage - t.ppage:
                return False
            if proposed.physpool != t.physpool:
                if t.physpool == '*':
                    pass
                elif proposed.physpool == '*':
                    proposed.physpool = t.physpool
                else:
                    return False
            if proposed.G != t.G:
                return False
            if proposed.A1A0 != t.A1A0:
                return False
            if proposed.ASID != t.ASID:
                return False
            if proposed.R != t.R:
                return False
            if proposed.C >= 0 and t.C >= 0 and proposed.U != t.U:
                return False
            if proposed.C >= 0 and t.C >= 0 and proposed.C != t.C:
                return False
            if proposed.locked != t.locked:
                pass       # Allow mixing locked and unlocked for now
            if proposed.mapping_group != t.mapping_group:
                return False
            if proposed.island != t.island:
                return False
            if proposed.reloc != t.reloc:
                return False
            if self.allow_map_wx.value != 'true':
                if proposed.X and t.W:
                    return False
                if proposed.W and t.X:
                    return False
            if t.W:
                proposed.W = True
            if t.X:
                proposed.X = True
            if t.locked:
                proposed.locked = True
            if not t.reclaim:
                proposed.reclaim = False
            proposed.C = max(proposed.C, t.C)
            proposed.overlay = max(overs)
        return proposed
    def get_opt_mappings(self):
        for a in self.all_ranges:
            base = a[0]
            nsize = a[1]-a[0]
            while nsize:
                best_map = None
                sz_lo = 0
                sz_hi = nsize
                # Do binary search on the size to find the biggest size
                #  that actually works
                while sz_hi > sz_lo:
                    tmp = (sz_hi + sz_lo + 1) / 2
                    v = self.get_map(base, tmp, a[2])
                    if v:
                        best_map = v
                        sz_lo = tmp
                    else:
                        sz_hi = tmp - 1
                if best_map == None:
                    self.get_map(base, tmp, a[2], verbose=True)
                    ex_str = 'Cannot find compatible mapping for 4K page at virtual address %X' % (base << 12)
                    raise Exception(ex_str)
                if best_map.locked or best_map.island:
                    # Locked mappings must be actually achievable in hardware
                    tmp = 0x1000
                    if best_map.reloc:
                        tmp = self.relocatable_image.align
                    while True:
                        if tmp <= sz_lo:
                            if (best_map.vpage & (tmp-1)) == 0:
                                if (best_map.ppage & (tmp-1)) == 0:
                                    break
                                if best_map.island:
                                    # If physical page is wrong on an island mapping now, it'll get fixed later
                                    #  when we actually allocate space for it in the island.
                                    break
                        tmp = tmp/4
                    sz_lo = tmp
                    best_map.size = tmp
                    best_map = self.get_map(best_map.vpage, best_map.size, a[2])
                    best_map.mapping_group = a[2][0].mapping_group
                base += sz_lo
                nsize -= sz_lo
                self.opt_mappings.append(best_map)
    def lookup_vma_to_lma(self, vaddr):
        for p in self.phdr:
            if vaddr >= p.p_vaddr and vaddr < p.p_vaddr+p.p_memsz:
                ret = p.p_paddr + (vaddr - p.p_vaddr)
                return ret
        raise Exception('Error finding LMA for VMA %X' % vaddr)
    def move_pool_mappings(self):
        w = []
        for t in self.opt_mappings:
            if t.physpool and t.physpool != '*':
                w.append(t)
        for t in w:
            tmp = (t.vpage | t.size)
            tmp = (tmp & -tmp)
            t.sortkey = (-tmp, t.vpage)
        w.sort(key=lambda x: x.sortkey)
        for t in w:
            pp = [p for p in self.pools if p.name == t.physpool]
            if len(pp) != 1:
                raise Exception('Error assigning mapping to pool %s' % t.physpool)
            p = pp[0]
            newpage = p.alloc_pages(t.size, t.size)
            t.ppage = newpage
            t.reloc = False
            orgaddr = self.lookup_vma_to_lma(t.vpage << 12) + (self.virt_base - self.phys_base)
            self.island_moves.append([orgaddr, newpage << 12, t.size << 12])
    def move_island_mappings(self):
        # Get the list of island mappings
        w = []
        self.island_pool_dynamic_usage = 0
        for t in self.opt_mappings:
            if t.island:
                if (t.ASID == 0 or t.W == 0 or t.infile.restart == 0):
                    w.append(t)
                else:
                    self.island_pool_dynamic_usage += t.size
        # Set a sort key for each island mapping which is the smallest
        #  power of two that divides both the base address and the size
        for t in w:
            tmp = (t.vpage | t.size)
            tmp = (tmp & -tmp)
            t.sortkey = tmp
        w.sort(key=lambda x: -x.sortkey)
        for t in w:
            moved = False
            for m in self.phys_island_regions:
                if not moved:
                    if (m[0] & (t.sortkey-1)) == 0:
                        # Alignment checks out
                        if m[0]+t.size <= m[1]:
                            orgaddr = self.lookup_vma_to_lma(t.vpage << 12) + (self.virt_base - self.phys_base)
                            # Size checks out
                            t.ppage = m[0]
                            t.C = m[2] & 0xF
                            t.reloc = ((m[2] & 0x10) != 0)
                            if t.reloc:
                                use_dest = (t.ppage << 12) + self.virt_base + 1
                                t.ppage += (self.phys_base >> 12)
                            else:
                                use_dest = t.ppage << 12
                            m[0] += t.size
                            m[2] += t.size << 16
                            moved = True
                            self.island_moves.append([orgaddr, use_dest, t.size << 12])
            if not moved:
                raise Exception('Problem with fitting all island sections into the island')
    def phys_island_report(self):
        total_bytes_copied = 0
        print 'BOOT island copies'
        for x in self.island_moves:
            #print 'Vaddr %08X -> Paddr %08X, size %X' % (x[0], x[1], x[2])
            total_bytes_copied += x[2]
        total_remaining_region = 0
        for x in self.phys_island_regions:
            total_remaining_region += x[1]-x[0]+1
        total_remaining_region -= self.island_pool_dynamic_usage
        print '*' * 60
        print 'Total island space copied at boot:      %4dK (%d bytes)' % (total_bytes_copied / 1024, total_bytes_copied)
        print 'Total island space used for spawn:      %4dK (%d bytes)' % (self.island_pool_dynamic_usage * 4, self.island_pool_dynamic_usage * 4096)
        print 'Total island space left in island pool: %4dK (%d bytes)' % (total_remaining_region * 4, total_remaining_region * 4096)
        print '*' * 60
        if total_remaining_region < 0:
            raise Exception('Problem with fitting all island sections into the island')
        if total_remaining_region*4096 < self.free_uimg:
            raise Exception('Failing build due to assert_free_microimage failure\nFree space is less than the requested %s bytes' % self.free_uimg)
    def can_be_reclaimed(self, map):
        # Return false if the map overlaps with any non-reclaim mappings
        # Return true otherwise
        for t in self.all_mappings:
            if t.overlaps(map):
                if not t.reclaim:
                    if t.C >= 0:
                        return False
        return True
    def ramfs_populate(self):
        if not self.ramfs:
            return
        from os.path import getsize, basename
        from struct import pack
        fsnames = ''
        for x in self.ramfs:
            if '=' in x[0]:
                runtime_file_name, build_file_name = x[0].split('=',1)
            else:
                runtime_file_name, build_file_name = (basename(x[0]), x[0])
            sz = getsize(x[0])
            f = open(x[0],'rb')
            paddr = self.physpool_space.find_optimal(sz,extra_instructions=True).addr
            vaddr = self.vaddr_space.find_optimal(sz,extra_instructions=True).addr
            self.add_segment(vaddr, paddr, sz, f);
            self.ramfs_info.append([len(fsnames), vaddr, sz, 1])
            fsnames += runtime_file_name + '\0'
            map = HexagonMapping()
            map.size = alignup(sz,0x1000) >> 12
            map.vpage = vaddr >> 12
            map.ppage = paddr >> 12
            map.locked = False
            map.reloc = self.relocatable_image.value
            map.mapping_group = UniqueString()
            map.G = 1
            map.A1A0 = 0
            map.ASID = 0
            map.X = 0
            map.W = 0
            map.R = 1
            map.U = 1
            map.C = 7
            self.extra_mappings.extend(map.fix_to_TLB_entries(maxsize = self.max_elf_tlb.value/0x1000))
        #
        #  Each directory entry takes 16 bytes:
        #   Name (stored as offset into fsnames, replaced with pointer at run time)
        #   Virtaddr (the pagetable basically *is* our "FAT" equivalent)
        #   Size (number of bytes)
        #   Flags (1 means a valid file; 0 means end of list)
        #
        ramfs_dir = ''
        for x in self.ramfs_info:
            x[0] += 16*(1+len(self.ramfs_info))
        for x in self.ramfs_info:
            ramfs_dir += pack('<LLLL', *x)
        ramfs_dir += pack('<LLLL', 0, 0, 0, 0)
        ramfs_dir += fsnames
        sz = len(ramfs_dir)
        paddr = self.physpool_space.find_optimal(sz, extra_instructions=True).addr
        vaddr = self.vaddr_space.find_optimal(sz, extra_instructions=True).addr
        import StringIO
        f = StringIO.StringIO(ramfs_dir)
        self.add_segment(vaddr, paddr, sz, f)
        map = HexagonMapping()
        map.size = alignup(sz,0x1000) >> 12
        map.vpage = vaddr >> 12
        map.ppage = paddr >> 12
        map.locked = False
        map.reloc = self.relocatable_image.value
        map.mapping_group = UniqueString()
        map.G = 1
        map.A1A0 = 0
        map.ASID = 0
        map.X = 0
        map.W = 0
        map.R = 1
        map.U = 1
        map.C = 7
        print 'vaddress = %X' % vaddr
        self.extra_mappings.extend(map.fix_to_TLB_entries(maxsize = self.max_elf_tlb.value/0x1000))
        if self.qurtos_ramfs_directory:
            self[(self.qurtos_ramfs_directory, 4)] = ('<L', vaddr)
    def shrink_to_default_physpool(self):
        self.physpool_space += AllocatedRange(left = -QurtInfinity,
                                              right = self.physaddr)
        for pool in self.pools:
            if pool.name == "DEFAULT_PHYSPOOL":
                self.physpool_space += AllocatedRange(left = self.physaddr + (pool.ranges[0][1] << 12),
                                                      right = QurtInfinity)
    def gap_recovery(self):
        for t in self.opt_mappings:
            if t.C < 0:
                continue
            if not t.locked:
                continue
            perms = dict()
            if t.R:
                perms['R'] = True
            if t.W:
                perms['W'] = True
            if t.X:
                perms['X'] = True
            if t.U:
                perms['U'] = True
            self.vaddr_space += AllocatedRange(addr = t.vpage << 12,
                                               size = t.size << 12,
                                               reserve_only = True)
            self.physpool_space += QurtAddressRange(addr=t.ppage << 12,
                                                    size=t.size << 12,
                                                    **perms)
        for x in self.memsectiongaps:
            x.process()
    def finish_extra_mappings(self):
        if not self.extra_mappings:
            return
        #
        # Generate the list of instructions.
        # This will be a list of relocatable mappings followed by a zero entry,
        #  followed by a list of absolute mappings followed by a zero entry.
        # Finally, after those, there is one more word, which is the total
        #  size of the mapping used for the instructions, in bytes.  This will
        #  usually be 4K, but might be larger in the future.
        #
        import struct
        x = ''
        physadjust = (self.physaddr >> 11)
        for e in self.extra_mappings:
            if not e.locked and e.reloc:
                x += struct.pack('<Q', e.get_TLB_entry() - physadjust)
        x += struct.pack('<Q', 0)
        for e in self.extra_mappings:
            if not e.locked and not e.reloc:
                x += struct.pack('<Q', e.get_TLB_entry())
        x += struct.pack('<Q', 0)
        sz = 0x1000
        while sz<len(x)+4:
            sz *= 4
        x += struct.pack('<L', sz)
        vaddr = self.vaddr_space.find(sz,sz,extra_instructions=True).addr
        paddr = self.physpool_space.find(sz,sz,extra_instructions=True).addr
        p = self.phdr[0].copy()
        p.p_type = p.PT_LOAD
        p.p_offset = 0
        p.p_vaddr = vaddr
        p.p_paddr = paddr
        p.p_filesz = len(x)
        p.p_memsz = len(x)
        p.p_flags = 4
        p.p_align = sz
        s = self.shdr[0].copy()
        s.name = '.qibextra'
        s.sh_type = s.SHT_PROGBITS
        s.sh_flags = s.SHF_ALLOC
        s.sh_addr = vaddr
        s.sh_offset = 0
        s.sh_size = len(x)
        s.sh_link = s.SHN_UNDEF
        s.sh_info = 0
        s.sh_addralign = 8
        s.sh_entsize = 0
        s.user_mode = False
        s.cache_policy = 7
        s.locked = True
        s.reclaim = True
        s.overlay = 0
        s.physpool = None
        p.section_list = [s]
        self.phdr.append(p)
        self.shdr.append(s)
        import StringIO
        p.f = StringIO.StringIO(x)
        #
        #  Start slightly hacky code.
        #
        self.add_elf_mapping(s,vaddr,sz,paddr,0,0,0,0,0)
        self.opt_mappings.append(self.all_mappings.pop())
        if self.qurtos_extra_instructions == 0:
            raise Exception('Cannot complete advanced memory instructions -- missing symbol')
        self[(self.qurtos_extra_instructions, 4)] = ('<L', vaddr)

    def populate_output_file(self):
        infiles = self.infiles
        firstfile = infiles[0]
        self.phdr = []
        for i in infiles:
            for p in i.phdr:
                np = p.copy()
                p.outcopy = np
                if np.p_paddr >= i.vmem_bot and np.p_paddr <= i.vmem_top:
                    np.p_paddr = i.output_physaddr + (np.p_paddr - i.vmem_bot)
                if not getattr(np, 'temp', False):
                    self.physpool_space += AllocatedRange(addr=np.p_paddr,
                                                          size=np.p_memsz,
                                                          phdr=np)
                self.phdr.append(np)
                for s in np.section_list:
                    s.phdr_link = np
                np.section_list = []
        self.shdr = []
        for i in infiles:
            for s in i.shdr:
                if s.sh_type != s.SHT_NULL:
                    if (s.sh_flags & s.SHF_ALLOC) != 0 or s.name == '.comment':
                        sp = s.copy()
                        self.shdr.append(sp)
                        if not i is firstfile:
                            sp.name = s.name+'.'+i.appname
                        if getattr(sp,'phdr_link',None):
                            sp.phdr_link.section_list.append(sp)
        self.ehdr = firstfile.ehdr.copy()
        self.ehdr.e_entry = firstfile.output_physaddr
    def set_root_prog_no(self):
        if self.QURTK_root_prog_no:
            current_value = self.unpack_from('<i', self.QURTK_root_prog_no)[0]
            if current_value < -1:
                new_value = self.program_maps.get(self.infiles[0].appname,-1)
                self[(self.QURTK_root_prog_no, 4)] = ('<i', new_value)
    def ProcessFile(self):
        infiles = self.infiles
        physaddr = self.physaddr
        self.phys_base = physaddr
        # Find the lowest virtual address in the os image
        base_addr = infiles[0].start_addr
        self.virt_base = base_addr
        if self.CONFIG_ADVANCED_MEMORY:
            self.read_boot_mappings()
            self.read_tlb_dump()
            self.read_mmap_table()
            self.read_pool_configs()
            for t in self.vrlist:
                self.vaddr_space.alloc(left=(t[0] << 12),
                                       right=((t[1]+1) << 12),
                                       reserve_only=True)
            for t in self.boot_mappings:
                self.vaddr_space.alloc(left=t[1],
                                       right=t[2]-1,
                                       reserve_only=True,
                                       boot_mapping=t[0])
            for pool in self.pools:
                for r in pool.ranges:
                    if pool.name != "DEFAULT_PHYSPOOL":
                        if (r[1] << 12) < 0x40000000:
                            self.vaddr_space.alloc(addr=(r[0] << 12),
                                                   size=(r[1] << 12),
                                                   reserve_only=True,
                                                   idempotent_avoidance=True)
            self.synthesize_elf_mappings(infiles)
            self.sort_mappings()
            self.get_ranges()
            self.get_opt_mappings()
            self.move_pool_mappings()
            if self.phys_island_regions:
                self.move_island_mappings()
            self.shrink_to_default_physpool()
            self.ramfs_populate()
            self.gap_recovery()
            self.finish_extra_mappings()
            reclaim_mask = 0L
            reclaim_index = 1L
            v = []
            vn = []
            print '*' * 60
            print 'BOOT TLB mappings'
            for t in self.opt_mappings:
                if t.locked and t.reloc and t.overlay != 1 and t.C >= 0:
                    reclaim = self.can_be_reclaimed(t)
                    reclaim_msg = ('',' unlock')[reclaim]
                    t.ppage -= (self.phys_base >> 12)
                    print '%05X000 -> BASE  +  %05X000, length %05X000, perms X:%u W:%u R:%u U:%u C:%X%s' % (t.vpage, t.ppage, t.size,
                                                                                                             t.X, t.W, t.R, t.U, t.C,
                                                                                                             reclaim_msg)
                    v.append(t.get_TLB_entry())
                    if reclaim:
                        reclaim_mask += reclaim_index
                    reclaim_index *= 2
            for t in self.opt_mappings:
                if t.locked and not t.reloc and t.overlay != 1 and t.C >= 0:
                    reclaim = self.can_be_reclaimed(t)
                    reclaim_msg = ('',' unlock')[reclaim]
                    print '%05X000 -> Physical %05X000, length %05X000, perms X:%u W:%u R:%u U:%u C:%X%s' % (t.vpage, t.ppage, t.size,
                                                                                                             t.X, t.W, t.R, t.U, t.C,
                                                                                                             reclaim_msg)
                    vn.append(t.get_TLB_entry())
                    if reclaim:
                        reclaim_mask += reclaim_index
                    reclaim_index *= 2
            self.do_tlb_compare(v,vn)
            self.write_tlb_dump(v,vn)
            self.write_tlb_reclaim(reclaim_mask)
            print '*' * 60
            print 'reclaim_mask is %X' % reclaim_mask
            for x in self.island_moves:
                print 'Vaddr %08X -> Paddr %08X, size %X' % (x[0], x[1], x[2])
            if self.phys_island_regions:
                addr = self.eip_addr_phys_island + 8
                for m in self.phys_island_regions:
                    self[(addr,4)] = ('<L', m[2])                # Write out the new third word with the number of
                                                                 #  pages used in the high half
                    addr += 12
                self.write_quick_mmu(infiles[0])
                self.phys_island_report()
        self.eip.writeback(None, infiles, self.island_moves, self.opt_mappings, seg_optimize)
    def setup_elf_sections(self):
        #
        # We've collected all of the ELF sections.  Now make a list
        #  of all of those which actually take up address space,
        #  sorted by address.
        #
        self.alloc_sections_sorted = []
        for a in self.alloc_sections:
            self.alloc_sections_sorted.extend(a)
        self.alloc_sections_sorted.sort(key=lambda s:s.sh_addr)
    def extract_qurt_info(self):
        from lib.elf_info_patch import ElfInfoPatch
        section_names_to_try = ('.qskernel_eip', '.qskernel_main', '.start')
        for N in section_names_to_try:
            for s in self.alloc_sections[0]:
                if s.name == N:
                    parse_build_elements(*ElfInfoPatch().Parse(self, s.sh_addr, s.sh_size))
                    return
    def extract_memsections(self):
        lock_count = 0
        lockloc = getattr(self, 'QURTK_tlblock_entries', 0)
        if lockloc:
            lock_count = self.unpack_from('<L', lockloc)[0]
        print 'lock_count == %X' % lock_count
        addr = getattr(self, 'qurtos_mmap_table', 0)
        if addr:
            data, fmt = (), '<Q'
            while data[-1:] != (0,):
                data, fmt = self.unpack_from(fmt, addr), fmt+'Q'
            #
            #  Create sections for each TLB entry that we read.
            #
            for TLB in data[:-1]:
                print '%016X' % TLB
                rsz = TLB & (-TLB)
                sz = 0x1000 * rsz * rsz
                vaddr = ((TLB >> 32) & 0xFFFFF) << 12
                paddr = (((TLB-rsz) >>  1) & 0x7FFFFF) << 12
                paddr |= (((TLB-rsz) >> 61) & 1) << 35
                s = QIBSection(None,
                               name = '',
                               physaddr = QIBPhysAddr(paddr),
                               G = ConfigField(bool((TLB >> 62) & 1)),
                               A1A0 = ConfigField(int((TLB >> 59) & 3)),
                               ASID = ConfigField(int((TLB >> 52) & 0x7F)),
                               X = ConfigField(bool((TLB >> 31) & 1)),
                               W = ConfigField(bool((TLB >> 30) & 1)),
                               R = ConfigField(bool((TLB >> 29) & 1)),
                               U = ConfigField(bool((TLB >> 28) & 1)),
                               C = ConfigField(int((TLB >> 24) & 0xF)),
                               memsection = True,
                               locked = ConfigField(list(data).index(TLB) < lock_count),
                               root_elf = True,
                               sh_name = 0,
                               sh_type = QIBSection.SHT_NOBITS,
                               sh_flags = QIBSection.SHF_ALLOC + QIBSection.SHF_INTERNAL_ONLY,
                               sh_addr = vaddr,
                               sh_offset = None,
                               sh_size = sz,
                               sh_link = QIBSection.SHN_UNDEF,
                               sh_info = 0,
                               sh_addralign = 0,
                               sh_entsize = 0)
                self.alloc_sections[0].append(s)
                self.alloc_sections_sorted.append(s)
                print '%X %X %X' % (s.sh_addr, int(s.physaddr), s.sh_size)
            self.alloc_sections_sorted.sort(key=lambda s:s.sh_addr)
    def extract_island_config(self):
        self.phys_island_regions = []
        addr = getattr(self, 'QURTK_phys_island', 0)
        if addr:
            data, fmt = (), '<L'
            while data[-1:] != (0xFFFFFFFF,):
                data, fmt = self.unpack_from(fmt, addr), fmt+'LLL'
            while len(data) > 1:
                region = data[:3]
                print 'Island region at %05X000 - %05XFFF, cache policy %s' % region
                self.phys_island_regions.append(list(region))
                data = data[3:]
    def apply_xml_to_sections(self):
        #
        #  To every alloc section, apply all XML section
        #   attributes which are global.
        #
        for s in self.alloc_sections_sorted:
            for o in self.section_info:
                if o.match(o,None):
                    for v in o.attrs.values():
                        v(s)
        #
        #  To every alloc section, apply all XML section
        #   attributes which match the XML section name.
        #
        for s in self.alloc_sections_sorted:
            for o in self.section_info:
                if o.match(o,s.name):
                    for v in o.attrs.values():
                        v(s)
        #
        #  A few other legacy things here to do to
        #   every alloc section...
        #
        for s in self.alloc_sections_sorted:
            mapattr = getattr(s,'mapping','normal')
            s.overlay = int(mapattr == 'overlay')
            s.need_map = True

    def allocate_phys_space(self):
        #
        #  For all alloc sections, allocate physical
        #   space for them.
        #
        kernel_sections = []
        root_sections_by_name = {}
        physaddr_current = QIBPhysAddr(0, True, self.physaddr)         # Relocatable, start at beginning of load image
        for a in self.alloc_sections:
            for s in self.alloc_sections_sorted:
                if s in a:
                    if hasattr(s,'memsection'):
                        continue
                    if s.root_elf:
                        if s.name.startswith('.qskernel_'):
                            kernel_sections.append(s)
                            continue
                        root_sections_by_name[s.name] = s
                    s.physaddr = physaddr_current.align_up_to_vaddr(s.sh_addr, 0x100000)   # Align up to 1MB boundary
                    physaddr_current = s.physaddr + s.sh_size
        for s in kernel_sections:
            lookup_names = []
            if s.name == '.qskernel_main':
                lookup_names.append('.start')
            lookup_names.append(s.name.replace('.qskernel','.qskshadow',1))
            s.physaddr = root_sections_by_name[lookup_names[0]].physaddr
            for N in lookup_names:
                root_sections_by_name[N].sh_flags |= QIBSection.SHF_INTERNAL_ONLY
                root_sections_by_name[N].sh_flags |= QIBSection.SHF_MAP_CAN_BE_SKIPPED

    def try_mapping(self,addr,sz):
        map = QIBSection(None,
                         name = '',
                         physaddr = None,
                         p_minus_v = DontCare,                  # Don't know the P->V offset yet
                         G = DontCare,                          # Don't know yet if we're global
                         A1A0 = DontCare,                       # Don't have bus attributes yet
                         ASID = DontCare,                       # Don't have an ASID yet
                         X = DontCare,                          # Don't know yet if we're executable
                         W = DontCare,                          # Don't know yet if we're write-enabled
                         R = DontCare,                          # Don't know yet if we're readable
                         U = DontCare,                          # Don't know yet if we're user-mode visible
                         C = ConfigField(default=7),            # Don't have cache attributes yet, but default to 7
                         sh_name = 0,
                         sh_type = QIBSection.SHT_NULL,
                         sh_flags = QIBSection.SHF_TLB_MAPPING,
                         sh_addr = addr & ~(sz-1),
                         sh_offset = None,
                         sh_size = sz,
                         sh_link = QIBSection.SHN_UNDEF,
                         sh_info = 0,
                         sh_addralign = 0,
                         sh_entsize = 0)

        gb=[]
        for s in self.alloc_sections_sorted:
            if not map.merge(s,gb):
                return None

        if len(gb):
            gb[0] &= ~0xFFF             # Round down to page boundary
            gb[1] |= 0xFFF              # Round up to page boundary
            if (gb[0] ^ gb[1]) < sz/4:
                return None

        map.physaddr = map.p_minus_v.value() + map.sh_addr

        poffset = int(map.p_minus_v.value())

        if map.p_minus_v.value().irelocatable:
            #
            #  If it's relocatable, anything > 1MB won't work
            #
            if sz > 0x100000:
                return None

        if int(map.p_minus_v.value()) & (sz-1):
            #
            #  The physical and virtual offset won't work with this size
            #
            return None

        return map

    def virt_to_physload(self, addr):
        #
        #  Given a virtual address, return its assigned physical load address.
        #  May return None if the virtual address doesn't have an associated
        #   physical load address.
        #
        for s in self.alloc_sections_sorted:
            offset = addr - s.sh_addr
            if offset >= 0 and offset < s.sh_size:
                return s.physaddr + offset
        return None

    def set_kernel_boundaries(self):
        first_vpage = QurtInfinity
        last_vpage = -QurtInfinity
        first_ppage = QurtInfinity
        last_ppage = -QurtInfinity
        for s in self.section_list:
            if s.root_elf:
                if s.name.startswith('.qskernel_'):
                    if s.sh_flags & s.SHF_ALLOC:
                        first_vpage = min(first_vpage, s.sh_addr >> 12)
                        last_vpage = max(last_vpage, (s.sh_addr + s.sh_size - 1) >> 12)
        for s in self.alloc_sections_sorted:
            if s.root_elf:
                if s.name.startswith('.qskernel_'):
                    first_ppage = min(first_ppage, int(s.physaddr) >> 12)
                    last_ppage = max(last_ppage, (int(s.physaddr) + s.sh_size - 1) >> 12)
        self[(self.QURTK_vprotect_start,4)] = ('<L', first_vpage)
        self[(self.QURTK_vprotect_end,4)] = ('<L', last_vpage + 1)
        self[(self.QURTK_pprotect_start,4)] = ('<L', first_ppage)
        self[(self.QURTK_pprotect_end,4)] = ('<L', last_ppage + 1)

    def add_trace_buffer_map(self):
        if self.QURTK_tbb:
            trace_buffer_bounds = self.unpack_from('<LL', self.QURTK_tbb)
            print 'Trace buffer starts at %X' % trace_buffer_bounds[0]
            print 'Trace buffer ends   at %X' % trace_buffer_bounds[1]
            for s in self.section_list:
                if s.name == '.qskernel_vspace':
                    tbphys_lo = self.virt_to_physload(trace_buffer_bounds[0])
                    tbphys_hi = self.virt_to_physload(trace_buffer_bounds[1]-1)
                    tbalign = 0x1000
                    # Keep increasing tbalign until both tbphys_lo and tbphys_hi
                    #  can fit in the same alignment block or until we're at
                    #  1MB alignment; no sense in going beyond 1MB.
                    while tbalign < 0x100000 and (int(tbphys_lo) ^ int(tbphys_hi)) >= tbalign:
                        tbalign *= 4
                    print 'Trace buffer physical address: %X' % int(tbphys_lo)
                    print 'Trace buffer physical address: %X' % int(tbphys_hi)
                    print 'Trace buffer valign          : %X' % tbalign
                    missing = (int(tbphys_lo) - s.sh_addr) & (tbalign-1)
                    missing += tbalign
                    tbsize = trace_buffer_bounds[1] - trace_buffer_bounds[0]
                    tbvaddr = s.sh_addr + missing
                    #
                    #  Add a new mapping for the kernel trace buffers
                    #
                    ns = QIBSection(None,
                                    name = '',
                                    physaddr = tbphys_lo,
                                    G = ConfigField(True),
                                    A1A0 = ConfigField(0),
                                    ASID = ConfigField(0),
                                    X = ConfigField(False),
                                    W = ConfigField(False),
                                    R = ConfigField(False),
                                    U = ConfigField(False),
                                    C = ConfigField(6),          # UNCACHED
                                    memsection = True,
                                    locked = ConfigField(True),
                                    root_elf = True,
                                    sh_name = 0,
                                    sh_type = QIBSection.SHT_NOBITS,
                                    sh_flags = QIBSection.SHF_ALLOC + QIBSection.SHF_INTERNAL_ONLY,
                                    sh_addr = tbvaddr,
                                    sh_offset = None,
                                    sh_size = tbsize,
                                    sh_link = QIBSection.SHN_UNDEF,
                                    sh_info = 0,
                                    sh_addralign = 0,
                                    sh_entsize = 0)
                    self.alloc_sections[0].append(ns)
                    self.alloc_sections_sorted.append(ns)
                    self.alloc_sections_sorted.sort(key=lambda s:s.sh_addr)
                    self[(self.QURTK_tbb,8)] = ('<LL', tbvaddr, tbvaddr+tbsize)
                    print 'kernel vspace is %X, size %X' % (s.sh_addr, s.sh_size)
                    print 'tb adjust is %X, size %X' % (missing, tbsize)
                    print 'tb virtual address is %X' % tbvaddr
                    s.sh_addr += missing + tbsize
                    s.sh_size -= missing + tbsize
                    if s.sh_size < 0:
                        raise Exception('Not enough reserved kernel virtual space for trace buffers')
                    break
            else:
                raise Exception('Cannot get virtual addresses for trace buffer')

    def optimize_mappings(self):
        #
        #  From our section list, come up with a set of optimized TLB
        #   mappings.
        #
        for s in self.alloc_sections_sorted:
            s.set_mapping_attributes()

        mapped_through = 0                      # We've mapped everything up to and including this address
        self.mappings = []
        while True:
            next_address = 0x100000000          # Provisional next address to map
            for s in self.alloc_sections_sorted:
                if s.sh_flags & s.SHF_MAP_CAN_BE_SKIPPED:
                    continue                    # Mapping this section is optional
                if s.sh_addr + s.sh_size <= mapped_through:
                    continue                    # We're already past this section
                if s.sh_addr <= mapped_through:
                    next_address = mapped_through
                    break                       # We need to map at "mapped_through"
                if next_address > s.sh_addr:
                    next_address = s.sh_addr
            if next_address == 0x100000000:
                break
            sz = 0x1000000                      # Try a 16MB mapping
            while sz >= 0x1000:
                map = self.try_mapping(next_address,sz)
                if map:
                    break
                sz /= 4
            if map == None:
                print '%X cannot map' % next_address
                raise Exception()
            self.mappings.append(map)
            mapped_through = map.sh_size + map.sh_addr
        all_mappings = ''
        for s in self.mappings:
            print '%X -> %X, size %X' % (s.sh_addr, int(s.physaddr), s.sh_size)
        for s in self.mappings:
            if s.locked.value():
                all_mappings += s.tlb_encoding(True)
        reloc_map_count = len(all_mappings)/8
        for s in self.mappings:
            if s.locked.value():
                all_mappings += s.tlb_encoding(False)
        print 'maps: %r' % all_mappings
        print 'reloc_map_count: %r' % reloc_map_count
        self[(self.QURTK_tlb_dump,len(all_mappings))] = all_mappings
        self[(self.QURTK_tlb_dump_relocs,4)] = ('<L', reloc_map_count)
        tlb_dump_physaddr = self.virt_to_physload(self.QURTK_tlb_dump)
        if tlb_dump_physaddr.irelocatable:
            self[(self.QURTK_quick_load_MMU,4)] = ('<L', int(tlb_dump_physaddr)+1)
        else:
            self[(self.QURTK_quick_load_MMU,4)] = ('<L', int(tlb_dump_physaddr))

    def last_fixups(self):
        if self.QURTK_hbi:
            if self.x__hexagon_bsp_init:
                hbi_physaddr = self.virt_to_physload(self.x__hexagon_bsp_init)
                if hbi_physaddr.irelocatable:
                    self[(self.QURTK_hbi,4)] = ('<L', int(hbi_physaddr)+1)
                else:
                    self[(self.QURTK_hbi,4)] = ('<L', int(hbi_physaddr))

    def eip_build(self):
        import struct
        try:
            for s in self.section_list:
                if s.name == '.qskernel_eip_build':
                    f = s.contents()
                    while True:
                        h = f.read(4)
                        if h == '':
                            break
                        hdr = struct.unpack('<HH', h)
                        if hdr == (0,0):
                            break
                        if hdr[0] < 16 or hdr[1] != 1:
                            raise Exception()
                        info = struct.unpack('<LL', f.read(8))
                        data = f.read(hdr[0]-12)
                        if info[1] > len(data):
                            raise Exception()
                        self[info] = data[:info[1]]
        except:
            print '.qskernel_eip_build section malformed'
            raise

    def build_phdrs(self):
        self.output_sections = [s for s in self.alloc_sections_sorted if not (s.sh_flags & s.SHF_INTERNAL_ONLY)]
        self.output_sections.sort(key=lambda s: s.physaddr)
        self.phdr = []
        p = None
        for s in self.output_sections:
            # See if we can add the section to the last program header we had
            if p and p.secure == s.SECURE and p.f == s.f:
                if s.sh_type == s.SHT_NOBITS:
                    # Can always add a NOBITS section
                    p.p_memsz = (s.sh_addr + s.sh_size) - p.p_vaddr
                    p.section_list.append(s)
                    continue
                else:
                    # Can add a PROGBITS section if the program header has
                    #  filesz == memsz and if the (vaddr-physaddr) matches
                    if p.p_filesz == p.p_memsz:
                        if (s.sh_addr - p.p_vaddr) == (s.physaddr - p.p_paddr):
                            p.p_memsz = (s.sh_addr + s.sh_size) - p.p_vaddr
                            p.p_filesz = p.p_memsz
                            p.section_list.append(s)
                            continue
            if s.sh_type == s.SHT_NOBITS:
                filesz = 0
            else:
                filesz = s.sh_size
            secureflags = 0
            if s.SECURE:
                secureflags = (1 << 21)
            p = Elf32ProgramHeader(None,
                                   p_type = Elf32ProgramHeader.PT_LOAD,
                                   p_offset = s.sh_offset,
                                   p_vaddr = s.sh_addr,
                                   p_paddr = s.physaddr,
                                   p_filesz = filesz,
                                   p_memsz = s.sh_size,
                                   p_flags = 4 + secureflags,
                                   p_align = 4096,
                                   secure = s.SECURE,
                                   temp = False,
                                   f = s.f)
            self.phdr.append(p)
            p.section_list = [s]
        for p in self.phdr:
            for s in p.section_list:
                if s.sh_flags & s.SHF_WRITE:
                    p.p_flags |= 2
                if s.sh_flags & s.SHF_EXECINSTR:
                    p.p_flags |= 1
        #
        #  Now fill the gaps so that all physical addresses are covered.
        #

        for pool in self.pools:
            if pool.name == "DEFAULT_PHYSPOOL":
                total_phys_size = pool.ranges[0][1] << 12
                p = Elf32ProgramHeader(None,
                                       p_type = Elf32ProgramHeader.PT_LOAD,
                                       p_offset = 0x1000,
                                       p_vaddr = 0,
                                       p_paddr = self.phdr[0].p_paddr + total_phys_size,
                                       p_filesz = 0,
                                       p_memsz = 1,
                                       p_flags = 4,
                                       p_align = 4096,
                                       secure = ConfigFalse,
                                       temp = True,
                                       f = self.phdr[0].f)
                p.section_list = []
                self.phdr.append(p)

        #
        #  Next expand each PHDR to either the start of the next PHDR
        #   or to the next 4K boundary, whichever comes first.
        #
        for left,right in zip(self.phdr[:-1],self.phdr[1:]):
            new_end_address = (left.p_paddr + left.p_memsz).align_up_to_vaddr(0,0x1000)
            new_end_address = min(new_end_address, right.p_paddr)
            left.p_memsz = new_end_address - left.p_paddr
            gapsize = right.p_paddr - new_end_address
            if gapsize:
                for s in self.section_list:
                    if s.name == '.qsvspace':
                        missing = (int(new_end_address) - s.sh_addr) & 0xFFF
                        vaddr = s.sh_addr + missing
                        s.sh_addr += gapsize + missing
                        s.sh_size -= gapsize + missing
                        if s.sh_size < 0:
                            s.sh_addr -= gapsize + missing
                            s.sh_size += gapsize + missing
                            #raise Exception('Not enough virtual address space in .qsvspace to fill gaps')
                            continue
                        print 'Adding PHDR with vaddr %X' % vaddr
                        p = Elf32ProgramHeader(None,
                                               p_type = Elf32ProgramHeader.PT_LOAD,
                                               p_offset = 0x1000,
                                               p_vaddr = vaddr,
                                               p_paddr = new_end_address,
                                               p_filesz = 0,
                                               p_memsz = gapsize,
                                               p_flags = 4,
                                               p_align = 4096,
                                               secure = ConfigFalse,
                                               temp = False,
                                               f = left.f)
                        self.phdr.append(p)
                        p.section_list = []
                        break
                else:
                    pass
                    #raise Exception('No .qsvspace section?')
        self.phdr.sort(key=lambda p: p.p_paddr)
        self.phdr.remove(self.phdr[-1])

    def write_output_file(self):
        #
        #  Write output file in this order:
        #   ELF header
        #   Program headers (only PT_LOAD, sorted by physical address)
        #   Section headers (index 0 is empty slot, index 1 is the string table, indexes 2 onward are alloc sections)
        #   Section string table (pointed to by index 1 of the section table)
        #   Padding to 4K boundary
        #   Output data, sorted by physical address, with appropriate padding between pieces.
        import shutil
        #
        #  Insert the special sections
        #
        self.output_sections.insert(0,QIBSection(None,
                                                 name = '',
                                                 sh_name = 0,
                                                 sh_type = QIBSection.SHT_NULL,
                                                 sh_flags = 0,
                                                 sh_addr = 0,
                                                 sh_offset = 0,
                                                 sh_size = 0,
                                                 sh_link = QIBSection.SHN_UNDEF,
                                                 sh_info = 0,
                                                 sh_addralign = 0,
                                                 sh_entsize = 0))
        self.output_sections.insert(1,QIBSection(None,
                                                 name = '.shstrtab',
                                                 sh_name = 0,
                                                 sh_type = QIBSection.SHT_STRTAB,
                                                 sh_flags = 0,
                                                 sh_addr = 0,
                                                 sh_offset = 0,
                                                 sh_size = 0,
                                                 sh_link = QIBSection.SHN_UNDEF,
                                                 sh_info = 0,
                                                 sh_addralign = 1,
                                                 sh_entsize = 0))
        #
        #  Build the string table
        #
        strtab = '\0'
        for s in sorted(self.output_sections, key=lambda s: -len(s.name)):
            zname = s.name + '\0'
            if strtab.find(zname) < 0:
                strtab += zname
            s.sh_name = strtab.find(zname)
        for s in self.output_sections[2:]:
            s.writedata = s.contents()
        #
        #  Calculate the number of bytes needed for the header portion up to and including the string table
        #
        output_location = 52                                    # ELF header
        self.ehdr.e_phoff = output_location
        self.ehdr.e_phnum = len(self.phdr)
        output_location += 32*len(self.phdr)                    # Program headers
        self.ehdr.e_shoff = output_location
        self.ehdr.e_shnum = len(self.output_sections)
        self.ehdr.e_shstrndx = 1
        output_location += 40*len(self.output_sections)         # Section headers
        self.output_sections[1].sh_offset = output_location
        self.output_sections[1].sh_size = len(strtab)
        output_location += len(strtab)                          # String table
        self.ehdr.e_entry = self.physaddr

        for p in self.phdr:
            p.p_paddr = p.p_paddr.basevalue()

        for p in self.phdr:
            offset = output_location
            alignment = 0x1000
            misalign = (output_location - int(p.p_paddr)) & (alignment-1)
            if misalign:
                offset += alignment-misalign
            p.p_offset = offset
            output_location = offset + p.p_filesz
            for s in p.section_list:
                s.sh_offset = p.p_offset + (s.sh_addr - p.p_vaddr)

        fo = open(self.outfilename,'wb')
        fo.write(self.ehdr.output())
        for p in self.phdr:
            fo.write(p.output())
        for s in self.output_sections:
            fo.write(s.output())
        fo.write(strtab)
        for s in self.output_sections[2:]:
            if s.sh_type != s.SHT_NOBITS:
                delta = s.sh_offset - fo.tell()
                if delta:
                    fo.write('\0' * delta)
                shutil.copyfileobj(s.writedata, fo)
        self.file = fo
        fo.flush()
        for t in self.deferred_writes:
            self[(t[0],len(t[1]))] = t[1]
        fo.flush()
        fo.close()

    def main(self, argv):
        self.parse_args(argv)
        self.setup_elf_sections()
        self.extract_qurt_info()
        self.extract_memsections()
        self.extract_island_config()
        self.read_pool_configs()
        self.apply_xml_to_sections()
        self.allocate_phys_space()
        self.set_kernel_boundaries()
        self.add_trace_buffer_map()
        self.optimize_mappings()
        self.last_fixups()
        self.eip_build()
        self.set_root_prog_no()
        self.build_phdrs()
        self.eip.writeback(None, self.infiles, self.island_moves, self.opt_mappings, self.mappings)
        self.write_output_file()
        return 0
        self.process_input_files()
        self.populate_output_file()
        self.ProcessFile()
        self.finish()
        if self.write_reloc:
            for f in self.infiles:
                srcname = f.rawfile.name
                tmp = srcname.rsplit('.',1)
                tmp[0] += '_reloc'
                dstname = '.'.join(tmp)
                from shutil import copyfile
                copyfile(srcname,dstname)
        return 0

if __name__ == '__main__':
    from lib.qurt import run_script
    run_script(QurtElfPatch().main)
