import os, re, inspect, subprocess
from qurt import *                # Make everything in qurt.py available
                                  #  to the included classes.

def token_combine(*args):
    args = [w for w in args if w]               # Remove anything which is not seen as "true" -- this removes
                                                #  None, False, 0, empty strings, etc.
    args = [w.strip() for w in args]            # Remove leading and trailing whitespace from the remaining strings
    args = [w for w in args if w]               # Remove strings which are empty after whitespace stripping
    ret = ' '.join(args)+'\n'
    return ret

AUTO = dict()                                   # Just a specific object representing AUTO so we can test
                                                #   things like "x is AUTO"...

class DeferredValue(object):
    def __init__(self, attrib):
        if attrib.startswith('<') and attrib.endswith('>'):
            attrib = '''cfg['%s']''' % attrib[1:-1]
        self.attrib = attrib
    def setcfg(self, cfg):
        self.cfg = cfg
    def getraw(self):
        cfg = self.cfg
        return eval(self.attrib)

class IntValue(DeferredValue):
    def __int__(self):
        raw = self.getraw()
        if isinstance(raw, basestring):
            raw = long(raw,0)
        return int(raw)
    def __str__(self):
        return str(int(self))
    def __bool__(self):
        return bool(int(self))

class StrValue(DeferredValue):
    def __str__(self):
        return str(self.getraw())
    def __bool__(self):
        return bool(str(self))

class BoolValue(DeferredValue):
    def __bool__(self):
        return bool(self.getraw())

class ConfigVar(object):
    already_defined = False

    def stdString(self, arg):
        if isinstance(arg, DeferredValue):
            return str(arg)
        if arg != None:
            if arg.startswith('<') and arg.endswith('>'):
                args = arg[1:-1].split(':',1)
                try:
                    arg = self.cfg[args[0]]
                except:
                    if len(args) == 1:
                        raise Exception('Cannot find %s in cfg' % args[0])
                    arg = args[1]
                return self.stdString(arg)
        return arg

    def stdInteger(self, arg):
        if arg != None:
            if not isinstance(arg, (int, long, bool)):
                arg = self.stdString(arg)
            if isinstance(arg, basestring):
                arg = int(arg, 0)
            else:
                arg = int(arg)
        return arg

    def stdBoolean(self, arg):
        if arg != None:
            if not isinstance(arg, (int, long, bool)):
                arg = self.stdString(arg)
            arg = bool(arg)
        return arg

    def __getattr__(self, name):
        #
        #  Implement the ability to have a dynamic function to get the
        #   attribute.  If self.name doesn't exist, but if self.get_name
        #   exists and is callable, call it and insert the return value
        #   into the object's attribute dictionary.
        #
        if not name.startswith('get_') and not name.startswith('__'):
            ret = getattr(self,'get_'+name,None)
            if ret == None:
                raise Exception('Cannot get attribute %s' % name)
            if ret != None:
                ret = ret()
            self.__dict__[name] = ret
            return ret
        raise AttributeError('Object of class %s has no attribute %s' % (self.__class__.__name__, name))

    def get_name(self):
        return self.__class__.__name__

    def get_depends(self):
        return ()

    def get_verbatim(self):
        return None

    def get_c_type(self):
        return 'unsigned int'

    def get_value(self):
        return None

    def get_value_alignup(self):
        return None

    def get_array_fill(self):
        return None

    def get_dimension(self):
        return None

    def get_dimension_alignup(self):
        return None

    def get_aligned(self):
        return None

    def get_static(self):
        return False

    def get_const(self):
        return self.stdString(self.value_actual) != None

    def get_secure(self):
        return False

    def get_export(self):
        return False

    def get_island(self):
        return False

    def get_weakref(self):
        return False

    def get_weakdef(self):
        return False

    def get_value_actual(self):
        #
        #  Return the actual string that will be used to initialize the C variable,
        #   or return None.
        #
        afill = self.stdString(self.array_fill)
        if afill != None:
            #
            #  There is an array fill request.  This overrides the other initialization
            #   mechanisms.
            #
            array_len = self.stdInteger(self.dimension_actual)
            return '{ [0 ... %d] = %s }' % (array_len-1, afill)

        roundup = self.stdInteger(self.value_alignup)
        if roundup != None:
            #
            #  There is an alignup request.
            #
            value = self.stdInteger(self.value)
            extra = value % roundup
            if extra:
                value += (roundup-extra)
            return '%d' % value

        return self.stdString(self.value)

    def get_dimension_actual(self):
        #
        #  Return the actual dimension subscript for the C variable,
        #   or return None.
        #

        dimension = self.stdInteger(self.dimension)
        if dimension == None or (dimension is AUTO):
            return dimension

        roundup = self.stdInteger(self.dimension_alignup)
        if roundup != None:
            #
            #  There is an alignup request.
            #
            extra = dimension % roundup
            if extra:
                dimension += (roundup-extra)

        return '%d' % dimension

    def get_section(self):
        #
        #  Return the actual section to be used for the definition.
        #

        if self.stdBoolean(self.export):
            return '.qskernel_exports'

        if self.stdBoolean(self.secure):
            #
            #  Pick a monitor mode section
            #
            if self.stdBoolean(self.island):
                sname = '_island'
            else:
                sname = '_main'

            if self.stdString(self.value_actual) == None:
                #
                # Uninitialized
                #
                align = self.stdInteger(self.aligned)
                if align != None and align >= 4096:
                    sname = '_zipages' + sname
                else:
                    sname = '_zi' + sname

            return '.qskernel' + sname

        if self.stdBoolean(self.const):
            return '.rodata.user.config'

        if self.stdString(self.value_actual) == None:
            return '.bss.user.config'

        return 'data.user.config'

    def get_declattributes(self):
        #
        #  Return the C attribute modifier for this variable.
        #

        attrs = []

        a = self.stdInteger(self.aligned)
        if a != None:
            attrs.append('aligned(%d)' % a)

        if self.stdBoolean(self.weakref):
            attrs.append('weak')

        s = self.stdString(self.section)
        if s != None:
            #
            #  Don't include the section in the header file.  Doing so
            #   is misleading, because the variable may not end up in
            #   the same section in the final link where it was placed
            #   during the configuration step.
            #
            if False:
                attrs.append('section("%s")' % s)

        if attrs:
            return '__attribute__((%s))' % ','.join(attrs)
        else:
            return None

    def get_defattributes(self):
        #
        #  Return the C attribute modifier for this variable.
        #

        attrs = []

        a = self.stdInteger(self.aligned)
        if a != None:
            attrs.append('aligned(%d)' % a)

        if self.stdBoolean(self.weakdef):
            attrs.append('weak')

        s = self.stdString(self.section)
        if s != None:
            attrs.append('section("%s")' % s)

        if attrs:
            return '__attribute__((%s))' % ','.join(attrs)
        else:
            return None

    def get_declaration(self, *args):
        #
        #  Return the actual declaration for the object
        #

        decl = self.stdString(self.verbatim)
        if decl != None:
            return decl

        if self.stdBoolean(self.static) == True:
            return ''

        decl = ['extern']
        decl.append(self.stdString(self.declattributes))
        if self.stdBoolean(self.const):
            decl.append('const')
        decl.append(self.stdString(self.c_type))
        decl.append(self.stdString(self.name))
        if (self.dimension is AUTO) or self.stdInteger(self.dimension) != None:
            decl.append('[]')
        decl.append(';')

        return ' '.join([a.strip() for a in decl if a != None])+'\n'

    def get_definition(self, *args):
        #
        #  Return the actual declaration for the object
        #

        decl = self.stdString(self.verbatim)
        if decl != None:
            return decl

        if self.dimension is AUTO:
            dimstring = '[]'
        else:
            dimension = self.stdInteger(self.dimension)
            if dimension == 0:
                return ''
            if dimension == None:
                dimstring = None
            else:
                dimstring = '[%d]' % dimension

        decl = []
        if self.stdBoolean(self.static) == True:
            decl.append('static')
        decl.append(self.stdString(self.defattributes))
        if self.stdBoolean(self.const):
            decl.append('const')
        decl.append(self.stdString(self.c_type))
        decl.append(self.stdString(self.name))
        decl.append(dimstring)
        value = self.stdString(self.value_actual)
        if value != None:
            decl.append('=')
            decl.append(value)
        decl.append(';')

        return ' '.join([a.strip() for a in decl if a != None])+'\n'

class ZConfigVar(object):
    def __getattr__(self, name):
        #
        #  Implement the ability to have a dynamic function to get the
        #   attribute.  If self.name doesn't exist, but if self.get_name
        #   exists and is callable, call it and insert the return value
        #   into the object's attribute dictionary.
        #
        if not name.startswith('get_') and not name.startswith('__'):
            ret = getattr(self,'get_'+name,None)
            if ret != None:
                ret = ret()
            self.__dict__[name] = ret
            return ret
        raise AttributeError('Object of class %s has no attribute %s' % (self.__class__.__name__, name))
    def get_name(self):
        return self.__class__.__name__
    def get_attributes(self):
        a = []
        if self.section:
            a.append('section("%s")' % self.section)
        if self.aligned:
            a.append('aligned(%u)' % self.aligned)
        if self.optional:
            a.append('weak')
        if a:
            return '__attribute__((%s))' % ','.join(a)
        else:
            return ''
    def get_basetype(self):
        return token_combine(self.c_type.replace('[]',''),
                             self.volatile and 'volatile',
                             self.const and 'const')
    def get_subscript_inexact(self):
        return ('[]' in self.c_type) and '[]'
    def get_basedeclaration(self):
        return token_combine(self.basetype,
                             self.name,
                             self.subscript_inexact)
    def get_declaration(self, *args):
        return token_combine(self.before_string,
                             'extern',
                             self.attributes,
                             self.basedeclaration,
                             ';',
                             self.after_string)

class XConfigVar(object):
    #
    #  Generic implementation of a QuRT configuration variable.
    #  Each actual variable should be a subclass of this
    #   defined in qurt_config_vars.py, overriding those
    #   attributes and methods needed for its own implementation.
    #
    #  Default fields:
    #
    c_type = 'unsigned int'       # Variables are unsigned int unless overridden
    section = None                # Variables go in default section unless overridden
    export_to_qurtos = False      # Variables are not exported unless overridden
    guest_os_only = False         # Variables are visible to kernel unless overridden
    guest_os_write = False        # Variables are not writeable by guest-OS unless overridden
    island_resident = False       # Variables are not island resident unless overridden
    aligned = None                # Variables get no alignment attribute unless overridden
    array_fill = None             # Arrays are not filled unless overridden
    dimension = None              # Variables have no dimension unless overridden
    before_string = ''            # Variables have no prerequisite unless overridden
    after_string = ''             # Variables have no postrequisite unless overridden
    optional = False              # Variables are mandatory definition unless overridden
    nobss = False                 # Variables can go in bss unless overridden

    def __init__(self):
        self.name = self.__class__.__name__
    def get_before_string(self, is_definition):
        if not self.before_string:
            return ''
        return self.before_string
    def get_after_string(self, is_definition):
        if not self.after_string:
            return ''
        return self.after_string
    def get_basetype(self):
        if '[]' in self.c_type:
            return self.c_type.replace('[]',' ').strip()
        return self.c_type
    def get_value(self):
        return None
    def get_initializer(self):
        if self.array_fill:
            return '= { [0 ... %u] = %s }' % (self.get_dimension()-1, self.array_fill)
        v = self.get_value()
        if v == None:
            return ''
        return '= %s' % v
    def get_subscript(self, exact):
        if not '[]' in self.c_type:
            return ''
        if not exact:
            return '[]'
        tmp = self.get_dimension()
        if tmp == None:
            return '[]'
        return '[%u]' % tmp
    def get_dimension(self):
        return self.dimension
    def get_section(self):
        if self.section:
            return self.section
        if self.island_resident:
            return '.data.island'
        if self.guest_os_only:
            return '.data.user.config'
        if self.nobss:
            return '.data'
        return self.section
    def get_attributes(self):
        a = []
        s = self.get_section()
        if s:
            a.append('section("%s")' % s)
        if self.aligned:
            a.append('aligned(%u)' % self.aligned)
        if self.optional:
            a.append('weak')
        if a:
            return '__attribute__((%s))' % ','.join(a)
        return ''
    def get_basedeclaration(self, exact):
        return '%s %s%s' % (self.get_basetype(), self.name, self.get_subscript(exact))
    def get_declaration(self, kernel):
        ret = ['extern',
               self.get_attributes(),
               self.get_basedeclaration(exact=False),
               ';']
        ret = [w.strip() for w in ret]
        ret = [w for w in ret if w]
        return self.get_before_string(False) + ' '.join(ret)+'\n' + self.get_after_string(False)
    def get_definition(self, kernel):
        if bool(kernel) == bool(self.guest_os_write):
            return ''
        if self.optional and not self.include_optional():
            return ''
        ret = [self.get_attributes(),
               self.get_basedeclaration(exact=True),
               self.get_initializer(),
               ';']
        ret = [w.strip() for w in ret]
        ret = [w for w in ret if w]
        return self.get_before_string(True) + ' '.join(ret)+'\n' + self.get_after_string(True)

#
#  Note that we set existing_keys and var_keys both to None
#   in order to make sure that they show up in the list of globals
#   both before and after pulling in the variables.
#

existing_keys = None
var_keys = None

#
#  Execute qurt_config_vars.py in the current namespace;
#   grab list of global keys both before and after.
#

existing_keys = globals().keys()
execfile(os.path.join(os.path.dirname(__file__),'qurt_config_vars.py'))
var_keys = globals().keys()

#
#  Get the list of new keys, which should be the variable names.
#

qurt_vars = []

for k in var_keys:
    if k not in existing_keys:
        if isinstance(k,basestring) and not k.startswith('_'):
            v = globals()[k]
            if inspect.isclass(v):
                if issubclass(v,ConfigVar):
                    qurt_vars.append(v())

class QurtVars(object):
    #
    #  Class gets attached to cfg as cfg.vars
    #     from genkernel import QurtVars
    #     QurtVars(cfg)
    #
    def __init__(self, cfg):
        self.cfg = cfg
        cfg.vars = self
        self.cdict = dict()
        for v in qurt_vars:
            v.cfg = cfg
            for N in dir(v):
                A = getattr(v,N)
                if isinstance(A, DeferredValue):
                    A.setcfg(cfg)
            setattr(cfg, v.name, v)
            self.cdict[v.__class__.__name__] = v
    def get_qurtos_exports(self):
        L = []
        for v in qurt_vars:
            if v.export_to_qurtos:
                L.append((v.get_basetype(), v.name+v.get_subscript(False)))
        return tuple(L)
    def fix_attributes(self):
        pass
    def get_one_declaration(self, v):
        ret = ''
        if not v.already_defined:
            for N in v.depends:
                print N
                ret += self.get_one_declaration(self.cdict[N])
            ret += v.get_declaration()
            v.already_defined = True
        return ret
    def get_declarations(self, kernel):
        print '=== %s' % self.cfg.compiler
        compiler_command = [self.cfg.compiler, '-E', '-P', '-DGEN_CONFIG_HEADER']
        for f in self.cfg.includes:
            compiler_command.extend(['--include', f])
        compiler_command.append(os.path.normpath(os.path.join(os.path.dirname(__file__),'..','Input','cust_config.c')))
        process = subprocess.Popen(compiler_command, stdout=subprocess.PIPE)
        h_file = process.communicate()
        if process.returncode:
            raise Exception('Error generating QuRT config variable declarations')
        ret = h_file[0]
        if not ('\n'+ret).endswith('\n'):
            ret += '\n'
        for v in qurt_vars:
            ret += self.get_one_declaration(v)
        return ret
    def get_one_definition(self, v):
        ret = ''
        if not v.already_defined:
            for N in v.depends:
                print N
                ret += self.get_one_definition(self.cdict[N])
            ret += v.get_definition()
            v.already_defined = True
        return ret
    def get_definitions(self, kernel):
        if kernel:
            fname = os.path.normpath(os.path.join(os.path.dirname(__file__),'..','Input','cust_config.c'))
            fi = open(fname, 'r')
            c_code = fi.read()
            fi.close()
        else:
            c_code = ''
        # Fill in the template
        for k,v in self.cfg.compiled_dict.items():
            c_code = re.sub(k, v, c_code)
        for v in qurt_vars:
            c_code += self.get_one_definition(v)
        return c_code
