#
#  Each QuRT configuration variable should have a class
#   definition in this file which is a subclass of
#   ConfigVar.
#
#  The name of the class should be the name of the
#   variable (the C identifier).
#
#  See the definition of ConfigVar in genkernel.py
#   for more details.
#
#  Note that each class in this file will have an
#   instance of itself instantiated and stored as
#   an attribute off of the main "cfg" object, and
#   each of those instances will have an attribute
#   "cfg" pointing back at the main object.  So
#   for instance, if class qurt_foobar is defined
#   in this file, an object of type qurt_foobar
#   will be available as cfg.qurt_foobar; this can
#   be used to implement cross-communication between
#   variable classes.  Example...  To get the
#   value of the qurt_foobar variable from another
#   variable's declaration, this should work:
#     self.cfg.qurt_foobar.get_value()
#

"""

class qurtos_main_stack_size(ConfigVar):
    #
    #  Stack size for the primordial guest-OS thread.
    #
    guest_os_only = True
    def get_value(self):
        return alignup(int(eval(self.cfg['MAINSTACKSIZE'])),8)

class qurtos_app_stack(ConfigVar):
    #
    #  Preallocated stack for the primordial guest-OS thread.
    #
    c_type = 'unsigned char []'              # Array of unsigned char
    section = '.data.user.config'            # Put in .data.user.config section
    guest_os_write = True                    # Guest-OS needs to write this
    aligned = 32                             # Align it on a cache line boundary
    array_fill = 0xF8                        # Initialize it filled with 0xF8
    def get_dimension(self):
        return self.cfg.qurtos_main_stack_size.get_value()

class QURTOS_HEAP_SIZE(ConfigVar):
    #
    #  Heap size for the internal guest-OS heap.
    #
    guest_os_only = True
    def get_value(self):
        return alignup(int(eval(self.cfg['KERNELHEAPSIZE'])),4)

class qurtos_heap(ConfigVar):
    #
    #  Preallocated space for the internal guest-OS heap.
    #
    c_type = 'unsigned char []'              # Array of unsigned char
    section = '.data.user.config'            # Put in .data.user.config section
    guest_os_write = True                    # Guest-OS needs to write this
    aligned = 32                             # Align it on a cache line boundary
    array_fill = 0xFF                        # Initialize it filled with 0xFF
    def get_dimension(self):
        return self.cfg.QURTOS_HEAP_SIZE.get_value()

class qurtos_app_heap_cfg(ConfigVar):
    guest_os_write = True                    # Guest-OS needs to write this
    guest_os_only = True
    heap_types = '''
        QURTOS_CONFIG_HEAP_STATIC
        QURTOS_CONFIG_HEAP_DYNAMIC
        QURTOS_CONFIG_HEAP_ASLR
        QURTOS_CONFIG_HEAP_APPREGION
    '''.split()
    before_string = '''
        enum config_heap_type {
            %s
        };
        struct config_heap_info {
            enum config_heap_type type;
            void *vaddr;
            unsigned size;
        };
    ''' % ','.join(heap_types)
    c_type = 'struct config_heap_info'
    def get_before_string(self,is_definition):
        ret = self.before_string
        if is_definition:
            if self.get_actual_heap_type() == 0:
                ret += '__attribute__((section(".bss.user.config"),aligned(32))) '
                ret += 'static unsigned char qurtos_app_heap[%u];\n' % self.get_actual_heap_size()
        return ret
    def get_actual_heap_type(self):
        return int(eval(self.cfg['QURTOSAPPHEAPTYPE']))
    def get_actual_heap_align(self):
        return (4,0x1000,0x1000,0x1000)[self.get_actual_heap_type()]
    def get_actual_heap_size(self):
        return alignup(int(eval(self.cfg['HEAPSIZE'])),self.get_actual_heap_align())
    def get_actual_heap_address(self):
        ret = '0'
        if self.get_actual_heap_type() == 0:
            ret = 'qurtos_app_heap'
        return ret
    def get_value(self):
        return '{ %s, %s, %u }' % (self.heap_types[self.get_actual_heap_type()],
                                   self.get_actual_heap_address(),
                                   self.get_actual_heap_size())

class pool_configs(ConfigVar):
    #
    #  Description of preconfigured physical pools
    #
    c_type = 'struct phys_mem_pool_config []'
    guest_os_write = True
    guest_os_only = True
    before_string = '''
        struct phys_mem_pool_config {
           char name[32];
           struct range {
              unsigned int start;
              unsigned int size;
           } ranges[16];
        };
        #define POOLSTART(name) {name, {
        #define POOLREGION(addr,size) {addr, size},
        #define POOLEND {0}}},
    '''
    def get_value(self):
        return '{%s}' % self.cfg['PHYSPOOLS']
    def get_after_string(self, is_definition):
        ret = 'void __attribute__((section(".text.user.config"))) qurtos_pool_image_fixups(void (*pfn)(const char *,void *,void *))'
        if is_definition:
            ret += '\n%s\n' % self.cfg['IMAGEPOOLINSERT'].replace('\\\\','\\')
        else:
            ret += ';\n'
        return ret

class qurtos_island_heap_size(ConfigVar):
    #
    #  Heap size for the internal guest-OS island heap.
    #
    guest_os_only = True
    def get_value(self):
        return alignup(int(eval(self.cfg['KERNELHEAPSIZEISLAND'])),4)

class qurtos_island_heap(ConfigVar):
    #
    #  Preallocated space for the internal guest-OS island heap.
    #
    c_type = 'unsigned char []'              # Array of unsigned char
    guest_os_write = True                    # Guest-OS needs to write this
    aligned = 32                             # Align it on a cache line boundary
    array_fill = 0xFF                        # Initialize it filled with 0xFF
    island_resident = True                   # Must be island resident
    optional = True                          # Inclusion of this variable is optional
    def get_section(self):
        try:
            return self.cfg['ISLANDHEAPSECTION']
        except TypeError:
            return ''                        # Don't know the section yet when we build the header file
    def get_dimension(self):
        return self.cfg.qurtos_island_heap_size.get_value()
    def include_optional(self):
        return self.get_dimension() != 0

class _IslandConstInteger(ConfigVar):
    section = '.rodata.island'
    island_resident = True
    def get_value(self):
        return int(eval(self.cfg[self.cfgkey]))

class QURTK_hthread_startup(_IslandConstInteger):
    cfgkey = 'HTHREADMASK'

class QURTK_MAX_THREADS_IN_TCM(_IslandConstInteger):
    export_to_qurtos=True
    cfgkey = 'MAXTHREADSINTCM'

class QURTK_config_MAX_THREADS(_IslandConstInteger):
    export_to_qurtos=True
    cfgkey = 'MAXTHREADS'

class qdsp6ss_base_addr(_IslandConstInteger):
    cfgkey = 'QDSP6SSBASE'

class QURTK_config_max_island_int(_IslandConstInteger):
    cfgkey = 'INTISLANDNUM'

class QURTK_etm_cfg_base(_IslandConstInteger):
    section = '.data'
    cfgkey = 'ETMCFGBASEADDR'

class QURTK_etm_atid(_IslandConstInteger):
    section = '.data'
    cfgkey = 'ETMATIDVAL'

class QURTK_island_type(_IslandConstInteger):
    cfgkey = 'ISLANDTYPE'

class QURTK_int_privilege(_IslandConstInteger):
    section = '.data.QURTK.INTERRUPT'
    export_to_qurtos = True
    cfgkey = 'INTPRIV'

class QURTK_secure_proc_sid(_IslandConstInteger):
    cfgkey = 'SPDSIDINFO'

class QURTK_max_programs(_IslandConstInteger):
    export_to_qurtos = True
    cfgkey = 'MAXPROGRAMS'

def ConstUint(name, cfgkey, **kw):
    def get_value(self):
        return '%u' % alignup(int(eval(self.cfg[self.cfgkey])),self.round_up)
    kw.setdefault('cfgkey', cfgkey)
    kw.setdefault('get_value', get_value)
    kw.setdefault('round_up', 1)
    kw.setdefault('nobss', True)
    globals()[name] = type(name, (ConfigVar,), kw)

def ConstInt(name, cfgkey, **kw):
    def get_value(self):
        return '%u' % alignup(int(eval(self.cfg[self.cfgkey])),self.round_up)
    kw.setdefault('cfgkey', cfgkey)
    kw.setdefault('get_value', get_value)
    kw.setdefault('round_up', 1)
    kw.setdefault('c_type', 'int')
    kw.setdefault('nobss', True)
    globals()[name] = type(name, (ConfigVar,), kw)

ConstUint('qurtos_main_priority',             'QURTKMAINPRIORITY')
ConstUint('qurtos_main_bitmask',              'QURTKMAINBITMASK')
ConstUint('qurtos_reaper_priority',           'QURTOS_REAPER_PRIORITY', guest_os_only = True)
ConstUint('qurtos_reaper_tcb_partition',      'QURTOS_REAPER_TCB_PARTITION', guest_os_only = True)
ConstUint('qurtos_reaper_stack_size',         'QURTOSREAPERSTACKSIZE', guest_os_only = True)
ConstUint('QURTK_max_pi_prio',                'MAXPIMUTEXPRIO', export_to_qurtos = True)
ConstUint('qurtos_split_virt_pool',           'SPLITVIRTPOOL', guest_os_only = True)
ConstUint('qurt_osam_stack_fill_count',       'OSAMSTACKFILLCOUNT')
ConstUint('qurt_osam_stack_usage_fill_count', 'OSAMSTACKUSAGEFILLCOUNT', round_up = 4, guest_os_only = True)
ConstUint('qurtos_island_handle_count',       'QURTOSISLANDHANDLES', guest_os_only = True)
ConstUint('qurtos_qdi_handle_count',          'QURTOSQDIHANDLES', guest_os_only = True)
ConstUint('QURTK_timetest_port',              'TIMETESTPORT')
ConstUint('QURTK_isdb_flag_imem_addr',        'ISDBIMEMADDR')
ConstUint('QURTK_etm_to_q6etb',               'ETMTOQ6ETB')
ConstUint('QURTK_tcxo_intmask',               'TCXOINTMASK')
ConstUint('QURTK_dmt_enabled',                'DMT_ENABLED')
ConstUint('QURTK_BQ_shared',                  'BQ_SHARED')
ConstUint('qurt_tcm_dump_size',               'TCMSIZE', round_up = 32, export_to_qurtos = True)
ConstUint('qurt_tcm_save_size',               'TCMSAVESIZE')
ConstUint('QURTK_config_ccr_default',         'CACHEATTR', export_to_qurtos=True)
ConstUint('QURTK_config_usr_default',         'USRATTR', export_to_qurtos=True)
ConstUint('QURTK_chicken_bits_rgdr',          'QURTKCHICKENBITSRGDR')
ConstUint('QURTK_chicken_bits_duck',          'QURTKCHICKENBITSDUCK')
ConstUint('QURTK_chicken_bits_turkey',        'QURTKCHICKENBITSTURKEY')
ConstUint('QURTK_chicken_bits_chicken',       'QURTKCHICKENBITSCHICKEN')
ConstUint('QURTK_chicken_bits_rgdr_2nd',      'QURTKCHICKENBITS2NDRGDR')
ConstUint('QURTK_chicken_bits_chicken_2nd',   'QURTKCHICKENBITS2NDCHICKEN')
ConstUint('QURTK_chicken_bits_rgdr_3rd',      'QURTKCHICKENBITS3RDRGDR')
ConstUint('QURTK_chicken_bits_chicken_3rd',   'QURTKCHICKENBITS3RDCHICKEN')
ConstUint('QURTK_supplement_rgdr_num',        'QURTKSUPNORGDR')
ConstUint('QURTK_supplement_chicken_num',     'QURTKSUPNOCHICKEN')
ConstUint('QURTK_livelock_data',              'QURTKLIVELOCKDATA')
ConstUint('QURTK_hvx_check_power_lock',       'CHKHVXPWRLOCK')
ConstUint('QDSP6_QURT_TIMER_BASE',            'QDSP6QTIMERBASE', export_to_qurtos=True)
ConstUint('QURT_timer_intno',                 'TIMERINTNO', export_to_qurtos=True)
ConstUint('QURT_timerIST_priority',           'TIMERISTPRIORITY', guest_os_only = True)
ConstUint('QURT_timerIST_tcb_partition',      'TIMERISTTCBPARTITION', guest_os_only = True)
ConstUint('QURTK_dbg_cfg_mask',               'QDSP6SSDBGCFGMASK')
ConstUint('QURTK_WB_dbg_cfg_mask',            'WBQDSP6SSDBGCFGMASK')
ConstUint('QURTK_tlblock_entries',            'TLBLOCKNUM')
ConstInt('QURTK_dynamic_tlb_reserve',        'DYNAMICTLBRESERVE')
ConstUint('config_tlb_first_replaceable',     'TLBFIRSTREPL')
ConstUint('QURTK_tlb_debug_level',            'TLBDEBUGLEVEL')
ConstUint('QURTK_min_image_page',             'MINIMAGEPAGE')
ConstUint('QURTK_bus_prio_cfg',               'INTERNALBUSPRIO', export_to_qurtos=True)
ConstUint('QURTK_l2cache_partition_cfg',      'CACHEL2PARTDATA', export_to_qurtos=True)
ConstUint('QURTK_int_nest_allow',             'L1NESTALLOW')
ConstUint('QURTK_stack_framekey_enabled',     'STACKFRAMEKEYEN', export_to_qurtos=True)
ConstUint('QURTK_int_usrpd_check',            'CHKUSRPDINT')
ConstUint('QURTK_int_max',                    'MAXINT')
ConstUint('QURTK_MAX_USER_PROCESSES',         'MAXUSERPROCESSES')
ConstUint('QURTK_max_mailboxes',              'MAXMAILBOXES')
ConstUint('QURTK_config_debug_buffer_size',   'DEBUGBUFSIZE')
ConstUint('QURTK_trace_buffer_size',          'TRACESIZE', round_up = 8, export_to_qurtos = True)

class QURTK_ssr_default(ConfigVar):
    export_to_qurtos = True
    def get_value(self):
        return 0x18F0000

class QURTK_CONTEXT_SIZE(ConfigVar):
    export_to_qurtos = True
    def get_value(self):
        import qurt_consts
        return qurt_consts.THREAD_CONTEXT_TOTALSIZE

class qurt_tcm_utcb_from_stack(ConfigVar):
    guest_os_only = True
    def get_value(self):
        value = int(eval(self.cfg['TCMUTCBFROMSTACK']))
        if value < 0:
            value = int(eval(self.cfg['ISLAND_IS_CONFIGURED']))

class QURTK_phys_island(ConfigVar):
    c_type = 'int []'
    guest_os_only = True
    def get_value(self):
        return '{ %s -1 }' % self.cfg['PHYS_ISLAND_RANGES']

class qurtos_boot_mappings(ConfigVar):
    before_string = '''
        struct boot_mapping {
            unsigned long long template;
            char *start_ptr;
            char *end_ptr;
        };
    '''
    c_type = 'struct boot_mapping []'
    guest_os_only = True
    def get_value(self):
        return '{ %s }' % self.cfg['BOOTMAPDEFINITIONS']

class qurtos_virt_reserved(ConfigVar):
    c_type = 'int []'
    guest_os_only = True
    def get_value(self):
        return '{ %s -1 }' % self.cfg['VIRT_RESERVE_RANGES']

class qurtos_unlock_after_boot(ConfigVar):
    before_string = '''
        struct unlock_after_boot {
            unsigned vpageno;
            unsigned pagecnt;
        };
    '''
    c_type = 'struct unlock_after_boot []'
    guest_os_only = True
    def get_value(self):
        return self.cfg['UNLOCKAFTERBOOT']

class qurtos_objcache_config(ConfigVar):
    before_string = '''
        struct objcache_config {
            short memory_blocks;
            short virt_regions;
            short phys_regions;
            short user_processes;
            short user_threads;
            short qurtos_threads;
            short shmem_regions;
        };
        #define OBJCACHE_DEFAULT (-1)
    '''
    c_type = 'struct objcache_config'
    guest_os_only = True
    def get_value(self):
        return '{ %s }' % self.cfg['OBJCACHECONFIG']

class qurtos_stack_size(ConfigVar):
    guest_os_only = True
    def get_value(self):
        value = int(eval(self.cfg['QURTOSSTACKSIZE']))
        if value <= 0:
            value = 0x7C0
        return alignup(value,32)

class qurtos_island_stack_size(ConfigVar):
    guest_os_only = True
    def get_value(self):
        value = int(eval(self.cfg['QURTOSISLANDSTACKSIZE']))
        if value <= 0:
            value = self.cfg.qurtos_stack_size.get_value()
        return alignup(value,32)

class QURTK_program_names(ConfigVar):
    export_to_qurtos = True
    before_string = '''
        typedef char ConfigProgName[64];
    '''
    c_type = 'ConfigProgName []'
    def get_value(self):
        return '{ %s }' % self.cfg['QURTKPROGNAMES']

class QURTK_thread_contexts(ConfigVar):
    export_to_qurtos = True
    def get_before_string(self, is_definition):
        import qurt_consts
        return '''
            #ifndef RAW_THREAD_CONTEXT
            #define RAW_THREAD_CONTEXT
            struct _raw_thread_context_ {
                char _raw[%u] __attribute__((aligned(32)));
            };
            #endif
        ''' % qurt_consts.THREAD_CONTEXT_TOTALSIZE
    c_type = 'struct _raw_thread_context_ []'
    def get_dimension(self):
        return self.cfg.QURTK_config_MAX_THREADS.get_value() - self.cfg.QURTK_MAX_THREADS_IN_TCM.get_value()

class QURTK_thread_contexts_tcm(ConfigVar):
    export_to_qurtos = True
    def get_before_string(self, is_definition):
        return self.cfg.QURTK_thread_contexts.get_before_string(is_definition)
    c_type = 'struct _raw_thread_context_ []'
    def get_dimension(self):
        return self.cfg.QURTK_MAX_THREADS_IN_TCM.get_value()

class QURTK_ramfs_scan(ConfigVar):
    export_to_qurtos = True
    before_string = '''
        struct ramfs_scan_info {
            unsigned ppn_start;
            unsigned ppn_end;
            unsigned ppn_step;
        };
    '''
    c_type = 'struct ramfs_scan_info'
    def get_value(self):
        return '{ %s }' % self.cfg['RAMFSSCAN']

class qurtos_config_init_funcs(ConfigVar):
    guest_os_only = True
    def get_before_string(self,is_definition):
        before_string = '''
            typedef enum {
                QURTOS_INIT_STAGE_ZERO,             // Reserved
                QURTOS_INIT_STAGE_GENERIC,          // Install method handlers on the generic handle
            } qurtos_init_stage_t;
            typedef void (*qurtos_init_funcptr)(qurtos_init_stage_t);
            void qurtos_call_init_funcs(qurtos_init_stage_t stage);
        '''
        if is_definition:
            before_string += '''
                #define INIT_FUNC_PROTO(n) void n(qurtos_init_stage_t)
                %s
            ''' % self.cfg['CFG_INIT_FUNC_PROTOS']
        return before_string
    c_type = 'qurtos_init_funcptr []'
    def get_value(self):
        return self.cfg['CFG_INIT_FUNCS']

class qurtos_mmap_table(ConfigVar):
    export_to_qurtos = True
    c_type = 'unsigned long long []'
    def get_before_string(self, is_definition):
        if not is_definition:
            return ''
        return '''
            #define R    1
            #define W    2
            #define RW   3
            #define WR   3
            #define X    4
            #define RX   5
            #define XR   5
            #define WX   6
            #define XW   6
            #define RWX  7
            #define XWR  7

            #define MAIN  0
            #define AUX   1

            #define MEMORY_MAP(glob, asid, vpn, umode, busattr, perm, cache_attr, pgsize, cache_part, ppn) \
               ( ((unsigned long long)((1 << 31) | (glob << 30) | ((ppn & 0x800000) << 6) | (busattr << 27) | (asid << 20)|vpn) << 32 ) \
               | ( ((perm << 29) & 0xe0000000) | ((umode & 1) << 28) | (cache_attr << 24) | ((ppn & 0x7FFFFF) << 1) | pgsize))
        '''
    def get_value(self):
        return '{ %s 0 }' % self.cfg['MEMORY_MAPS']

class qurtos_mmap_unlocked(ConfigVar):
    export_to_qurtos = True
    c_type = 'unsigned long long *'
    def get_before_string(self, is_definition):
        if not is_definition:
            return ''
        return self.cfg.qurtos_mmap_table.get_declaration(False)
    def get_value(self):
        return '&qurtos_mmap_table[%s]' % self.cfg['TLBLOCKNUM']

class QURTK_page_table_v2(ConfigVar):
    #
    #  This class is very weird.  Don't use it as an example
    #   of how to do anything.
    #
    export_to_qurtos = True
    c_type = 'unsigned short []'
    def get_declaration(self, kernel):
        return '''
            extern unsigned short __attribute__((section(".data.ukernel.main"),aligned(32))) QURTK_page_table_v2[];
            extern unsigned QURTK_tlb_available_count;
            extern unsigned qurt_tlb_idx;
            extern unsigned QURTK_tlb_first_replaceable;
            extern unsigned QURTK_tlb_last_replaceable;
            extern void *QURTK_pagetables[];
        '''
    def get_definition(self, kernel):
        if not kernel:
            return ''
        else:
            return r'''
                struct {
                   unsigned long long _save[4];                                    // Offset 0, -64 from QURTK_page_table_v2
                   unsigned pad[4];                                                // Offset 32, -32 from QURTK_page_table_v2
                   unsigned _QURTK_tlb_available_count;                            // Offset 48, -16 from QURTK_page_table_v2
                   unsigned _qurt_tlb_idx;                                         // Offset 52, -12 from QURTK_page_table_v2
                   unsigned _QURTK_tlb_first_replaceable;                          // Offset 56, -8 from QURTK_page_table_v2
                   unsigned _QURTK_tlb_last_replaceable;                           // Offset 60, -4 from QURTK_page_table_v2
                   unsigned short _raw[%s];                                        // Offset 64, -0 from QURTK_page_table_v2
                } QURTK_pagetablestruct_v2 __attribute__((section(".data.ukernel.main"),aligned(32))) = {
                   ._raw[1] = %s
                };
                void *QURTK_pagetables[1] __attribute__((section(".data.ukernel.main"))) = { 0 };
                __asm__(
                    ".global QURTK_TLBMISS_SCRATCH_AREA\n"
                    ".set    QURTK_TLBMISS_SCRATCH_AREA, QURTK_pagetablestruct_v2+0\n"
                    ".size   QURTK_TLBMISS_SCRATCH_AREA, 32\n"
                    ".global QURTK_tlb_available_count\n"
                    ".set    QURTK_tlb_available_count, QURTK_pagetablestruct_v2+48\n"
                    ".size   QURTK_tlb_available_count, 4\n"
                    ".global qurt_tlb_idx\n"
                    ".set    qurt_tlb_idx, QURTK_pagetablestruct_v2+52\n"
                    ".size   qurt_tlb_idx, 4\n"
                    ".global QURTK_tlb_first_replaceable\n"
                    ".set    QURTK_tlb_first_replaceable, QURTK_pagetablestruct_v2+56\n"
                    ".size   QURTK_tlb_first_replaceable, 4\n"
                    ".global QURTK_tlb_last_replaceable\n"
                    ".set    QURTK_tlb_last_replaceable, QURTK_pagetablestruct_v2+60\n"
                    ".size   QURTK_tlb_last_replaceable, 4\n"
                    ".global QURTK_page_table_v2\n"
                    ".set    QURTK_page_table_v2, QURTK_pagetablestruct_v2+64\n"
                    ".size   QURTK_page_table_v2, %s*2\n"
                    ".global QURTK_page_table_v2_0x15\n"
                    ".set    QURTK_page_table_v2_0x15, QURTK_pagetablestruct_v2+64+0x15\n"
                    );
            ''' % (self.cfg['V2PAGE_ENTRIES'],
                   self.cfg['V2PAGE_BLOCKS'],
                   self.cfg['V2PAGE_ENTRIES'])

"""
