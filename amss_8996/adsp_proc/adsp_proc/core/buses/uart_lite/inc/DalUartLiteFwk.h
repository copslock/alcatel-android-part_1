#ifndef __DALUARTLITEFWK_H__
#define __DALUARTLITEFWK_H__
/*==================================================================================================

FILE: DalUartLiteFwk.h

DESCRIPTION: Interface between UART driver and DAL framework

                              Copyright (c) 2009 Qualcomm Technologies Incorporated
                                        All Rights Reserved
                                     QUALCOMM Proprietary/GTDR

==================================================================================================*/
/*==================================================================================================

$Header: //components/rel/core.adsp/2.7/buses/uart_lite/inc/DalUartLiteFwk.h#1 $

==================================================================================================*/
/*==================================================================================================
                                           INCLUDE FILES
==================================================================================================*/

#include "DALFramework.h"
#include "DDIUartLite.h"

/*==================================================================================================
                                              TYPEDEFS
==================================================================================================*/
typedef struct
{
   // base members (MUST match DalDeviceHandle)
   uint32                    dwDalHandleId;
   const UartLiteInterface  *pVtbl;
   DALClientCtxt            *pClientCtxt;

   // driver-specific extensions
   DALClientCtxt        client_ctxt;
   DALDevCtxt           device_ctxt;
   UartHandle           uart_handle;
   EventCbCtxt         *intr_cb_ctxt;
} DalUartLiteHandle;

/*==================================================================================================
                                        FUNCTION PROTOTYPES
==================================================================================================*/

DALResult UartLite_DeviceClose     (DalDeviceHandle *h);
DALResult UartLite_DeviceOpen      (DalDeviceHandle *h, uint32 mode);
DALResult UartLite_Receive         (DalDeviceHandle *h, uint32 unused, void *read_buffer, 
                                                        uint32 buffer_len, uint32 *bytes_read);
DALResult UartLite_RegisterEventCb (DalDeviceHandle *h, EventCbCtxt *cb_ctxt, uint32 unused);
DALResult UartLite_Transmit        (DalDeviceHandle *h, uint32 unused, void *write_buffer,
                                                        uint32 buffer_len, uint32 *bytes_written);

#endif  // __DALUARTFWK_H__
