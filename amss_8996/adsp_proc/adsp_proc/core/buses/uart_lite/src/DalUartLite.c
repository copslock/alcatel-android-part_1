/*==================================================================================================

FILE: DalUartLite.c

DESCRIPTION: This module provides the DAL shim layer for the UART Lite driver.

                        Copyright (c) 2014 QUALCOMM Technologies Incorporated
                                        All Rights Reserved
                                     QUALCOMM Proprietary/GTDR

==================================================================================================*/
/*==================================================================================================

$Header: //components/rel/core.adsp/2.7/buses/uart_lite/src/DalUartLite.c#2 $

==================================================================================================*/
/*==================================================================================================
                                            DESCRIPTION
====================================================================================================

GLOBAL FUNCTIONS:
   UartLite_DeviceClose
   UartLite_DeviceOpen
   UartLite_Receive
   UartLite_RegisterEventCb
   UartLite_Transmit
 
==================================================================================================*/
/*==================================================================================================
                                           INCLUDE FILES
==================================================================================================*/

#include "DALFramework.h"
#include "DalDevice.h"
#include "DDIUartLite.h"
#include "DalUartLiteFwk.h"
#include "Uart.h"


/*==================================================================================================
                                     LOCAL FUNCTION PROTOTYPES
==================================================================================================*/

static void intr_event_callback(UartEvent ue, uint32 bytes_available, void *cb_data);

/*==================================================================================================
                                          LOCAL FUNCTIONS
==================================================================================================*/
/*==================================================================================================

FUNCTION: intr_event_callback

DESCRIPTION:

==================================================================================================*/
static void intr_event_callback(UartEvent intr_event, uint32 bytes_available, void *handle)
{
   EventCbCtxt *cb_ctxt = ((DalUartLiteHandle *)handle)->intr_cb_ctxt;

   cb_ctxt->intr_event       = intr_event;
   cb_ctxt->bytes_available  = bytes_available;

   DALSYS_EventCtrl(cb_ctxt->cb_event, DALSYS_EVENT_CTRL_TRIGGER);
}

/*==================================================================================================
                                          GLOBAL FUNCTIONS
==================================================================================================*/
/*==================================================================================================

FUNCTION: UartLite_DeviceClose

DESCRIPTION:

==================================================================================================*/
DALResult UartLite_DeviceClose(DalDeviceHandle *h)
{
   DalUartLiteHandle *handle = (DalUartLiteHandle *)h;

   Uart_deinit(handle->uart_handle);

   return DAL_SUCCESS;
}

/*==================================================================================================

FUNCTION: UartLite_DeviceOpen

DESCRIPTION:

==================================================================================================*/
DALResult UartLite_DeviceOpen(DalDeviceHandle *h, uint32 mode)
{
   DalUartLiteHandle *handle = (DalUartLiteHandle *)h;
   UartPortID         port;
   UartResult         result;

   if (strcmp(handle->device_ctxt.strDeviceName, "UartMainPort") == 0)
   {
      port = UART_MAIN_PORT;
   }
   else if (strcmp(handle->device_ctxt.strDeviceName, "UartSecondPort") == 0)
   {
      port = UART_SECOND_PORT;
   }
   else if (strcmp(handle->device_ctxt.strDeviceName, "UartThirdPort") == 0)
   {
      port = UART_THIRD_PORT;
   }
   else
   {
      return DAL_ERROR;
   }

   result = Uart_init(&handle->uart_handle, port); 
   if (result == UART_ERROR)
   {
      return DAL_ERROR;
   }

   return DAL_SUCCESS;
}

/*==================================================================================================

FUNCTION: UartLite_Receive

DESCRIPTION:

==================================================================================================*/
DALResult UartLite_Receive (DalDeviceHandle *h, uint32 unused, void *read_buffer, 
                            uint32 buffer_len, uint32 *bytes_read)
{
   DalUartLiteHandle *handle = (DalUartLiteHandle *)h;

   *bytes_read = Uart_receive(handle->uart_handle, (char *)read_buffer, buffer_len);

   return DAL_SUCCESS;
}

/*==================================================================================================

FUNCTION: UartLite_RegisterEventCb

DESCRIPTION:

==================================================================================================*/
DALResult UartLite_RegisterEventCb (DalDeviceHandle *h, EventCbCtxt *cb_ctxt, uint32 unused)
{
   DalUartLiteHandle *handle = (DalUartLiteHandle *)h;
   UartResult         result;

   handle->intr_cb_ctxt = cb_ctxt;

   result = Uart_register_event_callback(handle->uart_handle, intr_event_callback, handle);
   if (result == UART_ERROR)
   {
      return DAL_ERROR;
   }

   return DAL_SUCCESS;
}

/*==================================================================================================

FUNCTION: UartLite_Transmit

DESCRIPTION:

==================================================================================================*/
DALResult UartLite_Transmit (DalDeviceHandle *h, uint32 unused, void *write_buffer,
                             uint32 buffer_len, uint32 *bytes_written)
{  
   DalUartLiteHandle *handle = (DalUartLiteHandle *)h;

   *bytes_written = Uart_transmit(handle->uart_handle, (char *)write_buffer, buffer_len);

   return DAL_SUCCESS;
}
