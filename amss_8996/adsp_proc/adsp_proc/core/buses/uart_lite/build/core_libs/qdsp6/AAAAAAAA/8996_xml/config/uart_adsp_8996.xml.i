<!-- ================================================================================================== -->
<!-- GPIO configs. -->
<!-- -->
<!-- Source: IP Catalog -->
<!-- ================================================================================================== -->
<!--
DAL_GPIO_OUTPUT = 1
DAL_GPIO_INPUT = 0
DAL_GPIO_PULL_UP = 3
DAL_GPIO_PULL_DOWN = 1
     BLSP_UART1_TX_DATA DAL_GPIO_CFG( 0, 2, DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA )
     BLSP_UART1_RX_DATA DAL_GPIO_CFG( 1, 2, DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA )
     BLSP_UART1_CTS_N DAL_GPIO_CFG( 2, 2, DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA )
     BLSP_UART1_RFR_N DAL_GPIO_CFG( 3, 2, DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA )
     BLSP_UART2_TX_DATA DAL_GPIO_CFG( 41, 2, DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA )
     BLSP_UART2_RX_DATA DAL_GPIO_CFG( 42, 2, DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA )
     BLSP_UART2_CTS_N DAL_GPIO_CFG( 43, 2, DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA )
     BLSP_UART2_RFR_N DAL_GPIO_CFG( 44, 2, DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA )
     BLSP_UART3_TX_DATA DAL_GPIO_CFG( 45, 2, DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA )
     BLSP_UART3_RX_DATA DAL_GPIO_CFG( 46, 2, DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA )
     BLSP_UART3_CTS_N DAL_GPIO_CFG( 47, 2, DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA )
     BLSP_UART3_RFR_N DAL_GPIO_CFG( 48, 2, DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA )
     BLSP_UART4_TX_DATA DAL_GPIO_CFG( 65, 3, DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA )
     BLSP_UART4_RX_DATA DAL_GPIO_CFG( 66, 3, DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA )
     BLSP_UART4_CTS_N DAL_GPIO_CFG( 67, 3, DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA )
     BLSP_UART4_RFR_N DAL_GPIO_CFG( 68, 3, DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA )
     BLSP_UART5_TX_DATA DAL_GPIO_CFG( 81, 3, DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA )
     BLSP_UART5_RX_DATA DAL_GPIO_CFG( 82, 3, DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA )
     BLSP_UART5_CTS_N DAL_GPIO_CFG( 83, 3, DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA )
     BLSP_UART5_RFR_N DAL_GPIO_CFG( 84, 2, DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA )
     BLSP_UART6_TX_DATA DAL_GPIO_CFG( 25, 4, DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA )
     BLSP_UART6_RX_DATA DAL_GPIO_CFG( 26, 3, DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA )
     BLSP_UART6_CTS_N DAL_GPIO_CFG( 27, 2, DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA )
     BLSP_UART6_RFR_N DAL_GPIO_CFG( 28, 2, DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA )
     BLSP_UART7_TX_DATA DAL_GPIO_CFG( 53, 2, DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA )
     BLSP_UART7_RX_DATA DAL_GPIO_CFG( 54, 2, DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA )
     BLSP_UART7_CTS_N DAL_GPIO_CFG( 55, 2, DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA )
     BLSP_UART7_RFR_N DAL_GPIO_CFG( 56, 2, DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA )
     BLSP_UART8_TX_DATA DAL_GPIO_CFG( 4, 2, DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA )
     BLSP_UART8_RX_DATA DAL_GPIO_CFG( 5, 2, DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA )
     BLSP_UART8_CTS_N DAL_GPIO_CFG( 6, 2, DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA )
     BLSP_UART8_RFR_N DAL_GPIO_CFG( 7, 2, DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA )
     BLSP_UART9_TX_DATA DAL_GPIO_CFG( 49, 2, DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA )
     BLSP_UART9_RX_DATA DAL_GPIO_CFG( 50, 2, DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA )
     BLSP_UART9_CTS_N DAL_GPIO_CFG( 51, 2, DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA )
     BLSP_UART9_RFR_N DAL_GPIO_CFG( 52, 2, DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA )
     BLSP_UART10_TX_DATA DAL_GPIO_CFG( 8, 2, DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA )
     BLSP_UART10_RX_DATA DAL_GPIO_CFG( 9, 2, DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA )
     BLSP_UART10_CTS_N DAL_GPIO_CFG( 10, 4, DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA )
     BLSP_UART10_RFR_N DAL_GPIO_CFG( 11, 4, DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA )
     BLSP_UART11_TX_DATA DAL_GPIO_CFG( 58, 3, DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA )
     BLSP_UART11_RX_DATA DAL_GPIO_CFG( 59, 3, DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA )
     BLSP_UART11_CTS_N DAL_GPIO_CFG( 60, 3, DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA )
     BLSP_UART11_RFR_N DAL_GPIO_CFG( 61, 3, DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA )
     BLSP_UART12_TX_DATA DAL_GPIO_CFG( 85, 2, DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA )
     BLSP_UART12_RX_DATA DAL_GPIO_CFG( 86, 2, DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA )
     BLSP_UART12_CTS_N DAL_GPIO_CFG( 87, 2, DAL_GPIO_INPUT, DAL_GPIO_PULL_DOWN, DAL_GPIO_2MA )
     BLSP_UART12_RFR_N DAL_GPIO_CFG( 88, 2, DAL_GPIO_OUTPUT, DAL_GPIO_PULL_UP, DAL_GPIO_2MA )
======================================================================================================= -->
<driver name="NULL">
  <global_def>
    <string name="UART_PHY_DEVICE_1" type=DALPROP_DATA_TYPE_STRING> /core/buses/uart/1 </string>
    <string name="UART_PHY_DEVICE_2" type=DALPROP_DATA_TYPE_STRING> /core/buses/uart/2 </string>
    <string name="UART_PHY_DEVICE_3" type=DALPROP_DATA_TYPE_STRING> /core/buses/uart/3 </string>
    <string name="UART_PHY_DEVICE_4" type=DALPROP_DATA_TYPE_STRING> /core/buses/uart/4 </string>
    <string name="UART_PHY_DEVICE_5" type=DALPROP_DATA_TYPE_STRING> /core/buses/uart/5 </string>
    <string name="UART_PHY_DEVICE_6" type=DALPROP_DATA_TYPE_STRING> /core/buses/uart/6 </string>
    <string name="UART_PHY_DEVICE_7" type=DALPROP_DATA_TYPE_STRING> /core/buses/uart/7 </string>
    <string name="UART_PHY_DEVICE_8" type=DALPROP_DATA_TYPE_STRING> /core/buses/uart/8 </string>
    <string name="UART_PHY_DEVICE_9" type=DALPROP_DATA_TYPE_STRING> /core/buses/uart/9 </string>
    <string name="UART_PHY_DEVICE_10" type=DALPROP_DATA_TYPE_STRING> /core/buses/uart/10 </string>
    <string name="UART_PHY_DEVICE_11" type=DALPROP_DATA_TYPE_STRING> /core/buses/uart/11 </string>
    <string name="UART_PHY_DEVICE_12" type=DALPROP_DATA_TYPE_STRING> /core/buses/uart/12 </string>
    <string name="blsp1_ahb_clock_name" type=DALPROP_DATA_TYPE_STRING> gcc_blsp1_ahb_clk </string>
    <string name="blsp2_ahb_clock_name" type=DALPROP_DATA_TYPE_STRING> gcc_blsp2_ahb_clk </string>
    <string name="uartbam_1_clock_name" type=DALPROP_DATA_TYPE_STRING> gcc_blsp1_uart1_apps_clk </string>
    <string name="uartbam_2_clock_name" type=DALPROP_DATA_TYPE_STRING> gcc_blsp1_uart2_apps_clk </string>
    <string name="uartbam_3_clock_name" type=DALPROP_DATA_TYPE_STRING> gcc_blsp1_uart3_apps_clk </string>
    <string name="uartbam_4_clock_name" type=DALPROP_DATA_TYPE_STRING> gcc_blsp1_uart4_apps_clk </string>
    <string name="uartbam_5_clock_name" type=DALPROP_DATA_TYPE_STRING> gcc_blsp1_uart5_apps_clk </string>
    <string name="uartbam_6_clock_name" type=DALPROP_DATA_TYPE_STRING> gcc_blsp1_uart6_apps_clk </string>
    <string name="uartbam_7_clock_name" type=DALPROP_DATA_TYPE_STRING> gcc_blsp2_uart1_apps_clk </string>
    <string name="uartbam_8_clock_name" type=DALPROP_DATA_TYPE_STRING> gcc_blsp2_uart2_apps_clk </string>
    <string name="uartbam_9_clock_name" type=DALPROP_DATA_TYPE_STRING> gcc_blsp2_uart3_apps_clk </string>
    <string name="uartbam_10_clock_name" type=DALPROP_DATA_TYPE_STRING> gcc_blsp2_uart4_apps_clk </string>
    <string name="uartbam_11_clock_name" type=DALPROP_DATA_TYPE_STRING> gcc_blsp2_uart5_apps_clk </string>
    <string name="uartbam_12_clock_name" type=DALPROP_DATA_TYPE_STRING> gcc_blsp2_uart6_apps_clk </string>
  </global_def>
  <!-- =================================================================== -->
  <!-- TARGET SPECIFIC UART PROPERTIES -->
  <!-- =================================================================== -->
  <device id="/core/buses/uart">
      <props name="UartMainPortPhy" type=DALPROP_ATTR_TYPE_STRING_PTR> UART_PHY_DEVICE_8 </props>
  </device>
  <!-- =================================================================== -->
  <!-- UART1 -->
  <!-- =================================================================== -->
  <device id="/core/buses/uart/1">
    <props name="GpioRxData" type=DALPROP_ATTR_TYPE_UINT32> 0x20008012 </props>
    <props name="GpioTxData" type=DALPROP_ATTR_TYPE_UINT32> 0x2001C002 </props>
    <props name="GpioRfrN" type=DALPROP_ATTR_TYPE_UINT32> 0x2001C032 </props>
    <props name="GpioCtsN" type=DALPROP_ATTR_TYPE_UINT32> 0x20008022 </props>
    <props name="UartBase" type=DALPROP_ATTR_TYPE_UINT32> 0x756F000 </props>
    <props name="IsLoopback" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
    <props name="BitRate" type=DALPROP_ATTR_TYPE_UINT32> 115200 </props>
    <props name="UartClockName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_1_clock_name </props>
    <props name="PClockName" type=DALPROP_ATTR_TYPE_STRING_PTR> blsp1_ahb_clock_name </props>
    <props name="Irq" type=DALPROP_ATTR_TYPE_UINT32> 104 </props>
    <props name="UartIntSelOffset" type=DALPROP_ATTR_TYPE_UINT32> 0x0000B040 </props>
    <props name="UartIntSelVal" type=DALPROP_ATTR_TYPE_UINT32> 0x001 </props>
  </device>
  <!-- =================================================================== -->
  <!-- UART2 -->
  <!-- =================================================================== -->
  <device id="/core/buses/uart/2">
    <props name="GpioRxData" type=DALPROP_ATTR_TYPE_UINT32> 0x200082A2 </props>
    <props name="GpioTxData" type=DALPROP_ATTR_TYPE_UINT32> 0x2001C292 </props>
    <props name="GpioRfrN" type=DALPROP_ATTR_TYPE_UINT32> 0x2001C2C2 </props>
    <props name="GpioCtsN" type=DALPROP_ATTR_TYPE_UINT32> 0x200082B2 </props>
    <props name="UartBase" type=DALPROP_ATTR_TYPE_UINT32> 0x7570000 </props>
    <props name="IsLoopback" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
    <props name="BitRate" type=DALPROP_ATTR_TYPE_UINT32> 115200 </props>
    <props name="UartClockName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_2_clock_name </props>
    <props name="PClockName" type=DALPROP_ATTR_TYPE_STRING_PTR> blsp1_ahb_clock_name </props>
    <props name="Irq" type=DALPROP_ATTR_TYPE_UINT32> 104 </props>
    <props name="UartIntSelOffset" type=DALPROP_ATTR_TYPE_UINT32> 0x0000B040 </props>
    <props name="UartIntSelVal" type=DALPROP_ATTR_TYPE_UINT32> 0x002 </props>
  </device>
  <!-- =================================================================== -->
  <!-- UART3 -->
  <!-- =================================================================== -->
  <device id="/core/buses/uart/3">
    <props name="GpioRxData" type=DALPROP_ATTR_TYPE_UINT32> 0x200082E2 </props>
    <props name="GpioTxData" type=DALPROP_ATTR_TYPE_UINT32> 0x2001C2D2 </props>
    <props name="GpioRfrN" type=DALPROP_ATTR_TYPE_UINT32> 0x2001C302 </props>
    <props name="GpioCtsN" type=DALPROP_ATTR_TYPE_UINT32> 0x200082F2 </props>
    <props name="UartBase" type=DALPROP_ATTR_TYPE_UINT32> 0x7571000 </props>
    <props name="IsLoopback" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
    <props name="BitRate" type=DALPROP_ATTR_TYPE_UINT32> 115200 </props>
    <props name="UartClockName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_3_clock_name </props>
    <props name="PClockName" type=DALPROP_ATTR_TYPE_STRING_PTR> blsp1_ahb_clock_name </props>
    <props name="Irq" type=DALPROP_ATTR_TYPE_UINT32> 104 </props>
    <props name="UartIntSelOffset" type=DALPROP_ATTR_TYPE_UINT32> 0x0000B040 </props>
    <props name="UartIntSelVal" type=DALPROP_ATTR_TYPE_UINT32> 0x004 </props>
  </device>
  <!-- =================================================================== -->
  <!-- UART4 -->
  <!-- =================================================================== -->
  <device id="/core/buses/uart/4">
    <props name="GpioRxData" type=DALPROP_ATTR_TYPE_UINT32> 0x20008423 </props>
    <props name="GpioTxData" type=DALPROP_ATTR_TYPE_UINT32> 0x2001C413 </props>
    <props name="GpioRfrN" type=DALPROP_ATTR_TYPE_UINT32> 0x2001C443 </props>
    <props name="GpioCtsN" type=DALPROP_ATTR_TYPE_UINT32> 0x20008433 </props>
    <props name="UartBase" type=DALPROP_ATTR_TYPE_UINT32> 0x7572000 </props>
    <props name="IsLoopback" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
    <props name="BitRate" type=DALPROP_ATTR_TYPE_UINT32> 115200 </props>
    <props name="UartClockName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_4_clock_name </props>
    <props name="PClockName" type=DALPROP_ATTR_TYPE_STRING_PTR> blsp1_ahb_clock_name </props>
    <props name="Irq" type=DALPROP_ATTR_TYPE_UINT32> 104 </props>
    <props name="UartIntSelOffset" type=DALPROP_ATTR_TYPE_UINT32> 0x0000B040 </props>
    <props name="UartIntSelVal" type=DALPROP_ATTR_TYPE_UINT32> 0x008 </props>
  </device>
  <!-- =================================================================== -->
  <!-- UART5 -->
  <!-- =================================================================== -->
  <device id="/core/buses/uart/5">
    <props name="GpioRxData" type=DALPROP_ATTR_TYPE_UINT32> 0x20008523 </props>
    <props name="GpioTxData" type=DALPROP_ATTR_TYPE_UINT32> 0x2001C513 </props>
    <props name="GpioRfrN" type=DALPROP_ATTR_TYPE_UINT32> 0x2001C542 </props>
    <props name="GpioCtsN" type=DALPROP_ATTR_TYPE_UINT32> 0x20008533 </props>
    <props name="UartBase" type=DALPROP_ATTR_TYPE_UINT32> 0x7573000 </props>
    <props name="IsLoopback" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
    <props name="BitRate" type=DALPROP_ATTR_TYPE_UINT32> 115200 </props>
    <props name="UartClockName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_5_clock_name </props>
    <props name="PClockName" type=DALPROP_ATTR_TYPE_STRING_PTR> blsp1_ahb_clock_name </props>
    <props name="Irq" type=DALPROP_ATTR_TYPE_UINT32> 104 </props>
    <props name="UartIntSelOffset" type=DALPROP_ATTR_TYPE_UINT32> 0x0000B040 </props>
    <props name="UartIntSelVal" type=DALPROP_ATTR_TYPE_UINT32> 0x010 </props>
  </device>
  <!-- =================================================================== -->
  <!-- UART6 -->
  <!-- =================================================================== -->
  <device id="/core/buses/uart/6">
    <props name="GpioRxData" type=DALPROP_ATTR_TYPE_UINT32> 0x200081A3 </props>
    <props name="GpioTxData" type=DALPROP_ATTR_TYPE_UINT32> 0x2001C194 </props>
    <props name="GpioRfrN" type=DALPROP_ATTR_TYPE_UINT32> 0x2001C1C2 </props>
    <props name="GpioCtsN" type=DALPROP_ATTR_TYPE_UINT32> 0x200081B2 </props>
    <props name="UartBase" type=DALPROP_ATTR_TYPE_UINT32> 0x7574000 </props>
    <props name="IsLoopback" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
    <props name="BitRate" type=DALPROP_ATTR_TYPE_UINT32> 115200 </props>
    <props name="UartClockName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_6_clock_name </props>
    <props name="PClockName" type=DALPROP_ATTR_TYPE_STRING_PTR> blsp1_ahb_clock_name </props>
    <props name="Irq" type=DALPROP_ATTR_TYPE_UINT32> 104 </props>
    <props name="UartIntSelOffset" type=DALPROP_ATTR_TYPE_UINT32> 0x0000B040 </props>
    <props name="UartIntSelVal" type=DALPROP_ATTR_TYPE_UINT32> 0x020 </props>
  </device>
  <!-- =================================================================== -->
  <!-- UART7 -->
  <!-- =================================================================== -->
  <device id="/core/buses/uart/7">
    <props name="GpioRxData" type=DALPROP_ATTR_TYPE_UINT32> 0x20008362 </props>
    <props name="GpioTxData" type=DALPROP_ATTR_TYPE_UINT32> 0x2001C352 </props>
    <props name="GpioRfrN" type=DALPROP_ATTR_TYPE_UINT32> 0x2001C382 </props>
    <props name="GpioCtsN" type=DALPROP_ATTR_TYPE_UINT32> 0x20008372 </props>
    <props name="UartBase" type=DALPROP_ATTR_TYPE_UINT32> 0x75AF000 </props>
    <props name="IsLoopback" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
    <props name="BitRate" type=DALPROP_ATTR_TYPE_UINT32> 115200 </props>
    <props name="UartClockName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_7_clock_name </props>
    <props name="PClockName" type=DALPROP_ATTR_TYPE_STRING_PTR> blsp2_ahb_clock_name </props>
    <props name="Irq" type=DALPROP_ATTR_TYPE_UINT32> 104 </props>
    <props name="UartIntSelOffset" type=DALPROP_ATTR_TYPE_UINT32> 0x0000B040 </props>
    <props name="UartIntSelVal" type=DALPROP_ATTR_TYPE_UINT32> 0x040 </props>
  </device>
  <!-- =================================================================== -->
  <!-- UART8 -->
  <!-- =================================================================== -->
  <device id="/core/buses/uart/8">
    <props name="GpioRxData" type=DALPROP_ATTR_TYPE_UINT32> 0x20008052 </props>
    <props name="GpioTxData" type=DALPROP_ATTR_TYPE_UINT32> 0x2001C042 </props>
    <!-- On CDPs, the flow control lines are not present and UART driver does not support flow control.
    <props name="GpioRfrN" type=DALPROP_ATTR_TYPE_UINT32> 0x2001C072 </props>
    <props name="GpioCtsN" type=DALPROP_ATTR_TYPE_UINT32> 0x20008062 </props>
    -->
    <props name="UartBase" type=DALPROP_ATTR_TYPE_UINT32> 0x75B0000 </props>
    <props name="IsLoopback" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
    <props name="BitRate" type=DALPROP_ATTR_TYPE_UINT32> 115200 </props>
    <props name="UartClockName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_8_clock_name </props>
    <props name="PClockName" type=DALPROP_ATTR_TYPE_STRING_PTR> blsp2_ahb_clock_name </props>
    <props name="Irq" type=DALPROP_ATTR_TYPE_UINT32> 104 </props>
    <props name="UartIntSelOffset" type=DALPROP_ATTR_TYPE_UINT32> 0x0000B040 </props>
    <props name="UartIntSelVal" type=DALPROP_ATTR_TYPE_UINT32> 0x080 </props>
  </device>
  <!-- =================================================================== -->
  <!-- UART9 -->
  <!-- =================================================================== -->
  <device id="/core/buses/uart/9">
    <props name="GpioRxData" type=DALPROP_ATTR_TYPE_UINT32> 0x20008322 </props>
    <props name="GpioTxData" type=DALPROP_ATTR_TYPE_UINT32> 0x2001C312 </props>
    <props name="GpioRfrN" type=DALPROP_ATTR_TYPE_UINT32> 0x2001C342 </props>
    <props name="GpioCtsN" type=DALPROP_ATTR_TYPE_UINT32> 0x20008332 </props>
    <props name="UartBase" type=DALPROP_ATTR_TYPE_UINT32> 0x75B1000 </props>
    <props name="IsLoopback" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
    <props name="BitRate" type=DALPROP_ATTR_TYPE_UINT32> 115200 </props>
    <props name="UartClockName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_9_clock_name</props>
    <props name="PClockName" type=DALPROP_ATTR_TYPE_STRING_PTR> blsp2_ahb_clock_name </props>
    <props name="Irq" type=DALPROP_ATTR_TYPE_UINT32> 104 </props>
    <props name="UartIntSelOffset" type=DALPROP_ATTR_TYPE_UINT32> 0x0000B040 </props>
    <props name="UartIntSelVal" type=DALPROP_ATTR_TYPE_UINT32> 0x100 </props>
  </device>
  <!-- =================================================================== -->
  <!-- UART10 -->
  <!-- =================================================================== -->
  <device id="/core/buses/uart/10">
    <props name="GpioRxData" type=DALPROP_ATTR_TYPE_UINT32> 0x20008092 </props>
    <props name="GpioTxData" type=DALPROP_ATTR_TYPE_UINT32> 0x2001C082 </props>
    <props name="GpioRfrN" type=DALPROP_ATTR_TYPE_UINT32> 0x2001C0B4 </props>
    <props name="GpioCtsN" type=DALPROP_ATTR_TYPE_UINT32> 0x200080A4 </props>
    <props name="UartBase" type=DALPROP_ATTR_TYPE_UINT32> 0x75B2000 </props>
    <props name="IsLoopback" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
    <props name="BitRate" type=DALPROP_ATTR_TYPE_UINT32> 115200 </props>
    <props name="UartClockName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_10_clock_name</props>
    <props name="PClockName" type=DALPROP_ATTR_TYPE_STRING_PTR> blsp2_ahb_clock_name </props>
    <props name="Irq" type=DALPROP_ATTR_TYPE_UINT32> 104 </props>
    <props name="UartIntSelOffset" type=DALPROP_ATTR_TYPE_UINT32> 0x0000B040 </props>
    <props name="UartIntSelVal" type=DALPROP_ATTR_TYPE_UINT32> 0x200 </props>
  </device>
  <!-- =================================================================== -->
  <!-- UART11 -->
  <!-- =================================================================== -->
  <device id="/core/buses/uart/11">
    <props name="GpioRxData" type=DALPROP_ATTR_TYPE_UINT32> 0x200083B3 </props>
    <props name="GpioTxData" type=DALPROP_ATTR_TYPE_UINT32> 0x2001C3A3 </props>
    <props name="GpioRfrN" type=DALPROP_ATTR_TYPE_UINT32> 0x2001C3D3 </props>
    <props name="GpioCtsN" type=DALPROP_ATTR_TYPE_UINT32> 0x200083C3 </props>
    <props name="UartBase" type=DALPROP_ATTR_TYPE_UINT32> 0x75B3000 </props>
    <props name="IsLoopback" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
    <props name="BitRate" type=DALPROP_ATTR_TYPE_UINT32> 115200 </props>
    <props name="UartClockName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_11_clock_name</props>
    <props name="PClockName" type=DALPROP_ATTR_TYPE_STRING_PTR> blsp2_ahb_clock_name </props>
    <props name="Irq" type=DALPROP_ATTR_TYPE_UINT32> 104 </props>
    <props name="UartIntSelOffset" type=DALPROP_ATTR_TYPE_UINT32> 0x0000B040 </props>
    <props name="UartIntSelVal" type=DALPROP_ATTR_TYPE_UINT32> 0x400 </props>
  </device>
  <!-- =================================================================== -->
  <!-- UART12 -->
  <!-- =================================================================== -->
  <device id="/core/buses/uart/12">
    <props name="GpioRxData" type=DALPROP_ATTR_TYPE_UINT32> 0x20008562 </props>
    <props name="GpioTxData" type=DALPROP_ATTR_TYPE_UINT32> 0x2001C552 </props>
    <props name="GpioRfrN" type=DALPROP_ATTR_TYPE_UINT32> 0x2001C582 </props>
    <props name="GpioCtsN" type=DALPROP_ATTR_TYPE_UINT32> 0x20008572 </props>
    <props name="UartBase" type=DALPROP_ATTR_TYPE_UINT32> 0x75B4000 </props>
    <props name="IsLoopback" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
    <props name="BitRate" type=DALPROP_ATTR_TYPE_UINT32> 115200 </props>
    <props name="UartClockName" type=DALPROP_ATTR_TYPE_STRING_PTR> uartbam_12_clock_name</props>
    <props name="PClockName" type=DALPROP_ATTR_TYPE_STRING_PTR> blsp2_ahb_clock_name </props>
    <props name="Irq" type=DALPROP_ATTR_TYPE_UINT32> 104 </props>
    <props name="UartIntSelOffset" type=DALPROP_ATTR_TYPE_UINT32> 0x0000B040 </props>
    <props name="UartIntSelVal" type=DALPROP_ATTR_TYPE_UINT32> 0x800 </props>
  </device>
</driver>
