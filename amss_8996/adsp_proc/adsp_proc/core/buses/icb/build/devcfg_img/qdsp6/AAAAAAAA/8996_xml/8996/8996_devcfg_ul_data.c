#include "../config/devcfg_ABT_lpass_8996.h"
#include "../config/devcfg_icbcfg_adsp_8996.h"
typedef unsigned long int uint32;
typedef unsigned short uint16;
typedef unsigned char uint8;
typedef signed long int int32;
typedef signed short int16;
typedef signed char int8;
typedef unsigned long long uint64;
typedef long long int64;
typedef unsigned char byte;
typedef uint32 DALBOOL;
typedef uint32 DALDEVICEID;
typedef uint32 DalPowerCmd;
typedef uint32 DalPowerDomain;
typedef uint32 DalSysReq;
typedef uint32 DALHandle;
typedef int DALResult;
typedef void * DALEnvHandle;
typedef void * DALSYSEventHandle;
typedef uint32 DALMemAddr;
typedef uint32 DALSYSMemAddr;
typedef uint64 DALSYSPhyAddr;
typedef uint32 DALInterfaceVersion;
typedef unsigned char * DALDDIParamPtr;
typedef struct DALEventObject DALEventObject;
struct DALEventObject
{
    uint32 obj[8];
};
typedef DALEventObject * DALEventHandle;
typedef struct _DALMemObject
{
   uint32 memAttributes;
   uint32 sysObjInfo[2];
   uint32 dwLen;
   uint32 ownerVirtAddr;
   uint32 virtAddr;
   uint32 physAddr;
}
DALMemObject;
typedef struct _DALDDIMemBufDesc
{
   uint32 dwOffset;
   uint32 dwLen;
   uint32 dwUser;
}
DALDDIMemBufDesc;
typedef struct _DALDDIMemDescList
{
   uint32 dwFlags;
   uint32 dwNumBufs;
   DALDDIMemBufDesc bufList[1];
}
DALDDIMemDescList;
typedef struct DALSysMemDescBuf DALSysMemDescBuf;
struct DALSysMemDescBuf
{
   DALSYSMemAddr VirtualAddr;
   DALSYSMemAddr PhysicalAddr;
   uint32 size;
   uint32 user;
};
typedef struct { uint32 dwObjInfo; DALSYSMemAddr thisVirtualAddr; DALSYSMemAddr PhysicalAddr; DALSYSMemAddr VirtualAddr; uint32 hOwnerProc; uint32 dwCurBufIdx; uint32 dwNumDescBufs; DALSysMemDescBuf BufInfo[1];} DALSysMemDescList;
typedef struct DalDeviceInfo DalDeviceInfo;
struct DalDeviceInfo
{
   uint32 sizeOfActual;
   uint32 Version;
   char Name[32];
};
typedef struct DalDeviceStatus DalDeviceStatus;
struct DalDeviceStatus
{
   uint32 sizeOfActual;
   uint32 Status;
};
typedef struct DalDeviceHandle DalDeviceHandle;
typedef struct DalDevice DalDevice;
struct DalDevice
{
   DALResult (*Attach)(const char*,DALDEVICEID,DalDeviceHandle **);
   uint32 (*Detach)(DalDeviceHandle *);
   DALResult (*Init)(DalDeviceHandle *);
   DALResult (*DeInit)(DalDeviceHandle *);
   DALResult (*Open)(DalDeviceHandle *, uint32);
   DALResult (*Close)(DalDeviceHandle *);
   DALResult (*Info)(DalDeviceHandle *, DalDeviceInfo *, uint32);
   DALResult (*PowerEvent)(DalDeviceHandle *, DalPowerCmd, DalPowerDomain);
   DALResult (*SysRequest)(DalDeviceHandle *, DalSysReq, const void *, uint32,
                           void *,uint32, uint32*);
};
typedef struct DalInterface DalInterface;
struct DalInterface
{
   struct DalDevice DalDevice;
};
struct DalDeviceHandle
{
   uint32 dwDalHandleId;
   const DalInterface *pVtbl;
   void *pClientCtxt;
   uint32 dwVtblLen;
};
typedef struct DalDeviceHandle * DALDEVICEHANDLE;
typedef struct DalRemoteHandle DalRemoteHandle;
typedef struct DalRemote DalRemote;
struct DalRemote
{
   struct DalDevice DalDevice;
   DALResult (* FCN_0) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1 );
   DALResult (* FCN_1) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32 u2 );
   DALResult (* FCN_2) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32* p_u2 );
   DALResult (* FCN_3) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32 u2, uint32 u3 );
   DALResult (* FCN_4) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, uint32 u2, uint32* p_u3 );
   DALResult (* FCN_5) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen);
   DALResult (* FCN_6) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * ibuf, uint32 ilen);
   DALResult (* FCN_7) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * obuf, uint32 olen, uint32 *oalen);
   DALResult (* FCN_8) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * obuf, uint32 olen);
   DALResult (* FCN_9) ( uint32 ddi_idx, DalDeviceHandle *h, void * obuf, uint32 olen );
   DALResult (* FCN_10)( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * ibuf, uint32 ilen, void * obuf, uint32 olen, uint32 * oalen);
   DALResult (* FCN_11) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * obuf, uint32 olen);
   DALResult (* FCN_12) ( uint32 ddi_idx, DalDeviceHandle *h, uint32 u1, void * obuf, uint32 olen, uint32 *oalen);
   DALResult (* FCN_13) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * ibuf2, uint32 ilen2, void * obuf, uint32 olen);
   DALResult (* FCN_14) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * obuf1, uint32 olen, void * obuf2, uint32 olen2, uint32 * oalen);
   DALResult (* FCN_15) ( uint32 ddi_idx, DalDeviceHandle *h, void * ibuf, uint32 ilen, void * ibuf2, uint32 ilen2, void * obuf2, uint32 olen2, uint32 *oalen, void * obuf, uint32 olen );
};
struct DalRemoteHandle
{
   uint32 dwDalHandleId;
   const DalRemote *pVtbl;
   void *pClientCtxt;
};
static __inline uint32
DalDevice_Detach(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Detach(_h);
}
static __inline DALResult
DalDevice_Init(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Init(_h);
}
static __inline DALResult
DalDevice_DeInit(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.DeInit(_h);
}
static __inline DALResult
DalDevice_PowerEvent(DalDeviceHandle *_h, DalPowerCmd PowerCmd,
                     DalPowerDomain PowerDomain)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.PowerEvent(_h,PowerCmd,PowerDomain);
}
static __inline DALResult
DalDevice_Open(DalDeviceHandle *_h, uint32 mode)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Open(_h,mode);
}
static __inline DALResult
DalDevice_Close(DalDeviceHandle *_h)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Close(_h);
}
static __inline DALResult
DalDevice_Info(DalDeviceHandle *_h, DalDeviceInfo* info, uint32 infoSize)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.Info(_h,info,infoSize);
}
static __inline DALResult
DalDevice_SysRequest(DalDeviceHandle *_h, DalSysReq ReqIdx,
                     const unsigned char* SrcBuf, int SrcBufLen,
                     unsigned char* DestBuf, uint32 DestBufLen, uint32* DestBufLenReq)
{
   (_h = (DalDeviceHandle *)(((DALHandle)_h) & ~0x00000001));
   return _h->pVtbl->DalDevice.SysRequest(_h,ReqIdx,SrcBuf,SrcBufLen,
                                          DestBuf,DestBufLen,DestBufLenReq);
}
DALResult
DAL_DeviceAttach(DALDEVICEID DeviceId,
                 DalDeviceHandle **phDevice);
DALResult
DAL_DeviceAttachLocal(const char *pszArg,DALDEVICEID DeviceId,
                      DalDeviceHandle **phDevice);
DALResult
DAL_DeviceAttachEx(const char *pszArg, DALDEVICEID DeviceId,
               DALInterfaceVersion ClientVersion,DalDeviceHandle **phDevice);
DALResult
DAL_StringDeviceAttachEx(const char *pszArg,
                   const char *pszDevName,
                   DALInterfaceVersion ClientVersion,
                   DalDeviceHandle **phDalDevice);
DALResult
DAL_DeviceAttachRemote(const char *pszArg,
                   DALDEVICEID DevId,
                   DALInterfaceVersion ClientVersion,
                   DalDeviceHandle **phDALDevice);
DALResult
DAL_DeviceDetach(DalDeviceHandle *hDevice);
DALResult
DAL_StringDeviceAttach(const char *pszDevName,DalDeviceHandle **phDalDevice);
typedef struct DALREG_DriverInfo DALREG_DriverInfo;
struct DALREG_DriverInfo
{
 DALResult (*pfnDALNewFunc)(const char * , DALDEVICEID, DalDeviceHandle**);
 uint32 dwNumDevices;
 DALDEVICEID *pDeviceId;
};
typedef struct DALREG_DriverInfoList DALREG_DriverInfoList;
struct DALREG_DriverInfoList
{
 uint32 dwLen;
 DALREG_DriverInfo ** pDriverInfo;
};
typedef void * DALSYSObjHandle;
typedef void * DALSYSSyncHandle;
typedef void * DALSYSMemHandle;
typedef void * DALSYSWorkLoopHandle;
typedef uint32 *DALSYSPropertyHandle;
typedef struct DALSYSPropertyVar DALSYSPropertyVar;
struct DALSYSPropertyVar
{
    uint32 dwType;
    uint32 dwLen;
    union
    {
        byte *pbVal;
        char *pszVal;
        uint32 dwVal;
        uint32 *pdwVal;
        const void *pStruct;
    }Val;
};
typedef struct DALSYSPropStructTblType DALSYSPropStructTblType ;
struct DALSYSPropStructTblType{
   uint32 dwSize;
   const void *pStruct;
};
typedef struct StringDevice StringDevice;
 struct StringDevice{
   char *pszName;
   uint32 dwHash;
   uint32 dwOffset;
   DALREG_DriverInfo *pFunctionName;
   uint32 dwNumCollision;
   uint32 *pdwCollisions;
};
typedef struct DALProps DALProps;
struct DALProps
{
   const byte *pDALPROP_PropBin;
   const DALSYSPropStructTblType *pDALPROP_StructPtrs;
   const uint32 dwDeviceSize;
   const StringDevice *pDevices;
};
typedef struct DALSYSBaseObj DALSYSBaseObj;
struct DALSYSBaseObj
{
   uint32 dwObjInfo;
   uint32 hOwnerProc;
   DALSYSMemAddr thisVirtualAddr;
};
typedef struct DALSYSEventObj DALSYSEventObj;
struct DALSYSEventObj
{
   unsigned long long _bSpace[80/sizeof(unsigned long long)];
};
typedef struct DALSYSSyncObj DALSYSSyncObj;
struct DALSYSSyncObj
{
   unsigned long long _bSpace[40/sizeof(unsigned long long)];
};
typedef struct DALSYSMemObj DALSYSMemObj;
struct DALSYSMemObj
{
   unsigned long long _bSpace[40/sizeof(unsigned long long)];
};
typedef struct DALSYSWorkLoopEventObj DALSYSWorkLoopEventObj;
struct DALSYSWorkLoopEventObj
{
   unsigned long long _bSpace[32/sizeof(unsigned long long)];
};
typedef struct DALSYSWorkLoopObj DALSYSWorkLoopObj;
struct DALSYSWorkLoopObj
{
   unsigned long long _bSpace[64/sizeof(unsigned long long)];
};
typedef void * DALSYSCallbackFuncCtx;
typedef void * (*DALSYSCallbackFunc)(void*,uint32,void*,uint32);
typedef struct DALSYSMemInfo DALSYSMemInfo;
struct DALSYSMemInfo
{
    DALSYSMemAddr VirtualAddr;
    DALSYSMemAddr PhysicalAddr;
    uint32 dwLen;
    uint32 dwMappedLen;
    uint32 dwProps;
};
typedef enum
{
   DALSYS_MEM_CACHE_DEVICE,
   DALSYS_MEM_CACHE_NONE,
   DALSYS_MEM_CACHE_WRITEBACK,
   DALSYS_MEM_CACHE_WRITETHROUGH,
   DALSYS_MEM_CACHE_INVALID
}
DALSYSMemCacheType;
typedef enum
{
   DALSYS_MEM_PERM_NONE=0,
   DALSYS_MEM_PERM_R=0x1,
   DALSYS_MEM_PERM_W=0x2,
   DALSYS_MEM_PERM_RW=0x3,
   DALSYS_MEM_PERM_X=0x4,
   DALSYS_MEM_PERM_RX=0x5,
   DALSYS_MEM_PERM_WX=0x6,
   DALSYS_MEM_PERM_RWX=0x7,
   DALSYS_MEM_PERM_INVALID=0x8
}
DALSYSMemPermType;
typedef enum
{
   DALSYS_MEM_SHARE_ALLOWED,
   DALSYS_MEM_SHARE_NOT_ALLOWED,
   DALSYS_MEM_SHARE_INVALID
}
DALSYSMemShareType;
typedef struct DALSYSMemInfoEx DALSYSMemInfoEx;
struct DALSYSMemInfoEx
{
    DALSYSPhyAddr physicalAddr;
    DALSYSMemAddr virtualAddr;
    DALSYSMemAddr size;
    DALSYSMemCacheType cacheType;
    DALSYSMemPermType permission;
    DALSYSMemShareType shareType;
};
typedef struct DALSYSMemReq DALSYSMemReq;
struct DALSYSMemReq
{
    DALSYSMemInfoEx memInfo;
    DALSYSMemObj *pObj;
    DALBOOL prealloc;
};
typedef enum
{
   DEVCFG_TARGET_INFO_SOC,
   DEVCFG_TARGET_INFO_PLATFORM
}DEVCFG_TARGET_INFO_TYPE;
typedef DALResult
(*DALSYSWorkLoopExecute)
(
    DALSYSEventHandle,
    void *
);
typedef void
(*DALSYS_InitSystemHandleFncPtr)
(
    DalDeviceHandle * hDalDevice
);
typedef DALResult
(*DALSYS_DestroyObjectFncPtr)
(
    DALSYSObjHandle
);
typedef DALResult
(*DALSYS_CopyObjectFncPtr)
(
    DALSYSObjHandle,
    DALSYSObjHandle *
);
typedef DALResult
(*DALSYS_RegisterWorkLoopFncPtr)
(
    uint32,
    uint32,
    DALSYSWorkLoopHandle *,
    DALSYSWorkLoopObj *
);
typedef DALResult
(*DALSYS_RegisterWorkLoopExFncPtr)
(
    char *,
    uint32,
    uint32,
    uint32,
    DALSYSWorkLoopHandle *,
    DALSYSWorkLoopObj *
);
typedef DALResult
(*DALSYS_AddEventToWorkLoopFncPtr)
(
    DALSYSWorkLoopHandle,
    DALSYSWorkLoopExecute,
    void *,
    DALSYSEventHandle,
    DALSYSSyncHandle
);
typedef DALResult
(*DALSYS_DeleteEventFromWorkLoopFncPtr)
(
    DALSYSWorkLoopHandle,
    DALSYSEventHandle
);
typedef DALResult
(*DALSYS_EventCreateFncPtr)
(
    uint32 ,
    DALSYSEventHandle *,
    DALSYSEventObj *
);
typedef DALResult
(*DALSYS_EventCtrlFncPtr)
(
    DALSYSEventHandle,
    uint32,
   uint32,
   void *,
   uint32
);
typedef DALResult
(*DALSYS_EventWaitFncPtr)
(
    DALSYSEventHandle
);
typedef DALResult
(*DALSYS_EventMultipleWaitFncPtr)
(
    DALSYSEventHandle*,
    int,
    uint32,
    uint32 *
);
typedef DALResult
(*DALSYS_SetupCallbackEventFncPtr)
(
    DALSYSEventHandle,
    DALSYSCallbackFunc,
    DALSYSCallbackFuncCtx
);
typedef DALResult
(*DALSYS_SyncCreateFncPtr)
(
    uint32,
    DALSYSSyncHandle *,
    DALSYSSyncObj *
);
typedef void
(*DALSYS_SyncEnterFncPtr)
(
    DALSYSSyncHandle
);
typedef DALResult
(*DALSYS_SyncTryEnterFncPtr)
(
    DALSYSSyncHandle
);
typedef void
(*DALSYS_SyncLeaveFncPtr)
(
    DALSYSSyncHandle
);
typedef DALResult
(*DALSYS_MemRegionAllocFncPtr)
(
    uint32,
    DALSYSMemAddr,
    DALSYSMemAddr,
    uint32,
    DALSYSMemHandle *,
    DALSYSMemObj *
);
typedef DALResult
(*DALSYS_MemRegionMapPhysFncPtr)
(
    DALSYSMemHandle,
    uint32,
    DALSYSMemAddr,
    uint32
);
typedef DALResult
(*DALSYS_MemInfoFncPtr)
(
    DALSYSMemHandle,
    DALSYSMemInfo *
);
typedef DALResult
(*DALSYS_CacheCommandFncPtr)
(
    uint32 ,
    DALSYSMemAddr,
    uint32
);
typedef DALResult
(*DALSYS_MallocFncPtr)
(
    uint32 ,
    void **
);
typedef DALResult
(*DALSYS_FreeFncPtr)
(
    void *
);
typedef void
(*DALSYS_BusyWaitFncPtr)
(
   uint32
);
typedef DALResult
(*DALSYS_MemDescAddBufFncPtr)
(
    DALSysMemDescList *,
    uint32,
    uint32,
    uint32
);
typedef DALResult
(*DALSYS_MemDescPrepareFncPtr)
(
    DALSysMemDescList *,
    uint32
);
typedef DALResult
(*DALSYS_MemDescValidateFncPtr)
(
    DALSysMemDescList *,
    uint32
);
typedef DALResult
(*DALSYS_GetDALPropertyHandleFncPtr)
(
    DALDEVICEID,
    DALSYSPropertyHandle
);
typedef DALResult
(*DAL_DeviceAttachFncPtr)
(
    const char *pszArg,
    DALDEVICEID,
    DalDeviceHandle **
);
typedef DALResult
(*DAL_DeviceDetachFncPtr)
(
    DalDeviceHandle *
);
typedef DALResult
(*DAL_DeviceAttachExFncPtr)
(
 const char *pszArg,
    DALDEVICEID DevId,
    DALInterfaceVersion ClientVersion,
    DalDeviceHandle **phDalDevice
);
typedef DALResult
(*DALSYS_GetPropertyValueFncPtr)
(
    DALSYSPropertyHandle,
    const char *,
    uint32,
    DALSYSPropertyVar *
);
typedef void
(*DALSYS_LogEventFncPtr)
(
    DALDEVICEID,
    uint32,
    const char *
);
typedef struct DALSYSFncPtrTbl DALSYSFncPtrTbl;
struct DALSYSFncPtrTbl
{
    DALSYS_InitSystemHandleFncPtr DALSYS_InitSystemHandleFnc;
    DALSYS_DestroyObjectFncPtr DALSYS_DestroyObjectFnc;
    DALSYS_CopyObjectFncPtr DALSYS_CopyObjectFnc;
    DALSYS_RegisterWorkLoopFncPtr DALSYS_RegisterWorkLoopFnc;
    DALSYS_RegisterWorkLoopExFncPtr DALSYS_RegisterWorkLoopExFnc;
    DALSYS_AddEventToWorkLoopFncPtr DALSYS_AddEventToWorkLoopFnc;
    DALSYS_DeleteEventFromWorkLoopFncPtr DALSYS_DeleteEventFromWorkLoopFnc;
    DALSYS_EventCreateFncPtr DALSYS_EventCreateFnc;
    DALSYS_EventCtrlFncPtr DALSYS_EventCtrlFnc;
    DALSYS_EventWaitFncPtr DALSYS_EventWaitFnc;
    DALSYS_EventMultipleWaitFncPtr DALSYS_EventMultipleWaitFnc;
    DALSYS_SetupCallbackEventFncPtr DALSYS_SetupCallbackEventFnc;
    DALSYS_SyncCreateFncPtr DALSYS_SyncCreateFnc;
    DALSYS_SyncEnterFncPtr DALSYS_SyncEnterFnc;
    DALSYS_SyncTryEnterFncPtr DALSYS_SyncTryEnterFnc;
    DALSYS_SyncLeaveFncPtr DALSYS_SyncLeaveFnc;
    DALSYS_MemRegionAllocFncPtr DALSYS_MemRegionAllocFnc;
    DALSYS_MemRegionMapPhysFncPtr DALSYS_MemRegionMapPhysFnc;
    DALSYS_MemInfoFncPtr DALSYS_MemInfoFnc;
    DALSYS_CacheCommandFncPtr DALSYS_CacheCommandFnc;
    DALSYS_MallocFncPtr DALSYS_MallocFnc;
    DALSYS_FreeFncPtr DALSYS_FreeFnc;
    DALSYS_BusyWaitFncPtr DALSYS_BusyWaitFnc;
    DALSYS_MemDescAddBufFncPtr DALSYS_MemDescAddBufFnc;
    DALSYS_MemDescPrepareFncPtr DALSYS_MemDescPrepareFnc;
    DALSYS_MemDescValidateFncPtr DALSYS_MemDescValidateFnc;
    DALSYS_GetDALPropertyHandleFncPtr DALSYS_GetDALPropertyHandleFnc;
    DAL_DeviceAttachFncPtr DALSYS_DeviceAttachFnc;
    DAL_DeviceAttachExFncPtr DALSYS_DeviceAttachExFnc;
    DAL_DeviceAttachExFncPtr DALSYS_DeviceAttachRemoteFnc;
    DAL_DeviceDetachFncPtr DALSYS_DeviceDetachFnc;
    DALSYS_GetPropertyValueFncPtr DALSYS_GetPropertyValueFnc;
    DALSYS_LogEventFncPtr DALSYS_LogEventFnc;
};
typedef DALResult
(*DALRemote_NewFncPtr)(const char *, DALDEVICEID, DalDeviceHandle **);
typedef int
(*DALRemote_CommonInitFncPtr)(void);
typedef void
(*DALRemote_IPCInitFcnPtr)(uint32);
typedef int
(*DALRemote_InitFncPtr)(void);
typedef DALSYSEventHandle
(*DALRemote_CreateEventFncPtr)(void *pClientCtxt, DALSYSEventHandle hRemote);
typedef uint32
(*DALRemoteInterProcessCallFncPtr)(void *in_buf, void *out_buf);
typedef int
(*DALRemote_DeinitFncPtr)(void);
typedef struct DALRemoteVtbl DALRemoteVtbl;
struct DALRemoteVtbl
{
   DALRemote_NewFncPtr DALRemote_NewFnc;
   DALRemote_CommonInitFncPtr DALRemote_CommonInitFnc;
   DALRemote_InitFncPtr DALRemote_InitFnc;
   DALRemote_DeinitFncPtr DALRemote_DeinitFnc;
   DALRemote_CreateEventFncPtr DALRemote_CreateEventFnc;
   DALRemote_CreateEventFncPtr DALRemote_CreatePayloadEventFnc;
   DALRemoteInterProcessCallFncPtr DALRemoteInterProcessCallFnc;
   DALRemote_IPCInitFcnPtr DALRemote_IPCInitFcn;
};
typedef struct DALSYSConfig DALSYSConfig;
struct DALSYSConfig
{
    void *pNativeEnv;
    uint32 dwConfig;
   DALRemoteVtbl *pRemoteVtbl;
};
typedef long _Int32t;
typedef unsigned long _Uint32t;
typedef int _Ptrdifft;
typedef unsigned int _Sizet;
typedef __builtin_va_list va_list;
typedef long long _Longlong;
typedef unsigned long long _ULonglong;
typedef int _Wchart;
typedef int _Wintt;
typedef va_list _Va_list;
void _Atexit(void (*)(void));
typedef char _Sysch_t;
void _Locksyslock(int);
void _Unlocksyslock(int);
typedef unsigned short __attribute__((__may_alias__)) alias_short;
static alias_short *strict_aliasing_workaround(unsigned short *ptr) __attribute__((always_inline,unused));
static alias_short *strict_aliasing_workaround(unsigned short *ptr)
{
  alias_short *aliasptr = (alias_short *)ptr;
  return aliasptr;
}
typedef _Sizet size_t;
int memcmp(const void *, const void *, size_t) __attribute__((__nothrow__));
void *memcpy(void *, const void *, size_t) __attribute__((__nothrow__));
void *memcpy_v(volatile void *, const volatile void *, size_t) __attribute__((__nothrow__));
void *memset(void *, int, size_t) __attribute__((__nothrow__));
char *strcat(char *, const char *) __attribute__((__nothrow__));
int strcmp(const char *, const char *) __attribute__((__nothrow__));
char *strcpy(char *, const char *) __attribute__((__nothrow__));
size_t strlen(const char *) __attribute__((__nothrow__));
void *memmove(void *, const void *, size_t) __attribute__((__nothrow__));
void *memmove_v(volatile void *, const volatile void *, size_t) __attribute__((__nothrow__));
int strcoll(const char *, const char *) __attribute__((__nothrow__));
size_t strcspn(const char *, const char *) __attribute__((__nothrow__));
char *strerror(int) __attribute__((__nothrow__));
size_t strlcat(char *, const char *, size_t) __attribute__((__nothrow__));
char *strncat(char *, const char *, size_t) __attribute__((__nothrow__));
int strncmp(const char *, const char *, size_t) __attribute__((__nothrow__));
size_t strlcpy(char *, const char *, size_t) __attribute__((__nothrow__));
char *strncpy(char *, const char *, size_t) __attribute__((__nothrow__));
size_t strspn(const char *, const char *) __attribute__((__nothrow__));
char *strtok(char *, const char *) __attribute__((__nothrow__));
char *strsep(char **, const char *) __attribute__((__nothrow__));
size_t strxfrm(char *, const char *, size_t) __attribute__((__nothrow__));
char *strdup(const char *) __attribute__((__nothrow__));
int strcasecmp(const char *, const char *) __attribute__((__nothrow__));
int strncasecmp(const char *, const char *, size_t) __attribute__((__nothrow__));
char *strtok_r(char *, const char *, char **) __attribute__((__nothrow__));
void *memccpy (void *, const void *, int, size_t) __attribute__((__nothrow__));
int strerror_r (int, char *, size_t) __attribute__((__nothrow__));
char *strchr(const char *, int) __attribute__((__nothrow__));
char *strpbrk(const char *, const char *) __attribute__((__nothrow__));
char *strrchr(const char *, int) __attribute__((__nothrow__));
char *strstr(const char *, const char *) __attribute__((__nothrow__));
void *memchr(const void *, int, size_t) __attribute__((__nothrow__));
void
DALSYS_InitMod(DALSYSConfig * pCfg);
void
DALSYS_DeInitMod(void);
void
DALSYS_InitSystemHandle(DalDeviceHandle *hDalDevice);
DALSYS_LogEventFncPtr
DALSYS_SetLogCfg(uint32 dwMaxLogLevel, DALSYS_LogEventFncPtr DALSysLogFcn);
DALResult
DALSYS_DestroyObject(DALSYSObjHandle hObj);
DALResult
DALSYS_CopyObject(DALSYSObjHandle hObjOrig, DALSYSObjHandle *phObjCopy );
DALResult
DALSYS_RegisterWorkLoop(uint32 dwPriority,
                  uint32 dwMaxNumEvents,
                  DALSYSWorkLoopHandle *phWorkLoop,
                  DALSYSWorkLoopObj *pWorkLoopObj);
DALResult
DALSYS_RegisterWorkLoopEx(
                  char * pszname,
                  uint32 dwStackSize,
                  uint32 dwPriority,
                  uint32 dwMaxNumEvents,
                  DALSYSWorkLoopHandle *phWorkLoop,
                  DALSYSWorkLoopObj *pWorkLoopObj);
DALResult
DALSYS_AddEventToWorkLoop(DALSYSWorkLoopHandle hWorkLoop,
                    DALSYSWorkLoopExecute pfnWorkLoopExecute,
                    void * pArg,
                    DALSYSEventHandle hEvent,
                    DALSYSSyncHandle hSync);
DALResult
DALSYS_DeleteEventFromWorkLoop(DALSYSWorkLoopHandle hWorkLoop,
                         DALSYSEventHandle hEvent);
DALResult
DALSYS_EventCreate(uint32 dwAttribs, DALSYSEventHandle *phEvent,
               DALSYSEventObj *pEventObj);
DALResult
DALSYS_EventCopy(DALSYSEventHandle hEvent,
             DALSYSEventHandle *phEventCopy,DALSYSEventObj *pEventObjCopy,
                 uint32 dwMarshalFlags);
DALResult
DALSYS_EventCtrlEx(DALSYSEventHandle hEvent, uint32 dwCtrl, uint32 dwParam,
                   void *pPayload, uint32 dwPayloadSize);
DALResult
DALSYS_EventWait(DALSYSEventHandle hEvent);
DALResult
DALSYS_EventMultipleWait(DALSYSEventHandle* phEvent, int nEvents,
                         uint32 dwTimeoutUs,uint32 *pdwEventIdx);
DALResult
DALSYS_SetupCallbackEvent(DALSYSEventHandle hEvent, DALSYSCallbackFunc cbFunc,
                          DALSYSCallbackFuncCtx cbFuncCtx);
DALResult DALSYS_TimerStart( DALSYSEventHandle hEvent, uint32 time);
DALResult DALSYS_TimerStop( DALSYSEventHandle hEvent );
DALResult
DALSYS_SyncCreate(uint32 dwAttribs,
                  DALSYSSyncHandle *phSync,
                  DALSYSSyncObj *pSyncObj);
void
DALSYS_SyncEnter(DALSYSSyncHandle hSync);
DALResult
DALSYS_SyncTryEnter(DALSYSSyncHandle hSync);
void
DALSYS_SyncLeave(DALSYSSyncHandle hSync);
DALResult
DALSYS_MemRegionAlloc(uint32 dwAttribs, DALSYSMemAddr VirtualAddr,
                      DALSYSMemAddr PhysicalAddr, uint32 dwLen,
                      DALSYSMemHandle *phMem, DALSYSMemObj *pMemObj);
DALResult
DALSYS_MemRegionAllocEx( DALSYSMemHandle *phMem, const DALSYSMemReq *pMemReq,
      DALSYSMemInfoEx *pMemInfo );
DALResult
DALSYS_MemRegionMapPhys(DALSYSMemHandle hMem, uint32 dwVirtualBaseOffset,
                        DALSYSMemAddr PhysicalAddr, uint32 dwLen);
DALResult
DALSYS_MemInfo(DALSYSMemHandle hMem, DALSYSMemInfo *pMemInfo);
DALResult
DALSYS_MemInfoEx(DALSYSMemHandle hMem, DALSYSMemInfoEx *pMemInfo);
DALResult
DALSYS_CacheCommand(uint32 CacheCmd, DALSYSMemAddr VirtualAddr, uint32 dwLen);
DALResult
DALSYS_Malloc(uint32 dwSize, void **ppMem);
DALResult
DALSYS_Free(void *pmem);
void
DALSYS_BusyWait(uint32 pause_time_us);
DALResult
DALSYS_GetDALPropertyHandle(DALDEVICEID DeviceId, DALSYSPropertyHandle hDALProps);
DALResult
DALSYS_GetDALPropertyHandleEx(DALDEVICEID DeviceId,DALSYSPropertyHandle hDALProps,
                              DEVCFG_TARGET_INFO_TYPE target_info_type);
DALResult
DALSYS_GetDALPropertyHandleDyn(DALDEVICEID DeviceId, DALSYSPropertyHandle hDALProps);
DALResult
DALSYS_GetDALPropertyHandleStr(const char *pszDevName, DALSYSPropertyHandle hDALProps);
DALResult
DALSYS_GetDALPropertyHandleStrEx(const char *pszDevName, DALSYSPropertyHandle hDALProps,
                                 DEVCFG_TARGET_INFO_TYPE target_info_type);
DALResult
DALSYS_GetDALPropertyHandleStrDyn(const char *pszDevName, DALSYSPropertyHandle hDALProps);
DALResult
DALSYS_GetPropertyValue(DALSYSPropertyHandle hDALProps, const char *pszName,
                  uint32 dwId,
                   DALSYSPropertyVar *pDALPropVar);
void
DALSYS_LogEvent(DALDEVICEID DeviceId, uint32 dwLogEventType,
      const char * pszFmt, ...);
DALRemoteVtbl *
DALSYS_GetRemoteInterfaceVtbl(void);
uint32 DALSYS_SetThreadPriority(uint32 priority);
uint32 _DALSYS_memscpy(void * pDest, uint32 iDestSz,
      const void * pSrc, uint32 iSrcSize);
typedef enum
{
  NPA_SUCCESS = 0,
  NPA_ERROR = -1,
} npa_status;
typedef enum
{
  NPA_NO_CLIENT = 0x7fffffff,
  NPA_CLIENT_RESERVED1 = (1 << 0),
  NPA_CLIENT_RESERVED2 = (1 << 1),
  NPA_CLIENT_CUSTOM1 = (1 << 2),
  NPA_CLIENT_CUSTOM2 = (1 << 3),
  NPA_CLIENT_CUSTOM3 = (1 << 4),
  NPA_CLIENT_CUSTOM4 = (1 << 5),
  NPA_CLIENT_REQUIRED = (1 << 6),
  NPA_CLIENT_ISOCHRONOUS = (1 << 7),
  NPA_CLIENT_IMPULSE = (1 << 8),
  NPA_CLIENT_LIMIT_MAX = (1 << 9),
  NPA_CLIENT_VECTOR = (1 << 10),
  NPA_CLIENT_SUPPRESSIBLE = (1 << 11),
  NPA_CLIENT_SUPPRESSIBLE_VECTOR = (1 << 12),
  NPA_CLIENT_CUSTOM5 = (1 << 13),
  NPA_CLIENT_CUSTOM6 = (1 << 14),
  NPA_CLIENT_SUPPRESSIBLE2 = (1 << 15),
  NPA_CLIENT_SUPPRESSIBLE2_VECTOR = (1 << 16),
} npa_client_type;
typedef enum
{
  NPA_TRIGGER_RESERVED_EVENT_0,
  NPA_TRIGGER_RESERVED_EVENT_1,
  NPA_TRIGGER_RESERVED_EVENT_2,
  NPA_TRIGGER_CHANGE_EVENT,
  NPA_TRIGGER_WATERMARK_EVENT,
  NPA_TRIGGER_THRESHOLD_EVENT,
  NPA_TRIGGER_CUSTOM_EVENT1 = 0x010,
  NPA_TRIGGER_CUSTOM_EVENT2 = 0x020,
  NPA_TRIGGER_CUSTOM_EVENT3 = 0x040,
  NPA_TRIGGER_CUSTOM_EVENT4 = 0x080,
  NPA_TRIGGER_PRE_CHANGE_EVENT = 0x0100,
  NPA_TRIGGER_POST_CHANGE_EVENT = 0x0200,
  NPA_TRIGGER_NAS_REQUEST_ACTIVE_EVENT = 0x0400,
  NPA_TRIGGER_MAX_EVENT = 0x0000ffff,
  NPA_TRIGGER_RESERVED_BIT_30 = 0x40000000,
  NPA_TRIGGER_RESERVED_BIT_31 = ( int )0x80000000
} npa_event_trigger_type;
typedef enum
{
  NPA_EVENT_RESERVED,
  NPA_EVENT_RESERVED_1,
  NPA_EVENT_RESERVED_2,
  NPA_EVENT_HI_WATERMARK,
  NPA_EVENT_LO_WATERMARK,
  NPA_EVENT_CHANGE,
  NPA_EVENT_THRESHOLD_LO,
  NPA_EVENT_THRESHOLD_NOMINAL,
  NPA_EVENT_THRESHOLD_HI,
  NPA_EVENT_CUSTOM1 = 0x010,
  NPA_EVENT_CUSTOM2 = 0x020,
  NPA_EVENT_CUSTOM3 = 0x040,
  NPA_EVENT_CUSTOM4 = 0x080,
  NPA_EVENT_PRE_CHANGE = 0x0100,
  NPA_EVENT_POST_CHANGE = 0x0200,
  NPA_EVENT_NAS_REQUEST_ACTIVE = 0x0400,
  NPA_NUM_EVENT_TYPES
} npa_event_type;
typedef enum
{
  NPA_REQUEST_DEFAULT = 0,
  NPA_REQUEST_FIRE_AND_FORGET = 0x00000001,
  NPA_REQUEST_NEXT_AWAKE = 0x00000002,
  NPA_REQUEST_CHANGED_TYPE = 0x00000004,
  NPA_REQUEST_BEST_EFFORT = 0x00000008,
  NPA_REQUEST_RESERVED_ATTRIBUTE1 = 0x00000010,
  NPA_REQUEST_MULTIPLE_NEXT_AWAKE = 0x00000020,
} npa_request_attribute;
enum {
  NPA_QUERY_CURRENT_STATE,
  NPA_QUERY_CLIENT_ACTIVE_REQUEST,
  NPA_QUERY_ACTIVE_MAX,
  NPA_QUERY_RESOURCE_MAX,
  NPA_QUERY_RESOURCE_DISABLED,
  NPA_QUERY_RESOURCE_LATENCY,
  NPA_QUERY_CURRENT_AGGREGATION,
  NPA_MAX_PUBLIC_QUERY = 1023,
  NPA_QUERY_RESERVED_BEGIN = 1024,
  NPA_QUERY_REMOTE_RESOURCE_AVAILABLE = 1025,
  NPA_QUERY_RESERVED_END = 4095
};
typedef enum {
  NPA_QUERY_SUCCESS = 0,
  NPA_QUERY_UNSUPPORTED_QUERY_ID,
  NPA_QUERY_UNKNOWN_RESOURCE,
  NPA_QUERY_NULL_POINTER,
  NPA_QUERY_NO_VALUE,
} npa_query_status;
typedef unsigned int npa_resource_state;
typedef int npa_resource_state_delta;
typedef uint32 npa_time_sclk;
typedef npa_time_sclk npa_resource_time;
typedef void ( *npa_callback )( void *context,
                                unsigned int event_type,
                                void *data,
                                unsigned int data_size );
typedef void ( *npa_simple_callback )( void *context );
typedef struct npa_client * npa_client_handle;
typedef struct npa_event * npa_event_handle;
typedef struct npa_custom_event * npa_custom_event_handle;
typedef struct npa_event_data
{
  const char *resource_name;
  npa_resource_state state;
  npa_resource_state_delta headroom;
} npa_event_data;
typedef struct npa_prepost_change_data
{
  const char *resource_name;
  npa_resource_state from_state;
  npa_resource_state to_state;
  void *data;
} npa_prepost_change_data;
typedef struct npa_link * npa_query_handle;
typedef enum
{
   NPA_QUERY_TYPE_STATE,
   NPA_QUERY_TYPE_VALUE,
   NPA_QUERY_TYPE_NAME,
   NPA_QUERY_TYPE_REFERENCE,
   NPA_QUERY_TYPE_VALUE64,
   NPA_QUERY_TYPE_STATE_VECTOR,
   NPA_QUERY_NUM_TYPES
} npa_query_type_enum;
typedef struct npa_query_type
{
  npa_query_type_enum type;
  union
  {
    npa_resource_state state;
    unsigned int value;
    unsigned long long value64;
    char *name;
    void *reference;
    npa_resource_state *state_vector;
  } data;
} npa_query_type;
npa_client_handle npa_create_sync_client_ex( const char *resource_name,
                                             const char *client_name,
                                             npa_client_type client_type,
                                             unsigned int client_value,
                                             void *client_ref );
npa_client_handle
npa_create_async_client_cb_ex( const char *resource_name,
                               const char *client_name,
                               npa_client_type client_type,
                               npa_callback callback,
                               void *context,
                               unsigned int client_value,
                               void *client_ref );
void npa_destroy_client( npa_client_handle client );
void npa_issue_scalar_request( npa_client_handle client,
                               npa_resource_state state );
void npa_modify_required_request( npa_client_handle client,
                                  npa_resource_state_delta delta );
void npa_issue_scalar_request_unconditional( npa_client_handle client,
                                             npa_resource_state state );
void npa_issue_impulse_request( npa_client_handle client );
void npa_issue_vector_request( npa_client_handle client,
                               unsigned int num_elems,
                               npa_resource_state *vector );
void npa_issue_isoc_request( npa_client_handle client,
                             npa_resource_time deadline,
                             npa_resource_state level_hint );
void npa_issue_limit_max_request( npa_client_handle client,
                                  npa_resource_state max );
void npa_complete_request( npa_client_handle client );
void npa_cancel_request( npa_client_handle client );
void npa_set_request_attribute( npa_client_handle client,
                                npa_request_attribute attr );
void npa_set_client_type_required( npa_client_handle client );
void npa_set_client_type_suppressible( npa_client_handle client );
npa_event_handle
npa_create_event_cb( const char *resource_name,
                     const char *event_name,
                     npa_event_trigger_type trigger_type,
                     npa_callback callback,
                     void *context );
void npa_set_event_watermarks( npa_event_handle event,
                               npa_resource_state_delta hi_watermark,
                               npa_resource_state_delta lo_watermark );
void npa_set_event_thresholds( npa_event_handle event,
                               npa_resource_state lo_threshold,
                               npa_resource_state hi_threshold );
npa_event_handle npa_create_custom_event( const char *resource_name,
                                          const char *event_name,
                                          unsigned int trigger_type,
                                          void *reg_data,
                                          npa_callback callback,
                                          void *context );
void npa_destroy_custom_event( npa_event_handle event );
void npa_destroy_event_handle( npa_event_handle event );
npa_query_handle npa_create_query_handle( const char * resource_name );
void npa_destroy_query_handle( npa_query_handle query );
npa_query_status npa_query( npa_query_handle query,
                            uint32 query_id,
                            npa_query_type *query_result );
npa_query_status npa_query_by_name( const char *resource_name,
                                    uint32 query_id,
                                    npa_query_type *query_result );
npa_query_status npa_query_by_client( npa_client_handle client,
                                      uint32 query_id,
                                      npa_query_type *query_result );
npa_query_status npa_query_by_event( npa_event_handle event,
                                     uint32 query_id,
                                     npa_query_type *query_result );
npa_query_status npa_query_resource_available( const char *resource_name );
void npa_resources_available_cb( unsigned int num_resources,
                                 const char *resources[],
                                 npa_callback callback,
                                 void *context );
void npa_resource_available_cb( const char *resource_name,
                                npa_callback callback,
                                void *context );
void npa_dal_event_callback( void *dal_event_handle,
                             unsigned int event_type,
                             void *data,
                             unsigned int data_size );
typedef enum
{
  NPA_FORK_DEFAULT = 0,
  NPA_FORK_DISALLOWED,
  NPA_FORK_ALLOWED,
} npa_fork_pref;
typedef unsigned int (*npa_join_function) ( void *, void * );
void npa_set_client_fork_pref( npa_client_handle client,
                               npa_fork_pref pref );
npa_fork_pref npa_get_client_fork_pref( npa_client_handle client );
void npa_join_request( npa_client_handle client );
typedef enum
{
  NPA_RESOURCE_DEFAULT = 0,
  NPA_RESOURCE_DRIVER_UNCONDITIONAL = 0x00000001,
  NPA_RESOURCE_SINGLE_CLIENT = 0x00000002,
  NPA_RESOURCE_VECTOR_STATE = 0x00000004,
  NPA_RESOURCE_REMOTE_ACCESS_ALLOWED = 0x00000008,
  NPA_RESOURCE_INTERPROCESS_ACCESS_ALLOWED = 0x00000008,
  NPA_RESOURCE_REMOTE = 0x00000010,
  NPA_RESOURCE_REMOTE_PROXY = 0x00000020,
  NPA_RESOURCE_REMOTE_NO_INIT = 0x00000040,
  NPA_RESOURCE_SUPPORTS_SUPPRESSIBLE = 0x00000080,
  NPA_RESOURCE_DRIVER_UNCONDITIONAL_FIRST = 0x00000200,
  NPA_RESOURCE_LPR_ISSUABLE = 0x00000400,
  NPA_RESOURCE_ALLOW_CLIENT_TYPE_CHANGE = 0x00000800,
  NPA_RESOURCE_ENABLE_PREPOST_CHANGE_EVENTS = 0x00001000,
} npa_resource_attribute;
typedef enum
{
  NPA_NODE_DEFAULT = 0,
  NPA_NODE_NO_LOCK = 0x00000001,
  NPA_NODE_DISABLEABLE = 0x00000002,
  NPA_NODE_FORKABLE = 0x00000004,
} npa_node_attribute;
enum
{
  NPA_RESOURCE_AUTHOR_QUERY_START = NPA_MAX_PUBLIC_QUERY,
  NPA_QUERY_RESOURCE_ATTRIBUTES,
  NPA_QUERY_NODE_ATTRIBUTES,
  NPA_MAX_RESOURCE_AUTHOR_QUERY = 2047
};
typedef void* npa_user_data;
typedef struct npa_event_callback
{
  npa_callback callback;
  npa_user_data context;
} npa_event_callback;
typedef struct npa_work_request
{
  npa_resource_state state;
  union
  {
    npa_resource_state *vector;
    char *string;
    void *reference;
  } pointer;
} npa_work_request;
typedef struct npa_client
{
  struct npa_client *prev, *next;
  const char *name;
  const char *resource_name;
  struct npa_resource *resource;
  npa_user_data resource_data;
  npa_client_type type;
  npa_work_request work[3];
  unsigned int index;
  unsigned int sequence;
  struct npa_async_client_data *async;
  void *log_handle;
  unsigned int request_attr;
  void (*issue_request)( npa_client_handle client, int new_request );
  struct npa_scheduler_data *request_ptr;
} npa_client;
typedef struct npa_event
{
  struct npa_event *prev, *next;
  unsigned int trigger_type;
  const char *name;
  struct npa_resource *resource;
  union
  {
    npa_resource_state_delta watermark;
    npa_resource_state threshold;
  } lo;
  union
  {
    npa_resource_state_delta watermark;
    npa_resource_state threshold;
  } hi;
  npa_event_callback callback;
  void *reg_data;
  struct npa_event_action *action;
} npa_event;
typedef struct npa_link
{
  struct npa_link *next, *prev;
  const char *name;
  struct npa_resource *resource;
} npa_link;
typedef npa_resource_state (*npa_resource_driver_fcn) (
  struct npa_resource *resource,
  npa_client_handle client,
  npa_resource_state state
  );
typedef npa_resource_state (*npa_resource_update_fcn)(
  struct npa_resource *resource,
  npa_client_handle client
  );
typedef struct npa_resource_latency {
  uint32 request;
  uint32 fork;
  uint32 notify;
} npa_resource_latency;
typedef npa_query_status (*npa_resource_query_fcn)(
  struct npa_resource *resource,
  unsigned int query_id,
  npa_query_type *query_result );
typedef npa_query_status (*npa_link_query_fcn)(
  struct npa_link *resource_link,
  unsigned int query_id,
  npa_query_type *query_result );
typedef struct npa_resource_plugin
{
  npa_resource_update_fcn update_fcn;
  unsigned int supported_clients;
  void (*create_client_fcn) ( npa_client * );
  void (*destroy_client_fcn)( npa_client * );
  unsigned int (*create_client_ex_fcn)( npa_client *, unsigned int, void * );
  void (*cancel_client_fcn) ( npa_client *);
} npa_resource_plugin;
typedef struct npa_node_dependency
{
  const char *name;
  npa_client_type client_type;
  npa_client_handle handle;
} npa_node_dependency;
typedef struct npa_resource_definition
{
  const char *name;
  const char *units;
  npa_resource_state max;
  const npa_resource_plugin *plugin;
  unsigned int attributes;
  npa_user_data data;
  npa_resource_query_fcn query_fcn;
  npa_link_query_fcn link_query_fcn;
  struct npa_resource *handle;
} npa_resource_definition;
typedef struct npa_node_definition
{
  const char *name;
  npa_resource_driver_fcn driver_fcn;
  unsigned int attributes;
  npa_user_data data;
  unsigned int dependency_count;
  npa_node_dependency *dependencies;
  unsigned int resource_count;
  npa_resource_definition *resources;
} npa_node_definition;
typedef struct npa_resource
{
  npa_resource_definition *definition;
  npa_node_definition *node;
  unsigned int index;
  npa_client *clients;
  union
  {
    npa_event *creation_events;
    struct npa_event_list *list;
  } events;
  const npa_resource_plugin *active_plugin;
  npa_resource_state request_state;
  npa_resource_state active_state;
  npa_resource_state internal_state[8];
  npa_resource_state *state_vector;
  npa_resource_state *required_state_vector;
  npa_resource_state *suppressible_state_vector;
  npa_resource_state *suppressible2_state_vector;
  npa_resource_state *semiactive_state_vector;
  npa_resource_state active_max;
  npa_resource_state_delta active_headroom;
  struct CoreMutex *node_lock;
  struct CoreMutex *event_lock;
  struct npa_resource_internal_data *_internal;
  unsigned int sequence;
  void *log_handle;
  npa_resource_latency *latency;
} npa_resource;
typedef void* npa_join_token;
void npa_define_node_cb( npa_node_definition *node,
                         npa_resource_state initial_state[],
                         npa_callback node_cb,
                         void *context);
void npa_alias_resource_cb( const char *resource_name,
                            const char *alias_name,
                            npa_callback alias_cb,
                            void *context);
void npa_define_marker( const char *marker_name );
void npa_define_marker_with_attributes( const char *marker_name,
                                        npa_resource_attribute attributes );
void npa_issue_dependency_request( npa_client_handle cur_client,
                                   npa_client_handle req_client,
                                   npa_resource_state req_state,
                                   npa_client_handle sup_client,
                                   npa_resource_state sup_state );
void npa_issue_dependency_vector_request( npa_client_handle cur_client,
                                          npa_client_handle req_client,
                                          unsigned int req_num_elems,
                                          npa_resource_state *req_vector,
                                          npa_client_handle sup_client,
                                          unsigned int sup_num_elems,
                                          npa_resource_state *sup_vector );
void npa_assign_resource_state( npa_resource *resource,
                                npa_resource_state state );
npa_resource *npa_query_get_resource( npa_query_handle query_handle );
void npa_enable_node( npa_node_definition *node, npa_resource_state default_state[] );
void npa_disable_node( npa_node_definition *node );
npa_join_token npa_fork_resource( npa_resource *resource,
                                  npa_join_function join_func,
                                  void *join_data );
void npa_resource_lock( npa_resource *resource );
int npa_resource_trylock( npa_resource *resource );
void npa_resource_unlock( npa_resource *resource );
unsigned int npa_request_has_attribute( npa_client_handle client,
                                        npa_request_attribute attr );
unsigned int npa_get_request_attributes( npa_client_handle client );
npa_client_handle npa_pass_request_attributes( npa_client_handle current,
                                               npa_client_handle dependency );
void npa_change_resource_plugin( const char *resource_name,
                                 const npa_resource_plugin *plugin );
extern const npa_resource_plugin npa_binary_plugin;
extern const npa_resource_plugin npa_max_plugin;
extern const npa_resource_plugin npa_min_plugin;
extern const npa_resource_plugin npa_sum_plugin;
extern const npa_resource_plugin npa_identity_plugin;
extern const npa_resource_plugin npa_always_on_plugin;
extern const npa_resource_plugin npa_impulse_plugin;
extern const npa_resource_plugin npa_or_plugin;
extern const npa_resource_plugin npa_binary_and_plugin;
extern const npa_resource_plugin npa_no_client_plugin;
npa_resource_state npa_min_update_fcn( npa_resource *resource,
                                       npa_client_handle client);
npa_resource_state npa_max_update_fcn( npa_resource *resource,
                                       npa_client_handle client );
npa_resource_state npa_sum_update_fcn( npa_resource *resource,
                                       npa_client_handle client );
npa_resource_state npa_binary_update_fcn( npa_resource *resource,
                                          npa_client_handle client );
npa_resource_state npa_or_update_fcn( npa_resource *resource,
                                      npa_client_handle client);
npa_resource_state npa_binary_and_update_fcn( npa_resource *resource,
                                              npa_client_handle client);
npa_resource_state npa_identity_update_fcn( npa_resource *resource,
                                            npa_client_handle client );
npa_resource_state npa_always_on_update_fcn( npa_resource *resource,
                                             npa_client_handle client );
npa_resource_state npa_impulse_update_fcn( npa_resource *resource,
                                           npa_client_handle client );
void npa_issue_internal_request( npa_client_handle client );
npa_status npa_resource_add_system_event_callback( const char *resource_name,
                                                   npa_callback callback,
                                                   void *context );
npa_status npa_resource_remove_system_event_callback( const char *resource_name,
                                                      npa_callback callback,
                                                      void *context );
void npa_post_custom_event( npa_event_handle event,
                            npa_event_type type, void *event_data );
void npa_post_custom_event_nodups( npa_event_handle event,
                                   npa_event_type type, void *data );
void npa_post_custom_events( npa_resource *resource,
                             npa_event_type type, void *event_data );
void npa_dispatch_custom_event( npa_event_handle event,
                                npa_event_type type, void *data );
void npa_dispatch_custom_events( npa_resource *resource,
                                 npa_event_type type, void *data );
npa_event_handle
npa_get_first_event_of_trigger_type( npa_resource *resource,
                                     unsigned int trigger_type );
npa_event_handle
npa_get_next_event_of_trigger_type( npa_event_handle event,
                                    unsigned int trigger_type );
void npa_dispatch_pre_change_events( npa_resource *resource,
                                     npa_resource_state from_state,
                                     npa_resource_state to_state,
                                     void *data );
void npa_dispatch_post_change_events( npa_resource *resource,
                                      npa_resource_state from_state,
                                      npa_resource_state to_state,
                                      void *data );
struct npa_resource;
npa_status npa_add_resource_log_by_name( char *log_name, char *resource_name );
npa_status npa_remove_resource_log_by_name( char *log_name,
                                            char * resource_name );
npa_status npa_add_resource_log_by_handle( char *log_name,
                                           struct npa_resource *resource );
npa_status npa_remove_resource_log_by_handle( char *log_name,
                                              struct npa_resource *resource );
typedef enum {
  ICBID_MASTER_APPSS_PROC = 0,
  ICBID_MASTER_MSS_PROC,
  ICBID_MASTER_MNOC_BIMC,
  ICBID_MASTER_SNOC_BIMC,
  ICBID_MASTER_SNOC_BIMC_0 = ICBID_MASTER_SNOC_BIMC,
  ICBID_MASTER_CNOC_MNOC_MMSS_CFG,
  ICBID_MASTER_CNOC_MNOC_CFG,
  ICBID_MASTER_GFX3D,
  ICBID_MASTER_JPEG,
  ICBID_MASTER_MDP,
  ICBID_MASTER_MDP0 = ICBID_MASTER_MDP,
  ICBID_MASTER_MDPS = ICBID_MASTER_MDP,
  ICBID_MASTER_VIDEO,
  ICBID_MASTER_VIDEO_P0 = ICBID_MASTER_VIDEO,
  ICBID_MASTER_VIDEO_P1,
  ICBID_MASTER_VFE,
  ICBID_MASTER_VFE0 = ICBID_MASTER_VFE,
  ICBID_MASTER_CNOC_ONOC_CFG,
  ICBID_MASTER_JPEG_OCMEM,
  ICBID_MASTER_MDP_OCMEM,
  ICBID_MASTER_VIDEO_P0_OCMEM,
  ICBID_MASTER_VIDEO_P1_OCMEM,
  ICBID_MASTER_VFE_OCMEM,
  ICBID_MASTER_LPASS_AHB,
  ICBID_MASTER_QDSS_BAM,
  ICBID_MASTER_SNOC_CFG,
  ICBID_MASTER_BIMC_SNOC,
  ICBID_MASTER_BIMC_SNOC_0 = ICBID_MASTER_BIMC_SNOC,
  ICBID_MASTER_CNOC_SNOC,
  ICBID_MASTER_CRYPTO,
  ICBID_MASTER_CRYPTO_CORE0 = ICBID_MASTER_CRYPTO,
  ICBID_MASTER_CRYPTO_CORE1,
  ICBID_MASTER_LPASS_PROC,
  ICBID_MASTER_MSS,
  ICBID_MASTER_MSS_NAV,
  ICBID_MASTER_OCMEM_DMA,
  ICBID_MASTER_PNOC_SNOC,
  ICBID_MASTER_WCSS,
  ICBID_MASTER_QDSS_ETR,
  ICBID_MASTER_USB3,
  ICBID_MASTER_USB3_0 = ICBID_MASTER_USB3,
  ICBID_MASTER_SDCC_1,
  ICBID_MASTER_SDCC_3,
  ICBID_MASTER_SDCC_2,
  ICBID_MASTER_SDCC_4,
  ICBID_MASTER_TSIF,
  ICBID_MASTER_BAM_DMA,
  ICBID_MASTER_BLSP_2,
  ICBID_MASTER_USB_HSIC,
  ICBID_MASTER_BLSP_1,
  ICBID_MASTER_USB_HS,
  ICBID_MASTER_USB_HS1 = ICBID_MASTER_USB_HS,
  ICBID_MASTER_PNOC_CFG,
  ICBID_MASTER_SNOC_PNOC,
  ICBID_MASTER_RPM_INST,
  ICBID_MASTER_RPM_DATA,
  ICBID_MASTER_RPM_SYS,
  ICBID_MASTER_DEHR,
  ICBID_MASTER_QDSS_DAP,
  ICBID_MASTER_SPDM,
  ICBID_MASTER_TIC,
  ICBID_MASTER_SNOC_CNOC,
  ICBID_MASTER_GFX3D_OCMEM,
  ICBID_MASTER_GFX3D_GMEM = ICBID_MASTER_GFX3D_OCMEM,
  ICBID_MASTER_OVIRT_SNOC,
  ICBID_MASTER_SNOC_OVIRT,
  ICBID_MASTER_SNOC_GVIRT = ICBID_MASTER_SNOC_OVIRT,
  ICBID_MASTER_ONOC_OVIRT,
  ICBID_MASTER_USB_HS2,
  ICBID_MASTER_QPIC,
  ICBID_MASTER_IPA,
  ICBID_MASTER_DSI,
  ICBID_MASTER_MDP1,
  ICBID_MASTER_MDPE = ICBID_MASTER_MDP1,
  ICBID_MASTER_VPU_PROC,
  ICBID_MASTER_VPU,
  ICBID_MASTER_VPU0 = ICBID_MASTER_VPU,
  ICBID_MASTER_CRYPTO_CORE2,
  ICBID_MASTER_PCIE_0,
  ICBID_MASTER_PCIE_1,
  ICBID_MASTER_SATA,
  ICBID_MASTER_UFS,
  ICBID_MASTER_USB3_1,
  ICBID_MASTER_VIDEO_OCMEM,
  ICBID_MASTER_VPU1,
  ICBID_MASTER_VCAP,
  ICBID_MASTER_EMAC,
  ICBID_MASTER_BCAST,
  ICBID_MASTER_MMSS_PROC,
  ICBID_MASTER_SNOC_BIMC_1,
  ICBID_MASTER_SNOC_PCNOC,
  ICBID_MASTER_AUDIO,
  ICBID_MASTER_MM_INT_0,
  ICBID_MASTER_MM_INT_1,
  ICBID_MASTER_MM_INT_2,
  ICBID_MASTER_MM_INT_BIMC,
  ICBID_MASTER_MSS_INT,
  ICBID_MASTER_PCNOC_CFG,
  ICBID_MASTER_PCNOC_INT_0,
  ICBID_MASTER_PCNOC_INT_1,
  ICBID_MASTER_PCNOC_M_0,
  ICBID_MASTER_PCNOC_M_1,
  ICBID_MASTER_PCNOC_S_0,
  ICBID_MASTER_PCNOC_S_1,
  ICBID_MASTER_PCNOC_S_2,
  ICBID_MASTER_PCNOC_S_3,
  ICBID_MASTER_PCNOC_S_4,
  ICBID_MASTER_PCNOC_S_6,
  ICBID_MASTER_PCNOC_S_7,
  ICBID_MASTER_PCNOC_S_8,
  ICBID_MASTER_PCNOC_S_9,
  ICBID_MASTER_QDSS_INT,
  ICBID_MASTER_SNOC_INT_0,
  ICBID_MASTER_SNOC_INT_1,
  ICBID_MASTER_SNOC_INT_BIMC,
  ICBID_MASTER_TCU_0,
  ICBID_MASTER_TCU_1,
  ICBID_MASTER_BIMC_INT_0,
  ICBID_MASTER_BIMC_INT_1,
  ICBID_MASTER_CAMERA,
  ICBID_MASTER_RICA,
  ICBID_MASTER_SNOC_BIMC_2,
  ICBID_MASTER_BIMC_SNOC_1,
  ICBID_MASTER_A0NOC_SNOC,
  ICBID_MASTER_A1NOC_SNOC,
  ICBID_MASTER_A2NOC_SNOC,
  ICBID_MASTER_PIMEM,
  ICBID_MASTER_SNOC_VMEM,
  ICBID_MASTER_CPP,
  ICBID_MASTER_CNOC_A1NOC,
  ICBID_MASTER_PNOC_A1NOC,
  ICBID_MASTER_HMSS,
  ICBID_MASTER_PCIE_2,
  ICBID_MASTER_ROTATOR,
  ICBID_MASTER_VENUS_VMEM,
  ICBID_MASTER_DCC,
  ICBID_MASTER_MCDMA,
  ICBID_MASTER_PCNOC_INT_2,
  ICBID_MASTER_PCNOC_INT_3,
  ICBID_MASTER_PCNOC_INT_4,
  ICBID_MASTER_PCNOC_INT_5,
  ICBID_MASTER_PCNOC_INT_6,
  ICBID_MASTER_PCNOC_S_5,
  ICBID_MASTER_SENSORS_AHB,
  ICBID_MASTER_SENSORS_PROC,
  ICBID_MASTER_QSPI,
  ICBID_MASTER_VFE1,
  ICBID_MASTER_SNOC_INT_2,
  ICBID_MASTER_SMMNOC_BIMC,
  ICBID_MASTER_CRVIRT_A1NOC,
  ICBID_MASTER_XM_USB_HS1,
  ICBID_MASTER_XI_USB_HS1,
  ICBID_MASTER_COUNT,
  ICBID_MASTER_SIZE = 0x7FFFFFFF
} ICBId_MasterType;
typedef enum {
  ICBID_SLAVE_EBI1 = 0,
  ICBID_SLAVE_APPSS_L2,
  ICBID_SLAVE_BIMC_SNOC,
  ICBID_SLAVE_BIMC_SNOC_0 = ICBID_SLAVE_BIMC_SNOC,
  ICBID_SLAVE_CAMERA_CFG,
  ICBID_SLAVE_DISPLAY_CFG,
  ICBID_SLAVE_OCMEM_CFG,
  ICBID_SLAVE_CPR_CFG,
  ICBID_SLAVE_CPR_XPU_CFG,
  ICBID_SLAVE_MISC_CFG,
  ICBID_SLAVE_MISC_XPU_CFG,
  ICBID_SLAVE_VENUS_CFG,
  ICBID_SLAVE_GFX3D_CFG,
  ICBID_SLAVE_MMSS_CLK_CFG,
  ICBID_SLAVE_MMSS_CLK_XPU_CFG,
  ICBID_SLAVE_MNOC_MPU_CFG,
  ICBID_SLAVE_ONOC_MPU_CFG,
  ICBID_SLAVE_MNOC_BIMC,
  ICBID_SLAVE_SERVICE_MNOC,
  ICBID_SLAVE_OCMEM,
  ICBID_SLAVE_GMEM = ICBID_SLAVE_OCMEM,
  ICBID_SLAVE_SERVICE_ONOC,
  ICBID_SLAVE_APPSS,
  ICBID_SLAVE_LPASS,
  ICBID_SLAVE_USB3,
  ICBID_SLAVE_USB3_0 = ICBID_SLAVE_USB3,
  ICBID_SLAVE_WCSS,
  ICBID_SLAVE_SNOC_BIMC,
  ICBID_SLAVE_SNOC_BIMC_0 = ICBID_SLAVE_SNOC_BIMC,
  ICBID_SLAVE_SNOC_CNOC,
  ICBID_SLAVE_IMEM,
  ICBID_SLAVE_OCIMEM = ICBID_SLAVE_IMEM,
  ICBID_SLAVE_SNOC_OVIRT,
  ICBID_SLAVE_SNOC_GVIRT = ICBID_SLAVE_SNOC_OVIRT,
  ICBID_SLAVE_SNOC_PNOC,
  ICBID_SLAVE_SNOC_PCNOC = ICBID_SLAVE_SNOC_PNOC,
  ICBID_SLAVE_SERVICE_SNOC,
  ICBID_SLAVE_QDSS_STM,
  ICBID_SLAVE_SDCC_1,
  ICBID_SLAVE_SDCC_3,
  ICBID_SLAVE_SDCC_2,
  ICBID_SLAVE_SDCC_4,
  ICBID_SLAVE_TSIF,
  ICBID_SLAVE_BAM_DMA,
  ICBID_SLAVE_BLSP_2,
  ICBID_SLAVE_USB_HSIC,
  ICBID_SLAVE_BLSP_1,
  ICBID_SLAVE_USB_HS,
  ICBID_SLAVE_USB_HS1 = ICBID_SLAVE_USB_HS,
  ICBID_SLAVE_PDM,
  ICBID_SLAVE_PERIPH_APU_CFG,
  ICBID_SLAVE_PNOC_MPU_CFG,
  ICBID_SLAVE_PRNG,
  ICBID_SLAVE_PNOC_SNOC,
  ICBID_SLAVE_PCNOC_SNOC = ICBID_SLAVE_PNOC_SNOC,
  ICBID_SLAVE_SERVICE_PNOC,
  ICBID_SLAVE_CLK_CTL,
  ICBID_SLAVE_CNOC_MSS,
  ICBID_SLAVE_PCNOC_MSS = ICBID_SLAVE_CNOC_MSS,
  ICBID_SLAVE_SECURITY,
  ICBID_SLAVE_TCSR,
  ICBID_SLAVE_TLMM,
  ICBID_SLAVE_CRYPTO_0_CFG,
  ICBID_SLAVE_CRYPTO_1_CFG,
  ICBID_SLAVE_IMEM_CFG,
  ICBID_SLAVE_MESSAGE_RAM,
  ICBID_SLAVE_BIMC_CFG,
  ICBID_SLAVE_BOOT_ROM,
  ICBID_SLAVE_CNOC_MNOC_MMSS_CFG,
  ICBID_SLAVE_PMIC_ARB,
  ICBID_SLAVE_SPDM_WRAPPER,
  ICBID_SLAVE_DEHR_CFG,
  ICBID_SLAVE_MPM,
  ICBID_SLAVE_QDSS_CFG,
  ICBID_SLAVE_RBCPR_CFG,
  ICBID_SLAVE_RBCPR_CX_CFG = ICBID_SLAVE_RBCPR_CFG,
  ICBID_SLAVE_RBCPR_QDSS_APU_CFG,
  ICBID_SLAVE_CNOC_MNOC_CFG,
  ICBID_SLAVE_SNOC_MPU_CFG,
  ICBID_SLAVE_CNOC_ONOC_CFG,
  ICBID_SLAVE_PNOC_CFG,
  ICBID_SLAVE_SNOC_CFG,
  ICBID_SLAVE_EBI1_DLL_CFG,
  ICBID_SLAVE_PHY_APU_CFG,
  ICBID_SLAVE_EBI1_PHY_CFG,
  ICBID_SLAVE_RPM,
  ICBID_SLAVE_CNOC_SNOC,
  ICBID_SLAVE_SERVICE_CNOC,
  ICBID_SLAVE_OVIRT_SNOC,
  ICBID_SLAVE_OVIRT_OCMEM,
  ICBID_SLAVE_USB_HS2,
  ICBID_SLAVE_QPIC,
  ICBID_SLAVE_IPS_CFG,
  ICBID_SLAVE_DSI_CFG,
  ICBID_SLAVE_USB3_1,
  ICBID_SLAVE_PCIE_0,
  ICBID_SLAVE_PCIE_1,
  ICBID_SLAVE_PSS_SMMU_CFG,
  ICBID_SLAVE_CRYPTO_2_CFG,
  ICBID_SLAVE_PCIE_0_CFG,
  ICBID_SLAVE_PCIE_1_CFG,
  ICBID_SLAVE_SATA_CFG,
  ICBID_SLAVE_SPSS_GENI_IR,
  ICBID_SLAVE_UFS_CFG,
  ICBID_SLAVE_AVSYNC_CFG,
  ICBID_SLAVE_VPU_CFG,
  ICBID_SLAVE_USB_PHY_CFG,
  ICBID_SLAVE_RBCPR_MX_CFG,
  ICBID_SLAVE_PCIE_PARF,
  ICBID_SLAVE_VCAP_CFG,
  ICBID_SLAVE_EMAC_CFG,
  ICBID_SLAVE_BCAST_CFG,
  ICBID_SLAVE_KLM_CFG,
  ICBID_SLAVE_DISPLAY_PWM,
  ICBID_SLAVE_GENI,
  ICBID_SLAVE_SNOC_BIMC_1,
  ICBID_SLAVE_AUDIO,
  ICBID_SLAVE_CATS_0,
  ICBID_SLAVE_CATS_1,
  ICBID_SLAVE_MM_INT_0,
  ICBID_SLAVE_MM_INT_1,
  ICBID_SLAVE_MM_INT_2,
  ICBID_SLAVE_MM_INT_BIMC,
  ICBID_SLAVE_MMU_MODEM_XPU_CFG,
  ICBID_SLAVE_MSS_INT,
  ICBID_SLAVE_PCNOC_INT_0,
  ICBID_SLAVE_PCNOC_INT_1,
  ICBID_SLAVE_PCNOC_M_0,
  ICBID_SLAVE_PCNOC_M_1,
  ICBID_SLAVE_PCNOC_S_0,
  ICBID_SLAVE_PCNOC_S_1,
  ICBID_SLAVE_PCNOC_S_2,
  ICBID_SLAVE_PCNOC_S_3,
  ICBID_SLAVE_PCNOC_S_4,
  ICBID_SLAVE_PCNOC_S_6,
  ICBID_SLAVE_PCNOC_S_7,
  ICBID_SLAVE_PCNOC_S_8,
  ICBID_SLAVE_PCNOC_S_9,
  ICBID_SLAVE_PRNG_XPU_CFG,
  ICBID_SLAVE_QDSS_INT,
  ICBID_SLAVE_RPM_XPU_CFG,
  ICBID_SLAVE_SNOC_INT_0,
  ICBID_SLAVE_SNOC_INT_1,
  ICBID_SLAVE_SNOC_INT_BIMC,
  ICBID_SLAVE_TCU,
  ICBID_SLAVE_BIMC_INT_0,
  ICBID_SLAVE_BIMC_INT_1,
  ICBID_SLAVE_RICA_CFG,
  ICBID_SLAVE_SNOC_BIMC_2,
  ICBID_SLAVE_BIMC_SNOC_1,
  ICBID_SLAVE_PNOC_A1NOC,
  ICBID_SLAVE_SNOC_VMEM,
  ICBID_SLAVE_A0NOC_SNOC,
  ICBID_SLAVE_A1NOC_SNOC,
  ICBID_SLAVE_A2NOC_SNOC,
  ICBID_SLAVE_A0NOC_CFG,
  ICBID_SLAVE_A0NOC_MPU_CFG,
  ICBID_SLAVE_A0NOC_SMMU_CFG,
  ICBID_SLAVE_A1NOC_CFG,
  ICBID_SLAVE_A1NOC_MPU_CFG,
  ICBID_SLAVE_A1NOC_SMMU_CFG,
  ICBID_SLAVE_A2NOC_CFG,
  ICBID_SLAVE_A2NOC_MPU_CFG,
  ICBID_SLAVE_A2NOC_SMMU_CFG,
  ICBID_SLAVE_AHB2PHY,
  ICBID_SLAVE_CAMERA_THROTTLE_CFG,
  ICBID_SLAVE_DCC_CFG,
  ICBID_SLAVE_DISPLAY_THROTTLE_CFG,
  ICBID_SLAVE_DSA_CFG,
  ICBID_SLAVE_DSA_MPU_CFG,
  ICBID_SLAVE_SSC_MPU_CFG,
  ICBID_SLAVE_HMSS_L3,
  ICBID_SLAVE_LPASS_SMMU_CFG,
  ICBID_SLAVE_MMAGIC_CFG,
  ICBID_SLAVE_PCIE20_AHB2PHY,
  ICBID_SLAVE_PCIE_2,
  ICBID_SLAVE_PCIE_2_CFG,
  ICBID_SLAVE_PIMEM,
  ICBID_SLAVE_PIMEM_CFG,
  ICBID_SLAVE_QDSS_RBCPR_APU_CFG,
  ICBID_SLAVE_RBCPR_CX,
  ICBID_SLAVE_RBCPR_MX,
  ICBID_SLAVE_SMMU_CPP_CFG,
  ICBID_SLAVE_SMMU_JPEG_CFG,
  ICBID_SLAVE_SMMU_MDP_CFG,
  ICBID_SLAVE_SMMU_ROTATOR_CFG,
  ICBID_SLAVE_SMMU_VENUS_CFG,
  ICBID_SLAVE_SMMU_VFE_CFG,
  ICBID_SLAVE_SSC_CFG,
  ICBID_SLAVE_VENUS_THROTTLE_CFG,
  ICBID_SLAVE_VMEM,
  ICBID_SLAVE_VMEM_CFG,
  ICBID_SLAVE_QDSS_MPU_CFG,
  ICBID_SLAVE_USB3_PHY_CFG,
  ICBID_SLAVE_IPA_CFG,
  ICBID_SLAVE_PCNOC_INT_2,
  ICBID_SLAVE_PCNOC_INT_3,
  ICBID_SLAVE_PCNOC_INT_4,
  ICBID_SLAVE_PCNOC_INT_5,
  ICBID_SLAVE_PCNOC_INT_6,
  ICBID_SLAVE_PCNOC_S_5,
  ICBID_SLAVE_QSPI,
  ICBID_SLAVE_A1NOC_MS_MPU_CFG,
  ICBID_SLAVE_A2NOC_MS_MPU_CFG,
  ICBID_SLAVE_MODEM_Q6_SMMU_CFG,
  ICBID_SLAVE_MSS_MPU_CFG,
  ICBID_SLAVE_MSS_PROC_MS_MPU_CFG,
  ICBID_SLAVE_SKL,
  ICBID_SLAVE_SNOC_INT_2,
  ICBID_SLAVE_SMMNOC_BIMC,
  ICBID_SLAVE_CRVIRT_A1NOC,
  ICBID_SLAVE_COUNT,
  ICBID_SLAVE_SIZE = 0x7FFFFFFF
} ICBId_SlaveType;
typedef struct ul_treeNode
{
  uint64 key;
  void * value;
  struct ul_treeNode * left;
  struct ul_treeNode * right;
} ul_treeNodeType;
extern void * ul_TreeLookup( ul_treeNodeType * root, uint64 key );
typedef _Ptrdifft ptrdiff_t;
typedef _Wchart wchar_t;
typedef signed char int8_t;
typedef short int16_t;
typedef _Int32t int32_t;
typedef unsigned char uint8_t;
typedef unsigned short uint16_t;
typedef _Uint32t uint32_t;
typedef signed char int_least8_t;
typedef short int_least16_t;
typedef _Int32t int_least32_t;
typedef unsigned char uint_least8_t;
typedef unsigned short uint_least16_t;
typedef _Uint32t uint_least32_t;
typedef signed char int_fast8_t;
typedef short int_fast16_t;
typedef _Int32t int_fast32_t;
typedef unsigned char uint_fast8_t;
typedef unsigned short uint_fast16_t;
typedef _Uint32t uint_fast32_t;
typedef unsigned int uintptr_t;
typedef int intptr_t;
typedef _Longlong int64_t;
typedef _ULonglong uint64_t;
typedef _Longlong int_least64_t;
typedef _ULonglong uint_least64_t;
typedef _Longlong int_fast64_t;
typedef _ULonglong uint_fast64_t;
typedef _Longlong intmax_t;
typedef _ULonglong uintmax_t;
typedef struct
{
  uint32_t uBusId;
  uint32_t uAddrWidth;
  uint32_t uDataWidth;
  uint32_t uNumMasters;
  uint32_t uNumSlaves;
  uint32_t uHWRevision;
  uint32_t uNumSegments;
} HAL_bimc_DeviceParamsType;
typedef struct
{
  uint8_t * pBaseAddr;
  uint32_t uQosFreq;
  HAL_bimc_DeviceParamsType devParams;
} HAL_bimc_InfoType;
typedef struct
{
  uint32_t uConnMask;
  uint32_t uDataWidth;
} HAL_bimc_MasterPortParamsType;
typedef enum
{
  BIMC_ARB_MODE_RR = 0,
  BIMC_ARB_MODE_PRIORITY_RR = 1,
  BIMC_ARB_MODE_TIERED_RR = 2,
  BIMC_ARB_MODE_COUNT,
  BIMC_ARB_MODE_WIDTH = 0x7FFFFFFF
} HAL_bimc_ArbModeType;
typedef struct
{
  uint32_t uConnMask;
  uint32_t uDataWidth;
  HAL_bimc_ArbModeType eArbMode;
} HAL_bimc_SlavePortParamsType;
typedef enum
{
  BIMC_SEGMENT_TYPE_ADDITIVE = 0,
  BIMC_SEGMENT_TYPE_SUBTRACTIVE = 1,
  BIMC_SEGMENT_TYPE_COUNT,
  BIMC_SEGMENT_TYPE_WIDTH = 0x7FFFFFFF
} HAL_bimc_SegmentType;
typedef enum
{
  BIMC_INTERLEAVE_NONE = 0,
  BIMC_INTERLEAVE_ODD,
  BIMC_INTERLEAVE_EVEN,
  BIMC_INTERLEAVE_COUNT,
  BIMC_INTERLEAVE_WIDTH = 0x7FFFFFFF
} HAL_bimc_InterleaveType;
typedef struct
{
  _Bool bEnable;
  uint64_t uStartAddr;
  uint64_t uSegmentSize;
  HAL_bimc_SegmentType type;
  HAL_bimc_InterleaveType interleave;
} HAL_bimc_SlaveSegmentType;
typedef struct
{
  _Bool bEnable;
  uint64_t uStartAddr;
  uint64_t uSegmentSize;
  HAL_bimc_InterleaveType interleave;
  int64_t nSegmentOffset;
  _Bool bDeinterleave;
} HAL_bimc_RemapSegmentType;
typedef struct
{
  uint32_t uPolicyMask;
  uint32_t uPolicySel;
} HAL_bimc_QosPolicyType;
typedef enum
{
  BIMC_QOS_MODE_FIXED = 0,
  BIMC_QOS_MODE_LIMITER = 1,
  BIMC_QOS_MODE_BYPASS = 2,
  BIMC_QOS_MODE_REGULATOR = 3,
  BIMC_QOS_MODE_COUNT,
  BIMC_QOS_MODE_WIDTH = 0x7FFFFFFF,
} HAL_bimc_QosModeType;
typedef struct
{
  _Bool bLimitCommands;
  uint32_t uAReqPriority;
  uint32_t uPriorityLvl;
} HAL_bimc_QosHealthType;
typedef struct
{
  struct
  {
    uint32_t uPriorityLvl;
    uint32_t uAReqPriorityRead;
    uint32_t uAReqPriorityWrite;
  } fixed;
  struct
  {
    HAL_bimc_QosHealthType health3;
    HAL_bimc_QosHealthType health2;
    HAL_bimc_QosHealthType health1;
    HAL_bimc_QosHealthType health0;
  } regulator;
} HAL_bimc_QosPriorityType;
typedef struct
{
  uint64_t uBandwidth;
  uint32_t uWindowSize;
  uint64_t uThresholdHigh;
  uint64_t uThresholdMedium;
  uint64_t uThresholdLow;
} HAL_bimc_QosBandwidthType;
typedef struct
{
  _Bool bCoreClockGateEn;
  _Bool bArbClockGateEn;
  _Bool bPortClockGateEn;
} HAL_bimc_ClockGatingType;
void HAL_bimc_Init( HAL_bimc_InfoType *pInfo );
void HAL_bimc_Reset( HAL_bimc_InfoType *pInfo ) ;
void HAL_bimc_Save( HAL_bimc_InfoType *pInfo );
void HAL_bimc_Restore ( HAL_bimc_InfoType *pInfo );
void HAL_bimc_SetSlaveSegment( HAL_bimc_InfoType * pInfo,
                               uint32_t uSlaveIdx,
                               uint32_t uSegmentIdx,
                               HAL_bimc_SlaveSegmentType *pSegment );
void HAL_bimc_SetRemapSegment( HAL_bimc_InfoType * pInfo,
                               uint32_t uSegmentIdx,
                               HAL_bimc_RemapSegmentType *pSegment );
void HAL_bimc_SetSlaveClockGating( HAL_bimc_InfoType * pInfo,
                                   uint32_t uSlaveIdx,
                                   HAL_bimc_ClockGatingType *pGating );
void HAL_bimc_SetMasterClockGating( HAL_bimc_InfoType * pInfo,
                                    uint32_t uMasterIdx,
                                    HAL_bimc_ClockGatingType *pGating );
void HAL_bimc_ArbitrationEnable( HAL_bimc_InfoType *pInfo,
                                 uint32_t uSlaveIdx,
                                 _Bool bEnable );
void HAL_bimc_SetQosPolicy( HAL_bimc_InfoType * pInfo,
                            uint32_t uMasterIdx,
                            HAL_bimc_QosPolicyType *pPolicy );
void HAL_bimc_SetQosMode( HAL_bimc_InfoType * pInfo,
                          uint32_t uMasterIdx,
                          HAL_bimc_QosModeType eMode );
void HAL_bimc_SetQosPriority( HAL_bimc_InfoType * pInfo,
                              uint32_t uMasterIdx,
                              HAL_bimc_QosModeType eMode,
                              HAL_bimc_QosPriorityType *pPriority );
void HAL_bimc_SetQosBandwidth( HAL_bimc_InfoType * pInfo,
                               uint32_t uMasterIdx,
                               HAL_bimc_QosBandwidthType *pBandwidth );
void HAL_bimc_SetDangerPolicy( HAL_bimc_InfoType * pInfo,
                               uint32_t uMasterIdx,
                               uint32_t uContext,
                               HAL_bimc_QosPolicyType *pPolicy );
void HAL_bimc_SetDangerMode( HAL_bimc_InfoType * pInfo,
                             uint32_t uMasterIdx,
                             uint32_t uContext,
                             _Bool bEnable );
void HAL_bimc_SetDangerPriority( HAL_bimc_InfoType * pInfo,
                                 uint32_t uMasterIdx,
                                 uint32_t uContext,
                                 HAL_bimc_QosPriorityType *pPriority );
void HAL_bimc_SetDangerBandwidth( HAL_bimc_InfoType * pInfo,
                                  uint32_t uMasterIdx,
                                  uint32_t uContext,
                                  HAL_bimc_QosBandwidthType *pBandwidth );
void HAL_bimc_GetDeviceParameters( HAL_bimc_InfoType * pInfo,
                                   HAL_bimc_DeviceParamsType *pDevParams );
void HAL_bimc_GetMasterParameters( HAL_bimc_InfoType * pInfo,
                                   uint32_t uMasterIdx,
                                   HAL_bimc_MasterPortParamsType *pPortParams );
void HAL_bimc_GetSlaveParameters( HAL_bimc_InfoType * pInfo,
                                  uint32_t uSlaveIdx,
                                  HAL_bimc_SlavePortParamsType *pPortParams );
_Bool HAL_bimc_GetArbitrationEnable( HAL_bimc_InfoType *pInfo,
                                    uint32_t uSlaveIdx );
void HAL_bimc_GetSlaveSegment( HAL_bimc_InfoType * pInfo,
                               uint32_t uSlaveIdx,
                               uint32_t uSegmentIdx,
                               HAL_bimc_SlaveSegmentType *pSegment );
void HAL_bimc_GetRemapSegment( HAL_bimc_InfoType * pInfo,
                               uint32_t uSegmentIdx,
                               HAL_bimc_RemapSegmentType *pSegment );
_Bool HAL_bimc_IsRemapSupported( HAL_bimc_InfoType *pInfo );
typedef enum
{
  NOC_QOS_MODE_FIXED = 0,
  NOC_QOS_MODE_LIMITER = 1,
  NOC_QOS_MODE_BYPASS = 2,
  NOC_QOS_MODE_REGULATOR = 3,
  NOC_QOS_MODE_COUNT,
} HAL_noc_QosModeType;
typedef enum
{
  NOC_QOS_ALLOWED_MODE_FIXED = (1 << NOC_QOS_MODE_FIXED),
  NOC_QOS_ALLOWED_MODE_LIMITER = (1 << NOC_QOS_MODE_LIMITER),
  NOC_QOS_ALLOWED_MODE_BYPASS = (1 << NOC_QOS_MODE_BYPASS),
  NOC_QOS_ALLOWED_MODE_REGULATOR = (1 << NOC_QOS_MODE_REGULATOR),
} HAL_noc_QosMaskType;
typedef struct
{
  uint32_t uAllowedModes;
} HAL_noc_QosMasterInfoType;
typedef enum
{
  NOC_QOS_OFFSET_LEGACY = 0,
  NOC_QOS_OFFSET_0x7_0x1,
  NOC_QOS_OFFSET_0x5000_0x80,
  NOC_QOS_OFFSET_INVALID,
  NOC_QOS_OFFSET_COUNT,
} HAL_noc_QosOffsetEnumType;
typedef struct
{
  uint8_t * pBaseAddr;
  uint32_t uNumMasters;
  uint32_t uNumQosMasters;
  uint32_t uNumSlaves;
  uint32_t uQosFreq;
  HAL_noc_QosMasterInfoType *aMasters;
  _Bool bSaveValid;
  uint32_t *pSaveBuf;
  uint32_t uNumAddrs;
  uint32_t **paAddrList;
  HAL_noc_QosOffsetEnumType eOffsetType;
  uint32_t uQosOffset;
  uint32_t uQosSize;
} HAL_noc_InfoType;
typedef struct
{
  uint32_t p1;
  uint32_t p0;
} HAL_noc_QosPriorityType;
typedef struct
{
  uint64_t uBandwidth;
  uint32_t uWindowSize;
} HAL_noc_QosBandwidthType;
void HAL_noc_Init( HAL_noc_InfoType * pInfo );
void HAL_noc_Reset( HAL_noc_InfoType * pInfo ) ;
void HAL_noc_Save( HAL_noc_InfoType * pInfo );
void HAL_noc_Restore( HAL_noc_InfoType * pInfo );
void HAL_noc_SetQosMode( HAL_noc_InfoType * pInfo,
                         uint32_t uMasterPort,
                         HAL_noc_QosModeType eMode );
void HAL_noc_SetQosPriority( HAL_noc_InfoType * pInfo,
                             uint32_t uMasterPort,
                             HAL_noc_QosModeType eMode,
                             HAL_noc_QosPriorityType *pPriority );
void HAL_noc_SetQosBandwidth( HAL_noc_InfoType * pInfo,
                              uint32_t uMasterPort,
                              HAL_noc_QosBandwidthType *pBandwidth );
void HAL_noc_GetQosMode( HAL_noc_InfoType * pInfo,
                         uint32_t uMasterPort,
                         HAL_noc_QosModeType *pMode );
void HAL_noc_GetQosPriority( HAL_noc_InfoType * pInfo,
                             uint32_t uMasterPort,
                             HAL_noc_QosPriorityType *pPriority );
void HAL_noc_GetQosBandwidth( HAL_noc_InfoType * pInfo,
                              uint32_t uMasterPort,
                              HAL_noc_QosBandwidthType *pBandwidth );
typedef struct
{
  char * pszBaseName;
  uint8_t *pBaseAddr;
  uint32_t uRegionSize;
  uint32_t uNumPorts;
  uint32_t uHaltOffset;
  uint32_t uHaltBlockSize;
  uint32_t uTimeoutCntUs;
  uint32_t uTimeoutCnt;
} HAL_tcsr_InfoType;
void HAL_tcsr_Init( HAL_tcsr_InfoType * pInfo );
void HAL_tcsr_Reset( HAL_tcsr_InfoType * pInfo ) ;
void HAL_tcsr_Save( HAL_tcsr_InfoType * pInfo );
void HAL_tcsr_Restore( HAL_tcsr_InfoType * pInfo );
_Bool HAL_tcsr_HaltPort( HAL_tcsr_InfoType * pInfo,
                        uint32_t uPort );
_Bool HAL_tcsr_UnhaltPort( HAL_tcsr_InfoType * pInfo,
                          uint32_t uPort );
typedef struct
{
  uint32_t uAddr;
  uint32_t uMask;
} icb_segment_def_type;
typedef enum
{
  ICB_BUS_BIMC = 0,
  ICB_BUS_NOC,
  ICB_BUS_VIRT,
} icb_bus_type;
typedef enum
{
  ICB_AGG_SCHEME_UNUSED = 0,
  ICB_AGG_SCHEME_LEGACY = 0,
  ICB_AGG_SCHEME_0
} icb_agg_type;
typedef struct
{
  icb_agg_type aggType;
  uint32_t uP0;
  uint32_t uP1;
} icb_agg_info_type;
typedef struct
{
  char * pszBaseName;
  _Bool bRemote;
  uint8_t * pBaseAddr;
  uint32_t uRegionSize;
  icb_bus_type busType;
  uint32_t uAddrWidth;
  uint32_t uDataWidth;
  void * pInfo;
  uint32_t uNumCfgClks;
  const char * const * aCfgClkNames;
  npa_client_handle * aCfgClkClients;
  uint32_t uNumEnCfgClks;
  const char * const * aEnCfgClkNames;
  uint32_t * aEnCfgClkIds;
  icb_agg_info_type aggInfo;
} icb_bus_def_type;
typedef enum
{
  ICB_BW_REQ_TYPE_DEFAULT = 0,
  ICB_BW_REQ_TYPE_STATIC = (1 << 0),
  ICB_BW_REQ_TYPE_THRESHOLD = (1 << 1),
} icb_bw_req_type;
typedef struct
{
  uint64_t uBandwidth;
  uint32_t uWindowSize;
  uint32_t uThresholdHighPct;
  uint32_t uThresholdMedPct;
  uint32_t uThresholdLowPct;
} icb_bimc_bw_def_type;
typedef struct
{
  HAL_bimc_ClockGatingType bimcClockGate;
  icb_bw_req_type reqType;
  HAL_bimc_QosModeType bimcMode[2];
  HAL_bimc_QosPriorityType bimcPriority;
  icb_bimc_bw_def_type bimcBandwidth;
  uint64_t bwThreshold;
  _Bool bApplied[2];
} icb_bimc_master_defaults_type;
typedef struct
{
  HAL_noc_QosModeType nocMode;
  HAL_noc_QosPriorityType nocPriority;
  HAL_noc_QosBandwidthType nocBandwidth;
  icb_bw_req_type reqType;
  _Bool bApplied;
} icb_noc_master_defaults_type;
typedef struct
{
  ICBId_MasterType masterId;
  icb_bus_def_type *pBus;
  uint32_t uNumPorts;
  uint32_t * pPortIds;
  uint32_t * pQosPortIds;
  _Bool bRemote;
  uint32_t uWindowSize;
  void * pDefaults;
  uint32_t * pHaltPorts;
  _Bool bLatency;
  uint32_t uNumAggPorts;
  icb_agg_info_type aggInfo;
  void * pQosPolicyOverride;
} icb_master_def_type;
typedef struct
{
  HAL_bimc_ClockGatingType bimcClockGate;
  _Bool bArbEn;
} icb_bimc_slave_defaults_type;
typedef struct
{
  ICBId_SlaveType slaveId;
  icb_bus_def_type *pBus;
  uint32_t uNumPorts;
  uint32_t * pPortIds;
  _Bool bRemote;
  uint32_t uNumSegments;
  void * pDefaults;
  uint32_t uNumAggPorts;
  icb_agg_info_type aggInfo;
} icb_slave_def_type;
typedef struct ul_request_queue_node
{
  struct ul_request_queue_node * next;
  npa_client_handle hClient;
  DALBOOL bQueued;
  void * pRequest;
  void (*req_func)( struct ul_request_queue_node *);
} ul_request_type;
typedef struct
{
  uint64 uIb;
  uint64 uAb;
  npa_client_handle hClient;
  npa_client_type clientType;
  uint32 uLatencyNs;
} ul_bw_request_type;
typedef struct
{
  uint32 uEntryCount;
  uint32 uListSize;
  ul_bw_request_type ** aRequestList;
} ul_bw_request_list_type;
typedef struct
{
  uint64 uTotalBW;
  uint64 uIb;
  uint64 uSuppIb;
  uint64 uActiveOnlyIb;
  uint64 uAb;
  uint64 uSuppAb;
  uint64 uActiveOnlyAb;
} ul_node_state_type;
typedef struct
{
  const char * const name;
  npa_client_handle normal;
  npa_client_handle suppressible;
  npa_client_handle activeonly;
} ul_clock_type;
typedef struct
{
  const char * clientName;
  ICBId_MasterType eExtMasterID;
  uint32 u32SlaveWidth;
  uint32 u32MstrClockCount;
  ul_clock_type * aMstrClocks;
  ul_node_state_type mstrState;
  ul_request_type masterRequest;
  icb_master_def_type * pMasterDef;
  ul_bw_request_list_type requestList;
  ul_request_type latencyRequest;
} ul_master_data_type;
typedef struct
{
  const char * clientName;
  ICBId_SlaveType eExtSlaveID;
  uint32 u32SlaveWidth;
  ul_node_state_type slvState;
  uint32 u32SlvClockCount;
  ul_clock_type * aSlvClocks;
  uint32 u32MemClockCount;
  ul_clock_type * aMemClocks;
  ul_bw_request_list_type requestList;
  ul_request_type slvRequest;
  icb_slave_def_type * pSlaveDef;
} ul_slave_data_type;
typedef struct
{
  ul_master_data_type * pMasterData;
  ul_slave_data_type * pSlaveData;
} ul_pair_type;
typedef struct
{
  uint32 u32NumPairs;
  ul_pair_type * aMasterSlavePairs;
} ul_route_list_type;
typedef struct
{
  ICBId_MasterType eExtMasterID;
  ICBId_SlaveType eExtSlaveID;
  ul_route_list_type * pRouteList;
} ul_route_type;
typedef struct
{
  uint32 uNumMasters;
  ul_master_data_type ** aMasterList;
} ul_int_master_list_type;
typedef struct
{
  uint32 uNumSlaves;
  ul_slave_data_type ** aSlaveList;
} ul_int_slave_list_type;
void ul_issue_pair_request
(
  npa_client_handle client,
  ul_bw_request_type * pBWReq,
  ul_bw_request_type * pOldReq,
  ul_pair_type * pMSPair,
  DALBOOL bFirst
);
void ul_add_bw_request
(
  ul_bw_request_type * pBWReq,
  ul_bw_request_list_type * pRequestList
);
void ul_remove_bw_request
(
  ul_bw_request_type * pBWReq,
  ul_bw_request_list_type * pRequestList
);
void ul_internal_init( void );
ul_route_list_type *ul_get_route
(
  ICBId_MasterType eMaster,
  ICBId_SlaveType eSlave
);
void ul_request_enqueue ( ul_request_type * req );
void ul_int_init( void );
void ul_target_init( void );
ul_treeNodeType * ul_target_get_route_tree( void );
void ul_target_get_slave_list( ul_int_slave_list_type *pSlaveList );
ul_node_state_type *ul_get_master_state( ICBId_MasterType eExtMasterID );
ul_node_state_type *ul_get_slave_state( ICBId_SlaveType eExtSlaveID );
static HAL_noc_QosMasterInfoType a1noc_master_info[2] =
{
  {NOC_QOS_ALLOWED_MODE_BYPASS | NOC_QOS_ALLOWED_MODE_FIXED},
  {NOC_QOS_ALLOWED_MODE_BYPASS | NOC_QOS_ALLOWED_MODE_FIXED},
};
static HAL_noc_InfoType a1noc_hal_info =
{
  0,
  2,
  2,
  1,
  19200,
  a1noc_master_info,
  0,
  0,
  0,
  0,
  NOC_QOS_OFFSET_INVALID,
  0x2000,
  0x1000,
};
static const char *bus_a1noc_clk_names[1] =
{
  "/clk/agr1",
};
static npa_client_handle bus_a1noc_clk_clients[1] = {0};
static icb_bus_def_type a1noc_bus =
{
  "A1_NOC_AGGRE1_NOC",
  1,
  0,
  0x4000,
  ICB_BUS_NOC,
  0,
  16,
  (void *)&a1noc_hal_info,
  1,
  bus_a1noc_clk_names,
  bus_a1noc_clk_clients,
  0,
  0,
  0,
  {
    ICB_AGG_SCHEME_LEGACY,
    0,
    0,
  },
};
static HAL_bimc_InfoType bimc_hal_info =
{
  0,
  19200,
};
static const char *bus_bimc_clk_names[1] =
{
  "/clk/bimc",
};
static npa_client_handle bus_bimc_clk_clients[1] = {0};
static icb_bus_def_type bimc_bus =
{
  "BIMC",
  1,
  0,
  0x62000,
  ICB_BUS_BIMC,
  0,
  8,
  (void *)&bimc_hal_info,
  1,
  bus_bimc_clk_names,
  bus_bimc_clk_clients,
  0,
  0,
  0,
  {
    ICB_AGG_SCHEME_0,
    153,
    100,
  },
};
static HAL_noc_InfoType cnoc_hal_info =
{
  0,
  1,
  0,
  6,
  19200,
  0,
  0,
  0,
  0,
  0,
  NOC_QOS_OFFSET_LEGACY,
};
static const char *bus_cnoc_clk_names[1] =
{
  "/clk/cnoc",
};
static npa_client_handle bus_cnoc_clk_clients[1] = {0};
static icb_bus_def_type cnoc_bus =
{
  "CONFIG_NOC",
  1,
  0,
  0x1000,
  ICB_BUS_NOC,
  0,
  8,
  (void *)&cnoc_hal_info,
  1,
  bus_cnoc_clk_names,
  bus_cnoc_clk_clients,
  0,
  0,
  0,
  {
    ICB_AGG_SCHEME_LEGACY,
    0,
    0,
  },
};
static icb_bus_def_type cr_virt_bus =
{
  "",
  1,
  0,
  0x0,
  ICB_BUS_VIRT,
  0,
  8,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  {
    ICB_AGG_SCHEME_LEGACY,
    0,
    0,
  },
};
static HAL_noc_InfoType pnoc_hal_info =
{
  0,
  8,
  0,
  9,
  19200,
  0,
  0,
  0,
  0,
  0,
  NOC_QOS_OFFSET_LEGACY,
};
static const char *bus_pnoc_clk_names[1] =
{
  "/clk/pnoc",
};
static npa_client_handle bus_pnoc_clk_clients[1] = {0};
static icb_bus_def_type pnoc_bus =
{
  "PERIPH_NOC",
  1,
  0,
  0x3000,
  ICB_BUS_NOC,
  0,
  8,
  (void *)&pnoc_hal_info,
  1,
  bus_pnoc_clk_names,
  bus_pnoc_clk_clients,
  0,
  0,
  0,
  {
    ICB_AGG_SCHEME_LEGACY,
    0,
    0,
  },
};
static HAL_noc_QosMasterInfoType snoc_master_info[6] =
{
  {NOC_QOS_ALLOWED_MODE_BYPASS | NOC_QOS_ALLOWED_MODE_FIXED},
  {0},
  {0},
  {0},
  {0},
  {NOC_QOS_ALLOWED_MODE_BYPASS | NOC_QOS_ALLOWED_MODE_FIXED},
};
static HAL_noc_InfoType snoc_hal_info =
{
  0,
  3,
  6,
  6,
  19200,
  snoc_master_info,
  0,
  0,
  0,
  0,
  NOC_QOS_OFFSET_INVALID,
  0x4000,
  0x1000,
};
static const char *bus_snoc_clk_names[1] =
{
  "/clk/snoc",
};
static npa_client_handle bus_snoc_clk_clients[1] = {0};
static icb_bus_def_type snoc_bus =
{
  "SYSTEM_NOC",
  0,
  0,
  0xB000,
  ICB_BUS_NOC,
  0,
  16,
  (void *)&snoc_hal_info,
  1,
  bus_snoc_clk_names,
  bus_snoc_clk_clients,
  0,
  0,
  0,
  {
    ICB_AGG_SCHEME_LEGACY,
    0,
    0,
  },
};
static uint32_t cfg_slave_a1noc_snoc_ports[1] =
{
  0,
};
static icb_slave_def_type cfg_slave_a1noc_snoc =
{
  ICBID_SLAVE_A1NOC_SNOC,
  &a1noc_bus,
  1,
  cfg_slave_a1noc_snoc_ports,
  1,
  3,
  0,
  1,
};
static uint32_t cfg_slave_blsp_1_ports[1] =
{
  8,
};
static icb_slave_def_type cfg_slave_blsp_1 =
{
  ICBID_SLAVE_BLSP_1,
  &pnoc_bus,
  1,
  cfg_slave_blsp_1_ports,
  1,
  3,
  0,
  1,
};
static uint32_t cfg_slave_blsp_2_ports[1] =
{
  5,
};
static icb_slave_def_type cfg_slave_blsp_2 =
{
  ICBID_SLAVE_BLSP_2,
  &pnoc_bus,
  1,
  cfg_slave_blsp_2_ports,
  1,
  3,
  0,
  1,
};
static uint32_t cfg_slave_clk_ctl_ports[1] =
{
  3,
};
static icb_slave_def_type cfg_slave_clk_ctl =
{
  ICBID_SLAVE_CLK_CTL,
  &cnoc_bus,
  1,
  cfg_slave_clk_ctl_ports,
  1,
  3,
  0,
  1,
};
static uint32_t cfg_slave_cr_virt_a1noc_ports[1] =
{
  0,
};
static icb_slave_def_type cfg_slave_cr_virt_a1noc =
{
  ICBID_SLAVE_CRVIRT_A1NOC,
  &cr_virt_bus,
  1,
  cfg_slave_cr_virt_a1noc_ports,
  0,
  3,
  0,
  1,
};
static uint32_t cfg_slave_ebi_ports[2] =
{
  0,
  1,
};
static icb_slave_def_type cfg_slave_ebi =
{
  ICBID_SLAVE_EBI1,
  &bimc_bus,
  2,
  cfg_slave_ebi_ports,
  1,
  3,
  0,
  2,
};
static uint32_t cfg_slave_hmss_l3_ports[1] =
{
  2,
};
static icb_slave_def_type cfg_slave_hmss_l3 =
{
  ICBID_SLAVE_HMSS_L3,
  &bimc_bus,
  1,
  cfg_slave_hmss_l3_ports,
  1,
  3,
  0,
  1,
};
static uint32_t cfg_slave_imem_ports[1] =
{
  6,
};
static icb_slave_def_type cfg_slave_imem =
{
  ICBID_SLAVE_IMEM,
  &snoc_bus,
  1,
  cfg_slave_imem_ports,
  1,
  3,
  0,
  1,
};
static uint32_t cfg_slave_message_ram_ports[1] =
{
  10,
};
static icb_slave_def_type cfg_slave_message_ram =
{
  ICBID_SLAVE_MESSAGE_RAM,
  &cnoc_bus,
  1,
  cfg_slave_message_ram_ports,
  1,
  3,
  0,
  1,
};
static uint32_t cfg_slave_pdm_ports[1] =
{
  9,
};
static icb_slave_def_type cfg_slave_pdm =
{
  ICBID_SLAVE_PDM,
  &pnoc_bus,
  1,
  cfg_slave_pdm_ports,
  1,
  3,
  0,
  1,
};
static uint32_t cfg_slave_pimem_ports[1] =
{
  7,
};
static icb_slave_def_type cfg_slave_pimem =
{
  ICBID_SLAVE_PIMEM,
  &snoc_bus,
  1,
  cfg_slave_pimem_ports,
  1,
  3,
  0,
  1,
};
static uint32_t cfg_slave_pmic_arb_ports[1] =
{
  13,
};
static icb_slave_def_type cfg_slave_pmic_arb =
{
  ICBID_SLAVE_PMIC_ARB,
  &cnoc_bus,
  1,
  cfg_slave_pmic_arb_ports,
  1,
  3,
  0,
  1,
};
static uint32_t cfg_slave_pnoc_a1noc_ports[1] =
{
  0,
};
static icb_slave_def_type cfg_slave_pnoc_a1noc =
{
  ICBID_SLAVE_PNOC_A1NOC,
  &pnoc_bus,
  1,
  cfg_slave_pnoc_a1noc_ports,
  1,
  3,
  0,
  1,
};
static uint32_t cfg_slave_qdss_stm_ports[1] =
{
  10,
};
static icb_slave_def_type cfg_slave_qdss_stm =
{
  ICBID_SLAVE_QDSS_STM,
  &snoc_bus,
  1,
  cfg_slave_qdss_stm_ports,
  1,
  3,
  0,
  1,
};
static uint32_t cfg_slave_rpm_ports[1] =
{
  31,
};
static icb_slave_def_type cfg_slave_rpm =
{
  ICBID_SLAVE_RPM,
  &cnoc_bus,
  1,
  cfg_slave_rpm_ports,
  1,
  3,
  0,
  1,
};
static uint32_t cfg_slave_sdcc_1_ports[1] =
{
  7,
};
static icb_slave_def_type cfg_slave_sdcc_1 =
{
  ICBID_SLAVE_SDCC_1,
  &pnoc_bus,
  1,
  cfg_slave_sdcc_1_ports,
  1,
  3,
  0,
  1,
};
static uint32_t cfg_slave_sdcc_2_ports[1] =
{
  2,
};
static icb_slave_def_type cfg_slave_sdcc_2 =
{
  ICBID_SLAVE_SDCC_2,
  &pnoc_bus,
  1,
  cfg_slave_sdcc_2_ports,
  1,
  3,
  0,
  1,
};
static uint32_t cfg_slave_sdcc_4_ports[1] =
{
  3,
};
static icb_slave_def_type cfg_slave_sdcc_4 =
{
  ICBID_SLAVE_SDCC_4,
  &pnoc_bus,
  1,
  cfg_slave_sdcc_4_ports,
  1,
  3,
  0,
  1,
};
static uint32_t cfg_slave_snoc_bimc_ports[2] =
{
  3,
  4,
};
static icb_slave_def_type cfg_slave_snoc_bimc =
{
  ICBID_SLAVE_SNOC_BIMC,
  &snoc_bus,
  2,
  cfg_slave_snoc_bimc_ports,
  1,
  3,
  0,
  2,
};
static uint32_t cfg_slave_snoc_cnoc_ports[1] =
{
  5,
};
static icb_slave_def_type cfg_slave_snoc_cnoc =
{
  ICBID_SLAVE_SNOC_CNOC,
  &snoc_bus,
  1,
  cfg_slave_snoc_cnoc_ports,
  1,
  3,
  0,
  1,
};
static uint32_t cfg_slave_snoc_pnoc_ports[1] =
{
  9,
};
static icb_slave_def_type cfg_slave_snoc_pnoc =
{
  ICBID_SLAVE_SNOC_PNOC,
  &snoc_bus,
  1,
  cfg_slave_snoc_pnoc_ports,
  1,
  3,
  0,
  1,
};
static uint32_t cfg_slave_tcsr_ports[1] =
{
  4,
};
static icb_slave_def_type cfg_slave_tcsr =
{
  ICBID_SLAVE_TCSR,
  &cnoc_bus,
  1,
  cfg_slave_tcsr_ports,
  1,
  3,
  0,
  1,
};
static uint32_t cfg_slave_tlmm_ports[1] =
{
  5,
};
static icb_slave_def_type cfg_slave_tlmm =
{
  ICBID_SLAVE_TLMM,
  &cnoc_bus,
  1,
  cfg_slave_tlmm_ports,
  1,
  3,
  0,
  1,
};
static uint32_t cfg_slave_tsif_ports[1] =
{
  4,
};
static icb_slave_def_type cfg_slave_tsif =
{
  ICBID_SLAVE_TSIF,
  &pnoc_bus,
  1,
  cfg_slave_tsif_ports,
  1,
  3,
  0,
  1,
};
static uint32_t cfg_slave_usb_hs_ports[1] =
{
  1,
};
static icb_slave_def_type cfg_slave_usb_hs =
{
  ICBID_SLAVE_USB_HS,
  &pnoc_bus,
  1,
  cfg_slave_usb_hs_ports,
  1,
  3,
  0,
  1,
};
static uint32_t cfg_master_a1noc_snoc_ports[1] =
{
  7,
};
static icb_master_def_type cfg_master_a1noc_snoc =
{
  ICBID_MASTER_A1NOC_SNOC,
  &snoc_bus,
  1,
  cfg_master_a1noc_snoc_ports,
  0,
  1,
  0,
  0,
  0,
  0,
  1,
};
static uint32_t cfg_master_blsp_1_ports[1] =
{
  5,
};
static icb_master_def_type cfg_master_blsp_1 =
{
  ICBID_MASTER_BLSP_1,
  &pnoc_bus,
  1,
  cfg_master_blsp_1_ports,
  0,
  1,
  0,
  0,
  0,
  0,
  1,
};
static uint32_t cfg_master_blsp_2_ports[1] =
{
  6,
};
static icb_master_def_type cfg_master_blsp_2 =
{
  ICBID_MASTER_BLSP_2,
  &pnoc_bus,
  1,
  cfg_master_blsp_2_ports,
  0,
  1,
  0,
  0,
  0,
  0,
  1,
};
static uint32_t cfg_master_cr_virt_a1noc_ports[1] =
{
  1,
};
static uint32_t cfg_master_cr_virt_a1noc_qos_ports[1] =
{
  0,
};
static icb_master_def_type cfg_master_cr_virt_a1noc =
{
  ICBID_MASTER_CRVIRT_A1NOC,
  &a1noc_bus,
  1,
  cfg_master_cr_virt_a1noc_ports,
  cfg_master_cr_virt_a1noc_qos_ports,
  1,
  0,
  0,
  0,
  0,
  1,
};
static uint32_t cfg_master_crypto_c0_ports[1] =
{
  0,
};
static icb_master_def_type cfg_master_crypto_c0 =
{
  ICBID_MASTER_CRYPTO_CORE0,
  &cr_virt_bus,
  1,
  cfg_master_crypto_c0_ports,
  0,
  1,
  0,
  0,
  0,
  0,
  1,
};
static uint32_t cfg_master_lpass_ahb_ports[1] =
{
  1,
};
static uint32_t cfg_master_lpass_ahb_qos_ports[1] =
{
  0,
};
static icb_noc_master_defaults_type cfg_master_lpass_ahb_defaults =
{
  NOC_QOS_MODE_FIXED,
  {
    1,
    1,
  },
};
static icb_master_def_type cfg_master_lpass_ahb =
{
  ICBID_MASTER_LPASS_AHB,
  &snoc_bus,
  1,
  cfg_master_lpass_ahb_ports,
  cfg_master_lpass_ahb_qos_ports,
  0,
  0,
  &cfg_master_lpass_ahb_defaults,
  0,
  0,
  1,
};
static uint32_t cfg_master_lpass_proc_ports[1] =
{
  11,
};
static uint32_t cfg_master_lpass_proc_qos_ports[1] =
{
  5,
};
static icb_noc_master_defaults_type cfg_master_lpass_proc_defaults =
{
  NOC_QOS_MODE_BYPASS,
};
static icb_master_def_type cfg_master_lpass_proc =
{
  ICBID_MASTER_LPASS_PROC,
  &snoc_bus,
  1,
  cfg_master_lpass_proc_ports,
  cfg_master_lpass_proc_qos_ports,
  0,
  0,
  &cfg_master_lpass_proc_defaults,
  0,
  1,
  1,
};
static uint32_t cfg_master_pnoc_a1noc_ports[1] =
{
  2,
};
static uint32_t cfg_master_pnoc_a1noc_qos_ports[1] =
{
  1,
};
static icb_master_def_type cfg_master_pnoc_a1noc =
{
  ICBID_MASTER_PNOC_A1NOC,
  &a1noc_bus,
  1,
  cfg_master_pnoc_a1noc_ports,
  cfg_master_pnoc_a1noc_qos_ports,
  1,
  0,
  0,
  0,
  0,
  1,
};
static uint32_t cfg_master_sdcc_1_ports[1] =
{
  1,
};
static icb_master_def_type cfg_master_sdcc_1 =
{
  ICBID_MASTER_SDCC_1,
  &pnoc_bus,
  1,
  cfg_master_sdcc_1_ports,
  0,
  1,
  0,
  0,
  0,
  0,
  1,
};
static uint32_t cfg_master_sdcc_2_ports[1] =
{
  2,
};
static icb_master_def_type cfg_master_sdcc_2 =
{
  ICBID_MASTER_SDCC_2,
  &pnoc_bus,
  1,
  cfg_master_sdcc_2_ports,
  0,
  1,
  0,
  0,
  0,
  0,
  1,
};
static uint32_t cfg_master_sdcc_4_ports[1] =
{
  3,
};
static icb_master_def_type cfg_master_sdcc_4 =
{
  ICBID_MASTER_SDCC_4,
  &pnoc_bus,
  1,
  cfg_master_sdcc_4_ports,
  0,
  1,
  0,
  0,
  0,
  0,
  1,
};
static uint32_t cfg_master_snoc_bimc_ports[1] =
{
  3,
};
static uint32_t cfg_master_snoc_bimc_qos_ports[1] =
{
  3,
};
static icb_master_def_type cfg_master_snoc_bimc =
{
  ICBID_MASTER_SNOC_BIMC,
  &bimc_bus,
  1,
  cfg_master_snoc_bimc_ports,
  cfg_master_snoc_bimc_qos_ports,
  1,
  10000,
  0,
  0,
  0,
  2,
};
static uint32_t cfg_master_snoc_cnoc_ports[1] =
{
  0,
};
static icb_master_def_type cfg_master_snoc_cnoc =
{
  ICBID_MASTER_SNOC_CNOC,
  &cnoc_bus,
  1,
  cfg_master_snoc_cnoc_ports,
  0,
  1,
  0,
  0,
  0,
  0,
  1,
};
static uint32_t cfg_master_snoc_pnoc_ports[1] =
{
  0,
};
static icb_master_def_type cfg_master_snoc_pnoc =
{
  ICBID_MASTER_SNOC_PNOC,
  &pnoc_bus,
  1,
  cfg_master_snoc_pnoc_ports,
  0,
  1,
  0,
  0,
  0,
  0,
  1,
};
static uint32_t cfg_master_tsif_ports[1] =
{
  8,
};
static icb_master_def_type cfg_master_tsif =
{
  ICBID_MASTER_TSIF,
  &pnoc_bus,
  1,
  cfg_master_tsif_ports,
  0,
  1,
  0,
  0,
  0,
  0,
  1,
};
static uint32_t cfg_master_usb_hs_ports[1] =
{
  4,
};
static icb_master_def_type cfg_master_usb_hs =
{
  ICBID_MASTER_USB_HS,
  &pnoc_bus,
  1,
  cfg_master_usb_hs_ports,
  0,
  1,
  0,
  0,
  0,
  0,
  1,
};
const uint32 u32cfg_bus_list_size = 6;
icb_bus_def_type *cfg_bus_list[6] =
{
  &a1noc_bus,
  &bimc_bus,
  &cnoc_bus,
  &cr_virt_bus,
  &pnoc_bus,
  &snoc_bus,
};
const uint32 u32cfg_slave_list_size = 25;
icb_slave_def_type *cfg_slave_list[25] =
{
  &cfg_slave_a1noc_snoc,
  &cfg_slave_blsp_1,
  &cfg_slave_blsp_2,
  &cfg_slave_clk_ctl,
  &cfg_slave_cr_virt_a1noc,
  &cfg_slave_ebi,
  &cfg_slave_hmss_l3,
  &cfg_slave_imem,
  &cfg_slave_message_ram,
  &cfg_slave_pdm,
  &cfg_slave_pimem,
  &cfg_slave_pmic_arb,
  &cfg_slave_pnoc_a1noc,
  &cfg_slave_qdss_stm,
  &cfg_slave_rpm,
  &cfg_slave_sdcc_1,
  &cfg_slave_sdcc_2,
  &cfg_slave_sdcc_4,
  &cfg_slave_snoc_bimc,
  &cfg_slave_snoc_cnoc,
  &cfg_slave_snoc_pnoc,
  &cfg_slave_tcsr,
  &cfg_slave_tlmm,
  &cfg_slave_tsif,
  &cfg_slave_usb_hs,
};
const uint32 u32cfg_master_list_size = 16;
icb_master_def_type *cfg_master_list[16] =
{
  &cfg_master_a1noc_snoc,
  &cfg_master_blsp_1,
  &cfg_master_blsp_2,
  &cfg_master_cr_virt_a1noc,
  &cfg_master_crypto_c0,
  &cfg_master_lpass_ahb,
  &cfg_master_lpass_proc,
  &cfg_master_pnoc_a1noc,
  &cfg_master_sdcc_1,
  &cfg_master_sdcc_2,
  &cfg_master_sdcc_4,
  &cfg_master_snoc_bimc,
  &cfg_master_snoc_cnoc,
  &cfg_master_snoc_pnoc,
  &cfg_master_tsif,
  &cfg_master_usb_hs,
};
const uint32 u32ul_cfg_clk_node_list_size = 5;
const char *ul_cfg_clock_node_list[] =
{
  "/clk/agr1",
  "/clk/bimc",
  "/clk/cnoc",
  "/clk/pnoc",
  "/clk/snoc",
};
HAL_tcsr_InfoType *cfg_tcsr_info = 0;
static ul_clock_type master_cr_virt_a1noc_clks[1] =
{
  {
    "/clk/agr1",
    0,
    0,
  },
};
static ul_master_data_type master_cr_virt_a1noc =
{
  "/icb/master/cr_virt_a1noc",
  ICBID_MASTER_CRVIRT_A1NOC,
  8,
  1,
  master_cr_virt_a1noc_clks,
  {
    0
  },
  {
    0,
    0,
    0,
    &master_cr_virt_a1noc,
    0,
  },
  &cfg_master_cr_virt_a1noc,
  {
    0,
    0,
    0,
  },
  {
    0,
    0,
    0,
    &master_cr_virt_a1noc,
    0,
  },
};
static ul_clock_type master_pnoc_a1noc_clks[1] =
{
  {
    "/clk/agr1",
    0,
    0,
  },
};
static ul_master_data_type master_pnoc_a1noc =
{
  "/icb/master/pnoc_a1noc",
  ICBID_MASTER_PNOC_A1NOC,
  8,
  1,
  master_pnoc_a1noc_clks,
  {
    0
  },
  {
    0,
    0,
    0,
    &master_pnoc_a1noc,
    0,
  },
  &cfg_master_pnoc_a1noc,
  {
    0,
    0,
    0,
  },
  {
    0,
    0,
    0,
    &master_pnoc_a1noc,
    0,
  },
};
static ul_clock_type slave_a1noc_snoc_slv_clks[1] =
{
  {
    "/clk/agr1",
    0,
    0,
  },
};
static ul_slave_data_type slave_a1noc_snoc =
{
  "/icb/slave/a1noc_snoc",
  ICBID_SLAVE_A1NOC_SNOC,
  16,
  {
    0,
    0,
    0,
  },
  1,
  slave_a1noc_snoc_slv_clks,
  0,
  0,
  {
    0,
    0,
    0
  },
  {
    0,
    0,
    0,
    &slave_a1noc_snoc,
    0,
  },
  &cfg_slave_a1noc_snoc
};
static ul_clock_type master_snoc_bimc_clks[1] =
{
  {
    "/clk/bimc",
    0,
    0,
  },
};
static ul_master_data_type master_snoc_bimc =
{
  "/icb/master/snoc_bimc",
  ICBID_MASTER_SNOC_BIMC,
  8,
  1,
  master_snoc_bimc_clks,
  {
    0
  },
  {
    0,
    0,
    0,
    &master_snoc_bimc,
    0,
  },
  &cfg_master_snoc_bimc,
  {
    0,
    0,
    0,
  },
  {
    0,
    0,
    0,
    &master_snoc_bimc,
    0,
  },
};
static ul_clock_type slave_ebi_slv_clks[1] =
{
  {
    "/clk/bimc",
    0,
    0,
  },
};
static ul_slave_data_type slave_ebi =
{
  "/icb/slave/ebi",
  ICBID_SLAVE_EBI1,
  8,
  {
    0,
    0,
    0,
  },
  1,
  slave_ebi_slv_clks,
  0,
  0,
  {
    0,
    0,
    0
  },
  {
    0,
    0,
    0,
    &slave_ebi,
    0,
  },
  &cfg_slave_ebi
};
static ul_clock_type slave_hmss_l3_slv_clks[1] =
{
  {
    "/clk/bimc",
    0,
    0,
  },
};
static ul_slave_data_type slave_hmss_l3 =
{
  "/icb/slave/hmss_l3",
  ICBID_SLAVE_HMSS_L3,
  8,
  {
    0,
    0,
    0,
  },
  1,
  slave_hmss_l3_slv_clks,
  0,
  0,
  {
    0,
    0,
    0
  },
  {
    0,
    0,
    0,
    &slave_hmss_l3,
    0,
  },
  &cfg_slave_hmss_l3
};
static ul_clock_type master_snoc_cnoc_clks[1] =
{
  {
    "/clk/cnoc",
    0,
    0,
  },
};
static ul_master_data_type master_snoc_cnoc =
{
  "/icb/master/snoc_cnoc",
  ICBID_MASTER_SNOC_CNOC,
  8,
  1,
  master_snoc_cnoc_clks,
  {
    0
  },
  {
    0,
    0,
    0,
    &master_snoc_cnoc,
    0,
  },
  &cfg_master_snoc_cnoc,
  {
    0,
    0,
    0,
  },
  {
    0,
    0,
    0,
    &master_snoc_cnoc,
    0,
  },
};
static ul_clock_type slave_clk_ctl_slv_clks[1] =
{
  {
    "/clk/cnoc",
    0,
    0,
  },
};
static ul_slave_data_type slave_clk_ctl =
{
  "/icb/slave/clk_ctl",
  ICBID_SLAVE_CLK_CTL,
  4,
  {
    0,
    0,
    0,
  },
  1,
  slave_clk_ctl_slv_clks,
  0,
  0,
  {
    0,
    0,
    0
  },
  {
    0,
    0,
    0,
    &slave_clk_ctl,
    0,
  },
  &cfg_slave_clk_ctl
};
static ul_clock_type slave_tcsr_slv_clks[1] =
{
  {
    "/clk/cnoc",
    0,
    0,
  },
};
static ul_slave_data_type slave_tcsr =
{
  "/icb/slave/tcsr",
  ICBID_SLAVE_TCSR,
  4,
  {
    0,
    0,
    0,
  },
  1,
  slave_tcsr_slv_clks,
  0,
  0,
  {
    0,
    0,
    0
  },
  {
    0,
    0,
    0,
    &slave_tcsr,
    0,
  },
  &cfg_slave_tcsr
};
static ul_clock_type slave_tlmm_slv_clks[1] =
{
  {
    "/clk/cnoc",
    0,
    0,
  },
};
static ul_slave_data_type slave_tlmm =
{
  "/icb/slave/tlmm",
  ICBID_SLAVE_TLMM,
  4,
  {
    0,
    0,
    0,
  },
  1,
  slave_tlmm_slv_clks,
  0,
  0,
  {
    0,
    0,
    0
  },
  {
    0,
    0,
    0,
    &slave_tlmm,
    0,
  },
  &cfg_slave_tlmm
};
static ul_clock_type slave_message_ram_slv_clks[1] =
{
  {
    "/clk/cnoc",
    0,
    0,
  },
};
static ul_slave_data_type slave_message_ram =
{
  "/icb/slave/message_ram",
  ICBID_SLAVE_MESSAGE_RAM,
  4,
  {
    0,
    0,
    0,
  },
  1,
  slave_message_ram_slv_clks,
  0,
  0,
  {
    0,
    0,
    0
  },
  {
    0,
    0,
    0,
    &slave_message_ram,
    0,
  },
  &cfg_slave_message_ram
};
static ul_clock_type slave_pmic_arb_slv_clks[1] =
{
  {
    "/clk/cnoc",
    0,
    0,
  },
};
static ul_slave_data_type slave_pmic_arb =
{
  "/icb/slave/pmic_arb",
  ICBID_SLAVE_PMIC_ARB,
  4,
  {
    0,
    0,
    0,
  },
  1,
  slave_pmic_arb_slv_clks,
  0,
  0,
  {
    0,
    0,
    0
  },
  {
    0,
    0,
    0,
    &slave_pmic_arb,
    0,
  },
  &cfg_slave_pmic_arb
};
static ul_clock_type slave_rpm_slv_clks[1] =
{
  {
    "/clk/cnoc",
    0,
    0,
  },
};
static ul_slave_data_type slave_rpm =
{
  "/icb/slave/rpm",
  ICBID_SLAVE_RPM,
  4,
  {
    0,
    0,
    0,
  },
  1,
  slave_rpm_slv_clks,
  0,
  0,
  {
    0,
    0,
    0
  },
  {
    0,
    0,
    0,
    &slave_rpm,
    0,
  },
  &cfg_slave_rpm
};
static ul_master_data_type master_crypto_c0 =
{
  "/icb/master/crypto_c0",
  ICBID_MASTER_CRYPTO_CORE0,
  650 | 0x80000000UL,
  0,
  0,
  {
    0
  },
  {
    0,
    0,
    0,
    &master_crypto_c0,
    0,
  },
  &cfg_master_crypto_c0,
  {
    0,
    0,
    0,
  },
  {
    0,
    0,
    0,
    &master_crypto_c0,
    0,
  },
};
static ul_slave_data_type slave_cr_virt_a1noc =
{
  "/icb/slave/cr_virt_a1noc",
  ICBID_SLAVE_CRVIRT_A1NOC,
  8,
  {
    0,
    0,
    0,
  },
  0,
  0,
  0,
  0,
  {
    0,
    0,
    0
  },
  {
    0,
    0,
    0,
    &slave_cr_virt_a1noc,
    0,
  },
  &cfg_slave_cr_virt_a1noc
};
static ul_clock_type master_snoc_pnoc_clks[1] =
{
  {
    "/clk/pnoc",
    0,
    0,
  },
};
static ul_master_data_type master_snoc_pnoc =
{
  "/icb/master/snoc_pnoc",
  ICBID_MASTER_SNOC_PNOC,
  8,
  1,
  master_snoc_pnoc_clks,
  {
    0
  },
  {
    0,
    0,
    0,
    &master_snoc_pnoc,
    0,
  },
  &cfg_master_snoc_pnoc,
  {
    0,
    0,
    0,
  },
  {
    0,
    0,
    0,
    &master_snoc_pnoc,
    0,
  },
};
static ul_clock_type master_sdcc_1_clks[1] =
{
  {
    "/clk/pnoc",
    0,
    0,
  },
};
static ul_master_data_type master_sdcc_1 =
{
  "/icb/master/sdcc_1",
  ICBID_MASTER_SDCC_1,
  8,
  1,
  master_sdcc_1_clks,
  {
    0
  },
  {
    0,
    0,
    0,
    &master_sdcc_1,
    0,
  },
  &cfg_master_sdcc_1,
  {
    0,
    0,
    0,
  },
  {
    0,
    0,
    0,
    &master_sdcc_1,
    0,
  },
};
static ul_clock_type master_sdcc_2_clks[1] =
{
  {
    "/clk/pnoc",
    0,
    0,
  },
};
static ul_master_data_type master_sdcc_2 =
{
  "/icb/master/sdcc_2",
  ICBID_MASTER_SDCC_2,
  8,
  1,
  master_sdcc_2_clks,
  {
    0
  },
  {
    0,
    0,
    0,
    &master_sdcc_2,
    0,
  },
  &cfg_master_sdcc_2,
  {
    0,
    0,
    0,
  },
  {
    0,
    0,
    0,
    &master_sdcc_2,
    0,
  },
};
static ul_clock_type master_sdcc_4_clks[1] =
{
  {
    "/clk/pnoc",
    0,
    0,
  },
};
static ul_master_data_type master_sdcc_4 =
{
  "/icb/master/sdcc_4",
  ICBID_MASTER_SDCC_4,
  8,
  1,
  master_sdcc_4_clks,
  {
    0
  },
  {
    0,
    0,
    0,
    &master_sdcc_4,
    0,
  },
  &cfg_master_sdcc_4,
  {
    0,
    0,
    0,
  },
  {
    0,
    0,
    0,
    &master_sdcc_4,
    0,
  },
};
static ul_clock_type master_usb_hs_clks[1] =
{
  {
    "/clk/pnoc",
    0,
    0,
  },
};
static ul_master_data_type master_usb_hs =
{
  "/icb/master/usb_hs",
  ICBID_MASTER_USB_HS,
  8,
  1,
  master_usb_hs_clks,
  {
    0
  },
  {
    0,
    0,
    0,
    &master_usb_hs,
    0,
  },
  &cfg_master_usb_hs,
  {
    0,
    0,
    0,
  },
  {
    0,
    0,
    0,
    &master_usb_hs,
    0,
  },
};
static ul_clock_type master_blsp_1_clks[1] =
{
  {
    "/clk/pnoc",
    0,
    0,
  },
};
static ul_master_data_type master_blsp_1 =
{
  "/icb/master/blsp_1",
  ICBID_MASTER_BLSP_1,
  4,
  1,
  master_blsp_1_clks,
  {
    0
  },
  {
    0,
    0,
    0,
    &master_blsp_1,
    0,
  },
  &cfg_master_blsp_1,
  {
    0,
    0,
    0,
  },
  {
    0,
    0,
    0,
    &master_blsp_1,
    0,
  },
};
static ul_clock_type master_blsp_2_clks[1] =
{
  {
    "/clk/pnoc",
    0,
    0,
  },
};
static ul_master_data_type master_blsp_2 =
{
  "/icb/master/blsp_2",
  ICBID_MASTER_BLSP_2,
  4,
  1,
  master_blsp_2_clks,
  {
    0
  },
  {
    0,
    0,
    0,
    &master_blsp_2,
    0,
  },
  &cfg_master_blsp_2,
  {
    0,
    0,
    0,
  },
  {
    0,
    0,
    0,
    &master_blsp_2,
    0,
  },
};
static ul_clock_type master_tsif_clks[1] =
{
  {
    "/clk/pnoc",
    0,
    0,
  },
};
static ul_master_data_type master_tsif =
{
  "/icb/master/tsif",
  ICBID_MASTER_TSIF,
  4,
  1,
  master_tsif_clks,
  {
    0
  },
  {
    0,
    0,
    0,
    &master_tsif,
    0,
  },
  &cfg_master_tsif,
  {
    0,
    0,
    0,
  },
  {
    0,
    0,
    0,
    &master_tsif,
    0,
  },
};
static ul_clock_type slave_pnoc_a1noc_slv_clks[1] =
{
  {
    "/clk/pnoc",
    0,
    0,
  },
};
static ul_slave_data_type slave_pnoc_a1noc =
{
  "/icb/slave/pnoc_a1noc",
  ICBID_SLAVE_PNOC_A1NOC,
  8,
  {
    0,
    0,
    0,
  },
  1,
  slave_pnoc_a1noc_slv_clks,
  0,
  0,
  {
    0,
    0,
    0
  },
  {
    0,
    0,
    0,
    &slave_pnoc_a1noc,
    0,
  },
  &cfg_slave_pnoc_a1noc
};
static ul_clock_type slave_usb_hs_slv_clks[1] =
{
  {
    "/clk/pnoc",
    0,
    0,
  },
};
static ul_slave_data_type slave_usb_hs =
{
  "/icb/slave/usb_hs",
  ICBID_SLAVE_USB_HS,
  4,
  {
    0,
    0,
    0,
  },
  1,
  slave_usb_hs_slv_clks,
  0,
  0,
  {
    0,
    0,
    0
  },
  {
    0,
    0,
    0,
    &slave_usb_hs,
    0,
  },
  &cfg_slave_usb_hs
};
static ul_clock_type slave_sdcc_2_slv_clks[1] =
{
  {
    "/clk/pnoc",
    0,
    0,
  },
};
static ul_slave_data_type slave_sdcc_2 =
{
  "/icb/slave/sdcc_2",
  ICBID_SLAVE_SDCC_2,
  4,
  {
    0,
    0,
    0,
  },
  1,
  slave_sdcc_2_slv_clks,
  0,
  0,
  {
    0,
    0,
    0
  },
  {
    0,
    0,
    0,
    &slave_sdcc_2,
    0,
  },
  &cfg_slave_sdcc_2
};
static ul_clock_type slave_sdcc_4_slv_clks[1] =
{
  {
    "/clk/pnoc",
    0,
    0,
  },
};
static ul_slave_data_type slave_sdcc_4 =
{
  "/icb/slave/sdcc_4",
  ICBID_SLAVE_SDCC_4,
  4,
  {
    0,
    0,
    0,
  },
  1,
  slave_sdcc_4_slv_clks,
  0,
  0,
  {
    0,
    0,
    0
  },
  {
    0,
    0,
    0,
    &slave_sdcc_4,
    0,
  },
  &cfg_slave_sdcc_4
};
static ul_clock_type slave_tsif_slv_clks[1] =
{
  {
    "/clk/pnoc",
    0,
    0,
  },
};
static ul_slave_data_type slave_tsif =
{
  "/icb/slave/tsif",
  ICBID_SLAVE_TSIF,
  4,
  {
    0,
    0,
    0,
  },
  1,
  slave_tsif_slv_clks,
  0,
  0,
  {
    0,
    0,
    0
  },
  {
    0,
    0,
    0,
    &slave_tsif,
    0,
  },
  &cfg_slave_tsif
};
static ul_clock_type slave_blsp_2_slv_clks[1] =
{
  {
    "/clk/pnoc",
    0,
    0,
  },
};
static ul_slave_data_type slave_blsp_2 =
{
  "/icb/slave/blsp_2",
  ICBID_SLAVE_BLSP_2,
  4,
  {
    0,
    0,
    0,
  },
  1,
  slave_blsp_2_slv_clks,
  0,
  0,
  {
    0,
    0,
    0
  },
  {
    0,
    0,
    0,
    &slave_blsp_2,
    0,
  },
  &cfg_slave_blsp_2
};
static ul_clock_type slave_sdcc_1_slv_clks[1] =
{
  {
    "/clk/pnoc",
    0,
    0,
  },
};
static ul_slave_data_type slave_sdcc_1 =
{
  "/icb/slave/sdcc_1",
  ICBID_SLAVE_SDCC_1,
  4,
  {
    0,
    0,
    0,
  },
  1,
  slave_sdcc_1_slv_clks,
  0,
  0,
  {
    0,
    0,
    0
  },
  {
    0,
    0,
    0,
    &slave_sdcc_1,
    0,
  },
  &cfg_slave_sdcc_1
};
static ul_clock_type slave_blsp_1_slv_clks[1] =
{
  {
    "/clk/pnoc",
    0,
    0,
  },
};
static ul_slave_data_type slave_blsp_1 =
{
  "/icb/slave/blsp_1",
  ICBID_SLAVE_BLSP_1,
  4,
  {
    0,
    0,
    0,
  },
  1,
  slave_blsp_1_slv_clks,
  0,
  0,
  {
    0,
    0,
    0
  },
  {
    0,
    0,
    0,
    &slave_blsp_1,
    0,
  },
  &cfg_slave_blsp_1
};
static ul_clock_type slave_pdm_slv_clks[1] =
{
  {
    "/clk/pnoc",
    0,
    0,
  },
};
static ul_slave_data_type slave_pdm =
{
  "/icb/slave/pdm",
  ICBID_SLAVE_PDM,
  4,
  {
    0,
    0,
    0,
  },
  1,
  slave_pdm_slv_clks,
  0,
  0,
  {
    0,
    0,
    0
  },
  {
    0,
    0,
    0,
    &slave_pdm,
    0,
  },
  &cfg_slave_pdm
};
static ul_clock_type master_lpass_ahb_clks[1] =
{
  {
    "/clk/snoc",
    0,
    0,
  },
};
static ul_master_data_type master_lpass_ahb =
{
  "/icb/master/lpass_ahb",
  ICBID_MASTER_LPASS_AHB,
  16,
  1,
  master_lpass_ahb_clks,
  {
    0
  },
  {
    0,
    0,
    0,
    &master_lpass_ahb,
    0,
  },
  &cfg_master_lpass_ahb,
  {
    0,
    0,
    0,
  },
  {
    0,
    0,
    0,
    &master_lpass_ahb,
    0,
  },
};
static ul_clock_type master_a1noc_snoc_clks[1] =
{
  {
    "/clk/snoc",
    0,
    0,
  },
};
static ul_master_data_type master_a1noc_snoc =
{
  "/icb/master/a1noc_snoc",
  ICBID_MASTER_A1NOC_SNOC,
  16,
  1,
  master_a1noc_snoc_clks,
  {
    0
  },
  {
    0,
    0,
    0,
    &master_a1noc_snoc,
    0,
  },
  &cfg_master_a1noc_snoc,
  {
    0,
    0,
    0,
  },
  {
    0,
    0,
    0,
    &master_a1noc_snoc,
    0,
  },
};
static ul_clock_type master_lpass_proc_clks[1] =
{
  {
    "/clk/snoc",
    0,
    0,
  },
};
static ul_master_data_type master_lpass_proc =
{
  "/icb/master/lpass_proc",
  ICBID_MASTER_LPASS_PROC,
  32,
  1,
  master_lpass_proc_clks,
  {
    0
  },
  {
    0,
    0,
    0,
    &master_lpass_proc,
    0,
  },
  &cfg_master_lpass_proc,
  {
    0,
    0,
    0,
  },
  {
    0,
    0,
    0,
    &master_lpass_proc,
    0,
  },
};
static ul_clock_type slave_snoc_bimc_slv_clks[1] =
{
  {
    "/clk/snoc",
    0,
    0,
  },
};
static ul_slave_data_type slave_snoc_bimc =
{
  "/icb/slave/snoc_bimc",
  ICBID_SLAVE_SNOC_BIMC,
  32,
  {
    0,
    0,
    0,
  },
  1,
  slave_snoc_bimc_slv_clks,
  0,
  0,
  {
    0,
    0,
    0
  },
  {
    0,
    0,
    0,
    &slave_snoc_bimc,
    0,
  },
  &cfg_slave_snoc_bimc
};
static ul_clock_type slave_snoc_cnoc_slv_clks[1] =
{
  {
    "/clk/snoc",
    0,
    0,
  },
};
static ul_slave_data_type slave_snoc_cnoc =
{
  "/icb/slave/snoc_cnoc",
  ICBID_SLAVE_SNOC_CNOC,
  16,
  {
    0,
    0,
    0,
  },
  1,
  slave_snoc_cnoc_slv_clks,
  0,
  0,
  {
    0,
    0,
    0
  },
  {
    0,
    0,
    0,
    &slave_snoc_cnoc,
    0,
  },
  &cfg_slave_snoc_cnoc
};
static ul_clock_type slave_imem_slv_clks[1] =
{
  {
    "/clk/snoc",
    0,
    0,
  },
};
static ul_slave_data_type slave_imem =
{
  "/icb/slave/imem",
  ICBID_SLAVE_IMEM,
  16,
  {
    0,
    0,
    0,
  },
  1,
  slave_imem_slv_clks,
  0,
  0,
  {
    0,
    0,
    0
  },
  {
    0,
    0,
    0,
    &slave_imem,
    0,
  },
  &cfg_slave_imem
};
static ul_clock_type slave_pimem_slv_clks[1] =
{
  {
    "/clk/snoc",
    0,
    0,
  },
};
static ul_slave_data_type slave_pimem =
{
  "/icb/slave/pimem",
  ICBID_SLAVE_PIMEM,
  16,
  {
    0,
    0,
    0,
  },
  1,
  slave_pimem_slv_clks,
  0,
  0,
  {
    0,
    0,
    0
  },
  {
    0,
    0,
    0,
    &slave_pimem,
    0,
  },
  &cfg_slave_pimem
};
static ul_clock_type slave_snoc_pnoc_slv_clks[1] =
{
  {
    "/clk/snoc",
    0,
    0,
  },
};
static ul_slave_data_type slave_snoc_pnoc =
{
  "/icb/slave/snoc_pnoc",
  ICBID_SLAVE_SNOC_PNOC,
  16,
  {
    0,
    0,
    0,
  },
  1,
  slave_snoc_pnoc_slv_clks,
  0,
  0,
  {
    0,
    0,
    0
  },
  {
    0,
    0,
    0,
    &slave_snoc_pnoc,
    0,
  },
  &cfg_slave_snoc_pnoc
};
static ul_clock_type slave_qdss_stm_slv_clks[1] =
{
  {
    "/clk/snoc",
    0,
    0,
  },
};
static ul_slave_data_type slave_qdss_stm =
{
  "/icb/slave/qdss_stm",
  ICBID_SLAVE_QDSS_STM,
  16,
  {
    0,
    0,
    0,
  },
  1,
  slave_qdss_stm_slv_clks,
  0,
  0,
  {
    0,
    0,
    0
  },
  {
    0,
    0,
    0,
    &slave_qdss_stm,
    0,
  },
  &cfg_slave_qdss_stm
};
static ul_pair_type route_blsp_1_clk_ctl_list_array[4] =
{
  {
    &master_blsp_1,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_clk_ctl
  },
};
static ul_route_list_type route_blsp_1_clk_ctl_list =
{
  4,
  route_blsp_1_clk_ctl_list_array
};
static ul_pair_type route_blsp_1_ebi_list_array[4] =
{
  {
    &master_blsp_1,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_bimc
  },
  {
    &master_snoc_bimc,
    &slave_ebi
  },
};
static ul_route_list_type route_blsp_1_ebi_list =
{
  4,
  route_blsp_1_ebi_list_array
};
static ul_pair_type route_blsp_1_hmss_l3_list_array[4] =
{
  {
    &master_blsp_1,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_bimc
  },
  {
    &master_snoc_bimc,
    &slave_hmss_l3
  },
};
static ul_route_list_type route_blsp_1_hmss_l3_list =
{
  4,
  route_blsp_1_hmss_l3_list_array
};
static ul_pair_type route_blsp_1_imem_list_array[3] =
{
  {
    &master_blsp_1,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_imem
  },
};
static ul_route_list_type route_blsp_1_imem_list =
{
  3,
  route_blsp_1_imem_list_array
};
static ul_pair_type route_blsp_1_message_ram_list_array[4] =
{
  {
    &master_blsp_1,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_message_ram
  },
};
static ul_route_list_type route_blsp_1_message_ram_list =
{
  4,
  route_blsp_1_message_ram_list_array
};
static ul_pair_type route_blsp_1_pimem_list_array[3] =
{
  {
    &master_blsp_1,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_pimem
  },
};
static ul_route_list_type route_blsp_1_pimem_list =
{
  3,
  route_blsp_1_pimem_list_array
};
static ul_pair_type route_blsp_1_pmic_arb_list_array[4] =
{
  {
    &master_blsp_1,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_pmic_arb
  },
};
static ul_route_list_type route_blsp_1_pmic_arb_list =
{
  4,
  route_blsp_1_pmic_arb_list_array
};
static ul_pair_type route_blsp_1_qdss_stm_list_array[3] =
{
  {
    &master_blsp_1,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_qdss_stm
  },
};
static ul_route_list_type route_blsp_1_qdss_stm_list =
{
  3,
  route_blsp_1_qdss_stm_list_array
};
static ul_pair_type route_blsp_1_rpm_list_array[4] =
{
  {
    &master_blsp_1,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_rpm
  },
};
static ul_route_list_type route_blsp_1_rpm_list =
{
  4,
  route_blsp_1_rpm_list_array
};
static ul_pair_type route_blsp_1_tcsr_list_array[4] =
{
  {
    &master_blsp_1,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_tcsr
  },
};
static ul_route_list_type route_blsp_1_tcsr_list =
{
  4,
  route_blsp_1_tcsr_list_array
};
static ul_pair_type route_blsp_1_tlmm_list_array[4] =
{
  {
    &master_blsp_1,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_tlmm
  },
};
static ul_route_list_type route_blsp_1_tlmm_list =
{
  4,
  route_blsp_1_tlmm_list_array
};
static ul_pair_type route_blsp_2_clk_ctl_list_array[4] =
{
  {
    &master_blsp_2,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_clk_ctl
  },
};
static ul_route_list_type route_blsp_2_clk_ctl_list =
{
  4,
  route_blsp_2_clk_ctl_list_array
};
static ul_pair_type route_blsp_2_ebi_list_array[4] =
{
  {
    &master_blsp_2,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_bimc
  },
  {
    &master_snoc_bimc,
    &slave_ebi
  },
};
static ul_route_list_type route_blsp_2_ebi_list =
{
  4,
  route_blsp_2_ebi_list_array
};
static ul_pair_type route_blsp_2_hmss_l3_list_array[4] =
{
  {
    &master_blsp_2,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_bimc
  },
  {
    &master_snoc_bimc,
    &slave_hmss_l3
  },
};
static ul_route_list_type route_blsp_2_hmss_l3_list =
{
  4,
  route_blsp_2_hmss_l3_list_array
};
static ul_pair_type route_blsp_2_imem_list_array[3] =
{
  {
    &master_blsp_2,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_imem
  },
};
static ul_route_list_type route_blsp_2_imem_list =
{
  3,
  route_blsp_2_imem_list_array
};
static ul_pair_type route_blsp_2_message_ram_list_array[4] =
{
  {
    &master_blsp_2,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_message_ram
  },
};
static ul_route_list_type route_blsp_2_message_ram_list =
{
  4,
  route_blsp_2_message_ram_list_array
};
static ul_pair_type route_blsp_2_pimem_list_array[3] =
{
  {
    &master_blsp_2,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_pimem
  },
};
static ul_route_list_type route_blsp_2_pimem_list =
{
  3,
  route_blsp_2_pimem_list_array
};
static ul_pair_type route_blsp_2_pmic_arb_list_array[4] =
{
  {
    &master_blsp_2,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_pmic_arb
  },
};
static ul_route_list_type route_blsp_2_pmic_arb_list =
{
  4,
  route_blsp_2_pmic_arb_list_array
};
static ul_pair_type route_blsp_2_qdss_stm_list_array[3] =
{
  {
    &master_blsp_2,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_qdss_stm
  },
};
static ul_route_list_type route_blsp_2_qdss_stm_list =
{
  3,
  route_blsp_2_qdss_stm_list_array
};
static ul_pair_type route_blsp_2_rpm_list_array[4] =
{
  {
    &master_blsp_2,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_rpm
  },
};
static ul_route_list_type route_blsp_2_rpm_list =
{
  4,
  route_blsp_2_rpm_list_array
};
static ul_pair_type route_blsp_2_tcsr_list_array[4] =
{
  {
    &master_blsp_2,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_tcsr
  },
};
static ul_route_list_type route_blsp_2_tcsr_list =
{
  4,
  route_blsp_2_tcsr_list_array
};
static ul_pair_type route_blsp_2_tlmm_list_array[4] =
{
  {
    &master_blsp_2,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_tlmm
  },
};
static ul_route_list_type route_blsp_2_tlmm_list =
{
  4,
  route_blsp_2_tlmm_list_array
};
static ul_pair_type route_crypto_c0_blsp_1_list_array[4] =
{
  {
    &master_crypto_c0,
    &slave_cr_virt_a1noc
  },
  {
    &master_cr_virt_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_pnoc
  },
  {
    &master_snoc_pnoc,
    &slave_blsp_1
  },
};
static ul_route_list_type route_crypto_c0_blsp_1_list =
{
  4,
  route_crypto_c0_blsp_1_list_array
};
static ul_pair_type route_crypto_c0_blsp_2_list_array[4] =
{
  {
    &master_crypto_c0,
    &slave_cr_virt_a1noc
  },
  {
    &master_cr_virt_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_pnoc
  },
  {
    &master_snoc_pnoc,
    &slave_blsp_2
  },
};
static ul_route_list_type route_crypto_c0_blsp_2_list =
{
  4,
  route_crypto_c0_blsp_2_list_array
};
static ul_pair_type route_crypto_c0_clk_ctl_list_array[4] =
{
  {
    &master_crypto_c0,
    &slave_cr_virt_a1noc
  },
  {
    &master_cr_virt_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_clk_ctl
  },
};
static ul_route_list_type route_crypto_c0_clk_ctl_list =
{
  4,
  route_crypto_c0_clk_ctl_list_array
};
static ul_pair_type route_crypto_c0_ebi_list_array[4] =
{
  {
    &master_crypto_c0,
    &slave_cr_virt_a1noc
  },
  {
    &master_cr_virt_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_bimc
  },
  {
    &master_snoc_bimc,
    &slave_ebi
  },
};
static ul_route_list_type route_crypto_c0_ebi_list =
{
  4,
  route_crypto_c0_ebi_list_array
};
static ul_pair_type route_crypto_c0_hmss_l3_list_array[4] =
{
  {
    &master_crypto_c0,
    &slave_cr_virt_a1noc
  },
  {
    &master_cr_virt_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_bimc
  },
  {
    &master_snoc_bimc,
    &slave_hmss_l3
  },
};
static ul_route_list_type route_crypto_c0_hmss_l3_list =
{
  4,
  route_crypto_c0_hmss_l3_list_array
};
static ul_pair_type route_crypto_c0_imem_list_array[3] =
{
  {
    &master_crypto_c0,
    &slave_cr_virt_a1noc
  },
  {
    &master_cr_virt_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_imem
  },
};
static ul_route_list_type route_crypto_c0_imem_list =
{
  3,
  route_crypto_c0_imem_list_array
};
static ul_pair_type route_crypto_c0_message_ram_list_array[4] =
{
  {
    &master_crypto_c0,
    &slave_cr_virt_a1noc
  },
  {
    &master_cr_virt_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_message_ram
  },
};
static ul_route_list_type route_crypto_c0_message_ram_list =
{
  4,
  route_crypto_c0_message_ram_list_array
};
static ul_pair_type route_crypto_c0_pdm_list_array[4] =
{
  {
    &master_crypto_c0,
    &slave_cr_virt_a1noc
  },
  {
    &master_cr_virt_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_pnoc
  },
  {
    &master_snoc_pnoc,
    &slave_pdm
  },
};
static ul_route_list_type route_crypto_c0_pdm_list =
{
  4,
  route_crypto_c0_pdm_list_array
};
static ul_pair_type route_crypto_c0_pimem_list_array[3] =
{
  {
    &master_crypto_c0,
    &slave_cr_virt_a1noc
  },
  {
    &master_cr_virt_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_pimem
  },
};
static ul_route_list_type route_crypto_c0_pimem_list =
{
  3,
  route_crypto_c0_pimem_list_array
};
static ul_pair_type route_crypto_c0_pmic_arb_list_array[4] =
{
  {
    &master_crypto_c0,
    &slave_cr_virt_a1noc
  },
  {
    &master_cr_virt_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_pmic_arb
  },
};
static ul_route_list_type route_crypto_c0_pmic_arb_list =
{
  4,
  route_crypto_c0_pmic_arb_list_array
};
static ul_pair_type route_crypto_c0_qdss_stm_list_array[3] =
{
  {
    &master_crypto_c0,
    &slave_cr_virt_a1noc
  },
  {
    &master_cr_virt_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_qdss_stm
  },
};
static ul_route_list_type route_crypto_c0_qdss_stm_list =
{
  3,
  route_crypto_c0_qdss_stm_list_array
};
static ul_pair_type route_crypto_c0_rpm_list_array[4] =
{
  {
    &master_crypto_c0,
    &slave_cr_virt_a1noc
  },
  {
    &master_cr_virt_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_rpm
  },
};
static ul_route_list_type route_crypto_c0_rpm_list =
{
  4,
  route_crypto_c0_rpm_list_array
};
static ul_pair_type route_crypto_c0_sdcc_1_list_array[4] =
{
  {
    &master_crypto_c0,
    &slave_cr_virt_a1noc
  },
  {
    &master_cr_virt_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_pnoc
  },
  {
    &master_snoc_pnoc,
    &slave_sdcc_1
  },
};
static ul_route_list_type route_crypto_c0_sdcc_1_list =
{
  4,
  route_crypto_c0_sdcc_1_list_array
};
static ul_pair_type route_crypto_c0_sdcc_2_list_array[4] =
{
  {
    &master_crypto_c0,
    &slave_cr_virt_a1noc
  },
  {
    &master_cr_virt_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_pnoc
  },
  {
    &master_snoc_pnoc,
    &slave_sdcc_2
  },
};
static ul_route_list_type route_crypto_c0_sdcc_2_list =
{
  4,
  route_crypto_c0_sdcc_2_list_array
};
static ul_pair_type route_crypto_c0_sdcc_4_list_array[4] =
{
  {
    &master_crypto_c0,
    &slave_cr_virt_a1noc
  },
  {
    &master_cr_virt_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_pnoc
  },
  {
    &master_snoc_pnoc,
    &slave_sdcc_4
  },
};
static ul_route_list_type route_crypto_c0_sdcc_4_list =
{
  4,
  route_crypto_c0_sdcc_4_list_array
};
static ul_pair_type route_crypto_c0_tcsr_list_array[4] =
{
  {
    &master_crypto_c0,
    &slave_cr_virt_a1noc
  },
  {
    &master_cr_virt_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_tcsr
  },
};
static ul_route_list_type route_crypto_c0_tcsr_list =
{
  4,
  route_crypto_c0_tcsr_list_array
};
static ul_pair_type route_crypto_c0_tlmm_list_array[4] =
{
  {
    &master_crypto_c0,
    &slave_cr_virt_a1noc
  },
  {
    &master_cr_virt_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_tlmm
  },
};
static ul_route_list_type route_crypto_c0_tlmm_list =
{
  4,
  route_crypto_c0_tlmm_list_array
};
static ul_pair_type route_crypto_c0_tsif_list_array[4] =
{
  {
    &master_crypto_c0,
    &slave_cr_virt_a1noc
  },
  {
    &master_cr_virt_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_pnoc
  },
  {
    &master_snoc_pnoc,
    &slave_tsif
  },
};
static ul_route_list_type route_crypto_c0_tsif_list =
{
  4,
  route_crypto_c0_tsif_list_array
};
static ul_pair_type route_crypto_c0_usb_hs_list_array[4] =
{
  {
    &master_crypto_c0,
    &slave_cr_virt_a1noc
  },
  {
    &master_cr_virt_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_pnoc
  },
  {
    &master_snoc_pnoc,
    &slave_usb_hs
  },
};
static ul_route_list_type route_crypto_c0_usb_hs_list =
{
  4,
  route_crypto_c0_usb_hs_list_array
};
static ul_pair_type route_lpass_ahb_blsp_1_list_array[2] =
{
  {
    &master_lpass_ahb,
    &slave_snoc_pnoc
  },
  {
    &master_snoc_pnoc,
    &slave_blsp_1
  },
};
static ul_route_list_type route_lpass_ahb_blsp_1_list =
{
  2,
  route_lpass_ahb_blsp_1_list_array
};
static ul_pair_type route_lpass_ahb_blsp_2_list_array[2] =
{
  {
    &master_lpass_ahb,
    &slave_snoc_pnoc
  },
  {
    &master_snoc_pnoc,
    &slave_blsp_2
  },
};
static ul_route_list_type route_lpass_ahb_blsp_2_list =
{
  2,
  route_lpass_ahb_blsp_2_list_array
};
static ul_pair_type route_lpass_ahb_ebi_list_array[2] =
{
  {
    &master_lpass_ahb,
    &slave_snoc_bimc
  },
  {
    &master_snoc_bimc,
    &slave_ebi
  },
};
static ul_route_list_type route_lpass_ahb_ebi_list =
{
  2,
  route_lpass_ahb_ebi_list_array
};
static ul_pair_type route_lpass_ahb_hmss_l3_list_array[2] =
{
  {
    &master_lpass_ahb,
    &slave_snoc_bimc
  },
  {
    &master_snoc_bimc,
    &slave_hmss_l3
  },
};
static ul_route_list_type route_lpass_ahb_hmss_l3_list =
{
  2,
  route_lpass_ahb_hmss_l3_list_array
};
static ul_pair_type route_lpass_ahb_imem_list_array[1] =
{
  {
    &master_lpass_ahb,
    &slave_imem
  },
};
static ul_route_list_type route_lpass_ahb_imem_list =
{
  1,
  route_lpass_ahb_imem_list_array
};
static ul_pair_type route_lpass_ahb_pdm_list_array[2] =
{
  {
    &master_lpass_ahb,
    &slave_snoc_pnoc
  },
  {
    &master_snoc_pnoc,
    &slave_pdm
  },
};
static ul_route_list_type route_lpass_ahb_pdm_list =
{
  2,
  route_lpass_ahb_pdm_list_array
};
static ul_pair_type route_lpass_ahb_pimem_list_array[1] =
{
  {
    &master_lpass_ahb,
    &slave_pimem
  },
};
static ul_route_list_type route_lpass_ahb_pimem_list =
{
  1,
  route_lpass_ahb_pimem_list_array
};
static ul_pair_type route_lpass_ahb_sdcc_1_list_array[2] =
{
  {
    &master_lpass_ahb,
    &slave_snoc_pnoc
  },
  {
    &master_snoc_pnoc,
    &slave_sdcc_1
  },
};
static ul_route_list_type route_lpass_ahb_sdcc_1_list =
{
  2,
  route_lpass_ahb_sdcc_1_list_array
};
static ul_pair_type route_lpass_ahb_sdcc_2_list_array[2] =
{
  {
    &master_lpass_ahb,
    &slave_snoc_pnoc
  },
  {
    &master_snoc_pnoc,
    &slave_sdcc_2
  },
};
static ul_route_list_type route_lpass_ahb_sdcc_2_list =
{
  2,
  route_lpass_ahb_sdcc_2_list_array
};
static ul_pair_type route_lpass_ahb_sdcc_4_list_array[2] =
{
  {
    &master_lpass_ahb,
    &slave_snoc_pnoc
  },
  {
    &master_snoc_pnoc,
    &slave_sdcc_4
  },
};
static ul_route_list_type route_lpass_ahb_sdcc_4_list =
{
  2,
  route_lpass_ahb_sdcc_4_list_array
};
static ul_pair_type route_lpass_ahb_tsif_list_array[2] =
{
  {
    &master_lpass_ahb,
    &slave_snoc_pnoc
  },
  {
    &master_snoc_pnoc,
    &slave_tsif
  },
};
static ul_route_list_type route_lpass_ahb_tsif_list =
{
  2,
  route_lpass_ahb_tsif_list_array
};
static ul_pair_type route_lpass_ahb_usb_hs_list_array[2] =
{
  {
    &master_lpass_ahb,
    &slave_snoc_pnoc
  },
  {
    &master_snoc_pnoc,
    &slave_usb_hs
  },
};
static ul_route_list_type route_lpass_ahb_usb_hs_list =
{
  2,
  route_lpass_ahb_usb_hs_list_array
};
static ul_pair_type route_lpass_proc_blsp_1_list_array[2] =
{
  {
    &master_lpass_proc,
    &slave_snoc_pnoc
  },
  {
    &master_snoc_pnoc,
    &slave_blsp_1
  },
};
static ul_route_list_type route_lpass_proc_blsp_1_list =
{
  2,
  route_lpass_proc_blsp_1_list_array
};
static ul_pair_type route_lpass_proc_blsp_2_list_array[2] =
{
  {
    &master_lpass_proc,
    &slave_snoc_pnoc
  },
  {
    &master_snoc_pnoc,
    &slave_blsp_2
  },
};
static ul_route_list_type route_lpass_proc_blsp_2_list =
{
  2,
  route_lpass_proc_blsp_2_list_array
};
static ul_pair_type route_lpass_proc_clk_ctl_list_array[2] =
{
  {
    &master_lpass_proc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_clk_ctl
  },
};
static ul_route_list_type route_lpass_proc_clk_ctl_list =
{
  2,
  route_lpass_proc_clk_ctl_list_array
};
static ul_pair_type route_lpass_proc_ebi_list_array[2] =
{
  {
    &master_lpass_proc,
    &slave_snoc_bimc
  },
  {
    &master_snoc_bimc,
    &slave_ebi
  },
};
static ul_route_list_type route_lpass_proc_ebi_list =
{
  2,
  route_lpass_proc_ebi_list_array
};
static ul_pair_type route_lpass_proc_hmss_l3_list_array[2] =
{
  {
    &master_lpass_proc,
    &slave_snoc_bimc
  },
  {
    &master_snoc_bimc,
    &slave_hmss_l3
  },
};
static ul_route_list_type route_lpass_proc_hmss_l3_list =
{
  2,
  route_lpass_proc_hmss_l3_list_array
};
static ul_pair_type route_lpass_proc_imem_list_array[1] =
{
  {
    &master_lpass_proc,
    &slave_imem
  },
};
static ul_route_list_type route_lpass_proc_imem_list =
{
  1,
  route_lpass_proc_imem_list_array
};
static ul_pair_type route_lpass_proc_message_ram_list_array[2] =
{
  {
    &master_lpass_proc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_message_ram
  },
};
static ul_route_list_type route_lpass_proc_message_ram_list =
{
  2,
  route_lpass_proc_message_ram_list_array
};
static ul_pair_type route_lpass_proc_pdm_list_array[2] =
{
  {
    &master_lpass_proc,
    &slave_snoc_pnoc
  },
  {
    &master_snoc_pnoc,
    &slave_pdm
  },
};
static ul_route_list_type route_lpass_proc_pdm_list =
{
  2,
  route_lpass_proc_pdm_list_array
};
static ul_pair_type route_lpass_proc_pimem_list_array[1] =
{
  {
    &master_lpass_proc,
    &slave_pimem
  },
};
static ul_route_list_type route_lpass_proc_pimem_list =
{
  1,
  route_lpass_proc_pimem_list_array
};
static ul_pair_type route_lpass_proc_pmic_arb_list_array[2] =
{
  {
    &master_lpass_proc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_pmic_arb
  },
};
static ul_route_list_type route_lpass_proc_pmic_arb_list =
{
  2,
  route_lpass_proc_pmic_arb_list_array
};
static ul_pair_type route_lpass_proc_qdss_stm_list_array[1] =
{
  {
    &master_lpass_proc,
    &slave_qdss_stm
  },
};
static ul_route_list_type route_lpass_proc_qdss_stm_list =
{
  1,
  route_lpass_proc_qdss_stm_list_array
};
static ul_pair_type route_lpass_proc_rpm_list_array[2] =
{
  {
    &master_lpass_proc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_rpm
  },
};
static ul_route_list_type route_lpass_proc_rpm_list =
{
  2,
  route_lpass_proc_rpm_list_array
};
static ul_pair_type route_lpass_proc_sdcc_1_list_array[2] =
{
  {
    &master_lpass_proc,
    &slave_snoc_pnoc
  },
  {
    &master_snoc_pnoc,
    &slave_sdcc_1
  },
};
static ul_route_list_type route_lpass_proc_sdcc_1_list =
{
  2,
  route_lpass_proc_sdcc_1_list_array
};
static ul_pair_type route_lpass_proc_sdcc_2_list_array[2] =
{
  {
    &master_lpass_proc,
    &slave_snoc_pnoc
  },
  {
    &master_snoc_pnoc,
    &slave_sdcc_2
  },
};
static ul_route_list_type route_lpass_proc_sdcc_2_list =
{
  2,
  route_lpass_proc_sdcc_2_list_array
};
static ul_pair_type route_lpass_proc_sdcc_4_list_array[2] =
{
  {
    &master_lpass_proc,
    &slave_snoc_pnoc
  },
  {
    &master_snoc_pnoc,
    &slave_sdcc_4
  },
};
static ul_route_list_type route_lpass_proc_sdcc_4_list =
{
  2,
  route_lpass_proc_sdcc_4_list_array
};
static ul_pair_type route_lpass_proc_tcsr_list_array[2] =
{
  {
    &master_lpass_proc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_tcsr
  },
};
static ul_route_list_type route_lpass_proc_tcsr_list =
{
  2,
  route_lpass_proc_tcsr_list_array
};
static ul_pair_type route_lpass_proc_tlmm_list_array[2] =
{
  {
    &master_lpass_proc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_tlmm
  },
};
static ul_route_list_type route_lpass_proc_tlmm_list =
{
  2,
  route_lpass_proc_tlmm_list_array
};
static ul_pair_type route_lpass_proc_tsif_list_array[2] =
{
  {
    &master_lpass_proc,
    &slave_snoc_pnoc
  },
  {
    &master_snoc_pnoc,
    &slave_tsif
  },
};
static ul_route_list_type route_lpass_proc_tsif_list =
{
  2,
  route_lpass_proc_tsif_list_array
};
static ul_pair_type route_lpass_proc_usb_hs_list_array[2] =
{
  {
    &master_lpass_proc,
    &slave_snoc_pnoc
  },
  {
    &master_snoc_pnoc,
    &slave_usb_hs
  },
};
static ul_route_list_type route_lpass_proc_usb_hs_list =
{
  2,
  route_lpass_proc_usb_hs_list_array
};
static ul_pair_type route_sdcc_1_clk_ctl_list_array[4] =
{
  {
    &master_sdcc_1,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_clk_ctl
  },
};
static ul_route_list_type route_sdcc_1_clk_ctl_list =
{
  4,
  route_sdcc_1_clk_ctl_list_array
};
static ul_pair_type route_sdcc_1_ebi_list_array[4] =
{
  {
    &master_sdcc_1,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_bimc
  },
  {
    &master_snoc_bimc,
    &slave_ebi
  },
};
static ul_route_list_type route_sdcc_1_ebi_list =
{
  4,
  route_sdcc_1_ebi_list_array
};
static ul_pair_type route_sdcc_1_hmss_l3_list_array[4] =
{
  {
    &master_sdcc_1,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_bimc
  },
  {
    &master_snoc_bimc,
    &slave_hmss_l3
  },
};
static ul_route_list_type route_sdcc_1_hmss_l3_list =
{
  4,
  route_sdcc_1_hmss_l3_list_array
};
static ul_pair_type route_sdcc_1_imem_list_array[3] =
{
  {
    &master_sdcc_1,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_imem
  },
};
static ul_route_list_type route_sdcc_1_imem_list =
{
  3,
  route_sdcc_1_imem_list_array
};
static ul_pair_type route_sdcc_1_message_ram_list_array[4] =
{
  {
    &master_sdcc_1,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_message_ram
  },
};
static ul_route_list_type route_sdcc_1_message_ram_list =
{
  4,
  route_sdcc_1_message_ram_list_array
};
static ul_pair_type route_sdcc_1_pimem_list_array[3] =
{
  {
    &master_sdcc_1,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_pimem
  },
};
static ul_route_list_type route_sdcc_1_pimem_list =
{
  3,
  route_sdcc_1_pimem_list_array
};
static ul_pair_type route_sdcc_1_pmic_arb_list_array[4] =
{
  {
    &master_sdcc_1,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_pmic_arb
  },
};
static ul_route_list_type route_sdcc_1_pmic_arb_list =
{
  4,
  route_sdcc_1_pmic_arb_list_array
};
static ul_pair_type route_sdcc_1_qdss_stm_list_array[3] =
{
  {
    &master_sdcc_1,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_qdss_stm
  },
};
static ul_route_list_type route_sdcc_1_qdss_stm_list =
{
  3,
  route_sdcc_1_qdss_stm_list_array
};
static ul_pair_type route_sdcc_1_rpm_list_array[4] =
{
  {
    &master_sdcc_1,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_rpm
  },
};
static ul_route_list_type route_sdcc_1_rpm_list =
{
  4,
  route_sdcc_1_rpm_list_array
};
static ul_pair_type route_sdcc_1_tcsr_list_array[4] =
{
  {
    &master_sdcc_1,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_tcsr
  },
};
static ul_route_list_type route_sdcc_1_tcsr_list =
{
  4,
  route_sdcc_1_tcsr_list_array
};
static ul_pair_type route_sdcc_1_tlmm_list_array[4] =
{
  {
    &master_sdcc_1,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_tlmm
  },
};
static ul_route_list_type route_sdcc_1_tlmm_list =
{
  4,
  route_sdcc_1_tlmm_list_array
};
static ul_pair_type route_sdcc_2_clk_ctl_list_array[4] =
{
  {
    &master_sdcc_2,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_clk_ctl
  },
};
static ul_route_list_type route_sdcc_2_clk_ctl_list =
{
  4,
  route_sdcc_2_clk_ctl_list_array
};
static ul_pair_type route_sdcc_2_ebi_list_array[4] =
{
  {
    &master_sdcc_2,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_bimc
  },
  {
    &master_snoc_bimc,
    &slave_ebi
  },
};
static ul_route_list_type route_sdcc_2_ebi_list =
{
  4,
  route_sdcc_2_ebi_list_array
};
static ul_pair_type route_sdcc_2_hmss_l3_list_array[4] =
{
  {
    &master_sdcc_2,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_bimc
  },
  {
    &master_snoc_bimc,
    &slave_hmss_l3
  },
};
static ul_route_list_type route_sdcc_2_hmss_l3_list =
{
  4,
  route_sdcc_2_hmss_l3_list_array
};
static ul_pair_type route_sdcc_2_imem_list_array[3] =
{
  {
    &master_sdcc_2,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_imem
  },
};
static ul_route_list_type route_sdcc_2_imem_list =
{
  3,
  route_sdcc_2_imem_list_array
};
static ul_pair_type route_sdcc_2_message_ram_list_array[4] =
{
  {
    &master_sdcc_2,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_message_ram
  },
};
static ul_route_list_type route_sdcc_2_message_ram_list =
{
  4,
  route_sdcc_2_message_ram_list_array
};
static ul_pair_type route_sdcc_2_pimem_list_array[3] =
{
  {
    &master_sdcc_2,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_pimem
  },
};
static ul_route_list_type route_sdcc_2_pimem_list =
{
  3,
  route_sdcc_2_pimem_list_array
};
static ul_pair_type route_sdcc_2_pmic_arb_list_array[4] =
{
  {
    &master_sdcc_2,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_pmic_arb
  },
};
static ul_route_list_type route_sdcc_2_pmic_arb_list =
{
  4,
  route_sdcc_2_pmic_arb_list_array
};
static ul_pair_type route_sdcc_2_qdss_stm_list_array[3] =
{
  {
    &master_sdcc_2,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_qdss_stm
  },
};
static ul_route_list_type route_sdcc_2_qdss_stm_list =
{
  3,
  route_sdcc_2_qdss_stm_list_array
};
static ul_pair_type route_sdcc_2_rpm_list_array[4] =
{
  {
    &master_sdcc_2,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_rpm
  },
};
static ul_route_list_type route_sdcc_2_rpm_list =
{
  4,
  route_sdcc_2_rpm_list_array
};
static ul_pair_type route_sdcc_2_tcsr_list_array[4] =
{
  {
    &master_sdcc_2,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_tcsr
  },
};
static ul_route_list_type route_sdcc_2_tcsr_list =
{
  4,
  route_sdcc_2_tcsr_list_array
};
static ul_pair_type route_sdcc_2_tlmm_list_array[4] =
{
  {
    &master_sdcc_2,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_tlmm
  },
};
static ul_route_list_type route_sdcc_2_tlmm_list =
{
  4,
  route_sdcc_2_tlmm_list_array
};
static ul_pair_type route_sdcc_4_clk_ctl_list_array[4] =
{
  {
    &master_sdcc_4,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_clk_ctl
  },
};
static ul_route_list_type route_sdcc_4_clk_ctl_list =
{
  4,
  route_sdcc_4_clk_ctl_list_array
};
static ul_pair_type route_sdcc_4_ebi_list_array[4] =
{
  {
    &master_sdcc_4,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_bimc
  },
  {
    &master_snoc_bimc,
    &slave_ebi
  },
};
static ul_route_list_type route_sdcc_4_ebi_list =
{
  4,
  route_sdcc_4_ebi_list_array
};
static ul_pair_type route_sdcc_4_hmss_l3_list_array[4] =
{
  {
    &master_sdcc_4,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_bimc
  },
  {
    &master_snoc_bimc,
    &slave_hmss_l3
  },
};
static ul_route_list_type route_sdcc_4_hmss_l3_list =
{
  4,
  route_sdcc_4_hmss_l3_list_array
};
static ul_pair_type route_sdcc_4_imem_list_array[3] =
{
  {
    &master_sdcc_4,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_imem
  },
};
static ul_route_list_type route_sdcc_4_imem_list =
{
  3,
  route_sdcc_4_imem_list_array
};
static ul_pair_type route_sdcc_4_message_ram_list_array[4] =
{
  {
    &master_sdcc_4,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_message_ram
  },
};
static ul_route_list_type route_sdcc_4_message_ram_list =
{
  4,
  route_sdcc_4_message_ram_list_array
};
static ul_pair_type route_sdcc_4_pimem_list_array[3] =
{
  {
    &master_sdcc_4,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_pimem
  },
};
static ul_route_list_type route_sdcc_4_pimem_list =
{
  3,
  route_sdcc_4_pimem_list_array
};
static ul_pair_type route_sdcc_4_pmic_arb_list_array[4] =
{
  {
    &master_sdcc_4,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_pmic_arb
  },
};
static ul_route_list_type route_sdcc_4_pmic_arb_list =
{
  4,
  route_sdcc_4_pmic_arb_list_array
};
static ul_pair_type route_sdcc_4_qdss_stm_list_array[3] =
{
  {
    &master_sdcc_4,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_qdss_stm
  },
};
static ul_route_list_type route_sdcc_4_qdss_stm_list =
{
  3,
  route_sdcc_4_qdss_stm_list_array
};
static ul_pair_type route_sdcc_4_rpm_list_array[4] =
{
  {
    &master_sdcc_4,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_rpm
  },
};
static ul_route_list_type route_sdcc_4_rpm_list =
{
  4,
  route_sdcc_4_rpm_list_array
};
static ul_pair_type route_sdcc_4_tcsr_list_array[4] =
{
  {
    &master_sdcc_4,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_tcsr
  },
};
static ul_route_list_type route_sdcc_4_tcsr_list =
{
  4,
  route_sdcc_4_tcsr_list_array
};
static ul_pair_type route_sdcc_4_tlmm_list_array[4] =
{
  {
    &master_sdcc_4,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_tlmm
  },
};
static ul_route_list_type route_sdcc_4_tlmm_list =
{
  4,
  route_sdcc_4_tlmm_list_array
};
static ul_pair_type route_tsif_clk_ctl_list_array[4] =
{
  {
    &master_tsif,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_clk_ctl
  },
};
static ul_route_list_type route_tsif_clk_ctl_list =
{
  4,
  route_tsif_clk_ctl_list_array
};
static ul_pair_type route_tsif_ebi_list_array[4] =
{
  {
    &master_tsif,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_bimc
  },
  {
    &master_snoc_bimc,
    &slave_ebi
  },
};
static ul_route_list_type route_tsif_ebi_list =
{
  4,
  route_tsif_ebi_list_array
};
static ul_pair_type route_tsif_hmss_l3_list_array[4] =
{
  {
    &master_tsif,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_bimc
  },
  {
    &master_snoc_bimc,
    &slave_hmss_l3
  },
};
static ul_route_list_type route_tsif_hmss_l3_list =
{
  4,
  route_tsif_hmss_l3_list_array
};
static ul_pair_type route_tsif_imem_list_array[3] =
{
  {
    &master_tsif,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_imem
  },
};
static ul_route_list_type route_tsif_imem_list =
{
  3,
  route_tsif_imem_list_array
};
static ul_pair_type route_tsif_message_ram_list_array[4] =
{
  {
    &master_tsif,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_message_ram
  },
};
static ul_route_list_type route_tsif_message_ram_list =
{
  4,
  route_tsif_message_ram_list_array
};
static ul_pair_type route_tsif_pimem_list_array[3] =
{
  {
    &master_tsif,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_pimem
  },
};
static ul_route_list_type route_tsif_pimem_list =
{
  3,
  route_tsif_pimem_list_array
};
static ul_pair_type route_tsif_pmic_arb_list_array[4] =
{
  {
    &master_tsif,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_pmic_arb
  },
};
static ul_route_list_type route_tsif_pmic_arb_list =
{
  4,
  route_tsif_pmic_arb_list_array
};
static ul_pair_type route_tsif_qdss_stm_list_array[3] =
{
  {
    &master_tsif,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_qdss_stm
  },
};
static ul_route_list_type route_tsif_qdss_stm_list =
{
  3,
  route_tsif_qdss_stm_list_array
};
static ul_pair_type route_tsif_rpm_list_array[4] =
{
  {
    &master_tsif,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_rpm
  },
};
static ul_route_list_type route_tsif_rpm_list =
{
  4,
  route_tsif_rpm_list_array
};
static ul_pair_type route_tsif_tcsr_list_array[4] =
{
  {
    &master_tsif,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_tcsr
  },
};
static ul_route_list_type route_tsif_tcsr_list =
{
  4,
  route_tsif_tcsr_list_array
};
static ul_pair_type route_tsif_tlmm_list_array[4] =
{
  {
    &master_tsif,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_tlmm
  },
};
static ul_route_list_type route_tsif_tlmm_list =
{
  4,
  route_tsif_tlmm_list_array
};
static ul_pair_type route_usb_hs_clk_ctl_list_array[4] =
{
  {
    &master_usb_hs,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_clk_ctl
  },
};
static ul_route_list_type route_usb_hs_clk_ctl_list =
{
  4,
  route_usb_hs_clk_ctl_list_array
};
static ul_pair_type route_usb_hs_ebi_list_array[4] =
{
  {
    &master_usb_hs,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_bimc
  },
  {
    &master_snoc_bimc,
    &slave_ebi
  },
};
static ul_route_list_type route_usb_hs_ebi_list =
{
  4,
  route_usb_hs_ebi_list_array
};
static ul_pair_type route_usb_hs_hmss_l3_list_array[4] =
{
  {
    &master_usb_hs,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_bimc
  },
  {
    &master_snoc_bimc,
    &slave_hmss_l3
  },
};
static ul_route_list_type route_usb_hs_hmss_l3_list =
{
  4,
  route_usb_hs_hmss_l3_list_array
};
static ul_pair_type route_usb_hs_imem_list_array[3] =
{
  {
    &master_usb_hs,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_imem
  },
};
static ul_route_list_type route_usb_hs_imem_list =
{
  3,
  route_usb_hs_imem_list_array
};
static ul_pair_type route_usb_hs_message_ram_list_array[4] =
{
  {
    &master_usb_hs,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_message_ram
  },
};
static ul_route_list_type route_usb_hs_message_ram_list =
{
  4,
  route_usb_hs_message_ram_list_array
};
static ul_pair_type route_usb_hs_pimem_list_array[3] =
{
  {
    &master_usb_hs,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_pimem
  },
};
static ul_route_list_type route_usb_hs_pimem_list =
{
  3,
  route_usb_hs_pimem_list_array
};
static ul_pair_type route_usb_hs_pmic_arb_list_array[4] =
{
  {
    &master_usb_hs,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_pmic_arb
  },
};
static ul_route_list_type route_usb_hs_pmic_arb_list =
{
  4,
  route_usb_hs_pmic_arb_list_array
};
static ul_pair_type route_usb_hs_qdss_stm_list_array[3] =
{
  {
    &master_usb_hs,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_qdss_stm
  },
};
static ul_route_list_type route_usb_hs_qdss_stm_list =
{
  3,
  route_usb_hs_qdss_stm_list_array
};
static ul_pair_type route_usb_hs_rpm_list_array[4] =
{
  {
    &master_usb_hs,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_rpm
  },
};
static ul_route_list_type route_usb_hs_rpm_list =
{
  4,
  route_usb_hs_rpm_list_array
};
static ul_pair_type route_usb_hs_tcsr_list_array[4] =
{
  {
    &master_usb_hs,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_tcsr
  },
};
static ul_route_list_type route_usb_hs_tcsr_list =
{
  4,
  route_usb_hs_tcsr_list_array
};
static ul_pair_type route_usb_hs_tlmm_list_array[4] =
{
  {
    &master_usb_hs,
    &slave_pnoc_a1noc
  },
  {
    &master_pnoc_a1noc,
    &slave_a1noc_snoc
  },
  {
    &master_a1noc_snoc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_tlmm
  },
};
static ul_route_list_type route_usb_hs_tlmm_list =
{
  4,
  route_usb_hs_tlmm_list_array
};
static ul_treeNodeType route_0x1200000000L_lpass_ahb_ebi_node;
static ul_treeNodeType route_0x120000001aL_lpass_ahb_imem_node;
static ul_treeNodeType route_0x120000001fL_lpass_ahb_sdcc_1_node;
static ul_treeNodeType route_0x1200000021L_lpass_ahb_sdcc_2_node;
static ul_treeNodeType route_0x1200000022L_lpass_ahb_sdcc_4_node;
static ul_treeNodeType route_0x1200000023L_lpass_ahb_tsif_node;
static ul_treeNodeType route_0x1200000025L_lpass_ahb_blsp_2_node;
static ul_treeNodeType route_0x1200000027L_lpass_ahb_blsp_1_node;
static ul_treeNodeType route_0x1200000028L_lpass_ahb_usb_hs_node;
static ul_treeNodeType route_0x1200000029L_lpass_ahb_pdm_node;
static ul_treeNodeType route_0x12000000a0L_lpass_ahb_hmss_l3_node;
static ul_treeNodeType route_0x12000000a6L_lpass_ahb_pimem_node;
static ul_treeNodeType route_0x1700000000L_crypto_c0_ebi_node;
static ul_treeNodeType route_0x170000001aL_crypto_c0_imem_node;
static ul_treeNodeType route_0x170000001eL_crypto_c0_qdss_stm_node;
static ul_treeNodeType route_0x170000001fL_crypto_c0_sdcc_1_node;
static ul_treeNodeType route_0x1700000021L_crypto_c0_sdcc_2_node;
static ul_treeNodeType route_0x1700000022L_crypto_c0_sdcc_4_node;
static ul_treeNodeType route_0x1700000023L_crypto_c0_tsif_node;
static ul_treeNodeType route_0x1700000025L_crypto_c0_blsp_2_node;
static ul_treeNodeType route_0x1700000027L_crypto_c0_blsp_1_node;
static ul_treeNodeType route_0x1700000028L_crypto_c0_usb_hs_node;
static ul_treeNodeType route_0x1700000029L_crypto_c0_pdm_node;
static ul_treeNodeType route_0x170000002fL_crypto_c0_clk_ctl_node;
static ul_treeNodeType route_0x1700000032L_crypto_c0_tcsr_node;
static ul_treeNodeType route_0x1700000033L_crypto_c0_tlmm_node;
static ul_treeNodeType route_0x1700000037L_crypto_c0_message_ram_node;
static ul_treeNodeType route_0x170000003bL_crypto_c0_pmic_arb_node;
static ul_treeNodeType route_0x170000004aL_crypto_c0_rpm_node;
static ul_treeNodeType route_0x17000000a0L_crypto_c0_hmss_l3_node;
static ul_treeNodeType route_0x17000000a6L_crypto_c0_pimem_node;
static ul_treeNodeType route_0x1900000000L_lpass_proc_ebi_node;
static ul_treeNodeType route_0x190000001aL_lpass_proc_imem_node;
static ul_treeNodeType route_0x190000001eL_lpass_proc_qdss_stm_node;
static ul_treeNodeType route_0x190000001fL_lpass_proc_sdcc_1_node;
static ul_treeNodeType route_0x1900000021L_lpass_proc_sdcc_2_node;
static ul_treeNodeType route_0x1900000022L_lpass_proc_sdcc_4_node;
static ul_treeNodeType route_0x1900000023L_lpass_proc_tsif_node;
static ul_treeNodeType route_0x1900000025L_lpass_proc_blsp_2_node;
static ul_treeNodeType route_0x1900000027L_lpass_proc_blsp_1_node;
static ul_treeNodeType route_0x1900000028L_lpass_proc_usb_hs_node;
static ul_treeNodeType route_0x1900000029L_lpass_proc_pdm_node;
static ul_treeNodeType route_0x190000002fL_lpass_proc_clk_ctl_node;
static ul_treeNodeType route_0x1900000032L_lpass_proc_tcsr_node;
static ul_treeNodeType route_0x1900000033L_lpass_proc_tlmm_node;
static ul_treeNodeType route_0x1900000037L_lpass_proc_message_ram_node;
static ul_treeNodeType route_0x190000003bL_lpass_proc_pmic_arb_node;
static ul_treeNodeType route_0x190000004aL_lpass_proc_rpm_node;
static ul_treeNodeType route_0x19000000a0L_lpass_proc_hmss_l3_node;
static ul_treeNodeType route_0x19000000a6L_lpass_proc_pimem_node;
static ul_treeNodeType route_0x2100000000L_sdcc_1_ebi_node;
static ul_treeNodeType route_0x210000001aL_sdcc_1_imem_node;
static ul_treeNodeType route_0x210000001eL_sdcc_1_qdss_stm_node;
static ul_treeNodeType route_0x210000002fL_sdcc_1_clk_ctl_node;
static ul_treeNodeType route_0x2100000032L_sdcc_1_tcsr_node;
static ul_treeNodeType route_0x2100000033L_sdcc_1_tlmm_node;
static ul_treeNodeType route_0x2100000037L_sdcc_1_message_ram_node;
static ul_treeNodeType route_0x210000003bL_sdcc_1_pmic_arb_node;
static ul_treeNodeType route_0x210000004aL_sdcc_1_rpm_node;
static ul_treeNodeType route_0x21000000a0L_sdcc_1_hmss_l3_node;
static ul_treeNodeType route_0x21000000a6L_sdcc_1_pimem_node;
static ul_treeNodeType route_0x2300000000L_sdcc_2_ebi_node;
static ul_treeNodeType route_0x230000001aL_sdcc_2_imem_node;
static ul_treeNodeType route_0x230000001eL_sdcc_2_qdss_stm_node;
static ul_treeNodeType route_0x230000002fL_sdcc_2_clk_ctl_node;
static ul_treeNodeType route_0x2300000032L_sdcc_2_tcsr_node;
static ul_treeNodeType route_0x2300000033L_sdcc_2_tlmm_node;
static ul_treeNodeType route_0x2300000037L_sdcc_2_message_ram_node;
static ul_treeNodeType route_0x230000003bL_sdcc_2_pmic_arb_node;
static ul_treeNodeType route_0x230000004aL_sdcc_2_rpm_node;
static ul_treeNodeType route_0x23000000a0L_sdcc_2_hmss_l3_node;
static ul_treeNodeType route_0x23000000a6L_sdcc_2_pimem_node;
static ul_treeNodeType route_0x2400000000L_sdcc_4_ebi_node;
static ul_treeNodeType route_0x240000001aL_sdcc_4_imem_node;
static ul_treeNodeType route_0x240000001eL_sdcc_4_qdss_stm_node;
static ul_treeNodeType route_0x240000002fL_sdcc_4_clk_ctl_node;
static ul_treeNodeType route_0x2400000032L_sdcc_4_tcsr_node;
static ul_treeNodeType route_0x2400000033L_sdcc_4_tlmm_node;
static ul_treeNodeType route_0x2400000037L_sdcc_4_message_ram_node;
static ul_treeNodeType route_0x240000003bL_sdcc_4_pmic_arb_node;
static ul_treeNodeType route_0x240000004aL_sdcc_4_rpm_node;
static ul_treeNodeType route_0x24000000a0L_sdcc_4_hmss_l3_node;
static ul_treeNodeType route_0x24000000a6L_sdcc_4_pimem_node;
static ul_treeNodeType route_0x2500000000L_tsif_ebi_node;
static ul_treeNodeType route_0x250000001aL_tsif_imem_node;
static ul_treeNodeType route_0x250000001eL_tsif_qdss_stm_node;
static ul_treeNodeType route_0x250000002fL_tsif_clk_ctl_node;
static ul_treeNodeType route_0x2500000032L_tsif_tcsr_node;
static ul_treeNodeType route_0x2500000033L_tsif_tlmm_node;
static ul_treeNodeType route_0x2500000037L_tsif_message_ram_node;
static ul_treeNodeType route_0x250000003bL_tsif_pmic_arb_node;
static ul_treeNodeType route_0x250000004aL_tsif_rpm_node;
static ul_treeNodeType route_0x25000000a0L_tsif_hmss_l3_node;
static ul_treeNodeType route_0x25000000a6L_tsif_pimem_node;
static ul_treeNodeType route_0x2700000000L_blsp_2_ebi_node;
static ul_treeNodeType route_0x270000001aL_blsp_2_imem_node;
static ul_treeNodeType route_0x270000001eL_blsp_2_qdss_stm_node;
static ul_treeNodeType route_0x270000002fL_blsp_2_clk_ctl_node;
static ul_treeNodeType route_0x2700000032L_blsp_2_tcsr_node;
static ul_treeNodeType route_0x2700000033L_blsp_2_tlmm_node;
static ul_treeNodeType route_0x2700000037L_blsp_2_message_ram_node;
static ul_treeNodeType route_0x270000003bL_blsp_2_pmic_arb_node;
static ul_treeNodeType route_0x270000004aL_blsp_2_rpm_node;
static ul_treeNodeType route_0x27000000a0L_blsp_2_hmss_l3_node;
static ul_treeNodeType route_0x27000000a6L_blsp_2_pimem_node;
static ul_treeNodeType route_0x2900000000L_blsp_1_ebi_node;
static ul_treeNodeType route_0x290000001aL_blsp_1_imem_node;
static ul_treeNodeType route_0x290000001eL_blsp_1_qdss_stm_node;
static ul_treeNodeType route_0x290000002fL_blsp_1_clk_ctl_node;
static ul_treeNodeType route_0x2900000032L_blsp_1_tcsr_node;
static ul_treeNodeType route_0x2900000033L_blsp_1_tlmm_node;
static ul_treeNodeType route_0x2900000037L_blsp_1_message_ram_node;
static ul_treeNodeType route_0x290000003bL_blsp_1_pmic_arb_node;
static ul_treeNodeType route_0x290000004aL_blsp_1_rpm_node;
static ul_treeNodeType route_0x29000000a0L_blsp_1_hmss_l3_node;
static ul_treeNodeType route_0x29000000a6L_blsp_1_pimem_node;
static ul_treeNodeType route_0x2a00000000L_usb_hs_ebi_node;
static ul_treeNodeType route_0x2a0000001aL_usb_hs_imem_node;
static ul_treeNodeType route_0x2a0000001eL_usb_hs_qdss_stm_node;
static ul_treeNodeType route_0x2a0000002fL_usb_hs_clk_ctl_node;
static ul_treeNodeType route_0x2a00000032L_usb_hs_tcsr_node;
static ul_treeNodeType route_0x2a00000033L_usb_hs_tlmm_node;
static ul_treeNodeType route_0x2a00000037L_usb_hs_message_ram_node;
static ul_treeNodeType route_0x2a0000003bL_usb_hs_pmic_arb_node;
static ul_treeNodeType route_0x2a0000004aL_usb_hs_rpm_node;
static ul_treeNodeType route_0x2a000000a0L_usb_hs_hmss_l3_node;
static ul_treeNodeType route_0x2a000000a6L_usb_hs_pimem_node;
static ul_treeNodeType route_0x1200000000L_lpass_ahb_ebi_node =
{
  0x1200000000ULL,
  &route_lpass_ahb_ebi_list,
  0,
  0
};
static ul_treeNodeType route_0x120000001aL_lpass_ahb_imem_node =
{
  0x120000001aULL,
  &route_lpass_ahb_imem_list,
  &route_0x1200000000L_lpass_ahb_ebi_node,
  &route_0x120000001fL_lpass_ahb_sdcc_1_node
};
static ul_treeNodeType route_0x120000001fL_lpass_ahb_sdcc_1_node =
{
  0x120000001fULL,
  &route_lpass_ahb_sdcc_1_list,
  0,
  0
};
static ul_treeNodeType route_0x1200000021L_lpass_ahb_sdcc_2_node =
{
  0x1200000021ULL,
  &route_lpass_ahb_sdcc_2_list,
  &route_0x120000001aL_lpass_ahb_imem_node,
  &route_0x1200000022L_lpass_ahb_sdcc_4_node
};
static ul_treeNodeType route_0x1200000022L_lpass_ahb_sdcc_4_node =
{
  0x1200000022ULL,
  &route_lpass_ahb_sdcc_4_list,
  0,
  &route_0x1200000023L_lpass_ahb_tsif_node
};
static ul_treeNodeType route_0x1200000023L_lpass_ahb_tsif_node =
{
  0x1200000023ULL,
  &route_lpass_ahb_tsif_list,
  0,
  0
};
static ul_treeNodeType route_0x1200000025L_lpass_ahb_blsp_2_node =
{
  0x1200000025ULL,
  &route_lpass_ahb_blsp_2_list,
  &route_0x1200000021L_lpass_ahb_sdcc_2_node,
  &route_0x1700000000L_crypto_c0_ebi_node
};
static ul_treeNodeType route_0x1200000027L_lpass_ahb_blsp_1_node =
{
  0x1200000027ULL,
  &route_lpass_ahb_blsp_1_list,
  0,
  0
};
static ul_treeNodeType route_0x1200000028L_lpass_ahb_usb_hs_node =
{
  0x1200000028ULL,
  &route_lpass_ahb_usb_hs_list,
  &route_0x1200000027L_lpass_ahb_blsp_1_node,
  &route_0x1200000029L_lpass_ahb_pdm_node
};
static ul_treeNodeType route_0x1200000029L_lpass_ahb_pdm_node =
{
  0x1200000029ULL,
  &route_lpass_ahb_pdm_list,
  0,
  0
};
static ul_treeNodeType route_0x12000000a0L_lpass_ahb_hmss_l3_node =
{
  0x12000000a0ULL,
  &route_lpass_ahb_hmss_l3_list,
  &route_0x1200000028L_lpass_ahb_usb_hs_node,
  &route_0x12000000a6L_lpass_ahb_pimem_node
};
static ul_treeNodeType route_0x12000000a6L_lpass_ahb_pimem_node =
{
  0x12000000a6ULL,
  &route_lpass_ahb_pimem_list,
  0,
  0
};
static ul_treeNodeType route_0x1700000000L_crypto_c0_ebi_node =
{
  0x1700000000ULL,
  &route_crypto_c0_ebi_list,
  &route_0x12000000a0L_lpass_ahb_hmss_l3_node,
  &route_0x170000001eL_crypto_c0_qdss_stm_node
};
static ul_treeNodeType route_0x170000001aL_crypto_c0_imem_node =
{
  0x170000001aULL,
  &route_crypto_c0_imem_list,
  0,
  0
};
static ul_treeNodeType route_0x170000001eL_crypto_c0_qdss_stm_node =
{
  0x170000001eULL,
  &route_crypto_c0_qdss_stm_list,
  &route_0x170000001aL_crypto_c0_imem_node,
  &route_0x170000001fL_crypto_c0_sdcc_1_node
};
static ul_treeNodeType route_0x170000001fL_crypto_c0_sdcc_1_node =
{
  0x170000001fULL,
  &route_crypto_c0_sdcc_1_list,
  0,
  0
};
static ul_treeNodeType route_0x1700000021L_crypto_c0_sdcc_2_node =
{
  0x1700000021ULL,
  &route_crypto_c0_sdcc_2_list,
  &route_0x1200000025L_lpass_ahb_blsp_2_node,
  &route_0x1700000032L_crypto_c0_tcsr_node
};
static ul_treeNodeType route_0x1700000022L_crypto_c0_sdcc_4_node =
{
  0x1700000022ULL,
  &route_crypto_c0_sdcc_4_list,
  0,
  0
};
static ul_treeNodeType route_0x1700000023L_crypto_c0_tsif_node =
{
  0x1700000023ULL,
  &route_crypto_c0_tsif_list,
  &route_0x1700000022L_crypto_c0_sdcc_4_node,
  &route_0x1700000025L_crypto_c0_blsp_2_node
};
static ul_treeNodeType route_0x1700000025L_crypto_c0_blsp_2_node =
{
  0x1700000025ULL,
  &route_crypto_c0_blsp_2_list,
  0,
  0
};
static ul_treeNodeType route_0x1700000027L_crypto_c0_blsp_1_node =
{
  0x1700000027ULL,
  &route_crypto_c0_blsp_1_list,
  &route_0x1700000023L_crypto_c0_tsif_node,
  &route_0x1700000029L_crypto_c0_pdm_node
};
static ul_treeNodeType route_0x1700000028L_crypto_c0_usb_hs_node =
{
  0x1700000028ULL,
  &route_crypto_c0_usb_hs_list,
  0,
  0
};
static ul_treeNodeType route_0x1700000029L_crypto_c0_pdm_node =
{
  0x1700000029ULL,
  &route_crypto_c0_pdm_list,
  &route_0x1700000028L_crypto_c0_usb_hs_node,
  &route_0x170000002fL_crypto_c0_clk_ctl_node
};
static ul_treeNodeType route_0x170000002fL_crypto_c0_clk_ctl_node =
{
  0x170000002fULL,
  &route_crypto_c0_clk_ctl_list,
  0,
  0
};
static ul_treeNodeType route_0x1700000032L_crypto_c0_tcsr_node =
{
  0x1700000032ULL,
  &route_crypto_c0_tcsr_list,
  &route_0x1700000027L_crypto_c0_blsp_1_node,
  &route_0x170000004aL_crypto_c0_rpm_node
};
static ul_treeNodeType route_0x1700000033L_crypto_c0_tlmm_node =
{
  0x1700000033ULL,
  &route_crypto_c0_tlmm_list,
  0,
  0
};
static ul_treeNodeType route_0x1700000037L_crypto_c0_message_ram_node =
{
  0x1700000037ULL,
  &route_crypto_c0_message_ram_list,
  &route_0x1700000033L_crypto_c0_tlmm_node,
  &route_0x170000003bL_crypto_c0_pmic_arb_node
};
static ul_treeNodeType route_0x170000003bL_crypto_c0_pmic_arb_node =
{
  0x170000003bULL,
  &route_crypto_c0_pmic_arb_list,
  0,
  0
};
static ul_treeNodeType route_0x170000004aL_crypto_c0_rpm_node =
{
  0x170000004aULL,
  &route_crypto_c0_rpm_list,
  &route_0x1700000037L_crypto_c0_message_ram_node,
  &route_0x17000000a0L_crypto_c0_hmss_l3_node
};
static ul_treeNodeType route_0x17000000a0L_crypto_c0_hmss_l3_node =
{
  0x17000000a0ULL,
  &route_crypto_c0_hmss_l3_list,
  0,
  &route_0x17000000a6L_crypto_c0_pimem_node
};
static ul_treeNodeType route_0x17000000a6L_crypto_c0_pimem_node =
{
  0x17000000a6ULL,
  &route_crypto_c0_pimem_list,
  0,
  0
};
static ul_treeNodeType route_0x1900000000L_lpass_proc_ebi_node =
{
  0x1900000000ULL,
  &route_lpass_proc_ebi_list,
  &route_0x1700000021L_crypto_c0_sdcc_2_node,
  &route_0x210000002fL_sdcc_1_clk_ctl_node
};
static ul_treeNodeType route_0x190000001aL_lpass_proc_imem_node =
{
  0x190000001aULL,
  &route_lpass_proc_imem_list,
  0,
  0
};
static ul_treeNodeType route_0x190000001eL_lpass_proc_qdss_stm_node =
{
  0x190000001eULL,
  &route_lpass_proc_qdss_stm_list,
  &route_0x190000001aL_lpass_proc_imem_node,
  &route_0x190000001fL_lpass_proc_sdcc_1_node
};
static ul_treeNodeType route_0x190000001fL_lpass_proc_sdcc_1_node =
{
  0x190000001fULL,
  &route_lpass_proc_sdcc_1_list,
  0,
  &route_0x1900000021L_lpass_proc_sdcc_2_node
};
static ul_treeNodeType route_0x1900000021L_lpass_proc_sdcc_2_node =
{
  0x1900000021ULL,
  &route_lpass_proc_sdcc_2_list,
  0,
  0
};
static ul_treeNodeType route_0x1900000022L_lpass_proc_sdcc_4_node =
{
  0x1900000022ULL,
  &route_lpass_proc_sdcc_4_list,
  &route_0x190000001eL_lpass_proc_qdss_stm_node,
  &route_0x1900000028L_lpass_proc_usb_hs_node
};
static ul_treeNodeType route_0x1900000023L_lpass_proc_tsif_node =
{
  0x1900000023ULL,
  &route_lpass_proc_tsif_list,
  0,
  0
};
static ul_treeNodeType route_0x1900000025L_lpass_proc_blsp_2_node =
{
  0x1900000025ULL,
  &route_lpass_proc_blsp_2_list,
  &route_0x1900000023L_lpass_proc_tsif_node,
  &route_0x1900000027L_lpass_proc_blsp_1_node
};
static ul_treeNodeType route_0x1900000027L_lpass_proc_blsp_1_node =
{
  0x1900000027ULL,
  &route_lpass_proc_blsp_1_list,
  0,
  0
};
static ul_treeNodeType route_0x1900000028L_lpass_proc_usb_hs_node =
{
  0x1900000028ULL,
  &route_lpass_proc_usb_hs_list,
  &route_0x1900000025L_lpass_proc_blsp_2_node,
  &route_0x1900000029L_lpass_proc_pdm_node
};
static ul_treeNodeType route_0x1900000029L_lpass_proc_pdm_node =
{
  0x1900000029ULL,
  &route_lpass_proc_pdm_list,
  0,
  0
};
static ul_treeNodeType route_0x190000002fL_lpass_proc_clk_ctl_node =
{
  0x190000002fULL,
  &route_lpass_proc_clk_ctl_list,
  &route_0x1900000022L_lpass_proc_sdcc_4_node,
  &route_0x190000003bL_lpass_proc_pmic_arb_node
};
static ul_treeNodeType route_0x1900000032L_lpass_proc_tcsr_node =
{
  0x1900000032ULL,
  &route_lpass_proc_tcsr_list,
  0,
  0
};
static ul_treeNodeType route_0x1900000033L_lpass_proc_tlmm_node =
{
  0x1900000033ULL,
  &route_lpass_proc_tlmm_list,
  &route_0x1900000032L_lpass_proc_tcsr_node,
  &route_0x1900000037L_lpass_proc_message_ram_node
};
static ul_treeNodeType route_0x1900000037L_lpass_proc_message_ram_node =
{
  0x1900000037ULL,
  &route_lpass_proc_message_ram_list,
  0,
  0
};
static ul_treeNodeType route_0x190000003bL_lpass_proc_pmic_arb_node =
{
  0x190000003bULL,
  &route_lpass_proc_pmic_arb_list,
  &route_0x1900000033L_lpass_proc_tlmm_node,
  &route_0x2100000000L_sdcc_1_ebi_node
};
static ul_treeNodeType route_0x190000004aL_lpass_proc_rpm_node =
{
  0x190000004aULL,
  &route_lpass_proc_rpm_list,
  0,
  0
};
static ul_treeNodeType route_0x19000000a0L_lpass_proc_hmss_l3_node =
{
  0x19000000a0ULL,
  &route_lpass_proc_hmss_l3_list,
  &route_0x190000004aL_lpass_proc_rpm_node,
  &route_0x19000000a6L_lpass_proc_pimem_node
};
static ul_treeNodeType route_0x19000000a6L_lpass_proc_pimem_node =
{
  0x19000000a6ULL,
  &route_lpass_proc_pimem_list,
  0,
  0
};
static ul_treeNodeType route_0x2100000000L_sdcc_1_ebi_node =
{
  0x2100000000ULL,
  &route_sdcc_1_ebi_list,
  &route_0x19000000a0L_lpass_proc_hmss_l3_node,
  &route_0x210000001eL_sdcc_1_qdss_stm_node
};
static ul_treeNodeType route_0x210000001aL_sdcc_1_imem_node =
{
  0x210000001aULL,
  &route_sdcc_1_imem_list,
  0,
  0
};
static ul_treeNodeType route_0x210000001eL_sdcc_1_qdss_stm_node =
{
  0x210000001eULL,
  &route_sdcc_1_qdss_stm_list,
  &route_0x210000001aL_sdcc_1_imem_node,
  0
};
static ul_treeNodeType route_0x210000002fL_sdcc_1_clk_ctl_node =
{
  0x210000002fULL,
  &route_sdcc_1_clk_ctl_list,
  &route_0x190000002fL_lpass_proc_clk_ctl_node,
  &route_0x2300000000L_sdcc_2_ebi_node
};
static ul_treeNodeType route_0x2100000032L_sdcc_1_tcsr_node =
{
  0x2100000032ULL,
  &route_sdcc_1_tcsr_list,
  0,
  &route_0x2100000033L_sdcc_1_tlmm_node
};
static ul_treeNodeType route_0x2100000033L_sdcc_1_tlmm_node =
{
  0x2100000033ULL,
  &route_sdcc_1_tlmm_list,
  0,
  0
};
static ul_treeNodeType route_0x2100000037L_sdcc_1_message_ram_node =
{
  0x2100000037ULL,
  &route_sdcc_1_message_ram_list,
  &route_0x2100000032L_sdcc_1_tcsr_node,
  &route_0x210000003bL_sdcc_1_pmic_arb_node
};
static ul_treeNodeType route_0x210000003bL_sdcc_1_pmic_arb_node =
{
  0x210000003bULL,
  &route_sdcc_1_pmic_arb_list,
  0,
  0
};
static ul_treeNodeType route_0x210000004aL_sdcc_1_rpm_node =
{
  0x210000004aULL,
  &route_sdcc_1_rpm_list,
  &route_0x2100000037L_sdcc_1_message_ram_node,
  &route_0x21000000a0L_sdcc_1_hmss_l3_node
};
static ul_treeNodeType route_0x21000000a0L_sdcc_1_hmss_l3_node =
{
  0x21000000a0ULL,
  &route_sdcc_1_hmss_l3_list,
  0,
  &route_0x21000000a6L_sdcc_1_pimem_node
};
static ul_treeNodeType route_0x21000000a6L_sdcc_1_pimem_node =
{
  0x21000000a6ULL,
  &route_sdcc_1_pimem_list,
  0,
  0
};
static ul_treeNodeType route_0x2300000000L_sdcc_2_ebi_node =
{
  0x2300000000ULL,
  &route_sdcc_2_ebi_list,
  &route_0x210000004aL_sdcc_1_rpm_node,
  &route_0x230000002fL_sdcc_2_clk_ctl_node
};
static ul_treeNodeType route_0x230000001aL_sdcc_2_imem_node =
{
  0x230000001aULL,
  &route_sdcc_2_imem_list,
  0,
  &route_0x230000001eL_sdcc_2_qdss_stm_node
};
static ul_treeNodeType route_0x230000001eL_sdcc_2_qdss_stm_node =
{
  0x230000001eULL,
  &route_sdcc_2_qdss_stm_list,
  0,
  0
};
static ul_treeNodeType route_0x230000002fL_sdcc_2_clk_ctl_node =
{
  0x230000002fULL,
  &route_sdcc_2_clk_ctl_list,
  &route_0x230000001aL_sdcc_2_imem_node,
  &route_0x2300000032L_sdcc_2_tcsr_node
};
static ul_treeNodeType route_0x2300000032L_sdcc_2_tcsr_node =
{
  0x2300000032ULL,
  &route_sdcc_2_tcsr_list,
  0,
  0
};
static ul_treeNodeType route_0x2300000033L_sdcc_2_tlmm_node =
{
  0x2300000033ULL,
  &route_sdcc_2_tlmm_list,
  &route_0x1900000000L_lpass_proc_ebi_node,
  &route_0x2900000000L_blsp_1_ebi_node
};
static ul_treeNodeType route_0x2300000037L_sdcc_2_message_ram_node =
{
  0x2300000037ULL,
  &route_sdcc_2_message_ram_list,
  0,
  &route_0x230000003bL_sdcc_2_pmic_arb_node
};
static ul_treeNodeType route_0x230000003bL_sdcc_2_pmic_arb_node =
{
  0x230000003bULL,
  &route_sdcc_2_pmic_arb_list,
  0,
  0
};
static ul_treeNodeType route_0x230000004aL_sdcc_2_rpm_node =
{
  0x230000004aULL,
  &route_sdcc_2_rpm_list,
  &route_0x2300000037L_sdcc_2_message_ram_node,
  &route_0x2400000000L_sdcc_4_ebi_node
};
static ul_treeNodeType route_0x23000000a0L_sdcc_2_hmss_l3_node =
{
  0x23000000a0ULL,
  &route_sdcc_2_hmss_l3_list,
  0,
  &route_0x23000000a6L_sdcc_2_pimem_node
};
static ul_treeNodeType route_0x23000000a6L_sdcc_2_pimem_node =
{
  0x23000000a6ULL,
  &route_sdcc_2_pimem_list,
  0,
  0
};
static ul_treeNodeType route_0x2400000000L_sdcc_4_ebi_node =
{
  0x2400000000ULL,
  &route_sdcc_4_ebi_list,
  &route_0x23000000a0L_sdcc_2_hmss_l3_node,
  &route_0x240000001eL_sdcc_4_qdss_stm_node
};
static ul_treeNodeType route_0x240000001aL_sdcc_4_imem_node =
{
  0x240000001aULL,
  &route_sdcc_4_imem_list,
  0,
  0
};
static ul_treeNodeType route_0x240000001eL_sdcc_4_qdss_stm_node =
{
  0x240000001eULL,
  &route_sdcc_4_qdss_stm_list,
  &route_0x240000001aL_sdcc_4_imem_node,
  &route_0x240000002fL_sdcc_4_clk_ctl_node
};
static ul_treeNodeType route_0x240000002fL_sdcc_4_clk_ctl_node =
{
  0x240000002fULL,
  &route_sdcc_4_clk_ctl_list,
  0,
  0
};
static ul_treeNodeType route_0x2400000032L_sdcc_4_tcsr_node =
{
  0x2400000032ULL,
  &route_sdcc_4_tcsr_list,
  &route_0x230000004aL_sdcc_2_rpm_node,
  &route_0x24000000a0L_sdcc_4_hmss_l3_node
};
static ul_treeNodeType route_0x2400000033L_sdcc_4_tlmm_node =
{
  0x2400000033ULL,
  &route_sdcc_4_tlmm_list,
  0,
  &route_0x2400000037L_sdcc_4_message_ram_node
};
static ul_treeNodeType route_0x2400000037L_sdcc_4_message_ram_node =
{
  0x2400000037ULL,
  &route_sdcc_4_message_ram_list,
  0,
  0
};
static ul_treeNodeType route_0x240000003bL_sdcc_4_pmic_arb_node =
{
  0x240000003bULL,
  &route_sdcc_4_pmic_arb_list,
  &route_0x2400000033L_sdcc_4_tlmm_node,
  &route_0x240000004aL_sdcc_4_rpm_node
};
static ul_treeNodeType route_0x240000004aL_sdcc_4_rpm_node =
{
  0x240000004aULL,
  &route_sdcc_4_rpm_list,
  0,
  0
};
static ul_treeNodeType route_0x24000000a0L_sdcc_4_hmss_l3_node =
{
  0x24000000a0ULL,
  &route_sdcc_4_hmss_l3_list,
  &route_0x240000003bL_sdcc_4_pmic_arb_node,
  &route_0x2500000000L_tsif_ebi_node
};
static ul_treeNodeType route_0x24000000a6L_sdcc_4_pimem_node =
{
  0x24000000a6ULL,
  &route_sdcc_4_pimem_list,
  0,
  0
};
static ul_treeNodeType route_0x2500000000L_tsif_ebi_node =
{
  0x2500000000ULL,
  &route_tsif_ebi_list,
  &route_0x24000000a6L_sdcc_4_pimem_node,
  &route_0x250000001eL_tsif_qdss_stm_node
};
static ul_treeNodeType route_0x250000001aL_tsif_imem_node =
{
  0x250000001aULL,
  &route_tsif_imem_list,
  0,
  0
};
static ul_treeNodeType route_0x250000001eL_tsif_qdss_stm_node =
{
  0x250000001eULL,
  &route_tsif_qdss_stm_list,
  &route_0x250000001aL_tsif_imem_node,
  &route_0x250000002fL_tsif_clk_ctl_node
};
static ul_treeNodeType route_0x250000002fL_tsif_clk_ctl_node =
{
  0x250000002fULL,
  &route_tsif_clk_ctl_list,
  0,
  0
};
static ul_treeNodeType route_0x2500000032L_tsif_tcsr_node =
{
  0x2500000032ULL,
  &route_tsif_tcsr_list,
  &route_0x2400000032L_sdcc_4_tcsr_node,
  &route_0x2700000032L_blsp_2_tcsr_node
};
static ul_treeNodeType route_0x2500000033L_tsif_tlmm_node =
{
  0x2500000033ULL,
  &route_tsif_tlmm_list,
  0,
  0
};
static ul_treeNodeType route_0x2500000037L_tsif_message_ram_node =
{
  0x2500000037ULL,
  &route_tsif_message_ram_list,
  &route_0x2500000033L_tsif_tlmm_node,
  0
};
static ul_treeNodeType route_0x250000003bL_tsif_pmic_arb_node =
{
  0x250000003bULL,
  &route_tsif_pmic_arb_list,
  &route_0x2500000037L_tsif_message_ram_node,
  &route_0x25000000a0L_tsif_hmss_l3_node
};
static ul_treeNodeType route_0x250000004aL_tsif_rpm_node =
{
  0x250000004aULL,
  &route_tsif_rpm_list,
  0,
  0
};
static ul_treeNodeType route_0x25000000a0L_tsif_hmss_l3_node =
{
  0x25000000a0ULL,
  &route_tsif_hmss_l3_list,
  &route_0x250000004aL_tsif_rpm_node,
  &route_0x25000000a6L_tsif_pimem_node
};
static ul_treeNodeType route_0x25000000a6L_tsif_pimem_node =
{
  0x25000000a6ULL,
  &route_tsif_pimem_list,
  0,
  0
};
static ul_treeNodeType route_0x2700000000L_blsp_2_ebi_node =
{
  0x2700000000ULL,
  &route_blsp_2_ebi_list,
  &route_0x250000003bL_tsif_pmic_arb_node,
  &route_0x270000001eL_blsp_2_qdss_stm_node
};
static ul_treeNodeType route_0x270000001aL_blsp_2_imem_node =
{
  0x270000001aULL,
  &route_blsp_2_imem_list,
  0,
  0
};
static ul_treeNodeType route_0x270000001eL_blsp_2_qdss_stm_node =
{
  0x270000001eULL,
  &route_blsp_2_qdss_stm_list,
  &route_0x270000001aL_blsp_2_imem_node,
  &route_0x270000002fL_blsp_2_clk_ctl_node
};
static ul_treeNodeType route_0x270000002fL_blsp_2_clk_ctl_node =
{
  0x270000002fULL,
  &route_blsp_2_clk_ctl_list,
  0,
  0
};
static ul_treeNodeType route_0x2700000032L_blsp_2_tcsr_node =
{
  0x2700000032ULL,
  &route_blsp_2_tcsr_list,
  &route_0x2700000000L_blsp_2_ebi_node,
  &route_0x270000004aL_blsp_2_rpm_node
};
static ul_treeNodeType route_0x2700000033L_blsp_2_tlmm_node =
{
  0x2700000033ULL,
  &route_blsp_2_tlmm_list,
  0,
  0
};
static ul_treeNodeType route_0x2700000037L_blsp_2_message_ram_node =
{
  0x2700000037ULL,
  &route_blsp_2_message_ram_list,
  &route_0x2700000033L_blsp_2_tlmm_node,
  &route_0x270000003bL_blsp_2_pmic_arb_node
};
static ul_treeNodeType route_0x270000003bL_blsp_2_pmic_arb_node =
{
  0x270000003bULL,
  &route_blsp_2_pmic_arb_list,
  0,
  0
};
static ul_treeNodeType route_0x270000004aL_blsp_2_rpm_node =
{
  0x270000004aULL,
  &route_blsp_2_rpm_list,
  &route_0x2700000037L_blsp_2_message_ram_node,
  &route_0x27000000a0L_blsp_2_hmss_l3_node
};
static ul_treeNodeType route_0x27000000a0L_blsp_2_hmss_l3_node =
{
  0x27000000a0ULL,
  &route_blsp_2_hmss_l3_list,
  0,
  &route_0x27000000a6L_blsp_2_pimem_node
};
static ul_treeNodeType route_0x27000000a6L_blsp_2_pimem_node =
{
  0x27000000a6ULL,
  &route_blsp_2_pimem_list,
  0,
  0
};
static ul_treeNodeType route_0x2900000000L_blsp_1_ebi_node =
{
  0x2900000000ULL,
  &route_blsp_1_ebi_list,
  &route_0x2500000032L_tsif_tcsr_node,
  &route_0x2a00000000L_usb_hs_ebi_node
};
static ul_treeNodeType route_0x290000001aL_blsp_1_imem_node =
{
  0x290000001aULL,
  &route_blsp_1_imem_list,
  0,
  &route_0x290000001eL_blsp_1_qdss_stm_node
};
static ul_treeNodeType route_0x290000001eL_blsp_1_qdss_stm_node =
{
  0x290000001eULL,
  &route_blsp_1_qdss_stm_list,
  0,
  0
};
static ul_treeNodeType route_0x290000002fL_blsp_1_clk_ctl_node =
{
  0x290000002fULL,
  &route_blsp_1_clk_ctl_list,
  &route_0x290000001aL_blsp_1_imem_node,
  &route_0x2900000032L_blsp_1_tcsr_node
};
static ul_treeNodeType route_0x2900000032L_blsp_1_tcsr_node =
{
  0x2900000032ULL,
  &route_blsp_1_tcsr_list,
  0,
  &route_0x2900000033L_blsp_1_tlmm_node
};
static ul_treeNodeType route_0x2900000033L_blsp_1_tlmm_node =
{
  0x2900000033ULL,
  &route_blsp_1_tlmm_list,
  0,
  0
};
static ul_treeNodeType route_0x2900000037L_blsp_1_message_ram_node =
{
  0x2900000037ULL,
  &route_blsp_1_message_ram_list,
  &route_0x290000002fL_blsp_1_clk_ctl_node,
  &route_0x290000004aL_blsp_1_rpm_node
};
static ul_treeNodeType route_0x290000003bL_blsp_1_pmic_arb_node =
{
  0x290000003bULL,
  &route_blsp_1_pmic_arb_list,
  0,
  0
};
static ul_treeNodeType route_0x290000004aL_blsp_1_rpm_node =
{
  0x290000004aULL,
  &route_blsp_1_rpm_list,
  &route_0x290000003bL_blsp_1_pmic_arb_node,
  &route_0x29000000a0L_blsp_1_hmss_l3_node
};
static ul_treeNodeType route_0x29000000a0L_blsp_1_hmss_l3_node =
{
  0x29000000a0ULL,
  &route_blsp_1_hmss_l3_list,
  0,
  &route_0x29000000a6L_blsp_1_pimem_node
};
static ul_treeNodeType route_0x29000000a6L_blsp_1_pimem_node =
{
  0x29000000a6ULL,
  &route_blsp_1_pimem_list,
  0,
  0
};
static ul_treeNodeType route_0x2a00000000L_usb_hs_ebi_node =
{
  0x2a00000000ULL,
  &route_usb_hs_ebi_list,
  &route_0x2900000037L_blsp_1_message_ram_node,
  &route_0x2a00000033L_usb_hs_tlmm_node
};
static ul_treeNodeType route_0x2a0000001aL_usb_hs_imem_node =
{
  0x2a0000001aULL,
  &route_usb_hs_imem_list,
  0,
  &route_0x2a0000001eL_usb_hs_qdss_stm_node
};
static ul_treeNodeType route_0x2a0000001eL_usb_hs_qdss_stm_node =
{
  0x2a0000001eULL,
  &route_usb_hs_qdss_stm_list,
  0,
  0
};
static ul_treeNodeType route_0x2a0000002fL_usb_hs_clk_ctl_node =
{
  0x2a0000002fULL,
  &route_usb_hs_clk_ctl_list,
  &route_0x2a0000001aL_usb_hs_imem_node,
  &route_0x2a00000032L_usb_hs_tcsr_node
};
static ul_treeNodeType route_0x2a00000032L_usb_hs_tcsr_node =
{
  0x2a00000032ULL,
  &route_usb_hs_tcsr_list,
  0,
  0
};
static ul_treeNodeType route_0x2a00000033L_usb_hs_tlmm_node =
{
  0x2a00000033ULL,
  &route_usb_hs_tlmm_list,
  &route_0x2a0000002fL_usb_hs_clk_ctl_node,
  &route_0x2a0000004aL_usb_hs_rpm_node
};
static ul_treeNodeType route_0x2a00000037L_usb_hs_message_ram_node =
{
  0x2a00000037ULL,
  &route_usb_hs_message_ram_list,
  0,
  0
};
static ul_treeNodeType route_0x2a0000003bL_usb_hs_pmic_arb_node =
{
  0x2a0000003bULL,
  &route_usb_hs_pmic_arb_list,
  &route_0x2a00000037L_usb_hs_message_ram_node,
  0
};
static ul_treeNodeType route_0x2a0000004aL_usb_hs_rpm_node =
{
  0x2a0000004aULL,
  &route_usb_hs_rpm_list,
  &route_0x2a0000003bL_usb_hs_pmic_arb_node,
  &route_0x2a000000a6L_usb_hs_pimem_node
};
static ul_treeNodeType route_0x2a000000a0L_usb_hs_hmss_l3_node =
{
  0x2a000000a0ULL,
  &route_usb_hs_hmss_l3_list,
  0,
  0
};
static ul_treeNodeType route_0x2a000000a6L_usb_hs_pimem_node =
{
  0x2a000000a6ULL,
  &route_usb_hs_pimem_list,
  &route_0x2a000000a0L_usb_hs_hmss_l3_node,
  0
};
ul_treeNodeType * route_tree_root =
  &route_0x2300000033L_sdcc_2_tlmm_node;
const uint32 u32ul_arb_master_list_size = 16;
ul_master_data_type *ul_arb_master_list[] =
{
  &master_a1noc_snoc,
  &master_blsp_1,
  &master_blsp_2,
  &master_cr_virt_a1noc,
  &master_crypto_c0,
  &master_lpass_ahb,
  &master_lpass_proc,
  &master_pnoc_a1noc,
  &master_sdcc_1,
  &master_sdcc_2,
  &master_sdcc_4,
  &master_snoc_bimc,
  &master_snoc_cnoc,
  &master_snoc_pnoc,
  &master_tsif,
  &master_usb_hs,
};
const uint32 u32ul_arb_slave_list_size = 25;
ul_slave_data_type *ul_arb_slave_list[] =
{
  &slave_a1noc_snoc,
  &slave_blsp_1,
  &slave_blsp_2,
  &slave_clk_ctl,
  &slave_cr_virt_a1noc,
  &slave_ebi,
  &slave_hmss_l3,
  &slave_imem,
  &slave_message_ram,
  &slave_pdm,
  &slave_pimem,
  &slave_pmic_arb,
  &slave_pnoc_a1noc,
  &slave_qdss_stm,
  &slave_rpm,
  &slave_sdcc_1,
  &slave_sdcc_2,
  &slave_sdcc_4,
  &slave_snoc_bimc,
  &slave_snoc_cnoc,
  &slave_snoc_pnoc,
  &slave_tcsr,
  &slave_tlmm,
  &slave_tsif,
  &slave_usb_hs,
};
const uint32 u32ul_arb_clk_node_list_size = 5;
const char *ul_arb_clock_node_list[] =
{
  "/clk/agr1",
  "/clk/bimc",
  "/clk/cnoc",
  "/clk/pnoc",
  "/clk/snoc",
};
