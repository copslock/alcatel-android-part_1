typedef unsigned char boolean;
typedef unsigned long int uint32;
typedef unsigned short uint16;
typedef unsigned char uint8;
typedef signed long int int32;
typedef signed short int16;
typedef signed char int8;
typedef unsigned char byte;
typedef unsigned short word;
typedef unsigned long dword;
typedef unsigned char uint1;
typedef unsigned short uint2;
typedef unsigned long uint4;
typedef signed char int1;
typedef signed short int2;
typedef long int int4;
typedef signed long sint31;
typedef signed short sint15;
typedef signed char sint7;
typedef uint16 UWord16 ;
typedef uint32 UWord32 ;
typedef int32 Word32 ;
typedef int16 Word16 ;
typedef uint8 UWord8 ;
typedef int8 Word8 ;
typedef int32 Vect32 ;
      typedef long long int64;
      typedef unsigned long long uint64;
typedef struct
{
    char* name;
    uint8 slave_id;
    void* base_addr;
    char* clk_name;
    uint8 slave_enable;
    uint8 intr_enable;
    uint8 timeout_val;
}ABT_slave_info_type;
typedef struct
{
    char* name;
    void** intr_map_addr;
    void** intr_status_addr;
    void* globa_en_addr;
    uint32 intr_vector;
    uint32 intr_priority;
    uint32 num_status_reg;
}ABT_platform_info_type;
typedef struct
{
    uint8 len;
    ABT_slave_info_type* slave_info;
    ABT_platform_info_type *platform_info;
}ABT_propdata_type;
typedef enum
{
  ABT_SUCCESS = 0,
  ABT_ERROR_INVALID_PARAM,
  ABT_ERROR_INIT_FAILURE,
  ABT_ERROR_SIZE = 0x7FFFFFFF,
  ABT_ERROR = -1
}ABT_error_type;
void ABT_Init(void);
static ABT_slave_info_type ABT_cfgdata[] =
{
  { "LPASS0", 0x15, (void*)(0xee000000 + 0x000b7000), 0, 1, 1, 0xFF, },
  { "LPASS1", 0x16, (void*)(0xee000000 + 0x000f6000), "audio_core_core_clk", 1, 1, 0xFF, }
};
static void *intrEnable[] =
{
  (void*)((0xe0900000 + 0x000a0000) + 0x00008050),
};
static void *intrStatus[] =
{
  (void*)((0xe0900000 + 0x000a0000) + 0x00008020),
};
static ABT_platform_info_type ABT_platform_info =
{
    "LPASS",
    intrEnable,
    intrStatus,
    (void*)((0xe0900000 + 0x000a0000) + 0x00000800),
    90,
    1,
    sizeof(intrEnable)/sizeof(intrEnable[0]),
};
ABT_propdata_type ABT_propdata =
{
    sizeof(ABT_cfgdata)/sizeof(ABT_slave_info_type),
    ABT_cfgdata,
    &ABT_platform_info
};
