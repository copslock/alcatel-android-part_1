/*=============================================================================

  FILE:   I2cPlatSvc.c

  OVERVIEW: This file contains the implementation for the platform services.
 
          Copyright (c) 2011 - 2014 Qualcomm Technologies Incorporated.
          All Rights Reserved.
          Qualcomm Confidential and Proprietary 

=============================================================================*/
/*=============================================================================
EDIT HISTORY FOR MODULE

$Header: //components/rel/core.adsp/2.7/buses/i2c/src/drv/8996/I2cPlatSvc.c#1 $
$DateTime: 2014/11/08 00:38:01 $$Author: pwbldsvc $
When     Who    What, where, why
-------- ---    -----------------------------------------------------------
07/17/13 LK     Added xml properties.
09/26/11 LK     Created
=============================================================================*/

/*-------------------------------------------------------------------------
 * Include Files
 * ----------------------------------------------------------------------*/

#include "I2cPlatSvc.h"
#include "I2cSys.h"
#include "I2cError.h"

#include "DALStdDef.h"
#include "DALDeviceId.h"
#include "DALSys.h"
#include "DDIClock.h"
#include "DDIHWIO.h"

#include "DDIInterruptController.h"

#include "DDITlmm.h"

/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * -------------------------------------------------------------------------*/
#define QUP_FREQ_HZ                            19200000

#define I2C_DEVICE_PLATBAM_MAX_BAM_THRESHOLD (32*1024)
#define I2C_DEVICE_PLATBAM_MAX_DESC_SIZE     0x100
#define I2C_DEVICE_PLATBAM_MAX_COMMAND_SIZE  0x100

#define I2C_ICB_CLIENT_CNT          1
#define I2C_PNOC_MSTRSLV_PAIRS_NUM	1
/*-------------------------------------------------------------------------
 * Static Variable Definitions
 * ----------------------------------------------------------------------*/
 
/*-------------------------------------------------------------------------
 * Static Function Declarations and Definitions
 * ----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
 * Externalized Function Definitions
 * ----------------------------------------------------------------------*/

/** @brief Enables the I2C clocks.
   
    @param[in] pPlat platform device pointer.
    
    @return             I2C_RES_SUCCESS is successful, otherwise
                        I2cPlat_Error.
  */
int32 I2cPlat_VoteI2cClkOn
(
   I2cPlat_DescType *pPlat
)
{
   DALResult dalRes;

   // Get Clock handle
   if (pPlat->props.pClkHandle == NULL) {
      dalRes = DAL_ClockDeviceAttach(DALDEVICEID_CLOCK, (DalDeviceHandle**)&pPlat->props.pClkHandle);
      if ( (DAL_SUCCESS != dalRes) || (NULL == pPlat->props.pClkHandle) ) {
         return I2CPLAT_ERROR_FAILED_TO_ATTACH_TO_CLOCKS;
      }
   }
	  
   // Init QUP Clock
   dalRes = DalClock_GetClockId((DalDeviceHandle*)pPlat->props.pClkHandle,
	                               pPlat->props.appsClkIdName, 
								   &pPlat->props.QupAppClkId);
   if (DAL_SUCCESS != dalRes)
   {
      return I2CPLAT_ERROR_GETTING_CLK_APPS;
   }

   // Init HCLK
   dalRes = DalClock_GetClockId((DalDeviceHandle*)pPlat->props.pClkHandle,
                                 pPlat->props.hClkIdName,
                                 &pPlat->props.QupHClkId);
   if (DAL_SUCCESS != dalRes)
   {
      return I2CPLAT_ERROR_GETTING_CLK_AHB;
   }

   // Enable QUP Clock
   dalRes = DalClock_EnableClock((DalDeviceHandle*)pPlat->props.pClkHandle,
	                                pPlat->props.QupAppClkId);
   if (DAL_SUCCESS != dalRes)
   {
      return I2CPLAT_ERROR_FAILED_TO_ENABLE_APPS_CLK;
   }

   // Enable HCLK
   dalRes = DalClock_EnableClock((DalDeviceHandle*)pPlat->props.pClkHandle,
	                                pPlat->props.QupHClkId);
   if (DAL_SUCCESS != dalRes)
   {
      return I2CPLAT_ERROR_FAILED_TO_ENABLE_HCLK;
   }
     
   return I2C_RES_SUCCESS;
}

/** @brief Disables the I2C clocks.
  
    @param[in] pPlat platform device pointer.
    
    @return             I2C_RES_SUCCESS is successful, otherwise
                        I2cPlat_Error.
  */
int32 I2cPlat_VoteI2cClkOff
(
   I2cPlat_DescType      *pPlat
)
{
   DALResult dalRes;

   // Disable QUP Clock
   dalRes = DalClock_DisableClock((DalDeviceHandle*)pPlat->props.pClkHandle,
                                  pPlat->props.QupAppClkId);
   if ( DAL_SUCCESS != dalRes ) {
      return I2CPLAT_ERROR_FAILED_TO_DISABLE_APPS_CLK;
   }

   // Disable HCLK
   dalRes = DalClock_DisableClock((DalDeviceHandle*)pPlat->props.pClkHandle,
	                                 pPlat->props.QupHClkId);
   if ( DAL_SUCCESS != dalRes ) {
      return I2CPLAT_ERROR_FAILED_TO_DISABLE_HCLK;
   }
   
   // DeInit Clock
   /*if ( pDev->initState & I2CPLAT_TGT_INIT_CLK_ALLOCATED ) {
      dalRes = DAL_DeviceDetach((DalDeviceHandle*)pPlat->props.pClkHandle);
      if ( DAL_SUCCESS != dalRes ) {
         res = I2CPLAT_ERROR_FAILED_TO_DETACH_FROM_CLOCKS;
         break;
      }
   }*/
   
   return I2C_RES_SUCCESS;
}



