<driver name="SlimBus">
  <global_def>
    <var_seq name="ee_assign_arr" type=DALPROP_DATA_TYPE_BYTE_SEQ>
      0x00, 0x01, 0x02, end
    </var_seq>
    <var_seq name="tlmm_name_str" type=DALPROP_DATA_TYPE_STRING>TLMM</var_seq>
    <var_seq name="svs_npa_str" type=DALPROP_DATA_TYPE_STRING>/pmic/client/rail_cx</var_seq>
  </global_def>
  <device id=DALDEVICEID_SLIMBUS_1>
    <props name="bsp_data" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
       SlimBusBSP
    </props>
    <props name="is_master" type=DALPROP_ATTR_TYPE_UINT32>1</props>
    <props name="default_clock_gear" type=DALPROP_ATTR_TYPE_UINT32>8</props>
    <props name="ee_assign" type=DALPROP_ATTR_TYPE_BYTE_SEQ_PTR>
      ee_assign_arr
    </props>
    <props name="secondary_dl_thresh" type=DALPROP_ATTR_TYPE_UINT32>32</props>
    <props name="device_props" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
      sbDeviceProps
    </props>
    <props name="num_device_props" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
      sbNumDeviceProps
    </props>
    <props name="mmpm_reg_param" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
      sbMmpmRegParam
    </props>
    <props name="prog_bam_trust" type=DALPROP_ATTR_TYPE_UINT32>0</props>
    <props name="tlmm_name" type=DALPROP_ATTR_TYPE_STRING_PTR>tlmm_name_str</props>
    <props name="tlmm_offset" type=DALPROP_ATTR_TYPE_UINT32>0x144000</props>
    <props name="tlmm_val" type=DALPROP_ATTR_TYPE_UINT32>0x2</props>
    <props name="svs_npa" type=DALPROP_ATTR_TYPE_STRING_PTR>svs_npa_str</props>
    <props name="use_gpio_int" type=DALPROP_ATTR_TYPE_UINT32>0</props>
    <props name="log_level" type=DALPROP_ATTR_TYPE_UINT32>4</props>
    <props name="num_local_ports" type=DALPROP_ATTR_TYPE_UINT32>22</props>
    <props name="local_port_base" type=DALPROP_ATTR_TYPE_UINT32>0</props>
    <props name="local_channel_base" type=DALPROP_ATTR_TYPE_UINT32>1</props>
    <props name="shared_channel_base" type=DALPROP_ATTR_TYPE_UINT32>128</props>
    <props name="num_local_counters" type=DALPROP_ATTR_TYPE_UINT32>22</props>
  </device>
  <device id=DALDEVICEID_SLIMBUS_2>
    <props name="bsp_data" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
       SlimBusBSP2
    </props>
    <props name="is_master" type=DALPROP_ATTR_TYPE_UINT32>1</props>
    <props name="default_clock_gear" type=DALPROP_ATTR_TYPE_UINT32>8</props>
    <props name="ee_assign" type=DALPROP_ATTR_TYPE_BYTE_SEQ_PTR>
      ee_assign_arr
    </props>
    <props name="device_props" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
      sbDeviceProps2
    </props>
    <props name="num_device_props" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
      sbNumDeviceProps2
    </props>
    <props name="mmpm_reg_param" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
      sbMmpmRegParam2
    </props>
    <props name="prog_bam_trust" type=DALPROP_ATTR_TYPE_UINT32>0</props>
    <props name="tlmm_name" type=DALPROP_ATTR_TYPE_STRING_PTR>tlmm_name_str</props>
    <props name="tlmm_offset" type=DALPROP_ATTR_TYPE_UINT32>0x145000</props>
    <props name="tlmm_val" type=DALPROP_ATTR_TYPE_UINT32>0x2</props>
    <props name="svs_npa" type=DALPROP_ATTR_TYPE_STRING_PTR>svs_npa_str</props>
    <props name="use_gpio_int" type=DALPROP_ATTR_TYPE_UINT32>0</props>
    <props name="log_level" type=DALPROP_ATTR_TYPE_UINT32>4</props>
    <props name="num_local_ports" type=DALPROP_ATTR_TYPE_UINT32>10</props>
    <props name="local_port_base" type=DALPROP_ATTR_TYPE_UINT32>0</props>
    <props name="local_channel_base" type=DALPROP_ATTR_TYPE_UINT32>1</props>
    <props name="shared_channel_base" type=DALPROP_ATTR_TYPE_UINT32>128</props>
    <props name="num_local_counters" type=DALPROP_ATTR_TYPE_UINT32>10</props>
  </device>
</driver>
