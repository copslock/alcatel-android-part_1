<!--
  Copyright (c) 2013 Qualcomm Technologies Incorporated.
           All Rights Reserved.
   DESCRIPTION: LPM Device Configuration.
   $Header: $
 -->
<!-- NULL Driver does not require Dal Driver Interface APIs, since none of AVS HWD drivers uses DAL Device framework -->
<driver name="NULL">
   <!-- LPA -->
   <device id="LPA">
      <props name="LPAPropStructPtr" type=DALPROP_ATTR_TYPE_STRUCT_PTR>lpa_prop</props>
   </device>
</driver>
