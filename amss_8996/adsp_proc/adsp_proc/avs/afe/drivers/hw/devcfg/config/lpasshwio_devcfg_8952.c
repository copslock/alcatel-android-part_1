/*==============================================================================
$Header: //components/rel/avs.adsp/2.7.1.c4/afe/drivers/hw/devcfg/config/lpasshwio_devcfg_8952.c#2 $
$DateTime: 2016/06/10 11:57:20 $
$Author: pwbldsvc $
$Change: 10672791 $
$Revision: #2 $

FILE: lpasshwio_devcfg_8952.c 

DESCRIPTION: 8952 Device Config

Copyright 2015 QUALCOMM Technologies, Inc. (QTI).
All Rights Reserved.
QUALCOMM Proprietary/GTDR
==============================================================================*/

/*=====================================================================
 Includes
 ======================================================================*/
#include "lpasshwio_devcfg.h"

/**< LPASS_HW_VERSION register base address */
HwdLpassPropertyType lpass_prop ={0x0C050000};

/**< LPAIF Configuration */
lpasshwio_prop_lpaif_struct_t lpaif_prop = 
{
   0xC0C0000,    /**< LPAIF reg base addr: LPASS_LPAIF_VERSION */
   (128*1024),   /**< LPAIF overal reg page size */

   /**< I2S Device Config Start */ 
   {
      0xC0C4000,   /**< LPASS_LPAIF_I2S_CTL0, offset addr */
      6,           /**< Number of supported mi2s interfaces */
      {
         /**< List of supported mi2s interfaces */ 
         PRIMARY_MI2S,
         SECONDARY_MI2S,
         TERTIARY_MI2S,
         QUATERNARY_MI2S,
         QUINARY_MI2S,
         SENARY_MI2S
      },
      {
         /**< Max # of channel for each mi2s */
         4,  /**< Pri mi2s */
         4,  /**< Sec mi2s */
         4,  /**< Tert mi2s */
         4,  /**< Quat mi2s */
         8,  /**< Qui mi2s */
         2   /**< Sen mi2s */
      },
      {
         /**< Direction for each mi2s */
         DATA_LINE_SINK,     /**< Pri mi2s, spkr-only */
         DATA_LINE_SINK,     /**< Sec mi2s, spkr-only */
         DATA_LINE_SOURCE,   /**< Tert mi2s, mic-only */
         DATA_LINE_BIDIRECT, /**< Quat mi2s, bi-dir */
         DATA_LINE_BIDIRECT, /**< Qui mi2s, bi-dir */
         DATA_LINE_SOURCE      /**< Sen i2s, mic-only */
      },
      SUPPORTED   /**< Mi2s Interface is supported */
   },  /**< End of I2S Device Config */

   /**< PCM Device Config Start */
   {
      0xC0C0010,  /**< LPASS_LPAIF_PCM_CTL0, offset addr */
      2,          /**< Number of supported PCM interfaces */
      {
         /**< List of supported PCM interfaces */ 
         PRIMARY_PCM,
         SECONDARY_PCM,
         0, 
         0
      },
      {
         /**< Max # of channel for each PCM intf */ 
         8,  /**< Pri PCM */
         8,  /**< Sec PCM */
         0,  /**< Un-supported */
         0   /**< Un-supported */
      },
      {
         DATA_LINE_BIDIRECT, /**< Pri PCM, bi-dir */
         DATA_LINE_BIDIRECT, /**< Sec PCM, bi-dir */
         0,
         0
      },
      SUPPORTED   /**< PCM Interface is supported */
   }, /**< PCM Device Config end */

   /**< TDM Device Config Start */
   {
      0, /**< LPASS_LPAIF_PCM_CTL0, offset addr */
      0, /**< Number of supported TDM interfaces */
      {
         /**< List of supported TDM interfaces */ 
         0, 
         0, 
         0, 
         0
      }, 
      {
         /**< Max # of channel for each TDM intf */ 
         0,
         0,
         0,
         0
      },
      {
         0, 
         0, 
         0, 
         0
      }, 
    NOT_SUPPORTED
   }, /**< TDM Device Config end */

   /**< HDMI 1.4 Device Config */
   {
      0,               /**< HDMI reg base addr */
      0,               /**< Reg page size */
      NOT_SUPPORTED    /**< HDMI 1.4 is un-supported */
   },

   /**< I2S/PCM selection mux */
   {
      0,             /**< Mux reg offset addr */
      0,             /**< Number of selection mux */
      {0, 0, 0, 0},  /**< List of supported mux  */
      NOT_SUPPORTED  /**< Mux is un-supported */
   },

   /**< LPASS h/w revision */
   LPASS_HW_VER_3_10_0  
};

/**< AudioIF DMA Configuration */
lpasshwio_prop_afe_dma_struct_t audioif_dma_prop = 
{
   0xC0C0000,              /**< LPAIF reg base addr: LPASS_LPAIF_VERSION */
   (64*1024),              /**< LPAIF reg page size */
   0xC0CF000,              /**< DMA offset: LPASS_LPAIF_IRQ_EN0 : interrupt */
   0xC0D2000,              /**< DMA offset: LPASS_LPAIF_RDDMA_CTLa */
   0xC0D8000,              /**< DMA offset: LPASS_LPAIF_WRDMA_CTLa */
   0xC0D2028,              /**< STC rddma offset: LPASS_LPAIF_RDDMA_STC_LSB0 */
   0xC0D8028,              /**< STC wrdma offset: LPASS_LPAIF_WRDMA_STC_LSB0 */
   4,                      /**< Read DMA channel count */
   4,                      /**< Write DMA channel count */
   19,                     /**< DMA intr L2VIC # */
   2,                      /**< interrupt line # */
   LPASS_HW_VER_3_10_0     /**< LPASS hw revision */
};

/**< HDMI Out DMA Configuration */
lpasshwio_prop_afe_dma_struct_t hdmiout_dma_prop = 
{
   0,    /**<  hdmi output reg base addr */
   0,    /**<  hdmi output reg page size */
   0,    /**<  not requried */
   0,    /**<  not required */
   0,    /**<  not requried */
   0,    /**<  not required */
   0,    /**<  not required */
   0,    /**<  sink DMA channel count */
   0,    /**<  source DMA channel count */
   0,    /**<  l2vic id for hdmi interrupt */
   0,    /**<  there is no interrupt host */
   0     /**<  hdmiout hw revision */
};

/**< AV TImer + Aud Sync Configuration */
HwdAvtimerPropertyType avtimer_prop = 
{
   0x0C0A3000,   /**< Reg base address: LPASS_LPASS_SYNC_WRAPPER */
   0x2000,       /**< Register page size */
   19200000,     /**< Root clock freq in Hz */
   0xC0A3000,    /**< AVT reg base addr: LPASS_AVTIMER_HW_VERSION  */
   0,            /**< ATIME1 reg base addr */
   0,            /**< ATIME2 reg base addr */
   0xC0A4000,    /**< AudSync reg base addr: LPASS_AUD_SYNC_CTL */
   50,           /**< AV Timer intr L2VIC # */
   0x30040000    /**< AV Timer h/w version */
};

/**< HW Resampler Configuration */
HwdResamplerPropertyType resampler_prop = 
{
   0,       /**< HW resampler reg base address */
   0,       /**< Reg page size */
   0,       /**< HW resampler intr irq # */
   0        /**< Resampler hw version */
};    

/**< VFR + Timestamp Configuration */
lpasshwio_vfr_prop_t vfr_prop = 
{
   0xC043000,       /**<  VFR reg base address  */
   0x9000,          /**<  VFR register page size */
   0xC047000,       /**<  LPASS_VFR_IRQ_MUX_CTL_1 */
   0xC049000,       /**<  LPASS_GP_IRQ_TS_MUX_CTL_0 */
   0xC04A000,       /**<  LPASS_GP_IRQ_TS_MUX_CTL_1 */
   0x10000000,      /**<  VFR hw revision */
   0x1,             /**<  VFR hw latching version */
   0x2,             /**<  Number of VFR timestamp mux */
   2,               /**<  Number of General Purpose timestamp mux */
   {
      /**<  L2VIC # for each VFR interrupt */
      51,   /**< VFR_0 intr L2VIC # */
      124   /**< VFR_1 intr L2VIC # */
   },
   {
      /** Mux input index for each VFR source */
      {0x0, 0x1},     /**<  vfr_src_prop[0]: {mux_ctl_in, hw_supported} */
      {0x7, 0x1}      /**<  vfr_src_prop[1]: {mux_ctl_in, hw_supported} */
   }
};

/**< BT-FM (WCNSS Interface) Configuration */
lpasshwio_prop_riva_struct_t afe_riva_prop =
{
   1,                   /**< H/w revision */
   0xA206008,           /**< CCU MB0 physical address */
   0xC285000,           /**< Q6SS IPC register physical address */
   16,                  /**< Q6ss IPC acknowledge interrupt bit position */
   0x20,                /**< Mail box offset form CCU MB0 */
   17,                  /**< Internal BT Tx L2VIC# */
   10,                  /**< Internal BT Rx L2VIC# */
   16,                  /**< Internal FM Tx/Rx L2VIC# */
   15,                  /**< Internal BTFM IPC L2VIC# */
    1                   /**< AVTIMER HW latching support */
};

/**< SLIMbus Configuration */
lpasshwio_prop_slimbus_struct_t afe_slimbus_prop = 
{
  1,   /**<  hw revision, 0-not supported  */
  1    /**<  Indicates if hw latching support for AV-timer present */
}; 

/**< HDMI Out Configuration */
lpasshwio_prop_hdmi_output_struct_t hdmi_output_prop = 
{
   0x0,  /**<  hdmi output reg base addr */
   0x0,  /**<  hdmi output reg page size */
   0x0,  /**<  hdmi output reset reg base addr */
   0x0,  /**<  hdmi output reset reg page size */
   0x0,  /**<  hdmi core output reg base addr */
   0x0,  /**<  hdmi core output reg page size */
   0x0   /**<  hdmi output hw version */
}; 

/**< LPM Configuration */
lpm_prop_struct_t lpm_prop = 
{
   0xC0E0000,  /**< Starting physical address, 64-bit */
   32*1024,    /**< Size of LPM in bytes */
   32*1024,    /**< Size allocated for AFE DMA buffers, in bytes */
   0,          /**< Size allocated for AFE working buffer, in bytes */
   QURT_MEM_CACHE_WRITEBACK_NONL2CACHEABLE,  /**< Cache attribute: L1 writeback L2 uncached */
   2 /**< AHB bus vote scaling factor to reach LPM. 1 - if uncached or cached with writethrough 
                                                    2 - if cached and writeback */
};

HwdGenericClkPropertyType genericclk_prop =
{
   0xC050000,              /**< TCSR reg base address */
   (64*1024),              /**< TCSR reg size in bytes */
   LPASS_HW_VER_3_10_0,    /**< hw revision */
   20,                     /**< Number of clk-id's defined below   */

   /**< List of clock ids */
   {
      /**< Mi2s interface bit/osr clocks */ 
      0x100, 0x101,  /**< Pri mi2s IBIT / EBIT clock */
      0x102, 0x103,  /**< Sec mi2s IBIT / EBIT clock */
      0x104, 0x105,  /**< Ter mi2s IBIT / EBIT clock */
      0x106, 0x107,  /**< Qua mi2s IBIT / EBIT clock */
      0x10B, 0x10C,  /**< Qui mi2s IBIT / EBIT clock */
      0x10D, 0x10E,  /**< Sen mi2s IBIT / EBIT clock */

      /**< PCM interface bit clocks */ 
      0x200, 0x201,  /**< Pri PCM IBIT / EBIT clock */
      0x202, 0x203,  /**< Sec PCM IBIT / EBIT clock */

      /**< mclk / dig codec clock */ 
      0x300,   /**< mclk1 */
      0x301,   /**< mclk2 */
      0x303,   /**< Internal dig codec core clock */

      /**< PCM OE clk-id (internal definition) */
      0x20000
   },

   4, /**< Number of PCM clock defined with OE set availability  */

   /**< List of PCM Clk Id that supported OE clk  */
   {0x200, 0x201, 0x202, 0x203},

   /**< Clk Id to use to enable individual PCM OE  */
   {0x20000,  0x20000,  0x20000,  0x20000},

   /**< Frequency (Hz) of PCM OE clock */
   {61440000, 61440000, 61440000, 61440000},
};

HwdMidiPropertyType midi_prop = {0,0, 0, 0, 0,0 };

lpasshwio_prop_spdif_struct_t spdiftx_prop = {0, 0, 0, 0, 0, 0, 0};
