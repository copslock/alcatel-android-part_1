/*========================================================================
   This file contains AFE Error handler implementation

  Copyright (c) 2015 Qualcomm Technologies, Inc.  All Rights Reserved.
  QUALCOMM Proprietary.  Export of this technology or software is regulated
  by the U.S. Government, Diversion contrary to U.S. law prohibited.
 
  $Header: 
 ====================================================================== */

#ifndef _AFE_ERR_HNDLR_I_H_
#define _AFE_ERR_HNDLR_I_H_

/*==========================================================================
  Include files
  ========================================================================== */

#include "qurt_elite.h"

/*----------------------------------------------------------------------------
 * Macro definition
 * -------------------------------------------------------------------------*/

/** Error handling types */
typedef enum afe_err_hndlr_type_t
{
   AFE_ERR_HNDL_NONE = 0,  /**< No Error Handling */
   AFE_DEBUG_CRASH,        /**< Force crash for worker thread signal miss */
   AFE_ERR_RECOVERY        /**< Recover device from permanent mute or stall due to signal miss */
} afe_err_hndlr_type_t;


#endif /* _AFE_ERR_HNDLR_I_H_ */
