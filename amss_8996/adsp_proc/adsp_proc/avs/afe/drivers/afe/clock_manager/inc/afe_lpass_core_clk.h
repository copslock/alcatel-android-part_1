/*========================================================================
  This file contains functions to set the parameters for different modules.

  Copyright (c) 2009-2014 Qualcomm Technologies Inc.  All Rights Reserved.
  QUALCOMM Proprietary.  Export of this technology or software is regulated
  by the U.S. Government, Diversion contrary to U.S. law prohibited.

  $Header: //components/rel/avs.adsp/2.7.1.c4/afe/drivers/afe/clock_manager/inc/afe_lpass_core_clk.h#1 $
 ====================================================================== */

#ifndef _AFE_LPASS_CORE_CLK_H_
#define _AFE_LPASS_CORE_CLK_H_

#include "Elite.h"

/*==========================================================================
  Include Files
========================================================================== */
#ifdef __cplusplus
extern "C" {
#endif //__cplusplus

ADSPResult afe_lpass_core_clk_init(uint32_t hw_version);

ADSPResult afe_lpass_core_clk_deinit(void);

ADSPResult afe_lpass_core_clk_set(int8_t *cfg_ptr, uint32_t payload_size);

#ifdef __cplusplus
}
#endif //__cplusplus

#endif

