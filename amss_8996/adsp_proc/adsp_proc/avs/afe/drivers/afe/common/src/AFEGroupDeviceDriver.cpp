/*==============================================================================
$Header: //components/rel/avs.adsp/2.7.1.c4/afe/drivers/afe/common/src/AFEGroupDeviceDriver.cpp#1 $
$DateTime: 2016/06/05 22:53:09 $
$Author: pwbldsvc $
$Change: 10625169 $
$Revision: #1 $

FILE:     AFEDeviceDriver.cpp

DESCRIPTION: Main Interface to the AFE Device driver

PUBLIC CLASSES:  Not Applicable

INITIALIZATION AND SEQUENCING REQUIREMENTS:  N/A

Copyright 2011 Qualcomm Technologies Incorporated.
All Rights Reserved.
QUALCOMM Proprietary/GTDR
==============================================================================*/
/*============================================================================
EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order. Please
use ISO format for dates.

$Header: //components/rel/avs.adsp/2.7.1.c4/afe/drivers/afe/common/src/AFEGroupDeviceDriver.cpp#1 $

when       who     what, where, why
--------   ---     -------------------------------------------------------
11/06/07    RA      Created file

============================================================================*/

/*=====================================================================
 Includes
 ======================================================================*/
#include "AFEGroupDeviceDriver.h"
#include "adsp_afe_service_commands.h"
#include "EliteMsg_AfeCustom.h"

#include "AFEHdmiOutputGroupDevice.h"

static afe_group_device_state_struct_t group_device_rx_secondary_mi2s;

bool_t afe_svc_validate_group_dev_id(uint16_t group_dev_id)
{
   bool_t      is_valid_group_id;

   /* check if the group device ID is supported one */
   switch (group_dev_id)
   {
      case AFE_GROUP_DEVICE_ID_SECONDARY_MI2S_RX:
      case AFE_GROUP_DEVICE_ID_HDMI_MULTISTREAM_RX:
         {
            is_valid_group_id = TRUE;
         }
         break;
      default:
         is_valid_group_id = FALSE;
         break;
   }

   return is_valid_group_id;
}

static inline bool_t afe_svc_validate_group_member_port_id(uint16_t group_dev_id, uint16_t member_port_id)
{
   bool_t      is_valid_port_id = FALSE;

   /* Check if the member port ID is supported for this group */
   switch (group_dev_id)
   {
      case AFE_GROUP_DEVICE_ID_SECONDARY_MI2S_RX:
         {
            switch (member_port_id)
            {
               case AFE_PORT_ID_SECONDARY_MI2S_RX:
               case AFE_PORT_ID_SECONDARY_MI2S_RX_2:
                  {
                     is_valid_port_id = TRUE;
                     break;
                  }
               default:
                  {
                     is_valid_port_id = FALSE;
                     break;
                  }
            } /* End of member port id switch */
         }
         break;
      default:
         break;
   } /* End of group ID switch */

   return is_valid_port_id;
}

ADSPResult afe_svc_get_group_device_state_object(afe_group_device_state_struct_t **p_group_device,
                                                 uint16_t group_dev_id)
{
   ADSPResult result = ADSP_EOK;

   switch (group_dev_id)
   {
      case AFE_GROUP_DEVICE_ID_SECONDARY_MI2S_RX:
         {
            *p_group_device = &group_device_rx_secondary_mi2s;
         }
         break;
      default:
         result = ADSP_EFAILED;
         break;
   }

   return result;
}

ADSPResult afe_svc_configure_group_device (afe_param_id_group_device_cfg_t *p_group_dev_cfg)
{
   ADSPResult                             result = ADSP_EOK;
   uint16_t                               port_idx=0;
   afe_group_device_state_struct_t        *p_group_device;
   afe_dev_port_t                         *p_dev_port;

   /* Fetch the state struct object for this group ID */
   if( ADSP_EOK != (result = afe_svc_get_group_device_state_object(&p_group_device, p_group_dev_cfg->group_id) ) )
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to get group device object, group_id: 0x%x", p_group_dev_cfg->group_id);
      return ADSP_EFAILED;
   }

   /* Check if this group device is already in RUN state */
   if (AFE_GROUP_DEVICE_STATE_RUN == p_group_device->group_state)
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Group config failed, all member ports are not in stop state, group_id: 0x%x", p_group_dev_cfg->group_id);
      return ADSP_EFAILED;
   }

   /* Check if this group device is already enabled */
   if (TRUE == p_group_device->is_enabled)
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Group config failed, already enabled, group_id: 0x%x", p_group_dev_cfg->group_id);
      return ADSP_EFAILED;
   }

   /* Group is either in CONFIG or DISABLE state now, and command is sent to (re)configure the group */
   memset((void *)p_group_device, 0, sizeof(afe_group_device_state_struct_t));

   for (port_idx = 0; port_idx < AFE_GROUP_DEVICE_NUM_PORTS; port_idx++)
   {
      if (0 != p_group_dev_cfg->port_id[port_idx])
      {
         if ( ADSP_EOK != (afe_svc_get_port(p_group_dev_cfg->port_id[port_idx], &p_group_device->p_dev_port[port_idx])) )
         {
            MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Group Device failed to get the obj for port_id: 0x%x",
                 p_group_dev_cfg->port_id[port_idx]);

            return ADSP_EFAILED;
         }

         p_dev_port = p_group_device->p_dev_port[port_idx];

         /* Check if this port ID is valid member of this group */
         if ( FALSE == afe_svc_validate_group_member_port_id(p_group_dev_cfg->group_id, p_dev_port->intf) )
         {
            MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Group Device invalid member port_id: 0x%x, group_id 0x%x",
                     p_dev_port->intf, p_group_dev_cfg->group_id);
               return ADSP_EFAILED;
         }

         /* Update group data member pointer to point to this port */
         p_group_device->p_dev_port[port_idx] = p_dev_port;

         /* Clear the dev port pointer pointer to group device object
            This will be set when group device is enabled */
         p_dev_port->p_group_device_state = NULL;

         /* Both group and member points to same device driver object */
         p_group_device->p_dev_drv_state = p_dev_port->afe_drv;
      }
   }

   
   /* Num channels for this group */
   p_group_device->num_channels = p_group_dev_cfg->num_channel;

   /* Default bytes per sample initialized to 2 */
   p_group_device->bytes_per_sample = 2;

   /* Default samples per period initialized to 48 */
   p_group_device->int_samples_per_period = 48;

   /* Init the DMA signal for the group */
   if (ADSP_FAILED(result = qurt_elite_signal_init(&(p_group_device->dma_signal))))
   {
      MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to init signal for group device, group_dev_id: 0x%x, result: %d",
            p_group_dev_cfg->group_id, result);
      return result;
   }

   /* Init group state to stop */
   p_group_device->group_state = AFE_GROUP_DEVICE_STATE_STOP;

   /* Pointer to working buffer for the group init to NULL */
   p_group_device->p_output_buf = NULL;

   /* Update the group config state to CONFIG */
   p_group_device->is_config_rcvd = TRUE;
   
   return result;
}


ADSPResult afe_svc_set_group_device_module_params(uint32_t param_id,
                                                  int8_t *param_buffer_ptr,
                                                  uint32_t param_size)
{
   ADSPResult                          result = ADSP_EOK;
   uint32_t                            group_dev_cfg_version;
   afe_param_id_group_device_cfg_t     *p_group_dev_cfg;
   afe_group_device_state_struct_t     *p_group_device = NULL;
   uint16_t                            enable_flag;
   afe_param_id_group_device_enable_t  *p_group_device_cfg_enable;
   uint16_t                            port_idx = 0;
   afe_dev_port_t                      *p_dev_port = NULL;

   switch(param_id)
   {
      case AFE_PARAM_ID_GROUP_DEVICE_CONFIG:
         {
            /* Choose the appropriate config version */
            group_dev_cfg_version = (uint32_t) *(param_buffer_ptr);

            if(group_dev_cfg_version > AFE_API_VERSION_GROUP_DEVICE_CONFIG)
            {
               group_dev_cfg_version = AFE_API_VERSION_GROUP_DEVICE_CONFIG;
            }

            switch (group_dev_cfg_version)
            {
               case GROUP_DEVICE_CFG_V1:
                  {
                     /* Check for the valid param size */
                     if (param_size < sizeof(afe_param_id_group_device_cfg_t) )
                     {
                        MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Group Device Cfg enable/disable, invalid payload size: %lu", param_size);
                        return ADSP_EBADPARAM;
                     }

                     p_group_dev_cfg =  (afe_param_id_group_device_cfg_t *)param_buffer_ptr;

                     /* Validate group device ID */
                     if(FALSE == afe_svc_validate_group_dev_id(p_group_dev_cfg->group_id))
                     {
                        MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Group Device Cfg invalid group_dev_id: 0x%x", p_group_dev_cfg->group_id);
                           return ADSP_EBADPARAM;
                     }
                    
                     if ( ADSP_EOK != (result = afe_svc_configure_group_device(p_group_dev_cfg)) )
                     {
                        MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to set group device config, group_dev_id: 0x%x, result: %d",
                              p_group_dev_cfg->group_id, result);
                        return result;
                     }

                     MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Group device config success, group_dev_id: 0x%x", p_group_dev_cfg->group_id);
                  }
                  break;
               default:
                  {
                     MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Un-supported group device config ver: %lu, param_id: 0x%lx",
                           group_dev_cfg_version, param_id);
                  }
                  break;
            } /* End of group_dev_cfg_version switch case*/

         } /*End of AFE_PARAM_ID_GROUP_DEVICE_CONFIG */
         break;
      case AFE_PARAM_ID_GROUP_DEVICE_ENABLE:
         {
            /* Check if payload size is valid */
            if (param_size < sizeof(afe_param_id_group_device_enable_t) )
            {
               MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Group Device enable/disable failed, invalid payload size: %lu", param_size);
               return ADSP_EBADPARAM;
            }

            p_group_device_cfg_enable =  (afe_param_id_group_device_enable_t *)param_buffer_ptr;

            if(AFE_GROUP_DEVICE_ID_HDMI_MULTISTREAM_RX == p_group_device_cfg_enable->group_id)
            {
               afe_hdmi_output_set_group_device_module_params(param_id, param_buffer_ptr, param_size);
            }
            else
            {
               /* Fetch the state struct object for this group ID */
               if( ADSP_EOK != (result = afe_svc_get_group_device_state_object(&p_group_device, p_group_device_cfg_enable->group_id) ) )
               {
                  MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to get group device object, group_dev_id: 0x%x", p_group_device_cfg_enable->group_id);
                  return ADSP_EFAILED;
               }

               /* Check if this group device is already in RUN state */
               if (AFE_GROUP_DEVICE_STATE_RUN == p_group_device->group_state)
               {
                  MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Group Device enable/disable failed, all member ports are not in stop state, group_id: 0x%x",
                        p_group_device_cfg_enable->group_id);
                  return ADSP_EFAILED;
               }
               
               enable_flag = p_group_device_cfg_enable->enable;

               /* If group is enabled before configuring the group device */
               if ( (FALSE == p_group_device->is_config_rcvd) && (TRUE == enable_flag) )
               {
                   MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Group Device enable failed, not configured, group_id: 0x%x",
                         p_group_device_cfg_enable->group_id);

                   return ADSP_EOK;
               }

               for (port_idx = 0; port_idx < AFE_GROUP_DEVICE_NUM_PORTS; port_idx++)
               {
                  if (NULL != (p_dev_port = p_group_device->p_dev_port[port_idx]))
                  {
                     /* If group disable, decouple the group object from the port */
                     if (FALSE == enable_flag)
                     {
                        p_dev_port->p_group_device_state = NULL;
                     }
                     else /* Group enable, restore the group coupling with port */
                     {
                        p_dev_port->p_group_device_state = p_group_device;
                     }
                  }
               }

               /* Set the group device enable / disable state */
               p_group_device->is_enabled = enable_flag;
              
               MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Group device enable/disable success, group_dev_id: 0x%x, enable: %u",
                     p_group_device_cfg_enable->group_id, enable_flag);
            }

         } /*End of AFE_PARAM_ID_ENABLE param */
         break;
      case AFE_PARAM_ID_GROUP_DEVICE_HDMI_MULTISTREAM_CONFIG:
         {
            result = afe_hdmi_output_set_group_device_module_params(param_id, param_buffer_ptr, param_size);

         }
         break;
      default:
         result = ADSP_EUNSUPPORTED;
         break;
   } /* End of param_id switch case */

   return result;
}

