/*========================================================================
   This file contains utilities used by multiple Afe Ports.

  Copyright (c) 2009-2012 Qualcomm Technologies, Incorporated.  All Rights Reserved.
  QUALCOMM Proprietary.  Export of this technology or software is regulated
  by the U.S. Government, Diversion contrary to U.S. law prohibited.
 
  $Header: //components/rel/avs.adsp/2.7.1.c4/afe/services/static/src/AFELoggingUtils.cpp#3 $
 ====================================================================== */

/*============================================================================
 *                       INCLUDE FILES FOR MODULE
 *==========================================================================*/
#include "qurt_elite.h"
#include "AFELoggingUtils.h"
#include "AFEPortManagement.h"
#include "audio_basic_op.h"

void afe_port_data_logging_init(afe_dev_port_t *pDevPort)
{
   uint16_t sizeFactor=0, unLogSizePerInt;
   uint32_t totalLogSize=0;

   unLogSizePerInt  = pDevPort->int_samples_per_period * pDevPort->channels * pDevPort->bytes_per_channel;

   totalLogSize = AFE_PCM_BUF_SIZE * unLogSizePerInt;

   // if size is larger than max allowed by diag, then assign total size as max int multiple of unLogSizePerInt less than AFE_MAX_LOG_SIZE_BYTES.
   if (totalLogSize > AFE_MAX_LOG_SIZE_BYTES)
   {
      while ((sizeFactor*unLogSizePerInt) < (int32_t)AFE_MAX_LOG_SIZE_BYTES) sizeFactor++;

      // above loop always incrementing one extra and hence decrement by one
      sizeFactor--;
      totalLogSize = sizeFactor*unLogSizePerInt ;
   }
   else
   {
      sizeFactor = AFE_PCM_BUF_SIZE;
   }

   // seq num for session id
   (pDevPort->data_log.port_session_id_for_log)++;

   pDevPort->data_log.bytes_per_int_all_ch = unLogSizePerInt;
   pDevPort->data_log.total_log_size   = totalLogSize;
   pDevPort->data_log.log_size_factor  = sizeFactor;
   pDevPort->data_log.log_buf_ptr      = NULL;
   pDevPort->data_log.log_buf_ptr      = (int16_t *) qurt_elite_memory_malloc(totalLogSize, QURT_ELITE_HEAP_DEFAULT);
   if (NULL == pDevPort->data_log.log_buf_ptr)
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Port %x Error allocating memory for log buffer", pDevPort->intf);
   }   
   pDevPort->data_log.log_size_used    = 0;
   pDevPort->data_log.log_pkt_ptr      = NULL;
}

void afe_port_data_logging_deinit(afe_dev_port_t *pDevPort)
{
   elite_log_info  log_info_var;
   pcm_data_info *pcm_data = &(log_info_var.data_info.pcm_data_fmt);

   /* If all the log buffer size has been used, do commit the log buffer. */
   if (pDevPort->data_log.log_size_used > 0 && pDevPort->data_log.log_pkt_ptr != NULL)
   {
	   uint16_t logId;
	   logId = AUDIOLOG_CHAN_PCM_RX_INTERNAL_AFE + pDevPort->intf;

	   log_info_var.qxdm_log_code = QXDM_LOG_CODE_AFE_RX_TX_OUT;
	   log_info_var.buf_ptr  = (uint8_t *)pDevPort->data_log.log_buf_ptr;
	   log_info_var.buf_size = pDevPort->data_log.log_size_used;
	   log_info_var.session_id = pDevPort->data_log.port_session_id_for_log;
	   log_info_var.log_tap_id  = logId;
	   log_info_var.log_time_stamp = qurt_elite_timer_get_time();
	   log_info_var.data_fmt = ELITE_LOG_DATA_FMT_PCM;
	   pcm_data->num_channels = pDevPort->channels;
	   pcm_data->sampling_rate = pDevPort->sample_rate;
	   pcm_data->bits_per_sample = (pDevPort->bytes_per_channel<<3);
	   pcm_data->interleaved = TRUE;
	   pcm_data->channel_mapping = NULL;
	   elite_log_buffer(pDevPort->data_log.log_pkt_ptr, &log_info_var);
   }

   if (pDevPort->data_log.log_buf_ptr != NULL)
   {
      qurt_elite_memory_free(pDevPort->data_log.log_buf_ptr);
   }
   pDevPort->data_log.log_buf_ptr = NULL;
   pDevPort->data_log.log_pkt_ptr = NULL;
   pDevPort->data_log.log_size_used = 0;
}

void afe_port_data_logging(afe_dev_port_t *pDevPort, int8_t *mem_start_addr)
{
   uint16_t          unLogSizePerInt;
   uint16_t          logId,nNumSamples;
   uint32_t          log_size_used=0, totalLogSize=0;
   int8_t            *log_buf_ptr;
   void              *log_pkt_ptr;
   elite_log_info    log_info_var;
   pcm_data_info     *pcm_data;

   /*< check for status of 0x1586 log code and non-zero check for total size of log in bytes per diag commit.*/
   if(0 == pDevPort->data_log.total_log_size || (ADSP_EFAILED == afe_log_status(QXDM_LOG_CODE_AFE_RX_TX_OUT)))
   {
      return;
   }

   /* afe log buf size, and data size per interrupt */
   unLogSizePerInt  = pDevPort->data_log.bytes_per_int_all_ch;
   totalLogSize     = pDevPort->data_log.total_log_size;
   nNumSamples      = pDevPort->int_samples_per_period;
   log_size_used    = pDevPort->data_log.log_size_used;
   log_pkt_ptr      = pDevPort->data_log.log_pkt_ptr;
   log_buf_ptr      = (int8_t *)pDevPort->data_log.log_buf_ptr;


   if (0 == log_size_used)
   {
      if (AFE_UNCOMPRESSED_DATA_PORT == pDevPort->port_data_type)
      {
         // If 0x1586 log code is enabled, allocate buffer for 0x1586
         if(NULL == (log_pkt_ptr = log_alloc_buffer(totalLogSize, QXDM_LOG_CODE_AFE_RX_TX_OUT, ELITE_LOG_DATA_FMT_PCM)))
         {
            /* If 0x1586 is disabled or log_alloc() failed, then simply return */
            return;
         }        

      }
      else /* Compressed data port */
      {
         log_pkt_ptr = log_alloc_buffer(totalLogSize, QXDM_LOG_CODE_AFE_RX_TX_OUT, ELITE_LOG_DATA_FMT_BITSTREAM);

         if(NULL == log_pkt_ptr)
         {
            /* If 0x1586 log code is disabled or not sufficient space in diag buffer */
            return;
         }
      }
   }

   /* Populate the log buffer for every DMA/BAM interrupt */
   if (AFE_UNCOMPRESSED_DATA_PORT == pDevPort->port_data_type)
   {
      /* If log buffer available, populate it with port samples  */

      /* Old parser does not support deinterleaved data for multichannel (> 2 channel) config
         Though new parser supports deinterleaved data, here we are interleaving for code simplification 
      */ 
      if(!pDevPort->is_interleaved)
      {
         afe_port_interleave_samples(mem_start_addr, (int8_t *)(log_buf_ptr + (log_size_used)), pDevPort->channels, nNumSamples, pDevPort->bytes_per_channel); 
      }
      else
      {
         memscpy( (log_buf_ptr + log_size_used),
                  (totalLogSize - log_size_used),
                  mem_start_addr,
                  unLogSizePerInt);
      }

      if(4 == pDevPort->bytes_per_channel)
      {
         /* convert the format Q27 to Q31 */
         int32_t *pBuf_frm_ptr32 = (int32_t *)(log_buf_ptr + (log_size_used));
         uint32_t  total_num_samples =  (pDevPort->int_samples_per_period) * (pDevPort->channels); 
         for(uint32_t i=0; i<total_num_samples; i++)
         {
            pBuf_frm_ptr32[i] = s32_shl_s32_sat(pBuf_frm_ptr32[i], pDevPort->q_format_shift_factor);
         }
      }
   }
   else /* Compressed data port */
   {
       memscpy( (log_buf_ptr + log_size_used),
                (totalLogSize - log_size_used),
                mem_start_addr,
                unLogSizePerInt);
   }

   /* Update used log buffer size */
   log_size_used += unLogSizePerInt;

   /* If all the log buffer size has been used, do commit the log buffer. */
   if (log_size_used >= (totalLogSize))
   {
	   /* Fill the logId with the port ID. */
	   logId = AUDIOLOG_CHAN_PCM_RX_INTERNAL_AFE + pDevPort->intf;
	   
	   log_info_var.qxdm_log_code = QXDM_LOG_CODE_AFE_RX_TX_OUT;
	   log_info_var.buf_ptr  = (uint8_t *)log_buf_ptr;
	   log_info_var.buf_size = log_size_used;
	   log_info_var.session_id = pDevPort->data_log.port_session_id_for_log;
	   log_info_var.log_tap_id  = logId;
	   log_info_var.log_time_stamp = qurt_elite_timer_get_time();

	   if (AFE_UNCOMPRESSED_DATA_PORT == pDevPort->port_data_type)
	   {
		   log_info_var.data_fmt = ELITE_LOG_DATA_FMT_PCM;

		   pcm_data = &(log_info_var.data_info.pcm_data_fmt);

		   pcm_data->num_channels = pDevPort->channels;
		   pcm_data->sampling_rate = pDevPort->sample_rate;
		   pcm_data->bits_per_sample = (pDevPort->bytes_per_channel<<3);
		   pcm_data->interleaved = TRUE;
		   pcm_data->channel_mapping = NULL;
	   }
	   else /* compressed data port */
	   {
		   log_info_var.data_fmt = ELITE_LOG_DATA_FMT_BITSTREAM;
		   log_info_var.data_info.media_fmt_id = AFE_IEC61937_COMPRESSED_DATA_PORT;
	   }

	   elite_log_buffer(log_pkt_ptr, &log_info_var);
	   /* Reset the used log size to 0 */
	   log_size_used = 0;
   }

   pDevPort->data_log.log_size_used = log_size_used;
   pDevPort->data_log.log_pkt_ptr   = (void*)log_pkt_ptr;
   pDevPort->data_log.log_buf_ptr   = (int16_t *)log_buf_ptr;
}

/**
 * This function is for checking log code status whether it is 
 * enable or not. 
 *
 * @param[in] log_code log code to be checked 
 * @param[out] ADSP_EOK if the code is enabled otherwise 
 *  	 ADSP_EFAILED
 * 
 */
ADSPResult afe_log_status(uint16_t log_code)
{
#ifndef SIM
	if (log_status (log_code))
	{
		return ADSP_EOK;
	}
	return ADSP_EFAILED;
#else
	return ADSP_EOK;
#endif 
}
