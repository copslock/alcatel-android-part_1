/*========================================================================
  This file contains AFE Sample rate conversion related apis

  Copyright (c) 2009-2012 Qualcomm Technologies, Incorporated.  All Rights Reserved.
  QUALCOMM Proprietary.  Export of this technology or software is regulated
  by the U.S. Government, Diversion contrary to U.S. law prohibited.
 
  $Header: //components/rel/avs.adsp/2.7.1.c4/afe/services/static/inc/AFESampRateConv.h#1 $
 ====================================================================== */
#ifndef _SAMP_RATE_CONV_H_
#define _SAMP_RATE_CONV_H_

/*==========================================================================
  Include files
  ========================================================================== */
#include "qurt_elite.h"
#include "EliteMsg.h"
#include "AFEInterface.h"
#include "capi_v2_dynamic_resampler.h"
#include "voice_resampler.h"

#ifdef __cplusplus
extern "C" {
#endif //__cplusplus

/*==========================================================================
	  Macro Defines
========================================================================== */
#define SIZE_OF_ARRAY(a) (sizeof(a)/sizeof((a)[0]))

#define TWO_BYTES_PER_SAMPLE 2

#define INSTANCE_MEM	1
#define CHANNEL_MEM		0 

typedef enum {
	FIR_BASED_RESAMPLER = 0,
	IIR_BASED_RESAMPLER = 1,
	INVALID_RESAMP_TYPE = 0xFFFF
} samp_rate_conv_type_t;


typedef struct samp_rate_converter
{
	void				    	*resample_chan_ptr[MAX_AUDIO_CHANNELS];/* resampler channel pointers*/
	void						*resample_instance_ptr; /* resampler instance pointer*/
	uint32_t					frame_size_in; /* Number of samples into the resampler*/
	uint32_t					frame_size_out; /* Expected samples out of the resampler*/
	samp_rate_conv_type_t	  	resamp_type; /* Represents the resampler type in use*/
	uint32_t					bytes_per_sample;	/* bytes_per_sample of resampler */
	uint32_t					in_ch_spacing_in_bytes; /* channel spacing at input rate */
	uint32_t					out_ch_spacing_in_bytes; /* channel spacing at output rate */
	uint32_t					delay_in_us; /* delay in micro seconds based on input sampling rate*/
	uint32_t					fir_rs_kpps; /* FIR resampler KPPS */
	uint32_t					fir_rs_bw;	/* FIR resampler BW */
}samp_rate_conv_t;

/**
* This function is to select the resampler type for the given 
* case 
*
* @param[in] resamp_type, requests the sampling rate converter 
*       to be used
* @param[in] inFreq, input frequency for the resampler. 
* @param[in] outFreq, output frequency for the resampler. 
* @param[in] bytes_per_sample, number of bytes per sample (Min.
*  		of client and port bytes per sample)
* @return samp_rate_conv_type_t, retuns resampler type
* */ 
samp_rate_conv_type_t get_samp_rate_conv_type(afe_client_resampler_type resamp_type, uint32_t inFreq, uint32_t outFreq, uint32_t  bytes_per_sample);

/**
* This function is the wrapper function for the initialization 
* of various resamplers (Sample rate converters) 
*
* @param[in] pClientInfo, pointer to the client instance
* @param[in] bytes_per_sample, number of bytes per sample (Min.
*  		of client and port bytes per sample)
* @param[in] inFreq, input frequency for the resampler. 
* @param[in] outFreq, output frequency for the resampler. 
* @param[in] samp_rate_conv, pointer to sample rate 
*  		converter (resampler) instance.
* @return ADSPResult, retuns success or failure of the 
*  		  initialization
*  		  Caller should take care of deallocating any memory
*  		  created for this API call in case of failure
* */ 
/* Intialization*/
ADSPResult sample_rate_conv_init( samp_rate_conv_t *samp_rate_conv, uint16_t num_channels, uint32_t  bytes_per_sample, 
	uint32_t inFreq, uint32_t outFreq, samp_rate_conv_type_t resamp_type);


/**
* This function is the wrapper function for the various 
* resamplers (Sample rate converters) 
*
* @param[in] samp_rate_conv, resampler strcture
* @param[in] in_buf, input buffer for the resampler. 
* @param[in] out_buf, output buffer for the resampler. 
* @param[in] nchan, channel number. 
* @param[out] samples_processed, actual number of samples 
*  		processed by resampler.
* @param[in] mode, resampler mode. * 
* @return ADSPResult 
*/
/* Process */
ADSPResult sample_rate_conv_process(samp_rate_conv_t *samp_rate_conv, int8 *inBuf, int8 *outBuf, uint32 *samplesprocessed, uint16_t nChan);

/**
* This function is for freeing the memory created for the 
* resampler. 
*
* @param[in] resampler, pointer to instance of the resampler
* @param[in] resampler_memory, pointer to instance voice 
*  		resampler memory.
* @param[in] resamplerType, information about the type of the 
*  		resampler.
* @return none
*/
/* De Initialization */
void sample_rate_conv_deinit(samp_rate_conv_t *samp_rate_conv);

/**
* This function is the wrapper function for getting the 
* algorithm delay of various resamplers (Sample rate converters)
*  
* @param[in] samp_rate_conv, pointer to sample rate 
*  		converter (resampler) instance.
* @param[out] delay_us, delay of the resampler in micro sec.  
* */ 
void sample_rate_conv_get_delay(samp_rate_conv_t *samp_rate_conv, uint64_t *delay_us);


/**
* This function is to clear the data memory of voice resampler
* @param[in] samp_rate_conv, pointer to instance of the resampler
* @param[in] num_channels, number of channels
* return ADSPResult
*/
ADSPResult sample_rate_conv_memory_init(samp_rate_conv_t *samp_rate_conv, uint16_t num_channels);

/*This function should be called by connecting afe client, when re-sampler is needed.
 * If FIR re-sampler, gets client resampler kpps and BW through CAPIv2.
 * If IIR re-sampler, gets client resampler kpps from IIR re-sampler predefined table.
 *
 *pDevPort          : Used pDevPort to access mmpm_info_ptr, from which aggregated client kpps/bw is updated/stored
 *psNewClient       : Used psNewClient to access afe_client, to store individual client kpps/bw
 * */
void afe_port_aggregate_client_resampler_kpps_bw(void *pDevPort, void *psNewClient);


#ifdef __cplusplus
}
#endif //__cplusplus

#endif /* #ifndef _SAMP_RATE_CONV_H_ */
