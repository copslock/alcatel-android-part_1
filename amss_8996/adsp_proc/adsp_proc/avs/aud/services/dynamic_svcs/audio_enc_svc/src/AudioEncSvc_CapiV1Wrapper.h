/*
 * CAPIV1DECWRAPPER.h
 *
 *  Created on: Jan 20, 2014
 *      Author: rbhatnk
 */
/** @file AudioEncSvc_CapiV1Wrapper.h
This file contains utility functions for Elite Audio Encoder service.

Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government, Diversion contrary to U.S. law prohibited.
*/

/**
========================================================================
Edit History

$Header: //components/rel/avs.adsp/2.7.1.c4/aud/services/dynamic_svcs/audio_enc_svc/src/AudioEncSvc_CapiV1Wrapper.h#1 $


when       who     what, where, why
--------   ---     -------------------------------------------------------
07/22/2014   rbhatnk     Created file.

==========================================================================
*/
#ifndef CAPIV1DECWRAPPER_H_
#define CAPIV1DECWRAPPER_H_

#if defined(__cplusplus)
extern "C" {
#endif // __cplusplus

#include "Elite_CAPI_V2.h"
#include "mmdefs.h"


/** @ingroup
Used to query for static properties of the module that are independent of the
instance. This function is used to query the memory requirements of the module
in order to create an instance. The same properties that are sent to the module
in the call to init() are also sent to this function to enable the module to
calculate the memory requirement.

@param[in]  init_set_properties  The same properties that will be sent in the
                         call to the init() function.
@param[out] static_properties   Pointer to the structure that the module must
                         fill with the appropriate values based on the property
                         id.

@return
Indication of success or failure.

@dependencies
None.
*/
capi_v2_err_t audio_enc_svc_capi_v1_wrapper_get_static_properties (
   capi_v2_proplist_t *init_set_properties,
   capi_v2_proplist_t *static_properties);


/** @ingroup
Instantiates the module to set up the virtual function table, and also
allocates any memory required by the module. States within the module must
be initialized at the same time.

@datatypes
capi_v2_t \n
capi_v2_proplist_t \n
capi_v2_event_cb \n
void *

@param[in,out] _pif            Pointer to the module object.
@param[in]    init_set_properties Properties set by the service to be used
                              while init().

@return
Indication of success or failure.

@dependencies
*/
capi_v2_err_t audio_enc_svc_capi_v1_wrapper_init (
   capi_v2_t*              _pif,
   capi_v2_proplist_t      *init_set_properties);

#if defined(__cplusplus)
}
#endif // __cplusplus

#endif /* CAPIV1DECWRAPPER_H_ */
