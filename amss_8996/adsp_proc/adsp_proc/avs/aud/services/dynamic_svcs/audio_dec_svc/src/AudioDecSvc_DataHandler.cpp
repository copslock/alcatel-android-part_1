/*========================================================================

 *//** @file AudioDecSvc_DataHandler.cpp
This file contains functions for Elite Decoder service.

Copyright (c) 2014-2015 Qualcomm Technologies Inc. All Rights Reserved.
QUALCOMM Proprietary. Export of this technology or software is regulated
by the U.S. Government, Diversion contrary to U.S. law prohibited.
  *//*====================================================================== */

/*========================================================================
Edit History

$Header: //components/rel/avs.adsp/2.7.1.c4/aud/services/dynamic_svcs/audio_dec_svc/src/AudioDecSvc_DataHandler.cpp#1 $


when       who     what, where, why
--------   ---     -------------------------------------------------------
02/12/14    rbhatnk      Created file.

========================================================================== */


/* =======================================================================
INCLUDE FILES FOR MODULE
========================================================================== */

#include "qurt_elite.h"
#include "Elite.h"
#include "EliteMsg_Custom.h"
#include "AudioStreamMgr_AprIf.h"
#include "AudioDecSvc.h"
#include "AudioDecSvc_Util.h"
#include "AudioDecSvc_Structs.h"
#include "AudioDecSvc_MiMoUtil.h"
#include "audio_basic_op.h"
#include <audio_basic_op_ext.h>
#include "adsp_asm_api.h"
#include "adsp_media_fmt.h"
#include "AudioStreamMgr_GetSetBits.h"
#include "Interleaver.h"
#include "AudioDecSvc_ChanMap.h"
#include "AudioDecSvc_PullMode.h"
#include "adsp_mtmx_strtr_api.h"
#include "AudioDecSvc_CapiV2CallbackHandler.h"
#include "AudioDecSvc_CapiV2Util.h"

#include "adsp_asm_stream_commands.h"
/*--------------------------------------------------------------*/
/* Macro definitions                                            */
/* -------------------------------------------------------------*/

/* -----------------------------------------------------------------------
 ** Function prototypes
 ** ----------------------------------------------------------------------- */

// Main processing function for Decoding the bitstream data
static ADSPResult AudioDecSvc_Process(AudioDecSvc_t *pMe, uint8_t capi_index);
static ADSPResult AudioDecSvc_ProcessLoop(AudioDecSvc_t *pMe, AudioDecSvc_OutStream_t *pOutStrm, ADSPResult *prevDecResult);
//after process of all CAPIs.
static ADSPResult AudioDecSvc_PostProcess(AudioDecSvc_t *pMe,  AudioDecSvc_OutStream_t *pOutStrm);

static ADSPResult AudioDecSvc_ProcessOutputDataQForCAPIs(AudioDecSvc_t* pMe,AudioDecSvc_OutStream_t *pOutStrm);
static ADSPResult AudioDecSvc_FillCapiInpBuffer(AudioDecSvc_t* pMe,uint8_t capi_index);
static ADSPResult AudioDecSvc_ProcessOutputDataQ_PCMPlayback(AudioDecSvc_t *pMe, AudioDecSvc_OutStream_t *pOutStrm);
static ADSPResult AudioDecSvc_ProcessPCMNonEmptyOutputBuf(AudioDecSvc_t *pMe);
static void AudioDecSvc_ProcessEos(AudioDecSvc_t *pMe, AudioDecSvc_InpStream_t *pInpStrm, AudioDecSvc_OutStream_t *pOutStrm, bool_t bResetDecoder);
static void AudioDecSvc_SendMetadata(AudioDecSvc_t* pMe, AudioDecSvc_OutStream_t *pOutStrm);
static void AudioDecSvc_HandleEosCases(AudioDecSvc_t *pMe, AudioDecSvc_InpStream_t *pInpStrm, AudioDecSvc_OutStream_t *pOutStrm, bool_t outBufNotEmpty);
/*
 * sends metadata, output buf, and EoS.
 */
static ADSPResult AudioDecSvc_SendOutBufs(AudioDecSvc_t* pMe, AudioDecSvc_OutStream_t *pOutStrm);

static ADSPResult AudioDecSvc_ProcessInputDataQ_PeerService(AudioDecSvc_t *pMe, AudioDecSvc_InpStream_t *pInpStrm)
{
   ADSPResult result = ADSP_EOK;

   //process messages
   switch ( pInpStrm->inpDataQMsg.unOpCode )
   {
   case ELITE_DATA_BUFFER:
   {
      elite_msg_data_buffer_t* pInpBuf = (elite_msg_data_buffer_t*) pInpStrm->inpDataQMsg.pPayload;

      //MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "buf_recv_cnt is %d",pMe->buf_recv_cnt);
      //checking on whether we are waiting for a media format command is to be done in the CAPI V2 (or capi v1 wrapper).

      // if input buffer do not contain integer PCM samples per channel, return it immediately with error code
      if( AudioDecSvc_IsPcmFmt(pMe->capiContainer[0]->dec_fmt_id) )  //check first CAPI because it accepts inputs.
      {
         uint32_t unit_size = pInpStrm->pcmFmt.chan_map.nChannels * pInpStrm->pcmFmt.usBytesPerSample;

         if ( pInpBuf->nActualSize % unit_size )
         {
            MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Returning an input buffer that do not contain the same PCM samples on all channel!");
            return AudioDecSvc_FreeInputDataCmd(pMe, pInpStrm, ADSP_EBADPARAM);
         }
      }

      pInpStrm->inp_buf_params.unVirtAddr = (uint32_t)&pInpBuf->nDataBuf;
      pInpStrm->inp_buf_params.unMemSize = pInpBuf->nActualSize;

      pInpStrm->inp_buf_params_backup = pInpStrm->inp_buf_params;

      //Set the signal to look for output buffers
      pMe->unCurrentBitfield = AUD_DEC_CMD_SIG | AudioDecSvc_GetAllOutputStreamMask(pMe);

      //get time stamp info from the input buffer
      (void) AudioDecSvc_FillTsStateFromInBuf(&pInpStrm->TsState,
            pMe->mainCapiContainer->dec_fmt_id,
            pInpBuf->ullTimeStamp,
            asm_get_timestamp_valid_flag(pInpBuf->nFlag),   //bit 31.. so works
            FALSE, FALSE,
            pInpStrm->WasPrevDecResNeedMore);

      result = AudioDecSvc_ProcessPCMNonEmptyOutputBuf(pMe);

      break;
   }
   case ELITE_DATA_MEDIA_TYPE:
   {
      MSG(MSG_SSID_QDSP6, DBG_MED_PRIO, "Decoder svc media format update cmd from peer service");

      return AudioDecSvc_UpdateMediaFmt_PeerService(pMe, pInpStrm);

      break;
   }
   case ELITE_DATA_EOS:
   {
      MSG(MSG_SSID_QDSP6, DBG_MED_PRIO, "Decoder svc received EOS cmd from peer service");

      pInpStrm->bEndOfStream =  TRUE;

      //there might be some data left in the internal buffer
      //in the raw bitstream case. Process this completely first
      //set end of frame to 1 to force decoding of raw bitstream
      dec_CAPI_container_t *first_capi = AudioDecSvc_GetFirstCapi(pMe);
      first_capi->inputs[AudioDecSvc_GetInputStreamIndex(pMe, pInpStrm)].flags.end_of_frame = true;

      //some decoders need to know when end of stream has been
      //reached to process the last frame
      first_capi->inputs[AudioDecSvc_GetInputStreamIndex(pMe, pInpStrm)].flags.marker_eos = true;

      //Set the signal to look for output buffers
      pMe->unCurrentBitfield = AUD_DEC_CMD_SIG | AudioDecSvc_GetAllOutputStreamMask(pMe);

      result = AudioDecSvc_ProcessPCMNonEmptyOutputBuf(pMe);

      break;
   }
   default:
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "AudDecSvc Rx'd unsupported message!! ");
      result = AudioDecSvc_FreeInputDataCmd(pMe, pInpStrm, ADSP_EUNSUPPORTED);
      break;
   }
   }
   return (result);
}

static ADSPResult AudioDecSvc_ProcessInputDataQ_AprClient(AudioDecSvc_t *pMe, AudioDecSvc_InpStream_t *pInpStrm)
{
   ADSPResult nResult = ADSP_EOK;
   uint32_t uAprOpCode;
   elite_apr_packet_t* pAprPacket;

   //get the payload of the msg
   pAprPacket = (elite_apr_packet_t*) (pInpStrm->inpDataQMsg.pPayload);

   uAprOpCode = elite_apr_if_get_opcode( pAprPacket ) ;

   switch (uAprOpCode)
   {
   //Input data(bitstream) command
   case ASM_DATA_CMD_WRITE_V2:
   {
      //MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "buf_recv_cnt is %d",pMe->buf_recv_cnt);
      //first check whether we are waiting for a media format command

      asm_data_cmd_write_v2_t *pDataPayload;
      pDataPayload = (asm_data_cmd_write_v2_t *)elite_apr_if_get_payload_ptr(pAprPacket);

      //If payload of input data msg is not valid, return
      if (NULL==pDataPayload)
      {
         MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Cannot get APR data payload");
         //return the buffer and wait for new ones
         return AudioDecSvc_FreeInputDataCmd(pMe, pInpStrm, ADSP_EBADPARAM);
      }

      //if input buffer is zero size, return it immediately
      if(0 == pDataPayload->buf_size)
      {
         MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Returning an input buffer of zero size!");
         return AudioDecSvc_FreeInputDataCmd(pMe, pInpStrm, ADSP_EOK);
      }
      //if input buffer is not 32 byte aligned return it
      if(pDataPayload->buf_addr_lsw & MASK_32BYTE_ALIGNMENT)
      {
         MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Input buffer not 32 byte aligned, returning it!");
         return AudioDecSvc_FreeInputDataCmd(pMe, pInpStrm, ADSP_EBADPARAM);
      }

      // if input buffer do not contain integer PCM samples per channel, return it immediately with error code
      if( AudioDecSvc_IsPcmFmt(pMe->capiContainer[0]->dec_fmt_id) ) //enough to check first CAPI as it is the one accepting inputs.
      {
         uint32_t unit_size = pInpStrm->pcmFmt.chan_map.nChannels * pInpStrm->pcmFmt.usBytesPerSample;

         if ( pDataPayload->buf_size % unit_size )
         {
            MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Returning an input buffer that do not contain the same PCM samples on all channel!");
            return AudioDecSvc_FreeInputDataCmd(pMe, pInpStrm, ADSP_EBADPARAM);
         }
      }

      /*if this compressed pass-through session then check if frame rate has been received*/
      if(DEC_SVC_IO_FORMAT_CONV_TYPE_61937_TO_61937 == pInpStrm->io_fmt_conv)
      {
         if(!pInpStrm->is_frame_rate_set)
         {
            MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
                  "Compressed pass-thru mode and frame rate not received! Returning buffer");
            return AudioDecSvc_FreeInputDataCmd(pMe, pInpStrm, ADSP_EUNEXPECTED);
         }
      }

      pInpStrm->inp_buf_params.unMemMapHandle = pDataPayload->mem_map_handle;
      //Convert the physical address of the shared memory buffer to virtual address
      if (ADSP_FAILED(nResult = elite_mem_map_get_shm_attrib_ref_counted(pDataPayload->buf_addr_lsw,pDataPayload->buf_addr_msw,
            pDataPayload->buf_size,
            &(pInpStrm->inp_buf_params) )))
      {
         MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
               "AudioDecSvc:Phy to Virt Failed(paddr,vaddr)-->(%lx%lx,%lx)\n",
               pInpStrm->inp_buf_params.unPhysAddrMsw,
               pInpStrm->inp_buf_params.unPhysAddrLsw,
               pInpStrm->inp_buf_params.unVirtAddr);
         return AudioDecSvc_FreeInputDataCmd(pMe, pInpStrm, ADSP_EBADPARAM);
      }

      //Backing up inp_buf_params for cache invalidation in FreeInputDataCmd
      pInpStrm->inp_buf_params_backup = pInpStrm->inp_buf_params;

      //invalidate the cache before reading from shared memory.
      elite_mem_invalidate_cache( &(pInpStrm->inp_buf_params) );

      //copy bitstream from sharedMem to internal buffer and
      //decode from the internal buffer (malloced on RAM)
      //Set the signal to look for output buffers
      pMe->unCurrentBitfield = AUD_DEC_CMD_SIG | AudioDecSvc_GetAllOutputStreamMask(pMe);

      //get time stamp info from the input buffer
      (void) AudioDecSvc_FillTsStateFromInBuf(&pInpStrm->TsState,
            pMe->capiContainer[0]->dec_fmt_id,
            ((((uint64_t)pDataPayload->timestamp_msw)<<32)|(uint64_t)pDataPayload->timestamp_lsw),
            (bool_t) asm_get_timestamp_valid_flag(pDataPayload->flags),
            (bool_t) asm_get_ts_continue_flag(pDataPayload->flags),
            asm_get_eof_flag(pDataPayload->flags),
            pInpStrm->WasPrevDecResNeedMore);

      //get the end of frame flag
      pInpStrm->bEndOfFrame = asm_get_eof_flag(pDataPayload->flags);

      //set EoF flag to False in the CAPI, since only the last copy from input buffer
      //assures an integral number of frames in the decoder internal buffer
      dec_CAPI_container_t *first_capi = AudioDecSvc_GetFirstCapi(pMe);
      first_capi->inputs[AudioDecSvc_GetInputStreamIndex(pMe, pInpStrm)].flags.end_of_frame = false;

      //get last buffer flag
      pInpStrm->silence_removal.last_buffer = asm_get_last_buffer_flag(pDataPayload->flags);

      nResult = AudioDecSvc_ProcessPCMNonEmptyOutputBuf(pMe);

      break;

   } // case ASM_DATA_CMD_WRITE_V2, end of processiong client data buffer

   case ASM_DATA_CMD_EOS:
   {
      MSG(MSG_SSID_QDSP6, DBG_MED_PRIO, "Decoder svc received EOS cmd!!!!");

      pInpStrm->bEndOfStream =  TRUE;

      //TODO : need to handle in case of secondary stream
      if(pInpStrm->common.stream_type != ASM_STREAM_TYPE_SECONDARY)
      {
         //there might be some data left in the internal buffer
         //in the raw bitstream case. Process this completely first
         //set end of frame to 1 to force decoding of raw bitstream
         dec_CAPI_container_t *first_capi = AudioDecSvc_GetFirstCapi(pMe);
         first_capi->inputs[AudioDecSvc_GetInputStreamIndex(pMe, pInpStrm)].flags.end_of_frame = true;

         //some decoders need to know when end of stream has been
         //reached to process the last frame
         first_capi->inputs[AudioDecSvc_GetInputStreamIndex(pMe, pInpStrm)].flags.marker_eos = true;
      }

      //Set the signal to look for output buffers
      pMe->unCurrentBitfield = AUD_DEC_CMD_SIG | AudioDecSvc_GetAllOutputStreamMask(pMe);

      nResult = AudioDecSvc_ProcessPCMNonEmptyOutputBuf(pMe);

      break;
   }

   case ASM_DATA_CMD_MARK_BUFFER_V2:
   {
      elite_msg_any_t markBuffer;
      uint32_t unMarkBufferPayloadSize = sizeof( elite_msg_data_mark_buffer_t );
      ADSPResult nRes;
      nRes = elite_msg_create_msg( &markBuffer,
            &unMarkBufferPayloadSize,
            ELITE_DATA_MARK_BUFFER,
            NULL,
            0,
            0);
      if (ADSP_FAILED(nRes))
      {
         MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"ERROR allocating Mark Buffer");
         //free input data cmd
         AudioDecSvc_DiscardMarkBuffer(pMe,pInpStrm);
         return AudioDecSvc_FreeInputDataCmd(pMe, pInpStrm, ADSP_EOK);
      }
      asm_data_cmd_mark_buffer_v2_t *apr_payload = NULL;
      elite_apr_if_get_payload((void **)&apr_payload, pAprPacket);
      elite_msg_data_mark_buffer_t *pMarkBufferPayload = (elite_msg_data_mark_buffer_t *) (markBuffer.pPayload);

      pMarkBufferPayload->token_lsw = apr_payload->token_lsw;
      pMarkBufferPayload->token_msw = apr_payload->token_msw;
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Decoder svc received Marked Buffer %lu !!!!",apr_payload->token_lsw);
      //TODO: wont work for MIMO
      AudioDecSvc_OutStream_t *pOutStrm = AudioDecSvc_GetDefaultOutputStream(pMe);
      if (ADSP_FAILED(qurt_elite_queue_push_back(pOutStrm->pDownStreamSvc->dataQ, (uint64_t*)&markBuffer)))
      {
         MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"ERROR sending Mark Buffer downstream");
         AudioDecSvc_DiscardMarkBuffer(pMe,pInpStrm);
         elite_msg_release_msg(&markBuffer);
      }

      //free input data cmd
      return AudioDecSvc_FreeInputDataCmd(pMe, pInpStrm, ADSP_EOK);
   }
   break;

   case ASM_DATA_CMD_MEDIA_FMT_UPDATE_V2:
   {
      MSG(MSG_SSID_QDSP6, DBG_MED_PRIO, "Decoder svc media format update cmd!!!!");

      //get media format update cmd payload
      asm_data_cmd_media_fmt_update_v2_t *pMediaFmt;
      pMediaFmt = (asm_data_cmd_media_fmt_update_v2_t *)elite_apr_if_get_payload_ptr(pAprPacket);

      nResult = ADSP_EFAILED;
      if(pMediaFmt)
      {
         uint32_t ulFmtBlkSize =
               ((asm_data_cmd_media_fmt_update_v2_t*)pMediaFmt)->fmt_blk_size;
         uint8_t *pFmtBlk = (uint8_t*)pMediaFmt + sizeof(asm_data_cmd_media_fmt_update_v2_t);

         nResult = AudioDecSvc_UpdateMediaFmt(pMe, pInpStrm, pFmtBlk, ulFmtBlkSize);
      }

      return AudioDecSvc_FreeInputDataCmd(pMe, pInpStrm, nResult);
   }

   case ASM_DATA_CMD_REMOVE_INITIAL_SILENCE:
   {
      asm_data_cmd_remove_initial_silence_t *initial_silence_ptr;
      void *pDummy;
      elite_apr_if_get_payload(&pDummy, pAprPacket);
      initial_silence_ptr = (asm_data_cmd_remove_initial_silence_t *)pDummy;
      int decoder_delay=0;

      /*Only update if decoding has not yet begun*/
      if(!decoding_begun(pMe))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,
               "AudioDecSvc: received REMOVIE_INITIAL_SILENCE, samples to remove:%lu",
               initial_silence_ptr->num_samples_to_remove);
         pInpStrm->silence_removal.initial_samples_to_remove = initial_silence_ptr->num_samples_to_remove;

         decoder_delay = 0; //ignore failure
         aud_dec_svc_get_param(pMe->capiContainer[0]->capi_ptr,CAPI_PARAM_DECODER_DELAY,
                  (int8_t*)&decoder_delay,sizeof(decoder_delay));

         pInpStrm->silence_removal.initial_samples_to_remove += decoder_delay;

         return AudioDecSvc_FreeInputDataCmd(pMe, pInpStrm, ADSP_EOK);
      }
      else
      {
         MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
               "AudioDecSvc:ASM_DATA_CMD_REMOVE_INITIAL_SILENCE recevied after decoding begun, ignoring");
         return AudioDecSvc_FreeInputDataCmd(pMe, pInpStrm, ADSP_EFAILED);
      }
   }

   case ASM_DATA_CMD_REMOVE_TRAILING_SILENCE:
   {

      asm_data_cmd_remove_trailing_silence_t *trailing_silence_ptr;
      void *pDummy;
      int trailing_delay=0;
      elite_apr_if_get_payload(&pDummy, pAprPacket);
      trailing_silence_ptr = (asm_data_cmd_remove_trailing_silence_t *)pDummy;

      /*Only update if last buffer has not yet been processed*/
      if(!pInpStrm->silence_removal.last_buffer)
      {
         MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,
               "AudioDecSvc: received REMOVIE_TRAILING_SILENCE, samples to remove:%lu",
               trailing_silence_ptr->num_samples_to_remove);
         pInpStrm->silence_removal.trailing_samples_to_remove =  trailing_silence_ptr->num_samples_to_remove;

         trailing_delay = 0; //ignore failure.
         aud_dec_svc_get_param(pMe->capiContainer[0]->capi_ptr,CAPI_PARAM_TRAILING_DELAY,
               (int8_t*)&trailing_delay,sizeof(trailing_delay));

         if((trailing_silence_ptr->num_samples_to_remove-trailing_delay) > 0)
         {
            pInpStrm->silence_removal.trailing_samples_to_remove =  (trailing_silence_ptr->num_samples_to_remove-trailing_delay);
         }
         return AudioDecSvc_FreeInputDataCmd(pMe, pInpStrm, ADSP_EOK);
      }
      else
      {
         MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
               "AudioDecSvc:ASM_DATA_CMD_REMOVE_TRAILING_SILENCE recevied after last buffer, ignoring");
         return AudioDecSvc_FreeInputDataCmd(pMe, pInpStrm, ADSP_EFAILED);
      }

   }
   case ASM_DATA_CMD_IEC_60958_FRAME_RATE:
   {
      asm_data_cmd_iec_60958_frame_rate *frame_rate_ptr;
      void *pDummy;
      elite_apr_if_get_payload(&pDummy, pAprPacket);
      frame_rate_ptr = (asm_data_cmd_iec_60958_frame_rate *)pDummy;

      /*This message should be received only in compressed pass through mode*/
      if(DEC_SVC_IO_FORMAT_CONV_TYPE_61937_TO_61937 != pInpStrm->io_fmt_conv)
      {
         MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "AudioDecSvc: Received frame rate in Non Pass-through case ");
         return AudioDecSvc_FreeInputDataCmd(pMe, pInpStrm, ADSP_EFAILED);
      }
      else
      {
         MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "AudioDecSvc: Received frame rate = %lu", frame_rate_ptr->frame_rate);

         (void) aud_dec_svc_set_in_sample_rate(pMe->capiContainer[0]->capi_ptr,
               AudioDecSvc_GetInputStreamIndex(pMe, pInpStrm), frame_rate_ptr->frame_rate);

         pInpStrm->is_frame_rate_set = TRUE;
         return AudioDecSvc_FreeInputDataCmd(pMe, pInpStrm, ADSP_EOK);
      }
   }

   default:
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Unexpected opCode for data write 0x%lx", uAprOpCode );
      return AudioDecSvc_FreeInputDataCmd(pMe, pInpStrm, ADSP_EUNSUPPORTED);
   }
   } /* switch (uAprOpCode) */

   return nResult;
}

ADSPResult AudioDecSvc_ProcessInputDataQ(AudioDecSvc_t *pMe, uint32_t channelBitIndex)
{
   ADSPResult nResult = ADSP_EOK;

   QURT_ELITE_ASSERT(AUD_DEC_INP_SIG_BIT_IND_TO_INDEX(channelBitIndex) < DEC_SVC_MAX_INPUT_STREAMS);

   AudioDecSvc_InpStream_t *pInpStrm = pMe->in_streams_ptr[AUD_DEC_INP_SIG_BIT_IND_TO_INDEX(channelBitIndex)];

   // Take next msg off the q
   nResult = AudioDecSvc_GetInputDataCmd(pMe, pInpStrm);
   if(ADSP_EOK != nResult)
   {
      //here, failure to get input data msg could mean (a)that we already hold
      //on to a data msg but erroneously tried to dequeue a new one (b) non-APR message.
      //release the msg and maintain status quo.
      return AudioDecSvc_FreeInputDataCmd(pMe, pInpStrm, nResult);
   }

   if (ELITE_APR_PACKET != pInpStrm->inpDataQMsg.unOpCode)
   {
      return AudioDecSvc_ProcessInputDataQ_PeerService(pMe, pInpStrm);
   }
   else
   {
      return AudioDecSvc_ProcessInputDataQ_AprClient(pMe, pInpStrm);
   }

   return nResult;
}

/**
 * Function to process partially filled output buffer for PCM decoder in
 * ULL and LLNP modes.
 *
 * PCM Decoder has only one output buffer. Decoder waits on it's
 * output buffer Q activity once it receives input buffer, if that
 * buffer is already popped in last input buffer process(and it was not
 * full due to input not multiple of 1ms in ULL and LLNP and that buffer
 * should not be pushed back to output Q else that much data will be
 * lost when it is popped next time), then there is nothing in decoder output
 * buffer Q, which will cause no data processing as it will wait
 * forever on output buffer Q and that buffer Q is empty so it's deadlock.
 * To remove that deadlock, first previously popped partially filled output
 * should be processed and then wait for output buffer after sending
 * downstream if buffer is full else same sequence should be executed.
 */
static ADSPResult AudioDecSvc_ProcessPCMNonEmptyOutputBuf(AudioDecSvc_t *pMe)
{
   ADSPResult result = ADSP_EOK;
   if (AudioDecSvc_IsPcmFmt(pMe->capiContainer[0]->dec_fmt_id) &&
       (DEC_SVC_PULL_MODE != pMe->ulMode))
   {
      AudioDecSvc_OutStream_t *pOutStrm = pMe->out_streams_ptr[0];
      if (NULL != pOutStrm->outDataBufferNode.pBuffer)
      {
         /* Output buffer is already popped, this needs to be processed first */
         result = AudioDecSvc_ProcessOutputDataQ_PCMPlayback(pMe, pOutStrm);
      }
   }
   return result;
}

static void AudioDecSvc_ProcessEos(AudioDecSvc_t *pMe, AudioDecSvc_InpStream_t *pInpStrm, AudioDecSvc_OutStream_t *pOutStrm, bool_t bResetDecoder)
{
   if (bResetDecoder)
   {
      //reset the decoder
      AudioDecSvc_ResetDecoder(pMe);
   }

   //send EOS command downstream
   // Need to repacket this as an internal EOS command.

   MSG(MSG_SSID_QDSP6, DBG_MED_PRIO,"DecSvc: sending EOS downstream");

   elite_msg_any_t eosMsg;
   uint32_t unEosPayloadSize = sizeof( elite_msg_data_eos_apr_t );
   ADSPResult nRes;
   nRes = elite_msg_create_msg( &eosMsg,
         &unEosPayloadSize,
         ELITE_DATA_EOS,
         NULL,
         0,
         0);
   if (ADSP_FAILED(nRes))
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"ERROR sending EOS downstream, out of buffer. client will not receive rendered EOS event from connected downstream!");
      return;
   }
   elite_msg_data_eos_apr_t *pEosPayload = (elite_msg_data_eos_apr_t *) (eosMsg.pPayload);
   pEosPayload->unEosFormat   = ELITEMSG_DATA_EOS_APR  ;

   //Extract client info from apr packet
   elite_msg_extract_client_info( (elite_apr_packet_t *) (pInpStrm->inpDataQMsg.pPayload),
         &(pEosPayload->eosInfo) );

   //fill up the EOS ack event opcode so that services like AFE can blindly raise it
   pEosPayload->eosInfo.event_opcode = ASM_DATA_EVENT_RENDERED_EOS;

   qurt_elite_queue_push_back(pOutStrm->pDownStreamSvc->dataQ, (uint64_t*)&eosMsg);
}

ADSPResult AudioDecSvc_ProcessOutputDataQ(AudioDecSvc_t *pMe, uint32_t channelBitIndex)
{
   ADSPResult nResult = ADSP_EFAILED;

   QURT_ELITE_ASSERT(AUD_DEC_OUT_SIG_BIT_IND_TO_INDEX(channelBitIndex) < DEC_SVC_MAX_OUTPUT_STREAMS);

   AudioDecSvc_OutStream_t *pOutStrm = pMe->out_streams_ptr[AUD_DEC_OUT_SIG_BIT_IND_TO_INDEX(channelBitIndex)];

   //PCM decoder is supported only with 1 CAPI container. PULL mode is always with only PCM.
   if (DEC_SVC_PULL_MODE == pMe->ulMode)
   {
      return AudioDecSvc_ProcessOutputDataQ_PCMPullMode(pMe, pOutStrm);
   }

   //PCM decoder is supported only with 1 CAPI container.
   if ( AudioDecSvc_IsPcmFmt(pMe->capiContainer[0]->dec_fmt_id) )
   {
      return AudioDecSvc_ProcessOutputDataQ_PCMPlayback(pMe, pOutStrm);
   }

   if (pOutStrm->metadata_xfr.is_waiting_to_push_metadata)
   {
      return AudioDecSvc_SendOutBufs(pMe, pOutStrm);
   }

   nResult = AudioDecSvc_ProcessOutputDataQForCAPIs(pMe,pOutStrm);

   return nResult;
}

//#define MULTI_CAPI_FILE_LOG 1

//TODO: this memcpy can be avoided if same buf is used for input of first capi and output of next capi. But prob is,
//      then buf_size is not a member of 1 capi, but 2 capis & its size depends on 2 capis - difficult to handle.
//TODO: In below actual_len is assigned with nonzero value, before passing to the CAPIs. this may not be desired as CAPIs might assume out buf is empty.
//      this can be handled by creating a temp var for capi_v2_buf_t & using it with actual=0 & appropriate max len.
static ADSPResult AudioDecSvc_CopyPrevOutputToNextInput(dec_CAPI_container_t *next_capi, dec_CAPI_container_t *prev_capi)
{
   uint32_t empty_space_in_next_inp = (next_capi->in_buf[0].max_data_len -
         next_capi->in_buf[0].actual_data_len);

   uint32_t bytes_to_copy = empty_space_in_next_inp > prev_capi->out_buf[0].actual_data_len ?
         prev_capi->out_buf[0].actual_data_len: empty_space_in_next_inp;

   memscpy(next_capi->in_buf[0].data_ptr + next_capi->in_buf[0].actual_data_len,
         empty_space_in_next_inp,
         prev_capi->out_buf[0].data_ptr, //always starts from beginning due to memmove
         bytes_to_copy);

   next_capi->in_buf[0].actual_data_len += bytes_to_copy;

   //if prev out has some valid data in it, then move it to the beginning.
   uint32_t bytes_remain_in_prev_out = prev_capi->out_buf[0].actual_data_len > bytes_to_copy ?
         (prev_capi->out_buf[0].actual_data_len - bytes_to_copy): 0;

   if (bytes_remain_in_prev_out)
   {
      memsmove(prev_capi->out_buf[0].data_ptr,bytes_remain_in_prev_out,
            prev_capi->out_buf[0].data_ptr + bytes_to_copy,
            bytes_remain_in_prev_out);
   }
   prev_capi->out_buf[0].actual_data_len = bytes_remain_in_prev_out;

#if (MULTI_CAPI_FILE_LOG == 1)
   int8_t * buf_addr;
   uint32_t buf_size;

   buf_addr = (int8_t *)(next_capi->input_buf_list.pBuf[0].Data) + next_capi->bytes_logged;
   buf_size = (next_capi->in_buf[0].actual_data_len -  next_capi->bytes_logged);

   if(buf_size)
   {
      FILE *fp;
      char fileName[100];
      sprintf(fileName, "CAPI_in_%p.bin", next_capi);
      fp = fopen(fileName,"a"); // in append mode
      fwrite(buf_addr,sizeof(int8_t),buf_size,fp);
      fclose(fp);
   }
#endif

   return ADSP_EOK;
}

//sets up the first CAPI with input data (from client buffer or LPM)
static ADSPResult AudioDecSvc_FillCapiInpBuffer(AudioDecSvc_t* pMe,uint8_t capi_input_index)
{
   ADSPResult nResult = ADSP_EOK;
   dec_CAPI_container_t *first_capi = AudioDecSvc_GetFirstCapi(pMe);
   AudioDecSvc_InpStream_t* pInpStrm = pMe->in_streams_ptr[capi_input_index];

   uint8_t* pucIntBufOffset = (uint8_t*)first_capi->in_buf[capi_input_index].data_ptr;
   uint32_t  nInternalBufSize  = first_capi->in_buf[capi_input_index].max_data_len;
   uint32_t  nIntBufWrIdx = (uint32_t)first_capi->in_buf[capi_input_index].actual_data_len;
   uint32_t  nEmptyIntBufBytes = (uint32_t)((int)nInternalBufSize - (int)nIntBufWrIdx);

   int32_t nBytesToCopy;

   uint32_t *remClientBytes;
   uint8_t  **ppucIn;

   ppucIn = (uint8_t**)&pInpStrm->inp_buf_params.unVirtAddr;
   remClientBytes = &pInpStrm->inp_buf_params.unMemSize;

   //bytes to be copied = minimum of left over client data or left over empty space in internal buffer
   nBytesToCopy = (*remClientBytes < nEmptyIntBufBytes)? *remClientBytes:nEmptyIntBufBytes;

   memscpy((pucIntBufOffset+nIntBufWrIdx), nEmptyIntBufBytes, (const void*)*ppucIn, nBytesToCopy);

   //book keeping of remaining bytes in client and empty bytes in internal buffer
   *remClientBytes -= nBytesToCopy;
   *ppucIn += nBytesToCopy;

   nEmptyIntBufBytes -= nBytesToCopy;
   first_capi->in_buf[capi_input_index].actual_data_len += nBytesToCopy;

   //if we have copied the entire data from client buffer
   //return the buffer with an ack
   if(0 == *remClientBytes)
   {
      //Ack to Client, because this acks ASM_DATA_CMD_WRITE_V2, ASM will fill the ack opcode, therefore,
      //NULL ack payload, ack payload size, and ack opCode
      nResult = AudioDecSvc_FreeInputDataCmd(pMe, pInpStrm, ADSP_EOK);

      //If EoF was set in the input buffer, then last copy to decoder internal buffer ensures
      //integral number of frames in the decoder internal buffer.
      dec_CAPI_container_t *first_capi = AudioDecSvc_GetFirstCapi(pMe);
      first_capi->inputs[AudioDecSvc_GetInputStreamIndex(pMe, pInpStrm)].flags.end_of_frame = pInpStrm->bEndOfFrame;
   }
   return nResult;
}


static ADSPResult AudioDecSvc_ProcessLoop(AudioDecSvc_t *pMe, AudioDecSvc_OutStream_t *pOutStrm, ADSPResult *prevDecResult)
{
   ADSPResult nResult = ADSP_EOK;
   dec_CAPI_container_t *capi_container;

   for (uint8_t capi_index = 0; capi_index < DEC_SVC_MAX_CAPI; capi_index++)
   {
      capi_container = pMe->capiContainer[capi_index];

      if (!capi_container) break;

      //except for first CAPI, handle change in media format based on the output of previous CAPI
      //also Copy output of previous CAPI to next CAPI input.
      if (capi_index != 0)
      {
         //TODO: to handle the media fmt change between CAPIs
         if (pMe->capiContainer[capi_index-1]->media_fmt_event[AudioDecSvc_GetOutputStreamIndex(pMe, pOutStrm)])
         {
            MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "sample rate or channel map changed between CAPI %u and CAPI %u. Need to implement this!",capi_index, capi_index-1);
            pMe->capiContainer[capi_index-1]->media_fmt_event[AudioDecSvc_GetOutputStreamIndex(pMe, pOutStrm)] = false;
         }

         //copy output of first CAPI to input of second except for first one.
         //first one has input already copied earlier
         //for last buffer output buf is the dec svc output buffer. already assigned.
         nResult = AudioDecSvc_CopyPrevOutputToNextInput(capi_container, pMe->capiContainer[capi_index-1]);
      }

      //Go for decoding
      nResult = AudioDecSvc_Process( pMe, capi_index);

      if ( ADSP_FAILED(nResult))
      {
         MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Decoder CAPI %u, process error 0x%x",capi_index, nResult);
      }

      *prevDecResult = nResult; //keep the decode result. nResult is used for other purpose below.

      //TODO : bytes logging for mimo case
      //any valid input bytes left in internal buffer shouldn't be logged, mark as already logged.
      capi_container->bytes_logged = capi_container->in_buf[0].actual_data_len;

      //if previous output was not finished, copy it now.
      if ((capi_index != 0) && (pMe->capiContainer[capi_index-1]->out_buf[0].actual_data_len > 0))
      {
         nResult = AudioDecSvc_CopyPrevOutputToNextInput(capi_container, pMe->capiContainer[capi_index-1]);

         if (pMe->capiContainer[capi_index-1]->out_buf[0].actual_data_len)
         {
            MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Despite copying prev output to current (%u) input twice, prev output is not finished.",capi_index);
         }

         //any valid input bytes left in internal buffer shouldn't be logged, mark as already logged.
         capi_container->bytes_logged = capi_container->in_buf[0].actual_data_len;
      }

      //even if one dec returns failure or need more return.
      if (ADSP_FAILED(nResult)) return nResult;
   }

   //release=false. vote only if there's an event.
   AudioDecSvc_ProcessKppsBw(pMe, FALSE, FALSE);

   return nResult;
}

static bool_t isCircbufEmpty(Circular_Buffer_List *pCirBufList)
{
   //If read index is equal to write index then there is no valid buffer in list
   return (pCirBufList->readI == pCirBufList->writeI);
}

static ADSPResult AudioDecSvc_SendFromCircBuf(AudioDecSvc_t *pMe, AudioDecSvc_OutStream_t *pOutStrm)
{
   ADSPResult nResult = ADSP_EOK;

   //use buffer present at read index from list

   //pop the buffer from output data Q
   if(ADSP_FAILED(nResult = qurt_elite_queue_pop_front(pOutStrm->pOutBufQ, (uint64_t*)&(pOutStrm->outDataBufferNode))))
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "AudioDecSvc:PBQ:Failure on Out Buf Pop.");
      return nResult;
   }

   elite_msg_data_buffer_t *pOutBuf = (elite_msg_data_buffer_t*)pOutStrm->outDataBufferNode.pBuffer;
   AudioDecSvc_SubtractBufDelayUsingClientToken(pOutStrm, &pOutBuf->unClientToken);
   if(AudioDecSvc_IsReallocateExternalBuffer(pMe, pOutStrm, &nResult)) { return nResult; }

   //swap the output buffer with buffer present at readI in list
   elite_msg_data_buffer_t * pBuf = pOutStrm->CirBufList.buflist[pOutStrm->CirBufList.readI];
   pOutStrm->CirBufList.buflist[pOutStrm->CirBufList.readI] = pOutBuf;
   pOutStrm->outDataBufferNode.pBuffer = (void*)pBuf;

   //incrementing readI
   pOutStrm->CirBufList.readI = (pOutStrm->CirBufList.readI + 1) % (pOutStrm->CirBufList.numBufs);

   return AudioDecSvc_SendOutBufs(pMe, pOutStrm);
}

/**
 * sets up the first CAPI with input data (from client buffer or LPM)
 * and last CAPI with output buffers (from output queue).
 */
static ADSPResult AudioDecSvc_ProcessOutputDataQForCAPIs(AudioDecSvc_t *pMe, AudioDecSvc_OutStream_t *pOutStrm)
{
   ADSPResult nResult = ADSP_EOK;
   ADSPResult rc = ADSP_EOK;
   dec_CAPI_container_t *last_capi = AudioDecSvc_GetLastCapi(pMe);
   dec_CAPI_container_t *first_capi = AudioDecSvc_GetFirstCapi(pMe);
   int16_t ii = 0;
   if(NULL == last_capi)
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "AudioDecSvc:Fail to get last capi");
      return ADSP_EFAILED;
   }
   //If circ buf has some data then send from there.
   if(!isCircbufEmpty(&pOutStrm->CirBufList))
   {
      return AudioDecSvc_SendFromCircBuf(pMe, pOutStrm);
   }

   //Get the channel status of all output data Q
   uint32_t OutStrmMask = AudioDecSvc_GetAllOutputStreamMask(pMe);
   uint32_t unChannelStatus = qurt_elite_channel_poll(&(pMe->channel), OutStrmMask) & pMe->unCurrentBitfield;

   //get default input stream(primary or primary_secondary stream for mimo decoder)
   AudioDecSvc_InpStream_t* pInpStrm = AudioDecSvc_GetDefaultInputStream(pMe);

   //remaining client bytes for each input stream
   uint32_t *remClientBytes[DEC_SVC_MAX_INPUT_STREAMS] = {NULL};

   //assigning buffers to capi outputs
   for(ii = 0; ii < DEC_SVC_MAX_OUTPUT_STREAMS; ii++)
   {
      if(pMe->out_streams_ptr[ii] == NULL) continue;

      //If Signal is set in output data q then pop from output data q
      if(AUD_DEC_OUT_INDEX_TO_DATA_SIG(ii) & unChannelStatus)
      {
         nResult |= qurt_elite_queue_pop_front(pMe->out_streams_ptr[ii]->pOutBufQ, (uint64_t*)&(pMe->out_streams_ptr[ii]->outDataBufferNode));
         if(ADSP_FAILED(nResult))
         {
            MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "AudioDecSvc:PBQ:Failure Buf Pop for output stream index : %d",(int)ii);
            //if pop failed for one output stream then return all previous buffers
            goto __bailout__;
         }

         elite_msg_data_buffer_t *pOutBuf = (elite_msg_data_buffer_t*)pMe->out_streams_ptr[ii]->outDataBufferNode.pBuffer;
         AudioDecSvc_SubtractBufDelayUsingClientToken(pMe->out_streams_ptr[ii], &pOutBuf->unClientToken);
         if(AudioDecSvc_IsReallocateExternalBuffer(pMe, pOutStrm, &nResult)) { return nResult; }
      }
      //if signal is not set then use buffer present at writeI in list
      else
      {
         pMe->out_streams_ptr[ii]->outDataBufferNode.pBuffer =
               (void*)pMe->out_streams_ptr[ii]->CirBufList.buflist[pMe->out_streams_ptr[ii]->CirBufList.writeI];

         //increment the writeI
         pMe->out_streams_ptr[ii]->CirBufList.writeI=
               (pMe->out_streams_ptr[ii]->CirBufList.writeI + 1) % (pMe->out_streams_ptr[ii]->CirBufList.numBufs);
      }

      elite_msg_data_buffer_t* pOutBuf = (elite_msg_data_buffer_t*) (pMe->out_streams_ptr[ii]->outDataBufferNode.pBuffer);

      last_capi->out_buf[ii].data_ptr = (int8_t *) (&(pOutBuf->nDataBuf));

      last_capi->out_buf[ii].actual_data_len = 0;

      pOutBuf->nActualSize = 0;
   }

   //filling capi input buffers
   for(ii = 0; ii < DEC_SVC_MAX_INPUT_STREAMS; ii++)
   {
      if(pMe->in_streams_ptr[ii] == NULL) continue;

      //Remaining bytes to copy from client Buffer for each input stream
      remClientBytes[ii] = &pMe->in_streams_ptr[ii]->inp_buf_params.unMemSize;

      if(NULL != remClientBytes[ii])
      {
         //If there is more data to be copied from client buffer.
         //Even if client data is not present, process has to be called to flush any remaining input data (esp. @ EoS)
         if(*remClientBytes[ii] > 0) nResult |= AudioDecSvc_FillCapiInpBuffer(pMe,ii);
      }

      //TODO : need to update byte logging for multiple input streams
      if(!ii)
      {
         //log any new input data
         int8_t * buf_addr;
         uint32_t buf_size;

         buf_addr = (int8_t *)(first_capi->in_buf[ii].data_ptr) + first_capi->bytes_logged;
         buf_size = (first_capi->in_buf[ii].actual_data_len -  first_capi->bytes_logged);

         if(buf_size)
         {
            /* Call to log the input bit-stream data to decoder service */
            AudioDecSvc_LogBitStreamData(buf_addr, buf_size, pMe->in_streams_ptr[ii]->common.ulDataLogId, first_capi->dec_fmt_id);
         }
      }

   }

   if (ADSP_SUCCEEDED(nResult))
   {
      //process over all CAPIs
      ADSPResult prevDecResult = ADSP_EOK;
      nResult = AudioDecSvc_ProcessLoop(pMe, pOutStrm, &prevDecResult);


      if ( (pMe->event_mask & AUD_DEC_SVC_EVENT__PORT_DATA_THRESH_CHANGE_MASK))
      {
         MSG( MSG_SSID_QDSP6,DBG_ERROR_PRIO,"Raising PortDataThreshChangeEvent in dec service");
         if(AudioDecSvc_HandlePortDataThreshChangeEvent(pMe) != ADSP_EOK)
         {
            return ADSP_EFAILED;
         }
         return ADSP_EOK;
      }

      //post process based on last CAPI output.
      //If result is fail, then execute the rest, but not postProcess
      if (ADSP_SUCCEEDED(nResult))
      {
         for(ii = 0; ii < DEC_SVC_MAX_OUTPUT_STREAMS; ii++)
         {
            if(pMe->out_streams_ptr[ii] != NULL)
            {
               nResult |= AudioDecSvc_PostProcess(pMe, pMe->out_streams_ptr[ii]);
            }
         }
      }

      for(ii = 0; ii < DEC_SVC_MAX_INPUT_STREAMS; ii++)
      {
         if(pMe->in_streams_ptr[ii] == NULL) continue;

         //If client buffer is empty or LPM is empty and either CAPI buf is zero size or not sufficient,
         //set data signal to listen for data
         if((0==*remClientBytes[ii]) && (pMe->in_streams_ptr[ii]->inp_buf_params.unMemSize == 0) &&
               (0 == first_capi->in_buf[ii].actual_data_len || (ADSP_EOK != prevDecResult) ||
                (pMe->in_streams_ptr[ii]->WasPrevDecResNeedMore)))
         {
            pMe->unCurrentBitfield = AUD_DEC_CMD_SIG | AUD_DEC_INP_INDEX_TO_DATA_SIG(ii);
            first_capi->inputs[ii].flags.end_of_frame = false;

            //If this was the last input buffer and we have completely processed then set the flag to propagate EOS downstream.
            //If an error occurs while processing this buffer then it is unrecoverable and so send EOS anyway.
            if(pMe->in_streams_ptr[ii]->bEndOfStream == TRUE)
            {
               //if secondary input stream has received  eos command then raise ASM_DATA_EVENT_RENDERED_EOS event to client
               //for primary or primary_secondary input stream ASM_DATA_EVENT_RENDERED_EOS will be raised by AFE.
               if(pMe->in_streams_ptr[ii]->common.stream_type == ASM_STREAM_TYPE_SECONDARY)
               {
                  elite_apr_packet_t *pAprPacket = ( elite_apr_packet_t *) (pMe->in_streams_ptr[ii]->inpDataQMsg.pPayload);
                  if(ADSP_FAILED(AudioStreamMgr_GenerateAck(pAprPacket, ADSP_EOK, NULL,  0, 0)))
                  {
                     MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "EOS ack failed for secondary input stream.");
                  }
               }
               else   pMe->need_to_send_eos = TRUE;
            }
         }
      }

      for(ii = 0; ii < DEC_SVC_MAX_OUTPUT_STREAMS; ii++)
      {
         if(pMe->out_streams_ptr[ii] == NULL) continue;

         elite_msg_data_buffer_t* pOutBuf = (elite_msg_data_buffer_t*) (pMe->out_streams_ptr[ii]->outDataBufferNode.pBuffer);
         //if decode is successful and if output buffer size is > 0,
         //deliver it to downstream, otherwise return the buffer to bufQ
         bool_t outBufNotEmpty = pOutBuf->nActualSize > 0;

         //maximum DEFAULT_NUM_INTERNAL_BUFFERS-1 borrowed buffer can be pushed downstream
         //if currently borrowing DEFAULT_NUM_INTERNAL_BUFFERS'th  buffer then return it to input Q without pushing into the downstream
         if((ADSP_EOK == nResult) && outBufNotEmpty)
         {
            //sending buffer downstream
            if(AUD_DEC_OUT_INDEX_TO_DATA_SIG(ii) & unChannelStatus)
            {
               rc |=  AudioDecSvc_SendOutBufs(pMe, pMe->out_streams_ptr[ii]);
            }
            else if(!(AUD_DEC_OUT_INDEX_TO_DATA_SIG(ii) & pMe->unCurrentBitfield))
            {
               //if output stream is disabled then discard the buffer
               if(pMe->out_streams_ptr[ii]->CirBufList.writeI == 0)
                  pMe->out_streams_ptr[ii]->CirBufList.writeI = pMe->out_streams_ptr[ii]->CirBufList.numBufs - 1;
               else (pMe->out_streams_ptr[ii]->CirBufList.writeI)--;
            }
            else
            {
               //if all buffers are filled in circular buffer then drop oldest buffer
               if(pMe->out_streams_ptr[ii]->CirBufList.writeI == pMe->out_streams_ptr[ii]->CirBufList.readI)
               {
                  pMe->out_streams_ptr[ii]->CirBufList.readI = (pMe->out_streams_ptr[ii]->CirBufList.readI + 1) % (pMe->out_streams_ptr[ii]->CirBufList.numBufs);
               }
            }
         }
         else
         {
            //Returning the output buffer to Q as it doesnt have any data to deliver downstream
#ifdef DBG_AUDIO_DECODE
            MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "No data in OP buffer. return it to Q!\n");
#endif
            if(AUD_DEC_OUT_INDEX_TO_DATA_SIG(ii) & unChannelStatus)
            {
               AudioDecSvc_ReturnOutBuf(pMe, pMe->out_streams_ptr[ii]);
               AudioDecSvc_HandleEosCases(pMe, pInpStrm, pMe->out_streams_ptr[ii], outBufNotEmpty);
            }
            else //need to decrement the writeI since not used the buffer
            {
               if(pMe->out_streams_ptr[ii]->CirBufList.writeI == 0)
                  pMe->out_streams_ptr[ii]->CirBufList.writeI = pMe->out_streams_ptr[ii]->CirBufList.numBufs - 1;
               else (pMe->out_streams_ptr[ii]->CirBufList.writeI)--;
            }
         }
      }

      return nResult | rc;
   }
   //else
   //return all the buffer to their respective Q if filling of capi input failed.
   ii =  DEC_SVC_MAX_OUTPUT_STREAMS;

   __bailout__:

   while(--ii >= 0)
   {
      if(pMe->out_streams_ptr[ii])
      {
         if(AUD_DEC_OUT_INDEX_TO_DATA_SIG(ii) & unChannelStatus)    AudioDecSvc_ReturnOutBuf(pMe, pMe->out_streams_ptr[ii]);
         else
         {
            if(pMe->out_streams_ptr[ii]->CirBufList.writeI == 0)
               pMe->out_streams_ptr[ii]->CirBufList.writeI = pMe->out_streams_ptr[ii]->CirBufList.numBufs - 1;
            else (pMe->out_streams_ptr[ii]->CirBufList.writeI)--;
         }
      }
   }
   return nResult;

}

static ADSPResult AudioDecSvc_ProcessOutputDataQ_PCMPlayback(AudioDecSvc_t *pMe, AudioDecSvc_OutStream_t *pOutStrm)
{
   ADSPResult nResult = ADSP_EOK;
   elite_msg_data_buffer_t *pOutBuf = NULL;
   uint8_t** ppucIn;

   //for PCM assumption is only one input and output (error check is made)
   AudioDecSvc_InpStream_t *pInpStrm = AudioDecSvc_GetDefaultInputStream(pMe);
   //if client buffer is empty, then release it. There is only one extern
   //output buffer, if control is reached here which means buffer is already
   //returned by downstream service and no check is required for output
   //buffer queue
   if(0 == pInpStrm->inp_buf_params.unMemSize)
   {
      if ((!pInpStrm->bEndOfStream))
      {
         //if no more data in client buffer release it
         //Dont release input data command if it is EOS, need to process EOS first
         nResult = AudioDecSvc_FreeInputDataCmd(pMe, pInpStrm, ADSP_EOK);
         pMe->unCurrentBitfield = AUD_DEC_CMD_SIG | AudioDecSvc_GetAllInputStreamMask(pMe);
         return nResult;
      }
   }


   if (NULL == pOutStrm->pDownStreamSvc)
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "AudioDecSvc: Decoder service not yet connected to Downstream");
      return nResult;
   }

   if(ADSP_FAILED(nResult = AudioDecSvc_PopOutBuf(pMe, pOutStrm)))
   {
      return nResult;
   }
   pOutBuf = (elite_msg_data_buffer_t*) (pOutStrm->outDataBufferNode.pBuffer);

   AudioDecSvc_SubtractBufDelayUsingClientToken(pOutStrm, &pOutBuf->unClientToken);
   if(AudioDecSvc_IsReallocateExternalBuffer(pMe, pOutStrm, &nResult)) { return nResult; }

   //Remaining bytes to be copied from source Buffer (Client/LPM buffer)
   uint32_t *pRemBytesInSrcBuf;
   bool_t isInvalidateCache;

   ppucIn = (uint8_t**)&(pInpStrm->inp_buf_params.unVirtAddr);
   pRemBytesInSrcBuf = &pInpStrm->inp_buf_params.unMemSize;
   isInvalidateCache = FALSE;

   uint32_t numBytesProcessed = 0;

   bool_t isChanSpacingFromOut = TRUE;
   if ((ASM_ULTRA_LOW_LATENCY_STREAM_SESSION == pInpStrm->common.perf_mode) ||
         (ASM_LOW_LATENCY_NO_PROC_STREAM_SESSION == pInpStrm->common.perf_mode))
   {
      isChanSpacingFromOut = FALSE;
   }

   nResult = AudioDecSvc_ProcessPCM(pMe, pInpStrm, pOutStrm, ppucIn, pRemBytesInSrcBuf, &numBytesProcessed, isChanSpacingFromOut, pOutBuf->nMaxSize, isInvalidateCache);

   if (ADSP_FAILED(nResult))
   {
      AudioDecSvc_ReturnOutBuf(pMe, pOutStrm);
      return nResult;
   }

   /* At this point, PCM data in output buffer is converted to output bytes per sample */

#if 0
   if(0 == *pRemBytesInSrcBuf)
   {
      if ((!pInpStrm->bEndOfStream))
      {
         //if no more data in client buffer release it
         //Dont release input data command if it is EOS, need to process EOS first
         nResult = AudioDecSvc_FreeInputDataCmd(pMe, pInpStrm, ADSP_EOK);
         pMe->unCurrentBitfield = AUD_DEC_CMD_SIG | AudioDecSvc_GetAllInputStreamMask(pMe);
      }
      else
      {
         if(0 == pInpStrm->inp_buf_params.unMemSize)
         {
            //if both LPM and client buffer are empty
            pMe->unCurrentBitfield = AUD_DEC_CMD_SIG | AudioDecSvc_GetAllInputStreamMask(pMe);
         }
      }
   }
#endif

   //update decoding result as success
   pInpStrm->WasPrevDecResNeedMore = false;

   //check if channel mapping changed
   bool_t chan_map_changed = AudioDecSvc_IsChanMapChanged(pMe->capiContainer[0]->PrevFmtBlk[0].num_channels,
         pMe->capiContainer[0]->PrevFmtBlk[0].channel_mapping,
         pInpStrm->pcmFmt.chan_map.nChannels,
         pInpStrm->pcmFmt.chan_map.nChannelMap);

   // if there is any change in fs/channels from previous frame
   // send media format to down stream before the fresh data is enqueued
   if ( (pInpStrm->pcmFmt.ulPCMSampleRate != pMe->capiContainer[0]->PrevFmtBlk[0].sample_rate)
         || chan_map_changed )
   {
      //notify client about the changes if the event is enabled
      (void) AudioDecSvc_SendSrCmEvent(pMe, pInpStrm, pInpStrm->pcmFmt.ulPCMSampleRate,
            &pInpStrm->pcmFmt.chan_map);


      //recompute output channel mapping that will be sent to downstream svc
      AudioDecSvc_GetOutChanMap(&pInpStrm->pcmFmt.chan_map, &pOutStrm->out_chan_map);

      nResult = AudioDecSvc_NotifyPeerSvcWithMediaFmtUpdate(pMe,pInpStrm, pOutStrm,
            pInpStrm->pcmFmt.ulPCMSampleRate,
            pOutStrm->out_chan_map.num_out_chan,
            pOutStrm->out_chan_map.out_chan,
            pOutStrm->output_bits_per_sample);
      if (ADSP_FAILED(nResult))
      {
         MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "AudioDecSvc:Fail to send Media Fmt update");
         AudioDecSvc_ReturnOutBuf(pMe, pOutStrm);
         return nResult;
      }

      //update prev format block
      AudioDecSvc_UpdatePrevFmtBlk(pMe,pInpStrm,
            pMe->capiContainer[0],
            pInpStrm->pcmFmt.ulPCMSampleRate,
            pInpStrm->pcmFmt.chan_map.nChannels,
            pInpStrm->pcmFmt.chan_map.nChannelMap,
            pOutStrm->output_bits_per_sample,0);
   }

   //remove initial silence
   if(pInpStrm->silence_removal.initial_samples_to_remove > 0)
   {
      AudioDecSvc_RemoveInitialSamples(&pInpStrm->silence_removal.initial_samples_to_remove,
            (uint32_t *)&pOutBuf->nActualSize,
            (uint8_t *)&(pOutBuf->nDataBuf),
            pInpStrm->pcmFmt.chan_map.nChannels,
            pInpStrm->pcmFmt.ulPCMSampleRate,
            &pInpStrm->TsState.ullNextOutBufTS,
            &pInpStrm->TsState.ullNextOutBufSampleCount,
            pOutStrm->output_bytes_per_sample);
   }

   bool_t input_buffers_empty = false;

   if((0 == pInpStrm->inp_buf_params.unMemSize) &&
         (0 == *pRemBytesInSrcBuf))  //for non LPM case both these are the same
   {
      input_buffers_empty = true;
   }

   //remove trailing silence
   if(pInpStrm->silence_removal.last_buffer)
   {
      //Currently trailing silence removal is only applicable for last frame
      //Make sure this is the last frame by seeing if all data in internal buffer has been
      //consumed and no data remains in the client buffer
      if(input_buffers_empty && pInpStrm->silence_removal.trailing_samples_to_remove > 0)
      {
         AudioDecSvc_RemoveTrailingSamples(pInpStrm->silence_removal.trailing_samples_to_remove,
               (uint32_t *)&pOutBuf->nActualSize,
               (uint8_t *)&(pOutBuf->nDataBuf),
               pInpStrm->pcmFmt.chan_map.nChannels,
               pOutStrm->output_bytes_per_sample);
      }
   }

   bool_t bIsStreamUllOrLlnp = ((ASM_ULTRA_LOW_LATENCY_STREAM_SESSION == pInpStrm->common.perf_mode) ||
                                (ASM_LOW_LATENCY_NO_PROC_STREAM_SESSION == pInpStrm->common.perf_mode));
   bool_t outBufHasDataForNonULL = (!bIsStreamUllOrLlnp) && (pOutBuf->nActualSize > 0);
   bool_t outBufIsFullForULL = (bIsStreamUllOrLlnp && ((int32_t)pOutBuf->nMaxSize == pOutBuf->nActualSize));
   bool_t eosForULL = (bIsStreamUllOrLlnp && ((pOutBuf->nActualSize) && (pInpStrm->bEndOfStream == TRUE)));

   if(outBufHasDataForNonULL || outBufIsFullForULL || eosForULL)
   {
      //in case of ULL, when APPS send shared buffers that are not 1ms multiples, numBytesProcessed will have
      //number of bytes processed to fill buffer. So this need to be reset in case of ULL to full bufffer size.
      if (outBufIsFullForULL)
      {
         numBytesProcessed = pOutBuf->nActualSize;
      }
      uint32_t num_samples_per_chan = numBytesProcessed/(pOutStrm->output_bytes_per_sample*pInpStrm->pcmFmt.chan_map.nChannels);
      //do channel downmixing if required
      AudioDecSvc_GetOutput(pMe,pOutStrm,
            pMe->capiContainer[0],
            &pInpStrm->pcmFmt.chan_map,
            (void*) &pOutBuf->nDataBuf,
            num_samples_per_chan,
            pOutStrm->output_bytes_per_sample,
            (uint32_t*)(&pOutBuf->nActualSize));

      nResult = AudioDecSvc_SendPcmToPeerSvc(pMe, pInpStrm, pOutStrm, pOutBuf);
   }
   else
   {
      if ((ASM_ULTRA_LOW_LATENCY_STREAM_SESSION != pInpStrm->common.perf_mode) &&
          (ASM_LOW_LATENCY_NO_PROC_STREAM_SESSION != pInpStrm->common.perf_mode))
      {
         //Returning the output buffer to Q as it doesnt have any data to deliver downstream
         MSG(MSG_SSID_QDSP6, DBG_LOW_PRIO, "No data in OP buffer. return it to Q!\n");
         AudioDecSvc_ReturnOutBuf(pMe, pOutStrm);
      }
      else if (!pInpStrm->bEndOfStream)
      {
         nResult = AudioDecSvc_FreeInputDataCmd(pMe, pInpStrm, ADSP_EOK);
         pMe->unCurrentBitfield = AUD_DEC_CMD_SIG | AudioDecSvc_GetAllInputStreamMask(pMe);
      }
   }


   //If this was the last input buffer and we have completely processed then propagate EOS downstream.
   //If an error occurs while processing this buffer then it is unrecoverable and so send EOS anyway.
   if((pInpStrm->bEndOfStream == TRUE) && ((pOutBuf->nActualSize == 0) || input_buffers_empty))
   {
      AudioDecSvc_ProcessEos(pMe, pInpStrm, pOutStrm, true);
      AudioDecSvc_FreeInputDataCmd(pMe, pInpStrm,  ADSP_EOK);
      //wait for an input buffer after processing EOS
      pMe->unCurrentBitfield = AUD_DEC_CMD_SIG | AudioDecSvc_GetAllInputStreamMask(pMe);
   }

   return nResult;
}

/**
 * For a given output buf, this function may be called once
 * or twice.
 *
 * for regular mode: input buf is copied to output buf after deinterleaving and converting byte len.
 *       channel spacing is min of inp size & output size.
 *       out buf is indexed starting from first in each call.
 * for pull mode: input buf is copied to output buf after deinterleaving and converting byte len.
 *       but the call may be made multiple times since the inp buf is actually circular, but this func handles only lin buf.
 *       channel spacing is always output buf size
 *       out buf is indexed from starting for the first call, and
 *       from anywhere for the subsequent calls.
 *
 *
 * ppucIn - ptr to the ptr to the buffer
 * pRemBytesInSrcBuf - ptr to the remaining bytes in the input buffer.
 * numBytesProcessed - num of bytes processed after sending to output.
 * isChanSpacingBasedOnInput -> if channel spacing is decided by number of input samples or
 *                            simply from output buf size.
 * maxUsableOutBufSize -> although out buf size is alloc to max, we need to use only correct length
 *                      (1ms or so in case of pull mode)
 * isInvalidateCache ->whether the source buffer cache needs to be invalidated.
 *
 */
ADSPResult AudioDecSvc_ProcessPCM(AudioDecSvc_t *pMe, AudioDecSvc_InpStream_t *pInpStrm, AudioDecSvc_OutStream_t *pOutStrm, uint8_t **ppucIn, uint32_t *pRemBytesInSrcBuf,
      uint32_t *numBytesProcessed, bool_t isChanSpacingBasedOnInput, uint32_t maxUsableOutBufSize, bool_t isInvalidateCache)
{
   ADSPResult nResult = ADSP_EOK;
   elite_msg_data_buffer_t *pOutBuf = NULL;
   uint8_t* pucIn;
   uint32_t bytes_per_sample_in;
   uint32_t bytes_per_sample_out;

   pOutBuf = (elite_msg_data_buffer_t*) (pOutStrm->outDataBufferNode.pBuffer);

   pucIn = *ppucIn;

   //Remaining bytes to be copied from source Buffer (Client/LPM buffer)

   uint32_t  RemBytesInSrcBuf = *pRemBytesInSrcBuf;

   bytes_per_sample_in = pInpStrm->pcmFmt.usBytesPerSample; /* PCM media format input bytes per sample */
   bytes_per_sample_out = pOutStrm->output_bytes_per_sample;

   uint32_t inp_samples_avail = RemBytesInSrcBuf / bytes_per_sample_in;

   uint32_t rem_out_buf_len = ( maxUsableOutBufSize - pOutBuf->nActualSize );
   uint32_t max_out_samples = ( rem_out_buf_len / bytes_per_sample_out );

   //samples for all channels
   uint32_t samples_to_copy = (inp_samples_avail > max_out_samples) ? max_out_samples : inp_samples_avail;

   uint32_t in_bytes_to_copy = samples_to_copy * bytes_per_sample_in;

   uint32_t num_in_channel = (pInpStrm->pcmFmt.chan_map.nChannels);
   uint32_t num_samples_per_chan = (samples_to_copy / num_in_channel);

   uint32_t num_out_bytes_per_chan = (num_samples_per_chan * bytes_per_sample_out);

   uint32_t out_chan_spacing_in_bytes = (isChanSpacingBasedOnInput) ?  num_out_bytes_per_chan : (maxUsableOutBufSize / num_in_channel);

   uint32_t byte_offset = pOutBuf->nActualSize/num_in_channel;

   int8_t *out_buf;
   int8_t *out_ptr[PCM_FORMAT_MAX_NUM_CHANNEL];

   if (isInvalidateCache)
   {
      if(ADSP_FAILED(nResult = qurt_elite_memorymap_cache_invalidate((uint32_t)pucIn, in_bytes_to_copy)))
      {
         MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "AudioDecSvc:LPM:Failure cache invalidate.");
         return nResult;
      }
   }

   /* Logging the input PCM data to Audio Decoder Module */
   AudioDecSvc_LogPcmData(pMe, pInpStrm, (int8_t *)pucIn, in_bytes_to_copy);

   //space for deinterleaving
   out_buf = ( ((int8_t *)(&pOutBuf->nDataBuf)) + byte_offset);

   for(uint32_t i = 0; i < num_in_channel; i++)
   {
      out_ptr[i] = out_buf + (i * out_chan_spacing_in_bytes) ;
   }

   // input can be in 2,3,4 byte registers and can be Q15,Q23,Q31. Output is 2 or 4 bytes and Q15 or Q27.
   //32 (Q31) -> 16 (Q15), 24 (Q23) -> 16 (Q15), 16->16
   if(2 == bytes_per_sample_out)
   {
      /* 1. First conversion to 16 bits */
      nResult = byte_convertor_intlv_16_out((int8_t *)pucIn, (int8_t *)(pMe->capiContainer[0]->scratch_out_buf), samples_to_copy, bytes_per_sample_in*8, (bytes_per_sample_in*8-1));
      if (ADSP_FAILED(nResult))
      {
         MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "AudioDecSvc:Error bytes_per_sample_in = %lu, bytes_per_sample_out = %lu, bps = %d",
               bytes_per_sample_in, bytes_per_sample_out, pInpStrm->pcmFmt.usBitsPerSample);
      }

      /* Now do de-interleaving of 16 bit */
      /* kw check: condition is applied for max number of channels */
      if(num_in_channel <= PCM_FORMAT_MAX_NUM_CHANNEL)
      {
         DeInterleave_Multich_16((int16_t *)pMe->capiContainer[0]->scratch_out_buf, (int16_t **)out_ptr,  num_in_channel, num_samples_per_chan);
      }
      else
      {
         MSG_1(MSG_SSID_QDSP6,  DBG_ERROR_PRIO, "AudioDecSvc: Error: Number of channels:[%lu]", num_in_channel);
      }
   }
   //16 -> 32 (Q27), 24 -> 32 (Q27), 32 (Q31) ->32 (Q27)
   else if ( 4 == bytes_per_sample_out )
   {
      /* first convert to 32 bits */
      nResult = byte_convertor_intlv_32_out((int8_t *)pucIn, (int8_t *)(pMe->capiContainer[0]->scratch_out_buf), samples_to_copy, bytes_per_sample_in*8, (bytes_per_sample_in*8-1));

      if (ADSP_FAILED(nResult))
      {
         MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "AudioDecSvc:Error bytes_per_sample_in = %lu, bytes_per_sample_out = %lu, bps = %d",
               bytes_per_sample_in, bytes_per_sample_out, pInpStrm->pcmFmt.usBitsPerSample);
      }

      /* kw check: condition is applied for max number of channels */
      if(num_in_channel <= PCM_FORMAT_MAX_NUM_CHANNEL)
      {
         DeInterleave_Multich_32((int32_t *)pMe->capiContainer[0]->scratch_out_buf, (int32_t **)out_ptr, num_in_channel, num_samples_per_chan);
      }
      else
      {
         MSG_1(MSG_SSID_QDSP6,  DBG_ERROR_PRIO, "AudioDecSvc: Error: Number of channels:[%lu]", num_in_channel);
      }
   }
   else
   {
      MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "AudioDecSvc:Error bytes_per_sample_in = %lu, bytes_per_sample_out = %lu", bytes_per_sample_in, bytes_per_sample_out);
   }

   /* At this point, PCM data in output buffer is converted to output bytes per sample */

   uint32_t new_bytes = samples_to_copy * bytes_per_sample_out;
   *numBytesProcessed = new_bytes;
   //Get the output size
   pOutBuf->nActualSize += new_bytes;

   //update input buffer remaining bytes and byte pointers
   *pRemBytesInSrcBuf -= in_bytes_to_copy;
   *ppucIn += in_bytes_to_copy;

   return nResult;
}

/* =======================================================================
 **                          Message handler functions
 ** ======================================================================= */

//#define AUD_DEC_SVC_CAPI_IO_SIZE_LOG

static ADSPResult AudioDecSvc_Process(AudioDecSvc_t *pMe, uint8_t capi_index)
{
   dec_CAPI_container_t *capi_container = pMe->capiContainer[capi_index];
   AudioDecSvc_InpStream_t * pInpStrm = AudioDecSvc_GetDefaultInputStream(pMe);
   //# bytes in CAPI buffer before decoding
   uint32_t inpSizeBefore[DEC_SVC_MAX_INPUT_STREAMS] = {0};
   uint32_t remainingBytesAfter[DEC_SVC_MAX_INPUT_STREAMS] = {0};

   /* Call the decoder CAPI */
   for (uint16_t input_stream_index = 0; input_stream_index<DEC_SVC_MAX_INPUT_STREAMS; input_stream_index++)
   {
      //having a nonNULL pointer implies that this CAPI has this input port
      if (NULL != capi_container->in_buf[input_stream_index].data_ptr)
      {
         inpSizeBefore[input_stream_index] = capi_container->in_buf[input_stream_index].actual_data_len;

#ifdef AUD_DEC_SVC_CAPI_IO_SIZE_LOG
         MSG_5(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Dec %lx CAPI %u process before: inLengh %lu, outLen[0] %lu for capi input port %u\n", pInpStrm->common.ulDataLogId,
               capi_index, inpSizeBefore[input_stream_index],
               capi_container->out_buf[0].actual_data_len, input_stream_index);

         uint8_t *data_ptr = (uint8_t*)capi_container->in_buf[input_stream_index].data_ptr;
         MSG_9(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Dec %lx CAPI %u: bytes in: %x %x %x %x  %x %x %x  ", pInpStrm->common.ulDataLogId,
               capi_index, *(data_ptr), *(data_ptr+1),*(data_ptr+2), *(data_ptr+3),
               *(data_ptr+4), *(data_ptr+5),*(data_ptr+6));

         MSG_4(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Dec %lx CAPI %u: EoF %lu, EoS %lu", pInpStrm->common.ulDataLogId,
               capi_index, capi_container->inputs[input_stream_index].flags.end_of_frame, capi_container->inputs[input_stream_index].flags.marker_eos);
#endif
      }
   }

   /**
    * At input of process, the input->actual_len is the size of input & data starts from data_ptr.
    *                      the output->actual_len is uninitialized & CAPI can write from data_ptr.
    * At output of process, the input->actual_len is the amount of data consumed (read) by CAPI. remaining data is from data_ptr+actual_len
    *                      the output->actual_len is output data, & data starts from data_ptr.
    */
   capi_v2_err_t nResult = capi_container->capi_ptr->vtbl_ptr->process(capi_container->capi_ptr, capi_container->inputs_ptr, capi_container->outputs_ptr);

   for(uint8_t ii = 0 ; ii < DEC_SVC_MAX_INPUT_STREAMS ; ii++)
   {
      if(capi_container->in_buf[ii].data_ptr)
      {
#ifdef AUD_DEC_SVC_CAPI_IO_SIZE_LOG
         MSG_6(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Dec %lx CAPI %u process after: nResult 0x%x, inLengh consumed %lu, outLen[0] %lu for capi input port %u\n",
               pInpStrm->common.ulDataLogId, capi_index,
               nResult, capi_container->in_buf[ii].actual_data_len,
               capi_container->out_buf[0].actual_data_len,ii);
#endif
         //error checks
         if((capi_container->in_buf[ii].actual_data_len > inpSizeBefore[ii]))
         {
            //The number of remaining bytes are negative or greater than valid number of bytes
            //at the beginning of decode. This indicates that decoding did not happen correctly
            //and decoder might be in a bad state
            MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
                  "Invalid # of available input bytes after CAPI process call at input port %u : Available bytes before = %lu, Consumed bytes after = %lu",
                  ii, inpSizeBefore[ii], capi_container->in_buf[ii].actual_data_len);

            //set the result to DEC_RECOVERY_FAILURE which will discard the contents of the
            //current buffer
            nResult = CAPI_V2_EFAILED;

            //reset the decoder in order to avoid undefined behavior with future buffers
            aud_dec_svc_algorithmic_reset(capi_container->capi_ptr);

            break ;
         }
         else
         {
            remainingBytesAfter[ii] = inpSizeBefore[ii] - capi_container->in_buf[ii].actual_data_len;
         }
      }
   }

   pInpStrm->WasPrevDecResNeedMore = false;
   if (CAPI_V2_SUCCEEDED(nResult) || (nResult==CAPI_V2_ENEEDMORE))
   {
      /* this condition handles
         success - some input consumed (or not), some out produced
         need_more - zero or all the input consumed (in either case we need to look into o/p buf), no output produced
         recovery_success - some in consumed, no output produced
       */

      //need more is detected whenever,
      //a) decode result is success or need_more
      //b) no output is produced in each of the outputs
      //c) no input is consumed from stream containing primary
      //when dec says need_more, one frame is not over yet, and we cannot sync to new buf TS, as the TS are synced to frames.
      //in rec success cases, there might be more frames in the buf (hence we won't even look into inp buf q until need-more is returned).
      bool_t out_produced = true, was_need_more = false;

      //error out if some outputs produce while others not. (all outputs must be in sync)
      for(uint8_t ii = 0 ; ii < DEC_SVC_MAX_OUTPUT_STREAMS ; ii++)
      {
         if(capi_container->out_buf[ii].data_ptr)
         {
            //if prev outputs didn't produce and this one produces, then it's an error.
            if (!out_produced && (capi_container->out_buf[ii].actual_data_len != 0))
            {
               MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
                     "All outputs must be synced, but out %u has output while others don't",ii);
               return ADSP_EFAILED;
            }

            if (capi_container->out_buf[ii].actual_data_len != 0)
            {
               out_produced = true;
            }
            else
            {
               out_produced = false;
            }
         }
      }

      for(uint8_t ii = 0 ; ii < DEC_SVC_MAX_INPUT_STREAMS ; ii++)
      {
         if(capi_container->in_buf[ii].data_ptr)
         {
            if ((remainingBytesAfter[ii] == 0) ||
                  (remainingBytesAfter[ii] == inpSizeBefore[ii])) //if in didn't get consumed
            {
               if (!out_produced) //no output produced
               {
                  pMe->in_streams_ptr[ii]->WasPrevDecResNeedMore = true;
                  was_need_more = true;
               }
            }
         }
      }

      if ( (capi_index != 0) && (was_need_more))
      {
         MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "need more cannot be returned except by first decoder");
         //as first decoder must generate enough data for the later ones. If this happens, we still continue and it may work without TS, but with TS
         //it may not work. Also, if this is due to buffering in the CAPI, then there's no way to get those buffers back later as no additional calls will be made to the
         //CAPIs which buffer. process of additional CAPIs is called only if first one generates an output.
      }

      if (!was_need_more)
      {
         if(capi_container == pMe->mainCapiContainer)
         {
            AudioDecSvc_DecErrorEvent_Clear(&pInpStrm->dec_err_event);

#ifdef DBG_AUDIO_DECODE
            for(uint8_t ii = 0 ; ii < DEC_SVC_MAX_INPUT_STREAMS ; ii++)
            {
               if(capi_container->in_buf[ii].data_ptr)
               {
                  MSG_2(MSG_SSID_QDSP6,  DBG_HIGH_PRIO, "Decoding consumed(%d) input bytes at capi input port %d",
                        capi_container->in_buf[ii].actual_data_len,(int)ii );
               }
            }
#endif
         }
      }
      nResult = CAPI_V2_EOK; //change needmore to EOK
   }
   else //CAPI failed
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Decoding failed");

      if(capi_container == pMe->mainCapiContainer)
      {
         AudioDecSvc_DecErrorEvent_RecordError (&pInpStrm->dec_err_event);
      }
   }

   if (pMe->mainCapiContainer == capi_container) AudioDecSvc_DecErrorEvent_CheckRaise(pMe, pInpStrm, &pInpStrm->dec_err_event);


   for(uint8_t ii = 0 ; (ii < DEC_SVC_MAX_INPUT_STREAMS) &&  (capi_container->in_buf[ii].data_ptr != NULL) ; ii++)
   {
      //Get the consumed input length
      int32_t nBytesUsed = capi_container->in_buf[ii].actual_data_len;

      memsmove(capi_container->in_buf[ii].data_ptr,
            remainingBytesAfter[ii],
            capi_container->in_buf[ii].data_ptr + nBytesUsed,
            remainingBytesAfter[ii] );
      capi_container->in_buf[ii].actual_data_len = remainingBytesAfter[ii];
   }


   return capi_v2_err_to_adsp_result(nResult);
}

static ADSPResult AudioDecSvc_PostProcess(AudioDecSvc_t *pMe, AudioDecSvc_OutStream_t *pOutStrm)
{
   dec_CAPI_container_t *last_capi = AudioDecSvc_GetLastCapi(pMe);
   AudioDecSvc_InpStream_t * pInpStrm = AudioDecSvc_GetDefaultInputStream(pMe);

   uint8_t capi_out_index = AudioDecSvc_GetOutputStreamIndex(pMe, pOutStrm);
   if(NULL == last_capi)
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "AudioDecSvc:Failed to get last capi");
      return ADSP_EFAILED;
   }

   //if input is 16 bits and output is 24 bits need to shift
   if((16 == last_capi->PrevFmtBlk[capi_out_index].bits_per_sample) && (24 == pOutStrm->output_bits_per_sample))
   {
      /* 16 -> 24 bit conversion if required */
      uint32_t num_channels = last_capi->PrevFmtBlk[capi_out_index].num_channels;
      uint32_t num_samples_per_chan = (last_capi->out_buf[capi_out_index].actual_data_len) /
            (num_channels * BYTES_PER_SAMPLE_TWO);
      int8_t * final_dst_ptr = (int8_t *)last_capi->out_buf[capi_out_index].data_ptr;

      memscpy(last_capi->scratch_out_buf, last_capi->out_buf[capi_out_index].max_data_len,
            final_dst_ptr, last_capi->out_buf[capi_out_index].actual_data_len);

      /* Q15->Q28 conversion */
      byte_up_down_convertor_deintlv((int8_t *)last_capi->scratch_out_buf, final_dst_ptr, num_samples_per_chan, num_channels,
            num_samples_per_chan, num_samples_per_chan, BYTE_UP_CONV);

      /* Double the out size in bytes */
      last_capi->out_buf[capi_out_index].actual_data_len <<= 1;
   }

   DecChannelMap_t channel_cfg_after;
   channel_cfg_after.nChannels = last_capi->PrevFmtBlk[capi_out_index].num_channels;
   for (uint16_t ch=0; ch<channel_cfg_after.nChannels; ch++)
   {
      channel_cfg_after.nChannelMap[ch] = last_capi->PrevFmtBlk[capi_out_index].channel_mapping[ch];
   }

   if ( (last_capi->media_fmt_event[capi_out_index]))
   {
      // if there is any change in fs/channel mapping from
      // previous frame send media format to down stream before the
      // fresh data is enqueued
      //notify client about the changes if the event is enabled
      //TODO : Do we need to send srcm event to each input stream for each output stream format change?
      (void) AudioDecSvc_SendSrCmEvent(pMe,pInpStrm,
            last_capi->PrevFmtBlk[capi_out_index].sample_rate,
            &channel_cfg_after);

      //recompute output channel mapping that will be sent to downstream svc
      AudioDecSvc_GetOutChanMap(&channel_cfg_after, &pOutStrm->out_chan_map);

      AudioDecSvc_NotifyPeerSvcWithMediaFmtUpdate(pMe,pInpStrm, pOutStrm,
            last_capi->PrevFmtBlk[capi_out_index].sample_rate,
            pOutStrm->out_chan_map.num_out_chan,
            pOutStrm->out_chan_map.out_chan,
            pOutStrm->output_bits_per_sample);

      //clear the event
      last_capi->media_fmt_event[capi_out_index] = false;
   }

   //check if we need to remove initial/trailing silence
   //TODO : mimo case handling.
   if(pInpStrm->silence_removal.initial_samples_to_remove > 0)
   {
      AudioDecSvc_RemoveInitialSamples(&pInpStrm->silence_removal.initial_samples_to_remove,
            (uint32_t *)&last_capi->out_buf[capi_out_index].actual_data_len,
            (uint8_t *)last_capi->out_buf[capi_out_index].data_ptr,
            last_capi->PrevFmtBlk[capi_out_index].num_channels,
            last_capi->PrevFmtBlk[capi_out_index].sample_rate,
            &pInpStrm->TsState.ullNextOutBufTS,
            &pInpStrm->TsState.ullNextOutBufSampleCount,
            pOutStrm->output_bytes_per_sample);
   }

   if(pInpStrm->silence_removal.last_buffer)
   {
      //Currently trailing silence removal is only applicable for last frame
      //Make sure this is the last frame by seeing if all data in internal buffer has been
      //consumed and no data remains in the client buffer. Also if we have consumed all data
      //in client buffer and run into error treat this as last frame.
      if((last_capi->in_buf[AudioDecSvc_GetInputStreamIndex(pMe,pInpStrm)].actual_data_len == 0)  &&
            (pInpStrm->inp_buf_params.unMemSize == 0) &&
            (pInpStrm->silence_removal.trailing_samples_to_remove > 0))
      {
         AudioDecSvc_RemoveTrailingSamples(pInpStrm->silence_removal.trailing_samples_to_remove,
               (uint32_t *)&last_capi->out_buf[capi_out_index].actual_data_len,
               (uint8_t *)last_capi->out_buf[capi_out_index].data_ptr,
               last_capi->PrevFmtBlk[capi_out_index].num_channels,
               pOutStrm->output_bytes_per_sample);
      }
   }

   uint32_t samples_per_ch =
         last_capi->out_buf[capi_out_index].actual_data_len/(last_capi->PrevFmtBlk[capi_out_index].num_channels * pOutStrm->output_bytes_per_sample);

   uint32_t* pOutBufActualSize = (uint32_t *) &(((elite_msg_data_buffer_t*)(pOutStrm->outDataBufferNode.pBuffer))->nActualSize);

   AudioDecSvc_GetOutput(pMe,
         pOutStrm,
         last_capi,
         &channel_cfg_after,
         (void*) last_capi->out_buf[capi_out_index].data_ptr,
         samples_per_ch,
         pOutStrm->output_bytes_per_sample,
         pOutBufActualSize);

   return ADSP_EOK;
}


static void AudioDecSvc_HandleEosCases(AudioDecSvc_t *pMe, AudioDecSvc_InpStream_t *pInpStrm, AudioDecSvc_OutStream_t *pOutStrm, bool_t outBufNotEmpty)
{
   if(pMe->capiContainer[0]->dec_fmt_id == ASM_MEDIA_FMT_DTMF)
   {
      pMe->unCurrentBitfield = AUD_DEC_CMD_SIG | AudioDecSvc_GetAllOutputStreamMask(pMe);

      uint32_t bDtmfRunning=0;
      aud_dec_svc_get_param(pMe->capiContainer[0]->capi_ptr, CAPI_PARAM_DTMF_RUNNING, (int8_t*)&bDtmfRunning, sizeof(bDtmfRunning));

      // This condition will be true if the tone is completely played out.
      if((FALSE == bDtmfRunning) && (outBufNotEmpty))
      {
         //send EOS
         AudioDecSvc_SendDtmfToneEndedEvent(pMe);
      }
   }

   //If this was the last input buffer and we have completely processed then propagte EOS downstream.
   //If an error occurs while processing this buffer then it is unrecoverable and so send EOS anyway.
   //There must be not valid buffer in circular list
   if(pMe->need_to_send_eos && (pOutStrm->CirBufList.readI == pOutStrm->CirBufList.writeI))
   {
      pOutStrm->metadata_xfr.need_to_send_eos = TRUE;

      //set unCurrentBitfield to those output stream for which "need_to_send_eos" flag is not true.
      pMe->unCurrentBitfield = AUD_DEC_CMD_SIG;
      for(uint8_t ii = 0 ; ii < DEC_SVC_MAX_OUTPUT_STREAMS ; ii++)
      {
         if(NULL != pMe->out_streams_ptr[ii])
         {
            if(pMe->out_streams_ptr[ii]->metadata_xfr.need_to_send_eos == FALSE)
            {
               //disable the output signals for which "need_to_send_eos" is already true.
               pMe->unCurrentBitfield |= AUD_DEC_OUT_INDEX_TO_DATA_SIG(ii);
            }
         }
      }

      if(AUD_DEC_CMD_SIG == pMe->unCurrentBitfield) //last output stream to send eos
      {
         // send eos to peer service for last output stream and reset decoder
         // reset decoder function will also reset "need_to_send_eos" flag to false for all output stream.
         AudioDecSvc_ProcessEos(pMe, pInpStrm, pOutStrm, true);
         AudioDecSvc_FreeInputDataCmd(pMe, pInpStrm,  ADSP_EOK);
         //EOS is processed for all output stream; wait on input queue
         pMe->unCurrentBitfield = AUD_DEC_CMD_SIG | AudioDecSvc_GetAllInputStreamMask(pMe);
      }
      else AudioDecSvc_ProcessEos(pMe, pInpStrm, pOutStrm, false); // send eos to peer service for current output stream
   }
}

static ADSPResult AudioDecSvc_SendOutBufs(AudioDecSvc_t* pMe, AudioDecSvc_OutStream_t *pOutStrm)
{
   ADSPResult nResult = ADSP_EOK;

   AudioDecSvc_InpStream_t *pInpStrm = AudioDecSvc_GetDefaultInputStream(pMe); //TODO: this won't work for MIMO

   /* meta data */
   //if meta-data has to be sent and there's an output queue proceed, else return & wait for output buf.
   //check if CAPI has some meta-data to send now for this output port.
   if (pMe->mainCapiContainer->metadata_available_event[AudioDecSvc_GetOutputStreamIndex(pMe, pOutStrm)])
   {
      if ( 0 != qurt_elite_channel_poll(&(pMe->channel), AudioDecSvc_GetAllOutputStreamMask(pMe)) )
      {
         AudioDecSvc_SendMetadata(pMe, pOutStrm);
      }
      else
      {
         //wait for output buffer, EoS will be sent afterwards, after sending metadata & out buf.
         pOutStrm->metadata_xfr.curr_wait_mask_backup = pMe->unCurrentBitfield;
         pOutStrm->metadata_xfr.is_waiting_to_push_metadata = TRUE;
         pMe->unCurrentBitfield = AUD_DEC_CMD_SIG | AudioDecSvc_GetAllOutputStreamMask(pMe);
         return nResult;
      }
   }

   /* PCM */
   elite_msg_data_buffer_t *pOutBuf = (elite_msg_data_buffer_t*) (pOutStrm->outDataBufferNode.pBuffer);
   bool_t outBufNotEmpty =  pOutBuf->nActualSize > 0;

   nResult = AudioDecSvc_SendPcmToPeerSvc(pMe, pInpStrm, pOutStrm, pOutBuf);

   if (pOutStrm->metadata_xfr.is_waiting_to_push_metadata)
   {
      //restore the mask
      pMe->unCurrentBitfield = pOutStrm->metadata_xfr.curr_wait_mask_backup;
      pOutStrm->metadata_xfr.is_waiting_to_push_metadata = FALSE;
   }

   /* EoS */
   AudioDecSvc_HandleEosCases(pMe, pInpStrm, pOutStrm, outBufNotEmpty);

   return nResult;
}

static void AudioDecSvc_SendMetadata(AudioDecSvc_t* pMe, AudioDecSvc_OutStream_t *pOutStrm)
{
   int metadata_size = 0;
   ADSPResult nResult;
   uint32_t buf_size;
   elite_msg_data_set_param_t *pMsgHeader;
   uint8_t *ptr;
   elite_msg_any_t *msg;

   qurt_elite_bufmgr_node_t outBufMgrNode;
   // Take next buffer off the output buf q
   if(ADSP_FAILED(nResult = qurt_elite_queue_pop_front(pOutStrm->pOutBufQ, (uint64_t*)&(outBufMgrNode))))
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "AudioDecSvc:PBQ:Failure on Out Buf Pop. Dropping metadata. ");
      goto __bailout;
   }

   // Create a set_param message using the output buffer mgr node.
   msg = elite_msg_convt_buf_node_to_msg(
         &outBufMgrNode,
         ELITE_DATA_SET_PARAM,
         NULL, /* do not need response */
         0,    /* token */
         0     /* do not care response result*/
   );

   //elite_msg_data_set_param_t is followed by many groups of { asm_stream_param_data_t & value }
   ptr = (uint8_t*)msg->pPayload;

   pMsgHeader = (elite_msg_data_set_param_t*)ptr;
   buf_size = pOutStrm->maxExtBufSize;

   ptr += sizeof(elite_msg_data_set_param_t);

   //ask CAPI to populate metadata into the ptr
   nResult = aud_dec_svc_get_metadata(pMe->mainCapiContainer->capi_ptr,
         AudioDecSvc_GetOutputStreamIndex(pMe, pOutStrm),
         ptr, &buf_size);
   if (ADSP_FAILED(nResult))
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "AudioDecSvc: Dropping metadata due to error in getparam. ");
      goto __bailout1;
   }

   if (buf_size == 0)
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "AudioDecSvc: metadata size is zero! ");
      goto __bailout1;
   }

   pMsgHeader->unPayloadSize = buf_size;

   if (ADSP_FAILED(qurt_elite_queue_push_back(pOutStrm->pDownStreamSvc->dataQ, (uint64_t*)msg )))
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Fail to send data set param message. Dropping metadata.");
      goto __bailout1;
   }
   else
   {
      MSG_1(MSG_SSID_QDSP6, DBG_LOW_PRIO, "Sent data set param to PP with metadata size = %d.", metadata_size);
   }

   pMe->mainCapiContainer->metadata_available_event[AudioDecSvc_GetOutputStreamIndex(pMe, pOutStrm)] = false;

   return;

   __bailout1:
   (void) elite_msg_push_payload_to_returnq(pOutStrm->pOutBufQ, (elite_msg_any_payload_t*)(outBufMgrNode.pBuffer));
   __bailout:
   pMe->mainCapiContainer->metadata_available_event[AudioDecSvc_GetOutputStreamIndex(pMe, pOutStrm)] = false;

   return;
}

#ifdef ELITE_CAPI_H
#error "Do not include CAPI V1 in this file"
#endif
