/** @file AudioDecSvc_CapiV2CallbackHandler.cpp
This file contains functions for Elite Decoder service.

Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government, Diversion contrary to U.S. law prohibited.
 *//*====================================================================== */

/*========================================================================
Edit History

$Header: //components/rel/avs.adsp/2.7.1.c4/aud/services/dynamic_svcs/audio_dec_svc/src/AudioDecSvc_CapiV2CallbackHandler.cpp#2 $


when       who     what, where, why
--------   ---     -------------------------------------------------------
02/05/14    rbhatnk      Created file.

========================================================================== */

#include "AudioDecSvc_CapiV2CallbackHandler.h"
#include "AudioDecSvc_Util.h"
#include "EliteMsg_Custom.h"
#include "AudioDecSvc_Structs.h"

static capi_v2_err_t audio_dec_svc_capi_v2_callback(void *context_ptr, capi_v2_event_id_t id, capi_v2_event_info_t *event_info_ptr)
{
   capi_v2_err_t result = CAPI_V2_EOK;

   capi_v2_buf_t *payload = &event_info_ptr->payload;

   dec_CAPI_callback_obj_t *cb_obj = reinterpret_cast<dec_CAPI_callback_obj_t*>(context_ptr);
   AudioDecSvc_OutStream_t *pOutStrm = NULL;

   AudioDecSvc_t *pMe = cb_obj->pMe;
   if (!( (cb_obj->capi_index < DEC_SVC_MAX_CAPI) && (NULL != pMe->capiContainer[cb_obj->capi_index])))
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Error in callback function. Data possibly corrupted.");
      return CAPI_V2_EFAILED;
   }

   dec_CAPI_container_t *capi_container = pMe->capiContainer[cb_obj->capi_index];
   if (capi_container == AudioDecSvc_GetLastCapi(pMe))
   {
      pOutStrm = pMe->out_streams_ptr[0];
   }

   if (payload->actual_data_len > payload->max_data_len)
   {
      MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, " Error in callback function. The actual size %lu is greater than the max size %lu for id %lu.",
            payload->actual_data_len, payload->max_data_len, static_cast<uint32_t>(id));
      return CAPI_V2_EBADPARAM;
   }

   switch(id)
   {
      case CAPI_V2_EVENT_KPPS:
      {
         if (payload->actual_data_len < sizeof(capi_v2_event_KPPS_t))
         {
            MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, " Error in callback function. The actual size %lu is less than the required size %zu for id %lu.",
                  payload->actual_data_len, sizeof(capi_v2_event_process_state_t), static_cast<uint32_t>(id));
            return CAPI_V2_ENEEDMORE;
         }

         capi_v2_event_KPPS_t *kpps = reinterpret_cast<capi_v2_event_KPPS_t*>(payload->data_ptr);

         MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, " Module KPPS  %lu", static_cast<uint32_t>(kpps->KPPS));

         if (capi_container->kpps != kpps->KPPS)
         {
            capi_container->kpps = kpps->KPPS;
            pMe->event_mask = (pMe->event_mask | AUD_DEC_SVC_EVENT__KPPS_MASK);
         }

         return CAPI_V2_EOK;
      }
      case CAPI_V2_EVENT_BANDWIDTH:
      {
         if (payload->actual_data_len < sizeof(capi_v2_event_bandwidth_t))
         {
            MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, " Error in callback function. The actual size %lu is less than the required size %zu for id %lu.",
                  payload->actual_data_len, sizeof(capi_v2_event_process_state_t), static_cast<uint32_t>(id));
            return CAPI_V2_ENEEDMORE;
         }

         capi_v2_event_bandwidth_t *bw = reinterpret_cast<capi_v2_event_bandwidth_t*>(payload->data_ptr);
         MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, " Module BW (code,data) %lu %lu.", bw->code_bandwidth, bw->data_bandwidth);

         if (capi_container->code_bw != bw->code_bandwidth)
         {
            capi_container->code_bw = bw->code_bandwidth;
            pMe->event_mask = (pMe->event_mask | AUD_DEC_SVC_EVENT__BW_MASK);
         }
         if (capi_container->data_bw != bw->data_bandwidth)
         {
            capi_container->data_bw = bw->data_bandwidth;
            pMe->event_mask = (pMe->event_mask | AUD_DEC_SVC_EVENT__BW_MASK);
         }
         return CAPI_V2_EOK;
      }
      case CAPI_V2_EVENT_PROCESS_STATE:
      {
         if (payload->actual_data_len < sizeof(capi_v2_event_process_state_t))
         {
            MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, " Error in callback function. The actual size %lu is less than the required size %zu for id %lu.",
                  payload->actual_data_len, sizeof(capi_v2_event_process_state_t), static_cast<uint32_t>(id));
            return CAPI_V2_ENEEDMORE;
         }
         capi_v2_event_process_state_t *pData = (capi_v2_event_process_state_t*)payload->data_ptr;
         MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, " Module process check set to %lu", static_cast<uint32_t>(pData->is_enabled));

         return CAPI_V2_EOK;
      }
      case CAPI_V2_EVENT_OUTPUT_MEDIA_FORMAT_UPDATED:
      {
         if (payload->actual_data_len < sizeof(capi_v2_set_get_media_format_t))
         {
            MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, " Error in callback function. The actual size %lu is less than the required size %zu for id %lu.",
                  payload->actual_data_len, sizeof(capi_v2_set_get_media_format_t), static_cast<uint32_t>(id));
            return CAPI_V2_ENEEDMORE;
         }

         capi_v2_set_get_media_format_t *pData = reinterpret_cast<capi_v2_set_get_media_format_t*>(payload->data_ptr);

         switch(pData->format_header.data_format)
         {
            case CAPI_V2_FIXED_POINT:
            case CAPI_V2_IEC61937_PACKETIZED:
            case  CAPI_V2_GENERIC_COMPRESSED:
            {
               if (payload->actual_data_len < (sizeof(capi_v2_set_get_media_format_t)+sizeof(capi_v2_standard_data_format_t)))
               {
                  MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, " Error in callback function. The actual size %lu is less than the required size for id %lu.",
                        payload->actual_data_len, static_cast<uint32_t>(id));
                  return CAPI_V2_ENEEDMORE;
               }

               if (!event_info_ptr->port_info.is_valid || event_info_ptr->port_info.is_input_port)
               {
                  MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, " Error in callback function. port is not valid or is an input port.");
                  return CAPI_V2_EBADPARAM;
               }

               dec_CAPI_container_t *capi_cont = pMe->capiContainer[cb_obj->capi_index];
               elite_multi_channel_pcm_fmt_blk_t *prev_fmt = &(capi_cont->PrevFmtBlk[event_info_ptr->port_info.port_index]);

               capi_v2_standard_data_format_t *pPcmData = (capi_v2_standard_data_format_t*) (payload->data_ptr+sizeof(capi_v2_set_get_media_format_t));

               //no need to raise event for bps change because the decoder always outputs at the bps needed by the PP (set during asm open)
               prev_fmt->bits_per_sample = pPcmData->bits_per_sample;
               //For compressed pass-through, since there are no options to set bits per sample at stream open, have to set it here if needed
               if (CAPI_V2_GENERIC_COMPRESSED == pData->format_header.data_format)
               {
                  if (NULL != pOutStrm && pOutStrm->output_bits_per_sample != pPcmData->bits_per_sample)
                  {
                     MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "AudioDecSvc: updating output media format for compressed pass-through");
                     pOutStrm->output_bits_per_sample = pPcmData->bits_per_sample;
                     pOutStrm->output_bytes_per_sample = (pPcmData->bits_per_sample > 16) ? BYTES_PER_SAMPLE_FOUR : BYTES_PER_SAMPLE_TWO;
                  }
               }

               if (prev_fmt->num_channels != pPcmData->num_channels)
               {
                  prev_fmt->num_channels = pPcmData->num_channels;
                  capi_cont->media_fmt_event[event_info_ptr->port_info.port_index] = true;
               }
               if (prev_fmt->sample_rate != pPcmData->sampling_rate)
               {
                  prev_fmt->sample_rate = pPcmData->sampling_rate;
                  capi_cont->media_fmt_event[event_info_ptr->port_info.port_index] = true;
               }

               for (uint32_t i=0;((i<sizeof(prev_fmt->channel_mapping)) && (i<pPcmData->num_channels)); i++)
               {
                  if (prev_fmt->channel_mapping[i] != pPcmData->channel_type[i])
                  {
                     prev_fmt->channel_mapping[i] = pPcmData->channel_type[i];
                     capi_cont->media_fmt_event[event_info_ptr->port_info.port_index] = true;
                  }
               }
               if (pData->format_header.data_format == CAPI_V2_FIXED_POINT)
               {
                  if (pPcmData->data_interleaving != CAPI_V2_DEINTERLEAVED_PACKED)
                  {
                     MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, " Unsupported data interleaving %u for fixed point data ", pPcmData->data_interleaving);
                  }
               }
               break;
            }
            case CAPI_V2_RAW_COMPRESSED:
            {
               //ignore
               break;
            }
            default:
               MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, " Error in callback function. Changing media type not supported for format %d",
                     pData->format_header.data_format);
               return CAPI_V2_EUNSUPPORTED;
         }

         MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO, " Module output format updated.");

         return CAPI_V2_EOK;
      }
      case CAPI_V2_EVENT_PORT_DATA_THRESHOLD_CHANGE:
      {
         if (payload->actual_data_len < sizeof(capi_v2_port_data_threshold_change_t))
         {
            MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, " Error in callback function. The actual size %lu is less than the required size %zu for id %lu.", payload->actual_data_len, sizeof(capi_v2_port_data_threshold_change_t), static_cast<uint32_t>(id));
            return CAPI_V2_ENEEDMORE;
         }

         if (!event_info_ptr->port_info.is_valid )
         {
            MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, " Error in callback function. port is not valid ");
            return CAPI_V2_EBADPARAM;
         }

         capi_v2_port_data_threshold_change_t *pData = reinterpret_cast<capi_v2_port_data_threshold_change_t*>(payload->data_ptr);
         if (event_info_ptr->port_info.is_input_port)
         {
            if (event_info_ptr->port_info.port_index >= DEC_SVC_MAX_INPUT_STREAMS)
            {
               MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, " Input port %lu more than supported value", event_info_ptr->port_info.port_index);
               return CAPI_V2_EFAILED;
            }
            capi_container->in_port_event_new_size[event_info_ptr->port_info.port_index] = pData->new_threshold_in_bytes;
            pMe->event_mask |= AUD_DEC_SVC_EVENT__PORT_DATA_THRESH_CHANGE_MASK;

            MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, " Input port %lu event with new buf len = %lu.", event_info_ptr->port_info.port_index, pData->new_threshold_in_bytes);
         }
         else
         {
            if (event_info_ptr->port_info.port_index >= DEC_SVC_MAX_OUTPUT_STREAMS)
            {
               MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, " Output port %lu more than supported value", event_info_ptr->port_info.port_index);
               return CAPI_V2_EFAILED;
            }

            capi_container->out_port_event_new_size[event_info_ptr->port_info.port_index] = pData->new_threshold_in_bytes;
            pMe->event_mask |= AUD_DEC_SVC_EVENT__PORT_DATA_THRESH_CHANGE_MASK;

            MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, " Output port %lu event with new buf len = %lu.", event_info_ptr->port_info.port_index, pData->new_threshold_in_bytes);
         }
         MSG(MSG_SSID_QDSP6, DBG_LOW_PRIO, " Module port data threshold update.");

         return CAPI_V2_EOK;
      }
      case CAPI_V2_EVENT_METADATA_AVAILABLE:
      {

         if (!event_info_ptr->port_info.is_valid || event_info_ptr->port_info.is_input_port)
         {
            MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, " Error in callback function. port is not valid or is an input port.");
            return CAPI_V2_EBADPARAM;
         }

         if (event_info_ptr->port_info.port_index >= DEC_SVC_MAX_OUTPUT_STREAMS)
         {
            MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, " Output port %lu more than supported value", event_info_ptr->port_info.port_index);
            return CAPI_V2_EFAILED;
         }

         capi_container->metadata_available_event[event_info_ptr->port_info.port_index] = true;
         pMe->event_mask |= AUD_DEC_SVC_EVENT__METADATA_AVAILABLE_MASK;

         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, " Output port %lu metadata_available event", event_info_ptr->port_info.port_index);
         break;
      }
      default:
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, " Error in callback function. ID %lu not supported.", static_cast<uint32_t>(id));
         return CAPI_V2_EUNSUPPORTED;
      }
   }
   return result;
}

capi_v2_event_callback_info_t audio_dec_svc_get_capi_v2_callback_handler(AudioDecSvc_t *pMe, uint32_t capi_index)
{
   capi_v2_event_callback_info_t cb_info;
   cb_info.event_cb = audio_dec_svc_capi_v2_callback;

   pMe->capiContainer[capi_index]->capi_callback_obj.capi_index = capi_index;
   pMe->capiContainer[capi_index]->capi_callback_obj.pMe = pMe;

   cb_info.event_context = (void*)&pMe->capiContainer[capi_index]->capi_callback_obj;

   return cb_info;
}

#ifdef ELITE_CAPI_H
#error "Do not include CAPI V1 in this file"
#endif
