/** @file AudioDecSvc_CapiV1Util.h
This file contains utility functions for Elite Audio Decoder service.

Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government, Diversion contrary to U.S. law prohibited.
*/

/**
========================================================================
Edit History

$Header: //components/rel/avs.adsp/2.7.1.c4/aud/services/dynamic_svcs/audio_dec_svc/src/AudioDecSvc_CapiV1Util.h#1 $


when       who     what, where, why
--------   ---     -------------------------------------------------------
02/04/2014   rbhatnk     Created file.

==========================================================================
*/
#ifndef CAPIV1DECUTIL_H_
#define CAPIV1DECUTIL_H_

#include "AudioDecSvc_Structs.h"

#if defined(__cplusplus)
extern "C" {
#endif // __cplusplus


ADSPResult audio_dec_svc_create_init_pack_capi_v1(capi_v2_t **capi_v2_ptr_ptr,  AudioDecSvcInitParams_t *pInitParams, dec_CAPI_init_params_t *pCapiInitParams);
ADSPResult audio_dec_svc_create_init_passthru_capi_v1(capi_v2_t **capi_v2_ptr_ptr,  AudioDecSvcInitParams_t *pInitParams, dec_CAPI_init_params_t *pCapiInitParams);
ADSPResult audio_dec_svc_create_init_conv_capi_v1(capi_v2_t **capi_v2_ptr_ptr,  AudioDecSvcInitParams_t *pInitParams, dec_CAPI_init_params_t *pCapiInitParams);
ADSPResult audio_dec_svc_create_init_depack_capi_v1(capi_v2_t **capi_v2_ptr_ptr,  AudioDecSvcInitParams_t *pInitParams, dec_CAPI_init_params_t *pCapiInitParams);
ADSPResult audio_dec_svc_create_init_auto_capi_v1(capi_v2_t **capi_v2_ptr_ptr,  AudioDecSvcInitParams_t *pInitParams, dec_CAPI_init_params_t *pCapiInitParams);

dec_AMDB_presence audio_dec_svc_get_amdb_presence(Dec_CAPI_Type type, uint32_t id1, uint32_t id2, void **amdb_capi_handle_ptr);

#if defined(__cplusplus)
}
#endif // __cplusplus

#endif /* CAPIV1DECUTIL_H_ */
