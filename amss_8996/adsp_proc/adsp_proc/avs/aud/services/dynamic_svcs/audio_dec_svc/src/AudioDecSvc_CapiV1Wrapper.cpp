/** @file AudioDecSvc_CapiV1Wrapper.cpp
This file contains functions for Elite Decoder service.

Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government, Diversion contrary to U.S. law prohibited.
 *//*====================================================================== */

/*========================================================================
Edit History

$Header: //components/rel/avs.adsp/2.7.1.c4/aud/services/dynamic_svcs/audio_dec_svc/src/AudioDecSvc_CapiV1Wrapper.cpp#1 $


when       who     what, where, why
--------   ---     -------------------------------------------------------
01/28/14    rbhatnk      Created file.

========================================================================== */

#include "AudioDecSvc_CapiV1Wrapper.h"
#include "AudioDecSvc_CapiV1WrapperPrivate.h"
#include "Elite_CAPI.h"
#include "adsp_media_fmt.h"
#include "CVocoderCapiLib.h"


#include "CHpMp2DecoderLib.h"
#include "CADPCMDecoderLib.h"
#include "CEtsiAmrWbPlusDecLib.h"
#include "CAc3PacketizerLib.h"
#include "CeAc3PacketizerLib.h"
#include "CMatPacketizerLib.h"
#include "CDtmfGeneratorLib.h"
#include "CPassthruFormatterLib.h"
#include "CDtshdPacketizerLib.h"
#include "CDepacketizerLib.h"
#include "adsp_asm_stream_commands.h"
#include "adsp_media_fmt.h"
#include "Elite_CAPI_V2_private_params.h"
#include "audio_basic_op_ext.h"

#define AUDIO_DEC_SVC_CAPI_V1_WRAPPER_STACK_SIZE 4096
#define AUDIO_DEC_SVC_CAPI_V1_WRAPPER_INPUT_SIZE 4096
#define AUDIO_DEC_SVC_CAPI_V1_WRAPPER_OUTPUT_SIZE 4098

#define AUDIO_DEC_SVC_CAPI_V1_WRAPPER_DEFAULT_KPPS 20000
#define AUDIO_DEC_SVC_CAPI_V1_WRAPPER_DEFAULT_CODE_BW 0
#define AUDIO_DEC_SVC_CAPI_V1_WRAPPER_DEFAULT_DATA_BW (2*1024*1024) //2MBPs



#if 0
static capi_v2_err_t audio_dec_svc_capi_v1_wrapper_change_media_fmt(capi_v2_t* _pif,
      const capi_v2_data_format_t* in_format_ptr[],
      capi_v2_data_format_t* out_format_ptr[]);
#endif

static capi_v2_err_t audio_dec_svc_capi_v1_wrapper_process(capi_v2_t* _pif,
      capi_v2_stream_data_t* input[],
      capi_v2_stream_data_t* output[]);
static capi_v2_err_t audio_dec_svc_capi_v1_wrapper_end(capi_v2_t* _pif);
static capi_v2_err_t audio_dec_svc_capi_v1_wrapper_set_param(capi_v2_t* _pif,
      uint32_t param_id,
      const capi_v2_port_info_t *port_info_ptr,
      capi_v2_buf_t *params_ptr);
static capi_v2_err_t audio_dec_svc_capi_v1_wrapper_get_param(capi_v2_t* _pif,
      uint32_t param_id,
      const capi_v2_port_info_t *port_info_ptr,
      capi_v2_buf_t *params_ptr);
static capi_v2_err_t audio_dec_svc_capi_v1_wrapper_set_properties(capi_v2_t* _pif,
      capi_v2_proplist_t *props_ptr);
static capi_v2_err_t audio_dec_svc_capi_v1_wrapper_get_properties(capi_v2_t* _pif,
      capi_v2_proplist_t *props_ptr);


static capi_v2_err_t audio_dec_svc_capi_v1_wrapper_process_set_properties(audio_dec_svc_capi_v1_wrapper_t* me, capi_v2_proplist_t *props_ptr);
static capi_v2_err_t audio_dec_svc_capi_v1_wrapper_process_get_properties(audio_dec_svc_capi_v1_wrapper_t* me, capi_v2_proplist_t *props_ptr);

static capi_v2_vtbl_t vtbl =
{
      audio_dec_svc_capi_v1_wrapper_process,
      audio_dec_svc_capi_v1_wrapper_end,
      audio_dec_svc_capi_v1_wrapper_set_param,
      audio_dec_svc_capi_v1_wrapper_get_param,
      audio_dec_svc_capi_v1_wrapper_set_properties,
      audio_dec_svc_capi_v1_wrapper_get_properties
};

static int init_capi(audio_dec_svc_capi_v1_wrapper_t* me, CAPI_Buf_t *params);

static bool_t has_media_fmt_changed(audio_dec_svc_capi_v2_media_fmt_t *old_fmt, audio_dec_svc_capi_v2_media_fmt_t *new_fmt)
{
   if ((old_fmt->main.format_header.data_format != new_fmt->main.format_header.data_format))
   {
      return true;
   }

   if (  (old_fmt->main.format_header.data_format == CAPI_V2_FIXED_POINT) ||
         (old_fmt->main.format_header.data_format == CAPI_V2_IEC61937_PACKETIZED) )
   {
      if (  (old_fmt->std.bitstream_format != new_fmt->std.bitstream_format) ||
            (old_fmt->std.num_channels != new_fmt->std.num_channels) ||
            (old_fmt->std.bits_per_sample != new_fmt->std.bits_per_sample) ||
            (old_fmt->std.sampling_rate != new_fmt->std.sampling_rate) ||
            (old_fmt->std.data_is_signed != new_fmt->std.data_is_signed) ||
            (old_fmt->std.data_interleaving != new_fmt->std.data_interleaving) ||
            (old_fmt->std.q_factor != new_fmt->std.q_factor) )
      {
         return true;
      }

      for (uint32_t j=0; (j<new_fmt->std.num_channels) && (j<CAPI_V2_MAX_CHANNELS); j++)
      {
         if (old_fmt->std.channel_type[j] != new_fmt->std.channel_type[j])
         {
            return true;
         }
      }
   }
   else
   {
      if (   (old_fmt->raw_fmt.bitstream_format != new_fmt->raw_fmt.bitstream_format))
      {
         return true;
      }
      // CAPI outputs raw bit stream only for depacketizer.
      // at that time we probably don't need an event (in dec this happens only when depacketizer chained with decoder.
   }

   return false;
}

static void init_media_fmt(audio_dec_svc_capi_v1_wrapper_t *me)
{
   audio_dec_svc_capi_v2_media_fmt_t *media_fmt = &(me->out_media_fmt[0]);

   media_fmt->main.format_header.data_format = me->out_data_fmt;
   switch(me->out_data_fmt)
   {
   case CAPI_V2_FIXED_POINT:
   {
      media_fmt->std.bits_per_sample = CAPI_V2_DATA_FORMAT_INVALID_VAL;
      media_fmt->std.bitstream_format = CAPI_V2_DATA_FORMAT_INVALID_VAL;
      media_fmt->std.data_interleaving = CAPI_V2_DEINTERLEAVED_PACKED;
      media_fmt->std.data_is_signed = 1;
      media_fmt->std.num_channels = CAPI_V2_DATA_FORMAT_INVALID_VAL;
      media_fmt->std.q_factor = CAPI_V2_DATA_FORMAT_INVALID_VAL;
      media_fmt->std.sampling_rate = CAPI_V2_DATA_FORMAT_INVALID_VAL;
      for (uint32_t j=0; (j<CAPI_V2_MAX_CHANNELS); j++)
      {
         media_fmt->std.channel_type[j] = (uint16_t)CAPI_V2_DATA_FORMAT_INVALID_VAL;
      }
   }
   break;
   case CAPI_V2_IEC61937_PACKETIZED:
   {
      media_fmt->std.bits_per_sample = CAPI_V2_DATA_FORMAT_INVALID_VAL;
      media_fmt->std.bitstream_format = me->media_fmt;
      media_fmt->std.data_interleaving = CAPI_V2_INVALID_INTERLEAVING;
      media_fmt->std.data_is_signed = 1;
      media_fmt->std.num_channels = CAPI_V2_DATA_FORMAT_INVALID_VAL;
      media_fmt->std.q_factor = CAPI_V2_DATA_FORMAT_INVALID_VAL;
      media_fmt->std.sampling_rate = CAPI_V2_DATA_FORMAT_INVALID_VAL;
      for (uint32_t j=0; (j<CAPI_V2_MAX_CHANNELS); j++)
      {
         media_fmt->std.channel_type[j] = (uint16_t)CAPI_V2_DATA_FORMAT_INVALID_VAL;
      }
   }
   break;
   case CAPI_V2_RAW_COMPRESSED:
   {
      //in case of depacketizer, unless we depacketize we don't know the fmt.
      media_fmt->raw_fmt.bitstream_format = 0;
   }
   break;
   default:
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Data format invalid %d", me->out_data_fmt);
      QURT_ELITE_ASSERT(0);
   }
   }
}

static void copy_media_fmt(audio_dec_svc_capi_v2_media_fmt_t *dest, const audio_dec_svc_capi_v2_media_fmt_t *src)
{
   memscpy(dest, sizeof(audio_dec_svc_capi_v2_media_fmt_t), src, sizeof(audio_dec_svc_capi_v2_media_fmt_t));
}

static capi_v2_err_t check_raise_kpps_event(audio_dec_svc_capi_v1_wrapper_t* me, uint32_t val)
{
   capi_v2_err_t result = CAPI_V2_EOK;
   if (me->kpps != val)
   {
      capi_v2_event_KPPS_t evnt;
      capi_v2_event_info_t event_info;
      event_info.port_info.is_valid = false;

      evnt.KPPS = val;

      event_info.payload.actual_data_len = sizeof(capi_v2_event_KPPS_t);
      event_info.payload.data_ptr = (int8_t*)&evnt;
      event_info.payload.max_data_len = sizeof(capi_v2_event_KPPS_t);
      result = me->cb_info.event_cb(me->cb_info.event_context, CAPI_V2_EVENT_KPPS, &event_info );
      if (CAPI_V2_EOK != result)
      {
         MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_dec_wrapper Failed to raise event for KPPS change");
      }
      else
      {
         MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Capi_V1_dec_wrapper raised event for KPPS change %lu", val);
         me->kpps = val;
      }
   }

   return result;
}

static uint32_t determine_data_bw(audio_dec_svc_capi_v1_wrapper_t* me)
{
   uint32_t bw=AUDIO_DEC_SVC_CAPI_V1_WRAPPER_DEFAULT_DATA_BW;
   switch( me->media_fmt )
   {
   case ASM_MEDIA_FMT_EVRC_FS:
   case ASM_MEDIA_FMT_V13K_FS:
   case ASM_MEDIA_FMT_EVRCB_FS:
   case ASM_MEDIA_FMT_AMRNB_FS:
   case ASM_MEDIA_FMT_G711_ALAW_FS:
   case ASM_MEDIA_FMT_G711_MLAW_FS:
   case ASM_MEDIA_FMT_EVRCWB_FS:
   case ASM_MEDIA_FMT_AMRWB_FS:
   case ASM_MEDIA_FMT_FR_FS:
   {
      //TODO: no change yet
      break;
   }
   case ASM_MEDIA_FMT_MP2:
   {
      bw = 7*1024*1024;
      break;
   }
   case ASM_MEDIA_FMT_ADPCM:
   {
      break;
   }
   case ASM_MEDIA_FMT_AMR_WB_PLUS_V2:
   {
      bw = 2*1024*1024;
      break;
   }
   case ASM_MEDIA_FMT_DTMF:
   {
      break;
   }
   case ASM_MEDIA_FMT_DTS:
   {
      break;
   }
   case ASM_MEDIA_FMT_DTS_LBR:
   {
      break;
   }
   default:
   {
      break;
   }
   }

   return bw;
}

static capi_v2_err_t check_raise_bw_event(audio_dec_svc_capi_v1_wrapper_t* me, uint32_t code_bw, uint32_t data_bw)
{
   capi_v2_err_t result = CAPI_V2_EOK;
   if ( (me->code_bw != code_bw) || (me->data_bw != data_bw) )
   {
      capi_v2_event_bandwidth_t evnt;
      capi_v2_event_info_t event_info;
      event_info.port_info.is_valid = false;

      evnt.code_bandwidth = code_bw;
      evnt.data_bandwidth = data_bw;

      event_info.payload.actual_data_len = sizeof(capi_v2_event_bandwidth_t);
      event_info.payload.data_ptr = (int8_t*)&evnt;
      event_info.payload.max_data_len = sizeof(capi_v2_event_bandwidth_t);
      result = me->cb_info.event_cb(me->cb_info.event_context, CAPI_V2_EVENT_BANDWIDTH, &event_info );
      if (CAPI_V2_EOK != result)
      {
         MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_dec_wrapper Failed to raise event for bandwidth change");
      }
      else
      {
         MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Capi_V1_dec_wrapper raised event for bw change %lu %lu", code_bw , data_bw);
         me->code_bw = code_bw;
         me->data_bw = data_bw;
      }
   }
   return result;
}

int  check_and_convert_enc_error_code_to_dec_error_code(int code)
{
   switch(code)
   {
      case ENC_NEED_MORE:
         return DEC_NEED_MORE;
      case ENC_BADPARAM_FAILURE:
         return DEC_BADPARAM_FAILURE;
      default:
         return code;
   }
}

static capi_v2_err_t check_raise_metadata_available_event(audio_dec_svc_capi_v1_wrapper_t* me)
{
   capi_v2_err_t result = CAPI_V2_EOK;

   int metadata_size = 0;

   //query if CAPI has some meta-data to send now.
   int res = me->capi_v1_ptr->GetParam(eIcapiActualMetaDataSize ,&metadata_size);
   if ((res != DEC_SUCCESS) || (metadata_size <= 0))
   {
      //return if not or if CAPI fails to get param.
      return CAPI_V2_EOK;
   }

   capi_v2_event_info_t event_info;
   event_info.port_info.is_valid = true;
   event_info.port_info.is_input_port = false;
   event_info.port_info.port_index = 0;

   event_info.payload.actual_data_len = 0;
   event_info.payload.data_ptr = NULL;
   event_info.payload.max_data_len = 0;
   result = me->cb_info.event_cb(me->cb_info.event_context, CAPI_V2_EVENT_METADATA_AVAILABLE, &event_info );
   if (CAPI_V2_EOK != result)
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_dec_wrapper Failed to raise metadata available event");
   }
   else
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Capi_V1_dec_wrapper raised metadata available event");
   }

   return result;
}

static capi_v2_err_t check_raise_output_media_fmt_updated_event(audio_dec_svc_capi_v1_wrapper_t* me, audio_dec_svc_capi_v2_media_fmt_t *new_fmt)
{
   capi_v2_err_t result = CAPI_V2_EOK;
   if ( has_media_fmt_changed(&(me->out_media_fmt[0]), new_fmt ) )
   {
      copy_media_fmt(&(me->out_media_fmt[0]), new_fmt);

      capi_v2_event_info_t event_info;
      event_info.port_info.is_valid = true;
      event_info.port_info.is_input_port = false;
      event_info.port_info.port_index = 0;

      event_info.payload.actual_data_len = aud_dec_svc_media_fmt_size(new_fmt->main.format_header.data_format);
      event_info.payload.data_ptr = (int8_t*)&me->out_media_fmt[0];
      event_info.payload.max_data_len = event_info.payload.actual_data_len;
      result = me->cb_info.event_cb(me->cb_info.event_context, CAPI_V2_EVENT_OUTPUT_MEDIA_FORMAT_UPDATED, &event_info );
      if (CAPI_V2_EOK != result)
      {
         MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_dec_wrapper Failed to raise event for out media fmt change");
      }
      else
      {
         MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Capi_V1_dec_wrapper raised event for out media fmt change");
      }
   }
   return result;
}




static capi_v2_err_t update_in_media_fmt_blk_raw_fmt(audio_dec_svc_capi_v1_wrapper_t *me, capi_v2_buf_t *payload)
{
   int dec_result = DEC_SUCCESS;
   capi_v2_err_t capi_v2_result = CAPI_V2_EOK;

   audio_dec_svc_capi_v2_media_fmt_t *data_ptr = (audio_dec_svc_capi_v2_media_fmt_t*)payload->data_ptr;

   if ((data_ptr->raw_fmt.bitstream_format != me->media_fmt) )
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
            "Capi_V1_dec_wrapper Media format: bs format doesn't match initially set values bit stream format 0x%lx",
            me->media_fmt);
      return CAPI_V2_EFAILED;
   }

   int8_t *pFmtBlk = payload->data_ptr + AUD_DEC_SVC_MIN_SIZE_OF_RAW_MEDIA_FMT;
   uint32_t ulFmtBlkSize = payload->actual_data_len - AUD_DEC_SVC_MIN_SIZE_OF_RAW_MEDIA_FMT;

   //initialize Params structure
   CAPI_Buf_t     Params;                 // for send parameters to Init() function

   switch( data_ptr->raw_fmt.bitstream_format )
   {
   case ASM_MEDIA_FMT_MULTI_CHANNEL_PCM_V2:
   case ASM_MEDIA_FMT_MULTI_CHANNEL_PCM_V3:
   {
      //for PCM CAPI is not used, so coming here is a crime.
      return CAPI_V2_EFAILED;
   }
   case ASM_MEDIA_FMT_ADPCM:
   {
      /// uCmdHeaderSize is in bytes, but pMediaFmt is not a uint8_t pointer.
      /// Must typecast to get correct address
      asm_adpcm_fmt_blk_t* pAdpcmFmtBlk = (asm_adpcm_fmt_blk_t*) ( pFmtBlk );
      if (ulFmtBlkSize != sizeof(asm_adpcm_fmt_blk_t)) { return CAPI_V2_EBADPARAM; }

      CADPCMDecoderLib* pCAdpcmLib = (CADPCMDecoderLib*) (me->capi_v1_ptr);
      (void) pCAdpcmLib->SetParam ( eIcapiNumChannels, pAdpcmFmtBlk->num_channels );
      (void) pCAdpcmLib->SetParam ( CADPCMDecoderLib::eADPCMBitsPerSample, pAdpcmFmtBlk->bits_per_sample );
      (void) pCAdpcmLib->SetParam ( eIcapiSamplingRate, pAdpcmFmtBlk->sample_rate );
      (void) pCAdpcmLib->SetParam ( CADPCMDecoderLib::eADPCMNumBlockSize, pAdpcmFmtBlk->blk_size );

      //cannot assert now, but we could query capi before and after setParam and compare.
      //QURT_ELITE_ASSERT( ((uint32_t)pMe->capiContainer[0]->input_buf_list.pBuf[0].nMaxDataLen) >= (pAdpcmFmtBlk->blk_size * 2));

      MSG_4(MSG_SSID_QDSP6, DBG_HIGH_PRIO,
            "APCM Media Format, ch=%u,SR=%lu,blkSize=%lu,bps=%u",pAdpcmFmtBlk->num_channels,
            pAdpcmFmtBlk->sample_rate,pAdpcmFmtBlk->blk_size,
            pAdpcmFmtBlk->bits_per_sample);

      //Initialize CAPI
      (void)init_capi(me, NULL);

      break;
   }

   case ASM_MEDIA_FMT_AMR_WB_PLUS_V2:
   {
      asm_amrwbplus_fmt_blk_v2_t* pAmrWbPlusFmtBlk = (asm_amrwbplus_fmt_blk_v2_t*) ( pFmtBlk );
      if (ulFmtBlkSize != sizeof(asm_amrwbplus_fmt_blk_v2_t)) { return CAPI_V2_EBADPARAM; }

      dec_result = me->capi_v1_ptr->SetParam(CEtsiAmrWbPlusDecLib::amrWbPlusFileFormat, pAmrWbPlusFmtBlk->amr_frame_fmt);
      if(DEC_SUCCESS != dec_result)
      {
         return CAPI_V2_EBADPARAM;
      }

      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,
            "AMR WB+ Media Format: amr_frame_fmt=%lu",
            pAmrWbPlusFmtBlk->amr_frame_fmt);

      //Initialize CAPI
      dec_result = init_capi(me, NULL);

      if(DEC_SUCCESS != dec_result)
      {
         return CAPI_V2_EBADPARAM;
      }

      break;
   }
   case ASM_MEDIA_FMT_G711_ALAW_FS:
   case ASM_MEDIA_FMT_G711_MLAW_FS:
      //format block for these two types are identical
   {
      VoiceInitParamsType VInitParams;
      if (ulFmtBlkSize != sizeof(asm_g711_alaw_fmt_blk_t)){ return CAPI_V2_EBADPARAM; }

      Params.Data = (char*)&VInitParams;
      Params.nActualDataLen = sizeof(VoiceInitParamsType);
      Params.nMaxDataLen = sizeof(VoiceInitParamsType);
      VInitParams.VocTypeID = data_ptr->raw_fmt.bitstream_format;
      VInitParams.VocConfigBlkSize = ulFmtBlkSize;
      VInitParams.VocConfigBlk = pFmtBlk;

      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Voice Media Format %lx",data_ptr->raw_fmt.bitstream_format);

      (void)init_capi(me, &Params);

      break;
   }

   /**
    * Updating the media format block parameters using Set Param Call. Parameters required by CAPI can be set, provided GetParam call for eIcapiWaitForFormatBlock returns 1.
    * To enable Set Param call, set OEM_ROOT from command prompt.
    */
   case ASM_MEDIA_FMT_AC3:
   case ASM_MEDIA_FMT_EAC3:
   case ASM_MEDIA_FMT_MP3:
   case ASM_MEDIA_FMT_MP2:
   case ASM_MEDIA_FMT_DTS:
   case ASM_MEDIA_FMT_DTS_LBR:
   case ASM_MEDIA_FMT_ATRAC:
   case ASM_MEDIA_FMT_MAT:
   case ASM_MEDIA_FMT_EVRC_FS:
   case ASM_MEDIA_FMT_EVRCB_FS:
   case ASM_MEDIA_FMT_V13K_FS:
   case ASM_MEDIA_FMT_AMRNB_FS:
   case ASM_MEDIA_FMT_AMRWB_FS:
   case ASM_MEDIA_FMT_EVRCWB_FS:
   case ASM_MEDIA_FMT_FR_FS:
      break; //Do nothing

   default: //Custom modules
   {
      dec_result = me->capi_v1_ptr->SetParam(eIcapiMediaInfoPtr,(uint32_t)pFmtBlk);
      MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Custom decoder media fmt update: fmt ID = %lx, dec_result = %d",data_ptr->raw_fmt.bitstream_format, dec_result);
      break;
   }

   }

   if (DEC_SUCCESS != dec_result)
   {
      return CAPI_V2_EFAILED;
   }
   //on success only
   me->wait_for_media_fmt = false;

   return capi_v2_result;
}

static capi_v2_err_t update_in_media_fmt_blk (audio_dec_svc_capi_v1_wrapper_t *me, capi_v2_buf_t *payload)
{
   int result = DEC_SUCCESS;

   capi_v2_set_get_media_format_t *data_ptr = (capi_v2_set_get_media_format_t*)payload->data_ptr;

   switch(data_ptr->format_header.data_format)
   {
   case CAPI_V2_FIXED_POINT:
   case CAPI_V2_IEC61937_PACKETIZED:
   {
      if (payload->actual_data_len < ( sizeof(audio_dec_svc_capi_v2_media_fmt_t) ))
      {
         MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_dec_wrapper Set, media fmt not enough size");
         return CAPI_V2_EFAILED;
      }

      audio_dec_svc_capi_v2_media_fmt_t *fmt = (audio_dec_svc_capi_v2_media_fmt_t*)data_ptr;

      if (fmt->std.sampling_rate != CAPI_V2_DATA_FORMAT_INVALID_VAL)
      {
         result = me->capi_v1_ptr->SetParam(eIcapiSamplingRate, fmt->std.sampling_rate);
      }
      //note: eIcapiNumChannels, eIcapiOutputChanMap, bitspersample are set as output media fmt
      break;
   }
   case CAPI_V2_RAW_COMPRESSED:
   {
      return update_in_media_fmt_blk_raw_fmt(me, payload);
      break;
   }
   default:
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_dec_wrapper Media format: data format %u. Unsupported.",
            data_ptr->format_header.data_format);
   }
   }

   if (DEC_SUCCESS != result)
   {
      return CAPI_V2_EFAILED;
   }
   return CAPI_V2_EOK;
}

static capi_v2_err_t set_param(audio_dec_svc_capi_v1_wrapper_t *me, uint32_t param_id,
      capi_v2_buf_t *params_ptr)
{
   capi_v2_err_t res = CAPI_V2_EOK;

   switch (param_id)
   {

   case ASM_PARAM_ID_DTS_MIX_LFE_TO_FRONT:
   case ASM_PARAM_ID_DTS_ENABLE_PARSE_REV2AUX:
   {
      asm_dts_generic_param_t *pDtsConfig = (asm_dts_generic_param_t *)params_ptr->data_ptr;
      int dec_result = me->capi_v1_ptr->SetParam(param_id, pDtsConfig->generic_parameter);
      if (DEC_SUCCESS != dec_result)
      {
         res = CAPI_V2_EBADPARAM;
      }
      break;
   }
   case ASM_PARAM_ID_DTS_LBR_MIX_LFE_TO_FRONT:
   case ASM_PARAM_ID_DTS_LBR_ENABLE_PARSE_REV2AUX:
   {
      asm_dts_lbr_generic_param_t *pDtsConfig = (asm_dts_lbr_generic_param_t *)params_ptr->data_ptr;
      int dec_result = me->capi_v1_ptr->SetParam(param_id, pDtsConfig->generic_parameter);
      if (DEC_SUCCESS != dec_result)
      {
         res = CAPI_V2_EBADPARAM;
      }
      break;
   }
   case CAPI_PARAM_DTMF_RUNNING:
   case CAPI_PARAM_DTMF_HIGH_TONE:
   case CAPI_PARAM_DTMF_LOW_TONE:
   case CAPI_PARAM_DTMF_DURATION:
   case CAPI_PARAM_DTMF_GAIN:
   case CAPI_PARAM_DTMF_SET:
   {
      if (me->media_fmt != ASM_MEDIA_FMT_DTMF)
      {
         MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "error setting param. fmt=0x%x,ulParamId=0x%x,res=0x%x (not applicable for non-DTMF)",
               (unsigned int)me->media_fmt, (unsigned int)param_id, res);
      }
      int id = 0;
      switch(param_id)
      {
      case CAPI_PARAM_DTMF_RUNNING:
         id = CDtmfGeneratorLib::eDtmfRunning; break;
      case CAPI_PARAM_DTMF_HIGH_TONE:
         id = CDtmfGeneratorLib::eDtmfHighTone; break;
      case CAPI_PARAM_DTMF_LOW_TONE:
         id = CDtmfGeneratorLib::eDtmfLowTone; break;
      case CAPI_PARAM_DTMF_DURATION:
         id = CDtmfGeneratorLib::eDtmfDuration; break;
      case CAPI_PARAM_DTMF_GAIN:
         id = CDtmfGeneratorLib::eDtmfGain; break;
      case CAPI_PARAM_DTMF_SET:
         id = CDtmfGeneratorLib::eDtmfSet; break;
      }
      int dec_result = me->capi_v1_ptr->SetParam(id, *(int32_t *)params_ptr->data_ptr);
      if (DEC_SUCCESS != dec_result)
      {
         res = CAPI_V2_EBADPARAM;
      }
      break;
   }
   default:
   {
      //Assume the Set param value would be a 32-bit integer, as is specified in CAPI document.
      int dec_result = me->capi_v1_ptr->SetParam(param_id, *(int32_t *)params_ptr->data_ptr);
      if (DEC_SUCCESS != dec_result)
      {
         res = CAPI_V2_EUNSUPPORTED;
      }
      break;
   }
   }
   if(CAPI_V2_FAILED(res))
   {
      MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "error setting param. fmt=0x%x,ulParamId=0x%x,res=0x%x", (unsigned int)me->media_fmt, (unsigned int)param_id, res);
   }

   return res;
}


static void set_end_of_stream(audio_dec_svc_capi_v1_wrapper_t *me, bool_t endOfStreamFlag)
{
   switch(me->media_fmt)
   {
   case ASM_MEDIA_FMT_AC3:
      if ( me->out_data_fmt == CAPI_V2_IEC61937_PACKETIZED )
      {
         me->capi_v1_ptr->SetParam(CAc3PacketizerLib::eAc3PacketizerEndOfStream, endOfStreamFlag);
      }
      break;

   case ASM_MEDIA_FMT_EAC3:
      if ( me->out_data_fmt == CAPI_V2_IEC61937_PACKETIZED )
      {
         me->capi_v1_ptr->SetParam(CeAc3PacketizerLib::eAc3PacketizerEndOfStream, endOfStreamFlag);
      }
      break;
   case ASM_MEDIA_FMT_MAT:
      me->capi_v1_ptr->SetParam(CMatPacketizerLib::eMatPacketizerEndOfStream, endOfStreamFlag);
      break;
   }
}

static void check_set_end_of_stream(audio_dec_svc_capi_v1_wrapper_t *me, bool_t endOfStreamFlag)
{
   //if EoS flag has not changed, then nothing to do
   if (endOfStreamFlag == me->prev_eos_flag)
   {
      return;
   }

   me->prev_eos_flag = endOfStreamFlag;

   return set_end_of_stream(me, endOfStreamFlag);
}

/**
 * reinits if already initialized
 */
static int init_capi(audio_dec_svc_capi_v1_wrapper_t* me, CAPI_Buf_t *params)
{
   int ret;
   if (me->capi_init_done)
   {
      ret = me->capi_v1_ptr->ReInit(params);
   }
   else
   {
      ret = me->capi_v1_ptr->Init(params);
   }

   me->prev_eof_flag = false;
   me->prev_eos_flag = false;
   //initialize the EoF/EoS flags in the beginning, as some CAPIs do not initialize themselves.
   set_end_of_stream(me, false);

   if (CAPI_V2_EOK == ret)
   {
      me->capi_init_done = true;
   }

   return ret;
}

/** @ingroup
  Processes an input data and provides output for all input and output ports.

  @datatypes
  capi_v2_t \n
  capi_v2_stream_data_t

  @param[in,out] _pif       Pointer to the module object.
  @param[in,out] input      Array of pointers to the input data for each
                            input port. The length of the array is the number
                            of input ports. The function
                            must modify the actual_data_len field to
                            indicate how many bytes were consumed.
  @param[out]    output     Array of pointers to the output data for each
                            output port. The function sets the actual_data_len
                            field to indicate how many bytes were generated.

  @detdesc
  This is a generic processing function.
  @par
  On each call to the %process() function, the behavior of the module depends
  on the value it returned for the requires_data_buffering property. Please
  refer to the comments for the requires_data_buffering property for a
  description of the behavior.
  @par
  No debug messages are allowed in this function.
  @par

  @return
  Indication of success or failure.

  @dependencies
  The change_media_type() function must have been executed.
 */
//#define DEBUG_MEDIUM
//#define DEBUG_VERBOSE
static capi_v2_err_t audio_dec_svc_capi_v1_wrapper_process(capi_v2_t* _pif,
      capi_v2_stream_data_t* input[],
      capi_v2_stream_data_t* output[])
{
   capi_v2_err_t result = CAPI_V2_EOK;

   if (!_pif) return CAPI_V2_EBADPARAM;

   audio_dec_svc_capi_v1_wrapper_t *me = (audio_dec_svc_capi_v1_wrapper_t*)_pif;

   //handling one input and one output only.
   uint32_t port=0 ;

#ifdef DEBUG_MEDIUM
   MSG_4(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Capi_V1_dec_wrapper process enter: input [port %lu] TS: %lu,%lu, bufs_num:%lu. ",port,
         (uint32_t)(input[port]->timestamp>>32), (uint32_t)input[port]->timestamp, input[port]->bufs_num);

   MSG_7(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Capi_V1_dec_wrapper process enter: input [port %lu] flag:[%lu,%lu,%lu,%lu,%lu,%lu]",port,
         (uint32_t)input[port]->flags.is_timestamp_valid, (uint32_t)input[port]->flags.end_of_frame,
         (uint32_t)input[port]->flags.marker_eos,(uint32_t)input[port]->flags.marker_1,
         (uint32_t)input[port]->flags.marker_2,(uint32_t)input[port]->flags.marker_3);

   for (buf_num = 0; buf_num < input[port]->bufs_num; buf_num++)
   {
      MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Capi_V1_dec_wrapper process enter: input [port %lu]. buf_num:%lu. in_size=%lu",port,
            buf_num, input[port]->buf_ptr[0].actual_data_len);
   }
   for (buf_num = 0; buf_num < input[port]->bufs_num; buf_num++)
   {
      MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Capi_V1_dec_wrapper process enter: output [port %lu]. buf_num:%lu. out_size=%lu",port,
            buf_num, output[port]->buf_ptr[0].actual_data_len);
   }
#endif
   //error check

   //do not drop data if waiting for media fmt (even though it breaks backward compatible behavior)
   //not dropping data helps in capi chaining when prev block cannot provide media fmt.
   if (!me->capi_init_done)
   {
      (void)init_capi(me, NULL);
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Capi_V1_dec_wrapper process automatically called init although media fmt was probably not received.");
   }

   //set end of stream
   check_set_end_of_stream(me, input[0]->flags.marker_eos>0);

   CAPI_Buf_t                          input_buf;
   CAPI_Buf_t                          output_buf;
   CAPI_BufList_t                      input_buf_list;
   CAPI_BufList_t                      output_buf_list;

   int in_size_before = input[port]->buf_ptr->actual_data_len;

   input_buf.Data = (char*)input[port]->buf_ptr->data_ptr;
   input_buf.nActualDataLen = input[port]->buf_ptr->actual_data_len;
   input_buf.nMaxDataLen = input[port]->buf_ptr->max_data_len;
   input_buf_list.nBufs = 1;
   input_buf_list.pBuf = &input_buf;

   output_buf.Data = (char*)output[port]->buf_ptr->data_ptr;
   output_buf.nActualDataLen = output[port]->buf_ptr->actual_data_len;
   output_buf.nMaxDataLen = output[port]->buf_ptr->max_data_len;
   output_buf_list.nBufs = 1;
   output_buf_list.pBuf = &output_buf;

#if DEBUG_VERBOSE
   int i=0;
   printf("Input data %x %x %x %x  %x %x %x %x  %x %x %x %x \n", input_buf.Data[i++], input_buf.Data[i++],input_buf.Data[i++],input_buf.Data[i++],
         input_buf.Data[i++], input_buf.Data[i++],input_buf.Data[i++],input_buf.Data[i++],
         input_buf.Data[i++], input_buf.Data[i++],input_buf.Data[i++],input_buf.Data[i++]);
#endif

#if DEBUG_VERBOSE
   {
      if (input_buf.nActualDataLen > me->bytes_logged) //this is valid scenario as input is not fetched unless capi returns input without generating output (since no need_more).
      {
         int8_t * buf_addr;
         uint32_t buf_size;

         printf("me->bytes_logged = %lu, input_buf.nActualDataLen %d\n", me->bytes_logged, input_buf.nActualDataLen);
         buf_addr = (int8_t *)(input_buf.Data) + me->bytes_logged;
         buf_size = (input_buf.nActualDataLen -  me->bytes_logged);

         if(buf_size)
         {
            FILE *fp;
            fp = fopen("dec_in.raw","a"); // in append mode
            fwrite(buf_addr,sizeof(int8_t),buf_size,fp);
            fclose(fp);
         }
      }
   }
#endif


   int dec_res = me->capi_v1_ptr->Process(&input_buf_list, &output_buf_list, NULL);

   bool_t need_events = true;

   if( (input_buf.nActualDataLen<0) || (input_buf.nActualDataLen > in_size_before))
   {
      //The number of remaining bytes are negative or greater than valid number of bytes
      //at the beginning of decode. This indicates that decoding did not happen correctly
      //and decoder might be in a bad state
      MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
            "Invalid # of available input bytes after CAPI process call at input port 0 : Available bytes before = %d, Available bytes after = %d",
            in_size_before, input_buf.nActualDataLen);

      //set the result to DEC_RECOVERY_FAILURE which will discard the contents of the
      //current buffer
      dec_res = DEC_RECOVERY_FAILURE;

      //reset the decoder in order to avoid undefined behavior with future buffers
      init_capi(me, NULL);
   }
   dec_res = check_and_convert_enc_error_code_to_dec_error_code(dec_res);

   //check the status of the decoder and take action as per the return type
   //handle enc error codes because depacketizer can return it.
   switch ( dec_res )
   {
   case DEC_SUCCESS:
   {
      break;
   }
   case DEC_NEED_MORE:
   {
      // Need more bitstream
#ifdef DBG_AUDIO_DECODE
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Need more bitstream to decode");
#endif
      //if more bitstream is needed, output should be zero length
      QURT_ELITE_ASSERT(0 == output_buf.nActualDataLen);
      result = CAPI_V2_EOK;
      need_events = false;
      break;
   }

   case DEC_RECOVERY_SUCCESS:
   {
      // If decode fails in the header parsing, we search for the next sync we return here
      // otherwise we return DEC_RECOVERY_FAILURE
      // We are not going for decode after finding fresh sync, instead coming here
      // this is to explicitly tell that at max a frame worth of data is discarded(useful for AV sync)
      MSG(MSG_SSID_QDSP6, DBG_LOW_PRIO, "Decoder recovery successful");
      if(!input_buf.nActualDataLen)
      {
         MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
               "Decoder recovery was successful but # of bytes left in input buffer is zero!!");
      }
      // We come out as soon as we find next sync, so output shud be zero
      // adding it here as a protection in case CAPI doesnt update properly
      output_buf.nActualDataLen = 0;
      result = CAPI_V2_EOK;

      need_events = false;
      break;
   }

   case DEC_BADPARAM_FAILURE:
   {
      //Bad parameters detected in bitstream so discard the current frame
      // (typical scenario is if decoding fails in core decoder, not in header)
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Decoder encountered bad param failure");
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Decoder discarding current frame, decode next frame\n");

      output_buf.nActualDataLen = 0;
      result = CAPI_V2_EBADPARAM;

      need_events = false;

      break;
   }

   case DEC_RECOVERY_FAILURE:
   default:
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Decoder recovery failure; Erroneous Bitstream");

      // Discard the data in internal buffer
      output_buf.nActualDataLen = 0;
      input_buf.nActualDataLen = 0;
      result = CAPI_V2_EFAILED;

      need_events = false;
      break;
   }
   }

   me->bytes_logged = input_buf.nActualDataLen;

   input[port]->buf_ptr->data_ptr = (int8_t*)input_buf.Data;
   input[port]->buf_ptr->actual_data_len = in_size_before - input_buf.nActualDataLen; //in capi v2 input buf actual size on output means, the num bytes read.
   input[port]->buf_ptr->max_data_len = input_buf.nMaxDataLen;

   output[port]->buf_ptr->data_ptr = (int8_t*)output_buf.Data;
   output[port]->buf_ptr->actual_data_len = output_buf.nActualDataLen;
   output[port]->buf_ptr->max_data_len = output_buf.nMaxDataLen;

#ifdef DEBUG_MEDIUM
   MSG_4(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Capi_V1_dec_wrapper process exit: output [port %lu] TS: %lu,%lu, bufs_num:%lu.",port,
         (uint32_t)(output[port]->timestamp>>32), (uint32_t)output[port]->timestamp, output[port]->bufs_num);

   MSG_7(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Capi_V1_dec_wrapper process exit: output [port %lu] flag:[%lu,%lu,%lu,%lu,%lu,%lu]",port,
         (uint32_t)output[port]->flags.is_timestamp_valid, (uint32_t)output[port]->flags.end_of_frame,
         (uint32_t)output[port]->flags.marker_eos,(uint32_t)output[port]->flags.marker_1,
         (uint32_t)output[port]->flags.marker_2,(uint32_t)output[port]->flags.marker_3);

   for (buf_num = 0; buf_num < output[port]->bufs_num; buf_num++)
   {
      MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Capi_V1_dec_wrapper process exit: output [port %lu]. buf_num:%lu. out_size=%lu",port,
            buf_num, output[port]->buf_ptr[0].actual_data_len);
   }
   for (buf_num = 0; buf_num < input[port]->bufs_num; buf_num++)
   {
      MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Capi_V1_dec_wrapper process exit: input [port %lu]. buf_num:%lu. in_size read=%lu\n",port,
            buf_num, input[port]->buf_ptr[0].actual_data_len);
   }
#endif

   //Do not raise events in case of failure or in case of NEED_MORE, RECOVERY_SUCCESS
   if (!need_events) return result;

   output[port]->flags = input[port]->flags;
   output[port]->timestamp = input[port]->timestamp;

   int val=0;
   int res =  me->capi_v1_ptr->GetParam(eIcapiKCPSRequired, &val);
   if (res != DEC_SUCCESS)
   {
      val = AUDIO_DEC_SVC_CAPI_V1_WRAPPER_DEFAULT_KPPS;
   }
   result = check_raise_kpps_event(me, val);

   result |= check_raise_bw_event(me, AUDIO_DEC_SVC_CAPI_V1_WRAPPER_DEFAULT_CODE_BW, determine_data_bw(me));

   //For Depacketizer don't raise media fmt event (output is RAW format)
   if ( (me->out_media_fmt[0].main.format_header.data_format == CAPI_V2_FIXED_POINT) ||
         (me->out_media_fmt[0].main.format_header.data_format == CAPI_V2_IEC61937_PACKETIZED))
   {
      audio_dec_svc_capi_v2_media_fmt_t new_fmt;

      copy_media_fmt(&new_fmt, &(me->out_media_fmt[0])); //initialize with old values.

      res = me->capi_v1_ptr->GetParam(eIcapiBitsPerSample, &val);

      if ((DEC_BADPARAM_FAILURE == res) || (ENC_BADPARAM_FAILURE == res))
      {
         val = 16; //ignore bad param failure (for voice capis)
      }
      else if (res != DEC_SUCCESS)
      {
         MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Capi_V1_dec_wrapper Get param failed for bits per sample");
         return CAPI_V2_EFAILED;
      }

      new_fmt.std.bits_per_sample = (uint32_t)val;
      new_fmt.std.q_factor = (val == 16)? PCM_16BIT_Q_FORMAT : ELITE_32BIT_PCM_Q_FORMAT;

      res = me->capi_v1_ptr->GetParam(eIcapiSamplingRate, &val);
      if (res != DEC_SUCCESS)
      {
         MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Capi_V1_dec_wrapper Get param failed for sampling rate");
         return CAPI_V2_EFAILED;
      }
      new_fmt.std.sampling_rate = (uint32_t)val;

      CAPI_ChannelMap_t *out_chan_map = NULL;
      res = me->capi_v1_ptr->GetParam(eIcapiOutputChanMap, (int*)&out_chan_map);
      if (res != DEC_SUCCESS)
      {
         MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Capi_V1_dec_wrapper Get param failed for sampling rate");
         return CAPI_V2_EFAILED;
      }
      new_fmt.std.num_channels = out_chan_map->nChannels;
      for (unsigned int ch=0;(ch<new_fmt.std.num_channels) && (ch<CAPI_V2_MAX_CHANNELS); ch++)
      {
         new_fmt.std.channel_type[ch] = (uint16_t)out_chan_map->nChannelMap[ch];
      }

      {
         capi_v2_err_t err_code = check_raise_output_media_fmt_updated_event(me, &new_fmt);
         if (CAPI_V2_FAILED(err_code))
         {
            result = err_code;
         }
      }
   }

   {
      capi_v2_err_t err_code = check_raise_metadata_available_event(me);
      if (CAPI_V2_FAILED(err_code))
      {
         result = err_code;
      }
   }

   return result;
}

/** @ingroup
  Returns the module to the uninitialized state and frees any memory that
  was allocated by it.

  @datatypes
  capi_v2_t

  @param[in,out] _pif    Pointer to the module object.

  @detdesc
  Before returning SUCCESS and once the function is complete, a debug
  message is to be printed:
  @par
  @code
  MSG(MSG_SSID_QDSP6,DBG_HIGH_PRIO,"CAPI_V2 Libname End done");
  @endcode
  @par
  @note1hang This function undoes all the work that capi_v2_init_f() has done.
             Any call using the module object is not allowed after this call.

  @dependencies
  None.
 */
static capi_v2_err_t audio_dec_svc_capi_v1_wrapper_end(capi_v2_t* _pif)
{
   if (!_pif) return CAPI_V2_EOK;

   audio_dec_svc_capi_v1_wrapper_t *me = (audio_dec_svc_capi_v1_wrapper_t*)_pif;

   int res = me->capi_v1_ptr->End();

   me->capi_init_done = false;

   me->vtbl.vtbl_ptr = NULL;

   MSG_1(MSG_SSID_QDSP6,DBG_HIGH_PRIO,"Capi_V1_dec_wrapper End done. res = %d", res);

   return CAPI_V2_EOK;
}

/** @ingroup
  Sets a parameter value or a parameter structure containing multiple
  parameters.

  @datatypes
  capi_v2_t \n
  capi_v2_buf_t

  @param[in,out] _pif        Pointer to the module object.
  @param[in]     param_id    Parameter ID of the parameter whose value is
                             being passed in this function. For example: \n
                             - CAPI_V2_LIBNAME_ENABLE
                             - CAPI_V2_LIBNAME_FILTER_COEFF @tablebulletend
  @param[in]     params_ptr  Buffer containing the value of the parameter.
                             The format depends on the implementation.

  @detdesc
  In the event of a failure, the appropriate error code is to be
  returned.
  The actual_data_len field of the parameter pointer is to be at least the size
  of the parameter structure. Therefore, the following check must be exercised
  for each tuning parameter ID.
  @par
  @code
  if (params_ptr->actual_data_len >= sizeof(asm_gain_struct_t))
  {
     :
     :
  }
  else
  {
     MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"CAPI_V2 Libname Set, Bad param size
     %lu",params_ptr->actual_data_len);
     return CAPI_V2_ENEEDMORE;
  }
  @endcode
  @par
  Optionally, some parameter values can be printed for tuning verification.
  @par
  Before returning, the actual_data_len field must be filled with the number
  of bytes read from the buffer.
  @par
  Before returning SUCCESS and once the function is complete, a debug message
  is to be printed:
  @par
  @code
  MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"CAPI_V2 Libname Set param for 0x%lx done",param_id);
  @endcode

  @return
  None.

  @dependencies
  None.
 */
static capi_v2_err_t audio_dec_svc_capi_v1_wrapper_set_param(capi_v2_t* _pif,
      uint32_t param_id,
      const capi_v2_port_info_t *port_info_ptr,
      capi_v2_buf_t *params_ptr)
{
   if (params_ptr->actual_data_len < sizeof(int))
   {
      MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_dec_wrapper Set, Param id 0x%lx Bad param size %lu", param_id, params_ptr->actual_data_len);
      return CAPI_V2_ENEEDMORE;
   }

   audio_dec_svc_capi_v1_wrapper_t *me = (audio_dec_svc_capi_v1_wrapper_t*)_pif;
   capi_v2_err_t result = set_param(me, param_id, params_ptr);

   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Capi_V1_dec_wrapper Set param for 0x%lx done", param_id);

   return result;
}

/** @ingroup capi_v2_virt_func_get_params
  Gets either a parameter value or a parameter structure containing multiple
  parameters.

  @datatypes
  capi_v2_t \n
  capi_v2_buf_t

  @param[in,out] _pif        Pointer to the module object.
  @param[in]     param_id    Parameter ID of the parameter whose value is
                             being passed in this function. For example:\n
                             - CAPI_V2_LIBNAME_ENABLE
                             - CAPI_V2_LIBNAME_FILTER_COEFF @tablebulletend
  @param[out]    params_ptr  Buffer containing the value of the parameter.
                             The format depends on the implementation.

  @detdesc
  In the event of a failure, the appropriate error code is to be
  returned.
  @par
  The max_data_len field of the parameter pointer must be at least the size
  of the parameter structure. Therefore, the following check must be
  exercised for each tuning parameter ID.
  @par
  @code
  if (params_ptr->max_data_len >= sizeof(asm_gain_struct_t))
  {
     :
     :
  }
  @endcode
  @newpage
  @code
  else
  {
     MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"CAPI_V2 Libname Get, Bad param size
     %lu",params_ptr->max_data_len);
     return CAPI_V2_ENEEDMORE;
  }
  @endcode
  @par
  Before returning, the actual_data_len field must be filled with the number
  of bytes written into the buffer.
  @par
  Optionally, some parameter values can be printed for tuning verification.
  @par
  Before returning SUCCESS and once the function is complete, a debug message
  is to be printed:
  @par
  @code
  MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"CAPI_V2 Libname Get param for 0x%lx done",param_id);
  @endcode

  @return
  Indication of success or failure.

  @dependencies
  None.
 */
static capi_v2_err_t audio_dec_svc_capi_v1_wrapper_get_param(capi_v2_t* _pif,
      uint32_t param_id,
      const capi_v2_port_info_t *port_info_ptr,
      capi_v2_buf_t *params_ptr)
{
   if (params_ptr->actual_data_len < sizeof(int))
   {
      MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_dec_wrapper Get, Param id 0x%lx Bad param size %lu", param_id, params_ptr->actual_data_len);
      return CAPI_V2_ENEEDMORE;
   }

   audio_dec_svc_capi_v1_wrapper_t *me = (audio_dec_svc_capi_v1_wrapper_t*)_pif;
   int res = 0;

   switch(param_id)
   {
   case CAPI_PARAM_DTMF_RUNNING:
   case CAPI_PARAM_DTMF_HIGH_TONE:
   case CAPI_PARAM_DTMF_LOW_TONE:
   case CAPI_PARAM_DTMF_DURATION:
   case CAPI_PARAM_DTMF_GAIN:
   case CAPI_PARAM_DTMF_SET:
   {
      if (me->media_fmt != ASM_MEDIA_FMT_DTMF)
      {
         MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "error setting param. fmt=0x%x,ulParamId=0x%x (not applicable for non-DTMF)",
               (unsigned int)me->media_fmt, (unsigned int)param_id);
      }
      int id = 0;
      switch(param_id)
      {
      case CAPI_PARAM_DTMF_RUNNING:
         id = CDtmfGeneratorLib::eDtmfRunning; break;
      case CAPI_PARAM_DTMF_HIGH_TONE:
         id = CDtmfGeneratorLib::eDtmfHighTone; break;
      case CAPI_PARAM_DTMF_LOW_TONE:
         id = CDtmfGeneratorLib::eDtmfLowTone; break;
      case CAPI_PARAM_DTMF_DURATION:
         id = CDtmfGeneratorLib::eDtmfDuration; break;
      case CAPI_PARAM_DTMF_GAIN:
         id = CDtmfGeneratorLib::eDtmfGain; break;
      case CAPI_PARAM_DTMF_SET:
         id = CDtmfGeneratorLib::eDtmfSet; break;
      }
      res = me->capi_v1_ptr->GetParam(id, (int*)(params_ptr->data_ptr));
      break;
   }
   default:
   {
      res = me->capi_v1_ptr->GetParam((int) param_id, (int*)(params_ptr->data_ptr));
   }
   }

   if (DEC_SUCCESS != res)
   {
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Capi_V1_dec_wrapper Get param for 0x%lx failed", param_id);
      return CAPI_V2_EFAILED;
   }

   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Capi_V1_dec_wrapper Get param for 0x%lx done", param_id);

   return CAPI_V2_EOK;
}

/** @ingroup
  Sets a list of property values.

  @datatypes
  capi_v2_t \n
  capi_v2_proplist_t

  @param[in,out] _pif        Pointer to the module object.
  @param[in]     props_ptr   Contains the property values to be set.

  @detdesc
  In the event of a failure, the appropriate error code is to be
  returned.
  @par
  Optionally, some property values can be printed for debugging.
  @par
  Before returning SUCCESS and once the function is complete, a debug message
  is to be printed:
  @par
  @code
  MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"CAPI_V2 Libname Set property for 0x%lx done", props_ptr->prop_ptr[i].id);
  @endcode

  @return
  None.

  @dependencies
  None.
 */
static capi_v2_err_t audio_dec_svc_capi_v1_wrapper_set_properties(capi_v2_t* _pif,
      capi_v2_proplist_t *props_ptr)
{
   if (!_pif)
   {
      return CAPI_V2_EBADPARAM;
   }

   audio_dec_svc_capi_v1_wrapper_t *me = (audio_dec_svc_capi_v1_wrapper_t*)_pif;

   return audio_dec_svc_capi_v1_wrapper_process_set_properties(me, props_ptr);
}

/** @ingroup
  Gets a list of property values.

  @datatypes
  capi_v2_t \n
  capi_v2_proplist_t

  @param[in,out] _pif        Pointer to the module object.
  @param[out]    props_ptr   Contains the empty structures that must be
                             filled with the appropriate property values
                             based on the property ids provided.

  @detdesc
  In the event of a failure, the appropriate error code is to be
  returned.
  @par
  Before returning SUCCESS and once the function is complete, a debug message
  is to be printed:
  @par
  @code
  MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"CAPI_V2 Libname Get property for 0x%lx done",props_ptr->prop_ptr[i].id);
  @endcode

  @return
  Indication of success or failure.

  @dependencies
  None.
 */
static capi_v2_err_t audio_dec_svc_capi_v1_wrapper_get_properties(capi_v2_t* _pif,
      capi_v2_proplist_t *props_ptr)
{
   if (!_pif)
   {
      return CAPI_V2_EBADPARAM;
   }

   audio_dec_svc_capi_v1_wrapper_t *me = (audio_dec_svc_capi_v1_wrapper_t*)_pif;

   return audio_dec_svc_capi_v1_wrapper_process_get_properties(me, props_ptr);
}

static capi_v2_err_t audio_dec_svc_capi_v1_wrapper_algorithmic_reset(audio_dec_svc_capi_v1_wrapper_t *me)
{
   return (capi_v2_err_t)init_capi(me, NULL);
}

static capi_v2_err_t audio_dec_svc_capi_v1_wrapper_process_set_properties(audio_dec_svc_capi_v1_wrapper_t* me, capi_v2_proplist_t *props_ptr)
{
   capi_v2_err_t result = CAPI_V2_EOK;

   capi_v2_prop_t *prop_ptr= props_ptr->prop_ptr;
   uint32_t i;

   for (i=0; i < props_ptr->props_num; i++)
   {
      capi_v2_buf_t *payload = &prop_ptr[i].payload;

      //Ignore prop_ptr[i].port_info.is_valid

      switch(prop_ptr[i].id)
      {
      case CAPI_V2_EVENT_CALLBACK_INFO:
      {
         if (payload->actual_data_len >= sizeof(capi_v2_event_callback_info_t))
         {
            capi_v2_event_callback_info_t *data_ptr = (capi_v2_event_callback_info_t*)payload->data_ptr;
            if (NULL == data_ptr)
            {
               payload->actual_data_len = 0; //according to CAPI V2, actual len should be num of bytes read(set)/written(get)
               return (result|CAPI_V2_EBADPARAM);
            }

            me->cb_info = *data_ptr;
         }
         else
         {
            MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_dec_wrapper Set, Param id 0x%lx Bad param size %lu", (uint32_t)prop_ptr[i].id, payload->actual_data_len);
            payload->actual_data_len = 0; //according to CAPI V2, actual len should be num of bytes read(set)/written(get)
            return (result|CAPI_V2_ENEEDMORE);
         }
         break;
      }
      case CAPI_V2_PORT_NUM_INFO:
      {
         if (payload->actual_data_len >= sizeof(capi_v2_port_num_info_t))
         {
            capi_v2_port_num_info_t *data_ptr = (capi_v2_port_num_info_t*)payload->data_ptr;

            if ((data_ptr->num_input_ports > AUDIO_DEC_SVC_CAPI_V1_WRAPPER_MAX_IN_PORTS) ||
                  (data_ptr->num_output_ports > AUDIO_DEC_SVC_CAPI_V1_WRAPPER_MAX_OUT_PORTS) )
            {
               MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_dec_wrapper Set, Param id 0x%lx num of in and out ports cannot be port than 1", (uint32_t)prop_ptr[i].id);
               payload->actual_data_len = 0; //according to CAPI V2, actual len should be num of bytes read(set)/written(get)
               return (result|CAPI_V2_EBADPARAM);
            }
         }
         else
         {
            MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_dec_wrapper Set, Param id 0x%lx Bad param size %lu", (uint32_t)prop_ptr[i].id, payload->actual_data_len);
            payload->actual_data_len = 0; //according to CAPI V2, actual len should be num of bytes read(set)/written(get)
            return (result|CAPI_V2_ENEEDMORE);
         }
         break;
      }
      case CAPI_V2_HEAP_ID:
      {
         if (payload->actual_data_len >= sizeof(capi_v2_heap_id_t))
         {
            //capi_v2_heap_id_t *data_ptr = (capi_v2_heap_id_t*)payload->data_ptr;
            MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_dec_wrapper Set, Param id 0x%lx unsupported", (uint32_t)prop_ptr[i].id);
            payload->actual_data_len = 0; //according to CAPI V2, actual len should be num of bytes read(set)/written(get)
            return (result|CAPI_V2_EUNSUPPORTED);
         }
         else
         {
            MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_dec_wrapper Set, Param id 0x%lx Bad param size %lu", (uint32_t)prop_ptr[i].id, payload->actual_data_len);
            payload->actual_data_len = 0; //according to CAPI V2, actual len should be num of bytes read(set)/written(get)
            return (result|CAPI_V2_ENEEDMORE);
         }
         break;
      }
      case CAPI_V2_ALGORITHMIC_RESET:
      {
         result |= audio_dec_svc_capi_v1_wrapper_algorithmic_reset(me);
         break;
      }
      case CAPI_V2_OUTPUT_MEDIA_FORMAT:
      {
         if (payload->actual_data_len >= sizeof(audio_dec_svc_capi_v2_media_fmt_t))
         {
            audio_dec_svc_capi_v2_media_fmt_t *data_ptr = (audio_dec_svc_capi_v2_media_fmt_t*)payload->data_ptr;
            if (NULL == me || NULL == me->capi_v1_ptr || NULL == data_ptr ||
                  (prop_ptr[i].port_info.is_valid && prop_ptr[i].port_info.port_index !=0 ) || (data_ptr->main.format_header.data_format != CAPI_V2_FIXED_POINT))
            {
               MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_dec_wrapper Set, failed to set Param id 0x%lx due to invalid/unexpected values", (uint32_t)prop_ptr[i].id);
               payload->actual_data_len = 0; //according to CAPI V2, actual len should be num of bytes read(set)/written(get)
               return (result|CAPI_V2_EFAILED);
            }

            //bits per sample is set.
            if (data_ptr->std.bits_per_sample != CAPI_V2_DATA_FORMAT_INVALID_VAL)
            {
               MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Capi_V1_dec_wrapper Set, bits per sample %lu", data_ptr->std.bits_per_sample);

               int dec_result = me->capi_v1_ptr->SetParam(eIcapiBitsPerSample, data_ptr->std.bits_per_sample);
               if ((DEC_BADPARAM_FAILURE == dec_result) || (ENC_BADPARAM_FAILURE == dec_result))
               {
                  //ignoring to be backward compatible (Eg. Mp2 dec returns fail to all set param, but we shouldn't fail dec create)
                  //dec svc anyway query bps and adjust if needed.
                  result |= CAPI_V2_EOK;
               }
               else if (DEC_SUCCESS != dec_result)
               {
                  MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_dec_wrapper Set, failed to set Param id 0x%lx (bits per sample). res = %d. Ignoring.",
                        (uint32_t)prop_ptr[i].id, dec_result);
                  payload->actual_data_len = 0; //according to CAPI V2, actual len should be num of bytes read(set)/written(get)
                  return (result|CAPI_V2_EFAILED);
               }
            }
            if (data_ptr->std.num_channels != CAPI_V2_DATA_FORMAT_INVALID_VAL)
            {
               MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Capi_V1_dec_wrapper Set, channels  %lu", data_ptr->std.num_channels);

               int dec_result = DEC_SUCCESS;

               //setting eIcapiNumChannels confuses some CAPIs such as WMApro to think its input num ch has changed (instead of output)
               //dec_result |= me->capi_v1_ptr->SetParam(eIcapiNumChannels, data_ptr->std.num_channels);

               CAPI_ChannelMap_t chan_map;
               chan_map.nChannels = data_ptr->std.num_channels;
               bool_t channelmap_valid = true;
               for (unsigned int i=0; i<chan_map.nChannels;i++)
               {
                  if (data_ptr->std.channel_type[i] != (uint16_t)CAPI_V2_DATA_FORMAT_INVALID_VAL)
                  {
                     chan_map.nChannelMap[i] = data_ptr->std.channel_type[i];
                  }
                  else
                  {
                     channelmap_valid = false;
                  }
               }

               if (channelmap_valid)
               {
                  dec_result |= me->capi_v1_ptr->SetParam(eIcapiOutputChanMap, (int)&chan_map);
               }

               if (DEC_SUCCESS != dec_result)
               {
                  payload->actual_data_len = 0; //according to CAPI V2, actual len should be num of bytes read(set)/written(get)
                  result |= CAPI_V2_EFAILED;
               }
            }
         }
         else
         {
            MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_dec_wrapper Set, Param id 0x%lx Bad param size %lu", (uint32_t)prop_ptr[i].id, payload->actual_data_len);
            payload->actual_data_len = 0; //according to CAPI V2, actual len should be num of bytes read(set)/written(get)
            return (result|CAPI_V2_ENEEDMORE);
         }
         break;
      }
      case CAPI_V2_INPUT_MEDIA_FORMAT:
      {
         if (payload->actual_data_len >= (sizeof(capi_v2_set_get_media_format_t)))
         {
            capi_v2_set_get_media_format_t *data_ptr = (capi_v2_set_get_media_format_t*)payload->data_ptr;

            if (NULL == me || NULL == me->capi_v1_ptr || NULL == data_ptr ||
                  (prop_ptr[i].port_info.is_valid && prop_ptr[i].port_info.port_index !=0 ) )
            {
               MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_dec_wrapper Set, failed to set Param id 0x%lx due to invalid/unexpected values",
                     (uint32_t)prop_ptr[i].id);
               payload->actual_data_len = 0; //according to CAPI V2, actual len should be num of bytes read(set)/written(get)
               return CAPI_V2_EFAILED;
            }

            result |= update_in_media_fmt_blk(me, payload);
         }
         else
         {
            MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_dec_wrapper Set, Param id 0x%lx Bad param size %lu", (uint32_t)prop_ptr[i].id, payload->actual_data_len);
            payload->actual_data_len = 0; //according to CAPI V2, actual len should be num of bytes read(set)/written(get)
            return (result|CAPI_V2_ENEEDMORE);
         }
         break;
      }
      default:
      {
         MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Capi_V1_dec_wrapper Set property for 0x%x. Not supported.",prop_ptr[i].id);
         payload->actual_data_len = 0;
         result |= CAPI_V2_EUNSUPPORTED;
      }
      }

      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Capi_V1_dec_wrapper Set property for 0x%x done", prop_ptr[i].id);
   }

   return result;
}

static capi_v2_err_t audio_dec_svc_capi_v1_wrapper_process_get_properties(audio_dec_svc_capi_v1_wrapper_t* me, capi_v2_proplist_t *props_ptr)
{
   capi_v2_err_t result = CAPI_V2_EOK;

   capi_v2_prop_t *prop_ptr= props_ptr->prop_ptr;
   uint32_t i;
   for (i=0; i < props_ptr->props_num; i++)
   {
      capi_v2_buf_t *payload = &prop_ptr[i].payload;
      //Ignore prop_ptr[i].port_info.is_valid

      switch(prop_ptr[i].id)
      {
      case CAPI_V2_INIT_MEMORY_REQUIREMENT:
      {
         if (payload->max_data_len >= sizeof(capi_v2_init_memory_requirement_t))
         {
            capi_v2_init_memory_requirement_t *data_ptr = (capi_v2_init_memory_requirement_t*)payload->data_ptr;
            data_ptr->size_in_bytes = sizeof(audio_dec_svc_capi_v1_wrapper_t);
            payload->actual_data_len = sizeof(capi_v2_init_memory_requirement_t);
         }
         else
         {
            MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_dec_wrapper Get, Param id 0x%lx Bad param size %lu", (uint32_t)prop_ptr[i].id, payload->max_data_len);
            payload->actual_data_len = 0; //according to CAPI V2, actual len should be num of bytes read(set)/written(get)
            return (result|CAPI_V2_ENEEDMORE);
         }
         break;
      }
      case CAPI_V2_STACK_SIZE:
      {
         if (payload->max_data_len >= sizeof(capi_v2_stack_size_t))
         {
            capi_v2_stack_size_t *data_ptr = (capi_v2_stack_size_t*)payload->data_ptr;

            int res, stack_size;

            if (NULL == me || NULL == me->capi_v1_ptr)
            {
               MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_dec_wrapper Get, Param id 0x%lx Wrapper or the ICAPI must be created", (uint32_t)prop_ptr[i].id);
               payload->actual_data_len = 0; //according to CAPI V2, actual len should be num of bytes read(set)/written(get)
               return (result|CAPI_V2_EFAILED);
            }

            res = me->capi_v1_ptr->GetParam(eIcapiThreadStackSize, &stack_size);
            if (DEC_SUCCESS != res)
            {
               MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_dec_wrapper Get, Param id 0x%lx ICAPI returned failure %d", (uint32_t)prop_ptr[i].id, res);
               stack_size = AUDIO_DEC_SVC_CAPI_V1_WRAPPER_STACK_SIZE;
            }
            data_ptr->size_in_bytes = (uint32_t)stack_size;
            payload->actual_data_len = sizeof(capi_v2_stack_size_t);
         }
         else
         {
            MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_dec_wrapper Get, Param id 0x%lx Bad param size %lu", (uint32_t)prop_ptr[i].id, payload->max_data_len);
            payload->actual_data_len = 0; //according to CAPI V2, actual len should be num of bytes read(set)/written(get)
            return (result|CAPI_V2_ENEEDMORE);
         }
         break;
      }
      case CAPI_V2_MAX_METADATA_SIZE:
      {
         if (payload->max_data_len >= sizeof(capi_v2_max_metadata_size_t))
         {
            capi_v2_max_metadata_size_t *data_ptr = (capi_v2_max_metadata_size_t*)payload->data_ptr;

            int metadata_size;

            if (NULL == me || NULL == me->capi_v1_ptr)
            {
               MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_dec_wrapper Get, Param id 0x%lx Wrapper or the ICAPI must be created", (uint32_t)prop_ptr[i].id);
               payload->actual_data_len = 0; //according to CAPI V2, actual len should be num of bytes read(set)/written(get)
               return (result|CAPI_V2_EFAILED);
            }

            metadata_size= 0;
            me->capi_v1_ptr->GetParam(eIcapiMaxMetaDataSize, &metadata_size);
            //ignore res to be backward compatible.

            data_ptr->size_in_bytes = (uint32_t)metadata_size;
            payload->actual_data_len = sizeof(capi_v2_max_metadata_size_t);
         }
         else
         {
            MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_dec_wrapper Get, Param id 0x%lx Bad param size %lu", (uint32_t)prop_ptr[i].id, payload->max_data_len);
            payload->actual_data_len = 0; //according to CAPI V2, actual len should be num of bytes read(set)/written(get)
            return (result|CAPI_V2_ENEEDMORE);
         }
         break;
      }
      case CAPI_V2_IS_INPLACE:
      {
         if (payload->max_data_len >= sizeof(capi_v2_is_inplace_t))
         {
            capi_v2_is_inplace_t *data_ptr = (capi_v2_is_inplace_t*)payload->data_ptr;
            data_ptr->is_inplace = FALSE;
            payload->actual_data_len = sizeof(capi_v2_is_inplace_t);
         }
         else
         {
            MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_dec_wrapper Get, Param id 0x%lx Bad param size %lu", (uint32_t)prop_ptr[i].id, payload->max_data_len);
            payload->actual_data_len = 0; //according to CAPI V2, actual len should be num of bytes read(set)/written(get)
            return (result|CAPI_V2_ENEEDMORE);
         }
         break;
      }
      case CAPI_V2_REQUIRES_DATA_BUFFERING:
      {
         if (payload->max_data_len >= sizeof(capi_v2_requires_data_buffering_t))
         {
            capi_v2_requires_data_buffering_t *data_ptr = (capi_v2_requires_data_buffering_t*)payload->data_ptr;
            data_ptr->requires_data_buffering = TRUE;
            payload->actual_data_len = sizeof(capi_v2_requires_data_buffering_t);
         }
         else
         {
            MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_dec_wrapper Get, Param id 0x%lx Bad param size %lu", (uint32_t)prop_ptr[i].id, payload->max_data_len);
            payload->actual_data_len = 0; //according to CAPI V2, actual len should be num of bytes read(set)/written(get)
            return (result|CAPI_V2_ENEEDMORE);
         }
         break;
      }
      case CAPI_V2_OUTPUT_MEDIA_FORMAT_SIZE:
      {
         if (payload->max_data_len >= sizeof(capi_v2_output_media_format_size_t))
         {
            capi_v2_output_media_format_size_t *data_ptr = (capi_v2_output_media_format_size_t*)payload->data_ptr;

            if (NULL == me || NULL == me->capi_v1_ptr )
            {
               MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_dec_wrapper Get, Param id 0x%lx Wrapper or the ICAPI must be created", (uint32_t)prop_ptr[i].id);
               payload->actual_data_len = 0; //according to CAPI V2, actual len should be num of bytes read(set)/written(get)
               return (result|CAPI_V2_EFAILED);
            }
            data_ptr->size_in_bytes = sizeof(capi_v2_standard_data_format_t);
            payload->actual_data_len = sizeof(capi_v2_output_media_format_size_t);
         }
         else
         {
            MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_dec_wrapper Get, Param id 0x%lx Bad param size %lu", (uint32_t)prop_ptr[i].id, payload->max_data_len);
            payload->actual_data_len = 0; //according to CAPI V2, actual len should be num of bytes read(set)/written(get)
            return (result|CAPI_V2_ENEEDMORE);
         }
         break;
      }
      case CAPI_V2_OUTPUT_MEDIA_FORMAT:
      {
         if (payload->max_data_len >= sizeof(audio_dec_svc_capi_v2_media_fmt_t))
         {
            audio_dec_svc_capi_v2_media_fmt_t *data_ptr = (audio_dec_svc_capi_v2_media_fmt_t*)payload->data_ptr;

            if (NULL == me || NULL == me->capi_v1_ptr || NULL == data_ptr ||
                  (prop_ptr[i].port_info.is_valid && prop_ptr[i].port_info.port_index !=0 ))
            {
               MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_dec_wrapper Get, failed to get Param id 0x%lx due to invalid/unexpected values", (uint32_t)prop_ptr[i].id);
               payload->actual_data_len = 0; //according to CAPI V2, actual len should be num of bytes read(set)/written(get)
               return (result|CAPI_V2_EFAILED);
            }

            *data_ptr = me->out_media_fmt[0];

            payload->actual_data_len = sizeof(audio_dec_svc_capi_v2_media_fmt_t);
         }
         else
         {
            MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_dec_wrapper Get, Param id 0x%lx Bad param size %lu", (uint32_t)prop_ptr[i].id, payload->max_data_len);
            payload->actual_data_len = 0; //according to CAPI V2, actual len should be num of bytes read(set)/written(get)
            return (result|CAPI_V2_ENEEDMORE);
         }
         break;
      }
      case CAPI_V2_METADATA:
      {
         if (payload->max_data_len >= sizeof(capi_v2_metadata_t))
         {
            capi_v2_metadata_t *data_ptr = (capi_v2_metadata_t*)payload->data_ptr;

            if (NULL == me || NULL == me->capi_v1_ptr || NULL == data_ptr )
            {
               MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_dec_wrapper Get, failed to get Param id 0x%lx due to invalid/unexpected values", (uint32_t)prop_ptr[i].id);
               payload->actual_data_len = 0; //according to CAPI V2, actual len should be num of bytes read(set)/written(get)
               return (result|CAPI_V2_EFAILED);
            }

            int metadata_size = 0;

            //query if CAPI has some meta-data to send now.
            int res = me->capi_v1_ptr->GetParam(eIcapiActualMetaDataSize ,&metadata_size);
            if ((res != DEC_SUCCESS) || (metadata_size <= 0))
            {
               //return if not or if CAPI fails to get param.
               payload->actual_data_len = 0; //according to CAPI V2, actual len should be num of bytes read(set)/written(get)
               return result;
            }

            capi_v2_buf_t *metadata_ptr = &data_ptr->payload;
            if (metadata_ptr->max_data_len >= (uint32_t)metadata_size)
            {
               //ask CAPI to populate metadata into the ptr
               res =me->capi_v1_ptr->GetParam(eIcapiMetaDataPtr ,(int*)metadata_ptr->data_ptr);
               if (CAPI_V2_FAILED(res))
               {
                  MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "AudioDecSvc: error getting metadata");
                  payload->actual_data_len = 0; //according to CAPI V2, actual len should be num of bytes read(set)/written(get)
                  return (result|CAPI_V2_EFAILED);
               }
               metadata_ptr->actual_data_len = metadata_size;
            }
            else
            {
               metadata_ptr->actual_data_len = 0;
            }
         }
         else
         {
            MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_dec_wrapper Get, Param id 0x%lx Bad param size %lu", (uint32_t)prop_ptr[i].id, payload->max_data_len);
            payload->actual_data_len = 0; //according to CAPI V2, actual len should be num of bytes read(set)/written(get)
            return (result|CAPI_V2_ENEEDMORE);
         }
         break;
      }
      case CAPI_V2_PORT_DATA_THRESHOLD:
      {
         if (payload->max_data_len >= sizeof(capi_v2_port_data_threshold_t))
         {
            capi_v2_port_data_threshold_t *data_ptr = (capi_v2_port_data_threshold_t*)payload->data_ptr;
            if (!prop_ptr[i].port_info.is_valid)
            {
               MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_dec_wrapper Get, port id not valid");
               payload->actual_data_len = 0; //according to CAPI V2, actual len should be num of bytes read(set)/written(get)
               return (result|CAPI_V2_EBADPARAM);
            }
            if (prop_ptr[i].port_info.is_input_port)
            {
               if (0 != prop_ptr[i].port_info.port_index)
               {
                  MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_dec_wrapper Get, Param id 0x%lx max input port is 1. asking for %lu", (uint32_t)prop_ptr[i].id, prop_ptr[i].port_info.port_index);
                  payload->actual_data_len = 0; //according to CAPI V2, actual len should be num of bytes read(set)/written(get)
                  return (result|CAPI_V2_EBADPARAM);
               }

               int res, input_buf_size;

               if (NULL == me || NULL == me->capi_v1_ptr)
               {
                  MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_dec_wrapper Get, Param id 0x%lx Wrapper or the ICAPI must be created", (uint32_t)prop_ptr[i].id);
                  payload->actual_data_len = 0; //according to CAPI V2, actual len should be num of bytes read(set)/written(get)
                  return (result|CAPI_V2_EFAILED);
               }

               res = me->capi_v1_ptr->GetParam(eIcapiInputBufferSize, &input_buf_size);
               if (DEC_SUCCESS != res)
               {
                  MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_dec_wrapper Get, Param id 0x%lx ICAPI returned failure %d", (uint32_t)prop_ptr[i].id, res);
                  payload->actual_data_len = 0; //according to CAPI V2, actual len should be num of bytes read(set)/written(get)
                  return (result|CAPI_V2_EUNSUPPORTED);
               }

               data_ptr->threshold_in_bytes = input_buf_size;
            }
            else
            {
               if (0 != prop_ptr[i].port_info.port_index)
               {
                  MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_dec_wrapper Get, Param id 0x%lx max output port is 1. asking for %lu", (uint32_t)prop_ptr[i].id, prop_ptr[i].port_info.port_index);
                  payload->actual_data_len = 0; //according to CAPI V2, actual len should be num of bytes read(set)/written(get)
                  return (result|CAPI_V2_EBADPARAM);
               }
               int res, output_buf_size;

               if (NULL == me || NULL == me->capi_v1_ptr)
               {
                  MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_dec_wrapper Get, Param id 0x%lx Wrapper or the ICAPI must be created", (uint32_t)prop_ptr[i].id);
                  payload->actual_data_len = 0; //according to CAPI V2, actual len should be num of bytes read(set)/written(get)
                  return CAPI_V2_EFAILED;
               }

               res = me->capi_v1_ptr->GetParam(eIcapiOutputBufferSize, &output_buf_size);
               if (DEC_SUCCESS != res)
               {
                  MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_dec_wrapper Get, Param id 0x%lx ICAPI returned failure %d", (uint32_t)prop_ptr[i].id, res);
                  payload->actual_data_len = 0; //according to CAPI V2, actual len should be num of bytes read(set)/written(get)
                  return (result|CAPI_V2_EUNSUPPORTED);
               }

               //Capi V1 is assumed to return for 16 bits.
               int bits_per_sample = 16;
               res = me->capi_v1_ptr->GetParam(eIcapiBitsPerSample, &bits_per_sample);
               if (res != DEC_SUCCESS)
               {
                  MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Capi_V1_dec_wrapper Set, didn't return bits per sample. assuming 16.");
               }

               if (bits_per_sample != 16)
               {
                  output_buf_size *= 2;
               }

               data_ptr->threshold_in_bytes = output_buf_size;
            }
            payload->actual_data_len = sizeof(capi_v2_port_data_threshold_t);
         }
         else
         {
            MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_dec_wrapper Get, Param id 0x%lx Bad param size %lu", (uint32_t)prop_ptr[i].id, payload->max_data_len);
            payload->actual_data_len = 0; //according to CAPI V2, actual len should be num of bytes read(set)/written(get)
            return (result|CAPI_V2_ENEEDMORE);
         }
         break;
      }
      default:
      {
         MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Capi_V1_dec_wrapper Get property for 0x%x. Not supported.",prop_ptr[i].id);
         payload->actual_data_len = 0;
         result |= CAPI_V2_EUNSUPPORTED;
      }
      }

      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Capi_V1_dec_wrapper Get property for 0x%x done",prop_ptr[i].id);
   }

   return result;
}

/** @ingroup
Used to query for static properties of the module that are independent of the
instance. This function is used to query the memory requirements of the module
in order to create an instance. The same properties that are sent to the module
in the call to init() are also sent to this function to enable the module to
calculate the memory requirement.

@param[in]  init_set_properties  The same properties that will be sent in the
                         call to the init() function.
@param[out] static_properties   Pointer to the structure that the module must
                         fill with the appropriate values based on the property
                         id.

@return
Indication of success or failure.

@dependencies
None.
 */
capi_v2_err_t audio_dec_svc_capi_v1_wrapper_get_static_properties (
      capi_v2_proplist_t *init_set_properties,
      capi_v2_proplist_t *static_properties)
{
   capi_v2_err_t result = CAPI_V2_EOK;

   if (NULL != static_properties)
   {
      result = audio_dec_svc_capi_v1_wrapper_process_get_properties((audio_dec_svc_capi_v1_wrapper_t*)NULL, static_properties);
      if (result != CAPI_V2_EOK)
      {
         return result;
      }
   }

   if (NULL != init_set_properties)
   {
      //ignore currently.
   }

   return result;
}

/** @ingroup
Instantiates the module to set up the virtual function table, and also
allocates any memory required by the module. States within the module must
be initialized at the same time.

@datatypes
capi_v2_t \n
capi_v2_proplist_t \n
capi_v2_event_cb \n
void *

@param[in,out] _pif            Pointer to the module object.
@param[in]    init_set_properties Properties set by the service to be used
                              while init().

@return
Indication of success or failure.

@dependencies
 */
capi_v2_err_t audio_dec_svc_capi_v1_wrapper_init (capi_v2_t*  _pif,
      capi_v2_proplist_t      *init_set_properties)
{
   capi_v2_err_t result = CAPI_V2_EOK;
   if (!_pif)
   {
      return CAPI_V2_EBADPARAM;
   }

   audio_dec_svc_capi_v1_wrapper_t *me = (audio_dec_svc_capi_v1_wrapper_t*)_pif;

   if ((NULL == me->capi_v1_ptr))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Capi_V1_dec_wrapper Capi creation failed");
      return CAPI_V2_ENOMEMORY;
   }

   init_media_fmt(me);

   if (NULL != init_set_properties) //should contain OUT_BITS_PER_SAMPLE,EVENT_CALLBACK_INFO, PORT_INFO
   {
      result = audio_dec_svc_capi_v1_wrapper_process_set_properties(me, init_set_properties);

      if (CAPI_V2_IS_ERROR_CODE_SET(result, CAPI_V2_EUNSUPPORTED))
      {
         MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Capi_V1_dec_wrapper ignoring unsupported prop error during init");
         result &= ~CAPI_V2_EUNSUPPORTED; //clear UNSUPPORTED prop errors as these are ok.
      }

      if (CAPI_V2_EOK != result)
      {
         return result;
      }
   }

   uint32_t ulWaitForMediaFmt = 1;

   switch( me->media_fmt )
   {
   case ASM_MEDIA_FMT_EVRC_FS:
   case ASM_MEDIA_FMT_V13K_FS:
   case ASM_MEDIA_FMT_EVRCB_FS:
   case ASM_MEDIA_FMT_AMRNB_FS:
   case ASM_MEDIA_FMT_G711_ALAW_FS:
   case ASM_MEDIA_FMT_G711_MLAW_FS:
   case ASM_MEDIA_FMT_EVRCWB_FS:
   case ASM_MEDIA_FMT_AMRWB_FS:
   case ASM_MEDIA_FMT_FR_FS:
   {
      CAPI_Buf_t     Params;
      VoiceInitParamsType VInitParams;
      Params.Data = (char*)&VInitParams;
      Params.nActualDataLen = sizeof(VoiceInitParamsType);
      Params.nMaxDataLen = sizeof(VoiceInitParamsType);
      VInitParams.VocTypeID = me->media_fmt;
      VInitParams.VocConfigBlkSize = 0;
      VInitParams.VocConfigBlk = NULL;

      //Initialize CAPI
      int dec_result = init_capi(me, &Params);
      if (DEC_SUCCESS != dec_result)
      {
         result = CAPI_V2_EFAILED;
      }

      int res = me->capi_v1_ptr->GetParam(eIcapiWaitForFormatBlock, (int *)&(ulWaitForMediaFmt));
      if (DEC_SUCCESS != res)
      {
         MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Capi_V1_dec_wrapper. Capi didn't return eIcapiWaitForFormatBlock successfully. assuming false.");
         ulWaitForMediaFmt = 0;
      }
      else
      {
         MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Capi_V1_dec_wrapper. eIcapiWaitForFormatBlock %lu. (this may be overridden if data is sent first)", ulWaitForMediaFmt);
      }

      break;
   }
   default:
   {
      int res = me->capi_v1_ptr->GetParam(eIcapiWaitForFormatBlock, (int *)&(ulWaitForMediaFmt));
      if (DEC_SUCCESS != res)
      {
         MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Capi_V1_dec_wrapper. Capi didn't return eIcapiWaitForFormatBlock successfully. assuming false.");
         ulWaitForMediaFmt = 0;
      }
      else
      {
         MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Capi_V1_dec_wrapper. eIcapiWaitForFormatBlock %lu. (this may be overridden if data is sent first)", ulWaitForMediaFmt);
      }

      if (ulWaitForMediaFmt == 0)
      {
         int dec_result = init_capi(me, NULL);
         if (DEC_SUCCESS != dec_result)
         {
            result = CAPI_V2_EFAILED;
         }
      }
   }
   }
   me->wait_for_media_fmt = ulWaitForMediaFmt!=0;

   int val=0;
   int res =  me->capi_v1_ptr->GetParam(eIcapiKCPSRequired, &val);
   if (res != CAPI_V2_EOK)
   {
      val = AUDIO_DEC_SVC_CAPI_V1_WRAPPER_DEFAULT_KPPS;
   }

   {
      int dec_result = check_raise_kpps_event(me, val);
      if (DEC_SUCCESS != dec_result)
      {
         result = CAPI_V2_EFAILED;
      }
   }


   {
      int dec_result = check_raise_bw_event(me, AUDIO_DEC_SVC_CAPI_V1_WRAPPER_DEFAULT_CODE_BW, determine_data_bw(me));
      if (DEC_SUCCESS != dec_result)
      {
         result = CAPI_V2_EFAILED;
      }
   }


   if (CAPI_V2_FAILED(result))   return CAPI_V2_EFAILED;

   me->prev_eof_flag = false;
   me->prev_eos_flag = false;

   me->vtbl.vtbl_ptr = &vtbl;

   return CAPI_V2_EOK;
}
