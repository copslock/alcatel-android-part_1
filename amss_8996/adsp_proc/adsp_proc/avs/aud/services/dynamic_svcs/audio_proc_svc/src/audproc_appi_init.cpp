/*==========================================================================
ELite Source File

This file implements the COPP topology.

Copyright (c) 2013 QUALCOMM Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Technologies Proprietary.  Export of this technology or software is regulated
by the U.S. Government, Diversion contrary to U.S. law prohibited.
*/

/*===========================================================================
Edit History

when       who     what, where, why
--------   ---     ---------------------------------------------------------
03/28/11   ss      Introducing PROCESS_CHECK for Resampler and Downmix (APPI Rev D)
01/28/11   DG      Removed all individual init functions; the topologies are
                   initialized using constant tables now.
01/12/11   ss      Delay accumulation mechanism for EOS, so that all the
                   intermediate buffers are flushed out before processing EOS
1/4/11     DG      Removed all functions except initialization, since the
                   generic topology handles everything else.
11/17/10   ss      Introducing APPI Rev B
06/30/10   DG      Created file.
=========================================================================== */

/*---------------------------------------------------------------------------
* Include Files
* -------------------------------------------------------------------------*/
#include "audproc_comdef.h"
#include "audpp_util.h"
#include "audproc_appi_topo.h"
#include "AdspCoreSvc.h"

/*---------------------------------------------------------------------------
 * Definitions and Constants
 * -------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
* Function Declarations
* -------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
 * Globals
 * -------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------
 * Function Definitions
 * -------------------------------------------------------------------------*/

ADSPResult init_hpf_coeffs(topo_struct *topo_obj)
{
   static const uint16_t num_stages = 3;
   static const uint16_t coeffs_per_stage = 5;

   struct hpf_coeff_data_struct
   {
      asm_stream_param_data_v2_t header;
      audproc_iir_filter_config_params_t config_params;
      int32_t filter_coeffs[num_stages * coeffs_per_stage];
      int16_t num_shift_factor[num_stages];
      uint16_t pan_setting[num_stages];
   } hpf_init_data;

   hpf_init_data.header.module_id = AUDPROC_MODULE_ID_HPF_IIR_TX_FILTER;
   hpf_init_data.header.param_id  = AUDPROC_PARAM_ID_HPF_IIR_TX_FILTER_CONFIG_PARAMS;
   hpf_init_data.header.param_size = sizeof(hpf_init_data) - sizeof(hpf_init_data.header);

   hpf_init_data.config_params.num_biquad_stages = num_stages;
   hpf_init_data.config_params.reserved          = 0;

   // initialize counter
   int16_t k=0;
   // For HPF the default coefficients set here
   // Filter coeff setting for band = 1
   hpf_init_data.filter_coeffs[ ( k * 5)]     = 0x3FDF73F8;
   hpf_init_data.filter_coeffs[ ( k * 5) + 1] = 0x80413203;
   hpf_init_data.filter_coeffs[ ( k * 5) + 2] = 0x3FDF73F8;
   hpf_init_data.filter_coeffs[ ( k * 5) + 3] = 0x804187F5;
   hpf_init_data.filter_coeffs[ ( k * 5) + 4] = 0x3FBF3DE2;

   // Filter coeff setting for band = 2
   k+=1;
   hpf_init_data.filter_coeffs[ ( k * 5)]     = 0x3FAB5548;
   hpf_init_data.filter_coeffs[ ( k * 5) + 1] = 0x80A95F52;
   hpf_init_data.filter_coeffs[ ( k * 5) + 2] = 0x3FAB5548;
   hpf_init_data.filter_coeffs[ ( k * 5) + 3] = 0x80A9B4FE;
   hpf_init_data.filter_coeffs[ ( k * 5) + 4] = 0x3F57003B;

   // Filter coeff setting for band = 3
   k+=1;
   hpf_init_data.filter_coeffs[ ( k * 5)]     = 0x3FCBAC3A;
   hpf_init_data.filter_coeffs[ ( k * 5) + 1] = 0xC03453C6;
   hpf_init_data.filter_coeffs[ ( k * 5) + 2] = 0;
   hpf_init_data.filter_coeffs[ ( k * 5) + 3] = 0xC068A78C;
   hpf_init_data.filter_coeffs[ ( k * 5) + 4] = 0;

   for (uint32_t i = 0; i < num_stages; i++)
   {
      hpf_init_data.num_shift_factor[i] = 2;
      hpf_init_data.pan_setting[i] = 1;
   }

   return topo_set_param(topo_obj, &hpf_init_data, sizeof(hpf_init_data));
}

ADSPResult init_spa_queue( topo_struct *topo_obj,
      qurt_elite_queue_t *spa_queue_ptr,
      uint32_t mem_map_client)
{
   struct spa_set_queue_struct
   {
      asm_stream_param_data_v2_t header;
      spa_queue_params_t payload;
   } queue_params;

   queue_params.header.module_id  = AUDPROC_MODULE_ID_SPA;
   queue_params.header.param_id   = AUDPROC_PARAM_ID_SPA_QUEUE;
   queue_params.header.param_size = sizeof(queue_params.payload);
   queue_params.header.reserved   = 0;

   queue_params.payload.mem_map_client      = mem_map_client;
   queue_params.payload.spa_input_queue_ptr = spa_queue_ptr;

   return topo_set_param(topo_obj, &queue_params, sizeof(queue_params));
}

