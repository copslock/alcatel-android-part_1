
/*==========================================================================
ELite Source File

This file implements the packet parsing functions for the Audio Post
Processing Dynamic Service.

Copyright (c) 2009-2015 QUALCOMM Technologies Incorporated.
All Rights Reserved.
QUALCOMM Technologies Proprietary.
Export of this technology or software is regulated
by the U.S. Government, Diversion contrary to U.S. law prohibited.
 */

/*===========================================================================
Edit History

when       who     what, where, why
--------   ---     ---------------------------------------------------------
1/3/2011   PC      Removing TSM and making POPP appi compliant
10/24/10   DG      Added support for ASM_MEDIA_FMT_MULTI_CHANNEL_PCM
09/09/10   SS      Adding data tapping support (Sim) for Postprocessing
06/09/10   DG      Created file and copied functions from AudDynaPPSvc.cpp.
=========================================================================== */

/*---------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include "audproc_comdef.h"
#include "AudioStreamMgr_GetSetBits.h"
#include "EliteTopology_db_if.h"
#include "audproc_appi_topo.h"
#include "AFEInterface.h"

/*---------------------------------------------------------------------------
 * Definitions and Constants
 * -------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
 * Globals
 * -------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------
 * Function Declarations
 * -------------------------------------------------------------------------*/
static bool_t AudPP_isBufferFull(const AudPP_BufInfo_t *pBufInfo);
static AdspAudioMediaFormatInfo_t AudPP_ParseMultichannelPCMMediaFmt(const elite_multi_channel_pcm_fmt_blk_t *pFormat);
static AdspAudioMediaFormatInfo_t AudPP_ParseCompressedMediaFmt(const elite_compressed_fmt_blk_t *pFormat);
static void AudPP_SoftPause_Terminate(ThisAudDynaPPSvc_t *me);
static bool AudPP_isDevicePath(const ThisAudDynaPPSvc_t *me);
static void AudPP_DataProc_SetParam(ThisAudDynaPPSvc_t *me);
static void AudPP_DataProc_SendZerBuffer(ThisAudDynaPPSvc_t* me);

/*---------------------------------------------------------------------------
 * Function Definitions
 * -------------------------------------------------------------------------*/

/**
 * Goes through the media type packet and converts to the internal
 * media format structure. All dependence
 * on packet format is contained in this function. Currently it
 * accepts APR packets only.
 *   - Dependencies: None
 *   - Side Effects: None
 *   - Re-entrant: Yes
 *
 * @param[in, out] me
 * Pointer to the current instance structure.
 * @param[in] pMediaFormatData
 * Pointer to the payload of the media type packet.
 * @param[in] pMediaFmt
 * Pointer to the media format structure which will be populated.
 *
 * @return ADSPResult
 * Return code to indicate status.
 */
ADSPResult AudPP_ParseMediaTypePacket(ThisAudDynaPPSvc_t *me, elite_msg_data_media_type_apr_t *pMediaFormatData, AdspAudioMediaFormatInfo_t *pMediaFmt)
{
   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: Parse Media type Begin", me->objId);

   if (pMediaFormatData->unMediaTypeFormat != ELITEMSG_DATA_MEDIA_TYPE_APR)
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX audproc_svc: Media Format is NOT in APR format!", me->objId);
      return ADSP_EBADPARAM;
   }

   switch (pMediaFormatData->unMediaFormatID)
   {
   case ELITEMSG_MEDIA_FMT_MULTI_CHANNEL_PCM:
   {
      elite_multi_channel_pcm_fmt_blk_t *pFormat = (elite_multi_channel_pcm_fmt_blk_t *) elite_msg_get_media_fmt_blk(pMediaFormatData);
      *pMediaFmt = AudPP_ParseMultichannelPCMMediaFmt(pFormat);
      break;
   }
   case ELITEMSG_MEDIA_FMT_COMPRESSED:
   {
      elite_compressed_fmt_blk_t *pFormat = (elite_compressed_fmt_blk_t *) elite_msg_get_media_fmt_blk(pMediaFormatData);
      *pMediaFmt = AudPP_ParseCompressedMediaFmt(pFormat);
      break;
   }
   default:
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX audproc_svc: Media format is NOT PCM.", me->objId);
      return ADSP_EBADPARAM;
   }
   }

   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: Parse Media type End", me->objId);

   return ADSP_EOK;
}

/*===========================================================================
FUNCTION AudPP_DataProc_MediaFormat()

DESCRIPTION
Message handler function for Media format data command.
PARAMETERS
Input:  me to the AudDynaPPSvc instance
pMyStat to svc/processing status

RETURN VALUE
success/failure/status.

DEPENDENCIES

SIDE EFFECTS
passing any residual output buffer to downstream
passing Media Format command to downstream
save corresponding media format information
Doing proper initialization for corresponding PP features.

NOTES:

===========================================================================*/
ADSPResult AudPP_DataProc_MediaFormat(ThisAudDynaPPSvc_t* me)
{
   ADSPResult result = ADSP_EOK;

   AudPPStatus_t *pMyStat = &me->audPPStatus;

   elite_msg_data_media_type_apr_t* pMediaFormatData = (elite_msg_data_media_type_apr_t *)(pMyStat->pInDataMsg->pPayload);

   AdspAudioMediaFormatInfo_t newInputMediaFormat;

   result = AudPP_ParseMediaTypePacket(me, pMediaFormatData, &newInputMediaFormat);
   if (ADSP_FAILED(result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX audproc_svc: Error in parsing media format.", me->objId);
   }

   {
      ADSPResult errCode;
      errCode = elite_msg_release_msg(pMyStat->pInDataMsg);
      if (ADSP_FAILED(errCode))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX audproc_svc: Failed to release MediaTypeMsg!", me->objId);
      }
      pMyStat->pInDataMsg = NULL;
   }

   if (ADSP_SUCCEEDED(result))
   {
      // Check to see if media type has been set/changed, if changed we need to reinitialize all the modules
      if( !(AudPP_MediaFormatsEqual(&newInputMediaFormat, &me->inputMediaFormatInfo)))
      {
         // input Media formats are not equal so reinitialize modules
         result = AudPP_setInputMediaInfo(me, &newInputMediaFormat);
      }
      if (ADSP_SUCCEEDED(result))
      {
         me->mediaFormatReceived = TRUE;
      }
   }

   return result;
}


/*===========================================================================
FUNCTION AudPP_VoteBw()

DESCRIPTION
Message handler function for voting BW request event
PARAMETERS
Input:  me to the AudDynaPPSvc instance
        Input Media format
        Output Media format

RETURN VALUE
success/failure/status.

DEPENDENCIES

SIDE EFFECTS

NOTES:

===========================================================================*/

ADSPResult AudPP_VoteBw(ThisAudDynaPPSvc_t* me)
{
   ADSPResult result = ADSP_EOK;
   if (me->is_processing_set_media_type) // This function will be called once all media type processing is done.
   {
      return ADSP_EOK;
   }

   if (!qurt_elite_adsppm_wrapper_is_registered(me->ulAdsppmClientId))
{
      return ADSP_EOK;
   }

   uint32_t newBw = AudPP_AggregateBw(me);

   if ( (me->prev_bw_vote != newBw) )
    {
      me->prev_bw_vote = newBw;

#if (ADSPPM_INTEGRATION==1)
      static const uint8_t NUM_REQUEST=1;
      MmpmRscParamType rscParam[NUM_REQUEST];
      MMPM_STATUS      retStats[NUM_REQUEST];
      MmpmRscExtParamType reqParam;
      uint8_t req_num=0;
      MmpmGenBwValType bwReqVal;
      MmpmGenBwReqType bwReq;

      reqParam.apiType                    = MMPM_API_TYPE_SYNC;
      reqParam.pExt                       = NULL;       //for future
      reqParam.pReqArray                  = rscParam;
      reqParam.pStsArray                  = retStats;   //for most cases mmpmRes is good enough, need not check this array.
      reqParam.reqTag                     = 0;          //for async only



      bwReqVal.busRoute.masterPort                 = MMPM_BW_PORT_ID_ADSP_MASTER;
      bwReqVal.busRoute.slavePort                  = MMPM_BW_PORT_ID_DDR_SLAVE;
      bwReqVal.bwValue.busBwValue.bwBytePerSec     = newBw;
      bwReqVal.bwValue.busBwValue.usagePercentage  = 100;
      bwReqVal.bwValue.busBwValue.usageType        = MMPM_BW_USAGE_LPASS_DSP;

      bwReq.numOfBw            = 1;
      bwReq.pBandWidthArray    = &bwReqVal;

      rscParam[req_num].rscId                   = MMPM_RSC_ID_GENERIC_BW_EXT;
      rscParam[req_num].rscParam.pGenBwReq      = &bwReq;
      req_num++;

      reqParam.numOfReq                   = req_num;

      if (newBw==0)
      {
         result = qurt_elite_adsppm_wrapper_release(me->ulAdsppmClientId, &me->adsppmClientPtr, &reqParam);
         MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "ADSPPM release BW by P%hX (%lu). Result %lu",me->objId, me->ulAdsppmClientId, result);
      }
      else
      {
         result = qurt_elite_adsppm_wrapper_request(me->ulAdsppmClientId, &me->adsppmClientPtr, &reqParam);
         MSG_4(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "ADSPPM request BW %lu KBPS by P%hX (%lu). Result %lu", newBw/1024, me->objId, me->ulAdsppmClientId, result);
      }
#endif //#if (ADSPPM_INTEGRATION==1)
    }


    return result;
}



/*===========================================================================
FUNCTION AudPP_DataProc_EosFormat()

DESCRIPTION
Message handler function for Eos format data command.
PARAMETERS
Input:  me to the AudDynaPPSvc instance

RETURN VALUE
success/failure/status.

DEPENDENCIES

SIDE EFFECTS
passing any residual output buffer to downstream
translating EOS command into internal EOS cmd if necessary.
passing EOS command to downstream

NOTES:

===========================================================================*/
ADSPResult AudPP_DataProc_EosFormat(ThisAudDynaPPSvc_t* me)
{
   ADSPResult result;
   AudPPStatus_t *pMyStat = &me->audPPStatus;

   if (!AudPP_isDevicePath(me))
   {
      if (pMyStat->pOutDataMsgDataBuf != NULL)
      {
         // currently there's buffer being processed.
         // From program flow, this can ONLY be PCM buffer.
         // deliver current buffer if it has samples. Else just release it.
         if (0 != pMyStat->pOutDataMsgDataBuf->nActualSize)
         {
            MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: EOS Delivering out %8lx ", me->objId, (int32) pMyStat->pOutDataMsgDataBuf);
            AudPP_deliverCurrentOutputBuffer(me);
         }
         else
         {
            AudPP_FreeCurrentOutputBuffer(me);
         }
      }  // if (pOutDataMsgDataBuf !=NULL)
   }

   elite_msg_data_eos_header_t *pelite_msg_data_eos_header_t =  (elite_msg_data_eos_header_t *)(pMyStat->pInDataMsg->pPayload);

   if ( (ELITEMSG_DATA_RESET_SESSION_CLK   == pelite_msg_data_eos_header_t->unEosFormat) || /* Don't reset for gapless EOS */
         (ELITEMSG_DATA_DTMF_EOT   == pelite_msg_data_eos_header_t->unEosFormat) ||          /* Indicates DTMF tone end, not a stream */
         AudPP_isDevicePath(me) ) /* Multiple streams can be playing concurrently on the device leg, so don't reset */
   {
   }
   else
   {
      topo_reset(me->pTopologyObj);
   }

   // enqueue EOS Msg to downstream bufQ.
   result = qurt_elite_queue_push_back(me->pDownstreamPeer->dataQ, (uint64_t*)(pMyStat->pInDataMsg));
   if (ADSP_FAILED(result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX audproc_svc: Can't enqueue Eos Msg to downstream dataQ!", me->objId);
   }

   //incr log-id after EoS
   me->ulDataLogId++;

   // finish media format processing, need to reset input indication pointer.
   pMyStat->pInDataMsg = NULL;

   return ADSP_EOK;
}

/*===========================================================================
FUNCTION AudPP_DataProc_DataBufFormatProcSetUp()

DESCRIPTION
DataBuffer format data command processing setup function.
PARAMETERS
Input:  me to the AudDynaPPSvc instance
pMyStat to svc/processing status

RETURN VALUE
success/failure/status.

DEPENDENCIES

SIDE EFFECTS
inSample set up properly according to data cmd & PP configuration.
PP processing/svc status could be setup accordingly.

NOTES:

===========================================================================*/
ADSPResult AudPP_DataProc_DataBufFormatProcSetUp(ThisAudDynaPPSvc_t* me)
{
   ADSPResult result = ADSP_EOK;
   uint32_t inSizeInSample = 0;
   AudPPStatus_t *pMyStat = &me->audPPStatus;

   uint32_t bytes_per_sample_in = (me->inputMediaFormatInfo.bitsPerSample > 16) ? BYTES_PER_SAMPLE_FOUR : BYTES_PER_SAMPLE_TWO;

   if (me->mediaFormatReceived)
   {
      // if data buffer from upstream service
      elite_msg_data_buffer_t *pInDataMsgDataBuf;

      pInDataMsgDataBuf = (elite_msg_data_buffer_t*) (pMyStat->pInDataMsg->pPayload);
      pMyStat->inputBufInfo.pBuf = (int8_t *) (&(pInDataMsgDataBuf->nDataBuf));

      QURT_ELITE_ASSERT(pInDataMsgDataBuf->nActualSize >= 0);

      uint32_t bufferSize = (uint32_t)(pInDataMsgDataBuf->nActualSize);

      inSizeInSample = bufferSize / (me->inputMediaFormatInfo.channels * (bytes_per_sample_in));
      bool_t isTimeStampValid = (0 == GET_DATA_BUFFER_TIME_STAMP_BIT(pInDataMsgDataBuf->nFlag)) ? FALSE : TRUE;

      pMyStat->inputBufInfo.sizeInSamples = inSizeInSample;
      pMyStat->inputBufInfo.offsetInSamples = 0;
      if(isTimeStampValid)
      {
         pMyStat->inputBufInfo.flags.is_timestamp_valid = isTimeStampValid;
      }
      pMyStat->inputBufInfo.timestamp = pInDataMsgDataBuf->ullTimeStamp;

      if (isCompressedInput(me))
      {
         result = AudPP_CheckAndAdjustCompressedBufSize(me, pInDataMsgDataBuf->nActualSize, pInDataMsgDataBuf->nMaxSize, inSizeInSample);
      }

      if (ADSP_SUCCEEDED(result))
      {
         /* Logging the input data to PP module */
         result = AudPP_LogData(me);


         // Do deinterleaving if needed.
         result = AudPP_ProcessFormatConv(&me->formatConverter, pMyStat->inputBufInfo.pBuf, inSizeInSample, bytes_per_sample_in);

         if (ADSP_ENOMEMORY == result)
         {
            MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "P%hX audproc_svc: Ran out of memory in format conversion function.", me->objId);
            AudPP_GotoFatalState(me);
         }
      }
   }
   else
   {
      // Not received a media format packet yet, just return with error
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX audproc_svc: Data buffer received before getting media format command. Dropping the buffer.", me->objId);
      result = ADSP_ENOTREADY;
   }

   return result;
}


#define  ENABLE_LOG_CODE_COPP_OUT 0
/*===========================================================================
FUNCTION AudPP_DataProc_DataBufFormatProc()

DESCRIPTION
Message handler function for DataBuffer format data command.
PARAMETERS
Input:  me to the AudDynaPPSvc instance

RETURN VALUE
success/failure/status.

DEPENDENCIES

SIDE EFFECTS
For input data buffer, do address mapping if necessary.
Process input data by running through AudProc funcs.
deliver output buffer (if fully filled) to downstream buffer

NOTES:

===========================================================================*/
void AudPP_DataProc_DataBufFormatProc(ThisAudDynaPPSvc_t* me)
{
   ADSPResult result;
   AudPPStatus_t *pMyStat = &me->audPPStatus;
   uint32_t *pMyCurrentBitfield = &pMyStat->unCurrentBitfield;
   uint32_t unMyBufBitfield = pMyStat->unBufBitfield;
   uint32_t unMyDataBitfield = pMyStat->unDataBitfield;
   // listen to output field
   *pMyCurrentBitfield = unMyBufBitfield;
   int64_t ss_compensated_drift = 0;
   // only when no current output data buffer possessed, need to listen
   // to empty buffer.
   if (pMyStat->pOutDataMsgDataBuf == NULL)
   {
      AudPP_TryGetOutputBuffer(me);
   }    // if (pMyStat->pOutDataMsgDataBuf == NULL)

   //if there are pending zero buffers to be sent down, enque to downstream service
   if ((pMyStat->pOutDataMsgDataBuf != NULL) && (me->numZeroBufsToSendDownStream))
   {
      AudPP_DataProc_SendZerBuffer(me);
   }
   
   // below only when pOutDataMsgDataBuf !=NULL can go through
   if (pMyStat->pOutDataMsgDataBuf != NULL)
   {
      // set up time stamp for output buf when necessary
      QURT_ELITE_ASSERT(!isCompressedInput(me) || (pMyStat->outputBufInfo.offsetInSamples == 0));
      if (pMyStat->outputBufInfo.offsetInSamples == 0)
      {
         // data buffer comes from upstream service
         elite_msg_data_buffer_t *pInputBuffer = (elite_msg_data_buffer_t*) (pMyStat->pInDataMsg->pPayload);
         elite_msg_data_buffer_t *pOutputBuffer = pMyStat->pOutDataMsgDataBuf;
         bool_t isTimeStampValid = (0 == GET_DATA_BUFFER_TIME_STAMP_BIT(pInputBuffer->nFlag)) ? FALSE : TRUE;


         if (isTimeStampValid)
         {
            pOutputBuffer->nFlag = SET_DATA_BUFFER_TIME_STAMP_BIT(pOutputBuffer->nFlag);
            uint32_t algorithmic_delay = 0;
            uint32_t internal_buffer_delay = 0;
            if (NULL != me->pAlgorithmicDelay)
            {
               algorithmic_delay = *me->pAlgorithmicDelay;
            }
            internal_buffer_delay = topo_get_internal_buffer_delay(me->pTopologyObj);

            pOutputBuffer->ullTimeStamp = pInputBuffer->ullTimeStamp +
                                          (((uint64_t)(pMyStat->inputBufInfo.offsetInSamples)*1000000))/me->inputMediaFormatInfo.samplingRate
                                          + (me->numZeroBufsSentDownStream * (me->ppCfgInfo.dynaPPSvcOutDurationInUsec)) - internal_buffer_delay - algorithmic_delay;


            if(AudPP_isDevicePath(me))
            {
               uint32_t   usActualParamSize = 0;
               audproc_sampleslip_sample_adjust_t  sampleslippayload;

               if (topo_is_module_present(me->pTopologyObj, AUDPROC_MODULE_ID_SAMPLESLIP))
               {
                  result = topo_get_param(me->pTopologyObj,
                        &sampleslippayload,
                        sizeof(audproc_sampleslip_sample_adjust_t), /* ParamMaxSize */
                        AUDPROC_MODULE_ID_SAMPLESLIP, /* ModuleId */
                        AUDPROC_PARAM_ID_SAMPLESLIP_SAMPLE_ADJUST, /* ParamId */
                        &usActualParamSize);

                  if (ADSP_EBADPARAM == result) // Error returned from the get_param, as module index is not found in the 	topology.
                  {
                     MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc:  SampleSlip Module not part of the Topology", me->objId);
                     sampleslippayload.compensated_drift = 0;
                  }
                  else if (ADSP_ENEEDMORE == result) // Error returned from the sample slip module
                  {
                     MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc:  SampleSlip Module : GetParam Failed", me->objId);
                     sampleslippayload.compensated_drift = 0;
                  }
                  else if (ADSP_EOK != result)
                  {
                     MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc:  Failed to get the SS drift and update the timestamp", me->objId);
                     sampleslippayload.compensated_drift = 0;
                  }
               }
               else
               {
                  sampleslippayload.compensated_drift = 0;
               }

               if(DYNA_SVC_PP_TYPE_COPREP == me->ppCfgInfo.dynaPPSvcType)
               {
                  if (sampleslippayload.compensated_drift < 0)
                  {
                     pOutputBuffer->ullTimeStamp = (uint64_t)((int64_t)pOutputBuffer->ullTimeStamp - ((int64_t)(sampleslippayload.compensated_drift)>>32));
                     ss_compensated_drift = -((int64_t)(sampleslippayload.compensated_drift)>>32);
                  }
                  else
                  {
                     pOutputBuffer->ullTimeStamp = (uint64_t)((int64_t)pOutputBuffer->ullTimeStamp + ((int64_t)(sampleslippayload.compensated_drift)>>32));
                     ss_compensated_drift = ((int64_t)(sampleslippayload.compensated_drift)>>32);
                  }
               }
               if(DYNA_SVC_PP_TYPE_COPP == me->ppCfgInfo.dynaPPSvcType)
               {
                  if (sampleslippayload.compensated_drift < 0)
                  {
                     pOutputBuffer->ullTimeStamp = (uint64_t)((int64_t)pOutputBuffer->ullTimeStamp + ((int64_t)(sampleslippayload.compensated_drift)>>32));
                     ss_compensated_drift = ((int64_t)(sampleslippayload.compensated_drift)>>32);
                  }
                  else
                  {
                     pOutputBuffer->ullTimeStamp = (uint64_t)((int64_t)pOutputBuffer->ullTimeStamp - ((int64_t)(sampleslippayload.compensated_drift)>>32));
                     ss_compensated_drift = -((int64_t)(sampleslippayload.compensated_drift)>>32);
                  }
               }
            }
         }
         else
         {
            pOutputBuffer->nFlag = CLR_DATA_BUFFER_TIME_STAMP_BIT(pOutputBuffer->nFlag);
            pOutputBuffer->ullTimeStamp = 0;
         }
      }


      //------------------------------------------------------------------------------------------------------------
      // Intermediate Buffers could have samples so empty intermediate buffer to output buffer and then send eos downstream
      //------------------------------------------------------------------------------------------------------------

      uint32_t bytes_per_sample_out = (me->outputMediaFormatInfo.bitsPerSample > 16) ? BYTES_PER_SAMPLE_FOUR : BYTES_PER_SAMPLE_TWO;

      if (pMyStat->m_fKeepProcessingonEos)
      {
         bool_t m_fdoneEmptyingBuf = FALSE;

         if ((DYNA_SVC_PP_TYPE_COPP == me->ppCfgInfo.dynaPPSvcType) || (DYNA_SVC_PP_TYPE_COPREP == me->ppCfgInfo.dynaPPSvcType))
         {
            // Don't empty the internal buffer on the device leg.
            m_fdoneEmptyingBuf = TRUE;
         }
         else if (isCompressedInput(me))
         {
            m_fdoneEmptyingBuf = TRUE;
         }
		 // check if POPP and while switchingin gapless
             // if yes then do not flush out history buffers, let the second stream flush the
             // next stream from history buffers
         else if ((DYNA_SVC_PP_TYPE_POPP == me->ppCfgInfo.dynaPPSvcType) && (switchInGapless(me)))
         {
            m_fdoneEmptyingBuf = TRUE;
         }

         else
         {
            uint32_t initial_outbuf_offset = pMyStat->outputBufInfo.offsetInSamples;
            me->is_processing_data = TRUE;
            topo_buffer_reset(me->pTopologyObj,
                  &pMyStat->outputBufInfo,
                  &pMyStat->prevInputBufInfo,
                  &m_fdoneEmptyingBuf);
            me->is_processing_data = FALSE;

            if((initial_outbuf_offset == pMyStat->outputBufInfo.offsetInSamples) &&
                  (!me->is_media_format_change_pending))
            {
               topo_free_eos_buffer(me->pTopologyObj,
                                            &m_fdoneEmptyingBuf);
            }

            pMyStat->pOutDataMsgDataBuf->nActualSize = (int32_t)(pMyStat->outputBufInfo.offsetInSamples *
                  (me->outputMediaFormatInfo.channels) *
                  (bytes_per_sample_out));

#if ((defined __hexagon__) || (defined __qdsp6__))
            if((DYNA_SVC_PP_TYPE_COPP == me->ppCfgInfo.dynaPPSvcType) && (ENABLE_LOG_CODE_COPP_OUT))
               AudPP_LogData_Output(me);
#endif

            // when output buffer is full
            if (AudPP_isBufferFull(&pMyStat->outputBufInfo))
            {
               pMyStat->pOutDataMsgDataBuf->ullTimeStamp =
                     (uint64_t)(pMyStat->outputBufInfo.timestamp + ss_compensated_drift 
					 + (me->numZeroBufsSentDownStream * (me->ppCfgInfo.dynaPPSvcOutDurationInUsec)));
               AudPP_deliverCurrentOutputBuffer(me);
            }

            if (me->is_media_format_change_pending)
            {
               AudPP_UpdateOutputMediaFormat(me, &me->new_media_format);
            }
         }

         //---------------------------------------------------------------------------------------------------------
         // Intermediate Buffer is empty so pass EOS to downstream
         //---------------------------------------------------------------------------------------------------------
         if (m_fdoneEmptyingBuf) //intermediate buffer is empty
         {
            //need input buffer
            *pMyCurrentBitfield = unMyDataBitfield;
            pMyStat->m_fKeepProcessingonEos = FALSE;
            (void) AudPP_DataProc_EosFormat(me);
         } // if ( pMyStat->SamplesPresentinIntermediateBuf  == 0 )
      } //if (pMyStat->m_fKeepProcessingonEos)
      else
      {
         //------------------------------------------------------------------------------------------------------------
         //Regular processing, input buffer has samples, generate output buffer
         //------------------------------------------------------------------------------------------------------------

          me->is_processing_data = TRUE;

    	  result = topo_process(me->pTopologyObj,
               &pMyStat->inputBufInfo,
               &pMyStat->outputBufInfo);

          me->is_processing_data = FALSE;

         if (ADSP_ENOMEMORY == result)
         {
            MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "P%hX audproc_svc: Ran out of memory in process function.", me->objId);
            AudPP_GotoFatalState(me);
            return;
         }

         // set up output buffer meta data when needed.
         QURT_ELITE_ASSERT(pMyStat->pOutDataMsgDataBuf->nActualSize >= 0);

         pMyStat->pOutDataMsgDataBuf->nActualSize = (int32_t)(pMyStat->outputBufInfo.offsetInSamples *
               (me->outputMediaFormatInfo.channels) *
               bytes_per_sample_out);

         // NOTE: changes needed if we're going to support 8bit PCM samples.

#if ((defined __hexagon__) || (defined __qdsp6__))
         if((DYNA_SVC_PP_TYPE_COPP == me->ppCfgInfo.dynaPPSvcType) && (ENABLE_LOG_CODE_COPP_OUT))
            AudPP_LogData_Output(me);
#endif
         QURT_ELITE_ASSERT(!isCompressedInput(me) || AudPP_isBufferFull(&pMyStat->inputBufInfo));
         if (AudPP_isBufferFull(&pMyStat->inputBufInfo))
         {
            AudPP_FreeCurrentInputBuffer(me);

            // Adjust master mask to listen for data commands again
            *pMyCurrentBitfield = unMyDataBitfield;
         }

         // when output buffer is full
         QURT_ELITE_ASSERT(!isCompressedInput(me) || AudPP_isBufferFull(&pMyStat->outputBufInfo));
         if (AudPP_isBufferFull(&pMyStat->outputBufInfo))
         {
            pMyStat->pOutDataMsgDataBuf->ullTimeStamp = (uint64_t)(pMyStat->outputBufInfo.timestamp + ss_compensated_drift + 
			(me->numZeroBufsSentDownStream * (me->ppCfgInfo.dynaPPSvcOutDurationInUsec)));
            AudPP_deliverCurrentOutputBuffer(me);
         }

         if (me->is_media_format_change_pending)
         {
            AudPP_UpdateOutputMediaFormat(me, &me->new_media_format);
         }

      }  //else portion for if (pMyStat->m_fKeepProcessingonEos )
   } // if (pMyStat->pOutDataMsgDataBuf != NULL)
}

/*===========================================================================
FUNCTION AudPP_DataProc_SendZerBuffer()

DESCRIPTION
Function to send zero buffer downstream during beginning of playback.
PARAMETERS
Input:  me to the AudDynaPPSvc instance

RETURN VALUE
void.

DEPENDENCIES

SIDE EFFECTS

NOTES:

===========================================================================*/
static void AudPP_DataProc_SendZerBuffer(ThisAudDynaPPSvc_t* me)
{
   AudPPStatus_t *pMyStat = &me->audPPStatus;
   elite_msg_data_buffer_t *pInputBuffer = (elite_msg_data_buffer_t*) (pMyStat->pInDataMsg->pPayload);   
   elite_msg_data_buffer_t *pOutputBuffer = pMyStat->pOutDataMsgDataBuf;   
   uint32_t bytes_per_sample_out = (me->outputMediaFormatInfo.bitsPerSample > 16) ? BYTES_PER_SAMPLE_FOUR : BYTES_PER_SAMPLE_TWO;	  

   pMyStat->pOutDataMsgDataBuf->nActualSize = (int32_t)(pMyStat->outputBufInfo.sizeInSamples *
         (me->outputMediaFormatInfo.channels) * bytes_per_sample_out);
		 
   memset(pMyStat->outputBufInfo.pBuf, 0, pMyStat->pOutDataMsgDataBuf->nActualSize);
	  
   pMyStat->outputBufInfo.offsetInSamples = pMyStat->outputBufInfo.sizeInSamples;
			   
   //set time stamp falg is invalid for zero buffers
   bool_t isTimeStampValid = (0 == GET_DATA_BUFFER_TIME_STAMP_BIT(pInputBuffer->nFlag)) ? FALSE : TRUE;
   if (isTimeStampValid)
   {
      pOutputBuffer->nFlag = SET_DATA_BUFFER_TIME_STAMP_BIT(pOutputBuffer->nFlag);
      uint32_t algorithmic_delay = 0;
      uint32_t internal_buffer_delay = 0;
      if (NULL != me->pAlgorithmicDelay)
      {
         algorithmic_delay = *me->pAlgorithmicDelay;
      }
      if(NULL!=me->pTopologyObj)
      {
         internal_buffer_delay = topo_get_internal_buffer_delay(me->pTopologyObj);
      }
      pOutputBuffer->ullTimeStamp = pInputBuffer->ullTimeStamp + 
         (me->numZeroBufsSentDownStream * (me->ppCfgInfo.dynaPPSvcOutDurationInUsec)) - internal_buffer_delay - algorithmic_delay;
   }
   else
   {
      pOutputBuffer->nFlag = CLR_DATA_BUFFER_TIME_STAMP_BIT(pOutputBuffer->nFlag);
      pOutputBuffer->ullTimeStamp = 0;	  
   }
	  
   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "P%hX audproc_svc:  sending zero buffer downstream", me->objId);	  
	  
   //send zero buffer down
   AudPP_deliverCurrentOutputBuffer(me);

   //increment counter related to number of buffers already sent down
   me->numZeroBufsSentDownStream = me->numZeroBufsSentDownStream + 1;
	  
   //decrement counter related to number of buffers to be sent down
   me->numZeroBufsToSendDownStream = me->numZeroBufsToSendDownStream - 1;
}

/**
 * Handles the processing of a packet from the data queue.
 *   - Dependencies: There should not be any command currently
 *     being processed.
 *   - Side Effects: Performs actions depending on the command.
 *     If it is a data buffer and cannot be fully processed if
 *     there is no output buffer, the command is remembered in
 *     the pInDataMsg variable.
 *   - Re-entrant: Yes
 *
 * @param[in, out] me
 *     A pointer to the current instance structure.
 * @param[in] pInDataMsg
 *     Pointer to the input data message
 */
void AudPP_DataProc_ProcessNewPacket(ThisAudDynaPPSvc_t *me, elite_msg_any_t *pInDataMsg)
{

   // remember input data cmd which is being processed.
   QURT_ELITE_ASSERT(NULL == me->audPPStatus.pInDataMsg);
   me->audPPStatus.pInDataMsg = pInDataMsg;

   switch (pInDataMsg->unOpCode)
   {
   case ELITE_DATA_MEDIA_TYPE:

      // For current ELite architecture, MEDIA TYPE command is
      // always in ELite internal cmd format.
      (void) AudPP_DataProc_MediaFormat(me);
      // still listen to input dataQ, no need to change.
      break;

   case ELITE_DATA_EOS:
      // This EOS command is coming from upstream svc, not SC.
   {
      me->audPPStatus.m_fKeepProcessingonEos = TRUE;
      AudPP_DataProc_DataBufFormatProc(me);
   }
   // still listen to input dataQ, no need to change.
   break;
   case ELITE_DATA_MARK_BUFFER:
      // enqueue EOS Msg to downstream bufQ.
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Audproc service received mark buffer");
      if (ADSP_FAILED(qurt_elite_queue_push_back(me->pDownstreamPeer->dataQ, (uint64_t*)(pInDataMsg))))
      {
         AudPP_DiscardMarkBuffer(me,pInDataMsg);
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX audproc_svc: Can't enqueue Mark Msg to downstream dataQ!", me->objId);
      }

      // finish media format processing, need to reset input indication pointer.
      me->audPPStatus.pInDataMsg = NULL;
      break;
   case ELITE_DATA_BUFFER:
   {
      ADSPResult result;
      result = AudPP_DataProc_DataBufFormatProcSetUp(me);
      if (ADSP_SUCCEEDED(result))
      {
         AudPP_DataProc_DataBufFormatProc(me);
      }
      else
      {
         // Just return the packet
         result = elite_msg_return_payload_buffer(pInDataMsg);
         if (ADSP_FAILED(result))
         {
            MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX audproc_svc: Failed to release input buffer", me->objId);
         }
         me->audPPStatus.pInDataMsg = NULL;
      }
   }
   break;

   case ELITE_DATA_SET_PARAM:
      AudPP_DataProc_SetParam(me);
      break;

   default:
      // unsupported data msg, need to release input msg.
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX audproc_svc: Unsupported Data Cmd type.", me->objId);
      (void) elite_msg_release_msg(pInDataMsg);
      me->audPPStatus.pInDataMsg = NULL;
      break;
   } // switch data Msg opcode
}


/*===========================================================================
FUNCTION AudPP_SoftPause_Start()

DESCRIPTION
start soft pause timer
PARAMETERS
Input:  me to the AudDynaPPSvc instance

RETURN VALUE: None.

===========================================================================*/
ADSPResult AudPP_SoftPause_Start(ThisAudDynaPPSvc_t *me)
{
   ADSPResult result = ADSP_EFAILED;

   uint32_t   unOutBufDurationMs = 0;
   uint32_t   usActualParamSize = 0;
   audproc_soft_pause_params_t softPauseParamPayload;

   /* PP svc get the duration from CSoftVolMultichannelConfig (CSoftVolMultichannelLib.h) */
   result = topo_get_param(me->pTopologyObj,
         &softPauseParamPayload,
         sizeof(audproc_soft_pause_params_t), /* ParamMaxSize */
         AUDPROC_MODULE_ID_VOL_CTRL, /* ModuleId: SoftPause is done via SoftVolumeControls */
         AUDPROC_PARAM_ID_SOFT_PAUSE_PARAMETERS, /* ParamId */
         &usActualParamSize);
   if (ADSP_EOK != result)
   {
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: soft pause failed getparam", me->objId);
      return result;
   }

   // MSG_5(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: softpause enable=%d, period=%d, step=%d, curve=%d",
   //        me->objId,
   //        softPauseParamPayload.enable_flag,
   //        softPauseParamPayload.period,
   //        softPauseParamPayload.step,
   //        softPauseParamPayload.ramping_curve);

   if (0 == softPauseParamPayload.enable_flag)
   {
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: soft pause not enabled", me->objId);
      return ADSP_EFAILED;
   }

   result = qurt_elite_timer_oneshot_cancel( &(me->softPauseTimer) );
   if (ADSP_FAILED(result))
   {
      /* timer cancel may fail in case it's not yet started/already finished,
       * hence this is not an error */
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,
            "P%hX audproc_svc: soft pause timer could not be cancelled/currently inactive", me->objId);
   }

   if (FALSE == me->mediaFormatReceived)
   {
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: Soft pause not Ready yet, issuing normal pause", me->objId);
      return ADSP_EFAILED;
   }

   //Derive all frame durations from the AFE Frame Size
   uint32_t unAfeFrameSize;
   uint32_t unFreq = 48000;
   if(me->outputMediaFormatInfo.samplingRate)
   {
	   unFreq = me->outputMediaFormatInfo.samplingRate;
   }

   elite_svc_get_frame_size(unFreq, &unAfeFrameSize);
   
   unOutBufDurationMs = ((me->ppCfgInfo.dynaPPSvcOutBufNumAfeFrames)* unAfeFrameSize * 1000)/(unFreq);

   // We need a guard duration to allow the matrix to play out the data we send
   const int64_t MATRIX_INTERNAL_BUF_DURATION = (5 * unAfeFrameSize * 1000)/(unFreq);// ms
   int64_t guardDurationMs = me->nBufsAllocated * unOutBufDurationMs + MATRIX_INTERNAL_BUF_DURATION;

   int64_t durationUs = (softPauseParamPayload.period + guardDurationMs)*1000;
   result = qurt_elite_timer_oneshot_start_duration(&(me->softPauseTimer), durationUs);

   if (ADSP_EOK==result)
   {
      //MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: soft pause buffer duration=%d", me->objId, unOutBufDurationMs);

      me->audPPStatus.nSoftPauseBufCounter = (softPauseParamPayload.period / unOutBufDurationMs) + 1;
      topo_set_start_pause(me->pTopologyObj, &softPauseParamPayload);

      //MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: Soft Pause after getparam &me=0x%x, me=0x%x", me->objId, &me, me);

      me->audPPStatus.unCurrentBitfield |= (me->audPPStatus.unSoftPauseSignalOnlyBitfield);
      me->audPPStatus.rampOnResume = TRUE;
   }

   MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: Soft Pause started with %d", me->objId, result);

   return result;
}

/*===========================================================================
FUNCTION AudPP_SoftPause_Terminate()

DESCRIPTION
Soft pause timer expired or the desired number of buffers are already delivered,
 set PP into sleep state.
PARAMETERS
Input:  me to the AudDynaPPSvc instance

RETURN VALUE: None.

===========================================================================*/
static void AudPP_SoftPause_Terminate(ThisAudDynaPPSvc_t *me)
{
   if (NULL != me->audPPStatus.pOutDataMsgDataBuf)
   {
      /*
       * Free up any partially filled output buffer. If we are holding a buffer
       * at this point, it means that the input got delayed somehow. Playing it
       * back will cause a pop. So just discard it.
       */
      MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: Soft Pause terminated, freeing partial output buffer %8lx ", me->objId, (uint32) me->audPPStatus.pOutDataMsgDataBuf);
      AudPP_FreeCurrentOutputBuffer(me);
   }

   //see if we need to send down an EOS marker
   EliteMsg_CustomPPPausePrepareType *pCustomPauseMsg = me->pPauseMsgPayload;

   if(pCustomPauseMsg->sendDownEosMarker)
   {
      ADSPResult result;
      MSG_1( MSG_SSID_QDSP6, DBG_HIGH_PRIO,"P%hX: AudProc received pause, generating internal eos ", me->objId);
      result = AudPP_GenerateEos(me);
      if (ADSP_FAILED(result))
      {
         MSG_1( MSG_SSID_QDSP6, DBG_ERROR_PRIO,"P%hX audproc_svc: Unable to send EOS message. Aborting pause.", me->objId);
         return ;
      }
   }

   //reset the buffer counter to zero
   me->audPPStatus.nSoftPauseBufCounter = 0;

   AudPP_GotoSleepState(me);
   topo_set_volume1_state_to_pause(me->pTopologyObj);
   return;
}

void AudPP_SoftPauseTimerExpireHandler(ThisAudDynaPPSvc_t *me)
{
   qurt_elite_signal_clear(&(me->softPauseExpireSignal));
   if (ADSP_FAILED(qurt_elite_timer_oneshot_cancel(&(me->softPauseTimer))))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: failed cancel soft pause timer on termination", me->objId);
   }


   AudPP_SoftPause_Terminate(me);

   if (NULL == me->pPauseMsgPayload)
   {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "P%hX audproc_svc: NULL Pause message ", me->objId);
      return;
   }

   elite_msg_any_t pauseMsg;
   pauseMsg.unOpCode = ELITE_CUSTOM_MSG;
   pauseMsg.pPayload = (void*) (me->pPauseMsgPayload);

   elite_svc_send_ack(&pauseMsg, ADSP_EOK);

   me->pPauseMsgPayload = NULL;

   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: Pause Handler End with Timer expired, sent ack", me->objId);
}

ADSPResult AudPP_GenerateEos(ThisAudDynaPPSvc_t *me)
{
   elite_msg_any_t eosMsg;
   ADSPResult result;
   uint32_t unEosPayloadSize = sizeof( elite_msg_data_tx_eos_t );

   result = elite_msg_create_msg( &eosMsg, &unEosPayloadSize,
         ELITE_DATA_EOS,
         NULL,
         0,
         ADSP_EFAILED );
   if (ADSP_FAILED(result))
   {
      MSG_1( MSG_SSID_QDSP6, DBG_ERROR_PRIO,"P%hX audproc_svc: failed to create eos message", me->objId);
      return result;
   }

   elite_msg_data_tx_eos_t *pEosPayload = (elite_msg_data_tx_eos_t *) (eosMsg.pPayload);
   pEosPayload->unEosFormat   = ELITEMSG_DATA_TX_EOS;

   result = qurt_elite_queue_push_back(me->pDownstreamPeer->dataQ, (uint64_t*)&eosMsg);
   if (ADSP_FAILED(result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX audproc_svc: Error in sending tx eos !", me->objId);
      return result;
   }

   return ADSP_EOK;
}

/**
 * Handles the output format message.
 *   - Dependencies: None
 *   - Side Effects: None
 *   - Re-entrant: Yes
 *
 * @param[in, out] me
 *   Pointer to the instance structure.
 * @param[in] pMsg
 *   Pointer to the output media type config message.
 *
 * @return ADSPResult
 *   Error code indicating status
 */
ADSPResult AudPP_ProcessOutputFormatMsg(ThisAudDynaPPSvc_t *me, elite_msg_any_t *pMsg)
{
   ADSPResult result;

   const EliteMsg_CustomCfgPPOutputType *pOutputCfgMsg = (const EliteMsg_CustomCfgPPOutputType*)(pMsg->pPayload);

   result = AudPP_setOutputMediaInfo(me, &(pOutputCfgMsg->params));

   elite_svc_send_ack(pMsg, result);

   return result;
}

ADSPResult AudPP_ProcessSetOutputSamplingRate(ThisAudDynaPPSvc_t *me, elite_msg_any_t *pMsg)
{
   ADSPResult result = ADSP_EOK;

   const EliteMsg_CustomCfgPPOutputSampleRateType *pOutputCfgMsg = (const EliteMsg_CustomCfgPPOutputSampleRateType*)(pMsg->pPayload);

   AudPPSvcOutputMediaTypeParams_t params = me->ppCfgInfo.outputParams;
   params.keepInputSamplingRate = pOutputCfgMsg->keepInputSamplingRate;
   params.outputSamplingRate = pOutputCfgMsg->outputSamplingRate;

   result = AudPP_setOutputMediaInfo(me, &params);

   elite_svc_send_ack(pMsg, result);
   return result;
}

/**
 * Callback function called by static services to directly
 * queue a message into PP. This function is meant to be called
 * from APR's context.
 *   - Dependencies: PP must be correctly initialized.
 *   - Side Effects: None
 *   - Re-entrant: Yes
 *
 * @param[in] pCtxt
 *   This should be a pointer to the PP ThisAudDynaPPSvc_t
 *   structure.
 * @param[in] msg
 *   APR message to be queued.
 *
 * @return ADSPResult
 *   Error code indicating status
 */
ADSPResult AudPP_AprMsgCallback(void *pCtxt, elite_msg_any_t msg)
{
   ThisAudDynaPPSvc_t *me = (ThisAudDynaPPSvc_t*)(pCtxt);
   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: APR Message callback Begin", me->objId);

   if (msg.unOpCode != ELITE_APR_PACKET)
   {
      return ADSP_EUNEXPECTED;
   }

   ADSPResult result;
   elite_apr_packet_t *pAprPacket = (elite_apr_packet_t *)msg.pPayload;
   uint32_t opCode         = elite_apr_if_get_opcode(pAprPacket);

   switch (opCode)
   {
   case ASM_DATA_CMD_SPA_V2:
   case ADM_DATA_CMD_SPA_V5:
   {
      if (NULL != me->pSPAInputQueue)
      {
         result = qurt_elite_queue_push_back(me->pSPAInputQueue, (uint64_t*)(&msg));
         if (ADSP_FAILED(result))
         {
            MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX: failed to push SPA message to queue", me->objId);
         }
      }
      else
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX: SPA data buffer received but SPA not supported in this topology.", me->objId);
         result = ADSP_EUNSUPPORTED;
      }
      break;
   }
   default:
      result = ADSP_EUNSUPPORTED;
   }

   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: APR Message callback End", me->objId);
   return result;
}

static bool_t AudPP_isBufferFull(const AudPP_BufInfo_t *pBufInfo)
{
   return (pBufInfo->offsetInSamples == pBufInfo->sizeInSamples);
}

/**
 * Goes through the multichannel PCM media type packet and converts
 * to the internal media format type.
 *   - Dependencies: None
 *   - Side Effects: None
 *   - Re-entrant: Yes
 *
 * @param[in] pFormat
 * Pointer to the multichannel PCM format block.
 *
 * @return AdspAudioMediaFormatInfo_t
 * The media format in the internal type.
 */
static AdspAudioMediaFormatInfo_t AudPP_ParseMultichannelPCMMediaFmt(const elite_multi_channel_pcm_fmt_blk_t *pFormat)
{
   AdspAudioMediaFormatInfo_t mediaInfo;

   mediaInfo.channels = pFormat->num_channels;
   memscpy(mediaInfo.channelMapping,sizeof(mediaInfo.channelMapping), pFormat->channel_mapping, sizeof(mediaInfo.channelMapping));
   mediaInfo.bitsPerSample = pFormat->bits_per_sample;
   mediaInfo.isSigned      = pFormat->is_signed;
   mediaInfo.isInterleaved = pFormat->is_interleaved;
   mediaInfo.samplingRate  = pFormat->sample_rate;
   mediaInfo.bitstreamFormat = ASM_MEDIA_FMT_MULTI_CHANNEL_PCM_V2;

   return mediaInfo;
}

/**
 * Goes through the compressed PCM media type packet and converts
 * to the internal media format type.
 *   - Dependencies: None
 *   - Side Effects: None
 *   - Re-entrant: Yes
 *
 * @param[in] pFormat
 * Pointer to the compressed PCM format block.
 *
 * @return AdspAudioMediaFormatInfo_t
 * The media format in the internal type.
 */
static AdspAudioMediaFormatInfo_t AudPP_ParseCompressedMediaFmt(const elite_compressed_fmt_blk_t *pFormat)
{
   AdspAudioMediaFormatInfo_t mediaInfo;

   mediaInfo.channels = pFormat->num_channels;
   for (uint32_t i = 0; i < SIZE_OF_ARRAY(mediaInfo.channelMapping); i++)
   {
      mediaInfo.channelMapping[i] = PCM_CHANNEL_INVALID;
   }
   mediaInfo.bitsPerSample = pFormat->bits_per_sample;
   mediaInfo.isSigned      = 1;
   mediaInfo.isInterleaved = 1;
   mediaInfo.samplingRate  = pFormat->sample_rate;
   mediaInfo.bitstreamFormat = pFormat->media_format;

   return mediaInfo;
}

ADSPResult AudPP_sendMediaTypeMsg(ThisAudDynaPPSvc_t *me)
{
   elite_msg_any_t msg;
   ADSPResult result;

   if (NULL == me->pDownstreamPeer)
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX: Downstream connection not established. Will not send media type.", me->objId);
      return ADSP_EOK;
   }

   uint32_t nPayloadSize = sizeof(elite_msg_data_media_type_apr_t) + sizeof(elite_multi_channel_pcm_fmt_blk_t);
   result = elite_msg_create_msg(&msg,&nPayloadSize, ELITE_DATA_MEDIA_TYPE, NULL,0,0 ) ;
   if (ADSP_FAILED(result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX: Fail to create media fmt message", me->objId);
      return result;
   }

   elite_msg_data_media_type_apr_t *pCmdMsgPayload = (elite_msg_data_media_type_apr_t*) (msg.pPayload);
   pCmdMsgPayload->unMediaTypeFormat = ELITEMSG_DATA_MEDIA_TYPE_APR;
   pCmdMsgPayload->unMediaFormatID   = ELITEMSG_MEDIA_FMT_MULTI_CHANNEL_PCM;

   elite_multi_channel_pcm_fmt_blk_t *pPcmFormatBlock =
         (elite_multi_channel_pcm_fmt_blk_t*) ((uint8_t*)(pCmdMsgPayload)+sizeof(elite_msg_data_media_type_apr_t));
   AdspAudioMediaFormatInfo_t *pOutputFmt = &me->outputMediaFormatInfo;

   pPcmFormatBlock->num_channels = pOutputFmt->channels;
   memscpy( pPcmFormatBlock->channel_mapping,  sizeof(pPcmFormatBlock->channel_mapping) , pOutputFmt->channelMapping, sizeof(pOutputFmt->channelMapping));
   pPcmFormatBlock->bits_per_sample = pOutputFmt->bitsPerSample;
   pPcmFormatBlock->sample_rate = pOutputFmt->samplingRate;
   pPcmFormatBlock->is_signed = pOutputFmt->isSigned;
   pPcmFormatBlock->is_interleaved = pOutputFmt->isInterleaved;

   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: Sending MediaTypeMsg downstream.", me->objId);
   result = qurt_elite_queue_push_back(me->pDownstreamPeer->dataQ, (uint64_t*)&msg );
   if (ADSP_FAILED(result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX audproc_svc: Can't enqueue MediaTypeMsg to downstream dataQ!", me->objId);
   }

   return result;
}

static bool AudPP_isDevicePath(const ThisAudDynaPPSvc_t *me)
{
   return ((DYNA_SVC_PP_TYPE_COPP == me->ppCfgInfo.dynaPPSvcType) || (DYNA_SVC_PP_TYPE_COPREP == me->ppCfgInfo.dynaPPSvcType));
}

/**
 * Handles the data path set param message.
 *   - Dependencies: None
 *   - Side Effects: Any partially filled output buffer is sent.
 *   - Re-entrant: Yes
 *
 * @param[in] me
 *   Pointer to the instance object.
 */
static void AudPP_DataProc_SetParam(ThisAudDynaPPSvc_t *me)
{
   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: Data Set param start", me->objId);

   // First send down any pending partial output buffer
   AudPP_deliverCurrentOutputBuffer(me);

   ADSPResult result = ADSP_EOK;

   elite_msg_data_set_param_t *pMsgPkt = (elite_msg_data_set_param_t*)(me->audPPStatus.pInDataMsg->pPayload);

   // Call set param on the topology
   result = topo_set_param(me->pTopologyObj, pMsgPkt + 1, pMsgPkt->unPayloadSize);
   if (ADSP_FAILED(result))
   {
      // This message may contain params for some modules in downstream services, so this is not
      // an error case.
      MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "P%hX audproc_svc: Error in data path set param!", me->objId);
   }

   // Send the message downstream, since some missing modules may be present downstream
   result = qurt_elite_queue_push_back(me->pDownstreamPeer->dataQ, (uint64_t*)(me->audPPStatus.pInDataMsg));
   if (ADSP_FAILED(result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX audproc_svc: Can't enqueue data path set param Msg to downstream dataQ!", me->objId);
      result = elite_msg_release_msg(me->audPPStatus.pInDataMsg);
      if (ADSP_FAILED(result))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX audproc_svc: Couldn't release the data set param message!", me->objId);
      }
   }

   me->audPPStatus.pInDataMsg = NULL;

   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: Data Set param end", me->objId);
}
