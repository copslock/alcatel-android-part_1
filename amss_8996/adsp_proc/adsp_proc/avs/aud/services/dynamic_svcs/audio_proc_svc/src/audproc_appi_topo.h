
/**
@file audproc_appi_topo.h

   Header file for the generic APPI topology data structures and
   functions.
*/

/*========================================================================

Copyright (c) 2013-2014 QUALCOMM Technologies, Incorporated.
All Rights Reserved.
QUALCOMM Technologies Proprietary.
Export of this technology or software is regulated
by the U.S. Government, Diversion contrary to U.S. law prohibited.
*/

/*========================================================================
Edit History

when       who     what, where, why
--------   ---     -------------------------------------------------------
03/29/11   wj      add per-channel IIR topology
03/28/11   ss      Introducing PROCESS_CHECK for Resampler and Downmix (APPI Rev D)
01/12/11   ss      Delay accumulation mechanism for EOS, so that all the
                   intermediate buffers are flushed out before processing EOS
1/4/11     DG      Created file.

========================================================================== */

#ifndef AUDPROC_APPI_TOPO_H
#define AUDPROC_APPI_TOPO_H


/*----------------------------------------------------------------------------
 * Include files
 * -------------------------------------------------------------------------*/

#ifndef TST_TOPO_LAYER
#ifdef AUDPPSVC_TEST
#include "hexagon_sim_timer.h"
#endif // AUDPPSVC_TEST
#include "adsp_media_fmt.h"
#include "capi_v2_sample_slip.h"
#include "adsp_amdb_static.h"
#include "adsp_audproc_api.h"
#include "AudDynaPPSvc.h"
#include "EliteCmnTopology_db_if.h"
#include "EliteCmnTopology_db.h"
#else
#include "tst_topo_layer.h"
#endif

/*---------------------------------------------------------------------------
 * Definitions and Constants
 * -------------------------------------------------------------------------*/
#define AUDPROC_COPP_TOPOLOGY_ID_MCHAN_V2_DEFAULT   0xABCDEFE3
//#define PP_TOPO_ENABLE_LOW_LEVEL_MSG  0

const uint32_t AUDPROC_MODULE_ID_RESAMPLER_DS1AP = 0x07;
const uint32_t AUDPROCTST_MODULE_ID_BYTESHIFTER_CAPIV2 = 100;

/*---------------------------------------------------------------------------
 * Globals
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Type Declarations
 * -------------------------------------------------------------------------*/

// post processing chain format information structure
typedef struct _AdspAudioMediaFormatInfo_t
{
   uint32_t channels;
   uint32_t  bitsPerSample;
   uint32_t  samplingRate;
   uint32_t isSigned;
   uint32_t isInterleaved;
   uint8_t channelMapping[8];
   uint32_t bitstreamFormat;
} AdspAudioMediaFormatInfo_t;

typedef struct _AudPP_BufInfo_t
{
   int8_t *pBuf;
   uint32_t offsetInSamples; // This value is per channel
   uint32_t sizeInSamples;   // This value is per channel
   capi_v2_stream_flags_t flags;
   int64_t timestamp;
} AudPP_BufInfo_t;

struct topo_struct;

struct topo_event_client_t;
struct topo_event_vtable_t
{
   void (*KPPS)(topo_event_client_t *obj_ptr, uint32_t new_KPPS);
   void (*bandwidth)(topo_event_client_t *obj_ptr, uint32_t new_bandwidth);
   void (*output_media_format)(topo_event_client_t *obj_ptr, const AdspAudioMediaFormatInfo_t *new_format);
   void (*algorithmic_delay)(topo_event_client_t *obj_ptr, uint32_t new_delay);
};

struct topo_event_client_t
{
   const topo_event_vtable_t *vtbl;
};

/*----------------------------------------------------------------------------
 * Function Declarations
 * -------------------------------------------------------------------------*/
ADSPResult topo_get_module_list(const topo_struct *topo_obj, uint32_t* topo_module_list_ptr, uint32_t payload_size);

ADSPResult topo_init(const audproc_topology_definition_t *def_ptr,
      const uint16_t instance_id,
      const uint32_t ouput_buffer_num_unit_frames,
      topo_struct **topo_obj,
      AudPP_AudProcType pp_type,
      uint32_t *stack_size_ptr,
      topo_event_client_t *event_client_ptr,
      bool_t initialize_with_compressed_format = FALSE);

ADSPResult common_topo_init(elite_cmn_topo_db_entry_t *def_ptr,
		const uint16_t instance_id,
		const uint32_t ouput_buffer_num_unit_frames,
		topo_struct **topo_obj,
		AudPP_AudProcType pp_type,
		uint32_t *stack_size_ptr,
		topo_event_client_t *event_client_ptr,
		bool_t initialize_with_compressed_format = FALSE);

#ifdef TST_TOPO_LAYER
ADSPResult topo_init_dag(const topo_tst_graph_helper &def,
      const uint16_t instance_id,
      const uint32_t max_output_samples,
      topo_struct **topo_obj,
      AudPP_AudProcType pp_type,
      uint32_t *stack_size_ptr,
      topo_event_client_t *event_client_ptr,
      bool_t initialize_with_compressed_format = FALSE);
#endif

uint64_t topo_get_internal_buffer_delay (topo_struct *obj_ptr);

ADSPResult topo_process(topo_struct *obj_ptr,
      AudPP_BufInfo_t *inbuf_ptr,
      AudPP_BufInfo_t *outbuf_ptr);

void topo_change_input_media_type (topo_struct *obj_ptr,
      const AdspAudioMediaFormatInfo_t *pInMediaFormat);

void topo_reset (topo_struct *obj_ptr);

ADSPResult topo_set_param (topo_struct *obj_ptr,
      void *pPayload,
      uint32_t payloadSize);

ADSPResult topo_get_param (topo_struct *obj_ptr,
      void *pPayload,
      const uint16_t payloadSize,
      const uint32_t module_id,
      const uint32_t paramId,
      uint32_t *pParamSize);

void topo_destroy (topo_struct *obj_ptr);

void topo_buffer_reset (topo_struct *obj_ptr,
      AudPP_BufInfo_t *outbuf_ptr,
      AudPP_BufInfo_t *prev_inbuf_ptr,
      bool_t *buffer_reset_flag_ptr);

void topo_free_eos_buffer (topo_struct *topo_ptr,
      bool_t* buffer_reset_flag_ptr);

bool_t topo_is_module_present(topo_struct *topo_ptr, const uint32_t module_id);

void topo_compressed_reinit_with_output_sample_size(topo_struct *topo_obj, uint16_t max_output_samples);

bool_t topo_check_if_module_is_enabled (topo_struct *obj_ptr, uint32_t module_id);

void topo_set_output_sample_rate (topo_struct *topo_ptr, uint32_t module_id , uint32_t output_sample_rate, uint32_t port_id = 0);

void topo_set_volume1_state_to_pause(topo_struct *topo_ptr);

void  topo_set_start_pause(topo_struct *topo_ptr , audproc_soft_pause_params_t *softPauseParamPayload);

void  topo_set_ramp_on_resume(topo_struct *topo_ptr);

ADSPResult topo_set_output_media_format (topo_struct *topo_ptr, uint32_t module_id , const AdspAudioMediaFormatInfo_t *output_media_format, uint32_t port_index = 0);
#endif // #ifndef AUDPROC_APPI_TOPO_H

