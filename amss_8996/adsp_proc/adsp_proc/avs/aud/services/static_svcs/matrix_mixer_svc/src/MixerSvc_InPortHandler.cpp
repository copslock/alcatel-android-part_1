/**
@file MixerSvc_InPortHandler.cpp
@brief This file define functions that handle the arrival of a
       buffer at an input port of the audio matrix mixer.

 */

/*========================================================================
Copyright (c) 2009 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government, Diversion contrary to U.S. law prohibited.
 *//*====================================================================== */

/*========================================================================
Edit History

$Header: //components/rel/avs.adsp/2.7.1.c4/aud/services/static_svcs/matrix_mixer_svc/src/MixerSvc_InPortHandler.cpp#1 $

when       who     what, where, why
--------   ---     -------------------------------------------------------
06/04/2010 AAA     Created file.
========================================================================== */


/* =======================================================================
INCLUDE FILES FOR MODULE
========================================================================== */
#include "qurt_elite.h"
#include "Elite.h"
#include "MixerSvc.h"
#include "adsp_media_fmt.h"
#include "adsp_adm_api.h"
#include "MixerSvc_InPortHandler.h"
#include "MixerSvc_MsgHandlers.h"
#include "AudioStreamMgr_GetSetBits.h"
#include "MixerSvc_Util.h"
#include "avsync_lib.h"

/* -----------------------------------------------------------------------
 ** Constant / Define Declarations
 ** ----------------------------------------------------------------------- */

/* =======================================================================
Static Function Declarations
========================================================================== */


/* =======================================================================
Function Definitions
========================================================================== */

void MtMx_ProcessDataQ(This_t* me, uint32_t unInPortID)
{
	elite_msg_any_t        myDataQMsg;
	ADSPResult              result;
	MatrixInPortInfoType    *pCurrentInPort;


	MtMx_ClearInputPortChannelStatus(me, unInPortID);
	pCurrentInPort = me->inPortParams[unInPortID];

	switch (pCurrentInPort->inPortState)
	{
	case INPUT_PORT_STATE_READY:
	{
		result = qurt_elite_queue_pop_front(pCurrentInPort->inPortHandle.portHandle.dataQ, (uint64_t*)&myDataQMsg );
		pCurrentInPort->myDataQMsg = myDataQMsg;

		switch (pCurrentInPort->myDataQMsg.unOpCode)
		{
		case ELITE_DATA_MEDIA_TYPE:
		{
			result = MtMx_MsgMediaType(me, unInPortID, pCurrentInPort->myDataQMsg);

			if (ADSP_EOK == result)
			{
				//Update number of samples per channel required to fill the local buffer
				pCurrentInPort->unNumSamplesPerChReqToFillLocalBuf = pCurrentInPort->unInPortPerChBufSize;

				//There is a remote timing related issue, where an already connected i/p port was moved to ready state
				//because of a port state change, however the media type with the new sampling rate arrived before the
				//new maprouting command, thus leading to a mismatch in the sampling rate. This can be an issue because
				//opening the i/p port to accept more data at this point (active transition) can lead to a potential crash.
				//In such a case, the i/p port should remain in ready state, until the maprouting command actually arrives.
				if (TRUE == MtMx_CheckIfInputPortCanTransitionAwayFromReady(me, unInPortID))
				{
					if ((INPUT_PORT_STATE_PAUSED == pCurrentInPort->inPortStatePrToStateChange) ||
							(INPUT_PORT_STATE_ACTIVE_HOLDING_INPUT_BUFFER == pCurrentInPort->inPortStatePrToStateChange))
					{
						pCurrentInPort->inPortState = pCurrentInPort->inPortStatePrToStateChange;
					}
					else
					{
						pCurrentInPort->inPortState = INPUT_PORT_STATE_ACTIVE;
						pCurrentInPort->inPortStatePrToStateChange = INPUT_PORT_STATE_ACTIVE;
					}
					MtMx_InPortToUpdateWaitMask(me, unInPortID);
				}
			}
			else
			{
				elite_msg_finish_msg(&myDataQMsg, result);
				break;
			}

			elite_msg_data_media_type_apr_t* pMediaTypePayload = (elite_msg_data_media_type_apr_t*)myDataQMsg.pPayload;
			MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port %lu: returning media_type buffer", me->mtMxID, unInPortID);
			elite_msg_push_payload_to_returnq(pMediaTypePayload->pBufferReturnQ, (elite_msg_any_payload_t*)pMediaTypePayload);

			break;
		}
		case ELITE_DATA_BUFFER:
		{
			MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port %lu: rcvd data buffer in ready State, returning it", me->mtMxID, unInPortID);
			if(ADSP_FAILED(result = elite_msg_return_payload_buffer(&(pCurrentInPort->myDataQMsg))))
			{
				MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu i/p port %lu: failed to return i/p data buffer", me->mtMxID, unInPortID);
			}
			break;
		}
		case ELITE_DATA_EOS:
		{
			MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port %lu rcvd EOS in READY state", me->mtMxID, unInPortID);
			MtMx_MsgEos(me, unInPortID, pCurrentInPort->myDataQMsg);
			break;
		}
		case ELITE_DATA_MARK_BUFFER:
		{
			MtMx_ProcessMarkBuffer(me, unInPortID, pCurrentInPort->myDataQMsg, ASM_DATA_EVENT_MARK_BUFFER_PROCESSED);
			break;
		}
		case ELITE_DATA_SET_PARAM:
		{
			MtMx_MsgDataSetParam(me, unInPortID, pCurrentInPort->myDataQMsg);
			elite_msg_release_msg(&myDataQMsg);
			break;
		}
		default:
		{
			MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port %lu rcvd unexpected buffer in ready state, returning it", me->mtMxID, unInPortID);
			if(ADSP_FAILED(result = elite_msg_return_payload_buffer(&(pCurrentInPort->myDataQMsg))))
			{
				MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu i/p port %lu failed to return unexpected i/p buffer", me->mtMxID, unInPortID);
			}
			break;
		}
		}
		break;
	}
	case INPUT_PORT_STATE_ACTIVE:
	case INPUT_PORT_STATE_PAUSED:
	{
		if (ADM_MATRIX_ID_AUDIO_RX == me->mtMxID)
		{
			result = qurt_elite_queue_pop_front(pCurrentInPort->inPortHandle.portHandle.dataQ, (uint64_t*)&myDataQMsg );
			pCurrentInPort->myDataQMsg = myDataQMsg;
			pCurrentInPort->bIsInBufShort = FALSE;

			switch (pCurrentInPort->myDataQMsg.unOpCode)
			{
			case ELITE_DATA_BUFFER:
			{
				if (NULL == pCurrentInPort->pStartLoc)
				{
					MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx %lu i/p port %lu rcvd data buffer, pStartLoc = NULL, Media type message not recd yet, returning buffer",
							me->mtMxID, unInPortID);
					if(ADSP_FAILED(result = elite_msg_return_payload_buffer(&(pCurrentInPort->myDataQMsg))))
					{
						MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx %lu i/p port %lu Error: MtMx failed to return i/p buffer", me->mtMxID, unInPortID);
					}
					break;
				}

				pCurrentInPort->numBufRcvd++;

				//This buffer is yet to update AVSync Stats and Drift
				pCurrentInPort->bHasInputBufferUpdatedStats = FALSE;
				pCurrentInPort->bHasInputBufferUpdatedDrift = FALSE;

#ifdef MT_MX_EXTRA_DEBUG
				MSG_5(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port %lu: [num buf rcvd, num buf ret] = [%lu, %lu], strmask %lu",
						me->mtMxID, unInPortID, pCurrentInPort->numBufRcvd, pCurrentInPort->numBufReturned, pCurrentInPort->strMask);
#endif

				if (pCurrentInPort->numBufReturned != pCurrentInPort->numBufRcvd - 1)
				{
					MSG_4(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu i/p port %lu: Mismatch: [num buf rcvd, num buf ret] = [%lu, %lu]",
							me->mtMxID, unInPortID, pCurrentInPort->numBufRcvd, pCurrentInPort->numBufReturned);
				}

				//Debug message
				if (100 >= pCurrentInPort->numBufRcvd)
				{
					MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port %lu received %lu sample", me->mtMxID, unInPortID, pCurrentInPort->numBufRcvd);

					if (1 == pCurrentInPort->numBufRcvd)
					{
						// After receiving the first sample rescan the output ports for connected inputs with valid timestamps.
						// It is only after receiving the first sample (really buffer) that we know whether or not an input has
						// valid timestamps.
						MtMx_ScanOutputPortsForInputsWithValidTimestamps(me);
					}
				}

				pCurrentInPort->bInBufStatus = INPUT_BUFFER_HELD;

				// If Adjust Session Clk is enabled, change the number of samples that
				// should be read from the input buffer to fill the local buffer.
				if (pCurrentInPort->bIsSampleAddDropEnabled)
				{
					if (MT_MX_INPUT_PORT_ADD_SAMPLES == pCurrentInPort->samplesAddOrDropMask)
					{
						if (TRUE == pCurrentInPort->bIsThisFirstSampleAddOrDrop)
						{
							pCurrentInPort->unNumSamplesPerChReqToFillLocalBuf -= MT_MX_NUM_SAMPLES_TO_ADD_OR_DROP_PER_BUFFER;
							pCurrentInPort->bIsThisFirstSampleAddOrDrop = FALSE;

							MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Rcvd i/p buf and changed num samples req to fill lcl buf to %lu since this will be first interpolate",
									pCurrentInPort->unNumSamplesPerChReqToFillLocalBuf);
						}
					}
					else if(MT_MX_INPUT_PORT_DROP_SAMPLES == pCurrentInPort->samplesAddOrDropMask)
					{
						if (TRUE == pCurrentInPort->bIsThisFirstSampleAddOrDrop)
						{
							pCurrentInPort->unNumSamplesPerChReqToFillLocalBuf += MT_MX_NUM_SAMPLES_TO_ADD_OR_DROP_PER_BUFFER;
							pCurrentInPort->bIsThisFirstSampleAddOrDrop = FALSE;

							MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Rcvd i/p buf and changed num samples req to fill lcl buf to %lu since this will be first drop",
									pCurrentInPort->unNumSamplesPerChReqToFillLocalBuf);
						}
					}
				}

				// AV-Sync
				// MtMx_InPortToHonorTimestamp returns ADSP_EFAILED,
				// if the i/p buffer has to be held or dropped (not rendered).
				if(TRUE == pCurrentInPort->bForceCheckTimeStampValidity)
				{
					result = MtMx_InPortToHonorTimestamp(me, unInPortID);

					if (ADSP_EOK != result)
					{
						break;
					}
				}

				if (TRUE == pCurrentInPort->bIsLastMsgEOS)
				{
					pCurrentInPort->bIsLastMsgEOS = FALSE;
					// If the last received EOS was sent down and session clock freezed, unfreeze it
					// and reset session time to incoming TS
					if (TRUE == pCurrentInPort->bIsSessionUnderEOS)
					{
						pCurrentInPort->bIsSessionUnderEOS = FALSE;
						pCurrentInPort->bShouldSessionTimeBeDerivedFromNextTS = TRUE;
						pCurrentInPort->bUseDefaultWindowForRendering = TRUE;
					}
				}

				// Do not block on the i/p port till it runs out of samples
				MtMx_RemoveInputPortFromWaitMask(me,unInPortID);

				//If it has been determined that a Hold is required (< 1 i/p frame size), then handle appropriately
				if((pCurrentInPort->ullInBufHoldDurationInUsec.int_part > 0) && (pCurrentInPort->unNewNumAfeFrames > MT_MX_LL_NUM_AFE_FRAMES))
				{
					MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port %lu calling MtMx_MsgDataBufferHold from i/p port", me->mtMxID, unInPortID);
					MtMx_MsgDataBufferHold(me, unInPortID, pCurrentInPort);
				}
				else
				{
					//Explictly set the hold duration to 0
					mt_mx_clear_time(&pCurrentInPort->ullInBufHoldDurationInUsec);

					//Process the input buffer normally
					MtMx_MsgDataBuffer(me, unInPortID, pCurrentInPort);
				}

				// If local buffer is full OR input port has accumulation pending AND top priority output port has a
				// pending accumulation request, then trigger the accumulation
				if ((pCurrentInPort->bIsLocalBufFull || pCurrentInPort->bIsAccumulationPending) && ((1 << pCurrentInPort->unTopPrioOutPort) & (pCurrentInPort->outputReqPendingMask)))
				{
					// Common processing routine for this i/p port
					MxAr_CommonInPortProcessingRoutine(me, unInPortID, pCurrentInPort);

					// Reset the accumulation pending flag for this input port
					pCurrentInPort->bIsAccumulationPending = FALSE;
				}

				//Check if any pull ports are waiting to send data downstream
				MxAr_ProcessPendingPullOutPorts(me);

				break;
			} // End of data buffer processing

			case ELITE_DATA_EOS:
			{
				MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port %lu rcvd EOS in ACTIVE/PAUSED state", me->mtMxID, unInPortID);

				MtMx_MsgEos(me, unInPortID, pCurrentInPort->myDataQMsg);
				break;
			} // End of data EOS processing
			case ELITE_DATA_MARK_BUFFER:
			{
				MtMx_ProcessMarkBuffer(me, unInPortID, pCurrentInPort->myDataQMsg, ASM_DATA_EVENT_MARK_BUFFER_PROCESSED);
				break;
			}
			case ELITE_DATA_MEDIA_TYPE:
			{
				if (TRUE == pCurrentInPort->bIsLastMsgEOS)
				{
					pCurrentInPort->bIsLastMsgEOS = FALSE;

					// If the last received EOS was sent down and session clock freezed,
					// unfreeze it and reset session time to incoming TS
					if (TRUE == pCurrentInPort->bIsSessionUnderEOS)
					{
						pCurrentInPort->bIsSessionUnderEOS = FALSE;
						pCurrentInPort->bShouldSessionTimeBeDerivedFromNextTS = TRUE;
						pCurrentInPort->bUseDefaultWindowForRendering = TRUE;
					}
				}
				result = MtMx_MsgMediaType(me, unInPortID, pCurrentInPort->myDataQMsg);

				if (ADSP_EOK != result)
				{
					elite_msg_finish_msg(&myDataQMsg, result);
					break;
				}

				elite_msg_data_media_type_apr_t* pMediaTypePayload = (elite_msg_data_media_type_apr_t*)myDataQMsg.pPayload;
				MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port %lu: returning media_type buffer", me->mtMxID, unInPortID);
				elite_msg_push_payload_to_returnq(pMediaTypePayload->pBufferReturnQ, (elite_msg_any_payload_t*)pMediaTypePayload);

				break;
			}// End of Data Media Type case handling
			case ELITE_DATA_SET_PARAM:
			{
				MtMx_MsgDataSetParam(me, unInPortID, pCurrentInPort->myDataQMsg);
				elite_msg_release_msg(&myDataQMsg);
				break;
			}
			}
		}
		else
		{
			result = qurt_elite_queue_pop_front(pCurrentInPort->inPortHandle.portHandle.dataQ, (uint64_t*)&myDataQMsg );
			pCurrentInPort->myDataQMsg = myDataQMsg;
			pCurrentInPort->bIsInBufShort = FALSE;

			switch (pCurrentInPort->bIsPortLive)
			{
			case TRUE:
			{
				switch (pCurrentInPort->myDataQMsg.unOpCode)
				{
				case ELITE_DATA_BUFFER:
				{
					if (NULL == pCurrentInPort->pStartLoc)
					{
						MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx %lu i/p port %lu rcvd data buffer, pStartLoc = NULL, Media type message not recd yet, returning buffer",
								me->mtMxID, unInPortID);
						if(ADSP_FAILED(result = elite_msg_return_payload_buffer(&(pCurrentInPort->myDataQMsg))))
						{
							MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx %lu i/p port %lu Error: MtMx failed to return i/p buffer", me->mtMxID, unInPortID);
						}
						break;
					}

					//Check to see if this i/p port is connected in the first place. If not, DROP!!
					if (((1 << unInPortID) & (me->steadyStateInPortsMask)) && (pCurrentInPort->strMask))
					{
						pCurrentInPort->numBufRcvd++;

#ifdef MT_MX_EXTRA_DEBUG
						MSG_5(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu live i/p port %lu: [num buf rcvd, num buf ret] = [%lu, %lu], strmask %lu",
								me->mtMxID, unInPortID, pCurrentInPort->numBufRcvd, pCurrentInPort->numBufReturned, pCurrentInPort->strMask);
#endif

						if (pCurrentInPort->numBufReturned != pCurrentInPort->numBufRcvd - 1)
						{
							MSG_4(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu i/p port %lu: [num buf rcvd, num buf ret] = [%lu, %lu]",
									me->mtMxID, unInPortID, pCurrentInPort->numBufRcvd, pCurrentInPort->numBufReturned);
						}

						//Default this buffer to "Held" status
						pCurrentInPort->bInBufStatus = INPUT_BUFFER_HELD;
						MtMx_RemoveInputPortFromWaitMask(me, unInPortID);

						//Debug message
						if (100 >= pCurrentInPort->numBufRcvd)
						{
							MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu live i/p port %lu received %lu sample", me->mtMxID, unInPortID, pCurrentInPort->numBufRcvd);
						}

						//Update the incoming buffer timestamps
						{
							elite_msg_data_buffer_t*   pInputBuf;
							pInputBuf = (elite_msg_data_buffer_t*)((pCurrentInPort->myDataQMsg).pPayload);
							pCurrentInPort->ullIncomingBufferTS = pInputBuf->ullTimeStamp;
						}

						//Process the input buffer
						MtMx_MsgDataBuffer(me, unInPortID, pCurrentInPort);

						// Since the MXAT is i/p driven, the data will try to accumulate immediately (live port) if:
						// The current i/p port's local buf. is FULL.
						if (pCurrentInPort->bIsLocalBufFull)
						{
							MxAt_CommonInPortProcessingRoutine(me, unInPortID, pCurrentInPort);
						}

					}
					else
					{
						MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu: i/p port %lu dropping buffer as port is not connected to anything yet. InpQ: 0x%p", me->mtMxID, unInPortID, pCurrentInPort->inPortHandle.portHandle.dataQ);
						if (ADSP_FAILED(result = elite_msg_return_payload_buffer(&(pCurrentInPort->myDataQMsg))))
						{
							MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx %lu i/p port %lu Error: MtMx failed to return i/p buffer", me->mtMxID, unInPortID);
						}

						pCurrentInPort->bInBufStatus = INPUT_BUFFER_RELEASED;
						MtMx_AddInputPortToWaitMask(me, unInPortID);

#ifdef MT_MX_EXTRA_DEBUG
						MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx#%lu i/p port %lu, #Buf Released: %lu, i/p port added to wait mask",
								me->mtMxID, unInPortID, pCurrentInPort->numBufReturned);
#endif

					}

					//Check if any output port should be moved to ACTIVE state
					MtMx_CheckIfInportMovesAnyOutportToActiveState(me, unInPortID);
					break;
				}
				case ELITE_DATA_MEDIA_TYPE:
				{
					if(ADSP_FAILED(result = MtMx_MsgMediaType(me, unInPortID, pCurrentInPort->myDataQMsg)))
					{
						MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx %lu i/p port %lu Error: MtMx failed to process media type", me->mtMxID, unInPortID);
					}
					if(ADSP_FAILED(result = elite_msg_return_payload_buffer(&(pCurrentInPort->myDataQMsg))))
					{
						MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx %lu i/p port %lu Error: MtMx failed to return media type buffer", me->mtMxID, unInPortID);
					}
					break;
				}
				case ELITE_DATA_EOS:
				{
					MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port %lu rcvd EOS in ACTIVE/PAUSED state", me->mtMxID, unInPortID);
					MtMx_MsgEos(me, unInPortID, pCurrentInPort->myDataQMsg);
					break;
				}
				}
				break;
			}
			case FALSE:
			{
				switch (pCurrentInPort->myDataQMsg.unOpCode)
				{
				case ELITE_DATA_BUFFER:
				{
					if (NULL == pCurrentInPort->pStartLoc)
					{
						MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx %lu i/p port %lu rcvd data buffer, pStartLoc = NULL, Media type message not recd yet, returning buffer. InpQ: 0x%p", me->mtMxID, unInPortID, pCurrentInPort->inPortHandle.portHandle.dataQ);
						if(ADSP_FAILED(result = elite_msg_return_payload_buffer(&(pCurrentInPort->myDataQMsg))))
						{
							MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx %lu i/p port %lu Error: MtMx failed to return i/p buffer", me->mtMxID, unInPortID);
						}
						break;
					}

					//Check to see if this i/p port is connected in the first place. If not, DROP!!
					if (((1 << unInPortID) & (me->steadyStateInPortsMask)) && (pCurrentInPort->strMask))
					{
						pCurrentInPort->numBufRcvd++;

#ifdef MT_MX_EXTRA_DEBUG
						MSG_5(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu non-live i/p port %lu: [num buf rcvd, num buf ret] = [%lu, %lu], strmask %lu",
								me->mtMxID, unInPortID, pCurrentInPort->numBufRcvd, pCurrentInPort->numBufReturned, pCurrentInPort->strMask);
#endif

						if (pCurrentInPort->numBufReturned != pCurrentInPort->numBufRcvd - 1)
						{
							MSG_4(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu i/p port %lu: Mismatch: [num buf rcvd, num buf ret] = [%lu, %lu]",
									me->mtMxID, unInPortID, pCurrentInPort->numBufRcvd, pCurrentInPort->numBufReturned);
						}

						//Default this buffer to "Held" status
						pCurrentInPort->bInBufStatus = INPUT_BUFFER_HELD;
						MtMx_RemoveInputPortFromWaitMask(me, unInPortID);

						//Debug message
						if (100 >= pCurrentInPort->numBufRcvd)
						{
							MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu non-live i/p port %lu received %lu sample", me->mtMxID, unInPortID, pCurrentInPort->numBufRcvd);
						}

						//Update the incoming buffer timestamps
						{
							elite_msg_data_buffer_t*   pInputBuf;
							pInputBuf = (elite_msg_data_buffer_t*)((pCurrentInPort->myDataQMsg).pPayload);
							pCurrentInPort->ullIncomingBufferTS = pInputBuf->ullTimeStamp;
						}

						//Process the input buffer
						MtMx_MsgDataBuffer(me, unInPortID, pCurrentInPort);

						if(pCurrentInPort->strMask)
						{
							//Since the MXAT is i/p driven, the data will try to accumulate (non-live port) if:
							//1) The current i/p port's local buf. is FULL. AND
							//2) The top prio o/p port is seeking new data.
							if (pCurrentInPort->bIsLocalBufFull)
							{
								if((1 << pCurrentInPort->unTopPrioOutPort) & (pCurrentInPort->outputReqPendingMask))
								{
									MxAt_CommonInPortProcessingRoutine(me, unInPortID, pCurrentInPort);
								}
								else
								{
									uint32_t strMask = pCurrentInPort->strMask;

									while (strMask)
									{
										MatrixOutPortInfoType *pCurrentOutPort;
										uint32_t unOutPortID = Q6_R_ct0_R(strMask);
										strMask ^= (1 << unOutPortID);
										pCurrentOutPort = me->outPortParams[unOutPortID];

										//Check if this non-live i/p port was the top priority i/p port for any o/p port.
										if((FALSE == pCurrentOutPort->bIsTopPriorityInputPortLive) &&
												((pCurrentOutPort->inPortsTopPriorityMask) & (1<<unInPortID)))
										{
											MxAt_CommonInPortProcessingRoutine(me, unInPortID, pCurrentInPort);
											break;
										}
										else
										{
											//Else, inform the o/p port about this non-live i/p port waiting to acc.
											pCurrentOutPort->inPortsWaitingToAccMask |= (1 << unInPortID);
											pCurrentInPort->bIsNonLiveInputPortWaitingToAcc = TRUE;

											if((pCurrentOutPort->inPortsMask) && (pCurrentOutPort->inPortsWaitingToAccMask == pCurrentOutPort->inPortsMask))
											{
												MtMx_AddOutputPortToWaitMask(me, unOutPortID);
											}
										}
									}
								}
							}
						}
						else
						{
							MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu Non-live i/p port %lu not connected to any o/p port. Removing i/p port from wait mask and mark pending acc.", me->mtMxID, unInPortID);
							MtMx_RemoveInputPortFromWaitMask(me,unInPortID);
							pCurrentInPort->bIsNonLiveInputPortWaitingToAcc = TRUE;
						}
					}
					else
					{
						MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu: i/p port %lu dropping buffer as port is not connected to anything yet. InpQ: 0x%p", me->mtMxID, unInPortID, pCurrentInPort->inPortHandle.portHandle.dataQ);
						if (ADSP_FAILED(result = elite_msg_return_payload_buffer(&(pCurrentInPort->myDataQMsg))))
						{
							MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu i/p port %lu: failed to return i/p data buffer", me->mtMxID, unInPortID);
						}

						pCurrentInPort->bInBufStatus = INPUT_BUFFER_RELEASED;
						MtMx_AddInputPortToWaitMask(me, unInPortID);
						/*MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx#%lu i/p port %lu, #Buf Released: %lu, i/p port added to wait mask",
								me->mtMxID, unInPortID, pCurrentInPort->numBufReturned);*/
					}
					break;
				}
				case ELITE_DATA_EOS:
				{
					MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port %lu rcvd EOS in ACTIVE/PAUSED state", me->mtMxID, unInPortID);
					MtMx_MsgEos(me, unInPortID, pCurrentInPort->myDataQMsg);
					break;
				}
				case ELITE_DATA_MEDIA_TYPE:
				{
					if(ADSP_FAILED(result = MtMx_MsgMediaType(me, unInPortID, pCurrentInPort->myDataQMsg)))
					{
						MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx %lu i/p port %lu Error: MtMx failed to process media type", me->mtMxID, unInPortID);
					}
					if(ADSP_FAILED(result = elite_msg_return_payload_buffer(&(pCurrentInPort->myDataQMsg))))
					{
						MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu i/p port %lu: failed to return media type buffer", me->mtMxID, unInPortID);
					}
					break;
				}
				}
				break;
			}
			}
		}
		break;
	}
	default:
	{
		MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu i/p port %lu: rcvd buf in unexpected state [%ld]", me->mtMxID, unInPortID, pCurrentInPort->inPortState);
		if(ADSP_FAILED(result = elite_msg_return_payload_buffer(&(pCurrentInPort->myDataQMsg))))
		{
			MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu i/p port %lu failed to return unexpected i/p buffer", me->mtMxID, unInPortID);
		}
		break;
	}
	}
}

void MtMx_MsgDataBuffer(This_t *me, uint32_t unInPortID, MatrixInPortInfoType *pCurrentInPort)
{
	uint32_t                     numSamplesReqPerCh;
	elite_msg_data_buffer_t*     pInputBuf;
	pCurrentInPort->pInputBuf = (elite_msg_data_buffer_t*)((pCurrentInPort->myDataQMsg).pPayload);
	pInputBuf = pCurrentInPort->pInputBuf;

	pCurrentInPort->nNumRemainingSamplesPerCh = (pInputBuf->nActualSize /
			(pCurrentInPort->unNumChannels * pCurrentInPort->unBytesPerSample));

	pCurrentInPort->nNumSamplesUsedPerCh = 0;

	pCurrentInPort->pCurrentSample = (int8_t *)(&(pInputBuf->nDataBuf));

	// Logging the input PCM data to Matrix mixer service
	MtMx_LogPcmData(me, unInPortID, pCurrentInPort->pCurrentSample);

	//Add/Drop samples if needed
	if (pCurrentInPort->nNumRemainingSamplesPerCh >= (int)pCurrentInPort->unNumSamplesPerChReqToFillLocalBuf)
	{
		if(TRUE == pCurrentInPort->bIsSampleAddDropEnabled)
		{
			switch(pCurrentInPort->samplesAddOrDropMask)
			{
			case MT_MX_INPUT_PORT_ADD_SAMPLES:
			{
				MtMx_AddInterpolatedSamples(me, unInPortID);
				break;
			}
			case MT_MX_INPUT_PORT_DROP_SAMPLES:
			{
				MtMx_DropSamples(me, unInPortID);
				break;
			}
			}
		}
		numSamplesReqPerCh = pCurrentInPort->unNumSamplesPerChReqToFillLocalBuf;
	}
	else
	{
		//i/p buffer contains lesser samples than the size of local buffer (short buffer)
		numSamplesReqPerCh = pCurrentInPort->nNumRemainingSamplesPerCh;
		pCurrentInPort->bIsInBufShort = TRUE;
	}

	//Update timestamps
	if(pCurrentInPort->bIsTimeStampValid)
	{
		pCurrentInPort->ullTimeStampAtCopy.int_part = pCurrentInPort->ullIncomingBufferTS;
		pCurrentInPort->ullTimeStampAtCopy.frac_part = 0;
	}

	//Copy samples from i/p buffer to local buffer and update nNumRemainingSamplesPerCh
	MtMx_FillInPortLocalBuf(me, unInPortID, numSamplesReqPerCh);

	if ((FALSE == pCurrentInPort->bIsLocalBufFull && TRUE == pCurrentInPort->bIsInBufShort) ||
			(0 == pCurrentInPort->nNumRemainingSamplesPerCh))
	{
		// The incoming buffer did not contain sufficient samples to fill local buffer
		// return the buffer and add the input port to the wait mask
		ADSPResult result;

		if (ADSP_FAILED(result = elite_msg_return_payload_buffer(&(pCurrentInPort->myDataQMsg))))
		{
			MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu i/p port %lu: failed to return i/p data buffer", me->mtMxID, unInPortID);
		}

		pCurrentInPort->bInBufStatus = INPUT_BUFFER_RELEASED;
		pCurrentInPort->numBufReturned++;
		MtMx_AddInputPortToWaitMask(me, unInPortID);

#ifdef MT_MX_EXTRA_DEBUG
		MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx#%lu i/p port %lu, #Buf Released: %lu, i/p port added to wait mask",
				me->mtMxID, unInPortID, pCurrentInPort->numBufReturned);
#endif

	}
}

void MtMx_MsgDataBufferHold(This_t *me, uint32_t unInPortID, MatrixInPortInfoType *pCurrentInPort)
{
	elite_msg_data_buffer_t*     pInputBuf;
	pCurrentInPort->pInputBuf = (elite_msg_data_buffer_t*)((pCurrentInPort->myDataQMsg).pPayload);

	pInputBuf = pCurrentInPort->pInputBuf;
	pCurrentInPort->nNumRemainingSamplesPerCh = (pInputBuf->nActualSize /
			(pCurrentInPort->unNumChannels * pCurrentInPort->unBytesPerSample));
	pCurrentInPort->nNumSamplesUsedPerCh = 0;
	pCurrentInPort->pCurrentSample = (int8_t *)(&(pInputBuf->nDataBuf));

	// Logging the input PCM data to Matrix mixer service
	MtMx_LogPcmData(me, unInPortID, pCurrentInPort->pCurrentSample);

	//Update timestamps
	if(pCurrentInPort->bIsTimeStampValid)
	{
		pCurrentInPort->ullTimeStampAtCopy.int_part = pCurrentInPort->ullIncomingBufferTS;
		pCurrentInPort->ullTimeStampAtCopy.frac_part = 0;
	}

	//Now, call the common hold routine
	MtMx_HoldCommonRoutine(me, unInPortID, pCurrentInPort);
}

void MxAr_CommonInPortProcessingRoutine(This_t *me, uint32_t unInPortID, MatrixInPortInfoType *pCurrentInPort)
{
	ADSPResult  result;
	mt_mx_sampleslip_t *pSampleSlip = &(pCurrentInPort->structSampleSlip);

	//Accumulate samples to accBufs of all o/p ports that have request pending for data from this i/p port
	MxAr_ApplyGainAndAccumulateSamples(me, unInPortID);

	if(TRUE == pCurrentInPort->bShouldActiveInputPortBeReProcessed && NULL != pSampleSlip->pSampleSlipAppi)
	{
		//Special case handling when SSLib was introduced
		//MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port %lu MtMx_MsgDataBuffer will be called from o/p port. Exiting now", me->mtMxID, unInPortID);
		return;
	}

#ifdef MT_MX_EXTRA_DEBUG
	MSG_4(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx#%lu i/p port %lu Post steps, Hold Duration [%lu, %lu]",
			me->mtMxID, unInPortID, (uint32_t)(pCurrentInPort->ullInBufHoldDurationInUsec.int_part >> 32), (uint32_t)(pCurrentInPort->ullInBufHoldDurationInUsec.int_part));
#endif

	//At this point, check if there are any hold-zeros that are remaining to be filled.
	if(0 == pCurrentInPort->ullInBufHoldDurationInUsec.int_part)
	{
		//Try refilling the i/p port's local buffer with new samples from upstream peer's o/p buffer
		if (pCurrentInPort->nNumRemainingSamplesPerCh <= (int)pCurrentInPort->unNumSamplesPerChReqToFillLocalBuf)
		{
			//i/p buffer has insufficient samples to fill local buffer
			//Because of sample slip, there may be a use case where there might be a small # of samples still left over in the i/p Q, that needs to be copied over
			//i/p buffer has insufficient samples to fill local buffer, however call the fill local buffer routine again to exhaust any remainder samples in the i/p Q.
			while (pCurrentInPort->nNumRemainingSamplesPerCh > 0 && FALSE == pCurrentInPort->bIsLocalBufFull)
			{
				MtMx_FillInPortLocalBuf(me, unInPortID, pCurrentInPort->nNumRemainingSamplesPerCh);
			}

			//Update timestamps
			if(pCurrentInPort->bIsTimeStampValid)
			{
				mt_mx_increment_time(&pCurrentInPort->ullTimeStampAtCopy, pCurrentInPort->unFrameDurationInUsec);
			}

			//Now it is OK to return the buffer, as it has been established that the i/p Q buffer has truly been emptied, at this point.
			if (INPUT_BUFFER_HELD == pCurrentInPort->bInBufStatus)
			{
				if (ADSP_FAILED(result = elite_msg_return_payload_buffer(&(pCurrentInPort->myDataQMsg))))
				{
					MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu i/p port %lu: failed to return i/p data buffer", me->mtMxID, unInPortID);
				}

				pCurrentInPort->bInBufStatus = INPUT_BUFFER_RELEASED;
				pCurrentInPort->numBufReturned++;
			}
			MtMx_AddInputPortToWaitMask(me, unInPortID);

#ifdef MT_MX_EXTRA_DEBUG
			MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx#%lu i/p port %lu, #Buf Released: %lu, i/p port added to wait mask",
					me->mtMxID, unInPortID, pCurrentInPort->numBufReturned);
#endif

		}
		else
		{
			//i/p buffer has sufficient samples to fill local buffer
			if(pCurrentInPort->bIsSampleAddDropEnabled)
			{
				switch(pCurrentInPort->samplesAddOrDropMask)
				{
				case MT_MX_INPUT_PORT_ADD_SAMPLES:
				{
					MtMx_AddInterpolatedSamples(me, unInPortID);
					break;
				}
				case MT_MX_INPUT_PORT_DROP_SAMPLES:
				{
					MtMx_DropSamples(me, unInPortID);
					break;
				}
				}
			}

			//Update timestamps
			if(pCurrentInPort->bIsTimeStampValid)
			{
				mt_mx_increment_time(&pCurrentInPort->ullTimeStampAtCopy, pCurrentInPort->unFrameDurationInUsec);
			}

			//copy samples from i/p buffer to local buffer and update nNumRemainingSamplesPerCh
			MtMx_FillInPortLocalBuf(me, unInPortID, pCurrentInPort->unNumSamplesPerChReqToFillLocalBuf);
		}
	}
	else
	{
		//If hold duration is less than the rounding < 1ms, treat it as immediate.
		if (pCurrentInPort->ullInBufHoldDurationInUsec.int_part < MT_MX_FRAME_DURATION_1000US)
		{
			mt_mx_clear_time(&pCurrentInPort->ullInBufHoldDurationInUsec);
		}

		//Update timestamps
		if (pCurrentInPort->bIsTimeStampValid)
		{
			mt_mx_increment_time(&pCurrentInPort->ullTimeStampAtCopy, pCurrentInPort->unFrameDurationInUsec);
		}

		//Now, call the common hold routine
		MtMx_HoldCommonRoutine(me, unInPortID, pCurrentInPort);
	}
}

void MxAt_CommonInPortProcessingRoutine(This_t *me, uint32_t unInPortID, MatrixInPortInfoType *pCurrentInPort)
{
	ADSPResult  result;

	//Try to Accumulate samples to accBufs of all o/p ports connected to this i/p port
	MxAt_ApplyGainAndAccumulateSamples(me, unInPortID);

	//If a non-live i/p port could not accumulate to any o/p port, that i/p port needs to wait, holding the data until acc. can be achieved
	if((FALSE == pCurrentInPort->bIsPortLive) && (TRUE == pCurrentInPort->bIsNonLiveInputPortWaitingToAcc))
	{
		return;
	}

	//Try refilling the i/p port's local buffer with new samples from upstream peer's o/p buffer
	if (pCurrentInPort->nNumRemainingSamplesPerCh <= (int)pCurrentInPort->unNumSamplesPerChReqToFillLocalBuf)
	{
		//I/p buffer has insufficient samples to fill local buffer
		//i. if nNumRemainingSamples is non-zero, copy them to local buffer
		if (pCurrentInPort->nNumRemainingSamplesPerCh)
		{
			MtMx_FillInPortLocalBuf(me, unInPortID, pCurrentInPort->nNumRemainingSamplesPerCh);
		}

		//ii. if held, return that buffer to the upstream peer's bufQ
		if (INPUT_BUFFER_HELD == pCurrentInPort->bInBufStatus)
		{
			if (ADSP_FAILED(result = elite_msg_return_payload_buffer(&(pCurrentInPort->myDataQMsg))))
			{
				MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu i/p port %lu: failed to return i/p data buffer", me->mtMxID, unInPortID);
			}
			pCurrentInPort->bInBufStatus = INPUT_BUFFER_RELEASED;
			pCurrentInPort->numBufReturned++;
		}
		//iii. add dataQ to the wait mask
		MtMx_AddInputPortToWaitMask(me, unInPortID);

#ifdef MT_MX_EXTRA_DEBUG
		MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx#%lu i/p port %lu, #Buf Released: %lu, i/p port added to wait mask",
				me->mtMxID, unInPortID, pCurrentInPort->numBufReturned);
#endif

	}
	else
	{
		//I/p buffer has sufficient samples to fill local buffer
		//i. copy samples from i/p buffer to local buffer and update nNumRemainingSamplesPerCh
		MtMx_FillInPortLocalBuf(me, unInPortID, pCurrentInPort->unNumSamplesPerChReqToFillLocalBuf);
		//As a result of above op, if the local buffer did get filled up, then re-trigger accumulation.
		if (pCurrentInPort->bIsLocalBufFull)
		{
			//Common processing routine for this i/p port
			MxAt_CommonInPortProcessingRoutine(me, unInPortID, pCurrentInPort);
		}
	}
}

ADSPResult MtMx_InPortToHonorTimestamp(This_t *me, uint32 unInPortID)
{
	ADSPResult                 result;
	MatrixInPortInfoType       *pCurrentInPort = me->inPortParams[unInPortID];
	elite_msg_data_buffer_t*   pInputBuf;
	int64_t                    unDataRcvdInUsec;
	uint32_t ullCurrentDevicePathDelay = 0, ullCurrentDevicePathDelayComp = 0;
	mt_mx_sampleslip_t	    *pSampleSlip        = &(pCurrentInPort->structSampleSlip);
	avsync_rendering_decision_t rendering_decision = RENDER;
	int64_t delta = 0;

	//Make sure that the pCurrentInPort->unTopPrioOutPort is valid.
	if(pCurrentInPort->unTopPrioOutPort < MT_MX_MAX_OUTPUT_PORTS)
	{
		MatrixOutPortInfoType       *pTopPrioOutPort = me->outPortParams[pCurrentInPort->unTopPrioOutPort];

		//Make sure that the punAFEDelay, punCoppDelay, punMtMxOutDelay are all valid.
		if((NULL != pTopPrioOutPort) && (NULL != pTopPrioOutPort->punAFEDelay) && (NULL != pTopPrioOutPort->punCoppBufDelay) && (NULL != pTopPrioOutPort->punCoppAlgDelay) && (NULL != pCurrentInPort->punMtMxInDelay))
		{
			ullCurrentDevicePathDelay = *(pTopPrioOutPort->punAFEDelay) + *(pTopPrioOutPort->punCoppBufDelay) + *(pTopPrioOutPort->punCoppAlgDelay) + *(pCurrentInPort->punMtMxInDelay);

			// The session time calculation happens in the output port context. So the rendering decision should only take into account the delay from the
			// output port down. We should thus remove the delays of the acc buf and the internal buf. Based on when the buffer was sent down on the device,
			// this can vary from (frame duration) to 2*(frame duration). Here we take the average.
			ullCurrentDevicePathDelayComp = ullCurrentDevicePathDelay - (pTopPrioOutPort->unFrameDurationInUsec.int_part) - 0.5*(pTopPrioOutPort->unFrameDurationInUsec.int_part);

		}
	}

	pCurrentInPort->pInputBuf = (elite_msg_data_buffer_t*)((pCurrentInPort->myDataQMsg).pPayload);
	pInputBuf = pCurrentInPort->pInputBuf;
	pCurrentInPort->bIsTimeStampValid = (bool_t)asm_get_timestamp_valid_flag(pInputBuf->nFlag);

	if (TRUE == pCurrentInPort->bShouldSessionTimeBeDerivedFromNextTSGapless)
	{
		//For gapless playback, reset session clk to zero before checking the timestamp validity flag so that in case the TS validity flag
		//is FALSE, session clk for the new stream begins from 0
		MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port %lu: Reset Gapless SessionTime to 0", me->mtMxID, unInPortID);
		MtMx_ResetSessionTimeClock(me, unInPortID);
	}

	/* if i/p buffer timestamp is not valid, simply return. In such case:
      a. Render the buffer immediately (skip the below logic of checking whether buffer has to be held or dropped),
      b. Do not increment session clock if silence is inserted (done in MtMx_OutPortToHonorInPortsTimestamps), and
      c. Ignore timestamps till the end of playback
	 */
	if (!pCurrentInPort->bIsTimeStampValid)
	{
		if((TRUE == pCurrentInPort->bShouldSessionTimeBeDerivedFromNextTS) ||
				(TRUE == pCurrentInPort->bShouldSessionTimeBeDerivedFromNextTSGapless))
		{
			pCurrentInPort->bShouldSessionTimeBeDerivedFromNextTS = FALSE;
			pCurrentInPort->bUseDefaultWindowForRendering = FALSE;
			pCurrentInPort->bShouldSessionTimeBeDerivedFromNextTSGapless = FALSE;
		}
		pCurrentInPort->bForceCheckTimeStampValidity = FALSE; //Set this to FALSE and never check for TS validity ever for this session (exception: 2nd gapless stream)
		MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port %lu: [TS flag = 0], ignoring all future TS's", me->mtMxID, unInPortID);

		// Since the input port timestamp validity has changed state from TRUE -> FALSE we need to trigger a rescan
		// on the output ports for any connected input ports with a valid timestamp.
		MtMx_ScanOutputPortsForInputsWithValidTimestamps(me);
		return ADSP_EOK;
	}

	/* The following procedure will calculate the current frame duration in microseconds. This procedure will
	 * avoid accumulating error over time due to truncation for frame sizes that aren't integer multiples
	 * of the sampling rate. The procedure will:
		1. Update number of sample received (over last one second).
      2. Calculate data received in microseconds from total number of samples received (over last one second).
      3. Calculate data received in microseconds for current frame by subtracting previous number.
         This will give current frame duration in microseconds.
      4. Roll over frame size and frame duration in microseconds every one second.
	 */

	//Update number of sample received (over last one second).
	pCurrentInPort->unDataRcvdInSamples += ((pInputBuf->nActualSize) / (pCurrentInPort->unNumChannels * pCurrentInPort->unBytesPerSample));

	//Calculate data received in microseconds from number of samples received (over last one second).
	unDataRcvdInUsec = MT_MX_SAMPLES_TO_USEC(pCurrentInPort->unDataRcvdInSamples, pCurrentInPort->unSampleRate);

	//Calculate data received in micro sec for current frame by subtracting previous number. 
	//This will give current frame duration in usec.
	unDataRcvdInUsec -= pCurrentInPort->unDataRcvdInUsec.int_part;

	// roll over frame size and frame duration in usec to within one second
	pCurrentInPort->unDataRcvdInUsec.int_part += unDataRcvdInUsec;	
	if (pCurrentInPort->unDataRcvdInSamples >= pCurrentInPort->unSampleRate)
	{
		pCurrentInPort->unDataRcvdInSamples -= pCurrentInPort->unSampleRate;
	}	
	if (pCurrentInPort->unDataRcvdInUsec.int_part >= 1000000)
	{
		pCurrentInPort->unDataRcvdInUsec.int_part -= 1000000;
	}

	//Update timestamps
	pCurrentInPort->ullIncomingBufferTS = pInputBuf->ullTimeStamp;

#ifdef MT_MX_EXTRA_DEBUG
	MSG_7(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu: i/p port %lu InputTS [%lu, %lu] usec, TSValidity: %d, current frame dur [%lu, %lu] usec", me->mtMxID, unInPortID,
			(uint32_t)(pInputBuf->ullTimeStamp>>32),(uint32_t)(pInputBuf->ullTimeStamp), pCurrentInPort->bIsTimeStampValid,
			(uint32_t)(unDataRcvdInUsec>>32), (uint32_t)unDataRcvdInUsec);
#endif

	//TODO:unify start flag.
	if(ASM_SESSION_CMD_RUN_START_TIME_RUN_AT_ABSOLUTE_TIME != pCurrentInPort->unStartFlag &&
			ASM_SESSION_CMD_RUN_START_TIME_RUN_WITH_DELAY != pCurrentInPort->unStartFlag)
	{
		avsync_lib_make_rendering_decision(pCurrentInPort->pAVSyncLib,
				pInputBuf->ullTimeStamp,
				(uint64_t)0,
				(bool_t)TRUE,
				&delta,
				&rendering_decision);

		//If this is the first TS received after a RUN cmd with RUN_IMMEDIATE, then obtain session time from i/p buffer TS
		if (TRUE == pCurrentInPort->bShouldSessionTimeBeDerivedFromNextTS)
		{
			//If the stat window ends are setup, call an update on the AVSync stat library, keeping in mind update has to be called only once per buffer
			if(FALSE == pCurrentInPort->bHasInputBufferUpdatedStats && HOLD != rendering_decision)
			{
				//Note that ADSP_ENOTREADY denotes that start and end of render windows haven't been set.
				if(ADSP_EOK ==avsync_lib_update_stat(pCurrentInPort->pAVSyncLib, delta, unDataRcvdInUsec))
				{
					pCurrentInPort->bHasInputBufferUpdatedStats = TRUE;
				}
			}

			//Update the drift, keeping in mind update has to be called only once per buffer
			if(FALSE == pCurrentInPort->bHasInputBufferUpdatedDrift)
			{
				avsync_lib_update_s2d_drift(pCurrentInPort->pAVSyncLib, pCurrentInPort->ullIncomingBufferTS, (uint32_t)unDataRcvdInUsec);
				pCurrentInPort->bHasInputBufferUpdatedDrift = TRUE;
			}

			pCurrentInPort->bShouldSessionTimeBeDerivedFromNextTS = FALSE;
			pCurrentInPort->bUseDefaultWindowForRendering = FALSE;
			pCurrentInPort->bShouldSessionTimeBeDerivedFromNextTSGapless = FALSE;
			pCurrentInPort->bIsStrClkSyncdWithSessClk = TRUE;
			pCurrentInPort->ullTimeStampForStcBaseInUsec.int_part = pInputBuf->ullTimeStamp;
			pCurrentInPort->ullTimeStampForStcBaseInUsec.frac_part = 0;
			//Since we are maintaining a base value for STC, the num samples for update
			//should also be reset when the RUN command is issued.
			pCurrentInPort->ullNumSamplesForStcUpdate = 0;
			avsync_lib_set_internal_param(pCurrentInPort->pAVSyncLib,SESSION_CLOCK,&pInputBuf->ullTimeStamp,sizeof(pInputBuf->ullTimeStamp));
			uint64_t proposed_expected_session_clock = pInputBuf->ullTimeStamp + unDataRcvdInUsec;
			avsync_lib_set_internal_param(pCurrentInPort->pAVSyncLib,EXPECTED_SESSION_CLOCK,&proposed_expected_session_clock,sizeof(proposed_expected_session_clock));
			return ADSP_EOK;
		}

		//If the stat window ends are setup, call an update on the AVSync stat library, keeping in mind update has to be called only once per buffer
		if(HOLD == rendering_decision)
		{
			pCurrentInPort->ullInBufHoldDurationInUsec.int_part = (-delta);
			pCurrentInPort->ullInBufHoldDurationInUsec.frac_part = 0;

			//If hold duration is less than the 1ms, treat it as immediate.
			if(pCurrentInPort->ullInBufHoldDurationInUsec.int_part < MT_MX_FRAME_DURATION_1000US)
			{
				mt_mx_clear_time(&pCurrentInPort->ullInBufHoldDurationInUsec);
				delta = 0;
				rendering_decision = RENDER;
				MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port %lu: Hold duration was < 1ms, decision to render instead", me->mtMxID, unInPortID);
			}
		}

		//If the stat window ends are setup, call an update on the AVSync stat library, keeping in mind update has to be called only once per buffer
		if(FALSE == pCurrentInPort->bHasInputBufferUpdatedStats)
		{
			avsync_lib_update_stat(pCurrentInPort->pAVSyncLib, delta, unDataRcvdInUsec);
			pCurrentInPort->bHasInputBufferUpdatedStats = TRUE;
		}

		if (RENDER == rendering_decision)
		{
			uint64_t proposed_expected_session_clock;
			avsync_lib_get_internal_param(pCurrentInPort->pAVSyncLib,EXPECTED_SESSION_CLOCK,&proposed_expected_session_clock);
			proposed_expected_session_clock += unDataRcvdInUsec;
			avsync_lib_set_internal_param(pCurrentInPort->pAVSyncLib,EXPECTED_SESSION_CLOCK,&proposed_expected_session_clock,sizeof(proposed_expected_session_clock));
			if(0 != delta && pCurrentInPort->bIsStrClkSyncdWithSessClk == FALSE)
			{
				avsync_lib_set_internal_param(pCurrentInPort->pAVSyncLib,SESSION_CLOCK,&pInputBuf->ullTimeStamp,sizeof(pInputBuf->ullTimeStamp));
			}

			//Update the drift, keeping in mind update has to be called only once per buffer
			if(FALSE == pCurrentInPort->bHasInputBufferUpdatedDrift)
			{
				avsync_lib_update_s2d_drift(pCurrentInPort->pAVSyncLib, pInputBuf->ullTimeStamp, unDataRcvdInUsec);
				pCurrentInPort->bHasInputBufferUpdatedDrift = TRUE;
			}
			pCurrentInPort->bIsStrClkSyncdWithSessClk = TRUE;

			return ADSP_EOK;
		}
		else if (DROP == rendering_decision)
		{
			MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port %lu: Past TS, drop buf", me->mtMxID, unInPortID);

			//Update timestamps
			pCurrentInPort->ullTimeStampAtCopy.int_part = pInputBuf->ullTimeStamp;
			pCurrentInPort->ullTimeStampAtCopy.frac_part = 0;
			pCurrentInPort->ullTimeStampAtAccumulation.int_part = pInputBuf->ullTimeStamp;
			pCurrentInPort->ullTimeStampAtAccumulation.frac_part = 0;
			pCurrentInPort->ullTimeStampProcessed.int_part = pInputBuf->ullTimeStamp;
			pCurrentInPort->ullTimeStampProcessed.frac_part = 0;

			MSG_5(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu: i/p port %lu InputTS [%lu, %lu], TSValidity: %d", me->mtMxID, unInPortID,
					(uint32_t)(pInputBuf->ullTimeStamp>>32),(uint32_t)(pInputBuf->ullTimeStamp), pCurrentInPort->bIsTimeStampValid);

			//Drop the buffer
			if (ADSP_FAILED(result = elite_msg_return_payload_buffer(&(pCurrentInPort->myDataQMsg))))
			{
				MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to release i/p buffer back to upstr svc!\n");
			}
			pCurrentInPort->numBufReturned++;
			pCurrentInPort->bInBufStatus = INPUT_BUFFER_RELEASED;

			//Now, that the absolute time and session time are updated, commit the AVSync update changes for this i/p port.
			ADSPResult commit_result = avsync_lib_commit_stat(pCurrentInPort->pAVSyncLib);
			if (ADSP_FAILED(commit_result) && ADSP_EBADPARAM != commit_result)
			{
				MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu: in i/p port %lu context, failed to commit AVSync stats with result=%d", me->mtMxID, unInPortID, commit_result);
			}

			//No holding
			mt_mx_clear_time(&pCurrentInPort->ullInBufHoldDurationInUsec);
			return ADSP_EFAILED;
		}
		else
		{
			MSG_4(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port %lu: Future TS, hold buf for MSW: %lu LSW: %lu usec",
					me->mtMxID, unInPortID, (uint32_t)(pCurrentInPort->ullInBufHoldDurationInUsec.int_part >> 32), (uint32_t)(pCurrentInPort->ullInBufHoldDurationInUsec.int_part));

			//If hold duration is less than the 1ms, treat it as immediate.
			MtMx_RemoveInputPortFromWaitMask(me, unInPortID);
			pCurrentInPort->inPortState = INPUT_PORT_STATE_ACTIVE_HOLDING_INPUT_BUFFER;
			pCurrentInPort->inPortStatePrToStateChange = INPUT_PORT_STATE_ACTIVE_HOLDING_INPUT_BUFFER;
			return ADSP_EFAILED;
		}
	}
	else
	{
		//ASM_SESSION_CMD_RUN_START_TIME_RUN_AT_ABSOLUTE_TIME
		//ASM_SESSION_CMD_RUN_START_TIME_RUN_WITH_DELAY
		//IMPORTANT: Start time has already been verified to be positive at this point.
		//If client is using relative TS, the first buffer will have 0 TS.
		//If client is using absolute TS, the first buffer may have non 0 absolute TS.

		//Account for any partly filled input buffer at this point.
		uint32_t unSamplesAlreadyFilled = pCurrentInPort->unInPortPerChBufSize - pCurrentInPort->unNumSamplesPerChReqToFillLocalBuf;
		uint64_t ullTimeOffsetUsec = 0;

		if(0 != pCurrentInPort->unSamplesPer1Msec)
		{
			ullTimeOffsetUsec = MT_MX_SAMPLES_TO_USEC(unSamplesAlreadyFilled, pCurrentInPort->unSampleRate);
		}
		else
		{
			ullTimeOffsetUsec = MT_MX_SAMPLES_TO_USEC(unSamplesAlreadyFilled, MT_MX_SAMPLING_RATE_48000);
		}

		if(ullTimeOffsetUsec)
		{
			MSG_5(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port %lu: OffsetUsec: [%lu %lu], unSamplesAlreadyFilled: %lu",
					me->mtMxID, unInPortID, (uint32_t)(ullTimeOffsetUsec >> 32), (uint32_t)(ullTimeOffsetUsec), unSamplesAlreadyFilled);
		}

		//Special case handling when SSLib was introduced
		if(unSamplesAlreadyFilled == pCurrentInPort->unInPortPerChBufSize && NULL != pSampleSlip->pSampleSlipAppi)
		{
			/*MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port %lu: unSamplesAlreadyFilled: %lu matches i/p port sample size. This will be processed later.",
					me->mtMxID, unInPortID, unSamplesAlreadyFilled);*/
			pCurrentInPort->bShouldActiveInputPortBeReProcessed = TRUE;
			mt_mx_clear_time(&pCurrentInPort->ullInBufHoldDurationInUsec);

			//Stop further processing from this port.
			MtMx_RemoveInputPortFromWaitMask(me, unInPortID);
			return ADSP_EFAILED;
		}

		avsync_lib_update_stc_clock(pCurrentInPort->pAVSyncLib);

		if(TRUE == pCurrentInPort->bShouldSessionTimeBeDerivedFromNextTS)
		{
			//For local STC based rendering, this mode will be used at all times.
			//That means, each buffer will be treated as the first buffer.
			//Therefore, bShouldSessionTimeBeDerivedFromNextTS will always be TRUE for local STC.
			//For default rendering, this mode will be used only for the 1st buffer after a RUN command.
			avsync_lib_make_rendering_decision(pCurrentInPort->pAVSyncLib,
					pInputBuf->ullTimeStamp,
					(uint64_t)ullCurrentDevicePathDelayComp+ullTimeOffsetUsec,
					FALSE,
					&delta,
					&rendering_decision);

			if(HOLD == rendering_decision)
			{
				pCurrentInPort->ullInBufHoldDurationInUsec.int_part = (-delta);
				pCurrentInPort->ullInBufHoldDurationInUsec.frac_part = 0;

				//If hold duration is less than the 1ms, treat it as immediate.
				if(pCurrentInPort->ullInBufHoldDurationInUsec.int_part < MT_MX_FRAME_DURATION_1000US)
				{
					mt_mx_clear_time(&pCurrentInPort->ullInBufHoldDurationInUsec);
					delta = 0;
					rendering_decision = RENDER;
					MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port %lu: Hold duration was < 1ms, decision to render instead", me->mtMxID, unInPortID);
				}
			}

			//Compare Wall clock (+ Device latency) <==> Start time + TS
			if((RENDER == rendering_decision))
			{

				if(FALSE == pCurrentInPort->bHasInputBufferUpdatedStats)
				{
					avsync_lib_update_stat(pCurrentInPort->pAVSyncLib, delta, unDataRcvdInUsec);
					pCurrentInPort->bHasInputBufferUpdatedStats = TRUE;
				}

				//Update the drift, keeping in mind update has to be called only once per buffer
				if(FALSE == pCurrentInPort->bHasInputBufferUpdatedDrift)
				{
					avsync_lib_update_s2d_drift(pCurrentInPort->pAVSyncLib, pInputBuf->ullTimeStamp, unDataRcvdInUsec);
					pCurrentInPort->bHasInputBufferUpdatedDrift = TRUE;
				}

				//Update [session time, absolute time] pair.
				avsync_lib_set_internal_param(pCurrentInPort->pAVSyncLib,SESSION_CLOCK,&pInputBuf->ullTimeStamp,sizeof(pInputBuf->ullTimeStamp));
				uint64_t proposed_expected_session_clock = pInputBuf->ullTimeStamp + unDataRcvdInUsec;
				avsync_lib_set_internal_param(pCurrentInPort->pAVSyncLib, EXPECTED_SESSION_CLOCK, &proposed_expected_session_clock, sizeof(proposed_expected_session_clock));
				avsync_lib_update_absolute_time(pCurrentInPort->pAVSyncLib,ullCurrentDevicePathDelay+ullTimeOffsetUsec,FALSE);

				//No holding required.
				mt_mx_clear_time(&pCurrentInPort->ullInBufHoldDurationInUsec);
				uint32_t current_avsync_rendering_decision =ASM_SESSION_MTMX_STRTR_PARAM_RENDER_DECISION_DEFAULT;
				avsync_lib_get_internal_param(pCurrentInPort->pAVSyncLib,RENDERING_DECISION_TYPE,&current_avsync_rendering_decision);
				//Sync done.
				if(ASM_SESSION_MTMX_STRTR_PARAM_RENDER_DECISION_DEFAULT == current_avsync_rendering_decision)
				{
					pCurrentInPort->bShouldSessionTimeBeDerivedFromNextTS =  FALSE;
				}
				else
				{
					pCurrentInPort->bShouldSessionTimeBeDerivedFromNextTS =  TRUE;
				}
				pCurrentInPort->bUseDefaultWindowForRendering = FALSE;
				pCurrentInPort->bShouldSessionTimeBeDerivedFromNextTSGapless = FALSE;
				pCurrentInPort->bIsStrClkSyncdWithSessClk = TRUE;

				return ADSP_EOK;
			}
			else if(DROP == rendering_decision)
			{
				MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port %lu: Past TS, drop buf", me->mtMxID, unInPortID);

				//Update timestamps
				pCurrentInPort->ullTimeStampAtCopy.int_part = pInputBuf->ullTimeStamp;
				pCurrentInPort->ullTimeStampAtCopy.frac_part = 0;
				pCurrentInPort->ullTimeStampAtAccumulation.int_part = pInputBuf->ullTimeStamp;
				pCurrentInPort->ullTimeStampAtAccumulation.frac_part = 0;
				pCurrentInPort->ullTimeStampProcessed.int_part = pInputBuf->ullTimeStamp;
				pCurrentInPort->ullTimeStampProcessed.frac_part = 0;

				MSG_5(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu: i/p port %lu InputTS [%lu, %lu], TSValidity: %d", me->mtMxID, unInPortID,
						(uint32_t)(pInputBuf->ullTimeStamp>>32),(uint32_t)(pInputBuf->ullTimeStamp), pCurrentInPort->bIsTimeStampValid);

				//No holding required.
				mt_mx_clear_time(&pCurrentInPort->ullInBufHoldDurationInUsec);
				//If the stat window ends are setup, call an update on the AVSync stat library, keeping in mind update has to be called only once per buffer
				//If the stat window ends are setup, call an update on the AVSync stat library, keeping in mind update has to be called only once per buffer
				if(FALSE == pCurrentInPort->bHasInputBufferUpdatedStats)
				{
					avsync_lib_update_stat(pCurrentInPort->pAVSyncLib, delta, unDataRcvdInUsec);
					pCurrentInPort->bHasInputBufferUpdatedStats = TRUE;
				}

				//Past TS, Drop the buffer.
				if (ADSP_FAILED(result = elite_msg_return_payload_buffer(&(pCurrentInPort->myDataQMsg))))
				{
					MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to release i/p buffer back to upstr svc!\n");
				}

				pCurrentInPort->numBufReturned++;
				pCurrentInPort->bInBufStatus = INPUT_BUFFER_RELEASED;
				return ADSP_EFAILED;
			}
			else //(HOLD == rendering_decision)
			{
				MSG_4(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port %lu: Future TS, hold buf for [%lu %lu] usec",
						me->mtMxID, unInPortID, (uint32_t)(pCurrentInPort->ullInBufHoldDurationInUsec.int_part >> 32), (uint32_t)(pCurrentInPort->ullInBufHoldDurationInUsec.int_part));
				//If hold duration is less than the rounding < 1ms, treat it as immediate.
				//i/p port is in a held state.
				pCurrentInPort->inPortState = INPUT_PORT_STATE_ACTIVE_HOLDING_INPUT_BUFFER;
				pCurrentInPort->inPortStatePrToStateChange = INPUT_PORT_STATE_ACTIVE_HOLDING_INPUT_BUFFER;

				//Update Expected ST for the next buffer.
				uint64_t proposed_expected_session_clock = pInputBuf->ullTimeStamp + unDataRcvdInUsec;
				avsync_lib_set_internal_param(pCurrentInPort->pAVSyncLib,EXPECTED_SESSION_CLOCK,&proposed_expected_session_clock,sizeof(proposed_expected_session_clock));

				//Stop further processing from this port.
				MtMx_RemoveInputPortFromWaitMask(me, unInPortID);

				//Sync (partially) done.
				uint32_t avsync_rendering_decision_type =ASM_SESSION_MTMX_STRTR_PARAM_RENDER_DECISION_DEFAULT;
				avsync_lib_get_internal_param(pCurrentInPort->pAVSyncLib,RENDERING_DECISION_TYPE,&avsync_rendering_decision_type);
				if(ASM_SESSION_MTMX_STRTR_PARAM_RENDER_DECISION_DEFAULT == avsync_rendering_decision_type)
				{
					pCurrentInPort->bShouldSessionTimeBeDerivedFromNextTS =  FALSE;
				}
				else
				{
					pCurrentInPort->bShouldSessionTimeBeDerivedFromNextTS =  TRUE;
				}
				pCurrentInPort->bUseDefaultWindowForRendering = FALSE;
				pCurrentInPort->bShouldSessionTimeBeDerivedFromNextTSGapless =  FALSE;

				return ADSP_EFAILED;
			}
		}
		else
		{
			//Steady state operation (for default rendering decision mode)
			//For local STC based rendering, it will treat every buffer as the first buffer. Therefore, it will never hit this logic.
			avsync_lib_make_rendering_decision(pCurrentInPort->pAVSyncLib,
					pInputBuf->ullTimeStamp,
					(uint64_t)0,
					(bool_t)TRUE,
					&delta,
					&rendering_decision);

			if(HOLD == rendering_decision)
			{
				pCurrentInPort->ullInBufHoldDurationInUsec.int_part = (-delta);
				pCurrentInPort->ullInBufHoldDurationInUsec.frac_part = 0;

				//If hold duration is less than the 1ms, treat it as immediate.
				if(pCurrentInPort->ullInBufHoldDurationInUsec.int_part < MT_MX_FRAME_DURATION_1000US)
				{
					mt_mx_clear_time(&pCurrentInPort->ullInBufHoldDurationInUsec);
					delta = 0;
					rendering_decision = RENDER;
					MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port %lu: Hold duration was < 1ms, decision to render instead", me->mtMxID, unInPortID);
				}
			}

			//If the stat window ends are setup, call an update on the AVSync stat library, only in steady state operation
			if(FALSE == pCurrentInPort->bHasInputBufferUpdatedStats)
			{
				avsync_lib_update_stat(pCurrentInPort->pAVSyncLib, delta, unDataRcvdInUsec);
				pCurrentInPort->bHasInputBufferUpdatedStats = TRUE;
			}
			//If past by less than or equal to llRenderWindowEnd, process immediately
			if(RENDER == rendering_decision)
			{
				//Update [session time, absolute time] pair.
				avsync_lib_set_internal_param(pCurrentInPort->pAVSyncLib,SESSION_CLOCK,&pInputBuf->ullTimeStamp,sizeof(pInputBuf->ullTimeStamp));
				avsync_lib_increment_expected_session_clock(pCurrentInPort->pAVSyncLib,unDataRcvdInUsec);

				//No holding required.
				mt_mx_clear_time(&pCurrentInPort->ullInBufHoldDurationInUsec);

				//Sync done.
				pCurrentInPort->bShouldSessionTimeBeDerivedFromNextTS =  FALSE;
				pCurrentInPort->bUseDefaultWindowForRendering = FALSE;
				pCurrentInPort->bShouldSessionTimeBeDerivedFromNextTSGapless = FALSE;
				pCurrentInPort->bIsStrClkSyncdWithSessClk = TRUE;

				//Update the drift, keeping in mind update has to be called only once per buffer
				if(FALSE == pCurrentInPort->bHasInputBufferUpdatedDrift)
				{
					avsync_lib_update_s2d_drift(pCurrentInPort->pAVSyncLib, pInputBuf->ullTimeStamp, unDataRcvdInUsec);
					pCurrentInPort->bHasInputBufferUpdatedDrift = TRUE;
				}

				return ADSP_EOK;
			}
			else if(DROP == rendering_decision)
			{
				MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port %lu: Past TS, drop buf", me->mtMxID, unInPortID);

				//Update timestamps
				pCurrentInPort->ullTimeStampAtCopy.int_part = pInputBuf->ullTimeStamp;
				pCurrentInPort->ullTimeStampAtCopy.frac_part = 0;
				pCurrentInPort->ullTimeStampAtAccumulation.int_part = pInputBuf->ullTimeStamp;
				pCurrentInPort->ullTimeStampAtAccumulation.frac_part = 0;
				pCurrentInPort->ullTimeStampProcessed.int_part = pInputBuf->ullTimeStamp;
				pCurrentInPort->ullTimeStampProcessed.frac_part = 0;

				MSG_5(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu: i/p port %lu InputTS [%lu, %lu], TSValidity: %d", me->mtMxID, unInPortID,
						(uint32_t)(pInputBuf->ullTimeStamp>>32),(uint32_t)(pInputBuf->ullTimeStamp), pCurrentInPort->bIsTimeStampValid);

				//No holding required.
				mt_mx_clear_time(&pCurrentInPort->ullInBufHoldDurationInUsec);

				//Past TS, Drop the buffer.
				if (ADSP_FAILED(result = elite_msg_return_payload_buffer(&(pCurrentInPort->myDataQMsg))))
				{
					MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to release i/p buffer back to upstr svc!\n");
				}

				pCurrentInPort->numBufReturned++;
				pCurrentInPort->bInBufStatus = INPUT_BUFFER_RELEASED;

				//Commit the AVSync update changes for this i/p port for this dropped buffer immediately.
				ADSPResult commit_result = avsync_lib_commit_stat(pCurrentInPort->pAVSyncLib);
				if (ADSP_FAILED(commit_result) && ADSP_EBADPARAM != commit_result)
				{
					MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu: in i/p port %lu context, failed to commit AVSync stats with result=%d", me->mtMxID, unInPortID, commit_result);
				}

				return ADSP_EFAILED;
			}
			else// if HOLD ==  rendering decision
			{
				MSG_4(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu i/p port %lu: Future TS, hold buf for MSW: %lu LSW: %lu usec",
						me->mtMxID, unInPortID, (uint32_t)(pCurrentInPort->ullInBufHoldDurationInUsec.int_part >> 32), (uint32_t)(pCurrentInPort->ullInBufHoldDurationInUsec.int_part));
				//If hold duration is less than the 1ms, treat it as immediate.
				//Update expected ST.
				avsync_lib_increment_expected_session_clock(pCurrentInPort->pAVSyncLib,unDataRcvdInUsec);
				//i/p port is in a held state.
				pCurrentInPort->inPortState = INPUT_PORT_STATE_ACTIVE_HOLDING_INPUT_BUFFER;
				pCurrentInPort->inPortStatePrToStateChange = INPUT_PORT_STATE_ACTIVE_HOLDING_INPUT_BUFFER;

				//Stop further processing from this port.
				MtMx_RemoveInputPortFromWaitMask(me, unInPortID);

				//Out of sync.
				pCurrentInPort->bIsStrClkSyncdWithSessClk = FALSE;

				return ADSP_EFAILED;
			}
		}
	}

	return ADSP_EOK;
}

void MtMx_AddInterpolatedSamples(This_t *me, uint32 unInPortID)
{
	MatrixInPortInfoType *pCurrentInPort = me->inPortParams[unInPortID];

	// Pointer to current sample in upstream peer output buffer and write location in local buffer
	int8_t *pRdLoc = pCurrentInPort->pCurrentSample;
	int8_t *pWrLoc = pCurrentInPort->pWrLoc;

	uint32_t num_bytes_to_interpolate_per_ch = (MT_MX_NUM_SAMPLES_TO_ADD_OR_DROP_PER_BUFFER * pCurrentInPort->unBytesPerSample);
	uint32_t num_bytes_per_ch_in_port_buf = (pCurrentInPort->unInPortPerChBufSize * pCurrentInPort->unBytesPerSample);
	uint32_t in_buf_ch_spacing_in_bytes = (pCurrentInPort->nNumRemainingSamplesPerCh + pCurrentInPort->nNumSamplesUsedPerCh) \
			* pCurrentInPort->unBytesPerSample;

	for (uint16_t j = 0; j < pCurrentInPort->unNumChannels; j++)
	{
		memscpy((void*)pWrLoc, num_bytes_to_interpolate_per_ch,(void*)pRdLoc, num_bytes_to_interpolate_per_ch);
		pRdLoc += in_buf_ch_spacing_in_bytes;
		pWrLoc += num_bytes_per_ch_in_port_buf;
	}

	pCurrentInPort->bShouldSessionTimeBeAdjWhenSendingBufDown = TRUE;
	pCurrentInPort->pWrLoc += num_bytes_to_interpolate_per_ch;
}

void MtMx_DropSamples(This_t *me, uint32_t unInPortID)
{
	MatrixInPortInfoType *pCurrentInPort = me->inPortParams[unInPortID];

	pCurrentInPort->pCurrentSample += (MT_MX_NUM_SAMPLES_TO_ADD_OR_DROP_PER_BUFFER * pCurrentInPort->unBytesPerSample);
	pCurrentInPort->unNumSamplesPerChReqToFillLocalBuf -= MT_MX_NUM_SAMPLES_TO_ADD_OR_DROP_PER_BUFFER;
	pCurrentInPort->nNumRemainingSamplesPerCh -= MT_MX_NUM_SAMPLES_TO_ADD_OR_DROP_PER_BUFFER;
	pCurrentInPort->nNumSamplesUsedPerCh += MT_MX_NUM_SAMPLES_TO_ADD_OR_DROP_PER_BUFFER;
	pCurrentInPort->bShouldSessionTimeBeAdjWhenSendingBufDown = TRUE;
}

void MtMx_CheckIfInportMovesAnyOutportToActiveState(This_t *me, uint32_t unInPortID)
{
	MatrixInPortInfoType       *pCurrentInPort = me->inPortParams[unInPortID];
	MatrixOutPortInfoType      *pCurrentOutPort;
	uint32                     strMask = pCurrentInPort->strMask;
	uint32                     unOutPortID;
	while (strMask)
	{
		unOutPortID = Q6_R_ct0_R(strMask);
		strMask ^= (1 << unOutPortID);
		pCurrentOutPort = me->outPortParams[unOutPortID];

		if (OUTPUT_PORT_STATE_WAITING_TO_BE_ACTIVE == pCurrentOutPort->outPortState)
		{
			mt_mx_decrement_time(&pCurrentOutPort->ullOutBufHoldDurationInUsec, pCurrentOutPort->unFrameDurationInUsec);
			// accurate to the nearest 1us
			if (pCurrentOutPort->ullOutBufHoldDurationInUsec.int_part < pCurrentOutPort->unFrameDurationInUsec.int_part)
			{
				MtMx_RunOutputPort(me, unOutPortID);
				pCurrentOutPort->bIsFirstOutBufYetToBeSent = TRUE;
			}
		}
	}
}

void MtMx_InPortUpdateMediaFmtParams(This_t *me, uint32_t unInPortID, uint32_t unSampleRate,
		uint32_t nNumChannels, uint32_t unBytesPerSample)
{
	MatrixInPortInfoType *pCurrentInPort = me->inPortParams[unInPortID];

	if ((0 != pCurrentInPort->unSampleRate) && (unSampleRate != pCurrentInPort->unSampleRate))
	{
		// adjust the base value
		pCurrentInPort->ullNumSamplesForStcUpdate = (pCurrentInPort->ullNumSamplesForStcUpdate * unSampleRate) / pCurrentInPort->unSampleRate;
		MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu: i/p #%lu sample rate change, reset bHasFirstSampleAccumulated to FALSE",
				me->mtMxID, unInPortID);
		pCurrentInPort->bHasFirstSampleAccumulated = FALSE;
	}

	//Update number of channels, bps, sample rate and frame duration
	pCurrentInPort->unNumChannels = nNumChannels;
	pCurrentInPort->unBytesPerSample = unBytesPerSample;
	pCurrentInPort->unSampleRate = unSampleRate;

	MtMx_SetInPortFrameDuration(me, unInPortID);

	avsync_lib_check_set_default_rendering_window(pCurrentInPort->pAVSyncLib,MT_MX_FRAME_DURATION_5000US);
	MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Render window and Default window changed to 5ms");

	return;
}

void MtMx_HoldCommonRoutine(This_t *me, uint32_t unInPortID, MatrixInPortInfoType *pCurrentInPort)
{
	uint32_t unNumSampPerChZerosToPreFill = 0;
	uint32_t numSamplesReqPerCh;

	if (pCurrentInPort->ullInBufHoldDurationInUsec.int_part > 0)
	{
		//Calculate the number of samples/ch of zeros to pre-fill
		//Hold Dur (uS) / 1000 = Hold Dur in ms.
		//Hold Dur (ms) * #samp/ms/ch = #samp/ch
		unNumSampPerChZerosToPreFill = (pCurrentInPort->ullInBufHoldDurationInUsec.int_part * pCurrentInPort->unSamplesPer1Msec)/(1000);

		MSG_5(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu: i/p port %lu Hold Dur [%lu %lu], ZerosToFill %lu sam/ch",
				me->mtMxID, unInPortID, (uint32_t)(pCurrentInPort->ullInBufHoldDurationInUsec.int_part>>32),
				(uint32_t)(pCurrentInPort->ullInBufHoldDurationInUsec.int_part), unNumSampPerChZerosToPreFill);

		//Pre-fill with hold duration worth of zeros (This will guaranteed to be < 1 i/p frame size worth by the caller).
		if(unNumSampPerChZerosToPreFill <= pCurrentInPort->unNumSamplesPerChReqToFillLocalBuf)
		{
			//Most common use case. There is room in the existing internal buffer to accommodate all the hold zeros.
			mt_mx_clear_time(&pCurrentInPort->ullInBufHoldDurationInUsec);
			numSamplesReqPerCh = unNumSampPerChZerosToPreFill;
			MtMx_FillInPortLocalBufHold(me, unInPortID, numSamplesReqPerCh);
		}
		else
		{
			//Rare use case. The number of 'hold zeros' to fill is more than what the internal buffer can hold at this point.
			if(0 != pCurrentInPort->unSamplesPer1Msec)
			{
				pCurrentInPort->ullInBufHoldDurationInUsec.int_part -= ((pCurrentInPort->unNumSamplesPerChReqToFillLocalBuf * 1000) / (pCurrentInPort->unSamplesPer1Msec));
			}
			else
			{
				//This use case should never occur.
				// Assume 48kHz sampling rate which gives 48 samples per 1ms
				pCurrentInPort->ullInBufHoldDurationInUsec.int_part -= ((pCurrentInPort->unNumSamplesPerChReqToFillLocalBuf * 1000) / (48));
			}
			numSamplesReqPerCh = pCurrentInPort->unNumSamplesPerChReqToFillLocalBuf;
			MtMx_FillInPortLocalBufHold(me, unInPortID, numSamplesReqPerCh);
			goto __cmnroutineafterfill;
		}
	}

	//Now, fill the remainder of internal buffer as usual.
	if (pCurrentInPort->nNumRemainingSamplesPerCh >= (int)pCurrentInPort->unNumSamplesPerChReqToFillLocalBuf)
	{
		numSamplesReqPerCh = pCurrentInPort->unNumSamplesPerChReqToFillLocalBuf;
	}
	else
	{
		//i/p buffer contains lesser samples than the size of local buffer (short buffer)
		numSamplesReqPerCh = pCurrentInPort->nNumRemainingSamplesPerCh;
		pCurrentInPort->bIsInBufShort = TRUE;
	}

	//Copy samples from i/p buffer to local buffer and update nNumRemainingSamplesPerCh
	MtMx_FillInPortLocalBuf(me, unInPortID, numSamplesReqPerCh);

	__cmnroutineafterfill:
	if (FALSE == pCurrentInPort->bIsLocalBufFull && TRUE == pCurrentInPort->bIsInBufShort)
	{
		// The incoming buffer did not contain sufficient samples to fill local buffer
		// return the buffer and add the input port to the wait mask
		ADSPResult result;

		if (ADSP_FAILED(result = elite_msg_return_payload_buffer(&(pCurrentInPort->myDataQMsg))))
		{
			MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu i/p port %lu: failed to return i/p data buffer", me->mtMxID, unInPortID);
		}

		pCurrentInPort->bInBufStatus = INPUT_BUFFER_RELEASED;
		pCurrentInPort->numBufReturned++;
		MtMx_AddInputPortToWaitMask(me, unInPortID);

#ifdef MT_MX_EXTRA_DEBUG
		MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx#%lu i/p port %lu, #Buf Released: %lu, i/p port added to wait mask",
				me->mtMxID, unInPortID, pCurrentInPort->numBufReturned);
#endif

	}
}
