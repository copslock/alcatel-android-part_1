/**
@file MixerSvc_MsgHandlers.h
@brief This file declares functions that the audio matrix mixer
       uses to handle various control commands it receives.

*/

/*========================================================================
Copyright (c) 2009 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government, Diversion contrary to U.S. law prohibited.
*//*====================================================================== */

/*========================================================================
Edit History

$Header: //components/rel/avs.adsp/2.7.1.c4/aud/services/static_svcs/matrix_mixer_svc/src/MixerSvc_MsgHandlers.h#1 $

when       who     what, where, why
--------   ---     -------------------------------------------------------
06/04/2010 AAA     Created file.
========================================================================== */

#ifndef MIXER_SVC_MSG_HANDLERS_H
#define MIXER_SVC_MSG_HANDLERS_H


/*-------------------------------------------------------------------------
Include Files
-------------------------------------------------------------------------*/

#include "qurt_elite.h"
#include "Elite.h"
#include "MixerSvc.h"

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

/*---------------------------------------------------------------------------
Function Declarations and Documentation
----------------------------------------------------------------------------*/

/**
This function is the handler for the
ELITEMSG_CUSTOM_CFG_INPUT_PORTS cmd. It configures input ports on
the matrix mixer.

@param me [in] A pointer to instance struct of the matrix
@param pMsg [in] The message that needs handling
@return An indication of success or failure
*/
ADSPResult MtMx_CfgInputPorts(This_t *me, elite_msg_any_t* pMsg);

/**
This function is the handler for the
ELITEMSG_CUSTOM_CFG_OUTPUT_PORTS cmd. It configures output ports
on the matrix mixer.

@param me [in] A pointer to instance struct of the matrix
@param pMsg [in] The message that needs handling
@return An indication of success or failure
*/
ADSPResult MtMx_CfgOutputPorts(This_t *me, elite_msg_any_t* pMsg);

/**
This function is the handler for the ELITEMSG_CUSTOM_MT_MX_PAUSE cmd,
which is a per-stream (or per-input-port) cmd. It changes the
state of that input port to paused and then acks back.

@param me [in] A pointer to instance struct of the matrix
@param pMsg [in] The message that needs handling
@return An indication of success or failure
*/
ADSPResult MtMx_Pause(This_t *me, elite_msg_any_t* pMsg);

/**
This function is the handler for the ELITEMSG_CUSTOM_RESUME cmd,
which is a per-stream (or per-input-port) cmd. It changes the
state of that input port to active and then acks back.

@param me [in] A pointer to instance struct of the matrix
@param pMsg [in] The message that needs handling
@return An indication of success or failure
*/
ADSPResult MtMx_Run(This_t *me, elite_msg_any_t* pMsg);

/**
This function is the handler for the ELITEMSG_CUSTOM_MT_MX_FLUSH cmd,
which is a per-stream (or per-input-port) cmd. It releses the
buffer that the input port may be holding on to, releases any
queued-up buffers in the data queue and then acks back.

@param me [in] A pointer to instance struct of the matrix
@param pMsg [in] The message that needs handling
@return An indication of success or failure
*/
ADSPResult MtMx_Flush(This_t *me, elite_msg_any_t* pMsg);

/**
This function is the handler for the ELITEMSG_CUSTOM_MT_MX_SUSPEND cmd,
which is a per-stream (or per-input-port) cmd. 

@param me [in] A pointer to instance struct of the matrix
@param pMsg [in] The message that needs handling
@return An indication of success or failure
*/
ADSPResult MtMx_Suspend(This_t *me, elite_msg_any_t* pMsg);

/**
This function destroys the instance of the matrix mixer.

@param pInstance [in] A pointer to instance struct of the
                 matrix
@param pMsg [in] The message that needs handling
@return An indication of success or failure
*/
ADSPResult MtMx_DestroyYourself(void* pInstance, elite_msg_any_t* pMsg);

/**
This function maps the input ports of the matrix mixer to output
ports.

@param me [in] A pointer to instance struct of the matrix
@param pPkt [in] The message that contains the mapping
@return An indication of success or failure
*/
ADSPResult MtMx_MapRoutings(This_t *me, elite_msg_any_t* pMsg);


/**
This function handles the Media Type msg received in the data
queue of an input port.

@param me [in] A pointer to instance struct of the matrix
@param unInPortID [in] ID of the input port that received the
                  media type msg.
@param myDataQMsg [in] contents of the dataQ
@return An indication of success or failure
*/
ADSPResult MtMx_MsgMediaType(This_t *me, uint32_t unInPortID, elite_msg_any_t myDataQMsg);

/**
This function handles the End Of Stream msg received in the data
queue of an input port.

@param me [in] A pointer to instance struct of the matrix
@param unInPortID [in] ID of the input port that received EOS
@param myDataQMsg [in] contents of the dataQ
@return none
*/
void MtMx_MsgEos(This_t *me, uint32_t unInPortID, elite_msg_any_t myDataQMsg);

/**
This function releases a mark buffer msg and raises a mark buffer
event depending on the state

@param me [in] A pointer to instance struct of the matrix
@param unInPortID [in] ID of the input port that received EOS
@param myDataQMsg [in] contents of the dataQ
@return none
*/
void MtMx_ProcessMarkBuffer(This_t *me, uint32_t unInPortID, elite_msg_any_t myDataQMsg, uint16_t status);

/**
This function is the handler for the
ELITEMSG_CUSTOM_MT_MX_REGISTER_UNDERFLOW_OVERFLOW_EVENT cmd.

@param me [in] A pointer to instance struct of the matrix
@param pMsg [in] The message that needs handling
@return An indication of success or failure
*/
ADSPResult MtMx_RegisterUnderflowOverflowEvent(This_t *me, elite_msg_any_t *pMsg);

/**
This function is the handler for the
ELITEMSG_CUSTOM_MT_MX_RAMP_GAINS cmd.

@param me [in] A pointer to instance struct of the matrix
@param pMsg [in] The message that needs handling
@return An indication of success or failure
*/
ADSPResult MtMx_RampGains(This_t *me, elite_msg_any_t *pMsg);

/**
This function is the handler for the
ELITEMSG_CUSTOM_MT_MX_ADJUST_SESSION_CLK cmd.

@param me [in] A pointer to instance struct of the matrix
@param pMsg [in] The message that needs handling
@return An indication of success or failure
*/
ADSPResult MtMx_AdjustSessionClk(This_t *me, elite_msg_any_t *pMsg);

/**
This function is the handler for the
ELITEMSG_CUSTOM_MT_MX_MUTE cmd.

@param me [in] A pointer to instance struct of the matrix
@param pMsg [in] The message that needs handling
@return An indication of success or failure
*/
ADSPResult MtMx_Mute(This_t *me, elite_msg_any_t *pMsg);

/**
This function is the handler for the ELITEMSG_CUSTOM_MT_MX_PORT_STATE_CHANGE cmd,
which is a per-stream (or per-input-port) cmd. It releses the
buffer that the input port may be holding on to, releases any
queued-up buffers in the data queue, changes the port state 
and then acks back.

@param me [in] A pointer to instance struct of the matrix
@param pMsg [in] The message that needs handling
@return An indication of success or failure
*/
ADSPResult MtMx_PortStateChange(This_t *me, elite_msg_any_t* pMsg);

/**
This function is the handler for the
ELITEMSG_CUSTOM_MT_MX_SET_PARAM cmd.

@param me [in] A pointer to instance struct of the matrix
@param pMsg [in] The message that needs handling
@return An indication of success or failure
*/
ADSPResult MtMx_SetParamHandler(This_t *me, elite_msg_any_t *pMsg);

/**
This function is the handler for the
ELITEMSG_CUSTOM_MT_MX_GET_PARAM cmd.

@param me [in] A pointer to instance struct of the matrix
@param pMsg [in] The message that needs handling
@return An indication of success or failure
*/
ADSPResult MtMx_GetParamHandler(This_t *me, elite_msg_any_t *pMsg);

/**
This function handles the set param msg on the data path.

@param me [in] A pointer to instance struct of the matrix
@param unInPortID [in] ID of the input port that received this message
@param myDataQMsg [in] contents of the dataQ
@return none
*/
void MtMx_MsgDataSetParam(This_t *me, uint32_t unInPortID, elite_msg_any_t myDataQMsg);
void MtMx_RunOutputPort(This_t *me, uint32_t unPort2Run);

/**
This function is the handler for the
ELITEMSG_CUSTOM_MT_MX_SET_PRIMARY_PORT cmd.

@param me [in] A pointer to instance struct of the matrix
@param pMsg [in] The message that needs handling
@return An indication of success or failure
*/
ADSPResult MtMx_SetPrimaryOutputPort(This_t *me, elite_msg_any_t *pMsg);

/**
This function is the handler for the
ELITEMSG_CUSTOM_MT_MX_PSPD_SET_PARAM cmd.

@param me [in] A pointer to instance struct of the matrix
@param pMsg [in] The message that needs handling
@return An indication of success or failure
*/
ADSPResult MtMx_SetPsPdParamHandler(This_t *me, elite_msg_any_t *pMsg);

/**
This function is the handler for the
ELITEMSG_CUSTOM_MT_MX_SET_RATEMATCHING_PARAM cmd.

@param me [in] A pointer to instance struct of the matrix
@param pMsg [in] The message that needs handling
@return An indication of success or failure
*/
ADSPResult MtMx_SetRateMatchingParamHandler(This_t *me, elite_msg_any_t *pMsg);

#ifdef __cplusplus
}
#endif // __cplusplus
#endif //MIXER_SVC_MSG_HANDLERS_H
