/**
@file MixerSvc.h
@brief This file declares some common data types used by Audio
       Matrix Mixer.
*/

/*========================================================================
Copyright (c) 2009 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government, Diversion contrary to U.S. law prohibited.
*//*====================================================================== */

/*========================================================================
Edit History

$Header: //components/rel/avs.adsp/2.7.1.c4/aud/services/static_svcs/matrix_mixer_svc/src/MixerSvc.h#1 $

when       who     what, where, why
--------   ---     -------------------------------------------------------
12/01/2010 AAA      Created file.
==========================================================================*/

#ifndef MATRIX_MIXER_H
#define MATRIX_MIXER_H

#include "EliteLoggingUtils_i.h"

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

#include "adsp_adm_api.h"
#include "MixerSvc_Interface.h"
#include "appi_channelmixer.h"
#include "avtimer_drv_api.h"
#include "appi_dts_downmix.h"
#include "stream_dev_drift_calc.h"
#include "AFEInterface.h"
#include "qurt_elite_adsppm_wrapper.h"

//#define MT_MX_EXTRA_DEBUG
#define NUM_CHANNELS_MONO                                         1
#define NUM_CHANNELS_STEREO                                       2
#define MT_MX_NUM_CHANNELS_ONE                                    1
#define MT_MX_NUM_CHANNELS_TWO                                    2
#define MT_MX_NUM_CHANNELS_THREE                                  3
#define MT_MX_NUM_CHANNELS_FOUR                                   4
#define MT_MX_NUM_CHANNELS_FIVE                                   5
#define MT_MX_NUM_CHANNELS_SIX                                    6
#define MT_MX_NUM_CHANNELS_SEVEN                                  7
#define MT_MX_NUM_CHANNELS_EIGHT                                  8
#define MAX_CHANNEL_MAPPING_NUMBER                               34

#define MT_MX_DEFAULT_BYTES_PER_SAMPLE                            2
#define MT_MX_BYTES_PER_SAMPLE_TWO											2
#define MT_MX_BYTES_PER_SAMPLE_FOUR											4

/*****************************************************************/
/* IMPORTANT: New input port states must be added such that any  */
/* state before input port starts processing PCM buffers should  */
/* have a value less than INPUT_PORT_STATE_ACTIVE.               */
/*****************************************************************/
#define INPUT_PORT_STATE_INACTIVE                                 0 /* port not in use */
#define INPUT_PORT_STATE_INIT                                     1 /* port just created, hasn't received any cmds/msgs, can process only cmds, cannot process dataQ in this state */
#define INPUT_PORT_STATE_WAITING_TO_BE_READY                      2 /* after processing first RUN cmd (right after INIT) but waiting to be able to process dataQ (due to RUN cmd specifying a future time or a positive relative offset) */
#define INPUT_PORT_STATE_READY                                    3 /* after processing first RUN cmd (right after INIT) and now able to process dataQ, can process only MEDIA_TYPE msg on dataQ*/
#define INPUT_PORT_STATE_WAITING_TO_BE_ACTIVE                     4 /* after processing steady-state RUN cmd but waiting to be able tp process dataQ (due to RUN cmd specifying a future time or a positive relative offset) */
/* input port can process PCM data buffers in the below states. */
#define INPUT_PORT_STATE_ACTIVE                                   8 /* after processing first MEDIA_TYPE msg, can now process PCM buffers on dataQ */
#define INPUT_PORT_STATE_ACTIVE_HOLDING_INPUT_BUFFER              9
#define INPUT_PORT_STATE_PAUSED                                   10 /* port in pause state */

#define OUTPUT_PORT_STATE_INACTIVE                                0
#define OUTPUT_PORT_STATE_ACTIVE                                  1
#define OUTPUT_PORT_STATE_INIT                                    2
#define OUTPUT_PORT_STATE_WAITING_TO_BE_ACTIVE                    3
#define OUTPUT_PORT_STATE_PAUSED                                  4
#define OUTPUT_PORT_STATE_DISABLED                                5

#define INPUT_BUFFER_RELEASED                                     0
#define INPUT_BUFFER_HELD                                         1

#define MT_MX_MAX_PORTS                                           25
#define MT_MX_MAX_INPUT_PORTS                                     16
#define MT_MXAR_MAX_INPUT_PORTS                                   16
#define MT_MXAT_MAX_INPUT_PORTS                                   9
#define MT_MXAR_MAX_OUTPUT_PORTS                                  9
#define MT_MXAT_MAX_OUTPUT_PORTS                                  9

#define MT_MX_MAX_OUTPUT_PORTS                                    (MT_MX_MAX_PORTS - MT_MX_MAX_INPUT_PORTS)

//This will be used to identify that a stream session is ULL and will be used in the map routing command.
#define MT_MX_ULL_SESSION                                         (MT_MX_MAX_PORTS + 1)
#define MT_MX_LLNP_SESSION                                        ((MT_MX_MAX_PORTS*2) + 1)
#define MT_MX_ULLPP_SESSION                                       ((MT_MX_MAX_PORTS*3) + 1)

#define MT_MX_INPUT_PORT_UNDERFLOW_REPORT_STATUS_DO_NOT_REPORT    0
#define MT_MX_INPUT_PORT_UNDERFLOW_REPORT_STATUS_REPORT           1

#define MT_MX_OUTPUT_PORT_OVERFLOW_REPORT_STATUS_DO_NOT_REPORT    0
#define MT_MX_OUTPUT_PORT_OVERFLOW_REPORT_STATUS_REPORT    1

#define MT_MX_INPUT_PORT_STRMASK_CLEARED                          0
#define MT_MX_INPUT_PORT_STRMASK_BIT_REMOVED                      1
#define MT_MX_INPUT_PORT_STRMASK_BIT_ADDED                        2

#define MT_MX_OUTPUT_PORT_INPORTSMASK_CLEARED                     0
#define MT_MX_OUTPUT_PORT_INPORTSMASK_BIT_REMOVED                 1
#define MT_MX_OUTPUT_PORT_INPORTSMASK_BIT_ADDED                   2

#define MT_MX_NUM_SAMPLES_TO_ADD_OR_DROP_PER_BUFFER               1
#define MT_MX_INPUT_PORT_NO_ADJUSTMENT_OF_SAMPLES                 0
#define MT_MX_INPUT_PORT_ADD_SAMPLES                              1
#define MT_MX_INPUT_PORT_DROP_SAMPLES                             2

#define MT_MX_SAMPLING_RATE_48000                                 48000

#define MT_MX_FRAME_DURATION_1000US                               1000
#define MT_MX_FRAME_DURATION_5000US                               5000
#define MT_MX_FRAME_DURATION_10000US                             10000

#define MT_MX_LEGACY_NUM_AFE_FRAMES                5
#define MT_MX_LL_NUM_AFE_FRAMES                    1

#define MAX_DATA_Q_ELEMENTS           16
#define MAX_INPUT_CMD_Q_ELEMENTS  8
#define MAX_INDEX_CHANNEL_MIXER_SET_PARAM            8
#define UNITY_Q14                                                             16384


//Following are the KPPS (= MPPS / 1000) requirement of MTMX

// MTMX requirement for 48kHz, stereo, 16-bit cases for both legacy and low latency cases.
#define MTMX_KPPS_48K_STEREO_16BIT_LEG_LAT (2000)
#define MTMX_KPPS_48K_STEREO_16BIT_LOW_LAT (3000)
#define MTMX_KPPS_48K_STEREO_24BIT_LEG_LAT (3000)
#define MTMX_KPPS_48K_STEREO_24BIT_LOW_LAT (4000)

// MTMX requirement for 192kHz, stereo, 16-bit cases for both legacy and low latency cases.
#define MTMX_KPPS_192K_STEREO_16BIT_LEG_LAT (4000)
#define MTMX_KPPS_192K_STEREO_16BIT_LOW_LAT (5000)
#define MTMX_KPPS_192K_STEREO_24BIT_LEG_LAT (5000)
#define MTMX_KPPS_192K_STEREO_24BIT_LOW_LAT (6000)

//Number of elements in the PSPD Q's
#define MTMX_NUM_PSPD_Q_ELEM        (1)

enum stc_value_validity
{
   STC_VALUE_DEFAULT = 0, //Default state
   STC_VALUE_INVALID, //Invalid state
   STC_VALUE_VALID, //This state will be set once the STC value has been logged (in real time)
};

typedef enum eChMixerType
{
	PSPD_CHMIXER_NONE = 0,
    PSPD_CHMIXER_QCOM,
    PSPD_CHMIXER_DTS
}eChMixerType;

//Utility macros
#define MTMX_FREE(ptr) \
do { \
qurt_elite_memory_free(ptr); \
ptr = NULL; \
} while(0)

#define MTMX_ALIGNED_FREE(ptr) \
do { \
qurt_elite_memory_aligned_free(ptr); \
ptr = NULL; \
} while(0)

#define MT_MX_SAMPLES_TO_USEC(num_of_samples, samples_per_second) \
   (((samples_per_second) == 0) ? (0) : (((uint64_t)(num_of_samples) * 1000000) / (samples_per_second)))

#define MTMX_TIME_Q_FACTOR 45
#define MTMX_TIME_FRAC_MASK (~(((uint64_t)(-1)) << MTMX_TIME_Q_FACTOR))

/* Notes on the following adsp_time structure and time/clock mgmt in the Mixer
 *
 * Error accumulation corrections are performed in three ways:
 * 1) Slow error, highly optimized code - uses the adsp_time structure to correct error.
 *
 *    This is used by all the internal state values that deal with time/timestamps in the mixer in/out
 *    port state structures.
 *
 * 2) No error, non-optimized code - uses aggregate number of samples processed to calculate timestamp
 *    and clock values. Must perform conversion between samples <-> time which requires 64-bit division.
 *
 *    This is used by the session clock updates from the output port context and render decisions in the
 *    input port context.
 *
 * 3) No correction - some code is not corrected for error accumulation due to trunctaion
 *
 *    This only applies to the expected session clock updates during input port underflow events.
 *
 *    TODO: move the adsp_time struct into a separate header/implementation file and update the
 *    avsync lib to use this construct. This will resolve the STC/Expected time clock workarounds
 *    in the mixer service layer and clean up the code.
 */
typedef struct adsp_time
{
   int64_t int_part;
   int64_t frac_part;   // Q19.45
} adsp_time;

static inline void mt_mx_copy_time_value(adsp_time *to, const adsp_time from)
{
   if (NULL == to) return;
   to->int_part = from.int_part;
   to->frac_part = from.frac_part;
   return;
}

static inline void mt_mx_clear_time(adsp_time *a)
{
   if (NULL == a) return;
   a->int_part = a->frac_part = 0;
   return;
}

static inline void mt_mx_overflow_time_value(adsp_time *a)
{
   if (NULL == a) return;
   // overflow any digits left of the radix point into the integer part of the time value
   a->int_part += (a->frac_part >> MTMX_TIME_Q_FACTOR);
   // efficiently clear values left of radix point preserving the sign.
   // Note: 1 | ( x >> 63) evaluates to 1 if x > 0 and -1 otherwise.
   a->frac_part = (1 | (a->frac_part >> 63)) * (a->frac_part & MTMX_TIME_FRAC_MASK);
   return;
}

static inline void mt_mx_decrement_time(adsp_time *a, adsp_time b)
{
   mt_mx_overflow_time_value(a);
   mt_mx_overflow_time_value(&b);

   // compute a -= b
   a->int_part -= b.int_part;
   a->frac_part -= b.frac_part;

   mt_mx_overflow_time_value(a);
   return;
}

static inline void mt_mx_increment_time(adsp_time *a, adsp_time b)
{
   mt_mx_overflow_time_value(a);
   mt_mx_overflow_time_value(&b);

   // compute a += b
   a->int_part += b.int_part;
   a->frac_part += b.frac_part;

   mt_mx_overflow_time_value(a);
   return;
}

typedef struct mt_mx_struct_ramp_gain_t  mt_mx_struct_ramp_gain_t;
struct mt_mx_struct_ramp_gain_t
{
   uint32_t                   unRampSizeInSamples;
   uint16_t                   unStepSizeInSamples;
   uint16_t                   unRampCurve;
   uint32_t                   unNumRemSteps;
   uint16_t                   unTargetGain[MT_MX_NUM_CHANNELS_EIGHT];
   int16_t                     nGainIncDecStep[MT_MX_NUM_CHANNELS_EIGHT];
   uint16_t                   unCurrentGain[MT_MX_NUM_CHANNELS_EIGHT];
   bool_t                      bShouldRampGainBeApplied;
   bool_t                      bIsRunningInSlowRampModeSR[MT_MX_NUM_CHANNELS_EIGHT];
   uint16_t                   unNumSamplesWaitToIncDecSR[MT_MX_NUM_CHANNELS_EIGHT];
   uint16_t                   unCurrentSamplesWaitCounterSR[MT_MX_NUM_CHANNELS_EIGHT];
   bool_t                      bIsChannelMuted[MT_MX_NUM_CHANNELS_EIGHT];
   uint16_t                   unCachedTargetGainAfterUnMute[MT_MX_NUM_CHANNELS_EIGHT];
};

typedef struct mt_mx_struct_channel_mixer_t  mt_mx_struct_channel_mixer_t;
struct mt_mx_struct_channel_mixer_t
{
   bool_t                                    bIsDTSMixerLibCreated;
   bool_t                                    bIsQcomChannelMixerLibCreated;
   bool_t                                    bCanChMixerBeOptimizedOut;
   uint32_t                                 unCachedPsPdSetParamID;
   void                                      *punCachedPsPdSetParam;
};

typedef struct mt_mx_struct_latency_module_t  mt_mx_struct_latency_module_t;
struct mt_mx_struct_latency_module_t
{
   bool_t                   bIsLatencyModuleCreated;
   uint32_t                 unLatency;
};

typedef struct mt_mx_struct_pspd_t mt_mx_struct_pspd_t;
struct mt_mx_struct_pspd_t
{
   elite_svc_handle_t             thread_param;
   int8_t                         *pPspdOutBuf;
   uint32_t                       unPspdOutBufSize;
   volatile uint32_t              unPspdKpps; /* Var used by the PSPD to store req KPPS */   
   volatile uint32_t              unPspdBw; /* Var used by the PSPD to store req BW in Bps */
   elite_msg_data_buffer_v2_t     inp_buf;
   elite_msg_data_buffer_v2_t     out_buf;
   QURT_ELITE_ALIGN(char cmd_resp_q[QURT_ELITE_QUEUE_GET_REQUIRED_BYTES(MTMX_NUM_PSPD_Q_ELEM)], 8);
   QURT_ELITE_ALIGN(char inp_data_q[QURT_ELITE_QUEUE_GET_REQUIRED_BYTES(MTMX_NUM_PSPD_Q_ELEM)], 8);
   QURT_ELITE_ALIGN(char out_data_q[QURT_ELITE_QUEUE_GET_REQUIRED_BYTES(MTMX_NUM_PSPD_Q_ELEM)], 8);
};

typedef struct mt_mx_sampleslip_t mt_mx_sampleslip_t;

struct mt_mx_sampleslip_t
{
   appi_t                     *pSampleSlipAppi;
   appi_buf_t               inBufs[MT_MX_NUM_CHANNELS_EIGHT];
   appi_buf_t               outBufs[MT_MX_NUM_CHANNELS_EIGHT];
};

typedef struct MatrixInPortInfoType MatrixInPortInfoType;
struct MatrixInPortInfoType
{
   //General properties
   MtMxInPortHandle_t       inPortHandle;                                            //Handle to the input port
   char                      dataQName[QURT_ELITE_DEFAULT_NAME_LEN];  //Data queue name
   elite_msg_any_t     myDataQMsg;
   elite_msg_data_buffer_t* pInputBuf;                                   //Input PCM buffer
   int32_t                   inPortState;                                          //Holds the current state of the i/p
   int32_t                   inPortStatePrToStateChange;               //Holds the state of the i/p port  prior to a port state change.
   bool_t                    bIsPortLive;                          //Indicates whether the input port is live (TRUE) or non-live (FALSE)
   bool_t                    bIsInPortBurstMode;            //Indicates whether the input port is in burst mode (TRUE) or non-burst mode (FALSE)
   Elite_CbType          eliteCb;                                //Callback structure provided by static svc
   uint16_t                 unPortPerfMode;    //Performance mode in which the input port is operating. Supported values are
                                                              //ADM_LEGACY_DEVICE_SESSION & ADM_LOW_LATENCY_DEVICE_SESSION
                                                              //For ADM_LOW_LATENCY_DEVICE_SESSION, 1msec local buffer is allocated
                                                              //For ADM_LEGACY_DEVICE_SESSION, 5msec local buffer is allocated

   //Masks
   uint32_t                 strMask;                              //Mask that shows the outputs that the current input feeds
   uint32_t                 activeOutPortsMask;           //Mask that indicates which output ports (out of all that this input is feeding to) are in active state
   uint32_t                 outputReqPendingMask;     //Mask that shows the outputs currently waiting for samples from this input.
   uint32_t                 eosPendingMask;                //Mask that indicates which output ports (out of all that this input is feeding to) have an EOS pending

   //Local buffer and filling logic
   uint32_t                 unNumSamplesPerChReqToFillLocalBuf; //Number of samples (per channel) required to fill local buffer
   int8_t                     *pCurrentSample;               //Points to the current sample being used from the upstream peer svc's output buffer
   int                         nNumRemainingSamplesPerCh;  //Number of samples remaining in the upstream peer svc's output buffer
   int                         nNumSamplesUsedPerCh;          //Number of samples used up from the upstream peer svc's output buffer
   uint32_t                 unTopPrioOutPort;         //ID of the highest priority o/p port that this i/p port is feeding to
   int8_t*                   pWrLoc;                          //Write pointer of local buffer
   int8_t*                   pStartLoc;                      //Start pointer
   uint32_t                 numBufRcvd;                 //Counter keeping track of # of i/p bufs rcvd by this i/p port so far
   uint32_t                 numBufReturned;          //Counter keeping track of # of i/p bufs returned by this i/p port so far
   bool_t                    bInBufStatus;                 //Flag to indicate of a request for new input buffer has been sent
   bool_t                    bIsLocalBufFull;
   bool_t                    bIsInBufShort;
   bool_t                    bIsLocalBufEmpty;
   bool_t                    bIsAccumulationPending;
   bool_t                    bIsNonLiveInputPortWaitingToAcc;
   bool_t                    bHasFirstSampleAccumulated;

   //EOS
   bool_t                    bIsLastMsgEOS;
   bool_t                    bIsEOSHeld;
   elite_msg_any_t     gaplessEOSmsg;  // Stores the Gapless EOS message temporarily until all the connected o/p ports sent it downstream.
   bool_t                    bIsGaplessEOSMsgWaitingToBeSentDS; //A flag to indicate whether an i/p port wants to send Gapless EOS downstream.
   uint64_t                 ullStaleEosSamples;

   //Media type
   uint32_t                 unAfeFrameSizeInSamples; //Hold the unit frame size in samples as defined by AFE
   adsp_time                unFrameDurationInUsec;      // Frame duration in microseconds
   uint32_t                 unSamplesPer1Msec;          // Samples per 1 milli second
   uint32_t                 unSampleRate;             // Sample Rate
   uint32_t                 unNewSampleRate;                   //New sample Rate: Used when media fmt is rcvd in steady-state and i/p port's local buf cannot be init'ed immediately
   uint32_t                 unTargetSampleRate;                //Target sample rate to expect. This is set in case there is a POPP sampling rate change initiated that will
                                                                                  //eventually change the sampling rate of the i/p port
   elite_msg_any_t     msgsToReturn[MAX_DATA_Q_ELEMENTS];//Temporarily, store the messages to return to the PP later on. This will be used when there is a port
                                                                                                //state change on the i/p port of RX matrix. Buffers will be returned to upstream PP service once the
                                                                                                //new media type has been received. This is done to avoid a drainage of the pipeline in the case of FTRT.
   uint16_t                 unNumMsgsToReturn;//Keeps track of how many messages to return.                                                                                                                 
   uint32_t                 unNumChannels;              //Number of channels
   uint32_t                 unNewNumChannels;       //New number of channels: Used when media fmt is rcvd in steady-state and i/p port's local buf cannot be init'ed immediately
   uint8_t                   unChannelMapping[MT_MX_NUM_CHANNELS_EIGHT];        //Channel mapping
   uint8_t                   unNewChannelMapping[MT_MX_NUM_CHANNELS_EIGHT]; //New channel mapping: Used when media fmt is rcvd in steady-state and
                                                                                                                          //i/p port's local buf cannot be init'ed immediately
   uint32_t                 unChMapToChRevLUMask[MAX_CHANNEL_MAPPING_NUMBER];
                                                   // An array of masks which indicates the following
                                                   // unChMapToChRevLUMask[0] = Channel Map Value "1" --> Which channel?
                                                   // unChMapToChRevLUMask[1] = Channel Map Value "2" --> Which channel?
                                                   //                          ...
                                                   // unChMapToChRevLUMask[7] = Channel Map Value "8" --> Which channel?
                                                   //                          ...
                                                   // unChMapToChRevLUMask[11]= Channel Map Value "12"--> Which channel?
   int                        nInterleaveFlag;              //Interleaved/non_interleaved data
   int                        unBitwidth;                     //16/24 bit
   uint16_t                unBytesPerSample;         //# of bytes per sample
   uint16_t                unNewBytesPerSample;  //New # of bytes per sample: Used when media fmt is rcvd in steady-state and i/p port's local buf cannot be init'ed immediately
   uint32_t                unInPortPerChBufSize;        //Input port local buffer per channel size
   uint32_t                unInPortBufSize;                 //Input port local buffer size
   bool_t                  bShouldLocalBufBeReInit;  //Boolean indicating whether this input port needs to be reinit
   uint32_t                unNumAfeFrames;           //Number of AFE frames in matrix frame
   uint32_t                unNewNumAfeFrames;        //New number of AFE frames in matrix frame
   bool_t                   bIsBufferReinitPending;          //Indicates if buffer reinitialization is pending in this port
   bool_t                  bDoesInputPortNeedToTransitionAwayFromReady; //Does this i/p port need to transition away from ready?

   //DTS
   bool_t                   bIsDTSStream;                        //Indicates the DTS stream attached to the port
   void                     *punSetParamPayloadCache;  //Pointer to store the setparam coefficients
   int32                     nParamID;                               //DTS coefficients parameter ID

   //AV-Sync
   bool_t                    bInputPortJustMovedFromHeldToActive;
   adsp_time                 ullInBufHoldDurationInUsec;     //Hold duration for current input buffer in usec
   uint32_t                 unStartFlag;                                                       //Start flag provided in the Run command
   uint8_t                   unUnderflowReportStatus;  //Whether to report underflow or wait for valid data
   bool_t                   bIsRegisteredForUnderflowEvents;
   uint8_t                   samplesAddOrDropMask;    //Mask that indicates whether samples should be added or dropped in order to achieve required session clock adjustment
   uint32_t                 unNumRemSamplesAdj;      //Number of samples that still need to be added or dropped to achieve required session clock adjustment
   bool_t                   bIsTimeStampValid;
   bool_t                   bForceCheckTimeStampValidity;
   bool_t                   bIsStrClkSyncdWithSessClk;
   bool_t                   bShouldSessionTimeBeDerivedFromNextTS;
   bool_t                   bShouldSessionTimeBeDerivedFromNextTSGapless;
   bool_t                   bShouldSessionTimeBeAdjWhenSendingBufDown;
   bool_t                   bIsSampleAddDropEnabled;
   bool_t                   bIsThisFirstSampleAddOrDrop;
      bool_t                   bIsSessionUnderEOS;
   uint64_t                 ullIncomingBufferTS;   //Incoming buffer TS in usec

      bool_t                  bHasInputBufferUpdatedStats;//Flag to indicate whether the i/p buffer has updated the stats and drift atleast once.
   uint64_t                 ullNumSamplesForStcUpdate;      // Keeps track of the total number of samples processed, used to calculate and set the Session Time Clock
   adsp_time		    ullTimeStampForStcBaseInUsec; //Keeps track of the first buffer TS received after run command 
   uint32_t                 unDataRcvdInSamples;
   adsp_time                 unDataRcvdInUsec;
	  
   //AV-Sync TV-Player requirements (GetParam)
   adsp_time             ullTimeStampAtCopy;          //Timestamp in microseconds of the current input buffer that is being copied (offset from the beginning).
   adsp_time             ullTimeStampAtAccumulation;  //Timestamp in microseconds of the current input buffer that is being accumulated (offset from the beginning).
   adsp_time             ullTimeStampProcessed;       //Timestamp in microseconds of the buffer that was last rendered.
   
   //Advanced AV-Sync
   bool_t                  bHasInputBufferUpdatedDrift;//Flag to indicate whether the i/p buffer has updated its drift atleast once.
   volatile const afe_drift_info_t *pAfeDriftPtr; //AFE drift pointer. Applicable to TX matrix only.
   bool_t                  bUseDefaultWindowForRendering; //The first buffer after a Run command will use default window for rendering
   stc_value_validity eSTCValueValidity; //An enum to indicate whether the current STC tick is valid or not.
   mt_mx_sampleslip_t      structSampleSlip; // structure for sampleslip module needed for stream-to-device ratematching
   bool_t                  bShouldActiveInputPortBeReProcessed; //When sample slip library is enabled, the i/p port processing may need to be delayed, esp. in low latency
      //AV-Sync Lib Structure
      void                    *pAVSyncLib;//Pointer to AvSync Lib, it stores the most common AVSync variables across mtmx and strtr

   //PSPD (Per-Stream, Per-Device)
   mt_mx_struct_pspd_t                structPspd[MT_MX_MAX_OUTPUT_PORTS]; //Stucture holding information about the PSPD thread
   mt_mx_struct_channel_mixer_t structChanMixer[MT_MX_MAX_OUTPUT_PORTS]; //Structure holding information about the PSPD Channel Mixer library
   mt_mx_struct_latency_module_t structLatencyModule[MT_MX_MAX_OUTPUT_PORTS]; //Structure holding information about the PSPD latency module library  
   mt_mx_struct_ramp_gain_t        structRampGain[MT_MX_MAX_OUTPUT_PORTS]; // structure holds info about PSPD ramp gains
   qurt_elite_channel_t                  pspd_channel; //Channel to manage PSPD threads under this input port. For each PSPD thread 3 bits are used - cmd, inp, out
                                                                           //9 output ports per input port would require 27 signal bits
   uint32_t                                     unCurrentWaitMask; //A combinational mask which indicates which bits of the above channel is this i/p port waiting on

   //Misc.
   uint32_t                unDataLogId;           //Data logging id: In case of stream-side port, upper 16 bits: asm apr port id lower 16 bits seq num.
   volatile uint32_t  *punMtMxInDelay;                                          //Address to the MXAT i/p port delay. MXAR will ignore this. (AV-Sync).
};

typedef struct MatrixOutPortInfoType MatrixOutPortInfoType;
struct MatrixOutPortInfoType
{
   //General properties
   MtMxOutPortHandle_t outPortHandle;   //Handle to the output port
   qurt_elite_queue_t*     bufQ;                 //Handle to the buffer Q of the port
   char                            bufQName[QURT_ELITE_DEFAULT_NAME_LEN];     //Buffer Q name
   uint32_t                       unMaxBufQCapacity;      //Max number of buffers the buf Q can hold
   elite_svc_handle_t*      pDownstreamPeerHandle; //Ptr to downstream peer's handle
   elite_msg_data_buffer_t* pOutputBuf;             //Ptr to port's output buffer
   int                               outPortState;                            //Holds the current state of the o/p
   int                               nOutputMode;                  //Mode in which output port is operating: push mode or pull mode
   uint32_t                       numBufRcvd;                    //Number of o/p buffers received
   uint32_t                       numBufSent;                    //Number of o/p buffers sent downstream
   int32_t*                       pAccBuf;                           //Pointer to output port's accumulator buffer (32-bit buffer)
   int32_t*                       pSecondaryAccBuf;                  //Pointer to output port's secondary accumulator buffer (32-bit buffer)   
   Elite_CbType                eliteCb;                //Callback structure provided by static svc
   bool_t                          bIsFirstOutBufYetToBeSent;
   bool_t                          bIsTopPriorityInputPortLive;     //A flag to indicate that the o/p port's top priority i/p port is Live
   uint16_t                       unPortPerfMode;    //Performance mode in which the output port is operating. Supported values are
                                                                     //ADM_LEGACY_DEVICE_SESSION & ADM_LOW_LATENCY_DEVICE_SESSION
                                                                     //For ADM_LOW_LATENCY_DEVICE_SESSION, 1msec local buffer is allocated
                                                                     //For ADM_LEGACY_DEVICE_SESSION, 5msec local buffer is allocated

   //Media type
   uint32_t                     unAfeFrameSizeInSamples; //Hold the unit frame size in samples as defined by AFE
   adsp_time                    unFrameDurationInUsec;   // Frame duration in microseconds
   uint32_t                     unSampleRate;                 //Sampling Frequency
   uint32_t                     unNumChannels;              //Number of channels
   uint8_t                      unChannelMapping[MT_MX_NUM_CHANNELS_EIGHT];    //Channel mapping
   uint32_t                     unBytesPerSample;          //Number of bytes per sample
   uint32_t                     unBufSize;                       //Output port's buffer size in samples.
   uint32_t                     unBufSizeInBytes;                       //Output port's buffer size in bytes.
   uint32_t                     unOutPortPerChBufSize; //Output port's buffer size per channel
   uint32_t                     unInterleaveFlag;            //Flag indicating whether output is interleaved or not.
   uint32_t                     unBitwidth;             // PCM samples bit width.
   uint32_t                     unNumAfeFrames;           //Number of AFE frames in matrix frame
   uint32_t                     unNewNumAfeFrames;        //New number of AFE frames in matrix frame


   //Masks
   uint32_t                     inPortsMask;                      //Mask that indicates which input ports feed to this output port.
   uint32_t                     accInPortsMask;                 //Mask that indicates which input ports accumulated to this output port's accBuf
   uint32_t                     secondaryAccInPortsMask;         //Mask that indicates which input ports accumulated to this output port's secondary accBuf
   uint32_t                     accBufAvailabilityMask;       //Mask that indicates which input ports are allowed to accumulate to this output port's accBuf
   uint32_t                     inPortsWaitingToAccMask; //Mask that indicates which input ports are waiting to accumulate to this output port's accBuf
                                                                               //NOTE: Live i/p ports should not be setting this mask. It is intended for non-live i/p ports ONLY
   uint32_t                     inPortsTopPriorityMask;     //Mask that indicates which of the input ports connected to this o/p port has the highest
                                                                               //(top) priority. The highest priority i/p always drives the action in the case of an i/p driven model (TX matrix)
                                                                               //If multiple i/ps connect to one o/p port, then live ports gets priority over non-live ports
                                                                               //Tie breaker: First come first privelege to decide on the top priority i/p port

   //Re-init/Re-config
   uint32_t                    unNumOutputBufs;                           //Number of o/p buffers to be allocated for this output port
   uint32_t                    unNumOutputBufsPendingRelease;  //Number of o/p buffers that this port needs to wait on before reconfiguring this o/p port
   uint32_t                    unOutPortReconfigMode;                //The reconfig mode that this o/p port was configured for.
   bool_t                       bIsOutPortWaitingForRscsToSendOut;//A flag to indicate that the o/p port needs more resources to send out the data already accumulated in its acc. buffer
   uint8_t                     unNativeModeFlags; // Flag used to indicate native mode operation. Flags are set according to ADM_CMD_DEVICE_OPEN_V6 API
   bool_t                       bIsOutPortInNativeModePendingReInit;//A flag to indicate that an o/p port that is operating in native mode is pending Re-init
   bool_t                       bIsBufferReinitPending;   //Indicates if buffer reinitialization is pending in this port
   bool_t                       bIsOutPortPendingReleaseBuffers; //This indicates if this o/p port is pending any buffers to be released
   uint32_t                    unNumChannelsPrRecfg, unBytesPerSamplePrRecfg, unSampleRatePrRecfg;
   uint8_t                      usChannelMappingPrRecfg[MT_MX_NUM_CHANNELS_EIGHT];
   int                            outPortStatusPriorToReconfig; //Holds the o/p port status prior to reconfig.

   //AV-Sync
   uint64_t                     ullPreviousWCTime;       //Wallclock time when previous output buffer was sent down
   adsp_time                       llSessionTime;               //Session time of the stream in microseconds
   adsp_time                     ullOutBufHoldDurationInUsec; //Hold duration for current output buffer in microseconds
   volatile uint32_t       *punCoppBufDelay;             //Address to the downstream COPP buffering delay (AV-Sync).
   volatile uint32_t       *punCoppAlgDelay;             //Address to the downstream COPP algorithmic delay (AV-Sync).
   volatile uint32_t       *punAFEDelay;                //Address to the downstream AFE delay (AV-Sync).
   volatile uint32_t       *punMtMxOutDelay;       //Address to the MXAR o/p port delay. MXAT will ignore this. (AV-Sync).
   volatile const afe_drift_info_t *pAfeDriftPtr; //AFE drift pointer. Applicable to RX matrix only.
   uint32_t                     unConnInPortIdWithValidTs; //Index of the first connected input port with a valid timestamp. This input port timestamp should be propagated downstream.
                                                                            //Set this to MT_MX_INVALID_PORT_ID upon initialization or if there isn't an input port with a valid timestamp
   avtimer_drv_handle_t avt_drv_handle; //Handle to avtimer
   bool_t                        bIsRegisteredForOverflowEvents;
   uint8_t                       unOverflowReportStatus;  //Status to report overflow
   bool_t                        bIsOutPortDelivTimeSyncDataWithAbsoluteTS;//A flag to indicate that this o/p port is sending down time synchronized data and absolute timestamps
   uint32_t                     unConnectedAsmSessionID; //The session/stream ID of connected ASM session. Applicable to TX matrix only.

   //Misc.
   uint32_t                     unDataLogId;                    //Data logging id: In case of stream-side port, upper 16 bits: asm apr port id lower 16 bits seq num.
};

// This contains all the required data for a service instance. Like the data members of a class...
typedef struct
{
	//General properties
   uint32_t                 mtMxID;                        // Mt Mx instance identifier
   elite_svc_handle_t        serviceHandle;              //Handle to give out to other services
   MatrixInPortInfoType    *inPortParams[MT_MX_MAX_INPUT_PORTS];        //Array of ptr to structures that hold info of all input ports
   MatrixOutPortInfoType *outPortParams[MT_MX_MAX_OUTPUT_PORTS];  //Array of ptr to structures that hold info of all output ports
   uint32_t                 maxInPortID;                       //Max input port ID
   uint32_t                 maxOutPortID;                    //Max output port ID
   char                      cmdQName[QURT_ELITE_DEFAULT_NAME_LEN];  //Command queue name
   bool_t                    bIsMxAtOperatingInBurstMode;   //This would indicate if the MXAT is operating in burst mode

   //Channels, bitmasks etc
   qurt_elite_channel_t     channel;                   //Mask for MQ's owned by this obj
   uint32_t                 unDataBitfield;                // pChannel sub-mask containing data Qs.
   uint32_t                 unCurrentBitfield;           //Matrix's overall pChannel wait mask.
   uint32_t                 unBufBitfield;                 // pChannel sub-mask containing buf Qs.
   uint32_t                 inPortIDMap[32];            //Array that maps i/p port IDs to their bit position in the channel.
   uint32_t                 outPortIDMap[32];         //Array that maps o/p port IDs to their bit position in the channel.
   uint32_t                 unChannelStatus;          //Bitfield returned from qurt_elite_channel_wait indicating which blocked Q's have msgs.

   //Masks
   uint32_t                 steadyStateInPortsMask;        //Input ports that: a. are active, b. have non-zero strMask and c. and are feeding to at least one active o/p port.

   //Misc.
   elite_qxdm_log_code 		qxdm_log_code;					//QXDM log code for the Matrix mixer service

   //MPPS/BW
   uint32_t                                             unKpps;                                    //KPPS for this matrix
   uint32_t                                             unBandWidth;                        //BW needs for this matrix
   uint32_t                                             unAdsppmClientId;
   qurt_elite_adsppm_client_t           *pAdsppmClient;
} This_t;

ADSPResult AudCreateMatrixMixerSvc (uint32_t inputParam, void **handle, MtMxParams_t* pMtMxParams);
ADSPResult MtMx_CustomMsgHandler(void* pInstance, elite_msg_any_t* pMsg);

#ifdef __cplusplus
}
#endif // __cplusplus
#endif //MATRIX_MIXER_H
