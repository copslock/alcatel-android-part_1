/*========================================================================
adsp_privatedefs.h

This file contains private ADSP APIs

Copyright (c) 2010 Qualcomm Technologies, Incorporated.
All rights reserved.
Qualcomm Confidential and Proprietary.
======================================================================== */

/*========================================================================
Edit History

$Header: //components/rel/avs.adsp/2.7.1.c4/aud/services/static_svcs/audio_stream_mgr/inc/adsp_private_api_ext.h#1 $

when       who     what, where, why
--------   ---     -------------------------------------------------------
10/07/13   RB      Created

========================================================================== */
/**
@file adsp_privatedefs_ext.h

@brief This file contains private ADSP APIs
*/

#ifndef _ADSP_PRIVATEAPI_EXT_H_
#define _ADSP_PRIVATEAPI_EXT_H_

#ifdef __cplusplus
extern "C" {
#endif /*__cplusplus*/

/** @addtogroup adsp_privatedefs
    @{ */

/******************************************************************************* stream commands */

/** End point IDs to go in stream_cmd */
/** Identifies the source as matrix or stream router. */
#define ASM_SOURCE_MATRIX_STRTR  0
/** Identifies the source as client */
#define ASM_SOURCE_CLIENT  1

/** Identifies the sink as matrix or stream router. */
#define ASM_SINK_MATRIX_STRTR  0
/** Identifies the sink as client */
#define ASM_SINK_CLIENT  1

/** Stream types to go in stream_cmd */
/** Identifies as stream type unknown */
#define ASM_STREAM_TYPE_UNKNOWN 0
/** Identifies the primary/main audio PID. */
#define ASM_STREAM_TYPE_PRIMARY  1
/** Identifies the secondary/associated audio/descriptor audio PID */
#define ASM_STREAM_TYPE_SECONDARY  2
/** Identifies the mix of primary and secondary */
#define ASM_STREAM_TYPE_PRIMARY_PLUS_SECONDARY  3
/** Identifies the a bit stream with both primary and secondary */
#define ASM_STREAM_TYPE_PRIMARY_SECONDARY  4



/** Parameter used by #ASM_SESSION_MTMX_STRTR_MODULE_ID_AVSYNC which allows the
    audio client choose the rendering decision that the audio DSP should use.

    @msgpayload{asm_session_mtmx_strtr_param_render_decision_t}
    @table{weak__asm__session__mtmx__strtr__param__render__decision__t}
*/
#define ASM_SESSION_MTMX_STRTR_PARAM_RENDER_DECISION        0x00012F0D

/** Indicates that rendering decision will be based on default rate (session clock based rendering, device driven).
 	  1. The default session clock based rendering is inherently driven by the timing of the device.
	  2. After the initial decision is made (first buffer after a run command), subsequent data rendering decisions
	  are made with respect to the rate at which the device is rendering, thus deriving its timing from the device.
	  3. While this decision making is simple, it has some inherent limitations (mentioned in the next section).
	  4. If this API is not set, the session clock based rendering will be assumed and this will ensure that the DSP is backward compatible.
 */
#define ASM_SESSION_MTMX_STRTR_PARAM_RENDER_DECISION_DEFAULT               0

/** Indicates that rendering decision will be based on local clock rate.
 	  1. In the DSP loopback/client loopback use cases (frame based inputs), the incoming data into audio DSP is time-stamped at the local clock rate (STC).
      2. This TS rate may match the incoming data rate or maybe different from the incoming data rate.
	  3. Regardless, the data will be time-stamped with local STC and therefore, the client is recommended to set this mode for these use cases.
	      This method is inherently more robust to sequencing (AFE Start/Stop) and device switches, among other benefits.
      4. This API will inform the DSP to compare every incoming buffer TS against local STC.
      5. DSP will continue to honor render windows APIs, as before.
  */
#define ASM_SESSION_MTMX_STRTR_PARAM_RENDER_DECISION_LOCAL_STC           1

/* Structure for rendering decision parameter */
typedef struct asm_session_mtmx_strtr_param_render_decision_t asm_session_mtmx_strtr_param_render_decision_t;

#include "adsp_begin_pack.h"

/** @weakgroup weak_asm_session_mtmx_strtr_param_render_decision_t
@{ */
/* Payload of the #ASM_SESSION_MTMX_STRTR_PARAM_RENDER_DECISION parameter.
*/
struct asm_session_mtmx_strtr_param_render_decision_t
{
    uint32_t                  flags;
    /**< Specifies the type of rendering decision the audio DSP should use.

         @values
         - #ASM_SESSION_MTMX_STRTR_PARAM_RENDER_DECISION_DEFAULT
         - #ASM_SESSION_MTMX_STRTR_PARAM_RENDER_DECISION_LOCAL_STC
         */
}
#include "adsp_end_pack.h"
;
/** @} */ /* end_weakgroup weak_asm_session_mtmx_strtr_param_render_decision_t */

/** Parameter used by #ASM_SESSION_MTMX_STRTR_MODULE_ID_AVSYNC which allows the
    audio client to specify the clock recovery mechanism that the audio DSP should use.

    @msgpayload{asm_session_mtmx_strtr_param_clock_recovery_t}
    @table{weak__asm__session__mtmx__strtr__param__clock__recovery__t}
*/
#define ASM_SESSION_MTMX_STRTR_PARAM_CLOCK_RECOVERY        0x00012F0E

/** Indicates that default clock recovery will be used (no clock recovery).
 	  If the client wishes that no clock recovery be done, the client can choose this.
 	  This means that no attempt will made by the DSP to try and match the rates of the input and output audio.
 */
#define ASM_SESSION_MTMX_STRTR_PARAM_CLOCK_RECOVERY_DEFAULT               0

/** Indicates that independent clock recovery needs to be used.
	1. In the DSP loopback/client loopback use cases (frame based inputs),
	    the client should choose the independent clock recovery option.
	2. This basically de-couples the audio and video from knowing each other�s clock sources
	    and lets the audio DSP independently rate match the input and output rates.
	3. After drift detection, the drift correction is achieved by either pulling the PLLs (if applicable) or
	    by stream to device rate matching (for PCM use cases) by comparing drift with respect to STC.
	4. For compressed use cases, since the PLL pulling is the only option, a best effort will be made.
	    If PLL pulling is not possible / available, the rendering will be done without rate matching.
  */
#define ASM_SESSION_MTMX_STRTR_PARAM_CLOCK_RECOVERY_INDEPENDENT           1

/* Structure for clock recovery parameter */
typedef struct asm_session_mtmx_strtr_param_clock_recovery_t asm_session_mtmx_strtr_param_clock_recovery_t;

#include "adsp_begin_pack.h"

/** @weakgroup weak_asm_session_mtmx_strtr_param_clock_recovery_t
@{ */
/* Payload of the #ASM_SESSION_MTMX_STRTR_PARAM_CLOCK_RECOVERY parameter.
*/
struct asm_session_mtmx_strtr_param_clock_recovery_t
{
    uint32_t                  flags;
    /**< Specifies the type of clock recovery that the audio DSP should use for rate matching

         @values
         - #ASM_SESSION_MTMX_STRTR_PARAM_CLOCK_RECOVERY_DEFAULT
         - #ASM_SESSION_MTMX_STRTR_PARAM_CLOCK_RECOVERY_INDEPENDENT
         */
}
#include "adsp_end_pack.h"
;
/** @} */ /* end_weakgroup weak_asm_session_mtmx_strtr_param_clock_recovery_t */


/** @} */ /* end_addtogroup adsp_privatedefs */


#ifdef __cplusplus
}
#endif /*__cplusplus*/

#endif /* _ADSP_PRIVATEAPI_EXT_H_ */
