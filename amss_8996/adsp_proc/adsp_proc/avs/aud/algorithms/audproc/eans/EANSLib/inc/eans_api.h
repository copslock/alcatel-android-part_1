#ifndef _AUDPROC_FENS_API_H
#define _AUDPROC_FENS_API_H
/*============================================================================
  @file EANS_api.h

  Public header file for EANS multi-mic algorithm.

        Copyright (c) 2009 Qualcomm Technologies Incorporated.
        All Rights Reserved.
        Qualcomm Confidential and Proprietary
============================================================================*/
/* $Header: //components/rel/avs.adsp/2.7.1.c4/aud/algorithms/audproc/eans/EANSLib/inc/eans_api.h#1 $    */
/*===========================================================================*
 * REVISION HISTORY                                                          *
 *when       who     what, where, why                                        *
 *---------- ------- --------------------------------------------------------*
 *12/22/09   swang   change single band NS pointer to three element array    *
 * 05/10/10   dineshr added a composite mode word for enabling VAD           *
 *===========================================================================*/

/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/

#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif //__cplusplus
/*----------------------------------------------------------------------------
 * Type Declarations
 * -------------------------------------------------------------------------*/

/**
  @brief Algorithm features, corresponding to bits in features word in EANSCfgType

// Mode word for configuring Far-end NS
// on/off bits 15-12      [    -    |    -    |  SWBNS  |    SWB   ] 
// on/off-bits 11-8       [   CNG   |    -    |  HISS   |   ENAL   ]
// on/off-bits 7-4        [   NAL   |   WB    |   NS    |    FFT   ]
// on/off-bits 3-0        [   ENS   |  NNS    |  SNS    |    HPF   ] 
 */
typedef enum
{
   EANS_FEATURE_HPF =    0x0001,     /* Apply a high-pass filter to the signal */
   EANS_FEATURE_SNS =    0x0002,     /* Enable stationary noise suppression */
   EANS_FEATURE_NNS =    0x0004,     /* Enable non-stationary noise suppression */
   EANS_FEATURE_ENS =    0x0008,     /* Enable excess noise suppression */   
   EANS_FEATURE_FFT =    0x0010,     /* Enable FFT and EANS processing */
   EANS_FEATURE_NS  =    0x0020,     /* Enable EANS gain processing for 8 & 16 KHz */// MOD_CHANGES: NOV 09
   EANS_FEATURE_WB  =    0x0040,     /* Enable wide-band (16kHz) processing */
   EANS_FEATURE_NAL =    0x0080,     /* Enable SNR based over-subtraction */   
   EANS_FEATURE_ENAL=    0x0100,     /* Enable excessive over-subtraction */  
   EANS_FEATURE_HISS=    0x0200,     /* hiss noise reduction */
   EANS_FEATURE_FQSM=    0x0400,     /* Enable frequency smoothing */
   EANS_FEATURE_CNG =    0x0800,     /* Perform comfort noise injection after NS */
   EANS_FEATURE_SWB =    0x1000,     /* super wide band support */
   EANS_FEATURE_SWBNS =  0x2000,     /* super wide band support noise suppression algorithm */
   EANS_FEATURE_LPF   =  0x4000,       /* low pass filter at 7200 Hz for WB */

   EANS_FEATURES_VAD = 0x218E        /* Mode word to check if VAD needs to be enabled */

} EansFeaturesType;

/** Brief Status return codes **/
typedef enum
{
   EANS_STATUS_SUCCESS  = 0,              /* Success */
   EANS_STATUS_FRAMESIZE_MISMATCH = -1    /* Frame size mismatch error for calibration re-init */

} EANSStatusType;


typedef struct _EansInterfaceParamsStruct
{
   int16	eans_mode;
   int16	eans_input_gain;
   int16	eans_output_gain;
   int16	eans_target_ns;
   int16	eans_s_alpha;
   int16	eans_n_alpha;
   int16	eans_n_alphamax;
   int16	eans_e_alpha;
   int16	eans_ns_snrmax;
   int16	eans_sns_block;
   int16	eans_ns_i;
   int16	eans_np_scale;
   int16	eans_n_lambda;
   int16	eans_n_lambdaf;
   int16	eans_gs_bias;
   int16	eans_gs_max;
   int16	eans_s_alpha_hb;
   int16	eans_n_alphamax_hb;
   int16	eans_e_alpha_hb;
   int16	eans_n_lambda0;
   int16	eans_gs_fast;
   int16	eans_gs_med;
   int16	eans_gs_slow;
   int16	eans_swb_nalpha;
   int16	eans_swb_salpha;
   int16	thresh;
   int16	pwr_scale;
   int16	hangover_max;
   int16	alpha_snr;
   int16	snr_diff_max;
   int16	snr_diff_min;
   int16	init_length;
   int16	max_val;
   int16	init_bound;
   int16	reset_bound;
   int16	avar_scale;
   int16	sub_nc;
   int16	spow_min;
   int16    eansTargetNoiseFloor;    /* Q15 target noise floor for the EANS output */
   int16    eansSlopeNoiseFloor;     /* Q15 slope of the noise floor for the EANS output */
} EansInterfaceParamsStruct;

/**
  @brief Initialize EANS algorithm

  Performs initialization of data structures for the
  EANS algorithm. A pointer to static memory is storage
  is passed for configuring the EANS static configuration
  structure.

  @param params: [in] Pointer to tuning parameter list
  @param pStaticMem: [in,out] Pointer to static memory
  @return static memory size
 */
int eans_init ( int16 *params, 
      void *pStaticMem );

/**
  @brief Re-initialize the EANS calibration parameters

  Performs initialization of the calibration data structures for the
  EANS algorithm. A pointer to static memory is storage
  is passed for configuring the EANS static configuration
  structure.

  @param params: [in] Pointer to tuning parameter list
  @param pStaticMem: [in,out] Pointer to static memory
  @return success or failure
 */
int16 eans_reinit ( int16 *params,
			      void *pStaticMem );

/**
  @brief Process one frame of EANS algorithm

  @param pInL16: [in] Pointer to input frame data
  @param pOutL16: [out] Pointer to output frame data
  @param pStaticMem: [in,out] Pointer to static memory storage
  @param pScratchMem: [in,out] Pointer to scratch memory storage
 */
int eans_process ( int16 *pInL16,
      int16 *pOutL16,
      void *pStaticMem,
      void *pScratchMem );
	  
#ifdef __cplusplus
}
#endif //__cplusplus
	  

#endif /* _AUDPROC_FENS_API_H */
