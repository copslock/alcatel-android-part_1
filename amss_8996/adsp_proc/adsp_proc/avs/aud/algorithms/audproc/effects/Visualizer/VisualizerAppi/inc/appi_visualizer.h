/* ======================================================================== */
/**
@file appi_visualizer.h

Header file to implement the Audio Post Processor Interface for
visualizer
*/

/* =========================================================================
Copyright (c) 2013 QUALCOMM Technology Incorporated.
All rights reserved. Qualcomm Proprietary and Confidential.
========================================================================== */

/* =========================================================================
Edit History

when       who     what, where, why
--------   ---     ------------------------------------------------------
10/01/13   WJ       Initial creation
========================================================================= */

/*------------------------------------------------------------------------
* Include files
* -----------------------------------------------------------------------*/
#ifndef __APPI_VISUALIZER_H
#define __APPI_VISUALIZER_H

#ifdef __cplusplus
extern "C"{
#endif /*__cplusplus*/

#include "Elite_APPI.h"

/*------------------------------------------------------------------------
* Macros, Defines, Type declarations
* -----------------------------------------------------------------------*/


/*------------------------------------------------------------------------
* Function declarations
* -----------------------------------------------------------------------*/

ADSPResult appi_visualizer_getsize(
                            const appi_buf_t*     params_ptr,
                            uint32_t*             size_ptr);

ADSPResult appi_visualizer_init(
                         appi_t*                    _pif,
                         bool_t*                    is_in_place_ptr,
                         const appi_format_t*       in_format_ptr,
                         appi_format_t*             out_format_ptr,
                         appi_buf_t*                info_ptr);


#ifdef __cplusplus
}
#endif /*__cplusplus*/


#endif //__APPI_VISUALIZER_H

