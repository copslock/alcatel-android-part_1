#ifndef C_ETSI_AMRWBPLUS_DEC_LIB_H
#define C_ETSI_AMRWBPLUS_DEC_LIB_H

/* ========================================================================
   ETSI AmrWbPlus decoder library wrapper header file

  *//** @file CEtsiAmrWbPlusDec.h
  This is a wrapper code for ETSI AmrWbPlus Core decoder library.
  the function in this file are called by the CEtsiAmrWbPlusDec media module.
  It is derived from CAudioProcLib class

  Copyright (c) 2008 Qualcomm Technologies Incorporated.
  All Rights Reserved. Qualcomm Proprietary and Confidential.
  *//*====================================================================== */

/* =========================================================================
                             Edit History

   when       who     what, where, why
   --------   ---     ------------------------------------------------------
   03/03/09    Raj     Created file.
   04/28/10    Raj     Cleaned up. moved most of the members to separate structure

   ========================================================================= */

#include "Elite_CAPI.h"


/* =======================================================================
 *                       DEFINITIONS AND DECLARATIONS
 * ====================================================================== */
   class CEtsiAmrWbPlusDecLib:public ICAPI
   {

private:
   
    void *m_pDecState;
    int CreateState();

public:

      enum AmrExtParamIdx
      {
        amrWbPlusMonoDecStereo = eIcapiMaxParamIndex,
        amrWbPlusLimiterOn,
        amrWbPlusFileFormat
      };

      enum AmrFormatType
      {
         AMR_FF_CONFORMANCE,
         AMR_FF_IF1,
         AMR_FF_IF2,
         AMR_FF_FSF,
         AMR_FF_RTP,
         AMR_FF_ITU,
         AMR_WB_PLUS_FF_TIF,
         AMR_WB_PLUS_FF_FSF,
         AMR_FF_MAX,        
      };  

   public:
   /* =======================================================================
    *                          Public Function Declarations
    * ======================================================================= */

   /**
    * Constructor of CEaacPlusDecLib
    */
   CEtsiAmrWbPlusDecLib (ADSPResult &nRes);

   /**
    * Destructor of CEaacPlusDecLib
    */
   ~CEtsiAmrWbPlusDecLib ( );

   /*************************************************************************
    * CAudioProcLib Methods
    *************************************************************************/

   /**
    * Initialize the core decoder library
    *
    * @return     success/failure is returned
    */
   virtual int CDECL Init ( CAPI_Buf_t* pParams );

   /**
    * Re initialize the core decoder library in the case of repositioning or
    * when full initialization is not required
    *
    * @return     success/failure is returned
    */
   virtual int CDECL ReInit ( CAPI_Buf_t* pParams );

   /**
    * Gracefully exit the core decoder library
    *
    * @return     success/failure is returned
    */
   virtual int CDECL End ( void );

   /**
    * Get the value of the EaacPLus decoder parameters
    *
    * @param[in]   nParamIdx      Enum value of the parameter of interest
    * @param[out]  pnParamVal     Desired value of the parameter of interest
    *
    * @return   Success/fail
    */
   virtual int CDECL GetParam ( int nParamIdx, int *pnParamVal );

   /**
    * Get the value of the EaacPLus decoder parameters
    *
    * @param[in]   nParamIdx      Enum value of the parameter of interest
    * @param[out]  nPrarmVal      Desired value of the parameter of interest
    *
    * @return   Success/fail
    */
   virtual int CDECL SetParam ( int nParamIdx, int nParamVal );

   /**
    * Decode audio bitstream and produce one frame worth of samples
    *
    * @param[in]   pInBitStream     Pointer to input bit stream
    * @param[out]  pOutSamples      Pointer to output samples
    * @param[out]  pOutParams       Pointer to output parameters
    *
    * @return     Success/failure
    */
   virtual int CDECL Process ( const CAPI_BufList_t* pInBitStream,
                               CAPI_BufList_t*       pOutSamples,
                               CAPI_Buf_t*       pOutParams );

  };

#endif /* C_ETSI_AMRWBPLUS_DEC_LIB_H */

