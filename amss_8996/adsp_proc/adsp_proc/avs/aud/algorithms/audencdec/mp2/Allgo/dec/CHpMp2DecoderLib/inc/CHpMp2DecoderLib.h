#ifndef C_HP_MP2_DECODER_LIB_H
#define C_HP_MP2_DECODER_LIB_H

/* ========================================================================
   High precission Mp2 decoder library wrapper header file

  *//** @file CHpMp2DecoderLib.h
  This is a wrapper code for Mp2 Core decoder library from Allgo.
  the function in this file are called by the CHpMp2Decoder media module.

  Copyright (c) 2011 Qualcomm Technologies Incorporated.
  All Rights Reserved. Qualcomm Proprietary and Confidential.
  *//*====================================================================== */

/* =========================================================================
                             Edit History

   when       who            what, where, why
   --------   --------     -- ----------------------------------------------
   13/08/2012    vnadipal           Created file

   ========================================================================= */


/* =======================================================================
 *                       DEFINITIONS AND DECLARATIONS
 * ====================================================================== */
#include "Elite_CAPI.h"


class CHpMp2DecoderLib : public ICAPI
{

   private:
      void* m_pDecObj;
      void* m_pScratchBuf;
      void* m_pUpsampTempBuf;
      void* m_pUpsampOutBuf;
      void* lfeOutput;
      void* globalPtr;
      int32 firstFrame;

      /*Default constructor private*/
      CHpMp2DecoderLib ( );

      int initState();

      void deinterleavePCMdata(int32_t * ptr_src_buf, int32_t * ptr_out_buf, uint32_t numSamples,uint32_t numChannels,int bits_per_sample);

      int lfeUpsampling( void * lfe_ptr,int32_t * ptr_src_buf, int32_t * ptr_out_buf, uint32_t numSamples,int bits_per_sample);

   public:
   /* =======================================================================
    *                          Public Function Declarations
    * ======================================================================= */

      /**
       * Constructor of CHpMp2DecoderLib that creats an instance of decoder lib
       */
      CHpMp2DecoderLib (uint16_t outputBitsPerSample, ADSPResult &nRes );

      /**
       * Destructor of CHpMp2DecoderLib
       */
      virtual ~CHpMp2DecoderLib ( );



      /*************************************************************************
       * Methods
       *************************************************************************/

      /**
       * Initialize the core decoder library
       *
       * @return     success/failure is returned
       */
      virtual int CDECL Init ( CAPI_Buf_t* pParams );

      /**
       * Re initialize the core decoder library in the case of repositioning or
       * when full initialization is not required
       *
       * @return     success/failure is returned
       */
      virtual int CDECL ReInit ( CAPI_Buf_t* pParams );

      /**
       * Gracefully exit the core decoder library
       *
       * @return     success/failure is returned
       */
      virtual int CDECL End ( void );

      /**
       * Get the value of the Mp2 decoder parameters
       *
       * @param[in]   nParamIdx      Enum value of the parameter of interest
       * @param[out]  pnParamVal     Desired value of the parameter of interest
       *
       * @return   Success/fail
       */
      virtual int CDECL GetParam ( int nParamIdx, int *pnParamVal );

      /**
       * Set the value of the Mp2 decoder parameters
       *
       * @param[in]   nParamIdx      Enum value of the parameter of interest
       * @param[out]  nPrarmVal      Desired value of the parameter of interest
       *
       * @return   Success/fail
       */
      virtual int CDECL SetParam ( int nParamIdx, int nParamVal );

      /**
       * Decode audio bitstream and produce one frame worth of samples
       *
       * @param[in]   pInBitStream     Pointer to input bit stream
       * @param[out]  pOutSamples      Pointer to output samples
       * @param[out]  pOutParams       Pointer to output parameters
       *
       * @return     Success/failure
       */
      virtual int CDECL Process ( const CAPI_BufList_t* pInBitStream,
                                  CAPI_BufList_t*       pOutSamples,
                                  CAPI_Buf_t*       pOutParams );
};

#endif /* C_HP_MP2_DECODER_LIB_H */

