/* ======================================================================== */
/**
@file appi_dts_drc.h

   Header file to implement the Audio Post Processor Interface for DRC in DTS
*/

/* =========================================================================
Copyright (c) 2012 Qualcomm Technologies Incorporated.
All rights reserved. Qualcomm Proprietary and Confidential.
========================================================================= */

/* =========================================================================
                             Edit History

   when       who       what, where, why
   --------   ---       ---------------------------------------------------
    6/15/12  bvodapal   Introducing Audio Post Processor Interface for DRC
                        in DTS
   ======================================================================== */

/*------------------------------------------------------------------------
 * Include files
 * -----------------------------------------------------------------------*/
#ifndef __APPI_DTS_DRC_SRC_H
#define __APPI_DTS_DRC_SRC_H

#include "Elite_APPI.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/*------------------------------------------------------------------------
 * Macros, Defines, Type declarations
 * -----------------------------------------------------------------------*/

#define AUDPROC_PARAM_ID_DTS_DRC_METADATA        0x1234  // TBD
static const uint32_t DTS_DRC_MAX_CHANNELS = 8;
/*------------------------------------------------------------------------
 * Function declarations
 * -----------------------------------------------------------------------*/

ADSPResult appi_dts_drc_getsize(
      const appi_buf_t*    params_ptr,
      uint32_t*            size_ptr);

ADSPResult appi_dts_drc_init(
      appi_t*              _pif,
      bool_t*              is_in_place_ptr,
      const appi_format_t* in_format_ptr,
      appi_format_t*       out_format_ptr,
      appi_buf_t*          info_ptr);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif //__APPI_DTS_DRC_SRC_H


