/* =========================================================================
 * Copyright (c) 2015-2015 QUALCOMM Technologies Incorporated.
 * All rights reserved.
 * Qualcomm Technologies Proprietary and Confidential.
 * =========================================================================*/

/**
 * @file capi_v2_interface.h
 *
 * Interface to a capi_v2 module by another capi_v2 module.
 */

/* =========================================================================
 * Edit History:
 * when         who         what, where, why
 * YYYY/MM/DD
 * ----------   -------     ------------------------------------------------
 * 2015/03/11   kgodara     CAPI_V2 Interface for CAPI_V2
 * =========================================================================*/

#ifndef CAPI_V2_INTERFACE_H
#define CAPI_V2_INTERFACE_H

#ifndef CAPI_V2_STANDALONE
/* For shared libraries. */
#include "shared_lib_api.h"
#else
#include "capi_v2_util.h"
#endif

#ifndef QURT_ELITE_ASSERT
#define QURT_ELITE_ASSERT(...) {;}
#endif

#include "Elite_CAPI_V2.h"

typedef struct capi_v2_dts_hpx_media_fmt {
    capi_v2_set_get_media_format_t main;
    capi_v2_standard_data_format_t std_fmt;
} capi_v2_dts_hpx_media_fmt_t;

typedef enum {
    MODULE_ACTIVE,
    MODULE_INACTIVE,
    MODULE_NOIMPL,
} module_state_t;

typedef enum {
    MIN_MODULE_INPUT_CHANNELS = 1,
    MAX_MODULE_INPUT_CHANNELS = 8,
    MIN_MODULE_OUTPUT_CHANNELS = 1,
    MAX_MODULE_OUTPUT_CHANNELS = 8,
} module_chan_limits_t;

typedef enum {
    MIN_INPUT_PORTS = 1,
    MAX_INPUT_PORTS = 1,
    MIN_OUTPUT_PORTS = 1,
    MAX_OUTPUT_PORTS = 1,
} module_port_limits_t;

typedef struct capi_v2_interface {
    capi_v2_t *capi_v2_module;
    capi_v2_stream_data_t module_input[MAX_INPUT_PORTS];
    capi_v2_stream_data_t module_output[MAX_OUTPUT_PORTS];
    capi_v2_buf_t module_input_bufs[MAX_MODULE_INPUT_CHANNELS];
    capi_v2_buf_t module_output_bufs[MAX_MODULE_OUTPUT_CHANNELS];
    capi_v2_dts_hpx_media_fmt_t input_media_fmt;
    capi_v2_dts_hpx_media_fmt_t output_media_fmt;
    module_state_t module_state;
    int8_t *buf_mem_ptr;
    uint32_t buf_mem_size;
    uint32_t delay_in_us;
    uint32_t kpps;
    bool_t is_enabled;
} capi_v2_interface_t;

#define SIZE_OF_ARRAY(array) (sizeof(array)/ sizeof(array[0]))


/* =========================================================================
 * CAPI_V2 to CAPI_V2 Interface functions.
 * =========================================================================*/

capi_v2_err_t capi_v2_interface_get_memory_requirement(
        capi_v2_proplist_t *init_set_properties,
        capi_v2_event_callback_info_t *cb_ptr,
        uint32_t module_id,
        uint32_t *module_mem_size);

capi_v2_err_t capi_v2_interface_init_module(
        capi_v2_interface_t *module_struct,
        capi_v2_event_callback_info_t *cb_ptr,
        capi_v2_proplist_t *init_set_properties,
        uint32_t module_id);

capi_v2_err_t capi_v2_interface_set_param(
		capi_v2_interface_t *module_struct,
		uint32_t param_id,
		const capi_v2_port_info_t *port_info_ptr,
        capi_v2_buf_t *params_ptr);

capi_v2_err_t capi_v2_interface_set_input_media_fmt(
        capi_v2_interface_t *module_struct,
        capi_v2_dts_hpx_media_fmt_t *data_ptr);

capi_v2_err_t capi_v2_interface_set_output_media_fmt(
        capi_v2_interface_t *module_struct,
        capi_v2_dts_hpx_media_fmt_t *data_ptr);

capi_v2_err_t capi_v2_interface_process(
        capi_v2_interface_t *module_struct);

capi_v2_err_t capi_v2_interface_end(
        capi_v2_interface_t *module_struct);

capi_v2_err_t capi_v2_interface_set_static_properties(
        capi_v2_interface_t *module_struct,
        capi_v2_proplist_t  *module_proplist);

capi_v2_err_t capi_v2_interface_allocate_module_bufs(
        capi_v2_interface_t *module_struct);

capi_v2_err_t capi_v2_interface_set_delay(
        capi_v2_interface_t *module_struct,
        uint32_t delay_in_us);

capi_v2_err_t capi_v2_interface_get_delay(
        capi_v2_interface_t *module_struct,
        uint32_t *delay_in_us);

capi_v2_err_t capi_v2_interface_set_kpps(
        capi_v2_interface_t *module_struct,
        uint32_t kpps);

capi_v2_err_t capi_v2_interface_get_kpps(
        capi_v2_interface_t *module_struct,
        uint32_t *kpps);

capi_v2_err_t capi_v2_interface_set_process_check(
        capi_v2_interface_t *module_struct,
        bool_t process_check);

capi_v2_err_t capi_v2_interface_get_process_check(
        capi_v2_interface_t *module_struct,
        bool_t *process_check);

#endif /* CAPI_V2_INTERFACE_H */

