/*===========================================================================
 * Copyright (c) 2013-2015 Qualcomm Technologies, Inc. All rights reserved.
 * Qualcomm Technologies Proprietary and Confidential.
 *===========================================================================*/
/**
   @file dts_hpx_appi_common_api.h
   General api parameters of dts hpx
*/

/*===========================================================================
 * Edit History
 * when       who         what, where, why
 * --------   ---         ---------------------------------------------------
 * 06/06/13   kgodara     common header for dts headphoneX premix and postmix
 *===========================================================================*/

/*---------------------------------------------------------------------------
 * Include files
 *---------------------------------------------------------------------------*/
#ifndef DTS_HPX_APPI_COMMON_API_H
#define DTS_HPX_APPI_COMMON_API_H

#ifndef CAPI_V2_STANDALONE
/* For shared libraries. */
#include "shared_lib_api.h"
#else
#include "capi_v2_util.h"
#endif


#include "Elite_APPI.h"
#include "Elite_CAPI_V2.h"
#include "adsp_dts_hpx_internal_api.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/**
 * module type information for dts_hpx
 */
typedef enum
{
    DTS_HPX_PREMIX,
    DTS_HPX_POSTMIX
} module_type_t;

/**
 * Device type informatoin of dts hpx, headphone or spk
 */
typedef enum
{
    DTS_HPX_SPK,
    DTS_HPX_HEADPHONE
} device_type_t;


/* DTS Headphone X premix and postmix module common function declaration */

dtsInt32 DTS_Eagle_DSEC_GetParam(
        void                  *theInstance,
        dtsInt32              nParameterType,
        void                  *pValue);

dtsInt32 DTS_Eagle_DSEC_SetParam(
        void                  *theInstance,
        dtsInt32              nParameterType,
        const void            *pValue,
        dtsUint32             nSize);

ADSPResult dts_hpx_license_validation_fn(
        const void            *license_ptr,
		uint32_t              license_size,
		const void            **license_info_ptr,
		uint32_t              *license_info_size,
		const void            *prev_license_info_ptr);


capi_v2_err_t dts_hpx_get_size_of_api_payload(
        uint32_t              param_id,
        uint32_t              *param_size);

capi_v2_err_t dts_hpx_check_supported_media_format(
        const appi_format_t   *format_ptr,
        module_type_t         module_id);

capi_v2_err_t dts_hpx_pcm_dump_utility(
        const appi_buflist_t  *input,
        char                  *file_tag,
        int                   bytes_per_sample,
        int                   file_create_flag);


/* Enabling this macro will enable file dump code in premix and postmix
 * modules. Input pcm and output pcm data will be dumps in separate files
 * for premix and postmix modules */
//#define DTS_HPX_APPI_FILEDUMP_DEBUG  1

/* Enabling this macro will log get param values to QXDM */
//#define DTS_HPX_GETPARAM_LOGGING     1


typedef enum
{
    DTS_PARAM_FIRST_PARAM = 0x10015140,
    DTS_PARAM_INPUT_SAMPLERATE_I32,
    DTS_PARAM_INPUT_CHANNELS_I32,
    DTS_PARAM_INPUT_CHANNEL_MASK_I64,
    DTS_PARAM_OUTPUT_SAMPLERATE_I32,
    DTS_PARAM_OUTPUT_CHANNELS_I32,
    DTS_PARAM_OUTPUT_CHANNEL_MASK_I64,
    DTS_PARAM_RESET_DEFAULT_CONFIG_NUL,
    DTS_PARAM_RESET_INTERNAL_BUFFER_NUL,
    DTS_PARAM_BUFFER_LATENCY_SAMPLES_I32,
    DTS_PARAM_MIN_FRAME_SAMPLES_I32,
    DTS_PARAM_ALGORITHM_LATENCY_SAMPLES_I32,
    DTS_PARAM_LIBRARY_DESCRIPTION_STRING,
    DTS_PARAM_LIBRARY_VERSION_STRING,
    DTS_PARAM_LAST_PARAM
} dts_hpx_gen_api_t;

/** \brief DTS Eagle Premix parameter type */
typedef enum
{
    DTS_PARAM_EAGLE_PREMIX_FIRST_PARAM = 0x100150F0,
    DTS_PARAM_EAGLE_PREMIX_OUTPUT_DEVICE_MODE_I32 = DTS_PARAM_EAGLE_PREMIX_FIRST_PARAM,
    DTS_PARAM_EAGLE_PREMIX_STREAM_GAIN_I32,
    DTS_PARAM_EAGLE_PREMIX_SHADOW_I32,
    DTS_PARAM_EAGLE_PREMIX_FADER_OUT_LENGTH_I32,
    DTS_PARAM_EAGLE_PREMIX_FADER_HOLD_LENGTH_I32,
    DTS_PARAM_EAGLE_PREMIX_FADER_IN_LENGTH_I32,
    DTS_PARAM_EAGLE_PREMIX_FADER_STATE_I32,
    DTS_PARAM_EAGLE_PREMIX_CONTROLS_STU,
    DTS_PARAM_EAGLE_PREMIX_LAST_PARAM = DTS_PARAM_EAGLE_PREMIX_CONTROLS_STU
} dtsParamEaglePremixType;

/** \brief DTS Eagle Postmix parameter type */
typedef enum
{
    DTS_PARAM_EAGLE_POSTMIX_FIRST_PARAM = 0x10015200,
    DTS_PARAM_EAGLE_POSTMIX_OUTPUT_DEVICE_MODE_I32 = DTS_PARAM_EAGLE_POSTMIX_FIRST_PARAM,
    DTS_PARAM_EAGLE_POSTMIX_GAIN_I32,
    DTS_PARAM_EAGLE_POSTMIX_HPF_ENABLE_I32,
    DTS_PARAM_EAGLE_POSTMIX_LIMITER_ENABLE_I32,
    DTS_PARAM_EAGLE_POSTMIX_HPX_STEREO_MODE_I32,
    DTS_PARAM_EAGLE_POSTMIX_DC_CUT_ENABLE_I32,
    DTS_PARAM_EAGLE_POSTMIX_SHADOW_I32,
    DTS_PARAM_EAGLE_POSTMIX_FADER_OUT_LENGTH_I32,
    DTS_PARAM_EAGLE_POSTMIX_FADER_HOLD_LENGTH_I32,
    DTS_PARAM_EAGLE_POSTMIX_FADER_IN_LENGTH_I32,
    DTS_PARAM_EAGLE_POSTMIX_FADER_STATE_I32,
    DTS_PARAM_EAGLE_HPF_FILT_COEF_STU,
    DTS_PARAM_EAGLE_POSTMIX_ROOM_MULTI_CH_STU,
    DTS_PARAM_EAGLE_POSTMIX_ROOM_SS_FRONT_STU,
    DTS_PARAM_EAGLE_POSTMIX_ROOM_SS_WIDE_STU,
    DTS_PARAM_EAGLE_POSTMIX_HPEQ_STU,
    DTS_PARAM_EAGLE_POSTMIX_HPX_INPUT_GAIN_MULTI_CH_I32,
    DTS_PARAM_EAGLE_POSTMIX_HPX_INPUT_GAIN_SS_FRONT_I32,
    DTS_PARAM_EAGLE_POSTMIX_HPX_INPUT_GAIN_SS_WIDE_I32,
    DTS_PARAM_EAGLE_POSTMIX_HPX_BYPASS_HPEQ_MULTI_CH_I32,
    DTS_PARAM_EAGLE_POSTMIX_HPX_BYPASS_HPEQ_SS_FRONT_I32,
    DTS_PARAM_EAGLE_POSTMIX_HPX_BYPASS_HPEQ_SS_WIDE_I32,
    DTS_PARAM_EAGLE_POSTMIX_AEQ_ENABLE_MULTI_CH_I32,
    DTS_PARAM_EAGLE_POSTMIX_AEQ_ENABLE_SS_FRONT_I32,
    DTS_PARAM_EAGLE_POSTMIX_AEQ_ENABLE_SS_WIDE_I32,
    DTS_PARAM_EAGLE_POSTMIX_HPX_MULTI_CH_MODE_I32,
    DTS_PARAM_EAGLE_POSTMIX_HPX_INPUT_GAIN_STEREO_I32,
    DTS_PARAM_EAGLE_POSTMIX_HPX_INPUT_GAIN_BYPASS_I32,
    DTS_PARAM_EAGLE_POSTMIX_HPX_INPUT_GAIN_DOWNMIX_I32,
    DTS_PARAM_EAGLE_POSTMIX_HPX_BYPASS_HPEQ_STEREO_I32,
    DTS_PARAM_EAGLE_POSTMIX_HPX_BYPASS_HPEQ_BYPASS_I32,
    DTS_PARAM_EAGLE_POSTMIX_HPX_BYPASS_HPEQ_DOWNMIX_I32,
    DTS_PARAM_EAGLE_POSTMIX_AEQ_ENABLE_STEREO_I32,
    DTS_PARAM_EAGLE_POSTMIX_AEQ_ENABLE_BYPASS_I32,
    DTS_PARAM_EAGLE_POSTMIX_AEQ_ENABLE_DOWNMIX_I32,
    DTS_PARAM_EAGLE_POSTMIX_CONTROLS_STU,
    DTS_PARAM_EAGLE_POSTMIX_CONTROLS_COEFFS_STU,
    DTS_PARAM_EAGLE_POSTMIX_LAST_PARAM = DTS_PARAM_EAGLE_POSTMIX_CONTROLS_COEFFS_STU
} dtsParamEaglePostmixType;


enum
{
    DTS_PARAM_EAGLE_DSEC_FIRST_PARAM = 0x10015100,
    DTS_PARAM_EAGLE_DSEC_LICENSE_DATA_STU = DTS_PARAM_EAGLE_DSEC_FIRST_PARAM,
    DTS_PARAM_EAGLE_DSEC_STATUS_I32,
    DTS_PARAM_EAGLE_DSEC_LAST_PARAM = DTS_PARAM_EAGLE_DSEC_STATUS_I32
};


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif  /* DTS_HPX_APPI_COMMON_API_H */
