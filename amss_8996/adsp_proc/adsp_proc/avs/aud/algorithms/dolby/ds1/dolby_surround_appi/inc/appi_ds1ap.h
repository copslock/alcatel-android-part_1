/* ======================================================================== */
/**
@file appi_ds1.h

   Header file to implement the Audio Post Processor Interface for DS1
*/

/*===========================================================================
  Copyright (c) 2013 Qualcomm Technologies, Inc. All rights reserved.
  Qualcomm Technologies Proprietary and Confidential.
===========================================================================*/

/*========================================================================
Edit History
when       who         what, where, why
--------   ---         -------------------------------------------------------
01/29/13   bvodapal      APPI public header file for DS1AP 

========================================================================== */

/*------------------------------------------------------------------------
 * Include files
 * -----------------------------------------------------------------------*/
#ifndef __APPI_DS1AP_H
#define __APPI_DS1AP_H


#include "Elite_APPI.h"


#ifdef __cplusplus
extern "C" {
#endif
 
      
typedef struct _audproc_param_id_ds1ap_output_media_fmt_t
{
   uint32_t sample_rate; /**< Sample Rate. */
   uint32_t num_channels; /**< Output number of channels. */
   uint8_t channel_type[8]; /**< Channel types for each of the num_channels. The valid channel types
                                                     are the PCM_CHANNEL_* types defined in adsp_media_fmt.h */
} audproc_param_id_ds1ap_output_media_fmt_t;


typedef struct _audproc_param_id_ds1ap_license_t
{
  uint32_t manufacturer;
  uint32_t license_len;    
  uint8_t  *license_ptr;
} audproc_param_id_ds1ap_license_t;      

#define AUDPROC_PARAM_ID_DS1AP_LICENSE             0x0000A0001  
#define AUDPROC_PARAM_ID_DS1AP_OUTPUT_MEDIA_FMT    0x0000A0002
/*------------------------------------------------------------------------
 * Function declarations
 * -----------------------------------------------------------------------*/

ADSPResult appi_ds1ap_getsize(
      const appi_buf_t* params_ptr,
      uint32_t* size_ptr);

ADSPResult appi_ds1ap_init( 
      appi_t*              _pif,
      bool_t*              is_inplace_ptr,
      const appi_format_t* in_format_ptr,
      appi_format_t*       out_format_ptr,
      appi_buf_t*          info_ptr);
      


#ifdef __cplusplus
}
#endif

#endif //__APPI_DS1AP_H

