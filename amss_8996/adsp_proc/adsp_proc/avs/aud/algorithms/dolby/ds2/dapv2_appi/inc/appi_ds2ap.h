/* ======================================================================== */
/**
@file appi_ds2ap.h

   Header file to implement the Audio Post Processor Interface for DAPv2
*/

/*===========================================================================
  Copyright (c) 2013 Qualcomm Technologies, Inc. All rights reserved.
  Qualcomm Technologies Proprietary and Confidential.
===========================================================================*/

/*========================================================================
Edit History
when       who         what, where, why
--------   ---         -------------------------------------------------------
11/14/13   bvodapal      APPI public header file for DAPv2

========================================================================== */

/*------------------------------------------------------------------------
 * Include files
 * -----------------------------------------------------------------------*/
#ifndef __APPI_DS2AP_H
#define __APPI_DS2AP_H


#include "Elite_APPI.h"


#ifdef __cplusplus
extern "C"
{
#endif
 
      
typedef struct _audproc_param_id_ds2ap_output_media_fmt_t
{
   uint32_t sample_rate;  /**< Sample Rate. */
   uint32_t num_channels;   /**< Output number of channels. */
   uint8_t  channel_type[8]; /**< Channel types for each of the num_channels. The valid channel types
                                                     are the PCM_CHANNEL_* types defined in adsp_media_fmt.h */
} audproc_param_id_ds2ap_output_media_fmt_t;


typedef struct _audproc_param_id_ds2ap_license_t
{
  uint32_t manufacturer;
  uint32_t license_len;    
  uint8_t  *license_ptr;
} audproc_param_id_ds2ap_license_t;

typedef struct _audproc_param_id_ds2ap_override_mode_t
{
  uint32_t enable;
} audproc_param_id_ds2ap_override_mode_t;


#define AUDPROC_PARAM_ID_DS2AP_LICENSE             0x0000A0003
#define AUDPROC_PARAM_ID_DS2AP_OUTPUT_MEDIA_FMT    0x0000A0004



/*------------------------------------------------------------------------
 * Function declarations
 * -----------------------------------------------------------------------*/

ADSPResult appi_ds2ap_getsize(
      const appi_buf_t* params_ptr,
      uint32_t* size_ptr);

ADSPResult appi_ds2ap_init(
      appi_t*              _pif,
      bool_t*              is_inplace_ptr,
      const appi_format_t* in_format_ptr,
      appi_format_t*       out_format_ptr,
      appi_buf_t*          info_ptr);
      


#ifdef __cplusplus
}
#endif

#endif //__APPI_DS2AP_H

