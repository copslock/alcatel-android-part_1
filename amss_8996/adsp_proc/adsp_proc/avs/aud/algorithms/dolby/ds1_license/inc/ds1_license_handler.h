/* ======================================================================== */
/**
@file ds1_license_handler.h

   Header file where APIs are defined for accessing the dolby license security
   mechanism related functions.
*/

/* =========================================================================
  Copyright (c) 2013 Qualcomm Technologies Incorporated.
  All rights reserved. Qualcomm Proprietary and Confidential.
  ========================================================================== */

/* =========================================================================
                             Edit History

   when       who     what, where, why
   --------   ---     ------------------------------------------------------
   03/03/13   SS      created file
   ========================================================================= */

/*------------------------------------------------------------------------
 * Include files
 * -----------------------------------------------------------------------*/
#ifndef __DS1_LICENSE_HANDLER_H
#define __DS1_LICENSE_HANDLER_H

#include "mmdefs.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

uint32_t get_ds1_license_size(void);
void* get_ds1_license_ptr(void);
uint32_t set_ds1_manufacture_id(const uint32_t manufacture_id);
uint32_t get_ds1_manufacture_id(void);
uint32_t set_ds1_license_unittest(int8_t* srcptr, uint32_t len);		// To set the license for the unit test



#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif //__DS1_LICENSE_HANDLER_H

