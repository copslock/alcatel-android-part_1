#ifndef _C_EAC3_PACKETIZER_LIB_H
#define _C_EAC3_PACKETIZER_LIB_H

/* ========================================================================
   eAc3 Packetizer library wrapper header file

  *//** @file eCAc3Packetizer.h
  This is a wrapper code for eAc3 Core Packetizer library. 

  Copyright (c) 2008 Qualcomm Technologies Incorporated.
  All Rights Reserved. Qualcomm Proprietary and Confidential.
  *//*====================================================================== */

/* =========================================================================
                             Edit History

   when       who           what, where, why
   --------   ---           ------------------------------------------------
   07/26/11   RP            Created file.

   ========================================================================= */

#include "Elite_CAPI.h"

/* =======================================================================
 *                       DEFINITIONS AND DECLARATIONS
 * ====================================================================== */

class CeAc3PacketizerLib : public ICAPI
{

   private:
   
   uint32_t m_frame_rate; /*IEC 60958 frame rate*/
   uint16_t m_stream_id;
   uint32_t m_frame_number;
   bool_t m_end_of_stream;
   int CreateState();

   public:
   
   enum eAc3PacketizerParamIdx
   {
      eAc3PacketizerBitstreamId = eIcapiMaxParamIndex,    
      eAc3PacketizerEndOfStream
   };

   /*channen mapping info for eAc3 packetizer*/
   CAPI_ChannelMap_t m_eAc3ChannelMap;

   /* =======================================================================
    *                          Public Function Declarations
    * ======================================================================= */

   /**
    * Constructor of CeAc3PacketizerLib
    */
   
   /*Default constructor*/
   CeAc3PacketizerLib ( );

   CeAc3PacketizerLib (ADSPResult    &nRes);

   /**
    * Destructor of CeAc3PacketizerLib
    */
   ~CeAc3PacketizerLib ( );


   /*************************************************************************
    * CAudioProcLib Methods
    *************************************************************************/

   /**
    * Initialize the core Packetizer library
    *
    * @return     success/failure is returned
    */
   virtual int CDECL Init ( CAPI_Buf_t* pParams );

   /**
    * Re initialize the core Packetizer library in the case of repositioning or
    * when full initialization is not required
    *
    * @return     success/failure is returned
    */
   virtual int CDECL ReInit ( CAPI_Buf_t* pParams );

   /**
    * Gracefully exit the core Packetizer library
    *
    * @return     success/failure is returned
    */
   virtual int CDECL End ( void );

   /**
    * Get the value of the Ac3 Packetizer parameters
    *
    * @param[in]   nParamIdx      Enum value of the parameter of interest
    *
    * @return  Value of the parameter of interest
    */
   virtual int CDECL GetParam ( int nParamIdx, int *pnParamVal );

   /**
    * Get the value of the Ac3 Packetizer parameters
    *
    * @param[in]   nParamIdx      Enum value of the parameter of interest
    * @param[out]  nPrarmVal      Desired value of the parameter of interest
    *
    * @return  None
    */
   virtual int CDECL SetParam ( int nParamIdx, int nParamVal );

   /**
    * Decode audio bitstream and produce one frame worth of samples
    *
    * @param[in]   pInBitStream     Pointer to input bit stream
    * @param[out]  pOutSamples      Pointer to output samples
    * @param[out]  pOutParams       Pointer to output parameters
    *
    * @return     Success/failure
    */
   virtual int CDECL Process ( const CAPI_BufList_t* pInBitStream,
                               CAPI_BufList_t*       pOutSamples,
                               CAPI_Buf_t*       pOutParams );   

};



#endif /* _C_EAC3_PACKETIZER_LIB_H */

