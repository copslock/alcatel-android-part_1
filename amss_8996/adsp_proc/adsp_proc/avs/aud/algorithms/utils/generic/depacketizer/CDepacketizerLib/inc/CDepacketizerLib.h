#ifndef _C_DEPACKETIZER_LIB_H
#define _C_DEPACKETIZER_LIB_H

/* ========================================================================
   Depacketizer library wrapper header file

  *//** @file Depacketizer.h
  This is a wrapper code for Depacketizer library. 

  Copyright (c) 2012 Qualcomm Technologies Incorporated.
  All Rights Reserved. Qualcomm Proprietary and Confidential.
  *//*====================================================================== */

/* =========================================================================
                             Edit History

   when       who           what, where, why
   --------   ---           ------------------------------------------------
   2/20/12   RP            Created file.

   ========================================================================= */

#include "Elite_CAPI.h"

/* =======================================================================
 *                       DEFINITIONS AND DECLARATIONS
 * ====================================================================== */

class CDepacketizerLib : public ICAPI
{

   private:

   uint32_t m_samples_per_frame;
   uint32_t m_sampling_rate;
   uint32_t m_num_channels;
   uint16_t m_format;
   void *params;


   /*Default constructor*/
   CDepacketizerLib( );
   
   public:  
   
   enum DepacketizerExtParamIdx
   {
	   eDepacketizerFormatType = eIcapiMaxParamIndex,
   };

    /* =======================================================================
    *                          Public Function Declarations
    * ======================================================================= */

   /**
    * Constructor of CDepacketizer
    */
   CDepacketizerLib (ADSPResult    &nRes);

   /**
    * Destructor of CDepacketizer
    */
   ~CDepacketizerLib ( );


   /*************************************************************************
    * CAudioProcLib Methods
    *************************************************************************/

   /**
    * Initialize the core Depacketizer library
    *
    * @return     success/failure is returned
    */
   virtual int CDECL Init ( CAPI_Buf_t* pParams );

   /**
    * Re initialize the core Depacketizer library in the case of repositioning or
    * when full initialization is not required
    *
    * @return     success/failure is returned
    */
   virtual int CDECL ReInit ( CAPI_Buf_t* pParams );

   /**
    * Gracefully exit the core Depacketizer library
    *
    * @return     success/failure is returned
    */
   virtual int CDECL End ( void );

   /**
    * Get the value of the Depacketizer parameters
    *
    * @param[in]   nParamIdx      Enum value of the parameter of interest
    *
    * @return  Value of the parameter of interest
    */
   virtual int CDECL GetParam ( int nParamIdx, int *pnParamVal );

   /**
    * Get the value of the Depacketizer parameters
    *
    * @param[in]   nParamIdx      Enum value of the parameter of interest
    * @param[out]  nPrarmVal      Desired value of the parameter of interest
    *
    * @return  None
    */
   virtual int CDECL SetParam ( int nParamIdx, int nParamVal );

   /**
    * Decode audio bitstream and produce one frame worth of samples
    *
    * @param[in]   pInBitStream     Pointer to input bit stream
    * @param[out]  pOutSamples      Pointer to output samples
    * @param[out]  pOutParams       Pointer to output parameters
    *
    * @return     Success/failure
    */
   virtual int CDECL Process ( const CAPI_BufList_t* pInBitStream,
                               CAPI_BufList_t*       pOutSamples,
                               CAPI_Buf_t*       pOutParams );   

};



#endif /* _C_DEPACKETIZER_LIB_H */

