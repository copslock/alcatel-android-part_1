#ifndef _C_DTSHD_PACKETIZER_LIB_H
#define _C_DTSHD_PACKETIZER_LIB_H

/* ========================================================================
   DTS-HD Packetizer library wrapper header file

  *//** @file CDtshdPacketizer.h
  This is a wrapper code for DTS-HD Packetizer library. 

  Copyright (c) 2012 Qualcomm Technologies Incorporated.
  All Rights Reserved. Qualcomm Proprietary and Confidential.
  *//*====================================================================== */

/* =========================================================================
                             Edit History

   when       who           what, where, why
   --------   ---           ------------------------------------------------
   7/31/12    RP            Created file.

   ========================================================================= */

#include "Elite_CAPI.h"

/* =======================================================================
 *                       DEFINITIONS AND DECLARATIONS
 * ====================================================================== */

class CDtshdPacketizerLib : public ICAPI
{

   private:
   
   uint32_t m_sampling_rate;
   uint16_t m_stream_id;   
   void *m_dtshd_pack_state_ptr;
   
   /*Default constructor*/
   CDtshdPacketizerLib ( );

   public:
   
   enum eDtsPacketizerParamIdx
   {
      eDtsPacketizerBitstreamId = eIcapiMaxParamIndex,          
   };

   /*channen mapping info for DTS packetizer*/
   CAPI_ChannelMap_t m_DtshdChannelMap;

   /* =======================================================================
    *                          Public Function Declarations
    * ======================================================================= */

   /**
    * Constructor of CDtshdPacketizerLib
    */ 
   
   CDtshdPacketizerLib (ADSPResult    &nRes);

   /**
    * Destructor of CDtshdPacketizerLib
    */
   ~CDtshdPacketizerLib ( );


   /*************************************************************************
    * CAudioProcLib Methods
    *************************************************************************/

   /**
    * Initialize the core Packetizer library
    *
    * @return     success/failure is returned
    */
   virtual int Init ( CAPI_Buf_t* pParams );

   /**
    * Re initialize the core Packetizer library in the case of repositioning or
    * when full initialization is not required
    *
    * @return     success/failure is returned
    */
   virtual int ReInit ( CAPI_Buf_t* pParams );

   /**
    * Gracefully exit the core Packetizer library
    *
    * @return     success/failure is returned
    */
   virtual int End ( void );

   /**
    * Get the value of the DTS Packetizer parameters
    *
    * @param[in]   nParamIdx      Enum value of the parameter of interest
    *
    * @return  Value of the parameter of interest
    */
   virtual int GetParam ( int nParamIdx, int *pnParamVal );

   /**
    * Get the value of the DTS Packetizer parameters
    *
    * @param[in]   nParamIdx      Enum value of the parameter of interest
    * @param[out]  nPrarmVal      Desired value of the parameter of interest
    *
    * @return  None
    */
   virtual int SetParam ( int nParamIdx, int nParamVal );

   /**
    * Decode audio bitstream and produce one frame worth of samples
    *
    * @param[in]   pInBitStream     Pointer to input bit stream
    * @param[out]  pOutSamples      Pointer to output samples
    * @param[out]  pOutParams       Pointer to output parameters
    *
    * @return     Success/failure
    */
   virtual int Process ( const CAPI_BufList_t* pInBitStream,
                               CAPI_BufList_t*       pOutSamples,
                               CAPI_Buf_t*       pOutParams );   

};



#endif /* _C_DTSHD_PACKETIZER_LIB_H */

