#ifndef _ADSP_AMDB_H
#define _ADSP_AMDB_H
/// @file adsp_amdb_mgr.idl
///
#include "AEEStdDef.h"
#ifndef __QAIC_HEADER
#define __QAIC_HEADER(ff) ff
#endif //__QAIC_HEADER

#ifndef __QAIC_HEADER_EXPORT
#define __QAIC_HEADER_EXPORT
#endif // __QAIC_HEADER_EXPORT

#ifndef __QAIC_HEADER_ATTRIBUTE
#define __QAIC_HEADER_ATTRIBUTE
#endif // __QAIC_HEADER_ATTRIBUTE

#ifndef __QAIC_IMPL
#define __QAIC_IMPL(ff) ff
#endif //__QAIC_IMPL

#ifndef __QAIC_IMPL_EXPORT
#define __QAIC_IMPL_EXPORT
#endif // __QAIC_IMPL_EXPORT

#ifndef __QAIC_IMPL_ATTRIBUTE
#define __QAIC_IMPL_ATTRIBUTE
#endif // __QAIC_IMPL_ATTRIBUTE
#ifdef __cplusplus
extern "C" {
#endif
#if !defined(__QAIC_STRING1_OBJECT_DEFINED__) && !defined(__STRING1_OBJECT__)
#define __QAIC_STRING1_OBJECT_DEFINED__
#define __STRING1_OBJECT__
typedef struct _cstring1_s {
   char* data;
   int dataLen;
} _cstring1_t;

#endif /* __QAIC_STRING1_OBJECT_DEFINED__ */
/*******************************************************************************
  When working with CAPI and CAPI_V2 modules use the following table to
  determine the values of id1 and id2

  Type       | id2           | id1
  -----------+---------------+---------------
  Generic    | Module id Set | Set to zero
  Encoder    | Output format | Set to zero
  Decoder    | Input format  | Set to zero
  Converter  | Input format  | Output format
  Packetizer | Input format  | Set to zero
*******************************************************************************/
#define AMDB_MODULE_TYPE_GENERIC 0
#define AMDB_MODULE_TYPE_DECODER 1
#define AMDB_MODULE_TYPE_ENCODER 2
#define AMDB_MODULE_TYPE_CONVERTER 3
#define AMDB_MODULE_TYPE_PACKETIZER 4
/***************************************************************************** 
    Add a dynamic CAPI module to the database
   
    id - module id
    preload - true if the shared object should be loaded during the add process
              false if the shared object should be loaded only when a module
              is queried that requires it
    filename_str - name of the shared object file (absolute path)
    getsize_str - name of the module's getsize()
    ctor_str - name of the module's ctor() function
  *****************************************************************************/
__QAIC_HEADER_EXPORT int __QAIC_HEADER(adsp_amdb_add_capi)(int id, boolean preload, const char* filename_str, const char* getsize_str, const char* ctor_str) __QAIC_HEADER_ATTRIBUTE;
/***************************************************************************** 
    Add a dynamic APPI module to the database
 
    id - module id
    preload - true if the shared object should be loaded during the add process
              false if the shared object should be loaded only when a module
              is queried that requires it
    filename_str - name of the shared object file (absolute path)
    getsize_str - name of the module's getsize()
    init_str - name of the module's init() function
  *****************************************************************************/
__QAIC_HEADER_EXPORT int __QAIC_HEADER(adsp_amdb_add_appi)(int id, boolean preload, const char* filename_str, const char* getsize_str, const char* init_str) __QAIC_HEADER_ATTRIBUTE;
/***************************************************************************** 
    Remove an APPI or CAPI module from the database based on module ID.  if
    filename_str is an empty string ("") then the module matching the id will
    be removed.  Otherwise all the APPI or CAPI associated with the shared
    object listed in filename_str will be removed.

    id - module id
    filename_str - name of the shared object, must not be NULL
  *****************************************************************************/
__QAIC_HEADER_EXPORT int __QAIC_HEADER(adsp_amdb_remove_capi)(int id, const char* filename_str) __QAIC_HEADER_ATTRIBUTE;
__QAIC_HEADER_EXPORT int __QAIC_HEADER(adsp_amdb_remove_appi)(int id, const char* filename_str) __QAIC_HEADER_ATTRIBUTE;
/***************************************************************************** 
    Remove all CAPI or APPI modules from the database (static modules are not removed)
  *****************************************************************************/
__QAIC_HEADER_EXPORT int __QAIC_HEADER(adsp_amdb_remove_all_capi)(void) __QAIC_HEADER_ATTRIBUTE;
__QAIC_HEADER_EXPORT int __QAIC_HEADER(adsp_amdb_remove_all_appi)(void) __QAIC_HEADER_ATTRIBUTE;
/***************************************************************************** 
    Print all CAPI or APPI modules via diagnostic messages
  *****************************************************************************/
__QAIC_HEADER_EXPORT int __QAIC_HEADER(adsp_amdb_print_all_capi)(void) __QAIC_HEADER_ATTRIBUTE;
__QAIC_HEADER_EXPORT int __QAIC_HEADER(adsp_amdb_print_all_appi)(void) __QAIC_HEADER_ATTRIBUTE;
/***************************************************************************** 
    Add a dynamic CAPI and CAPI_V2 module to the database
   
    type - type of module to be added (encoder, decoder, etc...)
    id1, id2 - depends on type
    preload - true if the shared object should be loaded during the add process
              false if the shared object should be loaded only when a module
              is queried that requires it
    filename_str - name of the shared object file (absolute path)
    getsize/getstatic properties_str - name of the module's getsize() or
                                       get_static_properties_str()
    ctor/init_str - name of the module's ctor() or init() function
  *****************************************************************************/
__QAIC_HEADER_EXPORT int __QAIC_HEADER(adsp_amdb_add_capi_ex)(int type, int id1, int id2, boolean preload, const char* filename_str, const char* getsize_str, const char* ctor_str) __QAIC_HEADER_ATTRIBUTE;
__QAIC_HEADER_EXPORT int __QAIC_HEADER(adsp_amdb_add_capi_v2)(int type, int id1, int id2, boolean preload, const char* filename_str, const char* get_static_properties_str, const char* init_str) __QAIC_HEADER_ATTRIBUTE;
/***************************************************************************** 
    Remove an CAPI or CAPI_V2 module from the database.  if
    filename_str is an empty string ("") then the module matching the type/ids
    will be removed.  Otherwise all the CAPI or CAPI_V2 associated with the
    shared object listed in filename_str will be removed.

    type - type of module to be added
    id1, id2 - depends on type
    filename_str - name of the shared object, must not be NULL
  *****************************************************************************/
__QAIC_HEADER_EXPORT int __QAIC_HEADER(adsp_amdb_remove_capi_ex)(int type, int id1, int idt, const char* filename_str) __QAIC_HEADER_ATTRIBUTE;
__QAIC_HEADER_EXPORT int __QAIC_HEADER(adsp_amdb_remove_capi_v2)(int type, int id1, int id2, const char* filename_str) __QAIC_HEADER_ATTRIBUTE;
/***************************************************************************** 
    Remove all CAPI_V2 modules from the database (static modules are not removed)
  *****************************************************************************/
__QAIC_HEADER_EXPORT int __QAIC_HEADER(adsp_amdb_remove_all_capi_v2)(void) __QAIC_HEADER_ATTRIBUTE;
/***************************************************************************** 
    Print all CAPI_V2 modules
  *****************************************************************************/
__QAIC_HEADER_EXPORT int __QAIC_HEADER(adsp_amdb_print_all_capi_v2)(void) __QAIC_HEADER_ATTRIBUTE;
#ifdef __cplusplus
}
#endif
#endif //_ADSP_AMDB_H
