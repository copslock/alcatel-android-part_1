/*============================================================================
            Copyright (c) 2013 Qualcomm Technologies Incorporated.
            All Rights Reserved.
            Qualcomm Confidential and Proprietary
============================================================================*/

#ifndef __SVA_MODEL_PARSER_UTILS_H__
#define __SVA_MODEL_PARSER_UTILS_H__


///////////////////////////////////////////////////////////////
/// MACRO FUNCTIONS ///////////////////////////////////////////
///////////////////////////////////////////////////////////////
#define ROUNDUP_MULTIPLE2(x) (((x) + 1) & ~1)
#define ROUNDUP_MULTIPLE4(x) (((x) + 3) & ~3)
#define ROUNDUP_MULTIPLE8(x) (((x) + 7) & ~7)

#define SELECT_MAX(x, y) ((x) > (y)  ? (x) : (y))
#define SELECT_MIN(x, y) ((x) < (y)  ? (x) : (y))


#define SERIALIZE_STATIC_FIELD(x,y)								\
		{														\
			memcpy((void *)(y), (void *)(&(x)), sizeof((x)));	\
			(y) += sizeof((x));									\
		}

#define SERIALIZE_DYNAMIC_FIELD(x,y,z)				\
		{											\
			memcpy((void *)(y), (void *)(x), (z));	\
			(y) += (z);								\
		}

#define ASSIGN_U16_FROM_ADDRESS_AND_MOVE(dst, src) {\
	dst = *(uint16 *)src;							\
	src += sizeof(dst);								\
	}												\

#define ASSIGN_S16_FROM_ADDRESS_AND_MOVE(dst, src) {\
	dst = *(int16 *)src;							\
	src += sizeof(dst);								\
	}												\
	
#define ASSIGN_U32_FROM_ADDRESS_AND_MOVE(dst, src) {\
	dst = *(uint32 *)src;							\
	src += sizeof(dst);								\
	}												\

#define ASSIGN_S32_FROM_ADDRESS_AND_MOVE(dst, src) {\
	dst = *(int32 *)src;							\
	src += sizeof(dst);								\
	}												\

#define ASSIGN_S16P_FROM_ADDRESS_AND_MOVE(dst, src, len) {\
	dst = (int16 *)src;							\
	src += len;										\
	}												\



#define ADD_S16_FROM_ADDRESS_AND_MOVE(dst, src) {   \
	dst += *(int16 *)src;							\
	src += sizeof(dst);								\
	}												\

#define float_to_fixed_s16(x, q)		((int16) ((x)*((double)(1 << q)) + (x>=0 ? 0.5 : -0.5)))
#define float_to_fixed_s16_sat(x, q)	((int16) ((((x)*((double)(1 << q)) + (x>=0 ? 0.5 : -0.5)) > MAX_S16) ? MAX_S16 : ((((x)*((double)(1 << q)) + (x>=0 ? 0.5 : -0.5)) < MIN_S16) ? MIN_S16 : ((x)*((double)(1 << q)) + (x>=0 ? 0.5 : -0.5)) )))
#define float_to_fixed_u16(x, q)		((uint16) ((x)*((double)(1 << q)) + 0.5))
#define float_to_fixed_u16_sat(x, q)	((uint16) ((((x)*((double)(1 << q)) + 0.5) > MAX_U16) ? MAX_U16 : ((((x)*((double)(1 << q)) + 0.5) < MIN_U16) ? MIN_U16 : ((x)*((double)(1 << q)) + 0.5) )))
#define float_to_fixed_s32(x, q)		((int32) ((x)*((double)((long long)1 << q)) + (x>=0 ? 0.5 : -0.5)))
#define float_to_fixed_s32_sat(x, q)	((int32) ((((x)*((double)((long long)1 << q)) + (x>=0 ? 0.5 : -0.5)) > MAX_S32) ? MAX_S32 : ((((x)*((double)((long long)1 << q)) + (x>=0 ? 0.5 : -0.5)) < MIN_S32) ? MIN_S32 : ((x)*((double)((long long)1 << q)) + (x>=0 ? 0.5 : -0.5)) )))
#define float_to_fixed_u32(x, q)		((uint32) ((x)*((double)((long long)1 << q)) + 0.5))
#define float_to_fixed_u32_sat(x, q)	((uint32) ((((x)*((double)((long long)1 << q)) + 0.5) > MAX_U32) ? MAX_U32 : ((((x)*((double)((long long)1 << q)) + 0.5) < MIN_U32) ? MIN_U32 : ((x)*((double)((long long)1 << q)) + 0.5) )))

#define fixed_to_float(x, q)			((float)(x) / (1LL << q))

#endif /* __SVA_MODEL_PARSER_UTILS_H__ */
