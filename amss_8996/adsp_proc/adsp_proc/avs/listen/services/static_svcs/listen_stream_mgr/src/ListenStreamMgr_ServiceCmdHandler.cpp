/**
@file ListenStreamMgr_ServiceCmdHandler.cpp

@brief This file contains the implementation for service cmdQ Handler functions
for ListenStreamMgr.

*/

/*========================================================================

$Header: //components/rel/avs.adsp/2.7.1.c4/listen/services/static_svcs/listen_stream_mgr/src/ListenStreamMgr_ServiceCmdHandler.cpp#1 $

Edit History

when       who     what, where, why
--------    ---       -----------------------------------------------------
07/14/2014  Unni      Initial version
==========================================================================*/

/*-----------------------------------------------------------------------
   Copyright (c) 2012-2014 Qualcomm  Technologies, Inc.  All Rights Reserved.
   Qualcomm Technologies Proprietary and Confidential.
-----------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include "ListenStreamMgr_Type.h"
#include "ListenStreamMgr_AprIf.h"
#include "ListenStreamMgr_ServiceCmdHandler.h"
#include "EliteMem_Util.h"
#include "adsp_lsm_service_commands.h"
#include "EliteTopology_db_if.h"
/* Memory Map handling */
#include "EliteMem_Util.h"

/*----------------------------------------------------------------------------
 * Constant / Define Declarations
 *--------------------------------------------------------------------------*/
extern uint32_t lsm_memory_map_client;


/*----------------------------------------------------------------------------
 * Function Definitions
 *--------------------------------------------------------------------------*/

/*
   This function processes  LSM_CMD_ADD_TOPOLOGIES command that is sent to LSM
   service.

   @param me_ptr[in/out] This points to the instance of ListenStreamMgr.

   @param pkt_ptr[in/out] This points to the elite_apr_packet that needs to be
                          processed.

   @return Success or Failure
*/
ADSPResult  lsm_service_cmd_q_add_topologies_handler(lsm_t *me_ptr,
                                                    elite_apr_packet_t *pkt_ptr)
{
  ADSPResult result = ADSP_EOK;
  lsm_cmd_add_topologies_t *palyoad_ptr = (lsm_cmd_add_topologies_t*)
                                          elite_apr_if_get_payload_ptr(pkt_ptr);

  /* Map the buffer */
  uint32_t phy_addr_lsw = (uint32_t)(palyoad_ptr->data_payload_addr_lsw);
  uint32_t phy_addr_msw = (uint32_t)(palyoad_ptr->data_payload_addr_msw);

  static const uint32_t CACHE_LINE_SIZE = 32; /* Must be a power of 2  */
  static const uint32_t CACHE_ALIGNMENT_MASK = (CACHE_LINE_SIZE - 1);
  bool_t is_aligned_cache_line = ((phy_addr_lsw & CACHE_ALIGNMENT_MASK) == 0);
  audproc_custom_topologies_t *header_ptr = NULL;
  uint8_t *virt_addr_ptr = NULL;
  uint32_t buffer_size = 0;
  uint32_t size_required = 0;

  if(!is_aligned_cache_line)
  {
    result = ADSP_EBADPARAM;
    MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
        "LSM: Physical address is not aligned to cache line");
    goto done;
  }



  elite_mem_shared_memory_map_t buffer_mem_node;
  buffer_mem_node.unMemMapHandle = palyoad_ptr->mem_map_handle;
  buffer_mem_node.unMemMapClient = lsm_memory_map_client;
  result =  elite_mem_map_get_shm_attrib(phy_addr_lsw,
                                         phy_addr_msw,
                                         palyoad_ptr->buffer_size,
                                         &buffer_mem_node);
  if(ADSP_FAILED(result))
  {
    MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "LSM: Failed to map physical memory");
    goto done;
  }

  // Since this buffer will be read, need to invalidate the cache.
  result = elite_mem_invalidate_cache(&buffer_mem_node);
  if(ADSP_FAILED(result))
  {
    MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
          "LSM: Failed to invalidate memory, result[0x%x]",
          result);
    goto done;
  }

  virt_addr_ptr = (uint8_t*)(buffer_mem_node.unVirtAddr);
  buffer_size = palyoad_ptr->buffer_size;

  if(buffer_size < sizeof(audproc_custom_topologies_t))
  {
    MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
          "LSM: Size of custom topologies buffer %lu bytes is insufficient",
          buffer_size);
    result = ADSP_ENEEDMORE;
    goto done;
  }

  header_ptr = (audproc_custom_topologies_t*)(virt_addr_ptr);
  virt_addr_ptr += sizeof(audproc_custom_topologies_t);
  buffer_size -= sizeof(audproc_custom_topologies_t);
  size_required = (header_ptr->num_topologies *
                   sizeof(audproc_topology_definition_t));

  if(buffer_size < size_required)
  {
    MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
          "LSM: Size of custom topologies array %lu bytes is insufficient,"
          " %lu bytes needed",
          buffer_size, size_required);
    result = ADSP_ENEEDMORE;
    goto done;
  }

  result = topo_db_add_topologies(TOPO_DB_CLIENT_TYPE_LSM,
                                  header_ptr->num_topologies,
                           (const audproc_topology_definition_t*)virt_addr_ptr);
  if(ADSP_FAILED(result))
  {
    MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
          "LSM: Failed to insert custom topologies for LSM into the database,"
          "result[0x%x]",
          result);
    goto done;
  }

done:
  lsm_generate_ack(pkt_ptr, result, 0, 0, 0);
  return result;
}

/*
   This function process the client command that are send to LSM service.

   @param pMe[in/out] This points to the instance of ListenStreamMgr

   @return Success or Failure
*/
ADSPResult lsm_service_cmd_q_apr_pkt_handler(lsm_t *me_ptr)
{
  ADSPResult result = ADSP_EOK;
  elite_apr_packet_t *pkt_ptr = (elite_apr_packet_t *)(me_ptr->msg.pPayload);
  uint32_t opcode = elite_apr_if_get_opcode(pkt_ptr);

  switch(opcode)
  {
    case LSM_SESSION_CMD_SHARED_MEM_MAP_REGIONS :
    {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,
          "ListenStreamMgr: Processing "
          "LSM_SESSION_CMD_SHARED_MEM_MAP_REGIONS Command");
      result = elite_mem_shared_memory_map_regions_cmd_handler(
                                     lsm_memory_map_client,
                                     lsm_get_apr_handle(),
                                     pkt_ptr,
                                     LSM_SESSION_CMDRSP_SHARED_MEM_MAP_REGIONS);
      break;
    }
    case LSM_SESSION_CMD_SHARED_MEM_UNMAP_REGIONS :
    {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,
          "ListenStreamMgr: Processing "
          "LSM_SESSION_CMD_SHARED_MEM_UNMAP_REGIONS Command");
      result = elite_mem_shared_memory_un_map_regions_cmd_handler(
                                                          lsm_memory_map_client,
                                                          lsm_get_apr_handle(),
                                                          pkt_ptr);
      break;
    }
    case LSM_CMD_ADD_TOPOLOGIES:
    {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,
          "ListenStreamMgr: Processing "
          "LSM_CMD_ADD_TOPOLOGIES Command");
      result = lsm_service_cmd_q_add_topologies_handler(me_ptr, pkt_ptr);
      break;
    }
    default:
    {
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "LSM: unsupported command 0x%lx",
            opcode);
      result = ADSP_EUNSUPPORTED;
      /* Generate ACK and free apr packet */
      lsm_generate_ack(pkt_ptr, ADSP_EUNSUPPORTED, NULL, 0, 0);
      break;
    }
  }

  return result;
}
