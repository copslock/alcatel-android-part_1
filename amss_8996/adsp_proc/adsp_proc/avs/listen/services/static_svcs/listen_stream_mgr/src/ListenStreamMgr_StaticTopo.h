#ifndef LISTENSTREAMMGR_StaicTopo_H_
#define LISTENSTREAMMGR_StaicTopo_H_
/** @file ListenStreamMgr_StaticTopo.h

 @brief This file contains Listen Specific Topology management helper

 */

/*========================================================================
 $Header: //components/rel/avs.adsp/2.7.1.c4/listen/services/static_svcs/listen_stream_mgr/src/ListenStreamMgr_StaticTopo.h#1 $

 Edit History

 when       who     what, where, why
 --------   ---     -------------------------------------------------------
 6/13/2014  Unni    Initial version
 ==========================================================================*/

/*-----------------------------------------------------------------------
   Copyright (c) 2012-2014 Qualcomm  Technologies, Inc.  All rights reserved.
   Qualcomm Technologies Proprietary and Confidential.
 -----------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include "Elite_APPI.h"
#include "adsp_error_codes.h"

/*----------------------------------------------------------------------------
 * Function declarations
 * -------------------------------------------------------------------------*/

/*
  This function gets topology_id from app_id to support LSM_SESSION_CMD_OPEN_TX
  which uses app_id

  @param[in]   app_id: app_id
  @param[out]  topology_id_ptr: pointer to return value (topology_id)

  @return
  Indication of success or failure.

  @dependencies
  None.
 */
ADSPResult lsm_get_topology_id_from_app_id(uint32_t app_id,
                                           uint32_t *topology_id_ptr);

#endif /* LISTENSTREAMMGR_StaicTopo_H_ */
