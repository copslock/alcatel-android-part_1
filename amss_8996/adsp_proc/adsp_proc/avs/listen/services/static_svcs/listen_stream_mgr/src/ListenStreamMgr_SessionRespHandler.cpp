/**
@file ListenStreamMgr_SessionRespHandler.cpp

@brief This file contains the implementation for session response handler functions
for ListenStreamMgr.

*/

/*========================================================================
$Header: //components/rel/avs.adsp/2.7.1.c4/listen/services/static_svcs/listen_stream_mgr/src/ListenStreamMgr_SessionRespHandler.cpp#1 $

Edit History

when       who     what, where, why
--------   ---     -------------------------------------------------------
12/18/2012  Sudhir      Initial version
==========================================================================*/

/*-----------------------------------------------------------------------
   Copyright (c) 2012-2013 Qualcomm  Technologies, Inc.  All Rights Reserved.
   Qualcomm Technologies Proprietary and Confidential.
-----------------------------------------------------------------------*/


/* =======================================================================
INCLUDE FILES FOR MODULE
========================================================================== */
#include "ListenStreamMgr_Type.h"
#include "ListenStreamMgr_Session.h"
#include "ListenStreamMgr_AprIf.h"
#include "ListenStreamMgr_AprDataBase.h"
#include "ListenStreamMgr_Session.h"
#include "ListenStreamMgr_SessionRespHandler.h"
#include "ListenProcSvc_CustMsg.h"
#include "EliteMsg_Custom.h"
#include "EliteMsg_AfeCustom.h"

/*--------------------------------------------------------------*/
/* Macro definitions                                            */
/* -------------------------------------------------------------*/

/* -----------------------------------------------------------------------
** Constant / Define Declarations
** ----------------------------------------------------------------------- */
extern uint32_t lsm_memory_map_client;
/* =======================================================================
**                          Function Definitions
** ======================================================================= */

/* =======================================================================
**        Session State Machine: RespQ message handler
** ======================================================================= */

ADSPResult lsm_process_session_resp_q(lsm_t *pMe, int32_t nChannelBit)
{
   ADSPResult result = ADSP_EOK;
   uint32_t opcode;
   uint8_t ucSessionId = (nChannelBit >> 1) - 1 ;
   lsm_session_state_type_t *pSession = &(pMe->aSessions[ucSessionId]);
   // Take next msg off the q
   result = qurt_elite_queue_pop_front(pSession->session_resp_q_ptr, (uint64_t*)&(pSession->processed_resp_msg));
   opcode = pSession->processed_resp_msg.unOpCode;
   //process the opcode
   switch (opcode)
   {
     case ELITE_CUSTOM_MSG:
      {
         result = lsm_session_resp_q_custom_msg_handler(pMe,pSession);
         break;
      }
     case ELITE_CMD_DESTROY_SERVICE:
      {
         result = lsm_session_resp_q_destroy_svc_handler(pMe,pSession);
         break;
      }
     case ELITE_CMD_SET_PARAM:
      {
         result = lsm_session_resp_q_set_param_handler(pMe,pSession);
         break;
      }
     case ELITE_CMD_GET_PARAM:
     {
    	 result = lsm_session_resp_q_get_param_handler(pMe,pSession);
    	 break;
     }
     default:
      {
        result = lsm_session_resp_q_unsupported_handler(pMe,pSession);
        break;
      }
   }
   return result;
}



ADSPResult lsm_session_resp_q_unsupported_handler(lsm_t *pMe, lsm_session_state_type_t *pSession)
{
   MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"lsm_session_resp_q_unsupported_handler");

   lsm_session_resp_q_generic_ack(pMe, pSession);
   return ADSP_EUNSUPPORTED; 
}


/* Destroy_Svc handler */
ADSPResult lsm_session_resp_q_destroy_svc_handler(lsm_t *pMe,lsm_session_state_type_t *pSession)
{
   int status;
   MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"ListenStreamMgr_SessionRespQDestroySvcHandler");

   qurt_elite_thread_join((pSession->dyn_svc.handle)->threadId, &status);
   // Generate ACK and free apr packet
   lsm_session_resp_q_generic_ack(pMe, pSession);
      
   pMe->curr_bit_field |= pSession->session_cmd_mask;

   pSession->session_state = LISTEN_STREAM_MGR_SESSION_DEINIT;

   return ADSP_EOK;
}



ADSPResult lsm_session_resp_q_generic_ack(lsm_t *pMe, lsm_session_state_type_t *pSession)
{
  ADSPResult result = ADSP_EOK;
  elite_apr_packet_t *pAprPkt = (elite_apr_packet_t*) (pSession->processed_msg.pPayload);
  elite_msg_any_payload_t *pRespPayload = (elite_msg_any_payload_t *) pSession->processed_resp_msg.pPayload;

  MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"ListenStreamMgr_SessionRespQGenericAck for opcode - 0x%lx", ((elite_msg_custom_header_t*)pRespPayload)->unSecOpCode);
  uint32_t status = pRespPayload->unResponseResult;
  uint8_t ucModuleId = 0;

   elite_msg_release_msg(&pSession->processed_resp_msg);

   // update expected response mask
   pSession->required_resp--;
   pSession->overall_result |= status;

   if ( ADSP_FAILED(status) )
   {
      MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"ListenStreamMgr internal command to module %d failed w/ error code = %lu",
            ucModuleId, status);
   }

   if ( 0 == pSession->required_resp )
   {
      //we got all expected responses...ack back the client command
      result = ADSP_EOK;
      if (ADSP_EOK != pSession->overall_result)
      {
         result = ADSP_EFAILED;
      }
      uint32_t opCode = pAprPkt->opcode;
      result = lsm_generate_ack(pAprPkt, result, NULL, 0, 0 );
      if ( ADSP_FAILED(result) )
      {
         MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"ListenStreamMgr sending ack to client command %lx failed w/ error code = %d",
               opCode, result);
      }

      //start listening to command queue
      pMe->curr_bit_field |= pSession->session_cmd_mask;
   }

   return result;
}


ADSPResult lsm_session_resp_q_custom_msg_handler(lsm_t *pMe, lsm_session_state_type_t *pSession)
{
   elite_msg_custom_header_t *pPayload = (elite_msg_custom_header_t*) pSession->processed_resp_msg.pPayload;
   ADSPResult result = ADSP_EOK;
   

   // Given the potential custom messages. 
   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"ListenStreamMgr_SessionRespQCustomMsgHandler for secondry opcode 0x%lx", pPayload->unSecOpCode);
   switch ( pPayload->unSecOpCode )
   {
      case ELITEMSG_CUSTOM_LISTEN_START:
	  {
	    result = lsm_session_resp_q_start_handler(pMe, pSession);
		break;
	  }	  
	  case ELITEMSG_CUSTOM_LISTEN_STOP:
	  {
	    result = lsm_session_resp_q_stop_handler(pMe, pSession);
		break;
	  }	  
	  case ELITEMSG_CUSTOM_LISTEN_REGISTER_SOUND_MODEL:
	  {
         result = lsm_session_resp_q_generic_ack(pMe, pSession);
         break;
	  }
	  case ELITEMSG_CUSTOM_LISTEN_DEREGISTER_SOUND_MODEL:
	  {
         result = lsm_session_resp_q_generic_ack(pMe, pSession);
         break;
	  }	  
	  case ELITEMSG_CUSTOM_LISTEN_EOB:
	  {
	    result = lsm_session_resp_q_eob_handler(pMe, pSession);
	    break;
	  }
	  default:
         elite_msg_release_msg( &pSession->processed_resp_msg);
         result = ADSP_EUNSUPPORTED;
         break;
   }
   return result;
}



ADSPResult lsm_session_resp_q_start_handler(lsm_t *pMe, lsm_session_state_type_t *pSession)
{
   ADSPResult result = ADSP_EOK;
   elite_apr_packet_t *pPkt = (elite_apr_packet_t*) ((pSession->processed_msg).pPayload);
   elite_msg_any_payload_t *pPayload = (elite_msg_any_payload_t *) pSession->processed_resp_msg.pPayload;
   uint32_t status = pPayload->unResponseResult;
 
   pSession->required_resp--;
   if (ADSP_FAILED(pPayload->unResponseResult ))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"ListenStreamMgr start command failed w/ error code = %lu",status);
   }
   pSession->overall_result |= pPayload->unResponseResult;

   if (0 == pSession->required_resp)
   {
      if (ADSP_EOK == pSession->overall_result )
      {
         pSession->session_state = LISTEN_STREAM_MGR_SESSION_ACTIVE;
      }
      else
      {
         result = ADSP_EFAILED;
      }
      result = lsm_generate_ack( pPkt, result, pPayload, 0, 0 );
   if ( ADSP_FAILED(result) )
  {
     MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"SessionRespQOnStartAck  sending ack to client command  failed result = %d", result);
   }
   //start listening to command queue
   elite_msg_release_msg(&pSession->processed_resp_msg);

   pMe->curr_bit_field |= pSession->session_cmd_mask;
   }
   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Processed LSM start cmd with status:0x%lx",pPayload->unResponseResult);
   return result;
 }
 
 
 ADSPResult lsm_session_resp_q_stop_handler(lsm_t *pMe, lsm_session_state_type_t *pSession)
{
   ADSPResult result = ADSP_EOK;
   elite_apr_packet_t *pPkt = (elite_apr_packet_t*) ((pSession->processed_msg).pPayload);
   elite_msg_any_payload_t *pPayload = (elite_msg_any_payload_t *) pSession->processed_resp_msg.pPayload;
 
   pSession->required_resp--;
   pSession->overall_result |= pPayload->unResponseResult;
   if (0 == pSession->required_resp)
   {
      //Irrespective of success/failure returned by dynamic service move state to INIT
      pSession->session_state = LISTEN_STREAM_MGR_SESSION_INIT;
     
      if (ADSP_EOK != pSession->overall_result)
      {
         result = ADSP_EFAILED;
      }
      result = lsm_generate_ack(pPkt, result, pPayload, 0, 0 ); 
   //start listening to command queue
   elite_msg_release_msg(&pSession->processed_resp_msg);
   pMe->curr_bit_field |= pSession->session_cmd_mask;
   }
   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Processed LSM stop cmd with status:0x%lx",pPayload->unResponseResult);
   return result;
 }
 
 
 ADSPResult lsm_session_resp_q_set_param_handler( lsm_t *pMe,
                                              lsm_session_state_type_t *pSession )
 {
    ADSPResult result = ADSP_EOK;
    elite_apr_packet_t *pPkt = (elite_apr_packet_t*)((pSession->processed_msg).pPayload);
    elite_msg_any_payload_t *pPayload = (elite_msg_any_payload_t *)pSession->processed_resp_msg.pPayload;

    pSession->required_resp--;
    pSession->overall_result |= pPayload->unResponseResult;
    if(0 == pSession->required_resp)
    {
       if(ADSP_EOK != pSession->overall_result)
       {
          result = ADSP_EFAILED;
       }
       result = lsm_generate_ack(pPkt, pPayload->unResponseResult, pPayload, 0, 0);
       if(ADSP_FAILED(result))
       {
          MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"SessionRespQsetparamAck  sending ack to client command  failed result = %d", result);
       }
       //start listening to command queue
       elite_msg_release_msg(&pSession->processed_resp_msg);
       pMe->curr_bit_field |= pSession->session_cmd_mask;
    }
    MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Processed set param cmd with status:0x%lx",pPayload->unResponseResult);
    return result;
 }


 ADSPResult lsm_session_resp_q_get_param_handler( lsm_t *pMe,
                                               lsm_session_state_type_t *pSession )
  {
     ADSPResult result = ADSP_EOK;
     elite_apr_packet_t *apr_packet_ptr = (elite_apr_packet_t*)((pSession->processed_msg).pPayload);
     elite_msg_param_any_t *payload_ptr = (elite_msg_param_any_t *)pSession->processed_resp_msg.pPayload;

     pSession->required_resp--;
     pSession->overall_result |= payload_ptr->unResponseResult;

     if(0 != pSession->required_resp)
     {
    	 pSession->overall_result |= payload_ptr->unResponseResult;
    	 return result;
     }

     switch(payload_ptr->unParamId)
     {
     case ELITEMSG_PARAM_ID_CAL:
     {
    	 elite_msg_param_cal_t *cal_payload_ptr = (elite_msg_param_cal_t *)payload_ptr;
    	 uint32_t opcode = elite_apr_if_get_opcode(apr_packet_ptr);

    	 /*Generate ACK. For in-band, apr_packet_ptr is created by LSM */
    	 if((LSM_SESSION_CMDRSP_GET_PARAMS == opcode)   ||
    	    (LSM_SESSION_CMDRSP_GET_PARAMS_V2 == opcode)  )
    	 {
    		 lsm_session_cmdrsp_get_params_v2_t *ret_payload_ptr = (lsm_session_cmdrsp_get_params_v2_t*)(elite_apr_if_get_payload_ptr(apr_packet_ptr));
    		 ret_payload_ptr->status = payload_ptr->unResponseResult;

    		 result = lsm_generate_ack(apr_packet_ptr, payload_ptr->unResponseResult, NULL, 0, 0 );
    	 }
    	 /*For out-of-band apr_packet_ptr is created by client*/
    	 else
    	 {
    		 lsm_session_cmd_get_params_v2_t *apr_payload_ptr = (lsm_session_cmd_get_params_v2_t *)(elite_apr_if_get_payload_ptr(apr_packet_ptr));

    		 /*Flush the cache*/
    		 elite_mem_shared_memory_map_t buffer_mem_node;
    		 buffer_mem_node.unMemMapClient     = lsm_memory_map_client;
    		 buffer_mem_node.unMemMapHandle     = apr_payload_ptr->mem_map_handle;
    		 buffer_mem_node.unMemSize          = apr_payload_ptr->param_max_size;
    		 buffer_mem_node.unPhysAddrLsw      = apr_payload_ptr->data_payload_addr_lsw;
    		 buffer_mem_node.unPhysAddrMsw      = apr_payload_ptr->data_payload_addr_msw;
    		 buffer_mem_node.unVirtAddr         = (uint32_t)cal_payload_ptr->pnParamData;

    		 result = elite_mem_flush_cache(&buffer_mem_node);

    		 if(ADSP_FAILED(result))
    		 {
    			 MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "LSM GET PARAM for session [%u]: Out-band Get Param failed to flush cache",
    					 pSession->ucInternalSessionId);
    			 payload_ptr->unResponseResult = ADSP_EFAILED;
    		 }

    		 /*send the ACK*/
    		 result = lsm_generate_ack(apr_packet_ptr, payload_ptr->unResponseResult,
    				 &payload_ptr->unResponseResult, sizeof(payload_ptr->unResponseResult), 0 );
    	 }
    	 break;
     }

     default:
     {
    	 MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"LSM GET PARAM for session [%u]: GET PARAM invalid ParamID 0x%lx",
    	                pSession->ucInternalSessionId,  payload_ptr->unParamId);
    	 result = ADSP_EFAILED;
    	 break;
     }
     }

     /*log error in response message*/
     if(ADSP_FAILED(payload_ptr->unResponseResult))
     {
    	 MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"LSM GET PARAM for session [%u]: ParamID 0x%lx failed with result 0x%lx",
    	     	                pSession->ucInternalSessionId,  payload_ptr->unParamId, payload_ptr->unResponseResult);
    	 result = ADSP_EFAILED;
     }
     else
     {
    	 MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"LSM GET PARAM for session [%u] processed successfully!",pSession->ucInternalSessionId);
     }

     /*release the response message*/
     elite_msg_release_msg(&pSession->processed_resp_msg);

     /*start listening to the command queue again*/
     pMe->curr_bit_field |= pSession->session_cmd_mask;

     return result;
  }

 ADSPResult lsm_session_resp_q_eob_handler(lsm_t *pMe, lsm_session_state_type_t *pSession)
 {
   ADSPResult result = ADSP_EOK;
   MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"ListenStreamMgr_SessionRespQEOBSvcHandler");
   if(ADSP_FAILED(result = lsm_session_resp_q_generic_ack(pMe, pSession)))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"SessionRespQEOBAck sending ack to client command  failed result = %d", result);
   }
   return result;
 }
