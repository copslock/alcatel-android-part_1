/**
@file ListenStreamMgr_SessionCmdHandler.h
@brief This file declares session cmd handler functions for ListenStreamMgr.

*/

/*========================================================================
Edit History

$Header: //components/rel/avs.adsp/2.7.1.c4/listen/services/static_svcs/listen_stream_mgr/src/ListenStreamMgr_SessionCmdHandler.h#1 $

when       who     what, where, why
--------   ---     -------------------------------------------------------
12/18/2012  Sudhir      Initial version
==========================================================================*/
/*-----------------------------------------------------------------------
   Copyright (c) 2012-2013 Qualcomm  Technologies, Inc.  All Rights Reserved.
   Qualcomm Technologies Proprietary and Confidential.
-----------------------------------------------------------------------*/

#ifndef LISTEN_STREAM_MGR_SESSION_CMD_HANDLER_H
#define LISTEN_STREAM_MGR_SESSION_CMD_HANDLER_H

/*-------------------------------------------------------------------------
Include Files
-------------------------------------------------------------------------*/

// System
#include "qurt_elite.h"
#include "Elite.h"

// Listen
#include "ListenStreamMgr_Type.h"



#ifdef __cplusplus
extern "C" {
#endif //__cplusplus

/*-------------------------------------------------------------------------
Preprocessor Definitions and Constants
-------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
Type Declarations
-------------------------------------------------------------------------*/
#define  LSM_CACHE_LINE_SIZE  32 // Must be a power of 2

#define  LSM_CACHE_ALIGNMENT_MASK  (LSM_CACHE_LINE_SIZE - 1)

static inline bool_t check_cache_line_alignment(uint32_t addr)
{
   return ((addr & LSM_CACHE_ALIGNMENT_MASK) == 0);
}
/*---------------------------------------------------------------------------
Class Definitions
----------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
Function Declarations and Documentation
----------------------------------------------------------------------------*/
/* Session cmd queue: processing functions and utility functions */

/*
   This function calls appropriate cmd handler based on opcode received.

   @param pMe[in/out] This points to the instance of ListenStreamMgr. The channel bit in this instance
                     that associated with the given session might be turned on/off depending
                     on command processing.
   @param nChannelBit[in] On which bit signal got.
   @return Success or Failure
*/
ADSPResult lsm_process_session_cmd_q(lsm_t *pMe, int32_t nChannelBit);

/*
   This function process the client command that are send to a given session.

   @param pMe[in/out] This points to the instance of ListenStreamMgr. The channel bit in this instance
                     that associated with the given session might be turned on/off depending
                     on command processing.
   @param pSession[in/out]  This points to the session that are to process the client command.
   @return Success or Failure
*/
ADSPResult lsm_session_cmd_q_apr_pkt_handler( lsm_t *pMe,
                                             lsm_session_state_type_t *pSession );


/*
   This function process unsupported messages in the session's command queue.

   @param pMe[in/out] This points to the instance of ListenStreamMgr. The channel bit in this instance
                     that associated with the given session might be turned on/off depending
                     on command processing.
   @param pSession[in/out]  This points to the session that are to process the client command.
   @return Success or Failure
*/
ADSPResult lsm_session_cmd_q_unsupported_handler( lsm_t *pMe,
                                             lsm_session_state_type_t *pSession );

/*
   This function process LSM_SESSION_CMD_OPEN_TX command that are send to a given session.

   @param pMe[in/out] This points to the instance of ListenStreamMgr. The channel bit in this instance
                     that associated with the given session might be turned on/off depending
                     on command processing.
   @param pSession[in/out]  This points to the session that are to process the client command.
   @return Success or Failure
*/
ADSPResult lsm_session_cmd_q_open_tx_stream_handler(lsm_t* pMe, lsm_session_state_type_t* pSession);


/*
   This function process   LSM_SESSION_CMD_CLOSE command that are to CLOSE a given session.

   @param pMe[in/out] This points to the instance of ListenStreamMgr. The channel bit in this instance
                     that associated with the given session might be turned on/off depending
                     on command processing.
   @param pSession[in/out]  This points to the session that are to process the client command.
   @return Success or Failure
*/
ADSPResult  lsm_session_cmd_q_close_tx_stream_handler(lsm_t *pMe,
                                                lsm_session_state_type_t *pSession);

/*
   This function processes  LSM_SESSION_CMD_SET_PARAM command that is sent to a given session.
   

   @param pMe[in/out] This points to the instance of ListenStreamMgr. The channel bit in this instance
                     that associated with the given session might be turned on/off depending
                     on command processing.
   @param pSession[in/out]  This points to the session that are to process the client command.
   @return Success or Failure
*/
ADSPResult lsm_session_cmd_q_set_param_handler(lsm_t* pMe,
                                            lsm_session_state_type_t *pSession);

/*
   This function processes  LSM_SESSION_CMD_GET_PARAM command that is sent to a given session.


   @param pMe[in/out] This points to the instance of ListenStreamMgr. The channel bit in this instance
                     that associated with the given session might be turned on/off depending
                     on command processing.
   @param pSession[in/out]  This points to the session that are to process the client command.
   @return Success or Failure
*/
ADSPResult lsm_session_cmd_q_get_param_handler(lsm_t* pMe,
                                               lsm_session_state_type_t *session_ptr);

/*
   This function processes  LSM_SESSION_CMD_START command that is sent to a given session

   @param pMe[in/out] This points to the instance of ListenStreamMgr. The channel bit in this instance
                     that associated with the given session might be turned on/off depending
                     on command processing.
   @param pSession[in/out]  This points to the session that are to process the client command.
   @return Success or Failure
*/
ADSPResult  lsm_session_cmd_q_start_handler(lsm_t *pMe,  lsm_session_state_type_t *pSession);



/*
   This function processes  LSM_SESSION_CMD_STOP command that is sent to a given session.

   @param pMe[in/out] This points to the instance of ListenStreamMgr. The channel bit in this instance
                     that associated with the given session might be turned on/off depending
                     on command processing.
   @param pSession[in/out]  This points to the session that are to process the client command.
   @return Success or Failure
*/
ADSPResult  lsm_session_cmd_q_stop_handler(lsm_t *pMe,
                                                lsm_session_state_type_t *pSession);
												
												
/*
   This function processes  LSM_SESSION_CMD_REGISTER_SOUND_MODEL command that is sent to a given session.

   @param pMe[in/out] This points to the instance of ListenStreamMgr. The channel bit in this instance
                     that associated with the given session might be turned on/off depending
                     on command processing.
   @param pSession[in/out]  This points to the session that are to process the client command.
   @return Success or Failure
*/
ADSPResult	lsm_session_cmd_q_register_sound_model_handler(lsm_t *pMe,
                                                lsm_session_state_type_t *pSession);
												
/*
   This function processes  LSM_SESSION_CMD_DEREGISTER_SOUND_MODEL command that is sent to a given session.
   

   @param pMe[in/out] This points to the instance of ListenStreamMgr. The channel bit in this instance
                     that associated with the given session might be turned on/off depending
                     on command processing.
   @param pSession[in/out]  This points to the session that are to process the client command.
   @return Success or Failure
*/
ADSPResult	lsm_session_cmd_q_deregister_sound_model_handler(lsm_t *pMe,
                                                lsm_session_state_type_t *pSession);												

/*
   This function processes  LSM_SESSION_CMD_EOB command that is sent to a given session.

   @param pMe[in/out] This points to the instance of ListenStreamMgr. The channel bit in this instance
                     that associated with the given session might be turned on/off depending
                     on command processing.
   @param pSession[in/out]  This points to the session that are to process the client command.
   @return Success or Failure
*/
ADSPResult  lsm_session_cmd_q_eob_handler(lsm_t *pMe,
                                                lsm_session_state_type_t *pSession);
#ifdef __cplusplus
}
#endif //__cplusplus

#endif // #ifndef LISTEN_STREAM_MGR_SESSION_CMD_HANDLER_H

