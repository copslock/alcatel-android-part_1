/**
@file ListenStreamMgr.cpp

@brief This file contains the implementation for ListenStreamMgr.

*/

/*========================================================================
$Header: //components/rel/avs.adsp/2.7.1.c4/listen/services/static_svcs/listen_stream_mgr/src/ListenStreamManager.cpp#1 $

Edit History

when       who     what, where, why
--------   ---     -------------------------------------------------------
10/18/2012  Sudhir      Initial version
==========================================================================*/

/*-----------------------------------------------------------------------
   Copyright (c) 2012-2013 Qualcomm  Technologies, Inc.  All Rights Reserved.
   Qualcomm Technologies Proprietary and Confidential.
-----------------------------------------------------------------------*/

/*========================================================================
INCLUDE FILES FOR MODULE
========================================================================== */
#include "ListenStreamMgr.h"
#include "ListenStreamMgr_Type.h"
#include "ListenStreamMgr_SessionCmdHandler.h"
#include "ListenStreamMgr_ServiceCmdHandler.h"
#include "ListenStreamMgr_SessionRespHandler.h"
#include "ListenStreamMgr_Session.h"
#include "EliteMsg_Custom.h"
#include "ListenStreamMgr_AprIf.h"
#include "ListenStreamMgr_AprDataBase.h"
#include "audio_basic_op_ext.h"
#include "ListenProcSvc.h"

/*--------------------------------------------------------------*/
/* Macro definitions                                            */
/* -------------------------------------------------------------*/

/* Only support the first three msgs currently in system cmdQ */
#define LISTEN_STREAM_MGR_MAX_OP_CODE_SYS_CMD_Q 16

/* -----------------------------------------------------------------------
** Constant / Define Declarations
** ----------------------------------------------------------------------- */
//LSM Static service  stack size
#define LSM_STAT_SVC_STACK_SIZE 4096

//LSM system Command Q Mask
#define LSM_SYSTEM_CMD_Q_MASK  0x00000001

//sys cmdQ + sys reserveQ + session CmdQ + session resp Q
#define LSM_NUM_HANDLERS 4

//system cmd Q handler
#define LSM_SYSTEM_CMDQ_HANDLER    0

//system reserve Q handler
#define LSM_SYSTEM_RESERVE_HANDLER 1

//session cmdQ handler
#define LSM_SESSION_CMDQ_HANDLER   2

//session respQ handler
#define LSM_SESSION_RESPQ_HANDLER  3

// public queue name
static char THREAD_NAME[]="LSM";

char LISTEN_STREAM_MGR_SYS_CMD_Q_NAME[] = "LSMsysC" ;

//qurt_elite MemoryMap Client
uint32_t lsm_memory_map_client;

//declaration of LSM service object
static lsm_t lsm_stat_svc_obj;

/* -----------------------------------------------------------------------
** Function prototypes
** ----------------------------------------------------------------------- */
// destructor
static void lsm_destroy (lsm_t* pMe);

// Main work loop for service thread. Pulls msgs off of queues and processes them.
static int lsm_workloop(void* pInstance);

// Process system CmdQ
static ADSPResult  lsm_process_system_cmd_q(lsm_t *pMe, int32_t nChannelBit);

// process system reserveQ
static ADSPResult lsm_process_system_reserve_q(lsm_t *pMe, int32_t nChannelBit) ;
//Handler for unsupported commands
static ADSPResult lsm_system_unsupported_handler(lsm_t *pMe) ;
//Handler for destroying static service
static ADSPResult lsm_system_destroy_svc_handler(lsm_t *pMe) ;
//Handler for starting static service
static ADSPResult lsm_system_start_svc_handler(lsm_t *pMe) ;
//Handler for processing channels 
static ADSPResult lsm_process_channels(lsm_t *pMe, uint32_t channel_status);

/* -----------------------------------------------------------------------
** Static variables
** ----------------------------------------------------------------------- */

/* Queue handler table.*/
static lsm_q_handler_func q_handler[LSM_NUM_HANDLERS] =
{
   lsm_process_system_cmd_q,
   lsm_process_system_reserve_q,
   lsm_process_session_cmd_q,  
   lsm_process_session_resp_q,
};
/* =======================================================================
**                          Function Definitions
** ======================================================================= */

/* =======================================================================
**         ListenStreamMgr Instance Create/Destroy/Workloop
** ======================================================================= */

ADSPResult aud_create_stat_lsm_svc (uint32_t dummy,void **handle)
{
   ADSPResult result = ADSP_EOK;
   qurt_elite_queue_t  *pQTemp;

   MSG(MSG_SSID_QDSP6, DBG_MED_PRIO, "LSM: Creating");

   memset(&lsm_stat_svc_obj,0,sizeof(lsm_t));

   lsm_t *pMe = (lsm_t*)&lsm_stat_svc_obj;

   // Create global cmd queue
   pQTemp = (qurt_elite_queue_t *)pMe->lsmCmdQBuf;
   if (ADSP_FAILED(result = qurt_elite_queue_init(LISTEN_STREAM_MGR_SYS_CMD_Q_NAME,
                      LISTEN_STREAM_MGR_MAX_SYS_CMD_Q_ELEMENTS, pQTemp)))
   {
      MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "Failed to init LSM Svc message queues!");
      lsm_destroy(pMe);
      return result;
   }
   pMe->serviceHandle.cmdQ = pQTemp;

   // set up channel
   qurt_elite_channel_init(&pMe->channel);

   //add system command queue
   if (ADSP_FAILED(result = qurt_elite_channel_addq(&pMe->channel, pMe->serviceHandle.cmdQ, LSM_SYSTEM_CMD_Q_MASK)))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "LSM: fail to add mqs to channel result = %d!", result);
      return(result);
   }
    // populate me
   pMe->serviceHandle.unSvcId = ELITE_STATLSM_SVCID;  
   
   //Create all session states here
   lsm_create_all_session_states(pMe);

   // Once session is created, initialize data base
   if ( ADSP_FAILED( result = lsm_apr_router_init(pMe)) ) 
   {
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "LSM: fail to init router result = %d!", result);
      return(result);
   }

   // Launch the thread
   if (ADSP_FAILED(result = qurt_elite_thread_launch(&(pMe->serviceHandle.threadId), THREAD_NAME, NULL,
      LSM_STAT_SVC_STACK_SIZE, ELITETHREAD_STAT_LISTEN_STREAM_SVC_PRIO, lsm_workloop, (void*)pMe,
      QURT_ELITE_HEAP_DEFAULT)))
   {
      MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "Failed to create LSM Thread!");
      lsm_destroy(pMe);
      return result;
   }

   *handle = &(pMe->serviceHandle);
   // register with qurt_elite memory map.
   qurt_elite_memorymap_register(&lsm_memory_map_client);

   MSG(MSG_SSID_QDSP6, DBG_MED_PRIO, "LSM Created");

   return ADSP_EOK;
}


/*
  Destroys the instance of the LSM
  @param[in]  pMe  The instance to destroy
  @return
  None
  @dependencies
  None.
*/
void lsm_destroy(lsm_t *pMe)
{
   MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "LSM Destroy");

   qurt_elite_memorymap_unregister(lsm_memory_map_client);
   for ( int i = 0; i < LSM_MAX_SESSION_ID; i++ )
   {
      lsm_session_destroy_state(pMe, i);
   }
   elite_svc_deinit_cmd_queue(pMe->serviceHandle.cmdQ);
   qurt_elite_channel_destroy(&pMe->channel);   
}


/* This function is the main work loop for the service. Commands from Apps
  are handled with the highest priority,
  service.
 */
static int lsm_workloop(void* pInstance)
{
   lsm_t *pMe = (lsm_t*)pInstance;       // instance structure
   ADSPResult result=ADSP_EOK;

   // set up mask for listening to the msg queues.
   pMe->curr_bit_field |= LSM_SYSTEM_CMD_Q_MASK ;   
   MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Entering LSM workloop...");
   // Enter forever loop
   for(;;)
   {
     // block on any one or more of selected queues to get a msg
     pMe->channel_status = qurt_elite_channel_wait(&(pMe->channel), pMe->curr_bit_field);
     MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Signal rcvd = %lx !", pMe->channel_status);
     result = lsm_process_channels(pMe, pMe->channel_status);
     /* If Service is destroyed, return immediately */
     if(result == ADSP_ETERMINATED) 
     {
        return ADSP_EOK;
     }
   } // forever loop

   return result;
}

/* =======================================================================
**                          SystemCmdQ processing
** ======================================================================= */

/* Do not process Systerm Reserve Q. Just report error. */
static ADSPResult lsm_process_system_reserve_q(lsm_t *pMe, int32_t channel_bit)
{
   MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"ProcessSysReserveQ for C= %ld", channel_bit);
   return ADSP_EUNSUPPORTED;
}

static ADSPResult  lsm_process_system_cmd_q(lsm_t *pMe, int32_t nChannelBit)
{
   ADSPResult result;
   uint32_t opcode;

   result = qurt_elite_queue_pop_front(pMe->serviceHandle.cmdQ, (uint64_t*)&(pMe->msg));
   opcode = pMe->msg.unOpCode;
   //process opcode
   switch (opcode)
   {
      case  ELITE_CMD_START_SERVICE:
      {
         result = lsm_system_start_svc_handler(pMe);
         break;
      }
      case ELITE_CMD_DESTROY_SERVICE:
      {
         result = lsm_system_destroy_svc_handler(pMe);
         break;
      }
      case ELITE_APR_PACKET:
      {
         result = lsm_service_cmd_q_apr_pkt_handler(pMe);
         break;
      }
      default:
      {
         result = lsm_system_unsupported_handler(pMe);
         break;
      }
   }
   return result;
}

static ADSPResult  lsm_system_unsupported_handler(lsm_t *pMe)
{
   MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"LSM: unsupporte msg");
   //print error
   lsm_print_error_msg(&pMe->msg);
   //return payload
   elite_msg_finish_msg( & (pMe->msg), ADSP_EUNSUPPORTED);
   return ADSP_EFAILED;
}

/* Message Handler to ELITE_CMD_START_SERVICE in sys Q */
static ADSPResult  lsm_system_start_svc_handler(lsm_t *pMe)
{
   ADSPResult result;

   MSG(MSG_SSID_QDSP6, DBG_MED_PRIO, "LSM: StartSvc  Enter");

   /****************************************************************
    ** Register ISR call back function with APR
   ******************************************************************/
   elite_apr_handle_t handle = 0;
   char aLsmName[] = "qcom.audio.lsm";
   uint32_t ulLsmNameSize = strlen( aLsmName);
   uint16_t usRetAddr = 0;
   if (ADSP_FAILED( result = elite_apr_if_register_by_name( &handle, &usRetAddr, aLsmName, ulLsmNameSize, &lsm_apr_callback_function,NULL))) 
   {
      MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "Fail to register with LSM  0x%8x", result );
      return result;
   }

   if (ADSP_FAILED( result = lsm_set_apr_handle( handle) )) 
   {
      MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "ListenStreamMgr: Fail to set handle 0x%8x", result);
   }

   elite_msg_finish_msg( & (pMe->msg), ADSP_EOK);

   MSG(MSG_SSID_QDSP6, DBG_MED_PRIO, "LSM: StartSvc  Leave");

   return result;
}

/* Message handler ELITE_CMD_DESTROY_SERVICE */
static ADSPResult lsm_system_destroy_svc_handler(lsm_t *pMe)
{

   MSG(MSG_SSID_QDSP6, DBG_MED_PRIO, "LSM: DestroySvc  Enter");

   elite_msg_any_t msg = pMe->msg;

   lsm_destroy(pMe);

   elite_msg_finish_msg( &msg, ADSP_EOK );

   MSG(MSG_SSID_QDSP6, DBG_MED_PRIO, "LSM: DestroySvc  Leave");

   // send ADSP_ETERMINATED so calling routine knows the destroyer has been invoked.
   return ADSP_ETERMINATED;
}

static ADSPResult lsm_process_channels(lsm_t *pMe, uint32_t channel_status)
{
   int32_t nEndBit, nStartBit, nMask;
   ADSPResult result=ADSP_EOK;

   if(!channel_status)
   {
      return ADSP_EOK;
   }

   // Process each ready channel once
   nEndBit   = LISTEN_STREAM_MGR_MAX_NUM_CHANNEL_BITS - s32_cl0_s32(channel_status);
   nStartBit = s32_get_lsb_s32(channel_status);
   nMask = 0x1 << nStartBit;
   
   for ( int32_t i = nStartBit; i < nEndBit; i++ ) 
   {
      if ( nMask & channel_status )
      {
         //if it is even call session cmdQ handler
         if ((i>1) && ((i&1) == 0) )
         {
            result = q_handler[LSM_SESSION_CMDQ_HANDLER](pMe,i);
         }
         //if it is odd call session respQ handler
         else if ((i>1) && ((i&1) == 1) )
         {
            result = q_handler[LSM_SESSION_RESPQ_HANDLER](pMe,i);
         }
         //call system cmdQ handler
         else
         {
         result = q_handler[i](pMe,i);
      }
      }
      nMask <<= 1;
      /*Service is destroyed, return immediately */
      if(result == ADSP_ETERMINATED) break;
   } 

   return result;
}

