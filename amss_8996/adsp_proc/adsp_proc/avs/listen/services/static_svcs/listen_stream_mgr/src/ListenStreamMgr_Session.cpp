/**
@file ListenStreamMgr_Session.cpp

@brief This file contains the implementation for session-control-related functions 
for ListenStreamMgr.

*/

/*========================================================================
$Header: //components/rel/avs.adsp/2.7.1.c4/listen/services/static_svcs/listen_stream_mgr/src/ListenStreamMgr_Session.cpp#1 $

Edit History

when       who     what, where, why
--------   ---     -------------------------------------------------------
12/18/2012  Sudhir   Initial version
==========================================================================*/

/*-----------------------------------------------------------------------
   Copyright (c) 2012-2013 Qualcomm  Technologies, Inc.  All Rights Reserved.
   Qualcomm Technologies Proprietary and Confidential.
-----------------------------------------------------------------------*/

/* =======================================================================
INCLUDE FILES FOR MODULE
========================================================================== */
#include "ListenStreamMgr_Type.h"
#include "ListenStreamMgr_Session.h"
#include "ListenStreamMgr_AprIf.h"
#include "ListenStreamMgr_AprDataBase.h"
#include "EliteMsg_Custom.h"
#include "EliteMsg_AfeCustom.h"
#include "qurt_elite_globalstate.h"

/*--------------------------------------------------------------*/
/* Macro definitions                                            */
/* -------------------------------------------------------------*/

/* -----------------------------------------------------------------------
** Constant / Define Declarations
** ----------------------------------------------------------------------- */
// maximum number of commands expected ever in command queue.
#define LISTEN_STREAM_MGR_MAX_CMD_Q_ELEMENTS  16
#define LISTEN_STREAM_MGR_MAX_RESP_Q_ELEMENTS 16

//declare buffers for maximumm number of sessions
static char lsmSessionCmdQ[LSM_MAX_SESSION_ID][QURT_ELITE_QUEUE_GET_REQUIRED_BYTES(LISTEN_STREAM_MGR_MAX_CMD_Q_ELEMENTS)];
static char lsmSessionRespQ[LSM_MAX_SESSION_ID][QURT_ELITE_QUEUE_GET_REQUIRED_BYTES(LISTEN_STREAM_MGR_MAX_RESP_Q_ELEMENTS)];

/* =======================================================================
**                          Function Definitions
** ======================================================================= */

/* =======================================================================
**                          Session State Machine Create/Destroy
** ======================================================================= */

/* This function creates all sessions state machine here. */
ADSPResult lsm_create_all_session_states(lsm_t *pMe) 
{
   int i;

   for (i =0; i< LSM_MAX_SESSION_ID ; i ++ )
   {
      if ( ADSP_FAILED( lsm_create_session_state( pMe, i) )) 
      {         
         MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "Failed to create ListenStreamMgr session %d!", i);
         return ADSP_EFAILED; 
      }
   }
   
   return ADSP_EOK; 
}

/**
* \brief This function creates the necessary session state
* structure given the internal session ID that it needs to
* create. Currently only supports up to 15 sessions.
*  
*/
ADSPResult lsm_create_session_state(lsm_t *pMe, uint8_t ucSessionId)
{
   lsm_session_state_type_t *pSession = &(pMe->aSessions[ucSessionId]); 
   ADSPResult result;
   int nStartBit; 
   qurt_elite_queue_t *pTempCmdQ, *pTempRespQ;

   /*Sanity Check for debugging only*/
   QURT_ELITE_ASSERT( ucSessionId < LSM_MAX_SESSION_ID );

   /* Create cmd  and resp queue */
   char aCmdQName[QURT_ELITE_DEFAULT_NAME_LEN], aRespQName[QURT_ELITE_DEFAULT_NAME_LEN];
   snprintf(aCmdQName, QURT_ELITE_DEFAULT_NAME_LEN,"LSMsc%d", ucSessionId);
   snprintf(aRespQName, QURT_ELITE_DEFAULT_NAME_LEN,"LSMsr%d", ucSessionId);

   pTempCmdQ = (qurt_elite_queue_t *)&lsmSessionCmdQ[ucSessionId];
   pTempRespQ = (qurt_elite_queue_t *)&lsmSessionRespQ[ucSessionId];

   // cmd and resp queue init for each LSM session 
   if (ADSP_FAILED(result = qurt_elite_queue_init(aCmdQName,LISTEN_STREAM_MGR_MAX_CMD_Q_ELEMENTS,pTempCmdQ))
      || ADSP_FAILED(result = qurt_elite_queue_init(aRespQName,LISTEN_STREAM_MGR_MAX_RESP_Q_ELEMENTS, pTempRespQ)))

   {
      MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "Failed to init LSM session cmd and resp queues!");
      return result;
   }
   pSession->session_cmd_q_ptr = pTempCmdQ;
   pSession->session_resp_q_ptr = pTempRespQ;

     /* Calcuate the bit mask for this session's command queue.
     The system reserves bit 0 and 1 of the channel. Session X
     cmd queue shall take bit 2*x+2 and rep queue takes bit
     2*x+3 */
   nStartBit   = 2 * ucSessionId  + 2; 
   pSession->session_cmd_mask   = 1 << nStartBit;
   pSession->session_resp_mask  = 1 << (nStartBit+1);

   /* Add queue to channel */
   if ( ADSP_FAILED(result = qurt_elite_channel_addq(&pMe->channel, pSession->session_cmd_q_ptr,pSession->session_cmd_mask))
        || ADSP_FAILED(result = qurt_elite_channel_addq(&pMe->channel, pSession->session_resp_q_ptr,pSession->session_resp_mask))
      )
   {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "CreateSessionState Failed to add Qs to C result = %d!", result);
      return result;
   }
   
   /* Session is in CLOSED state*/
   pSession->session_state = LISTEN_STREAM_MGR_SESSION_DEINIT;
   pSession->ucInternalSessionId = ucSessionId; 

   /* Immediately switch on channel */
   pMe->curr_bit_field |= pSession->session_cmd_mask; 

   return ADSP_EOK;
}


/**
* This function destroy session state assuming this session is
* no longer in use. This function should only be called when
* the whole static service is destroyed
*/
ADSPResult lsm_session_destroy_state(lsm_t *pMe, uint8_t ucSessionId)
{
   lsm_session_state_type_t *pSession = NULL;

   //even though session id is gettign in range of 1-8, still this check is required 
    //to avoid KW error
   if(ucSessionId < LSM_MAX_SESSION_ID)	
   {
      pSession = &(pMe->aSessions[ucSessionId]);
   }
   else
   {
      return  ADSP_EFAILED;
   }

   if (pSession == NULL)
   {
      return  ADSP_EFAILED;
   }

   if(LISTEN_STREAM_MGR_SESSION_DEINIT != pSession->session_state)
   {
      //Send a destroy to the dynamic service
      ADSPResult result = ADSP_EOK;
      uint32_t nSize = sizeof(elite_msg_cmd_destroy_svc_t);
      elite_msg_any_t destroyMsg;
      elite_svc_handle_t * pDynSvc = pSession->dyn_svc.handle;

      if(ADSP_FAILED(result = elite_msg_create_msg( &destroyMsg, &nSize, ELITE_CMD_DESTROY_SERVICE, pSession->session_resp_q_ptr, 0, ADSP_EOK)))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"lsm_session_destroy_state: Failed in elite_msg_create_msg with return code: %d", result);
      }

      if(ADSP_FAILED(result = qurt_elite_queue_push_back(pDynSvc->cmdQ, (uint64_t* )&destroyMsg)))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"lsm_session_destroy_state: Failed in qurt_elite_queue_push_back with return code: %d", result);
      }
      elite_msg_release_msg(&destroyMsg);
   }

   elite_svc_deinit_cmd_queue(pSession->session_cmd_q_ptr);
   elite_svc_deinit_cmd_queue(pSession->session_resp_q_ptr);
  
   memset(pSession, 0, sizeof(lsm_session_state_type_t));

   return ADSP_EOK;
}


ADSPResult lsm_session_fill_cb_data_for_dynamic_svc(lsm_t *pMe, 
                                         lsm_session_state_type_t *pSession,
                                                  lsm_callback_handle_t *pCbData)
{
   if ( pCbData )
   {
      elite_apr_packet_t *pPkt = (elite_apr_packet_t*) (pSession->processed_msg.pPayload);
      pCbData->apr_handle   = lsm_get_apr_handle();
      pCbData->client_addr  = elite_apr_if_get_src_addr(pPkt);
      pCbData->client_port  = elite_apr_if_get_src_port(pPkt);
      pCbData->self_addr  = elite_apr_if_get_dst_addr(pPkt);
      pCbData->self_port   = elite_apr_if_get_dst_port(pPkt);
      return ADSP_EOK; 
   }
   else
   {
      return ADSP_EBADPARAM; 
   }
}
