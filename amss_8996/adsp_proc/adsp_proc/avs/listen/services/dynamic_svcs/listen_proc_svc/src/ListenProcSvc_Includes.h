/**
  @file ListenProcSvc_Includes.h
  @brief This file contains Elite Listen Processing service include components.
*/

/*==============================================================================
  Copyright (c) 2012-2014 Qualcomm Technologies, Inc.(QTI)
  All rights reserved.
  Qualcomm Technologies Proprietary and Confidential.
==============================================================================*/

/*==============================================================================
  Edit History

  $Header: //components/rel/avs.adsp/2.7.1.c4/listen/services/dynamic_svcs/listen_proc_svc/src/ListenProcSvc_Includes.h#1 $

  when        who      what, where, why
  --------    ---      -------------------------------------------------------
  06/23/14    Unni     Added LSM topology support
  11/26/12    Sudhir   Initial version
==============================================================================*/
#ifndef _LISTENPROCSVC_INC_H_
#define _LISTENPROCSVC_INC_H_

/*------------------------------------------------------------------------------
 * Include Files
 *----------------------------------------------------------------------------*/
#include "qurt_elite.h"
#include "Elite.h"
#include "EliteMsg_Custom.h"
#include "ListenProcSvc.h"
#include "audio_basic_op_ext.h"
#include "adsp_lsm_api.h"
#include <stringl/stringl.h>
#include "topo_utils.h"
#include "Elite_CAPI_V2_types.h"
#include "AFEInterface.h"
#include "ListenProcSvc_mmpm.h"

/*------------------------------------------------------------------------------
 * Constant / Define Declarations
 *----------------------------------------------------------------------------*/
static const uint32_t LISTEN_PROC_CMD_SIG = 0x80000000;
static const uint32_t LISTEN_PROC_DATA_SIG = 0x40000000;
static const uint32_t LISTEN_PROC_RESP_SIG = 0x20000000;
static const uint32_t LISTEN_PROC_OUT_SIG = 0x10000000;
static const uint32_t LISTEN_PROC_NUM_Q = 2;

/**@brief Maximum data messages in data queue.*/
static const uint32_t MAX_DATA_Q_ELEMENTS = 16;

/**@brief Maximum number of commands expected ever in command queue. */
static const uint32_t MAX_CMD_Q_ELEMENTS = 8;

/* Macro for the number of buffers between dynamic service and AFE. */
#define NUM_DATA_BUFFERS  2

/* Number of channels */
#define NUM_CHANNELS  1

/* bytes per channel */
#define BYTES_PER_CHANNEL  2

/* interleaved or de intelreaved */
#define INTERLEAVED_DATA 0

/* Tap ID for logging LSM output */
#define LSMLOG_OUT_TAP_ID    0x0001

/*------------------------------------------------------------------------------
 * Global Data Definitions
 *----------------------------------------------------------------------------*/
extern uint32_t lsm_memory_map_client;


/* This is the state of each session */
enum lsm_proc_svc_state_t
{
  /* Stable States*/
  LISTEN_PROC_STATE_STOP = 0,    /**< Session is in STOP state. No data
                                     processing happens in this state.. */
  LISTEN_PROC_STATE_RUN ,        /**< Session is in RUN state. Data processing
                                     happens in this state. */
};

enum listen_proc_err_return_t {
	LISTEN_PROC_SUCCESS = 0,
	LISTEN_PROC_FAIL = 1,
	LISTEN_PROC_NEEDMORE,
	LISTEN_PROC_NOTPREPARED,	
	LISTEN_PROC_NULLREF,
	LISTEN_PROC_WRONGSIZE,
	LISTEN_PROC_WRONGMODEL,
	LISTEN_PROC_FATAL_BUFFER_OVERFLOW,
	LISTEN_PROC_DISABLED,
	LISTEN_PROC_LASTERROR,
};

typedef struct
{
  uint32_t out_buf_wr_offset;
  /**<Offset in output buffer */

  uint32_t empty_bytes;
  /**<Empty bytes in output buffer */

  elite_mem_shared_memory_map_t shared_memmap_type_node;
  /**< Shared Memory Node for Client buffers*/

} lsm_proc_svc_out_buf_param_t;

/**< @brief Listen Proc Svc data structure */
typedef struct
{
   elite_svc_handle_t svc_handle;
   /**<Handle to give out */

   elite_svc_handle_t *afe_svc_handle;
   /**< Peer service handle */

   qurt_elite_queue_t* response_q_ptr;
   /**< Response Q */

   qurt_elite_channel_t channel;
   /**< Mask for MQ's owned by this obj */

   uint32_t curr_bit_field;
   /**< Waiting signals indicator */

   uint32_t channel_status;
   /**< status of the channel */
   
   elite_msg_any_t cmd_msg;
   /**< Command message */
 
   elite_msg_any_t input_data_msg;
   /**< Input data message */

   lsm_callback_handle_t callback_data;
   /**< Local copy of call back data */

   lsm_proc_svc_state_t state;
   /**< Internal state of the session */

   uint32_t afe_port_id;
   /**< AFE port ID as provided by client */

   topo_handle_t topo_handle;
   /**< Libraries currently opened */

   uint16_t num_channels;
   /**< Num channels    */

   uint32_t sample_rate;
   /**< sampling rate using by library*/

   int16_t bits_per_sample;
   /**< bits per sample*/

   uint16_t session_id;
   /**< Session Id of this particular session*/

   uint32_t input_frame_size;
   /**< the minimum number of samples that listen block needs to process */

   uint16_t mode;
   /**< operation mode of library*/

   bool_t is_data_to_process;
   /**< Variable which indicates to process in coming data or not This will be
        true when it receiveds MEdia Format command. This will be set to false
        after sending DC reset command */

   lsm_mmpm_info_t mmpm_info;
   /**< Info/Params used for mmpm interactions */

   uint32_t cmds_received;
   /**< Variable to track the list of cmds received */

#ifdef DEBUG_LISTEN
   uint16_t afe_buffer_cnt;
   /**< for debugging purpose */
#endif

   elite_mem_shared_memory_map_t model_params;
   /**< Sound model params */

   int8_t detection_status;
   /**< Status of Detection Engine */

   void *lab_struct_ptr;
   /**< Pointer to LAB structure */

   elite_msg_any_t output_data_msg;
   /** output data message popped from output data queue */

   lsm_proc_svc_out_buf_param_t out_buf_params;
   /**< output buffer parameters*/

   uint32_t kw_end_max_delay_bytes;
   /**< max frame delay that SVA Algo can return */

   lsm_proc_revision_t revision;
   /**< Session revision defining and used to address compatibility details */

}listen_proc_svc_t;

/*------------------------------------------------------------------------------
 * Function declarations
 *----------------------------------------------------------------------------*/

/*
  This function creates all service queues

  @param me_ptr [in/out] This points to the instance of listen_proc_svc_t

  @return
  Indication of success or failure.

  @dependencies
  None.
*/
ADSPResult listen_proc_svc_create_svc_q(listen_proc_svc_t* me_ptr);

/*
  This function destroys the Listen Proc service

  @param me_ptr [in/out] This points to the instance of listen_proc_svc_t

  @return
  None

  @dependencies
  None.
*/
void listen_proc_svc_destroy_svc(listen_proc_svc_t* me_ptr);

/**
  Flushes buffers present in input Queue

  @param me_ptr [in/out] This points to the instance of listen_proc_svc_t

  @return
  Success/failure

  @dependencies
  None.
 */
ADSPResult listen_proc_svc_flush_input_q(listen_proc_svc_t* me_ptr);

/*
  Flushes buffers present in output Queue

  @param me_ptr [in/out] This points to the instance of listen_proc_svc_t
         result [in] This is the error result to be propagated to client
  @return
  Success/failure

  @dependencies
  None.

*/
ADSPResult listen_proc_svc_flush_output_q(listen_proc_svc_t *me_ptr,ADSPResult result);

/* 
  This function sets the parameters for the requested module id and param id 

  @param me_ptr [in/out] This points to the instance of listen_proc_svc_t

  @return
  Indication of success or failure.

  @dependencies
  None. 
*/
ADSPResult listen_proc_svc_set_param_handler(listen_proc_svc_t *me_ptr);

/**
  Function which gets the parameters from the library

  @param me_ptr [in/out] This points to the instance of listen_proc_svc_t

  @return
  Success/failure

  @dependencies
  None.
 */
ADSPResult listen_proc_svc_get_param_handler(listen_proc_svc_t *me_ptr);


/* 
  This function moves dynamic session from STOP to RUN state 

  @param me_ptr [in/out] This points to the instance of listen_proc_svc_t

  @return
  Indication of success or failure.

  @dependencies
  None. 
*/
ADSPResult listen_proc_svc_start_handler(listen_proc_svc_t *me_ptr);

/* 
  This function stops dynamic session
 
  @param me_ptr [in/out] This points to the instance of listen_proc_svc_t
 
  @return
  Indication of success or failure.

  @dependencies
  None. 
*/
ADSPResult listen_proc_svc_stop_handler(listen_proc_svc_t *me_ptr);

/* 
  This function registers sound model with  opened session
 
  @param me_ptr [in/out] This points to the instance of listen_proc_svc_t
 
  @return
  Indication of success or failure.

  @dependencies
  None. 
*/
ADSPResult  listen_proc_svc_register_sound_model_handler(listen_proc_svc_t *me_ptr);

/* 
  This function deregisters sound model with  opened session
 
  @param me_ptr [in/out] This points to the instance of listen_proc_svc_t

  @return
  Indication of success or failure.

  @dependencies
  None. 
*/
ADSPResult  listen_proc_svc_deregister_sound_model_handler(listen_proc_svc_t *me_ptr);

/*
  This function do the actual processing on data

  @param [in/out] me_ptr : pointer to instance of listen_proc_svc_t
  @param [in] inp_buf_size : size of input buffer
  @param [in/out] inp_buf_ptr : pointer to input data buffer


  @return
  Indication of success or failure.

  @dependencies
  None.
*/
ADSPResult listen_proc_svc_process(listen_proc_svc_t *me_ptr,
                                   uint32_t inp_buf_size,
                                   int8_t *inp_buf_ptr);

/* 
  This function sends the commands to AFE
 
  @param me_ptr [in/out] This points to the instance of listen_proc_svc_t
  @param afe_port_id [in]  afe port id to which session needs to connect/disconnect
  @param cmd [in]   type of command send to AFe

  @return
  Indication of success or failure.

  @dependencies
  None. 
*/
ADSPResult listen_proc_send_cmd_to_afe(listen_proc_svc_t *me_ptr,
                                       uint16_t afe_port_id,
                                       uint32_t cmd);



/*
  This function sends the following commands to AFE


  @param me_ptr [in/out] This points to the instance of listen_proc_svc_t
  @param criteria [in]  Criterion for which the client wants to register with AFE
  @param criteria_enable [in]  To enable or disable criterion
  @param cmd [in]   type of command send to AFe

  @return
  Indication of success or failure.

  @dependencies
  None.
*/
ADSPResult listen_proc_send_register_cmd_to_afe(listen_proc_svc_t *me_ptr,
                                                afe_client_register_criteria_type criteria,
                                                bool_t criteria_enable, uint32_t cmd);

/*
  This function Handles Capi_V2 Events

  @param inp_buf_params_ptr [in] pointer to input buf params

  @return
  Indication of success or failure.

  @dependencies
  None.
*/
capi_v2_err_t lsm_capi_v2_event_callback(void *context_ptr,
                                         capi_v2_event_id_t id,
                                         capi_v2_event_info_t *event_ptr);

/*
  This function stops buffering and do dc reset

  @param me_ptr [in/out] This points to the instance of listen_proc_svc_t

  @return
  Indication of success or failure.

  @dependencies
  None.
*/
ADSPResult listen_proc_svc_eob_handler(listen_proc_svc_t *me_ptr);

#endif /* _LISTENPROCSVC_INC_H_ */


