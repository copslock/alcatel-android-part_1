/**
  @file ListenSvc_CustMsg.h
  @brief Listen processing service Custom messages header.
*/

/*==============================================================================
  Copyright (c) 2012-2014 Qualcomm Technologies, Inc.(QTI)
  All rights reserved.
  Qualcomm Technologies Proprietary and Confidential.
==============================================================================*/

/*==============================================================================
  Edit History

  $Header: //components/rel/avs.adsp/2.7.1.c4/listen/services/dynamic_svcs/listen_proc_svc/inc/ListenProcSvc_CustMsg.h#1 $

  when        who      what, where, why
  --------    ---      -------------------------------------------------------
  11/26/12    Sudhir   Initial version
==============================================================================*/
#ifndef ELITE_LISTEN_PROCSVC_CUSTMSG_H
#define ELITE_LISTEN_PROCSVC_CUSTMSG_H

#ifdef __cplusplus
extern "C" {
#endif /*__cplusplus*/

/*------------------------------------------------------------------------------
 * Include Files
 *----------------------------------------------------------------------------*/
#include "qurt_elite.h"
#include "Elite.h"
#include "adsp_lsm_api.h"

/*------------------------------------------------------------------------------
 * Global definitions/forward declarations
 *----------------------------------------------------------------------------*/
/**  ID for an internal Start command */
#define ELITEMSG_CUSTOM_LISTEN_START      0x00001000

/** @brief internal start command */
typedef elite_msg_custom_header_t elite_msg_custom_start_t;


/**  ID for an internal Stop command */
#define ELITEMSG_CUSTOM_LISTEN_STOP      0x00001001

/** @brief internal Stop command */
typedef elite_msg_custom_header_t elite_msg_custom_stop_t;


/** ID for an internal register sound model command */
#define ELITEMSG_CUSTOM_LISTEN_REGISTER_SOUND_MODEL      0x00001002

/** @brief internal register sound model command */
typedef struct {
    qurt_elite_queue_t *pBufferReturnQ;
    /**< Queue to which the payload buffer must be returned.*/

    qurt_elite_queue_t *pResponseQ;
    /**< Queue to which to send the acknowledgment. NULL indicates that no
         response is required.*/

    uint32_t unClientToken;
    /**< Token to be given in the acknowledgment. This is different from the
         unResponseResult and can be used to identify which service sent the
         response to the server. */

    uint32_t unResponseResult;
    /**< Response result that the server sends back to the client. */

    uint32_t unSecOpCode;
    /**< This is the secondary opcode indicating the format for the rest of
         payload */

    uint32_t model_size;
    /**< Size of the registering sound model in bytes */

    uint32_t model_addr_lsw;
    /**< LSW of parameter data payload address. */

    uint32_t model_addr_msw;
    /**< MSW of parameter data payload address. */

    uint32_t mem_map_handle;
    /**< memory map handle */

} elite_msg_custom_register_sound_model_t;

/** ID for an internal deregister sound model command */
#define ELITEMSG_CUSTOM_LISTEN_DEREGISTER_SOUND_MODEL        0x00001003

/** @brief internal Deregister sound model command */
typedef elite_msg_custom_header_t elite_msg_custom_deregister_sound_model_t;

/**
 *ID for an internal EOB command*/
#define ELITEMSG_CUSTOM_LISTEN_EOB                         0x0000100A

/** @brief internal EOB command 
*/
typedef elite_msg_custom_header_t elite_msg_custom_eob_t;

#ifdef __cplusplus
}
#endif /*__cplusplus*/

#endif /* #ifndef ELITE_LISTEN_PROCSVC_CUSTMSG_H*/
