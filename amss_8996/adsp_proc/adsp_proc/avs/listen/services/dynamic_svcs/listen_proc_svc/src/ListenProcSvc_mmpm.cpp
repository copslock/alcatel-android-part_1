/**
  @file ListenProcSvc_mmpm.cpp
  @brief LSM power manager : Manages power related activities for LSM service
*/

/*==============================================================================
  Edit History

  $Header:

  when       who     what, where, why
  --------   ---     -------------------------------------------------------
  12/12/14   unni    Moving LSM MMPM from static to dynamic service

==============================================================================*/

/*==============================================================================
  Copyright (c) 2012-2015 Qualcomm Technologies, Inc.(QTI)
  All rights reserved.
  Qualcomm Technologies Proprietary and Confidential.
==============================================================================*/

/*------------------------------------------------------------------------------
 * Include Files
 *----------------------------------------------------------------------------*/
#include "qurt_elite.h"
#include "ListenProcSvc_mmpm.h"
#include "lsm_devcfg.h"

/*------------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 *----------------------------------------------------------------------------*/
#define LSM_NUM_REQUEST_MAX 1
#define ADSPPM_NULL_HANDLE  0
#define CLIENT_NAME_SESSION_ID_OFFSET  4
/* Marco for ceil div, alter. (x + y - 1)/y includes overflow*/
#define ceil_div(x, y) ((x)==0 ? 0 : 1 + (((x) - 1) / (y)))

#if (ADSPPM_INTEGRATION==1)
#include "mmpm.h"

/**
 * Registers a lsm session with mmpm and populate mmpm client info
 * @param mmpm_info_ptr - lsm session mmpm state variables
 * @param session_id - lsm session id
 *
 * @return adsp result status
 */
ADSPResult lsm_mmpm_register(lsm_mmpm_info_t *mmpm_info_ptr,
                             uint16_t session_id)
{
  char client_name[]  = "LSM[0]";
  MmpmRegParamType regParam;

  if(NULL == mmpm_info_ptr)
  {
    MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "lsm mmpm reg: NULL arg");
    return ADSP_EBADPARAM;
  }

  /* read lsm device power properties */
  if(ADSP_EOK != lsm_devcfg_read_mmpm_data(&mmpm_info_ptr->dev_mmpm_prop))
  {
    MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
        "LSM MMPM chip specific CPP factors failed, using default CPP!");
  }


  /* Unique name for each session */
  client_name[CLIENT_NAME_SESSION_ID_OFFSET] += session_id;

  regParam.rev             = MMPM_REVISION;
  regParam.instanceId      = MMPM_CORE_INSTANCE_0;
  regParam.pwrCtrlFlag     = PWR_CTRL_NONE;
  regParam.callBackFlag    = CALLBACK_NONE;
  regParam.MMPM_Callback   = NULL;
  regParam.cbFcnStackSize  = 0;

  regParam.coreId          = MMPM_CORE_ID_LPASS_ADSP;
  regParam.pClientName     = client_name;

  mmpm_info_ptr->client_id = MMPM_Register_Ext(&regParam);
  if(ADSPPM_NULL_HANDLE == mmpm_info_ptr->client_id)
  {
    MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
        "MMPM_Register_Ext for LSM ADSP failed");
    return ADSP_EFAILED;
  }
  else
  {
    MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,
          "MMPM client for LSM ADSP session id [%u] is [%u]",
          session_id, mmpm_info_ptr->client_id);
  }

  //Register for client class as Audio to ADSPPM.
  MmpmClientClassType class_type = MMPM_AUDIO_CLIENT_CLASS;
  MmpmParameterConfigType param_cfg;
  param_cfg.pParamConfig = (void*)&class_type;
  param_cfg.paramId = MMPM_PARAM_ID_CLIENT_CLASS;
  MMPM_STATUS status = MMPM_SetParameter(mmpm_info_ptr->client_id, &param_cfg);
  if(status != MMPM_STATUS_SUCCESS)
  {
    MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MMPM_SetParameter class type failed for LSM ADSP session id [%u] "
                                         "with client id %lu with status %lu ", session_id, 
                                         mmpm_info_ptr->client_id, (uint32_t)status);
  }
  else
  {
    MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MMPM_SetParameter class type success for LSM ADSP session id [%u] "
                                         "with client id %lu", session_id, mmpm_info_ptr->client_id);
  }

  //Register for DCVS up only
  //LSM core/bus votes are subject to automatic up adjustment by DCVS
  MmpmDcvsParticipationType dcvs_participation;
  dcvs_participation.enable = TRUE;
  dcvs_participation.enableOpt = MMPM_DCVS_ADJUST_ONLY_UP;
  param_cfg.pParamConfig = (void*)&dcvs_participation;
  param_cfg.paramId = MMPM_PARAM_ID_DCVS_PARTICIPATION;
  status = MMPM_SetParameter(mmpm_info_ptr->client_id, &param_cfg);
  if(status != MMPM_STATUS_SUCCESS)
  {
    MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MMPM_SetParameter DCVS participation for Only UP failed for LSM ADSP "
                                         "session id [%u] with client id %lu with status %lu ", session_id,
          mmpm_info_ptr->client_id, (uint32_t)status);
  }
  else
  {
    MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MMPM_SetParameter DCVS participation for Only UP success for LSM ADSP "
                                         "session id [%u] with client id %lu", session_id, mmpm_info_ptr->client_id);
  }

  return ADSP_EOK;
}

/**
 * De-registers from mmpm using mmpm client info
 * @param mmpm_info_ptr - lsm session mmpm state variables
 *
 * @return adsp result status
 */
ADSPResult lsm_mmpm_deregister(lsm_mmpm_info_t *mmpm_info_ptr)
{
  MMPM_STATUS status = MMPM_STATUS_SUCCESS;

  if(NULL == mmpm_info_ptr)
  {
    MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "lsm mmpm dereg: NULL arg");
    return ADSP_EBADPARAM;
  }

  if (ADSPPM_NULL_HANDLE != mmpm_info_ptr->client_id)
  {
    status = MMPM_Deregister_Ext(mmpm_info_ptr->client_id);
    if (MMPM_STATUS_SUCCESS != status)
    {
      MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
            "MMPM_Deregister_Ext for LSM ADSP client [%u] failed [0x%x]",
            mmpm_info_ptr->client_id, status);
    }
    else
    {
      mmpm_info_ptr->client_id = ADSPPM_NULL_HANDLE;
    }
  }
  return ADSP_EOK;
}

/**
 * Vote for KPPS
 * @param mmpm_info_ptr - lsm session mmpm state variables
 * @param topo_ptr - lsm session topology state variables.
 *
 * @return adsp result status
 */
static ADSPResult lsm_mmpm_mcps_vote(lsm_mmpm_info_t* mmpm_info_ptr,
                                     uint32_t mcps,
                                     uint32_t kbps)
{
  ADSPResult result = ADSP_EOK;
  uint16_t j;
  uint16_t bw_idx = LSM_MMPM_BW_MAX_INDEX -1;
  const lpasshwio_prop_lsm_mmpm_struct_t *dev_mmpm_prop_ptr = NULL;

  dev_mmpm_prop_ptr = &mmpm_info_ptr->dev_mmpm_prop;

  if(mcps > 0)
  {
    /* Retrieving the proper BW level based on total bw */
    for(j=0;j<LSM_MMPM_BW_MAX_INDEX;j++)
    {
      if(dev_mmpm_prop_ptr->bw[j] > kbps)
      {
        bw_idx = j;
        break;
      }
    }

    /* Multiplying CPP factor - cache miss compensation */
    mcps = (mcps * dev_mmpm_prop_ptr->cpp[bw_idx]) / LSM_MMPM_COMMON_DENOM;
  }

  MMPM_STATUS mmpmRes = MMPM_STATUS_SUCCESS;
  MmpmRscExtParamType reqParam;
  reqParam.apiType = MMPM_API_TYPE_SYNC;

  //Requesting 0 will be equivalent to releasing particular resource
  MmpmMipsReqType mipsReq;
  mipsReq.codeLocation         = MMPM_BW_PORT_ID_NONE;     //we need to provide code location only if reqOperation==MMPM_MIPS_REQUEST_CPU_CLOCK_AND_BW
  mipsReq.mipsPerThread        = mcps;
  mipsReq.mipsTotal            = mcps;
  mipsReq.reqOperation         = MMPM_MIPS_REQUEST_CPU_CLOCK_ONLY;

  MmpmRscParamType rscParam[LSM_NUM_REQUEST_MAX];
  MMPM_STATUS      retStats[LSM_NUM_REQUEST_MAX];
  rscParam[0].rscId                   = MMPM_RSC_ID_MIPS_EXT;
  rscParam[0].rscParam.pMipsExt       = &mipsReq;

  reqParam.numOfReq                   = LSM_NUM_REQUEST_MAX;
  reqParam.pExt                       = NULL;     //for future
  reqParam.pReqArray                  = rscParam;
  reqParam.pStsArray                  = retStats; //for most cases mmpmRes is good enough, need not check this array.
  reqParam.reqTag                     = 0;     //for async only


  if (mcps > 0)
  {
    mmpmRes = MMPM_Request_Ext(mmpm_info_ptr->client_id, &reqParam);
  }
  else
  {
    mmpmRes = MMPM_Release_Ext(mmpm_info_ptr->client_id, &reqParam);
  }
  MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO,
        "LSM client [%u] requesting ADSPPM "
        "MIPs per thread = %lu, total MIPs = %lu",
        mmpm_info_ptr->client_id, mipsReq.mipsPerThread, mipsReq.mipsTotal);

  if(MMPM_STATUS_SUCCESS != mmpmRes)
  {
    MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
          "LSM client [%u] MMPM_Request for MIPS failed [%x]",
          mmpm_info_ptr->client_id, mmpmRes);
    result = ADSP_EFAILED;
  }
  else
  {
    MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,
          "LSM client [%u] MMPM Request for LSM MIPS success",
          mmpm_info_ptr->client_id);
    /* Update debug variables */
    mmpm_info_ptr->dbg_current_mips_per_thread = mcps;
  }

  return result;
}


/**
 * Vote for BPS
 * @param mmpm_info_ptr - lsm session mmpm state variables
 * @param topo_ptr - lsm session topology state variables.
 *
 * @return adsp result status
 */
static ADSPResult lsm_mmpm_kbps_vote(lsm_mmpm_info_t *mmpm_info_ptr,
                                     uint32_t kbps)
{
  MMPM_STATUS mmpmRes = MMPM_STATUS_SUCCESS;
  MmpmRscExtParamType reqParam;
  reqParam.apiType = MMPM_API_TYPE_SYNC;

  uint32_t numBw = 0, adsp_ddr_ind;
  MmpmGenBwValType bwArr[LSM_NUM_REQUEST_MAX];

  //previously MMPM_AUD_BW_TYPE_CPU , MMPM_AUD_BW_PORT_EXT_EBI1 ,MMPM_AUD_BW_PORT_QDSP6TCM
  adsp_ddr_ind = numBw;
  bwArr[numBw].busRoute.masterPort                 = MMPM_BW_PORT_ID_ADSP_MASTER;
  bwArr[numBw].busRoute.slavePort                  = MMPM_BW_PORT_ID_DDR_SLAVE;
  bwArr[numBw].bwValue.busBwValue.bwBytePerSec     = kbps*128; //(*1024/8)
  bwArr[numBw].bwValue.busBwValue.usagePercentage  = 100;
  bwArr[numBw].bwValue.busBwValue.usageType        = MMPM_BW_USAGE_LPASS_DSP;
  numBw++; //1

  MSG_4(MSG_SSID_QDSP6, DBG_HIGH_PRIO,
        "LSM  client [%u] requesting MMPM "
        "(BW bytesPerSec, Usage perc): ADSP DDR (0x%lx%lx), Usage ( %ld)",
        mmpm_info_ptr->client_id,
        (uint32_t)(bwArr[adsp_ddr_ind].bwValue.busBwValue.bwBytePerSec>>32),
        (uint32_t)bwArr[adsp_ddr_ind].bwValue.busBwValue.bwBytePerSec,
        (uint32_t)bwArr[adsp_ddr_ind].bwValue.busBwValue.usagePercentage);


  MmpmGenBwReqType bwReq;
  bwReq.numOfBw            = numBw;
  bwReq.pBandWidthArray    = bwArr;

  MmpmRscParamType rscParam[LSM_NUM_REQUEST_MAX];
  MMPM_STATUS      retStats[LSM_NUM_REQUEST_MAX];
  rscParam[0].rscId                   = MMPM_RSC_ID_GENERIC_BW;
  rscParam[0].rscParam.pGenBwReq      = &bwReq;

  reqParam.numOfReq                   = LSM_NUM_REQUEST_MAX;
  reqParam.pExt                       = NULL;     //for future
  reqParam.pReqArray                  = rscParam;
  reqParam.pStsArray                  = retStats; //for most cases mmpmRes is good enough, need not check this array.
  reqParam.reqTag                     = 0;     //for async only

  //for BW any client Id is fine (independent if DML, or some other). Actually client id is specific only for power, register access.
  if(kbps > 0)
  {
    mmpmRes = MMPM_Request_Ext(mmpm_info_ptr->client_id, &reqParam);
  }
  else
  {
    mmpmRes = MMPM_Release_Ext(mmpm_info_ptr->client_id, &reqParam);
  }

  ADSPResult result = ADSP_EOK;
  if(MMPM_STATUS_SUCCESS != mmpmRes)
  {
    MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
          "LSM client [%u] MMPM Request for BW failed [%x]",
          mmpm_info_ptr->client_id, mmpmRes);
    result = ADSP_EFAILED;
  }
  else
  {
    MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,
          "LSM client [%u] MMPM Request for LSM BW success",
          mmpm_info_ptr->client_id);
    /* Update debug variables */
    mmpm_info_ptr->dbg_current_bw_bytes_per_sec = kbps*128;
  }
  return result;
}


ADSPResult lsm_mmpm_config(lsm_mmpm_info_t* mmpm_info_ptr,
                           topo_t* topo_ptr,
                           lsm_mmpm_vote_t vote)
{
  ADSPResult result = ADSP_EOK;
  uint32_t mcps = 0;
  uint32_t kbps = 0;

  if((NULL == mmpm_info_ptr)                         ||
     (NULL == topo_ptr && LSM_MMPM_VOTE_NONE != vote)  )
  {
    MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
          "lsm mmpm kpps vote null param mmpm_info %p, topo %p, vote %d",
          mmpm_info_ptr, topo_ptr, vote);
    return ADSP_EBADPARAM;
  }

  switch(vote)
  {
    case LSM_MMPM_VOTE_NONE:
    {
      result = lsm_mmpm_mcps_vote(mmpm_info_ptr,0,0);
      result |= lsm_mmpm_kbps_vote(mmpm_info_ptr,0);
      break;
    }
    case LSM_MMPM_VOTE_UPDATE_BPS_ONLY:
    {
      kbps = ceil_div(topo_ptr->bps,1000);
      result = lsm_mmpm_kbps_vote(mmpm_info_ptr,kbps);
      break;
    }
    case LSM_MMPM_VOTE_UPDATE_KPPS_ONLY:
    {
      mcps = ceil_div(topo_ptr->kpps,1000);
      kbps = ceil_div(topo_ptr->bps,1000);
      result = lsm_mmpm_mcps_vote(mmpm_info_ptr,mcps,kbps);
      break;
    }
    case LSM_MMPM_VOTE_ALL:
    {
      mcps = ceil_div(topo_ptr->kpps,1000);
      kbps = ceil_div(topo_ptr->bps,1000);
      result = lsm_mmpm_mcps_vote(mmpm_info_ptr,mcps,kbps);
      result |= lsm_mmpm_kbps_vote(mmpm_info_ptr,kbps);
      break;
    }
    default:
    {
      MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
            "Unsupported vote %d by lsm client [%u]",
            vote, mmpm_info_ptr->client_id);
      result = ADSP_EUNSUPPORTED;
      break;
    }
  }
  return result;
}

#else

/** Stub Functions */
ADSPResult lsm_mmpm_register(lsm_mmpm_info_t *mmpm_info_ptr,
                             uint16_t session_id)
{
  return ADSP_EOK;
}

/** Stub Functions */
ADSPResult lsm_mmpm_deregister(lsm_mmpm_info_t *mmpm_info_ptr)
{
  return ADSP_EOK;
}

/** Stub Functions */
ADSPResult lsm_mmpm_config(lsm_mmpm_info_t* mmpm_info_ptr,
                           topo_t* topo_ptr,
                           lsm_mmpm_vote_t vote)
{
  return ADSP_EOK;
}

#endif //ADSPPM_INTEGRATION

