/*
  Copyright (C) 2015 QUALCOMM Technologies, Inc.
  All rights reserved.
  Qualcomm Technologies, Inc. Confidential and Proprietary.

  $Header:
  $Author: pwbldsvc $
*/

#include "adsp_vpm_api.h"
#include "mmpm.h"

#include "vss_private_if.h"

/****************************************************************************
 * CVD DEVICE CONFIGURATION DATA                                            *
 ****************************************************************************/

/**
  This file along with the cvd_devcfg.xml (in the same folder) stores the
  following data:

  1. Voice use case <-> clock value table Floor Core Clock as follows:

     Tx Topology and  | 1 voice session   | 1 voice session     | Any other number     |
     VFR mode         | with 1 NB stream  | with 1 WB/WB+       | of voice sessions    |
                      | and/or 1 NB       | stream and/or       | or any other number  |
                      | vocproc           | 1 WB/WB+ vocproc    | of streams/vocprocs  |
     -----------------------------------------------------------------------------------
     NONE +           |  384MHz           |  384MHz             |  576MHz              |
     SOFT VFR mode    |                   |                     |                      |
     -----------------------------------------------------------------------------------
     NONE +           |  384MHz           |  576MHz             |  576MHz              |
     HARD VFR mode    |                   |                     |                      |
     -----------------------------------------------------------------------------------
     SM_ECNS +        |  384MHz           |  384MHz             |  576MHz              |
     SOFT VFR mode    |                   |                     |                      |
     -----------------------------------------------------------------------------------
     SM_ECNS +        |  384MHz           |  576MHz             |  576MHz              |
     HARD VFR mode    |                   |                     |                      |
     -----------------------------------------------------------------------------------
     DM_FLUENCE +     |  384MHz           |  384MHz             |  576MHz              |
     SOFT VFR mode    |                   |                     |                      |
     -----------------------------------------------------------------------------------
     DM_FLUENCE +     |  384MHz           |  576MHz             |  576MHz              |
     HARD VFR mode    |                   |                     |                      |
     -----------------------------------------------------------------------------------
     SM_FLUENCEV5 +   |  384MHz           |  576MHz             |  576MHz              |
     any VFR mode     |                   |                     |                      |
     -----------------------------------------------------------------------------------
     SM_CUSTOM TOPOs+ |  384MHz           |  576MHz             |  576MHz              |
     any VFR mode     |                   |                     |                      |
     -----------------------------------------------------------------------------------
     DM_VPECNS +      |  384MHz           |  384MHz             |  576MHz              |
     SOFT VFR mode    |                   |                     |                      |
     -----------------------------------------------------------------------------------
     DM_VPECNS +      |  384MHz           |  576MHz             |  576MHz              |
     HARD VFR mode    |                   |                     |                      |
     -----------------------------------------------------------------------------------
     DM_FLUENCEV5 +   |  384MHz           |  576MHz             |  576MHz              |
     any VFR mode     |                   |                     |                      |
     -----------------------------------------------------------------------------------
     DM_CUSTOM TOPOs+ |  384MHz           |  576MHz             |  576MHz              |
     any VFR mode     |                   |                     |                      |
     -----------------------------------------------------------------------------------
     DM_FV5 BROADSIDE +| 384MHz           |  576MHz             |  576MHz              |
     any VFR mode     |                   |                     |                      |
     -----------------------------------------------------------------------------------
     QM_FLUENCE_PRO + |  576MHz           |  576MHz             |  576MHz              |
     any VFR mode     |                   |                     |                      |
     -----------------------------------------------------------------------------------
     QM_FLUENCE_PROV2 |  576MHz           |  576MHz             | 576MHz               |
     any VFR mode     |                   |                     |                      |
     -----------------------------------------------------------------------------------
     QM_CUSTOM TOPOs +|  576MHz           |  576MHz             | 576MHz               |
     any VFR mode     |                   |                     |                      |
     -----------------------------------------------------------------------------------





  2. MMPM core information which is required for MMPM registration:
     - Core ID = MMPM_CORE_ID_LPASS_ADSP
     - Instance ID = MMPM_CORE_INSTANCE_0

  3. MMPM Bus Bandwidth requirement:
     - Number of bus bandwidth requirement = 1
     - Bus route master port = MMPM_BW_PORT_ID_ADSP_MASTER
     - Bus route slave port = MMPM_BW_PORT_ID_DDR_SLAVE
     - Usage = ( 150 << 20 ) Bytes per second
     - Usage percentage = 30
     - Usage type = MMPM_BW_USAGE_LPASS_DSP

   4. Sleep latency in microsends, which means during voice call, the ADSP
      should not go to sleep (power collapse) unless it can wake up within
      this latency. (this information is directly stored in the
      cvd_devcfg.xml).
      - 20

   5. Number of clock cycles required to execute 1000 instruction packets (
      this information is directly stored in the cvd_devcfg.xml).
      - 2400
*/

/* Use case:
   { Number of voice session,
     Number of NB streams,  
     Number of WB streams,
     Number of SWB streams, 
     Number of FB plus streams,     
     Number of NB vocprocs,  
     Number of WB vocprocs,
     Number of SWB vocprocs, 
     Number of FB plus vocprocs,
     Tx topology ID, 
     Rx topology ID, 
     Media ID, 
     VFR mode 
   } 
*/

cvd_devcfg_voice_use_case_na_values_t cvd_devcfg_voice_use_case_na_values =
  { 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFF };

static cvd_devcfg_supported_voice_use_case_t cvd_devcfg_clock_level_0_use_cases[ 25 ] =
  {
    { 1, 1, 0, 0, 0, 1, 0, 0, 0, VPM_TX_NONE,             0xFFFFFFFF, 0xFFFFFFFF, 0xFFFF },
    { 1, 1, 0, 0, 0, 1, 0, 0, 0, VPM_TX_SM_ECNS,          0xFFFFFFFF, 0xFFFFFFFF, 0xFFFF },
    { 1, 1, 0, 0, 0, 1, 0, 0, 0, VPM_TX_DM_FLUENCE,       0xFFFFFFFF, 0xFFFFFFFF, 0xFFFF },
    { 1, 1, 0, 0, 0, 1, 0, 0, 0, VPM_TX_SM_FLUENCEV5,     0xFFFFFFFF, 0xFFFFFFFF, 0xFFFF },
    { 1, 1, 0, 0, 0, 1, 0, 0, 0, VPM_TX_CUSTOM_SM_ECNS_1, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFF },
    { 1, 1, 0, 0, 0, 1, 0, 0, 0, VPM_TX_CUSTOM_SM_ECNS_2, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFF },
    { 1, 1, 0, 0, 0, 1, 0, 0, 0, VPM_TX_CUSTOM_SM_ECNS_3, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFF },
    { 1, 1, 0, 0, 0, 1, 0, 0, 0, VPM_TX_DM_VPECNS,        0xFFFFFFFF, 0xFFFFFFFF, 0xFFFF },
    { 1, 1, 0, 0, 0, 1, 0, 0, 0, VPM_TX_DM_FLUENCEV5,     0xFFFFFFFF, 0xFFFFFFFF, 0xFFFF },
    { 1, 1, 0, 0, 0, 1, 0, 0, 0, VPM_TX_DM_FLUENCEV5_BROADSIDE, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFF },
    { 1, 1, 0, 0, 0, 1, 0, 0, 0, VPM_TX_CUSTOM_DM_ECNS_1, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFF },
    { 1, 1, 0, 0, 0, 1, 0, 0, 0, VPM_TX_CUSTOM_DM_ECNS_2, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFF },
    { 1, 1, 0, 0, 0, 1, 0, 0, 0, VPM_TX_CUSTOM_DM_ECNS_3, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFF },
    { 1, 1, 0, 0, 0, 0, 1, 0, 0, VPM_TX_NONE, 0xFFFFFFFF, VSS_ICOMMON_VFR_MODE_SOFT, 0xFFFF },
    { 1, 0, 1, 0, 0, 1, 0, 0, 0, VPM_TX_NONE, 0xFFFFFFFF, VSS_ICOMMON_VFR_MODE_SOFT, 0xFFFF },
    { 1, 0, 1, 0, 0, 0, 1, 0, 0, VPM_TX_NONE, 0xFFFFFFFF, VSS_ICOMMON_VFR_MODE_SOFT, 0xFFFF },
    { 1, 1, 0, 0, 0, 0, 1, 0, 0, VPM_TX_SM_ECNS, 0xFFFFFFFF, VSS_ICOMMON_VFR_MODE_SOFT, 0xFFFF },
    { 1, 0, 1, 0, 0, 1, 0, 0, 0, VPM_TX_SM_ECNS, 0xFFFFFFFF, VSS_ICOMMON_VFR_MODE_SOFT, 0xFFFF },
    { 1, 0, 1, 0, 0, 0, 1, 0, 0, VPM_TX_SM_ECNS, 0xFFFFFFFF, VSS_ICOMMON_VFR_MODE_SOFT, 0xFFFF },
    { 1, 1, 0, 0, 0, 0, 1, 0, 0, VPM_TX_DM_VPECNS, 0xFFFFFFFF, VSS_ICOMMON_VFR_MODE_SOFT, 0xFFFF },
    { 1, 0, 1, 0, 0, 1, 0, 0, 0, VPM_TX_DM_VPECNS, 0xFFFFFFFF, VSS_ICOMMON_VFR_MODE_SOFT, 0xFFFF },
    { 1, 0, 1, 0, 0, 0, 1, 0, 0, VPM_TX_DM_VPECNS, 0xFFFFFFFF, VSS_ICOMMON_VFR_MODE_SOFT, 0xFFFF },
    { 1, 1, 0, 0, 0, 0, 1, 0, 0, VPM_TX_DM_FLUENCE, 0xFFFFFFFF, VSS_ICOMMON_VFR_MODE_SOFT, 0xFFFF },
    { 1, 0, 1, 0, 0, 1, 0, 0, 0, VPM_TX_DM_FLUENCE, 0xFFFFFFFF, VSS_ICOMMON_VFR_MODE_SOFT, 0xFFFF },
    { 1, 0, 1, 0, 0, 0, 1, 0, 0, VPM_TX_DM_FLUENCE, 0xFFFFFFFF, VSS_ICOMMON_VFR_MODE_SOFT, 0xFFFF }
  };

static cvd_devcfg_supported_voice_use_case_t cvd_devcfg_clock_level_1_use_cases[ 1 ] =
  {
    { 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFF }
  };

static cvd_devcfg_clock_level_t cvd_devcfg_clock_levels[ 2 ] =
  {
    { 384000000, 25, cvd_devcfg_clock_level_0_use_cases },
    { 576000000, 1, cvd_devcfg_clock_level_1_use_cases }
  };

cvd_devcfg_clock_table_t cvd_devcfg_clock_table =
  { 2, cvd_devcfg_clock_levels };

cvd_devcfg_mmpm_core_info_t cvd_devcfg_mmpm_core_info =
  { MMPM_CORE_ID_LPASS_ADSP, MMPM_CORE_INSTANCE_0 };

static cvd_devcfg_supported_voice_use_case_t cvd_devcfg_bw_level_0_use_cases[ 6 ] = 
  {
    { 1, 1, 0, 0, 0, 1, 0, 0, 0, VPM_TX_NONE,         0xFFFFFFFF, 0xFFFFFFFF, 0xFFFF },
    { 1, 1, 0, 0, 0, 1, 0, 0, 0, VPM_TX_SM_ECNS,      0xFFFFFFFF, 0xFFFFFFFF, 0xFFFF },
    { 1, 1, 0, 0, 0, 1, 0, 0, 0, VPM_TX_DM_FLUENCE,   0xFFFFFFFF, 0xFFFFFFFF, 0xFFFF },
    { 1, 1, 0, 0, 0, 1, 0, 0, 0, VPM_TX_SM_FLUENCEV5, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFF },
    { 1, 1, 0, 0, 0, 1, 0, 0, 0, VPM_TX_DM_VPECNS,    0xFFFFFFFF, 0xFFFFFFFF, 0xFFFF },
    { 1, 1, 0, 0, 0, 1, 0, 0, 0, VPM_TX_DM_FLUENCEV5, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFF }
  };

static cvd_devcfg_supported_voice_use_case_t cvd_devcfg_bw_level_1_use_cases[ 1 ] = 
  {  
    { 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFF }
  };

static cvd_devcfg_mmpm_bw_value_t cvd_devcfg_mmpm_bw_values[ 2 ] =
  {
    {
      {
        .busRoute = { MMPM_BW_PORT_ID_ADSP_MASTER, MMPM_BW_PORT_ID_DDR_SLAVE },
        .bwValue.busBwValue = { ( 45 << 20 ), 100, MMPM_BW_USAGE_LPASS_DSP }
      },
        6,
        cvd_devcfg_bw_level_0_use_cases
    },
  {
    {
        .busRoute = { MMPM_BW_PORT_ID_ADSP_MASTER, MMPM_BW_PORT_ID_DDR_SLAVE },
        .bwValue.busBwValue = { ( 80 << 20 ), 100, MMPM_BW_USAGE_LPASS_DSP }
      },
        1,
        cvd_devcfg_bw_level_1_use_cases
    }
  };

cvd_devcfg_mmpm_bw_table_t cvd_devcfg_parser_bw_table = 
  { 2, cvd_devcfg_mmpm_bw_values };

