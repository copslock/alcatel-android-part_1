#ifndef __CVS_MAILBOX_TIMER_API_I_H__
#define __CVS_MAILBOX_TIMER_API_I_H__

/*
  Copyright (C) 2014 QUALCOMM Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  $Header: //components/rel/avs.adsp/2.7.1.c4/vsd/common/cvd/cvs/inc/cvs_mailbox_timer_api_i.h#1 $
  $Author: pwbldsvc $
*/

#include "apr_comdef.h"

/* This file contains the definitions for the timer used for mailbox vocoder
 * packet exchange. The actual timer used is product requirement specific. The
 * current timer used is AVTimer.
 *
 * Current limitation when using AVTimer. The following timer APIs in this file
 * can only be used after a voice call is started (i.e. modem and APPS client
 * issued start voice to CVD).
 * - cvs_mailbox_timer_get_time
 * - cvs_mailbox_timer_start_absolute
 * - cvs_mailbox_timer_create
 * - cvs_mailbox_timer_destroy
 * This is because CVD only votes for the AVTimer HW resources after a voice
 * call is started and release the AVTimer HW resources after a voice call is
 * ended in order to allow ADSP power collapse when there is no voice call.
 */

/****************************************************************************
 * DEFINITIONS                                                              *
 ****************************************************************************/

typedef void ( *cvs_mailbox_timer_cb_fn_t ) ( void* client_data );


/****************************************************************************
 * PROTOTYPES                                                               *
 ****************************************************************************/

/** 
  Initialize the CVS mailbox timer driver. Must be called before calling any
  other CVS mailbox timer APIs.

  @return APR_EOK when successful.
*/
APR_INTERNAL int32_t cvs_mailbox_timer_init ( void );


/** 
  Deinitialize the CVS mailbox timer driver.

  @return APR_EOK when successful.
*/
APR_INTERNAL int32_t cvs_mailbox_timer_deinit ( void );


/**
  Creates a timer.

  @param[in] timer_cb Client supplied timer callback function which will be
                      called when the timer expires.

  @param[in] client_data Client supplied timer callback data.

  @param[out] ret_timer_handle Return timer handle.

  @return APR_EOK when successful.
*/
APR_INTERNAL int32_t cvs_mailbox_timer_create (
  cvs_mailbox_timer_cb_fn_t timer_cb,
  void* client_data,
  uint32_t* ret_timer_handle
);


/**
  Destroys a timer.

  @param[in] timer_handle Timer handle.

  @return APR_EOK when successful.
*/
APR_INTERNAL int32_t cvs_mailbox_timer_destroy (
  uint32_t timer_handle
);


/**
  Starts an absolute timer. The timer will expire at the client specified
  absolute timestamp, upon which the client's timer_cb will be called.

  @param[in] timer_handle Timer handle.

  @param[in] expiry_time_us Timer expiry time in absolute time in microseconds.

  @return APR_EOK when successful.
*/
APR_INTERNAL int32_t cvs_mailbox_timer_start_absolute (
  uint32_t timer_handle,
  uint64_t expiry_time_us
);


/**
  Cancels an absolute timer.

  @param[in] timer_handle Timer handle.

  @return APR_EOK when successful.
*/
APR_INTERNAL int32_t cvs_mailbox_timer_cancel_absolute (
  uint32_t timer_handle
);


/**
  Gets the current time in microseconds.

  @param[in] timer_handle Timer handle.

  @param[out] ret_time_us Current time in microseconds.

  @return APR_EOK when successful.
*/
APR_INTERNAL int32_t cvs_mailbox_timer_get_time (
  uint64_t* ret_time_us
);

#endif /* __CVS_MAILBOX_TIMER_API_I_H__ */

