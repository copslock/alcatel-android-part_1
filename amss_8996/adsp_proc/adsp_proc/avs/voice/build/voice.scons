#===============================================================================
#
# AVS AU
#
# GENERAL DESCRIPTION
#    Build script
#
# Copyright (c) 2009-2009 by Qualcomm Technologies, Incorporated.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#
#-------------------------------------------------------------------------------
#
#  $Header: $
#  $DateTime: $
#  $Author:  $
#  $Change:  $
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
#
#===============================================================================
Import('env')
import os


env.Append(CFLAGS = ' -Werror ')
env.Append(CPPFLAGS = ' -Werror ')
env.Append(ASMFLAGS = ' -Werror ')

env.Append(CFLAGS = ' -DVOICE_OBFUSCATE_NAMES ')
env.Append(CPPFLAGS = ' -DVOICE_OBFUSCATE_NAMES ')
env.Append(ASMFLAGS = ' -DVOICE_OBFUSCATE_NAMES ')   
   
env.PublishProtectedApi('AVS',[
   '${AVS_ROOT}/voice/algos/cng/inc',
   '${AVS_ROOT}/voice/algos/cross_fade/inc',
   '${AVS_ROOT}/voice/algos/ctm/inc',
   '${AVS_ROOT}/voice/algos/dtmf_detect/dtmf_det_lib/inc',
   '${AVS_ROOT}/voice/algos/dtmf_detect/capiv2_dtmf_det/inc',
   '${AVS_ROOT}/voice/algos/fir_filter/capi_v2_fir/inc',
   '${AVS_ROOT}/voice/algos/iir_filter/capi_v2_hpf/inc',
   '${AVS_ROOT}/voice/algos/iir_filter/capi_v2_iir_mic1/inc',
   '${AVS_ROOT}/voice/algos/iir_filter/capi_v2_iir_mic2/inc',
   '${AVS_ROOT}/voice/algos/iir_filter/capi_v2_iir_mic3/inc',
   '${AVS_ROOT}/voice/algos/iir_filter/capi_v2_iir_mic4/inc',
   '${AVS_ROOT}/voice/algos/iir_filter/capi_v2_iir_utils/inc',
   '${AVS_ROOT}/voice/algos/iir_filter/capi_v2_gain_dep_iir/inc',
   '${AVS_ROOT}/voice/algos/pbe/capi_v2_voice_pbe/inc',
   '${AVS_ROOT}/voice/algos/avc_rve/capi_v2_avc_rve_rx/inc',
   '${AVS_ROOT}/voice/algos/avc_rve/capi_v2_avc_rve_tx/inc',
   '${AVS_ROOT}/voice/algos/mmecns/capiv2_mmecns/inc', 
   '${AVS_ROOT}/voice/algos/resample_by_two_tobe_deleted/inc',
   '${AVS_ROOT}/voice/algos/sample_slip/sample_slip_lib/inc',
   '${AVS_ROOT}/voice/algos/sample_slip/capi_v2_sample_slip/inc',
   '${AVS_ROOT}/voice/algos/slow_talk/inc',
   '${AVS_ROOT}/voice/algos/time_warping/inc',
   '${AVS_ROOT}/voice/algos/tty/inc',
   '${AVS_ROOT}/voice/algos/ltetty/inc',
   '${AVS_ROOT}/voice/algos/vfe_filters/capi_v2_vfe_filters/inc',
   '${AVS_ROOT}/voice/algos/vocoder_amrwb/inc',
   '${AVS_ROOT}/voice/algos/vocoder_eamr/inc',
   '${AVS_ROOT}/voice/algos/vocoder_efr/inc',
   '${AVS_ROOT}/voice/algos/vocoder_evrc/inc',
   '${AVS_ROOT}/voice/algos/vocoder_fourgv/inc',
   '${AVS_ROOT}/voice/algos/vocoder_fr/inc',
   '${AVS_ROOT}/voice/algos/vocoder_g711/inc',
   '${AVS_ROOT}/voice/algos/vocoder_g729ab/inc',
   '${AVS_ROOT}/voice/algos/vocoder_hr/inc',
   '${AVS_ROOT}/voice/algos/vocoder_v13k/inc',
   '${AVS_ROOT}/voice/algos/vocoder_evs/evs_lib/inc',
   '${AVS_ROOT}/voice/algos/volume_control/capi_v2_soft_mute/inc',
   '${AVS_ROOT}/voice/algos/volume_control/capi_v2_volume_control/inc',
   '${AVS_ROOT}/voice/algos/volume_control/volume_control_lib/inc',
   '${AVS_ROOT}/voice/algos/echo_canceller/capi_v2_ecns/inc',
   '${AVS_ROOT}/voice/algos/echo_canceller/capi_v2_ec_rx/inc',
   '${AVS_ROOT}/voice/algos/fluence_voiceplus/capi_v2_dmecns/inc',
   '${AVS_ROOT}/voice/algos/fluence_voiceplus/capi_v2_vpecns/inc',   
   '${AVS_ROOT}/voice/algos/wide_voice/inc',
   '${AVS_ROOT}/voice/algos/voice_resampler/voice_resampler_lib/inc',
   '${AVS_ROOT}/voice/algos/voice_resampler/capi_v2_voice_resampler/inc',
   '${AVS_ROOT}/voice/algos/vocoder_capi/inc',
   '${AVS_ROOT}/voice/algos/wide_voice_v2/inc',
   #TCT-NB Tianhongwei add for NXP Voice solution 2016/01/04
   '${AVS_ROOT}/voice/algos/LVVEFQ/inc',
   '${AVS_ROOT}/voice/algos/LVVEFQ/capi_v2_lvvefq/inc',
   '${AVS_ROOT}/voice/algos/LVVEFQ_SSRC/inc',
   #TCT-NB Tianhongwei end
   '${AVS_ROOT}/voice/services/voice_common/inc',
   '${AVS_ROOT}/voice/services/voice_dec/src'  
   ])

env.PublishProtectedApi('VOICE_CAPI_V2_SAMPLE_SLIP',[
   '${AVS_ROOT}/voice/algos/sample_slip/capi_v2_sample_slip/inc'
   ])
env.RequireRestrictedApi('AUDPROC_RESAMPLER')
env.RequireRestrictedApi('AUDPROC_GAINCONTROL')
env.RequireRestrictedApi('AUDPROC_EANS')
env.RequireRestrictedApi('AUDPROC_MULTIBANDIIR')
env.RequireRestrictedApi('AUDPROC_SAMPLESLIP')
env.RequireRestrictedApi('CVD_PROTECTED')
env.RequireRestrictedApi('MODULE_INTERFACES_API')
env.RequireRestrictedApi('MODULE_INTERFACES_UTILS')
env.RequirePublicApi(['ADSP_AMDB'])

env.LoadSoftwareUnits()
