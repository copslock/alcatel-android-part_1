#ifndef __4GV_ENC_API_H
#define __4GV_ENC_API_H

#define FOURGV_ENC_UNSUPPORTED 0xFFFFFFFFLL
#define LOOKAHEAD_LEN            80
#define SPEECH_BUFFER_LEN        160
#define FrameSize   160		/* CELP frame size */
#define FSIZE FrameSize
// TTY
extern "C" {
    #include "tty_api.h"
}

class DTMF_fx;
class fgvEncoder;

class FourGVEncode
{

   public:

   FourGVEncode(int &result);
   FourGVEncode();
   ~FourGVEncode();
   void FGVEncodeDestructor(); //this member function is called from Venc wrapper, which compliments the placement new functionality in destructor
   void encoder(short int *inbuf16, short int *outbuf16,short int outbufsize, short int ttyEncflag,TTYEncStruct* pTTYEncStruct);
   void init_encoder();
   void set_encoder_params();
   
   short int buf_fx[SPEECH_BUFFER_LEN*2+LOOKAHEAD_LEN];
   short int*     buf16P;
   short int     *bufP_fx;
   char dim;
   fgvEncoder       *pFgvEncoderObj;

   short operatingpoint_bck;
   short NS;
   short first_time;
   short int   max_rate;   
   short int   min_rate;
   short int   ibuf_len;
   short int   obuf_len;
   short int avg_rate_target;
   short int avg_rate_control;
   int Fsop;
   int Fsinp;
   int operating_point;
   int dtx;   
   int joint_source_modem_dtx;
};
extern int fourgv_repacking_dsp_to_mvs(char *iPacket);
extern int get_fourgv_enc_static_mem_size();
void init_fourgv_enc_static_mem(void *mem_ptr, FourGVEncode **four_gv_encode_ptr);

#endif  //__4GV_ENC_API_H

