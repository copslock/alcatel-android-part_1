/****************************************************************************************/
/*  Copyright (c) 2013-2014 NXP Software. All rights are reserved.                      */
/*  Reproduction in whole or in part is prohibited without the prior                    */
/*  written consent of the copyright owner.                                             */
/*                                                                                      */
/*  This software and any compilation or derivative thereof is and                      */
/*  shall remain the proprietary information of NXP Software and is                     */
/*  highly confidential in nature. Any and all use hereof is restricted                 */
/*  and is subject to the terms and conditions set forth in the                         */
/*  software license agreement concluded with NXP Software.                             */
/*                                                                                      */
/*  Under no circumstances is this software or any derivative thereof                   */
/*  to be combined with any Open Source Software in any way or                          */
/*  licensed under any Open License Terms without the express prior                     */
/*  written  permission of NXP Software.                                                */
/*                                                                                      */
/*  For the purpose of this clause, the term Open Source Software means                 */
/*  any software that is licensed under Open License Terms. Open                        */
/*  License Terms means terms in any license that require as a                          */
/*  condition of use, modification and/or distribution of a work                        */
/*                                                                                      */
/*           1. the making available of source code or other materials                  */
/*              preferred for modification, or                                          */
/*                                                                                      */
/*           2. the granting of permission for creating derivative                      */
/*              works, or                                                               */
/*                                                                                      */
/*           3. the reproduction of certain notices or license terms                    */
/*              in derivative works or accompanying documentation, or                   */
/*                                                                                      */
/*           4. the granting of a royalty-free license to any party                     */
/*              under Intellectual Property Rights                                      */
/*                                                                                      */
/*  regarding the work and/or any work that contains, is combined with,                 */
/*  requires or otherwise is based on the work.                                         */
/*                                                                                      */
/*  This software is provided for ease of recompilation only.                           */
/*  Modification and reverse engineering of this software are strictly                  */
/*  prohibited.                                                                         */
/*                                                                                      */
/****************************************************************************************/

/****************************************************************************************/
/*                                                                                      */
/*     $Author: nxp47058 $                                                              */
/*     $Revision: 4 $                                                               */
/*     $Date: 2015-08-28 15:30:13 +0900 (Fri, 28 Aug 2015) $                            */
/*                                                                                      */
/****************************************************************************************/
#if defined(LVVE)


/****************************************************************************************/
/*                                                                                      */
/*  Includes                                                                            */
/*                                                                                      */
/****************************************************************************************/
#include "LVVEFQ_Rx.h"
#ifdef TARGET_BUILD
#include "EliteMsg_Util.h"
#include "adsp_vparams_api.h"
#else
#include "HAP_farf.h"
#include "HAP_SDK_test_defines.h"
#include <string.h>
//#include "LVVE_ExampleApp.h"
#endif


/****************************************************************************************/
/*                                                                                      */
/*  Macro definitions Constant / Define Declarations                                    */
/*                                                                                      */
/****************************************************************************************/
#define ROUNDTO8(x) ((((x) + 7) >> 3) << 3)

/* kpps */ /* TODO */
#define VOICE_LVVEFQ_RX_NB_KPPS 7540
#define VOICE_LVVEFQ_RX_WB_KPPS 29300
#define VOICE_LVVEFQ_RX_FB_KPPS 29300

static const uint32_t LVVEFQ_RX_ENABLE_PARAM_SIZE = sizeof(uint16_t)
                                                  + sizeof(uint16_t); /* Reserved */

static const uint32_t LVVEFQ_RX_CONTROL_PARAM_SIZE = sizeof(LVM_Mode_en)     // OperatingMode
                                                   + sizeof(LVM_Mode_en);    // Mute

static const uint32_t LVVEFQ_RX_VOL_PARAM_SIZE = sizeof(LVM_Mode_en)
                                               + sizeof(LVM_INT16)
                                               + sizeof(uint16_t);

static const uint32_t LVVEFQ_RX_LVHF_PARAM_SIZE = sizeof(LVM_Mode_en)
                                                + sizeof(LVNLPP_ControlParams_st);

static const uint32_t LVVEFQ_RX_EQ_PARAM_SIZE = sizeof(LVM_Mode_en)
                                              + sizeof(LVM_UINT16)
                                              + sizeof(uint16_t) /* RESERVED */
                                              + (LVEQ_EQ_LENGTH_MAX*sizeof(LVM_INT16));

static const uint32_t LVVEFQ_RX_HPF_PARAM_SIZE = sizeof(LVM_Mode_en)
                                               + sizeof(LVM_INT16)
                                               + sizeof(uint16_t); /* RESERVED */

static const uint32_t LVVEFQ_RX_CNG_PARAM_SIZE = sizeof(LVM_Mode_en)
                                               + sizeof(LVCNG_ControlParams_st)
                                               + sizeof(uint16_t); /* RESERVED */

static const uint32_t LVVEFQ_RX_NF_PARAM_SIZE = sizeof(LVM_Mode_en)
                                              + sizeof(LVNF_ControlParams_st)
                                              +  sizeof(uint16_t); /* RESERVED */

static const uint32_t LVVEFQ_RX_DRC_PARAM_SIZE = sizeof(LVDRC_ControlParams_st)
                                               + sizeof(uint16_t)  /* RESERVED */
                                               + sizeof(uint16_t); /* RESERVED */

static const uint32_t LVVEFQ_RX_LVNG_PARAM_SIZE = sizeof(LVNG_ControlParams_st)
                                                + sizeof(uint16_t)  /* RESERVED */
                                                + sizeof(uint16_t); /* RESERVED */


/****************************************************************************************/
/*                                                                                      */
/*  Function Definitions                                                                */
/*                                                                                      */
/****************************************************************************************/


/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
void LVVEFQ_Rx_config(LVVEFQ_Rx_Instance_st *pInstance, uint32_t sample_rate)
{
#if defined(LVVE_DEBUG)
  MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[DEBUG] LVVEFQ_Rx_config: sample rate = %d", sample_rate);
#endif
  if(sample_rate == 8000)
  {
    pInstance->InstanceParams.SampleRate = LVM_FS_8000;
    pInstance->InstanceParams.EQ_InstParams.EQ_MaxLength = LVEQ_EQ_LENGTH_MIN*4;
  }
  else if(sample_rate == 16000)
  {
    pInstance->InstanceParams.SampleRate = LVM_FS_16000;
    pInstance->InstanceParams.EQ_InstParams.EQ_MaxLength = LVEQ_EQ_LENGTH_MIN*8;
  }
  else if(sample_rate == 48000)
  {
    pInstance->InstanceParams.SampleRate = LVM_FS_48000;
    pInstance->InstanceParams.EQ_InstParams.EQ_MaxLength = LVEQ_EQ_LENGTH_MIN*24;
  }
  else
  {
    MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Rx_config: Invalid sample rate = %d", sample_rate);
  }
}


/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
uint32_t LVVEFQ_Rx_get_size(LVVEFQ_Rx_Instance_st *pInstance)
{
  uint32_t mem_size = 0;
  int32_t i;
  LVVE_ReturnStatus_en LVVE_Status;

#if defined(LVVE_DEBUG)
  MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[DEBUG] LVVEFQ_Rx_get_size");
#endif

  /* control parameter */
  mem_size = ROUNDTO8(sizeof(LVVE_Rx_ControlParams_st));

  /* memory table */
  LVVE_Status = LVVE_Rx_GetMemoryTable(LVM_NULL, &(pInstance->MemTab), &(pInstance->InstanceParams));
  if(LVVE_Status != LVVE_SUCCESS)
  {
    MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"[ERROR] LVVEFQ_Rx_get_size: LVVE_Rx_GetMemoryTable returned %d", (int32_t)LVVE_Status);
    return 0;
  }
  for(i = 0; i < LVM_NR_MEMORY_REGIONS; i++)
  {
    if(pInstance->MemTab.Region[i].Size != 0)
    {      
      mem_size += ROUNDTO8(pInstance->MemTab.Region[i].Size);
    }
  }

  /* eq */
  mem_size += ROUNDTO8(LVEQ_EQ_LENGTH_MAX*sizeof(LVM_INT16));

  /* message */
  pInstance->LVVEFQ_LVAVC_Message.message_size = LVVE_Tx2RxMessage_GetMaxSize();
  mem_size += ROUNDTO8(pInstance->LVVEFQ_LVAVC_Message.message_size);

  /* version info */
  mem_size += ROUNDTO8(sizeof(LVVE_VersionInfo));

#if defined(LVVE_DEBUG)
  MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[DEBUG] LVVEFQ_Rx_get_size: total memory size requested = %d", mem_size);
#endif

  return mem_size;
}


/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
ADSPResult LVVEFQ_Rx_set_mem(LVVEFQ_Rx_Instance_st *pInstance,
                             int8_t *mem_addr_ptr,
                             uint32_t mem_size)
{
  ADSPResult result = ADSP_EOK;
  int32_t i;
  int8_t *pBase = mem_addr_ptr;
  uint32_t temp_mem_size = 0;

#if defined(LVVE_DEBUG)
  MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[DEBUG] LVVEFQ_Rx_set_mem: total memory size acquired = %d", mem_size);
#endif

  /* assign base address for each region */
  /* control parameter */
  temp_mem_size = ROUNDTO8(sizeof(LVVE_Rx_ControlParams_st));
  pInstance->ControlParams = (LVVE_Rx_ControlParams_st *)pBase;
  memset(pInstance->ControlParams, 0, temp_mem_size);
  pBase += temp_mem_size;

  /* memory table */
  for(i = 0; i < LVM_NR_MEMORY_REGIONS; i++)
  {
    if(pInstance->MemTab.Region[i].Size != 0)
    {
      temp_mem_size = ROUNDTO8(pInstance->MemTab.Region[i].Size);
      pInstance->MemTab.Region[i].pBaseAddress = (void *)pBase;
      memset(pInstance->MemTab.Region[i].pBaseAddress, 0, temp_mem_size);
      pBase += temp_mem_size;
    }
  }

  /* eq */
  temp_mem_size = ROUNDTO8(LVEQ_EQ_LENGTH_MAX*sizeof(LVM_INT16));
  pInstance->pEQ_Coefs = (LVM_INT16 *)pBase;
  memset(pInstance->pEQ_Coefs, 0, temp_mem_size);
  pBase += temp_mem_size;

  /* message */
  temp_mem_size = ROUNDTO8(pInstance->LVVEFQ_LVAVC_Message.message_size);
  pInstance->LVVEFQ_LVAVC_Message.message_ptr = (LVM_CHAR **)pBase;
  memset((LVM_INT8 *)pInstance->LVVEFQ_LVAVC_Message.message_ptr, 0, temp_mem_size);
  pInstance->LVVEFQ_LVAVC_Message.message_size = 0;
  pBase += temp_mem_size;

  /* version info */
  temp_mem_size = ROUNDTO8(sizeof(LVVE_VersionInfo));
  pInstance->pVersion = (LVVE_VersionInfo *)pBase;
  memset(pInstance->pVersion, 0, temp_mem_size);
  pBase += temp_mem_size;

  return result;
}


/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
ADSPResult LVVEFQ_Rx_init(LVVEFQ_Rx_Instance_st *pInstance)
{
  ADSPResult result = ADSP_EOK;
  LVVE_ReturnStatus_en LVVE_Status;

#if defined(LVVE_DEBUG)
  MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[DEBUG] LVVEFQ_Rx_init");
#endif

  pInstance->enable = 0;
  pInstance->hInstance = LVM_NULL;

  /* instance handle */
  LVVE_Status = LVVE_Rx_GetInstanceHandle(&(pInstance->hInstance), &(pInstance->MemTab), &(pInstance->InstanceParams));
  if(LVVE_Status != LVVE_SUCCESS)
  {
    MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"[ERROR] LVVEFQ_Rx_init: LVVE_Rx_GetInstanceHandle returned %d", (int32_t)LVVE_Status);
    result = ADSP_EFAILED;
  }

  /* Configure the Rx Voice Engine */
  /* input */
  LVVE_Status = LVVE_Rx_SetInputChannelCount(pInstance->hInstance, LVVE_RX_INPUT_CHANNEL_COUNT_MAX);
  if(LVVE_Status != LVVE_SUCCESS)
  {
    MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"[ERROR] LVVEFQ_Rx_init: LVVE_Rx_SetInputChannelCount returned %d", (int32_t)LVVE_Status);
    result = ADSP_EFAILED;
  }

  /* output */
  LVVE_Status = LVVE_Rx_SetOutputChannelCount(pInstance->hInstance, LVVE_RX_OUTPUT_CHANNEL_COUNT_MAX);
  if(LVVE_Status != LVVE_SUCCESS)
  {
    MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"[ERROR] LVVEFQ_Rx_init: LVVE_Rx_SetOutputChannelCount returned %d", (int32_t)LVVE_Status);
    result = ADSP_EFAILED;
  }

  /* eq */
  pInstance->ControlParams->EQ_ControlParams.pEQ_Coefs = pInstance->pEQ_Coefs;

  /* set control parameters */
  LVVE_Status = LVVE_Rx_SetControlParameters(pInstance->hInstance, pInstance->ControlParams);
  if(LVVE_Status != LVVE_SUCCESS)
  {
    MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"[ERROR] LVVEFQ_Rx_init: LVVE_Rx_SetControlParameters returned %d", (int32_t)LVVE_Status);
    result = ADSP_EFAILED;
  }

  /* version info */
  LVVE_Status = LVVE_GetVersionInfo(pInstance->pVersion);
  if(LVVE_Status != LVVE_SUCCESS)
    MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"[ERROR] LVVEFQ_Rx_init: LVVE_GetVersionInfo returned %d", (int32_t)LVVE_Status);
  else
  {
    MSG_3(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"LVVEFQ_Rx_init: LVVE Version is %d.%d.%d",
          (int32_t)pInstance->pVersion->VersionNumber[5] - 48,
          ((int32_t)pInstance->pVersion->VersionNumber[7] - 48)*10+((int32_t)pInstance->pVersion->VersionNumber[8] - 48),
          ((int32_t)pInstance->pVersion->VersionNumber[10] - 48)*10+((int32_t)pInstance->pVersion->VersionNumber[11] - 48));
  }


  return result;
}


/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
ADSPResult LVVEFQ_Rx_get_kpps(LVVEFQ_Rx_Instance_st *pInstance, uint32_t *kpps_ptr)
{
  ADSPResult result = ADSP_EOK;

#if defined(LVVE_DEBUG)
  MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[DEBUG] LVVEFQ_Rx_get_kpps");
#endif

  if((NULL != pInstance) && (NULL!= kpps_ptr))
  {
    pInstance->kpps = 0;
    if(pInstance->enable)
    {
      if(pInstance->InstanceParams.SampleRate == LVM_FS_48000)
        pInstance->kpps += VOICE_LVVEFQ_RX_FB_KPPS;
      else if(pInstance->InstanceParams.SampleRate == LVM_FS_16000)
        pInstance->kpps += VOICE_LVVEFQ_RX_WB_KPPS;
      else
        pInstance->kpps += VOICE_LVVEFQ_RX_NB_KPPS;
    }
    *kpps_ptr = pInstance->kpps;
  }
  else
  {
    result = ADSP_EBADPARAM;
    MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "LVVEFQ_Rx_get_kpps : Null pointers");
  }
  return result;
}


/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
ADSPResult LVVEFQ_Rx_get_delay(LVVEFQ_Rx_Instance_st *pInstance, uint32_t *delay_ptr)
{
  ADSPResult result = ADSP_EOK;

#if defined(LVVE_DEBUG)
  MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[DEBUG] LVVEFQ_Rx_get_delay");
#endif

  if((NULL != pInstance) && (NULL!= delay_ptr))
  {
    *delay_ptr = 0;
  }
  else
  {
    result = ADSP_EBADPARAM;
    MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "LVVEFQ_Rx_get_delay : Null pointers");
  }
  return result;
}


/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
void LVVEFQ_Rx_end(LVVEFQ_Rx_Instance_st *pInstance)
{
  int32_t i;

#if defined(LVVE_DEBUG)
  MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[DEBUG] LVVEFQ_Rx_end");
#endif

  pInstance->enable = 0;
  pInstance->kpps = 0;

  /* control parameter */
  if(pInstance->ControlParams != LVM_NULL)
  {
    pInstance->ControlParams = LVM_NULL;
  }

  /* memory table */
  for(i = 0; i< LVM_NR_MEMORY_REGIONS; i++)
  {
    pInstance->MemTab.Region[i].Size = 0;
    if(pInstance->MemTab.Region[i].pBaseAddress != LVM_NULL)
    {
      pInstance->MemTab.Region[i].pBaseAddress = LVM_NULL;
    }
  }

  /* eq */
  if(pInstance->pEQ_Coefs != LVM_NULL)
  {
    pInstance->pEQ_Coefs = LVM_NULL;
  }

  /* message */
  if(pInstance->LVVEFQ_LVAVC_Message.message_ptr != NULL)
    pInstance->LVVEFQ_LVAVC_Message.message_ptr = NULL;

  /* version info */
  if(pInstance->pVersion != LVM_NULL)
    pInstance->pVersion = LVM_NULL;

}


/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
ADSPResult LVVEFQ_Rx_process(LVVEFQ_Rx_Instance_st *pInstance,
                             short *InBuffer16_Mic,
                             short *OutBuffer16_Mic,
                             uint32_t ProcessBlockSize)
{
  ADSPResult result = ADSP_EOK;
  LVVE_ReturnStatus_en LVVE_Status;

#if defined(LVVE_DEBUG)
  MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[DEBUG] LVVEFQ_Rx_process");
#endif

  if(ProcessBlockSize % 80 == 0)
  {
    LVM_PCM_Buffer_st LineInBuffer;
    LVM_PCM_Channel_t LineInChannels[LVVE_RX_INPUT_CHANNEL_COUNT_MAX];
    LVM_PCM_Buffer_st PostProcessedBuffer;
    LVM_PCM_Channel_t PostProcessedChannels[LVVE_RX_OUTPUT_CHANNEL_COUNT_MAX];

    LineInBuffer.channels = LineInChannels;
    PostProcessedBuffer.channels = PostProcessedChannels;

    if(pInstance->LVVEFQ_LVAVC_Message.message_size != 0)
    {
      LVVE_Status = LVVE_Rx_ConsumeTx2RxMessage(pInstance->hInstance,
                                                (LVVE_Tx2RxMessage_Handle_t)pInstance->LVVEFQ_LVAVC_Message.message_ptr,
                                                pInstance->LVVEFQ_LVAVC_Message.message_size);
      if(LVVE_Status != LVVE_SUCCESS)
      {
        MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"[ERROR] LVVEFQ_Rx_process: LVVE_Rx_ConsumeTx2RxMessage returned %d", (int32_t)LVVE_Status);
        result = ADSP_EFAILED;
      }
    }

    /* Select the required buffers */
    LineInBuffer.channels[0] = (LVM_INT16 *)InBuffer16_Mic;
    LineInBuffer.FrameCount = ProcessBlockSize;
    PostProcessedBuffer.channels[0] = (LVM_INT16 *)OutBuffer16_Mic;

    LVVE_Status = LVVE_Rx_Process(pInstance->hInstance,   /* Instance handle */
                                  &LineInBuffer,          /* Input buffer with data from line in */
                                  &PostProcessedBuffer);  /* Output buffer with postprocessed data */
    if(LVVE_Status != LVVE_SUCCESS)
    {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Rx_process: LVVE_Rx_Process returned %d", (int32_t)LVVE_Status);
      result = ADSP_EFAILED;
    }
  }
  else
  {
    MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Rx_process: Invalid block size = %d", ProcessBlockSize);
    result = ADSP_EFAILED;
  }

  return result;
}


/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
ADSPResult LVVEFQ_Rx_set_param(LVVEFQ_Rx_Instance_st *pInstance,
                               int8_t *params_buffer_ptr,
                               uint32_t param_id,
                               uint32_t param_size)
{
  ADSPResult result = ADSP_EOK;
  uint16_t *param_ptr = (uint16_t *)params_buffer_ptr;
  LVVE_ReturnStatus_en LVVE_Status;

#if defined(LVVE_DEBUG)
  MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[DEBUG] LVVEFQ_Rx_set_param: parameter ID = 0x%08x, parameter size = %d", param_id, param_size);
#endif

  /* ENABLE */
  if(param_id == VOICE_PARAM_MOD_ENABLE)
  {
    if(param_size > LVVEFQ_RX_ENABLE_PARAM_SIZE) // 4byte
    {
      MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Rx_set_param: Invalid parameter size for ENABLE: received = %d, required size = %d", param_size, LVVEFQ_RX_ENABLE_PARAM_SIZE);
      result = ADSP_EBADPARAM;
    }

    /* update parameter */
    pInstance->enable = *(param_ptr+0);
  }

  /* CONTROL */
  else if(param_id == VOICE_PARAM_LVVEFQ_RX_CONTROL)
  {
    if(param_size > LVVEFQ_RX_CONTROL_PARAM_SIZE)
    {
      MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Rx_set_param: Invalid parameter size for CONTROL: received = %d, required size = %d", param_size, LVVEFQ_RX_CONTROL_PARAM_SIZE);
      result = ADSP_EBADPARAM;
    }

    /* update parameters */
    memcpy(pInstance->ControlParams, (LVVE_Rx_ControlParams_st *)param_ptr, LVVEFQ_RX_CONTROL_PARAM_SIZE);

    /* set control parameter */
    LVVE_Status = LVVE_Rx_SetControlParameters(pInstance->hInstance, pInstance->ControlParams);
    if(LVVE_Status != LVVE_SUCCESS)
    {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Rx_set_param: LVVE_Rx_SetControlParameters error for CONTROL returned %d", (int32_t)LVVE_Status);
      result = ADSP_EBADPARAM;
    }
  }

  /* VOL */
  else if(param_id == VOICE_PARAM_LVVEFQ_RX_VOL)
  {
    if(param_size > LVVEFQ_RX_VOL_PARAM_SIZE)
    {
      MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Rx_set_param: Invalid parameter size for CONTROL: received = %d, required size = %d", param_size, LVVEFQ_RX_VOL_PARAM_SIZE);
      result = ADSP_EBADPARAM;
    }

    pInstance->ControlParams->VOL_OperatingMode = (LVM_Mode_en)*(param_ptr+0);
    pInstance->ControlParams->VOL_Gain = (LVM_INT16)*(param_ptr+2);

    /* update parameter */
    LVVE_Status = LVVE_Rx_SetControlParameters(pInstance->hInstance, pInstance->ControlParams);
    if(LVVE_Status != LVVE_SUCCESS)
    {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Rx_set_param: LVVE_Rx_SetControlParameters error for VOL returned %d", (int32_t)LVVE_Status);
      result = ADSP_EBADPARAM;
    }
  }

  /* FENS */
  else if(param_id == VOICE_PARAM_LVVEFQ_RX_LVFENS)
  {
    if(param_size > sizeof(LVFENS_ControlParams_st))
    {
      MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Rx_set_param: Invalid parameter size for FENS: received = %d, required size = %d", param_size, sizeof(LVFENS_ControlParams_st));
      result = ADSP_EBADPARAM;
    }

    memcpy(&(pInstance->ControlParams->FENS_ControlParams), (LVFENS_ControlParams_st *)param_ptr, sizeof(LVFENS_ControlParams_st));

    /* update parameter */
    LVVE_Status = LVVE_Rx_SetControlParameters(pInstance->hInstance, pInstance->ControlParams);
    if(LVVE_Status != LVVE_SUCCESS)
    {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Rx_set_param: LVVE_Rx_SetControlParameters error for FENS returned %d", (int32_t)LVVE_Status);
      result = ADSP_EBADPARAM;
    }
  }

  /* LVWBE */
  else if(param_id == VOICE_PARAM_LVVEFQ_RX_LVWBE)
  {
    if(param_size > sizeof(LVWBE_ControlParams_st))
    {
      MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Rx_set_param: Invalid parameter size for LVWBE: received = %d, required size = %d", param_size, sizeof(LVWBE_ControlParams_st));
      result = ADSP_EBADPARAM;
    }

    memcpy(&(pInstance->ControlParams->WBE_ControlParams), (LVWBE_ControlParams_st *)param_ptr, sizeof(LVWBE_ControlParams_st));

    /* update parameter */
    LVVE_Status = LVVE_Rx_SetControlParameters(pInstance->hInstance, pInstance->ControlParams);
    if(LVVE_Status != LVVE_SUCCESS)
    {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Rx_set_param: LVVE_Rx_SetControlParameters error for LVWBE returned %d", (int32_t)LVVE_Status);
      result = ADSP_EBADPARAM;
    }
  }

  /* NLPP */
  else if(param_id == VOICE_PARAM_LVVEFQ_RX_LVNLPP)
  {
    if(param_size > LVVEFQ_RX_LVHF_PARAM_SIZE)
    {
      MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Rx_set_param: Invalid parameter size for LVWBE: received = %d, required size = %d", param_size, LVVEFQ_RX_LVHF_PARAM_SIZE);
      result = ADSP_EBADPARAM;
    }

    pInstance->ControlParams->NLPP_OperatingMode = (LVM_Mode_en)*(param_ptr+0);
    memcpy(&(pInstance->ControlParams->NLPP_ControlParams), (LVNLPP_ControlParams_st *)(param_ptr+2), sizeof(LVNLPP_ControlParams_st));

    /* update parameter */
    LVVE_Status = LVVE_Rx_SetControlParameters(pInstance->hInstance, pInstance->ControlParams);
    if(LVVE_Status != LVVE_SUCCESS)
    {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Rx_set_param: LVVE_Rx_SetControlParameters error for LVWBE returned %d", (int32_t)LVVE_Status);
      result = ADSP_EBADPARAM;
    }
  }

  /* LVAVC */
  else if(param_id == VOICE_PARAM_LVVEFQ_RX_LVAVC)
  {
    if(param_size > sizeof(LVAVC_ControlParams_st))
    {
      MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Rx_set_param: Invalid parameter size for LVAVC: received = %d, required size = %d", param_size, sizeof(LVAVC_ControlParams_st));
      result = ADSP_EBADPARAM;
    }

    memcpy(&(pInstance->ControlParams->AVC_ControlParams), (LVAVC_ControlParams_st *)param_ptr, sizeof(LVAVC_ControlParams_st));

    /* update parameter */
    LVVE_Status = LVVE_Rx_SetControlParameters(pInstance->hInstance, pInstance->ControlParams);
    if(LVVE_Status != LVVE_SUCCESS)
    {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Rx_set_param: LVVE_Rx_SetControlParameters error for LVAVC returned %d", (int32_t)LVVE_Status);
      result = ADSP_EBADPARAM;
    }
  }

  /* LVEQ */
  else if(param_id == VOICE_PARAM_LVVEFQ_RX_LVEQ)
  {
    if(param_size > LVVEFQ_RX_EQ_PARAM_SIZE)
    {
      MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Rx_set_param: Invalid parameter size for EQ: received = %d, required size = %d", param_size, LVVEFQ_RX_EQ_PARAM_SIZE);
      result = ADSP_EBADPARAM;
    }

    /* update eq parameter */
    pInstance->ControlParams->EQ_OperatingMode = (LVM_Mode_en)*(param_ptr+0);
    pInstance->ControlParams->EQ_ControlParams.EQ_Length = (LVM_UINT16)*(param_ptr+2);
    memcpy(pInstance->pEQ_Coefs, (LVM_INT16 *)(param_ptr+4), LVEQ_EQ_LENGTH_MAX*sizeof(LVM_INT16));

    /* override eq length in case of invalid eq length */
    if((pInstance->ControlParams->EQ_OperatingMode == LVM_MODE_ON) && (pInstance->ControlParams->EQ_ControlParams.EQ_Length == 0))
      pInstance->ControlParams->EQ_ControlParams.EQ_Length = LVEQ_EQ_LENGTH_MIN;

    /* update parameter */
    LVVE_Status = LVVE_Rx_SetControlParameters(pInstance->hInstance, pInstance->ControlParams);
    if(LVVE_Status != LVVE_SUCCESS)
    {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Rx_set_param: LVVE_Rx_SetControlParameters error for EQ returned %d", (int32_t)LVVE_Status);
      result = ADSP_EBADPARAM;
    }
  }

  /* DRC */
  else if(param_id == VOICE_PARAM_LVVEFQ_RX_LVDRC)
  {
    if(param_size > LVVEFQ_RX_DRC_PARAM_SIZE)
    {
      MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Rx_set_param: Invalid parameter size for DRC: received = %d, required size = %d", param_size, LVVEFQ_RX_DRC_PARAM_SIZE);
      result = ADSP_EBADPARAM;
    }

    /* update parameter */
    pInstance->ControlParams->DRC_ControlParams.OperatingMode = (LVM_Mode_en)*(param_ptr+0);
    pInstance->ControlParams->DRC_ControlParams.NumKnees = (LVM_UINT16)*(param_ptr+2);
    memcpy(&(pInstance->ControlParams->DRC_ControlParams.CompressorCurveInputLevels[0]), (LVM_INT16 *)(param_ptr+4), LVDRC_NUMKNEES_DEFAULT*sizeof(LVM_INT16));
    memcpy(&(pInstance->ControlParams->DRC_ControlParams.CompressorCurveOutputLevels[0]), (LVM_INT16 *)(param_ptr+10), LVDRC_NUMKNEES_DEFAULT*sizeof(LVM_INT16));
    pInstance->ControlParams->DRC_ControlParams.AttackTime = (LVM_INT16)*(param_ptr+15);
    pInstance->ControlParams->DRC_ControlParams.ReleaseTime = (LVM_INT16)*(param_ptr+16);
    pInstance->ControlParams->DRC_ControlParams.LimiterOperatingMode = (LVM_Mode_en)*(param_ptr+18);
    pInstance->ControlParams->DRC_ControlParams.LimitLevel = (LVM_INT16)*(param_ptr+20);

    /* update parameter */
    LVVE_Status = LVVE_Rx_SetControlParameters(pInstance->hInstance, pInstance->ControlParams);
    if(LVVE_Status != LVVE_SUCCESS)
    {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Rx_set_param: LVVE_Rx_SetControlParameters error for DRC returned %d", (int32_t)LVVE_Status);
      result = ADSP_EBADPARAM;
    }
  }

  /* HPF */
  else if(param_id == VOICE_PARAM_LVVEFQ_RX_HPF)
  {
    if(param_size > LVVEFQ_RX_HPF_PARAM_SIZE)
    {
      MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Rx_set_param: Invalid parameter size for HPF: received = %d, required size = %d", param_size, LVVEFQ_RX_HPF_PARAM_SIZE);
      result = ADSP_EBADPARAM;
    }

    pInstance->ControlParams->HPF_OperatingMode = (LVM_Mode_en)*(param_ptr+0);
    pInstance->ControlParams->HPF_CornerFreq = (LVM_UINT16)*(param_ptr+2);

    /* update parameter */
    LVVE_Status = LVVE_Rx_SetControlParameters(pInstance->hInstance, pInstance->ControlParams);
    if(LVVE_Status != LVVE_SUCCESS)
    {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Rx_set_param: LVVE_Rx_SetControlParameters error for HPF returned %d", (int32_t)LVVE_Status);
      result = ADSP_EBADPARAM;
    }
  }

  /* CNG */
  else if(param_id == VOICE_PARAM_LVVEFQ_RX_LVCNG)
  {
    if(param_size > LVVEFQ_RX_CNG_PARAM_SIZE)
    {
      MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Rx_set_param: Invalid parameter size for CNG: received = %d, required size = %d", param_size, LVVEFQ_RX_CNG_PARAM_SIZE);
      result = ADSP_EBADPARAM;
    }

    pInstance->ControlParams->CNG_OperatingMode = (LVM_Mode_en)*(param_ptr+0);
    memcpy(&(pInstance->ControlParams->CNG_ControlParams), (LVCNG_ControlParams_st *)(param_ptr+2), sizeof(LVCNG_ControlParams_st));

    /* update parameter */
    LVVE_Status = LVVE_Rx_SetControlParameters(pInstance->hInstance, pInstance->ControlParams);
    if(LVVE_Status != LVVE_SUCCESS)
    {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Rx_set_param: LVVE_Rx_SetControlParameters error for CNG returned %d", (int32_t)LVVE_Status);
      result = ADSP_EBADPARAM;
    }
  }

  /* LVWM */
  else if(param_id == VOICE_PARAM_LVVEFQ_RX_LVWM)
  {
    if(param_size > sizeof(LVWM_ControlParams_st))
    {
      MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Rx_set_param: Invalid parameter size for WM: received = %d, required size = %d", param_size, sizeof(LVWM_ControlParams_st));
      result = ADSP_EBADPARAM;
    }

    memcpy(&(pInstance->ControlParams->WM_ControlParams), (LVWM_ControlParams_st *)param_ptr, sizeof(LVWM_ControlParams_st));

    /* update parameter */
    LVVE_Status = LVVE_Rx_SetControlParameters(pInstance->hInstance, pInstance->ControlParams);
    if(LVVE_Status != LVVE_SUCCESS)
    {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Rx_set_param: LVVE_Rx_SetControlParameters error for WM returned %d", (int32_t)LVVE_Status);
      result = ADSP_EBADPARAM;
    }
  }

  /* LVNG */
  else if(param_id == VOICE_PARAM_LVVEFQ_RX_LVNG)
  {
    if(param_size > LVVEFQ_RX_LVNG_PARAM_SIZE)
    {
      MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Rx_set_param: Invalid parameter size for NG: received = %d, required size = %d", param_size, LVVEFQ_RX_LVNG_PARAM_SIZE);
      result = ADSP_EBADPARAM;
    }

    pInstance->ControlParams->NG_ControlParams.OperatingMode = (LVM_Mode_en)*(param_ptr+0);
    pInstance->ControlParams->NG_ControlParams.NumKnees = (LVM_UINT16)*(param_ptr+2);
    memcpy(&(pInstance->ControlParams->NG_ControlParams.CompressorCurveInputLevels[0]), (LVM_INT16 *)(param_ptr+4), LVNG_NUMKNEES_DEFAULT*sizeof(LVM_INT16));
    memcpy(&(pInstance->ControlParams->NG_ControlParams.CompressorCurveOutputLevels[0]), (LVM_INT16 *)(param_ptr+10), LVNG_NUMKNEES_DEFAULT*sizeof(LVM_INT16));
    pInstance->ControlParams->NG_ControlParams.AttackTime = (LVM_INT16)*(param_ptr+15);
    pInstance->ControlParams->NG_ControlParams.ReleaseTime = (LVM_INT16)*(param_ptr+16);

    /* set parameter */
    LVVE_Status = LVVE_Rx_SetControlParameters(pInstance->hInstance, pInstance->ControlParams);
    if(LVVE_Status != LVVE_SUCCESS)
    {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Rx_set_param: LVVE_Rx_SetControlParameters error for NG returned %d", (int32_t)LVVE_Status);
      result = ADSP_EBADPARAM;
    }
  }

  /* NF */
  else if(param_id == VOICE_PARAM_LVVEFQ_RX_LVNF)
  {
    if(param_size > LVVEFQ_RX_NF_PARAM_SIZE)
    {
      MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Rx_set_param: Invalid parameter size for NF: received = %d, required size = %d", param_size, LVVEFQ_RX_NF_PARAM_SIZE);
      result = ADSP_EBADPARAM;
    }

    pInstance->ControlParams->NF_OperatingMode = (LVM_Mode_en)*(param_ptr+0);
    memcpy(&(pInstance->ControlParams->NF_ControlParams.NF_Attenuation), (LVNF_ControlParams_st *)(param_ptr+2), sizeof(LVNF_ControlParams_st));

    /* set parameter */
    LVVE_Status = LVVE_Rx_SetControlParameters(pInstance->hInstance, pInstance->ControlParams);
    if(LVVE_Status != LVVE_SUCCESS)
    {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Rx_set_param: LVVE_Rx_SetControlParameters error for NF returned %d", (int32_t)LVVE_Status);
      result = ADSP_EBADPARAM;
    }
  }

  /* ERROR */
  else
  {
    MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Rx_set_param: Invalid parameter = 0x%08x", param_id);
    result = ADSP_EBADPARAM;
  }

  return result;
}


/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
ADSPResult LVVEFQ_Rx_get_param(LVVEFQ_Rx_Instance_st *pInstance,
                               int8_t *params_buffer_ptr,
                               uint32_t param_id,
                               uint32_t buf_size,
                               uint32_t *param_size_ptr)
{
  ADSPResult result = ADSP_EOK;
  uint16_t *param_ptr = (uint16_t *)params_buffer_ptr;

#if defined(LVVE_DEBUG)
  MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[DEBUG] LVVEFQ_Rx_get_param");
#endif

  /* ENABLE */
  if(param_id == VOICE_PARAM_MOD_ENABLE)
  {
    if(LVVEFQ_RX_ENABLE_PARAM_SIZE <= buf_size)
    {
      *(param_ptr+0) = pInstance->enable;
      *(param_ptr+1) = 0;
      *param_size_ptr = LVVEFQ_RX_ENABLE_PARAM_SIZE;
    }
    else
    {
      MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Rx_get_param: Insufficient buffer length for ENABLE: received = %d, required size = %d", buf_size, LVVEFQ_RX_ENABLE_PARAM_SIZE);
      result = ADSP_EBADPARAM;
    }
  }

  /* CONTROL */
  else if(param_id == VOICE_PARAM_LVVEFQ_RX_CONTROL)
  {
    if(LVVEFQ_RX_CONTROL_PARAM_SIZE <= buf_size)
    {
      memcpy(param_ptr, (uint16_t *)&(pInstance->ControlParams->OperatingMode), LVVEFQ_RX_CONTROL_PARAM_SIZE);
      *param_size_ptr = LVVEFQ_RX_CONTROL_PARAM_SIZE;
    }
    else
    {
      MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Rx_get_param: Insufficient buffer length for CONTROL: received = %d, required size = %d", buf_size, LVVEFQ_RX_CONTROL_PARAM_SIZE);
      result = ADSP_EBADPARAM;
    }
  }

  /* VOL */
  else if(param_id == VOICE_PARAM_LVVEFQ_RX_VOL)
  {
    if(LVVEFQ_RX_VOL_PARAM_SIZE <= buf_size)
    {
      *(param_ptr+0) = (uint16_t)pInstance->ControlParams->VOL_OperatingMode;
      *(param_ptr+1) = 0; // RESERVED
      *(param_ptr+2) = (uint16_t)pInstance->ControlParams->VOL_Gain;
      *(param_ptr+3) = 0; // RESERVED
      *param_size_ptr = LVVEFQ_RX_VOL_PARAM_SIZE;
    }
    else
    {
      MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Rx_get_param: Insufficient buffer length for VOL: received = %d, required size = %d", buf_size, LVVEFQ_RX_VOL_PARAM_SIZE);
      result = ADSP_EBADPARAM;
    }
  }

  /* FENS */
  else if(param_id == VOICE_PARAM_LVVEFQ_RX_LVFENS)
  {
    if(sizeof(LVFENS_ControlParams_st) <= buf_size)
    {
      memcpy(param_ptr, (uint16_t *)&(pInstance->ControlParams->FENS_ControlParams.OperatingMode), sizeof(LVFENS_ControlParams_st));
      *param_size_ptr = sizeof(LVFENS_ControlParams_st);
    }
    else
    {
      MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Rx_get_param: Insufficient buffer length for FENS: received = %d, required size = %d", buf_size, sizeof(LVFENS_ControlParams_st));
      result = ADSP_EBADPARAM;
    }
  }

  /* LVWBE */
  else if(param_id == VOICE_PARAM_LVVEFQ_RX_LVWBE)
  {
    if(sizeof(LVWBE_ControlParams_st) <= buf_size)
    {
      memcpy(param_ptr, (uint16_t *)&(pInstance->ControlParams->WBE_ControlParams.OperatingMode), sizeof(LVWBE_ControlParams_st));
      *param_size_ptr = sizeof(LVWBE_ControlParams_st);
    }
    else
    {
      MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Rx_get_param: Insufficient buffer length for LVWBE: received = %d, required size = %d", buf_size, sizeof(LVWBE_ControlParams_st));
      result = ADSP_EBADPARAM;
    }
  }

  /* LVNLPP */
  else if(param_id == VOICE_PARAM_LVVEFQ_RX_LVNLPP)
  {
    if(LVVEFQ_RX_LVHF_PARAM_SIZE <= buf_size)
    {
      *(param_ptr+0) = (uint16_t)pInstance->ControlParams->NLPP_OperatingMode;
      memcpy(param_ptr+2, (uint16_t *)&(pInstance->ControlParams->NLPP_ControlParams.NLPP_Limit), sizeof(LVNLPP_ControlParams_st));
      *param_size_ptr = LVVEFQ_RX_LVHF_PARAM_SIZE;
    }
    else
    {
      MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Rx_get_param: Insufficient buffer length for NLPP: received = %d, required size = %d", buf_size, LVVEFQ_RX_LVHF_PARAM_SIZE);
      result = ADSP_EBADPARAM;
    }
  }

  /* LVAVC */
  else if(param_id == VOICE_PARAM_LVVEFQ_RX_LVAVC)
  {
    if(sizeof(LVAVC_ControlParams_st) <= buf_size)
    {
      memcpy(param_ptr, (uint16_t *)&(pInstance->ControlParams->AVC_ControlParams.OperatingMode), sizeof(LVAVC_ControlParams_st));
      *param_size_ptr = sizeof(LVAVC_ControlParams_st);
    }
    else
    {
      MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Rx_get_param: Insufficient buffer length for LVAVC: received = %d, required size = %d", buf_size, sizeof(LVAVC_ControlParams_st));
      result = ADSP_EBADPARAM;
    }
  }

  /* EQ */
  else if(param_id == VOICE_PARAM_LVVEFQ_RX_LVEQ)
  {
    if(LVVEFQ_RX_EQ_PARAM_SIZE <= buf_size)
    {
      *(param_ptr+0) = (uint16_t)pInstance->ControlParams->EQ_OperatingMode;
      *(param_ptr+1) = 0; // RESERVED
      *(param_ptr+2) = (uint16_t)pInstance->ControlParams->EQ_ControlParams.EQ_Length;
      *(param_ptr+3) = 0; // RESERVED
      memcpy(param_ptr+4, (uint16_t *)&(pInstance->pEQ_Coefs[0]), LVEQ_EQ_LENGTH_MAX*sizeof(LVM_INT16));
      *param_size_ptr = LVVEFQ_RX_EQ_PARAM_SIZE;
    }
    else
    {
      MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Rx_get_param: Insufficient buffer length for EQ: received = %d, required size = %d", buf_size, LVVEFQ_RX_EQ_PARAM_SIZE);
      result = ADSP_EBADPARAM;
    }
  }

  /* LVDRC */
  else if(param_id == VOICE_PARAM_LVVEFQ_RX_LVDRC)
  {
    if(LVVEFQ_RX_DRC_PARAM_SIZE <= buf_size)
    {
      *(param_ptr+0) = (uint16_t)pInstance->ControlParams->DRC_ControlParams.OperatingMode;
      *(param_ptr+1) = 0; // RESERVED
      *(param_ptr+2) = (uint16_t)pInstance->ControlParams->DRC_ControlParams.NumKnees;
      *(param_ptr+3) = 0; // RESERVED
      memcpy(param_ptr+4, (uint16_t *)&(pInstance->ControlParams->DRC_ControlParams.CompressorCurveInputLevels[0]), LVDRC_NUMKNEES_DEFAULT*sizeof(LVM_INT16));
      *(param_ptr+9) = 0; // RESERVED
      memcpy(param_ptr+10, (uint16_t *)&(pInstance->ControlParams->DRC_ControlParams.CompressorCurveOutputLevels[0]), LVDRC_NUMKNEES_DEFAULT*sizeof(LVM_INT16));
      *(param_ptr+15) = (uint16_t)pInstance->ControlParams->DRC_ControlParams.AttackTime;
      *(param_ptr+16) = (uint16_t)pInstance->ControlParams->DRC_ControlParams.ReleaseTime;
      *(param_ptr+17) = 0; // RESERVED
      *(param_ptr+18) = (uint16_t)pInstance->ControlParams->DRC_ControlParams.LimiterOperatingMode;
      *(param_ptr+19) = 0;
      *(param_ptr+20) = (uint16_t)pInstance->ControlParams->DRC_ControlParams.LimitLevel;
      *(param_ptr+21) = 0; // RESERVED
      *param_size_ptr = LVVEFQ_RX_DRC_PARAM_SIZE;
    }
    else
    {
      MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Rx_get_param: Insufficient buffer length for DRC: received = %d, required size = %d", buf_size, LVVEFQ_RX_DRC_PARAM_SIZE);
      result = ADSP_EBADPARAM;
    }
  }

  /* HPF */
  else if(param_id == VOICE_PARAM_LVVEFQ_RX_HPF)
  {
    if(LVVEFQ_RX_HPF_PARAM_SIZE <= buf_size)
    {
      *(param_ptr+0) = (uint16_t)pInstance->ControlParams->HPF_OperatingMode;
      *(param_ptr+1) = 0; // RESERVED
      *(param_ptr+2) = (uint16_t)pInstance->ControlParams->HPF_CornerFreq;
      *(param_ptr+3) = 0; // RESERVED
      *param_size_ptr = LVVEFQ_RX_HPF_PARAM_SIZE;
    }
    else
    {
      MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Rx_get_param: Insufficient buffer length for HPF: received = %d, required size = %d", buf_size, LVVEFQ_RX_HPF_PARAM_SIZE);
      result = ADSP_EBADPARAM;
    }
  }

  /* LVCNG */
  else if(param_id == VOICE_PARAM_LVVEFQ_RX_LVCNG)
  {
    if(LVVEFQ_RX_CNG_PARAM_SIZE <= buf_size)
    {
      *(param_ptr+0) = (uint16_t)pInstance->ControlParams->CNG_OperatingMode;
      memcpy(param_ptr+2, (uint16_t *)&(pInstance->ControlParams->CNG_ControlParams.CNG_Volume), sizeof(LVCNG_ControlParams_st));
      *param_size_ptr = LVVEFQ_RX_CNG_PARAM_SIZE;
    }
    else
    {
      MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Rx_get_param: Insufficient buffer length for CNG: received = %d, required size = %d", buf_size, LVVEFQ_RX_CNG_PARAM_SIZE);
      result = ADSP_EBADPARAM;
    }
  }

  /* LVWM */
  else if(param_id == VOICE_PARAM_LVVEFQ_RX_LVWM)
  {
    if(sizeof(LVWM_ControlParams_st) <= buf_size)
    {
      memcpy(param_ptr, (uint16_t *)&(pInstance->ControlParams->WM_ControlParams.OperatingMode), sizeof(LVWM_ControlParams_st));
      *param_size_ptr = sizeof(LVWM_ControlParams_st);
    }
    else
    {
      MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Rx_get_param: Insufficient buffer length for WM: received = %d, required size = %d", buf_size, sizeof(LVWM_ControlParams_st));
      result = ADSP_EBADPARAM;
    }
  }

  /* LVNG */
  else if(param_id == VOICE_PARAM_LVVEFQ_RX_LVNG)
  {
    if(LVVEFQ_RX_LVNG_PARAM_SIZE <= buf_size)
    {
      *(param_ptr+0) = (uint16_t)pInstance->ControlParams->NG_ControlParams.OperatingMode;
      *(param_ptr+1) = 0; // RESERVED
      *(param_ptr+2) = (uint16_t)pInstance->ControlParams->NG_ControlParams.NumKnees;
      *(param_ptr+3) = 0; // RESERVED
      memcpy(param_ptr+4, (uint16_t *)&(pInstance->ControlParams->NG_ControlParams.CompressorCurveInputLevels[0]), LVNG_NUMKNEES_DEFAULT*sizeof(LVM_INT16));
      *(param_ptr+9) = 0; // RESERVED
      memcpy(param_ptr+10, (uint16_t *)&(pInstance->ControlParams->NG_ControlParams.CompressorCurveOutputLevels[0]), LVNG_NUMKNEES_DEFAULT*sizeof(LVM_INT16));
      *(param_ptr+15) = (uint16_t)pInstance->ControlParams->NG_ControlParams.AttackTime;
      *(param_ptr+16) = (uint16_t)pInstance->ControlParams->NG_ControlParams.ReleaseTime;
      *param_size_ptr = LVVEFQ_RX_LVNG_PARAM_SIZE;
    }
    else
    {
      MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Rx_get_param: Insufficient buffer length for NG: received = %d, required size = %d", buf_size, LVVEFQ_RX_LVNG_PARAM_SIZE);
      result = ADSP_EBADPARAM;
    }
  }

  /* NF */
  else if(param_id == VOICE_PARAM_LVVEFQ_RX_LVNF)
  {
    if(LVVEFQ_RX_NF_PARAM_SIZE <= buf_size)
    {
      *(param_ptr+0) = (uint16_t)pInstance->ControlParams->NF_OperatingMode;
      memcpy(param_ptr+2, (uint16_t *)&(pInstance->ControlParams->NF_ControlParams.NF_Attenuation), sizeof(LVNF_ControlParams_st));
      *param_size_ptr = LVVEFQ_RX_NF_PARAM_SIZE;
    }
    else
    {
      MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Rx_get_param: Insufficient buffer length for NF: received = %d, required size = %d", buf_size, LVVEFQ_RX_NF_PARAM_SIZE);
      result = ADSP_EBADPARAM;
    }
  }

  /* ERROR */
  else
  {
    MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[ERROR] LVVEFQ_Rx_get_param: Invalid parameter = 0x%08x", param_id);
    result = ADSP_EBADPARAM;
  }

  return result;
}


/****************************************************************************************/
/*                                                                                      */
/* FUNCTION:                                                                            */
/*                                                                                      */
/* DESCRIPTION:                                                                         */
/*                                                                                      */
/* PARAMETERS:                                                                          */
/*                                                                                      */
/* RETURNS:                                                                             */
/*                                                                                      */
/* NOTES:                                                                               */
/*                                                                                      */
/****************************************************************************************/
void LVVEFQ_Rx_set_noise_param(LVVEFQ_Rx_Instance_st *pInstance, const int8_t *pParamsBuffer, int32_t pParamsBufferLen)
{
  const LVVEFQ_LVAVC_Message_st *pPos = reinterpret_cast<const LVVEFQ_LVAVC_Message_st *>(pParamsBuffer);
#if defined(LVVE_DEBUG)
  MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[DEBUG] LVVEFQ_Rx_set_noise_param");
#endif
  pInstance->LVVEFQ_LVAVC_Message.message_size = pPos->message_size;
  memcpy(pInstance->LVVEFQ_LVAVC_Message.message_ptr, pPos->message_ptr, pInstance->LVVEFQ_LVAVC_Message.message_size);
}

#if !defined(TARGET_BUILD) && defined (LVVE_DEBUG)
void LVVEFQ_Rx_PrintParams(LVVEFQ_Rx_Instance_st *pInstance)
{
  LVVE_Rx_ControlParams_st *ControlParams_Rx = pInstance->ControlParams;
  int32_t i;

  FARF(HIGH, "ControlParams_Rx->OperatingMode                                    %d          ", 				  ControlParams_Rx->OperatingMode									 ); 		  
  FARF(HIGH, "ControlParams_Rx->Mute                                             %d          ", 				  ControlParams_Rx->Mute											 ); 		  
  FARF(HIGH, "ControlParams_Rx->VOL_OperatingMode                                %d          ", 				  ControlParams_Rx->VOL_OperatingMode								 ); 		  
  FARF(HIGH, "ControlParams_Rx->VOL_Gain                                         %d          ", 				  ControlParams_Rx->VOL_Gain										 ); 		  
  FARF(HIGH, "ControlParams_Rx->FENS_ControlParams.OperatingMode                 %d          ", 				  ControlParams_Rx->FENS_ControlParams.OperatingMode				 ); 		  
  FARF(HIGH, "ControlParams_Rx->FENS_ControlParams.FENS_limit_NS                 %d          ", 				  ControlParams_Rx->FENS_ControlParams.FENS_limit_NS				 ); 		  
  FARF(HIGH, "ControlParams_Rx->FENS_ControlParams.FENS_NoiseThreshold           %d          ", 				  ControlParams_Rx->FENS_ControlParams.FENS_NoiseThreshold			 ); 		  
  FARF(HIGH, "ControlParams_Rx->FENS_ControlParams.Mode                          %d          ", 				  ControlParams_Rx->FENS_ControlParams.Mode 						 ); 		  
  FARF(HIGH, "ControlParams_Rx->NLPP_OperatingMode                               %d          ", 				  ControlParams_Rx->NLPP_OperatingMode                               ); 		  
  FARF(HIGH, "ControlParams_Rx->NLPP_ControlParams.NLPP_Limit                    %d          ", 				  ControlParams_Rx->NLPP_ControlParams.NLPP_Limit                    ); 		  
  FARF(HIGH, "ControlParams_Rx->NLPP_ControlParams.NLPP_HPF_CornerFreq           %d          ", 				  ControlParams_Rx->NLPP_ControlParams.NLPP_HPF_CornerFreq 			 ); 		  
  
  for(i = 0; i < 64; i++)
  FARF(HIGH, "ControlParams_Rx->EQ_ControlParams.pEQ_Coefs[%d]                   %d          ", 				  i, ControlParams_Rx->EQ_ControlParams.pEQ_Coefs[i]				 );
  
  FARF(HIGH, "ControlParams_Rx->DRC_ControlParams.OperatingMode                  %d          ", 				  ControlParams_Rx->DRC_ControlParams.OperatingMode 				 ); 		  
  FARF(HIGH, "ControlParams_Rx->DRC_ControlParams.NumKnees                       %d          ", 				  ControlParams_Rx->DRC_ControlParams.NumKnees						 ); 		  
  FARF(HIGH, "ControlParams_Rx->DRC_ControlParams.CompressorCurveInputLevels[0]  %d          ", 				  ControlParams_Rx->DRC_ControlParams.CompressorCurveInputLevels[0]  ); 		  
  FARF(HIGH, "ControlParams_Rx->DRC_ControlParams.CompressorCurveInputLevels[1]  %d          ", 				  ControlParams_Rx->DRC_ControlParams.CompressorCurveInputLevels[1]  ); 		  
  FARF(HIGH, "ControlParams_Rx->DRC_ControlParams.CompressorCurveInputLevels[2]  %d          ", 				  ControlParams_Rx->DRC_ControlParams.CompressorCurveInputLevels[2]  ); 		  
  FARF(HIGH, "ControlParams_Rx->DRC_ControlParams.CompressorCurveInputLevels[3]  %d          ", 				  ControlParams_Rx->DRC_ControlParams.CompressorCurveInputLevels[3]  ); 		  
  FARF(HIGH, "ControlParams_Rx->DRC_ControlParams.CompressorCurveInputLevels[4]  %d          ", 				  ControlParams_Rx->DRC_ControlParams.CompressorCurveInputLevels[4]  ); 		  
  FARF(HIGH, "ControlParams_Rx->DRC_ControlParams.CompressorCurveOutputLevels[0] %d          ", 				  ControlParams_Rx->DRC_ControlParams.CompressorCurveOutputLevels[0] ); 		  
  FARF(HIGH, "ControlParams_Rx->DRC_ControlParams.CompressorCurveOutputLevels[1] %d          ", 				  ControlParams_Rx->DRC_ControlParams.CompressorCurveOutputLevels[1] ); 		  
  FARF(HIGH, "ControlParams_Rx->DRC_ControlParams.CompressorCurveOutputLevels[2] %d          ", 				  ControlParams_Rx->DRC_ControlParams.CompressorCurveOutputLevels[2] ); 		  
  FARF(HIGH, "ControlParams_Rx->DRC_ControlParams.CompressorCurveOutputLevels[3] %d          ", 				  ControlParams_Rx->DRC_ControlParams.CompressorCurveOutputLevels[3] ); 		  
  FARF(HIGH, "ControlParams_Rx->DRC_ControlParams.CompressorCurveOutputLevels[4] %d          ", 				  ControlParams_Rx->DRC_ControlParams.CompressorCurveOutputLevels[4] ); 		  
  FARF(HIGH, "ControlParams_Rx->DRC_ControlParams.AttackTime                     %d          ", 				  ControlParams_Rx->DRC_ControlParams.AttackTime					 ); 		  
  FARF(HIGH, "ControlParams_Rx->DRC_ControlParams.ReleaseTime                    %d          ", 				  ControlParams_Rx->DRC_ControlParams.ReleaseTime					 ); 		  
  FARF(HIGH, "ControlParams_Rx->DRC_ControlParams.LimiterOperatingMode           %d          ", 				  ControlParams_Rx->DRC_ControlParams.LimiterOperatingMode			 ); 		  
  FARF(HIGH, "ControlParams_Rx->DRC_ControlParams.LimitLevel                     %d          ", 				  ControlParams_Rx->DRC_ControlParams.LimitLevel					 ); 		  
  FARF(HIGH, "ControlParams_Rx->HPF_OperatingMode                                %d          ", 				  ControlParams_Rx->HPF_OperatingMode								 ); 		  
  FARF(HIGH, "ControlParams_Rx->HPF_CornerFreq                                   %d          ", 				  ControlParams_Rx->HPF_CornerFreq									 ); 		  
  FARF(HIGH, "ControlParams_Rx->CNG_OperatingMode                                %d          ", 				  ControlParams_Rx->CNG_OperatingMode 			                     ); 		  
  FARF(HIGH, "ControlParams_Rx->CNG_ControlParams.CNG_Volume                     %d          ", 				  ControlParams_Rx->CNG_ControlParams.CNG_Volume					 ); 		  
  FARF(HIGH, "ControlParams_Rx->WM_ControlParams.OperatingMode                   %d          ", 				  ControlParams_Rx->WM_ControlParams.OperatingMode					 ); 		  
  FARF(HIGH, "ControlParams_Rx->WM_ControlParams.mode                            %d          ", 				  ControlParams_Rx->WM_ControlParams.mode							 ); 		  
  FARF(HIGH, "ControlParams_Rx->WM_ControlParams.AVL_Target_level_lin            %d          ", 				  ControlParams_Rx->WM_ControlParams.AVL_Target_level_lin			 ); 		  
  FARF(HIGH, "ControlParams_Rx->WM_ControlParams.AVL_MinGainLin                  %d          ", 				  ControlParams_Rx->WM_ControlParams.AVL_MinGainLin 				 ); 		  
  FARF(HIGH, "ControlParams_Rx->WM_ControlParams.AVL_MaxGainLin                  %d          ", 				  ControlParams_Rx->WM_ControlParams.AVL_MaxGainLin 				 ); 		  
  FARF(HIGH, "ControlParams_Rx->WM_ControlParams.AVL_Attack                      %d          ", 				  ControlParams_Rx->WM_ControlParams.AVL_Attack 					 ); 		  
  FARF(HIGH, "ControlParams_Rx->WM_ControlParams.AVL_Release                     %d          ", 				  ControlParams_Rx->WM_ControlParams.AVL_Release					 ); 		  
  FARF(HIGH, "ControlParams_Rx->WM_ControlParams.AVL_Limit_MaxOutputLin          %d          ", 				  ControlParams_Rx->WM_ControlParams.AVL_Limit_MaxOutputLin 		 ); 		  
  FARF(HIGH, "ControlParams_Rx->WM_ControlParams.SpDetect_Threshold              %d          ", 				  ControlParams_Rx->WM_ControlParams.SpDetect_Threshold 			 ); 		  
  FARF(HIGH, "ControlParams_Rx->NG_ControlParams.OperatingMode                   %d          ", 				  ControlParams_Rx->NG_ControlParams.OperatingMode					 ); 		  
  FARF(HIGH, "ControlParams_Rx->NG_ControlParams.NumKnees                        %d          ", 				  ControlParams_Rx->NG_ControlParams.NumKnees						 ); 		  
  FARF(HIGH, "ControlParams_Rx->NG_ControlParams.CompressorCurveInputLevels[0]   %d          ", 				  ControlParams_Rx->NG_ControlParams.CompressorCurveInputLevels[0]	 ); 		  
  FARF(HIGH, "ControlParams_Rx->NG_ControlParams.CompressorCurveInputLevels[1]   %d          ", 				  ControlParams_Rx->NG_ControlParams.CompressorCurveInputLevels[1]	 ); 		  
  FARF(HIGH, "ControlParams_Rx->NG_ControlParams.CompressorCurveInputLevels[2]   %d          ", 				  ControlParams_Rx->NG_ControlParams.CompressorCurveInputLevels[2]	 ); 		  
  FARF(HIGH, "ControlParams_Rx->NG_ControlParams.CompressorCurveInputLevels[3]   %d          ", 				  ControlParams_Rx->NG_ControlParams.CompressorCurveInputLevels[3]	 ); 		  
  FARF(HIGH, "ControlParams_Rx->NG_ControlParams.CompressorCurveInputLevels[4]   %d          ", 				  ControlParams_Rx->NG_ControlParams.CompressorCurveInputLevels[4]	 ); 		  
  FARF(HIGH, "ControlParams_Rx->NG_ControlParams.CompressorCurveOutputLevels[0]  %d          ", 				  ControlParams_Rx->NG_ControlParams.CompressorCurveOutputLevels[0]  ); 		  
  FARF(HIGH, "ControlParams_Rx->NG_ControlParams.CompressorCurveOutputLevels[1]  %d          ", 				  ControlParams_Rx->NG_ControlParams.CompressorCurveOutputLevels[1]  ); 		  
  FARF(HIGH, "ControlParams_Rx->NG_ControlParams.CompressorCurveOutputLevels[2]  %d          ", 				  ControlParams_Rx->NG_ControlParams.CompressorCurveOutputLevels[2]  ); 		  
  FARF(HIGH, "ControlParams_Rx->NG_ControlParams.CompressorCurveOutputLevels[3]  %d          ", 				  ControlParams_Rx->NG_ControlParams.CompressorCurveOutputLevels[3]  ); 		  
  FARF(HIGH, "ControlParams_Rx->NG_ControlParams.CompressorCurveOutputLevels[4]  %d          ", 				  ControlParams_Rx->NG_ControlParams.CompressorCurveOutputLevels[4]  ); 		  
  FARF(HIGH, "ControlParams_Rx->NG_ControlParams.AttackTime                      %d          ", 				  ControlParams_Rx->NG_ControlParams.AttackTime 					 ); 		  
  FARF(HIGH, "ControlParams_Rx->NG_ControlParams.ReleaseTime                     %d          ", 				  ControlParams_Rx->NG_ControlParams.ReleaseTime					 ); 		  
  FARF(HIGH, "ControlParams_Rx->NF_OperatingMode                                 %d          ", 				  ControlParams_Rx->NF_OperatingMode				                 ); 		  
  FARF(HIGH, "ControlParams_Rx->NF_ControlParams.NF_Attenuation                  %d          ", 				  ControlParams_Rx->NF_ControlParams.NF_Attenuation 				 ); 		  
  FARF(HIGH, "ControlParams_Rx->NF_ControlParams.NF_Frequency                    %d          ", 				  ControlParams_Rx->NF_ControlParams.NF_Frequency					 ); 		  
  FARF(HIGH, "ControlParams_Rx->NF_ControlParams.NF_QFactor                      %d          ", 				  ControlParams_Rx->NF_ControlParams.NF_QFactor 					 ); 		  

}


void LVVEFQ_Rx_SetDefaultParams(LVVEFQ_Rx_Instance_st *pInstance)
{
  LVVE_Rx_ControlParams_st *ControlParams_Rx = pInstance->ControlParams;
  LVVE_ReturnStatus_en LVVE_Status;

  ControlParams_Rx->OperatingMode                                     = LVVE_RX_MODE_ON;
  ControlParams_Rx->Mute                                              = LVM_MODE_OFF;
  ControlParams_Rx->VOL_OperatingMode                                 = LVM_MODE_ON;
  ControlParams_Rx->VOL_Gain                                          = 0;

  memcpy((LVFENS_ControlParams_st *)&(ControlParams_Rx->FENS_ControlParams),
         (LVFENS_ControlParams_st *)&LVFENS_ControlParams_Active,
         sizeof(LVFENS_ControlParams_st));


  ControlParams_Rx->NLPP_OperatingMode                                = LVM_MODE_OFF;
  ControlParams_Rx->NLPP_ControlParams.NLPP_Limit                     = 0;
  ControlParams_Rx->NLPP_ControlParams.NLPP_HPF_CornerFreq            = 0;

  ControlParams_Rx->EQ_OperatingMode                                  = LVM_MODE_OFF;
  if(pInstance->InstanceParams.SampleRate == LVM_FS_16000)
    ControlParams_Rx->EQ_ControlParams.EQ_Length                      = LVEQ_EQ_LENGTH_MAX;
  else
    ControlParams_Rx->EQ_ControlParams.EQ_Length                      = LVEQ_EQ_LENGTH_MAX/2;
  ControlParams_Rx->EQ_ControlParams.pEQ_Coefs                        = pInstance->pEQ_Coefs;
  memset(pInstance->pEQ_Coefs, 0, LVEQ_EQ_LENGTH_MAX*sizeof(LVM_INT16));
  pInstance->pEQ_Coefs[0]                                             = 4096;


  memcpy((LVDRC_ControlParams_st *)&(ControlParams_Rx->DRC_ControlParams),
         (LVDRC_ControlParams_st *)&LVDRC_ControlParams_Active,
         sizeof(LVDRC_ControlParams_st));


  ControlParams_Rx->HPF_OperatingMode                                 = LVM_MODE_OFF;
  ControlParams_Rx->HPF_CornerFreq                                    = 100;

  memcpy((LVCNG_ControlParams_st *)&(ControlParams_Rx->CNG_ControlParams),
         (LVCNG_ControlParams_st *)&CNGControl_Parameters_Active,
          sizeof(LVCNG_ControlParams_st));
  ControlParams_Rx->CNG_OperatingMode                                 = LVM_MODE_OFF;


  memcpy((LVWM_ControlParams_st *)&(ControlParams_Rx->WM_ControlParams),
         (LVWM_ControlParams_st *)&LVWM_ControlParams_Active,
         sizeof(LVWM_ControlParams_st));


  memcpy((LVNG_ControlParams_st *)&(ControlParams_Rx->NG_ControlParams),
         (LVNG_ControlParams_st *)&LVNG_ControlParams_Active,
         sizeof(LVNG_ControlParams_st));

  memcpy((LVNF_ControlParams_st *)&(ControlParams_Rx->NF_ControlParams),
         (LVNF_ControlParams_st *)&LVNF_ControlParams_Active,
         sizeof(LVNF_ControlParams_st));
  ControlParams_Rx->NF_OperatingMode                                  = LVM_MODE_OFF; 


  LVVE_Status = LVVE_Rx_SetControlParameters(pInstance->hInstance, pInstance->ControlParams);
  if(LVVE_Status != LVVE_SUCCESS)
  {
    FARF(ERROR, "[ERROR] LVVEFQ_Rx_SetDefaultParams:  error returned %d", (int32_t)LVVE_Status);
  }

}
#endif


#endif // LVVE

