/*======================= COPYRIGHT NOTICE ======================*/
/* Copyright (c) 2013 by Qualcomm Technologies, Inc. All rights reserved.     */
/* All data and information contained in or disclosed by this    */
/* document is confidential and proprietary information of       */
/* Qualcomm Technologies, Inc and all rights therein are expressly reserved.  */
/* By accepting this material the recipient agrees that this     */
/* material and the information contained therein is held in     */
/* confidence and in trust and will not be used, copied,         */
/* reproduced in whole or in part, nor its contents revealed in  */
/* any manner to others without the express written permission   */
/* of Qualcomm Technologies, Inc.                                             */
/*===============================================================*/
/*---------------------------------------------------------------*/
/* FileName: tty_tx_api.h         FileType: C Header             */
/*---------------------------------------------------------------*/
/* Description:                                                  */
/*   Publicizes the functions to systems code                    */
/*---------------------------------------------------------------*/
/* Revision History: None                                        */
/* Author            Date                      Comments          */
/* -------          ------                     ----------        */
/* Ravi O        27,Feb,2013             Systems Version 1.0.1   */
/*****************************************************************/

#ifndef TTY_TX_API_H
#define TTY_TX_API_H


/* Public function declarations  */

extern uint16 ltetty_tx_get_struct_size(void);

extern uint16 ltetty_rx_get_struct_size(void);

extern uint16 ltetty_get_fifo_struct_size(void);

extern void init_ltetty_tx(void *oobtty_tx_struct_instance_ptr);

extern void init_ltetty_rx(void *oobtty_rx_struct_instance_ptr);

extern void init_ltetty_fifo(void *lte_tty_fifo_ptr, void *fifoBuffer);

extern int16 lte_tty_tx_processing(int16 *txPCMIn,
                            int16 *txPCMOut,
                            int16 frameSize,
                            int16 *ttyChar,
                            boolean rxActivityFlag,
                            void *oobtty_tx_struct_instance_ptr);

extern boolean lte_tty_rx_processing(int16 *rxPCMIn,
                              int16 *rxPCMOut,
                              int16 frameSize,
                              int16 *rxChar,
                              int16 *rxFifoLen,
                              void *oobtty_rx_struct_instance_ptr,
                              void *lte_tty_fifo_ptr);

extern boolean lte_tty_get_tx_bypass_flag(void *oobtty_tx_struct_instance_ptr);

extern boolean lte_tty_get_rx_bypass_flag(void *oobtty_rx_struct_instance_ptr);

extern boolean lte_tty_fifo_push(void  *ltetty_fifo_ptr,
                          int16           *rxChar,
                          int16            num_elements_to_push,
                          int16           *num_elements_pushed);

#endif
