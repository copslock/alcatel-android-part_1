#ifndef __WV_V2_ALGORITHM_API__
#define __WV_V2_ALGORITHM_API__


//#define WV_SCRATCH_PROFILE

#define MAX_IN_WV_CHS              (1)
#define MIN_IN_WV_CHS              (1)
#define MAX_OUT_WV_CHS             (MAX_IN_WV_CHS)
#define MIN_OUT_WV_CHS             (MIN_IN_WV_CHS)
#define WV_BIT_WIDTH               (16) // only 16 bits per sample supported
#define WV_DEFAULT_FEATURE_MODE    (0) // only one algorithm feature mode supported.

/*
   Parameter ID can be used to enable/disable the library during the fly.
   Structure wv_mode_t defines the parameter supported by the library.
   */
#define WV_MODE_PARAM (0x4000L)

/*
   Parameter ID supporting the basic library calibration parameters.
   Structure wv_calib_param_t defines the parameter supported by the library.
   */
#define WV_CALIB_PARAM (0x1000L)

/*
   Parameter ID can be used to get the crossfading status of the library.
   Structure wv_crossfade_mode_t defines the parameter supported by the library.
   */
#define WV_XFADE_MODE_PARAM (0x8000L)

/*
   Parameter ID can be used to init wv data structure and initialize resampler
   */
#define WV_RESET_PARAM (0x2000L)

/*
   enum encapsulating the different return codes for each function call
   */
typedef enum WV_RESULT_T {
   WV_SUCCESS = 0,
   WV_FAIL,              // general failure code
   WV_CONFIG_ERROR,      // static configuration error
   WV_MEMORY_ERROR,      // memory requirements/sizing error
   WV_BAD_PARAM          // calibration param error
}WV_RESULT_T;

/*
   wv_static_config_t is the library structure encapsulating library configurations which
   do not change with calibration or without memory reallocation
   */
typedef struct wv_static_config_t{
   /* in/out/operating sampling rate.
      Supported rates 8000Hz, 16000Hz */
   unsigned long int sampling_rate;
   /* Number of input samples per frame
    Only 20ms per frame is supported*/
   unsigned long int num_in_samples_frame;
   /* Indicates minimum size of output buffer to be provided
    Only 20 ms per frame is supported*/
   unsigned long int num_out_samples_frame;
   /* Number of bits for input and output samples,
      Currently only 16 bits per sample is supported */
   unsigned long int bits_per_sample;
   /* Operational mode of the library (to optimize for memory and add new features to the library)
    Only one more supported, 0 - Default mode */
   unsigned long int feature_mode;
} wv_static_config_t;

/*
   wv_memory_t is the library structure encapsulating memory details of the library.
   Memory for this structure has to be allocated before calling any of the library apis

   */
typedef struct wv_memory_t{
   /* Size of library memory required in bytes */
   unsigned long int lib_memory_size;
   /* Size of the stack needed by library*/
   unsigned long int lib_stack_size;
} wv_memory_t;

/*
   wv_lib_t is the library instance structure.
   Memory for this structure has to be allocated before calling any of the library apis

   */
typedef struct wv_lib_t{
   /* pointer to the library memory */
   void *lib_memory_ptr;
} wv_lib_t;

/*
wv_v2_get_mem_req
- provides the memory requirements of the library based on the configuration set
This function is expected to be called before wv_v2_init_memory is invoked.

Return:
result - WV_RESULT_T
   */
WV_RESULT_T wv_v2_get_mem_req(
      /* wv_memory_t : [OUT] : pointer to the structure for memory requirements */
      wv_memory_t *wv_memory_ptr,
      /* wv_static_config_ptr: [IN] : pointer to static wv library configuration */
      wv_static_config_t *wv_input_static_config_ptr
      );

/*
wv_v2_init_memory
- Allocates memory to library &
- Initializes calibration to default values
This function is expected to be called before wv_v2_process is invoked.

Return:
result - WV_RESULT_T
   */
WV_RESULT_T wv_v2_init_memory(
      /* wv_lib_ptr: [IN/OUT] : pointer to the wv public library structure  */
      wv_lib_t *wv_lib_ptr,
      /* wv_static_config_ptr: [IN] : pointer to static wv library configuration */
      wv_static_config_t *wv_input_static_config_ptr,
      /* memory_ptr: [IN] : pointer to memory */
      void *memory_ptr,
      /* memory_size: [IN] : size of the memory */
      unsigned long int memory_size
      );

/*
wv_v2_set_param
- Sets parameter defined by the param ids
- Param ID defines the type of the parameter and can be used for differentiating
  - Calibration data with data flush and reinit of entire lib
  - Updating calibration parameters which out reinit of entire lib
  - Flush buffers while retaining calibration data
  - Auto Calibration

Return:
result - WV_RESULT_T
   */
WV_RESULT_T wv_v2_set_param(
      /* wv_lib_ptr: [IN/OUT] : pointer to the wv public library structure  */
      wv_lib_t *wv_lib_ptr,
      /* param_buffer_ptr: [IN] : pointer to parameter buffer */
      signed char *param_buffer_ptr,
      /* param_id: [IN] : Unique parameter ID*/
      unsigned long int param_id,
      /* param_size: [IN] : size of the parameter data*/
      signed long int param_size
      );

/*
wv_v2_get_param
- Returns parameters defined by the param ids in the buffer pointer
- Param ID defines the type of the parameter and can be used for
  - Auto Calibration
  - All Calibration data
  - Parameter data for monitoring library status
  - Parameter data for library version

Return:
result - WV_RESULT_T
   */
WV_RESULT_T wv_v2_get_param(
      /* wv_lib_ptr: [IN/OUT] : pointer to the wv public library structure  */
      wv_lib_t *wv_lib_ptr,
      /* param_buffer_ptr: [OUT] : pointer to parameter buffer for writing param data by library */
      signed char *param_buffer_ptr,
      /* param_id: [IN] : Unique parameter ID*/
      unsigned long int param_id,
      /* buffer_size: [IN] : Size of the input buffer available for writing */
      signed long int buffer_size,
      /* param_size_ptr: [OUT] : Actual size of the parameter data written by the library */
      signed long int *param_size_ptr
      );

/*
wv_v2_process
Reads input data and processes it according to the calibration data set using wv_v2_set_param
Writes output data and returns number of output buffer samples filled
Default calibration data is used if wv_v2_set_param was called.
This function can only be invoked after successful call to wv_v2_init_memory

Return:
result - WV_RESULT_T

   */
WV_RESULT_T wv_v2_process(
      /* wv_lib_ptr: [IN/OUT] : pointer to the wv public library structure  */
      wv_lib_t *wv_lib_ptr,
      /* input_buffer_ptr: [IN] : array of MAX_IN_WV_CHS pointing to memory of int8_t input data*/
      signed char *in_buffer_ptr[MAX_IN_WV_CHS],
      /* input_samples: [IN] : number of input samples (per channel) */
      signed long int num_in_samples,
      /* out_buffer_ptr: [OUT] : array of MAX_OUT_WV_CHS pointing to memory of int8_t library output data*/
      signed char *out_buffer_ptr[MAX_OUT_WV_CHS],
      /* output buf max samples: [IN] : available output buffer size in samples (per channel) */
      signed long int num_out_samples_max,
      /* output buf filled samples: [OUT] : number of output buffer samples filled (per channel) */
      signed long int *num_out_samples_filled_ptr
      );



#endif//#define __WV_ALGORITHM_API__
