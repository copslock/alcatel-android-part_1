/*======================= COPYRIGHT NOTICE ======================*/
/* Copyright (c) 2006 by Qualcomm Technologies, Inc. All rights reserved.     */
/* All data and information contained in or disclosed by this    */
/* document is confidential and proprietary information of       */
/* Qualcomm Technologies, Inc and all rights therein are expressly reserved.  */
/* By accepting this material the recipient agrees that this     */
/* material and the information contained therein is held in     */
/* confidence and in trust and will not be used, copied,         */
/* reproduced in whole or in part, nor its contents revealed in  */
/* any manner to others without the express written permission   */
/* of Qualcomm Technologies, Inc.                                             */
/*===============================================================*/
/*---------------------------------------------------------------*/
/* FileName: ctmRx.h         FileType:ctmRx C Header             */
/*---------------------------------------------------------------*/
/* Description:                                                  */
/*   Publicizes the functions to systems code                    */
/*---------------------------------------------------------------*/
/* Revision History: None                                        */
/* Author            Date                      Comments          */
/* -------          ------                     ----------        */
/* Syed Arif        27,Feb,2008             Written for QDSP6    */
/*****************************************************************/

#ifndef CTMTX_API_H
#define CTMTX_API_H

#ifndef UNIT_TEST
typedef int32 Bool;
#endif

/* Public function declarations  */
extern int ctmtx_get_struct_size(void);

extern void init_ctmTx(void *pCTMTxStructInstance);

extern void deinit_ctmTx(void *pCTMTxStructInstance);

extern int16 ctmtx_ttydet(void *pCTMTxStructInstance,
                        int16 *inputSignalBufferLeft,
            uint16 numSamples
#ifdef MIPS_PROFILE
            ,void *pProfileStructInstance
#endif                        
                        );

extern char ctmtx_modulate(void *pCTMTxStructInstance,
            int16 *inputSignalBufferLeft,
                        int16 *txToneVecRight,
            uint16 numSamples,
                        Bool *enquiryFromFarEndDetected,
                        Bool *ctmCharacterTransmitted,
            Bool *byPassed,       /* to indicate current audio is being bypassed */
                        Bool ctmFromFarEndDetected,
                        Bool disableBypass,
            Bool fHandover
#ifdef MIPS_PROFILE
            ,void *pProfileStructInstance
#endif                        
                        );

/* returns flag to indicate if current mode is FIGS */
extern uint8 ctmtx_is_in_figs_mode( void *pCTMTxStructInstance);
#endif
