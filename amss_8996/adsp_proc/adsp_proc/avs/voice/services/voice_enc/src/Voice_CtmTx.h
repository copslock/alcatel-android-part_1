#ifndef VOICE_CTMTX_H
#define VOICE_CTMTX_H
/****************************************************************************
Copyright (c) 2009-20010 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government. Diversion contrary to U.S. law prohibited.
****************************************************************************/
/*====================================================================== */

/*========================================================================
                             Edit History

$Header: //components/rel/avs.adsp/2.7.1.c4/voice/services/voice_enc/src/Voice_CtmTx.h#1 $

when          who      what, where, why
--------      ---      -------------------------------------------------------
08/05/2010     dp      Created
========================================================================== */
#include "Elite.h"
#include "qurt_elite.h"

#ifdef __cplusplus
extern "C" {
#endif //__cplusplus

#include "voice_resampler.h"
typedef struct {

  uint8  enable;                        // Enable/Disable CTM Processing
  int32_t  sync_recover_tx;                 // flag to Resync CTM
  int32_t  ctm_character_transmitted;       // flag to indicate CTM Tx was sent
  int32_t  enquiry_from_far_end_detected;     // detect flag of Enquiry burst at Rx
  int32_t  ctm_from_far_end_detected;         // detect flag of CTM from far end
  voice_resampler_config_struct_t down_samp_config; //CTM downsampler config struct for Tx
  voice_resampler_config_struct_t up_samp_config; //CTM upsampler config struct for Tx
  void   *down_samp_mem_ptr;             // CTM downsampler memory pointer for Tx;
  void   *up_samp_mem_ptr;               // CTM upsampler memory pointer for Tx
  void   *ctm_tx_struct_instance_ptr;          // structure to hold CTM lib state
  uint32_t session_id;
  uint16_t sampling_rate;
} ctm_tx_struct_t;

/* initialize Ctm module, allocate memory */
ADSPResult voice_ctm_tx_init(ctm_tx_struct_t* ctm_tx_struct_ptr, uint32_t session_id);

/* process 20 ms buffer, assume in place */
ADSPResult voice_ctm_tx_process(ctm_tx_struct_t* ctm_tx_struct_ptr, int16_t *in_ptr, int16_t *out_ptr, uint16_t samples, uint16_t out_buff_size);

/* destroy Ctm module, free memory */
ADSPResult voice_ctm_tx_end(ctm_tx_struct_t* ctm_tx_struct_ptr);

/* enable/disable module.  disable will reset state flags for peer notification */
ADSPResult voice_ctm_tx_set_enable(ctm_tx_struct_t* ctm_tx_struct_ptr, uint8 enable);

uint8_t voice_ctm_tx_get_enable(ctm_tx_struct_t* ctm_tx_struct_ptr);

/* set FB vs WB vs NB */
ADSPResult voice_ctm_tx_commit_format(ctm_tx_struct_t* ctm_tx_struct_ptr, uint16_t sampling_rate);

/* resync CTM during handover */
ADSPResult voice_ctm_tx_resync(ctm_tx_struct_t* ctm_tx_struct_ptr);

/* get when CTM char transmitted */
ADSPResult voice_ctm_tx_get_ctm_char_transmitted(ctm_tx_struct_t* ctm_tx_struct_ptr, uint8 *ctm_char_tx);

/* set status of CTM detect on far end, and whether status has been notified */
ADSPResult voice_ctm_tx_set_ctm_from_far_end_detected(ctm_tx_struct_t* ctm_tx_struct_ptr, uint8 ctm_from_far_end);

/* set status of Enquiry detect from far end */
ADSPResult voice_ctm_tx_set_enquiry_from_far_end_detected(ctm_tx_struct_t* ctm_tx_struct_ptr, uint8 enquiry_from_far_end);

/* initialize Ctm resampler, allocate memory */
ADSPResult voice_ctm_tx_resampler_init(ctm_tx_struct_t* ctm_tx_struct_ptr);

ADSPResult voice_ctm_tx_get_kpps(ctm_tx_struct_t* ctm_tx_struct_ptr,uint32_t* kpps_ptr, uint8_t enable, uint16_t sampling_rate);


#ifdef __cplusplus
}
#endif //__cplusplus
#endif /* VOICE_DTMF_DETECT_H */

