#ifndef VOICE_OOBTTYTX_H
#define VOICE_OOBTTYTX_H
/****************************************************************************
Copyright (c) 2013-2014 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government. Diversion contrary to U.S. law prohibited.
****************************************************************************/
/*====================================================================== */

/*========================================================================
                             Edit History

$Header: //components/rel/avs.adsp/2.7.1.c4/voice/services/voice_enc/src/Voice_OOBTTYTx.h#1 $

when          who      what, where, why
--------      ---      -------------------------------------------------------
02/27/2013     rojha      Created
========================================================================== */
#include "Elite.h"
#include "qurt_elite.h"
#include "voice_resampler.h"

#ifdef __cplusplus
extern "C" {
#endif //__cplusplus

typedef struct {

  uint8_t  enable;                        // Enable/Disable CTM Processing
  uint32_t  sampling_rate;                      // sampling_rate
  voice_resampler_config_struct_t down_samp_config; //OOB TTY downsampler config struct for Rx
  void   *down_samp_mem_ptr;             // OOB TTY downsampler memory ptr for Rx;
  void   *oobtty_tx_struct_instance_ptr;     // State structure pointer for Tx LTE TTY
  int16_t txChar;
  uint32_t session_id;
} oobtty_tx_struct_t;

/*  allocate memory for OOB TTY Tx */
ADSPResult voice_oobtty_tx_mem_alloc(oobtty_tx_struct_t* oobtty_tx_struct_ptr);

/* initialize OOB TTY Tx module */
void voice_oobtty_tx_init(oobtty_tx_struct_t* oobtty_tx_struct_ptr, uint32_t session_id);

/* process 20 ms buffer, assume in place */
ADSPResult voice_process_oobtty_tx(oobtty_tx_struct_t* oobtty_tx_struct_ptr, int16_t *in_ptr, int16_t *out_ptr, uint16_t samples, int16_t rxActivity);

/* destroy OOB TTY Tx module, free memory */
ADSPResult voice_oobtty_tx_end(oobtty_tx_struct_t* oobtty_tx_struct_ptr);

/* This will set TTY enable and wideband mode flag based on config. */
ADSPResult voice_oobtty_tx_set_config(oobtty_tx_struct_t* oobtty_tx_struct_ptr, uint8 enable, uint32 samp_rate);

/* function to check if character is detected by VoLTE TTY Tx module */
uint8_t voice_oobtty_tx_char_detected(oobtty_tx_struct_t* oobtty_tx_struct_ptr);

/* function  to query the tx character detected by VoLTE TTY Tx module */
int16_t voice_oobtty_tx_get_char(oobtty_tx_struct_t* oobtty_tx_struct_ptr);

/* function  to query the tx character detected by VoLTE TTY Tx module */
uint8_t voice_oobtty_tx_get_enable(oobtty_tx_struct_t* oobtty_tx_struct_ptr);

ADSPResult voice_oobtty_tx_get_kpps(oobtty_tx_struct_t* oobtty_tx_struct_ptr,uint32_t* kpps_ptr,uint8_t enable, uint16_t sampling_rate);

/* resampler init for OOB TTY */
ADSPResult voice_oobtty_tx_resampler_init(oobtty_tx_struct_t* oobtty_tx_struct_ptr);

#ifdef __cplusplus
}
#endif //__cplusplus
#endif /* VOICE_OOBTTYTX_H */

