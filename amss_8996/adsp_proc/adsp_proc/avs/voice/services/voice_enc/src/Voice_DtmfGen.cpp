
/*============================================================================
                             Edit History

$Header: //components/rel/avs.adsp/2.7.1.c4/voice/services/voice_enc/src/Voice_DtmfGen.cpp#1 $

when       who     what, where, why
--------   ---     -------------------------------------------------------
12/17/10   sivab   dtmf wrapper simplified, tested for all cases.
12/08/10   sivab   C++ to C wrapper changes

============================================================================*/

/*============================================================================
 *                       INCLUDE FILES FOR MODULE
 *==========================================================================*/

#include "voice_dtmfgen.h"
#include "VoiceCmnUtils.h"

/* =============================================================*/
/*                                                              */
/*         DEFINITIONS AND DECLARATIONS FOR MODULE              */
/*                                                              */
/* This section contains definitions for constants,macros,types,*/
/* variables and other items needed by this module.             */
/*==============================================================*/

/* -------------------------------------------------------------*/
/* Constant / Define Declarations                               */
/* -------------------------------------------------------------*/

#define VOICE_DTMFGEN_NB_KPPS (160)
#define VOICE_DTMFGEN_WB_KPPS (300)
#define VOICE_DTMFGEN_FB_KPPS (1250)
#define VOICE_DTMFGEN_SWB_KPPS (VOICE_DTMFGEN_NB_KPPS + VOICE_RESAMPLER_KPPS_8K_TO_32K)
static const vcmn_sampling_kpps_t VOICE_DTMFGEN_SAMPLING_KPPS_TABLE[] = {{VOICE_NB_SAMPLING_RATE, VOICE_DTMFGEN_NB_KPPS },
                                                                         {VOICE_WB_SAMPLING_RATE, VOICE_DTMFGEN_WB_KPPS },
                                                                         {VOICE_SWB_SAMPLING_RATE, VOICE_DTMFGEN_SWB_KPPS },
                                                                         {VOICE_FB_SAMPLING_RATE, VOICE_DTMFGEN_FB_KPPS },
                                                                         {VOICE_INVALID_SAMPLING_RATE, 0}};

/*--------------------------------------------------------------*/
/* Local Object Definitions                                     */
/* -------------------------------------------------------------*/

void voice_dtmf_resampler_mem_free(dtmf_gen_struct_t *dtmf_gen_ptr)
{
   if(NULL != dtmf_gen_ptr->dtmf_resampler_channel_mem_ptr)
   {
    qurt_elite_memory_free(dtmf_gen_ptr->dtmf_resampler_channel_mem_ptr);
    dtmf_gen_ptr->dtmf_resampler_channel_mem_ptr = NULL;
   }
   else
   {
    MSG(MSG_SSID_QDSP6, DBG_LOW_PRIO,"VCP: dtmf gen resampler memory not allocated or already freed");
   }
}


ADSPResult voice_dtmf_resampler_init(dtmf_gen_struct_t *dtmf_gen_ptr, uint32 sampling_rate)
{
   if(NULL == dtmf_gen_ptr)
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: voice_dtmf_resampler_init():: NULL Pointer error");
      return ADSP_EFAILED;
   }

   if(EIGHT == dtmf_gen_ptr->sampling_freq || SIXTEEN== dtmf_gen_ptr->sampling_freq)
   {
    MSG(MSG_SSID_QDSP6, DBG_LOW_PRIO,"VCP: dtmf gen resampler init not required for NB/WB");
    return ADSP_EOK;
   }

   uint32 num_out_samples_ptr = 0;
   // 8-> 32/48 resampler configuration
   int32 resampler_result =  voice_resampler_set_config(&(dtmf_gen_ptr->dtmf_resampler_cfg_str),VOICE_NB_SAMPLING_RATE,
        sampling_rate, NO_OF_BITS_PER_SAMPLE,VOICE_FRAME_SIZE_NB,&num_out_samples_ptr);

   if( VOICE_RESAMPLE_SUCCESS != resampler_result )
   {
    MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: ERROR: DTMF Gen up-sampler set config failed");
    return ADSP_EFAILED;
   }

    //Allocate channel memory for resampler
   uint32 channel_mem_size = VOICE_ROUNDTO8(dtmf_gen_ptr->dtmf_resampler_cfg_str.total_channel_mem_size);
   dtmf_gen_ptr->dtmf_resampler_channel_mem_ptr = (int8_t *)qurt_elite_memory_malloc(channel_mem_size, QURT_ELITE_HEAP_DEFAULT);

   if(NULL == dtmf_gen_ptr->dtmf_resampler_channel_mem_ptr)
   {
    MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: ERROR: DTMF Gen resampler channel mem alloc failed");
    return ADSP_ENOMEMORY;
   }

   resampler_result = voice_resampler_channel_init(&(dtmf_gen_ptr->dtmf_resampler_cfg_str),
            dtmf_gen_ptr->dtmf_resampler_channel_mem_ptr,
            dtmf_gen_ptr->dtmf_resampler_cfg_str.total_channel_mem_size
            );

   if( VOICE_RESAMPLE_SUCCESS != resampler_result )
   {
    MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: ERROR: DTMF Gen upsampler-sampler init failed");
    qurt_elite_memory_free(dtmf_gen_ptr->dtmf_resampler_channel_mem_ptr);
    dtmf_gen_ptr->dtmf_resampler_channel_mem_ptr = NULL;
    return ADSP_EFAILED;
   }

   return ADSP_EOK;
}

ADSPResult voice_dtmf_gen_default_init(dtmf_gen_struct_t *dtmf_gen_ptr)
{
   if(NULL == dtmf_gen_ptr)
   {
        MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: voice_dtmf_gen_default_init():: NULL Pointer error");
      return ADSP_EFAILED;
   }
   dtmf_gen_ptr->dtmf_gen_tone1        = 1209;
   dtmf_gen_ptr->dtmf_gen_tone2        = 697;
   dtmf_gen_ptr->duration_in_micro_secs     = 0;
   dtmf_gen_ptr->dtmf_gen_gain         = 0x2000; //unity in unsigned Q13 format.
   dtmf_gen_ptr->dtmf_gen_mix_flag     = FALSE;
   dtmf_gen_ptr->current_dtmf_state    = DTMF_STOP;
   dtmf_gen_ptr->dtmf_gen_last_block   = FALSE;
   dtmf_gen_ptr->second_dtmf_command   = FALSE;

   dtmf_gen_ptr->second_dtmf_samples_in_block1 = 0;
   dtmf_gen_ptr->sampling_freq = EIGHT;
   return ADSP_EOK;


}

#if !defined(__qdsp6__)
static int16_t voice_dtmf_gen_add_sat(int16_t var1, int16_t var2)
{
   int32_t L_sum;
   L_sum = (int32_t) var1 + var2;
   if (L_sum > 0x00007fffL)
   {
     L_sum = 0x00007fffL;
   }
   else if (L_sum < (int32_t) 0xffff8000L)
   {
     L_sum = 0xffff8000L;
   }
   return (int16_t)L_sum;


}
#endif

static void dtmf_gen_mix_with_voice(int16 *in_ptr, int16 *out_ptr, int16 size)
{
   int16_t i;

   for(i=0;i<size;i++)
   {
      out_ptr[i] = voice_dtmf_gen_add_sat(out_ptr[i],in_ptr[i]);
   }
}
ADSPResult voice_dtmf_gen_process(dtmf_gen_struct_t *dtmf_gen_ptr,int16_t *in_ptr,int16_t *out_ptr, int16_t in_size)
{


   if (DTMF_RUN == dtmf_gen_ptr->current_dtmf_state)
   {
    int16_t dtmf_gen_samples[VOICE_FRAME_SIZE_FB];
    int16_t num_dtmf_samples;

#if defined(VOICE_DBG_MSG)
      MSG(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VOICE: Tx DTMF GEN processing");
#endif

      uint32 num_input_buf_samples = in_size>>1;
      num_dtmf_samples = (SIXTEEN < dtmf_gen_ptr->sampling_freq)?VOICE_FRAME_SIZE_NB:num_input_buf_samples;

      dtmf_gen_ptr->current_dtmf_state = dtmf_tone_gen(&(dtmf_gen_ptr->dtmf_generator),
            &dtmf_gen_samples[0],
            0xFFFE, /* max gain in unsigned format, divide by 2 is done inside lib */
          (int16_t)num_dtmf_samples, /*For FB we need samples corresponding to NB and resample it to FB*/
            &dtmf_gen_ptr->second_dtmf_samples_in_block1,
            FALSE,
            dtmf_gen_ptr->dtmf_gen_last_block
            );

      //For 48kHz tone generation, resample 8khz tone to 48kHz tone
      if(SIXTEEN < dtmf_gen_ptr->sampling_freq)
      {
         //8->32/48 Resample
        if(NULL != dtmf_gen_ptr->dtmf_resampler_channel_mem_ptr)
        {
          int32 resampler_result =  voice_resampler_process(
                  &(dtmf_gen_ptr->dtmf_resampler_cfg_str),
                  dtmf_gen_ptr->dtmf_resampler_channel_mem_ptr,
                  (int8 *)&dtmf_gen_samples[0],
                  num_dtmf_samples,
                  (int8 *)&dtmf_gen_samples[0],
                  num_input_buf_samples
                  );
          if(VOICE_RESAMPLE_SUCCESS != resampler_result)
          {
              MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: DTMF Gen resampler process failed");
              return ADSP_EFAILED;
          }
        }
        else
        {
          MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: NULL pointer Error: No memory is allocated for dtmf gen resampler process");
          return ADSP_EFAILED;
        }
      }

      if(dtmf_gen_ptr->dtmf_gen_mix_flag)
      {
         //Apply DTMF gen gain on DTMF generated samples
         volume_control(dtmf_gen_samples, dtmf_gen_samples, num_input_buf_samples, dtmf_gen_ptr->dtmf_gen_gain);

         // mixing with voice samples
         dtmf_gen_mix_with_voice(dtmf_gen_samples,out_ptr,num_input_buf_samples);
      }
      else
      {
         //Apply DTMF gen gain on DTMF generated samples
         volume_control(dtmf_gen_samples, out_ptr, num_input_buf_samples, dtmf_gen_ptr->dtmf_gen_gain);
      }
   }

   return ADSP_EOK;
}

ADSPResult voice_dtmf_gen_process_end(dtmf_gen_struct_t *dtmf_gen_ptr)
{
   // Reset all variables, safe in the event of a forced MS_STOP
   dtmf_gen_ptr->dtmf_gen_tone1        = 1209;
   dtmf_gen_ptr->dtmf_gen_tone2        = 697;
   dtmf_gen_ptr->duration_in_micro_secs     = 0;
   dtmf_gen_ptr->dtmf_gen_gain         = 0x2000; //unity in unsigned Q13 format.
   dtmf_gen_ptr->dtmf_gen_mix_flag     = FALSE;
   dtmf_gen_ptr->current_dtmf_state    = DTMF_STOP;
   dtmf_gen_ptr->dtmf_gen_last_block   = FALSE;
   dtmf_gen_ptr->second_dtmf_command   = FALSE;
   dtmf_gen_ptr->second_dtmf_samples_in_block1 = 0;
   return ADSP_EOK;
}
ADSPResult voice_dtmf_gen_start(dtmf_gen_struct_t *dtmf_gen_ptr,uint16_t tone1,uint16_t tone2,int32_t duration,
                                uint16_t dtmf_mix_enable,uint16_t dtmf_gain)
{

   if(VOICE_DTMF_GEN_CONTINUOUS == duration)
   {
      dtmf_gen_ptr->duration_in_micro_secs = VOICE_DTMF_GEN_CONTINUOUS;
   }
   else if(VOICE_DTMF_GEN_STOP == duration)
   {
      dtmf_gen_ptr->duration_in_micro_secs = VOICE_DTMF_GEN_STOP;
      dtmf_gen_ptr->dtmf_gen_last_block =TRUE;
   }
   else
   {
      /* convert duration in to micro seconds as per lib expectation*/
      dtmf_gen_ptr->duration_in_micro_secs = duration*1000;
   }

   if (DTMF_RUN == dtmf_gen_ptr->current_dtmf_state)
   {
      // Copy function parameters to member variables.
      dtmf_gen_ptr->dtmf_gen_tone1 = tone1;
      dtmf_gen_ptr->dtmf_gen_tone2 = tone2;

      if(VOICE_DTMF_GEN_STOP != duration)
      {
         dtmf_gen_ptr->second_dtmf_command=TRUE;
      }

      dtmf_gen_ptr->dtmf_gen_mix_flag = dtmf_mix_enable;
      dtmf_gen_ptr->dtmf_gen_gain = dtmf_gain;

      if(SIXTEEN < dtmf_gen_ptr->sampling_freq)
      {
          //No resampler initialize is required if already in DTMF_RUN
          dtmf_tone_init_general(&(dtmf_gen_ptr->dtmf_generator),tone1, tone2, dtmf_gen_ptr->duration_in_micro_secs,
                  EIGHT, 0, 0);
      }
      else
      {
      dtmf_tone_init_general(&(dtmf_gen_ptr->dtmf_generator),tone1, tone2, dtmf_gen_ptr->duration_in_micro_secs,
      dtmf_gen_ptr->sampling_freq, 0, 0);
      }

      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Second DTMF Command issues while DTMF is already running\n");
      return ADSP_EOK;
   }
   else
   {
      // Copy function parameters to member variables.
      dtmf_gen_ptr->dtmf_gen_tone1 = tone1;
      dtmf_gen_ptr->dtmf_gen_tone2 = tone2;

      if(VOICE_DTMF_GEN_STOP != duration)
      {
         dtmf_gen_ptr->dtmf_gen_last_block =FALSE;
      }

      dtmf_gen_ptr->dtmf_gen_mix_flag = dtmf_mix_enable;

      // This flag must be enabled when a DTMF Start command is received
      dtmf_gen_ptr->current_dtmf_state = DTMF_RUN;
      dtmf_gen_ptr->dtmf_gen_gain = dtmf_gain;

      if(SIXTEEN < dtmf_gen_ptr->sampling_freq)
      {

        (void)dtmf_tone_init(&(dtmf_gen_ptr->dtmf_generator),
                dtmf_gen_ptr->dtmf_gen_tone1,
                dtmf_gen_ptr->dtmf_gen_tone2,
                dtmf_gen_ptr->duration_in_micro_secs,
                EIGHT
                );

        //Channel init for resampler
        if(NULL != dtmf_gen_ptr->dtmf_resampler_channel_mem_ptr)
        {
            int32 resampler_result = voice_resampler_channel_init(&(dtmf_gen_ptr->dtmf_resampler_cfg_str),
                    dtmf_gen_ptr->dtmf_resampler_channel_mem_ptr,
                    dtmf_gen_ptr->dtmf_resampler_cfg_str.total_channel_mem_size
                    );

            if( VOICE_RESAMPLE_SUCCESS != resampler_result )
            {
                MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: DTMF Gen upsampler init failed");
            qurt_elite_memory_free(dtmf_gen_ptr->dtmf_resampler_channel_mem_ptr);
                dtmf_gen_ptr->dtmf_resampler_channel_mem_ptr = NULL;
                return ADSP_EFAILED;
            }
        }
        else
        {
            MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Error: No Memory is allocated for Resampler for DTMF Gen. skip resample to 48 kHz");
            return ADSP_EFAILED;
        }
      }
      else
      {
      (void) dtmf_tone_init(&(dtmf_gen_ptr->dtmf_generator),
            dtmf_gen_ptr->dtmf_gen_tone1,
            dtmf_gen_ptr->dtmf_gen_tone2,
            dtmf_gen_ptr->duration_in_micro_secs,
            dtmf_gen_ptr->sampling_freq);
      }

      return ADSP_EOK;
   }
}

ADSPResult voice_dtmf_gen_get_kpps(dtmf_gen_struct_t *dtmf_gen_ptr,uint32_t* kpps_ptr, uint16_t sampling_rate)
{
   ADSPResult result = ADSP_EOK;


   if ((NULL!= dtmf_gen_ptr) && (NULL!= kpps_ptr))
   {
      *kpps_ptr = 0;
      if(DTMF_RUN == dtmf_gen_ptr->current_dtmf_state)
      {
         result = vcmn_find_kpps_table(VOICE_DTMFGEN_SAMPLING_KPPS_TABLE, sampling_rate, kpps_ptr);
      }
   }
   else
   {
      result = ADSP_EBADPARAM;
   }
   return result;
}

