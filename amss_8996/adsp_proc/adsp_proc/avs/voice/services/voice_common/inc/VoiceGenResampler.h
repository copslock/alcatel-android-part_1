/*========================================================================
Elite

This file contains Generic Resampler APIs for Voice.

Copyright (c) 2010-2011,2012 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government, Diversion contrary to U.S. law prohibited.
*//*====================================================================== */

/*========================================================================
Edit History

$Header: //components/rel/avs.adsp/2.7.1.c4/voice/services/voice_common/inc/VoiceGenResampler.h#1 $

when       who     what, where, why
--------   ---     -------------------------------------------------------
08/27/12   pb     Initial version

Supported Utils :
1) 24 bit generic resampler
========================================================================== */
#ifndef VOICEGENRESAMPLER_H
#define VOICEGENRESAMPLER_H

/* =======================================================================
INCLUDE FILES FOR MODULE
========================================================================== */
#include "VoiceCmnUtils.h"
#include "GenericResamplerLib24SRC.h"

#ifdef __cplusplus
extern "C" {
#endif //__cplusplus

/*-------------------------------------------------------------------------
Type Declarations
-------------------------------------------------------------------------*/

/** Defines for channel memory and instance memory
 */
enum resampler_mem
{
    CHANNEL_MEM = 0,
    INSTANCE_MEM
};

/** Structure for each channel of the resampler
 */
typedef struct resampler_channel_t
{
   uint32_t            channel_size;
   void                *channel_memory_ptr;
} resampler_channel_t;

/** Structure for instance of the resampler
 */
typedef struct resampler_instance_t
{
   uint32_t            instance_size;
   void                *instance_memory_ptr;
} resampler_instance_t;

/** Structure of the generic resampler
 */
typedef struct voice_generic_resampler_t
{
   resampler_channel_t  channel;
   resampler_instance_t instance;
} voice_generic_resampler_t;

#define GEN_RESAMP_FIXED_OUTPUT_MODE (2)

/**
 Given the resampling parameters, this API returns the combined size required for the channel memory and instance memory.
 @param [in] Quality of resampling required/
 @param [in] Num of bytes per sampler/
 @param [out] generic_resampler instance populated with appropriate size requirements.
 @param [out] combined size required for the channel memory and instance memory.
*/
uint32_t voice_gen_resamp_get_size(int16_t quality, uint8_t num_bytes_per_sample, voice_generic_resampler_t *gen_resampler_ptr);

/**
 Given the resampling parameters, this API initializes the memory pointers for the channel and the instance.
 @param [in] generic resampler instance
 @param [in] memory start pointer
 @param [in] available size
 @param [out] generic_resampler instance memory pointers are updated to point to valid memory locations
 @param [out] Returns size of memory allocated
*/
ADSPResult voice_gen_resamp_set_mem(voice_generic_resampler_t *gen_resampler_ptr, int8_t* mem_ptr, uint32_t size);

/**
 Makes NULL the channel and instance pointers of generic resampler
 @param [in] structure - pointer to voice_generic_resampler_t
 @param [in] input sampling rate
 @param [in] output sampling rate
 @param [in] mode for which the internal resampler structures need to be initialized
             Bit 0 --> bits per sample
             0 : 16, 1: 32
             Bit 1 --> FIXED_INPUT/OUTPUT mode of operation
             0 : FIXED_INPUT, 1 : FIXED_OUTPUT
             Bit 5:2 --> Channel number
             Bit 9:6 --> Quality
 @param [out] Returns a SUCCESS/FAILURE code.
*/
ADSPResult voice_gen_resamp_init(voice_generic_resampler_t *gen_resampler_ptr, int32_t in_samp_rate, int32_t out_samp_rate, int16_t mode);

/**
 Generates the resampled output
 @param [in] structure - pointer to voice_generic_resampler_t
 @param [in] input buffer pointer
 @param [in] output buffer pointer
 @param [in] input sample count
 @param [in] output sample count
 @param [in] mode
 @param [out] output buffer with resampled outputs
 @param [out] Returns a SUCCESS/FAILURE code.
*/
ADSPResult voice_gen_resamp_process(voice_generic_resampler_t *gen_resampler_ptr, int32_t *in_ptr, int32_t *out_ptr, int32_t in_cnt, int32_t out_cnt, int16_t mode);

/**
 Makes NULL the channel and instance pointers of generic resampler
 @param [in] structure - pointer to voice_generic_resampler_t
*/
void voice_gen_resamp_end(voice_generic_resampler_t *gen_resampler_ptr);


#ifdef __cplusplus
}
#endif //__cplusplus

#endif // #ifndef VOICEGENRESAMPLER_H

