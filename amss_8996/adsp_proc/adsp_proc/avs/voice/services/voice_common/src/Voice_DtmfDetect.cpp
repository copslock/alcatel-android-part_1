/*============================================================================
                             Edit History

$Header: //components/rel/avs.adsp/2.7.1.c4/voice/services/voice_common/src/Voice_DtmfDetect.cpp#1 $

when       who     what, where, why
--------   ---     -------------------------------------------------------
15/07/2010 sb      calibration added.
08/07/2010 sb      converted from C++ to C wrapper
10/12/09   cp      Created file.

============================================================================*/

#define  NB_VOCODER_FRAME_SIZE               160   // nb vocoder frame size
#define  WB_VOCODER_FRAME_SIZE               320   // nb vocoder frame size
#define  DTMF_TONE_STATUS_PAYLOAD_SIZE_X2      8   // each dtmf tone status needs 2*int32_t
                                                   // buffering up to two tones. 8*int16_t
#define  FRAME_SIZE_DTMF_DETECTION           105   // @8KHz,corresponds to 13.125msec
/*============================================================================
 *                       INCLUDE FILES FOR MODULE
 *==========================================================================*/
#include "Voice_DtmfDetect.h"
#include "adsp_vparams_api.h"
#include "VoiceCmnUtils.h"
extern "C" {
    #include "dtmf_det_api.h"
 }

/* KPPS values */
#define VOICE_DTMFDET_NB_KPPS ( 520)
#define VOICE_DTMFDET_WB_KPPS (850)
#define VOICE_DTMFDET_SWB_KPPS (VOICE_DTMFDET_NB_KPPS + VOICE_RESAMPLER_KPPS_32K_TO_8K)
#define VOICE_DTMFDET_FB_KPPS (1300)

static const vcmn_sampling_kpps_t VOICE_DTMFDET_SAMPLING_KPPS_TABLE[] = {{VOICE_NB_SAMPLING_RATE, VOICE_DTMFDET_NB_KPPS },
                                                                       {VOICE_WB_SAMPLING_RATE, VOICE_DTMFDET_WB_KPPS },
                                                                       {VOICE_SWB_SAMPLING_RATE, VOICE_DTMFDET_SWB_KPPS },
                                                                       {VOICE_FB_SAMPLING_RATE, VOICE_DTMFDET_FB_KPPS },
                                                                       {VOICE_INVALID_SAMPLING_RATE, 0}};

static const uint16_t dtmf_low_frequency[4] =  {697,770,852,941};
static const uint16_t dtmf_high_frequency[4] =  {1209,1336,1477,1633};

ADSPResult voice_dtmf_detect_init(dtmf_detect_struct_t *dtmf_detect_ptr,uint32_t sampling_rate,uint32_t num_in_samples)
{
   int32 nResult;
   uint32 num_out_samples;

   dtmf_detect_ptr->enable_dtmf_detect = FALSE;
   //initialize the DTMF DETECTION static memory
   dtmf_det_init_default(dtmf_detect_ptr->dtmf_detect_static_mem);
   dtmf_detect_ptr->prev_tone_status=0;
   // Initialize Circular Buffer for DtmfDet
   voice_circbuf_init(&(dtmf_detect_ptr->circ_struct_dtmf_det), (int8_t*)(dtmf_detect_ptr->circ_buf),
      (int32_t)NB_VOCODER_FRAME_SIZE*2 ,MONO_VOICE,(int32_t)NO_OF_BITS_PER_SAMPLE /*bitperchannel*/);

   // Initialize Circular Buffer for ToneStatus
   voice_circbuf_init(&(dtmf_detect_ptr->tone_status_circbuf_struct),
                      (int8_t*)(dtmf_detect_ptr->tone_status_buf),
                      (int32_t)DTMF_TONE_STATUS_PAYLOAD_SIZE_X2 ,
                       MONO_VOICE,
                       (int32_t)NO_OF_BITS_PER_SAMPLE /*bitperchannel*/);

   dtmf_detect_ptr->sampling_rate = sampling_rate;
   // Do set config for voice resampler
   nResult= voice_resampler_set_config(
            &(dtmf_detect_ptr->dtmf_det_config_rs),
                  sampling_rate,
                  VOICE_NB_SAMPLING_RATE,
                  NO_OF_BITS_PER_SAMPLE,
                  num_in_samples,
                  &num_out_samples );
   if(VOICE_RESAMPLE_FAILURE == nResult)
     {
       MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: voice_resampler_set_config():: Failed to set config for resampler in DTMF detection init");
       return ADSP_EFAILED;
     }

   // Calculate channel memory size
   nResult= voice_resampler_get_channel_mem(&(dtmf_detect_ptr->dtmf_det_config_rs),(uint32 *)&(dtmf_detect_ptr->dtmf_det_resamp_channel_size));
   dtmf_detect_ptr->dtmf_det_resamp_channel_mem_ptr = qurt_elite_memory_malloc(dtmf_detect_ptr->dtmf_det_resamp_channel_size,
                                                                             QURT_ELITE_HEAP_DEFAULT);
   if(NULL == dtmf_detect_ptr->dtmf_det_resamp_channel_mem_ptr)
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Out of memory for voice resampler in DTMF detection init");
      return (ADSP_ENOMEMORY);
   }
   nResult= voice_resampler_channel_init(&(dtmf_detect_ptr->dtmf_det_config_rs),dtmf_detect_ptr->dtmf_det_resamp_channel_mem_ptr,(uint32)dtmf_detect_ptr->dtmf_det_resamp_channel_size);
  // memset(dtmf_detect_ptr->dtmf_det_downBy2_filter_mem, 0, GetMemSizeOfDownBy2Filter());
   if(VOICE_RESAMPLE_FAILURE == nResult)
     {
       MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: voice_resampler_channel_init():: Failed to initialize channel memory for resampler in DTMF detection");
       qurt_elite_memory_free(dtmf_detect_ptr->dtmf_det_resamp_channel_mem_ptr);
       dtmf_detect_ptr->dtmf_det_resamp_channel_mem_ptr = NULL;
       return ADSP_EFAILED;
     }
   return (ADSP_EOK);
}

void voice_dtmf_detect_end(dtmf_detect_struct_t *dtmf_detect_ptr)
{
   voice_circbuf_reset(&(dtmf_detect_ptr->circ_struct_dtmf_det));
   voice_circbuf_reset(&(dtmf_detect_ptr->tone_status_circbuf_struct));
   if(NULL != dtmf_detect_ptr->dtmf_det_resamp_channel_mem_ptr)
   {
      qurt_elite_memory_free(dtmf_detect_ptr->dtmf_det_resamp_channel_mem_ptr);
      dtmf_detect_ptr->dtmf_det_resamp_channel_mem_ptr = NULL;
   }

}

void voice_dtmf_detect_process(dtmf_detect_struct_t *dtmf_detect_ptr,
                               int16_t *out_ptr,
                               int16_t *in_ptr,
                               uint16_t no_of_samples,
                               uint16_t sampling_rate)
{
   int16_t tone_status[2],current,update_flag = 0;
    int8_t *tone_status_ptr;
    tone_status_ptr = (int8_t *)(&tone_status[0]);
   int16_t inp_nb[NB_VOCODER_FRAME_SIZE];
   uint16_t rs_out_no_of_samples=(no_of_samples*VOICE_NB_SAMPLING_RATE)/sampling_rate;
    if(TRUE == dtmf_detect_ptr->enable_dtmf_detect)
    {
        if(VOICE_NB_SAMPLING_RATE != sampling_rate)
        {
           voice_resampler_process(&(dtmf_detect_ptr->dtmf_det_config_rs),dtmf_detect_ptr->dtmf_det_resamp_channel_mem_ptr,
                                (int8 *)in_ptr,no_of_samples,(int8 *)inp_nb,rs_out_no_of_samples);
  /*         DownSampleBy2(in_ptr, inp_nb, no_of_samples,
                         (int16_t*)dtmf_detect_ptr->dtmf_det_downBy2_filter_mem);
  */       in_ptr = inp_nb;
           no_of_samples = rs_out_no_of_samples;
        }
        dtmf_detect_ptr->dtmf_tone_status = 0;
        voice_circbuf_write(&(dtmf_detect_ptr->circ_struct_dtmf_det), (int8_t*)in_ptr,no_of_samples);

        //DTMF DETECTION block size is 105 samples @8KHz (13.125msec)
        //Call the Detection block until we have a min unread samples of 105
        while (dtmf_detect_ptr->circ_struct_dtmf_det.unread_samples >= (FRAME_SIZE_DTMF_DETECTION))
        {
            int16_t   naDtmfDetectInp[FRAME_SIZE_DTMF_DETECTION];

            voice_circbuf_read(&(dtmf_detect_ptr->circ_struct_dtmf_det),(int8_t *)naDtmfDetectInp,FRAME_SIZE_DTMF_DETECTION, sizeof(naDtmfDetectInp) );
            tone_status[0] = dtmf_detect(&naDtmfDetectInp[0],dtmf_detect_ptr->dtmf_detect_static_mem);
            current = do_dtmf_debounce(tone_status[0],dtmf_detect_ptr->dtmf_detect_static_mem);
            //save the tone status in the toneStatusBuf
            if ((dtmf_detect_writeflag(dtmf_detect_ptr->prev_tone_status,current))&&(0 != current))
            {
                tone_status[1] = (current & 0xF0) >> 4; // high freq in enum
                tone_status[0] = current & 0x0F; // low freq in enum
                //convert to DTMF detection payload as per the API
                //low freq, high freq in hz
                tone_status[0] = dtmf_low_frequency[voice_get_high_lsb(tone_status[0])]; // index offset by -1, as table stores frequencies from index 1
                tone_status[1] = dtmf_high_frequency[voice_get_high_lsb(tone_status[1])];

                voice_circbuf_write(&(dtmf_detect_ptr->tone_status_circbuf_struct), tone_status_ptr,2);  // 2 16 bit samples => 4 byte tone struct
                update_flag = 1;
            }
            dtmf_detect_ptr->prev_tone_status=current;
        }
        if (1 == update_flag)
        {
            update_flag = 0;
            // update the flag to send the status to client :: VPM_EVT_SEND_DTMF_STATUS
            dtmf_detect_ptr->dtmf_tone_status = 1;
        }
    }
}

uint8_t voice_dtmf_detect_get_tone_status(dtmf_detect_struct_t *dtmf_detect_ptr)
{
    if(TRUE == dtmf_detect_ptr->enable_dtmf_detect)
   {
       return dtmf_detect_ptr->dtmf_tone_status;
   }
   else
   {
       return FALSE;
   }
}
ADSPResult voice_dtmf_detect_set_param(dtmf_detect_struct_t *dtmf_detect_ptr,int8_t *params_buffer_ptr,uint32_t param_id,uint16_t param_size)
{
   ADSPResult result = ADSP_EOK;
   if(NULL == params_buffer_ptr)
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: voice_dtmf_detect_set_param():: NULL Pointer error");
      return ADSP_EFAILED;
   }
   switch(param_id)
   {
      case VOICE_PARAM_DTMF_DETECTION:
         {
            dtmf_detect_interface_params *pInterfaceParams = (dtmf_detect_interface_params*)params_buffer_ptr;
            if (sizeof(dtmf_detect_interface_params) != param_size)
            {
               MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: voice_dtmf_detect_set_param():: Bad Param Size");
               result = ADSP_EBADPARAM;
               break;
            }
            dtmf_det_init(dtmf_detect_ptr->dtmf_detect_static_mem, (int16_t *)pInterfaceParams);
            result= voice_resampler_channel_init(&(dtmf_detect_ptr->dtmf_det_config_rs),dtmf_detect_ptr->dtmf_det_resamp_channel_mem_ptr,(uint32)dtmf_detect_ptr->dtmf_det_resamp_channel_size);
            result = ADSP_EOK;
            break;
         }
      case VOICE_PARAM_MOD_ENABLE:
         {
            if (sizeof(int32_t) != param_size)
            {
               MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: voice_dtmf_detect_set_param():: Bad Param Size");
               result = ADSP_EBADPARAM;
               break;
            }
            dtmf_detect_ptr->enable_dtmf_detect = *((int16_t*)params_buffer_ptr);
            if (TRUE == dtmf_detect_ptr->enable_dtmf_detect)
            {
               // clear the DTMF DETECTION static memory
               memset( (int8_t *)(dtmf_detect_ptr->dtmf_detect_static_mem + ((sizeof(dtmf_detect_interface_params))/2 )),0,
                       (sizeof(dtmf_detect_ptr->dtmf_detect_static_mem ) - sizeof(dtmf_detect_interface_params)));
               result= voice_resampler_channel_init(&(dtmf_detect_ptr->dtmf_det_config_rs),dtmf_detect_ptr->dtmf_det_resamp_channel_mem_ptr,(uint32)dtmf_detect_ptr->dtmf_det_resamp_channel_size);
            }
            result = ADSP_EOK;
            break;
         }
      default:
         result = ADSP_EBADPARAM;
         MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: voice_dtmf_detect_set_param() :: Module ID not support \n");
   }
   return result;
}
ADSPResult voice_dtmf_detect_get_param(dtmf_detect_struct_t *dtmf_detect_ptr,int8_t *params_buffer_ptr,uint32_t param_id,int32_t buffer_size, uint16_t *param_size_ptr)
{
   if(NULL == params_buffer_ptr)
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: voice_dtmf_detect_get_param():: NULL Pointer error");
      return ADSP_EFAILED;
   }
   switch(param_id)
   {
      case VOICE_PARAM_DTMF_DETECTION:
         {
            *param_size_ptr = sizeof(dtmf_detect_interface_params);
            if(*param_size_ptr > buffer_size )
            {
               MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: voice_dtmf_detect_get_param required size = %d, given size = %ld",*param_size_ptr, buffer_size);
               return ADSP_ENOMEMORY;
            }

            int16_t *params = dtmf_detect_ptr->dtmf_detect_static_mem;
            dtmf_detect_interface_params *pInterfaceParams = (dtmf_detect_interface_params*)params_buffer_ptr;
            memscpy((int8_t*)pInterfaceParams, buffer_size, (int8_t*)params, *param_size_ptr);
         }
         break;
      case VOICE_PARAM_MOD_ENABLE:
         {
            *param_size_ptr = sizeof(int32_t);
            if(*param_size_ptr > buffer_size )
            {
               MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: voice_dtmf_detect_get_param required size = %d, given size = %ld",*param_size_ptr, buffer_size);
               return ADSP_ENOMEMORY;
            }
            *((int32_t*)params_buffer_ptr)=0; // Clearing the whole buffer
            *((int16_t*)params_buffer_ptr) = dtmf_detect_ptr->enable_dtmf_detect;
         }
         break;
      default:
         MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: voice_dtmf_detect_get_param() :: Module ID not support \n");
         return ADSP_EUNSUPPORTED;

   }
   return ADSP_EOK;
}
ADSPResult dtmf_det_resampler_init(
      dtmf_detect_struct_t *dtmf_detect_ptr,
      uint32 input_sampling_rate)
{
   int32 result;
   uint32 num_in_samples,num_out_samples;
   num_in_samples=NB_VOCODER_FRAME_SIZE*input_sampling_rate/VOICE_NB_SAMPLING_RATE;
   result = voice_resampler_set_config(
      &(dtmf_detect_ptr->dtmf_det_config_rs),
      input_sampling_rate,
      VOICE_NB_SAMPLING_RATE,
      NO_OF_BITS_PER_SAMPLE,
      num_in_samples,
      &num_out_samples
      );
   if (VOICE_RESAMPLE_FAILURE == result)
   {
      return ADSP_EFAILED;
   }
   result =voice_resampler_channel_init(
      &(dtmf_detect_ptr->dtmf_det_config_rs),
      dtmf_detect_ptr->dtmf_det_resamp_channel_mem_ptr,
      dtmf_detect_ptr->dtmf_det_resamp_channel_size
      );
   if (VOICE_RESAMPLE_FAILURE == result)
   {
      qurt_elite_memory_free(dtmf_detect_ptr->dtmf_det_resamp_channel_mem_ptr);
      dtmf_detect_ptr->dtmf_det_resamp_channel_mem_ptr = NULL;
      return ADSP_EFAILED;
   }
   return ADSP_EOK;
}

ADSPResult voice_dtmf_det_get_kpps(dtmf_detect_struct_t* dtmf_detect_ptr, uint32_t* kpps_ptr, uint16_t sampling_rate)
{
   ADSPResult result = ADSP_EOK;

   if ((NULL!= dtmf_detect_ptr) && (NULL!= kpps_ptr))
   {
      *kpps_ptr = 0;
      if(TRUE == dtmf_detect_ptr->enable_dtmf_detect)
      {
         result = vcmn_find_kpps_table(VOICE_DTMFDET_SAMPLING_KPPS_TABLE, sampling_rate, kpps_ptr);
      }
   }
   else
   {
      result = ADSP_EBADPARAM;
   }
   return result;
}

