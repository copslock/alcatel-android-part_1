/*========================================================================

*//** @file VoiceDevMgr.cpp
This file Voice service - control logic for voice call/voip calls.
and in call features.

Copyright (c) 2009 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government, Diversion contrary to U.S. law prohibited.
*//*====================================================================== */

/*========================================================================
Edit History

$Header: //components/rel/avs.adsp/2.7.1.c4/voice/services/voice_proc_mgr/src/VoiceDevSvc.cpp#1 $

when       who     what, where, why
--------   ---     -------------------------------------------------------
10/06/09   vn      Created file.

========================================================================== */


/* =======================================================================
INCLUDE FILES FOR MODULE
========================================================================== */
#include "avtimer_drv_api.h"
#include "qurt_elite.h"
#include "Elite.h"
#include "VoiceDevSvc.h"
#include "VoiceProcTx.h"
#include "VoiceProcRx.h"
#include "AFEDevService.h"
#include "EliteMsg_Util.h"
#include "EliteMsg_AfeCustom.h"
#include "VoiceMixerSvc.h"
#include "VoiceCmnUtils.h"
#include "adsp_vcmn_api.h"
#if defined(__qdsp6__) && !defined(SIM)
#include "VoiceLoggingUtils.h"
#endif

/* APR/API related */
#include "EliteAprIf.h"
#include "aprv2_ids_domains.h"
#include "adsp_vpm_api.h"
#include "adsp_vparams_api.h"
#include "adsp_media_fmt.h"

#include "mvm_api_i.h" //for calibration utility for set param

/*--------------------------------------------------------------*/
/* Macro definitions                                            */
/* -------------------------------------------------------------*/
#define VPTX 0
#define VPRX 1
#define FARSRC 1
/* -----------------------------------------------------------------------
** Constant / Define Declarations
** ----------------------------------------------------------------------- */
// maximum number of commands expected ever in command queue.
static const uint32_t MAX_CMD_Q_ITEMS = 16; // power of 2


static char_t VPM_THREAD_NAME[]="VPM";

static const uint32_t VPM_THREAD_STACK_SIZE = (12*1024);

// This contains all the required data for a service instance.
static char_t cmdQName[]="VpmCmdQ";

static VoiceProcMgr_t *me = NULL;

uint32_t vPM_memory_map_client = 0;

//TODO: use proper include files here
#define NO_INTERFACE (0xFFFF)
extern elite_svc_handle_t *txVoiceMatrix;
extern elite_svc_handle_t *rxVoiceMatrix;
static ADSPResult create_tx_mixer_input( int32_t nSessionIndex, uint32_t priority);
static ADSPResult create_rx_mixer_output( int32_t nSessionIndex, uint16_t samplingRate);
static ADSPResult reconfigure_tx_mixer_input( int32_t nSessionIndex, uint32_t priority);
static ADSPResult reconfigure_rx_mixer_output( int32_t nSessionIndex, uint16_t samplingRate);
static ADSPResult close_tx_mixer_input(int32_t nSessionIndex);
static ADSPResult close_rx_mixer_output(int32_t nSessionIndex);
static ADSPResult control_mixer_ports(void* pInstance, uint32_t nSessionIndex, uint32_t command);

/* -----------------------------------------------------------------------
 ** Function prototypes
 ** ----------------------------------------------------------------------- */
// no destructor needed for static service

// Main work loop for service thread. Pulls msgs off of queues and processes them.
static int voice_proc_mgr_thread(void* pInstance);

// message handler functions
static ADSPResult vpm_custom_msg(void* pInstance, elite_msg_any_t* pMsg);
static ADSPResult voice_proc_apr(void* pInstance, elite_msg_any_t* pMsg);
static int32_t voice_proc_create(void* pInstance, void* pMsg, uint16_t* handle, uint32_t opcode);
static int32_t voice_proc_destroy(void* pInstance, uint16_t nSessionIndex);
static int32_t voice_proc_reinit(void* pInstance, void * pMsg, uint16_t nSessionIndex, uint32 opcode);
static int32_t voice_proc_run(void* pInstance, uint16_t nSessionIndex, uint32_t session_thread_clock_mhz);
static int32_t voice_proc_stop(void* pInstance, uint16_t nSessionIndex);
static ADSPResult vpm_set_shared_objs_handler(void* pInstance, elite_msg_any_t* pMsg);

static ADSPResult vpm_send_tx_mute( void* pInstance, uint32_t nSessionIndex, voice_set_soft_mute_v2_t *pCmd);
static ADSPResult vpm_send_rx_mute( void* pInstance, uint32_t nSessionIndex, voice_set_soft_mute_v2_t *pCmd);
static ADSPResult vpm_send_start_tx_host_pcm( void* pInstance, uint32_t nSessionIndex, elite_apr_packet_t *pPacket);
static ADSPResult vpm_send_start_rx_host_pcm( void* pInstance, uint32_t nSessionIndex, elite_apr_packet_t *pPacket);
static ADSPResult vpm_send_stop_tx_host_pcm( void* pInstance, uint32_t nSessionIndex, elite_apr_packet_t *pPacket);
static ADSPResult vpm_send_stop_rx_host_pcm( void* pInstance, uint32_t nSessionIndex, elite_apr_packet_t *pPacket);

static ADSPResult connect_afe_tx(void* pInstance, uint16_t nSessionIndex, uint16_t samplingRate, uint16_t txPort, uint16_t rxPort /* FARSRC */, uint32_t nTopologyId);
static ADSPResult connect_afe_rx(void* pInstance, uint16_t nSessionIndex, uint16_t samplingRate, uint16_t rxPort);
static ADSPResult disconnect_afe_tx(void* pInstance, uint16_t nSessionIndex, uint16_t txPort, uint16_t rxPort /* FARSRC */);
static ADSPResult disconnect_afe_rx(void* pInstance, uint16_t nSessionIndex, uint16_t rxPort);
static ADSPResult send_disconnect_marker_to_afe(void* pInstance, uint16_t nSessionIndex, uint16_t rxPort);
static ADSPResult send_afe_tx_dataq_destroyed_msg( void* pInstance, uint16_t nSessionIndex, uint16_t txPort, uint16_t rxPort /* FARSRC */);
static ADSPResult voice_proc_connect_peer(uint32_t unSecOpCode, qurt_elite_queue_t *CmdQHandle, qurt_elite_queue_t *RespQHandle, elite_svc_handle_t *Peer);
static ADSPResult is_pseudo_port(uint16_t port);
static ADSPResult vpm_set_timing_params( void *pInstance, elite_msg_any_t *pMsg);
static ADSPResult vpm_set_timingv2_params( void *pInstance, elite_msg_any_t *pMsg);
static ADSPResult vpm_set_timingv3_params( void *pInstance, elite_msg_any_t *pMsg);
static ADSPResult vpm_send_vfr_mode( void* pInstance, uint32_t nSessionIndex, uint32_t nFreeIndex, int32_t *pVFRCmd, uint32_t nDirection, uint32_t sec_opcode);
static ADSPResult vpm_send_custom_voiceproc_run_msg(qurt_elite_queue_t *respQ, qurt_elite_queue_t *destQ, uint32_t sec_opcode,
      uint32_t client_token, bool_t wait_for_ack, void *data_ptr, void *far_src_drift_ptr,
      uint32_t session_thread_clock_mhz);
uint16_t vpm_get_ec_ref_port(vpm_create_session_v2_t* create_ptr);

static void vpm_set_mmpm_clocks(void *pInstance, uint32_t* session_thread_clock_mhz);
static ADSPResult vpm_process_cmd_queue(void* pInstance, elite_svc_handle_t *pHandle,
      elite_svc_msg_handler_func pHandler[], uint32_t handler_table_size);
static ADSPResult vpm_process_set_param_msg(void* instance_ptr, elite_msg_any_t* msg_ptr);
static ADSPResult vpm_update_additional_create_params(void *pMsg,vptx_create_params_t *create_param,uint8_t tx_rx_flag);

//SA: TODO: remove this after VSD integration
#define VPM_PARAM_NUM_DEV_CHANNELS 0x0001316B

struct vpm_ivocproc_cmd_topology_set_dev_channels_t
{
   uint16_t tx_num_channels;
   /**< Number of Mics.
                     @Supported values
                     - 1 - VSS_NUM_DEV_CHANNELS_1 \n
                     - 2 - VSS_NUM_DEV_CHANNELS_2 \n
                     - 3 - VSS_NUM_DEV_CHANNELS_3 \n
                     - 4 - VSS_NUM_DEV_CHANNELS_4 */
   uint16_t rx_num_channels;
   /**< Number of speaker channels.
                     @Supported values
                     - 1 - VSS_NUM_DEV_CHANNELS_1 */
};


/* -----------------------------------------------------------------------
 ** Message handler
 ** ----------------------------------------------------------------------- */
// when RUN or STOP is commanded.

static elite_svc_msg_handler_func pHandlerCustom[] =
{
   vpm_custom_msg,              //0 - ELITE_CMD_CUSTOM
   elite_svc_unsupported,          //1 - ELITE_CMD_START_SERVICE
   elite_svc_unsupported,          //2 - ELITE_CMD_DESTROY_SERVICE
   elite_svc_unsupported,          //3 - ELITE_CMD_CONNECT
   elite_svc_unsupported,          //4 - ELITE_CMD_DISCONNECT
   elite_svc_unsupported,          //5 - ELITE_CMD_PAUSE
   elite_svc_unsupported,          //6 - ELITE_CMD_RESUME
   elite_svc_unsupported,          //7 - ELITE_CMD_FLUSH
   elite_svc_unsupported,          //8 - ELITE_CMD_SET_PARAM
   elite_svc_unsupported,          //9 - ELITE_CMD_GET_PARAM
   elite_svc_unsupported,          //10 - ELITE_DATA_BUFFER
   elite_svc_unsupported,          //11 - ELITE_DATA_MEDIA_TYPE
   elite_svc_unsupported,          //12 - ELITE_DATA_EOS
   elite_svc_unsupported,          //13 - ELITE_DATA_RAW_BUFFER
   elite_svc_unsupported,          //14 - ELITE_CMD_STOP_SERVICE
   voice_proc_apr                 //15 - ELITE_APR_PACKET
};

/* VPM custom msg handler */
static elite_svc_msg_handler_func pHandlerCustomMsgs[VOICEDEV_NUM_MSGS] =
{
   vpm_set_shared_objs_handler,      // VOICEDEV_SET_SHARED_OBJECTS
};

/* =======================================================================
 **                          Function Definitions
 ** ======================================================================= */

static int32_t vpm_isr_recv_fn ( elite_apr_packet_t * pAprPacket, void* dispatch_data )
{
   uint32_t           payload_size;
   int32_t            rc = APR_EOK;
   elite_msg_any_t     sMsg;
   ADSPResult result;

   if ( pAprPacket == NULL )
   {
      return APR_EBADPARAM;
   }

   payload_size = elite_apr_if_get_payload_size( pAprPacket);


   /* debug info.  Destination address should be VDM domain/service ID to get to this ISR, DstPort
    * should be the session handle that this is directed to, in case a session has already been established
    */
   MSG_7(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VPM pAprPacket(%p): srcAddr(%x) srcPort(%x) dstPort(%x) opCode(0x%lx) token(0x%lx) Size(%ld)",
         pAprPacket,
         elite_apr_if_get_src_addr(pAprPacket),
         elite_apr_if_get_src_port(pAprPacket),
         elite_apr_if_get_dst_port(pAprPacket),
         elite_apr_if_get_opcode(pAprPacket),
         elite_apr_if_get_client_token(pAprPacket),
         (int32_t) payload_size);

   /* dispatch the packets to either the VDM manager or the endpoint dynamic service depending on the opcode.  Assuming
    * queue onto Elite service succeeds, the receiving service is expected to respond that the command was accepted,
    * to process, and free the APR packet when the requested operation is complete.  Current design is to use simple 8 byte
    * elite header to manage APR packet delivery, so no additional copies of APR payload is required, and FADD message
    * doesn't need to be obtained from buffer manager to handle delivery to the Elite service.  This assumes it is OK to hold
    * onto the incoming apr_packet_t *pAprPacket pointer until the packet is processed by the destination service.
    */

   /* Report command acceptance when requested. */
   if ( elite_apr_if_msg_type_is_cmd( pAprPacket) )
   {
      rc = elite_apr_if_send_event_accepted( me->apr_handle, pAprPacket);

      if( APR_EOK != rc)
      {
         (void) elite_apr_if_free( me->apr_handle, pAprPacket );
         MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: VPM Error result=%lx sending APR_IBASIC_RSP_ACCEPTED", rc);
         return APR_EOK;
      }
   }

   /* simple delivery...copy pointer.  receiver must free packet if queue'd successfully */
   sMsg.unOpCode = ELITE_APR_PACKET;
   sMsg.pPayload = (void *) pAprPacket;

   uint16_t handle = elite_apr_if_get_dst_port(pAprPacket);
   uint16_t nSessionIndex = voice_map_handle_to_session_index( handle);
   uint32_t cmd_opcode = elite_apr_if_get_opcode( pAprPacket);

   //For everything other than create/memory map/unmap, check for NULL session
   if (  (VPM_CMD_CREATE_SESSION_V2 != cmd_opcode)          &&
         (VPM_CMD_CREATE_SESSION_V3 != cmd_opcode)          &&
         (VOICE_CMD_SHARED_MEM_MAP_REGIONS != cmd_opcode)   &&
         (VOICE_CMD_SHARED_MEM_UNMAP_REGIONS != cmd_opcode)
      )
   {
      if( nSessionIndex >= VOICE_MAX_VP_SESSIONS)
      {
         MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Rcvd opCode(0x%lx) on invalid sessionIndex(%x)!!", elite_apr_if_get_opcode( pAprPacket), nSessionIndex);
         if ( elite_apr_if_msg_type_is_cmd( pAprPacket) )
         {
            rc = elite_apr_if_end_cmd( me->apr_handle, pAprPacket, APR_EBADPARAM);
         }
         else
         {
            rc = elite_apr_if_free( me->apr_handle, pAprPacket );
         }
         return APR_EOK; // returning OK since elite_apr_if_end_cmd/elite_apr_if_free is called with the required error code
      }
      if( NULL == me->session[nSessionIndex])
      {
         MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Rcvd opCode(0x%lx) on non-existent sessionIndex(%x)!!", elite_apr_if_get_opcode( pAprPacket), nSessionIndex);
         if ( elite_apr_if_msg_type_is_cmd( pAprPacket) )
         {
            rc = elite_apr_if_end_cmd( me->apr_handle, pAprPacket, APR_EHANDLE);
         }
         else
         {
            rc = elite_apr_if_free( me->apr_handle, pAprPacket );
         }
         return APR_EOK; // returning OK since elite_apr_if_end_cmd/elite_apr_if_free is called with the required error code
      }
   }

   switch( cmd_opcode )
   {
      /* test: route these to VPM */
      case VPM_CMD_CREATE_SESSION_V2:
      case VPM_CMD_CREATE_SESSION_V3:
      case VPM_CMD_REINIT_SESSION_V2:
      case VPM_CMD_REINIT_SESSION_V3:
      case VPM_CMD_DESTROY_SESSION:
      case VPM_CMD_START_SESSION:
      case VPM_CMD_STOP_SESSION:
      case VOICE_CMD_SET_SOFT_MUTE_V2:
      case VOICE_CMD_START_HOST_PCM_V2:
      case VOICE_CMD_STOP_HOST_PCM:
      case VOICE_EVT_PUSH_HOST_BUF_V2:
      case VOICE_CMD_SET_PARAM_V2:
      case VOICE_CMD_SET_PARAM_V3:
      case VOICE_CMD_GET_PARAM_V2:
      case VPM_CMD_GET_KPPS:
      case VPM_CMD_GET_DELAY:
      case VOICE_CMD_SET_TIMING_PARAMS:
      case VPM_CMD_SET_TIMING_PARAMS:
      case VPM_CMD_SET_TIMING_PARAMS_V2:
      case VOICE_CMD_SHARED_MEM_MAP_REGIONS:
      case VOICE_CMD_SHARED_MEM_UNMAP_REGIONS:
         {
            MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Dispatching APR message from VPM callback");
            /* Queue packet onto VSM command Q via standard Elite op/payload format */
            if (ADSP_FAILED(result =qurt_elite_queue_push_back(me->svc_handle.cmdQ,(uint64_t*)&sMsg)))
            {
               MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: VSM APR isr Q to manager: qurt_elite_queue_push_back error = %d!!\n", result);
               if ( elite_apr_if_msg_type_is_cmd( pAprPacket) )
               {
                  rc = elite_apr_if_end_cmd( me->apr_handle, pAprPacket, APR_EBUSY );
               }
               else
               {
                  rc = elite_apr_if_free( me->apr_handle, pAprPacket );
               }
            }

            break;
         }
      case VPM_CMD_SET_TX_DTMF_DETECTION:
         {
            /* check if valid tx session exists */
            if (NULL != me->session[nSessionIndex]->tx)
            {
               if (ADSP_FAILED(result =qurt_elite_queue_push_back(me->session[nSessionIndex]->tx->cmdQ,(uint64_t*)&sMsg)))
               {
                  rc = elite_apr_if_end_cmd( me->apr_handle, pAprPacket, APR_EBUSY );
                  MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: VPM APR isr Q to endpoint: qurt_elite_queue_push_back error = %d!!\n",(int) result);
               }
            }
            else
            {
               rc = elite_apr_if_end_cmd( me->apr_handle, pAprPacket, APR_ENOTREADY );
               MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: VSM APR isr Q to endpoint: vsm_ptr->session[%d]->rx->cmdQ is NULL!!\n",(int) nSessionIndex);
            }
            break;
         }
      default:
         {
            if ( elite_apr_if_msg_type_is_cmd( pAprPacket) )
            {
               rc = elite_apr_if_end_cmd( me->apr_handle, pAprPacket, APR_EUNSUPPORTED );
            }
            else
            {
               rc = elite_apr_if_free( me->apr_handle, pAprPacket );
            }
            MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: unsupported msg/cmd to vpm, opcode(%#x)\n",(unsigned int)elite_apr_if_get_opcode( pAprPacket));
            break;
         }
   }
   return APR_EOK; // returning APR_EOK always since all commands/msgs freed in case of errors
}

// TODO: can most of this be moved to a utility function, shared by multiple services? Probably...
ADSPResult VoiceProcMgrCreate (uint32_t inputParam, void **handle)
{
   // static variable counter to make queue and thread name strings unique
   // limit to 16 bits so it will roll over and and only cost 4 characters in hexadecimal.
   // Queues in existence likely to have unique names, but not required...
   int32_t result;
   int32_t  rc;

   // allocate instance struct
   me = (VoiceProcMgr_t*) qurt_elite_memory_malloc( sizeof(VoiceProcMgr_t), QURT_ELITE_HEAP_DEFAULT);
   if (!me)
   {
       MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Failed to allocate memory for VPM structure");
       return ADSP_ENOMEMORY;
   }

   // zero out all the fields.
   memset(me, 0, sizeof(VoiceProcMgr_t));

   // Create the queues. Use non-blocking queues, since pselect is always used.
   // pselect blocks on any non-masked queue to receive, then can do non-blocking
   // checks.

   me->svc_handle.cmdQ = (qurt_elite_queue_t*) qurt_elite_memory_malloc(QURT_ELITE_QUEUE_GET_REQUIRED_BYTES (MAX_CMD_Q_ITEMS), QURT_ELITE_HEAP_DEFAULT );
   me->vpm_respq = (qurt_elite_queue_t*) qurt_elite_memory_malloc(QURT_ELITE_QUEUE_GET_REQUIRED_BYTES (MAX_CMD_Q_ITEMS), QURT_ELITE_HEAP_DEFAULT );
   me->vpm_aferespq = (qurt_elite_queue_t*) qurt_elite_memory_malloc(QURT_ELITE_QUEUE_GET_REQUIRED_BYTES (MAX_CMD_Q_ITEMS), QURT_ELITE_HEAP_DEFAULT );

   if ( ( NULL == me->svc_handle.cmdQ )|| ( NULL == me->vpm_respq ) || ( NULL == me->vpm_aferespq ) )
   {
       MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Failed to allocate memory for VPM cmdq or responseq or aferespq");
       return ADSP_ENOMEMORY;
   }


   if (ADSP_FAILED(result = qurt_elite_queue_init(cmdQName, MAX_CMD_Q_ITEMS, me->svc_handle.cmdQ))
         ||  ADSP_FAILED(result = qurt_elite_queue_init((char*)"VpmRespQ", MAX_CMD_Q_ITEMS, me->vpm_respq))
         ||  ADSP_FAILED(result = qurt_elite_queue_init((char*)"VpmAfeQ", MAX_CMD_Q_ITEMS, me->vpm_aferespq)))
   {
      MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: MQ creation failed");
      return result;
   }

   qurt_elite_channel_init(&me->channel);

   if (ADSP_FAILED(result = qurt_elite_channel_addq(&me->channel, (me->svc_handle.cmdQ), NULL))
         || ADSP_FAILED(result = qurt_elite_channel_addq(&me->channel, (me->vpm_respq), NULL))
         || ADSP_FAILED(result = qurt_elite_channel_addq(&me->channel, (me->vpm_aferespq), NULL)))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: MQ add to channel failed = %d", (int)result);
      return result;
   }

   // populate me
   me->svc_handle.unSvcId = ELITE_VOICE_DEVICE_SVCID;


   if( ADSP_FAILED( result = voice_mmpm_register( &me->mmpm,
               "VPM_ADSP",
               MMPM_CORE_ID_LPASS_ADSP)))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: MMPM_Register_Ext for VPM failed\n");
      return ADSP_EFAILED;
   }

   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: MMPM Registration done for VPM, received client_id(%lu)\n", me->mmpm.client_id);

   // Launch the service thread
   if (ADSP_FAILED(result = qurt_elite_thread_launch(&(me->svc_handle.threadId), VPM_THREAD_NAME, NULL,
               VPM_THREAD_STACK_SIZE, ELITETHREAD_STAT_VOICE_PROC_SVC_PRIO, voice_proc_mgr_thread, (void*)me,QURT_ELITE_HEAP_DEFAULT)))
   {
      MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: Thread creation failed");
      return result;
   }

   /* Register with APR service, using domain name of "qcom.adsp.vpm".  Registers our callback and
    * assigns the address for us
    */
   rc = elite_apr_if_register_by_name(
         &me->apr_handle, &me->apr_addr, (char*)"qcom.adsp.vpm", sizeof("qcom.adsp.vpm"), vpm_isr_recv_fn,NULL);
   if (ADSP_FAILED(rc))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: ERROR! Elite APR message handler has returned error %d, continuing...", (int)rc);
   }

   MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VPM APR register apr_handle(%lx) apr_addr(%x), pFn(%p)", (uint32_t)me->apr_handle, me->apr_addr, vpm_isr_recv_fn);

   // register with qurt_elite memory map.
   qurt_elite_memorymap_register(&vPM_memory_map_client);

   *handle = me;
   return ADSP_EOK;
}

/**
 * Main Thread for all commands
 */

// using int until qurt_elite clean up
static int voice_proc_mgr_thread(void* pInstance)
{
   // You tell it which MQ's to wait on, but you have to also pass in the 1 + the maximum
   // MQ descriptor of all the MQ's in the mask. That is why all this weirdness.
   int32_t rc;
   elite_msg_any_t msg;

   ADSPResult result = ADSP_EOK;                            // general result value
   VoiceProcMgr_t *me = (VoiceProcMgr_t*)pInstance;       // instance structure

   // get the bitfield for the cmdQ from channel;
   me->cmd_mask = qurt_elite_queue_get_channel_bit(me->svc_handle.cmdQ);
   me->resp_mask = qurt_elite_queue_get_channel_bit(me->vpm_respq);
   me->afe_resp_mask = qurt_elite_queue_get_channel_bit(me->vpm_aferespq);

   MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Entering VPM workloop...");

   me->wait_mask = me->cmd_mask|me->resp_mask|me->afe_resp_mask; //adding afe mask as well to ensure rogue signals don't go unhandled


   // Enter forever loop
   for(;;)
   {
      // make a copy of the mask, since it will be over-written by call to AudMqWaitOnMask.

      // block on any one or more of selected queues to get a msg
      me->output_mask = qurt_elite_channel_wait(&me->channel, me->wait_mask);

      // Check if commands came in
      if (me->output_mask & me->cmd_mask)
      {
         // call common utility to process command queue in standard way.
         const uint32_t customHandlerTableSize = sizeof(pHandlerCustom)/sizeof(pHandlerCustom[0]);
         //result = elite_svc_process_cmd_queue(pInstance, &(me->svc_handle), pHandlerCustom, customHandlerTableSize);
         result = vpm_process_cmd_queue(pInstance, &(me->svc_handle), pHandlerCustom, customHandlerTableSize);
         if(ADSP_FAILED(result))
         {
            MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: VPM error while processing command queue, %d!", (int)result);
         }

         // If service has been destroyed, exit.
         if (ADSP_ETERMINATED == result) return ADSP_EOK;
      }
      else if (me->output_mask & me->resp_mask)
      {
         if (ADSP_FAILED(result = qurt_elite_queue_pop_front(me->vpm_respq, (uint64_t*)&msg)))
         {
            MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: Failed to get response !!!");
         }
         else
         {
            switch (msg.unOpCode)
            {
               case ELITE_CMD_SET_PARAM :
                  {
                     vpm_process_set_param_msg(pInstance, &msg);
                     break;
                  }
               case ELITE_CMD_GET_PARAM :
                  {
                     uint32_t nFreeIndex;
                     uint32_t nSessionIndex;
                     uint32_t opcode;
                     async_struct_t *async_struct;
                     elite_msg_param_cal_t* pParamPayload;
                     elite_apr_packet_t* pAprPacket;
                     uint32_t nPayloadAddress = 0;

                     pParamPayload = (elite_msg_param_cal_t*)msg.pPayload;
                     nFreeIndex    = VOICE_ASYNC_EXTRACT_INDEX(pParamPayload->unClientToken);   // extracting Index from packed ClientToken
                     nSessionIndex   = VOICE_ASYNC_EXTRACT_SESSION(pParamPayload->unClientToken); // extracting SessionNum from packed ClientToken
                     async_struct   = &(me->session[nSessionIndex]->async_struct);
                     pAprPacket    = async_struct->async_status[nFreeIndex].apr_packet_ptr;
                     //need this to determine in band or out of band, since in inband case, packet is overwritten with ack packet
                     opcode = elite_apr_if_get_opcode(pAprPacket);

                     MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: vpm response(get param) sessionIndex(%lx) index(%ld)",nSessionIndex,nFreeIndex );
                     if (0 == --(async_struct->async_status[nFreeIndex].state))
                     {
                        voice_get_param_v2_t* pVoiceProcGetParams = (voice_get_param_v2_t*) elite_apr_if_get_payload_ptr(pAprPacket);

                        //dbg: MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Get Param FADD message cleared");

                        // if out of band flush the data before sending ack back
                        if(VOICE_CMD_GET_PARAM_V2 == opcode)
                        {
                           // Out of band data, so flush cache
                           if (NULL != vPM_memory_map_client )
                           {
                              elite_mem_shared_memory_map_t memmap = {0};
                              memmap.unMemMapClient = vPM_memory_map_client;
                              memmap.unMemMapHandle = pVoiceProcGetParams->mem_map_handle;
                              result = elite_mem_map_get_shm_attrib(pVoiceProcGetParams->payload_address_lsw,
                                    pVoiceProcGetParams->payload_address_msw,
                                    pVoiceProcGetParams->param_max_size,
                                    &memmap);
                              if( ADSP_SUCCEEDED(result))
                              {
                                 nPayloadAddress = memmap.unVirtAddr;
                                 MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VPM Memory Map Phy low(%lx) high(%lx) virtual address(%lx)",pVoiceProcGetParams->payload_address_lsw, pVoiceProcGetParams->payload_address_msw, nPayloadAddress);
                                 result = elite_mem_flush_cache_v2(&memmap);
                              }
                           }
                           else
                           {
                              result = ADSP_EFAILED;
                           }

                           {
                              voice_get_param_ack_t get_param_ack;
                              if ((ADSP_EOK == async_struct->async_status[nFreeIndex].result) || (ADSP_EOK == pParamPayload->unResponseResult))
                              {
                                 get_param_ack.status = APR_EOK;

#if defined(__qdsp6__) && !defined(SIM)
                                 int8_t *bufptr[4] = { (int8_t *) nPayloadAddress, NULL, NULL, NULL };
                                 voice_log_buffer( bufptr,
                                       VOICE_LOG_CHAN_VPM_GET_PARAM,
                                       nSessionIndex,
                                       qurt_elite_timer_get_time(),
                                       VOICE_LOG_DATA_FORMAT_PCM_MONO,
                                       1,   /* dummy value */
                                       pVoiceProcGetParams->param_max_size,
                                       NULL);
#endif

                              }
                              else if ((ADSP_EUNSUPPORTED == async_struct->async_status[nFreeIndex].result) && (ADSP_EUNSUPPORTED == pParamPayload->unResponseResult))
                              {
                                 get_param_ack.status = APR_EUNSUPPORTED;
                              }
                              // ADSP_EUNSUPPORTED is a default error code returned by a dynamic service
                              // in case there's no MID/PID match. If one of the services returns a non-
                              // EOK or non-EUNSUPPORTED error, then propagate that to the client.
                              else if (ADSP_EUNSUPPORTED == async_struct->async_status[nFreeIndex].result)
                              {
                                 get_param_ack.status = pParamPayload->unResponseResult;
                              }
                              else if (ADSP_EUNSUPPORTED == pParamPayload->unResponseResult)
                              {
                                 get_param_ack.status = async_struct->async_status[nFreeIndex].result;
                              }
                              // If unable to resolve the errors returned by the dynamic services, just
                              // propagate default failure.
                              else
                              {
                                 get_param_ack.status = APR_EFAILED;
                              }

                              // sending out of band result
                              elite_apr_if_alloc_send_ack(me->apr_handle,
                                    pAprPacket->dst_addr,
                                    pAprPacket->dst_port,
                                    pAprPacket->src_addr,
                                    pAprPacket->src_port,
                                    pAprPacket->token,
                                    VOICE_RSP_GET_PARAM_ACK,
                                    &get_param_ack,
                                    sizeof(voice_get_param_ack_t)
                                    );

                              elite_apr_if_free( me->apr_handle, pAprPacket);

                              if( ADSP_FAILED(result))
                              {
                                 MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: VPM Get Param get virtual addr or flush error");
                              }
                              MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Get Param ack pAprPacket(0x%p)",pAprPacket);
                           }
                        }
                        else
                        {  // In band case
                           voice_get_param_ack_t* pVoiceProcGetParams = (voice_get_param_ack_t*) (pParamPayload->pnParamData);

                           // Need to subtract size of voice_get_param_ack_t from above. That pointer points to start of voice_param_data_t
                           pVoiceProcGetParams = (voice_get_param_ack_t*)(((int8_t*)pVoiceProcGetParams) - sizeof(voice_get_param_ack_t));

                           // fill the response status
                           if ((ADSP_EOK == async_struct->async_status[nFreeIndex].result) || (ADSP_EOK == pParamPayload->unResponseResult))
                           {
                              pVoiceProcGetParams->status = APR_EOK;
                           }
                           else if ((ADSP_EUNSUPPORTED == async_struct->async_status[nFreeIndex].result) && (ADSP_EUNSUPPORTED == pParamPayload->unResponseResult))
                           {
                              pVoiceProcGetParams->status = APR_EUNSUPPORTED;
                           }
                           // ADSP_EUNSUPPORTED is a default error code returned by a dynamic service
                           // in case there's no MID/PID match. If one of the services returns a non-
                           // EOK or non-EUNSUPPORTED error, then propagate that to the client.
                           else if (ADSP_EUNSUPPORTED == async_struct->async_status[nFreeIndex].result)
                           {
                              pVoiceProcGetParams->status = pParamPayload->unResponseResult;
                           }
                           else if (ADSP_EUNSUPPORTED == pParamPayload->unResponseResult)
                           {
                              pVoiceProcGetParams->status = async_struct->async_status[nFreeIndex].result;
                           }
                           // If unable to resolve the errors returned by the dynamic services, just
                           // propagate default failure.
                           else
                           {
                              pVoiceProcGetParams->status = APR_EFAILED;
                           }
                           // send get param ack back
                           elite_apr_if_async_send( me->apr_handle, pAprPacket);

                           MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VPM Get Param In-Band end ack packet(0x%p), payload (0x%p)",pAprPacket, elite_apr_if_get_payload_ptr(pAprPacket));
                        }

                        // free the fadd message
                        elite_msg_return_payload_buffer( &msg );
                        // free async structure
                        voice_end_async_control( async_struct, nFreeIndex);
                        me->wait_mask |= me->cmd_mask;
                     }
                     else
                     {
                        MSG_3(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: vpm response(get param) sessionIndex(%lx) index(%ld) state(%ld)",nSessionIndex,nFreeIndex,async_struct->async_status[nFreeIndex].state );
                        async_struct->async_status[nFreeIndex].result = pParamPayload->unResponseResult; // update the result if needed
                        elite_msg_return_payload_buffer( &msg );
                     }
                     break;
                  }
               case ELITE_CUSTOM_MSG :
                  {
                     elite_msg_custom_header_t* pParamPayload;
                     pParamPayload = (elite_msg_custom_header_t*)msg.pPayload;

                     switch(pParamPayload->unSecOpCode)
                     {
                        case VOICEPROC_SET_TIMING_CMD:
                        case VOICEPROC_SET_TIMINGV2_CMD:
                        case VOICEPROC_SET_TIMINGV3_CMD:
                           {
                              uint32_t nFreeIndex;
                              uint32_t nSessionIndex;
                              async_struct_t *async_struct;

                              elite_apr_packet_t* pAprPacket;

                              nFreeIndex    = VOICE_ASYNC_EXTRACT_INDEX(pParamPayload->unClientToken);   // extracting Index from packed ClientToken
                              nSessionIndex   = VOICE_ASYNC_EXTRACT_SESSION(pParamPayload->unClientToken); // extracting SessionNum from packed ClientToken
                              async_struct   = &(me->session[nSessionIndex]->async_struct);
                              pAprPacket    = async_struct->async_status[nFreeIndex].apr_packet_ptr;

                              MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: vpm response(set timing) sessionIndex(%lx) index(%ld)",nSessionIndex,nFreeIndex );
                              if (0 == --(async_struct->async_status[nFreeIndex].state))
                              {
                                 elite_msg_return_payload_buffer( &msg );
                                 //dbg: MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Set timing FADD message cleared");
                                 (void) elite_apr_if_end_cmd( me->apr_handle, pAprPacket, APR_EOK );// TODO: send proper error code
                                 MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Set timing ack pAprPacket(0x%p)",pAprPacket);

                                 voice_end_async_control( async_struct, nFreeIndex);
                                 me->wait_mask |= me->cmd_mask;
                              }
                              else
                              {
                                 MSG_3(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: vpm response(set timing) sessionIndex(%lx) index(%ld) state(%ld)",nSessionIndex,nFreeIndex,async_struct->async_status[nFreeIndex].state );
                                 async_struct->async_status[nFreeIndex].result = pParamPayload->unResponseResult; // update the result if needed
                                 elite_msg_return_payload_buffer( &msg );
                              }
                              break;
                           }
                        case VOICEPROC_GET_DELAY_CMD:
                           {
                              //most of the handling for this command is same as kpps,
                              //only difference is you have to account for afe group delay
                              uint32_t nFreeIndex;
                              uint32_t nSessionIndex;
                              async_struct_t *async_struct;
                              nFreeIndex    = VOICE_ASYNC_EXTRACT_INDEX(pParamPayload->unClientToken);   // extracting Index from packed ClientToken
                              nSessionIndex   = VOICE_ASYNC_EXTRACT_SESSION(pParamPayload->unClientToken); // extracting SessionNum from packed ClientToken
                              async_struct   = &(me->session[nSessionIndex]->async_struct);
                              if (1 == (async_struct->async_status[nFreeIndex].state))
                              {
                                 elite_apr_packet_t* pAprPacket;
                                 vpm_get_delay_ack_t* delay_ptr;
                                 uint32_t tx_afe_delay, rx_afe_delay;
                                 tx_afe_delay = me->session[nSessionIndex]->tx_afe_delay + me->session[nSessionIndex]->tx_afe_hw_delay;
                                 rx_afe_delay = me->session[nSessionIndex]->rx_afe_delay + me->session[nSessionIndex]->rx_afe_hw_delay;
                                 pAprPacket    = async_struct->async_status[nFreeIndex].apr_packet_ptr;
                                 delay_ptr = (vpm_get_delay_ack_t*)elite_apr_if_get_payload_ptr(pAprPacket);
                                 MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VPM delay tx %lu, afe_tx %lu, session(%ld)", delay_ptr->vptx_delay, tx_afe_delay, nSessionIndex);
                                 MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VPM delay rx %lu, afe_rx %lu, session(%ld)", delay_ptr->vprx_delay, rx_afe_delay, nSessionIndex);
                                 delay_ptr->vptx_delay += tx_afe_delay;
                                 delay_ptr->vprx_delay += rx_afe_delay;
                              }
                              //fall through. the lack of break here is deliberate
                           }
                        case VOICEPROC_GET_KPPS_CMD:
                           {
                              uint32_t nFreeIndex;
                              uint32_t nSessionIndex;
                              async_struct_t *async_struct;
                              elite_apr_packet_t* pAprPacket;

                              nFreeIndex    = VOICE_ASYNC_EXTRACT_INDEX(pParamPayload->unClientToken);   // extracting Index from packed ClientToken
                              nSessionIndex   = VOICE_ASYNC_EXTRACT_SESSION(pParamPayload->unClientToken); // extracting SessionNum from packed ClientToken
                              async_struct   = &(me->session[nSessionIndex]->async_struct);
                              pAprPacket    = async_struct->async_status[nFreeIndex].apr_packet_ptr;

                              MSG_4(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: vpm response secopcode(%lx) sessionIndex(%lx) index(%ld) state(%ld)",
                                    pParamPayload->unSecOpCode, nSessionIndex, nFreeIndex, async_struct->async_status[nFreeIndex].state);
                              if (0 == --(async_struct->async_status[nFreeIndex].state))
                              {
                                 // send get kpps/delay ack back
                                 elite_apr_if_async_send( me->apr_handle, pAprPacket);

                                 MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VPM ack packet(0x%p), payload (0x%p)",pAprPacket, elite_apr_if_get_payload_ptr(pAprPacket));

                                 // free the fadd message
                                 elite_msg_return_payload_buffer( &msg );
                                 // free async structure
                                 voice_end_async_control( async_struct, nFreeIndex);
                                 me->wait_mask |= me->cmd_mask;
                              }
                              else
                              {
                                 async_struct->async_status[nFreeIndex].result = pParamPayload->unResponseResult; // update the result if needed
                                 elite_msg_return_payload_buffer( &msg );
                              }
                              break;
                           }
                        case VOICEPROC_SET_PARAM_V3_CMD:
                           {
                              vpm_process_set_param_msg(pInstance, &msg);
                              break;
                           }
                        default:
                           {
                              MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Error unknown secondary opcode (%lx)!!",pParamPayload->unSecOpCode);
                              // free up message
                              elite_msg_return_payload_buffer( &msg );
                           }
                     }
                     break;
                  }
               default:
                  {
                     MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Error unknown opcode (%lx)!!",msg.unOpCode);
                     break;
                  }
            } // end of switch (msg.unOpCode)
         }
      } //else if (output_mask & resp_mask)
      else if (me->output_mask & me->afe_resp_mask)
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Error! Response from afeq in workloop, result %d!!", result);
      }
      else // there was error reading queue.
      {
         MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: Error %d reading cmd queue!!", result);
      }

   }

   // close mq
   qurt_elite_queue_deinit(me->svc_handle.cmdQ);
   qurt_elite_memory_free(me->svc_handle.cmdQ);
   qurt_elite_queue_deinit(me->vpm_respq);
   qurt_elite_memory_free(me->vpm_respq);
   qurt_elite_queue_deinit(me->vpm_aferespq);
   qurt_elite_memory_free(me->vpm_aferespq);

   /* Release the APR resource...doesn't occur since we don't destroy the static service */
   rc = elite_apr_if_deregister( me->apr_handle );
   if (ADSP_FAILED(rc))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: ERROR! Elite APR message handler has returned error %d, continuing...", (int)rc);
   }

   // free alloc
   qurt_elite_memory_free(me);

   return 0;
}

static ADSPResult vpm_update_additional_create_params(void *pMsg,vptx_create_params_t *create_param_tx, uint8_t tx_rx_flag)
{

   vpm_create_session_v3_t* createCommand = (vpm_create_session_v3_t*)pMsg;
   uint32_t size = 0;
   uint32_t total_param_size = createCommand->param_payload_size;
   uint32 payload_address = createCommand->param_payload_virt_addr;

   if(VPRX == tx_rx_flag)
   {
      //no support added yet
      return ADSP_EOK;
   }

   /*
    * Check if the topology is known, if yes, then set the number of channels
    * else read the number of channels from virtual address
    */
   for (uint32_t i=0,j=0; i<sizeof(topo_vptx); i+=sizeof(topo_vptx[0]),j++)
   {
      if (create_param_tx->topology_id == topo_vptx[j].topology_id)
      {
         create_param_tx->num_mics = topo_vptx[j].num_in_channels;
         MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Num channels for Tx topology(%lx) = (%ld)",create_param_tx->topology_id,create_param_tx->num_mics);
         return ADSP_EOK;
      }
   }

   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VPTx topology(%lx) is a custom topo. Reading number of channels from virtual memory",create_param_tx->topology_id);

   if(NULL == payload_address)
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Error: Virtual address is null for custom topology(0%lx)",create_param_tx->topology_id);
      return ADSP_EBADPARAM;
   }

   create_param_tx->num_mics = 0;
   do
   {
      vpm_create_param_t* additional_create_params = (vpm_create_param_t*)payload_address;
      switch (additional_create_params->param_id)
      {
         case VPM_PARAM_NUM_DEV_CHANNELS:
         {
            vpm_ivocproc_cmd_topology_set_dev_channels_t *channel_info_ptr = (vpm_ivocproc_cmd_topology_set_dev_channels_t *)additional_create_params->param_virt_addr;

            if( (NULL == channel_info_ptr) || (additional_create_params->param_size != sizeof (vpm_ivocproc_cmd_topology_set_dev_channels_t)) )
            {
               MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Error!! virtual address is null or incorrect size. Required size (%ld) Received size (%ld)",(sizeof (vpm_ivocproc_cmd_topology_set_dev_channels_t)),additional_create_params->param_size);
               return ADSP_EBADPARAM;
            }
            create_param_tx->num_mics = channel_info_ptr->tx_num_channels;
            MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Num channels for Custom Tx topology(%lx) = (%ld)",create_param_tx->topology_id,create_param_tx->num_mics);
            break;
         }
         default:
         {
            MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Error!! Unrecognized param id (0x%lx) during vocproc creation/reconfig ",additional_create_params->param_id);
            break;
         }
      }
      size += sizeof(vpm_create_param_t);
      payload_address += sizeof(vpm_create_param_t);


   } while (size<total_param_size);

   if(0 == create_param_tx->num_mics)
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Error!! num_channel is not received for custom topo(0x%lx)",create_param_tx->topology_id);
      return ADSP_EFAILED;
   }
   return ADSP_EOK;
}
/* =======================================================================
 **                          Message handler functions
 ** ======================================================================= */
static int32_t voice_proc_create(void* pInstance, void* pMsg, uint16_t* handle, uint32_t opcode)
{
   MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: voice_proc_create Begin");
   VoiceProcMgr_t* me = (VoiceProcMgr_t*)pInstance;
   ADSPResult result = APR_EOK;
   vpm_create_session_v2_t* createCommand = (vpm_create_session_v2_t*)pMsg;
   uint16_t rx_port_for_vptx;
   int32_t nSessionIndex = 0;
   MSG_6(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: VPM tx sample rate %lu, rx sample rate %lu, tx port %lx, rx port %lx, tx topo %lx, rx topo %lx",
         createCommand->tx_sampling_rate, createCommand->rx_sampling_rate, createCommand->tx_port, createCommand->rx_port, 
         createCommand->tx_topology_id, createCommand->rx_topology_id);


   // Create a new session
   // Check for first available free session
   for (nSessionIndex = 0; nSessionIndex < VOICE_MAX_VP_SESSIONS; nSessionIndex++)
   {
      if(me->session[nSessionIndex] == NULL)
      {
         break;
      }
   }
   if(nSessionIndex == VOICE_MAX_VP_SESSIONS)
   {
      MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: Reached Max Active VP sessions! Unable to create!");
      return APR_ENORESOURCE;
   }


   me->session[nSessionIndex] = (voice_proc_session_t*) qurt_elite_memory_malloc( sizeof(voice_proc_session_t), QURT_ELITE_HEAP_DEFAULT);
   if (!me->session[nSessionIndex])
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: VoiceProc session struct could not be allocated memory, sessionIndex(%lx)\n",nSessionIndex);
      return APR_ENORESOURCE;
   }
   memset(me->session[nSessionIndex], 0, sizeof(voice_proc_session_t));
   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VoiceProc session creation Success, sessionIndex(%lx)\n",nSessionIndex);

   // Store a pointer to me in the session (for backtracking)
   me->session[nSessionIndex]->vpm_ptr = me;
   rx_port_for_vptx = vpm_get_ec_ref_port(createCommand);
   //Check which sessions to create: rx, tx or rx+tx
   if(createCommand->tx_port != NO_INTERFACE)
   {
      vptx_create_params_t create_param;
      vptx_appi_fptr_t appi_fptr;
      create_param.num_mics = 0;
      create_param.near_port_id = createCommand->tx_port;
      create_param.far_port_id = rx_port_for_vptx;
      create_param.topology_id = createCommand->tx_topology_id;
      create_param.sampling_rate = createCommand->tx_sampling_rate;
      create_param.session_num = (uint32_t) voice_map_session_index_to_handle(nSessionIndex);
      create_param.shared_mem_client = vPM_memory_map_client;

      //call this function to get the number of channels for V2 command as well. All the static topologies
      //will be provided with prop num of channels
      if (ADSP_FAILED (result = vpm_update_additional_create_params(pMsg,&create_param, VPTX)))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: VoiceProc Tx Creation V3 Failed, topo:0x%lx", createCommand->tx_topology_id);
         return APR_EFAILED;
      }

      appi_fptr.so_getsize_fptr = me->appi_tx.getsize_fptr;
      appi_fptr.so_init_fptr = me->appi_tx.init_fptr;

      if(ADSP_FAILED (result = vptx_create((void**)&(me->session[nSessionIndex]->tx),
                  (void**)&(me->session[nSessionIndex]->tx_far),
                  &create_param,
                  &appi_fptr
                  )))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: VoiceProc Tx Creation Failed, topo:0x%lx", createCommand->tx_topology_id);
         return APR_EFAILED;
      }
      else
      {
         MSG_4(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VpTx Creation success Handle(%p), SO done flag: (%d), SO Handles: (%p), (%p)",
               me->session[nSessionIndex]->tx,
               me->so_done,
               me->appi_tx.getsize_fptr,
               me->appi_tx.init_fptr);
         //update number of channels
         me->session[nSessionIndex]->tx_num_channels = create_param.num_mics;
      }
   }

   if(createCommand->rx_port != NO_INTERFACE)
   {
      vprx_create_params_t create_param;
      create_param.afe_port_id   = createCommand->rx_port;
      create_param.sampling_rate = createCommand->rx_sampling_rate;
      create_param.topology_id   = createCommand->rx_topology_id;
      create_param.session_num   =  (uint32_t)voice_map_session_index_to_handle(nSessionIndex);
      create_param.shared_mem_client = vPM_memory_map_client;
      create_param.num_channels  = 1;

      //SA: TODO: Need to verify num of channels for RX? CVD might be doing it any way
      vprx_appi_fptr_t appi_param = { me->appi_rx.getsize_fptr, me->appi_rx.init_fptr};
      if(ADSP_FAILED (result = vprx_create((void**)&(me->session[nSessionIndex]->rx),
                  &create_param, &appi_param)))
      {
         MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: VoiceProc Rx Creation Failed");
         return APR_EFAILED;
      }
      else
      {
         MSG_4(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VpRx Creation success Handle(%p), SO done flag: (%d), SO Handles: (%p), (%p)",
               me->session[nSessionIndex]->rx,
               me->so_done,
               me->appi_rx.getsize_fptr,
               me->appi_rx.init_fptr);
         me->session[nSessionIndex]->rx_num_channels = 1;
      }
   }

   /* send rx peer to Tx */
   if((createCommand->tx_port != NO_INTERFACE) && (createCommand->rx_port != NO_INTERFACE))
   {
      if (ADSP_FAILED(result = voice_proc_connect_peer(VOICEPROC_CONNECT_RX_PEER, me->session[nSessionIndex]->tx->cmdQ, me->vpm_respq, me->session[nSessionIndex]->rx)))
      {
         MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: VoiceProc Tx/Rx connection Failed");
      }
      else
      {
         MSG(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VoiceProc bind Rx/Tx success");
      }
   }

   // Connect To AFE
   if (qurt_elite_globalstate.pAfeStatSvcCmdQ == NULL)
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Failed to get Afe cmdQ session");
      return ADSP_EUNSUPPORTED;
   }
   me->afe_serv_cmdq = qurt_elite_globalstate.pAfeStatSvcCmdQ;

   //Get Matrix ports


   // create Rx port
   if( createCommand->rx_port != NO_INTERFACE)
   {
      result = create_rx_mixer_output( nSessionIndex, createCommand->rx_sampling_rate);
      if(ADSP_FAILED(result))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: Unable to connect to rx matrix, error = %d!!\n", result);
         return result;
      }
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VPM Connected to rx matrix! PortID = %ld!!\n", me->session[nSessionIndex]->mixer_out_port->outport_id);
   }

   // create Tx port and attach Vptx to matrix input
   if( createCommand->tx_port != NO_INTERFACE)
   {
      uint32_t priority;
      if(!is_pseudo_port(createCommand->tx_port))
      {
         priority = VMX_INPUT_HIGH_PRIO;
      }
      else
      {
         priority = VMX_INPUT_LOW_PRIO;
      }
      MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VPM setting session %ld AFE port %#x i/p port prio %ld", nSessionIndex, createCommand->tx_port, priority);
      result = create_tx_mixer_input( nSessionIndex, priority );
      if(ADSP_FAILED(result))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: Unable to connect to tx matrix, error = %d!!\n", result);
         return result;
      }
      //dbg: MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VPM acquired tx matrix input! PortID = %d!!\n", me->session[nSessionIndex]->mixer_in_port->inport_id);

      {
         //Connect VPTx to acquired input port
         elite_msg_any_t txConnectMsg;
         elite_msg_cmd_connect_t txConnectParams;
         //local variable, thus must wait for connection to finish.
         //else must malloc it here and free it at the endpoint. which is the preferred approach?
         txConnectParams.pSvcHandle = &me->session[nSessionIndex]->mixer_in_port->port_handle;
         //MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Tx connecting to: %#x",txConnectParams.pSvcHandle);
         txConnectParams.pResponseQ = me->vpm_respq;
         txConnectMsg.unOpCode = ELITE_CMD_CONNECT;
         txConnectMsg.pPayload = (void*)&txConnectParams;
         result = qurt_elite_queue_push_back(me->session[nSessionIndex]->tx->cmdQ,(uint64_t*)&txConnectMsg);
         if (ADSP_FAILED(result))
         {
            MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: tx session connect to mixer cmd push back error result(%#x)!!\n", result);
            return result;
         }
         elite_svc_wait_for_ack(&txConnectMsg);
      }

      //dbg: MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Connected to tx matrix! PortID = %d!!\n", me->session[nSessionIndex]->mixer_in_port->inport_id);
   }

   // AFE related values cached since connect moved to Run state
   me->session[nSessionIndex]->session_config.tx_port          = createCommand->tx_port;
   me->session[nSessionIndex]->session_config.tx_topology_id   = createCommand->tx_topology_id;
   me->session[nSessionIndex]->session_config.tx_sampling_rate = createCommand->tx_sampling_rate;
   me->session[nSessionIndex]->session_config.rx_port          = createCommand->rx_port;
   me->session[nSessionIndex]->session_config.rx_topology_id   = createCommand->rx_topology_id;
   me->session[nSessionIndex]->session_config.rx_sampling_rate = createCommand->rx_sampling_rate;
   me->session[nSessionIndex]->session_config.ec_mode          = createCommand->ec_mode;
   me->session[nSessionIndex]->session_config.far_port         = rx_port_for_vptx;

   *handle = nSessionIndex;
   MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: voice_proc_create End");
   return result;
}

static int32_t voice_proc_destroy(void* pInstance, uint16_t nSessionIndex)
{
   VoiceProcMgr_t* me = (VoiceProcMgr_t*)pInstance;
   ADSPResult result = APR_EOK;

   /* The naming of this function is now a little inconsistent for Tx.  Tx has 2 step
    * process...first is to send ELITEMSG_CUSTOM_AFECLIENTDISABLE, which is done in
    * disconnect_afe_tx function.  After the VoiceProcTx is destroyed,
    * we can later finish second step of Disconnect, since we know the buffers are returend to
    * AFE Tx. In Rx, we can directly send ELITEMSG_CUSTOM_AFEDISCONNECT_REQ
    */
   elite_svc_handle_t *tx, *rx;
   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: voice_proc_destroy Begin, session : %d", nSessionIndex);

   tx = me->session[nSessionIndex]->tx;
   rx = me->session[nSessionIndex]->rx;

   if(tx)
   {
      // sending cmd message to Tx for disconnecting with RX
      // no harm in sending the following message even if rx is NULL
      if(rx)
      {
         if (ADSP_FAILED(result = voice_proc_connect_peer(VOICEPROC_DISCONNECT_RX_PEER, me->session[nSessionIndex]->tx->cmdQ, me->vpm_respq, me->session[nSessionIndex]->rx)))
         {
            MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: VoiceProc Tx/Rx connection Failed");
         }
      }
      // Cache the thread id as context will be lost once destroy message is sent
      qurt_elite_thread_t txThread = tx->threadId;
      (void)voice_custom_msg_send(me->vpm_respq, tx->cmdQ,
            VOICEPROC_DESTROY_YOURSELF_CMD, NULL, FALSE);
      // Wait for the threads to finish
      qurt_elite_thread_join(txThread, &result); //TODO: Determine whether to wait here or to wait below after sending the rx message as well.
   }
   if(rx)
   {

      // Cache the thread id as context will be lost once destroy message is sent
      qurt_elite_thread_t rxThread = rx->threadId;
      (void)voice_custom_msg_send(me->vpm_respq, rx->cmdQ,
            VOICEPROC_DESTROY_YOURSELF_CMD, NULL, FALSE);
      // Wait for the threads to finish
      qurt_elite_thread_join(rxThread, &result);
   }

   // now that Tx data Q is destroyed, send this indication to AFE Tx
   me->session[nSessionIndex]->tx = NULL;
   me->session[nSessionIndex]->rx = NULL;
   qurt_elite_memory_free(me->session[nSessionIndex]);
   me->session[nSessionIndex] = NULL;

   // Sending a dummy message, back to response Q.
   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: voice_proc_destroy End, session : %d", nSessionIndex);
   return result;
}

static int32_t voice_proc_reinit(void* pInstance, void * pMsg, uint16_t nSessionIndex, uint32 opcode)
{

   VoiceProcMgr_t* me = (VoiceProcMgr_t*)pInstance;
   ADSPResult result = APR_EOK;
   vpm_create_session_v2_t  *reinitCommand = (vpm_create_session_v2_t *)pMsg ;

   /* rx */

   if( me->session[nSessionIndex]->rx )
   {
      /* for rx we are currently ignoring topology */

      uint8_t fRxSampleRateChange = (reinitCommand->rx_sampling_rate != me->session[nSessionIndex]->session_config.rx_sampling_rate);
      uint8_t fRxPortChange       = (reinitCommand->rx_port          != me->session[nSessionIndex]->session_config.rx_port);
      uint8_t fRxTopologyChange   = (reinitCommand->rx_topology_id   != me->session[nSessionIndex]->session_config.rx_topology_id);

      /* Rx exists, check if need to destroy */
      if( reinitCommand->rx_port == NO_INTERFACE)
      {
         MSG(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VPM Reinit: destroying Rx");

         /* send port close command to mixer output port */
         close_rx_mixer_output( nSessionIndex);

         if( me->session[nSessionIndex]->tx )
         {
            if (ADSP_FAILED(result = voice_proc_connect_peer(VOICEPROC_DISCONNECT_RX_PEER,
                        me->session[nSessionIndex]->tx->cmdQ,
                        me->vpm_respq, me->session[nSessionIndex]->rx)))
            {
               MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: VoiceProc Tx/Rx disconnection Failed");
            }
         }

         /* destroy Vprx */
         // Cache the thread id as context will be lost once destroy message is sent
         qurt_elite_thread_t rxThread = me->session[nSessionIndex]->rx->threadId;
         (void)voice_custom_msg_send(me->vpm_respq, me->session[nSessionIndex]->rx->cmdQ,
               VOICEPROC_DESTROY_YOURSELF_CMD, NULL, FALSE);
         // Wait for the threads to finish
         qurt_elite_thread_join(rxThread, &result);

         me->session[nSessionIndex]->rx = NULL;
      }
      /* check if we need to reconfigure the existing sampling rate and/or port */
      else
      {
         /* Send reinit/reconfig to vprx
          * now send the reconfig command.  We assume that Vprx will always return any input data buffers back to mixer upon receiving
          * the reconfig command.  Also, we assume Vprx will attempt to destroy bufQ.  Because of this, we need to force AFE Rx to return
          * its data buffers to Vprx bufQ. Since we currently blindly send Reinit, command we must currently blindly send AFE Rx disconnect/reconnect

          * In addition the
          * reconfig will setup for new sampling rate and/or topology (port not considered in Vprx now).
          */

         /* Normally we could do AFE Rx disconnect/reconnect only when SR or Port changes.  But since it involves buffers
          * that AFE might be holding on to, we need to do disconnect/reconnect any time we are sending Vprx reconfig (since
          * it expects buffers returned.  If logic changes for when to send Vprx reconfig, we can change Disconnect/Reconnect AFE
          * logic in same way
          */
         /* it's possible to slightly optimize this if only sampling rate changes...can just use media type message
          * instead of 2 step disconnect/reconnect
          */

         if( !fRxPortChange && !fRxSampleRateChange && !fRxTopologyChange)
         {
            MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP:  Reinit to same rx parameters, topo(%lx) port(%x) sampRate(%d)\n",reinitCommand->rx_topology_id, reinitCommand->rx_port, reinitCommand->rx_sampling_rate);
         }

         /* disconnect AFE Rx, which will return any held buffers to Vprx */
         MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VPM Reinit: reconnecting AFE port(%x),sampRate(%d)",
               reinitCommand->rx_port,reinitCommand->rx_sampling_rate);
         /* reconfig Vprx */
         elite_msg_any_t  reinitMsg;
         elite_msg_voice_re_config_type *pPayload;

         uint32_t nSize = sizeof(elite_msg_voice_re_config_type);
         result = elite_msg_create_msg( &reinitMsg,
               &nSize,
               ELITE_CUSTOM_MSG,
               me->vpm_respq,
               NULL,
               ADSP_EOK);
         if( ADSP_FAILED( result))
         {
            MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg for rx session to afe disconnect!!\n");
            return result;
         }
         pPayload = (elite_msg_voice_re_config_type *) reinitMsg.pPayload;
         pPayload->topology_id   = reinitCommand->rx_topology_id;     // this field is ignored
         pPayload->sampling_rate = reinitCommand->rx_sampling_rate;
         pPayload->port_id_rx    = reinitCommand->rx_port;            //tbd should port even be in this message?  not related to Vprx
         pPayload->sec_opcode    = VOICEPROC_RECONFIG_CMD;
         pPayload->num_channels  = 1;

         if (ADSP_FAILED(result =qurt_elite_queue_push_back(me->session[nSessionIndex]->rx->cmdQ,(uint64_t *)&reinitMsg )))
         {
            MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Error trying to reconfigure Vprx result(%d)\n", result);

            elite_msg_return_payload_buffer( &reinitMsg);
            return(result);
         }

         (void)elite_svc_wait_for_ack(&reinitMsg);
         result = pPayload->unResponseResult;
         if (ADSP_FAILED(result))
         {
            MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VPM reinit: Vprx reconfigure wait for ack result(%d),sessionIndex(%x) \n",
                  result,nSessionIndex);

            elite_msg_return_payload_buffer( &reinitMsg);
            return( result);
         }
         else
         {
            MSG(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VPM reinit: Vprx reconfigure success");
            me->session[nSessionIndex]->rx_num_channels = pPayload->num_channels;
         }
         elite_msg_return_payload_buffer( &reinitMsg);

         if( fRxSampleRateChange)
         {
            /* reconfigure matrix ouptut */
            /* Media type message should be sent from matrix to Vprx for sampling rate dependent Vprx */
            MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VPM Reinit: reconfig rx matrix sampRate(%d)",reinitCommand->rx_sampling_rate);
            reconfigure_rx_mixer_output( nSessionIndex, reinitCommand->rx_sampling_rate);
         }
      } // else part of if( reinitCommand->rx_port == NO_INTERFACE)
   }
   else
   {
      /* rx doesn't exist, check if need to create */
      if( reinitCommand->rx_port != NO_INTERFACE)
      {
         vprx_create_params_t create_param;
         create_param.afe_port_id   = reinitCommand->rx_port;
         create_param.sampling_rate = reinitCommand->rx_sampling_rate;
         create_param.topology_id   = reinitCommand->rx_topology_id;
         create_param.session_num   =  (uint32_t)voice_map_session_index_to_handle(nSessionIndex);
         create_param.shared_mem_client = vPM_memory_map_client;
         create_param.num_channels = 1;

         vprx_appi_fptr_t appi_param = { me->appi_rx.getsize_fptr, me->appi_rx.init_fptr};
         /* create and connect Vprx */
         if(ADSP_FAILED (result = vprx_create((void**)&(me->session[nSessionIndex]->rx),
                     &create_param, &appi_param)))
         {
            MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: VoiceProc Rx Creation Failed");
            return APR_EFAILED;
         }
         else
         {
            MSG_4(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VpRx reinit success topo (%lx), SO done flag: (%d), SO Handles: (%p), (%p)",
                  reinitCommand->rx_topology_id,
                  me->so_done,
                  me->appi_rx.getsize_fptr,
                  me->appi_rx.init_fptr);
            me->session[nSessionIndex]->rx_num_channels = create_param.num_channels;
         }

         /* create output port for rx matrix */
         /* Media type message should be sent from matrix to Vprx for sampling rate dependent Vprx */

         result = create_rx_mixer_output( nSessionIndex, reinitCommand->rx_sampling_rate);
         if(ADSP_FAILED(result))
         {
            MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: Unable to connect to rx matrix, result(%d)\n", result);
            return result;
         }
         MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VPM Connected to rx matrix! outPortId(%ld)!!\n", me->session[nSessionIndex]->mixer_out_port->outport_id);
      }
   }

   uint16_t rx_port_for_vptx = vpm_get_ec_ref_port(reinitCommand);

   /* Tx */
   if( me->session[nSessionIndex]->tx )
   {

      uint8_t fTxSampleRateChange = (reinitCommand->tx_sampling_rate != me->session[nSessionIndex]->session_config.tx_sampling_rate);
      uint8_t fTxPortChange       = (reinitCommand->tx_port          != me->session[nSessionIndex]->session_config.tx_port);
      uint8_t fTxTopologyChange   = (reinitCommand->tx_topology_id   != me->session[nSessionIndex]->session_config.tx_topology_id);
      uint8_t fTxPortPrioChange   = 0;

      /* tx exists, check if need to destroy or reconfigure */
      if( reinitCommand->tx_port == NO_INTERFACE)
      {

         MSG(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VPM Reinit: destroying Tx");

         /* send port close command to Tx mixer input */

         close_tx_mixer_input( nSessionIndex);

         // Cache the thread id as context will be lost once destroy message is sent
         qurt_elite_thread_t txThread = me->session[nSessionIndex]->tx->threadId;
         (void)voice_custom_msg_send(me->vpm_respq, me->session[nSessionIndex]->tx->cmdQ, VOICEPROC_DESTROY_YOURSELF_CMD, NULL, FALSE);
         // Wait for the threads to finish
         qurt_elite_thread_join(txThread, &result); //TODO: Determine whether to wait here or to wait below after sending the rx message as well.

         me->session[nSessionIndex]->tx = NULL;
      }
      else
      {

         if( fTxSampleRateChange || fTxPortChange || fTxTopologyChange)
         {

            MSG(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VPM reinit: reconfigure AFE Tx");
            //Check if going from real to pseudoport or vice versa. This will necessitate mixer input port priority change
            if(fTxPortChange)
            {
               if((is_pseudo_port( me->session[nSessionIndex]->session_config.tx_port) && !is_pseudo_port(reinitCommand->tx_port)) ||
                     (!is_pseudo_port( me->session[nSessionIndex]->session_config.tx_port) && is_pseudo_port(reinitCommand->tx_port)) )
               {
                  MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VPM changing input port prio");
                  fTxPortPrioChange = 1;
               }
            }


            /* now send the reconfig command.  We assume that Vptx will always return any near/far data buffers back to AFE upon receiving
             * the reconfig command.  Then AFE can properly free those malloc'd output buffers, preventing any memleaks.  The subsequent
             * AFE connect will malloc new output buffers to feed Vptx data Q.  The Vptx Q itself doesn't need to be destroyed.

             * In addition the
             * reconfig will setup for new sampling rate and/or topology (port not considered in Vptx now).
             */

            /*
             * Perform Reconfig (before reconnect) here so config params are updated...when AFE connect sends updated MediaType, then Vptx can properly
             * validate the media type.
             */
            elite_msg_any_t  reinitMsg;
            elite_msg_voice_re_config_type *pPayload;

            uint32_t nSize = sizeof(elite_msg_voice_re_config_type);
            result = elite_msg_create_msg( &reinitMsg,
                  &nSize,
                  ELITE_CUSTOM_MSG,
                  me->vpm_respq,
                  NULL,
                  ADSP_EOK);
            if( ADSP_FAILED( result))
            {
               MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg for tx session to afe disconnect!!\n");
               return result;
            }
            pPayload = (elite_msg_voice_re_config_type *) reinitMsg.pPayload;
            pPayload->topology_id   = reinitCommand->tx_topology_id;
            pPayload->sampling_rate = reinitCommand->tx_sampling_rate;
            pPayload->port_id_tx       = reinitCommand->tx_port;            //tbd should port even be in this message?  not related to Vptx
            pPayload->sec_opcode   = VOICEPROC_RECONFIG_CMD;
            pPayload->port_id_rx       = rx_port_for_vptx;
            pPayload->num_channels = 1;      //set default

//          if (VPM_CMD_REINIT_SESSION_V3== opcode)
            {
               vptx_create_params_t create_param;
               create_param.topology_id = reinitCommand->tx_topology_id;
               if (ADSP_FAILED (result = vpm_update_additional_create_params(pMsg,&create_param,VPTX)))
               {
                  MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: VoiceProc Tx Creation/reconfig V3 Failed, topo:%lx", reinitCommand->tx_topology_id);
                  // return buffer
                  elite_msg_return_payload_buffer(&reinitMsg);
                  return APR_EFAILED;
               }
               pPayload->num_channels = create_param.num_mics;
            }

            if (ADSP_FAILED(result =qurt_elite_queue_push_back(me->session[nSessionIndex]->tx->cmdQ,(uint64_t *)&reinitMsg )))
            {
               MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Error trying to reconfigure Vptx result(%d)\n", result);

               elite_msg_return_payload_buffer( &reinitMsg);
               return(result);
            }

            (void) elite_svc_wait_for_ack(&reinitMsg);
            result = pPayload->unResponseResult;
            if (ADSP_FAILED(result))
            {
               MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VPM reinit: Vptx reconfigure wait for ack result(%d),sessionIndex(%x) \n",
                     result,nSessionIndex);

               elite_msg_return_payload_buffer( &reinitMsg);
               return( result);
            }
            else
            {
               MSG(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VPM reinit: Vptx reconfigure success");
               me->session[nSessionIndex]->tx_num_channels = pPayload->num_channels;
            }
            elite_msg_return_payload_buffer( &reinitMsg);

            /* Now we can reconnect to AFE tx.  This will allocate new AFE Tx output buffers based on new config */
            MSG(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VPM reinit: reconfigure AFE Tx");
            //dbg: MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VPM reinit: end reconfigure AFE Tx");

            if(fTxPortPrioChange)
            {
               uint32_t priority;
               if(!is_pseudo_port(reinitCommand->tx_port))
               {
                  priority = VMX_INPUT_HIGH_PRIO;
               }
               else
               {
                  priority = VMX_INPUT_LOW_PRIO;
               }
               if(ADSP_FAILED(result = reconfigure_tx_mixer_input(nSessionIndex, priority)))
               {
                  MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Error reconfiguring tx input! Result %d",result);
               }
            }
         }

         /* It's assumed that Vptx reconfig will NOT do anything to reset the downstream peer handle (which is Tx Matrix)
          * matrix sampling rate will be updated when moving to run state.  Vptx service will propogate MT message when back in run state,
          * so there is no need to do a port reconfigure for Tx matrix*/
      }
   }
   else
   {
      /* tx doesn't exist, check if need to create */
      if( reinitCommand->tx_port != NO_INTERFACE)
      {
         /* create Vptx */
         vptx_create_params_t create_param;
         vptx_appi_fptr_t appi_fptr;

         create_param.near_port_id = reinitCommand->tx_port;
         create_param.far_port_id = rx_port_for_vptx;
         create_param.topology_id = reinitCommand->tx_topology_id;
         create_param.sampling_rate = reinitCommand->tx_sampling_rate;
         create_param.session_num = (uint32_t) voice_map_session_index_to_handle(nSessionIndex);
         create_param.shared_mem_client = vPM_memory_map_client;
         create_param.num_mics = 0;

//         if (VPM_CMD_REINIT_SESSION_V3== opcode)
         {
            if (ADSP_FAILED (result = vpm_update_additional_create_params(pMsg,&create_param,VPTX)))
            {
               MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: VoiceProc Tx Creation/reconfig V3 Failed, topo:%ld", reinitCommand->tx_topology_id);
               return APR_EFAILED;
            }

         }


         appi_fptr.so_getsize_fptr = me->appi_tx.getsize_fptr;
         appi_fptr.so_init_fptr = me->appi_tx.init_fptr;
         if(ADSP_FAILED (result = vptx_create((void**)&(me->session[nSessionIndex]->tx),
                     (void**)&(me->session[nSessionIndex]->tx_far),
                     &create_param,
                     &appi_fptr
                     )))
         {
            MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: VPM reinit: VoiceProc Tx Creation Failed, topo(%lx)", reinitCommand->tx_topology_id);
            return APR_EFAILED;
         }
         else
         {
            MSG_4(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VpTx reinit success topo (%lx), SO done flag: (%d), SO Handles: (%p), (%p)",
                  reinitCommand->tx_topology_id,
                  me->so_done,
                  me->appi_tx.getsize_fptr,
                  me->appi_tx.init_fptr);
            me->session[nSessionIndex]->tx_num_channels = create_param.num_mics;
         }

         /* create/connect to Matrix Tx input */
         //TODO: Switch from real to pseudo/vice versa possible? Priority cannot be changed once port created..
         uint32_t priority;
         if(!is_pseudo_port(reinitCommand->tx_port))
         {
            priority = VMX_INPUT_HIGH_PRIO;
         }
         else
         {
            priority = VMX_INPUT_LOW_PRIO;
         }
         result = create_tx_mixer_input( nSessionIndex , priority);
         if(ADSP_FAILED(result))
         {
            MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: Unable to connect to tx matrix, result(%d)\n", result);
            return result;
         }
         MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VPM acquired tx matrix input! inPortId(%ld)\n", me->session[nSessionIndex]->mixer_in_port->inport_id);

         {
            //Connect VPTx to acquired input port
            elite_msg_any_t txConnectMsg;
            elite_msg_cmd_connect_t txConnectParams;
            //local variable, thus must wait for connection to finish.
            //else must malloc it here and free it at the endpoint. which is the preferred approach?
            txConnectParams.pSvcHandle = &me->session[nSessionIndex]->mixer_in_port->port_handle;
            //MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Tx connecting to: %#x",txConnectParams.pSvcHandle);
            txConnectParams.pResponseQ = me->vpm_respq;
            txConnectMsg.unOpCode = ELITE_CMD_CONNECT;
            txConnectMsg.pPayload = (void*)&txConnectParams;
            result = qurt_elite_queue_push_back(me->session[nSessionIndex]->tx->cmdQ,(uint64_t*)&txConnectMsg);
            if (ADSP_FAILED(result))
            {
               MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: tx session connect cmd push back error result(%#x)!!\n", result);
               return result;
            }
            elite_svc_wait_for_ack(&txConnectMsg);
         }

         //dbg: MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Connected to tx matrix! inPortId(%d)!!\n", me->session[nSessionIndex]->mixer_in_port->inport_id);
      }
   }

   /* if tx connect peer with updated rx module (rx can be NULL, tx should handle this)*/
   if((me->session[nSessionIndex]->tx) && ( me->session[nSessionIndex]->rx))
   {
      if (ADSP_FAILED(result = voice_proc_connect_peer(VOICEPROC_CONNECT_RX_PEER, me->session[nSessionIndex]->tx->cmdQ, me->vpm_respq, me->session[nSessionIndex]->rx)))
      {
         MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: VPM reinit: VoiceProc Tx/Rx connection Failed");
      }
      else
      {
         MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VPM reinit: connected Tx to Rx peer pHandle(%p)", me->session[nSessionIndex]->rx);
      }
   }

   {
      /* fill in the details of new session_config -- values also cached since AFE connect is moved to RUN command handling */
      me->session[nSessionIndex]->session_config.tx_port          = reinitCommand->tx_port;
      me->session[nSessionIndex]->session_config.tx_topology_id   = reinitCommand->tx_topology_id;
      me->session[nSessionIndex]->session_config.tx_sampling_rate = reinitCommand->tx_sampling_rate;
      me->session[nSessionIndex]->session_config.rx_port          = reinitCommand->rx_port;
      me->session[nSessionIndex]->session_config.rx_topology_id   = reinitCommand->rx_topology_id;
      me->session[nSessionIndex]->session_config.rx_sampling_rate = reinitCommand->rx_sampling_rate;
      me->session[nSessionIndex]->session_config.ec_mode          = reinitCommand->ec_mode;
      me->session[nSessionIndex]->session_config.far_port         = rx_port_for_vptx;
   }
   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VPM reinit: done.  sessionIndex(%x)",nSessionIndex);

   return result;

}

static ADSPResult send_afe_tx_dataq_destroyed_msg( void* pInstance, uint16_t nSessionIndex, uint16_t txPort, uint16_t rxPort /* FARSRC */)
{
   VoiceProcMgr_t* me = (VoiceProcMgr_t*)pInstance;

   ADSPResult result = ADSP_EOK;

   if(nSessionIndex >= VOICE_MAX_VP_SESSIONS)
   {
      MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: Exceeded max number of sessions !!\n");
      return ADSP_EBADPARAM;
   }
   voice_proc_session_t *currSession = me->session[nSessionIndex];

   MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Send DataQ destroyed msg to AFE Tx port sessionIndex(%x),pHandle(%p)",
         nSessionIndex,currSession->tx_afe_handle);

   elite_msg_any_t sMsg;

   // Common Variables
   elite_msg_custom_afe_connect_t   *pPayload;
   uint32_t nSize;

   nSize = sizeof(elite_msg_custom_afe_connect_t);
   result = elite_msg_create_msg( &sMsg,
         &nSize,
         ELITE_CUSTOM_MSG,
         me->vpm_aferespq,
         NULL,
         ADSP_EOK);
   if( ADSP_FAILED( result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg!!\n");
      return(result);
   }
   pPayload = (elite_msg_custom_afe_connect_t *) sMsg.pPayload;
   pPayload->afe_id = txPort;
   pPayload->sec_op_code = ELITEMSG_CUSTOM_AFEDISCONNECT_REQ;   // second step is send DISCONNECT now that data buffers returned to AFE Tx
   pPayload->svc_handle_ptr  = currSession->tx_afe_handle;

   // Send the msg to device static service cmdQ.  Can instead be send directly to port handle (me->tx_afe_handle Q) either way ok
   if (ADSP_FAILED(result =qurt_elite_queue_push_back(me->afe_serv_cmdq,(uint64_t *)&sMsg )))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: ChannelSelf thread encountered qurt_elite_queue_push_back error = %d!!\n", result);
      elite_msg_return_payload_buffer(&sMsg);
      return(result);
   }

   if (ADSP_FAILED(result = elite_svc_wait_for_ack(&sMsg)))
   {
      MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: AFE tx dis-connected client bufQ destroy failure %d, sessionIndex(%x) \n",
            result,nSessionIndex);
      elite_msg_return_payload_buffer(&sMsg);
      return( result);
   }
   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: AFE tx dis-connected client bufQ destroy success, sessionIndex(%x). \n",nSessionIndex);

#if FARSRC
   if (NO_INTERFACE != rxPort)
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Send far afe bufQ destroy command\n");
      pPayload = (elite_msg_custom_afe_connect_t *) sMsg.pPayload;
      pPayload->afe_id = rxPort;
      pPayload->sec_op_code = ELITEMSG_CUSTOM_AFEDISCONNECT_REQ;   // second step is send DISCONNECT now that data buffers returned to AFE Tx
      pPayload->svc_handle_ptr  = currSession->far_afe_handle;

      // Send the msg to device static service cmdQ.  Can instead be send directly to port handle (me->tx_afe_handle Q) either way ok
      if (ADSP_FAILED(result =qurt_elite_queue_push_back(me->afe_serv_cmdq,(uint64_t *)&sMsg )))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: ChannelSelf thread encountered qurt_elite_queue_push_back error = %d!!\n", result);
      }
      else
      {
         if (ADSP_FAILED(result = elite_svc_wait_for_ack(&sMsg)))
         {
            MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: AFE far dis-connected client ack receive error %d. \n", result);
         }
         else
         {
            result = pPayload->response_result;

            if (ADSP_EOK != result)
            {
               MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: AFE far dis-connected client bufQ destroy failure %d. \n", result);
            }

            MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: AFE far dis-connected client bufQ destroy Done. \n");
         }
      }
   }
#endif

   elite_msg_return_payload_buffer( &sMsg);
   return result;
}

static ADSPResult connect_afe_tx(void* pInstance, uint16_t nSessionIndex, uint16_t samplingRate, uint16_t txPort, uint16_t rxPort /* FARSRC */, uint32_t nTopologyId)
{
   VoiceProcMgr_t* me = (VoiceProcMgr_t*)pInstance;
   ADSPResult result = ADSP_EOK;
   afe_client_t txAfeClient;      // tx afe client params
   afe_client_t farAfeClient;      // far afe client params

   if(nSessionIndex >= VOICE_MAX_VP_SESSIONS)
   {
      MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: Exceeded max number of sessions !!\n");
      return ADSP_EBADPARAM;
   }
   voice_proc_session_t *currSession = me->session[nSessionIndex];

   {
      elite_msg_any_t sMsg;

      // Common Variables
      elite_msg_custom_afe_connect_t   *pPayload;
      uint32_t nSize;

      MSG_4(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Connecting to AFE Tx, SR(%d),txPort(%x), num_channel(%d), sessionIndex(%x)",samplingRate,txPort,currSession->tx_num_channels, nSessionIndex);

      //***************************************************************************
      // Connecting to afe tx port
      //***************************************************************************
      // Build the tx client parameters
      // Initialize AFE client to all zeros (default values)
      memset(&txAfeClient, 0, sizeof(txAfeClient));
      txAfeClient.sample_rate      = samplingRate;
      txAfeClient.data_path        = AFE_PP_OUT;

      // Get the number of input channels for corresponding Tx topology.
      txAfeClient.channel = currSession->tx_num_channels;

      txAfeClient.bytes_per_channel = 2;
      txAfeClient.bit_width = 16;
      txAfeClient.afe_dp_delay_ptr = &currSession->tx_afe_delay;
      txAfeClient.afe_dev_hw_delay_ptr = &currSession->tx_afe_hw_delay;
      txAfeClient.buf_size         = VPTX_DMA_BLOCK(samplingRate); // 1ms
      txAfeClient.num_buffers      = 21; // 21 buffers for 20ms frame and 1ms to handler jitter, this number should be greater that the maximum processing time of vptx
      txAfeClient.interleaved      = FALSE;
      txAfeClient.cust_proc.resampler_type = IIR_BASED_SRC;
      txAfeClient.afe_client_handle        = currSession->tx; // EliteSvcHandle instead of VptxHandle
      txAfeClient.cust_proc.subscribe_to_avt_drift = FALSE; // dont subscribe to AV Timer
      txAfeClient.cust_proc.is_pop_suppressor_required = FALSE; // dont subscribe to pop suppressor

      // TODO: Voice Timer will take care of this

      nSize = sizeof(elite_msg_custom_afe_connect_t);
      result = elite_msg_create_msg( &sMsg,
            &nSize,
            ELITE_CUSTOM_MSG,
            me->vpm_aferespq,
            NULL,
            ADSP_EOK);
      if( ADSP_FAILED( result))
      {
         MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg for tx afe connect !!\n");
         return result;
      }
      pPayload = (elite_msg_custom_afe_connect_t *) sMsg.pPayload;
      pPayload->sec_op_code = ELITEMSG_CUSTOM_AFECONNECT_REQ;
      pPayload->afe_id     = txPort;
      pPayload->afe_client   = txAfeClient;

      // Send the msg to device static service cmdQ
      if (ADSP_FAILED(result =qurt_elite_queue_push_back(me->afe_serv_cmdq,(uint64_t *)&sMsg )))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: ChannelSelf thread encountered qurt_elite_queue_push_back error = %d!!\n", result);
         elite_msg_return_payload_buffer( &sMsg);
         return(result);
      }
      // TODO: Is it required to specify txAfeClient payload here?
      //***************************************************************************

      // Now wait for confirm message from AFE */
      (void) qurt_elite_channel_wait(qurt_elite_queue_get_channel(me->vpm_aferespq), qurt_elite_queue_get_channel_bit(me->vpm_aferespq));

      // Read the msg and get the handle of the AFE
      result = qurt_elite_queue_pop_front (me->vpm_aferespq,(uint64_t*)&sMsg);
      if (result != ADSP_EOK)
      {
         MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Failure on mq %p qurt_elite_queue_pop_front: %d\n", me->vpm_aferespq, result);
         elite_msg_return_payload_buffer( &sMsg);
         return result;
      }
      pPayload = ( elite_msg_custom_afe_connect_t *) sMsg.pPayload;

      if( ADSP_EOK == pPayload->response_result)
      {
         currSession->tx_afe_handle = (elite_svc_handle_t *) pPayload->svc_handle_ptr; // not needed on the tx side??
         currSession->tx_afe_drift_ptr = (void *) pPayload->afe_drift_ptr; // save ptr to drift struct for tx port
         MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: AFE Near Tx Connected, sessionIndex(%x),pHandle(%p),pDrift(%p)",nSessionIndex,currSession->tx_afe_handle,currSession->tx_afe_drift_ptr);
      }
      else
      {
         currSession->tx_afe_handle = NULL;
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: TxAfe Near Connect Ack error = %ld!!\n", pPayload->response_result);
      }

#if FARSRC
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Connecting to AFE far, sessionIndex(%x)",nSessionIndex);
      {
         if ((NO_INTERFACE != rxPort) && (VPM_TX_NONE != nTopologyId))
         {
            MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Connecting to AFE FAR, SR(%d),farPort(%x),sessionIndex(%x)",samplingRate,rxPort,nSessionIndex);

            //***************************************************************************
            // Connecting to afe far tx port
            //***************************************************************************
            // Build the far tx client parameters
            // Initialize AFE client to all zeros (default values)
            memset(&farAfeClient, 0, sizeof(farAfeClient));
            farAfeClient.sample_rate       = samplingRate;

            farAfeClient.data_path         = AFE_PP_OUT;
            farAfeClient.channel           = 1;  // mono
            farAfeClient.bytes_per_channel = 2;
            farAfeClient.bit_width = 16;
            farAfeClient.buf_size          = VPTX_DMA_BLOCK(samplingRate); // 1ms
            farAfeClient.num_buffers       = 21; // 21 buffers for 20ms frame and 1ms to handler jitter,this number should be greater that the maximum processing time of vptx
            farAfeClient.interleaved       = FALSE;

            farAfeClient.afe_client_handle        = currSession->tx_far; // EliteSvcHandle instead of VpfarHandle
            farAfeClient.cust_proc.resampler_type = IIR_BASED_SRC;
            farAfeClient.cust_proc.subscribe_to_avt_drift = FALSE; // dont subscribe to AV Timer
            farAfeClient.cust_proc.is_pop_suppressor_required = FALSE; // dont subscribe to pop suppressor
            // TODO: Voice Timer will take care of this

            pPayload = (elite_msg_custom_afe_connect_t *) sMsg.pPayload;
            pPayload->sec_op_code = ELITEMSG_CUSTOM_AFECONNECT_REQ;
            pPayload->afe_id     = rxPort;
            pPayload->afe_client   = farAfeClient;

            // Send the msg to device static service cmdQ
            if (ADSP_FAILED(result =qurt_elite_queue_push_back(me->afe_serv_cmdq,(uint64_t *)&sMsg )))
            {
               MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: ChannelSelf thread encountered qurt_elite_queue_push_back error = %d!!\n", result);
               elite_msg_return_payload_buffer( &sMsg);
               return(result);
            }
            // TODO: Is it required to specify farAfeClient payload here?
            //***************************************************************************

            // Now wait for confirm message from AFE */
            (void) qurt_elite_channel_wait(qurt_elite_queue_get_channel(me->vpm_aferespq), qurt_elite_queue_get_channel_bit(me->vpm_aferespq));

            // Read the msg and get the handle of the AFE
            result = qurt_elite_queue_pop_front (me->vpm_aferespq,(uint64_t*)&sMsg);
            if (result != ADSP_EOK)
            {
               MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Failure on mq %p qurt_elite_queue_pop_front: %d\n", me->vpm_aferespq, result);
               elite_msg_return_payload_buffer( &sMsg);
               return result;
            }
            pPayload = ( elite_msg_custom_afe_connect_t *) sMsg.pPayload;

            if( ADSP_EOK == pPayload->response_result)
            {
               currSession->far_afe_handle = (elite_svc_handle_t *) pPayload->svc_handle_ptr; // not needed on the far side??
               currSession->far_src_drift_ptr = (void *) pPayload->afe_drift_ptr; // save ptr to drift struct for tx port
               MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: AFE Far Tx Connected, sessionIndex(%x),Handle(%p)",nSessionIndex,currSession->far_afe_handle);
            }
            else
            {
               currSession->far_afe_handle = NULL;
               currSession->far_src_drift_ptr = NULL;
               MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: TxAfe Connect Ack error = %ld!!\n", pPayload->response_result);
            }
         }
         else
         {
            MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: no rx port hence Far not connected. sessionIndex(%x)!!\n", nSessionIndex);
         }
      }
#endif

      elite_msg_return_payload_buffer( &sMsg);

      //***************************************************************************
      // End of connecting tx port
      //***************************************************************************

      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Done connecting AFE Tx and Far, sessionIndex(%x)",nSessionIndex);
   }

   return result;
}

static ADSPResult disconnect_afe_tx(void* pInstance, uint16_t nSessionIndex, uint16_t txPort, uint16_t rxPort /* FARSRC */)
{
   VoiceProcMgr_t* me = (VoiceProcMgr_t*)pInstance;
   ADSPResult result = ADSP_EOK;

   if(nSessionIndex >= VOICE_MAX_VP_SESSIONS)
   {
      MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: Exceeded max number of sessions !!\n");
      return ADSP_EBADPARAM;
   }
   voice_proc_session_t *currSession = me->session[nSessionIndex];

   MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: DisConnecting to AFE Tx, txPort(%x),sessionIndex(%x)",txPort,nSessionIndex);
   {
      elite_msg_any_t sMsg;

      // Common Variables
      elite_msg_custom_afe_connect_t   *pPayload;
      uint32_t nSize;

      //***************************************************************************
      // DisConnecting to afe tx port
      //***************************************************************************
      // Build the tx client parameters (all this required?)

      nSize = sizeof(elite_msg_custom_afe_connect_t);
      result = elite_msg_create_msg( &sMsg,
            &nSize,
            ELITE_CUSTOM_MSG,
            me->vpm_aferespq,
            NULL,
            ADSP_EOK);
      if( ADSP_FAILED( result))
      {
         MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg!!\n");
         return result;
      }
      pPayload = (elite_msg_custom_afe_connect_t *) sMsg.pPayload;
      pPayload->sec_op_code = ELITEMSG_CUSTOM_AFECLIENTDISABLE;    // inconsistent naming, but proper sequence.  DISABLE is first step
      pPayload->afe_id     = txPort;
      pPayload->svc_handle_ptr  = currSession->tx_afe_handle;

      // Send the msg to device static service cmdQ. Can instead be send directly to port handle (me->tx_afe_handle Q) either way ok
      if (ADSP_FAILED(result =qurt_elite_queue_push_back(me->afe_serv_cmdq,(uint64_t *)&sMsg )))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: ChannelSelf thread encountered qurt_elite_queue_push_back error = %d!!\n", result);
         elite_msg_return_payload_buffer( &sMsg);
         return(result);
      }
      // TODO: Is it required to specify txAfeClient payload here?
      //***************************************************************************

      // Now wait for confirm message from AFE */
      (void) qurt_elite_channel_wait(qurt_elite_queue_get_channel(me->vpm_aferespq), qurt_elite_queue_get_channel_bit(me->vpm_aferespq));

      // Read the msg and get the handle of the AFE
      result = qurt_elite_queue_pop_front (me->vpm_aferespq,(uint64_t*)&sMsg);
      if (result != ADSP_EOK)
      {
         MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Failure on mq %p qurt_elite_queue_pop_front: %d\n", me->vpm_aferespq, result);
         elite_msg_return_payload_buffer( &sMsg);
         return result;
      }

      pPayload = ( elite_msg_custom_afe_connect_t *) sMsg.pPayload;
      if( ADSP_EOK != pPayload->response_result)
      {
         MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: TxAfe Client Disable error = %ld!!, sessionIndex(%x)\n",
               pPayload->response_result,
               nSessionIndex);
      }
      else
      {
         MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: AFE Tx Client Disable success,to  sessionIndex(%x)", nSessionIndex);
      }

#if FARSRC
      {
         if ((NO_INTERFACE != rxPort) && (VPM_TX_NONE != me->session[nSessionIndex]->session_config.tx_topology_id))
         {

            MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: DisConnecting to AFE Far/rx, rxPort(%x),sessionIndex(%x)",rxPort,nSessionIndex);
            //***************************************************************************
            // DisConnecting to afe far port
            //***************************************************************************
            // Build the far client parameters (all this required?)

            pPayload = (elite_msg_custom_afe_connect_t *) sMsg.pPayload;
            pPayload->sec_op_code      = ELITEMSG_CUSTOM_AFECLIENTDISABLE;;    // inconsistent naming, but proper sequence.  DISABLE is first step
            pPayload->afe_id           = rxPort;
            pPayload->svc_handle_ptr   = currSession->far_afe_handle;

            // Send the msg to device static service cmdQ. Can instead be send directly to port handle (me->far_afe_handle Q) either way ok
            if (ADSP_FAILED(result =qurt_elite_queue_push_back(me->afe_serv_cmdq,(uint64_t *)&sMsg )))
            {
               MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: ChannelSelf thread encountered qurt_elite_queue_push_back error = %d!!\n", result);
               elite_msg_return_payload_buffer( &sMsg);
               return(result);
            }
            // farAfeClient payload is not needed
            //***************************************************************************

            // Now wait for confirm message from AFE */
            (void) qurt_elite_channel_wait(qurt_elite_queue_get_channel(me->vpm_aferespq), qurt_elite_queue_get_channel_bit(me->vpm_aferespq));

            // Read the msg and get the handle of the AFE
            result = qurt_elite_queue_pop_front (me->vpm_aferespq,(uint64_t*)&sMsg);
            if (result != ADSP_EOK)
            {
               MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Failure on mq %p qurt_elite_queue_pop_front: %d\n", me->vpm_aferespq, result);
               elite_msg_return_payload_buffer( &sMsg);
               return result;
            }

            pPayload = ( elite_msg_custom_afe_connect_t *) sMsg.pPayload;
            if( ADSP_EOK != pPayload->response_result)
            {
               MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: TxAfe Client Disable error = %ld!!, sessionIndex(%x)\n",
                     pPayload->response_result,
                     nSessionIndex);
            }
            else
            {
               MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: AFE Tx Client Disable success,to  sessionIndex(%x)", nSessionIndex);
            }
         }
         else
         {
            MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: no rx port hence Far not connected to disconnect. sessionIndex(%x)!!\n", nSessionIndex);
         }
      }
#endif

      elite_msg_return_payload_buffer( &sMsg);

      // Reinitialize delay to zero
      me->session[nSessionIndex]->tx_afe_delay = 0;
      me->session[nSessionIndex]->tx_afe_hw_delay = 0;

      //***************************************************************************
      // End of connecting tx port
      //***************************************************************************

      MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Done DisConnecting AFE Tx and AFE Far, sessionIndex(%x)",nSessionIndex);
   }
   return result;
}
//TODO: Should this method be moved entirely to voice proc session?
static ADSPResult connect_afe_rx(void* pInstance, uint16_t nSessionIndex, uint16_t samplingRate, uint16_t rxPort)
{
   VoiceProcMgr_t* me = (VoiceProcMgr_t*)pInstance;
   afe_client_t rxAfeClient;      // rx afe client params
   ADSPResult result = ADSP_EOK;

   elite_msg_any_t sMsg;

   if(nSessionIndex >= VOICE_MAX_VP_SESSIONS)
   {
      MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: Exceeded max number of sessions !!\n");
      return ADSP_EBADPARAM;
   }
   voice_proc_session_t* currSession = me->session[nSessionIndex];

   // Common Variables
   elite_msg_custom_afe_connect_t   *pPayload;
   uint32_t nSize;

   MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Connecting to AFE Rx, sampRate(%d),rxPort(%x),sessionIndex(%x)",samplingRate,rxPort,nSessionIndex);

   // Build the rx client parameters
   // Initialize AFE client to all zeros (default values)
   memset(&rxAfeClient, 0, sizeof(rxAfeClient));
   rxAfeClient.sample_rate      = samplingRate;
   rxAfeClient.data_path        = AFE_RX_VOC_MIXER_IN;
   rxAfeClient.cust_proc.resampler_type               = IIR_BASED_SRC;
   rxAfeClient.cust_proc.subscribe_to_avt_drift       = FALSE; // dont subscribe to AV Timer, Voice Timer will take care of this
   rxAfeClient.cust_proc.is_pop_suppressor_required   =TRUE;
   rxAfeClient.channel         = 1;  // mono
   rxAfeClient.bytes_per_channel = 2;  //TBD?
   rxAfeClient.bit_width = 16;
   rxAfeClient.buf_size         =  200 * (samplingRate/VOICE_NB_SAMPLING_RATE);            //(samplingRate / 1000)*(20 + RX_PRE_BUF); // 21 ms
   rxAfeClient.num_buffers      = 5;
   rxAfeClient.interleaved      = FALSE;

   //   rxAfeClient.afe_client_handle        = me->session[0]->Audrx[0]; // not used by AFE, so leaving it un-initialized

   rxAfeClient.afe_dp_delay_ptr = &currSession->rx_afe_delay;
   rxAfeClient.afe_dev_hw_delay_ptr = &currSession->rx_afe_hw_delay;



   //***************************************************************************
   // --------------------------------------------------
   // Connect to AFE RX Port
   // --------------------------------------------------
   nSize = sizeof(elite_msg_custom_afe_connect_t);
   result = elite_msg_create_msg( &sMsg,
         &nSize,
         ELITE_CUSTOM_MSG,
         me->vpm_aferespq,
         NULL,
         ADSP_EOK);
   if( ADSP_FAILED( result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg!!\n");
      return result;
   }
   pPayload = (elite_msg_custom_afe_connect_t *) sMsg.pPayload;
   pPayload->sec_op_code   = ELITEMSG_CUSTOM_AFECONNECT_REQ;
   pPayload->afe_id        = rxPort;
   pPayload->afe_client    = rxAfeClient;

   // Send the msg to device static service cmdQ
   if (ADSP_FAILED(result =qurt_elite_queue_push_back(me->afe_serv_cmdq,(uint64_t *)&sMsg)))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: ChannelSelf thread encountered qurt_elite_queue_push_back error = %d!!\n", result);
      elite_msg_return_payload_buffer( &sMsg);
      return(result);
   }
   // TODO: Is it required to specify rxAfeClient payload here?
   //***************************************************************************

   // Now wait for confirm message from AFE */
   (void) qurt_elite_channel_wait(qurt_elite_queue_get_channel(me->vpm_aferespq), qurt_elite_queue_get_channel_bit(me->vpm_aferespq));


   // Read the msg and get the handle of the AFE
   result = qurt_elite_queue_pop_front (me->vpm_aferespq,(uint64_t*)&sMsg);
   if (result != ADSP_EOK)
   {
      MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Failure on mq %p qurt_elite_queue_pop_front: %d\n", me->vpm_aferespq, result);
      elite_msg_return_payload_buffer( &sMsg);
      return result;
   }

   pPayload = ( elite_msg_custom_afe_connect_t *) sMsg.pPayload;

   if( ADSP_EOK == pPayload->response_result)
   {
      currSession->rx_afe_handle = (elite_svc_handle_t *) pPayload->svc_handle_ptr; // Read rx_afe_handle for the dataQ handle
      currSession->rx_afe_drift_ptr = (void *) pPayload->afe_drift_ptr; // save ptr to drift struct for rx port

      // provide the afe handle to VoiceprocRx
      // TODO: This needs restructuring
      result = elite_msg_return_payload_buffer( &sMsg);

      elite_msg_custom_voc_svc_connect_type *pPayloadConnectCmd = NULL;

      nSize = sizeof(elite_msg_custom_voc_svc_connect_type);

      result = elite_msg_create_msg( &sMsg,
            &nSize,
            ELITE_CUSTOM_MSG,
            me->vpm_aferespq,   /* use same respQ */
            NULL,
            ADSP_EOK);
      if( ADSP_FAILED( result))
      {
         MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg!!\n");
         return result;
      }
      pPayloadConnectCmd              = (elite_msg_custom_voc_svc_connect_type *) sMsg.pPayload;
      pPayloadConnectCmd->sec_opcode = VOICEPROC_CONNECT_DWN_STREAM_CMD;
      pPayloadConnectCmd->svc_handle_ptr  = currSession->rx_afe_handle;

      result = qurt_elite_queue_push_back(me->session[nSessionIndex]->rx->cmdQ, (uint64_t*)&sMsg );
      if (ADSP_FAILED(result))
      {
         MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED message \n");
         elite_msg_return_payload_buffer( &sMsg);
         return result;
      }
      result = elite_svc_wait_for_ack(&sMsg);
      result = elite_msg_return_payload_buffer( &sMsg);

      assert(ADSP_SUCCEEDED(result));
      MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: AFE Rx Connected, sessionIndex(%x),pHandle(%p),pDrift(%p)",nSessionIndex,
            currSession->rx_afe_handle,currSession->rx_afe_drift_ptr);

   }
   else
   {
      result = elite_msg_return_payload_buffer( &sMsg);

      MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: RxAfe Connect Ack result(%ld), sessionIndex(%x)!!\n",
            pPayload->response_result, nSessionIndex);
   }

   return result;
}

static ADSPResult send_disconnect_marker_to_afe(void* pInstance, uint16_t nSessionIndex,uint16_t rxPort)
{

   VoiceProcMgr_t* me = (VoiceProcMgr_t*)pInstance;
   ADSPResult result = ADSP_EOK;

   elite_msg_any_t sMsg;

   if(nSessionIndex >= VOICE_MAX_VP_SESSIONS)
   {
      MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: Exceeded max number of sessions !!\n");
      return ADSP_EBADPARAM;
   }
   voice_proc_session_t *currSession = me->session[nSessionIndex];

   elite_msg_custom_afeclient_disconnect_marker_t   *pPayload;
   uint32_t nSize;

   MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: sending disconnect marker to AFE Rx rxPort(%x),sessionIndex(%x)",rxPort,nSessionIndex);

   nSize = sizeof(elite_msg_custom_afeclient_disconnect_marker_t);
   result = elite_msg_create_msg( &sMsg,
         &nSize,
         ELITE_CUSTOM_MSG,
         me->vpm_aferespq,
         0,
         ADSP_EOK);
   if( ADSP_FAILED( result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg!!\n");
      return result;
   }
   pPayload = (elite_msg_custom_afeclient_disconnect_marker_t *) sMsg.pPayload;
   pPayload->sec_op_code      = ELITEMSG_CUSTOM_AFECLIENT_DISCONNECT_MARKER;
   pPayload->afe_id           = rxPort;

   pPayload->svc_handle_ptr   = currSession->rx_afe_handle;

   // Send the msg to device static service cmdQ
   if (ADSP_FAILED(result =qurt_elite_queue_push_back(me->afe_serv_cmdq,(uint64_t *)&sMsg )))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: ChannelSelf thread encountered qurt_elite_queue_push_back error = %d!!\n", result);
      elite_msg_return_payload_buffer( &sMsg);
      return(result);
   }


   // Now wait for confirm message from AFE */
   (void) qurt_elite_channel_wait(qurt_elite_queue_get_channel(me->vpm_aferespq), qurt_elite_queue_get_channel_bit(me->vpm_aferespq));


   // Read the msg and get the handle of the AFE
   result = qurt_elite_queue_pop_front (me->vpm_aferespq,(uint64_t*)&sMsg);
   if (result != ADSP_EOK)
   {
      MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Failure on mq %p qurt_elite_queue_pop_front: %d\n", me->vpm_aferespq, result);
      elite_msg_return_payload_buffer( &sMsg);
      return result;
   }

   pPayload = ( elite_msg_custom_afeclient_disconnect_marker_t *) sMsg.pPayload;
   if( ADSP_EOK == pPayload->response_result)
   {
      result = elite_msg_return_payload_buffer( &sMsg);
   }
   else
   {

      result = elite_msg_return_payload_buffer( &sMsg);

      MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: RxAfe Disconnect Ack error = %ld, sessionIndex(%x)!!\n",
            pPayload->response_result, nSessionIndex);
   }

   return result;
}

static ADSPResult disconnect_afe_rx(void* pInstance, uint16_t nSessionIndex, uint16_t rxPort)
{
   VoiceProcMgr_t* me = (VoiceProcMgr_t*)pInstance;
   ADSPResult result = ADSP_EOK;

   elite_msg_any_t sMsg;

   if(nSessionIndex >= VOICE_MAX_VP_SESSIONS)
   {
      MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: Exceeded max number of sessions !!\n");
      return ADSP_EBADPARAM;
   }
   voice_proc_session_t *currSession = me->session[nSessionIndex];

   elite_msg_custom_afe_connect_t   *pPayload;
   uint32_t nSize;

   MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: DisConnecting to AFE Rx rxPort(%x),sessionIndex(%x)",rxPort,nSessionIndex);

   //***************************************************************************
   // --------------------------------------------------
   // DisConnect to AFE RX Port
   // --------------------------------------------------
   nSize = sizeof(elite_msg_custom_afe_connect_t);
   result = elite_msg_create_msg( &sMsg,
         &nSize,
         ELITE_CUSTOM_MSG,
         me->vpm_aferespq,
         NULL,
         ADSP_EOK);
   if( ADSP_FAILED( result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg!!\n");
      return result;
   }
   pPayload = (elite_msg_custom_afe_connect_t *) sMsg.pPayload;
   pPayload->sec_op_code      = ELITEMSG_CUSTOM_AFEDISCONNECT_REQ;
   pPayload->afe_id           = rxPort;
   pPayload->svc_handle_ptr   = currSession->rx_afe_handle;

   // Send the msg to device static service cmdQ
   if (ADSP_FAILED(result =qurt_elite_queue_push_back(me->afe_serv_cmdq,(uint64_t *)&sMsg )))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: ChannelSelf thread encountered qurt_elite_queue_push_back error = %d!!\n", result);
      elite_msg_return_payload_buffer( &sMsg);
      return(result);
   }
   // TODO: Is it required to specify rxAfeClient payload here?
   //***************************************************************************

   // Now wait for confirm message from AFE */
   (void) qurt_elite_channel_wait(qurt_elite_queue_get_channel(me->vpm_aferespq), qurt_elite_queue_get_channel_bit(me->vpm_aferespq));


   // Read the msg and get the handle of the AFE
   result = qurt_elite_queue_pop_front (me->vpm_aferespq,(uint64_t*)&sMsg);
   if (result != ADSP_EOK)
   {
      MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Failure on mq %p qurt_elite_queue_pop_front: %d\n", me->vpm_aferespq, result);
      elite_msg_return_payload_buffer( &sMsg);
      return result;
   }

   pPayload = ( elite_msg_custom_afe_connect_t *) sMsg.pPayload;
   if( ADSP_EOK == pPayload->response_result)
   {

      // provide the afe handle to VoiceprocRx
      // TODO: This needs restructuring
      result = elite_msg_return_payload_buffer( &sMsg);

      elite_msg_custom_voc_svc_connect_type *pPayloadConnectCmd = NULL;

      nSize = sizeof(elite_msg_custom_voc_svc_connect_type);

      result = elite_msg_create_msg( &sMsg,
            &nSize,
            ELITE_CUSTOM_MSG,
            me->vpm_aferespq,   /* use same respQ */
            NULL,
            ADSP_EOK);
      if( ADSP_FAILED( result))
      {
         MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg!!\n");
         return result;
      }
      pPayloadConnectCmd              = (elite_msg_custom_voc_svc_connect_type *) sMsg.pPayload;
      pPayloadConnectCmd->sec_opcode = VOICEPROC_DISCONNECT_DWN_STREAM_CMD;
      pPayloadConnectCmd->svc_handle_ptr  = currSession->rx_afe_handle;

      result = qurt_elite_queue_push_back(me->session[nSessionIndex]->rx->cmdQ, (uint64_t*)&sMsg );
      if (ADSP_FAILED(result))
      {
         MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED message \n");
         elite_msg_return_payload_buffer( &sMsg);
         return result;
      }
      result = elite_svc_wait_for_ack(&sMsg);

      result = pPayloadConnectCmd->unResponseResult;
      assert(ADSP_SUCCEEDED(result));

      result = elite_msg_return_payload_buffer( &sMsg);

      MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: AFE Rx Disconnected, sessionIndex(%x),pHandle(%p)",nSessionIndex,
            currSession->rx_afe_handle);

      // Reinitialize delay to zero
      me->session[nSessionIndex]->rx_afe_delay = 0;
      me->session[nSessionIndex]->rx_afe_hw_delay = 0;

   }
   else
   {

      result = elite_msg_return_payload_buffer( &sMsg);

      MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: RxAfe Disconnect Ack error = %ld, sessionIndex(%x)!!\n",
            pPayload->response_result, nSessionIndex);
   }

   return result;
}

static int32_t voice_proc_run(void* pInstance, uint16_t nSessionIndex, uint32_t session_thread_clock_mhz)
{
   VoiceProcMgr_t* me = (VoiceProcMgr_t*)pInstance;
   ADSPResult result = ADSP_EOK;
   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: voice_proc_run Invoked sessionIndex(%x)",nSessionIndex);

   if(NULL == me->session[nSessionIndex])
   {
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Attempted to run nonexistent VP sessionIndex(%x) !",nSessionIndex);
      return APR_EBADPARAM;
   }

   //as there is no ec ref source, assuming EC is disable in voiceprocTx

   // Send message to run the Tx/Rx pp.
   if(me->session[nSessionIndex]->tx)
   {
      (void)vpm_send_custom_voiceproc_run_msg(me->vpm_respq, me->session[nSessionIndex]->tx->cmdQ,
            VOICEPROC_RUN_CMD, NULL, TRUE,me->session[nSessionIndex]->tx_afe_drift_ptr,me->session[nSessionIndex]->far_src_drift_ptr,
            session_thread_clock_mhz);
   }
   if(me->session[nSessionIndex]->rx)
   {
      (void)vpm_send_custom_voiceproc_run_msg(me->vpm_respq, me->session[nSessionIndex]->rx->cmdQ,
            VOICEPROC_RUN_CMD, NULL, TRUE,me->session[nSessionIndex]->rx_afe_drift_ptr,NULL,
            session_thread_clock_mhz);
   }

   MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: voice_proc_run End");

   return result;
}

static int32_t voice_proc_stop(void* pInstance, uint16_t nSessionIndex)
{
   VoiceProcMgr_t* me = (VoiceProcMgr_t*)pInstance;
   ADSPResult result = ADSP_EOK;

   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: voice_proc_stop Invoked sessionIndex(%x)",nSessionIndex);

   if(NULL == me->session[nSessionIndex])
   {
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Attempted to stop nonexistent VP sessionIndex(%x)!",nSessionIndex);
      return APR_EBADPARAM;
   }

   // Send message to stop the Tx/Rx pp.
   if(me->session[nSessionIndex]->tx)
   {
      (void)voice_custom_msg_send(me->vpm_respq, me->session[nSessionIndex]->tx->cmdQ,
            VOICEPROC_STOP_CMD, NULL, TRUE);
   }
   if(me->session[nSessionIndex]->rx)
   {
      (void)voice_custom_msg_send(me->vpm_respq, me->session[nSessionIndex]->rx->cmdQ,
            VOICEPROC_STOP_CMD, NULL, TRUE);
   }

   MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: voice_proc_stop End");

   return result;

}

static ADSPResult voice_proc_apr(void* pInstance, elite_msg_any_t* pMsg)
{

   ADSPResult result = ADSP_EOK;
   elite_apr_packet_t * pAprPacket;
   int32_t      rc;
   VoiceProcMgr_t *me = (VoiceProcMgr_t*)pInstance;       // instance structure

   assert(pMsg);

   pAprPacket = (elite_apr_packet_t*) pMsg->pPayload;

   uint16_t handle = elite_apr_if_get_dst_port( pAprPacket);
   uint16_t nSessionIndex = voice_map_handle_to_session_index( handle);

   MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: voice_proc_apr Message handler Invoked, opCode(0x%lx),token(0x%lx),sessionIndex(%x)",
         elite_apr_if_get_opcode(pAprPacket),
         elite_apr_if_get_client_token(pAprPacket),
         nSessionIndex);

   /* parse out the received pakcet.  Note in this current framework we are not prioritizing commands
    * that can be completed immediately (non-gating).  We are simply processing command in order they are received, need a separate
    * Elite Q to push to for command which can't be handled directly from this main dispatcher */

   switch( elite_apr_if_get_opcode(pAprPacket))
   {
      case VPM_CMD_CREATE_SESSION_V2:
      case VPM_CMD_CREATE_SESSION_V3:
         {
            // Associate who is creating the session based on the Domain ID of the client.
            // If the client is the Second Application Domain (APPS), then this supercedes every other setup
            // and assumes the application to be Test client running on Target. Meaning, a single instance
            // of VPM communicating with APPS directly implies that VPM will request for clocks.
            uint16_t client_domain_id = ((elite_apr_if_get_src_addr(pAprPacket)>>8) & 0xFF);
            uint32_t opcode = elite_apr_if_get_opcode(pAprPacket);
            if (APRV2_IDS_DOMAIN_ID_APPS2_V == client_domain_id)
            {
               me->req_clock = TRUE;
            }

            uint16_t  handle;
            uint32_t  status;
            void* createCommand = (void *) elite_apr_if_get_payload_ptr( pAprPacket);

            /* create returns new sessionIndex to use -> TBD should dyn services get handle instead of nSessionIndex.  */
            status = voice_proc_create(pInstance, createCommand, (uint16_t*)&nSessionIndex,opcode);
            handle = voice_map_session_index_to_handle( nSessionIndex);

            if( APR_EOK == status)
            {
               me->session[nSessionIndex]->state = VPM_STOP_STATE;
               me->session[nSessionIndex]->async_struct.tracking_mask = 0;
               me->session[nSessionIndex]->my_handle = handle;
            }
            else
            {
               qurt_elite_memory_free(me->session[nSessionIndex]);
               me->session[nSessionIndex] = NULL;
            }
            /* send the response with status */
            rc = elite_apr_if_alloc_send_ack_result(
                  me->apr_handle,
                  elite_apr_if_get_dst_addr(pAprPacket),
                  (status == APR_EOK) ? handle : 0,       /* send the new session handle to client if valid */
                  elite_apr_if_get_src_addr(pAprPacket),
                  elite_apr_if_get_src_port(pAprPacket),
                  elite_apr_if_get_client_token(pAprPacket),  /* token */
                  elite_apr_if_get_opcode(pAprPacket),
                  status
                  );

            /* free the original packet */
            MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VPM sessionIndex(%x) Create Complete, giving handle(%x)",nSessionIndex, handle);

            rc = elite_apr_if_free( me->apr_handle, pAprPacket);
            break;
         }
      case VPM_CMD_REINIT_SESSION_V2:
      case VPM_CMD_REINIT_SESSION_V3:
         {
            uint32_t opcode = elite_apr_if_get_opcode(pAprPacket);
            if( me->session[nSessionIndex]->state != VPM_STOP_STATE )
            {
               rc = elite_apr_if_end_cmd( me->apr_handle, pAprPacket, APR_ENOTREADY);
            }
            else
            {
               void *createCommand;
               createCommand = (void *) elite_apr_if_get_payload_ptr( pAprPacket);
               rc = voice_proc_reinit(pInstance, createCommand, nSessionIndex, opcode);
               rc = elite_apr_if_end_cmd( me->apr_handle, pAprPacket, rc );
            }
            break;
         }

      case VPM_CMD_DESTROY_SESSION:
         {
            //Destroy mixer ports created
            close_tx_mixer_input( nSessionIndex);
            close_rx_mixer_output( nSessionIndex);
            rc = voice_proc_destroy( pInstance, nSessionIndex );
            rc = elite_apr_if_end_cmd( me->apr_handle, pAprPacket, rc );

            break;
         }

      case VPM_CMD_START_SESSION:
         {
            if( me->session[nSessionIndex]->state != VPM_STOP_STATE )
            {
               rc = elite_apr_if_end_cmd( me->apr_handle, pAprPacket, APR_ENOTREADY);
            }
            else
            {
               uint32_t session_thread_clock_mhz=0;

               // Set mask to indicate session has started.
               // TODO: Future setting would have to be done after the session has moved to RUN state.
               // But right now, we need session_thread_clock_mhz returned from the clock plan settings
               me->session_run_mask = voice_set_bit(me->session_run_mask, nSessionIndex);

               /* Assess clock requests. */
               vpm_set_mmpm_clocks(me, &session_thread_clock_mhz);

               // connect using the cached port values from create/reinit commands
               if( me->session[nSessionIndex]->tx)
               {
                  connect_afe_tx( me, nSessionIndex,
                        me->session[nSessionIndex]->session_config.tx_sampling_rate,
                        me->session[nSessionIndex]->session_config.tx_port,
                        me->session[nSessionIndex]->session_config.far_port,
                        me->session[nSessionIndex]->session_config.tx_topology_id);
               }
               if( me->session[nSessionIndex]->rx)
               {
                  connect_afe_rx(me, nSessionIndex, me->session[nSessionIndex]->session_config.rx_sampling_rate, me->session[nSessionIndex]->session_config.rx_port );
               }

               control_mixer_ports(me, nSessionIndex, ELITEMSG_CUSTOM_VMX_RUN);

               // give session clock to run command.  services can use this for worst case offset calculation.  Only give session clock
               // since that is the minimum which individual service may assume.  Concurrency scenarios may change the clock, but
               // sessions will not be made aware.

               rc = voice_proc_run(me, nSessionIndex, session_thread_clock_mhz);
               // Stop mixer ports TODO: Resume before or after running stream?

               if( APR_EOK == rc)
               {
                  me->session[nSessionIndex]->state = VPM_RUN_STATE;

               }
               rc = elite_apr_if_end_cmd( me->apr_handle, pAprPacket, rc );
            }
            break;
         }

      case VPM_CMD_STOP_SESSION:
         {
            if( me->session[nSessionIndex]->state != VPM_RUN_STATE )
            {
               rc = elite_apr_if_end_cmd( me->apr_handle, pAprPacket, APR_ENOTREADY);
            }
            else
            {
               uint32_t session_thread_clock_mhz=0;

               // disconnect from Afe Tx and Rx...For Tx this will prevent AFE from sending buffers to Vptx.  The buffers
               // will be guaranteed returned to AFE after voice_proc_stop. After this time we
               // will issue the ELITEMSG_CUSTOM_AFEDISCONNECT_REQ command as part of send_afe_tx_dataq_destroyed_msg function

               if( me->session[nSessionIndex]->tx)
               {
                  rc = disconnect_afe_tx( me,
                        nSessionIndex,
                        me->session[nSessionIndex]->session_config.tx_port,
                        me->session[nSessionIndex]->session_config.far_port);
               }

               rc = voice_proc_stop(me, nSessionIndex);
               // Stop mixer ports
               control_mixer_ports(me, nSessionIndex, ELITEMSG_CUSTOM_VMX_STOP);

               if( me->session[nSessionIndex]->rx)
               {
                  /*
                   * the following code is to inform AFE that voice is about to disconnect. With this information
                   * AFE would ramp down the voice data before pumping out. voice would sleep for 2 ms
                   * to allow the rampdown process to complete
                   */
                  avtimer_open_param_t open_param;
                  avtimer_drv_handle_t       avt_drv;
                  open_param.client_name = (char*)"VPM";
                  open_param.flag = 0;

                  if (ADSP_EOK != (result = avtimer_drv_hw_open(&avt_drv, &open_param)))
                  {
                     MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Failed to Open AVTimer Driver for VPM_SLEEP");
                  }
                  else
                  {
                     rc = send_disconnect_marker_to_afe(me, nSessionIndex, me->session[nSessionIndex]->session_config.rx_port);
                     MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VPM going to sleep for 2ms");
                     avtimer_drv_sleep(2000) ;
                     MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VPM coming out of sleep");
                     if (ADSP_EOK != (result = avtimer_drv_hw_close(avt_drv)))
                     {
                        MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Failed to close AVTimer Driver for VPM_SLEEP");
                     }
                  }
                  // Rx is stopped so we can disconnect downstream
                  rc = disconnect_afe_rx( me, nSessionIndex, me->session[nSessionIndex]->session_config.rx_port);
               }

               // now it's safe to send ELITEMSG_CUSTOM_AFEDISCONNECT_REQ message
               if( me->session[nSessionIndex]->tx)
               {
                  send_afe_tx_dataq_destroyed_msg( me, nSessionIndex, me->session[nSessionIndex]->session_config.tx_port, me->session[nSessionIndex]->session_config.far_port);
                  me->session[nSessionIndex]->tx_afe_handle = NULL;
                  me->session[nSessionIndex]->far_afe_handle = NULL;
               }
               if( APR_EOK == rc)
               {
                  me->session[nSessionIndex]->state = VPM_STOP_STATE;

                  // Clear mask to indicate session has stopped.
                  me->session_run_mask = voice_clr_bit(me->session_run_mask, nSessionIndex);

                  /* Reassess clock requests. */
                  vpm_set_mmpm_clocks(me, &session_thread_clock_mhz);
               }
               rc = elite_apr_if_end_cmd( me->apr_handle, pAprPacket, rc );
            }

            break;
         }

      case VOICE_CMD_SET_PARAM_V2:
         {
            uint32_t nSize = 0;
            elite_msg_any_t sMsgTx;
            elite_msg_any_t sMsgRx;
            uint32_t nFreeIndex;
            async_struct_t *async_struct;
            uint32_t nPayloadAddress = 0;
            elite_mem_shared_memory_map_t mem_shared_memory_map = {0};
            voice_set_param_v2_t* pVoiceProcSetParams = (voice_set_param_v2_t *) elite_apr_if_get_payload_ptr(pAprPacket);

            mem_shared_memory_map.unMemMapClient = vPM_memory_map_client;
            mem_shared_memory_map.unMemMapHandle = pVoiceProcSetParams->mem_map_handle;
            if (NULL != pVoiceProcSetParams->mem_map_handle) // check memory map handle for out of band data
            {
               if (NULL != vPM_memory_map_client) // check if the vpm client is valid
               {
                  result = voice_cmn_check_align_size(pVoiceProcSetParams->payload_address_lsw, pVoiceProcSetParams->payload_address_msw, pVoiceProcSetParams->payload_size, 4);
                  if( ADSP_FAILED(result))
                  {
                     MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: VPM set param: address has to be 4-byte aligned and size has to be multiple of 4 bytes, lsw(%lx) msw(%lx) size(%lx)",pVoiceProcSetParams->payload_address_lsw, pVoiceProcSetParams->payload_address_msw, pVoiceProcSetParams->payload_size);
                  }
                  else
                  {
                     result = elite_mem_map_get_shm_attrib(pVoiceProcSetParams->payload_address_lsw, pVoiceProcSetParams->payload_address_msw, pVoiceProcSetParams->payload_size, &mem_shared_memory_map);
                     if( ADSP_SUCCEEDED(result))
                     {
                        MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VPM Memory Map lsw(%lx) msw(%lx) virtual address(%lx)",pVoiceProcSetParams->payload_address_lsw, pVoiceProcSetParams->payload_address_msw, mem_shared_memory_map.unVirtAddr);
                        result = elite_mem_invalidate_cache_v2(&mem_shared_memory_map);

#if defined(__qdsp6__) && !defined(SIM)
                        int8_t *bufptr[4] = { (int8_t *) mem_shared_memory_map.unVirtAddr, NULL, NULL, NULL };
                        voice_log_buffer( bufptr,
                              VOICE_LOG_CHAN_VPM_SET_PARAM,
                              nSessionIndex,
                              qurt_elite_timer_get_time(),
                              VOICE_LOG_DATA_FORMAT_PCM_MONO,
                              1,   /* dummy value */
                              pVoiceProcSetParams->payload_size,
                              NULL);
#endif
                     }
                  }
               }
               else
               {
                  result = ADSP_EFAILED;
               }
               if( ADSP_FAILED(result))
               {
                  MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: VPM set param error");
                  elite_apr_if_end_cmd( me->apr_handle, pAprPacket, APR_EFAILED);
                  break;
               }
               nPayloadAddress = mem_shared_memory_map.unVirtAddr;
            }
            else
            {  // for in-band
               nPayloadAddress = (uint32_t)pVoiceProcSetParams + sizeof(voice_set_param_v2_t);
            }


            async_struct = &(me->session[nSessionIndex]->async_struct);
            /* check if we have a place to hold this pending packet */
            result = voice_init_async_control( async_struct, pAprPacket, &nFreeIndex);
            if( ADSP_SUCCEEDED(result))
            {

               if( me->session[nSessionIndex]->tx )
               {
                  //*************** Compose FADD Message *****************
                  nSize = sizeof(elite_msg_param_cal_t);
                  result = elite_msg_create_msg( &sMsgTx,
                        &nSize,
                        ELITE_CMD_SET_PARAM,
                        me->vpm_respq,
                        VOICE_ASYNC_STUFF_TOKEN(nSessionIndex, nFreeIndex),
                        ADSP_EOK);
                  if( ADSP_FAILED( result ))
                  {
                     MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg for tx setparam cmd!!\n");
                     return result;
                  }

                  elite_msg_param_cal_t* pFaddPayload;
                  pFaddPayload = (elite_msg_param_cal_t*) sMsgTx.pPayload;
                  pFaddPayload->pnParamData = (int32_t*)nPayloadAddress;
                  pFaddPayload->unSize = pVoiceProcSetParams->payload_size;
                  pFaddPayload->unParamId = ELITEMSG_PARAM_ID_CAL;

                  // MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Dispatch SetParams to Tx/Rx, Apr packet addr = 0x%x!!\n",(uint32_t)pAprPacket);
                  result = qurt_elite_queue_push_back(me->session[nSessionIndex]->tx->cmdQ, (uint64_t*)&sMsgTx);

                  if (ADSP_FAILED(result))
                  {
                     elite_msg_return_payload_buffer( &sMsgTx);
                     MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to set params on tx!!\n");
                  }
                  else
                  {
                     async_struct->async_status[nFreeIndex].state++;
                  }
               }

               if( me->session[nSessionIndex]->rx )
               {
                  //*************** Compose FADD Message *****************
                  nSize = sizeof(elite_msg_param_cal_t);
                  result = elite_msg_create_msg( &sMsgRx,
                        &nSize,
                        ELITE_CMD_SET_PARAM,
                        me->vpm_respq,
                        VOICE_ASYNC_STUFF_TOKEN(nSessionIndex, nFreeIndex),
                        ADSP_EOK);
                  if( ADSP_FAILED( result ))
                  {
                     MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg for rx setparam cmd!!\n");
                     return result;
                  }

                  elite_msg_param_cal_t* pFaddPayload;
                  pFaddPayload = (elite_msg_param_cal_t*) sMsgRx.pPayload;
                  pFaddPayload->pnParamData = (int32_t*)nPayloadAddress;
                  pFaddPayload->unSize = pVoiceProcSetParams->payload_size;
                  pFaddPayload->unParamId = ELITEMSG_PARAM_ID_CAL;

                  // MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Dispatch SetParams to Rx, Apr packet addr = 0x%x!!\n",(uint32_t)pAprPacket);
                  result = qurt_elite_queue_push_back(me->session[nSessionIndex]->rx->cmdQ, (uint64_t*)&sMsgRx);

                  if (ADSP_FAILED(result))
                  {
                     elite_msg_return_payload_buffer( &sMsgRx);
                     MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to set params on rx!!\n");
                  }
                  else
                  {
                     async_struct->async_status[nFreeIndex].state++;
                  }
               }

               /* if state == 0, nothing is pending.  Can finish command here */
               if( 0 == async_struct->async_status[nFreeIndex].state)
               {
                  rc = elite_apr_if_end_cmd( me->apr_handle, pAprPacket, APR_EFAILED);
                  voice_end_async_control( async_struct, nFreeIndex);
               }
               else
               {
                  me->wait_mask &= (~(me->cmd_mask));
               }
            }
            else
            {
               /* no free index to store asyncStatus, can end this command as EBUSY */
               rc = elite_apr_if_end_cmd( me->apr_handle, pAprPacket, APR_EBUSY);
            }
            break;
         }
      case VOICE_CMD_SET_PARAM_V3:
         {
            elite_msg_any_t msg_rx_t;
            elite_msg_any_t msg_tx_t;
            uint32_t free_index, size;
            ADSPResult rc_rx = ADSP_EOK;
            ADSPResult rc_tx = ADSP_EOK;
            //async_struct_t *async_struct;
            //async_struct = &(me->session[nSessionIndex]->async_struct);
            voice_cmd_set_param_v3_t* voice_set_params_ptr = (voice_cmd_set_param_v3_t*) elite_apr_if_get_payload_ptr(pAprPacket);
#if defined(__qdsp6__) && !defined(SIM)
            //log the buffer
            voice_log_cal_data(voice_set_params_ptr->cal_handle, VOICE_LOG_CHAN_VPM_SET_PARAM, nSessionIndex);
#endif
            /* check if we have a place to hold this pending packet */
            rc = voice_init_async_control( &me->session[nSessionIndex]->async_struct,
                  pAprPacket, &free_index);
            if( ADSP_SUCCEEDED(rc))
            {
               if( me->session[nSessionIndex]->rx )
               {
                  //*************** Compose FADD Message *****************
                  size = sizeof(elite_msg_custom_set_param_v3_type);
                  //using rc_rx for result as this is used for immediate termination of command if both Tx and Rx failed
                  rc_rx = elite_msg_create_msg( &msg_rx_t,
                        &size,
                        ELITE_CUSTOM_MSG,
                        me->vpm_respq,
                        VOICE_ASYNC_STUFF_TOKEN(nSessionIndex, free_index),
                        ADSP_EOK);
                  if( ADSP_FAILED( rc_rx ))
                  {
                     MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: FAILED to create rx set_param EliteMsg, nSessionIndex(%x)",(int) nSessionIndex);
                  }
                  else
                  {
                     elite_msg_custom_set_param_v3_type* fadd_payload_ptr;
                     fadd_payload_ptr = (elite_msg_custom_set_param_v3_type*) msg_rx_t.pPayload;
                     fadd_payload_ptr->sec_opcode = VOICEPROC_SET_PARAM_V3_CMD;
                     fadd_payload_ptr->cal_handle = voice_set_params_ptr->cal_handle;

                     rc_rx = qurt_elite_queue_push_back(me->session[nSessionIndex]->rx->cmdQ, (uint64_t*)&msg_rx_t);
                     if (ADSP_FAILED(rc_rx))
                     {
                        MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: FAILED to push set params on rx, nSessionIndex(%x)",(int) nSessionIndex);
                        elite_msg_return_payload_buffer( &msg_rx_t);
                     }
                     else
                     {
                        me->session[nSessionIndex]->async_struct.async_status[free_index].state++;
                     }
                  }
               }
               if( me->session[nSessionIndex]->tx )
               {
                  //*************** Compose FADD Message *****************
                  size = sizeof(elite_msg_custom_set_param_v3_type);
                  //using rc_tx for result as this is used for immediate termination of command if both Tx and Rx failed
                  rc_tx = elite_msg_create_msg( &msg_tx_t,
                        &size,
                        ELITE_CUSTOM_MSG,
                        me->vpm_respq,
                        VOICE_ASYNC_STUFF_TOKEN(nSessionIndex, free_index),
                        ADSP_EOK);
                  if( ADSP_FAILED( rc_tx ))
                  {
                     MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: FAILED to create tx set_param EliteMsg, nSessionIndex(%x)",(int) nSessionIndex);
                  }
                  else
                  {
                     elite_msg_custom_set_param_v3_type* fadd_payload_ptr;
                     fadd_payload_ptr = (elite_msg_custom_set_param_v3_type*) msg_tx_t.pPayload;
                     fadd_payload_ptr->cal_handle = voice_set_params_ptr->cal_handle;
                     fadd_payload_ptr->sec_opcode = VOICEPROC_SET_PARAM_V3_CMD;

                     rc_tx = qurt_elite_queue_push_back(me->session[nSessionIndex]->tx->cmdQ, (uint64_t*)&msg_tx_t);
                     if (ADSP_FAILED(rc_tx))
                     {
                        MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: FAILED to push set params on tx, nSessionIndex(%x)",(int) nSessionIndex);
                        elite_msg_return_payload_buffer( &msg_tx_t);
                     }
                     else
                     {
                        me->session[nSessionIndex]->async_struct.async_status[free_index].state++;
                     }
                  }
               }

               /* initialize results */
               /*update result as this is what is returned outside*/
               result = rc_tx | rc_rx;

               /* if both Rx and Tx failed, send result now, else we process async */
               if( !((me->session[nSessionIndex]->rx && ADSP_SUCCEEDED(rc_rx)) || (me->session[nSessionIndex]->tx && ADSP_SUCCEEDED(rc_tx))) )
               {
                  rc = elite_apr_if_end_cmd( me->apr_handle, pAprPacket, APR_EFAILED);
                  voice_end_async_control( &me->session[nSessionIndex]->async_struct, free_index);
               }
               else
               {
                  // don't listen for other commands. this can cause issues if RUN is sent right after SET_PARAM_v3 without waiting for the response
                  // VPM waits synchronously for RUN response, so reponses for SET_PARAM_V3 can get misinterpreted as responses for run
                  // resulting in no response for SET_PARAM_V3
                  me->wait_mask &= (~(me->cmd_mask));
               }
            }
            else
            {
               rc = elite_apr_if_end_cmd( me->apr_handle, pAprPacket, APR_EBUSY);
            }
            break;
         }


      case VOICE_CMD_SET_SOFT_MUTE_V2:
         {
            voice_set_soft_mute_v2_t    *pSetMuteCmd = (voice_set_soft_mute_v2_t *) elite_apr_if_get_payload_ptr( pAprPacket);

            MSG_4(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VOICE_CMD_SET_SOFT_MUTE_V2 on VPM, direction(0x%x), muteCmd(%d), rampDuration_in_msec(%d),sessionState(0x%lx)",
                  pSetMuteCmd->direction, pSetMuteCmd->mute, pSetMuteCmd->ramp_duration, me->session[nSessionIndex]->state );

            //validate params.  Can do SET_MUTE in run state and parallel to other commands (TODO: should not process this or any other commands with destroy pending)
            if( pSetMuteCmd->mute > VOICE_MUTE         ||
                  pSetMuteCmd->direction   > VOICE_SET_MUTE_TX_AND_RX ||
                  pSetMuteCmd->ramp_duration > MAX_MUTE_RAMP_DURATION_MSEC
              )
            {
               rc = elite_apr_if_end_cmd( me->apr_handle, pAprPacket, APR_EBADPARAM);
            }
            else
            {
               ADSPResult rcRx = ADSP_EOK;
               ADSPResult rcTx = ADSP_EOK;

               if( pSetMuteCmd->direction == VOICE_SET_MUTE_TX_ONLY ||
                     pSetMuteCmd->direction == VOICE_SET_MUTE_TX_AND_RX)

               {
                  if( me->session[nSessionIndex]->tx )
                  {
                     rcTx = vpm_send_tx_mute( me, nSessionIndex, pSetMuteCmd);
                  }
                  else
                  {
                     rcTx = ADSP_EBADPARAM;
                  }
               }

               if( pSetMuteCmd->direction == VOICE_SET_MUTE_RX_ONLY ||
                     pSetMuteCmd->direction == VOICE_SET_MUTE_TX_AND_RX)

               {
                  if( me->session[nSessionIndex]->rx )
                  {
                     rcRx = vpm_send_rx_mute( me, nSessionIndex, pSetMuteCmd);
                  }
                  else
                  {
                     rcRx = ADSP_EBADPARAM;
                  }
               }

               /* check if any action attempt failed...if so send error response */
               if( ((pSetMuteCmd->direction == VOICE_SET_MUTE_RX_ONLY || pSetMuteCmd->direction == VOICE_SET_MUTE_TX_AND_RX) && ADSP_FAILED(rcRx)) ||
                     ((pSetMuteCmd->direction == VOICE_SET_MUTE_TX_ONLY || pSetMuteCmd->direction == VOICE_SET_MUTE_TX_AND_RX) && ADSP_FAILED(rcTx)))
               {
                  rc = elite_apr_if_end_cmd( me->apr_handle, pAprPacket, APR_EFAILED);
               }
               else
               {
                  rc = elite_apr_if_end_cmd( me->apr_handle, pAprPacket, APR_EOK);
               }
            }
            break;
         }
      case VOICE_CMD_START_HOST_PCM_V2:
         {

            voice_start_host_pcm_v2_t    *pStartHostPcmCmd = (voice_start_host_pcm_v2_t *) elite_apr_if_get_payload_ptr( pAprPacket);

            MSG_3(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VOICE_CMD_START_HOST_PCM_V2, numTapPoints(%ld) numActivePoints(%d) sessionState(0x%lx)", pStartHostPcmCmd->num_tap_points,
                  me->session[nSessionIndex]->host_pcm_num_active_tap_points,
                  me->session[nSessionIndex]->state );

            // currently can support 1 tx and 1 rx tap point per session. More comprehensive error checks can be done, if session keeps list of all active tap points.
            // it's not valid to start a tap point that has already been started, need to first stop the tap point.  This is not currently checked.  If the command attempts
            // to start a tap point that is already started, the whold command should be a NOP
            if( pStartHostPcmCmd->num_tap_points > 2 || pStartHostPcmCmd->num_tap_points == 0)
            {
               rc = elite_apr_if_end_cmd( me->apr_handle, pAprPacket, APR_EBADPARAM);
            }
            else
            {
               ADSPResult rcRx = ADSP_EOK;
               ADSPResult rcTx = ADSP_EOK;

               if( me->session[nSessionIndex]->tx )
               {
                  rcTx = vpm_send_start_tx_host_pcm( me, nSessionIndex, pAprPacket);
               }

               if( me->session[nSessionIndex]->rx )
               {
                  rcRx = vpm_send_start_rx_host_pcm( me, nSessionIndex, pAprPacket);
               }

               /* check if any action attempt failed...if so send error response */
               // TODO: if enabling Rx & Tx, and one succeeded, and one failed -> how to handle
               if( ADSP_FAILED(rcRx) && ADSP_FAILED(rcTx))
               {
                  MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: VOICE_CMD_START_HOST_PCM_V2 FAILED, sessionState(%#lx), rx_result(%#x), tx_result(%#x)", \
                        me->session[nSessionIndex]->state,
                        rcRx,
                        rcTx );
                  rc = elite_apr_if_end_cmd( me->apr_handle, pAprPacket, APR_EFAILED);
               }
               else if( ADSP_FAILED(rcRx) || ADSP_FAILED(rcTx))
               {
                  MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: VOICE_CMD_START_HOST_PCM_V2 : rx or tx FAILED, but ignoring, sessionState(%#lx), rx_result(%#x), tx_result(%#x)", \
                        me->session[nSessionIndex]->state,
                        rcRx,
                        rcTx );
                  rc = elite_apr_if_end_cmd( me->apr_handle, pAprPacket, APR_EOK);
               }
               else
               {
                  rc = elite_apr_if_end_cmd( me->apr_handle, pAprPacket, APR_EOK);
                  // in absense of robust error checking, we can just increment the number of active tap points
                  me->session[nSessionIndex]->host_pcm_num_active_tap_points += pStartHostPcmCmd->num_tap_points;
               }
            }
            break;
         }
      case VOICE_CMD_STOP_HOST_PCM:
         {
            MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VOICE_CMD_STOP_HOST_PCM, numActivePoints(%d) sessionState(0x%lx)", me->session[nSessionIndex]->host_pcm_num_active_tap_points,
                  me->session[nSessionIndex]->state );
            // basic sanity check.
            //   - Currently allow only 2 tap points to be enabled, so no more than 2 can be disabled
            //   - We should be disabling at least one tap point
            //   - We should be disabling tap points only if we already have a tap point active
            // TODO:  if we are trying to stop a tap point that isn't started, treat the whole command as a nop
            {
               ADSPResult rcRx = ADSP_EOK;
               ADSPResult rcTx = ADSP_EOK;

               if( me->session[nSessionIndex]->tx )
               {
                  rcTx = vpm_send_stop_tx_host_pcm( me, nSessionIndex, pAprPacket);
               }

               if( me->session[nSessionIndex]->rx)
               {
                  rcRx = vpm_send_stop_rx_host_pcm( me, nSessionIndex, pAprPacket);
               }

               if( ADSP_SUCCEEDED(rcRx) && ADSP_SUCCEEDED(rcTx))
               {
                  rc = elite_apr_if_end_cmd( me->apr_handle, pAprPacket, APR_EOK);
                  // in absense of robust error checking, we can just decrement the number of active tap points
               }
               else
               {
                  rc = elite_apr_if_end_cmd( me->apr_handle, pAprPacket, APR_EFAILED);
               }
            }
            break;
         }
      case VOICE_EVT_PUSH_HOST_BUF_V2:
         {
            voice_evt_push_host_pcm_buf_v2_t    *pHostPcmData = (voice_evt_push_host_pcm_buf_v2_t *) elite_apr_if_get_payload_ptr( pAprPacket);

            MSG_3(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VOICE_EVT_PUSH_HOST_BUF_V2, tap_point(0x%lx) mask(%ld) sessionIndex(%d)",
                  pHostPcmData->tap_point, pHostPcmData->mask, nSessionIndex );

            if( NULL == vPM_memory_map_client)
            {
               MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: VPM Q Event Host PCM buffer, no mem map client, Event dropped");
               rc = elite_apr_if_free( me->apr_handle, pAprPacket);
            }
            else if( NULL == me->session[nSessionIndex])
            {
               MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: VPM Q Event Host PCM buffer, Inactive Session, Event dropped");
               rc = elite_apr_if_free( me->apr_handle, pAprPacket);
            }
            else
            {
               ADSPResult result = ADSP_EFAILED;

               /* push directly on to CMD Q (need to check which Q appropriate based on tap point -- assume default tap points only).  treat as a command, since dataQ already used */
               if( VOICEPROC_MODULE_TX == pHostPcmData->tap_point)
               {
                  if( me->session[nSessionIndex]->tx )
                  {
                     result =qurt_elite_queue_push_back(me->session[nSessionIndex]->tx->cmdQ,(uint64_t*)pMsg);
                  }
               }
               else if( VOICEPROC_MODULE_RX == pHostPcmData->tap_point)
               {
                  if( me->session[nSessionIndex]->rx )
                  {
                     result =qurt_elite_queue_push_back(me->session[nSessionIndex]->rx->cmdQ,(uint64_t*)pMsg);
                  }
               }
               if( ADSP_FAILED(result))
               {
                  MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: VPM Q Event Host PCM buffer to endpoint Err, Event dropped");
                  rc = elite_apr_if_free( me->apr_handle, pAprPacket);
               }
            }
            break;
         }
      case VPM_CMD_GET_KPPS:
         {
            uint32_t nSize = 0;
            elite_msg_any_t sMsgTx;
            elite_msg_any_t sMsgRx;
            uint32_t nFreeIndex;
            async_struct_t *async_struct;
            void* pnKpps;
            elite_apr_packet_t * pAprInBandPacket = NULL;
            if((NULL == me->session[nSessionIndex]->rx) && (NULL == me->session[nSessionIndex]->tx))
            {
               // Blank session, cannot return any valid kpps, respond with failure
               rc = elite_apr_if_end_cmd( me->apr_handle, pAprPacket, APR_EFAILED);
               break;
            }

            async_struct = &(me->session[nSessionIndex]->async_struct);
            /* check if we have a place to hold this pending packet */
            result = voice_init_async_control( async_struct, pAprPacket, &nFreeIndex);
            if( ADSP_SUCCEEDED(result))
            {
               // In-Band processing
               int32_t rc;
               rc = elite_apr_if_alloc_cmd_rsp(
                     me->apr_handle,
                     pAprPacket->dst_addr,
                     pAprPacket->dst_port,
                     pAprPacket->src_addr,
                     pAprPacket->src_port,
                     pAprPacket->token,
                     VPM_RSP_GET_KPPS_ACK,
                     sizeof(vpm_get_kpps_ack_t),
                     &pAprInBandPacket
                     );
               if ( rc || NULL == pAprInBandPacket)
               {
                  MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Failed to create APR packet for GetKPPS Ack in VPM 0x%8x ",(int)rc);
                  rc = elite_apr_if_free( me->apr_handle, pAprPacket);
                  return ADSP_EFAILED;
               }
               // overwrite the get param apr packet with the response pkt in the async structure tracking
               async_struct->async_status[nFreeIndex].apr_packet_ptr = pAprInBandPacket;

               pnKpps= elite_apr_if_get_payload_ptr(pAprInBandPacket);

               // free the apr getparam pkt and use the apr getparam_ack pkt
               rc = elite_apr_if_free( me->apr_handle, pAprPacket);

               // size of the FADD message
               nSize = sizeof(elite_msg_custom_kpps_type);

               if( me->session[nSessionIndex]->tx )
               {
                  //*************** Compose FADD Message *****************
                  result = elite_msg_create_msg( &sMsgTx,
                        &nSize,
                        ELITE_CUSTOM_MSG,
                        me->vpm_respq,
                        VOICE_ASYNC_STUFF_TOKEN(nSessionIndex, nFreeIndex),
                        ADSP_EOK);
                  if( ADSP_FAILED( result ))
                  {
                     MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: FAILED to create EliteMsg for tx getkpps cmd!!\n");
                     return result;
                  }

                  elite_msg_custom_kpps_type* pFaddPayload;
                  pFaddPayload = (elite_msg_custom_kpps_type*) sMsgTx.pPayload;
                  pFaddPayload->sec_opcode= VOICEPROC_GET_KPPS_CMD;
                  pFaddPayload->pnKpps= pnKpps;

                  // Dispatch message to Tx
                  result = qurt_elite_queue_push_back(me->session[nSessionIndex]->tx->cmdQ, (uint64_t*)&sMsgTx);

                  if (ADSP_FAILED(result))
                  {
                     elite_msg_return_payload_buffer(&sMsgTx);
                     MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: FAILED to send KPPS cmd to Vptx !!\n");
                  }
                  else
                  {
                     async_struct->async_status[nFreeIndex].state++;
                  }
               }

               if( me->session[nSessionIndex]->rx )
               {
                  //*************** Compose FADD Message *****************
                  result = elite_msg_create_msg( &sMsgRx,
                        &nSize,
                        ELITE_CUSTOM_MSG,
                        me->vpm_respq,
                        VOICE_ASYNC_STUFF_TOKEN(nSessionIndex, nFreeIndex),
                        ADSP_EOK);
                  if( ADSP_FAILED( result ))
                  {
                     MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: FAILED to create EliteMsg for rx getkpps cmd!!\n");
                     return result;
                  }

                  elite_msg_custom_kpps_type* pFaddPayload;
                  pFaddPayload = (elite_msg_custom_kpps_type*) sMsgRx.pPayload;
                  pFaddPayload->sec_opcode = VOICEPROC_GET_KPPS_CMD;
                  pFaddPayload->pnKpps= pnKpps;

                  // Dispatch message to Rx
                  result = qurt_elite_queue_push_back(me->session[nSessionIndex]->rx->cmdQ, (uint64_t*)&sMsgRx);

                  if (ADSP_FAILED(result))
                  {
                     elite_msg_return_payload_buffer(&sMsgRx);
                     MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: FAILED to send KPPS cmd to Vprx !!\n");
                  }
                  else
                  {
                     async_struct->async_status[nFreeIndex].state++;
                  }
               }

               if(async_struct->async_status[nFreeIndex].state)
               {
                  me->wait_mask &= (~(me->cmd_mask));
               }
            }
            else
            {
               /* no free index to store asyncStatus, can end this command as EBUSY */
               rc = elite_apr_if_end_cmd( me->apr_handle, pAprPacket, APR_EBUSY);
            }
            break;
         }
      case VPM_CMD_GET_DELAY:
         {
            uint32_t nSize = 0;
            elite_msg_any_t sMsgTx;
            elite_msg_any_t sMsgRx;
            uint32_t nFreeIndex;
            async_struct_t *async_struct;
            void* payload_ptr;
            elite_apr_packet_t * pAprInBandPacket = NULL;
            if((NULL == me->session[nSessionIndex]->rx) && (NULL == me->session[nSessionIndex]->tx))
            {
               // Blank session, cannot return any valid delay, respond with failure
               rc = elite_apr_if_end_cmd( me->apr_handle, pAprPacket, APR_EFAILED);
               break;
            }

            async_struct = &(me->session[nSessionIndex]->async_struct);
            /* check if we have a place to hold this pending packet */
            result = voice_init_async_control( async_struct, pAprPacket, &nFreeIndex);
            if( ADSP_SUCCEEDED(result))
            {
               // In-Band processing
               int32_t rc;
               rc = elite_apr_if_alloc_cmd_rsp(
                     me->apr_handle,
                     pAprPacket->dst_addr,
                     pAprPacket->dst_port,
                     pAprPacket->src_addr,
                     pAprPacket->src_port,
                     pAprPacket->token,
                     VPM_RSP_GET_DELAY_ACK,
                     sizeof(vpm_get_delay_ack_t),
                     &pAprInBandPacket
                     );
               if ( rc || NULL == pAprInBandPacket)
               {
                  MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Failed to create APR packet for Get Delay Ack in VPM 0x%8x ",(int)rc);
                  rc = elite_apr_if_free( me->apr_handle, pAprPacket);
                  return ADSP_EFAILED;
               }
               // overwrite the get param apr packet with the response pkt in the async structure tracking
               async_struct->async_status[nFreeIndex].apr_packet_ptr = pAprInBandPacket;

               payload_ptr= elite_apr_if_get_payload_ptr(pAprInBandPacket);

               // free the apr get delay pkt and use the apr get_delay_ack pkt
               rc = elite_apr_if_free( me->apr_handle, pAprPacket);

               // size of the FADD message
               nSize = sizeof(elite_msg_custom_delay_type);

               if( me->session[nSessionIndex]->tx )
               {
                  //*************** Compose FADD Message *****************
                  result = elite_msg_create_msg( &sMsgTx,
                        &nSize,
                        ELITE_CUSTOM_MSG,
                        me->vpm_respq,
                        VOICE_ASYNC_STUFF_TOKEN(nSessionIndex, nFreeIndex),
                        ADSP_EOK);
                  if( ADSP_FAILED( result ))
                  {
                     MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: FAILED to create EliteMsg for tx getdelay cmd!!\n");
                     return result;
                  }

                  elite_msg_custom_delay_type* pFaddPayload;
                  pFaddPayload = (elite_msg_custom_delay_type*) sMsgTx.pPayload;
                  pFaddPayload->sec_opcode = VOICEPROC_GET_DELAY_CMD;
                  pFaddPayload->delay_ptr = payload_ptr;

                  // Dispatch message to Tx
                  result = qurt_elite_queue_push_back(me->session[nSessionIndex]->tx->cmdQ, (uint64_t*)&sMsgTx);

                  if (ADSP_FAILED(result))
                  {
                     elite_msg_return_payload_buffer(&sMsgTx);
                     MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: FAILED to send delay cmd to Vptx !!\n");
                  }
                  else
                  {
                     async_struct->async_status[nFreeIndex].state++;
                  }
               }

               if( me->session[nSessionIndex]->rx )
               {
                  //*************** Compose FADD Message *****************
                  result = elite_msg_create_msg( &sMsgRx,
                        &nSize,
                        ELITE_CUSTOM_MSG,
                        me->vpm_respq,
                        VOICE_ASYNC_STUFF_TOKEN(nSessionIndex, nFreeIndex),
                        ADSP_EOK);
                  if( ADSP_FAILED( result ))
                  {
                     MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: FAILED to create EliteMsg for rx getdelay cmd!!\n");
                     return result;
                  }

                  elite_msg_custom_delay_type* pFaddPayload;
                  pFaddPayload = (elite_msg_custom_delay_type*) sMsgRx.pPayload;
                  pFaddPayload->sec_opcode = VOICEPROC_GET_DELAY_CMD;
                  pFaddPayload->delay_ptr = payload_ptr;

                  // Dispatch message to Rx
                  result = qurt_elite_queue_push_back(me->session[nSessionIndex]->rx->cmdQ, (uint64_t*)&sMsgRx);

                  if (ADSP_FAILED(result))
                  {
                     elite_msg_return_payload_buffer(&sMsgRx);
                     MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: FAILED to send delay cmd to Vprx !!\n");
                  }
                  else
                  {
                     async_struct->async_status[nFreeIndex].state++;
                  }
               }

               if(async_struct->async_status[nFreeIndex].state)
               {
                  me->wait_mask &= (~(me->cmd_mask));
               }
            }
            else
            {
               /* no free index to store asyncStatus, can end this command as EBUSY */
               rc = elite_apr_if_end_cmd( me->apr_handle, pAprPacket, APR_EBUSY);
            }
            break;
         }
      case VOICE_CMD_GET_PARAM_V2:
         {  // TODO: In band support
            uint32_t nSize = 0;
            elite_msg_any_t sMsgTx;
            elite_msg_any_t sMsgRx;
            uint32_t nFreeIndex;
            async_struct_t *async_struct;
            voice_get_param_v2_t *pPayload;
            int32_t *pGetParam;
            elite_apr_packet_t * pAprInBandPacket = NULL;
            pPayload = (voice_get_param_v2_t*)elite_apr_if_get_payload_ptr(pAprPacket);
            qurt_elite_memorymap_mapping_mode_t mapping_mode;
            uint32_t alignment_check;

            // Validate address for out of band
            if(NULL != pPayload->mem_map_handle)
            {
               result = qurt_elite_memorymap_get_mapping_mode(vPM_memory_map_client,
                     pPayload->mem_map_handle, &mapping_mode);
               if(ADSP_FAILED(result))
               {
                  rc = elite_apr_if_free( me->apr_handle, pAprPacket);
                  return ADSP_EBADPARAM;
               }

               if(QURT_ELITE_MEMORYMAP_HEAP_ADDR_MAPPING == mapping_mode)
               {
                  alignment_check = 4;
               }
               else
               {
                  alignment_check = 32;
               }

               result = voice_cmn_check_align_size(pPayload->payload_address_lsw, pPayload->payload_address_msw, pPayload->param_max_size, alignment_check);
               if(ADSP_FAILED(result))
               {
                  voice_get_param_ack_t get_param_ack;
                  get_param_ack.status = APR_EBADPARAM;
                  elite_apr_if_alloc_send_ack(me->apr_handle,
                        pAprPacket->dst_addr,
                        pAprPacket->dst_port,
                        pAprPacket->src_addr,
                        pAprPacket->src_port,
                        pAprPacket->token,
                        VOICE_RSP_GET_PARAM_ACK,
                        &get_param_ack,
                        sizeof(voice_get_param_ack_t)
                        );
                  rc = elite_apr_if_free( me->apr_handle, pAprPacket);
                  return ADSP_EBADPARAM;
               }
            }

            async_struct = &(me->session[nSessionIndex]->async_struct);
            /* check if we have a place to hold this pending packet */
            result = voice_init_async_control( async_struct, pAprPacket, &nFreeIndex);
            if( ADSP_SUCCEEDED(result))
            {
               if ( (NULL == pPayload->mem_map_handle) &&
                     (( NULL != me->session[nSessionIndex]->tx ) ||
                      ( NULL != me->session[nSessionIndex]->rx )))
               {
                  // In-Band processing
                  // allocate response apr packet and use that for get param
                  int32_t rc;
                  rc = elite_apr_if_alloc_cmd_rsp(
                        me->apr_handle,
                        pAprPacket->dst_addr,
                        pAprPacket->dst_port,
                        pAprPacket->src_addr,
                        pAprPacket->src_port,
                        pAprPacket->token,
                        VOICE_RSP_GET_PARAM_ACK,
                        pPayload->param_max_size + sizeof(voice_get_param_ack_t),
                        &pAprInBandPacket
                        );
                  if ( rc || NULL == pAprInBandPacket)
                  {
                     MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Failed to create APR packet for GetParamAck in VPM 0x%8x ",(int)rc);
                     rc = elite_apr_if_free( me->apr_handle, pAprPacket);
                     return ADSP_EFAILED;
                  }
                  // overwrite the get param apr packet with the response pkt in the async structure tracking
                  async_struct->async_status[nFreeIndex].apr_packet_ptr = pAprInBandPacket;

                  pGetParam = (int32_t*)elite_apr_if_get_payload_ptr(pAprInBandPacket);
                  // Add 4 bytes for result in get_param_ack
                  pGetParam = (int32_t*)(((int8_t*)pGetParam) + sizeof(voice_get_param_ack_t));
                  // Copy over mod id/param id/max size
                  memscpy (pGetParam,pPayload->param_max_size, (int32_t *)(&pPayload->module_id), (sizeof(voice_param_data_t)));

                  // free the apr getparam pkt and use the apr getparam_ack pkt
                  rc = elite_apr_if_free( me->apr_handle, pAprPacket);
               }
               // out of band processing
               else
               {
                  // Get virtual address
                  elite_mem_shared_memory_map_t memmap = {0};
                  memmap.unMemMapClient = vPM_memory_map_client;
                  memmap.unMemMapHandle = pPayload->mem_map_handle;
                  result = elite_mem_map_get_shm_attrib(pPayload->payload_address_lsw,
                        pPayload->payload_address_msw,
                        pPayload->param_max_size,
                        &memmap);
                  if( ADSP_FAILED( result ))
                  {
                     MSG_4(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VPM get param Error lsw(%lx) msw(%lx) size(%lx) handle(%#x)",pPayload->payload_address_lsw, pPayload->payload_address_msw, (uint32_t)pPayload->param_max_size,(int)memmap.unMemMapHandle);
                     MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Error! Failed to get virtual address with result %d!", result);
                     // sending out of band result
                     voice_get_param_ack_t get_param_ack;
                     get_param_ack.status = APR_EBADPARAM;
                     elite_apr_if_alloc_send_ack(me->apr_handle,
                           pAprPacket->dst_addr,
                           pAprPacket->dst_port,
                           pAprPacket->src_addr,
                           pAprPacket->src_port,
                           pAprPacket->token,
                           VOICE_RSP_GET_PARAM_ACK,
                           &get_param_ack,
                           sizeof(voice_get_param_ack_t)
                           );
                     elite_apr_if_free( me->apr_handle, pAprPacket);
                     return result;
                  }
                  pGetParam = (int32_t*)memmap.unVirtAddr;
                  // Copy over data to this buffer
                  memscpy (pGetParam,pPayload->param_max_size, (int32_t *)(&pPayload->module_id), (sizeof(voice_param_data_t)));
               }

               // size of the FADD message
               nSize = sizeof(elite_msg_param_cal_t);

               if( me->session[nSessionIndex]->tx )
               {
                  //*************** Compose FADD Message *****************
                  result = elite_msg_create_msg( &sMsgTx,
                        &nSize,
                        ELITE_CMD_GET_PARAM,
                        me->vpm_respq,
                        VOICE_ASYNC_STUFF_TOKEN(nSessionIndex, nFreeIndex),
                        ADSP_EOK);
                  if( ADSP_FAILED( result ))
                  {
                     MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg for tx getparam cmd!!\n");
                     return result;
                  }

                  elite_msg_param_cal_t* pFaddPayload;
                  pFaddPayload = (elite_msg_param_cal_t*) sMsgTx.pPayload;
                  pFaddPayload->unParamId = ELITEMSG_PARAM_ID_CAL;
                  pFaddPayload->pnParamData = (int32_t*)pGetParam;
                  //dbg: MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Dispatch SetParams to Tx/Rx, Apr packet addr = 0x%x!!\n",(uint32_t)pAprPacket);
                  result = qurt_elite_queue_push_back(me->session[nSessionIndex]->tx->cmdQ, (uint64_t*)&sMsgTx);

                  if (ADSP_FAILED(result))
                  {
                     elite_msg_return_payload_buffer(&sMsgTx);
                     MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to set params on tx!!\n");
                  }
                  else
                  {
                     async_struct->async_status[nFreeIndex].state++;
                  }
               }

               if( me->session[nSessionIndex]->rx )
               {
                  //*************** Compose FADD Message *****************
                  result = elite_msg_create_msg( &sMsgRx,
                        &nSize,
                        ELITE_CMD_GET_PARAM,
                        me->vpm_respq,
                        VOICE_ASYNC_STUFF_TOKEN(nSessionIndex, nFreeIndex),
                        ADSP_EOK);
                  if( ADSP_FAILED( result ))
                  {
                     MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg for rx getparam cmd!!\n");
                     return result;
                  }

                  elite_msg_param_cal_t* pFaddPayload;
                  pFaddPayload = (elite_msg_param_cal_t*) sMsgRx.pPayload;
                  pFaddPayload->unParamId = ELITEMSG_PARAM_ID_CAL;
                  pFaddPayload->pnParamData = (int32_t*)pGetParam;

                  //dbg: MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Dispatch SetParams to Rx, Apr packet addr = 0x%x!!\n",(uint32_t)pAprPacket);
                  result = qurt_elite_queue_push_back(me->session[nSessionIndex]->rx->cmdQ, (uint64_t*)&sMsgRx);

                  if (ADSP_FAILED(result))
                  {
                     elite_msg_return_payload_buffer(&sMsgRx);
                     MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to set params on rx!!\n");
                  }
                  else
                  {
                     async_struct->async_status[nFreeIndex].state++;
                  }
               }
               /* if state == 0, nothing is pending.  Can finish command here */
               if( 0 == async_struct->async_status[nFreeIndex].state)
               {
                  if (NULL != pAprInBandPacket)
                  {
                     rc = elite_apr_if_end_cmd( me->apr_handle, pAprPacket, APR_EFAILED);
                  }
                  else
                  {
                     rc = elite_apr_if_end_cmd( me->apr_handle, pAprInBandPacket, APR_EFAILED);
                  }
                  voice_end_async_control( async_struct, nFreeIndex);
               }
               else
               {
                  me->wait_mask &= (~(me->cmd_mask));
               }
            }
            else
            {
               /* no free index to store asyncStatus, can end this command as EBUSY */
               rc = elite_apr_if_end_cmd( me->apr_handle, pAprPacket, APR_EBUSY);
            }
            break;
         }
      case VOICE_CMD_SHARED_MEM_MAP_REGIONS:
         {
            result = voice_shared_memory_map_regions_process(vPM_memory_map_client, me->apr_handle, pAprPacket);
            break;
         }
      case VOICE_CMD_SHARED_MEM_UNMAP_REGIONS:
         {
            result = elite_mem_shared_memory_un_map_regions_cmd_handler(vPM_memory_map_client, me->apr_handle, pAprPacket);
            //result = voice_shared_memory_un_map_regions_process(vPM_memory_map_client, me->apr_handle, pAprPacket);
            break;
         }
      case VOICE_CMD_SET_TIMING_PARAMS:
         {
            result = vpm_set_timing_params( me, pMsg);
            break;
         }
      case VPM_CMD_SET_TIMING_PARAMS:
         {
            result = vpm_set_timingv2_params( me, pMsg);
            break;
         }
      case VPM_CMD_SET_TIMING_PARAMS_V2:
         {
            result = vpm_set_timingv3_params( me, pMsg);
            break;
         }
      default:
         {
            /* Handle error. */
            if ( elite_apr_if_msg_type_is_cmd( pAprPacket) )
            {  /* Complete unsupported commands. */
               rc = elite_apr_if_end_cmd( me->apr_handle, pAprPacket, APR_EUNSUPPORTED );
            }
            else
            {  /* Drop unsupported events. */
               rc = elite_apr_if_free( me->apr_handle, pAprPacket );
            }
            result = ADSP_EUNSUPPORTED;
            break;
         }
   }
   return result;
}

static ADSPResult vpm_set_timingv2_params( void *pInstance, elite_msg_any_t *pMsg)
{
   elite_apr_packet_t * pAprPacket;
   VoiceProcMgr_t *me = (VoiceProcMgr_t*)pInstance;       // instance structure
   uint16_t handle;
   uint16_t nSessionIndex;
   uint32_t nFreeIndex;
   int32_t rc = APR_EOK;
   int32_t rcTx = APR_EOK;
   int32_t rcRx = APR_EOK;

   pAprPacket = (elite_apr_packet_t*) pMsg->pPayload;
   handle = elite_apr_if_get_dst_port( pAprPacket);
   nSessionIndex = voice_map_handle_to_session_index( handle);
   vpm_set_timing_params_t* pVfrModeCmd = (vpm_set_timing_params_t*) elite_apr_if_get_payload_ptr( pAprPacket);

   if( me->session[nSessionIndex]->state == VPM_RUN_STATE)
   {
      MSG(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: cannot change VFR in RUN state");

      rc = elite_apr_if_end_cmd( me->apr_handle, pAprPacket, APR_ENOTREADY );
   }
   else
   {
      rc = voice_init_async_control( &me->session[nSessionIndex]->async_struct, pAprPacket, &nFreeIndex);

      /* Store timing version. */
      me->session[nSessionIndex]->timing_cmd_version = VOICE_TIMING_VERSION_2;

      /* kick off Enc/Dec MT config. */
      if( ADSP_SUCCEEDED(rc))
      {
         if( me->session[nSessionIndex]->tx )
         {
            rcTx = vpm_send_vfr_mode( me, nSessionIndex, nFreeIndex, (int32_t *)pVfrModeCmd, VPTX, VOICEPROC_SET_TIMINGV2_CMD);

            me->session[nSessionIndex]->async_struct.async_status[nFreeIndex].result = rcTx;
         }

         if( me->session[nSessionIndex]->rx )
         {
            rcRx = vpm_send_vfr_mode( me, nSessionIndex, nFreeIndex, (int32_t *)pVfrModeCmd, VPRX, VOICEPROC_SET_TIMINGV2_CMD);

            me->session[nSessionIndex]->async_struct.async_status[nFreeIndex].result = rcRx;
         }
         /* check if any action attempt succeeded...if so need to handle an ack */
         if( (me->session[nSessionIndex]->rx && ADSP_SUCCEEDED(rcRx)) ||
               (me->session[nSessionIndex]->tx && ADSP_SUCCEEDED(rcTx)))
         {
            me->wait_mask &= (~(me->cmd_mask));
         }
         else
         {
            rc = elite_apr_if_end_cmd( me->apr_handle, pAprPacket, APR_EFAILED);
            voice_end_async_control( &me->session[nSessionIndex]->async_struct, nFreeIndex);
         }
      }
      else
      {
         rc = elite_apr_if_end_cmd( me->apr_handle, pAprPacket, APR_EBUSY);
         voice_end_async_control( &me->session[nSessionIndex]->async_struct, nFreeIndex);
      }
   }
   MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Sent VPM_CMD_SET_TIMING_PARAMS to dynamic svcs, VFR(0x%x), sessionState(0x%lx)", pVfrModeCmd->mode,me->session[nSessionIndex]->state );
   return ADSP_EOK;
}

static ADSPResult vpm_set_timing_params( void *pInstance, elite_msg_any_t *pMsg)
{
   elite_apr_packet_t * pAprPacket;
   VoiceProcMgr_t *me = (VoiceProcMgr_t*)pInstance;       // instance structure
   uint16_t handle;
   uint16_t nSessionIndex;
   uint32_t nFreeIndex;
   int32_t rc = APR_EOK;
   int32_t rcTx = APR_EOK;
   int32_t rcRx = APR_EOK;

   pAprPacket = (elite_apr_packet_t*) pMsg->pPayload;
   handle = elite_apr_if_get_dst_port( pAprPacket);
   nSessionIndex = voice_map_handle_to_session_index( handle);
   voice_set_timing_params_t  *pVfrModeCmd = (voice_set_timing_params_t *) elite_apr_if_get_payload_ptr( pAprPacket);

   //dbg: MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VOICE_CMD_SET_TIMING_PARAMS, VFR(0x%x), sessionState(0x%x)", pVfrModeCmd->mode,me->session[nSessionIndex]->state );

   //TODO:check if we want to store VFR type in VPM
   if( me->session[nSessionIndex]->state == VPM_RUN_STATE)
   {
      MSG(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: cannot change VFR in RUN state");

      rc = elite_apr_if_end_cmd( me->apr_handle, pAprPacket, APR_ENOTREADY );
   }
#if 0 // TODO: Need to enable this
   else if( me->session[nSessionIndex]->pending == TRUE)
   {
      rc = elite_apr_if_end_cmd( me->apr_handle, pAprPacket, APR_EBUSY);
   }
#endif
   else
   {
      rc = voice_init_async_control( &me->session[nSessionIndex]->async_struct, pAprPacket, &nFreeIndex);

      /* Store timing version. */
      me->session[nSessionIndex]->timing_cmd_version = VOICE_TIMING_VERSION_1;

      /* kick off Enc/Dec MT config. */
      if( ADSP_SUCCEEDED(rc))
      {
         if( me->session[nSessionIndex]->tx )
         {
            rcTx = vpm_send_vfr_mode( me, nSessionIndex, nFreeIndex, (int32_t *)pVfrModeCmd, VPTX,VOICEPROC_SET_TIMING_CMD);

            me->session[nSessionIndex]->async_struct.async_status[nFreeIndex].result = rcTx;
         }

         if( me->session[nSessionIndex]->rx )
         {
            rcRx = vpm_send_vfr_mode( me, nSessionIndex, nFreeIndex, (int32_t *)pVfrModeCmd, VPRX,VOICEPROC_SET_TIMING_CMD);

            me->session[nSessionIndex]->async_struct.async_status[nFreeIndex].result = rcRx;
         }
         /* check if any action attempt succeeded...if so need to handle an ack */
         if( (me->session[nSessionIndex]->rx && ADSP_SUCCEEDED(rcRx)) ||
               (me->session[nSessionIndex]->tx && ADSP_SUCCEEDED(rcTx)))
         {
            //            me->session[nSessionIndex]->pending = TRUE; //TODO: TO be enabled later
            me->wait_mask &= (~(me->cmd_mask));
         }
         else
         {
            rc = elite_apr_if_end_cmd( me->apr_handle, pAprPacket, APR_EFAILED);
            voice_end_async_control( &me->session[nSessionIndex]->async_struct, nFreeIndex);
         }
      }
      else
      {
         rc = elite_apr_if_end_cmd( me->apr_handle, pAprPacket, APR_EBUSY);
         voice_end_async_control( &me->session[nSessionIndex]->async_struct, nFreeIndex);
      }
   }
   MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Sent VOICE_CMD_SET_TIMING_PARAMS to dynamic svcs, VFR(0x%x), sessionState(0x%lx)", pVfrModeCmd->mode,me->session[nSessionIndex]->state );
   return ADSP_EOK;
}

static ADSPResult vpm_send_vfr_mode( void* pInstance, uint32_t nSessionIndex, uint32_t nFreeIndex, int32_t *pVFRCmd, uint32_t nDirection, uint32_t sec_opcode)
{
   ADSPResult result;
   elite_msg_any_t sMsg;
   elite_msg_custom_voc_timing_param_type *pPayload = NULL;
   uint32_t nSize = sizeof(elite_msg_custom_voc_timing_param_type);
   VoiceProcMgr_t *me = (VoiceProcMgr_t*)pInstance;       // instance structure

   me->session[nSessionIndex]->async_struct.async_status[nFreeIndex].state++;

   result = elite_msg_create_msg( &sMsg,
         &nSize,
         ELITE_CUSTOM_MSG,
         me->vpm_respq,
         VOICE_ASYNC_STUFF_TOKEN( nSessionIndex, nFreeIndex),
         ADSP_EOK);
   if( ADSP_FAILED( result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg!!\n");
      return result;
   }
   pPayload = (elite_msg_custom_voc_timing_param_type*) sMsg.pPayload;
   pPayload->param_data_ptr = (int32_t*)pVFRCmd;
   pPayload->sec_opcode = sec_opcode;

   result = qurt_elite_queue_push_back((((VPTX == nDirection) ? me->session[nSessionIndex]->tx->cmdQ : me->session[nSessionIndex]->rx->cmdQ)), (uint64_t*)&sMsg);

   if (ADSP_FAILED(result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to set VFR type direction(%lx) !!\n",nDirection);
      elite_msg_return_payload_buffer( &sMsg);
      return result;
   }

   return result;
}

static ADSPResult create_tx_mixer_input( int32_t nSessionIndex, uint32_t priority)
{
   elite_msg_any_t matrixConnectMsg;
   uint32_t nSize = sizeof(elite_msg_custom_vmx_cfg_inports_t);

   ADSPResult result = elite_msg_create_msg( &matrixConnectMsg,
         &nSize,
         ELITE_CUSTOM_MSG,
         me->vpm_respq,
         NULL,
         ADSP_EOK);
   if (ADSP_FAILED(result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg for tx mixer input create!!\n");
      return result;
   }
   elite_msg_custom_vmx_cfg_inports_t* inportspayload = (elite_msg_custom_vmx_cfg_inports_t*) matrixConnectMsg.pPayload;
   inportspayload->unSecOpCode = ELITEMSG_CUSTOM_VMX_CFG_INPUT_PORTS;
   inportspayload->cfgInPortsPayload.ack_ptr = &me->session[nSessionIndex]->mixer_in_port;
   inportspayload->cfgInPortsPayload.index = -1;
   inportspayload->cfgInPortsPayload.configuration = VMX_NEW_INPUT;
   inportspayload->cfgInPortsPayload.input_mapping_mask = me->session[nSessionIndex]->input_mapping_mask = 0;
   inportspayload->cfgInPortsPayload.priority = priority;

   if (ADSP_FAILED(result = qurt_elite_queue_push_back(txVoiceMatrix->cmdQ,(uint64_t *)&matrixConnectMsg )))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Unable to push to tx matrix cmdQ, error = %d!!\n", result);
      elite_msg_return_payload_buffer( &matrixConnectMsg);
      return(result);
   }
   result = elite_svc_wait_for_ack(&matrixConnectMsg);
   result = inportspayload->unResponseResult;
   elite_msg_return_payload_buffer( &matrixConnectMsg);
   if(ADSP_FAILED(result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: Unable to connect to tx matrix, error = %d!!\n", result);
      return result;
   }
   return ADSP_EOK;
}

static ADSPResult reconfigure_tx_mixer_input( int32_t nSessionIndex, uint32_t priority)
{
   elite_msg_any_t matrixConnectMsg;
   uint32_t nSize = sizeof(elite_msg_custom_vmx_cfg_inports_t);

   ADSPResult result = elite_msg_create_msg( &matrixConnectMsg,
         &nSize,
         ELITE_CUSTOM_MSG,
         me->vpm_respq,
         NULL,
         ADSP_EOK);
   if (ADSP_FAILED(result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg for tx mixer input reconfigure!!\n");
      return result;
   }
   elite_msg_custom_vmx_cfg_inports_t* inportspayload = (elite_msg_custom_vmx_cfg_inports_t*) matrixConnectMsg.pPayload;
   inportspayload->unSecOpCode = ELITEMSG_CUSTOM_VMX_CFG_INPUT_PORTS;
   inportspayload->cfgInPortsPayload.ack_ptr = &me->session[nSessionIndex]->mixer_in_port;
   inportspayload->cfgInPortsPayload.index = me->session[nSessionIndex]->mixer_in_port->inport_id;
   inportspayload->cfgInPortsPayload.configuration = VMX_RECONFIGURE_INPUT;
   inportspayload->cfgInPortsPayload.priority = priority;

   if (ADSP_FAILED(result = qurt_elite_queue_push_back(txVoiceMatrix->cmdQ,(uint64_t *)&matrixConnectMsg )))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Unable to push to tx matrix cmdQ, error = %d!!\n", result);
      elite_msg_return_payload_buffer( &matrixConnectMsg);
      return(result);
   }
   result = elite_svc_wait_for_ack(&matrixConnectMsg);
   result = inportspayload->unResponseResult;
   elite_msg_return_payload_buffer( &matrixConnectMsg);
   if(ADSP_FAILED(result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: Unable to connect to tx matrix, error = %d!!\n", result);
      return result;
   }
   return ADSP_EOK;
}

static ADSPResult reconfigure_rx_mixer_output( int32_t nSessionIndex, uint16_t samplingRate)
{
   elite_msg_any_t matrixConnectMsg;
   uint32_t nSize = sizeof(elite_msg_custom_vmx_cfg_outports_t);
   ADSPResult result;

   result = elite_msg_create_msg( &matrixConnectMsg,
         &nSize,
         ELITE_CUSTOM_MSG,
         me->vpm_respq,
         NULL,
         ADSP_EOK);
   if (ADSP_FAILED(result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg for rx mixer output reconfigure!!\n");
      return result;
   }
   elite_msg_custom_vmx_cfg_outports_t* outportspayload = (elite_msg_custom_vmx_cfg_outports_t*)matrixConnectMsg.pPayload;
   outportspayload->unSecOpCode = ELITEMSG_CUSTOM_VMX_CFG_OUTPUT_PORTS;
   outportspayload->cfgOutPortsPayload.ack_ptr = &me->session[nSessionIndex]->mixer_out_port;
   outportspayload->cfgOutPortsPayload.index = me->session[nSessionIndex]->mixer_out_port->outport_id;
   outportspayload->cfgOutPortsPayload.configuration = VMX_RECONFIGURE_OUTPUT;
   outportspayload->cfgOutPortsPayload.svc_handle_ptr = me->session[nSessionIndex]->rx;   //required?
   outportspayload->cfgOutPortsPayload.out_sample_rate = samplingRate / 1000;

   if (ADSP_FAILED(result = qurt_elite_queue_push_back(rxVoiceMatrix->cmdQ,(uint64_t *)&matrixConnectMsg )))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Unable to push to rx matrix cmdQ, error = %d!!\n", result);
      elite_msg_return_payload_buffer( &matrixConnectMsg);
      return(result);
   }

   result = elite_svc_wait_for_ack(&matrixConnectMsg);
   result = outportspayload->unResponseResult;
   elite_msg_return_payload_buffer( &matrixConnectMsg);
   if(ADSP_FAILED(result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: Unable to connect to rx matrix, error = %d!!\n", result);
      return result;
   }
   return ADSP_EOK;
}

static ADSPResult create_rx_mixer_output( int32_t nSessionIndex, uint16_t samplingRate)
{

   elite_msg_any_t matrixConnectMsg;
   uint32_t nSize = sizeof(elite_msg_custom_vmx_cfg_outports_t);
   ADSPResult result;

   result = elite_msg_create_msg( &matrixConnectMsg,
         &nSize,
         ELITE_CUSTOM_MSG,
         me->vpm_respq,
         NULL,
         ADSP_EOK);
   if (ADSP_FAILED(result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg for rx mixer output reconfigure!!\n");
      return result;
   }
   elite_msg_custom_vmx_cfg_outports_t* outportspayload = (elite_msg_custom_vmx_cfg_outports_t*)matrixConnectMsg.pPayload;
   outportspayload->unSecOpCode = ELITEMSG_CUSTOM_VMX_CFG_OUTPUT_PORTS;
   outportspayload->cfgOutPortsPayload.ack_ptr = &me->session[nSessionIndex]->mixer_out_port;
   outportspayload->cfgOutPortsPayload.index = -1;
   outportspayload->cfgOutPortsPayload.configuration = VMX_NEW_OUTPUT;
   outportspayload->cfgOutPortsPayload.num_output_bufs = 2;
   outportspayload->cfgOutPortsPayload.max_bufq_capacity = 4;
   outportspayload->cfgOutPortsPayload.output_mapping_mask = me->session[nSessionIndex]->output_mapping_mask = 0;
   outportspayload->cfgOutPortsPayload.svc_handle_ptr = me->session[nSessionIndex]->rx;
   outportspayload->cfgOutPortsPayload.out_sample_rate = samplingRate / 1000;

   if (ADSP_FAILED(result = qurt_elite_queue_push_back(rxVoiceMatrix->cmdQ,(uint64_t *)&matrixConnectMsg )))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Unable to push to rx matrix cmdQ, error = %d!!\n", result);
      elite_msg_return_payload_buffer( &matrixConnectMsg);
      return(result);
   }

   result = elite_svc_wait_for_ack(&matrixConnectMsg);
   result = outportspayload->unResponseResult;
   elite_msg_return_payload_buffer( &matrixConnectMsg);
   if(ADSP_FAILED(result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: Unable to connect to rx matrix, error = %d!!\n", result);
      return result;
   }
   return ADSP_EOK;
}
static ADSPResult close_tx_mixer_input(int32_t nSessionIndex)
{
   ADSPResult result=ADSP_EOK;
   uint32_t nSize;
   //Input port
   if(me->session[nSessionIndex]->mixer_in_port != 0)
   {
      elite_msg_any_t matrixConnectMsg;
      nSize = sizeof(elite_msg_custom_vmx_cfg_inports_t);
      result = elite_msg_create_msg( &matrixConnectMsg,
            &nSize,
            ELITE_CUSTOM_MSG,
            me->vpm_respq,
            NULL,
            ADSP_EOK);
      if (ADSP_FAILED(result))
      {
         MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg for tx mixer close !!\n");
         return result;
      }
      elite_msg_custom_vmx_cfg_inports_t* inportspayload = (elite_msg_custom_vmx_cfg_inports_t*) matrixConnectMsg.pPayload;
      inportspayload->unSecOpCode = ELITEMSG_CUSTOM_VMX_CFG_INPUT_PORTS;
      inportspayload->cfgInPortsPayload.ack_ptr = &me->session[nSessionIndex]->mixer_in_port;
      inportspayload->cfgInPortsPayload.index = me->session[nSessionIndex]->mixer_in_port->inport_id;
      inportspayload->cfgInPortsPayload.configuration = VMX_CLOSE_INPUT;

      if (ADSP_FAILED(result = qurt_elite_queue_push_back(txVoiceMatrix->cmdQ,(uint64_t *)&matrixConnectMsg )))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Unable to push to tx matrix cmdQ, error = %d!!\n", result);
         elite_msg_return_payload_buffer( &matrixConnectMsg);
         return(result);
      }
      result = elite_svc_wait_for_ack(&matrixConnectMsg);
      result = inportspayload->unResponseResult;
      elite_msg_return_payload_buffer( &matrixConnectMsg);
      if(ADSP_FAILED(result))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: Unable to destroy tx matrix port, error = %d!!\n", result);
         return result;
      }
   }
   return result;
}

static ADSPResult close_rx_mixer_output(int32_t nSessionIndex)
{
   ADSPResult result = ADSP_EOK;
   uint32_t nSize;
   //output port
   if(me->session[nSessionIndex]->mixer_out_port != NULL)
   {
      //create output port on tx path to connect to encoder
      elite_msg_any_t matrixConnectMsg;
      nSize = sizeof(elite_msg_custom_vmx_cfg_outports_t);
      result = elite_msg_create_msg( &matrixConnectMsg,
            &nSize,
            ELITE_CUSTOM_MSG,
            me->vpm_respq,
            NULL,
            ADSP_EOK);
      if (ADSP_FAILED(result))
      {
         MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg for rx mixer close !!\n");
         return result;
      }
      elite_msg_custom_vmx_cfg_outports_t* outportspayload = (elite_msg_custom_vmx_cfg_outports_t*)matrixConnectMsg.pPayload;
      outportspayload->unSecOpCode = ELITEMSG_CUSTOM_VMX_CFG_OUTPUT_PORTS;
      outportspayload->cfgOutPortsPayload.ack_ptr = &me->session[nSessionIndex]->mixer_out_port;
      outportspayload->cfgOutPortsPayload.index = me->session[nSessionIndex]->mixer_out_port->outport_id;
      outportspayload->cfgOutPortsPayload.configuration = VMX_CLOSE_OUTPUT;


      if (ADSP_FAILED(result = qurt_elite_queue_push_back(rxVoiceMatrix->cmdQ,(uint64_t *)&matrixConnectMsg )))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Unable to push to rx matrix cmdQ, error = %d!!\n", result);
         elite_msg_return_payload_buffer( &matrixConnectMsg);
         return(result);
      }

      result = elite_svc_wait_for_ack(&matrixConnectMsg);
      result = outportspayload->unResponseResult;
      elite_msg_return_payload_buffer( &matrixConnectMsg);
      if(ADSP_FAILED(result))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: Unable to connect to rx matrix, error = %d!!\n", result);
         return result;
      }
   }
   return result;
}

static ADSPResult voice_proc_connect_peer(uint32_t unSecOpCode, qurt_elite_queue_t *CmdQHandle, qurt_elite_queue_t *RespQHandle, elite_svc_handle_t *Peer)
{

   elite_msg_any_t msg;
   ADSPResult result = ADSP_EOK;
   uint32_t nSize;
   elite_msg_custom_voc_svc_connect_type *pVoiceDevSvcConnectPayload;

   nSize = sizeof(elite_msg_custom_voc_svc_connect_type);
   result = elite_msg_create_msg( &msg,
         &nSize,
         ELITE_CUSTOM_MSG,
         RespQHandle,
         NULL,
         ADSP_EOK);
   if( ADSP_FAILED( result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg for peer connect!!\n");
      return result;
   }
   pVoiceDevSvcConnectPayload = (elite_msg_custom_voc_svc_connect_type *) msg.pPayload;
   pVoiceDevSvcConnectPayload->sec_opcode = unSecOpCode;
   pVoiceDevSvcConnectPayload->svc_handle_ptr  = Peer;

   result = qurt_elite_queue_push_back(CmdQHandle , (uint64_t*)&msg );
   if (ADSP_FAILED(result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to connect services!!\n");
      elite_msg_return_payload_buffer( &msg);
      return result;
   }

   elite_svc_wait_for_ack( &msg);
   elite_msg_return_payload_buffer( &msg);

   return result;

}

static ADSPResult control_mixer_ports(void* pInstance, uint32_t nSessionIndex, uint32_t command)
{
   ADSPResult result = ADSP_EOK;
   VoiceProcMgr_t* me = (VoiceProcMgr_t*)pInstance;
   if(me->session[nSessionIndex]->mixer_in_port)
   {
      elite_msg_any_t mixerControlMsg;
      uint32_t nSize = sizeof(elite_msg_custom_vmx_stop_run_t);
      result = elite_msg_create_msg(&mixerControlMsg,
            &nSize,
            ELITE_CUSTOM_MSG,
            me->vpm_respq,
            NULL,
            ADSP_EOK);
      if (ADSP_FAILED(result))
      {
         MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg for control mixer ports !!\n");
         return result;
      }
      elite_msg_custom_stop_t *payload = (elite_msg_custom_stop_t*)mixerControlMsg.pPayload;
      payload->unSecOpCode = command;
      payload->unPortID = me->session[nSessionIndex]->mixer_in_port->inport_id;
      payload->unDirection = VMX_INPUT_DIRECTION;
      MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VPM session %ld stopping tx mixer input port %ld", nSessionIndex, payload->unPortID);
      if (ADSP_FAILED(result = qurt_elite_queue_push_back(txVoiceMatrix->cmdQ,(uint64_t *)&mixerControlMsg )))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Unable to push to tx matrix cmdQ, error = %d!!\n", result);
         elite_msg_return_payload_buffer(&mixerControlMsg);
         return(result);
      }
      result = elite_svc_wait_for_ack(&mixerControlMsg);
      result = payload->unResponseResult;
      elite_msg_return_payload_buffer( &mixerControlMsg);
      if(ADSP_FAILED(result))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: Unable to connect to tx matrix, error = %d!!\n", result);
         return result;
      }
   }
   // rx path - stopping output port
   if(me->session[nSessionIndex]->mixer_out_port)
   {
      elite_msg_any_t mixerControlMsg;
      uint32_t nSize = sizeof(elite_msg_custom_vmx_stop_run_t);
      result = elite_msg_create_msg(&mixerControlMsg,
            &nSize,
            ELITE_CUSTOM_MSG,
            me->vpm_respq,
            NULL,
            ADSP_EOK);
      if (ADSP_FAILED(result))
      {
         MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg for control mixer ports !!\n");
         return result;
      }
      elite_msg_custom_stop_t *payload = (elite_msg_custom_stop_t*)mixerControlMsg.pPayload;
      payload->unSecOpCode = command;
      payload->unPortID = me->session[nSessionIndex]->mixer_out_port->outport_id;
      payload->unDirection = VMX_OUTPUT_DIRECTION;
      MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VPM session %ld stopping rx mixer output port %ld", nSessionIndex, payload->unPortID);
      if (ADSP_FAILED(result = qurt_elite_queue_push_back(rxVoiceMatrix->cmdQ,(uint64_t *)&mixerControlMsg )))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Unable to push to rx matrix cmdQ, error = %d!!\n", result);
         elite_msg_return_payload_buffer(&mixerControlMsg);
         return(result);
      }
      result = elite_svc_wait_for_ack(&mixerControlMsg);
      result = payload->unResponseResult;
      elite_msg_return_payload_buffer( &mixerControlMsg);
      if(ADSP_FAILED(result))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: Unable to connect to rx matrix, error = %d!!\n", result);
         return result;
      }
   }
   return result;
}

static ADSPResult vpm_send_stop_tx_host_pcm( void* pInstance, uint32_t nSessionIndex, elite_apr_packet_t *pPacket)
{
   ADSPResult result;
   VoiceProcMgr_t* me = (VoiceProcMgr_t*)pInstance;
   elite_msg_any_t sMsgTx;
   elite_msg_custom_voc_config_host_pcm_type *pPayload = NULL;
   uint32_t nSize = sizeof(elite_msg_custom_voc_config_host_pcm_type);

   result = elite_msg_create_msg( &sMsgTx,
         &nSize,
         ELITE_CUSTOM_MSG,
         me->vpm_respq,
         NULL,
         ADSP_EOK);
   if( ADSP_FAILED( result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg!!\n");
      return result;
   }
   pPayload = (elite_msg_custom_voc_config_host_pcm_type*) sMsgTx.pPayload;
   pPayload->sec_opcode        = VOICEPROC_CONFIG_HOST_PCM;
   pPayload->apr_packet_ptr    = pPacket;

   result = qurt_elite_queue_push_back(me->session[nSessionIndex]->tx->cmdQ, (uint64_t*)&sMsgTx);

   if (ADSP_FAILED(result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to start Host PCM on tx!!\n");
      elite_msg_return_payload_buffer( &sMsgTx);
      return result;
   }
   result = elite_svc_wait_for_ack(&sMsgTx);
   result = pPayload->unResponseResult;
   elite_msg_return_payload_buffer( &sMsgTx);
   if(ADSP_FAILED(result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: FAILED to start Host PCM on tx!!, error = %d!!\n", result);
      return result;
   }

   return result;
}
static ADSPResult vpm_send_stop_rx_host_pcm( void* pInstance, uint32_t nSessionIndex, elite_apr_packet_t *pPacket)
{
   ADSPResult result;
   VoiceProcMgr_t* me = (VoiceProcMgr_t*)pInstance;
   elite_msg_any_t sMsgRx;
   elite_msg_custom_voc_config_host_pcm_type *pPayload = NULL;
   uint32_t nSize = sizeof(elite_msg_custom_voc_config_host_pcm_type);

   result = elite_msg_create_msg( &sMsgRx,
         &nSize,
         ELITE_CUSTOM_MSG,
         me->vpm_respq,
         NULL,
         ADSP_EOK);
   if( ADSP_FAILED( result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg!!\n");
      return result;
   }
   pPayload = (elite_msg_custom_voc_config_host_pcm_type*) sMsgRx.pPayload;
   pPayload->sec_opcode        = VOICEPROC_CONFIG_HOST_PCM;
   pPayload->apr_packet_ptr    = pPacket;

   result = qurt_elite_queue_push_back(me->session[nSessionIndex]->rx->cmdQ, (uint64_t*)&sMsgRx);

   if (ADSP_FAILED(result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to start Host PCM on rx!!\n");
      elite_msg_return_payload_buffer( &sMsgRx);
      return result;
   }
   result = elite_svc_wait_for_ack(&sMsgRx);
   result = pPayload->unResponseResult;
   elite_msg_return_payload_buffer( &sMsgRx);
   if(ADSP_FAILED(result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: FAILED to start Host PCM on rx!!, error = %d!!\n", result);
      return result;
   }
   return result;
}


static ADSPResult vpm_send_start_tx_host_pcm( void* pInstance, uint32_t nSessionIndex, elite_apr_packet_t *pPacket)
{

   ADSPResult result;
   VoiceProcMgr_t* me = (VoiceProcMgr_t*)pInstance;
   elite_msg_any_t sMsgTx;
   elite_msg_custom_voc_config_host_pcm_type *pPayload = NULL;
   uint32_t nSize = sizeof(elite_msg_custom_voc_config_host_pcm_type);

   result = elite_msg_create_msg( &sMsgTx,
         &nSize,
         ELITE_CUSTOM_MSG,
         me->vpm_respq,
         NULL,
         ADSP_EOK);
   if( ADSP_FAILED( result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg!!\n");
      return result;
   }
   pPayload = (elite_msg_custom_voc_config_host_pcm_type*) sMsgTx.pPayload;
   pPayload->sec_opcode        = VOICEPROC_CONFIG_HOST_PCM;
   pPayload->apr_handle        = me->apr_handle;
   pPayload->apr_packet_ptr    = pPacket;

   result = qurt_elite_queue_push_back(me->session[nSessionIndex]->tx->cmdQ, (uint64_t*)&sMsgTx);

   if (ADSP_FAILED(result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to start Host PCM on tx!!\n");
      elite_msg_return_payload_buffer( &sMsgTx);
      return result;
   }
   result = elite_svc_wait_for_ack(&sMsgTx);
   result = pPayload->unResponseResult;
   elite_msg_return_payload_buffer( &sMsgTx);
   if(ADSP_FAILED(result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: FAILED to start Host PCM on tx!!, error = %d!!\n", result);
      return result;
   }

   return result;
}
static ADSPResult vpm_send_start_rx_host_pcm( void* pInstance, uint32_t nSessionIndex, elite_apr_packet_t *pPacket)
{

   ADSPResult result;
   VoiceProcMgr_t* me = (VoiceProcMgr_t*)pInstance;
   elite_msg_any_t sMsgRx;
   elite_msg_custom_voc_config_host_pcm_type *pPayload = NULL;
   uint32_t nSize = sizeof(elite_msg_custom_voc_config_host_pcm_type);

   result = elite_msg_create_msg( &sMsgRx,
         &nSize,
         ELITE_CUSTOM_MSG,
         me->vpm_respq,
         NULL,
         ADSP_EOK);
   if( ADSP_FAILED( result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg!!\n");
      return result;
   }
   pPayload = (elite_msg_custom_voc_config_host_pcm_type*) sMsgRx.pPayload;
   pPayload->sec_opcode        = VOICEPROC_CONFIG_HOST_PCM;
   pPayload->apr_handle        = me->apr_handle;
   pPayload->apr_packet_ptr    = pPacket;
   result = qurt_elite_queue_push_back(me->session[nSessionIndex]->rx->cmdQ, (uint64_t*)&sMsgRx);

   if (ADSP_FAILED(result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to start Host PCM on rx!!\n");
      elite_msg_return_payload_buffer( &sMsgRx);
      return result;
   }
   result = elite_svc_wait_for_ack(&sMsgRx);
   result = pPayload->unResponseResult;
   elite_msg_return_payload_buffer( &sMsgRx);
   if(ADSP_FAILED(result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: FAILED to start Host PCM on rx!!, error = %d!!\n", result);
      return result;
   }

   return result;
}

static ADSPResult vpm_send_tx_mute( void* pInstance, uint32_t nSessionIndex, voice_set_soft_mute_v2_t *pCmd)
{
   ADSPResult result;
   VoiceProcMgr_t* me = (VoiceProcMgr_t*)pInstance;
   elite_msg_any_t sMsgTx;
   elite_msg_custom_voc_set_soft_mute_type *pPayload = NULL;
   uint32_t nSize = sizeof(elite_msg_custom_voc_set_soft_mute_type);

   if( VOICE_SET_MUTE_RX_ONLY == pCmd->direction)
   {
      return ADSP_EBADPARAM;
   }

   result = elite_msg_create_msg( &sMsgTx,
         &nSize,
         ELITE_CUSTOM_MSG,
         me->vpm_respq,
         NULL,
         ADSP_EOK);
   if( ADSP_FAILED( result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg!!\n");
      return result;
   }
   pPayload = (elite_msg_custom_voc_set_soft_mute_type*) sMsgTx.pPayload;
   pPayload->sec_opcode = VOICEPROC_SET_MUTE_CMD;
   pPayload->mute       = pCmd->mute;
   pPayload->ramp_duration       = pCmd->ramp_duration;

   result = qurt_elite_queue_push_back(me->session[nSessionIndex]->tx->cmdQ, (uint64_t*)&sMsgTx);

   if (ADSP_FAILED(result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to set mute on tx!!\n");
      elite_msg_return_payload_buffer( &sMsgTx);
      return result;
   }
   result = elite_svc_wait_for_ack(&sMsgTx);
   result = pPayload->unResponseResult;
   elite_msg_return_payload_buffer( &sMsgTx);
   if(ADSP_FAILED(result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: FAILED to set mute on tx!!, error = %d!!\n", result);
      return result;
   }

   return result;
}

static ADSPResult vpm_send_rx_mute( void* pInstance, uint32_t nSessionIndex, voice_set_soft_mute_v2_t *pCmd)
{

   ADSPResult result = ADSP_EOK;
   VoiceProcMgr_t* me = (VoiceProcMgr_t*)pInstance;
   elite_msg_any_t sMsgRx;
   elite_msg_custom_voc_set_soft_mute_type *pPayload = NULL;
   uint32_t nSize = sizeof(elite_msg_custom_voc_set_soft_mute_type);

   if( VOICE_SET_MUTE_TX_ONLY == pCmd->direction)
   {
      return ADSP_EBADPARAM;
   }

   result = elite_msg_create_msg( &sMsgRx,
         &nSize,
         ELITE_CUSTOM_MSG,
         me->vpm_respq,
         NULL,
         ADSP_EOK);
   if( ADSP_FAILED( result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg!!\n");
      return result;
   }
   pPayload = (elite_msg_custom_voc_set_soft_mute_type*) sMsgRx.pPayload;
   pPayload->sec_opcode = VOICEPROC_SET_MUTE_CMD;
   pPayload->mute       = pCmd->mute;
   pPayload->ramp_duration       = pCmd->ramp_duration;


   result = qurt_elite_queue_push_back(me->session[nSessionIndex]->rx->cmdQ, (uint64_t*)&sMsgRx);

   if (ADSP_FAILED(result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to set mute on rx!!\n");
      elite_msg_return_payload_buffer( &sMsgRx);
      return result;
   }
   result = elite_svc_wait_for_ack(&sMsgRx);
   result = pPayload->unResponseResult;
   elite_msg_return_payload_buffer( &sMsgRx);
   if(ADSP_FAILED(result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: FAILED to set mute on rx!!, error = %d!!\n", result);
      return result;
   }
   return result;
}

static ADSPResult is_pseudo_port(uint16_t port)
{
   return (IS_PSEUDO_PORT_AFE_ID(port));
}

static ADSPResult vpm_send_custom_voiceproc_run_msg(qurt_elite_queue_t *respQ, qurt_elite_queue_t *destQ, uint32_t sec_opcode,
      uint32_t client_token, bool_t wait_for_ack, void *data_ptr, void *far_src_drift_ptr, uint32 session_thread_clock_mhz)
{

   ADSPResult result=ADSP_EOK;
   uint32_t size = 0;
   elite_msg_any_t msg;
   elite_msg_custom_voiceproc_run_payload_t *payload_ptr;

   size = sizeof(elite_msg_custom_voiceproc_run_payload_t);
   result = elite_msg_create_msg( &msg,
         &size,
         ELITE_CUSTOM_MSG,
         ((TRUE == wait_for_ack) ? respQ : NULL),
         client_token,
         ADSP_EOK);
   if( ADSP_FAILED( result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to create EliteMsg custom vocproc run!!\n");
      return result;
   }
   payload_ptr = (elite_msg_custom_voiceproc_run_payload_t *) msg.pPayload;
   payload_ptr->unSecOpCode = sec_opcode;
   payload_ptr->afe_drift_ptr = data_ptr;
   payload_ptr->far_src_drift_ptr = far_src_drift_ptr;
   payload_ptr->session_thread_clock_mhz = session_thread_clock_mhz;

   result = qurt_elite_queue_push_back(destQ, (uint64*)&msg);
   if (ADSP_FAILED(result))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FAILED to connect services!!\n");
      elite_msg_return_payload_buffer( &msg);
      return result;
   }

   if (TRUE == wait_for_ack)
   {
      result = elite_svc_wait_for_ack(&msg);
      elite_msg_return_payload_buffer( &msg);
   }

   return result;
}

/* provide proper far reference port for Vptx considering the topology id and external mixing mode */
uint16_t vpm_get_ec_ref_port(vpm_create_session_v2_t* create_ptr)
{
   uint16_t rx_port = AFE_PORT_ID_INVALID;
   if( VPM_TX_EXT_MIX_MODE == create_ptr->ec_mode )
   {
      rx_port = (NO_INTERFACE == create_ptr->audio_ref_port) ? AFE_PORT_ID_INVALID : create_ptr->audio_ref_port;
   }
   else
   {
      rx_port = (NO_INTERFACE == create_ptr->rx_port) ? AFE_PORT_ID_INVALID : create_ptr->rx_port;
   }

   if (VPM_TX_NONE == create_ptr->tx_topology_id)
   {
      rx_port = AFE_PORT_ID_INVALID;
   }
   return rx_port;
}


/**
  Retrieves the mmpm clock plan.
  @param [in] pInstance - VPM instance
  @param [in] min_session_thread_mhz -- thread mips requirement
  for new session being put to run state.
  @param [out] mmpm_min_thread_mips -- min thread mips
  requirement considering all sessions
  @param [out] mmpm_total_mips -- total mips requirement
  considering all sessions
 */
static void vpm_set_mmpm_clocks(void *pInstance, uint32_t* session_thread_clock_mhz)
{
   ADSPResult result;
   VoiceProcMgr_t* me = (VoiceProcMgr_t*)pInstance;

   if (TRUE == me->req_clock)
   {
      // If all sessions are successfully transitioned to STOP state, then release MMPM requests.
      if (!me->session_run_mask)
      {
         MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VPM has no sessions running, releasing all MMPM clock requests");

         if( ADSP_FAILED( result = voice_mmpm_release( &me->mmpm )))
         {
            MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: Failed to release MMPM resources for VPM");
         }
         else
         {
            MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Released resources for VPM");

            // Reset MMPM Configuration settings
            me->mmpm_min_mips_per_thread = 0;
            me->mmpm_mips_request = 0;
         }
      }
      else
      {
         uint32_t mmpm_mips_request = VPM_MAX_CLK_TOTAL_MIPS;
         uint32_t mmpm_min_mips_per_thread = VOICE_PER_THREAD_MAXCLK;
         uint32_t num_sessions = 0;
         uint32_t max_clk_session = 0;
         uint32_t mid_clk_session = 0;
         uint32_t min_clk_session = 0;

         uint32_t session_run_mask = me->session_run_mask;
         uint32_t i=0;
         while (session_run_mask!=0)
         {
            i = voice_get_high_lsb (session_run_mask);
            session_run_mask = voice_clr_bit(session_run_mask, i);
            num_sessions++;
         }

         MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VPM has %d sessions running", (int)num_sessions);

         // Make a local copy to parse through active sessions (reuse same variable)
         session_run_mask = me->session_run_mask;

         do
         {
            // Get first running session.
            uint32_t index = voice_get_high_lsb (session_run_mask);

            if (VOICE_WB_SAMPLING_RATE != me->session[index]->session_config.tx_sampling_rate
                  && VPM_TX_NONE       == me->session[index]->session_config.tx_topology_id)
            {
               min_clk_session++;
            }
            else if ((( VPM_TX_DM_FLUENCEV5 == me->session[index]->session_config.tx_topology_id
                     || VPM_TX_CUSTOM_DM_ECNS_1 == me->session[index]->session_config.tx_topology_id
                     || VPM_TX_CUSTOM_DM_ECNS_2 == me->session[index]->session_config.tx_topology_id
                     || VPM_TX_CUSTOM_DM_ECNS_3 == me->session[index]->session_config.tx_topology_id
                     || VPM_TX_SM_FLUENCEV5 == me->session[index]->session_config.tx_topology_id
                     || VPM_TX_SM_ECNS_V2 == me->session[index]->session_config.tx_topology_id
                     || VPM_TX_CUSTOM_SM_ECNS_1 == me->session[index]->session_config.tx_topology_id
                     || VPM_TX_CUSTOM_SM_ECNS_2 == me->session[index]->session_config.tx_topology_id
                     || VPM_TX_CUSTOM_SM_ECNS_3 == me->session[index]->session_config.tx_topology_id
                     || VPM_TX_DM_FLUENCE   == me->session[index]->session_config.tx_topology_id
                     || VPM_TX_SINGLE_MIC_DYNAMIC_TOPOLOGY == me->session[index]->session_config.tx_topology_id
                     || VPM_TX_DUAL_MIC_DYNAMIC_TOPOLOGY == me->session[index]->session_config.tx_topology_id
                     || VPM_TX_DM_VPECNS == me->session[index]->session_config.tx_topology_id
                     || VPM_TX_SM_ECNS      == me->session[index]->session_config.tx_topology_id
                     || VPM_TX_DM_FLUENCEV5_BROADSIDE      == me->session[index]->session_config.tx_topology_id
/*TCT-NB Tianhongwei add for nxp voice solution 2016/01/04*/
#if defined(LVVE)
					 || VOICE_TOPOLOGY_LVVEFQ_TX_SM == me->session[index]->session_config.tx_topology_id
					 || VOICE_TOPOLOGY_LVVEFQ_TX_DM == me->session[index]->session_config.tx_topology_id
#endif
                     )
                  && VOICE_NB_SAMPLING_RATE == me->session[index]->session_config.tx_sampling_rate)
               || (VPM_TX_NONE        == me->session[index]->session_config.tx_topology_id
                  && VOICE_WB_SAMPLING_RATE == me->session[index]->session_config.tx_sampling_rate))
         {
            mid_clk_session++;
         }
         else
         {
            max_clk_session++;
         }

            // Clear index bit that has been tested.
            session_run_mask = voice_clr_bit(session_run_mask, index);

         } while (session_run_mask!=0);

         // Total MIPS request
         // TODO: This is an approximation based on worst case estimates and
         // does not account for module enablement/disablement.
         mmpm_mips_request = (max_clk_session * VPM_MAX_CLK_TOTAL_MIPS)
            + (mid_clk_session * VPM_MID_CLK_TOTAL_MIPS)
            + (min_clk_session * VPM_MIN_CLK_TOTAL_MIPS);

         // If only 1 session, then set clock in accordance with above rules.
         // Else, it'd default to max clock.
         if (1 == num_sessions)
         {
            // Minimum MIPS per thread required
            mmpm_min_mips_per_thread = (max_clk_session * VOICE_PER_THREAD_MAXCLK)
               + (mid_clk_session * VOICE_PER_THREAD_MIDCLK)
               + (min_clk_session * VOICE_PER_THREAD_MINCLK);
         }

         //If any client is the Second Application Domain (APPS), then assume
         // end application to be ADSP Test framework on Target.
         // Verify if this clock request is the same as before,
         // if it is, then no need to send one more request.
         if (me->mmpm_min_mips_per_thread != mmpm_min_mips_per_thread
               || me->mmpm_mips_request != mmpm_mips_request)
         {
            MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VPM requesting MIPS=%ld; mmpm_min_mips_per_thread=%d; sessions %ld", mmpm_mips_request, (int)mmpm_min_mips_per_thread, num_sessions );

            if (ADSP_FAILED(result = voice_mmpm_config( &me->mmpm,
                        mmpm_mips_request,
                        mmpm_min_mips_per_thread,
                        VOICE_DEFAULT_AXI_IB,
                        VOICE_DEFAULT_AXI_USAGE,
                        VOICE_DEFAULT_SLEEP_LATENCY_MICROSEC)))
            {
               MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: Failed to config MMPM for VPM");
            }
            else
            {
               // Update latest clock request settings
               me->mmpm_min_mips_per_thread = mmpm_min_mips_per_thread;
               me->mmpm_mips_request = mmpm_mips_request;
            }
         }
         else
         {
            MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VPM not requesting mips/clock (CVD will request OR Request not needed)");
         }

         // Return session clock setting
         *session_thread_clock_mhz = me->mmpm_min_mips_per_thread;
      }
   }
}

/* General command queue handler that only handles one command at a time */
static ADSPResult vpm_process_cmd_queue(void* pInstance, elite_svc_handle_t *pHandle,
      elite_svc_msg_handler_func pHandler[], uint32_t handler_table_size)
{
   elite_msg_any_t msg;
   ADSPResult result;

   // Remove for loop, only doing one command at a time
   //for (;;)
   {
      // Take next msg off the q
      result = qurt_elite_queue_pop_front(pHandle->cmdQ, (uint64_t*) &msg);

      // Process the msg
      if (ADSP_EOK == result)
      {
         if (msg.unOpCode >= handler_table_size)
         {
            if (ADSP_FAILED(result = elite_svc_unsupported(pInstance, &msg)))
            {
               // in case of errors, return them
               return result;
            }
         }

         // table lookup to call handling function, with FALSE to indicate processing of msg
         if (ADSP_FAILED(result = pHandler[msg.unOpCode](pInstance, &msg)))
         {
            // in case of errors, return them
            return result;
         }
         // successfully processed msg, check for additional msgs
      }
      /* This part not necessary since popping only once at a time. ENEEDMORE is actually an error in this situation
         else if (ADSP_ENEEDMORE == result)
         {
      // if there are no more msgs in the q, return successfully
      return ADSP_EOK;
      }
       */
   } //for (;;)
   return result;
}

static ADSPResult vpm_set_timingv3_params( void *pInstance, elite_msg_any_t *pMsg)
{
   elite_apr_packet_t * pAprPacket;
   VoiceProcMgr_t *me = (VoiceProcMgr_t*)pInstance;       // instance structure
   uint16_t handle;
   uint16_t nSessionIndex;
   uint32_t nFreeIndex;
   int32_t rc = APR_EOK;
   int32_t rcTx = APR_EOK;
   int32_t rcRx = APR_EOK;

   pAprPacket = (elite_apr_packet_t*) pMsg->pPayload;
   handle = elite_apr_if_get_dst_port( pAprPacket);
   nSessionIndex = voice_map_handle_to_session_index( handle);
   vpm_set_timing_params_v2_t* pVfrModeCmd = (vpm_set_timing_params_v2_t*) elite_apr_if_get_payload_ptr( pAprPacket);
   MSG_6(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VPM_CMD_SET_TIMING_PARAMS_V2, mode(%d), VSID(0x%lx), \
         vptx_start(%d), vptx_delivery(%d), vprx_delivery(%d),session_index(%x)",
         pVfrModeCmd->mode,pVfrModeCmd->vsid,pVfrModeCmd->vptx_start_offset,pVfrModeCmd->vptx_delivery_offset,
         pVfrModeCmd->vprx_delivery_offset,nSessionIndex);
   if( me->session[nSessionIndex]->state == VPM_RUN_STATE)
   {
      MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VPM_CMD_SET_TIMING_PARAMS_V2: cannot change timing in RUN state, session_index(%x)",nSessionIndex);
      rc = elite_apr_if_end_cmd( me->apr_handle, pAprPacket, APR_ENOTREADY );
   }
   else
   {
      rc = voice_init_async_control( &me->session[nSessionIndex]->async_struct, pAprPacket, &nFreeIndex);

      /* Store timing version. */
      me->session[nSessionIndex]->timing_cmd_version = VOICE_TIMING_VERSION_3;

      /* kick off Enc/Dec MT config. */
      if( ADSP_SUCCEEDED(rc))
      {
         if( me->session[nSessionIndex]->tx )
         {
            rcTx = vpm_send_vfr_mode( me, nSessionIndex, nFreeIndex, (int32_t *)pVfrModeCmd, VPTX, VOICEPROC_SET_TIMINGV3_CMD);

            me->session[nSessionIndex]->async_struct.async_status[nFreeIndex].result = rcTx;
         }

         if( me->session[nSessionIndex]->rx )
         {
            rcRx = vpm_send_vfr_mode( me, nSessionIndex, nFreeIndex, (int32_t *)pVfrModeCmd, VPRX, VOICEPROC_SET_TIMINGV3_CMD);

            me->session[nSessionIndex]->async_struct.async_status[nFreeIndex].result = rcRx;
         }
         /* check if any action attempt succeeded...if so need to handle an ack */
         if( (me->session[nSessionIndex]->rx && ADSP_SUCCEEDED(rcRx)) ||
               (me->session[nSessionIndex]->tx && ADSP_SUCCEEDED(rcTx)))
         {
            me->wait_mask &= (~(me->cmd_mask));
         }
         else
         {
            rc = elite_apr_if_end_cmd( me->apr_handle, pAprPacket, APR_EFAILED);
            voice_end_async_control( &me->session[nSessionIndex]->async_struct, nFreeIndex);
         }
      }
      else
      {
         rc = elite_apr_if_end_cmd( me->apr_handle, pAprPacket, APR_EBUSY);
         voice_end_async_control( &me->session[nSessionIndex]->async_struct, nFreeIndex);
      }
   }
   return ADSP_EOK;
}

static ADSPResult vpm_custom_msg(void* pInstance, elite_msg_any_t* pMsg)
{
   ADSPResult result = ADSP_EOK;
   elite_msg_custom_header_t *pCustom = (elite_msg_custom_header_t *) pMsg->pPayload;

   if( pCustom->unSecOpCode < VOICEDEV_NUM_MSGS)
   {
      MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VPM_Cmd, SecOpcode: %ld",pCustom->unSecOpCode);
      result = pHandlerCustomMsgs[pCustom->unSecOpCode](pInstance, pMsg);
   }
   else
   {
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Unsupported SecOpcode message ID 0x%8lx!!", pCustom->unSecOpCode);
      result = ADSP_EFAILED;
   }
   return result;
}

static ADSPResult vpm_set_shared_objs_handler(void* pInstance, elite_msg_any_t* pMsg)
{
   VoiceProcMgr_t *me = (VoiceProcMgr_t*)pInstance;       // instance structure

   elite_msg_custom_voice_shared_objects_type *pPayload = (elite_msg_custom_voice_shared_objects_type*)(pMsg->pPayload);

   me->appi_tx = pPayload->appi_tx;
   me->appi_rx = pPayload->appi_rx;
   me->so_done = TRUE;

   {
      ADSPResult result = elite_msg_release_msg(pMsg);
      if (ADSP_FAILED(result))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: voice_proc_custom Failed to release the voice shared objects message with %d", result);
      }
   }

   return ADSP_EOK;
}

static ADSPResult vpm_process_set_param_msg(void* instance_ptr, elite_msg_any_t* msg_ptr)
{
   ADSPResult result = ADSP_EOK;                            // general result value
   VoiceProcMgr_t *me = (VoiceProcMgr_t*)instance_ptr;       // instance structure

   uint32_t nFreeIndex;
   uint32_t nSessionIndex;
   async_struct_t *async_struct;
   elite_apr_packet_t* pAprPacket;

   elite_msg_any_payload_t *param_payload_ptr = (elite_msg_any_payload_t *) msg_ptr->pPayload;
   nFreeIndex    = VOICE_ASYNC_EXTRACT_INDEX(param_payload_ptr->unClientToken);   // extracting Index from packed ClientToken
   nSessionIndex   = VOICE_ASYNC_EXTRACT_SESSION(param_payload_ptr->unClientToken); // extracting SessionNum from packed ClientToken
   async_struct   = &(me->session[nSessionIndex]->async_struct);
   pAprPacket    = async_struct->async_status[nFreeIndex].apr_packet_ptr;

   MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: vpm response(set param) sessionIndex(%lx) index(%ld)",nSessionIndex,nFreeIndex );
   if (0 == --(async_struct->async_status[nFreeIndex].state))
   {
      ADSPResult local_result = APR_EOK;

      if ( ((ADSP_EOK == async_struct->async_status[nFreeIndex].result) && (ADSP_EOK == param_payload_ptr->unResponseResult || ADSP_EUNSUPPORTED == param_payload_ptr->unResponseResult))
            || ((ADSP_EOK == param_payload_ptr->unResponseResult) && (ADSP_EOK == async_struct->async_status[nFreeIndex].result || ADSP_EUNSUPPORTED == async_struct->async_status[nFreeIndex].result))
         )
      {  // if result was success from both tx and rx return success result
         local_result = APR_EOK;
      }
      else
      {  // return failure
         local_result = APR_EBADPARAM;
      }

      elite_msg_return_payload_buffer( msg_ptr );
      //dbg: MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Set Param FADD message cleared");
      (void) elite_apr_if_end_cmd( me->apr_handle, pAprPacket, local_result);// TODO: send proper error code
      MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Set Param ack pAprPacket(0x%p), apr result(0x%x)",pAprPacket, local_result);

      voice_end_async_control( async_struct, nFreeIndex);
      me->wait_mask |= me->cmd_mask;
   }
   else
   {
      MSG_3(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: vpm response(set param) sessionIndex(%lx) index(%ld) state(%ld)",nSessionIndex,nFreeIndex,async_struct->async_status[nFreeIndex].state );
      async_struct->async_status[nFreeIndex].result = param_payload_ptr->unResponseResult; // update the result if needed
      elite_msg_return_payload_buffer( msg_ptr );
   }
   return result;
}

