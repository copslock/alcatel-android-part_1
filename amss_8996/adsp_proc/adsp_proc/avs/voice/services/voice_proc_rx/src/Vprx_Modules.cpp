/*========================================================================

*//** @file Vprx_Modules.cpp

Copyright (c) 2011 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government, Diversion contrary to U.S. law prohibited.

This file contains the code for module handling within Voice Proc Rx(VPRX)
Dynamic service.
*//*====================================================================== */

/*========================================================================
Edit History

$Header: //source/qcom/qct/multimedia2/Audio/vocproc/VoiceDevRx/...

when       who     what, where, why
--------   ---     -------------------------------------------------------
21/10/11   ss       Created file.

========================================================================== */


/* =======================================================================
INCLUDE FILES FOR MODULE
========================================================================== */
#include "Vprx_Modules.h"
#include "Vprx_Svc.h"
#include "adsp_amdb_static.h"
#include "capi_v2_library_factory.h"

//thread stack for from config file
extern const unsigned int VPRX_STACK_SIZE;

/* -----------------------------------------------------------------------
** Function prototypes
** ----------------------------------------------------------------------- */
static ADSPResult vprx_handle_fwk_extension(vprx_t* pVprx,voice_capi_module_t* module_info);
capi_v2_err_t vprx_capi_v2_cb_func(void *context_ptr, capi_v2_event_id_t id, capi_v2_event_info_t *event_info_ptr);
static uint32_t vprx_non_capi_get_size(vprx_t* pVprx, voice_capi_module_t *module_info);
static ADSPResult vprx_non_capi_module_init(vprx_t* pVprx,voice_capi_module_t* module_info);
static void vprx_process_non_capi_module(vprx_t* pVprx,voice_capi_module_t *curr_module_ptr,capi_v2_stream_data_t *input[],capi_v2_stream_data_t *output[]);
static void vprx_get_kpps_non_capi_module(vprx_t* pVprx,voice_capi_module_t *curr_module_ptr);
static void vprx_get_delay_non_capi_module(vprx_t* pVprx,voice_capi_module_t *curr_module_ptr);
static ADSPResult vprx_reconfig_stream_io_buf(voice_capi_module_t* module_ptr);

static ADSPResult vprx_handle_fwk_extension(vprx_t* pVprx,voice_capi_module_t* module_info)
{
   ADSPResult result = ADSP_EOK;
   for (uint32_t i=0;i < module_info->num_extensions.num_extensions; i++)
   {
      switch (module_info->fwk_extn_ptr[i].id)
      {
         case FWK_EXTN_AVC_RVE_IMC_DESTINATION:
         {
            capi_v2_buf_t param_data_buf;
            capi_v2_port_info_t port_info = {FALSE,FALSE,0};
            avc_rve_imc_key_t key;
            capi_v2_err_t result_avc;

            key.comm_id = COMM_ID_AVC_RVE_IMC;
            key.dest_mod_id = VOICE_MODULE_RX_AVCRVE;//dest module_id
            key.src_mod_id = VOICE_MODULE_TX_AVCRVE;   //Src module_id
            key.session_num = pVprx->session.session_num;

            param_data_buf.actual_data_len = param_data_buf.max_data_len = sizeof(key);
            param_data_buf.data_ptr = (int8_t*)&key;

            result_avc = module_info->module_ptr->vtbl_ptr->set_param(module_info->module_ptr,PARAM_ID_AVC_RVE_IMC_KEY,&port_info,&param_data_buf);
            if(CAPI_V2_FAILED(result_avc))
            {
               MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: failed to send Key for AVC_RVE IMC, ModuleId(%lx), result(%lu), session (%lx)",module_info->module_id,result_avc,pVprx->session.session_num);
               return capi_v2_err_to_adsp_result(result_avc);
            }
            break;
         }
         case FWK_EXTN_AVC_RVE_TX_IMC_SOURCE:
         case FWK_EXTN_AVC_RVE_TX_IMC_DESTINATION:
         {
            capi_v2_buf_t param_data_buf;
            capi_v2_port_info_t port_info = {FALSE,FALSE,0};
            avc_rve_tx_imc_key_t key;
            capi_v2_err_t result_avc;

            key.comm_id = COMM_ID_AVC_RVE_TX_IMC;
            key.dest_mod_id = VOICE_MODULE_TX_AVCRVE;
            key.session_num = pVprx->session.session_num;

            param_data_buf.actual_data_len = param_data_buf.max_data_len = sizeof(key);
            param_data_buf.data_ptr = (int8_t*)&key;

            result_avc = module_info->module_ptr->vtbl_ptr->set_param(module_info->module_ptr,PARAM_ID_AVC_RVE_TX_IMC_KEY,&port_info,&param_data_buf);
            if(CAPI_V2_FAILED(result_avc))
            {
               MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: failed to send Key for AVCTx IMC, ModuleId(%lx), result(%lu), session (%lx)",module_info->module_id,result_avc,pVprx->session.session_num);
               return capi_v2_err_to_adsp_result(result_avc);
            }
            break;
         }
         default:
         {
            MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: ERROR !! un supported fwk extention : %lx", module_info->fwk_extn_ptr[i].id);
            break;
         }
      }
   }
   return result;
}

static void vprx_check_for_capi_module(voice_capi_module_t *module_info)
{
   switch(module_info->module_id)
   {
      case VOICE_MODULE_HPCM:
      {
         module_info->is_capi = FALSE;
         break;
      }
      default:
      {
         //default is capi.
         module_info->is_capi = TRUE;
         break;
      }
   }
}

static uint32_t vprx_non_capi_get_size(vprx_t* pVprx, voice_capi_module_t *module_info)
{
   //For non capi modules, config function need to be called before get size function.
   switch(module_info->module_id)
   {
      case VOICE_MODULE_HPCM:
      {
        module_info->is_in_place = TRUE;
        module_info->mem_size_allocated = 0;
        break;
      }
      default:
      {
         MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: get size for unknown non-capi moduleid(0x%lx) is called, session(0x%lx)",module_info->module_id, pVprx->session.session_num);
         break;
      }
   }

   return module_info->mem_size_allocated;
}

static ADSPResult vprx_non_capi_module_init(vprx_t* pVprx,voice_capi_module_t* module_info)
{
   ADSPResult result = ADSP_EOK;
   switch(module_info->module_id)
   {
      case VOICE_MODULE_HPCM:
      {
         break;
      }
      default:
      {
         MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: get size for unknown non-capi moduleid(0x%lx) is called, session(0x%lx)",module_info->module_id, pVprx->session.session_num);
         break;
      }
   }
   return result;
}
/*
 * This function queries modules to identify memory requirements and
 * allocates memory accordingly.
 */
ADSPResult vprx_allocate_mem(vprx_t* pVprx, uint32_t nInCircBufSamples, uint32_t nOutCircBufSamples)
{
   //dbg: MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: vprx_allocate_mem begin session(%x)",pVprx->session.session_num);
   ADSPResult nResult     = ADSP_EOK;
   int32_t  nTotalSamples, nTotalSize;
   uint32_t vprx_proc_buf_size=0;
   int8_t i;
   capi_v2_err_t capi_result = CAPI_V2_EOK;
   uint32_t ss_size;

   //Stack variables
   uint32_t max_module_stack_needed = 0, stack_size = 0;

   // Used for querying/setting memory requirements of
   // shared object. Currently, two size determining factors
   // are used, hence the array size of 2.
   appi_prop_t prop[2];
   appi_proplist_t proplist;
   appi_format_t in_format,out_format;

   vprx_proc_buf_size = nInCircBufSamples; // Rx voice processing buffer of size 20ms + 1ms for SS.
   nTotalSamples      = nInCircBufSamples + nOutCircBufSamples + (vprx_proc_buf_size<<1); // Required Samples + Rx voice processing buffer size of 20ms
   nTotalSize         = (nTotalSamples << 1);

   voice_ss_get_size(&ss_size);
   ss_size = VOICE_ROUNDTO8(ss_size);

   if (VPM_RX_DYNAMIC_TOPOLOGY == pVprx->session.topology_id)
   {
      //for these topologies use existing stack
      stack_size = VPRX_STACK_SIZE;
      if (pVprx->so.getsize_fptr)
      {
         // Send down three properties to Shared object
         proplist.prop_ptr = &prop[0];
         proplist.props_num = 2; // Input media type and input frame size that determine the memory allocation.

         prop[0].id=APPI_PROP_ID_DATA_FORMAT;
         prop[0].size=sizeof(in_format);
         prop[0].info_ptr=(int8_t*)&in_format;

         int32_t input_frame_size = pVprx->io.input.frame_samples << 1;
         prop[1].id=APPI_PROP_ID_FRAME_SIZE;
         prop[1].size=sizeof(uint32_t);
         prop[1].info_ptr=(int8_t*)&input_frame_size; // bytes

         // Current input media type (ignore channel mapping)
         in_format.num_channels = pVprx->io.input.mediatype.num_channels;
         in_format.bits_per_sample = pVprx->io.input.mediatype.bits_per_sample;
         in_format.sampling_rate = pVprx->io.input.mediatype.sample_rate;
         in_format.data_is_signed = pVprx->io.input.mediatype.is_signed;
         in_format.data_is_interleaved = pVprx->io.input.mediatype.is_interleaved;
         memset(&in_format.channel_type, 0, sizeof(in_format.channel_type));

         // Get memory requirements for algorithms in shared object.
         uint32_t so_size=0;
         (void) pVprx->so.getsize_fptr(&proplist, &so_size);
         MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: SO Gain getsize returns %lu, session (%lx)!", so_size, pVprx->session.session_num);

         //only one module in the topology
         pVprx->memory.module_size[0] = pVprx->modules.modules_array[0].mem_size_allocated = VOICE_ROUNDTO8(so_size);

         //only one module capi is allocated
         pVprx->modules.modules_array[0].is_in_place = TRUE;
         pVprx->modules.modules_array[0].is_capi = FALSE;
         pVprx->modules.modules_array[0].module_id = 0xFFFFFFFF;
         pVprx->modules.modules_array[0].module_index = 0;
      }
   }
   else
   {
      // For all other topologies including none topology
      // Iterate over array and do these things:
      // 1. Update internal stuff, like module id etc.
      // 2. Update if it's not a CAPI for special handling in later stages

         if(FALSE == pVprx->reconfig_state.retain_amdb_handle)
         {
            pVprx->modules.handle_info_ptr = (adsp_amdb_module_handle_info_t*)qurt_elite_memory_malloc(sizeof(adsp_amdb_module_handle_info_t)*pVprx->modules.num_modules, QURT_ELITE_HEAP_DEFAULT);
            if (NULL == pVprx->modules.handle_info_ptr)
            {
               MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: Failed to allocate memory for the module info structure for AMDB.");
               return ADSP_ENOMEMORY;
            }

            for (i = 0 ; i < (int32_t)pVprx->modules.num_modules; i++)
            {
               //for last module update module id with soft mute
               if(i == (int32_t)(pVprx->modules.num_modules-1))
               {
                  pVprx->modules.modules_array[i].module_id = VOICE_MODULE_SOFT_MUTE;
               }
               else
               {
                  pVprx->modules.modules_array[i].module_id = pVprx->modules.modules_list[i].module_id;
               }
               pVprx->modules.modules_array[i].module_index = i; //for backtracing array if needed
            }

            for (i = 0 ; i < (int32_t)pVprx->modules.num_modules; i++)
            {
               // Fill the structure to query the handles from AMDB
               pVprx->modules.handle_info_ptr[i].interface_type = CAPI_V2;
               pVprx->modules.handle_info_ptr[i].type = AMDB_MODULE_TYPE_GENERIC;
               pVprx->modules.handle_info_ptr[i].id1 = pVprx->modules.modules_array[i].module_id;
               pVprx->modules.handle_info_ptr[i].id2 = 0;
               pVprx->modules.handle_info_ptr[i].h.capi_v2_handle = NULL;
               pVprx->modules.handle_info_ptr[i].result = ADSP_EFAILED;
            }

            /*
             * Note: This call will block till all modules with 'preload = 0' are loaded by the AMDB. This loading
             * happens using a thread pool using threads of very low priority. This can cause the current thread
             * to be blocked because of a low priority thread. If this is not desired, a callback function
             * should be provided that can be used by the AMDB to signal when the modules are loaded. The current
             * thread can then handle other tasks in parallel.
             */
            adsp_amdb_get_modules_request(pVprx->modules.handle_info_ptr, pVprx->modules.num_modules, NULL, NULL);
         }

      voice_capi_module_t* curr_module_ptr = pVprx->modules.modules_array;

      for(i = 0; i < (int32_t)pVprx->modules.num_modules; i++)
      {
         curr_module_ptr->svc_ptr = pVprx;
         vprx_check_for_capi_module(curr_module_ptr);

         if(TRUE == curr_module_ptr->is_capi)
         {
            // populate function pointers from AMDB

            if(FALSE == pVprx->reconfig_state.retain_amdb_handle)
            {
               nResult = pVprx->modules.handle_info_ptr[i].result;
               if(ADSP_FAILED(nResult))
               {
                  MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: failed to get AMDB handle for moduleId(0x%lx), session(0x%lx)",curr_module_ptr->module_id,pVprx->session.session_num);
                  return nResult;
               }

               if ((CAPI_V2 != pVprx->modules.handle_info_ptr[i].interface_type)&&(STUB != pVprx->modules.handle_info_ptr[i].interface_type))
               {
                  MSG_4(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: AMDB handle for moduleId(0x%lx), session(0x%lx) has type %d, when expected %d.",curr_module_ptr->module_id,pVprx->session.session_num, (int)pVprx->modules.handle_info_ptr[i].interface_type, (int)CAPI_V2);
                  return ADSP_EUNSUPPORTED;
               }
               if (STUB != pVprx->modules.handle_info_ptr[i].interface_type)
               {
            	   //dbg MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: AMDB handle for moduleId(0x%lx) = (0x%p)",curr_module_ptr->module_id,pVprx->modules.handle_info_ptr[i].h.capi_v2_handle);
            	   curr_module_ptr->amdb_handle_ptr = (void*)pVprx->modules.handle_info_ptr[i].h.capi_v2_handle;
               }
               else
               {
            	   curr_module_ptr->is_virtual_stub = TRUE;
                   MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VPRX moduleId(0x%lx) is virtually stubbed  session(0x%lx)",curr_module_ptr->module_id,pVprx->session.session_num);
               }
            }

            if(!curr_module_ptr->is_virtual_stub)
            {
               // get static props and the module memory
               pVprx->memory.module_size[i] = voice_capi_get_static_prop(curr_module_ptr);
            }

            //check max stack size
            if(curr_module_ptr->stack_size > max_module_stack_needed)
            {
               max_module_stack_needed = curr_module_ptr->stack_size;
            }
         }
         else
         {
            pVprx->memory.module_size[i] = vprx_non_capi_get_size(pVprx, curr_module_ptr);
         }

         curr_module_ptr->media_format = VOICE_CAPI_MEDIA_FMT_PRE_EC;
         curr_module_ptr++;
      }
   }

   //add framework stack
   max_module_stack_needed += VPRX_SELF_STACK_IN_BYTES;
   if(0 == stack_size)
   {
      stack_size = max_module_stack_needed>VPRX_MIN_STACK_IN_BYTES? max_module_stack_needed:VPRX_MIN_STACK_IN_BYTES;
   }

   // Update stack size if zero (indicating dynamic topologies did not update it)
   MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: Vprx max module stack(%lu), stack needed(%lu), current stack(%lu)", max_module_stack_needed, stack_size, pVprx->session.stack_size);

   if( stack_size != pVprx->session.stack_size )
   {
      pVprx->session.stack_changed = TRUE;
      pVprx->session.stack_size = stack_size;
   }


   // Add size of (AIG + DRC + FIR + EC + PBE + HPF + MBDRC + SS + (GUARD_BAND_SIZE x VOICE_RX_NO_OF_MODULES) ) to nTotalSize
   for(i = 0; i < (int8_t)pVprx->modules.num_modules; i++)
   {
      nTotalSize             +=   voice_add_guard_band(pVprx->memory.module_size[i]);
   }

   nTotalSize+=ss_size;

   pVprx->memory.pStartAddr = (int8_t *)qurt_elite_memory_malloc(nTotalSize, QURT_ELITE_HEAP_DEFAULT);

   if (NULL == pVprx->memory.pStartAddr)
   {
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Failed to allocate memory for VPRX modules!! session(%lx)",pVprx->session.session_num);
      return ADSP_ENOMEMORY;
   }

   memset( pVprx->memory.pStartAddr, 0, nTotalSize); // clear the memory before use

   pVprx->memory.nSize = nTotalSize; // size allocated
   pVprx->memory.pUsageAddr = pVprx->memory.pStartAddr; // Usage level pointer

   pVprx->io.input.circ_buf_ptr = pVprx->memory.pUsageAddr;
   pVprx->memory.pUsageAddr += (nInCircBufSamples << 1); // UsageAddr is a byte pointer
   pVprx->memory.pUsageAddr = (int8_t*)ROUNDTO8(pVprx->memory.pUsageAddr);

   pVprx->io.output.circ_buf_ptr = pVprx->memory.pUsageAddr;
   pVprx->memory.pUsageAddr += (nOutCircBufSamples << 1);  // UsageAddr is a byte pointer
   pVprx->memory.pUsageAddr = (int8_t*)ROUNDTO8(pVprx->memory.pUsageAddr);

   pVprx->io.proc_buf_ptr = pVprx->memory.pUsageAddr;
   pVprx->memory.pUsageAddr +=(vprx_proc_buf_size << 1);
   pVprx->memory.pUsageAddr = (int8_t*)ROUNDTO8(pVprx->memory.pUsageAddr);

   pVprx->io.proc_buf_ptr_sec = pVprx->memory.pUsageAddr;
   pVprx->memory.pUsageAddr +=(vprx_proc_buf_size << 1);
   pVprx->memory.pUsageAddr = (int8_t*)ROUNDTO8(pVprx->memory.pUsageAddr);

   // 1. Populate Guard band memory locations allocated after each module and update in guard band pointers array.
   // 2. Initialize guard band memory with known value.
   nResult = voice_init_guard_band( (int8**)pVprx->memory.guard_band_ptrs, &(pVprx->memory.module_size[0]), (int16_t)pVprx->modules.num_modules, pVprx->memory.pUsageAddr);
   if ( ADSP_EOK != nResult )
   {
      return nResult; // Returning error because voice_init_guard_band() is called with NULL pointer
   }

   // populate init prop list for capi modules
   capi_v2_proplist_t init_props_list;
   voice_capi_data_format_struct_t data_format_struct;

   // populate data buffer for media type
   voice_capi_populate_data_format_struct(&data_format_struct, &pVprx->io.input.mediatype);

   capi_v2_port_info_t port_info = {TRUE,TRUE,0};

   capi_v2_event_callback_info_t cb_info = { vprx_capi_v2_cb_func, NULL };
   capi_v2_heap_id_t heap_id = { (uint32_t)QURT_ELITE_HEAP_DEFAULT };
   capi_v2_port_num_info_t port_num = {1,1}; // populate these accordingly

   capi_v2_session_identifier_t sess_identifier;
   sess_identifier.service_id = (uint16_t)((ELITE_VOICEPROCRX_SVCID & 0xFFFF0000ul) >> 16);
   sess_identifier.session_id = pVprx->session.session_num;
   //MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: Vprx session identifier svc %d, session %d", sess_identifier.service_id, sess_identifier.session_id);

   //Set up default init prop list. The order of the prop should not be changed
   capi_v2_prop_t init_props[]= {
         { CAPI_V2_HEAP_ID,             { reinterpret_cast<int8_t *>(&heap_id), sizeof(heap_id),  sizeof(heap_id) }, port_info },
         { CAPI_V2_EVENT_CALLBACK_INFO, { reinterpret_cast<int8_t *>(&cb_info), sizeof(cb_info),  sizeof(cb_info) }, port_info },
         { CAPI_V2_PORT_NUM_INFO,       { reinterpret_cast<int8_t *>(&port_num), sizeof(port_num), sizeof(port_num) }, port_info },
         { CAPI_V2_INPUT_MEDIA_FORMAT,  { reinterpret_cast<int8_t *>(&data_format_struct), sizeof(data_format_struct), sizeof(data_format_struct) }, port_info },
         { CAPI_V2_SESSION_IDENTIFIER,  { reinterpret_cast<int8_t *>(&sess_identifier), sizeof(sess_identifier), sizeof(sess_identifier) }, port_info }
   };
   // setup init prop lists
   init_props_list.props_num = VOICE_SIZE_OF_ARRAY(init_props);
   init_props_list.prop_ptr = init_props;

   if (VPM_RX_DYNAMIC_TOPOLOGY == pVprx->session.topology_id)
   {
      // MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: SO init Usage addr = 0x%p", pVprx->memory.pUsageAddr);
      if (pVprx->so.init_fptr)
      {
         MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Begin initialization of algorithm in shared object, session (%lx)", pVprx->session.session_num);

         // Use the current memory usage pointer for the APPI module and invoke initialization
         pVprx->so.appi_ptr = (appi_t*)pVprx->memory.pUsageAddr;
         nResult = pVprx->so.init_fptr(pVprx->so.appi_ptr, &pVprx->so.is_inplace, &in_format, &out_format, &proplist);
         if (ADSP_FAILED(nResult))
         {
            MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Initialization of algorithm in shared object failed, %d, session (%lx)", nResult, pVprx->session.session_num);
            return nResult;
         }

         if (in_format.num_channels != out_format.num_channels
               || out_format.bits_per_sample != in_format.bits_per_sample
               || out_format.sampling_rate != in_format.sampling_rate
               || out_format.data_is_signed != in_format.data_is_signed
               || out_format.data_is_interleaved != in_format.data_is_interleaved)
         {
            MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Unsupported format type for Voice being requested by shared object, session (%lx)", pVprx->session.session_num);
            return ADSP_EUNSUPPORTED;
         }
      }
      pVprx->memory.pUsageAddr += voice_add_guard_band(pVprx->memory.module_size[0]);
   }
   else
   {
      voice_capi_module_t* curr_module_ptr = pVprx->modules.modules_array;
      for(i = 0; i < (int32_t)pVprx->modules.num_modules; i++)
      {
         //check media format to use and update internal init prop accordingly
         if(TRUE == curr_module_ptr->is_capi)
         {
            cb_info.event_context = curr_module_ptr;
            curr_module_ptr->module_ptr = (capi_v2_t*)pVprx->memory.pUsageAddr;

            // CAPI assumes modules are enabled by default, we can keep the same behavior here
            // If a module does not support the media fmt internally or disabled by default, it need to raise process state.
          
            if (!curr_module_ptr->is_virtual_stub)
            {
               curr_module_ptr->is_enabled = TRUE;
               capi_result = adsp_amdb_capi_v2_init_f((adsp_amdb_capi_v2_t*)curr_module_ptr->amdb_handle_ptr, curr_module_ptr->module_ptr, &init_props_list);

               if(CAPI_V2_EOK != capi_result)
               {
                  MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: Error! Module id(0x%lx) failed to init with error %ld. proceeding", curr_module_ptr->module_id, capi_result);
                  curr_module_ptr->module_ptr = NULL; //make it NULL so that later it is not used
                  curr_module_ptr->is_enabled = false;
               }
               else
               {
                  if(curr_module_ptr->fwk_extn_ptr)
                  {
                     nResult = vprx_handle_fwk_extension(pVprx,curr_module_ptr);
                     if(ADSP_FAILED(nResult))
                     {
                        MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: failed to handle fwk extn, session(%#lx)",pVprx->session.session_num);
                        return nResult;
                     }
                  }
               }
            }
            else
            {
                curr_module_ptr->is_enabled = FALSE;         // disabling enable flag for stubbed module
                curr_module_ptr->module_ptr = NULL;
            	MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: bypassing init for stubbed module (0x%lx), session(%#lx)",curr_module_ptr->module_id,pVprx->session.session_num);
            }
         }
         else
         {
            curr_module_ptr->is_enabled = TRUE;
            nResult = vprx_non_capi_module_init(pVprx, curr_module_ptr);
            if(ADSP_FAILED(nResult))
            {
               MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Error! Module id(0x%lx) failed to init with error %ld, Returning!", curr_module_ptr->module_id, nResult);
               return ADSP_EFAILED;
            }
         }

         // take care of pUsageAddr
         pVprx->memory.pUsageAddr += voice_add_guard_band(curr_module_ptr->mem_size_allocated);
         curr_module_ptr++;
      }
   }

   //SS init
   if(VOICE_MODULE_SIZE_ZERO != ss_size)
   {
      voice_ss_set_mem(&(pVprx->modules.ss_struct),pVprx->memory.pUsageAddr, ss_size);
   }
   pVprx->memory.pUsageAddr += ss_size;   // increment Sample Slip size

   // Check guard band for memory corruption
   nResult = voice_check_guard_band((int8**)pVprx->memory.guard_band_ptrs, (int16_t)pVprx->modules.num_modules, RX, pVprx->session.session_num);
   if ( ADSP_EOK != nResult )
   {
      return nResult; // returning error as corruption is detected in check_guard_band()
   }

   // Sanity check to see the allocated memory was enough
   if ((uint32_t)(pVprx->memory.pUsageAddr - pVprx->memory.pStartAddr) <= pVprx->memory.nSize)
   {
      nResult = ADSP_EOK;
   }
   else
   {
      MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Error allocating memory, result %d session(%lx)",nResult,pVprx->session.session_num);
      nResult = ADSP_ENOMEMORY;
      return nResult;
   }
   // TODO: To add other modules

   MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: vprx_allocate_mem end nResult=%x session(%x)",nResult,(int)pVprx->session.session_num);
   return nResult;
}

/*
 * This function frees up memory allocated for modules (Calls any end routines as required)
 */
ADSPResult vprx_allocate_mem_free(vprx_t* pVprx)
{
   if (pVprx->memory.pStartAddr)
   {
      qurt_elite_memory_free(pVprx->memory.pStartAddr);
      pVprx->memory.pStartAddr = NULL;
   }
   return ADSP_EOK;
}

/*
 * This function calls the end routines of modules
 */
void vprx_modules_end(vprx_t* pVprx)
{
   if (VPM_RX_DYNAMIC_TOPOLOGY == pVprx->session.topology_id)
   {
      // If Shared object is available, exercise it. If not, just by pass.
      if (pVprx->so.appi_ptr)
      {
         ADSPResult result = pVprx->so.appi_ptr->vtbl_ptr->end(pVprx->so.appi_ptr);

         if (ADSP_FAILED(result))
         {
            MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: End shared object failed with result=%d session (%lx)", result, pVprx->session.session_num);
         }
      }
   }
   else
   {
      // Check guard band for memory corruption
      voice_check_guard_band((int8_t**)pVprx->memory.guard_band_ptrs, (int16_t)pVprx->modules.num_modules, RX, pVprx->session.session_num);
      uint32_t temp_modid,temp_mod_index;
      bool_t temp_stub_info;
      void *temp_handle;

      voice_capi_module_t* curr_module_ptr = pVprx->modules.modules_array;
      for(uint32_t i=0; i < pVprx->modules.num_modules ; i++)
      {

         if(curr_module_ptr->is_capi)
         {
            if(curr_module_ptr->fwk_extn_ptr)
            {
               qurt_elite_memory_free(curr_module_ptr->fwk_extn_ptr);
               curr_module_ptr->fwk_extn_ptr = NULL;
            }
            if(curr_module_ptr->module_ptr)
            {
               curr_module_ptr->module_ptr->vtbl_ptr->end(curr_module_ptr->module_ptr);
            }
            curr_module_ptr->module_ptr = NULL;

            if(TRUE == pVprx->reconfig_state.retain_amdb_handle)
            {
               temp_modid = curr_module_ptr->module_id;
               temp_handle = curr_module_ptr->amdb_handle_ptr;
               temp_mod_index = curr_module_ptr->module_index;
               temp_stub_info = curr_module_ptr->is_virtual_stub;
               memset(curr_module_ptr,0,sizeof(voice_capi_module_t));
               curr_module_ptr->module_id = temp_modid;
               curr_module_ptr->amdb_handle_ptr = temp_handle ;
               curr_module_ptr->module_index = temp_mod_index ;
               curr_module_ptr->is_virtual_stub = temp_stub_info;
            }
            else
            {
               curr_module_ptr->amdb_handle_ptr = NULL;
            }

         }
         else
         {
            switch(curr_module_ptr->module_id)
            {
               case VOICE_MODULE_HPCM:
               {
                  break;
               }
               default:
               {
                  MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: module end(): Unknown non-capi module ModId(0x%lx)",curr_module_ptr->module_id);
                  break;
               }
            }
         }
         curr_module_ptr++;
      }
   }

   if ((NULL != pVprx->modules.handle_info_ptr) && (FALSE == pVprx->reconfig_state.retain_amdb_handle))
   {
      adsp_amdb_release_handles(pVprx->modules.handle_info_ptr, pVprx->modules.num_modules);
      qurt_elite_memory_free(pVprx->modules.handle_info_ptr);
      pVprx->modules.handle_info_ptr = NULL;
   }

   //clear special capi pointers and resampler state

   voice_ss_end(&(pVprx->modules.ss_struct));

   pVprx->io.proc_buf_ptr = NULL;
   pVprx->io.proc_buf_ptr_sec = NULL;
}

void vprx_modules_config(vprx_t* pVprx)
{
   voice_ss_config(&(pVprx->modules.ss_struct),pVprx->session.sampling_rate);
}

/*
 * This function initializes all the modules
 */
void vprx_modules_init(vprx_t* pVprx)
{
   // Call sample slip process for 10 msec data.
   int32_t frame_size_10msec = pVprx->io.input.frame_samples >> 1;
   voice_sample_slip_init(&(pVprx->modules.ss_struct),frame_size_10msec,VPRX_SS_MULTIFRAME);
}

/*
 * This function initializes all the modules with fresh calibration at the start of every call
 */
ADSPResult vprx_modules_full_init(vprx_t* pVprx)
{
   capi_v2_err_t capi_result=CAPI_V2_EOK;
   capi_v2_prop_t reset_prop[] = {
      { CAPI_V2_ALGORITHMIC_RESET, { NULL, 0, 0 }, {FALSE, FALSE, 0} },
   };
   capi_v2_proplist_t reset_prop_list = {sizeof(reset_prop)/sizeof(capi_v2_prop_t), reset_prop};

   if(VPM_RX_DYNAMIC_TOPOLOGY != pVprx->session.topology_id)
   {
      for(uint32_t i=0; i<pVprx->modules.num_modules;i++)
      {
         if((pVprx->modules.modules_array[i].is_capi)&& (!pVprx->modules.modules_array[i].is_virtual_stub))
         {
            //MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Issuing ALGO_RESET for module ID(%lu), session (%lx)",pVprx->modules.modules_array[i].module_id, pVprx->session.session_num);
            if(pVprx->modules.modules_array[i].module_ptr)
            {
               capi_result = pVprx->modules.modules_array[i].module_ptr->vtbl_ptr->set_properties(pVprx->modules.modules_array[i].module_ptr, &reset_prop_list);
            }
            else
            {
               capi_result = CAPI_V2_EUNSUPPORTED;
            }
            if(CAPI_V2_FAILED(capi_result))
            {
               MSG_4(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: ALGO_RESET failed for moduleId(%lx),module_ptr(%p),result(0x%lx) session (%lx)",
                     pVprx->modules.modules_array[i].module_id,pVprx->modules.modules_array[i].module_ptr,capi_result,pVprx->session.session_num);
            }
         }
      }
   }

   return ADSP_EOK;
}

static void vprx_do_sample_slip_stuff(vprx_t *pVprx, int16_t* pIn,int16_t adj_samp_num)
{
   uint16_t input_frame_size = 0;
   uint16_t offset_10msec = 0;
   uint16_t output_frame_size=0;
   uint16_t frame_size_10msec = VOICE_FRAME_SIZE_NB_10MS*pVprx->session.nb_sampling_rate_factor;

   //Hard coding number of 10 msec frames to 2.
   uint16_t num_10msec_frames = 2;

   //dbg: MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Voice SS Rx: adj_samp_num=%d,ss_multiframe_counter=%d",adj_samp_num,(int)pVprx->ratematch.ss_multiframe_counter);

   input_frame_size = frame_size_10msec;

   for(uint16_t i=0; i < num_10msec_frames;i++)
   {
      output_frame_size = frame_size_10msec - (adj_samp_num*i);
      offset_10msec = frame_size_10msec*i;
      //dbg: MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Voice SS Rx: voice_sample_slip_process-in_frame_size=%d,out_frame_size=%d,i=%d",input_frame_size,output_frame_size,i);

      (void)voice_sample_slip_process(&pVprx->modules.ss_struct,&pIn[offset_10msec],&pIn[offset_10msec],input_frame_size,output_frame_size);

      if(0 < (int8_t)pVprx->ratematch.ss_multiframe_counter)
      {
         pVprx->ratematch.ss_multiframe_counter -= 1;
      }
   }
}

static void vprx_process_non_capi_module(vprx_t* pVprx,voice_capi_module_t *curr_module_ptr,capi_v2_stream_data_t *input[],capi_v2_stream_data_t *output[])
{
   switch(curr_module_ptr->module_id)
   {
      case VOICE_MODULE_HPCM:
      { // HOST PCM processing
         int16_t *pHostPcmBuf[MAX_NUM_HOST_PCM_CHANNELS] = { (int16_t *) input[0]->buf_ptr[0].data_ptr, NULL, NULL, NULL};

         // no sampslip adjustments at this point, so hardcode to 0.  hardcode number of channels to mono for Rx
         // validate that we have the correct data size.  May be necessary during transition from host pcm stop to
         // start. TODO: where to place host PCM before EC.
         uint16_t num_host_pcm_samples_required = HOST_PCM_FRAME_SAMPLES*(pVprx->session.nb_sampling_rate_factor);

         if( num_host_pcm_samples_required ==  pVprx->io.min_process_frame_samples )
         {
            voice_host_pcm_process( &pVprx->modules.host_pcm_context, 1, pHostPcmBuf, pVprx->io.min_process_frame_samples, 0, TRUE);
         }
         break;
      }
      default:
      {
         MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: process() for unknown non-capi moduleid(0x%lx) is called, session(0x%lx)",curr_module_ptr->module_id, pVprx->session.session_num);
         break;
      }
   }
   return;
}

/*
 * This function calls the process functions of all the modules.
 */
ADSPResult vprx_modules_process(vprx_t* pVprx)
{
   int16_t *vprx_final_out_ptr = (int16_t*)pVprx->io.proc_buf_ptr;
   int16_t nSlipStuffSamples = 0;
   ADSPResult nResult = ADSP_EOK;
   int16_t proc_buf_size = ((FRAME_SAMPLES + DMA_SAMPLES) * pVprx->session.nb_sampling_rate_factor)<< 1;
   uint32_t i;

   voice_capi_module_t *curr_module_ptr = pVprx->modules.modules_array;
   //dbg: MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: vprx_modules_process begin session(%x)",pVprx->session.session_num);

   voice_circbuf_read(&(pVprx->io.input.circ_struct), (int8_t*)(pVprx->io.proc_buf_ptr), pVprx->io.min_process_frame_samples,proc_buf_size);

   /*
    * capi takes array of pointers of capi_v2_stream_data_t size.
    * stream_data_ptrs is a 2D array of pointers with each row correpsonding to 1D array of pointers.
    * each capi stores input and output buffer indices. And these indices point to proper array
    * of stream data pointers.
    */
   capi_v2_stream_data_t *stream_data_ptrs[VPRX_MAX_TEMP_BUFFERS][1] = {
         {&pVprx->input_output_stream_data[VPRX_TEMP_BUF_1]},
         {&pVprx->input_output_stream_data[VPRX_TEMP_BUF_2]}
   };

   pVprx->input_output_stream_data[VPRX_TEMP_BUF_1].buf_ptr[0].actual_data_len = ((pVprx->io.min_process_frame_samples) << 1);
   pVprx->input_output_stream_data[VPRX_TEMP_BUF_2].buf_ptr[0].actual_data_len = ((pVprx->io.min_process_frame_samples) << 1);

   if (VPM_RX_DYNAMIC_TOPOLOGY == pVprx->session.topology_id)
   {
      // If Shared object is available, exercise it. If not, just by pass.
      if (pVprx->so.appi_ptr)
      {
         uint32_t process_frame_samples=pVprx->io.min_process_frame_samples;
         appi_buflist_t inputlist, outputlist;
         appi_buf_t input;
         // Clear buffer object first
         memset(&input,0,sizeof(input));

         // Input list would contain deinterleaved near input content followed by
         // deinterleaved far end content (as of right now, only Mono far end content
         // is supported anyways).
         inputlist.buf_ptr = &input;
         inputlist.bufs_num = pVprx->io.input.mediatype.num_channels;

         outputlist.buf_ptr = &input;
         outputlist.bufs_num = pVprx->io.output.mediatype.num_channels;  // Only Mono as of now.

         // Add far end data at the end (**still using variable i**)
         // ***still using variable i***
         input.data_ptr = (int8_t*)(pVprx->io.proc_buf_ptr);
         input.actual_data_len = process_frame_samples<<1; // In bytes
         input.max_data_len = process_frame_samples<<1; // In bytes

         // Call processing routine.
         nResult = pVprx->so.appi_ptr->vtbl_ptr->process (pVprx->so.appi_ptr, &inputlist, &outputlist, NULL);

         // Verify data bounds and any failures, just print message if something fails.
         if (ADSP_FAILED(nResult) || (process_frame_samples<<1 != input.actual_data_len))
         {
            MSG_4(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Processing of shared object algorithm failed with nResult=%d; actual_data_len=%lu, max_data_len=%lu, session (%lx)",
                  nResult, input.actual_data_len, input.max_data_len, pVprx->session.session_num);
         }
      }
   }
   else
   {
      for( i=0; i<pVprx->modules.num_modules; i++)
      {

            if(curr_module_ptr->is_enabled)
            {
               if(curr_module_ptr->is_capi)
               {
#if defined(LVVE)/*TCT-NB Tianhongwei add for nxp voice solution 2016/01/04*/
                   if (curr_module_ptr->module_id == VOICE_MODULE_LVVEFQ_RX) {
#if defined(__qdsp6__) && !defined(SIM)
			          int8_t *bufptr[4] = { (int8_t *) (stream_data_ptrs[curr_module_ptr->input_buf_index][0]->buf_ptr[0].data_ptr), NULL, NULL, NULL };
			          uint32_t sampling_rate_log_id;
					  sampling_rate_log_id = voice_get_sampling_rate_log_id(pVprx->session.sampling_rate);
					  voice_log_buffer(  bufptr,
							VOICE_LOG_TAP_POINT_VPRX_IN,
							(sampling_rate_log_id << 3) | pVprx->session.session_num,
							qurt_elite_timer_get_time(),
							VOICE_LOG_DATA_FORMAT_PCM_MONO,
							pVprx->session.sampling_rate,
							pVprx->io.min_process_frame_samples << 1,
							NULL);
#endif
				 }
#endif/*TCT-NB Tianhongwei end 2016/01/04*/
                  curr_module_ptr->module_ptr->vtbl_ptr->process(curr_module_ptr->module_ptr,stream_data_ptrs[curr_module_ptr->input_buf_index],
                        stream_data_ptrs[curr_module_ptr->output_buf_index]);
#if defined(LVVE)/*TCT-NB Tianhongwei add for nxp voice solution 2016/01/04*/
					if (curr_module_ptr->module_id == VOICE_MODULE_LVVEFQ_RX) {
#if defined(__qdsp6__) && !defined(SIM)
						int8_t *bufptr[4] = { (int8_t *) (stream_data_ptrs[curr_module_ptr->output_buf_index][0]->buf_ptr[0].data_ptr), NULL, NULL, NULL };
						uint32_t sampling_rate_log_id;
						sampling_rate_log_id = voice_get_sampling_rate_log_id(pVprx->session.sampling_rate);
						voice_log_buffer(  bufptr,
						VOICE_LOG_TAP_POINT_VPRX_OUT,
						(sampling_rate_log_id << 3) | pVprx->session.session_num,
						qurt_elite_timer_get_time(),
						VOICE_LOG_DATA_FORMAT_PCM_MONO,
						pVprx->session.sampling_rate,
						pVprx->io.min_process_frame_samples << 1,
						NULL);
#endif
					}
#endif
/*TCT-NB Tianhongwei end 2016/01/04*/
               }
               else
               {
                  vprx_process_non_capi_module(pVprx,curr_module_ptr,stream_data_ptrs[curr_module_ptr->input_buf_index],
                        stream_data_ptrs[curr_module_ptr->output_buf_index]);
               }
            }
            else
            {
               // for virtual stub modules is_enabled is false and hence calling process is bypassed
               if( curr_module_ptr->input_buf_index != curr_module_ptr->output_buf_index )
               {
                  MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: vptx mod_id(0x%lx) is not enabled but has different io pointers. input_idx(%d), output_idx(%d)",
                        curr_module_ptr->module_id,curr_module_ptr->input_buf_index,curr_module_ptr->output_buf_index);
               }
            }

#if defined(LOG_VOICEPROC_DATA) && defined(SIM)
         {
            MSG_4(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: vprx session(%d), mod_idx(%d), input samples (%d)  output samples (%d)",
                  pVprx->session.session_num,curr_module_ptr->module_index,stream_data_ptrs[curr_module_ptr->input_buf_index][0]->buf_ptr[0].actual_data_len>>1,
                  stream_data_ptrs[curr_module_ptr->output_buf_index][0]->buf_ptr[0].actual_data_len>>1);
            {
               char buffer[100]; // The filename buffer.
               // Put output of ith module in ith file
               snprintf(buffer, sizeof(char) * 100, "vprx_session_%x_samp_rate_%d_module_%d_output.raw", pVprx->session.session_num,pVprx->session.sampling_rate,curr_module_ptr->module_index);
               FILE *fp;
               int16_t *pIn = (int16_t*)stream_data_ptrs[curr_module_ptr->output_buf_index][0]->buf_ptr->data_ptr;
               fp = fopen(buffer,"ab"); // in append mode
               fwrite(pIn,sizeof(char),stream_data_ptrs[curr_module_ptr->output_buf_index][0]->buf_ptr->actual_data_len,fp);
               fclose(fp);
            }
         }
#endif
         curr_module_ptr++;
      }
      curr_module_ptr--; //go back to last module in processing chain
   }

   //update the output of the chain with the last module's output buffer pointer. if the last module is ECNS then update with the primary output port pointer
   vprx_final_out_ptr = (int16_t*)stream_data_ptrs[curr_module_ptr->output_buf_index][0]->buf_ptr[0].data_ptr;

   vprx_cal_drift(pVprx); // accumulate drift info w.r.t av/hp timer
   vprx_cal_sample_slip_stuff(pVprx, &(nSlipStuffSamples));

   vprx_do_sample_slip_stuff(pVprx,vprx_final_out_ptr,nSlipStuffSamples);

   if(nSlipStuffSamples < 0)
   {
      MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Vprx !! Stuffed %d Samples !! session(%ld)",nSlipStuffSamples,pVprx->session.session_num);
   }
   else if (nSlipStuffSamples > 0)
   {
      MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Vprx !! Slipped %d Samples !! session(%ld)",nSlipStuffSamples,pVprx->session.session_num);
   }

   pVprx->io.output.buf_samples = pVprx->io.min_process_frame_samples - nSlipStuffSamples;
   //dbg: MSG_3(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: vprx_modules_process out samples (%d) session(%x) vfr mode(%x)",pVprx->io.output.buf_samples, pVprx->session.session_num,pVprx->session.vfr_mode);

   //Copy Data to output buffer (assuming inplace for now, so the output is in the in aLocal buffer)
   nResult = voice_circbuf_write(&(pVprx->io.output.circ_struct), (int8_t*)(vprx_final_out_ptr),pVprx->io.output.buf_samples);
   if (ADSP_FAILED(nResult))
   {
      MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: output circular buffer write failure (%d)  unread samples (%ld) ",nResult,pVprx->io.output.circ_struct.unread_samples);
   }

#if !defined(LVVE)/*TCT-NB Tianhongwei add for nxp voice solution 2016/01/04*/
#if defined(__qdsp6__) && !defined(SIM)

   int8_t *bufptr[4] = { (int8_t *) (vprx_final_out_ptr), NULL, NULL, NULL };
   uint32_t sampling_rate_log_id;
   sampling_rate_log_id = voice_get_sampling_rate_log_id(pVprx->session.sampling_rate);
   voice_log_buffer(  bufptr,
         VOICE_LOG_TAP_POINT_VPRX_OUT,
         (sampling_rate_log_id << 3) | pVprx->session.session_num,
         qurt_elite_timer_get_time(),
         VOICE_LOG_DATA_FORMAT_PCM_MONO,
         pVprx->session.sampling_rate,
         pVprx->io.output.buf_samples << 1,
         NULL);

#endif
#endif/*TCT-NB Tianhongwei end 2016/01/04*/

   //dbg: MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: vprx_modules_process end session(%x)",pVprx->session.session_num);
   return ADSP_EOK;
}

/*
 * This function iterates through the modules to set configuration parameters
 */
ADSPResult vprx_modules_set_param(vprx_t* pVprx, uint32_t payload_address, uint32_t payload_size)
{
   uint32_t byteSizeCounter=0;
   int8_t *pCalibrationDataPayLoad;
   voice_param_data_t *ptr;
   ptr = (voice_param_data_t *)payload_address;
   pCalibrationDataPayLoad = (int8_t *)ptr + sizeof(voice_param_data_t);
   ADSPResult nLocalResult = ADSP_EUNSUPPORTED, nAggregateResult = ADSP_EOK;

   if (VPM_RX_DYNAMIC_TOPOLOGY == pVprx->session.topology_id)
   {
      // If Shared object is available, exercise it. If not, just by pass.
      if (pVprx->so.appi_ptr)
      {
         do
         {
            ptr = (voice_param_data_t *)payload_address;
            appi_buf_t params_buf;
            params_buf.data_ptr = (int8_t*)payload_address;
            params_buf.actual_data_len = payload_size;
            params_buf.max_data_len = payload_size;

            if( (ptr->param_size & 0x00000003L) != 0 )
            {
               MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Error! Vprx received bad param size, mod %d, param %d, size %d!!", (int)ptr->module_id, (int)ptr->param_id, (int)ptr->param_size);
               nAggregateResult = ADSP_EBADPARAM;
               break;
            }

            nLocalResult = pVprx->so.appi_ptr->vtbl_ptr->set_param (pVprx->so.appi_ptr, APPI_PARAM_ID_MODULE_CAL, &params_buf);
            if (ADSP_FAILED(nLocalResult))
            {
               MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: set_param to shared object failed with result=%d; session(%lx)",
                     nLocalResult, pVprx->session.session_num);
            }

            if (ADSP_EBADPARAM == nLocalResult)
            {
               MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Vprx SetParam bailing!!!");
               nAggregateResult = ADSP_EBADPARAM;
            }
            else if(ADSP_FAILED(nLocalResult) && ADSP_EUNSUPPORTED != nLocalResult)
            {
               nAggregateResult = ADSP_EFAILED;
            }

            byteSizeCounter += (sizeof(voice_param_data_t) + ptr->param_size);
            MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: byteSizeCounter=%d; payload_size=%d",
                  (int)nLocalResult, (int)pVprx->session.session_num);
            payload_address += (sizeof(voice_param_data_t) + ptr->param_size);
         } while(byteSizeCounter < payload_size);
      }
      else
      {
         MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Shared object for vprx dynamic module 0x%lx does not exist - ignoring", ptr->module_id);
         nLocalResult = ADSP_EUNSUPPORTED;
      }
   }
   else
   {
      // Iterate through the entire payload size and copy all updated parameters
      do
      {
         ptr = (voice_param_data_t *)payload_address;
         pCalibrationDataPayLoad = (int8_t *)ptr + sizeof(voice_param_data_t);
         // check to make sure size is a multiple of 4. If not, stop calibrating
         if( (ptr->param_size & 0x00000003L) != 0 )
         {
            MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Error! Vprx received bad param size, mod %d, param %d, size %d!!", (int)ptr->module_id, (int)ptr->param_id, (int)ptr->param_size);
            nAggregateResult = ADSP_EBADPARAM;
            break;
         }

         nLocalResult = vprx_modules_set_param_int(pVprx, ptr->module_id, ptr->param_id, pCalibrationDataPayLoad, ptr->param_size);

         if (ADSP_EBADPARAM == nLocalResult)
         {
            MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Vprx SetParam bailing!!!");
            nAggregateResult = ADSP_EBADPARAM;
            break; // in case of any error dont go forward with parsing
         }
         else if(ADSP_FAILED(nLocalResult) && ADSP_EUNSUPPORTED != nLocalResult)
         {
            nAggregateResult = ADSP_EFAILED;
         }
         byteSizeCounter += (sizeof(voice_param_data_t) + ptr->param_size);
         payload_address += (sizeof(voice_param_data_t) + ptr->param_size);
         //dbg: MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Vprx SetParam addr(%#x), byte_size(%#x)", payload_address, byteSizeCounter);
         //dbg: MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Vprx received mod %d, param %d, size %d!!", (int)ptr->module_id, (int)ptr->param_id, (int)ptr->param_size);
      } while(byteSizeCounter < payload_size);
   }

   // Check guard band for memory corruption
   nAggregateResult = nAggregateResult | (voice_check_guard_band((int8_t**)pVprx->memory.guard_band_ptrs, (int16_t)pVprx->modules.num_modules, RX, pVprx->session.session_num));

   return nAggregateResult;

}

ADSPResult vprx_modules_set_param_int(vprx_t* pVprx, uint32_t module_id, uint32_t param_id, void* payload_address_in, uint32_t param_size )
{
   ADSPResult result = ADSP_EOK;
   capi_v2_err_t capi_result = CAPI_V2_EOK;
   int8_t* payload_address = (int8_t*)payload_address_in;

   // populate buffer to issue set param to capi modules
   capi_v2_buf_t param_data_buf;
   capi_v2_port_info_t port_info;
   port_info.is_valid = FALSE;
   param_data_buf.data_ptr = payload_address;
   param_data_buf.actual_data_len = param_data_buf.max_data_len = param_size;

   //MSG_4(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Vprx internal set param, mod id %lx, param id %lx, payload add %p, size %lu", module_id, param_id, payload_address_in, param_size);
   if(VOICEPROC_MODULE_TX == module_id) // listen for Tx module so it's triggered off a single module/param combo.  User sees as putting Tx in loopback
   {        /* handle loopback param */
      if( VOICE_PARAM_LOOPBACK_ENABLE == param_id)
      {
         int16_t nEnableFlag = *((int16_t*)payload_address);
         if (sizeof(int32_t) != param_size)
         {
            result = ADSP_EBADPARAM;
         }
         else
         {
            if( TRUE == nEnableFlag)
            {
               pVprx->loopback_enable_flag        = TRUE;
               MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: vprx_set_param_cmd VpTx->VpRx Loopback Enable session(%x)",(int)pVprx->session.session_num);
            }
            else
            {
               pVprx->loopback_enable_flag        = FALSE;
               MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: vprx_set_param_cmd vprx->VpRx Loopback Disable session(%x)",(int)pVprx->session.session_num);
            }
            result = ADSP_EOK;
         }
      }
      else
      {
         result = ADSP_EUNSUPPORTED;
      }
   }
   else
   {
      voice_capi_module_t* curr_module_ptr = NULL;
#if defined(LVVE)/*TCT-NB Tianhongwei add for nxp voice solution 2016/01/04*/
        if((module_id == VOICE_MODULE_LVVEFQ_RX_COMMON) || (module_id == VOICE_MODULE_LVVEFQ_RX_INJECTION))
		{
		    module_id = VOICE_MODULE_LVVEFQ_RX;
		}
#endif
      result = voice_topo_get_module_info( pVprx->modules.modules_array, module_id,pVprx->modules.num_modules,&curr_module_ptr);
      if(ADSP_FAILED(result))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: set_param(): Unsupported set vprx module(%#lx), Ignoring", module_id);
         return ADSP_EUNSUPPORTED;
      }
      else
      {
         if(curr_module_ptr->is_capi)
         {
            if(curr_module_ptr->is_virtual_stub)
            {
               capi_result = CAPI_V2_EOK;
               MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: set_param(): for virtualy stubbed  vprx module(%#lx)", module_id);
            }
            else if(curr_module_ptr->module_ptr)
            {
               capi_result = curr_module_ptr->module_ptr->vtbl_ptr->set_param(curr_module_ptr->module_ptr, param_id, &port_info, &param_data_buf);
            }
            else
            {
               //updating result with EUNSUPPORTED to continue set param for other modules
               capi_result = CAPI_V2_EUNSUPPORTED;
               MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: set_param(): module_ptr is null for module_id(%#lx), Ignoring", module_id);
            }
            result = capi_v2_err_to_adsp_result(capi_result);
         }
         else
         {
            MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: set_param(): set_param is received for non-capi module(0x%lx)", module_id);
            return ADSP_EUNSUPPORTED;
         }
      }
   }
   //MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Vprx internal set param returning with status %ld", result);
   return result;
}

/*
 * This function gets the configuration parameters of a given module ID
 */
ADSPResult vprx_modules_get_param(vprx_t* pVprx, voice_param_data_t* param_data_ptr, uint16_t* param_size)
{
   // TODO: put the following in a big function which includes all the mods and parameters function
   ADSPResult nLocalResult    = ADSP_EUNSUPPORTED;
   uint32_t module_id;
   // Iterate through the entire payload size and copy all updated parameters
   int8_t* pCalibrationDataPayLoad    = (int8_t *)param_data_ptr + sizeof(voice_param_data_t);
   // param_max_size sent from client is copied over to param_size from static service.
   // however this param_size corresponds to the actual size of the parameter, plus the header
   // thus the header size must be subtracted from the size that is available to write the parameter
   // data to.
   int32_t nBufferSize = param_data_ptr->param_size - sizeof(voice_param_data_t);

   if (VPM_RX_DYNAMIC_TOPOLOGY == pVprx->session.topology_id)
   {
      // If Shared object is available, exercise it. If not, just by pass.
      nLocalResult=ADSP_EOK;
      if (pVprx->so.appi_ptr)
      {
         appi_buf_t params_buf;
         params_buf.data_ptr = (int8_t*)param_data_ptr;
         params_buf.actual_data_len = param_data_ptr->param_size;
         params_buf.max_data_len = param_data_ptr->param_size;

         nLocalResult= pVprx->so.appi_ptr->vtbl_ptr->get_param (pVprx->so.appi_ptr, APPI_PARAM_ID_MODULE_CAL, &params_buf);

         // Check if APPI wrote more than has been allocated, indicating that more data was expected..
         if (params_buf.actual_data_len > params_buf.max_data_len)
         {
            nLocalResult=ADSP_ENEEDMORE;
         }
         if (ADSP_FAILED(nLocalResult))
         {
            MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: get_param to shared object failed with result=%d, session(%lx)",
                  nLocalResult, pVprx->session.session_num);
            *param_size=0;
         }
         else
         {
            *param_size = param_data_ptr->param_size;
         }
      }
      else
      {
         *param_size=0;
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: get_param: Shared object for vprx dynamic module 0x%lx does not exist - ignoring", param_data_ptr->module_id);
         nLocalResult = ADSP_EUNSUPPORTED;
      }
   }
   else
   {
      // Populate buffer to issue get param to capi modules
      capi_v2_buf_t param_data_buf;
      capi_v2_port_info_t port_info;
      capi_v2_err_t capi_result = CAPI_V2_EOK;
      port_info.is_valid = FALSE;
      param_data_buf.data_ptr = pCalibrationDataPayLoad;
      param_data_buf.max_data_len = nBufferSize;

      voice_capi_module_t* curr_module_ptr = NULL;
      module_id = param_data_ptr->module_id;
#if defined(LVVE)/*TCT-NB Tianhongwei add for nxp voice solution 2016/01/04*/
    if((module_id == VOICE_MODULE_LVVEFQ_RX_COMMON) || (module_id == VOICE_MODULE_LVVEFQ_RX_INJECTION))
	{
		module_id = VOICE_MODULE_LVVEFQ_RX;
	}
#endif

      nLocalResult = voice_topo_get_module_info( pVprx->modules.modules_array, module_id,pVprx->modules.num_modules,&curr_module_ptr);
      if(ADSP_FAILED(nLocalResult))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: get_param(): Unsupported get vprx module(%#lx). Ignoring",module_id);
         nLocalResult = ADSP_EUNSUPPORTED;
      }
      else
      {
         if(curr_module_ptr->is_capi)
         {
            if(curr_module_ptr->is_virtual_stub)
            {
               capi_result = CAPI_V2_EFAILED;
               MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: get_param(): for  virtually stubbed  vprx module(%#lx)", module_id);
            }
            else if(curr_module_ptr->module_ptr)
            {
               capi_result = curr_module_ptr->module_ptr->vtbl_ptr->get_param(curr_module_ptr->module_ptr, param_data_ptr->param_id, &port_info, &param_data_buf);
            }
            else
            {
               capi_result = CAPI_V2_EFAILED;
               MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: get_param(): module_ptr is null for module_id(%#lx), Ignoring", module_id);

            }
            if(CAPI_V2_SUCCEEDED(capi_result))
            {
               param_data_ptr->param_size = param_data_buf.actual_data_len;
            }
            nLocalResult = capi_v2_err_to_adsp_result(capi_result);
         }
         else
         {
            MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: get_param(): get_param is received for non-capi module(0x%lx)", module_id);
            return ADSP_EUNSUPPORTED;
         }
      }
   }
   if(ADSP_SUCCEEDED(nLocalResult))
   {
      // Return size
      *param_size = param_data_ptr->param_size;
   }
   else
   {
      *param_size = 0;
   }

   MSG_4(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Rx get param,mod id(0x%lx) param id (0x%lx), result(0x%x) session(0x%lx)\n",
         param_data_ptr->module_id, param_data_ptr->param_id, nLocalResult, pVprx->session.session_num);
   return nLocalResult;
}

static void vprx_get_kpps_non_capi_module(vprx_t* pVprx,voice_capi_module_t *curr_module_ptr)
{
   curr_module_ptr->kpps = 0;
   switch(curr_module_ptr->module_id)
   {
      case VOICE_MODULE_HPCM:
      {
         voice_host_pcm_get_kpps( &pVprx->modules.host_pcm_context, &curr_module_ptr->kpps);
         MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: get_kpps(): HPCM kpps (%ld)", curr_module_ptr->kpps);
         break;
      }
      default:
      {
         MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: get_kpps(): Unknown non-capi module ModId(0x%lx)",curr_module_ptr->module_id);
         break;
      }
   }
   return;
}

static void vprx_get_delay_non_capi_module(vprx_t* pVprx,voice_capi_module_t *curr_module_ptr)
{
   curr_module_ptr->delay = 0;
   switch(curr_module_ptr->module_id)
   {
      case VOICE_MODULE_HPCM:
      {
         curr_module_ptr->delay = 0;
         break;
      }
      default:
      {
         MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: get_kpps(): Unknown non-capi module ModId(0x%lx)",curr_module_ptr->module_id);
         break;
      }
   }
   return;
}

ADSPResult vprx_aggregate_modules_kpps(vprx_t* pVprx, uint32_t* kpps_changed)
{
   uint32_t kpps = 0;
   uint32_t modules_kpps=0;
   ADSPResult result=ADSP_EOK;

   if (VPM_RX_DYNAMIC_TOPOLOGY == pVprx->session.topology_id)
   {
      // If Shared object is available, exercise it. If not, just by pass.
      if (pVprx->so.appi_ptr)
      {
         appi_buf_t kpps_buf;
         kpps_buf.data_ptr = (int8_t*)&kpps;
         kpps_buf.max_data_len = sizeof(kpps);

         result = pVprx->so.appi_ptr->vtbl_ptr->get_param (pVprx->so.appi_ptr, APPI_PARAM_ID_KPPS, &kpps_buf);
         if (ADSP_FAILED(result) || kpps_buf.actual_data_len != kpps_buf.max_data_len)
         {
            MSG_4(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: KPPS query to shared object failed with result=%d; actual_data_len=%lu, max_data_len=%lu, session (%lx)",
                  result, kpps_buf.actual_data_len, kpps_buf.max_data_len, pVprx->session.session_num);
         }
         MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: SO KPPS query %lu, session (%x)",kpps, (int) pVprx->session.session_num);
         modules_kpps += kpps;
      }
   }
   else
   {
      voice_capi_module_t* curr_module_ptr = pVprx->modules.modules_array;

      for(uint32_t i=0; i<pVprx->modules.num_modules;i++)
      {
         if(curr_module_ptr->is_capi)
         {
            //account for KPPS only if module is enabled.
            kpps = 0;
            if(TRUE == curr_module_ptr->is_enabled)
            {
               kpps = curr_module_ptr->kpps;
            }
            MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: get_kpps(): ModId(0x%lx) reported KPPS(%d)",curr_module_ptr->module_id ,(int)kpps);
            modules_kpps += kpps;
         }
         else
         {
            vprx_get_kpps_non_capi_module(pVprx,curr_module_ptr);
            modules_kpps += curr_module_ptr->kpps;
         }
         curr_module_ptr++;
      }

   }

   // sample slip
   voice_ss_get_kpps(&(pVprx->modules.ss_struct), &kpps);
   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: SS kpps (%d)", (int)kpps);
   modules_kpps += kpps;
   kpps = 0;

   modules_kpps += 1000; // adding extra 1000 kpps to ceil the number to mpps

   // Retaining behavior as it is as of today. That is to report an increase in KPPS only.
   // Once the VOICE_CMD_SET_TIMING_PARAMS commands are removed,
   // this can be modified to (pVprx->timing.modules_kpps != modules_kpps)
   if (pVprx->timing.modules_kpps >= modules_kpps)
   {
      *kpps_changed = FALSE;
   }
   else
   {
      *kpps_changed = TRUE;
      MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Vprx KPPS changed from (%lu) to (%lu), session(%x)", pVprx->timing.modules_kpps, modules_kpps, (int)pVprx->session.session_num);
   }

   // Update state
   pVprx->timing.modules_kpps = modules_kpps;

   return ADSP_EOK;
}

ADSPResult vprx_aggregate_modules_delay(vprx_t* pVprx)
{
   uint32_t delay = 0;
   uint32_t modules_delay=0;
   ADSPResult result;

   if (VPM_RX_DYNAMIC_TOPOLOGY == pVprx->session.topology_id)
   {
      // If Shared object is available, exercise it. If not, just by pass.
      if (pVprx->so.appi_ptr)
      {
         appi_buf_t delay_buf;
         delay_buf.data_ptr = (int8_t*)&delay;
         delay_buf.max_data_len = sizeof(delay);

         result = pVprx->so.appi_ptr->vtbl_ptr->get_param (pVprx->so.appi_ptr, APPI_PARAM_ID_ALGORITHMIC_DELAY, &delay_buf);
         if (ADSP_FAILED(result) || delay_buf.actual_data_len != delay_buf.max_data_len)
         {
            MSG_4(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: DELAY query to shared object failed with result=%d; actual_data_len=%lu, max_data_len=%lu, session (%lx)",
                  result, delay_buf.actual_data_len, delay_buf.max_data_len, pVprx->session.session_num);
         }
         MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: SO delay query %lu, session (%x)",delay, (int) pVprx->session.session_num);
         modules_delay += delay;
      }
   }
   else
   {
      voice_capi_module_t* curr_module_ptr = pVprx->modules.modules_array;

      for(uint32_t i=0; i<pVprx->modules.num_modules;i++)
      {
         if(curr_module_ptr->is_capi)
         {
            //account for delay only if module is enabled.
            delay = 0;
            if(TRUE == curr_module_ptr->is_enabled)
            {
               delay = curr_module_ptr->delay;
            }
            MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: get_delay(): ModId(0x%lx) reported delay(%d)",curr_module_ptr->module_id ,(int)delay);
            modules_delay += delay;
         }
         else
         {
            vprx_get_delay_non_capi_module(pVprx,curr_module_ptr);
            modules_delay += curr_module_ptr->delay;
         }
         curr_module_ptr++;
      }
   }

   // Sample slip
   result = voice_ss_get_delay(&(pVprx->modules.ss_struct), &delay);
   MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: SS delay (%ld), result %d", delay, (int)result);
   modules_delay += delay;
   delay = 0;

   modules_delay += (PRE_BUF * 1000); // prebuffering

   // Update state
   pVprx->timing.modules_delay = modules_delay;

   return ADSP_EOK;
}

capi_v2_err_t vprx_capi_v2_cb_func(void *context_ptr, capi_v2_event_id_t id, capi_v2_event_info_t *event_info_ptr)
{
   capi_v2_err_t result = CAPI_V2_EOK;
   voice_capi_module_t* module_ptr = (voice_capi_module_t*)context_ptr;
   vprx_t *pVprx = (vprx_t*)module_ptr->svc_ptr;
   if(NULL == context_ptr)
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Error! Received bad context pointer in vprx callback");
      return CAPI_V2_EFAILED;
   }
   MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Vprx event callback invoked, event_id(%d), module(0x%lx)", (int)id, module_ptr->module_id);
   switch(id)
   {
      case CAPI_V2_EVENT_KPPS:
      {
         capi_v2_event_KPPS_t* kpps_ptr = (capi_v2_event_KPPS_t* )event_info_ptr->payload.data_ptr;
         module_ptr->kpps = kpps_ptr->KPPS;
         break;
      }
      case CAPI_V2_EVENT_ALGORITHMIC_DELAY:
      {
         capi_v2_event_algorithmic_delay_t* delay_ptr = (capi_v2_event_algorithmic_delay_t* )event_info_ptr->payload.data_ptr;
         module_ptr->delay = delay_ptr->delay_in_us;
         break;
      }
      case CAPI_V2_EVENT_PROCESS_STATE:
      {
         uint32_t prev_is_enabled = module_ptr->is_enabled;
         module_ptr->is_enabled = ((capi_v2_event_process_state_t *)(event_info_ptr->payload.data_ptr))->is_enabled;

         MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: Module ID(0x%lx) process state changed from (%lu) to (%lu)",module_ptr->module_id,prev_is_enabled, module_ptr->is_enabled);

         //process state is not acceptable for special modules which change media fmt.
         //no such module right now in VPRx

         /*
          * update stream data buffers for modules if the state has changed and module is not inplace
          * Propagate the process state only when the topo graph is created and when we are initializing
          * all modules in the topology
          */
         if(pVprx->modules.module_list_locked)
         {
            if( (prev_is_enabled != module_ptr->is_enabled )  && (FALSE == module_ptr->is_in_place) )
            {
               vprx_reconfig_stream_io_buf(module_ptr);
            }
         }
         break;
      }
      case CAPI_V2_EVENT_GET_LIBRARY_INSTANCE:
      {
         capi_v2_event_get_library_instance_t* get_library_instance = (capi_v2_event_get_library_instance_t*)event_info_ptr->payload.data_ptr;;

         result = capi_v2_library_factory_get_instance(get_library_instance->id,&get_library_instance->ptr);
         if(CAPI_V2_FAILED(result))
         {
             MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: VPRx failed to get library instance with lib_id(0x%x), payload_ptr(0x%p)",get_library_instance->id,get_library_instance->ptr);
             return result;
         }
         break;
      }
      default:
      {
         MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Unsupported event %d", (int)id);
         return CAPI_V2_EBADPARAM;
      }
   }
   return result;
}

static ADSPResult vprx_reconfig_stream_io_buf(voice_capi_module_t* module_ptr)
{
   uint32_t i, start_idx = module_ptr->module_index ;
   vprx_t *pVprx = (vprx_t *)module_ptr->svc_ptr;
   uint32_t end_idx = pVprx->modules.num_modules;
   voice_capi_io_stream_data_idx_t currentIdx = module_ptr->input_buf_index;

   /*
    * iterate over the modules and update the io buffer pointers.
    */
   for( i = start_idx; i < end_idx; i++)
   {
      module_ptr->input_buf_index = currentIdx;
      //switch the buffer index when module is enabled and does not support inplace processing
      if( (TRUE == module_ptr->is_enabled) && (FALSE == module_ptr->is_in_place) )
      {
         switch(currentIdx)
         {
            case INPUT_STRM_BUF_1:
            {
               currentIdx = INPUT_STRM_BUF_2;
               break;
            }
            case INPUT_STRM_BUF_2:
            {
               currentIdx = INPUT_STRM_BUF_1;
               break;
            }
            default:
            {
               MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: Error!! invalid currentIdx(%d), session(%lx)",(int)currentIdx, pVprx->session.session_num);
               break;
            }
         }
      }
      module_ptr->output_buf_index = currentIdx;

      //dbg message
      MSG_6(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: vprx_reconfig_stream_io_buf(): moduleId(%lx), idx(%ld),is_inplace(%ld), enable(%lx), input_index(%ld), output_index(%ld)",
            module_ptr->module_id, module_ptr->module_index,module_ptr->is_in_place, module_ptr->is_enabled,
            module_ptr->input_buf_index, module_ptr->output_buf_index);

      module_ptr++;
   }

   return ADSP_EOK;
}
