
/*========================================================================

@file VoiceProcTx.h
@brief This file provides interfaces to create and control an instance of the
voice proc tx dynamic service

Copyright (c) 2010-2011 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government, Diversion contrary to U.S. law prohibited.

*//*====================================================================== */

/*========================================================================
Edit History

$Header://source/qcom/qct/multimedia2/Audio/voc/svcs/VoiceProcTx/dev/elite/src/VoiceProcTx.cpp

when       who     what, where, why
--------   ---     -------------------------------------------------------
10/11/10   ksa     Added doxygen markup
10/29/09   dc      Created file.

========================================================================== */
#ifndef VOICEPROCTX_H
#define VOICEPROCTX_H

/* =======================================================================
INCLUDE FILES FOR MODULE
========================================================================== */

#include "qurt_elite.h"
#include "Elite.h"
#include "adsp_vpm_api.h"
#include "VoiceCmnUtils.h"
#include "Elite_APPI.h"

#ifdef __cplusplus
extern "C" {
#endif //__cplusplus


/****************************************************************************
** Service related types
*****************************************************************************/

/* -----------------------------------------------------------------------
** Global definitions/forward declarations
** ----------------------------------------------------------------------- */

// 1ms block size in samples
#define VPTX_DMA_BLOCK(SAMPLING_RATE) ((uint32_t)(SAMPLING_RATE)/(uint32_t)(1000))
// #define LOG_VOICEPROC_DATA   

// table to get the number of channels for corresponding Tx topology.
const topo_descriptor_struct topo_vptx[] =
{
   { VPM_TX_NONE,               1},
   { VPM_TX_SM_ECNS,            1},
   { VPM_TX_SM_FLUENCEV5,       1},
   { VPM_TX_SM_ECNS_V2,         1},
   { VPM_TX_CUSTOM_SM_ECNS_1,   1},
   { VPM_TX_CUSTOM_SM_ECNS_2,   1},
   { VPM_TX_CUSTOM_SM_ECNS_3,   1},
   { VPM_TX_DM_FLUENCE,         2},
   { VPM_TX_DM_FLUENCEV5,       2},
   { VPM_TX_CUSTOM_DM_ECNS_1,   2},
   { VPM_TX_CUSTOM_DM_ECNS_2,   2},
   { VPM_TX_CUSTOM_DM_ECNS_3,   2},
   { VPM_TX_DM_FLUENCEV5_BROADSIDE, 2},
   { VPM_TX_DM_VPECNS,          2},
   { VPM_TX_QM_FLUENCE_PRO,     4},
   { VPM_TX_QM_FLUENCE_PROV2,   4},
   { VPM_TX_CUSTOM_QM_ECNS_1,   4},
   { VPM_TX_CUSTOM_QM_ECNS_2,   4},
   { VPM_TX_CUSTOM_QM_ECNS_3,   4},
   { VPM_TX_SINGLE_MIC_DYNAMIC_TOPOLOGY, 1},
   { VPM_TX_DUAL_MIC_DYNAMIC_TOPOLOGY, 2},
   { VPM_TX_QUAD_MIC_DYNAMIC_TOPOLOGY, 4},
#if defined(LVVE)/*TCT-NB Tianhongwei add for nxp voice solution 2016/01/04*/
	{ VOICE_TOPOLOGY_LVVEFQ_TX_SM,	   1},
	{ VOICE_TOPOLOGY_LVVEFQ_TX_DM,	   2},
#endif
};

typedef struct vptx_create_params_t
{
   uint16_t near_port_id;         //afe_tx_port_id ID of Tx AFE port to connect to
   uint16_t far_port_id;          //afe_rx_port_id ID of Rx AFE port to connect to for far reference
   uint32_t topology_id;          //topology_id Topology ID to be used
   uint32_t sampling_rate;        //sampling_rate Sampling rate to run the voice proc at
   uint32_t session_num;          //session_num VPM/ADM session number that this instance belongs to
   uint32_t shared_mem_client;    //shared_mem_client Client to use when converting physical addresses to virtual
   uint16_t num_mics;             //number of mics
} vptx_create_params_t;

typedef struct vptx_appi_fptr_t
{
   appi_getsize_v2_f so_getsize_fptr;  //Function handle to get size of the APPI module in the shared object
   appi_init_v2_f    so_init_fptr;     //Function handle to initialize the APPI module in the shared object
}vptx_appi_fptr_t;

/** Creates an instance of the voice proc tx dynamic service for use in the
    voice path


    @param [out] near handle Return handle to the created instance for near data Q
    @param [out] far handle Return handle to the created instance for far dataQ
    @param [out] handle Return handle to the created instance
    @param [in] vptx_create_params_t - create params
    @param [in] vptx_appi_fptr_t - create params for shared object for VDL v1 topos

    NOTE: Initialization of algorithms is exercised during vptx_create, so the APPI
    function handles must be sent down right now, and not later (using a message for example).
    Otherwise, Vptx sequencing would need to be revisited.
*/
ADSPResult vptx_create (void **near_handle,
                        void **far_handle,
                        vptx_create_params_t* create_param_ptr,
                        vptx_appi_fptr_t* appi_fptr_ptr
                        );

/** Creates an instance of the voice proc tx dynamic service for use in the
    audio path.

    This instance is used when echo cancellation or dual mic is required in the
    audio record path. This instance differs from the voice call instance
    in the following ways:
    1. Contains a resampler to upsample output to 48k
    2. Runs in faster than real time mode
    3. Output is upmixed to stereo

    @param [out] near handle Return handle to the created instance for near data Q
    @param [out] far handle Return handle to the created instance for far dataQ
    @param [in] afe_tx_port_id ID of Tx AFE port to connect to
    @param [in] afe_rx_port_id ID of Rx AFE port to connect to for far reference
    @param [in] topology_id Topology ID to be used
    @param [in] sampling_rate Sampling rate to run the voice proc at
    @param [in] session_num VPM/ADM session number that this instance belongs to
    @param [in] shared_mem_client Client to use when converting physical
    addresses to virtual
*/
ADSPResult vptx_aud_create (void **handle,
                           void **far_handle,
                           uint16_t afe_tx_port_id,
                           uint16_t afe_rx_port_id,
                           uint32_t topology_id,
                           uint32_t sampling_rate,
                           uint32_t session_num,
                           uint32_t shared_mem_client);

/** Creates an instance of the voice proc tx dynamic service for use in the
    audio path

    @param [out] near handle Return handle to the created instance for near data Q
    @param [out] far handle Return handle to the created instance for far dataQ
    @param [out] handle Return handle to the created instance
    @param [in] vptx_create_params_t - create params
*/
ADSPResult vptx_aud_create_v2 (void **near_handle,
                               void **far_handle,
                               vptx_create_params_t* create_param_ptr
                              );

#ifdef __cplusplus
}
#endif //__cplusplus

#endif//#ifndef VOICEPROCTX_H

