
/*========================================================================
Elite

This file contains an example service belonging to the Elite framework.

Copyright (c) 2009 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government, Diversion contrary to U.S. law prohibited.
*//*====================================================================== */

/*========================================================================
Edit History

$Header:

when       who     what, where, why
--------   ---     -------------------------------------------------------
4/29/09   dc      Created file.

========================================================================== */
#include "qurt_elite.h"
#include "VoiceCmnUtils.h"
#include "Voice_SampleSlip.h"
#include "voice_multi_ch_circ_buf.h"
#include "adsp_vparams_api.h"
#include "voice_resampler.h"

#ifndef VOICE_HOST_PCM_H
#define VOICE_HOST_PCM_H

#ifdef __cplusplus
extern "C" {
#endif //__cplusplus

#define MAX_NUM_HOST_PCM_CHANNELS          4      // support quad mic
#define MAX_HOST_PCM_Q_ELEMENTS            1      // host pcm queue depth in each direction (r/w)
#define VOICE_HOST_PCM_LOCALBUF_SIZE      966     // 20 ms in 48kHz + samp slip
#define MAX_VOICE_HOST_PCM_FRAME_SAMPLES  960     // 20 ms in 48kHz
#define NB_VOICE_HPCM_FRAME_SAMPLES       160     // 20 ms in 8kHz
#define BUF_VALID                         1
#define BUF_INVALID                       0


/** Enum indicating host PCM states */
enum 
{
    VOICE_HOST_PCM_OFF = 0, /**< Host PCM is off */
    VOICE_HOST_PCM_READ_ONLY, /**< Host PCM is on in read direction */
    VOICE_HOST_PCM_WRITE_ONLY, /**< Host PCM is on in write direction */
    VOICE_HOST_PCM_READ_WRITE /**< Host PCM is on in both directions */
};


// size should be multiple of 2 in order to easily work well with circbuf
typedef struct
{

   uint32_t tap_point;
   /**< Tap point( GUID ) at which the buffer is operated. */  
   uint32_t buff_addr_msw;
   /**< Upper word of physical address of the read buffer. */
   uint32_t buff_addr_lsw;
   /**< Lower word of physical address of the read buffer. */
   uint16_t buff_size;
   /**< available size of the buffer in the read direction. Size to be written in the write direction */
   uint16_t sampling_rate;
   /**< Sampling rate of the read/write buffer. Supported values 
      are 8000 and 16000. */
   uint16_t num_chan;
   /**< number of channels associated with write buffer (non-interleaved) */
   uint16_t valid;
   /**< flag to check if buffer is valid and should be consumed.  Used to drop stale buffers across reinit */
} voice_host_pcm_buffer_element_t;   

// size of each voice_host_pcm_buffer_element_t in uint16 words (like 16bit samples) -> 16 bytes -> 8 samples
#define VOICE_HOST_PCM_CIRC_BUFFER_ELEMENT_SIZE    (sizeof(voice_host_pcm_buffer_element_t) / 2)
// size of circ buffer -> 8 elements -> 128 bytes -> 64 samples
#define VOICE_HOST_PCM_CIRC_BUFFER_SIZE      (MAX_HOST_PCM_Q_ELEMENTS*VOICE_HOST_PCM_CIRC_BUFFER_ELEMENT_SIZE)
// Multiframe sample slip soultion used in HPCM
#define VOICE_HPCM_SS_MULTIFRAME          VOICE_SS_MULTI_FRAME_4

/** This structure contains the config of Host Pcm in one direction (read or write) */
typedef struct
{
   uint8                    enable;
   /**< Host Pcm enable flag in desired direction */
   uint16_t                 host_sampling_rate;
   /**< sampling rate of Pcm samples at Host in desired direction, NOT the Vptx/Vprx topology rate */
   uint16_t                 host_sampling_rate_factor;
   /*sampling_rate_factor for host/client */
   uint16_t                 num_channels;
   /**< Number of channels configured */
   circbuf_struct           circ_struct_client_buffers;
   /**< circular buffer structure to manage pushed voice_host_pcm_buffer_element_t in desired direction */
   voice_host_pcm_buffer_element_t elements[MAX_HOST_PCM_Q_ELEMENTS];  
   /**< array to actually hold circ buffer data.  Should be large enough to push MAX_HOST_PCM_Q_ELEMENTS. 
   */   
   voice_resampler_config_struct_t  hpcm_resamp_config;
   /* resampler config structure                */
   void       *hpcm_resamp_channel_mem_ptr[MAX_NUM_HOST_PCM_CHANNELS]; //channel mem pointers for resamplers
   /**< Resampler instance for any necessary resampling in desired direction.
       Implementation uses 1 resampler per channel, in mono mode.  For quad-mic, this
       requires four resamplers.
       In read direction resampling from voice proc sampling rate to
       client sampling rate. Vice versa for write direction.
   */
   ss_struct_type_t         ss_struct[MAX_NUM_HOST_PCM_CHANNELS];       
   
   int8_t                   *ss_memory;

   voice_multi_ch_circ_buf_t  circ_struct_local_buf;
   /**< circular buffer struct to accumulate data for client read/wr */
   
} voice_host_pcm_config_t;

/** This defines the state of Host Pcm for Vptx */
typedef struct
{

   uint32_t                  shared_mem_client;
   /**< Handle used to map physical addresses to virtual addresses */
   uint32_t                  apr_handle;
   /**< Handle used for APR packet alloc/send */
   uint16_t                  self_addr; 
   /**< indicate to dynamic service proper APR src Addr */
   uint16_t                  self_port;
   /**< indicate to dynamic service proper APR src Port */
   uint16_t                  client_addr;
   /**< indicate to dynamic service proper APR dst Addr */
   uint16_t                  client_port;
   /**< indicate to dynamic service proper APR dst Port */
   uint16_t                  sampling_rate;
   /**< sampling rate of Pcm samples for device and stream services -- this is topology rate, NOT host pcm rate */   
   uint16_t                  sampling_rate_factor;
   /**< sampling rate factor for device and stream services */
   uint8_t                   ss_enable;
   /**< sample slip enable within host pcm */
   voice_host_pcm_config_t    read_config;
   /**< config in the Read direction (PCM delivered to Host) */
   voice_host_pcm_config_t    write_config;
   /**< config in the Write direction (PCM sent from Host) */
   uint32_t                   tap_point;
   /**< currently support one tap point per context */
   uint32_t                   mem_map_handle;
   /**< memory map handle from client */
   int16_t                  *scratch_resamp_ptr[MAX_NUM_HOST_PCM_CHANNELS];
} voice_host_pcm_context_t;

// Query KPPS of module
ADSPResult voice_host_pcm_get_kpps(voice_host_pcm_context_t *context_ptr, uint32_t* kpps_ptr);

// initialize the host pcm service (default disabled) -- sampling rate is topology rate
ADSPResult voice_host_pcm_init(voice_host_pcm_context_t *context_ptr, uint16 sampling_rate, int32_t in_num_channels, uint8_t ss_enable);

// reinitialize the host pcm service (without affecting enable/disable state) -- sampling rate is topology rate, num_samples is nominal frame size
// reinit has to be called after enabling the HPCM irrespective of the enable history
ADSPResult voice_host_pcm_reinit(voice_host_pcm_context_t *context_ptr, uint16_t num_channels, uint16_t sampling_rate, uint16_t num_samples);                                
 // run host pcm in read/write direction, num_samples is nominal framesize for given topology, assume in-place
ADSPResult voice_host_pcm_process(voice_host_pcm_context_t *context_ptr, uint16_t num_channels, int16 *pBuf[MAX_NUM_HOST_PCM_CHANNELS], uint16_t num_samples, int16_t numSampSlip, int16_t client_rd_wr_signal);
// processes incoming APR buffers 
ADSPResult voice_host_pcm_buffer_rd_wr(voice_host_pcm_context_t *context_ptr, void *pMsg, uint16_t write_direction_valid);                               
// enable/disable the service. num_samples is nominal frame size for given topology              
ADSPResult voice_host_pcm_set_enable(voice_host_pcm_context_t *context_ptr, uint16_t enable, uint16_t direction, uint16_t num_channels, uint16_t hostSamplingRate, uint16_t dsp_sampling_rate, uint16_t num_samples);            
// get enable/disable state
ADSPResult voice_host_pcm_get_enable( voice_host_pcm_context_t *context_ptr, uint16_t *enable, uint16_t direction);
// destroy the host pcm service                                    
ADSPResult voice_host_pcm_end(voice_host_pcm_context_t *context_ptr);                                                        

#ifdef __cplusplus
}
#endif //__cplusplus

#endif//#ifndef VOICE_HOST_PCM_H
