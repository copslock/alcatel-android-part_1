/*========================================================================

 *//** @file VoiceDec.cpp

 Copyright (c) 2009 Qualcomm Technologies, Incorporated.  All Rights Reserved.
 QUALCOMM Proprietary.  Export of this technology or software is regulated
 by the U.S. Government, Diversion contrary to U.S. law prohibited.

 This file contains the code for Voice Decoder (Vdec) Dynamic service. This service
 has two threads. One thread which is the main thread does decode and decoder related
 processing like time-warp etc. Second thread is a worker thread for doing stream
 post-processing. Main thread receives commands from Voice Stream Manager (VSM) and it
 communicates with the worker thread as needed. Vdec initializes the vocoder algorithms
 and processes the data. Multiple Vdec can be instantiated and each of them is identified
 by a session number provided while creating.
  *//*====================================================================== */

/*========================================================================
 Edit History

 $Header: //components/rel/avs.adsp/2.7.1.c4/voice/services/voice_dec/src/vdec_svc.cpp#1 $

 when       who     what, where, why
 --------   ---     -------------------------------------------------------
 04/22/14   pm    removed vdec pp thread as part of tw module removal task
 8/8/2012   pg     Add worker thread for stream pp to off load main thread for time-warp scheduling
 07/12/10   pg     Major cleanup
 06/03/10   pg     Merge features from main to 8660DevAprV2
 EFR, FR, HR, PCM_NB, 4GV_NB, V13k, G711 A/MLAW, 4GV_WB,ENC-DEC
 pkt loopback
 10/29/09   dc     Created file.
 ========================================================================== */

/* =======================================================================
 INCLUDE FILES FOR MODULE
 ========================================================================== */
#include "vdec_private.h"
#include "capi_v2_soft_mute.h"
#include "capi_v2_rx_gain.h"
#include "adsp_vparams_internal_api.h"
#include "capi_v2_adsp_error_code_converter.h"

/*--------------------------------------------------------------*/
/* Macro definitions                                            */
/* -------------------------------------------------------------*/

// Masks for the signals and queues for main thread
#define VDEC_RESYNC_MASK               0x80000000
#define VDEC_DECODE_TICK_MASK          0x40000000
#define VDEC_CMD_MASK                  0x20000000
#define VDEC_BUF_MASK                  0x10000000
#define VDEC_REC_BUF_MASK              0x08000000
#define VDEC_DATA_MASK                 0x04000000
#define VDEC_DECODE_TICK_END_MASK      0x02000000
// no need for handler or to include in any composite masks as this is used for synchronous wait from pp thread
#define VDEC_RESP_MASK                 0x01000000
// not listening actively, no need for handler or to include in any composite masks
#define VDEC_GPQ_DATA_MASK             0x00800000
#define VDEC_VDS_PKT_REQ_TICK_END_MASK 0x00400000

#define VDEC_CMD_TICK_MASK          (VDEC_DECODE_TICK_MASK | \
         VDEC_CMD_MASK |VDEC_RESYNC_MASK)
#define VDEC_ALL_MASK               (VDEC_CMD_TICK_MASK | VDEC_BUF_MASK |\
         VDEC_DECODE_TICK_END_MASK | \
      VDEC_REC_BUF_MASK|VDEC_RESYNC_MASK)

#define VDEC_SS_MULTIFRAME VOICE_SS_MULTI_FRAME_4

#define FR_DEC_FROM_VOICE_PATH         0 // flag to differentiate of unpacking
// of bitstream for FR decoder starting from LSB or MSB

// TODO: Remove when API updated
/**  Parameter for enabling internal loopback feature for a particular session.
  Fixed size of LOOPBACK_ENABLE_PARAM_BYTES = 4 bytes. 
 */
#define VOICE_PARAM_LOOPBACK_ENABLE (0x00010E18)

// externs
extern uint32_t vsm_memory_map_client;

/* -----------------------------------------------------------------------
 ** Constant / Define Declarations
 ** ----------------------------------------------------------------------- */
// How many buffers in output buffer queue? May need to make this configurable...
static const uint32_t MAX_BUF_Q_ELEMENTS = 4;   // this means 3 output buffers available for main
// thread to deliver to worker thread. 3 are needed
// to account TW worst case compression - 8.875ms
// Main Thread name
static char_t aTHREAD_NAME[6] =
{ 'V', 'D', 'C', '$', '\0' };
// Main Thread stack size
static const uint32_t THREAD_STACK_SIZE = 75 * 1024;

// todo: move these to api hdr
static const uint32_t G729AB_2ND_DEC_PKT_OFFSET = 11;
static const uint32_t G729AB_DEC_PKT_SIZE_IN_WORDS_10MSEC = 6;

//Number of bits in an MODEM IF1 packet for a given rate.
//Only header is different compared to IF1
//ModemIF1 = IF1 - 24 + 8
// -24 : IF1 pkt header and +8 : Modem IF1 pkt header
static const uint32_t num_bits_for_modemIF1[] =
{ 103, 111, 126, 142, 156, 167, 212, 252 };
//Number of bits in an IF2 packet for a given rate
static const uint32_t num_bits_forIF2[] =
{ 99, 107, 122, 138, 152, 163, 208, 248, 43, 47, 42, 41, 0, 0, 0, 4 };
//Number of bits in an MODEM IF1 packet for a given rate.
//Only header is different compared to IF1
//ModemIF1 = IF1 - 24 + 8
// -24 : IF1 pkt header and +8 : Modem IF1 pkt header
static const uint32_t amrWBNum_bits_for_modemIF1[] =
{ 140, 185, 261, 293, 325, 373, 405, 469, 485 };
// Array of number of bytes for a given evrc packet type
static const int32_t evrc_num_bits_for_modem[] =
{ 1, 3, 0, 11, 23, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0 };
// Array of number of bytes for a given v13k packet type
static const int32_t V13k_num_bits_for_modem[] =
{ 1, 4, 8, 17, 35, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0 };
// Array of number of bytes for a given 4gv packet type
static const int32_t fourgv_num_bits_for_modem[] =
{ 8, 24, 48, 88, 179, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0 };

// values returned from 1x TTY lib
enum
{
   VOICE_DEC_1X_NON_TTY = 0, VOICE_DEC_1X_TTY_SILENCE, VOICE_DEC_1X_TTY
};

// for logging purposes
#define VDEC_LOG_PKT_SIZE_AMR_NB_MODEM    36
#define VDEC_LOG_PKT_SIZE_AMR_NB_IF2      36
#define VDEC_LOG_PKT_SIZE_EFR_MODEM       36
#define VDEC_LOG_PKT_SIZE_FR_MODEM        36
#define VDEC_LOG_PKT_SIZE_HR_MODEM        36
#define VDEC_LOG_PKT_SIZE_AMR_WB_MODEM    62
#define VDEC_LOG_PKT_SIZE_EVRC_MODEM      22
#define VDEC_LOG_PKT_SIZE_4GV             22
#define VDEC_LOG_PKT_SIZE_13K_MODEM       36
#define VDEC_LOG_PKT_SIZE_G729AB          6

static const uint32_t evs_mode_bandwidth_table[][2] = {
   {VSM_EVS_VOC_BANDWIDTH_NB             , VSM_VOC_OPERATING_MODE_NB},
   {VSM_EVS_VOC_BANDWIDTH_WB             , VSM_VOC_OPERATING_MODE_WB},
   {VSM_EVS_VOC_BANDWIDTH_SWB            , VSM_VOC_OPERATING_MODE_SWB},
   {VSM_EVS_VOC_BANDWIDTH_FB             , VSM_VOC_OPERATING_MODE_FB},
   {4                                    , VSM_VOC_OPERATING_MODE_NONE}
};

static uint32_t evs_find_operating_bandwidth(uint32_t  mode_bandwidth)
{
   int16_t i = 0;
   while( 4 != evs_mode_bandwidth_table[i][0])
   {
      if (evs_mode_bandwidth_table[i][0] == mode_bandwidth)
      {
         return(evs_mode_bandwidth_table[i][1]);
      }
      i++;
   }
   return 0;
}
static const uint32_t evs_pkt_size_rate_table[][2] = {
   {0             ,17 },
   {1             ,23 },
   {2             ,32 },
   {3             ,36 },
   {4             ,40 },
   {5             ,46 },
   {6             ,50 },
   {7             ,58 },
   {8             ,60 },
   {9             ,5 },
   {14            ,0},
   {15            ,0},
   {16            ,7},
   {17            ,18},
   {18            ,20},
   {19            ,24},
   {20            ,33},
   {21           , 41},
   {22            ,61},
   {23            ,80},
   {24            ,120},
   {25           , 160},
   {26            ,240},
   {27            ,320},
   {28           , 6},
   {29           , -1}
};


static uint32_t evs_find_pkt_size(uint32_t  header_payload)
{
   int16_t i = 0;
   while( 29 != evs_pkt_size_rate_table[i][0])
   {
      if (evs_pkt_size_rate_table[i][0] == header_payload)
      {
         return(evs_pkt_size_rate_table[i][1]);
      }
      i++;
   }
   return 0;
}
/* -----------------------------------------------------------------------
 ** Function prototypes
 ** ----------------------------------------------------------------------- */
// channel handler functions
static ADSPResult vdec_resync_handler (void* instance_ptr);
static ADSPResult vdec_vfr_decode_handler (void* instance_ptr);
static ADSPResult vdec_cmd_handler (void* instance_ptr);
static ADSPResult vdec_buf_handler (void* instance_ptr);
static ADSPResult vdec_data_handler (void* instance_ptr);

// elite message handler functions
static ADSPResult vdec_custom_msg (void* instance_ptr, elite_msg_any_t* msg_ptr);
static ADSPResult vdec_destroy_yourself_cmd (void* instance_ptr, elite_msg_any_t* msg_ptr);
static ADSPResult vdec_connect_dwn_stream_cmd (void* instance_ptr, elite_msg_any_t* msg_ptr);
static ADSPResult vdec_disconnect_dwn_stream_cmd (void* instance_ptr, elite_msg_any_t* msg_ptr);
static ADSPResult vdec_stop_cmd (void* instance_ptr, elite_msg_any_t* msg_ptr);
static ADSPResult vdec_run_cmd (void* instance_ptr, elite_msg_any_t* msg_ptr);
static ADSPResult vdec_set_param_cmd (void* instance_ptr, elite_msg_any_t* msg_ptr);
static ADSPResult vdec_get_param_cmd (void* instance_ptr, elite_msg_any_t* msg_ptr);
static ADSPResult vdec_set_media_type (void* instance_ptr, elite_msg_any_t* msg_ptr);
static ADSPResult vdec_apr_cmd (void* instance_ptr, elite_msg_any_t* msg_ptr);

// custom message handler functions
static ADSPResult vdec_set_mute_cmd (void* instance_ptr, elite_msg_any_t* msg_ptr);
static ADSPResult vdec_set_timing_cmd (void* instance_ptr, elite_msg_any_t* msg_ptr);
static ADSPResult vdec_set_timingv2_cmd (void* instance_ptr, elite_msg_any_t* msg_ptr);
static ADSPResult vdec_set_timingv3_cmd (void* instance_ptr, elite_msg_any_t* msg_ptr);
static ADSPResult vdec_reinit_cmd (void* instance_ptr, elite_msg_any_t* msg_ptr);
static ADSPResult vdec_stop_record_cmd (void* instance_ptr, elite_msg_any_t* msg_ptr);
static ADSPResult vdec_start_record_cmd (void* instance_ptr, elite_msg_any_t* msg_ptr);
static ADSPResult vdec_config_host_pcm (void* instance_ptr, elite_msg_any_t* msg_ptr);
static ADSPResult vdec_register_event (void* instance_ptr, elite_msg_any_t* msg_ptr);
static ADSPResult vdec_unregister_event (void* instance_ptr, elite_msg_any_t* msg_ptr);
static ADSPResult vdec_set_param_v3 (void* instance_ptr, elite_msg_any_t* msg_ptr);
static ADSPResult vdec_set_enc_rate (void* instance_ptr, elite_msg_any_t* msg_ptr);

// Helper functions
static ADSPResult vdec_thread (void* instance_ptr);
static void vdec_destroy (vdec_t* vdec_ptr);
static ADSPResult vdec_process (vdec_t* vdec_ptr);
static ADSPResult vdec_send_dec_pkt_req (vdec_t *vdec_ptr);
static ADSPResult vdec_send_out_buf_downstream (vdec_t* vdec_ptr, qurt_elite_bufmgr_node_t *out_buf_mgr_node_ptr);
static ADSPResult vdec_init (vdec_t *vdec_ptr);
static ADSPResult vdec_dec_params_init (vdec_t *vdec_ptr);
static ADSPResult vdec_ctrl_params_init (vdec_t *vdec_ptr);
static ADSPResult vdec_processG711 (vdec_t* vdec_ptr, int16_t *in_ptr, int16_t *out_ptr, int16_t outputbufsize);
static ADSPResult vdec_voc_init (vdec_t* vdec_ptr);
static ADSPResult vdec_process_amr_nb_modem (vdec_t* vdec_ptr);
static ADSPResult vdec_process_amr_nb_if2 (vdec_t* vdec_ptr);
static ADSPResult vdec_process_amr_wb_modem (vdec_t* vdec_ptr);
static ADSPResult vdec_process_evrc (vdec_t* vdec_ptr);
static ADSPResult vdec_processV13k (vdec_t* vdec_ptr);
static ADSPResult vdec_process4GV (vdec_t* vdec_ptr);
static ADSPResult vdec_process_efr (vdec_t* vdec_ptr);
static ADSPResult vdec_process_fr (vdec_t* vdec_ptr);
static ADSPResult vdec_process_hr (vdec_t* vdec_ptr);
static ADSPResult vdec_process_pkt_data (vdec_t* vdec_ptr);
static ADSPResult vdec_process_oob_pkt_data (vdec_t* vdec_ptr);
static ADSPResult vdec_clear_loopback_gpq (vdec_t *vdec_ptr);
static ADSPResult vdec_read_apr_msg (vdec_t* vdec_ptr, elite_msg_any_t *sMsg);
static ADSPResult vdec_check_dec_struct_size (uint32_t dec_struct_size, uint32_t sess_num);
static ADSPResult vdec_log_vocoder_packet (uint32_t voc_type, uint16_t sampling_rate, uint32_t log_session_id, void *out_ptr, uint16_t size);
static ADSPResult vdec_send_mode_notification (vdec_t* vdec_ptr, uint32_t eamr_curr_bw_det_state);
void vdec_clear_data_queue (vdec_t *vdec_ptr);
void vdec_vtm_unsubscribe (vdec_t* vdec_ptr, Vtm_SubUnsubMsg_t *data_ptr, uint32_t mask, qurt_elite_channel_t *channel);
void vdec_vtm_subscribe (vdec_t* vdec_ptr, Vtm_SubUnsubMsg_t *data_ptr);
static ADSPResult vdec_allocate_mem (vdec_t* vdec_ptr);
static void vdec_allocate_mem_free (vdec_t* vdec_ptr);
static uint32_t vdec_get_dec_static_size ();
static ADSPResult vdec_set_pp_samp_rate (void* instance_ptr, elite_msg_any_t* msg_ptr);
static ADSPResult vdec_set_pkt_exchange_mode (void* instance_ptr, elite_msg_any_t* msg_ptr);
static ADSPResult vdec_set_oob_pkt_exchange_config (void* instance_ptr, elite_msg_any_t* msg_ptr);
static ADSPResult vdec_get_kpps_cmd (void* instance_ptr, elite_msg_any_t* msg_ptr);
static ADSPResult vdec_aggregate_modules_kpps (void* instance_ptr, uint32_t* kpps_changed);
static ADSPResult vdec_get_delay_cmd (void* instance_ptr, elite_msg_any_t* msg_ptr);
static ADSPResult vdec_aggregate_modules_delay (void* instance_ptr);
static ADSPResult vdec_get_vocoder_delay (uint32_t voc_type, uint32_t* delay_ptr);
static void vdec_set_default_op_mode (void* instance_ptr);
static ADSPResult vdec_send_mode_notification_v2 (void* instance_ptr);
static ADSPResult vdec_set_param_int (vdec_t* vdec_ptr, uint32_t module_id, uint32_t param_id, void* payload_address_in, uint32_t param_size);

//vdec pp related
static ADSPResult vdec_rec_buf_handler (void *instance_ptr);
static ADSPResult vdec_pp_process (vdec_t* vdec_ptr);
static ADSPResult vdec_pp_allocate_mem (vdec_t* vdec_ptr);
static ADSPResult vdec_pp_init (vdec_t* vdec_ptr);
static ADSPResult vdec_pp_modules_init (vdec_t* vdec_ptr);
static void vdec_init_rec_circbuf (vdec_t* vdec_ptr);
static void vdec_pp_allocate_mem_free (vdec_t *vdec_ptr);
static ADSPResult vdec_send_mixer_media_type (vdec_t *vdec_ptr);
static ADSPResult vdec_stream_pp_init (vdec_t *vdec_ptr);
static ADSPResult vdec_pp_ctrl_params_init (vdec_t *vdec_ptr);
static ADSPResult vdec_pp_reinit (vdec_t *vdec_ptr);
static void inline vdec_cal_drift (vdec_t *vdec_ptr);
static void vdec_cal_sample_slip_stuff (vdec_t *vdec_ptr, int16_t* slip_stuff_samples);
static ADSPResult vdec_process_ctm_rx (vdec_t *vdec_ptr, int16_t *in_ptr, int16_t *out_ptr, uint16_t samples);
static ADSPResult vdec_process_oobtty_rx (vdec_t* vdec_ptr, int16_t *in_ptr, int16_t *out_ptr, uint16_t samples);
static int32_t vdec_send_vsm_dtmf_tone_status (vdec_t *vdec_ptr);
static ADSPResult vdec_send_afe_media_type (vdec_t *vdec_ptr);
static ADSPResult vdec_send_rec_buf_to_afe (vdec_t *vdec_ptr, qurt_elite_bufmgr_node_t *out_buf_mgr_node_ptr);
static int32_t vdec_send_oobtty_char_accepted (vdec_t *vdec_ptr);

// calibration callback function
static void vdec_calibration_cb_func (cvd_cal_param_t* cal_params_ptr, void* cb_data);

// Rx gain module functions
static ADSPResult voice_rx_gain_set_param (voice_capi_module_t *rx_gain_ptr, int8_t *params_buffer_ptr, uint32_t param_id, uint16_t param_size);
static ADSPResult voice_rx_gain_get_param (voice_capi_module_t *rx_gain_ptr, int8_t *params_buffer_ptr, uint32_t param_id, uint32_t buffer_size, uint16_t *param_size_ptr);

// Capi callback function
capi_v2_err_t vdec_capi_v2_cb_func(void *context_ptr, capi_v2_event_id_t id, capi_v2_event_info_t *event_info_ptr);

// DTMF detect process wrapper function
void voice_dtmf_detect_process_wrapper (dtmf_detect_struct_t *dtmf_detect_ptr, int16_t *out_ptr, int16_t *in_ptr, uint16_t no_of_samples, uint16_t sampling_rate);

// ECALL RX wrapper function
void voice_ecall_rx (vdec_t *vdec_ptr, int16_t *in_out_ptr);
/* -----------------------------------------------------------------------
 ** Message handlers
 ** -----------------------------------------------------------------------
 */

/* Vdec channel handler */
static ADSPResult (*pVdecHandler[]) (void *) =
{
   vdec_resync_handler,       // handles resync                     - VDEC_RESYNC_MASK
   vdec_vfr_decode_handler,   // handles vfr signal for decode      - VDEC_DECODE_TICK_MASK
   vdec_cmd_handler,          // handles cmds                       - VDEC_CMD_MASK
   vdec_buf_handler,          // handles output buffers             - VDEC_BUF_MASK
   vdec_rec_buf_handler,      // handles record path                - VDEC_REC_BUF_MASK
   vdec_data_handler,         // handles input data buffers         - VDEC_DATA_MASK
};

/* Vdec FADD msg handler */
static elite_svc_msg_handler_func pEliteHandler[] =
{
   vdec_custom_msg,                //0  - ELITE_CUSTOM_MSG
   elite_svc_unsupported,          //1  - ELITE_CMD_START_SERVICE
   vdec_destroy_yourself_cmd,      //2  - ELITE_CMD_DESTROY_SERVICE
   vdec_connect_dwn_stream_cmd,    //3  - ELITE_CMD_CONNECT
   vdec_disconnect_dwn_stream_cmd, //4  - ELITE_CMD_DISCONNECT
   vdec_stop_cmd,                  //5  - ELITE_CMD_PAUSE
   vdec_run_cmd,                   //6  - ELITE_CMD_RESUME
   elite_svc_unsupported,          //7  - ELITE_CMD_FLUSH
   vdec_set_param_cmd,             //8  - ELITE_CMD_SET_PARAM
   vdec_get_param_cmd,             //9  - ELITE_CMD_GET_PARAM
   elite_svc_unsupported,          //10 - ELITE_DATA_BUFFER
   vdec_set_media_type,            //11 - ELITE_DATA_MEDIA_TYPE
   elite_svc_unsupported,          //12 - ELITE_DATA_EOS
   elite_svc_unsupported,          //13 - ELITE_DATA_RAW_BUFFER
   elite_svc_unsupported,          //14 - ELITE_CMD_STOP_SERVICE
   vdec_apr_cmd                    //15 - ELITE_APR_PACKET_OPCODE
};

/* Vdec custom msg handler */
static elite_svc_msg_handler_func pHandler[VDEC_NUM_MSGS] =
{
   vdec_get_kpps_cmd,                // VDEC_GET_KPPS_CMD
   vdec_set_mute_cmd,                // VDEC_SET_MUTE_CMD
   vdec_set_timing_cmd,              // VDEC_SET_TIMING_CMD
   vdec_set_timingv2_cmd,            // VDEC_SET_TIMINGV2_CMD
   vdec_reinit_cmd,                  // VDEC_REINIT_CMD
   vdec_stop_record_cmd,             // VDEC_STOP_RECORD_CMD
   vdec_start_record_cmd,            // VDEC_START_RECORD_CMD_V2
   vdec_config_host_pcm,             // VDEC_CONFIG_HOST_PCM
   vdec_set_timingv3_cmd,            // VDEC_SET_TIMINGV3_CMD
   vdec_get_delay_cmd,               // VDEC_GET_DELAY_CMD
   vdec_set_param_v3,                // VDEC_SET_PARAM_V3
   vdec_set_pp_samp_rate,            // VDEC_SET_STREAM_PP_SAMP_RATE
   vdec_set_pkt_exchange_mode,       // VDEC_SET_PKT_EXCHANGE_MODE
   vdec_set_oob_pkt_exchange_config, // VDEC_SET_OOB_PKT_EXCHANGE_CONFIG
   vdec_register_event,              // VDEC_REGISTER_EVENT
   vdec_unregister_event,            // VDEC_UNREGISTER_EVENT
   vdec_set_enc_rate,                // VDEC_SET_ENC_RATE
};

/* =======================================================================
 **                          Function Definitions
 ** ======================================================================= */
ADSPResult vdec_create (uint32_t nInputParam,
      voice_strm_apr_info_t *apr_info_ptr,
      voice_strm_tty_state_t *tty_state_ptr, void **ppHandle, uint32_t session_num)
{
   ADSPResult result = ADSP_EOK;
   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: vdec_create enter session(%lx)", session_num);

   // allocate instance struct
   vdec_t *vdec_ptr = (vdec_t*) qurt_elite_memory_malloc (sizeof(vdec_t), QURT_ELITE_HEAP_DEFAULT);
   if (!vdec_ptr)
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: Failed to allocate memory for VDEC struct !!");
      return ADSP_ENOMEMORY;
   }

   // zero out all the fields
   memset (vdec_ptr, 0, sizeof(vdec_t));

   // Initialize instance variables
   vdec_ptr->svc_handle.unSvcId = ELITE_VOICE_DEC_SVCID;
   vdec_ptr->wait_mask = VDEC_CMD_TICK_MASK;
   vdec_ptr->apr_info_ptr = apr_info_ptr;
   vdec_ptr->tty_state_ptr = tty_state_ptr;
   vdec_ptr->session_num = session_num;
   vdec_ptr->modules.vdec_pp_modules.dtmf_det.apr_info_dtmf.apr_handle = vdec_ptr->apr_info_ptr->apr_handle;
   // initialze qurt_elite channel for main thread. This is required for any destroy to succeed.
   qurt_elite_channel_init(&vdec_ptr->qurt_elite_channel);

   (void) vdec_init (vdec_ptr);

   if (qurt_elite_globalstate.pVoiceTimerCmdQ == NULL)
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: Failed to get Vtm cmdQ session(%lx)", session_num);
      vdec_destroy (vdec_ptr);
      return ADSP_EUNSUPPORTED;
   }
   vdec_ptr->vtm_cmdq_ptr = qurt_elite_globalstate.pVoiceTimerCmdQ;

   vdec_ptr->vds.vds_handle_ptr = vds_handle;
   vdec_ptr->vds.vds_client_token = VDS_CREATE_TOKEN(vdec_ptr->session_num, VDS_CLIENT_VDEC_PKT_REQUEST);

   vdec_ptr->modules.decoder.fourGV_decode_ptr = NULL;
   result = vdec_allocate_mem (vdec_ptr);
   if (ADSP_FAILED(result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "VCP: Failed vdec Allocate Memory function (%lx)", session_num);
      vdec_destroy (vdec_ptr);
      return result;
   }

   result = vdec_pp_allocate_mem (vdec_ptr);
   if (ADSP_FAILED(result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "VCP: Failed vdec pp Allocate Memory function (%lx)", session_num);
      vdec_destroy (vdec_ptr);
      return result;
   }

   if (ADSP_FAILED(result = voice_oobtty_rx_mem_alloc (&vdec_ptr->modules.vdec_pp_modules.oobtty.oobtty_rx_struct)))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: OOB TTY Rx not created session(%lx)", session_num);
      vdec_destroy (vdec_ptr);
      return result;
   }

   (void) vdec_pp_init (vdec_ptr);

   result = vdec_pp_modules_init (vdec_ptr);
   if (ADSP_FAILED(result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "VCP: Failed vdec_pp_modules_init (%lx)", session_num);
      vdec_destroy (vdec_ptr);
      return result;
   }

   // initialize CTM demodulator here
   // so NOT to re-initialize when STOP->RUN, eg, handover happens
   if (ADSP_FAILED(result = voice_ctm_rx_init (&vdec_ptr->modules.vdec_pp_modules.ctm.ctm_rx_struct, session_num)))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: CTM Rx not created session(%lx)", session_num);
      vdec_destroy (vdec_ptr);
      return result;
   }

   // DTMF Detection init
   result = voice_dtmf_detect_init (&(vdec_ptr->modules.vdec_pp_modules.dtmf_det.dtmf_det_struct),
         VOICE_FB_SAMPLING_RATE, MAX_FRAME_SIZE);
   if (ADSP_FAILED(result))
   {
      MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: DTMF DETECT init failed, result(%d) session(%lx) ", result, vdec_ptr->session_num);
      vdec_destroy (vdec_ptr);
      return result;
   }

   // SlowTalk init
   result = voice_slow_talk_init (&(vdec_ptr->modules.vdec_pp_modules.slow_talk_struct));
   if (ADSP_FAILED(result))
   {
      MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: SlowTalk init failed, result(%d) session(%lx) ", result, vdec_ptr->session_num);
      vdec_destroy (vdec_ptr);
      return result;
   }

   // initialize host pcm module
   {
      result = voice_host_pcm_init (&vdec_ptr->modules.host_pcm_context, 8000 /*default sampling rate*/, 1 /*init for 1 channel by default*/,
            FALSE /* disable sample slip/stuff */);
      if (ADSP_FAILED(result))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "VCP: Failed Host Pcm Init in rx stream!! session(%lx)", session_num);
         vdec_destroy (vdec_ptr);
         return result;
      }
   }

   vdec_init_rec_circbuf (vdec_ptr);

   // make Vdec Q names unique per instance
   snprintf (vdec_ptr->data_q_name, QURT_ELITE_DEFAULT_NAME_LEN, "Vdec_DataQ%2x", (int) session_num);
   snprintf (vdec_ptr->gp_q_name, QURT_ELITE_DEFAULT_NAME_LEN, "Vdec_GpQ%2x", (int) session_num);
   snprintf (vdec_ptr->cmd_q_name, QURT_ELITE_DEFAULT_NAME_LEN, "Vdec_CmdQ%2x", (int) session_num);
   snprintf (vdec_ptr->buf_q_name, QURT_ELITE_DEFAULT_NAME_LEN, "Vdec_BufQ%2x", (int) session_num);
   snprintf (vdec_ptr->resp_q_name, QURT_ELITE_DEFAULT_NAME_LEN, "Vdec_RespQ%2x", (int) session_num);
   snprintf (vdec_ptr->modules.vdec_pp_modules.record.rec_buf_q_name,
         QURT_ELITE_DEFAULT_NAME_LEN, "Vdec_Rec_BufQ%2x", (int) session_num);


   // Create the queues. Use non-blocking queues, since pselect is always used.
   // pselect blocks on any non-masked queue to receive, then can do non-blocking checks.
   // DataQ for inputs
   // CmdQ for cmd inputs
   // BufQ for output buffers - no need of a Q, instead use local buffer, todo: need bufQ for enc-dec loopback
   // signal_ptr for 20ms tick
   // signal_end_ptr for handshaking unsubscribe with VoiceTimer

   /* Allocate memory for queues */
   vdec_ptr->svc_handle.dataQ = (qurt_elite_queue_t*) qurt_elite_memory_malloc (QURT_ELITE_QUEUE_GET_REQUIRED_BYTES(MAX_DATA_Q_ELEMENTS), QURT_ELITE_HEAP_DEFAULT);
   vdec_ptr->svc_handle.cmdQ = (qurt_elite_queue_t*) qurt_elite_memory_malloc (QURT_ELITE_QUEUE_GET_REQUIRED_BYTES(MAX_CMD_Q_ELEMENTS), QURT_ELITE_HEAP_DEFAULT);
   vdec_ptr->svc_handle.gpQ = (qurt_elite_queue_t*) qurt_elite_memory_malloc (QURT_ELITE_QUEUE_GET_REQUIRED_BYTES(MAX_DATA_GPQ_ELEMENTS), QURT_ELITE_HEAP_DEFAULT);
   vdec_ptr->buf_q_ptr = (qurt_elite_queue_t*) qurt_elite_memory_malloc (QURT_ELITE_QUEUE_GET_REQUIRED_BYTES(MAX_CMD_Q_ELEMENTS), QURT_ELITE_HEAP_DEFAULT);
   vdec_ptr->vtm_sub_unsub_decode_data.signal_ptr = (qurt_elite_signal_t*) qurt_elite_memory_malloc (sizeof(qurt_elite_signal_t), QURT_ELITE_HEAP_DEFAULT);
   vdec_ptr->vtm_sub_unsub_decode_data.signal_end_ptr = (qurt_elite_signal_t*) qurt_elite_memory_malloc (sizeof(qurt_elite_signal_t), QURT_ELITE_HEAP_DEFAULT);
   vdec_ptr->timing.vtm_sub_unsub_vds_pkt_req_data.resync_signal_ptr = (qurt_elite_signal_t*) qurt_elite_memory_malloc (sizeof(qurt_elite_signal_t), QURT_ELITE_HEAP_DEFAULT);
   vdec_ptr->vtm_sub_unsub_decode_data.resync_signal_ptr = NULL;
   vdec_ptr->modules.vdec_pp_modules.record.rec_buf_q_ptr = (qurt_elite_queue_t*) qurt_elite_memory_malloc (QURT_ELITE_QUEUE_GET_REQUIRED_BYTES(PP_MAX_DATA_Q_ELEMENTS), QURT_ELITE_HEAP_DEFAULT);
   vdec_ptr->resp_q_ptr = (qurt_elite_queue_t*) qurt_elite_memory_malloc (QURT_ELITE_QUEUE_GET_REQUIRED_BYTES(MAX_CMD_Q_ELEMENTS), QURT_ELITE_HEAP_DEFAULT);

   /* NOTE: This delivery tick signal will be allocated by VDS, don't allocate/free/add to channel here*/
   vdec_ptr->timing.vtm_sub_unsub_vds_pkt_req_data.signal_ptr = NULL;
   vdec_ptr->timing.vtm_sub_unsub_vds_pkt_req_data.signal_end_ptr = (qurt_elite_signal_t*) qurt_elite_memory_malloc(sizeof(qurt_elite_signal_t), QURT_ELITE_HEAP_DEFAULT);

   if (NULL == vdec_ptr->svc_handle.dataQ
         || NULL == vdec_ptr->svc_handle.cmdQ
         || NULL == vdec_ptr->svc_handle.gpQ
         || NULL == vdec_ptr->buf_q_ptr
         || NULL == vdec_ptr->vtm_sub_unsub_decode_data.signal_ptr
         || NULL == vdec_ptr->timing.vtm_sub_unsub_vds_pkt_req_data.signal_end_ptr
         || NULL == vdec_ptr->vtm_sub_unsub_decode_data.signal_end_ptr
         || NULL == vdec_ptr->modules.vdec_pp_modules.record.rec_buf_q_ptr
         || NULL == vdec_ptr->timing.vtm_sub_unsub_vds_pkt_req_data.resync_signal_ptr
         || NULL == vdec_ptr->resp_q_ptr)
   {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "VCP: Failed to allocate memory for Vdec Queues/Signals session(%#lx)", session_num);
      vdec_destroy (vdec_ptr);
      return ADSP_ENOMEMORY;
   }

   if ( ADSP_FAILED(result = qurt_elite_queue_init (vdec_ptr->data_q_name, MAX_DATA_Q_ELEMENTS,vdec_ptr->svc_handle.dataQ))
         || ADSP_FAILED(result = qurt_elite_queue_init(vdec_ptr->cmd_q_name, MAX_CMD_Q_ELEMENTS, vdec_ptr->svc_handle.cmdQ))
         || ADSP_FAILED(result = qurt_elite_queue_init(vdec_ptr->gp_q_name, MAX_DATA_GPQ_ELEMENTS, vdec_ptr->svc_handle.gpQ))
         || ADSP_FAILED(result = qurt_elite_queue_init(vdec_ptr->buf_q_name, MAX_CMD_Q_ELEMENTS, vdec_ptr->buf_q_ptr))
         || ADSP_FAILED(result = qurt_elite_queue_init(vdec_ptr->resp_q_name, MAX_CMD_Q_ELEMENTS, vdec_ptr->resp_q_ptr))
         || ADSP_FAILED(result = qurt_elite_queue_init(vdec_ptr->modules.vdec_pp_modules.record.rec_buf_q_name, PP_MAX_DATA_Q_ELEMENTS,vdec_ptr->modules.vdec_pp_modules.record.rec_buf_q_ptr))
         || ADSP_FAILED(result = qurt_elite_signal_init(vdec_ptr->vtm_sub_unsub_decode_data.signal_ptr))
         || ADSP_FAILED(result = qurt_elite_signal_init(vdec_ptr->timing.vtm_sub_unsub_vds_pkt_req_data.signal_end_ptr))
         || ADSP_FAILED(result = qurt_elite_signal_init(vdec_ptr->vtm_sub_unsub_decode_data.signal_end_ptr))
         || ADSP_FAILED(result = qurt_elite_signal_init(vdec_ptr->timing.vtm_sub_unsub_vds_pkt_req_data.resync_signal_ptr))
         || ADSP_FAILED(result = qurt_elite_channel_addq(&vdec_ptr->qurt_elite_channel, (vdec_ptr->svc_handle.cmdQ), VDEC_CMD_MASK))
         || ADSP_FAILED(result = qurt_elite_channel_addq(&vdec_ptr->qurt_elite_channel, (vdec_ptr->svc_handle.dataQ), VDEC_DATA_MASK))
         || ADSP_FAILED(result = qurt_elite_channel_addq(&vdec_ptr->qurt_elite_channel, (vdec_ptr->svc_handle.gpQ), VDEC_GPQ_DATA_MASK))
         || ADSP_FAILED(result = qurt_elite_channel_addq(&vdec_ptr->qurt_elite_channel, (vdec_ptr->buf_q_ptr), VDEC_BUF_MASK))
         || ADSP_FAILED(result = qurt_elite_channel_addq(&vdec_ptr->qurt_elite_channel, (vdec_ptr->modules.vdec_pp_modules.record.rec_buf_q_ptr), VDEC_REC_BUF_MASK))
         || ADSP_FAILED(result = qurt_elite_channel_add_signal(&vdec_ptr->qurt_elite_channel, (vdec_ptr->vtm_sub_unsub_decode_data.signal_ptr), VDEC_DECODE_TICK_MASK))
         || ADSP_FAILED(result = qurt_elite_channel_add_signal(&vdec_ptr->qurt_elite_channel, (vdec_ptr->timing.vtm_sub_unsub_vds_pkt_req_data.signal_end_ptr),VDEC_VDS_PKT_REQ_TICK_END_MASK))
         || ADSP_FAILED(result = qurt_elite_channel_add_signal(&vdec_ptr->qurt_elite_channel, (vdec_ptr->timing.vtm_sub_unsub_vds_pkt_req_data.resync_signal_ptr), VDEC_RESYNC_MASK))
         || ADSP_FAILED(result = qurt_elite_channel_add_signal(&vdec_ptr->qurt_elite_channel, (vdec_ptr->vtm_sub_unsub_decode_data.signal_end_ptr), VDEC_DECODE_TICK_END_MASK))
         || ADSP_FAILED(result = qurt_elite_channel_addq(&vdec_ptr->qurt_elite_channel, (vdec_ptr->resp_q_ptr), VDEC_RESP_MASK)))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "VCP: Failed create Vdec MsgQs or Signals!! session(%lx)", session_num);
      vdec_destroy (vdec_ptr);
      return result;
   }

   // Allocate and queue up the output buffers for main thread
   for (uint32_t i = 0; i < MAX_BUF_Q_ELEMENTS - 1; i++)
   {
      //allocate the databuffer payload (metadata + pcm buffer size)
      int32_t req_size = GET_ELITEMSG_DATABUF_REQ_SIZE(BUFFER_SIZE);
      elite_msg_data_buffer_t* data_payload_ptr = (elite_msg_data_buffer_t*) qurt_elite_memory_malloc (req_size, QURT_ELITE_HEAP_DEFAULT);

      if (!data_payload_ptr)
      {
         MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: Failed to allocate mem for output buffer in vdec_create, session(%lx)", vdec_ptr->session_num);
         vdec_destroy (vdec_ptr);
         return ADSP_ENOMEMORY;
      }

      data_payload_ptr->nActualSize = BUFFER_SIZE;
      data_payload_ptr->nMaxSize = BUFFER_SIZE;

      vdec_ptr->bufs_allocated++;

      if (ADSP_FAILED(result = elite_msg_push_payload_to_returnq (vdec_ptr->buf_q_ptr, (elite_msg_any_payload_t* ) data_payload_ptr)))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "VCP: Failed to fill Vdec buf queue session(%lx)", vdec_ptr->session_num);
         qurt_elite_memory_free (data_payload_ptr);
         vdec_destroy (vdec_ptr);
         return result;
      }
   }

   // Allocate and queue up the  recording output buffers
   for (uint32_t i = 0; i < MAX_REC_BUF_Q_ELEMENTS - 1; i++)
   {
      //allocate the databuffer payload (metadata + pcm buffer size)
      int32_t req_size = GET_ELITEMSG_DATABUF_REQ_SIZE(REC_BUFFER_SIZE);
      elite_msg_data_buffer_t* data_payload_ptr = (elite_msg_data_buffer_t*) qurt_elite_memory_malloc (req_size, QURT_ELITE_HEAP_DEFAULT);

      if (!data_payload_ptr)
      {
         MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: Out of Memory session(%lx)", vdec_ptr->session_num);
         vdec_destroy (vdec_ptr);
         return ADSP_ENOMEMORY;
      }

      data_payload_ptr->nActualSize = REC_BUFFER_SIZE;
      data_payload_ptr->nMaxSize = REC_BUFFER_SIZE;

      vdec_ptr->modules.vdec_pp_modules.record.rec_bufs_allocated++;
      if (ADSP_FAILED(result = elite_msg_push_payload_to_returnq (vdec_ptr->modules.vdec_pp_modules.record.rec_buf_q_ptr, (elite_msg_any_payload_t* ) data_payload_ptr)))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "VCP: Failed to fill Vdec Rec buf queue session(%lx)", vdec_ptr->session_num);
         qurt_elite_memory_free (data_payload_ptr);
         vdec_destroy (vdec_ptr);
         return result;
      }
   }

   // Launch the main thread
   aTHREAD_NAME[3] = ((session_num + 48) & 0xff);   // int32_t to ascii conversion
   if (ADSP_FAILED(
            result = qurt_elite_thread_launch(&(vdec_ptr->svc_handle.threadId), aTHREAD_NAME, NULL, THREAD_STACK_SIZE, ELITETHREAD_DYNA_VOICE_DEC_PRIO, vdec_thread, (void*)vdec_ptr, QURT_ELITE_HEAP_DEFAULT)))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "VCP: Failed create Vdec thread!! session(%lx)", session_num);
      vdec_destroy (vdec_ptr);
      return result;
   }

   *ppHandle = &(vdec_ptr->svc_handle);
   MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: vdec_create end %d session(%lx)", result, session_num);
   return ADSP_EOK;
}

// This must not be called when the threads are active
static void vdec_destroy (vdec_t* vdec_ptr)
{
   if (vdec_ptr)
   {
      uint32_t session_num = vdec_ptr->session_num;
      MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: vdec_destroy begin session(%lx)", session_num);

      // destroy the Host pcm module first
      voice_host_pcm_end (&vdec_ptr->modules.host_pcm_context);

      // free any pending APR packets from modem.
      vdec_clear_data_queue (vdec_ptr);

      // call utility function to destroy data Q
      if (NULL != vdec_ptr->svc_handle.dataQ)
      {
         elite_svc_deinit_data_queue (vdec_ptr->svc_handle.dataQ);
         qurt_elite_memory_free (vdec_ptr->svc_handle.dataQ);
         vdec_ptr->svc_handle.dataQ = NULL;
      }

      // call utility function to destroy resp Q
      if (NULL != vdec_ptr->resp_q_ptr)
      {
         elite_svc_deinit_data_queue (vdec_ptr->resp_q_ptr);
         qurt_elite_memory_free (vdec_ptr->resp_q_ptr);
         vdec_ptr->resp_q_ptr = NULL;
      }

      // call utility function to destroy cmd Q
      if (NULL != vdec_ptr->svc_handle.cmdQ)
      {
         elite_svc_deinit_cmd_queue (vdec_ptr->svc_handle.cmdQ);
         qurt_elite_memory_free (vdec_ptr->svc_handle.cmdQ);
         vdec_ptr->svc_handle.cmdQ = NULL;
      }

      /* free any pending APR packets from vdec in the loopback gpQ */

      if (NULL != vdec_ptr->svc_handle.gpQ)
      {
         vdec_clear_loopback_gpq( vdec_ptr);
         /* now destroy Q */
         qurt_elite_queue_deinit (vdec_ptr->svc_handle.gpQ);
         qurt_elite_memory_free (vdec_ptr->svc_handle.gpQ);
         vdec_ptr->svc_handle.gpQ = NULL;
      }

      // call utility function to destroy buf Q
      if (NULL != vdec_ptr->buf_q_ptr)
      {
         elite_svc_deinit_buf_queue (vdec_ptr->buf_q_ptr, vdec_ptr->bufs_allocated);
         qurt_elite_memory_free (vdec_ptr->buf_q_ptr);
         vdec_ptr->bufs_allocated = 0;
         vdec_ptr->buf_q_ptr = NULL;
      }

      if (NULL != vdec_ptr->vtm_sub_unsub_decode_data.signal_ptr)
      {
         qurt_elite_signal_deinit (vdec_ptr->vtm_sub_unsub_decode_data.signal_ptr);
         qurt_elite_memory_free (vdec_ptr->vtm_sub_unsub_decode_data.signal_ptr);
         vdec_ptr->vtm_sub_unsub_decode_data.signal_ptr = NULL;
      }


      if (NULL != vdec_ptr->timing.vtm_sub_unsub_vds_pkt_req_data.signal_end_ptr)
      {
         qurt_elite_signal_deinit (vdec_ptr->timing.vtm_sub_unsub_vds_pkt_req_data.signal_end_ptr);
         qurt_elite_memory_free (vdec_ptr->timing.vtm_sub_unsub_vds_pkt_req_data.signal_end_ptr);
         vdec_ptr->timing.vtm_sub_unsub_vds_pkt_req_data.signal_end_ptr = NULL;
      }

      if (NULL != vdec_ptr->vtm_sub_unsub_decode_data.signal_end_ptr)
      {
         qurt_elite_signal_deinit (vdec_ptr->vtm_sub_unsub_decode_data.signal_end_ptr);
         qurt_elite_memory_free (vdec_ptr->vtm_sub_unsub_decode_data.signal_end_ptr);
         vdec_ptr->vtm_sub_unsub_decode_data.signal_end_ptr = NULL;
      }

      if (NULL != vdec_ptr->timing.vtm_sub_unsub_vds_pkt_req_data.resync_signal_ptr)
      {
         qurt_elite_signal_deinit (vdec_ptr->timing.vtm_sub_unsub_vds_pkt_req_data.resync_signal_ptr);
         qurt_elite_memory_free (vdec_ptr->timing.vtm_sub_unsub_vds_pkt_req_data.resync_signal_ptr);
         vdec_ptr->timing.vtm_sub_unsub_vds_pkt_req_data.resync_signal_ptr = NULL;
      }

      //record buffer
      if (NULL != vdec_ptr->modules.vdec_pp_modules.record.rec_buf_q_ptr)
      {
         elite_svc_deinit_buf_queue (vdec_ptr->modules.vdec_pp_modules.record.rec_buf_q_ptr, vdec_ptr->modules.vdec_pp_modules.record.rec_bufs_allocated);
         qurt_elite_memory_free (vdec_ptr->modules.vdec_pp_modules.record.rec_buf_q_ptr);
         vdec_ptr->modules.vdec_pp_modules.record.rec_bufs_allocated = 0;
         vdec_ptr->modules.vdec_pp_modules.record.rec_buf_q_ptr = NULL;
      }

      // Destroy the DTMF Detection Module
      voice_dtmf_detect_end (&(vdec_ptr->modules.vdec_pp_modules.dtmf_det.dtmf_det_struct));

      // Destroy CTM Rx
      if (NULL != vdec_ptr->modules.vdec_pp_modules.ctm.ctm_rx_struct.ctm_rx_struct_instance_ptr)
      {
         voice_ctm_rx_end (&vdec_ptr->modules.vdec_pp_modules.ctm.ctm_rx_struct);
         vdec_ptr->modules.vdec_pp_modules.ctm.ctm_rx_struct.ctm_rx_struct_instance_ptr=NULL;
      }

      //Destroy OOB TTY Rx
      voice_oobtty_rx_end (&vdec_ptr->modules.vdec_pp_modules.oobtty.oobtty_rx_struct);

      vdec_pp_allocate_mem_free (vdec_ptr);

      // Destroy the qurt_elite_channel
      qurt_elite_channel_destroy (&vdec_ptr->qurt_elite_channel);

      if( vdec_ptr->voc_type == VSM_MEDIA_TYPE_EVS)
      {
         evs_dec_destroy((void*)vdec_ptr->modules.decoder.dec_state_ptr);
      }

      // Free any pending packet
      if (NULL != vdec_ptr->data_msg_payload_ptr)
      {
         elite_apr_if_free (vdec_ptr->apr_info_ptr->apr_handle, vdec_ptr->data_msg_payload_ptr);
         vdec_ptr->data_msg_payload_ptr = NULL;
      }

      vdec_allocate_mem_free (vdec_ptr);

      qurt_elite_memory_free (vdec_ptr);

      MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: vdec_destroy end session(%lx)", session_num);
   }
   vdec_ptr = NULL;
}

static ADSPResult vdec_thread (void* instance_ptr)
{
   ADSPResult result;
   vdec_t *vdec_ptr = (vdec_t*) instance_ptr;
   uint32_t session_num = vdec_ptr->session_num;
   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: vdec_thread begin session(%lx)", session_num);

   // Enter forever loop
   for (;;)
   {
      // ***************** Wait for the MASK
      // block on any one or more of selected queues and signals
      vdec_ptr->rcvd_mask = qurt_elite_channel_wait (&vdec_ptr->qurt_elite_channel, vdec_ptr->wait_mask);

      while (vdec_ptr->rcvd_mask)
      {
         vdec_ptr->bit_pos = voice_get_signal_pos (vdec_ptr->rcvd_mask);

         // De queue and signal clear done in the handler functions.
         result = pVdecHandler[vdec_ptr->bit_pos] (vdec_ptr);

         if (result != ADSP_EOK)
         {
            if (ADSP_ETERMINATED == result)
            {
               MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: vdec_thread end session(%lx)", session_num);
               return ADSP_EOK;
            }
            MSG_3(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "VCP: Vdec thread Handler Error bit_pos=%ld, res=%d, session=%ld", vdec_ptr->bit_pos, result, session_num);
         }

         vdec_ptr->rcvd_mask = qurt_elite_channel_poll (&vdec_ptr->qurt_elite_channel, vdec_ptr->wait_mask);
      }   // end of while loop
   }   // end of for loop
   return 0;
}


static ADSPResult vdec_vfr_decode_handler (void *instance_ptr)
{
   ADSPResult result = ADSP_EOK;
   vdec_t *vdec_ptr = (vdec_t*) instance_ptr;
   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: vdec_vfr_decode_handler begin session(%lx)", vdec_ptr->session_num);

   qurt_elite_signal_clear (vdec_ptr->vtm_sub_unsub_decode_data.signal_ptr);   // clear the signal
   if (FALSE == vdec_ptr->process_data)
   {
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: Not in RUN state, this should not happen!! session(%lx)", vdec_ptr->session_num);
      return ADSP_EOK;
   }

   if (IN_BAND == vdec_ptr->pkt_xchg.pkt_exchange_mode)
   {
      vdec_process_pkt_data (vdec_ptr);
   }
   else
   {
      vdec_process_oob_pkt_data (vdec_ptr);
   }
   if ( FALSE == vdec_ptr->loopback_enable)
   {
      if (FALSE == vdec_ptr->pkt_xchg.dec_pkt_ready)
      {
         vdec_ptr->modules.decoder.erasure = TRUE;   // if dec pkt didn't arrive, signal erasure
         vdec_ptr->pkt_xchg.pkt_miss_ctr++;   // increment pkt miss counter
         MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: Vdec Pkt miss count from call start is %ld session(%lx)", vdec_ptr->pkt_xchg.pkt_miss_ctr, vdec_ptr->session_num);
      }
   }
   else
   {
      /* Loopback mode....pull mode -> if a packet is in gpQ, process it.  If not, erasure.
       * perform APR free on any packet pulled off queue, since it was created with APR alloc from Encoder
       */
      elite_msg_any_t in_buf_msg;
      memset (&in_buf_msg, 0, sizeof(elite_msg_any_t));

      // ***************** Pull mode from gpQ
      result = qurt_elite_queue_pop_front (vdec_ptr->svc_handle.gpQ, (uint64_t*) &in_buf_msg);
      if (ADSP_FAILED(result))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_LOW_PRIO, "VCP: vdec_process gpQ packet not available session(%lx)", vdec_ptr->session_num);

         vdec_ptr->modules.decoder.erasure = TRUE;   // if dec pkt didn't arrive, signal erasure
         vdec_ptr->pkt_xchg.pkt_miss_ctr++;   // increment pkt miss counter
      }
      else
      {
         MSG_1(MSG_SSID_QDSP6, DBG_LOW_PRIO, "VCP: vdec_process gpQ packet available session(%lx)", vdec_ptr->session_num);
         vdec_ptr->modules.decoder.erasure = FALSE;
         vdec_read_apr_msg (vdec_ptr, &in_buf_msg);
         if (NULL != vdec_ptr->data_msg_payload_ptr)
         {
            vdec_ptr->data_msg_payload_ptr = NULL;
         }

         result = elite_apr_if_free (vdec_ptr->apr_info_ptr->apr_handle, (elite_apr_packet_t *) in_buf_msg.pPayload);
      }
   }
   vdec_ptr->pkt_xchg.dec_pkt_ready = FALSE;   //reset

   //queue next packet request to VDS as previous packet is already read
   //error msgs are printed inside and return result is ignored because
   //process should happen irrespective of it
   (void) vdec_send_dec_pkt_req (vdec_ptr);

   uint64_t time = qurt_elite_timer_get_time ();
   uint64_t cycles = qurt_elite_profile_get_pcycles ();

   result = vdec_process (vdec_ptr);

   time = qurt_elite_timer_get_time () - time;
   cycles = qurt_elite_profile_get_pcycles () - cycles;

   voice_cmn_time_profile_add_data (&vdec_ptr->time_profile_struct[VOICE_DEC_PROFILE_DECODER], (uint32_t) time, (uint32_t) cycles);

   if (result)
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: vdec_process Error, nothing to deliver session(%lx)", vdec_ptr->session_num);
   }

   //dbg: MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: vdec_vfr_decode_handler end session(%lx)",vdec_ptr->vdec_cmn.session_num);
   return result;
}

static ADSPResult vdec_buf_handler (void *instance_ptr)
{
   ADSPResult result = ADSP_EOK;
   vdec_t *vdec_ptr = (vdec_t*) instance_ptr;
   qurt_elite_bufmgr_node_t out_buf_mgr_node;
   //dbg: MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: vdec_buf_handler begin session(%lx)",vdec_ptr->vdec_cmn.session_num);
   vdec_ptr->wait_mask ^= VDEC_BUF_MASK;   // don't listen to output buf
   if (FALSE == vdec_ptr->process_data)
   {
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: vdec_buf_handler: Not in RUN state, this should not happen!! session(%lx)", vdec_ptr->session_num);
      return ADSP_ENOTREADY;
   }
   if (TRUE == vdec_ptr->send_media_type)
   {
      result = vdec_send_mixer_media_type (vdec_ptr);
   }
   else
   {

      // Take next buffer off the Q
      result = qurt_elite_queue_pop_front (vdec_ptr->buf_q_ptr, (uint64_t*) &out_buf_mgr_node);
      if (ADSP_EOK != result)
      {
         MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: Error reading buf queue, result(%d) session(%lx)", result, vdec_ptr->session_num);
      }
      else
      {
         result = vdec_send_out_buf_downstream (vdec_ptr, &out_buf_mgr_node);
      }
   }
   //dbg: MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: vdec_buf_handler end session(%lx)",vdec_ptr->vdec_cmn.session_num);
   return result;
}

static ADSPResult vdec_cmd_handler (void *instance_ptr)
{
   ADSPResult result = ADSP_EOK;
   vdec_t *vdec_ptr = (vdec_t*) instance_ptr;
   elite_msg_any_t msg;
   uint32_t session_num = vdec_ptr->session_num;
   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: vdec_cmd_handler begin session(%lx)", session_num);
   result = qurt_elite_queue_pop_front (vdec_ptr->svc_handle.cmdQ, (uint64_t*) &msg);
   // ***************** Process the msg
   if (ADSP_EOK == result)
   {
      const uint32_t cmdTableSize = sizeof(pEliteHandler) / sizeof(pEliteHandler[0]);
      if (msg.unOpCode >= cmdTableSize)
      {
         MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: Unsupported message ID 0x%8lx!! session(%lx)", msg.unOpCode, session_num);
         return ADSP_EUNSUPPORTED;
      }
      // table lookup to call handling function, with FALSE to indicate processing of msg
      result = pEliteHandler[msg.unOpCode] (instance_ptr, &msg);
   }
   MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: vdec_cmd_handler end result=%d session(%lx)", result, session_num);
   return result;
}

static ADSPResult vdec_custom_msg (void* instance_ptr, elite_msg_any_t* msg_ptr)
{
   ADSPResult result = ADSP_EOK;
   vdec_t* vdec_ptr = (vdec_t*) instance_ptr;
   elite_msg_custom_header_t *pCustom = (elite_msg_custom_header_t *) msg_ptr->pPayload;

   if (pCustom->unSecOpCode < VDEC_NUM_MSGS)
   {
      MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: Vdec_Cmd, SecOpcode: %ld session(%lx)", pCustom->unSecOpCode, vdec_ptr->session_num);
      result = pHandler[pCustom->unSecOpCode] (instance_ptr, msg_ptr);
   }
   else
   {
      MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: Unsupported SecOpcode message ID 0x%8lx!! session(%lx)", pCustom->unSecOpCode, vdec_ptr->session_num);
      result = ADSP_EFAILED;
   }
   return result;
}

static ADSPResult vdec_resync_handler (void* instance_ptr)
{
   ADSPResult result = ADSP_EOK;
   vdec_t* vdec_ptr = (vdec_t*) instance_ptr;
   qurt_elite_signal_clear (vdec_ptr->timing.vtm_sub_unsub_vds_pkt_req_data.resync_signal_ptr);
   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: vdec resync, session(%lx)", vdec_ptr->session_num);
   // Vdec itself doesn't need to do anything to handle resync. It just needs to be
   // propagated to Vdec PP which does need to do some handling
   vdec_ptr->modules.vdec_pp_modules.record.ss_info_counter = 0;
   vdec_ptr->modules.vdec_pp_modules.record.ss_multiframe_counter = 0;
   // reset the device drift counters
   memset (&vdec_ptr->modules.vdec_pp_modules.record.voice_cmn_drift_info_dec_rec, 0, sizeof(vdec_ptr->modules.vdec_pp_modules.record.voice_cmn_drift_info_dec_rec));
   //update sampling rate
   vdec_ptr->modules.vdec_pp_modules.record.voice_cmn_drift_info_dec_rec.nb_sampling_rate_factor = vdec_ptr->samp_rate_factor;
   return result;

}

//This command comes in STOP state which means PP thread is also in STOP state.
//Hence, safe to process in main thread itself and in the end send run cmd to pp thread.
static ADSPResult vdec_run_cmd (void* instance_ptr, elite_msg_any_t* msg_ptr)
{
   vdec_t* vdec_ptr = (vdec_t*) instance_ptr;
   ADSPResult result = ADSP_EOK;
   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: vdec_run_cmd begin session(%lx)", vdec_ptr->session_num);

   if (vdec_ptr->reset_voc_flag)
   {
      if (ADSP_FAILED(result = vdec_voc_init (vdec_ptr)))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: Vdec failed to vdec_voc_init in run() session(%lx)", vdec_ptr->session_num);
         elite_svc_send_ack (msg_ptr, result);
         MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: vdec_run_cmd end result(%d) session(%lx)", result, vdec_ptr->session_num);
         return result;
      }
   }

   /* Enable/Disable LTE TTY for VCO or FULL mode */
   bool_t oobtty_enable = (vdec_ptr->tty_state_ptr->m_oobtty_mode == VSM_TTY_MODE_VCO || vdec_ptr->tty_state_ptr->m_oobtty_mode == VSM_TTY_MODE_FULL);
   if ((vdec_ptr->reset_voc_flag) || (oobtty_enable ^ vdec_ptr->oobtty_enable))
   {
      vdec_ptr->oobtty_enable = oobtty_enable;
      vdec_ptr->oobtty_reset_flag = TRUE;
   }
   // initialize vocoder/tty only when necessary (handover to different media type, or after reinit command)
   // if here,it session is already in STOP state.(RUN or STOP is already is checked in VSM)
   /* handle CDMA TTY/CTM init */
   /* Enable TTY in HCO mode, for CTM negotiation.  Should not effect Rx audio, or 1x TTY */
   bool_t tty_enable = (vdec_ptr->tty_state_ptr->m_etty_mode == VSM_TTY_MODE_VCO || vdec_ptr->tty_state_ptr->m_etty_mode == VSM_TTY_MODE_HCO
         || vdec_ptr->tty_state_ptr->m_etty_mode == VSM_TTY_MODE_FULL);

   if ((tty_enable) && (vdec_ptr->oobtty_enable))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: Error ! TTY enabled for both CS and PS mode : session(%lx)", vdec_ptr->session_num);
      elite_svc_send_ack (msg_ptr, ADSP_EFAILED);
      return ADSP_EFAILED;
   }

   switch (vdec_ptr->voc_type)
   {
      case VSM_MEDIA_TYPE_AMR_NB_MODEM:
      case VSM_MEDIA_TYPE_AMR_NB_IF2:
      case VSM_MEDIA_TYPE_AMR_WB_MODEM:
      case VSM_MEDIA_TYPE_EFR_MODEM:
      case VSM_MEDIA_TYPE_FR_MODEM:
      case VSM_MEDIA_TYPE_HR_MODEM:
      case VSM_MEDIA_TYPE_EAMR:
      case VSM_MEDIA_TYPE_EVS:
         {
            if ((vdec_ptr->reset_voc_flag) || (tty_enable ^ vdec_ptr->ctm_rx_enable))
            {
               vdec_ptr->ctm_rx_enable = tty_enable;
               vdec_ptr->ctm_reset_flag = TRUE;
            }
            break;
         }
      case VSM_MEDIA_TYPE_13K_MODEM:
         {
            if ((vdec_ptr->reset_voc_flag) || (tty_enable ^ vdec_ptr->modules.decoder.tty_option))
            {
               vdec_ptr->modules.decoder.tty_option = tty_enable;
               init_tty_dec (&(vdec_ptr->modules.decoder.tty_dec_struct), (int16_t) vdec_ptr->modules.decoder.tty_option, CDMA_V13K);
            }
            break;
         }
      case VSM_MEDIA_TYPE_EVRC_MODEM:
         {
            if ((vdec_ptr->reset_voc_flag) || (tty_enable ^ vdec_ptr->modules.decoder.tty_option))
            {
               vdec_ptr->modules.decoder.tty_option = tty_enable;
               evrc_init_tty_dec ((void*) vdec_ptr->modules.decoder.dec_state_ptr, (int16_t) vdec_ptr->modules.decoder.tty_option);
            }
            break;
         }
      case VSM_MEDIA_TYPE_4GV_NB_MODEM:
      case VSM_MEDIA_TYPE_4GV_WB_MODEM:
      case VSM_MEDIA_TYPE_4GV_NW_MODEM:
      case VSM_MEDIA_TYPE_4GV_NW:
      case VSM_MEDIA_TYPE_EVRC_NW_2K:
         {
            if ((vdec_ptr->reset_voc_flag) || (tty_enable ^ vdec_ptr->modules.decoder.tty_option))
            {
               vdec_ptr->modules.decoder.tty_option = tty_enable;
               init_tty_dec (&(vdec_ptr->modules.decoder.tty_dec_struct), (int16_t) vdec_ptr->modules.decoder.tty_option, CDMA_FOURGV);
            }
            break;
         }
      default:
         {
            break;   // do nothing
         }
   }

   if(vdec_ptr->voc_type == VSM_MEDIA_TYPE_EVS)  // re-init is required for evs vocoder to update sampling rate
   {
      evs_reinit_dec((void*)vdec_ptr->modules.decoder.dec_state_ptr, vdec_ptr->evs_samp_rate, ((vdec_ptr->vfr_mode == VFR_NONE)? 1:0));
   }

   //VDS returns the signal ptr which is passed to VTM
   result = voice_cmn_send_vds_command (&vdec_ptr->vds.vds_client_id, vdec_ptr->vds.vds_client_token, vdec_ptr->resp_q_ptr, VDS_SUBSCRIBE, &vdec_ptr->timing.vtm_sub_unsub_vds_pkt_req_data.signal_ptr,
         NULL, vdec_ptr->vds.vds_handle_ptr, vdec_ptr->session_num);
   if (ADSP_FAILED(result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "VCP: Failed Vdec subscribe to VDS session(%lx)!", vdec_ptr->session_num);

   }

   if ( NULL == vdec_ptr->timing.vtm_sub_unsub_vds_pkt_req_data.signal_ptr)
   {
      elite_svc_send_ack (msg_ptr, ADSP_EFAILED);
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "VCP: Vdec subscribe to VDS session(%lx) returned null signal ptr!", vdec_ptr->session_num);
      return ADSP_EFAILED;
   }

   //Subscribe to VTM for decode start tick
   vdec_vtm_subscribe (vdec_ptr, &(vdec_ptr->vtm_sub_unsub_decode_data));
   //Subscribe to VTM for pkt req delivery tick for VDS. The tick will directly go to VDS.
   //For VFR NONE case the delivery tick will be based on one shot timer
   vdec_vtm_subscribe (vdec_ptr, &(vdec_ptr->timing.vtm_sub_unsub_vds_pkt_req_data));

   // handle Host Pcm resampler (re)init here if enabled (keep Host Pcm active thru handover if not previously disabled )
   // this reinit is mainly to latch stream parameters such as sampling rate and frame size.
   // if hpcm is disabled, hpcm start on the fly will take care of all the settings
   if ( TRUE == vdec_ptr->modules.host_pcm_context.read_config.enable || TRUE == vdec_ptr->modules.host_pcm_context.write_config.enable)
   {
         result = voice_host_pcm_reinit (&vdec_ptr->modules.host_pcm_context, 1 /*num of channels*/, vdec_ptr->sampling_rate_dec, vdec_ptr->frame_samples_dec);
         if (ADSP_FAILED(result))
         {
            MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: voice_host_pcm_reinit failed !! in vdec_run_cmd, session(%lx)", vdec_ptr->session_num);
            voice_host_pcm_end (&vdec_ptr->modules.host_pcm_context);
            elite_svc_send_ack (msg_ptr, result);
            return result;
         }
         MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: voice_host_pcm_reinit done in vdec_run_cmd, session(%lx)", vdec_ptr->session_num);
      }

   //queue first packet request to VDS. Errors are printed inside and return result
   //is ignored because decode will have to happen anyway.
   (void) vdec_send_dec_pkt_req (vdec_ptr);

   // added run cmd contents from pp thread
   int16_t beamr_enable;

   //Init and in the case of VFR_NONE pre-fill input circ buf with one frame worth of samples
   //result = vdec_init_pp_circbuf(vdec_ptr);

   vdec_init_rec_circbuf (vdec_ptr);   // initializing recording circular buffer in run cmnd to take of sampling rate change due to BBWE enable

   result = vdec_stream_pp_init (vdec_ptr);
   if (ADSP_FAILED(result))
   {
      elite_svc_send_ack (msg_ptr, result);
      MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: voice_stream_pp_init  failed in run cmnd!!  result(%d) session(%lx) ", result, vdec_ptr->session_num);
      return result;
   }
   beamr_enable = (VOICE_BBWE_BEAMR == vdec_ptr->voice_module_bbwe) ? TRUE : FALSE;
   //disable WVE V1 if not NB media type
   int8_t is_nb_flag;
   is_nb_flag = ((voice_get_sampling_rate (vdec_ptr->voc_type, beamr_enable,vdec_ptr->evs_samp_rate) == 8000) ? TRUE:FALSE);

   if ((FALSE == is_nb_flag) && (VOICE_BBWE_WV_V1 == vdec_ptr->voice_module_bbwe))   // !NB and WVE enabled - disable WVE
   {
      vdec_ptr->voice_module_bbwe = VOICE_BBWE_NONE;
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: vdec_run_cmd - disable WVE V1 for WB/SWB, session(%lx)", vdec_ptr->session_num);
   }

   //disable WVE V2 if not NB or WB media type
   int8_t is_nb_wb_flag;
   is_nb_wb_flag = (((voice_get_sampling_rate (vdec_ptr->voc_type, beamr_enable,vdec_ptr->evs_samp_rate) == 8000) || (voice_get_sampling_rate (vdec_ptr->voc_type, beamr_enable,vdec_ptr->evs_samp_rate) == 16000)) ?
         TRUE :
         FALSE);
   if ((FALSE == is_nb_wb_flag) && (VOICE_BBWE_WV_V2 == vdec_ptr->voice_module_bbwe))   // !NB and !WB and WVE V2 enabled - disable WVE V2
   {
      vdec_ptr->voice_module_bbwe = VOICE_BBWE_NONE;
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: vdec_run_cmd - disable WVE V2 for SWB/FB, session(%lx)", vdec_ptr->session_num);
   }
   //send media type to mixer if sampling rate changed
   if (vdec_get_sampling_rate (vdec_ptr->voc_type, vdec_ptr->voice_module_bbwe,vdec_ptr->evs_samp_rate) != vdec_ptr->samp_rate)
   {
      MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: Vdec sampling rate change(%d->%d) session(%lx)", vdec_ptr->samp_rate, vdec_get_sampling_rate (vdec_ptr->voc_type, vdec_ptr->voice_module_bbwe,  vdec_ptr->evs_samp_rate),
            vdec_ptr->session_num);

      vdec_ptr->samp_rate = vdec_get_sampling_rate (vdec_ptr->voc_type, vdec_ptr->voice_module_bbwe,  vdec_ptr->evs_samp_rate);
   }

   vdec_ptr->wv_nb_samp_factor = (vdec_ptr->samp_rate / VOICE_NB_SAMPLING_RATE);
   // init sample slipping algorithm
   voice_sample_slip_init (&(vdec_ptr->modules.vdec_pp_modules.record.ss_struct), (vdec_ptr->samp_rate_factor * VOICE_FRAME_SIZE_NB_10MS),
         VDEC_SS_MULTIFRAME);

   //set the flag to send media type downstream
   vdec_ptr->send_media_type = TRUE;
   vdec_ptr->modules.vdec_pp_modules.record.send_media_type_rec = TRUE;

   //update sampling rate factor for drift detection
   vdec_ptr->modules.vdec_pp_modules.record.voice_cmn_drift_info_dec_rec.nb_sampling_rate_factor = vdec_ptr->samp_rate_factor;

   // Commit CTM (pVenc->fCtmTxEnable = FALSE if CTM off or in 1x vocoder -> should reset CTM state whenever CTM is not enabled going into Run)
   // Assuming we are going Stop->Run at this time
   if (vdec_ptr->ctm_reset_flag)
   {
      voice_ctm_rx_commit_format (&vdec_ptr->modules.vdec_pp_modules.ctm.ctm_rx_struct, voice_get_sampling_rate (vdec_ptr->voc_type, beamr_enable,vdec_ptr->evs_samp_rate));
      result = voice_ctm_rx_resampler_init (&vdec_ptr->modules.vdec_pp_modules.ctm.ctm_rx_struct);
      if (ADSP_FAILED(result))
      {
         MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "VCP: Resampler init for CTM-Rx failed in VDEC, result(%d) session(%lx) ", result, vdec_ptr->session_num);
      }
      else
      {
         voice_ctm_rx_set_enable (&vdec_ptr->modules.vdec_pp_modules.ctm.ctm_rx_struct, vdec_ptr->ctm_rx_enable);
      }
      vdec_ptr->ctm_reset_flag = FALSE;
   }
   if (vdec_ptr->oobtty_reset_flag)
   {
      voice_oobtty_rx_set_config (&vdec_ptr->modules.vdec_pp_modules.oobtty.oobtty_rx_struct, vdec_ptr->oobtty_enable, vdec_ptr->samp_rate);
      /* handle VoLTE TTY init and commit based on sampling rate and mode */
      voice_oobtty_rx_init (&vdec_ptr->modules.vdec_pp_modules.oobtty.oobtty_rx_struct, vdec_ptr->session_num);
      vdec_ptr->oobtty_reset_flag = FALSE;
   }

   //NOTE: reset_voc_flag is used in this thread and also in pp thread before reaching this point.
   //hence setting to false at the end.
   if (vdec_ptr->reset_voc_flag)
   {
      vdec_ptr->reset_voc_flag = FALSE;
   }
   vdec_ptr->process_data = TRUE;   // Start processing - RUN state

   voice_cmn_time_profile_init (&vdec_ptr->time_profile_struct[VOICE_DEC_PROFILE_DECODER]);
   voice_cmn_time_profile_init (&vdec_ptr->time_profile_struct[VOICE_DEC_PROFILE_POST_DEC]);

   elite_svc_send_ack (msg_ptr, result);
   MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: vdec_run_cmd end result(%d) session(%lx)", result, vdec_ptr->session_num);
   return result;
}

//This command comes in STOP state which means PP thread is also in STOP state.
//Kill pp thread first by sending destroy command and do the remaining stuff here.
static ADSPResult vdec_destroy_yourself_cmd (void *instance_ptr, elite_msg_any_t* msg_ptr)
{

   vdec_destroy ((vdec_t*) instance_ptr);

   // send ADSP_ETERMINATED so calling routine knows the destroyer has been invoked.
   return ADSP_ETERMINATED;
}

//This command comes in STOP state which means PP thread is also in STOP state.
//Hence, safe to process in main thread itself.
static ADSPResult vdec_set_media_type (void *instance_ptr, elite_msg_any_t* msg_ptr)
{
   ADSPResult result = ADSP_EOK;
   vdec_t *vdec_ptr = (vdec_t*) instance_ptr;
   elite_msg_data_media_type_header_t* pMediaTypeMsgPayload;

   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: vdec_set_media_type begin session(%lx)", vdec_ptr->session_num);

   /* only handle VocType changes in stop state.  Latch all settings perform init/TTY init/postproc init/postproc
    * sample rate dependent init when transitioning to Run state
    */
   if (TRUE == vdec_ptr->process_data)
   {
      result = ADSP_EBUSY;
      elite_svc_send_ack (msg_ptr, result);
      return result;
   }
   pMediaTypeMsgPayload = (elite_msg_data_media_type_header_t*) msg_ptr->pPayload;

   // set reset flag if we need to reset vocoder.  Latch flag high, only reset when actually processed (Run)
   if (vdec_ptr->voc_type != pMediaTypeMsgPayload->unMediaTypeFormat)
   {
      vdec_ptr->reset_voc_flag = TRUE;

      vdec_ptr->evs_samp_rate = VOICE_FB_SAMPLING_RATE;
      if( vdec_ptr->voc_type == VSM_MEDIA_TYPE_EVS)
      {
         evs_dec_destroy((void*)vdec_ptr->modules.decoder.dec_state_ptr);
      }
      // Media type change and hence reset decoder config params
      (void) vdec_dec_params_init (vdec_ptr);
      if ((VSM_MEDIA_TYPE_4GV_NB_MODEM == vdec_ptr->voc_type) || (VSM_MEDIA_TYPE_4GV_WB_MODEM == vdec_ptr->voc_type) || (VSM_MEDIA_TYPE_4GV_NW_MODEM == vdec_ptr->voc_type)
            || (VSM_MEDIA_TYPE_4GV_NW == vdec_ptr->voc_type) || (VSM_MEDIA_TYPE_EVRC_NW_2K == vdec_ptr->voc_type))
      {
         if ( NULL != vdec_ptr->modules.decoder.fourGV_decode_ptr)
         {
            vdec_ptr->modules.decoder.fourGV_decode_ptr->FGVDecodeDestructor ();
            vdec_ptr->modules.decoder.fourGV_decode_ptr = NULL;
         }
      }

      switch (pMediaTypeMsgPayload->unMediaTypeFormat)
      {
         case VSM_MEDIA_TYPE_EVRC_MODEM:
            {
               if (0 == get_sizeof_evrc_dec_struct ())
               {
                  //This implies this vocoder is not supported!
                  MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: Unsupported media type 0x%lx. Library structure size 0. session(%lx)", pMediaTypeMsgPayload->unMediaTypeFormat, vdec_ptr->session_num);
                  vdec_ptr->voc_type = pMediaTypeMsgPayload->unMediaTypeFormat;
                  result = ADSP_EOK;
               }
               else
               {
                  vdec_ptr->voc_type = pMediaTypeMsgPayload->unMediaTypeFormat;
               }
               break;
            }
         case VSM_MEDIA_TYPE_AMR_NB_MODEM:
         case VSM_MEDIA_TYPE_AMR_NB_IF2:
            {
               if (0 == get_eamr_dec_struct_size ())   // similar api for eAMR and AMR-NB
               {
                  //This implies this vocoder is not supported!
                  MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: Unsupported media type 0x%lx. Library structure size 0. session(%lx)", pMediaTypeMsgPayload->unMediaTypeFormat, vdec_ptr->session_num);
                  vdec_ptr->voc_type = pMediaTypeMsgPayload->unMediaTypeFormat;
                  result = ADSP_EOK;
               }
               else
               {
                  vdec_ptr->voc_type = pMediaTypeMsgPayload->unMediaTypeFormat;
               }
               break;
            }
         case VSM_MEDIA_TYPE_EAMR:
            {
               if (0 == get_eamr_dec_struct_size ())
               {
                  //This implies this vocoder is not supported!
                  MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: Unsupported media type 0x%lx. Library structure size 0. session(%lx)", pMediaTypeMsgPayload->unMediaTypeFormat, vdec_ptr->session_num);
                  vdec_ptr->voc_type = pMediaTypeMsgPayload->unMediaTypeFormat;
                  result = ADSP_EOK;
               }
               else
               {
                  vdec_ptr->voc_type = pMediaTypeMsgPayload->unMediaTypeFormat;
               }
               break;
            }

         case VSM_MEDIA_TYPE_AMR_WB_MODEM:
            {
               if (0 == get_amr_wb_dec_struct_size ())
               {
                  //This implies this vocoder is not supported!
                  MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: Unsupported media type 0x%lx. Library structure size 0. session(%lx)", pMediaTypeMsgPayload->unMediaTypeFormat, vdec_ptr->session_num);
                  vdec_ptr->voc_type = pMediaTypeMsgPayload->unMediaTypeFormat;
                  result = ADSP_EOK;
               }
               else
               {
                  vdec_ptr->voc_type = pMediaTypeMsgPayload->unMediaTypeFormat;
               }
               break;
            }
         case VSM_MEDIA_TYPE_EFR_MODEM:
            {
               if (0 == get_efr_dec_struct_size ())
               {
                  //This implies this vocoder is not supported!
                  MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: Unsupported media type 0x%lx. Library structure size 0. session(%lx)", pMediaTypeMsgPayload->unMediaTypeFormat, vdec_ptr->session_num);
                  vdec_ptr->voc_type = pMediaTypeMsgPayload->unMediaTypeFormat;
                  result = ADSP_EOK;
               }
               else
               {
                  vdec_ptr->voc_type = pMediaTypeMsgPayload->unMediaTypeFormat;
               }
               break;
            }
         case VSM_MEDIA_TYPE_FR_MODEM:
            {
               if (0 == get_fr_dec_struct_size ())
               {
                  //This implies this vocoder is not supported!
                  MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: Unsupported media type 0x%lx. Library structure size 0. session(%lx)", pMediaTypeMsgPayload->unMediaTypeFormat, vdec_ptr->session_num);
                  vdec_ptr->voc_type = pMediaTypeMsgPayload->unMediaTypeFormat;
                  result = ADSP_EOK;
               }
               else
               {
                  vdec_ptr->voc_type = pMediaTypeMsgPayload->unMediaTypeFormat;
               }
               break;
            }

         case VSM_MEDIA_TYPE_HR_MODEM:
            {
               if (0 == gsmhr_get_hr_dec_struct_size ())
               {
                  //This implies this vocoder is not supported!
                  MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: Unsupported media type 0x%lx. Library structure size 0. session(%lx)", pMediaTypeMsgPayload->unMediaTypeFormat, vdec_ptr->session_num);
                  vdec_ptr->voc_type = pMediaTypeMsgPayload->unMediaTypeFormat;
                  result = ADSP_EOK;
               }
               else
               {
                  vdec_ptr->voc_type = pMediaTypeMsgPayload->unMediaTypeFormat;
               }
               break;
            }
         case VSM_MEDIA_TYPE_PCM_8_KHZ:
         case VSM_MEDIA_TYPE_PCM_16_KHZ:
         case VSM_MEDIA_TYPE_PCM_32_KHZ:
         case VSM_MEDIA_TYPE_PCM_44_1_KHZ:
         case VSM_MEDIA_TYPE_PCM_48_KHZ:

            {
               vdec_ptr->voc_type = pMediaTypeMsgPayload->unMediaTypeFormat;
               break;
            }
         case VSM_MEDIA_TYPE_4GV_NB_MODEM:
         case VSM_MEDIA_TYPE_4GV_WB_MODEM:
         case VSM_MEDIA_TYPE_4GV_NW_MODEM:
         case VSM_MEDIA_TYPE_4GV_NW:
         case VSM_MEDIA_TYPE_EVRC_NW_2K:
            {
               if (NULL == vdec_ptr->modules.decoder.dec_state_ptr)
               {
                  //This implies this vocoder is not supported!
                  MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: Unsupported media type 0x%lx. Library structure size 0. session(%lx)", pMediaTypeMsgPayload->unMediaTypeFormat, vdec_ptr->session_num);
                  vdec_ptr->voc_type = pMediaTypeMsgPayload->unMediaTypeFormat;
                  result = ADSP_EOK;
               }
               else
               {
                  init_fourgv_dec_static_mem (vdec_ptr->modules.decoder.dec_state_ptr, &vdec_ptr->modules.decoder.fourGV_decode_ptr);
                  vdec_ptr->voc_type = pMediaTypeMsgPayload->unMediaTypeFormat;
               }
               break;
            }
         case VSM_MEDIA_TYPE_13K_MODEM:
            {
               if (0 == v13k_get_dec_struct_size ())
               {
                  //This implies this vocoder is not supported!
                  MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: Unsupported media type 0x%lx. Library structure size 0. session(%lx)", pMediaTypeMsgPayload->unMediaTypeFormat, vdec_ptr->session_num);
                  vdec_ptr->voc_type = pMediaTypeMsgPayload->unMediaTypeFormat;
                  result = ADSP_EOK;
               }
               else
               {
                  vdec_ptr->voc_type = pMediaTypeMsgPayload->unMediaTypeFormat;
               }
               break;
            }

         case VSM_MEDIA_TYPE_G711_ALAW:
         case VSM_MEDIA_TYPE_G711_MLAW:
         case VSM_MEDIA_TYPE_G711_ALAW_V2:
         case VSM_MEDIA_TYPE_G711_MLAW_V2:
            {
               if (0 == G711DecoderGetSize ())
               {
                  //This implies this vocoder is not supported!
                  MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: Unsupported media type 0x%lx. Library structure size 0. session(%lx)", pMediaTypeMsgPayload->unMediaTypeFormat, vdec_ptr->session_num);
                  vdec_ptr->voc_type = pMediaTypeMsgPayload->unMediaTypeFormat;
                  result = ADSP_EOK;
               }
               else
               {
                  vdec_ptr->voc_type = pMediaTypeMsgPayload->unMediaTypeFormat;
               }
               break;
            }

         case VSM_MEDIA_TYPE_G729AB:
            {
               if (0 == (g729a_get_g729a_dec_struct_size () + g729a_get_g729a_dec_com_struct_size ()))
               {
                  //This implies this vocoder is not supported!
                  MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: Unsupported media type 0x%lx. Library structure size 0. session(%lx)", pMediaTypeMsgPayload->unMediaTypeFormat, vdec_ptr->session_num);
                  vdec_ptr->voc_type = pMediaTypeMsgPayload->unMediaTypeFormat;
                  result = ADSP_EOK;
               }
               else
               {
                  vdec_ptr->voc_type = pMediaTypeMsgPayload->unMediaTypeFormat;
               }
               break;
            }
         case VSM_MEDIA_TYPE_EVS:
            {
               if(0 == get_evs_dec_struct_size())
               {
                  //This implies this vocoder is not supported!
                  MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Unsupported media type 0x%lx. Library structure size 0. session(%lx)",
                        pMediaTypeMsgPayload->unMediaTypeFormat,vdec_ptr->session_num);
                  vdec_ptr->voc_type = pMediaTypeMsgPayload->unMediaTypeFormat;
                  result = ADSP_EOK;
               }
               else
               {
                  vdec_ptr->voc_type = pMediaTypeMsgPayload->unMediaTypeFormat;
               }
               break;
            }
         default:
            {
               MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: Invalid decoder type: 0x%lx session(%lx)", pMediaTypeMsgPayload->unMediaTypeFormat, vdec_ptr->session_num);
               vdec_ptr->voc_type = VSM_MEDIA_TYPE_NONE;
               result = ADSP_EBADPARAM;
               break;
            }
      }

      // Set default operating mode for the media type
      vdec_set_default_op_mode (instance_ptr);
      int16_t beamr_enable = (VOICE_BBWE_BEAMR == vdec_ptr->voice_module_bbwe) ? TRUE : FALSE;

      //latch sampling rate and frame samples after media type is set
      if (VSM_MEDIA_TYPE_NONE != vdec_ptr->voc_type)
      {
         vdec_ptr->sampling_rate_dec = voice_get_sampling_rate (vdec_ptr->voc_type, beamr_enable, vdec_ptr->evs_samp_rate);
         vdec_ptr->samp_rate_factor = (vdec_ptr->sampling_rate_dec / VOICE_NB_SAMPLING_RATE);
         vdec_ptr->frame_samples_dec = VOICE_FRAME_SIZE_NB_20MS * (vdec_ptr->samp_rate_factor);
      }

   }
   //mode notification always sent, even if media type is the same
   vdec_send_mode_notification_v2 (instance_ptr);

   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: vdec_set_media_type end session(%lx)", vdec_ptr->session_num);
   elite_svc_send_ack (msg_ptr, result);
   return result;

}

void vdec_clear_data_queue (vdec_t *vdec_ptr)
{
   qurt_elite_queue_t* dataq_ptr = vdec_ptr->svc_handle.dataQ;
   elite_msg_any_t dataQMsg;
   ADSPResult result;
   // Clear input DataQ
   if (dataq_ptr)
   {
      do
      {
         // Non-blocking MQ receive
         result = qurt_elite_queue_pop_front (dataq_ptr, (uint64_t*) &dataQMsg);

         // return the buffer to its rightful q.
         if (ADSP_EOK == result)
         {
            vdec_ptr->data_msg_payload_ptr = (elite_apr_packet_t *) dataQMsg.pPayload;
            if (NULL != vdec_ptr->data_msg_payload_ptr)
            {
               elite_apr_if_free (vdec_ptr->apr_info_ptr->apr_handle, vdec_ptr->data_msg_payload_ptr);
               vdec_ptr->data_msg_payload_ptr = NULL;
            }
         }
      }
      while (ADSP_EOK == result);
   }

}

//Need to first STOP the PP thread and then do remaining stuff here.
static ADSPResult vdec_stop_cmd (void* instance_ptr, elite_msg_any_t* msg_ptr)
{
   vdec_t* vdec_ptr = (vdec_t*) instance_ptr;
   ADSPResult result = ADSP_EOK;
   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: vdec_stop_cmd begin session(%lx)", vdec_ptr->session_num);

   // add vdec_pp_stop

   (void) vdec_pp_ctrl_params_init (vdec_ptr);

   vdec_ptr->process_data = FALSE;   // Stop processing

   vdec_ptr->wait_mask = VDEC_CMD_TICK_MASK;   // don't listen to output buf

   vdec_vtm_unsubscribe (vdec_ptr, &(vdec_ptr->vtm_sub_unsub_decode_data),
         VDEC_DECODE_TICK_END_MASK, &(vdec_ptr->qurt_elite_channel));
   vdec_vtm_unsubscribe (vdec_ptr, &(vdec_ptr->timing.vtm_sub_unsub_vds_pkt_req_data),
         VDEC_VDS_PKT_REQ_TICK_END_MASK, &(vdec_ptr->qurt_elite_channel));

   //unsubcribe to VDS only after unsubscribe to VTM
   voice_cmn_send_vds_command (&vdec_ptr->vds.vds_client_id, vdec_ptr->vds.vds_client_token, vdec_ptr->resp_q_ptr, VDS_UNSUBSCRIBE, &vdec_ptr->timing.vtm_sub_unsub_vds_pkt_req_data.signal_ptr, NULL,
         vdec_ptr->vds.vds_handle_ptr, vdec_ptr->session_num);

   vdec_clear_data_queue (vdec_ptr);
   vdec_clear_loopback_gpq (vdec_ptr);

   MSG_3(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: Vdec Pkt miss count from call start %ld, total pkts req %ld, session(%lx)", vdec_ptr->pkt_xchg.pkt_miss_ctr, vdec_ptr->pkt_xchg.pkt_req_ctr,
         vdec_ptr->session_num);
   MSG_3(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: Vdec Pkr req delays: Min = %ld, Max= %ld session(%lx)", vdec_ptr->pkt_xchg.min_pkt_req_delay, vdec_ptr->pkt_xchg.max_pkt_req_delay, vdec_ptr->session_num);
   if (vdec_ptr->pkt_xchg.pkt_exchange_mode == OUT_OF_BAND)
   {
      MSG_5(MSG_SSID_QDSP6, DBG_MED_PRIO,
            "VCP: Vdec OOB Pkt miss count from call start %ld, miss due to OOB packet not received in time (%d)," "total pkts req %ld, total packets received %ld session(%lx)",
            vdec_ptr->pkt_xchg.pkt_miss_ctr, (int )vdec_ptr->pkt_xchg.oob_pkt_miss_ctr, vdec_ptr->pkt_xchg.pkt_req_ctr, vdec_ptr->pkt_xchg.oob_pkt_ready_ctr, vdec_ptr->session_num);
   }
   (void) vdec_ctrl_params_init (vdec_ptr);

   // print out profiling stats
   if (vdec_ptr->time_profile_struct[VOICE_DEC_PROFILE_DECODER].num_samples > 0)
   {
      MSG_7(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: vdec_stop session(%lx) voc_type(%x): dec profiler microsec: num_samples(%d) max_time(%d) min_time(%d) avg_time(%d) max_cycles(%d)",
            vdec_ptr->session_num, (int )vdec_ptr->voc_type, (int )vdec_ptr->time_profile_struct[VOICE_DEC_PROFILE_DECODER].num_samples,
            (int )vdec_ptr->time_profile_struct[VOICE_DEC_PROFILE_DECODER].max_time, (int )vdec_ptr->time_profile_struct[VOICE_DEC_PROFILE_DECODER].min_time,
            (int )(vdec_ptr->time_profile_struct[VOICE_DEC_PROFILE_DECODER].total_time / vdec_ptr->time_profile_struct[VOICE_DEC_PROFILE_DECODER].num_samples),
            (int )vdec_ptr->time_profile_struct[VOICE_DEC_PROFILE_DECODER].max_cycles);
   }

   // ack back to msg sender
   elite_svc_send_ack (msg_ptr, result);

   MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: vdec_stop_cmd end result(%d) session(%lx)", result, vdec_ptr->session_num);
   return result;
}

// Foward the command to pp thread, let it handle.
static ADSPResult vdec_connect_dwn_stream_cmd (void* instance_ptr, elite_msg_any_t* msg_ptr)
{
   ADSPResult result = ADSP_EOK;
   vdec_t* vdec_ptr = (vdec_t*) instance_ptr;

   // vdec_pp
   elite_msg_cmd_connect_t* connect_msg_payload_ptr;
   elite_svc_handle_t *svc2Connect_ptr;

   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: vdec_connect_dwn_stream_cmd begin session(%lx)", vdec_ptr->session_num);

   if (vdec_ptr->process_data)
   {
      result = ADSP_ENOTREADY;   // Cannot reconnect in RUN state. todo: needed?
      elite_svc_send_ack (msg_ptr, result);
      return result;
   }

   connect_msg_payload_ptr = (elite_msg_cmd_connect_t*) (msg_ptr->pPayload);
   svc2Connect_ptr = connect_msg_payload_ptr->pSvcHandle;
   MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: Vdec connecting to SvcID 0x%8lx session(%lx)", svc2Connect_ptr->unSvcId, vdec_ptr->session_num);

   // This service only allows one downstream
   if (NULL != vdec_ptr->downstream_peer_ptr)
   {
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: Vdec peer connection failed, already connected session(%lx)", vdec_ptr->session_num);
      result = ADSP_EUNSUPPORTED;
   }
   else   //else accept the connection
   {
      vdec_ptr->downstream_peer_ptr = svc2Connect_ptr;
   }
   elite_svc_send_ack (msg_ptr, result);

   MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: vdec_connect_dwn_stream_cmd end result=%d session(%lx)", result, vdec_ptr->session_num);

   return result;
}

// Forward the command to pp thread, let it handle.
static ADSPResult vdec_disconnect_dwn_stream_cmd (void* instance_ptr, elite_msg_any_t* msg_ptr)
{
   ADSPResult result = ADSP_EOK;
   vdec_t* vdec_ptr = (vdec_t*) instance_ptr;

   elite_msg_cmd_connect_t* disconnect_msg_payload_ptr;
   elite_svc_handle_t *svc2Disconnect_ptr;

   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: vdec_disconnect_dwn_stream_cmd begin session(%lx)", vdec_ptr->session_num);

   disconnect_msg_payload_ptr = (elite_msg_cmd_connect_t*) (msg_ptr->pPayload);
   svc2Disconnect_ptr = disconnect_msg_payload_ptr->pSvcHandle;
   if (svc2Disconnect_ptr != vdec_ptr->downstream_peer_ptr)
   {
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: FAILED, not connected to that service. session(%lx)", vdec_ptr->session_num);
      result = ADSP_EBADPARAM;
   }
   else   //else accept the dis-connection
   {
      vdec_ptr->downstream_peer_ptr = NULL;
   }

   elite_svc_send_ack (msg_ptr, result);
   MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: vdec_disconnect_dwn_stream_cmd end result=%d session(%lx)", result, vdec_ptr->session_num);

   return result;
}

//This command doesn't have anything for main thread. Hence, forward it to pp thread.
static ADSPResult vdec_set_mute_cmd (void* instance_ptr, elite_msg_any_t* msg_ptr)
{
   ADSPResult result = ADSP_EOK;
   vdec_t* vdec_ptr = (vdec_t*) instance_ptr;
   //vdec pp

   elite_msg_custom_voc_set_soft_mute_type *set_mute_cmd_ptr = (elite_msg_custom_voc_set_soft_mute_type *) msg_ptr->pPayload;
   MSG_3(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: vdec_set_soft_mute begin mute(%d) ramp_duration(%ld) session(%lx)", set_mute_cmd_ptr->mute, set_mute_cmd_ptr->ramp_duration, vdec_ptr->session_num);

   if (set_mute_cmd_ptr->mute > VOICE_MUTE)
   {
      result = ADSP_EBADPARAM;
   }
   else if (vdec_ptr->modules.vdec_pp_modules.mute.mute != set_mute_cmd_ptr->mute)
   {
      vdec_ptr->modules.vdec_pp_modules.mute.mute = set_mute_cmd_ptr->mute;
      //We cannot initialize the the mute structure because sampling rate is not decided yet.
      //final sampling rate is known only in RUN/vdec_process, so deferring the init until process
      //and copying the cmd payload into local copy.
      vdec_ptr->modules.vdec_pp_modules.mute.set_param_cached.mute = set_mute_cmd_ptr->mute;
      vdec_ptr->modules.vdec_pp_modules.mute.set_param_cached.ramp_duration = set_mute_cmd_ptr->ramp_duration;
      vdec_ptr->modules.vdec_pp_modules.mute.new_mute_cmd = TRUE;
   }

   elite_svc_send_ack (msg_ptr, result);

   MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: vdec_set_soft_mute end result(%d) session(%lx)", result, vdec_ptr->session_num);

   return result;
}

static ADSPResult vdec_get_kpps_cmd (void* instance_ptr, elite_msg_any_t* msg_ptr)
{
   ADSPResult result = ADSP_EFAILED;
   vdec_t* vdec_ptr = (vdec_t*) instance_ptr;
   elite_msg_custom_kpps_type *kpps_msg_ptr = (elite_msg_custom_kpps_type*) msg_ptr->pPayload;
   vsm_get_kpps_ack_t* kpps_ptr = (vsm_get_kpps_ack_t*) kpps_msg_ptr->pnKpps;
   uint32_t kpps_changed = FALSE;

   if (kpps_ptr)
   {
      kpps_ptr->vdec_kpps = 0;   // Initialization
      result = vdec_aggregate_modules_kpps (instance_ptr, &kpps_changed);

      if (ADSP_SUCCEEDED(result))
      {
         kpps_ptr->vdec_kpps = vdec_ptr->modules.aggregate_kpps;
         kpps_ptr->vdecpp_kpps = 0;
      }
   }
   elite_svc_send_ack (msg_ptr, result);

   return ADSP_EOK;
}

static ADSPResult vdec_get_delay_cmd (void* instance_ptr, elite_msg_any_t* msg_ptr)
{
   ADSPResult result = ADSP_EFAILED;
   vdec_t* vdec_ptr = (vdec_t*) instance_ptr;
   elite_msg_custom_delay_type *delay_msg_ptr = (elite_msg_custom_delay_type*) msg_ptr->pPayload;
   vsm_get_delay_ack_t* delay_ptr = (vsm_get_delay_ack_t*) delay_msg_ptr->delay_ptr;

   if (delay_ptr)
   {
      delay_ptr->vdec_delay = 0;   // Initialization
      result = vdec_aggregate_modules_delay (instance_ptr);

      if (ADSP_SUCCEEDED(result))
      {
         delay_ptr->vdec_delay = vdec_ptr->modules.aggregate_delay;
      }
   }

   elite_svc_send_ack (msg_ptr, result);

   return ADSP_EOK;
}

//This command comes in STOP state which means PP thread is also in STOP state.
//Hence, no need to forward this cmd to PP thread, safe to process in main thread.
static ADSPResult vdec_set_timing_cmd (void* instance_ptr, elite_msg_any_t* msg_ptr)
{
   ADSPResult result = ADSP_EOK;
   vdec_t* vdec_ptr = (vdec_t*) instance_ptr;
   elite_msg_custom_voc_timing_param_type *set_timing_cmd_ptr = (elite_msg_custom_voc_timing_param_type *) msg_ptr->pPayload;
   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: vdec_set_timing_cmd begin  session(%lx)", vdec_ptr->session_num);
   {   //todo: don't allow vfr in run state
      voice_set_timing_params_t* vfr_cmd_ptr = (voice_set_timing_params_t*) set_timing_cmd_ptr->param_data_ptr;
      MSG_4(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: VOICE_SET_TIMING_PARAMS, dec vfr_mode(%x), dec_req_offset(%x), dec_offset(%x), process_data(%x)", vfr_cmd_ptr->mode, vfr_cmd_ptr->dec_req_offset,
            vfr_cmd_ptr->dec_offset, vdec_ptr->process_data);
      if (FALSE == vdec_ptr->process_data)
      {
         if (VFR_HARD_EXT >= vfr_cmd_ptr->mode)
         {
            vdec_ptr->vfr_mode = vfr_cmd_ptr->mode;
         }
         else
         {
            vdec_ptr->vfr_mode = VFR_NONE;
            MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: VOICE_SET_TIMING_PARAMS, Invalid dec vfr_mode(%x),setting vfr_mode to VFR_NONE", vfr_cmd_ptr->mode);
            result = ADSP_EBADPARAM;
         }
         // vdec_ptr->timing.vtm_sub_unsub_pkt_req_data.vfr_mode = (uint8_t) vdec_ptr->vfr_mode;
         vdec_ptr->timing.vtm_sub_unsub_vds_pkt_req_data.vfr_mode = (uint8_t) vdec_ptr->vfr_mode;
         vdec_ptr->vtm_sub_unsub_decode_data.vfr_mode = (uint8_t) vdec_ptr->vfr_mode;

         vdec_ptr->vfr_source = voice_cmn_get_vfr_source (vdec_ptr->vfr_mode);

         // MIN_TIMER_OFFSET is set to 0, so no need to check if offset is below min
         if ((MAX_TIMER_OFFSET < vfr_cmd_ptr->dec_req_offset) || (MAX_TIMER_OFFSET < vfr_cmd_ptr->dec_offset))
         {
            //  vdec_ptr->timing.vtm_sub_unsub_pkt_req_data.offset = 3300;   // default 3.3 ms
            vdec_ptr->timing.vtm_sub_unsub_vds_pkt_req_data.offset = 3300;   // default 3.3 ms
            vdec_ptr->vtm_sub_unsub_decode_data.offset = 8300;   // default 8.3ms
            MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: VOICE_SET_TIMING_PARAMS, Invalid dec_req_offset(%x), dec_offset(%x), Defaulting to 3.3msec, 8.3msec", vfr_cmd_ptr->dec_req_offset,
                  vfr_cmd_ptr->dec_offset);
            result = ADSP_EOK;   // since ISOD/API says we support zero offsets, ack EOK and return EOK.
            // just printing message is good enough.  Changed the MSG to error to hightlight
         }
         else
         {
            //vdec_ptr->timing.vtm_sub_unsub_pkt_req_data.offset = vfr_cmd_ptr->dec_req_offset;   // default 3.3 ms
            vdec_ptr->timing.vtm_sub_unsub_vds_pkt_req_data.offset = vfr_cmd_ptr->dec_req_offset;
            vdec_ptr->vtm_sub_unsub_decode_data.offset = vfr_cmd_ptr->dec_offset;   // default 8.3ms
         }
         //update version of timing used
         //vdec_ptr->timing.vtm_sub_unsub_pkt_req_data.timing_ver =
         //       VFR_CLIENT_INFO_VER_1;
         vdec_ptr->timing.vtm_sub_unsub_vds_pkt_req_data.timing_ver =
            VFR_CLIENT_INFO_VER_1;
         vdec_ptr->vtm_sub_unsub_decode_data.timing_ver = VFR_CLIENT_INFO_VER_1;

         elite_svc_send_ack (msg_ptr, result);

      }
      else
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: Timing can't be changed in RUN session(%lx)", vdec_ptr->session_num);
         result = ADSP_EBUSY;
         elite_svc_send_ack (msg_ptr, result);
      }
   }

   MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: vdec_set_timing_cmd end result(%d) session(%lx)", result, vdec_ptr->session_num);
   return result;
}

//This command comes in STOP state, however for Dec PP offset,
// the command needs to be forwarded to PP thread as well.
static ADSPResult vdec_set_timingv2_cmd (void* instance_ptr, elite_msg_any_t* msg_ptr)
{
   ADSPResult result = ADSP_EOK;
   vdec_t* vdec_ptr = (vdec_t*) instance_ptr;
   elite_msg_custom_voc_timing_param_type *set_timing_cmd_ptr = (elite_msg_custom_voc_timing_param_type *) msg_ptr->pPayload;
   vsm_set_timing_params_t* vfr_cmd_ptr = (vsm_set_timing_params_t*) set_timing_cmd_ptr->param_data_ptr;
   MSG_4(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: VSM_CMD_SET_TIMING_PARAMS Begin, dec vfr_mode(%x), dec_req_offset(%x), dec_offset(%x), process_data(%x)", vfr_cmd_ptr->mode, vfr_cmd_ptr->dec_req_offset,
         vfr_cmd_ptr->dec_offset, vdec_ptr->process_data);

   // Verify stop state of the thread.
   if (FALSE == vdec_ptr->process_data)
   {
      // Verify validity of VFR mode
      if (VFR_HARD_EXT >= vfr_cmd_ptr->mode)
      {
         vdec_ptr->vfr_mode = vfr_cmd_ptr->mode;
      }
      else
      {
         vdec_ptr->vfr_mode = VFR_NONE;
         MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: VSM_CMD_SET_TIMING_PARAMS, Invalid dec vfr_mode(%x),setting vfr_mode to VFR_NONE", vfr_cmd_ptr->mode);
         result = ADSP_EBADPARAM;
      }

      // Update VFR modes everywhere applicable.
      // vdec_ptr->timing.vtm_sub_unsub_pkt_req_data.vfr_mode = (uint8_t) vdec_ptr->vfr_mode;
      vdec_ptr->timing.vtm_sub_unsub_vds_pkt_req_data.vfr_mode = (uint8_t) vdec_ptr->vfr_mode;
      vdec_ptr->vtm_sub_unsub_decode_data.vfr_mode = (uint8_t) vdec_ptr->vfr_mode;

      vdec_ptr->vfr_source = voice_cmn_get_vfr_source (vdec_ptr->vfr_mode);

      // MIN_TIMER_OFFSET is set to 0, so no need to check if offset is below min
      if ((MAX_TIMER_OFFSET < vfr_cmd_ptr->dec_req_offset) || (MAX_TIMER_OFFSET < vfr_cmd_ptr->dec_offset))
      {
         //  vdec_ptr->timing.vtm_sub_unsub_pkt_req_data.offset = 3300;   // default 3.3 ms
         vdec_ptr->timing.vtm_sub_unsub_vds_pkt_req_data.offset = 3300;   // default 3.3 ms
         vdec_ptr->vtm_sub_unsub_decode_data.offset = 8300;   // default 8.3ms
         MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: VSM_CMD_SET_TIMING_PARAMS, Invalid dec_req_offset(%x), dec_offset(%x), Defaulting to 3.3msec, 8.3msec", vfr_cmd_ptr->dec_req_offset,
               vfr_cmd_ptr->dec_offset);
         result = ADSP_EOK;   // since ISOD/API says we support zero offsets, ack EOK and return EOK.
         // just printing message is good enough.  Changed the MSG to error to hightlight
      }
      else
      {
         // vdec_ptr->timing.vtm_sub_unsub_pkt_req_data.offset = vfr_cmd_ptr->dec_req_offset;   // default 3.3 ms
         vdec_ptr->timing.vtm_sub_unsub_vds_pkt_req_data.offset = vfr_cmd_ptr->dec_req_offset;
         vdec_ptr->vtm_sub_unsub_decode_data.offset = vfr_cmd_ptr->dec_offset;   // default 8.3ms
      }

      //update version of timing used
      //vdec_ptr->timing.vtm_sub_unsub_pkt_req_data.timing_ver =
      //       VFR_CLIENT_INFO_VER_1;
      vdec_ptr->timing.vtm_sub_unsub_vds_pkt_req_data.timing_ver =
         VFR_CLIENT_INFO_VER_1;
      vdec_ptr->vtm_sub_unsub_decode_data.timing_ver = VFR_CLIENT_INFO_VER_1;

      elite_svc_send_ack (msg_ptr, result);

   }

   else
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: VFR mode can't be changed in RUN session(%lx)", vdec_ptr->session_num);
      result = ADSP_EBUSY;
      elite_svc_send_ack (msg_ptr, result);
   }

   return result;
}

static ADSPResult vdec_set_pkt_exchange_mode (void* instance_ptr, elite_msg_any_t* msg_ptr)
{
   ADSPResult result = ADSP_EOK;
   vdec_t* vdec_ptr = (vdec_t*) instance_ptr;
   elite_msg_custom_pkt_exchange_mode_type *set_pkt_exchange_mode_ptr = (elite_msg_custom_pkt_exchange_mode_type *) msg_ptr->pPayload;
   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: vdec_set_pkt_exchange_mode begin session(%lx)", vdec_ptr->session_num);

   if (FALSE == vdec_ptr->process_data)
   {
      MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: VDEC_SET_PKT_EXCHANGE_MODE ,pkt_exchange_mode(%d) session(%lx)", (int )set_pkt_exchange_mode_ptr->pkt_exchange_mode, vdec_ptr->session_num);
      vdec_ptr->pkt_xchg.pkt_exchange_mode = set_pkt_exchange_mode_ptr->pkt_exchange_mode;
      /* make packet exchange pointer NULL as config command with new addresses must follow a mode command */
      vdec_ptr->pkt_xchg.oob_pkt_exchange_ptr = NULL;
   }
   else
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: pkt_exchange_mode mode can't be changed in RUN session(%lx)", vdec_ptr->session_num);
      result = ADSP_EBUSY;
   }
   elite_svc_send_ack (msg_ptr, result);
   return result;

}

static ADSPResult vdec_set_oob_pkt_exchange_config (void* instance_ptr, elite_msg_any_t* msg_ptr)
{
   ADSPResult result = ADSP_EOK;
   vdec_t* vdec_ptr = (vdec_t*) instance_ptr;
   elite_msg_custom_oob_pkt_exchange_config_type *set_oob_pkt_exchange_config_ptr = (elite_msg_custom_oob_pkt_exchange_config_type *) msg_ptr->pPayload;
   vsm_config_packet_exchange_t *pkt_exchange_config_ptr = (vsm_config_packet_exchange_t*) set_oob_pkt_exchange_config_ptr->param_data_ptr;

   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: vdec_set_pkt_exchange_config begin session(%lx)", vdec_ptr->session_num);

   if (TRUE == vdec_ptr->process_data)
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: oob_pkt_exchange_config can't be come in RUN session(%lx)", vdec_ptr->session_num);
      result = ADSP_EBUSY;
   }

   else
   {
      vdec_ptr->pkt_xchg.memmap_vdec_oob.unMemMapClient = vsm_memory_map_client;
      vdec_ptr->pkt_xchg.memmap_vdec_oob.unMemMapHandle = pkt_exchange_config_ptr->mem_handle;
      result = elite_mem_map_get_shm_attrib (pkt_exchange_config_ptr->dec_buf_addr_lsw, pkt_exchange_config_ptr->dec_buf_addr_msw, pkt_exchange_config_ptr->dec_buf_size,
            &vdec_ptr->pkt_xchg.memmap_vdec_oob);
      if (ADSP_FAILED(result))
      {
         MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: Error! Mapping to virtual address for OOB pkt exchange failed for DECODER %d! session(%lx)", result, vdec_ptr->session_num);
         vdec_ptr->pkt_xchg.oob_pkt_exchange_ptr = NULL;
      }
      else
      {
         vdec_ptr->pkt_xchg.oob_pkt_exchange_ptr = (int32_t *) vdec_ptr->pkt_xchg.memmap_vdec_oob.unVirtAddr;
         MSG_3(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: VDEC_SET_OOB_PKT_EXCHANGE_CONFIG ,vdec OOB address(%x) size (%d) session(%lx)", (unsigned int )vdec_ptr->pkt_xchg.oob_pkt_exchange_ptr,
               (int )pkt_exchange_config_ptr->dec_buf_size, vdec_ptr->session_num);
      }
   }
   elite_svc_send_ack (msg_ptr, result);
   return result;

}
//This command comes in STOP state which means PP thread is also in STOP state.
//Hence, no need to forward this cmd to PP thread, safe to process in main thread.
static ADSPResult vdec_reinit_cmd (void* instance_ptr, elite_msg_any_t* msg_ptr)
{
   ADSPResult result = ADSP_EOK;
   vdec_t* vdec_ptr = (vdec_t*) instance_ptr;
   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: vdec_reinit_cmd begin  session(%lx)", vdec_ptr->session_num);
   {
      if (FALSE == vdec_ptr->process_data)
      {
         MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: vdec_reinit_cmd process  session(%lx)", vdec_ptr->session_num);

         // Must reset vocoder on reinit command
         vdec_ptr->reset_voc_flag = TRUE;

         (void) vdec_init (vdec_ptr);
         if (NULL != vdec_ptr->modules.decoder.fourGV_decode_ptr)
         {
            vdec_ptr->modules.decoder.fourGV_decode_ptr->FGVDecodeDestructor ();
            vdec_ptr->modules.decoder.fourGV_decode_ptr = NULL;
         }

         //vdec_pp
         result = vdec_pp_reinit (vdec_ptr);
      }
      else
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: Vdec reinit can't be done in RUN session(%lx)", vdec_ptr->session_num);
         result = ADSP_EBUSY;
      }
   }

   elite_svc_send_ack (msg_ptr, result);

   MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: vdec_reinit_cmd end result(%d) session(%lx)", result, vdec_ptr->session_num);
   return result;
}

//This command doesn't have anything for main thread. Hence, forward it to pp thread.
static ADSPResult vdec_start_record_cmd (void* instance_ptr, elite_msg_any_t* msg_ptr)
{
   ADSPResult result = ADSP_EOK;
   vdec_t* vdec_ptr = (vdec_t*) instance_ptr;

   elite_msg_custom_voc_svc_connect_record_type* connect_msg_payload_ptr;
   elite_svc_handle_t *svc_2_connect_ptr;
   connect_msg_payload_ptr = (elite_msg_custom_voc_svc_connect_record_type*) (msg_ptr->pPayload);

   MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: vdec_start_record_cmd opcode(%#lx) session(%#lx)", connect_msg_payload_ptr->sec_opcode, vdec_ptr->session_num);

   svc_2_connect_ptr = connect_msg_payload_ptr->svc_handle_ptr;

   // This service only allows one downstream
   if (NULL != vdec_ptr->modules.vdec_pp_modules.record.afe_handle_ptr)
   {
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "FAILED, only 1 peer allowed session(%lx)", vdec_ptr->session_num);
      result = ADSP_EUNSUPPORTED;
   }
   else   //else accept the connection
   {
      vdec_ptr->modules.vdec_pp_modules.record.afe_handle_ptr = svc_2_connect_ptr;
      vdec_ptr->modules.vdec_pp_modules.record.enable_recording = TRUE;
      vdec_ptr->modules.vdec_pp_modules.record.recording_mode = connect_msg_payload_ptr->record_mode;
      vdec_ptr->modules.vdec_pp_modules.record.rec_first_frame = TRUE;
      vdec_ptr->modules.vdec_pp_modules.record.aud_ref_port = connect_msg_payload_ptr->aud_ref_port;
      vdec_ptr->modules.vdec_pp_modules.record.ss_info_counter = 0;   // reset the sample slip counter
      vdec_ptr->modules.vdec_pp_modules.record.ss_multiframe_counter = 0;   // reset multiframe counter
      vdec_ptr->modules.vdec_pp_modules.record.send_media_type_rec = TRUE;
      // vtm parameters
      vdec_ptr->modules.vdec_pp_modules.record.afe_drift_ptr = connect_msg_payload_ptr->afe_drift_ptr;   // afe drift pointer is stored only at 1 location, hence not in the if condition of vfr_hard

      vdec_init_rec_circbuf (vdec_ptr);

      // reset the hptimer vs device drift counter
      memset (&vdec_ptr->modules.vdec_pp_modules.record.voice_cmn_drift_info_dec_rec, 0, sizeof(vdec_ptr->modules.vdec_pp_modules.record.voice_cmn_drift_info_dec_rec));
      //update sampling rate
      vdec_ptr->modules.vdec_pp_modules.record.voice_cmn_drift_info_dec_rec.nb_sampling_rate_factor = vdec_ptr->samp_rate_factor;
   }

   elite_svc_send_ack (msg_ptr, result);

   //dbg: MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO, "vdec_start_record_cmd end result=%x session(%lx)",result,vdec_ptr->vdec_cmn.session_num);
   return result;
}

//This command doesn't have anything for main thread. Hence, forward it to pp thread.
static ADSPResult vdec_stop_record_cmd (void* instance_ptr, elite_msg_any_t* msg_ptr)
{
   ADSPResult result = ADSP_EOK;
   vdec_t* vdec_ptr = (vdec_t*) instance_ptr;

   elite_msg_custom_voc_svc_connect_type* connect_msg_payload_ptr;
   elite_svc_handle_t *svc_2_connect_ptr;

   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: vdec_stop_record_cmd session(%lx)", vdec_ptr->session_num);

   connect_msg_payload_ptr = (elite_msg_custom_voc_svc_connect_type*) (msg_ptr->pPayload);
   if (VDEC_STOP_RECORD_CMD != connect_msg_payload_ptr->sec_opcode)
   {
      result = ADSP_EBADPARAM;
      elite_svc_send_ack (msg_ptr, result);
      return result;
   }

   svc_2_connect_ptr = NULL;

   // This service only allows one downstream
   vdec_ptr->modules.vdec_pp_modules.record.afe_handle_ptr = svc_2_connect_ptr;
   vdec_ptr->modules.vdec_pp_modules.record.enable_recording = FALSE;
   vdec_ptr->modules.vdec_pp_modules.record.recording_mode = 0;
   voice_circbuf_reset (&(vdec_ptr->modules.vdec_pp_modules.record.circ_struct_decout_rec));

   vdec_ptr->modules.vdec_pp_modules.record.afe_drift_ptr = NULL;

   vdec_ptr->wait_mask &= ~(VDEC_REC_BUF_MASK);   // Once stopped, disable listening to Record buffers

   elite_svc_send_ack (msg_ptr, result);

   //dbg: MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO, "vdec_stop_record_cmd end result=%x session(%lx)",result,vdec_ptr->vdec_cmn.session_num);
   return result;
}

static ADSPResult vdec_beamr_set_param (vdec_t* vdec_ptr, int8_t *cal_payload, uint32_t pid, uint16 param_size)
{
   ADSPResult result = ADSP_EOK;
   int16_t beamr_flag;

   switch (pid)
   {
      case VOICE_PARAM_MOD_ENABLE:
         {
            if (sizeof(int32_t) != param_size)
            {
               MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: vdec_beamr_set_param():: Error!! Bad Param Size, session(%#lx)", vdec_ptr->session_num);
               result = ADSP_EBADPARAM;
               break;
            }

            //expect set param for BeAMR only when other BBWE modules are not enabled.
            if ((VOICE_BBWE_NONE != vdec_ptr->voice_module_bbwe) && (VOICE_BBWE_BEAMR != vdec_ptr->voice_module_bbwe))
            {
               MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "vdec_beamr_set_param():: Error!! trying to enable/disable BeAMR while other BBWE module is alredy enabled,BBWE_flag(%d) session(%#lx)",
                     (int )vdec_ptr->voice_module_bbwe, vdec_ptr->session_num);
               result = ADSP_EBADPARAM;
               break;
            }

            beamr_flag = *((int16_t*) cal_payload);
            if ((VSM_MEDIA_TYPE_AMR_NB_IF2 == vdec_ptr->voc_type) || (VSM_MEDIA_TYPE_AMR_NB_MODEM == vdec_ptr->voc_type))
            {
               //Beamr set param for AMR-NB is not supported in RUN state
               if (TRUE == vdec_ptr->process_data)
               {
                  MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "vdec_beamr_set_param():: BeAMR enable/disable is not supported in RUN state for AMR-NB vocoder, session(%#lx)", vdec_ptr->session_num);
                  result = ADSP_EBADPARAM;
                  break;
               }

               if ((VOICE_BBWE_BEAMR == vdec_ptr->voice_module_bbwe) && (beamr_flag))
               {
                  MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: vdec_beamr_set_param():: BeAMR is already enabled. Ignoring. session(%#lx)", vdec_ptr->session_num);
                  break;
               }

               if (TRUE == beamr_flag)
               {
                  vdec_ptr->reset_voc_flag = TRUE;   //TTY will also be reinitialized.
                  vdec_ptr->voice_module_bbwe = VOICE_BBWE_BEAMR;
                  MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: vdec_beamr_set_param():: BeAMR enabled for AMR-NB. voc_reset_flag is enabled.session(%#lx)", vdec_ptr->session_num);
               }
               else
               {
                  vdec_ptr->voice_module_bbwe = VOICE_BBWE_NONE;
               }
            }
            else if (VSM_MEDIA_TYPE_EAMR == vdec_ptr->voc_type)
            {
               vdec_ptr->voice_module_bbwe = (TRUE == beamr_flag) ? VOICE_BBWE_BEAMR : VOICE_BBWE_NONE;
            }

            //latch sampling rate after BeAMR flag change
            vdec_ptr->sampling_rate_dec = voice_get_sampling_rate (vdec_ptr->voc_type, (VOICE_BBWE_BEAMR == vdec_ptr->voice_module_bbwe), vdec_ptr->evs_samp_rate);
            vdec_ptr->samp_rate_factor = (vdec_ptr->sampling_rate_dec / VOICE_NB_SAMPLING_RATE);
            vdec_ptr->frame_samples_dec = VOICE_FRAME_SIZE_NB_20MS * (vdec_ptr->samp_rate_factor);

            MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: vdec_beamr_set_param():: BBWE flag(%d), session(%#lx)", (int )vdec_ptr->voice_module_bbwe, vdec_ptr->session_num);
         }
         break;
      default:
         {
            MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: vdec_beamr_set_param():: Error!! Bad Param id. session(%#lx)", vdec_ptr->session_num);
            result = ADSP_EBADPARAM;
         }
         break;
   }
   return result;
}

static ADSPResult vdec_beamr_get_param (vdec_t* vdec_ptr, int8_t *cal_payload, uint32_t pid, uint16 buffer_size, uint16_t *param_size)
{
   ADSPResult result = ADSP_EOK;

   switch (pid)
   {
      case VOICE_PARAM_MOD_ENABLE:
         {
            if (sizeof(int32_t) > buffer_size)
            {
               MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: vdec_beamr_get_param():: Error!! Bad Param Size. session(%#lx)", vdec_ptr->session_num);
               result = ADSP_EBADPARAM;
               break;
            }
            *param_size = sizeof(int32_t);

            *((int32_t*) cal_payload) = 0;   // Clearing the whole buffer
            *((int16_t*) cal_payload) = (VOICE_BBWE_BEAMR == vdec_ptr->voice_module_bbwe) ? TRUE : FALSE;
         }
         break;
      default:
         {
            MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: vdec_beamr_get_param():: Error!! Bad Param id. session(%#lx)", vdec_ptr->session_num);
            result = ADSP_EBADPARAM;
         }
         break;
   }
   return result;
}

static ADSPResult vdec_set_param_int (vdec_t* vdec_ptr, uint32_t module_id, uint32_t param_id, void* payload_address_in, uint32_t param_size)
{
   ADSPResult result = ADSP_EOK;
   int8_t* calibration_data_pay_load_ptr = (int8_t*) payload_address_in;
   switch (module_id)
   {
      case VOICESTREAM_MODULE_TX:   // listen for Tx module so it's triggered off a single module/param combo.  User sees as putting Tx in loopback
         {
            /* handle loopback param */
            if ( VOICE_PARAM_LOOPBACK_ENABLE == param_id)
            {
               int16_t enable_flag = *((int16_t*) calibration_data_pay_load_ptr);
               if (sizeof(int32_t) != param_size)
               {
                  result = ADSP_EBADPARAM;
               }
               else
               {
                  if ( TRUE == enable_flag)
                  {
                     vdec_ptr->loopback_enable = TRUE;
                     MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: vdec_set_param_cmd Venc->Vdec Loopback Enable session(%lx)", vdec_ptr->session_num);
                  }
                  else
                  {
                     vdec_ptr->loopback_enable = FALSE;

                     /* if loopback is going off, clear out any queued APR packets and free */
                     vdec_clear_loopback_gpq (vdec_ptr);

                     MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: vdec_set_param_cmd Venc->Vdec Loopback Disable session(%lx)", vdec_ptr->session_num);
                  }
                  result = ADSP_EOK;
               }
            }
         }
         break;
      case VOICE_MODULE_BEAMR:
         {
            int16_t beamr_enable = *((int16_t*) calibration_data_pay_load_ptr);

            //if BeAMR is enabled and setparam is for disable.
            if ((FALSE == beamr_enable) && (vdec_ptr->voice_module_bbwe == VOICE_BBWE_BEAMR || vdec_ptr->voice_module_bbwe == VOICE_BBWE_NONE))
            {
               vdec_ptr->voice_module_bbwe = VOICE_BBWE_NONE;

               //latch sampling rate after BeAMR flag change
               vdec_ptr->sampling_rate_dec = voice_get_sampling_rate (vdec_ptr->voc_type, (VOICE_BBWE_BEAMR == vdec_ptr->voice_module_bbwe), vdec_ptr->evs_samp_rate);
               vdec_ptr->samp_rate_factor = (vdec_ptr->sampling_rate_dec / VOICE_NB_SAMPLING_RATE);
               vdec_ptr->frame_samples_dec = VOICE_FRAME_SIZE_NB_20MS * (vdec_ptr->samp_rate_factor);

               MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: vdec_beamr_set_param():: disabling BeAMR for mediatype(%x), session(%#lx)", (int )vdec_ptr->voc_type, vdec_ptr->session_num);
               result = ADSP_EOK;
            }
            else if ((VSM_MEDIA_TYPE_AMR_NB_IF2 == vdec_ptr->voc_type) || (VSM_MEDIA_TYPE_AMR_NB_MODEM == vdec_ptr->voc_type) || (VSM_MEDIA_TYPE_EAMR == vdec_ptr->voc_type))
            {
               result = vdec_beamr_set_param (vdec_ptr, calibration_data_pay_load_ptr, param_id, param_size);
            }
            else
            {
               result = ADSP_EUNSUPPORTED;
               MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: vdec_beamr_set_param():: BeAMR enable is not supported for mediatype(%x). Ignoring, session(%#lx)", (int )vdec_ptr->voc_type,
                     vdec_ptr->session_num);
            }
            break;
         }
      case VOICE_MODULE_WV:
         {
            int16_t beamr_enable = (VOICE_BBWE_BEAMR == vdec_ptr->voice_module_bbwe) ? TRUE : FALSE;
            if (VOICE_PARAM_MOD_ENABLE == param_id)
            {
               MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: SetParam WVE V1 enable %x, session(%lx)", *calibration_data_pay_load_ptr, vdec_ptr->session_num);

               if (((VOICE_BBWE_NONE == vdec_ptr->voice_module_bbwe) && (TRUE == *calibration_data_pay_load_ptr))
                     || ((VOICE_BBWE_WV_V1 == vdec_ptr->voice_module_bbwe) && (FALSE == *calibration_data_pay_load_ptr)))
               {
                  if ((vdec_ptr->process_data) || ((VOICE_WB_SAMPLING_RATE == voice_get_sampling_rate (vdec_ptr->voc_type, beamr_enable, vdec_ptr->evs_samp_rate)) && (TRUE == *calibration_data_pay_load_ptr)))
                  {
                     MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: Error :trying to enable/disable WVE V1 in RUN or enabling WV V1 for WB media type, session(%lx)", vdec_ptr->session_num);
                     //local_result = ADSP_EUNSUPPORTED;       // to maintain backward compatibility not returning error to upper layers
                     result = ADSP_EOK;
                     break;
                  }

                  vdec_ptr->voice_module_bbwe = ( TRUE == *calibration_data_pay_load_ptr) ? VOICE_BBWE_WV_V1 : VOICE_BBWE_NONE;

                  result = voice_wve_set_param (&(vdec_ptr->modules.vdec_pp_modules.wve_struct),
                        VOICE_PARAM_MOD_ENABLE, (char_t*) calibration_data_pay_load_ptr, param_size);
                  if (ADSP_FAILED(result))
                  {
                     MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: voice_wve_v1 mode enable failed !! disabling Wide Voice V1, result(%d) session(%lx)", (int32_t )result, vdec_ptr->session_num);
                     vdec_ptr->voice_module_bbwe = VOICE_BBWE_NONE;
                     break;
                  }
               }
               else if ((VOICE_BBWE_WV_V1 != vdec_ptr->voice_module_bbwe) && (TRUE == *calibration_data_pay_load_ptr))   // if any other BBWE is already enabled and WV V2 enable comes
               {
                  MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: wv_v1_set_param : trying to enable multiple BBWE simultaneously, session(%lx)", vdec_ptr->session_num);
                  return ADSP_EBADPARAM;
               }

            }
            else if (VOICE_PARAM_WV == param_id)
            {
               MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: SetParam WVE V1 param id(%#x), session(%lx)", VOICE_PARAM_WV, vdec_ptr->session_num);
               result = voice_wve_set_param (&(vdec_ptr->modules.vdec_pp_modules.wve_struct),
                     VOICE_PARAM_WV, (char_t*) calibration_data_pay_load_ptr, param_size);

            }
            break;
         }
      case VOICE_MODULE_WV_V2:
         {
            int16_t beamr_enable = (VOICE_BBWE_BEAMR == vdec_ptr->voice_module_bbwe) ? TRUE : FALSE;
            if (VOICE_PARAM_MOD_ENABLE == param_id)
            {
               MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: SetParam WVE V2 enable %x, session(%lx)", *calibration_data_pay_load_ptr, vdec_ptr->session_num);

               if (((VOICE_BBWE_NONE == vdec_ptr->voice_module_bbwe) && (TRUE == *calibration_data_pay_load_ptr))
                     || ((VOICE_BBWE_WV_V2 == vdec_ptr->voice_module_bbwe) && (FALSE == *calibration_data_pay_load_ptr)))
               {
                  if ((VOICE_NB_SAMPLING_RATE == voice_get_sampling_rate (vdec_ptr->voc_type, beamr_enable, vdec_ptr->evs_samp_rate)) && (vdec_ptr->process_data))
                  {
                     MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: Error : Wide Voice V2  enable/disable is not expected in run cmnd for NB media type, session(%lx)", vdec_ptr->session_num);
                     return ADSP_EUNSUPPORTED;   // for WV V2 error will be propagated to upper layers
                  }

                  vdec_ptr->voice_module_bbwe = ( TRUE == *calibration_data_pay_load_ptr) ? VOICE_BBWE_WV_V2 : VOICE_BBWE_NONE;

                  result = voice_wve_v2_set_param (&(vdec_ptr->modules.vdec_pp_modules.wve_v2_struct),
                        VOICE_PARAM_MOD_ENABLE, (char_t*) calibration_data_pay_load_ptr, param_size);
                  if (ADSP_FAILED(result))
                  {
                     MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: voice_wve_v2 mode enable failed !! disabling Wide Voice V2, result(%d) session(%lx)", (int32_t )result, vdec_ptr->session_num);
                     vdec_ptr->voice_module_bbwe = VOICE_BBWE_NONE;
                     break;
                  }
               }
               else if ((VOICE_BBWE_WV_V2 != vdec_ptr->voice_module_bbwe) && (TRUE == *calibration_data_pay_load_ptr))   // if any other BBWE is already enabled and WV V2 enable comes
               {
                  MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: wv_v2_set_param : trying to enable multiple BBWE simultaneously, session(%lx)", vdec_ptr->session_num);
                  return ADSP_EBADPARAM;
               }

            }
            else if (VOICE_PARAM_WV_V2 == param_id)
            {
               MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: SetParam WVE V2 param id(%#x), session(%lx)", VOICE_PARAM_WV_V2, vdec_ptr->session_num);
               result = voice_wve_v2_set_param (&(vdec_ptr->modules.vdec_pp_modules.wve_v2_struct),
                     VOICE_PARAM_WV_V2, (char_t*) calibration_data_pay_load_ptr, param_size);

            }
            break;
         }
      case VOICE_MODULE_RX_GAIN:
         {
            MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: SetParam Rx Gain, session(%lx)", vdec_ptr->session_num);
            result = voice_rx_gain_set_param (&(vdec_ptr->modules.vdec_pp_modules.rx_gain), calibration_data_pay_load_ptr, param_id, param_size);
         }
         break;

      case VOICE_MODULE_FNS:
         {
            result = voice_eans_set_param (&(vdec_ptr->modules.vdec_pp_modules.eans_struct), calibration_data_pay_load_ptr, param_id, param_size);
         }
         break;
      case VOICE_MODULE_DTMF_DETECTION:
         {
            /* not valid to enable DTMF detection via VOICE_CMD_SET_PARAM, must use VOICE_CMD_START_DTMF_DETECT */
            if ( VOICE_PARAM_MOD_ENABLE == param_id)
            {
               result = ADSP_EFAILED;
            }
            else
            {
               result = voice_dtmf_detect_set_param (&(vdec_ptr->modules.vdec_pp_modules.dtmf_det.dtmf_det_struct), calibration_data_pay_load_ptr, param_id, param_size);
            }
            MSG_4(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: vdec_pp_set_param_cmd - DTMF Det mid(%lx) pid(%lx), result=%d session(%lx)", module_id, param_id, result, vdec_ptr->session_num);
            for (int16_t i = 0; i < 18; i++)
            {
               MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO, "dtmf det param [%d] = %d", i, vdec_ptr->modules.vdec_pp_modules.dtmf_det.dtmf_det_struct.dtmf_detect_static_mem[i]);
            }
         }
         break;
      case VOICE_MODULE_ST:
         {
            result = voice_slow_talk_set_param (&(vdec_ptr->modules.vdec_pp_modules.slow_talk_struct), param_id, calibration_data_pay_load_ptr, param_size);

            if( VSM_MEDIA_TYPE_EVS == vdec_ptr->voc_type )
            {
               // If slowtalk is enabled, ceil to WB operating mode, else use default or vocoder native operating mode.
               if (VSM_VOC_OPERATING_MODE_WB < vdec_ptr->modules.decoder.voc_native_op_mode)
               {
                  uint32_t op_mode = vdec_ptr->modules.decoder.voc_native_op_mode;

                  if (NO_SLOWTALK != vdec_ptr->modules.vdec_pp_modules.slow_talk_struct.n_slowtalk)
                  {
                     op_mode = VSM_VOC_OPERATING_MODE_WB;
                  }

                  // Now compare to previous setting and notify client only if there's a change.
                  // Also helps when Slowtalk gets disabled on the fly.
                  if(op_mode != vdec_ptr->modules.decoder.session_op_mode)
                  {
                     vdec_ptr->modules.decoder.session_op_mode = op_mode;
                     vdec_send_mode_notification_v2(vdec_ptr);
                  }
               }
            }
         }
         break;

      default:
         {
            MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: Unsupported set dec  module 0x%lx", module_id);
            result = ADSP_EUNSUPPORTED;
            break;
         }
   }
   return result;

}
//set param can have some params used in main thread and some in pp thread.
//params used in main thread, must be handled here.
//params used in pp thread must be handled in pp thread itself.
static ADSPResult vdec_set_param_cmd (void* instance_ptr, elite_msg_any_t* msg_ptr)
{
   vdec_t* vdec_ptr = (vdec_t*) instance_ptr;
   ADSPResult result = ADSP_EOK;
   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: vdec_set_param_cmd begin session(%lx)", vdec_ptr->session_num);

   // TODO: should send an ACK back to caller to avoid race conditions, and to inform
   // success/fail.
   if (ELITE_CMD_SET_PARAM == msg_ptr->unOpCode)
   {
      elite_msg_param_cal_t* fadd_payload_ptr = (elite_msg_param_cal_t*) msg_ptr->pPayload;
      if (ELITEMSG_PARAM_ID_CAL == fadd_payload_ptr->unParamId)
      {
         uint32_t byte_size_counter = 0;
         int8_t *calibration_data_pay_load_ptr;
         voice_param_data_t *ptr = (voice_param_data_t *) fadd_payload_ptr->pnParamData;
         uint32_t nPayloadSize = fadd_payload_ptr->unSize;
         uint32_t payload_address = (uint32_t) ptr;

         {
            ADSPResult local_result = ADSP_EUNSUPPORTED;
            /* initialize to unsupported in case no valid module_id is found */

            // Iterate through the entire payload size and copy all updated parameters
            do
            {
               ptr = (voice_param_data_t *) payload_address;
               calibration_data_pay_load_ptr = (int8_t*) ptr + sizeof(voice_param_data_t);
               // check to make sure size is a multiple of 4. If not, stop calibrating
               if ((ptr->param_size & 0x00000003L) != 0)
               {
                  MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: Error! Vdec main received bad param size, mod %d, param %d, size %d!!", (int )ptr->module_id, (int )ptr->param_id, (int )ptr->param_size);
                  local_result = ADSP_EBADPARAM;
                  result = ADSP_EOK;
                  break;
               }

               local_result = vdec_set_param_int (vdec_ptr, ptr->module_id, ptr->param_id, calibration_data_pay_load_ptr, ptr->param_size);
               if (!((ADSP_EOK == local_result) || (ADSP_EUNSUPPORTED == local_result)))
               {

                  break;   // in case of any error dont go forward with parsing
               }
               byte_size_counter += (sizeof(voice_param_data_t) + ptr->param_size);
               payload_address += (sizeof(voice_param_data_t) + ptr->param_size);
            }
            while (byte_size_counter < nPayloadSize);

            MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, " Setparam of Vdec thread session(%lx)", local_result, vdec_ptr->session_num);
            elite_svc_send_ack (msg_ptr, local_result);
         }
      }
      else
      {
         elite_svc_send_ack (msg_ptr, ADSP_EFAILED);
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: Invalid param id session(%lx)", vdec_ptr->session_num);
      }
   }
   else
   {
      result = ADSP_EFAILED;
      elite_svc_send_ack (msg_ptr, result);
      MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: vdec_set_param_cmd secondary opcode not supported session(%lx)", vdec_ptr->session_num);

   }
   MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO, "vdec_set_param_cmd end result=%d session(%lx)", result, vdec_ptr->session_num);
   return ADSP_EOK;
}

//This command doesn't have anything for main thread. Hence, forward it to pp thread.
static ADSPResult vdec_get_param_cmd (void* instance_ptr, elite_msg_any_t* msg_ptr)
{
   vdec_t* vdec_ptr = (vdec_t*) instance_ptr;
   ADSPResult result = ADSP_EOK;
   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: vdec_get_param_cmd begin session(%lx)", vdec_ptr->session_num);
   elite_msg_param_cal_t* fadd_payload_ptr = (elite_msg_param_cal_t*) msg_ptr->pPayload;
   voice_param_data_t* voice_data_param_ptr;
   if (ELITE_CMD_GET_PARAM == msg_ptr->unOpCode)
   {
      ADSPResult local_result = ADSP_EUNSUPPORTED;

      if (ELITEMSG_PARAM_ID_CAL == fadd_payload_ptr->unParamId)
      {
         // extract Voice params payload pointer
         voice_data_param_ptr = (voice_param_data_t*) fadd_payload_ptr->pnParamData;

         uint32_t param_size_max = voice_data_param_ptr->param_size;
         int8_t* calibration_data_pay_load_ptr = (int8_t*) (((int8_t*) fadd_payload_ptr->pnParamData) + sizeof(voice_param_data_t));

         {   // TODO: put the following in a big function which includes all the mods and parameters function

            // Iterate through the entire payload size and copy all updated parameters
            switch (voice_data_param_ptr->module_id)
            {
               case VOICE_MODULE_BEAMR:
                  {
                     local_result = vdec_beamr_get_param (vdec_ptr, calibration_data_pay_load_ptr, voice_data_param_ptr->param_id, param_size_max, &voice_data_param_ptr->param_size);
                  }
                  break;
               case VOICE_MODULE_WV:
                  {
                     local_result = voice_wve_get_param (&(vdec_ptr->modules.vdec_pp_modules.wve_struct), voice_data_param_ptr->param_id, (char_t*) calibration_data_pay_load_ptr, param_size_max,
                           (int32_t*) &voice_data_param_ptr->param_size, (vdec_ptr->voice_module_bbwe == VOICE_BBWE_WV_V1) ?
                           TRUE :
                           FALSE);
                     MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: GetParam WVE V1 param ID(%lx), sessions(%lx)", voice_data_param_ptr->param_id, vdec_ptr->session_num);
                     break;
                  }
               case VOICE_MODULE_WV_V2:
                  {
                     local_result = voice_wve_v2_get_param (&(vdec_ptr->modules.vdec_pp_modules.wve_v2_struct), voice_data_param_ptr->param_id, (char_t*) calibration_data_pay_load_ptr, param_size_max,
                           (int32_t*) &voice_data_param_ptr->param_size);
                     MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: GetParam WVE V2 param ID(%lx), sessions(%lx)", voice_data_param_ptr->param_id, vdec_ptr->session_num);
                     break;
                  }
               case VOICE_MODULE_ST:
                  {
                     local_result = voice_slow_talk_get_param (&(vdec_ptr->modules.vdec_pp_modules.slow_talk_struct), voice_data_param_ptr->param_id, calibration_data_pay_load_ptr, param_size_max,
                           (int32_t*) &voice_data_param_ptr->param_size);
                     MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: GetParam ST param ID(%lx), sessions(%lx)", voice_data_param_ptr->param_id, vdec_ptr->session_num);
                     break;
                  }
               case VOICE_MODULE_FNS:
                  {
                     local_result = voice_eans_get_param (&(vdec_ptr->modules.vdec_pp_modules.eans_struct), calibration_data_pay_load_ptr, voice_data_param_ptr->param_id, param_size_max,
                           &voice_data_param_ptr->param_size);
                     MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: GetParam FNS param ID(%lx), sessions(%lx)", voice_data_param_ptr->param_id, vdec_ptr->session_num);
                     break;
                  }
               case VOICE_MODULE_DTMF_DETECTION:
                  {
                     local_result = voice_dtmf_detect_get_param (&(vdec_ptr->modules.vdec_pp_modules.dtmf_det.dtmf_det_struct), calibration_data_pay_load_ptr, voice_data_param_ptr->param_id, param_size_max,
                           &voice_data_param_ptr->param_size);
                     MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: GetParam DTMF DET param ID(%lx), sessions(%lx)", voice_data_param_ptr->param_id, vdec_ptr->session_num);
                     break;
                  }
               case VOICE_MODULE_RX_GAIN:
                  {
                     local_result = voice_rx_gain_get_param (&(vdec_ptr->modules.vdec_pp_modules.rx_gain), calibration_data_pay_load_ptr, voice_data_param_ptr->param_id, param_size_max,
                           &voice_data_param_ptr->param_size);
                     MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: GetParam Rx gain param ID(%lx), sessions(%lx)", voice_data_param_ptr->param_id, vdec_ptr->session_num);
                     break;
                  }

               default:
                  {
                     MSG(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: Unsupported Vdec module");
                     local_result = ADSP_EUNSUPPORTED;
                     break;
                  }
            }

            MSG_4(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: Vdec get param, mod id(0x%lx), param id (0x%lx), result(0x%x) session(0x%lx)", voice_data_param_ptr->module_id, voice_data_param_ptr->param_id,
                  local_result, vdec_ptr->session_num);
            result = local_result;
         }   // Local scope
      }
      else
      {
         MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: vdec_get_param_cmd : bad fadd id,  result(0x%x) session(0x%lx)\n", ADSP_EFAILED, vdec_ptr->session_num);
         result = ADSP_EFAILED;
      }
   }
   else
   {
      MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: vdec_get_param_cmd secondary opcode not supported :(%lx)\n", vdec_ptr->session_num);
      result = ADSP_EFAILED;
   }

   if (ADSP_SUCCEEDED(result))
   {
      fadd_payload_ptr->unSize = voice_data_param_ptr->param_size;
   }
   else
   {
      fadd_payload_ptr->unSize = 0;
   }
   elite_svc_send_ack (msg_ptr, result);
   MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: vdec_get_param_cmd end handler result(0x%x) session(0x%lx)", result, vdec_ptr->session_num);
   return ADSP_EOK;
}

static ADSPResult vdec_clear_loopback_gpq (vdec_t *vdec_ptr)
{
   elite_msg_any_t dataQMsg;
   ADSPResult result_q_pop, result_apr;

   do
   {
      // Non-blocking MQ receive
      result_q_pop = qurt_elite_queue_pop_front (vdec_ptr->svc_handle.gpQ, (uint64_t*) &dataQMsg);
      if (ADSP_EOK == result_q_pop)
      {
         result_apr = elite_apr_if_free (vdec_ptr->apr_info_ptr->apr_handle, (elite_apr_packet_t *) dataQMsg.pPayload);
         if (ADSP_FAILED(result_apr))
         {
            MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: ERROR! Elite APR message handler has returned error %d, continuing...", result_apr);
         }
      }
   }
   while (ADSP_EOK == result_q_pop);

   return ADSP_EOK;
}

/* Read APR vocoder packet into local buffer */
static ADSPResult vdec_read_apr_msg (vdec_t* vdec_ptr, elite_msg_any_t *sMsg)
{
   uint32_t nPayloadSize, dec_pkt_size;
   vsm_send_dec_packet_t *pDecPktStruct = NULL;
   int8_t* pDecPkt;
   vdec_ptr->data_msg_payload_ptr = (elite_apr_packet_t *) sMsg->pPayload;
   nPayloadSize = elite_apr_if_get_payload_size (vdec_ptr->data_msg_payload_ptr);

   if (nPayloadSize < sizeof(vsm_send_dec_packet_t))
   { /* The payload is too small to be valid so throw the packet away. */
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: APR pkt payload too small, erasure session(%lx)", vdec_ptr->session_num);
      vdec_ptr->modules.decoder.erasure = TRUE;   //signal erasure
   }
   else
   {
      pDecPktStruct = (vsm_send_dec_packet_t *) elite_apr_if_get_payload_ptr (vdec_ptr->data_msg_payload_ptr);
      pDecPkt = (int8_t *) APR_PTR_END_OF(pDecPktStruct, sizeof(vsm_send_dec_packet_t));
      dec_pkt_size = (nPayloadSize - sizeof(vsm_send_dec_packet_t));
      if (0 == dec_pkt_size || (sizeof(vdec_ptr->in_buf) < dec_pkt_size))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: Dec pkt size is 0, erasure session(%lx)", vdec_ptr->session_num);
         vdec_ptr->modules.decoder.erasure = TRUE;   // signal erasure
      }
      else if (vdec_ptr->voc_type != pDecPktStruct->media_type)
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: MediaType mismatch, erasure session(%lx)", vdec_ptr->session_num);
         vdec_ptr->modules.decoder.erasure = TRUE;   // signal erasure
      }
      else   //copy dec pkt into local buffer
      {
         vdec_ptr->in_buf_size = dec_pkt_size;
         memsmove (&(vdec_ptr->in_buf[0]), sizeof(vdec_ptr->in_buf), pDecPkt, dec_pkt_size);
         //dbg: MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Copy Data to local buf, bytes(%d) session(%lx)",dec_pkt_size,vdec_ptr->vdec_cmn.session_num);
      }
   }

   return ADSP_EOK;
}

static ADSPResult vdec_data_handler (void *instance_ptr)
{
   //this is a dummy function
   //Every Queue in qurt_elite needs to be associated with a channel. So this
   //function is just to meet that requirement. we never wait for this mask, so
   //this function should never be called.
   MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: This function shoul never be called. We never wait for data mask");

   return ADSP_EOK;
}

static ADSPResult vdec_process_oob_pkt_data (vdec_t* vdec_ptr)
{
   ADSPResult result = ADSP_EOK;
   vdec_ptr->modules.decoder.erasure = FALSE;     // reset  erasure flag
   vsm_oob_pkt_exchange_header_t *pDecPktStruct = NULL;
   int8_t* pDecPkt;

   if ( FALSE == vdec_ptr->loopback_enable)
   {
      if (vdec_ptr->pkt_xchg.oob_pkt_exchange_ptr == NULL)
      {
         MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: ERROR ! OOB packet exchange pointer to shared memory for DECODER is NULL, session(%x)", (int )vdec_ptr->session_num);
         result = ADSP_EFAILED;
         vdec_ptr->modules.decoder.erasure = TRUE;   // signal erasure

      }
      else if (vdec_ptr->pkt_xchg.dec_pkt_oob_ready == TRUE)
      {
         //invalidate the cache for read operation
         result = elite_mem_invalidate_cache (&vdec_ptr->pkt_xchg.memmap_vdec_oob);
         // set dec_pkt_oob_ready to FALSE
         vdec_ptr->pkt_xchg.dec_pkt_oob_ready = FALSE;
         pDecPktStruct = (vsm_oob_pkt_exchange_header_t *) vdec_ptr->pkt_xchg.oob_pkt_exchange_ptr;
         pDecPkt = (int8_t *) APR_PTR_END_OF(pDecPktStruct, sizeof(vsm_oob_pkt_exchange_header_t));
         if (vdec_ptr->voc_type != pDecPktStruct->media_type)
         {
            MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: MediaType mismatch for OOB packet exchange, erasure session(%lx)", vdec_ptr->session_num);
            vdec_ptr->modules.decoder.erasure = TRUE;   // signal erasure
         }
         else if (0 == pDecPktStruct->size || (sizeof(vdec_ptr->in_buf) < pDecPktStruct->size))
         {
            MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: Dec pkt size is 0, erasure session(%lx)", vdec_ptr->session_num);
            vdec_ptr->modules.decoder.erasure = TRUE;   // signal erasure
         }
         else
         {
            vdec_ptr->pkt_xchg.dec_pkt_ready = TRUE;
            vdec_ptr->in_buf_size = pDecPktStruct->size;
            memsmove (&(vdec_ptr->in_buf[0]), sizeof(vdec_ptr->in_buf), pDecPkt, pDecPktStruct->size);
            MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: OOB Dec pkt size received (%d),  session(%lx)", (int )vdec_ptr->in_buf_size, vdec_ptr->session_num);
         }
      }
      else
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: OOB Dec pkt not received in time , erasure session(%lx)", vdec_ptr->session_num);
         vdec_ptr->pkt_xchg.oob_pkt_miss_ctr++;
         vdec_ptr->modules.decoder.erasure = TRUE;
      }
   }
   return result;
}

static ADSPResult vdec_process_pkt_data (vdec_t* vdec_ptr)
{
   ADSPResult result = ADSP_EOK;
   elite_msg_any_t in_buf_msg;

   vdec_ptr->modules.decoder.erasure = FALSE;     // reset  erasure flag
   if (vdec_ptr->pkt_xchg.dec_pkt_ready)
   {
      vdec_ptr->pkt_xchg.dec_pkt_not_consumed++;
   }

   // ***************** Pop Input buffer
   memset (&in_buf_msg, 0, sizeof(elite_msg_any_t));

   //dbg: MSG_1(MSG_SSID_QDSP6, DBG_LOW_PRIO,"VCP: vdec_process_pkt_data begin session(%lx)",vdec_ptr->vdec_cmn.session_num); //TODO: not needed

   // ***************** Read the input data
   result = qurt_elite_queue_pop_front (vdec_ptr->svc_handle.dataQ, (uint64_t*) &in_buf_msg);
   if (ADSP_EOK != result)
   {
      if (ADSP_ENEEDMORE == result)
      {
         MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: Vdec pkt not available for decoding, result %d, session(%lx)", result, vdec_ptr->session_num);
      }
      else
      {
         MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: Vdec pkt queue pop operation error result %d, session(%lx)", result, vdec_ptr->session_num);
      }
      return result;
   }

   vdec_ptr->pkt_xchg.dec_pkt_ready = TRUE;   // flag to indicate data buffer arrived

   // ***************** Check Media Type
   /* Media type is not valid in Vdec */

   // ***************** Copy data to local buffer
   // If RUN state and if data payload is valid & if data is apr pkt
   if ((vdec_ptr->process_data) && (NULL != in_buf_msg.pPayload) && (ELITE_APR_PACKET == in_buf_msg.unOpCode))
   {
      if ( FALSE == vdec_ptr->loopback_enable)
      {
         vdec_read_apr_msg (vdec_ptr, &in_buf_msg);

         // reset data_msg_payload_ptr.  TODO: is this variable necessary?
         if (NULL != vdec_ptr->data_msg_payload_ptr)
         {
            vdec_ptr->data_msg_payload_ptr = NULL;
         }
      }

      /* run state and APR packet, need to free the APR packet (loopback or not) */
      elite_apr_if_free (vdec_ptr->apr_info_ptr->apr_handle, (elite_apr_packet_t *) in_buf_msg.pPayload);

   }
   else
   {
      if (FALSE == vdec_ptr->process_data)
      {
         /* if not in run state, still need to free the APR packet */
         elite_apr_if_free (vdec_ptr->apr_info_ptr->apr_handle, (elite_apr_packet_t *) in_buf_msg.pPayload);

         MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: STOP state, throw away dec pkt  session(%lx)", vdec_ptr->session_num);
      }
      else
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: Invalid APR pkt session(%lx)", vdec_ptr->session_num);
         vdec_ptr->modules.decoder.erasure = TRUE;   //signal erasure
      }
   }

   // todo: change mask, take care of PS call

   //dbg: MSG_1(MSG_SSID_QDSP6, DBG_LOW_PRIO,"VCP: vdec_process_pkt_data end session(%lx)",vdec_ptr->vdec_cmn.session_num); //TODO: not needed
   return result;
}

static ADSPResult vdec_process (vdec_t* vdec_ptr)
{
   ADSPResult result = ADSP_EOK;
   int16_t *in_ptr = (int16_t*) &(vdec_ptr->in_buf[0]);   // pointer to local input buffer
   int16_t *out_ptr = (int16_t*) &(vdec_ptr->dec_out_buf[0]);   // pointer to local output buffer
   uint32_t i;
   //MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: vdec_process begin");

   vdec_ptr->dec_out_size_bytes = (VOICE_FRAME_SIZE_NB_20MS * (vdec_ptr->samp_rate_factor)) << 1;

   if (vdec_ptr->modules.decoder.erasure)
   {
      MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: Erasure detected in vdec_process: (%ld)\n", vdec_ptr->session_num);
   }

   // vocoder decoder processing
   switch (vdec_ptr->voc_type)
   {
      case VSM_MEDIA_TYPE_EVRC_MODEM:
         {
            (void) vdec_process_evrc (vdec_ptr);
            break;
         }
      case VSM_MEDIA_TYPE_AMR_NB_MODEM:
      case VSM_MEDIA_TYPE_EAMR:
         {
            (void) vdec_process_amr_nb_modem (vdec_ptr);
            break;
         }
      case VSM_MEDIA_TYPE_AMR_NB_IF2:
         {
            (void) vdec_process_amr_nb_if2 (vdec_ptr);
            break;
         }
      case VSM_MEDIA_TYPE_AMR_WB_MODEM:
         {
            (void) vdec_process_amr_wb_modem (vdec_ptr);
            break;
         }
      case VSM_MEDIA_TYPE_EFR_MODEM:
         {
            (void) vdec_process_efr (vdec_ptr);
            break;
         }
      case VSM_MEDIA_TYPE_FR_MODEM:
         {
            (void) vdec_process_fr (vdec_ptr);
            break;
         }
      case VSM_MEDIA_TYPE_HR_MODEM:
         {
            (void) vdec_process_hr (vdec_ptr);
            break;
         }
      case VSM_MEDIA_TYPE_PCM_8_KHZ:
      case VSM_MEDIA_TYPE_PCM_16_KHZ:
      case VSM_MEDIA_TYPE_PCM_32_KHZ:
      case VSM_MEDIA_TYPE_PCM_48_KHZ:

         {
            if (vdec_ptr->modules.decoder.erasure)
            {
               memset ((void*) in_ptr, 0, vdec_ptr->dec_out_size_bytes);   //silence, todo: remove hardcoding
            }
            for (i = 0; i < vdec_ptr->dec_out_size_bytes / 2; i++)
            {
               out_ptr[i] = in_ptr[i] >> 2;
               out_ptr[i] = out_ptr[i] << 2;   // convert decoder output to normalized 16-bit
            }
            break;
         }
      case VSM_MEDIA_TYPE_PCM_44_1_KHZ:
         {
            if (vdec_ptr->modules.decoder.erasure)
            {
               memset ((void*) out_ptr, 0, VOICE_FRAME_SIZE_FB << 1);   //silence,
            }
            else   //Call resampler only if not erasure
            {
               ADSPResult resampler_result = voice_gen_resamp_process (&(vdec_ptr->modules.codec_resampler), (int32 *) in_ptr, (int32 *) out_ptr, (int32) VOICE_FRAME_SIZE_44_1, (int32) VOICE_FRAME_SIZE_FB,
                     (int16) GEN_RESAMP_FIXED_OUTPUT_MODE);
               if (ADSP_EOK != resampler_result)
               {
                  MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: ERROR: Generic Resampler failed for decoder session(%x)", (int )vdec_ptr->session_num);
                  memset ((void*) out_ptr, 0, VOICE_FRAME_SIZE_FB << 1);   //silence,
                  return ADSP_EFAILED;
               }
               for (i = 0; i < vdec_ptr->dec_out_size_bytes / 2; i++)   //for 44.1 PCM Mediatype, the stream runs at 48K
               {
                  out_ptr[i] = out_ptr[i] >> 2;
                  out_ptr[i] = out_ptr[i] << 2;   // convert decoder output to normalized 16-bit
               }
            }
            break;
         }
      case VSM_MEDIA_TYPE_4GV_NB_MODEM:
      case VSM_MEDIA_TYPE_4GV_WB_MODEM:
      case VSM_MEDIA_TYPE_4GV_NW_MODEM:
      case VSM_MEDIA_TYPE_4GV_NW:
      case VSM_MEDIA_TYPE_EVRC_NW_2K:
         {
            (void) vdec_process4GV (vdec_ptr);
            break;
         }
      case VSM_MEDIA_TYPE_13K_MODEM:
         {
            (void) vdec_processV13k (vdec_ptr);
            break;
         }
      case VSM_MEDIA_TYPE_G711_ALAW:
      case VSM_MEDIA_TYPE_G711_MLAW:
      case VSM_MEDIA_TYPE_G711_ALAW_V2:
      case VSM_MEDIA_TYPE_G711_MLAW_V2:
         {
            vdec_processG711 (vdec_ptr, in_ptr, out_ptr, sizeof(vdec_ptr->dec_out_buf));
            break;
         }
      case VSM_MEDIA_TYPE_G729AB:
         {
            int16_t aDecBuf[G729AB_DEC_PKT_SIZE_IN_WORDS_10MSEC];
            memset (aDecBuf, 0, sizeof(aDecBuf));
            /* deframe into format library accepts (1 word frame type + 5 words params ) */
            g729_dec_deframing ((int8_t *) in_ptr, aDecBuf);
            /* 1st 10 ms */
            g729a_decode (vdec_ptr->modules.decoder.g729a_decoder_ptr, vdec_ptr->modules.decoder.g729a_common_ptr, aDecBuf, out_ptr, &vdec_ptr->modules.decoder.synth_buf[G729_DEC_M],
                  vdec_ptr->modules.decoder.erasure || (aDecBuf[0] == 3));

            /* deframe into format library accepts (1 word frame type + 5 words params ) */
            g729_dec_deframing ((int8_t *) in_ptr + G729AB_2ND_DEC_PKT_OFFSET, aDecBuf);
            /* 2nd 10 ms, offset input and output buffers by proper amount */
            g729a_decode (vdec_ptr->modules.decoder.g729a_decoder_ptr, vdec_ptr->modules.decoder.g729a_common_ptr, aDecBuf, out_ptr + G729_DEC_L_FRAME, &vdec_ptr->modules.decoder.synth_buf[G729_DEC_M],
                  vdec_ptr->modules.decoder.erasure || (aDecBuf[0] == 3));
            break;
         }
      case VSM_MEDIA_TYPE_EVS:
         {
            int16_t mode_bit;
            uint32_t pkt_size = 0;
            pkt_size = 1 + evs_find_pkt_size((uint32_t )(((uint8_t )in_ptr[0])& 0x1F));
            if (vdec_ptr->modules.decoder.erasure)
            {
               in_ptr[0] = (((in_ptr[0])& 0xFFE0) | 0x000F);  // make frame type as no data frame for erasures
               pkt_size = 1;
            }
            MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: VDEC: EVS pkt size of the frame (%d) ",
                  (int)pkt_size);
            // log packet in Q6 format
            uint16_t sampling_rate = voice_get_sampling_rate (vdec_ptr->voc_type, 0/*beamr is dont care*/, vdec_ptr->evs_samp_rate);
            vdec_log_vocoder_packet (vdec_ptr->voc_type, sampling_rate, ((sampling_rate == 16000) << 3) | vdec_ptr->session_num, in_ptr, pkt_size);
            (void) evs_dec_process ((void*)vdec_ptr->modules.decoder.dec_state_ptr, in_ptr, out_ptr );
            // if mode has changed and if mode detection is enabled, issue event
            if(vdec_ptr->modules.decoder.vocoder_op_detection == 1)
            {
               //retrieve mode bit
               evs_dec_operating_mode((void*)vdec_ptr->modules.decoder.dec_state_ptr, &mode_bit);
               uint32_t op_mode = evs_find_operating_bandwidth((uint32_t )mode_bit);

               MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Mode bit from VCP EVS vocoder = %d, vocoder native mode = (%lx)", mode_bit, op_mode);

               // Wait until a genuine operating mode has been detected. Until then, notifications sent earlier will suffice.
               if(0 != op_mode)
               {
                  // Update vocoder native mode (no use updating if its a startup transient mode).
                  vdec_ptr->modules.decoder.voc_native_op_mode = op_mode;

                  // If slowtalk is enabled, ceil to WB operating mode.
                  if ((NO_SLOWTALK != vdec_ptr->modules.vdec_pp_modules.slow_talk_struct.n_slowtalk)
                        && (VSM_VOC_OPERATING_MODE_WB < op_mode))
                  {
                     op_mode = VSM_VOC_OPERATING_MODE_WB;
                  }

                  // Now compare to previous setting and notify client only if there's a change.
                  if(op_mode != vdec_ptr->modules.decoder.session_op_mode)
                  {
                     vdec_ptr->modules.decoder.session_op_mode = op_mode;
                  vdec_send_mode_notification_v2(vdec_ptr);
               }
            }
            }
            break;
         }
      default:
         {
            result = ADSP_EUNSUPPORTED;
            MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: Vdec not supported session(%lx)", vdec_ptr->session_num);
            return result;
         }
   }

   /* Host PCM processing */
   /* This point is chosen so that client gets pure e-call data before any other stream PP */
   voice_ecall_rx (vdec_ptr, out_ptr);

   //MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: vdec_process end supported session(%lx)", vdec_ptr->session_num);

   result = vdec_pp_process(vdec_ptr);

   return result;
}

/* utility function to get sampling rate from voc media type */
/* TODO: Use common function */
uint16_t vdec_get_sampling_rate (uint32_t unMediaTypeFormat, voice_bbwe_feature_t BbweEnable, uint16_t evs_samp_rate)
{
   uint16_t sampling_rate = voice_get_sampling_rate( unMediaTypeFormat,(VOICE_BBWE_BEAMR == BbweEnable), evs_samp_rate);
   if ((sampling_rate == VOICE_NB_SAMPLING_RATE) && ((VOICE_BBWE_WV_V2 == BbweEnable) || (VOICE_BBWE_WV_V1 == BbweEnable)))
   {
      sampling_rate = VOICE_WB_SAMPLING_RATE;
   }
   return sampling_rate;
}

static ADSPResult vdec_processG711 (vdec_t* vdec_ptr, int16_t *in_ptr, int16_t *out_ptr, int16_t outputbufsize)
{
   uint8_t i;
   uint16_t deframed_buf[81]; /* 162 byte word aligned buffer */
   int16_t fPlcEnabled = 1;
   int16_t frameisgood = 1;
   int16_t bgood;
   uint16_t offset_for_second_decode = 161; ////1 extra byte for header

   //adding packet logging
   if( vdec_ptr->voc_type != VSM_MEDIA_TYPE_G711_ALAW && vdec_ptr->voc_type != VSM_MEDIA_TYPE_G711_MLAW )
   {
      uint32_t pkt_size = 161;
      uint16_t sampling_rate = voice_get_sampling_rate(vdec_ptr->voc_type,0,0);
      vdec_log_vocoder_packet( vdec_ptr->voc_type,sampling_rate, ((sampling_rate == 16000) << 3) | vdec_ptr->session_num, in_ptr, pkt_size);
   }


   // G711 decoding, assumes 2 frames of 10 ms of data, with header for each of the two frames.
   for( i = 0; i < G711_L_TOTALFRAME/vdec_ptr->modules.decoder.g711_frame_size; i++)
   {
      uint8_t *parm = (uint8_t *) in_ptr;
      if (1 == i)
      {
         parm += offset_for_second_decode;
      }
      if( ( parm[0] & 0x3) == ACTIVE_FRAME)
      {
         frameisgood = 1;
      }
      else if( ( parm[0] & 0x3) == SID)
      {
         frameisgood = 1;
      }
      else if(( parm[0] & 0x3) == NTX)
      {
         frameisgood = 0;
      }
      else if(( parm[0] & 0x3) == ERASURE)
      {
         frameisgood = 0;
      }
      if(TRUE == vdec_ptr->modules.decoder.erasure)
      {
         frameisgood = 0;
      }        
        //We will determine how to handle it if it is a NTX/erasure frame
        bgood = G711_check_erasure(frameisgood, (void*)vdec_ptr->modules.decoder.dec_state_ptr);

        if (bgood)
      {

         /* can use max size for deframing */
         G711Deframing( parm, reinterpret_cast<uint8_t *> (deframed_buf), G711_PCM_FRAMED_SIZE, sizeof(deframed_buf));
         /* current frame is good, pass to decoder and store in decoded frame buffer at proper offset */
         G711A2Decoder( (void *) vdec_ptr->modules.decoder.dec_state_ptr, reinterpret_cast<int16_t *> (deframed_buf), &out_ptr[vdec_ptr->modules.decoder.g711_frame_size*i], outputbufsize - vdec_ptr->modules.decoder.g711_frame_size*i*sizeof(int16_t));
         G711A2DecoderPlcAddToHistory(vdec_ptr->modules.decoder.dec_state_ptr,&out_ptr[vdec_ptr->modules.decoder.g711_frame_size*i]);
      }
      else
      {
         /* Generation of synth signal for the erased frame*/
         if (fPlcEnabled)
         {
            G711A2DecoderPlcConceal(vdec_ptr->modules.decoder.dec_state_ptr,&out_ptr[vdec_ptr->modules.decoder.g711_frame_size*i]);
         }
         else
         {
            /* plc disabled, insert silence  */
            memset( out_ptr, 0, vdec_ptr->modules.decoder.g711_frame_size * sizeof(int16_t));
            G711A2DecoderPlcAddToHistory( vdec_ptr->modules.decoder.dec_state_ptr, &out_ptr[vdec_ptr->modules.decoder.g711_frame_size*i]);
         }
      }
   }

   /* G711 core decoder operates on 16 bit, so right shift by 2 to give to
    * voice PP which expects 14 bit */
   for( i = 0; i < 160; i++)
   {
      out_ptr[i] = out_ptr[i] >> 2;
      out_ptr[i] = out_ptr[i] << 2;   // convert decoder output to normalized 16-bit
   }

   return ADSP_EOK;
}

/*
   static void voice_result_check(ADSPResult result, uint32_t session_num)
   {
   if (ADSP_EOK != result)
   {
   MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Vdec operation error result %d, session(%lx)",result,session_num);
   }
   }
 */

static ADSPResult vdec_init (vdec_t *vdec_ptr)
{
   ADSPResult result = ADSP_EOK;
   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: vdec_init begin session(%lx)", vdec_ptr->session_num);

   // Reset decoder specific params
   (void) vdec_dec_params_init (vdec_ptr);
   // Reset control code params
   (void) vdec_ctrl_params_init (vdec_ptr);

   // Reset params for a new call

   // Fill payload struct for sub/unsub with vtm for pkt req tick
   vdec_ptr->timing.vtm_sub_unsub_vds_pkt_req_data.offset = 3300;   // 3.3 ms
   vdec_ptr->timing.vtm_sub_unsub_vds_pkt_req_data.signal_enable = 1;
   vdec_ptr->timing.vtm_sub_unsub_vds_pkt_req_data.client_id = VOICE_DEC_VDS_PKT_REQ;
   vdec_ptr->timing.vtm_sub_unsub_vds_pkt_req_data.vfr_mode = VFR_NONE;
   vdec_ptr->timing.vtm_sub_unsub_vds_pkt_req_data.vsid = 0;
   vdec_ptr->timing.vtm_sub_unsub_vds_pkt_req_data.timing_ver =
      VFR_CLIENT_INFO_VER_1;
   // Fill payload struct for sub/unsub with vtm for decode tick
   vdec_ptr->vtm_sub_unsub_decode_data.offset = 8300;   // 8.3ms
   vdec_ptr->vtm_sub_unsub_decode_data.signal_enable = 1;
   vdec_ptr->vtm_sub_unsub_decode_data.client_id = VOICE_DEC;
   vdec_ptr->vtm_sub_unsub_decode_data.vfr_mode = VFR_NONE;
   vdec_ptr->vtm_sub_unsub_decode_data.vsid = 0;
   vdec_ptr->vtm_sub_unsub_decode_data.timing_ver = VFR_CLIENT_INFO_VER_1;

   vdec_ptr->pkt_xchg.pkt_exchange_mode = IN_BAND;
   vdec_ptr->pkt_xchg.oob_pkt_exchange_ptr = NULL;
   vdec_ptr->pkt_xchg.dec_pkt_oob_ready = FALSE;
   vdec_ptr->pkt_xchg.max_pkt_req_delay = 0;        //in usec
   vdec_ptr->pkt_xchg.min_pkt_req_delay = 0x7fffffffL;        //in usec
   vdec_ptr->pkt_xchg.pkt_req_ctr = 0;
   vdec_ptr->pkt_xchg.pkt_miss_ctr = 0;
   vdec_ptr->pkt_xchg.oob_pkt_miss_ctr = 0;
   vdec_ptr->pkt_xchg.oob_pkt_ready_ctr = 0;
   vdec_ptr->voc_type = VSM_MEDIA_TYPE_NONE;
   vdec_ptr->voice_module_bbwe = VOICE_BBWE_NONE;   //clear BBWE flag in (RE)INIT
   vdec_ptr->vfr_mode = VFR_NONE;
   vdec_ptr->loopback_enable = FALSE;      // reset on init/reinit.
   vdec_ptr->sampling_rate_dec = VOICE_NB_SAMPLING_RATE;
   vdec_ptr->frame_samples_dec = VOICE_FRAME_SIZE_NB;
   vdec_ptr->samp_rate_factor = 1;
   vdec_ptr->vsid = 0;
   vdec_ptr->modules.decoder.eamr_last_bw_det_state = VSM_EAMR_MODE_NARROWBAND;   // 0 - NB mode, start in NB mode
   vdec_ptr->modules.decoder.eamr_mode_flag = VSM_EAMR_MODE_CHANGE_DETECTION_DISABLE;
   vdec_ptr->modules.decoder.tty_option = FALSE;
   vdec_ptr->oobtty_enable = FALSE;
   // default mode is none
   vdec_ptr->modules.decoder.session_op_mode = VSM_VOC_OPERATING_MODE_NONE;
   vdec_ptr->modules.decoder.voc_native_op_mode = VSM_VOC_OPERATING_MODE_NONE;

   vdec_ptr->modules.vdec_pp_modules.record.enable_recording = FALSE;   // should not be reset in init to support handover cases
   vdec_ptr->modules.vdec_pp_modules.record.recording_mode = 0;   // init with 0
   vdec_ptr->modules.vdec_pp_modules.record.afe_drift_ptr = NULL;
   vdec_ptr->modules.vdec_pp_modules.record.ss_info_counter = 0;
   vdec_ptr->modules.vdec_pp_modules.record.aud_ref_port = 0;

   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "vdec_init end session(%lx)", vdec_ptr->session_num);
   return result;
}

// Reset decoder specific params, should be called for a new call and HO to another vocoder
static ADSPResult vdec_dec_params_init (vdec_t *vdec_ptr)
{
   ADSPResult result = ADSP_EOK;
   //dbg: MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: vdec_dec_params_init begin session(%lx)",vdec_ptr->vdec_cmn.session_num);

   vdec_ptr->modules.decoder.amr_prev_rate = 7;

   //dbg: MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "vdec_dec_params_init end session(%lx)",vdec_ptr->vdec_cmn.session_num);
   return result;
}

// Reset control code params, should be called for a new call and in STOP state
static ADSPResult vdec_ctrl_params_init (vdec_t *vdec_ptr)
{
   ADSPResult result = ADSP_EOK;
   //dbg: MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: vdec_ctrl_params_init begin session(%lx)",vdec_ptr->vdec_cmn.session_num);

   vdec_ptr->in_buf_size = 0;
   vdec_ptr->dec_out_size_bytes = 0;
   vdec_ptr->pkt_xchg.dec_pkt_ready = FALSE;
   vdec_ptr->pkt_xchg.dec_pkt_not_consumed = 0;
   //dbg: MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: vdec_ctrl_params_init end session(%lx)",vdec_ptr->vdec_cmn.session_num);
   return result;
}

static ADSPResult vdec_send_dec_pkt_req (vdec_t *vdec_ptr)
{
   int32_t rc;
   elite_apr_packet_t* packet = NULL;
   ADSPResult result = ADSP_EOK;
   //dbg: MSG_1(MSG_SSID_QDSP6, DBG_LOW_PRIO,"VCP: vdec_send_dec_pkt_req begin session(%x)",(int)vdec_ptr->vdec_cmn.session_num);

   if (vdec_ptr->pkt_xchg.pkt_exchange_mode == IN_BAND)
   {
      rc = elite_apr_if_alloc_event (vdec_ptr->apr_info_ptr->apr_handle, vdec_ptr->apr_info_ptr->self_addr, vdec_ptr->apr_info_ptr->self_port, vdec_ptr->apr_info_ptr->client_addr,
            vdec_ptr->apr_info_ptr->client_port, vdec_ptr->pkt_xchg.pkt_req_ctr++,
            VSM_EVT_REQ_DEC_PACKET, 0, &packet);
   }
   else
   {
      rc = elite_apr_if_alloc_event (vdec_ptr->apr_info_ptr->apr_handle, vdec_ptr->apr_info_ptr->self_addr, vdec_ptr->apr_info_ptr->self_port, vdec_ptr->apr_info_ptr->client_addr,
            vdec_ptr->apr_info_ptr->client_port, vdec_ptr->pkt_xchg.pkt_req_ctr++,
            VSM_EVT_OOB_DEC_BUF_REQUEST, 0, &packet);
   }

   if (rc || (NULL == packet))
   {
      MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: Failed to create APR packet for pkt request, res(0x%8lx), packet exchange mode(%d), session(%x)", rc, (int )vdec_ptr->pkt_xchg.pkt_exchange_mode,
            (int )vdec_ptr->session_num);
      return ADSP_EFAILED;
   }
   result = voice_cmn_send_vds_apr_request (vdec_ptr->vds.vds_client_id, vdec_ptr->vds.vds_client_token, &vdec_ptr->apr_info_ptr->apr_handle, vdec_ptr->vds.vds_handle_ptr, packet,
         vdec_ptr->session_num);
   if (ADSP_FAILED(result))
   {
      MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: Failed to send APR packet to VDS for pkt req result(%d), pkt exchange mode(%ld), session(%lx)", (unsigned int )result,
            vdec_ptr->pkt_xchg.pkt_exchange_mode, vdec_ptr->session_num);
      elite_apr_if_free (vdec_ptr->apr_info_ptr->apr_handle, packet);
      return result;
   }

   return result;
}

static ADSPResult vdec_send_out_buf_downstream (vdec_t* vdec_ptr, qurt_elite_bufmgr_node_t *out_buf_mgr_node_ptr)
{
   ADSPResult result = ADSP_EOK;
   elite_msg_any_t* peer_data_Q_msg_ptr;
   elite_msg_data_buffer_t* out_data_payload_ptr = (elite_msg_data_buffer_t*) out_buf_mgr_node_ptr->pBuffer;
   out_data_payload_ptr->pResponseQ = NULL;
   out_data_payload_ptr->unClientToken = NULL;
   out_data_payload_ptr->pBufferReturnQ = (vdec_ptr->buf_q_ptr);
   out_data_payload_ptr->nOffset = 0;
   out_data_payload_ptr->nActualSize = vdec_ptr->pp_out_buf_size;

   // Read one Frame from output data
   memscpy (&(out_data_payload_ptr->nDataBuf), out_data_payload_ptr->nMaxSize, vdec_ptr->pp_out_buf, out_data_payload_ptr->nActualSize);

   // send output buf to downstream service
   peer_data_Q_msg_ptr = elite_msg_convt_buf_node_to_msg (out_buf_mgr_node_ptr,
         ELITE_DATA_BUFFER,
         NULL, /* do not need response */
         0, /* token */
         0 /* do not care response result*/
         );
   // todo: check for downstreampeer null
   result = qurt_elite_queue_push_back (vdec_ptr->downstream_peer_ptr->dataQ, (uint64_t*) peer_data_Q_msg_ptr);
   if (ADSP_FAILED(result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: Failed to deliver buffer dowstream. Dropping session(%lx)", vdec_ptr->session_num);
      (void) elite_msg_push_payload_to_returnq (vdec_ptr->buf_q_ptr, (elite_msg_any_payload_t*) out_buf_mgr_node_ptr->pBuffer);
   }

   return result;
}

static ADSPResult vdec_voc_init (vdec_t* vdec_ptr)
{
   ADSPResult result = ADSP_EOK;

   /* perform vocoder decoder initialization here */
   switch (vdec_ptr->voc_type)
   {
      case VSM_MEDIA_TYPE_EVRC_MODEM:
         {
            result = vdec_check_dec_struct_size (get_sizeof_evrc_dec_struct (), vdec_ptr->session_num);
            if (ADSP_FAILED(result))
            {
               return result;
            }
            // todo: need this?
            memset ((void*) vdec_ptr->modules.decoder.dec_state_ptr, 0, get_sizeof_evrc_dec_struct ());
            evrc_init_dec ((void*) vdec_ptr->modules.decoder.dec_state_ptr);
            break;
         }
      case VSM_MEDIA_TYPE_AMR_NB_MODEM:
      case VSM_MEDIA_TYPE_AMR_NB_IF2:
         {
            /* Don't reinit TTY here.  Assuming RESYNC_CTM is invoked */
            result = vdec_check_dec_struct_size (get_eamr_dec_struct_size (), vdec_ptr->session_num);
            if (ADSP_FAILED(result))
            {
               return result;
            }
            // todo: need this?
            memset ((void*) vdec_ptr->modules.decoder.dec_state_ptr, 0, get_eamr_dec_struct_size ());
            result = eamr_decoder_frame_init ((void*) vdec_ptr->modules.decoder.dec_state_ptr, 0, (char*) "decoder");   // similar api for eAMR and AMR-NB init
            vdec_ptr->modules.decoder.amr_prev_rate = 7;   // todo: use marco
            break;
         }
      case VSM_MEDIA_TYPE_EAMR:
         {
            /* Don't reinit TTY here.  Assuming RESYNC_CTM is invoked */
            result = vdec_check_dec_struct_size (get_eamr_dec_struct_size (), vdec_ptr->session_num);
            if (ADSP_FAILED(result))
            {
               return result;
            }
            // todo: need this?
            memset ((void*) vdec_ptr->modules.decoder.dec_state_ptr, 0, get_eamr_dec_struct_size ());
            result = eamr_decoder_frame_init ((void*) vdec_ptr->modules.decoder.dec_state_ptr, 0, (char*) "decoder");
            vdec_ptr->modules.decoder.amr_prev_rate = 7;   // todo: use marco
            break;
         }
      case VSM_MEDIA_TYPE_AMR_WB_MODEM:
         {
            /* Don't reinit TTY here.  Assuming RESYNC_CTM is invoked */
            result = vdec_check_dec_struct_size (get_amr_wb_dec_struct_size (), vdec_ptr->session_num);
            if (ADSP_FAILED(result))
            {
               return result;
            }
            // todo: need this?
            memset ((void*) vdec_ptr->modules.decoder.dec_state_ptr, 0, get_amr_wb_dec_struct_size ());
            amrwb_init_decoder ((void*) vdec_ptr->modules.decoder.dec_state_ptr);
            vdec_ptr->modules.decoder.amr_prev_rate = 7;   // todo: use macro
            break;
         }
      case VSM_MEDIA_TYPE_EFR_MODEM:
         {
            result = vdec_check_dec_struct_size (get_efr_dec_struct_size (), vdec_ptr->session_num);
            if (ADSP_FAILED(result))
            {
               return result;
            }
            // todo: need this?
            memset ((void*) vdec_ptr->modules.decoder.dec_state_ptr, 0, get_efr_dec_struct_size ());
            efr_reset_dec ((void*) vdec_ptr->modules.decoder.dec_state_ptr);
            break;
         }
      case VSM_MEDIA_TYPE_FR_MODEM:
         {
            // todo: need this?
            result = vdec_check_dec_struct_size (get_fr_dec_struct_size (), vdec_ptr->session_num);
            if (ADSP_FAILED(result))
            {
               return result;
            }
            memset ((void*) vdec_ptr->modules.decoder.dec_state_ptr, 0, get_fr_dec_struct_size ());
            gsm_fr_decoderInit ((void*) vdec_ptr->modules.decoder.dec_state_ptr);
            break;
         }
      case VSM_MEDIA_TYPE_HR_MODEM:
         {
            // todo: need this?
            result = vdec_check_dec_struct_size (gsmhr_get_hr_dec_struct_size (), vdec_ptr->session_num);
            if (ADSP_FAILED(result))
            {
               return result;
            }
            memset ((void*) vdec_ptr->modules.decoder.dec_state_ptr, 0, gsmhr_get_hr_dec_struct_size ());
            gsmhr_resetDec ((void*) vdec_ptr->modules.decoder.dec_state_ptr);
            break;
         }
      case VSM_MEDIA_TYPE_PCM_8_KHZ:
      case VSM_MEDIA_TYPE_PCM_16_KHZ:
      case VSM_MEDIA_TYPE_PCM_32_KHZ:
      case VSM_MEDIA_TYPE_PCM_48_KHZ:
         {
            break;
         }
      case VSM_MEDIA_TYPE_PCM_44_1_KHZ:
         {
            int16_t voice_resamp_mode_16_bit = (GEN_RESAMP_LP_LPH << 6) | GEN_RESAMP_FIXED_OUTPUT_MODE; /* Resampler Quality: Low Power Linear Phase, FIXED OUTPUT mode*/
            ADSPResult resampler_result = voice_gen_resamp_init (&(vdec_ptr->modules.codec_resampler), (int32) VOICE_44_1_SAMPLING_RATE, (int32) VOICE_FB_SAMPLING_RATE, voice_resamp_mode_16_bit);
            if ( ADSP_EOK != resampler_result)
            {
               MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: Generic Resampler initialization failed for decoder session(%x)", (int )vdec_ptr->session_num);
               return ADSP_EFAILED;
            }
            break;
         }
      case VSM_MEDIA_TYPE_4GV_NB_MODEM:
      case VSM_MEDIA_TYPE_4GV_WB_MODEM:
      case VSM_MEDIA_TYPE_4GV_NW_MODEM:
      case VSM_MEDIA_TYPE_4GV_NW:
      case VSM_MEDIA_TYPE_EVRC_NW_2K:
         {
            //some initializations used in init_decoder function.
            FourGVDecode* fourGV_decode_ptr = (FourGVDecode*) vdec_ptr->modules.decoder.fourGV_decode_ptr;
            fourGV_decode_ptr->Fsop = 16000;
            fourGV_decode_ptr->Fsinp = 16000;

            if (VSM_MEDIA_TYPE_4GV_WB_MODEM == vdec_ptr->voc_type)
            {
               fourGV_decode_ptr->serviceOption = 70;
            }
            else if ((VSM_MEDIA_TYPE_4GV_NW_MODEM == vdec_ptr->voc_type) || (VSM_MEDIA_TYPE_4GV_NW == vdec_ptr->voc_type))
            {
               fourGV_decode_ptr->serviceOption = 73;
            }
            else if (VSM_MEDIA_TYPE_EVRC_NW_2K == vdec_ptr->voc_type)
            {
               fourGV_decode_ptr->serviceOption = 77;
            }
            else
            {
               fourGV_decode_ptr->Fsop = 8000;
               fourGV_decode_ptr->Fsinp = 8000;
               fourGV_decode_ptr->serviceOption = 68;
            }
            //initialize TW structure
            vdec_ptr->modules.decoder.fourGV_tw_struct.tw_enable = 0;
            vdec_ptr->modules.decoder.fourGV_tw_struct.phase_matching_enable = 0;
            vdec_ptr->modules.decoder.fourGV_tw_struct.tw_output_len =
               VOICE_FRAME_SIZE_NB;
            vdec_ptr->modules.decoder.fourGV_tw_struct.tw_fraction = 0x4000; //1in Q14
            vdec_ptr->modules.decoder.fourGV_tw_struct.tw_phase_offset = 0;
            vdec_ptr->modules.decoder.fourGV_tw_struct.tw_run_length = 0;

            fourGV_decode_ptr->init_decoder ();
            break;
         }
      case VSM_MEDIA_TYPE_13K_MODEM:
         {
            result = vdec_check_dec_struct_size (v13k_get_dec_struct_size (), vdec_ptr->session_num);
            if (ADSP_FAILED(result))
            {
               return result;
            }
            // todo: need this?
            memset (vdec_ptr->modules.decoder.dec_state_ptr, 0, v13k_get_dec_struct_size ());
            v13k_init_dec13 ((void*) vdec_ptr->modules.decoder.dec_state_ptr);
            break;
         }
      case VSM_MEDIA_TYPE_G711_ALAW:
      case VSM_MEDIA_TYPE_G711_MLAW:
      case VSM_MEDIA_TYPE_G711_ALAW_V2:
      case VSM_MEDIA_TYPE_G711_MLAW_V2:
         {
            vdec_ptr->modules.decoder.g711_frame_size = 80;
            if (VSM_MEDIA_TYPE_G711_ALAW_V2==vdec_ptr->voc_type || VSM_MEDIA_TYPE_G711_MLAW_V2 == vdec_ptr->voc_type)
            {
               vdec_ptr->modules.decoder.g711_frame_size = 160;
            }
            result = vdec_check_dec_struct_size (G711DecoderGetSize (), vdec_ptr->session_num);
            if (ADSP_FAILED(result))
            {
               return result;
            }
            memset (vdec_ptr->modules.decoder.dec_state_ptr, 0, G711DecoderGetSize ());
            G711A2DecoderInit((void *) vdec_ptr->modules.decoder.dec_state_ptr,vdec_ptr->modules.decoder.g711_frame_size);
            break;
         }
      case VSM_MEDIA_TYPE_G729AB:
         {
            result = vdec_check_dec_struct_size ((g729a_get_g729a_dec_struct_size () + g729a_get_g729a_dec_com_struct_size ()), vdec_ptr->session_num);
            if (ADSP_FAILED(result))
            {
               return result;
            }
            // todo: need this?
            memset (vdec_ptr->modules.decoder.dec_state_ptr, 0, (g729a_get_g729a_dec_struct_size () + g729a_get_g729a_dec_com_struct_size ()));
            vdec_ptr->modules.decoder.g729a_decoder_ptr = (int8_t *) vdec_ptr->modules.decoder.dec_state_ptr;
            vdec_ptr->modules.decoder.g729a_common_ptr = vdec_ptr->modules.decoder.g729a_decoder_ptr + g729a_get_g729a_dec_struct_size ();
            // todo: replace with memset
            for (uint8_t sCounterI = 0; sCounterI < G729_DEC_M; sCounterI++)
            {
               vdec_ptr->modules.decoder.synth_buf[sCounterI] = 0;
            }
            g729_initDec (vdec_ptr->modules.decoder.g729a_decoder_ptr, vdec_ptr->modules.decoder.g729a_common_ptr);
            break;
         }
      case VSM_MEDIA_TYPE_EVS:
         {
            result = vdec_check_dec_struct_size(get_evs_dec_struct_size(),vdec_ptr->session_num);
            if (ADSP_FAILED(result))
            {
               return result;
            }
            // todo: need this?
            memset((void*)vdec_ptr->modules.decoder.dec_state_ptr,0,get_evs_dec_struct_size());
            evs_voc_dec_init ((void*)vdec_ptr->modules.decoder.dec_state_ptr, (int)vdec_ptr->evs_samp_rate );
            break;
         }
      default:
         MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: Invalid decoder type: 0x%lx", vdec_ptr->voc_type);
         vdec_ptr->voc_type = VSM_MEDIA_TYPE_NONE;
         result = ADSP_EBADPARAM;
         break;
   }

   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: Init vdec in run cmd : session(%lx)", vdec_ptr->session_num);

   return result;
}

//APR commands have some used in main thread and some in pp thread.
//commands used in main thread, must be handled here -  VOICE_EVT_PUSH_HOST_BUF_V2
//commands used in pp thread must be handled in pp thread itself, hence fwd - VSM_CMD_SET_RX_DTMF_DETECTION
static ADSPResult vdec_apr_cmd (void* instance_ptr, elite_msg_any_t* msg_ptr)
{
   ADSPResult result = APR_EOK;
   elite_apr_packet_t * pAprPacket;
   int32_t rc = ADSP_EOK;
   vdec_t *vdec_ptr = (vdec_t*) instance_ptr;

   assert(msg_ptr);

   pAprPacket = (elite_apr_packet_t*) msg_ptr->pPayload;

   MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: VoiceDecApr Message handler Invoked, (Op,Token,Session):(%lx,%lx,%lx)", elite_apr_if_get_opcode (pAprPacket), elite_apr_if_get_client_token (pAprPacket),
         vdec_ptr->session_num);

   /* parse out the received pakcet.  Note in this current framework we are not prioritizing commands
    * that can be completed immediately.  We are simply processing command in order they are received */

   switch (elite_apr_if_get_opcode (pAprPacket))
   {

      case VSM_CMD_SET_RX_DTMF_DETECTION:
         {
            vsm_set_rx_dtmf_detection_t *pDtmfDetect = (vsm_set_rx_dtmf_detection_t *) elite_apr_if_get_payload_ptr (pAprPacket);
            MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO, "VSM_CMD_SET_RX_DTMF_DETECTION, enable(%ld), session(%lx)", pDtmfDetect->enable, vdec_ptr->session_num);

            {
               uint32_t enable = pDtmfDetect->enable;
               if (VSM_RX_DTMF_DETECTION_ENABLE == pDtmfDetect->enable)
               {
                  if ( NULL != vdec_ptr->modules.vdec_pp_modules.dtmf_det.apr_info_dtmf.client_addr ||
                        NULL != vdec_ptr->modules.vdec_pp_modules.dtmf_det.apr_info_dtmf.client_port)
                  {
                     /* already have a client, should not happen */
                     rc = elite_apr_if_end_cmd (vdec_ptr->apr_info_ptr->apr_handle, pAprPacket,
                           APR_ENOTREADY);
                  }
                  else
                  {
                     /* save self/client info for DTMF status updates.  Client is source address from incoming packet */
                     vdec_ptr->modules.vdec_pp_modules.dtmf_det.apr_info_dtmf.self_addr = elite_apr_if_get_dst_addr (pAprPacket);
                     vdec_ptr->modules.vdec_pp_modules.dtmf_det.apr_info_dtmf.self_port = elite_apr_if_get_dst_port (pAprPacket);
                     vdec_ptr->modules.vdec_pp_modules.dtmf_det.apr_info_dtmf.client_addr = elite_apr_if_get_src_addr (pAprPacket);
                     vdec_ptr->modules.vdec_pp_modules.dtmf_det.apr_info_dtmf.client_port = elite_apr_if_get_src_port (pAprPacket);

                     MSG_3(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: VSM_CMD_SET_RX_DTMF_DETECTION, saving clientAddr(%x) clientPort(%x) :session(%lx)",
                           vdec_ptr->modules.vdec_pp_modules.dtmf_det.apr_info_dtmf.client_addr, vdec_ptr->modules.vdec_pp_modules.dtmf_det.apr_info_dtmf.client_port, vdec_ptr->session_num);

                     /* initialize to init mem/circ buffers.  currently no mem alloc.  TBD: proper place? */
                     /* enable the module.  Not allowed via VOICE_CMD_SET_PARAM */
                     voice_dtmf_detect_set_param (&(vdec_ptr->modules.vdec_pp_modules.dtmf_det.dtmf_det_struct), (int8_t *) &enable, VOICE_PARAM_MOD_ENABLE, sizeof(enable));

                     rc = elite_apr_if_end_cmd (vdec_ptr->apr_info_ptr->apr_handle, pAprPacket, APR_EOK);
                  }
               }
               else
               {
                  /* disabling DTMF, reset aprInfo */
                  vdec_ptr->modules.vdec_pp_modules.dtmf_det.apr_info_dtmf.self_addr =
                     NULL;
                  vdec_ptr->modules.vdec_pp_modules.dtmf_det.apr_info_dtmf.self_port =
                     NULL;
                  vdec_ptr->modules.vdec_pp_modules.dtmf_det.apr_info_dtmf.client_addr =
                     NULL;
                  vdec_ptr->modules.vdec_pp_modules.dtmf_det.apr_info_dtmf.client_port =
                     NULL;
                  voice_dtmf_detect_set_param (&(vdec_ptr->modules.vdec_pp_modules.dtmf_det.dtmf_det_struct), (int8_t *) &enable, VOICE_PARAM_MOD_ENABLE, sizeof(enable));
                  rc = elite_apr_if_end_cmd (vdec_ptr->apr_info_ptr->apr_handle, pAprPacket, APR_EOK);
               }
            }
            break;
         }
      case VSM_EVT_OUTOFBAND_TTY_RX_CHAR_PUSH:

         {
            vsm_evt_outofband_tty_rx_char_push_t *pLTETTYRx = (vsm_evt_outofband_tty_rx_char_push_t *) elite_apr_if_get_payload_ptr (pAprPacket);
            MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: VSM_EVT_OUTOFBAND_TTY_RX_CHAR_PUSH, tty_char(%d), session(%lx)", pLTETTYRx->tty_char, vdec_ptr->session_num);
            uint32_t client_token = elite_apr_if_get_client_token (pAprPacket);
            /* Check if previous char push is done */
            if ((FALSE == vdec_ptr->modules.vdec_pp_modules.oobtty.oobtty_char_flag) && (TRUE == vdec_ptr->oobtty_enable))
            {
               /* Save the new character and client token to interface variables in the structures */
               vdec_ptr->modules.vdec_pp_modules.oobtty.oobtty_rx_char = pLTETTYRx->tty_char;
               vdec_ptr->modules.vdec_pp_modules.oobtty.oobtty_client_token = client_token;
               vdec_ptr->modules.vdec_pp_modules.oobtty.oobtty_char_flag = TRUE;
            }
            else
            {
               /* Since the previous char is not yet pushed or mode is not properly set, need to send error event to notify client that char push failed */
               MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: Error ! OOB TTY previous char push not done or Rx TTY enable(%d) not proper, token(%d), session(%lx)", (int )vdec_ptr->oobtty_enable,
                     (int )client_token, vdec_ptr->session_num);
               rc = elite_apr_if_alloc_send_event (vdec_ptr->apr_info_ptr->apr_handle, vdec_ptr->apr_info_ptr->self_addr, vdec_ptr->apr_info_ptr->self_port, vdec_ptr->apr_info_ptr->client_addr,
                     vdec_ptr->apr_info_ptr->client_port, client_token,
                     VSM_EVT_OUTOFBAND_TTY_ERROR,
                     NULL, 0);
            }
            // free the event since not done internally
            rc = elite_apr_if_free (vdec_ptr->apr_info_ptr->apr_handle, pAprPacket);
            break;
         }
      case VOICE_EVT_PUSH_HOST_BUF_V2:
         {
            ADSPResult local_result = APR_EOK;
            // handle the event, message info will get copied and queued.  No policy on write buffers for multichannel topology,
            // can mark all as valid
            local_result = voice_host_pcm_buffer_rd_wr (&vdec_ptr->modules.host_pcm_context, msg_ptr,
                  BUF_VALID);
            if (ADSP_FAILED(local_result))
            {
               MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: vdec-error-cannot process voice_evt_push_host_pcm_buf_t, result(%#x) session(%#lx)", local_result, vdec_ptr->session_num);
            }

            // free the event since not done internally
            rc = elite_apr_if_free (vdec_ptr->modules.host_pcm_context.apr_handle, pAprPacket);
            break;
         }
      case VSM_EVT_OOB_DEC_BUF_READY:
         {
            // set flag to indicate OOB pkt is ready
            if (vdec_ptr->pkt_xchg.dec_pkt_oob_ready == TRUE)
            {
               MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: Previous OOB pkt not yet read by Decoder, and hence OVERWRITTEN (%lx)", vdec_ptr->session_num);
            }
            vdec_ptr->pkt_xchg.dec_pkt_oob_ready = TRUE;
            vdec_ptr->pkt_xchg.oob_pkt_ready_ctr++;
            MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: VDEC : VSM_EVT_OOB_DEC_BUF_READY received number (%d) session(%lx)", (int )vdec_ptr->pkt_xchg.oob_pkt_ready_ctr, vdec_ptr->session_num);
            // free the event since not done internally
            rc = elite_apr_if_free (vdec_ptr->apr_info_ptr->apr_handle, pAprPacket);
            break;

         }
      case VSM_CMD_SET_EAMR_MODE_CHANGE_DETECTION:
         {
            vsm_set_eamr_mode_change_detection_t *pEamrMode = (vsm_set_eamr_mode_change_detection_t *) elite_apr_if_get_payload_ptr (pAprPacket);
            MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: VSM_CMD_SET_EAMR_MODE_CHANGE_DETECTION, enable(%ld), session(%lx)", pEamrMode->enable, vdec_ptr->session_num);

            if ((VSM_EAMR_MODE_CHANGE_DETECTION_DISABLE == pEamrMode->enable) || (VSM_EAMR_MODE_CHANGE_DETECTION_ENABLE == pEamrMode->enable))
            {
               vdec_ptr->modules.decoder.eamr_mode_flag = pEamrMode->enable;
            }
            else
            {
               MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: Invalid eamr mode flag session(%x), enable(%lx)", (int )vdec_ptr->session_num, pEamrMode->enable);
               result = ADSP_EUNSUPPORTED;
            }
            rc = elite_apr_if_end_cmd (vdec_ptr->apr_info_ptr->apr_handle, pAprPacket, result);
            break;
         }
      default:
         {
            /* Handle error. */
            if (elite_apr_if_msg_type_is_cmd (pAprPacket))
            { /* Complete unsupported commands. */
               rc = elite_apr_if_end_cmd (vdec_ptr->apr_info_ptr->apr_handle, pAprPacket,
                     APR_EUNSUPPORTED);
            }
            else
            { /* Drop unsupported events. */
               rc = elite_apr_if_free (vdec_ptr->apr_info_ptr->apr_handle, pAprPacket);
            }
            result = ADSP_EUNSUPPORTED;
            break;
         }
   }
   if (ADSP_FAILED(rc))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: ERROR! Elite APR message handler has returned error %d, continuing...", (int )rc);
   }
   return result;
}


static ADSPResult vdec_process_amr_nb_modem (vdec_t* vdec_ptr)
{
   ADSPResult result = ADSP_EOK;
   int16_t *in_ptr = (int16_t*) &(vdec_ptr->in_buf[0]);   // pointer to local input buffer
   int16_t *out_ptr = (int16_t*) &(vdec_ptr->dec_out_buf[0]);   // pointer to local output buffer
   int16_t decode_input_buf[18];
   uint32_t pkt_size = 0;   // size in bytes
   int16 out_buf[320];    // to store WB output of eAMR decoder

   // Error checks
   if (FALSE == vdec_ptr->modules.decoder.erasure)
   {
      int8_t frame_type, rate_type;

      //get frame type which will be in rxFrameType enum
      frame_type = (in_ptr[0] & 0xf0) >> 4;
      //get mode
      rate_type = (in_ptr[0] & 0x0f);

      if (rate_type >= 8)
      {
         MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: Invalid AmrNb rate session(%lx)", vdec_ptr->session_num);
         vdec_ptr->modules.decoder.erasure = TRUE;   // signal erasure
      }
      //In case of no_data and onset frametype, packet size is 1 byte
      //which is just the header with no information bits
      else if ((RX_ONSET == frame_type) || (RX_NO_DATA == frame_type))
      {
         pkt_size = 1;   //one byte header
      }
      //In case of sid_first, sid_update and sid_bad frametypes packet size is 6 bytes
      //39 - information bits, 8 - header, 7 - to make it octect aligned
      else if ((RX_SID_FIRST == frame_type) || (RX_SID_UPDATE == frame_type) || (RX_SID_BAD == frame_type))
      {
         pkt_size = (39 + 8 + 7) / 8;
      }
      //In case of speech_good, speech_degraded and speech_bad frametypes
      //packet size depends on the mode.
      //
      else if ((RX_SPEECH_GOOD == frame_type) || (RX_SPEECH_DEGRADED == frame_type) || (RX_SPEECH_BAD == frame_type))
      {
         pkt_size = ((num_bits_for_modemIF1[rate_type] + 7) / 8);
      }
      else
      {
         MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: Invalid AmrNb frame type session(%lx)", vdec_ptr->session_num);
         vdec_ptr->modules.decoder.erasure = TRUE;   // signal erasure
      }

      if (FALSE == vdec_ptr->modules.decoder.erasure)
      {
         if (pkt_size < vdec_ptr->in_buf_size)
         {
            MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: More than one frame session(%lx)", vdec_ptr->session_num);
         }
         else if (pkt_size > vdec_ptr->in_buf_size)
         {
            MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: Less than one frame session(%lx)", vdec_ptr->session_num);
            vdec_ptr->modules.decoder.erasure = TRUE;
         }
      }
   }

   //check for erasure flag
   if (TRUE == vdec_ptr->modules.decoder.erasure)
   {
      int8_t *in_byte_ptr = (int8_t *) in_ptr;
      in_byte_ptr[0] = 0x70;   // indicates NO_DATA frame
      pkt_size = 1;
   }

   int16_t beamr_enable = ((VOICE_BBWE_BEAMR == vdec_ptr->voice_module_bbwe) ? TRUE : FALSE);
   uint16_t sampling_rate = voice_get_sampling_rate (vdec_ptr->voc_type, beamr_enable, vdec_ptr->evs_samp_rate);
   // log packet in Q6 format
   vdec_log_vocoder_packet (vdec_ptr->voc_type, sampling_rate, ((sampling_rate == 16000) << 3) | vdec_ptr->session_num, in_ptr, pkt_size);

   // Call de_framing function to convert modem IF1 pkt to DSP std pkt data
   eamrsup_modem_if1_de_framing_main ((uint8_t *) &decode_input_buf[0], (uint8_t *) in_ptr, (uint8_t *) &vdec_ptr->modules.decoder.amr_prev_rate,
         CODEC_TYPE_AMR_NB);

   if (VSM_MEDIA_TYPE_AMR_NB_MODEM == vdec_ptr->voc_type)
   {
      // AMR-NB core decoding
      if (beamr_enable)
      {
         eamr_decode ((void*) vdec_ptr->modules.decoder.dec_state_ptr, decode_input_buf, out_buf, out_ptr, 0/*disable_WM*/, 1);   // BeAMR enabled for AMR-NB
      }
      else
      {
         eamr_decode ((void*) vdec_ptr->modules.decoder.dec_state_ptr, decode_input_buf, out_ptr, out_buf, 0/*disable_WM*/, 0);   // BeAMR disabled for AMR-NB
      }
   }
   else
   {
      eamr_decode ((void*) vdec_ptr->modules.decoder.dec_state_ptr, decode_input_buf, out_buf, out_ptr, 1/*enable_WM*/, beamr_enable);

      if ((VSM_EAMR_MODE_CHANGE_DETECTION_ENABLE == vdec_ptr->modules.decoder.eamr_mode_flag) || (vdec_ptr->modules.decoder.vocoder_op_detection))
      {
         // check mode has changed in this frame
         ADSPResult local_result = ADSP_EOK;
         uint32_t eamr_curr_bw_det_state = get_eamr_bw_det_state ((void*) vdec_ptr->modules.decoder.dec_state_ptr);

         // DBG: MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: eamr_curr_bw_det_state = %d ",(int)eamr_curr_bw_det_state);

         if (vdec_ptr->modules.decoder.eamr_last_bw_det_state != eamr_curr_bw_det_state)
         {
            if (VSM_EAMR_MODE_CHANGE_DETECTION_ENABLE == vdec_ptr->modules.decoder.eamr_mode_flag)
            {
               local_result = vdec_send_mode_notification (vdec_ptr, eamr_curr_bw_det_state);
               if (ADSP_FAILED(local_result))
               {
                  MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: Failed to send mode change notification, result=%d session(%x)", (int )local_result, (int )vdec_ptr->session_num);
               }
               vdec_ptr->modules.decoder.eamr_last_bw_det_state = eamr_curr_bw_det_state;
            }
            else
            {
               if (VSM_EAMR_MODE_NARROWBAND == eamr_curr_bw_det_state)
               {
                  vdec_ptr->modules.decoder.session_op_mode =
                     VSM_VOC_OPERATING_MODE_NB;
               }
               else
               {
                  vdec_ptr->modules.decoder.session_op_mode =
                     VSM_VOC_OPERATING_MODE_WB;
               }
               vdec_send_mode_notification_v2 (vdec_ptr);
               vdec_ptr->modules.decoder.eamr_last_bw_det_state = eamr_curr_bw_det_state;
            }
         }
      }
   }

   return result;
}

static ADSPResult vdec_process_amr_nb_if2 (vdec_t* vdec_ptr)
{
   ADSPResult result = ADSP_EOK;
   int16_t decode_input_buf[18];
   int16_t *in_ptr = (int16_t*) &(vdec_ptr->in_buf[0]);   // pointer to local input buffer
   int16_t *out_ptr = (int16_t*) &(vdec_ptr->dec_out_buf[0]);   // pointer to local output buffer
   uint32_t pkt_size = 0;   // size in bytes
   int16 out_buf[320];     // to store WB output of eAMR decoder
   // Error checks
   if (FALSE == vdec_ptr->modules.decoder.erasure)
   {
      int8_t frame_type = (in_ptr[0] & 0x0f);   // extract frame type
      if (((frame_type > 8) && (frame_type < 15)) || (frame_type > 15))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: Invalid AmrNbIf2 frame type session(%lx)", vdec_ptr->session_num);
         vdec_ptr->modules.decoder.erasure = TRUE;
      }
      else
      {
         pkt_size = ((num_bits_forIF2[frame_type] + 7) / 8);
         //check if split or multiple frames
         if ((pkt_size < vdec_ptr->in_buf_size) && (pkt_size > 0))
         {
            MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: More than one frame session(%lx)", vdec_ptr->session_num);
         }
         else if (pkt_size > vdec_ptr->in_buf_size)
         {
            MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: Less than one frame session(%lx)", vdec_ptr->session_num);
            vdec_ptr->modules.decoder.erasure = TRUE;
         }
         else if (0 == pkt_size)
         {
            MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: Unexpected FrameType or Rate session(%lx)", vdec_ptr->session_num);
            vdec_ptr->modules.decoder.erasure = TRUE;
         }   //else, pkt_size ==  in_buf_size
      }
   }

   //check for erasure flag
   if (TRUE == vdec_ptr->modules.decoder.erasure)
   {
      int8_t *in_byte_ptr = (int8_t *) in_ptr;
      in_byte_ptr[0] = 0xF;   // indicates NO_DATA frame
      pkt_size = 1;
   }

   int16_t beamr_enable = ((VOICE_BBWE_BEAMR == vdec_ptr->voice_module_bbwe) ? TRUE : FALSE);
   uint16_t sampling_rate = voice_get_sampling_rate (vdec_ptr->voc_type, beamr_enable, vdec_ptr->evs_samp_rate);
   // log packet in Q6 format
   vdec_log_vocoder_packet (vdec_ptr->voc_type, sampling_rate, ((sampling_rate == 16000) << 3) | vdec_ptr->session_num, in_ptr, pkt_size);

   //call de_framing function to convert IF2 pkt to DSP std pkt data
   eamrsup_if2_de_framing_main ((uint8_t *) &decode_input_buf[0], (uint8_t *) in_ptr, (uint8_t *) &vdec_ptr->modules.decoder.amr_prev_rate);

   // AMR-NB core decoding
   if (beamr_enable)
   {
      eamr_decode ((void*) vdec_ptr->modules.decoder.dec_state_ptr, decode_input_buf, out_buf, out_ptr, 0/*disable_WM*/, beamr_enable);   // BeAMR enabled for AMR-NB
   }
   else
   {
      eamr_decode ((void*) vdec_ptr->modules.decoder.dec_state_ptr, decode_input_buf, out_ptr, out_buf, 0/*disable_WM*/, beamr_enable);   // BeAMR disabled for AMR-NB
   }
   return result;
}

static ADSPResult vdec_process_amr_wb_modem (vdec_t* vdec_ptr)
{
   ADSPResult result = ADSP_EOK;
   int16_t *in_ptr = (int16_t*) &(vdec_ptr->in_buf[0]);   // pointer to local input buffer
   int16_t *out_ptr = (int16_t*) &(vdec_ptr->dec_out_buf[0]);   // pointer to local output buffer
   int16_t decode_input_buf[32];
   uint32_t pkt_size = 0;   // size in bytes

   // Error checks
   if (FALSE == vdec_ptr->modules.decoder.erasure)
   {
      int8_t frame_type, rate_type;

      //get frame type which will be in rxFrameType enum
      frame_type = (in_ptr[0] & 0xf0) >> 4;
      //get mode
      rate_type = (in_ptr[0] & 0x0f);

      if (rate_type >= 9)
      {
         MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: Invalid AmrWb rate session(%lx)", vdec_ptr->session_num);
         vdec_ptr->modules.decoder.erasure = TRUE;   // signal erasure
      }
      //In case of no_data and onset frametype, packet size is 1 byte
      //which is just the header with no information bits
      else if ((AMRSUP_ONSET == frame_type) || (AMRSUP_NO_DATA == frame_type) || (AMRSUP_SPEECH_LOST == frame_type))
      {
         pkt_size = 1;   //one byte header
      }
      //In case of sid_first, sid_update and sid_bad frametypes packet size is 6 bytes
      //39 - information bits, 8 - header, 7 - to make it octect aligned
      else if ((AMRSUP_SID_FIRST == frame_type) || (AMRSUP_SID_UPDATE == frame_type) || (AMRSUP_SID_BAD == frame_type))
      {
         pkt_size = (40 + 8 + 7) / 8;
      }
      //In case of speech_good, speech_degraded and speech_bad frametypes
      //packet size depends on the mode.
      else if ((AMRSUP_SPEECH_GOOD == frame_type) || (AMRSUP_SPEECH_DEGRADED == frame_type) || (AMRSUP_SPEECH_BAD == frame_type))
      {
         pkt_size = ((amrWBNum_bits_for_modemIF1[rate_type] + 7) / 8);
      }
      else
      {
         MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: Invalid AmrWb frame type session(%lx)", vdec_ptr->session_num);
         vdec_ptr->modules.decoder.erasure = TRUE;   // signal erasure
      }

      if (FALSE == vdec_ptr->modules.decoder.erasure)
      {
         if (pkt_size < vdec_ptr->in_buf_size)
         {
            MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: More than one frame session(%lx)", vdec_ptr->session_num);
         }
         else if (pkt_size > vdec_ptr->in_buf_size)
         {
            MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: Less than one frame session(%lx)", vdec_ptr->session_num);
            vdec_ptr->modules.decoder.erasure = TRUE;
         }
      }
   }

   //check for erasure flag
   if (TRUE == vdec_ptr->modules.decoder.erasure)
   {
      int8_t *in_byte_ptr = (int8_t *) in_ptr;
      in_byte_ptr[0] = 0x70;   // NO_DATA frame
      pkt_size = 1;
   }

   // log packet in Q6 format
   uint16_t sampling_rate = voice_get_sampling_rate (vdec_ptr->voc_type, 0/*beamr is dont care*/,  vdec_ptr->evs_samp_rate);
   vdec_log_vocoder_packet (vdec_ptr->voc_type, sampling_rate, ((sampling_rate == 16000) << 3) | vdec_ptr->session_num, in_ptr, pkt_size);

   //call de_framing function to convert modem IF1 pkt to DSP std pkt data
   amrwb_sup_modem_if1_de_framing_main ((uint8_t *) &decode_input_buf[0], (uint8_t *) in_ptr, (uint8_t *) &vdec_ptr->modules.decoder.amr_prev_rate,
         CODEC_TYPE_AMR_WB);

   // AMR-WB core decoding
   amrwb_dec_api (decode_input_buf, out_ptr, (void*) vdec_ptr->modules.decoder.dec_state_ptr);

   return result;
}

static ADSPResult vdec_process_evrc (vdec_t* vdec_ptr)
{
   ADSPResult result = ADSP_EOK;
   int8_t tty_dec_flag = VOICE_DEC_1X_NON_TTY;
   int16_t *in_ptr = (int16_t*) &(vdec_ptr->in_buf[0]);   // pointer to local input buffer
   int16_t *out_ptr = (int16_t*) &(vdec_ptr->dec_out_buf[0]);   // pointer to local output buffer
   uint32_t pkt_size = 0;

   // Error checks
   if (FALSE == vdec_ptr->modules.decoder.erasure)
   {
      int8_t pkt_rate_type;
      int8_t *in_temp_ptr = (int8_t*) &(vdec_ptr->in_buf[0]);   // pointer to local input buffer
      // Extract Packet Type information from Header
      // while discarding Erasure and other info
      pkt_rate_type = (in_temp_ptr[0] & 0xF);
      if (!((pkt_rate_type == 0) || (pkt_rate_type == 1) || (pkt_rate_type == 3) || (pkt_rate_type == 4) || (pkt_rate_type == 0xE)))
      {
         MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: Invalid EVRC rate type = %d session(%lx)", pkt_rate_type, vdec_ptr->session_num);
         vdec_ptr->modules.decoder.erasure = TRUE;
      }
      // Known incorrect EVRC packet type, Signal erasure (Workaround for BMP)
      else if ((pkt_rate_type == 3) && (0 == (in_temp_ptr[2] & 0xFF)) && (0 == (in_temp_ptr[3] & 0xFF)) && (0 == (in_temp_ptr[4] & 0xFF)))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: Invalid EVRC pkt content session(%lx)", vdec_ptr->session_num);
         vdec_ptr->modules.decoder.erasure = TRUE;
      }
      else
      {
         pkt_size = evrc_num_bits_for_modem[pkt_rate_type];
         if ((pkt_size < vdec_ptr->in_buf_size) && (pkt_size > 0))
         {
            MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: More than one frame session(%lx)", vdec_ptr->session_num);
         }
         else if (pkt_size > vdec_ptr->in_buf_size)
         {
            MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: Less than one frame session(%lx)", vdec_ptr->session_num);
            vdec_ptr->modules.decoder.erasure = TRUE;
         }
         else if (0 == pkt_size)
         {
            MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: Unexpected FrameType or Rate session(%lx)", vdec_ptr->session_num);
            vdec_ptr->modules.decoder.erasure = TRUE;
         }      //else, pkt_size ==  in_buf_size
      }
   }

   //check for erasure flag
   if (TRUE == vdec_ptr->modules.decoder.erasure)
   {
      int8_t *in_byte_ptr = (int8_t *) in_ptr;
      in_byte_ptr[0] = 0xE;   // indicates erasure frame
      pkt_size = 1;
   }

   // log packet in Q6 format
   uint16_t sampling_rate = voice_get_sampling_rate (vdec_ptr->voc_type, 0/*beamr is dont care*/, vdec_ptr->evs_samp_rate);
   vdec_log_vocoder_packet (vdec_ptr->voc_type, sampling_rate, ((sampling_rate == 16000) << 3) | vdec_ptr->session_num, in_ptr, pkt_size);

   //Re-packing from MVS data to DSP data
   evrc_repacking_mvs_to_dsp ((char_t *) in_ptr);

   tty_dec_flag = evrc_decode ((void*) vdec_ptr->modules.decoder.dec_state_ptr, in_ptr, out_ptr, 0);
   // inform CNG on Rx TTY detection.  0 -> non tty, 1 -> tty silence
   vdec_ptr->tty_state_ptr->m_rx_tty_detected = (tty_dec_flag == VOICE_DEC_1X_NON_TTY || tty_dec_flag == VOICE_DEC_1X_TTY_SILENCE) ?
      FALSE :
      TRUE;
   if (tty_dec_flag)
   {
      MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: EVRC Rx TTY Process, tty_dec_flag(%d) session(%lx)", tty_dec_flag, vdec_ptr->session_num);
   }

   return result;
}

static ADSPResult vdec_processV13k (vdec_t* vdec_ptr)
{
   ADSPResult result = ADSP_EOK;
   int8_t tty_dec_flag = VOICE_DEC_1X_NON_TTY;
   int16_t *in_ptr = (int16_t*) &(vdec_ptr->in_buf[0]);   // pointer to local input buffer
   int16_t *out_ptr = (int16_t*) &(vdec_ptr->dec_out_buf[0]);   // pointer to local output buffer
   uint32_t pkt_size = 0;

   // Error checks
   if (FALSE == vdec_ptr->modules.decoder.erasure)
   {
      int8_t pkt_rate_type;
      // Extract Packet Type information from Header
      // while discarding Erasure and other info
      pkt_rate_type = (in_ptr[0] & 0xF);
      if ((pkt_rate_type < 0) || ((pkt_rate_type > 4) && (pkt_rate_type != 0xe)))
      {
         MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: Invalid V13K rate type = %d session(%lx)", pkt_rate_type, vdec_ptr->session_num);
         vdec_ptr->modules.decoder.erasure = TRUE;
      }
      else
      {
         pkt_size = V13k_num_bits_for_modem[pkt_rate_type];
         if ((pkt_size < vdec_ptr->in_buf_size) && (pkt_size > 0))
         {
            MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: More than one frame session(%lx)", vdec_ptr->session_num);
         }
         else if (pkt_size > vdec_ptr->in_buf_size)
         {
            MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: Less than one frame session(%lx)", vdec_ptr->session_num);
            vdec_ptr->modules.decoder.erasure = TRUE;
         }
         else if (0 == pkt_size)
         {
            MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: Unexpected FrameType or Rate session(%lx)", vdec_ptr->session_num);
            vdec_ptr->modules.decoder.erasure = TRUE;
         }      //else, pkt_size ==  in_buf_size
      }
   }

   //check for erasure flag
   if (TRUE == vdec_ptr->modules.decoder.erasure)
   {
      int8_t *in_byte_ptr = (int8_t *) in_ptr;
      in_byte_ptr[0] = 0xE;   // indicates erasure frame
      pkt_size = 1;
   }

   // log packet in Q6 format
   uint16_t sampling_rate = voice_get_sampling_rate (vdec_ptr->voc_type, 0, vdec_ptr->evs_samp_rate);
   vdec_log_vocoder_packet (vdec_ptr->voc_type, sampling_rate, ((sampling_rate == 16000) << 3) | vdec_ptr->session_num, in_ptr, pkt_size);

   //Re-packing from MVS data to DSP data
   v13k_repacking_mvs_to_dsp ((int8_t *) in_ptr);

   tty_dec_flag = v13k_decoder13 ((void*) vdec_ptr->modules.decoder.dec_state_ptr, in_ptr, out_ptr, &(vdec_ptr->modules.decoder.tty_dec_struct));
   // inform CNG on Rx TTY detection.  0 -> non tty, 1 -> tty silence
   vdec_ptr->tty_state_ptr->m_rx_tty_detected = (tty_dec_flag == VOICE_DEC_1X_NON_TTY || tty_dec_flag == VOICE_DEC_1X_TTY_SILENCE) ?
      FALSE :
      TRUE;
   if (tty_dec_flag)
   {
      MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: V13K Rx TTY Process, tty_dec_flag(%d) session(%lx)", tty_dec_flag, vdec_ptr->session_num);
   }

   return result;
}

static ADSPResult vdec_process4GV (vdec_t* vdec_ptr)
{
   ADSPResult result = ADSP_EOK;
   int8_t tty_dec_flag = VOICE_DEC_1X_NON_TTY;
   int16_t *in_ptr = (int16_t*) &(vdec_ptr->in_buf[0]);   // pointer to local input buffer
   int16_t *out_ptr = (int16_t*) &(vdec_ptr->dec_out_buf[0]);   // pointer to local output buffer
   uint32_t pkt_size = 0;

   // Error checks
   if (FALSE == vdec_ptr->modules.decoder.erasure)
   {
      int8_t pkt_rate_type;
      // Extract Packet Type information from Header
      pkt_rate_type = (in_ptr[0] & 0xF);
      if ((pkt_rate_type < 0) || ((pkt_rate_type > 4) && (pkt_rate_type != 0xE)))
      {
         MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: Invalid 4GV rate type = %d session(%lx)", pkt_rate_type, vdec_ptr->session_num);
         vdec_ptr->modules.decoder.erasure = TRUE;
      }
      else
      {
         pkt_size = (fourgv_num_bits_for_modem[pkt_rate_type] + 7) / 8;
         if ((pkt_size < vdec_ptr->in_buf_size) && (pkt_size > 0))
         {
            MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: More than one frame session(%lx)", vdec_ptr->session_num);
         }
         else if (pkt_size > vdec_ptr->in_buf_size)
         {
            MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: Less than one frame session(%lx)", vdec_ptr->session_num);
            vdec_ptr->modules.decoder.erasure = TRUE;
         }
         else if (0 == pkt_size)
         {
            MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: Unexpected FrameType or Rate session(%lx)", vdec_ptr->session_num);
            vdec_ptr->modules.decoder.erasure = TRUE;
         }   //else, pkt_size ==  in_buf_size
      }
   }

   //check for erasure flag
   if (TRUE == vdec_ptr->modules.decoder.erasure)
   {
      int8_t *in_byte_ptr = (int8_t *) in_ptr;
      in_byte_ptr[0] = 0xE;   // indicates erasure frame
      pkt_size = 1;
   }

   // log packet in Q6 format
   uint16_t sampling_rate = voice_get_sampling_rate (vdec_ptr->voc_type, 0, vdec_ptr->evs_samp_rate);
   vdec_log_vocoder_packet (vdec_ptr->voc_type, sampling_rate, ((sampling_rate == 16000) << 3) | vdec_ptr->session_num, in_ptr, pkt_size);

   //Re-packing from MVS data to DSP data
   fourgv_repacking_mvs_to_dsp ((char_t *) in_ptr);

   FourGVDecode* fourGV_decode_ptr = (FourGVDecode*) vdec_ptr->modules.decoder.fourGV_decode_ptr;
   tty_dec_flag = fourGV_decode_ptr->decoder (in_ptr, out_ptr, sizeof(vdec_ptr->dec_out_buf), &(vdec_ptr->modules.decoder.tty_dec_struct), &(vdec_ptr->modules.decoder.fourGV_tw_struct));
   // inform CNG on Rx TTY detection.  0 -> non tty, 1 -> tty silence
   vdec_ptr->tty_state_ptr->m_rx_tty_detected = (tty_dec_flag == VOICE_DEC_1X_NON_TTY || tty_dec_flag == VOICE_DEC_1X_TTY_SILENCE) ?
      FALSE :
      TRUE;
   if (tty_dec_flag)
   {
      MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: 4GV Rx TTY Process, tty_dec_flag(%d) session(%lx)", tty_dec_flag, vdec_ptr->session_num);
   }
   //if TW inside 4GV is enabled and VFR_NONE, then output lenght is given by TW
   //structure
   if ((1 == vdec_ptr->modules.decoder.fourGV_tw_struct.tw_enable) && !vdec_ptr->vfr_mode)
   {
      vdec_ptr->dec_out_size_bytes = vdec_ptr->modules.decoder.fourGV_tw_struct.tw_output_len * 2;
   }

   // if mode has changed and if mode detection is enabled, issue event
   if (vdec_ptr->modules.decoder.vocoder_op_detection)
   {
      //retrieve mode bit
      int16_t mode_bit = (int16_t) fourGV_decode_ptr->fourgv_get_mode_bit ();
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: Mode bit from 4gv = %d", mode_bit);

      if ((0 == mode_bit) && (VSM_VOC_OPERATING_MODE_WB == vdec_ptr->modules.decoder.session_op_mode))
      {
         vdec_ptr->modules.decoder.session_op_mode = VSM_VOC_OPERATING_MODE_NB;
         vdec_send_mode_notification_v2 (vdec_ptr);
      }
      else if ((0 != mode_bit) && (VSM_VOC_OPERATING_MODE_NB == vdec_ptr->modules.decoder.session_op_mode))
      {
         vdec_ptr->modules.decoder.session_op_mode = VSM_VOC_OPERATING_MODE_WB;
         vdec_send_mode_notification_v2 (vdec_ptr);
      }
   }

   return result;
}

static ADSPResult vdec_process_efr (vdec_t* vdec_ptr)
{
   ADSPResult result = ADSP_EOK;
   int16_t *in_ptr = (int16_t*) &(vdec_ptr->in_buf[0]);   // pointer to local input buffer
   int16_t *out_ptr = (int16_t*) &(vdec_ptr->dec_out_buf[0]);   // pointer to local output buffer
   uint32_t pkt_size = 32;

   // Error checks
   if (FALSE == vdec_ptr->modules.decoder.erasure)
   {
      if (pkt_size < vdec_ptr->in_buf_size)
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: More than one frame session(%lx)", vdec_ptr->session_num);
      }
      else if (pkt_size > vdec_ptr->in_buf_size)
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: Less than one frame session(%lx)", vdec_ptr->session_num);
         vdec_ptr->modules.decoder.erasure = TRUE;
      }   //else, pkt_size ==  in_buf_size
   }

   //check for erasure flag
   if (TRUE == vdec_ptr->modules.decoder.erasure)
   {
      int8_t *in_byte_ptr = (int8_t *) in_ptr;
      in_byte_ptr[0] = 0x1;   // bfi (bad frame indicator) = 1
   }

   // log packet in Q6 format
   uint16_t sampling_rate = voice_get_sampling_rate (vdec_ptr->voc_type, 0, vdec_ptr->evs_samp_rate);
   vdec_log_vocoder_packet (vdec_ptr->voc_type, sampling_rate, ((sampling_rate == 16000) << 3) | vdec_ptr->session_num, in_ptr, pkt_size);

   //Re-packing from MVS data to DSP data
   gsm_efr_repacking_mvs_to_dsp ((int8_t *) in_ptr);

   // EFR core decoding
   int16_t *pOutPtr;
   efr_decoder ((void*) vdec_ptr->modules.decoder.dec_state_ptr, in_ptr, &pOutPtr);
   // todo: use voice_memsmove
   for (uint32_t i = 0; i < VOICE_FRAME_SIZE_NB; i++)
   {
      out_ptr[i] = pOutPtr[i];
   }

   return result;
}

static ADSPResult vdec_process_fr (vdec_t* vdec_ptr)
{
   ADSPResult result = ADSP_EOK;
   int16_t pkt_buffer[18];
   int16_t *in_ptr = (int16_t*) &(vdec_ptr->in_buf[0]);   // pointer to local input buffer
   int16_t *out_ptr = (int16_t*) &(vdec_ptr->dec_out_buf[0]);   // pointer to local output buffer
   uint32_t pkt_size = 34;

   // Error checks
   if (FALSE == vdec_ptr->modules.decoder.erasure)
   {
      if (pkt_size < vdec_ptr->in_buf_size)
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: More than one frame session(%lx)", vdec_ptr->session_num);
      }
      else if (pkt_size > vdec_ptr->in_buf_size)
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: Less than one frame session(%lx)", vdec_ptr->session_num);
         vdec_ptr->modules.decoder.erasure = TRUE;
      }   //else, pkt_size ==  in_buf_size
   }

   //check for erasure flag
   if (TRUE == vdec_ptr->modules.decoder.erasure)
   {
      int8_t *in_byte_ptr = (int8_t *) in_ptr;
      in_byte_ptr[0] = 0x1;   // bfi (bad frame indicator) = 1
   }

   // log packet in Q6 format
   uint16_t sampling_rate = voice_get_sampling_rate (vdec_ptr->voc_type, 0/*beamr is dont care*/, vdec_ptr->evs_samp_rate);
   vdec_log_vocoder_packet (vdec_ptr->voc_type, sampling_rate, ((sampling_rate == 16000) << 3) | vdec_ptr->session_num, in_ptr, pkt_size);

   //Re-packing from MVS data to DSP data
   gsm_fr_repacking_mvs_to_dsp (reinterpret_cast<int8_t*> (in_ptr), pkt_buffer);

   // FR core decoding
   gsm_fr_decoder ((void*) vdec_ptr->modules.decoder.dec_state_ptr, pkt_buffer, out_ptr,
         FR_DEC_FROM_VOICE_PATH);

   return result;
}

static ADSPResult vdec_process_hr (vdec_t* vdec_ptr)
{
   ADSPResult result = ADSP_EOK;
   int16_t *in_ptr = (int16_t*) &(vdec_ptr->in_buf[0]);   // pointer to local input buffer
   int16_t *out_ptr = (int16_t*) &(vdec_ptr->dec_out_buf[0]);   // pointer to local output buffer
   uint32_t pkt_size = 15;

   // Error checks
   if (FALSE == vdec_ptr->modules.decoder.erasure)
   {
      if (pkt_size < vdec_ptr->in_buf_size)
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: More than one frame session(%lx)", vdec_ptr->session_num);
      }
      else if (pkt_size > vdec_ptr->in_buf_size)
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: Less than one frame session(%lx)", vdec_ptr->session_num);
         vdec_ptr->modules.decoder.erasure = TRUE;
      }   //else, pkt_size ==  in_buf_size
   }

   //check for erasure flag
   if (TRUE == vdec_ptr->modules.decoder.erasure)
   {
      int8_t *in_byte_ptr = (int8_t *) in_ptr;
      in_byte_ptr[0] = 0x1;   // bfi (bad frame indicator) = 1
   }

   // log packet in Q6 format
   uint16_t sampling_rate = voice_get_sampling_rate (vdec_ptr->voc_type, 0/*beamr is dont care*/, vdec_ptr->evs_samp_rate);
   vdec_log_vocoder_packet (vdec_ptr->voc_type, sampling_rate, ((sampling_rate == 16000) << 3) | vdec_ptr->session_num, in_ptr, pkt_size);

   //Re-packing from MVS data to DSP data
   gsm_hr_repacking_mvs_to_dsp ((int8_t *) in_ptr);

   // HR core decoding
   gsmhr_decode ((void*) vdec_ptr->modules.decoder.dec_state_ptr, in_ptr, out_ptr);

   return result;
}

static ADSPResult vdec_check_dec_struct_size (uint32_t dec_struct_size, uint32_t sess_num)
{
   ADSPResult result = ADSP_EOK;
   if (dec_struct_size > DEC_STATE_MEMORY_IN_BYTES)
   {
      MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "VCP: Error - Dec struct size(%ld) exceeds static size allocated, \
            session(%lx)", dec_struct_size, sess_num);
      result = ADSP_ENOMEMORY;
   }
   return result;
}

static ADSPResult vdec_log_vocoder_packet (uint32_t voc_type, uint16_t sampling_rate, uint32_t log_session_id, void *voc_packet, uint16_t size)
{
#if defined(__qdsp6__) && !defined(SIM)
   int8_t *bufptr[4] =
   {  (int8_t *) voc_packet, NULL, NULL, NULL};
   // log saved input buf and preproc output prior to encode
   voice_log_buffer( bufptr,
         VOICE_LOG_CHAN_PKT_RX_STREAM,
         log_session_id,
         qurt_elite_timer_get_time(),
         VOICE_LOG_DATA_FORMAT_PCM_MONO,
         sampling_rate,
         size,
         &voc_type);// custom data, to send media type info
#endif

   return ADSP_EOK;
}

void vdec_vtm_subscribe (vdec_t* vdec_ptr, Vtm_SubUnsubMsg_t *data_ptr)
{
   ADSPResult result = ADSP_EFAILED;
   //if(VFR_HARD == vdec_cmn_ptr->vfr_mode || VFR_HARD_EXT == vdec_cmn_ptr->vfr_mode)
   {
      if (ADSP_FAILED(result = voice_custom_vt_sub_unsub_msg_send(NULL,vdec_ptr->vtm_cmdq_ptr,VOICE_TIMER_SUBSCRIBE,NULL,FALSE,data_ptr)))
      {
         MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: Failed Vdec subscribe with vtm memory(%p) Vtm session(%#lx) ", data_ptr, vdec_ptr->session_num);
      }
      else
      {
         MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: Vdec subscribed with vtm memory(%p) Vtm session(%#lx) ", data_ptr, vdec_ptr->session_num);
      }
   }
}

void vdec_vtm_unsubscribe (vdec_t* vdec_ptr, Vtm_SubUnsubMsg_t *data_ptr, uint32_t mask, qurt_elite_channel_t *channel)
{
   ADSPResult result = ADSP_EFAILED;
   //if(VFR_HARD == vdec_cmn_ptr->vfr_mode || VFR_HARD_EXT == vdec_cmn_ptr->vfr_mode)
   {
      if (ADSP_FAILED(result = voice_custom_vt_sub_unsub_msg_send(NULL,vdec_ptr->vtm_cmdq_ptr,VOICE_TIMER_UNSUBSCRIBE,NULL,FALSE,data_ptr)))
      {
         MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: Failed Vdec unsubscribe with vtm memory(%p) Vtm session(%#lx) ", data_ptr, vdec_ptr->session_num);
      }
      else
      {
         MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: Vdec unsubscribed with vtm memory(%p) Vtm session(%#lx) ", data_ptr, vdec_ptr->session_num);
      }

      MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: Vdec waiting for unsubscribe to finish - vtm memory(%p) Vtm session(%#lx) ", data_ptr, vdec_ptr->session_num);
      (void) qurt_elite_channel_wait (channel, mask);
      qurt_elite_signal_clear (data_ptr->signal_end_ptr);
   }
}

//This command has nothing to do with pp thread.
static ADSPResult vdec_config_host_pcm (void* pInstance, elite_msg_any_t* pMsg)
{
   ADSPResult nResult = ADSP_EOK;
   ADSPResult apr_pkt_result = APR_EOK;
   vdec_t* vdec_ptr = (vdec_t*) pInstance;
   elite_msg_custom_voc_config_host_pcm_type *pConfig = (elite_msg_custom_voc_config_host_pcm_type *) pMsg->pPayload;
   elite_apr_packet_t *apr_packet_ptr = pConfig->apr_packet_ptr;

   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: vdec_config_host_pcm begin session(%x)\n", (int )vdec_ptr->session_num);

   uint16_t read_enable, write_enable;

   if ( (VOICE_CMD_START_HOST_PCM_V2 == elite_apr_if_get_opcode (apr_packet_ptr))
         && ((VOICE_NB_SAMPLING_RATE == vdec_ptr->sampling_rate_dec) || (VOICE_WB_SAMPLING_RATE == vdec_ptr->sampling_rate_dec) ||
                                                      (VOICE_SWB_SAMPLING_RATE == vdec_ptr->sampling_rate_dec) || (VOICE_FB_SAMPLING_RATE == vdec_ptr->sampling_rate_dec)))
   {
      voice_start_host_pcm_v2_t *start_host_pcm_ptr = (voice_start_host_pcm_v2_t *) elite_apr_if_get_payload_ptr (apr_packet_ptr);

      uint8 index;

      // check if default Rx stream tap point in the list
      for (index = 0; index < start_host_pcm_ptr->num_tap_points; index++)
      {
         if ( VOICESTREAM_MODULE_RX == start_host_pcm_ptr->tap_points[index].tap_point)
         {
            MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: vdec_config_host_pcm: tap point being enabled is (%lx), session(%x)\n", (uint32_t)VOICESTREAM_MODULE_RX, (int )vdec_ptr->session_num);
            break;
         }
      }

      voice_host_pcm_get_enable (&vdec_ptr->modules.host_pcm_context, &read_enable,
            VOICE_HOST_PCM_READ);
      voice_host_pcm_get_enable (&vdec_ptr->modules.host_pcm_context, &write_enable,
            VOICE_HOST_PCM_WRITE);

      /* make sMode match definition of VOICE_HOST_PCM_* => 0,1,2,3 = {OFF,R,W,RW} */
      uint16_t sMode = (write_enable << 1) | read_enable;

      // check if valid index found.  TODO: check if tap point already enabled
      if (index < start_host_pcm_ptr->num_tap_points && VOICE_HOST_PCM_OFF == sMode)
      {

         voice_tap_point_v2_t *voice_tap_point_ptr = &start_host_pcm_ptr->tap_points[index];

         if (NULL == voice_tap_point_ptr->mem_map_handle)
         {
            MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: Error! Vdec received NULL mem map handle!");
            apr_pkt_result = APR_EBADPARAM;
         }
         else
         {   // scope begining
            /* copy Host Pcm APR info */
            vdec_ptr->modules.host_pcm_context.apr_handle = pConfig->apr_handle;
            vdec_ptr->modules.host_pcm_context.shared_mem_client = vsm_memory_map_client;
            vdec_ptr->modules.host_pcm_context.mem_map_handle = voice_tap_point_ptr->mem_map_handle;
            vdec_ptr->modules.host_pcm_context.self_addr = elite_apr_if_get_dst_addr (apr_packet_ptr);
            vdec_ptr->modules.host_pcm_context.self_port = elite_apr_if_get_dst_port (apr_packet_ptr);
            vdec_ptr->modules.host_pcm_context.client_addr = elite_apr_if_get_src_addr (apr_packet_ptr);
            vdec_ptr->modules.host_pcm_context.client_port = elite_apr_if_get_src_port (apr_packet_ptr);
            vdec_ptr->modules.host_pcm_context.tap_point =
               VOICESTREAM_MODULE_RX;
            MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: Vdec setting hpcm context, memmmap handle (%lx), client addr (%x), client port (%x)", vdec_ptr->modules.host_pcm_context.mem_map_handle,
                  vdec_ptr->modules.host_pcm_context.client_addr, vdec_ptr->modules.host_pcm_context.client_port);

            /* enable Host pcm, R only,W only,or RW */

            /* enable read session if enabled */
            if (voice_tap_point_ptr->direction & VOICE_HOST_PCM_READ)
            {
               voice_host_pcm_set_enable (&vdec_ptr->modules.host_pcm_context,
                     TRUE,
                     VOICE_HOST_PCM_READ, 1 /*num of chan*/, voice_tap_point_ptr->sampling_rate, vdec_ptr->sampling_rate_dec, vdec_ptr->frame_samples_dec);
            }

            /* enable write session if enabled */
            if (voice_tap_point_ptr->direction & VOICE_HOST_PCM_WRITE)
            {
               voice_host_pcm_set_enable (&vdec_ptr->modules.host_pcm_context,
                     TRUE,
                     VOICE_HOST_PCM_WRITE, 1 /*num of chan*/, voice_tap_point_ptr->sampling_rate, vdec_ptr->sampling_rate_dec, vdec_ptr->frame_samples_dec);
            }   // end of if( voice_tap_point_ptr->direction & VOICE_HOST_PCM_WRITE)
         }   // scope ending
      }   // end of if valid index found
      else
      {
         MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: vdec_config_host_pcm START command NOP - tappoint not found or already enabled, session(%lx)\n", vdec_ptr->session_num);
      }
   }
   else if ( VOICE_CMD_STOP_HOST_PCM == elite_apr_if_get_opcode (apr_packet_ptr))
   {
      voice_host_pcm_set_enable (&vdec_ptr->modules.host_pcm_context,
            FALSE,
            VOICE_HOST_PCM_READ, 1, 0, /* dummy sampling rate */
            vdec_ptr->sampling_rate_dec, vdec_ptr->frame_samples_dec);

      voice_host_pcm_set_enable (&vdec_ptr->modules.host_pcm_context,
            FALSE,
            VOICE_HOST_PCM_WRITE, 1, 0, /* dummy sampling rate */
            vdec_ptr->sampling_rate_dec, vdec_ptr->frame_samples_dec);
   }
   else
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: vdec_config_host_pcm failed - opcode or sampling rate unsupported, session(%lx)\n", vdec_ptr->session_num);
      apr_pkt_result = APR_EUNSUPPORTED;
   }

   MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: vdec_config_host_pcm end apr pkt result(%d) session(%x)\n", apr_pkt_result, (int )vdec_ptr->session_num);

   elite_svc_send_ack (pMsg, apr_pkt_result);

   return nResult;
}

static uint32_t vdec_get_dec_static_size ()
{
   uint32_t mem_size = 0;

   if (DEC_STATE_MEMORY_IN_BYTES > get_evs_dec_struct_size ())
   {
      mem_size = DEC_STATE_MEMORY_IN_BYTES;
   }
   else
   {
      mem_size = get_evs_dec_struct_size ();
   }
   return (mem_size);
}

static ADSPResult vdec_allocate_mem (vdec_t * vdec_ptr)
{
   ADSPResult result = ADSP_EOK;
   uint32_t total_size;
   uint32_t dec_size, resampler_size;

   dec_size = VOICE_ROUNDTO8(vdec_get_dec_static_size ());
   resampler_size = VOICE_ROUNDTO8(voice_gen_resamp_get_size((int16)GEN_RESAMP_LP_LPH,(uint8)TWO_BYTES_PER_SAMPLE,&(vdec_ptr->modules.codec_resampler)));
   total_size = dec_size + resampler_size;

   MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: Computed memory requirement for vdec - %ld, session (%lx)", total_size, vdec_ptr->session_num);

   vdec_ptr->vdec_memory.start_addr_ptr = (int8_t *) qurt_elite_memory_malloc (total_size, QURT_ELITE_HEAP_DEFAULT);

   if (NULL == vdec_ptr->vdec_memory.start_addr_ptr)
   {
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: vdec_allocate_mem: Out of Memory!! session(%lx)", vdec_ptr->session_num);
      return ADSP_ENOMEMORY;
   }

   memset (vdec_ptr->vdec_memory.start_addr_ptr, 0, total_size);   // clear the memory before use

   vdec_ptr->vdec_memory.size = total_size;   // size allocated

   MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: vdec_allocate_mem: allocated memory %ld session (%lx)", vdec_ptr->vdec_memory.size, vdec_ptr->session_num);

   vdec_ptr->vdec_memory.usage_addr_ptr = vdec_ptr->vdec_memory.start_addr_ptr;   // Usage level pointer

   vdec_ptr->modules.decoder.dec_state_ptr = (int64_t *) vdec_ptr->vdec_memory.usage_addr_ptr;
   vdec_ptr->vdec_memory.usage_addr_ptr += dec_size;   // UsageAddr is a byte pointer
   vdec_ptr->vdec_memory.usage_addr_ptr = (int8_t *) VOICE_ROUNDTO8(vdec_ptr->vdec_memory.usage_addr_ptr);

   voice_gen_resamp_set_mem (&(vdec_ptr->modules.codec_resampler), vdec_ptr->vdec_memory.usage_addr_ptr, resampler_size);
   vdec_ptr->vdec_memory.usage_addr_ptr += resampler_size;   // UsageAddr is a byte pointer
   vdec_ptr->vdec_memory.usage_addr_ptr = (int8_t *) VOICE_ROUNDTO8(vdec_ptr->vdec_memory.usage_addr_ptr);

   if ((uint32_t) (vdec_ptr->vdec_memory.usage_addr_ptr - vdec_ptr->vdec_memory.start_addr_ptr) <= vdec_ptr->vdec_memory.size)
   {
      result = ADSP_EOK;
   }
   else
   {
      MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: Error: vdec_allocate_mem: allocated memory %d, memory consumed %d session(%lx)", (int )vdec_ptr->vdec_memory.size,
            (int )(vdec_ptr->vdec_memory.usage_addr_ptr - vdec_ptr->vdec_memory.start_addr_ptr), vdec_ptr->session_num);
      result = ADSP_ENOMEMORY;
      return result;
   }

   return result;
}

static void vdec_allocate_mem_free (vdec_t* vdec_ptr)
{
   vdec_ptr->modules.decoder.dec_state_ptr = NULL;

   if (NULL != vdec_ptr->modules.decoder.fourGV_decode_ptr)
   {
      vdec_ptr->modules.decoder.fourGV_decode_ptr->FGVDecodeDestructor ();
      vdec_ptr->modules.decoder.fourGV_decode_ptr = NULL;
   }

   voice_gen_resamp_end (&vdec_ptr->modules.codec_resampler);

   qurt_elite_memory_free (vdec_ptr->vdec_memory.start_addr_ptr);
   vdec_ptr->vdec_memory.start_addr_ptr = NULL;
   vdec_ptr->vdec_memory.usage_addr_ptr = NULL;
   vdec_ptr->vdec_memory.size = 0;
}

static ADSPResult vdec_set_pp_samp_rate (void* instance_ptr, elite_msg_any_t* msg_ptr)
{
   ADSPResult result ADSP_EOK;
   vdec_t* vdec_ptr = (vdec_t*) instance_ptr;
   elite_msg_custom_voc_stream_set_sampling_type *set_samp_rate_cmd_ptr = (elite_msg_custom_voc_stream_set_sampling_type *) msg_ptr->pPayload;

   switch (vdec_ptr->voc_type)
   {
      case VSM_MEDIA_TYPE_EVS:
         {
            if(vdec_ptr->evs_samp_rate != set_samp_rate_cmd_ptr->rx_samp_rate)
            {
               vdec_ptr->reset_voc_flag = TRUE;
               evs_dec_destroy((void*)vdec_ptr->modules.decoder.dec_state_ptr);
               vdec_ptr->evs_samp_rate = set_samp_rate_cmd_ptr->rx_samp_rate;
               vdec_ptr->sampling_rate_dec = voice_get_sampling_rate(vdec_ptr->voc_type,0,vdec_ptr->evs_samp_rate);
               vdec_ptr->samp_rate_factor = (vdec_ptr->sampling_rate_dec/VOICE_NB_SAMPLING_RATE);
               vdec_ptr->frame_samples_dec = VOICE_FRAME_SIZE_NB_20MS*(vdec_ptr->samp_rate_factor);

            }
            vdec_set_default_op_mode(instance_ptr);
            vdec_send_mode_notification_v2(instance_ptr);
            break;
         }
      default:
         MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "VCP: Invalid Vocoder(0x%lx) for set pp sampling rate command!", vdec_ptr->voc_type);
         result = ADSP_EUNSUPPORTED;
         break;
   }
   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: vdec_set_pp_samp_rate end session(%lx)", vdec_ptr->session_num);
   elite_svc_send_ack (msg_ptr, result);
   return result;
}

ADSPResult vdec_aggregate_modules_kpps (void* instance_ptr, uint32_t* kpps_changed)
{
   vdec_t* vdec_ptr = (vdec_t*) instance_ptr;

   if ((NULL == vdec_ptr) || (NULL == kpps_changed))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO, " VCP: Voice Decoder thread KPPS aggregator received bad pointers");
      return ADSP_EBADPARAM;
   }

   uint32_t kpps;
   uint32_t aggregate_kpps = 0;
   uint16_t sampling_rate = 0;

   // Voice Decoder KPPS
   kpps = 0;
   int16_t beamr_enable = (VOICE_BBWE_BEAMR == vdec_ptr->voice_module_bbwe) ? TRUE : FALSE;
   vcmn_find_vockpps_table (VOICE_DECODE_KPPS_TABLE, vdec_ptr->voc_type, beamr_enable, &kpps,vdec_ptr->evs_samp_rate, 0);
   MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: Decoder kpps (%d), session number (%lx)", (int )kpps, vdec_ptr->session_num);
   aggregate_kpps += kpps;

   // Host PCM
   kpps = 0;
   voice_host_pcm_get_kpps (&vdec_ptr->modules.host_pcm_context, &kpps);
   MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: Host PCM kpps (%d), session number (%lx)", (int )kpps, vdec_ptr->session_num);
   aggregate_kpps += kpps;

   //vdec_pp
   sampling_rate = voice_get_sampling_rate (vdec_ptr->voc_type, (VOICE_BBWE_BEAMR == vdec_ptr->voice_module_bbwe),vdec_ptr->evs_samp_rate );

   uint8_t ctm_rx_enable = 0;
   bool_t tty_enable = (vdec_ptr->tty_state_ptr->m_etty_mode != VSM_TTY_MODE_OFF);
   bool_t oobtty_enable = (vdec_ptr->tty_state_ptr->m_oobtty_mode != VSM_TTY_MODE_OFF);

   switch (vdec_ptr->voc_type)
   {
      case VSM_MEDIA_TYPE_AMR_NB_MODEM:
      case VSM_MEDIA_TYPE_AMR_NB_IF2:
      case VSM_MEDIA_TYPE_AMR_WB_MODEM:
      case VSM_MEDIA_TYPE_EFR_MODEM:
      case VSM_MEDIA_TYPE_FR_MODEM:
      case VSM_MEDIA_TYPE_HR_MODEM:
      case VSM_MEDIA_TYPE_EAMR:
      case VSM_MEDIA_TYPE_EVS:
         {
            ctm_rx_enable = tty_enable;
            break;
         }
   }

   // CTM Rx
   voice_ctm_rx_get_kpps (&vdec_ptr->modules.vdec_pp_modules.ctm.ctm_rx_struct, &kpps, ctm_rx_enable, sampling_rate);
   MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: CTM Rx kpps (%d), session number (%lx)", (int )kpps, vdec_ptr->session_num);
   aggregate_kpps += kpps;

   // OOBTTY Rx
   kpps = 0;
   voice_oobtty_rx_get_kpps( &vdec_ptr->modules.vdec_pp_modules.oobtty.oobtty_rx_struct, &kpps, oobtty_enable, sampling_rate);
   MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: OOBTTY Rx kpps (%d), session number (%lx)", (int)kpps, vdec_ptr->session_num);
   aggregate_kpps += kpps;


   // DTMF Detect
   kpps = 0;
   voice_dtmf_det_get_kpps (&(vdec_ptr->modules.vdec_pp_modules.dtmf_det.dtmf_det_struct), &kpps, sampling_rate);
   MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: DTMF Detect kpps (%d), session number (%lx)", (int )kpps, vdec_ptr->session_num);
   aggregate_kpps += kpps;

   // Rx gain kpps
   kpps = vdec_ptr->modules.vdec_pp_modules.rx_gain.kpps;
   MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: Rx gain kpps (%d), session number (%lx)", (int )kpps, vdec_ptr->session_num);
   aggregate_kpps += kpps;

   // EANS
   kpps = 0;
   voice_eans_get_kpps (&(vdec_ptr->modules.vdec_pp_modules.eans_struct), &kpps, sampling_rate);
   MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: EANS kpps (%d), session number (%lx)", (int )kpps, vdec_ptr->session_num);
   aggregate_kpps += kpps;

   // Slowtalk
   kpps = 0;
   voice_slow_talk_get_kpps (&(vdec_ptr->modules.vdec_pp_modules.slow_talk_struct), &kpps, sampling_rate);
   MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: Slowtalk kpps (%d), session number (%lx)", (int )kpps, vdec_ptr->session_num);
   aggregate_kpps += kpps;

   // Widevoice V1
   kpps = 0;
   voice_wve_get_kpps (&(vdec_ptr->modules.vdec_pp_modules.wve_struct), &kpps, (vdec_ptr->voice_module_bbwe == VOICE_BBWE_WV_V1) ? TRUE : FALSE);
   MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: Widevoice V1 kpps (%d), session number (%lx)", (int )kpps, vdec_ptr->session_num);
   aggregate_kpps += kpps;

   // Widevoice V2
   kpps = 0;
   voice_wve_v2_get_kpps (&(vdec_ptr->modules.vdec_pp_modules.wve_v2_struct), &kpps, (vdec_ptr->voice_module_bbwe == VOICE_BBWE_WV_V2) ? TRUE : FALSE);
   MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: Widevoice V2 kpps (%d), session number (%lx)", (int )kpps, vdec_ptr->session_num);
   aggregate_kpps += kpps;

   // Soft mute
   kpps = vdec_ptr->modules.vdec_pp_modules.mute.soft_mute_capi.kpps;
   MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: Soft mute kpps (%d), session number (%lx)", (int )kpps, vdec_ptr->session_num);
   aggregate_kpps += kpps;

   // TODO: Retaining this margin for safety, will need to be reassessed
   aggregate_kpps += 2000;   // adding extra 1000 kpps to ceil the number to mpps

   // Retaining behavior as it is as of today. That is to report an increase in KPPS only.
   // Once the VOICE_CMD_SET_TIMING_PARAMS commands are removed,
   // this can be modified to (vdec_pp_ptr->modules.aggregate_kpps != aggregate_kpps)
   if (vdec_ptr->modules.aggregate_kpps >= aggregate_kpps)
   {
      *kpps_changed = FALSE;
   }
   else
   {
      *kpps_changed = TRUE;
      MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: vdec KPPS changed from (%lu) to (%lu), session(%lx)", vdec_ptr->modules.aggregate_kpps, aggregate_kpps, vdec_ptr->session_num);
   }

   // Update state
   vdec_ptr->modules.aggregate_kpps = aggregate_kpps;

   return ADSP_EOK;
}

ADSPResult vdec_aggregate_modules_delay (void* instance_ptr)
{
   ADSPResult result = ADSP_EOK;
   vdec_t* vdec_ptr = (vdec_t*) instance_ptr;

   if (NULL == vdec_ptr)
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: Voice Decoder delay aggregator received bad pointers");
      return ADSP_EBADPARAM;
   }

   uint32_t delay;
   uint32_t aggregate_delay = 0;

   // Voice Decoder delay
   delay = 0;
   (void) vdec_get_vocoder_delay (vdec_ptr->voc_type, &delay);
   MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: Decoder delay (%d), session number (%lx)", (int )delay, vdec_ptr->session_num);
   aggregate_delay += delay;

   // EANS
   result = voice_eans_get_delay (&(vdec_ptr->modules.vdec_pp_modules.eans_struct), &delay);
   MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: EANS delay (%ld), result (%d)", delay, (int )result);
   aggregate_delay += delay;

   // Widevoice V1
   delay = 0;
   result = voice_wve_get_delay (&(vdec_ptr->modules.vdec_pp_modules.wve_struct), &delay, (vdec_ptr->voice_module_bbwe == VOICE_BBWE_WV_V1) ? TRUE : FALSE);
   MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: Widevoice V1 delay (%ld), result (%d)", delay, (int )result);
   aggregate_delay += delay;

   // Widevoice V2
   delay = 0;
   result = voice_wve_v2_get_delay (&(vdec_ptr->modules.vdec_pp_modules.wve_v2_struct), &delay, (vdec_ptr->voice_module_bbwe == VOICE_BBWE_WV_V2) ? TRUE : FALSE);
   MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: Widevoice V2 delay (%ld), result (%d)", delay, (int )result);
   aggregate_delay += delay;

   // Update state
   vdec_ptr->modules.aggregate_delay = aggregate_delay;

   return ADSP_EOK;

}

// This command is supported only in STOP state.
// The command needs to be forwarded to PP thread for Dec PP offset.
static ADSPResult vdec_set_timingv3_cmd (void* instance_ptr, elite_msg_any_t* msg_ptr)
{
   ADSPResult result = ADSP_EOK;
   vdec_t* vdec_ptr = (vdec_t*) instance_ptr;
   elite_msg_custom_voc_timing_param_type *set_timing_cmd_ptr = (elite_msg_custom_voc_timing_param_type *) msg_ptr->pPayload;
   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: vdec_set_timingv3_cmd begin session(%lx)", vdec_ptr->session_num);

   // Verify stop state of the thread.
   if (FALSE == vdec_ptr->process_data)
   {
      vsm_set_timing_params_v2_t* vfr_cmd_ptr = (vsm_set_timing_params_v2_t*) set_timing_cmd_ptr->param_data_ptr;
      //dbg msg is printed in VSM with all params
      /*MSG_7(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: vdec_set_timingv3_cmd, mode(%d), VSID(0x%lx), \
        enc_offset(%d), decreq_offset(%d), dec_offset(%d),decpp_offset(%d),session(%x)",
        vfr_cmd_ptr->mode,vfr_cmd_ptr->vsid,vfr_cmd_ptr->enc_offset,vfr_cmd_ptr->dec_req_offset,
        vfr_cmd_ptr->dec_offset,vfr_cmd_ptr->decpp_offset,vdec_ptr->session_num); */

      // Verify validity of VFR mode. In this version of timing cmd, mode supports only two values - VFR_NONE and VFR_HARD
      // If VFR_HARD, further information is derived from VSID
      if (VFR_HARD >= vfr_cmd_ptr->mode)
      {
         vdec_ptr->vfr_mode = vfr_cmd_ptr->mode;
      }
      else
      {
         vdec_ptr->vfr_mode = VFR_NONE;
         MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: vdec_set_timingv3_cmd, invalid mode(%x),setting to VFR_NONE,session(%lx)", vfr_cmd_ptr->mode, vdec_ptr->session_num);
         result = ADSP_EBADPARAM;
      }

      // Update VFR modes everywhere applicable.
      //vdec_ptr->timing.vtm_sub_unsub_pkt_req_data.vfr_mode = (uint8_t) vdec_ptr->vfr_mode;
      vdec_ptr->timing.vtm_sub_unsub_vds_pkt_req_data.vfr_mode = (uint8_t) vdec_ptr->vfr_mode;
      vdec_ptr->vtm_sub_unsub_decode_data.vfr_mode = (uint8_t) vdec_ptr->vfr_mode;

      // Verify validity of VSID. In VFR_HARD case, VSID should be non-zero. In VFR_NONE case, VSID is don't care.
      if ((VFR_HARD == vdec_ptr->vfr_mode) && (0 == vfr_cmd_ptr->vsid))
      {
         MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: vdec_set_timingv3_cmd, invalid VSID(%lx), session(%lx)", vfr_cmd_ptr->vsid, vdec_ptr->session_num);
         result = ADSP_EBADPARAM;
      }
      else
      {
         vdec_ptr->vsid = vfr_cmd_ptr->vsid;
      }

      // Update VFR modes everywhere applicable.
      //vdec_ptr->timing.vtm_sub_unsub_pkt_req_data.vsid = vdec_ptr->vsid;
      vdec_ptr->timing.vtm_sub_unsub_vds_pkt_req_data.vsid = vdec_ptr->vsid;
      vdec_ptr->vtm_sub_unsub_decode_data.vsid = vdec_ptr->vsid;

      // Verify validity of offsets
      // MIN_TIMER_OFFSET is set to 0, so no need to check if offset is below min because it's unsigned
      if ((MAX_TIMER_OFFSET < vfr_cmd_ptr->dec_req_offset) || (MAX_TIMER_OFFSET < vfr_cmd_ptr->dec_offset))
      {
         //vdec_ptr->timing.vtm_sub_unsub_pkt_req_data.offset = 3300;   // default 3.3 ms
         vdec_ptr->timing.vtm_sub_unsub_vds_pkt_req_data.offset = 3300;   // default 3.3 ms
         vdec_ptr->vtm_sub_unsub_decode_data.offset = 8300;    // default 8.3ms
         MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: vdec_set_timingv3_cmd: Invalid offset(s) - dec_req(%d), dec(%d), defaulting to 3.3ms, 8.3ms", vfr_cmd_ptr->dec_req_offset,
               vfr_cmd_ptr->dec_offset);
         result = ADSP_EOK;   // since ISOD/API says we support zero offsets, ack EOK and return EOK.
         // just printing message is good enough.  Changed the MSG to error to hightlight
      }
      else
      {
         // vdec_ptr->timing.vtm_sub_unsub_pkt_req_data.offset = vfr_cmd_ptr->dec_req_offset;   // default 3.3 ms
         vdec_ptr->timing.vtm_sub_unsub_vds_pkt_req_data.offset = vfr_cmd_ptr->dec_req_offset;
         vdec_ptr->vtm_sub_unsub_decode_data.offset = vfr_cmd_ptr->dec_offset;   // default 8.3ms
      }

      //update version of timing used
      //vdec_ptr->timing.vtm_sub_unsub_pkt_req_data.timing_ver =
      //       VFR_CLIENT_INFO_VER_2;
      vdec_ptr->timing.vtm_sub_unsub_vds_pkt_req_data.timing_ver =
         VFR_CLIENT_INFO_VER_2;
      vdec_ptr->vtm_sub_unsub_decode_data.timing_ver = VFR_CLIENT_INFO_VER_2;

      elite_svc_send_ack (msg_ptr, result);

   }
   else
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: Timing can't be changed in RUN, session(%lx)", vdec_ptr->session_num);
      result = ADSP_EBUSY;
      elite_svc_send_ack (msg_ptr, result);
   }

   return result;
}
static ADSPResult vdec_send_mode_notification (vdec_t *vdec_ptr, uint32_t eamr_curr_bw_det_state)
{
   ADSPResult rc = ADSP_EOK;
   vsm_eamr_mode_changed_t eamr_bw_mode;
   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: vdec_send_mode_notification begin session(%lx)", vdec_ptr->session_num);

   eamr_bw_mode.mode = eamr_curr_bw_det_state;

   rc = elite_apr_if_alloc_send_event (vdec_ptr->apr_info_ptr->apr_handle, vdec_ptr->apr_info_ptr->self_addr, vdec_ptr->apr_info_ptr->self_port, vdec_ptr->apr_info_ptr->client_addr,
         vdec_ptr->apr_info_ptr->client_port, 0,
         VSM_EVT_EAMR_MODE_CHANGED, &eamr_bw_mode, (sizeof(vsm_eamr_mode_changed_t)));

   if (ADSP_FAILED(rc))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: Failed to create APR request for VSM_EVT_EAMR_MODE_CHANGED :session(%lx)", vdec_ptr->session_num);
   }
   else
   {
      MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: Vdec sent the eAMR mode change notification, session(%lx), mode(%lx)", vdec_ptr->session_num, eamr_curr_bw_det_state);
   }
   return rc;
}

ADSPResult vdec_get_vocoder_delay (uint32_t voc_type, uint32_t* delay_ptr)
{
   //delay accounted for at encoder side
   *delay_ptr = 0;
   switch (voc_type)
   {
      case VSM_MEDIA_TYPE_13K_MODEM:
         {
            break;
         }
      case VSM_MEDIA_TYPE_EVRC_MODEM:
         {
            break;
         }
      case VSM_MEDIA_TYPE_4GV_NB_MODEM:
         {
            break;
         }
      case VSM_MEDIA_TYPE_4GV_WB_MODEM:
         {
            break;
         }
      case VSM_MEDIA_TYPE_4GV_NW_MODEM:
         {
            break;
         }
      case VSM_MEDIA_TYPE_AMR_NB_MODEM:
      case VSM_MEDIA_TYPE_AMR_NB_IF2:
         {
            break;
         }
      case VSM_MEDIA_TYPE_AMR_WB_MODEM:
         {
            break;
         }
      case VSM_MEDIA_TYPE_EFR_MODEM:
         {
            break;
         }
      case VSM_MEDIA_TYPE_FR_MODEM:
         {
            break;
         }
      case VSM_MEDIA_TYPE_HR_MODEM:
         {
            break;
         }
      case VSM_MEDIA_TYPE_PCM_8_KHZ:
      case VSM_MEDIA_TYPE_PCM_16_KHZ:
      case VSM_MEDIA_TYPE_PCM_32_KHZ:
      case VSM_MEDIA_TYPE_PCM_44_1_KHZ:
      case VSM_MEDIA_TYPE_PCM_48_KHZ:
         {
            break;
         }
      case VSM_MEDIA_TYPE_G711_ALAW:
      case VSM_MEDIA_TYPE_G711_MLAW:
      case VSM_MEDIA_TYPE_G711_ALAW_V2:
      case VSM_MEDIA_TYPE_G711_MLAW_V2:
         {
            break;
         }
      case VSM_MEDIA_TYPE_G729AB:
         {
            break;
         }
      case VSM_MEDIA_TYPE_4GV_NW:
         {
            break;
         }
      case VSM_MEDIA_TYPE_EAMR:
         {
            break;
         }
      case VSM_MEDIA_TYPE_EVRC_NW_2K:
         {
            break;
         }
      case VSM_MEDIA_TYPE_EVS:
         {
            break;
         }
      default:
         {
            MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: Error! Invalid vocoder type for delay (%lx), returning zero", voc_type);
         }
   }
   return ADSP_EOK;
}

static ADSPResult vdec_register_event (void* instance_ptr, elite_msg_any_t* msg_ptr)
{
   vdec_t* vdec_ptr = (vdec_t*) instance_ptr;
   elite_msg_custom_event_reg_type *payload_ptr = (elite_msg_custom_event_reg_type*) msg_ptr->pPayload;
   uint32_t event_id = payload_ptr->event_id;
   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: Vdec received registration for event %lx", event_id);

   switch (event_id)
   {
      case VSM_EVT_VOC_OPERATING_MODE_UPDATE:
         {
            // set mode detection to true
            vdec_ptr->modules.decoder.vocoder_op_detection = 1;
            // Issue default mode event
            vdec_send_mode_notification_v2 (instance_ptr);
            break;
         }
      default:
         {
            MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: Error! Vdec received registration for invalid event %lx", event_id);
         }
   }
   elite_msg_return_payload_buffer (msg_ptr);
   return ADSP_EOK;
}

static ADSPResult vdec_unregister_event (void* instance_ptr, elite_msg_any_t* msg_ptr)
{
   vdec_t* vdec_ptr = (vdec_t*) instance_ptr;
   elite_msg_custom_event_reg_type *payload_ptr = (elite_msg_custom_event_reg_type*) msg_ptr->pPayload;
   uint32_t event_id = payload_ptr->event_id;
   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: Vdec received deregistration for event %lx", event_id);

   switch (event_id)
   {
      case VSM_EVT_VOC_OPERATING_MODE_UPDATE:
         {
            // set mode detection to true
            vdec_ptr->modules.decoder.vocoder_op_detection = 0;
            break;
         }
      default:
         {
            MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: Error! Vdec received deregistration for invalid event %lx", event_id);
         }
   }
   elite_msg_return_payload_buffer (msg_ptr);
   return ADSP_EOK;
}

static ADSPResult vdec_send_mode_notification_v2 (void* instance_ptr)
{
   ADSPResult rc = ADSP_EOK;
   vdec_t* vdec_ptr = (vdec_t*) instance_ptr;

   if (0 == vdec_ptr->modules.decoder.vocoder_op_detection)
   {
      return ADSP_EOK;
   }

   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: vdec_send_mode_notification_v2 begin session(%lx)", vdec_ptr->session_num);
   vsm_evt_voc_operating_mode_update_t mode_update;

   mode_update.direction = VSM_VOC_OPERATING_MODE_DIRECTION_RX;
   mode_update.reserved = 0;
   mode_update.mode = vdec_ptr->modules.decoder.session_op_mode;

   rc = elite_apr_if_alloc_send_event (vdec_ptr->apr_info_ptr->apr_handle, vdec_ptr->apr_info_ptr->self_addr, vdec_ptr->apr_info_ptr->self_port, vdec_ptr->apr_info_ptr->client_addr,
         vdec_ptr->apr_info_ptr->client_port, 0,
         VSM_EVT_VOC_OPERATING_MODE_UPDATE, &mode_update, (sizeof(mode_update)));

   if (ADSP_FAILED(rc))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: Failed to create APR request for VSM_EVT_VOC_OPERATING_MODE_UPDATE :session(%lx)", vdec_ptr->session_num);
      return rc;
   }

   // For eAMR also need to send tx side mode
   if (VSM_MEDIA_TYPE_EAMR == vdec_ptr->voc_type)
   {
      mode_update.direction = VSM_VOC_OPERATING_MODE_DIRECTION_TX;
      mode_update.reserved = 0;
      mode_update.mode = vdec_ptr->modules.decoder.session_op_mode;

      rc = elite_apr_if_alloc_send_event (vdec_ptr->apr_info_ptr->apr_handle, vdec_ptr->apr_info_ptr->self_addr, vdec_ptr->apr_info_ptr->self_port, vdec_ptr->apr_info_ptr->client_addr,
            vdec_ptr->apr_info_ptr->client_port, 0,
            VSM_EVT_VOC_OPERATING_MODE_UPDATE, &mode_update, (sizeof(mode_update)));

      if (ADSP_FAILED(rc))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: Failed to create APR request for VSM_EVT_VOC_OPERATING_MODE_UPDATE :session(%lx)", vdec_ptr->session_num);
         return rc;
      }

   }

   MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: Vdec sent the vocoder mode change notification, notified mode(%lx), vocoder native mode (%lx), session (%lx)",
         mode_update.mode,
         vdec_ptr->modules.decoder.voc_native_op_mode,
         vdec_ptr->session_num);
   return rc;
}

static void vdec_set_default_op_mode (void* instance_ptr)
{
   vdec_t* vdec_ptr = (vdec_t*) instance_ptr;
   switch (vdec_ptr->voc_type)
   {
      case VSM_MEDIA_TYPE_NONE:
         {
            vdec_ptr->modules.decoder.session_op_mode = VSM_VOC_OPERATING_MODE_NONE;
            break;
         }
      case VSM_MEDIA_TYPE_EAMR:
         {
            vdec_ptr->modules.decoder.session_op_mode = VSM_VOC_OPERATING_MODE_NB;
            break;
         }
      case VSM_MEDIA_TYPE_4GV_NW_MODEM:
      case VSM_MEDIA_TYPE_4GV_NW:
      case VSM_MEDIA_TYPE_EVRC_NW_2K:
         {
            vdec_ptr->modules.decoder.session_op_mode = VSM_VOC_OPERATING_MODE_NB;
            break;
         }
      default:
         {
            uint32_t sampling_rate = voice_get_sampling_rate (vdec_ptr->voc_type, 0, vdec_ptr->evs_samp_rate); /* mode notification should not depend on BBWE flag, hard coding the second arguement to 0 */
            if (VOICE_NB_SAMPLING_RATE == sampling_rate)
            {
               vdec_ptr->modules.decoder.session_op_mode = VSM_VOC_OPERATING_MODE_NB;
            }
            else if (VOICE_WB_SAMPLING_RATE == sampling_rate)
            {
               vdec_ptr->modules.decoder.session_op_mode = VSM_VOC_OPERATING_MODE_WB;
            }
            else if (VOICE_SWB_SAMPLING_RATE == sampling_rate)
            {
               vdec_ptr->modules.decoder.session_op_mode = VSM_VOC_OPERATING_MODE_SWB;
            }
            else
            {
               vdec_ptr->modules.decoder.session_op_mode = VSM_VOC_OPERATING_MODE_FB;
            }

            vdec_ptr->modules.decoder.voc_native_op_mode = vdec_ptr->modules.decoder.session_op_mode;

            // If slowtalk is enabled, always report WB operating mode.
            // (note that this does not apply for eAMR or 4GV-NW)
            if ((NO_SLOWTALK != vdec_ptr->modules.vdec_pp_modules.slow_talk_struct.n_slowtalk)
                  && (VSM_VOC_OPERATING_MODE_WB <  vdec_ptr->modules.decoder.session_op_mode))
            {
               vdec_ptr->modules.decoder.session_op_mode = VSM_VOC_OPERATING_MODE_WB;
            }
            break;
         }
   }
   return;
}

static ADSPResult vdec_set_param_v3 (void* instance_ptr, elite_msg_any_t* msg_ptr)
{
   ADSPResult result = ADSP_EOK;
   vdec_t* vdec_ptr = (vdec_t*) instance_ptr;
   elite_msg_custom_set_param_v3_type *payload_ptr = (elite_msg_custom_set_param_v3_type*) msg_ptr->pPayload;
   uint32_t cal_handle = payload_ptr->cal_handle;
   MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: vdec received set_param_v3, handle %lx, session(%lx)", cal_handle, vdec_ptr->session_num);

   //call into mvm using mvm_call as an entry point to cvd_cal_query
   vss_imvm_cmd_cal_query_t mvm_payload;
   mvm_payload.query_handle = payload_ptr->cal_handle;
   mvm_payload.cb_fn = vdec_calibration_cb_func;
   mvm_payload.client_data = instance_ptr;

   result = mvm_call (MVM_CMDID_CAL_QUERY, &mvm_payload, sizeof(mvm_payload));

   elite_svc_send_ack (msg_ptr, result);

   return result;
}

void vdec_calibration_cb_func (cvd_cal_param_t* cal_params_ptr, void* cb_data)
{
   ADSPResult result = ADSP_EOK;
   vdec_t* vdec_ptr = (vdec_t*) cb_data;
   uint32_t mod_id, param_id, param_size;
   mod_id = cal_params_ptr->module_id;
   param_id = cal_params_ptr->param_id;
   param_size = cal_params_ptr->param_data_size;

   result = vdec_set_param_int (vdec_ptr, mod_id, param_id, cal_params_ptr->param_data, param_size);
   if (ADSP_EOK != result && ADSP_EUNSUPPORTED != result)
   {
      MSG_4(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: vdec set param error %u, mod %lx, param %lx, session(%lx)", result, mod_id, param_id, vdec_ptr->session_num);
   }
   return;
}

static ADSPResult vdec_set_enc_rate (void* instance_ptr, elite_msg_any_t* msg_ptr)
{
   ADSPResult result = ADSP_EOK;
   vdec_t* vdec_ptr = (vdec_t*) instance_ptr;
   elite_msg_custom_set_enc_rate_type *payload_ptr = (elite_msg_custom_set_enc_rate_type*) msg_ptr->pPayload;
   uint32_t media_type, encoder_rate;

   //Only update mode/send notification for set_rate in stop state
   // This is based on the assumption that the COPs will most likely be symmetric
   if (TRUE == vdec_ptr->process_data)
   {
      elite_svc_send_ack (msg_ptr, result);
      return result;
   }

   media_type = payload_ptr->media_type;
   encoder_rate = payload_ptr->encoder_rate;

   switch (media_type)
   {
      case VSM_MEDIA_TYPE_4GV_WB_MODEM:
      case VSM_MEDIA_TYPE_EVRC_NW_2K:
      case VSM_MEDIA_TYPE_4GV_NW_MODEM:
      case VSM_MEDIA_TYPE_4GV_NW:
         {
            if (encoder_rate <= 7)
            {
               //check if we need to issue a mode change event
               if ((0 == encoder_rate) && (VSM_VOC_OPERATING_MODE_NB == vdec_ptr->modules.decoder.session_op_mode))
               {
                  //nb to wb transition
                  vdec_ptr->modules.decoder.session_op_mode =
                     VSM_VOC_OPERATING_MODE_WB;
                  vdec_send_mode_notification_v2 (instance_ptr);
               }
               else if ((0 != encoder_rate) && (VSM_VOC_OPERATING_MODE_WB == vdec_ptr->modules.decoder.session_op_mode))
               {
                  //wb to nb transition
                  vdec_ptr->modules.decoder.session_op_mode =
                     VSM_VOC_OPERATING_MODE_NB;
                  vdec_send_mode_notification_v2 (instance_ptr);
               }
            }
            else
            {
               MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: Invalid cop session(%lx)", vdec_ptr->session_num);
               result = ADSP_EUNSUPPORTED;
            }
            break;
         }

      default:
         {
            result = ADSP_EOK;
         }
   }
   elite_svc_send_ack (msg_ptr, result);
   return result;

}

static ADSPResult vdec_pp_allocate_mem (vdec_t *vdec_ptr)
{
   ADSPResult result = ADSP_EOK;
   uint32_t total_size;
   uint32_t eans_size, st_size, wve_size, rec_buf_size, ss_size, scratch_size, ctm_rx_size, wve_v2_size;
   int8_t *temp_ptr, *rx_gain_ptr;
#if defined(LVVE)/*TCT-NB Tianhongwei add for nxp voice solution 2016/01/04*/
	  uint32_t LVVEFQ_BE_SSRC_Instance_size;
#endif

   eans_size = VOICE_ROUNDTO8(eans_get_size ());
   st_size = VOICE_ROUNDTO8(st_get_size ());
   //in_buf_size=VOICE_ROUNDTO8(PP_IN_CIRC_BUF_SIZE_SAMPLES*2);
   rec_buf_size = VOICE_ROUNDTO8(DEC_OUT_CIRC_BUF_SIZE_REC*MAX_SAMP_RATE_FACTOR*2);
   voice_ss_get_size (&ss_size);
   ss_size = VOICE_ROUNDTO8(ss_size);
   wve_size = VOICE_ROUNDTO8(wve_compute_data_struct_size ());
   scratch_size = VOICE_ROUNDTO8(VDEC_SCRATCH_MEM_SIZE_IN_BYTES);
   ctm_rx_size = VOICE_ROUNDTO8(sizeof(int16_t) * ctmrx_get_struct_size ());

   {
      //GET Static properties
      capi_v2_init_memory_requirement_t mem_req = {0};

      capi_v2_prop_t get_static_props[] =
      {
         {CAPI_V2_INIT_MEMORY_REQUIREMENT, {reinterpret_cast<int8_t*> (&mem_req), 0, sizeof(mem_req) }, {0, 0, 0 } },
      };

      capi_v2_proplist_t get_static_prop_list = {VOICE_SIZE_OF_ARRAY(get_static_props), get_static_props};

      {
         mem_req.size_in_bytes = 0;
         //Rx soft mute. TODO. Make a list
         if (ADSP_FAILED(result=capi_v2_voice_soft_mute_get_static_properties(NULL,&get_static_prop_list)))
         {
            MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: Vdec soft mute: Get Static Properties error");
            return ADSP_EFAILED;
         }
         temp_ptr = (int8_t *) qurt_elite_memory_malloc (mem_req.size_in_bytes, QURT_ELITE_HEAP_DEFAULT);
         if (!temp_ptr)
         {
            MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: Vdec soft mute: Malloc failed");
            return ADSP_ENOMEMORY;
         }
         vdec_ptr->modules.vdec_pp_modules.mute.soft_mute_capi.module_ptr = (capi_v2_t *) temp_ptr;

         mem_req.size_in_bytes = 0;

         // Query memory requirement for rx gain capi instance.
         if (ADSP_FAILED (result = capi_v2_voice_rx_gain_get_static_properties (NULL, &get_static_prop_list)))
         {
            MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: Vdec rx gain: Get Static Properties error");
            return ADSP_EFAILED;
         }
         // Allocate memory for rx gain capi instance
         rx_gain_ptr = (int8_t*) qurt_elite_memory_malloc (mem_req.size_in_bytes, QURT_ELITE_HEAP_DEFAULT);
         if (!rx_gain_ptr)
         {
            MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: Vdec rx gain: Malloc failed");
            return ADSP_ENOMEMORY;
         }
         vdec_ptr->modules.vdec_pp_modules.rx_gain.module_ptr = (capi_v2_t*) rx_gain_ptr;
      }
   }

   if (ADSP_FAILED(voice_wve_v2_get_mem_req (&(vdec_ptr->modules.vdec_pp_modules.wve_v2_struct), 8000, &wve_v2_size)))   // default set sampling rate as 8000
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: vdec_pp_allocate_mem: Wide Voice V2 get memory request failed, session(%lx)", vdec_ptr->session_num);
      return ADSP_EFAILED;
   }
   wve_v2_size = VOICE_ROUNDTO8(wve_v2_size);

   total_size = eans_size + st_size + rec_buf_size + ss_size + wve_size + scratch_size + ctm_rx_size + wve_v2_size;

#if defined(LVVE)/*TCT-NB Tianhongwei add for nxp voice solution 2016/01/04*/
	  LVVEFQ_BE_SSRC_Instance_size = VOICE_ROUNDTO8(LVVEFQ_BE_SSRC_get_size(&(vdec_ptr->modules.vdec_pp_modules.LVVEFQ_BE_SSRC_Instance)));
	  total_size += LVVEFQ_BE_SSRC_Instance_size;
#endif

   MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: Computed memory requirement for Vdec PP - %ld, session (%lx)", total_size, vdec_ptr->session_num);

   vdec_ptr->vdec_pp_memory.start_addr_ptr = (int8_t *) qurt_elite_memory_malloc (total_size, QURT_ELITE_HEAP_DEFAULT);

   if (NULL == vdec_ptr->vdec_pp_memory.start_addr_ptr)
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: vdec_pp_allocate_mem: Out of Memory!! session(%lx)", vdec_ptr->session_num);
      return ADSP_ENOMEMORY;
   }

   memset (vdec_ptr->vdec_pp_memory.start_addr_ptr, 0, total_size);   // clear the memory before use

   vdec_ptr->vdec_pp_memory.size = total_size;   // size allocated

   MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: vdec_pp_allocate_mem: allocated memory %ld, session (%lx)", vdec_ptr->vdec_pp_memory.size, vdec_ptr->session_num);

   vdec_ptr->vdec_pp_memory.usage_addr_ptr = vdec_ptr->vdec_pp_memory.start_addr_ptr;   // Usage level pointer

   voice_eans_set_mem (&(vdec_ptr->modules.vdec_pp_modules.eans_struct), vdec_ptr->vdec_pp_memory.usage_addr_ptr, eans_size);
   vdec_ptr->vdec_pp_memory.usage_addr_ptr += eans_size;
   vdec_ptr->vdec_pp_memory.usage_addr_ptr = (int8_t *) VOICE_ROUNDTO8(vdec_ptr->vdec_pp_memory.usage_addr_ptr);

   voice_st_set_mem (&(vdec_ptr->modules.vdec_pp_modules.slow_talk_struct), vdec_ptr->vdec_pp_memory.usage_addr_ptr, st_size);
   vdec_ptr->vdec_pp_memory.usage_addr_ptr += st_size;
   vdec_ptr->vdec_pp_memory.usage_addr_ptr = (int8_t *) VOICE_ROUNDTO8(vdec_ptr->vdec_pp_memory.usage_addr_ptr);

   voice_wve_set_mem (&(vdec_ptr->modules.vdec_pp_modules.wve_struct), vdec_ptr->vdec_pp_memory.usage_addr_ptr, wve_size);
   vdec_ptr->vdec_pp_memory.usage_addr_ptr += wve_size;
   vdec_ptr->vdec_pp_memory.usage_addr_ptr = (int8_t *) VOICE_ROUNDTO8(vdec_ptr->vdec_pp_memory.usage_addr_ptr);

   voice_wve_v2_set_mem (&(vdec_ptr->modules.vdec_pp_modules.wve_v2_struct), vdec_ptr->vdec_pp_memory.usage_addr_ptr, wve_v2_size);
   vdec_ptr->vdec_pp_memory.usage_addr_ptr += wve_v2_size;
   vdec_ptr->vdec_pp_memory.usage_addr_ptr = (int8_t *) VOICE_ROUNDTO8(vdec_ptr->vdec_pp_memory.usage_addr_ptr);
#if defined(LVVE)/*TCT-NB Tianhongwei add for nxp voice solution 2016/01/04*/
	LVVEFQ_BE_SSRC_set_mem(&(vdec_ptr->modules.vdec_pp_modules.LVVEFQ_BE_SSRC_Instance), vdec_ptr->vdec_pp_memory.usage_addr_ptr);
	vdec_ptr->vdec_pp_memory.usage_addr_ptr += LVVEFQ_BE_SSRC_Instance_size;
	vdec_ptr->vdec_pp_memory.usage_addr_ptr = (int8_t *)VOICE_ROUNDTO8(vdec_ptr->vdec_pp_memory.usage_addr_ptr);
#endif

   //vdec_ptr->pp_in_circ_buf_ptr = vdec_ptr->vdec_pp_memory.usage_addr_ptr;
   //vdec_ptr->vdec_pp_memory.usage_addr_ptr += in_buf_size;
   //vdec_ptr->vdec_pp_memory.usage_addr_ptr = (int8_t *)VOICE_ROUNDTO8(vdec_ptr->vdec_pp_memory.usage_addr_ptr);

   vdec_ptr->modules.vdec_pp_modules.record.circ_decout_rec_buf_ptr = vdec_ptr->vdec_pp_memory.usage_addr_ptr;
   vdec_ptr->vdec_pp_memory.usage_addr_ptr += rec_buf_size;
   vdec_ptr->vdec_pp_memory.usage_addr_ptr = (int8_t *) VOICE_ROUNDTO8(vdec_ptr->vdec_pp_memory.usage_addr_ptr);

   vdec_ptr->modules.vdec_pp_modules.record.ss_struct_ptr = vdec_ptr->vdec_pp_memory.usage_addr_ptr;
   vdec_ptr->vdec_pp_memory.usage_addr_ptr += ss_size;
   vdec_ptr->vdec_pp_memory.usage_addr_ptr = (int8_t *) VOICE_ROUNDTO8(vdec_ptr->vdec_pp_memory.usage_addr_ptr);
   voice_ss_set_mem (&(vdec_ptr->modules.vdec_pp_modules.record.ss_struct), vdec_ptr->modules.vdec_pp_modules.record.ss_struct_ptr, ss_size);

   vdec_ptr->scratch_ptr = vdec_ptr->vdec_pp_memory.usage_addr_ptr;
   vdec_ptr->vdec_pp_memory.usage_addr_ptr += scratch_size;
   vdec_ptr->vdec_pp_memory.usage_addr_ptr = (int8_t *) VOICE_ROUNDTO8(vdec_ptr->vdec_pp_memory.usage_addr_ptr);

   (vdec_ptr->modules.vdec_pp_modules.ctm.ctm_rx_struct).ctm_rx_struct_instance_ptr = vdec_ptr->vdec_pp_memory.usage_addr_ptr;
   vdec_ptr->vdec_pp_memory.usage_addr_ptr += ctm_rx_size;
   vdec_ptr->vdec_pp_memory.usage_addr_ptr = (int8_t *) VOICE_ROUNDTO8(vdec_ptr->vdec_pp_memory.usage_addr_ptr);

   if ((uint32_t) (vdec_ptr->vdec_pp_memory.usage_addr_ptr - vdec_ptr->vdec_pp_memory.start_addr_ptr) <= vdec_ptr->vdec_pp_memory.size)
   {
      result = ADSP_EOK;
   }
   else
   {
      MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: Error: vdec_pp_allocate_mem: allocated memory %d, memory consumed %d session(%lx)", (int )vdec_ptr->vdec_pp_memory.size,
            (int )(vdec_ptr->vdec_pp_memory.usage_addr_ptr - vdec_ptr->vdec_pp_memory.start_addr_ptr), vdec_ptr->session_num);
      result = ADSP_ENOMEMORY;
      return result;
   }

   return result;
}

static ADSPResult vdec_pp_init (vdec_t *vdec_ptr)   //PG: call this from reinit also
{
   ADSPResult result = ADSP_EOK;
   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: vdec_pp_init begin session(%lx)", vdec_ptr->session_num);

   // Reset control code params
   (void) vdec_pp_ctrl_params_init (vdec_ptr);
   // Reset params for a new call
   vdec_ptr->modules.vdec_pp_modules.mute.mute = VOICE_UNMUTE;   // don't cache this

   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: vdec_pp_init end session(%lx)", vdec_ptr->session_num);
   return result;
}

// Reset control code params, should be called for a new call and in STOP state
static ADSPResult vdec_pp_ctrl_params_init (vdec_t *vdec_ptr)
{
   ADSPResult result = ADSP_EOK;
   //dbg: MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "vdec_pp_ctrl_params_init begin session(%lx)",vdec_pp_ptr->vdec_cmn_ptr->session_num);

   vdec_ptr->pp_out_buf_size = 0;
   vdec_ptr->modules.vdec_pp_modules.record.send_media_type_rec = FALSE;
   vdec_ptr->send_media_type = FALSE;

   //dbg: MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "vdec_pp_ctrl_params_init end session(%lx)",vdec_pp_ptr->vdec_cmn_ptr->session_num);
   return result;
}

static ADSPResult vdec_pp_modules_init (vdec_t *vdec_ptr)
{
   ADSPResult result = ADSP_EOK;

   //Init properties CAPI_V2_INPUT_MEDIA_FORMAT
   voice_capi_data_format_struct_t voice_std_mediatype;
   capi_v2_event_callback_info_t cb_info = { vdec_capi_v2_cb_func, NULL };
   capi_v2_port_info_t port_info = {TRUE,TRUE,0};

   //init mediatype
   voice_std_mediatype.media_format.format_header.data_format = CAPI_V2_FIXED_POINT;
   voice_std_mediatype.data_format.bits_per_sample = 16;
   voice_std_mediatype.data_format.sampling_rate = 8000;   //TODO: Revisit this. soft volume size does not depend on sampling rates
   voice_std_mediatype.data_format.data_is_signed = TRUE;
   voice_std_mediatype.data_format.num_channels = 1;

   capi_v2_prop_t init_props[] =
   {
      { CAPI_V2_EVENT_CALLBACK_INFO, { reinterpret_cast<int8_t *>(&cb_info), sizeof(cb_info), sizeof(cb_info) }, port_info },
      { CAPI_V2_INPUT_MEDIA_FORMAT, {reinterpret_cast<int8_t*> (&voice_std_mediatype), sizeof(voice_std_mediatype), sizeof(voice_std_mediatype)}, port_info},
   };

   capi_v2_proplist_t init_props_list = {VOICE_SIZE_OF_ARRAY(init_props), init_props};

   // call soft mute init
   cb_info.event_context = vdec_ptr->modules.vdec_pp_modules.mute.soft_mute_capi.module_ptr;
   capi_v2_voice_soft_mute_init (vdec_ptr->modules.vdec_pp_modules.mute.soft_mute_capi.module_ptr, &init_props_list);

   // call rx gain init
   cb_info.event_context =  vdec_ptr->modules.vdec_pp_modules.rx_gain.module_ptr;
   capi_v2_voice_rx_gain_init (vdec_ptr->modules.vdec_pp_modules.rx_gain.module_ptr, &init_props_list);

   result = voice_eans_init_default (&(vdec_ptr->modules.vdec_pp_modules.eans_struct));
   if (ADSP_FAILED(result))
   {
      MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: FNS init failed, result(%d) session(%lx) ", result, vdec_ptr->session_num);
      //vdec_pp_destroy(vdec_pp_ptr);
      return result;
   }
   //vdec_pp_ptr->vdec_cmn_ptr->voice_module_bbwe = VOICE_BBWE_NONE;     // already done in vdec_init              // by default disable BBWE
   voice_wve_init (&(vdec_ptr->modules.vdec_pp_modules.wve_struct));

   result = voice_wve_v2_init_default (&(vdec_ptr->modules.vdec_pp_modules.wve_v2_struct));   // default initialization of Wide Voice V2
   if (ADSP_FAILED(result))
   {
      MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: Wide Voice V2 default init failed, result(%d) session(%lx) ", result, vdec_ptr->session_num);
      return result;
   }
   
#if defined(LVVE)/*TCT-NB Tianhongwei add for nxp voice solution 2016/01/04*/
	  LVVEFQ_BE_SSRC_init(&(vdec_ptr->modules.vdec_pp_modules.LVVEFQ_BE_SSRC_Instance));
#endif
   vdec_ptr->modules.vdec_pp_modules.oobtty.oobtty_char_flag = FALSE;
   voice_oobtty_reset_fifo (&vdec_ptr->modules.vdec_pp_modules.oobtty.oobtty_rx_struct);
   return result;
}

static void vdec_init_rec_circbuf (vdec_t *vdec_ptr)
{
   if (NULL != vdec_ptr->modules.vdec_pp_modules.record.circ_decout_rec_buf_ptr)
   {
      int16_t beamr_enable = (VOICE_BBWE_BEAMR == vdec_ptr->voice_module_bbwe) ? TRUE : FALSE;
      uint8_t samp_rate_factor;
      int16_t temp_buf[SS_DELAY_RECORD_PATH_DEC * MAX_SAMP_RATE_FACTOR];   // temp 2msec buffer for worst case - fb number of samples
      memset (temp_buf, 0,
            SS_DELAY_RECORD_PATH_DEC * MAX_SAMP_RATE_FACTOR * sizeof(int16_t));   // clear the buffer
      // Init cirucular buffer to 24ms buffer based ont the smapling frequncy

      samp_rate_factor = (voice_get_sampling_rate (vdec_ptr->voc_type, beamr_enable,  vdec_ptr->evs_samp_rate)) / (VOICE_NB_SAMPLING_RATE);

      voice_circbuf_init (&(vdec_ptr->modules.vdec_pp_modules.record.circ_struct_decout_rec), (int8_t*) (vdec_ptr->modules.vdec_pp_modules.record.circ_decout_rec_buf_ptr),
            (int32_t) samp_rate_factor * DEC_OUT_CIRC_BUF_SIZE_REC /* 20ms + 2msec delay*/, MONO_VOICE, (int32_t) 16 /*bitperchannel*/
            );
      // add pre-buffering
      voice_circbuf_write (&(vdec_ptr->modules.vdec_pp_modules.record.circ_struct_decout_rec), (int8_t *) &(temp_buf[0]), samp_rate_factor * SS_DELAY_RECORD_PATH_DEC   // add 2ms delay for pre buffering
            );
   }
}

static ADSPResult vdec_pp_reinit (vdec_t* vdec_ptr)
{
   ADSPResult result = ADSP_EOK;
   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: vdec_pp_reinit_cmd begin  session(%lx)", vdec_ptr->session_num);

   // reinit CTM
   vdec_ptr->modules.vdec_pp_modules.ctm.ctm_from_far_end_detect_notified =
      FALSE;
   voice_ctm_rx_end (&vdec_ptr->modules.vdec_pp_modules.ctm.ctm_rx_struct);
   voice_ctm_rx_init (&vdec_ptr->modules.vdec_pp_modules.ctm.ctm_rx_struct, vdec_ptr->session_num);
   if (vdec_ptr->modules.vdec_pp_modules.oobtty.oobtty_char_flag)
   {
      result = vdec_send_oobtty_char_accepted (vdec_ptr);
      if (ADSP_FAILED(result))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: RX CHAR ACCEPTED DUMMY EVENT failed session(%lx)", vdec_ptr->session_num);
      }
      vdec_ptr->modules.vdec_pp_modules.oobtty.oobtty_char_flag = FALSE;
      MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: RX CHAR ACCEPTED DUMMY EVENT sent in vdec_pp_reinit  session(%lx)", vdec_ptr->session_num);
   }
   vdec_pp_modules_init (vdec_ptr);

   // reinit the SlowTalk module
   voice_slow_talk_end (&(vdec_ptr->modules.vdec_pp_modules.slow_talk_struct));
   voice_slow_talk_init (&(vdec_ptr->modules.vdec_pp_modules.slow_talk_struct));

   // reinit the DTMF Detection Module
   vdec_ptr->modules.vdec_pp_modules.dtmf_det.apr_info_dtmf.self_addr = NULL;
   vdec_ptr->modules.vdec_pp_modules.dtmf_det.apr_info_dtmf.self_port = NULL;
   vdec_ptr->modules.vdec_pp_modules.dtmf_det.apr_info_dtmf.client_addr = NULL;
   vdec_ptr->modules.vdec_pp_modules.dtmf_det.apr_info_dtmf.client_port = NULL;
   voice_dtmf_detect_end (&(vdec_ptr->modules.vdec_pp_modules.dtmf_det.dtmf_det_struct));
   result = voice_dtmf_detect_init (&(vdec_ptr->modules.vdec_pp_modules.dtmf_det.dtmf_det_struct),
         VOICE_FB_SAMPLING_RATE, MAX_FRAME_SIZE);
   if (ADSP_FAILED(result))
   {
      MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: DTMF DETECT init failed, result(%d) session(%lx) ", result, vdec_ptr->session_num);
      //vdec_pp_destroy(vdec_pp_ptr); //PG: cannot destroy while the thread is active
      return result;
   }

   vdec_ptr->modules.vdec_pp_modules.mute.mute = VOICE_UNMUTE;

   MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: vdec_pp_reinit_cmd end result(%d) session(%lx)", result, vdec_ptr->session_num);
   return result;
}

static void vdec_pp_allocate_mem_free (vdec_t *vdec_ptr)
{
   voice_eans_end (&(vdec_ptr->modules.vdec_pp_modules.eans_struct));

   if (NULL != vdec_ptr->modules.vdec_pp_modules.record.ss_struct_ptr)
   {
      voice_ss_end (&(vdec_ptr->modules.vdec_pp_modules.record.ss_struct));
      vdec_ptr->modules.vdec_pp_modules.record.ss_struct_ptr = NULL;
   }

   voice_slow_talk_end (&(vdec_ptr->modules.vdec_pp_modules.slow_talk_struct));
   vdec_ptr->modules.vdec_pp_modules.slow_talk_struct.exp_struct_ptr = NULL;
   vdec_ptr->modules.vdec_pp_modules.slow_talk_struct.st_data_ptr = NULL;

   voice_wve_end (&(vdec_ptr->modules.vdec_pp_modules.wve_struct));

   voice_wve_v2_end (&(vdec_ptr->modules.vdec_pp_modules.wve_v2_struct));
#if defined(LVVE)/*TCT-NB Tianhongwei add for nxp voice solution 2016/01/04*/
	  LVVEFQ_BE_SSRC_end(&(vdec_ptr->modules.vdec_pp_modules.LVVEFQ_BE_SSRC_Instance));
#endif

   vdec_ptr->modules.vdec_pp_modules.record.circ_decout_rec_buf_ptr = NULL;

   vdec_ptr->scratch_ptr = NULL;

   if (vdec_ptr->modules.vdec_pp_modules.mute.soft_mute_capi.module_ptr)
   {
      vdec_ptr->modules.vdec_pp_modules.mute.soft_mute_capi.module_ptr->vtbl_ptr->end (vdec_ptr->modules.vdec_pp_modules.mute.soft_mute_capi.module_ptr);   //soft mute structure should be reset at stop cmd
      qurt_elite_memory_free (vdec_ptr->modules.vdec_pp_modules.mute.soft_mute_capi.module_ptr);
      vdec_ptr->modules.vdec_pp_modules.mute.soft_mute_capi.module_ptr = NULL;
   }

   if (vdec_ptr->modules.vdec_pp_modules.rx_gain.module_ptr)
   {
      vdec_ptr->modules.vdec_pp_modules.rx_gain.module_ptr->vtbl_ptr->end (vdec_ptr->modules.vdec_pp_modules.rx_gain.module_ptr);
      qurt_elite_memory_free (vdec_ptr->modules.vdec_pp_modules.rx_gain.module_ptr);
      vdec_ptr->modules.vdec_pp_modules.rx_gain.module_ptr = NULL;
   }

   qurt_elite_memory_free (vdec_ptr->vdec_pp_memory.start_addr_ptr);
   vdec_ptr->vdec_pp_memory.start_addr_ptr = NULL;
   vdec_ptr->vdec_pp_memory.usage_addr_ptr = NULL;
   vdec_ptr->vdec_pp_memory.size = 0;
   (vdec_ptr->modules.vdec_pp_modules.ctm.ctm_rx_struct).ctm_rx_struct_instance_ptr =
      NULL;
}

static void vdec_do_sample_slip_stuff (vdec_t* vdec_ptr, int16_t *in_ptr, int16_t *out_ptr, int16_t slip_stuff_samples)
{
   uint16_t frame_size_10msec = vdec_ptr->frame_samples_dec >> 1;   //size of 10 msec data.
   uint16_t offset_10msec = 0, output_samples;
   uint16_t input_samples = frame_size_10msec;   //input samples = 10 msec frame samples

   uint16_t num_10msec_frames = 2;

   // Process 10 msec frames. adjust samples in second 10 msec frames
   // when num_10msec_frame=1, adjust drift in same frame i.e for nb do 79->79(stuff) or 81->81(slip) correction
   // when num_10msec_frame=2, adjust drift in second 10 msec frame i.e for nb do 80->80 and 79->79(stuff) or 81->81(slip) in second 10 msec frame.
   for (uint16_t i = 0; i < num_10msec_frames; i++)
   {
      output_samples = input_samples - (i * slip_stuff_samples);
      offset_10msec = frame_size_10msec * i;

      //dbg: MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Voice SS vdec: voice_sample_slip_process-in_frame_size=%d,out_frame_size=%d,i=%d",input_samples,output_samples,i);

      (void) voice_sample_slip_process (&(vdec_ptr->modules.vdec_pp_modules.record.ss_struct), &out_ptr[offset_10msec], &in_ptr[offset_10msec], input_samples, output_samples);

      if ((int8_t) vdec_ptr->modules.vdec_pp_modules.record.ss_multiframe_counter > 0)
      {
         //decrease multiframe counter
         vdec_ptr->modules.vdec_pp_modules.record.ss_multiframe_counter -= 1;
      }
   }
}

static ADSPResult vdec_pp_process (vdec_t* vdec_ptr)
{
   ADSPResult result = ADSP_EOK;
   int16_t *in_ptr = (int16_t*) &(vdec_ptr->dec_out_buf[0]);   // pointer to local input buffer
   int16_t *out_ptr = (int16_t*) &(vdec_ptr->pp_out_buf[0]);   // pointer to local output buffer

   //MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: vdec_pp_process begin");

   int16_t beamr_enable = (VOICE_BBWE_BEAMR == vdec_ptr->voice_module_bbwe) ? TRUE : FALSE;

   if (vdec_ptr->process_data)
   {
      if ( TRUE == vdec_ptr->modules.vdec_pp_modules.record.enable_recording)
      {
         int16_t slip_stuff_samples = 0;
         int16_t *local_ss_ptr = (int16_t*) vdec_ptr->scratch_ptr;   // local input buffer

         //calculate drift with respect to av/hp timer
         vdec_cal_drift (vdec_ptr);
         //calculate sample slipping on the pp out put w.r.t external audio reference port
         vdec_cal_sample_slip_stuff (vdec_ptr, &(slip_stuff_samples));
         //conversion from VFR rate to Codec rate
         vdec_do_sample_slip_stuff (vdec_ptr, in_ptr, local_ss_ptr, slip_stuff_samples);
         //copy the output in to circualr buffer
         result = voice_circbuf_write (&vdec_ptr->modules.vdec_pp_modules.record.circ_struct_decout_rec, (int8_t *) local_ss_ptr, (vdec_ptr->frame_samples_dec - slip_stuff_samples));
         if (ADSP_FAILED(result))
         {
            MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: circular buffer write error for recording session(%lx)", vdec_ptr->session_num);
         }
         vdec_ptr->wait_mask |= VDEC_REC_BUF_MASK;
      }
   }

#if defined(__qdsp6__) && !defined(SIM)
   int16_t *post_dec_in_temp = (int16_t*)vdec_ptr->scratch_ptr;
   memscpy( post_dec_in_temp, VOICE_ROUNDTO8(VDEC_SCRATCH_MEM_SIZE_IN_BYTES) ,in_ptr, vdec_ptr->frame_samples_dec<<1);   // destinatoin is size allocated to scratch buffer  // worst scratch memory size = 22 ms for FB
#endif

   //Copy input to output before starting any processing
   memsmove ((int8_t*) out_ptr, sizeof(vdec_ptr->pp_out_buf), (int8_t*) in_ptr, vdec_ptr->frame_samples_dec << 1);

   //dbg: MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "vdec_stream_pp begin :(%d)\n",vdec_pp_ptr->vdec_cmn_ptr->session_num); //TODO: not needed

   //CTM Rx processing
   result = vdec_process_ctm_rx (vdec_ptr, out_ptr, out_ptr, vdec_ptr->frame_samples_dec);
   if (ADSP_FAILED(result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: CTM-Rx Processing Error session(%lx)", vdec_ptr->session_num);
   }
   //CTM Rx processing
   result = vdec_process_oobtty_rx (vdec_ptr, out_ptr, out_ptr, vdec_ptr->frame_samples_dec);
   if (ADSP_FAILED(result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: OOB TTY-Rx Processing Error session(%lx)", vdec_ptr->session_num);
   }
   //DTMF detection processing
   voice_dtmf_detect_process_wrapper (&(vdec_ptr->modules.vdec_pp_modules.dtmf_det.dtmf_det_struct), out_ptr, out_ptr, vdec_ptr->frame_samples_dec, vdec_ptr->sampling_rate_dec);

   if (TRUE == voice_dtmf_detect_get_tone_status (&(vdec_ptr->modules.vdec_pp_modules.dtmf_det.dtmf_det_struct)))
   {
      result = (ADSPResult) vdec_send_vsm_dtmf_tone_status (vdec_ptr);
      if (ADSP_FAILED(result))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: DTMF_TONE_STATUS_SEND failed session(%lx)", vdec_ptr->session_num);
      }
   }
   {
      // Call voice rx gain module process function
      capi_v2_buf_t vdec_pp_buf = { (int8_t*) out_ptr, vdec_ptr->frame_samples_dec<<1, vdec_ptr->frame_samples_dec<<1};
      capi_v2_stream_data_t vdec_pp_stream_in[1];
      capi_v2_stream_data_t *vdec_pp_stream_in_ptr[] =  { &vdec_pp_stream_in[0] };
      vdec_pp_stream_in[0].bufs_num = 1;
      vdec_pp_stream_in[0].buf_ptr = &vdec_pp_buf;

      vdec_ptr->modules.vdec_pp_modules.rx_gain.module_ptr->vtbl_ptr->process (vdec_ptr->modules.vdec_pp_modules.rx_gain.module_ptr, vdec_pp_stream_in_ptr, vdec_pp_stream_in_ptr);
   }

   //EANS processing
   result = voice_eans_process (&(vdec_ptr->modules.vdec_pp_modules.eans_struct), out_ptr, out_ptr, vdec_ptr->frame_samples_dec, sizeof(vdec_ptr->pp_out_buf));
   if (ADSP_FAILED(result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: EANS Processing Error session(%lx)", vdec_ptr->session_num);
   }

   //SlowTalk processing
   {
      result = voice_slow_talk_process (&(vdec_ptr->modules.vdec_pp_modules.slow_talk_struct), out_ptr, out_ptr, sizeof(vdec_ptr->pp_out_buf));
      if (ADSP_FAILED(result))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: SlowTalk Processing Error session(%lx)", vdec_ptr->session_num);
      }
   }

#if defined(__qdsp6__) && !defined(SIM)
   // log saved input buf and output prior to WV, so both are always at same vocoder sampling rate
   int8_t *buf_ptr[4] =
   {  (int8_t *) post_dec_in_temp, (int8_t *) out_ptr, NULL, NULL};
   uint32_t sampling_rate_log_id;
   sampling_rate_log_id = voice_get_sampling_rate_log_id(voice_get_sampling_rate(vdec_ptr->voc_type,beamr_enable,vdec_ptr->evs_samp_rate));
   voice_log_buffer( buf_ptr,
         VOICE_LOG_TAP_POINT_VDEC_OUT,
         (sampling_rate_log_id << 3) | vdec_ptr->session_num,
         qurt_elite_timer_get_time(),
         VOICE_LOG_DATA_FORMAT_PCM_STEREO_NON_INTLV,
         voice_get_sampling_rate(vdec_ptr->voc_type,beamr_enable,vdec_ptr->evs_samp_rate),
         vdec_ptr->frame_samples_dec<<1,
         NULL);
#endif

   //WVE processing
   voice_wve_process (&(vdec_ptr->modules.vdec_pp_modules.wve_struct), out_ptr, out_ptr, vdec_ptr->frame_samples_dec << 1, sizeof(vdec_ptr->pp_out_buf),
         (vdec_ptr->voice_module_bbwe == VOICE_BBWE_WV_V1) ? TRUE : FALSE);
#if defined(LVVE)/*TCT-NB Tianhongwei add for nxp voice solution 2016/01/04*/
	  //MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"LVVE: vdec_pp_ptr->voice_module_bbwe(%lx)",vdec_pp_ptr->vdec_cmn_ptr->voice_module_bbwe);
	  if(vdec_ptr->voice_module_bbwe == VOICE_BBWE_WV_V2 && voice_get_sampling_rate(vdec_ptr->voc_type, beamr_enable,vdec_ptr->evs_samp_rate) == VOICE_NB_SAMPLING_RATE)
	  {
		//MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"LVVE: LVVEFQ_BE_SSRC_process vdec_pp_ptr->voice_module_bbwe(%lx)",vdec_pp_ptr->vdec_cmn_ptr->voice_module_bbwe);
		LVVEFQ_BE_SSRC_process(&(vdec_ptr->modules.vdec_pp_modules.LVVEFQ_BE_SSRC_Instance), out_ptr, out_ptr, vdec_ptr->frame_samples_dec<<1);
	  }
#else
   //WVE V2 processing
   voice_wve_v2_process (&(vdec_ptr->modules.vdec_pp_modules.wve_v2_struct), out_ptr, out_ptr, vdec_ptr->frame_samples_dec << 1, sizeof(vdec_ptr->pp_out_buf),
         (vdec_ptr->voice_module_bbwe == VOICE_BBWE_WV_V2) ? TRUE : FALSE);
#endif/*TCT-NB Tianhongwei add for nxp voice solution 2016/01/04*/

   uint32_t sampling_rate = voice_get_sampling_rate (vdec_ptr->voc_type, beamr_enable,vdec_ptr->evs_samp_rate);
   uint8_t sampling_rate_factor = (vdec_ptr->samp_rate_factor);
   vdec_ptr->pp_out_buf_size = (VOICE_FRAME_SIZE_NB_20MS * sampling_rate_factor) << 1;
   if ( VOICE_NB_SAMPLING_RATE == sampling_rate)
   {
      if (VOICE_BBWE_NONE != vdec_ptr->voice_module_bbwe)
      {
         vdec_ptr->pp_out_buf_size = (VOICE_FRAME_SIZE_NB_20MS * 2) << 1;
      }
   }

   if (TRUE == vdec_ptr->modules.vdec_pp_modules.mute.new_mute_cmd)
   {
      capi_v2_buf_t buf_soft_mute;
      buf_soft_mute.actual_data_len = buf_soft_mute.max_data_len = sizeof(voice_soft_mute_cal_param_t);
      buf_soft_mute.data_ptr = (int8_t*) &vdec_ptr->modules.vdec_pp_modules.mute.set_param_cached;
      vdec_ptr->modules.vdec_pp_modules.mute.soft_mute_capi.module_ptr->vtbl_ptr->set_param (vdec_ptr->modules.vdec_pp_modules.mute.soft_mute_capi.module_ptr,
            VOICE_PARAM_SOFT_MUTE, NULL, &buf_soft_mute);
      vdec_ptr->modules.vdec_pp_modules.mute.new_mute_cmd = FALSE;
   }

   //Apply soft mute. If mute is not enabled it copies the input to output buffer
   {
      capi_v2_buf_t vdec_pp_buf =
      { (int8_t*) out_ptr, vdec_ptr->pp_out_buf_size, vdec_ptr->pp_out_buf_size };
      capi_v2_stream_data_t vdec_pp_stream_in[1];
      capi_v2_stream_data_t *vdec_pp_stream_in_ptr[] =
      { &vdec_pp_stream_in[0] };
      vdec_pp_stream_in[0].bufs_num = 1;
      vdec_pp_stream_in[0].buf_ptr = &vdec_pp_buf;

      vdec_ptr->modules.vdec_pp_modules.mute.soft_mute_capi.module_ptr->vtbl_ptr->process (vdec_ptr->modules.vdec_pp_modules.mute.soft_mute_capi.module_ptr, vdec_pp_stream_in_ptr,
            vdec_pp_stream_in_ptr);
   }
   //We have atleast one frame Size worth of data which can be sent downstream
   //so set the waitmask also to wait for buffer
   //listen to output buf for delivery to downstream svc
   if (vdec_ptr->process_data)
   {
      vdec_ptr->wait_mask |= VDEC_BUF_MASK;
   }

   //MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: vdec_pp_process end");

   return result;
}

static int32_t vdec_send_oobtty_char_accepted (vdec_t *vdec_ptr)
{
   int32_t rc = ADSP_EOK;
   rc = elite_apr_if_alloc_send_event (vdec_ptr->apr_info_ptr->apr_handle, vdec_ptr->apr_info_ptr->self_addr, vdec_ptr->apr_info_ptr->self_port, vdec_ptr->apr_info_ptr->client_addr,
         vdec_ptr->apr_info_ptr->client_port, vdec_ptr->modules.vdec_pp_modules.oobtty.oobtty_client_token,
         VSM_EVT_OUTOFBAND_TTY_RX_CHAR_ACCEPTED,
         NULL, 0);

   if (ADSP_FAILED(rc))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: Failed to create APR request for RX CHAR ACCEPTED event 0x%08lx", rc);
   }
   return rc;
}

static ADSPResult vdec_rec_buf_handler (void *instance_ptr)
{
   ADSPResult result = ADSP_EOK;
   vdec_t *vdec_ptr = (vdec_t *) instance_ptr;
   qurt_elite_bufmgr_node_t out_buf_mgr_node;
   MSG_1(MSG_SSID_QDSP6, DBG_LOW_PRIO, "VCP: vdec_rec_buf_handler begin session(%lx)", vdec_ptr->session_num);

   if (FALSE == vdec_ptr->process_data)
   {
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: Not in RUN state, this should not happen!! session(%lx)", vdec_ptr->session_num);
      return ADSP_ENOTREADY;
   }
   if ( TRUE == vdec_ptr->modules.vdec_pp_modules.record.enable_recording)
   {
      if (TRUE == vdec_ptr->modules.vdec_pp_modules.record.send_media_type_rec)
      {
         result = vdec_send_afe_media_type (vdec_ptr);
      }
      else
      {
         // Take next buffer off the Q
         result = qurt_elite_queue_pop_front (vdec_ptr->modules.vdec_pp_modules.record.rec_buf_q_ptr, (uint64_t*) &out_buf_mgr_node);
         if (ADSP_EOK != result)
         {
            MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: Error reading buf queue, result(%d) session(%lx)", result, vdec_ptr->session_num);
         }
         else
         {
            result = vdec_send_rec_buf_to_afe (vdec_ptr, &out_buf_mgr_node);
            if (!ADSP_FAILED(result))
            {   // make the flag false if the result was success
               vdec_ptr->modules.vdec_pp_modules.record.rec_first_frame = FALSE;
            }
         }
      }
   }
   //dbg: MSG_1(MSG_SSID_QDSP6, DBG_LOW_PRIO, "vdec_buf_handler end session(%lx)",vdec_pp_ptr->vdec_cmn_ptr->session_num);
   return result;
}

static ADSPResult vdec_send_afe_media_type (vdec_t *vdec_ptr)
{
   ADSPResult result = ADSP_EOK;
   qurt_elite_bufmgr_node_t buf_mgr_node;
   int32_t actual_size;
   elite_msg_any_t media_type_msg;
   MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: vdec_send_afe_media_type begin session(%lx)", vdec_ptr->session_num);

   if (ADSP_FAILED(elite_mem_get_buffer (sizeof(elite_msg_data_media_type_apr_t) + sizeof(elite_multi_channel_pcm_fmt_blk_t), &buf_mgr_node, (int * )&actual_size)))
   {
      return ADSP_ENEEDMORE;
   }

   elite_msg_data_media_type_apr_t* pMediaTypePayload = (elite_msg_data_media_type_apr_t*) buf_mgr_node.pBuffer;

   pMediaTypePayload->pBufferReturnQ = buf_mgr_node.pReturnQ;
   pMediaTypePayload->pResponseQ = NULL;
   pMediaTypePayload->unClientToken = NULL;

   pMediaTypePayload->unMediaTypeFormat = ELITEMSG_DATA_MEDIA_TYPE_APR;
   pMediaTypePayload->unMediaFormatID = ELITEMSG_MEDIA_FMT_MULTI_CHANNEL_PCM;

   elite_multi_channel_pcm_fmt_blk_t *pMediaFormatBlk = (elite_multi_channel_pcm_fmt_blk_t*) elite_msg_get_media_fmt_blk (pMediaTypePayload);
   memset (pMediaFormatBlk, 0, sizeof(elite_multi_channel_pcm_fmt_blk_t));

   pMediaFormatBlk->num_channels = 1;
   pMediaFormatBlk->channel_mapping[0] = PCM_CHANNEL_C;
   if (VSM_RECORD_TX_RX_STEREO == vdec_ptr->modules.vdec_pp_modules.record.recording_mode)
   {
      pMediaFormatBlk->num_channels = 2;   // for stereo recording
      pMediaFormatBlk->channel_mapping[0] = PCM_CHANNEL_L;
      pMediaFormatBlk->channel_mapping[1] = PCM_CHANNEL_R;
   }
   pMediaFormatBlk->bits_per_sample = 16;

   int16_t beamr_enable = (VOICE_BBWE_BEAMR == vdec_ptr->voice_module_bbwe) ? TRUE : FALSE;

   pMediaFormatBlk->sample_rate = voice_get_sampling_rate (vdec_ptr->voc_type, beamr_enable,vdec_ptr->evs_samp_rate);
   pMediaFormatBlk->is_signed = TRUE;
   pMediaFormatBlk->is_interleaved = FALSE;

   media_type_msg.unOpCode = ELITE_DATA_MEDIA_TYPE;
   media_type_msg.pPayload = (void*) pMediaTypePayload;

   result = qurt_elite_queue_push_back (vdec_ptr->modules.vdec_pp_modules.record.afe_handle_ptr->dataQ, (uint64_t*) &media_type_msg);
   if (ADSP_FAILED(result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: Vdec failed to send afe media type in BufHandler() session(%lx)", vdec_ptr->session_num);
      (void) elite_msg_push_payload_to_returnq (buf_mgr_node.pReturnQ, (elite_msg_any_payload_t*) buf_mgr_node.pBuffer);
   }
   else
   {
      MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: vdec delivered media type to AFE session(%lx)", vdec_ptr->session_num);
      vdec_ptr->modules.vdec_pp_modules.record.send_media_type_rec = FALSE;
   }
   MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: vdec_send_afe_media_type end, result(%d) session(%lx)", result, vdec_ptr->session_num);
   return result;
}

static ADSPResult vdec_send_rec_buf_to_afe (vdec_t *vdec_ptr, qurt_elite_bufmgr_node_t *out_buf_mgr_node_ptr)
{
   ADSPResult result = ADSP_EOK;
   elite_msg_any_t* peer_data_Q_msg_ptr;
   elite_msg_data_buffer_t* out_data_payload_ptr = (elite_msg_data_buffer_t*) out_buf_mgr_node_ptr->pBuffer;
   uint8_t samp_rate_factor = vdec_ptr->samp_rate_factor;
   int32_t sample_rate_ms_samples = samp_rate_factor * 8;
   int32_t output_buf_size = voice_cmn_int_div (vdec_ptr->modules.vdec_pp_modules.record.circ_struct_decout_rec.unread_samples, sample_rate_ms_samples);   // finding the number of 1ms samples

   output_buf_size = (output_buf_size * sample_rate_ms_samples) << 1;   // covert to num samples, then to bytes

   out_data_payload_ptr->pResponseQ = NULL;
   out_data_payload_ptr->unClientToken = NULL;
   out_data_payload_ptr->pBufferReturnQ = (vdec_ptr->modules.vdec_pp_modules.record.rec_buf_q_ptr);
   out_data_payload_ptr->nOffset = 0;
   out_data_payload_ptr->nActualSize = output_buf_size;
   if (VSM_RECORD_TX_RX_STEREO == vdec_ptr->modules.vdec_pp_modules.record.recording_mode)
   {
      out_data_payload_ptr->nActualSize = out_data_payload_ptr->nActualSize << 1;
   }

   int16_t *recording_input = (int16_t*) vdec_ptr->scratch_ptr;   //22msec data for FB case
   int16_t *rec_buf_ptr = (int16_t *) (&(out_data_payload_ptr->nDataBuf));
   int i;
   // Read one Frame from output data
   // TOOD: Move this into process?
   {
      if (TRUE == vdec_ptr->modules.vdec_pp_modules.record.rec_first_frame)   // TODO: not sure why this is needed
      {
         // send 20ms of zeros, which will act as pre-buffering to take care of processing jitters
         // pre-buffering for processing jitters should be atleast "max. processing time - min. processing time"
         // since delay is not a concern in recording, we are considering  "max. processing time - min. processing time = 20ms"
         memset (recording_input, 0, output_buf_size);
      }
      else
      {
         voice_circbuf_read (&vdec_ptr->modules.vdec_pp_modules.record.circ_struct_decout_rec, (int8_t*) recording_input, (int32_t) output_buf_size / 2,
               VOICE_ROUNDTO8(VDEC_SCRATCH_MEM_SIZE_IN_BYTES));
         vdec_ptr->wait_mask ^= VDEC_REC_BUF_MASK;   // don't listen to output buf
      }

      if (VSM_RECORD_TX_RX_STEREO == vdec_ptr->modules.vdec_pp_modules.record.recording_mode)
      {
         /* filling the left channel with zeroes for stereo mode recording */
         for (i = 0; i < out_data_payload_ptr->nActualSize / 4; i++)
         {
            rec_buf_ptr[i] = 0;
            rec_buf_ptr[i + out_data_payload_ptr->nActualSize / 4] = recording_input[i];
         }
      }
      else
      {
         for (i = 0; i < out_data_payload_ptr->nActualSize / 2; i++)
         {
            rec_buf_ptr[i] = recording_input[i];
         }
      }
   }

   // send output buf to downstream service
   peer_data_Q_msg_ptr = elite_msg_convt_buf_node_to_msg (out_buf_mgr_node_ptr,
         ELITE_DATA_BUFFER,
         NULL, /* do not need response */
         0, /* token */
         0 /* do not care response result*/
         );
   // todo: check for downstreampeer null
   result = qurt_elite_queue_push_back (vdec_ptr->modules.vdec_pp_modules.record.afe_handle_ptr->dataQ, (uint64_t*) peer_data_Q_msg_ptr);
   if (ADSP_FAILED(result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: Failed to deliver buffer dowstream. Dropping session(%lx)", vdec_ptr->session_num);
      (void) elite_msg_push_payload_to_returnq (vdec_ptr->modules.vdec_pp_modules.record.rec_buf_q_ptr, (elite_msg_any_payload_t*) out_buf_mgr_node_ptr->pBuffer);
   }
   else
   {
      //dbg: MSG_3(MSG_SSID_QDSP6, DBG_MED_PRIO, "Delivered buffer dowstream, muteCmd(%d) bytes(%d) session(%lx)",vdec_pp_ptr->modules.mute.mute,vdec_pp_ptr->pp_out_buf_size,vdec_pp_ptr->vdec_cmn_ptr->session_num);
   }
   return result;
}

static ADSPResult vdec_stream_pp_init (vdec_t *vdec_ptr)
{
   ADSPResult result = ADSP_EOK;
   int16_t beamr_enable = (VOICE_BBWE_BEAMR == vdec_ptr->voice_module_bbwe) ? TRUE : FALSE;

   if ( VOICE_NB_SAMPLING_RATE != vdec_ptr->sampling_rate_dec)
   {
      result = dtmf_det_resampler_init (&(vdec_ptr->modules.vdec_pp_modules.dtmf_det.dtmf_det_struct), vdec_ptr->sampling_rate_dec);
      if (ADSP_FAILED(result))
      {
         MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: Resampler init for DTMF detection failed in VDEC, result(%d) session(%lx) ", result, vdec_ptr->session_num);
         return result;
      }
   }

   voice_slow_talk_resampler_init (&(vdec_ptr->modules.vdec_pp_modules.slow_talk_struct), vdec_ptr->sampling_rate_dec);

   //stream pp initialization
   voice_eans_init (&(vdec_ptr->modules.vdec_pp_modules.eans_struct), voice_get_sampling_rate (vdec_ptr->voc_type, beamr_enable,vdec_ptr->evs_samp_rate));

   result = voice_wve_v2_init (&(vdec_ptr->modules.vdec_pp_modules.wve_v2_struct), voice_get_sampling_rate (vdec_ptr->voc_type, beamr_enable,vdec_ptr->evs_samp_rate));
   if (ADSP_FAILED(result))
   {
       MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: Wide Voice V2 init  failed in VDEC PP run cmnd, result(%d) session(%lx) ", result, vdec_ptr->session_num);
       return result;
   }
   {
      //Init properties CAPI_V2_INPUT_MEDIA_FORMAT based on mediatype
      voice_capi_data_format_struct_t voice_std_mediatype;
      capi_v2_err_t result = CAPI_V2_EOK;

      //init mediatype
      voice_std_mediatype.media_format.format_header.data_format = CAPI_V2_FIXED_POINT;
      voice_std_mediatype.data_format.bits_per_sample = 16;
      voice_std_mediatype.data_format.sampling_rate = vdec_get_sampling_rate (vdec_ptr->voc_type, vdec_ptr->voice_module_bbwe,  vdec_ptr->evs_samp_rate);
      voice_std_mediatype.data_format.data_is_signed = TRUE;
      voice_std_mediatype.data_format.num_channels = 1;

      capi_v2_prop_t init_props[] =
      {
         { CAPI_V2_INPUT_MEDIA_FORMAT, { reinterpret_cast<int8_t*> (&voice_std_mediatype), sizeof(voice_std_mediatype), sizeof(voice_std_mediatype) }, { FALSE, FALSE, 0 } },
      };
      capi_v2_proplist_t init_props_list = { VOICE_SIZE_OF_ARRAY(init_props), init_props };

      result = vdec_ptr->modules.vdec_pp_modules.mute.soft_mute_capi.module_ptr->vtbl_ptr->set_properties (vdec_ptr->modules.vdec_pp_modules.mute.soft_mute_capi.module_ptr, &init_props_list);
      if (CAPI_V2_FAILED(result))
      {
         MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: Soft Mute set property failed sampling_rate(%d) session(%lx) ", (int )voice_get_sampling_rate (vdec_ptr->voc_type, beamr_enable,vdec_ptr->evs_samp_rate),
               vdec_ptr->session_num);
      }


      voice_std_mediatype.data_format.sampling_rate = voice_get_sampling_rate (vdec_ptr->voc_type, beamr_enable, vdec_ptr->evs_samp_rate);

      result = vdec_ptr->modules.vdec_pp_modules.rx_gain.module_ptr->vtbl_ptr->set_properties (vdec_ptr->modules.vdec_pp_modules.rx_gain.module_ptr, &init_props_list);
      if (CAPI_V2_FAILED(result))
      {
         MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: Rx gain set property failed sampling_rate(%d) session(%lx) ", (int )voice_get_sampling_rate (vdec_ptr->voc_type, beamr_enable,vdec_ptr->evs_samp_rate),
               vdec_ptr->session_num);
      }
   }

   return ADSP_EOK;
}

static ADSPResult vdec_send_mixer_media_type (vdec_t *vdec_ptr)
{
   ADSPResult result = ADSP_EOK;
   qurt_elite_bufmgr_node_t buf_mgr_node;
   int32_t actual_size;
   elite_msg_any_t media_type_msg;
   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: vdec_send_mixer_media_type begin session(%lx)", vdec_ptr->session_num);

   if (ADSP_FAILED(elite_mem_get_buffer (sizeof(elite_msg_data_media_type_apr_t) + sizeof(elite_multi_channel_pcm_fmt_blk_t), &buf_mgr_node, (int * )&actual_size)))
   {
      return ADSP_ENEEDMORE;
   }

   elite_msg_data_media_type_apr_t* pMediaTypePayload = (elite_msg_data_media_type_apr_t*) buf_mgr_node.pBuffer;

   pMediaTypePayload->pBufferReturnQ = buf_mgr_node.pReturnQ;
   pMediaTypePayload->pResponseQ = NULL;
   pMediaTypePayload->unClientToken = NULL;

   pMediaTypePayload->unMediaTypeFormat = ELITEMSG_DATA_MEDIA_TYPE_APR;
   pMediaTypePayload->unMediaFormatID = ELITEMSG_MEDIA_FMT_MULTI_CHANNEL_PCM;

   elite_multi_channel_pcm_fmt_blk_t *pMediaFormatBlk = (elite_multi_channel_pcm_fmt_blk_t*) elite_msg_get_media_fmt_blk (pMediaTypePayload);
   memset (pMediaFormatBlk, 0, sizeof(elite_multi_channel_pcm_fmt_blk_t));

   pMediaFormatBlk->num_channels = 1;
   pMediaFormatBlk->bits_per_sample = 16;
   pMediaFormatBlk->sample_rate = vdec_get_sampling_rate (vdec_ptr->voc_type, vdec_ptr->voice_module_bbwe,  vdec_ptr->evs_samp_rate);
   pMediaFormatBlk->is_signed = TRUE;
   pMediaFormatBlk->is_interleaved = FALSE;
   pMediaFormatBlk->channel_mapping[0] = PCM_CHANNEL_C;

   media_type_msg.unOpCode = ELITE_DATA_MEDIA_TYPE;
   media_type_msg.pPayload = (void*) pMediaTypePayload;

   result = qurt_elite_queue_push_back (vdec_ptr->downstream_peer_ptr->dataQ, (uint64_t*) &media_type_msg);
   if (ADSP_FAILED(result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: Vdec failed to send afe media type in BufHandler() session(%lx)", vdec_ptr->session_num);
      (void) elite_msg_push_payload_to_returnq (buf_mgr_node.pReturnQ, (elite_msg_any_payload_t*) buf_mgr_node.pBuffer);
   }
   else
   {
      MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "VCP: vdec delivered media type session(%lx)", vdec_ptr->session_num);
      vdec_ptr->send_media_type = FALSE;
   }

   MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: vdec_send_mixer_media_type end, result(%d) session(%lx)", result, vdec_ptr->session_num);
   return result;
}

static void inline vdec_cal_drift (vdec_t *vdec_ptr)
{
   voice_cmn_accu_drift (&vdec_ptr->modules.vdec_pp_modules.record.ss_info_counter, &vdec_ptr->modules.vdec_pp_modules.record.voice_cmn_drift_info_dec_rec,
         vdec_ptr->modules.vdec_pp_modules.record.afe_drift_ptr, vdec_ptr->vfr_source, VOICE_DEC_REC, vdec_ptr->vfr_mode, vdec_ptr->session_num, vdec_ptr->vtm_sub_unsub_decode_data.timing_ver,
         vdec_ptr->vsid);
}

static void vdec_cal_sample_slip_stuff (vdec_t *vdec_ptr, int16_t* slip_stuff_samples)
{
   if (0 != vdec_ptr->modules.vdec_pp_modules.record.ss_info_counter)   //if any samples to correct
   {
      //Check if previous correction is done
      if (0 == vdec_ptr->modules.vdec_pp_modules.record.ss_multiframe_counter)
      {
         //reset multiframe counter
         vdec_ptr->modules.vdec_pp_modules.record.ss_multiframe_counter =
            VDEC_SS_MULTIFRAME;

         if (0 > vdec_ptr->modules.vdec_pp_modules.record.ss_info_counter)
         {
            //This means DMS is faster and we need to do sample stuff
            *slip_stuff_samples = -1;
            //decrease(magnitude)  of info counter
            vdec_ptr->modules.vdec_pp_modules.record.ss_info_counter += (int32_t) 1;   // arithmetic to make the slip stuff counter zero
            MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: ICCR:: vdec sample stuff !!  count (%d) session(%lx)", *slip_stuff_samples, vdec_ptr->session_num);
         }
         else   // if (0 < vdec_pp_ptr->modules.record.ss_info_counter) .
         {
            //DMA is slow so we need to slip 1 samples
            *slip_stuff_samples = 1;
            //decrease(magnitude)  of info counter
            vdec_ptr->modules.vdec_pp_modules.record.ss_info_counter -= (int32_t) 1;   // arithmetic to make the slip counter zero
            MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: ICCR:: vdec sample slip !!  count (%d) session(%lx)", *slip_stuff_samples, vdec_ptr->session_num);
         }
      }
   }
}

static ADSPResult vdec_process_ctm_rx (vdec_t *vdec_ptr, int16_t *in_ptr, int16_t *out_ptr, uint16_t samples)
{
   ADSPResult result = ADSP_EOK;

   // check enable before CTM processing or state variable update
   if (!vdec_ptr->modules.vdec_pp_modules.ctm.ctm_rx_struct.enable)
   {
      return result;
   }

   // TODO: Use Rx<->Tx messaging for peer connect
   // Set state variables
   if ( TRUE == vdec_ptr->tty_state_ptr->m_sync_recover_rx)
   {
      voice_ctm_rx_resync (&vdec_ptr->modules.vdec_pp_modules.ctm.ctm_rx_struct);
      vdec_ptr->tty_state_ptr->m_sync_recover_rx = FALSE;
   }

   voice_ctm_rx_set_ctm_char_transmitted (&vdec_ptr->modules.vdec_pp_modules.ctm.ctm_rx_struct, vdec_ptr->tty_state_ptr->m_ctm_character_transmitted);

   result = voice_ctm_rx_process (&vdec_ptr->modules.vdec_pp_modules.ctm.ctm_rx_struct, in_ptr, out_ptr, samples);

   uint8_t ctm_from_far_end_detected;
   voice_ctm_rx_get_ctm_from_far_endDetected (&vdec_ptr->modules.vdec_pp_modules.ctm.ctm_rx_struct, &ctm_from_far_end_detected);
   if ( FALSE == vdec_ptr->modules.vdec_pp_modules.ctm.ctm_from_far_end_detect_notified &&
         TRUE == ctm_from_far_end_detected)
   {
      vdec_ptr->tty_state_ptr->m_ctm_from_far_end_detected = TRUE;
      vdec_ptr->modules.vdec_pp_modules.ctm.ctm_from_far_end_detect_notified =
         TRUE;
   }
   uint8_t enquiry_from_far_end_detected;
   voice_ctm_rx_get_enquiry_from_far_endDetected (&vdec_ptr->modules.vdec_pp_modules.ctm.ctm_rx_struct, &enquiry_from_far_end_detected);

   // set enq from far end detected flag if high
   if ( TRUE == enquiry_from_far_end_detected)
   {
      vdec_ptr->tty_state_ptr->m_enquiry_from_far_end_detected = enquiry_from_far_end_detected;
   }

   uint8_t rxTTYDetected;
   voice_ctm_rx_get_rxTTYDetected (&vdec_ptr->modules.vdec_pp_modules.ctm.ctm_rx_struct, &rxTTYDetected);
   vdec_ptr->tty_state_ptr->m_rx_tty_detected = (int32_t) rxTTYDetected;

   return result;
}

static ADSPResult vdec_process_oobtty_rx (vdec_t* vdec_ptr, int16_t *in_ptr, int16_t *out_ptr, uint16_t samples)
{
   ADSPResult result = ADSP_EOK;

   // check enable before OOB TTY processing or state variable update
   if (!vdec_ptr->modules.vdec_pp_modules.oobtty.oobtty_rx_struct.enable)
   {
      return result;
   }
   // check if we have new character to render
   if (vdec_ptr->modules.vdec_pp_modules.oobtty.oobtty_char_flag)
   {
      if (voice_oobtty_push_char (&vdec_ptr->modules.vdec_pp_modules.oobtty.oobtty_rx_struct, &vdec_ptr->modules.vdec_pp_modules.oobtty.oobtty_rx_char))
      {
         result = vdec_send_oobtty_char_accepted (vdec_ptr);
         if (ADSP_FAILED(result))
         {
            MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "VCP: RX CHAR ACCEPTED EVENT failed session(%lx)", vdec_ptr->session_num);
         }
         vdec_ptr->modules.vdec_pp_modules.oobtty.oobtty_char_flag = FALSE;
      }
   }

   voice_oobtty_rx_process (&vdec_ptr->modules.vdec_pp_modules.oobtty.oobtty_rx_struct, in_ptr, out_ptr, samples);

   vdec_ptr->tty_state_ptr->m_rx_oobtty_detected = (uint32_t) voice_oobtty_get_rx_detected (&vdec_ptr->modules.vdec_pp_modules.oobtty.oobtty_rx_struct);

   return result;
}

static int32_t vdec_send_vsm_dtmf_tone_status (vdec_t *vdec_ptr)
{
   vsm_rx_dtmf_detected_t dtmf_tone_status;   // low frequency and high frequency
   int32_t rc = ADSP_EOK;
   circbuf_struct *circ_buff_dtmf_tone_status_ptr = &(vdec_ptr->modules.vdec_pp_modules.dtmf_det.dtmf_det_struct.tone_status_circbuf_struct);
   while (circ_buff_dtmf_tone_status_ptr->unread_samples >= 2)   // check if 2=>16 bit samples are unread
   {
      voice_circbuf_read (circ_buff_dtmf_tone_status_ptr, (int8_t*) &dtmf_tone_status, (int32_t) 2, sizeof(vsm_rx_dtmf_detected_t));   //read 2=>16 bit samples
      MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: VSM_EVT_RX_DTMF_DETECTED: low_freq(%d), high_freq(%d), APR pkt sent from Q6 - A9 session(%lx)", dtmf_tone_status.low_freq, dtmf_tone_status.high_freq,
            vdec_ptr->session_num);

      // vsm_rx_dtmf_detected_t has same formatting as the 4 bytes just read from circ buffer
      rc = elite_apr_if_alloc_send_event (vdec_ptr->apr_info_ptr->apr_handle, vdec_ptr->modules.vdec_pp_modules.dtmf_det.apr_info_dtmf.self_addr,
            vdec_ptr->modules.vdec_pp_modules.dtmf_det.apr_info_dtmf.self_port, vdec_ptr->modules.vdec_pp_modules.dtmf_det.apr_info_dtmf.client_addr,
            vdec_ptr->modules.vdec_pp_modules.dtmf_det.apr_info_dtmf.client_port,
            NULL,
            VSM_EVT_RX_DTMF_DETECTED, &dtmf_tone_status, sizeof(vsm_rx_dtmf_detected_t));

      if (ADSP_FAILED(rc))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "VCP: Failed to create APR request for DTMF TONE SEND STATUS  0x%08lx", rc);
      }
   }
   return rc;
}

static ADSPResult voice_rx_gain_set_param (voice_capi_module_t *rx_gain_ptr, int8_t *params_buffer_ptr, uint32_t param_id, uint16_t param_size)
{
   ADSPResult result = ADSP_EOK;
   capi_v2_err_t capi_result = CAPI_V2_EOK;
   int8_t* payload_address = (int8_t*)params_buffer_ptr;

   // populate buffer to issue set param to rx gain capi
   capi_v2_buf_t param_data_buf;
   capi_v2_port_info_t port_info;
   port_info.is_valid = FALSE;
   param_data_buf.data_ptr = payload_address;
   param_data_buf.actual_data_len = param_data_buf.max_data_len = param_size;

   capi_result = rx_gain_ptr->module_ptr->vtbl_ptr->set_param(rx_gain_ptr->module_ptr, param_id, &port_info, &param_data_buf);
   result = capi_v2_err_to_adsp_result(capi_result);
   return result;
}


static ADSPResult voice_rx_gain_get_param (voice_capi_module_t *rx_gain_ptr, int8_t *params_buffer_ptr, uint32_t param_id, uint32_t buffer_size, uint16_t *param_size_ptr)
{
   ADSPResult result = ADSP_EOK;

   // Populate buffer to issue get param to capi modules
   int8_t* payload_address = (int8_t*)params_buffer_ptr;

   capi_v2_buf_t param_data_buf;
   capi_v2_port_info_t port_info;
   capi_v2_err_t capi_result;
   port_info.is_valid = FALSE;
   param_data_buf.data_ptr = payload_address;
   param_data_buf.max_data_len = buffer_size;

   capi_result = rx_gain_ptr->module_ptr->vtbl_ptr->get_param(rx_gain_ptr->module_ptr, param_id, &port_info, &param_data_buf);
   *param_size_ptr = param_data_buf.actual_data_len;
   result = capi_v2_err_to_adsp_result(capi_result);
   return result;
}

capi_v2_err_t vdec_capi_v2_cb_func(void *context_ptr, capi_v2_event_id_t id, capi_v2_event_info_t *event_info_ptr)
{
   capi_v2_err_t result = CAPI_V2_EOK;
   voice_capi_module_t* module_ptr = (voice_capi_module_t*)context_ptr;
   if(NULL == context_ptr)
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Error! Received bad context pointer in vdec callback");
      return CAPI_V2_EFAILED;
   }
   MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: vdec event callback invoked, event_id(%d), module(0x%lx)", (int)id, module_ptr->module_id);
   switch(id)
   {
      case CAPI_V2_EVENT_KPPS:
         {
            capi_v2_event_KPPS_t* kpps_ptr = (capi_v2_event_KPPS_t* )event_info_ptr->payload.data_ptr;
            module_ptr->kpps = kpps_ptr->KPPS;
            break;
         }
      case CAPI_V2_EVENT_ALGORITHMIC_DELAY:
         {
            capi_v2_event_algorithmic_delay_t* delay_ptr = (capi_v2_event_algorithmic_delay_t* )event_info_ptr->payload.data_ptr;
            module_ptr->delay = delay_ptr->delay_in_us;
            break;
         }
      default:
         {
            MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: Unsupported event %d", (int)id);
            return CAPI_V2_EBADPARAM;
         }
   }
   return result;
}

void voice_dtmf_detect_process_wrapper (dtmf_detect_struct_t *dtmf_detect_ptr, int16_t *out_ptr, int16_t *in_ptr, uint16_t no_of_samples, uint16_t sampling_rate)
{
   int16_t dtmf_temp_buf[VOICE_FRAME_SIZE_FB];   // temp buffer to hold 14-bit input samples 960 = 20ms @48Khz

   if (VOICE_FRAME_SIZE_FB >=no_of_samples)
   {
      for (uint32_t i=0; i<no_of_samples; i++)
      {
         dtmf_temp_buf[i] = out_ptr[i] >> 2;   // Convert input from 16-bit to 14-bit before passing to dtmf detect lib
      }
   }

   // call dtmf detect process
   voice_dtmf_detect_process (dtmf_detect_ptr, &dtmf_temp_buf[0], &dtmf_temp_buf[0], no_of_samples, sampling_rate);
}

void voice_ecall_rx (vdec_t *vdec_ptr, int16_t *in_out_ptr)
{
   int16_t ecall_temp_buffer[VOICE_FRAME_SIZE_FB];   // worst case size: 20ms samples at 48KHz

   // Clear ecall_temp_buffer
   memset (&ecall_temp_buffer[0], 0, sizeof(ecall_temp_buffer));

   // If hpcm read enable, client wants to read data from fw.
   // Ecall interface API's are defined for 14-bit input/output
   // Decoder output is in 16-bit format so convert decoder from 16-bit to 14 bit before giving it to client.
   if ((TRUE == vdec_ptr->modules.host_pcm_context.read_config.enable) && (VOICE_FRAME_SIZE_FB >= vdec_ptr->frame_samples_dec))
   {
      for (uint32_t i=0; i<vdec_ptr->frame_samples_dec; i++)
      {
         ecall_temp_buffer[i] = in_out_ptr[i] >> 2;   // convert input 16-bit to 14-bit
      }
   }

   // Call HPCM process
   int16_t *pHostPcmBuf[MAX_NUM_HOST_PCM_CHANNELS] = { (int16_t *) &ecall_temp_buffer[0], NULL, NULL, NULL };
   voice_host_pcm_process (&vdec_ptr->modules.host_pcm_context, 1, pHostPcmBuf, vdec_ptr->frame_samples_dec, 0, TRUE);

   // If hpcm write enable, client has wwritten data to fw.
   // Ecall interface API's are defined for 14-bit input/output
   // Ecall output is in 14-bit format so convert output from 14-bit to 16 bit before processing in fw
   if ((TRUE == vdec_ptr->modules.host_pcm_context.write_config.enable) && (VOICE_FRAME_SIZE_FB >= vdec_ptr->frame_samples_dec))
   {
      for (uint32_t i=0; i<vdec_ptr->frame_samples_dec; i++)
      {
         in_out_ptr[i] = ecall_temp_buffer[i] << 2;   // convert output 14-bit to 16-bit
      }
   }
}


