#ifndef VOICE_FNS_H
#define VOICE_FNS_H

/****************************************************************************
Copyright (c) 2009-20010 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government. Diversion contrary to U.S. law prohibited.
****************************************************************************/
/*====================================================================== */

/*========================================================================
                             Edit History

$Header: //components/rel/avs.adsp/2.7.1.c4/voice/services/voice_dec/src/Voice_FNS.h#1 $

when       who     what, where, why
--------   ---     -------------------------------------------------------
07/15/10   sb       calibration structure added for setparam,getparam.
05/13/10   sb       Added changes for C++ to C conversion
07/17/08   c_rreine Doxygenation.
11/15/07   sj       Created file.

========================================================================== */

/* =======================================================================

                     INCLUDE FILES FOR MODULE

========================================================================== */
#include "Elite.h"
#include "VoiceCmnUtils.h"


/* =======================================================================
                        DATA DECLARATIONS
========================================================================== */
/* -----------------------------------------------------------------------
** Constant / Define Declarations
** ----------------------------------------------------------------------- */
#define FRAME_SIZE_EANS_NB        80     // Minimum number of samples per frame
                                        // for narrowband signals
#define FRAME_SIZE_EANS_WB        160    // Minimum number of samples per frame
                                        // for wideband signals

#define VOICE_PARAM_FNS_V2_VER_ZERO_SIZE   sizeof(eans_interface_params_v2_struct)

#define VOICE_EANS_NUM_PARAMS         42     // number of eans tuning parameters

enum
{
   VOICE_PARAM_FNS_V2_VERSION_0 = 0,
   VOICE_PARAM_FNS_V2_NUM_SUPPORTED_VERSIONS
};
/* -----------------------------------------------------------------------
** Type Declarations
** ----------------------------------------------------------------------- */
typedef struct
{
   int16_t   eans_params[VOICE_EANS_NUM_PARAMS];           //EANS tuning parameters
   int16_t   enable_eans;
   uint16_t  sampling_rate;
   int8_t    params_modified;
   void    *p_eans_static_mem;
}eans_struct_t;
typedef struct
{
   int16_t	eansMode;
   int16_t	eansInputGain;
   int16_t	eansOutputGain;
   int16_t	eansTargetNS;
   int16_t	eansSalpha;
   int16_t	eansNalpha;
   int16_t	eansNalphaMax;
   int16_t	eansEalpha;
   int16_t	eansNSNRmax;
   int16_t	eansSNblock;
   int16_t	eansNi;
   int16_t	eansNPscale;
   int16_t	eansNLambda;
   int16_t	eansNLambdaf;
   int16_t	eansGsBias;
   int16_t	eansGsMax;
   int16_t	eansSalphaHB;
   int16_t	eansNalphaMaxHB;
   int16_t	eansEalphaHB;
   int16_t	eansNLambda0;
   int16_t	eansGsFast;
   int16_t	eansGsMed;
   int16_t	eansGsSlow;
   int16_t	eansSwbNalpha;
   int16_t	eansSwbSalpha;
   int16_t	thresh;
   int16_t	pwrScale;
   int16_t	hangoverMax;
   int16_t	alphaSNR;
   int16_t	snrDiffMax;
   int16_t	snrDiffMin;
   int16_t	initLength;
   int16_t	maxVal;
   int16_t	initBound;
   int16_t	resetBound;
   int16_t	avarScale;
   int16_t	sub_Nc;
   int16_t	spowMin;
} eans_interface_params_struct;                        // interface param structure for  VOICE_PARAM_FNS param ID

typedef struct
{
   int16_t	version;                               // version info 
   int16_t	reserved;                              // reserved field
   eans_interface_params_struct  eans_v1_params;       // VOICE_PARAM_FNS param ID param structure
   int16_t	eansTargetNoiseFloor;                  // Q0.15 Target noise floor level
   int16_t	eansSlopeNoiseFloor;                   // Q0.15 Spectral slope of the target noise floor
} eans_interface_params_v2_struct;                     // interface param structure for  VOICE_PARAM_FNS_V2 param ID


// TODO : this enum need to move to library when we upgrade EANS algorithm
typedef enum
{
   EANS_FRAME_SIZE         = 0,         // Frame length
   EANS_MODE               = 1,         // Mode word
   EANS_INPUT_GAIN         = 2,         // Input gain
   EANS_OUTPUT_GAIN        = 3,         // Output gain
   EANS_TARGET_NS          = 4,         // Target noise suppression in dB
   EANS_SALPHA             = 5,         // Stationary NS over-subtraction factor
   EANS_NALPHA             = 6,         // Non-stationary NS over-subtraction factor
   EANS_NALPHA_MAX         = 7,         // Non-stationary NS max over-subtraction factor
   EANS_EALPHA             = 8,         // Excess NS subtraction factor
   EANS_NSNR_MAX           = 9,         // Non-stationary NS SNR max
   EANS_SN_BLOCK           = 10,        // Block size for stationary NS estimation
   EANS_NI                 = 11,        // Initial block length for non-stationary NS initialization
   EANS_NPSCALE            = 12,        // SNR scale factor for non-stationary NS estimation
   EANS_NLAMBDA            = 13,        // Smoothing factor for non-stationary NS estimation
   EANS_NLAMBDAF           = 14,        // Higher smoothing factor for non-stationary NS estimation
   EANS_GSBIAS             = 15,        // SNR Bias for gain calculation
   EANS_GSMAX              = 16,        // SNR limit for gain calculation
   EANS_SALPHA_HB          = 17,        // HB Stationary NS over-subtraction factor
   EANS_NALPHA_MAX_HB      = 18,        // HB Non-stationary NS max over-subtraction factor
   EANS_EALPHA_HB          = 19,        // HB Excess NS subtraction factor
   EANS_NLAMBDA0           = 20,        // Highest smoothing factor for non-stationary NS
   EANS_AVAD_TH            = 21,        // Single-mic VAD threshold
   EANS_AVAD_PSCALE        = 22,        // Scale factor for speech power level
   EANS_AVAD_HOVER         = 23,        // Hangover limit for VAD decision
   EANS_AVAD_ALPHAS        = 24,        // Averaging factor for SNR smoothing
   EANS_AVAD_SDMAX         = 25,        // Max limit for log SNR difference
   EANS_AVAD_SDMIN         = 26,        // Min limit for log SNR difference
   EANS_AVAD_INITL         = 27,        // Single-mic vad initialization block length
   EANS_AVAD_MAXVAL        = 28,        // Max value for noise power estimate
   EANS_AVAD_INITB         = 29,        // Bound value for initial noise power estimate
   EANS_AVAD_RESETB        = 30,        // Bound value for re-setting noise power estimation
   EANS_AVAD_AVAR          = 31,        // Scale factor for noise power estimation
   EANS_AVAD_NC            = 32,        // Window length for stationary Noise floor estimation
   EANS_AVAD_SPMIN         = 33,        // Minimum limit for speech power level
   EANS_GSM_FAST           = 34,        // Fast gain smoothing factor
   EANS_GSM_MED            = 35,        // Medium gain smoothing factor
   EANS_GSM_SLOW           = 36,        // Slow gain smoothing factor
   EANS_SWB_SALPHA         = 37,        // Stationary NS over-subtraction factor for band above 16 kHz
   EANS_SWB_NALPHA         = 38,        // Non-stationary NS over-subtraction factor for band above 16 kHz
   EANS_TARGET_NOISEFLOOR  = 39,        // Target noise floor level
   EANS_SLOPE_NOISEFLOOR   = 40,        // Slope of the noise floor level
   EANS_PAR_TOTAL          = 41         // Total number of tuning parameters
} eans_params_list;
/* =======================================================================
**                          Function Declarations
** ======================================================================= */

/*************************************************************************
FUNCTION: voice_eans_get_size
*************************************************************************/
/**
Function for EANS module meory allocation and static initialization.

@return None.
*/
uint32_t voice_eans_get_size( );




/*************************************************************************
FUNCTION: voice_eans_init_default
*************************************************************************/
/**
Function for EANS module meory allocation and static initialization.

@return None.
*/
ADSPResult voice_eans_init_default(eans_struct_t  *eans_struct);

/*************************************************************************
FUNCTION: voice_eans_tx_init_default
*************************************************************************/
/**
Function for EANS module meory allocation and static initialization.

@return None.
*/
void voice_eans_tx_init_default(eans_struct_t  *eans_struct, uint32_t sampling_rate);



/*************************************************************************
FUNCTION: voice_eans_init
*************************************************************************/
/**
Function for EANS module static initialization.

@return None.
*/
void voice_eans_init(eans_struct_t  *eans_struct, uint16_t sampling_rate);


/*************************************************************************
FUNCTION: voice_eans_end
*************************************************************************/
/**
This is to free up the memory for EANS module.

@return None.
*/
ADSPResult voice_eans_end(eans_struct_t  *eans_struct);

/*************************************************************************
FUNCTION: voice_eans_tx_end
*************************************************************************/
/**
This is to free up the memory for EANS module.

@return None.
*/

void voice_eans_tx_end(eans_struct_t  *eans_struct_ptr);

/*************************************************************************
FUNCTION:voice_eans_process
*************************************************************************/
/**
This is a function for EANS processing.

@return
- ADSP_EFAILED - The pointers not initialized.
- ADSP_EOK - The processing was successful.
*/
ADSPResult voice_eans_process(eans_struct_t  *eans_struct,
				 int16_t *out_ptr,
             /**< This output parameter is a
             pointer to output Buffer.*/
             int16_t *in_ptr,
             /**< This input parameter is a
             pointer to input Buffer.*/
             uint16_t block_size,
             /**< processing block size.*/
             int16_t out_buff_size
             /* size of output buffer */
             );

/*************************************************************************
FUNCTION: voice_eans_set_param
*************************************************************************/
/**
This function is to set the calibration data

@return
- ADSP_EOK - The initialization was successful.
*/
ADSPResult voice_eans_set_param(eans_struct_t  *eans_struct_ptr,int8_t *params_buffer_ptr,uint32_t param_id,uint16_t param_size);

/*************************************************************************
FUNCTION: voice_eans_get_param
*************************************************************************/
/**
This function is to check the calibration data.

@return
- ADSP_EOK - The initialization was successful.
*/
ADSPResult voice_eans_get_param(eans_struct_t  *eans_struct_ptr,int8_t *params_buffer_ptr,uint32_t param_id,int32_t buffer_size, uint16_t *param_size_ptr);

ADSPResult voice_eans_set_mem(eans_struct_t *eans_struct_ptr,int8 *pMemAddr,uint32_t nSize);

uint32_t eans_get_size();

ADSPResult voice_eans_get_kpps(eans_struct_t* eans_struct_ptr,uint32_t* kpps_ptr, uint16_t sampling_rate);

ADSPResult voice_eans_get_delay(eans_struct_t* eans_struct_ptr,uint32_t* delay_ptr);

/* -----------------------------------------------------------------------
** Global Object Definitions
** ----------------------------------------------------------------------- */

/* =======================================================================
**                          Macro Definitions
** ======================================================================= */

#endif /* VOICE_EANS_H */

