#ifndef VOICE_SLOWTALK_H
#define VOICE_SLOWTALK_H

/*========================================================================
                             Edit History

$Header: //components/rel/avs.adsp/2.7.1.c4/voice/services/voice_dec/src/Voice_SlowTalk.h#1 $

when       who     what, where, why
--------   ---     -------------------------------------------------------
05/13/10   sb       Added changes for C++ to C conversion
10/15/09   sj       Created file.

========================================================================== */

/* =======================================================================

                     INCLUDE FILES FOR MODULE

========================================================================== */
#include "Elite.h"
#include "st_api.h"
#include "voice_resampler.h"
#include "VoiceCmnUtils.h"


/* =======================================================================

                        DATA DECLARATIONS

========================================================================== */
/* -----------------------------------------------------------------------
** Constant / Define Declarations
** ----------------------------------------------------------------------- */

/* -----------------------------------------------------------------------
** Type Declarations
** ----------------------------------------------------------------------- */

//structure as per ISOD
typedef struct
{

   int16_t AVAD_THRESH;  //ST VAD related Parameters start here
   int16_t	AVAD_PWR_SCALE;
   int16_t	AVAD_HANGOVER;
   int16_t	AVAD_ALPHA;
   int16_t	AVAD_SD_MAX;
   int16_t	AVAD_SD_MIN;
   int16_t	AVAD_INIT_LENGTH;
   int16_t	nReserved1;
   int32_t	AVAD_MAX_VAL;
   int32_t	AVAD_INIT_BOUND;
   int16_t	AVAD_SUB_NC;
   int16_t	nReserved2;
   int32_t	AVAD_VAR;
   int16_t	AVAD_SPOW_MIN;
   int16_t	AVAD_CFAC;     //ST VAD related Parameters end here
   int32_t	MAX_DELAY;     //ST expansion related Parameters start here
   int16_t	nReserved3;    //EXPANSION_RATIO moved to a separated IID
   int16_t	MAX_EXP_DIFF;
   int16_t	MIN_PAUSE;
   int16_t	LOOK_AHEAD;
   int32_t	VOICE_QUAL_THRESH1;
   int32_t	VOICE_QUAL_THRESH2;
   int32_t	VOICE_QUAL_THRESH_FORCE; //ST expansion related Parameters start here

}slow_talk_param;

typedef struct
{
    //SlowTalk related parameters
    int		n_slowtalk;            //SlowTalk State
    int32_t	st_param[10];        //Slowtalk Config structure
    int32_t	vad_param[13];       //VAD config structure
    void	*st_data_ptr; //SlowTalk structure
    void  *exp_struct_ptr; //TimeWarping structure
    int32_t nCir_buf_clear;              //flag to clear circular buffer
    int16_t enable_slow_talk;         //flag for ST enabling
    bool_t wide_band;               //flag for WB
    bool_t param_modified;          //flag for Interface modification
    bool_t param_modified_prev;      //flag for Interface modification
    bool_t param_modified_enable;    // flag to indicate module is moved from disable to enable
    voice_resampler_config_struct_t inp_resamp_cfg_struct;
    void   *inp_resamp_channel_mem_ptr;
    voice_resampler_config_struct_t out_resamp_cfg_struct;
    void   *out_resamp_channel_mem_ptr;
    uint32_t sampling_rate;
    uint8_t  sampling_rate_factor;
    uint32_t inp_resamp_inp_block_size;
    uint32_t inp_resamp_out_block_size;
    uint32_t out_resamp_inp_block_size;
    uint32_t out_resamp_out_block_size;

}slow_talk_struct_t;

/* -----------------------------------------------------------------------
** Global Object Definitions
** ----------------------------------------------------------------------- */

/* =======================================================================
**                          Macro Definitions
** ======================================================================= */
#define FRAME_SIZE_ST_NB        160     // Minimum number of samples per frame
                                        // for narrowband signals
#define FRAME_SIZE_ST_WB        320    // Minimum number of samples per frame
                                        // for wideband signals
#define FRAME_SIZE_ST_FB        960    // Minimum number of samples per frame
                                        // for wideband signals


/* =======================================================================
**                          Function Declarations
** ======================================================================= */
/*************************************************************************
FUNCTION:voice_slow_talk_init
*************************************************************************/
/**
This function performs the memory allocation and initialization of
slowtalk module.
@return
- ADSP_ENOMEMORY - The PCM format is not supported.
- ADSP_EOK - The conversion was successful.
*/
ADSPResult voice_slow_talk_init(slow_talk_struct_t *slow_talk_struct_ptr);
/*************************************************************************
FUNCTION:voice_slow_talk_end
*************************************************************************/
/**
This function performs the frees the memory.
@return
- ADSP_EOK - The conversion was successful.
*/
ADSPResult voice_slow_talk_end (slow_talk_struct_t *slow_talk_struct_ptr);

/*************************************************************************
FUNCTION:voice_slow_talk_process
*************************************************************************/
/**
This function performs the processing of slow talk.
@return
- ADSP_EFAILED - Pointers are NULL.
- ADSP_EOK - The conversion was successful.
*/
ADSPResult voice_slow_talk_process(slow_talk_struct_t *slow_talk_struct_ptr,
			/** This i/o parameter is pointer to slow talk structure **/
			int16_t *out_ptr,
            /**< This output parameter is a
            pointer to output Buffer.*/
            int16_t *in_ptr,
            /**< This input parameter is a
            pointer to input Buffer.*/
            int16_t out_buff_size
            /* size of output buffer */
            );

/*************************************************************************
FUNCTION: voice_slow_talk_resampler_init
*************************************************************************/
/**
Initializes the resampler as per the new sampling rate requirement

@return
- ADSP_EOK - The initialization was successful.
- error code - There was an error which needs to propagate.
*/
uint32_t voice_slow_talk_resampler_init(slow_talk_struct_t *slow_talk_struct_ptr,
    					  /*in*/ uint32_t sampling_rate
						 );


/*************************************************************************
FUNCTION: voice_slow_talk_set_param
*************************************************************************/
/**
Sets the calibration parameters to slow talk module.

@return
- ADSP_EOK - The initialization was successful.
- error code - There was an error which needs to propagate.
*/
ADSPResult voice_slow_talk_set_param(slow_talk_struct_t *slow_talk_struct_ptr,
    					  /*in*/ uint32_t alg_aspectID,
                         /*in*/ const int8_t* params_buffer_ptr,
                         /*in*/ int32_t params_buffer_len
						 );
/*************************************************************************
FUNCTION: voice_slow_talk_get_param
*************************************************************************/
/**
Gets the calibration parameters from the slow talk module.

@return
- ADSP_EOK - The initialization was successful.
- error code - There was an error which needs to propagate.
*/
ADSPResult voice_slow_talk_get_param(slow_talk_struct_t *slow_talk_struct_ptr,
						 /*in*/ uint32_t alg_aspectID,
                         /*in*/  int8_t* params_buffer_ptr,
                         /*in*/ int32_t params_buffer_len,
                         int32_t* params_buffer_len_req_ptr
						 );

/*************************************************************************
FUNCTION: voice_slow_talk_get_kpps
*************************************************************************/
/**
Gets the KPPS from the slow talk module.

@return
- ADSP_EOK - The operation completed with no errors.
- error code - There was an error which needs to propagate.
*/
ADSPResult voice_slow_talk_get_kpps(slow_talk_struct_t *slow_talk_struct_ptr,
                         uint32_t* kpps_ptr, uint16_t sampling_rate
						 );

#endif /* VOICE_SLOWTALK_H */

uint32_t st_get_size(void);
ADSPResult voice_st_set_mem(slow_talk_struct_t *slow_talk_struct_ptr,int8 *pMemAddr,uint32_t nSize);
