/*============================================================================

                         W I D E V O I C E V2    W R A P P E R
*//** @file CWideVoiceV2.cpp
  This file contains the definitions of the CWideVoice class methods.

@par EXTERNALIZED FUNCTIONS
  (none)

@par INITIALIZATION AND SEQUENCING REQUIREMENTS
  (none)

Copyright (c) 2010 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government, Diversion contrary to U.S. law prohibited.
*//*========================================================================*/

/*============================================================================
                             Edit History

$Header:

when       who     what, where, why
--------   ---     -------------------------------------------------------
06/19/14   rt       Created file.
============================================================================*/


/*===========================================================================
                     INCLUDE FILES FOR MODULE
===========================================================================*/

#include "Voice_WideVoiceV2.h"
#include "adsp_vparams_api.h"
#include "VoiceCmnUtils.h"

//#define VOICE_DBG_WIDE_VOICE

/* Peak kpps taken */
#define VOICE_WVE_V2_KPPS (7305)

/* Algorithmic delay in microseconds */
#define VOICE_WVE_V2_DELAY  (0) //(5000)            // rt : TODO : verify exact delay..should be 0

static void wve_v2_static_var_default(wv_static_config_t  *wve_v2_static_config, uint32_t sampling_rate);




static void wve_v2_static_var_default(wv_static_config_t  *wve_v2_static_config, uint32_t sampling_rate)
{
   wve_v2_static_config->bits_per_sample           = 16;
   wve_v2_static_config->num_in_samples_frame      = (sampling_rate*20)/1000;                    //no. of input samples will depend on sampling rate
   wve_v2_static_config->num_out_samples_frame     = 320;                                        //no. of output samples will be 320 always
   wve_v2_static_config->sampling_rate             = sampling_rate;                              // removed from config param file, so just hardcode
   wve_v2_static_config->feature_mode              = WV_DEFAULT_FEATURE_MODE;
}

ADSPResult voice_wve_v2_init_default(voice_wve_v2_struct_t *wve_v2_struct_ptr)
{
   int32_t lib_result = 0;

   wve_v2_static_var_default(&wve_v2_struct_ptr->wve_v2_static_config, 8000); // default init with 8000 sampling rate in create and reinit

   // initialize the lib.  note : this function will intialize both data structure and config structure of Wide Voice V2
   lib_result = wv_v2_init_memory( &wve_v2_struct_ptr->wve_v2_lib, &wve_v2_struct_ptr->wve_v2_static_config, wve_v2_struct_ptr->wve_v2_lib_ptr, wve_v2_struct_ptr->wve_v2_memory.lib_memory_size);
   if (ADSP_FAILED(lib_result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: VOICE: WVE V2 initialization failed, %d", (int)lib_result);
      return ADSP_EFAILED;
   }
   return ADSP_EOK;
}


ADSPResult voice_wve_v2_init(voice_wve_v2_struct_t *wve_v2_struct_ptr, uint32_t sampling_rate)  // initializes just the resampler and static config
{


      if(sampling_rate != wve_v2_struct_ptr->wve_v2_static_config.sampling_rate)
      {
         int32_t lib_result = 0;
         wve_v2_static_var_default(&wve_v2_struct_ptr->wve_v2_static_config, sampling_rate);    // update the static configuration
         if((8000 == sampling_rate) || (16000 == sampling_rate))
         {
            lib_result = wv_v2_set_param( &wve_v2_struct_ptr->wve_v2_lib, (int8_t *) &wve_v2_struct_ptr->wve_v2_static_config, WV_RESET_PARAM, sizeof(wv_static_config_t));
            if (0 != lib_result)
            {
               MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: wv_v2_reinit failed with result (%d)",lib_result);
               return ADSP_EFAILED;
            }
         }
      }

   return ADSP_EOK;
}


ADSPResult voice_wve_v2_process (voice_wve_v2_struct_t *wve_v2_struct_ptr,
      int16_t *out_ptr, int16_t *in_ptr, int32_t in_size, int16_t out_buff_size, int8_t wv_v2_enable)
{
   ADSPResult lib_result = ADSP_EOK;
   int32_t cross_fade_mode = 0;

   if(wv_v2_enable)     // TODO : if cross fading is needed then this check needs to be removed and below set-param will be sent for 8k sampling rate only
   {
      // disable cross fading as Wide Voice V2 enable/disable will happen only in stop state
      wv_v2_set_param(&wve_v2_struct_ptr->wve_v2_lib,(int8_t *)(&cross_fade_mode),WV_XFADE_MODE_PARAM,sizeof(int32_t));


      int32_t num_out_samples_filled = 0;
      int8_t *wv_mult_ch_input[1];
      int8_t *wv_mult_ch_output[1];

      wv_mult_ch_input[0] = (int8_t *)in_ptr;
      wv_mult_ch_output[0] = (int8_t *)out_ptr;

      // call the Wide Voice V2 process unconditionally, library will take care of mode enable check and crossfading
      lib_result = wv_v2_process(&wve_v2_struct_ptr->wve_v2_lib,
                    wv_mult_ch_input,
                    wve_v2_struct_ptr->wve_v2_static_config.num_in_samples_frame,
                    wv_mult_ch_output,
                    out_buff_size,
                    &num_out_samples_filled);

      if (ADSP_FAILED(lib_result))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: VOICE: WVE V2 process failed with result, %d", (int)lib_result);
         return ADSP_EFAILED;
      }

      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: WVE V2 Processing Done !!");
   }
   else if (in_ptr != out_ptr)
   {
      memscpy(out_ptr, out_buff_size, in_ptr, in_size);
   }

   return ADSP_EOK;
}


ADSPResult voice_wve_v2_set_param (voice_wve_v2_struct_t *wve_v2_struct_ptr,uint32_t param_id,
      char_t* param_buffer_ptr, int32_t param_buf_len)
{

   if(NULL == param_buffer_ptr)
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: WVE V2 set_param:: NULL param_buffer_ptr error");
      return ADSP_EFAILED;
   }

   switch(param_id)
   {
      case VOICE_PARAM_MOD_ENABLE:
         {
            int32_t lib_result = 0;

            if(0 == wve_v2_struct_ptr->wve_v2_memory.lib_memory_size)
            {
               MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: voice_wve_v2_set_param():: wv v2 lib size 0");
               return ADSP_EUNSUPPORTED; // wv v2 is stubbed
            }

            if (sizeof(int32_t) != param_buf_len)
            {
               MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: voice_wve_v2_set_param():: Bad Param Size");
               return ADSP_EBADPARAM;
            }
            int16_t mod_enable = *((int16_t*)param_buffer_ptr);
            int32_t wv_v2_enable = (int32_t)mod_enable;

            // call Wide Voice V2 library set param   // library will take care of param_buf_len check and NULL wve_v2_lib pointer, no need to check here
            lib_result = wv_v2_set_param(&wve_v2_struct_ptr->wve_v2_lib,(int8_t *)(&wv_v2_enable),WV_MODE_PARAM,param_buf_len);
            if (0 != lib_result)
            {
               MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: wv_v2_set_param : Failed with result (%d)",lib_result);
               return ADSP_EFAILED;
            }

            break;
         }
      case VOICE_PARAM_WV_V2:
         {
            int32_t lib_result = 0;
            // call Wide Voice V2 library set param   // library will take care of NULL wve_v2_lib pointer, no need to check here
            lib_result = wv_v2_set_param(&wve_v2_struct_ptr->wve_v2_lib,(int8_t *)param_buffer_ptr,WV_CALIB_PARAM,param_buf_len);
            if (0 != lib_result)
            {
               MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: wv_v2_set_param : failed with result(%d)",lib_result);
               return ADSP_EFAILED;
            }

            break;
         }
      default:
         {
            MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: voice_wve_v2_set_param:Bad Parameter ID");
            return ADSP_EBADPARAM;
         }
   }
   return ADSP_EOK;
}


ADSPResult voice_wve_v2_get_param (voice_wve_v2_struct_t *wve_v2_struct_ptr,uint32_t param_id,
      char_t* param_buffer_ptr, int32_t param_buf_len, int32_t* params_buffer_len_req_ptr)
{

   if((NULL == param_buffer_ptr) || (NULL == params_buffer_len_req_ptr))
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: WVE V2 get_param:: NULL param_buffer_ptr error");
      return ADSP_EFAILED;
   }

   switch(param_id)
   {
      case VOICE_PARAM_MOD_ENABLE:
         {
            int32_t lib_result = 0;
            *((int32_t*)param_buffer_ptr) = 0;       // Clearing the whole buffer

            // call Wide Voice V2 library get param   // library will take care of NULL wve_v2_lib pointer, no need to check here
            lib_result = wv_v2_get_param(&wve_v2_struct_ptr->wve_v2_lib,(int8_t *)param_buffer_ptr,WV_MODE_PARAM,param_buf_len,params_buffer_len_req_ptr);
            if (0 != lib_result)
            {
               MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: wv_v2_get_param : failed with result(%d)",lib_result);
               return ADSP_EFAILED;
            }
            break;
         }
      case VOICE_PARAM_WV_V2:
         {

            int32_t lib_result = 0;
            *((int32_t*)param_buffer_ptr) = 0;       // Clearing the whole buffer

            // call Wide Voice V2 library get param   // library will take care of NULL wve_v2_lib pointer checks, no need to check here
            lib_result = wv_v2_get_param(&wve_v2_struct_ptr->wve_v2_lib,(int8_t *)param_buffer_ptr,WV_CALIB_PARAM,param_buf_len,params_buffer_len_req_ptr);

            if (0 != lib_result)
            {
               MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: wv_v2_get_param : failed with result(%d)",lib_result);
               return ADSP_EFAILED;
            }

            break;
         }
      default:
         {
            MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: voice_wve_get_param:Bad Parameter ID");
            return ADSP_EBADPARAM;
         }
   }
   return ADSP_EOK;
}


ADSPResult voice_wve_v2_end (voice_wve_v2_struct_t *wve_v2_struct_ptr)
{
   wve_v2_struct_ptr->wve_v2_lib_ptr = NULL;
   return ADSP_EOK;
}

ADSPResult voice_wve_v2_set_mem(voice_wve_v2_struct_t *wve_v2_struct_ptr,int8_t *pMemAddr,uint32_t nSize)
{
   uint32_t wveSize = wve_v2_struct_ptr->wve_v2_memory.lib_memory_size;
   if (nSize < wveSize)
   {
       MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP:  WVE V2 memory size insufficient - allocated (%ld) required (%ld)", nSize,wveSize);
       return ADSP_EFAILED;
   }

   wve_v2_struct_ptr->wve_v2_lib_ptr = (void *) pMemAddr;
   return ADSP_EOK;

}

ADSPResult voice_wve_v2_get_mem_req(voice_wve_v2_struct_t *wve_v2_struct_ptr, uint32_t sampling_rate, uint32_t *wve_v2_size)
{
   int32_t lib_result;
   wve_v2_static_var_default(&wve_v2_struct_ptr->wve_v2_static_config, sampling_rate);  // intialize the static variables like : sampling rate, number of input/output samples to Wide Voice V2

   *wve_v2_size =0;               // clear the size buffer
   // get stack and heap requirements
   lib_result = wv_v2_get_mem_req(&wve_v2_struct_ptr->wve_v2_memory, &wve_v2_struct_ptr->wve_v2_static_config);
   if (lib_result != 0)
   {
       MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP:  WVE V2 : static configuration is not supported : result(%d)",lib_result);
       return ADSP_EFAILED;
   }
   *wve_v2_size = (uint32_t)wve_v2_struct_ptr->wve_v2_memory.lib_memory_size;
   //MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP:  WVE V2 : heap memory required : (%lu)",*wve_v2_size);

   return ADSP_EOK;

}

ADSPResult voice_wve_v2_get_kpps(voice_wve_v2_struct_t* wve_v2_struct_ptr, uint32_t* kpps_ptr, int8_t wv_v2_enable)
{
   ADSPResult result = ADSP_EOK;

   if ((NULL!= wve_v2_struct_ptr) && (NULL!= kpps_ptr))
   {
      *kpps_ptr = 0;
      if(TRUE == wv_v2_enable)
      {
         // Not using sampling rate tables here, as Widevoice enablement
         // is a sufficient check for KPPS
         *kpps_ptr = VOICE_WVE_V2_KPPS;
      }
   }
   else
   {
      result = ADSP_EBADPARAM;
   }
   return result;
}

ADSPResult voice_wve_v2_get_delay(voice_wve_v2_struct_t* wve_v2_struct_ptr, uint32_t* delay_ptr, int8_t wv_v2_enable)
{
   ADSPResult result = ADSP_EOK;

   if ((NULL!= wve_v2_struct_ptr) && (NULL!= delay_ptr))
   {
      *delay_ptr = 0;
      if(TRUE == wv_v2_enable)
      {
         *delay_ptr = VOICE_WVE_V2_DELAY;
      }
   }
   else
   {
      result = ADSP_EBADPARAM;
   }
   return result;
}



