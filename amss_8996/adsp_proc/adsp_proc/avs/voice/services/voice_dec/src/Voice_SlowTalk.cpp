
/*============================================================================
                             Edit History

$Header:

when       who     what, where, why
--------   ---     -------------------------------------------------------
05/13/10   sb       Added changes for C++ to C conversion
11/22/09   ss       Removed redundant in-place processing.
10/12/09   sj       Created file.

============================================================================*/


/*===========================================================================
                     INCLUDE FILES FOR MODULE
===========================================================================*/

#include "Voice_SlowTalk.h"
#include "adsp_vparams_api.h"
#include "VoiceCmnUtils.h"
// #define ST_DEBUG_PRINT_PARAMS


#define ST_WB_FRAME_SIZE 320

#define VOICE_ST_NB_KPPS (3575)  // added resampler kpps inaddition to wb kpps
#define VOICE_ST_WB_KPPS (2875)
#define VOICE_ST_SWB_KPPS (VOICE_ST_WB_KPPS + VOICE_RESAMPLER_KPPS_32K_TO_16K + VOICE_RESAMPLER_KPPS_16K_TO_32K)
#define VOICE_ST_FB_KPPS (5275) // added resampler kpps inaddition to wb kpps
static const vcmn_sampling_kpps_t VOICE_ST_SAMPLING_KPPS_TABLE[] = {{VOICE_NB_SAMPLING_RATE, VOICE_ST_NB_KPPS },
                                                                      {VOICE_WB_SAMPLING_RATE, VOICE_ST_WB_KPPS },
                                                                      {VOICE_SWB_SAMPLING_RATE, VOICE_ST_SWB_KPPS },
                                                                      {VOICE_FB_SAMPLING_RATE, VOICE_ST_FB_KPPS },
                                                                      {VOICE_INVALID_SAMPLING_RATE, 0}};


uint32_t voice_slow_talk_resampler_init(slow_talk_struct_t *slow_talk_struct_ptr,
                                   /*in*/ uint32_t sampling_rate )
{
   uint32_t channel_mem_size = 0;
   if(sampling_rate != slow_talk_struct_ptr->sampling_rate)
   {
      slow_talk_struct_ptr->sampling_rate = sampling_rate;
      slow_talk_struct_ptr->sampling_rate_factor = slow_talk_struct_ptr->sampling_rate/VOICE_NB_SAMPLING_RATE;
      //configure the input resampler, for input resampler the target resampling frequency is always 16KHz
      slow_talk_struct_ptr->inp_resamp_inp_block_size = FRAME_SIZE_ST_NB * slow_talk_struct_ptr->sampling_rate_factor;
      int32_t confg_result = voice_resampler_set_config( &(slow_talk_struct_ptr->inp_resamp_cfg_struct),
                                                         slow_talk_struct_ptr->sampling_rate,
                                                         VOICE_WB_SAMPLING_RATE,
                                                         16,
                                                         slow_talk_struct_ptr->inp_resamp_inp_block_size,
                                                         &slow_talk_struct_ptr->inp_resamp_out_block_size );
      if(VOICE_RESAMPLE_SUCCESS != confg_result)
      {
         MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Error in resampler config setup in ST");
         return ADSP_EBADPARAM;
      }

      voice_resampler_get_channel_mem( &(slow_talk_struct_ptr->inp_resamp_cfg_struct), &channel_mem_size );
      confg_result = voice_resampler_channel_init( &(slow_talk_struct_ptr->inp_resamp_cfg_struct),
                                    slow_talk_struct_ptr->inp_resamp_channel_mem_ptr,
                                    channel_mem_size );
      if(VOICE_RESAMPLE_SUCCESS != confg_result)
      {
         MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Error in resampler init setup in ST");
         return ADSP_EBADPARAM;
      }

     //configure the output resampler,for output resampler the input sampling frequency is always 16KHz
      slow_talk_struct_ptr->out_resamp_inp_block_size = FRAME_SIZE_ST_WB;

      confg_result = voice_resampler_set_config( &(slow_talk_struct_ptr->out_resamp_cfg_struct),
                                                 VOICE_WB_SAMPLING_RATE,
                                                 slow_talk_struct_ptr->sampling_rate,
                                                 16,
                                                 slow_talk_struct_ptr->out_resamp_inp_block_size,
                                                 &slow_talk_struct_ptr->out_resamp_out_block_size );
      if(VOICE_RESAMPLE_SUCCESS != confg_result)
      {
         MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Error in resampler config setup in ST");
         return ADSP_EBADPARAM;
      }

      voice_resampler_get_channel_mem( &(slow_talk_struct_ptr->out_resamp_cfg_struct), &channel_mem_size );

      confg_result = voice_resampler_channel_init( &(slow_talk_struct_ptr->out_resamp_cfg_struct),
                                    slow_talk_struct_ptr->out_resamp_channel_mem_ptr,
                                    channel_mem_size );
      if(VOICE_RESAMPLE_SUCCESS != confg_result)
      {
         MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Error in resampler config setup in ST");
         return ADSP_EBADPARAM;
      }
   }
   return ADSP_EOK;
}


static int32_t slowtalk_init (slow_talk_struct_t *slow_talk_struct_ptr)
{

   //initialize the slowtalk and vad parameters for easyhearing_init
   if(FALSE == slow_talk_struct_ptr->param_modified && FALSE == slow_talk_struct_ptr->param_modified_prev)
   {
      easyhearing_default_init(slow_talk_struct_ptr->st_param, slow_talk_struct_ptr->vad_param);

      if(slow_talk_struct_ptr->wide_band)
      {
         slow_talk_struct_ptr->st_param[2] = 2 ; //not coming to ACDB
      }else
      {
         slow_talk_struct_ptr->st_param[2] = 1 ; //not coming to ACDB
      }

   }
   slow_talk_struct_ptr->nCir_buf_clear = 1;

   easyhearing_init(slow_talk_struct_ptr->st_param, slow_talk_struct_ptr->vad_param, slow_talk_struct_ptr->st_data_ptr, slow_talk_struct_ptr->exp_struct_ptr);

   MSG(MSG_SSID_QDSP6, DBG_LOW_PRIO,"VCP: SlowTalk ProcessInit End");
   return ADSP_EOK;
}
ADSPResult voice_slow_talk_init(slow_talk_struct_t *slow_talk_struct_ptr)
{
   ADSPResult result = ADSP_EOK;
   uint32_t channel_mem_size = 0;

   slow_talk_struct_ptr->enable_slow_talk = FALSE;
   slow_talk_struct_ptr->wide_band = TRUE;
   slow_talk_struct_ptr->param_modified_prev = FALSE;
   slow_talk_struct_ptr->param_modified = FALSE;
   slow_talk_struct_ptr->param_modified_enable = FALSE;
   slow_talk_struct_ptr->nCir_buf_clear = 1;
   slow_talk_struct_ptr->sampling_rate = VOICE_FB_SAMPLING_RATE;
   slow_talk_struct_ptr->sampling_rate_factor= slow_talk_struct_ptr->sampling_rate/VOICE_NB_SAMPLING_RATE;
   slow_talk_struct_ptr->inp_resamp_inp_block_size = FRAME_SIZE_ST_FB;
   slow_talk_struct_ptr->inp_resamp_out_block_size = FRAME_SIZE_ST_WB;
   slow_talk_struct_ptr->out_resamp_inp_block_size = FRAME_SIZE_ST_WB;
   slow_talk_struct_ptr->out_resamp_out_block_size = FRAME_SIZE_ST_FB;
   /**** To test enable slow talk by default ***/
   slow_talk_struct_ptr->n_slowtalk = NO_SLOWTALK ;

   /** This function returns ADSP_ENOMEMORY if memory allocation failed in init function **/
   result = slowtalk_init(slow_talk_struct_ptr);

   // configuring input resampler

   int32_t confg_result = voice_resampler_set_config(
         &(slow_talk_struct_ptr->inp_resamp_cfg_struct),
         VOICE_FB_SAMPLING_RATE,  //setting these defaults to achive max mem allocation
         VOICE_WB_SAMPLING_RATE,  //setting these defaults to achive max mem allocation
         16,
         slow_talk_struct_ptr->inp_resamp_inp_block_size,
         &slow_talk_struct_ptr->inp_resamp_out_block_size
         );

   if(VOICE_RESAMPLE_SUCCESS != confg_result)
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Error in resampler config setup in ST");
      return ADSP_EBADPARAM;
   }

   //Allocate memory for input resampler

   voice_resampler_get_channel_mem( &(slow_talk_struct_ptr->inp_resamp_cfg_struct), &channel_mem_size );

   int8_t *mem_ptr = (int8_t *)qurt_elite_memory_malloc(channel_mem_size,QURT_ELITE_HEAP_DEFAULT);

   if (NULL == mem_ptr)
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Memory allocation failed for resampler memory in ST");
      return ADSP_ENOMEMORY;
   }

   slow_talk_struct_ptr->inp_resamp_channel_mem_ptr = mem_ptr;

   // configuring output resampler

   confg_result = voice_resampler_set_config(
         &(slow_talk_struct_ptr->out_resamp_cfg_struct),
         VOICE_WB_SAMPLING_RATE,  //setting these defaults to achive max mem allocation
         VOICE_FB_SAMPLING_RATE,  //setting these defaults to achive max mem allocation
         16,
         slow_talk_struct_ptr->out_resamp_inp_block_size,
         &slow_talk_struct_ptr->out_resamp_out_block_size
         );

   if(VOICE_RESAMPLE_SUCCESS != confg_result)
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Error in resampler config setup in ST");
      return ADSP_EBADPARAM;
   }


   //Allocate memory for output resampler

   mem_ptr = NULL;
   channel_mem_size = 0;

   voice_resampler_get_channel_mem( (&slow_talk_struct_ptr->out_resamp_cfg_struct), &channel_mem_size );


   mem_ptr = (int8_t *)qurt_elite_memory_malloc(channel_mem_size,QURT_ELITE_HEAP_DEFAULT);

   if (NULL == mem_ptr)
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Memory allocation failed for resampler memory in ST");
      return ADSP_ENOMEMORY;
   }

   slow_talk_struct_ptr->out_resamp_channel_mem_ptr = mem_ptr;

   // Initialize input resampler memory
   confg_result =  voice_resampler_channel_init(
         (&slow_talk_struct_ptr->inp_resamp_cfg_struct),
         slow_talk_struct_ptr->inp_resamp_channel_mem_ptr,
         channel_mem_size );
   if(VOICE_RESAMPLE_SUCCESS != confg_result)
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Error in resampler config setup in ST");
      return ADSP_EBADPARAM;
   }

   //Initialize output resampler memory
   confg_result =  voice_resampler_channel_init(
         &(slow_talk_struct_ptr->out_resamp_cfg_struct),
         slow_talk_struct_ptr->out_resamp_channel_mem_ptr,
         channel_mem_size );
   if(VOICE_RESAMPLE_SUCCESS != confg_result)
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Error in resampler config setup in ST");
      return ADSP_EBADPARAM;
   }
   return result;
}

ADSPResult voice_slow_talk_end (slow_talk_struct_t *slow_talk_struct_ptr)
{
   ADSPResult result = ADSP_EOK;
   qurt_elite_memory_free(slow_talk_struct_ptr->inp_resamp_channel_mem_ptr);
   qurt_elite_memory_free(slow_talk_struct_ptr->out_resamp_channel_mem_ptr);
   slow_talk_struct_ptr->inp_resamp_channel_mem_ptr = NULL;
   slow_talk_struct_ptr->out_resamp_channel_mem_ptr = NULL;
   MSG(MSG_SSID_QDSP6, DBG_LOW_PRIO,"VCP: **** SlowTalk Module Destroy Success");
   return result;
}

ADSPResult voice_slow_talk_process (slow_talk_struct_t *slow_talk_struct_ptr,
      int16_t *out_ptr,
      int16_t* in_ptr,
      int16_t out_buff_size)
{
   int8_t inp_buf[FRAME_SIZE_ST_FB*2];
   uint32_t channel_mem_size = 0;

   if (NO_SLOWTALK != slow_talk_struct_ptr->n_slowtalk)
   {

      memscpy(inp_buf, sizeof(inp_buf) ,in_ptr,(2*FRAME_SIZE_ST_NB * slow_talk_struct_ptr->sampling_rate_factor));

         if(slow_talk_struct_ptr->param_modified_enable)
         {

           voice_resampler_get_channel_mem( &(slow_talk_struct_ptr->inp_resamp_cfg_struct), &channel_mem_size );
           // Initialize input resampler memory
           int32_t  confg_result =  voice_resampler_channel_init(
              (&slow_talk_struct_ptr->inp_resamp_cfg_struct),
               slow_talk_struct_ptr->inp_resamp_channel_mem_ptr,
               channel_mem_size );
           if(VOICE_RESAMPLE_SUCCESS != confg_result)
            {
               MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Error in resampler config setup in ST");
               return ADSP_EBADPARAM;
            }

            voice_resampler_get_channel_mem( (&slow_talk_struct_ptr->out_resamp_cfg_struct), &channel_mem_size );
           //Initialize output resampler memory
            confg_result =  voice_resampler_channel_init(
                &(slow_talk_struct_ptr->out_resamp_cfg_struct),
                slow_talk_struct_ptr->out_resamp_channel_mem_ptr,
                channel_mem_size );
            if(VOICE_RESAMPLE_SUCCESS != confg_result)
            {
               MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: Error in resampler config setup in ST");
               return ADSP_EBADPARAM;
            }
            slow_talk_struct_ptr->param_modified_enable = FALSE;
         }

      if (VOICE_WB_SAMPLING_RATE != slow_talk_struct_ptr->sampling_rate)
      {
         voice_resampler_process( &(slow_talk_struct_ptr->inp_resamp_cfg_struct),
                                  slow_talk_struct_ptr->inp_resamp_channel_mem_ptr,
                                  inp_buf,
                                  slow_talk_struct_ptr->inp_resamp_inp_block_size,
                                  inp_buf,
                                  ST_WB_FRAME_SIZE);
      }
   if(slow_talk_struct_ptr->param_modified)
   {
      easyhearing_init(slow_talk_struct_ptr->st_param, slow_talk_struct_ptr->vad_param, slow_talk_struct_ptr->st_data_ptr, slow_talk_struct_ptr->exp_struct_ptr);
      slow_talk_struct_ptr->param_modified = FALSE;
      slow_talk_struct_ptr->param_modified_prev = TRUE;
   }
      if(slow_talk_struct_ptr->n_slowtalk == ENABLED)
      {
#if defined(VOICE_DBG_MSG)
         MSG(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VOICE: Rx SlowTalk processing");
#endif
         slow_talk_struct_ptr->nCir_buf_clear =
         easyhearing_enabled (slow_talk_struct_ptr->st_data_ptr,(int16_t *)inp_buf, (int16_t *)inp_buf, slow_talk_struct_ptr->exp_struct_ptr, sizeof(inp_buf));
      }
      else if((slow_talk_struct_ptr->nCir_buf_clear == 0)&&(slow_talk_struct_ptr->n_slowtalk == DISABLED))
      {
         slow_talk_struct_ptr->nCir_buf_clear =
         easyhearing_disabled (slow_talk_struct_ptr->st_data_ptr, (int16_t *)inp_buf, (int16_t *)inp_buf, sizeof(inp_buf));

         //Only if the circular buffer is clean we change the ST state to
         //NO_SLOWTALK
         if (slow_talk_struct_ptr->nCir_buf_clear == 1)
         {
            //easyhearing_close(slow_talk_struct_ptr->st_data_ptr); //part of warning fix as this func is empty
            slow_talk_struct_ptr->n_slowtalk = NO_SLOWTALK;
         }
      }
      if (VOICE_WB_SAMPLING_RATE != slow_talk_struct_ptr->sampling_rate)
         {
         voice_resampler_process( &(slow_talk_struct_ptr->out_resamp_cfg_struct),
                                  slow_talk_struct_ptr->out_resamp_channel_mem_ptr,
                                  inp_buf,
                                  ST_WB_FRAME_SIZE,
                                  inp_buf,
                                  slow_talk_struct_ptr->out_resamp_out_block_size);
         }

      memscpy(out_ptr, out_buff_size, inp_buf,(2*FRAME_SIZE_ST_NB * slow_talk_struct_ptr->sampling_rate_factor));
      }
   else
   {
      memscpy(out_ptr, out_buff_size, in_ptr,(2*FRAME_SIZE_ST_NB * slow_talk_struct_ptr->sampling_rate_factor));
   }
   return ADSP_EOK;
}


ADSPResult voice_slow_talk_set_param(slow_talk_struct_t *slow_talk_struct_ptr,
      /*in*/ uint32_t alg_aspectID,
      /*in*/ const int8_t* params_buffer_ptr,
      /*in*/ int32_t params_buffer_len)
{
   MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: set_param on slow_talk param_id = %x, param_size = %d",(int)alg_aspectID, (int)params_buffer_len);
   switch(alg_aspectID)
   {
      case VOICE_PARAM_MOD_ENABLE:
         {
             slow_talk_struct_ptr->enable_slow_talk = *params_buffer_ptr;
             if(slow_talk_struct_ptr->enable_slow_talk == 1)
             {
                if (slow_talk_struct_ptr->n_slowtalk == NO_SLOWTALK)
                {
                   slow_talk_struct_ptr->param_modified_enable=TRUE;
                   slow_talk_struct_ptr->param_modified = TRUE;
                }
                slow_talk_struct_ptr->n_slowtalk = ENABLED ; //ENABLED
              }
            else
            {
               if (slow_talk_struct_ptr->n_slowtalk == ENABLED)
               {
                  slow_talk_struct_ptr->n_slowtalk = DISABLED; //DISABLED;
                  MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: SlowTalk disabled\n");
               }
            }
            break;
         }
      case VOICE_PARAM_ST:
         {


            if(params_buffer_len != sizeof(slow_talk_param))
            {
               MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: slow_talk_param  required size = %d, given size = %ld",sizeof(slow_talk_param), params_buffer_len);
               return ADSP_EFAILED;
            }
            //typecast the buffer you got with structure of type ISOD definition
            slow_talk_param *st_int_param_struct = (slow_talk_param*) params_buffer_ptr;

            slow_talk_struct_ptr->vad_param[0] = st_int_param_struct->AVAD_THRESH;
            slow_talk_struct_ptr->vad_param[1] = st_int_param_struct->AVAD_PWR_SCALE;
            slow_talk_struct_ptr->vad_param[2] = st_int_param_struct->AVAD_HANGOVER;
            slow_talk_struct_ptr->vad_param[3] = st_int_param_struct->AVAD_ALPHA;
            slow_talk_struct_ptr->vad_param[4] = st_int_param_struct->AVAD_SD_MAX;
            slow_talk_struct_ptr->vad_param[5] = st_int_param_struct->AVAD_SD_MIN;
            slow_talk_struct_ptr->vad_param[6] = st_int_param_struct->AVAD_INIT_LENGTH;
            slow_talk_struct_ptr->vad_param[7] = st_int_param_struct->AVAD_MAX_VAL;


            slow_talk_struct_ptr->vad_param[8] = st_int_param_struct->AVAD_INIT_BOUND;
            slow_talk_struct_ptr->vad_param[9] = st_int_param_struct->AVAD_SUB_NC;


            slow_talk_struct_ptr->vad_param[10] = st_int_param_struct->AVAD_VAR;
            slow_talk_struct_ptr->vad_param[11] = st_int_param_struct->AVAD_SPOW_MIN;
            slow_talk_struct_ptr->vad_param[12] = st_int_param_struct->AVAD_CFAC;

#ifdef ST_DEBUG_PRINT_PARAMS
            for(i=0; i<13; i++)
            {
               MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: set vad_param [%d]=0x%04x",i,slow_talk_struct_ptr->vad_param[i]);
            }
#endif // ST_DEBUG_PRINT_PARAMS

            //ST expansion params
            if(slow_talk_struct_ptr->wide_band)
            {
               slow_talk_struct_ptr->st_param[0] = st_int_param_struct->MAX_DELAY*16000;//converting seconds to samples
            }else
            {
               slow_talk_struct_ptr->st_param[0] = st_int_param_struct->MAX_DELAY*8000;//converting seconds to samples
            }

            //moved to a different param ID
            //slow_talk_struct_ptr->st_param[1] = st_int_param_struct->EXPANSION_RATIO;


            if(slow_talk_struct_ptr->wide_band)
            {
               slow_talk_struct_ptr->st_param[2] = 2 ; //not coming to ACDB

            }else
            {
               slow_talk_struct_ptr->st_param[2] = 1 ; //not coming to ACDB
            }

            slow_talk_struct_ptr->st_param[3] = slow_talk_struct_ptr->nCir_buf_clear ; //not coming from ACDB

            slow_talk_struct_ptr->st_param[4] = st_int_param_struct->MAX_EXP_DIFF;
            slow_talk_struct_ptr->st_param[5] = st_int_param_struct->MIN_PAUSE;
            slow_talk_struct_ptr->st_param[6] = st_int_param_struct->LOOK_AHEAD;
            slow_talk_struct_ptr->st_param[7] = st_int_param_struct->VOICE_QUAL_THRESH1;
            slow_talk_struct_ptr->st_param[8] = st_int_param_struct->VOICE_QUAL_THRESH2;
            slow_talk_struct_ptr->st_param[9] = st_int_param_struct->VOICE_QUAL_THRESH_FORCE;

#ifdef ST_DEBUG_PRINT_PARAMS
            for(i=0; i<10; i=i++)
            {
               MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: set st_param [%d]=0x%04x",i,slow_talk_struct_ptr->st_param[i]);
            }
#endif // ST_DEBUG_PRINT_PARAMS

            slow_talk_struct_ptr->param_modified = TRUE;

            break;
         }

      case VOICE_PARAM_ST_EXP:
         {
            if(params_buffer_len != sizeof(uint32_t))
            {
               MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: slow_talk_param  required size = %d, given size = %ld",sizeof(uint32_t), params_buffer_len);
               return ADSP_EFAILED;
            }

            //typecast the buffer you got with structure of type ISOD definition
            int16_t *st_int_param_struct = (int16_t*) params_buffer_ptr;

            slow_talk_struct_ptr->st_param[1] = *st_int_param_struct;
            MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: SlowTalk exp factor = st_param[1] = 0x%04lx\n",slow_talk_struct_ptr->st_param[1]);
            slow_talk_struct_ptr->param_modified = TRUE;
            break;

         }
      default:
         return ADSP_EFAILED;
   }

   return ADSP_EOK;
}


ADSPResult voice_slow_talk_get_param(slow_talk_struct_t *slow_talk_struct_ptr,
      /*in*/ uint32_t alg_aspectID,
      /*in*/  int8_t* params_buffer_ptr,
      /*in*/ int32_t params_buffer_len,
      int32_t* params_buffer_len_req_ptr)
{
   MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: get_param on slow_talk param_id=%x,param_size=%d",(int)alg_aspectID,(int)params_buffer_len);
   switch(alg_aspectID)
   {

      case VOICE_PARAM_MOD_ENABLE:
         {
            *params_buffer_len_req_ptr = sizeof(uint32_t); //2 bytes. Enable is a uint32_t in ISOD
            if(*params_buffer_len_req_ptr > params_buffer_len )
            {
               MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: voice_slow_talk_get_param required size = %ld, given size = %ld",*params_buffer_len_req_ptr, params_buffer_len);
               return ADSP_ENOMEMORY;
            }
            *((int32_t*)params_buffer_ptr) = 0;  // Clearing the whole buffer

            *((int16_t*)params_buffer_ptr) = slow_talk_struct_ptr->enable_slow_talk;

            break;
         }
      case VOICE_PARAM_ST:
         {
            //TODO: check if there will be any mem alignment issues with following
            //approach

            *params_buffer_len_req_ptr =  sizeof(slow_talk_param);

            if(*params_buffer_len_req_ptr > params_buffer_len )
            {
               MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: voice_slow_talk_get_param required size = %ld, given size = %ld",*params_buffer_len_req_ptr, params_buffer_len);
               return ADSP_ENOMEMORY;
            }

            //typecast the buffer you got with structure of type ISOD definition
            slow_talk_param *st_int_param_struct = (slow_talk_param*) params_buffer_ptr;

            st_int_param_struct->AVAD_THRESH    = slow_talk_struct_ptr->vad_param[0];
            st_int_param_struct->AVAD_PWR_SCALE = slow_talk_struct_ptr->vad_param[1];
            st_int_param_struct->AVAD_HANGOVER  = slow_talk_struct_ptr->vad_param[2];
            st_int_param_struct->AVAD_ALPHA     = slow_talk_struct_ptr->vad_param[3];
            st_int_param_struct->AVAD_SD_MAX    = slow_talk_struct_ptr->vad_param[4];
            st_int_param_struct->AVAD_SD_MIN    = slow_talk_struct_ptr->vad_param[5];
            st_int_param_struct->AVAD_INIT_LENGTH = slow_talk_struct_ptr->vad_param[6];
            st_int_param_struct->AVAD_MAX_VAL     = slow_talk_struct_ptr->vad_param[7];
            st_int_param_struct->nReserved1       = 0;

            st_int_param_struct->AVAD_INIT_BOUND  = slow_talk_struct_ptr->vad_param[8];
            st_int_param_struct->AVAD_SUB_NC      = slow_talk_struct_ptr->vad_param[9];
            st_int_param_struct->nReserved2       = 0;

            st_int_param_struct->AVAD_VAR         = slow_talk_struct_ptr->vad_param[10];
            st_int_param_struct->AVAD_SPOW_MIN    = slow_talk_struct_ptr->vad_param[11];
            st_int_param_struct->AVAD_CFAC        = slow_talk_struct_ptr->vad_param[12];

            //ST expansion params
            if(slow_talk_struct_ptr->wide_band)
            {
               st_int_param_struct->MAX_DELAY     = slow_talk_struct_ptr->st_param[0]/16000;//converting samples to sec
            }else
            {
               st_int_param_struct->MAX_DELAY     = slow_talk_struct_ptr->st_param[0]/8000;//converting samples to sec
            }

            //moved to a different param IID
            st_int_param_struct->nReserved3 = 0;

            st_int_param_struct->MAX_EXP_DIFF = slow_talk_struct_ptr->st_param[4];
            st_int_param_struct->MIN_PAUSE    = slow_talk_struct_ptr->st_param[5];
            st_int_param_struct->LOOK_AHEAD   = slow_talk_struct_ptr->st_param[6];
            st_int_param_struct->VOICE_QUAL_THRESH1 = slow_talk_struct_ptr->st_param[7];
            st_int_param_struct->VOICE_QUAL_THRESH2 = slow_talk_struct_ptr->st_param[8];
            st_int_param_struct->VOICE_QUAL_THRESH_FORCE  = slow_talk_struct_ptr->st_param[9];


         }
         break;

      case VOICE_PARAM_ST_EXP:
         {
            *params_buffer_len_req_ptr = sizeof(int32_t);
            if(*params_buffer_len_req_ptr > params_buffer_len )
            {
               MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: voice_slow_talk_get_param required size = %ld, given size = %ld",*params_buffer_len_req_ptr, params_buffer_len);
               return ADSP_ENOMEMORY;
            }

            //typecast the buffer you got with structure of type ISOD definition
            *((int32_t*)params_buffer_ptr) = 0;  // Clearing the whole buffer
            int16_t *st_int_param_struct = (int16_t*) params_buffer_ptr;

            *st_int_param_struct = slow_talk_struct_ptr->st_param[1];
            MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: SlowTalkGetParam - Unsupported IID\n");
            break;

         }
      default:
         return ADSP_EFAILED;
   }

   return ADSP_EOK;
}

uint32_t st_get_size()
{
   unsigned int required_size=0,expStruct_size,stStruct_size;
   expStruct_size=VOICE_ROUNDTO8(easyhearing_get_exapansion_struct_size());
   stStruct_size=VOICE_ROUNDTO8(easyhearing_get_struct_size());
   required_size=expStruct_size+stStruct_size;
   return required_size;
}

ADSPResult voice_st_set_mem(slow_talk_struct_t *slow_talk_struct_ptr,int8 *pMemAddr,uint32_t nSize)
{
   uint32_t stSize=st_get_size();

   if (nSize < stSize)
   {
       MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP:  slowtalk memory size insufficient - allocated (%ld) required (%ld)", nSize,stSize);
       return ADSP_EFAILED;
   }

   uint32_t stStruct_size=VOICE_ROUNDTO8(easyhearing_get_struct_size());

   slow_talk_struct_ptr->st_data_ptr = (void*)pMemAddr;
   slow_talk_struct_ptr->exp_struct_ptr = (void*)(pMemAddr + stStruct_size);
   return ADSP_EOK;

}

ADSPResult voice_slow_talk_get_kpps(slow_talk_struct_t* slow_talk_struct_ptr,uint32_t* kpps_ptr, uint16_t sampling_rate)
{
   ADSPResult result = ADSP_EOK;

   if ((NULL!= slow_talk_struct_ptr) && (NULL!= kpps_ptr))
   {
      *kpps_ptr = 0;
      if(slow_talk_struct_ptr->n_slowtalk == ENABLED)
      {
         result = vcmn_find_kpps_table(VOICE_ST_SAMPLING_KPPS_TABLE, sampling_rate ,kpps_ptr);
      }
   }
   else
   {
      result = ADSP_EBADPARAM;
   }
   return result;
}

