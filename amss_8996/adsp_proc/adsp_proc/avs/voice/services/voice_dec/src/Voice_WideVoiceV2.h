#ifndef CWIDEVOICEV2_H
#define CWIDEVOICEV2_H
/*========================================================================
** @file Voice_WideVoiceV2.h
  This file contains internal declarations common to all parts of the media
  module.  It is meant to be used only by the media module implementation.
  Nothing else should include it.

  This media module does not expose a clock.
*/
/****************************************************************************
Copyright (c) 2010 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government. Diversion contrary to U.S. law prohibited.
****************************************************************************/
/*====================================================================== */

/*========================================================================
                             Edit History

$Header: //components/rel/avs.adsp/2.7.1.c4/voice/services/voice_dec/src/Voice_WideVoiceV2.h#1 $

when       who     what, where, why
--------   ---     -------------------------------------------------------
06/19/14   rt       Created file for WideVoice V2

========================================================================== */

/* =======================================================================

                     INCLUDE FILES FOR MODULE

========================================================================== */


#include "qurt_elite.h"
extern "C" {
#include "wve_v2_api.h"
}


/* =======================================================================

   DATA DECLARATIONS

   ========================================================================== */

typedef struct {
   wv_static_config_t     wve_v2_static_config;
   wv_memory_t            wve_v2_memory;
   void*                  wve_v2_lib_ptr;
   wv_lib_t               wve_v2_lib;
} voice_wve_v2_struct_t;





/* =======================================================================
 **                          Function Declarations
 ** ======================================================================= */

// wrapper function for wide voice V2 process function
// wve_v2_struct_ptr:  This is ptr to Wide Voice V2 state structure.
// out_ptr:  This output parameter is a pointer to output Buffer.
// in_ptr:  This input parameter is a pointer to input Buffer.
// in_size: This inputparameter is the pointer to the size of the Buffer.
// out_buff_size:  size of output buffer
// wv_v2_enable : wv v2 enable flag
ADSPResult voice_wve_v2_process (voice_wve_v2_struct_t *wve_v2_struct_ptr,
      int16_t *out_ptr,
      int16_t *in_ptr,
      int32_t in_size,
      int16_t out_buff_size,
      int8_t wv_v2_enable
      );


// wrapper function for wide voice V2 init function which initializes the wide voice v2 state and intializes the configuration structure with default values
// wve_v2_struct_ptr : This is ptr to Wide Voice V2 state structure
ADSPResult voice_wve_v2_init_default(voice_wve_v2_struct_t *wve_v2_struct_ptr);

// initializes just the resampler and static config
// wve_v2_struct_ptr : This is ptr to Wide Voice V2 state structure
// sampling_rate : sampling rate of input stream to Wide Voice V2
ADSPResult voice_wve_v2_init(voice_wve_v2_struct_t *wve_v2_struct_ptr, uint32_t sampling_rate);

// wrapper function for wide voice V2 set param
// wve_v2_struct_ptr : This is ptr to Wide Voice V2 state structure
// param_id : parameter id for Wide Voice v2 : MODE ENABLE or CONFIG PARAM
// param_buffer_ptr : pointer to payload buffer
// param_buf_len : payload length
ADSPResult voice_wve_v2_set_param (voice_wve_v2_struct_t *wve_v2_struct_ptr,uint32_t param_id,
      char_t* param_buffer_ptr, int32_t param_buf_len);



// wrapper function for wide voice V2 get param
// wve_v2_struct_ptr : This is ptr to Wide Voice V2 state structure
// param_id : parameter id for WVE v2 : MODE ENABLE or CONFIG PARAM
// param_buffer_ptr : pointer to payload buffer
// param_buf_len : max payload length
// params_buffer_len_req_ptr : pointer to buffer length required by module to write the data.
ADSPResult voice_wve_v2_get_param (voice_wve_v2_struct_t *wve_v2_struct_ptr,uint32_t param_id,
      char_t* param_buffer_ptr, int32_t param_buf_len, int32_t* params_buffer_len_req_ptr);


// Wide Voice V2 end function : makes the lib pointer NULL
// wve_v2_struct_ptr : This is ptr to Wide Voice V2 state structure
ADSPResult voice_wve_v2_end (voice_wve_v2_struct_t *wve_v2_struct_ptr);


// Wide Voice V2 set memory function :  assigns the memory to wide Voice V2 lib pointer
// wve_v2_struct_ptr : This is ptr to Wide Voice V2 state structure
// pMemAddr : pointer to memory allocated for module
// nSize : memory size block available for module
ADSPResult voice_wve_v2_set_mem(voice_wve_v2_struct_t *wve_v2_struct_ptr,int8 *pMemAddr,uint32_t nSize);


// wrapper function for wide voice V2 get memory request function which returns the size of memory required for Wide Voice V2 module
// wve_v2_struct_ptr : This is ptr to Wide Voice V2 state structure
// sampling_rate : sampling rate of input PCM data
ADSPResult voice_wve_v2_get_mem_req(voice_wve_v2_struct_t *wve_v2_struct_ptr, uint32_t sampling_rate, uint32_t *wve_v2_size);


// This function returns the worst KPPS value taken by Wide Voice V2 module
// wve_v2_struct_ptr : This is ptr to Wide Voice V2 state structure
// kpps_ptr : pointer to kpps value
// wv_v2_enable : wv v2 enable flag
ADSPResult voice_wve_v2_get_kpps(voice_wve_v2_struct_t* wve_v2_struct_ptr, uint32_t* kpps_ptr, int8_t wv_v2_enable);



// This function returns the algorithmic delay of Wide Voice V2 module
// wve_v2_struct_ptr : This is ptr to Wide Voice V2 state structure
// delay_ptr : pointer to delay value
// wv_v2_enable : wv v2 enable flag
ADSPResult voice_wve_v2_get_delay(voice_wve_v2_struct_t* wve_v2_struct_ptr, uint32_t* delay_ptr, int8_t wv_v2_enable);

#endif /* CWIDEVOICEV2_H */

