
/****************************************************************************
Copyright (c) 2013-2014 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government. Diversion contrary to U.S. law prohibited.
****************************************************************************/
/*====================================================================== */

/*========================================================================
                             Edit History

$Header: //components/rel/avs.adsp/2.7.1.c4/voice/services/voice_dec/src/Voice_OOBTTYRx.cpp#1 $

when          who      what, where, why
--------      ---      -------------------------------------------------------
03/08/2013     rojha      Created
========================================================================== */
#include "Voice_OOBTTYRx.h"
#include "VoiceLoggingUtils.h"
#include "adsp_vsm_api.h"

static void voice_oobtty_rx_free_local_memory(oobtty_rx_struct_t* oobtty_rx_struct_ptr);
static void voice_oobtty_rx_free_resamp_memory(oobtty_rx_struct_t* oobtty_rx_struct_ptr);

extern "C" {
    #include "lte_tty_api.h"
}

#define VOICE_OOBTTYRX_NB_KPPS (150)
#define VOICE_OOBTTYRX_WB_KPPS (650)
#define VOICE_OOBTTYRX_SWB_KPPS (VOICE_OOBTTYRX_NB_KPPS + VOICE_RESAMPLER_KPPS_8K_TO_32K)
#define VOICE_OOBTTYRX_FB_KPPS (1150)
static const vcmn_sampling_kpps_t VOICE_OOBTTYRX_SAMPLING_KPPS_TABLE[] = {{VOICE_NB_SAMPLING_RATE, VOICE_OOBTTYRX_NB_KPPS },
                                                                       {VOICE_WB_SAMPLING_RATE, VOICE_OOBTTYRX_WB_KPPS },
                                                                       {VOICE_SWB_SAMPLING_RATE, VOICE_OOBTTYRX_SWB_KPPS },
                                                                       {VOICE_FB_SAMPLING_RATE, VOICE_OOBTTYRX_FB_KPPS },
                                                                       {VOICE_INVALID_SAMPLING_RATE, 0}};

/*  allocate memory for VoLTE TTY Rx */
ADSPResult voice_oobtty_rx_mem_alloc(oobtty_rx_struct_t* oobtty_rx_struct_ptr)
{
   ADSPResult result = ADSP_EOK;
   oobtty_rx_struct_ptr->lte_tty_fifo_ptr=qurt_elite_memory_malloc(ltetty_get_fifo_struct_size(), QURT_ELITE_HEAP_DEFAULT);
   oobtty_rx_struct_ptr->oobtty_rx_struct_instance_ptr = qurt_elite_memory_malloc(ltetty_rx_get_struct_size(), QURT_ELITE_HEAP_DEFAULT);

   if (NULL == oobtty_rx_struct_ptr->oobtty_rx_struct_instance_ptr
         || NULL == oobtty_rx_struct_ptr->lte_tty_fifo_ptr)
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: VOICE: Rx OOB TTY init failed, out of memory");
      voice_oobtty_rx_free_local_memory(oobtty_rx_struct_ptr);
      return ADSP_ENOMEMORY;
   }
   return result;
}

/* reset fifo for LTE TTY */
void voice_oobtty_reset_fifo(oobtty_rx_struct_t* oobtty_rx_struct_ptr)
{
   init_ltetty_fifo(oobtty_rx_struct_ptr->lte_tty_fifo_ptr,oobtty_rx_struct_ptr->fifoBuffer);
   return;
}
/* initialize VoLTE TTY Rx module */
void voice_oobtty_rx_init(oobtty_rx_struct_t* oobtty_rx_struct_ptr, uint32_t session_id)
{

   oobtty_rx_struct_ptr->rxTTYActive                  = FALSE;
   oobtty_rx_struct_ptr->session_id                   = session_id;

   init_ltetty_rx(oobtty_rx_struct_ptr->oobtty_rx_struct_instance_ptr);

   voice_oobtty_rx_resampler_init(oobtty_rx_struct_ptr);

   return;
}
/* function to push rx character in fifo, it will return 1 if push happens successfully */
int32_t voice_oobtty_push_char(oobtty_rx_struct_t* oobtty_rx_struct_ptr,int16_t* rxChar)
{
    int32_t fifoResult;
    int16_t pushed;
    fifoResult = lte_tty_fifo_push(oobtty_rx_struct_ptr->lte_tty_fifo_ptr, rxChar, 1, &pushed);
    if (!fifoResult)
    {
       MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: FIFO buffer overflow for Rx char (%d) push, session (%lx), will Retry ",(int)*rxChar,oobtty_rx_struct_ptr->session_id );
    }
    else
    {
#if defined(__qdsp6__) && !defined(SIM)
       // log tty character if available
       int8_t *bufptr[4] = { (int8_t *) rxChar, NULL, NULL, NULL };
       voice_log_buffer( bufptr,
                         VOICE_LOG_CHAN_VSM_RX_OOBTTY_CHAR,
                         (((oobtty_rx_struct_ptr->sampling_rate == 16000) << 3) | oobtty_rx_struct_ptr->session_id),
                         qurt_elite_timer_get_time(),
                         VOICE_LOG_DATA_FORMAT_PCM_MONO,
                         (oobtty_rx_struct_ptr->sampling_rate),
                         sizeof(int8_t),
                         NULL);
#endif
    }
    return fifoResult;
}
/* process 20 ms buffer, assume in place */
ADSPResult voice_oobtty_rx_process(oobtty_rx_struct_t* oobtty_rx_struct_ptr, int16_t *in_ptr, int16_t *out_ptr, uint16_t samples)
{
   ADSPResult result = ADSP_EOK;

   if( TRUE == oobtty_rx_struct_ptr->enable )
   {
      int32_t rxActivityFlag;
      int16_t rxChar;
      int16_t charsInFifo;

      MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: VOICE: Rx OOB TTY processing, samples(%d), session(%lx)", samples,oobtty_rx_struct_ptr->session_id);

      samples = (samples*VOICE_NB_SAMPLING_RATE)/oobtty_rx_struct_ptr->sampling_rate;

      assert( samples == 160);
 // do LTE TTY modulation

      rxActivityFlag = lte_tty_rx_processing(in_ptr, out_ptr, samples, &rxChar, &charsInFifo,
                                                     oobtty_rx_struct_ptr->oobtty_rx_struct_instance_ptr,
                                                     oobtty_rx_struct_ptr->lte_tty_fifo_ptr);

      // set Rx TTY detection (this is actually a flag for TTY baudot regeneration which controls muting).
      oobtty_rx_struct_ptr->rxTTYActive = rxActivityFlag;

      if((VOICE_NB_SAMPLING_RATE != oobtty_rx_struct_ptr->sampling_rate) && (NULL != oobtty_rx_struct_ptr->up_samp_mem_ptr))
      {
         if(FALSE ==lte_tty_get_rx_bypass_flag(oobtty_rx_struct_ptr->oobtty_rx_struct_instance_ptr))
         {
            int32_t resample_result;

            samples = (samples*oobtty_rx_struct_ptr->sampling_rate)/VOICE_NB_SAMPLING_RATE;
            resample_result = voice_resampler_process(
                               &(oobtty_rx_struct_ptr->up_samp_config),
                               oobtty_rx_struct_ptr->up_samp_mem_ptr,
                               (int8 *)out_ptr,
                               VOICE_FRAME_SIZE_NB,
                               (int8 *)out_ptr,
                               (uint32)samples
                               );
            if(VOICE_RESAMPLE_SUCCESS !=resample_result)
            {
               MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: Rx OOB TTY up sampler process failed");
               return ADSP_EFAILED;
            }


         }
         // Since in-place operation is assumed here, no need to copy samples through
         // if byPassed is TRUE

      }
   }

   return result;
}
/* get query function for rx tty activity */
uint8_t voice_oobtty_get_rx_detected(oobtty_rx_struct_t* oobtty_rx_struct_ptr)
{
    return oobtty_rx_struct_ptr->rxTTYActive;
}
/* destroy OOB TTY module, free memory */
ADSPResult voice_oobtty_rx_end(oobtty_rx_struct_t* oobtty_rx_struct_ptr)
{
   ADSPResult result = ADSP_EOK;

   voice_oobtty_rx_free_local_memory(oobtty_rx_struct_ptr);
   voice_oobtty_rx_free_resamp_memory(oobtty_rx_struct_ptr);

   return result;
}


/* enable the module and set wb flag */
ADSPResult voice_oobtty_rx_set_config(oobtty_rx_struct_t* oobtty_rx_struct_ptr, uint8_t enable, uint32_t samp_rate)
{
   ADSPResult result = ADSP_EOK;

   oobtty_rx_struct_ptr->enable = enable;
   oobtty_rx_struct_ptr->sampling_rate = samp_rate;
   MSG_2(MSG_SSID_QDSP6, DBG_MED_PRIO,"VCP: OOB TTY Rx setting enable flag(%d) and sampling_rate(%d)",enable, (int)samp_rate);

   return result;
}


ADSPResult voice_oobtty_rx_resampler_init(oobtty_rx_struct_t* oobtty_rx_struct_ptr)
{
   //freeup existing local memory for resampler, if any
   voice_oobtty_rx_free_resamp_memory(oobtty_rx_struct_ptr);
   // Resampler config for OOB TTY Rx
   if(VOICE_NB_SAMPLING_RATE != oobtty_rx_struct_ptr->sampling_rate)
   {
     uint32_t out_frame_samples, sampling_rate;
     sampling_rate=(uint32_t)oobtty_rx_struct_ptr->sampling_rate;
     voice_resampler_set_config(&(oobtty_rx_struct_ptr->up_samp_config),VOICE_NB_SAMPLING_RATE,sampling_rate,NO_OF_BITS_PER_SAMPLE,VOICE_FRAME_SIZE_NB,&out_frame_samples);
     oobtty_rx_struct_ptr->up_samp_mem_ptr = qurt_elite_memory_malloc( (oobtty_rx_struct_ptr->up_samp_config.total_channel_mem_size) , QURT_ELITE_HEAP_DEFAULT);
     if (NULL == oobtty_rx_struct_ptr->up_samp_mem_ptr)
     {
        MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"VCP: voice_oobtty_rx_resampler_init: Rx OOB TTY resampler init failed, out of memory");
        voice_oobtty_rx_free_resamp_memory(oobtty_rx_struct_ptr);
        return ADSP_ENOMEMORY;
     }

     // Initialize upsampler needed for OOB TTY Rx
     int32_t resample_result;
     resample_result = voice_resampler_channel_init(
                         &(oobtty_rx_struct_ptr->up_samp_config),
                         oobtty_rx_struct_ptr->up_samp_mem_ptr,
                         (oobtty_rx_struct_ptr->up_samp_config).total_channel_mem_size
                         );
     if(VOICE_RESAMPLE_SUCCESS !=resample_result)
     {
        MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO,"VCP: Rx OOB TTY up sampler init failed");
        voice_oobtty_rx_free_resamp_memory(oobtty_rx_struct_ptr);
        return ADSP_EFAILED;
     }
   }
   return ADSP_EOK;
}


static void voice_oobtty_rx_free_local_memory(oobtty_rx_struct_t* oobtty_rx_struct_ptr)
{
   // Destroy OOB TTY structures
   if(NULL != oobtty_rx_struct_ptr->oobtty_rx_struct_instance_ptr)
   {
      qurt_elite_memory_free(oobtty_rx_struct_ptr->oobtty_rx_struct_instance_ptr);
      oobtty_rx_struct_ptr->oobtty_rx_struct_instance_ptr = NULL;
   }
   if(NULL != oobtty_rx_struct_ptr->lte_tty_fifo_ptr)
   {
      qurt_elite_memory_free(oobtty_rx_struct_ptr->lte_tty_fifo_ptr);
      oobtty_rx_struct_ptr->lte_tty_fifo_ptr = NULL;
   }
}

static void voice_oobtty_rx_free_resamp_memory(oobtty_rx_struct_t* oobtty_rx_struct_ptr)
   {
   // Destroy Up sampler memory
   if(NULL != oobtty_rx_struct_ptr->up_samp_mem_ptr)
   {
      qurt_elite_memory_free(oobtty_rx_struct_ptr->up_samp_mem_ptr );
      oobtty_rx_struct_ptr->up_samp_mem_ptr = NULL;
   }
   return;
}

ADSPResult voice_oobtty_rx_get_kpps(oobtty_rx_struct_t* oobtty_rx_struct_ptr,uint32_t* kpps_ptr,uint8_t enable,uint16_t sampling_rate)
{
   ADSPResult result = ADSP_EOK;

   if ((NULL!= oobtty_rx_struct_ptr) && (NULL!= kpps_ptr))
   {
      *kpps_ptr = 0;
      if(enable)
      {
        result = vcmn_find_kpps_table(VOICE_OOBTTYRX_SAMPLING_KPPS_TABLE,sampling_rate,kpps_ptr);
      }
   }
   else
   {
      result = ADSP_EBADPARAM;
   }
   return result;
}
