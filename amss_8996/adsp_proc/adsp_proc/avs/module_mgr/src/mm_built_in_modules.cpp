/**
@file mm_iter.cpp***********************

@brief module manager AMDB iterface

 */

/*========================================================================
$Header: //components/rel/avs.adsp/2.7.1.c4/module_mgr/src/mm_built_in_modules.cpp#1 $

Edit History

when       who     what, where, why
--------   ---     -------------------------------------------------------
06/12/2015  rbhatnk      Created file.
==========================================================================*/

/*-----------------------------------------------------------------------
   Copyright (c) 2015 Qualcomm Technologies, Incorporated.  All Rights Reserved.
   QUALCOMM Proprietary.
-----------------------------------------------------------------------*/

#include "mm_main.h"
#ifndef STAND_ALONE
#include "DALSys.h"
#endif


static const uint32_t DEV_CFG_TYPE_CAPI = 0;
static const uint32_t DEV_CFG_TYPE_APPI = 1;
static const uint32_t DEV_CFG_TYPE_CAPI_V2 = 2;

#define CHECK_RET_DAL(x) \
      { \
   DALResult res = (x); \
   if (DAL_SUCCESS != res) \
   { \
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed in DAL call. Error code = %d", res); \
      return ADSP_EFAILED; \
   } \
      }

#define VERIFY_RET(x) \
      { \
   if (!(x)) \
   { \
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Condition check failed"); \
      return ADSP_EFAILED; \
   } \
      }

static void remove_all_static_entries();

#ifndef STAND_ALONE
static ADSPResult register_one_from_xml(DALSYSPropertyHandle hProps, uint32_t index)
{
   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Adding dynamic module %lu to the AMDB", index);
   ADSPResult result = ADSP_EOK;

   const uint32_t buffer_size = sizeof ("CAPIV2TYPE_") /* The longest token */
                           + 3 /* 3 digits for the index */
                           + 1; /* NULL character */
   char string[buffer_size];
   DALSYSPropertyVar PropVar;

   snprintf(string, buffer_size, "URI_%lu", index);
   CHECK_RET_DAL(DALSYS_GetPropertyValue(hProps, string, 0, &PropVar));
   VERIFY_RET(PropVar.dwType == DALSYS_PROP_TYPE_STR_PTR);
   const char *uri = PropVar.Val.pszVal;

   snprintf(string, buffer_size, "TYPE_%lu", index);
   CHECK_RET_DAL(DALSYS_GetPropertyValue(hProps, string, 0, &PropVar));
   VERIFY_RET(PropVar.dwType == DALSYS_PROP_TYPE_UINT32);
   const uint32_t type = PropVar.Val.dwVal;

   snprintf(string, buffer_size, "PRELOAD_%lu", index);
   CHECK_RET_DAL(DALSYS_GetPropertyValue(hProps, string, 0, &PropVar));
   VERIFY_RET(PropVar.dwType == DALSYS_PROP_TYPE_UINT32);
   const bool_t preload = (PropVar.Val.dwVal == 0) ? FALSE : TRUE;

   switch(type)
   {
   case DEV_CFG_TYPE_CAPI:
   {
      snprintf(string, buffer_size, "ID_%lu", index);
      CHECK_RET_DAL(DALSYS_GetPropertyValue(hProps, string, 0, &PropVar));
      VERIFY_RET(PropVar.dwType == DALSYS_PROP_TYPE_UINT32);
      const uint32_t id = PropVar.Val.dwVal;

      snprintf(string, buffer_size, "GETSIZE_%lu", index);
      CHECK_RET_DAL(DALSYS_GetPropertyValue(hProps, string, 0, &PropVar));
      VERIFY_RET(PropVar.dwType == DALSYS_PROP_TYPE_STR_PTR);
      const char *getsize = PropVar.Val.pszVal;

      snprintf(string, buffer_size, "CTOR_%lu", index);
      CHECK_RET_DAL(DALSYS_GetPropertyValue(hProps, string, 0, &PropVar));
      VERIFY_RET(PropVar.dwType == DALSYS_PROP_TYPE_STR_PTR);
      const char *ctor = PropVar.Val.pszVal;

      result = adsp_amdb_add_capi(id, preload, uri, getsize, ctor);
      if (ADSP_FAILED(result))
      {
         MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to add dev cfg CAPI module %lx. result %d. (ok when overridden)",
                        id, result);
      }
   }
   break;
   case DEV_CFG_TYPE_APPI:
   {
      snprintf(string, buffer_size, "ID_%lu", index);
      CHECK_RET_DAL(DALSYS_GetPropertyValue(hProps, string, 0, &PropVar));
      VERIFY_RET(PropVar.dwType == DALSYS_PROP_TYPE_UINT32);
      const uint32_t id = PropVar.Val.dwVal;

      snprintf(string, buffer_size, "GETSIZE_%lu", index);
      CHECK_RET_DAL(DALSYS_GetPropertyValue(hProps, string, 0, &PropVar));
      VERIFY_RET(PropVar.dwType == DALSYS_PROP_TYPE_STR_PTR);
      const char *getsize = PropVar.Val.pszVal;

      snprintf(string, buffer_size, "INIT_%lu", index);
      CHECK_RET_DAL(DALSYS_GetPropertyValue(hProps, string, 0, &PropVar));
      VERIFY_RET(PropVar.dwType == DALSYS_PROP_TYPE_STR_PTR);
      const char *init = PropVar.Val.pszVal;

      result = adsp_amdb_add_appi(id, preload, uri, getsize, init);
      if (ADSP_FAILED(result))
      {
         MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to add dev cfg APPI module %lx. result %d. (ok when overridden)",
               id, result); //this error is ok if module is overridden.
      }
   }
   break;
   case DEV_CFG_TYPE_CAPI_V2:
   {
      snprintf(string, buffer_size, "CAPIV2TYPE_%lu", index);
      CHECK_RET_DAL(DALSYS_GetPropertyValue(hProps, string, 0, &PropVar));
      VERIFY_RET(PropVar.dwType == DALSYS_PROP_TYPE_UINT32);
      const uint32_t type = PropVar.Val.dwVal;

      snprintf(string, buffer_size, "ID1_%lu", index);
      CHECK_RET_DAL(DALSYS_GetPropertyValue(hProps, string, 0, &PropVar));
      VERIFY_RET(PropVar.dwType == DALSYS_PROP_TYPE_UINT32);
      const uint32_t id1 = PropVar.Val.dwVal;

      snprintf(string, buffer_size, "ID2_%lu", index);
      CHECK_RET_DAL(DALSYS_GetPropertyValue(hProps, string, 0, &PropVar));
      VERIFY_RET(PropVar.dwType == DALSYS_PROP_TYPE_UINT32);
      const uint32_t id2 = PropVar.Val.dwVal;

      snprintf(string, buffer_size, "TAG_%lu", index);
      CHECK_RET_DAL(DALSYS_GetPropertyValue(hProps, string, 0, &PropVar));
      VERIFY_RET(PropVar.dwType == DALSYS_PROP_TYPE_STR_PTR);
      const char *tag = PropVar.Val.pszVal;

      result = adsp_amdb_add_capi_v2_with_entry_type(ADSP_AMDB_ENTRY_TYPE_INIT, type, id1, id2, preload, uri, tag);
      if (ADSP_FAILED(result))
      {
         MSG_4(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to add dev cfg CAPI V2 module %lx, 0x%lx, 0x%lx. result %d. (ok when overridden)",
                        type, id1, id2, result);
      }
   }
   break;
   default:
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed with invalid module type %lu.", type);
      result = ADSP_EBADPARAM;
   }

   return result;
}
#endif //STAND_ALONE

static ADSPResult register_all_from_dev_cfg_xml()
{
   ADSPResult result = ADSP_EOK;
#ifndef STAND_ALONE
   DALSYSPropertyVar PropVar;
   DALSYS_PROPERTY_HANDLE_DECLARE(hProps);

   CHECK_RET_DAL(DALSYS_GetDALPropertyHandleStr("dynamic_modules", hProps));
   CHECK_RET_DAL(DALSYS_GetPropertyValue(hProps, "NumModules", 0, &PropVar));
   VERIFY_RET(PropVar.dwType == DALSYS_PROP_TYPE_UINT32);

   uint64_t start_time=qurt_elite_timer_get_time_in_msec();
   uint32_t num_dev_cfg_modules = PropVar.Val.dwVal;
   uint32_t num_failed = 0;
   for (uint32_t i = 1; i < num_dev_cfg_modules + 1; i++)
   {
      result = register_one_from_xml(hProps, i);

      if (ADSP_FAILED(result))
      {
         num_failed ++;
      }
   }
   uint64_t end_time=qurt_elite_timer_get_time_in_msec();
   MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Loading modules from dev cfg XML: Successfully loaded %lu modules. Total failures %lu. time taken %lu ms ",
         num_dev_cfg_modules - num_failed, num_failed, (uint32_t)(end_time-start_time));

   if (num_failed > 0)
   {
      return ADSP_EFAILED;
   }
#endif
   return ADSP_EOK;
}

static bool_t check_file_exists(const char *filename)
{
   FILE *fp = fopen(filename, "rb");
   if (fp)
   {
      fclose(fp);
      return true;
   }
   return false;
}

/** returns the amount by which array index needs to be incremented from current position
 *
 * if a module has multiple revisions, only then  check for existence of file.
 * if only one version then return it.
 */
static uint32_t pick_appi_revision(uint32_t curr_ind, uint32_t *picked_ind)
{
   *picked_ind=curr_ind;
   if (curr_ind+1 < mm_num_dynamic_appi_modules)
   {
      if (mm_dynamic_appi_modules[curr_ind].id1 != mm_dynamic_appi_modules[curr_ind+1].id1)
      {
         //picked index is same as current index as next module is different.
         return 0; //curr_ind+1-curr_ind-1
      }
   }
   else
   {
      //picked index is same as current index as next module is different (doesn't exist).
      return 0;
   }

   //Do the following when multiple revisions are present
   //check for existence of file.
   //if file exists, then picked_ind=that index where revision's file exists
   //   if so move till next module is hit.
   //if file doesn't exist move till next module but chose picked index as cur_ind (input to func)
   //   this will help in AMDB logging about this module.
   bool_t found=false;
   for (uint32_t i = curr_ind; i < mm_num_dynamic_appi_modules; i++)
   {
      if (i != curr_ind)
      {
         //even if file is found, move index until module id changes.
         if (mm_dynamic_appi_modules[i-1].id1 != mm_dynamic_appi_modules[i].id1)
         {
            return i-curr_ind-1;
         }
      }
      //check for file only if it's not already found.
      if (!found)
      {
         if (check_file_exists(mm_dynamic_appi_modules[i].filename))
         {
            *picked_ind = i;
            found=true;
         }
      }
   }
   return mm_num_dynamic_appi_modules-curr_ind-1;
}

/**
 * similar to pick_appi_revision: check doc there.
 */
static uint32_t pick_capi_revision(uint32_t curr_ind, uint32_t *picked_ind)
{
   *picked_ind=curr_ind;
   if (curr_ind+1 < mm_num_dynamic_capi_modules)
   {
      if ( (mm_dynamic_capi_modules[curr_ind].id1 != mm_dynamic_capi_modules[curr_ind+1].id1) ||
            (mm_dynamic_capi_modules[curr_ind].mtype != mm_dynamic_capi_modules[curr_ind+1].mtype))
      {
         return 0; //curr_ind+1-curr_ind-1
      }
   }
   else
   {
      return 0;
   }

   bool_t found=false;
   for (uint32_t i = curr_ind; i < mm_num_dynamic_capi_modules; i++)
   {
      if (i != curr_ind)
      {
         if ( (mm_dynamic_capi_modules[i-1].id1 != mm_dynamic_capi_modules[i].id1) ||
               (mm_dynamic_capi_modules[i-1].mtype != mm_dynamic_capi_modules[i].mtype))
         {
            return i-curr_ind-1;
         }
      }
      if (!found)
      {
         if (check_file_exists(mm_dynamic_capi_modules[i].filename))
         {
            *picked_ind = i;
            MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Revision picked for CAPI m-id (%lu,0x%lx) is %lu",
                  mm_dynamic_capi_modules[i].mtype, mm_dynamic_capi_modules[i].id1,
                  mm_dynamic_capi_modules[i].revision);
            found=true;
         }
      }
   }
   return mm_num_dynamic_capi_modules-curr_ind-1;
}

/**
 * similar to pick_appi_revision: check doc there.
 */
static uint32_t pick_capi_v2_revision(uint32_t curr_ind, uint32_t *picked_ind)
{
   *picked_ind=curr_ind;
   if (curr_ind+1 < mm_num_dynamic_capi_v2_modules)
   {
      if ( (mm_dynamic_capi_v2_modules[curr_ind].id1 != mm_dynamic_capi_v2_modules[curr_ind+1].id1) ||
            (mm_dynamic_capi_v2_modules[curr_ind].id2 != mm_dynamic_capi_v2_modules[curr_ind+1].id2) ||
            (mm_dynamic_capi_v2_modules[curr_ind].mtype != mm_dynamic_capi_v2_modules[curr_ind+1].mtype))
      {
         return 0; //curr_ind+1-curr_ind-1
      }
   }
   else
   {
      return 0;
   }

   bool_t found=false;
   for (uint32_t i = curr_ind; i < mm_num_dynamic_capi_v2_modules; i++)
   {
      if (i != curr_ind)
      {
         if ( (mm_dynamic_capi_v2_modules[i-1].id1 != mm_dynamic_capi_v2_modules[i].id1) ||
               (mm_dynamic_capi_v2_modules[i-1].id2 != mm_dynamic_capi_v2_modules[i].id2) ||
               (mm_dynamic_capi_v2_modules[i-1].mtype != mm_dynamic_capi_v2_modules[i].mtype))
         {
            return i-curr_ind-1;
         }
      }
      if (!found)
      {
         if (check_file_exists(mm_dynamic_capi_v2_modules[i].filename))
         {
            *picked_ind = i;
            MSG_4(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Revision picked for CAPI V2 m-id (%lu,0x%lx,0x%lx) is %lu",
                  mm_dynamic_capi_v2_modules[i].mtype, mm_dynamic_capi_v2_modules[i].id1, mm_dynamic_capi_v2_modules[i].id2,
                  mm_dynamic_capi_v2_modules[i].revision);
            found=true;
         }
      }
   }
   return mm_num_dynamic_capi_v2_modules-curr_ind-1;
}


static ADSPResult add_appi_with_entry_type_tag(uint32_t entry_type, int id,
      boolean preload, const char* filename_str, const char* tag1, const char* tag2)
{
   if ( (NULL==tag2) || (strlen(tag2)==0))
   {
      return adsp_amdb_add_appi_with_tag(entry_type, id, preload,filename_str,tag1);
   }
   else
   {
      //if preload=false and more than one revision present then need to open file and check.
      ADSPResult result = adsp_amdb_add_appi_with_entry_type(ADSP_AMDB_ENTRY_TYPE_INIT, id,
            preload, filename_str, tag1, tag2);
      return result;
   }
}

static ADSPResult add_capi_with_entry_type_tag(uint32_t entry_type, int type, int id1, int id2,
      boolean preload, const char* filename_str, const char* tag1, const char* tag2)
{
   if ( (NULL==tag2) || (strlen(tag2)==0))
   {
      return adsp_amdb_add_capi_with_tag(entry_type, type, id1, id2, preload, filename_str, tag1);
   }
   else
   {
      ADSPResult result = adsp_amdb_add_capi_with_entry_type(ADSP_AMDB_ENTRY_TYPE_INIT, type,
            id1, 0, preload, filename_str, tag1, tag2);
      return result;
   }
}

static ADSPResult register_all_built_in_dynamic_modules()
{
   ADSPResult result = ADSP_EOK;

   uint64_t start_time=qurt_elite_timer_get_time_in_msec();
   const char empty[]="";

   MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "register_all_built_in_dynamic_modules adding dynamic modules to AMDB");
   // Even though module are separately listed in this file, there's no repetition from APPI to CAPI to CAPI V2 list.
   //add modules to the AMDB
   uint32_t num_capi_v2_added=0, num_appi_added=0, num_capi_added=0;
   uint32_t num_capi_v2_add_failed=0, num_appi_add_failed=0, num_capi_add_failed=0;

   uint32_t picked_ind=0;
   //Revision: always the latest revision is kept first by json parser.
   for (uint32_t i = 0; i < mm_num_dynamic_appi_modules; i++)
   {
      i+=pick_appi_revision(i, &picked_ind);

      //if preload=false and more than one revision present then need to open file and check.
      result = add_appi_with_entry_type_tag(ADSP_AMDB_ENTRY_TYPE_INIT, mm_dynamic_appi_modules[picked_ind].id1,
            mm_dynamic_appi_modules[picked_ind].preload,
            mm_dynamic_appi_modules[picked_ind].filename,
            mm_dynamic_appi_modules[picked_ind].tag, empty);

      if(ADSP_FAILED(result))
      {
         MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to add dynamic APPI module %lx. result %d. (ok when overridden)",
               mm_dynamic_appi_modules[picked_ind].id1, result); //this error is ok if module is overridden.
         num_appi_add_failed++;
      }
      else
      {
         num_appi_added++;
      }
   }
   MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Registering built-in APPI dynamic modules to AMDB. Num added %lu, Num Failed %lu, Total %lu. (any rest are revisions)",
         num_appi_added, num_appi_add_failed, mm_num_dynamic_appi_modules); //rest are skipped as they are revisions.

   for (uint32_t i = 0; i < mm_num_dynamic_capi_modules; i++)
   {
      i+=pick_capi_revision(i, &picked_ind);

      result = add_capi_with_entry_type_tag(ADSP_AMDB_ENTRY_TYPE_INIT, mm_dynamic_capi_modules[picked_ind].mtype,
            mm_dynamic_capi_modules[picked_ind].id1, 0, mm_dynamic_capi_modules[picked_ind].preload,
            mm_dynamic_capi_modules[picked_ind].filename, mm_dynamic_capi_modules[picked_ind].tag, empty);
      if(ADSP_FAILED(result))
      {
         MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to add dynamic CAPI module %lx. result %d. (ok when overridden)",
               mm_dynamic_capi_modules[picked_ind].id1, result);
         num_capi_add_failed++;
      }
      else
      {
         num_capi_added++;
      }
   }
   MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Registering built-in CAPI dynamic modules to AMDB. Num added %lu, Num Failed %lu, Total %lu. (any rest are revisions)",
         num_capi_added, num_capi_add_failed, mm_num_dynamic_capi_modules); //rest are skipped as they are revisions.

   for (uint32_t i = 0; i < mm_num_dynamic_capi_v2_modules; i++)
   {
      i+=pick_capi_v2_revision(i, &picked_ind);

      result = adsp_amdb_add_capi_v2_with_entry_type( ADSP_AMDB_ENTRY_TYPE_INIT, mm_dynamic_capi_v2_modules[picked_ind].mtype,
            mm_dynamic_capi_v2_modules[picked_ind].id1, mm_dynamic_capi_v2_modules[picked_ind].id2, mm_dynamic_capi_v2_modules[picked_ind].preload,
            mm_dynamic_capi_v2_modules[picked_ind].filename,
            mm_dynamic_capi_v2_modules[picked_ind].tag);

      if(ADSP_FAILED(result))
      {
         MSG_4(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to add dynamic CAPI V2 module %lx, 0x%lx, 0x%lx. result %d. (ok when overridden)",
               mm_dynamic_capi_v2_modules[picked_ind].mtype, mm_dynamic_capi_v2_modules[picked_ind].id1, mm_dynamic_capi_v2_modules[picked_ind].id2, result);
         num_capi_v2_add_failed++;
      }
      else
      {
         num_capi_v2_added++;
      }
   }
   MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Registering built-in CAPI V2 dynamic modules to AMDB. Num added %lu, Num Failed %lu, Total %lu. (any rest are revisions)",
         num_capi_v2_added, num_capi_v2_add_failed, mm_num_dynamic_capi_v2_modules); //rest are skipped as they are revisions.
   uint64_t end_time=qurt_elite_timer_get_time_in_msec();
   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "register_all_built_in_dynamic_modules done adding dynamic modules to AMDB. Time taken %lu ms",
         (uint32_t)(end_time-start_time));

   return result;
}

ADSPResult mm_register_all_static_modules_with_entry_type(uint32_t entry_type)
{
   ADSPResult result = ADSP_EOK;
   uint64_t start_time=qurt_elite_timer_get_time_in_msec();
   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "mm_register_static_modules adding static modules to AMDB. entry-type %lu", entry_type);
   // Even though module are separately listed in this file, there's no repetition from APPI to CAPI to CAPi V2 list.
   //add modules to the data base
   uint32_t num_capi_v2_added=0, num_appi_added=0, num_capi_added=0;
   uint32_t num_capi_v2_add_failed=0, num_appi_add_failed=0, num_capi_add_failed=0;
   for (uint32_t i = 0; i < mm_num_static_appi_modules; i++)
   {
      result = adsp_amdb_add_static_appi(entry_type, mm_static_appi_modules[i].id1,
            mm_static_appi_modules[i].appi_get_size_fn,
            mm_static_appi_modules[i].appi_init_fn);
      if(ADSP_FAILED(result))
      {
         MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to add static APPI module %lx. result %d. (ok when overridden)",
               mm_static_appi_modules[i].id1, result);
         num_appi_add_failed++;
      }
      else
      {
         num_appi_added++;
      }
   }
   for (uint32_t i = 0; i < mm_num_static_capi_modules; i++)
   {
      result = adsp_amdb_add_static_capi_ex(entry_type, mm_static_capi_modules[i].mtype, mm_static_capi_modules[i].id1,
            0, mm_static_capi_modules[i].get_size_fn, mm_static_capi_modules[i].ctor_fn);
      if(ADSP_FAILED(result))
      {
         MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to add static CAPI module %lx. result %d. (ok when overridden)",
               mm_static_capi_modules[i].id1, result);
         num_capi_add_failed++;
      }
      else
      {
         num_capi_added++;
      }
   }
   for (uint32_t i = 0; i < mm_num_static_capi_v2_modules; i++)
   {
      result = adsp_amdb_add_static_capi_v2( entry_type, mm_static_capi_v2_modules[i].mtype, mm_static_capi_v2_modules[i].id1,
            mm_static_capi_v2_modules[i].id2,
            mm_static_capi_v2_modules[i].get_static_prop_fn,
            mm_static_capi_v2_modules[i].init_fn);

      if(ADSP_FAILED(result))
      {
         MSG_4(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to add static CAPI V2 module %lx, 0x%lx, 0x%lx. result %d. (ok when overridden)",
               mm_static_capi_v2_modules[i].mtype, mm_static_capi_v2_modules[i].id1, mm_static_capi_v2_modules[i].id2, result);
         num_capi_v2_add_failed++;
      }
      else
      {
         num_capi_v2_added++;
      }
   }
   MSG_6(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Registering static modules to AMDB. Num added (CAPI V2, APPI, CAPI)=(%lu, %lu, %lu), Num Failed (%lu, %lu, %lu)).",
         num_capi_v2_added, num_appi_added, num_capi_added, num_capi_v2_add_failed, num_appi_add_failed, num_capi_add_failed);
   uint64_t end_time=qurt_elite_timer_get_time_in_msec();
   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "mm_register_static_modules done adding static modules to AMDB. Time taken %lu ms",(uint32_t)(end_time-start_time));

   return result;
}

/**
 * all the entries to got INIT entry.
 */
ADSPResult mm_register_all_built_in()
{
   ADSPResult result = ADSP_EOK;

   uint64_t start_time = qurt_elite_timer_get_time_in_msec();

   MSG(MSG_SSID_QDSP6, DBG_MED_PRIO, "mm_register_all_built_in: dev cfg XML ");
   result = register_all_from_dev_cfg_xml();

   if (ADSP_FAILED(result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "register_all_from_dev_cfg_xml failed %d", result);
   }
   else
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "register_all_from_dev_cfg_xml success");
   }


   MSG(MSG_SSID_QDSP6, DBG_MED_PRIO, "mm_register_all_built_in: built-in dynamic");
   result = register_all_built_in_dynamic_modules();

   if (ADSP_FAILED(result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "register_all_built_in_dynamic_modules failed %d", result);
   }
   else
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "register_all_built_in_dynamic_modules success");
   }


   MSG(MSG_SSID_QDSP6, DBG_MED_PRIO, "mm_register_all_built_in: built-in static ");
   result = mm_register_all_static_modules_with_entry_type(ADSP_AMDB_ENTRY_TYPE_INIT);

   if (ADSP_FAILED(result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "mm_register_static_modules failed %d", result);
   }
   else
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "mm_register_static_modules success");
   }

   MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "mm_register_all_built_in removing static entries as they are no longer needed");
   remove_all_static_entries();

   uint64_t end_time = qurt_elite_timer_get_time_in_msec();
   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "mm_register_all_built_in : time taken %lu ms", (uint32_t)(end_time-start_time));

   return result;
}

typedef struct mm_mod_built_in_info_t
{
   bool_t is_static; //dynamic otherwise
   uint32_t itype;
   union
   {
      struct //dynamic
      {
         bool_t preload;
         const char *filename; //for dynamic only
         const char *tag1; //althout built-in dynamic has only tag, dev cfg has both tags(=func names).
         const char *tag2;
      };
      struct
      {
         void *fn1; //func ptrs
         void *fn2;
      };
   };
} mm_mod_built_in_info_t;

/**
 * struct to help load needed prereq modules that are not registered already
 */
typedef struct mm_is_prereq_reg_t
{
   bool_t is_mod_needed; //true => corresponding mm_prereq_mids.id is needed module.
   bool_t is_prereq_registered[MM_MAX_PREREQUISITE_MIDS]; //true=> corresponding mm_prereq_mids.prereq_mids is in AMDB.
} mm_is_prereq_reg_t;
/**
 * this counts as long as module is mentioned in ACDB.
 * the module may not be marked as needed.
 * Eg. not-needed module: 'generic' it will be registered as stub.
 *                        'decoder' etc will not be registered.
 */
static bool_t *mm_is_dev_cfg_modules_in_acdb;
static bool_t *mm_is_dynamic_capi_v2_modules_in_acdb;
static bool_t *mm_is_dynamic_capi_modules_in_acdb;
static bool_t *mm_is_dynamic_appi_modules_in_acdb;
static bool_t *mm_is_static_capi_v2_modules_in_acdb;
static bool_t *mm_is_static_capi_modules_in_acdb;
static bool_t *mm_is_static_appi_modules_in_acdb;
static mm_is_prereq_reg_t *mm_is_prereq_reg_per_acdb; //tracks: is prereq registered per ACDB & also is module requiring prereq, needed?


static ADSPResult process_dev_cfg_modules_not_found_in_acdb()
{
   MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Processing any dev_cfg_modules_not_found_in_acdb");

   if (NULL == mm_is_dev_cfg_modules_in_acdb)
   {
      return ADSP_EOK;
   }
   //for built-in modules that didnt find a mention in ACDB file, we need to add a stub to INIT entry
#ifndef STAND_ALONE
   ADSPResult result=ADSP_EOK;

   DALSYSPropertyVar PropVar;
   DALSYS_PROPERTY_HANDLE_DECLARE(hProps);

   CHECK_RET_DAL(DALSYS_GetDALPropertyHandleStr("dynamic_modules", hProps));
   CHECK_RET_DAL(DALSYS_GetPropertyValue(hProps, "NumModules", 0, &PropVar));
   VERIFY_RET(PropVar.dwType == DALSYS_PROP_TYPE_UINT32);
   uint32_t num_dev_cfg_modules = PropVar.Val.dwVal;

   const uint32_t buffer_size = sizeof ("CAPIV2TYPE_") /* The longest token */
                           + 3 /* 3 digits for the index */
                           + 1; /* NULL character */
   char string[buffer_size];

   for (uint32_t index = 1; index < num_dev_cfg_modules + 1; index++)
   {
      //for modules which didn't get added
      if (!mm_is_dev_cfg_modules_in_acdb[index-1])
      {
         DALSYSPropertyVar PropVar;

         snprintf(string, buffer_size, "TYPE_%lu", index);
         CHECK_RET_DAL(DALSYS_GetPropertyValue(hProps, string, 0, &PropVar));
         VERIFY_RET(PropVar.dwType == DALSYS_PROP_TYPE_UINT32);
         const uint32_t type = PropVar.Val.dwVal;

         switch(type)
         {
         case DEV_CFG_TYPE_CAPI:
         {
            snprintf(string, buffer_size, "ID_%lu", index);
            CHECK_RET_DAL(DALSYS_GetPropertyValue(hProps, string, 0, &PropVar));
            VERIFY_RET(PropVar.dwType == DALSYS_PROP_TYPE_UINT32);
            const uint32_t id = PropVar.Val.dwVal;

            MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Ignoring dev cfg built-in CAPI module not found in ACDB file. ID 0x%lx.",  id);

            //result |= adsp_amdb_add_stub_with_entry_type(ADSP_AMDB_ENTRY_TYPE_INIT, AMDB_MODULE_TYPE_DECODER, id, 0); //mtype by default is decoder.
            //mm_is_dev_cfg_modules_in_acdb[index-1] = true;
         }
         break;
         case DEV_CFG_TYPE_APPI:
         {
            snprintf(string, buffer_size, "ID_%lu", index);
            CHECK_RET_DAL(DALSYS_GetPropertyValue(hProps, string, 0, &PropVar));
            VERIFY_RET(PropVar.dwType == DALSYS_PROP_TYPE_UINT32);
            const uint32_t id = PropVar.Val.dwVal;

            MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Ignoring dev cfg built-in APPI module not found in ACDB file.ID 0x%lx.",  id);

            //result |= adsp_amdb_add_stub_with_entry_type(ADSP_AMDB_ENTRY_TYPE_INIT, AMDB_MODULE_TYPE_GENERIC, id, 0);
            //mm_is_dev_cfg_modules_in_acdb[index-1] = true;

         }
         break;
         case DEV_CFG_TYPE_CAPI_V2:
         {
            snprintf(string, buffer_size, "CAPIV2TYPE_%lu", index);
            CHECK_RET_DAL(DALSYS_GetPropertyValue(hProps, string, 0, &PropVar));
            VERIFY_RET(PropVar.dwType == DALSYS_PROP_TYPE_UINT32);
            const uint32_t mtype = PropVar.Val.dwVal;

            snprintf(string, buffer_size, "ID1_%lu", index);
            CHECK_RET_DAL(DALSYS_GetPropertyValue(hProps, string, 0, &PropVar));
            VERIFY_RET(PropVar.dwType == DALSYS_PROP_TYPE_UINT32);
            const uint32_t _id1 = PropVar.Val.dwVal;

            snprintf(string, buffer_size, "ID2_%lu", index);
            CHECK_RET_DAL(DALSYS_GetPropertyValue(hProps, string, 0, &PropVar));
            VERIFY_RET(PropVar.dwType == DALSYS_PROP_TYPE_UINT32);
            const uint32_t _id2 = PropVar.Val.dwVal;

            MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Ignoring dev cfg built-in CAPI V2 module not found in ACDB. capiv2type %lu, ID1 0x%lx ID2 0x%lx.", mtype,_id1, _id2);

            //result |= adsp_amdb_add_stub_with_entry_type(ADSP_AMDB_ENTRY_TYPE_INIT, mtype, _id1, _id2);
            //mm_is_dev_cfg_modules_in_acdb[index-1] = true;

         }
         break;
         default:
            MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed with invalid module type %lu.", type);
            result |= ADSP_EBADPARAM;
         }
      }
   }
   if (ADSP_FAILED(result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failure occured in registering stub. %lu",result);
   }
#endif //#ifndef STAND_ALONE
   return ADSP_EOK;

}

void process_dynamic_modules_not_found_in_acdb()
{
   MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Processing any dynamic_modules_not_found_in_acdb");
   ADSPResult result = ADSP_EOK;
   /** dynamic */
   //for built-in modules that didnt find a mention in ACDB file, we need to add a stub to INIT entry
   if (mm_is_dynamic_capi_v2_modules_in_acdb)
   {
      for (uint32_t i = 0; i < mm_num_dynamic_capi_v2_modules; i++)
      {
         if ( !mm_is_dynamic_capi_v2_modules_in_acdb[i] )
         {
            MSG_4(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Ignoring built-in dynamic CAPI V2 module not found in ACDB file. mtype %lu, ID1 0x%lx ID2 0x%lx. Revision %lu",
                  mm_dynamic_capi_v2_modules[i].mtype, mm_dynamic_capi_v2_modules[i].id1, mm_dynamic_capi_v2_modules[i].id2, mm_dynamic_capi_v2_modules[i].revision);

            //result |= adsp_amdb_add_stub_with_entry_type(ADSP_AMDB_ENTRY_TYPE_INIT, mm_dynamic_capi_v2_modules[i].mtype, mm_dynamic_capi_v2_modules[i].id1, mm_dynamic_capi_v2_modules[i].id2);
            //mm_is_dynamic_capi_v2_modules_in_acdb[i] = true;
         }
      }
   }

   if (mm_is_dynamic_appi_modules_in_acdb)
   {
      for (uint32_t i = 0; i < mm_num_dynamic_appi_modules; i++)
      {
         if (!mm_is_dynamic_appi_modules_in_acdb[i])
         {
            MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Ignoring built-in dynamic APPI module not found in ACDB file. ID1 0x%lx. Revision %lu",
                  mm_dynamic_appi_modules[i].id1, mm_dynamic_appi_modules[i].revision);

            //result |= adsp_amdb_add_stub_with_entry_type(ADSP_AMDB_ENTRY_TYPE_INIT, AMDB_MODULE_TYPE_GENERIC, mm_dynamic_capi_modules[i].id1, 0);
            //mm_is_dynamic_appi_modules_in_acdb[i] = true;
         }
      }
   }

   if (mm_is_dynamic_capi_modules_in_acdb)
   {
      for (uint32_t i = 0; i < mm_num_dynamic_capi_modules; i++)
      {
         if (!mm_is_dynamic_capi_modules_in_acdb[i])
         {
            MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Ignoring built-in dynamic CAPI module not found in ACDB file. mtype %lu, ID1 0x%lx. Revision %lu",
                  mm_dynamic_capi_modules[i].mtype, mm_dynamic_capi_modules[i].id1, mm_dynamic_capi_modules[i].revision);

            //result |= adsp_amdb_add_stub_with_entry_type(ADSP_AMDB_ENTRY_TYPE_INIT, mm_dynamic_capi_modules[i].mtype, mm_dynamic_capi_modules[i].id1, 0);
            //mm_is_dynamic_capi_modules_in_acdb[i] = true;
         }
      }
   }
   if (ADSP_FAILED(result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failure occured in registering stub. %d",result);
   }
}

bool_t is_private_module(uint32_t mtype, uint32_t id1, uint32_t id2)
{
   for (uint32_t i=0; i<mm_num_private_modules;i++)
   {
      if ( (mtype == mm_private_modules[i].mtype) &&
            (id1 == mm_private_modules[i].id1) &&
            (id2 == mm_private_modules[i].id2))
      {
         return true;
      }
   }
   return false;
}

void process_static_modules_not_found_in_acdb()
{
   MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Processing any static_modules_not_found_in_acdb");

   ADSPResult result = ADSP_EOK;
   /** static */
   //for built-in modules that didnt find a mention in ACDB file, we need to add a stub to INIT entry
   //and remove static entry (except for private)
   if (mm_is_static_capi_v2_modules_in_acdb)
   {
      for (uint32_t i = 0; i < mm_num_static_capi_v2_modules; i++)
      {
         if ( !mm_is_static_capi_v2_modules_in_acdb[i] )
         {
            //move private modules to init entry.
            if (is_private_module(mm_static_capi_v2_modules[i].mtype, mm_static_capi_v2_modules[i].id1, mm_static_capi_v2_modules[i].id2))
            {
               MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Registering built-in private static CAPI V2 module to AMDB. mtype %lu, ID1 0x%lx ID2 0x%lx.",
                     mm_static_capi_v2_modules[i].mtype, mm_static_capi_v2_modules[i].id1, mm_static_capi_v2_modules[i].id2);

               result |= adsp_amdb_add_static_capi_v2(ADSP_AMDB_ENTRY_TYPE_INIT,
                     mm_static_capi_v2_modules[i].mtype, mm_static_capi_v2_modules[i].id1, mm_static_capi_v2_modules[i].id2,
                     mm_static_capi_v2_modules[i].get_static_prop_fn,  mm_static_capi_v2_modules[i].init_fn);
            }
            else
            {
               MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Ignoring built-in static CAPI V2 module not found in ACDB file. mtype %lu, ID1 0x%lx ID2 0x%lx.",
                     mm_static_capi_v2_modules[i].mtype, mm_static_capi_v2_modules[i].id1, mm_static_capi_v2_modules[i].id2);
               //result |= adsp_amdb_add_stub_with_entry_type(ADSP_AMDB_ENTRY_TYPE_INIT, mm_static_capi_v2_modules[i].mtype, mm_static_capi_v2_modules[i].id1, mm_static_capi_v2_modules[i].id2);
            }

            mm_is_static_capi_v2_modules_in_acdb[i] = true;

         }
      }
   }

   if (mm_is_static_appi_modules_in_acdb)
   {
      for (uint32_t i = 0; i < mm_num_static_appi_modules; i++)
      {
         if (!mm_is_static_appi_modules_in_acdb[i])
         {

            //move private modules to init entry.
            if (is_private_module(AMDB_MODULE_TYPE_GENERIC, mm_static_appi_modules[i].id1, 0))
            {
               MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Registering built-in private static APPI module to AMDB. ID1 0x%lx",
                     mm_static_appi_modules[i].id1);

               result |= adsp_amdb_add_static_appi(ADSP_AMDB_ENTRY_TYPE_INIT,
                     mm_static_appi_modules[i].id1, mm_static_appi_modules[i].appi_get_size_fn, (appi_init_f)mm_static_appi_modules[i].appi_init_fn);
            }
            else
            {
               MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Ignoring built-in static APPI module not found in ACDB file. ID1 0x%lx",
                     mm_static_appi_modules[i].id1);
               //result |= adsp_amdb_add_stub_with_entry_type(ADSP_AMDB_ENTRY_TYPE_INIT, AMDB_MODULE_TYPE_GENERIC, mm_static_appi_modules[i].id1, 0);
            }

            mm_is_static_appi_modules_in_acdb[i] = true;
         }
      }
   }

   if (mm_is_static_capi_modules_in_acdb)
   {
      for (uint32_t i = 0; i < mm_num_static_capi_modules; i++)
      {
         if (!mm_is_static_capi_modules_in_acdb[i])
         {
            //move private modules to init entry.
            if (is_private_module(mm_static_capi_modules[i].mtype, mm_static_capi_modules[i].id1, 0))
            {
               MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Registering built-in private static CAPI module to AMDB. mtype %lu, ID1 0x%lx",
                     mm_static_capi_modules[i].mtype, mm_static_capi_modules[i].id1);

               result |= adsp_amdb_add_static_capi_ex(ADSP_AMDB_ENTRY_TYPE_INIT,
                     mm_static_capi_modules[i].mtype, mm_static_capi_modules[i].id1, 0,
                     mm_static_capi_modules[i].get_size_fn, mm_static_capi_modules[i].ctor_fn);
            }
            else
            {
               MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Ignoring built-in static CAPI module not found in ACDB file. mtype %lu, ID1 0x%lx",
                     mm_static_capi_modules[i].mtype, mm_static_capi_modules[i].id1);
               //result |= adsp_amdb_add_stub_with_entry_type(ADSP_AMDB_ENTRY_TYPE_INIT, mm_static_capi_modules[i].mtype, mm_static_capi_modules[i].id1, 0);
            }

            mm_is_static_capi_modules_in_acdb[i] = true;
         }
      }
   }
   if (ADSP_FAILED(result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failure occured in registering private static or in adding stub. %d",result);
   }
}

static void remove_all_static_entries()
{
   ADSPResult result = ADSP_EOK;
   //Remove all static entries
   MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Removing all static entries from AMDB");
   for (uint32_t i = 0; i < mm_num_static_capi_v2_modules; i++)
   {
      result |= adsp_amdb_remove_capi_v2_with_entry_type(ADSP_AMDB_ENTRY_TYPE_STATIC, mm_static_capi_v2_modules[i].mtype, mm_static_capi_v2_modules[i].id1, mm_static_capi_v2_modules[i].id2);
   }

   for (uint32_t i = 0; i < mm_num_static_appi_modules; i++)
   {
      result |= adsp_amdb_remove_appi_with_entry_type(ADSP_AMDB_ENTRY_TYPE_STATIC, mm_static_appi_modules[i].id1);
   }

   for (uint32_t i = 0; i < mm_num_static_capi_modules; i++)
   {
      result |= adsp_amdb_remove_capi_with_entry_type(ADSP_AMDB_ENTRY_TYPE_STATIC, mm_static_capi_modules[i].mtype, mm_static_capi_modules[i].id1, 0);
   }

   if (ADSP_FAILED(result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failure occured in removing private static built-in %d",result);
   }
}

static void process_prereq_modules()
{
   MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Processing any pre-req modules ");
   //dependent modules which are needed per ACDB but not added to amdb are added now.
   if (!mm_is_prereq_reg_per_acdb) return;

   for (uint32_t i = 0; i < mm_num_prereq_mids; i++)
   {
      if (mm_is_prereq_reg_per_acdb[i].is_mod_needed)
      {
         for (uint32_t j = 0; j < MM_MAX_PREREQUISITE_MIDS; j++)
         {
            if (mm_prereq_mids[i].prereq_mids[j].id1 != 0)
            {
               if (!mm_is_prereq_reg_per_acdb[i].is_prereq_registered[j])
               {
                  //now register
                  MSG_6(MSG_SSID_QDSP6, DBG_HIGH_PRIO, " Prerequisite module-id (%lu, 0x%lx, 0x%lx) needed because of (%lu, 0x%lx, 0x%lx). being registered to AMDB now ",
                        mm_prereq_mids[i].prereq_mids[j].mtype, mm_prereq_mids[i].prereq_mids[j].id1, mm_prereq_mids[i].prereq_mids[j].id2,
                        mm_prereq_mids[i].id.mtype, mm_prereq_mids[i].id.id1, mm_prereq_mids[i].id.id2);


                  built_in_mod_info_t mod_info;
                  mod_info.mtype = mm_prereq_mids[i].prereq_mids[j].mtype;
                  mod_info.id1 = mm_prereq_mids[i].prereq_mids[j].id1;
                  mod_info.id2 = mm_prereq_mids[i].prereq_mids[j].id2;
                  mod_info.preload = AMDB_CFG_PRELOAD_FLAG_USE_FROM_BUILD;
                  mod_info.needed = true;

                  ADSPResult result = register_built_in_module(&mod_info);
                  if (ADSP_FAILED(result))
                  {
                     MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Error while processing prerequisite modules %d", result);
                  }
               }
            }
         }
      }
   }
}

//called only for ACDB file processing
//this is useful for tracking which in-built modules dont get registered.
//returns a ptr, which should be passed back in finalize.
uint8_t * mm_built_in_registration_tracker_init()
{
   uint32_t num_dev_cfg_modules = 0;
#ifndef STAND_ALONE
   DALSYSPropertyVar PropVar;
   DALSYS_PROPERTY_HANDLE_DECLARE(hProps);

   DALResult res = DALSYS_GetDALPropertyHandleStr("dynamic_modules", hProps);
   if (DAL_SUCCESS == res)
   {
      res = DALSYS_GetPropertyValue(hProps, "NumModules", 0, &PropVar);
      if (PropVar.dwType == DALSYS_PROP_TYPE_UINT32)
      {
         num_dev_cfg_modules = PropVar.Val.dwVal;
      }
   }
#endif //STAND_ALONE

   uint32_t total=(num_dev_cfg_modules+mm_num_dynamic_capi_v2_modules+mm_num_dynamic_capi_modules+mm_num_dynamic_appi_modules+
         mm_num_static_capi_v2_modules+mm_num_static_capi_modules+mm_num_static_appi_modules)*sizeof(bool_t)+
               (mm_num_prereq_mids*sizeof(mm_is_prereq_reg_t));

   uint8_t *ptr =  (uint8_t*)qurt_elite_memory_malloc(total, QURT_ELITE_HEAP_DEFAULT);
   if (ptr==NULL)
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to allocate memory");
      return ptr;
   }
   uint8_t *ret=ptr;
   memset(ptr, 0, total); //initialize with false.

   if (num_dev_cfg_modules)               mm_is_dev_cfg_modules_in_acdb = (uint8_t*)ptr;
   else                                   mm_is_dev_cfg_modules_in_acdb = NULL;
   ptr+=(num_dev_cfg_modules*sizeof(bool_t));

   if (mm_num_dynamic_capi_v2_modules)    mm_is_dynamic_capi_v2_modules_in_acdb = (uint8_t*)ptr;
   else                                   mm_is_dynamic_capi_v2_modules_in_acdb = NULL;
   ptr+=(mm_num_dynamic_capi_v2_modules*sizeof(bool_t));

   if (mm_num_dynamic_capi_modules)       mm_is_dynamic_capi_modules_in_acdb = (uint8_t*)ptr;
   else                                   mm_is_dynamic_capi_modules_in_acdb = NULL;
   ptr+=(mm_num_dynamic_capi_modules*sizeof(bool_t));

   if (mm_num_dynamic_appi_modules)       mm_is_dynamic_appi_modules_in_acdb = (uint8_t*)ptr;
   else                                   mm_is_dynamic_appi_modules_in_acdb = NULL;
   ptr+=(mm_num_dynamic_appi_modules*sizeof(bool_t));

   if (mm_num_static_capi_v2_modules)     mm_is_static_capi_v2_modules_in_acdb = (uint8_t*)ptr;
   else                                   mm_is_static_capi_v2_modules_in_acdb = NULL;
   ptr+=(mm_num_static_capi_v2_modules*sizeof(bool_t));

   if (mm_num_static_capi_modules)        mm_is_static_capi_modules_in_acdb = (uint8_t*)ptr;
   else                                   mm_is_static_capi_modules_in_acdb = NULL;
   ptr+=(mm_num_static_capi_modules*sizeof(bool_t));

   if (mm_num_static_appi_modules)        mm_is_static_appi_modules_in_acdb = (uint8_t*)ptr;
   else                                   mm_is_static_appi_modules_in_acdb = NULL;
   ptr+=(mm_num_static_appi_modules*sizeof(bool_t));

   if (mm_num_prereq_mids)                mm_is_prereq_reg_per_acdb = (mm_is_prereq_reg_t*)ptr;
   else                                   mm_is_prereq_reg_per_acdb = NULL;
   ptr+=(mm_num_prereq_mids*sizeof(mm_is_prereq_reg_t));

   return ret;
}

//called only for ACDB file processing
void mm_built_in_registration_tracker_finalize(bool_t built_in_sec_found, uint8_t* ptr)
{
   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Finalizing the registration. built_in_sec_found %u", built_in_sec_found);

   if (built_in_sec_found)
   {
      // process prereq modules
      process_prereq_modules();

      //process(print) all built-in modules that didn't get loaded to AMDB.
      //built-in modules that didnt find a mention in ACDB file, we just ignore.
      //earlier the idea was to add as stub, but then if a topo is created with those modules, a stub would be silent error but
      //failing is better as it immediately notifies.
      //finally all static entries are also removed. this way, modules not found in ACDB file never get used/consume any memory.

      //if modules mentioned in dev-cfg/dynamic are overridden on top of modules mentioned in dynamic/static, then
      // AMDB will fail (but failure is ignored).
      process_dev_cfg_modules_not_found_in_acdb();
      process_dynamic_modules_not_found_in_acdb();
      process_static_modules_not_found_in_acdb();

      //since built-in are added to init entry, static entry can be removed.
      //when built_in_sec_found is not found, mm_register_all_built_in does this.
      remove_all_static_entries();
   }

   if (NULL != ptr)
   {
      qurt_elite_memory_free(ptr);
   }

}

static ADSPResult look_up_module_in_dev_cfg(uint32_t mtype, uint32_t id1, uint32_t id2, mm_mod_built_in_info_t *binfo)
{
   ADSPResult result = ADSP_EFAILED; //look-up failed. didn't find.

#ifndef STAND_ALONE
   DALSYSPropertyVar PropVar;
   DALSYS_PROPERTY_HANDLE_DECLARE(hProps);

   CHECK_RET_DAL(DALSYS_GetDALPropertyHandleStr("dynamic_modules", hProps));
   CHECK_RET_DAL(DALSYS_GetPropertyValue(hProps, "NumModules", 0, &PropVar));
   VERIFY_RET(PropVar.dwType == DALSYS_PROP_TYPE_UINT32);
   uint32_t num_dev_cfg_modules = PropVar.Val.dwVal;

   const uint32_t buffer_size = sizeof ("CAPIV2TYPE_") /* The longest token */
                           + 3 /* 3 digits for the index */
                           + 1; /* NULL character */
   char string[buffer_size];

   for (uint32_t index = 1; index < num_dev_cfg_modules + 1; index++)
   {
      DALSYSPropertyVar PropVar;

      snprintf(string, buffer_size, "TYPE_%lu", index);
      CHECK_RET_DAL(DALSYS_GetPropertyValue(hProps, string, 0, &PropVar));
      VERIFY_RET(PropVar.dwType == DALSYS_PROP_TYPE_UINT32);
      const uint32_t type = PropVar.Val.dwVal;

      bool_t found=false;
      switch(type)
      {
      case DEV_CFG_TYPE_CAPI:
      {
         if ( (mtype == AMDB_MODULE_TYPE_DECODER) ||
               (mtype == AMDB_MODULE_TYPE_ENCODER) )
         {
            snprintf(string, buffer_size, "ID_%lu", index);
            CHECK_RET_DAL(DALSYS_GetPropertyValue(hProps, string, 0, &PropVar));
            VERIFY_RET(PropVar.dwType == DALSYS_PROP_TYPE_UINT32);
            const uint32_t id = PropVar.Val.dwVal;

            if (id == id1)
            {
               snprintf(string, buffer_size, "GETSIZE_%lu", index);
               CHECK_RET_DAL(DALSYS_GetPropertyValue(hProps, string, 0, &PropVar));
               VERIFY_RET(PropVar.dwType == DALSYS_PROP_TYPE_STR_PTR);
               const char *getsize = PropVar.Val.pszVal;

               snprintf(string, buffer_size, "CTOR_%lu", index);
               CHECK_RET_DAL(DALSYS_GetPropertyValue(hProps, string, 0, &PropVar));
               VERIFY_RET(PropVar.dwType == DALSYS_PROP_TYPE_STR_PTR);
               const char *ctor = PropVar.Val.pszVal;

               found = true;
               binfo->tag1 = getsize;
               binfo->tag2 = ctor;
               binfo->is_static = false;
               binfo->itype = AMDB_INTERFACE_TYPE_CAPI;

#ifdef MEDIUM_LOG
               MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Found module ID 0x%lx as CAPI in dev cfg.",  id);
#endif
            }
         }

      }
      break;
      case DEV_CFG_TYPE_APPI:
      {
         if (mtype==AMDB_MODULE_TYPE_GENERIC)
         {
            snprintf(string, buffer_size, "ID_%lu", index);
            CHECK_RET_DAL(DALSYS_GetPropertyValue(hProps, string, 0, &PropVar));
            VERIFY_RET(PropVar.dwType == DALSYS_PROP_TYPE_UINT32);
            const uint32_t id = PropVar.Val.dwVal;

            if (id == id1)
            {
               snprintf(string, buffer_size, "GETSIZE_%lu", index);
               CHECK_RET_DAL(DALSYS_GetPropertyValue(hProps, string, 0, &PropVar));
               VERIFY_RET(PropVar.dwType == DALSYS_PROP_TYPE_STR_PTR);
               const char *getsize = PropVar.Val.pszVal;

               snprintf(string, buffer_size, "INIT_%lu", index);
               CHECK_RET_DAL(DALSYS_GetPropertyValue(hProps, string, 0, &PropVar));
               VERIFY_RET(PropVar.dwType == DALSYS_PROP_TYPE_STR_PTR);
               const char *init = PropVar.Val.pszVal;

               found = true;
               binfo->tag1 = getsize;
               binfo->tag2 = init;
               binfo->is_static = false;
               binfo->itype = AMDB_INTERFACE_TYPE_APPI;
   #ifdef MEDIUM_LOG
               MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Found module ID 0x%lx as APPI in dev cfg.",  id);
   #endif
            }
         }
      }
      break;
      case DEV_CFG_TYPE_CAPI_V2:
      {
         snprintf(string, buffer_size, "CAPIV2TYPE_%lu", index);
         CHECK_RET_DAL(DALSYS_GetPropertyValue(hProps, string, 0, &PropVar));
         VERIFY_RET(PropVar.dwType == DALSYS_PROP_TYPE_UINT32);
         const uint32_t type = PropVar.Val.dwVal;

         snprintf(string, buffer_size, "ID1_%lu", index);
         CHECK_RET_DAL(DALSYS_GetPropertyValue(hProps, string, 0, &PropVar));
         VERIFY_RET(PropVar.dwType == DALSYS_PROP_TYPE_UINT32);
         const uint32_t _id1 = PropVar.Val.dwVal;

         snprintf(string, buffer_size, "ID2_%lu", index);
         CHECK_RET_DAL(DALSYS_GetPropertyValue(hProps, string, 0, &PropVar));
         VERIFY_RET(PropVar.dwType == DALSYS_PROP_TYPE_UINT32);
         const uint32_t _id2 = PropVar.Val.dwVal;

         if ( (type==mtype) && (id1==_id1) && (id2==_id2) )
         {
            snprintf(string, buffer_size, "TAG_%lu", index);
            CHECK_RET_DAL(DALSYS_GetPropertyValue(hProps, string, 0, &PropVar));
            VERIFY_RET(PropVar.dwType == DALSYS_PROP_TYPE_STR_PTR);
            const char *tag = PropVar.Val.pszVal;

            found = true;
            binfo->tag1 = tag;
            binfo->is_static = false;
            binfo->itype = AMDB_INTERFACE_TYPE_CAPI_V2;

#ifdef MEDIUM_LOG
            MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Found module mtype %lu, ID1 0x%lx ID2 0x%lx as CAPI V2 in dev cfg.", mtype,id1, id2);
#endif
         }
      }
      break;
      default:
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed with invalid module type %lu.", type);
         result = ADSP_EBADPARAM;
      }

      if (found)
      {
         snprintf(string, buffer_size, "URI_%lu", index);
         CHECK_RET_DAL(DALSYS_GetPropertyValue(hProps, string, 0, &PropVar));
         VERIFY_RET(PropVar.dwType == DALSYS_PROP_TYPE_STR_PTR);
         const char *uri = PropVar.Val.pszVal;

         snprintf(string, buffer_size, "PRELOAD_%lu", index);
         CHECK_RET_DAL(DALSYS_GetPropertyValue(hProps, string, 0, &PropVar));
         VERIFY_RET(PropVar.dwType == DALSYS_PROP_TYPE_UINT32);
         const bool_t preload = (PropVar.Val.dwVal == 0) ? FALSE : TRUE;

         binfo->filename = uri;
         binfo->preload = preload;

         //put a tick mark against module. so that remaining can be errored out.
         if (mm_is_dev_cfg_modules_in_acdb) mm_is_dev_cfg_modules_in_acdb[index-1] = true;
         return ADSP_EOK;
      }
   }
#endif //#ifndef STAND_ALONE
   return result;
}


static void mark_dependent_modules(uint32_t mtype, uint32_t id1, uint32_t id2, bool_t needed)
{
   if (!mm_is_prereq_reg_per_acdb)
   {
      return;
   }
   if (!needed)
   {
      return; //since default value = not needed.
   }

   //check if input module matches either the  mm_prereq_mids_t.id or one of the dependent modules.
   //since we cannot predict the order.
   for (uint32_t i = 0; i < mm_num_prereq_mids; i++)
   {
      if ( (mtype == mm_prereq_mids[i].id.mtype) &&
            (id1 == mm_prereq_mids[i].id.id1) &&
            (id2 == mm_prereq_mids[i].id.id2))
      {
         mm_is_prereq_reg_per_acdb[i].is_mod_needed = true; //eg. ds1
      }

      for (uint32_t j = 0; j < MM_MAX_PREREQUISITE_MIDS; j++)
      {
         if (mm_prereq_mids[i].prereq_mids[j].id1 != 0)
         {
            if ( (mtype == mm_prereq_mids[i].prereq_mids[j].mtype) &&
                  (id1 == mm_prereq_mids[i].prereq_mids[j].id1) &&
                  (id2 == mm_prereq_mids[i].prereq_mids[j].id2)) //eg. resampler, chmixer,
            {
               //a prereq module could be added from MREGINFO (overriding).
               //that's not tracked here. AMDB would return errors for overridden modules anyway.
               mm_is_prereq_reg_per_acdb[i].is_prereq_registered[j] = true;
            }
         }
      }
   }
}

static ADSPResult look_up_module_in_dynamic_mod_lists(uint32_t mtype, uint32_t id1, uint32_t id2, mm_mod_built_in_info_t *binfo, bool_t needed)
{
   //ADSPResult result = ADSP_EFAILED; //look-up failed. didn't find.

   uint32_t picked_ind=0;

   for (uint32_t i = 0; i < mm_num_dynamic_capi_v2_modules; i++)
   {
      if ( (mtype == mm_dynamic_capi_v2_modules[i].mtype) &&
            (id1 == mm_dynamic_capi_v2_modules[i].id1) &&
            (id2 == mm_dynamic_capi_v2_modules[i].id2))
      {
         uint32_t temp = i;
         i+=pick_capi_v2_revision(i, &picked_ind);

         binfo->is_static = false;
         binfo->preload = mm_dynamic_capi_v2_modules[picked_ind].preload;
         binfo->filename = mm_dynamic_capi_v2_modules[picked_ind].filename;
         binfo->tag1 = mm_dynamic_capi_v2_modules[picked_ind].tag;
         binfo->itype = AMDB_INTERFACE_TYPE_CAPI_V2;

         //put a tick mark against module. so that remaining can be errored out.
         if (mm_is_dynamic_capi_v2_modules_in_acdb)
         {
            for (uint32_t j=temp;j<=i;j++)
            {
               mm_is_dynamic_capi_v2_modules_in_acdb[j] = true;
            }
         }

         mark_dependent_modules(mtype, id1, id2, needed);

#ifdef MEDIUM_LOG
         MSG_4(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Found module mtype %lu, ID1 0x%lx ID2 0x%lx as CAPI V2 in dynamic mod list. Revision %lu",
               mm_dynamic_capi_v2_modules[picked_ind].mtype, mm_dynamic_capi_v2_modules[picked_ind].id1, mm_dynamic_capi_v2_modules[picked_ind].id2,
               mm_dynamic_capi_v2_modules[picked_ind].revision);
#endif
         return ADSP_EOK;
      }
   }

   if (mtype == AMDB_MODULE_TYPE_GENERIC)
   {
      for (uint32_t i = 0; i < mm_num_dynamic_appi_modules; i++)
      {
         if (mm_dynamic_appi_modules[i].id1 == id1)
         {
            uint32_t temp = i;
            i+=pick_appi_revision(i, &picked_ind);

            binfo->is_static = false;
            binfo->preload = mm_dynamic_appi_modules[picked_ind].preload;
            binfo->filename = mm_dynamic_appi_modules[picked_ind].filename;
            binfo->tag1 = mm_dynamic_appi_modules[picked_ind].tag;
            binfo->itype = AMDB_INTERFACE_TYPE_APPI;

            //put a tick mark against module. so that remaining can be errored out.
            if (mm_is_dynamic_appi_modules_in_acdb)
            {
               for (uint32_t j=temp;j<=i;j++)
               {
                  mm_is_dynamic_appi_modules_in_acdb[j] = true;
               }
            }

            mark_dependent_modules(mtype, id1, 0, needed);

#ifdef MEDIUM_LOG
            MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Found module ID1 0x%lx as APPI in dynamic mod list. Revision %lu",
                  mm_dynamic_appi_modules[picked_ind].id1, mm_dynamic_appi_modules[picked_ind].revision);
#endif
            return ADSP_EOK;
         }
      }
   }

   if ( (mtype == AMDB_MODULE_TYPE_DECODER) ||
         (mtype == AMDB_MODULE_TYPE_ENCODER) )
   {
      for (uint32_t i = 0; i < mm_num_dynamic_capi_modules; i++)
      {
         if ( (mtype == mm_dynamic_capi_modules[i].mtype) &&
               (id1 == mm_dynamic_capi_modules[i].id1) )
         {
            uint32_t temp = i;

            i+=pick_capi_revision(i, &picked_ind);

            binfo->is_static = false;
            binfo->preload = mm_dynamic_capi_modules[picked_ind].preload;
            binfo->filename = mm_dynamic_capi_modules[picked_ind].filename;
            binfo->tag1 = mm_dynamic_capi_modules[picked_ind].tag;
            binfo->itype = AMDB_INTERFACE_TYPE_CAPI;

            //put a tick mark against module. so that remaining can be errored out.
            if (mm_is_dynamic_capi_modules_in_acdb)
            {
               for (uint32_t j=temp;j<=i;j++)
               {
                  mm_is_dynamic_capi_modules_in_acdb[j] = true;
               }
            }

            mark_dependent_modules(mtype, id1, 0, needed);

#ifdef MEDIUM_LOG
            MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Found module mtype %lu, ID1 0x%lx as CAPI in dynamic mod list. Revision %lu",
                  mm_dynamic_capi_modules[picked_ind].mtype, mm_dynamic_capi_modules[picked_ind].id1, mm_dynamic_capi_modules[picked_ind].revision);
#endif
            return ADSP_EOK;
         }
      }
   }

   return ADSP_EFAILED;
}


static ADSPResult look_up_module_in_static_mod_lists(uint32_t mtype, uint32_t id1, uint32_t id2, mm_mod_built_in_info_t *binfo, bool_t needed)
{
   //ADSPResult result = ADSP_EFAILED;//look-up failed. didn't find.

   for (uint32_t i = 0; i < mm_num_static_capi_v2_modules; i++)
   {
      if ( (mtype == mm_static_capi_v2_modules[i].mtype) &&
            (id1 == mm_static_capi_v2_modules[i].id1) &&
            (id2 == mm_static_capi_v2_modules[i].id2))
      {
         binfo->is_static = true;
         binfo->fn1 = (void*) mm_static_capi_v2_modules[i].get_static_prop_fn;
         binfo->fn2 = (void*) mm_static_capi_v2_modules[i].init_fn;
         binfo->itype = AMDB_INTERFACE_TYPE_CAPI_V2;

         //put a tick mark against module. so that remaining can be errored out.
         if (mm_is_static_capi_v2_modules_in_acdb) mm_is_static_capi_v2_modules_in_acdb[i] = true;
         mark_dependent_modules(mtype, id1, id2, needed);

#ifdef MEDIUM_LOG
         MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Found module mtype %lu, ID1 0x%lx ID2 0x%lx as CAPI_V2 in static module list..",
               mm_static_capi_v2_modules[i].mtype, mm_static_capi_v2_modules[i].id1, mm_static_capi_v2_modules[i].id2);
#endif
         return ADSP_EOK;
      }
   }

   if (mtype == AMDB_MODULE_TYPE_GENERIC)
   {
      for (uint32_t i = 0; i < mm_num_static_appi_modules; i++)
      {
         if (mm_static_appi_modules[i].id1 == id1)
         {
            binfo->is_static = true;
            binfo->fn1 = (void*) mm_static_appi_modules[i].appi_get_size_fn;
            binfo->fn2 = (void*) mm_static_appi_modules[i].appi_init_fn;
            binfo->itype = AMDB_INTERFACE_TYPE_APPI;

            //put a tick mark against module. so that remaining can be errored out.
            if(mm_is_static_appi_modules_in_acdb) mm_is_static_appi_modules_in_acdb[i] = true;
            mark_dependent_modules(mtype, id1, 0, needed);

#ifdef MEDIUM_LOG
            MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Found module ID1 0x%lx as APPI in static module list",
                  mm_static_appi_modules[i].id1);
#endif
            return ADSP_EOK;
         }
      }
   }

   if ( (mtype == AMDB_MODULE_TYPE_DECODER) ||
         (mtype == AMDB_MODULE_TYPE_ENCODER) )
   {
      for (uint32_t i = 0; i < mm_num_static_capi_modules; i++)
      {
         if ( (mtype == mm_static_capi_modules[i].mtype) &&
               (id1 == mm_static_capi_modules[i].id1) )
         {
            binfo->is_static = true;
            binfo->fn1 = (void*) mm_static_capi_modules[i].get_size_fn;
            binfo->fn2 = (void*) mm_static_capi_modules[i].ctor_fn;
            binfo->itype = AMDB_INTERFACE_TYPE_CAPI;

            //put a tick mark against module. so that remaining can be errored out.
            if(mm_is_static_capi_modules_in_acdb) mm_is_static_capi_modules_in_acdb[i] = true;
            mark_dependent_modules(mtype, id1, 0, needed);

#ifdef MEDIUM_LOG
            MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Found module mtype %lu, ID1 0x%lx as CAPI in static module list",
                  mm_static_capi_modules[i].mtype, mm_static_capi_modules[i].id1);
#endif
            return ADSP_EOK;
         }
      }
   }

   return ADSP_EFAILED;
}

static ADSPResult mm_query_built_in_info_per_module(uint32_t mtype, uint32_t id1, uint32_t id2, mm_mod_built_in_info_t *binfo, bool_t needed)
{
   ADSPResult result = ADSP_EOK;

   //order of lookup is important. precedence.

   //first look into dev cfg xml
   result = look_up_module_in_dev_cfg(mtype, id1, id2, binfo);
   if (ADSP_SUCCEEDED(result))
   {
      MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Module (%lu, 0x%lx, 0x%lx) found in dev cfg", mtype, id1, id2);
      return result;
   }
   MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Module (%lu, 0x%lx, 0x%lx) not found in dev cfg. Looking into list of dynamic modules", mtype, id1, id2);

   //look into dynamic modules
   result = look_up_module_in_dynamic_mod_lists(mtype, id1, id2, binfo, needed);
   if (ADSP_SUCCEEDED(result))
   {
      MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Module (%lu, 0x%lx, 0x%lx) found in dynamic mod list", mtype, id1, id2);
      return result;
   }
   MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Module (%lu, 0x%lx, 0x%lx) not found in dynamic mod list. Looking into list of static modules", mtype, id1, id2);

   //look into static modules list
   result = look_up_module_in_static_mod_lists(mtype, id1, id2, binfo, needed); //needed is for handling dependent modules.
   if (ADSP_SUCCEEDED(result))
   {
      MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Module (%lu, 0x%lx, 0x%lx) found in static mod list", mtype, id1, id2);
      return result;
   }
   MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Module (%lu, 0x%lx, 0x%lx) not found in any list", mtype, id1, id2);

   return result;
}

ADSPResult register_built_in_module(built_in_mod_info_t *mod_info)
{
   ADSPResult result = ADSP_EOK;

   mm_mod_built_in_info_t binfo;
   memset(&binfo,0,sizeof(binfo));
   //need to call this outside mod_info->needed condition because, even if its not needed, we need to know whether ACDB knows about module.
   //module not known to ACDB are stubbed just like not-needed ones.
   result = mm_query_built_in_info_per_module(mod_info->mtype, mod_info->id1, mod_info->id2, &binfo, mod_info->needed);
   if (ADSP_FAILED(result))
   {
      MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO," Built-in information not found for module-id (%lu, 0x%lx, 0x%lx). Ignoring this module.", mod_info->mtype, mod_info->id1, mod_info->id2);
      return result;
   }

   if (mod_info->preload == AMDB_CFG_PRELOAD_FLAG_USE_FROM_BUILD)
   {
      mod_info->preload = binfo.preload;
   }

   if (mod_info->needed)
   {
      switch(binfo.itype)
      {
      case AMDB_INTERFACE_TYPE_APPI:
      {
         if (binfo.is_static)
         {
            result = adsp_amdb_add_static_appi(ADSP_AMDB_ENTRY_TYPE_INIT,
                  mod_info->id1, (appi_getsize_f)binfo.fn1, (appi_init_f)binfo.fn2);
         }
         else
         {
            result = add_appi_with_entry_type_tag(ADSP_AMDB_ENTRY_TYPE_INIT,
                  mod_info->id1, mod_info->preload,(const char*) binfo.filename, (const char*)binfo.tag1, (const char*)binfo.tag2);
         }
         break;
      }
      case AMDB_INTERFACE_TYPE_CAPI:
      {
         if (binfo.is_static)
         {
            result = adsp_amdb_add_static_capi_ex(ADSP_AMDB_ENTRY_TYPE_INIT,
                  mod_info->mtype, mod_info->id1, mod_info->id2,
                  (capi_getsize_f)binfo.fn1, (capi_ctor_f)binfo.fn2);
         }
         else
         {
            result = add_capi_with_entry_type_tag(ADSP_AMDB_ENTRY_TYPE_INIT,
                  mod_info->mtype, mod_info->id1, mod_info->id2,
                  mod_info->preload, (const char*)binfo.filename, (const char*)binfo.tag1, (const char*)binfo.tag2);
         }
         break;
      }
      case AMDB_INTERFACE_TYPE_CAPI_V2:
      {
         if (binfo.is_static)
         {
            result = adsp_amdb_add_static_capi_v2(ADSP_AMDB_ENTRY_TYPE_INIT,
                  mod_info->mtype, mod_info->id1, mod_info->id2,
                  (capi_v2_get_static_properties_f)binfo.fn1, (capi_v2_init_f)binfo.fn2);
         }
         else
         {
            result = adsp_amdb_add_capi_v2_with_entry_type(ADSP_AMDB_ENTRY_TYPE_INIT,
                  mod_info->mtype, mod_info->id1, mod_info->id2,
                  mod_info->preload, (const char*)binfo.filename,
                  (const char*)binfo.tag1);
         }
         break;
      }
      default:
      {
         result = ADSP_EUNSUPPORTED;
         break;
      }
      }
   }
   else
   {
      MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO," Module is marked as not needed module-id (%lu, 0x%lx, 0x%lx). Adding as stub. ",
            mod_info->mtype, mod_info->id1, mod_info->id2);
      result = adsp_amdb_add_stub_with_entry_type(ADSP_AMDB_ENTRY_TYPE_INIT, mod_info->mtype, mod_info->id1, mod_info->id2);
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"adsp_amdb_add_stub_with_entry_type returned %d", result);
   }

   return result;
}

