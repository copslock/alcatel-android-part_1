/*=============================================================================
  @file pd_audio_spawn.c

  This file contains sensors PD initialization code.

*******************************************************************************
* Copyright (c) 2012-2015 Qualcomm Technologies, Inc.  All Rights Reserved
* Qualcomm Technologies Confidential and Proprietary.
********************************************************************************/

/* $Header: //components/rel/dspcore.adsp/2.8/main/src/pd_audio_spawn.c#5 $ */
/* $DateTime: 2016/04/07 19:22:14 $ */
/* $Author: pwbldsvc $ */

/*============================================================================
  EDIT HISTORY FOR FILE

  when        who  what, where, why
  ----------  ---  -----------------------------------------------------------
  2015-04-06   AJ   pd spwan for audio
  
============================================================================*/

/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include <string.h>
#include <errno.h>
#include <stdio.h>

#include "qurt.h"
#include "pd_mon_qurt.h"
#include "engg_timestamp.h"
#include "qcom_timestamp.h"
#include "qurt_elite.h"

/* APR */
#include "aprv2_api_inline.h"
#include "apr_errcodes.h"
#include "aprv2_msg_if.h"
#include "aprv2_api.h"
extern unsigned int image_pstart;
extern unsigned int image_vstart;
static char Engg_ImageCreationDate[80];
static char Qcom_ImageCreationDate[80];
static char Adsp_StartAddress[50];

void pd_mon_audio(void)
{
    PD_MON_HANDLE hPdAudio;
   PD_MON_RESTART restart = PD_MON_RESTART_ALWAYS;
   hPdAudio=pd_mon_spawn(&restart, AUDIO_IMG_NAME);
   if (hPdAudio == 0){
    qurt_printf("%s: failed to spawn ... restart: %d \n", __func__, restart);
  }
  else{
  qurt_printf("%s: Protection Domain Launched %s\n", __func__, AUDIO_IMG_NAME);
}
}

void image_creation_timestamp(void)
{
    strlcpy(Engg_ImageCreationDate,engg_timestampstring,sizeof(Engg_ImageCreationDate));
    strlcpy(Qcom_ImageCreationDate,qcom_timestampstring,sizeof(Qcom_ImageCreationDate));
    snprintf ( Adsp_StartAddress, 50, "*ADSP_START_ADDRESS: PA-0X%x VA-0X%x", image_pstart, image_vstart  );
    qurt_printf("\n*ADSP_STARTADDRESS - %s*",Adsp_StartAddress);
    qurt_printf("\n**********************************\n* DSP Image Creation Date: %s\n*******************************************************\n",Engg_ImageCreationDate);
    MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "Engineering Image Creation Date: %s\n", Engg_ImageCreationDate);
    MSG_1(MSG_SSID_QDSP6, DBG_MED_PRIO, "Qcom Image Creation Date: %s\n", Qcom_ImageCreationDate);
}

void ipc_init_hack(void)
{
    if ( apr_call( APRV2_CMDID_HACK_INIT_IPC, NULL, 0 ) != APR_EOK )
    {
        qurt_printf("\n APR IPC INIT failed \n"); 
        ERR_FATAL("QDSP6 Main.c: APR IPC INIT failed ",0,0,0);
    }
}

void fatal_debug_error()
{

#if defined(__qdsp6__) && !defined(SIM)   
   ERR_FATAL("FATAL_ERR: Force crash Q6 due to AFE signal miss", 0, 0, 0);
#endif

   return; /* After entering ERR_FATAL(), execution never comes back here */
}

