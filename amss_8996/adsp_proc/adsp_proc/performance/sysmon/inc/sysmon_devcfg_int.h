/*-----------------------------------------------------------------------
   Copyright (c) 2015 QUALCOMM Technologies, Incorporated.
   All Rights Reserved.
   QUALCOMM Proprietary.
-----------------------------------------------------------------------*/

#ifndef SYSMON_DEVCFG_INT_H_
#define SYSMON_DEVCFG_INT_H_

#include "DALDeviceId.h"
#include "DDIChipInfo.h"
#include "DALSys.h"
#include "sysmon_global.h"

/** Sysmon device name in device configuration database  */
#define SYSMON_DEVCONFIG_DEVICE_NAME                  "/qdsp6/sysmon"

/** Device version management defines */
#define SD_HW_VER(nMinMajor, nMinMinor, nMaxMajor, nMaxMinor)               \
            ( ((nMinMajor<<24)& 0xFF000000) | ((nMinMinor<<16) & 0x00FF0000) | \
              ((nMaxMajor<<8) & 0x0000FF00) |  (nMaxMinor&0xFF) )

#define SD_HW_VER_MIN( nBSPVersion ) ( ((nBSPVersion & 0xFF000000)>>8) | \
                                        ((nBSPVersion & 0x00FF0000)>>16) )
#define SD_HW_VER_MAX( nBSPVersion ) ( ((nBSPVersion & 0x0000FF00)<<8) | \
                                         (nBSPVersion & 0x000000FF) )

/* Enum for corner voltage levels */
typedef enum
{
  SYSMON_CLOCK_VREG_LEVEL_OFF          = 0,
  SYSMON_CLOCK_VREG_LEVEL_RETENTION    = 1,
  SYSMON_CLOCK_VREG_LEVEL_LOW_MINUS    = 2,
  SYSMON_CLOCK_VREG_LEVEL_LOW          = 3,
  SYSMON_CLOCK_VREG_LEVEL_LOW_PLUS     = 4,
  SYSMON_CLOCK_VREG_LEVEL_NOMINAL      = 5,
  SYSMON_CLOCK_VREG_LEVEL_NOMINAL_PLUS = 6,
  SYSMON_CLOCK_VREG_LEVEL_HIGH         = 7,
  SYSMON_CLOCK_VREG_NUM_LEVELS,
}sd_vreg_type_t;

/* Structure type for device version database */
typedef struct{
    DalChipInfoVersionType nChipVersion;
    DalChipInfoFamilyType eChipInfoFamily;
    const DalChipInfoIdType *aeChipInfoId;
}sd_hw_version_type_t;

/* Structure type for bus clock table elements */
typedef struct{
    uint32_t freqKHz;                   //frequency in KiloHertz
    sd_vreg_type_t vRegLevel;           //Voltage corner for this perf level
    sd_hw_version_type_t hwVersion;     //HW versions supporting this perf level
}sd_devcfg_bus_clk_table_t;

/* Structure type for bus clock descriptor */
typedef struct{
    uint32_t numLevels;                     //Number of perf levels in the table
    sd_devcfg_bus_clk_table_t* tablePtr;    //bus clock table pointer
}sd_devcfg_bus_clk_desc_t;

/* Structure type for core clock table elements */
typedef struct{
    uint32_t freqKHz;                   //frequency in KiloHertz
    sd_vreg_type_t vRegLevel;           //Voltage corner for this perf level
    sd_hw_version_type_t hwVersion;     //HW versions supporting this perf level
}sd_devcfg_core_clk_table_t;

/* Structure type for core clock descriptor */
typedef struct{
    uint32_t numLevels;                     //Number of perf levels in the table
    sd_devcfg_core_clk_table_t* tablePtr;   //core clock table pointer
}sd_devcfg_core_clk_desc_t;

/**
 * @brief structure definition for global sysmon device config data
 */
typedef struct{
    /* Device version information from DAL */
    DalChipInfoFamilyType nChipFamily;
    DalChipInfoIdType nChipId;
    DalChipInfoVersionType nChipVersion;
    /* DCVS configurable parameters */
    uint32_t hfwWinNonAudio;
    uint32_t hfwWinAudio;
    uint32_t coreClkSafeLevelAudio;
    uint32_t coreClkDangerLevelAudio;
    uint32_t coreClkSafeLevelNonAudio;
    uint32_t coreClkDangerLevelNonAudio;
    uint32_t coreClk1TSafeLevel;
    uint32_t coreClkGoLowLevelAudio;
    uint32_t coreClkGoLowLevelNonAudio;
    uint32_t coreClkGoLowHystAdj;
    uint32_t coreClkGoLowHystAdjMin;
    uint32_t coreClkGoDownTimeThres;
    uint32_t coreClkGoDownHFWSamples;
    uint32_t coreClkGoDownHystTime;
    uint32_t coreClkGoDownHystMaxTime;
    uint32_t busClkGoDownTimeThres;
    uint32_t busClkGoDownHFWSamples;
    uint32_t dcvsTransDur;
    uint32_t dcvsTransDurMax;
    /* Clock table descriptors */
    sd_devcfg_bus_clk_desc_t* devCfgBusClkDesc;
    sd_devcfg_core_clk_desc_t* devCfgCoreClkDesc;
    /* BW correction factors for prefetch and demand misses */
    uint32_t bwPrefetchMissFact;
    uint32_t bwDemandMissFact;
    uint32_t bwWriteMissFact;
    uint32_t bwCoprocMissFact;
    uint32_t bwOverallFact;
    uint32_t snocBusWidth;
    uint32_t npaSnocClkMult;
    uint32_t pCppDangerLevel;
    uint32_t pCppSafeLevel;
    uint32_t coreStallsDangerLevel;
    uint32_t bwPeaksMax;
    uint32_t bwPeaksWindow;
    uint32_t bwPeaksMaxCompute;
    uint32_t bwPeaksWindowCompute;
}sysmon_devcfg_struct_t;

/**************************************************************************//**
 * @fn sysmon_devcfg_init
 * @brief SysMon device configuration init function. Initializes device
 *        interface and reads the configuration parameters
 * @return SYSMON_SUCCESS - for success
 * @return SYSMON_FAILURE - for failure
 *****************************************************************************/
SYSMON_RETURN sysmon_devcfg_init(void);

#endif /* SYSMON_DEVCFG_INT_H_ */
