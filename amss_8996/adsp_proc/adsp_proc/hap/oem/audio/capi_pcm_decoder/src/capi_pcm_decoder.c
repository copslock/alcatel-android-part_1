/*==============================================================================
  Copyright (c) 2012-2013 Qualcomm Technologies, Inc.
  All rights reserved. Qualcomm Proprietary and Confidential.
==============================================================================*/

#include <string.h>
#include <stdlib.h>

#include "decode.h"
#include "Elite_CAPI.h"
#include "Elite_pcm_ch_defs.h"
#include "capi_pcm_decoder.h"
#include "decode_struct.h"

#ifndef _DEBUG
#define _DEBUG
#endif
#include "HAP_farf.h"

static inline uint32_t align_to_8_byte(const uint32_t num)
{
   return ((num + 7) & (0xFFFFFFF8));
}

ADSPResult capi_pcm_decoder_Init(ICAPIVtbl* _pif, CAPI_Buf_t* b)
{
  return DECODE_SUCCESS;
}

ADSPResult capi_pcm_decoder_ReInit(ICAPIVtbl* _pif, CAPI_Buf_t* b)
{
  //No parameters to be initialized again for Decoder module. Hence nothing done.
  return DECODE_SUCCESS;
}

ADSPResult capi_pcm_decoder_End(ICAPIVtbl* _pif)
{
  return DECODE_SUCCESS;
}

ADSPResult capi_pcm_decoder_GetParam(ICAPIVtbl* _pif, int nParamIdx, int* pnParamVal)
{
  capi_pcm_decoder_t* me = (capi_pcm_decoder_t*)_pif;

  switch(nParamIdx) {
    case eIcapiNumChannels :
      *pnParamVal = (int)me->m_sNumberOfChannels;
      break;

    case eIcapiSamplingRate :
      *pnParamVal = (int)me->m_lSamplingRate;
      break;

    case eIcapiInputBufferSize :
      *pnParamVal = DECODER_INP_BUF_SIZE;
      break;

    case eIcapiOutputBufferSize :
      *pnParamVal = (int)DECODER_OUT_BUF_SIZE;
      break;

    case eIcapiThreadStackSize :
      *pnParamVal = (int)DECODER_THREAD_STACK_SIZE;
      break;

    case eIcapiWaitForFormatBlock :
      *pnParamVal = (int)DECODER_FORMAT_BLOCK_REQ;
      break;

    case eIcapiBitsPerSample:
      *pnParamVal = 16;
      break;

    case eIcapiKCPSRequired:
      *pnParamVal = DECODER_KIPS;
      break;

    case eNumBlockSize :
      *pnParamVal = (int)me->m_lNumBlockSize;
      break;

    case eDec_output_bytes_per_sample :
      *pnParamVal = (int)(me->m_lDec_output_bytes_per_sample);
      break;

    case eDec_input_bytes_per_sample :
      *pnParamVal = (int)(me->m_lDec_input_bytes_per_sample);
      break;


    case eDec_EndOfFrame :
      *pnParamVal = (int)me->m_lDec_EndOfFrame;
      break;

    case eIcapiOutputChanMap:

      me->out_chan_map.nChannels =
         (unsigned int)me->m_sNumberOfChannels;

      if (1 == me->out_chan_map.nChannels) {
        me->out_chan_map.nChannelMap[0] = eIcapiCentre;
      } else if (2 == me->out_chan_map.nChannels) {
        me->out_chan_map.nChannelMap[0] = eIcapiLeft;
        me->out_chan_map.nChannelMap[1] = eIcapiRight;
      }
      *pnParamVal = (int)(&me->out_chan_map);

      break;

    default:
      return (DEC_BADPARAM_FAILURE);
  }
  FARF(HIGH, "Reached end of GetParam function Sample Decoder CAPI : nParamIdx = %x , pnParamVal = %x", nParamIdx, pnParamVal);
  return DECODE_SUCCESS;
}

ADSPResult capi_pcm_decoder_SetParam(ICAPIVtbl* _pif, int lParamIdx, int lParamVal)
{
  capi_pcm_decoder_t* me = (capi_pcm_decoder_t*)_pif;

  switch(lParamIdx) {
    case eFormatTag:
      me->m_lFormatTag = (uint32_t)lParamVal;
      break;

    case eIcapiNumChannels :
      me->m_sNumberOfChannels = (uint16_t)lParamVal;
      break;

    case eIcapiSamplingRate :
      me->m_lSamplingRate = (uint32_t)lParamVal;
      break;

    case eIsSigned :
      me->m_lIsSigned = (uint32_t)lParamVal;
      break;

    case eIsInterleaved :
      me->m_lIsInterleaved = (uint32_t)lParamVal;
      break;

    case eNumBlockSize :
      me->m_lNumBlockSize = (uint32_t)lParamVal;
      break;

    case eIcapiBitsPerSample:
      me->m_sBitsPerSample = (uint32_t)lParamVal;
      break;

    case eDec_output_bytes_per_sample :
      me->m_lDec_output_bytes_per_sample = (uint32_t)lParamVal;
      break;

    case eDec_input_bytes_per_sample :
      me->m_lDec_input_bytes_per_sample = (uint32_t)lParamVal;
      break;

    case eDec_EndOfFrame :
      me->m_lDec_EndOfFrame = (uint32_t)lParamVal;
      break;

    case eDec_samples_to_copy :
      me->m_lDec_SamplesToCopy = (uint32_t)lParamVal;
      break;

      /* eIcapiMediaInfoPtr is newly added enum to handle setparam for dynamic module.
      The payload recieved from APPS processor is passed to CAPI module via this enum.
      */
    case eIcapiMediaInfoPtr :
      me->m_lDec_MediaFmtPtr = (asm_decode_fmt_blk_t*)(lParamVal);
      me->m_lDec_input_bytes_per_sample = (me->m_lDec_MediaFmtPtr->bits_per_sample) / 8;
      me->m_lDec_output_bytes_per_sample = (me->m_lDec_MediaFmtPtr->bits_per_sample) / 8;
      me->m_sNumberOfChannels = me->m_lDec_MediaFmtPtr->num_channels;
      me->m_lDec_SamplesToCopy = 2048;
      me->m_lSamplingRate = me->m_lDec_MediaFmtPtr->sample_rate;
      break;

    default:
      return DEC_BADPARAM_FAILURE;
  }

  FARF(HIGH, "Reached end of SetParam function Sample Decoder CAPI : lParamIdx = %x , lParamVal = %x", lParamIdx, lParamVal);
  return DECODE_SUCCESS;
}

ADSPResult capi_pcm_decoder_Process(ICAPIVtbl* _pif, const CAPI_BufList_t* pBitStream,
                                CAPI_BufList_t* pPcmData, CAPI_Buf_t* b)
{
  capi_pcm_decoder_t* me = (capi_pcm_decoder_t*)_pif;

  int32_t nRes = 0;
  int32_t in_bytes_to_copy = me->m_lDec_input_bytes_per_sample;
  // pDecState->m_lDec_input_bytes_per_sample is set by SetParam Routine.
  // It should be set to 2

  int32_t out_bytes_to_copy = me->m_lDec_output_bytes_per_sample;
  // pDecState->m_lDec_output_bytes_per_sample is set by SetParam Routine.
  // It should be set to 2
  int32_t num_channels = me->m_sNumberOfChannels;
  // pDecState->m_sNumberOfChannels is set by SetParam Routine.
  //It supports Mono and Stereo files

  uint32_t samples_to_copy = me->m_lDec_SamplesToCopy;
  // pDecState->m_lDec_SamplesToCopy is set by SetParam Routine.

  int8_t* poutput_buffer; // Pointer to output buffer
  int8_t* pinput_buffer; // Pointer to input buffer
  int nInpSize;
  uint32_t nOutputSize = 0;


  FARF(HIGH, "In CAPI Process. all parameters are  in_bytes_to_copy = %x ,  num_channels = %d, samples_to_copy =%d", in_bytes_to_copy, num_channels, samples_to_copy);
  /*Make sure the input buffer contains some valid data
   if(pBitStream->pBuf[0].nActualDataLen <= DEC_FRAME_SIZE)
       nRes = SAMPLE_NEED_MORE;


  //Checking if Output buffer is too small and cannot hold a frame.
  if( (pPcmData->pBuf[0].nMaxDataLen - pPcmData->pBuf[0].nActualDataLen) < pBitStream->pBuf[0].nActualDataLen )
      nRes = SAMPLE_BUFFERTOOSMALL_FAILURE;
  */

  if (pBitStream->pBuf[0].nActualDataLen > pPcmData->pBuf[0].nMaxDataLen) nInpSize = pPcmData->pBuf[0].nMaxDataLen;
  else nInpSize = pBitStream->pBuf[0].nActualDataLen;

  pBitStream->pBuf[0].nActualDataLen -= nInpSize;

  pinput_buffer = (int8_t*)pBitStream->pBuf[0].Data;
  poutput_buffer = (int8_t*)pPcmData->pBuf[0].Data;

  //Calling the Functional Library
  if (nRes != SAMPLE_BUFFERTOOSMALL_FAILURE && nRes != SAMPLE_NEED_MORE) nRes =
       DecodeFrame(samples_to_copy,
                   num_channels,
                   in_bytes_to_copy,
                   out_bytes_to_copy,
                   nInpSize,
                   &nOutputSize,
                   pinput_buffer,
                   poutput_buffer);

  pPcmData->pBuf[0].nActualDataLen += nOutputSize;
  FARF(HIGH, "Reached end of process function Sample Decoder CAPI");

  switch(nRes) {
    case DECODE_SUCCESS :
      return DECODE_SUCCESS;
      break;

    case SAMPLE_NEED_MORE :
      return DEC_NEED_MORE;
      break;

    case SAMPLE_BUFFERTOOSMALL_FAILURE :
      return DEC_BUFFERTOOSMALL_FAILURE;
      break;

    default:
      return DEC_FAILURE;
      break;

  }
}

/* capi_pcm_decoder_destroy is virtual destructor.
 It is called when destructor gets called for ICAPI class
 Destructor is called automatically for class.
 This dummy function handles destructor calling
*/
ADSPResult capi_pcm_decoder_destroy(void)
{
  return DECODE_SUCCESS;
}

/* Vtbl definition
*/
static const ICAPIVtbl vtbl_decode_capi =
{
  (int)capi_pcm_decoder_destroy, // Place holder for dummy destructor
  0,
  capi_pcm_decoder_SetParam,
  capi_pcm_decoder_GetParam,
  capi_pcm_decoder_Process,
  capi_pcm_decoder_Init,
  capi_pcm_decoder_ReInit,
  capi_pcm_decoder_End,
};

ADSPResult capi_pcm_decoder_getsize(int32_t format, uint32_t bps,
                                uint32_t* size_ptr)
{
//returning both capi and functional library memory requirement
  *size_ptr = align_to_8_byte(sizeof(capi_pcm_decoder_t)) +
     align_to_8_byte(sizeof(decode_t));

  FARF(HIGH, "In Getsize of Sample Decoder");
  return ADSP_EOK;
}

/*
ctor function is called from Elite framework.
It passed the vtbl pointer to Elite framework.
*/
ADSPResult capi_pcm_decoder_ctor(ICAPI* capi_ptr, uint32_t format, uint32_t bps)
{
  capi_pcm_decoder_t* me = (capi_pcm_decoder_t*)capi_ptr;
  // update vtbl pointer
  FARF(HIGH, "In Constructor of Sample Decoder");
  me->vtbl = &vtbl_decode_capi;

  return ADSP_EOK;
}

