#ifndef CAPI_PCM_DECODER_H
#define CAPI_PCM_DECODER_H
/*==============================================================================
  Copyright (c) 2012-2013 Qualcomm Technologies, Inc.
  All rights reserved. Qualcomm Proprietary and Confidential.
==============================================================================*/

#include "Elite_CAPI.h"

#ifdef __cplusplus
extern "C"
{
#endif

ADSPResult capi_pcm_decoder_getsize(int32_t format, uint32_t bps, uint32_t* size_ptr);

ADSPResult capi_pcm_decoder_ctor(ICAPI* capi_ptr, uint32_t format, uint32_t bps);

#ifdef __cplusplus
}
#endif

#endif /*#ifndef CAPI_PCM_DECODER_H */

