#include "platform_libs_list.h"
#include "platform_libs.h"

PL_DEP(HAP_utils)
PL_DEP(HAP_diag)
PL_DEP(rtld)
PL_DEP(sigverify)

struct platform_lib* (*pl_list[])(void) = {
	PL_ENTRY(HAP_utils),
	PL_ENTRY(HAP_diag),
	PL_ENTRY(rtld),
	PL_ENTRY(sigverify),
	0
};
