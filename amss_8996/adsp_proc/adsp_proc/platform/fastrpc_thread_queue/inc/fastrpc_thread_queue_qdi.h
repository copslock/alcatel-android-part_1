#include "fastrpc_thread_queue.h"
#include "qurt_qdi.h"
#include "qurt_types.h"
#include "qurt_memory.h"

#define QDI_FTQ_NAME                   "/dev/fastrpc-smd"
#define QDI_FTQ_BASE                   256
#define QDI_FTQ_PROCESS_INIT           (QDI_FTQ_BASE + 0)
#define QDI_FTQ_THREAD_INIT            (QDI_FTQ_BASE + 1)
#define QDI_FTQ_THREAD_EXIT            (QDI_FTQ_BASE + 2)
#define QDI_FTQ_INVOKE_RSP             (QDI_FTQ_BASE + 3)
#define QDI_FTQ_INVOKE_REQUEUE         (QDI_FTQ_BASE + 4)
#define QDI_FTQ_INVOKE_RSP_PENDING     (QDI_FTQ_BASE + 5)
#define QDI_FTQ_MEM_ADD                (QDI_FTQ_BASE + 6)
#define QDI_FTQ_POOL_ADD_PAGES         QDI_FTQ_MEM_ADD
#define QDI_FTQ_POOL_REMOVE_PAGES      (QDI_FTQ_BASE + 8)
#define QDI_FTQ_PROCESS_EXIT           (QDI_FTQ_BASE + 10)

static __inline int ftq_qdi_open(void) {
   return qurt_qdi_open(QDI_FTQ_NAME);
}
static __inline int ftq_qdi_close(int handle) {
   return qurt_qdi_close(handle);
}
static __inline int ftq_qdi_process_init(int handle, qurt_thread_t tidQ, char name[255], struct ftq_umsg** pmsg) {
   return qurt_qdi_handle_invoke(handle, QDI_FTQ_PROCESS_INIT, tidQ, name, pmsg);
}
static __inline int ftq_qdi_thread_init(int handle, qurt_thread_t tidQ, int tidA, struct ftq_umsg** pmsg) {
   return qurt_qdi_handle_invoke(handle, QDI_FTQ_THREAD_INIT, tidQ, tidA, pmsg);
}
static __inline int ftq_qdi_thread_exit(int handle, qurt_thread_t tidQ) {
   return qurt_qdi_handle_invoke(handle, QDI_FTQ_THREAD_EXIT, tidQ);
}
static __inline int ftq_qdi_invoke_rsp(int handle, qurt_thread_t tidQ, int ret) {
   return qurt_qdi_handle_invoke(handle, QDI_FTQ_INVOKE_RSP, tidQ, ret);
}
static __inline int ftq_qdi_invoke_requeue(int handle, qurt_thread_t tidQ) {
   return qurt_qdi_handle_invoke(handle, QDI_FTQ_INVOKE_REQUEUE, tidQ);
}
static __inline int ftq_qdi_invoke_rsp_pending(int handle, int tidA, int ret) {
   return qurt_qdi_handle_invoke(handle, QDI_FTQ_INVOKE_RSP_PENDING, tidA, ret);
}
static __inline int ftq_qdi_mem_add(int handle, unsigned addr, unsigned size) {
   return qurt_qdi_handle_invoke(handle, QDI_FTQ_MEM_ADD, addr, size);
}
static __inline int ftq_qdi_pool_add_pages(int handle, qurt_mem_pool_t pool, unsigned addr, unsigned size) {
   return qurt_qdi_handle_invoke(handle, QDI_FTQ_POOL_ADD_PAGES, pool, addr, size);
}
static __inline int ftq_qdi_pool_remove_pages(int handle, qurt_mem_pool_t pool, unsigned addr, unsigned size) {
   return qurt_qdi_handle_invoke(handle, QDI_FTQ_POOL_REMOVE_PAGES, pool, addr, size);
}
static __inline int ftq_qdi_process_exit(int handle) {
   return qurt_qdi_handle_invoke(handle, QDI_FTQ_PROCESS_EXIT);
}

#ifdef QURT_POOL_REMOVE_ALL_OR_NONE

static __inline int ftq_qdi_add_pages(int client_handle, unsigned ppn, unsigned pages, qurt_mem_pool_t pool) {
   return qurt_mem_pool_add_pages(pool, ppn, pages);
}

static __inline int ftq_qdi_remove_pages(qurt_mem_pool_t pool, unsigned ppn, unsigned pages) {
   return qurt_mem_pool_remove_pages(pool, ppn, pages, QURT_POOL_REMOVE_ALL_OR_NONE, 0, 0);
}

#else

static __inline int ftq_qdi_add_pages(int client_handle, unsigned ppn, unsigned pages, qurt_mem_pool_t pool) {
   return qurt_qdi_handle_invoke(client_handle, QDI_OS_MEM_POOL_ADD, ppn, pages);
}

static __inline int ftq_qdi_remove_pages(qurt_mem_pool_t pool, unsigned ppn, unsigned pages) {
   return -1;
}

#endif

/**
 * kernel library functions
 */
int ftq_qdi_group_kcreateu(const char* name, int pidA, int asid, int client_handle);
void ftq_qdi_group_kdestroyu(int client_handle);
