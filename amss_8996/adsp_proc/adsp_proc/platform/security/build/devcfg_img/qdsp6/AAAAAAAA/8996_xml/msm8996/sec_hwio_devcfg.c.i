#pragma clang diagnostic ignored "-Wunused-function"
typedef unsigned char boolean;
typedef unsigned long int uint32;
typedef unsigned short uint16;
typedef unsigned char uint8;
typedef signed long int int32;
typedef signed short int16;
typedef signed char int8;
typedef unsigned char byte;
typedef unsigned short word;
typedef unsigned long dword;
typedef unsigned char uint1;
typedef unsigned short uint2;
typedef unsigned long uint4;
typedef signed char int1;
typedef signed short int2;
typedef long int int4;
typedef signed long sint31;
typedef signed short sint15;
typedef signed char sint7;
typedef uint16 UWord16 ;
typedef uint32 UWord32 ;
typedef int32 Word32 ;
typedef int16 Word16 ;
typedef uint8 UWord8 ;
typedef int8 Word8 ;
typedef int32 Vect32 ;
      typedef long long int64;
      typedef unsigned long long uint64;
typedef struct {
    uint32 serial_number_addr;
    uint32 chip_id_addr;
    uint32 oem_id_addr;
    uint32 reserved1;
    uint32 reserved2;
    uint32 reserved3;
}pl_sechwio_type;
uint32 SecHWIO_GetSerialNumber(void);
uint16 SecHWIO_GetChipID(void);
uint16 SecHWIO_GetOEMID(void);
typedef unsigned long int bool32;
pl_sechwio_type pl_sechwio = {
    ((0xe0270000 + 0x00000000) + 0x00004138),
    ((0xe0270000 + 0x00000000) + 0x0000413c),
    ((0xe0270000 + 0x00000000) + 0x000060f8),
    0,
    0,
    0
};
