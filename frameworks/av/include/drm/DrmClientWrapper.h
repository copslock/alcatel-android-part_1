#ifndef __DRM_CONSTRAINTS_H__
#define __DRM_CONSTRAINTS_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>

#define OPENSESSION_FAILER -1
#define MAX_CASHE_LENGTH 8196

typedef struct DrmWrapperResouce{
    void* mClient;
    void* mHandle;
    char mDrmBuf[MAX_CASHE_LENGTH]; //store 8K data
    long moffset;
    long mBufSize;
}DrmWrapperRes;

bool isDrm(const char* path);

bool isDrmFd(int fd);

bool openDecryptDrmSession(DrmWrapperRes* wrapperRes,long dataSize, int descripter);

long decryptData(DrmWrapperRes* wrapperRes, void* buffer, long dataSize, int offset);

void closeSession(DrmWrapperRes* wrapperRes);

#ifdef __cplusplus
}
#endif

#endif

