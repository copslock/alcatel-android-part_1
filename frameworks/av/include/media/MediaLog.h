#ifndef _MEDIA_LOGS_H_
#define _MEDIA_LOGS_H_

#include <inttypes.h>
#include <utils/Log.h>

namespace android {

/*
 * Change logging-level at runtime with "persist.debug.media.logs.level"
 *
 * level     MediaLOGV      MediaLOGD
 * ----------------------------------
 * 0         silent          silent
 * 1         silent          printed
 * 2         printed         printed
 *
 * MediaLOGI/W/E are printed always
 */

extern uint32_t gMediaLogLevel;

#define MediaLOGV(format, args...) ALOGD_IF((gMediaLogLevel > 1), format, ##args)
#define MediaLOGD(format, args...) ALOGD_IF((gMediaLogLevel > 0), format, ##args)
#define MediaLOGI(format, args...) ALOGI(format, ##args)
#define MediaLOGW(format, args...) ALOGW(format, ##args)
#define MediaLOGE(format, args...) ALOGE(format, ##args)

void updateLogLevel();

} //namespace android

#endif // _MEDIA_LOGS_H_

