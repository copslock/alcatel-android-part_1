#include <stdlib.h>
#include <utils/Log.h>
#include <cutils/properties.h>

#include "media/MediaLog.h"

namespace android {

uint32_t gMediaLogLevel;

void updateLogLevel() {
    char level[PROPERTY_VALUE_MAX];
    property_get("persist.debug.media.logs.level", level, "0");
    gMediaLogLevel = atoi(level);
}

} // namespace android

