/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * @file
 * Time Porting Layer
 *
 * Basic support functions that are needed by time.
 *
 * <!-- #interface list begin -->
 * \section drm_time Interface
 * - DRM_time_getElapsedSecondsFrom1970()
 * - DRM_time_sleep()
 * - DRM_time_getSysTime()
 * <!-- #interface list end -->
 */

#ifndef __DRM_TIME_H__
#define __DRM_TIME_H__
#define TCT_DEBUG_LOG 0
#ifdef TCT_DEBUG_LOG
#define TCTALOGE ALOGE
#define TCTALOGD ALOGD
#define TCTALOGW ALOGW
#else
#define TCTALOGE(a...) do { } while(0)
#define TCTALOGD(a...) do { } while(0)
#define TCTALOGW(a...) do { } while(0)
#endif
#ifdef __cplusplus
extern "C" {
#endif

#include <time.h>
#include <drm_common_types.h>

#define SECURE_TIME_PATH "/data/drm/native_securetime.txt"
#define SECONDS_PER_MINUTE 60                       /* Seconds per minute*/
#define SECONDS_PER_HOUR   60 * SECONDS_PER_MINUTE  /* Seconds per hour */
#define SECONDS_PER_DAY    24 * SECONDS_PER_HOUR    /* Seconds per day */

#define DAY_PER_MONTH 30                    /* Days per month */
#define DAY_PER_YEAR  365                   /* Days per year */

#define leap(y) (((y) % 4 == 0 && (y) % 100 != 0) || (y) % 400 == 0)
#define nleap(y) (((y) - 1969) / 4 - ((y) - 1901) / 100 + ((y) - 1601) / 400)
#define int64_const(s)          (s)
#define int64_add(dst, s1, s2)  ((void)((dst) = (s1) + (s2)))
#define int64_mul(dst, s1, s2)  ((void)((dst) = (int64_t)(s1) * (int64_t)(s2)))

static const int32_t ydays[] = {
    0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334
};

static const int DRM_TIME_VAILD = 1;
static const int DRM_TIME_INVAILD = 0;

/** the time format */
typedef struct __db_system_time_
{
    uint16_t year;
    uint16_t month;
    uint16_t day;
    uint16_t hour;
    uint16_t min;
    uint16_t sec;
} T_DB_TIME_SysTime;

/**
 * Get the system time.it's up to UTC
 * \return Return the time in elapsed seconds.
 */
uint32_t DRM_time_getElapsedSecondsFrom1970(void);

/**
 * Suspend the execution of the current thread for a specified interval
 * \param ms suspended time by millisecond
 */
void DRM_time_sleep(uint32_t ms);

/**
 * function: get current system time
 * \param  time_ptr[OUT]  the system time got
 * \attention
 *    time_ptr must not be NULL
 */
void DRM_time_getSysTime(T_DB_TIME_SysTime *time_ptr);

int checkEndTimeExpired(int32_t a, int32_t b);

int readSecureTimeFromTXT(int *flag, int64_t *delta);

void changeTime(T_DB_TIME_SysTime* timeTm);

//bool checkEndTimeExpired(int32_t checkdate, int32_t checktimea);

#ifdef __cplusplus
}
#endif

#endif /* __DRM_TIME_H__ */
