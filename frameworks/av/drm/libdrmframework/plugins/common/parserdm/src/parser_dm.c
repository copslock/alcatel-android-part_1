/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <parser_dm.h>
//#include <parser_dcf.h>
//#include <svc_drm.h>

#define DRM_SKIP_SPACE_TAB(p) while( (*(p) == ' ') || (*(p) == '\t') ) \
                                  p++

typedef enum _DM_PARSE_STATUS {
    DM_PARSE_START,
    DM_PARSING_RIGHTS,
    DM_PARSING_CONTENT,
    DM_PARSE_END
} DM_PARSE_STATUS;

static int drm_strnicmp(const uint8_t* s1, const uint8_t* s2, int32_t n)
{
    if (n < 0 || NULL == s1 || NULL == s2)
        return -1;

    if (n == 0)
        return 0;

    while (n-- != 0 && tolower(*s1) == tolower(*s2))
    {
        if (n == 0 || *s1 == '\0' || *s2 == '\0')
            break;
        s1++;
        s2++;
    }

    return tolower(*s1) - tolower(*s2);
}

const uint8_t * drm_strnstr(const uint8_t * str, const uint8_t * strSearch, int32_t len)
{
    int32_t i, stringLen;

    if (NULL == str || NULL == strSearch || len <= 0)
        return NULL;

    stringLen = strlen((char *)strSearch);
    for (i = 0; i < len - stringLen + 1; i++) {
        if (str[i] == *strSearch && 0 == memcmp(str + i, strSearch, stringLen))
            return str + i;
    }
    return NULL;
}

/**
 *
 */
int32_t drm_parseDMPart(const uint8_t *buffer, int32_t bufferLen, T_DRM_DM_Info *pDmInfo)
{
    const uint8_t *pStart = NULL, *pEnd = NULL;

    if (NULL == buffer || bufferLen <= 0 || NULL == pDmInfo)
        return FALSE;

    /* Find out the boundary */
    pStart = drm_strnstr(buffer, (uint8_t *) "<o-ex:rights", bufferLen);
    if (NULL == pStart)
        return FALSE; /* No boundary error */
    pEnd = pStart;

    pDmInfo->rightsOffset = pStart - buffer;

    pStart = drm_strnstr(buffer, (uint8_t *) "</o-ex:rights>", bufferLen);
    if (NULL == pStart)
        return FALSE; /* No boundary error */

    pDmInfo->rightsLen = pStart - pEnd + strlen((char *) ("</o-ex:rights>")) + 2;
    return TRUE;
}

