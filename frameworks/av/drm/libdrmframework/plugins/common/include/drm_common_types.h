/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __COMMON_TYPES_H__
#define __COMMON_TYPES_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <assert.h>
#include <ctype.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#define MAX_CONTENT_ID              256

#define TYPE_DRM_MESSAGE            0x48    /**< The mime type is "application/vnd.oma.drm.message" */
#define TYPE_DRM_CONTENT            0x49    /**< The mime type is "application/vnd.oma.drm.content" */
#define TYPE_DRM_RIGHTS_XML         0x4a    /**< The mime type is "application/vnd.oma.drm.rights+xml" */
#define TYPE_DRM_RIGHTS_WBXML       0x4b    /**< The mime type is "application/vnd.oma.drm.rights+wbxml" */
#define TYPE_DRM_UNKNOWN            0xff    /**< The mime type is unknown */

/**
 * Define the return values for those interface.
 */
#define DRM_SUCCESS                 0
#define DRM_FAILURE                 -1
#define DRM_MEDIA_EOF               -2
#define DRM_RIGHTS_DATA_INVALID     -3
#define DRM_MEDIA_DATA_INVALID      -4
#define DRM_SESSION_NOT_OPENED      -5
#define DRM_NO_RIGHTS               -6
#define DRM_NOT_SD_METHOD           -7
#define DRM_RIGHTS_PENDING          -8
#define DRM_RIGHTS_EXPIRED          -9
#define DRM_UNKNOWN_DATA_LEN        -10

/**
 * Define the constraints.
 */
#define DRM_NO_CONSTRAINT           0x80    /**< Indicate have no constraint, it can use freely */
#define DRM_END_TIME_CONSTRAINT     0x08    /**< Indicate have end time constraint */
#define DRM_INTERVAL_CONSTRAINT     0x04    /**< Indicate have interval constraint */
#define DRM_COUNT_CONSTRAINT        0x02    /**< Indicate have count constraint */
#define DRM_START_TIME_CONSTRAINT   0x01    /**< Indicate have start time constraint */
#define DRM_NO_PERMISSION           0x00    /**< Indicate no rights */


/**
 * Define the permissions.
 */
#define DRM_PERMISSION_PLAY         0x01    /**< Play */
#define DRM_PERMISSION_FORWARD      0x03    /**< Forward */
#define DRM_PERMISSION_DISPLAY      0x07    /**< Display */
#define DRM_PERMISSION_EXECUTE      0x06    /**< Execute */
#define DRM_PERMISSION_PRINT        0x08    /**< Print */

/**
 * Define the delivery methods.
 */
#define FORWARD_LOCK                1       /**< Forward_lock */
#define COMBINED_DELIVERY           2       /**< Combined delivery */
#define SEPARATE_DELIVERY           3       /**< Separate delivery */
#define SEPARATE_DELIVERY_FL        4       /**< Separate delivery but DCF is forward-lock */

#define INT_2_YMD_HMS(year, mon, day, date, hour, min, sec, time) do{\
    year = date / 10000;\
    mon = date % 10000 / 100;\
    day = date %100;\
    hour = time / 10000;\
    min = time % 10000 / 100;\
    sec = time % 100;\
}while(0)

#define YMD_HMS_2_INT(year, mon, day, date, hour, min, sec, time) do{\
    date = year * 10000 + mon * 100 + day;\
    time = hour * 10000 + min * 100 + sec;\
}while(0)

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

#define Trace(...)

#ifdef __cplusplus
}
#endif

#endif /* __COMMON_TYPES_H__ */
