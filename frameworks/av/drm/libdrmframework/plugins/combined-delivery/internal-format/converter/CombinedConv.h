/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __COMBINEDCONV_H__
#define __COMBINEDCONV_H__
#define TCT_DEBUG_LOG 0
#ifdef TCT_DEBUG_LOG
#define TCTALOGE ALOGE
#define TCTALOGD ALOGD
#define TCTALOGW ALOGW
#else
#define TCTALOGE(a...) do { } while(0)
#define TCTALOGD(a...) do { } while(0)
#define TCTALOGW(a...) do { } while(0)
#endif
#ifdef __cplusplus
extern "C" {
#endif

#include <sys/types.h>

/**
 * The size of the data and header signatures combined. The signatures are adjacent to each other in
 * the produced output file.
 */
#define COMBINED_DELIVERY_SIGNATURES_SIZE (2 * 20)

/**
 * Data type for the output from CombinedConv_ConvertData.
 */
typedef struct CombinedConv_ConvertData_Output {
    /// The converted data.
    void *pBuffer;

    /// The size of the converted data.
    size_t numBytes;

    /// The file position where the error occurred, in the case of a syntax error.
    off64_t errorPos;
} CombinedConv_ConvertData_Output_t;

/**
 * Data type for the output from CombinedConv_CloseSession.
 */
typedef struct CombinedConv_CloseSession_Output {
    /// The final set of signatures.
    unsigned char signatures[COMBINED_DELIVERY_SIGNATURES_SIZE];

    /// The offset in the produced output file where the signatures are located.
    off64_t fileOffset;

    /// The file position where the error occurred, in the case of a syntax error.
    off64_t errorPos;
} CombinedConv_CloseSession_Output_t;

/**
 * Data type for the output from the conversion process.
 */
typedef union CombinedConv_Output {
    CombinedConv_ConvertData_Output_t fromConvertData;
    CombinedConv_CloseSession_Output_t fromCloseSession;
} CombinedConv_Output_t;

/**
 * Data type for the Posix-style read function used by the converter in pull mode.
 *
 * @param[in] fileDesc The file descriptor of a file opened for reading.
 * @param[out] pBuffer A reference to the buffer that should receive the read data.
 * @param[in] numBytes The number of bytes to read.
 *
 * @return The number of bytes read.
 * @retval -1 Failure.
 */
typedef ssize_t CombinedConv_ReadFunc_t(int fileDesc, void *pBuffer, size_t numBytes);

/**
 * Data type for the Posix-style write function used by the converter in pull mode.
 *
 * @param[in] fileDesc The file descriptor of a file opened for writing.
 * @param[in] pBuffer A reference to the buffer containing the data to be written.
 * @param[in] numBytes The number of bytes to write.
 *
 * @return The number of bytes written.
 * @retval -1 Failure.
 */
typedef ssize_t CombinedConv_WriteFunc_t(int fileDesc, const void *pBuffer, size_t numBytes);

/**
 * Data type for the Posix-style lseek function used by the converter in pull mode.
 *
 * @param[in] fileDesc The file descriptor of a file opened for writing.
 * @param[in] offset The offset with which to update the file position.
 * @param[in] whence One of SEEK_SET, SEEK_CUR, and SEEK_END.
 *
 * @return The new file position.
 * @retval ((off64_t)-1) Failure.
 */
typedef off64_t CombinedConv_LSeekFunc_t(int fileDesc, off64_t offset, int whence);

/**
 * The status codes returned by the converter functions.
 */
typedef enum CombinedConv_Status {
    /// The operation was successful.
    CombinedConv_Status_OK = 0,

    /// An actual argument to the function is invalid (a program error on the caller's part).
    CombinedConv_Status_InvalidArgument = 1,

    /// There is not enough free dynamic memory to complete the operation.
    CombinedConv_Status_OutOfMemory = 2,

    /// An error occurred while opening the input file.
    CombinedConv_Status_FileNotFound = 3,

    /// An error occurred while creating the output file.
    CombinedConv_Status_FileCreationFailed = 4,

    /// An error occurred while reading from the input file.
    CombinedConv_Status_FileReadError = 5,

    /// An error occurred while writing to the output file.
    CombinedConv_Status_FileWriteError = 6,

    /// An error occurred while seeking to a new file position within the output file.
    CombinedConv_Status_FileSeekError = 7,

    /// The input file is not a syntactically correct OMA DRM v1 Forward Lock file.
    CombinedConv_Status_SyntaxError = 8,

    /// Support for this DRM file format has been disabled in the current product configuration.
    CombinedConv_Status_UnsupportedFileFormat = 9,

    /// The content transfer encoding is not one of "binary", "base64", "7bit", or "8bit"
    /// (case-insensitive).
    CombinedConv_Status_UnsupportedContentTransferEncoding = 10,

    /// The generation of a random number failed.
    CombinedConv_Status_RandomNumberGenerationFailed = 11,

    /// Key encryption failed.
    CombinedConv_Status_KeyEncryptionFailed = 12,

    /// The calculation of a keyed hash for integrity protection failed.
    CombinedConv_Status_IntegrityProtectionFailed = 13,

    /// There are too many ongoing sessions for another one to be opened.
    CombinedConv_Status_TooManySessions = 14,

    /// An unexpected error occurred.
    CombinedConv_Status_ProgramError = 15
} CombinedConv_Status_t;

/**
 * Opens a session for converting an OMA DRM v1 Forward Lock file to the internal Forward Lock file
 * format.
 *
 * @param[out] pSessionId The session ID.
 * @param[out] pOutput The output from the conversion process (initialized).
 *
 * @return A status code.
 * @retval CombinedConv_Status_OK
 * @retval CombinedConv_Status_InvalidArgument
 * @retval CombinedConv_Status_TooManySessions
 */
CombinedConv_Status_t CombinedConv_OpenSession(int *pSessionId, CombinedConv_Output_t *pOutput);

/**
 * Supplies the converter with data to convert. The caller is expected to write the converted data
 * to file. Can be called an arbitrary number of times.
 *
 * @param[in] sessionId The session ID.
 * @param[in] pBuffer A reference to a buffer containing the data to convert.
 * @param[in] numBytes The number of bytes to convert.
 * @param[in,out] pOutput The output from the conversion process (allocated/reallocated).
 *
 * @return A status code.
 * @retval CombinedConv_Status_OK
 * @retval CombinedConv_Status_InvalidArgument
 * @retval CombinedConv_Status_OutOfMemory
 * @retval CombinedConv_Status_SyntaxError
 * @retval CombinedConv_Status_UnsupportedFileFormat
 * @retval CombinedConv_Status_UnsupportedContentTransferEncoding
 * @retval CombinedConv_Status_RandomNumberGenerationFailed
 * @retval CombinedConv_Status_KeyEncryptionFailed
 * @retval CombinedConv_Status_DataEncryptionFailed
 */
CombinedConv_Status_t CombinedConv_ConvertData(int sessionId,
                                             const void *pBuffer,
                                             size_t numBytes,
                                             CombinedConv_Output_t *pOutput);

/**
 * Closes a session for converting an OMA DRM v1 Forward Lock file to the internal Forward Lock
 * file format. The caller must update the produced output file at the indicated file offset with
 * the final set of signatures.
 *
 * @param[in] sessionId The session ID.
 * @param[in,out] pOutput The output from the conversion process (deallocated and overwritten).
 *
 * @return A status code.
 * @retval CombinedConv_Status_OK
 * @retval CombinedConv_Status_InvalidArgument
 * @retval CombinedConv_Status_OutOfMemory
 * @retval CombinedConv_Status_IntegrityProtectionFailed
 */
CombinedConv_Status_t CombinedConv_CloseSession(int sessionId, CombinedConv_Output_t *pOutput);

/**
 * Converts an open OMA DRM v1 Forward Lock file to the internal Forward Lock file format in pull
 * mode.
 *
 * @param[in] inputFileDesc The file descriptor of the open input file.
 * @param[in] fpReadFunc A reference to a read function that can operate on the open input file.
 * @param[in] outputFileDesc The file descriptor of the open output file.
 * @param[in] fpWriteFunc A reference to a write function that can operate on the open output file.
 * @param[in] fpLSeekFunc A reference to an lseek function that can operate on the open output file.
 * @param[out] pErrorPos
 *   The file position where the error occurred, in the case of a syntax error. May be NULL.
 *
 * @return A status code.
 * @retval CombinedConv_Status_OK
 * @retval CombinedConv_Status_InvalidArgument
 * @retval CombinedConv_Status_OutOfMemory
 * @retval CombinedConv_Status_FileReadError
 * @retval CombinedConv_Status_FileWriteError
 * @retval CombinedConv_Status_FileSeekError
 * @retval CombinedConv_Status_SyntaxError
 * @retval CombinedConv_Status_UnsupportedFileFormat
 * @retval CombinedConv_Status_UnsupportedContentTransferEncoding
 * @retval CombinedConv_Status_RandomNumberGenerationFailed
 * @retval CombinedConv_Status_KeyEncryptionFailed
 * @retval CombinedConv_Status_DataEncryptionFailed
 * @retval CombinedConv_Status_IntegrityProtectionFailed
 * @retval CombinedConv_Status_TooManySessions
 */
CombinedConv_Status_t CombinedConv_ConvertOpenFile(int inputFileDesc,
                                                 CombinedConv_ReadFunc_t *fpReadFunc,
                                                 int outputFileDesc,
                                                 CombinedConv_WriteFunc_t *fpWriteFunc,
                                                 CombinedConv_LSeekFunc_t *fpLSeekFunc,
                                                 off64_t *pErrorPos);

/**
 * Converts an OMA DRM v1 Forward Lock file to the internal Forward Lock file format in pull mode.
 *
 * @param[in] pInputFilename A reference to the input filename.
 * @param[in] pOutputFilename A reference to the output filename.
 * @param[out] pErrorPos
 *   The file position where the error occurred, in the case of a syntax error. May be NULL.
 *
 * @return A status code.
 * @retval CombinedConv_Status_OK
 * @retval CombinedConv_Status_InvalidArgument
 * @retval CombinedConv_Status_OutOfMemory
 * @retval CombinedConv_Status_FileNotFound
 * @retval CombinedConv_Status_FileCreationFailed
 * @retval CombinedConv_Status_FileReadError
 * @retval CombinedConv_Status_FileWriteError
 * @retval CombinedConv_Status_FileSeekError
 * @retval CombinedConv_Status_SyntaxError
 * @retval CombinedConv_Status_UnsupportedFileFormat
 * @retval CombinedConv_Status_UnsupportedContentTransferEncoding
 * @retval CombinedConv_Status_RandomNumberGenerationFailed
 * @retval CombinedConv_Status_KeyEncryptionFailed
 * @retval CombinedConv_Status_DataEncryptionFailed
 * @retval CombinedConv_Status_IntegrityProtectionFailed
 * @retval CombinedConv_Status_TooManySessions
 */
CombinedConv_Status_t CombinedConv_ConvertFile(const char *pInputFilename,
                                             const char *pOutputFilename,
                                             off64_t *pErrorPos);

#ifdef __cplusplus
}
#endif

#endif // __FWDLOCKCONV_H__
