/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __SEPARATEENGINECONST_H__
#define __SEPARATEENGINECONST_H__

namespace android {

/**
 * Constants for forward Lock Engine used for exposing engine's capabilities.
 */
#define SEPERATE_EXTENSION_DCF           ("DCF")
#define SEPERATE_DOTEXTENSION_DCF        (".dcf")
//#define SEPERATE_MIMETYPE_SD             ("application/x-android-drm-sd")
#define SEPERATE_MIMETYPE_SD             ("application/vnd.oma.drm.content")


#define SEPERATE_DRM_TYPE                ("separate-delivery")
#define SEPERATE_RIGHT_MIME_TYPE_1       ("application/vnd.oma.drm.rights+xml")
#define SEPERATE_RIGHT_MIME_TYPE_2       ("application/vnd.oma.drm.rights+wbxml")

#define SEPERATE_CONTENT_MIMETYPE        ("application/vnd.oma.drm.content")
#define SEPERATE_DESCRIPTION             ("OMA V1 Separate Delivery")

};

#endif /* __SEPERATEENGINECONST_H__ */
