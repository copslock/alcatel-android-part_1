/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __SEPARATEENGINE_H__
#define __SEPARATEENGINE_H__

#include <DrmEngineBase.h>
#include <DrmConstraints.h>
#include <DrmRights.h>
#include <DrmInfo.h>
#include <DrmInfoStatus.h>
#include <DrmConvertedStatus.h>
#include <DrmInfoRequest.h>
#include <DrmSupportInfo.h>
#include <DrmInfoEvent.h>
#include "SessionMap.h"

namespace android {

/**
 * Separate Engine class.
 */

typedef enum SeparateConv_Status {
    /// The operation was successful.
    SeparateConv_Status_OK = 0,

    /// An actual argument to the function is invalid (a program error on the caller's part).
    SeparateConv_Status_InvalidArgument = 1,

    /// There is not enough free dynamic memory to complete the operation.
    SeparateConv_Status_OutOfMemory = 2,

    /// An error occurred while opening the input file.
    SeparateConv_Status_FileNotFound = 3,

    /// An error occurred while creating the output file.
    SeparateConv_Status_FileCreationFailed = 4,

    /// An error occurred while reading from the input file.
    SeparateConv_Status_FileReadError = 5,

    /// An error occurred while writing to the output file.
    SeparateConv_Status_FileWriteError = 6,

    /// An error occurred while seeking to a new file position within the output file.
    SeparateConv_Status_FileSeekError = 7,

    /// The input file is not a syntactically correct OMA DRM v1 Forward Lock file.
    SeparateConv_Status_SyntaxError = 8,

    /// Support for this DRM file format has been disabled in the current product configuration.
    SeparateConv_Status_UnsupportedFileFormat = 9,

    /// The content transfer encoding is not one of "binary", "base64", "7bit", or "8bit"
    /// (case-insensitive).
    SeparateConv_Status_UnsupportedContentTransferEncoding = 10,

    /// The generation of a random number failed.
    SeparateConv_Status_RandomNumberGenerationFailed = 11,

    /// Key encryption failed.
    SeparateConv_Status_KeyEncryptionFailed = 12,

    /// The calculation of a keyed hash for integrity protection failed.
    SeparateConv_Status_IntegrityProtectionFailed = 13,

    /// There are too many ongoing sessions for another one to be opened.
    SeparateConv_Status_TooManySessions = 14,

    /// An unexpected error occurred.
    SeparateConv_Status_ProgramError = 15

} SeparateConv_Status_t;

class SeparateEngine : public android::DrmEngineBase {
public:
    static const String8 CONTENT_TYPE;
    /**
     * The remaining repeat count
     */
    static const String8 CONTENT_UID;

    /**
     * The time before which the protected file can not be played/viewed
     */
    static const String8 ENCRYPTION_METHOD;

    /**
     * The time after which the protected file can not be played/viewed
     */
    static const String8 CONTENT_NAME;

    /**
     * The available time for license
     */
    static const String8 RIGHTS_ISSUER;

    /**
     * The data stream for extended metadata
     */
    static const String8 CONTENT_DESCRIPTION;

    static const String8 CONTENT_VENDOR;

    /**
     * The data stream for extended metadata
     */
    static const String8 ICON_URI;
public:
    SeparateEngine();
    virtual ~SeparateEngine();

protected:
 void printConstraintInfo(T_DRM_Rights_Constraint *constraint);
    int getDatatime(T_DRM_DATETIME *dateTime, int *year, int *month, int *day, int *hour, int *min,
            int *sec);
    void changeDisplayTime(int date, int time, bool isChange, char* sOutput);
    // [drm] jian.gu@tct-nj.com
    bool onIsCountOrIntervalConstraint(const String8& path);

    /**
     * Get constraint information associated with input content
     *
     * @param[in] uniqueId Unique identifier for a session
     * @param[in] path Path of the protected content
     * @param[in] action Actions defined such as,
     *     Action::DEFAULT, Action::PLAY, etc
     * @return DrmConstraints
     *     key-value pairs of constraint are embedded in it
     * @note
     *     In case of error, return NULL
     */
    DrmConstraints* onGetConstraints( int uniqueId, const String8* path, int action);

    DrmConstraints* onGetConstraintsByFd( int uniqueId, int fd, int action);

     /**
     * Get list of constraint information associated with input content
     *
     * @param[in] uniqueId Unique identifier for a session
     * @param[in] path Path of the protected content
     * @param[in] action Actions defined such as,
     *     Action::DEFAULT, Action::PLAY, etc
     * @return DrmConstraints
     *     key-value pairs of constraint are embedded in it
     * @note
     *     In case of error, return NULL
     */
    List<DrmConstraints*>* onGetConstraintsList( int uniqueId, const String8* path, int action);

    /**
     * Get metadata information associated with input content
     *
     * @param[in] uniqueId Unique identifier for a session
     * @param[in] path Path of the protected content
     * @return DrmMetadata
     *         key-value pairs of metadata
     * @note
     *     In case of error, return NULL
     */
    DrmMetadata* onGetMetadata(int uniqueId, const String8* path);

    /**
     * Initialize plug-in
     *
     * @param[in] uniqueId Unique identifier for a session
     * @return status_t
     *     Returns DRM_NO_ERROR for success, DRM_ERROR_UNKNOWN for failure
     */
    status_t onInitialize(int uniqueId);

    /**
     * Register a callback to be invoked when the caller required to
     * receive necessary information
     *
     * @param[in] uniqueId Unique identifier for a session. uniqueId is a random
     *                     number generated in the DRM service. If the DrmManagerClient
     *                     is created in native code, uniqueId will be a number ranged
     *                     from 0x1000 to 0x1fff. If it comes from Java code, the uniqueId
     *                     will be a number ranged from 0x00 to 0xfff. So bit 0x1000 in
     *                     uniqueId could be used in DRM plugins to differentiate native
     *                     OnInfoListener and Java OnInfoListener.
     * @param[in] infoListener Listener
     * @return status_t
     *     Returns DRM_NO_ERROR for success, DRM_ERROR_UNKNOWN for failure
     */
    status_t onSetOnInfoListener(
            int uniqueId, const IDrmEngine::OnInfoListener* infoListener);

    /**
     * Terminate the plug-in
     * and release resource bound to plug-in
     *
     * @param[in] uniqueId Unique identifier for a session
     * @return status_t
     *     Returns DRM_NO_ERROR for success, DRM_ERROR_UNKNOWN for failure
     */
    status_t onTerminate(int uniqueId);

    /**
     * Get whether the given content can be handled by this plugin or not
     *
     * @param[in] uniqueId Unique identifier for a session
     * @param[in] path Path the protected object
     * @return bool
     *     Returns true if this plugin can handle , false in case of not able to handle
     */
    bool onCanHandle(int uniqueId, const String8& path);

    //xiaoqin.zhou@jrdcom.com fr270238
    /**
       * Get whether the given content can be handled by this plugin or not.
       *
       * @param uniqueId Unique identifier for a session
       * @param fd file descriptor
       * @return bool
       *        Returns true if this plugin can handle , false in case of not able to handle
       */
    bool onCanHandle(int uniqueId, const int fd);

    /**
     * Executes given drm information based on its type
     *
     * @param[in] uniqueId Unique identifier for a session
     * @param[in] drmInfo Information needs to be processed
     * @return DrmInfoStatus
     *     instance as a result of processing given input
     */
    DrmInfoStatus* onProcessDrmInfo(int uniqueId, const DrmInfo* drmInfo);

    /**
     * Save DRM rights to specified rights path
     * and make association with content path
     *
     * @param[in] uniqueId Unique identifier for a session
     * @param[in] drmRights DrmRights to be saved
     * @param[in] rightsPath File path where rights to be saved
     * @param[in] contentPath File path where content was saved
     * @return status_t
     *     Returns DRM_NO_ERROR for success, DRM_ERROR_UNKNOWN for failure
     */
    status_t onSaveRights(int uniqueId, const DrmRights& drmRights,
            const String8& rightspath, const String8& contentPath);

    /**
     * Retrieves necessary information for registration, unregistration or rights
     * acquisition information.
     *
     * @param[in] uniqueId Unique identifier for a session
     * @param[in] drmInfoRequest Request information to retrieve drmInfo
     * @return DrmInfo
     *     instance as a result of processing given input
     */
    DrmInfo* onAcquireDrmInfo(int uniqueId, const DrmInfoRequest* drmInforequest);

    /**
     * Retrieves the mime type embedded inside the original content
     *
     * @param[in] uniqueId Unique identifier for a session
     * @param[in] path Path of the protected content
     * @return String8
     *     Returns mime-type of the original content, such as "video/mpeg"
     */
    String8 onGetOriginalMimeType(int uniqueId, const String8& path);
    /**
     * Retrieves the mime type embedded inside the original content
     *
     * @param[in] uniqueId Unique identifier for a session
     * @param[in] path Path of the protected content
     * @param[in] fd descriptor of the protected content as a file source
     * @return String8
     *     Returns mime-type of the original content, such as "video/mpeg"
     */
    String8 onGetOriginalMimeType(int uniqueId, const String8& path,int fd);


    /**
    [pengfei.zhong@jrdcom.com-PR300460]
     * Retrieves the mime type embedded inside the original content
     *
     * @param[in] uniqueId Unique identifier for a session
     * @param[in] fd file descriptor of the protected content
     * @return String8
     *     Returns mime-type of the original content, such as "video/mpeg"
     */
    String8 onGetOriginalMimeType(int uniqueId, int fd);


    /**
     * Retrieves the type of the protected object (content, rights, etc..)
     * using specified path or mimetype. At least one parameter should be non null
     * to retrieve DRM object type
     *
     * @param[in] uniqueId Unique identifier for a session
     * @param[in] path Path of the content or null.
     * @param[in] mimeType Mime type of the content or null.
     * @return type of the DRM content,
     *     such as DrmObjectType::CONTENT, DrmObjectType::RIGHTS_OBJECT
     */
    int onGetDrmObjectType(
            int uniqueId, const String8& path, const String8& mimeType);

    /**
     * Check whether the given content has valid rights or not
     *
     * @param[in] uniqueId Unique identifier for a session
     * @param[in] path Path of the protected content
     * @param[in] action Action to perform (Action::DEFAULT, Action::PLAY, etc)
     * @return the status of the rights for the protected content,
     *     such as RightsStatus::RIGHTS_VALID, RightsStatus::RIGHTS_EXPIRED, etc.
     */
    int onCheckRightsStatus(int uniqueId, const String8& path, int action);

    //xiaoqin.zhou@jrdcom.com fr270238
    /**
       * Check whether the given content has valid rights or not.
       *
       * @param uniqueId Unique identifier for a session
       * @param fd file descriptor
       * @param action Action to perform (Action::DEFAULT, Action::PLAY, etc)
      * @return the status of the rights for the protected content,
      *    such as RightsStatus::RIGHTS_VALID, RightsStatus::RIGHTS_EXPIRED, etc.
      */
    int onCheckRightsStatus(int uniqueId, const int fd, int action);

    /**
     * Consumes the rights for a content.
     * If the reserve parameter is true the rights is reserved until the same
     * application calls this api again with the reserve parameter set to false.
     *
     * @param[in] uniqueId Unique identifier for a session
     * @param[in] decryptHandle Handle for the decryption session
     * @param[in] action Action to perform. (Action::DEFAULT, Action::PLAY, etc)
     * @param[in] reserve True if the rights should be reserved.
     * @return status_t
     *     Returns DRM_NO_ERROR for success, DRM_ERROR_UNKNOWN for failure
     */
    status_t onConsumeRights(int uniqueId, DecryptHandle* decryptHandle,
            int action, bool reserve);

    /**
     * Informs the DRM Engine about the playback actions performed on the DRM files.
     *
     * @param[in] uniqueId Unique identifier for a session
     * @param[in] decryptHandle Handle for the decryption session
     * @param[in] playbackStatus Playback action (Playback::START, Playback::STOP, Playback::PAUSE)
     * @param[in] position Position in the file (in milliseconds) where the start occurs.
     *     Only valid together with Playback::START.
     * @return status_t
     *     Returns DRM_NO_ERROR for success, DRM_ERROR_UNKNOWN for failure
     */
#ifdef USE_64BIT_DRM_API
status_t onSetPlaybackStatus(int uniqueId,
                             DecryptHandle* decryptHandle,
                             int playbackStatus,
                             int64_t position);
#else
status_t onSetPlaybackStatus(int uniqueId,
                             DecryptHandle* decryptHandle,
                             int playbackStatus,
                             int position);
#endif

    /**
     * Validates whether an action on the DRM content is allowed or not.
     *
     * @param[in] uniqueId Unique identifier for a session
     * @param[in] path Path of the protected content
     * @param[in] action Action to validate (Action::PLAY, Action::TRANSFER, etc)
     * @param[in] description Detailed description of the action
     * @return true if the action is allowed.
     */
    bool onValidateAction(int uniqueId, const String8& path,
            int action, const ActionDescription& description);

    /**
     * Removes the rights associated with the given protected content
     *
     * @param[in] uniqueId Unique identifier for a session
     * @param[in] path Path of the protected content
     * @return status_t
     *     Returns DRM_NO_ERROR for success, DRM_ERROR_UNKNOWN for failure
     */
    status_t onRemoveRights(int uniqueId, const String8& path);

    /**
     * Removes all the rights information of each plug-in associated with
     * DRM framework. Will be used in master reset
     *
     * @param[in] uniqueId Unique identifier for a session
     * @return status_t
     *     Returns DRM_NO_ERROR for success, DRM_ERROR_UNKNOWN for failure
     */
    status_t onRemoveAllRights(int uniqueId);

    /**
     * This API is for Forward Lock based DRM scheme.
     * Each time the application tries to download a new DRM file
     * which needs to be converted, then the application has to
     * begin with calling this API.
     *
     * @param[in] uniqueId Unique identifier for a session
     * @param[in] convertId Handle for the convert session
     * @return status_t
     *     Returns DRM_NO_ERROR for success, DRM_ERROR_UNKNOWN for failure
     */
    status_t onOpenConvertSession(int uniqueId, int convertId);

    /**
     * Accepts and converts the input data which is part of DRM file.
     * The resultant converted data and the status is returned in the DrmConvertedInfo
     * object. This method will be called each time there are new block
     * of data received by the application.
     *
     * @param[in] uniqueId Unique identifier for a session
     * @param[in] convertId Handle for the convert session
     * @param[in] inputData Input Data which need to be converted
     * @return Return object contains the status of the data conversion,
     *     the output converted data and offset. In this case the
     *     application will ignore the offset information.
     */
    DrmConvertedStatus* onConvertData(
            int uniqueId, int convertId, const DrmBuffer* inputData);

    /**
     * Informs the Drm Agent when there is no more data which need to be converted
     * or when an error occurs. Upon successful conversion of the complete data,
     * the agent will inform that where the header and body signature
     * should be added. This signature appending is needed to integrity
     * protect the converted file.
     *
     * @param[in] uniqueId Unique identifier for a session
     * @param[in] convertId Handle for the convert session
     * @return Return object contains the status of the data conversion,
     *     the header and body signature data. It also informs
     *     the application on which offset these signature data
     *     should be appended.
     */
    DrmConvertedStatus* onCloseConvertSession(int uniqueId, int convertId);

    /**
     * Returns the information about the Drm Engine capabilities which includes
     * supported MimeTypes and file suffixes.
     *
     * @param[in] uniqueId Unique identifier for a session
     * @return DrmSupportInfo
     *     instance which holds the capabilities of a plug-in
     */
    DrmSupportInfo* onGetSupportInfo(int uniqueId);

    /**
     * Open the decrypt session to decrypt the given protected content
     *
     * @param[in] uniqueId Unique identifier for a session
     * @param[in] decryptHandle Handle for the current decryption session
     * @param[in] fd File descriptor of the protected content to be decrypted
     * @param[in] offset Start position of the content
     * @param[in] length The length of the protected content
     * @return
     *     DRM_ERROR_CANNOT_HANDLE for failure and DRM_NO_ERROR for success
     */
#ifdef USE_64BIT_DRM_API
status_t onOpenDecryptSession(int uniqueId,
                              DecryptHandle* decryptHandle,
                              int fd, off64_t offset, off64_t length);
#else
status_t onOpenDecryptSession(int uniqueId,
                              DecryptHandle* decryptHandle,
                              int fd, int offset, int length);
#endif

    /**
     * Open the decrypt session to decrypt the given protected content
     *
     * @param[in] uniqueId Unique identifier for a session
     * @param[in] decryptHandle Handle for the current decryption session
     * @param[in] uri Path of the protected content to be decrypted
     * @return
     *     DRM_ERROR_CANNOT_HANDLE for failure and DRM_NO_ERROR for success
     */
    status_t onOpenDecryptSession(
            int uniqueId, DecryptHandle* decryptHandle, const char* uri);

    /**
     * Close the decrypt session for the given handle
     *
     * @param[in] uniqueId Unique identifier for a session
     * @param[in] decryptHandle Handle for the decryption session
     * @return status_t
     *     Returns DRM_NO_ERROR for success, DRM_ERROR_UNKNOWN for failure
     */
    status_t onCloseDecryptSession(int uniqueId, DecryptHandle* decryptHandle);

    /**
     * Initialize decryption for the given unit of the protected content
     *
     * @param[in] uniqueId Unique identifier for a session
     * @param[in] decryptId Handle for the decryption session
     * @param[in] decryptUnitId ID Specifies decryption unit, such as track ID
     * @param[in] headerInfo Information for initializing decryption of this decrypUnit
     * @return status_t
     *     Returns DRM_NO_ERROR for success, DRM_ERROR_UNKNOWN for failure
     */
    status_t onInitializeDecryptUnit(int uniqueId, DecryptHandle* decryptHandle,
            int decryptUnitId, const DrmBuffer* headerInfo);

    /**
     * Decrypt the protected content buffers for the given unit
     * This method will be called any number of times, based on number of
     * encrypted streams received from application.
     *
     * @param[in] uniqueId Unique identifier for a session
     * @param[in] decryptId Handle for the decryption session
     * @param[in] decryptUnitId ID Specifies decryption unit, such as track ID
     * @param[in] encBuffer Encrypted data block
     * @param[out] decBuffer Decrypted data block
     * @param[in] IV Optional buffer
     * @return status_t
     *     Returns the error code for this API
     *     DRM_NO_ERROR for success, and one of DRM_ERROR_UNKNOWN, DRM_ERROR_LICENSE_EXPIRED
     *     DRM_ERROR_SESSION_NOT_OPENED, DRM_ERROR_DECRYPT_UNIT_NOT_INITIALIZED,
     *     DRM_ERROR_DECRYPT for failure.
     */
    status_t onDecrypt(int uniqueId, DecryptHandle* decryptHandle, int decryptUnitId,
            const DrmBuffer* encBuffer, DrmBuffer** decBuffer, DrmBuffer* IV);

    /**
     * Finalize decryption for the given unit of the protected content
     *
     * @param[in] uniqueId Unique identifier for a session
     * @param[in] decryptHandle Handle for the decryption session
     * @param[in] decryptUnitId ID Specifies decryption unit, such as track ID
     * @return status_t
     *     Returns DRM_NO_ERROR for success, DRM_ERROR_UNKNOWN for failure
     */
    status_t onFinalizeDecryptUnit(
            int uniqueId, DecryptHandle* decryptHandle, int decryptUnitId);

    /**
     * Reads the specified number of bytes from an open DRM file.
     *
     * @param[in] uniqueId Unique identifier for a session
     * @param[in] decryptHandle Handle for the decryption session
     * @param[out] buffer Reference to the buffer that should receive the read data.
     * @param[in] numBytes Number of bytes to read.
     * @param[in] offset Offset with which to update the file position.
     *
     * @return Number of bytes read. Returns -1 for Failure.
     */
#ifdef USE_64BIT_DRM_API
ssize_t onPread(int uniqueId,
                DecryptHandle* decryptHandle,
                void* buffer,
                ssize_t numBytes,
                off64_t offset);
#else
ssize_t onPread(int uniqueId,
                DecryptHandle* decryptHandle,
                void* buffer,
                ssize_t numBytes,
                off_t offset);
#endif

private:
/**
 * Session Class for Separate Delivery decoder. An object of this class is created
 * for every decoding session.
 */
class DecodeSession {
    public :
        int32_t mSessionID;
        int mFd;

        DecodeSession(int32_t sessionID, int fd) {
            mSessionID = sessionID;
            mFd = fd;
        }

        virtual ~DecodeSession() {}
};

SessionMap<DecodeSession*> decodeSessionMap;

/**
 * Converts the error code from Forward Lock Converter to DrmConvertStatus error code.
 *
 * @param Forward Lock Converter error code
 *
 * @return Status code from DrmConvertStatus.
 */
static int getConvertedStatus(SeparateConv_Status_t status);
};

};

#endif /* __SEPARATEENGINE_H__ */
