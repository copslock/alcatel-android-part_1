/*************************************************************************/
/*                                                               Date:11/2012                                               */
/*                                PRESENTATION                                                                            */
/*                                                                                                                                 */
/*       Copyright 2012 TCL Communication Technology Holdings Limited.                                    */
/*                                                                                                                                 */
/* This material is company confidential, cannot be reproduced in any form                                */
/* without the written permission of TCL Communication Technology Holdings                              */
/* Limited.                                                                                                                       */
/*                                                                                                                                  */
/* ---------------------------------------------------------------------------------*/
/*  Author :  Zonghui.Li                                                                                                       */
/*  Email  :  Zonghui.Li@tcl-mobile.com                                                                                  */
/*  Role   :                                                                                                                        */
/*  Reference documents :                                                                                                   */
/* ----------------------------------------------------------------------------------*/
/*  Comments :                                                                                                                   */
/*  File     :                                                                                                                        */
/*  Labels   :                                                                                                                      */
/* ----------------------------------------------------------------------------------*/
/* =========================================================================*/
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/

#ifndef __DRM_API_H__
#define __DRM_API_H__

#ifdef __cplusplus
extern "C" {
#endif
#include "drm_common_types.h"

#define DRM_KEY_LEN         16
typedef struct _T_DRM_Session_Node {
    int32_t sessionId;
    int32_t inputHandle;
    int32_t mimeType;
    int32_t (*getInputDataLengthFunc)(int32_t inputHandle);
    int32_t (*readInputDataFunc)(int32_t inputHandle, uint8_t* buf, int32_t bufLen);
    int32_t (*seekInputDataFunc)(int32_t inputHandle, int32_t offset);
    int32_t deliveryMethod;
    int32_t transferEncoding;
    uint8_t contentType[64];
    int32_t contentLength;
    int32_t contentOffset;
    uint8_t contentID[256];
    uint8_t* rawContent;
    int32_t rawContentLen;
    int32_t bEndData;
    uint8_t* readBuf;
    int32_t readBufLen;
    int32_t readBufOff;
    void* infoStruct;
    uint8_t keyValue[DRM_KEY_LEN];
    struct _T_DRM_Session_Node* next;
} T_DRM_Session_Node;

/**
 * The input DRM data structure, include DM, DCF, DR, DRC.
 */
typedef struct _T_DRM_Input_Data {
    /**
     * The handle of the input DRM data.
     */
    int32_t inputHandle;

    /**
     * The mime type of the DRM data, if the mime type set to unknown, DRM engine
     * will try to scan the input data to confirm the mime type, but we must say that
     * the scan and check of mime type is not strictly precise.
     */
    int32_t mimeType;

    /**
     * The function to get input data length, this function should be implement by out module,
     * and DRM engine will call-back it.
     *
     * \param inputHandle   The handle of the DRM data.
     *
     * \return
     *      -A positive integer indicate the length of input data.
     *      -0, if some error occurred.
     */
    int32_t (*getInputDataLength)(int32_t inputHandle);

    /**
     * The function to read the input data, this function should be implement by out module,
     * and DRM engine will call-back it.
     *
     * @param inputHandle   The handle of the DRM data.
     * @param buf       The buffer mallocced by DRM engine to save the data.
     * @param bufLen    The length of the buffer.
     *
     * @return
     *      -A positive integer indicate the actually length of byte has been read.
     *      -0, if some error occurred.
     *      -(-1), if reach to the end of the data.
     */
    int32_t (*readInputData)(int32_t inputHandle, uint8_t* buf, int32_t bufLen);

    /**
     * The function to seek the current file pointer, this function should be implement by out module,
     * and DRM engine will call-back it.
     *
     * @param inputHandle   The handle of the DRM data.
     * @param offset    The offset from the start position to be seek.
     *
     * @return
     *      -0, if seek operation success.
     *      -(-1), if seek operation fail.
     */
    int32_t (*seekInputData)(int32_t inputHandle, int32_t offset);
} T_DRM_Input_Data;

T_DRM_Session_Node* newSession(T_DRM_Input_Data data);

void freeSession(T_DRM_Session_Node* s);

T_DRM_Session_Node* getSession(int32_t sessionId);

int32_t addSession(T_DRM_Session_Node* s);

void removeSession(int32_t sessionId);

int32_t getMimeType(const uint8_t *buf, int32_t bufLen);
#ifdef __cplusplus
}
#endif

#endif /* __DRM_API_H__ */
