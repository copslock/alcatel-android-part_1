/* Copyright (C) 2016 Tcl Corporation Limited */
/******************************************************************************/
/*                                                               Date:08/2013 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2013 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  jianglong pan                                                   */
/*  Email  :  jianglong.pan@tcl-mobile.com                                    */
/*  Role   :                                                                  */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments :                                                                */
/*  File     : frameworks/tct-ext/                                            */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 07/30/2013|Jianglong Pan         |PR-493060             |CDMA operator name*/
/* ----------|----------------------|----------------------|----------------- */
/* 08/29/2013|xiaming.gu            |PR-516151             |[FT][HK][Roaming] */
/*                                                         | ?????????  */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/

package com.android.internal.telephony.cdma;
import java.util.HashMap;

/**
 * {@hide}
 */
public class TctCdmaMccMncSpnMap {
    private static TctCdmaMccMncSpnMap sTctCdmaMccMncSpnMap;
    private HashMap<Integer, String[]> mMccMncToSpnMap;

    private TctCdmaMccMncSpnMap() {
        mMccMncToSpnMap = new HashMap<Integer, String[]>();
    }

    private void loadEntry(Integer mccmnc, String[] spn) {
        if (!mMccMncToSpnMap.containsKey(mccmnc)) {
            mMccMncToSpnMap.put(mccmnc, spn);
        }
    }

    public String[] getSpnFromMccMnc(int mccmnc) {
        if (mccmnc != 0) {
            return mMccMncToSpnMap.get(mccmnc);
        }
        return null;
    }

    public boolean hasMccMnc(int mccmnc) {
        if (mccmnc != 0) {
            return mMccMncToSpnMap.containsKey(mccmnc);
        }
        return false;
    }

    public static TctCdmaMccMncSpnMap getSingleton() {
        if (sTctCdmaMccMncSpnMap == null) {
            sTctCdmaMccMncSpnMap = new TctCdmaMccMncSpnMap();
            //Name String list of CDMA Operators all over the world

            //China
            sTctCdmaMccMncSpnMap.loadEntry(46003,new String[]{"China Telecom","China Telecom"});

            //[FEATURE]-Add-BEGIN by TCTNJ.(guosheng.yu),10/24/2013, PR-543738,
            //[MNC] Add new MNC: 46011 - APP
            sTctCdmaMccMncSpnMap.loadEntry(46011,new String[]{"China Telecom","China Telecom"});
            //[FEATURE]-Add-END  by TCTNJ.(guosheng.yu)

            //[BUGFIX]-Add-BEGIN by TCTNJ.(guosheng.yu),10/17/2013, PR-534452,
            //[CTCC LE][TC-IRLAB-01039]
            // Macao China Telecom
            sTctCdmaMccMncSpnMap.loadEntry(45502,new String[]{"China Telecom (Macao)","China Telecom (Macao)"});
            //[BUGFIX]-Add-END  by TCTNJ.(guosheng.yu)
            //[FEATURE]-Add-BEGIN by TCTNB.(chuanjun chen), 12/16/2015, Task-1170332, Porting PR-997732.
            sTctCdmaMccMncSpnMap.loadEntry(45507,new String[]{"China Telecom (Macao)","China Telecom (Macao)"});
            //[FEATURE]-Add-END by TCTNB.(chuanjun chen).
            //Taiwan
            sTctCdmaMccMncSpnMap.loadEntry(466005,new String[]{"Asia Pacific Telecom","APTG"});

          //[FEATURE]-Add Begin by TCTNJ(Xiaming.gu), 2013/8/28, FR-516151
            //HongKong
            sTctCdmaMccMncSpnMap.loadEntry(45429,new String[]{"PCCW","PCCW"});
            sTctCdmaMccMncSpnMap.loadEntry(45413,new String[]{"CMHK","CMHK"});
          //[FEATURE]-Add End by TCTNJ(Xiaming.gu), 2013/8/28, FR-516151,
            //US
            sTctCdmaMccMncSpnMap.loadEntry(310059,new String[]{"Cellular One","Cellular One"});
            sTctCdmaMccMncSpnMap.loadEntry(310032,new String[]{"IT&E Overseas","IT&E Wireless"});
            sTctCdmaMccMncSpnMap.loadEntry(310053,new String[]{"Virgin Mobile US","Virgin Mobile US"});
            sTctCdmaMccMncSpnMap.loadEntry(310066,new String[]{"U.S. Cellular","U.S. Cellular"});
            sTctCdmaMccMncSpnMap.loadEntry(310005,new String[]{"Verizon Wireless","Verizon"});
            sTctCdmaMccMncSpnMap.loadEntry(310012,new String[]{"Verizon Wireless","Verizon"});
            sTctCdmaMccMncSpnMap.loadEntry(310016,new String[]{"Cricket Wireless","Cricket Communications"});
            sTctCdmaMccMncSpnMap.loadEntry(310090,new String[]{"Cricket Wireless","Cricket Communications"});
            sTctCdmaMccMncSpnMap.loadEntry(310120,new String[]{"Sprint","Sprint"});
            sTctCdmaMccMncSpnMap.loadEntry(310470,new String[]{"nTelos","nTelos"});
            sTctCdmaMccMncSpnMap.loadEntry(310500,new String[]{"Alltel","Alltel"});
            sTctCdmaMccMncSpnMap.loadEntry(310850,new String[]{"Aeris","Aeris"});
            sTctCdmaMccMncSpnMap.loadEntry(310900,new String[]{"Mid-Rivers Communications","Mid-Rivers Wireless"});
            //sTctCdmaMccMncSpnMap.loadEntry(311000,new String[]{"Mid-Tex Cellular","Mid-Tex Cellular"});
            sTctCdmaMccMncSpnMap.loadEntry(311660,new String[]{"metroPCS","metroPCS"});

            //Others
            sTctCdmaMccMncSpnMap.loadEntry(204436,new String[]{"C Spire Wireless","C Spire Wireless"});
            sTctCdmaMccMncSpnMap.loadEntry(226002,new String[]{"Romtelecom","Romtelecom"});
            sTctCdmaMccMncSpnMap.loadEntry(226004,new String[]{"Cosmote Rom?nia","Cosmote"});
            sTctCdmaMccMncSpnMap.loadEntry(230002,new String[]{"Telefonica Czech Republic","O2"});
            sTctCdmaMccMncSpnMap.loadEntry(230004,new String[]{"MobilKom, a. s.","U:fon"});
            sTctCdmaMccMncSpnMap.loadEntry(240003,new String[]{"Nordisk Mobiltelefon","Ice.net"});
            sTctCdmaMccMncSpnMap.loadEntry(242006,new String[]{"Nordisk Mobiltelefon","Ice"});
            sTctCdmaMccMncSpnMap.loadEntry(247003,new String[]{"Telekom Baltija","TRIATEL"});
            sTctCdmaMccMncSpnMap.loadEntry(250005,new String[]{"Yeniseytelecom","ETK"});
            sTctCdmaMccMncSpnMap.loadEntry(250006,new String[]{"Skylink","Skylink"});
            sTctCdmaMccMncSpnMap.loadEntry(250009,new String[]{"Skylink","Skylink"});
            sTctCdmaMccMncSpnMap.loadEntry(250012,new String[]{"Baykal Westcom","Baykalwestcom"});
            sTctCdmaMccMncSpnMap.loadEntry(255021,new String[]{"PEOPLEnet","PEOPLEnet"});
            sTctCdmaMccMncSpnMap.loadEntry(255001,new String[]{"MTS Ukraine","MTS"});
            sTctCdmaMccMncSpnMap.loadEntry(255023,new String[]{"ITC","CDMA Ukraine "});
            sTctCdmaMccMncSpnMap.loadEntry(255004,new String[]{"Intertelecom","IT"});
            sTctCdmaMccMncSpnMap.loadEntry(257003,new String[]{"BelCel","DIALLOG"});


            sTctCdmaMccMncSpnMap.loadEntry(260003,new String[]{"Orange","Orange"});
            sTctCdmaMccMncSpnMap.loadEntry(260011,new String[]{"Nordisk Polska","Nordisk Polska"});
            sTctCdmaMccMncSpnMap.loadEntry(268021,new String[]{"Zapp Portugal","Zapp"});
            sTctCdmaMccMncSpnMap.loadEntry(282005,new String[]{"Silknet CDMA","Silknet"});
            sTctCdmaMccMncSpnMap.loadEntry(282003,new String[]{"Magtifix","MagtiCom"});
            sTctCdmaMccMncSpnMap.loadEntry(302361,new String[]{"Telus Mobility","Telus"});
            sTctCdmaMccMncSpnMap.loadEntry(302653,new String[]{"Telus Mobility","Telus"});
            sTctCdmaMccMncSpnMap.loadEntry(302657,new String[]{"Telus Mobility","Telus"});
            sTctCdmaMccMncSpnMap.loadEntry(302656,new String[]{"TBay","TBay"});
            sTctCdmaMccMncSpnMap.loadEntry(302680,new String[]{"SaskTel Mobility","SaskTel"});
            sTctCdmaMccMncSpnMap.loadEntry(302703,new String[]{"New Tel Mobility","New Tel Mobility"});
            sTctCdmaMccMncSpnMap.loadEntry(302655,new String[]{"MTS Mobility","MTS"});
            sTctCdmaMccMncSpnMap.loadEntry(302702,new String[]{"MT&T Mobility","MT&T Mobility"});
            sTctCdmaMccMncSpnMap.loadEntry(302701,new String[]{"MB Tel Mobility ","MB Tel Mobility "});
            sTctCdmaMccMncSpnMap.loadEntry(302640,new String[]{"Bell Mobility","Bell"});
            sTctCdmaMccMncSpnMap.loadEntry(302652,new String[]{"BC Tel Mobility ","BC Tel Mobility "});


            sTctCdmaMccMncSpnMap.loadEntry(334030,new String[]{"Movistar","movistar"});
            sTctCdmaMccMncSpnMap.loadEntry(334040,new String[]{"Iusacell","Iusacell"});
            sTctCdmaMccMncSpnMap.loadEntry(338050,new String[]{"Digicel","Digicel"});
            sTctCdmaMccMncSpnMap.loadEntry(362095,new String[]{"E.O.C.G. Wireless","MIO"});
            sTctCdmaMccMncSpnMap.loadEntry(370004,new String[]{"Trilogy Dominicana, S.A.","Viva"});
            sTctCdmaMccMncSpnMap.loadEntry(370002,new String[]{"Claro","Claro"});
            sTctCdmaMccMncSpnMap.loadEntry(400003,new String[]{"CATEL","FONEX"});
            sTctCdmaMccMncSpnMap.loadEntry(401008,new String[]{"Kazakhtelecom","Kazakhtelecom"});
            sTctCdmaMccMncSpnMap.loadEntry(401007,new String[]{"Dalacom","Dalacom"});
            sTctCdmaMccMncSpnMap.loadEntry(405025,new String[]{"TATA Teleservice","TATA Teleservice"});
            sTctCdmaMccMncSpnMap.loadEntry(405026,new String[]{"TATA Teleservice","TATA Teleservice"});
            sTctCdmaMccMncSpnMap.loadEntry(405027,new String[]{"TATA Teleservice","TATA Teleservice"});
            sTctCdmaMccMncSpnMap.loadEntry(405029,new String[]{"TATA Teleservice","TATA Teleservice"});
            sTctCdmaMccMncSpnMap.loadEntry(405030,new String[]{"TATA Teleservice","TATA Teleservice"});
            sTctCdmaMccMncSpnMap.loadEntry(405031,new String[]{"TATA Teleservice","TATA Teleservice"});
            sTctCdmaMccMncSpnMap.loadEntry(405032,new String[]{"TATA Teleservice","TATA Teleservice"});
            sTctCdmaMccMncSpnMap.loadEntry(405033,new String[]{"TATA Teleservice","TATA Teleservice"});
            sTctCdmaMccMncSpnMap.loadEntry(405034,new String[]{"TATA Teleservice","TATA Teleservice"});
            sTctCdmaMccMncSpnMap.loadEntry(405035,new String[]{"TATA Teleservice","TATA Teleservice"});
            sTctCdmaMccMncSpnMap.loadEntry(405036,new String[]{"TATA Teleservice","TATA Teleservice"});
            sTctCdmaMccMncSpnMap.loadEntry(405037,new String[]{"TATA Teleservice","TATA Teleservice"});
            sTctCdmaMccMncSpnMap.loadEntry(405038,new String[]{"TATA Teleservice","TATA Teleservice"});
            sTctCdmaMccMncSpnMap.loadEntry(405039,new String[]{"TATA Teleservice","TATA Teleservice"});
            sTctCdmaMccMncSpnMap.loadEntry(405041,new String[]{"TATA Teleservice","TATA Teleservice"});
            sTctCdmaMccMncSpnMap.loadEntry(405042,new String[]{"TATA Teleservice","TATA Teleservice"});
            sTctCdmaMccMncSpnMap.loadEntry(405043,new String[]{"TATA Teleservice","TATA Teleservice"});
            sTctCdmaMccMncSpnMap.loadEntry(405044,new String[]{"TATA Teleservice","TATA Teleservice"});
            sTctCdmaMccMncSpnMap.loadEntry(405045,new String[]{"TATA Teleservice","TATA Teleservice"});
            sTctCdmaMccMncSpnMap.loadEntry(405046,new String[]{"TATA Teleservice","TATA Teleservice"});
            sTctCdmaMccMncSpnMap.loadEntry(405047,new String[]{"TATA Teleservice","TATA Teleservice"});
            sTctCdmaMccMncSpnMap.loadEntry(418092,new String[]{"Omnnea Wireless","Omnnea"});
            sTctCdmaMccMncSpnMap.loadEntry(418062,new String[]{"Itisaluna","Itisaluna"});
            sTctCdmaMccMncSpnMap.loadEntry(421003,new String[]{"Yemen Mobile","Yemen Mobile"});
            sTctCdmaMccMncSpnMap.loadEntry(425016,new String[]{"Rami Levy","Rami Levy"});
            sTctCdmaMccMncSpnMap.loadEntry(425003,new String[]{"Pelephone","Pelephone"});
            sTctCdmaMccMncSpnMap.loadEntry(428091,new String[]{"Skytel LLC","Skytel"});
            sTctCdmaMccMncSpnMap.loadEntry(428098,new String[]{"G-Mobile LLC","G.Mobile"});
            sTctCdmaMccMncSpnMap.loadEntry(429003,new String[]{"Nepal Telecom","Sky/C-Phone"});
            sTctCdmaMccMncSpnMap.loadEntry(434006,new String[]{"Perfectum Mobile","Perfectum Mobile"});
            sTctCdmaMccMncSpnMap.loadEntry(437003,new String[]{"Aktel Ltd","Fonex"});
            sTctCdmaMccMncSpnMap.loadEntry(440007,new String[]{"KDDI Corporation","KDDI"});
            sTctCdmaMccMncSpnMap.loadEntry(440008,new String[]{"KDDI Corporation","KDDI"});
            sTctCdmaMccMncSpnMap.loadEntry(440070,new String[]{"KDDI Corporation","au"});
            sTctCdmaMccMncSpnMap.loadEntry(450005,new String[]{"SK Telecom","SKTelecom"});
            sTctCdmaMccMncSpnMap.loadEntry(450003,new String[]{"Shinsegi Telecom","Power 017"});
            sTctCdmaMccMncSpnMap.loadEntry(450006,new String[]{"LG Telecom","LG U+"});
            sTctCdmaMccMncSpnMap.loadEntry(450002,new String[]{"KT","KT"});
            sTctCdmaMccMncSpnMap.loadEntry(450004,new String[]{"KT","KT"});
            sTctCdmaMccMncSpnMap.loadEntry(452003,new String[]{"S-Telecom","S-Fone"});
            sTctCdmaMccMncSpnMap.loadEntry(452006,new String[]{"EVNTelecom","EVNTelecom"});
            sTctCdmaMccMncSpnMap.loadEntry(454029,new String[]{"PCCW Limited","PCCW Mobile"});
            sTctCdmaMccMncSpnMap.loadEntry(454005,new String[]{"Hutchison Telecom","3 (CDMA)"});
            sTctCdmaMccMncSpnMap.loadEntry(456003,new String[]{"S Telecom","S Telecom"});
            sTctCdmaMccMncSpnMap.loadEntry(456011,new String[]{"Excell","Excell"});
            sTctCdmaMccMncSpnMap.loadEntry(470005,new String[]{"Citycell","Citycell"});
            sTctCdmaMccMncSpnMap.loadEntry(502001,new String[]{"Telekom Malaysia","ATUR 450"});
            sTctCdmaMccMncSpnMap.loadEntry(502018,new String[]{"Telekom Malaysia","TM Homeline"});
            sTctCdmaMccMncSpnMap.loadEntry(502020,new String[]{"Electcoms Wireless","Electcoms Wireless"});
            sTctCdmaMccMncSpnMap.loadEntry(510007,new String[]{"PT Telkom","TelkomFlexi"});
            sTctCdmaMccMncSpnMap.loadEntry(510009,new String[]{"PT Smart Telecom","SMART"});
            sTctCdmaMccMncSpnMap.loadEntry(510028,new String[]{"PT Mobile-8 Telecom","Fren/Hepi"});
            sTctCdmaMccMncSpnMap.loadEntry(510003,new String[]{"PT Indosat Tbk","StarOne"});
            sTctCdmaMccMncSpnMap.loadEntry(510099,new String[]{"PT Bakrie Telecom","Esia"});
            sTctCdmaMccMncSpnMap.loadEntry(510027,new String[]{"Ceria","Ceria"});
            sTctCdmaMccMncSpnMap.loadEntry(520002,new String[]{"CAT Telecom","CAT CDMA"});
            sTctCdmaMccMncSpnMap.loadEntry(530002,new String[]{"Telecom New Zealand","Telecom"});
            sTctCdmaMccMncSpnMap.loadEntry(606006,new String[]{"Hatef Libya","Hatef Libya"});
            sTctCdmaMccMncSpnMap.loadEntry(617002,new String[]{"MTML","MTML"});
            sTctCdmaMccMncSpnMap.loadEntry(618020,new String[]{"LIBTELCO","LIBTELCO"});
            sTctCdmaMccMncSpnMap.loadEntry(620004,new String[]{"Expresso","Expresso"});
            sTctCdmaMccMncSpnMap.loadEntry(621025,new String[]{"Visafone","Visafone"});
            sTctCdmaMccMncSpnMap.loadEntry(622002,new String[]{"TAWALI","TAWALI"});
            sTctCdmaMccMncSpnMap.loadEntry(634007,new String[]{"Sudatel Group","Sudani One"});
            sTctCdmaMccMncSpnMap.loadEntry(637082,new String[]{"Telcom Somalia","Telcom"});
            sTctCdmaMccMncSpnMap.loadEntry(639007,new String[]{"Telkom Kenya","Orange Kenya"});
            sTctCdmaMccMncSpnMap.loadEntry(640007,new String[]{"TTCL Mobile","TTCL Mobile"});
            sTctCdmaMccMncSpnMap.loadEntry(640006,new String[]{"Dovetel Limited","Sasatel"});
            sTctCdmaMccMncSpnMap.loadEntry(640008,new String[]{"Benson Online","Benson Online"});
            sTctCdmaMccMncSpnMap.loadEntry(649002,new String[]{"Telecom Namibia","switch"});
            sTctCdmaMccMncSpnMap.loadEntry(655013,new String[]{"Neotel","Neotel"});
            sTctCdmaMccMncSpnMap.loadEntry(659007,new String[]{"Sudani","Sudani"});
            sTctCdmaMccMncSpnMap.loadEntry(702099,new String[]{"Smart","Smart"});
            sTctCdmaMccMncSpnMap.loadEntry(704003,new String[]{"movistar","movistar"});
            sTctCdmaMccMncSpnMap.loadEntry(704001,new String[]{"Claro","Claro"});
            sTctCdmaMccMncSpnMap.loadEntry(706004,new String[]{"movistar","movistar"});
            sTctCdmaMccMncSpnMap.loadEntry(708002,new String[]{"Tigo","Tigo"});
            sTctCdmaMccMncSpnMap.loadEntry(716006,new String[]{"Movistar","Movistar"});
            sTctCdmaMccMncSpnMap.loadEntry(734004,new String[]{"movistar","movistar"});
            sTctCdmaMccMncSpnMap.loadEntry(734006,new String[]{"Movilnet","Movilnet"});
            sTctCdmaMccMncSpnMap.loadEntry(740002,new String[]{"CNT Mobile","CNT Mobile"});
            sTctCdmaMccMncSpnMap.loadEntry(901018,new String[]{"AT&T Mobility","Cellular @Sea"});
        }
        return sTctCdmaMccMncSpnMap;
    }
}
