/*
 * Copyright (C) 2008 The Android Open Source Project
 * Copyright (c) 2011-2013, The Linux Foundation. All rights reserved.
 * Not a Contribution.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.internal.telephony;

import android.content.ContentValues;
import android.os.ServiceManager;
import android.telephony.Rlog;

import com.android.internal.telephony.IIccPhoneBook;
import com.android.internal.telephony.uicc.AdnRecord;

import java.lang.ArrayIndexOutOfBoundsException;
import java.lang.NullPointerException;
import java.util.List;

public class UiccPhoneBookController extends IIccPhoneBook.Stub {
    private static final String TAG = "UiccPhoneBookController";
    private Phone[] mPhone;

    /* only one UiccPhoneBookController exists */
    public UiccPhoneBookController(Phone[] phone) {
        if (ServiceManager.getService("simphonebook") == null) {
               ServiceManager.addService("simphonebook", this);
        }
        mPhone = phone;
    }

    @Override
    public boolean
    updateAdnRecordsInEfBySearch (int efid, String oldTag, String oldPhoneNumber,
            String newTag, String newPhoneNumber, String pin2) throws android.os.RemoteException {
        return updateAdnRecordsInEfBySearchForSubscriber(getDefaultSubscription(), efid, oldTag,
                oldPhoneNumber, newTag, newPhoneNumber, pin2);
    }

    @Override
    public boolean
    updateAdnRecordsInEfBySearchForSubscriber(int subId, int efid, String oldTag,
            String oldPhoneNumber, String newTag, String newPhoneNumber,
            String pin2) throws android.os.RemoteException {
        IccPhoneBookInterfaceManager iccPbkIntMgr =
                             getIccPhoneBookInterfaceManager(subId);
        if (iccPbkIntMgr != null) {
            return iccPbkIntMgr.updateAdnRecordsInEfBySearch(efid, oldTag,
                    oldPhoneNumber, newTag, newPhoneNumber, pin2);
        } else {
            Rlog.e(TAG,"updateAdnRecordsInEfBySearch iccPbkIntMgr is" +
                      " null for Subscription:"+subId);
            return false;
        }
    }

    @Override
    public boolean
    updateAdnRecordsInEfByIndex(int efid, String newTag,
            String newPhoneNumber, int index, String pin2) throws android.os.RemoteException {
        return updateAdnRecordsInEfByIndexForSubscriber(getDefaultSubscription(), efid, newTag,
                newPhoneNumber, index, pin2);
    }

    @Override
    public boolean
    updateAdnRecordsInEfByIndexForSubscriber(int subId, int efid, String newTag,
            String newPhoneNumber, int index, String pin2) throws android.os.RemoteException {
        IccPhoneBookInterfaceManager iccPbkIntMgr =
                             getIccPhoneBookInterfaceManager(subId);
        if (iccPbkIntMgr != null) {
            return iccPbkIntMgr.updateAdnRecordsInEfByIndex(efid, newTag,
                    newPhoneNumber, index, pin2);
        } else {
            Rlog.e(TAG,"updateAdnRecordsInEfByIndex iccPbkIntMgr is" +
                      " null for Subscription:"+subId);
            return false;
        }
    }

    @Override
    public int[] getAdnRecordsSize(int efid) throws android.os.RemoteException {
        return getAdnRecordsSizeForSubscriber(getDefaultSubscription(), efid);
    }

    @Override
    public int[]
    getAdnRecordsSizeForSubscriber(int subId, int efid) throws android.os.RemoteException {
        IccPhoneBookInterfaceManager iccPbkIntMgr =
                             getIccPhoneBookInterfaceManager(subId);
        if (iccPbkIntMgr != null) {
            return iccPbkIntMgr.getAdnRecordsSize(efid);
        } else {
            Rlog.e(TAG,"getAdnRecordsSize iccPbkIntMgr is" +
                      " null for Subscription:"+subId);
            return null;
        }
    }

    @Override
    public List<AdnRecord> getAdnRecordsInEf(int efid) throws android.os.RemoteException {
        return getAdnRecordsInEfForSubscriber(getDefaultSubscription(), efid);
    }

    @Override
    public List<AdnRecord> getAdnRecordsInEfForSubscriber(int subId, int efid)
           throws android.os.RemoteException {
        IccPhoneBookInterfaceManager iccPbkIntMgr =
                             getIccPhoneBookInterfaceManager(subId);
        if (iccPbkIntMgr != null) {
            return iccPbkIntMgr.getAdnRecordsInEf(efid);
        } else {
            Rlog.e(TAG,"getAdnRecordsInEf iccPbkIntMgr is" +
                      "null for Subscription:"+subId);
            return null;
        }
    }

    @Override
    public int[] getAdnRecordsCapacity() throws android.os.RemoteException {
        return getAdnRecordsCapacityForSubscriber(getDefaultSubscription());
    }

    @Override
    public int[] getAdnRecordsCapacityForSubscriber(int subId)
           throws android.os.RemoteException {
        IccPhoneBookInterfaceManager iccPbkIntMgr =
                             getIccPhoneBookInterfaceManager(subId);
        if (iccPbkIntMgr != null) {
            return iccPbkIntMgr.getAdnRecordsCapacity();
        } else {
            Rlog.e(TAG,"getAdnRecordsCapacity iccPbkIntMgr is" +
                      " null for Subscription:"+subId);
            return null;
        }
    }

    public boolean
    updateAdnRecordsWithContentValuesInEfBySearch(int efid, ContentValues values,
        String pin2) throws android.os.RemoteException {
            return updateAdnRecordsWithContentValuesInEfBySearchUsingSubId(
                getDefaultSubscription(), efid, values, pin2);
    }

    public boolean
    updateAdnRecordsWithContentValuesInEfBySearchUsingSubId(int subId, int efid,
        ContentValues values, String pin2)
        throws android.os.RemoteException {
        IccPhoneBookInterfaceManager iccPbkIntMgr =
                             getIccPhoneBookInterfaceManager(subId);
        if (iccPbkIntMgr != null) {
            return iccPbkIntMgr.updateAdnRecordsWithContentValuesInEfBySearch(
                efid, values, pin2);
        } else {
            Rlog.e(TAG,"updateAdnRecordsWithContentValuesInEfBySearchUsingSubId " +
                "iccPbkIntMgr is null for Subscription:"+subId);
            return false;
        }
    }

    /**
     * get phone book interface manager object based on subscription.
     **/
    private IccPhoneBookInterfaceManager
            getIccPhoneBookInterfaceManager(int subId) {

        int phoneId = SubscriptionController.getInstance().getPhoneId(subId);
        try {
            return mPhone[phoneId].getIccPhoneBookInterfaceManager();
        } catch (NullPointerException e) {
            Rlog.e(TAG, "Exception is :"+e.toString()+" For subscription :"+subId );
            e.printStackTrace(); //To print stack trace
            return null;
        } catch (ArrayIndexOutOfBoundsException e) {
            Rlog.e(TAG, "Exception is :"+e.toString()+" For subscription :"+subId );
            e.printStackTrace();
            return null;
        }
    }

    private int getDefaultSubscription() {
        return PhoneFactory.getDefaultSubscription();
    }

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/30/2016, SOLUTION-2481203
//SIM Contacts management for telephony support
    public int getAdnCount() throws android.os.RemoteException {
        return getAdnCountUsingSubId(getDefaultSubscription());
    }

    public int getAdnCountUsingSubId(int subId) throws android.os.RemoteException {
        IccPhoneBookInterfaceManager iccPbkIntMgr =
                             getIccPhoneBookInterfaceManager(subId);
        if (iccPbkIntMgr != null) {
            return iccPbkIntMgr.getAdnCount();
        } else {
            Rlog.e(TAG,"getAdnCount iccPbkIntMgr is" +
                      "null for Subscription:" + subId);
            return 0;
        }
    }

    public int getAnrCount() throws android.os.RemoteException {
        return getAnrCountUsingSubId(getDefaultSubscription());
    }

    public int getAnrCountUsingSubId(int subId) throws android.os.RemoteException {
        IccPhoneBookInterfaceManager iccPbkIntMgr =
                             getIccPhoneBookInterfaceManager(subId);
        if (iccPbkIntMgr != null) {
            return iccPbkIntMgr.getAnrCount();
        } else {
            Rlog.e(TAG,"getAnrCount iccPbkIntMgr is" +
                      "null for Subscription:" + subId);
            return 0;
        }
    }

    public int getEmailCount() throws android.os.RemoteException {
        return getEmailCountUsingSubId(getDefaultSubscription());
    }

    public int getEmailCountUsingSubId(int subId) throws android.os.RemoteException {
        IccPhoneBookInterfaceManager iccPbkIntMgr =
                             getIccPhoneBookInterfaceManager(subId);
        if (iccPbkIntMgr != null) {
            return iccPbkIntMgr.getEmailCount();
        } else {
            Rlog.e(TAG,"getEmailCount iccPbkIntMgr is" +
                      "null for Subscription:" + subId);
            return 0;
        }
    }

    public int getSpareAnrCount() throws android.os.RemoteException {
        return getSpareAnrCountUsingSubId(getDefaultSubscription());
    }

    public int getSpareAnrCountUsingSubId(int subId) throws android.os.RemoteException {
        IccPhoneBookInterfaceManager iccPbkIntMgr =
                             getIccPhoneBookInterfaceManager(subId);
        if (iccPbkIntMgr != null) {
            return iccPbkIntMgr.getSpareAnrCount();
        } else {
            Rlog.e(TAG,"getSpareAnrCount iccPbkIntMgr is" +
                      "null for Subscription:" + subId);
            return 0;
        }
    }

    public int getSpareEmailCount() throws android.os.RemoteException {
        return getSpareEmailCountUsingSubId(getDefaultSubscription());
    }

    public int getSpareEmailCountUsingSubId(int subId) throws android.os.RemoteException {
        IccPhoneBookInterfaceManager iccPbkIntMgr =
                             getIccPhoneBookInterfaceManager(subId);
        if (iccPbkIntMgr != null) {
            return iccPbkIntMgr.getSpareEmailCount();
        } else {
            Rlog.e(TAG,"getSpareEmailCount iccPbkIntMgr is" +
                      "null for Subscription:" + subId);
            return 0;
        }
    }

    public int getAdnRecordSize() throws android.os.RemoteException {
        return getAdnRecordSizeUsingSubId(getDefaultSubscription());
    }

    public int getAdnRecordSizeUsingSubId(int subId) throws android.os.RemoteException {
        IccPhoneBookInterfaceManager iccPbkIntMgr =
                             getIccPhoneBookInterfaceManager(subId);
        if (iccPbkIntMgr != null) {
            return iccPbkIntMgr.getAdnRecordSize();
        } else {
            Rlog.e(TAG,"getAdnRecordSize iccPbkIntMgr is" +
                      "null for Subscription:"+subId);
            return 0;
        }
    }

    public int getEmailRecordSize() throws android.os.RemoteException {
        return getEmailRecordSizeUsingSubId(getDefaultSubscription());
    }

    public int getEmailRecordSizeUsingSubId(int subId) throws android.os.RemoteException {
        IccPhoneBookInterfaceManager iccPbkIntMgr =
                             getIccPhoneBookInterfaceManager(subId);
        if (iccPbkIntMgr != null) {
            return iccPbkIntMgr.getEmailRecordSize();
        } else {
            Rlog.e(TAG,"getEmailRecordSize iccPbkIntMgr is" +
                      "null for Subscription:"+subId);
            return 0;
        }
    }

    public int getSneRecordSize() throws android.os.RemoteException {
        return getSneRecordSizeUsingSubId(getDefaultSubscription());
    }

    public int getSneRecordSizeUsingSubId(int subId) throws android.os.RemoteException {
        IccPhoneBookInterfaceManager iccPbkIntMgr =
                             getIccPhoneBookInterfaceManager(subId);
        if (iccPbkIntMgr != null) {
            return iccPbkIntMgr.getSneRecordSize();
        } else {
            Rlog.e(TAG,"getSneRecordSize iccPbkIntMgr is" +
                      "null for Subscription:"+subId);
            return 0;
        }
    }

    public int getAnrRecordSize() throws android.os.RemoteException {
        return getAnrRecordSizeUsingSubId(getDefaultSubscription());
    }

    public int getAnrRecordSizeUsingSubId(int subId) throws android.os.RemoteException {
        IccPhoneBookInterfaceManager iccPbkIntMgr =
                             getIccPhoneBookInterfaceManager(subId);
        if (iccPbkIntMgr != null) {
            return iccPbkIntMgr.getAnrRecordSize();
        } else {
            Rlog.e(TAG,"getAnrRecordSize iccPbkIntMgr is" +
                      "null for Subscription:"+subId);
            return 0;
        }
    }

    public int getAnr2RecordSize() throws android.os.RemoteException {
        return getAnr2RecordSizeUsingSubId(getDefaultSubscription());
    }

    public int getAnr2RecordSizeUsingSubId(int subId) throws android.os.RemoteException {
        IccPhoneBookInterfaceManager iccPbkIntMgr =
                             getIccPhoneBookInterfaceManager(subId);
        if (iccPbkIntMgr != null) {
            return iccPbkIntMgr.getAnr2RecordSize();
        } else {
            Rlog.e(TAG,"getAnr2RecordSize iccPbkIntMgr is" +
                      "null for Subscription:"+subId);
            return 0;
        }
    }

    public List<String> getAasList() throws android.os.RemoteException{
        return getAasListUsingSubId(getDefaultSubscription());
    }

    public List<String> getAasListUsingSubId(int subId) throws android.os.RemoteException {
        IccPhoneBookInterfaceManager iccPbkIntMgr =
                             getIccPhoneBookInterfaceManager(subId);
        if (iccPbkIntMgr != null) {
            return iccPbkIntMgr.getAasList();
        } else {
            Rlog.e(TAG,"getAasList iccPbkIntMgr is" +
                      "null for Subscription:" + subId);
            return null;
        }
    }

    public boolean isAdnLoadDone() throws android.os.RemoteException{
        return isAdnLoadDoneUsingSubId(getDefaultSubscription());
    }

    public boolean isAdnLoadDoneUsingSubId(int subId) throws android.os.RemoteException {
        IccPhoneBookInterfaceManager iccPbkIntMgr =
                             getIccPhoneBookInterfaceManager(subId);
        if (iccPbkIntMgr != null) {
            return iccPbkIntMgr.isAdnLoadDone();
        } else {
            Rlog.e(TAG,"isAdnLoadDone iccPbkIntMgr is" +
                      "null for Subscription:" + subId);
            return false;
        }
    }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
}
