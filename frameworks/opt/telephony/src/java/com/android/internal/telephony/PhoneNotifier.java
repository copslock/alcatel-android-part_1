/*
 * Copyright (C) 2006 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.internal.telephony;

import android.telephony.CellInfo;
import android.telephony.VoLteServiceState;

import com.android.ims.wfc.WFC; // MODIFIED by jianglong.pan, 2016-08-17,BUG-2743873
import java.util.List;

/**
 * {@hide}
 */
public interface PhoneNotifier {

    public void notifyPhoneState(Phone sender);

    public void notifyServiceState(Phone sender);

    public void notifyCellLocation(Phone sender);

    public void notifySignalStrength(Phone sender);

    public void notifyMessageWaitingChanged(Phone sender);

    public void notifyCallForwardingChanged(Phone sender);

    /** TODO - reason should never be null */
    public void notifyDataConnection(Phone sender, String reason, String apnType,
            PhoneConstants.DataState state);

    public void notifyDataConnectionFailed(Phone sender, String reason, String apnType);

    public void notifyDataActivity(Phone sender);

    public void notifyOtaspChanged(Phone sender, int otaspMode);

    public void notifyCellInfo(Phone sender, List<CellInfo> cellInfo);

    public void notifyPreciseCallState(Phone sender);

    public void notifyDisconnectCause(int cause, int preciseCause);

    public void notifyPreciseDataConnectionFailed(Phone sender, String reason, String apnType,
            String apn, String failCause);

    public void notifyVoLteServiceStateChanged(Phone sender, VoLteServiceState lteState);

    public void notifyOemHookRawEventForSubscriber(int subId, byte[] rawData);

/* MODIFIED-BEGIN by jianglong.pan, 2016-08-17,BUG-2743873*/
//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/19/2016, SOLUTION-2743874
//Porting WifiCall IR94 Interface which Gapp needeed
    /* { T949479 '''WIFI Calling''' bob.shen 2015-11-19 */
    // added for wifi calling
    @WFC.JrdFeature({WFC.FR_TASK_949479})
    void notifyIMSRegisterStateChanged();
    @WFC.JrdFeature({WFC.FR_TASK_949479})
    void notifyIMSCallHandover(boolean isHandoverSuccess, int srcAccessTech, int targetAccessTech, com.android.ims.ImsReasonInfo reasonInfo);
    /* } T949479 bob.shen 2015-11-19 */

    /*add by fzhang for 1278538   */
    public void notifyVideoCapabilitiesChanged(boolean isVideoCapable);
    /*add by fzhang for 1278538   */

    //Added by yuting-wang@tcl.com for defect 2226136 begin 2016-6-20
    public void notifyIMSCallSrvccCompleted();
    //Added by yuting-wang@tcl.com for defect 2226136 end 2016-6-20
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
/* MODIFIED-END by jianglong.pan,BUG-2743873*/

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/26/2016, SOLUTION-2743874
//Porting WifiCall IR94 Interface which Gapp needeed
    //IR.94 fix for task 2127445, zhi-zhang@tcl.com, 2016/5/12
    public void notifyForVideoCallingStateToHold(int state);
    public void notifyForCallSessionSrvccChange(boolean isImsCall);
    //IR.94 fix for task 2127445, zhi-zhang@tcl.com, 2016/5/12
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
}
