
/* ============================================================================================== */
/*              Modifications on Features list / Changes Request / Problems Report                */
/* ---------------------------------------------------------------------------------------------- */
/*    date           |        author          |           Key          |             comment      */
/* ------------------|----------------------  |------------------------|------------------------- */
/* 08/13/2015        |     wei.huang          |      PR1067379         |FOR IDOL4 SSV PRE         */
/* ------------------|------------------------|------------------------|------------------------- */

//[BUGFIX]-ADD-BEGIN by TSNJ.wei huang 08/13/2015 PR1067379 FOR IDOL4 SSV PRE
#if !defined (JRD_FOR_HOST_BUILD_AAPT)

#include <stdlib.h>
#include <sys/stat.h>
#include <utils/Log.h>
#include <utils/String16.h>
#include <utils/String8.h>
#include <cutils/properties.h>

namespace android {
static const char * movil_pkgs[] = { "android",
                        "com.android.keyguard",
                        "com.android.mms",
                        "com.android.providers.settings",
                        "com.android.dialer",
                        "com.tct.email",
                        "com.android.phone",
                        "com.android.browser",
                        "com.android.settings.notification",
                        "com.android.bluetooth",
                        "com.android.providers.downloads",
                        "android.media",
                        NULL };

static const char * telefonica_pkgs[] = { "android",
                             "com.android.browser",
                             "com.android.providers.settings",
                             "com.android.mms",
                             "com.android.calendar",
                             "com.android.dialer",
                             "com.tct.email",
                             "com.android.phone",
                             "com.android.settings.notification",
                             "com.android.gallery3d",
                             "com.android.bluetooth",
                             "com.android.providers.downloads",
                             "android.media",
                             NULL };
static const char * tmobile_pkgs[] = { "com.android.bluetooth",
                 "android",
                 "com.android.mms",
                 "com.android.phone",
                 "com.android.dialer",
                 "com.tct.email",
                 "com.android.browser",
                 "com.tct.launcher",
                 "com.android.settings",
                 "com.android.providers.settings",
                 "com.example.testmccmnc",
                 NULL};

#define ARRAY_SIZE(a) (sizeof((a))/sizeof((a)[0]))

enum SsvOperator { TMO = 0, TEF, AMV, OPTR_NUMS };

struct SsvPackage {
    SsvPackage(const char* _pkgName, SsvPackage* _next) : pkgName(_pkgName), next(_next) {}
    String16 pkgName;
    SsvPackage *next;
};

struct OperatorParam {
    bool operator_enable;
    char const *operator_name;
    char **operator_pkgs;
    int operator_pkgs_size;
};

struct SsvConfig {
    int which_operator;

    int mcc;
    int mnc;
    //int spn;

    bool debug_ssv;
    bool enable_ssv;
    bool valid_prev_mccmnc;
    bool ssvInited;
    //bool spn_to_mnc;
    struct SsvPackage* pkgList;
};

static OperatorParam g_OperatorParams[] = {
    { false, "TMO", (char**)&tmobile_pkgs, (ARRAY_SIZE(tmobile_pkgs)) -1 },
    { false, "TEF", (char**)&telefonica_pkgs,  (ARRAY_SIZE(telefonica_pkgs)) -1 },
    { false, "AMV",  (char**)&movil_pkgs, (ARRAY_SIZE(movil_pkgs)) -1 },
};

static SsvConfig gSsvConfig = { -1, 0, 0, /*0,*/ false, false, false, false, /*false,*/ NULL};

static void initSSV() {
    if(!(gSsvConfig.ssvInited)) {
        char ssvprop[PROPERTY_VALUE_MAX];

        property_get("persist.ssv.debug", ssvprop, "false");
        gSsvConfig.debug_ssv = (strcmp(ssvprop, "true") == 0);

        property_get("ro.ssv.enabled", ssvprop, "false");
        gSsvConfig.enable_ssv = (strcmp(ssvprop, "true") == 0);

        //property_get("persist.sys.lang.mccmnc", ssvprop, "0");

        int len = strlen(ssvprop);
        if(len >= 3 && len < 7) {
            gSsvConfig.mnc = atoi(&(ssvprop[3]));
            ssvprop[3] = '\0';
            gSsvConfig.mcc = atoi(ssvprop);
        }
        gSsvConfig.valid_prev_mccmnc = (gSsvConfig.mcc != 0 || gSsvConfig.mnc != 0);

        //property_get("ro.ssv.operator.choose",ssvprop, "unknown");
        int operator_size = ARRAY_SIZE(g_OperatorParams);
        for(int i = 0; i < operator_size; i ++) {
            char const * operator_name = g_OperatorParams[i].operator_name;
            if(strcmp(operator_name, ssvprop) == 0) {
                g_OperatorParams[i].operator_enable = true;
                gSsvConfig.which_operator = i;
                break;
            }
        }

        if(gSsvConfig.which_operator != -1) {
            int pkg_size = g_OperatorParams[gSsvConfig.which_operator].operator_pkgs_size;
            for(int i = 0; i < pkg_size; i ++) {
                char* pkg_name = g_OperatorParams[gSsvConfig.which_operator].operator_pkgs[i];
                if(pkg_name != NULL) {
                    SsvPackage* t = new SsvPackage(pkg_name, gSsvConfig.pkgList);
                    if(t != NULL) {
                        gSsvConfig.pkgList = t;
                    } else {
                        if(gSsvConfig.debug_ssv) ALOGW(" new SsvPackage, index: %d failed", i);
                    }
                }
            }
        }

        ALOGW("enable_ssv:[%s], debug_ssv:[%s], operator:[%s], mccmnc:[%d,%d], package_length:[%d]",
                        (gSsvConfig.enable_ssv?"true":"false"),
                        (gSsvConfig.debug_ssv?"true" : "false"),
                        (gSsvConfig.which_operator == -1 ? "unknown" : g_OperatorParams[gSsvConfig.which_operator].operator_name),
                        gSsvConfig.mcc,
                        gSsvConfig.mnc,
                        (gSsvConfig.which_operator == -1 ? 0 : g_OperatorParams[gSsvConfig.which_operator].operator_pkgs_size));

        gSsvConfig.ssvInited = true;
    }
}

bool mapMccMnc(const String16 & pkgName, int* mappedMcc, int* mappedMnc) {

    if(!(gSsvConfig.ssvInited)) {
        initSSV();
    }

    if(gSsvConfig.enable_ssv)
    {
        if (gSsvConfig.valid_prev_mccmnc && gSsvConfig.pkgList != NULL)
        {
            for (SsvPackage* cur = gSsvConfig.pkgList; cur != NULL; cur = cur->next)
            {
                //if(debug_ssv)ALOGW("compare, %s,%s", String8(packageGroup->name).string(), String8(cur->mPackageName).string());
                if (cur != NULL && cur->pkgName == pkgName)
                {
                    *mappedMcc = gSsvConfig.mcc;
                    *mappedMnc = gSsvConfig.mnc;
                    return true;
                }
            }
        }
    }
    return false;
}
} // namespace
#endif
//[BUGFIX]-ADD-END by TSNJ.wei huang 08/13/2015 PR1067379 FOR IDOL4 SSV PRE
