package com.tct.drm;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Iterator;

import android.drm.DrmUtils;
import android.webkit.MimeTypeMap;

/**
 * {@hide}
 */
public class TctDrmUtils {
    /**
     * @hide
     */
    public static final int MILLI_SECOND = 1000;
    /**
     * @hide
     */
    public static final String TAG = "TctDrmUtils";

    /**
     * @hide
     */
    public static void removeFile(String str) throws IOException {
        DrmUtils.tctRemoveFile(str);
    }

    /**
     * from the type, we can get the extension. TODO use and simple method
     *
     * @param type string
     * @return String extension.
     * @hide
     */
    public static String getExtensionFromMime(String type) {
        if (type == null) {
            TctDrmLog.e(TAG, "the mimeType should not be null");
            return null;
        }
        String extension = MimeTypeMap.getSingleton().getExtensionFromMimeType(type);

        if (type.startsWith("audio/mp3") || type.startsWith("video/mp3")
                || type.startsWith("audio/mpeg")) {
            extension = new String("mp3");
        }
        if (type.startsWith("audio/mp4") || type.startsWith("video/mp4")) {
            extension = new String("mp4");
        }

        if (type.startsWith("audio/3gpp") || type.startsWith("video/3gpp")) {
            extension = new String("3gp");
        }

        if (type.startsWith("audio/aac") || type.startsWith("video/aac")) {
            extension = new String("aac");
        }
        if (type.startsWith("audio/mp4a-latm")
                || type.startsWith("video/mp4a-latm")) {
            extension = new String("aac");
        }
        if (type.startsWith("audio/amr-wb")) {
            extension = new String("amr");
        }
        if (type.startsWith("video/mpeg4")) {
            extension = new String("mp4");
        }
        if (type.startsWith("image/jpeg")) {
            extension = new String("jpg");
        }
        return extension;
    }

    /**
     * convert interval date. TODO use and simple method
     *
     * @param strDate string date
     * @return int date.
     * @hide
     */
    public static long convertStrDateToIntDate(String strDate) {
        final int YEAR = 365 * 30 * 24 * 60 * 60;
        final int MONTH = 30 * 24 * 60 * 60;
        final int DAY = 24 * 60 * 60;
        final int HOUR = 60 * 60;
        final int MINUTE = 60;
        final int SECOND = 1;
        final int OFFSET = 1;
        final int DATE_LENGTH = 19;
        long integerDate = 0;

        if (strDate == null) {
            return 0;
        }
        if (strDate.indexOf('-') == -1) {
            try {
                integerDate = Long.parseLong(strDate);
                return integerDate;
            } catch (NumberFormatException e) {
                e.printStackTrace();
                integerDate = 0;
            }
        }
        if (strDate.length() != DATE_LENGTH) {
            return 0;
        }
        int countNumber = 6;
        long[] date = new long[countNumber];
        int head = 0;
        int tail = 4;
        for (int i = 0; i < countNumber; i++) {
            String subStr = strDate.substring(head, tail);
            try {
                date[i] = Integer.parseInt(subStr);
            } catch (NumberFormatException e) {
                e.printStackTrace();
                return 0;
            }
            head = tail + OFFSET;
            tail = tail + OFFSET + OFFSET + OFFSET;
        }
        TctDrmLog.d("drmmediaplay", "year:" + date[0] + ", month:" + date[1]
                + ", day:" + date[2] + ", hour:" + date[3] + ", minute:"
                + date[4] + ", second:" + date[5]);

        integerDate = date[0] * YEAR + date[1] * MONTH + date[2] * DAY
                + date[3] * HOUR + date[4] * MINUTE + date[5] * SECOND;

        return integerDate;
    }

    /**
     * Defines actions that can be performed on rights-protected content.
     * @hide
     */
    public static class Action {
        /**
         * The default action.
         */
        public static final int DEFAULT = 0x00;
        /**
         * The rights-protected content can be played.
         */
        public static final int PLAY = 0x01;
        /**
         * The rights-protected content can be set as a ringtone.
         */
        public static final int RINGTONE = 0x02;
        /**
         * The rights-protected content can be transferred.
         */
        public static final int TRANSFER = 0x03;
        /**
         * The rights-protected content can be set as output.
         */
        public static final int OUTPUT = 0x04;
        /**
         * The rights-protected content can be previewed.
         */
        public static final int PREVIEW = 0x05;
        /**
         * The rights-protected content can be executed.
         */
        public static final int EXECUTE = 0x06;
        /**
         * The rights-protected content can be displayed.
         */
        public static final int DISPLAY = 0x07;

        public static boolean isValid(int action) {
            boolean isValid = false;
            switch (action) {
                case DEFAULT:
                case PLAY:
                case RINGTONE:
                case TRANSFER:
                case OUTPUT:
                case PREVIEW:
                case EXECUTE:
                case DISPLAY:
                    isValid = true;
            }
            return isValid;
        }
    }
}
