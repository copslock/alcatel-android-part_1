package com.tct.drm.api;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.drm.DrmManagerClient;
import android.drm.DrmStore;
import android.drm.DrmUtils;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.graphics.Movie;
import android.graphics.Matrix;
import android.os.SystemProperties;
import android.os.ParcelFileDescriptor;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import com.tct.drm.TctDrmManagerClient;

/**
 * The main programming interface for the Generic Application. An Generic application must
 * be enable LOCAL_PREBUILT_STATIC_JAVA_LIBRARIES flags in Generic app.
 * instantiate this class to access DRM agents through the DRM framework.
 * NOTE:
 *     Please DO NOT modify this file
 *     Please use jar library under you APP directory ONLY
 */
public class TctDrmManager {
    /** The ID to designate non DRM-protected content. */
    public static final int DRM_SCHEME_NOT_PROTECTED = 0;
    /** The ID to designate OMA1 Forward Locked DRM content. */
    public static final int DRM_SCHEME_OMA1_FL = 1;
    /** The ID to designate OMA1 Combined Delivery DRM content. */
    public static final int DRM_SCHEME_OMA1_CD = 2;
    /** The ID to designate OMA1 Separate Delivery DRM content. */
    public static final int DRM_SCHEME_OMA1_SD = 3;
    /** The "application/vnd.oma.drm.message" MIME type. */
    public static final String DRM_MIMETYPE_MESSAGE_STRING = "application/vnd.oma.drm.message";
    /** The "application/vnd.oma.drm.rights+xml" MIME type. */
    public static final String DRM_MIMETYPE_RIGHTS_XML_STRING = "application/vnd.oma.drm.rights+xml";
    /** The "application/vnd.oma.drm.rights+wbxml" MIME type. */
    public static final String DRM_MIMETYPE_RIGHTS_WBXML_STRING = "application/vnd.oma.drm.rights+wbxml";
    /** The "application/vnd.oma.drm.content" MIME type. */
    public static final String DRM_MIMETYPE_CONTENT_STRING = "application/vnd.oma.drm.content";
    /** The "application/vnd.oma.drm.dcf" MIME type. */
    public static final String DRM_MIMETYPE_DCF_STRING = "application/vnd.oma.drm.dcf";
    /** The "application/vnd.oma.dd+xml" MIME type. */
    public static final String DRM_MIMETYPE_DD_STRING = "application/vnd.oma.dd+xml";

    /**
     * for store the drm wallpaper's path.
     * */
    public static final String NEW_WALLPAPER_DRMPATH = "new_wallpaper_drmpath";
    /**
     * for store the current wallpaper's path.
     * */
    public static final String CURR_WALLPAPER_DRMPATH = "curr_wallpaper_drmpath";

    public static final String TCT_IS_DRM = "tct_is_drm";

    public static final String TCT_DRM_TYPE = "tct_drm_type";

    public static final String TCT_DRM_RIGHT_TYPE = "tct_drm_right_type";

    public static final String TCT_DRM_VALID = "tct_drm_valid";

    public final static String DRM_TIME_OUT_ACTION = "drm_time_out_action";

    public static final String RIGHTS_ISSUER = "rights_issuer";
    public static final String CONSTRAINT_TYPE = "constraint_type";
    public static final String CONTENT_VENDOR = "content_vendor";
    /**The ID to designate non Constraint*/
    public static final int NO_CONSTRAINT = 0;
    /**The ID to designate count Constraint*/
    public static final int COUNT_CONSTRAINT = 1;
    /**The ID to designate interval Constraint*/
    public static final int INTERVAL_CONSTRAINT = 2;
    /**The ID to designate times Constraint*/
    public static final int TIMES_CONSTRAINT = 2;
    /**
     * The digital rights are valid.
     */
    public static final int RIGHTS_VALID = 0x00;
    /**
     * The digital rights are invalid.
     */
    public static final int RIGHTS_INVALID = 0x01;
    /**
     * The digital rights have expired.
     */
    public static final int RIGHTS_EXPIRED = 0x02;
    /**
     * The digital rights have not been acquired for the rights-protected content.
     */
    public static final int RIGHTS_NOT_ACQUIRED = 0x03;

    private static TctDrmManagerClient mTctDrmClient = null;

    private static boolean isTctDrmEnable = TctDrmManagerClient.isDrmEnabled();

    private static Map<String, Integer> drmTypeCaches = new HashMap(1000);
    private static final int DRM_BUFFER_MAX = 500;
    private static boolean DEBUG = false;
    private static final String TAG = "TctDrmManager-API";

    public static final String VERSION = "v1.03";

    /*static{
        if (SystemProperties.get("feature.tct.drm.enabled", "false")
                .equalsIgnoreCase("true")
                || SystemProperties.get("feature.tct.drm.enabled", "0")
                        .equalsIgnoreCase("1")) {
            isTctDrmEnable = true;
        }
    }*/

    public TctDrmManager(Context context) {
        if (mTctDrmClient == null)
            mTctDrmClient = TctDrmManagerClient.getInstance(context);
    }

    /**
     * need enable or disable debug flag for Generic APP.
     */
    public TctDrmManager(Context context,boolean isDebug){
        if (mTctDrmClient == null)
            mTctDrmClient = TctDrmManagerClient.getInstance(context);
         DEBUG = isDebug;
    }

    /**
     * Check support TCT-DRM solution or not
     */
    public static boolean isDrmEnabled() {
        LogD(TAG,"isDrmEnabled() == "+isTctDrmEnable);
        return isTctDrmEnable;
    }
    /**
     * Check support TCT-DRM solution or not
     */
    public static boolean getDrmEnabled() {
        LogD(TAG,"getDrmEnabled() == "+isTctDrmEnable);
        return isTctDrmEnable;
    }
    /**
     * Check the content is DRM or not
     * @param path : the content's absolute path(such as /storage/sdcard0/Download/xxx.jpg)
     * @return
     *       true: DRM content
     *       falst: NOT DRM content
     */
    public boolean isDrm(String path) {
        boolean result = false;
        if(!isTctDrmEnable){
            return result;
        }
        int scheme = getDrmType(path);
        if (scheme == DRM_SCHEME_OMA1_CD
                || scheme == DRM_SCHEME_OMA1_FL
                || scheme == DRM_SCHEME_OMA1_SD) {
            result = true;
        }
        LogD(TAG,"isDrm path="+path+",return="+result);
        return result;
    }

    /**
     * Check the content is DRM or not
     * @param path : the content's absolute path(such as /storage/sdcard0/Download/xxx.jpg)
     * @return
     *       true: DRM content
     *       falst: NOT DRM content
     * NOTE: this medhod just use for when you call @see #isDrm(String) falied(the content is DRM).
     *       use for SELinux(the method start with SELinux)
     */
     public boolean SELinux_isDrm(String path){
         boolean result = false;
         if(!isTctDrmEnable){
             return result;
         }
         if (path == null || TextUtils.isEmpty(path)) {
             throw new IllegalArgumentException("SELinux_isDrm:your filePath DO NOT be empty or NULL");
         }
         if (mTctDrmClient != null) {
             result =  mTctDrmClient.isDrm(path);
         }
         return result;
     }

    /**
     * Check the content is OMA1 Forward Locked DRM content
     * @param filePath : the content's absolute path(such as /storage/sdcard0/Download/xxx.jpg)
     * @return
     *       true: OMA1 Forward Locked DRM content
     *       false: NOT OMA1 Forward Locked DRM content
     */
    public boolean isFLType(String filePath) {
        if(!isTctDrmEnable){
            return false;
        }
        return (getDrmType(filePath) == DRM_SCHEME_OMA1_FL);
    }

    /**
     * Check the content is OMA1 Combined Delivery DRM content
     * @param filePath : the content's absolute path(such as /storage/sdcard0/Download/xxx.jpg)
     * @return
     *       true: OMA1 Combined Delivery DRM content
     *       false: NOT OMA1 Combined Delivery DRM content
     */
    public boolean isCDType(String filePath) {
        if(!isTctDrmEnable){
            return false;
        }
        return (getDrmType(filePath) == DRM_SCHEME_OMA1_CD);
    }

    /**
     * Check the content is OMA1 Separate Delivery DRM content
     * @param path : the content's absolute path(such as /storage/sdcard0/Download/xxx.jpg)
     * @return
     *       true: OMA1 Separate Delivery DRM content
     *       false: NOT OMA1 Separate Delivery DRM content
     */
    public boolean isSDType(String filePath) {
        if(!isTctDrmEnable){
            return false;
        }
        return (getDrmType(filePath) == DRM_SCHEME_OMA1_SD);
    }
    /**
     * Check the content is OMA1 Separate Delivery DRM content
     * @param path : the content's absolute path(such as /storage/sdcard0/Download/xxx.jpg)
     * @return
     *       true: OMA1 Separate Delivery DRM content
     *       false: NOT OMA1 Separate Delivery DRM content
     * @deprecated
     * @see isSDType
     */
    public boolean isSdType(String filePath) {
        return isSDType(filePath);
    }
    /**
     * to obtain the DRM file's type,such as Forward Locked(FL)\Combined Delivery(CD)\Separate Delivery(SD)
     * @param filePath : the content's absolute path(such as /storage/sdcard0/Download/xxx.jpg)
     * @return
     *       DRM_SCHEME_OMA1_FL: OMA1 Forward Locked DRM content
     *       DRM_SCHEME_OMA1_CD: OMA1 Combined Delivery DRM content
     *       DRM_SCHEME_OMA1_SD: OMA1 Separate Delivery DRM content
     */
    public static int getDrmType(String filePath) {
        if(!isTctDrmEnable){
            return DRM_SCHEME_NOT_PROTECTED;
        }
        if (filePath == null || TextUtils.isEmpty(filePath)) {
            throw new IllegalArgumentException("your filePath DO NOT be empty or NULL");
        }

        int mDrmType = DRM_SCHEME_NOT_PROTECTED;
        byte[] data = new byte[DRM_BUFFER_MAX];
        int count = 0;

        FileInputStream fis = null;
        try {
            fis = new FileInputStream(filePath);
            count = fis.read(data);
        } catch (Exception e) {
            // do nothing
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        byte checkFLCD[] = new byte[10];
        for (int i = 0; i < checkFLCD.length; i++) {
            checkFLCD[i] = data[i];
        }

        // the first 5 header are 'f'(0x46) 'l'(0x57) 'w'(0x4c) 'k'(0x4b)
        for (int i = 0; i < checkFLCD.length - 4; i++) {
            if (((checkFLCD[i] ^ 0x46) == 0) && ((checkFLCD[i + 1] ^ 0x57) == 0)
                    && ((checkFLCD[i + 2] ^ 0x4c) == 0) && ((checkFLCD[i + 3] ^ 0x4b) == 0)) {
                // Log.d(TAG, "00 Is TctDrm (FL or CD ) path");
                byte firstByte = data[5];
                int sixth_byte = (firstByte & 0xFF);
                if (sixth_byte == 1) {
                    mDrmType = DRM_SCHEME_OMA1_CD;
                } else if (sixth_byte == 0) {
                    mDrmType = DRM_SCHEME_OMA1_FL;
                }
            }
        }

        if (mDrmType != DRM_SCHEME_NOT_PROTECTED){
            LogD(TAG,"This file "+filePath+" is DRM file (FL/CD) ,mDrmType="+mDrmType);
            return mDrmType;
        }

        // SD drm content is originally encrypted data
        mDrmType = getSDDrmScheme(data, 0, count, filePath);

        return mDrmType;
    }

    private static int getSDDrmScheme(byte[] data, int offset, int length, String filePath) {
        if(!isTctDrmEnable){
            return DRM_SCHEME_NOT_PROTECTED;
        }
        int mDrmType = DRM_SCHEME_NOT_PROTECTED;
        String str = new String(data);
        int start_index = 0;
        // is possible file is sd drm content
        byte second_byte = data[1];
        byte third_byte = data[2];
        int cid_length = (third_byte & 0xFF);
        int ctype_length = (second_byte & 0xFF);

        if (cid_length > 0 && ctype_length > 0) {
            start_index = ctype_length + 2;
        }

        if (start_index != 0 && start_index + 3 < length) {
            byte[] cid_byte_array = {
                    data[start_index + 1],
                    data[start_index + 2], data[start_index + 3]
            };

            String cid_str = new String(cid_byte_array);
            if (cid_str.equals("cid")) {
                if (str.indexOf("Rights-Issuer") != -1
                    || str.indexOf("Encryption-Method") > 0) {
                    LogD(TAG,"This file "+filePath+" is DRM SD file . mDrmType="+mDrmType);
                    mDrmType = DRM_SCHEME_OMA1_SD;
                }
            }
        }
        return mDrmType;
    }

    /**
     * to obtain the DRM file's thumbnails
     * @param bitmap : The Bitmap which decode from MediaMetadataRetriever.retriever.getFrameAtTime(0).
     *                 This param use for get Video's thumbnail,audio/image can set null.
     * @param filePath : the content's absolute path(such as /storage/sdcard0/Download/xxx.jpg)
     * @param mimeType :the content's mimetype
     * @param size :    size of thumbnail you want to get.(default = 48)
     * @return Bitmap: DRM content's thumbnail.
     */
    public static Bitmap getDrmThumbnail(Bitmap bitmap, String filePath, String mimeType, int size) {
        if(!isTctDrmEnable){
            return bitmap;
        }
        Bitmap mBitmap = null;
        if (mTctDrmClient == null) {
            LogE(TAG, "Fail to getDrmThumbnail");
            return mBitmap;
        }

        if (bitmap != null
                && mimeType != null
                && mimeType.startsWith("video")) {// for Video Thumbnail
            LogD(TAG,"bitmap="+bitmap+",filePath="+filePath+",mimeType="+mimeType+",size="+size+",to getVideoThumbnail(video)");
            mBitmap = mTctDrmClient.getDrmVideoThumbnail(bitmap, filePath, size);
        } else if (mimeType != null
                && mimeType.startsWith("image")) {// for image Thumbnail
            // TODO
            LogD(TAG,"bitmap="+bitmap+",filePath="+filePath+",mimeType="+mimeType+",size="+size+",to getDrmRealThumbnail(image)");
            Options myOpts = new Options();
            myOpts.inSampleSize = 4;
            mBitmap = mTctDrmClient.getDrmRealThumbnail(filePath, myOpts, size);
        } else if (mimeType != null
                && mimeType.startsWith("audio")
                || mimeType.startsWith("application/ogg")) {// for audio Thumbnail
            // TODO
            LogD(TAG,"bitmap="+bitmap+",filePath="+filePath+",mimeType="+mimeType+",size="+size+",to getDrmThumbnail(audio)");
            mBitmap = mTctDrmClient.getDrmThumbnail(filePath, size);
        }

        return mBitmap;
    }

    /**
        * @deprecated
        * Please DO NOT use this api.try another getDrmThumbnail(Bitmap bitmap, String filePath, String mimeType, int size)
        */
    public static Bitmap getDrmThumbnail(String path, int size) {
        if(!isTctDrmEnable){
            return null;
        }
        if (mTctDrmClient != null) {
            return mTctDrmClient.getDrmThumbnail(path, size);
        }
        return null;
    }
    /**
     * to obtain the DRM file's can be forward or not
     * @param filePath : the content's absolute path(such as /storage/sdcard0/Download/xxx.jpg)
     * @return
     *       true: can be forward
     *       false: can NOT be forward
     */
    public static boolean isAllowForward(String filePath) {
        boolean canForward = false;
        if(!isTctDrmEnable){
            return true;
        }
        int mDrmType = getDrmType(filePath);
        if ( (mDrmType == DRM_SCHEME_NOT_PROTECTED)
               || (mDrmType == DRM_SCHEME_OMA1_SD) ) {
            canForward = true;
        }
        LogD(TAG,"filePath="+filePath+",isAllowForward="+canForward);
        return canForward;
    }

    /**
     * to obtain the DRM file's can be used(right valid or not)
     * @param filePath : the content's absolute path(such as /storage/sdcard0/Download/xxx.jpg)
     * @return
     *       true: right valid,use normally
     *       false: right invalid ,use un-normally
     */
    public boolean isRightValid(String filePath) {
        if(!isTctDrmEnable){
            return true;
        }
        if (mTctDrmClient != null) {
            boolean isRightValid = false;
            isRightValid =  mTctDrmClient.isRightValid(filePath);
            LogD(TAG,"filepath="+filePath+",isRightValid="+isRightValid);
            return isRightValid;
        }
        return false;
    }

    public static ContentValues getMetadata(String path) {
        if(!isTctDrmEnable){
            return null;
        }
        if (mTctDrmClient != null) {
            return mTctDrmClient.getMetadata(path);
        }
        return null;
    }

    public static ContentValues getConstraints(String path, int action) {
        if(!isTctDrmEnable){
            return null;
        }
        if (mTctDrmClient != null) {
            return mTctDrmClient.getConstraints(path, action);
        }
        return null;
    }

    public static String getSDRightsIssuer(String filePath) {
        if(!isTctDrmEnable){
            return null;
        }
        if (mTctDrmClient != null) {
            return mTctDrmClient.getSdRightsIssuer(filePath);
        }
        return null;
    }

    public static String getSDVendor(String filePath) {
        String sdVendorUrl = "";
        if(!isTctDrmEnable){
            return sdVendorUrl;
        }
        if (getDrmType(filePath) != DRM_SCHEME_OMA1_SD) {
            throw new IllegalArgumentException(" getSdVendor filePath should be Sd filePath");
        }
        ContentValues contentValue = getMetadata(filePath);
        if (contentValue != null) {
            sdVendorUrl = contentValue.getAsString("content_vendor");
        }

        return sdVendorUrl.trim();
    }

    public void activateContent(Context context, String filepath) {
        if(!isTctDrmEnable){
            return ;
        }
        if (mTctDrmClient != null){
            mTctDrmClient.activateContent(context, filepath);
        }
    }

    public static int checkRightTypes(String filePath) {
        int localRightType = 0;
        if(!isTctDrmEnable){
            return localRightType ;
        }
        if (mTctDrmClient != null) {

            if (mTctDrmClient.hasCountConstraint(filePath)){
                localRightType = COUNT_CONSTRAINT;
            }else if (mTctDrmClient.hasIntervalConstraint(filePath)){
                localRightType = INTERVAL_CONSTRAINT;
            }else if (mTctDrmClient.hasTimeConstraint(filePath)) {
                localRightType = TIMES_CONSTRAINT;
            }

        }
        return localRightType;
    }

    public static int checkRightsStatus(String path, int action) {
        int mRightStatsu = RIGHTS_VALID;
        ParcelFileDescriptor pfd = null;
        File mFile = null;

        if(!isTctDrmEnable){
            return mRightStatsu ;
        }

        if (path == null || TextUtils.isEmpty(path)) {
            throw new IllegalArgumentException("your filePath DO NOT be empty or NULL");
        }else{
            mFile = new File(path);
        }

        if( !mFile.exists() ){
            LogE(TAG,path+",NOT found please check");
        }

        try{
            pfd = ParcelFileDescriptor.open(mFile,ParcelFileDescriptor.MODE_READ_ONLY);
        }catch(FileNotFoundException e){
            e.printStackTrace();
        }

        if(pfd != null){
            FileDescriptor fd = pfd.getFileDescriptor();
            if(fd != null){
                LogD(TAG,"checkRightsStatus,path="+path+",fd ="+fd);
                mRightStatsu = mTctDrmClient.checkRightsStatus(fd);
            }

            try{
                pfd.close();
                pfd = null;
                fd = null;
            }catch(IOException e){
                e.printStackTrace();
            }

        }

        LogD(TAG,"checkRightsStatus path="+path+",mRightStatus="+mRightStatsu);
        return mRightStatsu;
    }

    /**
    * @deprecated
    */
    public static Bitmap getDrmVideoThumbnail(Bitmap bitmap, String filePath, int size) {
        if(!isTctDrmEnable){
            return null;
        }
        if (mTctDrmClient != null) {
            return mTctDrmClient.getDrmVideoThumbnail(bitmap, filePath, size);
        }
        return null;
    }

    /**
    * @deprecated
    */
    public static Bitmap getDrmRealThumbnail(String filePath, BitmapFactory.Options options,int size) {
        if(!isTctDrmEnable){
            return null;
        }
        if (mTctDrmClient != null) {
            return mTctDrmClient.getDrmRealThumbnail(filePath, options, size);
        }
        return null;
    }

    public static int getDrmScheme(String filePath) {
        if(!isTctDrmEnable){
            return DRM_SCHEME_NOT_PROTECTED;
        }
        if (mTctDrmClient != null) {
            return mTctDrmClient.getDrmScheme(filePath);
        }
        return DRM_SCHEME_OMA1_FL;
    }

    public  boolean hasCountConstraint(String filePath) {
        if(!isTctDrmEnable){
            return false;
        }

        if (filePath == null || TextUtils.isEmpty(filePath)) {
            throw new IllegalArgumentException("your filePath DO NOT be empty or NULL");
        }

        return mTctDrmClient.hasCountConstraint(filePath);
    }


    public static Movie getMovie(Uri uri,Context context){
        Movie mMovie = null;
        if(!isTctDrmEnable){
            return mMovie;
        }
        mMovie = Movie.decodeFromUri(uri, context);
        LogD(TAG,"getMovie uri="+uri+",context="+context+",mMovie="+mMovie);

        return mMovie;
    }

    public static Movie getMovie(String path){
        Movie mMovie = null;
        if(!isTctDrmEnable){
            return mMovie;
        }
        mMovie = Movie.decodeFromPath(path);
        LogD(TAG,"getMovie path="+path+",mMovie="+mMovie);

        return mMovie;
    }

    public Bitmap getDrmBitmap(String path){
        Bitmap ret = null;
        if(!isTctDrmEnable){
            return ret;
        }
 
        if (path == null || TextUtils.isEmpty(path)) {
            throw new IllegalArgumentException("your filePath DO NOT be empty or NULL");
        }

        ret = BitmapFactory.getDrmThumbnailNotConsume(path,null);
        return ret;
    }
    public Bitmap getDrmBitmap2(String path){
        Bitmap ret = null;
        if(!isTctDrmEnable){
            return ret;
        }
        if (path == null || TextUtils.isEmpty(path)) {
            throw new IllegalArgumentException("your filePath DO NOT be empty or NULL");
        }
        ret = BitmapFactory.getDrmThumbnailNotConsumeIn(path,null);
        return ret;
    }

    private static void LogD(String TAG, String message) {
        if (DEBUG) {
            Log.d(TAG, message);
        }
    }

    private static void LogI(String TAG, String message) {
        if (DEBUG) {
            Log.i(TAG, message);
        }
    }

    private static void LogE(String TAG, String message) {
        if (true) {
            Log.e(TAG, message);
        }
    }
}
