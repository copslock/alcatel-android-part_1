package com.tct.drm;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.provider.MediaStore;

/**
 * {@hide}
 */
public class DrmUri {
    public static String convertUriToPath(Uri uri, Context context) {
        String path = null;
        if (null != uri) {
            String scheme = uri.getScheme();
            if (null == scheme || scheme.equals("")
                    || scheme.equals(ContentResolver.SCHEME_FILE)) {
                path = uri.getPath();
            } else if (scheme.equals("http")) {
                path = uri.toString();
            } else if (scheme.equals(ContentResolver.SCHEME_CONTENT)) {
                String[] projection = new String[] { MediaStore.MediaColumns.DATA };
                Cursor cursor = null;
                try {
                    cursor = context.getContentResolver().query(uri,
                            projection, null, null, null);
                    if (null == cursor || 0 == cursor.getCount() || !cursor.moveToFirst()) {
                        TctDrmLog.e(Uri.class.getSimpleName(), "Given Uri could not be found" + "in media store");
                        return null;
                    }
                    int pathIndex = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                    path = cursor.getString(pathIndex);
                } catch (SQLiteException e) {
                    TctDrmLog.e(Uri.class.getSimpleName(), "Given Uri is not formatted in a way "
                            + "so that it can be found in media store.");
                    return null;
                } finally {
                    if (null != cursor) {
                        cursor.close();
                    }
                }
            } else {
                TctDrmLog.e(Uri.class.getSimpleName(), "Given Uri scheme is not supported");
                return null;
            }
        }
        return path;
    }
}
