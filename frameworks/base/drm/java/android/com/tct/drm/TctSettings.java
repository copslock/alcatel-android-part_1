package com.tct.drm;

import java.io.File;
import android.content.res.AssetManager;
import android.util.Log;
import android.content.ContentResolver;
import android.content.Context;
import android.drm.DrmManagerClient;
import android.provider.Settings;
import android.content.Intent;
import com.tct.drm.TctDrmStore;
import android.net.Uri;
import android.content.ContentResolver.*;
import android.content.ContentValues;
import android.database.SQLException;
import android.provider.BaseColumns;
import android.provider.MediaStore;

import com.tct.drm.DrmApp;

/**
 * {@hide}
 */
public final class TctSettings {
    private static DrmManagerClient drmClient = null;
    private static final String TAG = "Settings";
    public static final String AUTHORITY = "settings";
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/system");

    public static boolean putString(ContentResolver resolver, String name,
            String value, boolean sendIntent, int userHandle) {
        Log.d(TAG, "name: " + name + "value:" + value);
        Context context = resolver.getContext();
        if (sendIntent
                && TctDrmManagerClient.isDrmEnabled()
                && (name.equals(Settings.System.RINGTONE)
                || name.equals(Settings.System.ALARM_ALERT)
                || name.equals(Settings.System.NOTIFICATION_SOUND))) {
            if (value == null) {
                Intent drmIntent = new Intent(TctDrmStore.ACITON_DRM_RINGTONE_SET);
                drmIntent.putExtra("ringtype", name);
                context.sendBroadcast(drmIntent);
            } else {
                drmClient = new DrmManagerClient(context);
                TctDrmManagerClient mDrmClient = TctDrmManagerClient.getInstance(context);
                Uri uri = Uri.parse(value);
                if (drmClient.canHandle(uri, null)) {
                    if (mDrmClient.canSetasRingtone(uri)) {
                        Intent drmIntent = new Intent(TctDrmStore.ACITON_DRM_RINGTONE_SET);
                        drmIntent.putExtra("ringtype", name);
                        drmIntent.putExtra(name, value);
                        context.sendBroadcast(drmIntent);
                    } else {
                        ContentValues values = new ContentValues(3);
                        values.put(MediaStore.Audio.Media.IS_RINGTONE, 0);
                        values.put(MediaStore.Audio.Media.IS_ALARM, 0);
                        values.put(MediaStore.Audio.Media.IS_NOTIFICATION, 0);
                        try {
                            resolver.update(uri, values, null, null);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        new DrmApp(context).showToast(
                            com.android.internal.R.string.drm_ringtone_with_count_constraint,
                            mDrmClient.convertUriToPath(uri));

                        return false;
                    }
                } else {
                    Intent drmIntent = new Intent(
                            TctDrmStore.ACITON_DRM_RINGTONE_SET);
                    drmIntent.putExtra("ringtype", name);
                    context.sendBroadcast(drmIntent);
                }
            }
        }

        return Settings.putString(resolver, CONTENT_URI, name, value, userHandle);
    }

}
