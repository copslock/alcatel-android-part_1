package com.tct.drm;

import com.tct.drm.TctDrmManagerClient;

import java.io.FileDescriptor;
import android.net.Uri;
import android.util.TctLog;

/**
 * {@hide}
 */
public class DrmBitmapFactory {

    private DrmBitmapFactory() {
    }

    /**
     * @hide
     */
    public static boolean doWithWifidisplay(String path, FileDescriptor fd, Uri uri) {
        return TctDrmManagerClient.doWithWifidisplay(path, fd, uri);
    }

}
