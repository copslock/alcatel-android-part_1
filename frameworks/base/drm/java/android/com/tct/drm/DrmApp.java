package com.tct.drm;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.IContentProvider;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.RemoteException;
import android.provider.MediaStore;
import android.provider.MediaStore.TctDrmMediaColumns.DrmColumns;
import android.text.TextUtils;
import android.widget.Toast;

/**
 * TODO: consider adding HashMap<object, object> for globals globals TODO: hook
 * up other activity classes to DRMApp (besides Gallery and Search)
 *
 * @hide
 */
public class DrmApp {
    private static final String TAG = "DrmApp";
    private final Context mContext;
    private final HandlerThread mHandlerThread = new HandlerThread("AppHandlerThread");
    private final Handler mHandler;
    private boolean mPaused = false;

    private static final String AUTHORITY = "media";
    private static final String CONTENT_AUTHORITY_SLASH = "content://" + AUTHORITY + "/";

    /**
     * @hide
     */
    public DrmApp(Context context) {
        mContext = context;
        mHandlerThread.start();
        mHandler = new Handler(mHandlerThread.getLooper());
    }

    /**
     * @hide
     */
    public Context getContext() {
        return mContext;
    }

    /**
     * @hide
     */
    public Handler getHandler() {
        while (mHandler == null) {
            // Wait till the handler is created.
            ;
        }
        return mHandler;
    }

    /*
     * public ReverseGeocoder getReverseGeocoder() { return mReverseGeocoder; }
     * @hide
     */
    public boolean isPaused() {
        return mPaused;
    }

    /**
     * @hide
     */
    public void onResume() {
        mPaused = false;
    }

    /**
     * @hide
     */
    public void onPause() {
        mPaused = true;
    }

    /**
     * @hide
     */
    public void showToast(final int messageId, final String filePath) {
        TctDrmLog.d(TAG,"showToast int string");
        mHandler.post(new Runnable() {
            public void run() {
                String fileName = "";
                String message = "";
                if (filePath != null) {
                    int index = filePath.lastIndexOf("/");
                    if (index >= 0) {
                        fileName = filePath.substring(index + 1);
                    } else {
                        fileName = filePath;
                    }
                    message = String.format(mContext.getString(messageId),
                            fileName);
                    Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(mContext, messageId, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    /**
     * update Database. When app call {@link DrmApp#showToast(int, String)}
     * means that protected contect was INVALID update
     * {@link DrmColumns.TCT_DRM_VALID} to false
     * 
     * @param filePath
     */
    private void updateDatabase(String filePath) {
        if (mContext != null && null != filePath && !TextUtils.isEmpty(filePath)) {
            IContentProvider mediaProvider = mContext.getContentResolver().acquireProvider("media");
            String where = MediaStore.Files.FileColumns.DATA + "=?";
            Uri uri = Uri.parse(CONTENT_AUTHORITY_SLASH + "external" + "/object");
            String[] whereArgs = new String[] {
                    filePath
            };

            ContentValues values = new ContentValues();
            values.put(DrmColumns.TCT_DRM_VALID, false);

            try {
                TctDrmLog.d(TAG, "updateDatabase filePath,update.");
                mediaProvider.update(mContext.getPackageName(), uri, values, where, whereArgs);
                if (!filePath.startsWith("/storage/sdcard0/Recording")
                        && !filePath.startsWith("/storage/sdcard1/Recording")) {
                    scanPathforMediaStore(filePath);
                }

            } catch (RemoteException e) {
                TctDrmLog.e(TAG, "RemoteException in mediaProvider.update()");
            }
        }
    }

    /**
     * scan Path for new file or folder in MediaStore
     * 
     * @param path the scan path
     * @hide
     */
    public void scanPathforMediaStore(String path) {
        if (mContext != null && !TextUtils.isEmpty(path)) {
            String[] paths = {
                    path
            };
            MediaScannerConnection.scanFile(mContext, paths, null, null);
        }
    }

    /**
     * @hide
     */
    public void showAlertDialog(String title, String message) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(android.R.string.yes, new OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        mHandler.post(new Runnable() {
            public void run() {
                builder.show();
            }
        });
    }

    /**
     * @hide
     */
    public void showAlertDialog(int titleId, int messageId, String filePath) {
        String message = null;
        String title = null;
        String fileName = null;

        if (filePath == null) {
            message = mContext.getString(messageId);
        } else {
            int index = filePath.lastIndexOf("/");
            if (index >= 0) {
                fileName = filePath.substring(index + 1);
            } else {
                fileName = filePath;
            }
            message = String.format(mContext.getString(messageId), fileName);
        }

        if (titleId == 0) {
            titleId = android.R.string.dialog_alert_title;
        }
        title = mContext.getString(titleId);

        final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(android.R.string.yes, new OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        mHandler.post(new Runnable() {
            public void run() {
                builder.show();
            }
        });
    }
}
