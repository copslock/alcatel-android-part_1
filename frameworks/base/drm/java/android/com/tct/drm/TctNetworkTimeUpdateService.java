package com.tct.drm;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.ContentObserver;
import android.drm.DrmManagerClient;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.provider.Settings;
import android.text.format.Time;
import android.util.Log;
import android.util.NtpTrustedTime;
import android.util.TrustedTime;
import android.widget.Toast;
import android.text.format.Time;

import java.io.File;
import com.android.internal.telephony.TelephonyIntents;

/**
 * {@hide}
 */
public final class TctNetworkTimeUpdateService {
    private static final String TAG = "NetworkTimeUpdateService";
    private static final boolean DBG = false;
    private static final long POLLING_INTERVAL_MS = 24L * 60 * 60 * 1000;
    private static final long NOT_SET = -1;
    private static long mNitzTimeSetTime = NOT_SET;
    private static long mLastNtpFetchTime = NOT_SET;
    private static final int TIME_ERROR_THRESHOLD_MS = 5 * 1000;
    private static Context mContext = null;
    private static TrustedTime mTime;

    public TctNetworkTimeUpdateService() {
    }

    public static void setToast(Context context) {
        mTime = NtpTrustedTime.getInstance(context);
    }

    public static void writeTime() {
        TctDrmManagerClient mDrmclient = TctDrmManagerClient.getInstance(null);
        if (null != mDrmclient) {
            mDrmclient.writeSecureTimeToFile(1, 0);
        }
    }

    public static void modifyDrmSecureTime(Context context) {
        final long refTime = SystemClock.elapsedRealtime();
        mTime = NtpTrustedTime.getInstance(context);
        if (mNitzTimeSetTime != NOT_SET && refTime - mNitzTimeSetTime < POLLING_INTERVAL_MS) {
            return;
        }
        if (mTime.getCacheAge() >= POLLING_INTERVAL_MS) {
            return;
        }
        final long currentTime = System.currentTimeMillis();
        final long ntp = mTime.currentTimeMillis();
        long[] attr = { 0, 0 };
        TctDrmManagerClient mDrmclient = TctDrmManagerClient.getInstance(null);
        boolean result = mDrmclient.readSecureTimeFromFile(attr);
        if (DBG) {
            Log.d(TAG, "attr[0] = " + attr[0] + "  attr[1] = " + attr[1]
                    + " Math.abs(ntp - currentTime) = " + Math.abs(ntp - currentTime));
        }
        if (0 == attr[0]) {
            if (Math.abs(ntp - currentTime) > TIME_ERROR_THRESHOLD_MS) {
                mDrmclient.writeSecureTimeToFile(1, ntp - currentTime);
            } else {
                mDrmclient.writeSecureTimeToFile(1, 0);
            }
        } else if (1 == attr[0]) {
            long timeInterval = ntp - currentTime;
            if (Math.abs(attr[1] - timeInterval) >= TIME_ERROR_THRESHOLD_MS) {
                mDrmclient.writeSecureTimeToFile(1, ntp - currentTime);
            }
        }
    }

    public static void setmLastNtpFetchTime() {
        mLastNtpFetchTime = SystemClock.elapsedRealtime();
    }
}
