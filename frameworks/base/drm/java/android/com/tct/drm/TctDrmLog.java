package com.tct.drm;

import android.util.Log;
/**
 * The main programming interface for the TCT-DRM framework's log Output.
 * {@hide}
 */
public final class TctDrmLog{
    private static final boolean TCT_DRM_DEBUG = true;
    private static final String LOCAL_TAG = "TctDrmLog";

    /**
     * @hide
     */
    public static void e(String TAG,String msg){
        if(TCT_DRM_DEBUG)
            Log.e(LOCAL_TAG+"/"+TAG,msg);
    }

    /**
     * @hide
     */
    public static void d(String TAG,String msg){
        if(TCT_DRM_DEBUG)
            Log.d(LOCAL_TAG+"/"+TAG,msg);
    }

    /**
     * @hide
     */
    public static void v(String TAG,String msg){
        if(TCT_DRM_DEBUG)
            Log.v(LOCAL_TAG+"/"+TAG,msg);
    }

    /**
     * @hide
     */
    public static void w(String TAG,String msg){
        if(TCT_DRM_DEBUG)
            Log.w(LOCAL_TAG+"/"+TAG,msg);
    }

    /**
     * @hide
     */
    public static void i(String TAG,String msg){
        if(TCT_DRM_DEBUG)
            Log.i(LOCAL_TAG+"/"+TAG,msg);
    }
}
