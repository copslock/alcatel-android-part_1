package com.android.server;

import android.content.Context;
import android.content.res.XmlResourceParser;
import android.util.TctLog;

import org.xmlpull.v1.XmlPullParser;

import java.util.ArrayList;
import java.util.List;
import java.io.FileInputStream;
import android.util.Xml;
import android.text.TextUtils;

/**
 * Created by wei.huang on 9/9/15.
 */
public class MncMappedXmlParser{
    public static final String TAG = "MncMappedXmlParser";

    public static final String TAG_SIMCARDINFO_LIST = "SimCardList";
    public static final String TAG_SIMCARDINFO = "SimCardInfo";
    public static final String ATTR_OPERATOR = "operator";
    public static final String ATTR_MCC = "mcc";
    public static final String ATTR_MNC = "mnc";
    public static final String ATTR_SPN = "spn";
    public static final String ATTR_MAPPED = "mapped";
    public static final String ATTR_GID = "gid";
    public static final String ATTR_IMSI = "imsi";
    public static final String ATTR_TYPE = "type";
    public static final String ATTR_DESC = "desc";

    public static final String OPERATOR_TMO = "TMO";
    public static final String OPERATOR_AMV = "AMV";
    public static final String OPERATOR_TEF = "TEF";
    public static final String OPERATOR_FOC = "FOC";
    public static final String OPERATOR_GSM = "GSM";

    public static final String xmlResourcePath = "/data/system/ssv_simcard.xml";

    public MncMappedXmlParser(){
    }

    public List<SIMCardInfo> parse(String parse_operator) throws Exception{
        TctLog.d(TAG,"!! PARSE START !!");
        List<SIMCardInfo> simCardInfoList = null;
        SIMCardInfo simCardInfo = null;

        FileInputStream inputStream = new FileInputStream(xmlResourcePath);
        XmlPullParser xml = Xml.newPullParser();
        xml.setInput(inputStream,"utf-8");


        boolean isOperator = false;
        int xmlEventType;
        while ((xmlEventType = xml.next()) != XmlPullParser.END_DOCUMENT){
            switch (xmlEventType){
                case XmlPullParser.START_TAG:
                    TctLog.d(TAG,"- START_TAG - ");
                    if(xml.getName().equals(TAG_SIMCARDINFO_LIST)){
                        TctLog.d(TAG,"CREATE SIMCardInfoList");
                        simCardInfoList = new ArrayList<SIMCardInfo>();
                    } else if(xml.getName().equals(TAG_SIMCARDINFO)){
                        String operator = xml.getAttributeValue(null,ATTR_OPERATOR);
                        TctLog.d(TAG, "SIMCardInfo : " + operator);
                        if(TextUtils.isEmpty(operator) || operator.equals(parse_operator)){
                            isOperator = true;
                            simCardInfo = new SIMCardInfo();
                            simCardInfo.setOperator(operator);
                            TctLog.d(TAG, "CREATE SIMCardInfo & isOpertor = true");
                        }else{
                            isOperator = false;
                        }
                    } else if(xml.getName().equals(ATTR_MCC) && isOperator){
                        xml.next();
                        simCardInfo.setMcc(xml.getText());
                    } else if(xml.getName().equals(ATTR_MNC) && isOperator){
                        xml.next();
                        simCardInfo.setMnc(xml.getText());
                    } else if(xml.getName().equals(ATTR_SPN) && isOperator){
                        xml.next();
                        simCardInfo.setSpn(xml.getText());
                    } else if(xml.getName().equals(ATTR_GID) && isOperator){
                        xml.next();
                        simCardInfo.setGid(xml.getText());
                    } else if(xml.getName().equals(ATTR_MAPPED) && isOperator){
                        xml.next();
                        simCardInfo.setMapped(xml.getText());
                    } else if(xml.getName().equals(ATTR_TYPE) && isOperator){
                        xml.next();
                        simCardInfo.setType(xml.getText());
                    } else if(xml.getName().equals(ATTR_IMSI) && isOperator){
                        xml.next();
                        simCardInfo.setImsi(xml.getText());
                    } else if(xml.getName().equals(ATTR_DESC) && isOperator){
                        xml.next();
                        simCardInfo.setDesc(xml.getText());
                    }
                    break;
                case XmlPullParser.END_TAG:
                    TctLog.d(TAG,"- END_TAG -");
                    if(xml.getName().equals(TAG_SIMCARDINFO) && isOperator){
                        TctLog.d(TAG,"PARSE RESULT : " + simCardInfo.toString());
                        TctLog.d(TAG,"add Simcardinfo to list");
                        simCardInfoList.add(simCardInfo);
                    }
                    break;
            }
        }

        inputStream.close();
        TctLog.d(TAG,"!! PARSE END !!");
        TctLog.d(TAG,"RESULT SIZE = " + simCardInfoList.size());
        return  simCardInfoList;
    }
}
