
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 08/20/2015|     haibin.yu        |        1067379       |SSV Design Idol4  */
/*           |                      |                      |                  */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/
package com.android.server;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
//import android.os.SystemProperties;

class DefaultSIMCardStore implements ISIMCardStore {
    private static final String TAG = "SSV_DefaultSIMCardStore";
    private static final boolean DEBUG_SSV = SsvUtil.DEBUG_SSV;

    public DefaultSIMCardStore() {
    }

    //@override
    public boolean compareOperatorSIMCard(String mappedlhs, String mappedrhs) {
        return mappedlhs.equals(mappedrhs);
    }

    //@override
    public boolean isValidOperatorSIMCard(String mapped) {
        return SsvUtil.isValidMccMnc(mapped);
    }

    //@override
    public String getMappedMccMnc(String mccmnc, String spn) {
        if (SsvUtil.isValidMccMnc(mccmnc))
            return mccmnc;

        return mccmnc;
    }

    //@override
    public String toString() {
        return " DefaultSIMCardStore ";
    }
}