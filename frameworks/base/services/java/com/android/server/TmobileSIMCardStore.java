
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 08/20/2015|     haibin.yu        |        1067379       |SSV Design Idol4  */
/*           |                      |                      |                  */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/
package com.android.server;

import java.io.File;
import java.lang.Exception;
import java.lang.Object;
import java.lang.String;
import java.util.Map;
import java.util.List;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Iterator;

import android.content.res.Resources;
import android.os.SystemProperties;
import android.util.TctLog;
import android.content.Context;


/*
-Germany         262        01            Telekom.de      54656C656B6F6D2E6465FFFFFFFFFFFF  DT Customized UI
-Germany         262        01            T-Mobile D      542D4D6F62696C652044FFFFFFFFFFFF  DT Customized UI
-Germany         262        01            Business        427573696E657373FFFFFFFFFFFFFFFF  DT Customized UI
-Germany         262        01            Privat          507269766174FFFFFFFFFFFFFFFFFFFF  DT Customized UI
-Germany         262        01            congstar        636F6E6773746172FFFFFFFFFFFFFFFF  Congstar UI
-Germany         262        01            congstar.de     636F6E67737461722E6465FFFFFFFFFF  Congstar UI
-Germany         262        01            Other value     Other value                         Open Market UI
-Germany         262        Other value   not applicable  not applicable                      Open Market UI
-
-Austria         232        03            not applicable  not applicable                      DT Customized UI - TMA
-Austria         232        07            not applicable  not applicable                      DT Customized UI - TR
-Austria         232        Other value   not applicable  not applicable                      Open Market UI
-
-Netherlands     204        16            T-Mobile  NL    542D4D6F62696C6520204E4CFFFFFFFF  DT Customized UI
-Netherlands     204        20            T-Mobile  NL    542D4D6F62696C6520204E4CFFFFFFFF  DT Customized UI
-Netherlands     204        16            Ben NL          42656E204E4CFFFFFFFFFFFFFFFFFFFF  Ben UI
-Netherlands     204        16            Other value     Other value                         Open Market UI
-Netherlands     204        Other value   not applicable  not applicable                      Open Market UI
-
-Poland          260        02            T-Mobile.pl     542D4D6F62696C652E706CFFFFFFFFFF  DT Customized UI
-Poland          260        02            T-Mobile.pl Q   542D4D6F62696C652E706C2051FFFFFF  DT Customized UI
-Poland          260        02            heyah           6865796168FFFFFFFFFFFFFFFFFFFFFF  Heyah UI
-Poland          260        02            Other value     Other value                         Open Market UI
-Poland          260        Other value   not applicable  not applicable                      Open Market UI
-
-Czech Republic  230        01            not applicable  not applicable                      DT Customized UI
-Czech Republic  230        Other value   not applicable  not applicable                      Open Market UI
-
-Croatia         219        01            T-Mobile HR     542D4D6F62696C65204852FFFFFFFFFF  DT Customized UI
-Croatia         219        01            HT HR           4854204852FFFFFFFFFFFFFFFFFFFFFF  DT Customized UI
-Croatia         219        01            bonbon          626F6E626F6EFFFFFFFFFFFFFFFFFFFF  Bonbon UI
-Croatia         219        01            Other value     Other value                         Open Market UI
-Croatia         219        Other value   not applicable  not applicable                      Open Market UI
-
-Greece          202        01            not applicable  not applicable                      COSMOTE GR Customized UI
-Greece          202        Other value   not applicable  not applicable                      Open Market UI
-
-Slovakia        231        02            Telekom SK      54656C656B6F6D20534BFFFFFFFFFFFF  DT Customized UI
-Slovakia        231        02            Other value     Other value                         Open Market UI
-Slovakia        231        Other value   not applicable  not applicable                      Open Market UI
-
-Hungary         216        30            Telekom HU      54656C656B6F6D204855FFFFFFFFFFFF  DT Customized UI
-Hungary         216        30            T-Mobile H      542D4D6F62696C652048FFFFFFFFFFFF  DT Customized UI
-Hungary         216        30            Other value     Other value                         Open Market UI
-Hungary         216        Other value   not applicable  not applicable                      Open Market UI
-
-Macedonia       294        01            T-Mobile MK     542D4D6F62696C65204D4BFFFFFFFFFF  DT Customized UI
-Macedonia       294        01            Other value     Other value                         Open Market UI
-Macedonia       294        Other value   not applicable  not applicable                      Open Market UI
-
-Montenegro      297/220    02/04         Telekom.me      54656C656B6F6D2E6D65FFFFFFFFFFFF  DT Customized UI
-Montenegro      297/220    02/04         Other value     Other value                         Open Market UI
-Montenegro      297/220    Other value   not applicable  not applicable                      Open Market UI
-
-Romania         226        03/06         TELEKOM.RO      54454C454B4F4D2E524FFFFFFFFFFFFF  DT Customized UI
-Romania         226        03/06         COSMOTE         434F534D4F5445FFFFFFFFFFFFFFFFFF  DT Customized UI
-Romania         226        03/06         frog            66726F67FFFFFFFFFFFFFFFFFFFFFFFF  DT Customized UI
-Romania         226        03/06         MTV Mobile      4D5456204D6F62696C65FFFFFFFFFFFF  DT Customized UI
-Romania         226        03/06         Other value     Other value                         Open Market UI
-Romania         226        Other value   not applicable  not applicable                      Open Market UI
*/

//Just allow GR(Greece)/HR(Croatia)/NL(Netherlands)/ME(Montenegro)/MK(Macedonia) Operator now.
public class TmobileSIMCardStore implements ISIMCardStore {
    public static final String TAG = "TmobileSIMCardStore";
    private static final int SIMCARD_OPEN = 0;
    private static final int SIMCARD_MAIN = 1;
    private static final int SIMCARD_SUB = 2;

    private static final String MAPPED_MNC_MAIN = "999";
    private static final String MAPPED_MNC_SUB = "998";

    private /*static*/ List<SIMCardInfo> SIMCARD_ITEMS = null;
    private /*static*/ HashMap<String, SIMCardInfo> MAPMNC_ITEMS = null;

    private HashMap<String, SIMCardInfo> createMncMappedSet(List<SIMCardInfo> simCardInfos) {
        HashMap<String, SIMCardInfo> mappedSimcardInfos = new HashMap<String, SIMCardInfo>();
        for (SIMCardInfo s : simCardInfos) {
            mappedSimcardInfos.put(s.getMccMncSpn(), s);
        }
        return mappedSimcardInfos;
    }

    /*static*/
    private void init() {

        TctLog.d(TAG, "TMOCardStore init");
        SIMCARD_ITEMS = new ArrayList<SIMCardInfo>();
        MAPMNC_ITEMS = new HashMap<String, SIMCardInfo>();
        // because perso can only set 91 for string,so use three perso.

        String logMsg = "init mccmnc list";
        MncMappedXmlParser parser = new MncMappedXmlParser();
        try {
            SIMCARD_ITEMS = parser.parse(MncMappedXmlParser.OPERATOR_TMO);
        } catch (Exception e) {
            TctLog.d(TAG, "parse xml exception : " + e.getMessage());
        }


        MAPMNC_ITEMS = createMncMappedSet(SIMCARD_ITEMS);


    }

    public TmobileSIMCardStore() {
        init();
    }

    //@override
    public boolean isValidOperatorSIMCard(String mapped) {
        TctLog.d(TAG, "isValidOperatorSIMCard : " + mapped);
        if (mapped.equals("sim_lock")) {
            return true;
        }
        if (SsvUtil.isValidMccMnc(mapped)) {
            TctLog.d(TAG, "here" );
            for (SIMCardInfo item : SIMCARD_ITEMS) {
                if (item.getMappedMccMnc().equals(mapped)){
                    TctLog.d(TAG, "mapped is valid in the SIMCardItems");
                    return true;
                }
            }
        }
        return false;
    }

    //@override
    public boolean compareOperatorSIMCard(String mappedlhs, String mappedrhs) {
        return mappedlhs.equals(mappedrhs);
    }

    //@override
    public String getMappedMccMnc(String mccmnc, String spn) {
        final String mccmncspn = mccmnc + spn;
        TctLog.d(TAG, "getMappedMccMnc mccspn = " + mccmncspn + ", keyset : " + MAPMNC_ITEMS.keySet().toString());

        SIMCardInfo item = MAPMNC_ITEMS.get(mccmncspn);

        if (item != null) {
            TctLog.d(TAG, "getMappedMccMnc1 : " + item.getMappedMccMnc());
            return item.getMappedMccMnc();
        }

        TctLog.d(TAG, "getMappedMccMnc : " + mccmnc);
        return mccmnc;
    }

    //@override
    /*static*/
    public String toString() {
        Iterator<SIMCardInfo> itr = SIMCARD_ITEMS.iterator();
        StringBuilder builder = new StringBuilder();
        int index = 0;
        builder.append(" TmobileSIMCardStore \n");
        while (itr.hasNext()) {
            SIMCardInfo item = itr.next();
            builder.append("Item").append(String.valueOf(index)).append("->: ");
            builder.append(item.toString());
            builder.append("\n");
            index++;
        }
        return builder.toString();
    }
}
