
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 08/20/2015|     haibin.yu        |        1067379       |SSV Design Idol4  */
/*           |                      |                      |                  */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/
package com.android.server;

import java.lang.String;
import java.util.Map;
import java.util.List;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Iterator;

import android.content.res.Resources;
import android.os.SystemProperties;
import android.util.TctLog;

public class FocSIMCardStore implements ISIMCardStore {
    public static final String TAG = "FocSIMCardStore";

    private static final String MAPPED_MNC_XML_PATH = "/data/system/ssv_simcard.xml";// from perso

    private /*static*/ List<SIMCardInfo> SIMCARD_ITEMS = null;
    private /*static*/ HashMap<String,SIMCardInfo> MAPMNC_ITEMS = null;

    private HashMap<String,SIMCardInfo> createMncMappedSet(List<SIMCardInfo> simCardInfos){
        HashMap<String,SIMCardInfo> mappedSimcardInfos = new HashMap<String, SIMCardInfo>();
        for(SIMCardInfo s : simCardInfos){
            mappedSimcardInfos.put(s.getMccMncGid(),s);
        }
        return mappedSimcardInfos;
    }

    /*static*/
    private void init()
    {

        TctLog.d(TAG,"FocSIMCardStore init");
        SIMCARD_ITEMS = new ArrayList<SIMCardInfo>();
        MAPMNC_ITEMS = new HashMap<String, SIMCardInfo>();

        String logMsg = "init mccmnc list:";

        MncMappedXmlParser parser = new MncMappedXmlParser();
        try{
            SIMCARD_ITEMS = parser.parse(MAPPED_MNC_XML_PATH);
        }catch (Exception e){
            TctLog.d(TAG,"parse xml exception : " + e.getMessage());
        }


        MAPMNC_ITEMS = createMncMappedSet(SIMCARD_ITEMS);

    }

    public FocSIMCardStore() {
        init();
    }

    //@override
    public boolean isValidOperatorSIMCard(String mapped) {
        TctLog.d(TAG, "isValidOperatorSIMCard : " + mapped);
        if(SsvUtil.isValidMccMnc(mapped)) {
            for(SIMCardInfo item : SIMCARD_ITEMS) {
                if(item.getMappedMccMnc().equals(mapped)){
                    TctLog.d(TAG, "isValid");
                    return true;
                }
            }
        }
        return false;
    }

    //@override
    public boolean compareOperatorSIMCard(String mappedlhs, String mappedrhs) {
        return mappedlhs.equals(mappedrhs);
    }

    //@override
    public String getMappedMccMnc(String mccmnc, String gid) {
        //final String mnc = (mccmnc.size() >=3 ? mccmnc.substring(3, mccmnc.size()) : "");
        final String mcc = (mccmnc.length() >=3 ? mccmnc.substring(0, 3) : mccmnc);
        final String mccgid = mcc + gid;
        TctLog.d(TAG,"mccmnc= " +mccmnc +" mcc = "+mcc+" getMappedMccMnc mccspn = " + mccgid + ", keyset : " + MAPMNC_ITEMS.keySet().toString());

        //if(!SsvUtils.isValidMccMnc(mccmnc))
        //    return MCCMNC_NULL;


        SIMCardInfo item = MAPMNC_ITEMS.get(mccgid);


        if(item != null){
            TctLog.d(TAG,"getMappedMccMnc1 : "+ item.getMappedMccMnc());
            return item.getMappedMccMnc();
        }

        TctLog.d(TAG,"getMappedMccMnc : "+ mccmnc);
        return mccmnc;
    }

    //@override
    /*static*/public String toString() {
        Iterator<SIMCardInfo> itr = SIMCARD_ITEMS.iterator();
        StringBuilder builder = new StringBuilder();
        int index = 0;
        builder.append(" TmobileSIMCardStore \n");
        while(itr.hasNext()) {
            SIMCardInfo item = itr.next();
            builder.append("Item").append(String.valueOf(index)).append("->: ");
            builder.append(item.toString());
            builder.append("\n");
            index ++;
        }
        return builder.toString();
    }
}
