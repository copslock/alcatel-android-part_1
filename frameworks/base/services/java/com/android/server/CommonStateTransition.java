
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 08/20/2015|     haibin.yu        |        1067379       |SSV Design Idol4  */
/*           |                      |                      |                  */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/
package com.android.server;

import android.os.SsvManager;

public class CommonStateTransition implements IStateTransition {
    private static int transitions[][] = {
        //ORG,                        DEF,                               OP
        { SsvManager.TRANSITION_NONE, SsvManager.TRANSITION_ORIG_TO_DEF, SsvManager.TRANSITION_ORIG_TO_OP },         // ORG
        { SsvManager.TRANSITION_NONE, SsvManager.TRANSITION_NONE,        SsvManager.TRANSITION_DEF_TO_OP },          // DEF
        { SsvManager.TRANSITION_NONE, SsvManager.TRANSITION_NONE,        SsvManager.TRANSITION_OP_TO_OP  }           // OP
    };

    public CommonStateTransition(int state) {
        mState = state;
        mTransition = SsvManager.TRANSITION_NONE;
    }

    public void handleNewState(int newState, boolean simcardChanged) {
       mTransition = transitions[mState][newState];

       if (mTransition == SsvManager.TRANSITION_OP_TO_OP && !simcardChanged) {
           mTransition = SsvManager.TRANSITION_NONE;
       }
       mState = newState;
    }

    public int getTransition() {
        return mTransition;
    }

    private int mTransition;
    private int mState;
}