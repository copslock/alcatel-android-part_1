
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 08/20/2015|     haibin.yu        |        1067379       |SSV Design Idol4  */
/*           |                      |                      |                  */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/
package com.android.server;

import android.os.SsvManager;
import android.util.TctLog;

public class TmobileStateTransition implements IStateTransition {
    public static final String TAG = "TmobileStateTransition";
    private static int transitions[][] = {
        //ORG,                        DEF,                               OP,                               sub
        { SsvManager.TRANSITION_NONE, SsvManager.TRANSITION_ORIG_TO_DEF, SsvManager.TRANSITION_ORIG_TO_OP, SsvManager.TRANSITION_ORIG_TO_SUB },         // ORG
        { SsvManager.TRANSITION_NONE, SsvManager.TRANSITION_NONE,        SsvManager.TRANSITION_DEF_TO_OP,  SsvManager.TRANSITION_ORIG_TO_SUB },          // DEF
        { SsvManager.TRANSITION_NONE, SsvManager.TRANSITION_OP_TO_DEF,   SsvManager.TRANSITION_OP_TO_OP,   SsvManager.TRANSITION_NONE },//op
        { SsvManager.TRANSITION_NONE, SsvManager.TRANSITION_NONE,        SsvManager.TRANSITION_NONE,       SsvManager.TRANSITION_NONE }// SUB
    };

    public TmobileStateTransition(int state) {
        mState = state;
        mTransition = SsvManager.TRANSITION_NONE;
    }

    public void handleNewState(int newState, boolean simcardChanged) {
       TctLog.d(TAG,"handleNewState : mState = " + mState + ", newState = " + newState + ", mTranstion = " + mTransition);
       mTransition = transitions[mState][newState];
       mState = newState;
       TctLog.d(TAG,"handleNewState : mState = " + mState + ", newState = " + newState + ", mTranstion = " + mTransition);

    }

    public int getTransition() {
        return mTransition;
    }

    private int mTransition;
    private int mState;
}
