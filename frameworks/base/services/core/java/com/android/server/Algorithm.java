package com.android.server;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;


/**
 * Algorithm
 * 
 * <pre>
 * Support DES,DESede(TripleDES,3DES),AES,Blowfish,RC2,RC4(ARCFOUR)
 * DES			key size must be equal to 56
 * DESede(TripleDES)	key size must be equal to 112 or 168
 * AES			key size must be equal to 128, 192 or 256,but 192 and 256 bits may not be available
 * Blowfish             key size must be multiple of 8, and can only range from 32 to 448 (inclusive)
 * RC2                  key size must be between 40 and 1024 bits
 * RC4(ARCFOUR)         key size must be between 40 and 1024 bits
 * Detail pls focus on JDK Document http://.../docs/technotes/guides/security/SunProviders.html
 * 
 * Mode:ECB/CBC/PCBC/CTR/CTS/CFB/CFB8 to CFB128/OFB/OBF8 to OFB128
 * Padding:Nopadding/PKCS5Padding/ISO10126Padding/
 * </pre>
 * 
 * @author TCTNB.93391
 * @version 0.9 2014/05/07
 */
public class Algorithm {
	
    private String algorithm = "AES";
    private String algorithm_mode_padding = "AES/CBC/Nopadding";
    
    public Algorithm() {
    }
    
	/**
	 * <pre>
	 * Support DES,DESede(TripleDES,3DES),AES,Blowfish,RC2,RC4(ARCFOUR)
	 * DES			key size must be equal to 56
	 * DESede(TripleDES)	key size must be equal to 112 or 168
	 * AES			key size must be equal to 128, 192 or 256,but 192 and 256 bits may not be available
	 * Blowfish             key size must be multiple of 8, and can only range from 32 to 448 (inclusive)
	 * RC2                  key size must be between 40 and 1024 bits
	 * RC4(ARCFOUR)         key size must be between 40 and 1024 bits
	 * Detail pls focus on JDK Document http://.../docs/technotes/guides/security/SunProviders.html
	 * 
	 * Mode:ECB/CBC/PCBC/CTR/CTS/CFB/CFB8 to CFB128/OFB/OBF8 to OFB128
	 * Padding:Nopadding/PKCS5Padding/ISO10126Padding/
	 * 
	 * @param algorithm, for example: AES
	 * @param mode, for example: AES/CBC/Nopadding
	 * </pre>
	 * */
    public Algorithm(String algorithm, String mode) {
    	if (algorithm != null && mode != null) {
    		this.algorithm = algorithm;
    		this.algorithm_mode_padding = mode;
    	}
    }

    /**
     * decrypt
     * 
     * @param data
     * @param key
     * @return byte[]
     */
    public byte[] decrypt(byte[] data, byte[] key) {
    	
    	if (data == null || key == null) {
    		return new byte[0];
    	}
    	
		try {
			SecretKeySpec secretKey = new SecretKeySpec(key, algorithm);
			Cipher cipher = Cipher.getInstance(algorithm_mode_padding);
			cipher.init(Cipher.DECRYPT_MODE, secretKey);
	        return cipher.doFinal(data);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			e.printStackTrace();
		}
        
		return new byte[0];
    }

    /**
     * encrypt
     * 
     * @param data
     * @param key
     * @return byte[]
     */
    public byte[] encrypt(byte[] data, byte[] key) {
    	
    	if (data == null || key == null) {
    		return new byte[0];
    	}
    	
		try {
			SecretKeySpec secretKey = new SecretKeySpec(key, algorithm);
			Cipher cipher = Cipher.getInstance(algorithm_mode_padding);
			cipher.init(Cipher.ENCRYPT_MODE, secretKey);
	        return cipher.doFinal(data);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			e.printStackTrace();
		}
        
		return new byte[0];
    }
    
    /**
     * decrypt
     * 
     * @param data
     * @param key
     * @return byte[]
     */
    public byte[] decrypt(byte[] data, byte[] key, byte[] iv) {
    	
    	if (data == null || key == null || iv == null) {
    		return new byte[0];
    	}
    	
		try {
			SecretKeySpec secretKey = new SecretKeySpec(key, algorithm);
			IvParameterSpec ivspec = new IvParameterSpec(iv);
			Cipher cipher = Cipher.getInstance(algorithm_mode_padding);
			cipher.init(Cipher.DECRYPT_MODE, secretKey, ivspec);
	        return cipher.doFinal(data);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (InvalidAlgorithmParameterException e) {
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			e.printStackTrace();
		}
        
		return new byte[0];
    }

    /**
     * encrypt
     * 
     * @param data
     * @param key
     * @return byte[]
     */
    public byte[] encrypt(byte[] data, byte[] key, byte[] iv) {
    	
    	if (data == null || key == null || iv == null) {
    		return new byte[0];
    	}
    	
		try {
			SecretKeySpec secretKey = new SecretKeySpec(key, algorithm);
			IvParameterSpec ivspec = new IvParameterSpec(iv);
			Cipher cipher = Cipher.getInstance(algorithm_mode_padding);
			cipher.init(Cipher.ENCRYPT_MODE, secretKey, ivspec);
	        return cipher.doFinal(data);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (InvalidAlgorithmParameterException e) {
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			e.printStackTrace();
		}
        
		return new byte[0];
    }
    
    public void setAlgorithm(String algorithm, String mode) {
    	if (algorithm != null && mode != null) {
    		this.algorithm = algorithm;
    		this.algorithm_mode_padding = mode;
    	}
    }
    
    public String getAlgorithm() {
    	return this.algorithm;
    }
    
    public String getMode() {
    	return this.algorithm_mode_padding;
    }
}
