/* Copyright (C) 2016 Tcl Corporation Limited */
package com.android.server.policy;

/**
 *
 * @author tangjun
 *  add for three finger screenshot
 *
 */

public interface MstITakeScreenShot {
	public void takeScreenShot();
}
