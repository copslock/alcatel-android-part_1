/* Copyright (C) 2016 Tcl Corporation Limited */
package com.android.server.pm;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageParser;
import android.content.pm.ResolveInfo;
import android.util.ArrayMap;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public interface IProviderIntentResolver {
    List<ResolveInfo> queryIntent(Intent intent, String resolvedType,
                                  boolean defaultOnly, int userId);
    List<ResolveInfo> queryIntent(Intent intent, String resolvedType, int flags,
                                  int userId);
    List<ResolveInfo> queryIntentForPackage(Intent intent, String resolvedType,
                                            int flags, ArrayList<PackageParser.Provider> packageProviders, int userId);
    void addProvider(PackageParser.Provider p);
    void removeProvider(PackageParser.Provider p);
    boolean dump(PrintWriter out, String title, String prefix, String packageName,
                 boolean printFilter, boolean collapseDuplicates);
    ArrayMap<ComponentName, PackageParser.Provider> getProviders();
}
