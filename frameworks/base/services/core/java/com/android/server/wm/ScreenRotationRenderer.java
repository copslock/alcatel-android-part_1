/**************************************************************************************************/
/*                                                                                 Date : 12/2013 */
/*                    Copyright (c) 2015 JRD Communications, Inc.                                 */
/**************************************************************************************************/
/*                                                                                                */
/*    This material is company confidential, cannot be reproduced in any                          */
/*    form without the written permission of JRD Communications, Inc.                             */
/*                                                                                                */
/*================================================================================================*/
/*   Author : Frederic Fillieux                                                                   */
/*   Role : Screen rotation animation in OpenGL ES 2.0                                            */
/*   Reference documents :                                                                        */
/*================================================================================================*/
/* Modifications                                                                                  */
/*================================================================================================*/
/* Date     | Author     | FeatureID                  | Modification                              */
/*=======================|============================|===========================================*/
/* 02/03/15 | F.Fillieux |                            | File creation                             */
/*================================================================================================*/
/* Problems Report(PR/CR)                                                                         */
/*================================================================================================*/
/* Date     | Author     | PR #                       | Modification                              */
/*========= |============|============================|===========================================*/
/*          |            |                            |                                           */
/**************************************************************************************************/


package com.android.server.wm;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.opengl.EGL14;
import android.opengl.EGLConfig;
import android.opengl.EGLContext;
import android.opengl.EGLDisplay;
import android.opengl.EGLSurface;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.GLUtils;
import android.opengl.Matrix;
import android.os.SystemClock;
import android.view.Surface;
import android.view.SurfaceControl;
import android.util.Log;
import android.util.TypedValue;
import java.util.Locale;


/***
 * Public API:
 * - ScreenRotationRenderer new(): constructor
 * - void drawFrame(): draws next frame on the specified surface
 * - boolean isAnimating(): check if animation is over
 * - void destroy(): MUST be called (one or more times) before dereferencing the object
 ***/
class ScreenRotationRenderer {
    private static final String TAG = "ScreenRotationRenderer";

    private static final boolean LOG_DEBUG = false;

    // Animation duration, and debug factor
    // 'single anim' is the duration for any given horizontal line to flip upside-down
    private static final float ANIM_DEBUG_FACTOR = 1f; // xxx >= 1.0f
    private static  float TOTAL_ANIM_DURATION ;
    private static  float SINGLE_ANIM_DURATION ;
     private static final long EXPECTED_FRAME_DURATION = 16; // 16 ms, i.e. 60 fps - not to be tuned
    
    // Highlight and shadow settings: keep between 0 and 1 
    private static final float MAX_HIGHLIGHT = 0.50f;
    private static final float MAX_SHADOW = 0.75f;
    
    // Overshoot setting, keep > 1
    private static final float MAX_OVERSHOOT = 2f;
    
    // Perspective settings
    private static final float LOOK_AT_Z = 5.0f; // Reduce to increase the perspective effect
    private static final float FRUSTUM_NEAR = 3.0f; // Keep smaller than LOOK_AT_Z
    private static final float FRUSTUM_FAR = 7.0f; // Keep larger than LOOK_AT_Z
    
    // The vertex mesh size. Decrease the size if tiles start to be obvious
    private static int TILE_SIZE_DIP = 20; // A few millimeters 
            
    // Private variables
    private static int HORIZONTAL_TILE_COUNT = 1; // Filled when we know the screen size
    private static int VERTICAL_TILE_COUNT = 1; // Filled when we know the screen size
    private static final int SIZEOF_SHORT = 2;
    private static final int SIZEOF_FLOAT = 4;
    private static final int VERTICES_DATA_STRIDE_BYTES = 5 * SIZEOF_FLOAT;
    
    private static int mIndicesVbo = -1, mVerticesVbo = -1;
    
    private FloatBuffer mVertices;
    private ShortBuffer mIndicesBuffer;

    private String mVertexShader;
    private String mFragmentShader;

    void initDuration(long duration) {
        TOTAL_ANIM_DURATION = duration * ANIM_DEBUG_FACTOR; // ms
        SINGLE_ANIM_DURATION = 0.65f * TOTAL_ANIM_DURATION; // ms
        //Mod-Begin by Tao.Jiang,2016-05-19,Defect: 1864309
        if(animationType != ANIMATION_FLIP)
        {
            mVertexShader =
                    "precision mediump float;\n" +
                            "attribute vec4 aPosition;\n" +
                            "attribute vec2 aTextureCoord;\n" +
                            "varying vec2 vTextureCoord;\n" +
                            "varying float vLightAngle;\n" +
                            "uniform float uAnimTime;\n" +
                            "uniform float uAnimDirection;\n" +
                            "uniform mat4 uMVPMatrix;\n" +
                            "uniform float uTileCount;\n" +
                            "uniform int uCurrRotation;\n" +
                            "uniform float uBackSide_x_PI;\n" +
                            "#define PI 3.1415926536\n" +
                            "#define TOTAL_ANIM_DURATION " + String.format(Locale.US,"%.2f", TOTAL_ANIM_DURATION) + "\n" +
                            "#define SINGLE_ANIM_DURATION " + String.format(Locale.US,"%.2f", SINGLE_ANIM_DURATION) + "\n" +
                            "#define MAX_OVERSHOOT " + String.format(Locale.US,"%.2f", MAX_OVERSHOOT) + "\n" +
                            "\n" +
                            "float getBladeAngle(float bladeIdx){\n" +
                            "   float singleAnimStartTime = 1.0 * bladeIdx * (TOTAL_ANIM_DURATION - SINGLE_ANIM_DURATION) / (uTileCount - 1.0);\n" +
                            "   float singleAnimTime = 1.0 * uAnimTime - singleAnimStartTime;\n" +
                            "   float animFraction = max(0.0, min(1.0, singleAnimTime / SINGLE_ANIM_DURATION));\n" +

                            // Overshoot interpolation... From OvershootInterpolator.java
                            "   float tension = MAX_OVERSHOOT * bladeIdx / uTileCount;\n" +
                            "   animFraction -= 1.0;\n" +
                            "   float interpolatedFraction = animFraction * animFraction * ((tension + 1.0) * animFraction + tension) + 1.0;\n" +

                            "   if (uAnimDirection < 0.0){\n" +
                            "       interpolatedFraction = 1.0 - interpolatedFraction;\n" +
                            "   }\n" +
                            "   float bladeAngle = PI * interpolatedFraction;\n" +
                            "   return bladeAngle;\n" +
                            "}\n" +
                            "\n" +
                            "void main() {\n" +
                            "  float coord = 1.0 - aPosition.y;\n" +


                            "  float bladeAngle = getBladeAngle((coord / 2.0) * uTileCount);\n" +
                            "  vLightAngle = bladeAngle;\n" +

                            "  mat4 rotationMatrix = mat4(cos(-bladeAngle), 0.0, -sin(-bladeAngle), 0.0,\n" +
                            "                             0.0,              1.0,               0.0, 0.0,\n" +
                            "                             sin(-bladeAngle), 0.0,  cos(-bladeAngle), 0.0,\n" +
                            "                             0.0,              0.0,               0.0, 1.0);\n" +
                            "  gl_Position = rotationMatrix * aPosition;\n" +
                            "  gl_Position = uMVPMatrix * gl_Position;\n" +
                            "  vTextureCoord = aTextureCoord;\n" +
                            "  vTextureCoord.x = 1.0 - vTextureCoord.x;\n" +
                            "  if (uBackSide_x_PI == 0.0){\n" +
                            "    vTextureCoord.y = 1.0 - vTextureCoord.y;\n" +
                            "  }\n" +

                            "  if (uCurrRotation == " + Surface.ROTATION_180 + " ||\n" +
                            "     uCurrRotation == " + Surface.ROTATION_270 + "){\n" +
                            "    gl_Position.x   = - gl_Position.x;\n" +
                            "    gl_Position.y   = - gl_Position.y;\n" +
                            "    vTextureCoord.x = 1.0 - vTextureCoord.x;\n" +
                            "    vTextureCoord.y = 1.0 - vTextureCoord.y;\n" +
                            "  }\n" +
                            "}\n";
        }else {
            mVertexShader =
                    "precision mediump float;\n" +
                            "attribute vec4 aPosition;\n" +
                            "attribute vec2 aTextureCoord;\n" +
                            "varying vec2 vTextureCoord;\n" +
                            "varying float vLightAngle;\n" +
                            "uniform float uAnimTime;\n" +
                            "uniform float uAnimDirection;\n" +
                            "uniform mat4 uMVPMatrix;\n" +
                            "uniform float uTileCount;\n" +
                            "uniform int uCurrRotation;\n" +
                            "uniform float uBackSide_x_PI;\n" +
                            "#define PI 3.1415926536\n" +
                            "#define TOTAL_ANIM_DURATION " + String.format(Locale.US,"%.2f", TOTAL_ANIM_DURATION) + "\n" +
                            "#define SINGLE_ANIM_DURATION " + String.format(Locale.US,"%.2f", SINGLE_ANIM_DURATION) + "\n" +
                            "#define MAX_OVERSHOOT " + String.format(Locale.US,"%.2f", MAX_OVERSHOOT) + "\n" +
                            "\n" +
                            "float getBladeAngle(float bladeIdx){\n" +
                            "   float singleAnimStartTime = 1.0 * bladeIdx * (TOTAL_ANIM_DURATION - SINGLE_ANIM_DURATION) / (uTileCount - 1.0);\n" +
                            "   float singleAnimTime = 1.0 * uAnimTime - singleAnimStartTime;\n" +
                            "   float animFraction = max(0.0, min(1.0, singleAnimTime / SINGLE_ANIM_DURATION));\n" +

                            // Overshoot interpolation... From OvershootInterpolator.java
                            "   float tension = MAX_OVERSHOOT * bladeIdx / uTileCount;\n" +
                            "   animFraction -= 1.0;\n" +
                            "   float interpolatedFraction = animFraction * animFraction * ((tension + 1.0) * animFraction + tension) + 1.0;\n" +

                            "   if (uAnimDirection < 0.0){\n" +
                            "       interpolatedFraction = 1.0 - interpolatedFraction;\n" +
                            "   }\n" +
                            "   float bladeAngle = PI * interpolatedFraction;\n" +
                            "   return bladeAngle;\n" +
                            "}\n" +
                            "\n" +
                            "void main() {\n" +
                            "  float coord;\n" +
                            "       if (uCurrRotation == " + Surface.ROTATION_0 + ") coord = 1.0 - aPosition.y;\n" +
                            "  else if (uCurrRotation == " + Surface.ROTATION_180 + ") coord = 1.0 - aPosition.y;\n" +
                            "  else if (uCurrRotation == " + Surface.ROTATION_90 + ") coord = 1.0 - aPosition.x;\n" +
                            "  else if (uCurrRotation == " + Surface.ROTATION_270 + ") coord = 1.0 - aPosition.x;\n" +

                            "  float bladeAngle = getBladeAngle((coord / 2.0) * uTileCount);\n" +
                            "  vLightAngle = bladeAngle;\n" +
                            "       if (uCurrRotation == " + Surface.ROTATION_0 + ") ;\n" +
                            "  else if (uCurrRotation == " + Surface.ROTATION_180 + ") ;\n" +
                            "  else if (uCurrRotation == " + Surface.ROTATION_90 + ") bladeAngle = - bladeAngle;\n" +
                            "  else if (uCurrRotation == " + Surface.ROTATION_270 + ") bladeAngle = - bladeAngle;\n" +

                            "  mat4 rotationMatrix = mat4(cos(-bladeAngle), 0.0, -sin(-bladeAngle), 0.0,\n" +
                            "                             0.0,              1.0,               0.0, 0.0,\n" +
                            "                             sin(-bladeAngle), 0.0,  cos(-bladeAngle), 0.0,\n" +
                            "                             0.0,              0.0,               0.0, 1.0);\n" +
                            "  if (uCurrRotation == " + Surface.ROTATION_0 + " ||\n" +
                            "     uCurrRotation == " + Surface.ROTATION_180 + "){\n" +
                            "       rotationMatrix = mat4(1.0,               0.0,               0.0, 0.0,\n" +
                            "                             0.0,  cos(-bladeAngle), -sin(-bladeAngle), 0.0,\n" +
                            "                             0.0,  sin(-bladeAngle),  cos(-bladeAngle), 0.0,\n" +
                            "                             0.0,               0.0,               0.0, 1.0);\n" +
                            "  }\n" +
                            "  gl_Position = rotationMatrix * aPosition;\n" +
                            "  gl_Position = uMVPMatrix * gl_Position;\n" +
                            "  vTextureCoord = aTextureCoord;\n" +

                            "  if (uCurrRotation == " + Surface.ROTATION_0 + "){\n" +
                            "    vTextureCoord.y = 1.0 - vTextureCoord.y;\n" +
                            "  if (uBackSide_x_PI == 0.0){\n" +
                            "    vTextureCoord.x = 1.0 - vTextureCoord.x;\n" +
                            "  }\n" +
                            "  }\n" +

                            "  if (uCurrRotation == " + Surface.ROTATION_180 + "){\n" +
                            "    gl_Position.x   = - gl_Position.x;\n" +
                            "    gl_Position.y   = - gl_Position.y;\n" +
                            "    vTextureCoord.x = 1.0 - vTextureCoord.x;\n" +
                            "  if (uBackSide_x_PI == 0.0){\n" +
                            "    vTextureCoord.x = 1.0 - vTextureCoord.x;\n" +
                            "  }\n" +
                            "  }\n" +

                            "  if (uCurrRotation == " + Surface.ROTATION_90 + "){\n" +
                            "    vTextureCoord.x = 1.0 - vTextureCoord.x;\n" +
                            "  if (uBackSide_x_PI == 0.0){\n" +
                            "    vTextureCoord.y = 1.0 - vTextureCoord.y;\n" +
                            "  }\n" +
                            "  }\n" +

                            "  if (uCurrRotation == " + Surface.ROTATION_270 + "){\n" +
                            "    gl_Position.x   = - gl_Position.x;\n" +
                            "    gl_Position.y   = - gl_Position.y;\n" +
                            "    vTextureCoord.y = 1.0 - vTextureCoord.y;\n" +
                            "  if (uBackSide_x_PI == 0.0){\n" +
                            "    vTextureCoord.y = 1.0 - vTextureCoord.y;\n" +
                            "  }\n" +
                            "  }\n" +
                            "}\n";
        }
        //Mod-End by Tao.Jiang

        mFragmentShader =
                "precision mediump float;\n" +
                        "varying vec2 vTextureCoord;\n" +
                        "varying float vLightAngle;\n" +
                        "uniform sampler2D sTexture;\n" +
                        "uniform vec4 uShade;\n" +
                        "uniform float uShadeMix;\n" +
                        "uniform float uBackSide_x_PI;\n" +
                        "const vec4 BLACK = vec4(0.0, 0.0, 0.0, 1.0);\n" +
                        "const vec4 WHITE = vec4(1.0, 1.0, 1.0, 1.0);\n" +
                        "#define PI 3.1415926536\n" +
                        "#define MAX_HIGHLIGHT " + String.format(Locale.US,"%.2f", MAX_HIGHLIGHT) + "\n" +
                        "#define MAX_SHADOW " + String.format(Locale.US,"%.2f", MAX_SHADOW) + "\n" +
                        "\n" +
                        "void main() {\n" +
                        "  gl_FragColor = texture2D(sTexture, vTextureCoord);\n" +
                        "  float shadeMix;\n" +
                        "  vec4 shade;\n" +
                        "  float lightAngle = mod(vLightAngle + uBackSide_x_PI, 2.0 * PI);\n" +
                        "  if (lightAngle < PI / 2.0){\n" +
                        "    shade = WHITE;\n" +
                        "    shadeMix = (1.0 - abs(lightAngle / (PI / 4.0) - 1.0)) * MAX_HIGHLIGHT;\n" +
                        "  } else {\n" +
                        "    shade = BLACK;\n" +
                        "      shadeMix = 0.0;\n" +
//                        "    if (lightAngle < PI){\n" +
//                        "      shadeMix = (lightAngle / (PI / 2.0) - 1.0) * MAX_SHADOW;\n" +
//                        "    } else if (lightAngle > 3.0 * PI / 2.0){\n" +
//                        "      shadeMix = (1.0 - (lightAngle - 3.0 * PI / 2.0) / (PI / 2.0)) * MAX_SHADOW;\n" +
//                        "    } else {\n" +
//                        "      shadeMix = MAX_SHADOW;\n" +
//                        "    }\n" +
                        "  }\n" +
                        "  gl_FragColor = mix(gl_FragColor, shade, shadeMix);\n" +
                        "}\n";
    }

    private float[] mMVPMatrix = new float[16];
    private float[] mProjMatrix = new float[16];
    private float[] mMMatrix = new float[16];
    private float[] mVMatrix = new float[16];
    
    private int mProgram;
    private int mTextureID;
    private int muMVPMatrixLocation;
    private int muBackSideLocation;
    private int maPositionLocation;
    private int maTextureLocation;
    private int muAnimTimeLocation;
    private int muAnimDirectionLocation;
    private int muTileCountLocation;
    private int muCurrRotationLocation;
    private int mBackgroundColor = 0;

    private long mAnimStartTime = -1, mAnimTime = 0;
    private float mAnimDirection = 1f;
    
    
    private Context mContext;    
    private Bitmap mScreenBitmap;
    private SurfaceControl mSurfaceControl;
    private Surface mSurface;
    private boolean mValid = false;
    private boolean mDestroyed = false;
    private int mCurrRotation = Surface.ROTATION_0;
    
    private EGLDisplay mEglDisplay;
    private EGLConfig mEglConfig;
    private EGLContext mEglContext;
    private EGLSurface mEglSurface;

    private EGLDisplay mPrevEglDisplay = EGL14.EGL_NO_DISPLAY;
    private EGLContext mPrevEglContext = EGL14.EGL_NO_CONTEXT;
    private EGLSurface mPrevEglReadSurface = EGL14.EGL_NO_SURFACE;
    private EGLSurface mPrevEglDrawSurface = EGL14.EGL_NO_SURFACE;

    //Add-Begin by Tao.Jiang,2016-05-19,Defect: 1864309
    private int animationType = -1;
    public static final int ANIMATION_SWIRL = 0;
    public static final int ANIMATION_FLIP = 1;
    //Add-End by Tao.Jiang

    public ScreenRotationRenderer(Context context,
                                  SurfaceControl surfaceControl,
                                  int width, int height,
                                  int currRotation, int animationType) {
        if(LOG_DEBUG)Log.d(TAG, "new ScreenRotationRenderer(" + width + "x" + height + ", currRotation: " + currRotation + "){");
        //Add-Begin by Tao.Jiang,2016-05-19,Defect: 1864309
        this.animationType = animationType;
        //Add-End by Tao.Jiang
        initDuration(900);
        try {
            mContext = context;
            mCurrRotation = currRotation;
            Bitmap screenBitmap = surfaceControl.screenshot(width, height);
            mScreenBitmap = screenBitmap.copy(Bitmap.Config.ARGB_8888, true);
            screenBitmap.recycle();
            screenBitmap = null;
            if(LOG_DEBUG)Log.d(TAG, "Got screenshot bitmap: " + mScreenBitmap);
            mSurfaceControl = surfaceControl;
            mSurface = new Surface();
            mSurface.release(); // To avoid CloseGuard() warning...
            mSurface.copyFrom(mSurfaceControl);
            
            createEglContext();
            createEglSurface();
            
            attachEglContext();
            initProgram();
            setupVertices(mScreenBitmap.getWidth(), mScreenBitmap.getHeight());
            detachEglContext();

            mValid = true;
        }
        catch (Exception e){
            e.printStackTrace();
        }

        if(LOG_DEBUG)Log.d(TAG, "} new ScreenRotationRenderer()");
    }

    void drawFrame(){
        if (! mValid) return;

        /** PR949177-SWD4-FRAMEWORK-kehao.wei-001 20150313 delete **/
        /*updateAnimationTime();*/
        if(LOG_DEBUG)Log.d(TAG, "ScreenRotationRenderer.drawFrame(animTime: " + mAnimTime + "){");

        doDraw();

        updateAnimationState();

        if(LOG_DEBUG)Log.d(TAG, "} ScreenRotationRenderer.drawFrame()");
    }

    boolean isAnimating(){
        boolean isAnimating = mValid && mAnimTime < TOTAL_ANIM_DURATION;
        if(LOG_DEBUG)Log.d(TAG, "ScreenRotationRenderer.isAnimating(mValid: " + mValid +
                   ", mAnimTime: " + mAnimTime + 
                   ") => " + isAnimating);
        return isAnimating;
    }
    
    void destroy(){
        if(LOG_DEBUG)Log.d(TAG, "ScreenRotationRenderer.destroy(){");

        if (! mDestroyed){
            mDestroyed = true;
            
            try {
                //detachEglContext();
                destroyEglContext();
                destroyEglSurface();
                if (mScreenBitmap != null){
                    mScreenBitmap.recycle();
                }
            }
            catch (Exception e){
                e.printStackTrace();
            }
            
            mEglConfig = null;
            mEglDisplay = null;
            mEglContext = null;
            mEglSurface = null;
            mContext = null;
            mScreenBitmap = null;
            mSurfaceControl = null;
            mContext = null;

            mPrevEglDisplay = null;
            mPrevEglContext = null;
            mPrevEglReadSurface = null;
            mPrevEglDrawSurface = null;

            mValid = false;
        }

        if(LOG_DEBUG)Log.d(TAG, "} ScreenRotationRenderer.destroy()");
    }

    private static void logEglError(String func) {
        Log.e(TAG, "*** " + func + " failed: error " + EGL14.eglGetError(), new Throwable());
    }
    
    private boolean createEglContext() {
        if (mEglDisplay == null) {
            mEglDisplay = EGL14.eglGetDisplay(EGL14.EGL_DEFAULT_DISPLAY);
            if (mEglDisplay == EGL14.EGL_NO_DISPLAY) {
                logEglError("eglGetDisplay");
                return false;
            }
            
            int[] version = new int[2];
            if (! EGL14.eglInitialize(mEglDisplay, version, 0, version, 1)) {
                mEglDisplay = null;
                logEglError("eglInitialize");
                return false;
            }

            if(LOG_DEBUG)Log.d(TAG, "createEglDisplay() => ok " + mEglDisplay);
        }

        if (mEglConfig == null) {
            int[] eglConfigAttribList = new int[] {
                    EGL14.EGL_RENDERABLE_TYPE, EGL14.EGL_OPENGL_ES2_BIT,
                    EGL14.EGL_COLOR_BUFFER_TYPE, EGL14.EGL_RGB_BUFFER,
                    EGL14.EGL_RED_SIZE, 8,
                    EGL14.EGL_GREEN_SIZE, 8,
                    EGL14.EGL_BLUE_SIZE, 8,
                    //EGL14.EGL_ALPHA_SIZE, 8,
                    EGL14.EGL_DEPTH_SIZE, 16,
                    EGL14.EGL_NONE
            };
            int[] numEglConfigs = new int[1];
            EGLConfig[] eglConfigs = new EGLConfig[1];
            if (! EGL14.eglChooseConfig(mEglDisplay, eglConfigAttribList, 0,
                                        eglConfigs, 0, eglConfigs.length, numEglConfigs, 0)) {
                logEglError("eglChooseConfig");
                return false;
            }
            mEglConfig = eglConfigs[0];
            if(LOG_DEBUG)Log.d(TAG, "createEglConfig() => ok " + mEglConfig);
        }

        if (mEglContext == null) {
            int[] eglContextAttribList = new int[] {
                    EGL14.EGL_CONTEXT_CLIENT_VERSION, 2,
                    EGL14.EGL_NONE
            };
            mEglContext = EGL14.eglCreateContext(mEglDisplay, mEglConfig,
                                                 EGL14.EGL_NO_CONTEXT, eglContextAttribList, 0);
            if (mEglContext == null) {
                logEglError("eglCreateContext");
                return false;
            }

            if(LOG_DEBUG)Log.d(TAG, "createEglContext() => ok " + mEglContext);
        }
        return true;
    }

    private void destroyEglContext(){
         if (mEglContext != null) {
             if(LOG_DEBUG)Log.d(TAG, "destroyEglContext(" + mEglContext + ")");
            if (! EGL14.eglDestroyContext(mEglDisplay, mEglContext)) {
                logEglError("eglDestroyContext");
            }
            mEglContext = null;
        }
    }

    private boolean createEglSurface(){
        if (mEglSurface == null) {
            int[] eglSurfaceAttribList = new int[] {
                    EGL14.EGL_NONE
            };
            // turn our SurfaceControl into a Surface
            mEglSurface = EGL14.eglCreateWindowSurface(mEglDisplay, mEglConfig, mSurface,
                                                       eglSurfaceAttribList, 0);
            if (mEglSurface == null) {
                logEglError("eglCreateWindowSurface");
                return false;
            }
            if(LOG_DEBUG)Log.d(TAG, "createEglSurface() => ok " + mEglSurface);
        }
        return true;
    }

    private void destroyEglSurface() {
        if (mEglSurface != null) {
            if(LOG_DEBUG)Log.d(TAG, "destroyEglSurface(" + mEglSurface + ")");
            if (! EGL14.eglDestroySurface(mEglDisplay, mEglSurface)) {
                logEglError("eglDestroySurface");
            }
            mEglSurface = null;
        }
    }
    
    private boolean attachEglContext() {
        if (mEglSurface == null) {
            if(LOG_DEBUG)Log.d(TAG, "attachEglContext() *** no EGL surface");
            return false;
        }
        
        mPrevEglDisplay = EGL14.eglGetCurrentDisplay();
        mPrevEglContext = EGL14.eglGetCurrentContext();
        mPrevEglReadSurface = EGL14.eglGetCurrentSurface(EGL14.EGL_READ);
        mPrevEglDrawSurface = EGL14.eglGetCurrentSurface(EGL14.EGL_DRAW);
        if(LOG_DEBUG)Log.d(TAG, "attachEglContext(prevInfo: " + mPrevEglDisplay +
               ", " + mPrevEglContext + 
               ", " + mPrevEglReadSurface +
               ", " + mPrevEglDrawSurface);

        if (! EGL14.eglMakeCurrent(mEglDisplay, mEglSurface, mEglSurface, mEglContext)) {
            logEglError("eglMakeCurrent");
            return false;
        }
        if(LOG_DEBUG)Log.d(TAG, "attachEglContext() => ok");
        return true;
    }

    private void detachEglContext() {
        if (mEglDisplay != null) {
            if(LOG_DEBUG)Log.d(TAG, "detachEglContext()");
            EGL14.eglMakeCurrent(mEglDisplay,
                                 EGL14.EGL_NO_SURFACE, EGL14.EGL_NO_SURFACE, EGL14.EGL_NO_CONTEXT);
        }

        EGLDisplay eglDisplay = 
            (mPrevEglDisplay != EGL14.EGL_NO_DISPLAY) ? mPrevEglDisplay : mEglDisplay;
        EGL14.eglMakeCurrent(mPrevEglDisplay, 
                             mPrevEglDrawSurface, mPrevEglReadSurface,
                             mPrevEglContext);

        mPrevEglDisplay = null;
        mPrevEglContext = null;
        mPrevEglReadSurface = null;
        mPrevEglDrawSurface = null;
    }
    
    private void initVertices(int width, int height){
        Resources r = mContext.getResources();
        int tileSizePx = 
                Math.max(1, 
                         (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 
                                                         TILE_SIZE_DIP, 
                                                         r.getDisplayMetrics()));
        
        HORIZONTAL_TILE_COUNT = Math.max(1, width  / tileSizePx);
        VERTICAL_TILE_COUNT   = Math.max(1, height / tileSizePx);

        if(LOG_DEBUG)Log.d(TAG, "ScreenRotationRenderer.initVertices(tileSizePx: " + tileSizePx + ")" +
              " HORIZONTAL_TILE_COUNT => " + HORIZONTAL_TILE_COUNT +
              ", VERTICAL_TILE_COUNT => " + VERTICAL_TILE_COUNT);
        
        mVertices = ByteBuffer.allocateDirect((HORIZONTAL_TILE_COUNT + 1) * (VERTICAL_TILE_COUNT + 1) * VERTICES_DATA_STRIDE_BYTES)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer();
        for (int y=0; y <= VERTICAL_TILE_COUNT; y++){
            //--- Write { X, Y, Z } vertex data and { U, V } texture info
            for (int x=0; x <= HORIZONTAL_TILE_COUNT; x++){
                // X, Y, Z
                mVertices.put(-1.0f + 2.0f * x / HORIZONTAL_TILE_COUNT);
                mVertices.put(-1.0f + 2.0f * y / VERTICAL_TILE_COUNT);
                mVertices.put( 0.0f);
                // U, V
                mVertices.put(1.0f * x / HORIZONTAL_TILE_COUNT);
                mVertices.put(1.0f * y / VERTICAL_TILE_COUNT);
            }
        }
        mVertices.position(0);
        
        
        // C.f. http://www.learnopengles.com/tag/triangle-strips/
        int indexCount =
                2 * VERTICAL_TILE_COUNT * (HORIZONTAL_TILE_COUNT + 1) + // Triangle strips
                2 * (VERTICAL_TILE_COUNT - 1); // Degenerate triangles to connect line strips
        if(LOG_DEBUG)Log.d(TAG, "ScreenRotationRenderer.initVertices(" + HORIZONTAL_TILE_COUNT + "x" + VERTICAL_TILE_COUNT + ") => #indices: " + indexCount);
        mIndicesBuffer = ByteBuffer.allocateDirect(indexCount * SIZEOF_SHORT)
                             .order(ByteOrder.nativeOrder())
                             .asShortBuffer();
        
        for (int y=0, p1=0, p2=HORIZONTAL_TILE_COUNT+1; y < VERTICAL_TILE_COUNT; y++){
            mIndicesBuffer.put((short) p1++);
            mIndicesBuffer.put((short) p2++);
            
            for (int x=0; x < HORIZONTAL_TILE_COUNT; x++){
                mIndicesBuffer.put((short) p1++);
                mIndicesBuffer.put((short) p2++);
            }
            if (y < VERTICAL_TILE_COUNT - 1){
                mIndicesBuffer.put((short) (p2 - 1));
                mIndicesBuffer.put((short) p1);
            }
        }
        mIndicesBuffer.position(0);
    }
    
    private void doDraw(){
        if (! attachEglContext()){
            return;
        }

        GLES20.glClearColor(1f * Color.red(mBackgroundColor) / 255,
                            1f * Color.green(mBackgroundColor) / 255, 
                            1f * Color.blue(mBackgroundColor) / 255,
                            1f);
        GLES20.glClear( GLES20.GL_DEPTH_BUFFER_BIT | GLES20.GL_COLOR_BUFFER_BIT);

        GLES20.glUniform1f(muAnimTimeLocation, mAnimTime);
        GLES20.glUniform1f(muAnimDirectionLocation, mAnimDirection);
        float tileCount = 
            (mCurrRotation == Surface.ROTATION_0 ||
             mCurrRotation == Surface.ROTATION_180) ? 
            VERTICAL_TILE_COUNT : HORIZONTAL_TILE_COUNT;
        GLES20.glUniform1f(muTileCountLocation, tileCount);
        GLES20.glUniform1i(muCurrRotationLocation, mCurrRotation);
        
        Matrix.setIdentityM(mMMatrix, 0);
        Matrix.multiplyMM(mMVPMatrix, 0, mVMatrix, 0, mMMatrix, 0);
        Matrix.multiplyMM(mMVPMatrix, 0, mProjMatrix, 0, mMVPMatrix, 0);
        GLES20.glUniformMatrix4fv(muMVPMatrixLocation, 1, false, mMVPMatrix, 0);
        
        GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, mIndicesVbo);
        
        GLES20.glCullFace(GLES20.GL_BACK);
        GLES20.glUniform1f(muBackSideLocation, (float) 0.0);
        GLES20.glDrawElements(GLES20.GL_TRIANGLE_STRIP, mIndicesBuffer.capacity(), GLES20.GL_UNSIGNED_SHORT, 0);
        checkGlError("glDrawElements");

        GLES20.glCullFace(GLES20.GL_FRONT);
        GLES20.glUniform1f(muBackSideLocation, (float) Math.PI);
        GLES20.glDrawElements(GLES20.GL_TRIANGLE_STRIP, mIndicesBuffer.capacity(), GLES20.GL_UNSIGNED_SHORT, 0);
        checkGlError("glDrawElements");
        
        GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, 0);

        EGL14.eglSwapBuffers(mEglDisplay, mEglSurface);

        detachEglContext();
    }

    /** PR949177-SWD4-FRAMEWORK-kehao.wei-001 20150313 modify **/
    public void updateAnimationTime(){
        long time = SystemClock.uptimeMillis(); // % 4000L;
        if (mAnimStartTime == -1){
            // Remove EXPECTED_FRAME_DURATION so that the first displayed frame
            // already displays some movement, i.e. make the UI look more responsive
            mAnimStartTime = time - EXPECTED_FRAME_DURATION;
        }
        mAnimTime = time - mAnimStartTime;
    }
    
    private void updateAnimationState(){
        // Can be used to reverse the animation once it's finished (mainly for demo)
        /*
        if (mAnimTime > 1.5f * TOTAL_ANIM_DURATION || mAnimTime > TOTAL_ANIM_DURATION + 2000){
            mAnimDirection *= -1f;
            mAnimStartTime = -1;
        }
        */
    }

    private void setupVertices(int width, int height){
        if(LOG_DEBUG)Log.d(TAG, "ScreenRotationRenderer.setupVertices(){");
        
        GLES20.glViewport(0, 0, width, height);
        initVertices(width, height);
        
        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, mVerticesVbo);
        GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, mVertices.capacity() * SIZEOF_FLOAT, mVertices, GLES20.GL_STATIC_DRAW);
        checkGlError("glBufferData GL_ARRAY_BUFFER");
        
        GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, mIndicesVbo);
        GLES20.glBufferData(GLES20.GL_ELEMENT_ARRAY_BUFFER, mIndicesBuffer.capacity() * SIZEOF_SHORT, mIndicesBuffer, GLES20.GL_STATIC_DRAW);
        checkGlError("glBufferData GL_ELEMENT_ARRAY_BUFFER");
        
        GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, mIndicesVbo);
        mIndicesBuffer.position(0);
        GLES20.glBufferData(GLES20.GL_ELEMENT_ARRAY_BUFFER, mIndicesBuffer.capacity() * SIZEOF_SHORT, mIndicesBuffer, GLES20.GL_STATIC_DRAW);
        
        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);
        GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, 0);

        if(LOG_DEBUG)Log.d(TAG, "} ScreenRotationRenderer.setupVertices()");
    }

    private void initProgram(){
        if(LOG_DEBUG)Log.d(TAG, "ScreenRotationRenderer.initProgram(){");

        mProgram = createProgram(mVertexShader, mFragmentShader);
        if (mProgram != 0){
            GLES20.glUseProgram(mProgram);
            checkGlError("glUseProgram");

            maPositionLocation = GLES20.glGetAttribLocation(mProgram, "aPosition");
            if (maPositionLocation == -1) throw new RuntimeException("Could not get attrib location for aPosition");
            
            maTextureLocation = GLES20.glGetAttribLocation(mProgram, "aTextureCoord");
            if (maTextureLocation == -1) throw new RuntimeException("Could not get attrib location for aTextureCoord");
            
            muMVPMatrixLocation = GLES20.glGetUniformLocation(mProgram, "uMVPMatrix");
            if (muMVPMatrixLocation == -1) throw new RuntimeException("Could not get attrib location for uMVPMatrix");
            
            muBackSideLocation = GLES20.glGetUniformLocation(mProgram, "uBackSide_x_PI");
            if (muBackSideLocation == -1) throw new RuntimeException("Could not get attrib location for uBackSide_x_PI");
            
            muAnimTimeLocation = GLES20.glGetUniformLocation(mProgram, "uAnimTime");
            if (muAnimTimeLocation == -1) throw new RuntimeException("Could not get attrib location for uAnimTime");
            
            muAnimDirectionLocation = GLES20.glGetUniformLocation(mProgram, "uAnimDirection");
            if (muAnimDirectionLocation == -1) throw new RuntimeException("Could not get attrib location for uAnimDirection");
            
            muTileCountLocation = GLES20.glGetUniformLocation(mProgram, "uTileCount");
            if (muTileCountLocation == -1) throw new RuntimeException("Could not get attrib location for uTileCount");
            
            muCurrRotationLocation = GLES20.glGetUniformLocation(mProgram, "uCurrRotation");
            if (muCurrRotationLocation == -1) throw new RuntimeException("Could not get attrib location for uCurrRotation");
            
            
            
            GLES20.glEnable(GLES20.GL_DEPTH_TEST);
            
            GLES20.glFrontFace(GLES20.GL_CCW);
            GLES20.glCullFace(GLES20.GL_BACK);
            GLES20.glEnable(GLES20.GL_CULL_FACE);
            
            GLES20.glEnable(GLES20.GL_DITHER);
            GLES20.glEnable(GLES20.GL_BLEND);
            GLES20.glBlendFuncSeparate(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA, // color
                                       GLES20.GL_ONE,       GLES20.GL_ONE_MINUS_SRC_ALPHA); // alpha
            GLES20.glBlendEquation(GLES20.GL_FUNC_ADD);
            
            
            Matrix.setLookAtM(mVMatrix, 0,
                              0f, 0f, -LOOK_AT_Z, // Eye pointer
                              0f, 0f, 0f, // Center of view
                              0f, 1f, 0f); // Up vector

            Matrix.frustumM(mProjMatrix, 0,
                            -FRUSTUM_NEAR / LOOK_AT_Z, FRUSTUM_NEAR / LOOK_AT_Z, 
                            -FRUSTUM_NEAR / LOOK_AT_Z, FRUSTUM_NEAR / LOOK_AT_Z, 
                            FRUSTUM_NEAR, FRUSTUM_FAR);
             

            int buffers[] = new int[2]; 
            GLES20.glGenBuffers(2, buffers, 0);
            mVerticesVbo = buffers[0];
            mIndicesVbo = buffers[1];
            checkGlError("glGenBuffers");
            
            GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextureID);
            
            GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, mVerticesVbo);
            GLES20.glEnableVertexAttribArray(maPositionLocation);
            GLES20.glVertexAttribPointer(maPositionLocation, 3, GLES20.GL_FLOAT, false, VERTICES_DATA_STRIDE_BYTES, 0 * SIZEOF_FLOAT);
            checkGlError("glEnableVertexAttribArray maPositionLocation");
            
            GLES20.glEnableVertexAttribArray(maTextureLocation);
            GLES20.glVertexAttribPointer(maTextureLocation, 2, GLES20.GL_FLOAT, false, VERTICES_DATA_STRIDE_BYTES, 3 * SIZEOF_FLOAT);
            checkGlError("glEnableVertexAttribArray maTextureLocation");


            /***
             * Create our texture. This has to be done each time the
             * surface is created.
             ***/
            
            int[] textures = new int[1];
            GLES20.glGenTextures(1, textures, 0);
            
            mTextureID = textures[0];
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextureID);
            
            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
            
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);

            /***
             * For the background color of the animation:
             * - Pick the main color from the bitmap
             * - Turn it dark with 15% brightness
             ***/
            int mainColor = getMainColor(mScreenBitmap);
            float hsv[] = new float[3];
            Color.colorToHSV(mainColor, hsv);
            hsv[2] = Math.min(0.15f, 0.75f * hsv[2]); // Empirical value, quite dark
            mBackgroundColor = Color.HSVToColor(hsv);
            if(LOG_DEBUG)Log.d(TAG, "Background color: " + String.format("%08X", mBackgroundColor) +
                       ", h: " + hsv[0] + ", s: " + hsv[1] + ", v: " + hsv[2]);
            
            GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, mScreenBitmap, 0);
        }

        if(LOG_DEBUG)Log.d(TAG, "} ScreenRotationRenderer.initProgram()");
    }
    
    
    private int loadShader(int shaderType, String source) {
        int shader = GLES20.glCreateShader(shaderType);
        if (shader != 0) {
            GLES20.glShaderSource(shader, source);
            GLES20.glCompileShader(shader);
            int[] compiled = new int[1];
            GLES20.glGetShaderiv(shader, GLES20.GL_COMPILE_STATUS, compiled, 0);
            if (compiled[0] == 0) {
                Log.e(TAG, "*** Could not compile shader " + shaderType + ": " + source);
                Log.e(TAG, GLES20.glGetShaderInfoLog(shader));
                GLES20.glDeleteShader(shader);
                shader = 0;
            }
        }
        return shader;
    }

    private int createProgram(String vertexSource, String fragmentSource) {
        int vertexShader = loadShader(GLES20.GL_VERTEX_SHADER, vertexSource);
        if (vertexShader == 0) {
            return 0;
        }

        int pixelShader = loadShader(GLES20.GL_FRAGMENT_SHADER, fragmentSource);
        if (pixelShader == 0) {
            return 0;
        }

        int program = GLES20.glCreateProgram();
        if (program != 0) {
            GLES20.glAttachShader(program, vertexShader);
            checkGlError("glAttachShader");
            GLES20.glAttachShader(program, pixelShader);
            checkGlError("glAttachShader");
            GLES20.glLinkProgram(program);
            int[] linkStatus = new int[1];
            GLES20.glGetProgramiv(program, GLES20.GL_LINK_STATUS, linkStatus, 0);
            if (linkStatus[0] != GLES20.GL_TRUE) {
                Log.e(TAG, "*** Could not link program: ");
                Log.e(TAG, GLES20.glGetProgramInfoLog(program));
                GLES20.glDeleteProgram(program);
                program = 0;
            }
        }

        return program;
    }

    private void checkGlError(String op) {
        int error;
        while ((error = GLES20.glGetError()) != GLES20.GL_NO_ERROR) {
            Log.e(TAG, op + ": glError " + error);
            throw new RuntimeException(op + ": glError " + error);
        }
    }

    private static class ColorStats {
        int mRedSum;
        int mGreenSum;
        int mBlueSum;
        int mHitCount;
        
        void addColor(int color){
            mRedSum += Color.red(color);
            mGreenSum += Color.green(color);
            mBlueSum += Color.blue(color);
            mHitCount++;
        }
        
        int getAverage(){
            if (mHitCount > 0){
                return Color.argb(0xFF, mRedSum / mHitCount, mGreenSum / mHitCount, mBlueSum / mHitCount);
            }
            else {
                return 0;
            }
        }
    }
    
    /***
     * Get the 'main' i.e. more interesting color from a bitmap
     * The selection is globally as follow:
     * - For a few sample pixels, check if the pixel is colorful or grayish
     * - Classify colorful pixels into buckets, i.e. hue ranges
     * - If a range is much more represented than any other range, pick it, otherwise go grayscale     
     ***/
    private int getMainColor(Bitmap bitmap){
        int color = Color.WHITE;
        final int nbDistinctRanges = 6; // Empirical value, i.e. every 60 degree
        ColorStats grayscale = new ColorStats();
        ColorStats colors[] = new ColorStats[2 * nbDistinctRanges];
        for (int i=0; i < 2 * nbDistinctRanges; i++){
            colors[i] = new ColorStats();
        }
        ColorStats stats1, stats2;
        
        try {
            // Pick a few pixels:
            // - get their hue
            // - classify the hue into hue ranges
            // at the end, if any range is highly above the others, mix colors within it
            float hsv[] = new float[3];
            
            int xMax = Math.min(bitmap.getWidth(), 16); 
            int yMax = Math.min(bitmap.getHeight(), 16);
            for (int y=0; y < yMax; y++){
                int yy = y * (bitmap.getHeight() - 1) / (yMax - 1);
                for (int x=0; x < xMax; x++){
                    int xx = x * (bitmap.getWidth() - 1) / (xMax - 1);
                    int pixelColor = bitmap.getPixel(xx, yy);
                    Color.colorToHSV(pixelColor, hsv);
                    
                    if (hsv[1] < 0.3f){ // Empirical threshold...
                        // Let's count it as grayscale...
                        stats1 = grayscale;
                        stats2 = null;
                    }
                    else {
                        // Let's count it as colorful
                        int statIndex = ((int) hsv[0]) / Math.max(1, 360 / nbDistinctRanges / 2);
                        stats1 = colors[statIndex];
                        statIndex = (statIndex + 2 * nbDistinctRanges - 1) % (2 * nbDistinctRanges);
                        stats2 = colors[statIndex];
                    }
                    // stats1 always exists, stats2 is optional
                    stats1.addColor(pixelColor);
                    if (stats2 != null){
                        stats2.addColor(pixelColor);
                    }
                }
            }
            
            
            
            // Search a winner...
            // - Check if any color range is much more represented than any other (except immediate neighbor)
            // - If not, or if grayscale count is larger than X times the best color range, use grayscale
            int bestHitCount = 0;
            int bestIndex = -1;
            //Color.colorToHSV(color, hsv);
            //Log.d(TAG, "getMainColor() color h: " + hsv[0] + ", s: " + hsv[1] + ", v: " + hsv[2]);
            if(LOG_DEBUG)Log.d(TAG, "getMainColor() [gray] hitCount: " + grayscale.mHitCount);
            for (int r=0; r < 2 * nbDistinctRanges; r++){
                ColorStats stats = colors[r];
                if(LOG_DEBUG)Log.d(TAG, "getMainColor() [#" + r + "] hitCount: " + stats.mHitCount);
                if (bestIndex == -1 || bestHitCount < stats.mHitCount){
                    bestIndex = r;
                    bestHitCount = stats.mHitCount;
                }
            }
            if(LOG_DEBUG)Log.d(TAG, "getMainColor() bestHitCount: " + bestHitCount + " for #" + bestIndex);
            

            boolean isInteresting = true;
            float worseColorRatio = 0.0f;
            float grayscaleRatio = 10000000.0f;
            if (grayscale.mHitCount > 0){
                grayscaleRatio = (1.0f * colors[bestIndex].mHitCount) / grayscale.mHitCount;
            }
            if (colors[bestIndex].mHitCount == 0){
                isInteresting = false;
                if(LOG_DEBUG)Log.d(TAG, "getMainColor() is NOT interesting, because null hitCount #" + bestIndex);
            }
            else {
                for (int r=0; r < 2 * nbDistinctRanges; r++){
                    if (r !=   bestIndex &&
                        r != ((bestIndex + 1 + 2 * nbDistinctRanges) % (2 * nbDistinctRanges)) &&
                        r != ((bestIndex - 1 + 2 * nbDistinctRanges) % (2 * nbDistinctRanges))){
                        float ratio = (1.0f * colors[r].mHitCount) / colors[bestIndex].mHitCount;
                        if (ratio >= 0.5f){
                            isInteresting = false;
                            if(LOG_DEBUG)Log.d(TAG, "getMainColor() is NOT interesting, because of #" + r);
                            break;
                        }
                        worseColorRatio = Math.max(worseColorRatio, ratio);
                    }
                }
                if (isInteresting){
                    if(LOG_DEBUG)Log.d(TAG, "getMainColor() worseRatio: " + worseColorRatio +
                                 ", grayscaleRatio: " + grayscaleRatio);
                }
            }
            
            if (! isInteresting || grayscaleRatio < 0.1f || worseColorRatio >= grayscaleRatio){
                color = grayscale.getAverage();
                if(LOG_DEBUG)Log.d(TAG, "getMainColor() getting color from grayscale");
            }
            else {
                color = colors[bestIndex].getAverage();
                if(LOG_DEBUG)Log.d(TAG, "getMainColor() getting color from colors[#" + bestIndex + "]");
            }
            
            Color.colorToHSV(color, hsv);
            if(LOG_DEBUG)Log.d(TAG, "getMainColor() color h: " + hsv[0] + ", s: " + hsv[1] + ", v: " + hsv[2]);
        }
        catch (Exception e){
            e.printStackTrace();
        }
        
        return color;
    }
}

/* EOF */
