/* Copyright (C) 2016 Tcl Corporation Limited */
package com.android.server.pm;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageParser;
import android.content.pm.ProviderInfo;
import android.content.pm.ServiceInfo;
import android.content.pm.ResolveInfo;

import java.util.ArrayList;
import java.util.List;

public interface IPMSFunc {
    ActivityInfo getActivityInfo(ComponentName component, int flags, int userId);
    ServiceInfo getServiceInfo(ComponentName component, int flags, int userId);
    ProviderInfo getProviderInfo(ComponentName component, int flags, int userId);
    PackageInfo getPackageInfo(String packageName, int flags, int userId);
    PackageInfo generatePackageInfo(PackageSetting p, int flags, int userId);
    ApplicationInfo getApplicationInfo(String packageName, int flags, int userId);
    ProviderInfo resolveContentProvider(String name, int flags, int userId);

    List<ResolveInfo> queryIntentActivitiesHook(ComponentName comp, Intent intent, int flags, int userId);
    List<ResolveInfo> filterBySmartContainerHook(Intent intent, List<ResolveInfo> resolveInfos, int userId);
    ArrayList<ApplicationInfo> getInstalledApplicationsHook(ArrayList<ApplicationInfo> list, int flags, int userId);
    void clearPackagePreferredActivitiesHook(String packageName);
}
