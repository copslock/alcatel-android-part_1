package com.android.server;

/**
 * ByteArray to HexString, or HexString to ByteArray
 * 
 * @author TCTNB.93391
 * @version 0.9 2014/05/07
 */
public class Code {
	
	/**
	 * byte array to hex string
	 * 
	 * @param bytes
	 * @return String
	 */
	public static String parseByteArray2HexString(byte[] bytes) {
		
		StringBuffer sb = new StringBuffer();
		
		if (bytes == null) {
			return sb.toString();
		}
		
		for (int i = 0; i < bytes.length; i++) {
			String hex = Integer.toHexString(bytes[i] & 0xFF);
			if (hex.length() == 1) {
				hex = '0' + hex;
			}
			sb.append(hex.toUpperCase());
		}
		
		return sb.toString();
	}
	
	/**
	 * hex string to byte array
	 * 
	 * @param hex
	 * @return byte[]
	 */
	public static byte[] parseHexString2ByteArray(String hex) {
		
		if (hex == null || hex.length() % 2 == 1) {
			return new byte[0];
		}
		
		int sLen = hex.length();
		int bLen = sLen / 2;
		byte[] result = new byte[bLen];
		for (int i = 0; i < bLen; i++) {
			int high = Integer.parseInt(hex.substring(i * 2, i * 2 + 1), 16);
			int low = Integer.parseInt(hex.substring(i * 2 + 1, i * 2 + 2), 16);
			result[i] = (byte) (high * 16 + low);
		}
		
		return result;
	}
}
