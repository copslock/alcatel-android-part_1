/* Copyright (C) 2016 Tcl Corporation Limited */
// [FEATURE]-ADD-BEGIN by TCTSH.(yanxi.liu), For TBR, Task-784014
package com.android.server.am;

import android.content.pm.InstrumentationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ParceledListSlice;
import android.content.pm.UserInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.PathPermission;
import android.content.pm.ProviderInfo;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.content.res.CompatibilityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Proxy;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.Debug;
import android.os.DropBoxManager;
import android.os.Environment;
import android.os.FileObserver;
import android.os.FileUtils;
import android.os.Handler;
import android.os.IBinder;
import android.os.IPermissionController;
import android.os.IRemoteCallback;
import android.os.IUserManager;
import android.os.Looper;
import android.os.Message;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Process;
import android.os.RemoteCallbackList;
import android.os.RemoteException;
import android.os.SELinux;
import android.os.ServiceManager;
import android.os.StrictMode;
import android.os.SystemClock;
import android.os.SystemProperties;
import android.os.UpdateLock;
import android.os.UserHandle;
import android.provider.Settings;
import android.text.format.DateUtils;
import android.text.format.Time;
import android.util.AtomicFile;
import android.util.EventLog;
import android.util.Log;
import android.util.Pair;
import android.util.PrintWriterPrinter;
import android.util.Slog;
import android.util.SparseArray;
import android.util.TimeUtils;
import android.util.Xml;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.WindowManagerPolicy;
import android.content.BroadcastReceiver;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import android.app.ApplicationErrorReport;
import java.io.OutputStreamWriter;
import java.io.BufferedWriter;
import com.android.server.am.ActivityManagerService;

public class JrdRecordService {
    private static final String TAG="JRDRecordService";

    //private static final native void native_executeShell(String cmd_str);
    //private static final native void native_jrdrecord(String filename,int pid);
    private static JrdRecordService self =null;

    //public JrdRecordService{}

    public static JrdRecordService getInstance() {
        if(self ==null) self = new JrdRecordService();
        return self;
    }

    /*class JrdThread extends Thread {
        private String mFilename;
        private int    mPid;

        JrdThread(String filename,int pid) {
            super("JrdThread");
            mFilename = filename;
            mPid = pid;
        }

        @Override
        public void run() {
            ActivityManagerService.native_jrdrecord(mFilename,mPid);
        }
    }

    public void jrdrecord(String traceRenameFile,int pid) {
        android.util.Log.e(TAG, "tianfa.yang jrdrecord start traceRenameFile = "+traceRenameFile+" pid "+pid);
        Thread thread = new JrdThread(traceRenameFile,pid);
        thread.start();
    }*/

    public String jrdCrashHandler(int pid,String appname,ApplicationErrorReport.CrashInfo crashInfo,String EventType) {
        Slog.i(TAG,"jrdCrashHandler invoke ytf...");

        StringBuilder  loginfo = new StringBuilder();
        loginfo.setLength(0);
        loginfo.append("process:").append(appname).append("\n");
        loginfo.append("pid:").append(pid).append("\n");
        if(crashInfo != null) {
            loginfo.append("Classname:").append(crashInfo.exceptionClassName).append("\n");
            loginfo.append("Filename:").append(crashInfo.throwFileName).append("\n");
            loginfo.append("Methodname:").append(crashInfo.throwMethodName).append("\n");
            loginfo.append("LineNumber:").append(crashInfo.throwLineNumber).append("\n");
            loginfo.append("Cause:").append(crashInfo.exceptionMessage).append("\n");
            loginfo.append("stackTrace:").append("\n");
            loginfo.append(crashInfo.stackTrace);
        } else {
            loginfo.append("No detail of exception!");
        }
        return savetoologfile(pid,loginfo.toString(),EventType,appname);
    }

    private String savetoologfile(int pid,String msg,String EventType,String processName) {
        String tracesPath = SystemProperties.get("dalvik.vm.stack-trace-file", null);
        if (tracesPath == null || tracesPath.length() == 0) {
            return null;
        }
        String newTracesPath;
        int lpos = tracesPath.lastIndexOf ("/");
        if (-1 != lpos)
            newTracesPath = tracesPath.substring (0, lpos+1) + EventType+"of" + processName + ".txt";
        else
            newTracesPath = tracesPath + "_" + EventType+"_"+processName;

        StrictMode.ThreadPolicy oldPolicy = StrictMode.allowThreadDiskReads();
        StrictMode.allowThreadDiskWrites();
        try {
            final File tracesFile = new File(newTracesPath);
            final File tracesDir = tracesFile.getParentFile();
            final File tracesTmp = new File(tracesDir, "__tmp__");
            try {
                if (!tracesDir.exists()) {
                    tracesDir.mkdirs();
                    if (!SELinux.restorecon(tracesDir.getPath())) {
                        Slog.e(TAG,"jrdCrashHandler SElinux restorecon failed");
                        return null;
                    }
                }
                FileUtils.setPermissions(tracesDir.getPath(), 0775, -1, -1); // drwxrwxr-x

                if (tracesFile.exists()) {
                    tracesTmp.delete();
                    tracesFile.renameTo(tracesTmp);
                }
                StringBuilder sb = new StringBuilder();
                Time tobj = new Time();
                tobj.set(System.currentTimeMillis());
                sb.append(EventType).append(tobj.format(" happen at %Y-%m-%d %H:%M:%S")).append("\n");
                sb.append(msg);
                FileOutputStream fos = new FileOutputStream(tracesFile);
                fos.write(sb.toString().getBytes());
                fos.close();
                FileUtils.setPermissions(tracesFile.getPath(), 0666, -1, -1); // -rw-rw-rw-
            } catch (IOException e) {
                Slog.e(TAG, "Unable to prepare slow app traces file: " + newTracesPath, e);
                return null;
            }

            //tracesFile.renameTo(new File(newTracesPath));
            //if(Global.FEATURE_TCL_BUG_RECORD) {
                //jrdrecord(newTracesPath,pid);
            //}
        } finally {
            StrictMode.setThreadPolicy(oldPolicy);
        }
        return newTracesPath;
    }
}
// [FEATURE]-ADD-END by TCTSH.(yanxi.liu)
