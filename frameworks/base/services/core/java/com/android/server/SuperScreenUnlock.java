package com.android.server;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import android.util.Log;

/**
 * Super screen unlock
 * 
 * @author TCTNB.93391
 * @version 0.9 2014/07/28
 */
public class SuperScreenUnlock {
	
	public boolean unlock(String s) {
		
		File file = null;
		BufferedReader reader = null;
		String row = null;
		
		try {
			file = new File(s);
			if (!file.exists()) {
				return false;
			}
			reader = new BufferedReader(new FileReader(file));
			if ((row = reader.readLine()) == null) {
				return false;
			}
		} catch (FileNotFoundException e) {
			return false;
		} catch (IOException e) {
			return false;
		} catch (Exception e) {
			return false;
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
				}
			}
		}
		
		// row is a hex string, we need to decrypt it
		// decrypt row(32 chars)
		// serialno(8) + TAlcatel(8) + flag(3) + timestamp(13);
		// flag: 000 - timestamp work
		// flag: 001 - timestamp not work, super screen unlock once
		// flag: 002 - timestamp not work, super screen unlock forever
		try {
			String prefix = row.substring(0, row.length()/2);
			String suffix = row.substring(row.length()/2);
			String key = "6bD$c!g3n-~kJ~cW";
			String iv = "ta$d*m#i,7@qlMTk";
			Algorithm a = new Algorithm();
			byte[] v_prefix = a.decrypt(Code.parseHexString2ByteArray(prefix), key.getBytes(), iv.getBytes());
			byte[] v_suffix = a.decrypt(Code.parseHexString2ByteArray(suffix), key.getBytes(), iv.getBytes());
			String s_prefix = new String(v_prefix, "utf-8");
			String s_suffix = new String(v_suffix, "utf-8");
			prefix = s_prefix;
			suffix = s_suffix;
			
			String serialno = prefix.substring(0, 8);
			String flag = suffix.substring(0, 3);
                        Log.e("93391", "flag=" + flag);
			if (!serialno.equals(getSerialNumber())) {
				file.delete();
				return false;
			}
			if (flag.equals("000")) {
				long timestamp = Long.valueOf(suffix.substring(3));
				long now = System.currentTimeMillis()/1000;
				if ((now - timestamp > 7200) || (now - timestamp < 0)) { // 2 hours
					file.delete();
					return false;
				}
			} else if (flag.equals("001")) { // once
                                Log.e("93391", "flag=" + file.delete());
				file.delete();
			} else if (flag.equals("002")) { // forever
				
			} else {
				file.delete();
				return false;
			}
		} catch (UnsupportedEncodingException e) {
			return false;
		} catch (Exception e) {
			return false;
		}
		
		return true;
	}
	


	private String getSerialNumber() {

		String serial = "";

		try {
			Class<?> c = Class.forName("android.os.SystemProperties");
			Method get = c.getMethod("get", String.class);
			serial = (String) get.invoke(c, "ro.serialno");
			int len = serial.length();
			while (len < 8) {
				serial = serial + "0";
				len++;
			}
			if (len > 8) {
				serial = serial.substring(0, 8);
				len = 8;
			}
		} catch (Exception e) {
			return "";
		}
		
		return serial;
	}
}
