/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define LOG_TAG "ActivityManagerService"
//#define LOG_NDEBUG 0

#include <android_runtime/AndroidRuntime.h>
#include <jni.h>

#include <ScopedLocalRef.h>
#include <ScopedPrimitiveArray.h>

#include <cutils/log.h>
#include <utils/misc.h>
#include <utils/Log.h>

#include <stdio.h>
#include <errno.h>
#include <fcntl.h>
#include <semaphore.h>
#include <stddef.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

/* MODIFIED-BEGIN by li jiang, 2016-09-26,BUG-2987596*/
// [FEATURE]-ADD-BEGIN by TCTSH.(yanxi.liu), For TBR, Task-784014
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/un.h>

#include <utils/Log.h>
#include <utils/misc.h>

#include "jni.h"
#include "JNIHelp.h"

#ifndef MAX_STRING_LENGTH
#define MAX_STRING_LENGTH 256
#endif //MAX_STRING_LENGTH

#ifndef DEBUGGER_SOCKET_NAME
#define DEBUGGER_SOCKET_NAME "android:debuggerd"
#endif //DEBUGGER_SOCKET_NAME
// [FEATURE]-ADD-END by TCTSH.(yanxi.liu)

namespace android
{
    // [FEATURE]-ADD-BEGIN by TCTSH.(yanxi.liu), For TBR, Task-784014
    enum action_t {
        // dump a crash
        DEBUGGER_ACTION_CRASH = 0,
        // dump a tombstone file
        DEBUGGER_ACTION_DUMP_TOMBSTONE,
        // dump a backtrace only back to the socket
        DEBUGGER_ACTION_DUMP_BACKTRACE,
        // call jrdrecord
        DEBUGGER_ACTION_CALL_JRD_RECORD,
    };
    // [FEATURE]-ADD-END by TCTSH.(yanxi.liu)
    /* MODIFIED-END by li jiang,BUG-2987596*/

    // migrate from foreground to foreground_boost
    static jint migrateToBoost(JNIEnv *env, jobject _this)
    {
#ifdef USE_SCHED_BOOST
        // File descriptors open to /dev/cpuset/../tasks, setup by initialize, or -1 on error
        FILE* fg_cpuset_file = NULL;
        int   boost_cpuset_fd = 0;
        if (!access("/dev/cpuset/tasks", F_OK)) {
            fg_cpuset_file = fopen("/dev/cpuset/foreground/tasks", "r+");
            if (ferror(fg_cpuset_file)) {
                return 0;
            }
            boost_cpuset_fd = open("/dev/cpuset/foreground/boost/tasks", O_WRONLY);
            if (boost_cpuset_fd < 0) {
                fclose(fg_cpuset_file);
                return 0;
            }

        }
        if (!fg_cpuset_file || !boost_cpuset_fd) {
            fclose(fg_cpuset_file);
            close(boost_cpuset_fd);
            return 0;
        }
        char buf[17];
        while (fgets(buf, 16, fg_cpuset_file)) {
            int i = 0;
            for (; i < 16; i++) {
                if (buf[i] == '\n') {
                    buf[i] = 0;
                    break;
                }
            }
            if (write(boost_cpuset_fd, buf, i) < 0) {
                // ignore error
            }
            if (feof(fg_cpuset_file))
                break;
        }
        fclose(fg_cpuset_file);
        close(boost_cpuset_fd);
#endif
        return 0;
    }

    // migrate from foreground_boost to foreground
    static jint migrateFromBoost(JNIEnv *env, jobject _this)
    {
#ifdef USE_SCHED_BOOST
        // File descriptors open to /dev/cpuset/../tasks, setup by initialize, or -1 on error
        int   fg_cpuset_fd = 0;
        FILE* boost_cpuset_file = NULL;
        if (!access("/dev/cpuset/tasks", F_OK)) {
            boost_cpuset_file = fopen("/dev/cpuset/foreground/boost/tasks", "r+");
            if (ferror(boost_cpuset_file)) {
                return 0;
            }
            fg_cpuset_fd = open("/dev/cpuset/foreground/tasks", O_WRONLY);
            if (fg_cpuset_fd < 0) {
                fclose(boost_cpuset_file);
                return 0;
            }

        }
        if (!boost_cpuset_file || !fg_cpuset_fd) {
            fclose(boost_cpuset_file);
            close(fg_cpuset_fd);
            return 0;
        }
        char buf[17];
        while (fgets(buf, 16, boost_cpuset_file)) {
            //ALOGE("Appending FD %s to fg", buf);
            int i = 0;
            for (; i < 16; i++) {
                if (buf[i] == '\n') {
                    buf[i] = 0;
                    break;
                }
            }
            if (write(fg_cpuset_fd, buf, i) < 0) {
                //ALOGE("Appending FD %s to fg ERROR", buf);
                // handle error?
            }
            if (feof(boost_cpuset_file))
                break;
        }

        close(fg_cpuset_fd);
        fclose(boost_cpuset_file);

#endif
        return 0;

    }


    /* MODIFIED-BEGIN by li jiang, 2016-09-26,BUG-2987596*/
    /*static JNINativeMethod method_table[] = {
        { "nativeMigrateToBoost",   "()I", (void*)migrateToBoost },
        { "nativeMigrateFromBoost", "()I", (void*)migrateFromBoost },
    };*/

    //int register_android_server_ActivityManagerService(JNIEnv *env)
    //{
    //    return jniRegisterNativeMethods(env, "com/android/server/am/ActivityManagerService",
    //                                    method_table, NELEM(method_table));
    //}


    // [FEATURE]-ADD-BEGIN by TCTSH.(yanxi.liu), For TBR, Task-784014
    /* message sent over the socket */
    struct record_msg_t {
        // version 1 included:
        action_t action;
        pid_t pid;
        // version 2 added:
        uintptr_t abort_msg_address;
        int32_t original_si_code;
        char fileName[MAX_STRING_LENGTH];
    };

    static void ExecuteShell(JNIEnv* env, jobject clazz,jstring cmd_str) {
        const char *nativeString = env->GetStringUTFChars(cmd_str, 0);
        ALOGE("jackywei ExecuteShell = %s",nativeString);
        system(nativeString);
        env->ReleaseStringUTFChars(cmd_str,nativeString);
    }

    static int socket_abstract_client(const char* name, int type) {
        sockaddr_un addr;

        // Test with length +1 for the *initial* '\0'.
        size_t namelen = strlen(name);
        if ((namelen + 1) > sizeof(addr.sun_path)) {
            errno = EINVAL;
            return -1;
        }

        /* This is used for abstract socket namespace, we need
         * an initial '\0' at the start of the Unix socket path.
         *
         * Note: The path in this case is *not* supposed to be
         * '\0'-terminated. ("man 7 unix" for the gory details.)
         */
        memset(&addr, 0, sizeof(addr));
        addr.sun_family = AF_LOCAL;
        addr.sun_path[0] = 0;
        memcpy(addr.sun_path + 1, name, namelen);

        socklen_t alen = namelen + offsetof(sockaddr_un, sun_path) + 1;

        int s = socket(AF_LOCAL, type, 0);
        if (s == -1) {
            return -1;
        }

        int err = TEMP_FAILURE_RETRY(connect(s, (sockaddr*) &addr, alen));
        if (err == -1) {
            close(s);
            s = -1;
        }

        return s;
    }

    static void myjrdrecord(JNIEnv* env, jobject clazz,jstring filename,jint pid)
    {
        const char *nativeString = env->GetStringUTFChars(filename, 0);
        ALOGE("jackywei filename = %s,pid = %d",nativeString,pid);
        int s = socket_abstract_client(DEBUGGER_SOCKET_NAME, SOCK_STREAM);
        if (s >= 0) {
            record_msg_t msg;
            int ret;
            msg.action = DEBUGGER_ACTION_CALL_JRD_RECORD;
            msg.pid = pid;
            //msg.fileName = reinterpret_cast<uintptr_t>(gAbortMessage);
            //char *strncpy(char *dest, const char *src, size_t n);
            strncpy(msg.fileName,nativeString,MAX_STRING_LENGTH);
            ret = write(s, &msg, sizeof(msg));
            if (ret != sizeof(msg)) {
                ALOGE("jackywei jrdrecord error  by: %s",strerror(errno));
            }
            close(s);
        }
        env->ReleaseStringUTFChars(filename,nativeString);
    }

    /*
     * JNI registration.
     */
    static JNINativeMethod gMethods[] = {
        /* name, signature, funcPtr */
        { "nativeMigrateToBoost",   "()I", (void*)migrateToBoost },
        { "nativeMigrateFromBoost", "()I", (void*)migrateFromBoost },
        { "native_executeShell", "(Ljava/lang/String;)V", (void*)ExecuteShell},
        { "native_jrdrecord", "(Ljava/lang/String;I)V", (void*)myjrdrecord},
    };

    int register_android_server_ActivityManagerService(JNIEnv *env)
    {
        return jniRegisterNativeMethods(env, "com/android/server/am/ActivityManagerService",
                                        gMethods, NELEM(gMethods));
                                        /* MODIFIED-END by li jiang,BUG-2987596*/
    }

}
