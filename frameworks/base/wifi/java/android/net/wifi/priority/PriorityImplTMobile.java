/******************************************************************************/
/*                                                               Date:10/2012 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2012 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  Yu.Feng                                                         */
/*  Email  :  Yu.Feng@tcl-mobile.com                                          */
/*  Role   :                                                                  */
/*  Reference documents : T-Mobile requirements                               */
/* -------------------------------------------------------------------------- */
/*  Comments : Implements of wifi priority on T-Mobile, Private SSIDs (WPA/WP */
/*             A2-PSK) have grade Four, SSIDs like EAP-SIM have grade Three,  */
/*             Public SSIDs have grade two, Others have grade one             */
/*  File     :                                                                */
/*  Labels   : Wifi Priority                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */

package android.net.wifi.priority;

import java.util.List;

import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiConfiguration.KeyMgmt;

/**
 * @hide
 */
public class PriorityImplTMobile implements IPriority{

    @Override
    public int getPriorityGrade(WifiConfiguration mConfig) {
        //private ssids
        if(mConfig.allowedKeyManagement.get(KeyMgmt.WPA_PSK) || mConfig.allowedKeyManagement.get(KeyMgmt.WPA2_PSK)){
            return IPriority.PRIORITY_GRADE_FOUR;
        }
        //enterprise ssids
        if(mConfig.allowedKeyManagement.get(KeyMgmt.WPA_EAP) || mConfig.allowedKeyManagement.get(KeyMgmt.IEEE8021X)) {
            return IPriority.PRIORITY_GRADE_THREE;
        }
        //WEP ssids
        if(mConfig.wepKeys[0] != null) {
            return IPriority.PRIORITY_GRADE_ONE;
        }
        return IPriority.PRIORITY_GRADE_TWO;
    }

}
