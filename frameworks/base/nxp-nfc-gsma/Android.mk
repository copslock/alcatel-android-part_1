LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)
# MODIFIED-BEGIN by Ji.Chen, 2016-08-11,BUG-2701855
LOCAL_MODULE := com.gsma.services.nfc.xml
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := ETC
LOCAL_MODULE_PATH := $(TARGET_OUT_ETC)/permissions
LOCAL_SRC_FILES := $(LOCAL_MODULE)
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
# MODIFIED-END by Ji.Chen,BUG-2701855

LOCAL_SRC_FILES := $(call all-java-files-under, java)

LOCAL_MODULE:= com.gsma.services.nfc
LOCAL_MODULE_TAGS := optional

include $(BUILD_JAVA_LIBRARY)



