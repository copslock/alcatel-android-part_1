/* Copyright (C) 2016 Tcl Corporation Limited */
package com.android.systemui.aod.style;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import com.android.systemui.statusbar.BaseStatusBar;
import com.android.systemui.statusbar.NotificationData;
import com.android.systemui.statusbar.StatusBarIconView;

import android.app.Notification;
import android.app.Notification.Builder;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.service.notification.StatusBarNotification;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.android.internal.statusbar.StatusBarIcon;
import com.android.systemui.R;

public class NotificationStyle extends LinearLayout { // MODIFIED by song.huan, 2016-10-11,BUG-3000498

    private Context mContext;
    private LayoutInflater mInflater;

    private LinearLayout mNotificationView;
    private TextView[] not_num;
    private ImageView[] not_img;
    private int showIcon = 0; // MODIFIED by song.huan, 2016-10-11,BUG-3000498
    private static final int MAX_NOT_ICONS = 3;

    public NotificationStyle(Context context, AttributeSet attrs) {
        super(context, attrs);
        // TODO Auto-generated constructor stub
        mContext = context;
    }

    @Override
    protected void onFinishInflate() {
        // TODO Auto-generated method stub
        super.onFinishInflate();
        prepare();
    }


    private void prepare() {
        // TODO Auto-generated method stub
        mInflater = LayoutInflater.from(mContext);
        View view = mInflater.inflate(R.layout.aod_notification_style, null);

        not_num = new TextView[4];
        not_num[0] = (TextView) view.findViewById(R.id.noti_num_1);
        not_num[1] = (TextView) view.findViewById(R.id.noti_num_2);
        not_num[2] = (TextView) view.findViewById(R.id.noti_num_3);
        not_num[3] = (TextView) view.findViewById(R.id.noti_num_n);

        not_img = new ImageView[3];
        not_img[0] = (ImageView) view.findViewById(R.id.noti_img_1);
        not_img[1] = (ImageView) view.findViewById(R.id.noti_img_2);
        not_img[2] = (ImageView) view.findViewById(R.id.noti_img_3);

        this.removeAllViews();
        this.addView(view);

    }

    //show icon
    public void displayIconImage(NotificationData.Entry entry,int index){

        final StatusBarIcon ic = new StatusBarIcon(entry.notification.getPackageName(),
                entry.notification.getUser(),
                entry.notification.getNotification().icon,
                entry.notification.getNotification().iconLevel,
                entry.notification.getNotification().number,
                entry.notification.getNotification().tickerText);

        Drawable iconDrawable = StatusBarIconView.getIcon(getContext(), ic);
        not_img[index].setImageDrawable(iconDrawable);
        not_img[index].setColorFilter(Color.WHITE);

    /* MODIFIED-BEGIN by song.huan, 2016-10-13,BUG-3000498*/
    }

    public boolean checkNotification(NotificationData.Entry entry){
    /* MODIFIED-END by song.huan,BUG-3000498*/

        /* MODIFIED-BEGIN by song.huan, 2016-10-19,BUG-3146693*/
        if(entry.notification.getPackageName().equals("com.android.dialer")
                || entry.notification.getPackageName().equals("com.tct.email")
                || entry.notification.getPackageName().equals("com.android.mms")){
            return true;
        }else{
            return false;
            /* MODIFIED-END by song.huan,BUG-3146693*/
        }

    }

    public void updateIcons(){
        for(int i =0;i < MAX_NOT_ICONS; i++){
            not_num[i].setText(null);
            not_img[i].setImageDrawable(null);
        }
        not_num[MAX_NOT_ICONS].setVisibility(View.GONE);
    }

    public void updateTextView(LinkedHashMap<String, Integer> mpkgNum){
        Iterator iter = mpkgNum.entrySet().iterator();
        Integer moreNum = 0;
        while (iter.hasNext()) {
        	if(showIcon < MAX_NOT_ICONS){
        		Map.Entry entry = (Map.Entry) iter.next();
                String packagename = (String) entry.getKey();
                Integer num = (Integer) entry.getValue();
                    not_num[showIcon].setText("" + num);
                    showIcon++;
        	} else if(showIcon >= MAX_NOT_ICONS){
        		Map.Entry entry = (Map.Entry) iter.next();
                String packagename = (String) entry.getKey();
                moreNum += (Integer) entry.getValue();
                showIcon++;
        	}
        }
        if(showIcon > MAX_NOT_ICONS){
        	not_num[MAX_NOT_ICONS].setVisibility(View.VISIBLE);
        	not_num[MAX_NOT_ICONS].setText("+" + moreNum);
        } else {
        	not_num[MAX_NOT_ICONS].setVisibility(View.GONE);
        }
        showIcon = 0;
    }

}
