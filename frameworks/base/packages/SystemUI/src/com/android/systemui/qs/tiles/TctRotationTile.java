/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/******************************************************************************/
/*                                                               Date:12/2014 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2014 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  yan.teng                                                        */
/*  Email  : yan.teng@tcl.com                                                 */
/*  Role   :  SystemUI                                                        */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/*12/04/2014 |       yan.Teng       |      FR-862142       |add a switch      */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/

package com.android.systemui.qs.tiles;

import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import android.os.UserHandle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.CheckBox;

import com.android.internal.view.RotationPolicy;
import com.android.systemui.R;
import com.android.systemui.qs.QSTile;

//[BUGFIX]-Add-Start by TSCD.biao.tang,2015-04-10,PR-973571
import android.content.Intent;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
//[BUGFIX]-Add-End by TSCD.biao.tang,2015-04-10,PR-973571
import android.widget.TextView;//add by quanxiang.liu@jrdcom.com for defect1125981,2015-12-21

/** Quick settings tile: Rotation **/
public class TctRotationTile extends QSTile<QSTile.BooleanState> {
    private final boolean DEBUG = false;

    private final String TAG = "TctRotationTile";

    private final String KEY_NEVER_SHOW = "never_show_ReverEnableCheckActivity";

    private CheckBox mAlwaysAllow;

    boolean hasCall = false;
    boolean reversibleEnbleBeforehasCall = false; // MODIFIED by yanjun.kang, 2016-12-02,BUG-3517039

    private TelephonyManager teleManager;

    private PhoneStateListener mPhoneStateListener = new PhoneStateListener() {
        @Override
        public void onCallStateChanged(int state, String incomingNumber)
        {
            if (state == TelephonyManager.CALL_STATE_IDLE)
            {
                hasCall = false;
                /* MODIFIED-BEGIN by yanjun.kang, 2016-12-02,BUG-3517039*/
                RotationPolicy.enableReversibleRotation(mContext, reversibleEnbleBeforehasCall);
            } else
            {
                hasCall = true;
                if(RotationPolicy.isReversibleRotationEnabled(mContext)){
                    reversibleEnbleBeforehasCall = true;
                    RotationPolicy.enableReversibleRotation(mContext, false);
                }else{
                    reversibleEnbleBeforehasCall = false;
                }
            }
            Log.d(TAG, "onCallStateChanged hasCall ==" + hasCall + " reversibleEnbleBeforehasCall == " + reversibleEnbleBeforehasCall);
            /* MODIFIED-END by yanjun.kang,BUG-3517039*/
            refreshState();
        }
    };

    public TctRotationTile(Host host) {
        super(host);
        teleManager = (TelephonyManager) mContext
                .getSystemService(android.app.Service.TELEPHONY_SERVICE);
        if (teleManager != null)
        {
            teleManager.listen(mPhoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);
        }
    }

    @Override
    public BooleanState newTileState()
    {
        return new BooleanState();
    }

    public void setListening(boolean listening)
    {
        if (DEBUG)
            Log.d(TAG, "listening==" + listening);
        if (listening)
        {
            RotationPolicy.registerRotationPolicyListener(mContext, mRotationPolicyListener,
                    UserHandle.USER_ALL);
        } else
        {
            RotationPolicy.unregisterRotationPolicyListener(mContext, mRotationPolicyListener);
        }
    }

    public void changeState()
    {
        boolean state = RotationPolicy.isReversibleRotationEnabled(mContext);
        if (DEBUG)
            Log.d(TAG, "changeState==" + state);
        RotationPolicy.enableReversibleRotation(mContext, !state);
    }

    @Override
    protected void handleClick()
    {
        if (hasCall)
        {
            return;
        }
        // Show Dialog
        boolean isShow = mContext.getResources().getBoolean(
                com.android.internal.R.bool.feature_tctsetting_rever_dialog);
        boolean isReversibleEnabled = RotationPolicy.isReversibleRotationEnabled(mContext);
        if (isShow
                && !isReversibleEnabled
                && Settings.System.getInt(mContext.getContentResolver(),
                        "never_show_ReverEnableCheckActivity", 0) == 0)
        {
            AlertDialog.Builder b = new Builder(mContext,
                    android.R.style.Theme_DeviceDefault_Light_Dialog_Alert);
            b.setTitle(R.string.rever_enable_check_diag_title);
            b.setPositiveButton(android.R.string.ok, new OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    if (mAlwaysAllow != null && mAlwaysAllow.isChecked())
                    {
                        Settings.System.putInt(mContext.getContentResolver(), KEY_NEVER_SHOW, 1);
                    }

                    RotationPolicy.enableReversibleRotation(mContext, true);
                }
            });
            b.setNegativeButton(android.R.string.cancel, null);
            b.setOnDismissListener(new android.content.DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog)
                {
                    mAlwaysAllow = null;
                }
            });

            // add "always allow" checkbox
            LayoutInflater inflater = LayoutInflater.from(b.getContext());
            View customView = inflater.inflate(R.layout.reversible, null);
            TextView text = (TextView) customView.findViewById(R.id.reversible);
            text.setText(R.string.rever_enable_check_diag_msg);
            mAlwaysAllow = (CheckBox) customView.findViewById(R.id.alwaysUse);
            mAlwaysAllow.setText(R.string.rever_enable_check_never_remind);
            b.setView(customView);

            Dialog d = b.create();
            d.getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD_DIALOG);
            d.show();
        } else
        {
            changeState();
        }
    }

    @Override
    protected void handleUpdateState(BooleanState state, Object arg)
    {
        state.value = RotationPolicy.isReversibleRotationEnabled(mContext);
        Log.d(TAG, "handleUpdateState isReversibleRotationEnabled ==" + state.value); // MODIFIED by yanjun.kang, 2016-12-02,BUG-3517039
        final Resources res = mContext.getResources();
        state.label = mContext.getString(R.string.tct_rotation_label);
        if (hasCall)
        {
            state.icon = ResourceIcon.get(R.drawable.ic_qs_degrees_rotation_off);
        } else
        {
            state.icon = state.value ? ResourceIcon.get(R.drawable.ic_qs_degrees_rotation_on)
                    : ResourceIcon.get(R.drawable.ic_qs_degrees_rotation_off);
        }
        state.contentDescription = mContext.getString(R.string.tct_rotation_label);
    }

    private final RotationPolicy.RotationPolicyListener mRotationPolicyListener = new RotationPolicy.RotationPolicyListener() {
        @Override
        public void onChange()
        {
            refreshState();
        }
    };

    public int getMetricsCategory()
    {
        return 240;
    }

    @Override
    public Intent getLongClickIntent()
    {
       /* MODIFIED-BEGIN by dongdong.li, 2016-11-17,BUG-3471697*/
       // return new Intent(Settings.ACTION_DISPLAY_SETTINGS);
       return new Intent("android.settings.DISPLAY_SETTINGS_QS");
       /* MODIFIED-END by dongdong.li,BUG-3471697*/
    }

    @Override
    public CharSequence getTileLabel()
    {
        return getState().label;
    }
}
