/* Copyright (C) 2016 Tcl Corporation Limited */
package com.android.systemui.aod.style;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import java.util.Calendar;


public class TimeStyleFive extends View {

    private int width;
    private int height;
    private int[][] dateNum;
    private CalendarState[][] calendarStates;
    private int year;
    private int month;
    private DrawCalendar drawCalendar;
    /* MODIFIED-BEGIN by song.huan, 2016-10-11,BUG-3000498*/
    private float dateNumWidth = 10.0f;
    private int fontSize = 45;
    /* MODIFIED-END by song.huan,BUG-3000498*/
    private int currentMonthFontColor = Color.WHITE;
    private int weekFontColor = Color.WHITE;
    private int noCurrentMonthFontColor = Color.TRANSPARENT;
    private int todayFontColor = Color.BLACK;
    private char[] week = {'S', 'M', 'T', 'W', 'T', 'F', 'S'};

    public enum CalendarState {
        TODAY, CURRENT_MONTH, NO_CURRENT_MONTH, WEEK
    }

    public TimeStyleFive(Context context) {
        super(context);
        initUI(context);
    }

    public TimeStyleFive(Context context, AttributeSet attrs) {
        super(context, attrs);
        initUI(context);
    }

    public TimeStyleFive(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initUI(context);
    }

    private void initUI(Context context) {
        year = getYear();
        month = getMonth();
        calendarStates = new CalendarState[7][7];
        drawCalendar = new DrawCalendar(year, month);
    }

    public int getYear() {
        return Calendar.getInstance().get(Calendar.YEAR);
    }

    public int getMonth() {
        return Calendar.getInstance().get(Calendar.MONTH) + 1;
    }

    public int getCurrentMonthDay() {
        return Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
    }

    public int[][] getMonthNumFromDate(int year, int month) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month - 1, 1);

        int days[][] = new int[7][7];

        int firstDayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);

        int monthDaysNum = getMonthDaysNum(year, month);
        int lastMonthDaysNum = getLastMonthDaysNum(year, month);

        int dayNum = 1;
        int lastDayNum = 1;
        for (int i = 1; i < days.length; i++) {
            for (int j = 0; j < days[i].length; j++) {
                if (i == 1 && j < firstDayOfWeek - 1) {
                    days[i][j] = lastMonthDaysNum - firstDayOfWeek + 2 + j;
                } else if (dayNum <= monthDaysNum) {
                    days[i][j] = dayNum++;
                } else {
                    days[i][j] = lastDayNum++;
                }
            }
        }

        return days;

    }

    public int getLastMonthDaysNum(int year, int month) {

        int lastMonthDaysNum = 0;

        if (month == 1) {
            lastMonthDaysNum = getMonthDaysNum(year - 1, 12);
        } else {
            lastMonthDaysNum = getMonthDaysNum(year, month - 1);
        }
        return lastMonthDaysNum;

    }

    public int getMonthDaysNum(int year, int month) {

        if (year < 0 || month <= 0 || month > 12) {
            return -1;
        }

        //the number of days of each month in a year
        int[] array = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

        if (month != 2) {
            return array[month - 1];
        } else {
            if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0) {// judge the year is leap year or not
                return 29;
            } else {
                return 28;
            }
        }

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        // TODO Auto-generated method stub
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        width = getMeasuredWidth();
        dateNumWidth = width / 7.0f;
        height = (int) (dateNumWidth * 7);
        setMeasuredDimension(width, height);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        // TODO Auto-generated method stub
        super.onDraw(canvas);
        drawCalendar.drawCalendarCanvas(canvas);

    }

    class DrawCalendar {

        private Paint mPaintText;
        private Paint mPaintCircle;
        private float fontHeight;

        public DrawCalendar(int year, int month) {
            dateNum = getMonthNumFromDate(year, month);

            mPaintText = new Paint();
            mPaintText.setTextSize(fontSize);
            mPaintText.setColor(noCurrentMonthFontColor);
            mPaintText.setAntiAlias(true);

            Paint.FontMetrics fm = mPaintText.getFontMetrics();
            fontHeight = (float) Math.ceil(fm.descent - fm.top)/2;

            //set the style of today, blue circle
            mPaintCircle = new Paint();
            mPaintCircle.setColor(Color.WHITE);
            mPaintCircle.setAntiAlias(true);
        }


        public void drawCalendarCanvas(Canvas canvas) {

            for (int i = 0; i < dateNum.length; i++) {
                for (int j = 0; j < dateNum[i].length; j++) {
                    if (i == 0){// draw the week in the first row
                        drawCalendarCell(i, j, CalendarState.WEEK,
                                canvas);
                    } else if (i == 1 && dateNum[i][j] > 20) {// the days of last month

                        drawCalendarCell(i, j, CalendarState.NO_CURRENT_MONTH,
                                canvas);
                    } else if ((i == 6 || i == 5) && dateNum[i][j] < 20) {// the days of next month

                        drawCalendarCell(i, j, CalendarState.NO_CURRENT_MONTH,
                                canvas);
                    } else {// the days of current month
                        if (dateNum[i][j] == getCurrentMonthDay()) {
                            if (year == getYear()
                                    && month == getMonth()) {
                                drawCalendarCell(i, j, CalendarState.TODAY,
                                        canvas);
                            }

                        } else {
                            drawCalendarCell(i, j, CalendarState.CURRENT_MONTH,
                                    canvas);
                        }
                    }
                }
            }
        }

        private void drawCalendarCell(int i, int j, CalendarState state,
                                      Canvas canvas) {
            switch (state) {
                case WEEK:
                    calendarStates[i][j] = CalendarState.WEEK;
                    mPaintText.setColor(weekFontColor);
                    break;
                case TODAY:
                    calendarStates[i][j] = CalendarState.TODAY;
                    mPaintText.setColor(todayFontColor);
                    canvas.drawCircle(dateNumWidth * j + dateNumWidth / 2,
                            dateNumWidth * i + dateNumWidth / 2, dateNumWidth / 2,
                            mPaintCircle);
                    break;
                case CURRENT_MONTH:
                    calendarStates[i][j] = CalendarState.CURRENT_MONTH;
                    mPaintText.setColor(currentMonthFontColor);
                    break;
                case NO_CURRENT_MONTH:
                    calendarStates[i][j] = CalendarState.NO_CURRENT_MONTH;
                    mPaintText.setColor(noCurrentMonthFontColor);
                    break;
                default:
                    break;
            }

            if(state == CalendarState.WEEK){
                canvas.drawText(week[j] + "", dateNumWidth * j + dateNumWidth
                                / 2 - mPaintText.measureText(dateNum[i][j] + "") / 2,
                        dateNumWidth * i + dateNumWidth / 2 + fontHeight / 2.0f,
                        mPaintText);
            }else {
                canvas.drawText(dateNum[i][j] + "", dateNumWidth * j + dateNumWidth
                                / 2 - mPaintText.measureText(dateNum[i][j] + "") / 2,
                        dateNumWidth * i + dateNumWidth / 2 + fontHeight / 2.0f,
                        mPaintText);
            }

        }
    }


}
