/* Copyright (C) 2016 Tcl Corporation Limited */
/* ***************************************************************************/
/*                                                       Date : Feb 2, 2015  */
/*                      Android Resource Customization Tool                  */
/*              Copyright (c) 2015 JRD Communications, Inc.                  */
/* ***************************************************************************/
/*                                                                           */
/*    This material is company confidential, cannot be reproduced in any     */
/*    form without the written permission of JRD Communications, Inc.        */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/*   Author :  Hanwu.xie (XHANWU)                                            */
/*   Role :    Telecom Leader                                                */
/*   Reference documents :                                                   */
/*---------------------------------------------------------------------------*/
/* Comments :                                                                */
/*     file    : AttrNameBroadcastReceiver.java                           */
/*     Labels  :                                                             */
/*===========================================================================*/
/* Modifications   (month/day/year)                                          */
/*---------------------------------------------------------------------------*/
/* date    | author    | modification                                        */
/*---------+-----------+-----------------------------------------------------*/
/*02/02/15 | hanwu.xie | on creation                                         */
/*---------+-----------+-----------------------------------------------------*/
/*         |           |                                                     */
/*===========================================================================*/
/* Problems Report                                                           */
/*---------------------------------------------------------------------------*/
/* date    | author    | PR #     |                                          */
/*---------|-----------|----------|------------------------------------------*/
/*         |           |          |                                          */
/*---------|-----------|----------|------------------------------------------*/
/*         |           |          |                                          */
/*===========================================================================*/

package com.android.systemui.jrdRefBuilder;

import android.content.Context;
import android.content.Intent;
import android.content.BroadcastReceiver;
import static com.android.internal.telephony.TelephonyIntents.SECRET_CODE_ACTION;
import android.os.Build;
import android.os.SystemProperties;

public class AttrNameBroadcastReceiver extends BroadcastReceiver {
    // private static final boolean IS_USER_BUILD = "user".equals(Build.TYPE) ||
    // "userdebug".equals(Build.TYPE);
    static final String BOOTUP = "android.intent.action.BOOT_COMPLETED";
    boolean mIsDetectToolStart = SystemProperties.getBoolean("persist.sys.isToolStart", false);

    public AttrNameBroadcastReceiver() {
        // donothing
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(SECRET_CODE_ACTION) && !mIsDetectToolStart) {
            /*
             * If user input *#*#2618888#*#* on dialer, we need prompt user to
             * restart the phone
             */

            /* MODIFIED-BEGIN by jianguang.sun, 2016-09-12,BUG-2856089*/
            boolean fpenable = context.getResources().getBoolean(com.android.internal.R.bool.feature_attrname_enabled);
            if(!fpenable){
              return;
            }
            /* MODIFIED-END by jianguang.sun,BUG-2856089*/
            Intent activityIntent = new Intent();
            activityIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            activityIntent.setClassName("com.android.systemui",
                    "com.android.systemui.jrdRefBuilder.AttrNamePromptActivity");
            activityIntent.putExtra("ToStartTool", true);
            context.startActivity(activityIntent);
        }
    }

}
