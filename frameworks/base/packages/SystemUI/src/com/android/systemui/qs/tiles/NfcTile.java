/* Copyright (C) 2016 Tcl Corporation Limited */
/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/******************************************************************************/
/*                                                               Date:12/2014 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2014 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  yulong.zhang                                                    */
/*  Email  : yulong.zhang@jrdcom.com                                          */
/*  Role   :  SystemUI                                                        */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/*12/08/2014 |       yulong.zhang   |      FR-847639       |add a switch      */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/

package com.android.systemui.qs.tiles;

import com.android.systemui.*;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NfcAdapter;
import android.provider.Settings;

import com.android.internal.logging.MetricsLogger;
import com.android.internal.logging.MetricsProto.MetricsEvent;
import com.android.settingslib.WirelessUtils;
import com.android.systemui.Prefs;
import com.android.systemui.R;
import com.android.systemui.qs.QSTile;
import com.android.systemui.statusbar.policy.NfcController;
import android.util.Log;

/** Quick settings tile: NFC **/
public class NfcTile extends QSTile<QSTile.BooleanState> {
    private final NfcController mController;
    //private final UsageTracker mUsageTracker;
    private final Callback mCallback = new Callback();
    private boolean isAlwaysShowNFC;//add by kun.gao for defect 1031699
    private static boolean mAlwaysShow;//add by jiajunwu for Task1032512 2015-12-25
    public NfcTile(Host host) {
        super(host);
        mController = host.getNfcController();
        //modify by kun.gao@tcl.com for defect928806 begin
        //mUsageTracker = newUsageTracker(host.getContext());
//        mUsageTracker = new UsageTracker(host.getContext(), "LastUsed",NfcTile.class, R.integer.days_to_show_hotspot_tile);//Edited by ping.li for FR 522968 10/26/2015
        //modify by kun.gao@tcl.com for defect928806 end
        //mUsageTracker.setListening(true);
        isAlwaysShowNFC = mContext.getResources().getBoolean(R.bool.config_always_show_nfc_tile_in_quicksettings);//add by kun.gao for defect 1031699
        //add by jiajunwu for Task1032512 2015-12-25
        mAlwaysShow = mContext.getResources().getBoolean(R.bool.feature_systemui_quicksettings_display_show_forever);
    }

    @Override
    protected void handleDestroy() {
        super.handleDestroy();
        //mUsageTracker.setListening(false);
    }

    @Override
    public void setListening(boolean listening) {
        if (listening) {
            mController.addCallback(mCallback);
            refreshState();
        } else {
            mController.removeCallback(mCallback);
        }
    }

    @Override
    public Intent getLongClickIntent() {
        return new Intent(Settings.ACTION_NFC_SETTINGS);
    }

    @Override
    public CharSequence getTileLabel() {
        return mContext.getString(R.string.nfc_label);
    }

    @Override
    public BooleanState newTileState() {
        return new BooleanState();
    }

    @Override
    protected void handleClick() {
        if (WirelessUtils.isRadioAllowed(mContext,Settings.Global.RADIO_NFC)) { //Added by liping for task 1123166
            final boolean isEnabled = (Boolean) mState.value;
            MetricsLogger.action(mContext, getMetricsCategory(), !isEnabled);
            mController.setNfcEnabled(!isEnabled);
        }
    }

    @Override
    protected void handleUpdateState(BooleanState state, Object arg) {
        //add by jiajunwu for Task1032512 2015-12-25 begin
        if(mAlwaysShow){
           state.visible = mController.isNfcSupported();
           }else{
           state.visible = mController.isNfcSupported() && (/*mUsageTracker.isRecentlyUsed() ||*/ isAlwaysShowNFC);//modify by kun.gao for defect 1031699
          }
        //add by jiajunwu for Task1032512 2015-12-25 end
        state.label = mContext.getString(R.string.nfc_label);
        state.value = mController.isNfcEnabled();
        state.icon = state.visible && state.value ? ResourceIcon.get(R.drawable.ic_nfc_on)
                                                  : ResourceIcon.get(R.drawable.ic_nfc_off);
        state.contentDescription = state.visible && state.value ? mContext.getString(R.string.accessibility_quick_settings_nfc_changed_on)
                :mContext.getString(R.string.accessibility_quick_settings_nfc_changed_off);//add by jiajunwu for Defect937846 2015-12-05
    }

    //add by jiajunwu for Defect937846 2015-12-05 begin
    @Override
    protected String composeChangeAnnouncement() {
        if (mState.value) {
            return mContext.getString(R.string.accessibility_quick_settings_nfc_changed_on);
        } else {
            return mContext.getString(R.string.accessibility_quick_settings_nfc_changed_off);
        }
    }
    //add by jiajunwu for Defect937846 2015-12-05 end

    @Override
    public int getMetricsCategory() {
        return MetricsEvent.QS_HOTSPOT;//Temp value, for no filed of QS_NFC in metric lib.
    }

    /*private static UsageTracker newUsageTracker(Context context) {
        return new UsageTracker(context, Prefs.Key.NFC_TILE_LAST_USED, NfcTile.class,
                R.integer.days_to_show_nfc_tile);//Edited by ping.li for FR 522968 10/26/2015
    }*/

    private final class Callback implements NfcController.Callback {
        @Override
        public void onNfcChanged(boolean enabled) {
            refreshState();
        }
    };

     /**
     * This will catch broadcasts for changes in nfc state so we can show
     * the nfc tile for a number of days after use.
     */
    /*public static class NfcChangedReceiver extends BroadcastReceiver {
        private UsageTracker mUsageTracker;

        @Override
        public void onReceive(Context context, Intent intent) {
            if (mUsageTracker == null) {
                //modify by kun.gao@tcl.com for defect928806 begin
                mUsageTracker = newUsageTracker(context);
//                mUsageTracker = new UsageTracker(context, Prefs.Key.NFC_TILE_LAST_USED, NfcTile.class, R.integer.days_to_show_nfc_tile);//Edited by ping.li for FR 522968 10/26/2015
                //modify by kun.gao@tcl.com for defect928806 end
            }
            mUsageTracker.trackUsage();
        }
    }*/
}
