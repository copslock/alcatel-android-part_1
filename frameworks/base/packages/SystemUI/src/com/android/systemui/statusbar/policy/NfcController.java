/* Copyright (C) 2016 Tcl Corporation Limited */
/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/******************************************************************************/
/*                                                               Date:12/2014 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2014 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  yulong.zhang                                                    */
/*  Email  : yulong.zhang@jrdcom.com                                          */
/*  Role   :  SystemUI                                                        */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/*12/08/2014 |       yulong.zhang   |      FR-847639       |add a switch      */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/
package com.android.systemui.statusbar.policy;

public interface NfcController {
    void addCallback(Callback mCallback);
    void removeCallback(Callback callback);
    boolean isNfcEnabled();
    boolean isNfcSupported();
    void setNfcEnabled(boolean enabled);

    public interface Callback {
        void onNfcChanged(boolean enabled);
    }
}
