/**
 * Copyright (c) 2016, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.android.systemui.tuner;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.Nullable;
import android.app.UiModeManager;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings.Secure;
import android.support.v14.preference.PreferenceFragment;
import android.support.v14.preference.SwitchPreference;
import android.support.v7.preference.Preference;
import android.support.v7.preference.Preference.OnPreferenceChangeListener;
import android.text.format.DateFormat;
import android.util.Log;
import android.util.MathUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Switch;
import com.android.internal.logging.MetricsLogger;
import com.android.internal.logging.MetricsProto.MetricsEvent;
import com.android.systemui.R;
import com.android.systemui.statusbar.policy.NightModeController;
import com.android.systemui.statusbar.policy.NightModeController.Listener;
import com.android.systemui.tuner.TunerService.Tunable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class NightModeFragment extends PreferenceFragment implements Tunable,
        Listener, OnPreferenceChangeListener {

    private static final String TAG = "NightModeFragment";

    public static final String EXTRA_SHOW_NIGHT_MODE = "show_night_mode";

    private static final CharSequence KEY_AUTO = "auto";
    private static final CharSequence KEY_ADJUST_TINT = "adjust_tint";
    private static final CharSequence KEY_ADJUST_BRIGHTNESS = "adjust_brightness";
    //Add-Begin by Tao.Jiang, 2016/9/8, Task-2759036
    private static final CharSequence KEY_TINT_AMOUNT = "tint_amount";
    private static final CharSequence KEY_MANUAL = "manual";
    private static final CharSequence KEY_SCHEDULE = "schedule";
    //Add-End by Tao.Jiang

//    private Switch mSwitch;

    private NightModeController mNightModeController;
    private SwitchPreference mAutoSwitch;
//    private SwitchPreference mAdjustTint;
//    private SwitchPreference mAdjustBrightness;
    private UiModeManager mUiModeManager;
    //Add-Begin by Tao.Jiang, 2016/9/8, Task-2759036
    private SwitchPreference mManualSwitch;
    private TintAmountPreferences mTintAmountPreferences;
    private ValueAnimator mTintAnimator;
    private Preference mSchedulePref;
    //Add-End by Tao.Jiang

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        mNightModeController = new NightModeController(getContext());
        mUiModeManager = getContext().getSystemService(UiModeManager.class);
    }

//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//            Bundle savedInstanceState) {
//        Log.d(TAG, "onCreateView");
//        final View view = LayoutInflater.from(getContext()).inflate(
//                R.layout.night_mode_settings, container, false);
//        ((ViewGroup) view).addView(super.onCreateView(inflater, container, savedInstanceState));
//        return view;
//    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        Log.d(TAG, "onCreatePreferences");
        final Context context = getPreferenceManager().getContext();

        addPreferencesFromResource(R.xml.night_mode);
        mAutoSwitch = (SwitchPreference) findPreference(KEY_AUTO);
        mAutoSwitch.setOnPreferenceChangeListener(this);
        //Add-Begin by Tao.Jiang, 2016/9/20, Task: 2759036
        mSchedulePref = findPreference(KEY_SCHEDULE);
        mSchedulePref.setEnabled(mAutoSwitch.isChecked());
        //Add-End by Tao.Jiang
        //Del-Begin by Tao.Jiang, 2016/9/8, Task-2759036
//        mAdjustTint = (SwitchPreference) findPreference(KEY_ADJUST_TINT);
//        mAdjustTint.setOnPreferenceChangeListener(this);
//        mAdjustBrightness = (SwitchPreference) findPreference(KEY_ADJUST_BRIGHTNESS);
//        mAdjustBrightness.setOnPreferenceChangeListener(this);
        //Del-End by Tao.Jiang
        //Add-Begin by Tao.Jiang, 2016/9/8, Task-2759036
        mManualSwitch = (SwitchPreference) findPreference(KEY_MANUAL);
        mManualSwitch.setOnPreferenceChangeListener(this);
        mTintAmountPreferences = (TintAmountPreferences) findPreference(KEY_TINT_AMOUNT);
        mTintAmountPreferences.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
            {
                if (fromUser)
                {
                    Log.d(TAG, "Tint onProgressChanged: " + progress);
                    float mAmount = ((float) progress / 100) + 0.5f;

                    // force display color matrix temporarily
                    applyDisplayTint(mAmount);;
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar)
            {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar)
            {
                Log.d(TAG, "onStopTrackingTouch: " + seekBar.getProgress());
                int mProgress = seekBar.getProgress();
                final float mAmount = ((float) mProgress / 100) + 0.5f;
                if (!mNightModeController.isEnabled())
                {
                    if(mTintAnimator != null && mTintAnimator.isRunning())
                    {
                        mTintAnimator.cancel();
                    }
                    mTintAnimator = ValueAnimator.ofFloat(mAmount, 0f);
                    mTintAnimator.setDuration(1000);
                    mTintAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                        @Override
                        public void onAnimationUpdate(ValueAnimator animation)
                        {
                            float value = (float) animation.getAnimatedValue();
                            if (value == 0f)
                            {
                                setNightModeTintAmount(mAmount);
                                return;
                            }
                            applyDisplayTint(value);
                        }
                    });
                    mTintAnimator.start();
                } else
                {
                    setNightModeTintAmount(mAmount);
                }
            }
        });
        //Add-End by Tao.Jiang
    }

    //Add-Begin by Tao.Jiang, 2016/9/8, Task-2759036
    private void applyDisplayTint(float mAmount)
    {
        Log.d(TAG, "applyDisplayTint:" + mAmount);
        float[] values = scaleValues(NightModeController.IDENTITY_MATRIX,
                NightModeController.NIGHT_VALUES, mAmount);
        TunerService.get(getContext()).setValue(Secure.ACCESSIBILITY_DISPLAY_COLOR_MATRIX,
                NightModeController.toString(values));
    }

    private void setNightModeTintAmount(float mAmount)
    {
        Log.d(TAG, "setNightModeTintAmount:" + mAmount);
        TunerService.get(getContext()).setValue(Secure.NIGHT_MODE_TINT_COLOR_AMOUNT,
                String.valueOf(mAmount));
    }
    //Add-End by Tao.Jiang

//    @Override
//    public void onViewCreated(View view, Bundle savedInstanceState) {
//        Log.d(TAG, "onViewCreated");
//        super.onViewCreated(view, savedInstanceState);
//        View switchBar = view.findViewById(R.id.switch_bar);
//        mSwitch = (Switch) switchBar.findViewById(android.R.id.switch_widget);
//        mSwitch.setChecked(mNightModeController.isEnabled());
        //Add-Begin by Tao.Jiang, 2016/8/16, Task: 2759036
//        mSwitch.setClickable(false);
        //Add-End by Tao.Jiang
//        switchBar.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                boolean newState = !mNightModeController.isEnabled();
//                MetricsLogger.action(getContext(), MetricsEvent.ACTION_TUNER_NIGHT_MODE, newState);
//                mNightModeController.setNightMode(newState);
//                mSwitch.setChecked(newState);
//            }
//        });
//    }

    @Override
    public void onResume() {
        Log.d(TAG, "onResume");
        super.onResume();
        MetricsLogger.visibility(getContext(), MetricsEvent.TUNER_NIGHT_MODE, true);
        mNightModeController.addListener(this);
        /* MODIFIED-BEGIN by y, 2016-11-21,BUG-3461726*/
        mTintAmountPreferences.setEnabled(false);
        if (mAutoSwitch.isChecked()){
            mManualSwitch.setEnabled(false);
            mTintAmountPreferences.setEnabled(false);
        }
        if (mManualSwitch.isChecked()){
            mAutoSwitch.setEnabled(false);
            mSchedulePref.setEnabled(false);
            mTintAmountPreferences.setEnabled(true);
        }
        /* MODIFIED-END by y,BUG-3461726*/
        TunerService.get(getContext()).addTunable(this, Secure.BRIGHTNESS_USE_TWILIGHT,
                NightModeController.NIGHT_MODE_ADJUST_TINT);
//        calculateDisabled(); //Del by Tao.Jiang, 2016/9/8, Task-2759036
        //Add-Begin by Tao.Jiang, 2016/9/20, Task: 2759036
        updateSchedulePref();
        //Add-End by Tao.Jiang
    }

    //Add-Begin by Tao.Jiang, 2016/9/20, Task: 2759036
    private void updateSchedulePref()
    {
        String currentPlan = TunerService.get(getContext()).getValue(
                Secure.NIGHT_MODE_CUSTOM_SCHEDULE_PLAN);
        if (currentPlan != null)
        {
            switch (currentPlan)
            {
                case "custom":
                    String mTurnOnTime = TunerService.get(getContext()).getValue(
                            Secure.NIGHT_MODE_CUSTOM_SCHEDULE_ON);
                    String mTurnOffTime = TunerService.get(getContext()).getValue(
                            Secure.NIGHT_MODE_CUSTOM_SCHEDULE_OFF);

                    Calendar c = Calendar.getInstance();
                    final SimpleDateFormat s = new SimpleDateFormat("HH:mm");

                    try
                    {
                        Date d = s.parse(mTurnOnTime);
                        c.set(Calendar.HOUR_OF_DAY, d.getHours());
                        c.set(Calendar.MINUTE, d.getMinutes());
                    } catch (ParseException e)
                    {
                        e.printStackTrace();
                    } catch (NullPointerException e)
                    {
                        e.printStackTrace();
                    }
                    String from = DateFormat.getTimeFormat(getContext()).format(c.getTime());

                    c = Calendar.getInstance();
                    try
                    {
                        Date d = s.parse(mTurnOffTime);
                        c.set(Calendar.HOUR_OF_DAY, d.getHours());
                        c.set(Calendar.MINUTE, d.getMinutes());
                    } catch (ParseException e)
                    {
                        e.printStackTrace();
                    } catch (NullPointerException e)
                    {
                        e.printStackTrace();
                    }
                    String to = DateFormat.getTimeFormat(getContext()).format(c.getTime());

                    String summary = from + " - " + to;
                    mSchedulePref.setSummary(summary);
                    break;
                case "sun":
                default:
                    mSchedulePref.setSummary(R.string.night_mode_sunset_plan);
                    break;
            }
        }
    }
    //Add-End by Tao.Jiang

    @Override
    public void onPause() {
        super.onPause();
        MetricsLogger.visibility(getContext(), MetricsEvent.TUNER_NIGHT_MODE, false);
        mNightModeController.removeListener(this);
        TunerService.get(getContext()).removeTunable(this);
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        //Add-Begin by Tao.Jiang, 2016/9/20, Task: 2759036
        if(mTintAnimator != null && mTintAnimator.isRunning())
        {
            mTintAnimator.cancel();
        }
        //Add-End by Tao.Jiang
        final Boolean value = (Boolean) newValue;
        if (mAutoSwitch == preference) {
            MetricsLogger.action(getContext(), MetricsEvent.ACTION_TUNER_NIGHT_MODE_AUTO, value);
            mNightModeController.setAuto(value);
            //Add-Begin by Tao.Jiang, 2016/9/20, Task: 2759036
            mSchedulePref.setEnabled(value);
            /* MODIFIED-BEGIN by y, 2016-11-21,BUG-3461726*/
            mManualSwitch.setEnabled(!value);
            if (value) mTintAmountPreferences.setEnabled(false);
            /* MODIFIED-END by y,BUG-3461726*/
            //Add-End by Tao.Jiang
        //Del-Begin by Tao.Jiang, 2016/9/8, Task-2759036
//        } else if (mAdjustTint == preference) {
//            MetricsLogger.action(getContext(),
//                    MetricsEvent.ACTION_TUNER_NIGHT_MODE_ADJUST_TINT, value);
//            mNightModeController.setAdjustTint(value);
//            postCalculateDisabled();
//        } else if (mAdjustBrightness == preference) {
//            MetricsLogger.action(getContext(),
//                    MetricsEvent.ACTION_TUNER_NIGHT_MODE_ADJUST_BRIGHTNESS, value);
//            TunerService.get(getContext()).setValue(Secure.BRIGHTNESS_USE_TWILIGHT,
//                    value ? 1 : 0);
//            postCalculateDisabled();
        //Del-End by Tao.Jiang
        //Add-Begin by Tao.Jiang, 2016/9/8, Task-2759036
        } else if (mManualSwitch == preference) {
            MetricsLogger.action(getContext(), MetricsEvent.ACTION_TUNER_NIGHT_MODE, value);
            mNightModeController.setNightMode(value);
            /* MODIFIED-BEGIN by y, 2016-11-21,BUG-3461726*/
            mAutoSwitch.setEnabled(!value);
            mTintAmountPreferences.setEnabled(value);
            if (value){
                mSchedulePref.setEnabled(false);

            }
            /* MODIFIED-END by y,BUG-3461726*/
        //Add-End by Tao.Jiang
        } else {
            return false;
        }
        return true;
    }

    //Del-Begin by Tao.Jiang, 2016/9/8, Task-2759036
//    private void postCalculateDisabled() {
//        // Post this because its the easiest way to wait for all state to be calculated.
//        getView().post(new Runnable() {
//            @Override
//            public void run() {
//                calculateDisabled();
//            }
//        });
//    }

//    private void calculateDisabled() {
//        int enabledCount = (mAdjustTint.isChecked() ? 1 : 0)
//                + (mAdjustBrightness.isChecked() ? 1 : 0);
//        if (enabledCount == 1) {
//            if (mAdjustTint.isChecked()) {
//                mAdjustTint.setEnabled(false);
//            } else {
//                mAdjustBrightness.setEnabled(false);
//            }
//        } else {
//            mAdjustTint.setEnabled(true);
//            mAdjustBrightness.setEnabled(true);
//        }
//    }
    //Del-End by Tao.Jiang

    @Override
    public void onTuningChanged(String key, String newValue) {
        //Del-Begin by Tao.Jiang, 2016/9/8, Task-2759036
//        if (Secure.BRIGHTNESS_USE_TWILIGHT.equals(key)) {
//            mAdjustBrightness.setChecked(newValue != null && Integer.parseInt(newValue) != 0);
//        } else if (NightModeController.NIGHT_MODE_ADJUST_TINT.equals(key)) {
//            // Default on.
//            mAdjustTint.setChecked(newValue == null || Integer.parseInt(newValue) != 0);
//        }
        //Del-End by Tao.Jiang
    }

    @Override
    public void onNightModeChanged() {
        //Mod-Begin by Tao.Jiang, 2016/9/8, Task-2759036
//        mSwitch.setChecked(mNightModeController.isEnabled());
        mManualSwitch.setChecked(mNightModeController.isEnabled());
        //Mod-End by Tao.Jiang
    }

    @Override
    public void onTwilightAutoChanged() {
        mAutoSwitch.setChecked(mNightModeController.isAuto());
    }

    //Add-Begin by Tao.Jiang, 2016/9/8, Task-2759036
    private float[] scaleValues(float[] identityMatrix, float[] nightValues, float amount)
    {
        float[] values = new float[identityMatrix.length];
        for (int i = 0; i < values.length; i++)
        {
            values[i] = MathUtils.lerp(identityMatrix[i], nightValues[i], amount);
        }
        return values;
    }
    //Add-End by Tao.Jiang
}
