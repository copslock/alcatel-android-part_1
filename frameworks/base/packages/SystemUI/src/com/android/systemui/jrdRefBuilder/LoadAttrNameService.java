/* Copyright (C) 2016 Tcl Corporation Limited */
/* ***************************************************************************/
/*                                                       Date : Feb 2, 2015  */
/*                      Android Resource Customization Tool                  */
/*              Copyright (c) 2015 JRD Communications, Inc.                  */
/* ***************************************************************************/
/*                                                                           */
/*    This material is company confidential, cannot be reproduced in any     */
/*    form without the written permission of JRD Communications, Inc.        */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/*   Author :  Hanwu.xie (XHANWU)                                                    */
/*   Role :    Telecom Leader                                                */
/*   Reference documents :                                                   */
/*---------------------------------------------------------------------------*/
/* Comments :                                                                */
/*     file    : PersoStringBuilderEx.java                                                */
/*     Labels  :                                                             */
/*===========================================================================*/
/* Modifications   (month/day/year)                                          */
/*---------------------------------------------------------------------------*/
/* date    | author    | modification                                        */
/*---------+-----------+-----------------------------------------------------*/
/*02/02/15 | hanwu.xie | on creation                                         */
/*---------+-----------+-----------------------------------------------------*/
/*         |           |                                                     */
/*===========================================================================*/
/* Problems Report                                                           */
/*---------------------------------------------------------------------------*/
/* date    | author    | PR #     |                                          */
/*---------|-----------|----------|------------------------------------------*/
/*         |           |          |                                          */
/*---------|-----------|----------|------------------------------------------*/
/*         |           |          |                                          */
/*===========================================================================*/
package com.android.systemui.jrdRefBuilder;


import android.app.AttrNameManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import android.os.IBinder;
import android.util.Slog;
import android.widget.Toast;

import android.os.SystemProperties;

public class LoadAttrNameService extends Service {
    private static final String TAG = "LoadAttrNameService";

    private AttrNameManager attrNameManager = null;
    private NotificationManager mNotificationManager;


    @Override
    public void onCreate() {
        super.onCreate();

        attrNameManager = (AttrNameManager)getSystemService(Context.JRD_ATTR_NAME_SERVICE);
        if (attrNameManager == null) {
            Toast.makeText(this,"getSystemService jrd attrName service is null", 2).show();
            return;
        }
        attrNameManager.setAttrNameServiceStart(true);

        //update notification information
        CharSequence title = "AttrName APP Setting";
        CharSequence message = "show strId, string attribute, export to excel,...";

        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification = new Notification();
        notification.icon = com.android.internal.R.drawable.stat_sys_adb;
        notification.when = 0;
        notification.flags = Notification.FLAG_ONGOING_EVENT;
        notification.tickerText = title;
        notification.defaults = 0; // please be quiet
        notification.sound = null;
        notification.vibrate = null;
        notification.priority = Notification.PRIORITY_DEFAULT;


        Intent intent = new Intent(this,AttrNameSettingActivity.class);
        PendingIntent pendingIntent=PendingIntent.getActivity(this, 0, intent, 0);
        notification.setLatestEventInfo(this, title, message, pendingIntent);

        mNotificationManager.notify(0, notification);

        //Register the receiver for shutdown
        //IntentFilter shutDownIntentFilter = new IntentFilter();
        //shutDownIntentFilter.addAction(Intent.ACTION_SHUTDOWN);
        //registerReceiver(mBroadcastReceiver, shutDownIntentFilter);

    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mNotificationManager.cancel(0);
    }


   /*
    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver(){
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if(action.equals(Intent.ACTION_SHUTDOWN)){
                //attrNameManager.setAttrNameServiceStart(false);
                //attrNameManager.setShowId(false);
                //attrNameManager.clearAllCache();
            }
        }

    };
   */
}
