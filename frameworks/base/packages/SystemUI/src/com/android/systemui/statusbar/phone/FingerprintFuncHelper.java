/* Copyright (C) 2016 Tcl Corporation Limited */
package com.android.systemui.statusbar.phone;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.Uri;
import android.os.UserHandle;
import android.provider.Contacts;
import android.provider.MediaStore;
import android.provider.Settings;
import android.view.MotionEvent;
import android.view.View;
import android.content.ContentValues;
import android.content.ContentProviderClient;
import android.os.RemoteException;
import android.util.Log;
import com.android.internal.R;
import com.android.internal.widget.LockPatternUtils;
import com.android.keyguard.KeyguardUpdateMonitor;
import com.android.systemui.statusbar.KeyguardIndicationController;
import com.android.systemui.statusbar.policy.FlashlightController;
import android.content.ComponentName; // MODIFIED by jianguang.sun, 2016-11-04,BUG-3224803

public class FingerprintFuncHelper implements FlashlightController.FlashlightListener{

    private Context mContext;
    private long mPreviousTime, mCurrentTime;
    private boolean mVisible = true;

    private ActivityStarter mActivityStarter;
    private FlashlightController mFlashlightController;
    private LockPatternUtils mLockPatternUtils;
    private KeyguardIndicationController mIndicationController;
    private KeyguardUpdateMonitor mUpdateMonitor;
    private static final Intent SECURE_CAMERA_INTENT =
            new Intent(MediaStore.INTENT_ACTION_STILL_IMAGE_CAMERA_SECURE)
                    .addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
    private static final Intent INSECURE_CAMERA_INTENT =
            new Intent(MediaStore.INTENT_ACTION_STILL_IMAGE_CAMERA);
    private static final int DOUBLE_TAP_TIMEOUT = 1200;

    private static final int NAVIGATE_TYPE_CAR = 1;
    private static final int NAVIGATE_TYPE_TRANSIT = 2;
    private static final int NAVIGATE_TYPE_WALK = 3;
    private static final int NAVIGATE_TYPE_BIKING = 4;
    //[FEATURE]-Add-BEGIN by TCTNB.(Chuanjun Chen), 10/11/2016, FEATURE-2792219 And TASk-2989788.
    private static final String TAG = "FuncHelper";
    private static final String SCHEME = "content";
    private static final String AUTHORITY = "com.tct.diagnostics.provider.diagnosticsinfo";
    private static final String TABLE_NAME = "diagnostics";
    private static final Uri CONTENT_URI = new Uri.Builder().scheme(SCHEME)
                .authority(AUTHORITY).path(TABLE_NAME).build();
    //[FEATURE]-Add-END by TCTNB.(Chuanjun Chen.

    public FingerprintFuncHelper(Context context) {
        mContext = context;
        mLockPatternUtils = new LockPatternUtils(mContext);
        mUpdateMonitor = KeyguardUpdateMonitor.getInstance(mContext);
    }

    public void setFlashlightController(FlashlightController flashlightController) {
        mFlashlightController = flashlightController;
    }

    public void setActivityStarter(ActivityStarter activityStarter) {
        mActivityStarter = activityStarter;
    }

    public void setKeyguardIndicationController(
            KeyguardIndicationController keyguardIndicationController) {
        mIndicationController = keyguardIndicationController;
    }


    public void handleEvent(int id, String selectedApp) {
        String mFuncName = "";
        if (id == R.id.func_torch) {
            mFuncName = "func_torch";
            mFlashlightController.setFlashlight(!mFlashlightController.isEnabled());
            return;
        }
        if (id == R.id.func_apps) {
            mFuncName = "func_apps";
            Log.d(TAG, "selectedApp:" + selectedApp);
            PackageManager pm = mContext.getPackageManager();
            Intent targetIntent = pm.getLaunchIntentForPackage(selectedApp);
            if (targetIntent != null) {
                mContext.startActivityAsUser(targetIntent, UserHandle.CURRENT);
            }
            return;
        }
        /* MODIFIED-BEGIN by jianguang.sun, 2016-11-05,BUG-2740734*/
        if (id == R.id.func_dial) {
            String number = selectedApp;
            Intent targetIntent = new Intent();
            if(number == null || number.equals("")) {
                targetIntent.putExtra("isNullNumber", true);
            } else {
                targetIntent.putExtra("isNullNumber", false);
                targetIntent.putExtra("number", number);
            }
            targetIntent.setClassName("com.android.systemui",
                    "com.android.systemui.statusbar.phone.CallContactActivity");
            mContext.startActivityAsUser(targetIntent, UserHandle.CURRENT);
            return;
        }
        /* MODIFIED-END by jianguang.sun,BUG-2740734*/
        Intent targetIntent = new Intent();
        boolean dismissShade = false;
        boolean canSkipBouncer = mUpdateMonitor.getUserCanSkipBouncer(
                KeyguardUpdateMonitor.getCurrentUser());
        boolean secure = mLockPatternUtils.isSecure(KeyguardUpdateMonitor.getCurrentUser());
        switch (id) {
            case R.id.func_voice_search:
                targetIntent.setAction(Intent.ACTION_VOICE_ASSIST);
                mFuncName = "func_voice_search";
                break;
            case R.id.func_call:
                targetIntent.setAction(Intent.ACTION_DIAL);
                mFuncName = "func_call";
                break;
            case R.id.func_yahoo_search:
                String mYahooUrl = mContext.getResources().getString(com.android.systemui.R.string.def_jrdsystemui_yahoo_search_url_default);
                targetIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(mYahooUrl));
                targetIntent.putExtra("isYahooSearch", true);
                mFuncName = "func_yahoo_search";
                break;
            case R.id.func_camera:
                dismissShade = secure && !canSkipBouncer;
                targetIntent = dismissShade ? SECURE_CAMERA_INTENT : INSECURE_CAMERA_INTENT;
                mFuncName = "func_camera";
                break;
            case R.id.func_recognise_song:
                targetIntent.setClassName("com.shazam.android",
                   "com.shazam.android.activities.MainActivity");
                targetIntent.setAction("com.shazam.android.intent.actions.START_TAGGING");
                mFuncName = "func_recognise_song";
                break;
            case R.id.func_timer:
                targetIntent.setAction("android.intent.action.SET_TIMER");
                // i.setPackage("com.android.deskclock");
                mFuncName = "func_timer";
                break;
            case R.id.func_magic_unlock:
                targetIntent.setAction("com.tct.magiclock.action.SETTINGS");
                mFuncName = "func_magic_unlock";
                break;
            case R.id.func_selfie:
                dismissShade = secure && !canSkipBouncer;
                if (dismissShade) {
                    targetIntent = SECURE_CAMERA_INTENT;
                    targetIntent.setClassName("com.tct.camera", "com.android.camera.SecureCameraActivity");
                    targetIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    targetIntent.setAction(MediaStore.INTENT_ACTION_STILL_IMAGE_CAMERA_SECURE);
                    targetIntent.putExtra("func_selfie",true);
                } else {
                    targetIntent.setAction("com.tct.camera.STARTFRONTCAMERA");
                }
                mFuncName = "func_selfie";
                break;
            case R.id.func_music:
                /* MODIFIED-BEGIN by jianguang.sun, 2016-11-04,BUG-3224803*/
                ComponentName componet = ComponentName.unflattenFromString(
                        mContext.getResources().getString(com.android.systemui.R.string.func_music_component));
                 targetIntent.setAction("android.media.action.MEDIA_PLAY_FROM_SEARCH");
                 targetIntent.setComponent(componet);
                 /* MODIFIED-END by jianguang.sun,BUG-3224803*/
                break;
            case R.id.func_message_new:
                Uri smsToUri = Uri.parse("smsto:");
                targetIntent = new Intent(Intent.ACTION_SENDTO, smsToUri);
                targetIntent.putExtra("sms_body", "");
                mFuncName = "func_message_new";
                break;
            case R.id.func_email_new:
                Uri uri = Uri.parse("mailto:");
                targetIntent = new Intent(Intent.ACTION_SENDTO, uri);
                mFuncName = "func_email_new";
                break;
            case R.id.func_contact_new:
                targetIntent.setAction(Contacts.Intents.Insert.ACTION);
                targetIntent.setType(Contacts.People.CONTENT_TYPE);
                mFuncName = "func_contact_new";
                break;
            case R.id.func_event_new:
                targetIntent.setAction("com.google.android.calendar.EVENT_EDIT");
                targetIntent.setClassName("com.google.android.calendar", "com.android.calendar.AllInOneActivity"); // MODIFIED by jianguang.sun, 2016-11-04,BUG-3224803
                targetIntent.putExtra("createEditSource", "fab");
                targetIntent.putExtra("is_find_time_promo", false);
                break;
            case R.id.func_sound_record:
                targetIntent.setClassName("com.tct.soundrecorder", "com.tct.soundrecorder.SoundRecorder");
                targetIntent.putExtra("dowhat", "record");
                targetIntent.setAction(Intent.ACTION_MAIN);
                mFuncName = "func_sound_record";
                break;
            case R.id.func_navigate:
                targetIntent.setClassName("com.google.android.apps.maps",
                        "com.google.android.maps.driveabout.app.DestinationActivity");
                break;
            case R.id.func_alarm:
                targetIntent.setAction("android.intent.action.SET_ALARM");
                mFuncName = "func_alarm";
                break;
            case R.id.func_calculator:
                dismissShade = secure && !canSkipBouncer;
                targetIntent.setClassName("com.tct.calculator",
                        "com.tct.calculator.Calculator");
                targetIntent.putExtra("showByLocked", true);
                targetIntent.putExtra("IsSecure", dismissShade);
                mFuncName = "func_calculator";
                break;
            case R.id.func_settings:
                mFuncName = "func_settings";
            default:
                targetIntent.setAction("android.settings.FUNC_SETTINGS");
                break;
        }
        if (targetIntent != null) {
            mContext.startActivityAsUser(targetIntent, UserHandle.CURRENT);
        }

    }

    //[FEATURE]-Add-BEGIN by TCTNB.(Chuanjun Chen), 10/11/2016, FEATURE-2792219 And TASk-2989788.
    private void saveEachFuncUsage(String FuncName) {
        if (FuncName.length() <= 1) {
            return;
        }
        ContentValues values = new ContentValues();
        values.put("action","ADD");
        values.put(("FUNC_TOTAL_"+FuncName.replace(" ", "_")).toUpperCase(),1);
        ContentProviderClient provider   = mContext.getContentResolver().acquireUnstableContentProviderClient(AUTHORITY);
        try {
            if(provider!=null){
                provider.update(CONTENT_URI, values, null, null);
            }
        } catch (IllegalArgumentException e) {
            Log.d(TAG,"llegalArgumentException Exception: " + e);
        } catch (RemoteException e) {
            Log.d(TAG, "write2DB() RemoteException Exception: " + e);
            //Add by quan.zhai for Defect: 1816237 begin
        } catch (NullPointerException e){
            Log.d(TAG, "NullPointerException Exception: " + e);
            //Add by quan.zhai for Defect: 1816237 end
        } finally {
            if(provider!=null){
                provider.release();
            }
        }
    }
    //[FEATURE]-Add-END by TCTNB.(Chuanjun Chen).

    public void onVisibilityChanged(int visibility) {
        boolean vis = visibility == View.VISIBLE;
        if (vis && !mVisible) { // from invisible/gone to visible
            //updateFuncView();
            if (mFlashlightController != null) mFlashlightController.addListener(this);
        } else if (!vis && mVisible) { // from visible to invisible/gone
            if (mFlashlightController != null) mFlashlightController.removeListener(this);
        }
        mVisible = vis;
    }


    public boolean isFuncVisible() {
        return Settings.System.getIntForUser(mContext.getContentResolver(),
                Settings.System.TCT_FUNC, 1, UserHandle.USER_CURRENT) == 1;
    }

    @Override
    public void onFlashlightChanged(boolean enabled) {
       // hostView.updateTorch(enabled);
    }

    @Override
    public void onFlashlightError() {}

    @Override
    public void onFlashlightAvailabilityChanged(boolean available) {}

}
