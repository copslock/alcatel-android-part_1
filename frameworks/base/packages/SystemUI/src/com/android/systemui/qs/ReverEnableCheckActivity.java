/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
//[BUGFIX]-Add file by TSCD.biao.tang,2015-04-10,PR-973571

package com.android.systemui.qs;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.os.Bundle;
import com.android.internal.app.AlertActivity;
import com.android.internal.app.AlertController;
import com.android.internal.view.RotationPolicy;
import com.android.systemui.R;
import android.provider.Settings;
import android.view.WindowManager;
import android.widget.TextView;//add by quanxiang.liu@jrdcom.com for defect1125981,2015-12-21

public class ReverEnableCheckActivity extends AlertActivity implements
        DialogInterface.OnClickListener {
    private static final String TAG = "ReverEnableCheckActivity";

    private CheckBox mAlwaysAllow;

    private final String KEY_NEVER_SHOW = "never_show_ReverEnableCheckActivity";

    @Override
    public void onCreate(Bundle icicle)
    {
        super.onCreate(icicle);

        final AlertController.AlertParams ap = mAlertParams;
        ap.mTitle = getString(R.string.rever_enable_check_diag_title);
        ap.mPositiveButtonText = getString(android.R.string.ok);
        ap.mNegativeButtonText = getString(android.R.string.cancel);
        ap.mPositiveButtonListener = this;
        ap.mNegativeButtonListener = this;

        // add "always allow" checkbox
        LayoutInflater inflater = LayoutInflater.from(ap.mContext);
        View customView = inflater.inflate(R.layout.reversible, null);
        TextView text = (TextView) customView.findViewById(R.id.reversible);
        text.setText(R.string.rever_enable_check_diag_msg);
        mAlwaysAllow = (CheckBox) customView.findViewById(R.id.alwaysUse);
        mAlwaysAllow.setText(R.string.rever_enable_check_never_remind);
        ap.mView = customView;

        setupAlert();
    }

    @Override
    protected void onStop()
    {
        super.onStop();
    }

    @Override
    public void onClick(DialogInterface dialog, int which)
    {
        boolean allow = (which == AlertDialog.BUTTON_POSITIVE);
        boolean alwaysAllow = allow && mAlwaysAllow.isChecked();

        // allow == true, never show
        if (alwaysAllow)
        {
            Settings.System.putInt(getContentResolver(), KEY_NEVER_SHOW, 1);
        }

        if (allow)
        {
            RotationPolicy.enableReversibleRotation(this, true);
        }
        finish();
    }
}
