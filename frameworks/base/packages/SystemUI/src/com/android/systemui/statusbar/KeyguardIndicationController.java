/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.android.systemui.statusbar;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.Color;
import android.hardware.fingerprint.FingerprintManager;
import android.os.BatteryManager;
import android.os.BatteryStats;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.UserHandle;
import android.text.TextUtils;
import android.text.format.Formatter;
import android.util.Log;
import android.view.View;

import com.android.internal.app.IBatteryStats;
import com.android.keyguard.KeyguardUpdateMonitor;
import com.android.keyguard.KeyguardUpdateMonitorCallback;
import com.android.systemui.R;
import com.android.systemui.statusbar.phone.KeyguardIndicationTextView;
import com.android.systemui.statusbar.phone.LockIcon;
import com.android.systemui.statusbar.phone.StatusBarKeyguardViewManager;

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/26/2016, SOLUTION-2520381
//[Orange][Android guidelines]Add network name in the status bar and in the lockscreen
import com.android.keyguard.CarrierText;
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

/**
 * Controls the indications and error messages shown on the Keyguard
 */
public class KeyguardIndicationController {

    private static final String TAG = "KeyguardIndication";
    private static final boolean DEBUG_CHARGING_SPEED = false;

    private static final int MSG_HIDE_TRANSIENT = 1;
    private static final int MSG_CLEAR_FP_MSG = 2;
    //[FEATURE]-ADD-BEGIN by TCTNB(Guoqiang.Qiu), 2016-10-19, Task-3159146
    private static final int MSG_SHOW_TRANSIENT = 3;
    //[FEATURE]-ADD-END by TCTNB(Guoqiang.Qiu)
    private static final long TRANSIENT_FP_ERROR_TIMEOUT = 1300;

    private final Context mContext;
    private final KeyguardIndicationTextView mTextView;
    private final IBatteryStats mBatteryInfo;

    private final int mSlowThreshold;
    private final int mFastThreshold;
    private final LockIcon mLockIcon;
    private StatusBarKeyguardViewManager mStatusBarKeyguardViewManager;

    private String mRestingIndication;
    private String mTransientIndication;
    private int mTransientTextColor;
    private boolean mVisible;

    private boolean mPowerPluggedIn;
    private boolean mPowerCharged;
    private int mChargingSpeed;
    private int mChargingWattage;
    private String mMessageToShowOnScreenOn;

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/26/2016, SOLUTION-2520381
//[Orange][Android guidelines]Add network name in the status bar and in the lockscreen
    //Task528454
    private CarrierText mOrangeF = null;
    private boolean isOrangeFDisplayed = false;
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/26/2016, SOLUTION-2520381
//[Orange][Android guidelines]Add network name in the status bar and in the lockscreen
    public KeyguardIndicationController(Context context, KeyguardIndicationTextView textView, CarrierText orangeName,
                                        LockIcon lockIcon) {
        mOrangeF = orangeName;
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
        mContext = context;
        mTextView = textView;
        mLockIcon = lockIcon;

        Resources res = context.getResources();
        mSlowThreshold = res.getInteger(R.integer.config_chargingSlowlyThreshold);
        mFastThreshold = res.getInteger(R.integer.config_chargingFastThreshold);


        mBatteryInfo = IBatteryStats.Stub.asInterface(
                ServiceManager.getService(BatteryStats.SERVICE_NAME));
        KeyguardUpdateMonitor.getInstance(context).registerCallback(mUpdateMonitor);
        context.registerReceiverAsUser(mReceiver, UserHandle.SYSTEM,
                new IntentFilter(Intent.ACTION_TIME_TICK), null, null);

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/26/2016, SOLUTION-2520381
//[Orange][Android guidelines]Add network name in the status bar and in the lockscreen
        //Task528454
        if(null != mContext) {
            isOrangeFDisplayed = mContext.getResources().getBoolean(com.android.internal.R.bool.feature_keyguard_jrd_orange_f_indication);
        }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
    }

    public void setVisible(boolean visible) {
        mVisible = visible;
        mTextView.setVisibility(visible ? View.VISIBLE : View.GONE);
        if (visible) {
            hideTransientIndication();
            updateIndication();
        }
    }

    //[FEATURE]-ADD-BEGIN by TCTNB(Guoqiang.Qiu), 2016-10-19, Task-3159146
    public void showIndicationDelayed(int delayMs) {
        mHandler.sendEmptyMessageDelayed(MSG_SHOW_TRANSIENT, delayMs);
    }
    //[FEATURE]-ADD-END by TCTNB(Guoqiang.Qiu)

    /**
     * Sets the indication that is shown if nothing else is showing.
     */
    public void setRestingIndication(String restingIndication) {
        mRestingIndication = restingIndication;
        updateIndication();
    }

    /**
     * Hides transient indication in {@param delayMs}.
     */
    public void hideTransientIndicationDelayed(long delayMs) {
        mHandler.sendMessageDelayed(
                mHandler.obtainMessage(MSG_HIDE_TRANSIENT), delayMs);
    }

    /**
     * Shows {@param transientIndication} until it is hidden by {@link #hideTransientIndication}.
     */
    public void showTransientIndication(int transientIndication) {
        //[FEATURE]-ADD-BEGIN by TCTNB(Guoqiang.Qiu), 2016-10-19, Task-3159146
        if (transientIndication == R.string.keyguard_unlock) {
            setVisible(false);
            mHandler.removeMessages(MSG_SHOW_TRANSIENT);
            mLockIcon.update(true);
        } else {
        //[FEATURE]-ADD-END by TCTNB(Guoqiang.Qiu)
            showTransientIndication(mContext.getResources().getString(transientIndication));
        }
    }

    /**
     * Shows {@param transientIndication} until it is hidden by {@link #hideTransientIndication}.
     */
    public void showTransientIndication(String transientIndication) {
        showTransientIndication(transientIndication, Color.WHITE);
    }

    /**
     * Shows {@param transientIndication} until it is hidden by {@link #hideTransientIndication}.
     */
    public void showTransientIndication(String transientIndication, int textColor) {
        mTransientIndication = transientIndication;
        mTransientTextColor = textColor;
        mHandler.removeMessages(MSG_HIDE_TRANSIENT);
        updateIndication();
    }

    /**
     * Hides transient indication.
     */
    public void hideTransientIndication() {
        if (mTransientIndication != null) {
            mTransientIndication = null;
            mHandler.removeMessages(MSG_HIDE_TRANSIENT);
            updateIndication();
        }
    }

    private void updateIndication() {
        if (mVisible) {
//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/26/2016, SOLUTION-2520381
//[Orange][Android guidelines]Add network name in the status bar and in the lockscreen
            //Task528454-Li-Zhao merge FR893623 begin
            String indication = computeIndication();
            refreshOrangeFCarrierInfo(TextUtils.isEmpty(indication) ? View.VISIBLE : View.GONE);
            mTextView.switchIndication(indication);
            //Task528454-Li-Zhao merge FR893623 end
            mTextView.setTextColor(computeColor());
        }
    }

    private int computeColor() {
        if (!TextUtils.isEmpty(mTransientIndication)) {
            return mTransientTextColor;
        }
        return Color.WHITE;
    }

    private String computeIndication() {
        if (!TextUtils.isEmpty(mTransientIndication)) {
            return mTransientIndication;
        }
        if (mPowerPluggedIn) {
            String indication = computePowerIndication();
            if (DEBUG_CHARGING_SPEED) {
                indication += ",  " + (mChargingWattage / 1000) + " mW";
            }
            return indication;
        }
        return mRestingIndication;
    }

    private String computePowerIndication() {
        if (mPowerCharged) {
            return mContext.getResources().getString(R.string.keyguard_charged);
        }

        // Try fetching charging time from battery stats.
        long chargingTimeRemaining = 0;
        try {
            chargingTimeRemaining = mBatteryInfo.computeChargeTimeRemaining();

        } catch (RemoteException e) {
            Log.e(TAG, "Error calling IBatteryStats: ", e);
        }
        final boolean hasChargingTime = chargingTimeRemaining > 0;

        int chargingId;
        switch (mChargingSpeed) {
            case KeyguardUpdateMonitor.BatteryStatus.CHARGING_FAST:
                chargingId = hasChargingTime
                        ? R.string.keyguard_indication_charging_time_fast
                        : R.string.keyguard_plugged_in_charging_fast;
                break;
            case KeyguardUpdateMonitor.BatteryStatus.CHARGING_SLOWLY:
                chargingId = hasChargingTime
                        ? R.string.keyguard_indication_charging_time_slowly
                        : R.string.keyguard_plugged_in_charging_slowly;
                break;
            default:
                chargingId = hasChargingTime
                        ? R.string.keyguard_indication_charging_time
                        : R.string.keyguard_plugged_in;
                break;
        }

        //[SOLUTION]-Add-BEGIN by TCTNB.(Yubin.Ying), 08/18/2016, SOLUTION-2481824
        boolean removeQuickCharge = mContext.getResources().getBoolean(R.bool.feature_systemui_lockscreen_remove_quickcharge);
        if (removeQuickCharge) {
            chargingId = hasChargingTime
                    ? R.string.keyguard_indication_charging_time
                    : R.string.keyguard_plugged_in;
        }
        //[SOLUTION]-Add-END by TCTNB.(Yubin.Ying)

        if (hasChargingTime) {
            String chargingTimeFormatted = Formatter.formatShortElapsedTimeRoundingUpToMinutes(
                    mContext, chargingTimeRemaining);
            return mContext.getResources().getString(chargingId, chargingTimeFormatted);
        } else {
            return mContext.getResources().getString(chargingId);
        }
    }

    KeyguardUpdateMonitorCallback mUpdateMonitor = new KeyguardUpdateMonitorCallback() {
        @Override
        public void onRefreshBatteryInfo(KeyguardUpdateMonitor.BatteryStatus status) {
            boolean isChargingOrFull = status.status == BatteryManager.BATTERY_STATUS_CHARGING
                    || status.status == BatteryManager.BATTERY_STATUS_FULL;
            mPowerPluggedIn = status.isPluggedIn() && isChargingOrFull;
            mPowerCharged = status.isCharged();
            mChargingWattage = status.maxChargingWattage;
/* MODIFIED-BEGIN by xiang.ma, 2016-11-02,BUG-3280481*/
//            mChargingSpeed = status.getChargingSpeed(mSlowThreshold, mFastThreshold);
            mChargingSpeed = status.getChargingSpeed();
            /* MODIFIED-END by xiang.ma,BUG-3280481*/
            updateIndication();
        }

        @Override
        public void onFingerprintHelp(int msgId, String helpString) {
            KeyguardUpdateMonitor updateMonitor = KeyguardUpdateMonitor.getInstance(mContext);
            if (!updateMonitor.isUnlockingWithFingerprintAllowed()) {
                return;
            }
            int errorColor = mContext.getResources().getColor(R.color.system_warning_color, null);
            if (mStatusBarKeyguardViewManager.isBouncerShowing()) {
                mStatusBarKeyguardViewManager.showBouncerMessage(helpString, errorColor);
            } else if (updateMonitor.isDeviceInteractive()) {
                mLockIcon.setTransientFpError(true);
                showTransientIndication(helpString, errorColor);
                mHandler.removeMessages(MSG_CLEAR_FP_MSG);
                mHandler.sendMessageDelayed(mHandler.obtainMessage(MSG_CLEAR_FP_MSG),
                        TRANSIENT_FP_ERROR_TIMEOUT);
            }
        }

        @Override
        public void onFingerprintError(int msgId, String errString) {
            KeyguardUpdateMonitor updateMonitor = KeyguardUpdateMonitor.getInstance(mContext);
            if (!updateMonitor.isUnlockingWithFingerprintAllowed()
                    || msgId == FingerprintManager.FINGERPRINT_ERROR_CANCELED) {
                return;
            }
            int errorColor = mContext.getResources().getColor(R.color.system_warning_color, null);
            if (mStatusBarKeyguardViewManager.isBouncerShowing()) {
                mStatusBarKeyguardViewManager.showBouncerMessage(errString, errorColor);
            } else if (updateMonitor.isDeviceInteractive()) {
                    showTransientIndication(errString, errorColor);
                    // We want to keep this message around in case the screen was off
                    mHandler.removeMessages(MSG_HIDE_TRANSIENT);
                    hideTransientIndicationDelayed(5000);
             } else {
                    mMessageToShowOnScreenOn = errString;
            }
        }

        @Override
        public void onScreenTurnedOn() {
            if (mMessageToShowOnScreenOn != null) {
                int errorColor = mContext.getResources().getColor(R.color.system_warning_color,
                        null);
                showTransientIndication(mMessageToShowOnScreenOn, errorColor);
                // We want to keep this message around in case the screen was off
                mHandler.removeMessages(MSG_HIDE_TRANSIENT);
                hideTransientIndicationDelayed(5000);
                mMessageToShowOnScreenOn = null;
            }
            //[FEATURE]-ADD-BEGIN by TCTNB(Guoqiang.Qiu), 2016-10-19, Task-3159146
            setVisible(false);
            mHandler.sendEmptyMessageDelayed(MSG_SHOW_TRANSIENT, 2000);
            //[FEATURE]-ADD-END by TCTNB(Guoqiang.Qiu)
        }

        @Override
        public void onFingerprintRunningStateChanged(boolean running) {
            if (running) {
                mMessageToShowOnScreenOn = null;
            }
        }
    };

    BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (mVisible) {
                updateIndication();
            }
        }
    };

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == MSG_HIDE_TRANSIENT && mTransientIndication != null) {
                mTransientIndication = null;
                updateIndication();
            } else if (msg.what == MSG_CLEAR_FP_MSG) {
                mLockIcon.setTransientFpError(false);
                hideTransientIndication();
            //[FEATURE]-ADD-BEGIN by TCTNB(Guoqiang.Qiu), 2016-10-19, Task-3159146
            } else if (msg.what == MSG_SHOW_TRANSIENT) {
                setVisible(true);
            //[FEATURE]-ADD-END by TCTNB(Guoqiang.Qiu)
            }
        }
    };

    public void setStatusBarKeyguardViewManager(
            StatusBarKeyguardViewManager statusBarKeyguardViewManager) {
        mStatusBarKeyguardViewManager = statusBarKeyguardViewManager;
    }

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/26/2016, SOLUTION-2520381
//[Orange][Android guidelines]Add network name in the status bar and in the lockscreen
    //Task528454
    private void refreshOrangeFCarrierInfo(int visibility) {
        if(isOrangeFDisplayed && null != mOrangeF) {
            mOrangeF.setVisibility(visibility);
        }
    }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
}
