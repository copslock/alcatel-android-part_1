package com.vlife;

import android.content.Context;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * 微乐
 */
public class VlifeLockScreen {
    private static Class dynamicViewClass;

    public static ILockScreen createLockScreenInstance(final Context context) {
        try {
            if(dynamicViewClass==null) {
                Context appContext = context.createPackageContext(ILockScreen.packageName, Context.CONTEXT_INCLUDE_CODE | Context.CONTEXT_IGNORE_SECURITY);
                dynamicViewClass = appContext.getClassLoader().loadClass(ILockScreen.className);
            }
            Object object = dynamicViewClass.newInstance();
            Method[] methods=object.getClass().getMethods();
            if (object instanceof InvocationHandler) {
                ILockScreen dynamicView = (ILockScreen) Proxy.newProxyInstance(ILockScreen.class.getClassLoader(), new Class[]{ILockScreen.class}, (InvocationHandler) object);
                dynamicView.init(context);
                return dynamicView;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
