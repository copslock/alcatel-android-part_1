package com.android.systemui.wallshuffle;

import android.annotation.LayoutRes;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.AttributeSet;
import android.util.Log;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.Animation.AnimationListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.android.systemui.R;
import com.android.systemui.statusbar.phone.ActivityStarter;

//[SOLUTION]-ADD by TCTNB(Guoqiang.Qiu), 2016-8-25, Solution-2699694

public class ArcMenu extends RelativeLayout implements View.OnTouchListener, View.OnDragListener {
    private ArcLayout mArcLayout;

    private String TAG = "ArcMenu";

    private boolean isStartDrag = false;
    private boolean isDrop = true;
    private boolean disableUp = true;
    private boolean mState = false;//state of toggle
    private int mCurrentGuideLayout;
    private Context mContext;
    private ImageView magicFav;
    private ImageView mLike;
    private ImageView mPin;
    private ActivityStarter mActivityStarter;

    public ArcMenu(Context context) {
        this(context, null);
    }

    public ArcMenu(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        mArcLayout = (ArcLayout) findViewById(R.id.item_layout);
        final int N = mArcLayout.getChildCount();
        for (int i = 0; i < N; i++) {
            View item = mArcLayout.getChildAt(i);
            item.setOnDragListener(this);
        }

        mLike = (ImageView) findViewById(R.id.magic_like);
        mPin = (ImageView) findViewById(R.id.magic_pin);

        magicFav = (ImageView)findViewById(R.id.magiclock_favorite_btn);
        // magicFav.setClickable(true);
        magicFav.setOnTouchListener(this);

        magicFav.setOnLongClickListener(view -> {
            if (!mState) return false;
            isStartDrag = true;
            view.startDrag(null, new View.DragShadowBuilder(view), null, 0);
            return false;
        });
        magicFav.setOnDragListener((view, event) -> {
            if (!mState) return false;
            switch (event.getAction()) {
                case DragEvent.ACTION_DRAG_STARTED:
                    isStartDrag = false;
                    break;
                case DragEvent.ACTION_DRAG_ENDED:
                    if(isDrop && disableUp) {
                        mArcLayout.switchState(true);
                    }
                    isDrop = true;
                    break;
            }
            return true;
        });
    }

    @Override
    public boolean onDrag(View v, DragEvent event) {
        if (event.getAction() == DragEvent.ACTION_DROP) {
            Log.d(TAG, "ACTION_DROP"+v.getId());
            magicFav.setOnTouchListener(null);
            Animation animation = bindItemAnimation(v, true, 400);
            animation.setAnimationListener(new AnimationListener() {

                @Override
                public void onAnimationStart(Animation animation) {}

                @Override
                public void onAnimationRepeat(Animation animation) {}

                @Override
                public void onAnimationEnd(Animation animation) {
                    post(() -> itemDidDisappear());
                }
            });

            final int itemCount = mArcLayout.getChildCount();
            for (int i = 0; i < itemCount; i++) {
                View item = mArcLayout.getChildAt(i);
                if (v != item) {
                    bindItemAnimation(item, false, 300);
                }
            }
            isDrop = false;
            disableUp = false;
            mArcLayout.invalidate();

            switch (v.getId()) {
                case R.id.magic_like:
                    WallShuffleController.likePicture();
                    updatePinOrLike();
                    showMenuTips(R.layout.magic_guide_favourite);
                    break;
                case R.id.magic_pin:
                    WallShuffleController.pinPicture();
                    updatePinOrLike();
                    break;
                case R.id.magic_dislike:
                    dislikeWarining();
                    break;
            }
        }
        return true;
    }

    private Animation bindItemAnimation(final View child, final boolean isClicked, final long duration) {
        Animation animation = createItemDisapperAnimation(duration, isClicked);
        child.setAnimation(animation);

        return animation;
    }

    private void itemDidDisappear() {
        final int itemCount = mArcLayout.getChildCount();
        for (int i = 0; i < itemCount; i++) {
            View item = mArcLayout.getChildAt(i);
            item.clearAnimation();
            item.setVisibility(View.GONE);
        }
        mArcLayout.switchState(false);
        magicFav.setOnTouchListener(this);
    }

    private static Animation createItemDisapperAnimation(final long duration, final boolean isClicked) {
        AnimationSet animationSet = new AnimationSet(true);
        animationSet.addAnimation(new ScaleAnimation(1.0f, isClicked ? 2.0f : 0.0f, 1.0f, isClicked ? 2.0f : 0.0f,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f));
        animationSet.addAnimation(new AlphaAnimation(1.0f, 0.0f));

        animationSet.setDuration(duration);
        animationSet.setInterpolator(new DecelerateInterpolator());
        animationSet.setFillAfter(true);

        return animationSet;
    }

    private void shrinkAllItem() {
        final int itemCount = mArcLayout.getChildCount();
        for (int i = 0; i < itemCount; i++) {
            View item = mArcLayout.getChildAt(i);
            item.setVisibility(View.GONE);
        }

        mArcLayout.switchState(false);
    }

    public void switchState() {
        mArcLayout.switchState(true);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (!mState) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                Intent targetIntent = new Intent("com.tct.magiclock.action.SETTINGS");
                mActivityStarter.startActivity(targetIntent, false);
            }
            return false;
        }
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            showMenuTips(R.layout.magic_guide_menu);
            mArcLayout.switchState(true);
            disableUp = true;
        } else if (event.getAction() == MotionEvent.ACTION_UP) {
            if(disableUp) {
                shrinkAllItem();
            }
        } else if (event.getAction() == MotionEvent.ACTION_CANCEL) {
            Log.d(TAG, "ACTION_CANCEL"+"_isStartDrag == "+isStartDrag);
            if(!isStartDrag && disableUp) {
                shrinkAllItem();
            }
        }
        return false;
    }

    public void setState(boolean state) {
        mState = state;
        if (state) {
            updatePinOrLike();
            showMenuTips(R.layout.magic_guide_first);
        } else {
            magicFav.setImageResource(R.drawable.magic_btn_settings);
        }
    }

    public void setActivityStarter(ActivityStarter activityStarter) {
        mActivityStarter = activityStarter;
    }

    private void updatePinOrLike() {
        boolean like = WallShuffleController.stateLike;
        boolean pin = WallShuffleController.statePin;
        if (like && pin) {
            magicFav.setImageResource(R.drawable.magic_btn_both);
            mLike.setImageResource(R.drawable.magic_like);
            mPin.setImageResource(R.drawable.magic_pin);
        } else if (like) {
            magicFav.setImageResource(R.drawable.magic_btn_like);
            mLike.setImageResource(R.drawable.magic_like);
            mPin.setImageResource(R.drawable.magic_pin_off);
        } else if (pin) {
            magicFav.setImageResource(R.drawable.magic_btn_pin);
            mLike.setImageResource(R.drawable.magic_like_off);
            mPin.setImageResource(R.drawable.magic_pin);
        } else {
            magicFav.setImageResource(R.drawable.magic_btn_option);
            mLike.setImageResource(R.drawable.magic_like_off);
            mPin.setImageResource(R.drawable.magic_pin_off);
        }
    }

    private void dislikeWarining() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setMessage(R.string.magic_dialog_content);
        builder.setPositiveButton(R.string.yes,
                (dialog, which) -> WallShuffleController.dislikePicture());
        builder.setNegativeButton(R.string.cancel,
                (dialog, which) -> dialog.dismiss());
        AlertDialog dialog = builder.create();
        dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD_DIALOG);
        dialog.show();
    }

    /**
     * Inflate a guide view for arc menu.
     * @param layout ID for an XML layout resource to load.
     */
    private void showMenuTips(@LayoutRes int layout) {
        if (layout == mCurrentGuideLayout) return;
        mCurrentGuideLayout = layout;
        final String key = mContext.getResources().getResourceEntryName(layout);
        final SharedPreferences sp = mContext.getSharedPreferences(mContext.getPackageName(), Context.MODE_PRIVATE);
        boolean shouldShow = sp.getBoolean(key, true);
        if (!shouldShow) return;
        final View menuGuide = LayoutInflater.from(mContext).inflate(layout, this, false);
        View btnOk = menuGuide.findViewById(R.id.magic_guide_menu_ok);

        addView(menuGuide);
        btnOk.setOnClickListener(view -> {
            try {
                removeView(menuGuide);

                SharedPreferences.Editor ed = sp.edit();
                ed.putBoolean(key, false);
                ed.commit();

                menuGuide.setOnTouchListener(null);
            } catch (Exception e) {
                Log.e(TAG, "tips", e);
            }
        });

        // Add empty listener for guide to disable touch event on keyguard
        menuGuide.setOnTouchListener((view, event) -> true);
    }
}
