package com.android.systemui.wallshuffle;

import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.ContentObserver;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.UserHandle;
import android.provider.Settings;
import android.net.Uri;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;

import com.android.systemui.R;
import com.android.systemui.statusbar.phone.PhoneStatusBar;
import com.android.systemui.statusbar.phone.StatusBarWindowView;
import com.vlife.ILockScreen;
import com.vlife.VlifeLockScreen;

/**
 * [SOLUTION]-Created by TCTNB(Guoqiang.Qiu), 2016-8-25, Solution-2699694
 */
public class MagicUnlockManager implements WallShuffleController.OnChangeListener {
    private Context mContext;
    private StatusBarWindowView mStatusBarWindow;
    private FrameLayout mHostView;
    private View mask;
    private PhoneStatusBar mPhoneStatusBar;

    private ArcMenu mArcMenu;
    private View mWallpaper;

    private ILockScreen mLockScreen;
    private WallShuffleController mController;
    private WallShuffleObserver mObserver;

    private boolean isKeyguardShowing;
    private boolean mDozing;
    private boolean flickrEnable;
    private boolean isWallShuffleEnable;

    public MagicUnlockManager(Context context, StatusBarWindowView statusBarWindow) {
        mContext = context;
        mStatusBarWindow = statusBarWindow;
        initView();
    }

    private void initView() {
        mHostView = (FrameLayout) mStatusBarWindow.findViewById(R.id.magic_unlock);
        mask = mHostView.findViewById(R.id.magic_unlock_mask);
        mArcMenu = (ArcMenu) mStatusBarWindow.findViewById(R.id.arc_menu);
        mArcMenu.setVisibility(View.VISIBLE);

        if (mContext.getResources().getBoolean(R.bool.feature_systemui_wallshuffle_animation_on)) {
            mLockScreen = VlifeLockScreen.createLockScreenInstance(mContext);
        }
        mController = WallShuffleController.getInstance(mContext);

        if (mLockScreen != null) {
            mWallpaper = mLockScreen.loadLockView("", mController.mAnim);
            if (mWallpaper != null) {
                mWallpaper.setVisibility(View.GONE);
                mHostView.addView(mWallpaper, 0);
            }
            mLockScreen.onHide();
        }
        if (Settings.Secure.getIntForUser(mContext.getContentResolver(),
                Settings.Secure.USER_SETUP_COMPLETE, 0, UserHandle.USER_CURRENT) == 0) {
            mHostView.setVisibility(View.GONE);
        }

        mStatusBarWindow.setMagicUnlockManager(this);

        mController.bindService();
        mController.setOnChangeListener(this);

        mObserver = new WallShuffleObserver(new Handler());
    }

    public void setPhoneStatusBar(PhoneStatusBar phoneStatusBar) {
        mPhoneStatusBar = phoneStatusBar;
        mArcMenu.setActivityStarter(phoneStatusBar);
    }

    public void setKeyguardShowing(boolean show) {
        isKeyguardShowing = show;
        updateVisiblity();
    }

    public void setDozing(boolean dozing) {
        if (mDozing == dozing) return;
        mDozing = dozing;
        updateVisiblity();
    }

    public void onStartedGoingToSleep() {
        if (isWallShuffleEnable) {
            mController.prepareWallpaper();
        }
    }

    public void onTouchEvent(MotionEvent event) {
        if (flickrEnable) {
            final float y = event.getY();
            final float x = event.getX();
            int left = mContext.getResources().getDimensionPixelSize(R.dimen.magic_credit_left);
            int right = mContext.getResources().getDimensionPixelSize(R.dimen.magic_credit_right);
            int top = mContext.getResources().getDimensionPixelSize(R.dimen.magic_credit_top);
            if (x > left && x < right && y > top) {
                openFlickr();
            }
        }
    }

    public boolean isWallShuffleEnable() {
        return isWallShuffleEnable;
    }

    public void onDestroy() {
        mController.setOnChangeListener(null);
        mController.unbindService();
    }

    /**
     * Set wallpaper whether visible to user
     */
    private void updateVisiblity() {
        if (isKeyguardShowing && !mDozing && isWallShuffleEnable) {
            mHostView.setVisibility(View.VISIBLE);
            if (mLockScreen != null) {
                mLockScreen.onShow();
            }
            mController.registerSensor(true);
        } else {
            mHostView.setVisibility(View.GONE);
            if (mLockScreen != null) {
                mLockScreen.onHide();
            }
            mController.registerSensor(false);
        }
        mObserver.resetObserving(isKeyguardShowing && !mDozing);
    }

    public void onChange(String name, String author) {
        if (name == null) {
            mHostView.setVisibility(View.GONE);
            mArcMenu.setState(false);
            return;
        }
        if (mWallpaper != null) {
            mWallpaper.setVisibility(View.VISIBLE);
        }
        if (mLockScreen != null) {
            mLockScreen.setResourceInfo(WallShuffleController.path + name, mController.mAnim);
        }
        flickrEnable = author != null;
        Drawable bg = Drawable.createFromPath(WallShuffleController.path + name);
        mHostView.setBackground(bg);
        mArcMenu.setState(bg != null);
    }

    private void openFlickr() {
        try {
            Intent targetIntent;
            boolean hasFlickrApp = checkPackageExist("com.yahoo.mobile.client.android.flickr");
            if (hasFlickrApp) {
                ComponentName componet = ComponentName.unflattenFromString(
                        mContext.getResources().getString(R.string.flickr_component));
                targetIntent = new Intent();
                targetIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                targetIntent.setComponent(componet);
                mPhoneStatusBar.startActivity(targetIntent, false);
            } else {
                Uri uri = Uri.parse("https://www.flickr.com");
                targetIntent = new Intent(Intent.ACTION_VIEW, uri);
                mPhoneStatusBar.executeRunnableDismissingKeyguard(
                        () -> mPhoneStatusBar.startActivity(targetIntent, false),
                        null, false, true, true);
            }
        } catch (Exception e) {
            Log.e("Qiu", "touch pic info exception. exception info:", e);
        }
    }

    private boolean checkPackageExist(String packageName) {
        try {
            mContext.getPackageManager().getPackageInfo(packageName, PackageManager.GET_UNINSTALLED_PACKAGES);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private class WallShuffleObserver extends ContentObserver {
        public WallShuffleObserver(Handler handler) {
            super(handler);
            updateToggle();
        }

        @Override
        public void onChange(boolean selfChange) {
            updateToggle();
        }

        public void resetObserving(boolean status) {
            final ContentResolver cr = mContext.getContentResolver();
            cr.unregisterContentObserver(this);
            if (!status) {
                cr.registerContentObserver(
                    Settings.System.getUriFor(Settings.System.TCT_MAGIC_UNLOCK), false, this);
            }
        }

        private void updateToggle() {
            isWallShuffleEnable = Settings.System.getInt(mContext.getContentResolver(),
                Settings.System.TCT_MAGIC_UNLOCK, 1) == 1;
            if (!isWallShuffleEnable) {
                mArcMenu.setState(false);
            } else if (mHostView.getBackground() != null) {
                mArcMenu.setState(true);
            }
        }
    }

}
