package com.android.systemui.statusbar.phone;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.UserHandle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import com.android.systemui.R;

/**
 * Layout of Func
 * [SOLUTION]-ADD by TCTNB(Guoqiang.Qiu), 2016-8-8, Solution-2520273
 */

public class FuncLayout extends ViewGroup {
    private static final String PKG_NAME = "com.android.systemui";

    private Context mContext;
    private int itemWidth;
    private int childSize;
    private int iconSize;
    private int torchIndex = -1;
    private boolean mTorchSwitch = false; // MODIFIED by dong.liu1, 2016-11-12,BUG-3412239
    private String mFuncList;

    public FuncLayout(Context context) {
        this(context, null);
    }

    public FuncLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FuncLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mContext = context;
    }

    void updateView() {
        String choosedList = Settings.System.getStringForUser(mContext.getContentResolver(),
                "choosed_list", UserHandle.USER_CURRENT);
        if (mFuncList != null && mFuncList.equals(choosedList)) return;//choosed list not change
        mFuncList = choosedList;
        Log.d(PKG_NAME, "updateView");

        removeAllViews();

        Resources res = mContext.getResources();
        String funcList[];
        if (TextUtils.isEmpty(choosedList)) {
            choosedList = res.getString(com.android.internal.R.string.def_func_list_default);
        }
        funcList = choosedList.split(";");

        final PackageManager pm = getContext().getPackageManager();
        for (String item : funcList) {
            FuncItemView icon = new FuncItemView(mContext);
            int id = res.getIdentifier("id/" + item, null, "android");
            try {
                if (id != 0) {
                    int imgId = res.getIdentifier("drawable/" + item, null, PKG_NAME);
                    /* MODIFIED-BEGIN by dong.liu1, 2016-11-12,BUG-3412239*/
                    if(imgId == R.drawable.func_torch_on || imgId == R.drawable.func_torch){
                        icon.setImageResource(mTorchSwitch ? R.drawable.func_torch_on : R.drawable.func_torch);
                    /* MODIFIED-BEGIN by dong.liu1, 2016-11-30,BUG-3412239*/
                    }else{
                        icon.setImageResource(imgId);
                        /* MODIFIED-END by dong.liu1,BUG-3412239*/
                    }
                    /* MODIFIED-END by dong.liu1,BUG-3412239*/
                } else {
                    id = com.android.internal.R.id.func_apps;
                    // MOD-BEGIN by zhiqianghu, 11/15/2016, for 3402581
                    /*
                    Drawable drawable = pm.getApplicationIcon(item);
                    icon.setImageDrawable(drawable, iconSize);
                    */
                    String[] componentName = item.split("/");
                    if (componentName != null && componentName.length == 2) {
                        try {
                            Drawable drawable = pm.getActivityIcon(
                                    new ComponentName(componentName[0], componentName[1]));
                            icon.setImageDrawable(drawable, iconSize);
                        } catch (NameNotFoundException e) {
                            // for some icons can't attach when power on
                            mFuncList = null;
                        }
                    }
                    // MOD-END by zhiqianghu, 11/15/2016, for 3402581
                }
                icon.setId(id);
                icon.setContentDescription(item);
                icon.setScaleType(FuncItemView.ScaleType.CENTER);
                addView(icon);
            } catch (Exception e) {
                Log.d(PKG_NAME, "func", e);
            }
        }
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        childSize = getResources().getDimensionPixelSize(R.dimen.keyguard_affordance_width);
        iconSize = getResources().getDimensionPixelSize(R.dimen.keyguard_affordance_icon_width);
        updateView();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        torchIndex = -1;
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);
        final int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            final View child = getChildAt(i);
            if (child.getId() == com.android.internal.R.id.func_torch) torchIndex = i;

            child.measure(heightMeasureSpec, heightMeasureSpec);
        }
        setMeasuredDimension(widthSize, heightSize);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        final int childCount = getChildCount();

        // If there is no child, return directly
        // In this view, all the children are visible
        if (childCount < 1) {
            return;
        }

        int rest  = 0;
        if (childCount == 1) {
            itemWidth = childSize;
        } else {
            final int space = right - left - childSize * childCount;
            itemWidth = childSize + space / (childCount - 1);
            rest = space % (childCount - 1); //In some case, will rest some space
        }

        int startLeft = 0;
        for (int i = 0; i < childCount; i++) {
            final View v = getChildAt(i);

            v.layout(startLeft, 0, startLeft + childSize, childSize);
            startLeft += itemWidth;
            if (rest != 0 && i == childCount / 2) { //Add rest space to first one at bottom half
                startLeft += rest;
            }
        }
    }

    public FuncItemView getIconAtPosition(float x, float y) {
        int index = (int)(x / itemWidth);
        FuncItemView child = (FuncItemView)getChildAt(index);
        if (child == null) return null;
        if (x > child.getLeft() && x < child.getRight()
                && y > getTop() + child.getTop() && y < getBottom()) {
            return child;
        }
        return null;
    }

    public void updateTorch(boolean enabled) {
        if (torchIndex != -1) {
            mTorchSwitch = enabled; // MODIFIED by dong.liu1, 2016-11-12,BUG-3412239
            FuncItemView torch = (FuncItemView)getChildAt(torchIndex);
            torch.setImageResource(enabled ? R.drawable.func_torch_on : R.drawable.func_torch);
        }
    }
}
