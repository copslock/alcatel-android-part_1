/******************************************************************************/
/*                                                               Date:3/2016 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2016 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Reference documents : AT&T req doc                                        */
/* -------------------------------------------------------------------------- */
/*  Comments : This file is used to reset related settings menu configures    */
/*  File     : packages/apps/Settings/tct-src/com/tct/settings/settingsResetR */
/*             eceiver.java                                                   */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 3/09/2016 |    rurong.zhang      |FR1716095             |<13340Track><42>  */
/*           |                      |                      |<CDR-MRMC-090>    */
/*           |                      |                      |Result of Invoking*/
/*           |                      |                      |the Device Reset Function*/
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/

package com.tct.providers.settings;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;

import com.android.internal.telephony.RILConstants;
import com.android.internal.widget.LockPatternUtils;
import com.android.providers.settings.R;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.app.ActivityManagerNative;
import android.app.ActivityThread;
import android.app.INotificationManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.res.XmlResourceParser;
import android.content.res.Resources.NotFoundException;
import android.database.Cursor;
import android.media.AudioManager;
import android.media.AudioSystem;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.provider.MediaStore;
import android.provider.Settings;
import android.provider.Settings.Secure;
import android.service.notification.ZenModeConfig;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.ThreadedRenderer;
import android.view.IWindowManager;
import android.view.textservice.TextServicesManager;
import android.os.UserHandle;
import android.app.AppOpsManager;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.IPackageManager;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.app.ActivityThread;
import android.app.admin.DeviceAdminInfo;
import android.app.admin.DeviceAdminReceiver;
import android.app.admin.DevicePolicyManager;
import android.Manifest;
import android.hardware.usb.UsbManager;
import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import android.app.admin.DeviceAdminInfo;

public class ProviderSettingsResetReceiver extends BroadcastReceiver {
    private boolean isParserd = false;
    private HashMap<String, setData> data = null;
    private setData curSetData = null;
    private cVariant curVar = null;
    private String curSetName = null;
    private ContentValues mValues;
    private boolean DEBUG = false;

    private final String TAG = "settingsProviderConfig";
    private final String RES_PACKAGE_NAME = "com.android.providers.settings";
    static final Uri DATABASE_SYSTEM = Settings.System.CONTENT_URI;
    static final Uri DATABASE_SECURE = Settings.Secure.CONTENT_URI;
    static final Uri DATABASE_GLOBAL = Settings.Global.CONTENT_URI;

    /* All base data tag type */
    static final int TYPE_UNBASE = 0;
    static final int TYPE_INT     = 1;
    static final int TYPE_STRING  = 2;
    static final int TYPE_BOOL    = 3;
    static final int TYPE_DEFAULT = 4;
    static final int TYPE_DELETE  = 5;
    static final int TYPE_CUSTOM  = 6;

    static final int TYPE_SP = 1;
    static final int TYPE_DATABASE = 2;

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();

        int count = this.getResultCode() + 1;
        this.setResultCode(count);
        Log.i(TAG, "Receive " + action);

        XmlResourceParser parser = context.getResources().getXml(
                R.xml.reset_configures);

        try {
            doParse(parser, context);
        } catch (Exception e) {
            Log.i(TAG, "Parse xml exception " + e.toString());
        }

        if (isParserd && (data != null)) {
            Set<String> sets = data.keySet();
            Iterator<String> iterator = sets.iterator();
            while (iterator.hasNext()) {
                String set = iterator.next();
                if (set.equalsIgnoreCase("system")) {
                    doConfig(context, data.get(set), DATABASE_SYSTEM);
                } else if (set.equalsIgnoreCase("secure")) {
                    doConfig(context, data.get(set), DATABASE_SECURE);
                } else if (set.equalsIgnoreCase("global")) {
                    doConfig(context, data.get(set), DATABASE_GLOBAL);
                }
            }
        }
    }

    /**
     * get the uri of the default ringtone/notification/alarm alert.
     */
    private String getDefaultRingUri(Context context, String ringtype) {
        String defaultRingtoneName = SystemProperties.get("ro.config."
                + ringtype);
        String typeFilter = "";
        if (ringtype.equalsIgnoreCase(Settings.System.RINGTONE)) {
            typeFilter = MediaStore.Audio.Media.IS_RINGTONE;
        } else if (ringtype
                .equalsIgnoreCase(Settings.System.NOTIFICATION_SOUND)) {
            typeFilter = MediaStore.Audio.Media.IS_NOTIFICATION;
        } else if (ringtype.equalsIgnoreCase(Settings.System.ALARM_ALERT)) {
            typeFilter = MediaStore.Audio.Media.IS_ALARM;
        }

        if (typeFilter.equals("")) {
            return null;
        }
        Cursor c = context.getContentResolver().query(
                MediaStore.Audio.Media.INTERNAL_CONTENT_URI,
                new String[] { MediaStore.Audio.Media._ID },
                MediaStore.Audio.Media.DISPLAY_NAME + "=? AND " + typeFilter
                        + "= 1", new String[] { defaultRingtoneName }, null);
        if (c != null) {
            try {
                c.moveToFirst();
                if (c.getCount() > 0) {
                    String id = c.getString(c
                            .getColumnIndex(MediaStore.Audio.Media._ID));
                    // c.close();
                    return (MediaStore.Audio.Media.INTERNAL_CONTENT_URI)
                            .toString() + "/" + id;
                }
            } finally {
                c.close();
            }
        }

        return null;
    }

    /**
     * reset configure.
     */
    private void doConfig(Context context, setData curData, Uri uri) {
        ContentResolver resolver = context.getContentResolver();
        Iterator<Entry<String, cVariant>> iterator = curData.getAll();
        mValues = new ContentValues();
        Log.i(TAG, "start doConfig()...");
        while (iterator.hasNext()) {
            Map.Entry<String, cVariant> entry = iterator.next();
            String key = entry.getKey();
            cVariant var = entry.getValue();
            Object obj = var.getObj();
            int type = var.getType();
            Log.i(TAG,
                    "type = " + type + ", key = " + key + ", value = "
                            + obj.toString());
            if ((type == TYPE_DEFAULT) || (type == TYPE_INT)
                    || (type == TYPE_BOOL) || (type == TYPE_STRING)) {
                if ((obj != null) && (!key.equals(""))) {
                    mValues.put(Settings.NameValueTable.NAME, key);
                    mValues.put(Settings.NameValueTable.VALUE, obj.toString());
                    resolver.insert(uri, mValues);
                    mValues.clear();
                }
            } else if (type == TYPE_DELETE) {
                if ((obj == null) || (obj.toString().equals(""))) {
                    resolver.delete(uri, Settings.NameValueTable.NAME + " = ?",
                            new String[] { key });
                } else {
                    resolver.delete(uri, Settings.NameValueTable.NAME
                            + " like ?", new String[] { obj.toString() });
                }
            } else if (type == TYPE_CUSTOM) {
                if (key.equalsIgnoreCase("data_roaming")) {
                    /*TelephonyManager telephonyManager = context.getSystemService(TelephonyManager.class);
                    telephonyManager.setNetworkRoaming(false);*/
                    SubscriptionManager subscriptionManager = SubscriptionManager.from(context);
                    int subId = subscriptionManager.getDefaultSubscriptionId();
                    if(subscriptionManager.isNetworkRoaming(subId)){
                        subscriptionManager.setDataRoaming(SubscriptionManager.DATA_ROAMING_DISABLE, subId);
                    }
                //add by haifeng.zhai for defect 1815966 begin
                }else if(key.equalsIgnoreCase("system_volume")){
                    AudioManager audioManager = context
                            .getSystemService(AudioManager.class);
                    audioManager
                            .setStreamVolume(
                                    AudioManager.STREAM_RING,
                                    AudioSystem.DEFAULT_STREAM_VOLUME[AudioManager.STREAM_RING],
                                    AudioManager.FLAG_SHOW_UI_WARNINGS);
                    audioManager
                            .setStreamVolume(
                                    AudioManager.STREAM_ALARM,
                                    AudioSystem.DEFAULT_STREAM_VOLUME[AudioManager.STREAM_ALARM],
                                    AudioManager.FLAG_SHOW_UI_WARNINGS);
                    audioManager
                            .setStreamVolume(
                                    AudioManager.STREAM_MUSIC,
                                    AudioSystem.DEFAULT_STREAM_VOLUME[AudioManager.STREAM_MUSIC],
                                    AudioManager.FLAG_SHOW_UI_WARNINGS);
                    //add by kun.yao for defect  2094738  begin
                    audioManager
                    		.setStreamVolume(
                            AudioManager.STREAM_NOTIFICATION,
                            AudioSystem.DEFAULT_STREAM_VOLUME[AudioManager.STREAM_NOTIFICATION],
                            AudioManager.FLAG_SHOW_UI_WARNINGS);
                    //add by kun.yao for defect  2094738 end
                  //add by haifeng.zhai for defect 1815966 end
                  //add by haifeng.zhai for defect 1963923 begin
                }else if(key.equalsIgnoreCase("def_usb_config")){
                    UsbManager usbManager = context.getSystemService(UsbManager.class);
                    String defautfunction = SystemProperties.get("persist_sys_usb_config",
                    UsbManager.USB_FUNCTION_MTP);
                    usbManager.setCurrentFunction(defautfunction);
                  //add by haifeng.zhai for defect 1963923 end
                } else if (key.equalsIgnoreCase("mobile_data")) {
                    TelephonyManager telephonyManager = context.getSystemService(TelephonyManager.class);
                    telephonyManager.setDataEnabled(true);
                } else if ((obj != null)
                        && (obj.toString().equalsIgnoreCase("sound"))) {
                    String ringUri = getDefaultRingUri(context, key);
                    if (ringUri != null) {
                        Settings.System.putString(resolver, key, ringUri);
                    }
                } else if (key.equalsIgnoreCase("bluetooth")) {
                    Bundle bundle = new Bundle();
                    boolean value = false;
                    if ((obj != null) && !(obj.toString().equals(""))) {
                        try {
                            int id = context.getResources().getIdentifier(
                                    obj.toString(), "bool", RES_PACKAGE_NAME);
                            value = context.getResources().getBoolean(id);
                        } catch (NotFoundException e) {
                            Log.i(TAG, e.toString());
                        }
                        bundle.putBoolean(obj.toString(), value);
                    }
                    this.setResultExtras(bundle);
                } else if (key.equalsIgnoreCase("netpolicy")) {
                    File systemDir = new File(Environment.getDataDirectory(),
                            "system");
                    File fl = new File(systemDir, "netpolicy.xml");
                    if (fl.exists()) {
                        fl.delete();
                    }

                    File syncDir = new File(systemDir, "sync");
                    File file = new File(syncDir, "accounts.xml");
                    if (file.exists()) {
                        file.delete();
                    }
                //TCT-Add-Start by charlesyang,2016.04.20,Defect1953663
                } else if (key.equalsIgnoreCase("wifi_hotspot_setup")) {
                    // restore Mobile Hotspot turn off timer
                    Settings.System.putInt(resolver, Settings.System.WIFI_HOTSPOT_AUTO_DISABLE,Settings.System.WIFI_HOTSPOT_AUTO_DISABLE_FOR_TEN_MINS);
                    String AP_CONFIG_FILE = Environment.getDataDirectory() +
                            "/misc/wifi/softap.conf";
                    DataOutputStream out = null;
                    try {
                        out = new DataOutputStream(new BufferedOutputStream(
                                    new FileOutputStream(AP_CONFIG_FILE)));

                        out.writeInt(2);
                    } catch (IOException e) {
                        Log.e(TAG, "Error writing hotspot configuration" + e);
                    } finally {
                        if (out != null) {
                            try {
                                out.close();
                            } catch (IOException e) {}
                        }
                    }
                    //TCT-Add-End by charlesyang,2016.04.20,Defect1953663
                   //TCT-Add-Start by charlesyang,2016.04.22,Defect1966500
                  } else if (key.equalsIgnoreCase("wifi_on")) {
                        if (context.getResources().getBoolean(R.bool.def_tctfw_settings_wifiActivate_enable)) {
                            Log.i(TAG, "def_tctfw_settings_wifiActivate_enable is true, set wifi_on to 1");
                            Settings.Global.putInt(resolver, Settings.Global.WIFI_ON, 1);
                        } else {
                            Log.i(TAG, "def_tctfw_settings_wifiActivate_enable is false, set wifi_on to 0");
                            Settings.Global.putInt(resolver, Settings.Global.WIFI_ON, 0);
                        }
                //TCT-Add-End by charlesyang,2016.04.22,Defect1966500
                //TCT-Add-Start by charlesyang,2016.04.22,Defect1952012
                // device data reset for Network notification
                //TCT-Add-Start by charlesyang,2016.04.22,Defect1990133
                boolean bNetNotifiValue = context.getResources().getBoolean(R.bool.def_settingsprovider_wifi_networks_available_notification_on);
                Settings.Global.putInt(resolver, Settings.Global.WIFI_NETWORKS_AVAILABLE_NOTIFICATION_ON, bNetNotifiValue ? 1 : 0);
                //TCT-Add-End by charlesyang,2016.04.22,Defect1990133
                // device data reset for Wi-Fi AutoJoin
                boolean bWifiAutoJoin = context.getResources().getBoolean(R.bool.wifi_autojoin_is_enable_default);
                Settings.Global.putInt(resolver, Settings.Global.KEY_WIFI_AUTOJOIN_AVAILABLE, bWifiAutoJoin ? 1 : 0);
                // device data reset for Wi-Fi frequency band
                Settings.Global.putInt(resolver, Settings.Global.WIFI_FREQUENCY_BAND, 0);
                //TCT-Add-End by charlesyang,2016.04.22,Defect1952012
                } else if (key.equalsIgnoreCase("mock_location")) {
                    String value = "1".equals(SystemProperties.get(
                            "ro.allow.mock.location", "0")) ? "1" : "0";
                    Settings.Secure.putString(resolver, key, value);
                } else if (key.equalsIgnoreCase("process_limit")) {
                    try {
                        ActivityManagerNative.getDefault().setProcessLimit(-1);
                    } catch (RemoteException e) {
                        Log.i(TAG, "exception " + e.toString());
                    }
                } else if (key.equalsIgnoreCase("debug_app")) {
                    try {
                        ActivityManagerNative.getDefault().setDebugApp(null,
                                false, true);
                    } catch (RemoteException ex) {
                    }
                } else if (key.equalsIgnoreCase("force_GPU_render")) {
                    SystemProperties.set("persist.sys.ui.hw", "false");
                    SystemProperties.set("persist.sys.hdcp_checking", "");
                } else if (key.equalsIgnoreCase("show_hw_screen_udpates")) {
                    SystemProperties.set(
                            ThreadedRenderer.DEBUG_DIRTY_REGIONS_PROPERTY,
                            "false");
                } else if (key.equalsIgnoreCase("auto_time")) {
                    Settings.Global.putInt(resolver, Settings.Global.AUTO_TIME,
                            1);
                } else if ("headset_mode".equalsIgnoreCase(key)) {
                    SystemProperties.set("persist.sys.headset_on", "true");
                } else if (key.equalsIgnoreCase("strict_mode")) {
                    try {
                        IWindowManager mWindowManager = IWindowManager.Stub
                                .asInterface(ServiceManager
                                        .getService("window"));
                        mWindowManager
                                .setStrictModeVisualIndicatorPreference("");
                        mWindowManager.setAnimationScale(0, 1);
                        mWindowManager.setAnimationScale(1, 1);
                        mWindowManager.setAnimationScale(2, 1);
                    } catch (RemoteException e) {
                    }
                } else if (key.equalsIgnoreCase("wifi_scan_always_enabled")) {
                    Settings.Global.putInt(resolver,
                            Settings.Global.WIFI_SCAN_ALWAYS_AVAILABLE, 1);
                } else if (key.equalsIgnoreCase("zen_mode")) {
                    if (obj != null && !obj.toString().equals("")) {
                        if (obj.toString().equals("ZEN_MODE_OFF")) {
                            Settings.Global.putInt(resolver,
                                    Settings.Global.ZEN_MODE,
                                    Settings.Global.ZEN_MODE_OFF);
                        } else if (obj.toString().equals(
                                "ZEN_MODE_IMPORTANT_INTERRUPTIONS")) {
                            Settings.Global
                                    .putInt(resolver,
                                            Settings.Global.ZEN_MODE,
                                            Settings.Global.ZEN_MODE_IMPORTANT_INTERRUPTIONS);
                        } else if (obj.toString().equals(
                                "ZEN_MODE_NO_INTERRUPTIONS")) {
                            Settings.Global.putInt(resolver,
                                    Settings.Global.ZEN_MODE,
                                    Settings.Global.ZEN_MODE_NO_INTERRUPTIONS);
                        }
                    }
                } else if (key.equalsIgnoreCase("usageaccess")) {
                    AppOpsManager mAppOpsManager;
                    PackageManager mPackageManager = context
                            .getPackageManager();
                    IPackageManager mIPackageManager = ActivityThread
                            .getPackageManager();

                    mAppOpsManager = (AppOpsManager) context
                            .getSystemService(Context.APP_OPS_SERVICE);
                    String[] packages = null;
                    try {
                        packages = mIPackageManager
                                .getAppOpPermissionPackages(Manifest.permission.PACKAGE_USAGE_STATS);
                    } catch (RemoteException e) {
                        Log.w(TAG,
                                "PackageManager is dead. Can't get list of packages requesting "
                                        + Manifest.permission.PACKAGE_USAGE_STATS);
                    }

                    if (packages.length > 0) {
                        for (final String packageName : packages) {
                            if (!(packageName.equals("android") || packageName
                                    .equals("com.android.settings"))) {
                                try {
                                    mAppOpsManager
                                            .setMode(
                                                    AppOpsManager.OP_GET_USAGE_STATS,
                                                    mPackageManager
                                                            .getPackageInfo(
                                                                    packageName,
                                                                    0).applicationInfo.uid,
                                                    packageName,
                                                    AppOpsManager.MODE_ALLOWED);
                                } catch (Exception e) {
                                    Log.i(TAG,
                                            "can not set the usageaccess app");
                                }
                            }
                        }
                    }
                } else if (key.equalsIgnoreCase("headset_mode")) {
                    SystemProperties.set("persist.sys.headset_on", "true");
                } else if (key.equalsIgnoreCase("is_func_on")) {
                    if (obj != null && obj.toString().equals("true")) {
                        Settings.System.putInt(context.getContentResolver(),
                                "is_func_on", 1);
                    } else {
                        Settings.System.putInt(context.getContentResolver(),
                                "is_func_on", 0);
                    }
                } else if (key
                        .equalsIgnoreCase("screensaver_activate_on_sleep")) {
                    boolean bValue = context
                            .getResources()
                            .getBoolean(
                                    com.android.internal.R.bool.config_dreamsActivatedOnSleepByDefault);
                    Settings.Secure.putInt(resolver,
                            Settings.Secure.SCREENSAVER_ACTIVATE_ON_SLEEP,
                            bValue ? 1 : 0);
                } else if (key.equalsIgnoreCase("screensaver_activate_on_dock")) {
                    boolean bValue = context
                            .getResources()
                            .getBoolean(
                                    com.android.internal.R.bool.config_dreamsActivatedOnDockByDefault);
                    Settings.Secure.putInt(resolver,
                            Settings.Secure.SCREENSAVER_ACTIVATE_ON_DOCK,
                            bValue ? 1 : 0);
                } else if (key
                        .equalsIgnoreCase("roaming_reminder_mode_setting")) {
                    int phoneCount = TelephonyManager.getDefault()
                            .getPhoneCount();
                    //ADD For Task: 1696767 By kaiyuan.ma begin
                    int defaultReminderMode = context.getResources().getInteger(R.integer.def_settingsprovider_roamingReminderMode);
                    if (context
                            .getResources()
                            .getBoolean(
                                    com.android.internal.R.bool.def_tctfw_settings_RoamingReminder_EE_enable)) {
                        for (int phoneId = 0; phoneId < phoneCount; phoneId++) {
                            Settings.System
                                    .putInt(resolver,
                                            Settings.System.ROAMING_REMINDER_MODE_SETTING
                                                    + phoneId, 0);
                            if(context.getResources().getBoolean(R.bool.def_tctfw_settings_RoamingReminder_enable_for_cricket))
                            {
	Settings.System.putInt(resolver,Settings.System.ROAMING_REMINDER_MODE_SETTING + phoneId, defaultReminderMode);
                            }
                            //ADD For Task: 1696767 By kaiyuan.ma end
                            Log.i(TAG,
                                    "Settings.System.ROAMING_REMINDER_MODE_SETTING mode is:"
                                            + String.valueOf(Settings.System
                                                    .getInt(resolver,
                                                            Settings.System.ROAMING_REMINDER_MODE_SETTING
                                                                    + phoneId,
                                                            0)));
                        }
                    }
                } else if (key.equalsIgnoreCase("QUICK_TRANSLATE_ENABLE")) {
                    Settings.Global.putInt(context.getContentResolver(),
                            "QUICK_TRANSLATE_ENABLE", 0);
                    Intent intent = new Intent("QUICK_TRANSLATE_ENABLE");
                    context.sendBroadcast(intent);
                } else if (key.equalsIgnoreCase("screen_brightness_mode")) {
                    Settings.System
                            .putInt(context.getContentResolver(),
                                    "screen_brightness_mode",
                                    context.getResources()
                                            .getBoolean(
                                                    R.bool.def_settingsprovider_automaticMode_enable) ? 1
                                            : 0);
                } else if (key.equalsIgnoreCase("system_vibrate_when_ringing")) {
                    boolean vibValue = context.getResources().getBoolean(
                            R.bool.def_vibrate_when_ringing_enabled);
                    Settings.System.putInt(context.getContentResolver(),
                            Settings.System.VIBRATE_WHEN_RINGING, vibValue ? 1
                                    : 0);
                } else if (key.equalsIgnoreCase("language_spell_checker")) {
                    TextServicesManager mTsm = (TextServicesManager) context
                            .getSystemService(Context.TEXT_SERVICES_MANAGER_SERVICE);
                    mTsm.setSpellCheckerSubtype(null);
                } else if (key.equalsIgnoreCase("sms_outgoing_check_max_count")) {
                    Settings.Secure.putInt(context.getContentResolver(),
                            Settings.Global.SMS_OUTGOING_CHECK_MAX_COUNT, 30);
                } else if (key.equalsIgnoreCase("wifi_p2p_device_name")) {
                    String wifiDirectName = context.getResources().getString(
                            R.string.def_tctfw_settings_wifiP2pName_on);
                    Settings.Global.putString(context.getContentResolver(),
                            Settings.Global.WIFI_P2P_DEVICE_NAME,
                            wifiDirectName);
                /* MODIFIED-BEGIN by lin.lv2, 2016-04-20,BUG-1953219*/
                }else if(key.equalsIgnoreCase("camera_double_tap_power_gesture_disabled")){
                     Settings.Secure.putInt(context.getContentResolver(), Settings.Secure.CAMERA_DOUBLE_TAP_POWER_GESTURE_DISABLED,
                        context.getResources().getInteger(com.android.internal.R.integer.feature_camera_double_tap_power_gesture_disabled));
                        /* MODIFIED-END by lin.lv2,BUG-1953219*/
                }else if(key.equalsIgnoreCase("tct_colorservice_screen_mode")){
                      Settings.System.putInt(context.getContentResolver(), Settings.System.TCT_COLORSERVICE_SCREEN_MODE,
                             context.getResources().getInteger(com.android.internal.R.integer.def_tctfw_screenmode_id));
                }else if(key.equalsIgnoreCase("lockscreen")){
                     LockPatternUtils mLockPatternUtils;
                     mLockPatternUtils = new LockPatternUtils(context);
                     mLockPatternUtils.setVisiblePatternEnabled(true,  UserHandle.myUserId());
                     mLockPatternUtils.setPowerButtonInstantlyLocks(true, UserHandle.myUserId());
                     mLockPatternUtils.setOwnerInfo("",  UserHandle.myUserId());
                /* MODIFIED-BEGIN by feng.tang, 2016-11-10,BUG-3411928*/
                } else if(key.equalsIgnoreCase("lockscreen_sounds_enabled")){
                     Settings.System.putInt(context.getContentResolver(), Settings.System.LOCKSCREEN_SOUNDS_ENABLED,
                            context.getResources().getInteger(R.integer.def_settingsprovider_lockscreen_sounds_enabled));
                } else if(key.equalsIgnoreCase("charging_sounds_enabled")){
                    Settings.Global.putInt(context.getContentResolver(), Settings.Global.CHARGING_SOUNDS_ENABLED,
                            context.getResources().getInteger(R.integer.def_settingsprovider_charging_sounds_enabled));
                            /* MODIFIED-END by feng.tang,BUG-3411928*/
                } else if(key.equalsIgnoreCase("arkamys_audio_effect_enable")){
                    Settings.System.putInt(context.getContentResolver(),"arkamys_audio_effect_enable",
                            context.getResources().getInteger(R.integer.def_settingsprovider_arkamys_audio_effect_enabled));
                /* MODIFIED-BEGIN by feng.tang, 2016-11-14,BUG-3430322*/
                } else if(key.equals("notification_policy")){
                    final File file = new File(Environment.getDataDirectory(), "system/notification_policy.xml");
                    file.delete();
                    /* MODIFIED-END by feng.tang,BUG-3430322*/
                }
                /* MODIFIED-BEGIN by yongliang.zhu, 2016-12-01,BUG-3534531*/
                else if(key.equalsIgnoreCase("def_twilight_mode")){
                	Settings.Secure.putInt(context.getContentResolver(), Settings.Secure.TWILIGHT_MODE,Settings.Secure.TWILIGHT_MODE_LOCKED_OFF);
                }
                /* MODIFIED-END by yongliang.zhu,BUG-3534531*/
            } else {
                Log.i(TAG, "The type of " + key + " is not supported.");
            }
        }
    }

    /**
     * parse the hole xml file.
     */
    private synchronized void doParse(XmlPullParser parser, Context context) throws XmlPullParserException, IOException {
        if (!isParserd) {
            data = new HashMap<String, setData>();
            int eventType = parser.getEventType();
            while(true){
                switch(eventType){
                case XmlPullParser.END_DOCUMENT:
                    isParserd = true;
                    return;
                case XmlPullParser.START_DOCUMENT:
                    break;
                case XmlPullParser.START_TAG:
                    processStartTag(parser, context);
                    break;
                case XmlPullParser.TEXT:
                    break;
                case XmlPullParser.END_TAG:
                    String name = parser.getName();
                    if(name.equalsIgnoreCase("set")){
                        curSetData = null;
                        curSetName = null;
                    } else if (getBaseType(name) != TYPE_UNBASE) {
                        curVar = null;
                    }
                    break;
                default:
                    break;
                }
                eventType = parser.next();
            }
        }
    }

    /**
     * process the start tag.
     */
    private void processStartTag(XmlPullParser parser, Context context)
            throws XmlPullParserException, IOException {
        String name = parser.getName();
        int type = getBaseType(name);

        if (name.equalsIgnoreCase("set")) {
            curSetData = new setData();
            curSetName = parser.getAttributeValue(0).trim();
            if (!curSetName.equals("")) {
                data.put(curSetName, curSetData);
            }
        } else if (type != TYPE_UNBASE) {
            String curBaseName = parser.getAttributeValue(0).trim();
            String tmpText = parser.nextText().trim();
            if ((type == TYPE_DEFAULT) || (type == TYPE_DELETE)
                    || (type == TYPE_CUSTOM)) {
                curVar = new cVariant(tmpText, type);
            } else if (!tmpText.equals("")) {
                int id = 0;
                tmpText = tmpText.trim();
                try {
                    if (type == TYPE_INT) {
                        // Not use reflect as will cause NoSuchFieldException
                        // with user sw version
                        // fld = R.integer.class.getDeclaredField(tmpText);
                        id = context.getResources().getIdentifier(tmpText,
                                "integer", RES_PACKAGE_NAME);

                        int value = context.getResources().getInteger(id);
                        Log.i(TAG, "value = " + value);
                        curVar = new cVariant(value, type);
                    } else if (type == TYPE_BOOL) {
                        id = context.getResources().getIdentifier(tmpText,
                                "bool", RES_PACKAGE_NAME);
                        boolean value = context.getResources().getBoolean(id);
                        Log.i(TAG, "value = " + value);
                        curVar = new cVariant(value ? 1 : 0, type);
                    } else if (type == TYPE_STRING) {
                        id = context.getResources().getIdentifier(tmpText,
                                "string", RES_PACKAGE_NAME);
                        String value = context.getResources().getString(id);
                        Log.i(TAG, "value = " + value);
                        curVar = new cVariant(value, type);
                    }
                    Log.i(TAG,
                            "type = " + name + ", id = "
                                    + Integer.toHexString(id));
                } catch (IllegalArgumentException e) {
                    Log.i(TAG, e.toString());
                } catch (NotFoundException e) {
                    Log.i(TAG, e.toString());
                }
            }
            if ((!curBaseName.equals("")) && (curVar != null)) {
                curSetData.put(curBaseName, curVar);
                curVar = null;
            }
        }
    }

    /**
     * find out the current tag is base tag. only support int, bool, string,
     * default, delete, custom.
     */
    private int getBaseType(String name) {
        int ret = TYPE_UNBASE;
        if ((name.equalsIgnoreCase("int"))
                || (name.equalsIgnoreCase("integer"))) {
            ret = TYPE_INT;
        } else if (name.equalsIgnoreCase("bool")) {
            ret = TYPE_BOOL;
        } else if (name.equalsIgnoreCase("string")) {
            ret = TYPE_STRING;
        } else if (name.equalsIgnoreCase("default")) {
            ret = TYPE_DEFAULT;
        } else if (name.equalsIgnoreCase("delete")) {
            ret = TYPE_DELETE;
        } else if (name.equalsIgnoreCase("custom")) {
            ret = TYPE_CUSTOM;
        }
        return ret;
    }

    /**
     * Module data, it restore the all data parsed from xml file.
     */
    private class setData {
        private HashMap<String, cVariant> map = new HashMap<String, cVariant>();

        public cVariant get(String key) {
            cVariant v = null;
            if (map.containsKey(key)) {
                v = map.get(key);
            }
            return v;
        }

        void put(String key, cVariant o) {
            if (key != null && key.length() != 0) {
                map.put(key, o);
            }
        }

        public boolean containsKey(String key) {
            return map.containsKey(key);
        }

        public Iterator<Entry<String, cVariant>> getAll() {
            return map.entrySet().iterator();
        }
    }

    /**
     * all data parsed from xml file will packaged to Variant.
     */
    private class cVariant {
        Object obj = null;
        int type = TYPE_UNBASE;

        public cVariant(Object value, int type) {
            if (value != null) {
                this.obj = value;
                this.type = type;
            }
        }

        public int getType() {
            return type;
        }

        public Object getObj() {
            return obj;
        }
    }
}
