/******************************************************************************/
/*                                                               Date:09/2016 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2016 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  caixia.chen                                                     */
/*  Email  :                                                                  */
/*  Role   :                                                                  */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments :                                                                */
/*  File     :                                                                */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 09/07/2016|     caixia.chen      |     task 2854067     |Private mode      */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/

package tct.util.privacymode;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.RemoteException;

import tct.util.privacymode.ITctPrivacyModeResult;
import tct.util.privacymode.TctPrivacyModeHelper.OnResultListener;

/**
 * 
 * @author chencaixia
 * @hide
 */
public class TctPrivacyModeHelperImpl {

    private Context mContext;
    private ITctPrivacyModeService mService;
    private OnResultListener mResultListener = null;
    private static Handler mHandler = null;
    private static HandlerThread mHandlerThread = null;
    boolean mIsPrivacyMode;

    private ITctPrivacyModeResult.Stub mResultListenerStub = new ITctPrivacyModeResult.Stub() {
        @Override
        public void onResult(int result) throws RemoteException {
            try {
                if (mResultListener != null) {
                    mResultListener.onResult(result);
                }
            } finally {
                getHandler().post(new Runnable(){
                    @Override
                    public void run() {
                        unbindService();
                    }
                });
            }
        }
    };

    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mService = ITctPrivacyModeService.Stub.asInterface(service);

            try {
                mService.enterPrivacyMode(mIsPrivacyMode, mResultListenerStub);
            } catch (RemoteException e) {
                if (mResultListener != null) {
                    mResultListener.onResult(1);
                    android.util.Log.i("==Test==", "onResult failed");
                }
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
        }};

    public TctPrivacyModeHelperImpl(Context context) {
        mContext = context;
    }

    public void enterPrivacyMode(boolean isPrivacy, OnResultListener resultListener) {
        mIsPrivacyMode = isPrivacy;
        mResultListener = resultListener;

        bindService();
    }

    private void bindService () {
        //Intent intent = new Intent("com.tct.privacymode.action.PrivacyModeService");
        Intent intent = new Intent();
        intent.setClassName("com.tct.privacymode", "com.tct.privacymode.PrivacyModeService");
        mContext.bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    private void unbindService() {
        try {
            mContext.unbindService(mConnection);
        } catch (Exception e) {
            // unbind failed. But it's no mater, let it go.
        }
    }

    private static Handler getHandler() {
        if (mHandler == null) {
            mHandlerThread = new HandlerThread("Unbind Thread");
            mHandlerThread.start();
            mHandler = new Handler(mHandlerThread.getLooper());
        }

        return mHandler;
    }
}
