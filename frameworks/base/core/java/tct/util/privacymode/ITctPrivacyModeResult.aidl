
package tct.util.privacymode;


/**
 * result that add account to Email application
 *
 * {@hide}
 */
interface ITctPrivacyModeResult
{
    void onResult(int result);
}

