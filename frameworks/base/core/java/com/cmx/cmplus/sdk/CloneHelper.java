/* Copyright (C) 2016 Tcl Corporation Limited */
package com.cmx.cmplus.sdk;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.Log;

import com.cmx.cmplus.SmartContainerConfig;

import java.lang.reflect.Constructor;
import java.util.List;
import java.util.Set;

/**
 * Helper class for clone package.
 * @hide
 */
public abstract class CloneHelper {

    protected final static String TAG = "CloneHelper";

    private static CloneHelper sInstance = null;

    /**
     * Represent of all instances
     */
    public final static int ALL_INSTANCE = -1;
    /**
     * Represent of master instance
     */
    public final static int MASTER_INSTANCE = 0;
    /**
     * Represent of clone instance
     */
    public final static int CLONE_INSTANCE = 1;

    //All the public clone related intents begin

    /**
     * Sent when a container was created
     */
    public static final String ACTION_CONTAINER_CREATED = "com.cmx.cmplus.ACTION_CONTAINER_CREATED";

    /**
     * Sent when clone disabled/enabled.
     * Applications should call SmartContainerConfig.init() if receive this intent. Then the SmartContainerConfig
     * will be changed accordingly.
     */
    public static final String ACTION_CONTAINER_FEATURE_CHANGED = "com.cmx.cmplus.ACTION_CONTAINER_FEATURE_CHANGED";

    /**
     * Sent when a package was cloned. The intent contains EXTRA_PACKAGE_NAME and EXTRA_APPLICATION_INFO
     */
    public static final String ACTION_CLONE_ADDED = "com.cmx.cmplus.ACTION_CLONE_ADDED";

    /**
     * Sent when the clone of the package was removed
     */
    public static final String ACTION_CLONE_REMOVED = "com.cmx.cmplus.ACTION_CLONE_REMOVED";

    /**
     * Extra container info
     */
    public static final String EXTRA_CONTAINER_INFO = "com.cmx.cmplus.EXTRA_CONTAINER_INFO";

    /**
     * Extra package name. Use Intent.getStringExtra(EXTRA_PACKAGE_NAME) to get
     */
    public static final String EXTRA_PACKAGE_NAME = "com.cmx.cmplus.EXTRA_PACKAGE_NAME";

    /**
     * Extra application info. Use Intent.getParcelableExtra(EXTRA_APPLICATION_INFO) to get
     */
    public static final String EXTRA_APPLICATION_INFO = "com.cmx.cmplus.EXTRA_APPLICATION_INFO";

    //All the public clone related intents end

    /**
     * Method to get CloneHelper
     * @param context the context of current application
     * @return
     */
    public static CloneHelper get(Context context) {
        if (sInstance == null) {
            try {
                Class<?> cloneHelperImplClass = Class.forName("com.cmx.cmplus.sdk.CloneHelperImpl");
                Constructor ctor = cloneHelperImplClass.getConstructor(Context.class);
                ctor.setAccessible(true);
                sInstance = (CloneHelper) ctor.newInstance(context);
            } catch (Exception e){
                e.printStackTrace();
                Log.d(TAG, "Failed to find CloneHelper SDK. Turn off feature! ");
                sInstance = null;
                SmartContainerConfig.turnOff();
            }
        }
        return  sInstance;
    }

    /**
     * Create clone for the specified package
     * The application calling this API need to share system UID.
     * @param pkg the name of the package
     * @return true if clone successfully; false if failed
     */
    public boolean createCloneForPackage(String pkg) {
        return false;
    }

    /**
     * Delete clone for the specified package.
     * The application calling this API need to share system UID.
     * @param pkg the name of the package
     * @return true if the clone was deleted;
     */
    public boolean deleteCloneForPackage(String pkg){
        return false;
    }

    /**
     * Check whether the package can be cloned. The list is controlled by
     * "inbox-packge" of /system/etc/container/extra_clone_box_policy.xml
     * @param pkg the name of the package
     * @return true if it can be cloned; false if it cannot
     */
    public boolean canPackageBeCloned(String pkg){
        return false;
    }

    /**
     * Check whether the package is already cloned
     * @param pkg the name of the package
     * @return true if it's cloned; false if it's not.
     */
    public boolean isPackageCloned(String pkg) {
        return false;
    }

    /**
     * Get the badge icon for cloned apps
     * @return the badge icon
     */
    public Bitmap getCloneBadge(){
        return null;
    }

    /**
     * Set the badge icon for cloned apps
     * @param badge
     */
    public void setCloneBadge(Bitmap badge){
        return;
    }

    /**
     * Check whether the specified instance is a clone instance
     * @param uid the uid of the instance
     * @return true if the instance is a clone instance
     */
    public boolean isCloneInstance(int uid){
        return false;
    }

    /**
     * Check whether the specified instance is the master instance. Master intance will always exist
     * unless user uninstall the package
     * @param uid the uid of the instance
     * @return true if the instance is the master instance
     */
    public boolean isMasterInstance(int uid){
        return true;
    }

    /**
     * Start activity in cloned application
     * @param context context of the application
     * @param intent intent to start the activity
     */
    public void startActivityInClone(Context context, Intent intent){
        return;
    }

    /**
     * Get the icon with clone badge
     * @param icon
     * @return
     */
    public Drawable getCloneBadgedIcon(Drawable icon){
        return  null;
    }

    /**
     * Get the ApplicationInfo of the clone instance. PackageManager.getApplicationLabel and
     * getApplicationIcon will return the corresponding label and icon.
     * the uid in the application info will be different with the master instance
     * @param name the name of the package
     * @param flags the flags. See PackageManager.getApplicationInfo
     * @return null if the package not cloned;
     */
    public ApplicationInfo getCloneApplicationInfo(String name, int flags) { return  null; }

    /**
     * Disable clone feature dynamically
     * The application calling this API need to share system UID.
     */
    public void disableCloneFeature() {
        return;
    }

    /**
     * Enable clone feature dynamically
     * The application calling this API need to share system UID.
     */
    public void enableCloneFeature() {
        return;
    }

    /**
     * get cloned package list
     */
    public Set<String> getClonedPackages() {
        return null;
    }

    /**
     * Force stop package. If the package was not cloned, it will only stop package when instanceId is
     * ALL_INSTANCE or MASTER_INSTANCE.
     * The application calling this API need to have android.Manifest.permission.FORCE_STOP_PACKAGES
     * @param pkg name of the package
     * @param instanceId ALL_INSTANCE for all instances; MASTER_INSTANCE for master instance only;
     *                   CLONE_INSTANCE for clone instance only
     */
    public void forceStopPackage(String pkg, int instanceId){
        return;
    }

    /**
     * get default cloneable packages
     * @return
     */
    public List<String> getDefaultCloneablePackages(){return null;}

    /**
     * get OEM cloneable packages
     * @return
     */
    public List<String> getOEMCloneablePackages(){return null;}

    /**
     * mark a given label to cloned label
     * @return marked label
     */
    public String markCloneLabel(String label, Context context) { return null; }

    /**
     * Mark the intent whether it needs to be choosen for clone or master instance.
     * Generally the framework will auto select the instance according to the context and intent, or pop up selection
     * window. But if you don't want to show the selection window, you can mark this intent to be "restricted", and then
     * startActivity, then it will auto select the instance: if calling from master, it will start master instance; if
     * calling from clone, it will start clone instance.
     * If you want to explicitly startActivity in a clone instance, see the API startActivityInClone
     * @param intent intent to start the activity
     * @param restricted true if the intent is restricted in clone or master instance; false is not restricted.
     */
    public void markIntentRestricted(Intent intent, boolean restricted){ return; }
}
