MX_LOCAL_PATH := $(call my-dir)

ifneq ($(MX_LOCAL_PATH)/MixinFeatures,)
  mixin_features_path := $(ANDROID_BUILD_TOP)/$(MX_LOCAL_PATH)/MixinFeatures
  ifeq ($(TARGET_PRODUCT),london)
    $(shell cp $(mixin_features_path)/MixinFeaturesForLondon $(ANDROID_BUILD_TOP)/$(MX_LOCAL_PATH)/MixinFeatures.java)
    $(warning cp MixinFeaturesForLondon MixinFeatures.java)
  else ifeq ($(TARGET_PRODUCT),london_cn)
    $(shell cp $(mixin_features_path)/MixinFeaturesForLondon $(ANDROID_BUILD_TOP)/$(MX_LOCAL_PATH)/MixinFeatures.java)
    $(warning cp MixinFeaturesForLondon MixinFeatures.java)
    $(warning cp $(mixin_features_path)/MixinFeaturesForLondon)
  else
    $(shell cp $(mixin_features_path)/MixinFeaturesForOther $(ANDROID_BUILD_TOP)/$(MX_LOCAL_PATH)/MixinFeatures.java)
    $(warning cp MixinFeaturesForOther MixinFeatures.java)
  endif
else
  $(warning /MixinFeatures is not exist!!!)
endif

