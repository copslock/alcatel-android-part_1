/* Copyright Statement:
 *
 */

/**
 * aidl file : IBackupManagerServiceCallback.aidl
 * This file contains definitions of functions which are exposed by service
 */
package com.tct.backupmanager;

oneway interface IBackupManagerServiceCallback {
    void onStart();
    void onComplete();
    void onUpdate(String info);
    void onProgress(int progress);
    void onError(String error);
}
