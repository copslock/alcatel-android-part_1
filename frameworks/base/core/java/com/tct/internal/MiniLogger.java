/* Copyright (C) 2016 Tcl Corporation Limited */
/*
 *
 *[FEATURE]-ADD-BEGIN by TCTSH.lijuan.wu, mini log, task-1488899
 *
 */


package com.tct.internal;

import android.os.FileUtils;
import com.tct.feature.Global;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.util.Date;
import java.util.Scanner;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.text.SimpleDateFormat;
import android.util.Log;
import android.util.Xml;
import com.android.internal.util.XmlUtils;

import java.io.ByteArrayOutputStream;

public class MiniLogger{
	private static final String TAG= "minilogger";
	private static final File mLogDir= new File("/tctpersist/minilog");
	private static final File mAppCrashDir =new File (mLogDir,"app_crash");
	private static final File mAppAnrDir =new File (mLogDir,"app_anr");
	private static final File mSystemCrashDir =new File (mLogDir,"system_crash");
	private static final File mCounterDir =new File (mLogDir,"counter");
	private static final String mSCrashCounterFilename="system_crash_counter.xml";
	private static final String mACrashCounterFilename="app_crash_counter.xml";
	private static final String mAAnrCounterFilename="app_anr_counter.xml";
	private static final  int  crash_log_max_num=10;
	private static final  int  anr_log_max_num=20;
	private static final String VERSION_PROP = "ro.build.version.incremental";

	/**
	* logger type
	*
	*/
	public static final int MINILOG_APP_CRASH = 1;
	/**
	* logger type
	* @hide
	*/
	public static final int MINILOG_APP_ANR = 2;
	/**
	* logger type
	* @hide
	*/
	public static final int MINILOG_SYSTEM_CRASH = 3;

	private MiniLogger(){
	}

	private static boolean checkLogDirs(){
		boolean flag=true;
		if (!mLogDir.exists()) {
		        if(!mLogDir.mkdirs()){
				Log.e(TAG, "make mLogDir failed");
				flag= false;
			}else{
				FileUtils.setPermissions(mLogDir.toString(), 0755, -1, -1);  // drwxrwxr-x
			}
		    }
		if (!mAppCrashDir.exists()) {
		        if(!mAppCrashDir.mkdirs()){
				Log.e(TAG, "make mAppCrashDir failed");
				flag= false;
			}else{
				FileUtils.setPermissions(mAppCrashDir.toString(), 0755, -1, -1);
			}
		    }
		if (!mAppAnrDir.exists()) {
		        if(!mAppAnrDir.mkdirs()){
				Log.e(TAG, "make mAppAnrDir failed");
				flag= false;
			}else{
				FileUtils.setPermissions(mAppAnrDir.toString(), 0755, -1, -1);
			}
		    }
		if (!mSystemCrashDir.exists()) {
		        if(!mSystemCrashDir.mkdirs()){
				Log.e(TAG, "make mSystemCrashDir failed");
				flag= false;
			}else{
				FileUtils.setPermissions(mSystemCrashDir.toString(), 0755, -1, -1);
			}
		    }
		if (!mCounterDir.exists()) {
		        if(!mCounterDir.mkdirs()){
				Log.e(TAG, "make mCounterDir failed");
				flag= false;
			}else{
				FileUtils.setPermissions(mCounterDir.toString(), 0755, -1, -1);
			}
		    }
		return flag;
	}
	private static final Comparator<File> lastModified = new Comparator<File>() {
		public int compare(File o1, File o2) {
			return o1.lastModified() == o2.lastModified() ? 0 : (o1.lastModified() < o2.lastModified() ? 1 : -1 ) ;
		}
	};
	private static void checkLogNum(int type){
		File floder=null;
		int log_max_num=0;
		if(type==MINILOG_APP_ANR){
			floder=mAppAnrDir;
			log_max_num=anr_log_max_num;
		}else if(type==MINILOG_APP_CRASH){
			floder=mAppCrashDir;
			log_max_num=crash_log_max_num;
		}else if(type==MINILOG_SYSTEM_CRASH){
			floder=mSystemCrashDir;
			log_max_num=crash_log_max_num;
		}

		 File[] arrayOfFile = floder.listFiles();
	        if(arrayOfFile!=null&&arrayOfFile.length>=log_max_num) {
	            Arrays.sort(arrayOfFile, lastModified);
	            arrayOfFile[arrayOfFile.length-1].delete();
	            Log.d(TAG, "checkLog: delete_log"+ arrayOfFile[arrayOfFile.length-1].getName());
	        }
	}
	private static boolean writeCrashLog(String moudle, int type, int crash_id,String crash_info,String pkg_info){
		boolean wFlag=false;
		File newXmlFile=null;
		FileOutputStream  filestream=null;
		String crash_type;

		//write this crash log
		 String time_str = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss").format(new Date(System.currentTimeMillis()));
		String ver = android.os.SystemProperties.get(VERSION_PROP);
		 StringBuilder filename = new StringBuilder(256);
		 if(ver!=null && !ver.equals("NULL")){
		 filename.append(ver);
		 filename.append("-");
		 }
		filename.append(moudle);
		filename.append("-");
		filename.append(time_str);
		filename.append(".xml");

		if(type==MINILOG_APP_CRASH){
			newXmlFile= new File(mAppCrashDir,filename.toString());
			crash_type="force close";
		}else if(type==MINILOG_APP_ANR){
			newXmlFile= new File(mAppAnrDir,filename.toString());
			crash_type="ANR";
		}else if(type==MINILOG_SYSTEM_CRASH){
			newXmlFile= new File(mSystemCrashDir,filename.toString());
			crash_type="system process killed";
		}else{
			Log.d(TAG, "unkonw  minilog type "+type);
			return false;
		}
		 Log.d(TAG,"newXmlFile name is "+newXmlFile.toString());
		 try{
		 	if(newXmlFile.exists()){
				wFlag= newXmlFile.delete();
			}else{
				wFlag=true;
			}
			if(wFlag){
				if(newXmlFile.createNewFile()){
					FileUtils.setPermissions(newXmlFile.toString(), 0644, -1, -1);
					filestream=new FileOutputStream(newXmlFile);
					XmlSerializer  serializer=Xml.newSerializer();
					serializer.setOutput(filestream,"UTF-8");
					serializer.startDocument(null, true);
					serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);

					//start tag
					serializer.startTag(null, "crash");
					serializer.attribute(null, "id",Integer.toString(crash_id));// crash_id

					serializer.startTag(null, "app");
					serializer.attribute(null, "name",moudle);

					if(pkg_info !=null){
					serializer.startTag(null, "package_info");
					serializer.text(pkg_info);
					serializer.endTag(null, "package_info");
					}

					serializer.startTag(null, "crash_type");
					serializer.text(crash_type);
					serializer.endTag(null, "crash_type");

					serializer.startTag(null, "crash_time");
					serializer.text(time_str);
					serializer.endTag(null, "crash_time");

					serializer.startTag(null, "crash_info");
					if(crash_info !=null)serializer.text(crash_info);
					serializer.endTag(null, "crash_info");

					serializer.endTag(null, "app");
					serializer.endTag(null, "crash");

					serializer.endDocument();
					serializer.flush();

					filestream.close();

				}
			}
		 }catch (IOException e){
		   wFlag=false;
		   e.printStackTrace();
		 }

   	 return wFlag;
	}

    private static boolean writeSMapXml(Map<String, String> aMap, File file) {
        OutputStream out = null;
	boolean  flag=false;
        try {
            out = new BufferedOutputStream(new FileOutputStream(file));
            XmlUtils.writeMapXml(aMap, out);
		flag=true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
	return flag;
    }

   private  static int checkCounterLog(File  counterFile, String nameStr){
   		Map<String, String> map =new HashMap<String, String>();
		boolean parsedSucess=true;
		InputStream in = null;
		int crash_id=0;
		try {
			in = new BufferedInputStream(new FileInputStream(counterFile));
			map = (HashMap<String, String>)XmlUtils.readMapXml(in);
		}  catch (FileNotFoundException e) {
			e.printStackTrace();
			parsedSucess=false;
		}catch (XmlPullParserException e) {
			e.printStackTrace();
			parsedSucess=false;
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (in != null) {
				try {
				    in.close();
				} catch (IOException e) {
				    e.printStackTrace();
				}
			}
		}
		if(!parsedSucess){
			if (counterFile.exists()){
				counterFile.delete();
			}
			try {
				counterFile.createNewFile();
				FileUtils.setPermissions(counterFile.toString(), 0644, -1, -1);
			} catch (IOException e) {
				Log.e(TAG, "Create system_counter log file fail !", e);
			}
		}
		String CounterValue=map.get(nameStr);
		if(CounterValue ==null || !parsedSucess){
			crash_id=1;
			map.put(nameStr, "1");
		}else{
			crash_id= Integer.valueOf(CounterValue)+1;
			map.put(nameStr, Integer.toString(crash_id));
		}
		writeSMapXml(map,counterFile);
		return crash_id;
   }

   public static final boolean writeSystemLog(String ex_name, String crash_info){
		boolean wFlag=false;
		int crash_id =0;
		if(!checkLogDirs()){
			return false;
		}

		//check crash id
		 File counterFile = new File(mCounterDir, mSCrashCounterFilename);

		crash_id=checkCounterLog(counterFile,ex_name);
		//check file num
	        checkLogNum(MINILOG_SYSTEM_CRASH);
		wFlag=writeCrashLog("systemserver",MINILOG_SYSTEM_CRASH,crash_id,crash_info,null);
		return wFlag;
   	}

   	public static final  boolean writeAppCrashLog(String app_name, String crash_info, String pkg_info){
		boolean wFlag=false;
		int crash_id =0;
		if(!checkLogDirs()){
			return false;
		}
		//check crash id
		 File counterFile = new File(mCounterDir, mACrashCounterFilename);
		crash_id=checkCounterLog(counterFile,app_name);
		//check file num
	        checkLogNum(MINILOG_APP_CRASH);
		wFlag=writeCrashLog(app_name,MINILOG_APP_CRASH,crash_id,crash_info,pkg_info);
		return wFlag;
   	}
   	public static final boolean writeAppAnrLog(String app_name, String anr_info,String pkg_info){
		boolean wFlag=false;
		int crash_id =0;
		if(!checkLogDirs()){
			return false;
		}
		//check crash id
		 File counterFile = new File(mCounterDir, mAAnrCounterFilename);
		crash_id=checkCounterLog(counterFile,app_name);
		//check file num
	        checkLogNum(MINILOG_APP_ANR);
		wFlag=writeCrashLog(app_name,MINILOG_APP_ANR,crash_id,anr_info,pkg_info);
		return wFlag;
   	}

}
