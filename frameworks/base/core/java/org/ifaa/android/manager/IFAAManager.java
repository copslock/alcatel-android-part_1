/* Copyright (C) 2016 Tcl Corporation Limited */
package org.ifaa.android.manager;

import android.content.Context;

/**
 * Created by sunchunzhi on 11/2/16.
 */
public abstract class IFAAManager {
    public static final int AUTH_TYPE_FINGERPRINT = 0x01;
    public static final int AUTH_TYPE_IRIS = 0x02;

    public static final int COMMAND_OK = 0;
    public static final int COMMAND_FAIL = -1;

    public abstract int getSupportBIOTypes(Context context);
    public abstract int startBIOManager(Context context,int authType);
    public abstract byte[] processCmd(Context context,byte[] param);
    public abstract String getDeviceModel();
    public abstract int getVersion();
}
