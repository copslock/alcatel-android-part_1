/* Copyright (C) 2016 Tcl Corporation Limited */
package org.ifaa.android.manager;

import android.content.Context;
import org.ifaa.android.manager.IFAAManager;
import org.ifaa.android.manager.IAlipayFingerprintService;

import android.content.Intent;
import android.os.IBinder;
import android.os.ServiceManager;
import android.util.Log;

/**
 * Created by sunchunzhi on 11/2/16.
 */
public class IFAAManagerImpl extends IFAAManager {
    private static final boolean debug = true;
    protected static final String TAG = "IFAAManager";
    protected static final int VERSION = 1;
    protected Context mContext;

    private static IAlipayFingerprintService mService;


    public IFAAManagerImpl(Context context) {
        mContext = context;
        if (mService == null) {
            if (debug) {
                Log.d(TAG, "AlipayFingerprint opening");
            }
            IBinder b = ServiceManager.getService(Context.ALIPAY_FP_SERVICE);
            mService = IAlipayFingerprintService.Stub.asInterface(b);
        }
    }


    @Override
    public int getSupportBIOTypes(Context context) {
        int result = 0;
        Log.i(TAG,"getSupportBIOTypes...");
        result |= AUTH_TYPE_FINGERPRINT;
        return result;
    }

    @Override
    public int startBIOManager(Context context, int authType) {
        if (debug)
            Log.d(TAG, "startFpManager");
        try {
            Intent i = new Intent();
            i.setClassName("com.android.settings",
                    "com.android.settings.fingerprint.FingerprintSettings");
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS); //MODIFIED by jianguang.sun, 2016-04-09,BUG-1923597
            context.startActivity(i);
            return COMMAND_OK;
        } catch (Exception e) {
            Log.e(TAG, "startFpManager failed");
            e.printStackTrace();
        }
        return COMMAND_FAIL;
    }

    @Override
    public byte[] processCmd(Context context, byte[] param) {
        if (debug)
            Log.d(TAG, " paramlength:" + param.length);
        try {
            if (mService != null) {
                return mService.processCmd(param);
            }
        } catch (Exception e) {
            Log.e(TAG, "processCmd failed");
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String getDeviceModel() {
        /* MODIFIED-BEGIN by chunzhi.sun, 2016-11-16,BUG-3358323*/
        if(debug) {
            Log.d(TAG, "getDeviceModel...");
        }
        return "TCL-L7100";
        /* MODIFIED-END by chunzhi.sun,BUG-3358323*/
    }

    @Override
    public int getVersion() {
        Log.i(TAG,"gerVersion...");
        return VERSION;
    }
}
