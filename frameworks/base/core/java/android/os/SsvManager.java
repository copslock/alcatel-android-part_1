package android.os;

import android.util.TctLog;

/**
 * a client of SsvService
 *
 * @hide
 */
public class SsvManager {
    private static final String TAG = "SsvManager";

    private static SsvManager mInstance = null;
    private ISsvService mService = null;

    // states
    public static final int STATE_ORIG = 0;
    public static final int STATE_DEF = 1;
    public static final int STATE_OP = 2;
    public static final int STATE_SUB = 3;
    // transitions:
    public static final int TRANSITION_NONE = 0;        // no transition
    public static final int TRANSITION_ORIG_TO_DEF = 1; // first boot without OP SIM card
    public static final int TRANSITION_ORIG_TO_OP = 2;  // first boot with OP SIM card
    public static final int TRANSITION_DEF_TO_OP = 3;   // noSIM/nonOPSIM to OP SIM
    public static final int TRANSITION_OP_TO_OP = 4;    // OP SIM 1 to OP SIM 2
    public static final int TRANSITION_OP_TO_DEF = 5; // OP SIM to noSIM/nonOPSIM
    public static final int TRANSITION_ORIG_TO_SUB = 6; // first boot with sub OP SIM card
    public static final int TRANSITION_DEF_TO_SUB = 7; // open market to sub op card

    public static final int UI_CHANGE = 0;
    public static final int CONFIG_CHANGE = 1;
    public static boolean mSwitchEnable = true;

    /**
     * get singleton
     */
    public static SsvManager getInstance() {
        if (mInstance == null) {
            mInstance = new SsvManager();
        }
        return mInstance;
    }

    private SsvManager() {
        IBinder b = ServiceManager.getService("ssv");
        if (b != null) {
            mService = ISsvService.Stub.asInterface(b);
            TctLog.d("liu", "SsvManager: init mService");
        }
    }

    /**
     * get SIM state transition
     * @see #TRANSITION_NONE
     * @see #TRANSITION_ORIG_TO_DEF
     * @see #TRANSITION_ORIG_TO_OP
     * @see #TRANSITION_DEF_TO_OP
     * @see #TRANSITION_OP_TO_OP
     * @see #TRANSITION_OP_TO_DEF
     */
    public int getTransition() {
        int transition = TRANSITION_NONE;
        if (mService != null) {
            try {
                transition = mService.getTransition();
            } catch (RemoteException e) {
                TctLog.e(TAG, "getTransition() failed");
                e.printStackTrace();
            }
        }
        TctLog.d("liu", "SsvManager--getTransition, transition: " + transition);
        return transition;
    }

//    public boolean saveSpnAlias(){
//        boolean b= false;
//        if (mService != null) {
//            try {
//                b = mService.saveFocSpnAlias();
//            } catch (RemoteException e) {
//                TctLog.e(TAG, "saveSpnAlias() failed");
//                e.printStackTrace();
//            }
//        }
//        TctLog.d("liu", "SsvManager--getTransition, transition: " + b);
//        return b;
//    }

    /**
     * get mccmnc of current SIM card
     */
    public String getSimMccMnc() {
        String mccmnc = "";
        if (mService != null) {
            try {
                mccmnc = mService.getSimMccMnc();
            } catch (RemoteException e) {
                TctLog.e(TAG, "getSimMccMnc() failed");
                e.printStackTrace();
            }
        }
        TctLog.d("liu", "SsvManager--getSimMccMnc, mccmnc: " + mccmnc);
        return mccmnc;
    }

    /**
     * get spn of current SIM card
     */
    public String getSimSpn() {
        String spn = "";
        if (mService != null) {
            try {
                spn = mService.getSimSpn();
            } catch (RemoteException e) {
                TctLog.e(TAG, "getSimSpn() failed");
                e.printStackTrace();
            }
        }
        TctLog.d("liu", "SsvManager--getSimSpn, spn: " + spn);
        return spn;
    }

    /**
     * get mcc alias of current SIM card
     */
    public String getFocMccAlias() {
        String str = "";
        try {
            str = mService.getCurrentFocMccAlias();
        } catch (RemoteException e) {
            TctLog.e(TAG, "getFocMccAlias() failed");
            e.printStackTrace();
        }
        return str;
    }

    /**
     * get Gid of current SIM card
     */
    public String getSimGid() {
        String gid = "";
        if (mService != null) {
            try {
                gid = mService.getSimGid();
            } catch (RemoteException e) {
                TctLog.e(TAG, "getSimGid() failed");
                e.printStackTrace();
            }
        }
        TctLog.d("liu", "SsvManager--getSimGid, gid: " + gid);
        return gid;
    }

    /**
     * get isMccMncChanged
     */
    public boolean isMccMncChanged() {
        boolean change = false;
        if (mService != null) {
            try {
                change = mService.isMccMncChanged();
                TctLog.d(TAG,"change = " + change);
            } catch (RemoteException e) {
                TctLog.e(TAG, "getSimSpn() failed");
                e.printStackTrace();
            }
        }
        TctLog.d("liu", "SsvManager--isMccMncChanged, change: " + change);
        return change;
    }

    /**
     * set spn of previous SIM card
     */
    public void setPreSimSpn(String spn) {
        TctLog.d("liu", "SsvManager--setPreSimSpn, spn: " + spn);
        if (mService != null) {
            try {
                mService.setPreSimSpn(spn);
            } catch (RemoteException e) {
                TctLog.e(TAG, "setPreSimSpn() failed");
                e.printStackTrace();
            }
        }
    }

//[BUGFIX]-Add-BEGIN by TSCD.kun.jiang,1030417
    public boolean getIsFirstRun() {
        boolean val = false;
            if (mService != null) {
                try {
                    val = mService.getIsFirstRun();
                } catch (RemoteException e) {
                    TctLog.e(TAG, "getIsFirstRun() failed");
                    e.printStackTrace();
                }
            }
            TctLog.d("liu", "SsvManager--getIsFirstRun, val: " + val);
            return val;
    }

    public void setIsFirstRun(boolean b) {
        TctLog.d(TAG, "setIsFirstRun()");
            if (mService != null) {
                try {
                    mService.setIsFirstRun(b);
                } catch (RemoteException e) {
                    TctLog.e(TAG, "setIsFirstRun() failed");
                    e.printStackTrace();
                }
            }
    }
    //[BUGFIX]-Add-END by TSCD.kun.jiang,1030417

    /**
     * get spn of previous SIM card
     */
    public String getPreSimSpn() {
        String spn = "";
        if (mService != null) {
            try {
                spn = mService.getPreSimSpn();
            } catch (RemoteException e) {
                TctLog.e(TAG, "getPreSimSpn() failed");
                e.printStackTrace();
            }
        }
        TctLog.d("liu", "SsvManager--getPreSimSpn, spn: " + spn);
        return spn;
    }

    /**
     * get mccmnc of previous (valid) SIM card
     */
    public String getPrevMccMnc() {
        String mccmnc = "";
        if (mService != null) {
            try {
                mccmnc = mService.getPrevMccMnc();
            } catch (RemoteException e) {
                TctLog.e(TAG, "getPrevMccMnc() failed");
                e.printStackTrace();
            }
        }
        TctLog.d("liu", "SsvManager--getPrevMccMnc, prevMccmnc: " + mccmnc);
        return mccmnc;
    }

    /**
     * get mccmnc of every previous (valid) SIM card
     */
    public String getEveryPrevMccMnc() {
        String mccmnc = "";
        if (mService != null) {
            try {
                mccmnc = mService.getEveryPrevMccMnc();
            } catch (RemoteException e) {
                TctLog.e(TAG, "getEveryPrevMccMnc() failed");
                e.printStackTrace();
            }
        }
        TctLog.d("liu", "SsvManager--getEveryPrevMccMnc, EveryPrevMccMnc: " + mccmnc);
        return mccmnc;
    }

    /**
     * Get mccmnc for language customization.
     * if SIM card changed from OP1 to OP2, SSV may allow user to change language but not settings/data.
     * so we need a new mccmnc value related to the language.
     */
    public String getLangMccMnc() {
        String mccmnc = "";
        if (mService != null) {
            try {
                mccmnc = mService.getLangMccMnc();
            } catch (RemoteException e) {
                TctLog.e(TAG, "getLangMccMnc() failed");
                e.printStackTrace();
            }
        }
        TctLog.d("liu", "SsvManager--getLangMccMnc, LangMccMnc: " + mccmnc);
        return mccmnc;
    }

    /**
    * set ssvactivity no btn state
    *
    */
    public void setButtonNoNum(String btnnonum) {
        if (mService != null) {
            synchronized (this) {
                try {
                    mService.setButtonNoNum(btnnonum);
                    TctLog.d("liu", "SsvManager--setButtonNoNum, btnnonum: " + btnnonum);
                } catch (RemoteException e) {
                    TctLog.e(TAG, "setButtonNoNum() failed");
                    e.printStackTrace();
                }
            }
        }
    }

    public String getButtonNoNum() {
        String btnnostate = "";
        if (mService != null) {
            try {
                btnnostate = mService.getButtonNoNum();
            } catch (RemoteException e) {
                TctLog.e(TAG, "getButtonNoNum() failed");
                e.printStackTrace();
            }
        }
        TctLog.d("liu", "SsvManager--getButtonNoNum, btnnostate: " + btnnostate);
        return btnnostate;
    }
    /**
     * set mccmnc of previous SIM card
     * @param mccmnc
     */
    public void setPrevMccMnc(String mccmnc) {
        if (mccmnc == null) {
            return;
        }
        TctLog.d("liu", "SsvManager--setPrevMccMnc, mccmnc: " + mccmnc);
        if (mService != null) {
            synchronized(this) {
                try {
                    mService.setPrevMccMnc(mccmnc);
                } catch (RemoteException e) {
                    TctLog.e(TAG, "setPrevMccMnc() failed");
                    e.printStackTrace();
                }
            }
        }
    }

  //modify by yanqi.liu for pop-up dialog again when change Simcard begin
    /**
     * set mccmnc of every previous SIM card
     * @param mccmnc
     */
    public void setEveryPrevMccMnc(String mccmnc) {
        if (mccmnc == null) {
            return;
        }
        TctLog.d("liu", "SsvManager--setEveryPrevMccMnc, mccmnc: " + mccmnc);
        if (mService != null) {
            synchronized(this) {
                try {
                    mService.setEveryPrevMccMnc(mccmnc);
                } catch (RemoteException e) {
                    TctLog.e(TAG, "setEveryPrevMccMnc() failed");
                    e.printStackTrace();
                }
            }
        }
    }

  //modify by yanqi.liu for pop-up dialog again when change Simcard end
    /**
     * set mccmnc for language customization
     * @param mccmnc
     */
    public void setLangMccMnc(String mccmnc) {
        if (mccmnc == null) {
            return;
        }

        TctLog.d("liu", "SsvManager--setLangMccMnc, mccmnc: " + mccmnc);
        if (mService != null) {
            synchronized(this) {
                try {
                    mService.setLangMccMnc(mccmnc);
                } catch (RemoteException e) {
                    TctLog.e(TAG, "setLangMccMnc() failed");
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * set current SIM state
     * @param state
     */
    public void setCurrentState(int state) {
        // Modiyied by bin.wei.hz for PR. 602551
        if (state != STATE_DEF && state != STATE_OP && state != STATE_SUB) {
            return;
        }

        TctLog.d("liu", "SsvManager--setCurrentState, state: " + state);
        if (mService != null) {
            synchronized(this) {
                try {
                    mService.setCurrentState(state);
                } catch (RemoteException e) {
                    TctLog.e(TAG, "setCurrentState() failed");
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * set current SIM state
     * @param state
     */
    public int getCurrentState() {
        int currentState = SsvManager.STATE_ORIG;
        if (mService != null) {
            synchronized (this) {
                try {
                    currentState = mService.getCurrentState();
                } catch (RemoteException e) {
                    TctLog.e(TAG, "getCurrentState() failed");
                    e.printStackTrace();
                }
            }
        }
        TctLog.d("liu", "SsvManager--getCurrentState, currentState: " + currentState);
        return currentState;
    }

    /**
     * determine that if current SIM card belongs to Operator
     */
    public boolean isOpSim() {
        boolean ret = false;
        if (mService != null) {
            try {
                ret = mService.isOpSim();
            } catch (RemoteException e) {
                TctLog.e(TAG, "isVFSim() failed");
                e.printStackTrace();
            }
        }
        TctLog.d("liu", "SsvManager--isOpSim, ret: " + ret);
        return ret;
    }

    /**
     * determine that if current SIM card belongs to Operator
     */
    public void setSwitchEnable(boolean enable) {
        if (mService != null) {
            try {
                mService.setSwitchEnable(enable);
                TctLog.d("liu", "SsvManager--setSwitchEnable, enable: " + enable);
            } catch (RemoteException e) {
                TctLog.e(TAG, "isAppNeedChange() failed");
                e.printStackTrace();
            }
        }
    }

    /**
     * determine that if apps need to change their configurations or not
     */
    public boolean isAppNeedChange(String app) {
        boolean ret = false;
        if (mService != null) {
            try {
                if (mSwitchEnable) {
                    ret = mService.isAppNeedChange(app);
                }
            } catch (RemoteException e) {
                TctLog.e(TAG, "isAppNeedChange() failed");
                e.printStackTrace();
            }
        }
        TctLog.d("liu", "SsvManager--isAppNeedChange, ret: " + ret);
        return ret;
    }

    public void appChangeComplete(String app) {
        if (mService != null) {
            try {
                mService.appChangeComplete(app);
                TctLog.d("liu", "SsvManager--appChangeComplete, app: " + app);
            } catch (RemoteException e) {
                TctLog.e(TAG, "appChangeComplete() failed");
                e.printStackTrace();
            }
        }
    }

    /**
     * check if SIM card is locked
     */
    public boolean isSimLock() {
        boolean ret = false;
        if (mService != null) {
            try {
                ret = mService.isSimLock();
            } catch (RemoteException e) {
                TctLog.e(TAG, "isSimLock() failed");
                e.printStackTrace();
            }
        }
        TctLog.d("liu", "SsvManager--isSimLock, ret: " + ret);
        return ret;
    }

    /**
     * locked SIM card is unlocked, update mccmnc and transition
     */
    public void unlock(String mccmnc, String spn) {
        if (mService != null) {
            try {
              TctLog.d("liu", "SsvManager--unlock, mccmnc: " + mccmnc + " spn: " + spn);
                mService.unlock(mccmnc, spn);
            } catch (RemoteException e) {
                TctLog.e(TAG, "unlock() failed");
                e.printStackTrace();
            }
        }
    }

    public void unLock(String mccmnc, String spn, String gid){
        if (mService != null) {
            try {
                TctLog.d("liu", "SsvManager--unlock, mccmnc: " + mccmnc + " spn: " + spn);
                mService.unLock(mccmnc, spn, gid);
            } catch (RemoteException e) {
                TctLog.e(TAG, "unlock() failed");
                e.printStackTrace();
            }
        }
    }


    public boolean hasPinLock() {
        boolean ret = false;
        if (mService != null) {
            try {
                ret = mService.hasPinLock();
                TctLog.d("liu", "SsvManager--hasPinLock, ret: " + ret);
            } catch (RemoteException e) {
                TctLog.e(TAG, "hasPinLock() failed");
                e.printStackTrace();
            }
        }
        return ret;

    }

    public void setSsvKeyGuardState(boolean state) {
        if (mService != null) {
            try {
                mService.setSsvKeyGuardState(state);
            } catch (RemoteException e) {
                TctLog.e(TAG, "setSsvKeyGuardState failed");
                e.printStackTrace();
            }
        }
    }

    public boolean getSsvKeyGuardState() {
        boolean ret = false;
        if (mService != null) {
            try {
                ret = mService.getSsvKeyGuardState();
            } catch (RemoteException e) {
                TctLog.e(TAG, "getSsvKeyGuardState failed");
                e.printStackTrace();
            }
        }
        return ret;
    }

    public boolean isSsvEnable() {
        if (mService != null) {
            try {
              TctLog.d("liu", "SsvManager--isSsvEnable, enable: " + mService.isSsvEnable());
                return mService.isSsvEnable();
            } catch (RemoteException e) {
                TctLog.e(TAG, "Call isSsvEnable failed");
                e.printStackTrace();
            }
        }
        return false;
    }

    public void initSsvSettings() {
        if (mService != null) {
            try {
                mService.initSsvSettings();
            } catch (RemoteException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    //[FEATURE]-Add-BEGIN-by TSCD.Xian.Wang,CR-905066,03/01/2015
    public void updateMinMatchField(){
        if (mService != null) {
            try {
                mService.updateMinMatchField();
            } catch (RemoteException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }
    //[FEATURE]-Add-END-by TSCD.Xian.Wang,CR-905066,03/01/2015
    //[FEATURE]-ADD-BEGIN by TSNJ.xiaowei.yang-nb,PR-1067379,09/09/2015
    public void updateLogo()
    {
        if (mService != null) {
            try {
                mService.updateLogo();
            } catch (RemoteException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }
    //[FEATURE]-ADD-END by TSNJ.xiaowei.yang-nb,PR-1067379,09/09/2015
}
