/*willab*/
package android.hardware;

import android.os.Build;
import android.annotation.SystemApi;
import android.util.Log;

/**
 * @hide
 */
public class FileLock {
	private static final String TAG = "FileLock";
	private FlOperationResult mOpsResult;

	public FileLock() {
		mOpsResult = new FlOperationResult();
    }

	public final int lockFile(String fileName) {
		Log.e(TAG, "hello lockFile...");
		return native_lockFile(fileName);
	}

	public final int unLockFile(String fileName) {
		Log.e(TAG, "hello unlockFile...");
		return native_unLockFile(fileName);
	}

	public final int hideLockFile() {
		Log.e(TAG, "hello hideLockFiles...");
		return native_hideLockFile();
	}

	public final int unHideAnyFile() {
		Log.e(TAG, "hello unHideAnyFile...");
		return native_unHideAnyFile();
	}

	public final int hideUnLockFile() {
		Log.e(TAG, "hello hideUnLockFile...");
		return native_hideUnLockFile();
	}

	public final FlOperationResult checkFileState(String fileName) {
		Log.e(TAG, "hello checkFileState...");
		native_checkFileState(fileName);
		Log.e("hello", "result is " + mOpsResult.retVal + " state is "+mOpsResult.isLocked);
		return mOpsResult;
	}

	public void callbackForCheckState(int result, int state)  
    {  
		Log.e(TAG, "hello callbackForCheckState...");
		mOpsResult.retVal = result;
		if (state == 0)
			mOpsResult.isLocked = false;
		else
			mOpsResult.isLocked = true;
    }

	public final int checkFileLockService() {
		Log.e(TAG, "hello checkFileLockService...");
		return native_checkFileLockService();
	}

	//public native int native_create();
	private native final int native_lockFile(String fileName);
	private native final int native_unLockFile(String fileName);
	private native final int native_hideLockFile();
	private native final int native_unHideAnyFile();
	private native final int native_hideUnLockFile();
	private native final void native_checkFileState(String fileName);
	private native final int native_checkFileLockService();
}




