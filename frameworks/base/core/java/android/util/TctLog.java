// Architecture team modificaiton only
// ArchiNO:(011.tct_log)

package android.util;
import com.tct.feature.Global;

/**
 * @hide
 */
public final class TctLog {
    private static final int TCTLOG_BASE_FLAG = 1;
    private static final int TCTLOG_DEBUG_ENABLE = TCTLOG_BASE_FLAG << 0;
    private static final int TCTLOG_INFO_ENABLE  = TCTLOG_BASE_FLAG << 1;
    private static final int TCTLOG_WARN_ENABLE  = TCTLOG_BASE_FLAG << 2;
    private static final int TCTLOG_ERROR_ENABLE = TCTLOG_BASE_FLAG << 3;

    static final boolean debugEnable = (Global.TCT_TARGET_TCTLOG & TCTLOG_DEBUG_ENABLE) != 0;
    static final boolean infoEnable  = (Global.TCT_TARGET_TCTLOG & TCTLOG_INFO_ENABLE)  != 0;
    static final boolean warnEnable  = (Global.TCT_TARGET_TCTLOG & TCTLOG_WARN_ENABLE)  != 0;
    static final boolean errorEnable = (Global.TCT_TARGET_TCTLOG & TCTLOG_ERROR_ENABLE) != 0;

    private TctLog(){}

    public static int d(String tag, String msg) {
        if (debugEnable) {
            return Log.println_native(Log.LOG_ID_MAIN, Log.DEBUG, tag, msg);
        }
        return  0;
    }

    public static int i(String tag, String msg) {
        if (infoEnable) {
            return Log.println_native(Log.LOG_ID_MAIN, Log.INFO, tag, msg);
        }
        return  0;
    }

    public static int w(String tag, String msg) {
        if (warnEnable) {
            return Log.println_native(Log.LOG_ID_MAIN, Log.WARN, tag, msg);
        }
        return  0;
    }

    public static int e(String tag, String msg) {
        if (errorEnable) {
            return Log.println_native(Log.LOG_ID_MAIN, Log.ERROR, tag, msg);
        }
        return  0;
    }

    public static int d(String tag, String msg, Throwable tr) {
        return d(tag, msg + '\n' + Log.getStackTraceString(tr));
    }

    public static int i(String tag, String msg, Throwable tr) {
        return i(tag, msg + '\n' + Log.getStackTraceString(tr));
    }

    public static int w(String tag, String msg, Throwable tr) {
        return w(tag, msg + '\n' + Log.getStackTraceString(tr));
    }

    public static int w(String tag, Throwable tr) {
        return w(tag, Log.getStackTraceString(tr));
    }

    public static int e(String tag, String msg, Throwable tr) {
        return e(tag, msg + '\n' + Log.getStackTraceString(tr));
    }    
}
