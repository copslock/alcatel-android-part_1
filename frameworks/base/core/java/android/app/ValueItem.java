/* Copyright (C) 2016 Tcl Corporation Limited */
/* ***************************************************************************/
/*                                                       Date : Feb 2, 2015  */
/*                      Android Resource Customization Tool                  */
/*              Copyright (c) 2015 JRD Communications, Inc.                  */
/* ***************************************************************************/
/*                                                                           */
/*    This material is company confidential, cannot be reproduced in any     */
/*    form without the written permission of JRD Communications, Inc.        */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/*   Author :  Hanwu.xie (XHANWU)                                                    */
/*   Role :    Telecom Leader                                                */
/*   Reference documents :                                                   */
/*---------------------------------------------------------------------------*/
/* Comments :                                                                */
/*     file    : ValueItem.java                                                */
/*     Labels  :                                                             */
/*===========================================================================*/
/* Modifications   (month/day/year)                                          */
/*---------------------------------------------------------------------------*/
/* date    | author    | modification                                        */
/*---------+-----------+-----------------------------------------------------*/
/*02/02/15 | hanwu.xie | on creation                                         */
/*---------+-----------+-----------------------------------------------------*/
/*         |           |                                                     */
/*===========================================================================*/
/* Problems Report                                                           */
/*---------------------------------------------------------------------------*/
/* date    | author    | PR #     |                                          */
/*---------|-----------|----------|------------------------------------------*/
/*         |           |          |                                          */
/*---------|-----------|----------|------------------------------------------*/
/*         |           |          |                                          */
/*===========================================================================*/

package android.app;

import android.os.Parcel;
import android.os.Parcelable;

/** @hide */
public class ValueItem implements Parcelable {
    private String attrName;
    private String ModOP;
    private String packageName;
    private String attrValue;
    private String currentLanguage;
    private String currengContext;
    private int zoneWidth;
    private int zoneHeight;
    private int textSize;
    private int isBold;
    private int isItalic;
    private int isEllipse;

    public String getAttrName() {
        return attrName;
    }

    public void setAttrName(String attr) {
        attrName = attr;
    }

    public String getModOP() {
        return ModOP;
    }

    public void setModOP(String modop) {
        ModOP = modop;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packName) {
        packageName = packName;
    }

    public String getAttrValue() {
        return attrValue;
    }

    public void setAttrValue(String value) {
        attrValue = value;
    }

    public int getZoneWidth() {
        return zoneWidth;
    }

    public void setZoneWidth(int width) {
        zoneWidth = width;
    }

    public int getZoneHeight() {
        return zoneHeight;
    }

    public void setZoneHeight(int height) {
        zoneHeight = height;
    }

    public int getTextSize() {
        return textSize;
    }

    public void setTextSize(int size) {
        textSize = size;
    }

    public int getTextBold() {
        return isBold;
    }

    public void setTextBold(int bold) {
        isBold = bold;
    }

    public int getTextItalic() {
        return isItalic;
    }

    public void setTextItalic(int italic) {
        isItalic = italic;
    }

    public int getEllipse() {
        return isEllipse;
    }

    public void setEllipse(int ellipse) {
        isEllipse = ellipse;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getCurrentLanguage() {
        return currentLanguage;
    }

    public void setCurrentLanguage(String currLang) {
        currentLanguage = currLang;
    }

    public String getCurrentContext() {
        return currengContext;
    }

    public void setCurrentContext(String context) {
        currengContext = context;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(attrName);
        dest.writeString(ModOP);
        dest.writeString(packageName);
        dest.writeString(attrValue);
        dest.writeInt(zoneWidth);
        dest.writeInt(zoneHeight);
        dest.writeInt(textSize);
        dest.writeInt(isBold);
        dest.writeInt(isItalic);
        dest.writeInt(isEllipse);
        dest.writeString(currentLanguage);
    }

    public static final Parcelable.Creator<ValueItem> CREATOR = new Parcelable.Creator<ValueItem>() {
        @Override
        public ValueItem createFromParcel(Parcel in) {
            ValueItem value = new ValueItem();
            value.attrName = in.readString();
            value.ModOP = in.readString();
            value.packageName = in.readString();
            value.attrValue = in.readString();
            value.zoneWidth = in.readInt();
            value.zoneHeight = in.readInt();
            value.textSize = in.readInt();
            value.isBold = in.readInt();
            value.isItalic = in.readInt();
            value.isEllipse = in.readInt();
            value.currentLanguage = in.readString();
            return value;
        }

        @Override
        public ValueItem[] newArray(int size) {
            return new ValueItem[size];
        }
    };
}
