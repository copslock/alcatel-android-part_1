/* Copyright (C) 2016 Tcl Corporation Limited */
/* ***************************************************************************/
/*                                                       Date : Feb 2, 2015  */
/*                      Android Resource Customization Tool                  */
/*              Copyright (c) 2015 JRD Communications, Inc.                  */
/* ***************************************************************************/
/*                                                                           */
/*    This material is company confidential, cannot be reproduced in any     */
/*    form without the written permission of JRD Communications, Inc.        */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/*   Author :  Hanwu.xie (XHANWU)                                           */
/*   Role :    Telecom Leader                                                */
/*   Reference documents :                                                   */
/*---------------------------------------------------------------------------*/
/* Comments :                                                                */
/*     file    : StringMapParcelable.java                                     */
/*     Labels  :                                                             */
/*===========================================================================*/
/* Modifications   (month/day/year)                                          */
/*---------------------------------------------------------------------------*/
/* date    | author    | modification                                        */
/*---------+-----------+-----------------------------------------------------*/
/*02/02/15 | hanwu.xie | on creation                                         */
/*---------+-----------+-----------------------------------------------------*/
/*         |           |                                                     */
/*===========================================================================*/
/* Problems Report                                                           */
/*---------------------------------------------------------------------------*/
/* date    | author    | PR #     |                                          */
/*---------|-----------|----------|------------------------------------------*/
/*         |           |          |                                          */
/*---------|-----------|----------|------------------------------------------*/
/*         |           |          |                                          */
/*===========================================================================*/
package android.app;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.util.HashMap;

 /** @hide */
public class StringMapParcelable implements Parcelable {
    private static final String TAG = "StringMapParcelable";

    private HashMap<String, ValueItem> stringInfoMap;


    public StringMapParcelable() {
    }

    public HashMap<String, ValueItem> getStringMap() {
        return stringInfoMap;
    }

    public void setStringMap(HashMap<String, ValueItem> map) {
        this.stringInfoMap = map;
    }


    @Override
    public int describeContents() {
        Log.i(TAG, "stringMap parcelable");
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        Log.i(TAG, "stringMap writeToParcel");
        dest.writeMap(stringInfoMap);
    }

    public static final Parcelable.Creator<StringMapParcelable> CREATOR = new Parcelable.Creator<StringMapParcelable>() {
        @SuppressWarnings("unchecked")
        @Override
        public StringMapParcelable createFromParcel(Parcel in) {
            Log.i(TAG, "stringMap createFromParcel");
            StringMapParcelable stringMap = new StringMapParcelable();
            stringMap.setStringMap(in.readHashMap(null));
            return stringMap;
        }

        @Override
        public StringMapParcelable[] newArray(int size) {
            Log.i(TAG, "newArray");
            return new StringMapParcelable[size];
        }
    };

}
