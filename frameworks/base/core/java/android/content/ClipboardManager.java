/**
 * Copyright (c) 2010, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.content;

import android.content.Context;
import android.os.Message;
import android.os.RemoteException;
import android.os.Handler;
import android.os.IBinder;
import android.os.ServiceManager;

import java.util.ArrayList;

//[FEATURE]-Add-BEGIN by TCL_XA, 09/28/2016,FR-2825857
import android.app.ActivityManager;
import android.os.SystemProperties;
import android.util.Log;
import java.util.List;

import com.tcl.enc.TclEncrypt;
import com.tcl.enc.Blowfish;
import com.tcl.enc.MixinFeatures;
import android.content.ClipData.Item;
//[FEATURE]-Add-END by TCL_XA, 09/28/2016,FR-2825857

/**
 * Interface to the clipboard service, for placing and retrieving text in
 * the global clipboard.
 *
 * <p>
 * You do not instantiate this class directly; instead, retrieve it through
 * {@link android.content.Context#getSystemService}.
 *
 * <p>
 * The ClipboardManager API itself is very simple: it consists of methods
 * to atomically get and set the current primary clipboard data.  That data
 * is expressed as a {@link ClipData} object, which defines the protocol
 * for data exchange between applications.
 *
 * <div class="special reference">
 * <h3>Developer Guides</h3>
 * <p>For more information about using the clipboard framework, read the
 * <a href="{@docRoot}guide/topics/clipboard/copy-paste.html">Copy and Paste</a>
 * developer guide.</p>
 * </div>
 *
 * @see android.content.Context#getSystemService
 */
public class ClipboardManager extends android.text.ClipboardManager {
    private final static Object sStaticLock = new Object();
    private static IClipboard sService;

    private final Context mContext;

    //[FEATURE]-Add-BEGIN by TCL_XA, 09/28/2016,FR-2825857

    static final String TCL_TAG = "Mixin";

    static final String WECHAT_LAUNCHER_ACTIVITY = "com.tencent.mm.ui.LauncherUI";
    static final String WECHAT_CHATTING_ACTIVITY = "com.tencent.mm.ui.chatting.ChattingUI";
    static final String WECHAT_FAVOR_ACTIVITY = "com.tencent.mm.plugin.favorite.ui.FavoriteIndexUI";
    static final String WECHAT_FAVOR_DETAILS_ACTIVITY = "com.tencent.mm.plugin.favorite.ui.detail.FavoriteTextDetailUI";
    static final String WECHAT_FAVOR_TAG_EDIT_ACTIVITY = "com.tencent.mm.plugin.favorite.ui.FavTagEditUI";
    static final String WECHAT_FAVOR_DETAILS_EDIT_ACTIVITY = "com.tencent.mm.plugin.favorite.ui.FavTextEditUI";

    private TclEncrypt mEnc = null;

    private String mCurrentAc = "";

    //[FEATURE]-Add-END by TCL_XA, 09/28/2016,FR-2825857

    private final ArrayList<OnPrimaryClipChangedListener> mPrimaryClipChangedListeners
             = new ArrayList<OnPrimaryClipChangedListener>();

    private final IOnPrimaryClipChangedListener.Stub mPrimaryClipChangedServiceListener
            = new IOnPrimaryClipChangedListener.Stub() {
        public void dispatchPrimaryClipChanged() {
            mHandler.sendEmptyMessage(MSG_REPORT_PRIMARY_CLIP_CHANGED);
        }
    };

    static final int MSG_REPORT_PRIMARY_CLIP_CHANGED = 1;

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_REPORT_PRIMARY_CLIP_CHANGED:
                    reportPrimaryClipChanged();
            }
        }
    };

    /**
     * Defines a listener callback that is invoked when the primary clip on the clipboard changes.
     * Objects that want to register a listener call
     * {@link android.content.ClipboardManager#addPrimaryClipChangedListener(OnPrimaryClipChangedListener)
     * addPrimaryClipChangedListener()} with an
     * object that implements OnPrimaryClipChangedListener.
     *
     */
    public interface OnPrimaryClipChangedListener {

        /**
         * Callback that is invoked by {@link android.content.ClipboardManager} when the primary
         * clip changes.
         */
        void onPrimaryClipChanged();
    }

    static private IClipboard getService() {
        synchronized (sStaticLock) {
            if (sService != null) {
                return sService;
            }
            IBinder b = ServiceManager.getService("clipboard");
            sService = IClipboard.Stub.asInterface(b);
            return sService;
        }
    }

    /** {@hide} */
    public ClipboardManager(Context context, Handler handler) {
        mContext = context;

        //[FEATURE]-Add-BEGIN by TCL_XA, 09/28/2016,FR-2825857
        if (MixinFeatures.includeText()) {
            updateCurrentActivityName();
        }
        //[FEATURE]-Add-END by TCL_XA, 09/28/2016,FR-2825857

    }

    /**
     * Sets the current primary clip on the clipboard.  This is the clip that
     * is involved in normal cut and paste operations.
     *
     * @param clip The clipped data item to set.
     */
    public void setPrimaryClip(ClipData clip) {
        try {
            if (clip != null) {

                // [FEATURE]-Add-BEGIN by TCL_XA, 09/28/2016,FR-2825857
                if (MixinFeatures.includeText()) {
                    if (isEncSettingOpen()) {
                        String acName = getCurrentActivityName();
                        if (isInWeChat(acName)) {

                            CharSequence ctext = clip.getItemAt(0).getText();
                            if (null != ctext) {
                                String showText = ctext.toString();

                                mEnc = (null == mEnc) ? TclEncrypt.getInstance() : mEnc;
                                if (mEnc.isEncrypted(showText, true, false)) {
                                    ctext = mEnc.decrypt(TclEncrypt.CRYPTOGRAPHY_TYPE_BLOWFISH, showText);
                                    if (ctext.equals(Blowfish.ERROR_VALUE)) {
                                        ctext = showText;
                                    }
                                    clip = ClipData.newPlainText(null, ctext);
                                }
                            }
                        }
                    }
                }
                // [FEATURE]-Add-END by TCL_XA, 09/28/2016,FR-2825857


                clip.prepareToLeaveProcess(true);
            }
            getService().setPrimaryClip(clip, mContext.getOpPackageName());
        } catch (RemoteException e) {
            throw e.rethrowFromSystemServer();
        }
    }

    /**
     * Returns the current primary clip on the clipboard.
     */
    public ClipData getPrimaryClip() {
        try {
            return getService().getPrimaryClip(mContext.getOpPackageName());
        } catch (RemoteException e) {
            throw e.rethrowFromSystemServer();
        }
    }

    /**
     * Returns a description of the current primary clip on the clipboard
     * but not a copy of its data.
     */
    public ClipDescription getPrimaryClipDescription() {
        try {
            return getService().getPrimaryClipDescription(mContext.getOpPackageName());
        } catch (RemoteException e) {
            throw e.rethrowFromSystemServer();
        }
    }

    /**
     * Returns true if there is currently a primary clip on the clipboard.
     */
    public boolean hasPrimaryClip() {
        try {
            return getService().hasPrimaryClip(mContext.getOpPackageName());
        } catch (RemoteException e) {
            throw e.rethrowFromSystemServer();
        }
    }

    public void addPrimaryClipChangedListener(OnPrimaryClipChangedListener what) {
        synchronized (mPrimaryClipChangedListeners) {
            if (mPrimaryClipChangedListeners.size() == 0) {
                try {
                    getService().addPrimaryClipChangedListener(
                            mPrimaryClipChangedServiceListener, mContext.getOpPackageName());
                } catch (RemoteException e) {
                    throw e.rethrowFromSystemServer();
                }
            }
            mPrimaryClipChangedListeners.add(what);
        }
    }

    public void removePrimaryClipChangedListener(OnPrimaryClipChangedListener what) {
        synchronized (mPrimaryClipChangedListeners) {
            mPrimaryClipChangedListeners.remove(what);
            if (mPrimaryClipChangedListeners.size() == 0) {
                try {
                    getService().removePrimaryClipChangedListener(
                            mPrimaryClipChangedServiceListener);
                } catch (RemoteException e) {
                    throw e.rethrowFromSystemServer();
                }
            }
        }
    }

    /**
     * @deprecated Use {@link #getPrimaryClip()} instead.  This retrieves
     * the primary clip and tries to coerce it to a string.
     */
    public CharSequence getText() {
        ClipData clip = getPrimaryClip();
        if (clip != null && clip.getItemCount() > 0) {
            return clip.getItemAt(0).coerceToText(mContext);
        }
        return null;
    }

    //[FEATURE]-Add-BEGIN by TCL_XA, 09/28/2016,FR-2825857

    /**
     * To check whether the switch of 'Mixin' is on/off.
     */
    private boolean isEncSettingOpen() {
        String encSetting = SystemProperties.get("sys.tcl_enc_setting", "0");
        // It is open when the encSetting is "1" and it is closed when the
        // encSetting is "0".
        if (null != encSetting && encSetting.equals("1")) {
            return true;
        } else {
            return false;
        }
    }


    /**
     * Get the name of activity which is displaying in the screen.
     */
    private String getCurrentActivityName() {
        if (mCurrentAc.equals("")) {
            mCurrentAc = SystemProperties.get("sys.tcl_enc_target", "null");
        }

        return mCurrentAc;
    }

    /**
     * In case of someone want to update the current activity name,
     * or it really need to be updated.
     */
    private String updateCurrentActivityName() {
        mCurrentAc = "";
        mCurrentAc = getCurrentActivityName();
        return mCurrentAc;
    }


    /**
     * To check whether we running in App 'WeChat'.
     */
    private boolean isInWeChat(String activityName) {
        if (activityName.equals(WECHAT_LAUNCHER_ACTIVITY) || activityName.equals(WECHAT_CHATTING_ACTIVITY)
            || activityName.equals(WECHAT_FAVOR_ACTIVITY) || activityName.equals(WECHAT_FAVOR_DETAILS_ACTIVITY)
            || activityName.equals(WECHAT_FAVOR_DETAILS_EDIT_ACTIVITY)) {
            return true;
        } else {
            return false;
        }
    }

    //[FEATURE]-Add-END by TCL_XA, 09/28/2016,FR-2825857

    /**
     * @deprecated Use {@link #setPrimaryClip(ClipData)} instead.  This
     * creates a ClippedItem holding the given text and sets it as the
     * primary clip.  It has no label or icon.
     */
    public void setText(CharSequence text) {
        setPrimaryClip(ClipData.newPlainText(null, text));
    }

    /**
     * @deprecated Use {@link #hasPrimaryClip()} instead.
     */
    public boolean hasText() {
        try {
            return getService().hasClipboardText(mContext.getOpPackageName());
        } catch (RemoteException e) {
            throw e.rethrowFromSystemServer();
        }
    }

    void reportPrimaryClipChanged() {
        Object[] listeners;

        synchronized (mPrimaryClipChangedListeners) {
            final int N = mPrimaryClipChangedListeners.size();
            if (N <= 0) {
                return;
            }
            listeners = mPrimaryClipChangedListeners.toArray();
        }

        for (int i=0; i<listeners.length; i++) {
            ((OnPrimaryClipChangedListener)listeners[i]).onPrimaryClipChanged();
        }
    }
}
