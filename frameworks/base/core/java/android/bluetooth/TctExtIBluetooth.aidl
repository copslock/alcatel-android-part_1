package android.bluetooth;

import android.bluetooth.IBluetoothCallback;
import android.bluetooth.IBluetoothStateChangeCallback;
import android.bluetooth.BluetoothActivityEnergyInfo;
import android.bluetooth.BluetoothDevice;
import android.os.ParcelUuid;
import android.os.ParcelFileDescriptor;

/**
 * System private API for talking with the Bluetooth service.
 *
 * {@hide}
 */
interface TctExtIBluetooth
{
    /* MODIFIED-BEGIN by na.long, 2016-08-03,BUG-2655157*/
    int tct_setBtTestMode(int mode);
    int tct_setBtChannel(int position);
    /* MODIFIED-END by na.long,BUG-2655157*/
}