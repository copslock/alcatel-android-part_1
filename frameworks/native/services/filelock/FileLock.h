/*willab*/
#ifndef ANDROID_FILE_LOCK_H
#define ANDROID_FILE_LOCK_H

#include <stdint.h>
#include <sys/types.h>
//#include <utils/threads.h>
//#include <utils/AndroidThreads.h>
#include <utils/RefBase.h>
#include <utils/Looper.h>
#include <utils/String8.h>
#include <binder/BinderService.h>
#include <gui/IFileLockServer.h>
#include <cutils/compiler.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/syscall.h>
#include <linux/limits.h>
#include <utils/Thread.h>
#include <semaphore.h>







#if __clang__
// Clang warns about SensorEventConnection::dump hiding BBinder::dump
// The cause isn't fixable without changing the API, so let's tell clang
// this is indeed intentional.
#pragma clang diagnostic ignored "-Woverloaded-virtual"
#endif


#define FILE_LOCK_MODULE                 "/dev/file_lock"


#define FILE_LOCK_EN                     1
#define FILE_LOCK_DIS                    0
#define FILE_LOCK_HIDE_EN                1
#define FILE_LOCK_HIDE_DIS               0

#define FILE_LOCK_OPTION_FAILED		     -1
#define FILE_LOCK_OPTION_SUCCESS		 0

#define BUFQ_MAX_LENTH 20


#define FILE_LOCK_CMD_BASE               100
#define FILE_LOCK_CMD_GET_RANDOM         FILE_LOCK_CMD_BASE+1
#define FILE_LOCK_CMD_REGISTER_PID       FILE_LOCK_CMD_BASE+2
#define FILE_LOCK_CMD_HIDE_LOCK          FILE_LOCK_CMD_BASE+3
#define FILE_LOCK_CMD_FILE_LOCK          FILE_LOCK_CMD_BASE+4
#define FILE_LOCK_CMD_CHECK_STATE        FILE_LOCK_CMD_BASE+5

#define HIDE_LOCK_FLIE                   0
#define UNHIDE_ANY_FILE                  1
#define HIDE_UNLOCK_FILE                 2


// ---------------------------------------------------------------------------


namespace android {
// ---------------------------------------------------------------------------

class EventThread : public Thread
{
public:
    EventThread(){
        mFLModuleFd = -1;
        mInitFlag = false;
        mEventCount = 0;
        mWaitResultCount = 0;
        mEnabled = false;

        sem_init(&mEventSem, 0, 1);
        sem_init(&mResultSem, 0, 0);

        memset(&mFileOpsEvent, 0, sizeof(struct FileOpsEvent));
    }
    
    ~EventThread() {
        if (mFLModuleFd > 0) 
		    close(mFLModuleFd);
        sem_destroy(&mEventSem);
        sem_destroy(&mResultSem);
    }

    virtual void onFirstRef();
    
    void initiate(void);
    void parsePath(char* srcPath, char* destPath);
    void onReceiveEvent(int cmd, const char* filename, unsigned char flag);
    int waitForResult(void);
    int waitForResult(int& flag);
    void NotifyResult();
    unsigned long calcMagicData(unsigned long arg);
    int registerThreadToKmodule();
    void waitForLastEventProcess();
    void releaseCurrentEventProcess();
    int processCmd();
    void setEnable(bool enable);

private:
    virtual bool threadLoop();
    virtual status_t readyToRun();

    struct registerPtheadInfo {
        pid_t tid;
        unsigned long magic_data;
    };
    
    struct FileOpsEvent {
        int cmd;
        
        struct fileLockControl {
            char filename[PATH_MAX+NAME_MAX];
            unsigned char flag;
        }file_ctl;
        
    }mFileOpsEvent;
    
    int mFLModuleFd;
    mutable Condition mCondition;
    mutable Mutex mLock;
    bool mInitFlag;
    int mResult;
    int mEventCount;
    sem_t mEventSem;
    sem_t mResultSem;
    int mWaitResultCount;
    bool mEnabled;

    // constants
//    const unsigned long mCodeKeyBuf[8] = {
//        0x12d5d74e22c15f98, 
 //       0x4676a1b4dd9878ef,
//        0x45631dbc5ee13756,
 //       0x14561ce156431abd,
  //      0xfedc146913213411,
   //     0x31256746bf4ac953,
   //     0xbfdd14613136a1ce,
//0x16431645646df1bd,
 //   };
    
const unsigned long mCodeKeyBuf[8] = {

        static_cast<unsigned long>(0x12d5d74e22c15f98),

        static_cast<unsigned long>(0x4676a1b4dd9878ef),

        static_cast<unsigned long>(0x45631dbc5ee13756),

        static_cast<unsigned long>(0x14561ce156431abd),

        static_cast<unsigned long>(0xfedc146913213411),

        static_cast<unsigned long>(0x31256746bf4ac953),

        static_cast<unsigned long>(0xbfdd14613136a1ce),

        static_cast<unsigned long>(0x16431645646df1bd),

    };
};        

// ---------------------------------------------------------------------------
class FileLock :
        public BinderService<FileLock>,
        public BnFileLockServer
{
    friend class BinderService<FileLock>;

    static char const* getServiceName() ANDROID_API { return "FileLock"; }
    FileLock() ANDROID_API;
    virtual ~FileLock();
    virtual void onFirstRef();
    
    // IFileLockServer interface
    virtual int lockFile(const String8& path);
    virtual int unLockFile(const String8& path);
    virtual int hideLockFile(void);
    virtual int unHideAnyFile(void);
    virtual int hideUnLockFile(void);
    virtual int checkFileState(const String8& path, int32_t& flag);

    //EventThread* mEventThread;
    sp<EventThread> mEventThread;
  
    // constants
    
    

//public:
};

// ---------------------------------------------------------------------------
}; // namespace android

#endif // ANDROID_FILE_LOCK_H

