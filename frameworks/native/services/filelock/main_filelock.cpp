#include <binder/BinderService.h>
#include "FileLock.h"

using namespace android;

int main(int /*argc*/, char** /*argv*/) {
    FileLock::publishAndJoinThreadPool();
    return 0;
}

