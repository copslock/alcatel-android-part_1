/* Copyright (C) 2016 Tcl Corporation Limited */
// [FEATURE]-ADD-BEGIN by TCTSH.(yanxi.liu), For TBR, Task-784014
#include <stdlib.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>
#include <time.h>
#include <sys/klog.h>
#include <errno.h>
#include <fcntl.h>
#include <dirent.h>

#include <utils/Log.h>
#include <private/android_filesystem_config.h>
#include <sys/system_properties.h>
#include <sys/statfs.h>
#include <sys/stat.h>

//inner macro
#undef LOG_TAG
#define LOG_TAG "Jrdrecord"
#define KERNEL_LOG_FILE "SYS_KERNEL_LOG"
#define MAX_LENGTH 2048
#define TEMP_CLEAN() do{memset(g_temporary,0x00,MAX_LENGTH);}while(0)
#define JRDRECORD_ROOT_DIR  "/sdcard/jrdrecord"
#define MAX_JRDRECORDS 10
#define JRDRECORD_TEMP_DIR "/data/jrdrecord_tmp" // MODIFIED by xtfu, 2016-06-15,BUG-2353042
#define JRDLOG_FREE_LIMIT  5242880L//5M left packed file
#define JRDLOG_FILE_FREE_LIMIT  5242880L//5M left unpacked file

#define typecheck(x,y) {    \
    typeof(x) __dummy1;     \
    typeof(y) __dummy2;     \
    (void)(&__dummy1 == &__dummy2); }

char g_temporary[MAX_LENGTH] = {0,};
char TARGET_DIR[MAX_LENGTH] = {0,};

static void execute_check(char* cmd) {
    int status = -1;
    if ((NULL == cmd) || (0 == strlen(cmd))) {
        ALOGE("the cmd is null");
    }
    status = system(cmd);
    if(0 != status) {
        ALOGE("execute cmd fail : %s",cmd);
        ALOGE("error reason : %s",strerror(errno));
    } else {
        //ALOGW("run  cmd successfully : %s",cmd);
    }
}

static unsigned long get_file_size(const char *path) {
    //ALOGW("getfielzize_THE path is  :%s",path);
    unsigned long filesize = -1;
    struct stat statbuff;
    if(stat(path, &statbuff) < 0) {
        return filesize;
    } else {
        filesize = statbuff.st_size;
    }
    return filesize;
}

static void limit_file_size(const char *path) {
    //ALOGW("limitfilesize_THE path is  :%s",path);
    long file_size = get_file_size(path);
    if(file_size > JRDLOG_FILE_FREE_LIMIT) {
        char tmp_filename[MAX_LENGTH];
        snprintf(tmp_filename,MAX_LENGTH,"rm -rf %s",path);
        execute_check(tmp_filename);
    }
}

static void record_kernel_log(char* dir) {
    /* Get size of kernel buffer */
    int size = klogctl(KLOG_SIZE_BUFFER, NULL, 0);
    char *buf = NULL;
    int retval = 0;
    int fd = -1;
    //ALOGW("------ KERNEL LOG (dmesg) ------\n");

    if(NULL == dir) {
        return;
    }

    if (size <= 0) {
        ALOGE("Unexpected klogctl return value: %d\n\n", size);
        return;
    }

    buf = (char *) malloc(size + 1);

    if (buf == NULL) {
        ALOGE("memory allocation failed\n\n");
        return;
    }

    retval = klogctl(KLOG_READ_ALL, buf, size);

    if (retval < 0) {
        ALOGE("klogctl failure\n\n");
        free(buf);
        return;
    }
    buf[retval] = '\0';

    TEMP_CLEAN();
    snprintf(g_temporary,MAX_LENGTH,"%s/SYS_KERNEL_LOG",dir);

    fd = open(g_temporary, O_WRONLY | O_APPEND | O_CREAT, 0666);

    if (fd < 0) {
        ALOGE("Unable to open stack trace file '%s': %s\n",
               g_temporary, strerror(errno));
    } else {
        ssize_t actual = write(fd, buf, retval);
        if (actual != (ssize_t) retval) {
            ALOGE("Failed to write kernel log to %s (%d of %zd): %s\n",
                   g_temporary, (int) actual, retval, strerror(errno));
        } else {
            //ALOGW("success to write kernel log '%s'\n", g_temporary);
        }
        close(fd);
    }

    TEMP_CLEAN();
    snprintf(g_temporary,MAX_LENGTH,"%s/SYS_KERNEL_LOG",dir);
    limit_file_size(g_temporary);

    free(buf);
    return;
}

static void usage() {
    fprintf(stderr, "usage: dumpstate [-b soundfile] [-e soundfile] [-o file [-d] [-p] [-z]] [-s] [-q]\n"
                    "  -o: write to file (instead of stdout)\n"
                    "  -d: append date to filename (requires -o)\n"
                    "  -z: gzip output (requires -o)\n"
                    "  -p: capture screenshot to filename.png (requires -o)\n"
                    "  -s: write output to control socket (for init)\n"
                    "  -b: play sound file instead of vibrate, at beginning of job\n"
                    "  -e: play sound file instead of vibrate, at end of job\n"
                    "  -q: disable vibrate\n"
                    "  -B: send broadcast when finished (requires -o and -p)\n"
    );
}

static char* get_uncompress_files() {
    char filepathlist[PATH_MAX*12] = {0};
    char tempfile[PATH_MAX] = {0};
    DIR *dir = opendir(TARGET_DIR);
    // remember length of the path
    int i = 0;
    struct dirent* de;

    if(NULL == dir) {
        ALOGE("open dir %s error reason : %s",TARGET_DIR,strerror(errno));
        return NULL;
    }

    while ((de = readdir(dir))) {
        sprintf(tempfile, "%s/%s ",TARGET_DIR,de->d_name);
        if (!strcmp(de->d_name, ".") || !strcmp(de->d_name, "..")
            || strlen(tempfile) + strlen(filepathlist) + 1 >= sizeof(filepathlist))
            continue;

        //ALOGW("file %d name = %s",i++,de->d_name);
//        sprintf(tempfile, "%s ",de->d_name);//mark for tar
        strcat(filepathlist,tempfile);
    }
    closedir(dir);
    //ALOGW("filelist=%s",filepathlist);
    return strdup(filepathlist);
}

static char* find_jrdrecord_name(){
    unsigned long mtime = ULONG_MAX;
    struct stat sb;
    typecheck(mtime, (unsigned long)sb.st_mtime);
    char path[128];
    int oldest = 0;

    for (int i = 0; i < MAX_JRDRECORDS; i++) {
        snprintf(path,sizeof(path),"%s/jrdrecord%02d.zip",JRDRECORD_ROOT_DIR,i);

        if (!stat(path, &sb)) {
            if (sb.st_mtime < mtime) {
                oldest = i;
                mtime = sb.st_mtime;
            }
            continue;
        }
        if (errno != ENOENT)
            continue;

        return strdup(path);
    }

    /* we didn't find an available file, so we clobber the oldest one */
    snprintf(path,sizeof(path),"%s/jrdrecord%02d.zip",JRDRECORD_ROOT_DIR,oldest);

    return strdup(path);
}

static void record_command_info(char* cmd,char* outFileName) {
    if(NULL == cmd || NULL == outFileName) {
        ALOGE("command or fileName is null");
        return;
    }
    TEMP_CLEAN();
    snprintf(g_temporary,MAX_LENGTH,"%s > %s/%s",cmd,TARGET_DIR,outFileName);
    execute_check(g_temporary);
    TEMP_CLEAN();
    snprintf(g_temporary,MAX_LENGTH,"%s/%s",TARGET_DIR,outFileName);
    limit_file_size(g_temporary);
}

static void write_common_file() {
    record_command_info("logcat -v threadtime -b all -d","ANDROID_MAIN_LOG");
    record_command_info("logcat -v threadtime -b events -d","SYS_ANDROID_EVENT_LOG");
    record_command_info("logcat -v threadtime -b system -d","SYS_ANDROID_SYSTEM_LOG");
    record_command_info("logcat -v threadtime -b radio -d","SYS_ANDROID_RADIO_LOG");
    record_command_info("dumpsys cpuinfo","SYS_CPU_INFO");
    record_command_info("dumpsys activity","DUMPSYS_ACTIVITY");
    record_command_info("dumpsys window lastanr","DUMPSYS_WINDOW");
    record_command_info("cat /proc/meminfo","SYS_MEMORY_INFO");
    record_command_info("cat /proc/buddyinfo","SYS_BUDDY_INFO");
    record_command_info("cat /proc/vmallocinfo","SYS_VMALLOC_INFO");
    record_command_info("getprop","SYS_PROPERTIES");
    record_kernel_log(TARGET_DIR);
}

void copy_proc_dir_by_pid(int pid,const char *outfile) {
    char path[PATH_MAX]={0,};
    sprintf(path, "/proc/%d/fd", pid);
    struct dirent* de;
    FILE* pOut;

    pOut = fopen(outfile,"a+w");
    if(NULL == pOut)
    {
        ALOGE(" open file %s error reason:%s",outfile,strerror(errno));
        return;
    }
    DIR *dir = opendir(path);
    if(NULL == dir) {
        ALOGE("open dir %s error reason : %s",path,strerror(errno));
        return;
    }

    while ((de = readdir(dir))) {
        if (!strcmp(de->d_name, ".") || !strcmp(de->d_name, ".."))
            continue;

        memset(path,0,PATH_MAX);
        sprintf(path, "%d %s\n",de->d_ino,de->d_name);
        fwrite(path,1,strlen(path),pOut);
    }
    closedir(dir);
    fclose(pOut);
}

void copy_proc_maps_by_pid(int pid,const char *outfile) {
    char path[PATH_MAX] = {0,};
    int  readnum = 0;
    sprintf(path, "/proc/%d/maps", pid);

    FILE* pOut;
    pOut = fopen(outfile,"a+w");
    if(NULL == pOut) {
        ALOGE(" open file %s error reason:%s",outfile,strerror(errno));
        return;
    }
    FILE* pIn = fopen(path,"r");
    if(NULL == pIn ) {
        ALOGE("open %s error reason : %s",path,strerror(errno));
        return;
    }

    while ((readnum=fread(path,1,PATH_MAX,pIn))>0) {
        fwrite(path,1,readnum,pOut);
    }
    fclose(pIn);
    fclose(pOut);
}

void write_special_file(int pid) {
    if(0 != pid) {
        char cmd[256] = {0,};
        //memset(cmd,0x00,sizeof(cmd));
        //snprintf(cmd, sizeof(cmd), "ls -l /proc/%d/fd > %s/PROCESS_FILE_STATE", TARGET_DIR);
        //execute_check(cmd);
        //copy_proc_dir_by_pid(pid,cmd);
        memset(cmd,0x00,sizeof(cmd));// /proc/pid/fd from debugd, TODO why?
        snprintf(cmd, sizeof(cmd), "mv /cache/PROCESS_FILE_STATE %s/PROCESS_FILE_STATE",TARGET_DIR);
        execute_check(cmd);
        TEMP_CLEAN();
        snprintf(g_temporary,MAX_LENGTH,"%s/PROCESS_FILE_STATE",TARGET_DIR);
        limit_file_size(g_temporary);

        //memset(cmd,0x00,sizeof(cmd));
        //snprintf(cmd, sizeof(cmd), "cat /proc/%d/maps > %s/PROCESS_MAPS", pid,TARGET_DIR);
        //execute_check(cmd);
        //copy_proc_maps_by_pid(pid,cmd);
        //memset(cmd,0x00,sizeof(cmd));
        //snprintf(cmd, sizeof(cmd), "mv /cache/PROCESS_MAPS %s/PROCESS_MAPS",TARGET_DIR);
        //execute_check(cmd);
        //TEMP_CLEAN();
        //snprintf(g_temporary,MAX_LENGTH,"%s/PROCESS_MAPS",TARGET_DIR);
        //limit_file_size(g_temporary);
     } else {
        char cmd[256] = {0,};
        memset(cmd,0x00,sizeof(cmd));
        snprintf(cmd, sizeof(cmd), "ls -l /proc/self/fd/ > %s/PROCESS_FILE_STATE",TARGET_DIR);
        execute_check(cmd);
        TEMP_CLEAN();
        snprintf(g_temporary,MAX_LENGTH,"%s/PROCESS_FILE_STATE",TARGET_DIR);
        limit_file_size(g_temporary);

        //memset(cmd,0x00,sizeof(cmd));
        //snprintf(cmd, sizeof(cmd), "cat /proc/self/maps > %s/PROCESS_MAPS",TARGET_DIR);
        //execute_check(cmd);
        //TEMP_CLEAN();
        //snprintf(g_temporary,MAX_LENGTH,"%s/PROCESS_MAPS",TARGET_DIR);
        //limit_file_size(g_temporary);
    }
}

int mtimesort(const struct dirent** a, const struct dirent** b) {
    char tmp[1024];
    struct stat s1, s2;
    snprintf(tmp, sizeof(tmp), "%s/%s", JRDRECORD_TEMP_DIR, (*a)->d_name);
    lstat(tmp, &s1);
    snprintf(tmp, sizeof(tmp), "%s/%s", JRDRECORD_TEMP_DIR, (*b)->d_name);
    lstat(tmp, &s2);
    return s2.st_mtime - s1.st_mtime;
}

int filefilter(const struct dirent *de) {
    return de->d_type == DT_REG;
}

int limit_files(void) {
    char tmp[1024];
    struct dirent **namelist;
    int n, err = 0;
    n = scandir(JRDRECORD_TEMP_DIR, &namelist, filefilter, mtimesort);
    if (n < 0) {
        perror("scandir");
        err = -1;
    } else {
        while (n-- > 10) {
            snprintf(tmp, sizeof(tmp), "%s/%s", JRDRECORD_TEMP_DIR, namelist[n]->d_name);
            if (unlink(tmp) < 0) {
                perror("unlink");
                err = -1;
            }
        }
    }
    return err;
}

//main logical
int main(int argc,char* argv[]) {
    char stamp[80] = {0,};
    char* fileName = NULL;
    time_t timep;
    int c;

    int pid = -1;
    char* file_name = NULL;

    time(&timep);
    strftime(stamp, sizeof(stamp), "%Y-%m-%d %H:%M:%S", localtime(&timep));

    while ((c = getopt(argc, argv, "p:f:")) != -1) {
        switch (c) {
            case 'p': pid = atoi(optarg); break;
            case 'f': file_name = optarg; break;
            case '?': printf("\n");
            case 'h':
                usage();
                exit(1);
        }
    }

    ALOGW("jrdrecord start time:%s, pid:%d, file_name:%s\n",stamp,pid,file_name);

    //clear the file which in JRDRECORD_TEMP_DIR
    //TEMP_CLEAN();
    //snprintf(g_temporary,MAX_LENGTH,"rm -rf %s/*",JRDRECORD_TEMP_DIR);
    //execute_check(g_temporary);

    snprintf(TARGET_DIR,512,"%s/jrdrecord_%d_",JRDRECORD_TEMP_DIR,pid);
    //create the dir for jrdrecord
    mkdir(JRDRECORD_ROOT_DIR, 0766);
    //mkdir(JRDRECORD_TEMP_DIR, 0766); // MODIFIED by xtfu, 2016-06-15,BUG-2353042
    if (!mkdir(TARGET_DIR, 0766)) {
         //ALOGW("mkdir(): success");
    } else if (errno == EEXIST) {
        //remove old file
        TEMP_CLEAN();
//        snprintf(g_temporary,MAX_LENGTH,"rm -rf %s/* %s/jrdrecord_%d_.tar.gz",TARGET_DIR,JRDRECORD_ROOT_DIR,pid);//mark for tar
        snprintf(g_temporary,MAX_LENGTH,"rm -rf %s/* %s/jrdrecord_%d_.zip",TARGET_DIR,JRDRECORD_ROOT_DIR,pid);
        execute_check(g_temporary);
    } else {
        ALOGE("mkdir error: %s\n",strerror(errno));
        return -1; // TODO
    }

    if(-1  != pid)
        write_special_file(pid);

    if(NULL != file_name) {
        TEMP_CLEAN();
        snprintf(g_temporary,MAX_LENGTH,"cp %s %s/",file_name,TARGET_DIR);
        execute_check(g_temporary);
    }

    //record the common file
    write_common_file();

    //gzip cache dir to fir
    TEMP_CLEAN();
    snprintf(g_temporary,MAX_LENGTH,"tar -czf %s.zip -C %s %s",TARGET_DIR,TARGET_DIR,get_uncompress_files());//use tar TCTNB.lijiang
    //snprintf(g_temporary,MAX_LENGTH,"minizip -o -a -6 -j %s %s",TARGET_DIR,get_uncompress_files());
    execute_check(g_temporary);

    //delete the large log
    TEMP_CLEAN();
//    snprintf(g_temporary,MAX_LENGTH,"%s.tar.gz",TARGET_DIR);//mark for tar
    snprintf(g_temporary,MAX_LENGTH,"%s.zip",TARGET_DIR);
    long jrdlog_size = get_file_size(g_temporary);
    if(jrdlog_size > JRDLOG_FREE_LIMIT) {
        ALOGE("jrdrecord(%s) too big, size: %ld\n",TARGET_DIR,jrdlog_size);
        TEMP_CLEAN();
        snprintf(g_temporary,MAX_LENGTH,"rm -rf %s*",TARGET_DIR);
        execute_check(g_temporary);
        return -1; //TODO
    } else {
        TEMP_CLEAN();
//        snprintf(g_temporary,MAX_LENGTH,"mv %s.tar.gz %s",TARGET_DIR,JRDRECORD_ROOT_DIR);//mark for tar
        snprintf(g_temporary,MAX_LENGTH,"mv %s.zip %s",TARGET_DIR,JRDRECORD_ROOT_DIR);
        execute_check(g_temporary);
        TEMP_CLEAN();
        snprintf(g_temporary,MAX_LENGTH,"rm -rf %s",TARGET_DIR);
        execute_check(g_temporary);
    }

/* jrdrecord will remove from return analysis, so no need backup it
    //obtain the file name of needing to create.
    fileName = find_jrdrecord_name();

    //gzip temporary dir to the file.
    TEMP_CLEAN();
    snprintf(g_temporary,MAX_LENGTH,"minizip -o -a -6 -j %s %s",fileName,get_uncompress_files());
    execute_check(g_temporary);

    //delte the /cache/jrdrecord/jrdrecord_pid_
    TEMP_CLEAN();
    snprintf(g_temporary,MAX_LENGTH,"rm -rf %s",TARGET_DIR);
    execute_check(g_temporary);

    //delete cache/jrdrecord/jrdlog.zip
    char buf[128];
    __system_property_get("debug.jrdlog", buf);
    if(strcmp(buf, "1")) {
        ALOGW("delete the /cache/jrdrecord/jrdlog.zip");
        TEMP_CLEAN();
        snprintf(g_temporary,MAX_LENGTH,"rm -rf %s.zip",TARGET_DIR);
        execute_check(g_temporary);
    } else {
        ALOGW("move the /cache/jrdrecord/jrdlog.zip");
        mkdir("/sdcard/jrdrecord", 0766);
        TEMP_CLEAN();
        snprintf(g_temporary,MAX_LENGTH,"mv %s.zip /sdcard/jrdrecord/",TARGET_DIR);
        execute_check(g_temporary);
        TEMP_CLEAN();
        snprintf(g_temporary, MAX_LENGTH, "/sdcard/jrdrecord/jrdrecord_%d_.zip", pid);
        chown(g_temporary, AID_SYSTEM, AID_SYSTEM);
        chmod(g_temporary, 0666);
        limit_files();
    }
    chown(fileName, AID_SYSTEM, AID_SYSTEM);
    chmod(fileName,0664);
*/

    //notify the debugservice update the log
    TEMP_CLEAN();
    snprintf(g_temporary,MAX_LENGTH,"am broadcast -a android.intent.action.TBR_UPLOAD -e filename %s",fileName);
    //ALOGW("jrdrecord execute command : %s\n",g_temporary);
    //execute_check(g_temporary);

    time(&timep);
    strftime(stamp, sizeof(stamp), "%Y-%m-%d %H:%M:%S", localtime(&timep));
    ALOGW("jrdrecord end time:%s\n",stamp);

    return 0;
}
// [FEATURE]-ADD-END by TCTSH.(yanxi.liu)
