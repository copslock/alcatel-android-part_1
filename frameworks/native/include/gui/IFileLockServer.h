/*willab*/
#ifndef ANDROID_GUI_IFILELOCKSERVER_H
#define ANDROID_GUI_IFILELOCKSERVER_H

#include <stdint.h>
#include <sys/types.h>

#include <utils/Errors.h>
#include <utils/RefBase.h>
#include <utils/String8.h>

#include <binder/IInterface.h>

namespace android {
// ----------------------------------------------------------------------------
class IFileLockServer : public IInterface
{
public:
    DECLARE_META_INTERFACE(FileLockServer);

    virtual int32_t lockFile(const String8& path) = 0;
    virtual int32_t unLockFile(const String8& path) = 0;
    virtual int32_t hideLockFile(void) = 0;
    virtual int32_t unHideAnyFile(void) = 0;
    virtual int32_t hideUnLockFile(void) = 0;
    virtual int32_t checkFileState(const String8& path, int32_t& flag) = 0;
};

// ----------------------------------------------------------------------------

class BnFileLockServer : public BnInterface<IFileLockServer>
{
public:
    virtual status_t    onTransact( uint32_t code,
                                    const Parcel& data,
                                    Parcel* reply,
                                    uint32_t flags = 0);
};

// ----------------------------------------------------------------------------
}; // namespace android

#endif // ANDROID_GUI_IFILELOCKSERVER_H

