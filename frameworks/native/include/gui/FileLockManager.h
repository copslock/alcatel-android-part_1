/*
willab
 */

#ifndef ANDROID_GUI_FILELOCK_MANAGER_H
#define ANDROID_GUI_FILELOCK_MANAGER_H

#include <map>

#include <stdint.h>
#include <sys/types.h>

#include <binder/IBinder.h>
#include <binder/IPCThreadState.h>
#include <binder/IServiceManager.h>

#include <utils/Errors.h>
#include <utils/RefBase.h>
#include <utils/Singleton.h>
#include <utils/Vector.h>
#include <utils/String8.h>


namespace android {
// ----------------------------------------------------------------------------
class IFileLockServer;
// ----------------------------------------------------------------------------

class FileLockManager 
{
public:
    static FileLockManager& getInstanceForFileLock();
    ~FileLockManager();

    static status_t checkFileLockService();
    
    int lockFile(const String8 path); 
    int unLockFile(const String8 path);
    int hideLockFile(void);
    int unHideAnyFile(void);
    int hideUnLockFile(void);
    int checkFileState(const String8 path, int32_t& flag);

private:
    // DeathRecipient interface
    void fileLockManagerDied();

    FileLockManager();
    status_t assertStateLocked() const;
    
private:
    static Mutex sLock;

    mutable Mutex mLock;
    mutable sp<IFileLockServer> mFileLockServer;
    static FileLockManager* mFileLockManager;
    mutable sp<IBinder::DeathRecipient> mDeathObserver;
};

// ----------------------------------------------------------------------------
}; // namespace android

#endif // ANDROID_GUI_FILELOCK_MANAGER_H

