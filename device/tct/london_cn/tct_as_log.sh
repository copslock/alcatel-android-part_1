#!/system/bin/sh
time=`date "+%Y_%m%d_%H%M%S"`;
crashName=`getprop debug.as.name`;
PERSISTDIR="/tctpersist/aslog"
LOGCATDIR="$PERSISTDIR/logcat"
DMESGDIR="$PERSISTDIR/dmesg"

if [ ! -e $LOGCATDIR ]; then
    mkdir -p $LOGCATDIR
else
    logcatCount=`ls  $LOGCATDIR|wc -l`;
    if [ $logcatCount -ge 5 ]; then
        fileArray=(`ls $LOGCATDIR`);
        echo ${fileArray[0]};
        rm $LOGCATDIR/${fileArray[0]};
    fi
fi
if [ "$crashName" == "audio" ]; then
    sleep 1
fi
logcat -b all -d -v threadtime -f $LOGCATDIR/$time"_"$crashName.log

if [ ! -e $DMESGDIR ]; then
    mkdir -p $DMESGDIR
else
    dmesgCount=`ls  $DMESGDIR/|wc -l`
    if [ $dmesgCount -ge 5 ]; then
        fileArray=(`ls $DMESGDIR`);
        echo ${fileArray[0]};
        rm $DMESGDIR/${fileArray[0]};
    fi
fi
dmesg > $DMESGDIR/$time.dmesg

#if [ ! -e $PERSISTDIR/dropbox ]; then
#    mkdir $PERSISTDIR/dropbox
#fi

#if dropbox exist, save latest one
if [ -e $PERSISTDIR/dropbox ]; then
    rm -r $PERSISTDIR/dropbox
fi

cp -r /data/system/dropbox $PERSISTDIR/dropbox
