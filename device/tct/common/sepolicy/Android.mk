# Copyright (C) 2016 Tcl Corporation Limited
BOARD_SEPOLICY_DIRS += \
    device/tct/common/sepolicy \
    device/tct/$(TARGET_PRODUCT)/sepolicy

ifeq ($(TCT_TARGET_DEBUG_SEPOLICY),true)
BOARD_SEPOLICY_DIRS += device/tct/common/sepolicy/debug
endif #TCT_TARGET_DEBUG_SEPOLICY=true
