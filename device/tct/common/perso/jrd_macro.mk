#############################################
# the main fold paths
#############################################

#indicate the main path for the build system of jrdcom
JRD_BUILD_PATH := ./device/tct

#indicate the main path for the build system of a certain project
JRD_BUILD_PATH_DEVICE := $(JRD_BUILD_PATH)/$(JRD_PROJECT_NAME)
JRD_BUILD_PATH_COMMON := $(JRD_BUILD_PATH)/common

#indicate the path for the jrd product
JRD_PRODUCT_OUT := ./out/target/product/$(TARGET_PRODUCT)

#indicate the custpack path
ifeq ($(TCT_DELETE_CUSTPACK),true)
JRD_OUT_CUSTPACK := $(JRD_PRODUCT_OUT)/system/custpack
else
JRD_OUT_CUSTPACK := $(JRD_PRODUCT_OUT)/custpack
endif

#indicate the jrd custom resource path in /out
JRD_CUSTOM_RES := ./out/target/perso/$(TARGET_PRODUCT)/jrdResAssetsCust

#indicate the fold of wimdata in the source code
JRD_WIMDATA := ./custo_wimdata_ng

#############################################
# the main files paths
#############################################

# the path of the system properties plf
# [Architecture] ArchiNo:013 Perso Mechanism Porting Begin
JRD_PROPERTIES_PLF := device/tct/common/perso/plf/sys_properties/isdm_sys_properties.plf
FINAL_PROPERTIES_PLF := out/target/product/$(TARGET_PRODUCT)/plf/isdm_sys_properties.plf
JRD_NV_CONTROL_PLF := device/tct/common/perso/plf/nv_control/isdm_nv_control.plf
# [Architecture] Perso Mechanism Porting End

# the path of the system properties output file
JRD_SYS_PROPERTIES_OUTPUT := $(JRD_CUSTOM_RES)/jrd_sys_properties.prop

# the path of the build properties output file
JRD_BUILD_PROPERTIES_OUTPUT := $(JRD_CUSTOM_RES)/jrd_build_properties.mk

############################################
# the jrd tools
############################################

#indicate the path of the jrd tools
JRD_TOOLS := ./device/tct/common/perso/tools

#indicate the arct
JRD_TOOLS_ARCT := $(JRD_TOOLS)/arct/prebuilt/arct

#[BUG-FIX]-ADD BY NJTS.xiaowei.yang-nb,Task-528186
BUILD_PLF_SSV := $(JRD_BUILD_PATH)/common/perso/jrd_build_plf_ssv.mk


