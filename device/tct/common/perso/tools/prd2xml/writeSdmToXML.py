#!/usr/bin/env python

"""
 Summary:  Script is defined to build the customized plf file to the target xml file.
 Created:  2016-4-8
 Author :  Jianyong.Lou
 Contact:  0574-2796-0882
 Email  :  jianyong.lou@tcl.com

 Usage  :  python ./device/tct/common/perso/tools/prd2xml/writeSdmToXML.py PLF_TARGET_XML PLF_SOURCE_FILE
             Defined to build the customized plf file to the target xml file.
"""

import os,sys
import xml.dom.minidom
import re,struct
import traceback

copyWrite = os.linesep+'''/**
* Copyright (c) 2009, The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/'''+os.linesep

# MODIFIED-BEGIN by Ding Yanghunag, 2016-11-01,BUG-3211564
class DuplicatedKeyError(Exception):
    pass

def typeChange(type,sdmid):
    stringList = ['StringWLen','Ucs2StringWLen','AsciiString','string']
    integerList = ['Sdword','Sword','sword','Sbyte','Dword','DWORD','dword','Word','WORD']
    byteList = ['Byte','BYTE']
    # MODIFIED-END by Ding Yanghunag,BUG-3211564
    boolList = ['Boolean','eBOOLEAN','BOOLEAN','eBoolean']
    if type.split(',')[0] in stringList:
        return 'string'
    elif type.split(',')[0] in integerList:
        return 'integer'
    elif type.split(',')[0] in boolList:
        return 'bool'
    # MODIFIED-BEGIN by Ding Yanghunag, 2016-11-01,BUG-3211564
    elif type.split(',')[0] in byteList:
        return 'byte'
        # MODIFIED-END by Ding Yanghunag,BUG-3211564
    else:
        print 'Sdm ' + sdmid + ' Cannot find this type----->',type.split(',')[0]
        raise TypeError

def grepUString(value,ctype):
    ustrList = re.findall("(?<=[(])[^()]+[^()]+(?=[)])",value)
    if len(ustrList) == 0:
        return '""'
    b=map((lambda x: int(x,16)),ustrList[0].split(','))
    c=struct.pack('%dB ' % len(b), *b)
    if 'ucs2stringwlen' in ctype.lower():
        d=struct.unpack('>%dH' % (len(b)>>1),c)
    elif 'stringwlen' in ctype.lower():
        d=struct.unpack('>%dB' % len(b),c)
    return '\"' + ''.join(map(unichr,d)) + '\"'

def getAllSdmInfor(plfFile):
    sdmInforList = {}
    typeList = []
    if not os.path.exists(plfFile):
        print plfFile+' NOT Exists+++'
        raise IOError
    path = os.path.join(plfFile)
    try:
        dom = xml.dom.minidom.parse(path)
        var = dom.getElementsByTagName("VAR")
    except :
        raise ValueError
    for v in var:
        sdmid = v.getElementsByTagName("SDMID")[0].firstChild.nodeValue
        node  = v.getElementsByTagName("VALUE")
        value = node[0].firstChild.nodeValue if node and node[0].firstChild else ' '
        node  = v.getElementsByTagName("DESC")
        desc  = node[0].firstChild.nodeValue if node and node[0].firstChild else ' '
        node  = v.getElementsByTagName("METATYPE")
        ctype = node[0].firstChild.nodeValue if node and node[0].firstChild else ' '
        if (('ucs2stringwlen' in ctype.lower())
            or ('stringwlen' in ctype.lower())) :
            value = grepUString(value,ctype.lower())
        ctype = typeChange(ctype,sdmid)

        if ctype == 'bool':
            if value.lower() == 'true':
                value = 'true'
            elif value.lower() == 'false':
                value = 'false'
            elif int(value,16) > 0:
                value = 'true'
            # MODIFIED-BEGIN by Ding Yanghunag, 2016-11-01,BUG-3211564
            else:
                value = 'false'
        elif ctype == 'byte':
            value=str(int(value, 16))
            ctype='integer'

        if sdmInforList.has_key(sdmid):
            print "Duplicated SDMID found: %s from %s" % (sdmid, plfFile)
            raise DuplicatedKeyError

        sdmInforList[sdmid] = dict(path=path, value=value, ctype=ctype, desc=desc)
        # MODIFIED-END by Ding Yanghunag,BUG-3211564
    dom.unlink()
    return sdmInforList

def wirteSDMToXML(doc,sdmInforList):
    cdesc = doc.createComment(copyWrite)
    doc.appendChild(cdesc)
    res = doc.createElement('resources')
    doc.appendChild(res)
    for key in sdmInforList:
        cdesc = doc.createComment(' '+ sdmInforList[key]['desc'] + ' ')
        res.appendChild(cdesc)
        ctype = doc.createElement(sdmInforList[key]['ctype'])
        res.appendChild(ctype)
        ctype.setAttribute("name" , key)
        cvalue = doc.createTextNode(sdmInforList[key]['value'])
        ctype.appendChild(cvalue)
        res.appendChild(ctype)
    doc.appendChild(res)


def main():
    if len(sys.argv) !=3:
        usage()
        # MODIFIED-BEGIN by Ding Yanghunag, 2016-11-01,BUG-3211564
        sys.exit(1)
    plfFile = sys.argv[2]
    reload(sys)
    sys.setdefaultencoding("utf-8")

    try:
        sdmInforList = getAllSdmInfor(plfFile)
        # MODIFIED-END by Ding Yanghunag,BUG-3211564
        doc = xml.dom.minidom.Document()
        wirteSDMToXML(doc,sdmInforList)

        f = file(sys.argv[1],'w')
        doc.writexml(writer=f,indent=os.linesep, addindent="    ", newl="", encoding="utf-8")
        f.close()
    except Exception, e:
        traceback.print_exc()
        print 'ERROR: Build the customized plf---->>>' + sys.argv[2] + ' to target xml file failed'
        if os.path.exists(sys.argv[1]):
            os.remove(sys.argv[1])
        sys.exit(1) # MODIFIED by Ding Yanghunag, 2016-11-01,BUG-3211564

def usage():
    print __doc__

if __name__ == '__main__':
    main()
