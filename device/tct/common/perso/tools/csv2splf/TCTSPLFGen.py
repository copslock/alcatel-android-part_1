#!/usr/bin/env python
#
# Copyright (C) 2012 TCL Communication Technology Holdings Limited.
#
#      Author: Erlei
#      Date  : 2015-10-19
#      E-Mail: erlei.ding@tcl.com
#
# This material is company confidential, cannot be reproduced in any form    
# without the written permission of TCL Communication Technology Holdings    
# Limited.

"""
Generate splf file from tctnb_splf.csv

Usage:  TCTSPLFGen.py -option tctnb_splf.csv output_path
        ex:python ./device/tct/common/perso/tools/csv2splf/TCTSPLFGen.py ./device/tct/common/perso/plf/tctnb_splf.csv ./device/tct

  -h  (--help)
      Display this usage message and exit.
"""
import csv
import os
import sys
import getopt
import string
import shutil


macroname="MacroName"
comment="Description"
product=""
product_list=[]

class Options(object): pass
OPTIONS = Options()
OPTIONS.csv = ''
OPTIONS.output = ''

def get_productlist():
    with open(OPTIONS.csv,'rb') as macro_file:
        reader = csv.reader(macro_file,delimiter=',')
        for row in reader:
            col=len(row)
            #print col
            if reader.line_num ==2:
                break
            for i in range(3,13):
                if row[i]=="DESC":
                    break
                product_list.append(row[i])
        macro_file.seek(0)
    print "generate splf files from csv file"
    for product in product_list:
        #print "begin to generate "+ product +".h"
        auto_generate(product)
           

def auto_generate(product):

    dirname = os.path.join(OPTIONS.output, product)
    #delete all the original splf folder device/tct/product_name/perso/plf
    if os.path.exists(dirname+"/perso/plf"):
        shutil.rmtree(dirname+"/perso/plf") 

    with open(OPTIONS.csv,'rb') as macro_def:
        for row in csv.DictReader(macro_def):
            data = (row["TARGET_FILE"],row["SDMID"],row[product],row["DESC"])
            splf_filename = dirname + '/perso/plf/' + data[0]
            posfix = splf_filename.find(".plf")
            if posfix != -1:
                print "Error, TARGET_FILE name "+ data[0]+ " in device/tct/common/perso/plf/tctnb_splf.csv, it's postfix is \"plf\", actually the postfix must be \"splf\""
                sys.exit(3)

            splf_dirname = os.path.dirname(splf_filename)
            if not os.path.exists(splf_filename) and data[2] != 'N' and data[2] != 'Y':
                if not os.path.exists(splf_dirname):
                    os.makedirs(splf_dirname)
                default_content = "<TABLE_VAR>\n</TABLE_VAR>"
                default_file =  open(splf_filename,"w")
                default_file.write(default_content)
                default_file.close()
                               
            if data[2] != 'N' and data[2] != 'Y':
                origin_file = open(splf_filename,"r")
                origin_content = origin_file.read()
                #print origin_content
                origin_file.close()
                contentadd = "\n<VAR>\n<SDMID>" + data[1] + "</SDMID>\n<VALUE>"+ data[2] + "</VALUE>\n<DESC>" + data[3] + "</DESC>\n</VAR>\n"
                pos = origin_content.find("</TABLE_VAR>")
                if pos != -1:
                    origin_content = origin_content[:pos] + contentadd +  origin_content[pos:]
                    #print origin_content
                    update_file = open(splf_filename,"w")
                    update_file.write(origin_content)
                    update_file.close()
                             
def Usage(docstring):
    print docstring.rstrip("\n")

def main(argv):
    try:
      opts, args = getopt.getopt(argv, "h", ["help"])
    except getopt.GetoptError, err:
      Usage(__doc__)
      print "**", str(err), "**"
      sys.exit(2)

    for o, a in opts:
        if o in ("-h", "--help"):
          Usage(__doc__)
          sys.exit()
        else:
          assert False, "unhandled option"
    if len(args) != 2:
      Usage(__doc__)
      sys.exit(1)

    OPTIONS.csv = args[0]
    OPTIONS.output = args[1]
    get_productlist()
if __name__=="__main__":
    main(sys.argv[1:])
