#!/usr/bin/env python
#
# This script is used to check the syntax of all plf files,
# and output a table list all SDM values.
# We have to make sure the input plf syntax should be correct, otherwise,
# the customization may not work.
#

import os,sys
import csv
import traceback
import xml.dom.minidom

from unicodeCSV import UnicodeWriter


def parsePLF(List, path = None):
    if not path:
        # for test purpose
        path = './rio6/perso/plf/frameworks/base/packages/SystemUI/isdm_SystemUI.splf'
    print path
    if not os.path.exists(path):
        print 'target file not exist. Exit now!'
        return
    # Ignore .plf file under common/perso/tools dir
    if path.find("common/perso/tools") > -1:
        return

    #parse xml file by minidom
    try:
        dom = xml.dom.minidom.parse(path)
        var = dom.getElementsByTagName("VAR")
    except :
        raise ValueError

    for v in var:
        sdmid = v.getElementsByTagName("SDMID")[0].firstChild.nodeValue
        #print sdmid
        node  = v.getElementsByTagName("METATYPE")
        meta  = node[0].firstChild.nodeValue if node and node[0].firstChild else ' '
        node  = v.getElementsByTagName("VALUE")
        value = node[0].firstChild.nodeValue if node and node[0].firstChild else ' '
        node  = v.getElementsByTagName("DESC")
        desc  = node[0].firstChild.nodeValue if node and node[0].firstChild else ' '
        node  = v.getElementsByTagName("C_NAME")
        cname = node[0].firstChild.nodeValue if node and node[0].firstChild else ' '
        node  = v.getElementsByTagName("C_TYPE")
        ctype = node[0].firstChild.nodeValue if node and node[0].firstChild else ' '
        node1 = v.getElementsByTagName("ARRAY")
        if node1 and node1[0].firstChild:
           #print "\nnode is %s" % node1
           array = node1[0].firstChild.nodeValue
        else:
           array = ' '

        if List.has_key(sdmid):
            print "\nDuplicate SDM ID detected --> ", sdmid, path, desc
            print "same id %s exist in file %s" % (sdmid, List[sdmid]['path'])
            raise ValueError
        List[sdmid] = dict(path=path, meta=meta, value=value, cname=cname, ctype=ctype, desc=desc)

    dom.unlink()
    #print '\n'.join(SDMList.keys())
    return


def parseTctFeature(path = None):
    """ parse tct_feature.global file and extract all feature item into a csv file"""
    if not path:
        # for test purpose
        path = "eos/tct_feature.global"
    print path
    if not os.path.exists(path):
        print 'target file not exist. Exit now!'
        return

    with open(path) as f:
        lines = f.readlines()

    key = None
    t = v = target = None
    FeatureList = {}

    for line in lines:
        line = line.strip()
        if not line or line.startswith('#'):
            continue
        if line.startswith('['):
            if key:
                FeatureList[key] = dict(type = t, value = v, target = target)
            key = line [1:-1].strip()
            t = v = target = None
        if key:
            if line.startswith('type') :
                t = line.split('=')[1].strip()
            elif line.startswith('value') :
                v = line.split('=')[1].strip()
            elif line.startswith('target') :
                target = line.split('=')[1].strip()

    # add the last item
    if key:
        FeatureList[key] = dict(type = t, value = v, target = target)

    #print '\n'.join(FeatureList.keys())

    return FeatureList


def scanCommonDir(path):
    path = os.path.join(path, "common")
    SDMList = {}
    if not os.path.exists(path):
        print path
        return
    scanDir(SDMList, path)

    return SDMList

def scanTargetDir(path):
    """ process all found target product folder"""

    targetSDM = {}
    print "scanTargetDir", path
    for f in os.listdir(path):
        if os.path.isdir(os.path.join(path, f)) and not f.startswith('.') and f != 'common':
            prod = {}
            scanDir(prod, os.path.join(path, f))
            targetSDM[f] = prod
    #print targetSDM
    return targetSDM

def scanDir(List, path):
    """ Scan all sub folder under path to found out all *.plf and *.global files"""
    print "\nstart working at %s\n" % path
    for root, dirs, files in os.walk(path):
        for f in files:
            if f.endswith(".plf") or f.endswith(".splf"):
                parsePLF(List, os.path.join(root, f))
            elif f.endswith(".global"):
                parseTctFeature(os.path.join(root, f))

def main(path,prodname):
    """Write out the csv file with unicode support"""
    try:
        SDMList = scanCommonDir(path)
        prodSDM = scanTargetDir(path+'/'+prodname)
        #print prodSDM
    except:
        traceback.print_exc()
        sys.exit(1) # MODIFIED by Ding Yanghunag, 2016-11-01,BUG-3211564

    ROWS = {}
    header = ['SDMID', 'Path', 'Description', 'CNAME', 'CTYPE', 'MetaData', 'Common Value' ]
    for prod in sorted(prodSDM.keys()):
        print prod
        header.append(prod)

    for d in prodSDM.values():
        for sdm, attrs in d.items():
            if not SDMList.has_key(sdm):
                SDMList[sdm] = attrs

    for sdm, attrs in SDMList.items():
        if attrs['path'].startswith('common'):
            commonValue = attrs['value']
        else:
            commonValue = ''
        row = [sdm, attrs['path'], attrs['desc'], attrs['cname'],attrs['ctype'], attrs['meta'], commonValue]
        for prod in sorted(prodSDM.keys()):
            l = prodSDM[prod]
            if l.has_key(sdm):
                row.append(l[sdm]['value'])
            else:
                row.append('')
        ROWS[sdm] = row

    # open csv file and write the header.
    writer = UnicodeWriter(open("all_sdm.csv", "wb"), delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
    writer.writerow(header)
    try:
        for row in ROWS.values():
            writer.writerow(row)
    except :
        print row
        print traceback.print_exc()
    return 1

if __name__ == '__main__':
    main(sys.argv[1],sys.argv[2])
