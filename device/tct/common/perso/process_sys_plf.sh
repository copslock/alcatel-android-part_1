#!/bin/sh
#
# Process isdm_sys_properties.plf and generate jrd_sys_property.mk and jrd_sys_property.prop
# The scripts checks the timestamp of the dest files if they already exist and generates them
# only if the src file is newer.
#
arct=$1
src=$2
dest_dir=$3
MAKE_PERSO_OR_NOT=$4
JRD_PROPERTIES_PLF=$5
JRD_PROPERTIES_PLF_DIR=$(dirname $5)
JRD_NV_CONTROL_PLF_DIR=$(dirname $6)
FINAL_PLF_OUT=$(dirname $2)

# INT-DYH: merge plf for isdm_sys_properties.plf and isdm_nv_controls.splf
if [ ! -d $FINAL_PLF_OUT ]; then
    mkdir -p $FINAL_PLF_OUT
fi
mergeplf_tool="development/tcttools/mergeplf/mergeplf"

dest_mk="$dest_dir/jrd_build_properties.mk"
dest_prop="$dest_dir/jrd_sys_properties.prop"

if [ ! -d $dest_dir ]; then
	echo "Creating dir $dest_dir..."
	mkdir -p $dest_dir
fi

if [ ! -f $dest_mk -o ! -f $dest_prop ]; then
    $mergeplf_tool $MAKE_PERSO_OR_NOT $JRD_PROPERTIES_PLF_DIR $FINAL_PLF_OUT $TARGET_PRODUCT
    $mergeplf_tool $MAKE_PERSO_OR_NOT $JRD_NV_CONTROL_PLF_DIR $FINAL_PLF_OUT $TARGET_PRODUCT
	$arct p $src $dest_mk $dest_prop
elif [ $(stat -c %Y $dest_mk) -lt $(stat -c %Y $JRD_PROPERTIES_PLF) -o  $(stat -c %Y $dest_prop) -lt $(stat -c %Y $JRD_PROPERTIES_PLF) ]; then
    $mergeplf_tool $MAKE_PERSO_OR_NOT $JRD_PROPERTIES_PLF_DIR $FINAL_PLF_OUT $TARGET_PRODUCT
    $mergeplf_tool $MAKE_PERSO_OR_NOT $JRD_NV_CONTROL_PLF_DIR $FINAL_PLF_OUT $TARGET_PRODUCT
	$arct p $src $dest_mk $dest_prop
else
	echo "Nothing to do"
fi
