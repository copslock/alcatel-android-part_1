
#build the customized user manuals
ifeq (,$(wildcard $(JRD_WIMDATA)/wcustores/UserManual/*))
$(warning there's no user manuals in $(JRD_WIMDATA)/wcustores/UserManual/)
else
UM_FLAG := $(JRD_CUSTOM_RES)/flag/um_flag
$(UM_FLAG) : $(JRD_WIMDATA)/wcustores/UserManual/*
	@echo now building the customized user manuals...
	$(hide) mkdir -p $(JRD_CUSTOM_RES)/flag
	$(hide) mkdir -p $(JRD_OUT_CUSTPACK)/JRD_custres/user_manual
	$(hide) cp $(JRD_WIMDATA)/wcustores/UserManual/* $(JRD_OUT_CUSTPACK)/JRD_custres/user_manual
	@echo "### this file is generated by jrd_build_extra_res.mk, used as a flag, do not modify it" > $(UM_FLAG)
	@echo building the customized user manuals done.
endif #(,$(wildcard $(JRD_WIMDATA)/wcustores/UserManual/*))

#build the customized apns,plmn-list
FILES_FLAG := $(JRD_CUSTOM_RES)/flag/files_flag
$(FILES_FLAG) : $(JRD_WIMDATA)/wcustores/apns-conf.xml $(JRD_WIMDATA)/wcustores/plmn-list.conf
	@echo now building the customized files...
	$(hide) mkdir -p $(JRD_CUSTOM_RES)/flag
	$(hide) mkdir -p $(JRD_OUT_CUSTPACK)
	$(hide) cp $(JRD_WIMDATA)/wcustores/apns-conf.xml $(TARGET_OUT)/etc
	$(hide) cp $(JRD_WIMDATA)/wcustores/plmn-list.conf $(JRD_OUT_CUSTPACK)
	@echo "### this file is generated by jrd_build_extra_res.mk, used as a flag, do not modify it" > $(FILES_FLAG)
	@echo building the customized files done.
