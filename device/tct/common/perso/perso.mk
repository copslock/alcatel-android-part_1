TCT_BUILD_PERSO := true

TCT_DELETE_CUSTPACK := true

ifeq ($(TCT_BUILD_PERSO),true)

#indicate the name of the jrd project
JRD_PROJECT_NAME := $(TARGET_PRODUCT)

#############################################
# the flags
#############################################

#indicate if use the custom resource or not
#JRD_USE_CUSTOM_RES := true

#indicate the command is or not "make perso"
ifeq ("$(filter perso,$(MAKECMDGOALS))","perso")
MAKE_PERSO_OR_NOT := 1
else
MAKE_PERSO_OR_NOT := 0
endif #($(MAKECMDGOAL), perso)

#############################################


#include the jrd macro
include ./device/tct/common/perso/jrd_macro.mk

#generate the jrd_sys_properties.prop & jrd_build_properties.mk
$(shell $(JRD_BUILD_PATH)/common/perso/process_sys_plf.sh \
	$(JRD_TOOLS_ARCT) $(FINAL_PROPERTIES_PLF) $(JRD_CUSTOM_RES) $(MAKE_PERSO_OR_NOT) $(JRD_PROPERTIES_PLF) \
	$(JRD_NV_CONTROL_PLF) 1>/dev/null )

# include the build properties makefile
include $(JRD_BUILD_PROPERTIES_OUTPUT)
ifeq ($(TARGET_BUILD_VARIANT),user)
ifneq ($(SMCN_ROM_CONTROL_FLAG),true)
PRODUCT_LOCALES := en_US
endif
endif

ifneq ($(SMCN_ROM_CONTROL_FLAG),true)
PRODUCT_PACKAGES += $(JRD_PRODUCT_PACKAGES)
#TCTNB-INT-DYH: To include all apk when build main system
PRODUCT_PACKAGES += $(JRD_DELETED_PACKAGES)
endif
endif
