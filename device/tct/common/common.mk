
ADDITIONAL_BUILD_PROPERTIES += ro.product.first_api_level=24

PRODUCT_PACKAGES += qcril.db

#[FEATURE]-Add-BEGIN by TCTNB.Miao, 2670234
ifeq ($(TCT_TARGET_PREBUILD_ADB_VENDOR_KEYS),true)
PRODUCT_COPY_FILES += device/tct/common/adbkey.pub:/system/adbkey.pub
endif #TCT_TARGET_PREBUILD_ADB_VENDOR_KEYS

PRODUCT_COPY_FILES += device/tct/common/5_audio_network.cfg:/system/etc/5_audio_network.cfg

#[FEATURE]-Add-BEGIN by TCTNB.huaidi.feng, 2754770
ifeq ($(TCT_TARGET_PERSIST_DLOAD),true)
PRODUCT_PROPERTY_OVERRIDES += \
    persist.sys.dload.enable = 1 \
    persist.sys.ssr.restart_level = ALL_DISABLE
else
PRODUCT_PROPERTY_OVERRIDES += \
    persist.sys.dload.enable = 0 \
    persist.sys.ssr.restart_level = ALL_ENABLE
endif
#[FEATURE]-Add-END TCTNB.huaidi.feng

#ruili.liu add for wlan test mode begin 2670034
HAVE_WIFI_TEST := true
ifneq ($(TARGET_BUILD_VARIANT),user)
PRODUCT_COPY_FILES += \
    device/tct/common/rootdir/etc/init.qcom.wifitest.sh:system/etc/init.qcom.wifitest.sh \
    device/tct/common/rootdir/etc/init.qcom.wifitesttxn.sh:system/etc/init.qcom.wifitesttxn.sh \
    device/tct/common/rootdir/etc/init.qcom.wifitestRxOn.sh:system/etc/init.qcom.wifitestRxOn.sh \
    device/tct/common/rootdir/etc/init.qcom.RxRconnect.sh:system/etc/init.qcom.RxRconnect.sh \
    device/tct/common/rootdir/etc/init.qcom.wifitesttxunmod.sh:system/etc/init.qcom.wifitesttxunmod.sh \
    device/tct/common/rootdir/etc/init.qcom.wifitesttxbg_fixed.sh:system/etc/init.qcom.wifitesttxbg_fixed.sh \
    device/tct/common/rootdir/etc/init.qcom.wifitestRxStop.sh:system/etc/init.qcom.wifitestRxStop.sh \
    device/tct/common/rootdir/etc/init.qcom.wifitestTxStop.sh:system/etc/init.qcom.wifitestTxStop.sh \
    device/tct/common/rootdir/etc/init.qcom.wifitestwg.sh:system/etc/init.qcom.wifitestwg.sh \
    development/apps/WLANTestMode/wl/wavegenerator.sh:data/wl/wavegenerator.sh \
    development/apps/WLANTestMode/wl/rx.sh:data/wl/rx.sh \
    development/apps/WLANTestMode/wl/stop.sh:data/wl/stop.sh \
    development/apps/WLANTestMode/wl/rxn.sh:data/wl/rxn.sh \
    development/apps/WLANTestMode/wl/txstop.sh:data/wl/txstop.sh \
    development/apps/WLANTestMode/wl/rxstop.sh:data/wl/rxstop.sh \
    development/apps/WLANTestMode/wl/txn.sh:data/wl/txn.sh \
    development/apps/WLANTestMode/wl/RxOn.sh:data/wl/RxOn.sh \
    development/apps/WLANTestMode/wl/RxReconnect.sh:data/wl/RxReconnect.sh \
    development/apps/WLANTestMode/wl/txunmod.sh:data/wl/txunmod.sh \
    development/apps/WLANTestMode/wl/txbg_fixed.sh:data/wl/txbg_fixed.sh \
    device/tct/common/rootdir/etc/iwlist:system/bin/iwlist \
    device/tct/common/rootdir/etc/iwconfig:system/bin/iwconfig
endif

PRODUCT_COPY_FILES += \
    device/tct/common/rootdir/etc/iwpriv:system/bin/iwpriv \
    device/tct/common/rootdir/etc/init.qcom.wifitesttx.sh:system/etc/init.qcom.wifitesttx.sh \
    device/tct/common/rootdir/etc/init.qcom.wifitestmaxpowertx.sh:system/etc/init.qcom.wifitestmaxpowertx.sh \
    development/apps/WLANTestMode/wl/txbg.sh:data/wl/txbg.sh
#ruili.liu add for wlan test mode end 2670034

#[FEATURE][DEV]Add-BEGIN by TCTNB hanling.zhang Nov 8th 2016, audiocapture dev
ifeq ($(TCT_FEATURE_AUDIOCAPTURE), true)
PRODUCT_COPY_FILES += \
    device/tct/common/nxp/libLifevibes_lvac.so:system/vendor/lib/libLifevibes_lvac.so \
    device/tct/common/nxp/AcControlParams_SPEAKER.txt:system/vendor/etc/nxp/AcControlParams_SPEAKER.txt \
    device/tct/common/nxp/LVACFS_Configuration.txt:system/vendor/etc/nxp/LVACFS_Configuration.txt
endif
#[FEATURE][DEV]Add-END by TCTNB hanling.zhang Nov 8th 2016, audiocapture dev

#$(call inherit-product, frameworks/base/data/fonts/fonts.mk)
#$(call inherit-product, frameworks/base/data/keyboards/keyboards.mk)
#[BUGFIX]added by Miao,add tct packages,bug 2642065
PRODUCT_PACKAGES += \
		smcd \
		tctd \
		tdc \
		powerup_reason \
                backup_sfs

#[FEATURE]-Add-BEGIN by TCTNB.Fuqiang.Song,2016.08.11 2706546
PRODUCT_PACKAGES += sar_daemon
#[FEATURE]-Add-END by TCTNB.Fuqiang.Song

#[SOLUTION]-Add-BEGIN by TCTNB.(Chuanjun Chen), 08/23/2016, SOLUTION- 2481157 And TASK-2781045
PRODUCT_PACKAGES += PhoneFeatures
#[SOLUTION]-Add-END by TCTNB.(Chuanjun Chen)

#[FEATURE]-Add-BEGIN by TCTNB.XLJ,2016.08.16 2736666
PRODUCT_PACKAGES += \
        updater \
        librecovery_updater_tct
TARGET_RECOVERY_UPDATER_LIBS += \
        librecovery_updater_tct
#[FEATURE]-Add-END by TCTNB.XLJ
#ADD-BEGIN by Dingyi  2016/08/17 SOLUTION 2521545
PRODUCT_PACKAGES += disableapplist.xml
#ADD-END by Dingyi  2016/08/17 SOLUTION 2521545

# MODIFIED-BEGIN by guangchao.su, 2016-09-09,BUG-2880282
#BEGIN-ran.zhou-20160219-ADD-FOR-GET-INCOMPLETE-STRING-FEATURE
PRODUCT_PACKAGES += modulename.txt
#END-ran.zhou-20160219-ADD-FOR-GET-INCOMPLETE-STRING-FEATURE
# MODIFIED-END by guangchao.su,BUG-2880282

#[FEATURE]-Add-BEGIN by TCTNB.Fuqiang.Song,2016.08.23,2778582.Refer to 528403.
PRODUCT_COPY_FILES += \
    device/tct/common/dpm/dpm.conf:system/etc/dpm/dpm.conf
#[FEATURE]-Add-END by TCTNB.Fuqiang.Song

#ADD-BEGIN by Yubin.Ying  2016/09/12 task 2702241
PRODUCT_PACKAGES += \
    TestMode
#ADD-END by Yubin.Ying  2016/09/01
# MODIFIED-BEGIN by jianguang.sun, 2016-09-13,BUG-2856089
#TASK-1595314-ran.zhou-added begin
PRODUCT_PACKAGES += modulename.txt
#TASK-1595314-ran.zhou-added  end
# MODIFIED-END by jianguang.sun,BUG-2856089

# widevine drm Level 3
PRODUCT_PROPERTY_OVERRIDES += drm.service.enabled=true
PRODUCT_PACKAGES += \
com.google.widevine.software.drm.xml \
com.google.widevine.software.drm \
libwvdrmengine

DEVICE_PACKAGE_OVERLAYS += vendor/cmx/product/overlay

PRODUCT_PACKAGES += devinfo.img

#ADD-BEGIN by liu.yang 2016/10/10 task 3292554	
ifneq ($(TCT_FEATURE_MP_BRANCH),true)
PRODUCT_PACKAGES += tct_stress
PRODUCT_PACKAGES += tct_thermal
PRODUCT_PACKAGES += tct_awake
PRODUCT_PACKAGES += tct_consumption
PRODUCT_PACKAGES += tct_watch
endif

#ADD-BEGIN by wu.yan 2016/11/11 task 3443853
PRODUCT_PACKAGES += tct-diag
