/* Copyright (C) 2016 Tcl Corporation Limited */
/*
 * Copyright (c) 2013, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include "cutils/properties.h"
#include "edify/expr.h"
#include "trace_partition.h"
#include "nv_tracability.h"

#ifdef __cplusplus
extern "C" {
#endif
#include "sparse_crc32.h"
#ifdef __cplusplus
}
#endif

//#if defined(FEATURE_TCTNB_FOTA) || defined(FEATURE_TCTNB_FULL_UPDATE)
#define TVN_REF_LEN (16)
#define TVN_ERROR_COOKIE "12345678"
#define BLOCKDEVICEPATH    "/dev/block/bootdevice/by-name/"
#define TRACEPATITION      BLOCKDEVICEPATH"traceability"
#define TVNOFFSET         5120

static int read_TVN_trace(State* state)
{
    int fd = -1;
    char tvn_msg[TVN_REF_LEN+1];

    fd = open(TRACEPATITION, O_RDONLY);
    if (fd < 0) {
        ErrorAbort(state, "open %s failed: %s\n", TRACEPATITION, strerror(errno));
        return -1;
    }
    lseek(fd, TVNOFFSET, SEEK_SET);
    read(fd, tvn_msg, TVN_REF_LEN);

    close(fd);
    return atoi(tvn_msg);
}

Value* TVNEqual(const char* name, State* state, int argc, Expr* argv[])
{
    int rc = -1;
    int script_TVN_num;
    int traceabillity_TVN_num;
    char value[PROPERTY_VALUE_MAX];

    if (argc != 1) {
         return ErrorAbort(state, "%s() expects 1 arg, got %d", name, argc);
    }

    char* script_TVN;
    char traceabillity_TVN[TVN_REF_LEN + 1];
    memset(traceabillity_TVN, 0, sizeof(traceabillity_TVN));
    if (ReadArgs(state, argv, 1, &script_TVN) < 0) return NULL;
    script_TVN_num = atoi(script_TVN);
    traceabillity_TVN_num = read_TVN_trace(state);
    free(script_TVN);

    if(traceabillity_TVN_num == atoi(TVN_ERROR_COOKIE)){
        printf("traceabillity_TVN == TVN_ERROR_COOKIE\n");
    }

    if (traceabillity_TVN_num != script_TVN_num) {
        rc = 0;
        printf( "%s(), script_TVN(%d) didn't match traceabillity_TVN:(%d)\n", name, script_TVN_num, traceabillity_TVN_num);
    }

    return StringValue(strdup(rc ? "" : "t"));
}


/* Reset the first 1536 bytes of modemst */
#define MODEMST_SIZE (2048)
int fota_reset_modemst(const char* partition) {
     FILE* f = NULL;
     f = fopen(partition,"wb");
     if (f == NULL) {
         printf("failed to open emmc partition \"%s\": %s\n",
                  partition, strerror(errno));
         return -1;
     }

     char* data =(char*) malloc(MODEMST_SIZE);
     memset(data, 0x0, MODEMST_SIZE);
     int bytes_write = fwrite(data, 1, MODEMST_SIZE, f);
     if (bytes_write < MODEMST_SIZE) {
        printf("short write of \"%s\" (%d bytes of %d)\n",
                 partition, bytes_write, MODEMST_SIZE);
        free(data);
        fclose(f);
        return -1;
     }
     free(data);
     fclose(f);

     return 0;
}

Value* ResetModemstFn(const char* name, State* state, int argc, Expr* argv[]) {
    char* result = NULL;
    char* success = "success";
    char* fail = "fail";
    if (argc != 1) {
        return ErrorAbort(state, "%s() expects 1 arg, got %d", name, argc);
    }
    char* partition;
    if (ReadArgs(state, argv, 1, &partition) < 0) {
        return NULL;
    }
    if (strlen(partition) == 0) {
        ErrorAbort(state, "partition argument to reset() can't be empty");
        goto done;
    }

    if (fota_reset_modemst(partition)) {
        result = fail;
        goto done;
    }
    result = success;

done:
    return StringValue(strdup(result));
}

#define CU_REF_LEN  (20)
#define CU_REF_OFFSET   (166)
#define TOTAL_SIZE  (sizeof(rtrf_header) + sizeof(tracability_region_struct_t))
static inline int is_visible(unsigned char c)
{
    //visible characters
    return (c <= 0x7E) && (c >= 0x21);
}

static int read_trace_partition(tracability_region_struct_t* tra_region) {
    rtrf_header *p_header = NULL;
    int fd = 0;
    char buf[2048];
    //the buf contains header and region.
    tracability_region_struct_t *p_re_mmc =
        (tracability_region_struct_t *)( buf + sizeof(rtrf_header));

    //read patti
    if ((fd = open(MMCBLKP, O_RDONLY)) < 0) {
        printf("open %s failed (errno: %d)\n", MMCBLKP, errno);
        return -1;
    }

    memset(buf, 0, sizeof(buf));
    if ((read(fd, buf, TOTAL_SIZE)) < 0) {
        printf("read %s failed (errno: %d)\n", MMCBLKP, errno);
        close(fd);
        return -1;
    }
    close(fd);

    memcpy(tra_region, p_re_mmc, sizeof(tracability_region_struct_t));
    return 0;
}

void get_cu_from_trace(char* sys_cu) {
   tracability_region_struct_t tra_region;
   if (read_trace_partition(&tra_region) < 0) {
        printf("read trace failed\n");
        snprintf(sys_cu, CU_REF_LEN+1, "Unknown");
   }

   unsigned char *p = tra_region.data + CU_REF_OFFSET;
   int i = 0;
   for(; i < CU_REF_LEN; ++i) {
     if(is_visible(p[i])) {
         sys_cu[i] = (char)p[i];
     } else {
         break;
     }
   }
   sys_cu[i] = '\0';
   printf("sys_cu =%s\n",sys_cu);

   if(sys_cu[0] == 0x00) {
       snprintf(sys_cu, CU_REF_LEN+1, "Unknown");
  }
}
//Adding a cu compare rule:
//DIABLO-[ABC] equals to DIABLO-A, DIABLO-B or DIABLO-C
static int compare(char* s1, char* s2) {
    // match substring before '['
    while(*s1 != '\0' && *s2 != '\0' && *s1 == *s2) {
        ++s1;
        ++s2;
    }

    // match char between '[' and ']'
    if (*s2 == '[') {
        while (*s2 != '\0' && *s2 != ']' && *s2++ != *s1);
        if (*s2 == '\0' || *s2 == ']') return 0;
        while (*s2 != '\0' && *s2 != ']') ++s2;
        if (*s2 != ']') return 0;
        ++s1;
        ++s2;
    }

    // match substring after ']'
    while (*s2 != '\0' && *s1 != '\0'  && *s2++ == *s1++);
    return (*s2 == '\0' && *s1 == '\0');
}

Value* CheckCuFn(const char* name, State* state, int argc, Expr* argv[]) {
   if (argc != 1) {
        return ErrorAbort(state, "%s() expects 1 arg, got %d", name, argc);
   }
   char* script_cu;
   char system_cu[CU_REF_LEN + 1];
   memset(system_cu, 0, sizeof(system_cu));
   if (ReadArgs(state, argv, 1, &script_cu) < 0) return NULL;

   get_cu_from_trace(system_cu);
   int i = 0, separator = 0;
   for (; system_cu[i] != '\0'; ++i) {
       if (system_cu[i] == '-' && ++separator == 2) {
           memset(system_cu + i, 0, sizeof(system_cu) - i);
           break;
       }
   }
   printf(" handset cu to be matched: %s \n", system_cu);

   if (!compare(system_cu, script_cu)) {
        return ErrorAbort(state, "%s() failed, %s didn't match modified system cu: %s", name, script_cu, system_cu);
   }

   return StringValue(strdup("Check cu success!"));
}

void Register_librecovery_updater_tct() {
    RegisterFunction("reset_modemst",ResetModemstFn);
    RegisterFunction("TVN_equal",TVNEqual);
    RegisterFunction("check_cu", CheckCuFn);
}
