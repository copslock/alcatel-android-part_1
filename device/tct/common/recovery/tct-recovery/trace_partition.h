/* Copyright (C) 2016 Tcl Corporation Limited */
/******************************************************************************/
/*                                                               Date:08/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  He Zhang                                                          */
/* E-Mail:  He.Zhang@tcl-mobile.com                                           */
/* Role  :  GLUE                                                              */
/* Reference documents :  05_[ADR-09-001]Framework Dev Specification.pdf      */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : vender/tct/source/system/tct_diag/trace_partition.h              */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 12/09/21| He.Zhang       | FR-334604          | Adjust to fit new role     */
/* 12/09/21| Guobing.Miao   | FR-334604          | Create for nv backup       */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/

#ifndef __TRACE_PARTITION_H__
#define __TRACE_PARTITION_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

typedef struct {
    uint32_t magic1;                 /* 4 bytes */
    uint32_t magic2;                 /* 4 bytes */
    uint32_t total_length_bytes;     /* 4 bytes */
    uint32_t checksum;               /* 4 bytes */
    uint32_t nb_missing_items;       /* 4 bytes */
    uint8_t status;                  /* 1 byte */
    unsigned char name[10];         /* 10 bytes */
    uint8_t pad[1];                  /* 1 byte */
    /* total header size             = 32 bytes */
} __attribute__((packed)) rtrf_header;





#define RETROFIT_MAGIC1 0xFACB10C1L
#define RETROFIT_MAGIC2 0xFACB10C2L

#define RETROFIT_DONE   0xa8

#define MMCBLKP     "/dev/block/bootdevice/by-name/traceability"
#define MMCBLKR     "/dev/block/bootdevice/by-name/recovery"
#define MMCBLKN     "/dev/oemver"
#define SYSUSB     "/sys/class/android_usb/android0/iSerial"
#define BTADDR		"/data/param/btaddr"
#define WIFIADDR	"/data/param/macaddr"
#define FNONVER	"/firmware/verinfo/non.ver"
#define FSYSVER	"/system/system.ver"
#define FCUSVER	"/custpack/custpack.ver"
#define FBOOVER "/boot.ver"

#define TRACABILITY_TOTAL_SIZE \
        (sizeof(rtrf_header) + sizeof(tracability_region_struct_t))

#define TRACEABILITY_MMI_OFFSET            (sizeof(rtrf_header) + 4)
#define TRACEABILITY_DIAG_OFFSET           (sizeof(rtrf_header) + 4 + 31)
#define TRACEABILITY_MMITEST_SIZE          (600)//545
#define TRACEABILITY_MMAP_SIZE             (1024 * 1024) //total size
#define TRACEABILITY_ROOTFLAG_OFFSET       (4096)

#ifdef __cplusplus
}
#endif

#endif /* __TRACE_PARTITION_H__ */
