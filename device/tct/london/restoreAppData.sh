#!/system/bin/sh

finishAll()
{
    sleep 0.5
    exec 7>&-
    exec 8>&-
    rm $1 $2 $3 $4
}

ListFile="/data/.backup.cfg"
PausePath="/data/.backupTrans.cfg"
CfgFile="/data/.backupCfg.cfg"
BusyBoxPath="/system/bin/busybox"
MkfifoTool="$BusyBoxPath mkfifo"

TmpFile="/data/tmpfile"
ProcessFile="/data/processfile"

$MkfifoTool $TmpFile
exec 7<>$TmpFile
chmod 777 $TmpFile

$MkfifoTool $ProcessFile
exec 8<>$ProcessFile
chmod 777 $ProcessFile

tmp=`cat $CfgFile`

dstPath=`echo $tmp |cut -d"," -f1|cut -d"=" -f2`
dstType=`echo $tmp |cut -d"," -f2|cut -d"=" -f2`

echo "=====StartRestore" >&7

if [ x"$dstPath" = "x" ]; then
    echo "=====Exit1" >&7
    finishAll  $ListFile $CfgFile $TmpFile $ProcessFile
    exit
fi

if [ x"$dstType" = "x" ]; then
    echo "=====Exit2" >&7
    finishAll  $ListFile $CfgFile $TmpFile $ProcessFile
    exit
fi

if [ "$dstType" != "2" ] && [ "$dstType" != "1" ]; then
    echo "=====Exit3" >&7
    finishAll  $ListFile $CfgFile $TmpFile $ProcessFile
    exit
fi

#Environment Setting
export PATH=/sbin:/system/sbin:/system/bin:/system/xbin:$PATH

while read line
do
    result="Success"
    echo "=====Start:$line" >&7

    if [ "$dstType" = "1" ]; then
        uid=`ls -l /data/data/|$BusyBoxPath grep "$line"|$BusyBoxPath grep -v "$line\."|$BusyBoxPath awk '{i=index($3,"_a");print 10000+substr($3,i+2);}'`
        echo "$uid" >&7

        secontext=`ls -Z /data/data/|$BusyBoxPath grep "$line"|$BusyBoxPath grep -v "$line\."|$BusyBoxPath awk '{print $1}'`
        echo "$secontext" >&7

        tarfile="$dstPath"/"$line"".tar"
        if [ -e "$tarfile" ]; then
            if [ x"$uid" != x ] && [ x"$secontext" != x ]; then
                #just untar backuped files
                tar xfv "$dstPath"/"$line"".tar" -C / 2>&7 >&7 && echo SuccessSuccess >&8 || echo FailFail >&8 &

                read a<&8
                echo "=====Msg, $a" >&7

                chmod 777 "/data/data/$line" -R
                $BusyBoxPath chown $uid:$uid /data/data/$line/ -R
                chcon -hRv "$secontext" "/data/data/$line"
                chown 1000:1000 "/data/data/$line/lib"

                chmod 777 "/data/user/0/$line" -R
                $BusyBoxPath chown $uid:$uid "/data/user/0/$line/" -R
                chcon -hRv "$secontext" "/data/user/0/$line"
                chown 1000:1000 "/data/user/0/$line/lib"
            else
                echo "=====Fail, uid null, no apk installed" >&7
                result="Fail"
            fi
        else
            echo "=====Fail, not exist $tarfile" >&7
            result="Fail"
        fi
    else
        mkdir -p $dstPath
        cp -R "$line" "$dstPath"/ 2>&7 >&7 && echo SuccessSuccess >&8 || echo FailFail >&8 &

        read a<&8
        if [ x"$a" = x"FailFail" ]; then
            result="Fail"
        fi
    fi

    if [ "$result" != "Success" ]; then
        echo "=====Fail,$line" >&7
    else
        echo "=====Success,$line" >&7
    fi

    echo "=====End:$line" >&7

    if [ -e "$PausePath" ]; then
        echo "=====Pause" >&7
        finishAll  $ListFile $CfgFile $TmpFile $ProcessFile
        exit
    fi
done < "$ListFile"

echo "=====End" >&7
finishAll  $ListFile $CfgFile $TmpFile $ProcessFile

exit
