# Copyright (C) 2016 Tcl Corporation Limited
#
LOCAL_PATH := $(call my-dir)

PRODUCT_PACKAGES := \
    ArkamysAudio

PRODUCT_COPY_FILES := \
    device/tct/simba6x/arkamys/optimspeaker/system/etc/arkamys/config_presets/0_ADVANCED:system/etc/arkamys/config_presets/0_ADVANCED \
    device/tct/simba6x/arkamys/optimspeaker/system/etc/arkamys/config_presets/1_SPK_music:system/etc/arkamys/config_presets/1_SPK_music \
    device/tct/simba6x/arkamys/optimspeaker/system/etc/arkamys/config_presets/2_SPK_movie:system/etc/arkamys/config_presets/2_SPK_movie \
    device/tct/simba6x/arkamys/optimspeaker/system/etc/arkamys/config_presets/3_SPK_game:system/etc/arkamys/config_presets/3_SPK_game \
    device/tct/simba6x/arkamys/optimspeaker/system/etc/arkamys/config_presets/7_TUNING:system/etc/arkamys/config_presets/7_TUNING \
    device/tct/simba6x/arkamys/optimspeaker/system/etc/arkamys/config_presets/8_BYPASS:system/etc/arkamys/config_presets/8_BYPASS \
    device/tct/simba6x/arkamys/optimspeaker/system/etc/arkamys/config_presets/9_BYPASS:system/etc/arkamys/config_presets/9_BYPASS \
    device/tct/simba6x/arkamys/optimspeaker/system/etc/arkamys/config_presets/10_HDP_music:system/etc/arkamys/config_presets/10_HDP_music \
    device/tct/simba6x/arkamys/optimspeaker/system/etc/arkamys/config_presets/12_HDP_movie:system/etc/arkamys/config_presets/12_HDP_movie \
    device/tct/simba6x/arkamys/optimspeaker/system/etc/arkamys/config_presets/14_HDP_game:system/etc/arkamys/config_presets/14_HDP_game \
    device/tct/simba6x/arkamys/optimspeaker/system/etc/arkamys/ArkamysAudioService.xml:system/etc/arkamys/ArkamysAudioService.xml \
    device/tct/simba6x/arkamys/optimspeaker/system/etc/arkamys/volume_table:system/etc/arkamys/volume_table \
    device/tct/simba6x/arkamys/optimspeaker/system/etc/audio_effects.conf:system/etc/audio_effects.conf \
    device/tct/simba6x/arkamys/optimspeaker/system/etc/permissions/com.arkamys.audiofx.xml:system/etc/permissions/com.arkamys.audiofx.xml \
    device/tct/simba6x/arkamys/optimspeaker/system/framework/com.arkamys.audiofx.jar:system/framework/com.arkamys.audiofx.jar \
    device/tct/simba6x/arkamys/optimspeaker/system/lib/soundfx/libarkamys.so:system/lib/soundfx/libarkamys.so \



