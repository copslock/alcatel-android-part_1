DEVICE_PACKAGE_OVERLAYS := device/tct/$(TARGET_PRODUCT)/overlay
DEVICE_PACKAGE_OVERLAYS += custo_wimdata_ng/wlanguage/overlay
TARGET_ENABLE_QC_AV_ENHANCEMENTS := true
BOARD_HAVE_QCOM_FM := true
TARGET_USES_NQ_NFC := false # bring-up hack
TARGET_USES_NXP_NFC := true
TARGET_USES_QTIC := false # bring-up hack
TARGET_KERNEL_VERSION := 3.18
BOARD_FRP_PARTITION_NAME :=frp

#QTIC flag
-include $(QCPATH)/common/config/qtic-config.mk

$(call inherit-product, vendor/google/products/gms.mk)

# Enable features in video HAL that can compile only on this platform
TARGET_USES_MEDIA_EXTENSIONS := true

# copy customized media_profiles and media_codecs xmls for msm8996
ifeq ($(TARGET_ENABLE_QC_AV_ENHANCEMENTS), true)
PRODUCT_COPY_FILES += device/tct/$(TARGET_PRODUCT)/media_profiles.xml:system/etc/media_profiles.xml \
                      device/tct/$(TARGET_PRODUCT)/media_codecs.xml:system/etc/media_codecs.xml \
                      device/tct/$(TARGET_PRODUCT)/media_codecs_performance.xml:system/etc/media_codecs_performance.xml
endif  #TARGET_ENABLE_QC_AV_ENHANCEMENTS

PRODUCT_COPY_FILES += device/tct/$(TARGET_PRODUCT)/whitelistedapps.xml:system/etc/whitelistedapps.xml

# Override heap growth limit due to high display density on device
PRODUCT_PROPERTY_OVERRIDES += \
    dalvik.vm.heapgrowthlimit=256m
$(call inherit-product, frameworks/native/build/phone-xhdpi-2048-dalvik-heap.mk)
$(call inherit-product, device/qcom/common/common64.mk)
$(call inherit-product, device/tct/common/common.mk)

#[FEATURE]-Add-BEGIN by TCTNB.93391,11/30/2015,ALM927723,USB Driver Auto Install
PRODUCT_COPY_FILES += \
    device/tct/common/USBDriver_Manual.iso:system/etc/USBDriver.iso
#[FEATURE]-Add-END by TCTNB.93391,11/30/2015,ALM927723,USB Driver Auto Install

#msm8996 platform WLAN Chipset
WLAN_CHIPSET := qca_cld

#[BUGFIX]-MOD-BEGIN by TCTNB.ZhangJie,09/01/2016,2829368,
#Add this to track connectivity version upgrade
PRODUCT_COPY_FILES += \
    device/tct/$(TARGET_PRODUCT)/bt_wlan_ver:system/etc/wifi/bt_wlan_ver
#[BUGFIX]-MOD-END by TCTNB.ZhangJie

#for tct after sale log task1167051
PRODUCT_COPY_FILES += \
    device/tct/simba6x/tct_as_log.sh:system/etc/tct_as_log.sh

PRODUCT_NAME := $(TARGET_PRODUCT)
PRODUCT_DEVICE := $(TARGET_PRODUCT)
PRODUCT_BRAND := Android
PRODUCT_MODEL := MSM8996 for arm64

include ./device/tct/common/perso/perso.mk

PRODUCT_BOOT_JARS += tcmiface
PRODUCT_BOOT_JARS += telephony-ext

PRODUCT_PACKAGES += telephony-ext

ifneq ($(strip $(QCPATH)),)
PRODUCT_BOOT_JARS += WfdCommon
#PRODUCT_BOOT_JARS += com.qti.dpmframework
#PRODUCT_BOOT_JARS += dpmapi
#PRODUCT_BOOT_JARS += com.qti.location.sdk
#Android oem shutdown hook
PRODUCT_BOOT_JARS += oem-services
endif

ifeq ($(strip $(BOARD_HAVE_QCOM_FM)),true)
PRODUCT_BOOT_JARS += qcom.fmradio
endif #BOARD_HAVE_QCOM_FM

PRODUCT_BOOT_JARS += qcmediaplayer

#Android EGL implementation
PRODUCT_PACKAGES += libGLES_android

# Audio configuration file
-include $(TOPDIR)hardware/qcom/audio/configs/msm8996/msm8996.mk

# WLAN driver configuration files
PRODUCT_COPY_FILES += \
    device/tct/$(TARGET_PRODUCT)/WCNSS_cfg.dat:system/etc/firmware/wlan/qca_cld/WCNSS_cfg.dat \
    device/tct/$(TARGET_PRODUCT)/WCNSS_qcom_cfg.ini:system/etc/wifi/WCNSS_qcom_cfg.ini

# MIDI feature
PRODUCT_COPY_FILES += \
    frameworks/native/data/etc/android.software.midi.xml:system/etc/permissions/android.software.midi.xml

PRODUCT_PACKAGES += \
    wpa_supplicant_overlay.conf \
    p2p_supplicant_overlay.conf

ifneq ($(WLAN_CHIPSET),)
PRODUCT_PACKAGES += $(WLAN_CHIPSET)_wlan.ko
endif

#[SOLUTION]-Add-BEGIN by TCTNB.wen.zhuang, 08/09/2016, SOLUTION-2559001
PRODUCT_PACKAGES += TctSaleMode
#[SOLUTION]-Add-END by TCTNB.wen.zhuang

PRODUCT_PACKAGES += OTAProvisioningClient

#ANT+ stack
PRODUCT_PACKAGES += \
    AntHalService \
    libantradio \
    antradio_app \
    libvolumelistener \
    secsvr


# Sensor HAL conf file
PRODUCT_COPY_FILES += \
    device/tct/$(TARGET_PRODUCT)/sensors/hals.conf:system/etc/sensors/hals.conf

# Sensor features
PRODUCT_COPY_FILES += \
    frameworks/native/data/etc/android.hardware.sensor.accelerometer.xml:system/etc/permissions/android.hardware.sensor.accelerometer.xml \
    frameworks/native/data/etc/android.hardware.sensor.compass.xml:system/etc/permissions/android.hardware.sensor.compass.xml \
    frameworks/native/data/etc/android.hardware.sensor.gyroscope.xml:system/etc/permissions/android.hardware.sensor.gyroscope.xml \
    frameworks/native/data/etc/android.hardware.sensor.light.xml:system/etc/permissions/android.hardware.sensor.light.xml \
    frameworks/native/data/etc/android.hardware.sensor.proximity.xml:system/etc/permissions/android.hardware.sensor.proximity.xml \
    frameworks/native/data/etc/android.hardware.sensor.stepcounter.xml:system/etc/permissions/android.hardware.sensor.stepcounter.xml \
    frameworks/native/data/etc/android.hardware.sensor.stepdetector.xml:system/etc/permissions/android.hardware.sensor.stepdetector.xml \

# dm-verity configuration
PRODUCT_SUPPORTS_VERITY := true
PRODUCT_SYSTEM_VERITY_PARTITION := /dev/block/bootdevice/by-name/system
$(call inherit-product, build/target/product/verity.mk)

PRODUCT_COPY_FILES += \
    frameworks/native/data/etc/android.software.verified_boot.xml:system/etc/permissions/android.software.verified_boot.xml

#FEATURE_OPENGLES_EXTENSION_PACK support string config file
PRODUCT_COPY_FILES += \
    frameworks/native/data/etc/android.hardware.opengles.aep.xml:system/etc/permissions/android.hardware.opengles.aep.xml

# MSM IRQ Balancer configuration file
PRODUCT_COPY_FILES += \
    device/tct/$(TARGET_PRODUCT)/msm_irqbalance.conf:system/vendor/etc/msm_irqbalance.conf

#TCTNB.CY - nxp smart pa speaker develop
PRODUCT_COPY_FILES += \
    device/tct/$(TARGET_PRODUCT)/nxp/tfa98xx.cnt:system/etc/firmware/tfa98xx.cnt
#end TCTNB.CY
#[BUGFIX]-Add-BEGIN by TCTNB.FR-526559 YQJ add for tp firmware 2015/11/02
PRODUCT_COPY_FILES += \
    device/tct/$(TARGET_PRODUCT)/tp_firmware/st_fts.bin:persist/st_fts.bin
PRODUCT_COPY_FILES += \
    device/tct/$(TARGET_PRODUCT)/tp_firmware/st_fts_wqhd.bin:persist/st_fts_wqhd.bin
#[BUGFIX]-Add-END by TCTNB.YQJ


#[FEATURE]-Add-BEGIN by TCTNB.Bo.Yu,2016/10/27, task 3234021
$(call inherit-product, device/tct/simba6x/arkamys/optimspeaker/device-partial.mk)
#[FEATURE]-Add-END by TCTNB.Bo.Yu,2016/10/27, task 3234021

#[BUGFIX]-Add-BEGIN by Dandan.Fang, 2016/08/15 ,for Task2657073
PRODUCT_COPY_FILES += \
    device/tct/common/region_ver:system/etc/region_ver
#[BUGFIX]-Add-END by Dandan.Fang

#[FEATURE]-Add-BEGIN by TCTNB.LCY, task-3065919, porting from task-707162, 2016/10/13, add AD calib file
PRODUCT_COPY_FILES += \
    device/tct/$(TARGET_PRODUCT)/AD/calib.cfg:system/etc/calib.cfg
#[FEATURE]-Add-END by TCTNB.LCY, 2016/10/13
ifeq ($(strip $(TARGET_USES_NQ_NFC)),true)
PRODUCT_PACKAGES += \
    NQNfcNci \
    libnqnfc-nci \
    libnqnfc_nci_jni \
    nfc_nci.nqx.default \
    libp61-jcop-kit \
    com.nxp.nfc.nq \
    com.nxp.nfc.nq.xml \
    libpn547_fw.so \
    libpn548ad_fw.so \
    libnfc-brcm.conf \
    libnfc-nxp.conf \
    nqnfcee_access.xml \
    nqnfcse_access.xml \
    Tag \
    com.android.nfc_extras \
    libQPayJNI \
    com.android.qti.qpay \
    com.android.qti.qpay.xml \
    SmartcardService \
    org.simalliance.openmobileapi \
    org.simalliance.openmobileapi.xml

PRODUCT_COPY_FILES += \
    packages/apps/Nfc/migrate_nfc.txt:system/etc/updatecmds/migrate_nfc.txt \
    frameworks/native/data/etc/com.nxp.mifare.xml:system/etc/permissions/com.nxp.mifare.xml \
    frameworks/native/data/etc/com.android.nfc_extras.xml:system/etc/permissions/com.android.nfc_extras.xml \
    frameworks/native/data/etc/android.hardware.nfc.xml:system/etc/permissions/android.hardware.nfc.xml \
    frameworks/native/data/etc/android.hardware.nfc.hce.xml:system/etc/permissions/android.hardware.nfc.hce.xml
# SmartcardService, SIM1,SIM2,eSE1 not including eSE2,SD1 as default
ADDITIONAL_BUILD_PROPERTIES += persist.nfc.smartcard.config=SIM1,SIM2,eSE1
endif # TARGET_USES_NQ_NFC

ifeq ($(TARGET_USES_NXP_NFC),true)
PRODUCT_PACKAGES += \
    nfc.msm8952 \
    NfcNci \
    libnfc-nci \
    libnfc_nci_jni \
    Tag \
    nfc_nci.pn54x.default \
    com.android.nfc_extras \
    com.gsma.services.nfc \
    com.gsma.services.nfc.xml \
    libnfctest

# NFCEE access control config
ifeq ($(TARGET_BUILD_VARIANT),user)
     NFCEE_ACCESS_PATH := device/tct/$(TARGET_PRODUCT)/nfc/nfcee_access.xml
else
     NFCEE_ACCESS_PATH := device/tct/$(TARGET_PRODUCT)/nfc/nfcee_access_debug.xml
endif

# MODIFIED-BEGIN by BinhongWang, 2016-10-20,BUG-3135332
PRODUCT_COPY_FILES += \
    device/tct/$(TARGET_PRODUCT)/qdcm_calib_data_Dual_s6e3ha3_amoled_cmd_mode_dsi_panel_without_DSC_powered_by_pmi8994.xml:system/etc/qdcm_calib_data_Dual_s6e3ha3_amoled_cmd_mode_dsi_panel_without_DSC_powered_by_pmi8994.xml \
    device/tct/$(TARGET_PRODUCT)/qdcm_calib_data_S6e3fa3_amoled_cmd_mode_dsi_panel_without_DSC_powered_by_pmi8952.xml:system/etc/qdcm_calib_data_S6e3fa3_amoled_cmd_mode_dsi_panel_without_DSC_powered_by_pmi8952.xml \
# MODIFIED-END   by BinhongWang, 2016-10-20,BUG-3135332

PRODUCT_COPY_FILES += \
    packages/apps/Nfc/migrate_nfc.txt:system/etc/updatecmds/migrate_nfc.txt \
    $(NFCEE_ACCESS_PATH):system/etc/nfcee_access.xml \
    frameworks/native/data/etc/com.nxp.mifare.xml:system/etc/permissions/com.nxp.mifare.xml \
    frameworks/native/data/etc/com.android.nfc_extras.xml:system/etc/permissions/com.android.nfc_extras.xml \
    frameworks/native/data/etc/android.hardware.nfc.xml:system/etc/permissions/android.hardware.nfc.xml \
    frameworks/native/data/etc/android.hardware.nfc.hce.xml:system/etc/permissions/android.hardware.nfc.hce.xml \
    frameworks/native/data/etc/android.hardware.nfc.hcef.xml:system/etc/permissions/android.hardware.nfc.hcef.xml \
    device/tct/$(TARGET_PRODUCT)/nfc/libnfc-brcm.conf:system/etc/libnfc-brcm.conf \
    external/libnfc-nci/halimpl/pn54x/libpn548ad_fw.so:system/vendor/firmware/libpn548ad_fw.so \
    device/tct/$(TARGET_PRODUCT)/nfc/libnfc-nxp.conf:system/etc/libnfc-nxp.conf
endif # TARGET_USES_NXP_NFC

PRODUCT_PROPERTY_OVERRIDES += \
    camera.disable_zsl_mode=1

# List of AAPT configurations
PRODUCT_AAPT_CONFIG += xlarge large
PRODUCT_PACKAGES += FingerprintProvider
#[FEATURE]-Add-BEGIN by TCTNB.XQJ,2016/01/08, task 1019320
PRODUCT_PACKAGES += \
    fingerprint.default \
    libvalAuth \
    libvcsfp \
    libvfmClient \
    libvfmtztransport

PRODUCT_PACKAGES += \
   fingerprintd  \
   synaFpHalTest \
   vfmService
#[FEATURE]-Add-END by TCLNB.XQJ,2016/01/08.
#[SOLUTION]-Add-BEGIN by TCTNB.wen.zhuang, 08/09/2016, SOLUTION-2559001
PRODUCT_PACKAGES += boot_time_check
#[SOLUTION]-Add-END by TCTNB.wen.zhuang

#[FEATURE]-Add-BEGIN by TCTNB.dong.Jiang,2016/08/17, Task-2655792, [Telecom][Debug]Record Network related abnormal info
#Porting from Task-1647082
PRODUCT_PACKAGES += TelecomRecord
#[FEATURE]-Add-END by TCTNB.Dong.Jiang

#[BUGFIX]-Add-BEGIN by TCTNB.yongzhen.wang,11/03/2016,Defect-3291790,
#[Telecom][Internal]Remove redundant logs information
PRODUCT_PACKAGES += TctLogFilter
#[BUGFIX]-Add-END by TCTNB.yongzhen.wang

# Task-2753780, exfat file-system
PRODUCT_PACKAGES += mkfs.exfat fsck.exfat

#Task-2778076, thermal in power off charger
PRODUCT_PACKAGES += charger.fstab.qcom
#Task-3467786, disable FDE for mmi build
PRODUCT_PACKAGES += mini.fstab.qcom

#for android_filesystem_config.h
PRODUCT_PACKAGES += \
    fs_config_files

# Enable logdumpd service only for non-perf bootimage
ifeq ($(findstring perf,$(KERNEL_DEFCONFIG)),)
    ifeq ($(TARGET_BUILD_VARIANT),user)
        PRODUCT_DEFAULT_PROPERTY_OVERRIDES+= \
            ro.logdumpd.enabled=0
    else
        PRODUCT_DEFAULT_PROPERTY_OVERRIDES+= \
            ro.logdumpd.enabled=1
    endif
else
    PRODUCT_DEFAULT_PROPERTY_OVERRIDES+= \
        ro.logdumpd.enabled=0
endif
#[FEATURE]-Add-BEGIN by TCTNB.Yang.Hu,2015/9/30, FR-572426 TCT DRM solution
PRODUCT_PACKAGES += \
    libcombinedengine \
    libcombined-common \
    libcombined-converter \
    libcombined-decoder \
    libdrmdecoder \
    libdrmparserdm \
    libdrmrightmanager \
    libdrmxmlparser \
    libseparate-common \
    libseparate-decoder \
    libseparate-parser \
    libseparateengine
#[FEATURE]-Add-END by TCTNB.Yang.Hu,2015/9/30, TCT DRM solution

#[SOLUTION]-Add-BEGIN by TCTNB.Zhang Jinbo, 08/19/2016, TASK-2776549
ifeq ($(TCT_TARGET_CONNECTIVITY_VERBOSE_LOG_ENABLE),true)
PRODUCT_PACKAGES += tcpdump
PRODUCT_PACKAGES += ptt_socket_app
PRODUCT_PACKAGES += WifiLogger_app
endif
#[SOLUTION]-Add-BEGIN by TCTNB.Zhang Jinbo, 08/19/2016, TASK-2776549

#[SOLUTION]-Add-BEGIN by TCTNB(Guoqiang.Qiu),2016-8-26, Solution-2699694
PRODUCT_PACKAGES += MagicLock \
    VLife \
    libvlife_media \
    libvlife_openglutil \
    libvlife_render
#[SOLUTION]-Add-END by TCTNB(Guoqiang.Qiu)

# [FEATURE]-Add-BEGIN by TCTNB.yandong.sun, 2016/08/18 2759149, add atfwd
PRODUCT_PACKAGES += atfwd
# [FEATURE]-Add-END by TCTNB.yandong.sun

#BUGFIX]-Add-BEGIN by TCTNB.qili.zhang,30/08/2016,Task2813004 portingTask 1177186
#SMS and data auto-registeration features for China Telecom
PRODUCT_PACKAGES +=\
      TctAutoRegister\
      TctPsAutoRegister
#[BUGFIX]-Add-END by TCTNB.qili.zhang

#Task-2706622, build tools for dump to file feature
PRODUCT_PACKAGES += preprd unpackrd

#[FEATURE]-Add-BEGIN by TCTNB.caixia.chen,09/19/2016,task 2854067
PRODUCT_PACKAGES += PrivacyMode
PRODUCT_PACKAGES += filelock
#[FEATURE]-Add-END by TCTNB.caixia.chen

#[FEATURE]-Add-BEGIN by TCTNB.lijiang,For TBR
ifeq ($(FEATURE_TCL_BUG_RECORD),true)
PRODUCT_PACKAGES += \
    jrdrecord
endif
#[FEATURE]-Add-END by TCTNB.lijiang

#[FEATURE]-Add-BEGIN by TCTNB.lijiang,2016/09/27, task 2998964
PRODUCT_PACKAGES += ADruntime
#[FEATURE]-Add-END by TCLNB.lijiang,2016/09/27.

#[FEATURE]-Add-BEGIN by TCTNB.wen.zhuang,2016/10/13, task 3066103
PRODUCT_PACKAGES += ScreenshotEdit
#[FEATURE]-Add-END by TCLNB.wen.zhuang,2016/10/13.

#[FEATURE]-HDCP-add by minjie.cai
PRODUCT_COPY_FILES += \
vendor/qcom/proprietary/wfd-noship/mm/hdcp/DxHDCP.cfg:system/etc/DxHDCP.cfg

#GAPP list
PRODUCT_PACKAGES += \
        Mms \
        Contacts \
        Compass \
        Dialer \
        Elabel \
        TctCalculator \
        SalesTracker \
        OneTouchFeedback \
        Gallery \
        Tethering \
        TctWeather \
        Launcher \
        TctMiddleMan \
        Diagnostics \
        ScreenRecorder \
        TctEmail \
        TctExchange \
        Music \
        JrdFota \
        PhoneGuard \
        TctCamera \
        Robust \
        SimpleLauncher \
        ScreenRecorder \
        TctCellBroadcastReceiver \
        TctFileManager \
        TctSetupWizard \
        TurboBrowser \
        SetupWizardRes \
        FileManagerRes \
        MusicRes \
        EmailRes \
        TctSoundRecorder \
        SoundRecorderRes \
        DiagnosticsRes \
        CalculatorRes \
        DialerRes \
        GalleryRes \
        MmsRes \
        WeatherRes \
        CompassRes \
        ContactsRes \
        ElabelRes \
        LauncherRes \
        FotaRes \
        OneTouchFeedbackRes \
        TetheringRes \
        CellBroadcastReceiverRes \
        BrowserRes \
        CameraLRes \
        SimpleLauncherRes \
        PhoneGuardRes

PRODUCT_PACKAGES += \
      MMITest \
      TestMode

#[FEATURE]-Add-BEGIN by TCTNB.Shishun.Liu,2016/10/20, task-3067799
PRODUCT_COPY_FILES += \
    device/tct/$(TARGET_PRODUCT)/ftm_test_config_simba6x:system/etc/ftm_test_config_simba6x
#[FEATURE]-Add-END by TCTNB.Shishun.Liu,2016/10/20, task-3067799

#[BUGFIX]-Mod-BEGIN by TSNJ.liang.xiao,10/31/2016,PR-3232354 begin
PRODUCT_PACKAGES += ChromeCustomizations
#[BUGFIX]-Mod-BEGIN by TSNJ.liang.xiao,10/31/2016,PR-3232354  end
