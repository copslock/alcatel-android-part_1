/*
 * Copyright 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ANDROID_DEVICE_CAMERA_FAKE_CAMERA_DELEGATE_H
#define ANDROID_DEVICE_CAMERA_FAKE_CAMERA_DELEGATE_H

#include "CameraDeviceDelegate.h"

namespace android {

// Fake camera device that outputs fake video pattern.
class FakeCameraDelegate : public CameraDeviceDelegate {
public:
    FakeCameraDelegate();
    ~FakeCameraDelegate() override;

    // CameraDeviceDelegate overrides:
    int Connect(const std::string& device_path) override;
    void Disconnect() override;
    int StreamOn(int width, int height, uint32_t fourcc, float frame_rate,
                 std::vector<int>* fds, uint32_t* buffer_size) override;
    int StreamOff() override;
    int GetNextFrameBuffer(uint32_t* buffer_id, uint32_t* data_size) override;
    int ReuseFrameBuffer(uint32_t buffer_id) override;
    const SupportedFormats GetDeviceSupportedFormats(const std::string& device_path) override;
    const DeviceInfos GetCameraDeviceInfos() override;

private:
    DISALLOW_COPY_AND_ASSIGN(FakeCameraDelegate);
};

}  // namespace android

#endif
