LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := camera.v4l2
LOCAL_MODULE_RELATIVE_PATH := hw

LOCAL_CFLAGS += -fno-short-enums

# libyuv_static depends on libjpeg.
LOCAL_SHARED_LIBRARIES := \
    libcamera_client \
    libcamera_metadata \
    libcommon_exifutils \
    libcutils \
    libexif \
    libjpeg \
    liblog \
    libui \
    libutils

LOCAL_STATIC_LIBRARIES := \
    libyuv_static

LOCAL_C_INCLUDES += \
    $(call include-path-for, camera)

LOCAL_SRC_FILES := \
    CallbackDelegate.cpp \
    CameraClient.cpp \
    CameraDevice.cpp \
    CameraFactory.cpp \
    CameraHal.cpp \
    CameraHalDeviceOps.cpp \
    CapturedFrame.cpp \
    PreviewStream.cpp \
    PropertyManager.cpp \
    WorkerThread.cpp \
    fake_camera/FakeCameraDelegate.cpp

include $(BUILD_SHARED_LIBRARY)
