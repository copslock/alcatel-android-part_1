/*
 * Copyright 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "Common.h"
#include "CapturedFrame.h"

#include <cutils/log.h>
#include <libyuv.h>

#include <errno.h>

namespace android {

static void YUYVToYV12(const void* yuyv, void* yu12, int width, int height);
static void YUYVToNV21(const void* yuyv, void* nv21, int width, int height);

size_t CapturedFrame::getConvertedSize(const CapturedFrame &frame, uint32_t hal_pixel_format) {
    if ((frame.width % 2) || (frame.height % 2)) {
        HAL_LOGE("Width or height is not even (%d x %d)", frame.width, frame.height);
        return 0;
    }

    switch (hal_pixel_format) {
        case HAL_PIXEL_FORMAT_YV12: // YV12
            // Fall-through.
        case HAL_PIXEL_FORMAT_YCrCb_420_SP: // NV21
            return frame.width * frame.height * 3 / 2;
        case HAL_PIXEL_FORMAT_BGRA_8888:
            return frame.width * frame.height * 4;
        default:
            HAL_LOGE("Pixel format %d is unsupported.", hal_pixel_format);
            return 0;
    }
}

int CapturedFrame::convert(const CapturedFrame &frame, uint32_t hal_pixel_format,
                          void* output_buffer, size_t output_buffer_size) {
    if ((frame.width % 2) || (frame.height % 2)) {
        HAL_LOGE("Width or height is not even (%d x %d)", frame.width, frame.height);
        return 0;
    }

    if (output_buffer_size < getConvertedSize(frame, hal_pixel_format)) {
        HAL_LOGE("Buffer overflow: output buffer (size = %zu) insufficient for %dx%d frame.",
                 output_buffer_size, frame.width, frame.height);
        return -EINVAL;
    }

    if (frame.fourcc == V4L2_PIX_FMT_YUYV) {
        const int src_bytes_per_pixel = 2;

        switch (hal_pixel_format) {
            case HAL_PIXEL_FORMAT_YV12: // YV12
                YUYVToYV12(frame.buffer, output_buffer, frame.width, frame.height);
                return 0;
            case HAL_PIXEL_FORMAT_YCrCb_420_SP: // NV21
                YUYVToNV21(frame.buffer, output_buffer, frame.width, frame.height);
                return 0;
            case HAL_PIXEL_FORMAT_BGRA_8888:
            {
                // libyuv::YUY2ToARGB converts to ARGB little endian (BGRA in memory).
                // The naming convention of endianness is different between libyuv and gralloc.
                int res = libyuv::YUY2ToARGB(frame.buffer, frame.width * src_bytes_per_pixel,
                                             static_cast<uint8_t*>(output_buffer), frame.width * 4,
                                             frame.width, frame.height);
                HAL_LOGE_IF(res, "YUY2ToARGB() returns %d", res);
                return res ? -EINVAL : 0;
            }
            default:
                HAL_LOGE("Destination pixel format %d is unsupported for YUYV source format.",
                         hal_pixel_format);
                return -EINVAL;
        }
    } else if (frame.fourcc == V4L2_PIX_FMT_MJPEG) {
        switch (hal_pixel_format) {
            case HAL_PIXEL_FORMAT_YV12: // YV12
                // TODO: implement this.
                // TODO: By caching the converted RGBA buffer, it can save JPEG decode time to
                // convert RGBA to YV12.
                return -EINVAL;
            case HAL_PIXEL_FORMAT_YCrCb_420_SP: // NV21
                // TODO: implement this.
                // TODO: By caching the converted RGBA buffer, it can save JPEG decode time to
                // convert RGBA to NV21.
                return -EINVAL;
            case HAL_PIXEL_FORMAT_BGRA_8888:
            {
                int res = libyuv::MJPGToARGB(frame.buffer, frame.dataSize,
                                             static_cast<uint8_t*>(output_buffer), frame.width * 4,
                                             frame.width, frame.height,
                                             frame.width, frame.height);
                HAL_LOGE_IF(res, "MJPGToARGB() returns %d", res);
                return res ? -EINVAL : 0;
            }
            default:
                HAL_LOGE("Destination pixel format %d is unsupported for MJPEG source format.",
                         hal_pixel_format);
                return -EINVAL;
        }
    } else {
        HAL_LOGE("Captured frame only supports YUYV and MJPEG source format. "
                 "FOURCC 0x%x is unsupported", frame.fourcc);
        return -EINVAL;
    }
}

static void YUYVToYV12(const void* yuyv, void* yv12, int width, int height) {
    // TODO: can be optimized if the address is aligned.
    if ((width % 2) || (height % 2)) {
        HAL_LOGE("Width or height is not even (%d x %d)", width, height);
        return;
    }

    const uint8_t *src = reinterpret_cast<const uint8_t*>(yuyv);

    uint8_t *yplane = reinterpret_cast<uint8_t*>(yv12);
    uint8_t *vplane = yplane + width * height;
    uint8_t *uplane = vplane + width * height / 4;

    for (int i = 0; i < height / 2; ++i) {
        // Convert even rows.
        for (int j = 0; j < (width / 2); ++j) {
            yplane[0] = src[0];
            *uplane = src[1];
            yplane[1] = src[2];
            *vplane = src[3];
            src += 4;
            yplane += 2;
            uplane++;
            vplane++;
        }
        // Convert odd rows.
        for (int j = 0; j < (width / 2); ++j) {
            yplane[0] = src[0];
            yplane[1] = src[2];
            // TODO: we do not use U/V values of odd rows from source buffer. This will impact
            // image quality.
            src += 4;
            yplane += 2;
        }
    }
}

static void YUYVToNV21(const void* yuyv, void* nv21, int width, int height) {
    // TODO: can be optimized if the address is aligned.
    if ((width % 2) || (height % 2)) {
        HAL_LOGE("Width or height is not even (%d x %d)", width, height);
        return;
    }

    const uint8_t *src = reinterpret_cast<const uint8_t*>(yuyv);

    uint8_t *yplane = reinterpret_cast<uint8_t*>(nv21);
    uint8_t *vuplane = yplane + width * height;

    for (int i = 0; i < height / 2; ++i) {
        // Convert even rows.
        for (int j = 0; j < (width / 2); ++j) {
            yplane[0] = src[0];
            yplane[1] = src[2];
            vuplane[0] = src[3];
            vuplane[1] = src[1];
            src += 4;
            yplane += 2;
            vuplane += 2;
        }
        // Convert odd rows.
        for (int j = 0; j < (width / 2); ++j) {
            yplane[0] = src[0];
            yplane[1] = src[2];
            // TODO: we do not use U/V values of odd rows from source buffer. This will impact
            // image quality.
            src += 4;
            yplane += 2;
        }
    }
}

const std::vector<uint32_t> CapturedFrame::getSupportedFourCCs() {
    // The preference of supported fourccs in the list is from high to low.
    static const std::vector<uint32_t> kSupportedFourCCs = {
        V4L2_PIX_FMT_MJPEG,
        V4L2_PIX_FMT_YUYV,
    };
    return kSupportedFourCCs;
}

}  // namespace android
