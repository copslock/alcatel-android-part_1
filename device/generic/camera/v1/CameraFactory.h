/*
 * Copyright 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ANDROID_DEVICE_CAMERA_CAMERA_FACTORY_H
#define ANDROID_DEVICE_CAMERA_CAMERA_FACTORY_H

#include "PropertyManager.h"

#include <hardware/camera_common.h>
#include <utils/RefBase.h>
#include <utils/Vector.h>
#include <utils/Singleton.h>

namespace android {

class CameraProperties;
class CameraClient;

// CameraFactory implements Camera HAL interface functions defined in CameraHal.cpp and
// CameraHalDeviceOps.cpp.
class CameraFactory : public Singleton<CameraFactory> {
public:
    CameraFactory();
    ~CameraFactory();

    // Callbacks for camera_module_t:
    int deviceOpen(int camera_id, hw_device_t** hw_device);
    int getNumberOfCameras() const {
        return mCameras.size();
    }
    int getCameraInfo(int camera_id, camera_info* info);
    int setCallbacks(const camera_module_callbacks_t* callbacks);

private:
    // Create a new camera instance using provided properties.
    bool createCamera(const CameraProperties& properties);

    Vector<sp<CameraClient>> mCameras;
    Vector<CameraProperties> mProperties;

    const camera_module_callbacks_t* mCallbacks;

    PropertyManager mPropertyManager;

    DISALLOW_COPY_AND_ASSIGN(CameraFactory);
};

}  // namespace android

#endif
