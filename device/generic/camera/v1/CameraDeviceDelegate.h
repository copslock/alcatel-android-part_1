/*
 * Copyright 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ANDROID_DEVICE_CAMERA_CAMERA_DELEGATE_H
#define ANDROID_DEVICE_CAMERA_CAMERA_DELEGATE_H

#include "CommonTypes.h"

#include <string>
#include <vector>

namespace android {

// CameraDeviceDelegate should match the one in ChromeOS.
class CameraDeviceDelegate {
public:
    CameraDeviceDelegate() {};
    virtual ~CameraDeviceDelegate() {};

    // Connect camera device with |device_path|. Return 0 if device is opened
    // successfully. Otherwise, return -|errno|.
    virtual int Connect(const std::string& device_path) = 0;

    // Disconnect camera device. This function is a no-op if the camera device
    // is not connected. If the stream is on, this function will also stop the
    // stream.
    virtual void Disconnect() = 0;

    // Enable camera device stream. Setup captured frame with |width|x|height|
    // resolution, |pixel_format|, and |frame_rate|. Get frame buffer file
    // descriptors |fds| and |buffer_size|. |buffer_size| is the size allocated
    // for each buffer. The ownership of |fds| are transferred to the caller and
    // |fds| should be closed when done. Caller can memory map |fds| and should
    // unmap when done. Return 0 if device supports the format. Otherwise, return
    // -|errno|. This function should be called after Connect().
    virtual int StreamOn(
            int width, int height, uint32_t pixel_format, float frame_rate,
            std::vector<int>* fds, uint32_t* buffer_size) = 0;

    // Disable camera device stream. Return 0 if device disables stream
    // successfully. Otherwise, return -|errno|. This function is a no-op if the
    // stream is already stopped.
    virtual int StreamOff() = 0;

    // Get next frame buffer from device. Device returns the corresponding
    // buffer with |buffer_id| and |data_size| bytes. |data_size| is how many
    // bytes used in the buffer for this frame. Return 0 if device gets the
    // buffer successfully. Otherwise, return -|errno|. Return -EAGAIN immediately
    // if next frame buffer is not ready. This function should be called after
    // StreamOn().
    virtual int GetNextFrameBuffer(uint32_t* buffer_id, uint32_t* data_size) = 0;

    // Return |buffer_id| buffer to device. Return 0 if the buffer is returned
    // successfully. Otherwise, return -|errno|. This function should be called
    // after StreamOn().
    virtual int ReuseFrameBuffer(uint32_t buffer_id) = 0;

    // Get all supported formats of device by |device_path|. This function can be
    // called without calling Connect().
    virtual const SupportedFormats GetDeviceSupportedFormats(
            const std::string& device_path) = 0;

    // Get all camera devices information. This function can be called without
    // calling Connect().
    virtual const DeviceInfos GetCameraDeviceInfos() = 0;

    DISALLOW_COPY_AND_ASSIGN(CameraDeviceDelegate);
};

}  // namespace android

#endif
