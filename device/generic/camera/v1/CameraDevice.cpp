/*
 * Copyright 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "Common.h"
#include "CameraDevice.h"

#include "CameraProperties.h"
#include "CapturedFrame.h"
#include "fake_camera/FakeCameraDelegate.h"

#include <cutils/log.h>
#include <linux/videodev2.h>
#include <sys/mman.h>
#include <utils/Log.h>
#include <utils/Timers.h>

namespace android {

CameraDevice::CameraDevice(const CameraProperties& properties) :
        mProperties(properties),
        mWorkerThread(new WorkerThread(this)),
        mStoppingWorkerThread(false),
        mCameraDelegate(new FakeCameraDelegate()),
        mBufferSize(0) {
    DeviceInfos camera_devices = mCameraDelegate->GetCameraDeviceInfos();
    // TODO: check vid and pid to select correct camera when we have multiple cameras.
    // Currently, we always choose the first one.
    if (camera_devices.size() >= 1) {
        mDevicePath = camera_devices[0].devicePath;
        HAL_LOGV("Camera device path is %s", mDevicePath.c_str());
    } else {
        HAL_LOGE("No camera device found.");
    }
}

CameraDevice::~CameraDevice() {
    HAL_LOG_ENTER();
    stopDeliveringFrames();
    disconnect();
}

void CameraDevice::setClient(CameraDeviceClientBase* client) {
    mCameraClient = client;
}

int CameraDevice::connect() {
    HAL_LOG_ENTER();
    int ret = mCameraDelegate->Connect(mDevicePath);
    if (ret) {
        HAL_LOGE("Connect failed: %s", strerror(-ret));
        return ret;
    }
    return 0;
}

int CameraDevice::disconnect() {
    HAL_LOG_ENTER();
    mCameraDelegate->Disconnect();
    releaseBuffers();
    return 0;
}

int CameraDevice::streamOn(int width, int height, float fps, uint32_t pixel_format) {
    HAL_LOG_ENTER();
    std::vector<int> fds;
    uint32_t buffer_size;
    int ret;

    if ((ret = mCameraDelegate->StreamOn(width, height, pixel_format, fps, &fds, &buffer_size))) {
        HAL_LOGE("StreamOn failed: %s", strerror(-ret));
        return ret;
    }
    mBufferSize = buffer_size;

    CapturedFrame frame;
    frame.dataSize = 0;
    frame.width = width;
    frame.height = height;
    frame.fourcc = pixel_format;

    for (size_t i = 0; i < fds.size(); i++) {
        mFds.push_back(fds[i]);

        void* addr = mmap(NULL, buffer_size, PROT_READ, MAP_SHARED, fds[i], 0);
        if (addr== MAP_FAILED) {
            HAL_LOGE("mmap() (%zu) failed: %s", i, strerror(errno));
            streamOff();
            return -errno;
        }
        frame.buffer = static_cast<uint8_t*>(addr);
        HAL_LOGV("Buffer %zu, fd: %d address: %p", i, fds[i], frame.buffer);
        mBuffers.push_back(frame);
    }

    return 0;
}

int CameraDevice::streamOff() {
    HAL_LOG_ENTER();
    if (mWorkerThread->isRunning()) {
        // |mBuffers| is not protected and threadLoop should be run atomically.
        // Worker thread must be stopped first before streamOff.
        HAL_LOGE("Internal error - stopDeliveringFrames() must be called first");
        return -EIO;
    }
    int ret;
    if ((ret = mCameraDelegate->StreamOff())) {
        HAL_LOGE("StreamOff failed: %s", strerror(-ret));
    }
    releaseBuffers();
    return ret;
}

const SupportedFormats CameraDevice::getDeviceSupportedFormats() {
    return mCameraDelegate->GetDeviceSupportedFormats(mDevicePath);
}

int CameraDevice::startDeliveringFrames() {
    HAL_LOG_ENTER();
    return mWorkerThread->start();
}

int CameraDevice::stopDeliveringFrames() {
    HAL_LOG_ENTER();
    int res = 0;
    if (mWorkerThread->isRunning()) {
        mStoppingWorkerThread = true;
        // Blocking call waiting for worker thread fully stopped.
        res = mWorkerThread->stop();
        mStoppingWorkerThread = false;
    }
    return res;
}

bool CameraDevice::isStoppingDeliveryingFrames() const {
    return mStoppingWorkerThread;
}

bool CameraDevice::threadLoop() {
    int ret;
    uint32_t buffer_id, data_size;
    if ((ret = mCameraDelegate->GetNextFrameBuffer(&buffer_id, &data_size))) {
        HAL_LOGE("getNextFrameBuffer failed: %s", strerror(-ret));
        return false;
    }

    sp<CameraDeviceClientBase> client = mCameraClient.promote();
    if (client == NULL) {
        return false;
    }
    mBuffers[buffer_id].dataSize = data_size;
    client->onDeviceNextFrameAvailable(this, mBuffers[buffer_id],
                                       systemTime(SYSTEM_TIME_MONOTONIC));

    if ((ret = mCameraDelegate->ReuseFrameBuffer(buffer_id))) {
        HAL_LOGE("reuseFrameBuffer failed: %s", strerror(-ret));
        return false;
    }
    // Continue the loop.
    return true;
}

void CameraDevice::releaseBuffers() {
    for(auto const& frame : mBuffers) {
        if (munmap(frame.buffer, mBufferSize)) {
            HAL_LOGE("munmap() failed: %s", strerror(errno));
        }
    }
    mBuffers.clear();
    for (auto const& fd : mFds) {
        if (close(fd)) {
            HAL_LOGE("close(%d) failed: %s", fd, strerror(errno));
        }
    }
    mFds.clear();
    mBufferSize = 0;
}

}  // namespace android
