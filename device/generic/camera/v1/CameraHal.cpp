/*
 * Copyright 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "Common.h"
#include "CameraHal.h"

#include "CameraFactory.h"

#include <cutils/log.h>

namespace android {

static int camera_device_open(const hw_module_t* module,
                              const char* name, hw_device_t** device) {
    HAL_LOG_ENTER();
    if (module != &HAL_MODULE_INFO_SYM.common) {
        HAL_LOGE("Invalid module %p expected %p", module, &HAL_MODULE_INFO_SYM.common);
        return -EINVAL;
    }

    if (name == NULL) {
        HAL_LOGE("name is NULL");
        return -EINVAL;
    }
    return CameraFactory::getInstance().deviceOpen(atoi(name), device);
}

static int camera_get_number_of_cameras() {
    int count = CameraFactory::getInstance().getNumberOfCameras();
    HAL_LOGV("returns %d", count);
    return count;
}

static int camera_get_camera_info(int camera_id, struct camera_info* info) {
    HAL_LOG_ENTER();
    return CameraFactory::getInstance().getCameraInfo(camera_id, info);
}

static int camera_set_callbacks(const camera_module_callbacks_t* callbacks) {
    HAL_LOG_ENTER();
    return CameraFactory::getInstance().setCallbacks(callbacks);
}

}  // namespace android

static struct hw_module_methods_t camera_module_methods = {
    .open = android::camera_device_open
};

camera_module_t HAL_MODULE_INFO_SYM = {
    .common = {
        .tag =                  HARDWARE_MODULE_TAG,
        .module_api_version =   CAMERA_MODULE_API_VERSION_2_1,
        .hal_api_version =      HARDWARE_HAL_API_VERSION,
        .id =                   CAMERA_HARDWARE_MODULE_ID,
        .name =                 "V4L2 Camera HAL v1",
        .author =               "The Android Open Source Project",
        .methods =              &camera_module_methods,
        .dso =                  NULL,
        .reserved =             {0},
    },
    .get_number_of_cameras =    android::camera_get_number_of_cameras,
    .get_camera_info =          android::camera_get_camera_info,
    .set_callbacks =            android::camera_set_callbacks,
    .get_vendor_tag_ops =       NULL,
    .open_legacy =              NULL,
    .set_torch_mode =           NULL,
    .init =                     NULL,
    .reserved =                 {NULL, NULL, NULL, NULL, NULL}
};
