/*
 * Copyright 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "Common.h"
#include "WorkerThread.h"

#include <cutils/log.h>

namespace android {

WorkerThread::WorkerThread(WorkerThreadClient* client) :
        Thread(false),  // Callbacks will not involve Java calls.
        mClient(client) {}

int WorkerThread::start() {
    int res = run(NULL, ANDROID_PRIORITY_URGENT_DISPLAY, 0);
    HAL_LOGE_IF(res != 0, "failed to start thread (errno %d)", res);
    return -res;
}

int WorkerThread::stop() {
    mClient->abortLoop();
    // Stop the thread, and wait till it's terminated.
    int res = requestExitAndWait();
    HAL_LOGE_IF(res, "requestExitAndWait failed (errno %d)", res);
    return -res;
}

bool WorkerThread::threadLoop() {
    if (!mClient->threadLoop()) {
        return false;
    }
    return true;
}

}  // namespace android
