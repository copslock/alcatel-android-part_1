/*
 * Copyright 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "Common.h"
#include "PreviewStream.h"

#include "CapturedFrame.h"

#include <camera/CameraParameters.h>
#include <cutils/log.h>
#include <ui/GraphicBufferMapper.h>
#include <ui/Rect.h>

namespace android {

PreviewStream::PreviewStream() :
        mOps(NULL),
        mGeometryWidth(0),
        mGeometryHeight(0) {}

int PreviewStream::setPreviewWindow(struct preview_stream_ops* ops) {
    HAL_LOGV("%p -> %p", mOps, ops);
    int res = -EINVAL;
    mGeometryWidth = 0;
    mGeometryHeight = 0;
    mOps = NULL;
    if (!ops || !ops->set_usage || !ops->set_buffers_geometry || !ops->dequeue_buffer ||
        !ops->enqueue_buffer || !ops->lock_buffer || !ops->cancel_buffer || !ops->set_timestamp) {
        HAL_LOGE("NULL callback");
    } else {
        res = ops->set_usage(ops, GRALLOC_USAGE_SW_WRITE_OFTEN);
        if (!res) {
            mOps = ops;
        } else {
            HAL_LOGE("Couldn't set preview stream usage (errno %d)", res);
        }
    }

    return res;
}

void PreviewStream::onDeviceNextFrameAvailable(const CapturedFrame& frame, nsecs_t timestamp) {
    int res;
    if (!mOps) {
        HAL_LOGD("Skip update for invalid state.");
        return;
    }

    // Make sure that preview window dimensions matches the frame.
    if (mGeometryWidth != frame.width || mGeometryHeight != frame.height) {
        // Need to set / adjust buffer geometry for the preview window.
        // TODO: do we want to support non-RGB pixel format of preview window?
        HAL_LOGV("Adjusting preview windows %p geometry to %dx%d",
                 mOps, frame.width, frame.height);
        res = mOps->set_buffers_geometry(mOps, frame.width, frame.height,
                                         HAL_PIXEL_FORMAT_BGRA_8888);
        if (res) {
            HAL_LOGE("Error in set_buffers_geometry %d -> %s", -res, strerror(-res));
            return;
        }
        mGeometryWidth = frame.width;
        mGeometryHeight = frame.height;
    }

    // Push new frame to the preview window.
    // Dequeue preview window buffer for the frame.
    buffer_handle_t* buffer = NULL;
    int stride = 0;
    res = mOps->dequeue_buffer(mOps, &buffer, &stride);
    if (res || buffer == NULL) {
        HAL_LOGE("Unable to dequeue preview window buffer: %d -> %s", -res, strerror(-res));
        return;
    }
    if (stride != mGeometryWidth) {
        HAL_LOGE("Unsupported - stride (%d) mismatches buffer geometry (%d).",
                 stride, mGeometryWidth);
        return;
    }

    // Now let the graphics framework lock the buffer, and provide us with the framebuffer data
    // address.
    void* mapped_data = NULL;
    const Rect rect(mGeometryWidth, mGeometryHeight);
    GraphicBufferMapper& grbuffer_mapper(GraphicBufferMapper::get());
    res = grbuffer_mapper.lock(*buffer, GRALLOC_USAGE_SW_WRITE_OFTEN, rect, &mapped_data);
    if (res) {
        HAL_LOGE("grbuffer_mapper.lock failure: %d -> %s", -res, strerror(-res));
        mOps->cancel_buffer(mOps, buffer);
        return;
    }

    // Unfortunately GraphicBufferMapper doesn't provide size info, use getConvertedSize() for
    // boundary check of output buffer.
    if (CapturedFrame::convert(
                frame, HAL_PIXEL_FORMAT_BGRA_8888, mapped_data,
                CapturedFrame::getConvertedSize(frame, HAL_PIXEL_FORMAT_BGRA_8888)) == 0) {
        mOps->set_timestamp(mOps, timestamp);
        mOps->enqueue_buffer(mOps, buffer);
    } else {
        mOps->cancel_buffer(mOps, buffer);
    }
    grbuffer_mapper.unlock(*buffer);
}

} // namespace android
