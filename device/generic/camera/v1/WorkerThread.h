/*
 * Copyright 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ANDROID_DEVICE_CAMERA_WORKER_THREAD_H
#define ANDROID_DEVICE_CAMERA_WORKER_THREAD_H

#include <utils/Thread.h>

namespace android {

class WorkerThread;

class WorkerThreadClient {
public:
    WorkerThreadClient() {}
    virtual ~WorkerThreadClient() {}

    // The client of WorkerThread should implement the routine of single iteration (i.e., single
    // frame) in this method.
    // Return true to continue the next loop; false to forcibly stop looping (typically on failure).
    virtual bool threadLoop() = 0;

    // Notify to abort threadLoop() as soon as possible. It's up to the subclass to decide how to
    // abort the loop efficiently.
    virtual void abortLoop() {}
};

class WorkerThread : public Thread {
public:
    // |client| should not be destroyed before WorkerThread.
    explicit WorkerThread(WorkerThreadClient* client);
    ~WorkerThread() {}

    // Caller has to call stop() to stop the loop.
    int start();

    // stop() will wait blockingly until the thread is fully terminated.
    int stop();

private:
    // Thread overrides:
    bool threadLoop() override;

    WorkerThreadClient* mClient;
};

}  // namespace android

#endif
