/*
 * Copyright 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ANDROID_DEVICE_CAMERA_CAPTURED_FRAME_H
#define ANDROID_DEVICE_CAMERA_CAPTURED_FRAME_H

// Declarations of HAL_PIXEL_FORMAT_XXX.
#include <system/graphics.h>

// FourCC pixel formats (defined as V4L2_PIX_FMT_*).
#include <linux/videodev2.h>

#include <vector>

namespace android {

struct CapturedFrame {
    uint8_t* buffer;
    size_t dataSize; /* How many bytes used in the buffer */
    int width;
    int height;
    uint32_t fourcc;

    // Calculate the output buffer size when converting to the specified pixel format.
    // |hal_pixel_format| is defined as HAL_PIXEL_FORMAT_XXX in
    // /system/core/include/system/graphics.h.
    // Return 0 on error.
    static size_t getConvertedSize(const CapturedFrame &frame, uint32_t hal_pixel_format);

    // Return non-zero error code on failure; return 0 on success.
    static int convert(const CapturedFrame &frame, uint32_t hal_pixel_format, void* output_buffer,
                       size_t output_buffer_size);

    // Get all supported source pixel formats of conversion in fourcc.
    static const std::vector<uint32_t> getSupportedFourCCs();
};

}  // namespace android

#endif
