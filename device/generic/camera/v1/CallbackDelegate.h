/*
 * Copyright 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ANDROID_DEVICE_CAMERA_CALLBACK_NOTIFIER_H
#define ANDROID_DEVICE_CAMERA_CALLBACK_NOTIFIER_H

#include <stdatomic.h>
#include <hardware/camera.h>
#include <utils/List.h>
#include <utils/Timers.h>

#include "CapturedFrame.h"

namespace android {

struct CapturedFrame;

// Delegate class of callback mangement defined by Camera HALv1 API.
//
// The class is not thread-safe. Thread-safety should be implemented on the caller side
// (e.g., CameraClient).
class CallbackDelegate {
public:
    CallbackDelegate();
    ~CallbackDelegate() {}

    // Resets the callback notifier.
    void reset();

    // Delegates of callback-related methods in camera_device_ops:
    void setCallbacks(camera_notify_callback notify_cb,
                      camera_data_callback data_cb,
                      camera_data_timestamp_callback data_timestamp_cb,
                      camera_request_memory memory_cb, void* user);
    void enableMessage(int32_t msg_type);
    void disableMessage(int32_t msg_type);
    int isMessageEnabled(int32_t msg_type) const;

    // Events sent by camera device:

    // Notifies the framework about the available preview frame.
    // |preview_hal_pixel_format| is the pixel format to be deliverd with CAMERA_MSG_PREVIEW_FRAME
    // callback. It is defined as HAL_PIXEL_FORMAT_XXX in /system/core/include/system/graphics.h.
    void onDeviceNextPreviewFrameAvailable(const CapturedFrame& frame, nsecs_t timestamp,
                                           uint32_t preview_hal_pixel_format);
    // Notifies the framework about the available picture capture frame.
    void onDeviceNextPictureFrameAvailable(const CapturedFrame& frame);

    // |error| is defined as CAMERA_ERROR_* at /system/core/include/system/camera.h.
    void onDeviceError(int error);

private:
    void onMjpegPictureTaken(const CapturedFrame& frame);
    camera_memory_t* allocateCameraMemory(size_t size);

    // Callbacks:
    camera_notify_callback mNotifyCallback;
    camera_data_callback mDataCallback;
    camera_data_timestamp_callback mDataTimestampCallback;
    camera_request_memory mRequestMemoryCallback;
    void* mOpaqueData;

    // Represents CAMERA_MESSAGE_* enabled at /system/core/include/system/camera.h.
    std::atomic<int32_t> mEnabledMessages;
};

}  // namespace android

#endif
