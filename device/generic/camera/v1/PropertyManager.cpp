/*
 * Copyright 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "Common.h"
#include "PropertyManager.h"

#include <cutils/log.h>
#include <cutils/properties.h>

namespace android {

#define CAMERA_HAL_PROPERTY_PREFIX "v4l2camera."

PropertyManager::PropertyManager() {
    mBackProperties.facing = CAMERA_FACING_BACK;
    mFrontProperties.facing = CAMERA_FACING_FRONT;

    // The default setup is 'one front camera'.
    mHasBackCamera = property_get_bool(CAMERA_HAL_PROPERTY_PREFIX "has_back_camera" , false);
    mHasFrontCamera = property_get_bool(CAMERA_HAL_PROPERTY_PREFIX "has_front_camera" , true);

    if (mHasBackCamera) {
        mBackProperties.orientation = property_get_int32(
                CAMERA_HAL_PROPERTY_PREFIX "back_orientation", 0);
    }
    if (mHasFrontCamera) {
        mFrontProperties.orientation = property_get_int32(
                CAMERA_HAL_PROPERTY_PREFIX "front_orientation", 0);
    }
}

bool PropertyManager::hasBackCamera() const {
    return mHasBackCamera;
}

bool PropertyManager::hasFrontCamera() const {
    return mHasFrontCamera;
}

const CameraProperties& PropertyManager::getBackProperties() const {
    return mBackProperties;
}

const CameraProperties& PropertyManager::getFrontProperties() const {
    return mFrontProperties;
}

}  // namespace android
