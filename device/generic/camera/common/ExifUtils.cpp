/*
 * Copyright 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <utils/Log.h>

#include "ExifUtils.h"

#include <csetjmp>
#include <cstdlib>
#include <cstring>
#include <ctime>

extern "C" {
#include <jpeglib.h>
}

namespace std {

template<>
struct default_delete<ExifEntry> {
    inline void operator()(ExifEntry* entry) const {
        exif_entry_unref(entry);
    }
};

} // end of namespace std

namespace android {

/*
 * This is default huffman segment for 8-bit precision luminance and chrominance. The default
 * huffman segment is constructed with the tables from JPEG standard section K.3. Actually there
 * are no default tables. They are typical tables. These tables are useful for many applications.
 * Lots of softwares use them as standard tables such as ffmpeg.
 */
const uint8_t gDefaultDhtSeg[] = {
    0xFF, 0xC4, 0x01, 0xA2, 0x00, 0x00, 0x01, 0x05, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A,
    0x0B, 0x01, 0x00, 0x03, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x10, 0x00,
    0x02, 0x01, 0x03, 0x03, 0x02, 0x04, 0x03, 0x05, 0x05, 0x04, 0x04, 0x00, 0x00, 0x01, 0x7D, 0x01,
    0x02, 0x03, 0x00, 0x04, 0x11, 0x05, 0x12, 0x21, 0x31, 0x41, 0x06, 0x13, 0x51, 0x61, 0x07, 0x22,
    0x71, 0x14, 0x32, 0x81, 0x91, 0xA1, 0x08, 0x23, 0x42, 0xB1, 0xC1, 0x15, 0x52, 0xD1, 0xF0, 0x24,
    0x33, 0x62, 0x72, 0x82, 0x09, 0x0A, 0x16, 0x17, 0x18, 0x19, 0x1A, 0x25, 0x26, 0x27, 0x28, 0x29,
    0x2A, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x3A, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49, 0x4A,
    0x53, 0x54, 0x55, 0x56, 0x57, 0x58, 0x59, 0x5A, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68, 0x69, 0x6A,
    0x73, 0x74, 0x75, 0x76, 0x77, 0x78, 0x79, 0x7A, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89, 0x8A,
    0x92, 0x93, 0x94, 0x95, 0x96, 0x97, 0x98, 0x99, 0x9A, 0xA2, 0xA3, 0xA4, 0xA5, 0xA6, 0xA7, 0xA8,
    0xA9, 0xAA, 0xB2, 0xB3, 0xB4, 0xB5, 0xB6, 0xB7, 0xB8, 0xB9, 0xBA, 0xC2, 0xC3, 0xC4, 0xC5, 0xC6,
    0xC7, 0xC8, 0xC9, 0xCA, 0xD2, 0xD3, 0xD4, 0xD5, 0xD6, 0xD7, 0xD8, 0xD9, 0xDA, 0xE1, 0xE2, 0xE3,
    0xE4, 0xE5, 0xE6, 0xE7, 0xE8, 0xE9, 0xEA, 0xF1, 0xF2, 0xF3, 0xF4, 0xF5, 0xF6, 0xF7, 0xF8, 0xF9,
    0xFA, 0x11, 0x00, 0x02, 0x01, 0x02, 0x04, 0x04, 0x03, 0x04, 0x07, 0x05, 0x04, 0x04, 0x00, 0x01,
    0x02, 0x77, 0x00, 0x01, 0x02, 0x03, 0x11, 0x04, 0x05, 0x21, 0x31, 0x06, 0x12, 0x41, 0x51, 0x07,
    0x61, 0x71, 0x13, 0x22, 0x32, 0x81, 0x08, 0x14, 0x42, 0x91, 0xA1, 0xB1, 0xC1, 0x09, 0x23, 0x33,
    0x52, 0xF0, 0x15, 0x62, 0x72, 0xD1, 0x0A, 0x16, 0x24, 0x34, 0xE1, 0x25, 0xF1, 0x17, 0x18, 0x19,
    0x1A, 0x26, 0x27, 0x28, 0x29, 0x2A, 0x35, 0x36, 0x37, 0x38, 0x39, 0x3A, 0x43, 0x44, 0x45, 0x46,
    0x47, 0x48, 0x49, 0x4A, 0x53, 0x54, 0x55, 0x56, 0x57, 0x58, 0x59, 0x5A, 0x63, 0x64, 0x65, 0x66,
    0x67, 0x68, 0x69, 0x6A, 0x73, 0x74, 0x75, 0x76, 0x77, 0x78, 0x79, 0x7A, 0x82, 0x83, 0x84, 0x85,
    0x86, 0x87, 0x88, 0x89, 0x8A, 0x92, 0x93, 0x94, 0x95, 0x96, 0x97, 0x98, 0x99, 0x9A, 0xA2, 0xA3,
    0xA4, 0xA5, 0xA6, 0xA7, 0xA8, 0xA9, 0xAA, 0xB2, 0xB3, 0xB4, 0xB5, 0xB6, 0xB7, 0xB8, 0xB9, 0xBA,
    0xC2, 0xC3, 0xC4, 0xC5, 0xC6, 0xC7, 0xC8, 0xC9, 0xCA, 0xD2, 0xD3, 0xD4, 0xD5, 0xD6, 0xD7, 0xD8,
    0xD9, 0xDA, 0xE2, 0xE3, 0xE4, 0xE5, 0xE6, 0xE7, 0xE8, 0xE9, 0xEA, 0xF2, 0xF3, 0xF4, 0xF5, 0xF6,
    0xF7, 0xF8, 0xF9, 0xFA
};

// This comes from the Exif Version 2.3 standard table 9.
const uint8_t gExifAsciiPrefix[] = { 0x41, 0x53, 0x43, 0x49, 0x49, 0x0, 0x0, 0x0 };

enum JpegMarker {
    JPEG_MARKER_SOI = 0xD8,
    JPEG_MARKER_DHT = 0xC4,
    JPEG_MARKER_SOS = 0xDA,
    JPEG_MARKER_APP1 = 0xE1,
    JPEG_MARKER_PREFIX = 0xFF
};

enum JpegSegment {
    JPEG_SEGMENT_SOI = 0,
    JPEG_SEGMENT_APP1_HEADER = 1,
    JPEG_SEGMENT_APP1_BUFFER = 2
};

struct my_error_mgr {
    struct jpeg_error_mgr pub;
    jmp_buf setjmp_buffer;
};

struct libjpeg_source_mgr : jpeg_source_mgr {
    libjpeg_source_mgr(uint8_t* ptr, size_t len);
    ~libjpeg_source_mgr();

    uint8_t* mBufferPtr;
    size_t mBufferLength;
};

struct libjpeg_destination_mgr : jpeg_destination_mgr {
    libjpeg_destination_mgr(uint8_t* ptr, size_t size);
    ~libjpeg_destination_mgr();

    uint8_t* mBufferPtr;
    size_t mBufferLength;
    size_t jpegLength;
};

static void libjpeg_init_source(j_decompress_ptr cinfo) {
    libjpeg_source_mgr* src = static_cast<libjpeg_source_mgr*>(cinfo->src);
    src->next_input_byte = static_cast<const JOCTET*>(src->mBufferPtr);
    src->bytes_in_buffer = src->mBufferLength;
}

static boolean libjpeg_fill_input_buffer(j_decompress_ptr /* cinfo */) {
    ALOGE("%s : should not get here", __func__);
    return FALSE;
}

static void libjpeg_skip_input_data(j_decompress_ptr cinfo, long num_bytes) {
    libjpeg_source_mgr* src = static_cast<libjpeg_source_mgr*>(cinfo->src);

    if (num_bytes > static_cast<long>(src->bytes_in_buffer)) {
        ALOGD(": libjpeg_skip_input_data - num_bytes > src->bytes_in_buffer", __func__);
    } else {
        src->next_input_byte += num_bytes;
        src->bytes_in_buffer -= num_bytes;
    }
}

static void libjpeg_term_source(j_decompress_ptr /*cinfo*/) {}

libjpeg_source_mgr::libjpeg_source_mgr(uint8_t* ptr, size_t len) :
        mBufferPtr(ptr), mBufferLength(len) {
    init_source = libjpeg_init_source;
    fill_input_buffer = libjpeg_fill_input_buffer;
    skip_input_data = libjpeg_skip_input_data;
    resync_to_restart = jpeg_resync_to_restart;
    term_source = libjpeg_term_source;
}

libjpeg_source_mgr::~libjpeg_source_mgr() {}

static void libjpeg_init_destination (j_compress_ptr cinfo) {
    libjpeg_destination_mgr* dest = static_cast<libjpeg_destination_mgr*>(cinfo->dest);
    dest->next_output_byte = static_cast<JOCTET*>(dest->mBufferPtr);
    dest->free_in_buffer = dest->mBufferLength;
    dest->jpegLength = 0;
}

static boolean libjpeg_empty_output_buffer(j_compress_ptr /* cinfo */) {
    ALOGE("%s : should not get here", __func__);
    return FALSE;
}

static void libjpeg_term_destination (j_compress_ptr cinfo) {
    libjpeg_destination_mgr* dest = static_cast<libjpeg_destination_mgr*>(cinfo->dest);
    dest->jpegLength = dest->mBufferLength - dest->free_in_buffer;
}

libjpeg_destination_mgr::libjpeg_destination_mgr(uint8_t* ptr, size_t length) {
    init_destination = libjpeg_init_destination;
    empty_output_buffer = libjpeg_empty_output_buffer;
    term_destination = libjpeg_term_destination;

    mBufferPtr = ptr;
    mBufferLength = length;
    jpegLength = 0;
}

libjpeg_destination_mgr::~libjpeg_destination_mgr() {}

static void my_error_exit(j_common_ptr cinfo) {
    my_error_mgr* err = reinterpret_cast<my_error_mgr*>(cinfo->err);
    longjmp(err->setjmp_buffer, 1);
}

static void setLatitudeOrLongitudeData(unsigned char* data, double num) {
    // Take the integer part of |num|.
    ExifLong degrees = static_cast<ExifLong>(num);
    ExifLong minutes = static_cast<ExifLong>(60 * (num - degrees));
    ExifLong microseconds = static_cast<ExifLong>(3600000000u * (num - degrees - minutes / 60.0));
    exif_set_rational(data, EXIF_BYTE_ORDER_INTEL, {degrees, 1});
    exif_set_rational(data + sizeof(ExifRational), EXIF_BYTE_ORDER_INTEL, {minutes, 1});
    exif_set_rational(data + 2 * sizeof(ExifRational), EXIF_BYTE_ORDER_INTEL,
                      {microseconds, 1000000});
}

ExifUtils::ExifUtils() : mThumbnailRatio(0), mExifData(nullptr), mApp1Buffer(nullptr),
        mApp1Length(0), mInputPtr(nullptr), mInputLength(0), mInputHasMarkerDht(false) {
    memset(mApp1Header, 0, sizeof(mApp1Header));
}

ExifUtils::~ExifUtils() {
    reset();
}

void ExifUtils::reset() {
    destroyApp1();
    if (mExifData) {
        /*
         * Since we decided to ignore the original APP1, we are sure that there is no thumbnail
         * allocated by libexif. |mExifData->data| is actually allocated by this class and backed
         * by |mThumbnailBuffer|. Sets |mExifData->data| to nullptr to prevent exif_data_unref()
         * destroy it incorrectly.
         */
        mExifData->data = nullptr;
        mExifData->size = 0;
        exif_data_unref(mExifData);
        mExifData = nullptr;
    }
    mThumbnailRatio = 0;
    mInputPtr = nullptr;
    mInputLength = 0;
    mInputHasMarkerDht = false;
    mSegments.clear();
}

bool ExifUtils::parseJpegSegments() {
    mInputHasMarkerDht = false;
    bool hasMarkerSos = false;
    const uint8_t* ptr = mInputPtr;
    const uint8_t* end = mInputPtr + mInputLength;
    const size_t kSoiMarkerSize = 2;

    if (ptr + kSoiMarkerSize > end) {
        ALOGD("%s : unexpected EOS while trying to read marker", __func__);
        return false;
    }
    if (ptr[0] != JPEG_MARKER_PREFIX || ptr[1] != JPEG_MARKER_SOI) {
        ALOGD("%s : the input is not a Jpeg", __func__);
        return false;
    }
    ptr += kSoiMarkerSize;
    mSegments.resize(3);
    mSegments[JPEG_SEGMENT_SOI] = {mInputPtr, kSoiMarkerSize};
    mSegments[JPEG_SEGMENT_APP1_HEADER] = {nullptr, 0};
    mSegments[JPEG_SEGMENT_APP1_BUFFER] = {nullptr, 0};

    while (!hasMarkerSos && !mInputHasMarkerDht) {
        const uint8_t* segmentStartPtr = ptr;
        if (ptr >= end) {
            ALOGD("%s : unexpected EOS while trying to read marker", __func__);
            return false;
        }
        if (*ptr != JPEG_MARKER_PREFIX) {
            ALOGD("%s : the prefix of marker != 0xFF", __func__);
            return false;
        }
        do {
            ptr++;
            if (ptr >= end) {
                ALOGD("%s : unexpected EOS while trying to read marker", __func__);
                return false;
            }
        } while (*ptr == JPEG_MARKER_PREFIX); // Skips fill bytes.
        uint8_t marker = *ptr++;

        uint16_t size;
        if (ptr + sizeof(size) > end){
            ALOGD("%s : unexpected EOS while trying to read size", __func__);
            return false;
        }
        // It is big endian.
        size = ptr[0] << 8 | ptr[1];
        ptr += sizeof(size);
        if (size < sizeof(size)) {
            ALOGD("%s : segment size (%d) is smaller than size field (%zu)",
                    __func__, static_cast<int>(size), sizeof(size));
            return false;
        }
        size -= sizeof(size);

        switch (marker) {
            case JPEG_MARKER_DHT: {
                mInputHasMarkerDht = true;
                break;
            }
            case JPEG_MARKER_SOS: {
                if (!mInputHasMarkerDht) {
                    mSegments.push_back({gDefaultDhtSeg, sizeof(gDefaultDhtSeg)});
                }
                hasMarkerSos = true;
                break;
            }
            default:
                break;
        }

        if (ptr + size > end) {
            ALOGD("%s : remaining size (%zu) is smaller then header specified (%d)",
                    __func__, static_cast<size_t>(end - ptr), static_cast<int>(size));
            return false;
        }
        ptr += size;
        // Discard APP1 in the original image. This is not expected in MJPEG, but just in case.
        if (marker != JPEG_MARKER_APP1) {
            mSegments.push_back({segmentStartPtr, static_cast<size_t>(ptr - segmentStartPtr)});
        }
    }
    if (ptr < end) {
        mSegments.push_back({ptr, static_cast<size_t>(end - ptr)});
    }

    return true;
}

bool ExifUtils::initialize(const uint8_t* buffer, const size_t length) {
    reset();
    mInputPtr = buffer;
    mInputLength = length;
    if (!parseJpegSegments()) {
        ALOGD("%s : parse JPEG mSegments failed", __func__);
        return false;
    }

    mExifData = exif_data_new();
    if (mExifData == nullptr) {
        ALOGE("%s : allocate memory for mExifData failed", __func__);
        return false;
    }
    // Set the image options.
    exif_data_set_option(mExifData, EXIF_DATA_OPTION_FOLLOW_SPECIFICATION);
    exif_data_set_data_type(mExifData, EXIF_DATA_TYPE_COMPRESSED);
    exif_data_set_byte_order(mExifData, EXIF_BYTE_ORDER_INTEL);

    return true;
}

std::unique_ptr<ExifEntry> ExifUtils::addVariableLengthEntry(ExifIfd ifd, ExifTag tag,
                                                             ExifFormat format,
                                                             unsigned long components,
                                                             unsigned int size) {
    // Remove old entry if exists.
    exif_content_remove_entry(mExifData->ifd[ifd],
                              exif_content_get_entry(mExifData->ifd[ifd], tag));
    ExifMem* mem = exif_mem_new_default();
    if (!mem) {
        ALOGE("%s : allocate memory for exif entry failed", __func__);
        return nullptr;
    }
    std::unique_ptr<ExifEntry> entry(exif_entry_new_mem(mem));
    if (!entry) {
        ALOGE("%s : allocate memory for exif entry failed", __func__);
        exif_mem_unref(mem);
        return nullptr;
    }
    void* tmpBuffer = exif_mem_alloc(mem, size);
    if (!tmpBuffer) {
        ALOGE("%s : allocate memory for exif entry failed", __func__);
        exif_mem_unref(mem);
        return nullptr;
    }

    entry->data = static_cast<unsigned char*>(tmpBuffer);
    entry->tag = tag;
    entry->format = format;
    entry->components = components;
    entry->size = size;

    exif_content_add_entry(mExifData->ifd[ifd], entry.get());
    exif_mem_unref(mem);

    return entry;
}

std::unique_ptr<ExifEntry> ExifUtils::addEntry(ExifIfd ifd, ExifTag tag) {
    std::unique_ptr<ExifEntry> entry(exif_content_get_entry(mExifData->ifd[ifd], tag));
    if (entry) {
        // exif_content_get_entry() won't ref the entry, so we ref here.
        exif_entry_ref(entry.get());
        return entry;
    }
    entry.reset(exif_entry_new());
    if (!entry) {
        ALOGE("%s : allocate memory for exif entry failed", __func__);
        return nullptr;
    }
    entry->tag = tag;
    exif_content_add_entry(mExifData->ifd[ifd], entry.get());
    exif_entry_initialize(entry.get(), tag);
    return entry;
}

bool ExifUtils::setMaker(const std::string& maker) {
    size_t entrySize = maker.length() + 1;
    std::unique_ptr<ExifEntry> entry = addVariableLengthEntry(EXIF_IFD_0, EXIF_TAG_MAKE,
                                                              EXIF_FORMAT_ASCII, entrySize,
                                                              entrySize);
    if (!entry) {
        ALOGE("%s : Adding Make exif entry failed", __func__);
        return false;
    }
    memcpy(entry->data, maker.c_str(), entrySize);
    return true;
}

bool ExifUtils::setModel(const std::string& model) {
    size_t entrySize = model.length() + 1;
    std::unique_ptr<ExifEntry> entry = addVariableLengthEntry(EXIF_IFD_0, EXIF_TAG_MODEL,
                                                              EXIF_FORMAT_ASCII, entrySize,
                                                              entrySize);
    if (!entry) {
        ALOGE("%s : Adding Model exif entry failed", __func__);
        return false;
    }
    memcpy(entry->data, model.c_str(), entrySize);
    return true;
}

bool ExifUtils::setDateTime(const struct tm& t) {
    // The length is 20 bytes including NULL for termination in Exif standard.
    char str[20];
    int result = snprintf(str, sizeof(str), "%04i:%02i:%02i %02i:%02i:%02i", t.tm_year + 1900,
                          t.tm_mon + 1, t.tm_mday, t.tm_hour, t.tm_min, t.tm_sec);
    if (result != sizeof(str) - 1) {
        ALOGW("%s : input time is invalid", __func__);
        return false;
    }
    std::unique_ptr<ExifEntry> entry = addVariableLengthEntry(EXIF_IFD_0, EXIF_TAG_DATE_TIME,
                                                              EXIF_FORMAT_ASCII, sizeof(str),
                                                              sizeof(str));
    if (!entry) {
        ALOGE("%s : Adding DateTime exif entry failed", __func__);
        return false;
    }
    memcpy(entry->data, str, sizeof(str));
    return true;
}

bool ExifUtils::setImageWidth(uint16_t width) {
    std::unique_ptr<ExifEntry> entry = addEntry(EXIF_IFD_0, EXIF_TAG_IMAGE_WIDTH);
    if (!entry) {
        ALOGE("%s : Adding ImageWidth exif entry failed", __func__);
        return false;
    }
    exif_set_short(entry->data, EXIF_BYTE_ORDER_INTEL, width);
    return true;
}

bool ExifUtils::setImageLength(uint16_t length) {
    std::unique_ptr<ExifEntry> entry = addEntry(EXIF_IFD_0, EXIF_TAG_IMAGE_LENGTH);
    if (!entry) {
        ALOGE("%s : Adding ImageLength exif entry failed", __func__);
        return false;
    }
    exif_set_short(entry->data, EXIF_BYTE_ORDER_INTEL, length);
    return true;
}

bool ExifUtils::setFocalLength(uint32_t numerator, uint32_t denominator) {
    std::unique_ptr<ExifEntry> entry = addEntry(EXIF_IFD_EXIF, EXIF_TAG_FOCAL_LENGTH);
    if (!entry) {
        ALOGE("%s : Adding FocalLength exif entry failed", __func__);
        return false;
    }
    exif_set_rational(entry->data, EXIF_BYTE_ORDER_INTEL, {numerator, denominator});
    return true;
}

bool ExifUtils::setGpsLatitude(double latitude) {
    const ExifTag refTag = static_cast<ExifTag>(EXIF_TAG_GPS_LATITUDE_REF);
    std::unique_ptr<ExifEntry> refEntry = addVariableLengthEntry(EXIF_IFD_GPS, refTag,
                                                                 EXIF_FORMAT_ASCII, 2, 2);
    if (!refEntry) {
        ALOGE("%s : Adding GPSLatitudeRef exif entry failed", __func__);
        return false;
    }
    if (latitude >= 0) {
        memcpy(refEntry->data, "N", sizeof("N"));
    } else {
        memcpy(refEntry->data, "S", sizeof("S"));
        latitude *= -1;
    }

    const ExifTag tag = static_cast<ExifTag>(EXIF_TAG_GPS_LATITUDE);
    std::unique_ptr<ExifEntry> entry = addVariableLengthEntry(EXIF_IFD_GPS, tag,
                                                              EXIF_FORMAT_RATIONAL, 3,
                                                              3 * sizeof(ExifRational));
    if (!entry) {
        exif_content_remove_entry(mExifData->ifd[EXIF_IFD_GPS], refEntry.get());
        ALOGE("%s : Adding GPSLatitude exif entry failed", __func__);
        return false;
    }
    setLatitudeOrLongitudeData(entry->data, latitude);

    return true;
}

bool ExifUtils::setGpsLongitude(double longitude) {
    ExifTag refTag = static_cast<ExifTag>(EXIF_TAG_GPS_LONGITUDE_REF);
    std::unique_ptr<ExifEntry> refEntry = addVariableLengthEntry(EXIF_IFD_GPS, refTag,
                                                                 EXIF_FORMAT_ASCII, 2, 2);
    if (!refEntry) {
        ALOGE("%s : Adding GPSLongitudeRef exif entry failed", __func__);
        return false;
    }
    if (longitude >= 0) {
        memcpy(refEntry->data, "E", sizeof("E"));
    } else {
        memcpy(refEntry->data, "W", sizeof("W"));
        longitude *= -1;
    }

    ExifTag tag = static_cast<ExifTag>(EXIF_TAG_GPS_LONGITUDE);
    std::unique_ptr<ExifEntry> entry = addVariableLengthEntry(EXIF_IFD_GPS, tag,
                                                              EXIF_FORMAT_RATIONAL, 3,
                                                              3 * sizeof(ExifRational));
    if (!entry) {
        exif_content_remove_entry(mExifData->ifd[EXIF_IFD_GPS], refEntry.get());
        ALOGE("%s : Adding GPSLongitude exif entry failed", __func__);
        return false;
    }
    setLatitudeOrLongitudeData(entry->data, longitude);

    return true;
}

bool ExifUtils::setGpsAltitude(double altitude) {
    ExifTag refTag = static_cast<ExifTag>(EXIF_TAG_GPS_ALTITUDE_REF);
    std::unique_ptr<ExifEntry> refEntry = addVariableLengthEntry(EXIF_IFD_GPS, refTag,
                                                                 EXIF_FORMAT_BYTE, 1, 1);
    if (!refEntry) {
        ALOGE("%s : Adding GPSAltitudeRef exif entry failed", __func__);
        return false;
    }
    if (altitude >= 0) {
        *refEntry->data = 0;
    } else {
        *refEntry->data = 1;
        altitude *= -1;
    }

    ExifTag tag = static_cast<ExifTag>(EXIF_TAG_GPS_ALTITUDE);
    std::unique_ptr<ExifEntry> entry = addVariableLengthEntry(EXIF_IFD_GPS, tag,
                                                              EXIF_FORMAT_RATIONAL, 1,
                                                              sizeof(ExifRational));
    if (!entry) {
        exif_content_remove_entry(mExifData->ifd[EXIF_IFD_GPS], refEntry.get());
        ALOGE("%s : Adding GPSAltitude exif entry failed", __func__);
        return false;
    }
    exif_set_rational(entry->data, EXIF_BYTE_ORDER_INTEL,
                      {static_cast<ExifLong>(altitude * 1000), 1000});

    return true;
}

bool ExifUtils::setGpsTimestamp(const struct tm& t) {
    const ExifTag dateTag = static_cast<ExifTag>(EXIF_TAG_GPS_DATE_STAMP);
    const size_t kGpsDateStampSize = 11;
    std::unique_ptr<ExifEntry> entry = addVariableLengthEntry(EXIF_IFD_GPS, dateTag,
                                                              EXIF_FORMAT_ASCII, kGpsDateStampSize,
                                                              kGpsDateStampSize);
    if (!entry) {
        ALOGE("%s : Adding GPSDateStamp exif entry failed", __func__);
        return false;
    }
    int result = snprintf(reinterpret_cast<char*>(entry->data), kGpsDateStampSize, "%04i:%02i:%02i",
                          t.tm_year + 1900, t.tm_mon + 1, t.tm_mday);
    if (result != kGpsDateStampSize - 1) {
        ALOGW("%s : input time is invalid", __func__);
        return false;
    }

    const ExifTag timeTag = static_cast<ExifTag>(EXIF_TAG_GPS_TIME_STAMP);
    entry = addVariableLengthEntry(EXIF_IFD_GPS, timeTag, EXIF_FORMAT_RATIONAL, 3,
                                         3 * sizeof(ExifRational));
    if (!entry) {
        ALOGE("%s : Adding GPSTimeStamp exif entry failed", __func__);
        return false;
    }
    exif_set_rational(entry->data, EXIF_BYTE_ORDER_INTEL, {static_cast<ExifLong>(t.tm_hour), 1});
    exif_set_rational(entry->data + sizeof(ExifRational), EXIF_BYTE_ORDER_INTEL,
                      {static_cast<ExifLong>(t.tm_min), 1});
    exif_set_rational(entry->data + 2 * sizeof(ExifRational), EXIF_BYTE_ORDER_INTEL,
                      {static_cast<ExifLong>(t.tm_sec), 1});

    return true;
}

bool ExifUtils::setGpsProcessingMethod(const std::string& method) {
    ExifTag tag = static_cast<ExifTag>(EXIF_TAG_GPS_PROCESSING_METHOD);
    size_t size = sizeof(gExifAsciiPrefix) + method.length();
    std::unique_ptr<ExifEntry> entry = addVariableLengthEntry(EXIF_IFD_GPS, tag,
                                                              EXIF_FORMAT_UNDEFINED, size, size);
    if (!entry) {
        ALOGE("%s : Adding GPSProcessingMethod exif entry failed", __func__);
        return false;
    }
    memcpy(entry->data, gExifAsciiPrefix, sizeof(gExifAsciiPrefix));
    // Since the exif format is undefined, NULL termination is not necessary.
    memcpy(entry->data + sizeof(gExifAsciiPrefix), method.c_str(), method.length());

    return true;
}

void ExifUtils::setThumbnailSizeRatio(double ratio) {
    mThumbnailRatio = ratio;
}

bool ExifUtils::generateThumbnail() {
    uint8_t* imagePtr = const_cast<uint8_t*>(mInputPtr);
    size_t imageLength = mInputLength;
    /*
     * If the input image does not have Huffman table, add a typical table and copy the image to a
     * temporary buffer. libjpeg can only decompress a JPEG with Huffman table.
     */
    if (!mInputHasMarkerDht) {
        imageLength = getTotalLengthFromSegments();
        mJpegBuffer.resize(imageLength);
        imagePtr = mJpegBuffer.data();
        copyJpegFromSegments(imagePtr);
    }

    // decompress the image
    struct libjpeg_source_mgr decompressMgr(imagePtr, imageLength);
    struct jpeg_decompress_struct dinfo;
    struct my_error_mgr myerr;
    // set error handler
    dinfo.err = jpeg_std_error(&myerr.pub);
    myerr.pub.error_exit = my_error_exit;
    if (setjmp(myerr.setjmp_buffer)) {
        ALOGE("%s : decompress JPEG image failed", __func__);
        jpeg_destroy_decompress(&dinfo);
        return 0;
    }
    jpeg_create_decompress(&dinfo);

    dinfo.src = &decompressMgr;
    jpeg_read_header(&dinfo, TRUE);

    // set scaling ratio
    dinfo.scale_num = 1;
    if (mThumbnailRatio * 8 <= 1) {
        dinfo.scale_denom = 8;
    } else if (mThumbnailRatio * 4 <= 1) {
        dinfo.scale_denom = 4;
    } else if (mThumbnailRatio * 2 <= 1) {
        dinfo.scale_denom = 2;
    } else {
        dinfo.scale_denom = 1;
    }

    jpeg_start_decompress(&dinfo);

    size_t rowStride = dinfo.output_width * dinfo.output_components;
    size_t yuvLength = rowStride * dinfo.output_height;
    mYuvBuffer.resize(yuvLength);
    size_t offset = 0;
    while (dinfo.output_scanline < dinfo.output_height) {
        unsigned char* rowPtr = mYuvBuffer.data() + offset;
        if (jpeg_read_scanlines(&dinfo, &rowPtr, 1)) {
            offset += rowStride;
        }
    }

    jpeg_finish_decompress(&dinfo);
    jpeg_destroy_decompress(&dinfo);

    // compress the image
    mThumbnailBuffer.resize(yuvLength); // Make sure the buffer is big enough.
    struct libjpeg_destination_mgr compressMgr(mThumbnailBuffer.data(), mThumbnailBuffer.size());
    struct jpeg_compress_struct cinfo;
    // set error handler
    cinfo.err = jpeg_std_error(&myerr.pub);
    myerr.pub.error_exit = my_error_exit;
    if (setjmp(myerr.setjmp_buffer)) {
        ALOGE("%s : compress JPEG image failed", __func__);
        jpeg_destroy_compress(&cinfo);
        return 0;
    }
    jpeg_create_compress(&cinfo);

    cinfo.dest = &compressMgr;
    // Four fields of the cinfo struct must be filled in.
    cinfo.image_width = dinfo.output_width;
    cinfo.image_height = dinfo.output_height;
    cinfo.input_components = dinfo.output_components;
    cinfo.in_color_space = dinfo.out_color_space;
    jpeg_set_defaults(&cinfo);

    jpeg_start_compress(&cinfo, TRUE);
    while (cinfo.next_scanline < cinfo.image_height) {
        unsigned char* rowPtr = mYuvBuffer.data() + cinfo.next_scanline * rowStride;
        jpeg_write_scanlines(&cinfo, &rowPtr, 1);
    }

    jpeg_finish_compress(&cinfo);
    jpeg_destroy_compress(&cinfo);
    mThumbnailBuffer.resize(compressMgr.jpegLength);

    return true;
}

void ExifUtils::updateApp1Segment() {
    /*
     * mSegments[JPEG_SEGMENT_APP1_HEADER] and [JPEG_MARKER_PREFIX] are placeholders prepared by
     * parseJpegSegments().
     */
    if (mSegments.size() >= 3) {
        uint16_t size = mApp1Length + sizeof(size);
        mApp1Header[0] = JPEG_MARKER_PREFIX;
        mApp1Header[1] = JPEG_MARKER_APP1;
        mApp1Header[2] = static_cast<uint8_t>(size >> 8);
        mApp1Header[3] = static_cast<uint8_t>(size & 0xFF);
        mSegments[JPEG_SEGMENT_APP1_HEADER] = {mApp1Header, sizeof(mApp1Header)};
        mSegments[JPEG_SEGMENT_APP1_BUFFER] = {mApp1Buffer, mApp1Length};
    }
}

bool ExifUtils::generateApp1() {
    destroyApp1();
    if (mSegments.size() < 3) {
        ALOGE("%s : the size of |mSegments| is smaller than 3", __func__);
        return false;
    }
    if (mThumbnailRatio) {
        // Generate thumbnail.
        if (!generateThumbnail()) {
            ALOGE("%s : generate thumbnail image failed", __func__);
            return false;
        }
        mExifData->data = mThumbnailBuffer.data();
        mExifData->size = mThumbnailBuffer.size();;
    }
    // Save the result into |mApp1Buffer|.
    exif_data_save_data(mExifData, &mApp1Buffer, &mApp1Length);
    if (!mApp1Length) {
        ALOGE("%s : allocate memory for mApp1Buffer failed", __func__);
        return false;
    }
    /*
     * The JPEG segment size is 16 bits in spec. The size of APP1 segment should be smaller than
     * 65533 because there are two bytes for segment size field.
     */
    if (mApp1Length > 65533) {
        destroyApp1();
        ALOGE("%s : the size of APP1 segment is too large", __func__);
        return false;
    }
    updateApp1Segment();
    return true;
}

void ExifUtils::destroyApp1() {
    /*
     * Since there is no API to access ExifMem in ExifData->priv, we use free here, which is the
     * default free function in libexif. See exif_data_save_data() for detail.
     */
    free(mApp1Buffer);
    mApp1Buffer = nullptr;
    mApp1Length = 0;
    updateApp1Segment();
}

size_t ExifUtils::getTotalLengthFromSegments() {
    size_t result = 0;
    for (const auto& it : mSegments) {
        result += it.length;
    }
    return result;
}

void ExifUtils::copyJpegFromSegments(uint8_t* ptr) {
    size_t offset = 0;
    for (const auto& it : mSegments) {
        memcpy(ptr + offset, it.ptr, it.length);
        offset += it.length;
    }
}

size_t ExifUtils::getOutputJpegLength() {
    if (!generateApp1()) {
        ALOGE("%s : generate APP1 segment failed", __func__);
        return 0;
    }
    return getTotalLengthFromSegments();
}

bool ExifUtils::writeData(uint8_t* outputPtr) {
    if (!outputPtr) {
        ALOGE("%s : the output buffer is NULL", __func__);
        return false;
    }
    if (!mInputPtr) {
        ALOGE("%s : did not set input JPEG image before", __func__);
        return false;
    }
    if (!mApp1Buffer) {
        ALOGE("%s : generate APP1 segment failed", __func__);
        return false;
    }
    copyJpegFromSegments(outputPtr);
    return true;
}

} // end of namespace android
