/*
 * Copyright 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ANDROID_DEVICE_CAMERA_EXIFUTILS_H
#define ANDROID_DEVICE_CAMERA_EXIFUTILS_H

#include <cstddef>
#include <string>
#include <utility>
#include <vector>

extern "C" {
#include <libexif/exif-data.h>
}

namespace android {

/*
 * ExifUtils can add a typical huffman table to a MJPEG image if it doesn't have a huffman table.
 * Caller can set some Exif tags to the image. ExifUtils can add a thumbnail to the input image.
 * The size ratio of thumbnail to the image only supports 1/1, 1/2, 1/4 and 1/8. ExifUtils removes
 * the original APP1 segment. Finally, ExifUtils can write the JPEG image data to the buffer caller
 * assigns to. ExifUtils can be reused with different images by calling initialize().
 * TODO: Support more size ratio of thumbnail size.
 *
 * Example of using this class :
 *  ExifUtils utils;
 *  utils.initialize(inputJpegBuffer, inputJpegLength);
 *  ...
 *  // Call ExifUtils functions to set Exif tags.
 *  ...
 *  size_t outputLength = utils.getOutputJpegLength();
 *  uint8_t* outputBuffer = new uint8_t[outputLength];
 *  utils.writeData(outputBuffer);
 *
 */
class ExifUtils {
public:
    ExifUtils();
    ~ExifUtils();

    /*
     * Sets input JPEG image |buffer| with |length|.
     * The caller should not release |buffer| until writeData or the destructor is called.
     * initialize() can be called multiple times. The setting of Exif tags will be cleared.
     */
    bool initialize(const uint8_t* buffer, const size_t length);

    /*
     * Sets the manufacturer of camera.
     * Returns false if memory allocation fails.
     */
    bool setMaker(const std::string& maker);

    /*
     * Sets the model number of camera.
     * Returns false if memory allocation fails.
     */
    bool setModel(const std::string& model);

    /*
     * Sets the date and time of image last modified. The name of the tag is DateTime in IFD0.
     * Returns false if memory allocation fails.
     */
    bool setDateTime(const struct tm& t);

    /*
     * Sets the width (number of columes) of main image.
     * Returns false if memory allocation fails.
     */
    bool setImageWidth(uint16_t width);

    /*
     * Sets the length (number of rows) of main image.
     * Returns false if memory allocation fails.
     */
    bool setImageLength(uint16_t length);

    /*
     * Sets the focal length of lens used to take the image in millimeters.
     * Returns false if memory allocation fails.
     */
    bool setFocalLength(uint32_t numerator, uint32_t denominator);

    /*
     * Sets the latitude with degrees minutes seconds format.
     * Returns false if memory allocation fails.
     */
    bool setGpsLatitude(double latitude);

    /*
     * Sets the longitude with degrees minutes seconds format.
     * Returns false if memory allocation fails.
     */
    bool setGpsLongitude(double longitude);

    /*
     * Sets the altitude in meters.
     * Returns false if memory allocation fails.
     */
    bool setGpsAltitude(double altitude);

    /*
     * Sets GPS date stamp and time stamp (atomic clock).
     * Returns false if memory allocation fails.
     */
    bool setGpsTimestamp(const struct tm& t);

    /*
     * Sets GPS processing method.
     * Returns false if memory allocation fails.
     */
    bool setGpsProcessingMethod(const std::string& method);

    /*
     * Sets the size ratio of thumbnail to the input Jpeg image. Only 1/1, 1/2, 1/4 and 1/8 are
     * supported. Other ratio will be adjusted up to a supported ratio. Ratio greater than 1 will
     * be adjusted to 1. For example, 1/3 will be adjusted to 1/2.
     * Since the size of APP1 segment is limited, it is recommended the resolution of thumbnail is
     * equal to or smaller than 640x480. If the thumbnail is too big, getOutputJpegLength() will
     * return 0.
     */
    void setThumbnailSizeRatio(double ratio);

    // Returns the length of output JPEG image. Returns 0 if something went wrong.
    size_t getOutputJpegLength();

    /*
     * Copies the output image to |outputPtr|.
     * The caller should guarantee the size of |outputPtr| is at least getOutputJpegLength().
     */
    bool writeData(uint8_t* outputPtr);

private:
    struct Segment {
        const uint8_t* ptr;
        size_t length;
    };

    // Resets the pointers and memories.
    void reset();

    /*
     * Parses JPEG image and save the results in |mSegments|.
     * It will add default huffman segments to |mSegments| if no huffman segment exists.
     * Returns false if JPEG is invalid.
     */
    bool parseJpegSegments();

    /*
     * Adds a variable length tag to |mExifData|. It will remove the original one if the tag exists.
     * Returns the entry of the tag. The reference count of returned ExifEntry is two.
     */
    std::unique_ptr<ExifEntry> addVariableLengthEntry(ExifIfd ifd, ExifTag tag,
                                                      ExifFormat format,
                                                      unsigned long components,
                                                      unsigned int size);

    /*
     * Adds a entry of |tag| in |mExifData|. It won't remove the original one if the tag exists.
     * Returns the entry of the tag. It adds one reference count to returned ExifEntry.
     */
    std::unique_ptr<ExifEntry> addEntry(ExifIfd ifd, ExifTag tag);

    /*
     * Generates a thumbnail and save the result in |mThumbnailBuffer|.
     * Returns false if compress/decompress image failed.
     */
    bool generateThumbnail();

    // Updates the informations of APP1 segment in |mSegments|.
    void updateApp1Segment();

    // Generates data of APP1 segment if needed. Returns false if memory allocation fails.
    bool generateApp1();

    // Destroys the buffer of APP1 segment if exists.
    void destroyApp1();

    // Returns the total length of data in |mSegments|.
    size_t getTotalLengthFromSegments();

    // Copy the image data from |mSegments|.
    void copyJpegFromSegments(uint8_t* ptr);

    // The size ratio of thumbnail.
    double mThumbnailRatio;
    // The buffer for thumbnail.
    std::vector<uint8_t> mThumbnailBuffer;
    // The buffer for input JPEG image with Huffman table.
    std::vector<uint8_t> mJpegBuffer;
    // The buffer for image decompressing.
    std::vector<uint8_t> mYuvBuffer;
    // The Exif data (APP1). Owned by this class.
    ExifData* mExifData;
    // The header of APP1 segments.
    uint8_t mApp1Header[4];
    // The raw data of APP1 segment. It's allocated by ExifMem in mExifData but owned by this class.
    uint8_t* mApp1Buffer;
    // The length of |mApp1Buffer|.
    unsigned int mApp1Length;
    // The address of input JPEG image. Owned by caller.
    const uint8_t* mInputPtr;
    // The length of input JPEG image.
    size_t mInputLength;
    // The (addresses, length) pairs of JPEG segments.
    std::vector<Segment> mSegments;
    // Denotes whether the input JPEG image has a Huffman table.
    bool mInputHasMarkerDht;
};

} // end of namespace android

#endif // ANDROID_DEVICE_CAMERA_EXIFUTILS_H
