LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_ADDITIONAL_DEPENDENCIES := $(LOCAL_PATH)/Android.mk

LOCAL_SHARED_LIBRARIES := \
    liblog \
    libcommon_exifutils \
    libjpeg \
    libexif

LOCAL_SRC_FILES := \
    ExifUtils_test.cpp

LOCAL_CFLAGS += -fno-short-enums

LOCAL_MODULE := ExifUtils_test

LOCAL_MODULE_TAGS := tests

include $(BUILD_NATIVE_TEST)
