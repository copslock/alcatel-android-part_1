/*
 * Copyright 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define LOG_TAG "ExifUtils_test"

#include <ExifUtils.h>
#include <gtest/gtest.h>
#include <utils/Log.h>

#include <cmath>
#include <cstddef>
#include <cstdlib>
#include <fcntl.h>
#include <setjmp.h>
#include <sys/stat.h>
#include <unistd.h>

extern "C" {
#include "jpeglib.h"
}

namespace android {

#define IMAGE_WITH_DHT_FILENAME "peach_pi-1280x720.jpg"
#define IMAGE_WITHOUT_DHT_FILENAME "pixel-1280x720.jpg"
#define INVALID_IMAGE_LENGTH 514

// This comes from the Exif Version 2.3 standard table 9.
const uint8_t gExifAsciiPrefix[] = { 0x41, 0x53, 0x43, 0x49, 0x49, 0x0, 0x0, 0x0 };

struct libjpeg_source_mgr : jpeg_source_mgr {
    libjpeg_source_mgr(const uint8_t* ptr, int len);
    ~libjpeg_source_mgr();

    const uint8_t* mBufferPtr;
    size_t mBufferLength;
};

struct my_error_mgr {
    struct jpeg_error_mgr pub;
    jmp_buf setjmp_buffer;
};

static void libjpeg_init_source(j_decompress_ptr cinfo) {
    libjpeg_source_mgr* src = static_cast<libjpeg_source_mgr*>(cinfo->src);
    src->next_input_byte = static_cast<const JOCTET*>(src->mBufferPtr);
    src->bytes_in_buffer = src->mBufferLength;
}

static boolean libjpeg_fill_input_buffer(j_decompress_ptr /* cinfo */) {
    ALOGE("%s : should not get here", __func__);
    return FALSE;
}

static void libjpeg_skip_input_data(j_decompress_ptr cinfo, long num_bytes) {
    libjpeg_source_mgr* src = static_cast<libjpeg_source_mgr*>(cinfo->src);

    if (num_bytes > static_cast<long>(src->bytes_in_buffer)) {
        FAIL() << __func__ << ": libjpeg_skip_input_data - num_bytes > (long)src->bytes_in_buffer";
    } else {
        src->next_input_byte += num_bytes;
        src->bytes_in_buffer -= num_bytes;
    }
}

static void libjpeg_term_source(j_decompress_ptr /*cinfo*/) {}

libjpeg_source_mgr::libjpeg_source_mgr(const uint8_t* ptr, int len) :
        mBufferPtr(ptr), mBufferLength(len) {
    init_source = libjpeg_init_source;
    fill_input_buffer = libjpeg_fill_input_buffer;
    skip_input_data = libjpeg_skip_input_data;
    resync_to_restart = jpeg_resync_to_restart;
    term_source = libjpeg_term_source;
}

libjpeg_source_mgr::~libjpeg_source_mgr() {}

static void my_error_exit(j_common_ptr cinfo) {
    my_error_mgr* err = reinterpret_cast<my_error_mgr*>(cinfo->err);
    longjmp(err->setjmp_buffer, 1);
}

class ExifUtilsTest : public testing::Test {
public:
    struct Image {
        std::unique_ptr<uint8_t[]> buffer;
        size_t length;
    };
    ExifUtilsTest();
    ~ExifUtilsTest();

protected:
    virtual void SetUp();
    virtual void TearDown();
    void testGpsExifValues(double latitude, double longitude, double altitude, time_t timestamp,
                           const std::string& method);

    Image mImageWithDht, mImageWithoutDht, mInvalidImage;
};

static size_t getFileSize(int fd) {
    struct stat st;
    if (fstat(fd, &st) < 0) {
        ALOGW("%s : fstat failed", __func__);
        return 0;
    }
    return st.st_size; // bytes
}

static bool LoadFile(const char filename[], ExifUtilsTest::Image* result) {
    int fd = open(filename, O_RDONLY, 0644);
    if (fd < 0) {
        return false;
    }
    result->length = getFileSize(fd);
    if (result->length == 0) {
        close(fd);
        return false;
    }
    result->buffer.reset(new uint8_t[result->length]);
    if (read(fd, result->buffer.get(), result->length) != static_cast<ssize_t>(result->length)) {
        close(fd);
        return false;
    }
    close(fd);
    return true;
}

static double dmsFormat2Decimal(ExifRational degrees, ExifRational minutes, ExifRational seconds) {
    return (1.0 * degrees.numerator / degrees.denominator +
            1.0 * minutes.numerator / minutes.denominator / 60.0 +
            1.0 * seconds.numerator / seconds.denominator / 3600.0);
}

static ExifShort getExifEntryShort(const ExifData* exifData, ExifIfd ifd, ExifTag tag) {
    ExifEntry* entry = exif_content_get_entry(exifData->ifd[ifd], tag);
    if (!entry || entry->size != sizeof(ExifShort)) {
        return 0;
    }
    return exif_get_short(entry->data, EXIF_BYTE_ORDER_INTEL);
}

static std::string getExifEntryString(const ExifData* exifData, ExifIfd ifd, ExifTag tag) {
    ExifEntry* entry = exif_content_get_entry(exifData->ifd[ifd], tag);
    if (!entry || !entry->size || entry->data[entry->size - 1]) {
        return std::string();
    }
    return std::string(reinterpret_cast<const char*>(entry->data), entry->size - 1);
}

static double getExifEntryRational(const ExifData* exifData, ExifIfd ifd, ExifTag tag) {
    ExifEntry* entry = exif_content_get_entry(exifData->ifd[ifd], tag);
    if (!entry || entry->size != sizeof(ExifRational)) {
        return NAN;
    }
    ExifRational r = exif_get_rational(entry->data, EXIF_BYTE_ORDER_INTEL);
    return 1.0 * r.numerator / r.denominator;
}

static double getExifEntryThreeRational(const ExifData* exifData, ExifIfd ifd, ExifTag tag) {
    ExifEntry* entry = exif_content_get_entry(exifData->ifd[ifd], tag);
    if (!entry || entry->size != sizeof(ExifRational) * 3u) {
        return NAN;
    }
    ExifRational degrees = exif_get_rational(entry->data, EXIF_BYTE_ORDER_INTEL);
    ExifRational minutes =
        exif_get_rational(entry->data + sizeof(ExifRational), EXIF_BYTE_ORDER_INTEL);
    ExifRational seconds =
        exif_get_rational(entry->data + 2 * sizeof(ExifRational), EXIF_BYTE_ORDER_INTEL);
    return dmsFormat2Decimal(degrees, minutes, seconds);
}

static bool decompressJpegImage(const uint8_t* inputPtr, const size_t inputLength,
                                uint8_t** outputPtr, size_t* outputLength) {
    *outputPtr = nullptr;
    *outputLength = 0;
    if (!inputPtr) {
        return false;
    }
    struct jpeg_decompress_struct cinfo;
    struct libjpeg_source_mgr mgr(inputPtr, inputLength);
    struct my_error_mgr myerr;
    cinfo.err = jpeg_std_error(&myerr.pub);
    myerr.pub.error_exit = my_error_exit;
    if (setjmp(myerr.setjmp_buffer)) {
        jpeg_destroy_decompress(&cinfo);
        return false;
    }
    jpeg_create_decompress(&cinfo);

    cinfo.src = &mgr;
    jpeg_read_header(&cinfo, TRUE);
    jpeg_start_decompress(&cinfo);

    size_t rowStride = cinfo.output_width * cinfo.output_components;
    *outputPtr = new uint8_t[rowStride * cinfo.output_height];
    *outputLength = 0;
    while (cinfo.output_scanline < cinfo.output_height) {
        unsigned char* rowPtr = *outputPtr + *outputLength;
        if (jpeg_read_scanlines(&cinfo, &rowPtr, 1)) {
            *outputLength += rowStride;
        }
    }

    jpeg_finish_decompress(&cinfo);
    jpeg_destroy_decompress(&cinfo);

    return true;
}

ExifUtilsTest::ExifUtilsTest() {}

ExifUtilsTest::~ExifUtilsTest() {}

void ExifUtilsTest::SetUp() {
    if (!LoadFile(IMAGE_WITH_DHT_FILENAME, &mImageWithDht)) {
        FAIL() << "Load file " << IMAGE_WITH_DHT_FILENAME << " failed";
    }
    if (!LoadFile(IMAGE_WITHOUT_DHT_FILENAME, &mImageWithoutDht)) {
        FAIL() << "Load file " << IMAGE_WITHOUT_DHT_FILENAME << " failed";
    }
    mInvalidImage.buffer.reset(new uint8_t[INVALID_IMAGE_LENGTH]);
    mInvalidImage.length = INVALID_IMAGE_LENGTH;
    memset(mInvalidImage.buffer.get(), 0, mInvalidImage.length);
}

void ExifUtilsTest::TearDown() {}

void ExifUtilsTest::testGpsExifValues(double latitude, double longitude, double altitude,
                                      time_t timestamp, const std::string& processingMethod) {
    const auto kRationalSize = exif_format_get_size(EXIF_FORMAT_RATIONAL);
    ExifUtils imageTool;
    EXPECT_TRUE(imageTool.initialize(mImageWithoutDht.buffer.get(), mImageWithoutDht.length));
    EXPECT_TRUE(imageTool.setGpsLatitude(latitude));
    EXPECT_TRUE(imageTool.setGpsLongitude(longitude));
    EXPECT_TRUE(imageTool.setGpsAltitude(altitude));
    struct tm tm;
    localtime_r(&timestamp, &tm);
    EXPECT_TRUE(imageTool.setGpsTimestamp(tm));
    EXPECT_TRUE(imageTool.setGpsProcessingMethod(processingMethod));

    size_t bufferLength = imageTool.getOutputJpegLength();
    std::unique_ptr<uint8_t[]> buffer(new uint8_t[bufferLength]);
    EXPECT_TRUE(imageTool.writeData(buffer.get()));

    // Check whether exif GPS tags are correct.
    ExifData* exifData = exif_data_new_from_data(buffer.get(), bufferLength);

    // Check latitude.
    ExifTag tag = static_cast<ExifTag>(EXIF_TAG_GPS_LATITUDE_REF);
    EXPECT_STREQ(latitude >= 0 ? "N" : "S",
                 getExifEntryString(exifData, EXIF_IFD_GPS, tag).c_str());
    tag = static_cast<ExifTag>(EXIF_TAG_GPS_LATITUDE);
    EXPECT_NEAR(fabs(latitude), getExifEntryThreeRational(exifData, EXIF_IFD_GPS, tag), 1e-8);

    // Check longitude.
    tag = static_cast<ExifTag>(EXIF_TAG_GPS_LONGITUDE_REF);
    EXPECT_STREQ(longitude >= 0 ? "E" : "W",
                 getExifEntryString(exifData, EXIF_IFD_GPS, tag).c_str());
    tag = static_cast<ExifTag>(EXIF_TAG_GPS_LONGITUDE);
    EXPECT_NEAR(fabs(longitude), getExifEntryThreeRational(exifData, EXIF_IFD_GPS, tag), 1e-8);

    // Check altitude.
    tag = static_cast<ExifTag>(EXIF_TAG_GPS_ALTITUDE_REF);
    ExifEntry* entry = exif_content_get_entry(exifData->ifd[EXIF_IFD_GPS], tag);
    ASSERT_NE(nullptr, entry);
    ASSERT_EQ(1u, entry->size);
    EXPECT_EQ(altitude >= 0 ? 0 : 1, entry->data[0]);
    tag = static_cast<ExifTag>(EXIF_TAG_GPS_ALTITUDE);
    EXPECT_NEAR(fabs(altitude), getExifEntryRational(exifData, EXIF_IFD_GPS, tag), 1e-3);

    // Check date stamp.
    tag = static_cast<ExifTag>(EXIF_TAG_GPS_DATE_STAMP);
    char date[11];
    EXPECT_EQ(10, snprintf(date, 11, "%04d:%02d:%02d",
                           tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday));
    EXPECT_STREQ(date, getExifEntryString(exifData, EXIF_IFD_GPS, tag).c_str());

    // Check time stamp.
    tag = static_cast<ExifTag>(EXIF_TAG_GPS_TIME_STAMP);
    double actualTime = getExifEntryThreeRational(exifData, EXIF_IFD_GPS, tag);
    double expectedTime = dmsFormat2Decimal({static_cast<ExifLong>(tm.tm_hour), 1u},
                                            {static_cast<ExifLong>(tm.tm_min), 1u},
                                            {static_cast<ExifLong>(tm.tm_sec), 1u});
    EXPECT_NEAR(expectedTime, actualTime, 1e-12);

    // Check processing method.
    tag = static_cast<ExifTag>(EXIF_TAG_GPS_PROCESSING_METHOD);
    entry = exif_content_get_entry(exifData->ifd[EXIF_IFD_GPS], tag);
    ASSERT_NE(nullptr, entry);
    ASSERT_EQ(sizeof(gExifAsciiPrefix) + processingMethod.length(), entry->size);
    EXPECT_EQ(0, memcmp(entry->data, gExifAsciiPrefix, sizeof(gExifAsciiPrefix)));
    EXPECT_EQ(0, memcmp(entry->data + sizeof(gExifAsciiPrefix), processingMethod.c_str(),
                        processingMethod.length()));

    exif_data_unref(exifData);
}

TEST_F(ExifUtilsTest, InvalidFile) {
    ExifUtils imageTool;
    EXPECT_FALSE(imageTool.initialize(mImageWithDht.buffer.get(), 1));
    EXPECT_FALSE(imageTool.initialize(mInvalidImage.buffer.get(), mInvalidImage.length));
}

TEST_F(ExifUtilsTest, InputWithHuffmanTable) {
    ExifUtils imageTool;
    EXPECT_TRUE(imageTool.initialize(mImageWithDht.buffer.get(), mImageWithDht.length));
    size_t bufferLength = imageTool.getOutputJpegLength();
    std::unique_ptr<uint8_t[]> buffer(new uint8_t[bufferLength]);
    EXPECT_TRUE(imageTool.writeData(buffer.get()));
    // Compare the original image and the image returned by ExifUtils. They should be the same.
    uint8_t* originalRawImage;
    size_t originalRawImageLength;
    ASSERT_TRUE(decompressJpegImage(mImageWithDht.buffer.get(), mImageWithDht.length,
                                    &originalRawImage, &originalRawImageLength));
    uint8_t* newRawImage;
    size_t newRawImageLength;
    ASSERT_TRUE(decompressJpegImage(buffer.get(), bufferLength, &newRawImage, &newRawImageLength));
    ASSERT_EQ(originalRawImageLength, newRawImageLength);
    EXPECT_EQ(0, memcmp(originalRawImage, newRawImage, originalRawImageLength));
    delete [] originalRawImage;
    delete [] newRawImage;
}

TEST_F(ExifUtilsTest, InputWithoutHuffmanTable) {
    ExifUtils imageTool;
    EXPECT_TRUE(imageTool.initialize(mImageWithoutDht.buffer.get(), mImageWithoutDht.length));
    size_t bufferLength = imageTool.getOutputJpegLength();
    std::unique_ptr<uint8_t[]> buffer(new uint8_t[bufferLength]);
    EXPECT_TRUE(imageTool.writeData(buffer.get()));

    // Try to decode the output Jpeg. It will fail if the Huffman table segment didn't exist.
    uint8_t* result;
    size_t resultLength;
    ASSERT_FALSE(decompressJpegImage(mImageWithoutDht.buffer.get(), mImageWithoutDht.length,
                                     &result, &resultLength));
    EXPECT_TRUE(decompressJpegImage(buffer.get(), bufferLength, &result, &resultLength));
    delete [] result;
}

TEST_F(ExifUtilsTest, TestExifTags) {
    ExifUtils imageTool;
    std::vector<const Image*> tests;
    tests.push_back(&mImageWithDht);
    tests.push_back(&mImageWithoutDht);
    tests.push_back(&mImageWithDht);
    for (size_t i = 0; i < tests.size(); i++) {
        std::string maker = std::string("Google_") + std::to_string(i);
        std::string model = std::to_string(i) + std::string("_HanHan12358");
        ExifShort width = 1280 + i;
        ExifShort height = 720 + i;
        double focalLength = 3.14159265359 + i;
        time_t seconds = 1199145600 + i;
        struct tm tm;
        localtime_r(&seconds, &tm);
        EXPECT_TRUE(imageTool.initialize(tests[i]->buffer.get(), tests[i]->length));
        EXPECT_TRUE(imageTool.setDateTime(tm));
        EXPECT_TRUE(imageTool.setMaker(maker));
        EXPECT_TRUE(imageTool.setModel(model));
        EXPECT_TRUE(imageTool.setImageWidth(width));
        EXPECT_TRUE(imageTool.setImageLength(height));
        EXPECT_TRUE(imageTool.setFocalLength(focalLength*1000000, 1000000));
        EXPECT_NE(0u, imageTool.getOutputJpegLength());
        imageTool.setThumbnailSizeRatio(0.25);

        size_t bufferLength = imageTool.getOutputJpegLength();
        std::unique_ptr<uint8_t[]> buffer(new uint8_t[bufferLength]);
        EXPECT_TRUE(imageTool.writeData(buffer.get()));
        // Check whether exif tags are correct.
        ExifData* exifData = exif_data_new_from_data(buffer.get(), bufferLength);
        uint8_t* result;
        size_t resultLength;
        EXPECT_TRUE(decompressJpegImage(exifData->data, exifData->size, &result, &resultLength));
        delete [] result;
        EXPECT_EQ(maker, getExifEntryString(exifData, EXIF_IFD_0, EXIF_TAG_MAKE));
        EXPECT_EQ(model, getExifEntryString(exifData, EXIF_IFD_0, EXIF_TAG_MODEL));
        char expectedTime[20];
        EXPECT_EQ(static_cast<int>(sizeof(expectedTime)) - 1,
                  snprintf(expectedTime, sizeof(expectedTime), "%04i:%02i:%02i %02i:%02i:%02i",
                           tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min,
                           tm.tm_sec));
        EXPECT_STREQ(expectedTime,
                     getExifEntryString(exifData, EXIF_IFD_0, EXIF_TAG_DATE_TIME).c_str());
        EXPECT_EQ(width, getExifEntryShort(exifData, EXIF_IFD_0, EXIF_TAG_IMAGE_WIDTH));
        EXPECT_EQ(height, getExifEntryShort(exifData, EXIF_IFD_0, EXIF_TAG_IMAGE_LENGTH));
        EXPECT_NEAR(focalLength,
                    getExifEntryRational(exifData, EXIF_IFD_EXIF, EXIF_TAG_FOCAL_LENGTH), 1e-6);

        exif_data_unref(exifData);
    }
}

TEST_F(ExifUtilsTest, TestGpsExif) {
    // Test negative altitude.
    testGpsExifValues(-37.736071, -122.441983, -21.514, 1199145600, "GPS");
    /*
     * The parameters below are from
     * /cts/tests/tests/hardware/src/android/hardware/cts/CameraTest.java
     */
    testGpsExifValues(37.736071, -122.441983, 21, 1199145600, "GPS NETWORK HYBRID ARE ALL FINE.");
    testGpsExifValues(0.736071, 0.441983, 1, 1199145601, "GPS");
    testGpsExifValues(-89.736071, -179.441983, 100000, 1199145602, "NETWORK");
}

TEST_F(ExifUtilsTest, ThumbnailTooBig) {
    ASSERT_GT(mImageWithoutDht.length, 65533u);
    ExifUtils imageTool;
    EXPECT_TRUE(imageTool.initialize(mImageWithoutDht.buffer.get(), mImageWithoutDht.length));
    imageTool.setThumbnailSizeRatio(1);
    EXPECT_EQ(0u, imageTool.getOutputJpegLength());
}

} // end of namespace android

