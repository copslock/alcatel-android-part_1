LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_SRC_FILES := healthd_board_msm.cpp healthd_msm_alarm.cpp
LOCAL_MODULE := libhealthd.msm
LOCAL_CFLAGS := -Werror
LOCAL_C_INCLUDES := system/core/healthd/include/healthd/ bootable/recovery

# MODIFIED-BEGIN by li jiang, 2016-08-22,BUG-2779963
ifeq ($(TCT_TARGET_FEATURE_SET_POWER_OFF_LED),true)
LOCAL_CFLAGS    += -DFEATURE_TCTSH_SET_POWER_OFF_LED
endif
# MODIFIED-END by li jiang,BUG-2779963

include $(BUILD_STATIC_LIBRARY)
