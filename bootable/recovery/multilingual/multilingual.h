/* Copyright (C) 2016 Tcl Corporation Limited */
//define and declare

__BEGIN_DECLS

enum THE_LANGUAGES {
	EN,
	ZH_CN,
	DE,
	ES,
	JA,
	FR,
	PT,
	RU,
};


struct ml_strings {
	int lang;
	char name[16];
	const char **header;
	const char **menu;
};

int ml_string_fetch(char *name, char*** header, char*** menu);
int ml_select_language(int i);

extern char *g_ml_str_hint[];

__END_DECLS
