#!/bin/bash
IFS=' '
RECOVERYDIR=..
BUILDDIR=build
RECOVERYICONDIR=icon_recovery
GENFONTDIR=$BUILDDIR/recovery_fonts
mdpi=0
hdpi=0
xhdpi=0
xxhdpi=0
xxxhdpi=0

echo "Do you want copy the fonts tar files to recovery resource folder( recovery/res-*dpi ) now?"
read -p "* Input \"all\" or input one or more tag like \"mdpi\" \"hdpi\" \"xhdpi\" \"xxhdpi\" \"xxxhdpi\" to \
copy the fonts tar files to the recovery resource folder. [none]" opt

if [ ! -d $BUILDDIR ]; then
    mkdir $BUILDDIR
fi

if [ -d $GENFONTDIR ]; then
    rm -rf $GENFONTDIR
fi
mkdir $GENFONTDIR $GENFONTDIR/mdpi $GENFONTDIR/hdpi $GENFONTDIR/xhdpi $GENFONTDIR/xxhdpi $GENFONTDIR/xxxhdpi \
        $GENFONTDIR/mdpi/recovery_fonts $GENFONTDIR/hdpi/recovery_fonts $GENFONTDIR/xhdpi/recovery_fonts \
        $GENFONTDIR/xxhdpi/recovery_fonts $GENFONTDIR/xxxhdpi/recovery_fonts

if [ -f $BUILDDIR/recovery_fonts_mdpi.tar ]; then
    rm $BUILDDIR/recovery_fonts_mdpi.tar
fi
if [ -f $BUILDDIR/recovery_fonts_hdpi.tar ]; then
    rm $BUILDDIR/recovery_fonts_hdpi.tar
fi
if [ -f $BUILDDIR/recovery_fonts_xhdpi.tar ]; then
    rm $BUILDDIR/recovery_fonts_xhdpi.tar
fi
if [ -f $BUILDDIR/recovery_fonts_xxhdpi.tar ]; then
    rm $BUILDDIR/recovery_fonts_xxhdpi.tar
fi
if [ -f $BUILDDIR/recovery_fonts_xxxhdpi.tar ]; then
    rm $BUILDDIR/recovery_fonts_xxxhdpi.tar
fi
if [ -f $BUILDDIR/ml_string.h ]; then
    rm $BUILDDIR/ml_string.h
fi
./z.py > $BUILDDIR/ml_string.h

# tar fonts to tar files
tar -cf $BUILDDIR/recovery_fonts_mdpi.tar -C $GENFONTDIR/mdpi recovery_fonts/
echo "tar $GENFONTDIR/mdpi    -----> $BUILDDIR/recovery_fonts_mdpi.tar"
tar -cf $BUILDDIR/recovery_fonts_hdpi.tar -C $GENFONTDIR/hdpi recovery_fonts/
echo "tar $GENFONTDIR/hdpi    -----> $BUILDDIR/recovery_fonts_hdpi.tar"
tar -cf $BUILDDIR/recovery_fonts_xhdpi.tar -C $GENFONTDIR/xhdpi recovery_fonts/
echo "tar $GENFONTDIR/xhdpi   -----> $BUILDDIR/recovery_fonts_xhdpi.tar"
tar -cf $BUILDDIR/recovery_fonts_xxhdpi.tar -C $GENFONTDIR/xxhdpi recovery_fonts/
echo "tar $GENFONTDIR/xxhdpi  -----> $BUILDDIR/recovery_fonts_xxhdpi.tar"
tar -cf $BUILDDIR/recovery_fonts_xxxhdpi.tar -C $GENFONTDIR/xxxhdpi recovery_fonts/
echo "tar $GENFONTDIR/xxxhdpi -----> $BUILDDIR/recovery_fonts_xxxhdpi.tar"

# copy fonts tar files to res folder
if [ ! -n "$opt" ];then
    opt="none"
fi

for s in ${opt[@]}
do
    case "$s" in
        mdpi )
            mdpi=1;;
        hdpi )
            hdpi=1;;
        xhdpi )
            xhdpi=1;;
        xxhdpi )
            xxhdpi=1;;
        xxxhdpi )
            xxxhdpi=1;;
        all )
            mdpi=1
            hdpi=1
            xhdpi=1
            xxhdpi=1
            xxxhdpi=1;;
        none )
            mdpi=0
            hdpi=0
            xhdpi=0
            xxhdpi=0
            xxxhdpi=0
            break;;
        * )
            mdpi=0
            hdpi=0
            xhdpi=0
            xxhdpi=0
            xxxhdpi=0
            echo "can't identify $s"
            exit -1;;
    esac
done

if [ $[$mdpi+$hdpi+$xhdpi+$xxhdpi+$xxxhdpi] -gt 0 ] && [ -f "$RECOVERYDIR/multilingual/ml_string.h" ];then
    cp $BUILDDIR/ml_string.h $RECOVERYDIR/multilingual/ml_string.h
    echo "copy $BUILDDIR/ml_string.h -----> $RECOVERYDIR/multilingual/ml_string.h"
fi

if [ "$mdpi" == "1" ]; then
    if [ -d "$RECOVERYDIR/res-mdpi" ]; then
        if [ ! -d "$RECOVERYDIR/res-mdpi/fonts" ]; then
            mkdir $RECOVERYDIR/res-mdpi/fonts
        fi
        cp $BUILDDIR/recovery_fonts_mdpi.tar $RECOVERYDIR/res-mdpi/fonts/recovery_fonts.tar
        echo "copy $BUILDDIR/recovery_fonts_mdpi.tar    -----> $RECOVERYDIR/res-mdpi/fonts/recovery_fonts.tar"
        if [ ! -f "$RECOVERYDIR/res-mdpi/images/icon_recovery.png" ]; then
            cp $RECOVERYICONDIR/icon_recovery_mdpi.png $RECOVERYDIR/res-mdpi/images/icon_recovery.png
            echo "copy $RECOVERYICONDIR/icon_recovery_mdpi.png -----> $RECOVERYDIR/res-mdpi/images/icon_recovery.png"
        fi
    else echo "there is not $RECOVERYDIR/res-mdpi"
    fi
elif [ -d "$RECOVERYDIR/res-mdpi/fonts" ]; then
    rm -rf $RECOVERYDIR/res-mdpi/fonts
fi

if [ "$hdpi" == "1" ]; then
    if [ -d "$RECOVERYDIR/res-hdpi" ]; then
        if [ ! -d "$RECOVERYDIR/res-hdpi/fonts" ]; then
            mkdir $RECOVERYDIR/res-hdpi/fonts
        fi
        cp $BUILDDIR/recovery_fonts_hdpi.tar $RECOVERYDIR/res-hdpi/fonts/recovery_fonts.tar
        echo "copy $BUILDDIR/recovery_fonts_hdpi.tar    -----> $RECOVERYDIR/res-hdpi/fonts/recovery_fonts.tar"
        if [ ! -f "$RECOVERYDIR/res-hdpi/images/icon_recovery.png" ]; then
            cp $RECOVERYICONDIR/icon_recovery_hdpi.png $RECOVERYDIR/res-hdpi/images/icon_recovery.png
            echo "copy $RECOVERYICONDIR/icon_recovery_hdpi.png -----> $RECOVERYDIR/res-hdpi/images/icon_recovery.png"
        fi
    else echo "there is not $RECOVERYDIR/res-hdpi"
    fi
elif [ -d "$RECOVERYDIR/res-hdpi/fonts" ]; then
    rm -rf $RECOVERYDIR/res-hdpi/fonts
fi

if [ "$xhdpi" == "1" ]; then
    if [ -d "$RECOVERYDIR/res-xhdpi" ]; then
        if [ ! -d "$RECOVERYDIR/res-xhdpi/fonts" ]; then
            mkdir $RECOVERYDIR/res-xhdpi/fonts
        fi
        cp $BUILDDIR/recovery_fonts_xhdpi.tar $RECOVERYDIR/res-xhdpi/fonts/recovery_fonts.tar
        echo "copy $BUILDDIR/recovery_fonts_xhdpi.tar   -----> $RECOVERYDIR/res-xhdpi/fonts/recovery_fonts.tar"
        if [ ! -f "$RECOVERYDIR/res-xhdpi/images/icon_recovery.png" ]; then
            cp $RECOVERYICONDIR/icon_recovery_xhdpi.png $RECOVERYDIR/res-xhdpi/images/icon_recovery.png
            echo "copy $RECOVERYICONDIR/icon_recovery_xhdpi.png -----> $RECOVERYDIR/res-xhdpi/images/icon_recovery.png"
        fi
    else echo "there is not $RECOVERYDIR/res-xhdpi"
    fi
elif [ -d "$RECOVERYDIR/res-xhdpi/fonts" ]; then
    rm -rf $RECOVERYDIR/res-xhdpi/fonts
fi

if [ "$xxhdpi" == "1" ]; then
    if [ -d "$RECOVERYDIR/res-xxhdpi" ]; then
        if [ ! -d "$RECOVERYDIR/res-xxhdpi/fonts" ]; then
            mkdir $RECOVERYDIR/res-xxhdpi/fonts
        fi
        cp $BUILDDIR/recovery_fonts_xxhdpi.tar $RECOVERYDIR/res-xxhdpi/fonts/recovery_fonts.tar
        echo "copy $BUILDDIR/recovery_fonts_xxhdpi.tar  -----> $RECOVERYDIR/res-xxhdpi/fonts/recovery_fonts.tar"
        if [ ! -f "$RECOVERYDIR/res-xxhdpi/images/icon_recovery.png" ]; then
            cp $RECOVERYICONDIR/icon_recovery_xxhdpi.png $RECOVERYDIR/res-xxhdpi/images/icon_recovery.png
            echo "copy $RECOVERYICONDIR/icon_recovery_xxhdpi.png -----> $RECOVERYDIR/res-xxhdpi/images/icon_recovery.png"
        fi
    else echo "there is not $RECOVERYDIR/res-xxhdpi"
    fi
elif [ -d "$RECOVERYDIR/res-xxhdpi/fonts" ]; then
    rm -rf $RECOVERYDIR/res-xxhdpi/fonts
fi

if [ "$xxxhdpi" == "1" ]; then
    if [ -d "$RECOVERYDIR/res-xxxhdpi" ]; then
        if [ ! -d "$RECOVERYDIR/res-xxxhdpi/fonts" ]; then
            mkdir $RECOVERYDIR/res-xxxhdpi/fonts
        fi
        cp $BUILDDIR/recovery_fonts_xxxhdpi.tar $RECOVERYDIR/res-xxxhdpi/fonts/recovery_fonts.tar
        echo "copy $BUILDDIR/recovery_fonts_xxxhdpi.tar -----> $RECOVERYDIR/res-xxxhdpi/fonts/recovery_fonts.tar"
        if [ ! -f "$RECOVERYDIR/res-xxxhdpi/images/icon_recovery.png" ]; then
            cp $RECOVERYICONDIR/icon_recovery_xxxhdpi.png $RECOVERYDIR/res-xxxhdpi/images/icon_recovery.png
            echo "copy $RECOVERYICONDIR/icon_recovery_xxxhdpi.png -----> $RECOVERYDIR/res-xxxhdpi/images/icon_recovery.png"
        fi
    else echo "there is not $RECOVERYDIR/res-xxxhdpi"
    fi
elif [ -d "$RECOVERYDIR/res-xxxhdpi/fonts" ]; then
    rm -rf $RECOVERYDIR/res-xxxhdpi/fonts
fi

exit 0
