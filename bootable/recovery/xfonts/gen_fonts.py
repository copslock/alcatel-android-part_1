#!/usr/bin/python
# encoding=utf8

import sys
import commands

g_dict = {}

## for special chars such as "/".
def handle_special(cc):
    cfile = cc
    special = {'*':"asterisk", '?':"question", '/':"slash"}
    s2 = ('>', '<', ')', '(', '\'', '\"', '\\', '`', '%', '#', ' ', '+', '~', '|', '&', ';')
    if cc in special:
        cfile = special[cc]
        cc = "\\" + cc
    elif cc in s2:
        cc = "\\" + cc
        cfile = cc

    return (cc, cfile)

def gen_pgm(c):
    global g_dict
    if c in g_dict:
        return 0
    else:
        g_dict[c] = 1

    (cc, cf) = handle_special(c)

    ##TODO: configure.
    font = "WenQuanYiMicroHeiMono.ttf"
    ##font = "wqy-microhei.ttc"
    mfdir = "./build/recovery_fonts/mdpi/recovery_fonts"
    hfdir = "./build/recovery_fonts/hdpi/recovery_fonts"
    xhfdir = "./build/recovery_fonts/xhdpi/recovery_fonts"
    xxhfdir = "./build/recovery_fonts/xxhdpi/recovery_fonts"
    xxxhfdir = "./build/recovery_fonts/xxxhdpi/recovery_fonts"
    ftmp = "/tmp/recovery_font_tmp"

    cmd = u"echo %s > %s" % (cc, ftmp)
    commands.getoutput(cmd.encode("utf-8"))
    #o = commands.getoutput("xxd %s" % ftmp)
    #print o

    # ldpi      DPI = 120 Density = 0.8 PNG_Height = 160
    # mdpi      DPI = 160 Density = 1   PNG_Height = 200
    # hdpi      DPI = 240 Density = 1.5 PNG_Height = 300
    # xhdpi     DPI = 320 Density = 2   PNG_Height = 400
    # xxhdpi    DPI = 480 Density = 3   PNG_Height = 600
    # xxxhdpi   DPI = 560 Density = 3.5 PNG_Height = 700

    # Recovery_UI_Total_Height = PNG_Height + size * lines

    # pointsize: the font size in pgm; size: the height of pgm
    #cmd = u"convert -font %s -background black -fill white -pointsize 24 -size x32 -depth 8 label:@%s %s/%s.pgm" % (font, ftmp, fdir, cf)
    #for miata's low resolution screen
    cmd = u"convert -font %s -background black -fill white -pointsize 12 -size x16 -depth 8 label:@%s %s/%s.pgm" % (font, ftmp, mfdir, cf)
    commands.getoutput(cmd.encode("utf-8"))
    cmd = u"convert -font %s -background black -fill white -pointsize 18 -size x24 -depth 8 label:@%s %s/%s.pgm" % (font, ftmp, hfdir, cf)
    commands.getoutput(cmd.encode("utf-8"))
    cmd = u"convert -font %s -background black -fill white -pointsize 24 -size x32 -depth 8 label:@%s %s/%s.pgm" % (font, ftmp, xhfdir, cf)
    commands.getoutput(cmd.encode("utf-8"))
    cmd = u"convert -font %s -background black -fill white -pointsize 38 -size x48 -depth 8 label:@%s %s/%s.pgm" % (font, ftmp, xxhfdir, cf)
    commands.getoutput(cmd.encode("utf-8"))
    cmd = u"convert -font %s -background black -fill white -pointsize 44 -size x56 -depth 8 label:@%s %s/%s.pgm" % (font, ftmp, xxxhfdir, cf)
    commands.getoutput(cmd.encode("utf-8"))

def string_to_pgm(s):
    for c in s:
        gen_pgm(c)

def gen_ascii():
    a = u''' !"#$%&`()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_'abcdefghijklmnopqrstuvwxyz{|}~'''
    string_to_pgm(a)
    ##http://www.fileformat.info/info/unicode/block/arrows/utf8test.htm
    backarrow = u'\u21e6'
    string_to_pgm(backarrow)
