#!/usr/bin/python
# encoding=utf8

import xml.dom.minidom
import codecs
import locale
import sys
##import os
#3sys.path.append(os.path.abspath('./'))
import gen_fonts

dom = xml.dom.minidom.parse('./multilingual.xml')

def getText(nodelist):
    rc = []
    for node in nodelist:
        if node.nodeType == node.TEXT_NODE:
            rc.append(node.data)
    return ''.join(rc)

def count_elements(nodelist):
    acc = 0
    for node in nodelist:
        if node.nodeType == node.ELEMENT_NODE:
            acc += 1
    return acc

def handle_default(df):
    print "#define DEFAULT_LANG (%s)" % getText(df.childNodes)

def handle_page_count(pg, idt):
    global g_page_cnt
    if(g_page_cnt > 0):
        return
    else:
        g_page_cnt += count_elements(pg.childNodes)
        print "#define PAGE_COUNT (%d)" % g_page_cnt
        print ""

def handle_page(s, idt):
    array_h = "%s_%s_header" % (idt, s.localName)
    print "static const char *%s[] = {" % (array_h)
    headers = s.getElementsByTagName("header")
    for h in headers:
        t = getText(h.childNodes)
        print "\t%s," % t
        gen_fonts.string_to_pgm(t)
    # print "\t\"\","
    print "\tNULL"
    print "};"
    print ""

    main = 0
    if(s.localName == 'main'):
        main = 100

    array_m = "%s_%s_menu" % (idt, s.localName)
    print "static const char *%s[] = {" % (array_m)
    menus = s.getElementsByTagName("menu")
    for m in menus:
        ##workaround of MTK patch.
        main += 1
        t = getText(m.childNodes)
        print "\t%s," % t

        gen_fonts.string_to_pgm(t)
    # print u"\t\"\u21e6\","
    print "\tNULL"
    print "};"
    print ""

    array = "{\n\t.lang = %s,\n\t.name = \"%s\",\n\t.header = %s,\n\t.menu = %s,\n},\n" %(idt, s.localName, array_h, array_m)
    global g_array
    g_array += array

def handle_lang(la):
    lid = la.getElementsByTagName("id")[0]
    idt = getText(lid.childNodes)
    global g_list
    g_list += "\t%s,\n" % idt

    hint = la.getElementsByTagName("hint")[0]
    global g_hint
    t = getText(hint.childNodes)
    gen_fonts.string_to_pgm(t)
    g_hint += "\t%s,\n" % t

    page = la.getElementsByTagName("page")[0]
    handle_page_count(page, idt)

    nodelist = page.childNodes
    for node in nodelist:
        if node.nodeType == node.ELEMENT_NODE:
            handle_page(node, idt)

def handle_ml(ml):
    df = ml.getElementsByTagName("default")[0]
    handle_default(df)

    la = ml.getElementsByTagName("lang")
    for l in la:
        handle_lang(l)

def print_g_array():
    global g_array
    print "const struct ml_strings g_ml_str[] = {"
    print g_array
    print "};"
    print ""

def print_g_list():
    global g_list
    print "static const int supported_lang[] = {"
    print g_list
    print "};"
    print "static const int supported_lang_cnt = (sizeof(supported_lang)/(sizeof(int)));"
    print ""

def print_g_hint():
    global g_hint
    print "char *g_ml_str_hint[] = {"
    print g_hint
    print "\tNULL"
    print "};"
    print ""


##main
g_array = ""
g_list = ""
g_hint = ""
g_page_cnt = 0;

##refer to http://stackoverflow.com/questions/4545661/unicodedecodeerror-when-redirecting-to-file
# Wrap sys.stdout into a StreamWriter to allow writing unicode.
sys.stdout = codecs.getwriter(locale.getpreferredencoding())(sys.stdout)

##handle ascii first for avoiding special chars.
gen_fonts.gen_ascii()

handle_ml(dom)
print_g_array()
print_g_list()
print_g_hint()




