/* Copyright (C) 2016 Tcl Corporation Limited */
/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

#include "../minui/minui.h"

static int utf8_tar_ready = 0;

typedef struct {
	unsigned long long utf8_char;
	unsigned int width;
	unsigned int height;
	unsigned char *data;
	GRFont *p_gr_font;
} pgm_font;

typedef struct {
	char c;
	char str[15];
	int strlen;
} special_char_map;

//FIXME: need to be more flexible.
static const char TAR_PATH[] = "/res/fonts/recovery_fonts.tar";
static int tar_len = -1;
static char *tar = NULL;
#define PGM_LEN (500) //there are 230 uc for en/zh/fr/es/pt/ru, 20140611.
static pgm_font g_pgm_font[PGM_LEN];

#define TAR_DIR		"recovery_fonts"
#define TAR_DIR_LEN	(14)
#define TAR_FN_OFFSET	(15)

static special_char_map g_special_char[] = {
	{'/', "slash", 5},
	{'*', "asterisk", 8},
	{'?', "question", 8},
	{0, "", 0}
};

//get utf8-char from char.
//return uc and corresponding length(bytes).
static unsigned long long utf8_c_to_uc(char *p, int *len)
{
	char t = 0x80;
	int i, j;
	unsigned long long uc = 0;

	for(i=0; i<8; i++){
		if((*p & t) == 0){
			break;
		}
		t = t>>1;
	}

	if(i == 0){
		*len = 1;
		uc = p[0];
	}else if(i > 6){ //FIXME: error handle.
		*len = 1;
		uc = '?';
	}else{
		*len = i; //i is length.
		for(j=i-1;j>=0;j--){
			uc = (uc << 8) + p[j];
		}
	}

	return uc;
}

//return status.
//1, is special
//0, is not special
static inline int get_special_char_map(char c, special_char_map **call)
{
	int i;

	for(i=0; g_special_char[i].c!=0; i++){
		if(c == g_special_char[i].c){
			*call = &g_special_char[i];
			return 1;
		}
	}

	return 0;
}

//return pointer of pgm.
static char* lookup_tar(unsigned long long inuc)
{
	int tu = 512; //tar unit size.
	char *p = tar;
	char *p_max = tar + tar_len - tu;
	int i;

	special_char_map *scm = NULL;
	int is_special = 0;

	unsigned long long uc = 0;
	int uc_len;

	is_special = get_special_char_map((char)(inuc & 0xFF), &scm);

	for(i=0; p<p_max; i++, p+=tu){
		if(strncmp(p, TAR_DIR, TAR_DIR_LEN)){
			continue;
		}else if(p[TAR_FN_OFFSET] == 0){ //skip dir itself.
			continue;
		}

		if(is_special){
			if(0 == strncmp(&p[TAR_FN_OFFSET], scm->str, scm->strlen)){
				return p + tu;
			}
		}else{
			uc = utf8_c_to_uc(&p[TAR_FN_OFFSET], &uc_len);
			if((uc == inuc) && (p[TAR_FN_OFFSET + uc_len] == '.')){
				return p + tu;
			}
		}
	}

	return NULL;
}

//return status.
//0000400: 5035 0a31 3420 3332 0a32 3535 0aff ffff  P5.14 32.255....
static int pgm_get_data(pgm_font *pf, char* buf)
{
	char *p = buf;
	int i, k=0;
	int v = 0;

	//we assume the max string is 32 bytes.
	int const max = 32;

	for(i=0; i<max; i++){
		if(p[i] == '\n' || p[i] == ' '){
			k++;
			if((k == 1) && (v != 325)){
				//check magic of P5.
				return -1;
			}
			if(k == 2)
				pf->width = v;
			if(k == 3)
				pf->height = v;
			if(k == 4){
				pf->data = (unsigned char *)(&p[i + 1]);
				break;
			}
			v = 0;
			continue;
		}

		v = v* 10 + p[i] - '0';
	}

	if(i >= max){
		return -2;
	}

	return 0;
}

static void utf8_make_gr_font(int index)
{
	GRSurface *ftex;
	GRFont *_gr_font;

	_gr_font = reinterpret_cast<GRFont*>(calloc(sizeof(GRFont), 1));
	if (!_gr_font) {
		printf("allocate _gr_font error.\n");
		return;
	}

	_gr_font->texture = reinterpret_cast<GRSurface*>(calloc(sizeof(GRSurface), 1));
	if (!_gr_font->texture) {
		printf("allocate _gr_font->texture error.\n");
		return;
	}
	ftex = _gr_font->texture;

	//many _gr_font.
	g_pgm_font[index].p_gr_font = _gr_font;

	ftex->width = g_pgm_font[index].width;
	ftex->height = g_pgm_font[index].height;
	ftex->row_bytes = g_pgm_font[index].width;
	ftex->pixel_bytes = 1;
	ftex->data = g_pgm_font[index].data;

	_gr_font->cwidth = g_pgm_font[index].width;
	_gr_font->cheight = g_pgm_font[index].height;
	//_gr_font->ascent = g_pgm_font[index].height - 2;
}

//return g_pgm_font index.
static int utf8_lookup_uc(unsigned long long uc)
{
	int i;
	int current_index;

	char *p = NULL;
	pgm_font temp = {0, 0, 0, NULL, NULL};
	pgm_font *pf = &temp;

	//look for existed items.
	for(i=0; i<PGM_LEN; i++){
		if(g_pgm_font[i].utf8_char == 0x0){
			break; //no more data.
		}

		if(g_pgm_font[i].utf8_char == uc){
			return i; //found.
		}
	}

	if(i>=PGM_LEN){
		printf("g_pgm_font is full. uc=0x%llx\n", uc);
		return 0;
	}

	current_index = i;

	p = lookup_tar(uc);
	if(p == NULL){
		printf("not find resource of %llx\n", uc);
		return 0;
	}else{
		i = pgm_get_data(pf, p);
		if(i){
			return 0;
		}

		g_pgm_font[current_index].utf8_char = uc;
		g_pgm_font[current_index].width = pf->width;
		g_pgm_font[current_index].height = pf->height;
		g_pgm_font[current_index].data = pf->data;

		utf8_make_gr_font(current_index);
	}

	//NOTE: return 0 to make sure show a '?' as an error mark.
	return current_index;
}

int utf8_get_font(char *s, int *p_uc_len, GRFont **p_font)
{
	unsigned long long uc;
	int index;
	int uc_len = 1;

        uc = utf8_c_to_uc((char*)s, &uc_len);
        index = utf8_lookup_uc(uc);

	*p_uc_len = uc_len;
	*p_font = g_pgm_font[index].p_gr_font;
	return 0;
}

//get containable uc(utf8-char) font-picture number within screen width.
//return truncated BYTES of (utf8 string with '\0').
int utf8_pixel_constraint(char *s, int x_offset)
{
	int l = 0, total = 0;
	unsigned long long uc;

	int index, width = 0;
	int pixel_max;

	if(! utf8_is_ready()){
		return -1;
	}

	//Android KK, there is a 4-pixels-blank in left, so reserve x_offset.
	pixel_max = gr_fb_width() - x_offset;

	while(*s != '\0'){
		if(l){ //valid utf8.
			if((s[0] & 0xC0) != 0x80){
				break;
			}
		}else{
			uc = utf8_c_to_uc(s, &l);
			index = utf8_lookup_uc(uc);
			width += g_pgm_font[index].width;
			if(width > pixel_max){
				break;
			}
			total += l;
		}

		s++;
		l--;
	}

	return total + 1;
}

int utf8_is_ready(void)
{
	return utf8_tar_ready;
}

int utf8_init_font(int *gr_font_w, int *gr_font_h)
{
	FILE *fp;
	int index;

	memset(g_pgm_font, 0, sizeof(g_pgm_font));

	fp = fopen(TAR_PATH, "r");
	if(fp == NULL){
		printf("%s: open utf8-tar fail, use default.\n", __func__);
		return -1;
	}

	fseek(fp, 0, SEEK_END);
	tar_len = ftell(fp);
	fseek(fp, 0, SEEK_SET);

	tar = (char *)malloc(tar_len);
	if(tar == NULL){
		printf("%s: malloc tar fail, use default.\n", __func__);
		fclose(fp);
		return -1;
	}

	fread(tar, tar_len, 1, fp);
	fclose(fp);

	utf8_tar_ready = 1;

	//we always put '?' at place 1, as an error mark.
	printf("%s: setup an error mark.\n", __func__);
	index = utf8_lookup_uc('?');
	//init the cheight, while we will calculate the cwidth/gr_fb_width
	//in utf8_pixel_constraint().
	*gr_font_w = g_pgm_font[index].p_gr_font->cwidth;
	*gr_font_h = g_pgm_font[index].p_gr_font->cheight;

	return 0;
}

