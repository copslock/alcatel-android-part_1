/* Copyright (C) 2016 Tcl Corporation Limited */
/*
 * Copyright (c) 2008 Travis Geiselbrecht
 *
 * Copyright (c) 2009-2014, The Linux Foundation. All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

 /**
 * @file
 * @brief  Console functions
 *
 * @defgroup console Console
 * @{
 */

#include <kernel/console.h>
#include <kernel/mutex.h>

static mutex_t console_mutex;
struct console *console_drivers;
static int console_locked;


/**
 * @brief  Check if a thread hold the console lock
 */
int is_console_locked(void)
{
	return console_locked;
}

/**
 * @brief  acquire the console lock
 */
void console_lock(void)
{
	mutex_acquire(&console_mutex);
	console_locked = 1;
}

/**
 * @brief  try acquire the console lock
 * @return  1 on sucess, 0 otherwise
 *
 */
int console_trylock(void)
{
	if (is_console_locked())
		return 0;
	mutex_acquire(&console_mutex);
	console_locked = 1;
	return 1;
}

/**
 * @brief  release the console lock
 */
void console_unlock(void)
{
	console_locked = 0;
	mutex_release(&console_mutex);
}

/**
 * @brief  register a new console
 */
void register_console(struct console *newcon)
{
	if (newcon->setup && newcon->setup(newcon, NULL))
		return;
	console_lock();
	newcon->next = console_drivers;
	console_drivers = newcon;
	console_unlock();
}

/**
 * @brief  remove the console.
 */
int unregister_console(struct console *console)
{
	struct console *a, *b;
	int res = 1;

	console_lock();
	if (console_drivers == console) {
		console_drivers = console->next;
		res = 0;
	} else if (console_drivers) {
		for (a = console_drivers->next, b = console_drivers;
		     a; b = a, a = b->next) {
			if (a == console) {
				b->next = a->next;
				res = 0;
				break;
			}
		}
	}
	console_unlock();

	return res;
}

/**
 * @brief  early init the console layer.
 * lock the console before console_init()
 */
void console_init_early(void)
{
	console_drivers = NULL;
	console_locked = 1;
}

/**
 * @brief init the console layer.
 * one can only acquire console lock
 * after calling  this function
 */
void console_init(void)
{
	mutex_init(&console_mutex);
	console_locked = 0;
}
