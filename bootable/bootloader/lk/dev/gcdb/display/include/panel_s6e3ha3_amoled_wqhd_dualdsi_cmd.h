/* Copyright (c) 2015, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *  * Neither the name of The Linux Foundation nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#ifndef _PANEL_S6E3HA3_AMOLED_WQHD_DUALDSI_CMD_H_
#define _PANEL_S6E3HA3_AMOLED_WQHD_DUALDSI_CMD_H_
/*---------------------------------------------------------------------------*/
/* HEADER files                                                              */
/*---------------------------------------------------------------------------*/
#include "panel.h"

/*---------------------------------------------------------------------------*/
/* Panel configuration                                                       */
/*---------------------------------------------------------------------------*/
#if 1
static struct panel_config s6e3ha3_amoled_wqhd_dualdsi_cmd_panel_data_with_pmic = {
	"qcom,mdss_dsi_s6e3ha3_amoled_wqhd_pmic_cmd", "dsi:0:", "qcom,mdss-dsi-panel",
	10, 1, "DISPLAY_1", 0, 0, 60, 0, 0, 0, 1, 0, 0, 0, 0, 11, 0, 0,
	"qcom,mdss_dsi_s6e3ha3_amoled_wqhd_pmic_cmd"
};

/* [PLATFORM]-Mod-BEGIN by TCTNB.CY, FR-1192236, 2015/12/18, disable  HW TE if panel not connected*/
static struct panel_config s6e3ha3_amoled_wqhd_dualdsi_cmd_panel_data_sw_te = {
	"qcom,mdss_dsi_s6e3ha3_amoled_wqhd_sw_te", "dsi:0:", "qcom,mdss-dsi-panel",
	10, 1, "DISPLAY_1", 0, 0, 60, 0, 0, 0, 1, 0, 0, 0, 0, 11, 0, 0,
	"qcom,mdss_dsi_s6e3ha3_amoled_wqhd_sw_te"
};
/* [PLATFORM]-Mod-END by TCTNB.CY, 2015/12/18*/
#else
static struct panel_config s6e3ha3_amoled_wqhd_dualdsi_cmd_panel_data = {
	"qcom,mdss_dsi_s6e3ha3_amoled_wqhd_cmd", "dsi:0:", "qcom,mdss-dsi-panel",
	10, 1, "DISPLAY_1", 0, 0, 60, 0, 0, 0, 1, 0, 0, 0, 0, 11, 0, 0,
	"qcom,mdss_dsi_s6e3ha3_amoled_wqhd_cmd"
};
#endif

/*---------------------------------------------------------------------------*/
/* Panel resolution                                                          */
/*---------------------------------------------------------------------------*/
static struct panel_resolution s6e3ha3_amoled_wqhd_dualdsi_cmd_panel_res = {
	1440, 2560, 120, 120, 100, 0, 40, 40, 40, 0, 0, 0, 0, 0, 0, 0, 0, 0
};

/*---------------------------------------------------------------------------*/
/* Panel color information                                                   */
/*---------------------------------------------------------------------------*/
static struct color_info s6e3ha3_amoled_wqhd_dualdsi_cmd_color = {
	24, 0, 0xff, 0, 0, 0
};

/*---------------------------------------------------------------------------*/
/* Panel on/off command information                                          */
/*---------------------------------------------------------------------------*/
static char s6e3ha3_amoled_wqhd_dualdsi_cmd_on_cmd0[] = {
	0x11, 0x00, 0x05, 0x80
};

static char s6e3ha3_amoled_wqhd_dualdsi_cmd_on_cmd1[] = {		//TE on
	0x35, 0x00, 0x15, 0x80
};

//interface setting
static char s6e3ha3_amoled_wqhd_dualdsi_cmd_on_cmd2[] = {
	0x03, 0x00, 0x39, 0xc0,
	0xf0, 0x5a, 0x5a, 0xff
};

static char s6e3ha3_amoled_wqhd_dualdsi_cmd_on_cmd3[] = {
	0x02, 0x00, 0x39, 0xc0,
	0xc4, 0x03, 0xff, 0xff
};

static char s6e3ha3_amoled_wqhd_dualdsi_cmd_on_cmd4[] = {
	0x02, 0x00, 0x39, 0xc0,
	0xf9, 0x03, 0xff, 0xff
};

static char s6e3ha3_amoled_wqhd_dualdsi_cmd_on_cmd5[] = {
	0x14, 0x00, 0x39, 0xc0,
	0xc2, 0x00, 0x00, 0xd8,
	0xd8, 0x00, 0x80, 0x2b,
	0x05, 0x08, 0x0e, 0x07,
	0x0b, 0x05, 0x0d, 0x0a,
	0x15, 0x13, 0x20, 0x1e
};

static char s6e3ha3_amoled_wqhd_dualdsi_cmd_on_cmd6[] = {
	0x03, 0x00, 0x39, 0xc0,
	0xf0, 0xa5, 0xa5, 0xff
};
//common setting
static char s6e3ha3_amoled_wqhd_dualdsi_cmd_on_cmd7[] = {
	0x03, 0x00, 0x39, 0xc0,
	0xf0, 0x5a, 0x5a, 0xff
};

static char s6e3ha3_amoled_wqhd_dualdsi_cmd_on_cmd8[] = {
	0x02, 0x00, 0x39, 0xc0,
	0xed, 0x4d, 0xff, 0xff
};
/* [BUGFIX]-Mod-BEGIN by TCTNB.CY, task-929771, 2015/11/17, enable hsync for tp*/
static char s6e3ha3_amoled_wqhd_dualdsi_cmd_on_cmd8_1[] = {
	0x03, 0x00, 0x39, 0xc0,
	0xbc, 0x11, 0x11, 0xff
};
/* [PLATFORM]-Mod-END by TCTNB.CY, 2015/11/17*/
static char s6e3ha3_amoled_wqhd_dualdsi_cmd_on_cmd9[] = {
	0x03, 0x00, 0x39, 0xc0,
	0xf0, 0xa5, 0xa5, 0xff
};
/* [BUGFIX]-Mod-BEGIN by TCTNB.CY, task-988883, 2015/11/27, brightness blink*/
//brightness setting
static char s6e3ha3_amoled_wqhd_dualdsi_cmd_on_cmd10[] = {
	0x53, 0x20, 0x15, 0x80
};
/* [BUGFIX]-Mod-END by TCTNB.CY, 2015/11/27*/

static char s6e3ha3_amoled_wqhd_dualdsi_cmd_on_cmd11[] = {
	0x51, 0x7f, 0x15, 0x80
};

static char s6e3ha3_amoled_wqhd_dualdsi_cmd_on_cmd12[] = {		//ACL off
	0x55, 0x00, 0x15, 0x80
};

static char s6e3ha3_amoled_wqhd_dualdsi_cmd_on_cmd13[] = {
	0x2c, 0x00, 0x05, 0x80
};

static char s6e3ha3_amoled_wqhd_dualdsi_cmd_on_cmd14[] = {
	0x29, 0x00, 0x05, 0x80
};

static struct mipi_dsi_cmd s6e3ha3_amoled_wqhd_dualdsi_cmd_on_command[] = {
	{sizeof(s6e3ha3_amoled_wqhd_dualdsi_cmd_on_cmd0), s6e3ha3_amoled_wqhd_dualdsi_cmd_on_cmd0, 0x0a},
	{sizeof(s6e3ha3_amoled_wqhd_dualdsi_cmd_on_cmd1), s6e3ha3_amoled_wqhd_dualdsi_cmd_on_cmd1, 0x00},
	{sizeof(s6e3ha3_amoled_wqhd_dualdsi_cmd_on_cmd2), s6e3ha3_amoled_wqhd_dualdsi_cmd_on_cmd2, 0x00},
	{sizeof(s6e3ha3_amoled_wqhd_dualdsi_cmd_on_cmd3), s6e3ha3_amoled_wqhd_dualdsi_cmd_on_cmd3, 0x00},
	{sizeof(s6e3ha3_amoled_wqhd_dualdsi_cmd_on_cmd4), s6e3ha3_amoled_wqhd_dualdsi_cmd_on_cmd4, 0x00},
	{sizeof(s6e3ha3_amoled_wqhd_dualdsi_cmd_on_cmd5), s6e3ha3_amoled_wqhd_dualdsi_cmd_on_cmd5, 0x00},
	{sizeof(s6e3ha3_amoled_wqhd_dualdsi_cmd_on_cmd6), s6e3ha3_amoled_wqhd_dualdsi_cmd_on_cmd6, 0x96},
	{sizeof(s6e3ha3_amoled_wqhd_dualdsi_cmd_on_cmd7), s6e3ha3_amoled_wqhd_dualdsi_cmd_on_cmd7, 0x00},
	{sizeof(s6e3ha3_amoled_wqhd_dualdsi_cmd_on_cmd8), s6e3ha3_amoled_wqhd_dualdsi_cmd_on_cmd8, 0x00},
/* [BUGFIX]-Mod-BEGIN by TCTNB.CY, task-929771, 2015/11/17, enable hsync for tp*/
	{sizeof(s6e3ha3_amoled_wqhd_dualdsi_cmd_on_cmd8_1), s6e3ha3_amoled_wqhd_dualdsi_cmd_on_cmd8_1, 0x00},
	{sizeof(s6e3ha3_amoled_wqhd_dualdsi_cmd_on_cmd9), s6e3ha3_amoled_wqhd_dualdsi_cmd_on_cmd9, 0x00},
	{sizeof(s6e3ha3_amoled_wqhd_dualdsi_cmd_on_cmd10), s6e3ha3_amoled_wqhd_dualdsi_cmd_on_cmd10, 0x00},
	{sizeof(s6e3ha3_amoled_wqhd_dualdsi_cmd_on_cmd11), s6e3ha3_amoled_wqhd_dualdsi_cmd_on_cmd11, 0x00},
	{sizeof(s6e3ha3_amoled_wqhd_dualdsi_cmd_on_cmd12), s6e3ha3_amoled_wqhd_dualdsi_cmd_on_cmd12, 0x00},
	{sizeof(s6e3ha3_amoled_wqhd_dualdsi_cmd_on_cmd13), s6e3ha3_amoled_wqhd_dualdsi_cmd_on_cmd13, 0x00},
	{sizeof(s6e3ha3_amoled_wqhd_dualdsi_cmd_on_cmd14), s6e3ha3_amoled_wqhd_dualdsi_cmd_on_cmd14, 0x00},

};

#define S6E3HA3_AMOLED_WQHD_DUALDSI_CMD_ON_COMMAND 16
/* [PLATFORM]-Mod-END by TCTNB.CY, 2015/11/17*/

static char s6e3ha3_amoled_wqhd_dualdsi_cmdoff_cmd0[] = {
	0x28, 0x00, 0x05, 0x80
};

static char s6e3ha3_amoled_wqhd_dualdsi_cmdoff_cmd1[] = {
	0x10, 0x00, 0x05, 0x80
};

static struct mipi_dsi_cmd s6e3ha3_amoled_wqhd_dualdsi_cmd_off_command[] = {
	{0x4, s6e3ha3_amoled_wqhd_dualdsi_cmdoff_cmd0, 0x32},
	{0x4, s6e3ha3_amoled_wqhd_dualdsi_cmdoff_cmd1, 0x78}
};

#define S6E3HA3_AMOLED_WQHD_DUALDSI_CMD_OFF_COMMAND 2

/* [BUGFIX]-Mod-BEGIN by TCTNB.CY, FR-989087, 2015/11/27, change send cmd mode to hs mode*/
static struct command_state s6e3ha3_amoled_wqhd_dualdsi_cmd_state = {
	1, 1
};
/* [BUGFIX]-Mod-END by TCTNB.CY, 2015/11/27*/

/*---------------------------------------------------------------------------*/
/* Command mode panel information                                            */
/*---------------------------------------------------------------------------*/
static struct commandpanel_info s6e3ha3_amoled_wqhd_dualdsi_cmd_command_panel = {
	1, 1, 1, 0, 0, 0x2c, 0, 0, 0, 1, 0, 0
};

/*---------------------------------------------------------------------------*/
/* Video mode panel information                                              */
/*---------------------------------------------------------------------------*/
static struct videopanel_info s6e3ha3_amoled_wqhd_dualdsi_cmd_video_panel = {
	0, 0, 0, 0, 1, 1, 1, 0, 0
};

/*---------------------------------------------------------------------------*/
/* Lane configuration                                                        */
/*---------------------------------------------------------------------------*/
static struct lane_configuration s6e3ha3_amoled_wqhd_dualdsi_cmd_lane_config = {
	4, 0, 1, 1, 1, 1, 0
};

/*---------------------------------------------------------------------------*/
/* Panel timing                                                              */
/*---------------------------------------------------------------------------*/
/* [BUGFIX]-Mod-BEGIN by TCTNB.CY, FR-898333, 2015/11/11, optimize dsi clk configure*/
static const uint32_t s6e3ha3_amoled_wqhd_dualdsi_cmd_timings[] = {
	0x22, 0x40, 0x2a, 0x01, 0x74, 0x78, 0x30, 0x44, 0x35, 0x03, 0x04, 0x00
};
/* [PLATFORM]-Mod-END by TCTNB.CY, 2015/11/11*/

/* [BUGFIX]-Mod-BEGIN by TCTNB.WPL, 2016/06/18, defect-2378790, update mipi timing */
static const uint32_t s6e3ha3_amoled_wqhd_dualdsi_thulium_cmd_timings[] = {
	0x25, 0x20, 0x9, 0xa, 0x6, 0x3, 0x04, 0xa0,
	0x25, 0x20, 0x9, 0xa, 0x6, 0x3, 0x04, 0xa0,
	0x25, 0x20, 0x9, 0xa, 0x6, 0x3, 0x04, 0xa0,
	0x25, 0x20, 0x9, 0xa, 0x6, 0x3, 0x04, 0xa0,
	0x25, 0x1e, 0x9, 0xa, 0x6, 0x3, 0x04, 0xa0,
};

static struct panel_timing s6e3ha3_amoled_wqhd_dualdsi_cmd_timing_info = {
	0x0, 0x04, 0x0e, 0x35
};
/* [BUGFIX]-Mod-END by TCTNB.WPL, 2016/06/18 */

/*---------------------------------------------------------------------------*/
/* Panel reset sequence                                                      */
/*---------------------------------------------------------------------------*/
static struct panel_reset_sequence s6e3ha3_amoled_wqhd_dualdsi_cmd_reset_seq = {
	{1, 0, 1, }, {10, 10, 10, }, 2
};

/*---------------------------------------------------------------------------*/
/* Backlight setting                                                         */
/*---------------------------------------------------------------------------*/
static struct backlight s6e3ha3_amoled_wqhd_dualdsi_cmd_backlight = {
	2, 1, 255, 1, 1, NULL	/* BL_DCS */
};
/* [BUGFIX]-Mod-BEGIN by TCTNB.WPL, 2016/06/18, defect-2378790, WQHD crash, modify swire_contro 0->1 */
static struct labibb_desc s6e3ha3_amoled_wqhd_dualdsi_cmd_labibb = {
       1, 1, 4000000, 4000000, 4600000, 4600000, 3, 3, 1, 1
};
/* [BUGFIX]-Mod-END by TCTNB.WPL, 2016/06/18 */
#define S6E3HA3_AMOLED_WQHD_DUALDSI_CMD_SIGNATURE  0xFFFF
#endif
