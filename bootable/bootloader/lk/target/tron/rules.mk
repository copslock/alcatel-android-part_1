LOCAL_DIR := $(GET_LOCAL_DIR)

INCLUDES += -I$(LOCAL_DIR)/include -I$(LK_TOP_DIR)/platform/msm_shared
INCLUDES += -I$(LK_TOP_DIR)/dev/gcdb/display -I$(LK_TOP_DIR)/dev/gcdb/display/include
ifeq ($(ENABLE_MDTP_SUPPORT),1)
INCLUDES += -I$(LK_TOP_DIR)/app/aboot
endif

PLATFORM := msm8996

MEMBASE := 0x91600000 # SDRAM
MEMSIZE := 0x00400000 # 4MB

BASE_ADDR    := 0x0000000

SCRATCH_ADDR := 0x91C00000
SCRATCH_SIZE := 740

# LPAE supports only 32 virtual address, L1 pt size is 4
L1_PT_SZ     := 4
L2_PT_SZ     := 3

DEFINES += PMI_CONFIGURED=1

DEFINES += DISPLAY_SPLASH_SCREEN=1
DEFINES += DISPLAY_TYPE_MIPI=1
DEFINES += DISPLAY_TYPE_DSI6G=1

#Add-Begin TCTNB.lijiang TASK-1193601 2015/12/21:minisw need auto power on when plug-in USB
ifeq ($(TARGET_BUILD_MMITEST),true)
DEFINES += FEATURE_DISABLE_CHANGING_MMI
endif
#Add-End TCTNB.lijiang TASK-1193601 2015/12/21

#Add-Begin by TCTNB.lijiang:2015/12/21,task-1162933,press volume up+volume down ,into edl mode
DEFINES += FEATURE_EMERGENCY_DLOAD_LK
DEFINES += FEATURE_OLED_FLUSH_SPLASH
#Add-End by TCTNB.lijiang:2015/12/21,task-1162933

# [PLATFORM]-Mod-BEGIN by TCTNB.lijiang, task-1196058 2015/12/22, add for display low-power icon
ifeq ($(TARGET_BUILD_MMITEST), false)
DEFINES += FEATURE_LOW_POWER_DISP_LK
endif
# [PLATFORM]-Mod-END by TCTNB.lijiang

#Add-Begin TCTNB.lijiang TASK-1207414 2015/12/23:power off alarm
DEFINES += FEATURE_TCTNB_POWEROFF_ALARM
#Add-End TCTNB.lijiang TASK-1207414 2015/12/23

#Add Begin by TCTNB.lijiang 2015/12/29:charger boot logo
ifeq ($(TARGET_BUILD_MMITEST),false)
DEFINES += FEATURE_CHARGER_BOOT_LOGO
endif
#Add End by TCTNB.lijiang 2015/12/29

MODULES += \
	dev/keys \
	dev/pmic/pm8x41 \
	dev/qpnp_haptic \
	dev/vib \
	dev/qpnp_wled \
	dev/qpnp_led \
	dev/gcdb/display \
	dev/pmic/pmi8994 \
	lib/ptable \
	lib/libfdt

DEFINES += \
	MEMSIZE=$(MEMSIZE) \
	MEMBASE=$(MEMBASE) \
	BASE_ADDR=$(BASE_ADDR) \
	TAGS_ADDR=$(TAGS_ADDR) \
	RAMDISK_ADDR=$(RAMDISK_ADDR) \
	SCRATCH_ADDR=$(SCRATCH_ADDR) \
	SCRATCH_SIZE=$(SCRATCH_SIZE) \
	L1_PT_SZ=$(L1_PT_SZ) \
	L2_PT_SZ=$(L2_PT_SZ)


OBJS += \
	$(LOCAL_DIR)/init.o \
	$(LOCAL_DIR)/meminfo.o \
	$(LOCAL_DIR)/target_display.o \
	$(LOCAL_DIR)/oem_panel.o \

ifeq ($(ENABLE_GLINK_SUPPORT),1)
OBJS += \
    $(LOCAL_DIR)/regulator.o
endif

ifeq ($(ENABLE_MDTP_SUPPORT),1)
OBJS += \
	$(LOCAL_DIR)/mdtp_defs.o
endif
