## 8.3\. Power-Saving Modes

All apps exempted from App Standby and/or Doze mode MUST be made visible to the
end user. Further, the triggering, maintenance, wakeup algorithms and the use
of Global system settings of these power-saving modes MUST not deviate from the
Android Open Source Project.
