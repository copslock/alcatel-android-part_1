/******************************************************************************/
/*                                                            Date:27/01/2016 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2016 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  Long Na                                                         */
/*  Email  :  na.long@tcl-mobile.com                                          */
/*  Role   :                                                                  */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments : Application using for nfc test mode                            */
/*  File     : /vendor/tct/source/apps/NfcTestMode/src/com/NfcTestMode        */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 27/01/2016|LongNa                |FR1527651             |Add nfc test mode */
/* ----------|----------------------|----------------------|----------------- */
/* ========================================================================== */

package com.android.NfcTestMode;

import com.android.NfcTestMode.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.BroadcastReceiver;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Toast;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Spinner;
import android.os.RemoteException;
import android.os.IBinder;
import android.os.Handler;
import android.os.Message;

import com.nxp.nfc.NxpNfcAdapter;
import android.nfc.NfcAdapter;

public class NfcTestMode extends Activity {

    private Context mContext;
    private NfcAdapter mNfcAdapter;
    private NxpNfcAdapter mNxpNfcAdapter;
    private final String TAG = "NfcTestMode";
    private static final int ENABLE_NFC_TEST_READ_A = 1;

    private Button mButton01 = null;
    private Button mButton02 = null;
    private Button mButton03 = null;
    private Button mButton04 = null;
    private Button mButton05 = null;
	private TextView mTextView1 = null;
	private TextView mTextView2 = null;
    private boolean flag = false;

    private Thread uiReadThread = null;
    private Handler mHandler;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = this;

        mNfcAdapter = NfcAdapter.getDefaultAdapter(mContext);
        //mNxpNfcAdapter = NxpNfcAdapter.getNxpNfcAdapter(mNfcAdapter);

        if (mNfcAdapter.isEnabled()) {
            mNfcAdapter.disable();
        }

        try {
            Thread.sleep(200);
        } catch (Exception e) {
            Log.e(TAG, "exception when sleep" + e);
        }

        try {
            int ret = NfcTestModeJNI.testNfcRfOff();//init
            Log.e(TAG, "read ret = " + ret);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        setContentView(R.layout.main);

        mButton01 = (Button) findViewById(R.id.Button01);
        mButton02 = (Button) findViewById(R.id.Button02);
        mButton03 = (Button) findViewById(R.id.Button03);
        mButton04 = (Button) findViewById(R.id.Button04);
        mButton05 = (Button) findViewById(R.id.Button05);
        mTextView1=(TextView)findViewById(R.id.textView1);
        mTextView2=(TextView)findViewById(R.id.textView2);

        mButton01.setEnabled(true);
        mButton02.setEnabled(false);
        mButton03.setEnabled(true);
        mButton04.setEnabled(false);
        mButton05.setEnabled(true);

        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case ENABLE_NFC_TEST_READ_A:
                        int times = (int) msg.obj;
                        int suc = msg.arg1;
                        int fail = msg.arg2;
                        mTextView1.setText(Integer.toString(times) + "/10000, " + Integer.toString(suc) + " success, " + Integer.toString(fail) + " fail");
                        break;
                }
            }
        };

        mButton01.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                try {
                    int ret = NfcTestModeJNI.testNfcRfOn();
                    if (ret >= 0) {
                        flag = true;
                        mButton01.setEnabled(false);
                        mButton02.setEnabled(true);
                        mButton03.setEnabled(false);
                        mButton04.setEnabled(false);
                    }
                    Log.e(TAG, "ret = " + ret);
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        });

        mButton02.setOnClickListener(new OnClickListener() {
        public void onClick(View v) {

                try {
                    int ret = NfcTestModeJNI.testNfcRfOff();
                    if (ret >= 0) {
                        flag = false;
                        mButton02.setEnabled(false);
                        mButton01.setEnabled(true);
                        mButton03.setEnabled(true);
                        mButton04.setEnabled(false);
                    }
                    Log.e(TAG, "read ret = " + ret);
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        });
        mButton03.setOnClickListener(new OnClickListener() {
        public void onClick(View v) {
                mButton03.setEnabled(false);
                mButton04.setEnabled(true);
                mButton01.setEnabled(false);
                mButton02.setEnabled(false);
                uiReadThread = new ThreadRead (mContext);
                uiReadThread.start();
            }
        });
        mButton04.setOnClickListener(new OnClickListener() {
        public void onClick(View v) {
                mButton04.setEnabled(false);
                mButton03.setEnabled(true);
                mButton01.setEnabled(true);
                mButton02.setEnabled(false);
                uiReadThread.interrupt();
                uiReadThread = null;
            }
        });
        mButton05.setOnClickListener(new OnClickListener() {
        public void onClick(View v) {
                try {
                    int ret = NfcTestModeJNI.testNfcSwp();
                    if (ret == 0) {
                        mTextView2.setText("SWP-UICC OK");
                    } else {
                        mTextView2.setText("SWP-UICC NOK");
                    }
                    Log.e(TAG, "testNfcSwp ret = " + ret);
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        });

    }

    /**
     * Called when the activity will start interacting with the user.
     */
    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();

    }

    /**
     * Called when the system is about to start resuming a previous activity.
     */
    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        if (uiReadThread != null) {
            uiReadThread.interrupt();
            uiReadThread = null;
            mButton04.setEnabled(false);
            mButton03.setEnabled(true);
            mButton01.setEnabled(true);
            mButton02.setEnabled(false);
        }
        if (flag == true) {
            try {
                int ret = NfcTestModeJNI.testNfcRfOff();//reset
                Log.e(TAG, "reset ret = " + ret);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

    }

    /**
     * The final call you receive before your activity is destroyed.
     */
    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        if (uiReadThread != null) {
            uiReadThread.interrupt();
            uiReadThread = null;
            mButton04.setEnabled(false);
            mButton03.setEnabled(true);
            mButton01.setEnabled(true);
            mButton02.setEnabled(false);
        }
        if (flag == true) {
            try {
                int ret = NfcTestModeJNI.testNfcRfOff();//reset
                Log.e(TAG, "reset ret = " + ret);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    private class ThreadRead extends Thread {
        private Context mContext1;

        public ThreadRead(Context context) {
            super("NfcTestMode ThreadRead");
            mContext1 = context;
        }

        @Override
        public void run() {
                int i = 0;
                int suc = 0;
                int fail = 0;
                while ( (i<10000) && (!isInterrupted()) ) {
                    i++;
                    try {
                        int ret = NfcTestModeJNI.testNfcReadA();
                        if (ret > 0) {
                            suc++;
                        } else {
                            fail++;
                        }
                        Message msg = Message.obtain(mHandler);
                        msg.what = ENABLE_NFC_TEST_READ_A;
                        msg.arg1 = suc;
                        msg.arg2 = fail;
                        msg.obj = i;
                        msg.sendToTarget();
                        Log.e(TAG, Integer.toString(i) + "/10000, " + Integer.toString(suc) + " success, " + Integer.toString(fail) + " fail");

                        Log.e(TAG, "read ret = " + ret);
                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                        break;
                    }
                }
        }
    };

}
