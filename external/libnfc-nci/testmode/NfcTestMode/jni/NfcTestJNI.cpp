/*!
 * \file phDTALibJNI.cpp
 *
 * Project: NFC DTA
 *
 */

/*
 ************************* Header Files ****************************************
 */

#include <utils/Log.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include "jni.h"
#include "nfc_test.h"

//static const char *classPathName = "com/android/runintest/WIFIJNI";
static const char *classPathName = "com/android/NfcTestMode/NfcTestModeJNI";

//[FEATURE]-Add-BEGIN by TCTNB.LongNa,01/26/2016,1527651, Nfc Test Mode
/**********************************************************************************
 **
 ** Function:        nfcManager_testNfcSwp
 **
 ** Description:     test SWP
 **
 ** Returns:         test status
 **                  [0 OK, -1 NOK]
 **
 **********************************************************************************/

static jint NfcTestMode_testNfcSwp(JNIEnv* e, jobject o)
{
    int ret = -1;
    ret = testNfcSwp();
    return ret;
}

/**********************************************************************************
 **
 ** Function:        nfcManager_testNfcRfOn
 **
 ** Description:     test RF ON
 **
 ** Returns:         test status
 **                  [0 OK, -1 NOK]
 **
 **********************************************************************************/
static jint NfcTestMode_testNfcRfOn(JNIEnv* e, jobject o)
{
    int ret = -1;
    ret = testNfcRfOn();
    return ret;
}

/**********************************************************************************
 **
 ** Function:        nfcManager_testNfcRfOff
 **
 ** Description:     test RF OFF
 **
 ** Returns:         test status
 **                  [0 OK, -1 NOK]
 **
 **********************************************************************************/
static jint NfcTestMode_testNfcRfOff(JNIEnv* e, jobject o)
{
    int ret = -1;
    ret = testNfcRfOff();
    return ret;
}

/**********************************************************************************
 **
 ** Function:        nfcManager_testNfcReadA
 **
 ** Description:     test read type A card
 **
 ** Returns:         test status
 **                  [0 OK, -1 NOK]
 **
 **********************************************************************************/
static jint NfcTestMode_testNfcReadA(JNIEnv* e, jobject o)
{
    int ret = -1;
    ret = testNfcReadA();
    return ret;
}
//[FEATURE]-End-BEGIN by TCTNB.LongNa,01/26/2016,1527651, Nfc Test Mode

static JNINativeMethod methods[] = {{"testNfcSwp", "()I", (void*)NfcTestMode_testNfcSwp },
                                      {"testNfcRfOn", "()I", (void*)NfcTestMode_testNfcRfOn },
                                      {"testNfcRfOff", "()I", (void*)NfcTestMode_testNfcRfOff },
                                      {"testNfcReadA", "()I", (void*)NfcTestMode_testNfcReadA }};

/*
 * Register several native methods for one class.
 */
static int registerNativeMethods(JNIEnv* env, const char* className,
        JNINativeMethod* gMethods, int numMethods) {
    jclass clazz;

    clazz = env->FindClass(className);
    if (clazz == NULL) {
        ALOGE("Native registration unable to find class '%s'", className);
        return JNI_FALSE;
    }
    if (env->RegisterNatives(clazz, gMethods, numMethods) < 0) {
        ALOGE("RegisterNatives failed for '%s'", className);
        return JNI_FALSE;
    }

    return JNI_TRUE;
}

/*
 * Register native methods for all classes we know about.
 *
 * returns JNI_TRUE on success.
 */
static int registerNatives(JNIEnv* env) {
    if (!registerNativeMethods(env, classPathName, methods,
            sizeof(methods) / sizeof(methods[0]))) {
        return JNI_FALSE;
    }

    return JNI_TRUE;
}

/*
 * This is called by the VM when the shared library is first loaded.
 */

typedef union {
    JNIEnv* env;
    void* venv;
} UnionJNIEnvToVoid;

jint JNI_OnLoad(JavaVM* vm, void* reserved) {
    UnionJNIEnvToVoid uenv;
    uenv.venv = NULL;
    jint result = -1;
    JNIEnv* env = NULL;

    ALOGI("JNI_OnLoad");

    if (vm->GetEnv(&uenv.venv, JNI_VERSION_1_4) != JNI_OK) {
        ALOGE("ERROR: GetEnv failed");
        goto bail;
    }
    env = uenv.env;

    if (registerNatives(env) != JNI_TRUE) {
        ALOGE("ERROR: registerNatives failed");
        goto bail;
    }

    result = JNI_VERSION_1_4;

    bail: return result;
}
