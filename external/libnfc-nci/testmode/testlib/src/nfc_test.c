/**
 * \file nfc_test.c
 * \brief 
 *
 * Add by LongNa for Defect1527651, Nfc test mode
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <signal.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <cutils/log.h>
#include <time.h>

#define MAX_TRACE_BUFFER            200
#define _read_sample_test_size			30
#define PN544_SET_PWR _IOW(0xe9, 0x01, unsigned int)

#define BUILD_INFO (__DATE__ " " __TIME__)

int sendrecv(int fp, unsigned char* in, int inLen, unsigned char* out)
{
    int ret = 0, i = 0, rspLen = 0;

    clock_t t0,t1;

    usleep(5000);
    ALOGE("\nPC->NFC: ");
    for (i=0;i<inLen;i++)
        ALOGE("%02x ", in[i]);

    t0 = clock();
    while ((ret= write(fp, in, inLen)) < 0) {
        t1 = clock() - t0;
        if (t1 >= 2 * CLOCKS_PER_SEC) {
            ALOGE("\n write timeout");
            break;
        }
    }
    if(ret<0)
        ALOGE("\nPhysical Write Error!");
    else
        ret = 0;

    ALOGE("\nNFC->PC: ");
    if(ret == 0)
    {
        t0 = clock();
        while ((ret= read(fp, out, 3)) < 0) {
            t1 = clock() - t0;
            if (t1 >= 2 * CLOCKS_PER_SEC) {
                ALOGE("\n==TEST_RF_FIELD_CMD_ON== read timeout");
                break;
            }
        }
        if ( ret < 0 )
            ALOGE("\npn544 read error");
        else
        {
            for (i=0;i<3;i++)
                ALOGE("%02x ", out[i]);
            rspLen = out[2];
            t0 = clock();
            while ((ret= read(fp, out+3, rspLen)) < 0) {
                t1 = clock() - t0;
                if (t1 >= 2 * CLOCKS_PER_SEC) {
                    ALOGE("\n==TEST_RF_FIELD_CMD_ON== read timeout");
                    break;
                }
            }
            if(ret < 0)
                ALOGE("\npn544 read error");
            else
                ret = 0;
        }
        for (i=0;i<rspLen;i++)
            ALOGE("%02x ", out[3+i]);
    }

    return ret;
}

int testNfcSwp()
{
    unsigned char recvBuf[50];
    unsigned char CORE_RESET_CMD[] ={0x20, 0x00, 0x01, 0x01};
    unsigned char CORE_INIT_CMD[] ={0x20,0x01,0x00};
    unsigned char SWP_TEST_CMD1[] ={0x2F, 0x02, 0x00};
    unsigned char SWP_TEST_CMD2[] ={0x2F, 0x3E, 0x01, 0x00};
    int ret = 0, i, rspLen = 0;
    int fp;

    fp = open("/dev/pn544", O_RDWR);
    if( fp < 0 ) {ret = -1;
        ALOGE("failed to open /dev/pn544");
        return -1;
    }

    if(ret == 0)
    {
        ALOGD("\n==Hardware reseting==");
        ioctl(fp, PN544_SET_PWR, 1);
        ioctl(fp, PN544_SET_PWR, 0);
        ioctl(fp, PN544_SET_PWR, 1);
    }

    if(ret == 0)
    {
        ALOGD("\n==CORE_RESET_CMD==");
        memset(recvBuf,0,sizeof(recvBuf));
        ret = sendrecv(fp,CORE_RESET_CMD,sizeof(CORE_RESET_CMD),recvBuf);
        if(ret == 0)
        {
            ALOGD("\nConfiguration Status: %02x",recvBuf[3]);
            ALOGD("\nNCI Version: %02x",recvBuf[4]);
        }
    }

    if(ret == 0)
    {
        ALOGD("\n==CORE_INIT_CMD==");
        memset(recvBuf,0,sizeof(recvBuf));
        ret = sendrecv(fp,CORE_INIT_CMD,sizeof(CORE_INIT_CMD),recvBuf);
        if(ret == 0 && recvBuf[3] == 0x00)
        {
            rspLen = recvBuf[2];
            ALOGD("\nFirmware Version Number: %02x,%02x,%02x",recvBuf[3+rspLen-3],recvBuf[3+rspLen-2],recvBuf[3+rspLen-1]);
        }
    }

    if(ret == 0)
    {
        ALOGD("\n==SWP_TEST_CMD1==");
        memset(recvBuf,0,sizeof(recvBuf));
        ret = sendrecv(fp,SWP_TEST_CMD1,sizeof(SWP_TEST_CMD1),recvBuf);
        if(ret == 0)
        {
			for (i=0;i<sizeof(recvBuf);i++){
				ALOGD("%.2X ", recvBuf[i]);}
        }
    }

    if(ret == 0)
    {
        ALOGD("\n==SWP_TEST_CMD2==");
        memset(recvBuf,0,sizeof(recvBuf));
        ret = sendrecv(fp,SWP_TEST_CMD2,sizeof(SWP_TEST_CMD2),recvBuf);
        if(ret == 0)
        {
			for (i=0;i<sizeof(recvBuf);i++){
				ALOGD("%.2X ", recvBuf[i]);}
        }
    }

    if(ret == 0)
    {
        ALOGD("\n==READ SWP==");
        memset(recvBuf,0,sizeof(recvBuf));
        ret = read(fp, &recvBuf[0], _read_sample_test_size);
        if(ret >= 0)
        {
			for (i=0;i<sizeof(recvBuf);i++){
				ALOGD("%.2X ", recvBuf[i]);}
			if(recvBuf[3]==0x00)
			{
                ret = 0;
			    ALOGD("\nSWP LINK OK\n");
			} else {
                ret = -1;
                ALOGD("\nSWP LINK NOK\n");
            }
        }
    }

    ALOGD("test swp end");
    close(fp);
    return ret;
}

int testNfcRfOn()
{
    unsigned char recvBuf[50];
    unsigned char CORE_RESET_CMD[] ={0x20, 0x00, 0x01, 0x01};
    unsigned char CORE_INIT_CMD[] ={0x20,0x01,0x00};
    unsigned char RF_COMMAND_1[] ={0x2F, 0x02, 0x00};
    unsigned char RF_COMMAND_2[] ={0x2F, 0x00, 0x01, 0x00};
	unsigned char TEST_RF_FIELD_CMD_ON[] ={0x2F, 0x3D, 0x02, 0x20, 0x01};
    int ret = 0, i, rspLen = 0;
    int fp;

    fp = open("/dev/pn544", O_RDWR);
    if( fp < 0 ) {ret = -1;
        ALOGE("failed to open /dev/pn544");
        return -1;
    }

    if(ret == 0)
    {
        ALOGD("\n==Hardware reseting==");
        ioctl(fp, PN544_SET_PWR, 1);
        ioctl(fp, PN544_SET_PWR, 0);
        ioctl(fp, PN544_SET_PWR, 1);
    }

    if(ret == 0)
    {
        ALOGD("\n==CORE_RESET_CMD==");
        memset(recvBuf,0,sizeof(recvBuf));
        ret = sendrecv(fp,CORE_RESET_CMD,sizeof(CORE_RESET_CMD),recvBuf);
        if(ret == 0)
        {
            ALOGD("\nConfiguration Status: %02x",recvBuf[3]);
            ALOGD("\nNCI Version: %02x",recvBuf[4]);
        }
    }

    if(ret == 0)
    {
        ALOGD("\n==CORE_INIT_CMD==");
        memset(recvBuf,0,sizeof(recvBuf));
        ret = sendrecv(fp,CORE_INIT_CMD,sizeof(CORE_INIT_CMD),recvBuf);
        if(ret == 0 && recvBuf[3] == 0x00)
        {
            rspLen = recvBuf[2];
            ALOGD("\nFirmware Version Number: %02x,%02x,%02x",recvBuf[3+rspLen-3],recvBuf[3+rspLen-2],recvBuf[3+rspLen-1]);
        }
    }

    if(ret == 0)
    {
        ALOGD("\n==RF_COMMAND_1==");
        memset(recvBuf,0,sizeof(recvBuf));
        ret = sendrecv(fp,RF_COMMAND_1,sizeof(RF_COMMAND_1),recvBuf);
        if(ret == 0)
        {
			for (i=0;i<sizeof(recvBuf);i++){
				ALOGD("%.2X ", recvBuf[i]);}
        }
    }

    if(ret == 0)
    {
        ALOGD("\n==RF_COMMAND_2==");
        memset(recvBuf,0,sizeof(recvBuf));
        ret = sendrecv(fp,RF_COMMAND_2,sizeof(RF_COMMAND_2),recvBuf);
        if(ret == 0)
        {
			for (i=0;i<sizeof(recvBuf);i++){
				ALOGD("%.2X ", recvBuf[i]);}
        }
    }

    if(ret == 0)
    {
        ALOGD("\n==TEST_RF_FIELD_CMD_ON==");
        memset(recvBuf,0,sizeof(recvBuf));
        ret = sendrecv(fp,TEST_RF_FIELD_CMD_ON,sizeof(TEST_RF_FIELD_CMD_ON),recvBuf);
        if(ret == 0)
        {
			for (i=0;i<sizeof(recvBuf);i++){
				ALOGD("%.2X ", recvBuf[i]);}
        }
    }

    ALOGD("RF on end");
    close(fp);
    return ret;
}

int testNfcRfOff()
{
    unsigned char recvBuf[50];
    unsigned char CORE_RESET_CMD[] ={0x20, 0x00, 0x01, 0x01};
    unsigned char CORE_INIT_CMD[] ={0x20,0x01,0x00};
    unsigned char RF_COMMAND_1[] ={0x2F, 0x02, 0x00};
    unsigned char RF_COMMAND_2[] ={0x2F, 0x00, 0x01, 0x00};
	unsigned char TEST_RF_FIELD_CMD_ON[] ={0x2F, 0x3D, 0x02, 0x20, 0x01};
    int ret = 0, i, rspLen = 0;
    int fp;

    fp = open("/dev/pn544", O_RDWR);
    if( fp < 0 ) {ret = -1;
        ALOGE("failed to open /dev/pn544");
        return -1;
    }

    if(ret == 0)
    {
        ALOGD("\n==Hardware reseting==");
        ioctl(fp, PN544_SET_PWR, 1);
        ioctl(fp, PN544_SET_PWR, 0);
        ioctl(fp, PN544_SET_PWR, 1);
    }

    if(ret == 0)
    {
        ALOGD("\n==CORE_RESET_CMD==");
        memset(recvBuf,0,sizeof(recvBuf));
        ret = sendrecv(fp,CORE_RESET_CMD,sizeof(CORE_RESET_CMD),recvBuf);
        if(ret == 0)
        {
            ALOGD("\nConfiguration Status: %02x",recvBuf[3]);
            ALOGD("\nNCI Version: %02x",recvBuf[4]);
        }
    }

    if(ret == 0)
    {
        ALOGD("\n==CORE_INIT_CMD==");
        memset(recvBuf,0,sizeof(recvBuf));
        ret = sendrecv(fp,CORE_INIT_CMD,sizeof(CORE_INIT_CMD),recvBuf);
        if(ret == 0 && recvBuf[3] == 0x00)
        {
            rspLen = recvBuf[2];
            ALOGD("\nFirmware Version Number: %02x,%02x,%02x",recvBuf[3+rspLen-3],recvBuf[3+rspLen-2],recvBuf[3+rspLen-1]);
        }
    }

    ALOGD("RF off end");
    close(fp);
    return ret;
}

int testNfcReadA()
{
    unsigned char recvBuf[50];
    unsigned char CORE_RESET_CMD[] ={0x20, 0x00, 0x01, 0x01};
    unsigned char CORE_INIT_CMD[] ={0x20,0x01,0x00};
    unsigned char READ_COMMAND_1[] ={0x20, 0x02, 0x05, 0x01, 0xA0, 0x44, 0x01, 0x00};
    unsigned char READ_CONFIG_A007[] = {0x21,0x03,0x0D,0x06,0x00,0x01,0x01,0x01,0x02,0x01,0x03,0x01,0x05,0x01,0x70,0x01};
    int ret = 0, i, rspLen = 0;
    int fp;

    fp = open("/dev/pn544", O_RDWR | O_NONBLOCK);
    if( fp < 0 ) {ret = -1;
        ALOGE("failed to open /dev/pn544");
        return -1;
    }

    if(ret == 0)
    {
        ALOGD("\n==Hardware reseting==");
        ioctl(fp, PN544_SET_PWR, 1);
        ioctl(fp, PN544_SET_PWR, 0);
        ioctl(fp, PN544_SET_PWR, 1);
    }

    if(ret == 0)
    {
        ALOGD("\n==CORE_RESET_CMD==");
        memset(recvBuf,0,sizeof(recvBuf));
        ret = sendrecv(fp,CORE_RESET_CMD,sizeof(CORE_RESET_CMD),recvBuf);
        if(ret == 0)
        {
            ALOGD("\nConfiguration Status: %02x",recvBuf[3]);
            ALOGD("\nNCI Version: %02x",recvBuf[4]);
        }
    }

    if(ret == 0)
    {
        ALOGD("\n==CORE_INIT_CMD==");
        memset(recvBuf,0,sizeof(recvBuf));
        ret = sendrecv(fp,CORE_INIT_CMD,sizeof(CORE_INIT_CMD),recvBuf);
        if(ret == 0 && recvBuf[3] == 0x00)
        {
            rspLen = recvBuf[2];
            ALOGD("\nFirmware Version Number: %02x,%02x,%02x",recvBuf[3+rspLen-3],recvBuf[3+rspLen-2],recvBuf[3+rspLen-1]);
        }
    }

    if(ret == 0)
    {
        ALOGD("\n==READ_COMMAND_1==");
        memset(recvBuf,0,sizeof(recvBuf));
        ret = sendrecv(fp,READ_COMMAND_1,sizeof(READ_COMMAND_1),recvBuf);
        if(ret == 0)
        {
			for (i=0;i<sizeof(recvBuf);i++){
				ALOGD("%.2X ", recvBuf[i]);}
        }
    }

    if(ret == 0)
    {
        ALOGD("\n==READ_CONFIG_A007==");
        memset(recvBuf,0,sizeof(recvBuf));
        ret = sendrecv(fp,READ_CONFIG_A007,sizeof(READ_CONFIG_A007),recvBuf);
        if(ret == 0)
        {
			for (i=0;i<sizeof(recvBuf);i++){
				ALOGD("%.2X ", recvBuf[i]);}
        }
    }

    if(ret == 0)
    {
        ALOGD("\n==TEST_RF_FIELD_CMD_ON==");
        memset(recvBuf,0,sizeof(recvBuf));
        clock_t t0,t1;
        t0 = clock();
        //int timeout = 0;
        while ((ret = read(fp, &recvBuf[0], _read_sample_test_size)) < 0) {
            t1 = clock() - t0;
            if (t1 >= 2 * CLOCKS_PER_SEC) {
                ALOGD("\n==TEST_RF_FIELD_CMD_ON== read timeout");
                break;
            }
        }
        //ret = read(fp, &recvBuf[0], _read_sample_test_size);
        if(ret == 0)
        {
			for (i=0;i<sizeof(recvBuf);i++){
				ALOGD("%.2X ", recvBuf[i]);}
			if(recvBuf[0]==0x61&&recvBuf[1]==0x05)
			{
				return 0;
			} else {
				return -1;
            }
        }
    }

    ALOGD("read A end");
    close(fp);
    return ret;
}
