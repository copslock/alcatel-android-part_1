/* Copyright (C) 2016 Tcl Corporation Limited */
//TCL monster add by liuqin for permission 2016-11-11 start
package android.util;

import java.util.HashSet;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.PermissionInfo;
import android.os.SystemProperties;
import android.text.TextUtils;
/**
 * {@hide}
 */
public class MstCheckPermission {
	public static final String PREMIUM_SMS = "android.permission.PREMIUM_SMS";
	public static final String POST_NOTIFICATION = "android.permission.POST_NOTIFICATION";

    private static HashSet<String> mCheckPermissions = new HashSet<String>();
    static {
        mCheckPermissions.add("android.permission-group.CONTACTS");
        mCheckPermissions.add("android.permission-group.CALENDAR");
        mCheckPermissions.add("android.permission-group.SMS");
        mCheckPermissions.add("android.permission-group.STORAGE");
        mCheckPermissions.add("android.permission-group.LOCATION");
        mCheckPermissions.add("android.permission-group.PHONE");
        mCheckPermissions.add("android.permission-group.MICROPHONE");
        mCheckPermissions.add("android.permission-group.CAMERA");
        mCheckPermissions.add("android.permission-group.SENSORS");

        mCheckPermissions.add("android.permission-group.INSTALL_SHORTCUT");
        mCheckPermissions.add("android.permission-group.SYSTEM_ALERT_WINDOW");
        mCheckPermissions.add("com.android.launcher.action.INSTALL_SHORTCUT");
        mCheckPermissions.add("android.permission.SYSTEM_ALERT_WINDOW");

        mCheckPermissions.add("android.permission.WRITE_SETTINGS");
        mCheckPermissions.add("android.permission.PACKAGE_USAGE_STATS");
        mCheckPermissions.add(POST_NOTIFICATION);
        mCheckPermissions.add(PREMIUM_SMS);
        mCheckPermissions.add(Manifest.permission.ACCESS_VR_MANAGER);
        mCheckPermissions.add(Manifest.permission.BIND_DEVICE_ADMIN);
    }

    public static HashSet<String> mSpecialPerHs = new HashSet<String>();
    static {
        mSpecialPerHs.add("android.permission.CHANGE_WIFI_STATE");
        mSpecialPerHs.add("android.permission.BLUETOOTH_ADMIN");
    }

    private static HashSet<String> mImportantPermissions = new HashSet<String>();
    static {
        mImportantPermissions.add("android.permission-group.INSTALL_SHORTCUT");
        mImportantPermissions.add("android.permission-group.SYSTEM_ALERT_WINDOW");
        mImportantPermissions.add("com.android.launcher.action.INSTALL_SHORTCUT");
        mImportantPermissions.add("android.permission.SYSTEM_ALERT_WINDOW");
    }

    private static HashSet<String> mSeniorPermissions = new HashSet<String>();
    static {
        mSeniorPermissions .add("android.permission.WRITE_SETTINGS");
        mSeniorPermissions .add("android.permission.PACKAGE_USAGE_STATS");
        mSeniorPermissions .add(POST_NOTIFICATION);
        mSeniorPermissions .add(PREMIUM_SMS);
        mSeniorPermissions.add(Manifest.permission.ACCESS_VR_MANAGER);
        mSeniorPermissions.add(Manifest.permission.BIND_MIDI_DEVICE_SERVICE);
    }

    public static HashSet<String> mSepcialPackage = new HashSet<String>();
    static {
    }

    public static boolean isGnAppPermCtrlSupport() {
    	//if cts is running ,shut perm control down
        boolean ctsTestRunning = SystemProperties.get("persist.sys.cts.test").equals("yes");
        return !ctsTestRunning;
    }

    public static boolean containGrpPerm(Context context, String perm){
    	perm = mstGetPermGroupName(context.getPackageManager(), perm);
    	return mCheckPermissions.contains(perm);
    }

    public static boolean containImportantGrpPerm(Context context, String perm){
    	perm = mstGetPermGroupName(context.getPackageManager(), perm);
    	return mImportantPermissions.contains(perm);
    }

    public static boolean containSeniorGrpPerm(Context context, String perm){
    	perm = mstGetPermGroupName(context.getPackageManager(), perm);
    	return mSeniorPermissions.contains(perm);
    }

    private static String mstGetPermGroupName(PackageManager mPm, String permission) {
    	String groupName = permission;
    	try {
    		PermissionInfo permissionInfo = mPm.getPermissionInfo(permission, 0);
    		groupName = permissionInfo.group;
    		if(TextUtils.isEmpty(groupName)) {
    			groupName = permission;
    		}
		} catch (Exception e) {
		}
        return groupName;
    }
}
//TCL monster add by liuqin for permission 2016-11-11 end
