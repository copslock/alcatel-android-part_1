/* Copyright (C) 2016 Tcl Corporation Limited */
package com.android.server;

import java.security.MessageDigest;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.util.Log;

/**
 *
 * 检查手机管家的合法性
 *
 */
public class Md5CheckUtil {

	private Context mContext;

	private Md5CheckUtil(Context context) {
		mContext = context;
		regTMSInstallReceiver();
	}

	private volatile static Md5CheckUtil mInstance;

	public static Md5CheckUtil getInstace(Context context){
		if(mInstance != null){
			return mInstance;
		}else{
			synchronized (Md5CheckUtil.class) {
	            if (mInstance == null) {        // Double checked
	            	mInstance = new Md5CheckUtil(context);
	            }
			}
			return mInstance;
		}
	}

	// */add for check tencent TMS.
	public final static String TMSPKN = "com.tencent.qqpimsecure";
	private final String TMSMD5 = "00b1208638de0fcd3e920886d658daf6";
	private static int isTMSExist = 0; // 1:exist; -1:not exist; 0:not check
	private static int isTMSMd5Same = 0; // 1:equal; -1:not equal; 0:not check
	private static int TMSUid = 0; // 1:equal; -1:not equal; 0:not check
	private BroadcastReceiver mPkgReceiver;

	private void regTMSInstallReceiver() {
		IntentFilter filter = new IntentFilter();
		filter.addAction(Intent.ACTION_PACKAGE_ADDED);
		filter.addAction(Intent.ACTION_PACKAGE_REMOVED);
		filter.addDataScheme("package");
		mPkgReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				String packagename = intent.getDataString().substring(8);
				if (TMSPKN.equals(packagename)) {
					String action = intent.getAction();
					if (Intent.ACTION_PACKAGE_ADDED.equals(action)) {
						isTMSExist = 1;
					} else if (Intent.ACTION_PACKAGE_REMOVED.equals(action)) {
						isTMSExist = -1;
						isTMSMd5Same = 0;
						TMSUid = 0;
					}
				}
			}
		};
		mContext.registerReceiver(mPkgReceiver, filter);
	}

	public boolean checkTMSPackageName(int callingUid) {
		boolean ret = false;

		if (-1 == isTMSExist) {
			return ret;
		}

		if (0 == TMSUid) {
			try {
				PackageManager pm = mContext.getPackageManager();
				ApplicationInfo appinfo = pm.getApplicationInfo(TMSPKN,
						PackageManager.GET_ACTIVITIES);
				TMSUid = appinfo.uid;
				isTMSExist = 1;
			} catch (Exception ex) {
				isTMSExist = -1;
			}
		}

		if (TMSUid == callingUid) {
			ret = true;
		}
		Log.e("mayitx", "checkTMSPackageName =" + ret);
		return ret;

	}


	public boolean checkIsTmsSetMode(int uid,String packageName){
		if(TMSPKN.equals(packageName)){
			if(checkTMSPackageName(uid)){
				if(checkTMSMd5()){
					return true;
				}
			}

		}
		return false;
	}


	public boolean checkTMSMd5() {
		//管家没有安装
		boolean ret = false;
		if (0 == isTMSMd5Same) {
			String md5 = getSign();
			if (TMSMD5.equals(md5)) {
				ret = true;
				isTMSMd5Same = 1;
			} else {
				isTMSMd5Same = -1;
			}
		} else if (1 == isTMSMd5Same) {
			ret = true;
		}
		return ret;
	}

	private String getMessageDigest(byte[] paramArrayOfByte) {
		char[] arrayOfChar1 = { 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 97, 98,
				99, 100, 101, 102 };
		try {
			MessageDigest localMessageDigest = MessageDigest.getInstance("MD5");
			localMessageDigest.update(paramArrayOfByte);
			byte[] arrayOfByte = localMessageDigest.digest();
			int i = arrayOfByte.length;
			char[] arrayOfChar2 = new char[i * 2];
			int j = 0;
			int k = 0;
			while (true) {
				if (j >= i)
					return new String(arrayOfChar2);
				int m = arrayOfByte[j];
				int n = k + 1;
				arrayOfChar2[k] = arrayOfChar1[(0xF & m >>> 4)];
				k = n + 1;
				arrayOfChar2[n] = arrayOfChar1[(m & 0xF)];
				j++;
			}
		} catch (Exception localException) {
		}
		return null;
	}


	private String getSign() {
		Signature[] arrayOfSignature = getRawSignature(mContext, TMSPKN);
		if ((arrayOfSignature == null) || (arrayOfSignature.length == 0)) {
			return null;
		}
		return (String) (getMessageDigest(arrayOfSignature[0].toByteArray()));
	}

	private Signature[] getRawSignature(Context paramContext, String paramString) {
		if ((paramString == null) || (paramString.length() == 0)) {
			return null;
		}
		PackageManager localPackageManager = paramContext.getPackageManager();
		PackageInfo localPackageInfo;
		try {
			localPackageInfo = localPackageManager.getPackageInfo(paramString,
					64);
			if (localPackageInfo == null) {
				return null;
			}
		} catch (PackageManager.NameNotFoundException localNameNotFoundException) {
			return null;
		}
		return localPackageInfo.signatures;
	}
}
