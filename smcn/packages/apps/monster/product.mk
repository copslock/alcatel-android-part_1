ifeq ($(SMCN_ROM_CONTROL_FLAG),true)

ANDROID_PARTNER_MONSTER_HOME := smcn/packages/apps/monster

define all_files_under_subdir
$(foreach files, $(notdir $(wildcard $1)),$(dir $(1))$(files):$(2)/$(files)) 
endef

PRODUCT_COPY_FILES += \
        $(call all_files_under_subdir,$(ANDROID_PARTNER_MONSTER_HOME)/Monster_Changer/desktop/*.jpg,system/monster/wallpaper/desktop)

PRODUCT_COPY_FILES += \
        $(call all_files_under_subdir,$(ANDROID_PARTNER_MONSTER_HOME)/Monster_Changer/keyguard/Pure/*.jpg,system/monster/wallpaper/keyguard/Pure) \
        $(call all_files_under_subdir,$(ANDROID_PARTNER_MONSTER_HOME)/Monster_Changer/keyguard/Elegant/*.jpg,system/monster/wallpaper/keyguard/Elegant)

PRODUCT_COPY_FILES += \
        $(call all_files_under_subdir,$(ANDROID_PARTNER_MONSTER_HOME)/Monster_Changer/keyguard/*.xml,system/monster/wallpaper/keyguard) \
        $(call all_files_under_subdir,$(ANDROID_PARTNER_MONSTER_HOME)/Monster_Changer/keyguard/Pure/*.xml,system/monster/wallpaper/keyguard/Pure) \
        $(call all_files_under_subdir,$(ANDROID_PARTNER_MONSTER_HOME)/Monster_Changer/keyguard/Elegant/*.xml,system/monster/wallpaper/keyguard/Elegant)

PRODUCT_COPY_FILES += \
       $(call all_files_under_subdir,$(ANDROID_PARTNER_MONSTER_HOME)/Monster_Changer/bin/monster_data.sh,system/etc)

PRODUCT_COPY_FILES += \
       $(call all_files_under_subdir,$(ANDROID_PARTNER_MONSTER_HOME)/Monster_ThemeManager/icons/config.xml,system/monster/theme/system_default/default/icons) \
       $(call all_files_under_subdir,$(ANDROID_PARTNER_MONSTER_HOME)/Monster_ThemeManager/icons/drawable-xxhdpi/*,system/monster/theme/system_default/default/icons/drawable-xxhdpi)

PRODUCT_PACKAGES += Monster_AppIcons
PRODUCT_PACKAGES += Monster_APPManager
PRODUCT_PACKAGES += Monster_Calculator
PRODUCT_PACKAGES += Monster_Calendar
PRODUCT_PACKAGES += Monster_CalendarProvider
PRODUCT_PACKAGES += Monster_CallLogProvider
PRODUCT_PACKAGES += Monster_CallSettings
PRODUCT_PACKAGES += Monster_Changer
PRODUCT_PACKAGES += Monster_Contacts
PRODUCT_PACKAGES += Monster_ContactsCommon
PRODUCT_PACKAGES += Monster_ContactsProvider
PRODUCT_PACKAGES += Monster_DeskClock
PRODUCT_PACKAGES += Monster_Dialer
PRODUCT_PACKAGES += Monster_DownLoader
PRODUCT_PACKAGES += Monster_Gallery2
PRODUCT_PACKAGES += Monster_InCallUI
PRODUCT_PACKAGES += Monster_Launcher
PRODUCT_PACKAGES += Monster_Market
PRODUCT_PACKAGES += Monster_Mms
PRODUCT_PACKAGES += Monster_MmsService
PRODUCT_PACKAGES += Monster_NetManage
PRODUCT_PACKAGES += Monster_Permissions
PRODUCT_PACKAGES += Monster_Settings
PRODUCT_PACKAGES += Monster_SystemUpgrade
PRODUCT_PACKAGES += Monster_Telecomm
PRODUCT_PACKAGES += Monster_Telephony
PRODUCT_PACKAGES += Monster_TelephonyProvider
PRODUCT_PACKAGES += Monster_ThemeManager
PRODUCT_PACKAGES += Monster_TmsService
PRODUCT_PACKAGES += Monster_PhoneCommon
PRODUCT_PACKAGES += MingPian_CC
PRODUCT_PACKAGES += Monster_AutoStart
PRODUCT_PACKAGES += Monster_SystemUI
PRODUCT_PACKAGES += Monster_Cloud
PRODUCT_PACKAGES += Monster_PackageInstaller
PRODUCT_PACKAGES += AccountService
PRODUCT_PACKAGES += Monster_Reject
endif
