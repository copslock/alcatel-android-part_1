LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional
LOCAL_MODULE := Monster_Gallery2
LOCAL_SRC_FILES := $(patsubst $(LOCAL_PATH)/%,%,$(shell find $(LOCAL_PATH) -name "*.apk"))
LOCAL_MODULE_CLASS := APPS
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_OVERRIDES_PACKAGES := Gallery2
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)

LOCAL_MULTILIB := 64

LOCAL_PREBUILT_JNI_LIBS := \
    libs/arm64/libjni_eglfence.so \
    libs/arm64/libjni_filtershow_filters.so \
    libs/arm64/librsjni.so \
    libs/arm64/libjni_jpegstream.so

include $(BUILD_PREBUILT)
