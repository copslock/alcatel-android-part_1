# All Rights Reserved
#
# PowerMo Confidential
# Copyright 2012 PowerMo information tech ltd all rights reserved.
# The source code contained or described herein and all documents related to
# the source code ("material") are owned by PowerMo information tech ltd or its
# suppliers or licensors.
#
# Title to the material remains with PowerMo information tech ltd or its
# suppliers and licensors. the material contains trade secrets and proprietary
# and confidential information of PowerMo or its suppliers and licensors.
# The material is protected by worldwide copyright and trade secret
# laws and treaty provisions. no part of the material may be used, copied,
# reproduced, modified, published, uploaded, posted, transmitted, distributed,
# or disclosed in any way without PowerMo's prior express written permission.
#
# No license under any patent, copyright, trade secret or other intellectual
# property right is granted to or conferred upon you by disclosure or delivery
# of the materials, either expressly, by implication, inducement, estoppel or
# otherwise. Any license under such intellectual property rights must be
# express and approved by PowerMo in writing.

LOCAL_PATH:= $(call my-dir)

##################################################
include $(CLEAR_VARS)
LOCAL_MODULE := libsmartinstance
LOCAL_SRC_FILES := ./lib/$(TARGET_CPU_ABI)/libsmartinstance.a
ifneq ($(TARGET_2ND_ARCH),)
LOCAL_SRC_FILES_32 := ./lib/$(TARGET_2ND_CPU_ABI)/libsmartinstance.a
else
LOCAL_SRC_FILES_32 := ./lib/$(TARGET_CPU_ABI)/libsmartinstance.a
endif
LOCAL_MODULE_SUFFIX := $(suffix $(LOCAL_SRC_FILES))
LOCAL_MODULE_CLASS := STATIC_LIBRARIES
LOCAL_MULTILIB := both
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_WHOLE_STATIC_LIBRARIES += libsmartinstance
LOCAL_PRELINK_MODULE := false
LOCAL_MODULE_TAGS := optional
LOCAL_SHARED_LIBRARIES += liblog
LOCAL_MODULE := libsmartinstance
include $(BUILD_SHARED_LIBRARY)

##################################################
include $(CLEAR_VARS)
LOCAL_MODULE := libsmartinstance_sdcard
LOCAL_SRC_FILES := ./lib/$(TARGET_CPU_ABI)/libsmartinstance_sdcard.a
ifneq ($(TARGET_2ND_ARCH),)
LOCAL_SRC_FILES_32 := ./lib/$(TARGET_2ND_CPU_ABI)/libsmartinstance_sdcard.a
else
LOCAL_SRC_FILES_32 := ./lib/$(TARGET_CPU_ABI)/libsmartinstance_sdcard.a
endif
LOCAL_MODULE_SUFFIX := $(suffix $(LOCAL_SRC_FILES))
LOCAL_MODULE_CLASS := STATIC_LIBRARIES
LOCAL_MULTILIB := both
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_WHOLE_STATIC_LIBRARIES += libsmartinstance_sdcard
LOCAL_PRELINK_MODULE := false
LOCAL_MODULE_TAGS := optional
LOCAL_SHARED_LIBRARIES += liblog libcutils libpackagelistparser
LOCAL_MODULE := libsmartinstance_sdcard
include $(BUILD_SHARED_LIBRARY)

##################################################
include $(CLEAR_VARS)
LOCAL_MODULE := libsmartinstance_zygote
LOCAL_SRC_FILES := ./lib/$(TARGET_CPU_ABI)/libsmartinstance_zygote.a
ifneq ($(TARGET_2ND_ARCH),)
LOCAL_SRC_FILES_32 := ./lib/$(TARGET_2ND_CPU_ABI)/libsmartinstance_zygote.a
else
LOCAL_SRC_FILES_32 := ./lib/$(TARGET_CPU_ABI)/libsmartinstance_zygote.a
endif
LOCAL_MODULE_SUFFIX := $(suffix $(LOCAL_SRC_FILES))
LOCAL_MODULE_CLASS := STATIC_LIBRARIES
LOCAL_MULTILIB := both
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)

ZYGOTE_C_INCLUDES :=     $(JNI_H_INCLUDE)     $(LOCAL_PATH)/android/graphics     $(LOCAL_PATH)/../../libs/hwui     $(LOCAL_PATH)/../../../native/opengl/libs     $(call include-path-for, bluedroid)     $(call include-path-for, libhardware)/hardware     $(call include-path-for, libhardware_legacy)/hardware_legacy     $(TOP)/frameworks/av/include     $(TOP)/frameworks/base/media/jni     $(TOP)/system/media/camera/include     $(TOP)/system/netd/include     external/pdfium/core/include/fpdfapi     external/pdfium/core/include/fpdfdoc     external/pdfium/fpdfsdk/include     external/pdfium/public     external/skia/src/core     external/skia/src/effects     external/skia/src/images     external/sqlite/dist     external/sqlite/android     external/expat/lib     external/tremor/Tremor     external/jpeg     external/harfbuzz_ng/src     frameworks/opt/emoji     libcore/include     $(call include-path-for, audio-utils)     frameworks/minikin/include     external/freetype/include 	frameworks/base/core/jni

ZYGOTE_SHARE_LIBRARIES :=     libmemtrack     libandroidfw     libexpat     libnativehelper     liblog     libcutils     libutils     libbinder     libnetutils     libui     libgui     libinput     libinputflinger     libcamera_client     libcamera_metadata     libskia     libsqlite     libEGL     libGLESv1_CM     libGLESv2     libETC1     libhardware     libhardware_legacy     libselinux     libsonivox     libcrypto     libssl     libicuuc     libicui18n     libmedia     libjpeg     libusbhost     libharfbuzz_ng     libz     libaudioutils     libpdfium     libimg_utils     libnetd_client     libradio     libsoundtrigger     libminikin     libprocessgroup     libnativebridge     libradio_metadata     libhwui     libdl

LOCAL_WHOLE_STATIC_LIBRARIES += libsmartinstance_zygote
LOCAL_PRELINK_MODULE := false
LOCAL_MODULE_TAGS := optional
LOCAL_SHARED_LIBRARIES += liblog libcutils
LOCAL_MODULE := libsmartinstance_zygote

LOCAL_C_INCLUDES += $(ZYGOTE_C_INCLUDES)
LOCAL_SHARED_LIBRARIES += $(ZYGOTE_SHARE_LIBRARIES)

include $(BUILD_SHARED_LIBRARY)
