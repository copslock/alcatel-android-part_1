package com.cmx.cmplus.superclone;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.cmx.cmplus.SmartContainerConfig;

public class CloneReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("CloneReceiver",  "receive " + intent) ;
        SmartContainerConfig.init();
    }
}
