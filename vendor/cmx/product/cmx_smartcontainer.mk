
ifeq ($(BOARD_CONFIG_ENABLE_CLONE), true)
SKIP_BOOT_JARS_CHECK := true
PRODUCT_PACKAGES += \
                    CMVirtualBox \
                    container-service \
                    container-framework \
                    libsmartinstance

ifneq ($(TARGET_BUILD_VARIANT),user)
    PRODUCT_PACKAGES += \
                    SuperClone
endif



PRODUCT_SYSTEM_SERVER_JARS += \
    container-service

PRODUCT_BOOT_JARS += \
    container-framework

BOARD_SEPOLICY_DIRS += \
    vendor/cmx/product/sepolicy

BOARD_SEPOLICY_UNION += \
    service_contexts \
    installd.te \
    service.te

PRODUCT_PROPERTY_OVERRIDES += persist.sys.cmplus.oemwhitelist=/system/etc/container/extra_oem_clone_box_policy.xml

$(call inherit-product-if-exists, vendor/cmx/product/copyfiles/copyfiles.mk)

endif #BOARD_CONFIG_ENABLE_CLONE
