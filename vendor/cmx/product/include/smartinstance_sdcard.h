extern bool isSmartContainerEnabled();
extern int fuse_reply_entry_hook(struct fuse* fuse, __u64 unique,
        struct node* parent, const char* name, const char* actual_name,
        const char* path);
extern int fuse_reply_attr_hook(struct fuse* fuse, __u64 unique,
        const struct node* node, const char* path);
extern void attr_from_stat_hook(struct fuse* fuse, struct fuse_attr *attr,
        const struct stat *s, const struct node* node);

