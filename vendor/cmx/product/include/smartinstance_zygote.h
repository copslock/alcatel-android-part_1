extern bool isSmartContainerEnabled();
extern void smartinstance_zygote_hook_pre(JNIEnv* env, uid_t uid, gid_t gid, jintArray javaGids, jobjectArray javaRlimits, bool is_system_server, jintArray fdsToClose, jstring dataDir);
extern void smartinstance_zygote_hook_post(JNIEnv* env, uid_t uid, gid_t gid, jintArray javaGids, jobjectArray javaRlimits, bool is_system_server, jintArray fdsToClose, jstring dataDir);
extern void smartinstance_zygote_hook_uid_gid(JNIEnv* env, uid_t uid, gid_t gid, jintArray javaGids, jobjectArray javaRlimits, bool is_system_server, jintArray fdsToClose, jstring dataDir);

