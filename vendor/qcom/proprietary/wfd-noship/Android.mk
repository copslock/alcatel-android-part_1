# Copyright (C) 2016 Tcl Corporation Limited
#[PLATFORM]-Add-BEGIN by TCTNB.(QianCheng.Zhao), 2015/7/02 PR-403393,, reason HDCP function.
ifeq ($(TCT_TARGET_HDCP_ENABLE), true)
ifeq ($(call is-vendor-board-platform,QCOM),true)
ifneq ($(call is-board-platform,copper),true)
include $(call all-subdir-makefiles)
endif
endif
endif
#[PLATFORM]-Add-END by TCTNB.(QianCheng.Zhao)
