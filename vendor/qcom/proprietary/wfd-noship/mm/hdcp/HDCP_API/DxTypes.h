
 
 #ifndef DX_TYPES_H
#define DX_TYPES_H

#ifndef DX_NULL
#define DX_NULL 				0
#endif
//! \file DxTypes.h
//! \brief Defines the basic data types used in Descretix code.

#define __STDC_FORMAT_MACROS

#include <stdint.h>
#include <stddef.h>
#include <inttypes.h>
#include <stdbool.h>

//! Unsigned integer type. Its size is platform dependent.
//typedef unsigned int       		DxUint;
//! Unsigned value of 8 bits
//typedef unsigned char        	DxUint8;
//! Unsigned value of 16 bits
//typedef unsigned short       	DxUint16;
//! Unsigned value of 32 bits
//typedef unsigned long        	DxUint32;
//! Unsigned value of 64 bits
//typedef unsigned long long		DxUint64;

//! Signed integer type. Its size is platform dependent.
//typedef signed int				DxInt;
//! Signed value of 8 bits
//typedef signed char				DxInt8;
//! Signed value of 16 bits
//typedef signed short			DxInt16;
//! Signed value of 32 bits
//typedef signed long				DxInt32;
//! Signed value of 64 bits
//typedef signed long long		DxInt64;
//! Real number. Its size is platform dependent.
//typedef float			        DxFloat;

//! Character of 8 bits
//typedef char			        DxChar;
//! Wide character of 16 bits
typedef uint16_t 				DxWideChar16;
//! Wide character of 32 bits
//typedef DxUint32				DxWideChar32;
//! Byte value
//typedef DxUint8                 DxByte;


// pointer integer types
//! unsigned integer pointer type (can safely house a pointer)
//typedef  uintptr_t                DxUintPtr;
//! unsigned integer pointer type (can safely house a pointer)
//typedef  intptr_t                DxIntPtr;

// size types
//! signed size type
//typedef  ssize_t                  DxSSize;
//! unsigned size type
//typedef  size_t                   DxSize;

//! Enumeration of true and false. false = 0, true = 1
//typedef enum {
//    false = 0,
//    true = 1
//} EDxBool;

//! Boolean value. Represented by 32 bits
//typedef uint32_t                DxBool;

//! Unsigned value of 32 bits used for return codes.

/*
#define BUILD_BUG_ON(condition) ((void)sizeof(char[1 - 2*!!(condition)]))

#ifndef __DX_STATUS_DEFINED__
#define __DX_STATUS_DEFINED__
typedef uint32_t	 		    DxStatus;
#else

void DxTemp_zzzyzzzyz()
{
	BUILD_BUG_ON((sizeof (uint32_t) !=  sizeof (DxStatus)));
}
#endif
*/


// check that size of DxStatus is exactly 32bit - if defined previously.
#define __DX_CONCAT( x_, y_ )      x_ ## y_
#define DX_CONCAT( x_, y_ )        __DX_CONCAT( x_, y_ )

#define DX_CASSERT(c_)         static const uint8_t DX_CONCAT(__DX__DUMMY__,__LINE__) [ (c_) != 0 ] = {0}    /* Cassert */

#ifndef __DX_STATUS_DEFINED__
#define __DX_STATUS_DEFINED__
typedef uint32_t                      DxStatus;
#else

DX_CASSERT(sizeof (uint32_t) == sizeof(DxStatus)); // compilation errors on this line indicate that DxStatus was previously defined but it's size was not 32bit. 

#endif


#ifndef  DX_INVALID_VALUE 
#define DX_INVALID_VALUE	    0xFFFFFFFF
#else
#if DX_INVALID_VALUE != 0xFFFFFFFF
#error DX_INVALID_VALUE already defined with incorrect value (different from 0xFFFFFFFF)
#endif
#endif

#ifndef  DX_SUCCESS 
#define DX_SUCCESS			    0UL 
#else //DX_SUCEES already defined
#if DX_SUCCESS != 0
#error DX_SUCCESS already defined with incorrect value (different from 0)
#endif
#endif


#ifndef  DX_INFINITE 
#define DX_INFINITE			    0xFFFFFFFF
#else //DX_SUCEES already defined
#if DX_INFINITE != 0xFFFFFFFF
#error DX_INFINITE already defined with incorrect value (different from 0xFFFFFFFF)
#endif
#endif

#ifndef  DX_INFINITE64 
#define DX_INFINITE64			0xFFFFFFFFFFFFFFFFULL
#else //DX_SUCEES already defined
#if DX_INFINITE64 != 0xFFFFFFFFFFFFFFFFULL
#error DX_INFINITE64 already defined with incorrect value (different from 0xFFFFFFFFFFFFFFFFULL)
#endif
#endif

#ifndef DX_INVALID_VALUE64
#define DX_INVALID_VALUE64	    0xFFFFFFFFFFFFFFFFULL
#else //DX_SUCEES already defined
#if DX_INVALID_VALUE64 != 0xFFFFFFFFFFFFFFFFULL
#error DX_INVALID_VALUE64 already defined with incorrect value (different from 0xFFFFFFFFFFFFFFFFULL)
#endif
#endif
#endif
