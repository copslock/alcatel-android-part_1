/*!
 * @file vpp_def.h
 *
 * @cr
 * Copyright (c) 2015 Qualcomm Technologies, Inc.  All Rights Reserved.
 * Qualcomm Technologies Proprietary and Confidential.

 * @services
 */
#ifndef _VPP_DEF_H_
#define _VPP_DEF_H_

#define VPP_UNUSED(x) (void)(x)


enum vpp_bool {
    VPP_FALSE = 0,
    VPP_TRUE = (!VPP_FALSE),
};

typedef enum {
    VPP_RESOLUTION_SD,
    VPP_RESOLUTION_HD,
    VPP_RESOLUTION_FHD,
    VPP_RESOLUTION_UHD,
    VPP_RESOLUTION_MAX,
} t_EVppRes;

typedef enum {
    VPP_COLOR_FMT_NV12_VENUS,
    VPP_COLOR_FMT_MAX,
} t_EVppColorFmt;

typedef enum {
    VPP_IP_HVX,
    VPP_IP_GPU,
    VPP_IP_FRC,
    VPP_IP_MAX,
} t_EVppIp;

/***************************************************************************
 * Function Prototypes
 ***************************************************************************/


#endif /* _VPP_DEF_H_ */
