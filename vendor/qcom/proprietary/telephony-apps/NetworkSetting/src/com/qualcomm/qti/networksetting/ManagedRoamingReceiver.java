/*
 * Copyright (c) 2016 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

package com.qualcomm.qti.networksetting;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import com.android.internal.telephony.PhoneConstants;

import android.util.TctLog;

public class ManagedRoamingReceiver extends BroadcastReceiver {

    private static final String TAG = "ManagedRoamingReceiver";
    private static final String ACTION_MANAGED_ROAMING_IND =
            "codeaurora.intent.action.ACTION_MANAGED_ROAMING_IND";
    @Override
    public void onReceive(Context context, Intent intent) {

        String action = intent.getAction();
        if (action.equals(ACTION_MANAGED_ROAMING_IND)) {
                /* MODIFIED-BEGIN by bo.chen, 2016-11-01,BUG-3273808*/
                TctLog.d(TAG,"feature_networksetting_ShowRoamingManagementForAll_on = "
                        + context.getResources().getBoolean(R.bool.feature_networksetting_ShowRoamingManagementForAll_on));
                if (!context.getResources().getBoolean(R.bool.feature_networksetting_ShowRoamingManagementForAll_on)){
                    TctLog.d(TAG,"In all conditions does not show roaming management.");
                    return;
                }
                /* MODIFIED-END by bo.chen,BUG-3273808*/
                int subscription = intent.getIntExtra(PhoneConstants.SUBSCRIPTION_KEY,
                        SubscriptionManager.getDefaultSubscriptionId());

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/12/2016, SOLUTION-2476050
//new feature Roaming manager
                if (context.getResources().getBoolean(R.bool.feature_networksetting_notShowRoamingManagementInSimPin_on)) {
                    int slotId = SubscriptionManager.getSlotId(subscription);
                    int simState = TelephonyManager.getDefault().getSimState(slotId);
                    if (simState == TelephonyManager.SIM_STATE_PIN_REQUIRED
                        || simState == TelephonyManager.SIM_STATE_PUK_REQUIRED
                        || simState == TelephonyManager.SIM_STATE_NETWORK_LOCKED) {
                        TctLog.i(TAG, "In the SIM PIN verification interface does not show roaming management.");
                        return;
                    }
                }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

                Intent createIntent = new Intent();
                createIntent.setClass(context, ManagedRoaming.class);
                createIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                createIntent.putExtra(PhoneConstants.SUBSCRIPTION_KEY, subscription);
                context.startActivity(createIntent);
            }
    }
}
