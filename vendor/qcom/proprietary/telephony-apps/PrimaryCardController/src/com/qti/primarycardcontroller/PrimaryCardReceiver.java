/*
 * Copyright (c) 2015-2016 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

package com.qti.primarycardcontroller;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.Rlog;
import android.telephony.TelephonyManager;
import android.os.SystemProperties;
import android.os.UserHandle;

import com.qti.primarycardcontroller.cttuneaway.CtPrimaryStackMappingController;

public class PrimaryCardReceiver extends BroadcastReceiver {
    private static final String LOG_TAG = "PrimaryCardReceiver";
    private static final String CARRIER_MODE_CT_CLASS_C = "ct_class_c";
    private final String mCarrierMode = SystemProperties.get("persist.radio.carrier_mode", "default");
    private final boolean mIsCtClassC = mCarrierMode.equals(CARRIER_MODE_CT_CLASS_C);
    private CtPrimaryStackMappingController mCtPrimaryStackMappingController;

    @Override
    public void onReceive(Context context, Intent intent) {

        //[FEATURE]-Add-BEGIN by TCTNB.Xijun.Zhang,09/07/2016,Task-2830651,
        //Dual SIM logic development
        if ((UserHandle.myUserId() == 0) &&
            (TelephonyManager.getDefault().getPhoneCount() > 1)) {
            Rlog.d(LOG_TAG, "sys.boot_completed = " + SystemProperties.get("sys.boot_completed"));
            if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())
                    || ("android.phone.PHONE_CREATE".equals(intent.getAction()) && ("1".equals(SystemProperties.get("sys.boot_completed"))))) {
                Rlog.d(LOG_TAG, " invoking PrimarySubSelectionController init");
                PrimarySubSelectionController.init(context);
                if (mIsCtClassC) {
                    Rlog.d(LOG_TAG, " Runing up CtPrimaryStackMappingController");
                    mCtPrimaryStackMappingController = new CtPrimaryStackMappingController(
                            context.getApplicationContext());
                }
            }
        }
        //[FEATURE]-Add-END by TCTNB.Xijun.Zhang,09/07/2016,Task-2830651
    }
}
