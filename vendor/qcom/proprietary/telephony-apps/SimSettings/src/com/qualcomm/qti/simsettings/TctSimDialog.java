/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.qualcomm.qti.simsettings;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.telecom.PhoneAccount;
import android.telecom.PhoneAccountHandle;
import android.telecom.TelecomManager;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.provider.Settings;//[SOLUTION]-Add-BEGIN by TCTNB.(Chuanjun Chen), 09/07/2016, SOLUTION- 2473826 And TASk-2781430

import org.codeaurora.internal.IExtTelephony;

import com.android.internal.telephony.TelephonyProperties;
import com.android.internal.telephony.uicc.SpnOverride;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
public class TctSimDialog{
    private static String TAG = "TctSimDialog";
    // For carrier specific
    private static final String ACTION_CARRIER_PREFERRED_SIM_SWAPPED =
            "com.qualcomm.qti.loadcarrier.preferred_sim_swapped";
    private static final String CARRIER_TARGET_SLOT = "target_slot";

    public static String PREFERRED_SIM = "preferred_sim";
    public static String DIALOG_TYPE_KEY = "dialog_type";
    public static final int INVALID_PICK = -1;
    public static final int DATA_PICK = 0;
    public static final int CALLS_PICK = 1;
    public static final int SMS_PICK = 2;
    public static final int PREFERRED_PICK = 3;

    private static final String SETTING_USER_PREF_DATA_SUB = "user_preferred_data_sub";
    /* ADD-BEGIN by Dingyi for dualsim 2016/08/03 FR 2655692*/
    public static int mDialogType = -1;
    private boolean simnamedup = false;
    private List<SubscriptionInfo> mAvailableSubInfos = null;
    private SubscriptionManager mSubscriptionManager;
    private TelephonyManager mTelephonyManager;
    private Context mContext;
    public static int preferSlodId = -1;
    private Dialog mSimDialog=null;
    private TctPreferredSimChangeListener mTctPreferredSimChangeListener;
    private TctSimDialogCloseListener mTctSimDialogCloseListener;
    private MyHandler mHandler;
    private boolean restoreVoiceSmsSubId;
    /* ADD-END by Dingyi for dualsim 2016/08/03 FR 2655692*/

    public void setPreferredSimChangeListener(TctPreferredSimChangeListener listener){
        mTctPreferredSimChangeListener = listener;
    }
    public void setSimDialogCloseListener(TctSimDialogCloseListener listener){
        mTctSimDialogCloseListener = listener;
    }
    public void showdialog(Context context,int diatype, int preferslod, boolean restore) {

        final int dialogType = diatype;
        preferSlodId = preferslod;
        restoreVoiceSmsSubId = restore;
        mHandler= new MyHandler();
        /* ADD-BEGIN by Dingyi for dualsim 2016/08/03 FR 2655692*/
        mDialogType = dialogType;

        mContext = context;
        mSubscriptionManager = SubscriptionManager.from(mContext);
        mTelephonyManager = TelephonyManager.from(mContext);
        int mNumSlots = mTelephonyManager.getSimCount();
        mAvailableSubInfos = new ArrayList<SubscriptionInfo>(mNumSlots);
        for (int i = 0; i < mNumSlots; ++i) {
            final SubscriptionInfo sir = mSubscriptionManager
                    .getActiveSubscriptionInfoForSimSlotIndex(i);
            mAvailableSubInfos.add(sir);
        }

        CharSequence preName = "";
        simnamedup = false;
        for (int i = 0; i < mAvailableSubInfos.size(); ++i) {
            final SubscriptionInfo sir = mAvailableSubInfos.get(i);
            if (sir != null) {
                CharSequence name = getSimDisplayName(sir);
                if (preName.equals(name)) {
                    simnamedup = true;
                }
                preName = name;
            }
        }
        /* ADD-END by Dingyi for dualsim 2016/08/03 FR 2655692*/

        switch (dialogType) {
            case DATA_PICK:
            case CALLS_PICK:
            case SMS_PICK:
                createDialog(mContext, dialogType).show();
                /* ADD-BEGIN by Dingyi for dualsim 2016/08/03 FR 2655692*/
                if (restoreVoiceSmsSubId) {
                    Log.d(TAG, "need setDefaultVoiceSmsSubId()");
                    setDefaultVoiceSmsSubId();
                }
                /* ADD-END by Dingyi for dualsim 2016/08/03 FR 2655692*/
                break;
            case PREFERRED_PICK:
//                displayPreferredDialog(extras.getInt(PREFERRED_SIM));
                displayPreferredDialog(preferSlodId);
                break;
            default:
                throw new IllegalArgumentException("Invalid dialog type " + dialogType + " sent.");
        }

    }
    /* ADD-BEGIN by Dingyi for dualsim 2016/08/03 FR 2655692*/
    private String getSimDisplayName(SubscriptionInfo sir){
        String displayName = sir.getDisplayName().toString();
        if (TextUtils.isEmpty(displayName)) {
            displayName = getOperatorName(sir);
        }
        return displayName;
    }

    private String getOperatorName(SubscriptionInfo sir){
        String operatorName = mTelephonyManager.getSimOperatorName(sir.getSubscriptionId());
        Log.d(TAG,"getOperatorName, spn = " + operatorName);

        if (TextUtils.isEmpty(operatorName)) {
            operatorName = TelephonyManager.getDefault().getNetworkOperatorName(sir.getSubscriptionId());
            int mSlotId = sir.getSimSlotIndex();
            if (TextUtils.isEmpty(operatorName)) {
                String operator = TelephonyManager.getDefault().getSimOperator(sir.getSubscriptionId());
                SpnOverride mSpnOverride = new SpnOverride();
                String CarrierName = operator;
                if (mSpnOverride.containsCarrier(CarrierName)) {
                    operatorName = mSpnOverride.getSpn(CarrierName);
                } else {
                    operatorName = mContext.getResources().getString(R.string.sim_card_number_title, mSlotId + 1);
                    Log.d(TAG,"getOperatorName, def = " + operatorName);
                }
            }
        }
        return operatorName;
    }
    /* ADD-END by Dingyi for dualsim 2016/08/03 FR 2655692*/

    private void displayPreferredDialog(final int slotId) {
        Resources res = mContext.getResources();
        final SubscriptionInfo sir = SubscriptionManager.from(mContext)
                .getActiveSubscriptionInfoForSimSlotIndex(slotId);

        if (sir != null) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
            alertDialogBuilder.setTitle(R.string.sim_preferred_title);
            alertDialogBuilder.setMessage(res.getString(
                        R.string.sim_preferred_message, sir.getDisplayName()));

            alertDialogBuilder.setPositiveButton(R.string.yes, new
                    DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    final int subId = sir.getSubscriptionId();
                    PhoneAccountHandle phoneAccountHandle =
                            subscriptionIdToPhoneAccountHandle(subId);
                    setDefaultDataSubId(mContext, subId);
                    setUserPrefDataSubIdInDb(subId);
                    setDefaultSmsSubId(mContext, subId);
                    setUserSelectedOutgoingPhoneAccount(phoneAccountHandle);
//                    finish();
                    dialog.dismiss();
                }
            });
            alertDialogBuilder.setNegativeButton(R.string.no, new
                    DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog,int id) {
//                    finish();
                    dialog.dismiss();
                }
            });

            Dialog alertDialog = alertDialogBuilder.create();
            alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    if (mTctSimDialogCloseListener != null) {
                        mTctSimDialogCloseListener.TctSimDialogClose();
                    }
                }
            });
            alertDialog.show();
        } else {
//            finish();
        }
    }

    private static void setDefaultDataSubId(final Context context, final int subId) {
        final SubscriptionManager subscriptionManager = SubscriptionManager.from(context);
        subscriptionManager.setDefaultDataSubId(subId);
        Toast.makeText(context, R.string.data_switch_started, Toast.LENGTH_LONG).show();
    }

    private void setUserPrefDataSubIdInDb(int subId) {
        android.provider.Settings.Global.putInt(mContext.getContentResolver(),
                SETTING_USER_PREF_DATA_SUB, subId);
        Log.d(TAG, "updating data subId: " + subId + " in DB");
    }

    private static void setDefaultSmsSubId(final Context context, final int subId) {
        final SubscriptionManager subscriptionManager = SubscriptionManager.from(context);
        subscriptionManager.setDefaultSmsSubId(subId);
    }

    private void setUserSelectedOutgoingPhoneAccount(PhoneAccountHandle phoneAccount) {
        final TelecomManager telecomManager = TelecomManager.from(mContext);
        telecomManager.setUserSelectedOutgoingPhoneAccount(phoneAccount);
    }

    private static boolean carrierSwitchingEnabled(Context context) {
        return context.getResources().getBoolean(R.bool.config_show_carrier_switching_enabled);
    }

    private static void showCarrierSwitching(Context context, int slotId) {
        Intent intent = new Intent();
        intent.setAction(ACTION_CARRIER_PREFERRED_SIM_SWAPPED);
        intent.putExtra(CARRIER_TARGET_SLOT, slotId);
        context.sendBroadcast(intent);
    }

    private PhoneAccountHandle subscriptionIdToPhoneAccountHandle(final int subId) {
        final TelecomManager telecomManager = TelecomManager.from(mContext);
        final TelephonyManager telephonyManager = TelephonyManager.from(mContext);
        final Iterator<PhoneAccountHandle> phoneAccounts =
                telecomManager.getCallCapablePhoneAccounts().listIterator();

        while (phoneAccounts.hasNext()) {
            final PhoneAccountHandle phoneAccountHandle = phoneAccounts.next();
            final PhoneAccount phoneAccount = telecomManager.getPhoneAccount(phoneAccountHandle);
            if (subId == telephonyManager.getSubIdForPhoneAccount(phoneAccount)) {
                return phoneAccountHandle;
            }
        }

        return null;
    }

    //[SOLUTION]-Add-BEGIN by TCTNB.(Chuanjun Chen), 09/07/2016, SOLUTION- 2473826 And TASk-2781430
    //[ForTest][FDN] Mobile data connection and USSD should be disabled when FDN is enabled
    private boolean checkDataEnableByFDN(int subId){
        int fdnFlag = 0;
        if (-1 == subId) {
            fdnFlag = Settings.System.getInt(mContext.getContentResolver(), "FDN_DATA_CONNECTION_DISABLE_FLAG", -1);
        } else {
            fdnFlag = Settings.System.getInt(mContext.getContentResolver(), "FDN_DATA_CONNECTION_DISABLE_FLAG" + subId, -1);
        }
        return mTelephonyManager.getIccFdnEnabled(mSubscriptionManager.getPhoneId(subId))
                   && fdnFlag != -1;
    }
    //[SOLUTION]-Add-END by TCTNB.(Chuanjun Chen)

    public Dialog createDialog(final Context context, final int id) {
        final ArrayList<String> list = new ArrayList<String>();
        final SubscriptionManager subscriptionManager = SubscriptionManager.from(context);
        final ArrayList<SubscriptionInfo> smsSubInfoList = new ArrayList<SubscriptionInfo>();
        final List<SubscriptionInfo> subInfoList =
            subscriptionManager.getActiveSubscriptionInfoList();
        /* ADD-BEGIN by Dingyi for dualsim 2016/08/03 FR 2655692*/
        //add Turn off mobile data item
        if (id == DATA_PICK && subInfoList != null) {
            subInfoList.add(null);
        }
        /* ADD-END by Dingyi for dualsim 2016/08/03 FR 2655692*/
        final int selectableSubInfoLength = subInfoList == null ? 0 : subInfoList.size();

        final DialogInterface.OnClickListener selectionListener =
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int value) {

                        final SubscriptionInfo sir;
                        boolean ddsalertDisplayed = false;
                        switch (id) {
                            case DATA_PICK:
                                sir = subInfoList.get(value);
                                /* MODIFY-BEGIN by Dingyi for dualsim 2016/08/03 FR 2655692*/
                                //disable mobile data
                                TelephonyManager tm = TelephonyManager.getDefault();
                                int defaultDataSubId = SubscriptionManager.getDefaultDataSubscriptionId();
                                if(null == sir && !TextUtils.isEmpty(list.get(value))){
                                    if (tm.getDataEnabled(defaultDataSubId)) {
                                        TelephonyManager.from(context).setDataEnabled(false);
                                        if(mTctPreferredSimChangeListener!=null){
                                            mHandler.sendEmptyMessage(MyHandler.MESSAGE_UPDATE_PREFER_SIM);
                                        }
                                    }
//                                    finish();
                                    dialog.dismiss();
                                    return ;
                                }
                                final int preferredSubID = sir.getSubscriptionId();
                                /* MODIFY-END by Dingyi for dualsim 2016/08/03 FR 2655692*/
                                if (defaultDataSubId != preferredSubID) {
                                    ddsalertDisplayed = displayDdsAlertIfNeeded(context,
                                           preferredSubID, defaultDataSubId);
                                    if (ddsalertDisplayed == false) {
                                        setDefaultDataSubId(context, preferredSubID);
                                        setUserPrefDataSubIdInDb(preferredSubID);
                                        tm = TelephonyManager.getDefault();
                                        if (!tm.getDataEnabled(preferredSubID) && !checkDataEnableByFDN(preferredSubID)) {//[SOLUTION]-Add-BEGIN by TCTNB.(Chuanjun Chen), 09/07/2016, SOLUTION- 2473826 And TASk-2781430
                                            tm.setDataEnabled(preferredSubID, true);
                                        }
                                        if(mTctPreferredSimChangeListener!=null){
                                            mHandler.sendEmptyMessage(MyHandler.MESSAGE_UPDATE_PREFER_SIM);
                                        }
                                    }
                                /* ADD-BEGIN by Dingyi for dualsim 2016/08/03 FR 2655692*/
                                } else {
                                    Log.d(TAG, "Now click dds card, need change the data to enable if its data is disable!");
                                    tm = TelephonyManager.getDefault();
                                    if (!tm.getDataEnabled(defaultDataSubId) && !checkDataEnableByFDN(preferredSubID)) {//[SOLUTION]-Add-BEGIN by TCTNB.(Chuanjun Chen), 09/07/2016, SOLUTION- 2473826 And TASk-2781430
                                        tm.setDataEnabled(defaultDataSubId, true);
                                        if(mTctPreferredSimChangeListener!=null){
                                            mHandler.sendEmptyMessage(MyHandler.MESSAGE_UPDATE_PREFER_SIM);
                                        }
                                    }
                                /* ADD-END by Dingyi for dualsim 2016/08/03 FR 2655692*/
                                }
                                if (carrierSwitchingEnabled(context)) {
                                    showCarrierSwitching(context, sir.getSimSlotIndex());
                                }
                                break;
                            case CALLS_PICK:
                                final TelecomManager telecomManager =
                                        TelecomManager.from(context);
                                final List<PhoneAccountHandle> phoneAccountsList =
                                        telecomManager.getCallCapablePhoneAccounts();
                                setUserSelectedOutgoingPhoneAccount(
                                        value < 1 ? null : phoneAccountsList.get(value - 1));
                                if(mTctPreferredSimChangeListener!=null){
                                    mHandler.sendEmptyMessage(MyHandler.MESSAGE_UPDATE_PREFER_SIM);
                                }
                                break;
                            case SMS_PICK:
                                boolean isSmsPrompt = false;
                                if (value < 1) {
                                    isSmsPrompt = true;
                                } else {
                                    sir = smsSubInfoList.get(value);
                                    if ( sir != null) {
                                        setDefaultSmsSubId(context, sir.getSubscriptionId());
                                    } else {
                                        isSmsPrompt = true;
                                    }
                                    Log.d(TAG, "SubscriptionInfo:" + sir);
                                }
                                Log.d(TAG, "isSmsPrompt: " + isSmsPrompt);
                                IExtTelephony extTelephony = IExtTelephony.Stub.
                                       asInterface(ServiceManager.getService("extphone"));
                                try {
                                    extTelephony.setSMSPromptEnabled(isSmsPrompt);
                                } catch (RemoteException ex) {
                                    Log.e(TAG, "RemoteException @setSMSPromptEnabled" + ex);
                                } catch (NullPointerException ex) {
                                    Log.e(TAG, "NullPointerException @setSMSPromptEnabled" + ex);
                                }
                                if(mTctPreferredSimChangeListener!=null){
                                    mHandler.sendEmptyMessage(MyHandler.MESSAGE_UPDATE_PREFER_SIM);
                                }
                                break;
                            default:
                                throw new IllegalArgumentException("Invalid dialog type "
                                        + id + " in SIM dialog.");
                        }
                        if (id != DATA_PICK || ddsalertDisplayed == false) {
                            dialog.dismiss();
//                            finish();
                        }
                    }
                };

        ArrayList<SubscriptionInfo> callsSubInfoList = new ArrayList<SubscriptionInfo>();
        if (id == CALLS_PICK) {
            final TelecomManager telecomManager = TelecomManager.from(context);
            final TelephonyManager telephonyManager = TelephonyManager.from(context);
            final Iterator<PhoneAccountHandle> phoneAccounts =
                    telecomManager.getCallCapablePhoneAccounts().listIterator();
            /* MODIFY-BEGIN by Dingyi for dualsim 2016/08/03 FR 2655692 */
            // list.add(getResources().getString(R.string.sim_calls_ask_first_prefs_title));
            list.add(mContext.getResources().getString(R.string.sim_calls_both_sim_cards_prefs_title));
            /* MODIFY-END by Dingyi for dualsim 2016/08/03 FR 2655692 */
            callsSubInfoList.add(null);
            while (phoneAccounts.hasNext()) {
                final PhoneAccount phoneAccount =
                        telecomManager.getPhoneAccount(phoneAccounts.next());
                list.add((String)phoneAccount.getLabel());
                int subId = telephonyManager.getSubIdForPhoneAccount(phoneAccount);
                if (subId != SubscriptionManager.INVALID_SUBSCRIPTION_ID) {
                    final SubscriptionInfo sir = SubscriptionManager.from(context)
                            .getActiveSubscriptionInfo(subId);
                    callsSubInfoList.add(sir);
                } else {
                    callsSubInfoList.add(null);
                }
            }
        } else if ((id == SMS_PICK)){
            /* MODIFY-BEGIN by Dingyi for dualsim 2016/08/03 FR 2655692 */
            // list.add(getResources().getString(R.string.sim_sms_ask_first_prefs_title));
            list.add(mContext.getResources().getString(R.string.sim_sms_both_sim_cards_prefs_title));
            /* MODIFY-END by Dingyi for dualsim 2016/08/03 FR 2655692 */
            smsSubInfoList.add(null);
            for (int i = 0; i < selectableSubInfoLength; ++i) {
                final SubscriptionInfo sir = subInfoList.get(i);
                smsSubInfoList.add(sir);
                CharSequence displayName = sir.getDisplayName();
                if (displayName == null) {
                    displayName = "";
                }
                list.add(displayName.toString());
            }
        } else {
            for (int i = 0; i < selectableSubInfoLength; ++i) {
                final SubscriptionInfo sir = subInfoList.get(i);
                /* MODIFY-BEGIN by Dingyi for dualsim 2016/08/03 FR 2655692 */
                if(null == sir){
                    //add disable data item
                    list.add(mContext.getResources().getString(R.string.sim_calls_cellular_data_disabled));
                } else {
                    CharSequence displayName = sir.getDisplayName();
                    if (displayName == null) {
                        displayName = "";
                    }
                    list.add(displayName.toString());
                }
                /* MODIFY-END by Dingyi for 2016/08/03 FR 2655692 */
            }
        }

        String[] arr = list.toArray(new String[0]);

        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        ListAdapter adapter = new SelectAccountListAdapter(
                id == CALLS_PICK ? callsSubInfoList :
                (id == SMS_PICK ? smsSubInfoList: subInfoList),
                builder.getContext(),
                R.layout.select_account_list_item,
                arr, id);

        switch (id) {
            case DATA_PICK:
                builder.setTitle(R.string.select_sim_for_data);
                break;
            case CALLS_PICK:
                builder.setTitle(R.string.select_sim_for_calls);
                break;
            case SMS_PICK:
                builder.setTitle(R.string.sim_card_select_title);
                break;
            default:
                throw new IllegalArgumentException("Invalid dialog type "
                        + id + " in SIM dialog.");
        }

        mSimDialog = builder.setAdapter(adapter, selectionListener).create();
        mSimDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            if(mSimDialog !=null){
                                mSimDialog =null;
                            }
                            if(mTctSimDialogCloseListener!=null){
                                mTctSimDialogCloseListener.TctSimDialogClose();
                            }
                        }
                    });

        return mSimDialog;
    }

    private class SelectAccountListAdapter extends ArrayAdapter<String> {
        private Context mContext;
        private int mResId;
        private int mDialogId;
        private final float OPACITY = 0.54f;
        private List<SubscriptionInfo> mSubInfoList;
        /* ADD-BEGIN by Dingyi for dualsim 2016/08/03 FR 2655692 */
        private TelephonyManager mTelephonyManager;
        /* ADD-END by Dingyi for dualsim 2016/08/03 FR 2655692 */
        public SelectAccountListAdapter(List<SubscriptionInfo> subInfoList,
                Context context, int resource, String[] arr, int dialogId) {
            super(context, resource, arr);
            mContext = context;
            mResId = resource;
            mDialogId = dialogId;
            mSubInfoList = subInfoList;
            /* ADD-BEGIN by Dingyi for dualsim 2016/08/03 FR 2655692 */
            TelephonyManager mTelephonyManager = TelephonyManager.from(mContext);
            /* ADD-END by Dingyi for dualsim 2016/08/03 FR 2655692 */
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater)
                    mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView;
            final ViewHolder holder;

            if (convertView == null) {
                // Cache views for faster scrolling
                rowView = inflater.inflate(mResId, null);
                holder = new ViewHolder();
                holder.title = (TextView) rowView.findViewById(R.id.title);
                holder.summary = (TextView) rowView.findViewById(R.id.summary);
                holder.icon = (ImageView) rowView.findViewById(R.id.icon);
                rowView.setTag(holder);
            } else {
                rowView = convertView;
                holder = (ViewHolder) rowView.getTag();
            }

            final SubscriptionInfo sir = mSubInfoList.get(position);
            /* MODIFY-BEGIN by Dingyi for dualsim 2016/08/03 FR 2655692*/
//            if (sir == null) {
//                holder.title.setText(getItem(position));
//                holder.summary.setText("");
//                holder.icon.setImageDrawable(getResources()
//                        .getDrawable(R.drawable.ic_always_ask));
//                holder.icon.setAlpha(OPACITY);
//            } else {
//                holder.title.setText(sir.getDisplayName());
//                holder.summary.setText(sir.getNumber());
//                holder.icon.setImageBitmap(sir.createIconBitmap(mContext));
//            }
          if (sir == null) {
              holder.title.setText(getItem(position));
              holder.summary.setText("");
              holder.summary.setVisibility(View.GONE);
              if (mDialogId == DATA_PICK) {
                  holder.icon.setImageDrawable(mContext.getResources().getDrawable(
                          R.drawable.ic_turn_off_data));
              } else {
                  holder.icon.setImageDrawable(mContext.getResources().getDrawable(
                          R.drawable.ic_both_sim_cards));
              }
          } else {
              String sirDisplayName = getSimDisplayName(sir);
              String textTitle = sirDisplayName;
              Drawable drawable = sir.getSimIconTypeDrawable(mContext);
              if(simnamedup){
                  textTitle = sirDisplayName + " - " + (sir.getSimSlotIndex() + 1);
              }
              holder.title.setText(textTitle);
              String number = sir.getNumber();
              holder.summary.setText(number);
              if(TextUtils.isEmpty(number)){
                  holder.summary.setVisibility(View.GONE);
              } else {
                  holder.summary.setVisibility(View.VISIBLE);
              }
              holder.icon.setImageDrawable(drawable);
          }
          /* MODIFY-BEGIN by Dingyi for dualsim 2016/08/03 FR 2655692 */
            return rowView;
        }

        private class ViewHolder {
            TextView title;
            TextView summary;
            ImageView icon;
        }
    }

    private static boolean isDdsSwitchAlertDialogSupported(Context context, int subId) {
        Resources res = getResourcesForSubId(context, subId);
        return res.getBoolean(R.bool.config_dds_switch_alert_dialog_supported);
    }

    /**
     * Returns the resources related to Subscription.
     * @param Context object
     * @param Subscription Id of Subscription who's resources are required
     * @return Resources of the Sub.
     * @hide
     */
    private static Resources getResourcesForSubId(Context context, int subId) {
        String operatorNumeric = "";
//       String operatorNumeric = TelephonyManager.getDefault().getIccOperatorNumericForData(subId);SAND
        Configuration config = context.getResources().getConfiguration();
        Configuration newConfig = new Configuration();
        newConfig.setTo(config);

        if (!TextUtils.isEmpty(operatorNumeric)) {
            newConfig.mcc = Integer.parseInt(operatorNumeric.substring(0,3));
            newConfig.mnc = Integer.parseInt(operatorNumeric.substring(3));
        }
        Log.d(TAG, "getResourcesForSubId: " + subId +
                ", mccmnc = " + newConfig.mcc + newConfig.mnc);

        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        DisplayMetrics newMetrics = new DisplayMetrics();
        newMetrics.setTo(metrics);

        return new Resources(context.getResources().getAssets(), newMetrics, newConfig);
    }

    private boolean displayDdsAlertIfNeeded(
            final Context context, final int subId, final int defaultDataSubId) {
        final SubscriptionManager subscriptionManager = SubscriptionManager.from(context);
        final TelephonyManager telephonymanager = TelephonyManager.from(context);
        Log.d(TAG, "Default Data SubId [" + defaultDataSubId + "]");

        int NETWORK_TYPE_LTE_CA = 19; // SAND
        if (isDdsSwitchAlertDialogSupported(context, defaultDataSubId) &&
               ((telephonymanager.getVoiceNetworkType(defaultDataSubId) ==
                       TelephonyManager.NETWORK_TYPE_LTE) ||
                (telephonymanager.getVoiceNetworkType(defaultDataSubId) ==
                       NETWORK_TYPE_LTE_CA) ||
                (telephonymanager.getDataNetworkType(defaultDataSubId) ==
                       TelephonyManager.NETWORK_TYPE_LTE) ||
                (telephonymanager.getDataNetworkType(defaultDataSubId) ==
                       NETWORK_TYPE_LTE_CA))) {
            Log.d(TAG, "DDS switch request from LTE sub");

            AlertDialog alertDlg = new AlertDialog.Builder(context).create();
            String title = context.getResources().getString(
                    R.string.data_switch_warning_title,
                    SubscriptionManager.getSlotId(subId) + 1);
            alertDlg.setTitle(title);
            String warningString = context.getResources().getString(
                    R.string.data_switch_voice_calling_warning_text);
            alertDlg.setMessage(warningString);
            alertDlg.setCancelable(false);

            String yes = context.getResources().getString(
                    R.string.yes);
            alertDlg.setButton(yes, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    Log.d(TAG, "Switch DDS to subId: " + subId );
                    subscriptionManager.setDefaultDataSubId(subId);
                    setUserPrefDataSubIdInDb(subId);
                    Toast.makeText(context, R.string.data_switch_started, Toast.LENGTH_LONG)
                            .show();
                    if(mTctPreferredSimChangeListener!=null){
                        mHandler.sendEmptyMessage(MyHandler.MESSAGE_UPDATE_PREFER_SIM);
                    }
                    dialog.dismiss();
//                    finish();
                }
            });

            String no = context.getResources().getString(
                    R.string.no);
            alertDlg.setButton2(no, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    Log.d(TAG, "Cancelled switch DDS to subId: " + subId);
                    dialog.cancel();
//                    finish();
                    dialog.dismiss();
                    return;
                }
            });
            alertDlg.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    if(mSimDialog!=null && mSimDialog.isShowing()){
                        mSimDialog.dismiss();
                    }
                    if (mTctSimDialogCloseListener != null) {
                        mTctSimDialogCloseListener.TctSimDialogClose();
                    }
                }
            });

            alertDlg.show();
            return true;
        }
        return false;
    }

    /* ADD-BEGIN by Dingyi for dualsim 2016/08/03 FR 2655692*/
    private void setDefaultVoiceSmsSubId() {
        Context context = mContext;
        IExtTelephony extTelephony = IExtTelephony.Stub.
                asInterface(ServiceManager.getService("extphone"));
        setDefaultSmsSubId(context, SubscriptionManager.INVALID_SUBSCRIPTION_ID);
        try {
            //set sms prompt as true , changed for Task1535122.
            extTelephony.setSMSPromptEnabled(true);
        } catch (RemoteException ex) {
            Log.d(TAG, "RemoteException @isSMSPromptEnabled" + ex);
        } catch (NullPointerException ex) {
            Log.d(TAG, "NullPointerException @isSMSPromptEnabled" + ex);
        }
        setUserSelectedOutgoingPhoneAccount(null);
    }
    /* ADD-END by Dingyi for dualsim 2016/08/03 FR 2655692*/

    public interface TctPreferredSimChangeListener{
        public void TctupdateSimPrefer();
    }

    public interface TctSimDialogCloseListener{
        public void TctSimDialogClose();
    }

    private class MyHandler extends Handler {
        static final int MESSAGE_UPDATE_PREFER_SIM = 0;

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MESSAGE_UPDATE_PREFER_SIM:
                    updatePreferredSim();
                    break;
            }
        }

        private void updatePreferredSim() {
            if(mTctPreferredSimChangeListener!=null){
                mTctPreferredSimChangeListener.TctupdateSimPrefer();
            }
        }
    }
}
