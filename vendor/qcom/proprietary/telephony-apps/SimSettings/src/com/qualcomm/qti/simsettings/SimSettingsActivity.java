/*
 * Copyright (c) 2016 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc
 */

package com.qualcomm.qti.simsettings;

import android.app.Activity;
import android.os.Bundle;

import com.android.settingslib.drawer.SettingsDrawerActivity;

public class SimSettingsActivity extends SettingsDrawerActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SimSettings simsettings = new SimSettings();
        simsettings.setArguments(getIntent().getExtras());
        getFragmentManager().beginTransaction().replace(
                R.id.content_frame, simsettings, "simsettings").commit();//MODIFY by Dingyi for dualsim 2016/09/22 FR 2655692
    }
}
