 LOCAL_PATH:= $(call my-dir)
 include $(CLEAR_VARS)
 LOCAL_MODULE_TAGS := optional eng

 LOCAL_SRC_FILES := $(call all-subdir-java-files)

 LOCAL_JAVA_LIBRARIES := telephony-common telephony-ext
 LOCAL_STATIC_JAVA_LIBRARIES := android-support-v4

 LOCAL_RESOURCE_DIR := $(LOCAL_PATH)/res

 LOCAL_PACKAGE_NAME :=SimSettings

 LOCAL_CERTIFICATE := platform

include frameworks/opt/setupwizard/navigationbar/common.mk
include frameworks/opt/setupwizard/library/common-full-support.mk
include frameworks/base/packages/SettingsLib/common.mk

 include $(BUILD_PACKAGE)
