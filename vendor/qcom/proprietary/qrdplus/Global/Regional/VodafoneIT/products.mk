PRODUCT_PACKAGES += \
     mcfg_sw.mbn \
     .preloadspec \
     vendor.prop \
    VodafoneITBrowserRes \
    VodafoneITContactsRes \
    VodafoneITDialerRes \
    VodafoneITMmsRes \
    VodafoneITNetworkSettingRes \
    VodafoneITFrameworksRes \
    VodafoneITPhoneCommonRes \
    VodafoneITSettingsRes \
    VodafoneITSettingsProviderRes \
    VodafoneITSystemUIRes \
    VodafoneITTeleServiceRes \
    VodafoneITLatinIMERes \
    VodafoneITEmailRes \
    VodafoneITSimContactsRes \
    VodafoneITOmaDownloadRes \
    VodafoneITWallPaperRes \
    VodafoneITCellBroadcastReceiverRes \
    VodafoneITStkRes
