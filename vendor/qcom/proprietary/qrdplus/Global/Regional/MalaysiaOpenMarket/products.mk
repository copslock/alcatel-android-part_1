# Add MalaysiaOpenMarket Apps
PRODUCT_PACKAGES += \
    MalaysiaOpenMarketFrameworksRes \
    MalaysiaOpenMarketContactsRes \
    MalaysiaOpenMarketMmsRes \
    MalaysiaOpenMarketSettingsProviderRes \
    MalaysiaOpenMarketSystemUIRes \
    MalaysiaOpenMarketLatinIMERes \
    MalaysiaOpenMarketSoundRecorderRes \
    MalaysiaOpenMarketSettingsRes \
    MalaysiaOpenMarketFM2Res \
    MalaysiaOpenMarketFMRecordRes \
    MalaysiaOpenMarketTeleServiceRes \
    MalaysiaOpenMarketPhoneFeaturesRes
