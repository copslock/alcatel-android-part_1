# Add Cherry Mobile Apps
PRODUCT_PACKAGES += \
    CherryCommonContactsRes \
    CherryCommonLauncher2Res \
    CherryCommonMmsRes \
    CherryCommonSettingsProviderRes \
    CherryCommonSystemUIRes \
    CherryCommonCellBroadcastReceiverRes \
    CherryCommonFMRecordRes \
    CherryCommonLatinIMERes \
    CherryCommonSoundRecorderRes \
    CherryCommonTeleServiceRes \
    CherryCommonSettingsRes \
    CherryCommonPhoneFeaturesRes \
    CherryCommonTrebuchetRes \
    CherryCommonDialerRes \
    CherryCommonFM2Res
