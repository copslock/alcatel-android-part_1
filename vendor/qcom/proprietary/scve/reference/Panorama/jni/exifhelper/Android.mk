LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE            := libpanoramaref_exifhelper
LOCAL_SRC_FILES := exifhelper.cpp
LOCAL_SHARED_LIBRARIES  := libjpeg
include $(BUILD_STATIC_LIBRARY)
