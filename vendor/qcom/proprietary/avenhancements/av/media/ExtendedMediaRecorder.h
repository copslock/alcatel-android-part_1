/*
 * Copyright (c) 2015-2016, Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 *
 * Not a Contribution.
 * Apache license notifications and license are retained
 * for attribution purposes only.
 */
/*
 * Copyright (c) 2013 - 2015, The Linux Foundation. All rights reserved.
 */
/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef BRINGUP_WIP
#ifndef _EXTENDED_MEDIA_RECORDER_H_
#define _EXTENDED_MEDIA_RECORDER_H_

#include <mediarecorder.h>
#include <utils/Errors.h>

namespace android {

class ExtendedMediaRecorder : public MediaRecorder {
public:
    ExtendedMediaRecorder(const String16& opPackageName);
    virtual status_t  start();
    virtual status_t  stop();
    virtual status_t  setParameters(const String8& params);

protected:
    bool mIsPaused;
};

} //namespace android

#endif //_EXTENDED_MEDIA_RECORDER_H_
#endif
