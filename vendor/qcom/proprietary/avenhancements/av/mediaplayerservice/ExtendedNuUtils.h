/*
 * Copyright (c) 2015-2016, Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
*/

#ifndef _EXTENDED_NU_UTILS_H_
#define _EXTENDED_NU_UTILS_H_

namespace android {

struct ExtendedNuUtils : public AVNuUtils {

    virtual bool pcmOffloadException(const sp<AMessage> &format);

    bool isVorbisFormat(const sp<MetaData> &meta);
    bool flacSwSupports24Bit();

    virtual audio_format_t getPCMFormat(const sp<AMessage> &format);
    virtual void setCodecOutputFormat(const sp<AMessage> &format);
    virtual bool isByteStreamModeEnabled(const sp<MetaData> &meta);

    virtual void printFileName(int fd);
    virtual void checkFormatChange(bool *formatChange, const sp<ABuffer> &accessUnit);
    virtual uint32_t getFlags();
    virtual bool canUseSetBuffers(const sp<MetaData> &Meta);
    virtual void overWriteAudioOutputFormat(
            sp <AMessage> &dst, const sp <AMessage> &src);
    virtual bool dropCorruptFrame();

    // ----- NO TRESSPASSING BEYOND THIS LINE ------
protected:
    virtual ~ExtendedNuUtils();

private:
    ExtendedNuUtils(const ExtendedNuUtils&);
    ExtendedNuUtils &operator=(const ExtendedNuUtils&);
    bool findAndReplaceKeys(const char *key,
            sp <AMessage> &dst, const sp<AMessage> &src);
    bool mDropCorruptFrame;
public:
    ExtendedNuUtils();
};

extern "C" AVNuUtils *createExtendedNuUtils();

} //namespace android

#endif //_EXTENDED_NU_UTILS_H_

