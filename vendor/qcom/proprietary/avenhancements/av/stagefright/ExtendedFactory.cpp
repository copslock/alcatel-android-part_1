/*
 * Copyright (c) 2015-2016, Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
*/

//#define LOG_NDEBUG 0
#define LOG_TAG "ExtendedFactory"
#include <common/AVLog.h>

#include <media/stagefright/foundation/ADebug.h>
#include <media/stagefright/foundation/AMessage.h>
#include <media/stagefright/foundation/ABuffer.h>
#include <media/stagefright/MediaDefs.h>
#include <media/stagefright/MediaCodecList.h>
#include <media/stagefright/MetaData.h>
#include <media/stagefright/MediaExtractor.h>

#include <stagefright/AVExtensions.h>
#include "stagefright/ExtendedFactory.h"
#include "stagefright/ExtendedACodec.h"
#include "stagefright/ExtendedExtractor.h"
#include "stagefright/ExtendedCachedSource.h"
#include "stagefright/ExtendedAudioSource.h"
#include "stagefright/ExtendedCameraSource.h"
#include "stagefright/ExtendedCameraSourceTimeLapse.h"
#include "stagefright/ExtendedESQueue.h"
#include "stagefright/ExtendedMPEG4Writer.h"

namespace android {

AVFactory *createExtendedFactory() {
    return new ExtendedFactory;
}

sp<ACodec> ExtendedFactory::createACodec() {
    return new ExtendedACodec;
}

MediaExtractor* ExtendedFactory::createExtendedExtractor(
          const sp<DataSource> &source, const char *mime,
          const sp<AMessage> &meta) {
    return ExtendedExtractor::create(source, mime, meta);
}

#ifndef BRINGUP_WIP
sp<NuCachedSource2> ExtendedFactory::createCachedSource(
            const sp<DataSource> &source,
            const char *cacheConfig,
            bool disconnectAtHighwatermark) {
    return ExtendedCachedSource::Create(source, cacheConfig, disconnectAtHighwatermark);
}
#endif

AudioSource* ExtendedFactory::createAudioSource(
            audio_source_t inputSource,
            const String16 &opPackageName,
            uint32_t sampleRate,
            uint32_t channels,
            uint32_t outSampleRate,
	    uid_t clientUid,
	    pid_t clientPid
	    ) {
    return new ExtendedAudioSource(inputSource, opPackageName, sampleRate,
                            channels, outSampleRate,clientUid,clientPid);
}

#ifndef BRINGUP_WIP
CameraSource* ExtendedFactory::CreateCameraSourceFromCamera(
            const sp<hardware::ICamera> &camera,
            const sp<ICameraRecordingProxy> &proxy,
            int32_t cameraId,
            const String16& clientName,
            uid_t clientUid,
            uid_t clientPid,
            Size videoSize,
            int32_t frameRate,
            const sp<IGraphicBufferProducer>& surface,
            bool storeMetaDataInVideoBuffers) {
    return new ExtendedCameraSource(camera, proxy, cameraId,
            clientName, clientUid, clientPid, videoSize, frameRate, surface,
            storeMetaDataInVideoBuffers);
}
#endif
CameraSourceTimeLapse *ExtendedFactory::CreateCameraSourceTimeLapseFromCamera(
        const sp<hardware::ICamera> &camera,
        const sp<ICameraRecordingProxy> &proxy,
        int32_t cameraId,
        const String16& clientName,
        uid_t clientUid,
        pid_t clientPid,
        Size videoSize,
        int32_t videoFrameRate,
        const sp<IGraphicBufferProducer>& surface,
        int64_t timeBetweenFrameCaptureUs,
        bool storeMetaDataInVideoBuffers) {
    return new ExtendedCameraSourceTimeLapse(camera, proxy, cameraId, clientName,
            clientUid, clientPid, videoSize, videoFrameRate, surface,
            timeBetweenFrameCaptureUs, storeMetaDataInVideoBuffers);
}

ElementaryStreamQueue* ExtendedFactory::createESQueue(
         ElementaryStreamQueue::Mode mode, uint32_t flags) {
    return new ExtendedESQueue(mode, flags);
}

MPEG4Writer* ExtendedFactory::CreateMPEG4Writer(int fd) {
    return new ExtendedMPEG4Writer(fd);
}

ExtendedFactory::ExtendedFactory() {
    updateLogLevel();
    AVLOGV("ExtendedFactory()");
}

ExtendedFactory::~ExtendedFactory() {
    AVLOGV("~ExtendedFactory()");
}

}
