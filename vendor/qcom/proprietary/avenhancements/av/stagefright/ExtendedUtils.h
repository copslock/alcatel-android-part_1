/*
 * Copyright (c) 2015-2016, Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */
/*
 * Copyright (c) 2013 - 2015, The Linux Foundation. All rights reserved.
 */

#ifndef _EXTENDED_UTILS_H_
#define _EXTENDED_UTILS_H_

#include <system/audio.h> //for enum audio_format_t
#include <stagefright/AVExtensions.h>

#define MIN_BITRATE_AAC 24000
#define MAX_BITRATE_AAC 192000

namespace android {

// TODO: move to a separate file; import all cousins from QCMetaData.h
enum {
    kKeyLocalBatchSize    = 'btch', //int32_t
};

struct MediaCodec;

struct ExtendedUtils : public AVUtils {

    static const char* getMsgFromKey(int key);
    virtual status_t convertMetaDataToMessage(
            const sp<MetaData> &meta, sp<AMessage> *format);
    virtual status_t convertMessageToMetaData(
            const sp<AMessage> &msg, sp<MetaData> &meta);
    virtual DataSource::SnifferFunc getExtendedSniffer();
    virtual sp<MediaCodec> createCustomComponentByName(const sp<ALooper> &looper,
            const char* mime, bool encoder, const sp<AMessage> &msg);
    virtual bool isEnhancedExtension(const char *extension);

    virtual status_t mapMimeToAudioFormat(audio_format_t&, const char*);
    virtual status_t sendMetaDataToHal(const sp<MetaData>& meta, AudioParameter *param);
    virtual bool hasAudioSampleBits(const sp<MetaData> &meta);
    virtual bool hasAudioSampleBits(const sp<AMessage> &format);
    virtual int32_t getAudioSampleBits(const sp<MetaData> &meta);
    virtual int32_t getAudioSampleBits(const sp<AMessage> &format);

    virtual audio_format_t updateAudioFormat(audio_format_t audioFormat,
            const sp<AMessage> &format);
    virtual audio_format_t updateAudioFormat(audio_format_t audioFormat,
            const sp<MetaData> &meta);

    virtual bool canOffloadAPE(const sp<MetaData> &meta);

    virtual int32_t getAudioMaxInputBufferSize(audio_format_t audioFormat, const sp<AMessage> &);

    virtual bool mapAACProfileToAudioFormat(const sp<MetaData> &,
                          audio_format_t &,
                          uint64_t /*eAacProfile*/);

    virtual bool mapAACProfileToAudioFormat(const sp<AMessage> &,
                          audio_format_t &,
                          uint64_t /*eAacProfile*/);

    virtual void extractCustomCameraKeys(
            const CameraParameters& params, sp<MetaData> &meta);
    virtual void printFileName(int fd);
    virtual void addDecodingTimesFromBatch(MediaBuffer * buf,
            List<int64_t> &decodeTimeQueue);

    virtual bool useQCHWEncoder(const sp<AMessage> &outputFormat, Vector<AString> *role);

    virtual bool canDeferRelease(const sp<MetaData> &/*meta*/);
    virtual void setDeferRelease(sp<MetaData> &/*meta*/);
    static bool isHwAudioDecoderSessionAllowed(const sp<AMessage> &format);
#ifndef BRINGUP_WIP
    struct ExtendedHEVCMuxer : public HEVCMuxer {

        virtual bool reassembleHEVCCSD(const AString &mime, sp<ABuffer> csd0, sp<MetaData> &meta);

        virtual void writeHEVCFtypBox(MPEG4Writer *writer);

        virtual status_t makeHEVCCodecSpecificData(const uint8_t *data,
                  size_t size, void** codecSpecificData,
                  size_t *codecSpecificDataSize);

        virtual const char *getFourCCForMime(const char *mime);

        virtual void writeHvccBox(MPEG4Writer *writer,
                  void* codecSpecificData, size_t codecSpecificDataSize,
                  bool useNalLengthFour);

        virtual bool isVideoHEVC(const char* mime);

        virtual void getHEVCCodecSpecificDataFromInputFormatIfPossible(
                  sp<MetaData> meta, void **codecSpecificData,
                  size_t *codecSpecificDataSize, bool *gotAllCodecSpecificData);

    private:

        struct HEVCParamSet {
            HEVCParamSet(uint16_t length, const uint8_t *data)
                   : mLength(length), mData(data) {}

            uint16_t mLength;
            const uint8_t *mData;
        };

        static bool getHEVCCodecConfigData(const sp<MetaData> &meta,
                  const void **data, size_t *size);

        static status_t extractNALRBSPData(const uint8_t *data, size_t size,
                  uint8_t **header, bool *alreadyFilled);

        static status_t parserProfileTierLevel(const uint8_t *data, size_t size,
                  uint8_t **header, bool *alreadyFilled);

        static const uint8_t *parseHEVCParamSet(const uint8_t *data, size_t length,
                  List<HEVCParamSet> &paramSetList, size_t *paramSetLen);

        static size_t parseHEVCCodecSpecificData(const uint8_t *data, size_t size,
                  List<HEVCParamSet> &vidParamSet, List<HEVCParamSet> &seqParamSet,
                  List<HEVCParamSet> &picParamSet );

    protected:
        ExtendedHEVCMuxer() {}
        virtual ~ExtendedHEVCMuxer() {}
        friend struct ExtendedUtils;
    };

    virtual inline HEVCMuxer& HEVCMuxerUtils() {
         return mExtendedHEVCMuxer;
    }
#endif
    bool isAudioMuxFormatSupported(const char *mime);
    virtual void cacheCaptureBuffers(sp<hardware::ICamera> camera, video_encoder encoder);
    virtual const char *getCustomCodecsLocation();
    virtual const char *getCustomCodecsPerformanceLocation();

    virtual void setIntraPeriod(
            int nPFrames, int nBFrames, const sp<IOMX> OMXhandle,
            IOMX::node_id nodeID);

    virtual bool IsHevcIDR(const sp<ABuffer> &accessUnit);

private:
#ifndef BRINGUP_WIP
    ExtendedHEVCMuxer mExtendedHEVCMuxer;
#endif
    // ----- NO TRESSPASSING BEYOND THIS LINE ------
protected:
    virtual ~ExtendedUtils();

private:
    ExtendedUtils(const ExtendedUtils&);
    ExtendedUtils &operator=(const ExtendedUtils&);
    bool isAudioWMAPro(const sp<MetaData> &meta);
    bool isAudioWMAPro(const sp<AMessage> &meta);
    bool overrideMimeType(const sp<AMessage> &msg, AString* mime);
public:
    ExtendedUtils();
};

extern "C" AVUtils *createExtendedUtils();

} //namespace android

#endif //_EXTENDED_UTILS_H_

