/*
 * Copyright (c) 2015, Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 *
 * Not a Contribution.
 * Apache license notifications and license are retained
 * for attribution purposes only.
 */
/*
 * Copyright (c) 2013 - 2015, The Linux Foundation. All rights reserved.
 */
/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _EXTENDED_ESQUEUE_H_
#define _EXTENDED_ESQUEUE_H_

#include "mpeg2ts/ESQueue.h"

namespace android {

struct ExtendedESQueue : public ElementaryStreamQueue{
    ExtendedESQueue(
        ElementaryStreamQueue::Mode mode,
        uint32_t flags = 0);
    ~ExtendedESQueue();
protected:
    virtual sp<ABuffer> dequeueAccessUnitH265();
    static sp<MetaData> MakeHEVCCodecSpecificData(const sp<ABuffer> &accessUnit);

private:
    DISALLOW_EVIL_CONSTRUCTORS(ExtendedESQueue);
};

} // namespace android

#endif // _EXTENDED_NUCACHEDSOURCE2_H_

