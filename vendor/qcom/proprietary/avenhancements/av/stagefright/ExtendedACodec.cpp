/*
 * Copyright (c) 2015-2016 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 *
 * Not a Contribution.
 * Apache license notifications and license are retained
 * for attribution purposes only.
 */
/*
 * Copyright (c) 2013 - 2015, The Linux Foundation. All rights reserved.
 */
/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//#define LOG_NDEBUG 0
#define LOG_TAG "ExtendedACodec"

#ifdef __LP64__
#define OMX_ANDROID_COMPILE_AS_32BIT_ON_64BIT_PLATFORMS
#endif

#include <common/AVLog.h>

#include <media/stagefright/foundation/ADebug.h>
#include <media/stagefright/foundation/AMessage.h>
#include <media/stagefright/foundation/ABuffer.h>
#include <media/stagefright/foundation/ABitReader.h>
#include <media/stagefright/MediaDefs.h>
#include <media/stagefright/MediaCodecList.h>

#include <media/stagefright/MetaData.h>
#ifndef BRINGUP_WIP
#include <media/stagefright/OMXCodec.h>
#endif

#include <media/hardware/HardwareAPI.h>

#include <QCMetaData.h>

#include <stagefright/AVExtensions.h>
#include <stagefright/ExtendedACodec.h>
#include <stagefright/ExtendedUtils.h>
#include <cutils/properties.h>

#include <OMX_Component.h>
#include <OMX_VideoExt.h>
#include <OMX_IndexExt.h>
#include <QOMX_AudioExtensions.h>
#include <QOMX_AudioIndexExtensions.h>
#include <OMX_QCOMExtns.h>
#include <OMX_Skype_VideoExtensions.h>
#include <cutils/ashmem.h>

namespace android {

#ifndef OMX_QTI_INDEX_PARAM_VIDEO_PREFER_ADAPTIVE_PLAYBACK
#define OMX_QTI_INDEX_PARAM_VIDEO_PREFER_ADAPTIVE_PLAYBACK "OMX.QTI.index.param.video.PreferAdaptivePlayback"
#endif

/* VIDEO POSTPROCESSING APP LEVEL CTRLS */
typedef struct APP_VPP_HQVCONTROL {
    QOMX_VPP_HQV_MODE mode;
    QOMX_VPP_HQVCONTROLTYPE ctrl_type;
    union {
        QOMX_VPP_HQVCTRL_CADE cade;
        QOMX_VPP_HQVCTRL_DI di;
        QOMX_VPP_HQVCTRL_CNR cnr;
        QOMX_VPP_HQVCTRL_AIE aie;
        QOMX_VPP_HQVCTRL_CUSTOM custom;
        QOMX_VPP_HQVCTRL_GLOBAL_DEMO global_demo;
        QOMX_VPP_HQVCTRL_FRC frc;
    };
} APP_VPP_HQVCONTROL;

// checks and converts status_t to a non-side-effect status_t
static inline status_t makeNoSideEffectStatus(status_t err) {
    switch (err) {
    // the following errors have side effects and may come
    // from other code modules. Remap for safety reasons.
    case INVALID_OPERATION:
    case DEAD_OBJECT:
        return UNKNOWN_ERROR;
    default:
        return err;
    }
}

template<class T>
static void InitOMXParams(T *params) {
    memset(params, 0, sizeof(T));
    params->nSize = sizeof(T);
    params->nVersion.s.nVersionMajor = 1;
    params->nVersion.s.nVersionMinor = 0;
    params->nVersion.s.nRevision = 0;
    params->nVersion.s.nStep = 0;
}

ExtendedACodec::ExtendedACodec()
    : mComponentAllocByName(false),
      mEncoderComponent(false) {
    updateLogLevel();
    AVLOGV("ExtendedACodec()");
}

ExtendedACodec::~ExtendedACodec() {
    AVLOGV("~ExtendedACodec()");
}

void ExtendedACodec::initiateAllocateComponent(const sp<AMessage> &msg) {
    AString componentName;
    if (msg->findString("componentName", &componentName)) {
        mComponentAllocByName = true;
    } else {
        mComponentAllocByName = false;
        int32_t encoder;
        if (!msg->findInt32("encoder", &encoder)) {
            encoder = 0;
        }
        mEncoderComponent = encoder;
    }
    ACodec::initiateAllocateComponent(msg);
}

const char *ExtendedACodec::getComponentRole(
        bool isEncoder, const char *mime) {
    AVLOGD("getComponentRole");

    const char *role = ACodec::getComponentRole(isEncoder, mime);
    if ( role != NULL) {
         return role;
    }
    AVLOGD("getComponentRole for %s", mime);
    struct MimeToRole {
        const char *mime;
        const char *decoderRole;
        const char *encoderRole;
    };

    static const MimeToRole kMimeToRole[] = {
        { MEDIA_MIMETYPE_AUDIO_EVRC,
          "audio_decoder.evrchw", "audio_encoder.evrc" },
        { MEDIA_MIMETYPE_AUDIO_QCELP,
          "audio_decoder.qcelp13Hw", "audio_encoder.qcelp13" },
        { MEDIA_MIMETYPE_VIDEO_DIVX,
          "video_decoder.divx", NULL },
        { MEDIA_MIMETYPE_VIDEO_DIVX4,
          "video_decoder.divx", NULL },
        { MEDIA_MIMETYPE_VIDEO_DIVX311,
          "video_decoder.divx", NULL },
        { MEDIA_MIMETYPE_VIDEO_WMV,
          "video_decoder.vc1",  NULL },
        { MEDIA_MIMETYPE_VIDEO_WMV_VC1,
          "video_decoder.vc1",  NULL },
        { MEDIA_MIMETYPE_AUDIO_AC3,
          "audio_decoder.ac3", NULL },
        { MEDIA_MIMETYPE_AUDIO_WMA,
          "audio_decoder.wma", NULL },
        { MEDIA_MIMETYPE_AUDIO_ALAC,
          "audio_decoder.alac", NULL },
        { MEDIA_MIMETYPE_AUDIO_APE,
          "audio_decoder.ape", NULL },
        { MEDIA_MIMETYPE_VIDEO_HEVC,
          "video_decoder.hevc", "video_encoder.hevc" },
        { MEDIA_MIMETYPE_AUDIO_AMR_WB_PLUS,
            "audio_decoder.amrwbplus", "audio_encoder.amrwbplus" },
        { MEDIA_MIMETYPE_AUDIO_EVRC,
            "audio_decoder.evrchw", "audio_encoder.evrc" },
        { MEDIA_MIMETYPE_AUDIO_QCELP,
            "audio_decoder.qcelp13Hw", "audio_encoder.qcelp13" },
        { MEDIA_MIMETYPE_VIDEO_MPEG4_DP,
            "video_decoder.mpeg4", NULL },
#ifdef QTI_FLAC_DECODER
        { MEDIA_MIMETYPE_AUDIO_FLAC,
            "audio_decoder.flac", NULL },
#else
        { MEDIA_MIMETYPE_AUDIO_FLAC,
            "audio_decoder.flac", "audio_encoder.flac" },
#endif
        { MEDIA_MIMETYPE_AUDIO_DSD,
            "audio_decoder.dsd", NULL },
    };

    static const size_t kNumMimeToRole =
        sizeof(kMimeToRole) / sizeof(kMimeToRole[0]);

    size_t i;
    for (i = 0; i < kNumMimeToRole; ++i) {
        if (!strcasecmp(mime, kMimeToRole[i].mime)) {
            break;
        }
    }

    if (i == kNumMimeToRole) {
        AVLOGE("Unsupported mime %s for %s", mime, isEncoder ?
                "encoder" : "decoder");
        return NULL;
    }

    return isEncoder ? kMimeToRole[i].encoderRole
                  : kMimeToRole[i].decoderRole;
}

status_t ExtendedACodec::configureCodec(const char *mime, const sp<AMessage> &msg) {
    AVLOGV("configureCodec()");

    int32_t preferAdaptive = 0;
    status_t err = OK;

    int32_t priority = 0;
    if (msg->findInt32("priority", &priority)) {
        setPriority(priority);
    }

#ifdef OMX_QTI_INDEX_PARAM_VIDEO_CLIENT_EXTRADATA
    int32_t needExtradata = 0;
    if (msg->findInt32("video-extradata-enable", &needExtradata)
            && needExtradata == 1) {
        do {
            QOMX_VIDEO_CLIENT_EXTRADATATYPE extraData;
            OMX_INDEXTYPE indexType;
            InitOMXParams(&extraData);

            err = mOMX->getExtensionIndex(
                    mNode, OMX_QTI_INDEX_PARAM_VIDEO_CLIENT_EXTRADATA,
                    &indexType);
            if (err != OK) {
                AVLOGW("Failed to get extension for extradata parameter");
                break;
            }

            err = mOMX->getParameter(mNode, indexType,
                    (void *)&extraData, sizeof(extraData));
            if (err != OK) {
                AVLOGW("Failed get extradata parameter");
                break;
            }

            if (extraData.nExtradataAllocSize == 0) {
                AVLOGE("Extradata size is zero");
                break;
            }
            int fd = ashmem_create_region("Video-Extradata", extraData.nExtradataAllocSize);
            if (fd < 0) {
                AVLOGW("Failed to allocate extradata memory");
                break;
            }

            extraData.nFd = fd;
            err = mOMX->setParameter(mNode, indexType,
                    (void *)&extraData, sizeof(extraData));
            if (err != OK) {
                AVLOGW("Failed set extradata param");
                close(fd);
                break;
            }

            msg->setInt32("extradata-fd", fd);
            msg->setInt32("extradata-total-size", extraData.nExtradataAllocSize);
            msg->setInt32("extradata-size", extraData.nExtradataSize);
        } while(0);
        err = OK;
    }
#endif

    if (mFlags & kFlagIsSecure) {
        char value[PROPERTY_VALUE_MAX] = {0};
        property_get("mm.enable.sec.smoothstreaming", value, "0");
        if (!strncasecmp(value, "true", 4) || atoi(value)) {
            msg->setInt32("prefer-adaptive-playback", 1);
            msg->setInt32("max-width", 1920);
            msg->setInt32("max-height", 1088);
            AVLOGI("Enabling smooth streaming for secure video playback");
        }
    }

    if (msg->findInt32("prefer-adaptive-playback", &preferAdaptive)
            && preferAdaptive == 1) {
        AVLOGI("[%s] Adaptive playback preferred", mComponentName.c_str());

        do {
            QOMX_ENABLETYPE enableType;
            OMX_INDEXTYPE indexType;
            InitOMXParams(&enableType);

            err = mOMX->getExtensionIndex(
                    mNode, OMX_QTI_INDEX_PARAM_VIDEO_PREFER_ADAPTIVE_PLAYBACK,
                    &indexType);
            if (err != OK) {
                AVLOGW("Failed to get extension for adaptive playback");
                break;
            }

            enableType.bEnable = OMX_TRUE;
            err = mOMX->setParameter(mNode, indexType,
                    (void *)&enableType, sizeof(enableType));
            if (err != OK) {
                AVLOGW("Failed to set adaptive playback");
            }
            break;
        } while(1);
    }

    bool isDP = isMPEG4DP(msg);

    if (!isDP) {
        err = ACodec::configureCodec(mime, msg);
    } else {
        mime = MEDIA_MIMETYPE_VIDEO_MPEG4_DP;
    }

    bool needRealloc = isDP || (err != OK && !mComponentAllocByName && !mEncoderComponent &&
                            !strncmp(mime, "video/", strlen("video/")));

    if (needRealloc) {
        err = reallocateComponent(mime);
        if (err != OK) {
            AVLOGE("Failed to reallocate component");
            signalError(OMX_ErrorUndefined, makeNoSideEffectStatus(err));
            return err;
        }

        err = ACodec::configureCodec(mime, msg);
        if (err != OK) {
            AVLOGE("[%s] configureCodec returning error %d",
                  mComponentName.c_str(), err);
            signalError(OMX_ErrorUndefined, makeNoSideEffectStatus(err));
            return err;
        }
    }
    return err;
}

status_t ExtendedACodec::reallocateComponent(const char *mime) {
    AVLOGV("reallocateComponent()");
    Vector<AString> matchingCodecs;
//  Vector<OMXCodec::CodecNameAndQuirks> matchingCodecs;
    AString componentName;
    status_t err = UNKNOWN_ERROR;

     MediaCodecList::findMatchingCodecs(
        mime,
        false, // createEncoder
        0,     // flags
        &matchingCodecs);

    IOMX::node_id altNode = 0;
    for (size_t matchIndex = 0; matchIndex < matchingCodecs.size();
            ++matchIndex) {
        componentName = matchingCodecs[matchIndex];
        if (!strcmp(mComponentName.c_str(), componentName.c_str())) {
            continue;
        }

        sp<IOMXObserver> observer = createObserver();
        err = mOMX->allocateNode(componentName.c_str(), observer, &mNodeBinder, &altNode);

        if (err == OK) {
            break;
        } else {
            AVLOGW("Allocating component '%s' failed, try next one.", componentName.c_str());
        }
    }

    if (altNode == 0) {
        AVLOGE("Unable to replace %s of type '%s'", mComponentName.c_str(), mime);
    } else {
        status_t err = mOMX->freeNode(mNode);
        if (err != OK) {
            AVLOGE("Failed to freeNode");
            //mNode is deleted already..no point in returning
            //return err;
        }
        mNode = altNode;
        AVLOGI("Reallocated %s in place of %s", componentName.c_str(), mComponentName.c_str());
        mComponentName = componentName;
    }

    return err;
}

bool ExtendedACodec::isMPEG4DP(const sp<AMessage> &msg) {
    bool isDP = false;
    sp<ABuffer> csd0;
    if (!strncmp(mComponentName.c_str(), "OMX.qcom.video.decoder.mpeg4",
                 strlen("OMX.qcom.video.decoder.mpeg4"))
                 && msg->findBuffer("csd-0", &csd0)) {
        isDP = checkDPFromCodecSpecificData((const uint8_t*)csd0->data(),
                                                                csd0->size());
    }
    return isDP;
}

bool ExtendedACodec::checkDPFromCodecSpecificData(const uint8_t *data, size_t size) {
    bool retVal = false;
    size_t offset = 0, startCodeOffset = 0;
    bool isStartCode = false;
    const int kVolStartCode = 0x20;
    const char kStartCode[] = "\x00\x00\x01";
    // must contain at least 4 bytes for video_object_layer_start_code
    const size_t kMinCsdSize = 4;

    if (!data || (size < kMinCsdSize)) {
        AVLOGV("Invalid CSD (expected at least %zu bytes)", kMinCsdSize);
        return retVal;
    }

    while (offset < size - 3) {
        if ((data[offset + 3] & 0xf0) == kVolStartCode) {
            if (!memcmp(&data[offset], kStartCode, 3)) {
                startCodeOffset = offset;
                isStartCode = true;
                break;
            }
        }

        offset++;
    }

    if (isStartCode) {
        retVal = checkDPFromVOLHeader((const uint8_t*) &data[startCodeOffset],
                (size - startCodeOffset));
    }

    return retVal;
}

bool ExtendedACodec::checkDPFromVOLHeader(const uint8_t *data, size_t size) {
    bool retVal = false;
    // must contain at least 4 bytes for video_object_layer_start_code + 9 bits of data
    const size_t kMinHeaderSize = 6;

    if (!data || (size < kMinHeaderSize)) {
        AVLOGV("Invalid VOL header (expected at least %zu bytes)", kMinHeaderSize);
        return false;
    }

    AVLOGV("Checking for MPEG4 DP bit");
    ABitReader br(&data[4], (size - 4));
    br.skipBits(1); // random_accessible_vol

    unsigned videoObjectTypeIndication = br.getBits(8);
    if (videoObjectTypeIndication == 0x12u) {
        AVLOGW("checkDPFromVOLHeader: videoObjectTypeIndication:%u",
               videoObjectTypeIndication);
        return false;
    }

    unsigned videoObjectLayerVerid = 1;
    if (br.getBits(1)) {
        videoObjectLayerVerid = br.getBits(4);
        br.skipBits(3); // video_object_layer_priority
        AVLOGV("checkDPFromVOLHeader: videoObjectLayerVerid:%u",
               videoObjectLayerVerid);
    }

    if (br.getBits(4) == 0x0f) { // aspect_ratio_info
        AVLOGV("checkDPFromVOLHeader: extended PAR");
        br.skipBits(8); // par_width
        br.skipBits(8); // par_height
    }

    if (br.getBits(1)) { // vol_control_parameters
        br.skipBits(2);  // chroma_format
        br.skipBits(1);  // low_delay
        if (br.getBits(1)) { // vbv_parameters
            br.skipBits(15); // first_half_bit_rate
            br.skipBits(1);  // marker_bit
            br.skipBits(15); // latter_half_bit_rate
            br.skipBits(1);  // marker_bit
            br.skipBits(15); // first_half_vbv_buffer_size
            br.skipBits(1);  // marker_bit
            br.skipBits(3);  // latter_half_vbv_buffer_size
            br.skipBits(11); // first_half_vbv_occupancy
            br.skipBits(1);  // marker_bit
            br.skipBits(15); // latter_half_vbv_occupancy
            br.skipBits(1);  // marker_bit
        }
    }

    unsigned videoObjectLayerShape = br.getBits(2);
    if (videoObjectLayerShape != 0x00u /* rectangular */) {
        AVLOGV("checkDPFromVOLHeader: videoObjectLayerShape:%x",
               videoObjectLayerShape);
        return false;
    }

    br.skipBits(1); // marker_bit
    unsigned vopTimeIncrementResolution = br.getBits(16);
    br.skipBits(1); // marker_bit
    if (br.getBits(1)) {  // fixed_vop_rate
        // range [0..vopTimeIncrementResolution)

        // vopTimeIncrementResolution
        // 2 => 0..1, 1 bit
        // 3 => 0..2, 2 bits
        // 4 => 0..3, 2 bits
        // 5 => 0..4, 3 bits
        // ...

        if (vopTimeIncrementResolution <= 0u) {
            return BAD_VALUE;
        }

        --vopTimeIncrementResolution;
        unsigned numBits = 0;
        while (vopTimeIncrementResolution > 0) {
            ++numBits;
            vopTimeIncrementResolution >>= 1;
        }

        br.skipBits(numBits);  // fixed_vop_time_increment
    }

    br.skipBits(1);  // marker_bit
    br.skipBits(13); // video_object_layer_width
    br.skipBits(1);  // marker_bit
    br.skipBits(13); // video_object_layer_height
    br.skipBits(1);  // marker_bit
    br.skipBits(1);  // interlaced
    br.skipBits(1);  // obmc_disable
    unsigned spriteEnable = 0;
    if (videoObjectLayerVerid == 1) {
        spriteEnable = br.getBits(1);
    } else {
        spriteEnable = br.getBits(2);
    }

    if (spriteEnable == 0x1) { // static
        int spriteWidth = br.getBits(13);
        AVLOGV("checkDPFromVOLHeader: spriteWidth:%d", spriteWidth);
        br.skipBits(1) ; // marker_bit
        br.skipBits(13); // sprite_height
        br.skipBits(1);  // marker_bit
        br.skipBits(13); // sprite_left_coordinate
        br.skipBits(1);  // marker_bit
        br.skipBits(13); // sprite_top_coordinate
        br.skipBits(1);  // marker_bit
        br.skipBits(6);  // no_of_sprite_warping_points
        br.skipBits(2);  // sprite_warping_accuracy
        br.skipBits(1);  // sprite_brightness_change
        br.skipBits(1);  // low_latency_sprite_enable
    } else if (spriteEnable == 0x2) { // GMC
        br.skipBits(6); // no_of_sprite_warping_points
        br.skipBits(2); // sprite_warping_accuracy
        br.skipBits(1); // sprite_brightness_change
    }

    if (videoObjectLayerVerid != 1
            && videoObjectLayerShape != 0x0u) {
        br.skipBits(1);
    }

    if (br.getBits(1)) { // not_8_bit
        br.skipBits(4);  // quant_precision
        br.skipBits(4);  // bits_per_pixel
    }

    if (videoObjectLayerShape == 0x3) {
        br.skipBits(1);
        br.skipBits(1);
        br.skipBits(1);
    }

    if (br.getBits(1)) { // quant_type
        if (br.getBits(1)) { // load_intra_quant_mat
            unsigned IntraQuantMat = 1;
            for (int i = 0; i < 64 && IntraQuantMat; i++) {
                 IntraQuantMat = br.getBits(8);
            }
        }

        if (br.getBits(1)) { // load_non_intra_quant_matrix
            unsigned NonIntraQuantMat = 1;
            for (int i = 0; i < 64 && NonIntraQuantMat; i++) {
                 NonIntraQuantMat = br.getBits(8);
            }
        }
    } /* quantType */

    if (videoObjectLayerVerid != 1) {
        unsigned quarterSample = br.getBits(1);
        AVLOGV("checkDPFromVOLHeader: quarterSample:%u",
                quarterSample);
    }

    br.skipBits(1); // complexity_estimation_disable
    br.skipBits(1); // resync_marker_disable
    unsigned dataPartitioned = br.getBits(1);
    if (dataPartitioned) {
        retVal = true;
    }

    AVLOGD("checkDPFromVOLHeader: DP:%u", dataPartitioned);
    return retVal;
}

status_t ExtendedACodec::setupVideoDecoder(
        const char *mime, const sp<AMessage> &msg, bool usingNativeBuffers,
        bool usingSwRenderer, sp<AMessage> &outputFormat) {
    AVLOGI("setupVideoDecoder()");
    status_t err = OK;
    bool isQCComponent = mComponentName.startsWith("OMX.qcom.") ||
            mComponentName.startsWith("OMX.ittiam.") ||
            mComponentName.startsWith("OMX.qti.");

     if (isQCComponent) {
        // Setup Vpp parameters before port definition on input and output as vpp algo can be
        // changed only when vpp is in init state.
        err = configureVpp(msg);
        if (err != OK) {
            AVLOGW("Configuring of vpp failed with err:%d",err);
        }
    }

    err = ACodec::setupVideoDecoder(mime, msg, usingNativeBuffers,
        usingSwRenderer, outputFormat);

    if (err != OK || !isQCComponent) {
        return err;
    }

    configureFramePackingFormat(msg);
    setDIVXFormat(msg, mime);

    AString fileFormat;
    const char *fileFormatCStr = NULL;
    bool success = msg->findString(ExtendedUtils::getMsgFromKey(kKeyFileFormat), &fileFormat);
    if (success) {
        fileFormatCStr = fileFormat.c_str();
    }

    // Enable timestamp reordering for mpeg4 and vc1 codec types, the AVI file
    // type, and hevc content in the ts container
    bool tsReorder = false;
    const char* roleVC1 = "OMX.qcom.video.decoder.vc1";
    const char* roleMPEG4 = "OMX.qcom.video.decoder.mpeg4";
    const char* roleHEVC = "OMX.qcom.video.decoder.hevc";
    if (!strncmp(mComponentName.c_str(), roleVC1, strlen(roleVC1)) ||
            !strncmp(mComponentName.c_str(), roleMPEG4, strlen(roleMPEG4))) {
        // The codec requires timestamp reordering
        tsReorder = true;
    } else if (fileFormatCStr!= NULL) {
        // Check for containers that support timestamp reordering
        AVLOGD("Container format = %s", fileFormatCStr);
        if (!strncmp(fileFormatCStr, "video/avi", 9)) {
            // The container requires timestamp reordering
            tsReorder = true;
        } else if (!strncmp(fileFormatCStr, MEDIA_MIMETYPE_CONTAINER_MPEG2TS,
                strlen(MEDIA_MIMETYPE_CONTAINER_MPEG2TS)) &&
                !strncmp(mComponentName.c_str(), roleHEVC, strlen(roleHEVC))) {
            // HEVC content in the TS container requires timestamp reordering
            tsReorder = true;
        }
    }

    if (tsReorder) {
        AVLOGI("Enabling timestamp reordering");
        QOMX_INDEXTIMESTAMPREORDER reorder;
        InitOMXParams(&reorder);
        reorder.nPortIndex = kPortIndexOutput;
        reorder.bEnable = OMX_TRUE;
        status_t err = mOMX->setParameter(mNode,
                       (OMX_INDEXTYPE)OMX_QcomIndexParamEnableTimeStampReorder,
                       (void *)&reorder, sizeof(reorder));

        if (err != OK) {
            AVLOGW("Failed to enable timestamp reordering");
        }
    }

    // Enable Sync-frame decode mode for thumbnails
    int32_t thumbnailMode = 0;
    if (msg->findInt32("thumbnail-mode", &thumbnailMode) &&
        thumbnailMode) {
        AVLOGD("Enabling thumbnail mode.");
        QOMX_ENABLETYPE enableType;
        OMX_INDEXTYPE indexType;
        InitOMXParams(&enableType);

        status_t err = mOMX->getExtensionIndex(
                mNode, OMX_QCOM_INDEX_PARAM_VIDEO_SYNCFRAMEDECODINGMODE,
                &indexType);
        if (err != OK) {
            AVLOGW("Failed to get extension for SYNCFRAMEDECODINGMODE");
            return err;
        }

        enableType.bEnable = OMX_TRUE;
        err = mOMX->setParameter(mNode, indexType,
                   (void *)&enableType, sizeof(enableType));
        if (err != OK) {
            AVLOGW("Failed to set extension for SYNCFRAMEDECODINGMODE");
            return err;
        }
        AVLOGI("Thumbnail mode enabled.");
    }

    // MediaCodec clients can request decoder extradata by setting
    // "enable-extradata-<type>" in MediaFormat.
    // Following <type>s are supported:
    //    "user" => user-extradata
    int extraDataRequested = 0;
    if (msg->findInt32("enable-extradata-user", &extraDataRequested) &&
            extraDataRequested == 1) {
        AVLOGI("[%s] User-extradata requested", mComponentName.c_str());
        QOMX_ENABLETYPE enableType;
        InitOMXParams(&enableType);
        enableType.bEnable = OMX_TRUE;

        status_t err = mOMX->setParameter(
                mNode, (OMX_INDEXTYPE)OMX_QcomIndexEnableExtnUserData,
                (void *)&enableType, sizeof(enableType));
        if (err != OK) {
            AVLOGW("[%s] Failed to enable user-extradata", mComponentName.c_str());
        }
    }

    /* VQZIP */
    int32_t vqzipEnabled = 0;
    if (msg->findInt32("vqzip", (int32_t *)&vqzipEnabled) && vqzipEnabled) {
        OMX_QTI_VIDEO_PARAM_VQZIP_SEI_TYPE enableType;
        InitOMXParams(&enableType);

        enableType.bEnable = OMX_TRUE;
        status_t err = mOMX->setParameter(
                mNode, (OMX_INDEXTYPE)OMX_QTIIndexParamVQZIPSEIType,
                (void *)&enableType, sizeof(enableType));
        if (err != OK) {
            AVLOGW("[%s] Failed to enable VQZIP SEI type", mComponentName.c_str());
        }
    }

#ifdef OMX_QTI_INDEX_PARAM_VIDEO_LOW_LATENCY
    uint32_t lowLatency;
    bool paramsSet = false;
    paramsSet = msg->findInt32("vt-low-latency", (int32_t *)&lowLatency);
    if (paramsSet) {
        OMX_INDEXTYPE indexType;
        QOMX_EXTNINDEX_VIDEO_VENC_LOW_LATENCY_MODE lowLatencyMode;
        InitOMXParams(&lowLatencyMode);
        AVLOGI("setting vt-low-latency");
        status_t err = mOMX->getExtensionIndex(
                mNode, OMX_QTI_INDEX_PARAM_VIDEO_LOW_LATENCY,
                &indexType);
        if (err != OK) {
            AVLOGE("Failed to get extension for Low Latency");
            return err;
        }

        lowLatencyMode.bLowLatencyMode = lowLatency ? OMX_TRUE : OMX_FALSE;
        err = mOMX->setParameter(mNode, indexType,
                (void *)&lowLatencyMode, sizeof(lowLatencyMode));
        if (err != OK) {
            AVLOGE("Failed to set Low Latency");
            return err;
        }
    }
#endif

    return OK;
}

status_t ExtendedACodec::setupVideoEncoder(
            const char *mime, const sp<AMessage> &msg,
            sp<AMessage> &outputformat, sp<AMessage> &inputformat) {
    AVLOGI("setupVideoEncoder()");
    status_t err = ACodec::setupVideoEncoder(mime, msg, outputformat, inputformat);

    bool isQCComponent = mComponentName.startsWith("OMX.qcom.");

    if (err != OK || !isQCComponent) {
        return err;
    }

    AVLOGI("[%s] configure, AMessage : %s\n", mComponentName.c_str(), msg->debugString().c_str());

    // Set batch size for batch-mode encoding
    int32_t batchSize;
    if (msg->findInt32("batch-size", &batchSize)) {
        OMX_INDEXTYPE indexType;
        OMX_PARAM_U32TYPE batch;
        InitOMXParams(&batch);
        AVLOGI("Configuring batch-size: %d", batchSize);

        err = mOMX->getExtensionIndex(
                mNode, "OMX.QCOM.index.param.video.InputBatch", &indexType);
        if (err != OK) {
            AVLOGE("Failed to get extension for OMX.QCOM.index.param.video.InputBatch");
            return err;
        }

        err = mOMX->getParameter(mNode, indexType, (void *)&batch, sizeof(batch));
        if (err != OK) {
            AVLOGE("Failed to get InputBatch");
            return err;
        }

        batch.nU32 = batchSize;
        err = mOMX->setParameter(mNode, indexType, (void *)&batch, sizeof(batch));
        if (err != OK) {
            AVLOGE("Failed to set InputBatch");
            return err;
        }
    }

    OMX_INDEXTYPE indexType;
    bool paramsSet = false;
    uint32_t prependSpspps;

    paramsSet = msg->findInt32("vt-sequence-hdr-with-idr", (int32_t *)&prependSpspps);
    if (paramsSet) {
        PrependSPSPPSToIDRFramesParams SPSPPSIframe;
        InitOMXParams(&SPSPPSIframe);

        err = mOMX->getExtensionIndex(
                mNode, "OMX.google.android.index.prependSPSPPSToIDRFrames",
                &indexType);
        if (err != OK) {
            AVLOGE("Failed to get extension for PrependSPSPPSToIDRFramesParams");
            return err;
        }

        err = mOMX->getParameter(mNode, indexType,
                (void *)&SPSPPSIframe, sizeof(SPSPPSIframe));
        if (err != OK) {
            AVLOGE("Failed to get PrependSPSPPSToIDRFramesParams");
            return err;
        }

        SPSPPSIframe.bEnable = (OMX_BOOL)prependSpspps;
        err = mOMX->setParameter(mNode, indexType,
                (void *)&SPSPPSIframe, sizeof(SPSPPSIframe));
        if (err != OK) {
            AVLOGE("Failed to set PrependSPSPPSToIDRFramesParams");
            return err;
        }
    }

    uint32_t profile = 0, extended = 0, sliceSize = 0, sliceHdrSpacing = 0;
    uint32_t sliceMode = OMX_SKYPE_VIDEO_SliceControlModeNone;
    paramsSet = msg->findInt32("vt-extension-profile", (int32_t *)&profile);
    paramsSet |= msg->findInt32("vt-use-extended-profile", (int32_t *)&extended);
    paramsSet |= msg->findInt32("vt-slice-control-mode", (int32_t *)&sliceMode);
    paramsSet |= msg->findInt32("vt-slice-hdr-spacing", (int32_t *)&sliceSize);
    // Only profile given but no level so get it from component.
    if (paramsSet) {
        OMX_VIDEO_PARAM_AVCTYPE h264type;
        InitOMXParams(&h264type);
        h264type.nPortIndex = kPortIndexOutput;

        status_t err = OK;

        if (extended) {
            err = mOMX->getParameter(mNode, OMX_IndexParamVideoAvc,
                (void *)&h264type, sizeof(h264type));
            if (err != OK) {
                AVLOGE("Failed to get Param Video AVC");
                return err;
            }
            if (profile == OMX_VIDEO_AVCProfileBaseline) {
                h264type.eProfile = static_cast<OMX_VIDEO_AVCPROFILETYPE>(QOMX_VIDEO_AVCProfileConstrainedBaseline);
            } else if (profile == OMX_VIDEO_AVCProfileHigh) {
                h264type.eProfile = static_cast<OMX_VIDEO_AVCPROFILETYPE>(QOMX_VIDEO_AVCProfileConstrainedHigh);
            } else {
                AVLOGE("Invalid profile set");
                return BAD_VALUE;
            }
            err = mOMX->setParameter(mNode,
                OMX_IndexParamVideoAvc, &h264type, sizeof(h264type));

            if (err != OK) {
                AVLOGE("Failed to set Profile");
                return err;
            }
        }

        sliceHdrSpacing = sliceSize;
        switch ((OMX_SKYPE_VIDEO_SliceControlMode)sliceMode) {
        case OMX_SKYPE_VIDEO_SliceControlModeNone:
            break;
        case OMX_SKYPE_VIDEO_SliceControlModMBRow:
        {
            uint32_t width = 0, height = 0;
            OMX_PARAM_PORTDEFINITIONTYPE def;
            InitOMXParams(&def);
            def.nPortIndex = kPortIndexOutput;
            OMX_VIDEO_PORTDEFINITIONTYPE *videoDef = &def.format.video;

            err = mOMX->getParameter( mNode,
                    OMX_IndexParamPortDefinition, &def, sizeof(def));
            if (err != OK) {
                AVLOGE("SliceControlModeMBRow: failed to get port defn");
                return err;
            }

            height = videoDef->nFrameHeight >> 4;
            if (height < sliceSize) {
                AVLOGE("Invalid Slice Size set");
                return BAD_VALUE;
            } else if (height == sliceSize) {
                AVLOGV("Slice Size == Frame Size");
                width = 1;
            } else {
                width = videoDef->nFrameWidth >> 4;
            }
            sliceHdrSpacing = sliceSize * width;
        }
        /* Continue to ModeMB */
        case OMX_SKYPE_VIDEO_SliceControlModeMB:
            err = mOMX->getParameter(mNode, OMX_IndexParamVideoAvc,
                (void *)&h264type, sizeof(h264type));
            if (err != OK) {
                AVLOGE("Failed to get Param Video AVC");
                return err;
            }
            h264type.nSliceHeaderSpacing = sliceHdrSpacing;
            err = mOMX->setParameter(mNode,
                OMX_IndexParamVideoAvc, &h264type, sizeof(h264type));

            if (err != OK) {
                AVLOGE("Failed to set Slice Params");
                return err;
            }
            break;
        case OMX_SKYPE_VIDEO_SliceControlModeByte:
            OMX_VIDEO_PARAM_ERRORCORRECTIONTYPE errorCorrection;
            InitOMXParams(&errorCorrection);

            errorCorrection.nPortIndex = kPortIndexOutput;
            err = mOMX->getParameter(mNode,
                    OMX_IndexParamVideoErrorCorrection,
                    &errorCorrection, sizeof(errorCorrection));
            if (err != OK) {
                AVLOGE("Failed to get slice mode bytes");
                return err;
            }

            errorCorrection.bEnableRVLC = (OMX_BOOL)false;
            errorCorrection.bEnableDataPartitioning = (OMX_BOOL)false;
            errorCorrection.bEnableResync = (OMX_BOOL)true;
            errorCorrection.nResynchMarkerSpacing = sliceSize;
            err = mOMX->setParameter(mNode, OMX_IndexParamVideoErrorCorrection,
                    (void *)&errorCorrection, sizeof(errorCorrection));
            if (err != OK) {
                AVLOGE("Failed to set slice mode bytes");
                return err;
            }
            break;
        default:
            AVLOGE("Invalid SliceMode selected");
            return BAD_VALUE;
        }
    }

    uint32_t ltrFrames;

    paramsSet = msg->findInt32("vt-num-ltr-frames", (int32_t *)&ltrFrames);
    if (paramsSet) {
        QOMX_VIDEO_PARAM_LTRCOUNT_TYPE ltrFrameCount;
        InitOMXParams(&ltrFrameCount);

        status_t err = mOMX->getExtensionIndex(
                mNode, OMX_QCOM_INDEX_PARAM_VIDEO_LTRCOUNT,
                &indexType);
        if (err != OK) {
            AVLOGE("Failed to get extension for LTR");
            return err;
        }

        err = mOMX->getParameter(mNode, indexType,
                (void *)&ltrFrameCount, sizeof(ltrFrameCount));
        if (err != OK) {
            AVLOGE("Failed to get LTR");
            return err;
        }

        ltrFrameCount.nCount = ltrFrames;
        err = mOMX->setParameter(mNode, indexType,
                (void *)&ltrFrameCount, sizeof(ltrFrameCount));
        if (err != OK) {
            AVLOGE("Failed to set LTR");
            return err;
        }
    }

    uint32_t hpType = 0, maxLayercount = 0;
    // set Hierarchial layers with type  = P and layers = layers;
    paramsSet = msg->findInt32("vt-video-hierar-type", (int32_t *)&hpType);
    paramsSet |= msg->findInt32("vt-max-temporal-layer-count", (int32_t *)&maxLayercount);
    if (paramsSet && hpType) {
        QOMX_VIDEO_HIERARCHICALLAYERS hierType;
        InitOMXParams(&hierType);

        status_t err = mOMX->getExtensionIndex(
                mNode, OMX_QCOM_INDEX_PARAM_VIDEO_HIERSTRUCTURE,
                &indexType);
        if (err != OK) {
            AVLOGE("Failed to get extension for hier-p");
            return err;
        }

        err = mOMX->getParameter(mNode, indexType,
                (void *)&hierType, sizeof(hierType));
        if (err != OK) {
            AVLOGE("Failed to get hier-p");
            return err;
        }

        hierType.eHierarchicalCodingType = static_cast<QOMX_VIDEO_HIERARCHICALCODINGTYPE>(hpType);
        hierType.nNumLayers = maxLayercount;
        err = mOMX->setParameter(mNode, indexType,
                (void *)&hierType, sizeof(hierType));
        if (err != OK) {
            AVLOGE("Failed to set hier-p");
            return err;
        }
    }

    uint32_t sarIndex = 0, sarWidth = 0, sarHeight = 0;
    // If SAR is enabled then set the width and height here
    paramsSet = msg->findInt32("vt-sar-index", (int32_t *)&sarIndex);
    paramsSet |= msg->findInt32("vt-sar-width", (int32_t *)&sarWidth);
    paramsSet |= msg->findInt32("vt-sar-height", (int32_t *)&sarHeight);

    if (paramsSet && sarIndex) {
        QOMX_EXTNINDEX_VIDEO_VENC_SAR Sar;
        InitOMXParams(&Sar);
        status_t err = mOMX->getExtensionIndex(
                mNode, OMX_QCOM_INDEX_PARAM_VIDEO_SAR,
                &indexType);
        if (err != OK) {
            AVLOGE("Failed to get extension for SAR info");
            return err;
        }

        err = mOMX->getParameter(mNode, indexType,
                (void *)&Sar, sizeof(Sar));
        if (err != OK) {
            AVLOGE("Failed to get SAR info");
            return err;
        }

        Sar.nSARWidth = sarWidth;
        Sar.nSARHeight = sarHeight;

        err = mOMX->setParameter(mNode, indexType,
                (void *)&Sar, sizeof(Sar));
        if (err != OK) {
            AVLOGE("Failed to set SAR info");
            return err;
        }
    }

    uint32_t rateControl;
    paramsSet = msg->findInt32("vt-ratecontrol", (int32_t *)&rateControl);
    if (paramsSet) {
        OMX_VIDEO_PARAM_BITRATETYPE bitrateType;
        InitOMXParams(&bitrateType);
        bitrateType.nPortIndex = kPortIndexOutput;
        status_t err = mOMX->getParameter(
                               mNode, OMX_IndexParamVideoBitrate,
                               &bitrateType, sizeof(bitrateType));
        if (err != OK) {
            AVLOGE("Failed to get RC");
            return err;
        }
        bitrateType.eControlRate = (OMX_VIDEO_CONTROLRATETYPE) rateControl;
        bitrateType.nTargetBitrate = bitrateType.nTargetBitrate;

        err = mOMX->setParameter(
                       mNode, OMX_IndexParamVideoBitrate,
                       &bitrateType, sizeof(bitrateType));
        if(err != OK) {
            AVLOGE("Failed to set RC");
            return err;
        }
    }

#ifdef OMX_QTI_INDEX_PARAM_VIDEO_LOW_LATENCY
    uint32_t lowLatency;
    paramsSet = msg->findInt32("vt-low-latency", (int32_t *)&lowLatency);
    if (paramsSet) {
        QOMX_EXTNINDEX_VIDEO_VENC_LOW_LATENCY_MODE lowLatencyMode;
        InitOMXParams(&lowLatencyMode);
        AVLOGV("setting vt-low-latency");
        status_t err = mOMX->getExtensionIndex(
                mNode, OMX_QTI_INDEX_PARAM_VIDEO_LOW_LATENCY,
                &indexType);
        if (err != OK) {
            AVLOGE("Failed to get extension for Low Latency");
            return err;
        }

        err = mOMX->getParameter(mNode, indexType,
                (void *)&lowLatencyMode, sizeof(lowLatencyMode));
        if (err != OK) {
            AVLOGE("Failed to get Low Latency");
            return err;
        }

        lowLatencyMode.bLowLatencyMode = lowLatency ? OMX_TRUE : OMX_FALSE;
        err = mOMX->setParameter(mNode, indexType,
                (void *)&lowLatencyMode, sizeof(lowLatencyMode));
        if (err != OK) {
            AVLOGE("Failed to set Low Latency");
            return err;
        }
    }
#endif

    err = setupVQZIP(msg);
    if (err != OK) {
        AVLOGW("[%s] Failed to set component up VQZIP", mComponentName.c_str());
    }

    return OK;
}

status_t ExtendedACodec::setParameters(const sp<AMessage> &msg) {
    AVLOGV("setParameters()");
    status_t err = ACodec::setParameters(msg);

    bool isQCComponent = mComponentName.startsWith("OMX.qcom");

    //do nothing for non QC component and QC decoders
    if (err != OK || !isQCComponent) {
        return err;
    }

    err = setVppParameters(msg);
    if (err != OK) {
        AVLOGW("Setting of vpp parameters failed with err:%d",err);
    }

    int32_t markLtr;
    OMX_INDEXTYPE indexType;

    if (msg->findInt32("vt-config-mark-ltr", &markLtr)) {
        OMX_QCOM_VIDEO_CONFIG_LTRMARK_TYPE markLTRParams;
        InitOMXParams(&markLTRParams);

        err = mOMX->getExtensionIndex(
                mNode, OMX_QCOM_INDEX_CONFIG_VIDEO_LTRMARK,
                &indexType);
        if (err != OK) {
            AVLOGE("Failed to get extension for mark LTR");
            return err;
        }
        markLTRParams.nPortIndex = kPortIndexInput;
        markLTRParams.nID = markLtr;
        err = mOMX->setConfig(mNode, indexType,
                (void *)&markLTRParams, sizeof(markLTRParams));
        if (err != OK) {
            AVLOGE("Failed to set mark LTR");
            return err;
        }
    }

    int32_t useLtr;
    if (msg->findInt32("vt-config-use-ltr", &useLtr)) {
        OMX_QCOM_VIDEO_CONFIG_LTRUSE_TYPE useLTRParams;
        InitOMXParams(&useLTRParams);

        err = mOMX->getExtensionIndex(
                mNode, OMX_QCOM_INDEX_CONFIG_VIDEO_LTRUSE,
                &indexType);
        if (err != OK) {
            AVLOGE("Failed to get extension for use LTR");
            return err;
        }
        useLTRParams.nPortIndex = kPortIndexInput;
        useLTRParams.nID = useLtr;
        err = mOMX->setConfig(mNode, indexType,
                (void *)&useLTRParams, sizeof(useLTRParams));
        if (err != OK) {
            AVLOGE("Failed to set use LTR");
            return err;
        }
    }

    int32_t frameQp;
    if (msg->findInt32("vt-config-frame-qp", &frameQp)) {
        OMX_SKYPE_VIDEO_CONFIG_QP frameQPParams;
        InitOMXParams(&frameQPParams);

        err = mOMX->getExtensionIndex(
                mNode, OMX_QCOM_INDEX_CONFIG_VIDEO_QP,
                &indexType);
        if (err != OK) {
            AVLOGE("Failed to get extension for frame QP");
            return err;
        }

        frameQPParams.nQP = frameQp;
        err = mOMX->setConfig(mNode, indexType,
                (void *)&frameQPParams, sizeof(frameQPParams));
        if (err != OK) {
            AVLOGE("Failed to set frame QP");
            return err;
        }
    }

    int32_t templayerCount;
    if (msg->findInt32("vt-config-temporal-layer-count", &templayerCount)
            && templayerCount) {

        // Compatibility WA till QOMX_EXTNINDEX_VIDEO_HIER_P_LAYERS is available
#ifndef OMX_QCOM_INDEX_CONFIG_VIDEO_HIER_P_LAYERS
        QOMX_EXTNINDEX_VIDEO_MAX_HIER_P_LAYERS tpLayerCountParams;
        InitOMXParams(&tpLayerCountParams);

        err = mOMX->getExtensionIndex(
                mNode, OMX_QCOM_INDEX_CONFIG_VIDEO_MAX_HIER_P_LAYERS,
                &indexType);
        if (err != OK) {
            AVLOGE("Failed to get extension for hier-p layers");
            return err;
        }

        tpLayerCountParams.nMaxHierLayers = templayerCount;
        err = mOMX->setConfig(mNode, indexType,
                (void *)&tpLayerCountParams, sizeof(tpLayerCountParams));
        if (err != OK) {
            AVLOGE("Failed to set hier-p layers");
            return err;
        }
#else
        QOMX_EXTNINDEX_VIDEO_HIER_P_LAYERS tpLayerCountParams;
        InitOMXParams(&tpLayerCountParams);

        err = mOMX->getExtensionIndex(
                mNode, OMX_QCOM_INDEX_CONFIG_VIDEO_HIER_P_LAYERS,
                &indexType);
        if (err != OK) {
            AVLOGE("Failed to get extension for hier-p layers");
            return err;
        }

        tpLayerCountParams.nNumHierLayers = templayerCount;
        err = mOMX->setConfig(mNode, indexType,
                (void *)&tpLayerCountParams, sizeof(tpLayerCountParams));
        if (err != OK) {
            AVLOGE("Failed to set hier-p layers");
            return err;
        }
#endif

    }

    int32_t baselayerPid;
    if (msg->findInt32("vt-config-base-layer-pid", &baselayerPid)) {
        OMX_SKYPE_VIDEO_CONFIG_BASELAYERPID baseLayerPidParams;
        InitOMXParams(&baseLayerPidParams);

        err = mOMX->getExtensionIndex(
                mNode, OMX_QCOM_INDEX_PARAM_VIDEO_BASE_LAYER_ID,
                &indexType);
        if (err != OK) {
            AVLOGE("Failed to get extension for base layer pid");
            return err;
        }

        baseLayerPidParams.nPID = baselayerPid;
        err = mOMX->setConfig( mNode, indexType,
                (void *)&baseLayerPidParams, sizeof(baseLayerPidParams));
        if (err != OK) {
            AVLOGE("Failed to set base layer pid");
            return err;
        }
    }

    int32_t left, top, width, height;
    if (msg->findInt32("vt-config-input-crop-left", &left)
        && msg->findInt32("vt-config-input-crop-top", &top)
        && msg->findInt32("vt-config-input-crop-width", &width)
        && msg->findInt32("vt-config-input-crop-height", &height)) {
        OMX_CONFIG_RECTTYPE rectParams;
        InitOMXParams(&rectParams);

        err = mOMX->getExtensionIndex(
                mNode, OMX_QCOM_INDEX_CONFIG_RECTANGLE_TYPE,
                &indexType);
        if (err != OK) {
            AVLOGE("Failed to get extension for rect type");
            return err;
        }

        err = mOMX->getConfig(mNode, indexType,
                (void *)&rectParams, sizeof(rectParams));
        if (err != OK) {
            AVLOGE("Failed to get rect type");
            return err;
        }

        rectParams.nLeft = left;
        rectParams.nTop = top;
        rectParams.nWidth = width;
        rectParams.nHeight = height;
        err = mOMX->setConfig(mNode, indexType,
                (void *)&rectParams, sizeof(rectParams));
        if (err != OK) {
            AVLOGE("Failed to set rect type");
            return err;
        }
    }

    int32_t rotate;
    if (msg->findInt32("vt-config-rotate", &rotate)) {
        OMX_CONFIG_ROTATIONTYPE rotationParams;
        InitOMXParams(&rotationParams);

        err = mOMX->getConfig(mNode, OMX_IndexConfigCommonRotate,
                (void *)&rotationParams, sizeof(rotationParams));
        if (err != OK) {
            AVLOGE("Failed to get rotation config");
            return err;
        }

        rotationParams.nRotation = rotate;
        err = mOMX->setConfig(mNode, OMX_IndexConfigCommonRotate,
                (void *)&rotationParams, sizeof(rotationParams));
        if (err != OK) {
            AVLOGE("Failed to set rotation config");
            return err;
        }
    }


    int64_t       nTimeUs;
    if (msg->findInt64("vt-config-timestamp", (int64_t*)&nTimeUs)) {
        OMX_INDEXTYPE nIndex;
        OMX_TIME_CONFIG_TIMESTAMPTYPE timeStampInfo;
        InitOMXParams(&timeStampInfo);

        if (mOMX->getExtensionIndex (mNode,
            "OMX.QTI.index.config.video.settimedata",
            &nIndex) != OMX_ErrorNone) {
            AVLOGV("Time Stamp config not supported");
            return false;
        }

        timeStampInfo.nTimestamp = nTimeUs;
        AVLOGV("nTimeStamp = %lld", timeStampInfo.nTimestamp);
        err = mOMX->setConfig(mNode,
                 nIndex,
                 (void*)&timeStampInfo,
                 sizeof(timeStampInfo));
        if (err != OK) {
            AVLOGE("Failed to set nTimeStamp = %lld",
                timeStampInfo.nTimestamp);
        }
        return true;
    }

    return OK;
}

status_t ExtendedACodec::getVQZIPInfo(const sp<AMessage> &msg) {

    bool isQCComponent = mComponentName == AString("OMX.qcom.video.decoder.avc");

    //do nothing for non avc QC decoder component and QC encoders
    if (!isQCComponent) {
        return OK;
    }

    (void)msg;

    OMX_VIDEO_PARAM_PROFILELEVELTYPE profileLevel;
    QOMX_VIDEO_H264ENTROPYCODINGTYPE entropy;
    InitOMXParams(&profileLevel);
    InitOMXParams(&entropy);
    profileLevel.nPortIndex = kPortIndexInput;

    status_t err = mOMX->getParameter(
            mNode, (OMX_INDEXTYPE)OMX_IndexParamVideoProfileLevelCurrent, &profileLevel, sizeof(profileLevel));
    if (err != OK) {
        AVLOGW("Failed to get Profile and Level from Component");
    }

    err = mOMX->getConfig(
            mNode, (OMX_INDEXTYPE)OMX_QcomIndexConfigH264EntropyCodingCabac, &entropy, sizeof(entropy));
    if (err != OK) {
        AVLOGW("Failed to get Entropy from Component");
    }

    msg->setInt32("vqzip-profile", profileLevel.eProfile);
    msg->setInt32("vqzip-level", profileLevel.eLevel);
    msg->setInt32("vqzip-entropy", entropy.bCabac);

    return OK;
}

status_t ExtendedACodec::setupCustomCodec(
        status_t inputErr, const char *mime, const sp<AMessage> &msg) {
    status_t err = inputErr;

    AVLOGD("setupCustomCodec for %s", mime);
    if (mIsEncoder) {
        int32_t numChannels, sampleRate;
        if (msg->findInt32("channel-count", &numChannels)
              && msg->findInt32("sample-rate", &sampleRate)) {
            setupRawAudioFormat(kPortIndexInput, sampleRate, numChannels);
        }
    }

    int32_t numChannels, sampleRate;
    CHECK(msg->findInt32("channel-count", &numChannels));
    CHECK(msg->findInt32("sample-rate", &sampleRate));
    if (!strcasecmp(MEDIA_MIMETYPE_AUDIO_EVRC, mime)) {
        err = setEVRCFormat(numChannels, sampleRate);
    } else if (!strcasecmp(MEDIA_MIMETYPE_AUDIO_QCELP, mime)) {
        err = setQCELPFormat(numChannels, sampleRate);
    } else if (!strcasecmp(MEDIA_MIMETYPE_AUDIO_AMR_WB_PLUS, mime)) {
        err = setAMRWBPLUSFormat(numChannels, sampleRate);
    } else if (!strcasecmp(MEDIA_MIMETYPE_AUDIO_WMA, mime))  {
        err = setWMAFormat(msg);
    } else if (!strcasecmp(MEDIA_MIMETYPE_AUDIO_FLAC, mime) && !mIsEncoder) {
        int32_t bitsPerSample;
        int32_t minBlkSize, maxBlkSize, minFrmSize, maxFrmSize;
        AVLOGV("ExtendedCodec::setAudioFormat(): FLAC Decoder");
        if (!msg->findInt32(ExtendedUtils::getMsgFromKey(kKeySampleBits), &bitsPerSample)) {
            //if the parser does not set the bitwidth, default it to 16 bit
            AVLOGI("no bitwidth, setting default bitwidth as 16 bits");
            bitsPerSample = 16;
        }
        if (!msg->findInt32(ExtendedUtils::getMsgFromKey(kKeyMinBlkSize), &minBlkSize)) {
            AVLOGI("no min blksize, setting default block size as 16");
            minBlkSize = 16;
        }

        if (!msg->findInt32(ExtendedUtils::getMsgFromKey(kKeyMaxBlkSize), &maxBlkSize)) {
            AVLOGI("no max blksize, setting default block size as 16");
            maxBlkSize = 16;
        }

        if (!msg->findInt32(ExtendedUtils::getMsgFromKey(kKeyMinFrmSize), &minFrmSize)) {
            AVLOGI("no min frame size, setting default frame size as 0");
            minFrmSize = 0;
        }

        if (!msg->findInt32(ExtendedUtils::getMsgFromKey(kKeyMaxFrmSize), &maxFrmSize)) {
            AVLOGI("no max frame size, setting default frame size as 0");
            maxFrmSize = 0;
        }
        err = setFLACDecoderFormat(numChannels, sampleRate, bitsPerSample, minBlkSize, maxBlkSize,
                minFrmSize, maxFrmSize);
    } else if (!strcasecmp(MEDIA_MIMETYPE_AUDIO_ALAC, mime)) {
        int32_t bitsPerSample;
        if (!msg->findInt32(ExtendedUtils::getMsgFromKey(kKeySampleBits), &bitsPerSample)) {
            //if the parser does not set the bitwidth, default it to 16 bit
            AVLOGI("no bitwidth, setting default bitwidth as 16 bits");
            bitsPerSample = 16;
        }
        err = setALACFormat(numChannels, sampleRate, bitsPerSample);
    } else if (!strcasecmp(MEDIA_MIMETYPE_AUDIO_APE, mime)) {
        int32_t bitsPerSample;
        if (!msg->findInt32(ExtendedUtils::getMsgFromKey(kKeySampleBits), &bitsPerSample)) {
            //if the parser does not set the bitwidth, default it to 16 bit
            AVLOGI("no bitwidth, setting default bitwidth as 16 bits");
            bitsPerSample = 16;
        }
        err = setAPEFormat(numChannels, sampleRate, bitsPerSample);
    } else if (!strcasecmp(MEDIA_MIMETYPE_AUDIO_DSD, mime)) {
        int32_t bitsPerSample;
        if (!msg->findInt32(ExtendedUtils::getMsgFromKey(kKeySampleBits), &bitsPerSample)) {
            //if the parser does not set the bitwidth, default it to 16 bit
            AVLOGI("no bitwidth, setting default bitwidth as 16 bits");
            bitsPerSample = 16;
        }
        err = setDSDFormat(numChannels, sampleRate, bitsPerSample);
    } // TODO Audio : handle other Audio formats here

    return err;
}

status_t ExtendedACodec::setQCELPFormat(
       int32_t numChannels, int32_t /*sampleRate*/) {

   status_t err = OMX_ErrorNone;
   if (mIsEncoder) {
       CHECK(numChannels == 1);
       //////////////// input port ////////////////////
       //handle->setRawAudioFormat(kPortIndexInput, sampleRate, numChannels);
       //////////////// output port ////////////////////
       // format
       OMX_AUDIO_PARAM_PORTFORMATTYPE format;
       InitOMXParams(&format);
       format.nPortIndex = kPortIndexOutput;
       format.nIndex = 0;

       while (OMX_ErrorNone == err) {
            CHECK_EQ(mOMX->getParameter(mNode, OMX_IndexParamAudioPortFormat,
                    &format, sizeof(format)), (status_t)OK);
            if (format.eEncoding == OMX_AUDIO_CodingQCELP13) {
                break;
            }
            format.nIndex++;
        }
        if (OK != err) {
            AVLOGV("returning error %d at %d", err, __LINE__);
            return err;
        }
        err = mOMX->setParameter(mNode, OMX_IndexParamAudioPortFormat,
                &format, sizeof(format));
        if (err != OK) {
            AVLOGV("returning error %d at %d", err, __LINE__);
            return err;
        }
        // port definition
        OMX_PARAM_PORTDEFINITIONTYPE def;
        InitOMXParams(&def);
        def.nPortIndex = kPortIndexOutput;
        def.format.audio.cMIMEType = (OMX_STRING)NULL;
        err = mOMX->getParameter(mNode, OMX_IndexParamPortDefinition,
                &def, sizeof(def));
        if (err != OK) {
            AVLOGV("returning error %d at %d", err, __LINE__);
            return err;
        }
        def.format.audio.bFlagErrorConcealment = OMX_TRUE;
        def.format.audio.eEncoding = OMX_AUDIO_CodingQCELP13;
        err = mOMX->setParameter(mNode, OMX_IndexParamPortDefinition,
                &def, sizeof(def));

        // profile
        OMX_AUDIO_PARAM_QCELP13TYPE profile;
        InitOMXParams(&profile);
        profile.nPortIndex = kPortIndexOutput;
        err = mOMX->getParameter(mNode, OMX_IndexParamAudioQcelp13,
                &profile, sizeof(profile));
        if (err != OK) {
            AVLOGV("returning error %d at %d", err, __LINE__);
            return err;
        }
        profile.nChannels = numChannels;
        err = mOMX->setParameter(mNode, OMX_IndexParamAudioQcelp13,
                &profile, sizeof(profile));
    }
    return err;
}

status_t ExtendedACodec::setFLACDecoderFormat(
        int32_t numChannels, int32_t sampleRate, int32_t bitsPerSample,
        int32_t minBlkSize, int32_t maxBlkSize,
        int32_t minFrmSize, int32_t maxFrmSize) {

    QOMX_AUDIO_PARAM_FLAC_DEC_TYPE profileFLACDec;
    OMX_PARAM_PORTDEFINITIONTYPE portParam;
    status_t err;

    AVLOGV("FLACDec setformat sampleRate:%d numChannels:%d, bitsPerSample:%d",
            sampleRate, numChannels, bitsPerSample);

    //for input port
    InitOMXParams(&profileFLACDec);
    profileFLACDec.nPortIndex = kPortIndexInput;
    err = mOMX->getParameter(mNode, (OMX_INDEXTYPE)QOMX_IndexParamAudioFlacDec,
            &profileFLACDec, sizeof(profileFLACDec));
    if (err != OK) {
        AVLOGE("returning error %d for get QOMX_IndexParamAudioFlacDec", err);
        return err;
    }

    profileFLACDec.nSampleRate = sampleRate;
    profileFLACDec.nChannels = numChannels;
    profileFLACDec.nBitsPerSample = bitsPerSample;
    profileFLACDec.nMinBlkSize = minBlkSize;
    profileFLACDec.nMaxBlkSize = maxBlkSize;
    profileFLACDec.nMinFrmSize = minFrmSize;
    profileFLACDec.nMaxFrmSize = maxFrmSize;
    err = mOMX->setParameter(mNode, (OMX_INDEXTYPE)QOMX_IndexParamAudioFlacDec,
            &profileFLACDec, sizeof(profileFLACDec));
    if (err != OK) {
        AVLOGE("returning error %d for set OMX_IndexParamAudioFlacDec", err);
        return err;
    }

    //for output port
    OMX_AUDIO_PARAM_PCMMODETYPE profilePcm;
    InitOMXParams(&profilePcm);
    profilePcm.nPortIndex = kPortIndexOutput;
    err = mOMX->getParameter(
            mNode, OMX_IndexParamAudioPcm, &profilePcm, sizeof(profilePcm));
    if (err != OK) {
        AVLOGE("returning error %d for OMX_IndexParamAudioPcm", err);
        return err;
    }

    profilePcm.nSamplingRate = sampleRate;
    profilePcm.nChannels = numChannels;
    profilePcm.nBitPerSample = bitsPerSample;
    err = mOMX->setParameter(
            mNode, OMX_IndexParamAudioPcm, &profilePcm, sizeof(profilePcm));

    return err;
}

status_t ExtendedACodec::setEVRCFormat(
        int32_t numChannels, int32_t /*sampleRate*/) {
    AVLOGD("setEVRCFormat");
    status_t err = OMX_ErrorNone;
    if (mIsEncoder) {
        CHECK(numChannels == 1);
        //////////////// input port ////////////////////
        //handle->setRawAudioFormat(kPortIndexInput, sampleRate, numChannels);
        //////////////// output port ////////////////////
        // format
        OMX_AUDIO_PARAM_PORTFORMATTYPE format;
        InitOMXParams(&format);
        format.nPortIndex = kPortIndexOutput;
        format.nIndex = 0;
        while (OMX_ErrorNone == err) {
            CHECK_EQ(mOMX->getParameter(mNode, OMX_IndexParamAudioPortFormat,
                    &format, sizeof(format)), (status_t)OK);
            if (format.eEncoding == OMX_AUDIO_CodingEVRC) {
                break;
            }
            format.nIndex++;
        }
        if (OK != err) {
            AVLOGV("returning error %d at %d", err, __LINE__);
            return err;
        }
        err = mOMX->setParameter(mNode, OMX_IndexParamAudioPortFormat,
                &format, sizeof(format));
        if (OK != err) {
            AVLOGV("returning error %d at %d", err, __LINE__);
            return err;
        }

        // port definition
        OMX_PARAM_PORTDEFINITIONTYPE def;
        InitOMXParams(&def);
        def.nPortIndex = kPortIndexOutput;
        def.format.audio.cMIMEType = (OMX_STRING)NULL;
        err = mOMX->getParameter(mNode, OMX_IndexParamPortDefinition,
                &def, sizeof(def));
        if (OK != err) {
            AVLOGV("returning error %d at %d", err, __LINE__);
            return err;
        }
        def.format.audio.bFlagErrorConcealment = OMX_TRUE;
        def.format.audio.eEncoding = OMX_AUDIO_CodingEVRC;
        err = mOMX->setParameter(mNode, OMX_IndexParamPortDefinition,
                &def, sizeof(def));
        if (OK != err) {
            AVLOGV("returning error %d at %d", err, __LINE__);
            return err;
        }
        // profile
        OMX_AUDIO_PARAM_EVRCTYPE profile;
        InitOMXParams(&profile);
        profile.nPortIndex = kPortIndexOutput;
        err = mOMX->getParameter(mNode, OMX_IndexParamAudioEvrc,
                &profile, sizeof(profile));
        if (OK != err) {
            AVLOGV("returning error %d at %d", err, __LINE__);
            return err;
        }
        profile.nChannels = numChannels;
        err = mOMX->setParameter(mNode, OMX_IndexParamAudioEvrc,
                &profile, sizeof(profile));

    } else {
        AVLOGD("EVRC decoder");
    }
    return err;
}

status_t ExtendedACodec::setAMRWBPLUSFormat(
        int32_t numChannels, int32_t sampleRate) {

    QOMX_AUDIO_PARAM_AMRWBPLUSTYPE profileAMRWBPlus;
    OMX_INDEXTYPE indexTypeAMRWBPlus;
    OMX_PARAM_PORTDEFINITIONTYPE portParam;

    AVLOGV("AMRWB+ setformat sampleRate:%d numChannels:%d",sampleRate,numChannels);

    //configure input port
    InitOMXParams(&portParam);
    portParam.nPortIndex = kPortIndexInput;
    status_t err = mOMX->getParameter(
            mNode, OMX_IndexParamPortDefinition, &portParam, sizeof(portParam));
    CHECK_EQ(err, (status_t)OK);
    err = mOMX->setParameter(
            mNode, OMX_IndexParamPortDefinition, &portParam, sizeof(portParam));
    CHECK_EQ(err, (status_t)OK);

    //configure output port
    portParam.nPortIndex = kPortIndexOutput;
    err = mOMX->getParameter(
            mNode, OMX_IndexParamPortDefinition, &portParam, sizeof(portParam));
    CHECK_EQ(err, (status_t)OK);
    err = mOMX->setParameter(
            mNode, OMX_IndexParamPortDefinition, &portParam, sizeof(portParam));
    CHECK_EQ(err, (status_t)OK);

    err = mOMX->getExtensionIndex(mNode, OMX_QCOM_INDEX_PARAM_AMRWBPLUS, &indexTypeAMRWBPlus);
    CHECK_EQ(err, (status_t)OK);

    //for input port
    InitOMXParams(&profileAMRWBPlus);
    profileAMRWBPlus.nPortIndex = kPortIndexInput;
    err = mOMX->getParameter(mNode, indexTypeAMRWBPlus, &profileAMRWBPlus, sizeof(profileAMRWBPlus));
    CHECK_EQ(err,(status_t)OK);

    profileAMRWBPlus.nSampleRate = sampleRate;
    profileAMRWBPlus.nChannels = numChannels;
    err = mOMX->setParameter(mNode, indexTypeAMRWBPlus, &profileAMRWBPlus, sizeof(profileAMRWBPlus));
    CHECK_EQ(err,(status_t)OK);

    //for output port
    OMX_AUDIO_PARAM_PCMMODETYPE profilePcm;
    InitOMXParams(&profilePcm);
    profilePcm.nPortIndex = kPortIndexOutput;
    err = mOMX->getParameter(
            mNode, OMX_IndexParamAudioPcm, &profilePcm, sizeof(profilePcm));
    CHECK_EQ(err, (status_t)OK);

    profilePcm.nSamplingRate = sampleRate;
    profilePcm.nChannels = numChannels;
    err = mOMX->setParameter(
            mNode, OMX_IndexParamAudioPcm, &profilePcm, sizeof(profilePcm));
    CHECK_EQ(err, (status_t)OK);
    AVLOGE("returning error %d", err);
    return err;
}

status_t ExtendedACodec::setWMAFormat(
        const sp<AMessage> &msg) {
    AVLOGV("setWMAFormat Called");

    if (mIsEncoder) {
        AVLOGE("WMA encoding not supported");
        return OK;
    } else {
        int32_t version;
        OMX_AUDIO_PARAM_WMATYPE paramWMA;
        QOMX_AUDIO_PARAM_WMA10PROTYPE paramWMA10;
        CHECK(msg->findInt32(ExtendedUtils::getMsgFromKey(kKeyWMAVersion), &version));
        int32_t numChannels;
        int32_t bitRate;
        int32_t sampleRate;
        int32_t encodeOptions;
        int32_t blockAlign;
        int32_t bitspersample;
        int32_t formattag;
        int32_t advencopt1;
        int32_t advencopt2;
        int32_t VirtualPktSize;
        if (version==kTypeWMAPro || version==kTypeWMALossLess) {
            CHECK(msg->findInt32(ExtendedUtils::getMsgFromKey(kKeyWMABitspersample), &bitspersample));
            CHECK(msg->findInt32(ExtendedUtils::getMsgFromKey(kKeyWMAFormatTag), &formattag));
            CHECK(msg->findInt32(ExtendedUtils::getMsgFromKey(kKeyWMAAdvEncOpt1), &advencopt1));
            CHECK(msg->findInt32(ExtendedUtils::getMsgFromKey(kKeyWMAAdvEncOpt2), &advencopt2));
            CHECK(msg->findInt32(ExtendedUtils::getMsgFromKey(kKeyWMAVirPktSize), &VirtualPktSize));
        }
        if (version==kTypeWMA) {
            InitOMXParams(&paramWMA);
            paramWMA.nPortIndex = kPortIndexInput;
        } else if (version==kTypeWMAPro || version==kTypeWMALossLess) {
            InitOMXParams(&paramWMA10);
            paramWMA10.nPortIndex = kPortIndexInput;
        }
        CHECK(msg->findInt32("channel-count", &numChannels));
        CHECK(msg->findInt32("sample-rate", &sampleRate));
        CHECK(msg->findInt32(ExtendedUtils::getMsgFromKey(kKeyBitRate), &bitRate));
        CHECK(msg->findInt32(ExtendedUtils::getMsgFromKey(kKeyWMAEncodeOpt), &encodeOptions));
        CHECK(msg->findInt32(ExtendedUtils::getMsgFromKey(kKeyWMABlockAlign), &blockAlign));
        AVLOGV("Channels: %d, SampleRate: %d, BitRate; %d"
                "EncodeOptions: %d, blockAlign: %d", numChannels,
                sampleRate, bitRate, encodeOptions, blockAlign);
        if (sampleRate > 48000 && numChannels > 2) {
            AVLOGE("Unsupported samplerate/channels");
            return ERROR_UNSUPPORTED;
        }
        if (version==kTypeWMAPro || version==kTypeWMALossLess) {
            AVLOGV("Bitspersample: %d, wmaformattag: %d,"
                    "advencopt1: %d, advencopt2: %d VirtualPktSize %d", bitspersample,
                    formattag, advencopt1, advencopt2, VirtualPktSize);
        }
        status_t err = OK;
        OMX_INDEXTYPE index;
        if (version==kTypeWMA) {
            err = mOMX->getParameter(
                    mNode, OMX_IndexParamAudioWma, &paramWMA, sizeof(paramWMA));
        } else if (version==kTypeWMAPro || version==kTypeWMALossLess) {
            mOMX->getExtensionIndex(mNode,"OMX.Qualcomm.index.audio.wma10Pro",&index);
            err = mOMX->getParameter(
                    mNode, index, &paramWMA10, sizeof(paramWMA10));
        }
        if (err != OK) {
            return err;
        }
        if (version==kTypeWMA) {
            paramWMA.nChannels = numChannels;
            paramWMA.nSamplingRate = sampleRate;
            paramWMA.nEncodeOptions = encodeOptions;
            paramWMA.nBitRate = bitRate;
            paramWMA.nBlockAlign = blockAlign;
        } else if (version==kTypeWMAPro || version==kTypeWMALossLess) {
            paramWMA10.nChannels = numChannels;
            paramWMA10.nSamplingRate = sampleRate;
            paramWMA10.nEncodeOptions = encodeOptions;
            paramWMA10.nBitRate = bitRate;
            paramWMA10.nBlockAlign = blockAlign;
        }
        if (version==kTypeWMAPro || version==kTypeWMALossLess) {
            paramWMA10.advancedEncodeOpt = advencopt1;
            paramWMA10.advancedEncodeOpt2 = advencopt2;
            paramWMA10.formatTag = formattag;
            paramWMA10.validBitsPerSample = bitspersample;
            paramWMA10.nVirtualPktSize = VirtualPktSize;
        }
        if (version==kTypeWMA) {
            err = mOMX->setParameter(
                    mNode, OMX_IndexParamAudioWma, &paramWMA, sizeof(paramWMA));
        } else if (version==kTypeWMAPro || version==kTypeWMALossLess) {
            err = mOMX->setParameter(
                    mNode, index, &paramWMA10, sizeof(paramWMA10));
        }
        return err;
    }
    return OK;
}

status_t ExtendedACodec::setALACFormat(
        int32_t numChannels, int32_t sampleRate, int32_t bitsPerSample) {

    QOMX_AUDIO_PARAM_ALACTYPE paramALAC;
    OMX_PARAM_PORTDEFINITIONTYPE portParam;
    OMX_INDEXTYPE indexTypeALAC;
    status_t err = OK;

    AVLOGV("setALACFormat sampleRate:%d numChannels:%d bitsPerSample: %d",
            sampleRate, numChannels, bitsPerSample);

    err = mOMX->getExtensionIndex(mNode, OMX_QCOM_INDEX_PARAM_ALAC, &indexTypeALAC);
    if (err != OK) {
        return err;
    }

    //for input port
    InitOMXParams(&paramALAC);
    paramALAC.nPortIndex = kPortIndexInput;
    err = mOMX->getParameter(mNode, indexTypeALAC, &paramALAC, sizeof(paramALAC));
    if (err != OK) {
        return err;
    }

    paramALAC.nSampleRate = sampleRate;
    paramALAC.nChannels = numChannels;
    paramALAC.nBitDepth = bitsPerSample;
    err = mOMX->setParameter(mNode, indexTypeALAC, &paramALAC, sizeof(paramALAC));
    if (err != OK) {
        return err;
    }

    //for output port
    OMX_AUDIO_PARAM_PCMMODETYPE profilePcm;
    InitOMXParams(&profilePcm);
    profilePcm.nPortIndex = kPortIndexOutput;
    err = mOMX->getParameter(
           mNode, OMX_IndexParamAudioPcm, &profilePcm, sizeof(profilePcm));
    if (err != OK) {
        return err;
    }

    profilePcm.nSamplingRate = sampleRate;
    profilePcm.nChannels = numChannels;
    profilePcm.nBitPerSample = bitsPerSample;
    err = mOMX->setParameter(
           mNode, OMX_IndexParamAudioPcm, &profilePcm, sizeof(profilePcm));

    return err;
}
status_t ExtendedACodec::setAPEFormat(
        int32_t numChannels, int32_t sampleRate, int32_t bitsPerSample) {

    QOMX_AUDIO_PARAM_APETYPE paramAPE;
    OMX_PARAM_PORTDEFINITIONTYPE portParam;
    OMX_INDEXTYPE indexTypeAPE;
    status_t err = OK;

    AVLOGV("setAPEFormat sampleRate:%d numChannels:%d bitsPerSample:%d",
            sampleRate, numChannels, bitsPerSample);

    err = mOMX->getExtensionIndex(mNode, OMX_QCOM_INDEX_PARAM_APE, &indexTypeAPE);
    if (err != OK) {
        return err;
    }

    //for input port
    InitOMXParams(&paramAPE);
    paramAPE.nPortIndex = kPortIndexInput;
    err = mOMX->getParameter(mNode, indexTypeAPE, &paramAPE, sizeof(paramAPE));
    if (err != OK) {
        return err;
    }

    paramAPE.nSampleRate = sampleRate;
    paramAPE.nChannels = numChannels;
    paramAPE.nBitsPerSample = bitsPerSample;
    err = mOMX->setParameter(mNode, indexTypeAPE, &paramAPE, sizeof(paramAPE));
    if (err != OK) {
        return err;
    }

    //for output port
    OMX_AUDIO_PARAM_PCMMODETYPE profilePcm;
    InitOMXParams(&profilePcm);
    profilePcm.nPortIndex = kPortIndexOutput;
    err = mOMX->getParameter(
           mNode, OMX_IndexParamAudioPcm, &profilePcm, sizeof(profilePcm));
    if (err != OK) {
        return err;
    }

    profilePcm.nSamplingRate = sampleRate;
    profilePcm.nChannels = numChannels;
    profilePcm.nBitPerSample = bitsPerSample;
    err = mOMX->setParameter(
           mNode, OMX_IndexParamAudioPcm, &profilePcm, sizeof(profilePcm));

    return err;
}

status_t ExtendedACodec::setDSDFormat(
        int32_t numChannels, int32_t sampleRate, int32_t bitsPerSample) {

    QOMX_AUDIO_PARAM_DSD_TYPE paramDSD;
    OMX_PARAM_PORTDEFINITIONTYPE portParam;
    OMX_INDEXTYPE indexTypeDSD;
    status_t err = OK;

    AVLOGV("setDSDFormat sampleRate:%d numChannels:%d bitsPerSample:%d",
            sampleRate, numChannels, bitsPerSample);

    err = mOMX->getExtensionIndex(mNode, OMX_QCOM_INDEX_PARAM_DSD, &indexTypeDSD);
    if (err != OK) {
        return err;
    }

    //for output port
    OMX_AUDIO_PARAM_PCMMODETYPE profilePcm;
    InitOMXParams(&profilePcm);
    profilePcm.nPortIndex = kPortIndexOutput;
    err = mOMX->getParameter(
           mNode, OMX_IndexParamAudioPcm, &profilePcm, sizeof(profilePcm));
    if (err != OK) {
        return err;
    }

    profilePcm.nSamplingRate = sampleRate;
    profilePcm.nChannels = numChannels;
    profilePcm.nBitPerSample = 24;
    err = mOMX->setParameter(
           mNode, OMX_IndexParamAudioPcm, &profilePcm, sizeof(profilePcm));
    if (err != OK) {
        return err;
    }

    //for input port
    InitOMXParams(&paramDSD);
    paramDSD.nPortIndex = kPortIndexInput;
    err = mOMX->getParameter(mNode, indexTypeDSD, &paramDSD, sizeof(paramDSD));
    if (err != OK) {
        return err;
    }

    paramDSD.nSampleRate = sampleRate;
    paramDSD.nChannels = numChannels;
    paramDSD.nBitsPerSample = bitsPerSample;
    err = mOMX->setParameter(mNode, indexTypeDSD, &paramDSD, sizeof(paramDSD));
    if (err != OK) {
        return err;
    }
    return err;
}

status_t ExtendedACodec::GetVideoCodingTypeFromMime(
        const char *mime, OMX_VIDEO_CODINGTYPE *codingType) {
    if (ACodec::GetVideoCodingTypeFromMime(mime, codingType) == OK) {
        return OK;
    }

    status_t retVal = OK;
    if (!strcasecmp(MEDIA_MIMETYPE_VIDEO_DIVX, mime)) {
        *codingType = (OMX_VIDEO_CODINGTYPE)QOMX_VIDEO_CodingDivx;
    } else if (!strcasecmp(MEDIA_MIMETYPE_VIDEO_DIVX4, mime)) {
        *codingType = (OMX_VIDEO_CODINGTYPE)QOMX_VIDEO_CodingDivx;
    } else if (!strcasecmp(MEDIA_MIMETYPE_VIDEO_DIVX311, mime)) {
        *codingType = (OMX_VIDEO_CODINGTYPE)QOMX_VIDEO_CodingDivx;
    } else if (!strcasecmp(MEDIA_MIMETYPE_VIDEO_WMV, mime)) {
        *codingType = OMX_VIDEO_CodingWMV;
    } else if (!strcasecmp(MEDIA_MIMETYPE_VIDEO_WMV_VC1, mime)) {
        *codingType = OMX_VIDEO_CodingWMV;
    } else if (!strcasecmp(MEDIA_MIMETYPE_CONTAINER_MPEG2, mime)) {
        *codingType = OMX_VIDEO_CodingMPEG2;
    } else {
        retVal = ERROR_UNSUPPORTED;
    }

    return retVal;
}

status_t ExtendedACodec::getPortFormat(OMX_U32 portIndex, sp<AMessage> &notify) {
    AVLOGD("getPortFormat()");
    OMX_PARAM_PORTDEFINITIONTYPE def;
    InitOMXParams(&def);
    def.nPortIndex = portIndex;

    status_t err = mOMX->getParameter(mNode, OMX_IndexParamPortDefinition, &def, sizeof(def));
    if (err != OK) {
        ALOGE("Failed to getParameter for port %d", portIndex);
        return err;
    }

    if (def.eDir != (portIndex == kPortIndexOutput ? OMX_DirOutput : OMX_DirInput)) {
        ALOGE("Bad eDir in getParameter for port %d", portIndex);
        return BAD_VALUE;;
    }

    if (def.eDomain == OMX_PortDomainAudio) {

        OMX_AUDIO_PORTDEFINITIONTYPE *audioDef = &def.format.audio;

        switch ((int)audioDef->eEncoding) {
            case OMX_AUDIO_CodingQCELP13:
            {
                OMX_AUDIO_PARAM_QCELP13TYPE params;
                InitOMXParams(&params);
                params.nPortIndex = portIndex;

                CHECK_EQ(mOMX->getParameter(
                        mNode, OMX_IndexParamAudioQcelp13,
                        &params, sizeof(params)),
                        (status_t)OK);

                notify->setString("mime", MEDIA_MIMETYPE_AUDIO_QCELP);
                notify->setInt32("channel-count", params.nChannels);
                /* QCELP supports only 8k sample rate*/
                notify->setInt32("sample-rate", 8000);
                return OK;
            }
            case OMX_AUDIO_CodingEVRC:
            {
                OMX_AUDIO_PARAM_EVRCTYPE params;
                InitOMXParams(&params);
                params.nPortIndex = portIndex;
                CHECK_EQ(mOMX->getParameter(
                        mNode, OMX_IndexParamAudioEvrc,
                        &params, sizeof(params)),
                        (status_t)OK);
                notify->setString("mime", MEDIA_MIMETYPE_AUDIO_EVRC);
                notify->setInt32("channel-count", params.nChannels);
                /* EVRC supports only 8k sample rate*/
                notify->setInt32("sample-rate", 8000);
                return OK;
            }
            case QOMX_IndexParamAudioAmrWbPlus:
            {
                OMX_INDEXTYPE index;
                QOMX_AUDIO_PARAM_AMRWBPLUSTYPE params;
                InitOMXParams(&params);
                AVLOGD("AMRWB format");
                params.nPortIndex = portIndex;
                mOMX->getExtensionIndex(mNode, OMX_QCOM_INDEX_PARAM_AMRWBPLUS, &index);
                CHECK_EQ(mOMX->getParameter(mNode, index, &params, sizeof(params)),(status_t)OK);
                notify->setString("mime", MEDIA_MIMETYPE_AUDIO_AMR_WB_PLUS);
                notify->setInt32("channel-count", params.nChannels);
                notify->setInt32("sample-rate",  params.nSampleRate);
                return OK;
            }
          case OMX_AUDIO_CodingWMA:
          {
              status_t err = OK;
              OMX_INDEXTYPE index;
              OMX_AUDIO_PARAM_WMATYPE paramWMA;
              QOMX_AUDIO_PARAM_WMA10PROTYPE paramWMA10;

              InitOMXParams(&paramWMA);
              paramWMA.nPortIndex = portIndex;
              err = mOMX->getParameter(
                      mNode, OMX_IndexParamAudioWma, &paramWMA, sizeof(paramWMA));
              if(err == OK) {
                  AVLOGV("WMA format");
                  notify->setString("mime",MEDIA_MIMETYPE_AUDIO_WMA);
                  notify->setInt32("channel-count", paramWMA.nChannels);
                  notify->setInt32("sample-rate", paramWMA.nSamplingRate);
              } else {
                  InitOMXParams(&paramWMA10);
                  paramWMA10.nPortIndex = portIndex;
                  mOMX->getExtensionIndex(mNode,"OMX.Qualcomm.index.audio.wma10Pro",&index);
                  CHECK_EQ(mOMX->getParameter(mNode, index, &paramWMA10, sizeof(paramWMA10)),(status_t)OK);
                  AVLOGV("WMA10 format");
                  notify->setString("mime",MEDIA_MIMETYPE_AUDIO_WMA);
                  notify->setInt32("channel-count", paramWMA10.nChannels);
                  notify->setInt32("sample-rate", paramWMA10.nSamplingRate);
              }
              return OK;
          }
          case QOMX_IndexParamAudioAlac:
          {
              OMX_INDEXTYPE index;
              QOMX_AUDIO_PARAM_ALACTYPE params;

              InitOMXParams(&params);
              params.nPortIndex = portIndex;
              mOMX->getExtensionIndex(mNode, OMX_QCOM_INDEX_PARAM_ALAC, &index);
              CHECK_EQ(mOMX->getParameter(mNode, index, &params, sizeof(params)),(status_t)OK);
              notify->setString("mime", MEDIA_MIMETYPE_AUDIO_ALAC);
              notify->setInt32("channel-count", params.nChannels);
              notify->setInt32("sample-rate", params.nSampleRate);
              return OK;
          }

          case QOMX_IndexParamAudioApe:
          {
              OMX_INDEXTYPE index;
              QOMX_AUDIO_PARAM_APETYPE params;
              status_t status = OK;

              InitOMXParams(&params);
              params.nPortIndex = portIndex;
              mOMX->getExtensionIndex(mNode, OMX_QCOM_INDEX_PARAM_APE, &index);
              status = mOMX->getParameter(mNode, index, &params, sizeof(params));
              if (status != OK) {
                  return status;
              }
              notify->setString("mime", MEDIA_MIMETYPE_AUDIO_APE);
              notify->setInt32("channel-count", params.nChannels);
              notify->setInt32("sample-rate",  params.nSampleRate);
              return OK;
          }
          case QOMX_IndexParamAudioDsdDec:
          {
              OMX_INDEXTYPE index;
              QOMX_AUDIO_PARAM_DSD_TYPE params;
              status_t status = OK;

              InitOMXParams(&params);
              params.nPortIndex = portIndex;
              mOMX->getExtensionIndex(mNode, OMX_QCOM_INDEX_PARAM_DSD, &index);
              status = mOMX->getParameter(mNode, index, &params, sizeof(params));
              if (status != OK) {
                  return status;
              }
              notify->setString("mime", MEDIA_MIMETYPE_AUDIO_DSD);
              notify->setInt32("channel-count", params.nChannels);
              notify->setInt32("sample-rate",  params.nSampleRate);
              return OK;
          }
          default:
              // Let ACodec handle the rest
              break;
        }
    }

    return ACodec::getPortFormat(portIndex, notify);
}

bool ExtendedACodec::canAllocateBuffer(OMX_U32 portIndex) {
    uint32_t requiresAllocateBufferBit =
        (portIndex == kPortIndexInput)
            ? ACodec::kRequiresAllocateBufferOnInputPorts
            : ACodec::kRequiresAllocateBufferOnOutputPorts;
    return ((mQuirks & requiresAllocateBufferBit)
            && mOMX->livesLocally(mNode, getpid()));
}

void ExtendedACodec::setBFrames(OMX_VIDEO_PARAM_MPEG4TYPE *mpeg4type) {
    //ignore non QC components
    if (strncmp(mComponentName.c_str(), "OMX.qcom.", 9) || mpeg4type == NULL) {
        return;
    }
    if (mpeg4type->eProfile > OMX_VIDEO_MPEG4ProfileSimple) {
        mpeg4type->nAllowedPictureTypes |= OMX_VIDEO_PictureTypeB;
        mpeg4type->nPFrames = (mpeg4type->nPFrames + kNumBFramesPerPFrame) /
                (kNumBFramesPerPFrame + 1);
        mpeg4type->nBFrames = mpeg4type->nPFrames * kNumBFramesPerPFrame;
    }
    return;
}

void ExtendedACodec::setBFrames(OMX_VIDEO_PARAM_AVCTYPE *h264type,
        const int32_t iFramesInterval, const int32_t frameRate) {
    //ignore non QC components
    if (strncmp(mComponentName.c_str(), "OMX.qcom.", 9) || h264type == NULL) {
        return;
    }

    OMX_U32 val = 0;
    if (iFramesInterval < 0) {
        val =  INT_MAX;
    } else if (iFramesInterval == 0 ||
        (iFramesInterval * frameRate == 1)) {
        val = 0;
    } else {
        val  = frameRate * iFramesInterval - 1;
        CHECK_GT(val, 1);
    }

    h264type->nPFrames = val;

    if (h264type->nPFrames == 0) {
        h264type->nAllowedPictureTypes = OMX_VIDEO_PictureTypeI;
    }

    if (h264type->eProfile > OMX_VIDEO_AVCProfileBaseline) {
        h264type->nAllowedPictureTypes |= OMX_VIDEO_PictureTypeB;
        h264type->nPFrames = (h264type->nPFrames + kNumBFramesPerPFrame) /
                (kNumBFramesPerPFrame + 1);
        h264type->nBFrames = h264type->nPFrames * kNumBFramesPerPFrame;

        //enable CABAC as default entropy mode for High/Main profiles
        h264type->bEntropyCodingCABAC = OMX_TRUE;
        h264type->nCabacInitIdc = 0;
    }
    return;
}

status_t ExtendedACodec::setupErrorCorrectionParameters() {
    OMX_VIDEO_PARAM_ERRORCORRECTIONTYPE errorCorrectionType;
    InitOMXParams(&errorCorrectionType);
    errorCorrectionType.nPortIndex = kPortIndexOutput;

    status_t err = mOMX->getParameter(
            mNode, OMX_IndexParamVideoErrorCorrection,
            &errorCorrectionType, sizeof(errorCorrectionType));

    if (err != OK) {
        return OK;  // Optional feature. Ignore this failure
    }

    errorCorrectionType.bEnableHEC = OMX_FALSE;
    errorCorrectionType.bEnableResync = OMX_FALSE;
    errorCorrectionType.nResynchMarkerSpacing = 0;
    errorCorrectionType.bEnableDataPartitioning = OMX_FALSE;
    errorCorrectionType.bEnableRVLC = OMX_FALSE;

    return mOMX->setParameter(
            mNode, OMX_IndexParamVideoErrorCorrection,
            &errorCorrectionType, sizeof(errorCorrectionType));
}

status_t ExtendedACodec::configureFramePackingFormat(
        const sp<AMessage> &msg) {
    status_t err = OK;
    int32_t mode = 0;
    OMX_QCOM_PARAM_PORTDEFINITIONTYPE portFmt;
    InitOMXParams(&portFmt);
    portFmt.nPortIndex = kPortIndexInput;

    if (msg->findInt32("use-arbitrary-mode", &mode) && mode) {
        AVLOGI("Decoder will be in arbitrary mode");
        portFmt.nFramePackingFormat = OMX_QCOM_FramePacking_Arbitrary;
    } else {
        AVLOGI("Decoder will be in frame by frame mode");
        portFmt.nFramePackingFormat = OMX_QCOM_FramePacking_OnlyOneCompleteFrame;
    }
    err = mOMX->setParameter(
            mNode, (OMX_INDEXTYPE)OMX_QcomIndexPortDefn,
            (void *)&portFmt, sizeof(portFmt));
    if (err != OK) {
        AVLOGW("Failed to set frame packing format on component");
    }
    return err;
}

status_t ExtendedACodec::setDIVXFormat(
        const sp<AMessage> &msg, const char* mime) {
    status_t err = OK;

    if (!strcasecmp(MEDIA_MIMETYPE_VIDEO_DIVX, mime) ||
            !strcasecmp(MEDIA_MIMETYPE_VIDEO_DIVX4, mime) ||
            !strcasecmp(MEDIA_MIMETYPE_VIDEO_DIVX311, mime)) {
        AVLOGD("Setting QOMX_VIDEO_PARAM_DIVXTYPE params");
        QOMX_VIDEO_PARAM_DIVXTYPE paramDivX;
        InitOMXParams(&paramDivX);
        paramDivX.nPortIndex = kPortIndexOutput;
        int32_t DivxVersion = 0;

        if (!msg->findInt32(ExtendedUtils::getMsgFromKey(kKeyDivXVersion), &DivxVersion)) {
            DivxVersion = kTypeDivXVer_4;
            AVLOGW("Divx version key missing, initializing the version to %d", DivxVersion);
        }

        AVLOGD("Divx Version Type %d", DivxVersion);

        if (DivxVersion == kTypeDivXVer_4) {
            paramDivX.eFormat = QOMX_VIDEO_DIVXFormat4;
        } else if (DivxVersion == kTypeDivXVer_5) {
            paramDivX.eFormat = QOMX_VIDEO_DIVXFormat5;
        } else if (DivxVersion == kTypeDivXVer_6) {
            paramDivX.eFormat = QOMX_VIDEO_DIVXFormat6;
        } else if (DivxVersion == kTypeDivXVer_3_11 ) {
            paramDivX.eFormat = QOMX_VIDEO_DIVXFormat311;
        } else {
            paramDivX.eFormat = QOMX_VIDEO_DIVXFormatUnused;
        }
        paramDivX.eProfile = (QOMX_VIDEO_DIVXPROFILETYPE)0;    //Not used for now.

        err = mOMX->setParameter(mNode,
                (OMX_INDEXTYPE)OMX_QcomIndexParamVideoDivx,
                &paramDivX, sizeof(paramDivX));
    }

    return err;
}

status_t ExtendedACodec::setupVQZIP(const sp<AMessage> &msg) {

    (void)msg;

    int32_t vqzipEnabled = 0;
    if (msg->findInt32("vqzip", (int32_t *)&vqzipEnabled) && vqzipEnabled) {
        OMX_VIDEO_PARAM_PROFILELEVELTYPE profileLevel;
        QOMX_VIDEO_H264ENTROPYCODINGTYPE entropy;
        OMX_QTI_VIDEO_PARAM_VQZIP_SEI_TYPE enableType;
        int32_t cabac;
        InitOMXParams(&enableType);
        InitOMXParams(&profileLevel);
        InitOMXParams(&entropy);

        enableType.bEnable = OMX_TRUE;
        profileLevel.nPortIndex = kPortIndexOutput;
        status_t err = mOMX->setParameter(
                mNode, (OMX_INDEXTYPE)OMX_QTIIndexParamVQZIPSEIType,
                (void *)&enableType, sizeof(enableType));
        if (err != OK) {
            AVLOGW("Failed to enable SEI Type");
        }

        msg->findInt32("vqzip-profile", (int32_t *)&profileLevel.eProfile);
        msg->findInt32("vqzip-level", (int32_t *)&profileLevel.eLevel);

        err = mOMX->setParameter(
                mNode, (OMX_INDEXTYPE)OMX_IndexParamVideoProfileLevelCurrent,
                &profileLevel, sizeof(profileLevel));
        if (err != OK) {
            AVLOGW("Failed to configure Profile and Level to Encoder");
        }

        msg->findInt32("vqzip-entropy", (int32_t *)&entropy.bCabac);

        err = mOMX->setParameter(
                mNode, (OMX_INDEXTYPE)OMX_QcomIndexConfigH264EntropyCodingCabac,
                &entropy, sizeof(entropy));
        if (err != OK) {
            AVLOGW("Failed to configure Entropy to Encoder");
        }
    }

    return OK;
}

status_t ExtendedACodec::setVppParameters(
        const sp<AMessage> &msg) {

    status_t err = OK;
    bool paramsSet = false;
    QOMX_VPP_HQVCONTROL hQvCtrl;
    InitOMXParams(&hQvCtrl);
    int32_t value = 0;

    paramsSet = msg->findInt32("vpp-mode", (int32_t *)&value);
    hQvCtrl.mode = (paramsSet) ? (QOMX_VPP_HQV_MODE)value : hQvCtrl.mode;

    paramsSet &= msg->findInt32("vpp-ctrl-type", (int32_t *)&value);
    hQvCtrl.ctrl_type = (paramsSet) ? (QOMX_VPP_HQVCONTROLTYPE)value : hQvCtrl.ctrl_type;

    if (hQvCtrl.ctrl_type == VPP_HQV_CONTROL_GLOBAL_DEMO) {
        paramsSet &= msg->findInt32("vpp-demo-percent", (int32_t *)&value);
        hQvCtrl.global_demo.process_percent = (paramsSet) ? value : hQvCtrl.global_demo.process_percent;
    } else if (hQvCtrl.ctrl_type == VPP_HQV_CONTROL_FRC) {
        paramsSet &= msg->findInt32("vpp-frc-mode", (int32_t *)&value);
        hQvCtrl.frc.mode = (paramsSet) ? (QOMX_VPP_HQV_FRC_MODE)value : hQvCtrl.frc.mode;
    } else if (hQvCtrl.ctrl_type == VPP_HQV_CONTROL_DI) {
        paramsSet &= msg->findInt32("vpp-di-mode", (int32_t *)&value);
        hQvCtrl.di.mode = (paramsSet) ? (QOMX_VPP_HQV_DI_MODE)value : hQvCtrl.di.mode;
    } else {
        paramsSet &= msg->findInt32("vpp-algo-mode", (int32_t *)&value);
        hQvCtrl.aie.mode = (paramsSet) ? (QOMX_VPP_HQV_MODE)value : hQvCtrl.aie.mode;
    }

    if (hQvCtrl.ctrl_type == VPP_HQV_CONTROL_AIE) {
        paramsSet &= msg->findInt32("vpp-hue-mode", (int32_t *)&value);
        hQvCtrl.aie.hue_mode = (paramsSet) ? (QOMX_VPP_HQV_HUE_MODE)value : hQvCtrl.aie.hue_mode;

        paramsSet &= msg->findInt32("vpp-algo-level", (int32_t *)&value);
        hQvCtrl.aie.cade_level = (paramsSet) ? value : hQvCtrl.aie.cade_level;

        paramsSet &= msg->findInt32("vpp-ltm-level", (int32_t *)&value);
        hQvCtrl.aie.ltm_level = (paramsSet) ? value : hQvCtrl.aie.ltm_level;

    } else if (hQvCtrl.ctrl_type == VPP_HQV_CONTROL_CADE) {
        paramsSet &= msg->findInt32("vpp-algo-level", (int32_t *)&value);
        hQvCtrl.cade.level = (paramsSet) ? value : hQvCtrl.cade.level;

        paramsSet &= msg->findInt32("vpp-cade-contrast", (int32_t *)&value);
        hQvCtrl.cade.contrast = (paramsSet) ? value : hQvCtrl.cade.contrast;

        paramsSet &= msg->findInt32("vpp-cade-saturation", (int32_t *)&value);
        hQvCtrl.cade.saturation = (paramsSet) ? value : hQvCtrl.cade.saturation;

    } else if (hQvCtrl.ctrl_type == VPP_HQV_CONTROL_CNR) {
        paramsSet &= msg->findInt32("vpp-algo-level", (int32_t *)&value);
        hQvCtrl.cnr.level = (paramsSet) ? value : hQvCtrl.cnr.level;
    }

    if (paramsSet) {
        err = mOMX->setParameter(mNode, (OMX_INDEXTYPE)OMX_QcomIndexParamVppHqvControl,
                (void *)&hQvCtrl, sizeof(hQvCtrl));
    }

    if (err != OK) {
        AVLOGW("Failed to set vpp control parameters");
    }

    return err;
}

status_t ExtendedACodec::configureVpp(
        const sp<AMessage> &msg){

    status_t err = OK;

    /* VPP Stuff */
    AVLOGV("Setting of vpp-enable");
    int32_t value = 0;
    bool paramsSet = false;
    paramsSet = msg->findInt32("vpp-enable", (int32_t *)&value);
    AVLOGI("vpp-enable search is %d and value is %d", paramsSet, value);

    if (paramsSet) {
        /*Pass the vpp-enable flag to the OMX component */
        QOMX_VPP_ENABLE vppEnableCtrl;
        InitOMXParams(&vppEnableCtrl);
        vppEnableCtrl.enable_vpp = (OMX_BOOL)value;
        err = mOMX->setParameter(mNode, (OMX_INDEXTYPE)OMX_QcomIndexParamEnableVpp,
                   (void *)&vppEnableCtrl, sizeof(vppEnableCtrl));

        if (err != OK) {
            AVLOGW("Failed to set vpp-enable flag");
            return err;
        }
    }

    err = parseVppConfig(msg, "vpp-config");

    return err;
  }

status_t ExtendedACodec::parseVppConfig(
        const sp<AMessage> &msg, const char* key) {

    bool paramsSet = false;
    status_t err = OK;
    /* VPP Algo*/
    sp<ABuffer> cfgBuf;

    if (!key) {
        return BAD_VALUE;
    }

    paramsSet = msg->findBuffer(key, &cfgBuf);
    AVLOGV("%s search is %d ", key, paramsSet);

    if (paramsSet && ( cfgBuf != NULL ) && ( cfgBuf->size() > 0) ) {
        AVLOGV("%s buffer size is %zu", key, cfgBuf->size());
        uint8_t* vppStructBase = cfgBuf->data();
        APP_VPP_HQVCONTROL *appCtrl = NULL;
        size_t totalSize = 0;
        if (!vppStructBase) {
            AVLOGE("vpp setting parameter ptr is NULL");
            return BAD_VALUE;
        }

        int32_t num_algo = *((int32_t*)vppStructBase);
        if (num_algo <= 0) {
            AVLOGW("Unexpected setting of number of algorithms");
            return BAD_VALUE;
        }
        totalSize = num_algo * sizeof(APP_VPP_HQVCONTROL) + sizeof(int32_t);

        QOMX_VPP_HQVCONTROL hQvCtrl;
        InitOMXParams(&hQvCtrl);

        vppStructBase += sizeof(int32_t);
        appCtrl = (APP_VPP_HQVCONTROL *)vppStructBase;

        if (cfgBuf->size() != totalSize) {
            AVLOGW("Size of the buffer doesn't meet expected.Failed to set vpp-config params");
            return NO_MEMORY;
        }

        for (int i = 0; i < num_algo; i++) {
            hQvCtrl.mode = appCtrl->mode;
            hQvCtrl.ctrl_type= appCtrl->ctrl_type;
            hQvCtrl.custom = appCtrl->custom;
            appCtrl++;

            err = mOMX->setParameter(mNode, (OMX_INDEXTYPE)OMX_QcomIndexParamVppHqvControl,
                   (void *)&hQvCtrl, sizeof(hQvCtrl));

            if (err != OK) {
                AVLOGW("Failed to set %s params", key);
            }
        }
    }

    return err;
  }

status_t ExtendedACodec::setDSModeHint(
        sp<AMessage>& msg, OMX_U32 flags, int64_t timeUs) {

    if (strncmp(mComponentName.c_str(), "OMX.qcom.video.",
            strlen("OMX.qcom.video."))) {
        return INVALID_OPERATION;
    }

    if (msg == NULL) {
        return BAD_VALUE;
    }

    AVLOGV("setDSModeHint: timeUs = %"  PRId64, timeUs);
    msg->setInt32("cancel-buf", 0x0);
    msg->setInt64("timeUs", timeUs);
    if ((OMX_U32)(flags & QOMX_VIDEO_BUFFERFLAG_CANCEL) ==
           (OMX_U32)QOMX_VIDEO_BUFFERFLAG_CANCEL) {
        AVLOGV("setDSModeHint: CancelBuffer Set");
        msg->setInt32("cancel-buf", 0x1);
    }
    return OK;
}

bool ExtendedACodec::getDSModeHint(
        const sp<AMessage>& msg) {

    if (strncmp(mComponentName.c_str(), "OMX.qcom.video.",
            strlen("OMX.qcom.video.")) || msg == NULL) {
        return false;
    }

    int64_t       nTimeUs;
    int           cancelBuf = 0;
    OMX_INDEXTYPE nIndex;

    if (msg->findInt32("cancel-buf", (int32_t*)&cancelBuf) && cancelBuf
           && msg->findInt64("timeUs", (int64_t*)&nTimeUs)) {

        if( mOMX->getExtensionIndex (mNode,
                                     "OMX.QTI.index.config.video.settimedata",
                                     &nIndex) != OMX_ErrorNone) {
            AVLOGV("DSModeHint: settimedata not supported");
            return false;
        }


        status_t err = OK;

        int64_t timestampNs = 0;

        OMX_TIME_CONFIG_TIMESTAMPTYPE timeStampInfo;
        InitOMXParams(&timeStampInfo);

        if (msg->findInt64("timestampNs", &timestampNs)) {
            timeStampInfo.nTimestamp = timestampNs / 1000;
            timeStampInfo.nPortIndex = 0;
            AVLOGV("DSModeHint: nRealTime = %lld", timeStampInfo.nTimestamp);

            err = mOMX->setConfig(mNode,
                                  nIndex,
                                 (void*)&timeStampInfo,
                                  sizeof(timeStampInfo));
        }

        timeStampInfo.nTimestamp = nTimeUs;
        timeStampInfo.nPortIndex = 1;
        AVLOGV("DSModeHint: nTimeStamp = %lld", timeStampInfo.nTimestamp);

        err = mOMX->setConfig(mNode,
                              nIndex,
                             (void*)&timeStampInfo,
                              sizeof(timeStampInfo));

        if (err != OK) {
            AVLOGV("DSModeHint: Failed sendRenderingTime nTimeStamp = %lld",
                    timeStampInfo.nTimestamp);
        }
        return true;
    }
    return false;
}

}  // namespace android
