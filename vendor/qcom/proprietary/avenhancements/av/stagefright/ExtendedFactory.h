/*
 * Copyright (c) 2015-2016, Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
*/

#ifndef _EXTENDED_FACTORY_H_
#define _EXTENDED_FACTORY_H_

#include "mpeg2ts/ESQueue.h"

namespace android {

class MediaExtractor;
struct ExtendedFactory : public AVFactory {

    virtual sp<ACodec> createACodec();
    virtual MediaExtractor* createExtendedExtractor(
             const sp<DataSource> &source, const char *mime,
             const sp<AMessage> &msg);
#ifndef BRINGUP_WIP
    virtual sp<NuCachedSource2> createCachedSource(
            const sp<DataSource> &source,
            const char *cacheConfig,
            bool disconnectAtHighwatermark);
#endif
    virtual AudioSource* createAudioSource(
            audio_source_t inputSource,
            const String16 &opPackageName,
            uint32_t sampleRate,
            uint32_t channels,
            uint32_t outSampleRate = 0,
	    uid_t clientUid = -1,
	    pid_t clientPid = -1);
#ifndef BRINGUP_WIP

    virtual CameraSource *CreateCameraSourceFromCamera(
            const sp<hardware::ICamera> &camera,
            const sp<ICameraRecordingProxy> &proxy,
            int32_t cameraId,
            const String16& clientName,
            uid_t clientUid,
            uid_t clientPid,
            Size videoSize,
            int32_t frameRate,
            const sp<IGraphicBufferProducer>& surface,
            bool storeMetaDataInVideoBuffers = true);
#endif
    virtual CameraSourceTimeLapse *CreateCameraSourceTimeLapseFromCamera(
            const sp<hardware::ICamera> &camera,
            const sp<ICameraRecordingProxy> &proxy,
            int32_t cameraId,
            const String16& clientName,
            uid_t clientUid,
            pid_t clientPid,
            Size videoSize,
            int32_t videoFrameRate,
            const sp<IGraphicBufferProducer>& surface,
            int64_t timeBetweenFrameCaptureUs,
            bool storeMetaDataInVideoBuffers = true);

    virtual MPEG4Writer *CreateMPEG4Writer(int fd);

    virtual ElementaryStreamQueue* createESQueue(
         ElementaryStreamQueue::Mode mode, uint32_t flags = 0);

protected:
    virtual ~ExtendedFactory();

private:
    ExtendedFactory(const ExtendedFactory &);
    ExtendedFactory &operator=(ExtendedFactory &);

public:
    ExtendedFactory();
};

extern "C" AVFactory *createExtendedFactory();

} //namespace android

#endif // _EXTENDED_FACTORY_H_

