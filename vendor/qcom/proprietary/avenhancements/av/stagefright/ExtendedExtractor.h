/*
 * Copyright (c) 2015, Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */
/*
 * Copyright (c) 2013 - 2015, The Linux Foundation. All rights reserved.
 */

#ifndef _EXTENDED_EXTRACTOR_H_
#define _EXTENDED_EXTRACTOR_H_

#include <media/stagefright/DataSource.h>

namespace android {

class MediaExtractor;
class ExtendedExtractor
{
public:
    static MediaExtractor* create (
            const sp<DataSource> &source, const char *mime,
            const sp<AMessage> &meta);
    static DataSource::SnifferFunc getExtendedSniffer();
private:
    ExtendedExtractor();

    typedef MediaExtractor* (*CreateFunc)
            (const sp<DataSource> &source, const char* mime);

    static void *mLibHandle;
    static CreateFunc sCreator;
    static DataSource::SnifferFunc sSniffer;

    static CreateFunc loadCreator();
    static DataSource::SnifferFunc loadSniffer();
};

}  // namespace android

#endif //_EXTENDED_EXTRACTOR_H_
