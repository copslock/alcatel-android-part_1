/*
 * Copyright (c) 2015-2016, Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */
/*
 * Copyright (c) 2013 - 2015, The Linux Foundation. All rights reserved.
 */

//#define LOG_NDEBUG 0
#define LOG_TAG "ExtendedUtils"
#include <common/AVLog.h>
#include <cutils/properties.h>

#include <media/stagefright/foundation/ADebug.h>
#include <media/stagefright/foundation/AMessage.h>
#include <media/stagefright/foundation/ABuffer.h>
#include <media/stagefright/MediaDefs.h>
#include <media/stagefright/MediaBuffer.h>
#include <media/stagefright/MediaCodecList.h>
#include <media/stagefright/MediaCodec.h>
#include <media/stagefright/MetaData.h>
#ifndef BRINGUP_WIP
#include <media/stagefright/OMXCodec.h>
#endif
#include <media/openmax/OMX_Audio.h>
#include <media/AudioSystem.h>
#include <media/mediarecorder.h> //for enum audio_encoder
#include <QCMetaData.h>
#include <HardwareAPI.h>
#include <media/stagefright/MPEG4Writer.h>
#include <media/AudioParameter.h>
#include <QCMetaData.h>
#include <QCMediaDefs.h>
#include <OMX_QCOMExtns.h>
#include <OMX_Component.h>
#include <OMX_VideoExt.h>
#include <OMX_IndexExt.h>
#include <QOMX_AudioExtensions.h>
#include <QOMX_AudioIndexExtensions.h>
#include "include/avc_utils.h"
#include <stagefright/AVExtensions.h>
#include "stagefright/ExtendedUtils.h"
#include "stagefright/ExtendedExtractor.h"
#include <QOMX_AudioExtensions.h>
#include <camera/CameraParameters.h>
#include <common/EncoderMediaBuffer.h>

#if defined(FLAC_OFFLOAD_ENABLED) || defined(WMA_OFFLOAD_ENABLED) || \
    defined(PCM_OFFLOAD_ENABLED_24) || defined(ALAC_OFFLOAD_ENABLED) || \
    defined(APE_OFFLOAD_ENABLED)
#include "audio_defs.h"
#endif
#include <camera/CameraParameters.h>
#include <binder/IPCThreadState.h>

#define  AUDIO_PARAMETER_IS_HW_DECODER_SESSION_AVAILABLE  "is_hw_dec_session_available"

static const uint8_t kHEVCNalUnitTypeIDR         = 0x13;
static const uint8_t kHEVCNalUnitTypeIDRNoLP     = 0x14;
static const uint8_t kHEVCNalUnitTypeCRA         = 0x15;
static const uint8_t kHEVCNalUnitTypeVidParamSet = 0x20;
static const uint8_t kHEVCNalUnitTypeSeqParamSet = 0x21;
static const uint8_t kHEVCNalUnitTypePicParamSet = 0x22;

namespace android {

AVUtils *createExtendedUtils() {
    return new ExtendedUtils;
}

ExtendedUtils::ExtendedUtils() {
    updateLogLevel();
    AVLOGD("ExtendedUtils()");
}

ExtendedUtils::~ExtendedUtils() {
    AVLOGD("~ExtendedUtils()");
}

struct mime_conv_t {
    const char* mime;
    audio_format_t format;
};

static const struct mime_conv_t mimeLookup[] = {
    { MEDIA_MIMETYPE_AUDIO_FLAC,        AUDIO_FORMAT_FLAC },
    { MEDIA_MIMETYPE_CONTAINER_QTIFLAC, AUDIO_FORMAT_FLAC },
    { MEDIA_MIMETYPE_AUDIO_ALAC,        AUDIO_FORMAT_ALAC },
    { MEDIA_MIMETYPE_AUDIO_WMA,         AUDIO_FORMAT_WMA },
    { MEDIA_MIMETYPE_AUDIO_APE,         AUDIO_FORMAT_APE  },
    { MEDIA_MIMETYPE_AUDIO_EVRC,        AUDIO_FORMAT_EVRC },
    { MEDIA_MIMETYPE_AUDIO_QCELP,       AUDIO_FORMAT_QCELP },
    { MEDIA_MIMETYPE_AUDIO_AMR_WB_PLUS, AUDIO_FORMAT_AMR_WB_PLUS },
    { MEDIA_MIMETYPE_AUDIO_DSD,         AUDIO_FORMAT_DSD },
    { MEDIA_MIMETYPE_CONTAINER_DSF,     AUDIO_FORMAT_DSD },
    { MEDIA_MIMETYPE_CONTAINER_DFF,     AUDIO_FORMAT_DSD },
    { 0, AUDIO_FORMAT_INVALID }
};
enum MetaKeyType{
    INT32, INT64, STRING, DATA, CSD
};

struct MetaKeyEntry{
    int MetaKey;
    const char* MsgKey;
    MetaKeyType KeyType;
};

static const MetaKeyEntry MetaKeyTable[] {
   {kKeyBitRate              , "bitrate"                , INT32},
   {kKeyAacCodecSpecificData , "aac-codec-specific-data", CSD},
   {kKeyRawCodecSpecificData , "raw-codec-specific-data", CSD},
   {kKeyDivXVersion          , "divx-version"           , INT32},  // int32_t
   {kKeyDivXDrm              , "divx-drm"               , DATA},  // void *
   {kKeyWMAEncodeOpt         , "wma-encode-opt"         , INT32},  // int32_t
   {kKeyWMABlockAlign        , "wma-block-align"        , INT32},  // int32_t
   {kKeyWMAVersion           , "wma-version"            , INT32},  // int32_t
   {kKeyWMAAdvEncOpt1        , "wma-adv-enc-opt1"       , INT32},  // int16_t
   {kKeyWMAAdvEncOpt2        , "wma-adv-enc-opt2"       , INT32},  // int32_t
   {kKeyWMAFormatTag         , "wma-format-tag"         , INT32},  // int32_t
   {kKeyWMABitspersample     , "wma-bits-per-sample"    , INT32},  // int32_t
   {kKeyWMAVirPktSize        , "wma-vir-pkt-size"       , INT32},  // int32_t
   {kKeyWMAChannelMask       , "wma-channel-mask"       , INT32},  // int32_t

   {kKeyFileFormat           , "file-format"            , STRING},  // cstring

   {kkeyAacFormatAdif        , "aac-format-adif"        , INT32},  // bool (int32_t)
   {kkeyAacFormatLtp         , "aac-format-ltp"         , INT32},

   //DTS subtype
   {kKeyDTSSubtype           , "dts-subtype"            , INT32},  //int32_t

   //Extractor sets this
   {kKeyUseArbitraryMode     , "use-arbitrary-mode"     , INT32},  //bool (int32_t)
   {kKeySmoothStreaming      , "smooth-streaming"       , INT32},  //bool (int32_t)
   {kKeyHFR                  , "hfr"                    , INT32},  // int32_t

   {kKeySampleRate           , "sample-rate"            , INT32},
   {kKeyChannelCount         , "channel-count"          , INT32},
   {kKeyMinBlkSize           , "min-block-size"         , INT32},
   {kKeyMaxBlkSize           , "max-block-size"         , INT32},
   {kKeyMinFrmSize           , "min-frame-size"         , INT32},
   {kKeyMaxFrmSize           , "max-frame-size"         , INT32},
   {kKeySampleBits           , "bits-per-sample"        , INT32},
   {kKeyIsByteMode           , "is-byte-stream-mode"    , INT32},
};

enum {
    // Indicate if it is OK to hold on to the MediaBuffer and not
    // release it immediately
    kKeyCanDeferRelease   = 'drel', // bool (int32_t)
};

// static
const char* ExtendedUtils::getMsgFromKey(int key) {
    static const size_t numMetaKeys =
                     sizeof(MetaKeyTable) / sizeof(MetaKeyTable[0]);
    size_t i;
    for (i = 0; i < numMetaKeys; ++i) {
        if (key == MetaKeyTable[i].MetaKey) {
            return MetaKeyTable[i].MsgKey;
        }
    }
    return "unknown";
}

status_t ExtendedUtils::convertMetaDataToMessage(
        const sp<MetaData> &meta, sp<AMessage> *format) {
    CHECK (format != NULL);

    const char *str_val;
    int32_t int32_val;
    int64_t int64_val;
    uint32_t data_type;
    const void *data;
    size_t size;
    static const size_t numMetaKeys =
                     sizeof(MetaKeyTable) / sizeof(MetaKeyTable[0]);

    size_t i;
    for (i = 0; i < numMetaKeys; ++i) {
        if (MetaKeyTable[i].KeyType == INT32 &&
            meta->findInt32(MetaKeyTable[i].MetaKey, &int32_val)) {
            AVLOGV("convert: int32 : key=%s value=%d", MetaKeyTable[i].MsgKey, int32_val);
            format->get()->setInt32(MetaKeyTable[i].MsgKey, int32_val);
        } else if (MetaKeyTable[i].KeyType == INT64 &&
                 meta->findInt64(MetaKeyTable[i].MetaKey, &int64_val)) {
            AVLOGV("convert int64 : key=%s value=%" PRId64 "", MetaKeyTable[i].MsgKey, int64_val);
            format->get()->setInt64(MetaKeyTable[i].MsgKey, int64_val);
        } else if (MetaKeyTable[i].KeyType == STRING &&
                 meta->findCString(MetaKeyTable[i].MetaKey, &str_val)) {
            AVLOGV("convert: string : key=%s value=%s", MetaKeyTable[i].MsgKey, str_val);
            format->get()->setString(MetaKeyTable[i].MsgKey, str_val);
        } else if ( (MetaKeyTable[i].KeyType == DATA ||
                   MetaKeyTable[i].KeyType == CSD) &&
                   meta->findData(MetaKeyTable[i].MetaKey, &data_type, &data, &size)) {
            AVLOGV("convert: data : key=%s size=%zu", MetaKeyTable[i].MsgKey, size);
            if (MetaKeyTable[i].KeyType == CSD) {
                const char *mime;
                CHECK(meta->findCString(kKeyMIMEType, &mime));
                if (strcasecmp(mime, MEDIA_MIMETYPE_VIDEO_AVC)) {
                    sp<ABuffer> buffer = new ABuffer(size);
                    memcpy(buffer->data(), data, size);
                    buffer->meta()->setInt32("csd", true);
                    buffer->meta()->setInt64("timeUs", 0);
                    format->get()->setBuffer("csd-0", buffer);
                } else {
                    const uint8_t *ptr = (const uint8_t *)data;
                    CHECK(size >= 8);
                    int seqLength = 0, picLength = 0;
                    for (size_t i = 4; i < (size - 4); i++)
                    {
                        if ((*(ptr + i) == 0) && (*(ptr + i + 1) == 0) &&
                           (*(ptr + i + 2) == 0) && (*(ptr + i + 3) == 1))
                            seqLength = i;
                    }
                    sp<ABuffer> buffer = new ABuffer(seqLength);
                    memcpy(buffer->data(), data, seqLength);
                    buffer->meta()->setInt32("csd", true);
                    buffer->meta()->setInt64("timeUs", 0);
                    format->get()->setBuffer("csd-0", buffer);
                    picLength=size-seqLength;
                    sp<ABuffer> buffer1 = new ABuffer(picLength);
                    memcpy(buffer1->data(), (const uint8_t *)data + seqLength, picLength);
                    buffer1->meta()->setInt32("csd", true);
                    buffer1->meta()->setInt64("timeUs", 0);
                    format->get()->setBuffer("csd-1", buffer1);
                }
            } else {
                sp<ABuffer> buffer = new ABuffer(size);
                memcpy(buffer->data(), data, size);
                format->get()->setBuffer(MetaKeyTable[i].MsgKey, buffer);
            }
        }
    }
    return OK;
}

status_t ExtendedUtils::convertMessageToMetaData(
        const sp<AMessage> &msg, sp<MetaData> &meta) {
    CHECK ((msg != NULL) && (meta != NULL));

    AString str_val;
    int32_t int32_val;
    int64_t int64_val;
    static const size_t numMetaKeys =
        sizeof(MetaKeyTable) / sizeof(MetaKeyTable[0]);

    for (size_t i = 0; i < numMetaKeys; ++i) {
        if (MetaKeyTable[i].KeyType == INT32 &&
                msg->findInt32(MetaKeyTable[i].MsgKey, &int32_val)) {
            meta->setInt32(MetaKeyTable[i].MetaKey, int32_val);
        } else if (MetaKeyTable[i].KeyType == INT64 &&
                msg->findInt64(MetaKeyTable[i].MsgKey, &int64_val)) {
            meta->setInt64(MetaKeyTable[i].MetaKey, int64_val);
        } else if (MetaKeyTable[i].KeyType == STRING &&
                msg->findString(MetaKeyTable[i].MsgKey, &str_val)) {
            meta->setCString(MetaKeyTable[i].MetaKey, str_val.c_str());
        }
    }
    return OK;
}

bool ExtendedUtils::isHwAudioDecoderSessionAllowed(const sp<AMessage> &format) {

    if (format != NULL) {
        int isallowed = 0;
        String8  hal_result,hal_query;
        AString mime;

        hal_query = AUDIO_PARAMETER_IS_HW_DECODER_SESSION_AVAILABLE;
        CHECK(format->findString("mime", &mime));

        if (!mime.c_str()) {
            AVLOGE("voice_conc: null mime type string");
            return true;
        }

        //Append mime type to query
        hal_query += "=";
        hal_query +=  mime.c_str();

        //Check with HAL whether new session can be created for DSP only decoding formats
        hal_result =
                AudioSystem::getParameters((audio_io_handle_t)0, hal_query);
        AudioParameter result = AudioParameter(hal_result);
        if (result.getInt(
                    String8(AUDIO_PARAMETER_IS_HW_DECODER_SESSION_AVAILABLE),
                    isallowed) == NO_ERROR) {
            if (!isallowed) {
                AVLOGD("voice_conc:ExtendedUtils : Rejecting audio decoder"
                        " session request for %s ", mime.c_str());
                return false;
            }
        }
    }
    return true;
}


sp<MediaCodec> ExtendedUtils::createCustomComponentByName(const sp<ALooper> &looper,
        const char* mime, bool encoder, const sp<AMessage> &msg) {
    sp<MediaCodec> codec = NULL;
    AString mimeType;

    if (!mime) {
        AVLOGE("mime type is not set");
        return NULL;
    }

    mimeType.setTo(mime);
    if ( msg != NULL && !overrideMimeType(msg, &mimeType)) {
        if (mime != NULL) {
            AVLOGV("createByComponentName for clip of mimetype %s", mime);
            if (!strcasecmp(mime, MEDIA_MIMETYPE_AUDIO_AMR_WB_PLUS) && !encoder) {
                codec = MediaCodec::CreateByComponentName(looper, "OMX.qcom.audio.decoder.amrwbplus");
            } else if (!strcasecmp(mime, MEDIA_MIMETYPE_AUDIO_WMA) && !encoder) {
                codec = MediaCodec::CreateByComponentName(looper, "OMX.qcom.audio.decoder.wma");
            } else if (!strcasecmp(mime, MEDIA_MIMETYPE_AUDIO_ALAC) && !encoder) {
                bool useSwDecoder = property_get_bool("use.qti.sw.alac.decoder", false);
                if (useSwDecoder) {
                    codec = MediaCodec::CreateByComponentName(looper, "OMX.qti.audio.decoder.alac.sw");
                }
                else {
                    codec = MediaCodec::CreateByComponentName(looper, "OMX.qcom.audio.decoder.alac");
                }
            } else if (!strcasecmp(mime, MEDIA_MIMETYPE_AUDIO_APE) && !encoder) {
                bool useSwDecoder = property_get_bool("use.qti.sw.ape.decoder", false);
                if (useSwDecoder) {
                    codec = MediaCodec::CreateByComponentName(looper, "OMX.qti.audio.decoder.ape.sw");
                }
                else {
                    codec = MediaCodec::CreateByComponentName(looper, "OMX.qcom.audio.decoder.ape");
                }
            } else if (!strcasecmp(mime, MEDIA_MIMETYPE_AUDIO_QCELP) && !encoder) {
                codec = MediaCodec::CreateByComponentName(looper, "OMX.qcom.audio.decoder.Qcelp13");
            } else if (!strcasecmp(mime, MEDIA_MIMETYPE_AUDIO_EVRC) && !encoder) {
                codec = MediaCodec::CreateByComponentName(looper, "OMX.qcom.audio.decoder.evrc");
            } else if (!strcasecmp(mime, MEDIA_MIMETYPE_AUDIO_DSD) && !encoder) {
                codec = MediaCodec::CreateByComponentName(looper, "OMX.qti.audio.decoder.dsd");
            } else {
                  AVLOGV("Could not create by component name");
            }
        }
    } else {
           if (!strcasecmp(mimeType.c_str(), MEDIA_MIMETYPE_AUDIO_WMA_PRO) && !encoder) {
                codec = MediaCodec::CreateByComponentName(looper, "OMX.qcom.audio.decoder.wma10Pro");
            } else if (!strcasecmp(mimeType.c_str(), MEDIA_MIMETYPE_AUDIO_WMA_LOSSLESS) && !encoder) {
                codec = MediaCodec::CreateByComponentName(looper, "OMX.qcom.audio.decoder.wmaLossLess");
            }
    }


    return codec;
}

/*
QCOM HW AAC encoder allowed bitrates
------------------------------------------------------------------------------------------------------------------
Bitrate limit |AAC-LC(Mono)           | AAC-LC(Stereo)        |AAC+(Mono)            | AAC+(Stereo)            | eAAC+                      |
Minimum     |Min(24000,0.5 * f_s)   |Min(24000,f_s)           | 24000                      |24000                        |  24000                       |
Maximum    |Min(192000,6 * f_s)    |Min(192000,12 * f_s)  | Min(192000,6 * f_s)  | Min(192000,12 * f_s)  |  Min(192000,12 * f_s) |
------------------------------------------------------------------------------------------------------------------
*/
bool ExtendedUtils::useQCHWEncoder(const sp<AMessage> &outputFormat, Vector<AString> *role)
{
    int minBitRate = -1;
    int maxBitRate = -1;
    char propValue[PROPERTY_VALUE_MAX] = {0};
    bool currentState;
    int32_t bitRate = 0;
    int32_t sampleRate = 0;
    int32_t numChannels = 0;
    int32_t aacProfile = 0;
    audio_encoder aacEncoder = AUDIO_ENCODER_DEFAULT;
    AString mime;
    bool isQCHWEncoder = false;
    property_get("qcom.hw.aac.encoder",propValue,NULL);
    CHECK(outputFormat->findString("mime", &mime));

    if (!strcasecmp(mime.c_str(), MEDIA_MIMETYPE_AUDIO_AAC) &&
               !strncmp(propValue,"true",sizeof("true"))) {
        //check for QCOM's HW AAC encoder only when qcom.aac.encoder =  true;
        AVLOGV("qcom.aac.encoder enabled, check AAC encoder allowed bitrates");

        outputFormat->findInt32("bitrate", &bitRate);
        outputFormat->findInt32("channel-count", &numChannels);
        outputFormat->findInt32("sample-rate", &sampleRate);
        outputFormat->findInt32("aac-profile", &aacProfile);

        AVLOGD("bitrate:%d, samplerate:%d, channels:%d",
                        bitRate, sampleRate, numChannels);

        switch(aacProfile) {
            case OMX_AUDIO_AACObjectLC:
                aacEncoder = AUDIO_ENCODER_AAC;
                AVLOGV("AUDIO_ENCODER_AAC");
                break;
            case OMX_AUDIO_AACObjectHE:
                aacEncoder = AUDIO_ENCODER_HE_AAC;
                AVLOGV("AUDIO_ENCODER_HE_AAC");
                break;
            case OMX_AUDIO_AACObjectELD:
                aacEncoder = AUDIO_ENCODER_AAC_ELD;
                AVLOGV("AUDIO_ENCODER_AAC_ELD");
                break;
            default:
                aacEncoder = AUDIO_ENCODER_DEFAULT;
                AVLOGE("Wrong encoder profile");
                break;
        }

        switch (aacEncoder) {
            case AUDIO_ENCODER_AAC:// for AAC-LC format
                if (numChannels == 1) {//mono
                    minBitRate = MIN_BITRATE_AAC < (sampleRate / 2) ? MIN_BITRATE_AAC: (sampleRate / 2);
                    maxBitRate = MAX_BITRATE_AAC < (sampleRate*6) ? MAX_BITRATE_AAC: (sampleRate * 6);
                } else if (numChannels == 2) {//stereo
                    minBitRate = MIN_BITRATE_AAC < sampleRate ? MIN_BITRATE_AAC: sampleRate;
                    maxBitRate = MAX_BITRATE_AAC < (sampleRate*12) ? MAX_BITRATE_AAC: (sampleRate * 12);
                }
                break;
            case AUDIO_ENCODER_HE_AAC:// for AAC+ format
                // Do not use HW AAC encoder for HE AAC(AAC+) formats.
                isQCHWEncoder = false;
                break;
            default:
                AVLOGV("encoder:%d not supported by QCOM HW encoder", aacEncoder);
        }

        //return true only when 1. minBitRate and maxBitRate are updated(not -1) 2. minBitRate <= SampleRate <= maxBitRate
        if (bitRate >= minBitRate && bitRate <= maxBitRate) {
            isQCHWEncoder = true;
        }

        if (isQCHWEncoder) {
            role->push("OMX.qcom.audio.encoder.aac");
            AVLOGD("Using encoder OMX.qcom.audio.encoder.aac");
        } else {
           role->push("");
        }
    } else  if (!strcasecmp(mime.c_str(), MEDIA_MIMETYPE_AUDIO_QCELP)) {
        role->push("OMX.qcom.audio.encoder.qcelp13");
        isQCHWEncoder = true;
        AVLOGD("Using encoder OMX.qcom.audio.encoder.qcelp13");
    } else if(!strcasecmp(mime.c_str(), MEDIA_MIMETYPE_AUDIO_EVRC)) {
        role->push("OMX.qcom.audio.encoder.evrc");
        isQCHWEncoder = true;
        AVLOGD("Using encoder OMX.qcom.audio.encoder.evrc");
    } else {
        role->push("");
    }
    return isQCHWEncoder;
}

bool ExtendedUtils::canDeferRelease(const sp<MetaData> &meta) {
    if (meta == NULL) {
        return false;
    }
    int32_t deferRelease = false;
    meta->findInt32(kKeyCanDeferRelease, &deferRelease);
    AVLOGV("canDeferRelease: %s", deferRelease ? "yes" : "no");
    return deferRelease;
}

void ExtendedUtils::setDeferRelease(sp<MetaData> &meta) {
   if (meta == NULL) {
        return;
   }
   meta->setInt32(kKeyCanDeferRelease, true);
   AVLOGV("setDeferRelease");
   return;
}

bool ExtendedUtils::overrideMimeType(
        const sp<AMessage> &msg, AString* mime) {
    status_t ret = false;

    if (!mime) {
        return false;
    }

    if (!strncmp(mime->c_str(), MEDIA_MIMETYPE_AUDIO_WMA,
         strlen(MEDIA_MIMETYPE_AUDIO_WMA))) {
        int32_t WMAVersion = 0;
        if ((msg->findInt32(getMsgFromKey(kKeyWMAVersion), &WMAVersion))) {
            if (WMAVersion==kTypeWMA) {
                //no need to update mime type
            } else if (WMAVersion==kTypeWMAPro) {
                mime->setTo(MEDIA_MIMETYPE_AUDIO_WMA_PRO);
                ret = true;
            } else if (WMAVersion==kTypeWMALossLess) {
                mime->setTo(MEDIA_MIMETYPE_AUDIO_WMA_LOSSLESS);
                ret = true;
            } else {
                AVLOGE("could not set valid wma mime type");
            }
        }
    }
    return ret;
}

status_t ExtendedUtils::mapMimeToAudioFormat( audio_format_t& format, const char* mime )
{

    for (uint32_t i = 0; i < sizeof(mimeLookup)/sizeof(mimeLookup[0]); i++) {
        if (mimeLookup[i].mime != NULL && mime != NULL) {
            if (0 == strncasecmp(mime, mimeLookup[i].mime, strlen(mimeLookup[i].mime))) {
                format = (audio_format_t) mimeLookup[i].format;
                return OK;
            }
       }
    }

    return BAD_VALUE;
}

status_t ExtendedUtils::sendMetaDataToHal(const sp<MetaData>& meta, AudioParameter *param) {

    (void)param;
    const char *mime;
    bool success = meta->findCString(kKeyMIMEType, &mime);
    CHECK(success);

#ifdef FLAC_OFFLOAD_ENABLED
    int32_t minBlkSize = 0, maxBlkSize = 0, minFrmSize = 0, maxFrmSize = 0;
    if(!strcasecmp(mime, MEDIA_MIMETYPE_AUDIO_FLAC)) {
        if (meta->findInt32(kKeyMinBlkSize, &minBlkSize)) {
            param->addInt(String8(AUDIO_OFFLOAD_CODEC_FLAC_MIN_BLK_SIZE), minBlkSize);
        }
        if (meta->findInt32(kKeyMaxBlkSize, &maxBlkSize)) {
            param->addInt(String8(AUDIO_OFFLOAD_CODEC_FLAC_MAX_BLK_SIZE), maxBlkSize);
        }
        if (meta->findInt32(kKeyMinFrmSize, &minFrmSize)) {
            param->addInt(String8(AUDIO_OFFLOAD_CODEC_FLAC_MIN_FRAME_SIZE), minFrmSize);
        }
        if (meta->findInt32(kKeyMaxFrmSize, &maxFrmSize)) {
            param->addInt(String8(AUDIO_OFFLOAD_CODEC_FLAC_MAX_FRAME_SIZE), maxFrmSize);
        }
        AVLOGV("FLAC metadata: minBlkSize %d, maxBlkSize %d, minFrmSize %d, maxFrmSize %d",
                minBlkSize, maxBlkSize, minFrmSize, maxFrmSize);
        return OK;
    }
#endif

#ifdef WMA_OFFLOAD_ENABLED
    int32_t wmaFormatTag = 0, wmaBlockAlign = 0, wmaChannelMask = 0;
    int32_t wmaBitsPerSample = 0, wmaEncodeOpt = 0, wmaEncodeOpt1 = 0;
    int32_t wmaEncodeOpt2 = 0;

    if(!strcasecmp(mime, MEDIA_MIMETYPE_AUDIO_WMA)) {
        if (meta->findInt32(kKeyWMAFormatTag, &wmaFormatTag)) {
            param->addInt(String8(AUDIO_OFFLOAD_CODEC_WMA_FORMAT_TAG), wmaFormatTag);
        }
        if (meta->findInt32(kKeyWMABlockAlign, &wmaBlockAlign)) {
            param->addInt(String8(AUDIO_OFFLOAD_CODEC_WMA_BLOCK_ALIGN), wmaBlockAlign);
        }
        if (meta->findInt32(kKeyWMABitspersample, &wmaBitsPerSample)) {
            param->addInt(String8(AUDIO_OFFLOAD_CODEC_WMA_BIT_PER_SAMPLE), wmaBitsPerSample);
        }
        if (meta->findInt32(kKeyWMAChannelMask, &wmaChannelMask)) {
            param->addInt(String8(AUDIO_OFFLOAD_CODEC_WMA_CHANNEL_MASK), wmaChannelMask);
        }
        if (meta->findInt32(kKeyWMAEncodeOpt, &wmaEncodeOpt)) {
            param->addInt(String8(AUDIO_OFFLOAD_CODEC_WMA_ENCODE_OPTION), wmaEncodeOpt);
        }
        if (meta->findInt32(kKeyWMAAdvEncOpt1, &wmaEncodeOpt1)) {
            param->addInt(String8(AUDIO_OFFLOAD_CODEC_WMA_ENCODE_OPTION1), wmaEncodeOpt1);
        }
        if (meta->findInt32(kKeyWMAAdvEncOpt2, &wmaEncodeOpt2)) {
            param->addInt(String8(AUDIO_OFFLOAD_CODEC_WMA_ENCODE_OPTION2), wmaEncodeOpt2);
        }
        AVLOGV("WMA specific meta: fmt_tag 0x%x, blk_align %d, bits_per_sample %d, "
                "enc_options 0x%x", wmaFormatTag, wmaBlockAlign,
                wmaBitsPerSample, wmaEncodeOpt);
        return OK;
    }
#endif

#ifdef VORBIS_OFFLOAD_ENABLED
    int32_t bitstream_fmt = 1;
    if (!strcasecmp(mime, MEDIA_MIMETYPE_AUDIO_VORBIS)) {
        param->addInt(String8(AUDIO_OFFLOAD_CODEC_VORBIS_BITSTREAM_FMT), bitstream_fmt);
        AVLOGV("Vorbis metadata: bitstream_fmt %d", bitstream_fmt);
        return OK;
    }
#endif

    const void *data;
    size_t size;
    uint32_t type = 0;
    if (meta->findData(kKeyRawCodecSpecificData, &type, &data, &size)) {
        CHECK(data && (size == ALAC_CSD_SIZE || size == APE_CSD_SIZE));
        AVLOGV("Found kKeyRawCodecSpecificData of size %zu", size);
        const uint8_t *ptr = (uint8_t *) data;

#ifdef ALAC_OFFLOAD_ENABLED
        if (!strncasecmp(mime, MEDIA_MIMETYPE_AUDIO_ALAC,
                    strlen(MEDIA_MIMETYPE_AUDIO_ALAC))) {
            uint32_t frameLength = 0, maxFrameBytes = 0, avgBitRate = 0;
            uint32_t samplingRate = 0, channelLayoutTag = 0;
            uint8_t compatibleVersion = 0, pb = 0, mb = 0, kb = 0, numChannels = 0, bitDepth = 0;
            uint16_t maxRun = 0;

            memcpy(&frameLength, ptr + kKeyIndexAlacFrameLength, sizeof(frameLength));
            memcpy(&compatibleVersion, ptr + kKeyIndexAlacCompatibleVersion, sizeof(compatibleVersion));
            memcpy(&bitDepth, ptr + kKeyIndexAlacBitDepth, sizeof(bitDepth));
            memcpy(&pb, ptr + kKeyIndexAlacPb, sizeof(pb));
            memcpy(&mb, ptr + kKeyIndexAlacMb, sizeof(mb));
            memcpy(&kb, ptr + kKeyIndexAlacKb, sizeof(kb));
            memcpy(&numChannels, ptr + kKeyIndexAlacNumChannels, sizeof(numChannels));
            memcpy(&maxRun, ptr + kKeyIndexAlacMaxRun, sizeof(maxRun));
            memcpy(&maxFrameBytes, ptr + kKeyIndexAlacMaxFrameBytes, sizeof(maxFrameBytes));
            memcpy(&avgBitRate, ptr + kKeyIndexAlacAvgBitRate, sizeof(avgBitRate));
            memcpy(&samplingRate, ptr + kKeyIndexAlacSamplingRate, sizeof(samplingRate));

            AVLOGV("ALAC CSD values: frameLength %d bitDepth %d numChannels %d"
                    " maxFrameBytes %d avgBitRate %d samplingRate %d",
                    frameLength, bitDepth, numChannels, maxFrameBytes, avgBitRate, samplingRate);

            param->addInt(String8(AUDIO_OFFLOAD_CODEC_ALAC_FRAME_LENGTH), frameLength);
            param->addInt(String8(AUDIO_OFFLOAD_CODEC_ALAC_COMPATIBLE_VERSION), compatibleVersion);
            param->addInt(String8(AUDIO_OFFLOAD_CODEC_ALAC_BIT_DEPTH), bitDepth);
            param->addInt(String8(AUDIO_OFFLOAD_CODEC_ALAC_PB), pb);
            param->addInt(String8(AUDIO_OFFLOAD_CODEC_ALAC_MB), mb);
            param->addInt(String8(AUDIO_OFFLOAD_CODEC_ALAC_KB), kb);
            param->addInt(String8(AUDIO_OFFLOAD_CODEC_ALAC_NUM_CHANNELS), numChannels);
            param->addInt(String8(AUDIO_OFFLOAD_CODEC_ALAC_MAX_RUN), maxRun);
            param->addInt(String8(AUDIO_OFFLOAD_CODEC_ALAC_MAX_FRAME_BYTES), maxFrameBytes);
            param->addInt(String8(AUDIO_OFFLOAD_CODEC_ALAC_AVG_BIT_RATE), avgBitRate);
            param->addInt(String8(AUDIO_OFFLOAD_CODEC_ALAC_SAMPLING_RATE), samplingRate);
            param->addInt(String8(AUDIO_OFFLOAD_CODEC_ALAC_CHANNEL_LAYOUT_TAG), channelLayoutTag);
            return OK;
        }
#endif
#ifdef APE_OFFLOAD_ENABLED
        if (!strncasecmp(mime, MEDIA_MIMETYPE_AUDIO_APE,
                    strlen(MEDIA_MIMETYPE_AUDIO_APE))) {
            uint16_t compatibleVersion = 0, compressionLevel = 0;
            uint16_t bitsPerSample = 0, numChannels = 0;
            uint32_t formatFlags = 0, blocksPerFrame = 0, finalFrameBlocks = 0;
            uint32_t totalFrames = 0, sampleRate = 0, seekTablePresent = 0;

            memcpy(&compatibleVersion, ptr + kKeyIndexApeCompatibleVersion, sizeof(compatibleVersion));
            memcpy(&compressionLevel, ptr + kKeyIndexApeCompressionLevel, sizeof(compressionLevel));
            memcpy(&formatFlags, ptr + kKeyIndexApeFormatFlags, sizeof(formatFlags));
            memcpy(&blocksPerFrame, ptr + kKeyIndexApeBlocksPerFrame, sizeof(blocksPerFrame));
            memcpy(&finalFrameBlocks, ptr + kKeyIndexApeFinalFrameBlocks, sizeof(finalFrameBlocks));
            memcpy(&totalFrames, ptr + kKeyIndexApeTotalFrames, sizeof(totalFrames));
            memcpy(&bitsPerSample, ptr + kKeyIndexApeBitsPerSample, sizeof(bitsPerSample));
            memcpy(&numChannels, ptr + kKeyIndexApeNumChannels, sizeof(numChannels));
            memcpy(&sampleRate, ptr + kKeyIndexApeSampleRate, sizeof(sampleRate));
            memcpy(&seekTablePresent, ptr + kKeyIndexApeSeekTablePresent, sizeof(seekTablePresent));

            AVLOGV("APE CSD values: compatibleVersion %d compressionLevel %d formatFlags %d"
                    " blocksPerFrame %d finalFrameBlocks %d totalFrames %d bitsPerSample %d"
                    " numChannels %d sampleRate %d seekTablePresent %d",
                    compatibleVersion, compressionLevel, formatFlags, blocksPerFrame, finalFrameBlocks, totalFrames,
                    bitsPerSample, numChannels, sampleRate, seekTablePresent);

            param->addInt(String8(AUDIO_OFFLOAD_CODEC_APE_COMPATIBLE_VERSION), compatibleVersion);
            param->addInt(String8(AUDIO_OFFLOAD_CODEC_APE_COMPRESSION_LEVEL), compressionLevel);
            param->addInt(String8(AUDIO_OFFLOAD_CODEC_APE_FORMAT_FLAGS), formatFlags);
            param->addInt(String8(AUDIO_OFFLOAD_CODEC_APE_BLOCKS_PER_FRAME), blocksPerFrame);
            param->addInt(String8(AUDIO_OFFLOAD_CODEC_APE_FINAL_FRAME_BLOCKS), finalFrameBlocks);
            param->addInt(String8(AUDIO_OFFLOAD_CODEC_APE_TOTAL_FRAMES), totalFrames);
            param->addInt(String8(AUDIO_OFFLOAD_CODEC_APE_BITS_PER_SAMPLE), bitsPerSample);
            param->addInt(String8(AUDIO_OFFLOAD_CODEC_APE_NUM_CHANNELS), numChannels);
            param->addInt(String8(AUDIO_OFFLOAD_CODEC_APE_SAMPLE_RATE), sampleRate);
            param->addInt(String8(AUDIO_OFFLOAD_CODEC_APE_SEEK_TABLE_PRESENT), seekTablePresent);
            return OK;
        }
#endif
    }
    return OK;
}

bool ExtendedUtils::hasAudioSampleBits(const sp<MetaData> &meta) {
    bool status = false;
    const char* mime;
    if (meta != NULL) {
        if (meta->findCString(kKeyMIMEType, &mime)) {
            if (!strcasecmp(mime, MEDIA_MIMETYPE_AUDIO_WMA)) {
                status = meta->hasData(kKeyWMABitspersample);
            } else {
                status = meta->hasData(kKeySampleBits);
            }
        }
    }
    return status;
}

bool ExtendedUtils::hasAudioSampleBits(const sp<AMessage> &format) {
    bool status = false;
    AString mime;
    if (format != NULL) {
        if (format->findString("mime", &mime)) {
            if (!strcasecmp(mime.c_str(), MEDIA_MIMETYPE_AUDIO_WMA)) {
                status = format->contains("wma-bits-per-sample");
            } else {
                status = format->contains("bits-per-sample");
            }
        }
    }
    return status;
}

int32_t ExtendedUtils::getAudioSampleBits(const sp<MetaData> &meta) {
    int32_t bitWidth = 16;
    const char* mime;
    if (meta != NULL) {
        if (meta->findCString(kKeyMIMEType, &mime)) {
            if (!strcasecmp(mime, MEDIA_MIMETYPE_AUDIO_WMA)) {
                meta->findInt32(kKeyWMABitspersample, &bitWidth);
            }
            else {
                meta->findInt32(kKeySampleBits, &bitWidth);
            }
        }
    }
    return bitWidth;
}

int32_t ExtendedUtils::getAudioSampleBits(const sp<AMessage> &format) {
    int32_t bitWidth = 16;
    AString mime;
    if (format != NULL) {
        if (format->findString("mime", &mime)) {
            if (!strcasecmp(mime.c_str(), MEDIA_MIMETYPE_AUDIO_WMA)) {
                format->findInt32("wma-bits-per-sample", &bitWidth);
            } else {
                format->findInt32("bits-per-sample", &bitWidth);
            }
        }
    }
    return bitWidth;
}

bool ExtendedUtils::isAudioWMAPro(const sp<MetaData> &meta) {
    const char *mime = NULL;
    int32_t wmaVersion = kTypeWMA;
    if ((meta == NULL) || !(meta->findCString(kKeyMIMEType, &mime))) {
        return false;
    }

    if (!strcasecmp(mime, MEDIA_MIMETYPE_AUDIO_WMA)) {
       if (meta->findInt32(kKeyWMAVersion, &wmaVersion)) {
          if (wmaVersion == kTypeWMAPro || wmaVersion == kTypeWMALossLess) {
              AVLOGV("wma version is PRO");
              return true;
          }
       }
   }

   return false;
}

bool ExtendedUtils::isAudioWMAPro(const sp<AMessage> &format) {
    AString mime;
    int32_t wmaVersion = kTypeWMA;
    if ((format == NULL) || !(format->findString("mime", &mime))) {
        return false;
    }

    if (!strcasecmp(mime.c_str(), MEDIA_MIMETYPE_AUDIO_WMA)) {
       if (format->findInt32("wma-version", &wmaVersion)) {
          if (wmaVersion == kTypeWMAPro || wmaVersion == kTypeWMALossLess) {
              AVLOGV("wma version is PRO");
              return true;
          }
       }
   }

   return false;
}

audio_format_t ExtendedUtils::updateAudioFormat(audio_format_t audioFormat,
                                                const sp<AMessage> &format) {
    if (audioFormat == AUDIO_FORMAT_WMA) {
        return isAudioWMAPro(format)? AUDIO_FORMAT_WMA_PRO : AUDIO_FORMAT_WMA;
    }
    return audioFormat;
}

audio_format_t ExtendedUtils::updateAudioFormat(audio_format_t audioFormat,
                                                const sp<MetaData> &meta) {
    if (audioFormat == AUDIO_FORMAT_WMA) {
        return isAudioWMAPro(meta)? AUDIO_FORMAT_WMA_PRO : AUDIO_FORMAT_WMA;
    }
    return audioFormat;
}

bool ExtendedUtils::canOffloadAPE(const sp<MetaData> &meta)
{
    const char *mime;
    if(meta == NULL || !(meta->findCString(kKeyMIMEType, &mime))) {
        AVLOGD("canOffloadAPE: params are wrong");
        return true; // return true as, cannot make decision
    }
    if (!strcasecmp(mime, MEDIA_MIMETYPE_AUDIO_APE)) {
        const void *data;
        size_t size;
        uint32_t type = 0;
        uint16_t compressionLevel = 0;
        if (meta->findData(kKeyRawCodecSpecificData, &type, &data, &size)) {
            CHECK(data && (size == APE_CSD_SIZE));
            memcpy(&compressionLevel, (uint8_t *) data + kKeyIndexApeCompressionLevel, sizeof(compressionLevel));
        }
        if (compressionLevel != APE_COMPRESSION_LEVEL_FAST &&
            compressionLevel != APE_COMPRESSION_LEVEL_NORMAL &&
            compressionLevel != APE_COMPRESSION_LEVEL_HIGH) {
            AVLOGD("Disallow offload for APE clip with compressionLevel %d", compressionLevel);
            return false;
        }
    }
    return true;
}

int32_t ExtendedUtils::getAudioMaxInputBufferSize(audio_format_t audioFormat, const sp<AMessage> &format) {
    int32_t audioBufSize = 0;
    int32_t byteStreamMode = 0;
    if (format != NULL) {
        if (format->findInt32("is-byte-stream-mode", &byteStreamMode)) {
            if (byteStreamMode) {
                format->findInt32("max-input-size", &audioBufSize);
                AVLOGD("max-inp-size %d audio format %x", audioBufSize, audioFormat);
            }
        }
    }
    return audioBufSize;
}

#ifdef AAC_ADTS_OFFLOAD_ENABLED
struct aac_adts_format_conv_t {
        OMX_AUDIO_AACPROFILETYPE eAacProfileType;
        audio_format_t format;
};

static const struct aac_adts_format_conv_t profileLookup[] = {
    { OMX_AUDIO_AACObjectMain,        AUDIO_FORMAT_AAC_MAIN},
    { OMX_AUDIO_AACObjectLC,          AUDIO_FORMAT_AAC_ADTS_LC},
    { OMX_AUDIO_AACObjectSSR,         AUDIO_FORMAT_AAC_ADTS_SSR},
    { OMX_AUDIO_AACObjectLTP,         AUDIO_FORMAT_AAC_ADTS_LTP},
    { OMX_AUDIO_AACObjectHE,          AUDIO_FORMAT_AAC_ADTS_HE_V1},
    { OMX_AUDIO_AACObjectScalable,    AUDIO_FORMAT_AAC_ADTS_SCALABLE},
    { OMX_AUDIO_AACObjectERLC,        AUDIO_FORMAT_AAC_ADTS_ERLC},
    { OMX_AUDIO_AACObjectLD,          AUDIO_FORMAT_AAC_ADTS_LD},
    { OMX_AUDIO_AACObjectHE_PS,       AUDIO_FORMAT_AAC_ADTS_HE_V2},
    { OMX_AUDIO_AACObjectELD,         AUDIO_FORMAT_AAC_ADTS_ELD},
    { OMX_AUDIO_AACObjectNull,        AUDIO_FORMAT_AAC_ADTS},
};

bool ExtendedUtils::mapAACProfileToAudioFormat(const sp<MetaData> &meta,
                          audio_format_t& audioFormat,
                          uint64_t eAacProfile) {
    int32_t isADTS;
    if (meta->findInt32(kKeyIsADTS, &isADTS)) {
        const struct aac_adts_format_conv_t* p = &profileLookup[0];
        while (p->eAacProfileType != OMX_AUDIO_AACObjectNull) {
            if (eAacProfile == p->eAacProfileType) {
                audioFormat = p->format;
                return true;
            }
            ++p;
        }
        audioFormat = AUDIO_FORMAT_AAC_ADTS;
        return true;
    }
    return false;
}

bool ExtendedUtils::mapAACProfileToAudioFormat(const sp<AMessage> &format,
                          audio_format_t& audioFormat,
                          uint64_t eAacProfile) {

    int32_t isADTS;
    if (format->findInt32("is-adts", &isADTS)) {
        const struct aac_adts_format_conv_t* p = &profileLookup[0];
        while (p->eAacProfileType != OMX_AUDIO_AACObjectNull) {
            if (eAacProfile == p->eAacProfileType) {
                audioFormat = p->format;
                return true;
            }
            ++p;
        }
        audioFormat = AUDIO_FORMAT_AAC_ADTS;
        return true;
    }
    return false;
}
#else
bool ExtendedUtils::mapAACProfileToAudioFormat(const sp<MetaData> &,
                          audio_format_t& /*audioformat*/,
                          uint64_t /*eAacProfile*/) {
   return false;
}

bool ExtendedUtils::mapAACProfileToAudioFormat(const sp<AMessage> &,
                          audio_format_t& /*audioformat*/,
                          uint64_t /*eAacProfile*/) {
   return false;
}
#endif

DataSource::SnifferFunc ExtendedUtils::getExtendedSniffer() {
    return ExtendedExtractor::getExtendedSniffer();
}

bool ExtendedUtils::isEnhancedExtension(const char *extension) {
    const char *kEnhancedExtensions[] = {
        ".qcp", ".ac3", ".dts", ".wmv", ".ec3", ".mov", "flv",
        ".ape", ".oga", ".aif", ".aiff", ".wma", ".dsf", ".dff", ".dsd"
    };
    size_t kNumEnhancedExtensions =
        sizeof(kEnhancedExtensions) / sizeof(kEnhancedExtensions[0]);

    for (size_t i = 0; i < kNumEnhancedExtensions; ++i) {
        if (!strcasecmp(extension, kEnhancedExtensions[i])) {
            return true;
        }
    }

    return false;
}

void ExtendedUtils::extractCustomCameraKeys(
    const CameraParameters& params, sp<MetaData> &meta) {
    AVLOGV("extractCustomCameraKeys");

    // Camera pre-rotates video buffers. Width and Height of
    // of the image will be flipped if rotation is 90 or 270.
    // Encoder must be made aware of the flip in this case.
    const char *pRotation = params.get("video-rotation");
    int32_t preRotation = pRotation ? atoi(pRotation) : 0;
    bool flip = preRotation % 180;

    if (flip) {
        int32_t width = 0;
        int32_t height = 0;
        meta->findInt32(kKeyWidth, &width);
        meta->findInt32(kKeyHeight, &height);

        // width assigned to height is intentional
        meta->setInt32(kKeyWidth, height);
        meta->setInt32(kKeyStride, height);
        meta->setInt32(kKeyHeight, width);
        meta->setInt32(kKeySliceHeight, width);
    }

    int32_t batchSize = params.getInt("video-batch-size");
    if (batchSize > 0) {
        meta->setInt32(kKeyLocalBatchSize, batchSize);
    }
}

void ExtendedUtils::printFileName(int fd) {
    if (fd) {
        char symName[40] = {0};
        char fileName[256] = {0};
        snprintf(symName, sizeof(symName), "/proc/%d/fd/%d", getpid(), fd);

        if (readlink(symName, fileName, (sizeof(fileName) - 1)) != -1 ) {
            AVLOGI("printFileName fd(%d) -> %s", fd, fileName);
        }
    }
}

void ExtendedUtils::addDecodingTimesFromBatch(MediaBuffer *buf,
         List<int64_t>& decodeTimeQueue) {
    AVLOGV("addDecodingTimesFromBatch");

    if (buf == NULL) {
        return;
    }

    sp<EncoderMediaBuffer> b = EncoderMediaBuffer::Create(
            static_cast<VideoNativeHandleMetadata *>(buf->data()));
    if (b == NULL) {
        AVLOGV("Provided meta buffer is invalid!");
        return;
    } else if (b->count() == 1) {
        return;
    }

    int64_t baseTimeUs = 0ll;
    buf->meta_data()->findInt64(kKeyTime, &baseTimeUs);

    bool skipFirst = true;
    b->each([&](EncoderMediaBuffer::SubBuffer *sb) {
            // The first buffer's timestamp is inserted in MediaCodecSource
            if (skipFirst) {
                skipFirst = false;
                return;
            }

            // timestamp differences from Camera are in nano-seconds
            int64_t incrementalTimeUs = *sb->timestamp / 1E3,
                timeUs = baseTimeUs + incrementalTimeUs;

            AVLOGV("base timestamp %" PRId64 ", computed timestamp %" PRId64,
                baseTimeUs, timeUs);
            decodeTimeQueue.push_back(timeUs);

    });
}

template<class T>
static void InitOMXParams(T *params) {
    params->nSize = sizeof(T);
    params->nVersion.s.nVersionMajor = 1;
    params->nVersion.s.nVersionMinor = 0;
    params->nVersion.s.nRevision = 0;
    params->nVersion.s.nStep = 0;
}

#ifndef BRINGUP_WIP
bool ExtendedUtils::ExtendedHEVCMuxer::reassembleHEVCCSD(const AString &mime, sp<ABuffer> csd0, sp<MetaData> &meta) {
    if (mime.startsWith(MEDIA_MIMETYPE_VIDEO_HEVC)) {
        AVLOGV("writing HVCC key value pair");
        char hvcc[1024];
        void* reassembledHVCC = NULL;
        size_t reassembledHVCCBuffSize = 0;
        if (makeHEVCCodecSpecificData(csd0->data(), csd0->size(),
                    &reassembledHVCC, &reassembledHVCCBuffSize) == OK) {
            if (reassembledHVCC != NULL) {
                meta->setData(kKeyHVCC, kKeyHVCC, reassembledHVCC, reassembledHVCCBuffSize);
                free(reassembledHVCC);
            }
            return true;
        } else {
            AVLOGE("Failed to reassemble HVCC data");
        }
    }
    return false;
}
#endif
bool ExtendedUtils::isAudioMuxFormatSupported(const char *mime) {
    if (mime == NULL) {
        AVLOGE("NULL audio mime type");
        return false;
    }

    if (!strcasecmp(MEDIA_MIMETYPE_AUDIO_AMR_NB, mime) ||
        !strcasecmp(MEDIA_MIMETYPE_AUDIO_AMR_WB, mime) ||
        !strcasecmp(MEDIA_MIMETYPE_AUDIO_AAC, mime)) {
        return true;
    }
    return false;
}
#ifndef BRINGUP_WIP
void ExtendedUtils::ExtendedHEVCMuxer::getHEVCCodecSpecificDataFromInputFormatIfPossible(
        sp<MetaData> meta, void **codecSpecificData,
        size_t *codecSpecificDataSize, bool *gotAllCodecSpecificData) {
    uint32_t type;
    const void *data;
    size_t size;
    //kKeyHVCC needs to be populated
    if (getHEVCCodecConfigData(meta, &data, &size)) {
        *codecSpecificData = malloc(size);
        CHECK(*codecSpecificData != NULL);
        *codecSpecificDataSize = size;
        memcpy(*codecSpecificData, data, size);
        *gotAllCodecSpecificData = true;
    } else {
        AVLOGW("getHEVCCodecConfigData:: failed to find kKeyHvcc");
    }
}

bool ExtendedUtils::ExtendedHEVCMuxer::getHEVCCodecConfigData(
                          const sp<MetaData> &meta, const void **data,
                          size_t *size) {
    uint32_t type;
    AVLOGV("getHEVCCodecConfigData called");
    return meta->findData(kKeyHVCC, &type, data, size);
}

void ExtendedUtils::ExtendedHEVCMuxer::writeHEVCFtypBox(MPEG4Writer *writer) {
    AVLOGV("writeHEVCFtypBox called");
    writer->writeFourcc("3gp5");
    writer->writeInt32(0);
    writer->writeFourcc("hvc1");
    writer->writeFourcc("hev1");
    writer->writeFourcc("3gp5");
}

const char *ExtendedUtils::ExtendedHEVCMuxer::getFourCCForMime(const char *mime) {
    if (isVideoHEVC(mime)) {
        return "hvc1";
    } else {
        return NULL;
    }
}

void ExtendedUtils::ExtendedHEVCMuxer::writeHvccBox(MPEG4Writer *writer,
                                            void* codecSpecificData,
                                            size_t codecSpecificDataSize,
                                            bool useNalLengthFour) {
    AVLOGV("writeHvccBox called");
    CHECK(codecSpecificData);
    CHECK_GE(codecSpecificDataSize, 23);

    // Patch hvcc's lengthSize field to match the number
    // of bytes we use to indicate the size of a nal unit.
    uint8_t *ptr = (uint8_t *)codecSpecificData;
    ptr[21] = (ptr[21] & 0xfc) | (useNalLengthFour? 3 : 1);
    writer->beginBox("hvcC");
    writer->write(codecSpecificData, codecSpecificDataSize);
    writer->endBox();  // hvcC
}

bool ExtendedUtils::ExtendedHEVCMuxer::isVideoHEVC(const char* mime) {
    return (!strncasecmp(mime, MEDIA_MIMETYPE_VIDEO_HEVC,
                         strlen(MEDIA_MIMETYPE_VIDEO_HEVC)));
}

status_t ExtendedUtils::ExtendedHEVCMuxer::extractNALRBSPData(const uint8_t *data,
                                            size_t size,
                                            uint8_t **header,
                                            bool *alreadyFilled) {
    AVLOGV("extractNALRBSPData called");
    CHECK_GE(size, 2);

    uint8_t type = data[0] >> 1;
    type = 0x3f & type;

    //start parsing here
    size_t rbspSize = 0;
    uint8_t *rbspData = (uint8_t *) malloc(size);

    if (rbspData == NULL) {
        AVLOGE("allocation failed");
        return UNKNOWN_ERROR;
    }

    //populate rbsp data start from i+2, search for 0x000003,
    //and ignore emulation_prevention byte
    size_t itt = 2;
    while (itt < size) {
        if ((itt+2 < size) && (!memcmp("\x00\x00\x03", &data[itt], 3) )) {
            rbspData[rbspSize++] = data[itt++];
            rbspData[rbspSize++] = data[itt++];
            itt++;
        } else {
            rbspData[rbspSize++] = data[itt++];
        }
    }

    uint8_t maxSubLayerMinus1 = 0;

    //parser profileTierLevel
    if (type == kHEVCNalUnitTypeVidParamSet) { // if VPS
        AVLOGV("its VPS ... start with 5th byte");
        if (rbspSize < 5) {
            free(rbspData);
            return ERROR_MALFORMED;
        }

        maxSubLayerMinus1 = 0x0E & rbspData[1];
        maxSubLayerMinus1 = maxSubLayerMinus1 >> 1;
        parserProfileTierLevel(&rbspData[4], rbspSize - 4, header, alreadyFilled);

    } else if (type == kHEVCNalUnitTypeSeqParamSet) {
        AVLOGV("its SPS .. start with 2nd byte");
        if (rbspSize < 2) {
            free(rbspData);
            return ERROR_MALFORMED;
        }

        maxSubLayerMinus1 = 0x0E & rbspData[0];
        maxSubLayerMinus1 = maxSubLayerMinus1 >> 1;

        parserProfileTierLevel(&rbspData[1], rbspSize - 1, header, alreadyFilled);
    }
    free(rbspData);
    return OK;
}

status_t ExtendedUtils::ExtendedHEVCMuxer::parserProfileTierLevel(const uint8_t *data, size_t size,
                                                     uint8_t **header, bool *alreadyFilled) {
    CHECK_GE(size, 12);
    uint8_t *tmpHeader = *header;
    AVLOGV("parserProfileTierLevel called");
    uint8_t generalProfileSpace; //2 bit
    uint8_t generalTierFlag;     //1 bit
    uint8_t generalProfileIdc;   //5 bit
    uint8_t generalProfileCompatibilityFlag[4];
    uint8_t generalConstraintIndicatorFlag[6];
    uint8_t generalLevelIdc;     //8 bit

    // Need first 12 bytes

    // First byte will give below info
    generalProfileSpace = 0xC0 & data[0];
    generalProfileSpace = generalProfileSpace > 6;
    generalTierFlag = 0x20 & data[0];
    generalTierFlag = generalTierFlag > 5;
    generalProfileIdc = 0x1F & data[0];

    // Next 4 bytes is compatibility flag
    memcpy(&generalProfileCompatibilityFlag, &data[1], 4);

    // Next 6 bytes is constraint indicator flag
    memcpy(&generalConstraintIndicatorFlag, &data[5], 6);

    // Next 1 byte is general Level IDC
    generalLevelIdc = data[11];

    if (*alreadyFilled) {
        bool overwriteTierValue = false;

        //find profile space
        uint8_t prvGeneralProfileSpace; //2 bit
        prvGeneralProfileSpace = 0xC0 & tmpHeader[1];
        prvGeneralProfileSpace = prvGeneralProfileSpace > 6;
        //prev needs to be same as current
        if (prvGeneralProfileSpace != generalProfileSpace) {
            AVLOGW("Something wrong!!! profile space mismatch");
        }

        uint8_t prvGeneralTierFlag = 0x20 & tmpHeader[1];
        prvGeneralTierFlag = prvGeneralTierFlag > 5;

        if (prvGeneralTierFlag < generalTierFlag) {
            overwriteTierValue = true;
            AVLOGV("Found higher tier value, replacing old one");
        }

        uint8_t prvGeneralProfileIdc = 0x1F & tmpHeader[1];

        if (prvGeneralProfileIdc != generalProfileIdc) {
            AVLOGW("Something is wrong!!! profile space mismatch");
        }

        if (overwriteTierValue) {
            tmpHeader[1] = data[0];
        }

        //general level IDC should be set highest among all
        if (tmpHeader[12] < data[11]) {
            tmpHeader[12] = data[11];
            AVLOGV("Found higher level IDC value, replacing old one");
        }

    } else {
        *alreadyFilled = true;
        tmpHeader[1] = data[0];
        memcpy(&tmpHeader[2], &data[1], 4);
        memcpy(&tmpHeader[6], &data[5], 6);
        tmpHeader[12] = data[11];
    }

    char printCodecConfig[PROPERTY_VALUE_MAX];
    property_get("hevc.mux.print.codec.config", printCodecConfig, "0");

    if (atoi(printCodecConfig)) {
        //if property enabled, print these values
        AVLOGI("Start::-----------------");
        AVLOGI("generalProfileSpace = %2x", generalProfileSpace);
        AVLOGI("generalTierFlag     = %2x", generalTierFlag);
        AVLOGI("generalProfileIdc   = %2x", generalProfileIdc);
        AVLOGI("generalLevelIdc     = %2x", generalLevelIdc);
        AVLOGI("generalProfileCompatibilityFlag = %2x %2x %2x %2x",
                generalProfileCompatibilityFlag[0],
                generalProfileCompatibilityFlag[1],
                generalProfileCompatibilityFlag[2],
                generalProfileCompatibilityFlag[3]);
        AVLOGI("generalConstraintIndicatorFlag = %2x %2x %2x %2x %2x %2x",
                generalConstraintIndicatorFlag[0],
                generalConstraintIndicatorFlag[1],
                generalConstraintIndicatorFlag[2],
                generalConstraintIndicatorFlag[3],
                generalConstraintIndicatorFlag[4],
                generalConstraintIndicatorFlag[5]);
        AVLOGI("End::-----------------");
    }

    return OK;
}


static const uint8_t *findNextStartCode(
       const uint8_t *data, size_t length) {
    AVLOGV("findNextStartCode: %p %zu", data, length);

    size_t bytesLeft = length;

    while (bytesLeft > 4 &&
            memcmp("\x00\x00\x00\x01", &data[length - bytesLeft], 4)) {
        --bytesLeft;
    }

    if (bytesLeft <= 4) {
        bytesLeft = 0; // Last parameter set
    }

    return &data[length - bytesLeft];
}

const uint8_t *ExtendedUtils::ExtendedHEVCMuxer::parseHEVCParamSet(
        const uint8_t *data, size_t length, List<HEVCParamSet> &paramSetList, size_t *paramSetLen) {
    AVLOGV("parseHEVCParamSet called");
    const uint8_t *nextStartCode = findNextStartCode(data, length);
    *paramSetLen = nextStartCode - data;
    if (*paramSetLen == 0) {
        AVLOGE("Param set is malformed, since its length is 0");
        return NULL;
    }

    HEVCParamSet paramSet(*paramSetLen, data);
    paramSetList.push_back(paramSet);

    return nextStartCode;
}
#endif
static void getHEVCNalUnitType(uint8_t byte, uint8_t* type) {
    AVLOGV("getNalUnitType: %d", (int)byte);
    // nal_unit_type: 6-bit unsigned integer
    *type = (byte & 0x7E) >> 1;
}

#ifndef BRINGUP_WIP
size_t ExtendedUtils::ExtendedHEVCMuxer::parseHEVCCodecSpecificData(
        const uint8_t *data, size_t size,List<HEVCParamSet> &vidParamSet,
        List<HEVCParamSet> &seqParamSet, List<HEVCParamSet> &picParamSet ) {
    AVLOGV("parseHEVCCodecSpecificData called");
    // Data starts with a start code.
    // VPS, SPS and PPS are separated with start codes.
    uint8_t type = kHEVCNalUnitTypeVidParamSet;
    bool gotVps = false;
    bool gotSps = false;
    bool gotPps = false;
    const uint8_t *tmp = data;
    const uint8_t *nextStartCode = data;
    size_t bytesLeft = size;
    size_t paramSetLen = 0;
    size_t codecSpecificDataSize = 0;
    while (bytesLeft > 4 && !memcmp("\x00\x00\x00\x01", tmp, 4)) {
        getHEVCNalUnitType(*(tmp + 4), &type);
        if (type == kHEVCNalUnitTypeVidParamSet) {
            nextStartCode = parseHEVCParamSet(tmp + 4, bytesLeft - 4, vidParamSet, &paramSetLen);
            if (!gotVps) {
                gotVps = true;
            }
        } else if (type == kHEVCNalUnitTypeSeqParamSet) {
            nextStartCode = parseHEVCParamSet(tmp + 4, bytesLeft - 4, seqParamSet, &paramSetLen);
            if (!gotSps) {
                gotSps = true;
            }

        } else if (type == kHEVCNalUnitTypePicParamSet) {
            nextStartCode = parseHEVCParamSet(tmp + 4, bytesLeft - 4, picParamSet, &paramSetLen);
            if (!gotPps) {
                gotPps = true;
            }
        } else {
            AVLOGE("Only VPS, SPS and PPS Nal units are expected");
            return ERROR_MALFORMED;
        }

        if (nextStartCode == NULL) {
            AVLOGE("Next start code is NULL");
            return ERROR_MALFORMED;
        }

        // Move on to find the next parameter set
        bytesLeft -= nextStartCode - tmp;
        tmp = nextStartCode;
        codecSpecificDataSize += (2 + paramSetLen);
    }

#if 0
//not adding this check now, but might be needed
    if (!gotVps || !gotVps || !gotVps ) {
        return 0;
    }
#endif

    return codecSpecificDataSize;
}

status_t ExtendedUtils::ExtendedHEVCMuxer::makeHEVCCodecSpecificData(
                         const uint8_t *data, size_t size, void** codecSpecificData,
                         size_t *codecSpecificDataSize) {
    AVLOGV("makeHEVCCodecSpecificData called");

    if (*codecSpecificData != NULL) {
        AVLOGE("Already have codec specific data");
        return ERROR_MALFORMED;
    }

    if (size < 4) {
        AVLOGE("Codec specific data length too short: %zu", size);
        return ERROR_MALFORMED;
    }

    // Data is in the form of HVCCodecSpecificData
    if (memcmp("\x00\x00\x00\x01", data, 4)) {
        // 23 byte fixed header
        if (size < 23) {
            AVLOGE("Codec specific data length too short: %zu", size);
            return ERROR_MALFORMED;
        }

        *codecSpecificData = malloc(size);

        if (*codecSpecificData != NULL) {
            *codecSpecificDataSize = size;
            memcpy(*codecSpecificData, data, size);
            return OK;
        }

        return NO_MEMORY;
    }

    List<HEVCParamSet> vidParamSets;
    List<HEVCParamSet> seqParamSets;
    List<HEVCParamSet> picParamSets;

    if ((*codecSpecificDataSize = parseHEVCCodecSpecificData(data, size,
                                   vidParamSets, seqParamSets, picParamSets)) <= 0) {
        AVLOGE("cannot parser codec specific data, bailing out");
        return ERROR_MALFORMED;
    }

    size_t numOfNALArray = 0;
    bool doneWritingVPS = true, doneWritingSPS = true, doneWritingPPS = true;

    if (!vidParamSets.empty()) {
        doneWritingVPS = false;
        ++numOfNALArray;
    }

    if (!seqParamSets.empty()) {
       doneWritingSPS = false;
       ++numOfNALArray;
    }

    if (!picParamSets.empty()) {
       doneWritingPPS = false;
       ++numOfNALArray;
    }

    //additional 23 bytes needed (22 bytes for hvc1 header + 1 byte for number of arrays)
    *codecSpecificDataSize += 23;
    //needed 3 bytes per NAL array
    *codecSpecificDataSize += 3 * numOfNALArray;

    int count = 0;
    void *codecConfigData = malloc(*codecSpecificDataSize);
    if (codecConfigData == NULL) {
        AVLOGE("Failed to allocate memory, bailing out");
        return NO_MEMORY;
    }

    uint8_t *header = (uint8_t *)codecConfigData;
    // 8  - bit version
    header[0]  = 1;
    //Profile space 2 bit, tier flag 1 bit and profile IDC 5 bit
    header[1]  = 0x00;
    // 32 - bit compatibility flag
    header[2]  = 0x00;
    header[3]  = 0x00;
    header[4]  = 0x00;
    header[5]  = 0x00;
    // 48 - bit general constraint indicator flag
    header[6]  = header[7]  = header[8]  = 0x00;
    header[9]  = header[10] = header[11] = 0x00;
    // 8  - bit general IDC level
    header[12] = 0x00;
    // 4  - bit reserved '1111'
    // 12 - bit spatial segmentation idc
    header[13] = 0xf0;
    header[14] = 0x00;
    // 6  - bit reserved '111111'
    // 2  - bit parallelism Type
    header[15] = 0xfc;
    // 6  - bit reserved '111111'
    // 2  - bit chromaFormat
    header[16] = 0xfc;
    // 5  - bit reserved '11111'
    // 3  - bit DepthLumaMinus8
    header[17] = 0xf8;
    // 5  - bit reserved '11111'
    // 3  - bit DepthChromaMinus8
    header[18] = 0xf8;
    // 16 - bit average frame rate
    header[19] = header[20] = 0x00;
    // 2  - bit constant frame rate
    // 3  - bit num temporal layers
    // 1  - bit temoral nested
    // 2  - bit lengthSizeMinusOne
    header[21] = 0x07;

    // 8-bit number of NAL types
    header[22] = (uint8_t)numOfNALArray;

    header += 23;
    count  += 23;

    bool ifProfileIDCAlreadyFilled = false;

    if (!doneWritingVPS) {
        doneWritingVPS = true;
        AVLOGV("Writing VPS");
        //8-bit, last 6 bit for NAL type
        header[0] = 0x20; // NAL type is VPS
        //16-bit, number of nal Units
        uint16_t vidParamSetLength = vidParamSets.size();
        header[1] = vidParamSetLength >> 8;
        header[2] = vidParamSetLength & 0xff;

        header += 3;
        count  += 3;

        for (List<HEVCParamSet>::iterator it = vidParamSets.begin();
            it != vidParamSets.end(); ++it) {
            // 16-bit video parameter set length
            uint16_t vidParamSetLength = it->mLength;
            header[0] = vidParamSetLength >> 8;
            header[1] = vidParamSetLength & 0xff;

            extractNALRBSPData(it->mData, it->mLength,
                               (uint8_t **)&codecConfigData,
                               &ifProfileIDCAlreadyFilled);

            // VPS NAL unit (video parameter length bytes)
            memcpy(&header[2], it->mData, vidParamSetLength);
            header += (2 + vidParamSetLength);
            count  += (2 + vidParamSetLength);
        }
    }

    if (!doneWritingSPS) {
        doneWritingSPS = true;
        AVLOGV("Writting SPS");
        //8-bit, last 6 bit for NAL type
        header[0] = 0x21; // NAL type is SPS
        //16-bit, number of nal Units
        uint16_t seqParamSetLength = seqParamSets.size();
        header[1] = seqParamSetLength >> 8;
        header[2] = seqParamSetLength & 0xff;

        header += 3;
        count  += 3;

        for (List<HEVCParamSet>::iterator it = seqParamSets.begin();
              it != seqParamSets.end(); ++it) {
            // 16-bit sequence parameter set length
            uint16_t seqParamSetLength = it->mLength;

            // 16-bit number of NAL units of this type
            header[0] = seqParamSetLength >> 8;
            header[1] = seqParamSetLength & 0xff;

            extractNALRBSPData(it->mData, it->mLength,
                               (uint8_t **)&codecConfigData,
                               &ifProfileIDCAlreadyFilled);

            // SPS NAL unit (sequence parameter length bytes)
            memcpy(&header[2], it->mData, seqParamSetLength);
            header += (2 + seqParamSetLength);
            count  += (2 + seqParamSetLength);
        }
    }

    if (!doneWritingPPS) {
        doneWritingPPS = true;
        AVLOGV("writing PPS");
        //8-bit, last 6 bit for NAL type
        header[0] = 0x22; // NAL type is PPS
        //16-bit, number of nal Units
        uint16_t picParamSetLength = picParamSets.size();
        header[1] = picParamSetLength >> 8;
        header[2] = picParamSetLength & 0xff;

        header += 3;
        count  += 3;

        for (List<HEVCParamSet>::iterator it = picParamSets.begin();
             it != picParamSets.end(); ++it) {
            // 16-bit picture parameter set length
            uint16_t picParamSetLength = it->mLength;
            header[0] = picParamSetLength >> 8;
            header[1] = picParamSetLength & 0xff;

            // PPS Nal unit (picture parameter set length bytes)
            memcpy(&header[2], it->mData, picParamSetLength);
            header += (2 + picParamSetLength);
            count  += (2 + picParamSetLength);
        }
    }
    *codecSpecificData = codecConfigData;
    return OK;
}
#endif

void ExtendedUtils::cacheCaptureBuffers(sp<hardware::ICamera> camera, video_encoder encoder) {
    if (camera != NULL) {
        char propertyValue[PROPERTY_VALUE_MAX] = {0};
        const char *value = "disable";
        property_get("ro.board.platform", propertyValue, "0");
        if (!strncmp(propertyValue, "msm8909", 7) ||
            !strncmp(propertyValue, "msm8937", 7)) {
            if (encoder == VIDEO_ENCODER_H263 ||
                encoder == VIDEO_ENCODER_MPEG_4_SP) {
                value = "enable";
            }
        } else if (property_get("media.msm8956hw", propertyValue, "0") &&
                atoi(propertyValue)) {
            if (encoder == VIDEO_ENCODER_MPEG_4_SP) {
                value = "enable";
            }
        }

        int64_t token = IPCThreadState::self()->clearCallingIdentity();
        String8 s = camera->getParameters();
        CameraParameters params(s);
        AVLOGV("%s: caching %s", __func__, value);
        params.set("cache-video-buffers", value);
        if (camera->setParameters(params.flatten()) != OK) {
            AVLOGE("Failed to set parameter - cache-video-buffers");
        }
        IPCThreadState::self()->restoreCallingIdentity(token);
    }
}

const char *ExtendedUtils::getCustomCodecsLocation() {
    char value[PROPERTY_VALUE_MAX] = {0};

    if (property_get("media.msm8939hw", value, "0") &&
        atoi(value)) {
        return "/etc/media_codecs_8939.xml";
    } else if (property_get("media.msm8956hw", value, "0") &&
               atoi(value)) {
        // for msm8956 version v1 parse 8956_v1.xml which has vp9 decoder
        if (property_get("media.msm8956.version", value, "0") &&
            (atoi(value) == 1)) {
            return "/etc/media_codecs_8956_v1.xml";
        } else {
            return "/etc/media_codecs_8956.xml";
        }
    }
    return "/etc/media_codecs.xml";
}

void ExtendedUtils::setIntraPeriod(
        int nPFrames, int nBFrames, const sp<IOMX> OMXhandle,
        IOMX::node_id nodeID) {
    QOMX_VIDEO_INTRAPERIODTYPE intraperiod;
    InitOMXParams(&intraperiod);

    intraperiod.nPortIndex = 1;  // kPortIndexOutput
    intraperiod.nIDRPeriod = 1;
    intraperiod.nPFrames = nPFrames - 1;
    intraperiod.nBFrames = nBFrames;
    OMXhandle->setConfig(
        nodeID, (OMX_INDEXTYPE)QOMX_IndexConfigVideoIntraperiod, &intraperiod, sizeof(intraperiod));
}

const char *ExtendedUtils::getCustomCodecsPerformanceLocation() {
    char value[PROPERTY_VALUE_MAX] = {0};

    if (property_get("media.msm8956hw", value, "0") &&
        atoi(value)) {
        if (property_get("media.msm8956.version", value, "0") &&
            (atoi(value) == 1)) {
            return "/etc/media_codecs_performance_8956_v1.xml";
        } else {
            return "/etc/media_codecs_performance_8956.xml";
        }
    }else if (property_get("media.msm8939hw", value, "0") &&
               atoi(value)) {
            return "/etc/media_codecs_performance_8939.xml";
    }
    return "/etc/media_codecs_performance.xml";
}

bool ExtendedUtils::IsHevcIDR(const sp<ABuffer>& buffer) {
    const uint8_t *data = buffer->data();
    size_t size = buffer->size();

    bool foundRef = false;
    const uint8_t *nalStart;
    size_t nalSize;
    while (!foundRef && getNextNALUnit(&data, &size, &nalStart, &nalSize, true) == OK) {
        if (nalSize == 0) {
            AVLOGW("Encountered zero-length HEVC NAL");
            return false;
        }

        uint8_t nalType;
        getHEVCNalUnitType(nalStart[0], &nalType);

        switch(nalType) {
        case kHEVCNalUnitTypeIDR:
        case kHEVCNalUnitTypeIDRNoLP:
        case kHEVCNalUnitTypeCRA:
            foundRef = true;
            break;
        }
    }

    return foundRef;
}

} //namespace android

