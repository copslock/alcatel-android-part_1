/*
 * Copyright (c) 2015-2016, Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 *
 * Not a Contribution.
 * Apache license notifications and license are retained
 * for attribution purposes only.
 */
/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef BRINGUP_WIP
//#define LOG_NDEBUG 0
#define LOG_TAG "ExtendedCachedSource"

#include "stagefright/ExtendedCachedSource.h"
#include <common/AVLog.h>
#include <media/stagefright/foundation/AMessage.h>

namespace android {

ExtendedCachedSource::ExtendedCachedSource(
        const sp<DataSource> &source,
        const char *cacheConfig,
        bool disconnectAtHighwatermark)
          : NuCachedSource2(source, cacheConfig, disconnectAtHighwatermark) {
    AVLOGV("ExtendedCachedSource()");
}

// static
sp<ExtendedCachedSource> ExtendedCachedSource::Create(
        const sp<DataSource> &source,
        const char *cacheConfig,
        bool disconnectAtHighwatermark) {
    sp<ExtendedCachedSource> instance = new ExtendedCachedSource(
            source, cacheConfig, disconnectAtHighwatermark);
    Mutex::Autolock autoLock(instance->mLock);
    (new AMessage(kWhatFetchMore, instance->mReflector))->post();
    return instance;
}

void ExtendedCachedSource::suspendCaching() {
    AVLOGV("stopCaching");
    mLooper->stop();
}

void ExtendedCachedSource::resumeCaching(const sp<DataSource> &source) {
    AVLOGV("resumeCaching");
    mSource = source;
    mDisconnecting = false;
    mFetching = true;
    mFinalStatus = OK;
    mNumRetriesLeft = kMaxNumRetries;
    mLooper->start(false /* runOnCallingThread */, true /* canCallJava */);
}

} // namespace android
#endif
