/*
 * Copyright (c) 2015-2016, Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 *
 * Not a Contribution.
 * Apache license notifications and license are retained
 * for attribution purposes only.
 */
/*
 * Copyright (c) 2013 - 2015, The Linux Foundation. All rights reserved.
 */
/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


//#define LOG_NDEBUG 0
#define LOG_TAG "ExtendedAudioSource"
#include <common/AVLog.h>

#include <media/stagefright/AudioSource.h>
#include "ExtendedAudioSource.h"
#include <common/AVLog.h>
#include <cutils/properties.h>
#include <system/audio.h>

#define AUDIO_RECORD_DEFAULT_BUFFER_DURATION 20

namespace android {

static void AudioRecordCallbackFunction(int event, void *user, void *info) {
    ExtendedAudioSource *source = (ExtendedAudioSource *) user;
    source->onEvent(event, info);
}

ExtendedAudioSource::ExtendedAudioSource(
            audio_source_t inputSource,
            const String16 &opPackageName,
            uint32_t sampleRate,
            uint32_t channels,
            uint32_t outSampleRate,
	    uid_t clientUid,
	    pid_t clientPid)
    : AudioSource(inputSource, opPackageName, sampleRate,
                    channels, outSampleRate,clientUid, clientPid)
{
    audioRecordInit(inputSource, opPackageName, channels);
}

ExtendedAudioSource::~ExtendedAudioSource() {
    if (mStarted) {
        reset();
    }
    //destroy mRecord explicitly before freeing mTempBuf
    mRecord.clear();
    if (mTransferMode == AudioRecord::TRANSFER_SYNC) {
        if (mTempBuf.i16) {
            free(mTempBuf.i16);
            mTempBuf.i16 = (short*)NULL;
        }
    }
}


status_t ExtendedAudioSource::reset() {
    status_t ret = AudioSource::reset();
    if (ret == OK && mTransferMode == AudioRecord::TRANSFER_SYNC) {
        if (mAudioSessionId != -1) {
            AudioSystem::releaseAudioSessionId(mAudioSessionId, -1);
        }

        mAudioSessionId = (audio_session_t)-1;
        mTempBuf.size = 0;
        mTempBuf.frameCount = 0;
    }
    return ret;
}

void ExtendedAudioSource::onEvent(int event, void* info) {

    switch (event) {
        case AudioRecord::EVENT_MORE_DATA: {
            AudioSource::dataCallback(*((AudioRecord::Buffer *) info));
            break;
        }
        case AudioRecord::EVENT_NEW_POS: {
            uint32_t position = 0;
            mRecord->getPosition(&position);
            size_t framestoRead = position - mPrevPosition;
            size_t bytestoRead = (framestoRead * 2 * mRecord->channelCount());
            if (bytestoRead <=0 || bytestoRead > mAllocBytes) {
                //try to read only max
                AVLOGI("greater than allocated size in callback, adjusting size");
                bytestoRead =  mAllocBytes;
                framestoRead = (mAllocBytes / (2 * mRecord->channelCount()));
            }

            if (mTempBuf.i16 && framestoRead > 0) {
                //read only if you have valid data
                size_t bytesRead = mRecord->read(mTempBuf.i16, bytestoRead);
                size_t framesRead = 0;
                AVLOGV("event_new_pos, new pos %d, frames to read %zu",
                        position, framestoRead);
                AVLOGV("bytes read = %zu", bytesRead);
                if (bytesRead > 0) {
                    framesRead = (bytesRead / (2 * mRecord->channelCount()));
                    mPrevPosition += framesRead;
                    mTempBuf.size = bytesRead;
                    mTempBuf.frameCount = framesRead;
                    AudioSource::dataCallback(mTempBuf);
                } else {
                    AVLOGE("EVENT_NEW_POS did not return any data");
                }
            } else {
                AVLOGE("Init error");
            }
            break;
        }
        case AudioRecord::EVENT_OVERRUN: {
            AVLOGW("AudioRecord reported overrun!");
            break;
        }
        default:
            // does nothing
            break;
    }
    return;
}

void ExtendedAudioSource::audioRecordInit(audio_source_t inputSource, const String16 &opPackageName, uint32_t channels) {
    mTempBuf.size = 0;
    mTempBuf.frameCount = 0;
    mTempBuf.i16 = (short*)NULL;
    mPrevPosition = 0;
    mAudioSessionId = (audio_session_t)-1;
    mAllocBytes = 0;
    mTransferMode = AudioRecord::TRANSFER_CALLBACK;
    int buffDuration = AUDIO_RECORD_DEFAULT_BUFFER_DURATION;
    char propValue[PROPERTY_VALUE_MAX];
    if (property_get("audio.record.aggregate.buffer.duration", propValue, NULL) &&
            (atoi(propValue) > AUDIO_RECORD_DEFAULT_BUFFER_DURATION)) {
        buffDuration = atoi(propValue);
    }

    bool bAggregate  = ((channels == 1) || (channels == 2)) && (inputSource == AUDIO_SOURCE_CAMCORDER);
    // make sure that the AudioRecord callback never returns more than the maximum
    // buffer size
    if(bAggregate){
        mMaxBufferSize = 2 * channels * mSampleRate * audio_bytes_per_sample(AUDIO_FORMAT_PCM_16_BIT) *
            buffDuration /1000;
    }

    uint32_t frameCount = (bAggregate ? mMaxBufferSize : kMaxBufferSize) / sizeof(int16_t) / channels;

    size_t minFrameCount;
    status_t status = AudioRecord::getMinFrameCount(&minFrameCount,
                                           mSampleRate,
                                           AUDIO_FORMAT_PCM_16_BIT,
                                           audio_channel_in_mask_from_count(channels));

    // make sure that the AudioRecord total buffer size is large enough
    size_t bufCount = 2;
    while ((bufCount * frameCount) < minFrameCount) {
        bufCount++;
    }
    //decide whether to use callback or event pos callback
    //use position marker only for mono or stereo capture
    //and if input source is camera
    if (bAggregate) {

        //Need audioSession Id in the extended audio record constructor
        //where the transfer mode can be specified
        mAudioSessionId = (audio_session_t) AudioSystem::newAudioUniqueId(AUDIO_UNIQUE_ID_USE_SESSION);
        AudioSystem::acquireAudioSessionId(mAudioSessionId, -1);
        mRecord = new AudioRecord(
                    inputSource, mSampleRate, AUDIO_FORMAT_PCM_16_BIT,
                    audio_channel_in_mask_from_count(channels),
                    opPackageName,
                    (size_t) (bufCount * frameCount),
                    AudioRecordCallbackFunction,
                    this,
                    frameCount /*notificationFrames*/,
                    mAudioSessionId,
                    AudioRecord::TRANSFER_SYNC,
                    AUDIO_INPUT_FLAG_NONE);
        mInitCheck = mRecord->initCheck();
        if (mInitCheck != OK) {
            mRecord.clear();
        } else {

            /* set to update position after frames worth of buffduration
               time for 16 bits */
            mAllocBytes = ((sizeof(uint8_t) * frameCount * 2 * channels));
            AVLOGD("AudioSource in TRANSFER_SYNC with duration =%d ms bufCount = %zu frameCount = %d mMaxBufferSize = %zu",
                                                          buffDuration, bufCount, frameCount, mMaxBufferSize);
            mTempBuf.i16 = (short*) malloc(mAllocBytes);
            mTransferMode = AudioRecord::TRANSFER_SYNC;
            mRecord->setPositionUpdatePeriod((mSampleRate * buffDuration)/1000);
        }
    }
    if (mInitCheck != OK)
    {
        mRecord = new AudioRecord(
            inputSource, mSampleRate, AUDIO_FORMAT_PCM_16_BIT,
            audio_channel_in_mask_from_count(channels),
            opPackageName,
            (size_t) (bufCount * frameCount),
            AudioRecordCallbackFunction,
            this,
            frameCount /*notificationFrames*/);
    }
}

}
