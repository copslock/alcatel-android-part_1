/*
* Copyright (c) 2015, Qualcomm Technologies, Inc.
* All Rights Reserved.
* Confidential and Proprietary - Qualcomm Technologies, Inc.
*
* Not a Contribution.
* Apache license notifications and license are retained
* for attribution purposes only.
*
* Copyright (C) 2009 The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*       http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*
*
*/

#define LOG_TAG "ExtendedMPEG4Writer"

#include <media/stagefright/MediaBuffer.h>
#include <media/stagefright/foundation/ADebug.h>
#include <media/stagefright/foundation/AUtils.h>
#include "ExtendedMPEG4Writer.h"

namespace android {
ExtendedMPEG4Writer::ExtendedMPEG4Writer(int fd) :
    MPEG4Writer(fd) {

}

off64_t ExtendedMPEG4Writer::addLengthPrefixedSample_l(MediaBuffer *buffer) {
    off64_t oldOffset = mOffset;

    MediaBuffer *copy = new MediaBuffer(buffer->data(), buffer->size());
    copy->set_range(buffer->range_offset(), buffer->range_length());

    struct {
        const char *startCode;
        size_t length;
    } startCodes[] = {
        {"\x00\x01", 2},
        {"\x00\x00\x00\x01", 4}
    }, *sc = &startCodes[mUse4ByteNalLength];

    /*
     * Only consider kMaxBytesToSearch since we're mainly looking out of small
     * NALUs like AUD or SEI, which are expected to be at the top of the buffer.
     */
    const size_t kMaxBytesToSearch = 64;

    /* Walk thru the buffer and look for start codes */
    while (void *accessUnitPtr = memmem(static_cast<char *>(copy->data()) + copy->range_offset(),
                min(kMaxBytesToSearch, copy->range_length()), sc->startCode, sc->length)) {
        size_t accessUnitLen = static_cast<char *>(accessUnitPtr) -
            (static_cast<char *>(copy->data()) + copy->range_offset());
        MediaBuffer *accessUnit = new MediaBuffer(copy->data(), copy->size());

        accessUnit->set_range(copy->range_offset(), accessUnitLen);
        MPEG4Writer::addLengthPrefixedSample_l(accessUnit);
        accessUnit->release();

        copy->set_range(copy->range_offset() + accessUnitLen,
                copy->range_length() - accessUnitLen);
        CHECK_GT(copy->range_length(), sc->length);
        StripStartcode(copy);
    }

    if (copy->range_length() > 0)
        MPEG4Writer::addLengthPrefixedSample_l(copy);

    copy->release();

    return oldOffset;
}

}
