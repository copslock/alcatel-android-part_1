/*
 * Copyright (c) 2015-2016, Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 *
 * Not a Contribution.
 * Apache license notifications and license are retained
 * for attribution purposes only.
 */
/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define LOG_TAG "ExtendedPlaylistFetcher"

#include <common/AVLog.h>
#include <media/stagefright/foundation/ADebug.h>
#include <cutils/properties.h>

#include "M3UParser.h"
#include "ExtendedPlaylistFetcher.h"
#include "ExtendedLiveSession.h"

namespace android {

ExtendedPlaylistFetcher::ExtendedPlaylistFetcher(
        const sp<AMessage> &notify,
        const sp<LiveSession> &session,
        const char *uri,
        int32_t id,
        int32_t subtitleGeneration,
        bool downloadFirstTs)
    : PlaylistFetcher(notify, session, uri, id, subtitleGeneration),
      mIsFirstTsDownload(downloadFirstTs) {
    AVLOGV("Extended Playlist Fetcher created - %d", mIsFirstTsDownload);
}

ExtendedPlaylistFetcher::~ExtendedPlaylistFetcher() {
    AVLOGV("~ExtendedPlaylistFetcher");
}

bool ExtendedPlaylistFetcher::checkSwitchBandwidth() {
    if (mIsFirstTsDownload) {
        mIsFirstTsDownload = false;
        if (static_cast<ExtendedLiveSession *>(mSession.get())->switchToRealBandwidth()) {
            AVLOGV("change bandwidth at the start of first playback, don't download ts anymore");
            return true;
        }
    }
    return false;
}

bool ExtendedPlaylistFetcher::getSeqNumberInLiveStreaming(){
    int32_t targetDurationSecs;
    int64_t durationUsSecs;
    int32_t diff;
    sp<AMessage> itemMeta;
    int32_t mSeqSize;
    int32_t mIndex=0;
    int32_t totalDuration = 0;
    char value[PROPERTY_VALUE_MAX];
    int32_t firstSeqNumber = 0, lastSeqNumber = 0;

    if (mPlaylist != NULL) {
        mPlaylist->getSeqNumberRange(
                &firstSeqNumber, &lastSeqNumber);
    }

    if (property_get("persist.media.hls.start_SeqNo", value, "0")
            && atoi(value)) {
        CHECK(mPlaylist->meta()->findInt32("target-duration", &targetDurationSecs));
        AVLOGV("getSeqNumberInLiveStreaming target-duration= %d",
                targetDurationSecs);
        for (int32_t i = lastSeqNumber - firstSeqNumber; i > 0; i--) {
            AVLOGV("getSeqNumberInLiveStreaming, i = %d, lastSeqNumber = %d,"
                " firstSeqNumber = %d", i, lastSeqNumber, firstSeqNumber);
            CHECK(mPlaylist->itemAt(i, NULL, &itemMeta));
            CHECK(itemMeta->findInt64("durationUs", &durationUsSecs));
            durationUsSecs = durationUsSecs / 1000000ll;
            totalDuration  += durationUsSecs;
            mIndex++;
            AVLOGV("getSeqNumberInLiveStreaming, totalDuration = %d,"
                " durationUsSecs = %" PRId64 ", mIndex = %d",
                totalDuration, durationUsSecs, mIndex);
            if (totalDuration > 3 * targetDurationSecs) {
                break;
            }
        }
        mSeqNumber = lastSeqNumber - mIndex + 1;

    } else {
        mSeqNumber = lastSeqNumber - 3;
    }
    return true;
}



} // namespace android
