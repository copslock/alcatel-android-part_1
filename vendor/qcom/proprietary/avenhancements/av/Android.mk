LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_COPY_HEADERS_TO := avenhancements
LOCAL_COPY_HEADERS := common/AVMetaData.h
include $(BUILD_COPY_HEADERS)

LOCAL_SRC_FILES += common/AVLog.cpp
LOCAL_SRC_FILES += common/EncoderMediaBuffer.cpp

LOCAL_SRC_FILES += media/ExtendedMediaUtils.cpp
LOCAL_SRC_FILES += media/ExtendedMediaRecorder.cpp

LOCAL_SRC_FILES += stagefright/ExtendedFactory.cpp
LOCAL_SRC_FILES += stagefright/ExtendedUtils.cpp
LOCAL_SRC_FILES += stagefright/ExtendedACodec.cpp
LOCAL_SRC_FILES += stagefright/ExtendedExtractor.cpp
LOCAL_SRC_FILES += stagefright/ExtendedCachedSource.cpp
LOCAL_SRC_FILES += stagefright/ExtendedAudioSource.cpp
LOCAL_SRC_FILES += stagefright/ExtendedCameraSource.cpp
LOCAL_SRC_FILES += stagefright/ExtendedLiveSession.cpp
LOCAL_SRC_FILES += stagefright/ExtendedPlaylistFetcher.cpp
LOCAL_SRC_FILES += stagefright/ExtendedCameraSourceTimeLapse.cpp
LOCAL_SRC_FILES += stagefright/ExtendedMPEG4Writer.cpp
LOCAL_SRC_FILES += stagefright/ExtendedESQueue.cpp

LOCAL_SRC_FILES += mediaplayerservice/ExtendedNuFactory.cpp
LOCAL_SRC_FILES += mediaplayerservice/ExtendedNuPlayer.cpp
LOCAL_SRC_FILES += mediaplayerservice/ExtendedNuUtils.cpp
LOCAL_SRC_FILES += mediaplayerservice/ExtendedNuPlayerDecoderPassThrough.cpp
LOCAL_SRC_FILES += mediaplayerservice/ExtendedNuPlayerDecoder.cpp
LOCAL_SRC_FILES += mediaplayerservice/ExtendedNuPlayerRenderer.cpp
LOCAL_SRC_FILES += mediaplayerservice/ExtendedServiceFactory.cpp
LOCAL_SRC_FILES += mediaplayerservice/ExtendedSFRecorder.cpp
LOCAL_SRC_FILES += mediaplayerservice/ExtendedServiceUtils.cpp
LOCAL_SRC_FILES += mediaplayerservice/ExtendedARTSPConnection.cpp
LOCAL_SRC_FILES += mediaplayerservice/ExtendedARTPConnection.cpp
LOCAL_SRC_FILES += mediaplayerservice/ExtendedGenericSource.cpp
LOCAL_SRC_FILES += mediaplayerservice/ExtendedWriter.cpp
LOCAL_SRC_FILES += mediaplayerservice/WAVEWriter.cpp
LOCAL_SRC_FILES += mediaplayerservice/ExtendedHTTPLiveSource.cpp

LOCAL_C_INCLUDES:= \
        $(TOP)/frameworks/av/include/media/ \
        $(TOP)/frameworks/av/media/libavextensions \
        $(TOP)/frameworks/av/media/libmediaplayerservice \
        $(TOP)/frameworks/av/media/libstagefright \
        $(TOP)/frameworks/av/media/libstagefright/include \
        $(TOP)/frameworks/av/media/libstagefright/mpeg2ts \
        $(TOP)/frameworks/av/media/libstagefright/httplive \
        $(TOP)/frameworks/native/include/media/hardware \
        $(TOP)/frameworks/native/include/media/openmax \
        $(TOP)/system/media/audio/include/system/ \
        $(TOP)/external/flac/include \
        $(TOP)/hardware/qcom/media/mm-core/inc \
        $(TOP)/hardware/qcom/media/libstagefrighthw \
        $(TOP)/system/media/audio_utils/include

LOCAL_SHARED_LIBRARIES := \
        libbinder \
        libcamera_client \
        libcutils \
        libdl \
        libexpat \
        liblog \
        libutils \
        libz \
        libmedia \
        libstagefright \
        libstagefright_httplive \
        libmediaplayerservice \
        libstagefright_foundation \
        libaudioutils \

#QTI Decoder/offload flags
ifeq ($(call is-vendor-board-platform,QCOM),true)
LOCAL_C_INCLUDES += $(TARGET_OUT_HEADERS)/mm-audio
ifeq ($(strip $(AUDIO_FEATURE_ENABLED_EXTN_FLAC_DECODER)),true)
LOCAL_CFLAGS := -DQTI_FLAC_DECODER
endif
ifeq ($(strip $(AUDIO_FEATURE_ENABLED_FLAC_OFFLOAD)),true)
LOCAL_CFLAGS += -DFLAC_OFFLOAD_ENABLED
endif
ifeq ($(strip $(AUDIO_FEATURE_ENABLED_ALAC_OFFLOAD)),true)
LOCAL_CFLAGS += -DALAC_OFFLOAD_ENABLED
endif
ifeq ($(strip $(AUDIO_FEATURE_ENABLED_WMA_OFFLOAD)), true)
LOCAL_CFLAGS += -DWMA_OFFLOAD_ENABLED
endif
ifeq ($(strip $(AUDIO_FEATURE_ENABLED_APE_OFFLOAD)),true)
LOCAL_CFLAGS += -DAPE_OFFLOAD_ENABLED
endif
ifeq ($(strip $(AUDIO_FEATURE_ENABLED_VORBIS_OFFLOAD)),true)
LOCAL_CFLAGS += -DVORBIS_OFFLOAD_ENABLED
endif
endif

LOCAL_CFLAGS += -Wno-multichar -Werror

ifeq ($(strip $(AUDIO_FEATURE_ENABLED_PCM_OFFLOAD)),true)
   LOCAL_CFLAGS += -DPCM_OFFLOAD_ENABLED_16
endif

ifeq ($(strip $(AUDIO_FEATURE_ENABLED_PCM_OFFLOAD_24)),true)
   LOCAL_CFLAGS += -DPCM_OFFLOAD_ENABLED_24
endif

ifeq ($(strip $(AUDIO_FEATURE_ENABLED_AAC_ADTS_OFFLOAD)),true)
   LOCAL_CFLAGS += -DAAC_ADTS_OFFLOAD_ENABLED
endif

ENABLE_HLS_AUDIO_ONLY_IMG_DISPLAY := true
ifeq ($(ENABLE_HLS_AUDIO_ONLY_IMG_DISPLAY), true)
LOCAL_CFLAGS += -DHLS_AUDIO_ONLY_IMG_DISPLAY
LOCAL_C_INCLUDES += $(TOP)/external/jpeg
LOCAL_SHARED_LIBRARIES += libjpeg
LOCAL_SHARED_LIBRARIES += libgui
LOCAL_SHARED_LIBRARIES += libui
endif

LOCAL_CFLAGS += -DBRINGUP_WIP

LOCAL_MODULE:= libavenhancements

LOCAL_MODULE_TAGS := optional

LOCAL_MODULE_OWNER := qti
LOCAL_PROPRIETARY_MODULE := true

include $(BUILD_SHARED_LIBRARY)
