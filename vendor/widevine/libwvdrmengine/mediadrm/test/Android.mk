LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_SRC_FILES := \
  WVDrmPlugin_test.cpp \

LOCAL_C_INCLUDES := \
  external/gmock/include \
  external/gtest/include \
  frameworks/av/include \
  frameworks/native/include \
  vendor/widevine/libwvdrmengine/cdm/core/include \
  vendor/widevine/libwvdrmengine/cdm/profiler/include \
  vendor/widevine/libwvdrmengine/cdm/include \
  vendor/widevine/libwvdrmengine/include \
  vendor/widevine/libwvdrmengine/mediadrm/include \
  vendor/widevine/libwvdrmengine/oemcrypto/include \

LOCAL_STATIC_LIBRARIES := \
  libcdm \
  libcdm_protos \
  libcdm_utils \
  libcrypto_static \
  libjsmn \
  libgmock \
  libgmock_main \
  libgtest \
  libwvlevel3 \
  libwvdrmdrmplugin \

LOCAL_SHARED_LIBRARIES := \
  libcutils \
  libdl \
  liblog \
  libprotobuf-cpp-lite \
  libutils \

LOCAL_C_INCLUDES += \
  external/protobuf/src \

# End protobuf section

LOCAL_MODULE := libwvdrmdrmplugin_test

LOCAL_MODULE_TAGS := tests

LOCAL_MODULE_TARGET_ARCH := arm x86 mips

include $(BUILD_EXECUTABLE)
