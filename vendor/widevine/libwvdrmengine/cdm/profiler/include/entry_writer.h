// Copyright 2016 Google Inc. All Rights Reserved.

#ifndef WVCDM_PROFILER_ENTRY_WRITER_H_
#define WVCDM_PROFILER_ENTRY_WRITER_H_

#include <stddef.h>
#include <stdint.h>

namespace wvcdm {
namespace oemprofiler {

class EntryWriter {
 public:
  explicit EntryWriter();

  const uint8_t* GetData() const;

  size_t GetSize() const;

  int WriteU8(uint8_t value);

  int WriteU16(uint16_t value);

  int WriteU32(uint32_t value);

  int WriteU64(uint64_t value);

  // WriteVLV
  // Write a uint 64 value using a variable number of bytes. The number
  // of bytes used is based on the number of bytes required to
  // express the value.
  //
  // The first 3 bits are used to express the number of bytes
  // (ranging 1 to 8 bytes).
  //
  // 1 byte  = 0 to 31
  // 2 bytes = 32 to 8191
  // 3 bytes = 8192 to 2097151
  // 4 bytes = 2097152 to 536870911
  // 5 bytes = 536870912 to 137438953471
  // 6 bytes = 137438953472 to 35184372088831
  // 7 bytes = 35184372088832 to 9007199254740991
  // 8 bytes = 9007199254740992 to 2305843009213693951
  int WriteVLV(uint64_t value);

  void Clear();

  size_t GetFreeSpace() const;

 private:
  static const size_t kBufferSize = 32;

  uint8_t bytes_[kBufferSize];
  size_t write_head_;

  int Write(uint64_t v, size_t num_bytes);

  template <typename T>
  int Write(T v);

  uint8_t GetByte(uint64_t value, size_t byte_index);

  // disallow copy and assign
  EntryWriter(const EntryWriter&);
  void operator=(const EntryWriter&);
};

}  // oemprofiler
}  // wvcdm

#endif
