// Copyright 2016 Google Inc. All Rights Reserved.

#ifndef WVCDM_CALL_HISTORY_H_
#define WVCDM_CALL_HISTORY_H_

#include <stdint.h>
#include <vector>

#include "circular_buffer.h"
#include "oem_functions.h"

namespace wvcdm {
namespace oemprofiler {

class CallHistory {

 public:
  CallHistory();

  void Write(
      OEM_FUNCTION fid,
      uint64_t start_time,
      uint64_t end_time,
      const uint8_t* meta_data,
      size_t meta_data_length);

  void Read(std::vector<uint8_t>& output) const;

 private:
  CircularBuffer buffer_;

  uint64_t time_at_head_;
  uint64_t time_at_tail_;

  bool RequestSpace(uint8_t num_bytes);

  bool ReadNextEntryRealEndTime(uint64_t* output);

  bool DropLastEntry();

  // Read a variable length value. This is the read that matches
  // EntryWriter's WriteVLV.
  int ReadVLV(size_t offset, uint64_t* output) const;
};

}  // namespace oemprofiler
}  // namespace wvcdm

#endif
