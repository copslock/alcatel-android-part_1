// Copyright 2016 Google Inc. All Rights Reserved.

#ifndef WVCDM_PROFILER_CIRCULAR_BUFFER_H_
#define WVCDM_PROFILER_CIRCULAR_BUFFER_H_

#include <stddef.h>
#include <stdint.h>
#include <vector>

namespace wvcdm {
namespace oemprofiler {

class CircularBuffer {
 public:
  explicit CircularBuffer(size_t memoryBudget);

  size_t GetFreeSpace() const;

  size_t GetUsedSpace() const;

  // Add a series of bytes to the buffer. Values will remain in
  // the buffer until Remove removes them. If there is not enough
  // room for the full series, no bytes are written into the buffer.
  bool AddU8s(const uint8_t* values, size_t numValues);

  // Add a single byte to the buffer. The value will remain in the
  // buffer until Remove removes it. If the buffer is full, the
  // byte will not be written into the buffer.
  bool AddU8(uint8_t value);

  // Read a series of bytes without removing them. The start of the
  // read is from the end (the oldest entry) + offset (toward the newest
  // entry). If requested number of bytes is too large, no bytes are read
  // and the function returns false.
  bool PeekU8s(size_t offset, uint8_t* out, size_t numToRead) const;

  // Read a single byte without removing it. The read byte is at the index
  // of the oldest entry + offset (toward the newest entry). If there are
  // no bytes in the buffer the function returns false.
  bool PeekU8(size_t offset, uint8_t* out) const;

  // Remove the requested number of bytes from the buffer from oldest
  // to newest. If there are not enough bytes to remove, nothing is
  // removed and the function returns false.
  bool Remove(size_t numBytes);

 private:
  std::vector<uint8_t> buffer_;

  size_t fill_;
  size_t head_;
  size_t tail_;

  // disallow copy and assign
  CircularBuffer(const CircularBuffer&);
  void operator=(const CircularBuffer&);
};

}  // namespace oemprofiler
}  // namespace wvcdm

#endif
