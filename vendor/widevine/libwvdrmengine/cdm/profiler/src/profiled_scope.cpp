// Copyright 2016 Google Inc. All Rights Reserved

#include "profiled_scope.h"

#include <sys/time.h>

#include "profiler.h"

namespace wvcdm {
namespace oemprofiler {

ProfiledScope::ProfiledScope(OEM_FUNCTION fid) :
    meta_data_(),
    fid_(fid),
    start_time_(GetNowUS()) {

}

ProfiledScope::~ProfiledScope() {
  Submit(GetNowUS());
}

void ProfiledScope::Submit(uint64_t end_time) const {
  Profiler::GetTable().Write(fid_, end_time - start_time_);

  Profiler::GetHistory().Write(
        fid_,
        start_time_,
        end_time,
        meta_data_.GetData(),
        meta_data_.GetSize());
}

uint64_t ProfiledScope::GetNowUS() const {
  struct timeval tv;
  gettimeofday(&tv, NULL);

  const uint64_t kSecondsToUSeconds = 1000000;
  return static_cast<uint64_t>(tv.tv_sec) * kSecondsToUSeconds +
      static_cast<uint64_t>(tv.tv_usec);
}

} // namespace oemprofiler
} // namespace wvcdm
