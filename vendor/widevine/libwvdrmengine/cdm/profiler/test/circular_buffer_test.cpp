// Copyright 2015 Google Inc. All Rights Reserved.

#include <gtest/gtest.h>
#include <stdint.h>

#include "circular_buffer.h"

namespace wvcdm {

TEST(CircularBufferTest, MixAddU8AndU8s) {
  oemprofiler::CircularBuffer buffer(32);

  const uint8_t expected_values[16] = {
    0x00, 0x01, 0x02, 0x03,
    0x04, 0x05, 0x06, 0x07,
    0x00, 0x01, 0x02, 0x03,
    0x04, 0x05, 0x06, 0x07
  };

  ASSERT_TRUE(buffer.AddU8(expected_values[0]));
  ASSERT_TRUE(buffer.AddU8(expected_values[1]));
  ASSERT_TRUE(buffer.AddU8s(expected_values + 2, 6));

  ASSERT_TRUE(buffer.AddU8(expected_values[8]));
  ASSERT_TRUE(buffer.AddU8(expected_values[9]));
  ASSERT_TRUE(buffer.AddU8s(expected_values + 10, 6));

  uint8_t read[16];
  ASSERT_TRUE(buffer.PeekU8s(0, read, 16));

  for (size_t i = 0; i < 16; i++) {
    ASSERT_EQ(expected_values[i], read[i]);
  }
}

TEST(CircularBufferTest, MixPeekU8AndU8s) {
  oemprofiler::CircularBuffer buffer(32);

  const uint8_t expected_values[16] = {
    0x00, 0x01, 0x02, 0x03,
    0x04, 0x05, 0x06, 0x07,
    0x00, 0x01, 0x02, 0x03,
    0x04, 0x05, 0x06, 0x07
  };

  ASSERT_TRUE(buffer.AddU8s(expected_values, 16));

  for (size_t i = 0; i < 2; i++) {
    uint8_t read;
    ASSERT_TRUE(buffer.PeekU8(i, &read));
    ASSERT_EQ(expected_values[i], read);
  }

  {
    uint8_t read[8];
    ASSERT_TRUE(buffer.PeekU8s(2, read, 8));

    for (size_t i = 0; i < 8; i++) {
      ASSERT_EQ(expected_values[i + 2], read[i]);
    }
  }

  for (size_t i = 10; i < 16; i++) {
    uint8_t read;
    ASSERT_TRUE(buffer.PeekU8(i, &read));
    ASSERT_EQ(expected_values[i], read);
  }
}

TEST(CircularBufferTest, ZeroSpaceBuffer) {
    oemprofiler::CircularBuffer buffer(0);

    ASSERT_FALSE(buffer.AddU8(0));

    const uint8_t values[4] = { 0x00, 0x01, 0x02, 0x03 };

    ASSERT_FALSE(buffer.AddU8s(values, 4));
}

TEST(CircularBufferTest, AddU8sWithLengthZero) {
  oemprofiler::CircularBuffer buffer(16);

  const uint8_t expected_values[16] = {
    0x00, 0x01, 0x02, 0x03,
    0x04, 0x05, 0x06, 0x07,
    0x00, 0x01, 0x02, 0x03,
    0x04, 0x05, 0x06, 0x07
  };

  ASSERT_TRUE(buffer.AddU8s(expected_values, 0));

  uint8_t read;
  ASSERT_FALSE(buffer.PeekU8(0, &read));
}

TEST(CircularBufferTest, MeasureSpaceTest) {
  oemprofiler::CircularBuffer buffer(16);

  ASSERT_EQ(16u, buffer.GetFreeSpace());
  ASSERT_EQ(0u, buffer.GetUsedSpace());

  for (int i = 0; i < 16; i++) {
    buffer.AddU8(0);
  }

  ASSERT_EQ(0u, buffer.GetFreeSpace());
  ASSERT_EQ(16u, buffer.GetUsedSpace());

  buffer.Remove(16);

  ASSERT_EQ(16u, buffer.GetFreeSpace());
  ASSERT_EQ(0u, buffer.GetUsedSpace());
}

TEST(CircularBufferTest, PeekAddU8Test) {
  oemprofiler::CircularBuffer buffer(16);

  for (uint8_t i = 0; i < 16; i++) {
    ASSERT_TRUE(buffer.AddU8(i));
  }

  for (uint8_t i = 0; i < 16; i++) {
    uint8_t read;
    ASSERT_TRUE(buffer.PeekU8(i, &read));
    ASSERT_EQ(i, read);
  }

  ASSERT_EQ(16u, buffer.GetUsedSpace());
}

TEST(CircularBufferTest, PeekAddU8WrapTest) {
  oemprofiler::CircularBuffer buffer(16);

  const uint8_t expected_values[] = {
     0,  1,  2,  3,  4,  5,  6,  7,
     8,  9, 10, 11, 12, 13, 14, 15,
    16, 17, 18, 19, 20, 21, 22, 23,
  };

  for (size_t i = 0; i < 16; i++) {
    ASSERT_TRUE(buffer.AddU8(expected_values[i]));
  }

  for (size_t i = 0; i < 16; i++) {
    uint8_t read;
    ASSERT_TRUE(buffer.PeekU8(i, &read));
    ASSERT_EQ(expected_values[i], read);
  }

  ASSERT_EQ(16u, buffer.GetUsedSpace());
  buffer.Remove(8);
  ASSERT_EQ(8u, buffer.GetUsedSpace());

  for (size_t i = 0; i < 8; i++) {
    const size_t expected_value_index = 8 + i;
    const size_t buffer_index = i;

    uint8_t read;
    ASSERT_TRUE(buffer.PeekU8(buffer_index, &read));
    ASSERT_EQ(expected_values[expected_value_index], read);
  }

  for (size_t i = 16; i < 24; i++) {
    ASSERT_TRUE(buffer.AddU8(expected_values[i]));
  }

  for (size_t i = 0; i < 16; i++) {
    const size_t expected_value_index = 8 + i;
    const size_t buffer_index = i;

    uint8_t read;
    buffer.PeekU8(buffer_index, &read);

    ASSERT_EQ(expected_values[expected_value_index], read);
  }
}

TEST(CircularBufferTest, OutOfRoomTest) {
  oemprofiler::CircularBuffer buffer(16);

  for (int i = 0; i < 16; i++) {
    ASSERT_TRUE(buffer.AddU8(i));
  }

  ASSERT_FALSE(buffer.AddU8(0));
}

TEST(CircularBufferTest, PassTailPeekTest) {
  oemprofiler::CircularBuffer buffer(16);

  uint8_t read;
  ASSERT_FALSE(buffer.PeekU8( 0, &read));
  ASSERT_FALSE(buffer.PeekU8(17, &read));
}

TEST(CircularBufferTest, NullOutputPeekTest) {
  oemprofiler::CircularBuffer buffer(16);
  buffer.AddU8(0);

  ASSERT_FALSE(buffer.PeekU8(0, nullptr));
}

TEST(CircularBufferTest, PeekAddU8sTest) {
  oemprofiler::CircularBuffer buffer(16);

  const uint8_t data[] = {
       0,  1,  2,  3,
       4,  5,  6,  7,
       8,  9, 10, 11,
      12, 13, 14, 15
    };

  ASSERT_TRUE(buffer.AddU8s(data, 16));

  uint8_t read[16];
  ASSERT_TRUE(buffer.PeekU8s(0, read, 16));

  for (uint8_t i = 0; i < 16; i++) {
    ASSERT_EQ(i, read[i]);
  }

  ASSERT_EQ(16u, buffer.GetUsedSpace());
}

}  // namespace
