// Copyright 2013 Google Inc. All Rights Reserved.
//
// Wrapper of OEMCrypto APIs for platforms that support both Levels 1 and 3.
// This should be used when liboemcrypto.so is dynamically loaded at run
// time and not linked with the CDM code at compile time.
// An implementation should compile either oemcrypto_adapter_dynamic.cpp or
// oemcrypto_adapter_static.cpp, but not both.
//

#include "oemcrypto_adapter.h"

#include <dlfcn.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <iostream>
#include <map>
#include <string>

#include "file_store.h"
#include "level3.h"
#include "lock.h"
#include "log.h"
#include "profiled_scope.h"
#include "properties.h"
#include "wv_cdm_constants.h"

using namespace wvoec3;
using wvcdm::kLevelDefault;
using wvcdm::kLevel3;

namespace {

static const size_t kMaxGenericEncryptChunkSize = 100*1024;

typedef struct {
    const uint8_t* key_id;
    size_t         key_id_length;
    const uint8_t* key_data_iv;
    const uint8_t* key_data;
    size_t         key_data_length;
    const uint8_t* key_control_iv;
    const uint8_t* key_control;
} OEMCrypto_KeyObject_V10;

typedef OEMCryptoResult (*L1_Initialize_t)(void);
typedef OEMCryptoResult (*L1_Terminate_t)(void);
typedef OEMCryptoResult (*L1_OpenSession_t)(OEMCrypto_SESSION* session);
typedef OEMCryptoResult (*L1_CloseSession_t)(OEMCrypto_SESSION session);
typedef OEMCryptoResult (*L1_GenerateDerivedKeys_t)(
    OEMCrypto_SESSION session, const uint8_t* mac_key_context,
    uint32_t mac_key_context_length, const uint8_t* enc_key_context,
    uint32_t enc_key_context_length);
typedef OEMCryptoResult (*L1_GenerateNonce_t)(OEMCrypto_SESSION session,
                                              uint32_t* nonce);
typedef OEMCryptoResult (*L1_GenerateSignature_t)(OEMCrypto_SESSION session,
                                                  const uint8_t* message,
                                                  size_t message_length,
                                                  uint8_t* signature,
                                                  size_t* signature_length);
typedef OEMCryptoResult (*L1_LoadKeys_t)(
    OEMCrypto_SESSION session, const uint8_t* message, size_t message_length,
    const uint8_t* signature, size_t signature_length,
    const uint8_t* enc_mac_key_iv, const uint8_t* enc_mac_key, size_t num_keys,
    const OEMCrypto_KeyObject* key_array, const uint8_t* pst,
    size_t pst_length);
typedef OEMCryptoResult (*L1_LoadKeys_V9_or_V10_t)(
    OEMCrypto_SESSION session, const uint8_t* message, size_t message_length,
    const uint8_t* signature, size_t signature_length,
    const uint8_t* enc_mac_key_iv, const uint8_t* enc_mac_key, size_t num_keys,
    const OEMCrypto_KeyObject_V10* key_array, const uint8_t* pst,
    size_t pst_length);
typedef OEMCryptoResult (*L1_LoadKeys_V8_t)(
    OEMCrypto_SESSION session, const uint8_t* message, size_t message_length,
    const uint8_t* signature, size_t signature_length,
    const uint8_t* enc_mac_key_iv, const uint8_t* enc_mac_key, size_t num_keys,
    const OEMCrypto_KeyObject_V10* key_array);
typedef OEMCryptoResult (*L1_RefreshKeys_t)(
    OEMCrypto_SESSION session, const uint8_t* message, size_t message_length,
    const uint8_t* signature, size_t signature_length, size_t num_keys,
    const OEMCrypto_KeyRefreshObject* key_array);
typedef OEMCryptoResult (*L1_QueryKeyControl_t)(
    OEMCrypto_SESSION session, const uint8_t* key_id, size_t key_id_length,
    uint8_t* key_control_block, size_t* key_control_block_length);
typedef OEMCryptoResult (*L1_SelectKey_t)(const OEMCrypto_SESSION session,
                                          const uint8_t* key_id,
                                          size_t key_id_length);
typedef OEMCryptoResult (*L1_DecryptCTR_V10_t)(
    OEMCrypto_SESSION session, const uint8_t* data_addr, size_t data_length,
    bool is_encrypted, const uint8_t* iv, size_t offset,
    const OEMCrypto_DestBufferDesc* out_buffer, uint8_t subsample_flags);
typedef OEMCryptoResult (*L1_DecryptCENC_t)(
    OEMCrypto_SESSION session, const uint8_t* data_addr, size_t data_length,
    bool is_encrypted, const uint8_t* iv, size_t offset,
    const OEMCrypto_DestBufferDesc* out_buffer,
    const OEMCrypto_CENCEncryptPatternDesc* pattern, uint8_t subsample_flags);
typedef OEMCryptoResult (*L1_CopyBuffer_t)(const uint8_t* data_addr,
                                           size_t data_length,
                                           OEMCrypto_DestBufferDesc* out_buffer,
                                           uint8_t subsample_flags);
typedef OEMCryptoResult (*L1_WrapKeybox_t)(const uint8_t* keybox,
                                           size_t keyBoxLength,
                                           uint8_t* wrappedKeybox,
                                           size_t* wrappedKeyBoxLength,
                                           const uint8_t* transportKey,
                                           size_t transportKeyLength);
typedef OEMCryptoResult (*L1_InstallKeybox_t)(const uint8_t* keybox,
                                              size_t keyBoxLength);
typedef OEMCryptoResult (*L1_LoadTestKeybox_t)();
typedef OEMCryptoResult (*L1_IsKeyboxValid_t)();
typedef OEMCryptoResult (*L1_GetDeviceID_t)(uint8_t* deviceID,
                                            size_t* idLength);
typedef OEMCryptoResult (*L1_GetKeyData_t)(uint8_t* keyData,
                                           size_t* keyDataLength);
typedef OEMCryptoResult (*L1_GetRandom_t)(uint8_t* randomData,
                                          size_t dataLength);
typedef OEMCryptoResult (*L1_RewrapDeviceRSAKey_t)(
    OEMCrypto_SESSION session, const uint8_t* message, size_t message_length,
    const uint8_t* signature, size_t signature_length, const uint32_t* nonce,
    const uint8_t* enc_rsa_key, size_t enc_rsa_key_length,
    const uint8_t* enc_rsa_key_iv, uint8_t* wrapped_rsa_key,
    size_t* wrapped_rsa_key_length);
typedef OEMCryptoResult (*L1_LoadDeviceRSAKey_t)(OEMCrypto_SESSION session,
                                                 const uint8_t* wrapped_rsa_key,
                                                 size_t wrapped_rsa_key_length);
typedef OEMCryptoResult (*L1_LoadTestRSAKey_t)();
typedef OEMCryptoResult (*L1_GenerateRSASignature_t)(
    OEMCrypto_SESSION session, const uint8_t* message, size_t message_length,
    uint8_t* signature, size_t* signature_length,
    RSA_Padding_Scheme padding_scheme);
typedef OEMCryptoResult (*L1_GenerateRSASignature_V8_t)(
    OEMCrypto_SESSION session, const uint8_t* message, size_t message_length,
    uint8_t* signature, size_t* signature_length);
typedef OEMCryptoResult (*L1_DeriveKeysFromSessionKey_t)(
    OEMCrypto_SESSION session, const uint8_t* enc_session_key,
    size_t enc_session_key_length, const uint8_t* mac_key_context,
    size_t mac_key_context_length, const uint8_t* enc_key_context,
    size_t enc_key_context_length);
typedef uint32_t (*L1_APIVersion_t)();
typedef uint8_t (*L1_SecurityPatchLevel_t)();
typedef const char* (*L1_SecurityLevel_t)();
typedef OEMCryptoResult (*L1_GetHDCPCapability_V9_t)(uint8_t* current,
                                                     uint8_t* maximum);
typedef OEMCryptoResult (*L1_GetHDCPCapability_t)(
    OEMCrypto_HDCP_Capability* current, OEMCrypto_HDCP_Capability* maximum);
typedef bool (*L1_SupportsUsageTable_t)();
typedef bool (*L1_IsAntiRollbackHwPresent_t)();
typedef OEMCryptoResult (*L1_GetNumberOfOpenSessions_t)(size_t* count);
typedef OEMCryptoResult (*L1_GetMaxNumberOfSessions_t)(size_t* maximum);
typedef OEMCryptoResult (*L1_Generic_Encrypt_t)(
    OEMCrypto_SESSION session, const uint8_t* in_buffer, size_t buffer_length,
    const uint8_t* iv, OEMCrypto_Algorithm algorithm, uint8_t* out_buffer);
typedef OEMCryptoResult (*L1_Generic_Decrypt_t)(
    OEMCrypto_SESSION session, const uint8_t* in_buffer, size_t buffer_length,
    const uint8_t* iv, OEMCrypto_Algorithm algorithm, uint8_t* out_buffer);
typedef OEMCryptoResult (*L1_Generic_Sign_t)(OEMCrypto_SESSION session,
                                             const uint8_t* in_buffer,
                                             size_t buffer_length,
                                             OEMCrypto_Algorithm algorithm,
                                             uint8_t* signature,
                                             size_t* signature_length);
typedef OEMCryptoResult (*L1_Generic_Verify_t)(OEMCrypto_SESSION session,
                                               const uint8_t* in_buffer,
                                               size_t buffer_length,
                                               OEMCrypto_Algorithm algorithm,
                                               const uint8_t* signature,
                                               size_t signature_length);
typedef OEMCryptoResult (*L1_UpdateUsageTable_t)();
typedef OEMCryptoResult (*L1_DeactivateUsageEntry_t)(const uint8_t* pst,
                                                     size_t pst_length);
typedef OEMCryptoResult (*L1_ReportUsage_t)(OEMCrypto_SESSION session,
                                            const uint8_t* pst,
                                            size_t pst_length,
                                            OEMCrypto_PST_Report* buffer,
                                            size_t* buffer_length);
typedef OEMCryptoResult (*L1_DeleteUsageEntry_t)(
    OEMCrypto_SESSION session, const uint8_t* pst, size_t pst_length,
    const uint8_t* message, size_t message_length, const uint8_t* signature,
    size_t signature_length);
typedef OEMCryptoResult (*L1_ForceDeleteUsageEntry_t)(const uint8_t* pst,
                                                      size_t pst_length);
typedef OEMCryptoResult (*L1_DeleteUsageTable_t)();

struct FunctionPointers {
  uint32_t version;
  L1_Initialize_t Initialize;
  L1_Terminate_t Terminate;
  L1_OpenSession_t OpenSession;
  L1_CloseSession_t CloseSession;
  L1_GenerateDerivedKeys_t GenerateDerivedKeys;
  L1_GenerateNonce_t GenerateNonce;
  L1_GenerateSignature_t GenerateSignature;
  L1_LoadKeys_t LoadKeys;
  L1_RefreshKeys_t RefreshKeys;
  L1_QueryKeyControl_t QueryKeyControl;
  L1_SelectKey_t SelectKey;
  L1_DecryptCTR_V10_t DecryptCTR_V10;
  L1_DecryptCENC_t DecryptCENC;
  L1_CopyBuffer_t CopyBuffer;
  L1_WrapKeybox_t WrapKeybox;
  L1_InstallKeybox_t InstallKeybox;
  L1_LoadTestKeybox_t LoadTestKeybox;
  L1_IsKeyboxValid_t IsKeyboxValid;
  L1_GetDeviceID_t GetDeviceID;
  L1_GetKeyData_t GetKeyData;
  L1_GetRandom_t GetRandom;
  L1_RewrapDeviceRSAKey_t RewrapDeviceRSAKey;
  L1_LoadDeviceRSAKey_t LoadDeviceRSAKey;
  L1_LoadTestRSAKey_t LoadTestRSAKey;
  L1_GenerateRSASignature_t GenerateRSASignature;
  L1_DeriveKeysFromSessionKey_t DeriveKeysFromSessionKey;
  L1_APIVersion_t APIVersion;
  L1_SecurityPatchLevel_t SecurityPatchLevel;
  L1_SecurityLevel_t SecurityLevel;
  L1_GetHDCPCapability_t GetHDCPCapability;
  L1_SupportsUsageTable_t SupportsUsageTable;
  L1_IsAntiRollbackHwPresent_t IsAntiRollbackHwPresent;
  L1_GetNumberOfOpenSessions_t GetNumberOfOpenSessions;
  L1_GetMaxNumberOfSessions_t GetMaxNumberOfSessions;
  L1_Generic_Encrypt_t Generic_Encrypt;
  L1_Generic_Decrypt_t Generic_Decrypt;
  L1_Generic_Sign_t Generic_Sign;
  L1_Generic_Verify_t Generic_Verify;
  L1_UpdateUsageTable_t UpdateUsageTable;
  L1_DeactivateUsageEntry_t DeactivateUsageEntry;
  L1_ReportUsage_t ReportUsage;
  L1_DeleteUsageEntry_t DeleteUsageEntry;
  L1_ForceDeleteUsageEntry_t ForceDeleteUsageEntry;
  L1_DeleteUsageTable_t DeleteUsageTable;

  L1_LoadKeys_V8_t LoadKeys_V8;
  L1_GenerateRSASignature_V8_t GenerateRSASignature_V8;
  L1_GetHDCPCapability_V9_t GetHDCPCapability_V9;
  L1_LoadKeys_V9_or_V10_t LoadKeys_V9_or_V10;
};

struct LevelSession {
  FunctionPointers* fcn;
  OEMCrypto_SESSION session;
  LevelSession() : fcn(0), session(0) {};
};

#define QUOTE_DEFINE(A) #A
#define QUOTE(A) QUOTE_DEFINE(A)
#define LOOKUP(Name, Function)                                           \
  level1_.Name = (L1_##Name##_t)dlsym(level1_library_, QUOTE(Function)); \
  if (!level1_.Name) {                                                   \
    LOGW("Could not load L1 %s. Falling Back to L3.",                    \
         QUOTE(OEMCrypto_##Name));                                       \
    return false;                                                        \
  }

// An Adapter keeps a block of FunctionPointers for the built-in level 3 and
// the dynamically loaded level 1 oemcrypto.  When initialized, it tries to
// load the level 1 library and verifies that all needed functions are present.
// If they are not, then it flags the level 1 as invalid.  Later, when the
// function get(kLevel3) is called, it returns the level 3 function pointers.
// When get(kLevelDefault) is called, it returns level 1 function pointers if
// level 1 is valid and otherwise returns the level 3 function pointers.
class Adapter {
 public:
  typedef std::map<OEMCrypto_SESSION, LevelSession>::iterator map_iterator;

  Adapter() : level1_valid_(false), level1_library_(NULL) {}

  ~Adapter() {
  }

  OEMCryptoResult Initialize() {
    LoadLevel3();
    OEMCryptoResult result = Level3_Initialize();
    if (force_level3()) {
      LOGW("Test code. User requested falling back to L3");
      return result;
    }
    std::string library_name;
    if (!wvcdm::Properties::GetOEMCryptoPath(&library_name)) {
      LOGW("L1 library not specified. Falling back to L3");
      return result;
    }
    level1_library_ = dlopen(library_name.c_str(), RTLD_NOW);
    if (level1_library_ == NULL) {
      LOGW("Could not load %s. Falling back to L3.  %s", library_name.c_str(),
           dlerror());
      return result;
    }
    if (LoadLevel1()) {
      LOGD("OEMCrypto_Initialize Level 1 success. I will use level 1.");
    } else {
      dlclose(level1_library_);
      level1_library_ = NULL;
      level1_valid_ = false;
    }
    return result;
  }

  bool LoadLevel1() {
    level1_valid_ = true;
    LOOKUP(Initialize,               OEMCrypto_Initialize);
    LOOKUP(Terminate,                OEMCrypto_Terminate);
    LOOKUP(OpenSession,              OEMCrypto_OpenSession);
    LOOKUP(CloseSession,             OEMCrypto_CloseSession);
    LOOKUP(GenerateDerivedKeys,      OEMCrypto_GenerateDerivedKeys);
    LOOKUP(GenerateNonce,            OEMCrypto_GenerateNonce);
    LOOKUP(GenerateSignature,        OEMCrypto_GenerateSignature);
    LOOKUP(RefreshKeys,              OEMCrypto_RefreshKeys);
    LOOKUP(SelectKey,                OEMCrypto_SelectKey);
    LOOKUP(InstallKeybox,            OEMCrypto_InstallKeybox);
    LOOKUP(IsKeyboxValid,            OEMCrypto_IsKeyboxValid);
    LOOKUP(GetDeviceID,              OEMCrypto_GetDeviceID);
    LOOKUP(GetKeyData,               OEMCrypto_GetKeyData);
    LOOKUP(GetRandom,                OEMCrypto_GetRandom);
    LOOKUP(WrapKeybox,               OEMCrypto_WrapKeybox);
    LOOKUP(RewrapDeviceRSAKey,       OEMCrypto_RewrapDeviceRSAKey);
    LOOKUP(LoadDeviceRSAKey,         OEMCrypto_LoadDeviceRSAKey);
    LOOKUP(DeriveKeysFromSessionKey, OEMCrypto_DeriveKeysFromSessionKey);
    LOOKUP(APIVersion,               OEMCrypto_APIVersion);
    LOOKUP(SecurityLevel,            OEMCrypto_SecurityLevel);
    LOOKUP(Generic_Decrypt,          OEMCrypto_Generic_Decrypt);
    LOOKUP(Generic_Encrypt,          OEMCrypto_Generic_Encrypt);
    LOOKUP(Generic_Sign,             OEMCrypto_Generic_Sign);
    LOOKUP(Generic_Verify,           OEMCrypto_Generic_Verify);
    if (!level1_valid_) {
      return false;
    }
    OEMCryptoResult st = level1_.Initialize();
    if (st != OEMCrypto_SUCCESS) {
      LOGW("Could not initialize L1. Falling Back to L3.");
      return false;
    }
    level1_.version = level1_.APIVersion();
    const uint32_t kMinimumVersion = 8;
    if (level1_.version < kMinimumVersion) {
      LOGW("liboemcrypto.so is version %d, not %d. Falling Back to L3.",
           level1_.version, kMinimumVersion);
      level1_.Terminate();
      return false;
    }
    if (level1_.version == 8) {
      LOOKUP(LoadKeys_V8,             OEMCrypto_LoadKeys_V8);
      LOOKUP(GenerateRSASignature_V8, OEMCrypto_GenerateRSASignature_V8);
    } else {
      LOOKUP(GenerateRSASignature,    OEMCrypto_GenerateRSASignature);
      LOOKUP(SupportsUsageTable,      OEMCrypto_SupportsUsageTable);
      LOOKUP(UpdateUsageTable,        OEMCrypto_UpdateUsageTable);
      LOOKUP(DeactivateUsageEntry,    OEMCrypto_DeactivateUsageEntry);
      LOOKUP(ReportUsage,             OEMCrypto_ReportUsage);
      LOOKUP(DeleteUsageEntry,        OEMCrypto_DeleteUsageEntry);
      LOOKUP(DeleteUsageTable,        OEMCrypto_DeleteUsageTable);
      if (level1_.version == 9) {
        LOOKUP(LoadKeys_V9_or_V10,      OEMCrypto_LoadKeys_V9_or_V10);
        LOOKUP(GetHDCPCapability_V9,    OEMCrypto_GetHDCPCapability_V9);
      } else {
        LOOKUP(LoadTestKeybox,          OEMCrypto_LoadTestKeybox);
        LOOKUP(LoadTestRSAKey,          OEMCrypto_LoadTestRSAKey);
        LOOKUP(QueryKeyControl,         OEMCrypto_QueryKeyControl);
        LOOKUP(CopyBuffer,              OEMCrypto_CopyBuffer);
        LOOKUP(GetHDCPCapability,       OEMCrypto_GetHDCPCapability);
        LOOKUP(IsAntiRollbackHwPresent, OEMCrypto_IsAntiRollbackHwPresent);
        LOOKUP(GetNumberOfOpenSessions, OEMCrypto_GetNumberOfOpenSessions);
        LOOKUP(GetMaxNumberOfSessions,  OEMCrypto_GetMaxNumberOfSessions);
        LOOKUP(ForceDeleteUsageEntry,   OEMCrypto_ForceDeleteUsageEntry);
        if (level1_.version == 10) {
          LOOKUP(LoadKeys_V9_or_V10,      OEMCrypto_LoadKeys_V9_or_V10);
          LOOKUP(DecryptCTR_V10,          OEMCrypto_DecryptCTR_V10);
        } else {  // version 11.
          LOOKUP(LoadKeys,                OEMCrypto_LoadKeys);
          LOOKUP(DecryptCENC,             OEMCrypto_DecryptCENC);
          LOOKUP(SecurityPatchLevel,      OEMCrypto_Security_Patch_Level);
        }
      }
    }
    if (OEMCrypto_SUCCESS == level1_.IsKeyboxValid()) {
      return true;
    }
    uint8_t buffer[1];
    size_t buffer_size = 0;
    if (OEMCrypto_ERROR_NOT_IMPLEMENTED == level1_.GetKeyData(buffer,
                                                              &buffer_size)){
      // If GetKeyData is not implemented, then the device should only use a
      // baked in certificate as identification.  We will assume that a device
      // with a bad keybox returns a different error code.
      if (!wvcdm::Properties::use_certificates_as_identification()) {
        // If OEMCrypto does not support a keybox, but the CDM code expects
        // one, things will not work well at all.  This is not a fatal error
        // because we still want to test OEMCrypto in that configuration.
        LOGE("OEMCrypto uses cert as identification, but cdm does not!");
        LOGE("This will not work on a production device.");
      }
      return true;
    }
    wvcdm::File file;
    std::string filename;
    if (!wvcdm::Properties::GetFactoryKeyboxPath(&filename)) {
      LOGW("Bad Level 1 Keybox. Falling Back to L3.");
      level1_.Terminate();
      return false;
    }
    ssize_t size = file.FileSize(filename);
    if (size <= 0 || !file.Open(filename, file.kBinary | file.kReadOnly)) {
      LOGW("Could not open %s. Falling Back to L3.", filename.c_str());
      level1_.Terminate();
      return false;
    }
    uint8_t keybox[size];
    ssize_t size_read = file.Read(reinterpret_cast<char*>(keybox), size);
    if (level1_.InstallKeybox(keybox, size) != OEMCrypto_SUCCESS) {
      LOGE("Could NOT install keybox from %s. Falling Back to L3.",
           filename.c_str());
      level1_.Terminate();
      return false;
    }
    LOGI("Installed keybox from %s", filename.c_str());
    return true;
  }

  void LoadLevel3() {
    level3_.Initialize = Level3_Initialize;
    level3_.Terminate = Level3_Terminate;
    level3_.OpenSession = Level3_OpenSession;
    level3_.CloseSession = Level3_CloseSession;
    level3_.GenerateDerivedKeys = Level3_GenerateDerivedKeys;
    level3_.GenerateNonce = Level3_GenerateNonce;
    level3_.GenerateSignature = Level3_GenerateSignature;
    level3_.LoadKeys = Level3_LoadKeys;
    level3_.RefreshKeys = Level3_RefreshKeys;
    level3_.QueryKeyControl = Level3_QueryKeyControl;
    level3_.SelectKey = Level3_SelectKey;
    level3_.DecryptCENC = Level3_DecryptCENC;
    level3_.CopyBuffer = Level3_CopyBuffer;
    level3_.WrapKeybox = Level3_WrapKeybox;
    level3_.InstallKeybox = Level3_InstallKeybox;
    level3_.LoadTestKeybox = Level3_LoadTestKeybox;
    level3_.IsKeyboxValid = Level3_IsKeyboxValid;
    level3_.GetDeviceID = Level3_GetDeviceID;
    level3_.GetKeyData = Level3_GetKeyData;
    level3_.GetRandom = Level3_GetRandom;
    level3_.RewrapDeviceRSAKey = Level3_RewrapDeviceRSAKey;
    level3_.LoadDeviceRSAKey = Level3_LoadDeviceRSAKey;
    level3_.LoadTestRSAKey = Level3_LoadTestRSAKey;
    level3_.GenerateRSASignature = Level3_GenerateRSASignature;
    level3_.DeriveKeysFromSessionKey = Level3_DeriveKeysFromSessionKey;
    level3_.APIVersion = Level3_APIVersion;
    level3_.SecurityPatchLevel = Level3_SecurityPatchLevel;
    level3_.SecurityLevel = Level3_SecurityLevel;
    level3_.GetHDCPCapability = Level3_GetHDCPCapability;
    level3_.SupportsUsageTable = Level3_SupportsUsageTable;
    level3_.IsAntiRollbackHwPresent = Level3_IsAntiRollbackHwPresent;
    level3_.GetNumberOfOpenSessions = Level3_GetNumberOfOpenSessions;
    level3_.GetMaxNumberOfSessions = Level3_GetMaxNumberOfSessions;
    level3_.Generic_Decrypt = Level3_Generic_Decrypt;
    level3_.Generic_Encrypt = Level3_Generic_Encrypt;
    level3_.Generic_Sign = Level3_Generic_Sign;
    level3_.Generic_Verify = Level3_Generic_Verify;
    level3_.UpdateUsageTable = Level3_UpdateUsageTable;
    level3_.DeactivateUsageEntry = Level3_DeactivateUsageEntry;
    level3_.ReportUsage = Level3_ReportUsage;
    level3_.DeleteUsageEntry = Level3_DeleteUsageEntry;
    level3_.ForceDeleteUsageEntry = Level3_ForceDeleteUsageEntry;
    level3_.DeleteUsageTable = Level3_DeleteUsageTable;

    level3_.version = Level3_APIVersion();
  }

  OEMCryptoResult Terminate() {
    for (map_iterator i = session_map_.begin(); i != session_map_.end(); ++i) {
      if (i->second.fcn) i->second.fcn->CloseSession(i->second.session);
    }
    session_map_.clear();
    OEMCryptoResult result = Level3_Terminate();
    if (level1_valid_) {
      result = level1_.Terminate();
      dlclose(level1_library_);
      level1_library_ = NULL;
    }
    return result;
  }

  const FunctionPointers* get(wvcdm::SecurityLevel level) {
    if (level1_valid_ && level == kLevelDefault) return &level1_;
    return &level3_;
  }

  LevelSession get(OEMCrypto_SESSION session) {
    wvcdm::AutoLock auto_lock(session_map_lock_);
    map_iterator pair = session_map_.find(session);
    if (pair == session_map_.end()) {
      return LevelSession();
    }
    return pair->second;
  }

  OEMCryptoResult OpenSession(OEMCrypto_SESSION* session,
                              wvcdm::SecurityLevel level) {
    wvcdm::oemprofiler::ProfiledScope ps(
        wvcdm::oemprofiler::OEM_FUNCTION_OPEN_SESSION);

    LevelSession new_session;
    OEMCryptoResult result;
    if (level == kLevelDefault && level1_valid_) {
      new_session.fcn = &level1_;
      result = level1_.OpenSession(&new_session.session);
      *session = new_session.session;
    } else {
      new_session.fcn = &level3_;
      result = level3_.OpenSession(&new_session.session);
      *session = new_session.session + kLevel3Offset;
    }
    if (result == OEMCrypto_SUCCESS) {
      wvcdm::AutoLock auto_lock(session_map_lock_);
      // Make sure session is not already in my list of sessions.
      while (session_map_.find(*session) != session_map_.end()) {
        (*session)++;
      }
      session_map_[*session] = new_session;
    }

    return result;
  }

  OEMCryptoResult CloseSession(OEMCrypto_SESSION session) {
    wvcdm::oemprofiler::ProfiledScope ps(
        wvcdm::oemprofiler::OEM_FUNCTION_CLOSE_SESSION);

    wvcdm::AutoLock auto_lock(session_map_lock_);
    map_iterator pair = session_map_.find(session);
    if (pair == session_map_.end()) {
      return OEMCrypto_ERROR_INVALID_SESSION;
    }
    OEMCryptoResult result =
        pair->second.fcn->CloseSession(pair->second.session);
    session_map_.erase(pair);

    return result;
  }

 private:
  bool level1_valid_;
  void* level1_library_;
  struct FunctionPointers level1_;
  struct FunctionPointers level3_;
  std::map<OEMCrypto_SESSION, LevelSession> session_map_;
  wvcdm::Lock session_map_lock_;
  // This is just for debugging the map between session ids.
  // If we add this to the level 3 session id, then the external session
  // id will match the internal session id in the last two digits.
  static const OEMCrypto_SESSION kLevel3Offset = 25600;

  // For running the unit tests using the level 3 oemcrypto.  If the user sets
  // the environment FORCE_LEVEL3_OEMCRYPTO, we ignore the level 1 library.
  bool force_level3() {
    const char* var = getenv("FORCE_LEVEL3_OEMCRYPTO");
    if (!var) return false;
    return !strcmp(var, "yes");
  }
};

static Adapter* kAdapter = 0;

}  // namespace

namespace wvcdm {

OEMCryptoResult OEMCrypto_OpenSession(OEMCrypto_SESSION* session,
                                      SecurityLevel level) {

  if (!kAdapter) return OEMCrypto_ERROR_OPEN_SESSION_FAILED;
  return kAdapter->OpenSession(session, level);
}

OEMCryptoResult OEMCrypto_CopyBuffer(
    SecurityLevel level, const uint8_t* data_addr, size_t data_length,
    OEMCrypto_DestBufferDesc* out_buffer, uint8_t subsample_flags) {

  oemprofiler::ProfiledScope ps(oemprofiler::OEM_FUNCTION_COPY_BUFFER);

  ps.meta_data_.WriteVLV(static_cast<uint64_t>(data_length));

  if (!kAdapter) return OEMCrypto_ERROR_UNKNOWN_FAILURE;
  const FunctionPointers* fcn = kAdapter->get(level);
  if (!fcn) return OEMCrypto_ERROR_INVALID_SESSION;
  if (fcn->version < 10) return OEMCrypto_ERROR_NOT_IMPLEMENTED;
  return fcn->CopyBuffer(data_addr, data_length, out_buffer, subsample_flags);
}

OEMCryptoResult OEMCrypto_InstallKeybox(const uint8_t* keybox,
                                        size_t keyBoxLength,
                                        SecurityLevel level) {

  oemprofiler::ProfiledScope ps(oemprofiler::OEM_FUNCTION_INSTALL_KEYBOX);

  if (!kAdapter) return OEMCrypto_ERROR_UNKNOWN_FAILURE;
  const FunctionPointers* fcn = kAdapter->get(level);
  if (!fcn) return OEMCrypto_ERROR_INVALID_SESSION;
  return fcn->InstallKeybox(keybox, keyBoxLength);
}

OEMCryptoResult OEMCrypto_IsKeyboxValid(SecurityLevel level) {

  oemprofiler::ProfiledScope ps(oemprofiler::OEM_FUNCTION_IS_KEYBOX_VALID);

  if (!kAdapter) return OEMCrypto_ERROR_UNKNOWN_FAILURE;
  const FunctionPointers* fcn = kAdapter->get(level);
  if (!fcn) return OEMCrypto_ERROR_INVALID_SESSION;
  return fcn->IsKeyboxValid();
}

OEMCryptoResult OEMCrypto_GetDeviceID(uint8_t* deviceID, size_t* idLength,
                                      SecurityLevel level) {

  oemprofiler::ProfiledScope ps(oemprofiler::OEM_FUNCTION_GET_DEVICE_ID);

  if (!kAdapter) return OEMCrypto_ERROR_UNKNOWN_FAILURE;
  const FunctionPointers* fcn = kAdapter->get(level);
  if (!fcn) return OEMCrypto_ERROR_INVALID_SESSION;
  return fcn->GetDeviceID(deviceID, idLength);
}

OEMCryptoResult OEMCrypto_GetKeyData(uint8_t* keyData, size_t* keyDataLength,
                                     SecurityLevel level) {

  oemprofiler::ProfiledScope ps(oemprofiler::OEM_FUNCTION_GET_KEY_DATA);

  if (!kAdapter) return OEMCrypto_ERROR_UNKNOWN_FAILURE;
  const FunctionPointers* fcn = kAdapter->get(level);
  if (!fcn) return OEMCrypto_ERROR_INVALID_SESSION;
  return fcn->GetKeyData(keyData, keyDataLength);
}

uint32_t OEMCrypto_APIVersion(SecurityLevel level) {

  oemprofiler::ProfiledScope ps(oemprofiler::OEM_FUNCTION_API_VERSION);

  if (!kAdapter) return 0;
  const FunctionPointers* fcn = kAdapter->get(level);
  if (!fcn) return 0;
  return fcn->APIVersion();
}

uint8_t OEMCrypto_Security_Patch_Level(SecurityLevel level) {

  oemprofiler::ProfiledScope ps(oemprofiler::OEM_FUNCTION_SECURITY_PATCH_LEVEL);

  if (!kAdapter) return 0;
  const FunctionPointers* fcn = kAdapter->get(level);
  if (!fcn) return 0;
  if (fcn->version < 11) return 0;
  return fcn->SecurityPatchLevel();
}

const char* OEMCrypto_SecurityLevel(SecurityLevel level) {

  oemprofiler::ProfiledScope ps(oemprofiler::OEM_FUNCTION_SECURITY_LEVEL);

  if (!kAdapter) return "";
  const FunctionPointers* fcn = kAdapter->get(level);
  if (!fcn) return "";
  return fcn->SecurityLevel();
}

OEMCryptoResult OEMCrypto_GetHDCPCapability(
    SecurityLevel level, OEMCrypto_HDCP_Capability* current,
    OEMCrypto_HDCP_Capability* maximum) {

  oemprofiler::ProfiledScope ps(oemprofiler::OEM_FUNCTION_GET_HDCP_CAPABILITIY);

  if (!kAdapter) return OEMCrypto_ERROR_UNKNOWN_FAILURE;
  const FunctionPointers* fcn = kAdapter->get(level);
  if (!fcn) return OEMCrypto_ERROR_NOT_IMPLEMENTED;
  if (fcn->version == 8) return OEMCrypto_ERROR_NOT_IMPLEMENTED;
  if (fcn->version == 9) {
    if (current == NULL) return OEMCrypto_ERROR_UNKNOWN_FAILURE;
    if (maximum == NULL) return OEMCrypto_ERROR_UNKNOWN_FAILURE;
    uint8_t current_byte, maximum_byte;
    OEMCryptoResult sts = fcn->GetHDCPCapability_V9(&current_byte, &maximum_byte);
    *current = static_cast<OEMCrypto_HDCP_Capability>(current_byte);
    *maximum = static_cast<OEMCrypto_HDCP_Capability>(maximum_byte);
    return sts;
  } else {
    return fcn->GetHDCPCapability(current, maximum);
  }
}

bool OEMCrypto_SupportsUsageTable(SecurityLevel level) {

  oemprofiler::ProfiledScope ps(oemprofiler::OEM_FUNCTION_SUPPORTS_USAGE_TABLE);

  if (!kAdapter) return false;
  const FunctionPointers* fcn = kAdapter->get(level);
  if (!fcn) return false;
  if (fcn->version == 8) return false;
  return fcn->SupportsUsageTable();
}

bool OEMCrypto_IsAntiRollbackHwPresent(SecurityLevel level) {

  oemprofiler::ProfiledScope ps(
      oemprofiler::OEM_FUNCTION_IS_ANTI_ROLLBACK_HW_PRESENT);

  if (!kAdapter) return false;
  const FunctionPointers* fcn = kAdapter->get(level);
  if (!fcn) return false;
  if (fcn->version < 10) return false;
  return fcn->IsAntiRollbackHwPresent();
}

OEMCryptoResult OEMCrypto_GetNumberOfOpenSessions(SecurityLevel level,
                                                  size_t* count) {

  oemprofiler::ProfiledScope ps(
      oemprofiler::OEM_FUNCTION_GET_NUMBER_OF_OPEN_SESSIONS);

  if (!kAdapter) return OEMCrypto_ERROR_UNKNOWN_FAILURE;
  const FunctionPointers* fcn = kAdapter->get(level);
  if (!fcn) return OEMCrypto_ERROR_INVALID_SESSION;
  if (fcn->version < 10) return OEMCrypto_ERROR_NOT_IMPLEMENTED;
  return fcn->GetNumberOfOpenSessions(count);
}

OEMCryptoResult OEMCrypto_GetMaxNumberOfSessions(SecurityLevel level,
                                                 size_t* maximum) {

  oemprofiler::ProfiledScope ps(
      oemprofiler::OEM_FUNCTION_GET_MAX_NUMBER_OF_SESSIONS);

  if (!kAdapter) return OEMCrypto_ERROR_UNKNOWN_FAILURE;
  const FunctionPointers* fcn = kAdapter->get(level);
  if (!fcn) return OEMCrypto_ERROR_INVALID_SESSION;
  if (fcn->version < 10) return OEMCrypto_ERROR_NOT_IMPLEMENTED;
  return fcn->GetMaxNumberOfSessions(maximum);
}
}  // namespace wvcdm

extern "C" OEMCryptoResult OEMCrypto_Initialize(void) {
  if (kAdapter) {
    kAdapter->Terminate();
    delete kAdapter;
  }
  kAdapter = new Adapter();
  return kAdapter->Initialize();
}

extern "C" OEMCryptoResult OEMCrypto_Terminate(void) {
  OEMCryptoResult result = OEMCrypto_SUCCESS;
  if (kAdapter) {
    result = kAdapter->Terminate();
    delete kAdapter;
  }
  kAdapter = NULL;
  return result;
}

extern "C" OEMCryptoResult OEMCrypto_OpenSession(OEMCrypto_SESSION* session) {
  return OEMCrypto_OpenSession(session, kLevelDefault);
}

extern "C" OEMCryptoResult OEMCrypto_CloseSession(OEMCrypto_SESSION session) {
  if (!kAdapter) return OEMCrypto_ERROR_CLOSE_SESSION_FAILED;
  return kAdapter->CloseSession(session);
}

extern "C" OEMCryptoResult OEMCrypto_GenerateDerivedKeys(
    OEMCrypto_SESSION session, const uint8_t* mac_key_context,
    uint32_t mac_key_context_length, const uint8_t* enc_key_context,
    uint32_t enc_key_context_length) {

  wvcdm::oemprofiler::ProfiledScope ps(
      wvcdm::oemprofiler::OEM_FUNCTION_GENERATE_DERIVED_KEYS);

  if (!kAdapter) return OEMCrypto_ERROR_UNKNOWN_FAILURE;
  LevelSession pair = kAdapter->get(session);
  if (!pair.fcn) return OEMCrypto_ERROR_INVALID_SESSION;
  return pair.fcn->GenerateDerivedKeys(pair.session, mac_key_context,
                                       mac_key_context_length, enc_key_context,
                                       enc_key_context_length);
}

extern "C" OEMCryptoResult OEMCrypto_GenerateNonce(OEMCrypto_SESSION session,
                                                   uint32_t* nonce) {

  wvcdm::oemprofiler::ProfiledScope ps(
      wvcdm::oemprofiler::OEM_FUNCTION_GENERATE_NONCE);

  if (!kAdapter) return OEMCrypto_ERROR_UNKNOWN_FAILURE;
  LevelSession pair = kAdapter->get(session);
  if (!pair.fcn) return OEMCrypto_ERROR_INVALID_SESSION;
  return pair.fcn->GenerateNonce(pair.session, nonce);
}

extern "C" OEMCryptoResult OEMCrypto_GenerateSignature(
    OEMCrypto_SESSION session, const uint8_t* message, size_t message_length,
    uint8_t* signature, size_t* signature_length) {

  wvcdm::oemprofiler::ProfiledScope ps(
      wvcdm::oemprofiler::OEM_FUNCTION_GENERATE_SIGNATURE);

  if (!kAdapter) return OEMCrypto_ERROR_UNKNOWN_FAILURE;
  LevelSession pair = kAdapter->get(session);
  if (!pair.fcn) return OEMCrypto_ERROR_INVALID_SESSION;
  return pair.fcn->GenerateSignature(pair.session, message, message_length,
                                     signature, signature_length);
}

extern "C" OEMCryptoResult OEMCrypto_LoadKeys(
    OEMCrypto_SESSION session, const uint8_t* message, size_t message_length,
    const uint8_t* signature, size_t signature_length,
    const uint8_t* enc_mac_key_iv, const uint8_t* enc_mac_key, size_t num_keys,
    const OEMCrypto_KeyObject* key_array, const uint8_t* pst,
    size_t pst_length) {

  wvcdm::oemprofiler::ProfiledScope ps(
      wvcdm::oemprofiler::OEM_FUNCTION_LOAD_KEYS);

  if (!kAdapter) return OEMCrypto_ERROR_UNKNOWN_FAILURE;
  LevelSession pair = kAdapter->get(session);
  if (!pair.fcn) return OEMCrypto_ERROR_INVALID_SESSION;
  if (pair.fcn->version < 11) {
    std::vector<OEMCrypto_KeyObject_V10> key_array_v10(num_keys);
    for(size_t i=0; i < num_keys; i++) {
      key_array_v10[i].key_id = key_array[i].key_id;
      key_array_v10[i].key_id_length = key_array[i].key_id_length;
      key_array_v10[i].key_data_iv = key_array[i].key_data_iv;
      key_array_v10[i].key_data = key_array[i].key_data;
      key_array_v10[i].key_data_length = key_array[i].key_data_length;
      key_array_v10[i].key_control_iv = key_array[i].key_control_iv;
      key_array_v10[i].key_control = key_array[i].key_control;
      if (key_array[i].cipher_mode == OEMCrypto_CipherMode_CBC) {
        LOGE("CBC Mode not supported.");
        return OEMCrypto_ERROR_NOT_IMPLEMENTED;
      }
    }
    if (pair.fcn->version == 8) {
      return pair.fcn->LoadKeys_V8(pair.session, message, message_length,
                                   signature, signature_length, enc_mac_key_iv,
                                   enc_mac_key, num_keys, &key_array_v10[0]);
    } else  {
      return pair.fcn->LoadKeys_V9_or_V10(pair.session, message, message_length,
                                          signature, signature_length,
                                          enc_mac_key_iv, enc_mac_key,
                                          num_keys, &key_array_v10[0],
                                          pst, pst_length);
    }
  } else {
    return pair.fcn->LoadKeys(pair.session, message, message_length, signature,
                              signature_length, enc_mac_key_iv, enc_mac_key,
                              num_keys, key_array, pst, pst_length);
  }
}

extern "C" OEMCryptoResult OEMCrypto_RefreshKeys(
    OEMCrypto_SESSION session, const uint8_t* message, size_t message_length,
    const uint8_t* signature, size_t signature_length, size_t num_keys,
    const OEMCrypto_KeyRefreshObject* key_array) {

  wvcdm::oemprofiler::ProfiledScope ps(
      wvcdm::oemprofiler::OEM_FUNCTION_REFRESH_KEYS);

  if (!kAdapter) return OEMCrypto_ERROR_UNKNOWN_FAILURE;
  LevelSession pair = kAdapter->get(session);
  if (!pair.fcn) return OEMCrypto_ERROR_INVALID_SESSION;
  return pair.fcn->RefreshKeys(pair.session, message, message_length, signature,
                               signature_length, num_keys, key_array);
}

extern "C" OEMCryptoResult OEMCrypto_QueryKeyControl(
    OEMCrypto_SESSION session, const uint8_t* key_id, size_t key_id_length,
    uint8_t* key_control_block, size_t* key_control_block_length) {

  wvcdm::oemprofiler::ProfiledScope ps(
      wvcdm::oemprofiler::OEM_FUNCTION_QUERY_KEY_CONTROL);

  if (!kAdapter) return OEMCrypto_ERROR_UNKNOWN_FAILURE;
  LevelSession pair = kAdapter->get(session);
  if (!pair.fcn) return OEMCrypto_ERROR_INVALID_SESSION;
  if (pair.fcn->version < 10) return OEMCrypto_ERROR_NOT_IMPLEMENTED;
  return pair.fcn->QueryKeyControl(pair.session, key_id, key_id_length,
                                   key_control_block, key_control_block_length);
}

extern "C" OEMCryptoResult OEMCrypto_SelectKey(const OEMCrypto_SESSION session,
                                               const uint8_t* key_id,
                                               size_t key_id_length) {

  wvcdm::oemprofiler::ProfiledScope ps(
      wvcdm::oemprofiler::OEM_FUNCTION_SELECT_KEY);

  if (!kAdapter) return OEMCrypto_ERROR_UNKNOWN_FAILURE;
  LevelSession pair = kAdapter->get(session);
  if (!pair.fcn) return OEMCrypto_ERROR_INVALID_SESSION;
  return pair.fcn->SelectKey(pair.session, key_id, key_id_length);
}

extern "C" OEMCryptoResult OEMCrypto_DecryptCENC(
    OEMCrypto_SESSION session, const uint8_t* data_addr, size_t data_length,
    bool is_encrypted, const uint8_t* iv, size_t offset,
    OEMCrypto_DestBufferDesc* out_buffer,
    const OEMCrypto_CENCEncryptPatternDesc* pattern,
    uint8_t subsample_flags) {

  wvcdm::oemprofiler::ProfiledScope ps(
      wvcdm::oemprofiler::OEM_FUNCTION_DECRYPT_CENC);

  ps.meta_data_.WriteVLV(static_cast<uint64_t>(data_length));

  if (!kAdapter) return OEMCrypto_ERROR_UNKNOWN_FAILURE;
  LevelSession pair = kAdapter->get(session);
  if (!pair.fcn) return OEMCrypto_ERROR_INVALID_SESSION;
  if (pair.fcn->version < 11) {
    return pair.fcn->DecryptCTR_V10(pair.session, data_addr, data_length,
                                    is_encrypted, iv, offset, out_buffer,
                                    subsample_flags);
  } else {
    return pair.fcn->DecryptCENC(pair.session, data_addr, data_length,
                                 is_encrypted, iv, offset, out_buffer, pattern,
                                 subsample_flags);
  }
}

extern "C" OEMCryptoResult OEMCrypto_CopyBuffer(
    const uint8_t* data_addr, size_t data_length,
    OEMCrypto_DestBufferDesc* out_buffer, uint8_t subsample_flags) {
  return OEMCrypto_CopyBuffer(kLevelDefault, data_addr, data_length, out_buffer,
                              subsample_flags);
}

extern "C" OEMCryptoResult OEMCrypto_WrapKeybox(const uint8_t* keybox,
                                                size_t keyBoxLength,
                                                uint8_t* wrappedKeybox,
                                                size_t* wrappedKeyBoxLength,
                                                const uint8_t* transportKey,
                                                size_t transportKeyLength) {

  wvcdm::oemprofiler::ProfiledScope ps(
      wvcdm::oemprofiler::OEM_FUNCTION_WRAP_KEYBOX);

  if (!kAdapter) return OEMCrypto_ERROR_UNKNOWN_FAILURE;
  const FunctionPointers* fcn = kAdapter->get(kLevelDefault);
  if (!fcn) return OEMCrypto_ERROR_INVALID_SESSION;
  return fcn->WrapKeybox(keybox, keyBoxLength, wrappedKeybox,
                         wrappedKeyBoxLength, transportKey, transportKeyLength);
}

extern "C" OEMCryptoResult OEMCrypto_InstallKeybox(const uint8_t* keybox,
                                                   size_t keyBoxLength) {
  return OEMCrypto_InstallKeybox(keybox, keyBoxLength, kLevelDefault);
}

extern "C" OEMCryptoResult OEMCrypto_LoadTestKeybox() {

  wvcdm::oemprofiler::ProfiledScope ps(
      wvcdm::oemprofiler::OEM_FUNCTION_LOAD_TEST_KEYBOX);

  if (!kAdapter) return OEMCrypto_ERROR_UNKNOWN_FAILURE;
  const FunctionPointers* fcn = kAdapter->get(kLevelDefault);
  if (!fcn) return OEMCrypto_ERROR_INVALID_SESSION;
  if (fcn->version < 10) return OEMCrypto_ERROR_NOT_IMPLEMENTED;
  return fcn->LoadTestKeybox();
}

extern "C" OEMCryptoResult OEMCrypto_IsKeyboxValid() {
  return OEMCrypto_IsKeyboxValid(kLevelDefault);
}

extern "C" OEMCryptoResult OEMCrypto_GetDeviceID(uint8_t* deviceID,
                                                 size_t* idLength) {
  return OEMCrypto_GetDeviceID(deviceID, idLength, kLevelDefault);
}

extern "C" OEMCryptoResult OEMCrypto_GetKeyData(uint8_t* keyData,
                                                size_t* keyDataLength) {
  return OEMCrypto_GetKeyData(keyData, keyDataLength, kLevelDefault);
}

extern "C" OEMCryptoResult OEMCrypto_GetRandom(uint8_t* randomData,
                                               size_t dataLength) {

  wvcdm::oemprofiler::ProfiledScope ps(
      wvcdm::oemprofiler::OEM_FUNCTION_GET_RANDOM);

  if (!kAdapter) return OEMCrypto_ERROR_UNKNOWN_FAILURE;
  const FunctionPointers* fcn = kAdapter->get(kLevelDefault);
  if (!fcn) return OEMCrypto_ERROR_INVALID_SESSION;
  return fcn->GetRandom(randomData, dataLength);
}

extern "C" OEMCryptoResult OEMCrypto_RewrapDeviceRSAKey(
    OEMCrypto_SESSION session, const uint8_t* message, size_t message_length,
    const uint8_t* signature, size_t signature_length, const uint32_t* nonce,
    const uint8_t* enc_rsa_key, size_t enc_rsa_key_length,
    const uint8_t* enc_rsa_key_iv, uint8_t* wrapped_rsa_key,
    size_t* wrapped_rsa_key_length) {

  wvcdm::oemprofiler::ProfiledScope ps(
      wvcdm::oemprofiler::OEM_FUNCTION_REWRAP_DEVICE_RSA_KEY);

  if (!kAdapter) return OEMCrypto_ERROR_UNKNOWN_FAILURE;
  LevelSession pair = kAdapter->get(session);
  if (!pair.fcn) return OEMCrypto_ERROR_INVALID_SESSION;
  return pair.fcn->RewrapDeviceRSAKey(
      pair.session, message, message_length, signature, signature_length, nonce,
      enc_rsa_key, enc_rsa_key_length, enc_rsa_key_iv, wrapped_rsa_key,
      wrapped_rsa_key_length);
}

extern "C" OEMCryptoResult OEMCrypto_LoadDeviceRSAKey(
    OEMCrypto_SESSION session, const uint8_t* wrapped_rsa_key,
    size_t wrapped_rsa_key_length) {

  wvcdm::oemprofiler::ProfiledScope ps(
      wvcdm::oemprofiler::OEM_FUNCTION_LOAD_DEVICE_RSA_KEY);

  if (!kAdapter) return OEMCrypto_ERROR_UNKNOWN_FAILURE;
  LevelSession pair = kAdapter->get(session);
  if (!pair.fcn) return OEMCrypto_ERROR_INVALID_SESSION;
  return pair.fcn->LoadDeviceRSAKey(pair.session, wrapped_rsa_key,
                                    wrapped_rsa_key_length);
}

extern "C" OEMCryptoResult OEMCrypto_LoadTestRSAKey() {

  wvcdm::oemprofiler::ProfiledScope ps(
      wvcdm::oemprofiler::OEM_FUNCTION_LOAD_TEST_RSA_KEY);

  if (!kAdapter) return OEMCrypto_ERROR_UNKNOWN_FAILURE;
  const FunctionPointers* fcn = kAdapter->get(kLevelDefault);
  if (!fcn) return OEMCrypto_ERROR_INVALID_SESSION;
  if (fcn->version < 10) return OEMCrypto_ERROR_NOT_IMPLEMENTED;
  return fcn->LoadTestRSAKey();
}

extern "C" OEMCryptoResult OEMCrypto_GenerateRSASignature(
    OEMCrypto_SESSION session, const uint8_t* message, size_t message_length,
    uint8_t* signature, size_t* signature_length,
    RSA_Padding_Scheme padding_scheme) {

  wvcdm::oemprofiler::ProfiledScope ps(
      wvcdm::oemprofiler::OEM_FUNCTION_GENERATE_RSA_SIGNATURE);

  if (!kAdapter) return OEMCrypto_ERROR_UNKNOWN_FAILURE;
  LevelSession pair = kAdapter->get(session);
  if (!pair.fcn) return OEMCrypto_ERROR_INVALID_SESSION;
  if (pair.fcn->version == 8) {
    return pair.fcn->GenerateRSASignature_V8(
        pair.session, message, message_length, signature, signature_length);
  } else {
    return pair.fcn->GenerateRSASignature(pair.session, message, message_length,
                                          signature, signature_length,
                                          padding_scheme);
  }
}

extern "C" OEMCryptoResult OEMCrypto_DeriveKeysFromSessionKey(
    OEMCrypto_SESSION session, const uint8_t* enc_session_key,
    size_t enc_session_key_length, const uint8_t* mac_key_context,
    size_t mac_key_context_length, const uint8_t* enc_key_context,
    size_t enc_key_context_length) {

  wvcdm::oemprofiler::ProfiledScope ps(
      wvcdm::oemprofiler::OEM_FUNCTION_DERIVE_KEYS_FROM_SESSION_KEY);

  if (!kAdapter) return OEMCrypto_ERROR_UNKNOWN_FAILURE;
  LevelSession pair = kAdapter->get(session);
  if (!pair.fcn) return OEMCrypto_ERROR_INVALID_SESSION;
  return pair.fcn->DeriveKeysFromSessionKey(
      pair.session, enc_session_key, enc_session_key_length, mac_key_context,
      mac_key_context_length, enc_key_context, enc_key_context_length);
}

extern "C" uint32_t OEMCrypto_APIVersion() {
  return OEMCrypto_APIVersion(kLevelDefault);
}

extern "C" uint8_t OEMCrypto_Security_Patch_Level() {
  return  OEMCrypto_Security_Patch_Level(kLevelDefault);
}

extern "C" const char* OEMCrypto_SecurityLevel() {
  return OEMCrypto_SecurityLevel(kLevelDefault);
}

extern "C" OEMCryptoResult OEMCrypto_GetHDCPCapability(
    OEMCrypto_HDCP_Capability* current, OEMCrypto_HDCP_Capability* maximum) {
  return OEMCrypto_GetHDCPCapability(kLevelDefault, current, maximum);
}

extern "C" bool OEMCrypto_SupportsUsageTable() {
  return OEMCrypto_SupportsUsageTable(kLevelDefault);
}

extern "C" bool OEMCrypto_IsAntiRollbackHwPresent() {
  return OEMCrypto_IsAntiRollbackHwPresent(kLevelDefault);
}

extern "C" OEMCryptoResult OEMCrypto_GetNumberOfOpenSessions(size_t* count) {
  return OEMCrypto_GetNumberOfOpenSessions(kLevelDefault, count);
}

extern "C" OEMCryptoResult OEMCrypto_GetMaxNumberOfSessions(size_t* maximum) {
  return OEMCrypto_GetMaxNumberOfSessions(kLevelDefault, maximum);
}

extern "C" OEMCryptoResult OEMCrypto_Generic_Encrypt(
    OEMCrypto_SESSION session, const uint8_t* in_buffer, size_t buffer_length,
    const uint8_t* iv, OEMCrypto_Algorithm algorithm, uint8_t* out_buffer) {

  wvcdm::oemprofiler::ProfiledScope ps(
      wvcdm::oemprofiler::OEM_FUNCTION_GENERIC_ENCRYPT);

  if (!kAdapter) return OEMCrypto_ERROR_UNKNOWN_FAILURE;
  LevelSession pair = kAdapter->get(session);
  if (!pair.fcn) return OEMCrypto_ERROR_INVALID_SESSION;
  OEMCryptoResult status = OEMCrypto_SUCCESS;
  std::vector<uint8_t> current_iv(iv, iv + wvcdm::KEY_IV_SIZE);
  while (buffer_length > 0 && status == OEMCrypto_SUCCESS) {
    const size_t chunk_size = std::min(buffer_length,
                                       kMaxGenericEncryptChunkSize);
    status = pair.fcn->Generic_Encrypt(pair.session, in_buffer, chunk_size,
                                       &current_iv[0],
                                       algorithm, out_buffer);
    buffer_length -= chunk_size;
    in_buffer += chunk_size;
    out_buffer += chunk_size;
    if (buffer_length > 0) {
      current_iv.assign(out_buffer - wvcdm::KEY_IV_SIZE,
                        out_buffer);
    }
  }
  return status;
}

extern "C" OEMCryptoResult OEMCrypto_Generic_Decrypt(
    OEMCrypto_SESSION session, const uint8_t* in_buffer, size_t buffer_length,
    const uint8_t* iv, OEMCrypto_Algorithm algorithm, uint8_t* out_buffer) {

  wvcdm::oemprofiler::ProfiledScope ps(
      wvcdm::oemprofiler::OEM_FUNCTION_GENERIC_DECRYPT);

  if (!kAdapter) return OEMCrypto_ERROR_UNKNOWN_FAILURE;
  LevelSession pair = kAdapter->get(session);
  if (!pair.fcn) return OEMCrypto_ERROR_INVALID_SESSION;
  OEMCryptoResult status = OEMCrypto_SUCCESS;
  std::vector<uint8_t> current_iv(iv, iv + wvcdm::KEY_IV_SIZE);
  while (buffer_length > 0 && status == OEMCrypto_SUCCESS) {
    const size_t chunk_size = std::min(buffer_length,
                                       kMaxGenericEncryptChunkSize);
    status =  pair.fcn->Generic_Decrypt(pair.session, in_buffer, chunk_size,
                                        &current_iv[0],
                                        algorithm, out_buffer);
    buffer_length -= chunk_size;
    in_buffer += chunk_size;
    out_buffer += chunk_size;
    if (buffer_length > 0) {
      current_iv.assign(in_buffer - wvcdm::KEY_IV_SIZE,
                        in_buffer);
    }
  }
  return status;
}

extern "C" OEMCryptoResult OEMCrypto_Generic_Sign(OEMCrypto_SESSION session,
                                                  const uint8_t* in_buffer,
                                                  size_t buffer_length,
                                                  OEMCrypto_Algorithm algorithm,
                                                  uint8_t* signature,
                                                  size_t* signature_length) {

  wvcdm::oemprofiler::ProfiledScope ps(
      wvcdm::oemprofiler::OEM_FUNCTION_GENERIC_SIGN);

  if (!kAdapter) return OEMCrypto_ERROR_UNKNOWN_FAILURE;
  LevelSession pair = kAdapter->get(session);
  if (!pair.fcn) return OEMCrypto_ERROR_INVALID_SESSION;
  return pair.fcn->Generic_Sign(pair.session, in_buffer, buffer_length,
                                algorithm, signature, signature_length);
}

extern "C" OEMCryptoResult OEMCrypto_Generic_Verify(
    OEMCrypto_SESSION session, const uint8_t* in_buffer, size_t buffer_length,
    OEMCrypto_Algorithm algorithm, const uint8_t* signature,
    size_t signature_length) {

  wvcdm::oemprofiler::ProfiledScope ps(
      wvcdm::oemprofiler::OEM_FUNCTION_GENERIC_VERIFY);

  if (!kAdapter) return OEMCrypto_ERROR_UNKNOWN_FAILURE;
  LevelSession pair = kAdapter->get(session);
  if (!pair.fcn) return OEMCrypto_ERROR_INVALID_SESSION;
  return pair.fcn->Generic_Verify(pair.session, in_buffer, buffer_length,
                                  algorithm, signature, signature_length);
}

extern "C" OEMCryptoResult OEMCrypto_UpdateUsageTable() {

  wvcdm::oemprofiler::ProfiledScope ps(
      wvcdm::oemprofiler::OEM_FUNCTION_UPDATE_USAGE_TABLE);

  if (!kAdapter) return OEMCrypto_ERROR_UNKNOWN_FAILURE;
  const FunctionPointers* fcn1 = kAdapter->get(kLevelDefault);
  const FunctionPointers* fcn3 = kAdapter->get(kLevel3);
  OEMCryptoResult sts = OEMCrypto_ERROR_NOT_IMPLEMENTED;
  if (fcn3 && fcn3->version > 8) sts = fcn3->UpdateUsageTable();
  if (fcn1 && fcn1 != fcn3 && fcn1->version > 8) sts = fcn1->UpdateUsageTable();
  return sts;
}

extern "C" OEMCryptoResult OEMCrypto_DeactivateUsageEntry(const uint8_t* pst,
                                                          size_t pst_length) {

  wvcdm::oemprofiler::ProfiledScope ps(
      wvcdm::oemprofiler::OEM_FUNCTION_DEACTIVATE_USAGE_ENTRY);

  if (!kAdapter) return OEMCrypto_ERROR_UNKNOWN_FAILURE;
  const FunctionPointers* fcn1 = kAdapter->get(kLevelDefault);
  const FunctionPointers* fcn3 = kAdapter->get(kLevel3);
  OEMCryptoResult sts = OEMCrypto_ERROR_NOT_IMPLEMENTED;
  if (fcn1 && fcn1->version > 8) {
    sts = fcn1->DeactivateUsageEntry(pst, pst_length);
  }
  if (OEMCrypto_SUCCESS != sts) {
    if (fcn3 && fcn1 != fcn3 && fcn3->version > 8) {
      sts = fcn3->DeactivateUsageEntry(pst, pst_length);
    }
  }
  return sts;
}

extern "C" OEMCryptoResult OEMCrypto_ReportUsage(OEMCrypto_SESSION session,
                                                 const uint8_t* pst,
                                                 size_t pst_length,
                                                 OEMCrypto_PST_Report* buffer,
                                                 size_t* buffer_length) {

  wvcdm::oemprofiler::ProfiledScope ps(
      wvcdm::oemprofiler::OEM_FUNCTION_REPORT_USAGE);

  if (!kAdapter) return OEMCrypto_ERROR_UNKNOWN_FAILURE;
  LevelSession pair = kAdapter->get(session);
  if (!pair.fcn) return OEMCrypto_ERROR_INVALID_SESSION;
  if (pair.fcn->version > 8) {
    return pair.fcn->ReportUsage(pair.session, pst, pst_length, buffer,
                                 buffer_length);
  } else {
    return OEMCrypto_ERROR_NOT_IMPLEMENTED;
  }
}

extern "C" OEMCryptoResult OEMCrypto_DeleteUsageEntry(
    OEMCrypto_SESSION session, const uint8_t* pst, size_t pst_length,
    const uint8_t* message, size_t message_length, const uint8_t* signature,
    size_t signature_length) {

  wvcdm::oemprofiler::ProfiledScope ps(
      wvcdm::oemprofiler::OEM_FUNCTION_DELETE_USAGE_ENTRY);

  if (!kAdapter) return OEMCrypto_ERROR_UNKNOWN_FAILURE;
  LevelSession pair = kAdapter->get(session);
  if (!pair.fcn) return OEMCrypto_ERROR_INVALID_SESSION;
  if (pair.fcn->version > 8) {
    return pair.fcn->DeleteUsageEntry(pair.session, pst, pst_length, message,
                                      message_length, signature,
                                      signature_length);
  } else {
    return OEMCrypto_ERROR_NOT_IMPLEMENTED;
  }
}

extern "C" OEMCryptoResult OEMCrypto_ForceDeleteUsageEntry(
    const uint8_t* pst, size_t pst_length) {

  wvcdm::oemprofiler::ProfiledScope ps(
      wvcdm::oemprofiler::OEM_FUNCTION_FORCE_DELETE_USAGE_ENTRY);

  if (!kAdapter) return OEMCrypto_ERROR_UNKNOWN_FAILURE;
  const FunctionPointers* fcn1 = kAdapter->get(kLevelDefault);
  const FunctionPointers* fcn3 = kAdapter->get(kLevel3);
  OEMCryptoResult sts = OEMCrypto_ERROR_NOT_IMPLEMENTED;
  if (fcn3 && fcn3->version > 9) {
    sts = fcn3->ForceDeleteUsageEntry(pst, pst_length);
  }
  if (fcn1 && fcn1 != fcn3 && fcn1->version > 9) {
    OEMCryptoResult sts1 = fcn1->ForceDeleteUsageEntry(pst, pst_length);
    if ((sts1 != OEMCrypto_SUCCESS) && (sts == OEMCrypto_SUCCESS)) {
      sts = sts1;
    }
  }
  return sts;
}

extern "C" OEMCryptoResult OEMCrypto_DeleteUsageTable() {

  wvcdm::oemprofiler::ProfiledScope ps(
      wvcdm::oemprofiler::OEM_FUNCTION_DELETE_USAGE_TABLE);

  if (!kAdapter) return OEMCrypto_ERROR_UNKNOWN_FAILURE;
  const FunctionPointers* fcn1 = kAdapter->get(kLevelDefault);
  const FunctionPointers* fcn3 = kAdapter->get(kLevel3);
  OEMCryptoResult sts = OEMCrypto_ERROR_NOT_IMPLEMENTED;
  if (fcn3 && fcn3->version > 8) sts = fcn3->DeleteUsageTable();
  if (fcn1 && fcn1 != fcn3 && fcn1->version > 8) sts = fcn1->DeleteUsageTable();
  return sts;
}
