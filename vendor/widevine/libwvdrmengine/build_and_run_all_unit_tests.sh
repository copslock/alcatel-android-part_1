#!/bin/sh

set -e

if [ -z "$ANDROID_BUILD_TOP" ]; then
    echo "Android build environment not set"
    exit -1
fi

. $ANDROID_BUILD_TOP/build/envsetup.sh

cd $ANDROID_BUILD_TOP/vendor/widevine/libwvdrmengine
pwd
mm || mma

cd $ANDROID_BUILD_TOP/vendor/widevine/libwvdrmengine/test/unit
pwd
mm || mma

cd $ANDROID_BUILD_TOP/vendor/widevine/libwvdrmengine/cdm/test
pwd
mm || mma

cd $ANDROID_BUILD_TOP/vendor/widevine/libwvdrmengine/mediacrypto/test
pwd
mm || mma

cd $ANDROID_BUILD_TOP/vendor/widevine/libwvdrmengine/mediadrm/test
pwd
mm || mma

cd $ANDROID_BUILD_TOP/vendor/widevine/libwvdrmengine/oemcrypto/test
pwd
mm || mma

cd $ANDROID_BUILD_TOP/vendor/widevine/libwvdrmengine/test/java/src/com/widevine/test
pwd
mm WITH_DEXPREOPT=false || mma WITH_DEXPREOPT=false

echo "waiting for device"
adb root && adb wait-for-device remount

try_adb_push() {
  # Swallow the error message -- assume it is a read-only file system.
  if ! adb push $@ /system/bin > /dev/null; then
    adb shell mkdir -p /data/widevine_tests
    # If this fails, the user will get the error message.
    adb push $@ /data/widevine_tests
  fi
}

try_adb_push $OUT/system/bin/oemcrypto_test
try_adb_push $OUT/system/bin/request_license_test
try_adb_push $OUT/system/bin/cdm_extended_duration_test
try_adb_push $OUT/system/bin/max_res_engine_unittest
try_adb_push $OUT/system/bin/policy_engine_unittest
try_adb_push $OUT/system/bin/libwvdrmmediacrypto_test
try_adb_push $OUT/system/bin/libwvdrmdrmplugin_test
try_adb_push $OUT/system/bin/cdm_engine_test
try_adb_push $OUT/system/bin/cdm_session_unittest
try_adb_push $OUT/system/bin/file_store_unittest
try_adb_push $OUT/system/bin/license_unittest
try_adb_push $OUT/system/bin/initialization_data_unittest
try_adb_push $OUT/system/bin/device_files_unittest
try_adb_push $OUT/system/bin/timer_unittest
try_adb_push $OUT/system/bin/libwvdrmengine_test
try_adb_push $OUT/system/bin/buffer_reader_test
try_adb_push $OUT/system/bin/circular_buffer_test
try_adb_push $OUT/system/bin/entry_writer_test
adb install -r $OUT/system/app/MediaDrmAPITest/MediaDrmAPITest.apk

cd $ANDROID_BUILD_TOP/vendor/widevine/libwvdrmengine
./run_all_unit_tests.sh
