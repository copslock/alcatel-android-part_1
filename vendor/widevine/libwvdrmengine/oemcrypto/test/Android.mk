LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE:=oemcrypto_test
LOCAL_MODULE_TAGS := tests

LOCAL_MODULE_TARGET_ARCH := arm x86 mips

include $(LOCAL_PATH)/common.mk

include $(BUILD_EXECUTABLE)
