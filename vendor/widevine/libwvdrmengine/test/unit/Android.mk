LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_SRC_FILES := \
  WVCreatePluginFactories_test.cpp \
  WVCryptoFactory_test.cpp \
  WVDrmFactory_test.cpp \

LOCAL_C_INCLUDES := \
  external/gtest/include \
  frameworks/av/include \
  frameworks/native/include \
  vendor/widevine/libwvdrmengine/include \
  vendor/widevine/libwvdrmengine/mediadrm/include \
  vendor/widevine/libwvdrmengine/oemcrypto/include \

LOCAL_STATIC_LIBRARIES := \
  libcrypto_static \
  libgtest \
  libgtest_main \

LOCAL_SHARED_LIBRARIES := \
  libdl \
  liblog \
  libutils \
  libwvdrmengine \

LOCAL_MODULE := libwvdrmengine_test

LOCAL_MODULE_TAGS := tests

LOCAL_MODULE_TARGET_ARCH := arm x86 mips

include $(BUILD_EXECUTABLE)
