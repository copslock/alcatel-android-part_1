#  This library does not build on mips64.
ifneq (mips64,$(TARGET_ARCH))
# -----------------------------------------------------------------------------
# CDM top level makefile
#
LOCAL_PATH := $(call my-dir)

# -----------------------------------------------------------------------------
# Builds libcdm_utils.a
#
include $(CLEAR_VARS)

LOCAL_MODULE := libcdm_utils
LOCAL_MODULE_CLASS := STATIC_LIBRARIES

LOCAL_C_INCLUDES := \
    vendor/widevine/libwvdrmengine/cdm/core/include \
    vendor/widevine/libwvdrmengine/cdm/include \
    vendor/widevine/libwvdrmengine/oemcrypto/include \
    vendor/widevine/libwvdrmengine/third_party/stringencoders/src

SRC_DIR := cdm/src
CORE_SRC_DIR := cdm/core/src
LOCAL_SRC_FILES := \
    third_party/stringencoders/src/modp_b64.cpp \
    third_party/stringencoders/src/modp_b64w.cpp \
    $(CORE_SRC_DIR)/properties.cpp \
    $(CORE_SRC_DIR)/string_conversions.cpp \
    $(SRC_DIR)/clock.cpp \
    $(SRC_DIR)/file_store.cpp \
    $(SRC_DIR)/lock.cpp \
    $(SRC_DIR)/log.cpp \
    $(SRC_DIR)/properties_android.cpp \
    $(SRC_DIR)/timer.cpp \

include $(BUILD_STATIC_LIBRARY)

# -----------------------------------------------------------------------------
# Builds libcdm_protos.a
# Generates *.a, *.pb.h and *.pb.cc for *.proto files.
#
include $(CLEAR_VARS)

LOCAL_MODULE := libcdm_protos
LOCAL_MODULE_CLASS := STATIC_LIBRARIES

LOCAL_SRC_FILES := $(call all-proto-files-under, cdm/core/src)

generated_sources_dir := $(call local-generated-sources-dir)

# $(generated_sources_dir)/proto/$(LOCAL_PATH)/cdm/core/src is used
# to locate *.pb.h by cdm source
# $(generated_sources_dir)/proto is used to locate *.pb.h included
# by *.pb.cc
# The module that depends on this library will have LOCAL_C_INCLUDES prepended
# with this path.
LOCAL_EXPORT_C_INCLUDE_DIRS := \
    $(generated_sources_dir)/proto \
    $(generated_sources_dir)/proto/$(LOCAL_PATH)/cdm/core/src

include $(BUILD_STATIC_LIBRARY)

# -----------------------------------------------------------------------------
# Builds libwvdrmengine.so
#
include $(CLEAR_VARS)

LOCAL_SRC_FILES := \
  src/WVCDMSingleton.cpp \
  src/WVCreatePluginFactories.cpp \
  src/WVCryptoFactory.cpp \
  src/WVDrmFactory.cpp \
  src/WVUUID.cpp

LOCAL_C_INCLUDES := \
  frameworks/av/include \
  frameworks/native/include \
  vendor/widevine/libwvdrmengine/cdm/core/include \
  vendor/widevine/libwvdrmengine/cdm/profiler/include \
  vendor/widevine/libwvdrmengine/cdm/include \
  vendor/widevine/libwvdrmengine/include \
  vendor/widevine/libwvdrmengine/mediacrypto/include \
  vendor/widevine/libwvdrmengine/mediadrm/include \
  vendor/widevine/libwvdrmengine/oemcrypto/include \

LOCAL_STATIC_LIBRARIES := \
  libcdm \
  libcdm_utils \
  libcrypto_static \
  libjsmn \
  libwvlevel3 \
  libwvdrmcryptoplugin \
  libwvdrmdrmplugin \

LOCAL_SHARED_LIBRARIES := \
  libcutils \
  libdl \
  liblog \
  libprotobuf-cpp-lite \
  libstagefright_foundation \
  libutils \

LOCAL_WHOLE_STATIC_LIBRARIES := libcdm_protos

LOCAL_MODULE := libwvdrmengine

LOCAL_MODULE_RELATIVE_PATH := mediadrm

LOCAL_MODULE_TAGS := optional

LOCAL_MODULE_OWNER := widevine

LOCAL_PROPRIETARY_MODULE := true

LOCAL_MODULE_TARGET_ARCH := arm x86 mips

include $(BUILD_SHARED_LIBRARY)

include vendor/widevine/libwvdrmengine/cdm/Android.mk
include vendor/widevine/libwvdrmengine/level3/Android.mk
include vendor/widevine/libwvdrmengine/mediacrypto/Android.mk
include vendor/widevine/libwvdrmengine/mediadrm/Android.mk

endif # if target is mips64.
