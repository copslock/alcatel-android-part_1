# Build rules for tests to be included in the google_tests.zip package
# are discovered by having an Android.mk file located in vendor/<project>/tests/.

LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

# Targets to be included in google_tests.zip
# Referenced by vendor/google/build/tasks/google_tests.mk
WIDEVINE_TEST_MAKE_TARGETS := \
    cdm_engine_test \
    cdm_extended_duration_test \
    cdm_session_unittest \
    device_files_unittest \
    file_store_unittest \
    initialization_data_unittest \
    libwvdrmdrmplugin_test \
    libwvdrmengine_test \
    libwvdrmmediacrypto_test \
    license_unittest \
    max_res_engine_unittest \
    MediaDrmAPITest \
    oemcrypto_test \
    policy_engine_unittest \
    request_license_test \
    timer_unittest \

LOCAL_XTS_PATH := vendor/xts/xts-tests/tests/media/Android.mk
# Need to make sure LOCAL_XTS_PATH actually exists in the build environment.
# The gnu wildcard function will return a space-separated list of existing
# files that match one of the given file name patterns. If no existing file
# name matches, then that pattern is omitted from the output of the wildcard
# function. For more details, see:
# https://www.gnu.org/software/make/manual/html_node/Wildcard-Function.html
_xts_makefile := $(strip $(wildcard $(LOCAL_XTS_PATH)))
ifdef _xts_makefile
    WIDEVINE_TEST_MAKE_TARGETS += XtsMediaTestCases
    include $(_xts_makefile)
endif

# Call the makefiles for all our tests used in TradeFederation
MODULAR_DRM_PATH := vendor/widevine/libwvdrmengine
include $(MODULAR_DRM_PATH)/cdm/test/Android.mk
include $(MODULAR_DRM_PATH)/mediacrypto/test/Android.mk
include $(MODULAR_DRM_PATH)/mediadrm/test/Android.mk
include $(MODULAR_DRM_PATH)/oemcrypto/test/Android.mk
include $(MODULAR_DRM_PATH)/test/unit/Android.mk
include $(MODULAR_DRM_PATH)/test/java/Android.mk
