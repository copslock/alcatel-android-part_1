/*
 * (c)Copyright 2011 Google Inc.
 */

package com.widevine.demo;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

public class SettingsFragment extends Fragment {
    // public static String CONTENT_PAGE = "/sdcard/Widevine/config.xml";
    public static String CONTENT_PAGE = "http://storage.googleapis.com/wvmedia/html/oem.html";

    private Button updateButton;
    private EditText drmServer, portalName, deviceId, contentPage;

    public static final String TAG = "WVM Player Settings";

    @Override
    public View onCreateView(LayoutInflater inflater,
            ViewGroup container, Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        View rootView = inflater.inflate(R.layout.settings, container, false);

        updateButton = (Button)rootView.findViewById(R.id.save_settings);

        View.OnClickListener clickListener = new View.OnClickListener() {

            public void onClick(View v) {
                Log.d(TAG, "Click update settings");
                WidevineDrm.Settings.DRM_SERVER_URI = drmServer.getText().toString();
                WidevineDrm.Settings.DEVICE_ID = deviceId.getText().toString();
                WidevineDrm.Settings.PORTAL_NAME = portalName.getText().toString();
                SettingsFragment.CONTENT_PAGE = contentPage.getText().toString();

                AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
                builder.setMessage("DRM Settings Updated").setCancelable(false)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Log.d(TAG, "Click DRM Settings OK.");
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        };

        updateButton.setOnClickListener(clickListener);

        drmServer = (EditText)rootView.findViewById(R.id.drm_server);
        drmServer.setText(WidevineDrm.Settings.DRM_SERVER_URI);

        deviceId = (EditText)rootView.findViewById(R.id.device_id);
        deviceId.setText(WidevineDrm.Settings.DEVICE_ID);

        portalName = (EditText)rootView.findViewById(R.id.portal_id);
        portalName.setText(WidevineDrm.Settings.PORTAL_NAME);

        contentPage = (EditText)rootView.findViewById(R.id.content_page);
        contentPage.setText(SettingsFragment.CONTENT_PAGE);

        return rootView;
    }
}
