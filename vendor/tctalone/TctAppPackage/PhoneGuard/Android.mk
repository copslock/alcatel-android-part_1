LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE := PhoneGuard
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := $(patsubst $(LOCAL_PATH)/%,%,$(shell find $(LOCAL_PATH) -name "PhoneGuard*.apk"))
LOCAL_MODULE_CLASS := APPS
LOCAL_CERTIFICATE := platform
LOCAL_PRIVILEGED_MODULE := true
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := $(call all-java-files-under, src)
LOCAL_PACKAGE_NAME := PhoneGuardRes
LOCAL_MODULE_STEM := PhoneGuard-overlay
LOCAL_MODULE_PATH := $(TARGET_OUT)/vendor/overlay
IS_INDEPENDENT_APP := true
include $(BUILD_PLF)
include $(BUILD_PACKAGE)
