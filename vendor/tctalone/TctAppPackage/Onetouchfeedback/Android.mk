LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE := OneTouchFeedback
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := $(patsubst $(LOCAL_PATH)/%,%,$(shell find $(LOCAL_PATH) -name "OneTouchFeedback*.apk"))
LOCAL_MODULE_CLASS := APPS
LOCAL_CERTIFICATE := platform
LOCAL_PRIVILEGED_MODULE := true
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := $(call all-java-files-under, src)
LOCAL_PACKAGE_NAME := OneTouchFeedbackRes
LOCAL_MODULE_STEM := OneTouchFeedback-overlay
LOCAL_MODULE_PATH := $(TARGET_OUT)/vendor/overlay
IS_INDEPENDENT_APP := true
include $(BUILD_PLF)
include $(BUILD_PACKAGE)
