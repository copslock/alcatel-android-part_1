import lldb
import xml.dom.minidom as dom


def __lldb_init_module (debugger, dict):
    debugger.HandleCommand('command script add -f hexagon_utils.pagetable_read pagetable')
    debugger.HandleCommand('command script add -f hexagon_utils.tlb_read tlb')
    debugger.HandleCommand('command script add -f hexagon_utils.pv8 pv8')
    debugger.HandleCommand('command script add -f hexagon_utils.pv16 pv16')
    debugger.HandleCommand('command script add -f hexagon_utils.pv32 pv32')
    print "Hexagon utilities (pagetable, tlb, pv) loaded"

def print_xml_table(root, result):
    itemlist = root.getElementsByTagName('item')
    if (not itemlist):
        print >>result, "Error: no data received"
        return
    line = ""
    for column in itemlist[0].getElementsByTagName('column'):
        line += '%10s' % column.getAttribute('name')
    print >>result, str(line)
    for item in itemlist:
        line = ""
        for column in item.getElementsByTagName('column'):
            line += '%10s' % column.childNodes[0].nodeValue
        print >>result, str(line)

def pagetable_read(debugger, register, result, dict):
    xml_root = page_tlb_read(debugger, "qXfer:osdata:read:pagetable:", result)
    if (xml_root != None):
        print_xml_table(xml_root, result)

def tlb_read(debugger, register, result, dict):
    xml_root = page_tlb_read(debugger, "qXfer:osdata:read:tlbinfo:", result)
    if (xml_root != None):
        print_xml_table(xml_root, result)

def page_tlb_read(debugger, rsp_string, result):
    l_found = 0
    offset = 0
    xml_string = ""
    while (l_found == 0):
        debugger.GetCommandInterpreter().HandleCommand('process plugin packet send "' + rsp_string + '{:x}'.format(offset) + ',2bb"', result)
        output = result.GetOutput()
        result.Clear()
        output_offset = output.find("response: ") + 10
        cur_line = output[output_offset:len(output) - 2] # -2 to remove \n
        offset += len(cur_line) - 1
        if (cur_line[0] == 'l'): l_found = 1
        if (cur_line[0:5] == 'ERROR:'): l_found = 1
        xml_string += cur_line[1:]
    try:
        xml_root = dom.parseString(xml_string)
    except:
        print xml_string
        result.SetError("***Unable to parse xml data from simulator!")
        result.SetStatus(lldb.eReturnStatusFailed)
        return None
    return xml_root

def pv(debugger, register, result, size):
    if (not register):
        result.Clear()
        result.SetError("must specify a vector register")
        result.SetStatus(lldb.eReturnStatusFailed)
        return
    if (not register[0] in 'vV'):
        result.Clear()
        result.SetError("register " + register + " is not a vector register")
        result.SetStatus(lldb.eReturnStatusFailed)
        return
    debugger.GetCommandInterpreter().HandleCommand('register read ' + register, result)
    error = result.GetError()
    if (error != ""):
        return
    output = result.GetOutput()
    result.Clear()
    output_offset = output.find("0x") + 2
    line = register + " ="
    temp_regval = output[output_offset:]
    numbytes = size / 8
    vector_length = len(temp_regval) / 2
    for index in range (0, vector_length / numbytes):
        printme = temp_regval[0:numbytes * 2]
        line += " 0x" + printme
        temp_regval = temp_regval[numbytes * 2:]
    print >>result, line
 
def pv8(debugger, register, result, dict):
    pv(debugger, register, result, 8)

def pv16(debugger, register, result, dict):
    pv(debugger, register, result, 16)

def pv32(debugger, register, result, dict):
    pv(debugger, register, result, 32)


