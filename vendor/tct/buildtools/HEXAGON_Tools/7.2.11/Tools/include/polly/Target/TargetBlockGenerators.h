//===---- TargetBlockGenerators.h - Code generate the Scops for Targets ---===//
//
//                     The LLVM Compiler Infrastructure
//
// (c) 2013 Qualcomm Innovation Center, Inc. All rights reserved.
//===----------------------------------------------------------------------===//
//
//
//
//===----------------------------------------------------------------------===//

#include "llvm/ADT/SetVector.h"
#include "polly/CodeGen/BlockGenerators.h"
#include "polly/CodeGen/IRBuilder.h"

namespace llvm {
class Pass;
class ScalarEvolution;
}

namespace polly {
using namespace llvm;
class ScopStmt;

class TargetBlockGenerator : protected VectorBlockGenerator {

protected:
  TargetBlockGenerator(PollyIRBuilder &B, VectorValueMapT &GlobalMaps,
                       std::vector<LoopToScevMapT> &VLTS, ScopStmt &Stmt,
                       __isl_keep isl_map *Schedule, Pass *P,
                       MDNode *ParallelMD, MapValueVectorT &Reductions,
                       const SumOfSums &Sum2, Value *InductionMap[],
                       bool AddAlignment, const DataLayout *Td)
      : VectorBlockGenerator(B, GlobalMaps, VLTS, Stmt, Schedule, P, ParallelMD,
                             Reductions, Sum2, AddAlignment, Td),
        InductionMap(InductionMap), AccessMap(MemoryMapsT(16)) {}

  virtual ~TargetBlockGenerator() {}

  Value **InductionMap;

  /// @brief Track multistrided accesses.
  MemoryMapsT AccessMap;

  /// @brief Track reduction binary operations.
  SetVector<const Value *> ReductionOps;

  bool hasVectorOperands(const Instruction *Inst, ValueMapT &ScalarMap,
                         VectorValueMapT &VectorMaps,
                         bool *HasBothVectorAndScalarMap, int *Idx);

  bool extractScalarValues(const Instruction *Inst, VectorValueMapT &VectorMap,
                           VectorValueMapT &ScalarMaps);

public:
  void setElementsPerVector(Type *ScalarType, int *Index);

  // Vector primitive support functions.

  /// @brief Combines half vectors into full vectors.
  /// @param Vectors       an array of half vectors.
  /// @param NumVectors    number of vectors.
  ///
  /// Each consecutive pair of vectors is combined into one full vector.
  /// Returns NumVector / 2.
  int combineHalfVectors(Value *Vectors[], int NumVectors);

  /// @brief Split vectors in two halves.
  /// @param Vectors       an array of full vectors.
  /// @param NumVectors    number of vectors.
  ///
  /// Each vector is split in two halves.
  /// Returns 2 * NumVector.
  int splitVectorsIntoHalves(Value *Opnd[], int NumVectors);

  /// Merge two halves (or pad when Opnd2 == nullptr) to form one vector.
  Value *mergeHalves(Value *Opnd, Value *Opnd2 = nullptr);

  Value *getHalf(Value *Opnd, unsigned i = 0);

  /// Sign/zero extend each element of the vector to double its size.
  Value *extendVector(Value *Opnd, llvm::Instruction::CastOps Opcode);

  /// Truncate each element of the vector to half it size.
  Value *truncateVector(Value *Opnd, llvm::Instruction::CastOps Opcode);

  Type *getScalarTypeFromIndex(int Index);

  int getIndex(const Value *Oper, VectorValueMapT &VectorMaps);
};
}
