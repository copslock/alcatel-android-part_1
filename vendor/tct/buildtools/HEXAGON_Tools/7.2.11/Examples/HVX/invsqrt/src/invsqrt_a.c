/* ======================================================================== */
/*  QUALCOMM TECHNOLOGIES, INC.                                             */
/*                                                                          */
/*  HEXAGON HVX Image/Video Processing Library                              */
/*                                                                          */
/* ------------------------------------------------------------------------ */
/*          Copyright (c) 2014 QUALCOMM TECHNOLOGIES Incorporated.          */
/*                           All Rights Reserved.                           */
/*                  QUALCOMM Confidential and Proprietary                   */
/* ======================================================================== */

/*[========================================================================]*/
/*[ FUNCTION                                                               ]*/
/*[     invsqrt                                                            ]*/
/*[                                                                        ]*/
/*[------------------------------------------------------------------------]*/
/*[ DESCRIPTION                                                            ]*/
/*[     This function computes 1 / squareroot(x) using interpolation.      ]*/
/*[     Input is in unsigned 16 bits while the output is in Q12 format.    ]*/
/*[     If zero is passed in, the function will return largest number in   ]*/
/*[     Q12 format.                                                        ]*/
/*[                                                                        ]*/
/*[------------------------------------------------------------------------]*/
/*[ REVISION DATE                                                          ]*/
/*[     AUG-01-2014                                                        ]*/
/*[                                                                        ]*/
/*[========================================================================]*/
#include <assert.h>
#include <stdlib.h>
#include "hvx.cfg.h"
#include "invsqrt.h"
#include "io.h"

/* ======================================================================== */
/*  Tables used in Assembly                                                 */
/* ======================================================================== */

unsigned short val_table[24] __attribute__((aligned(VLEN))) = {
    4096, 3862, 3664, 3493, 3344, 3213, 3096, 2991,
    2896, 2810, 2731, 2658, 2591, 2528, 2470, 2416,
    2365, 2317, 2272, 2230, 2189, 2151, 2115, 2081
};

unsigned char slope_table[24] __attribute__((aligned(VLEN))) = {
    234, 198, 171, 149, 131, 117, 105, 95,
    86, 79, 73, 67, 63, 58, 54, 51,
    48, 45, 42, 41, 38, 36, 34, 33
};
