##################################################################
# Copyright (c) $Date$ QUALCOMM INCORPORATED.
# All Rights Reserved.
# Modified by QUALCOMM INCORPORATED on $Date$
##################################################################

Hexify is a tool written in perl and only works in linux. The objective of
this tool is to help developers with porting their codebase that was built
using earlier hexagon toolchain versions prior to 7.x to the 7.x versions.

The objective is to provide a tool that converts existing projects using
older symbols to the latest naming convention with minimal effort.  This is
demonstrated in the myTestCase/q6_to_hexagon.txt example.  This example will
run the hexify perl script on the q6_to_hexagon.txt file, compare each string
with the Rep_List structure in the hexify perl script and convert the lines
to the new naming convention.

The intent is to limit the changes to only Qualcomm defined symbols.
These are some restrictions on what text is replaced:
  *  Symbols must be a bounded 100% match.
     The symbol text must match and be bounded by token separators such as
     blanks, punctuation or line boundaries.

The hexify script should be run from a linux shell only.  The script will
create an output directory structure identical to the original directory tree
you want converted in the destination folder.  Only files that are modified
files will be placed in the new folder area.

It is strongly recommended that hexify only be run on a *copy* of your source
tree.

The user has the responsiblity to verify that the changes made are appropriate.
Following verification, the output directory tree should be copied over the
orignial.

Qualcomm specific symbols defined in the distributed .h files or as Qualcomm
symbols known to the assembler or compiler are the targets for replacement.

Note that in the shift from 6.x to 7.x some tools related paths changed.
Hexify will convert those folder paths as well as strings in files when found.

An example file in myTestCase/q6_to_hexagon.txt demonstrates the symbols which
are part of the Rep_List structure in the hexify perl script.


Usage: hexify [<options>] <dirname> <dest dirname>

    Default (no options) enables mappings for:
     * Executable names
     * Library names
     * File paths
     * Command line options

    Possible options for hexify are:
       --full           Use all mappings.  Adds these mappings
                           * Header files (.h)
                           * Macro names
                           * Symbol names
       --translations   Output translations to STDERR
	                   (output affected by --full)
       --usage          Usage text output.
       --help           This text

    --full adds these additional mappings

  Assumes:
    * Destination directory exists and is writeable.
    * Any directory start with '.' (like .git) will be ignored.
    * The user must assess:
      1. If the changes made are appropriate
      2. If the changed file(s) should be copied back to the original source
         tree.

The example Makefile will read each file in the myTestCase folder and convert
all references to prior naming conventions to hexagon naming convention.
Files that need to be changed will be copied to a duplicate folder path as
specified on the command line.
