//===--- SCEVRangeAnalysis.h - Compute bounds on SCEV values ----*- C++ -*-===//
//
// (C) 2013 Qualcomm Innovation Center, Inc. All rights reserved.
//
//===----------------------------------------------------------------------===//
// Checks if a SCEV expression can be bounded by a value range
//===----------------------------------------------------------------------===//


#ifndef POLLY_SCEV_RANGE_ANALYSIS_H
#define POLLY_SCEV_RANGE_ANALYSIS_H

namespace llvm {
class SCEV;
class Loop;
}

namespace polly {

// Returns true if the loop trip count can be proven to be small
bool isSmallTripCount(const llvm::Loop *L,
                      const llvm::SCEV *BackedgeTakenCount);
}

#endif
