//===---- HexagonCodeGeneration.h - Code generate the Scops for Hexagon ---===//
//
//                     The LLVM Compiler Infrastructure
//
//===----------------------------------------------------------------------===//
//
// This files defines HexagonBlockGenerator class, an extension of  Polly
// TargetBlockGenerator. This class targets Hexagon vector instructions.
//
//
//===----------------------------------------------------------------------===//

#include "polly/CodeGen/IRBuilder.h"
#include "polly/Target/TargetBlockGenerators.h"
#include "llvm/Analysis/LoopInfo.h"

namespace llvm {
class Pass;
class ScalarEvolution;
}

namespace polly {
extern bool EnablePartialSums;
using namespace llvm;
class ScopStmt;

class HexagonBlockGenerator : TargetBlockGenerator {
public:
  static void generateHexagon(
      PollyIRBuilder &B, ScopStmt &Stmt, VectorValueMapT &GlobalMaps,
      std::vector<LoopToScevMapT> &VLTS, __isl_keep isl_map *Schedule, Pass *P,
      MapValueVectorT &Reductions, const SumOfSums &Sum2, Value *InductionMap[],
      bool AddAlignment, const DataLayout *Td, MDNode *ParallelMD) {
    HexagonBlockGenerator Generator(B, GlobalMaps, VLTS, Stmt, Schedule, P,
                                    ParallelMD, Reductions, Sum2, InductionMap,
                                    AddAlignment, Td);

    Generator.copyBB();
  }

protected:
  LoopInfo *LI;

  HexagonBlockGenerator(PollyIRBuilder &B, VectorValueMapT &GlobalMaps,
                        std::vector<LoopToScevMapT> &VLTS, ScopStmt &Stmt,
                        __isl_keep isl_map *Schedule, Pass *P,
                        MDNode *ParallelMD, MapValueVectorT &Reductions,
                        const SumOfSums &Sum2, Value *InductionMap[],
                        bool AddAlignment, const DataLayout *Td)
      : TargetBlockGenerator(B, GlobalMaps, VLTS, Stmt, Schedule, P, ParallelMD,
                             Reductions, Sum2, InductionMap, AddAlignment, Td) {
    LI = &P->getAnalysis<LoopInfo>();
  }

  ~HexagonBlockGenerator() {}

  /// copyUnaryInst vectorizes type conversion operation. It performs this
  /// operation so that it never produces a vector that exceeds the size
  /// of the register size. For example to convert <16 x i8> to <16 x i32>,
  /// it emits the following code:
  ///
  /// lo = shufflevector src, src, < 0, 1, 2, 3, 4, 5, 6, 7 >
  /// loTo16 = sext <8 x i8> lo to <8 x i16>
  /// hi = shufflevector src, src, < 7, 8, 9, 10, 11, 12, 13, 14, 15 >
  /// hiTo16 = sext <8 x i8> hi to <8 x i16>
  /// lo_lo = shufflevector loTo16, lo_to16, < 0, 1, 2, 3 >
  /// lo_loTo32 = sext <4 x i16> lo_lo to <4 x i32>
  /// lo_hi = shufflevector loTo16, lo_to16, < 4, 5, 6, 7 >
  /// lo_hiTo32 = sext <4 x i16> lo_hi to <4 x i32>
  /// hi_lo = shufflevector hiTo16, hi_to16, < 0, 1, 2, 3 >
  /// hi_loTo32 = sext <4 x i16> hi_lo to <4 x i32>
  /// hi_hi = shufflevector hiTo16, lo_to16, < 4, 5, 6, 7 >
  /// hi_hiTo32 = sext <4 x i16> hi_hi to <4 x i32>
  ///
  /// Four vector values are generated to fit <16 x i32>.
  void copyUnaryInst(const UnaryInstruction *Inst, VectorValueMapT &VectorMap,
                     VectorValueMapT &ScalarMaps, int Idx);

  /// coptInstructions decides which instruction can be vectorized by Hexagon
  /// and if the instruction is vecotrized then it emits the code so that the
  /// input/output vector values fit in a Hexagon 128-bits register.
  void copyInstruction(const Instruction *Inst, VectorValueMapT &VectorMap,
                       VectorValueMapT &ScalarMaps);

  virtual void copyBB();

private:
  // Vector primitive support functions.

  Value *HexagonGetVectorValue(const Value *Old, ValueMapT &VectorMap,
                               VectorValueMapT &ScalarMaps, Loop *L);

  Value *HexagonGetCustomVectorValue(const Value *Old, ValueMapT &VectorMap,
                                     VectorValueMapT &ScalarMaps, Loop *L,
                                     int Width, Type *OpType);

  Value *generateCustomMAC(const BinaryOperator *Inst, Value *In0, Value *In1,
                           const Instruction *Trunc, const Instruction *DepAdd,
                           Value *AddIn, VectorValueMapT &VectorMap,
                           VectorValueMapT &ScalarMaps);

  Value *generateCustomShift(const BinaryOperator *Inst, Type *ResultType,
                             Function *F, int Idx, VectorValueMapT &VectorMap,
                             VectorValueMapT &ScalarMaps);

  void HexagonCopyBinaryInst(const BinaryOperator *Inst, ValueMapT &VectorMap,
                             VectorValueMapT &ScalarMaps);

  void HexagonCopySelectInst(const SelectInst *Inst, ValueMapT &VectorMap,
                             VectorValueMapT &ScalarMaps);

  bool HexagonReduceBinaryInst(const BinaryOperator *Inst,
                               VectorValueMapT &VectorMap,
                               VectorValueMapT &ScalarMaps);

  void extendVectorOperand(const Value *Oper, VectorValueMapT &VectorMaps,
                           llvm::Instruction::CastOps Opcode, int Idx);

  int mapOperand(const Value *Oper, VectorValueMapT &VectorMaps,
                 VectorValueMapT &ScalarMaps, Loop *L,
                 CastInst *CastOp = nullptr,
                 const BinaryOperator *Binary = nullptr);
};
}
