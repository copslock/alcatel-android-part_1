//=== ScopDetectionDiagnostic.h -- Diagnostic for ScopDetection -*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// Small set of diagnostic helper classes to encapsulate any errors occurred
// during the detection of Scops.
//
// The ScopDetection defines a set of error classes (via Statistic variables)
// that groups a number of individual errors into a group, e.g. non-affinity
// related errors.
// On error we generate an object that carries enough additional information
// to diagnose the error and generate a helpful error message.
//
//===----------------------------------------------------------------------===//
#ifndef POLLY_SCOP_DETECTION_DIAGNOSTIC_H
#define POLLY_SCOP_DETECTION_DIAGNOSTIC_H

#include "llvm/Analysis/LoopInfo.h"
#include "llvm/Analysis/AliasSetTracker.h"
#include "llvm/Analysis/ScalarEvolution.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Value.h"
#include "llvm/ADT/SmallVector.h"
#include "llvm/ADT/Statistic.h"
#include "llvm/ADT/Twine.h"
#include "llvm/Analysis/ScalarEvolutionExpressions.h"

namespace llvm {
  class OptReporter;
}

using namespace llvm;

namespace llvm {
class SCEV;
class BasicBlock;
class Value;
class Region;
}


namespace polly {

/// @brief Get the location of a region from the debug info.
///
/// @param R The region to get debug info for.
/// @param LineBegin The first line in the region.
/// @param LineEnd The last line in the region.
/// @param FileName The filename where the region was defined.
void getDebugLocation(const Region *R, unsigned &LineBegin, unsigned &LineEnd,
                      std::string &FileName);

//===----------------------------------------------------------------------===//
/// @brief Base class of all reject reasons found during Scop detection.
///
/// Subclasses of RejectReason should provide means to capture enough
/// diagnostic information to help clients figure out what and where something
/// went wrong in the Scop detection.
class RejectReason {
protected:
  const BasicBlock *const BB;
  //===--------------------------------------------------------------------===//
public:
  RejectReason(const BasicBlock *BB) : BB(BB) {};
  virtual ~RejectReason() {}

  /// @brief Generate a reasonable diagnostic message describing this error.
  ///
  /// @return A debug message representing this error.
  virtual std::string getMessage() const = 0;

  /// @brief A block that can be used as an anchor point to get debug info.
  ///
  /// @return A basic block whose location is relavant to the error.
  const BasicBlock *getBlock() const { return BB; }

  /// @brief A name to identify the reject reason.
  ///
  /// @return A string that uniquely identifies the reject reason.
  virtual const char *getName() const = 0;

  virtual void report(OptReporter& Reporter, const Loop *L) const;
};

typedef std::shared_ptr<RejectReason> RejectReasonPtr;

/// @brief Stores all errors that ocurred during the detection.
class RejectLog {
  Region *R;
  llvm::SmallVector<RejectReasonPtr, 1> ErrorReports;

public:
  explicit RejectLog(Region *R) : R(R) {}

  typedef llvm::SmallVector<RejectReasonPtr, 1>::iterator iterator;

  iterator begin() { return ErrorReports.begin(); }
  iterator end() { return ErrorReports.end(); }
  size_t size() { return ErrorReports.size(); }

  const Region *region() const { return R; }
  void report(RejectReasonPtr Reject) { ErrorReports.push_back(Reject); }
};

//===----------------------------------------------------------------------===//
/// @brief Base class for CFG related reject reasons.
///
/// Scop candidates that violate structural restrictions can be grouped under
/// this reject reason class.
class ReportCFG : public RejectReason {
  //===--------------------------------------------------------------------===//
public:
  ReportCFG(BasicBlock *BB);
};

class ReportNonBranchTerminator : public ReportCFG {
public:
  ReportNonBranchTerminator(BasicBlock *BB) : ReportCFG(BB) {}

  /// @name RejectReason interface
  //@{
  virtual std::string getMessage() const;
  const char *getName() const override { return "CFG_NonBranch"; }
  void report(OptReporter&, const Loop*) const override;
  //@}
};

//===----------------------------------------------------------------------===//
/// @brief Captures a not well-structured condition within the CFG.
class ReportCondition : public ReportCFG {
  //===--------------------------------------------------------------------===//
public:
  ReportCondition(BasicBlock *BB) : ReportCFG(BB) {}

  /// @name RejectReason interface
  //@{
  virtual std::string getMessage() const;
  const char *getName() const override { return "CFG_CmpStructure"; }
  //@}
};

//===----------------------------------------------------------------------===//
/// @brief Base class for non-affine reject reasons.
///
/// Scop candidates that violate restrictions to affinity are reported under
/// this class.
class ReportAffFunc : public RejectReason {
  //===--------------------------------------------------------------------===//
public:
  ReportAffFunc(const BasicBlock *BB);
};

//===----------------------------------------------------------------------===//
/// @brief Captures a condition that is based on an 'undef' value.
class ReportUndefCond : public ReportAffFunc {
  //===--------------------------------------------------------------------===//
public:
  ReportUndefCond(BasicBlock *BB);

  /// @name RejectReason interface
  //@{
  virtual std::string getMessage() const;
  const char *getName() const override { return "Cmp_Undef"; }
  //@}
};

//===----------------------------------------------------------------------===//
/// @brief Captures an invalid condition
///
/// Conditions have to be either constants or icmp instructions.
class ReportInvalidCond : public ReportAffFunc {
  const Value *Condition;
  //===--------------------------------------------------------------------===//
public:
  ReportInvalidCond(BasicBlock *BB, const Value *Cond);

  /// @name RejectReason interface
  //@{
  virtual std::string getMessage() const;
  const char *getName() const override { return "Cmp_Unsupported"; }
  void report(OptReporter&, const Loop*) const override;
  //@}
};

//===----------------------------------------------------------------------===//
/// @brief Captures an invalid condition
///
/// Conditions have to be inside the region.
class ReportCondOutsideRegion : public ReportAffFunc {
  //===--------------------------------------------------------------------===//
public:
  ReportCondOutsideRegion(BasicBlock *BB);

  /// @name RejectReason interface
  //@{
  virtual std::string getMessage() const;
  const char *getName() const override { return "QCDRIVER_CmpOutsideRegion"; }
  //@}
};

//===----------------------------------------------------------------------===//
/// @brief Captures an invalid condition
///
/// Conditions have to be signed.
class ReportUnsignedCond : public ReportAffFunc {
  //===--------------------------------------------------------------------===//
  const Value *Condition;
public:
  ReportUnsignedCond(BasicBlock *BB, const Value *Cond);

  /// @name RejectReason interface
  //@{
  virtual std::string getMessage() const;
  const char *getName() const override { return "Cmp_Unsigned"; }
  void report(OptReporter&, const Loop*) const override;
  //@}
};

//===----------------------------------------------------------------------===//
/// @brief Captures an invalid condition
///
/// Conditions should be affine.
class ReportNonAffineCond : public ReportAffFunc {
  //===--------------------------------------------------------------------===//
  const Value *Condition;
public:
  ReportNonAffineCond(BasicBlock *BB, const Value *Cond);

  /// @name RejectReason interface
  //@{
  virtual std::string getMessage() const;
  const char *getName() const override { return "Cmp_NonAffineExit"; }
  void report(OptReporter&, const Loop*) const override;
  //@}
};

//===----------------------------------------------------------------------===//
/// @brief Captures an undefined operand.
class ReportUndefOperand : public ReportAffFunc {
  //===--------------------------------------------------------------------===//
public:
  ReportUndefOperand(BasicBlock *BB) : ReportAffFunc(BB) {}

  /// @name RejectReason interface
  //@{
  virtual std::string getMessage() const;
  const char *getName() const override { return "Cmp_UndefOp"; }
  //@}
};

//===----------------------------------------------------------------------===//
/// @brief Captures a non-affine branch.
class ReportNonAffBranch : public ReportAffFunc {
  //===--------------------------------------------------------------------===//
  /// @brief LHS & RHS of the failed condition.
  //@{
  const SCEV *LHS;
  const SCEV *RHS;
  //@}

public:
  ReportNonAffBranch(BasicBlock *BB, const SCEV *LHS, const SCEV *RHS)
      : ReportAffFunc(BB), LHS(LHS), RHS(RHS) {}

  const SCEV *lhs() { return LHS; }
  const SCEV *rhs() { return RHS; }

  /// @name RejectReason interface
  //@{
  virtual std::string getMessage() const;
  const char *getName() const override { return "Cmp_NonAffineBranch"; }
  //@}
};

//===----------------------------------------------------------------------===//
/// @brief Captures a missing base pointer.
class ReportNoBasePtr : public ReportAffFunc {
  //===--------------------------------------------------------------------===//
public:
  ReportNoBasePtr(BasicBlock *BB) : ReportAffFunc(BB) {}
  /// @name RejectReason interface
  //@{
  virtual std::string getMessage() const;
  const char *getName() const override { return "Mem_NoBasePointer"; }
  //@}
};

//===----------------------------------------------------------------------===//
/// @brief Captures an undefined base pointer.
class ReportUndefBasePtr : public ReportAffFunc {
  //===--------------------------------------------------------------------===//
public:
  ReportUndefBasePtr(BasicBlock *BB) : ReportAffFunc(BB) {}
  /// @name RejectReason interface
  //@{
  virtual std::string getMessage() const;
  const char *getName() const override { return "Mem_UndefBasePointer"; }
  //@}
};

//===----------------------------------------------------------------------===//
/// @brief Captures a base pointer that is not invariant in the region.
class ReportVariantBasePtr : public ReportAffFunc {
  //===--------------------------------------------------------------------===//
  // The variant base pointer.
  Value *BaseValue;

public:
  ReportVariantBasePtr(Value *BaseValue, BasicBlock *BB)
      : ReportAffFunc(BB), BaseValue(BaseValue) {}

  /// @name RejectReason interface
  //@{
  virtual std::string getMessage() const;
  const char *getName() const override { return "Mem_BasePointerNotInvariant"; }
  void report(OptReporter&, const Loop*) const override;
  //@}
};

//===----------------------------------------------------------------------===//
/// @brief Captures a non-affine access function.
class ReportNonAffineAccess : public ReportAffFunc {
  //===--------------------------------------------------------------------===//
  // The non-affine access function.
  const SCEV *AccessFunction;
  const Instruction *AccessInst;

public:
  ReportNonAffineAccess(const SCEV *AccessFunction, const Instruction *Inst)
      : ReportAffFunc(Inst->getParent()), AccessFunction(AccessFunction),
        AccessInst(Inst) {}

  const SCEV *get() { return AccessFunction; }

  /// @name RejectReason interface
  //@{
  virtual std::string getMessage() const;
  const char *getName() const override { return "Mem_NonAffineAccess"; }
  void report(OptReporter&, const Loop*) const override;
  //@}
};

//===----------------------------------------------------------------------===//
/// @brief Base class for reject reasons related to induction variables.
///
//  ReportIndVar reject reasons are generated when the ScopDetection finds
/// errors in the induction variable(s) of the Scop candidate.
class ReportIndVar : public RejectReason {
  //===--------------------------------------------------------------------===//
public:
  ReportIndVar(BasicBlock *BB);
};

//===----------------------------------------------------------------------===//
/// @brief Captures a phi node that refers to SSA names in the current region.
class ReportPhiNodeRefInRegion : public ReportIndVar {
  //===--------------------------------------------------------------------===//

  // The offending instruction.
  Instruction *Inst;

public:
  ReportPhiNodeRefInRegion(Instruction *Inst)
      : ReportIndVar(Inst->getParent()), Inst(Inst) {}

  /// @name RejectReason interface
  //@{
  virtual std::string getMessage() const;
  const char *getName() const override { return "IndVar_NonSynthesizablePhi"; }
  //@}
};

//===----------------------------------------------------------------------===//
/// @brief Captures a non canonical phi node.
class ReportNonCanonicalPhiNode : public ReportIndVar {
  //===--------------------------------------------------------------------===//

  // The offending instruction.
  Instruction *Inst;

public:
  ReportNonCanonicalPhiNode(Instruction *Inst)
      : ReportIndVar(Inst->getParent()), Inst(Inst) {}

  /// @name RejectReason interface
  //@{
  virtual std::string getMessage() const;
  const char *getName() const override { return "IndVar_NonCanonicalPhi"; }
  //@}
};

//===----------------------------------------------------------------------===//
/// @brief Captures a non canonical induction variable in the loop header.
class ReportLoopHeader : public ReportIndVar {
  //===--------------------------------------------------------------------===//

  // The offending loop.
  Loop *L;

public:
  ReportLoopHeader(Loop *L) : ReportIndVar(L->getHeader()), L(L) {}

  /// @name RejectReason interface
  //@{
  virtual std::string getMessage() const;
  const char *getName() const override { return "IndVar_LoopHeader"; }
  //@}
};

//===----------------------------------------------------------------------===//
/// @brief Captures a region with invalid entering edges.
class ReportIndEdge : public RejectReason {
  //===--------------------------------------------------------------------===//
public:
  ReportIndEdge(Loop *L);

  /// @name RejectReason interface
  //@{
  virtual std::string getMessage() const;
  const char *getName() const override { return "CFG_RegionEntryEdge"; }
  //@}
};

//===----------------------------------------------------------------------===//
/// @brief Captures errors with bad alignment.
class ReportBadAlignment : public RejectReason {
  //===--------------------------------------------------------------------===//
public:
  ReportBadAlignment(BasicBlock *BB);

  /// @name RejectReason interface
  //@{
  virtual std::string getMessage() const;
  const char *getName() const override { return "Mem_Alignment"; }
  //@}
};


//===----------------------------------------------------------------------===//
/// @brief Captures errors with bad QCDriver.
class ReportQCDriver : public RejectReason {
  //===--------------------------------------------------------------------===//

public:
  ReportQCDriver(BasicBlock *BB);
};

//===----------------------------------------------------------------------===//
/// @brief Captures a loop already optimized by Polly.
class ReportLoopNestLevel : public ReportQCDriver {
  //===--------------------------------------------------------------------===//

  // The offending loop.
  Loop *L;

public:
  ReportLoopNestLevel(Loop *L) : ReportQCDriver(L->getHeader()), L(L) {};

  /// @name RejectReason interface
  //@{
  virtual std::string getMessage() const;
  const char *getName() const override { return "QCDRIVER_LoopNest"; }
  void report(OptReporter&, const Loop*) const override {}; // Not reported
  //@}
};

//===----------------------------------------------------------------------===//
/// @brief Captures a loop already optimized by Polly.
class ReportMoreThanOneBB : public ReportQCDriver {
  //===--------------------------------------------------------------------===//
public:
  ReportMoreThanOneBB(BasicBlock &BB) : ReportQCDriver(&BB) {};

  /// @name RejectReason interface
  //@{
  virtual std::string getMessage() const;
  const char *getName() const override { return "QCDRIVER_LoopBody"; }
  void report(OptReporter&, const Loop*) const override;
  //@}
};


//===----------------------------------------------------------------------===//
/// @brief Captures errors with bad loop bounds.
class ReportBadLoopBound : public RejectReason {
  //===--------------------------------------------------------------------===//

public:
  ReportBadLoopBound(BasicBlock *BB);
};

//===----------------------------------------------------------------------===//
/// @brief Captures when we exclude a loop for having too many parameters.
class ReportMaxParams : public ReportQCDriver {
  //===--------------------------------------------------------------------===//
public:
  ReportMaxParams(BasicBlock *BB) : ReportQCDriver(BB) {}

  /// @name RejectReason interface
  //@{
  std::string getMessage() const override;
  const char *getName() const override { return "QCDRIVER_MaxParams"; }
  void report(OptReporter&, const Loop*) const override;
  //@}
};

//===----------------------------------------------------------------------===//
/// @brief Captures a loop with an invalid block.
class ReportBlockNotInLoop : public ReportQCDriver {
  //===--------------------------------------------------------------------===//
public:
  ReportBlockNotInLoop(BasicBlock *BB) : ReportQCDriver(BB) {}

  /// @name MVRejectReason interface
  //@{
  std::string getMessage() const override;
  const char *getName() const override { return "QCDRIVER_BlockNotInLoop"; }
  //@}
};

//===----------------------------------------------------------------------===//
/// @brief Captures errors with non affine loop bounds.
class ReportLoopBound : public ReportBadLoopBound {
  //===--------------------------------------------------------------------===//

  // The offending loop.
  Loop *L;

  // The non-affine loop bound.
  const SCEV *LoopCount;

public:
  ReportLoopBound(Loop *L, const SCEV *LoopCount);

  const SCEV *loopCount() { return LoopCount; }

  /// @name RejectReason interface
  //@{
  virtual std::string getMessage() const;
  const char *getName() const override { return "LoopBound_NonAffine"; }
  void report(OptReporter&, const Loop*) const override;
  //@}
};

//===----------------------------------------------------------------------===//
/// @brief Captures errors with loop bounds that have non loop-invariant bounds.
class ReportVariantLoopBound : public ReportBadLoopBound {
  //===--------------------------------------------------------------------===//

  // The offending loop.
  Loop *L;

  // The non-induction variable value that varies in the loop
  const Value *Bound;

public:
  ReportVariantLoopBound(Loop *L, const Value *B)
      : ReportBadLoopBound(L->getHeader()), L(L), Bound(B) {}

  virtual std::string getMessage() const override;
  const char *getName() const override { return "LoopBound_NotInvariant"; }
  void report(OptReporter &, const Loop *) const override;
};

//===----------------------------------------------------------------------===//
/// @brief Captures a loop already optimized by Polly.
class ReportLoopAnnotation : public ReportBadLoopBound {
  //===--------------------------------------------------------------------===//

  // The offending loop.
  Loop *L;

public:
  ReportLoopAnnotation(Loop *L) : ReportBadLoopBound(L->getHeader()), L(L) {};

  /// @name RejectReason interface
  //@{
  virtual std::string getMessage() const;
  const char *getName() const override { return "QCDRIVER_Annotation"; }
  void report(OptReporter&, const Loop*) const override {}; // Not reported
  //@}
};

//===----------------------------------------------------------------------===//
/// @brief Captures a loop with an upper bound larger than pointer types.
class ReportLargeUpperBound : public ReportBadLoopBound {
  //===--------------------------------------------------------------------===//

  // The offending loop.
  Loop *L;
  const SCEV *LoopCount;

public:
  ReportLargeUpperBound(Loop *L, const SCEV *LC)
      : ReportBadLoopBound(L->getHeader()), L(L), LoopCount(LC) {};

  /// @name RejectReason interface
  //@{
  virtual std::string getMessage() const;
  const char *getName() const override { return "LoopBound_UBLargerThanPointer"; }
  void report(OptReporter&, const Loop*) const override;
  //@}
};


//===----------------------------------------------------------------------===//
/// @brief Captures a loop with too few iterations.
class ReportSmallTripCount : public ReportBadLoopBound {
  //===--------------------------------------------------------------------===//

  // The offending loop.
  Loop *L;
  const SCEV *LoopCount;

public:
  ReportSmallTripCount(Loop *L, const SCEV *LC)
      : ReportBadLoopBound(L->getHeader()), L(L), LoopCount(LC) {};

  /// @name RejectReason interface
  //@{
  virtual std::string getMessage() const;
  const char *getName() const override { return "QCDRIVER_TripCountTooSmall"; }
  void report(OptReporter&, const Loop*) const override;
  //@}
};

//===----------------------------------------------------------------------===//
/// @brief Captures a loop with too few iterations.
class ReportWrapAround : public ReportBadLoopBound {
  //===--------------------------------------------------------------------===//

  // The offending loop.
  Loop *L;

public:
  ReportWrapAround(Loop *L) : ReportBadLoopBound(L->getHeader()), L(L) {};

  /// @name RejectReason interface
  //@{
  virtual std::string getMessage() const;
  const char *getName() const override { return "LoopBound_Wrap"; }
  void report(OptReporter&, const Loop*) const override;
  //@}
};

//===----------------------------------------------------------------------===//
/// @brief Captures a loop with too few iterations.
class ReportOneIterLoop : public ReportBadLoopBound {
  //===--------------------------------------------------------------------===//

  // The offending loop.
  Loop *L;

public:
  ReportOneIterLoop(Loop *L) : ReportBadLoopBound(L->getHeader()), L(L) {};

  /// @name RejectReason interface
  //@{
  virtual std::string getMessage() const;
  const char *getName() const override { return "QCDRIVER_OneIterLoop"; }
  //@}
};

//===----------------------------------------------------------------------===//
/// @brief Captures errors with non-side-effect-known function calls.
class ReportFuncCall : public RejectReason {
  //===--------------------------------------------------------------------===//

  // The offending call instruction.
  CallInst *Inst;

public:
  ReportFuncCall(CallInst *Inst);

  /// @name RejectReason interface
  //@{
  std::string getMessage() const;
  const char *getName() const override { return "Inst_FuncCall"; }
  void report(OptReporter&, const Loop*) const override;
  //@}
};

//===----------------------------------------------------------------------===//
/// @brief Captures errors with aliasing.
class ReportAlias : public RejectReason {
  //===--------------------------------------------------------------------===//

  // The offending alias set.
  AliasSet *AS;

  /// @brief Format an invalid alias set.
  ///
  /// @param AS The invalid alias set to format.
  std::string formatInvalidAlias(AliasSet &AS) const;

public:
  ReportAlias(AliasSet *AS, BasicBlock *BB);

  /// @name RejectReason interface
  //@{
  std::string getMessage() const;
  const char *getName() const override { return "Mem_Alias"; }
  void report(OptReporter&, const Loop*) const override;
  //@}
};

//===----------------------------------------------------------------------===//
/// @brief Captures errors with non simplified loops.
class ReportSimpleLoop : public RejectReason {
  //===--------------------------------------------------------------------===//
public:
  ReportSimpleLoop(Loop *L);

  /// @name RejectReason interface
  //@{
  std::string getMessage() const;
  const char *getName() const override { return "CFG_SimpleLoop"; }
  //@}
};

//===----------------------------------------------------------------------===//
/// @brief Base class for otherwise ungrouped reject reasons.
class ReportOther : public RejectReason {
  //===--------------------------------------------------------------------===//
public:
  ReportOther(const BasicBlock *BB);

  /// @name RejectReason interface
  //@{
  std::string getMessage() const { return "Unknown reject reason"; }
  //@}
};

//===----------------------------------------------------------------------===//
/// @brief Captures errors with bad IntToPtr instructions.
class ReportIntToPtr : public ReportOther {
  //===--------------------------------------------------------------------===//

  // The offending base value.
  Value *BaseValue;

public:
  ReportIntToPtr(Value *BaseValue, BasicBlock *BB)
      : ReportOther(BB), BaseValue(BaseValue) {}

  /// @name RejectReason interface
  //@{
  std::string getMessage() const;
  const char *getName() const override { return "Inst_IntToPtr"; }
  void report(OptReporter&, const Loop*) const override;
  //@}
};

//===----------------------------------------------------------------------===//
/// @brief Captures errors with alloca instructions.
class ReportAlloca : public ReportOther {
  //===--------------------------------------------------------------------===//
  Instruction *Inst;

public:
  ReportAlloca(Instruction *Inst)
      : ReportOther(Inst->getParent()), Inst(Inst) {}

  /// @name RejectReason interface
  //@{
  std::string getMessage() const;
  const char *getName() const override { return "Inst_AllocaInstruction"; }
  //@}
};

//===----------------------------------------------------------------------===//
/// @brief Captures errors with unknown instructions.
class ReportUnknownInst : public ReportOther {
  //===--------------------------------------------------------------------===//
  Instruction *Inst;

public:
  ReportUnknownInst(Instruction *Inst)
      : ReportOther(Inst->getParent()), Inst(Inst) {}

  /// @name RejectReason interface
  //@{
  std::string getMessage() const;
  const char *getName() const override { return "Inst_InvalidInstruction"; }
  //@}
};

//===----------------------------------------------------------------------===//
/// @brief Captures errors with phi nodes in exit BBs.
class ReportPHIinExit : public ReportOther {
  //===--------------------------------------------------------------------===//
public:
  ReportPHIinExit(BasicBlock *BB) : ReportOther(BB) {}
  /// @name RejectReason interface
  //@{
  std::string getMessage() const;
  const char *getName() const override { return "CFG_PhiNodeInExit"; }
  //@}
};

//===----------------------------------------------------------------------===//
/// @brief Captures errors with regions containing the function entry block.
class ReportEntry : public ReportOther {
  //===--------------------------------------------------------------------===//
public:
  ReportEntry(BasicBlock *BB) : ReportOther(BB) {}
  /// @name RejectReason interface
  //@{
  std::string getMessage() const;
  const char *getName() const override { return "CFG_FunctionEntry"; }
  //@}
};

//===----------------------------------------------------------------------===//
/// @brief Captures errors where loops are rejected only because they are
///        in a region that is rejected for another reason. The loop did not
///        get its own region that could be rejected independently.
class ReportSharedPrimaryRegion : public ReportOther {
  //===--------------------------------------------------------------------===//
public:
  ReportSharedPrimaryRegion(const Loop *L) : ReportOther(L->getHeader()) {}
  /// @name RejectReason interface
  //@{
  std::string getMessage() const {
    return "Primary region shared by multiple loops";
  }
  const char *getName() const override { return "Other_SharedPrimaryRegion"; }
  void report(OptReporter &, const Loop *) const override;
  //@}
};

//===----------------------------------------------------------------------===//
/// @brief Report that the top level region is rejected.
class ReportTopLevelRegion : public ReportOther {
  //===--------------------------------------------------------------------===//
public:
  ReportTopLevelRegion(const BasicBlock *BB) : ReportOther(BB) {}
  /// @name RejectReason interface
  //@{
  std::string getMessage() const { return "Top level region is not allowed."; }
  const char *getName() const override { return "Other_TopLevelRegion"; }
  //@}
};

//===----------------------------------------------------------------------===//
/// @brief Captures errors where loops are rejected because they contain vector
///        values.
class ReportContainsVectorValues : public ReportOther {
  //===--------------------------------------------------------------------===//
public:
  ReportContainsVectorValues(const BasicBlock *BB) : ReportOther(BB) {}
  /// @name RejectReason interface
  //@{
  std::string getMessage() const { return "Loop body contains vector values"; }
  const char *getName() const override { return "Other_ContainsVectorValue"; }
  //@}
};

//===----------------------------------------------------------------------===//
/// @brief Captures scops that contain inline assembly.
class ReportInlineAsm : public ReportOther {
  //===--------------------------------------------------------------------===//
  const CallInst *AsmCall;
public:
  ReportInlineAsm(const CallInst *Call)
    : ReportOther(Call->getParent()), AsmCall(Call) {}
  /// @name RejectReason interface
  //@{
  std::string getMessage() const { return "Loop body contains inline assembly."; }
  const char *getName() const override { return "Other_InlineAsm"; }
  void report(OptReporter&, const Loop*) const override;
  //@}
};

//===----------------------------------------------------------------------===//
/// @brief Captures errors where loops are rejected because they contain a base
///        access that is not divisible by step.
class ReportBaseAddressNotDivisible : public ReportOther {
  //===--------------------------------------------------------------------===//
public:
  ReportBaseAddressNotDivisible(const BasicBlock *BB) : ReportOther(BB) {}
  /// @name RejectReason interface
  //@{
  std::string getMessage() const {
    return "Base address is not divisible by element size";
  }
  const char *getName() const override {
    return "Other_BaseAddressNotDivisible";
  }
  //@}
};

} // namespace polly

#endif // POLLY_SCOP_DETECTION_DIAGNOSTIC_H
