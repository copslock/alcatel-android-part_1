//===-PollyDebug.h -Provide support for debugging Polly passes-*- C++ -*-===//
//
// (C) 2013 Qualcomm Innovation Center, Inc. All rights reserved.
//
//
//===----------------------------------------------------------------------===//

#ifndef POLLY_DEBUG_H
#define POLLY_DEBUG_H

#include "llvm/Support/Debug.h"
namespace polly {
using namespace llvm;
extern bool PollyDebugFlag;
bool getPollyDebugFlag();
#undef DEBUG
#define DEBUG(X)                                                               \
  do {                                                                         \
    if (polly::getPollyDebugFlag()) {                                          \
      X;                                                                       \
    } else {                                                                   \
      DEBUG_WITH_TYPE(DEBUG_TYPE, X);                                          \
    }                                                                          \
  } while (0)
}
#endif
