//=== ScopDetectionDiagnostic.h -- Diagnostic for ScopDetection -*- C++ -*-===//
//
// (C) 2014 Qualcomm Innovation Center, Inc. All rights reserved.
//===----------------------------------------------------------------------===//
// A class heirarchy to represent diagnostic messages from the vectorizer.
//===----------------------------------------------------------------------===//
#ifndef POLLY_VECTORIZER_DIAGNOSTIC_H
#define POLLY_VECTORIZER_DIAGNOSTIC_H

namespace llvm {
class Loop;
class OptReporter;
}
using namespace llvm;

namespace polly {
//===----------------------------------------------------------------------===//
/// @brief Base class for all vectorizer messages.
///
/// Provide a common interface that can be used to print messages to the
/// screen or write them to a JSON file for mavenview to consume.
class VectorizerDiagnostic {
protected:
  const Loop *const L;
  //===--------------------------------------------------------------------===//
public:
  VectorizerDiagnostic(const Loop *L_) : L(L_) {};
  virtual ~VectorizerDiagnostic() {}

  /// @brief Generate a diagnostic message.
  ///
  /// @return A message representing this diagnostic.
  virtual std::string getMessage() const = 0;

  /// @brief The Loop that the vectorizer is informing about.
  ///
  /// @return An LLVM Loop.
  const Loop *getLoop() const { return L; }

  /// @brief A name to identify the diagnostic message.
  ///
  /// @return A string that uniquely identifies the message type.
  virtual const char *getName() const = 0;

  /// @brief Report the diagnostic to the given reporter
  virtual void report(OptReporter& Reporter) const = 0;
};

//===----------------------------------------------------------------------===//
// Vectorizer diagnostic message definitions.

class VDLoop : public VectorizerDiagnostic {
public:
  VDLoop(const Loop *L) : VectorizerDiagnostic(L) {}
  std::string getMessage() const override { return "Found loop"; }
  const char *getName() const override { return "Loop"; }
  void report(OptReporter& Reporter) const override {}
};

class VDCodeGenLoop : public VectorizerDiagnostic {
public:
  VDCodeGenLoop(const Loop *L) : VectorizerDiagnostic(L) {}
  std::string getMessage() const override {
    return "Generating code for loop.";
  }
  const char *getName() const override { return "CodeGenLoop"; }
  void report(OptReporter& Reporter) const override {}
};

class VDVectorizedLoop : public VectorizerDiagnostic {
public:
  VDVectorizedLoop(const Loop *L) : VectorizerDiagnostic(L) {}
  std::string getMessage() const override { return "Vectorized loop."; }
  const char *getName() const override { return "VectorizedLoop"; }
  void report(OptReporter& Reporter) const override;
};

class VDNonVectorizableLoop : public VectorizerDiagnostic {
public:
  VDNonVectorizableLoop(const Loop *L) : VectorizerDiagnostic(L) {}
  std::string getMessage() const override { return "Non-vectorizable loop."; }
  const char *getName() const override { return "NonVectorizableLoop"; }
  void report(OptReporter& Reporter) const override;
};

class VDSkipLoop : public VectorizerDiagnostic {
public:
  VDSkipLoop(const Loop *L) : VectorizerDiagnostic(L) {}
  std::string getMessage() const override { return "Skipping loop."; }
  const char *getName() const override { return "SkipLoop"; }
  void report(OptReporter& Reporter) const override;
};

class VDSkipLowTripCountLoop : public VectorizerDiagnostic {
public:
  VDSkipLowTripCountLoop(const Loop *L) : VectorizerDiagnostic(L) {}
  std::string getMessage() const override {
    return "Skipping low trip-count loop.";
  }
  const char *getName() const override { return "SkipLowTripCountLoop"; }
  void report(OptReporter& Reporter) const override;
};

class VDCNCNumberOfIterations : public VectorizerDiagnostic {
public:
  VDCNCNumberOfIterations(const Loop *L) : VectorizerDiagnostic(L) {}
  std::string getMessage() const override {
    return "Could not compute number of iterations.";
  }
  const char *getName() const override { return "CNC_NumberOfIterations"; }
  void report(OptReporter& Reporter) const override;
};

class VDMaxMemoryAccess : public VectorizerDiagnostic {
public:
  VDMaxMemoryAccess(const Loop *L) : VectorizerDiagnostic(L) {}
  std::string getMessage() const override {
    return "Too many memory accesses in loop.";
  }
  const char *getName() const override { return "MaxMemoryAccess"; }
  void report(OptReporter& Reporter) const override;
};

class VDVectorizedLowTripCountLoop : public VectorizerDiagnostic {
public:
  VDVectorizedLowTripCountLoop(const Loop *L) : VectorizerDiagnostic(L) {}
  std::string getMessage() const override {
    return "Vectorized low trip-count loop.";
  }
  const char *getName() const override { return "VectorizedLowTripCountLoop"; }
  void report(OptReporter& Reporter) const override {}
};
}
#endif
