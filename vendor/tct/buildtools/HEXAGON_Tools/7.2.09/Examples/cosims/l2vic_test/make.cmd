::###############################################################
:: Copyright (c) \$Date\$ QUALCOMM INCORPORATED.
:: All Rights Reserved.
:: Modified by QUALCOMM INCORPORATED on \$Date\$
::################################################################
::
:: Make.cmd to build the example
:: Please note that this example assumes you have Microsoft Visual Studio 10.
:: A 64-bit dll will be created as part of this example if using Hexagon tools
:: 7.3.x or greater.
::

::@echo off
@set HEXAGON_TOOLS=
@set INCLUDE=
@set LIB=
@set COSIM_CC=

::################################################
:: Make sure that `where` command only picks up ##
:: first path it finds, in case there are more  ##
:: than one version tools path in PATH variable ##
::################################################
@for /f "delims=" %%a in ('where hexagon-sim') do (
    @set HEXAGON_TOOLS=%%a
    @goto :done
)
:done
@set HEXAGON_TOOLS=%HEXAGON_TOOLS%\..\..

for /f "delims=" %%a in ('cd') do @set SRC_TOP=%%a
::@set SRC_TOP=%CURRENT_DIR%

@set CC=hexagon-clang
@set SIM=hexagon-sim
@set Q6VERSION=v5
@set COSIM_NAME=l2vic_test_cosim
@set TESTFILE_NAME=mandelbrot
@set CFLAGS=-m%Q6VERSION%

@set INCLUDE_DIR=%HEXAGON_TOOLS%\include\iss
@set MYLIB=%HEXAGON_TOOLS%\lib\iss
@set BIN_DIR=%SRC_TOP%\bin
@set SRC_DIR=%SRC_TOP%\Source

@set SIMFLAGS=--timing --cosim_file ./Source/cosims_win.txt --m%Q6VERSION% %BIN_DIR%\%TESTFILE_NAME%.elf

:: Compiler to use to build cosim
@set VCTOOLS=c:\Program Files (x86)\Microsoft Visual Studio 10.0\VC
@set INCLUDE=%VCTOOLS%\INCLUDE;%VCTOOLS%\ATLMFC\INCLUDE;C:\Program Files (x86)\Microsoft SDKs\Windows\v7.0A\include;
@set COSIM_CFLAGS=/MT /O2 /nologo /Zm1000 /EHsc  -DVCPP -DLITTLE_ENDIAN /TP /I. /DCYGPC /I%HEXAGON_TOOLS%\include\iss\ /I%SRC_DIR%

::
:: setup for 32 bit dll
::
@set COSIM_CC="%VCTOOLS%\bin\cl.exe"
@set COSIM_SIZE=
@set LIB=%VCTOOLS%\LIB;%VCTOOLS%\ATLMFC\LIB;"C:\Program Files (x86)\Microsoft SDKs\Windows\v7.0A\lib";

::
:: test if 64 bit and if true set up for 64 bit DLL
::
@set ERRORLEVEL=
"c:\Program Files (x86)\Microsoft Visual Studio 10.0\VC\BIN\dumpbin" /headers %HEXAGON_TOOLS%/bin/hexagon-sim.exe | findstr /C:"64"
@if %ERRORLEVEL%==1 GOTO CONT

@set COSIM_CC="%VCTOOLS%\bin\amd64\cl.exe"
@set COSIM_SIZE=/MACHINE:X64
@set LIB="C:\WINDOWS\Microsoft.NET\Framework64\v4.0.30319";"C:\WINDOWS\Microsoft.NET\Framework64\v3.5";"%VCTOOLS%\LIB\amd64";"%VCTOOLS%\ATLMFC\LIB";"C:\Program Files (x86)\Microsoft SDKs\Windows\v7.0A\Lib\x64";

:CONT
@set COSIM_LINK=/nologo /link /dll libwrapper.lib %COSIM_SIZE%  /libpath:%LIBPATH%

@set LIBPATH=%HEXAGON_TOOLS%\lib\iss\

@IF "%1"=="clean" GOTO Clean
@IF "%1"=="build" GOTO Build
@IF "%1"=="sim" GOTO Sim

::
:: Clean up
::
:Clean
@echo.
@echo Cleaning files...
@IF EXIST pmu_stats* @DEL pmu_stats*
@IF EXIST stats.txt @DEL stats.txt
@IF EXIST .\bin @RMDIR /S /Q .\bin
@IF "%1"=="clean" GOTO End

::
:: Build the bus cosim
::
:Build
@echo.
@echo Compiling cosim ...
@MKDIR .\bin
%COSIM_CC% %COSIM_CFLAGS% -c %SRC_DIR%\%COSIM_NAME%.cpp /I%INCLUDE_DIR% /Fo%BIN_DIR%\%COSIM_NAME%.obj
%COSIM_CC% %BIN_DIR%\%COSIM_NAME%.obj %COSIM_LINK% /out:%BIN_DIR%\%COSIM_NAME%.dll

@IF EXIST %BIN_DIR%\%COSIM_NAME%.obj @DEL %BIN_DIR\%COSIM_NAME%.obj
%CC% %CFLAGS% %SRC_DIR%\%TESTFILE_NAME%.c -o %BIN_DIR%\%TESTFILE_NAME%.elf -lhexagon -DJUST_WAIT

@IF "%1"=="build" GOTO End

::
:: Simulate the example
::
:Sim
@echo.
@echo Simulating example
%SIM% %SIMFLAGS%
::
:End
::
