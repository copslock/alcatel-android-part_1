::
:: Make.cmd to build the example
:: Please note that this example assumes you have Microsoft Visual Studio 10.
:: A 64-bit dll will be created as part of this example.
::

:: Get Hexagon tools path
@echo off
@set HEXAGON_TOOLS=
@set INCLUDE=
@set LIB=
@set ERRORLEVEL=
::################################################
:: Make sure that `where` command only picks up ##
:: first path it finds, in case there are more  ##
:: than one version tools path in PATH variable ##
::################################################
@for /f "delims=" %%a in ('where hexagon-sim') do (
    @set HEXAGON_TOOLS=%%a
    @goto :done
)
:done

for /f "delims=" %%a in ('cd') do @set CURRENT_DIR=%%a

@set TESTFILE_NAME=PCRangeProfiler
@set HEXAGON_ARCH=v5

@set VCTOOLS=c:\Program Files (x86)\Microsoft Visual Studio 10.0\VC
@set INCLUDE=%VCTOOLS%\INCLUDE;%VCTOOLS%\ATLMFC\INCLUDE;C:\Program Files (x86)\Microsoft SDKs\Windows\v7.0A\include;

@set COSIM_NAME=..\bin\%TESTFILE_NAME%.dll
@set COSIM_CFLAGS=/MT /O2 /nologo /Zm1000 /EHsc  -DVCPP -DLITTLE_ENDIAN /TP /I. /DCYGPC /I%HEXAGON_TOOLS%\..\..\include\iss\ /I%CURRENT_DIR%\..\src

::
:: setup for 32 bit dll
::

@set COSIM_CC="%VCTOOLS%\bin\cl.exe"
@set COSIM_SIZE=
@set LIB=%VCTOOLS%\LIB;%VCTOOLS%\ATLMFC\LIB;"C:\Program Files (x86)\Microsoft SDKs\Windows\v7.0A\lib";

::
:: test if 64 bit and if true set up for 64 bit DLL
::
@echo off
"c:\Program Files (x86)\Microsoft Visual Studio 10.0\VC\BIN\dumpbin" /headers %HEXAGON_TOOLS% | findstr /C:"64"
if %ERRORLEVEL%==1 GOTO CONT

@set COSIM_CC="%VCTOOLS%\bin\amd64\cl.exe"
@set COSIM_SIZE=/MACHINE:X64
@set LIB="C:\WINDOWS\Microsoft.NET\Framework64\v4.0.30319";"C:\WINDOWS\Microsoft.NET\Framework64\v3.5";"%VCTOOLS%\LIB\amd64";"%VCTOOLS%\ATLMFC\LIB";"C:\Program Files (x86)\Microsoft SDKs\Windows\v7.0A\Lib\x64";

:CONT
@set COSIM_LINK=/nologo /link /dll libwrapper.lib %COSIM_SIZE% /libpath:%HEXAGON_TOOLS%\..\..\lib\iss\


@set SIM=hexagon-sim
@set SIMFLAGS=-m%HEXAGON_ARCH% --timing --cosim_file cosimCfg

@set CC=hexagon-clang
@set CFLAGS=-m%HEXAGON_ARCH% -g


@echo on

@IF "%1"=="clean" GOTO Clean
@IF "%1"=="build" GOTO Build
@IF "%1"=="sim" GOTO Sim

:: clean
:Clean
@echo.
@echo Cleaning files...
@IF EXIST ..\bin\%TESTFILE_NAME%.dll @DEL ..\bin\%TESTFILE_NAME%.*
@IF EXIST ..\bin\hello @DEL ..\bin\hello
@IF EXIST pmu_stats.txt @DEL pmu_stats.txt
@IF EXIST cosimCfg @DEL cosimCfg
@IF EXIST stats.txt @DEL stats.txt
@IF EXIST ..\build @RMDIR /S /Q ..\build
@IF "%1"=="clean" GOTO End

:: build
:Build
@echo.
@echo Compiling dll

@MKDIR ..\build

%COSIM_CC% %COSIM_CFLAGS%  -c %TESTFILE_NAME%.c /Fo%CURRENT_DIR%\..\build\%TESTFILE_NAME%.obj
%COSIM_CC% %CURRENT_DIR%\..\build\%TESTFILE_NAME%.obj %COSIM_LINK% /out:%CURRENT_DIR%\..\bin\%TESTFILE_NAME%.dll

%CC% %CFLAGS% %CURRENT_DIR%\..\test\hello.c -o %CURRENT_DIR%\..\bin\hello -lhexagon
@echo %CURRENT_DIR%\..\bin\%TESTFILE_NAME%.dll StartIteration EndIteration > %CURRENT_DIR%\cosimCfg
@IF "%1"=="build" GOTO End

:: run the example
:Sim
@echo.
@echo Running example...
%SIM% %SIMFLAGS% %CURRENT_DIR%/../bin/hello

:End
