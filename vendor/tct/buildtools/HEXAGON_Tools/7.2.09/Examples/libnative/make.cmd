::
:: Make.cmd to build the libnative example
::
:: Get Hexagon tools path
echo off
for /f "delims=" %%a in ('where hexagon-sim') do @set HEXAGON_TOOLS=%%a
echo on
@echo %HEXAGON_TOOLS%
@set TESTFILE_NAME=intrinsics
@set MYLIB=%HEXAGON_TOOLS%\..\..\libnative\lib\libnative.a
@set MYINCLUDE=%HEXAGON_TOOLS%\..\..\libnative\include

:: clean
@echo.
@echo Cleaning files...
@IF EXIST intrinsics.exe @DEL intrinsics.exe
@IF EXIST intrinsics.obj @DEL intrinsics.obj
@IF "%1"=="clean" GOTO End

:: build libnative example
@echo.
@echo Compiling libnative example...
cl /MD /I%MYINCLUDE% %TESTFILE_NAME%.c  /link  %MYLIB% /out:%TESTFILE_NAME%.exe

:: run the example
@echo.
@echo Running libnative example...
%TESTFILE_NAME%.exe

:End
