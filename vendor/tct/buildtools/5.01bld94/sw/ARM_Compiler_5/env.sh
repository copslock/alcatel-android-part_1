# Env file for ARM Compiler 5
# this is an sh source file to set the right environment variables
# it should be used as
# $ . /path/to/this/file /path/to/installation
if [ "X${MY_DOCS}" = "X" ] ; then MY_DOCS=$HOME; fi
if [ "X${PATH}" = "X" ] ; then PATH="$1/bin"
else PATH="$1/bin:${PATH}"; fi
export PATH
