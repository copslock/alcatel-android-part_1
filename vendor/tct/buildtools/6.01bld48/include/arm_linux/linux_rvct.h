/*
 * linux_rvct.h
 * Interoperation with the GNU toolchain and libraries
 *
 * Copyright (C) ARM Limited, 2007,2008. All rights reserved.
 *
 * RCS $Revision: 178362 $
 * Checkin $Date: 2013-01-10 16:08:02 +0000 (Thu, 10 Jan 2013) $
 * Revising $Author: pwright $
 */

/* Linux/glibc and libstdc++ constants and builtins mappings */

#define __ARMCLIB_VERSION 6010007

#warning "linux_rvct.h is deprecated, please use linux_armcc.h instead."

#include <linux_armcc.h>

/* end of linux_rvct.h */

