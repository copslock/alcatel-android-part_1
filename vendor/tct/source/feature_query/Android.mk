LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

# VERTU - erlei.ding - BUG-2148253
MY_LOCAL_PATH := $(LOCAL_PATH)
LOCAL_FEATURE_COMMON_FILE := ./device/tct/common/tct_feature.global
LOCAL_FEATURE_PRODUCT_FILE := ./device/tct/${TARGET_PRODUCT}/tct_feature.global
local_tctfeature_tool := ./development/tcttools/tct_feature/tct_feature
# end vertu - BUG-2148253

LOCAL_MODULE := tct.feature_query
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := JAVA_LIBRARIES
LOCAL_NO_STANDARD_LIBRARIES := false

##enable it when perso merged
TCT_FEATURE_OUT := $(OUT_DIR)/target/product/$(TARGET_PRODUCT)/tct_intermediates/feature/tct.feature_query
LOCAL_FQ_FEATURE_FILE := $(TCT_FEATURE_OUT)/global_fq.mk
LOCAL_JAVA_FEATURE_FILE := $(TCT_FEATURE_OUT)/src/com/tct/feature/Global.java

$(shell if [ $(LOCAL_FEATURE_COMMON_FILE) -nt $(LOCAL_JAVA_FEATURE_FILE) \
    -o $(LOCAL_FEATURE_PRODUCT_FILE) -nt $(LOCAL_JAVA_FEATURE_FILE) ]; then \
	$(local_tctfeature_tool) global $(MY_LOCAL_PATH) $(TCT_FEATURE_OUT) $(TARGET_PRODUCT) ; \
	fi)

LOCAL_SRC_FILES := ../../../../$(LOCAL_JAVA_FEATURE_FILE)

LOCAL_JAVA_LIBRARIES := core-libart

include $(BUILD_STATIC_JAVA_LIBRARY)


# ============================================================
include $(CLEAR_VARS)

LOCAL_MODULE := tct.feature_query.xml
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := ETC
LOCAL_MODULE_PATH := $(TARGET_OUT_ETC)/permissions
LOCAL_SRC_FILES := $(LOCAL_MODULE)
include $(BUILD_PREBUILT)

include $(call all-makefiles-under,$(LOCAL_PATH))


