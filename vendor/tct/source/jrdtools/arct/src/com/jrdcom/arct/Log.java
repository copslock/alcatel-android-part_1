/* ***************************************************************************/
/*                                                        Date : Dec 1, 2009 */
/*                      Android Resource Customization Tool                  */
/*              Copyright (c) 2009 JRD Communications, Inc.                  */
/* ***************************************************************************/
/*                                                                           */
/*    This material is company confidential, cannot be reproduced in any     */
/*    form without the written permission of JRD Communications, Inc.        */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/*   Author :  Android team                                                  */
/*   Role :    arct                                                          */
/*   Reference documents :                                                   */
/*---------------------------------------------------------------------------*/
/* Comments :                                                                */
/*     file    : Log.java                                                    */
/*     Labels  :                                                             */
/*===========================================================================*/
/* Modifications   (month/day/year)                                          */
/*---------------------------------------------------------------------------*/
/* date    | author    | modification                                        */
/*---------+-----------+-----------------------------------------------------*/
/*01/12/09 | Zwshi     | on creation                                         */
/*---------+-----------+-----------------------------------------------------*/
/*         |           |                                                     */
/*===========================================================================*/
/* Problems Report                                                           */
/*---------------------------------------------------------------------------*/
/* date    | author    | PR #     |                                          */
/*---------|-----------|----------|------------------------------------------*/
/*         |           |          |                                          */
/*---------|-----------|----------|------------------------------------------*/
/*         |           |          |                                          */
/*===========================================================================*/

package com.jrdcom.arct;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Logger class can print message to the console.
 *
 * @author zwshi
 */
public class Log {

    private boolean mVerbose = false;

    public final static Log myLog = new Log();

    public void setVerbose(boolean verbose) {
        mVerbose = verbose;
    }

    public void debug(String format, Object... args) {
        if (mVerbose) {
            info(format, args);
        }
    }

    public void info(String format, Object... args) {
        String s = String.format(format, args);
        outPrintln(s);
    }

    public void error(String format, Object... args) {
        String s = String.format(format, args);
        errPrintln(s);
    }

    public void exception(Throwable t, String format, Object... args) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        t.printStackTrace(pw);
        pw.flush();
        error(format + "\n" + sw.toString(), args);
    }

    /** for unit testing */
    protected void errPrintln(String msg) {
        System.err.println(msg);
    }

    /** for unit testing */
    protected void outPrintln(String msg) {
        System.out.println(msg);
    }

}
