/* ***************************************************************************/
/*                                                            Date : Dec 14, 2009 */
/*                      Android Resource Customization Tool                  */
/*              Copyright (c) 2009 JRD Communications, Inc.                  */
/* ***************************************************************************/
/*                                                                           */
/*    This material is company confidential, cannot be reproduced in any     */
/*    form without the written permission of JRD Communications, Inc.        */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/*   Author :  Android team                                                  */
/*   Role :    arct_dint                                               */
/*   Reference documents :                                                   */
/*---------------------------------------------------------------------------*/
/* Comments :                                                                */
/*     file    : StringList.java                                                */
/*     Labels  :                                                             */
/*===========================================================================*/
/* Modifications   (month/day/year)                                          */
/*---------------------------------------------------------------------------*/
/* date    | author    | modification                                        */
/*---------+-----------+-----------------------------------------------------*/
/*14/12/09 | Zwshi     | on creation                                         */
/*---------+-----------+-----------------------------------------------------*/
/*         |           |                                                     */
/*===========================================================================*/
/* Problems Report                                                           */
/*---------------------------------------------------------------------------*/
/* date    | author    | PR #     |                                          */
/*---------|-----------|----------|------------------------------------------*/
/*         |           |          |                                          */
/*---------|-----------|----------|------------------------------------------*/
/*         |           |          |                                          */
/*===========================================================================*/

package com.jrdcom.arct;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

/**
 * @author zwshi
 */
public class StringsHelper implements Cloneable {
    public enum StringType {
        string("string"), stringArray("string-array"), plurals("plurals");
        private final String value;

        private StringType(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public interface StringItem {
        public void put(String[] strItem);

        public StringType getType();

        public List<String[]> getAll();

        public String getFullName(); // like A:mudule1:apple:1

        public String getName(); // like apple
    }

    class AndroidString implements StringItem {
        List<String[]> l = new ArrayList<String[]>();


        public List<String[]> getAll() {
            return l;
        }


        public StringType getType() {
            return StringType.string;
        }


        public void put(String[] strItem) {
            l.add(strItem);
        }


        public String getName() {
            return l.get(0)[0].split(":")[2];
        }


        public String getFullName() {
            return l.get(0)[0];
        }
//[PLATFORM]-Add-BEGIN by TCTNB.(Yufeng.Lan),2014/01/02,PR-572112, reason Modify arct tools,add product attributes.
        public String getProductName(){
            return l.get(0)[0].split(":")[3];
        }
//[PLATFORM]-Add-END by TCTNB.(Yufeng.Lan).
    }

    class AndroidStringArray implements StringItem {

        List<String[]> l = new ArrayList<String[]>();

        public List<String[]> getAll() {
            return l;
        }

        public StringType getType() {
            return StringType.stringArray;
        }

        public void put(String[] strItem) {
            l.add(strItem);
            Collections.sort(l, new Comparator<String[]>() {

                public int compare(String[] arg0, String[] arg1) {
                    return arg0[0].compareTo(arg1[0]);
                }
            });

        }


        public String getName() {
            return l.get(0)[0].split(":")[2];
        }


        public String getFullName() {
            return l.get(0)[0].substring(0, l.get(0)[0].lastIndexOf(":"));
        }

    }

    class AndroidPlurals implements StringItem {

        List<String[]> l = new ArrayList<String[]>();


        public List<String[]> getAll() {
            return l;
        }


        public StringType getType() {
            return StringType.plurals;
        }


        public void put(String[] strItem) {
            l.add(strItem);
            Collections.sort(l, new Comparator<String[]>() {

                public int compare(String[] arg0, String[] arg1) {
                    return arg0[0].compareTo(arg1[0]);
                }
            });

        }


        public String getName() {
            return l.get(0)[0].split(":")[2];
        }


        public String getFullName() {
            return l.get(0)[0].substring(0, l.get(0)[0].lastIndexOf(":"));
        }

    }

    public void put(String[] stringValue) throws LogAbortException {

        String key = stringValue[0].startsWith("S:") ? stringValue[0] : stringValue[0].substring(0,
                stringValue[0].lastIndexOf(":"));
        StringItem si = sh.get(key);
        if (si != null) {
            if (stringValue[0].startsWith("S:")) {
                throw new LogAbortException(stringValue[0] + " is redundency.");
            }
            if (stringValue[0].startsWith("A") || stringValue[0].startsWith("P")) {
                si.put(stringValue);
            } else {
                throw new LogAbortException(stringValue[0] + "has invalid string name.");
            }
        } else {
            if (stringValue[0].startsWith("S:")) {
                si = new AndroidString();
            } else if (stringValue[0].startsWith("A")) {
                si = new AndroidStringArray();
            } else if (stringValue[0].startsWith("P")) {
                si = new AndroidPlurals();
            } else {
                throw new LogAbortException(stringValue[0] + "has invalid string name");
            }
            si.put(stringValue);
            sh.put(key, si);
        }
    }

    public StringsHelper() {
        sh = new HashMap<String, StringItem>();
    }

    HashMap<String, StringItem> sh = null;

    public List<StringItem> getSortedAll() {
        return getSortedAll(sh);
    }

    public List<StringItem> getSortedAll(HashMap<String, StringItem> strMap) {
        List<StringItem> list = new ArrayList<StringItem>();
        list.addAll(strMap.values());
        Collections.sort(list, new Comparator<StringItem>() {

            public int compare(StringItem arg0, StringItem arg1) {
                return arg0.getFullName().compareTo(arg1.getFullName());
            }

        });
        return list;
    }

    public StringItem get(String key) {
        return sh.get(key);

    }
}
