/* ***************************************************************************/
/*                                                        Date : Dec 1, 2009 */
/*                      Android Resource Customization Tool                  */
/*              Copyright (c) 2009 JRD Communications, Inc.                  */
/* ***************************************************************************/
/*                                                                           */
/*    This material is company confidential, cannot be reproduced in any     */
/*    form without the written permission of JRD Communications, Inc.        */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/*   Author :  Android team                                                  */
/*   Role :    arct                                                          */
/*   Reference documents :                                                   */
/*---------------------------------------------------------------------------*/
/* Comments :                                                                */
/*     file    : XMLResourceHandler.java                                     */
/*     Labels  :                                                             */
/*===========================================================================*/
/* Modifications   (month/day/year)                                          */
/*---------------------------------------------------------------------------*/
/* date    | author    | modification                                        */
/*---------+-----------+-----------------------------------------------------*/
/*12/01/09 | Zwshi     | on creation                                         */
/*---------+-----------+-----------------------------------------------------*/
/*         |           |                                                     */
/*===========================================================================*/
/* Problems Report                                                           */
/*---------------------------------------------------------------------------*/
/* date    | author    | PR #     |                                          */
/*---------|-----------|----------|------------------------------------------*/
/*         |           |          |                                          */
/*---------|-----------|----------|------------------------------------------*/
/*         |           |          |                                          */
/*===========================================================================*/

package com.jrdcom.arct;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.StringWriter;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * An abstract class to deal with the XML information using DOM.
 */
public abstract class XMLResourceHandler {

    protected String getElementContent(Node elem) {

        return elem.getTextContent();
        // return elem.getChildNodes().item(0).getNodeValue().trim();
    }

    /**
     * Get string from DOM node by attribute name.
     *
     * @param elem a node from DOM tree.
     * @param attrName the attribute name.
     * @return string value of the attribute name from the DOM node.
     */
    public String getStringAttributeValue(Node elem, String attrName) {
        Node node = elem.getAttributes().getNamedItem(attrName);
        if (node == null) {
            return null;
        }
        return node.getNodeValue().trim();
    }

    /**
     * Get integer attribute value.
     *
     * @param elem The element node.
     * @param attrName The attribute name.
     * @return The attribute value in integer.
     */
    protected int getAttributeValue(Node elem, String attrName) {
        return Integer.parseInt(getStringAttributeValue(elem, attrName));
    }

    /**
     * Get child by attribute.
     *
     * @param parent The parent node.
     * @param attrName The attribute name.
     * @param attrValue The attribute value.
     * @return The child node.setAttribute
     */
    protected Node getChildByAttribute(Node parent, String attrName, String attrValue) {
        if (parent == null || attrName == null || attrValue == null) {
            return null;
        }
        NodeList children = parent.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (child.getNodeType() == Node.ELEMENT_NODE) {
                if (attrValue.equals(getStringAttributeValue(child, attrName))) {
                    return child;
                }
            }
        }

        return null;
    }

    /**
     * Set the attribute value.
     *
     * @param doc The document.
     * @param elem The element node.
     * @param name The attribute name.
     * @param value The attribute value in integer.
     */
    protected void setAttribute(Document doc, Node elem, String name, int value) {
        setAttribute(doc, elem, name, Integer.toString(value));
    }

    /**
     * Set the attribute value.
     *
     * @param doc The document.
     * @param elem The element node.
     * @param name The attribute name.
     * @param value The attribute value in string.
     */
    protected void setAttribute(Document doc, Node elem, String name, String value) {
        Attr attrNode = doc.createAttribute(name);
        attrNode.setNodeValue(value);

        elem.getAttributes().setNamedItem(attrNode);
    }

    /**
     * Write a DOM Document object into a file.
     *
     * @param file XML file to be written
     * @param doc DOM Document
     */
    protected void writeToFile(File file, Document doc) throws FileNotFoundException,
            TransformerFactoryConfigurationError, TransformerException {
        Transformer t = TransformerFactory.newInstance().newTransformer();
        // enable indent in result file
        t.setOutputProperty("indent", "yes");
        t.transform(new DOMSource(doc), new StreamResult(new FileOutputStream(file)));
    }

    /**
     * output node to console
     *
     * @param file XML file to be written
     * @param doc DOM Document
     */
    public void output(Node node) {
        TransformerFactory transFactory = TransformerFactory.newInstance();
        try {
            Transformer transformer = transFactory.newTransformer();
            transformer.setOutputProperty("encoding", "utf-8");
            transformer.setOutputProperty("indent", "yes");

            DOMSource source = new DOMSource();
            source.setNode(node);
            StreamResult result = new StreamResult();
            result.setOutputStream(System.out);

            transformer.transform(source, result);
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }

    public String outputString(Node node) {
        TransformerFactory transFactory = TransformerFactory.newInstance();
        try {
            Transformer transformer = transFactory.newTransformer();
            DOMSource source = new DOMSource();
            source.setNode(node);
            StreamResult result = new StreamResult();
            StringWriter sw = new StringWriter();
            result.setWriter(sw);
            transformer.transform(source, result);
            return sw.toString().substring("<?xml version=\"1.0\" encoding=\"UTF-8\"?>".length())
                    .replaceAll("(\\s)+", " ");

        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String copyNodeText(Node node) {
        TransformerFactory transFactory = TransformerFactory.newInstance();
        try {
            Transformer transformer = transFactory.newTransformer();
            DOMSource source = new DOMSource();
            source.setNode(node);
            StreamResult result = new StreamResult();
            StringWriter sw = new StringWriter();
            result.setWriter(sw);
            transformer.transform(source, result);
            return sw.toString().substring("<?xml version=\"1.0\" encoding=\"UTF-8\"?>".length());

        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * find first conditioned node
     */
    public Node selectSingleNode(String express, Object source) {
        Node result = null;
        XPathFactory xpathFactory = XPathFactory.newInstance();
        XPath xpath = xpathFactory.newXPath();
        try {
            result = (Node)xpath.evaluate(express, source, XPathConstants.NODE);
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }

        return result;
    }

    /**
     * find conditioned node list
     */
    public NodeList selectNodes(String express, Object source) {
        NodeList result = null;
        XPathFactory xpathFactory = XPathFactory.newInstance();
        XPath xpath = xpathFactory.newXPath();
        try {
            result = (NodeList)xpath.evaluate(express, source, XPathConstants.NODESET);
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }

        return result;
    }

}
