/* ***************************************************************************/
/*                                                            Date : 2010-7-29 */
/*                      Android Resource Customization Tool                  */
/*              Copyright (c) 2009 JRD Communications, Inc.                  */
/* ***************************************************************************/
/*                                                                           */
/*    This material is company confidential, cannot be reproduced in any     */
/*    form without the written permission of JRD Communications, Inc.        */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/*   Author :  Android team                                                  */
/*   Role :    arct                                               */
/*   Reference documents :                                                   */
/*---------------------------------------------------------------------------*/
/* Comments :                                                                */
/*     file    : PlfToPropertyMakefile.java                                                */
/*     Labels  :                                                             */
/*===========================================================================*/
/* Modifications   (month/day/year)                                          */
/*---------------------------------------------------------------------------*/
/* date    | author    | modification                                        */
/*---------+-----------+-----------------------------------------------------*/
/*01/12/09 | Zwshi     | on creation                                         */
/*---------+-----------+-----------------------------------------------------*/
/* 12/10/24| Liu Jie   |    PR-318616  | plf control module target or not    */
/*---------+-----------+-----------------------------------------------------*/
/* 13/06/28| Qiu Ruifeng|   PR-479045  | The perso can\\\'t match with software when translation is updated.    */
/*===========================================================================*/
/* Problems Report                                                           */
/*---------------------------------------------------------------------------*/
/* date    | author    | PR #     |                                          */
/*---------|-----------|----------|------------------------------------------*/
/*         |           |          |                                          */
/*---------|-----------|----------|------------------------------------------*/
/*         |           |          |                                          */
/*===========================================================================*/

package com.jrdcom.arct;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
//[PLATFORM]-Add by TCTNB.(QiuRuifeng), 2014/02/20 PR-606265, reason [ArctTool]Arct should support Ucs2StringWLen.
import java.io.UnsupportedEncodingException;
/**
 * @author zwshi
 */
public class PlfToPropertyMakefile extends XMLResourceHandler {

    Log log = Log.myLog;

    Bundle bundle = null;

    String mJrdProductPackages = ""; //added by binbin.zhao, to combine JRD product packages
//[PLATFORM]-Add by TCTNB.(QiuRuifeng), 2013/06/28 PR-479045, reason The perso can't match with software when translation is updated
    String mJrdodexPackages = "";

    private FileWriter fwMake = null;

    private FileWriter fwProp = null;

    public PlfToPropertyMakefile(Bundle bundle) {
        this.bundle = bundle;
    }

    private PlfToPropertyMakefile() {
    }

    public void build() throws LogAbortException, ParserConfigurationException, SAXException,
            IOException {
        Document doc;
        DocumentBuilderFactory factory;
        DocumentBuilder docBuilder;
        Element root;
        FileInputStream in = null;

        String plfFileName = bundle.getPlfPath();
        String makeFileName = bundle.getMakePath();
        String propFileName = bundle.getPropPath();

        File outMakeFile = new File(makeFileName + ".tmp");
        File outPropFile = new File(propFileName + ".tmp");
        try {
            log.info("now processing %s...", plfFileName);

            in = new FileInputStream(plfFileName);
            factory = DocumentBuilderFactory.newInstance();
            factory.setValidating(false);

            docBuilder = factory.newDocumentBuilder();
            doc = docBuilder.parse(in);
            root = doc.getDocumentElement();

            if (outMakeFile.isFile())
                outMakeFile.delete();
            fwMake = new FileWriter(outMakeFile);
            if (outPropFile.isFile())
                outPropFile.delete();
            fwProp = new FileWriter(outPropFile);

            Node m = root.getFirstChild();
            m = findNode(m, "TABLE_HISTORY");
            printHistory(m);

            m = findNode(m, "MOD");
            m = m.getFirstChild();
            m = findNode(m, "NAME");
            printNodeMessage(m);
            m = findNode(m, "SDM_AREA");
            printSdmAreaInfo(m);

            fwMake.write("#Overrides\r\n\r\n");
            fwProp.write("#\r\n");
            fwProp.write("#jrd sys properties\r\n");
            fwProp.write("#\r\n\r\n");
            m = m.getFirstChild();
            m = findNode(m, "TABLE_VAR");
            m = m.getFirstChild();
            while ((m = m.getNextSibling()) != null) {
                try {
                    m = findNode(m, "VAR");

                } catch (LogAbortException e) {
                    // end of found
                    log.info("-----------end-------------");
                    break;
                }
                dealVar(m);
            }
//[PLATFORM]-Add by TCTNB.(QiuRuifeng), 2013/06/28 PR-479045, reason The perso can't match with software when translation is updated
            writeJrdodexPackages();
            writeJrdProductPackages(); //added by binbin.zhao, to combine JRD product packages
            fwMake.close();
            File makefile = new File(makeFileName);
            if (makefile.isFile())
                makefile.delete();
            outMakeFile.renameTo(makefile);
            log.info("make file created: " + makefile.getAbsolutePath());

            fwProp.close();
            File propfile = new File(propFileName);
            if (propfile.isFile())
                propfile.delete();
            outPropFile.renameTo(propfile);
            log.info("prop file created: " + propfile.getAbsolutePath());

        } finally {
            if (in != null)
                in.close();
            if (fwMake != null)
                fwMake.close();
            if (outMakeFile.getName().equals(makeFileName + ".tmp"))
                outMakeFile.delete();
        }
    }

    Node findNode(Node node, String name) throws LogAbortException {
        do {
            if (node.getNodeName().equals(name))
                break;
        } while ((node = node.getNextSibling()) != null);
        if (node == null)
            throw new LogAbortException("Node " + name + " not found.");
        else
            return node;
    }

    Node findNodeCtype(Node node, String name, String Cname) throws LogAbortException {
        do {
            if (node.getNodeName().equals(name))
                break;
        } while ((node = node.getNextSibling()) != null);
        if (node == null){
            log.info("----------------------------------------------------------------------\nThe SDMID "+Cname+"'s format is wrong, please modify it\nNode " + name + " not found.\n----------------------------------------------------------------------");
            throw new LogAbortException("");
        }
        else
            return node;
    }

    void printHistory(Node m) throws DOMException, IOException {

        Node n = m.getFirstChild();
        do {
            try {
                n = findNode(n, "HISTORY");
                printComment(n, "HISTORY");
                n = n.getNextSibling();
            } catch (LogAbortException e) {
                // end to abort
                fwMake.write("\r\n\r\n");
                return;
            }

        } while (n != null);
    }

    void printNodeMessage(Node node) {
        log.info("node message:"
                + (node.getFirstChild() == null ? "" : node.getFirstChild().getTextContent()));
    }

    void printComment(Node node, String title) throws DOMException, IOException {
        fwMake.write("#" + title + ":"
                + (node.getFirstChild() == null ? "" : node.getFirstChild().getTextContent()));
        fwMake.write("\r\n");
    }

    void printSdmAreaInfo(Node m) throws LogAbortException, DOMException, IOException {

        Node n = m.getFirstChild();

        n = findNode(n, "DSA_TITLE");
        printComment(n, "DSA_TITLE");
        n = n.getNextSibling();

        n = findNode(n, "MACRO_NAME");
        printComment(n, "MACRO_NAME");
        n = n.getNextSibling();

        n = findNode(n, "C_NAME");
        printComment(n, "C_NAME");
        n = n.getNextSibling();

        n = findNode(n, "C_TYPE");
        printComment(n, "C_TYPE");
        n = n.getNextSibling();

        n = findNode(n, "IDA");
        printComment(n, "IDA");
        n = n.getNextSibling();

        n = findNode(n, "IS_IN_DSA");
        printComment(n, "IS_IN_DSA");
        n = n.getNextSibling();

        fwMake.write("\r\n\r\n");
    }

    void dealVar(Node m) throws LogAbortException, IOException {

        String sdmId;
        String cname;
        String desc;
        String metatype;
        String value;
        Node n = m.getFirstChild();
        n = findNode(n, "SIMPLE_VAR");
        n = n.getFirstChild();
        n = findNode(n, "SDMID");
        sdmId = n.getFirstChild().getTextContent();
        
        n = findNode(n, "C_NAME");
        cname = n.getFirstChild().getTextContent();
        n = findNodeCtype(n, "C_TYPE", cname);
        n = findNode(n, "ARRAY");
        n = findNode(n, "METATYPE");
        metatype = n.getFirstChild().getTextContent().trim().toLowerCase();
        //[PLATFORM]-Mod by TCTNB.(QiuRuifeng), 2014/02/20 PR-606265, reason [ArctTool]Arct should support Ucs2StringWLen.
        if (!(metatype.startsWith("asciistring") || metatype.startsWith("byte")
                || metatype.startsWith("word") || metatype.startsWith("dword")||metatype.startsWith("ucs2stringwlen")))
            throw new LogAbortException("wrong metatype: " + metatype + " of " + sdmId);

        n = findNode(n, "IS_CUSTO");
        n = findNode(n, "FEATURE");
        n = findNode(n, "DESC");
        desc = n.getFirstChild() == null ? "" : n.getFirstChild().getTextContent();
        n = findNode(n, "VALUE");
        value = n.getFirstChild() == null ? "" : n.getFirstChild().getTextContent().trim();

        if (cname.indexOf(".") > 0) {
            // output to prop file
            if (metatype.startsWith("asciistring")) {
                if (!(value.charAt(0) == '\"' && value.charAt(value.length() - 1) == '\"'))
                    throw new LogAbortException("wrong asciistring value: " + value + " of "
                            + sdmId);
                fwProp.write(cname + "=" + value.substring(1, value.length() - 1));
                fwProp.write("\r\n");
        //[PLATFORM]-Mod-BEGIN by TCTNB.(QiuRuifeng), 2014/02/20 PR-606265, reason [ArctTool]Arct should support Ucs2StringWLen.
            } else if(metatype.startsWith("ucs2stringwlen")){
                value = DecodeUCS2(value);
                fwProp.write(cname + "=" + value);
                fwProp.write("\r\n");
            }else {
        //[PLATFORM]-Mod-END by TCTNB.(QiuRuifeng)
                if (value.toLowerCase().startsWith("0x")) {
                    fwProp.write(cname + "=" + Integer.valueOf(value.substring(2), 16).toString());
                    fwProp.write("\r\n");
                } else {
                    fwProp.write(cname + "=" + value);
                    fwProp.write("\r\n");
                }
            }
        } else {
            // output to make file
            //added by binbin.zhao, to combine JRD product packages. Don't write the comments.
            if (!sdmId.startsWith("JRD_PRODUCT_PACKAGES_")) {
            //end added by binbin.zhao
                String[] descs = desc.split("\n");
                fwMake.write("#" + descs[0] + "\r\n");
                for (int i = 1; i < descs.length; i++) {
                    fwMake.write("#" + descs[i]);
                    fwMake.write("\n");
                }
            } //added by binbin.zhao, to combine JRD product packages

            // Input from CLID will use ascii "\n"(0x5c 0x6e) represent a LF
            // (0x0a)
            if (metatype.startsWith("asciistring")) {
                if (!(value.charAt(0) == '\"' && value.charAt(value.length() - 1) == '\"'))
                    throw new LogAbortException("wrong asciistring value: " + value + " of "
                            + sdmId);
                value = value.substring(1, value.length() - 1);
            }
          //[PLATFORM]-Add-BEGIN by TCTNB.(QiuRuifeng), 2014/02/20 PR-606265, reason [ArctTool]Arct should support Ucs2StringWLen.
             if(metatype.startsWith("ucs2stringwlen")) {
                   value = DecodeUCS2(value);
            }
         //[PLATFORM]-Add-END by TCTNB.(QiuRuifeng)
            //added by binbin.zhao, to combine JRD product packages
            if (sdmId.startsWith("JRD_PRODUCT_PACKAGES_")) {
                int value_int;
                if (value.startsWith("0x") || value.startsWith("0X")){
                    value_int = Integer.parseInt(value.substring(2, value.length()),16);
                }
                else
                    value_int = Integer.parseInt(value);
/*[PLATFORM]-Mod-BEGIN by TCTNB.(LiuJie), 2012/10/24 PR-318616, reason plf control module target or not */
                if (value_int == 0) {
/*[PLATFORM]-Mod-END by TCTNB.(LiuJie)*/
                    mJrdProductPackages += sdmId.substring(21, sdmId.length());
                    mJrdProductPackages += "\n";
/*[PLATFORM]-Add-BEGIN by TCTNB.(QiuRuifeng), 2013/06/28 PR-479045, reason The perso can't match with software when translation is updated */
                }else if(value_int == 1){
                    mJrdodexPackages += sdmId.substring(21, sdmId.length());
                    mJrdodexPackages += "\n";
                }
/*[PLATFORM]-Add-END by TCTNB.(QiuRuifeng)*/
            }
            else {
            //end added by binbin.zhao
                String[] values = value.split("\\\\n");
                fwMake.write(sdmId + " := " + values[0].trim());
                for (int i = 1; i < values.length; i++) {
                    fwMake.write(" \\\r\n");
                    fwMake.write("\t" + values[i].trim());
                }
                fwMake.write("\r\n\r\n");
            } //added by binbin.zhao, to combine JRD product packages

        }

    }
//[PLATFORM]-Add-BEGIN by TCTNB.(QiuRuifeng), 2014/02/20 PR-606265, reason [ArctTool]Arct should support Ucs2StringWLen.
    private String DecodeUCS2(String unicodeString) {
        String retValue = "";
        if(unicodeString.equals("")){
            return retValue;
        }
        unicodeString = unicodeString.replace("(", "").replace(")", "");
        String[] unicodeStrings = unicodeString.split(",");
        int length = Integer.parseInt(unicodeStrings[0].replace("0x", ""), 16);
        if(0 == length){
            return retValue;
        }
        byte[] decodeByte = new byte[length];
        for (int i = 1; i < unicodeStrings.length; i++) {
            decodeByte[i - 1] = Byte.valueOf(unicodeStrings[i].replace("0x", ""), 16);
        }
        try {
            retValue = new String(decodeByte, "utf-16");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return retValue;
    }
//[PLATFORM]-Add-END by TCTNB.(QiuRuifeng)
    //added by binbin.zhao, to combine JRD product packages
    void writeJrdProductPackages() throws IOException {
        fwMake.write("#These are the customized modules that need to been compiled");
        fwMake.write("\r\n");
        String[] jrdProductPackageNames = mJrdProductPackages.split("\n");
        fwMake.write("JRD_PRODUCT_PACKAGES" + " := " + jrdProductPackageNames[0].trim());
        for (int i = 1; i < jrdProductPackageNames.length; i++) {
            fwMake.write(" \\\r\n");
            fwMake.write("\t" + jrdProductPackageNames[i].trim());
        }
        fwMake.write("\r\n\r\n");
    }
    //end added by binbin.zhao

    /*[PLATFORM]-Add-BEGIN by TCTNB.(QiuRuifeng), 2013/06/28 PR-479045, reason The perso can't match with software when translation is updated */
    void writeJrdodexPackages() throws IOException {
        fwMake.write("#These are the customized modules that need to remove odex");
        fwMake.write("\r\n");
        String[] jrdodexPackageNames = mJrdodexPackages.split("\n");
        fwMake.write("JRD_DELETED_PACKAGES" + " := " + jrdodexPackageNames[0].trim());
        for (int i = 1; i < jrdodexPackageNames.length; i++) {
            fwMake.write(" \\\r\n");
            fwMake.write("\t" + jrdodexPackageNames[i].trim());
        }
        fwMake.write("\r\n\r\n");
    }
    /*[PLATFORM]-Add-END by TCTNB.(QiuRuifeng)*/
}
