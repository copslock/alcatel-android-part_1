/* ***************************************************************************/
/*                                                        Date : Dec 1, 2009 */
/*                      Android Resource Customization Tool                  */
/*              Copyright (c) 2009 JRD Communications, Inc.                  */
/* ***************************************************************************/
/*                                                                           */
/*    This material is company confidential, cannot be reproduced in any     */
/*    form without the written permission of JRD Communications, Inc.        */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/*   Author :  Android team                                                  */
/*   Role :    arct                                                          */
/*   Reference documents :                                                   */
/*---------------------------------------------------------------------------*/
/* Comments :                                                                */
/*     file    : Bundle.java                                                 */
/*     Labels  :                                                             */
/*===========================================================================*/
/* Modifications   (month/day/year)                                          */
/*---------------------------------------------------------------------------*/
/* date    | author    | modification                                        */
/*---------+-----------+-----------------------------------------------------*/
/*01/12/09 | Zwshi     | on creation                                         */
/*---------+-----------+-----------------------------------------------------*/
/*         |           |                                                     */
/*===========================================================================*/
/* Problems Report                                                           */
/*---------------------------------------------------------------------------*/
/* date    | author    | PR #     |                                          */
/*---------|-----------|----------|------------------------------------------*/
/*         |           |          |                                          */
/*---------|-----------|----------|------------------------------------------*/
/*         |           |          |                                          */
/*===========================================================================*/

package com.jrdcom.arct;

/**
 * Collect parameters from command line, call the respondent method in Command.
 *
 * @author zwshi
 */
public class Bundle {

    public enum ArctCommand {
        androidStringToPerso, wimdataToAndroidString , plfToPropertyMakefile, plfToKeypadLayout
    }

    private ArctCommand cmd;

    private boolean isAllLanguage = false;

    private boolean isAllModule = false;

    private String[] languages = null;

    private String[] modules = null;

    private String persoPath = null;

    private String outputPath = null;

    private String repoPath = null;

    private String plfPath = null;

    private String makePath = null;

    private String propPath = null;

    private String klPath = null;

    private boolean isDedug = false;

    private String sourcePath=null;	//added by zhiyu for reserving source res directory



    public String getSourcePath() {
        return sourcePath;
    }

    public void setSourcePath(String sourcePath) {
        this.sourcePath = sourcePath;
    }

    public ArctCommand getCmd() {
        return cmd;
    }

    public void setCmd(ArctCommand command) {
        this.cmd = command;
    }

    public boolean isAllLanguage() {
        return isAllLanguage;
    }

    public void setAllLanguage(boolean isAllLanguage) {
        this.isAllLanguage = isAllLanguage;
    }

    public String[] getLanguages() {
        return languages;
    }

    public void setLanguages(String[] languages) {
        this.languages = languages;
    }

    public void handleCommand() throws LogAbortException {
        switch (cmd) {
            case wimdataToAndroidString:
                new Command().doWimdataToAndroidString(this);
                break;
            case androidStringToPerso:
                new Command().doAndroidStringToPerso(this);
                break;
            case plfToPropertyMakefile:
                new Command().doPlfToPropertyMakefile(this);
                break;
                // add by wteng
            case plfToKeypadLayout:
                new Command().doPlfToKeypadLayout(this);
                break;
                // add end
        }
    }

    public boolean isAllModule() {
        return isAllModule;
    }

    public void setAllModule(boolean isAllModule) {
        this.isAllModule = isAllModule;
    }

    public String[] getModules() {
        return modules;
    }

    public void setModules(String[] modules) {
        this.modules = modules;
    }

    public String getPersoPath() {
        return persoPath;
    }

    public void setPersoPath(String persoPath) {
        this.persoPath = persoPath;
    }

    public String getOutputPath() {
        return outputPath;
    }

    public void setOutputPath(String outputPath) {
        this.outputPath = outputPath;
    }

    public String getRepoPath() {
        return repoPath;
    }

    public void setRepoPath(String repoPath) {
        this.repoPath = repoPath;
    }

    public boolean isDedug() {
        return isDedug;
    }

    public void setDedug(boolean isDedug) {
        this.isDedug = isDedug;
    }

    public String getPlfPath() {
        return plfPath;
    }

    public void setPlfPath(String plfPath) {
        this.plfPath = plfPath;
    }

    public String getMakePath() {
        return makePath;
    }

    public void setMakePath(String makePath) {
        this.makePath = makePath;
    }

    public String getPropPath() {
        return propPath;
    }

    public void setPropPath(String propPath) {
        this.propPath = propPath;
    }
// add by wteng
    public String getKLPath() {
        return klPath ;
    }

    public void setKLPath(String klPath) {
        this.klPath = klPath;
    }
}
