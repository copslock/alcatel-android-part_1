#!/usr/bin/env python
'''
    This script is used for get/update patch delivery tools and Do patch delivery  
    Example:
        python patch_delivery_cli.py  
'''
import os
import sys
import commands
import shutil
import re
import optparse
import signal
import subprocess
import threading
import logging


def patch_delivery(): 
    tools_path = os.path.dirname(sys.argv[0])
    print("INFO: Updating patch delivery tools ...")
    current_path = os.getcwd()
    if tools_path:
        os.chdir(tools_path)
    execmd(['git','reset','--hard'])
    execmd(['git','clean','-df'])
    result = execmd(['git','pull', 'origin', 'Vertu/Tron'],30)[2]
    if result != 0:
        print("WARN: Update patch delivery tools failed")
        
    os.chdir(current_path)
    os.system("python " + os.path.join(tools_path, "patch-delivery-gui_NB"))
    
def execmd(cmd, timeout=300):
    """ Execute a command in a new child process.  
    The child process is killed if it timesout.

    """
    kill_check = threading.Event()

    def kill_process_after_timeout(pid):
        """Callback method to kill process `pid` on timeout."""
        p = subprocess.Popen(['ps', '--ppid', str(pid)],
                             stdout=subprocess.PIPE)
        child_pids = []
        for line in p.stdout:
            if len(line.split()) > 0:
                local_pid = (line.split())[0]
                if local_pid.isdigit():
                    child_pids.append(int(local_pid))
        os.kill(pid, signal.SIGKILL)
        for child_pid in child_pids:
            os.kill(child_pid, signal.SIGKILL)
        kill_check.set()  # tell the main routine that we had to kill
        return

    try:
        process = subprocess.Popen(cmd, stdout=subprocess.PIPE,
                                   stderr=subprocess.PIPE)
        watchdog = threading.Timer(timeout,
                                   kill_process_after_timeout,
                                   args=(process.pid, ))
        watchdog.start()
        out, err = process.communicate()
        result_out = ''.join(out).strip()
        result_err = ''.join(err).strip()
        watchdog.cancel()  # if it's still waiting to run
        if kill_check.isSet():
            result_err = "Timeout: the command \"%s\" did not " \
                         "complete in %d sec." % (" ".join(cmd), timeout)
        kill_check.clear()
        if process.poll() != 0:
            logging.error('Error executing: %s', " ".join(cmd))
        return result_out, result_err, process.poll()
    except OSError as exp:
        logging.error(exp)
    except KeyboardInterrupt:
        kill_process_after_timeout(process.pid)
        watchdog.cancel()
        return
    

if __name__ == '__main__':
    patch_delivery()







