LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional

LOCAL_PREBUILT_JAVA_LIBRARIES := \
        jxl$(COMMON_JAVA_PACKAGE_SUFFIX) \
        ostermillerutils_1_07_00$(COMMON_JAVA_PACKAGE_SUFFIX)

include $(BUILD_HOST_PREBUILT)

