/* Copyright (C) 2016 Tcl Corporation Limited */
#include "alisvr.h"

static const int blen=8192+1024;
#define ALITAPATH "/firmware/image/alipay.mbn" // MODIFIED by Xiaoyun.Wei, 2016-11-14,BUG-3335803

AlipaySvr::AlipaySvr()
{
    descriptor = String16(svrName[ALISVR]);
#ifdef QSEETA
    tee = new TCLSecQSEETA();
#else
    not support
#endif
    if(!tee)
      LOGE("new TCLSecQSEETA failed");
}

AlipaySvr::~AlipaySvr()
{
    if(tee)
    {
      delete tee;
      tee=NULL;
    }
}


status_t AlipaySvr::onTransact( uint32_t code, const Parcel& data, Parcel* reply, uint32_t flags)
{
  int ret = 0;
  LOGD("Enter Alipay server code = %d", code);
  mutex.lock();

  switch(code)
  {
  case ALI_BUSINESS_PROCESS:
    ret = processAliBusiness(data, reply);
    break;
  case ALI_GETKEY:
    ret = processData(data, reply);
    break;
  case ALI_PROVISIONKEY:
    ret = provisionData(data, reply);
    break;
  case ALI_AUTH:
    ret = authData(reply);
    break;
  default:
    LOGD("exit from default mutex cmd = %d", code);
    mutex.unlock();
    return BBinder::onTransact(code, data, reply, flags);
    break;
  }

  LOGD("exit normal");
  mutex.unlock();

  return ret;
}

int AlipaySvr::authData(Parcel* reply)
{
  int ret=0;
  qseecom_cmd *qcc=0;
  int len=0; //data.readInt32();
  if(tee && (tee->isLoad() || tee->load(ALITAPATH, blen) == 0))
  {
    qcc = (qseecom_cmd *)tee->getBuf();
    qcc->cmd = ALI_AUTH;
    qcc->len = len + sizeof(qseecom_cmd);
    len += sizeof(qseecom_cmd);
    if (len & QSEECOM_ALIGN_MASK)
      len = QSEECOM_ALIGN(len);
    if(tee->sendCmd(qcc) == 0)
    {
      ret = *(int *)(((char *)(qcc))+len);
      LOGD("auth ret=%d", ret);
      reply->writeInt32(ret);
      ret=0;
    }
    else
    {
      LOGE("Alipay tee sendcmd failed");
      tee->shutDown();
      ret=-1;
    }
  }
  else
  {
    LOGE("Alipay tee load failed");
    ret=-1;
  }
  
  return ret;
}

int AlipaySvr::provisionData(const Parcel& data, Parcel* reply)
{
  int ret=0;
  qseecom_cmd *qcc=0;
  int len=data.readInt32();
  if(tee && (tee->isLoad() || tee->load(ALITAPATH, blen) == 0))
  {
    qcc = (qseecom_cmd *)tee->getBuf();
    qcc->cmd = ALI_PROVISIONKEY;
    qcc->len = len + sizeof(qseecom_cmd);
    data.read((char *)(qcc+1), len);
    len += sizeof(qseecom_cmd);
    if (len & QSEECOM_ALIGN_MASK)
      len = QSEECOM_ALIGN(len);
    if(tee->sendCmd(qcc) == 0)
    {
      ret = *(int *)(((char *)(qcc))+len);
      reply->writeInt32(ret);
      if(!ret)
      {
        reply->writeInt32(qcc->len);
        if(qcc->len)
          reply->write(((char *)(qcc))+len+4, qcc->len);
      }
      ret=0;
    }
    else
    {
      LOGE("Alipay tee sendcmd failed");
      tee->shutDown();
      ret=-1;
    }
  }
  else
  {
    LOGE("Alipay tee load failed");
    ret=-1;
  }
  
  return ret;
}

int AlipaySvr::processData(const Parcel& data, Parcel* reply)
{
  int ret=0;
  qseecom_cmd *qcc=0;
  int len=data.readInt32();
  if(tee && (tee->isLoad() || tee->load(ALITAPATH, blen) == 0))
  {
    qcc = (qseecom_cmd *)tee->getBuf();
    qcc->cmd = ALI_GETKEY;
    qcc->len = len + sizeof(qseecom_cmd);
    data.read((char *)(qcc+1), len);
    len += sizeof(qseecom_cmd);
    if (len & QSEECOM_ALIGN_MASK)
      len = QSEECOM_ALIGN(len);
    if(tee->sendCmd(qcc) == 0)
    {
      ret = *(int *)(((char *)(qcc))+len);
      reply->writeInt32(ret);
      if(!ret)
      {
        reply->writeInt32(qcc->len);
        if(qcc->len)
          reply->write(((char *)(qcc))+len+4, qcc->len);
      }
      ret=0;
    }
    else
    {
      LOGE("Alipay tee sendcmd failed");
      tee->shutDown();
      ret=-1;
    }
  }
  else
  {
    LOGE("Alipay tee load failed");
    ret=-1;
  }
  
  return ret;
}

int AlipaySvr::processAliBusiness(const Parcel& data, Parcel* reply)
{
  int ret=0;
  qseecom_cmd *qcc=0;
  int len=data.readInt32();
  if(tee && (tee->isLoad() || tee->load(ALITAPATH, blen) == 0))
  {
    qcc = (qseecom_cmd *)tee->getBuf();
    qcc->cmd = ALI_BUSINESS_PROCESS;
    qcc->len = len + sizeof(qseecom_cmd);
    data.read((char *)(qcc+1), len);
    len += sizeof(qseecom_cmd);
    if (len & QSEECOM_ALIGN_MASK)
      len = QSEECOM_ALIGN(len);
    if(tee->sendCmd(qcc) == 0)
    {
      reply->writeInt32(*(int *)(((char *)(qcc))+len));
      reply->writeInt32(qcc->len);
      reply->writeInt32(qcc->len);
      if(qcc->len)
        reply->write(((char *)(qcc))+len+4, qcc->len);
      hexdump("processAli", ((char *)(qcc))+len, qcc->len+4);
    }
    else
    {
      LOGE("Alipay tee sendcmd failed");
      tee->shutDown();
      ret=-1;
    }
  }
  else
  {
    LOGE("Alipay tee load failed");
    ret=-1;
  }

  return ret;
}



