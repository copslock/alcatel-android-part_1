#ifndef FACTORY_H
#define FACTORY_H


#include <binder/IServiceManager.h>
#include <binder/IBinder.h>
#include <binder/Parcel.h>
#include <binder/ProcessState.h>
#include <binder/IPCThreadState.h>

using namespace android;

class FactorySvr : public BBinder
{
public:
  FactorySvr();
  virtual ~FactorySvr();
  virtual const String16& getInterfaceDescriptor() const;
  static void instantiate();

protected:
  virtual status_t onTransact( uint32_t code,
    const Parcel& data,
    Parcel* reply,
    uint32_t flags = 0);

private:
  String16 mydescriptor;
};




#endif


