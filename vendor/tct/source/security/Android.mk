LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)
LOCAL_C_INCLUDES := $(TARGET_OUT_INTERMEDIATES)/KERNEL_OBJ/usr/include \
                    $(TARGET_OUT_HEADERS)/common/inc \
                    $(LOCAL_PATH) \
                    $(LOCAL_PATH)/../include \
                    $(LOCAL_PATH)/../include/callback \
                    $(LOCAL_PATH)/../../../qcom/proprietary/securemsm/QSEEComAPI \

LOCAL_ADDITIONAL_DEPENDENCIES := $(TARGET_OUT_INTERMEDIATES)/KERNEL_OBJ/usr

LOCAL_SHARED_LIBRARIES := \
        libc \
        libcutils \
        libutils \
        libbinder \
        libQSEEComAPI

ifeq ($(TCT_TARGET_HDCP_ENABLE),true)
LOCAL_CFLAGS += -DHDCP_ENABLE
LOCAL_SHARED_LIBRARIES += libDxHdcp
endif

#LOCAL_CFLAGS += -DTCLAPP_DBG

LOCAL_32_BIT_ONLY := true
LOCAL_MODULE := secsvr
LOCAL_SRC_FILES := main.cpp common.cpp factory.cpp alisvr.cpp qseeta.cpp base_svr.cpp
LOCAL_MODULE_TAGS := optional

LOCAL_CFLAGS += -DSEC_DUMP
#LOCAL_CFLAGS += -DCONSOLE_DEBUG
include $(BUILD_EXECUTABLE)




include $(CLEAR_VARS)

LOCAL_C_INCLUDES := $(TARGET_OUT_INTERMEDIATES)/KERNEL_OBJ/usr/include \
                    $(TARGET_OUT_HEADERS)/common/inc \
                    $(LOCAL_PATH) \

LOCAL_ADDITIONAL_DEPENDENCIES := $(TARGET_OUT_INTERMEDIATES)/KERNEL_OBJ/usr

LOCAL_SHARED_LIBRARIES := \
        libc \
        libcutils \
        libutils \
        libbinder \

LOCAL_MODULE := testtclapp
LOCAL_SRC_FILES := test.cpp common.cpp
LOCAL_MODULE_TAGS := optional
LOCAL_CFLAGS := -fpermissive
LOCAL_CFLAGS += -DSEC_DUMP
LOCAL_CFLAGS += -DCONSOLE_DEBUG

include $(BUILD_EXECUTABLE)



include $(CLEAR_VARS)

LOCAL_C_INCLUDES := $(TARGET_OUT_INTERMEDIATES)/KERNEL_OBJ/usr/include \
                    $(TARGET_OUT_HEADERS)/common/inc \
                    $(LOCAL_PATH) \

LOCAL_ADDITIONAL_DEPENDENCIES := $(TARGET_OUT_INTERMEDIATES)/KERNEL_OBJ/usr

LOCAL_SHARED_LIBRARIES := \
        libc \
        libcutils \
        libutils \
        libbinder \

LOCAL_MODULE := libteeclientjni
LOCAL_SRC_FILES := teeclient.cpp common.cpp
LOCAL_MODULE_TAGS := optional
LOCAL_CFLAGS := -fpermissive
LOCAL_CFLAGS += -DSEC_DUMP
LOCAL_CFLAGS += -DCONSOLE_DEBUG

include $(BUILD_SHARED_LIBRARY)

