/* Copyright (C) 2016 Tcl Corporation Limited */
#ifndef TCLSEC_QSEETA_H
#define TCLSEC_QSEETA_H

#include "QSEEComAPI.h"
#include "tee.h"


class TCLSecQSEETA : public TCLSecTee
{
public:
  TCLSecQSEETA():qseeHandle(NULL){}
  ~TCLSecQSEETA(){shutDown();}

  virtual int load(const char *path, int bs);
  virtual int sendCmd(qseecom_cmd *cmd);
  virtual int shutDown();
  virtual char *getBuf() const{return qseeHandle ?  (char *)(qseeHandle->ion_sbuffer) : NULL; }

private:
  struct QSEECom_handle * qseeHandle;
};



#endif
