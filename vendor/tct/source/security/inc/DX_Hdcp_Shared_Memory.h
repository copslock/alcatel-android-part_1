/***************************************************************************
*   Copyright 2015 (C) Discretix Technologies Ltd.
*
*   This software is protected by copyright, international
*   treaties and various patents. Any copy, reproduction or otherwise use of
*   this software must be authorized by Discretix in a license agreement and
*   include this Copyright Notice and any other notices specified
*   in the license agreement. Any redistribution in binary form must be
*   authorized in the license agreement and include this Copyright Notice
*   and any other notices specified in the license agreement and/or in
*   materials provided with the binary distribution.
*
*   Some software modules which may be used or linked, may be subject to
*   respective "open source" license agreement(s), and may be copyrighted by
*   their respective author(s), as detailed in such respective license
*   agreement(s); please refer to those license agreement(s), and to any
*   Copyright file(s) or Copyright notices included in them, for further
*   information with regard to copyright of third parties and other license
*   provisions.
****************************************************************************/

#ifndef	__DX_HDCP_SHARED_MEMORY_H__
#define	__DX_HDCP_SHARED_MEMORY_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include "DX_Hdcp_Types.h"

typedef struct DX_Hdcp_SharedMemHandle_s *DX_Hdcp_SharedMemHandle;

    /*! Allocates and maps shared memory buffer.
    Typically this API should be used for testing.
    \parameters:
        size - Required allocation size.
        *handle - Allocated handler.
        **virtualAddr - Mapped virtual address (output).
    \return uint32_t - 0 on success, error code on failure.
    */
    EXTERNAL_API uint32_t DX_HDCP_Shared_Memory_Alloc(uint32_t size, DX_Hdcp_SharedMemHandle *handle, uint8_t **virtualAddr, bool secured);

    /*! Unmaps and frees shared memory buffer.
    Typically this API should be used for testing.
    \parameters:
        handle - Freed handler.
    \return uint32_t - 0 on success, error code on failure.
    */
    EXTERNAL_API uint32_t DX_HDCP_Shared_Memory_Free(DX_Hdcp_SharedMemHandle handle);

    /*! The DX_HDCP_Shared_Memory_ConvertMemHandle function converts a handle to usable handle.
    Typically this API should be used for testing.
    \parameters:
        handle - original handler.
        **convertedHandle - converted handle (output).
    \return uint32_t - 0 on success, error code on failure.
    */
    EXTERNAL_API uint32_t DX_HDCP_Shared_Memory_ConvertMemHandle(DX_Hdcp_SharedMemHandle handle, uint64_t *convertedHandle);

#ifdef __cplusplus
}
#endif

#endif//__DX_HDCP_SHARED_MEMORY_H__
