/* Copyright (C) 2016 Tcl Corporation Limited */
#include <binder/IServiceManager.h>
#include <binder/IBinder.h>
#include <binder/Parcel.h>
#include <binder/ProcessState.h>
#include <binder/IPCThreadState.h>
#include "common.h"


#define ALISVR "tcl.alipay"


using namespace android;


int invoke_command(void *send_buf, uint32_t sbuf_len, void *rcv_buf, uint32_t *rbuf_len)
{
  sp<IServiceManager> sm = defaultServiceManager();
  sp<IBinder> b = sm->getService(String16(ALISVR));

  LOGD("sbuf_len=%d, rbuf_len=%d", sbuf_len, *rbuf_len);

  Parcel in1,out1;
  LOGD("1");
  in1.writeInt32(sbuf_len);
  LOGD("2");
  in1.write(send_buf, sbuf_len);
  LOGD("3");
  int ret = b->transact(3, in1, &out1, 0);
  LOGD("ret=%d", ret);
  if(ret)
  {
    LOGE("alipay transact ret=%d", ret);
    return ret;
  }
  ret = out1.readInt32();
  LOGD("ret=%d", ret);
  if(ret)
  {
    LOGE("alipay process ret=%d", ret);
    return ret;
  }
  ret = out1.readInt32();
  out1.read(rcv_buf, (uint32_t)ret < *rbuf_len ? ret : *rbuf_len);
  *rbuf_len = (uint32_t)ret < *rbuf_len ? ret : *rbuf_len;

  return 0;
}
