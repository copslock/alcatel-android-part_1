#ifndef COMMON_H
#define COMMON_H

#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include <android/log.h>

#undef ALOGD
#undef ALOGI
#undef ALOGW
#undef ALOGE
#define MODNAME "SecSvr"

void hexdump(const char *hdr, const void *addr, uint32_t len);
#ifdef CONSOLE_DEBUG
#define LOGD(fmt, ...) printf(fmt"\n", ##__VA_ARGS__)
#define LOGI(fmt, ...) printf(fmt"\n", ##__VA_ARGS__)
#define LOGW(fmt, ...) printf(fmt"\n", ##__VA_ARGS__)
#define LOGE(fmt, ...) printf(fmt"\n", ##__VA_ARGS__)
#else
#define LOGD(fmt, ...) __android_log_print(ANDROID_LOG_DEBUG, MODNAME, fmt, ##__VA_ARGS__)
#define LOGI(fmt, ...) __android_log_print(ANDROID_LOG_INFO, MODNAME, fmt, ##__VA_ARGS__)
#define LOGW(fmt, ...) __android_log_print(ANDROID_LOG_WARN, MODNAME, fmt, ##__VA_ARGS__)
#define LOGE(fmt, ...) __android_log_print(ANDROID_LOG_ERROR, MODNAME, fmt, ##__VA_ARGS__)
#endif
#define ALOGD(fmt, ...) LOGD(fmt, ##__VA_ARGS__)
#define ALOGI(fmt, ...) LOGI(fmt, ##__VA_ARGS__)
#define ALOGW(fmt, ...) LOGW(fmt, ##__VA_ARGS__)
#define ALOGE(fmt, ...) LOGE(fmt, ##__VA_ARGS__)
#define LOG(fmt, ...) LOGI(fmt, ##__VA_ARGS__)



#endif
