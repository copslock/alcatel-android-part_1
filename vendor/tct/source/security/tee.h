/* Copyright (C) 2016 Tcl Corporation Limited */
#ifndef TCLSEC_TEE_H
#define TCLSEC_TEE_H

typedef struct
{
  int cmd;
  int len;
}__attribute__((packed)) qseecom_cmd;

#define TAMAX_PATH 256

class TCLSecTee
{
public:
  TCLSecTee():isload(0), bufSize(0){}
  virtual ~TCLSecTee(){}
  virtual int load(const char *path, int bs)=0;
  virtual int sendCmd(qseecom_cmd *cmd)=0;
  virtual int shutDown()=0;
  virtual char *getBuf() const{ return NULL;}

  int isLoad() const{return isload;}

protected:
  int isload;
  int bufSize;
  char ta[TAMAX_PATH];
};


#endif
