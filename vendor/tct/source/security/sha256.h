/* Copyright (C) 2016 Tcl Corporation Limited */
#ifndef DEVICEPROGRAMMER_SECURITY_H
#define DEVICEPROGRAMMER_SECURITY_H

#include <stdint.h>

typedef uint8_t  uint8;
typedef uint16_t uint16;
typedef uint32_t uint32;
typedef uint64_t uint64;
typedef uint8_t  boolean;

#define TRUE  1
#define FALSE 0

#define CONTEXT_LEFTOVER_FIELD_SIZE 64

/* This is the state of a running hash, known by all platforms */
struct __sechsh_ctx_s
{
	uint32  counter[2];
	uint32  iv[16];  // is 64 byte for SHA2-512
	uint8   leftover[CONTEXT_LEFTOVER_FIELD_SIZE];
	uint32  leftover_size;
};

/* Forward declarations of the internal working functions */
void sechsharm_sha256_init(   struct __sechsh_ctx_s* );
void sechsharm_sha256_update( struct __sechsh_ctx_s*, uint8*, uint32*, uint8*, uint32 );
void sechsharm_sha256_final(  struct __sechsh_ctx_s*, uint8*, uint32*, uint8* );

void PerformSHA256(
    uint8* inputPtr,
    uint32 inputLen,
    uint8* outputPtr);

#endif
