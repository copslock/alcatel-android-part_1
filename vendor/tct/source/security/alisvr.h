/* Copyright (C) 2016 Tcl Corporation Limited */
#ifndef TCLSEC_ALIPAY_H
#define TCLSEC_ALIPAY_H

#include "base_svr.h"
#define QSEETA
#ifdef QSEETA
#include "qseeta.h"
#else
not support
#endif


typedef enum
{
  //ALI_FP_NOTIFY=2,
  ALI_BUSINESS_PROCESS=3,
  ALI_GETKEY=5,
  ALI_PROVISIONKEY=6,
  ALI_AUTH=7,
}AliSvr;

class AlipaySvr : public TCLSecBaseSvr<AlipaySvr, ALISVR>
{
public:
  AlipaySvr();
  virtual ~AlipaySvr();

protected:
  virtual status_t onTransact( uint32_t code, const Parcel& data, Parcel* reply, uint32_t flags = 0);
  int processAliBusiness(const Parcel& data, Parcel* reply);
  int processData(const Parcel& data, Parcel* reply);
  int provisionData(const Parcel& data, Parcel* reply);
  int authData(Parcel* reply);

private:

};



#endif

