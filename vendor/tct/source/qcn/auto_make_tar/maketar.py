#!/usr/bin/python

import os
import sys
import struct
import xml.dom.minidom
import getParaFile as pvc
import confFileList_8x16 as conf
import parseDB as db
import parseDefinition as de
import getExtraNV
import getVersion
import zlib # for compressed RFNV
import tarfile
import traceback
import mcfg_factory
import zipfile
import re
import shutil
import parse_mbnxml
import stat
import commands


PROJECT = ""
VERSION = ""

CONF_PATH = "./nv/item_files/conf/"
NV_PATH   = "./nvm/num/"
RFNV_PATH = "./nv/item_files/rfnv/"
TVN_FILE = "./tvn.txt"

def getParaFiles(proj, ver=None, server=None):
    """Get the parameters xml & bin files of the given proj name from PVC system """
    Xml = None
    Bin = None
    global VERSION
    print "----maketar getParaFiles project=" + proj
    sessionid = pvc.getProjectId(proj, server)
    versionid = pvc.getVersionId(sessionid, ver, server)
    if versionid:
        items = pvc.getItemList(versionid[1], server)
        VERSION = versionid[0]
    else:
        raise ValueError
    if items and items.has_key('Xml'):
        Xml = pvc.getItem(items['Xml'], server)
    if items and items.has_key('Bin'):
        Bin = pvc.getItem(items['Bin'], server)
    if items and items.has_key('Mbn'):
        Mbn = pvc.getItem(items['Mbn'], server)

    if not Xml:
        print "Xml file not found!"
        raise ValueError

    return (Xml,Bin,Mbn)

def getNumOfFields(s):
    """
    struct.calcSize() will Return the size of the struct (and hence of the bytes) corresponding to the given format.
    This function will return how many fields of a struct have.
    """
    if type(s) is not str:
        return 0
    size = 0
    num = ''
    for i in range(len(s)):
        if s[i].isalpha() or s[i] == '?':
            size = size + 1 if not num else size + int(num)
            num = ''
        elif s[i].isdigit():
            num = num + s[i]
        else:
            num = ''

    return size


def writeValue(Id, name, value, encode = "dec"):

    dstr = value.strip().replace(' ', '').split(',')
    cond = db.isNvHasConditionValue(Id)
    if cond:
        format = db.getConditionStructFormat(Id, dstr, cond)
    else:
        format = db.getStructFormat(Id)

    if not format:
        print "Can't get format string for: ", Id, name
        raise ValueError

    format = '=' + format
    d = []

    # Save the values into a array before do packing.
    # For datatype = 9 (null terminated string), we should calculate the string length at run time.
    for i in range(len(dstr)):
        if len(dstr[i]) > 2 and not dstr[i].startswith('-') and not dstr[i].lower().startswith('0x') and not dstr[i].isdigit() :
        #if len(dstr[i]) > 2 and dstr[i][:1].isalpha():
            if format.find('0s') > -1:
                format = format.replace('0s', 128 * 'B') # pad 0s string to 128 bytes
                for c in str(dstr[i]):
                    d.append(struct.unpack('B',c)[0])
                d.append(0)
            else:
                for c in str(dstr[i]):
                    d.append(struct.unpack('B',c)[0])

        else:
            try:
                d.append(int(dstr[i]))
            except ValueError:
                d.append(int(dstr[i],16))
    format = format.replace('s', 'B')
    # special case...for nv 66050, get format from database is HHHH, infact, it should be BBBBBBBB.
    # It's an error on QCT NV format database.
    if Id == '66050':
        format = 'BBBBBBBB'

    # Some values in exported XML from PVC are stored in hex format, while the data type is defined
    # as decimal format, like nv #946, its value is '0,104,4', while the format string is 'BH'.
    # in this case, pack() will raise exception.
    formatSize = struct.calcsize(format)
    fieldSize = getNumOfFields(format)
    print 'NV: ', Id, format, formatSize, fieldSize, d
    if len(d) > formatSize:
        if formatSize >= fieldSize :
            if len(d) > formatSize:
                format = str(formatSize) + 'B'
            d = d[:formatSize]

        else:
            print "Can't be here"
            raise ValueError

    elif len(d) < formatSize:
        print "WARN:Data length is less then fieldSize"
        size = 0
        num = ''
        if len(format) >=len(d):
            for i in range(len(format)):
                if format[i].isalpha():
                    size = (size + int(num)) if num else (size + 1)
                    num = ''
                    if size >= len(d):
                        padSize = fieldSize - len(d)
                        format = format[:i +1]
                        #if (struct.calcsize(format) + padSize) > 128:
                        #    padSize = padSize - (struct.calcsize(format) + padSize) % 128
                        # Check nv size
                        nvSize = db.getNvMapSize(Id)
                        if nvSize:
                            padSize += nvSize - formatSize

                        if padSize > 0 :
                            format = format + str(padSize) + 'x'
                            if Id == '57':
                                format = format + 'x'
                        print 'format: ', format, len(format)
                        break
                elif format[i].isdigit():
                    num = num + format[i]
                else:
                    num = ''
        else:
            for i in range(len(format)):
                if format[-(i+1)].isalpha():
                    if not num:
                        continue
                    else:
                        break
                elif format[-(i+1)].isdigit():
                    num = format[-(i+1)] + num

            if not num:
                num = 0
            target = len(d) - (formatSize - int(num))
            padSize = int(num) - len(d)

            nvSize = db.getNvMapSize(Id)
            if nvSize:
                padSize += nvSize - formatSize

            if padSize > 0:
                format = format.replace(num, str(target)) + str(padSize) + 'x'
            else:
                format = format.replace(num, str(target))
            print 'format2: ', format


    elif len(d) == formatSize and formatSize > fieldSize:
        BBytes = {
            '=' : '=',
            '?' : '?',
            'B' : 'B',
            'H' : '2B',
            'I' : '4B',
            'Q' : '8B',
            'b' : 'B',
            'h' : '2B',
            'i' : '4B',
            'q' : '8B',
        }
        f2 = ''
        for i in range(len(format)):
            f2 += BBytes[format[i]]
        format = f2
    elif len(d) > fieldSize:
        d = d[:fieldSize]
    else:
        nvSize = db.getNvMapSize(Id)
        if nvSize:
            padSize = nvSize - formatSize
            if padSize > 0 :
                format = format + str(padSize) + 'x'
        print 'format3: ', format, len(format)

    # do packing according to the givin format.
    try:
        df = struct.pack(format, *d)
    except Exception,e:
        print "This NV can't be packed correctly: ", Id
        raise e

    # Some CDMA NV items may have values in different NAM_0 and NAM_1, like NV #10, its first field
    # has name 'nam' with 'uint8' data type, in this case, the filename in EFS should be changed
    # accordingly, like 10_1, 57_2, 57_4
    if db.NVhasNamField(Id):
        index = struct.unpack('B', df[0])[0]
        df = df[1:]
        if index > 0:
            name = name + '_' + str(index)

    # write to the file.
    f = file(name, 'wb')
    f.write(df)
    f.truncate()
    f.close()

    return

def writeValue2(Id, name, value, encode = "dec"):
    """
    Write RF NV EFS item
    """
    format = de.getRFNVFormat(Id)
    if not format:
        print "Can't get format string for: ", Id, name
        return
    format = '=' + format
    dstr = value.strip().replace(' ', '').split(',')
    d = []

    # Save the values into a array before do packing.
    # For datatype = 9 (null terminated string), we should calculate the string length at run time.
    try:
        for i in range(len(dstr)):
            if dstr[i][:1].isalpha():
                print "RF NV can't contain alpha character"
                raise ValueError
            elif dstr[i]:
                d.append(int(dstr[i]))
    except ValueError:
        #MODIFIED-BEGIN by Ding Yanghunag, 2016-04-16,BUG-1664027 # MODIFIED by Fan Yi, 2016-04-25,BUG-1989853
        print "ERROR: can't save NV %s value to array - %s %s" % (Id, i, dstr[i])
        sys.exit(1)
        #MODIFIED-END by Ding Yanghunag,BUG-1664027
    # Some values in exported XML from PVC are stored in hex format, while the data type is defined
    # as decimal format, like nv #946, its value is '0,104,4', while the format string is 'BH'.
    # in this case, pack() will raise exception.
    formatSize = struct.calcsize(format)
    fieldSize = getNumOfFields(format)
    print Id, len(d), format, formatSize, fieldSize, d
    if len(d) > formatSize:
        if formatSize >= fieldSize :
            if len(d) > formatSize:
                format = str(formatSize) + 'B'
            d = d[:formatSize]
        else:
            print "Can't be here"
            raise ValueError

    elif len(d) < formatSize:
        print "WARN:Data length is less then fieldSize"
        # truncate the format string
        size = 0
        num = ''
        for i in range(len(format)):
            if format[i].isalpha():
                size = (size + int(num)) if num else (size + 1)
                num = ''
                if size >= len(d):
                    format = format[:i +1]
                    print "WriteValue2: ", format, len(format)
                    break
            elif format[i].isdigit():
                num = num + format[i]
            else:
                num = ''
    elif len(d) == formatSize and formatSize > fieldSize:
        print "enter here?"
        BBytes = {
            '?' : '?',
            '=' : '=',
            'B' : 'B',
            'H' : '2B',
            'I' : '4B',
            'Q' : '8B',
            'b' : 'B',
            'h' : '2B',
            'i' : '4B',
            'q' : '8B',
        }
        f2 = ''
        for i in range(len(format)):
            f2 += BBytes[format[i]]
        format = f2
    elif len(d) >= fieldSize:
        d = d[:fieldSize]

    # do packing according to the givin format.
    try:
        df = struct.pack(format, *d)
    except Exception,e:
        print "This NV can't be packed correctly: ", Id
        raise e


    if db.NVhasNamField(Id):
        index = struct.unpack('B', df[0])[0]
        df = df[1:]
        if index > 0:
            name = name + '_' + str(index)
    # RFNV may has "compressed" attribute
    #compressed = de.isNvCompressed(Id)
    if de.isNvCompressed(Id):
        print "RFNV may has compressed attribute"
        df = zlib.compress(df, -1)

    if de.isNvHasVariantSize(Id):
        print "%s has veriant size" % Id
        #exit()

    # write to the file.
    f = file(name, 'wb')
    f.write(df)
    f.truncate()
    f.close()

    return

#MODIFIED-BEGIN by Ding Yanghunag, 2016-04-16,BUG-1664027
def writeByteStream(Id, name, value, encode = "dec"):
    """
    Since the mapping type is bytestream, so ignore the definiation of NV format, but write the value to NV item directly
    """

    print Id, name, encode, value
    base = 10 if encode == "dec" else 16
    d = map(lambda x : int(x, base), value.split())

    df = struct.pack("%dB" % len(d), *d)

    # write to the file.
    f = file(name, 'wb')
    f.write(df)
    f.truncate()
    f.close()
    #MODIFIED-END by Ding Yanghunag,BUG-1664027


def writeEFSFile(path, value, length):
    if os.path.isfile(path):
        print 'Duplicated file exist - ', path
        raise ValueError
    try:
        os.makedirs(os.path.dirname(path))
    except OSError, e:
        if not str(e).startswith("[Errno 17] File exists"):
            raise OSError, e

    d = []
    for i in range(int(length)):
        d.append(int(value[2*i:2*i+2], 16))

    format = str(length) + 'B'
    # do packing according to the givin format.
    df = struct.pack(format, *d)

    # write to the file.
    f = file(path, 'wb')
    f.write(df)
    f.truncate()
    f.close()

def handleNVItem(nv):
    """ Process NV items, id < 65535
    The RFNV system continues to use 16-bit numeric address numbers to address
    individual items. The address space above 20,000 is mapped to EFS, while the address
    space below 20,000 is mapped to the NV service. In the future, it will be possible
    that all RFNV items will be mapped to EFS.

    """
    Id = nv.getAttribute('id').strip()
    encode = nv.getAttribute('encoding')
    subscriptionid = nv.getAttribute('subscriptionid').strip() if nv.hasAttribute('subscriptionid') else "0"
    assert len(subscriptionid) == 1
    assert len(nv.childNodes) == 1
    value = nv.childNodes[0].data

    if int(subscriptionid) > 0:
        if int(Id) < 20000:
            path = "./nvm/context%s/%s" % (subscriptionid, Id)
            if not os.path.exists(os.path.dirname(path)):
                os.makedirs(os.path.dirname(path))
        else:
            path = RFNV_PATH + '000' + Id + "_Subscription" + subscriptionid.zfill(2)
    else:
        if int(Id) < 20000:
            path = NV_PATH + Id
        else:
            path = RFNV_PATH + '000' + Id

    #print "NV path: " + path
    writeValue(Id, path, value, encode)


def handleEFSItem(nv):
    """ Process EFS items """
    Id = nv.getAttribute('id').strip()
    encode = nv.getAttribute('encoding')
    assert len(nv.childNodes) == 1
    value = nv.childNodes[0].data

    item = db.getEfsPath(Id)

    if not item:
        print ("Can't find EFS full pathname for item - %s " % Id)
        raise ValueError

    subscriptionid = nv.getAttribute('subscriptionid').strip() if nv.hasAttribute('subscriptionid') else "0"
    assert len(subscriptionid) == 1
    if int(subscriptionid) > 0:
        item = item + "_Subscription" + subscriptionid.zfill(2)

    #print "EFS Items: ", item, value
    createEFSItem(Id, item, value, encode)

def handleRFNVItem(nv):
    """ process RFNV , which ID is < 20000 and > 8000
    """
    Id = nv.getAttribute('id').strip()
    encode = nv.getAttribute('encoding')
    mapping= nv.getAttribute('mapping') #MODIFIED by Ding Yanghunag, 2016-04-16,BUG-1664027
    subscriptionid = nv.getAttribute('subscriptionid').strip() if nv.hasAttribute('subscriptionid') else "0"
    assert len(subscriptionid) == 1
    assert len(nv.childNodes) == 1
    value = nv.childNodes[0].data

    if int(subscriptionid) > 0:
        if int(Id) < 20000:
            path = "./nvm/context%s/%s" % (subscriptionid, Id)
            if not os.path.exists(os.path.dirname(path)):
                os.makedirs(os.path.dirname(path))
        else:
            path = RFNV_PATH + '000' + Id + "_Subscription" + subscriptionid.zfill(2)
    else:
        if int(Id) < 20000:
            path = NV_PATH + Id
        else:
            path = RFNV_PATH + '000' + Id

    # MODIFIED-BEGIN by Fan Yi, 2016-04-25,BUG-1989853
    #MODIFIED-BEGIN by Ding Yanghunag, 2016-04-16,BUG-1664027
    if mapping == "byteStream":
        writeByteStream(Id, path, value, encode)
    else:
        writeValue2(Id, path, value, encode)
    #MODIFIED-END by Ding Yanghunag,BUG-1664027
    # MODIFIED-END by Fan Yi,BUG-1989853

def handleFiles(nv):
    """ Process the file uploaded to PVC
    """
    #Id = nv.getAttribute('id').strip()
    encode = nv.getAttribute('encoding')
    assert (encode=='hex')
    filename = nv.getAttribute('name')
    if not filename.startswith('/'):
        print "file path not correctly! - ", filename
        return
    #check subscirption field for files
    subscriptionid = nv.getAttribute('subscriptionid').strip() if nv.hasAttribute('subscriptionid') else "0"
    assert subscriptionid == '0'

    filesize = nv.getAttribute('filesize')
    value = nv.childNodes[0].data

    path = './' + filename
    print path, value

    writeEFSFile(path, value, filesize)

    # Add efs item into conf files
    if conf.conf_list.has_key(filename):
        #print "Create conf file for: " + item
        name = CONF_PATH + '/' + conf.conf_list[filename]
        f = file(name, "ab")
        f.write(filename + '\n')
        f.close()
    else:
        print "WARN:Can't find conf file for: " + filename
        #raise ValueError

def createEFSItem(Id, item, value, encode="dec"):
    # Recursively create the EFS item files
    path = "./" + os.path.dirname(item) + '/'
    try:
        os.makedirs(path)
    except OSError, e:
        if not str(e).startswith("[Errno 17] File exists"):
            raise OSError, e

    # Write the value into efs file
    name = "./" + item
    writeValue(Id, name, value, encode)

    # Add efs item into conf files
    if conf.conf_list.has_key(item):
        #print "Create conf file for: " + item
        name = CONF_PATH + '/' + conf.conf_list[item]
        f = file(name, "ab")
        f.write(item + '\n')
        f.close()
    else:
        print "WARN:Can't find conf file for: " + item
        #raise ValueError

def signMbn(mbnsource):
    cwd = os.getcwd()

    # set config for efuse sign server
    proj = "/8996"
    servip = "172.16.11.158"
    servport = "18962"
    tagname = "qdsp6sw"

    # generate CSC config xml
    configxml = xml.dom.minidom.Document()
    rootElement = configxml.createElement('codesign')
    projElement = configxml.createElement('project')
    typeElement = configxml.createElement('qdsp6sw')
    typeElement.setAttribute('path', mbnsource)
    projElement.appendChild(typeElement)
    projElement.setAttribute('name', proj)
    projElement.setAttribute('servip', servip)
    projElement.setAttribute('servport', servport)
    rootElement.appendChild(projElement)
    configxml.appendChild(rootElement)
    f = open('CSCconfig.xml', 'w')
    configxml.writexml(f, addindent = "  ", newl = "\n", encoding = 'utf-8')
    f.close()

    # sign mbn
    print "now sign %s file..." % mbnsource
    (status,output) = commands.getstatusoutput('./Client2')
    if status != 0:
        print "ERROR: \n %s" % output
        print "ERROR: when sign %s" % mbnsource
        os.remove('CSCconfig.xml')
        raise status
    else:
        print "copy signed file %s/output/qdsp6sw/qdsp6sw.mbn to %s" % (cwd, mbnsource)
        os.chmod('output/qdsp6sw/qdsp6sw.mbn',stat.S_IRUSR|stat.S_IWUSR|stat.S_IRGRP|stat.S_IROTH)
        shutil.copy('output/qdsp6sw/qdsp6sw.mbn',mbnsource)
        os.remove('CSCconfig.xml')
        shutil.rmtree('output')


def parseXML(doc):
    """
    Parse the xml parameter file that download from PVC
    First, check if nv can get format from NvDefinition.xml, if not, then try to get format from qxdm database.
    For those Id > 65535, they were EFS item files.
    """
    if not doc:
        return None
    dom = xml.dom.minidom.parse(doc)
    NvSource = dom.getElementsByTagName("NvSource")[0]
    if not NvSource:
        return None
    NvItems = NvSource.getElementsByTagName("NvItem")
    for nv in NvItems:
        Id = int(nv.getAttribute('id'))

        if de.isNvHasDefinition(str(Id)):
            handleRFNVItem(nv)
        elif db.isNvInDatabase(str(Id)):
            if Id < 20000:
                handleNVItem(nv)
            else:
                handleEFSItem(nv)
        elif Id >= 900000:
            handleFiles(nv)
        else:
            print "Unsupported NV ID found: ", Id
            raise ValueError

    dom.unlink()

def parseBin(doc):
    """Parse the bin parameter file that download from PVC"""
    pass


def parseMbn(mbn,generate_mbn,basedir,tarout):
    if not generate_mbn:
        if mbn:
            os.remove(mbn)
        return None
    cwd = os.getcwd()
    mbnparam = {}
    signtool = os.path.join(basedir,'signmbn','Client2')

    mbn_zip = zipfile.ZipFile(mbn,'r')
    mbn_zip.extractall('.')
    for item in os.listdir('.'):
        m = re.match(r'[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}', item)
        if m:
            zipfolder=item
            break

    for root, dirs, files in os.walk(zipfolder):
        for filename in files:
            mbnfile = os.path.join(root,filename)
            try:
                (zipfolder, region, operator, mbnxml) = mbnfile.split('/')
                os.chdir(os.path.join(zipfolder, region, operator))
                m = re.match(r'^mcfg_[s|h]w',mbnxml)
                if m:
                    parse_mbnxml.main(TopPath, mbnxml)
                    targetpath = os.path.join(tarout, region, operator)
                    targetname = m.group()+'.mbn'
                    mbnparam[os.path.join(region, operator)] = dict(typ=m.group(), path=os.path.join(region, operator, targetname))
                    targetmbn = os.path.join(targetpath, targetname)
                    if not os.path.isdir(targetpath):
                        os.makedirs(targetpath)
                        shutil.copy('mcfg_gen.mbn',targetmbn)
                    dom = xml.dom.minidom.parse(mbnxml)
                    NvEfsItems = dom.getElementsByTagName("NvEfsItemData")
                    NvEfsItems.extend(dom.getElementsByTagName("NvEfsFile"))
                    if NvEfsItems:
                        for EfsItem in NvEfsItems:
                            efsfile = EfsItem.getAttribute('fullpathname')
                            if not efsfile:
                                efsfile = EfsItem.getAttribute('targetPath')
                            if conf.conf_list.has_key(efsfile):
                                confname = os.path.join(cwd, CONF_PATH, conf.conf_list[efsfile])
                                f = file(confname, "ab")
                                f.write(efsfile + '\n')
                                f.close()
                            else:
                                print "WARN:Can't find conf file for: " + efsfile
                    os.chdir(cwd)
                else:
                    print "Invalid xml name: [%s], should be start with mcfg_hw or mcfg_sw" % mbnxml
                    os.chdir(cwd)
                    continue
            except parse_mbnxml.ParseMbnError as err:
                print "Error: %s" % err
                sys.exit(1)
            except ValueError:
                print "Error: MBN xml path on Parameter Management System should be MBN/region-name/operator-name"
                sys.exit(1)
            except Exception:
                print "Unexpected error when parse %s" % mbnfile
                sys.exit(1)

    if os.path.exists(zipfolder):
        shutil.rmtree(zipfolder)
    os.remove(mbn)
    return mbnparam

def usage():
    print "\n--------------------------------------------------------"
    print "Usage: \nPlease launch script with a project name. Ex:"
    print sys.argv[0] + " $(TARGET_PRODUCT) $BUILD_TYPE $input_plf $output_path"
    sys.exit(-1)

def createWorkSpace(proj, ver, output, server):
    prjs = pvc.getProjectList(server)

    if not prjs.has_key(proj):
        print "Can't find specified project: " + proj
        print "Hereafter the supported projects from PVC:"
        print prjs.keys()
        usage()
    else:
        global PROJECT
        PROJECT = proj
    from datetime import datetime

    start = datetime.now()
    dirname = os.path.join (output, proj + '-' + ver)

    if os.path.exists(proj):
        print "Can't be here..."
        sys.exit(1)
    # create all relative folders
    os.mkdir(dirname)
    os.chdir(dirname)
    if not os.path.exists(CONF_PATH):
        os.makedirs(CONF_PATH)
    if not os.path.exists(NV_PATH):
        os.makedirs(NV_PATH)
    if not os.path.exists(RFNV_PATH):
        os.makedirs(RFNV_PATH)

    return dirname


def createVersionNV(baseline):
    # create version num NV #2
    print "Create version NV: V" + VERSION
    if VERSION:
        value = str(int(float(VERSION) * 10))
        print value

        name = NV_PATH + '2'
        writeValue('2', name, value)

        # write version value to /tvn.txt file.
        # FIXME: Do not override TVN version now.
        #f = file(TVN_FILE, 'w')
        #f.write(value)
        #f.truncate()
        #f.close()

    # 2ndly, check the platform baseline NV, try to fill it from device/tct/Build_info.txt
    if baseline:
        print baseline
        if not os.path.exists(baseline):
            print 'input baseline file not exist.'
            return
        # MODIFIED-BEGIN by erlei.ding, 2016-05-20,BUG-2194552
        local_dom = xml.dom.minidom.parse(baseline)
        root = local_dom.documentElement
        #buildids = root .getElementsByTagName('build_id')
        #ver = buildids[0].firstChild.data.strip()
        builditems = root .getElementsByTagName('build')
        for builditem in builditems:
            nodename = builditem.getElementsByTagName('name')
            name = nodename[0].childNodes[0].nodeValue
            if name == "common":
                node_build_id = builditem.getElementsByTagName('build_id')
                ver = node_build_id[0].childNodes[0].nodeValue
                break
        # MODIFIED-END by erlei.ding,BUG-2194552
        print 'baseline: ', ver
        name = NV_PATH + '2808'
        writeValue('2808', name, ','.join(map(str, map(ord,ver))))

def writeFile(name, value):
    if not os.path.exists(os.path.dirname(name)):
        os.makedirs(os.path.dirname(name))
    f = file(name, 'wb')
    f.write(value)
    f.truncate()
    f.close()

def createExtraNV(mmitest, extra_plf_path):
    """ check the input extra plf file, and add those NV into tarball"""

    # Firstly, parse NV from plf file
    extraNv = getExtraNV.getExtraNvFromPLF(extra_plf_path)
    print 'ExtraNV: ', extraNv

    if not extraNv:
        extraNv = {}

    # Then, check build type to add NV #453
    if mmitest == 'true':
        extraNv['453'] = '1'
    else:
        extraNv['453'] = '0'

    # 3rd, save SVN number to NV #5153
    svn = getExtraNV.getSVN(os.path.join(os.path.dirname(extra_plf_path), 'isdm_sys_properties.plf'))
    if svn:
        extraNv['5153'] = svn
    else:
        print "ERROR: No SDMID found - ro_def_software_version, which is used for SVN!"
        raise ValueError
    model = getExtraNV.getModel(os.path.join(os.path.dirname(extra_plf_path), 'isdm_sys_properties.plf'))
    if model:
        extraNv['5079'] = model
    else:
        print "ERROR: NO SDMID found - PRODUCT_MODEL, which is used for nv 5079"
        raise ValueError
    # MODIFIED-BEGIN by Ding Yanghunag, 2016-08-19,BUG-2766016
    region = getExtraNV.getRegion(os.path.join(os.path.dirname(extra_plf_path), 'isdm_sys_properties.plf'))
    if region:
        extraNv['3'] = region
    else:
        if os.getenv("MAKECMDGOALS") == "perso":
            print "ERROR: NO SDMID found - def_region_for_perso , which is used for nv 3 (operator region)"
        # MODIFIED-END by Ding Yanghunag,BUG-2766016

    # fourth, write nv files
    for Id, v in extraNv.items():
        if type(v) is dict:
            # extraNv is a EFS file item
            if v['format'] == 'ascii':
                writeFile('./' + Id, v['value'] + "\n")
            elif v['format'] == 'array':
                dstr = v['value'].split(',')
                print "nv_format : %s" % v['nv_format']
                format = str(v['nv_format'])
                print "EFS ID :%s" % Id
                print "EFS format:%s" % format
                d = []
                if format == 'None':
                    print "EFS file custom is array format , but not set nv_format"
                    for value in dstr:
                        if value:
                            d.append(int(value))
                    writeFile('./' + Id, struct.pack("%dB" % len(d), *d))
                else:
                    for i in range(len(dstr)):
                        if len(dstr[i]) == 0:
                           d.append(dstr[i])
                        elif len(dstr[i]) > 2 and not dstr[i].startswith('-') and not dstr[i].lower().startswith('0x') and not dstr[i].isdigit() :
                             d.append(dstr[i])
                        else:
                             d.append(int(dstr[i]))
                    print "EFS value :%s" % d
                    writeFile('./' + Id, struct.pack(format, *d))
            else:
                print "Format Error", Id
                raise TypeError
        else:
            if int(Id) < 65535 :
                name = NV_PATH + Id
                if Id != '57':
                    writeValue(Id, name, v)
                else:
                    start = 99
                    ecc = v.split(',')
                    for num in ecc:
                        print 'ecc: ', num
                        ecclist = []
                        temp = list(num)
                        if len(temp)%2 == 1:
                            temp.append('0')
                        for i in range(0, len(temp), 2):
                            ecclist.append(str(int(temp[i+1]+temp[i], 16)))
                        value = str(start) + ',3,' + str(len(num)) + ',' + ','.join(ecclist)
                        writeValue(Id, name, value)
                        start += 1
            else:
                print Id, v
                # for those EFS item with ID > 65535
                item = db.getEfsPath(Id)
                if not item:
                    print ("Can't find EFS full pathname for item - %s " % Id)
                    raise ValueError
                #daul-SIM need add subscriptionid at PLF files
                if extraNv.has_key('subscriptionid'):
                    subscriptionid = extraNv.getAttribute('subscriptionid').strip()
                else:
                    subscriptionid = "0"
                assert len(subscriptionid) == 1
                if int(subscriptionid) > 0:
                    item = item + "_Subscription" + subscriptionid.zfill(2)

                createEFSItem(Id, item, v)



def resortConfFiles():
    """ Scan all sub folder under path to found out all *.plf and *.global files"""
    print "\nstart working at %s\n" % CONF_PATH
    for root, dirs, files in os.walk(CONF_PATH):
        for f in files:
            if f.endswith(".conf"):
                name = os.path.join(root, f)
                with open(name, 'r+') as fh:
                    l = list(set(fh.readlines()))
                    l.sort()
                    fh.seek(0)
                    fh.write(''.join(l))
                    fh.truncate()


def createTarball(output_file):

    tar = tarfile.open(output_file , "w")
    #print os.getcwd()
    for f in os.listdir(os.getcwd()):
        print "Add ", f
        if f.endswith('.xml'):
            continue
        tar.add(f)

    tar.close()
    return True


def getFileList(dirname):
    dat = os.listdir(dirname)
    dirs = []
    #print cwd, dirname
    for item in dat:
        if os.path.isdir(dirname + '/' + item):
            dirs = dirs + getFileList(dirname + '/' + item)
        else:
            dirs.append(dirname + '/' + item)
    return dirs

def appendFiles(appendDir):
    print os.getcwd()
    print appendDir
    if not os.path.exists(appendDir):
        appendDir = '../' + appendDir
    if not os.path.exists(appendDir):
        print "Can't find appendix files folder"
        return

    appendFileList = getFileList(appendDir)
    for item in appendFileList:
        idx = item.find(appendDir)
        if idx > -1:
            path = item[idx + len(appendDir):]
            path = os.path.dirname('./' + path)
            print item, path
            if not os.path.exists(path):
                os.makedirs(path)
            os.system('cp -r ' + item + ' ' + path)

def appendMbnFiles(mbns, outputdir):
    workdir = os.getcwd()
    mcfg_sw_path = []
    mcfg_hw_path = []
    sign_mbn = False

    signtool = os.path.join(outputdir,'Client2')
    if os.path.isfile(signtool):
        sign_mbn = True
        os.chmod(signtool,stat.S_IRWXU|stat.S_IRGRP|stat.S_IXGRP|stat.S_IROTH|stat.S_IXOTH)

    for name, mbn in mbns.items():
        if sign_mbn:
            os.chdir(outputdir)
            signMbn(os.path.join(os.getcwd(),mbn['path']))
            os.chdir(workdir)
        if mbn['typ'] == "mcfg_sw":
            mcfg_sw_path.append(os.path.join(outputdir, mbn['path'] + "@" + name))
        elif mbn['typ'] == "mcfg_hw":
            mcfg_hw_path.append(os.path.join(outputdir, mbn['path'] + "@" + name))

    if mcfg_sw_path:
        mcfg_factory.mcfg_factory(workdir, "mcfg_sw", ','.join(mcfg_sw_path))
    if mcfg_hw_path:
        mcfg_factory.mcfg_factory(workdir, "mcfg_hw", ','.join(mcfg_hw_path))
    if os.path.isdir(os.path.join(outputdir,'mbn')):
        shutil.rmtree(os.path.join(outputdir,'mbn'))

def makeTarball(project, version, mini_type, extra_plf, output_path, output_file, baseline, server, generate_mbn):

    basedir = os.path.dirname(os.path.realpath(__file__))
    try:
        db.prepareDB()
        de.parseDefinition()

        dirname = createWorkSpace(project, version, output_path, server)

        (Xml,Bin,Mbn) = getParaFiles(PROJECT, version, server)

        parseXML(Xml)
        parseBin(Bin)
        mbn = parseMbn(Mbn,generate_mbn,basedir,output_path)

        createVersionNV(baseline)
        createExtraNV(mini_type, extra_plf)

        resortConfFiles()
        if generate_mbn and mbn:
            if os.path.exists(os.path.join(basedir,'Client2')):
                shutil.copy(os.path.join(basedir,'Client2'),os.path.join(output_path,'Client2'))
            appendMbnFiles(mbn, output_path)

        if createTarball(os.path.join(output_path, output_file)):
            print "Generate tarball for %s success.\nPlease find it at folder: %s" % (PROJECT, output_path)
    except Exception,e:
        traceback.print_exc(e)
        print "Failed to generate tarball"
        sys.exit(1)

def compareFormat():
    """
    This function is used to check those NV id 0<id<20000, which both defined struct on NvDefinition and NV database, if they define the same format or not
    """
    count = 0
    db.prepareDB()
    de.parseDefinition()

    def regularFormat(f):
        if not f:
            return
        size = 0
        num = ''
        format = ''
        for i in range(len(f)):
            if f[i].isalpha():
                if num:
                    format = format + f[i] * int(num)
                    num = ''
                else:
                    format = format +f[i]
            elif f[i].isdigit():
                num = num + f[i]
            else:
                num = ''
        return format


    for Id in range(20000):
        if Id == 0:
            continue
        f1 = de.getRFNVFormat(str(Id))
        f2 = db.getStructFormat(str(Id))
        f1 = regularFormat(f1)
        f2 = regularFormat(f2)
        if f1 and f2 and f1 != f2:
            if f1.upper() == f2.upper():
                continue
            print Id, f1, f2
            count += 1
    print "Count = ", count

def main():
    """ This is the main entry of make tar script.
        It parse the product_parameter_vesrion.xml and get all parameter version need to generated.
    """
    if len(sys.argv) < 2:
        print "\nPlease input one of projects.\n"
        usage()

    # FIXME: try to parse the input option more smart here.
    #print("%d   %s  %s  %s  %s   %s" %(len(sys.argv),sys.argv[0], sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4]))
    assert(len(sys.argv) == 6)

    (target_product, mini_type, extra_plf, baseline, output_path) = (sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5])

    prd = getVersion.getProdParaVersion(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'product_parameter_vesrion.xml'))
    if not prd:
        print "Product list is empty!"
        sys.exit(1)
    if not prd.has_key(target_product):
        print "Your specified target project is not exist in product_parameter_vesrion.xml!"
        sys.exit(1)

    cwd = os.getcwd()
    print "WORKING DIR: ", cwd
    global TopPath
    TopPath = cwd
    global Target
    Target = target_product

    global CONF_PATH, NV_PATH, RFNV_PATH, TVN_FILE

    params = prd[target_product]
    for proj, d in params.items():
        os.chdir(cwd)
        print "----makeTar output=%s, version=%s, server=%s..."%(d['output'], d['version'], d['server'])
        if d['server'] == "nb":
            server = "http://172.16.11.155"
        elif d['server'] == "sh":
            server = "http://172.24.61.25"
        else:
            server = "http://172.16.11.155"
        makeTarball(proj, d['version'], mini_type, os.path.abspath(extra_plf), output_path, d['output'], os.path.abspath(baseline), server, d['generate_mbn'])


if __name__ == '__main__':
    main() #MODIFIED by Ding Yanghunag, 2016-04-16,BUG-1664027
