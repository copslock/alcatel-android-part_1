#!/usr/bin/python

# This script is aim to get parameters from PVC system

import os
import sys
import urllib2
import json
import re
from HTMLParser import HTMLParser

# Parameter server IP
#SERVER = "http://172.16.11.155"
HANDSHAKEID = "TCL-NB-TCT-9527"

#URL_OF_PROJECT_LIST = SERVER + "/servlet/TreeServlet?handshake=" \
#                               + HANDSHAKEID + "&id=null&type=100"  
#URL_OF_VERSION_LIST = SERVER + "/servlet/TreeServlet?handshake=" \
#                               + HANDSHAKEID + "&type=101&id="
#URL_OF_ITEM_LIST = SERVER + "/servlet/ExportProServlet?handshake=" \
#                               + HANDSHAKEID + "&versionId="     
#URL_TO_DOWNLOAD = SERVER + "/servlet/DownloadServlet?handshake=" \
#                               + HANDSHAKEID + '&'

class Parser(HTMLParser):
    """
    Parse the html file, and return the branch list info
    """
    def __init__(self):
        self.links = {} 
        self.getATag= 0
        self.path = None
        HTMLParser.__init__(self)

    def handle_starttag(self, tag, attrs):
        if tag == 'a':            
            for item, value in attrs:
                if item == "href" and value.startswith("servlet/DownloadServlet?"):
                    self.path = value[len("servlet/DownloadServlet?"):]
                    self.getATag = 1
                    
                

    def handle_data(self, data):
        if self.getATag:
            typ = data[0:data.index(':')]
            self.links[typ] = self.path

    def handle_endtag(self, tag):
        self.getATag = 0
        
    def return_links(self):
        return self.links


def urlopen(url, server):
    """
    read content from the given URL
    """
    print ('Reading content from url: ' + url)
    req = urllib2.Request(url)
    req.add_header('Referer', server)
    req.add_header('User-Agent', 'Auto-Script fake-client')

    rsp = urllib2.urlopen(req)
    ret = rsp.read()
    
    return ret

def getProjectList(server=None):
    """ Get project list by url 
    http://172.16.11.155/servlet/TreeServlet?handshake=TCL-NB-TCT-9527&id=null&type=100
    respone: 
    a json string, format is {"message":"","data":"","code":""}, data is a json array, each item in the list will contain a ID and display
    """
    
    if not server:
        return None
    URL_OF_PROJECT_LIST = server + "/servlet/TreeServlet?handshake=" \
                               + HANDSHAKEID + "&id=null&type=100"
    
    ret = urlopen(URL_OF_PROJECT_LIST, server)
    if not ret:
        return None
    ret = json.loads(ret)
    if ret['message'] != 'Success':
        return None

    data = json.loads(ret['data'])

    if type( data['tree']) is not list:
        return None

    ids = {}
    for item in data['tree']:
        t = json.loads(item)
        ids[t['displayName']] = t['id']
    #print ids
    return ids

def getProjectId(proj=None, server=None):
    if not proj:
        return None

    ids = getProjectList(server)
    if not ids.has_key(proj):
        return None
    print "Get project list for " + proj
    return ids[proj]
   
def getVersionList(session=None, server=None):
    """ Get version list of a project by url 
    http://172.16.11.155/servlet/TreeServlet?handshake=TCL-NB-TCT-9527&id=<session id>&type=101
    response:
    a json string, format is {"message":"","data":"","code":""}, data is a json array. Similar to project list.
    """
    if not session:
        return None
    if not server:
        return None
    
    URL_OF_VERSION_LIST = server + "/servlet/TreeServlet?handshake=" \
                               + HANDSHAKEID + "&type=101&id="
    url = URL_OF_VERSION_LIST + session

    ret = urlopen(url, server)
    if not ret:
        return None
    ret = json.loads(ret)
    if ret['message'] != 'Success':
        return None
    data = json.loads(ret['data'])
    if type( data['tree']) is not list:
        return None

    versions = {}
    
    for item in data['tree']:
        t = json.loads(item)
        displayName = t['displayName']
        
        match = re.search('V([0-9][0-9].[0-9][0-9])', displayName)
        if not match:
            continue
        name = match.group(1)
        versions[name] = t['id']
    #print versions.keys()
    return versions

def getVersionId(session, ver=None, server=None):
    """
    return version id of the given session, if version is none, then return the latest  parameter version
    """
    versions = getVersionList(session, server)
    print "Get version list:"
    print versions
    
    if not versions:
        return None
    if ver:
        match = re.search('V([0-9][0-9].[0-9][0-9])', ver)
        if not match:
            return None
        ver = match.group(1)
        if not versions.has_key(ver):
            return None
        else:
            return (ver, versions[ver])
    else:
        latest = "01.00"
        for item in versions.keys():
            if float(item) > float(latest):
                latest = item
        print "Latest version: v" + latest
        if not versions.has_key(latest):
            return None
        else:
            return (latest, versions[latest])

def getItemList(versionid=None, server=None):
    """
    Get a list of downloadable items of a dedicate version by url
    http://172.16.11.155/servlet/ExportProServlet?handshake=TCL-NB-TCT-9527&versionId=id
    Response:
    A json string, format is {"message":"","data":"","code":""}, data is a section of html code, we can't the link from the html to download .xml and .bin files
    """
    if not versionid:
        return None
    if not server:
        return None
    
    URL_OF_ITEM_LIST = server + "/servlet/ExportProServlet?handshake=" \
                               + HANDSHAKEID + "&versionId="
    url = URL_OF_ITEM_LIST + versionid
    ret = urlopen(url, server)
    if not ret:
        return None
    ret = json.loads(ret)
    if ret['message'] != 'Success':
        return None
    data = ret['data']
    
    parser = Parser()
    parser.feed(data)
    
    return  parser.return_links()

        

def getItem(path=None, server=None):
    """
    Get item from the given URL.
    http://172.16.11.155/href&handshake=TCL-NB-TCT-9527
    Response:
    download the .xml and .bin files, return the saved filename
    """
    if not path:
        return None
    if not server:
        return None
    
    URL_TO_DOWNLOAD = server + "/servlet/DownloadServlet?handshake=" \
                               + HANDSHAKEID + '&'
    url = URL_TO_DOWNLOAD + path
    url = url.replace(' ', '%20')
    print ('Reading content from url: ' + url)
   
    rsp = urllib2.urlopen(url, server)
    if rsp.info().has_key('Content-Disposition'):
        filename = rsp.info()['Content-Disposition'].split('filename=')[1]
        filename = filename.replace('"', '').replace("'",'')
        print "Get file: " + filename
        f = open(filename, "wb")
        f.write(rsp.read())
        f.close()
        return filename
    else:
        return None

if __name__ == '__main__':
    sessionid = getProjectId('Idol4s_EMEA','http://172.16.11.155')
    versionid = getVersionId(sessionid, "V01.40",'http://172.16.11.155')
    if versionid:
        items = getItemList(versionid[1],'http://172.16.11.155')
    if items.has_key('Xml'):
        xml = getItem(items['Xml'],'http://172.16.11.155')
    if items.has_key('Bin'):
        bin = getItem(items['Bin'],'http://172.16.11.155')
    if items.has_key('Mbn'):
        mbn = getItem(items['Mbn'],'http://172.16.11.155')
