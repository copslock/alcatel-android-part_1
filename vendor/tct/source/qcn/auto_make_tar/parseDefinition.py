#!/usr/bin/python 

import os, sys
import xml.dom.minidom

NV_DEFINITION_FILE = os.path.join(os.path.dirname(os.path.realpath(__file__)), "./database/NvDefinition.xml")
NV_DEFINITION = {}
DATA_TYPES = {}
RAW_DATA_TYPE = {
    'uint8' : 'B',
    'uint16': 'H',
    'uint32': 'I',
    'uint64': 'Q',
    'int8'  : 'b',
    'int16' : 'h',
    'int32' : 'i',
    'int64' : 'q',
}

def isRawDataType(name):
    global RAW_DATA_TYPE
    return True if RAW_DATA_TYPE.has_key(name) else False

def isNvCompressed(nv):
    global NV_DEFINITION

    if not NV_DEFINITION.has_key(nv):
        return False
    if not NV_DEFINITION[nv].has_key('compressed'):
        return False
    return True

def isNvVariantType(nv):
    global NV_DEFINITION

    if not NV_DEFINITION.has_key(nv):
        return False
    if not NV_DEFINITION[nv].has_key('variantType'):
        return False
    return True

def isNvHasVariantSize(nv):
    global NV_DEFINITION

    if not NV_DEFINITION.has_key(nv):
        return False
    if NV_DEFINITION[nv].has_key('variableSize') or NV_DEFINITION[nv].has_key('variantType'):
        return True 
    return False
    
def dumpNvStruct(nv):
    global NV_DEFINITION
    
    if not NV_DEFINITION.has_key(nv):
        return None

    return NV_DEFINITION[nv]

def isNvHasDefinition(nv):
    global NV_DEFINITION
    
    return True if NV_DEFINITION.has_key(nv) else False

def handleDataType(datatypes):
    global DATA_TYPES
    for item in datatypes:
        name = item.getAttribute('name')
        if isRawDataType(name):
            continue

        DATA_TYPES[name] = []
        # ignore typeid attribute now, don't know what it was used to
        #if item.hasAttribute('typeid'):
        #    DATA_TYPES[name]['typeid'] = item.getAttribute('typeid')
        members = item.getElementsByTagName('Member')
        for member in members:
            data = {}            
            data['num']  = int(member.getAttribute('sizeOf').strip())
            data['type'] = member.getAttribute('type')
            DATA_TYPES[name].append(data)
    return
                

def handleNvItems(nvItems):
    global NV_DEFINITION

    for item in nvItems:
        nv = {}
        Id = item.getAttribute('id')
        nv['name'] = item.getAttribute('name')
        if item.hasAttribute('compressed') and item.getAttribute('compressed') == 'true':
            nv['compressed'] = True
        if item.hasAttribute('variantType') and item.getAttribute('variantType') == 'true':
            nv['variantType'] = True
        if item.hasAttribute('variableSize') and item.getAttribute('variableSize') == 'true':
            nv['variableSize'] = True

        nv['member'] = []
        members = item.getElementsByTagName('Member')
        for member in members:
            data = {}            
            data['num']  = int(member.getAttribute('sizeOf').strip())
            data['type'] = member.getAttribute('type')
            nv['member'].append(data)
            #if nv.has_key('variantType') and nv.has_key('variableSize'):
            #    print Id, data['type']

        NV_DEFINITION[Id] = nv

    return  

def parseDefinition():
    """
    Readin the entire nvdefinition.xml, and parse it
    """
    global NV_DEFINITION
    if not NV_DEFINITION: 
        print "parseDefinition()"
        dom = xml.dom.minidom.parse(NV_DEFINITION_FILE)

        dataTypes = dom.getElementsByTagName("DataType")
        nvItems = dom.getElementsByTagName("NvItem")

        handleDataType(dataTypes)
        handleNvItems(nvItems)

        dom.unlink()

def getFormat(member):
    global RAW_DATA_TYPE, DATA_TYPES

    format = ''
    for field in member:
        typ = field['type']
        sizeof = field['num']
        
        if isRawDataType(typ):
            f = RAW_DATA_TYPE[typ]
            if sizeof > 1:
                f = (field['num']) * f
            format += f
        elif DATA_TYPES.has_key(typ):
            f = getFormat(DATA_TYPES[typ])
            format += f * sizeof
        else:
            print "err!"
            raise ValueError

    return format

def getRFNVFormat(nv):
    global NV_DEFINITION

    if not NV_DEFINITION:
        parseDefinition()

    if not NV_DEFINITION.has_key(nv):
        print "No such nv item: ", nv
        return None

    if not NV_DEFINITION[nv].has_key('member'):
        print "NV %s don't has any of member" % nv
        return None

    # assert each nv item has 'member' field
    s = NV_DEFINITION[nv]['member']
    
    return getFormat(s)
    

if __name__ == '__main__':  
    parseDefinition()
    if len(sys.argv) > 1:
        print getRFNVFormat(sys.argv[1])
