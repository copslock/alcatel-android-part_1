#!/usr/bin/python

import sys
import os
import xml.dom.minidom as minidom
import struct
import commands
import shutil
import re


# MODIFIED-BEGIN by Fan Yi, 2016-04-25,BUG-1989853
class ParseMbnError(Exception):
    def __init__(self, returncode, cmd, output=None):
        self.returncode = returncode
        self.cmd = cmd
        self.output = output
    def __str__(self):
        return "Command '%s' returned non-zero exit status %d" % (self.cmd, self.returncode)
        # MODIFIED-END by Fan Yi,BUG-1989853

def usage():
    print "\n--------------------------------------------------------"
    print "Usage: \nPlease launch script with a nv xml path. Ex:"
    print sys.argv[0]+" xmlpath"
    print "\n"
    sys.exit(-1)

def generate_c_from_xml(xmlfile,xmlfolder):

    cmd = "perl " + os.path.join(basedir,'mcfg_generate.pl')+" -i " + xmlfile + " -o " + os.path.join(xmlfolder,'tmp.c')+" -b " + os.path.join(xmlfolder,'')+" -xml"

    (status, output) = commands.getstatusoutput(cmd)
    if status == 0:
        print 'generate_c_from_xml successful'
    else:
        print cmd
        print output
        raise ParseMbnError(status, cmd, "generate_c_from_xml failed, please check the detail error information") # MODIFIED by Fan Yi, 2016-04-25,BUG-1989853

def generate_o_from_c(xmlfolder):

    gencfg = ('-Wall',
              '-Wpointer-arith',
              '-Wstrict-prototypes',
              '-Wnested-externs',
              '-Werror-high',
              '-Werror-medium',
              '-mv55',
              '-Uqdsp6',
              '-Uq6sim',
              '-Uqdsp6r0',
              '-Os',
              '-g',
              '-fdata-sections',
              '-ffunction-sections',
              '-nostdlib',
              '-fno-exceptions',
              '-fno-strict-aliasing',
              '-fstack-protector',
              '-DBUILD_LICENSE_ENVIRONMENT=NON_GPL_ENVIRONMENT',
              '-DNON_GPL_ENVIRONMENT=1',
              '-DGPL_ENVIRONMENT=2',
              '-D__FILENAME__=tmp.c',
              '-D__MSMHW_APPS_PROC__=2',
              '-D__MSMHW_MODEM_PROC__=1',
              '-D__MSMHW_PROC_DEF__=__MSMHW_MODEM_PROC__',
              '-DMSMHW_MODEM_PROC',
              '-DIMAGE_MODEM_PROC',
              '-DIMAGE_QDSP6_PROC',
              '-DFEATURE_RUN_ON_SINGLE_HWTHREAD',
              '-DARCH_QDSP6',
              '-DTHREAD_SAFE',
              '-DCONFIG_RESTRICTED_VM=1',
              '-DMACHINE_Q6SIM',
              '-DENDIAN_LITTLE',
              '-DASSERT=ASSERT_FATAL',
              '-DDAL_CONFIG_IMAGE_MODEM',
              '-DDAL_CONFIG_TARGET_ID=0x8996')

    cmd = os.path.join(hexagon,'qc','bin','hexagon-clang')+" "+" ".join(gencfg)+" -o " + os.path.join(xmlfolder,'tmp.o') + " -c " + os.path.join(xmlfolder,'tmp.c')

    (status, output) = commands.getstatusoutput(cmd)
    if status == 0:
        print 'generate_o_from_c successful'
    else:
        print cmd
        print output
        raise ParseMbnError(status, cmd, "generate_o_from_c failed, please check the detail error information") # MODIFIED by Fan Yi, 2016-04-25,BUG-1989853

def generate_lib_from_o(xmlfolder):

    cmd = os.path.join(hexagon,'gnu','bin','hexagon-ar')+" -rc " + os.path.join(xmlfolder,'mcfg_gen.lib')+" " + os.path.join(xmlfolder,'tmp.o')

    (status, output) = commands.getstatusoutput(cmd)
    if status == 0:
        print 'generate_lib_from_o successful'
    else:
        print cmd
        print output
        raise ParseMbnError(status, cmd, "generate_lib_from_o failed, please check the detail error information") # MODIFIED by Fan Yi, 2016-04-25,BUG-1989853

def generate_elf_from_lib(xmlfolder):

    gencfg = ('-mv55',
              '-nostdlib',
              '--strip-debug',
              '--strip-all',
              '--whole-archive',
              '--section-start',
              '.start=0x0',
              '--entry=0x0')

    cmd = os.path.join(hexagon,'gnu','bin','hexagon-ld')+" "+" ".join(gencfg)+" -T" + os.path.join(basedir,'mcfg_gen.lcs')+" -L"+os.path.join(hexagon,'dinkumware','lib','v55')+" --start-group "+os.path.join(xmlfolder,'mcfg_gen.lib')+" --end-group  -o  " + os.path.join(xmlfolder,'mcfg_gen.elf')

    (status, output) = commands.getstatusoutput(cmd)
    if status == 0:
        print 'generate_elf_from_lib successful'
    else:
        print cmd
        print output
        raise ParseMbnError(status, cmd, "generate_elf_from_lib failed, please check the detail error information") # MODIFIED by Fan Yi, 2016-04-25,BUG-1989853

def strip_elf(xmlfolder):

    gencfg = ('-I',
              'elf32-little',
              '--only-section',
              '.mcfg_seg')

    cmd = os.path.join(hexagon,'gnu','bin','hexagon-objcopy')+" "+" ".join(gencfg)+" "+os.path.join(xmlfolder,'mcfg_gen.elf')+" " + os.path.join(xmlfolder,'mcfg_gen_strip.elf')

    (status, output) = commands.getstatusoutput(cmd)
    if status == 0:
        print 'strip_elf successful'
    else:
        print cmd
        print output
        raise ParseMbnError(status, cmd, "strip_elf failed, please check the detail error information") # MODIFIED by Fan Yi, 2016-04-25,BUG-1989853

def generate_mbn_from_elf(xmlfolder):

    cmd = "perl "+os.path.join(basedir,'mcfg_conv_elf_2_mbn.pl')+" "+os.path.join(xmlfolder,'mcfg_gen_strip.elf')+" -o " + os.path.join(xmlfolder,'mcfg_gen_strip.mbn')

    (status, output) = commands.getstatusoutput(cmd)
    if status == 0:
        print 'generate_mbn_from_elf successful'
    else:
        print cmd
        print output
        raise ParseMbnError(status, cmd, "generate_mbn_from_elf failed, please check the detail error information") # MODIFIED by Fan Yi, 2016-04-25,BUG-1989853

def generate_qct_mbn(xmlfolder,imagetype):

    cmd = "python " + os.path.join(basedir,'mbn_builder.py')+" "+os.path.join(xmlfolder,'mcfg_gen.mbn')+" "+os.path.join(xmlfolder,'mcfg_gen_strip.mbn')+' '+imagetype

    (status, output) = commands.getstatusoutput(cmd)
    if status == 0:
        print 'generate_qct_mbn successful'
    else:
        print cmd
        print output
        raise ParseMbnError(status, cmd, "generate_qct_mbn failed, please check the detail error information") # MODIFIED by Fan Yi, 2016-04-25,BUG-1989853

def main(toppath,xmlfile):

    if len(sys.argv) < 3:
        print "\nPlease input one of nv xml."
        usage()

    global basedir
    basedir = os.path.dirname(os.path.realpath(__file__))
    global hexagon
    hexagon_path = os.path.join(toppath,'vendor','tct','buildtools','HEXAGON_Tools')
    hexagon_ver  = '6.4.06'
    version_path = os.path.join(hexagon_path,'version')
    if os.path.isfile(version_path):
        f = open(version_path,'r')
        lines = f.readlines()
        f.close()
        if len(lines) > 0:
            hexagon_ver = lines[0].strip()
    hexagon = os.path.join(hexagon_path,hexagon_ver)
    xmlfolder = os.path.dirname(os.path.realpath(xmlfile))

    dom = minidom.parse(xmlfile)
    NvFileList = dom.getElementsByTagName("NvEfsFile")
    for nvfile in NvFileList:
        buildpath = nvfile.getAttribute('buildPath')
        if not os.path.isdir(os.path.dirname(buildpath)):
            os.makedirs(os.path.dirname(buildpath))
        targetValue = nvfile.getAttribute('targetValue')
        if len(targetValue)%2 == 0:
            filesize = len(targetValue)/2
        else:
            raise ValueError("Invalid value, pls check %s" % buildPath) # MODIFIED by Fan Yi, 2016-04-25,BUG-1989853

        value = targetValue.decode('hex')
        f = file(buildpath, 'wb')
        f.write(value)
        f.close()

    root = dom.documentElement
    for NvTagItem in root.childNodes:
        if NvTagItem.nodeType == minidom.Node.ELEMENT_NODE:
            if NvTagItem.hasAttribute('targetValue'):
                NvTagItem.removeAttribute('targetValue')
    f = open(xmlfile,'w')
    dom.writexml(f, encoding = 'utf-8')
    f.close()

    m = re.search(r'^(mcfg_[sh]w)_.*',xmlfile)
    if m:
        ImageType = m.group(1)
    else:
        raise ValueError("%s name with wrong image type" % xmlfile) # MODIFIED by Fan Yi, 2016-04-25,BUG-1989853

    generate_c_from_xml(xmlfile,xmlfolder)
    generate_o_from_c(xmlfolder)
    generate_lib_from_o(xmlfolder)
    generate_elf_from_lib(xmlfolder)
    strip_elf(xmlfolder)
    generate_mbn_from_elf(xmlfolder)
    generate_qct_mbn(xmlfolder,ImageType)


if __name__ == '__main__':
    main(sys.argv[1],sys.argv[2])
