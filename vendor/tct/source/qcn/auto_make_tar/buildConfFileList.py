#!/usr/bin/python
#
# This script is aim to build a EFS item list, with all known EFS items and its 
# related .conf file. Once platform upgrade or other changes, we shall re-run this 
# script to update EFS item list.
# 
# TODO:
# 1. Launch EFS explorer tool and copy all .conf file under /nv/item_files/conf/ to PC
# 2. Run this script

import os
import sys

filter_files = [
    "hdrsrch_config_info.conf",
    "wcdma_csg_efs_1.conf",
    "wcdma_csg_efs_2.conf",
    "wcdma_csg_efs_3.conf",
    "wcdma_csg_efs_4.conf",   
    "wcdma_csg_efs_5.conf",
    "wcdma_csg_efs_db.conf",
]

# these items request extra parameters, not only file path name, so filter them out.
filter_items = [
]

outputfile = file("confFileList.py", 'w')
outputfile.write("#!/usr/bin/python\n")
outputfile.write("conf_list = {\n")

def usage():
    print "-------------------------------------------------"
    print "Usage: \tPlease launch script in .conf folder, Ex:"
    print "\tcd conf (locate to conf folder)\n\tpython " + sys.argv[0]
    exit(0)

def output(item, filename):
    #print ("%s -> %s" % (item, filename))
    outputfile.write("\t\"%s\" : \"%s\",\n" % (item, filename))

def build_conf_list():
    num_of_conf_files = 0    

    for f in os.listdir('.'):
        # omit all folders
        if os.path.isdir(f) or not f.endswith(".conf"):
            continue
        num_of_conf_files += 1

        # omit file in filter list
        if f in filter_files:
            continue
        
        h = file(f, 'r')
        for item in h:
            item = item.strip()
            # all filter items contain string '_hyst_timer'
            if item.find("_hyst_timer") < 0 : #and not item in filter_items:
                output(item, f)
    
    outputfile.write("}\n")    
    outputfile.close()
    
    if num_of_conf_files == 0:
        print "None of .conf file found"
        usage()
    print "Done. See outout in conf_list.txt"

    return        
        

if __name__ == "__main__": 
    build_conf_list()

