LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := libImageMerge
LOCAL_MODULE_TAGS := optional

LOCAL_SRC_FILES:= \
    com_tct_screenshotedit_nativeimagemerge_ImageMerge.cpp \
    com_tct_screenshotedit_nativeimagemerge_NativeBitmap.cpp \
    Debugger.cpp \
    FeatureCompare.cpp \
    HashCompare.cpp \
    ImageMerge.cpp \
    MultiThreadTask.cpp \
    NativeBitmap.cpp \
    NativeBitmapUtil.cpp \

LOCAL_C_INCLUDES += \
    inc.h \
    log.h \
    Feature.h \
    com_tct_screenshotedit_nativeimagemerge_ImageMerge.h \
    com_tct_screenshotedit_nativeimagemerge_NativeBitmap.h \
    Debugger.h \
    FeatureCompare.h \
    HashCompare.h \
    ImageMerge.h \
    MultiThreadTask.h \
    NativeBitmap.h \
    NativeBitmapUtil.h \
    
LOCAL_SHARED_LIBRARIES := \
    liblog \
#    libandroid_runtime \
#    libnativehelper \
#    libcutils \
#    libutils \
#    libhardware \
#    

#LOCAL_MULTILIB := 32

include $(BUILD_SHARED_LIBRARY)
