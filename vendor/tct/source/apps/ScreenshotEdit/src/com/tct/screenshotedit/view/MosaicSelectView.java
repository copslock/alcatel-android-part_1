package com.tct.screenshotedit.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

public class MosaicSelectView extends View {

    private Context mContext;

    private int index = 0;

    private Paint mPaint;
    private float mWidth;
    private float mSize = 1.0f;

    private boolean mSelected = false;

    public MosaicSelectView(Context context, AttributeSet attrs) {
        super(context, attrs);
        // TODO Auto-generated constructor stub
        mContext = context;
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        // TODO Auto-generated method stub

        mWidth = getWidth();
        float lineWidth = (float) (mWidth * 0.08);

        if (mSelected) {
            mPaint.setColor(Color.WHITE);
            mPaint.setStyle(Paint.Style.FILL);
        } else {
            mPaint.setColor(Color.GRAY);
            mPaint.setStyle(Paint.Style.STROKE);
        }
        mPaint.setStrokeWidth(lineWidth);
        canvas.drawRect((0.5f - mSize * 0.5f) * mWidth, (0.5f - mSize * 0.5f)
                * mWidth, (0.5f + mSize * 0.5f) * mWidth, (0.5f + mSize * 0.5f)
                * mWidth, mPaint);
        super.onDraw(canvas);
    }

    public void setMosaicSelected(boolean selected) {
        mSelected = selected;
        invalidate();
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getIndex() {
        return this.index;
    }

    public void setSize(float size) {
        this.mSize = size;
        invalidate();
    }

}
