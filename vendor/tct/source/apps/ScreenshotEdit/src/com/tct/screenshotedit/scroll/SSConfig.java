package com.tct.screenshotedit.scroll;

import android.os.Environment;

public class SSConfig {
    private static final String DIR = "2";
    private static final String PATH_DIR = Environment.getExternalStorageDirectory() + "/SupershotTest/" + DIR;
    public static final String PATH_IMG_OUT = PATH_DIR + "/out.png";
    public static final String PATH_IMG_BOTTOM = PATH_DIR + "/bottom.png";

    public static final int MAX_SCROLL_TIME = 15;

    private static SSConfig ssc = null;

    private int nScreenWidth = -1;// 屏幕宽度
    private int nScreenHeight = -1;// 屏幕高度
    private int nScreenRotation = -1;
    private int nStatusbarHeight = -1;
    private int nNavigationBarHeight = -1;
    private int nBottomPadding = -1;// 触屏落点起始位置距离底部的距离

    private int nMovelen = -1;// 移动距离
    private int nMoveDuration = -1;
    private int mTouchSlop = -1;

    private long nLaunchDelay = 300;//长截屏启动延迟时间
    private long nActionDelay = 600;//长截屏动作间隔时间
    
    private SSConfig(int nScreenWidth, int nScreenHeight, int nScreenRotation,
            int nStatusbarHeight, int nNavigationBarHeight, int nMovelen, int nBottomPadding, int nMoveDuration, int mTouchSlop) {
        // TODO Auto-generated constructor stub
        this.nScreenWidth = nScreenWidth;
        this.nScreenHeight = nScreenHeight;
        this.nScreenRotation = nScreenRotation;
        this.nStatusbarHeight = nStatusbarHeight;
        this.nNavigationBarHeight = nNavigationBarHeight;
        this.nMovelen = nMovelen;
        this.nBottomPadding = nBottomPadding;
        this.nMoveDuration = nMoveDuration;
        this.mTouchSlop = mTouchSlop;
    }

    public static void init(int nScreenWidth, int nScreenHeight, int nScreenRotation,
            int nStatusbarHeight, int nNavigationBarHeight, int nMovelen, int nBottomPadding, int nMoveDuration, int mTouchSlop){
        ssc = new SSConfig(nScreenWidth, nScreenHeight, nScreenRotation,
                nStatusbarHeight, nNavigationBarHeight, nMovelen, nBottomPadding, nMoveDuration, mTouchSlop);
    }

    public static SSConfig getInstance() {
        if (ssc == null) {
            throw new IllegalStateException("SSConfig is null !");
        }
        return ssc;
    }

    public int getnMoveDuration() {
        return nMoveDuration;
    }

    public int getnScreenWidth() {
        return nScreenWidth;
    }

    public int getnScreenHeight() {
        return nScreenHeight;
    }

    public int getnScreenRotation() {
        return nScreenRotation;
    }

    public int getnStatusbarHeight() {
        return nStatusbarHeight;
    }

    public int getnNavigationBarHeight() {
        return nNavigationBarHeight;
    }

    public int getnMovelen() {
        return nMovelen;
    }
    
    public int getnBottomPadding() {
        return nBottomPadding;
    }
    
    public int getmTouchSlop() {
        return mTouchSlop;
    }

    public boolean getnDirection() {
        return nScreenHeight > nScreenWidth ? true : false;
    }
    
    public long getLaunchDelay() {
        return nLaunchDelay;
    }

    public long getActionDelay() {
        return nActionDelay;
    }

}
