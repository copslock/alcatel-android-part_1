package com.tct.screenshotedit.view;

import com.tct.screenshotedit.impl.IOnImageScrolled;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ImageView;

/**
 * Created by elvis on 16/10/13.
 */

public class Preview extends ImageView {
    private static int w;
    private static int h;
    private float scale = 1;
    private IOnImageScrolled scrollCallback;

    Paint paint;
    Paint lPaint;
    Paint cropPaint;
    Paint linePaint;
    private int top;
    private int bottom;

    public Preview(Context context) {
        super(context);
        initPaint();
    }

    public Preview(Context context, AttributeSet attrs) {
        super(context, attrs);
        initPaint();
    }

    public Preview(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initPaint();
    }

    private void initPaint() {
        paint = new Paint();
        paint.setColor(Color.GREEN);
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        paint.setAlpha(80);

        lPaint = new Paint();
        lPaint.setColor(Color.GREEN);
        lPaint.setStyle(Paint.Style.STROKE);
        lPaint.setStrokeWidth(2);

        linePaint = new Paint();
        linePaint.setColor(Color.BLACK);
        linePaint.setStyle(Paint.Style.FILL);
        linePaint.setStrokeWidth(1);

        cropPaint = new Paint();
        cropPaint.setColor(Color.DKGRAY);
        cropPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        cropPaint.setAlpha(180);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float y = event.getY();
        switch (event.getAction()) {
        case MotionEvent.ACTION_MOVE:
            if (scrollCallback != null)
                scrollCallback.onPreviewScroll(y / h);
            updateRect(y);
            break;
        case MotionEvent.ACTION_DOWN:
            if (scrollCallback != null)
                scrollCallback.onPreviewScroll(y / h);
            updateRect(y);
            break;
        case MotionEvent.ACTION_UP:
            updateRect(y);
            break;
        default:
            return super.onTouchEvent(event);
        }

        invalidate();
        return true;

    }

    private int strokewidth = 2;
    private int thumbHeight;
    private Rect r = new Rect(strokewidth, strokewidth, w - strokewidth,
            thumbHeight - strokewidth);

    /**
     * TO be sure do not update out of Image rect
     * 
     * @param y
     *            offset Y
     * @return fixed Y
     */
    private float getFixedY(float y) {
        if (y <= 0)
            y = 0;
        if (y >= h - thumbHeight)
            y = h - thumbHeight;
        return y;
    }

    public void updateRect(float y) {

        y = getFixedY(y);
        int offset = (int) y;
        r = new Rect(strokewidth, strokewidth + offset, w - strokewidth,
                thumbHeight + offset - strokewidth);

    }

    public void updateRectWithOutFix(float y) {
        int offset = (int) y;
        r = new Rect(strokewidth, strokewidth + offset, w - strokewidth,
                thumbHeight + offset - strokewidth);
        invalidate();
    }

    public void updateCropRect(int top, int bottom) {

        this.top = top;
        this.bottom = bottom;

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        h = getMeasuredHeight();
        w = getMeasuredWidth();
        thumbHeight = (int) (h / scale);
        updateRect(0);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvas.drawRect(r, paint);

        canvas.drawRect(r, lPaint);
        drawTopCropArea(canvas, top);
        drawBottomCropArea(canvas, bottom);
    }

    private void drawTopCropArea(Canvas canvas, int offset) {

        int startX = (getWidth() - w) / 2;

        // draw like this
        // |---------------|
        // | |
        canvas.drawLine(startX, offset, startX, offset + 10, linePaint);
        canvas.drawLine(w + startX, offset, w + startX, offset + 10, linePaint);
        canvas.drawLine(startX, offset, w + startX, offset, linePaint);

        canvas.drawRect(new Rect(startX, 0, w + startX, offset), cropPaint);
    }

    private void drawBottomCropArea(Canvas canvas, int offset) {

        int startX = (getWidth() - w) / 2;

        // draw like this
        // | |
        // |_______________|
        canvas.drawLine(startX, offset - 10, startX, offset, linePaint);
        canvas.drawLine(w + startX, offset - 10, w + startX, offset, linePaint);
        canvas.drawLine(startX, offset, w + startX, offset, linePaint);

        canvas.drawRect(new Rect(startX, offset, w + startX, h), cropPaint);
    }

    public void setScale(float scale) {
        this.scale = scale;
    }

    public void setOnScrollCallback(IOnImageScrolled scrollCallback) {
        this.scrollCallback = scrollCallback;

    }

}