package com.tct.screenshotedit.scroll;

import com.tct.screenshotedit.R;

import android.content.Context;
import android.graphics.PixelFormat;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class DemoFloarview extends RelativeLayout{

    public LinearLayout linearLayout;
    public Button btnStart;
    public Button btnTest;
    public Button btnStop;
    public TextView txtStatus;
    public TextView txtMsg;
    private View mRootView;

    public DemoFloarview(Context context) {
        super(context);
        // TODO Auto-generated constructor stub
        CTools.PrintLogImage("DemoFloarview()");
        mRootView = inflate(getContext(), R.layout.demofloatview, null);
        linearLayout = (LinearLayout)mRootView.findViewById(R.id.background);
        btnStart = (Button) mRootView.findViewById(R.id.btnStart);
        btnTest = (Button) mRootView.findViewById(R.id.btnTest);
        btnStop = (Button) mRootView.findViewById(R.id.btnStop);
        txtStatus = (TextView) mRootView.findViewById(R.id.status);
        txtMsg = (TextView) mRootView.findViewById(R.id.msg);
        addView(mRootView, new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));
    }

    
    public void showWindow() {
        CTools.PrintLogImage("DemoFloarview--showWindow()");
        WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
        wm.addView(this, getWindowLayoutParams());
    }

    public void dismissWindow() {
        WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
        wm.removeView(this);
    }

    private WindowManager.LayoutParams getWindowLayoutParams() {
        WindowManager.LayoutParams param = new WindowManager.LayoutParams();

        param.type = WindowManager.LayoutParams.TYPE_PHONE;
        param.format = PixelFormat.RGBA_8888;
        param.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE; // 不能抢占聚焦点
        param.flags = param.flags| WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL;
        //param.flags = param.flags | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH;
        param.flags = param.flags | WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN; // 排版不受限制

        //param.flags = param.flags| WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE;//实现点击穿透！！！

        param.gravity = Gravity.LEFT | Gravity.TOP; // 调整悬浮窗口至左上角

        // 以屏幕左上角为原点，设置x、y初始值
        param.x = 0;
        param.y = 0;

        // 设置悬浮窗口长宽数据
        param.width = WindowManager.LayoutParams.MATCH_PARENT;
        param.height = WindowManager.LayoutParams.MATCH_PARENT;

        return param;
    }

    public void TemporaryHide(boolean bValue)
    {
        setVisibility(bValue?View.INVISIBLE:View.VISIBLE);
    }

}
