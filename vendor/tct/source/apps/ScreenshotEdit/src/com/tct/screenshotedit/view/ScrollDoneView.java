package com.tct.screenshotedit.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class ScrollDoneView extends View {
    private Paint mPaint;



    private int mBackgroundColor = Color.WHITE;
    private int mFgShapeColor = Color.BLACK;

    private float mWidth;

    public ScrollDoneView(Context context, AttributeSet attrs) {
        super(context, attrs);
        // TODO Auto-generated constructor stub
        mContext = context;
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // TODO Auto-generated method stub
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            this.setAlpha(0.7f);
        } else if (event.getAction() == MotionEvent.ACTION_UP) {
            this.setAlpha(1.0f);
        }
        return super.onTouchEvent(event);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        // TODO Auto-generated method stub
        mWidth = getWidth();
        float center = (float) (mWidth * 0.5);
        float ringWidth = (float) (mWidth * 0.05);

        float line1SX = (float) (mWidth * 0.25);
        float line1SY = (float) (mWidth * 0.45);
        float line1EX = (float) (mWidth * 0.4);
        float line1EY = (float) (mWidth * 0.65);

        float line2SX = (float) (mWidth * 0.4 - ringWidth / 4.0 * Math.sqrt(2));
        float line2SY = (float) (mWidth * 0.65 + ringWidth / 4.0 * Math.sqrt(2));
        float line2EX = (float) (mWidth * 0.75);
        float line2EY = (float) (mWidth * 0.3875);

        mPaint.setStyle(Paint.Style.FILL);
        mPaint.setColor(mBackgroundColor);
        canvas.drawCircle(center, center, center - 1, mPaint);

        mPaint.setStyle(Paint.Style.FILL);
        mPaint.setStrokeWidth(ringWidth);
        mPaint.setColor(mFgShapeColor);
        canvas.drawLine(line1SX, line1SY, line1EX, line1EY, mPaint);
        canvas.drawLine(line2SX, line2SY, line2EX, line2EY, mPaint);

        super.onDraw(canvas);
    }
}
