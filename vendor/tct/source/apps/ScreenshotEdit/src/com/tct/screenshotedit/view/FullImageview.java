package com.tct.screenshotedit.view;

import com.tct.screenshotedit.impl.onCropStateChanged;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.ImageView;

/**
 * Created by elvis on 16/10/15.
 */

public class FullImageview extends ImageView {
    private static int w;
    private static int h;

    Paint paint;
    Paint linePaint;
    private onCropStateChanged onCropStatus;

    public FullImageview(Context context) {
        super(context);
        initPaint();
    }

    public FullImageview(Context context, AttributeSet attrs) {
        super(context, attrs);
        initPaint();
    }

    public FullImageview(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initPaint();
    }

    private void initPaint() {
        paint = new Paint();
        paint.setColor(Color.DKGRAY);
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        paint.setAlpha(120);

        // canvas.drawRect(new Rect(0, h - 20, w, h), paint);

        linePaint = new Paint();
        linePaint.setColor(Color.RED);
        linePaint.setStyle(Paint.Style.FILL);
        linePaint.setStrokeWidth(15);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        h = getMeasuredHeight();
        drawTopCropArea(canvas, defaultTopOffset);
        drawBottomCropArea(canvas, defaultBottomOffset);

        onCropStatus.onCropRectUpdate(defaultTopOffset, defaultBottomOffset);

    }

    private int defaultBottomOffset = 0;
    private int defaultTopOffset = 0;
    private int parentScrollViewHeight = 0;
    private int parentScrollViewY = 0;
    private boolean topCropMove = false;
    private boolean bottomCropMove = false;

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int tempoffset = (int) event.getY();
        Log.v("kobe", "getY----->" + tempoffset);
        int rawY = (int) event.getRawY();
        switch (event.getAction()) {
        case MotionEvent.ACTION_MOVE:

            if (topCropMove || bottomCropMove) {

                if (bottomCropMove) {
                    defaultBottomOffset = Math.min(this.h, (int) event.getY());
                }

                if (topCropMove) {
                    defaultTopOffset = Math.max(0, (int) event.getY());
                }
                if (defaultBottomOffset < defaultTopOffset + 200) {
                    defaultTopOffset = defaultBottomOffset - 200;
                    // defaultBottomOffset =defaultTopOffset+200;
                }
                Log.v("kobe", "parentScrollViewY----->" + parentScrollViewY);
                Log.v("kobe", "parentScrollViewHeight----->"
                        + parentScrollViewHeight);
                if (rawY <= parentScrollViewY) {
                    onCropStatus.onCropAutoScrolled(-20);
                }
                if (rawY >= parentScrollViewY + parentScrollViewHeight) {
                    onCropStatus.onCropAutoScrolled(20);
                }
            }
            break;
        case MotionEvent.ACTION_DOWN:
            // draw bottom
            if (Math.abs(tempoffset - cropBottomY) < 80) {
                defaultBottomOffset = tempoffset;
                linePaint.setStrokeWidth(30);
                bottomCropMove = true;
            }
            // draw top
            if (Math.abs(tempoffset - cropTopY) < 80) {
                defaultTopOffset = tempoffset;
                linePaint.setStrokeWidth(30);
                topCropMove = true;
            }
            onCropStatus.onCropStateChanged(topCropMove || bottomCropMove);
            break;
        case MotionEvent.ACTION_UP:
            // reset
            linePaint.setStrokeWidth(15);
            bottomCropMove = false;
            topCropMove = false;
            onCropStatus.onCropStateChanged(topCropMove || bottomCropMove);
            break;
        default:
            return true;
        }

        invalidate();
        return true;
    }

    private static int cropTopY;
    private static int cropBottomY;

    private void drawTopCropArea(Canvas canvas, int offset) {

        int startX = (getWidth() - w) / 2;

        // draw like this
        // |---------------|
        // | |
        canvas.drawLine(startX, offset, startX, offset + 100, linePaint);
        canvas.drawLine(w + startX, offset, w + startX, offset + 100, linePaint);
        canvas.drawLine(startX, offset, w + startX, offset, linePaint);

        canvas.drawRect(new Rect(startX, 0, w + startX, offset), paint);
        cropTopY = offset;
    }

    private void drawBottomCropArea(Canvas canvas, int offset) {

        int startX = (getWidth() - w) / 2;

        // draw like this
        // | |
        // |_______________|
        canvas.drawLine(startX, offset - 100, startX, offset, linePaint);
        canvas.drawLine(w + startX, offset - 100, w + startX, offset, linePaint);
        canvas.drawLine(startX, offset, w + startX, offset, linePaint);

        canvas.drawRect(new Rect(startX, offset, w + startX, h), paint);
        cropBottomY = offset;
    }

    public void setWH(int scaleImageWidth, int height, int scrollViewHeight,
            int scrollViewY) {
        this.w = scaleImageWidth;
        this.defaultBottomOffset = height;
        this.parentScrollViewHeight = scrollViewHeight;
        this.parentScrollViewY = scrollViewY;
    }

    public void setonCropStatus(onCropStateChanged onCropStatus) {
        this.onCropStatus = onCropStatus;
    }

}
