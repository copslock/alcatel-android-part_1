package com.tct.screenshotedit.scroll;

import android.util.Log;

/**
 * 耦合工具类
 * @author shi
 *
 */
public class CTools {
    public static final String TAG = "CTools";

    private static final String DEBUG_IMAGE = "==MyTest==";
    private static final String DEBUG_PERFORMANCE = "==Performance==";
    private static final boolean bDebug = true;

    /**
     * 打印log使用，依赖开关bDebug
     * @param msg 需要打印的log信息
     */
    public static void PrintLogImage(String msg) {
        if (bDebug && msg != null) {
            Log.i(DEBUG_IMAGE, msg);
        }
    }
    public static void PrintLogPerformance(String msg) {
        if (bDebug && msg != null) {
            Log.i(DEBUG_PERFORMANCE, msg);
        }
    }
}
