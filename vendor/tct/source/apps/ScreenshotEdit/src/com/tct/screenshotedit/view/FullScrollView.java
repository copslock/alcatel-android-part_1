package com.tct.screenshotedit.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ScrollView;

/**
 * Created by elvis on 16/10/15.
 */

public class FullScrollView extends ScrollView {

    private boolean isCropMove = false;

    public FullScrollView(Context context) {
        super(context);
    }

    public FullScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FullScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setCropMove(boolean cropMove) {
        isCropMove = cropMove;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {

        // L.d("onInterceptTouchEvent");
        return (!isCropMove) && super.onInterceptTouchEvent(ev);
    }

    /**
     * @param ev
     * @return
     */
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        // L.d("dispatchTouchEvent");
        return super.dispatchTouchEvent(ev);
    }
}
