package com.tct.screenshotedit.impl;

/**
 * Created by elvis on 16/10/15.
 */

public interface onCropStateChanged {

      void onCropStateChanged(boolean status);
      void onCropAutoScrolled(int offset);
      void onCropRectUpdate(int top, int bottom);
}
