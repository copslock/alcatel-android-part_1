package com.tct.screenshotedit;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Iterator;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.Rect;
import android.net.Uri;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

public class MyDrawView extends View {

    private String TAG = "zhuang MyDrawView";
    private Bitmap bitmap_draw;
    private Bitmap bitmap_orig;
    private Bitmap bitmapMosaic;
    private Canvas mCanvas;
    private Paint bitmapPaint;
    private Paint pathPaint;
    private Path mPath;
    public Uri uri;
    DisplayMetrics dm = new DisplayMetrics();
    private int heightDrawContainer = 0; // Layout container height
    private int widthDrawContainer = 0;
    private int widthBt = 0;
    private int heightBt = 0;
    private int origWidthBt = 0;
    private int origHeightBt = 0;
    float scale = 0;
    private String mImageFilePath;

    private float displayScale = 0.75f;
    private int drawLeftTopX = 0;
    private int drawLeftTopY = 0;

    private int mPathPaintStrokeWidth = 20;
    private Paint.Cap mPathPaintCap = Paint.Cap.ROUND;

    public static final int typePen = 0;
    public static final int typeThickPen = 1;
    public static final int typeMarkPen = 2;
    public static final int typeMosaic = 3;

    private int mosaicSize = 0;
    private int masaicUnit = 15;
    private int drawType = typePen;
    private int penColor = Color.RED;
    private static final int maxCountUndo = 20;  //the max steps can undo
    private int countUndo = 0;
    private boolean canUndo = false;
    public boolean hasDraw = false;

    private static final int modeNone = 0;
    private static final int modeSinglePointer = 1;
    private static final int modeMultiPointer = 2;
    private int mode = modeNone;
    private float distance;
    private float preDistance;
    private Matrix  origMatrix = new Matrix();
    private Matrix preMatrix = new Matrix();
    private Matrix savedMatrix = new Matrix();
    //private PointF mid = new PointF();  //两指中点

    // 保存Undo Path路径的集合，用List集合来模拟栈
    private ArrayList<DrawPath> savePath = new ArrayList<DrawPath>();

    // 保存已固定Path路径的集合，用List集合来模拟栈
    private ArrayList<DrawPath> saveFixPath = new ArrayList<DrawPath>();

    // 记录Draw路径的对象
    private DrawPath dp;

    // 记录Mosaic路径点的对象
    private mosaicPoint mSP;

    private class DrawPath {
        private Path path;
        private Paint paint;
        private int type;
        private ArrayList<mosaicPoint> mosaicPoints = new ArrayList<mosaicPoint>();
        private int moSize; // Mosaic size
    }

    private class mosaicPoint {
        private int mX;
        private int mY;
    }

    private boolean invalidTouchDown = false;
    private boolean invalidTouchMove = false;

    private boolean isInValidXY(int x, int y) {
        boolean ret = false;
        if ((x < drawLeftTopX) || (x > (drawLeftTopX + widthBt) || (y < drawLeftTopY) || (y > drawLeftTopY + heightBt))) {
            ret = true;
        }
        return ret;
    }

    private void initialDrawPath(float x, float y) {
        dp = new DrawPath();
        if (drawType == typePen || drawType == typeThickPen || drawType == typeMarkPen) {
            mPath = new Path();
            pathPaint = new Paint();
            pathPaint.setColor(penColor);
            pathPaint.setStyle(Paint.Style.STROKE);
            pathPaint.setStrokeCap(mPathPaintCap);
            pathPaint.setStrokeWidth(mPathPaintStrokeWidth);

            dp.path = mPath;
            dp.paint = pathPaint;
            dp.type = typePen;
            mPath.moveTo(x, y);
            mX = x;
            mY = y;
        } else if (drawType == typeMosaic) {
            mX = x;
            mY = y;
            mSP = new mosaicPoint();
            mSP.mX = (int) x;
            mSP.mY = (int) y;
            dp.mosaicPoints.add(mSP);
            dp.type = typeMosaic;
            bitmapMosaic = bitmap_draw;
            Log.i(TAG, "mSP.mX=" + mSP.mX + " ,mSP.mY="
                    + mSP.mY + " ,Left="
                    + (mSP.mX - mosaicSize / 2) + ", Leftx="
                    + (int) (mX + mosaicSize / 2));
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // TODO Auto-generated method stub
        float x = event.getX();
        float y = event.getY();
        float size = event.getSize();

        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                Log.i(TAG, "touch down, type=" + drawType+" ,x="+x+" ,y="+y);
                mode = modeSinglePointer;
                if (isInValidXY((int)x, (int)y)) {
                    invalidTouchDown = true;
                    invalidTouchMove = true;
                    break;
                } else {
                    hasDraw = true;
                    initialDrawPath(x, y);
                }
                invalidate();
                break;
            case MotionEvent.ACTION_POINTER_DOWN:
                Log.i(TAG, "ACTION_POINTER_DOWN:"+event.getX(0)+","+event.getY(0)+","+event.getX(1)+","+event.getY(1));
                mode = modeMultiPointer;
                preDistance =getDistance(event);
                Log.i("testone","preDistance="+preDistance);
                if (preDistance > 10f) {
                   // preMatrix.post
                }
                break;
            case MotionEvent.ACTION_MOVE:
                if (isInValidXY((int)x, (int)y)) {
                    invalidTouchMove = true;
                } else if (invalidTouchDown) {
                    hasDraw = true;
                    initialDrawPath(x, y);
                    invalidTouchDown = false;
                } else {
                    if (invalidTouchMove && mPath != null) {
                        mPath.moveTo(x, y);
                        invalidTouchMove = false;
                    }
                    invalidTouchMove = false;
                }

                if (mode == modeSinglePointer) {
                    if (drawType == typePen || drawType == typeThickPen || drawType == typeMarkPen) {
                        if (!invalidTouchMove && mPath != null) {
                            mPath.quadTo(mX, mY, x, y);
                            invalidate();
                        }
                        mX = x;
                        mY = y;
                    } else if (drawType == typeMosaic) {
                        float dx = Math.abs(x - mX);
                        float dy = Math.abs(y - mY);
                        if (dx > masaicUnit / 2 || dy > masaicUnit / 2) {
                            mX = x;
                            mY = y;
                            if (!invalidTouchMove) {
                                mSP = new mosaicPoint();
                                mSP.mX = (int) x;
                                mSP.mY = (int) y;
                                dp.mosaicPoints.add(mSP);
                                bitmapMosaic = getMosaicPointBitmap(bitmapMosaic,
                                        new Rect(mSP.mX - mosaicSize / 2, mSP.mY
                                                - mosaicSize / 2, mSP.mX + mosaicSize
                                                / 2, mSP.mY + mosaicSize / 2),
                                        masaicUnit);
                                mCanvas.drawBitmap(bitmapMosaic, 0, 0, bitmapPaint);
                                invalidate();
                            }
                        }
                    }
                } else if (mode == modeMultiPointer) {
                    distance =getDistance(event);
                    Log.i("testone","distance="+distance);
                }
                break;
            case MotionEvent.ACTION_UP:
                Log.i(TAG, "ACTION_UP mPath="+mPath);
                if (invalidTouchDown && invalidTouchMove) {
                    break;
                }
                if (mode == modeSinglePointer) {
                    if (drawType == typePen || drawType == typeThickPen || drawType == typeMarkPen) {
                        mCanvas.drawPath(mPath, pathPaint);
                        updateUndoPath();
                        invalidate();
                    } else if (drawType == typeMosaic) {
                        updateUndoPath();
                    }
                }
                mode = modeNone;
                invalidTouchDown = false;
                invalidTouchMove = false;
                break;
            case MotionEvent.ACTION_POINTER_UP:
                Log.i("testone", "ACTION_POINTER_UP");
                mode = modeNone;
                break;
        }
        return true;
    }

    private float mX, mY;

    //获取两指之间的距离
    private float getDistance(MotionEvent event) {
        float x = event.getX(1) - event.getX(0);
        float y = event.getY(1) - event.getY(0);
        float dis = (float) Math.sqrt(x*x + y*y);
        return dis;
    }

    private void touch_start(float x, float y) {
        mPath.moveTo(x, y);
        mX = x;
        mY = y;
    }

    private void touch_move(float x, float y) {
        mPath.quadTo(mX, mY, x, y);
        mX = x;
        mY = y;
    }

    private void touch_up() {
        mCanvas.drawPath(mPath, pathPaint);
        Log.i(TAG, "touch up, pathPaint color=" + dp.paint.getColor());
        updateUndoPath();
    }

    //Save undo path according to max setting
    private void updateUndoPath() {
        Log.i(TAG, "updateUndoPath, savePath.size="+savePath.size());
        while ((savePath.size() >= maxCountUndo)) {
            saveFixPath.add(savePath.get(0));
            savePath.remove(0);
        }
        //if (savePath.size() < maxCountUndo) {
            savePath.add(dp);
            if (drawType == typePen || drawType == typeThickPen || drawType == typeMarkPen) {
                mPath = null;
                pathPaint = null;
            } else if (drawType == typeMosaic) {
                mSP = null;
                bitmapMosaic = null;
            }
        //}
    }

    //Saved when click done after draw
    public void save(boolean scroll) {
        Log.i(TAG, "draw save bitmap height="+bitmap_draw.getHeight()+" ,width="+bitmap_draw.getWidth()+" , "+heightBt+","+widthBt+",leftX="+drawLeftTopX+" ,LeftY="+drawLeftTopY);
        if (!scroll) {
            bitmap_draw = Bitmap.createBitmap(bitmap_draw, drawLeftTopX, drawLeftTopY, widthBt, heightBt);
        } else {
            bitmap_draw = Bitmap.createBitmap(bitmap_draw, drawLeftTopX, drawLeftTopY, widthBt, heightBt);
            bitmap_draw = Bitmap.createScaledBitmap(bitmap_draw, origWidthBt, origHeightBt, true);
            Log.i(TAG, "scroll draw save bitmap height="+bitmap_draw.getHeight()+" ,width="+bitmap_draw.getWidth());
        }
        try {
            FileOutputStream fos = new FileOutputStream(mImageFilePath);
            bitmap_draw.compress(CompressFormat.PNG, 100, fos);
            fos.close();
        } catch (Exception e) {
            Log.i(TAG, "save exception:" + e.toString());
        }
    }

    public void undo() {
        Log.i(TAG, "undo savePath savePath.size()=" + savePath.size()+" ,saveFixPath="+saveFixPath);
        if (savePath != null && savePath.size() > 0) {
            savePath.remove(savePath.size() - 1);  //remove last add one
            redrawOnBitmap();
        }
    }

    //Undo main body
    private void redrawOnBitmap() {
        loadBitmap(); // 重新设置画布，相当于清空画布
        Iterator<DrawPath> iter = saveFixPath.iterator();
        while (iter.hasNext()) {
            DrawPath drawPath = iter.next();
            if (drawPath.type == typePen) {
                Log.i(TAG, "redrawOnBitmap, draw pen...");
                mCanvas.drawPath(drawPath.path, drawPath.paint);
            } else if (drawPath.type == typeMosaic) {
                Log.i(TAG, "redrawOnBitmap, draw mosaic...");
                bitmapMosaic = bitmap_draw;
                bitmapMosaic = getMosaicPathBitmap(bitmapMosaic,
                        drawPath.mosaicPoints, masaicUnit);
                mCanvas.drawBitmap(bitmapMosaic, 0, 0, bitmapPaint);
            }
        }

        iter = savePath.iterator();
        while (iter.hasNext()) {
            DrawPath drawPath = iter.next();
            if (drawPath.type == typePen) {
                Log.i(TAG, "redrawOnBitmap, draw pen...");
                mCanvas.drawPath(drawPath.path, drawPath.paint);
            } else if (drawPath.type == typeMosaic) {
                Log.i(TAG, "redrawOnBitmap, draw mosaic...");
                bitmapMosaic = bitmap_draw;
                bitmapMosaic = getMosaicPathBitmap(bitmapMosaic,
                        drawPath.mosaicPoints, masaicUnit);
                mCanvas.drawBitmap(bitmapMosaic, 0, 0, bitmapPaint);
            }
        }
        invalidate();
    }

    //set pen color
    public void setColor(int color) {
        penColor = color;
    }

    //get pen color
    public int getColor() {
        return penColor;
    }

    //set draw type, pen or mosaic
    public void setDrawType(int type) {
        drawType = type;
        if (drawType == typePen) {
            mPathPaintStrokeWidth = 5;
            mPathPaintCap = Paint.Cap.ROUND;
        } else if (drawType == typeThickPen) {
            mPathPaintStrokeWidth = 15;
            mPathPaintCap = Paint.Cap.ROUND;
        } else if (drawType == typeMarkPen) {
            mPathPaintStrokeWidth = 25;
            mPathPaintCap = Paint.Cap.SQUARE;
        }
    }

    // get draw type
    public int getDrawType() {
        return drawType;
    }

    public void setMosaicSize(int size) {
        Log.i(TAG, "setMosaicSize mosaicSize=" + mosaicSize);
        mosaicSize = size;
    }

    //Used for undo
    private Bitmap getMosaicPathBitmap(Bitmap bmp, ArrayList<mosaicPoint> mPts,
            int unit) {
        long start = System.currentTimeMillis();

        int bmpW = bmp.getWidth();
        int bmpH = bmp.getHeight();
        int[] pixels = new int[bmpH * bmpW];
        bmp.getPixels(pixels, 0, bmpW, 0, 0, bmpW, bmpH);

        int rectH = mosaicSize;
        int rectW = mosaicSize;

        Iterator<mosaicPoint> pIter = mPts.iterator();
        while (pIter.hasNext()) {
            mosaicPoint mP = pIter.next();
            int rectLeft = mP.mX - mosaicSize / 2;
            int rectTop = mP.mY - mosaicSize / 2;

            for (int i = rectTop; i < (rectTop + rectH);) {
                for (int j = rectLeft; j < (rectLeft + rectW);) {
                    int leftTopPoint = i * bmpW + j;
                    for (int k = 0; k < unit; k++) {
                        for (int m = 0; m < unit; m++) {
                            int point = (i + k) * bmpW + (j + m);
                            if (point < pixels.length) {
                                pixels[point] = pixels[leftTopPoint];
                            }
                        }
                    }
                    j += unit;
                }
                i += unit;
            }
        }

        long end = System.currentTimeMillis();
        Log.i(TAG, "DrawMosaicPathTime=" + (end - start));
        return Bitmap.createBitmap(pixels, bmpW, bmpH, Config.ARGB_8888);
    }

    private Bitmap getMosaicPointBitmap(Bitmap bmp, Rect targetRect, int unit) {
        long start = System.currentTimeMillis();
        int bmpW = bmp.getWidth();
        int bmpH = bmp.getHeight();
        Log.i("zhuang", "getMosaicPointBitmap" + " ,unit=" + unit + " ,bmpW="
                + bmpW + " ,bmpH=" + bmpH);
        if (targetRect.isEmpty()) {
            Log.i(TAG, "targetRect is empty!!!");
            targetRect.set(0, 0, bmpW, bmpH);
        }
        int rectW = targetRect.width();
        int rectH = targetRect.height();
        int[] pixels = new int[bmpH * bmpW];
        bmp.getPixels(pixels, 0, bmpW, 0, 0, bmpW, bmpH);
        Log.i(TAG, "targetRect rectW=" + rectW + " ,rectH=" + rectH
                + " ,pixels.length=" + pixels.length);

        int rectLeft = targetRect.left;
        int rectTop = targetRect.top;
        try {
            for (int i = rectTop; i < (rectTop + rectH);) {
                for (int j = rectLeft; j < (rectLeft + rectW);) {
                    int leftTopPoint = i * bmpW + j;
                    for (int k = 0; k < unit; k++) {
                        for (int m = 0; m < unit; m++) {
                            int point = (i + k) * bmpW + (j + m);
                            if (point < pixels.length) {
                                pixels[point] = pixels[leftTopPoint];
                            }
                        }
                    }
                    j += unit;
                }
                i += unit;
            }
        } catch (Exception e) {
            Log.i(TAG, "e:" + e.toString());
        }
        long end = System.currentTimeMillis();
        Log.i(TAG, "DrawMosaicPointTime=" + (end - start));
        return Bitmap.createBitmap(pixels, bmpW, bmpH, Config.ARGB_8888);
    }

    private void initBitmap(String path) {
        Log.i(TAG, "initBitmap...path="+path);
        if (path != null && (!"".equals(path))) {
            mImageFilePath = path;
            try {
                bitmap_orig = BitmapFactory.decodeFile(path);
                origWidthBt = widthBt = bitmap_orig.getWidth();
                origHeightBt = heightBt = bitmap_orig.getHeight();

                heightDrawContainer = getResources().getDisplayMetrics().heightPixels + getNavigationBarH(this.getContext());
                widthDrawContainer = getResources().getDisplayMetrics().widthPixels;
                heightDrawContainer = (int) (heightDrawContainer * displayScale);

                float scaleX = (float) (widthDrawContainer) / widthBt;
                float scaleY = (float) (heightDrawContainer) / heightBt;
                scale = Math.min(scaleX, scaleY);
                origMatrix.setScale(scale, scale);

                widthBt = (int) (widthBt * scale);
                heightBt = (int) (heightBt * scale);

                Log.i(TAG, "bitmap width=" + bitmap_orig.getWidth()
                        + " ,bitmap height=" + bitmap_orig.getHeight());
                Log.i(TAG, "width=" + widthBt + " ,height=" + heightBt
                        + " ,scaleX=" + scaleX + " ,scaleY=" + scaleY);
                Log.i(TAG, "widthDrawContainer=" + widthDrawContainer
                        + " ,heightDrawContainer=" + heightDrawContainer);

                drawLeftTopX = (widthDrawContainer - widthBt) / 2;
                drawLeftTopY = (heightDrawContainer - heightBt) / 2;
                //bitmap_orig = Bitmap.createBitmap(bitmap_orig, 0, 0, widthCrop, heightCrop, origMatrix, true);
                bitmap_orig = Bitmap.createScaledBitmap(bitmap_orig, widthBt, heightBt, true);
            } catch (Exception e) {
                Log.i(TAG, "Bitmap init e:" + e.toString());
            }
        }
    }

    /**
     * 获取手机stausbar高度
     * @param con
     * @return
     */
    public int getStatusBarH(Context con) {
        int result = 0;
        int resourceId = con.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = con.getResources().getDimensionPixelSize(resourceId);
        }
        if (result == 0) {
            Log.i(TAG, "error,How could StatusBarHeight be zero");
        }
        return result;
    }
    
    /**
     * Get navigation bar height
     * @param con
     * @return
     */
    public int getNavigationBarH(Context con) {
        int result = 0;
        int resourceId = con.getResources().getIdentifier("navigation_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = con.getResources().getDimensionPixelSize(resourceId);
        }
        if (result == 0) {
            Log.i(TAG, "error,How could NavigationBarHeight be zero");
        }
        return result;
    }

    private void loadBitmap() {
        Log.i(TAG, "loadBitmap...");
        try {
            bitmap_draw = Bitmap.createBitmap(widthDrawContainer, heightDrawContainer, Config.ARGB_8888);
            mCanvas = new Canvas(bitmap_draw);
            mCanvas.drawBitmap(bitmap_orig, drawLeftTopX, drawLeftTopY, null);
        } catch (Exception e) {
            Log.i(TAG, "Bitmap load e:" + e.toString());
        }
    }

    public MyDrawView(Context context, String path) {
        super(context);
        initBitmap(path);
        loadBitmap();
        uri = Uri.fromFile(new File("/sdcard/Pictures/image.png"));
        bitmapPaint = new Paint(Paint.DITHER_FLAG);
    }

    public MyDrawView(Context context, int w, int h) {
        super(context);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        // TODO Auto-generated method stub
        super.onDraw(canvas);
        //canvas.setMatrix(matrix);
        if (drawType == typeMosaic) {
            canvas.drawBitmap(bitmap_draw, 0, 0, bitmapPaint);
            Log.i(TAG, "type mosaic onDraw, savePath =" + savePath
                    + " ,savePath size=" + savePath.size());
        } else {
            Log.i(TAG, "type pen onDraw, savePath =" + savePath
                    + " ,mPath=" + mPath);
            canvas.drawBitmap(bitmap_draw, 0, 0, bitmapPaint);
            if ((mPath != null) && (pathPaint != null)) {
                canvas.drawPath(mPath, pathPaint);
            }
        }
    }

    public void recycleBmp() {
        Log.i(TAG, "recycleBmp()");
        if (bitmap_draw != null && !bitmap_draw.isRecycled()) {
            bitmap_draw.recycle();
        }
        if (bitmap_orig != null && !bitmap_orig.isRecycled()) {
            bitmap_orig.recycle();
        }
        if (bitmapMosaic != null && !bitmapMosaic.isRecycled()) {
            bitmapMosaic.recycle();
        }
    }
}